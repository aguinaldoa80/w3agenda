package br.com.linkcom.sined.geral.bean;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.DoubleUtils;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.CalculoMarkupItemInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.CalculoTicketMedioPorFornecedor;
import br.com.linkcom.sined.geral.bean.auxiliar.InclusaoLoteVendaInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.PneuItemVendaInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.ValidateLoteestoqueObrigatorioInterface;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicms;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicmsst;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoicms;
import br.com.linkcom.sined.util.MoneyUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_vendamaterial", sequenceName = "sq_vendamaterial")
public class Vendamaterial implements VendaMaterialProducaoItemInterface, PneuItemVendaInterface, PopupImpostoVendaInterface, CalculoMarkupItemInterface, ValidateLoteestoqueObrigatorioInterface, InclusaoLoteVendaInterface, CalculoTicketMedioPorFornecedor{

	protected Integer cdvendamaterial;
	protected Integer ordem;
	protected Venda venda;
	protected Material material;
	protected Material materialmestre;
	protected Double quantidade;
	protected Double comprimento;
	protected Double comprimentooriginal;
	protected Double largura;
	protected Double altura;
	protected Double fatorconversao;
	protected Double qtdereferencia;
	protected Double preco;
	protected Money desconto;
	protected Unidademedida unidademedida;
	protected Pedidovendamaterial pedidovendamaterial;
	protected Vendaorcamentomaterial vendaorcamentomaterial;
	protected Date prazoentrega;
	protected String observacao;
	protected Loteestoque loteestoque;
	protected Double valorcustomaterial;
	protected Double saldo;
	protected Double multiplicador = 1.0;
	protected Boolean producaosemestoque;
	protected Fornecedor fornecedor;
	protected Money percentualrepresentacao;
	protected Boolean opcional;
	protected Patrimonioitem patrimonioitem;
	protected List<Expedicaoitem> listaExpedicaoitem;
	protected Material materialcoleta;
	protected List<Vendamaterialseparacao> listaVendamaterialseparacao = new ListSet<Vendamaterialseparacao>(Vendamaterialseparacao.class);
	protected String identificadorespecifico;
	protected String identificadorinterno;
	protected Double custooperacional;
	protected Double percentualcomissaoagencia;
	protected Pneu pneu;
	protected Double qtdevolumeswms;
	protected Money valorimposto;
	protected Double ipi;
	protected Money valoripi;
	protected Tipocobrancaipi tipocobrancaipi;
	protected Tipocalculo tipocalculoipi;
	protected Money aliquotareaisipi;
	protected Modalidadebcicms modalidadebcicms;
	protected Modalidadebcicmsst modalidadebcicmsst;
	protected Grupotributacao grupotributacao;
	protected Integer cdproducaoordemitemadicional;
	protected Integer cdproducaoagendaitemadicional;
	protected Double peso;
	protected Integer identificadorintegracao;
	protected Comissionamento comissionamento;
	protected Double pesomedio;
	protected Double percentualdesconto;
	protected Money descontogarantiareforma;
	protected Garantiareformaitem garantiareformaitem;
	protected Double valorMinimo;	
	protected MaterialFaixaMarkup materialFaixaMarkup;
	protected FaixaMarkupNome faixaMarkupNome;
	protected Money valorSeguro;
	protected Money outrasdespesas;
	protected Tipotributacaoicms tipotributacaoicms;
	protected Tipocobrancaicms tipocobrancaicms; 
	protected Money valorbcicms;
	protected Double icms;
	protected Money valoricms;
	protected Double percentualdesoneracaoicms;
	protected Money valordesoneracaoicms;
	protected Boolean abaterdesoneracaoicms;
	
	protected Money valorbcicmsst;
	protected Double icmsst;
	protected Money valoricmsst;
	
	protected Double reducaobcicmsst;
	protected Double margemvaloradicionalicmsst;
	
	protected Money valorbcfcp;
	protected Double fcp;
	protected Money valorfcp;
	protected Money valorbcfcpst;
	protected Double fcpst;
	protected Money valorfcpst;
	
	protected Boolean dadosicmspartilha;
	protected Money valorbcdestinatario;
	protected Money valorbcfcpdestinatario;
	protected Double fcpdestinatario;
	protected Double icmsdestinatario;
	protected Double icmsinterestadual;
	protected Double icmsinterestadualpartilha;
	protected Money valorfcpdestinatario;
	protected Money valoricmsdestinatario;
	protected Money valoricmsremetente;
	protected Double difal;
	protected Money valordifal;
	
	protected Boolean sinalizadoSemReserva;
	
	protected Cfop cfop;
	protected Ncmcapitulo ncmcapitulo;
	protected String ncmcompleto;
	
	private Money valorComissao;
	
	private List<Materialdevolucao> listaMaterialdevolucao = new ListSet<Materialdevolucao>(Materialdevolucao.class);
	
	//TRANSIENTES
	protected String whereInOSVM;
	protected Vendamaterial vendamaterialMaterialmestre;
	protected Vendamaterialmestre vendamaterialmestre;
	protected Money total;
	protected boolean achou = true;
	protected Double qtdedevolvida;
	protected Double qtdejadevolvida;
	protected Double valordevolvido;
	protected Double valorvendamaterial;
	protected Double valordescontocustovenda;
	protected Double percentualdescontocustovenda;
	protected Double margemcustovenda;
	protected Boolean considerarDesconto;
	protected Boolean considerarValeCompra;
	protected Money totalproduto;
	protected Produto produtotrans;
	protected Unidademedida unidademedidaDevolucao;
	protected Unidademedida unidademedidaAntiga;
	protected Double quantidadeTrans;
	protected Double descontoProporcional;
	protected String historicoTroca;
	protected Integer ordemexibicao;
	protected Boolean descriminarnota;
	protected Double qtdeProporcionalproducao;
	protected Boolean isMaterialmestregrade;
	protected Boolean isControlegrade;
	protected Double qtdeExpedida;
	protected Double qtdeExpedicao;
	protected Boolean somentesaidaMaterialproducao;
	protected Boolean isAdicionarExpedicao;
	protected Boolean isProducaoPedidovenda;
	protected Materialdevolucao materialdevolucao;
	protected String fornecedorStr;
	protected Double quantidadeInicial;
	protected Money descontoInicial;
	protected String index;
	protected Boolean bandaalterada;
	protected Boolean servicoalterado;
	protected Boolean precorecalculado;
	protected Double valorvalecomprapropocional;
	protected Double valordescontosemvalecompra;
	protected Boolean atualizarVenda;
	protected Materialtabelapreco materialtabelapreco;
	protected Materialtabelaprecoitem materialtabelaprecoitem;
		
	protected String descricaomaterialordemretirada;
	protected Boolean existematerialsimilar;
	protected Boolean existematerialsimilarColeta;
	protected Material materialAnterior;
	protected String materialcoleta_label;
	protected String calcularmargempor;
	protected Money totalprodutoReport;
	protected Money totalprodutoDescontoReport;
	protected Double quantidadeTotal;
	protected Boolean registraPesomedioMaterialgrupo;
	protected Double pesoLiquidoVendaOuMaterial;
	protected Integer identificadorfabricante;
	protected Boolean considerarMultiplicadorCusto;
	private Money valorFreteCalcCurvaAbc;
	private Money valorDescontoCalcCurvaAbc;
	private Integer indice;
	protected java.sql.Date dtprazoentrega;
	protected String dtprazoentregaStr;
	protected Boolean agendaProducaoGerada;
	protected Money totalprodutoOtr;
	protected Money totalImpostos;
	protected boolean exibirIpiPopup;
	protected boolean exibirIcmsPopup;
	protected boolean exibirDifalPopup;
	protected Money valorBruto;
	protected String nomeKitParaNF;
	protected String codigoBarrasEmbalagem;
	protected Boolean unidadesMedidaDiferentesConferenciaExpedicao;
	protected Boolean unidadeMedidaPrincipalMaterial;
	
	public Vendamaterial() {
	}
	
	public Vendamaterial(Integer cdvendamaterial) {
		this.cdvendamaterial = cdvendamaterial;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_vendamaterial")
	public Integer getCdvendamaterial() {
		return cdvendamaterial;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvenda")
	@DisplayName("Venda")
	public Venda getVenda() {
		return venda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	@DisplayName("Produto")
	public Material getMaterial() {
		return material;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialmestre")
	public Material getMaterialmestre() {
		return materialmestre;
	}
	
	@Required
	@DisplayName("Unidade de medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendamaterial")
	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}
	
	@Transient
	public Vendaorcamentomaterial getVendaorcamentomaterial() {
		return vendaorcamentomaterial;
	}

	public Double getQuantidade() {
		return quantidade;
	}
	
	@DisplayName("Pre�o")
	public Double getPreco() {
		return preco;
	}
	
	@DisplayName("Prazo de entrega")
	public Date getPrazoentrega() {
		return prazoentrega;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	public Money getDesconto() {
		return desconto;
	}
	@DisplayName("Lote")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdloteestoque")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	
	@DisplayName("Patrim�nio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpatrimonioitem")
	public Patrimonioitem getPatrimonioitem() {
		return patrimonioitem;
	}
	
	@OneToMany(mappedBy="vendamaterial")
	public List<Expedicaoitem> getListaExpedicaoitem() {
		return listaExpedicaoitem;
	}
	
	@OneToMany(mappedBy="vendamaterial")
	public List<Vendamaterialseparacao> getListaVendamaterialseparacao() {
		return listaVendamaterialseparacao;
	}

	public Double getMultiplicador() {
		return multiplicador;
	}
	
	public Boolean getProducaosemestoque() {
		return producaosemestoque;
	}
	
	@DisplayName("Material de coleta")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialcoleta")
	public Material getMaterialcoleta() {
		return materialcoleta;
	}
	
	
	public String getIdentificadorespecifico() {
		return identificadorespecifico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpneu")
	public Pneu getPneu() {
		return pneu;
	}
	
	public Money getValorComissao() {
		return valorComissao;
	}
	
	public void setValorComissao(Money valorComissao) {
		this.valorComissao = valorComissao;
	}
	
	public void addValorComissao(Money valorComissao) {
		if (this.valorComissao == null) {
			this.valorComissao = new Money();
		}
		
		this.valorComissao = this.valorComissao.add(valorComissao);
	}
	
	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}

	public void setIdentificadorespecifico(String identificadorespecifico) {
		this.identificadorespecifico = identificadorespecifico;
	}
	
	public String getIdentificadorinterno() {
		return identificadorinterno;
	}
	public void setIdentificadorinterno(String identificadorinterno) {
		this.identificadorinterno = identificadorinterno;
	}
	
	public void setMaterialcoleta(Material materialcoleta) {
		this.materialcoleta = materialcoleta;
	}
	
	public void setPatrimonioitem(Patrimonioitem patrimonioitem) {
		this.patrimonioitem = patrimonioitem;
	}
	
	public void setProducaosemestoque(Boolean producaosemestoque) {
		this.producaosemestoque = producaosemestoque;
	}
	
	public void setMultiplicador(Double multiplicador) {
		this.multiplicador = multiplicador;
	}
	
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}

	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
	public void setCdvendamaterial(Integer cdvendamaterial) {
		this.cdvendamaterial = cdvendamaterial;
	}
	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}
	public void setVendaorcamentomaterial(Vendaorcamentomaterial vendaorcamentomaterial) {
		this.vendaorcamentomaterial = vendaorcamentomaterial;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialmestre(Material materialmestre) {
		this.materialmestre = materialmestre;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public void setPrazoentrega(Date prazoentrega) {
		this.prazoentrega = prazoentrega;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setListaExpedicaoitem(List<Expedicaoitem> listaExpedicaoitem) {
		this.listaExpedicaoitem = listaExpedicaoitem;
	}
	
	public void setListaVendamaterialseparacao(List<Vendamaterialseparacao> listaVendamaterialseparacao) {
		this.listaVendamaterialseparacao = listaVendamaterialseparacao;
	}
	
	// TRANSIENTES
	@Transient
	public Money getTotal() {
		return total;
	}
	public void setTotal(Money total) {
		this.total = total;
	}
	
	@Transient
	public Double getQuantidadeInicial() {
		return quantidadeInicial;
	}
	public void setQuantidadeInicial(Double quantidadeInicial) {
		this.quantidadeInicial = quantidadeInicial;
	}
	@Transient
	public Money getDescontoInicial() {
		return descontoInicial;
	}
	public void setDescontoInicial(Money descontoInicial) {
		this.descontoInicial = descontoInicial;
	}
	
	public Double getFatorconversao() {
		return fatorconversao;
	}
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
	public void setFatorconversao(Double fatorconversao) {
		this.fatorconversao = fatorconversao;
	}
	
	@Transient
	public boolean isAchou() {
		return achou;
	}
	
	public void setAchou(boolean achou) {
		this.achou = achou;
	}
	
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	@Transient
	public Double getQtdedevolvida() {
		return qtdedevolvida;
	}

	public void setQtdedevolvida(Double qtdedevolvida) {
		this.qtdedevolvida = qtdedevolvida;
	}
	@Transient
	public Double getQtdejadevolvida() {
		return qtdejadevolvida;
	}
	public void setQtdejadevolvida(Double qtdejadevolvida) {
		this.qtdejadevolvida = qtdejadevolvida;
	}

	public Double getComprimento() {
		return comprimento;
	}
	public Double getComprimentooriginal() {
		return comprimentooriginal;
	}
	public Double getLargura() {
		return largura;
	}
	public Double getAltura() {
		return altura;
	}

	public void setComprimentooriginal(Double comprimentooriginal) {
		this.comprimentooriginal = comprimentooriginal;
	}
	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}
	public void setLargura(Double largura) {
		this.largura = largura;
	}
	public void setAltura(Double altura) {
		this.altura = altura;
	}

	@Transient
	public String getDescricaomaterialordemretirada() {
		StringBuilder s = new StringBuilder();
		StringBuilder alturalarguracomprimento = new StringBuilder();
		if(this.getMaterial() != null && this.getMaterial().getNome() != null){
			s.append(this.getMaterial().getNome());
		}
		if(this.getAltura() != null || this.getLargura() != null || this.getComprimentooriginal() != null){
			if(this.getLargura() != null && this.getLargura() > 0){
				alturalarguracomprimento.append(new DecimalFormat("#.#######").format(this.getLargura()));
			}
			if(this.getAltura() != null && this.getAltura() > 0){
				if(!"".equals(alturalarguracomprimento.toString()))
					alturalarguracomprimento.append("x");
				alturalarguracomprimento.append(new DecimalFormat("#.#######").format(this.getAltura()));
			}
			if(this.getComprimentooriginal() != null && this.getComprimentooriginal() > 0){
				if(!"".equals(alturalarguracomprimento.toString()))
					alturalarguracomprimento.append("x");
				alturalarguracomprimento.append(new DecimalFormat("#.#######").format(this.getComprimentooriginal()));
			}
			s.append(" ").append(alturalarguracomprimento.toString());
		}
		if(this.getObservacao() != null && !"".equals(this.getObservacao())){
			s.append("\nObs: ").append(this.getObservacao());
		}
		
		return s.toString();
	}
	
	@Transient
	public Double getValordescontocustovendaitem() {
		Double valordesconto = 0d;
		if(this.getValorvendamaterial() != null){
			if(this.getPreco() < this.getValorvendamaterial()){
				valordesconto = (this.getQuantidade()*this.getValorvendamaterial()) - this.getTotalproduto().getValue().doubleValue(); 
			}
		}else if(this.getMaterial().getValorvenda() != null){
			Double valorvenda = this.getMaterial().getValorvenda();
			if(this.getFatorconversao() != null && this.getQtdereferencia() != null){
				valorvenda = valorvenda / getFatorconversaoQtdereferencia();
			}
			if(this.getPreco() < valorvenda){
				valordesconto = (this.getQuantidade()*valorvenda) - this.getTotalproduto().getValue().doubleValue(); 
			}
		}
		return valordesconto + getValordescontocustovenda();
	}
	
	@Transient
	public Double getValordescontocustovenda() {
		Double valordesconto = 0.0;
		if(this.getMaterial() != null && this.getPreco() != null){
//			if(this.getValorvendamaterial() != null){
//				if(this.getPreco() < this.getValorvendamaterial()){
//					valordesconto = (this.getQuantidade()*this.getValorvendamaterial()) - this.getTotalproduto().getValue().doubleValue(); 
//				}
//			}else if(this.getMaterial().getValorvenda() != null){
//				Double valorvenda = this.getMaterial().getValorvenda();
//				if(this.getFatorconversao() != null && this.getQtdereferencia() != null){
//					valorvenda = valorvenda / getFatorconversaoQtdereferencia();
//				}
//				if(this.getPreco() < valorvenda){
//					valordesconto = (this.getQuantidade()*valorvenda) - this.getTotalproduto().getValue().doubleValue(); 
//				}
//			}
			if(this.getDesconto() != null){
				valordesconto += this.getDesconto().getValue().doubleValue();
			}
			if(this.getDescontoProporcional() != null){
				valordesconto += this.getDescontoProporcional();
			}
		}		
		return valordesconto;
	}

	@Transient
	public Double getValordescontocustovendaReport() {
		if(getConsiderarDesconto() != null && getConsiderarDesconto()){
			return getValordescontocustovenda();
		}
		return 0d;
	}

	@Transient
	public Double getPercentualdescontocustovenda() {
		Double valordesconto = 0.0;
		if(this.getMaterial() != null && this.getPreco() != null && this.getQuantidade() != null){
			if(this.getValorvendamaterial() != null){
				if(this.getPreco() < this.getValorvendamaterial()){
					valordesconto = (this.getQuantidade()*this.getValorvendamaterial()) - this.getTotalproduto().getValue().doubleValue(); 
				}
			}else if(this.getMaterial().getValorvenda() != null){
				if(this.getPreco() < this.getMaterial().getValorvenda()){
					valordesconto = (this.getQuantidade()*this.getMaterial().getValorvenda()) - this.getTotalproduto().getValue().doubleValue(); 
				}
			}
			if(this.getDesconto() != null){
				valordesconto += this.getDesconto().getValue().doubleValue();
			}
			if(this.getDescontoProporcional() != null){
				valordesconto += this.getDescontoProporcional();
			}
			
			if(valordesconto > 0 && this.getQuantidade() > 0){
				valordesconto = 100 - ((this.getTotalproduto().getValue().doubleValue()-valordesconto)*100)/(this.getQuantidade()*this.getPreco());
			}
		}		
		return valordesconto;
	}
	@Transient
	public Double getPercentualdescontocustovendaReport() {
		if(getConsiderarDesconto() != null && getConsiderarDesconto()){
			return getPercentualdescontocustovenda();
		}
		return 0d;
	}
	
	@Transient
	public Double getMargemcustovenda() {
		Double margem = 100.0;
		Double multiplicador = 1d;
		Double desconto = this.getDesconto() != null ? this.getDesconto().getValue().doubleValue() : 0d;
		Double valecompra = this.getValorvalecomprapropocional() != null ? this.getValorvalecomprapropocional().doubleValue() : 0d;
		if(getDescontoProporcional() != null){
			desconto += getDescontoProporcional();
		}
		
		if(this.getValorvalecomprapropocional() != null && this.getConsiderarValeCompra() && 
				(this.getConsiderarDesconto() == null || !this.getConsiderarDesconto())){
			desconto -= this.getValorvalecomprapropocional();
		}
		
		if(this.getMultiplicador() != null && this.getMultiplicador() > 0){
			multiplicador = this.getMultiplicador();
		}
		if(this.getMaterial() != null && this.getValorcustomaterial() != null && this.getValorcustomaterial() > 0 && this.getPreco() != null){
			if(getConsiderarDesconto() != null && getConsiderarDesconto()){
				margem = ((this.getPreco()*multiplicador-(desconto/this.getQuantidade()))-this.getValorcustoRelatorio())*100;
			}else if(this.getConsiderarValeCompra() != null && this.getConsiderarValeCompra()){
				margem = ((this.getPreco()*multiplicador-(valecompra/this.getQuantidade()))-this.getValorcustoRelatorio())*100;
			}else {
				margem = ((this.getPreco()*multiplicador)-this.getValorcustoRelatorio())*100;
			}
			if("Custo".equalsIgnoreCase(getCalcularmargempor())){
				margem = margem/this.getValorcustoRelatorio();
			}else {
				margem = margem/this.getPreco();
			}
		}else if(this.getMaterial() != null && this.getMaterial().getValorcusto() != null && this.getMaterial().getValorcusto() > 0 && this.getPreco() != null){
			Double valorcusto = this.material.getValorcusto();
			if(valorcusto != null && this.getFatorconversao() != null && this.getQtdereferencia() != null){
				valorcusto = valorcusto / getFatorconversaoQtdereferencia();
			}
			valorcusto = valorcusto * multiplicador;
			if(getConsiderarDesconto() != null && getConsiderarDesconto()){
				margem = ((this.getPreco()*multiplicador-(desconto/this.getQuantidade()))-(valorcusto))*100;
			}else if(this.getConsiderarValeCompra() != null && this.getConsiderarValeCompra()){
				margem = ((this.getPreco()*multiplicador-(valecompra/this.getQuantidade()))-this.getValorcustoRelatorio())*100;
			}else {
				margem = ((this.getPreco()*multiplicador)-(valorcusto))*100;
			}
			if("Custo".equalsIgnoreCase(getCalcularmargempor())){
				margem = margem/(valorcusto);
//			}else if(this.getMaterial().getValorvenda() != null) {
//				Double valorvenda = this.material.getValorvenda();
//				if(valorvenda != null && this.getFatorconversao() != null && this.getQtdereferencia() != null){
//					valorvenda = valorvenda / getFatorconversaoQtdereferencia();
//				}
//				margem = margem/(valorvenda*multiplicador);
//			}
			} else {
				margem = margem/(this.getPreco());
			}
		}
		return margem;
	}

	public void setPercentualdescontocustovenda(Double percentualdescontocustovenda) {
		this.percentualdescontocustovenda = percentualdescontocustovenda;
	}

	public void setMargemcustovenda(Double margemcustovenda) {
		this.margemcustovenda = margemcustovenda;
	}

	@Transient
	public Money getTotalproduto() {
		return getTotalprodutoItem(false);
	}
	
	public void setTotalproduto(Money totalproduto) {
		this.totalproduto = totalproduto;
	}
	
	@Transient
	public Money getTotalprodutoItem(boolean roundItem) {
		Double total = 0.0;
		Double multiplicador = 1d;
		if(this.getMultiplicador() != null && this.getMultiplicador() > 0){
			multiplicador = this.getMultiplicador();
		}
		if(this.getPreco() != null){
			total  = this.getPreco() * (this.getQuantidade() != null ? this.getQuantidade() : 1.0) * multiplicador;
		}
		return new Money(SinedUtil.round(BigDecimal.valueOf(roundItem ? SinedUtil.round(total, 10) : total), 2).doubleValue());
	}
	
	@Transient
	public Boolean getExistematerialsimilar() {
		return existematerialsimilar;
	}
	@Transient
	public Boolean getExistematerialsimilarColeta() {
		return existematerialsimilarColeta;
	}

	public void setExistematerialsimilarColeta(Boolean existematerialsimilarColeta) {
		this.existematerialsimilarColeta = existematerialsimilarColeta;
	}
	public void setExistematerialsimilar(Boolean existematerialsimilar) {
		this.existematerialsimilar = existematerialsimilar;
	}

	
	public Double getValorcustomaterial() {
		return valorcustomaterial;
	}
	public void setValorcustomaterial(Double valorcustomaterial) {
		this.valorcustomaterial = valorcustomaterial;
	}

	public Double getValorvendamaterial() {
		return valorvendamaterial;
	}
	public void setValorvendamaterial(Double valorvendamaterial) {
		this.valorvendamaterial = valorvendamaterial;
	}
	
	@Transient
	public Double getFatorconversaoQtdereferencia(){
		Double valor = 1.0;
		if(this.fatorconversao != null)
			valor = (this.fatorconversao / (this.qtdereferencia != null ? this.qtdereferencia : 1.0));
		if(valor == 0) valor = 1d;
		return valor;
	}
	
	@Transient
	public Double getValorcustoRelatorio(){
		Double valorretorno = 0.0;
		Double valorcusto = 0.0;
		Double multiplicador = 1d;
		
		if (this.getMultiplicador() != null && this.getMultiplicador() > 0) {
			multiplicador = this.getMultiplicador();
		}
		
		if (!Boolean.TRUE.equals(this.getConsiderarMultiplicadorCusto()) || (this.getMaterial() == null || Boolean.TRUE.equals(this.getMaterial().getMetrocubicovalorvenda()))) {
			multiplicador = 1d;
		}
		
		if (this.getValorcustomaterial() != null) {
			valorcusto = this.getValorcustomaterial() * multiplicador; 
			valorretorno = valorcusto;
		} else if(this.material != null && this.material.getValorcusto() != null) {
			valorcusto = this.material.getValorcusto() * multiplicador;
			if (valorcusto != null && this.getFatorconversao() != null && this.getQtdereferencia() != null) {
				valorcusto = valorcusto / getFatorconversaoQtdereferencia();
			}
			valorretorno = valorcusto;
		}
		
//		if(valorcusto != null && this.produtotrans != null){
//			boolean alturadiferente = false;
//			boolean larguradiferente = false;
//			boolean comprimentodiferente = false;
//			
//			/*if((this.produtotrans.getAltura() != null && this.altura == null) || (this.produtotrans.getAltura() == null && this.altura != null)){
//				alturadiferente = true;
//			} else */if(this.produtotrans.getAltura() != null && this.altura != null && !this.produtotrans.getAltura().equals(this.altura*1000)){
//				alturadiferente = true;
//			}
//			
//			/*if((this.produtotrans.getLargura() != null && this.largura == null) || (this.produtotrans.getLargura() == null && this.largura != null)){
//				larguradiferente = true;
//			} else */if(this.produtotrans.getLargura() != null && this.largura != null && !this.produtotrans.getLargura().equals(this.largura*1000)){
//				larguradiferente = true;
//			}
//			
//			/*if((this.produtotrans.getComprimento() != null && this.comprimento == null) || (this.produtotrans.getComprimento() == null && this.comprimento != null)){
//				comprimentodiferente = true;
//			} else */if(this.produtotrans.getComprimento() != null && this.comprimento != null && !this.produtotrans.getComprimento().equals(this.comprimento*1000)){
//				comprimentodiferente = true;
//			}
//			
//			if(alturadiferente || larguradiferente || comprimentodiferente){
//				if(valorcusto != null && this.getFatorconversao() != null && this.getQtdereferencia() != null){
//					valorcusto = valorcusto * getFatorconversaoQtdereferencia();
//				}
//				valorretorno = (this.altura != null && this.altura != 0 ? this.altura : 1.0) *
//						   (this.largura != null && this.largura != 0 ? this.largura : 1.0) *
//						   (this.comprimento != null && this.comprimento != 0 ? this.comprimento : 1.0) * 
//						   (valorcusto != null ? valorcusto : 0.0);
//			}
//		}

		return valorretorno;
	}
		
	@Transient
	public Double getValorvendaRelatorio(){
		Double valorretorno = 0.0;
		Double valorvenda = 0.0;
		Double multiplicador = 1d;
		
		if(this.getMultiplicador() != null && this.getMultiplicador() > 0){
			multiplicador = this.getMultiplicador();
		}
		
		if (this.getMaterial() != null && Boolean.TRUE.equals(this.getMaterial().getMetrocubicovalorvenda())) {
			multiplicador = 1d;
		}
		
		if (this.getValorvendamaterial() != null) {
			valorvenda = this.getValorvendamaterial() * multiplicador; 
			valorretorno = valorvenda;
		} else if(this.material != null && this.material.getValorvenda() != null) {
			valorvenda = this.material.getValorvenda() * multiplicador;
			valorretorno = valorvenda;
		}
		
//		if(valorvenda != null && this.produtotrans != null){
//			boolean alturadiferente = false;
//			boolean larguradiferente = false;
//			boolean comprimentodiferente = false;
//			
//			/*if((this.produtotrans.getAltura() != null && this.altura == null) || (this.produtotrans.getAltura() == null && this.altura != null)){
//				alturadiferente = true;
//			} else */if(this.produtotrans.getAltura() != null && this.altura != null && !this.produtotrans.getAltura().equals(this.altura*1000)){
//				alturadiferente = true;
//			}
//			
//			/*if((this.produtotrans.getLargura() != null && this.largura == null) || (this.produtotrans.getLargura() == null && this.largura != null)){
//				larguradiferente = true;
//			} else */if(this.produtotrans.getLargura() != null && this.largura != null && !this.produtotrans.getLargura().equals(this.largura*1000)){
//				larguradiferente = true;
//			}
//			
//			/*if((this.produtotrans.getComprimento() != null && this.comprimento == null) || (this.produtotrans.getComprimento() == null && this.comprimento != null)){
//				comprimentodiferente = true;
//			} else */if(this.produtotrans.getComprimento() != null && this.comprimento != null && !this.produtotrans.getComprimento().equals(this.comprimento*1000)){
//				comprimentodiferente = true;
//			}
//			
//			if(alturadiferente || larguradiferente || comprimentodiferente){
//				//obs: o valor de venda � salvo convertido
//				if(this.material.unidademedida != null && this.unidademedida != null && 
//						!this.material.unidademedida.equals(this.unidademedida) && 
//						this.fatorconversao != null && this.qtdereferencia != null){
//					valorvenda = valorvenda * getFatorconversaoQtdereferencia(); 
//				}
//				valorretorno = (this.altura != null && this.altura != 0 ? this.altura : 1.0) *
//						   (this.largura != null && this.largura != 0 ? this.largura : 1.0) *
//						   (this.comprimento != null && this.comprimento != 0 ? this.comprimento : 1.0) * 
//						   (valorvenda != null ? valorvenda : 0.0);
//			}
//		}
		
		return valorretorno;
	}
	@Transient
	public Double getPrecoRelatorio(){
		Double precoretorno = 0.0;
		Double multiplicador = 1d;
		
		if(this.getMultiplicador() != null && this.getMultiplicador() > 0){
			multiplicador = this.getMultiplicador();
		}
		
		if (this.getMaterial() != null && Boolean.TRUE.equals(this.getMaterial().getMetrocubicovalorvenda())) {
			multiplicador = 1d;
		}
		
		if(this.getPreco() != null){
			precoretorno = this.getPreco() * multiplicador;
		}
		
		return precoretorno;
	}
	
	@Transient
	public Double getQuantidadeRelatorio() {
		Double qtd = 1d;
		Double multiplicador = 1d;
		
		if(this.getMultiplicador() != null && this.getMultiplicador() > 0){
			multiplicador = this.getMultiplicador();
		}
		
		//desconsidera o multiplicador caso o metro cubico n�o esteja marcado no cadastro do material
		if (this.getMaterial() != null && !Boolean.TRUE.equals(this.getMaterial().getMetrocubicovalorvenda())) {
			multiplicador = 1d;
		}
		
		if(this.getQuantidade() != null){
			qtd = this.getQuantidade() *  multiplicador;
		}
		
		return qtd;
	}

	@Transient
	public Produto getProdutotrans() {
		return produtotrans;
	}
	public void setProdutotrans(Produto produtotrans) {
		this.produtotrans = produtotrans;
	}
	
	@Transient
	public Money getValorFreteCalcCurvaAbc() {
		return valorFreteCalcCurvaAbc;
	}
	
	public void setValorFreteCalcCurvaAbc(Money valorFreteCalcCurvaAbc) {
		this.valorFreteCalcCurvaAbc = valorFreteCalcCurvaAbc;
	}
	
	@Transient
	public Money getValorDescontoCalcCurvaAbc() {
		return valorDescontoCalcCurvaAbc;
	}
	public void setValorDescontoCalcCurvaAbc(Money valorDescontoCalcCurvaAbc) {
		this.valorDescontoCalcCurvaAbc = valorDescontoCalcCurvaAbc;
	}
	
	@Transient
	public Double getQtdeMetrocunico(Double qtde){
		Double qtderelatorio = 1.0;
		if(qtde != null){
			qtderelatorio = qtde * 
					(this.getAltura() != null ? this.getAltura() : 1.0) * 
					(this.getLargura() != null ? this.getLargura() : 1.0) *
					(this.getComprimento() != null ? this.getComprimento() : 1.0);
		}
		return qtderelatorio;
	}

	@Transient
	@DisplayName("Unidade de Medida")
	public Unidademedida getUnidademedidaDevolucao() {
		return unidademedidaDevolucao;
	}
	@Transient
	public Unidademedida getUnidademedidaAntiga() {
		return unidademedidaAntiga;
	}
	@Transient
	@DisplayName("Quantidade")
	public Double getQuantidadeTrans() {
		return quantidadeTrans;
	}

	public void setUnidademedidaDevolucao(Unidademedida unidademedidaDevolucao) {
		this.unidademedidaDevolucao = unidademedidaDevolucao;
	}
	public void setQuantidadeTrans(Double quantidadeTrans) {
		this.quantidadeTrans = quantidadeTrans;
	}
	public void setUnidademedidaAntiga(Unidademedida unidademedidaAntiga) {
		this.unidademedidaAntiga = unidademedidaAntiga;
	}

	@Transient
	public Double getDescontoProporcional() {
		return descontoProporcional;
	}
	public void setDescontoProporcional(Double descontoProporcional) {
		this.descontoProporcional = descontoProporcional;
	}
	
	@DisplayName("Fornecedor")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("Fornecedor")
	public Money getPercentualrepresentacao() {
		return percentualrepresentacao;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setPercentualrepresentacao(Money percentualrepresentacao) {
		this.percentualrepresentacao = percentualrepresentacao;
	}

	@Transient
	public String getFornecedorStr() {
		if (getFornecedor()!=null && getFornecedor().getNome()!=null){
			return getFornecedor().getNome();
		}else {
			return fornecedorStr;
		}
	}
	public void setFornecedorStr(String fornecedorStr) {
		this.fornecedorStr = fornecedorStr;
	}

	@Transient
	public String getHistoricoTroca() {
		return historicoTroca;
	}
	public void setHistoricoTroca(String historicoTroca) {
		this.historicoTroca = historicoTroca;
	}

	public Boolean getOpcional() {
		return opcional;
	}
	public void setOpcional(Boolean opcional) {
		this.opcional = opcional;
	}
	
	@Transient
	public Integer getOrdemexibicao() {
		return ordemexibicao;
	}
	@Transient
	public Boolean getDescriminarnota() {
		return descriminarnota;
	}
	@Transient
	public Double getQtdeProporcionalproducao() {
		return qtdeProporcionalproducao;
	}

	public void setOrdemexibicao(Integer ordemexibicao) {
		this.ordemexibicao = ordemexibicao;
	}
	public void setDescriminarnota(Boolean descriminarnota) {
		this.descriminarnota = descriminarnota;
	}
	public void setQtdeProporcionalproducao(Double qtdeProporcionalproducao) {
		this.qtdeProporcionalproducao = qtdeProporcionalproducao;
	}

	@Transient
	public Double getTotalItem(Money valorDescontoVendaProporcional, Money valorFreteVendaProporcional, Money valorValecompraProporcional) {
		Double total = 0d;
		Double multiplicador = 1d;
		if(this.getMultiplicador() != null && this.getMultiplicador() > 0){
			multiplicador = this.getMultiplicador();
		}
		if(this.getPreco() != null){
			total = this.getPreco() * (this.getQuantidade() != null ? this.getQuantidade() : 1d) * multiplicador;
		}
		
		if(DoubleUtils.isMaiorQueZero(this.getQuantidade())){
			if(this.getDesconto() != null){
				total -= this.getDesconto().getValue().doubleValue();
			}
			if(valorDescontoVendaProporcional != null){
				total -= valorDescontoVendaProporcional.getValue().doubleValue();
			}
			if(valorValecompraProporcional != null){
				total -= valorValecompraProporcional.getValue().doubleValue();
			}
			if(valorFreteVendaProporcional != null){
				total += valorFreteVendaProporcional.getValue().doubleValue();
			}
		}
//		return SinedUtil.round(BigDecimal.valueOf(total), 2).doubleValue();
		return total;
	}

	@Transient
	public Material getMaterialAnterior() {
		return materialAnterior;
	}

	public void setMaterialAnterior(Material materialAnterior) {
		this.materialAnterior = materialAnterior;
	}
	@Transient
	public Boolean getIsMaterialmestregrade() {
		return isMaterialmestregrade;
	}

	public void setIsMaterialmestregrade(Boolean isMaterialmestregrade) {
		this.isMaterialmestregrade = isMaterialmestregrade;
	}
	@Transient
	public Boolean getIsControlegrade() {
		return isControlegrade;
	}
	public void setIsControlegrade(Boolean isControlegrade) {
		this.isControlegrade = isControlegrade;
	}

	@Transient
	public Double getQtdeExpedida() {
		return qtdeExpedida;
	}
	
	@Transient
	@DisplayName("Qtde. Expedi��o")
	public Double getQtdeExpedicao() {
		return qtdeExpedicao;
	}

	public void setQtdeExpedida(Double qtdeExpedida) {
		this.qtdeExpedida = qtdeExpedida;
	}

	public void setQtdeExpedicao(Double qtdeExpedicao) {
		this.qtdeExpedicao = qtdeExpedicao;
	}

	@Transient
	public Boolean getSomentesaidaMaterialproducao() {
		return somentesaidaMaterialproducao;
	}

	public void setSomentesaidaMaterialproducao(Boolean somentesaidaMaterialproducao) {
		this.somentesaidaMaterialproducao = somentesaidaMaterialproducao;
	}

	@Transient
	public Boolean getIsAdicionarExpedicao() {
		return isAdicionarExpedicao;
	}

	public void setIsAdicionarExpedicao(Boolean isAdicionarExpedicao) {
		this.isAdicionarExpedicao = isAdicionarExpedicao;
	}
	
	@Transient
	public String getMaterialcoleta_label() {
		if(materialcoleta != null && materialcoleta.getNome() != null && !materialcoleta.getNome().equals("")){
			materialcoleta_label = materialcoleta.getNome();
		}
		return materialcoleta_label;
	}
	
	public void setMaterialcoleta_label(String materialcoletaLabel) {
		materialcoleta_label = materialcoletaLabel;
	}

	@Transient
	public Vendamaterial getVendamaterialMaterialmestre() {
		return vendamaterialMaterialmestre;
	}
	public void setVendamaterialMaterialmestre(Vendamaterial vendamaterialMaterialmestre) {
		this.vendamaterialMaterialmestre = vendamaterialMaterialmestre;
	}
	@Transient
	public Vendamaterialmestre getVendamaterialmestre() {
		return vendamaterialmestre;
	}

	public void setVendamaterialmestre(Vendamaterialmestre vendamaterialmestre) {
		this.vendamaterialmestre = vendamaterialmestre;
	}

	@Transient
	public Boolean getConsiderarDesconto() {
		return considerarDesconto;
	}
	public void setConsiderarDesconto(Boolean considerarDesconto) {
		this.considerarDesconto = considerarDesconto;
	}
	
	@Transient
	public Boolean getIsProducaoPedidovenda() {
		return isProducaoPedidovenda;
	}

	public void setIsProducaoPedidovenda(Boolean isProducaoPedidovenda) {
		this.isProducaoPedidovenda = isProducaoPedidovenda;
	}

	@Transient
	public String getCalcularmargempor() {
		return calcularmargempor != null ? calcularmargempor : "Custo";
	}
	public void setCalcularmargempor(String calcularmargempor) {
		this.calcularmargempor = calcularmargempor;
	}
	
	@Transient
	public Money getTotalprodutoReport() {
		return totalprodutoReport;
	}
	public void setTotalprodutoReport(Money totalprodutoReport) {
		this.totalprodutoReport = totalprodutoReport;
	}
	@Transient
	public Money getTotalprodutoDescontoReport() {
		return totalprodutoDescontoReport;
	}
	public void setTotalprodutoDescontoReport(Money totalprodutoDescontoReport) {
		this.totalprodutoDescontoReport = totalprodutoDescontoReport;
	}

	@Transient
	public Materialdevolucao getMaterialdevolucao() {
		return materialdevolucao;
	}
	public void setMaterialdevolucao(Materialdevolucao materialdevolucao) {
		this.materialdevolucao = materialdevolucao;
	}
	
	@Transient
	public Double getValordevolvido() {
		return valordevolvido;
	}
	
	public void setValordevolvido(Double valordevolvido) {
		this.valordevolvido = valordevolvido;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	@DisplayName("Custo Operacional")
	public Double getCustooperacional() {
		return custooperacional;
	}

	public void setCustooperacional(Double custooperacional) {
		this.custooperacional = custooperacional;
	}

	@DisplayName("Percentual Comiss�o Ag�ncia")
	public Double getPercentualcomissaoagencia() {
		return percentualcomissaoagencia;
	}

	public void setPercentualcomissaoagencia(Double percentualcomissaoagencia) {
		this.percentualcomissaoagencia = percentualcomissaoagencia;
	}

	@Transient
	public Double getQtdevolumeswms() {
		return qtdevolumeswms;
	}

	public void setQtdevolumeswms(Double qtdevolumeswms) {
		this.qtdevolumeswms = qtdevolumeswms;
	}

	public Money getValorimposto() {
		return valorimposto;
	}

	public void setValorimposto(Money valorimposto) {
		this.valorimposto = valorimposto;
	}

	@Transient
	public Double getQuantidadeTotal() {
		return quantidadeTotal;
	}
	public void setQuantidadeTotal(Double quantidadeTotal) {
		this.quantidadeTotal = quantidadeTotal;
	}
	
	public Integer getCdproducaoordemitemadicional() {
		return cdproducaoordemitemadicional;
	}
	
	public void setCdproducaoordemitemadicional(
			Integer cdproducaoordemitemadicional) {
		this.cdproducaoordemitemadicional = cdproducaoordemitemadicional;
	}

	@DisplayName("Valor IPI")
	public Money getValoripi() {
		return valoripi;
	}
	public Double getIpi() {
		return ipi;
	}
	public Tipocobrancaipi getTipocobrancaipi() {
		return tipocobrancaipi;
	}
	public Tipocalculo getTipocalculoipi() {
		return tipocalculoipi;
	}
	public Money getAliquotareaisipi() {
		return aliquotareaisipi;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgrupotributacao")
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}
	@DisplayName("Peso l�quido")
	public Double getPeso() {
		return peso;
	}
	@DisplayName("Peso m�dio")
	public Double getPesomedio() {
		return pesomedio;
	}
	@DisplayName("(%) Desconto")
	public Double getPercentualdesconto() {
		return percentualdesconto;
	}
	public Integer getIdentificadorintegracao() {
		return identificadorintegracao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcomissionamento")
	public Comissionamento getComissionamento() {
		return comissionamento;
	}

	public void setComissionamento(Comissionamento comissionamento) {
		this.comissionamento = comissionamento;
	}

	public void setIdentificadorintegracao(Integer identificadorintegracao) {
		this.identificadorintegracao = identificadorintegracao;
	}
	public void setValoripi(Money valoripi) {
		this.valoripi = valoripi;
	}
	public void setIpi(Double ipi) {
		this.ipi = ipi;
	}
	public void setTipocobrancaipi(Tipocobrancaipi tipocobrancaipi) {
		this.tipocobrancaipi = tipocobrancaipi;
	}
	public void setTipocalculoipi(Tipocalculo tipocalculoipi) {
		this.tipocalculoipi = tipocalculoipi;
	}
	public void setAliquotareaisipi(Money aliquotareaisipi) {
		this.aliquotareaisipi = aliquotareaisipi;
	}
	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	public void setPesomedio(Double pesomedio) {
		this.pesomedio = pesomedio;
	}
	public void setPercentualdesconto(Double percentualdesconto) {
		this.percentualdesconto = percentualdesconto;
	}

	@Transient
	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}
	
	@Transient
	public Double getPesoVendaOuMaterial(){
		return getPeso() != null ? getPeso() : (getMaterial() != null ? getMaterial().getPeso() : null);
	}

	@Transient
	public Boolean getBandaalterada() {
		return bandaalterada;
	}
	
	@Transient
	public Boolean getServicoalterado() {
		return servicoalterado;
	}
	
	public void setServicoalterado(Boolean servicoalterado) {
		this.servicoalterado = servicoalterado;
	}

	public void setBandaalterada(Boolean bandaalterada) {
		this.bandaalterada = bandaalterada;
	}

	@Transient
	public Boolean getPrecorecalculado() {
		return precorecalculado;
	}

	public void setPrecorecalculado(Boolean precorecalculado) {
		this.precorecalculado = precorecalculado;
	}
	@Transient
	public Double getValorvalecomprapropocional() {
		return valorvalecomprapropocional;
	}
	public void setValorvalecomprapropocional(Double valorvalecomprapropocional) {
		this.valorvalecomprapropocional = valorvalecomprapropocional;
	}
	@Transient
	public Double getValordescontosemvalecompra() {
		if(this.getDesconto() != null)
			return (valordescontosemvalecompra != null ? valordescontosemvalecompra : 0d) + this.getDesconto().getValue().doubleValue();
		else
			return valordescontosemvalecompra;
	}
	public void setValordescontosemvalecompra(Double valordescontosemvalecompra) {
		this.valordescontosemvalecompra = valordescontosemvalecompra;
	}
	
	@Transient
	public Boolean getConsiderarValeCompra() {
		return considerarValeCompra;
	}

	public void setConsiderarValeCompra(Boolean considerarValeCompra) {
		this.considerarValeCompra = considerarValeCompra;
	}

	@Transient
	public Boolean getAtualizarVenda() {
		return atualizarVenda;
	}

	public void setAtualizarVenda(Boolean atualizarVenda) {
		this.atualizarVenda = atualizarVenda;
	}
	
	@Transient
	public Boolean getRegistraPesomedioMaterialgrupo() {
		return registraPesomedioMaterialgrupo;
	}
	public void setRegistraPesomedioMaterialgrupo(
			Boolean registraPesomedioMaterialgrupo) {
		this.registraPesomedioMaterialgrupo = registraPesomedioMaterialgrupo;
	}
	
	@Transient
	public Double getPesoLiquidoVendaOuMaterial() {
		pesoLiquidoVendaOuMaterial = this.getPesoVendaOuMaterial();
		return SinedUtil.roundByParametro(pesoLiquidoVendaOuMaterial , 2);
	}
	public void setPesoLiquidoVendaOuMaterial(Double pesoLiquidoVendaOuMaterial) {
		this.pesoLiquidoVendaOuMaterial = pesoLiquidoVendaOuMaterial;
	}
	
	@Transient
	public String getWhereInOSVM() {
		return whereInOSVM;
	}

	public void setWhereInOSVM(String whereInOSVM) {
		this.whereInOSVM = whereInOSVM;
	}

	@Transient
	public Materialtabelapreco getMaterialtabelapreco() {
		return materialtabelapreco;
	}
	@Transient
	public Materialtabelaprecoitem getMaterialtabelaprecoitem() {
		return materialtabelaprecoitem;
	}

	public void setMaterialtabelapreco(Materialtabelapreco materialtabelapreco) {
		this.materialtabelapreco = materialtabelapreco;
	}
	public void setMaterialtabelaprecoitem(Materialtabelaprecoitem materialtabelaprecoitem) {
		this.materialtabelaprecoitem = materialtabelaprecoitem;
	}

	public Money getDescontogarantiareforma() {
		return descontogarantiareforma;
	}
	public void setDescontogarantiareforma(Money descontogarantiareforma) {
		this.descontogarantiareforma = descontogarantiareforma;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgarantiareformaitem")
	public Garantiareformaitem getGarantiareformaitem() {
		return garantiareformaitem;
	}
	public void setGarantiareformaitem(Garantiareformaitem garantiareformaitem) {
		this.garantiareformaitem = garantiareformaitem;
	}
	
	public Double getValorMinimo() {
		return valorMinimo;
	}
	public void setValorMinimo(Double valorMinimo) {
		this.valorMinimo = valorMinimo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialfaixamarkup")
	public MaterialFaixaMarkup getMaterialFaixaMarkup() {
		return materialFaixaMarkup;
	}
	public void setMaterialFaixaMarkup(MaterialFaixaMarkup materialFaixaMarkup) {
		this.materialFaixaMarkup = materialFaixaMarkup;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfaixamarkupnome")
	public FaixaMarkupNome getFaixaMarkupNome() {
		return faixaMarkupNome;
	}
	public void setFaixaMarkupNome(FaixaMarkupNome faixaMarkupNome) {
		this.faixaMarkupNome = faixaMarkupNome;
	}
	
	@DisplayName("Seguro")
	public Money getValorSeguro() {
		return valorSeguro;
	}
	public void setValorSeguro(Money valorSeguro) {
		this.valorSeguro = valorSeguro;
	}
	
	@DisplayName("Outras despesas")
	public Money getOutrasdespesas() {
		return outrasdespesas;
	}
	public void setOutrasdespesas(Money outrasdespesas) {
		this.outrasdespesas = outrasdespesas;
	}
	
	//IMPOSTOS
	public Tipotributacaoicms getTipotributacaoicms() {
		return tipotributacaoicms;
	}

	public Tipocobrancaicms getTipocobrancaicms() {
		return tipocobrancaicms;
	}

	public Money getValorbcicms() {
		return valorbcicms;
	}

	public Double getIcms() {
		return icms;
	}

	public Money getValoricms() {
		return valoricms;
	}
	
	public Money getValordesoneracaoicms() {
		return valordesoneracaoicms;
	}
	
	public void setValordesoneracaoicms(Money valordesoneracaoicms) {
		this.valordesoneracaoicms = valordesoneracaoicms;
	}
	
	public Double getPercentualdesoneracaoicms() {
		return percentualdesoneracaoicms;
	}
	
	public void setPercentualdesoneracaoicms(Double percentualdesoneracaoicms) {
		this.percentualdesoneracaoicms = percentualdesoneracaoicms;
	}
	
	public Boolean getAbaterdesoneracaoicms() {
		return abaterdesoneracaoicms;
	}
	
	public void setAbaterdesoneracaoicms(Boolean abaterdesoneracaoicms) {
		this.abaterdesoneracaoicms = abaterdesoneracaoicms;
	}

	public Money getValorbcicmsst() {
		return valorbcicmsst;
	}

	public Double getIcmsst() {
		return icmsst;
	}

	public Money getValoricmsst() {
		return valoricmsst;
	}

	public Double getReducaobcicmsst() {
		return reducaobcicmsst;
	}

	public Double getMargemvaloradicionalicmsst() {
		return margemvaloradicionalicmsst;
	}

	public Money getValorbcfcp() {
		return valorbcfcp;
	}

	public Double getFcp() {
		return fcp;
	}

	public Money getValorfcp() {
		return valorfcp;
	}

	public Money getValorbcfcpst() {
		return valorbcfcpst;
	}

	public Double getFcpst() {
		return fcpst;
	}

	public Money getValorfcpst() {
		return valorfcpst;
	}
	
	public Boolean getDadosicmspartilha() {
		return dadosicmspartilha;
	}

	public Money getValorbcdestinatario() {
		return valorbcdestinatario;
	}

	public Money getValorbcfcpdestinatario() {
		return valorbcfcpdestinatario;
	}

	public Double getFcpdestinatario() {
		return fcpdestinatario;
	}

	public Double getIcmsdestinatario() {
		return icmsdestinatario;
	}

	public Double getIcmsinterestadual() {
		return icmsinterestadual;
	}

	public Double getIcmsinterestadualpartilha() {
		return icmsinterestadualpartilha;
	}

	public Money getValorfcpdestinatario() {
		return valorfcpdestinatario;
	}

	public Money getValoricmsdestinatario() {
		return valoricmsdestinatario;
	}

	public Money getValoricmsremetente() {
		return valoricmsremetente;
	}
	
	public Double getDifal() {
		return difal;
	}
	public Money getValordifal() {
		return valordifal;
	}
	public Boolean getSinalizadoSemReserva() {
		return sinalizadoSemReserva;
	}
	
	@DisplayName("NCM")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdncmcapitulo")
	public Ncmcapitulo getNcmcapitulo() {
		return ncmcapitulo;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcfop")
	@DisplayName("CFOP")
	public Cfop getCfop() {
		return cfop;
	}
	
	@MaxLength(9)
	@DisplayName("NCM completo")
	public String getNcmcompleto() {
		return ncmcompleto;
	}
	
	public void setTipotributacaoicms(Tipotributacaoicms tipotributacaoicms) {
		this.tipotributacaoicms = tipotributacaoicms;
	}

	public void setTipocobrancaicms(Tipocobrancaicms tipocobrancaicms) {
		this.tipocobrancaicms = tipocobrancaicms;
	}

	public void setValorbcicms(Money valorbcicms) {
		this.valorbcicms = valorbcicms;
	}

	public void setIcms(Double icms) {
		this.icms = icms;
	}

	public void setValoricms(Money valoricms) {
		this.valoricms = valoricms;
	}

	public void setValorbcicmsst(Money valorbcicmsst) {
		this.valorbcicmsst = valorbcicmsst;
	}

	public void setIcmsst(Double icmsst) {
		this.icmsst = icmsst;
	}

	public void setValoricmsst(Money valoricmsst) {
		this.valoricmsst = valoricmsst;
	}

	public void setReducaobcicmsst(Double reducaobcicmsst) {
		this.reducaobcicmsst = reducaobcicmsst;
	}

	public void setMargemvaloradicionalicmsst(Double margemvaloradicionalicmsst) {
		this.margemvaloradicionalicmsst = margemvaloradicionalicmsst;
	}

	public void setValorbcfcp(Money valorbcfcp) {
		this.valorbcfcp = valorbcfcp;
	}

	public void setFcp(Double fcp) {
		this.fcp = fcp;
	}

	public void setValorfcp(Money valorfcp) {
		this.valorfcp = valorfcp;
	}

	public void setValorbcfcpst(Money valorbcfcpst) {
		this.valorbcfcpst = valorbcfcpst;
	}

	public void setFcpst(Double fcpst) {
		this.fcpst = fcpst;
	}

	public void setValorfcpst(Money valorfcpst) {
		this.valorfcpst = valorfcpst;
	}
	
	public void setDadosicmspartilha(Boolean dadosicmspartilha) {
		this.dadosicmspartilha = dadosicmspartilha;
	}

	public void setValorbcdestinatario(Money valorbcdestinatario) {
		this.valorbcdestinatario = valorbcdestinatario;
	}

	public void setValorbcfcpdestinatario(Money valorbcfcpdestinatario) {
		this.valorbcfcpdestinatario = valorbcfcpdestinatario;
	}

	public void setFcpdestinatario(Double fcpdestinatario) {
		this.fcpdestinatario = fcpdestinatario;
	}

	public void setIcmsdestinatario(Double icmsdestinatario) {
		this.icmsdestinatario = icmsdestinatario;
	}

	public void setIcmsinterestadual(Double icmsinterestadual) {
		this.icmsinterestadual = icmsinterestadual;
	}

	public void setIcmsinterestadualpartilha(Double icmsinterestadualpartilha) {
		this.icmsinterestadualpartilha = icmsinterestadualpartilha;
	}

	public void setValorfcpdestinatario(Money valorfcpdestinatario) {
		this.valorfcpdestinatario = valorfcpdestinatario;
	}

	public void setValoricmsdestinatario(Money valoricmsdestinatario) {
		this.valoricmsdestinatario = valoricmsdestinatario;
	}

	public void setValoricmsremetente(Money valoricmsremetente) {
		this.valoricmsremetente = valoricmsremetente;
	}
	
	public void setDifal(Double difal) {
		this.difal = difal;
	}
	
	public void setValordifal(Money valordifal) {
		this.valordifal = valordifal;
	}
	
	public void setSinalizadoSemReserva(Boolean sinalizadoSemReserva) {
		this.sinalizadoSemReserva = sinalizadoSemReserva;
	}
	
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	
	public void setNcmcapitulo(Ncmcapitulo ncmcapitulo) {
		this.ncmcapitulo = ncmcapitulo;
	}
	
	public void setNcmcompleto(String ncmcompleto) {
		this.ncmcompleto = ncmcompleto;
	}
	
	@Transient
	public String getLinkGarantiareformaVenda(){
		String link = "";
		if(getGarantiareformaitem() != null && getGarantiareformaitem().getGarantiareforma() != null && getGarantiareformaitem().getGarantiareforma().getCdgarantiareforma() != null){
			link = "<a href=\"javascript:visualizaGarantiareforma(" + getGarantiareformaitem().getGarantiareforma().getCdgarantiareforma() + ");\">" +
					"Garantia: " + getGarantiareformaitem().getGarantiareforma().getCdgarantiareforma() + "</a>"; 
		}
		return link;
	}
	
	public Integer getCdproducaoagendaitemadicional() {
		return cdproducaoagendaitemadicional;
	}
	
	public void setCdproducaoagendaitemadicional(Integer cdproducaoagendaitemadicional) {
		this.cdproducaoagendaitemadicional = cdproducaoagendaitemadicional;
	}

	@Transient
	public Integer getIdentificadorfabricante() {
		return identificadorfabricante;
	}

	public void setIdentificadorfabricante(Integer identificadorfabricante) {
		this.identificadorfabricante = identificadorfabricante;
	}
	
	@Transient
	public Boolean getConsiderarMultiplicadorCusto() {
		return considerarMultiplicadorCusto;
	}
	
	public void setConsiderarMultiplicadorCusto(
			Boolean considerarMultiplicadorCusto) {
		this.considerarMultiplicadorCusto = considerarMultiplicadorCusto;
	}
	
	@Transient
	public Money getTotalprodutoItemSemArredondamento() {
		Double total = 0.0;
		Double multiplicador = 1d;
		if(this.getMultiplicador() != null && this.getMultiplicador() > 0){
			multiplicador = this.getMultiplicador();
		}
		if(this.getPreco() != null){
			total  = this.getPreco() * (this.getQuantidade() != null ? this.getQuantidade() : 1.0) * multiplicador;
		}
		return new Money(SinedUtil.round(BigDecimal.valueOf(total), 10).doubleValue());
	}
	@Transient
	@Override
	public java.sql.Date getDtprazoentrega() {
		if(this.getPrazoentrega() != null){
			dtprazoentrega = new java.sql.Date(this.getPrazoentrega().getTime());;
		}
		return dtprazoentrega;
	}
	public void setDtprazoentrega(java.sql.Date dtprazoentrega) {
		this.dtprazoentrega = dtprazoentrega;
	}
	
	@Transient
	@Override
	public Integer getId() {
		return cdvendamaterial;
	}

	@Override
	public void setId(Integer id) {
//		this.id = id;
	}
	
	@Transient
	@Override
	public Integer getIndice() {
		return indice;
	}
	@Override
	public void setIndice(Integer indice) {
		this.indice = indice;
	}
	
	@Transient
	public String getDtprazoentregaStr() {
		return dtprazoentregaStr;
	}
	public void setDtprazoentregaStr(String dtprazoentregaStr) {
		this.dtprazoentregaStr = dtprazoentregaStr;
	}
	
	@Transient
	@Override
	public Boolean getAgendaProducaoGerada() {
		return agendaProducaoGerada;
	}
	@Override
	public void setAgendaProducaoGerada(Boolean agendaProducaoGerada) {
		this.agendaProducaoGerada = agendaProducaoGerada;
	}
	
	@Transient
	public Money getTotalprodutoOtr() {
		Double total = 0.0;
		if(this.getPreco() != null){
			total  = this.getPreco() * (this.getQuantidade() != null ? this.getQuantidade() : 1.0);
            total += MoneyUtils.moneyToDouble(this.valorSeguro);
            total += MoneyUtils.moneyToDouble(this.outrasdespesas);
			if(this.desconto != null) {
				total -= this.desconto.getValue().doubleValue();
			} else if(this.percentualdesconto != null) {
				total -= this.percentualdesconto / 100d * total;
			}
		}
		return new Money(SinedUtil.round(BigDecimal.valueOf(total), 2).doubleValue());
	}
	
	public void setTotalprodutoOtr(Money totalprodutoOtr) {
		this.totalprodutoOtr = totalprodutoOtr;
	}
	
	public Modalidadebcicms getModalidadebcicms() {
		return modalidadebcicms;
	}
	public void setModalidadebcicms(Modalidadebcicms modalidadebcicms) {
		this.modalidadebcicms = modalidadebcicms;
	}
	public Modalidadebcicmsst getModalidadebcicmsst() {
		return modalidadebcicmsst;
	}
	public void setModalidadebcicmsst(Modalidadebcicmsst modalidadebcicmsst) {
		this.modalidadebcicmsst = modalidadebcicmsst;
	}
	@Transient
	public Money getTotalImpostos() {
		Money totalImpostos = new Money()
								.add(getValoripi())
								.add(getValoricms())
								.add(getValoricmsst())
								.add(getValorfcp())
								.add(getValorfcpst())
								.add(getValordifal());
		if(abaterdesoneracaoicms != null && abaterdesoneracaoicms){
			totalImpostos = totalImpostos.subtract(getValordesoneracaoicms());
		}
		return totalImpostos;
	}
	public void setTotalImpostos(Money totalImpostos) {
		this.totalImpostos = totalImpostos;
	}
	
	@Transient
	@Override
	public Boolean getExibirIpiPopup() {
		return exibirIpiPopup;
	}
	@Override
	public void setExibirIpiPopup(Boolean exibirIpiPopup) {
		this.exibirIpiPopup = exibirIpiPopup;
	}
	@Transient
	@Override
	public Boolean getExibirIcmsPopup() {
		return exibirIcmsPopup;
	}
	@Override
	public void setExibirIcmsPopup(Boolean exibirIcmsPopup) {
		this.exibirIcmsPopup = exibirIcmsPopup;
	}
	@Transient
	@Override
	public Boolean getExibirDifalPopup() {
		return exibirDifalPopup;
	}
	@Override
	public void setExibirDifalPopup(Boolean exibirDifalPopup) {
		this.exibirDifalPopup = exibirDifalPopup;
	}
	@Transient
	@Override
	public Money getValorBruto() {
		if(this.quantidade != null && this.preco != null){
			valorBruto = new Money((this.quantidade * this.preco));
		}

		return valorBruto;
	}
	
	@Transient
	public double getPercentualProporcionalVenda() {
		Double percentualProporcionalProduto = 0d;
		
		if (venda != null && venda.getValorfrete() != null && venda.getListavendamaterial() != null) {
			Double valorBrutoVenda = 0d;
			Double valorBrutoProduto = 0d;
			
			for (Vendamaterial vendamaterial : venda.getListavendamaterial()) {
				if (vendamaterial.getPreco() != null && vendamaterial.getQuantidade() != null) {
					valorBrutoVenda = valorBrutoVenda + (vendamaterial.getPreco() * vendamaterial.getQuantidade());
				}
			}
			
			if	(this.quantidade != null && this.preco != null)	{
				valorBrutoProduto = this.quantidade * this.preco;
			}
			
			if (valorBrutoVenda > 0d && valorBrutoProduto > 0d) {
				percentualProporcionalProduto = (valorBrutoProduto * 100) / valorBrutoVenda;
			}
		}
		
		return percentualProporcionalProduto;
	}
	
	@Transient
	public double getPercentualProporcionalVenda(Double valorBrutoVenda) {
		Double percentualProporcionalProduto = 0d;
		
		if (venda != null && venda.getValorfrete() != null && venda.getListavendamaterial() != null) {
			Double valorBrutoProduto = 0d;
			
			if	(this.quantidade != null && this.preco != null)	{
				valorBrutoProduto = this.quantidade * this.preco;
			}
			
			if (valorBrutoVenda > 0d && valorBrutoProduto > 0d) {
				percentualProporcionalProduto = (valorBrutoProduto * 100) / valorBrutoVenda;
			}
		}
		
		return percentualProporcionalProduto;
	}
	
	@Transient
	public Money getFreteVendaPorporcional() {
		Money freteProporcional = null;
		
		if (venda.getValorTotal() != null && venda.getValorfrete() != null) {
			Double percentualProporcionalProduto = this.getPercentualProporcionalVenda(venda.getValorTotal().getValue().doubleValue());
			
			if (percentualProporcionalProduto != null && percentualProporcionalProduto > 0) {
				Double valorFreteVenda = venda.getValorfrete().getValue().doubleValue();
				Double valorFreteProduto = valorFreteVenda * (percentualProporcionalProduto / 100);
				
				freteProporcional = new Money(valorFreteProduto);
			}
		} else {
			freteProporcional = new Money();
		}
		
		return freteProporcional;
	}
	
	@Transient
	public Money getDescontoVendaPorporcional() {
		Money descontoProporcional = null;
		
		if (venda.getValorTotal() != null && venda.getDesconto() != null) {
			Double percentualProporcionalProduto = this.getPercentualProporcionalVenda(venda.getValorTotal().getValue().doubleValue());
			
			if (percentualProporcionalProduto != null && percentualProporcionalProduto > 0) {
				Double valorDescontoVenda = venda.getDesconto().getValue().doubleValue();
				Double valorDescontoProduto = valorDescontoVenda * (percentualProporcionalProduto / 100);
				
				descontoProporcional = new Money(valorDescontoProduto);
			}
		} else {
			descontoProporcional = new Money();
		}
		
		return descontoProporcional;
	}
	
	@OneToMany(mappedBy="vendamaterial")
	@DisplayName("Produtos")
	public List<Materialdevolucao> getListaMaterialdevolucao() {
		return listaMaterialdevolucao;
	}
	
	public void setListaMaterialdevolucao(List<Materialdevolucao> listaMaterialdevolucao) {
		this.listaMaterialdevolucao = listaMaterialdevolucao;
	}
	
	@Transient
	public String getNomeKitParaNF() {
		return nomeKitParaNF;
	}
	
	public void setNomeKitParaNF(String nomeKitParaNF) {
		this.nomeKitParaNF = nomeKitParaNF;
	}
	
	@Transient
	public String getCodigoBarrasEmbalagem() {
		return codigoBarrasEmbalagem;
	}
	
	public void setCodigoBarrasEmbalagem(String codigoBarrasEmbalagem) {
		this.codigoBarrasEmbalagem = codigoBarrasEmbalagem;
	}
	
	@Transient
	public Boolean getUnidadesMedidaDiferentesConferenciaExpedicao() {
		return unidadesMedidaDiferentesConferenciaExpedicao;
	}
	
	public void setUnidadesMedidaDiferentesConferenciaExpedicao(
			Boolean unidadesMedidaDiferentesConferenciaExpedicao) {
		this.unidadesMedidaDiferentesConferenciaExpedicao = unidadesMedidaDiferentesConferenciaExpedicao;
	}
	
	@Transient
	public Boolean getUnidadeMedidaPrincipalMaterial() {
		return unidadeMedidaPrincipalMaterial;
	}
	
	public void setUnidadeMedidaPrincipalMaterial(
			Boolean unidadeMedidaPrincipalMaterial) {
		this.unidadeMedidaPrincipalMaterial = unidadeMedidaPrincipalMaterial;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Vendamaterial) {
			Vendamaterial vendamaterial = (Vendamaterial) obj;
			return vendamaterial.getCdvendamaterial().equals(this.getCdvendamaterial());
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if (cdvendamaterial != null) {
			return cdvendamaterial.hashCode();
		}
		return super.hashCode();
	}
}
