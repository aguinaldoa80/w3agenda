package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name="sq_NotaHistorico",sequenceName="sq_NotaHistorico")
public class NotaHistorico implements Log {

	protected Integer cdNotaHistorico;
	protected Nota nota;
	protected NotaStatus notaStatus;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected MotivoCancelamentoFaturamento motivoCancelamentoFaturamento;
	protected Date dtrecebimento;
	
	//Transientes
	private String itensSelecionados;
	private Boolean cancelamentoVenda = Boolean.FALSE;
	private Boolean cancelamentoContareceberAposEmissaoNota = Boolean.FALSE;
	
	public NotaHistorico() {}

	public NotaHistorico(Integer cdNotaHistorico, Nota nota, NotaStatus notaStatus,
								String observacao, Integer cdusuarioaltera, Timestamp dtaltera) {
		this.cdNotaHistorico = cdNotaHistorico;
		this.nota = nota;
		this.notaStatus = notaStatus;
		this.observacao = observacao;
		this.cdusuarioaltera = cdusuarioaltera;
		this.dtaltera = dtaltera;
	}

	public NotaHistorico(Integer cdNotaHistorico, Nota nota, NotaStatus notaStatus, String observacao, 
			MotivoCancelamentoFaturamento motivoCancelamentoFaturamento, Integer cdusuarioaltera, Timestamp dtaltera) {
		this.cdNotaHistorico = cdNotaHistorico;
		this.nota = nota;
		this.notaStatus = notaStatus;
		this.observacao = observacao;
		this.cdusuarioaltera = cdusuarioaltera;
		this.dtaltera = dtaltera;
		this.motivoCancelamentoFaturamento = motivoCancelamentoFaturamento;
	}
	
	public NotaHistorico(Nota nota, String observacao, NotaStatus notaStatus){
		this.nota = nota;
		this.observacao = observacao;
		this.notaStatus = notaStatus;
	}

	public NotaHistorico(Nota nota, String observacao, NotaStatus notaStatus, Integer cdusuarioaltera, Timestamp dtaltera){
		this.nota = nota;
		this.observacao = observacao;
		this.notaStatus = notaStatus;
		this.cdusuarioaltera = cdusuarioaltera;
		this.dtaltera = dtaltera;
	}

	public NotaHistorico(Integer cdusuarioaltera, Timestamp dtaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
		this.dtaltera = dtaltera;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(generator="sq_NotaHistorico",strategy=GenerationType.AUTO)
	public Integer getCdNotaHistorico() {
		return cdNotaHistorico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdNota")
	public Nota getNota() {
		return nota;
	}
	
	@DisplayName("A��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdNotaStatus")
	public NotaStatus getNotaStatus() {
		return notaStatus;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
	@DisplayName("Motivo de Cancelamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmotivocancelamentofaturamento")
	public MotivoCancelamentoFaturamento getMotivoCancelamentoFaturamento() {
		return motivoCancelamentoFaturamento;
	}
	
	public Date getDtrecebimento() {
		return dtrecebimento;
	}
	
	public void setDtrecebimento(Date dtrecebimento) {
		this.dtrecebimento = dtrecebimento;
	}
	
	public void setCdNotaHistorico(Integer cdNotaHistorico) {
		this.cdNotaHistorico = cdNotaHistorico;
	}
	
	public void setNota(Nota nota) {
		this.nota = nota;
	}
	
	public void setNotaStatus(NotaStatus notaStatus) {
		this.notaStatus = notaStatus;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@Transient
	public String getItensSelecionados() {
		return itensSelecionados;
	}
	
	@Transient
	@DisplayName("Fazer o cancelamento da venda?")
	public Boolean getCancelamentoVenda() {
		return cancelamentoVenda;
	}

	@Transient
	@DisplayName("Fazer o cancelamento da conta a receber?")
	public Boolean getCancelamentoContareceberAposEmissaoNota() {
		return cancelamentoContareceberAposEmissaoNota;
	}
	
	public void setCancelamentoContareceberAposEmissaoNota(Boolean cancelamentoContareceberAposEmissaoNota) {
		this.cancelamentoContareceberAposEmissaoNota = cancelamentoContareceberAposEmissaoNota;
	}

	public void setCancelamentoVenda(Boolean cancelamentoVenda) {
		this.cancelamentoVenda = cancelamentoVenda;
	}
	
	public void setItensSelecionados(String itensSelecionados) {
		this.itensSelecionados = itensSelecionados;
	}
	
	
	public void setMotivoCancelamentoFaturamento(
			MotivoCancelamentoFaturamento motivoCancelamentoFaturamento) {
		this.motivoCancelamentoFaturamento = motivoCancelamentoFaturamento;
	}
	//Log:
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	//End Log





}
