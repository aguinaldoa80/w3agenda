package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.TipoUnidadeCarga;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_mdfeinformacaounidadecargamdfereferenciado", sequenceName = "sq_mdfeinformacaounidadecargamdfereferenciado")
public class MdfeInformacaoUnidadeCargaMdfeReferenciado {

	protected Integer cdMdfeInformacaoUnidadeCargaMdfeReferenciado;
	protected Mdfe mdfe;
	protected String idUnidadeTransporte;
	protected String idUnidadeCarga;
	protected TipoUnidadeCarga tipoCarga;
	protected String identificacaoUnidadeCarga;
	protected Money quantidadeRateada;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfeinformacaounidadecargamdfereferenciado")
	public Integer getCdMdfeInformacaoUnidadeCargaMdfeReferenciado() {
		return cdMdfeInformacaoUnidadeCargaMdfeReferenciado;
	}
	public void setCdMdfeInformacaoUnidadeCargaMdfeReferenciado(Integer cdMdfeInformacaoUnidadeCarga) {
		this.cdMdfeInformacaoUnidadeCargaMdfeReferenciado = cdMdfeInformacaoUnidadeCarga;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@DisplayName("ID da unidade de transporte")
	public String getIdUnidadeTransporte() {
		return idUnidadeTransporte;
	}
	public void setIdUnidadeTransporte(String idUnidadeTransporte) {
		this.idUnidadeTransporte = idUnidadeTransporte;
	}
	
	@DisplayName("ID da unidade de carga")
	public String getIdUnidadeCarga() {
		return idUnidadeCarga;
	}
	public void setIdUnidadeCarga(String idUnidadeCarga) {
		this.idUnidadeCarga = idUnidadeCarga;
	}
	
	@Required
	@DisplayName("Tipo de carga")
	public TipoUnidadeCarga getTipoCarga() {
		return tipoCarga;
	}
	public void setTipoCarga(TipoUnidadeCarga tipoCarga) {
		this.tipoCarga = tipoCarga;
	}
	
	@Required
	@MaxLength(value=20)
	@DisplayName("Identificação da unidade de carga")
	public String getIdentificacaoUnidadeCarga() {
		return identificacaoUnidadeCarga;
	}
	public void setIdentificacaoUnidadeCarga(String identificacaoUnidadeCarga) {
		this.identificacaoUnidadeCarga = identificacaoUnidadeCarga;
	}
	
	@DisplayName("Quantidade rateada")
	public Money getQuantidadeRateada() {
		return quantidadeRateada;
	}
	public void setQuantidadeRateada(Money quantidadeRateada) {
		this.quantidadeRateada = quantidadeRateada;
	}
	
	@Transient
	public List<MdfeLacreUnidadeCargaMdfeReferenciado> getListaLacreUnidadeCargaMdfeReferenciado(MdfeInformacaoUnidadeCargaMdfeReferenciado infUnTransMdfeReferenciado, Mdfe mdfe) {
		List<MdfeLacreUnidadeCargaMdfeReferenciado> list = new ArrayList<MdfeLacreUnidadeCargaMdfeReferenciado>();
		if(SinedUtil.isListNotEmpty(mdfe.getListaLacreUnidadeCargaMdfeReferenciado())){
			for(MdfeLacreUnidadeCargaMdfeReferenciado item : mdfe.getListaLacreUnidadeCargaMdfeReferenciado()){
				if(item.getIdLacre().equals(infUnTransMdfeReferenciado.getIdUnidadeTransporte())){
					list.add(item);
				}
			}
		}
		return list;
	}
}
