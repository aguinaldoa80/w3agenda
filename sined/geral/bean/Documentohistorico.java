package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name="sq_documentohistorico",sequenceName="sq_documentohistorico")
public class Documentohistorico implements Log{

	protected Integer cddocumentohistorico;
	protected Documento documento;
	protected Documentoacao documentoacao;
	protected Documentotipo documentotipo;
	protected String descricao;
	protected String numero;
	protected Pessoa pessoa;
	protected String outrospagamento;
	protected Tipopagamento tipopagamento;
	protected Date dtemissao;
	protected Date dtvencimento;
	protected Money valor;
	protected String observacao;
	protected Motivocancelamento motivocancelamento;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Usuario usuarioaltera;
	
	//TRANSIENTS
	protected Date dtcartorio;
	protected String ids;
	protected Double percentual;
	protected Money valortotaux;
	protected Boolean reagendar;
	protected Boolean fromcontareceber;
	protected Prazopagamento prazopagamento;
	protected List<Documento> parcelas;
	protected Date dtreferencia;
	protected Boolean fromAcaoProtestar;
	
	public Documentohistorico() {}
	
	public Documentohistorico(Documento documento) {
		this.setDocumento(documento);
		this.setDocumentotipo(documento.getDocumentotipo());
		this.setDtemissao(documento.getDtemissao());
		this.setDtvencimento(documento.getDtvencimento());
		this.setPessoa(documento.getPessoa());
		this.setTipopagamento(documento.getTipopagamento());
		this.setOutrospagamento(documento.getOutrospagamento());
		this.setNumero(documento.getNumero());
		this.setValor(documento.getValor());
		this.setDescricao(documento.getDescricao());
		this.setObservacao(documento.getObservacaoHistorico());
		this.setCdusuarioaltera(SinedUtil.getUsuarioLogado() != null ? SinedUtil.getUsuarioLogado().getCdpessoa() : null);
		this.setDtaltera(new Timestamp(System.currentTimeMillis()));
		
		if(documento.getAcaohistorico() != null){
			this.setDocumentoacao(documento.getAcaohistorico());
		}else{
			this.setDocumentoacao(documento.getDocumentoacao());
		}
	}
	
	@Id
	@GeneratedValue(generator="sq_documentohistorico",strategy=GenerationType.AUTO)
	public Integer getCddocumentohistorico() {
		return cddocumentohistorico;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}
	@DisplayName("A��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoacao")
	public Documentoacao getDocumentoacao() {
		return documentoacao;
	}
	
	@DisplayName("Motivo do cancelamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmotivocancelamento")
	public Motivocancelamento getMotivocancelamento() {
		return motivocancelamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentotipo")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Documento")
	public String getNumero() {
		return numero;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Pessoa getPessoa() {
		return pessoa;
	}
	@DisplayName("Tipo de pagamento")
	public Tipopagamento getTipopagamento() {
		return tipopagamento;
	}
	@DisplayName("Outros")
	@MaxLength(50)
	public String getOutrospagamento() {
		return outrospagamento;
	}
	@DisplayName("Data de emiss�o")
	public Date getDtemissao() {
		return dtemissao;
	}
	@DisplayName("Data de vencimento")
	public Date getDtvencimento() {
		return dtvencimento;
	}
	@Transient
	@DisplayName("Data de cart�rio")
	public Date getDtcartorio() {
		return dtcartorio;
	}
	public Money getValor() {
		return valor;
	}
	@DisplayName("Observa��o")
	@MaxLength(10000)
	public String getObservacao() {
		return observacao;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@DisplayName("Data de altera��o")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
	@Transient
	public String getIds() {
		return ids;
	}
	
	@Transient
	@MaxLength(6)
	public Double getPercentual() {
		return percentual;
	}
	
	@Transient
	public Money getValortotaux() {
		return valortotaux;
	}
	
	@Transient
	@DisplayName("Reagendar conta(s) a pagar.")
	public Boolean getReagendar() {
		return reagendar;
	}
	
	@Transient
	public Boolean getFromcontareceber() {
		return fromcontareceber;
	}
	
	public void setFromcontareceber(Boolean fromcontareceber) {
		this.fromcontareceber = fromcontareceber;
	}
	
	public void setReagendar(Boolean reagendar) {
		this.reagendar = reagendar;
	}
	
	public void setValortotaux(Money valortotaux) {
		this.valortotaux = valortotaux;
	}
	
	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}
	
	public void setIds(String ids) {
		this.ids = ids;
	}
	
	public void setCddocumentohistorico(Integer cddocumentohistorico) {
		this.cddocumentohistorico = cddocumentohistorico;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setDocumentoacao(Documentoacao documentoacao) {
		this.documentoacao = documentoacao;
	}
	public void setMotivocancelamento(Motivocancelamento motivocancelamento) {
		this.motivocancelamento = motivocancelamento;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public void setTipopagamento(Tipopagamento tipopagamento) {
		this.tipopagamento = tipopagamento;
	}
	public void setOutrospagamento(String outrospagamento) {
		this.outrospagamento = outrospagamento;
	}
	public void setDtemissao(Date dtemissao) {
		this.dtemissao = dtemissao;
	}
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}
	public void setDtcartorio(Date dtcartorio) {
		this.dtcartorio = dtcartorio;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	@Transient
	@DisplayName("Prazo de Pagamento")
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}

	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	
	@JoinColumn(name="cdusuarioaltera", insertable=false, updatable=false)
	@ManyToOne(fetch=FetchType.LAZY)
	public Usuario getUsuarioaltera() {
		return usuarioaltera;
	}
	
	public void setUsuarioaltera(Usuario usuarioaltera) {
		this.usuarioaltera = usuarioaltera;
	}

	@Transient
	public List<Documento> getParcelas() {
		return parcelas;
	}

	public void setParcelas(List<Documento> parcelas) {
		this.parcelas = parcelas;
	}
	
	@Transient
	@DisplayName("Data refer�ncia")
	public Date getDtreferencia() {
		return dtreferencia;
	}

	public void setDtreferencia(Date dtreferencia) {
		this.dtreferencia = dtreferencia;
	}

	@Transient
	public Boolean getFromAcaoProtestar() {
		return fromAcaoProtestar;
	}

	public void setFromAcaoProtestar(Boolean fromAcaoProtestar) {
		this.fromAcaoProtestar = fromAcaoProtestar;
	}
}
