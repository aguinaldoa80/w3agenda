package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_tipoeventos", sequenceName = "sq_tipoeventos")
public class TipoEventos {
	protected Integer cdtiposeventos;
	//protected TipoEventoEnum tipoEvento;
	protected TipoEventoCorreios tipoEventoCorreios;
	protected EventosCorreios eventosCorreios;
	protected Integer codigo;
	protected EventosFinaisCorreios eventosFinaisCorreios;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_tipoeventos")
	public Integer getCdtiposeventos() {
		return cdtiposeventos;
	}
	public void setCdtiposeventos(Integer cdtiposeventos) {
		this.cdtiposeventos = cdtiposeventos;
	}
	/*
	@Transient
	public TipoEventoEnum getTipoEvento() {
		return tipoEvento;
	}
	public void setTipoEvento(TipoEventoEnum tipoEvento) {
		this.tipoEvento = tipoEvento;
	}
	*/
	@DisplayName("Tipo de evento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipoeventocorreios")
	public TipoEventoCorreios getTipoEventoCorreios() {
		return tipoEventoCorreios;
	}
	public void setTipoEventoCorreios(TipoEventoCorreios tipoEventoCorreios) {
		this.tipoEventoCorreios = tipoEventoCorreios;
	}
	@DisplayName("Evento dos correios")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdeventoscorreios")
	public EventosCorreios getEventosCorreios() {
		return eventosCorreios;
	}
	public void setEventosCorreios(EventosCorreios eventosCorreios) {
		this.eventosCorreios = eventosCorreios;
	}
	@DisplayName("C�digo")
	@Transient
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdeventosfinaiscorreios")
	public EventosFinaisCorreios getEventosFinaisCorreios() {
		return eventosFinaisCorreios;
	}
	public void setEventosFinaisCorreios(EventosFinaisCorreios eventosFinaisCorreios) {
		this.eventosFinaisCorreios = eventosFinaisCorreios;
	}
	
	
}
