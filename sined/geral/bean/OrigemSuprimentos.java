package br.com.linkcom.sined.geral.bean;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum OrigemSuprimentos {
	
	PROJETO			(1,"Projeto"),
	REPOSICAO		(2,"Reposi��o"),
	USUARIO			(3,"Usu�rio"),
	REQUISICAO		(4,"Requisi��o"),
	LISTAMATERIAL 	(5, "Lista de Material do Or�amento"),
	PEDIDOVENDA		(6, "Pedido de venda");
	
	private Integer value;
	private String nome;
	
	private OrigemSuprimentos(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
