package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_garantiareformaresultado", sequenceName="sq_garantiareformaresultado")
public class Garantiareformaresultado implements Log{

	private Integer cdgarantiareformaresultado;
	private String nome;
	private String mensagem1;
	private String mensagem2;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	private Boolean ativo = Boolean.TRUE;
	
	@Id
	@GeneratedValue(generator="sq_garantiareformaresultado", strategy=GenerationType.AUTO)
	public Integer getCdgarantiareformaresultado() {
		return cdgarantiareformaresultado;
	}
	public void setCdgarantiareformaresultado(Integer cdgarantiareformaresultado) {
		this.cdgarantiareformaresultado = cdgarantiareformaresultado;
	}
	
	@Required
	@DescriptionProperty
	@MaxLength(50)
	@DisplayName("Resultado da garantia")
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@DisplayName("Mensagem do resultado")
	@MaxLength(500)
	public String getMensagem1() {
		return mensagem1;
	}
	public void setMensagem1(String mensagem1) {
		this.mensagem1 = mensagem1;
	}
	
	@DisplayName("Mensagem do destino")
	@MaxLength(500)
	public String getMensagem2() {
		return mensagem2;
	}
	public void setMensagem2(String mensagem2) {
		this.mensagem2 = mensagem2;
	}
	
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
		
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
