package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Colaboradortipo {

	public static final Integer NORMAL=1;
	public static final Integer DOMESTICO=2;
	public static final Integer CONTRATATO=3;
	public static final Integer MENOR_APRENDIZ=4;
	
	protected Integer cdcolaboradortipo;
	protected String nome;
	
	public Colaboradortipo(){}
	
	public Colaboradortipo(Integer cdcolaboradortipo){
		this.cdcolaboradortipo = cdcolaboradortipo;
	}
	
	@Id
	public Integer getCdcolaboradortipo() {
		return cdcolaboradortipo;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setCdcolaboradortipo(Integer cdcolaboradortipo) {
		this.cdcolaboradortipo = cdcolaboradortipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
