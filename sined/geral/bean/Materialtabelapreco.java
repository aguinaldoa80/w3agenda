package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoClienteEmpresa;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.CompararCampo;


@Entity
@DisplayName("Tabela de pre�o")
@SequenceGenerator(name = "sq_materialtabelapreco", sequenceName = "sq_materialtabelapreco")
public class Materialtabelapreco implements Log, PermissaoClienteEmpresa {

	protected Integer cdmaterialtabelapreco;
	protected Cliente cliente;
	protected Prazopagamento prazopagamento;
	protected String nome;
	protected Date dtinicio;
	protected Date dtfim;
	protected Pedidovendatipo pedidovendatipo;
	protected Money taxaacrescimo;
	protected Frequencia frequencia;
	protected Double descontopadrao;
	protected Double acrescimopadrao;
	protected Categoria categoria;
	protected Comissionamento comissionamento;
	protected String observacao;
	protected Materialgrupo materialgrupo;
	protected Tipocalculo tabelaprecotipo;
	protected Boolean discriminardescontovenda;
	
//	LISTAS
	protected Set<Materialtabelaprecoitem> listaMaterialtabelaprecoitem = new ListSet<Materialtabelaprecoitem>(Materialtabelaprecoitem.class);
	protected Set<Materialtabelaprecocliente> listaMaterialtabelaprecocliente = new ListSet<Materialtabelaprecocliente>(Materialtabelaprecocliente.class);
	protected Set<Materialtabelaprecoempresa> listaMaterialtabelaprecoempresa = new ListSet<Materialtabelaprecoempresa>(Materialtabelaprecoempresa.class);
	protected Set<MaterialTabelaPrecoHistorico> listaMaterialTabelaPrecoHistorico = new ListSet<MaterialTabelaPrecoHistorico>(MaterialTabelaPrecoHistorico.class);
	
//	LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
//	TRANSIENT
	protected Material materialaux;
	protected Prazopagamento prazopagamentoaux;
	protected Money valoraux;
	protected Money valorvendaminimoaux;
	protected Money valorvendamaximoaux;
	protected Double reajuste;
	protected List<Materialtabelaprecoitem> listaMaterialtabelaprecoitemTrans;
	protected Arquivo arquivoImportacaoProduto;
	
	public Materialtabelapreco() {}
	
	public Materialtabelapreco(Integer cdmaterialtabelapreco) {
		this.cdmaterialtabelapreco = cdmaterialtabelapreco;
	}	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialtabelapreco")
	public Integer getCdmaterialtabelapreco() {
		return cdmaterialtabelapreco;
	}	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Prazo de pagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprazopagamento")
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}

	@CompararCampo
	@DisplayName("Nome")
	@DescriptionProperty
	@Required
	@MaxLength(30)
	public String getNome() {
		return nome;
	}

	@CompararCampo
	@Required
	@DisplayName("V�lido de")
	public Date getDtinicio() {
		return dtinicio;
	}

	@CompararCampo
	@DisplayName("V�lido at�")
	public Date getDtfim() {
		return dtfim;
	}	
	@DisplayName("Tipo de Pedido de Venda")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendatipo")
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}
	@DisplayName("Materiais")
	@OneToMany(mappedBy="materialtabelapreco")
	public Set<Materialtabelaprecoitem> getListaMaterialtabelaprecoitem() {
		return listaMaterialtabelaprecoitem;
	}
	@DisplayName("Clientes")
	@OneToMany(mappedBy="materialtabelapreco")
	public Set<Materialtabelaprecocliente> getListaMaterialtabelaprecocliente() {
		return listaMaterialtabelaprecocliente;
	}
	@DisplayName("Empresas")
	@OneToMany(mappedBy="materialtabelapreco")
	public Set<Materialtabelaprecoempresa> getListaMaterialtabelaprecoempresa() {
		return listaMaterialtabelaprecoempresa;
	}

	@DisplayName("Hist�rico")
	@OneToMany(mappedBy = "materialtabelapreco")
	public Set<MaterialTabelaPrecoHistorico> getListaMaterialTabelaPrecoHistorico() {
		return listaMaterialTabelaPrecoHistorico;
	}

	@CompararCampo
	@DisplayName("Taxa de Acr�scimo")
	public Money getTaxaacrescimo() {
		return taxaacrescimo;
	}
	@DisplayName("Periodicidade")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfrequencia")
	public Frequencia getFrequencia() {
		return frequencia;
	}

	@CompararCampo
	@DisplayName("Desconto padr�o %")
	public Double getDescontopadrao() {
		return descontopadrao;
	}

	@CompararCampo
	@DisplayName("Acr�scimo padr�o %")
	public Double getAcrescimopadrao() {
		return acrescimopadrao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcategoria")
	public Categoria getCategoria() {
		return categoria;
	}
	@DisplayName("Comissionamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcomissionamento")
	public Comissionamento getComissionamento() {
		return comissionamento;
	}

	@CompararCampo
	@DisplayName("Observa��o")
	@MaxLength(500)
	public String getObservacao() {
		return observacao;
	}
	@DisplayName("Grupo de material")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialgrupo")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	
	@Required
	@DisplayName("Tipo de Tabela de Pre�o")
	public Tipocalculo getTabelaprecotipo() {
		return tabelaprecotipo;
	}

	@CompararCampo
	@DisplayName("Discriminar desconto na venda")
	public Boolean getDiscriminardescontovenda() {
		return discriminardescontovenda;
	}
	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}
	public void setDescontopadrao(Double descontopadrao) {
		this.descontopadrao = descontopadrao;
	}
	public void setAcrescimopadrao(Double acrescimopadrao) {
		this.acrescimopadrao = acrescimopadrao;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public void setComissionamento(Comissionamento comissionamento) {
		this.comissionamento = comissionamento;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setTabelaprecotipo(Tipocalculo tabelaprecotipo) {
		this.tabelaprecotipo = tabelaprecotipo;
	}
	public void setDiscriminardescontovenda(Boolean discriminardescontovenda) {
		this.discriminardescontovenda = discriminardescontovenda;
	}
	public void setTaxaacrescimo(Money taxaacrescimo) {
		this.taxaacrescimo = taxaacrescimo;
	}
	public void setListaMaterialtabelaprecoitem(Set<Materialtabelaprecoitem> listaMaterialtabelaprecoitem) {
		this.listaMaterialtabelaprecoitem = listaMaterialtabelaprecoitem;
	}
	public void setListaMaterialtabelaprecocliente(Set<Materialtabelaprecocliente> listaMaterialtabelaprecocliente) {
		this.listaMaterialtabelaprecocliente = listaMaterialtabelaprecocliente;
	}
	public void setListaMaterialtabelaprecoempresa(Set<Materialtabelaprecoempresa> listaMaterialtabelaprecoempresa) {
		this.listaMaterialtabelaprecoempresa = listaMaterialtabelaprecoempresa;
	}

	public void setListaMaterialTabelaPrecoHistorico(Set<MaterialTabelaPrecoHistorico> listaMaterialTabelaPrecoHistorico) {
		this.listaMaterialTabelaPrecoHistorico = listaMaterialTabelaPrecoHistorico;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}

	public void setCdmaterialtabelapreco(Integer cdmaterialtabelapreco) {
		this.cdmaterialtabelapreco = cdmaterialtabelapreco;
	}
	
	public String subQueryClienteEmpresa() {
		return "select materialtabelaprecoSubQueryClienteEmpresa.cdmaterialtabelapreco " +
				"from materialtabelapreco materialtabelaprecoSubQueryClienteEmpresa " +
				"left outer join materialtabelaprecoSubQueryClienteEmpresa.listaMaterialtabelaprecocliente listaMaterialtabelaprecoclienteSubQueryClienteEmpresa " +
				"left outer join listaMaterialtabelaprecoclienteSubQueryClienteEmpresa.cliente clienteSubQueryClienteEmpresa " +
				"where true = permissao_clienteempresa(clienteSubQueryClienteEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}

	
//	LOG
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	@Transient
	@DisplayName("Material")
	public Material getMaterialaux() {
		return materialaux;
	}
	@Transient
	@DisplayName("Prazo de pagamento")
	public Prazopagamento getPrazopagamentoaux() {
		return prazopagamentoaux;
	}
	@Transient
	@DisplayName("Valor")
	public Money getValoraux() {
		return valoraux;
	}
	@Transient
	@DisplayName("M�nimo")
	public Money getValorvendaminimoaux() {
		return valorvendaminimoaux;
	}
	@Transient
	@DisplayName("M�ximo")
	public Money getValorvendamaximoaux() {
		return valorvendamaximoaux;
	}

	public void setValorvendaminimoaux(Money valorvendaminimoaux) {
		this.valorvendaminimoaux = valorvendaminimoaux;
	}

	public void setValorvendamaximoaux(Money valorvendamaximoaux) {
		this.valorvendamaximoaux = valorvendamaximoaux;
	}

	public void setMaterialaux(Material materialaux) {
		this.materialaux = materialaux;
	}
	public void setPrazopagamentoaux(Prazopagamento prazopagamentoaux) {
		this.prazopagamentoaux = prazopagamentoaux;
	}
	public void setValoraux(Money valoraux) {
		this.valoraux = valoraux;
	}
	
	@Transient
	@MinValue(0)
	@DisplayName("Reajuste (%)")
	public Double getReajuste() {
		return reajuste;
	}
	public void setReajuste(Double reajuste) {
		this.reajuste = reajuste;
	}

	@Transient
	public List<Materialtabelaprecoitem> getListaMaterialtabelaprecoitemTrans() {
		return listaMaterialtabelaprecoitemTrans;
	}
	public void setListaMaterialtabelaprecoitemTrans(List<Materialtabelaprecoitem> listaMaterialtabelaprecoitemTrans) {
		this.listaMaterialtabelaprecoitemTrans = listaMaterialtabelaprecoitemTrans;
	}
	@Transient
	public Set<Materialtabelaprecoitem> getListaMaterialtabelaprecoitemUnidadesecundaria() {
		Set<Materialtabelaprecoitem> lista = new ListSet<Materialtabelaprecoitem>(Materialtabelaprecoitem.class);
		
		if(this.getListaMaterialtabelaprecoitem() != null && !this.getListaMaterialtabelaprecoitem().isEmpty()){
			for(Materialtabelaprecoitem materialtabelaprecoitem : this.getListaMaterialtabelaprecoitem()){
				if(materialtabelaprecoitem.getUnidademedida() != null && materialtabelaprecoitem.getMaterial() != null && materialtabelaprecoitem.getMaterial().getUnidademedida() != null && 
							!materialtabelaprecoitem.getMaterial().getUnidademedida().equals(materialtabelaprecoitem.getUnidademedida())){
					lista.add(materialtabelaprecoitem);
				}
			}
		}
		
		return lista;
	}
	@Transient
	public Set<Materialtabelaprecoitem> getListaMaterialtabelaprecoitemUnidadeprincipal() {
		Set<Materialtabelaprecoitem> lista = new ListSet<Materialtabelaprecoitem>(Materialtabelaprecoitem.class);
		
		if(this.getListaMaterialtabelaprecoitem() != null && !this.getListaMaterialtabelaprecoitem().isEmpty()){
			for(Materialtabelaprecoitem materialtabelaprecoitem : this.getListaMaterialtabelaprecoitem()){
				if(materialtabelaprecoitem.getUnidademedida() == null){
					lista.add(materialtabelaprecoitem);
				}else if(materialtabelaprecoitem.getMaterial() != null && materialtabelaprecoitem.getMaterial().getUnidademedida() != null && 
							materialtabelaprecoitem.getMaterial().getUnidademedida().equals(materialtabelaprecoitem.getUnidademedida())){
					lista.add(materialtabelaprecoitem);
				}
			}
		}
		
		return lista;
	}
	
	@Transient
	public Double getValorComDescontoAcrescimo(Double valorvenda, Integer numCasasDecimais) {
		Double valortabelapreco = valorvenda;
		if(valortabelapreco != null){
			Double totalAcrescimo = 0.0;
			if(this.getTaxaacrescimo() != null && this.getTaxaacrescimo().getValue().doubleValue() > 0){
				totalAcrescimo += valorvenda*this.getTaxaacrescimo().getValue().doubleValue()/100;
			}
			if(this.getDescontopadrao() != null && !Boolean.TRUE.equals(getDiscriminardescontovenda())){
				totalAcrescimo -= valorvenda*this.getDescontopadrao()/100;
			}
			if(this.getAcrescimopadrao() != null){
				totalAcrescimo += valorvenda*this.getAcrescimopadrao()/100;
			}
			valortabelapreco = valorvenda + totalAcrescimo; 
		}
		if(numCasasDecimais != null && numCasasDecimais > 0){
			valortabelapreco = SinedUtil.round(valortabelapreco, numCasasDecimais);
		}
		return valortabelapreco;
	}
	
	@Transient
	public Money getPercentualDescontoDiscriminado(){
		if(Boolean.TRUE.equals(getDiscriminardescontovenda()) && Tipocalculo.PERCENTUAL.equals(getTabelaprecotipo())){
			return this.getDescontopadrao() != null ? new Money(this.getDescontopadrao()) : null;
		}
		
		return null;
	}

	@Transient
	@DisplayName("Importar Produtos")
	public Arquivo getArquivoImportacaoProduto() {
		return arquivoImportacaoProduto;
	}

	public void setArquivoImportacaoProduto(Arquivo arquivoImportacaoProduto) {
		this.arquivoImportacaoProduto = arquivoImportacaoProduto;
	}
}
