package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_bancoconfiguracaoretornosegmentodetalhe",sequenceName="sq_bancoconfiguracaoretornosegmentodetalhe")
public class BancoConfiguracaoRetornoSegmentoDetalhe implements Log {

	private Integer cdBancoConfiguracaoRetornoSegmentoDetalhe;
	private BancoConfiguracaoRetorno bancoConfiguracaoRetorno;
	private BancoConfiguracaoRetornoSegmento bancoConfiguracaoRetornoSegmento;
	private Set<BancoConfiguracaoRetornoCampo> listaBancoConfiguracaoRetornoCampo = new ListSet<BancoConfiguracaoRetornoCampo>(BancoConfiguracaoRetornoCampo.class);
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoconfiguracaoretornosegmentodetalhe")
	public Integer getCdBancoConfiguracaoRetornoSegmentoDetalhe() {
		return cdBancoConfiguracaoRetornoSegmentoDetalhe;
	}
	public void setCdBancoConfiguracaoRetornoSegmentoDetalhe(
			Integer cdBancoConfiguracaoRetornoSegmentoDetalhe) {
		this.cdBancoConfiguracaoRetornoSegmentoDetalhe = cdBancoConfiguracaoRetornoSegmentoDetalhe;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaoretorno")
	@Required
	public BancoConfiguracaoRetorno getBancoConfiguracaoRetorno() {
		return bancoConfiguracaoRetorno;
	}
	public void setBancoConfiguracaoRetorno(
			BancoConfiguracaoRetorno bancoConfiguracaoRetorno) {
		this.bancoConfiguracaoRetorno = bancoConfiguracaoRetorno;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaoretornosegmento")
	public BancoConfiguracaoRetornoSegmento getBancoConfiguracaoRetornoSegmento() {
		return bancoConfiguracaoRetornoSegmento;
	}
	public void setBancoConfiguracaoRetornoSegmento(
			BancoConfiguracaoRetornoSegmento bancoConfiguracaoRetornoSegmento) {
		this.bancoConfiguracaoRetornoSegmento = bancoConfiguracaoRetornoSegmento;
	}
	
	@OneToMany(mappedBy="bancoConfiguracaoRetornoSegmentoDetalhe")
	public Set<BancoConfiguracaoRetornoCampo> getListaBancoConfiguracaoRetornoCampo() {
		return listaBancoConfiguracaoRetornoCampo;
	}
	public void setListaBancoConfiguracaoRetornoCampo(
			Set<BancoConfiguracaoRetornoCampo> listaBancoConfiguracaoRetornoCampo) {
		this.listaBancoConfiguracaoRetornoCampo = listaBancoConfiguracaoRetornoCampo;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdBancoConfiguracaoRetornoSegmentoDetalhe == null) ? 0
						: cdBancoConfiguracaoRetornoSegmentoDetalhe.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BancoConfiguracaoRetornoSegmentoDetalhe other = (BancoConfiguracaoRetornoSegmentoDetalhe) obj;
		if (cdBancoConfiguracaoRetornoSegmentoDetalhe == null) {
			if (other.cdBancoConfiguracaoRetornoSegmentoDetalhe != null)
				return false;
		} else if (!cdBancoConfiguracaoRetornoSegmentoDetalhe
				.equals(other.cdBancoConfiguracaoRetornoSegmentoDetalhe))
			return false;
		return true;
	}	
}
