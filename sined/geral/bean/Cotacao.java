package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.Email;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_cotacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoFornecedorEmpresa;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@Entity
@SequenceGenerator(name="sq_cotacao",sequenceName="sq_cotacao")
@JoinEmpresa("cotacao.listaOrigem.solicitacaocompra.empresa")
public class Cotacao implements Log, PermissaoProjeto, PermissaoFornecedorEmpresa {
	
	protected Integer cdcotacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Aux_cotacao aux_cotacao;
	protected Date dtcancelamento;
	protected Date dtbaixa;
	protected Boolean considerarlocalunicoentrega;
	protected Empresa empresa;

	protected Set<Cotacaoorigem> listaOrigem = new ListSet<Cotacaoorigem>(Cotacaoorigem.class);
	protected List<Cotacaofornecedor> listaCotacaofornecedor = new ListSet<Cotacaofornecedor>(Cotacaofornecedor.class);
	protected Set<Ordemcompra> listaOrdemcompra = new ListSet<Ordemcompra>(Ordemcompra.class);
	
//	TRANSIENTES
	protected Fornecedor fornecedor;
	protected List<Cotacaofornecedoritem> lista;
	protected Localarmazenagem localarmazenagem;
	protected Double qtdesol;
	protected Material material;
	protected Boolean fromSolicitacao;
	protected List<Solicitacaocompra> listaSolicitacao;
	protected String idssolicitacao;
	protected List<Arquivo> listaArquivos = new ListSet<Arquivo>(Arquivo.class);
	protected Boolean isFornecedorMinimo = Boolean.FALSE;
	protected Boolean enviaCopia;
	
	protected String remetente;
	protected String assunto;
	protected String email;
	
	protected List<Material> listaMateriais = new ArrayList<Material>();
	
	public Cotacao() {}
	
	public Cotacao(Integer cdcotacao, List<Cotacaofornecedor> listaCotacaofornecedor){
		this.cdcotacao = cdcotacao;
		this.listaCotacaofornecedor = listaCotacaofornecedor;
	}

	public Cotacao(Integer cdcotacao){
		this.cdcotacao = cdcotacao;
	}
	
	@Id
	@GeneratedValue(generator="sq_cotacao",strategy=GenerationType.AUTO)
	public Integer getCdcotacao() {
		return cdcotacao;
	}
	public void setCdcotacao(Integer cdcotacao) {
		this.cdcotacao = cdcotacao;
	}
	
	@OneToMany(mappedBy="cotacao")
	public List<Cotacaofornecedor> getListaCotacaofornecedor() {
		return listaCotacaofornecedor;
	}
	
	public void setListaCotacaofornecedor(List<Cotacaofornecedor> listaCotacaofornecedor) {
		this.listaCotacaofornecedor = listaCotacaofornecedor;
	}
	
	@OneToMany(mappedBy="cotacao")
	public Set<Cotacaoorigem> getListaOrigem() {
		return listaOrigem;
	}
	
	public void setListaOrigem(Set<Cotacaoorigem> listaOrigem) {
		this.listaOrigem = listaOrigem;
	}
	
	@OneToMany(mappedBy="cotacao")
	public Set<Ordemcompra> getListaOrdemcompra() {
		return listaOrdemcompra;
	}
	
	public void setListaOrdemcompra(Set<Ordemcompra> listaOrdemcompra) {
		this.listaOrdemcompra = listaOrdemcompra;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcotacao", insertable=false, updatable=false)
	public Aux_cotacao getAux_cotacao() {
		return aux_cotacao;
	}
	
	public void setAux_cotacao(Aux_cotacao aux_cotacao) {
		this.aux_cotacao = aux_cotacao;
	}
	
	public Date getDtbaixa() {
		return dtbaixa;
	}
	
	public Date getDtcancelamento() {
		return dtcancelamento;
	}
	
	public void setDtbaixa(Date dtbaixa) {
		this.dtbaixa = dtbaixa;
	}
	
	public void setDtcancelamento(Date dtcancelamento) {
		this.dtcancelamento = dtcancelamento;
	}
	
	/* API */
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	/* TRANSIENTES */
	
	
	@Transient
	public String getMateriais() {		
		List<Cotacaofornecedor> lista = getListaCotacaofornecedor();
		Set<Cotacaofornecedoritem> listaMaterias = new ListSet<Cotacaofornecedoritem>(Cotacaofornecedoritem.class);
		for (Cotacaofornecedor cotacaofornecedor : lista) {
			listaMaterias.addAll(cotacaofornecedor.getListaCotacaofornecedoritem());
		}
		String listAndConcatenateSort = SinedUtil.listAndConcatenateSort(listaMaterias, "material.autocompleteDescription", "<BR>");
		if (listAndConcatenateSort.split("<BR>").length > 9) {
			return "Diversos"; 
		}
		return listAndConcatenateSort;
	}
	
	@Transient
	public String getMateriaisForCsv() {		
		List<Cotacaofornecedor> lista = getListaCotacaofornecedor();
		Set<Cotacaofornecedoritem> listaMaterias = new ListSet<Cotacaofornecedoritem>(Cotacaofornecedoritem.class);
		for (Cotacaofornecedor cotacaofornecedor : lista) {
			listaMaterias.addAll(cotacaofornecedor.getListaCotacaofornecedoritem());
		}
		String listAndConcatenateSort = SinedUtil.listAndConcatenateSort(listaMaterias, "material.autocompleteDescription", " / ");
		if (listAndConcatenateSort.split(" / ").length > 9) {
			return "Diversos"; 
		}
		if(listAndConcatenateSort.lastIndexOf(" / ") != -1){
			return listAndConcatenateSort.substring(0, listAndConcatenateSort.lastIndexOf(" / "));
		}
		return listAndConcatenateSort;
	}
	
	@Transient
	public String getLocais() {		
		List<Cotacaofornecedor> lista = getListaCotacaofornecedor();
		Set<Cotacaofornecedoritem> listaMaterias = new ListSet<Cotacaofornecedoritem>(Cotacaofornecedoritem.class);
		for (Cotacaofornecedor cotacaofornecedor : lista) {
			listaMaterias.addAll(cotacaofornecedor.getListaCotacaofornecedoritem());
		}
		String listAndConcatenateSort = SinedUtil.listAndConcatenateSort(listaMaterias, "localarmazenagem.nome", "<BR>");
		if (listAndConcatenateSort.split("<BR>").length > 9) {
			return "Diversos"; 
		}
		return listAndConcatenateSort;
	}
	
	@Transient
	public String getLocaisForCsv() {		
		List<Cotacaofornecedor> lista = getListaCotacaofornecedor();
		Set<Cotacaofornecedoritem> listaMaterias = new ListSet<Cotacaofornecedoritem>(Cotacaofornecedoritem.class);
		for (Cotacaofornecedor cotacaofornecedor : lista) {
			listaMaterias.addAll(cotacaofornecedor.getListaCotacaofornecedoritem());
		}
		String listAndConcatenateSort = SinedUtil.listAndConcatenateSort(listaMaterias, "localarmazenagem.nome", " / ");
		if (listAndConcatenateSort.split(" / ").length > 9) {
			return "Diversos"; 
		}
		if(listAndConcatenateSort.lastIndexOf(" / ") != -1){
			return listAndConcatenateSort.substring(0, listAndConcatenateSort.lastIndexOf(" / "));
		}
		return listAndConcatenateSort;
	}
	
	@Transient
	public List<Cotacaofornecedoritem> getLista() {
		return lista;
	}
	
	public void setLista(List<Cotacaofornecedoritem> lista) {
		this.lista = lista;
	}

	@Transient
	@DisplayName("Material/Servi�o")
	public Material getMaterial() {
		return material;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}
	@Transient
	@DisplayName("Local")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	@Transient
	@DisplayName("Qtd Sol.")
	public Double getQtdesol() {
		return qtdesol;
	}
	
	public void setQtdesol(Double qtdesol) {
		this.qtdesol = qtdesol;
	}
	
	@Transient
	public Boolean getFromSolicitacao() {
		return fromSolicitacao;
	}
	
	public void setFromSolicitacao(Boolean fromSolicitacao) {
		this.fromSolicitacao = fromSolicitacao;
	}
	
	@Transient
	public List<Solicitacaocompra> getListaSolicitacao() {
		return listaSolicitacao;
	}
	
	public void setListaSolicitacao(List<Solicitacaocompra> listaSolicitacao) {
		this.listaSolicitacao = listaSolicitacao;
	}
	
	@Transient
	public String getIdssolicitacao() {
		return idssolicitacao;
	}
	
	public void setIdssolicitacao(String idssolicitacao) {
		this.idssolicitacao = idssolicitacao;
	}
	
	@Transient
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	@Transient
	@MaxLength(50)
	@Email
	public String getRemetente() {
		return remetente;
	}
	
	public void setRemetente(String remetente) {
		this.remetente = remetente;
	}
	
	@Transient
	@MaxLength(50)
	public String getAssunto() {
		return assunto;
	}
	
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	
	@Transient
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Transient
	public List<Arquivo> getListaArquivos() {
		return listaArquivos;
	}
	
	public void setListaArquivos(List<Arquivo> listaArquivos) {
		this.listaArquivos = listaArquivos;
	}
	
	@Transient
	public Boolean getIsFornecedorMinimo() {
		return isFornecedorMinimo;
	}
	@Transient
	@DisplayName("Desejo receber c�pia")
	public Boolean getEnviaCopia() {
		return enviaCopia;
	}
	public void setEnviaCopia(Boolean enviaCopia) {
		this.enviaCopia = enviaCopia;
	}
	public void setIsFornecedorMinimo(Boolean isFornecedorMinimo) {
		this.isFornecedorMinimo = isFornecedorMinimo;
	}
	
	public Boolean getConsiderarlocalunicoentrega() {
		return considerarlocalunicoentrega;
	}

	public void setConsiderarlocalunicoentrega(Boolean considerarlocalunicoentrega) {
		this.considerarlocalunicoentrega = considerarlocalunicoentrega;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Transient
	public List<Material> getListaMateriais() {
		return listaMateriais;
	}

	public void setListaMateriais(List<Material> listaMateriais) {
		this.listaMateriais = listaMateriais;
	}

	public String subQueryProjeto() {
		return "select cotacaosubQueryProjeto.cdcotacao " +
				"from Cotacao cotacaosubQueryProjeto " +
				"join cotacaosubQueryProjeto.listaOrigem listaOrigemsubQueryProjeto " +
				"join listaOrigemsubQueryProjeto.solicitacaocompra solicitacaocomprasubQueryProjeto " +
				"left outer join solicitacaocomprasubQueryProjeto.projeto projetosubQueryProjeto " +
				"where (projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ") " +
				"or projetosubQueryProjeto.cdprojeto is null)";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdcotacao == null) ? 0 : cdcotacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cotacao other = (Cotacao) obj;
		if (cdcotacao == null) {
			if (other.cdcotacao != null)
				return false;
		} else if (!cdcotacao.equals(other.cdcotacao))
			return false;
		return true;
	}
	
	public String subQueryFornecedorEmpresa() {
		return "select cotacaoSubQueryFornecedorEmpresa.cdcotacao " +
				"from Cotacao cotacaoSubQueryFornecedorEmpresa " +
				"left outer join cotacaoSubQueryFornecedorEmpresa.listaCotacaofornecedor listaCotacaofornecedorSubQueryFornecedorEmpresa " +
				"left outer join listaCotacaofornecedorSubQueryFornecedorEmpresa.fornecedor fornecedorSubQueryFornecedorEmpresa " +
				"where true = permissao_fornecedorempresa(fornecedorSubQueryFornecedorEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}
}
