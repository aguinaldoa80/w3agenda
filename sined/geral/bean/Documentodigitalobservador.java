package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_documentodigitalobservador",sequenceName="sq_documentodigitalobservador")
public class Documentodigitalobservador {

	private Integer cddocumentodigitalobservador;
	private Documentodigital documentodigital;
	private String email;

	@Id
	@GeneratedValue(generator = "sq_documentodigitalobservador", strategy = GenerationType.AUTO)
	public Integer getCddocumentodigitalobservador() {
		return cddocumentodigitalobservador;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentodigital")
	public Documentodigital getDocumentodigital() {
		return documentodigital;
	}
	
	@Required
	@MaxLength(500)
	@DisplayName("E-mail")
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setCddocumentodigitalobservador(
			Integer cddocumentodigitalobservador) {
		this.cddocumentodigitalobservador = cddocumentodigitalobservador;
	}

	public void setDocumentodigital(Documentodigital documentodigital) {
		this.documentodigital = documentodigital;
	}

	
}
