package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_restricaocontratomaterial",sequenceName="sq_restricaocontratomaterial")
public class Restricaocontratomaterial{
	
	protected Integer cdrestricaocontratomaterial;
	protected Restricao restricao;
	protected Contratomaterial contratomaterial;
	protected Date dtbloqueio;
	protected Date dtdesbloqueio;
	

	@Id
	@GeneratedValue(generator="sq_restricaocontratomaterial",strategy=GenerationType.AUTO)
	public Integer getCdrestricaocontratomaterial() {
		return cdrestricaocontratomaterial;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrestricao")
	public Restricao getRestricao() {
		return restricao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratomaterial")
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	public Date getDtbloqueio() {
		return dtbloqueio;
	}
	public Date getDtdesbloqueio() {
		return dtdesbloqueio;
	}
	public void setCdrestricaocontratomaterial(Integer cdrestricaocontratomaterial) {
		this.cdrestricaocontratomaterial = cdrestricaocontratomaterial;
	}
	public void setRestricao(Restricao restricao) {
		this.restricao = restricao;
	}
	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}
	public void setDtbloqueio(Date dtbloqueio) {
		this.dtbloqueio = dtbloqueio;
	}
	public void setDtdesbloqueio(Date dtdesbloqueio) {
		this.dtdesbloqueio = dtdesbloqueio;
	}

}
