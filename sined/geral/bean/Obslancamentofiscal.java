package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Observa��o de Lan�amento Fiscal")
@SequenceGenerator(name = "sq_obslancamentofiscal", sequenceName = "sq_obslancamentofiscal")
public class Obslancamentofiscal implements Log {

	private Integer cdobslancamentofiscal;
	private String descricao;
	private Boolean ativo;
	
	private Timestamp dtaltera;
	private Integer cdusuarioaltera;
	
	@Id
	@DisplayName("C�digo")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_obslancamentofiscal")
	public Integer getCdobslancamentofiscal() {
		return cdobslancamentofiscal;
	}
	@MaxLength(255)
	@DisplayName("Descri��o")
	@Required
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setCdobslancamentofiscal(Integer cdobslancamentofiscal) {
		this.cdobslancamentofiscal = cdobslancamentofiscal;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdobslancamentofiscal == null) ? 0 : cdobslancamentofiscal
						.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Obslancamentofiscal other = (Obslancamentofiscal) obj;
		if (cdobslancamentofiscal == null) {
			if (other.cdobslancamentofiscal != null)
				return false;
		} else if (!cdobslancamentofiscal.equals(other.cdobslancamentofiscal))
			return false;
		return true;
	}
}
