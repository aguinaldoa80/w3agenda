package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@DisplayName("Campo obrigatório")
@SequenceGenerator(name = "sq_campoobrigatorio", sequenceName = "sq_campoobrigatorio")
public class Campoobrigatorio implements Log {
	
	protected Integer cdcampoobrigatorio;
	protected String path;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Campoobrigatorioitem> listaCampoobrigatorioitem = new ListSet<Campoobrigatorioitem>(Campoobrigatorioitem.class);
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_campoobrigatorio")
	public Integer getCdcampoobrigatorio() {
		return cdcampoobrigatorio;
	}

	@Required
	@MaxLength(300)
	public String getPath() {
		return path;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@DisplayName("Campo(s)")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="campoobrigatorio")
	public List<Campoobrigatorioitem> getListaCampoobrigatorioitem() {
		return listaCampoobrigatorioitem;
	}

	public void setCdcampoobrigatorio(Integer cdcampoobrigatorio) {
		this.cdcampoobrigatorio = cdcampoobrigatorio;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setListaCampoobrigatorioitem(
			List<Campoobrigatorioitem> listaCampoobrigatorioitem) {
		this.listaCampoobrigatorioitem = listaCampoobrigatorioitem;
	}

	
}
