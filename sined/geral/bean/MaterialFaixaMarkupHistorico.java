package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.types.Money;

@Entity
@SequenceGenerator(name="sq_materialfaixamarkuphistorico", sequenceName="sq_materialfaixamarkuphistorico")
public class MaterialFaixaMarkupHistorico {
	
	private Integer cdMaterialFaixaMarkupHistorico;
	private MaterialFaixaMarkup materialFaixaMarkup;
	private FaixaMarkupNome faixaMarkupNome;
	private Empresa empresa;
	private Double markup;
	private Money valorCusto;
	private Boolean isValorMinimo;
	private Boolean isValorIdeal;
	
	
	@Id
	@GeneratedValue(generator="sq_materialfaixamarkuphistorico", strategy=GenerationType.AUTO)
	public Integer getCdMaterialFaixaMarkupHistorico() {
		return cdMaterialFaixaMarkupHistorico;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialfaixamarkup")
	public MaterialFaixaMarkup getMaterialFaixaMarkup() {
		return materialFaixaMarkup;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfaixamarkupnome")
	public FaixaMarkupNome getFaixaMarkupNome() {
		return faixaMarkupNome;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public Double getMarkup() {
		return markup;
	}
	public Money getValorCusto() {
		return valorCusto;
	}
	public Boolean getIsValorMinimo() {
		return isValorMinimo;
	}
	public Boolean getIsValorIdeal() {
		return isValorIdeal;
	}
	
	
	public void setCdMaterialFaixaMarkupHistorico(
			Integer cdMaterialFaixaMarkupHistorico) {
		this.cdMaterialFaixaMarkupHistorico = cdMaterialFaixaMarkupHistorico;
	}
	public void setMaterialFaixaMarkup(MaterialFaixaMarkup materialFaixaMarkup) {
		this.materialFaixaMarkup = materialFaixaMarkup;
	}
	public void setFaixaMarkupNome(FaixaMarkupNome faixaMarkupNome) {
		this.faixaMarkupNome = faixaMarkupNome;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setMarkup(Double markup) {
		this.markup = markup;
	}
	public void setValorCusto(Money valorCusto) {
		this.valorCusto = valorCusto;
	}
	public void setIsValorMinimo(Boolean isValorMinimo) {
		this.isValorMinimo = isValorMinimo;
	}
	public void setIsValorIdeal(Boolean isValorIdeal) {
		this.isValorIdeal = isValorIdeal;
	}
}
