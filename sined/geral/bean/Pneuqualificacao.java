package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_pneuqualificacao", sequenceName="sq_pneuqualificacao")
@DisplayName("Qualificação do Pneu")
public class Pneuqualificacao implements Log {

	private Integer cdpneuqualificacao;
	private String nome;
	private Boolean garantia;
	private Boolean ativo = Boolean.TRUE;
	private Timestamp dtaltera;
	private Integer cdusuarioaltera;
	
	public Pneuqualificacao(){
	}
	
	public Pneuqualificacao(Integer cdpneuqualificacao){
		this.cdpneuqualificacao = cdpneuqualificacao;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pneuqualificacao")
	public Integer getCdpneuqualificacao() {
		return cdpneuqualificacao;
	}
	@DescriptionProperty
	@MaxLength(50)
	@Required
	public String getNome() {
		return nome;
	}
	@Required
	public Boolean getGarantia() {
		return garantia;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public void setCdpneuqualificacao(Integer cdpneuqualificacao) {
		this.cdpneuqualificacao = cdpneuqualificacao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setGarantia(Boolean garantia) {
		this.garantia = garantia;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}	
}