package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Tipo de item de inspe��o")
@SequenceGenerator(name = "sq_inspecaoitemtipo", sequenceName = "sq_inspecaoitemtipo")
public class Inspecaoitemtipo implements Log{

	protected Integer cdinspecaoitemtipo;
	protected String nome;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_inspecaoitemtipo")
	public Integer getCdinspecaoitemtipo() {
		return cdinspecaoitemtipo;
	}
	
	@Required
	@DescriptionProperty
	@MaxLength(100)
	public String getNome() {
		return nome;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setNome(String nome) {
		this.nome = StringUtils.trimToNull(nome);
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setCdinspecaoitemtipo(Integer cdinspecaoitemtipo) {
		this.cdinspecaoitemtipo = cdinspecaoitemtipo;
	}
	
}
