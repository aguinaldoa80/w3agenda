package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_agendamentoservico;
//import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.Agendamentoservicoitemapoio;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_agendamentoservico", sequenceName = "sq_agendamentoservico")
@DisplayName("Agenda de Atendimento")
public class Agendamentoservico implements Log {

	protected Integer cdagendamentoservico;
	protected Date data = new Date(System.currentTimeMillis());
	protected Date dtrealizado;
	protected Date dtcancelado;
	protected Empresa empresa;
	protected Cliente cliente;
	protected Requisicao requisicao;
	protected Material material;
	protected Colaborador colaborador;
	protected Escalahorario escalahorario;
	protected Aux_agendamentoservico aux_agendamentoservico;
	protected Integer sessao;
	protected Agendamentoservicodocumento agendamentoservicodocumento;
	protected Agendamentoservicocancela agendamentoservicocancela;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	protected List<Agendamentoservicohistorico> listaagendamentosservicohistorico = new ListSet<Agendamentoservicohistorico>(Agendamentoservicohistorico.class);
	
	//Transient's
	protected Boolean possuiUsoVariosDias = false;
	protected Boolean isHorarioDisponivel = true;
	protected Boolean isFeriado = false;
	protected Boolean isDomingo = false;
	protected Boolean possuiPendenciaRestricao = false;
	protected Date dtfim;
	protected Long contaPagarSoma;
	protected Long contaReceberSoma;
	protected Long valorRecebido;
	protected Long valorDevido;
	protected Long saldo;
	protected Date dataAux;
	protected String motivo;
	protected String descriptionHour;
	
	public Agendamentoservico(){
	}

	public Agendamentoservico(Date data, Escalahorario escalahorario, Boolean isDomingo){
		this.data = data;
		this.escalahorario = escalahorario;
		this.isDomingo = isDomingo;
	}

	public Agendamentoservico(Integer cdagendamentoservico){
		this.cdagendamentoservico = cdagendamentoservico;
	}

	public Agendamentoservico(Escalahorario escalahorario){
		this.escalahorario = escalahorario;
	}
	
	@Id
	@DescriptionProperty
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_agendamentoservico")
	public Integer getCdagendamentoservico() {
		return cdagendamentoservico;
	}
	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}
	@Required
	public Date getData() {
		return data;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Ordem de Servi�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrequisicao")
	public Requisicao getRequisicao() {
		return requisicao;
	}
	@DisplayName("Servi�o")
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservico")
	public Material getMaterial() {
		return material;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@Required
	@DisplayName("Hor�rio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdescalahorario")
	public Escalahorario getEscalahorario() {
		return escalahorario;
	}
	@OneToMany(mappedBy="agendamentoservico")
	public List<Agendamentoservicohistorico> getListaagendamentosservicohistorico() {
		return listaagendamentosservicohistorico;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdagendamentoservico", insertable=false, updatable=false)
	public Aux_agendamentoservico getAux_agendamentoservico() {
		return aux_agendamentoservico;
	}
	@DisplayName("Data realiza��o")
	public Date getDtrealizado() {
		return dtrealizado;
	}
	@DisplayName("Data cancelamento")
	public Date getDtcancelado() {
		return dtcancelado;
	}
	@DisplayName("Sess�o")
	public Integer getSessao() {
		return sessao;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdagendamentoservicodocumento")
	public Agendamentoservicodocumento getAgendamentoservicodocumento() {
		return agendamentoservicodocumento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdagendamentoservicocancela")
	public Agendamentoservicocancela getAgendamentoservicocancela() {
		return agendamentoservicocancela;
	}
	
//	public List<Agendamentoservicoitemapoio> getlistaagendamentoservicoitemapoio() {
//		return listaagendamentoservicoitemapoio;	
//	}
////	public Ausenciamotivo getAusenciamotivo(){
//////		return ausenciamotivo;
////	}
////	
////	public void setAusenciamotivo(Ausenciamotivo ausenciamotivo){
////		this.ausenciamotivo = ausenciamotivo;
////	}
//	public void setListaagendamentoservicoitemapoio(List<Agendamentoservicoitemapoio> listaagendamentoservicoitemapoio){
//		this.listaagendamentoservicoitemapoio = listaagendamentoservicoitemapoio;
//	}
//	
	public void setAgendamentoservicocancela(
			Agendamentoservicocancela agendamentoservicocancela) {
		this.agendamentoservicocancela = agendamentoservicocancela;
	}

	public void setAgendamentoservicodocumento(
			Agendamentoservicodocumento agendamentoservicodocumento) {
		this.agendamentoservicodocumento = agendamentoservicodocumento;
	}

	public void setSessao(Integer sessao) {
		this.sessao = sessao;
	}
	public void setListaagendamentosservicohistorico(
			List<Agendamentoservicohistorico> listaagendamentosservicohistorico) {
		this.listaagendamentosservicohistorico = listaagendamentosservicohistorico;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setRequisicao(Requisicao requisicao) {
		this.requisicao = requisicao;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setEscalahorario(Escalahorario escalahorario) {
		this.escalahorario = escalahorario;
	}
	public void setCdagendamentoservico(Integer cdagendamentoservico) {
		this.cdagendamentoservico = cdagendamentoservico;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}
	public void setAux_agendamentoservico(
			Aux_agendamentoservico aux_agendamentoservico) {
		this.aux_agendamentoservico = aux_agendamentoservico;
	}
	public void setDtrealizado(Date dtrealizado) {
		this.dtrealizado = dtrealizado;
	}
	public void setDtcancelado(Date dtcancelado) {
		this.dtcancelado = dtcancelado;
	}
	
	@Transient
	public Boolean getPossuiUsoVariosDias() {
		return possuiUsoVariosDias;
	}
	@Transient
	public Boolean getIsHorarioDisponivel() {
		return isHorarioDisponivel;
	}
	@Transient
	public Boolean getPossuiPendenciaRestricao() {
		return possuiPendenciaRestricao;
	}
	@Transient
	public Date getDtfim() {
		return dtfim;
	}
	@Transient
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public void setPossuiPendenciaRestricao(Boolean possuiPendenciaRestricao) {
		this.possuiPendenciaRestricao = possuiPendenciaRestricao;
	}
	public void setPossuiUsoVariosDias(Boolean possuiUsoVariosDias) {
		this.possuiUsoVariosDias = possuiUsoVariosDias;
	}
	public void setIsHorarioDisponivel(Boolean isHorarioDisponivel) {
		this.isHorarioDisponivel = isHorarioDisponivel;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
	@Transient
	public Long getContaPagarSoma() {
		return contaPagarSoma;
	}
	@Transient
	public Long getContaReceberSoma() {
		return contaReceberSoma;
	}
	@Transient
	public Long getValorRecebido() {
		return valorRecebido;
	}
	@Transient
	public Long getValorDevido() {
		return valorDevido;
	}
	@Transient
	public Long getSaldo() {
		return saldo;
	}
	@Transient
	public Date getDataAux() {
		return dataAux;
	}
	public void setDataAux(Date dataAux) {
		this.dataAux = dataAux;
	}
	public void setContaPagarSoma(Long contaPagarSoma) {
		this.contaPagarSoma = contaPagarSoma;
	}
	public void setContaReceberSoma(Long contaReceberSoma) {
		this.contaReceberSoma = contaReceberSoma;
	}
	public void setValorRecebido(Long valorRecebido) {
		this.valorRecebido = valorRecebido;
	}
	public void setValorDevido(Long valorDevido) {
		this.valorDevido = valorDevido;
	}
	public void setSaldo(Long saldo) {
		this.saldo = saldo;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Agendamentoservico)
			return ((Agendamentoservico)obj).cdagendamentoservico.equals(this.cdagendamentoservico);
		return super.equals(obj);
	}
	
	@Transient
	public Boolean getIsDomingo() {
		return isDomingo;
	}
	@Transient
	public Boolean getIsFeriado() {
		return isFeriado;
	}
	public void setIsDomingo(Boolean isDomingo) {
		this.isDomingo = isDomingo;
	}
	public void setIsFeriado(Boolean isFeriado) {
		this.isFeriado = isFeriado;
	}
	
	@Transient
	public String getDescriptionHour() {
		if(escalahorario != null)
			return escalahorario.getHorainicio() + " - " + escalahorario.getHorafim();
		else
			return null;
	}
	
}
