package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Finalidadenfe;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_naturezaoperacao", sequenceName = "sq_naturezaoperacao")
@DisplayName("Natureza da Opera��o")
public class Naturezaoperacao implements Log {

	public static final Naturezaoperacao TRIBUTACAO_NO_MUNICIPIO = new Naturezaoperacao(1);
	public static final Naturezaoperacao TRIBUTACAO_FORA_MUNICIPIO = new Naturezaoperacao(2);
	public static final Naturezaoperacao ISENCAO = new Naturezaoperacao(3);
	public static final Naturezaoperacao IMUNE = new Naturezaoperacao(4);
	public static final Naturezaoperacao EXIGIBILIDADE_SUSPENSA_JUDICIAL = new Naturezaoperacao(5);
	public static final Naturezaoperacao EXIGIBILIDADE_SUSPENSA_ADMINISTRATIVO = new Naturezaoperacao(6);
	
	protected Integer cdnaturezaoperacao;
	protected String nome;
	protected String descricao;
	protected String codigonfse;
	protected Operacaocontabil operacaocontabilavista;
	protected Operacaocontabil operacaocontabilaprazo;
	protected Operacaocontabil operacaocontabilpagamento;
	protected NotaTipo notaTipo;
	protected String infoadicionalfisco;
	protected String infoadicionalcontrib;
	protected Boolean padrao;
	protected Boolean simplesremessa;
	protected Boolean ativo = Boolean.TRUE;
	protected List<Naturezaoperacaocfop> listaNaturezaoperacaocfop;
	protected List<Naturezaoperacaocst> listaNaturezaoperacaocst;
	protected Boolean obrigarNumeroPedido = Boolean.FALSE;
	protected Operacaonfe operacaonfe;
	protected Finalidadenfe finalidadenfe;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Boolean retornoindustrializacao;
	protected ReportTemplateBean templateinfcontribuinte;
	protected ReportTemplateBean templateinfproduto;
	protected String razaosocial;
	protected Boolean naoVincularMaterialFornecedor;
	
	public Naturezaoperacao() {
	}
	
	public Naturezaoperacao(Integer cdnaturezaoperacao) {
		this.cdnaturezaoperacao = cdnaturezaoperacao;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_naturezaoperacao")
	public Integer getCdnaturezaoperacao() {
		return cdnaturezaoperacao;
	}
	@Required
	@MaxLength(100)
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	@Required
	@MaxLength(100)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	@MaxLength(50)
	@DisplayName("C�digo para NFS-e")
	public String getCodigonfse() {
		return codigonfse;
	}
	@DisplayName("Opera��o Cont�bil - A vista")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdoperacaocontabilavista")
	public Operacaocontabil getOperacaocontabilavista() {
		return operacaocontabilavista;
	}
	@DisplayName("Opera��o Cont�bil - A prazo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdoperacaocontabilaprazo")
	public Operacaocontabil getOperacaocontabilaprazo() {
		return operacaocontabilaprazo;
	}
	@DisplayName("Opera��o Cont�bil - Pagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdoperacaocontabilpagamento")
	public Operacaocontabil getOperacaocontabilpagamento() {
		return operacaocontabilpagamento;
	}
	@Required
	@DisplayName("Aplica��o em")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnotatipo")
	public NotaTipo getNotaTipo() {
		return notaTipo;
	}
	@DisplayName("Informa��es Adicionais de Interesse do Contribuinte")
	@MaxLength(5000)
	public String getInfoadicionalcontrib() {
		return infoadicionalcontrib;
	}
	@DisplayName("Informa��es Adicionais de Interesse do Fisco")
	@MaxLength(5000)
	public String getInfoadicionalfisco() {
		return infoadicionalfisco;
	}
	@DisplayName("Padr�o")
	public Boolean getPadrao() {
		return padrao;
	}
	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	@DisplayName("CFOP")
	@OneToMany(mappedBy="naturezaoperacao", fetch=FetchType.LAZY)
	public List<Naturezaoperacaocfop> getListaNaturezaoperacaocfop() {
		return listaNaturezaoperacaocfop;
	}
	@DisplayName("CST")
	@OneToMany(mappedBy="naturezaoperacao")
	public List<Naturezaoperacaocst> getListaNaturezaoperacaocst() {
		return listaNaturezaoperacaocst;
	}
    @DisplayName("Obrigar N�mero do Pedido/Item do Pedido")
    public Boolean getObrigarNumeroPedido() {
		return obrigarNumeroPedido;
	}
    @DisplayName("N�o gera receita / despesa")
    public Boolean getSimplesremessa() {
		return simplesremessa;
	}
	    @DisplayName("Consumidor final?")
    public Operacaonfe getOperacaonfe() {
		return operacaonfe;
	}
    @DisplayName("Finalidade NF-e")
    public Finalidadenfe getFinalidadenfe() {
		return finalidadenfe;
	}
    public Boolean getNaoVincularMaterialFornecedor() {
		return naoVincularMaterialFornecedor;
	}
    public void setFinalidadenfe(Finalidadenfe finalidadenfe) {
		this.finalidadenfe = finalidadenfe;
	}   
    public void setOperacaonfe(Operacaonfe operacaonfe) {
		this.operacaonfe = operacaonfe;
	}
    @DisplayName("Retorno de industrializa��o")
    public Boolean getRetornoindustrializacao() {
		return retornoindustrializacao;
	}
    public void setSimplesremessa(Boolean simplesremessa) {
		this.simplesremessa = simplesremessa;
	}
	public void setCdnaturezaoperacao(Integer cdnaturezaoperacao) {
		this.cdnaturezaoperacao = cdnaturezaoperacao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCodigonfse(String codigonfse) {
		this.codigonfse = codigonfse;
	}
	public void setOperacaocontabilavista(Operacaocontabil operacaocontabilavista) {
		this.operacaocontabilavista = operacaocontabilavista;
	}
	public void setOperacaocontabilaprazo(Operacaocontabil operacaocontabilaprazo) {
		this.operacaocontabilaprazo = operacaocontabilaprazo;
	}
	public void setOperacaocontabilpagamento(Operacaocontabil operacaocontabilpagamento) {
		this.operacaocontabilpagamento = operacaocontabilpagamento;
	}
	public void setNotaTipo(NotaTipo notaTipo) {
		this.notaTipo = notaTipo;
	}
	public void setInfoadicionalcontrib(String infoadicionalcontrib) {
		this.infoadicionalcontrib = infoadicionalcontrib;
	}
	public void setInfoadicionalfisco(String infoadicionalfisco) {
		this.infoadicionalfisco = infoadicionalfisco;
	}
	public void setPadrao(Boolean padrao) {
		this.padrao = padrao;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setListaNaturezaoperacaocfop(List<Naturezaoperacaocfop> listaNaturezaoperacaocfop) {
		this.listaNaturezaoperacaocfop = listaNaturezaoperacaocfop;
	}
	public void setListaNaturezaoperacaocst(List<Naturezaoperacaocst> listaNaturezaoperacaocst) {
		this.listaNaturezaoperacaocst = listaNaturezaoperacaocst;
	}
    public void setObrigarNumeroPedido(
			Boolean obrigarNumeroPedido) {
		this.obrigarNumeroPedido = obrigarNumeroPedido;
	}
    public void setRetornoindustrializacao(Boolean retornoindustrializacao) {
		this.retornoindustrializacao = retornoindustrializacao;
	}
    public void setNaoVincularMaterialFornecedor(Boolean naoVincularMaterialFornecedor) {
		this.naoVincularMaterialFornecedor = naoVincularMaterialFornecedor;
	}
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdnaturezaoperacao == null) ? 0 : cdnaturezaoperacao
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Naturezaoperacao other = (Naturezaoperacao) obj;
		if (cdnaturezaoperacao == null) {
			if (other.cdnaturezaoperacao != null)
				return false;
		} else if (!cdnaturezaoperacao.equals(other.cdnaturezaoperacao))
			return false;
		return true;
	}

	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;		
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@JoinColumn(name="cdreporttemplateinfcontribuinte")
	@ManyToOne(fetch=FetchType.LAZY)
	public ReportTemplateBean getTemplateinfcontribuinte() {
		return templateinfcontribuinte;
	}
	public void setTemplateinfcontribuinte(
			ReportTemplateBean templateinfcontribuinte) {
		this.templateinfcontribuinte = templateinfcontribuinte;
	}

	@JoinColumn(name="cdreporttemplateinfproduto")
	@ManyToOne(fetch=FetchType.LAZY)
	public ReportTemplateBean getTemplateinfproduto() {
		return templateinfproduto;
	}

	public void setTemplateinfproduto(ReportTemplateBean templateinfproduto) {
		this.templateinfproduto = templateinfproduto;
	}
	
	@MaxLength(200)
	@DisplayName("Raz�o social")
	public String getRazaosocial() {
		return razaosocial;
	}
	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}
	
	@Transient
	public String getNomeCompleto() {
		return this.getNome();
	}
	
	@Transient
	@DescriptionProperty(usingFields={"nome"})
	public String getAutocompleteDescription(){
	return this.nome;	
	}
		
}