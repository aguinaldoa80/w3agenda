package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tempo;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.DiarioObraSubReportBean;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;


@Entity
@DisplayName("Di�rio de Obra")
@SequenceGenerator(name = "sq_diarioobra", sequenceName = "sq_diarioobra")
public class Diarioobra implements Log, PermissaoProjeto{

	protected Integer cddiarioobra;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Date dtprazocontratual;
	protected Integer diasdecorridos;
	protected Integer diasrestantes;
	protected Date dtdia;
	protected Projeto projeto;
	protected Tempo tempomanha;
	protected Tempo tempotarde;
	protected Tempo temponoite;
	protected Integer diasatraso;
	protected Date dtprorrogacao;
	
	protected Hora hrinicioparalizacaomanha;
	protected Hora hrfimparalizacaomanha;
	protected Hora hrinicioparalizacaotarde;
	protected Hora hrfimparalizacaotarde;
	protected Hora hrinicioparalizacaonoite;
	protected Hora hrfimparalizacaonoite;
	
	protected List<Diarioobramdo> listaMaodeobra;
	protected List<Diarioobramaterial> listMaterial;
	protected List<Diarioobraarquivo> listaDiarioobraarquivo = new ListSet<Diarioobraarquivo>(Diarioobraarquivo.class);
	
	//transient
	
	protected List<Atividade> listaAtividade;
	protected List<DiarioObraSubReportBean> listaCargo;
	protected List<DiarioObraSubReportBean> listaMaterial;
	
	
	protected Integer totalMaterial;
	protected Integer totalCargo;
	
	@Transient
	@DisplayName("Atvidades")
	public List<Atividade> getListaAtividade() {
		return listaAtividade;
	}

	public void setListaAtividade(List<Atividade> listaAtividade) {
		this.listaAtividade = listaAtividade;
	}

	@Transient
	public List<DiarioObraSubReportBean> getListaCargo() {
		return listaCargo;
	}

	public void setListaCargo(List<DiarioObraSubReportBean> listaCargo) {
		this.listaCargo = listaCargo;
	}

	@Transient
	public List<DiarioObraSubReportBean> getListaMaterial() {
		return listaMaterial;
	}

	public void setListaMaterial(List<DiarioObraSubReportBean> listaMaterial) {
		this.listaMaterial = listaMaterial;
	}

	// ======================================
	
	@Id
	@GeneratedValue(generator="sq_diarioobra", strategy=GenerationType.AUTO)
	public Integer getCddiarioobra() {
		return cddiarioobra;
	}
	
	@DisplayName("Tempo manh�")
	public Tempo getTempomanha() {
		return tempomanha;
	}
	
	@DisplayName("Tempo tarde")
	public Tempo getTempotarde() {
		return tempotarde;
	}
	
	@DisplayName("Tempo noite")
	public Tempo getTemponoite() {
		return temponoite;
	}
	
	public Hora getHrinicioparalizacaomanha() {
		return hrinicioparalizacaomanha;
	}

	public Hora getHrfimparalizacaomanha() {
		return hrfimparalizacaomanha;
	}

	public Hora getHrinicioparalizacaotarde() {
		return hrinicioparalizacaotarde;
	}

	public Hora getHrfimparalizacaotarde() {
		return hrfimparalizacaotarde;
	}

	public Hora getHrinicioparalizacaonoite() {
		return hrinicioparalizacaonoite;
	}

	public Hora getHrfimparalizacaonoite() {
		return hrfimparalizacaonoite;
	}
	
	@OneToMany(mappedBy="diarioobra")
	@DisplayName("Arquivos")
	public List<Diarioobraarquivo> getListaDiarioobraarquivo() {
		return listaDiarioobraarquivo;
	}

	public void setListaDiarioobraarquivo(
			List<Diarioobraarquivo> listaDiarioobraarquivo) {
		this.listaDiarioobraarquivo = listaDiarioobraarquivo;
	}

	public void setHrinicioparalizacaomanha(Hora hrinicioparalizacaomanha) {
		this.hrinicioparalizacaomanha = hrinicioparalizacaomanha;
	}

	public void setHrfimparalizacaomanha(Hora hrfimparalizacaomanha) {
		this.hrfimparalizacaomanha = hrfimparalizacaomanha;
	}

	public void setHrinicioparalizacaotarde(Hora hrinicioparalizacaotarde) {
		this.hrinicioparalizacaotarde = hrinicioparalizacaotarde;
	}

	public void setHrfimparalizacaotarde(Hora hrfimparalizacaotarde) {
		this.hrfimparalizacaotarde = hrfimparalizacaotarde;
	}

	public void setHrinicioparalizacaonoite(Hora hrinicioparalizacaonoite) {
		this.hrinicioparalizacaonoite = hrinicioparalizacaonoite;
	}

	public void setHrfimparalizacaonoite(Hora hrfimparalizacaonoite) {
		this.hrfimparalizacaonoite = hrfimparalizacaonoite;
	}

	public void setTempomanha(Tempo tempomanha) {
		this.tempomanha = tempomanha;
	}
	
	public void setTempotarde(Tempo tempotarde) {
		this.tempotarde = tempotarde;
	}
	
	public void setTemponoite(Tempo temponoite) {
		this.temponoite = temponoite;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	@Required
	@MaxLength(50)
	@DisplayName("Dia")
	public Date getDtdia() {
		return dtdia;
	}

	public void setCddiarioobra(Integer cddiarioobra) {
		this.cddiarioobra = cddiarioobra;
	}
	
	public void setDtdia(Date dtdia) {
		this.dtdia = dtdia;
	}

	@DisplayName("Prazo contratual")
	public Date getDtprazocontratual() {
		return dtprazocontratual;
	}

	public void setDtprazocontratual(Date dtprazocontratual) {
		this.dtprazocontratual = dtprazocontratual;
	}
	
	@DisplayName("Dias decorridos")
	public Integer getDiasdecorridos() {
		return diasdecorridos;
	}
	
	public void setDiasdecorridos(Integer diasdecorridos) {
		this.diasdecorridos = diasdecorridos;
	}
	
	@DisplayName("Dias restantes")
	public Integer getDiasrestantes() {
		return diasrestantes;
	}
	
	public void setDiasrestantes(Integer diasrestantes) {
		this.diasrestantes = diasrestantes;
	}
	
	@DisplayName("Dias de atraso")
	public Integer getDiasatraso() {
		return diasatraso;
	}
	
	@DisplayName("Prorroga��o")
	public Date getDtprorrogacao() {
		return dtprorrogacao;
	}	
	
	public void setDiasatraso(Integer diasatraso) {
		this.diasatraso = diasatraso;
	}
	
	public void setDtprorrogacao(Date dtprorrogacao) {
		this.dtprorrogacao = dtprorrogacao;
	}	
	
	@DisplayName ("M�o de Obra")
	@OneToMany (mappedBy="diarioobra")
	public List<Diarioobramdo> getListaMaodeobra() {
		return listaMaodeobra;
	}

	public void setListaMaodeobra(List<Diarioobramdo> listaMaodeobra) {
		this.listaMaodeobra = listaMaodeobra;
	}
	
	@DisplayName("Material")
	@OneToMany (mappedBy="diarioobra")
	public List<Diarioobramaterial> getListMaterial() {
		return listMaterial;
	}

	public void setListMaterial(List<Diarioobramaterial> listMaterial) {
		this.listMaterial = listMaterial;
	}

	//Log
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public Integer getTotalMaterial() {
		return totalMaterial;
	}
	
	@Transient
	public Integer getTotalCargo() {
		return totalCargo;
	}

	public void setTotalMaterial(Integer totalMaterial) {
		this.totalMaterial = totalMaterial;
	}

	public void setTotalCargo(Integer totalCargo) {
		this.totalCargo = totalCargo;
	}	
	
	
	public String subQueryProjeto() {
		return "select diarioobrasubQueryProjeto.cddiarioobra " +
				"from Diarioobra diarioobrasubQueryProjeto " +
				"join diarioobrasubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
	
	
	
}
