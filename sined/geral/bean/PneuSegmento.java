package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_pneusegmento", sequenceName = "sq_pneusegmento")
public class PneuSegmento implements Log{
	protected Integer cdPneuSegmento;
	protected String nome;
	protected Boolean tipoPneuOtr;
	protected Boolean marca;
	protected Boolean descricao;
	protected Boolean lonas;
	protected Boolean modelo;
	protected Boolean medida;
	protected Boolean qualificacaoPneu;
	protected Boolean serieFogo;
	protected Boolean dot;
	protected Boolean numeroReformas;
	protected Boolean bandaCamelbak;
	protected Boolean ativo;
	protected Boolean principal;
	protected Boolean otr;
	protected Boolean acompanhaRoda;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	
	public PneuSegmento(){
		
	}
	
	public PneuSegmento(Integer cdPneuSegmento){
		this.cdPneuSegmento = cdPneuSegmento;
	}
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pneusegmento")
	public Integer getCdPneuSegmento() {
		return cdPneuSegmento;
	}
	
	public void setCdPneuSegmento(Integer cdPneuSegmento) {
		this.cdPneuSegmento = cdPneuSegmento;
	}
	
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Boolean getMarca() {
		return marca;
	}
	public void setMarca(Boolean marca) {
		this.marca = marca;
	}
	
	@DisplayName("DESCRI��O")
	public Boolean getDescricao() {
		return descricao;
	}
	public void setDescricao(Boolean descricao) {
		this.descricao = descricao;
	}
	public Boolean getLonas() {
		return lonas;
	}
	public void setLonas(Boolean lonas) {
		this.lonas = lonas;
	}
	public Boolean getModelo() {
		return modelo;
	}
	public void setModelo(Boolean modelo) {
		this.modelo = modelo;
	}
	public Boolean getMedida() {
		return medida;
	}
	public void setMedida(Boolean medida) {
		this.medida = medida;
	}

	public Boolean getDot() {
		return dot;
	}
	public void setDot(Boolean dot) {
		this.dot = dot;
	}
	
	@DisplayName("TIPO DE PNEU OTR")
	public Boolean getTipoPneuOtr() {
		return tipoPneuOtr;
	}

	public void setTipoPneuOtr(Boolean tipoPneuOtr) {
		this.tipoPneuOtr = tipoPneuOtr;
	}
	
	@DisplayName("QUALIFICA��O DO PNEU")
	public Boolean getQualificacaoPneu() {
		return qualificacaoPneu;
	}

	public void setQualificacaoPneu(Boolean qualificacaoPneu) {
		this.qualificacaoPneu = qualificacaoPneu;
	}
	@DisplayName("S�RIE/FOGO")
	public Boolean getSerieFogo() {
		return serieFogo;
	}
	public void setSerieFogo(Boolean serieFogo) {
		this.serieFogo = serieFogo;
	}
	
	@DisplayName("N�MERO DE REFORMAS")
	public Boolean getNumeroReformas() {
		return numeroReformas;
	}
	public void setNumeroReformas(Boolean numeroReformas) {
		this.numeroReformas = numeroReformas;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	@DisplayName("BANDA/CAMELBACK")
	public Boolean getBandaCamelbak() {
		return bandaCamelbak;
	}

	public void setBandaCamelbak(Boolean bandaCamelbak) {
		this.bandaCamelbak = bandaCamelbak;
	}

	public Boolean getPrincipal() {
		return principal;
	}

	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}
	
	@DisplayName("OTR")
	public Boolean getOtr() {
		return otr;
	}

	public void setOtr(Boolean otr) {
		this.otr = otr;
	}

	@DisplayName("Acompanha roda?")
	public Boolean getAcompanhaRoda() {
		return acompanhaRoda;
	}
			
	public void setAcompanhaRoda(Boolean acompanhaRoda) {
		this.acompanhaRoda = acompanhaRoda;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
		
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
		
	}
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
}
