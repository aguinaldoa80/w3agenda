package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.AjusteIcmsOrigemProcesso;


@Entity
@SequenceGenerator(name = "sq_ajusteicmsprocesso", sequenceName = "sq_ajusteicmsprocesso")
public class Ajusteicmsprocesso {

	protected Integer cdajusteicmsprocesso;
	protected Ajusteicms ajusteicms;
	protected String numerodocumento;
	protected String numeroprocesso;
	protected AjusteIcmsOrigemProcesso origemprocesso;
	protected String descricao;
	protected String descricaocomplementar;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ajusteicmsprocesso")
	public Integer getCdajusteicmsprocesso() {
		return cdajusteicmsprocesso;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdajusteicms")
	public Ajusteicms getAjusteicms() {
		return ajusteicms;
	}

	@MaxLength(30)
	@DisplayName("N�mero documento de arrecada��o estadual")
	public String getNumerodocumento() {
		return numerodocumento;
	}

	@MaxLength(15)
	@DisplayName("N�mero do processo")
	public String getNumeroprocesso() {
		return numeroprocesso;
	}

	@DisplayName("Origem do processo")
	public AjusteIcmsOrigemProcesso getOrigemprocesso() {
		return origemprocesso;
	}

	@MaxLength(500)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}

	@MaxLength(500)
	@DisplayName("Descri��o complementar")
	public String getDescricaocomplementar() {
		return descricaocomplementar;
	}

	public void setCdajusteicmsprocesso(Integer cdajusteicmsprocesso) {
		this.cdajusteicmsprocesso = cdajusteicmsprocesso;
	}

	public void setAjusteicms(Ajusteicms ajusteicms) {
		this.ajusteicms = ajusteicms;
	}

	public void setNumerodocumento(String numerodocumento) {
		this.numerodocumento = numerodocumento;
	}

	public void setNumeroprocesso(String numeroprocesso) {
		this.numeroprocesso = numeroprocesso;
	}

	public void setOrigemprocesso(AjusteIcmsOrigemProcesso origemprocesso) {
		this.origemprocesso = origemprocesso;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setDescricaocomplementar(String descricaocomplementar) {
		this.descricaocomplementar = descricaocomplementar;
	}

}
