package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_bandarede", sequenceName = "sq_bandarede")
public class Bandarede implements Log{

	protected Integer cdbandarede;
	protected Integer valor;
	protected Integer valorqueue;
	protected Unidademedida unidademedida;
	protected Unidademedida unidademedidaqueue;
	protected Boolean ativo = true;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bandarede")
	public Integer getCdbandarede() {
		return cdbandarede;
	}

	@Required
	@DisplayName("Banda")
	@MaxLength(8)
	@DescriptionProperty
	public Integer getValor() {
		return valor;
	}

	@Required
	@DisplayName("Velocidade")	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	@Required	
	@MaxLength(8)
	@DisplayName("Queue")
	public Integer getValorqueue() {
		return valorqueue;
	}
	
	@Required
	@DisplayName("Tamanho")	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedidaqueue")
	public Unidademedida getUnidademedidaqueue() {
		return unidademedidaqueue;
	}
	
	@Required
	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setCdbandarede(Integer cdbandarede) {
		this.cdbandarede = cdbandarede;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

	public void setValorqueue(Integer valorqueue) {
		this.valorqueue = valorqueue;		
	}

	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}

	public void setUnidademedidaqueue(Unidademedida unidademedidaqueue) {
		this.unidademedidaqueue = unidademedidaqueue;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}
}
