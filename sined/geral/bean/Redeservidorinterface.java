package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_redeservidorinterface", sequenceName = "sq_redeservidorinterface")
public class Redeservidorinterface{

	protected Integer cdredeservidorinterface;
	protected Rede rede;
	protected Servidorinterface servidorinterface;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_redeservidorinterface")
	public Integer getCdredeservidorinterface() {
		return cdredeservidorinterface;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrede")
	public Rede getRede() {
		return rede;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservidorinterface")
	@DisplayName("Servidor interface")
	public Servidorinterface getServidorinterface() {
		return servidorinterface;
	}

	public void setCdredeservidorinterface(Integer cdredeservidorinterface) {
		this.cdredeservidorinterface = cdredeservidorinterface;
	}

	public void setRede(Rede rede) {
		this.rede = rede;
	}

	public void setServidorinterface(Servidorinterface servidorinterface) {
		this.servidorinterface = servidorinterface;
	}
	
	
}
