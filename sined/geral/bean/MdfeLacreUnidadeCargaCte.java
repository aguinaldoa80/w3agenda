package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_mdfelacreunidadecargacte", sequenceName = "sq_mdfelacreunidadecargacte")
public class MdfeLacreUnidadeCargaCte {

	protected Integer cdMdfeLacreUnidadeCargaCte;
	protected Mdfe mdfe;
	protected String numeroLacre;
	protected String idUnidadeCarga;
	protected String idLacre;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfelacreunidadecargacte")
	public Integer getCdMdfeLacreUnidadeCargaCte() {
		return cdMdfeLacreUnidadeCargaCte;
	}
	public void setCdMdfeLacreUnidadeCargaCte(Integer cdMdfeLacreUnidadeCarga) {
		this.cdMdfeLacreUnidadeCargaCte = cdMdfeLacreUnidadeCarga;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@Required
	@MaxLength(value=20)
	@DisplayName("N�mero do lacre")
	public String getNumeroLacre() {
		return numeroLacre;
	}
	public void setNumeroLacre(String numeroLacre) {
		this.numeroLacre = numeroLacre;
	}
	
	@DisplayName("ID da unidade de carga")
	public String getIdUnidadeCarga() {
		return idUnidadeCarga;
	}
	public void setIdUnidadeCarga(String idUnidadeCarga) {
		this.idUnidadeCarga = idUnidadeCarga;
	}
	
	@DisplayName("ID do lacre")
	public String getIdLacre() {
		return idLacre;
	}
	public void setIdLacre(String idLacre) {
		this.idLacre = idLacre;
	}
	
	@Transient
	public List<MdfeLacreUnidadeCargaCte> getListaLacreUnidadeCargaCte(MdfeCte mdfeCte, MdfeInformacaoUnidadeCargaCte infUnTransCte, Mdfe mdfe) {
		List<MdfeLacreUnidadeCargaCte> list = new ArrayList<MdfeLacreUnidadeCargaCte>();
		if(SinedUtil.isListNotEmpty(mdfe.getListaInformacaoUnidadeCargaCte())){
			for(MdfeLacreUnidadeCargaCte item : mdfe.getListaLacreUnidadeCargaCte()){
				if(item.getIdUnidadeCarga().equals(infUnTransCte.getIdUnidadeTransporte())){
					list.add(item);
				}
			}
		}
		return list;
	}
}