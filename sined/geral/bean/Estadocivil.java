package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@SequenceGenerator(name = "sq_estadocivil", sequenceName = "sq_estadocivil")
public class Estadocivil implements Log{

	public static final Estadocivil CASADO = new Estadocivil(1);
	public static final Estadocivil SOLTEIRO = new Estadocivil(2);
	public static final Estadocivil VIUVO = new Estadocivil(3);
	public static final Estadocivil SEPARADO_JUDICIALMENTE = new Estadocivil(4);
	public static final Estadocivil DIVORCIADO = new Estadocivil(5);
	public static final Estadocivil DESQUITADO = new Estadocivil(6);
	public static final Estadocivil AMASIADO = new Estadocivil(7);
	public static final Estadocivil OUTROS = new Estadocivil(8);
	
	protected Integer cdestadocivil;
	protected String nome;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;

	public Estadocivil() {
	}
	public Estadocivil(Integer cdestadocivil) {
		this.cdestadocivil = cdestadocivil;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_estadocivil")
	public Integer getCdestadocivil() {
		return cdestadocivil;
	}
	public void setCdestadocivil(Integer id) {
		this.cdestadocivil = id;
	}

	
	@Required
	@MaxLength(25)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdestadocivil == null) ? 0 : cdestadocivil.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Estadocivil other = (Estadocivil) obj;
		if (cdestadocivil == null) {
			if (other.cdestadocivil != null)
				return false;
		} else if (!cdestadocivil.equals(other.cdestadocivil))
			return false;
		return true;
	}
	
}
