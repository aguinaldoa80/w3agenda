package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_materialformulavalorvenda", sequenceName = "sq_materialformulavalorvenda")
public class Materialformulavalorvenda {

	protected Integer cdmaterialformulavalorvenda;
	protected Material material;
	protected Integer ordem;
	protected String identificador;
	protected String formula;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialformulavalorvenda")
	public Integer getCdmaterialformulavalorvenda() {
		return cdmaterialformulavalorvenda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@Required
	public Integer getOrdem() {
		return ordem;
	}
	@Required
	@MaxLength(5)
	public String getIdentificador() {
		return identificador;
	}
	@Required
	@DisplayName("F�rmula")
	public String getFormula() {
		return formula;
	}

	public void setCdmaterialformulavalorvenda(Integer cdmaterialformulavalorvenda) {
		this.cdmaterialformulavalorvenda = cdmaterialformulavalorvenda;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setFormula(String formula) {
		this.formula = formula;
	}
}
