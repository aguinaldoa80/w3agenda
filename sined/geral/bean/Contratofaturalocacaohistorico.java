package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Contratofaturalocacaoacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;


@Entity
@SequenceGenerator(name = "sq_contratofaturalocacaohistorico", sequenceName = "sq_contratofaturalocacaohistorico")
public class Contratofaturalocacaohistorico implements Log {

	protected Integer cdcontratofaturalocacaohistorico;
	protected Contratofaturalocacao contratofaturalocacao;
	protected Contratofaturalocacaoacao acao;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	// TRANSIENTE
	protected String whereIn;
	protected Boolean cancelamentoDocumento = Boolean.FALSE;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratofaturalocacaohistorico")
	public Integer getCdcontratofaturalocacaohistorico() {
		return cdcontratofaturalocacaohistorico;
	}
	
	@JoinColumn(name="cdcontratofaturalocacao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contratofaturalocacao getContratofaturalocacao() {
		return contratofaturalocacao;
	}
	
	@DisplayName("A��o")
	public Contratofaturalocacaoacao getAcao() {
		return acao;
	}

	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setAcao(Contratofaturalocacaoacao acao) {
		this.acao = acao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdcontratofaturalocacaohistorico(
			Integer cdcontratofaturalocacaohistorico) {
		this.cdcontratofaturalocacaohistorico = cdcontratofaturalocacaohistorico;
	}

	public void setContratofaturalocacao(Contratofaturalocacao contratofaturalocacao) {
		this.contratofaturalocacao = contratofaturalocacao;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}

	@Transient
	public String getWhereIn() {
		return whereIn;
	}

	@Transient
	public Boolean getCancelamentoDocumento() {
		return cancelamentoDocumento;
	}

	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

	public void setCancelamentoDocumento(Boolean cancelamentoDocumento) {
		this.cancelamentoDocumento = cancelamentoDocumento;
	}
	
	

}
