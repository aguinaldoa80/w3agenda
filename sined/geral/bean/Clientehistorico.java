
package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.AtividadetipoSituacaoEnum;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedDateUtils;

@Entity
@SequenceGenerator(name = "sq_clientehistorico", sequenceName = "sq_clientehistorico")
public class Clientehistorico implements Log {

	protected Integer cdclientehistorico;
	protected String observacao;
	protected Cliente cliente;
	protected Contato contato;
	protected Meiocontato meiocontato;
	protected Contrato contrato;
	protected Atividadetipo atividadetipo;
	protected AtividadetipoSituacaoEnum situacao;
	protected Situacaohistorico situacaohistorico;
	protected Empresa empresahistorico;
	 
	protected Usuario usuarioaltera;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	//Transiente
	protected Boolean agendarProxInteracao = false;
	protected Boolean situcaoAtencaoAtrasado;
	protected String dataHistorico;
	protected String whereInDocumentoCobranca;
	protected Boolean listaTodos;
	protected String usuarioAndEmpresa;
	protected Date dtproximacobranca;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_clientehistorico")
	public Integer getCdclientehistorico() {
		return cdclientehistorico;
	}
	
	@DisplayName("Observa��o")	
	public String getObservacao() {
		return observacao;
	}
	
	@DisplayName("Cliente")
	@JoinColumn(name="cdcliente")
	@ManyToOne(fetch=FetchType.LAZY)
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("Contato")
	@JoinColumn(name="cdcontato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contato getContato() {
		return contato;
	}
	
	@DisplayName("Meio contato")
	@JoinColumn(name="cdmeiocontato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Meiocontato getMeiocontato() {
		return meiocontato;
	}
	
	@DisplayName("Contrato")
	@JoinColumn(name="cdcontrato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contrato getContrato() {
		return contrato;
	}

	@DisplayName("Atividade")
	@JoinColumn(name="cdatividadetipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	
	@DisplayName("Situa��o")
	public AtividadetipoSituacaoEnum getSituacao() {
		return situacao;
	}

	@DisplayName("Situa��o Hist�rico")
	@JoinColumn(name="cdsituacaohistorico")
	@ManyToOne(fetch=FetchType.LAZY)
	public Situacaohistorico getSituacaohistorico() {
		return situacaohistorico;
	}
	
	@Required
	@DisplayName("Empresa")
	@JoinColumn(name="cdempresahistorico")
	@ManyToOne(fetch=FetchType.LAZY)
	public Empresa getEmpresahistorico() {
		return empresahistorico;
	}
	
	public void setSituacaohistorico(Situacaohistorico situacaohistorico) {
		this.situacaohistorico = situacaohistorico;
	}
	public void setCdclientehistorico(Integer cdclientehistorico) {
		this.cdclientehistorico = cdclientehistorico;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	
	
	public void setContato(Contato contato) {
		this.contato = contato;
	}
	
	@JoinColumn(name="cdusuarioaltera", insertable=false, updatable=false)
	@ManyToOne(fetch=FetchType.LAZY)
	public Usuario getUsuarioaltera() {
		return usuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setUsuarioaltera(Usuario usuarioaltera) {
		this.usuarioaltera = usuarioaltera;
	}
	
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setMeiocontato(Meiocontato meiocontato) {
		this.meiocontato = meiocontato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
	
	public void setSituacao(AtividadetipoSituacaoEnum situacao) {
		this.situacao = situacao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setEmpresahistorico(Empresa empresahistorico) {
		this.empresahistorico = empresahistorico;
	}
	

	@DisplayName("Agendar pr�xima intera��o")
	@Transient
	public Boolean getAgendarProxInteracao() {
		return agendarProxInteracao;
	}
	@Transient
	public Boolean getSitucaoAtencaoAtrasado() {
		return situcaoAtencaoAtrasado;
	}
	
	public void setAgendarProxInteracao(Boolean agendarProxInteracao) {
		this.agendarProxInteracao = agendarProxInteracao;
	}
	public void setSitucaoAtencaoAtrasado(Boolean situcaoAtencaoAtrasado) {
		this.situcaoAtencaoAtrasado = situcaoAtencaoAtrasado;
	}
	
	@Transient
	public boolean isAtividadetipoReclamacao(){
		return atividadetipo!=null && Boolean.TRUE.equals(atividadetipo.getReclamacao());
	}
	
	@Transient
	public String getDataHistorico() {
		return new SimpleDateFormat("dd/MM/yyyy").format(SinedDateUtils.timestampToDate(this.dtaltera));
	}
	public void setDataHistorico(String dataHistorico) {
		this.dataHistorico = dataHistorico;
	}

	@Transient
	public String getWhereInDocumentoCobranca() {
		return whereInDocumentoCobranca;
	}
	public void setWhereInDocumentoCobranca(String whereInDocumentoCobranca) {
		this.whereInDocumentoCobranca = whereInDocumentoCobranca;
	}
	
	@Transient
	public Boolean getListaTodos() {
		return listaTodos;
	}
	public void setListaTodos(Boolean listaTodos) {
		this.listaTodos = listaTodos;
	}
	
	@Transient
	public String getUsuarioAndEmpresa() {
		if(this.usuarioaltera != null){
			usuarioAndEmpresa = this.usuarioaltera.getNome();
			if(this.empresahistorico != null){
				usuarioAndEmpresa = usuarioAndEmpresa + " - " + this.empresahistorico.getNome();
			}
		}
		return usuarioAndEmpresa;
	}
	
	@Transient
	@DisplayName("Pr�xima cobran�a")
	public Date getDtproximacobranca() {
		return dtproximacobranca;
	}

	public void setDtproximacobranca(Date dtproximacobranca) {
		this.dtproximacobranca = dtproximacobranca;
	}
	
}
