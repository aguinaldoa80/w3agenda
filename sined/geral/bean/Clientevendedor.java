package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;

@Entity
@SequenceGenerator(name = "sq_clientevendedor", sequenceName = "sq_clientevendedor")
public class Clientevendedor {

	protected Integer cdclientevendedor;
	protected Tiporesponsavel tiporesponsavel;
	protected Fornecedor agencia;
	protected Colaborador colaborador;
	protected Cliente cliente;
	protected Boolean principal;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_clientevendedor")
	public Integer getCdclientevendedor() {
		return cdclientevendedor;
	}	
	@DescriptionProperty
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	

	@Required
	@DisplayName("Tipo Respons�vel")
	public Tiporesponsavel getTiporesponsavel() {
		return tiporesponsavel;
	}

	@ManyToOne
	@JoinColumn(name="cdagencia")
	@DisplayName("Ag�ncia")
	public Fornecedor getAgencia() {
		return agencia;
	}
	
	public Boolean getPrincipal() {
		return principal;
	}
	
	public void setTiporesponsavel(Tiporesponsavel tiporesponsavel) {
		this.tiporesponsavel = tiporesponsavel;
	}

	public void setAgencia(Fornecedor agencia) {
		this.agencia = agencia;
	}
	
	public void setCdclientevendedor(Integer cdclientevendedor) {
		this.cdclientevendedor = cdclientevendedor;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}
	
	@Transient	
	public String getNomeColaborador() {
		StringBuilder s = new StringBuilder();
		if(this.getColaborador() != null){
			s.append(this.getColaborador().getNome());
		}		
		return s.toString();
	}
	
}
