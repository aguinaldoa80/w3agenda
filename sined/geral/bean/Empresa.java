package br.com.linkcom.sined.geral.bean;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.Email;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Codigoregimetributario;
import br.com.linkcom.sined.geral.bean.enumeration.FormaEnvioBoletoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Indiceatividadesped;
import br.com.linkcom.sined.geral.bean.enumeration.Indiceatividadespedpiscofins;
import br.com.linkcom.sined.geral.bean.enumeration.Indiceperfilsped;
import br.com.linkcom.sined.geral.bean.enumeration.Presencacompradornfe;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@Entity
@JoinEmpresa("empresa")
public class Empresa extends Pessoa implements Log {

	protected Boolean propriedadeRural;
	protected String razaosocial;
	protected String nomefantasia;
	protected Boolean ativo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Arquivo logomarca;
	protected Arquivo logomarcasistema;
	protected String inscricaoestadual;
	protected String inscricaoestadualst;
	protected Boolean principal = Boolean.FALSE;
	protected List<Usuarioempresa> listaUsuarioempresa = new ArrayList<Usuarioempresa>();
//	protected Integer proximonumcontareceber;
	protected Conta contabancariacontareceber;
	protected Contacarteira contacarteiracontareceber;
	protected Conta contabaixadinheiro;
	protected Conta contabaixacheque;
	protected Conta contabaixacreditoconta;
	protected FormaEnvioBoletoEnum formaEnvioBoleto;
	protected Boolean considerarIdPneu;
	protected Integer proximonumnf;
	private Double incentivoFiscalIss;
	protected Integer proximonumnfproduto;
	protected Integer proximonummdfe;
	protected Integer proximonumoportunidade;
	protected Centrocusto centrocusto;
	protected Contagerencial contagerencial;
	protected Centrocusto centrocustoveiculouso;
	protected Contagerencial contagerencialcredito;
	protected Contagerencial contagerencialdebito;
	protected Contagerencial contagerencialdebitoadiantamento;
	protected Centrocusto centrocustodespesaviagem;
	protected String inscricaomunicipal;
	protected String textoboleto;
	protected String textoinfcontribuinte;
	protected String textoautenticidadeboleto;
	protected String termorecapagem;
	protected String integracaopdv;
	protected Integer proximonumeroordemservico;
	protected Presencacompradornfe presencacompradornfe;
	protected Localarmazenagem localarmazenagemprodutofinal;
	protected Localarmazenagem localarmazenagemmateriaprima;
	protected ReportTemplateBean cdreporttemplateboleto;

	protected Contagerencial contagerencialcreditotransferencia;
	protected Contagerencial contagerencialdebitotransferencia;
	protected Centrocusto centrocustotransferencia;
	
	protected Contagerencial contagerencialdevolucaocredito;
	protected Contagerencial contagerencialdevolucaodebito;
	
	protected Set<Empresamodelocontrato> listaEmpresamodelocontrato = new ListSet<Empresamodelocontrato>(Empresamodelocontrato.class);
	protected Set<Empresacodigocnae> listaEmpresacodigocnae = new ListSet<Empresacodigocnae>(Empresacodigocnae.class);
	protected Set<Empresarepresentacao> listaEmpresarepresentacao = new ListSet<Empresarepresentacao>(Empresarepresentacao.class);
	protected List<Empresahistorico> listaEmpresahistorico = new ListSet<Empresahistorico>(Empresahistorico.class);

	protected Naturezaoperacao naturezaoperacao;
	protected Regimetributacao regimetributacao;
	protected Codigotributacao codigotributacao;
	protected Itemlistaservico itemlistaservico;
	protected Codigoregimetributario crt;
	protected Boolean tributacaomunicipiocliente;
	protected Boolean naopreencherdtsaida;
	protected String especienf;
	protected String marcanf;
	protected Boolean discriminarservicoqtdevalor;
	protected Boolean exibirIcmsVenda;
	protected Boolean exibirDifalVenda;

	protected List<Contrato> listaContrato;
	protected List<Nota> listaNota;
	protected List<Venda> listaVenda;
	protected List<Mdfe> listaMdfe;
	protected List<Arquivonf> listaArquivonf;

	protected List<Endereco> listaEnderecoLIST;

	protected Fornecedor escritoriocontabilista;
	protected Fornecedor colaboradorcontabilista;
	protected Pessoaconfiguracao pessoaconfiguracao;
	protected String crccontabilista;
	protected Indiceperfilsped indiceperfilsped;
	protected Indiceatividadesped indiceatividadesped;
	protected Indiceatividadespedpiscofins indiceatividadespedpiscofins;
	protected Uf ufsped;
	protected Municipio municipiosped;
	protected Boolean geracaospedpiscofins;
	protected Integer diageracaospedpiscofins;
	protected Boolean geracaospedicmsipi;
	protected Integer diageracaospedicmsipi;

	protected String observacaoreciboreceber;
	protected String observacaorecibopagar;
	protected String observacaovenda;

	protected String emailordemcompra;
	protected Boolean integracaowms;
	protected String integracaourlwms;
	protected String integracaobts;
	protected Colaborador responsavel;
	protected ComprovanteConfiguravel comprovanteConfiguravelVenda;
	protected ComprovanteConfiguravel comprovanteConfiguravelPedidoVenda;
	protected ComprovanteConfiguravel comprovanteConfiguravelOrcamento;
	protected ComprovanteConfiguravel comprovanteConfiguravelColeta;
	protected Integer proximoidentificadorvenda;
	protected Integer proximoidentificadorvendaorcamento;
	protected Integer proximoidentificadorpedidovenda;
	protected Integer proximoidentificadorcontrato;
	protected Integer proximonumfaturalocacao;
	protected Integer proximonumanimal;
	protected Integer proximonumeromatricula;
	protected Integer proximoidentificadorcarregamento;
	protected Boolean exibiripivenda;
	protected List<Empresaproprietario> listaEmpresaproprietario;

	protected Contagerencial contagerencialicms;
	protected Contagerencial contagerencialicmsst;
	protected Contagerencial contagerencialipi;
	protected Contagerencial contagerencialiss;
	protected Contagerencial contagerencialpis;
	protected Contagerencial contagerencialcofins;
	protected Contagerencial contagerencialfrete;
	protected Contagerencial contagerencialii;
	protected Contagerencial contaGerencialSeguro;
	protected Contagerencial contaGerencialOutrasDespesas;

	protected Tipocobrancapis cstpisnfs;
	protected Tipocobrancacofins cstcofinsnfs;
	protected Telefone telefonePrincipal;
	protected Telefone celular;
	protected Boolean calcularQuantidadeTransporte;
	protected String emailfinanceiro;
	
	protected Boolean permitirInclusaoMaterialServico;
	protected Integer numfaturalocacaoReiniciar;
	
	protected Integer estabelecimento;
	
	protected Cliente clientefeedapplication;
	protected Material materialfeedapplication;
	protected Pedidovendatipo pedidovendatipofeedapplication;
	protected Prazopagamento prazopagamentofeedapplication;
	protected Documentotipo documentotipofeedapplication;
	protected String integracaocontabil;
	protected Integer serienfe;
	protected Conta contadevolucao;
	protected List<Empresamodelocomprovanteorcamento> listaEmpresamodelocomprovanteorcamento;
	
	protected Fornecedor pessoaconfiguracao_transportador;
	
	protected String codigoEmpresaDominioIntegracao;
	private Integer proximoNumNfce;
	private Integer serieNfce;
	protected String usuarioCorreios;
	protected String senhaCorreios;
	
	protected String nomeMunicipioUf;
	
	public Empresa() {
	}

	public Empresa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}

	public Empresa(String nome){
		this.nome = nome;
	}
	
	public Empresa(String nome, String razaosocial, String nomefantasia) {
		this.nome = nome;
		this.razaosocial = razaosocial;
		this.nomefantasia = nomefantasia;
	}

	@DisplayName("Ativo")
	@Required
	public Boolean getAtivo() {
		return ativo;
	}

	@DisplayName("Logomarca para relat�rios")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdarquivo")
	public Arquivo getLogomarca() {
		return logomarca;
	}

	@DisplayName("Logomarca para o sistema")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdlogomarcasistema")
	public Arquivo getLogomarcasistema() {
		return logomarcasistema;
	}

	@DisplayName("Principal")
	public Boolean getPrincipal() {
		return principal;
	}

	@OneToMany(mappedBy = "empresa")
	public List<Usuarioempresa> getListaUsuarioempresa() {
		return listaUsuarioempresa;
	}

	@DisplayName("Endere�os")
	@Transient
	@Override
	public Set<Endereco> getListaEndereco() {
		return super.getListaEndereco();
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@DisplayName("Inscri��o estadual")
	@MaxLength(14)
	public String getInscricaoestadual() {
		return inscricaoestadual;
	}

//	@DisplayName("Pr�ximo n�mero da conta a receber")
//	public Integer getProximonumcontareceber() {
//		return proximonumcontareceber;
//	}

	@DisplayName("Conta Banc�ria Boleto")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontabancariacontareceber")
	public Conta getContabancariacontareceber() {
		return contabancariacontareceber;
	}
	
	@DisplayName("Carteira")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontacarteiracontareceber")
	public Contacarteira getContacarteiracontareceber() {
		return contacarteiracontareceber;
	}
	
	@DisplayName("V�nculo baixa (Dinheiro)")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontabaixadinheiro")
	public Conta getContabaixadinheiro() {
		return contabaixadinheiro;
	}

	@DisplayName("V�nculo baixa (Cheque)")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontabaixacheque")
	public Conta getContabaixacheque() {
		return contabaixacheque;
	}

	@DisplayName("V�nculo baixa (Cr�dito em conta)")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontabaixacreditoconta")
	public Conta getContabaixacreditoconta() {
		return contabaixacreditoconta;
	}

	@DisplayName("Pr�ximo n�mero de nota fiscal de servi�o")
	public Integer getProximonumnf() {
		return proximonumnf;
	}
	
	@DisplayName("Incentivo Fiscal ISS (%)")
	public Double getIncentivoFiscalIss() {
		return incentivoFiscalIss;
	}

	@DisplayName("Pr�ximo n�mero de nota fiscal de produto")
	public Integer getProximonumnfproduto() {
		return proximonumnfproduto;
	}
	
	@DisplayName("Pr�ximo n�mero do MDF-e")
	public Integer getProximonummdfe() {
		return proximonummdfe;
	}

	@DisplayName("Centro de custo da venda")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcentrocustovenda")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}

	@DisplayName("Centro de custo do ve�culo uso")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcentrocustoveiculouso")
	public Centrocusto getCentrocustoveiculouso() {
		return centrocustoveiculouso;
	}

	@DisplayName("Conta gerencial da venda")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencialvenda")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}

	@DisplayName("Conta gerencial cr�dito (acerto)")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencialcredito")
	public Contagerencial getContagerencialcredito() {
		return contagerencialcredito;
	}

	@DisplayName("Conta gerencial d�bito (acerto)")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencialdebito")
	public Contagerencial getContagerencialdebito() {
		return contagerencialdebito;
	}
	
	@DisplayName("Conta gerencial d�bito (adiantamento)")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencialdebitoadiantamento")
	public Contagerencial getContagerencialdebitoadiantamento() {
		return contagerencialdebitoadiantamento;
	}

	@DisplayName("Centro de custo de Despesa de Viagem")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcentrocustodespesaviagem")
	public Centrocusto getCentrocustodespesaviagem() {
		return centrocustodespesaviagem;
	}

	@DisplayName("Inscri��o municipal")
	public String getInscricaomunicipal() {
		return inscricaomunicipal;
	}

	@MaxLength(100)
	@Required
	@Transient
	@Override
	public String getNome() {
		return super.getNome();
	}
	
	@DisplayName("Fazenda")
	public Boolean getPropriedadeRural() {
		return propriedadeRural;
	}

	@DisplayName("Raz�o Social")
	public String getRazaosocial() {
		return razaosocial;
	}

	@DescriptionProperty
	@DisplayName("Nome fantasia")
	public String getNomefantasia() {
		return nomefantasia;
	}

	@DisplayName("C�digo de regime tribut�rio")
	public Codigoregimetributario getCrt() {
		return crt;
	}

	@DisplayName("Inscri��o estadual de ST")
	public String getInscricaoestadualst() {
		return inscricaoestadualst;
	}

	@DisplayName("Perfil de apresenta��o do arquivo fiscal")
	public Indiceperfilsped getIndiceperfilsped() {
		return indiceperfilsped;
	}

	@DisplayName("Tipo de atividade Fiscal")
	public Indiceatividadesped getIndiceatividadesped() {
		return indiceatividadesped;
	}
	@DisplayName("Tipo de atividade EFD-Contribui��es")
	public Indiceatividadespedpiscofins getIndiceatividadespedpiscofins() {
		return indiceatividadespedpiscofins;
	}

	@DisplayName("Uf do domic�lio fiscal")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdufsped")
	public Uf getUfsped() {
		return ufsped;
	}

	@DisplayName("Munic�pio do domic�lio fiscal")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdmunicipiosped")
	public Municipio getMunicipiosped() {
		return municipiosped;
	}

	@DisplayName("Modelos de Contratos")
	@OneToMany(mappedBy = "empresa", fetch = FetchType.LAZY)
	public Set<Empresamodelocontrato> getListaEmpresamodelocontrato() {
		return listaEmpresamodelocontrato;
	}

	@DisplayName("Representa��o")
	@OneToMany(mappedBy = "empresa", fetch = FetchType.LAZY)
	public Set<Empresarepresentacao> getListaEmpresarepresentacao() {
		return listaEmpresarepresentacao;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy = "empresa", fetch = FetchType.LAZY)
	public List<Empresahistorico> getListaEmpresahistorico() {
		return listaEmpresahistorico;
	}

	@DisplayName("C�digos CNAE's")
	@OneToMany(mappedBy = "empresa", fetch = FetchType.LAZY)
	public Set<Empresacodigocnae> getListaEmpresacodigocnae() {
		return listaEmpresacodigocnae;
	}

	@MaxLength(2000)
	@DisplayName("Complemento de  texto para e-mail de boleto")
	public String getTextoboleto() {
		return textoboleto;
	}

	@MaxLength(2000)
	@DisplayName("Texto para Informa��es Adicionais de Interesse do Contribuinte")
	public String getTextoinfcontribuinte() {
		return textoinfcontribuinte;
	}
	
	@MaxLength(2000)
	@DisplayName("Termo de recapagem")
	public String getTermorecapagem() {
		return termorecapagem;
	}

	@DisplayName("Comprovante configur�vel de Venda")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcomprovanteconfiguravelvenda")
	public ComprovanteConfiguravel getComprovanteConfiguravelVenda() {
		return comprovanteConfiguravelVenda;
	}

	@DisplayName("Comprovante configur�vel de Pedido de Venda")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcomprovanteconfiguravelpedidovenda")
	public ComprovanteConfiguravel getComprovanteConfiguravelPedidoVenda() {
		return comprovanteConfiguravelPedidoVenda;
	}

	@DisplayName("Comprovante configur�vel de Or�amento")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcomprovanteconfiguravelorcamento")
	public ComprovanteConfiguravel getComprovanteConfiguravelOrcamento() {
		return comprovanteConfiguravelOrcamento;
	}
	@DisplayName("Comprovante configur�vel de Coleta")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcomprovanteconfiguravelcoleta")
	public ComprovanteConfiguravel getComprovanteConfiguravelColeta() {
		return comprovanteConfiguravelColeta;
	}

	@DisplayName("Tributa��o no munic�pio do cliente")
	public Boolean getTributacaomunicipiocliente() {
		return tributacaomunicipiocliente;
	}

	@DisplayName("Loja da integra��o PDV")
	public String getIntegracaopdv() {
		return integracaopdv;
	}
	
	@DisplayName("Marca")
	public String getMarcanf() {
		return marcanf;
	}
	
	@DisplayName("Esp�cie")
	public String getEspecienf() {
		return especienf;
	}
	
	@DisplayName("Pr�ximo identificador da oportunidade")
	public Integer getProximonumoportunidade() {
		return proximonumoportunidade;
	}
	
	@DisplayName("Calcular quantidade para transporte")
	public Boolean getCalcularQuantidadeTransporte() {
		return calcularQuantidadeTransporte;
	}
	
	@DisplayName("Conta gerencial (Cr�dito)")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencialcreditotransferencia")
	public Contagerencial getContagerencialcreditotransferencia() {
		return contagerencialcreditotransferencia;
	}
	
	@DisplayName("Conta gerencial (D�bito)")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencialdebitotransferencia")
	public Contagerencial getContagerencialdebitotransferencia() {
		return contagerencialdebitotransferencia;
	}
	
	@DisplayName("Centro de custo")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcentrocustotransferencia")
	public Centrocusto getCentrocustotransferencia() {
		return centrocustotransferencia;
	}
	
	@DisplayName("Devolu��o Cr�dito")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencialdevolucaocredito")
	public Contagerencial getContagerencialdevolucaocredito() {
		return contagerencialdevolucaocredito;
	}

	@DisplayName("Devolu��o D�bito")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencialdevolucaodebito")
	public Contagerencial getContagerencialdevolucaodebito() {
		return contagerencialdevolucaodebito;
	}

	@DisplayName("Pr�ximo n�mero de matr�cula")
	public Integer getProximonumeromatricula() {
		return proximonumeromatricula;
	}
	
	@DisplayName("Origem da opera��o")
	public Presencacompradornfe getPresencacompradornfe() {
		return presencacompradornfe;
	}
	
	@DisplayName("Considerar o ID do pneu como c�digo do produto nas notas fiscais com origem de coleta")
	public Boolean getConsiderarIdPneu() {
		return considerarIdPneu;
	}
	
	@DisplayName("Observa��es para confer�ncia de autenticidade do boleto")
	@MaxLength(2000)
	public String getTextoautenticidadeboleto() {
		return textoautenticidadeboleto;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdreporttemplateboleto")
	@DisplayName("Template padr�o de envio de e-mail de boleto")
	public ReportTemplateBean getCdreporttemplateboleto() {
		return cdreporttemplateboleto;
	}
	
	@DisplayName("Pr�ximo N�mero")
	public Integer getProximoNumNfce() {
		return proximoNumNfce;
	}
	
	public void setProximoNumNfce(Integer proximoNumNfce) {
		this.proximoNumNfce = proximoNumNfce;
	}
	
	@DisplayName("S�rie")
	public Integer getSerieNfce() {
		return serieNfce;
	}
	
	public void setSerieNfce(Integer serieNfce) {
		this.serieNfce = serieNfce;
	}
	
	@DisplayName("Usu�rio")
	public String getUsuarioCorreios() {
		return usuarioCorreios;
	}
	
	public void setUsuarioCorreios(String usuarioCorreios) {
		this.usuarioCorreios = usuarioCorreios;
	}
	
	@DisplayName("Senha")
	public String getSenhaCorreios() {
		return senhaCorreios;
	}
	
	public void setSenhaCorreios(String senhaCorreios) {
		this.senhaCorreios = senhaCorreios;
	}
	
	public void setCdreporttemplateboleto(ReportTemplateBean cdreporttemplateboleto) {
		this.cdreporttemplateboleto = cdreporttemplateboleto;
	}
	
	public void setTextoautenticidadeboleto(String textoautenticidadeboleto) {
		this.textoautenticidadeboleto = textoautenticidadeboleto;
	}
	
	public void setConsiderarIdPneu(Boolean considerarIdPneu) {
		this.considerarIdPneu = considerarIdPneu;
	}
	
	public void setPresencacompradornfe(
			Presencacompradornfe presencacompradornfe) {
		this.presencacompradornfe = presencacompradornfe;
	}

	public Integer getEstabelecimento() {
		return estabelecimento;
	}
	
	@DisplayName("Loca de Baixa Produto Final (Ch�o de F�brica)")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdlocalarmazenagemprodutofinal")
	public Localarmazenagem getLocalarmazenagemprodutofinal() {
		return localarmazenagemprodutofinal;
	}

	public void setLocalarmazenagemprodutofinal(Localarmazenagem localarmazenagemprodutofinal) {
		this.localarmazenagemprodutofinal = localarmazenagemprodutofinal;
	}

	@DisplayName("Local de baixa (mat�ria-prima)")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdlocalarmazenagemmateriaprima")
	public Localarmazenagem getLocalarmazenagemmateriaprima() {
		return localarmazenagemmateriaprima;
	}
	
	public void setLocalarmazenagemmateriaprima(
			Localarmazenagem localarmazenagemmateriaprima) {
		this.localarmazenagemmateriaprima = localarmazenagemmateriaprima;
	}
	
	public void setCentrocustotransferencia(Centrocusto centrocustotransferencia) {
		this.centrocustotransferencia = centrocustotransferencia;
	}
	
	public void setContagerencialcreditotransferencia(
			Contagerencial contagerencialcreditotransferencia) {
		this.contagerencialcreditotransferencia = contagerencialcreditotransferencia;
	}
	
	public void setContagerencialdebitotransferencia(
			Contagerencial contagerencialdebitotransferencia) {
		this.contagerencialdebitotransferencia = contagerencialdebitotransferencia;
	}

	public void setContagerencialdevolucaocredito(Contagerencial contagerencialdevolucaocredito) {
		this.contagerencialdevolucaocredito = contagerencialdevolucaocredito;
	}

	public void setContagerencialdevolucaodebito(Contagerencial contagerencialdevolucaodebito) {
		this.contagerencialdevolucaodebito = contagerencialdevolucaodebito;
	}

	public void setCalcularQuantidadeTransporte(Boolean calcularQuantidadeTransporte) {
		this.calcularQuantidadeTransporte = calcularQuantidadeTransporte;
	}

	public void setProximonumoportunidade(Integer proximonumoportunidade) {
		this.proximonumoportunidade = proximonumoportunidade;
	}
	
	public void setContabancariacontareceber(Conta contabancariacontareceber) {
		this.contabancariacontareceber = contabancariacontareceber;
	}
	
	public void setContacarteiracontareceber(Contacarteira contacarteiracontareceber) {
		this.contacarteiracontareceber = contacarteiracontareceber;
	}
	
	public void setContabaixadinheiro(Conta contabaixadinheiro) {
		this.contabaixadinheiro = contabaixadinheiro;
	}

	public void setContabaixacheque(Conta contabaixacheque) {
		this.contabaixacheque = contabaixacheque;
	}

	public void setContabaixacreditoconta(Conta contabaixacreditoconta) {
		this.contabaixacreditoconta = contabaixacreditoconta;
	}

	public void setMarcanf(String marcanf) {
		this.marcanf = marcanf;
	}
	
	public void setEspecienf(String especienf) {
		this.especienf = especienf;
	}

	public void setIntegracaopdv(String integracaopdv) {
		this.integracaopdv = integracaopdv;
	}

	public void setTributacaomunicipiocliente(Boolean tributacaomunicipiocliente) {
		this.tributacaomunicipiocliente = tributacaomunicipiocliente;
	}

	public void setComprovanteConfiguravelVenda(ComprovanteConfiguravel comprovanteConfiguravelVenda) {
		this.comprovanteConfiguravelVenda = comprovanteConfiguravelVenda;
	}

	public void setComprovanteConfiguravelPedidoVenda(ComprovanteConfiguravel comprovanteConfiguravelPedidoVenda) {
		this.comprovanteConfiguravelPedidoVenda = comprovanteConfiguravelPedidoVenda;
	}

	public void setComprovanteConfiguravelOrcamento(ComprovanteConfiguravel comprovanteConfiguravelOrcamento) {
		this.comprovanteConfiguravelOrcamento = comprovanteConfiguravelOrcamento;
	}
	
	public void setComprovanteConfiguravelColeta(ComprovanteConfiguravel comprovanteConfiguravelColeta) {
		this.comprovanteConfiguravelColeta = comprovanteConfiguravelColeta;
	}

	public void setTextoboleto(String textoboleto) {
		this.textoboleto = textoboleto;
	}

	public void setTextoinfcontribuinte(String textoinfcontribuinte) {
		this.textoinfcontribuinte = textoinfcontribuinte;
	}

	public void setTermorecapagem(String termorecapagem) {
		this.termorecapagem = termorecapagem;
	}

	public void setIndiceperfilsped(Indiceperfilsped indiceperfilsped) {
		this.indiceperfilsped = indiceperfilsped;
	}

	public void setIndiceatividadesped(Indiceatividadesped indiceatividadesped) {
		this.indiceatividadesped = indiceatividadesped;
	}
	
	public void setIndiceatividadespedpiscofins(Indiceatividadespedpiscofins indiceatividadespedpiscofins) {
		this.indiceatividadespedpiscofins = indiceatividadespedpiscofins;
	}

	public void setUfsped(Uf ufsped) {
		this.ufsped = ufsped;
	}

	public void setMunicipiosped(Municipio municipiosped) {
		this.municipiosped = municipiosped;
	}

	public void setInscricaoestadualst(String inscricaoestadualst) {
		this.inscricaoestadualst = inscricaoestadualst;
	}

	public void setCrt(Codigoregimetributario crt) {
		this.crt = crt;
	}

	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}
	
	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}
	
	public void setPropriedadeRural(Boolean propriedadeRural) {
		this.propriedadeRural = propriedadeRural;
	}

	public void setProximonumnfproduto(Integer proximonumnfproduto) {
		this.proximonumnfproduto = proximonumnfproduto;
	}
	
	public void setProximonummdfe(Integer proximonummdfe) {
		this.proximonummdfe = proximonummdfe;
	}

	public void setInscricaomunicipal(String inscricaomunicipal) {
		this.inscricaomunicipal = inscricaomunicipal;
	}

	public void setLogomarcasistema(Arquivo logomarcasistema) {
		this.logomarcasistema = logomarcasistema;
	}

	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}

	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}

	public void setContagerencialcredito(Contagerencial contagerencialcredito) {
		this.contagerencialcredito = contagerencialcredito;
	}

	public void setContagerencialdebito(Contagerencial contagerencialdebito) {
		this.contagerencialdebito = contagerencialdebito;
	}

	public void setContagerencialdebitoadiantamento(
			Contagerencial contagerencialdebitoadiantamento) {
		this.contagerencialdebitoadiantamento = contagerencialdebitoadiantamento;
	}
	
	public void setCentrocustodespesaviagem(Centrocusto centrocustodespesaviagem) {
		this.centrocustodespesaviagem = centrocustodespesaviagem;
	}

	public void setProximonumnf(Integer proximonumnf) {
		this.proximonumnf = proximonumnf;
	}
	
	public void setIncentivoFiscalIss(Double incentivoFiscalIss) {
		this.incentivoFiscalIss = incentivoFiscalIss;
	}

	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}

	public void setListaUsuarioempresa(List<Usuarioempresa> listaUsuarioempresa) {
		this.listaUsuarioempresa = listaUsuarioempresa;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setLogomarca(Arquivo logomarca) {
		this.logomarca = logomarca;
	}

	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}

	public void setCentrocustoveiculouso(Centrocusto centrocustoveiculouso) {
		this.centrocustoveiculouso = centrocustoveiculouso;
	}

	public void setListaEmpresamodelocontrato(Set<Empresamodelocontrato> listaEmpresamodelocontrato) {
		this.listaEmpresamodelocontrato = listaEmpresamodelocontrato;
	}

	public void setListaEmpresacodigocnae(Set<Empresacodigocnae> listaEmpresacodigocnae) {
		this.listaEmpresacodigocnae = listaEmpresacodigocnae;
	}

	public void setListaEmpresarepresentacao(Set<Empresarepresentacao> listaEmpresarepresentacao) {
		this.listaEmpresarepresentacao = listaEmpresarepresentacao;
	}
	
	public void setListaEmpresahistorico(List<Empresahistorico> listaEmpresahistorico) {
		this.listaEmpresahistorico = listaEmpresahistorico;
	}
	
	public void setProximonumeromatricula(Integer proximonumeromatricula) {
		this.proximonumeromatricula = proximonumeromatricula;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Empresa) {
			Empresa empresa = (Empresa) obj;
			return this.getCdpessoa().equals(empresa.getCdpessoa());
		}
		return super.equals(obj);
	}

	@Transient
	public List<Endereco> getListaEnderecoLIST() {
		if (getListaEndereco() != null) {
			return new ArrayList<Endereco>(getListaEndereco());
		} else {
			return null;
		}
	}

	@OneToMany(mappedBy = "empresa")
	public List<Contrato> getListaContrato() {
		return listaContrato;
	}

	public void setListaContrato(List<Contrato> listaContrato) {
		this.listaContrato = listaContrato;
	}

	@OneToMany(mappedBy = "empresa")
	public List<Nota> getListaNota() {
		return listaNota;
	}

	public void setListaNota(List<Nota> listaNota) {
		this.listaNota = listaNota;
	}
	
	@OneToMany(mappedBy = "empresa")
	public List<Venda> getListaVenda() {
		return listaVenda;
	}
	
	public void setListaVenda(List<Venda> listaVenda) {
		this.listaVenda = listaVenda;
	}
	
	@OneToMany(mappedBy = "empresa")
	public List<Mdfe> getListaMdfe() {
		return listaMdfe;
	}

	public void setListaMdfe(List<Mdfe> listaMdfe) {
		this.listaMdfe = listaMdfe;
	}

	@OneToMany(mappedBy = "empresa")
	public List<Arquivonf> getListaArquivonf() {
		return listaArquivonf;
	}

	public void setListaArquivonf(List<Arquivonf> listaArquivonf) {
		this.listaArquivonf = listaArquivonf;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdescritoriocontabil")
	@DisplayName("Escrit�rio Contabilista")
	public Fornecedor getEscritoriocontabilista() {
		return escritoriocontabilista;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontabilista")
	@DisplayName("Contabilista")
	public Fornecedor getColaboradorcontabilista() {
		return colaboradorcontabilista;
	}
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdpessoaconfiguracao")
	public Pessoaconfiguracao getPessoaconfiguracao() {
		return pessoaconfiguracao;
	}

	@DisplayName("CRC")
	@MaxLength(15)
	public String getCrccontabilista() {
		return crccontabilista;
	}

	@MaxLength(50)
	@Email
	@DisplayName("E-mail da ordem compra")
	public String getEmailordemcompra() {
		return emailordemcompra;
	}

	public void setEscritoriocontabilista(Fornecedor escritoriocontabilista) {
		this.escritoriocontabilista = escritoriocontabilista;
	}

	public void setColaboradorcontabilista(Fornecedor colaboradorcontabilista) {
		this.colaboradorcontabilista = colaboradorcontabilista;
	}
	
	public void setPessoaconfiguracao(Pessoaconfiguracao pessoaconfiguracao) {
		this.pessoaconfiguracao = pessoaconfiguracao;
	}

	public void setCrccontabilista(String crccontabilista) {
		this.crccontabilista = crccontabilista;
	}

	@DisplayName("Observa��o recibo receber")
	@MaxLength(500)
	public String getObservacaoreciboreceber() {
		return observacaoreciboreceber;
	}

	@DisplayName("Observa��o recibo pagar")
	@MaxLength(500)
	public String getObservacaorecibopagar() {
		return observacaorecibopagar;
	}

	@DisplayName("Observa��o venda")
	@MaxLength(500)
	public String getObservacaovenda() {
		return observacaovenda;
	}
	
	@DisplayName("Gera��o SPED ICMS/IPI")
	public Boolean getGeracaospedicmsipi() {
		return geracaospedicmsipi;
	}
	
	@DisplayName("Gera��o SPED PIS/COFINS")
	public Boolean getGeracaospedpiscofins() {
		return geracaospedpiscofins;
	}
	
	@DisplayName("Dia de Gera��o SPED PIS/COFINS")
	@MaxLength(2)
	public Integer getDiageracaospedpiscofins() {
		return diageracaospedpiscofins;
	}
	
	@DisplayName("Dia de Gera��o SPED ICMS/IPI")
	@MaxLength(2)
	public Integer getDiageracaospedicmsipi() {
		return diageracaospedicmsipi;
	}
	
	public void setDiageracaospedicmsipi(Integer diageracaospedicmsipi) {
		this.diageracaospedicmsipi = diageracaospedicmsipi;
	}
	
	public void setDiageracaospedpiscofins(Integer diageracaospedpiscofins) {
		this.diageracaospedpiscofins = diageracaospedpiscofins;
	}
	
	public void setGeracaospedicmsipi(Boolean geracaospedicmsipi) {
		this.geracaospedicmsipi = geracaospedicmsipi;
	}
	
	public void setGeracaospedpiscofins(Boolean geracaospedpiscofins) {
		this.geracaospedpiscofins = geracaospedpiscofins;
	}

	public void setObservacaovenda(String observacaovenda) {
		this.observacaovenda = observacaovenda;
	}

	public void setObservacaoreciboreceber(String observacaoreciboreceber) {
		this.observacaoreciboreceber = observacaoreciboreceber;
	}

	public void setObservacaorecibopagar(String observacaorecibopagar) {
		this.observacaorecibopagar = observacaorecibopagar;
	}

	public void setEmailordemcompra(String emailordemcompra) {
		this.emailordemcompra = emailordemcompra;
	}

//	public void setProximonumcontareceber(Integer proximonumcontareceber) {
//		this.proximonumcontareceber = proximonumcontareceber;
//	}

	@DisplayName("Integra��o com WMS")
	public Boolean getIntegracaowms() {
		return integracaowms;
	}

	public void setIntegracaowms(Boolean integracaowms) {
		this.integracaowms = integracaowms;
	}

	@DisplayName("URL de Integra��o com WMS")
	public String getIntegracaourlwms() {
		return integracaourlwms;
	}
	@DisplayName("Integra��o BTS")
	@MaxLength(6)
	public String getIntegracaobts() {
		return integracaobts;
	}

	public void setIntegracaourlwms(String integracaourlwms) {
		this.integracaourlwms = integracaourlwms;
	}
	public void setIntegracaobts(String integracaobts) {
		this.integracaobts = integracaobts;
	}

	@DisplayName("Respons�vel para contato")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdresponsavel")
	public Colaborador getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdnaturezaoperacao")
	@DisplayName("Natureza de opera��o")
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdregimetributacao")
	@DisplayName("Regime especial de tributa��o")
	public Regimetributacao getRegimetributacao() {
		return regimetributacao;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcodigotributacao")
	@DisplayName("C�digo de tributa��o")
	public Codigotributacao getCodigotributacao() {
		return codigotributacao;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cditemlistaservico")
	@DisplayName("Item da lista de servi�os")
	public Itemlistaservico getItemlistaservico() {
		return itemlistaservico;
	}

	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}

	public void setRegimetributacao(Regimetributacao regimetributacao) {
		this.regimetributacao = regimetributacao;
	}

	public void setCodigotributacao(Codigotributacao codigotributacao) {
		this.codigotributacao = codigotributacao;
	}

	public void setItemlistaservico(Itemlistaservico itemlistaservico) {
		this.itemlistaservico = itemlistaservico;
	}

	@DisplayName("Pr�ximo Identificador da Venda")
	public Integer getProximoidentificadorvenda() {
		return proximoidentificadorvenda;
	}

	public void setProximoidentificadorvenda(Integer proximoidentificadorvenda) {
		this.proximoidentificadorvenda = proximoidentificadorvenda;
	}
	
	@DisplayName("Pr�ximo Identificador do Or�amento")
	public Integer getProximoidentificadorvendaorcamento() {
		return proximoidentificadorvendaorcamento;
	}
	
	public void setProximoidentificadorvendaorcamento(
			Integer proximoidentificadorvendaorcamento) {
		this.proximoidentificadorvendaorcamento = proximoidentificadorvendaorcamento;
	}

	@DisplayName("Pr�ximo identificador do Pedido de Venda")
	public Integer getProximoidentificadorpedidovenda() {
		return proximoidentificadorpedidovenda;
	}

	@DisplayName("Pr�ximo identificador do Contrato")
	public Integer getProximoidentificadorcontrato() {
		return proximoidentificadorcontrato;
	}

	public void setProximoidentificadorcontrato(Integer proximoidentificadorcontrato) {
		this.proximoidentificadorcontrato = proximoidentificadorcontrato;
	}

	public void setProximoidentificadorpedidovenda(Integer proximoidentificadorpedidovenda) {
		this.proximoidentificadorpedidovenda = proximoidentificadorpedidovenda;
	}

	@DisplayName("ICMS")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencialicms")
	public Contagerencial getContagerencialicms() {
		return contagerencialicms;
	}

	@DisplayName("ICMS ST")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencialicmsst")
	public Contagerencial getContagerencialicmsst() {
		return contagerencialicmsst;
	}

	@DisplayName("Ipi")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencialipi")
	public Contagerencial getContagerencialipi() {
		return contagerencialipi;
	}

	@DisplayName("Iss")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencialiss")
	public Contagerencial getContagerencialiss() {
		return contagerencialiss;
	}

	@DisplayName("Pis")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencialpis")
	public Contagerencial getContagerencialpis() {
		return contagerencialpis;
	}

	@DisplayName("Cofins")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencialcofins")
	public Contagerencial getContagerencialcofins() {
		return contagerencialcofins;
	}

	@DisplayName("Frete")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencialfrete")
	public Contagerencial getContagerencialfrete() {
		return contagerencialfrete;
	}

	@DisplayName("Imposto Importa��o")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencialii")
	public Contagerencial getContagerencialii() {
		return contagerencialii;
	}
	
	@DisplayName("Conta Gerencial (Outras despesas)")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencialoutrasdespesas")
	public Contagerencial getContaGerencialOutrasDespesas() {
		return contaGerencialOutrasDespesas;
	}
	@DisplayName("Conta Gerencial (Seguro)")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencialseguro")
	public Contagerencial getContaGerencialSeguro() {
		return contaGerencialSeguro;
	}

	public void setContagerencialicms(Contagerencial contagerencialicms) {
		this.contagerencialicms = contagerencialicms;
	}

	public void setContagerencialicmsst(Contagerencial contagerencialicmsst) {
		this.contagerencialicmsst = contagerencialicmsst;
	}

	public void setContagerencialipi(Contagerencial contagerencialipi) {
		this.contagerencialipi = contagerencialipi;
	}

	public void setContagerencialiss(Contagerencial contagerencialiss) {
		this.contagerencialiss = contagerencialiss;
	}

	public void setContagerencialpis(Contagerencial contagerencialpis) {
		this.contagerencialpis = contagerencialpis;
	}

	public void setContagerencialcofins(Contagerencial contagerencialcofins) {
		this.contagerencialcofins = contagerencialcofins;
	}

	public void setContagerencialfrete(Contagerencial contagerencialfrete) {
		this.contagerencialfrete = contagerencialfrete;
	}

	public void setContagerencialii(Contagerencial contagerencialii) {
		this.contagerencialii = contagerencialii;
	}
	
	public void setContaGerencialOutrasDespesas(
			Contagerencial contaGerencialOutrasDespesas) {
		this.contaGerencialOutrasDespesas = contaGerencialOutrasDespesas;
	}
	
	public void setContaGerencialSeguro(Contagerencial contaGerencialSeguro) {
		this.contaGerencialSeguro = contaGerencialSeguro;
	}

	@DisplayName("C�d. Sit. Trib. PIS")
	public Tipocobrancapis getCstpisnfs() {
		return cstpisnfs;
	}

	@DisplayName("C�d. Sit. Trib. COFINS")
	public Tipocobrancacofins getCstcofinsnfs() {
		return cstcofinsnfs;
	}

	public void setCstpisnfs(Tipocobrancapis cstpisnfs) {
		this.cstpisnfs = cstpisnfs;
	}

	public void setCstcofinsnfs(Tipocobrancacofins cstcofinsnfs) {
		this.cstcofinsnfs = cstcofinsnfs;
	}

	@DisplayName("Pr�ximo n�mero de fatura de loca��o")
	public Integer getProximonumfaturalocacao() {
		return proximonumfaturalocacao;
	}

	public void setProximonumfaturalocacao(Integer proximonumfaturalocacao) {
		this.proximonumfaturalocacao = proximonumfaturalocacao;
	}

	@DisplayName("Pr�ximo n�mero de animal")
	public Integer getProximonumanimal() {
		return proximonumanimal;
	}

	public void setProximonumanimal(Integer proximonumanimal) {
		this.proximonumanimal = proximonumanimal;
	}
	
	@DisplayName("Forma de envio do boleto")
	@Required
	public FormaEnvioBoletoEnum getFormaEnvioBoleto() {
		return formaEnvioBoleto;
	}
	
	public void setFormaEnvioBoleto(FormaEnvioBoletoEnum formaEnvioBoleto) {
		this.formaEnvioBoleto = formaEnvioBoleto;
	}

	@Transient
	public Telefone getTelefonePrincipal() {
		return telefonePrincipal;
	}

	public void setTelefonePrincipal(Telefone telefonePrincipal) {
		this.telefonePrincipal = telefonePrincipal;
	}

	@Transient
	public Telefone getCelular() {
		return celular;
	}

	public void setCelular(Telefone celular) {
		this.celular = celular;
	}

	@DisplayName("N�o preencher data de sa�da")
	public Boolean getNaopreencherdtsaida() {
		return naopreencherdtsaida;
	}

	public void setNaopreencherdtsaida(Boolean naopreencherdtsaida) {
		this.naopreencherdtsaida = naopreencherdtsaida;
	}
	
	@MaxLength(9)
	@DisplayName("Pr�ximo n�mero da ordem de servi�o")
	public Integer getProximonumeroordemservico() {
		return proximonumeroordemservico;
	}

	public void setProximonumeroordemservico(Integer proximonumeroordemservico) {
		this.proximonumeroordemservico = proximonumeroordemservico;
	}	
	
	@DisplayName("Discriminar servi�o, quantidade e valor")
	public Boolean getDiscriminarservicoqtdevalor() {
		return discriminarservicoqtdevalor;
	}

	public void setDiscriminarservicoqtdevalor(Boolean discriminarservicoqtdevalor) {
		this.discriminarservicoqtdevalor = discriminarservicoqtdevalor;
	}
	
	@DisplayName("Exibir DIFAL na venda")
	public Boolean getExibirDifalVenda() {
		return exibirDifalVenda;
	}
	
	public void setExibirDifalVenda(Boolean exibirDifalNaVenda) {
		this.exibirDifalVenda = exibirDifalNaVenda;
	}
	
	@DisplayName("Exibir ICMS na venda")
	public Boolean getExibirIcmsVenda() {
		return exibirIcmsVenda;
	}
	
	public void setExibirIcmsVenda(Boolean exibirIcmsNaVenda) {
		this.exibirIcmsVenda = exibirIcmsNaVenda;
	}
	
	@MaxLength(50)
	@Email
	@DisplayName("E-mail financeiro")
	public String getEmailfinanceiro() {
		return emailfinanceiro;
	}
	
	public void setEmailfinanceiro(String emailfinanceiro) {
		this.emailfinanceiro = emailfinanceiro;
	}

	@DisplayName("Permitir Inclus�o de  Material/Servi�o")
	public Boolean getPermitirInclusaoMaterialServico() {
		return permitirInclusaoMaterialServico;
	}

	public void setPermitirInclusaoMaterialServico(Boolean permitirInclusaoMaterialServico) {
		this.permitirInclusaoMaterialServico = permitirInclusaoMaterialServico;
	}

	@Transient
	@DisplayName("Reiniciar para")
	public Integer getNumfaturalocacaoReiniciar() {
		return numfaturalocacaoReiniciar;
	}
	public void setNumfaturalocacaoReiniciar(Integer numfaturalocacaoReiniciar) {
		this.numfaturalocacaoReiniciar = numfaturalocacaoReiniciar;
	}
	public void setEstabelecimento(Integer estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	@Transient
	public String getRazaosocialOuNome() {
		if(StringUtils.isNotEmpty(getRazaosocial()))
			return getRazaosocial();
		return getNome();
	}
	
	@DisplayName("Cliente Importa��o Feed Application")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdclientefeedapplication")
	public Cliente getClientefeedapplication() {
		return clientefeedapplication;
	}
	@DisplayName("Cliente Importa��o Feed Application")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialfeedapplication")
	public Material getMaterialfeedapplication() {
		return materialfeedapplication;
	}
	@DisplayName("Cliente Importa��o Feed Application")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendatipofeedapplication")
	public Pedidovendatipo getPedidovendatipofeedapplication() {
		return pedidovendatipofeedapplication;
	}
	@DisplayName("Cliente Importa��o Feed Application")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprazopagamentofeedapplication")
	public Prazopagamento getPrazopagamentofeedapplication() {
		return prazopagamentofeedapplication;
	}
	@DisplayName("Cliente Importa��o Feed Application")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentotipofeedapplication")
	public Documentotipo getDocumentotipofeedapplication() {
		return documentotipofeedapplication;
	}

	public void setClientefeedapplication(Cliente clientefeedapplication) {
		this.clientefeedapplication = clientefeedapplication;
	}

	public void setMaterialfeedapplication(Material materialfeedapplication) {
		this.materialfeedapplication = materialfeedapplication;
	}

	public void setPedidovendatipofeedapplication(Pedidovendatipo pedidovendatipofeedapplication) {
		this.pedidovendatipofeedapplication = pedidovendatipofeedapplication;
	}

	public void setPrazopagamentofeedapplication(Prazopagamento prazopagamentofeedapplication) {
		this.prazopagamentofeedapplication = prazopagamentofeedapplication;
	}

	public void setDocumentotipofeedapplication(Documentotipo documentotipofeedapplication) {
		this.documentotipofeedapplication = documentotipofeedapplication;
	}

	@DisplayName("Exibir IPI na venda")
	public Boolean getExibiripivenda() {
		return exibiripivenda;
	}

	public void setExibiripivenda(Boolean exibiripivenda) {
		this.exibiripivenda = exibiripivenda;
	}
	
	@DisplayName("Propriet�rios")
	@OneToMany(mappedBy="empresa")
	public List<Empresaproprietario> getListaEmpresaproprietario() {
		return listaEmpresaproprietario;
	}
	public void setListaEmpresaproprietario(
			List<Empresaproprietario> listaEmpresaproprietario) {
		this.listaEmpresaproprietario = listaEmpresaproprietario;
	}
	
	@MaxLength(4)
	@DisplayName("Integra��o Cont�bil")
	public String getIntegracaocontabil() {
		return integracaocontabil;
	}
	
	public void setIntegracaocontabil(String integracaocontabil) {
		this.integracaocontabil = integracaocontabil;
	}
	
	@DisplayName("S�rie NF-e")
	public Integer getSerienfe() {
		return serienfe;
	}
	
	public void setSerienfe(Integer serienfe) {
		this.serienfe = serienfe;
	}
	
	@DisplayName("V�nculo devolu��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontadevolucao")
	public Conta getContadevolucao() {
		return contadevolucao;
	}
	
	public void setContadevolucao(Conta contadevolucao) {
		this.contadevolucao = contadevolucao;
	}
	
	@DisplayName(" Modelos de Comprovantes RTF")
	@OneToMany(mappedBy="empresa")
	public List<Empresamodelocomprovanteorcamento> getListaEmpresamodelocomprovanteorcamento() {
		return listaEmpresamodelocomprovanteorcamento;
	}
	
	public void setListaEmpresamodelocomprovanteorcamento(
			List<Empresamodelocomprovanteorcamento> listaEmpresamodelocomprovanteorcamento) {
		this.listaEmpresamodelocomprovanteorcamento = listaEmpresamodelocomprovanteorcamento;
	}
	
	public Object retornaValorCampo(Field field){
		try {
			return field.get(this);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Transient
	@DisplayName("Transportador")
	public Fornecedor getPessoaconfiguracao_transportador() {
		return pessoaconfiguracao_transportador;
	}

	public void setPessoaconfiguracao_transportador(Fornecedor pessoaconfiguracaoTransportador) {
		pessoaconfiguracao_transportador = pessoaconfiguracaoTransportador;
	}

	@Transient
	public Fornecedor getTransportador(){
		return getPessoaconfiguracao() != null ? getPessoaconfiguracao().getTransportador() : null; 
	}
	
	@Transient
	public Colaborador getProprietarioRuralPrincipal(){
		if(Boolean.TRUE.equals(getPropriedadeRural()) && Hibernate.isInitialized(getListaEmpresaproprietario()) && SinedUtil.isListNotEmpty(getListaEmpresaproprietario())){
			for(Empresaproprietario empresaproprietario : getListaEmpresaproprietario()){
				if(Boolean.TRUE.equals(empresaproprietario.getPrincipal()) && empresaproprietario.getColaborador() != null && Hibernate.isInitialized(empresaproprietario.getColaborador())){
					return empresaproprietario.getColaborador();
				}
			}
		}
		return null;
	}
	
	@Transient
	public String getNomeProprietarioPrincipalOuEmpresa(){
		try {
			Colaborador colaborador = getProprietarioRuralPrincipal();
			if(colaborador != null){
				return colaborador.getNome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return getNomefantasia();
	}
	
	@Transient
	public String getNomeFantasiaProprietarioPrincipalOuEmpresa(){
//		try {
//			Colaborador colaborador = getProprietarioRuralPrincipal();
//			if(colaborador != null){
//				return colaborador.getNome();
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
		return getNomefantasia();
	}
	
	@Transient
	public String getCpfCnpjProprietarioPrincipalOuEmpresa(){
		try {
			Colaborador colaborador = getProprietarioRuralPrincipal();
			if(colaborador != null){
				return colaborador.getCpfCnpj();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return getCpfCnpj();
	}
	
	@Transient
	public String getCpfOuCnpjValueProprietarioPrincipalOuEmpresa(){
		try {
			Colaborador colaborador = getProprietarioRuralPrincipal();
			if(colaborador != null){
				return colaborador.getCpfOuCnpjValue();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return getCpfOuCnpjValue();
	}
	
	@Transient
	public String getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa(){
		try {
			Colaborador colaborador = getProprietarioRuralPrincipal();
			if(colaborador != null){
				return colaborador.getNome();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return getRazaosocialOuNome();
	}
	
	@DisplayName("C�digo da Empresa")
	public String getCodigoEmpresaDominioIntegracao() {
		return codigoEmpresaDominioIntegracao;
	}
	
	public void setCodigoEmpresaDominioIntegracao(String codigoEmpresaDominioIntegracao) {
		this.codigoEmpresaDominioIntegracao = codigoEmpresaDominioIntegracao;
	}
	
	@DisplayName("Pr�ximo identificador do carregamento")
	public Integer getProximoidentificadorcarregamento() {
		return proximoidentificadorcarregamento;
	}
	
	public void setProximoidentificadorcarregamento(
			Integer proximoidentificadorcarregamento) {
		this.proximoidentificadorcarregamento = proximoidentificadorcarregamento;
	}
	
	@Transient
	public String getNomeMunicipioUf() {
		if(StringUtils.isNotBlank(nomeMunicipioUf))	return nomeMunicipioUf;
		
		StringBuilder sb = new StringBuilder();
		if(listaEndereco != null && Hibernate.isInitialized(listaEndereco) && listaEndereco.size() > 0){
			Endereco endereco = getEndereco();
			if(endereco != null){
				sb.append(endereco.getMunicipioUf());
			}
		}
		return sb.toString();
	}
	
	public void setNomeMunicipioUf(String nomeMunicipioUf) {
		this.nomeMunicipioUf = nomeMunicipioUf;
	}
}