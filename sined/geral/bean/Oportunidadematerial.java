package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_oportunidadematerial", sequenceName = "sq_oportunidadematerial")
@DisplayName("Produtos e Servi�os")
public class Oportunidadematerial {

	protected Integer cdoportunidadematerial;
	protected Oportunidade oportunidade;
	protected Material material;
	protected Unidademedida unidademedida;
	protected Double valorunitario;
	protected Double quantidade;
	protected Integer prazoentregamaterial;
	protected Double desconto;
	protected Double comissao;
	protected String observacao;
	
	//TRANSIENT
	protected Double total;
	protected Double margcontribvalor;
	protected Double margcontribpercentual;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_oportunidadematerial")
	public Integer getCdoportunidadematerial() {
		return cdoportunidadematerial;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdoportunidade")
	public Oportunidade getOportunidade() {
		return oportunidade;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@Required
	@DisplayName("Unidade de Medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	@Required
	@DisplayName("Valor unit�rio")
	public Double getValorunitario() {
		return valorunitario;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	@DisplayName("Desconto %")
	public Double getDesconto() {
		return desconto;
	}
	@DisplayName("Comiss�o %")
	public Double getComissao() {
		return comissao;
	}
	@MaxLength(1000)
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	public void setCdoportunidadematerial(Integer cdoportunidadematerial) {
		this.cdoportunidadematerial = cdoportunidadematerial;
	}
	public void setOportunidade(Oportunidade oportunidade) {
		this.oportunidade = oportunidade;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	public void setComissao(Double comissao) {
		this.comissao = comissao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@Transient
	public Double getTotal() {
		if(this.getQuantidade() != null && this.getValorunitario() != null){
			return this.getQuantidade() * this.getValorunitario();
		}
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	
	@DisplayName("Prazo de Entrega (em dias)")
	public Integer getPrazoentregamaterial() {
		return prazoentregamaterial;
	}
	
	public void setPrazoentregamaterial(Integer prazoentregamaterial) {
		this.prazoentregamaterial = prazoentregamaterial;
	}
	
	@Transient
	@DisplayName("Margem Contrib.(R$)")
	public Double getMargcontribvalor() {
		return margcontribvalor;
	}
	@Transient
	@DisplayName("Margem Contrib.(%)")
	public Double getMargcontribpercentual() {
		return margcontribpercentual;
	}
	
	public void setMargcontribvalor(Double margcontribvalor) {
		this.margcontribvalor = margcontribvalor;
	}
	public void setMargcontribpercentual(Double margcontribpercentual) {
		this.margcontribpercentual = margcontribpercentual;
	}
}
