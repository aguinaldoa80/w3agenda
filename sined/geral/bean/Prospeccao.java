package br.com.linkcom.sined.geral.bean;


import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.annotations.CollectionOfElements;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoClienteEmpresa;
import br.com.linkcom.sined.util.SinedUtil;

@DisplayName("Prospecção")
@Entity
@SequenceGenerator(name = "sq_prospeccao", sequenceName = "sq_prospeccao")
public class Prospeccao implements Log, PermissaoClienteEmpresa{
	
	protected Integer cdprospeccao;
	protected Cliente cliente;
	protected Date data;
	protected Contato contato;
	protected Colaborador colaborador;
	protected String descricao;
	protected String observacao;
	protected Interacao interacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;	
	protected List<Proposta> listaProposta;

//	TRANSIENTES
	protected String propostas;
	protected Boolean situcaoAtencaoAtrasado;
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_prospeccao")
	public Integer getCdprospeccao() {
		return cdprospeccao;
	}
	public void setCdprospeccao(Integer cdProspeccao) {
		this.cdprospeccao = cdProspeccao;
	}
	@DisplayName("Contato")	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcontato")
	public Contato getContato() {
		return contato;
	}
	@DisplayName("Vendedor")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdvendedor")
	public Colaborador getColaborador() {
		return colaborador;
	}
	public void setContato(Contato contato) {
		this.contato = contato;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	@Required
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}			
	@Required
	@DisplayName("Cliente")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	@DisplayName("Interação")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdinteracao")
	public Interacao getInteracao() {
		return interacao;
	}
	public void setInteracao(Interacao interacao) {
		this.interacao = interacao;
	}
	@Required
	@MaxLength(100)
	@DisplayName("Descrição")
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	@MaxLength(1000)
	@DisplayName("Observações")
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	@OneToMany(mappedBy="prospeccao")
	@CollectionOfElements
	public List<Proposta> getListaProposta() {
		return listaProposta;
	}
	public void setListaProposta(List<Proposta> listaProposta) {
		this.listaProposta = listaProposta;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public String subQueryClienteEmpresa(){
		return  "select prospeccaoSubQueryClienteEmpresa.cdprospeccao " +
				"from Prospeccao prospeccaoSubQueryClienteEmpresa " +
				"left outer join prospeccaoSubQueryClienteEmpresa.cliente clienteSubQueryClienteEmpresa " +
				"where true = permissao_clienteempresa(clienteSubQueryClienteEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}
	
//	TRANSIENTES
	@Transient
	public String getPropostas() {
		return getListaProposta() != null ? CollectionsUtil.listAndConcatenate(getListaProposta(), "numeroano", " - ") : "";
	}
	@Transient
	public Boolean getSitucaoAtencaoAtrasado() {
		return situcaoAtencaoAtrasado;
	}
	
	public void setPropostas(String propostas) {
		this.propostas = propostas;
	}
	public void setSitucaoAtencaoAtrasado(Boolean situcaoAtencaoAtrasado) {
		this.situcaoAtencaoAtrasado = situcaoAtencaoAtrasado;
	}
}
