package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_regiao", sequenceName = "sq_regiao")
public class Regiao implements Log {

	protected Integer cdregiao;
	protected String nome;
	protected Boolean ativo = Boolean.TRUE;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Set<Regiaolocal> listaRegiaolocal = new ListSet<Regiaolocal>(Regiaolocal.class);
	protected Set<Regiaovendedor> listaRegiaovendedor = new ListSet<Regiaovendedor>(Regiaovendedor.class);
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_regiao")
	public Integer getCdregiao() {
		return cdregiao;
	}

	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setCdregiao(Integer cdregiao) {
		this.cdregiao = cdregiao;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
//	LISTAS
	
	@DisplayName("Locais")
	@OneToMany(mappedBy="regiao")
	public Set<Regiaolocal> getListaRegiaolocal() {
		return listaRegiaolocal;
	}
	
	@DisplayName("Vendedor")
	@OneToMany(mappedBy="regiao")
	public Set<Regiaovendedor> getListaRegiaovendedor() {
		return listaRegiaovendedor;
	}
	
	public void setListaRegiaolocal(Set<Regiaolocal> listaRegiaolocal) {
		this.listaRegiaolocal = listaRegiaolocal;
	}
	
	public void setListaRegiaovendedor(Set<Regiaovendedor> listaRegiaovendedor) {
		this.listaRegiaovendedor = listaRegiaovendedor;
	}

//	LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}