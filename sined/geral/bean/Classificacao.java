package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.view.Vclassificacao;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_classificacao", sequenceName = "sq_classificacao")
public class Classificacao implements Log{

	protected Integer cdclassificacao;
	protected Classificacao classificacaosuperior;
	protected Integer item;
	protected String formato = "00";
	protected String nome;
	protected String observacao;
	protected Boolean ativo = Boolean.TRUE;
	protected Integer cdusuarioaltera;
  	protected Timestamp dtaltera;
  	protected Vclassificacao vclassificacao;
	
  	//TRANSIENT
  	private Classificacao pai;
  	private List<Classificacao> filhos = new ArrayList<Classificacao>();
  	private String descricaoInputPersonalizado;
  	private Boolean analiticacomregistros;
  	
  	public Classificacao() {
	}
  	
  	public Classificacao(String nome){
		this.nome = nome;
	}
  	
  	public Classificacao(Integer cdclassificacao) {
		this.cdclassificacao = cdclassificacao;
	}
  	
	public Classificacao(Integer cdclassificacao, String nome) {
		this.cdclassificacao = cdclassificacao;
		this.nome = nome;
	}
	
	public Classificacao(Integer cdclassificacao, String nome, String identificador) {
		this.cdclassificacao = cdclassificacao;
		this.nome = nome;
		
		Vclassificacao vclassificacao = new Vclassificacao();
		vclassificacao.setIdentificador(identificador);
		this.vclassificacao = vclassificacao;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_classificacao")
  	public Integer getCdclassificacao() {
		return cdclassificacao;
	}
  	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Classificação superior")
	@JoinColumn(name="cdclassificacaosuperior")
	public Classificacao getClassificacaosuperior() {
		return classificacaosuperior;
	}
  	@DisplayName("Item")
	public Integer getItem() {
		return item;
	}
	@MaxLength(4)
	@DisplayName("Formato (Zeros)")
	public String getFormato() {
		return formato;
	}
	@Required
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	@MaxLength(500)
	@DisplayName("Observação")
	public String getObservacao() {
		return observacao;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdclassificacao", insertable=false, updatable=false)
	public Vclassificacao getVclassificacao() {
		return vclassificacao;
	}
	
	public void setCdclassificacao(Integer cdclassificacao) {
		this.cdclassificacao = cdclassificacao;
	}
	public void setClassificacaosuperior(Classificacao classificacaosuperior) {
		this.classificacaosuperior = classificacaosuperior;
	}
	public void setItem(Integer item) {
		this.item = item;
	}
	public void setFormato(String formato) {
		this.formato = formato;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setVclassificacao(Vclassificacao vclassificacao) {
		this.vclassificacao = vclassificacao;
	}

	@Transient
	public Classificacao getPai() {
		return pai;
	}
	@Transient
	public List<Classificacao> getFilhos() {
		return filhos;
	}
	@Transient
	public String getDescricaoInputPersonalizado(){
		if(getVclassificacao() != null){
			this.descricaoInputPersonalizado = getVclassificacao().getIdentificador() + " - " + getNome() + (getVclassificacao().getArvorepai() != null ? " (" + getVclassificacao().getArvorepai() + ")" : "");
		}
		return this.descricaoInputPersonalizado;
	}
	@Transient
	public Boolean getAnaliticacomregistros() {
		return analiticacomregistros;
	}
	
	public void setPai(Classificacao pai) {
		this.pai = pai;
	}
	public void setFilhos(List<Classificacao> filhos) {
		this.filhos = filhos;
	}
	public void setDescricaoInputPersonalizado(String descricaoInputPersonalizado) {
		this.descricaoInputPersonalizado = descricaoInputPersonalizado;
	}
	public void setAnaliticacomregistros(Boolean analiticacomregistros) {
		this.analiticacomregistros = analiticacomregistros;
	}
	
	@Transient
	@DescriptionProperty(usingFields={"nome","vclassificacao"})
	public String getDescricaoCombo(){
		return (getVclassificacao() == null || getVclassificacao().getArvorepai() == null || getVclassificacao().getArvorepai().equals("") ? getNome() : (getNome()+" ("+getVclassificacao().getArvorepai()+")"));
	}
	
	@Transient	
	public String getDescricao() {
		String pais = "";
		Classificacao pai = this.getClassificacaosuperior();
		while(pai != null){
			if (!pais.equals("")) {
				pais = pai.getNome() + ">" + pais;
			} else {
				pais = pai.getNome();
			}
			pai = pai.getClassificacaosuperior();			
		}
		return this.getNome() + (!pais.equals("") ? " (" + pais + ")" : "");
	}
	
}
