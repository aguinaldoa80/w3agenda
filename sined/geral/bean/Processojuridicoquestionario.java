package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesposta;

@Entity
@SequenceGenerator(name = "sq_processojuridicoquestionario", sequenceName = "sq_processojuridicoquestionario")
public class Processojuridicoquestionario implements Comparable<Processojuridicoquestionario> {
	
	protected Integer cdprocessojuridicoquestionario;
	protected Processojuridico processojuridico;
	protected Integer ordem;
	protected String pergunta;
	protected Tiporesposta tiporesposta;
	protected String resposta;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_processojuridicoquestionario")
	public Integer getCdprocessojuridicoquestionario() {
		return cdprocessojuridicoquestionario;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprocessojuridico")
	public Processojuridico getProcessojuridico() {
		return processojuridico;
	}
	
	@Required
	@MaxLength(200)
	@DisplayName("Pergunta")
	public String getPergunta() {
		return pergunta;
	}
	
	@MaxLength(10)
	@Required
	public Integer getOrdem() {
		return ordem;
	}
	
	@Required
	public Tiporesposta getTiporesposta() {
		return tiporesposta;
	}

	public String getResposta() {
		return resposta;
	}

	public void setCdprocessojuridicoquestionario(
			Integer cdprocessojuridicoquestionario) {
		this.cdprocessojuridicoquestionario = cdprocessojuridicoquestionario;
	}
	
	public void setProcessojuridico(Processojuridico processojuridico) {
		this.processojuridico = processojuridico;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public void setPergunta(String pergunta) {
		this.pergunta = pergunta;
	}

	public void setTiporesposta(Tiporesposta tiporesposta) {
		this.tiporesposta = tiporesposta;
	}

	public void setResposta(String resposta) {
		this.resposta = resposta;
	}
	
	@Transient
	public Boolean getRespostaBoolean(){
		try{
			return Boolean.valueOf(resposta);
		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}
	
	public void setRespostaBoolean(Boolean resposta){
		if(resposta != null){
			this.resposta = resposta.toString();
		}
	}

	public int compareTo(Processojuridicoquestionario o) {
		return this.getOrdem().compareTo(o.getOrdem());
	}	
}
