package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivofolhaacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_arquivofolhahistorico", sequenceName = "sq_arquivofolhahistorico")
public class Arquivofolhahistorico implements Log {
	
	protected Integer cdarquivofolhahistorico;
	protected Arquivofolha arquivofolha;
	protected Arquivofolhaacao acao;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	// TRANSIENTES
	protected String whereIn;
	protected Boolean entrada;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivofolhahistorico")
	public Integer getCdarquivofolhahistorico() {
		return cdarquivofolhahistorico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivofolha")
	public Arquivofolha getArquivofolha() {
		return arquivofolha;
	}

	@DisplayName("A��o")
	public Arquivofolhaacao getAcao() {
		return acao;
	}

	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdarquivofolhahistorico(Integer cdarquivofolhahistorico) {
		this.cdarquivofolhahistorico = cdarquivofolhahistorico;
	}

	public void setArquivofolha(Arquivofolha arquivofolha) {
		this.arquivofolha = arquivofolha;
	}

	public void setAcao(Arquivofolhaacao acao) {
		this.acao = acao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}

	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	
	@Transient
	public Boolean getEntrada() {
		return entrada;
	}

	public void setEntrada(Boolean entrada) {
		this.entrada = entrada;
	}

	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	
	

	
}