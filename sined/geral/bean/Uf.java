package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "sq_uf", sequenceName = "sq_uf")
public class Uf implements Log {

	protected Integer cduf;
	protected String nome;
	protected String sigla;
	protected Integer cdibge;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	public static final Uf MINAS_GERAIS = new Uf(13);
	public static final Uf EXTERIOR_MDFE = new Uf("EX", 99);

	public Uf() {
	}

	public Uf(Integer cduf) {
		this.cduf = cduf;
	}
	
	public Uf(String sigla, Integer cdibge) {
		this.sigla = sigla;
		this.cdibge = cdibge;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_uf")
	public Integer getCduf() {
		return cduf;
	}

	public void setCduf(Integer id) {
		this.cduf = id;
	}

	public Integer getCdibge() {
		return cdibge;
	}

	public void setCdibge(Integer cdibge) {
		this.cdibge = cdibge;
	}

	@Required
	@MaxLength(50)
	public String getNome() {
		return nome;
	}

	@Required
	@MaxLength(2)
	@DescriptionProperty
	public String getSigla() {
		return sigla;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
