package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_materialnumeroserie", sequenceName = "sq_materialnumeroserie")
public class Materialnumeroserie {

	protected Integer cdmaterialnumeroserie;
	protected Material material;
	protected String numero;
	protected Entrega entrega;
	protected Notafiscalproduto notafiscalproduto;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialnumeroserie")
	public Integer getCdmaterialnumeroserie() {
		return cdmaterialnumeroserie;
	}
	@DisplayName("Material")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@Required
	@DescriptionProperty
	@DisplayName("N�mero")
	@MaxLength(100)
	public String getNumero() {
		return numero;
	}
	@DisplayName("Compra")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentrega")
	public Entrega getEntrega() {
		return entrega;
	}
	@DisplayName("Nota Fiscal Produto")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnota")
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}
		
	
	public void setCdmaterialnumeroserie(Integer cdmaterialnumeroserie) {
		this.cdmaterialnumeroserie = cdmaterialnumeroserie;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setEntrega(Entrega entrega) {
		this.entrega = entrega;
	}
	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}
}
