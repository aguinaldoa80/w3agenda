package br.com.linkcom.sined.geral.bean;


import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoCampoEnum;


@Entity
@SequenceGenerator(name="sq_bancoconfiguracaoretornocampo",sequenceName="sq_bancoconfiguracaoretornocampo")
public class BancoConfiguracaoRetornoCampo {

	private Integer cdbancoconfiguracaoretornocampo;
	private BancoConfiguracaoRetorno bancoConfiguracaoRetorno;
	private BancoConfiguracaoCampoEnum campo;
	private BancoConfiguracaoRetornoSegmento bancoConfiguracaoRetornoSegmento;
	private BancoConfiguracaoRetornoSegmentoDetalhe bancoConfiguracaoRetornoSegmentoDetalhe;
	private Integer posIni;
	private Integer tamanho;
	private Integer qtdeDecimal;
	private String formatoData;
	private boolean calculado = false;
	
	//Transient
	private String msgEstrutura;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoconfiguracaoretornocampo")
	public Integer getCdbancoconfiguracaoretornocampo() {
		return cdbancoconfiguracaoretornocampo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaoretorno")
	@Required
	public BancoConfiguracaoRetorno getBancoConfiguracaoRetorno() {
		return bancoConfiguracaoRetorno;
	}
	
	public BancoConfiguracaoCampoEnum getCampo() {
		return campo;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaoretornosegmento")
	@DisplayName("Segmento")
	public BancoConfiguracaoRetornoSegmento getBancoConfiguracaoRetornoSegmento() {
		return bancoConfiguracaoRetornoSegmento;
	}

	public Integer getPosIni() {
		return posIni;
	}

	public Integer getTamanho() {
		return tamanho;
	}

	public Integer getQtdeDecimal() {
		return qtdeDecimal;
	}

	public String getFormatoData() {
		return formatoData;
	}

	public boolean isCalculado() {
		return calculado;
	}

	public void setCdbancoconfiguracaoretornocampo(
			Integer cdbancoconfiguracaoretornocampo) {
		this.cdbancoconfiguracaoretornocampo = cdbancoconfiguracaoretornocampo;
	}

	public void setBancoConfiguracaoRetorno(
			BancoConfiguracaoRetorno bancoConfiguracaoRetorno) {
		this.bancoConfiguracaoRetorno = bancoConfiguracaoRetorno;
	}
	
	public void setCampo(BancoConfiguracaoCampoEnum campo) {
		this.campo = campo;
	}

	public void setBancoConfiguracaoRetornoSegmento(
			BancoConfiguracaoRetornoSegmento bancoConfiguracaoRetornoSegmento) {
		this.bancoConfiguracaoRetornoSegmento = bancoConfiguracaoRetornoSegmento;
	}

	public void setPosIni(Integer posIni) {
		this.posIni = posIni;
	}

	public void setTamanho(Integer tamanho) {
		this.tamanho = tamanho;
	}

	public void setQtdeDecimal(Integer qtdeDecimal) {
		this.qtdeDecimal = qtdeDecimal;
	}

	public void setFormatoData(String formatoData) {
		this.formatoData = formatoData;
	}

	public void setCalculado(boolean calculado) {
		this.calculado = calculado;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdBancoConfiguracaoRetornoSegmentoDetalhe")
	public BancoConfiguracaoRetornoSegmentoDetalhe getBancoConfiguracaoRetornoSegmentoDetalhe() {
		return bancoConfiguracaoRetornoSegmentoDetalhe;
	}
	
	public void setBancoConfiguracaoRetornoSegmentoDetalhe(
			BancoConfiguracaoRetornoSegmentoDetalhe bancoConfiguracaoRetornoSegmentoDetalhe) {
		this.bancoConfiguracaoRetornoSegmentoDetalhe = bancoConfiguracaoRetornoSegmentoDetalhe;
	}
	
	
	//Transient
	



	private boolean obrigatorio = false;
	private boolean campoData = false;
	private boolean campoValor = false;
	private boolean campoCalculado = false;

	@Transient
	public boolean isObrigatorio() {
		return obrigatorio;
	}

	@Transient
	public boolean isCampoData() {
		return campoData;
	}
	
	@Transient
	public boolean isCampoValor() {
		return campoValor;
	}

	@Transient
	public boolean isCampoCalculado() {
		return campoCalculado;
	}

	public void setCampoValor(boolean campoValor) {
		this.campoValor = campoValor;
	}

	public void setCampoCalculado(boolean campoCalculado) {
		this.campoCalculado = campoCalculado;
	}

	public void setObrigatorio(boolean obrigatorio) {
		this.obrigatorio = obrigatorio;
	}

	public void setCampoData(boolean campoData) {
		this.campoData = campoData;
	}
	
	public String getValorString(String linha){
		String valor = null;		
		if (getPosIni() != null && getTamanho() != null){
			Integer posFim = getPosIni() + getTamanho() - 1;
			if (linha.length() >= posFim){
				valor = linha.substring(getPosIni() - 1, posFim);
			}
		}		
		return valor != null ? valor.trim() : null;		
	}
	
	public Money getValorMoney(String linha){
		Money money = null;
		try {
			String valor = getValorString(linha);
			if (getCampo() != null && valor != null){
				if (getCampo().isValor()){
					if (getQtdeDecimal() != null && getQtdeDecimal() > 0){
						Integer pos = getTamanho() - getQtdeDecimal();
						String parteInteira = valor.substring(0, pos);
						String parteDecimal = valor.substring(pos, getTamanho());
						return new Money(Float.parseFloat(parteInteira + "." + parteDecimal));
					} else {
						return new Money(Long.parseLong(valor));
					}
				}
			}
		} catch (Exception e) {
		}
		
		if (money==null && getPosIni()!=null && getTamanho()!=null){
			msgEstrutura = campo.getNome() + ", ";
		}
		
		return money;
	}

	public Long getValorLong(String linha){
		try {
			String valor = getValorString(linha);
			if (getCampo() != null && valor != null){
				if (getCampo().isValor()){
					return new Long(valor);
				} else
					return null;
			} else 
				return null;	
		} catch (Exception e) {
			return null;
		}		
	}
	
	public Date getValorDate(String linha){
		return getValorDate(linha, true);
	}
	
	public Date getValorDate(String linha, boolean obrigatorio){
		Date date = null;
		try {
			String valor = getValorString(linha);
			String valorAux = valor != null ? valor.replaceAll("0", "") : null;
			if (getCampo().isData() && valorAux != null && valorAux.length() > 0){
				String formato = getFormatoData() != null ? getFormatoData() : "ddMMyy";
				SimpleDateFormat sdf = new SimpleDateFormat(formato);
				date = sdf.parse(valor);
			} 
		} catch (Exception e) {
		}
		
		if (date==null && getPosIni()!=null && getTamanho()!=null && obrigatorio){
			msgEstrutura = campo.getNome() + ", ";
		}
		
		return date;
	}
	
	@Transient
	public String getMsgEstrutura() {
		return msgEstrutura;
	}
	
	public void setMsgEstrutura(String msgEstrutura) {
		this.msgEstrutura = msgEstrutura;
	}
}