package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@SequenceGenerator(name = "sq_classificacaocarteira", sequenceName = "sq_classificacaocarteira")
public class Tipocarteiratrabalho {

	protected Integer cdtipocarteiratrabalho;
	protected String nome;

	public static final Integer DEFINITIVO = 1;
	public static final Integer PROVISORIO = 2;
	public static final Integer TECNICO = 3;
	public static final Integer NENHUMA = 4;
	
	public Tipocarteiratrabalho(){}
	
	public Tipocarteiratrabalho(Integer cdtipocarteiratrabalho){
		this.cdtipocarteiratrabalho = cdtipocarteiratrabalho;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_classificacaocarteira")
	public Integer getCdtipocarteiratrabalho() {
		return cdtipocarteiratrabalho;
	}
	public void setCdtipocarteiratrabalho(Integer id) {
		this.cdtipocarteiratrabalho = id;
	}

	
	@Required
	@MaxLength(10)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	
	public void setNome(String nome) {
		this.nome = nome;
	}

}
