package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.CreditoDebitoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.OrigemDocumentoEnum;

@Entity
@SequenceGenerator(name = "sq_ajusteipi", sequenceName = "sq_ajusteipi")
@DisplayName("Ajuste de IPI")
public class AjusteIpi {

	private Integer cdAjusteIpi;
	private Empresa empresa;
	private Date dtFatoGerador;
	private CreditoDebitoEnum tipoAjuste;
	private CodigoAjusteIpi codigoAjuste;
	private Money valor;
	private OrigemDocumentoEnum origemDocumento;
	private String numeroDocumento;
	private String descricaoComplementar;
	private List<AjusteIpiDocumento> ajusteIpiDocumentoList;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ajusteipi")
	public Integer getCdAjusteIpi() {
		return cdAjusteIpi;
	}

	public void setCdAjusteIpi(Integer cdAjusteIpi) {
		this.cdAjusteIpi = cdAjusteIpi;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Required
	@DisplayName("Data do Fato Gerador")
	public Date getDtFatoGerador() {
		return dtFatoGerador;
	}

	public void setDtFatoGerador(Date dtFatoGerador) {
		this.dtFatoGerador = dtFatoGerador;
	}

	@Required
	@DisplayName("Tipo de Ajuste")
	public CreditoDebitoEnum getTipoAjuste() {
		return tipoAjuste;
	}

	public void setTipoAjuste(CreditoDebitoEnum tipoAjuste) {
		this.tipoAjuste = tipoAjuste;
	}

	@Required
	@DisplayName("C�digo de Ajuste")
	@JoinColumn(name="cdcodigoajuste")
	@ManyToOne(fetch=FetchType.LAZY)
	public CodigoAjusteIpi getCodigoAjuste() {
		return codigoAjuste;
	}

	public void setCodigoAjuste(CodigoAjusteIpi codigoAjuste) {
		this.codigoAjuste = codigoAjuste;
	}

	@Required
	public Money getValor() {
		return valor;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	@Required
	@DisplayName("Origem do Documento")
	public OrigemDocumentoEnum getOrigemDocumento() {
		return origemDocumento;
	}

	public void setOrigemDocumento(OrigemDocumentoEnum origemDocumento) {
		this.origemDocumento = origemDocumento;
	}

	@MaxLength(10)
	@DisplayName("N�mero do Documento")
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	@Required
	@MaxLength(500)
	@DisplayName("Descri��o Complementar")
	public String getDescricaoComplementar() {
		return descricaoComplementar;
	}

	public void setDescricaoComplementar(String descricaoComplementar) {
		this.descricaoComplementar = descricaoComplementar;
	}
	
	@DisplayName("Documentos Fiscais")
	@OneToMany(mappedBy="ajusteIpi")
	public List<AjusteIpiDocumento> getAjusteIpiDocumentoList() {
		return ajusteIpiDocumentoList;
	}
	
	public void setAjusteIpiDocumentoList(List<AjusteIpiDocumento> ajusteIpiDocumentoList) {
		this.ajusteIpiDocumentoList = ajusteIpiDocumentoList;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
