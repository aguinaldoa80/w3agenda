package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.ibm.icu.text.SimpleDateFormat;

import br.com.correios.webservice.resource.Eventos;
import br.com.linkcom.sined.util.SinedDateUtils;

@Entity
@SequenceGenerator(name="sq_expedicaoacompanhamentoentrega", sequenceName="sq_expedicaoacompanhamentoentrega")
public class ExpedicaoAcompanhamentoEntrega {

	private Integer cdExpedicaoAcompanhamentoEntrega;
	private Expedicao expedicao;
	private Timestamp dataAcompanhamento;
	private String nome;
	private String categoria;
	private String tipoEvento;
	private String localizacao;
	private String codigo;
	private String cidade;
	private String uf;
	private String descricaoEvento;
	private String observacao;
	private String urlRastreamento;
	private String rastreamento;
	private EventosCorreios eventoCorreios;
	private Integer ordem;
	
	private String dataAcompanhamentoTrans;
	
	public ExpedicaoAcompanhamentoEntrega(){
		
	}
	
	public ExpedicaoAcompanhamentoEntrega(Eventos evento){
		this.setCidade(evento.getCidade());
		this.setCodigo(evento.getCodigo());
		try {
			this.setDataAcompanhamento(new Timestamp(new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(evento.getData()+" "+evento.getHora()).getTime()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setDescricaoEvento(evento.getDescricao());
		this.setLocalizacao(evento.getLocal());
		this.setUf(evento.getUf());
		this.setObservacao(evento.getDetalhe());
		this.setTipoEvento(evento.getStatus());
	}
	
	@Id
	@GeneratedValue(generator="sq_expedicaoacompanhamentoentrega", strategy=GenerationType.AUTO)
	public Integer getCdExpedicaoAcompanhamentoEntrega() {
		return cdExpedicaoAcompanhamentoEntrega;
	}
	public void setCdExpedicaoAcompanhamentoEntrega(Integer cdExpedicaoAcompanhamentoEntrega) {
		this.cdExpedicaoAcompanhamentoEntrega = cdExpedicaoAcompanhamentoEntrega;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdexpedicao")
	public Expedicao getExpedicao() {
		return expedicao;
	}
	public void setExpedicao(Expedicao expedicao) {
		this.expedicao = expedicao;
	}
	public Timestamp getDataAcompanhamento() {
		return dataAcompanhamento;
	}
	public void setDataAcompanhamento(Timestamp data) {
		this.dataAcompanhamento = data;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getTipoEvento() {
		return tipoEvento;
	}
	public void setTipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}
	public String getLocalizacao() {
		return localizacao;
	}
	public void setLocalizacao(String local) {
		this.localizacao = local;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getDescricaoEvento() {
		return descricaoEvento;
	}
	public void setDescricaoEvento(String descricaoEvento) {
		this.descricaoEvento = descricaoEvento;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getRastreamento() {
		return rastreamento;
	}
	public void setRastreamento(String rastreamento) {
		this.rastreamento = rastreamento;
	}
	public String getUrlRastreamento() {
		return urlRastreamento;
	}
	public void setUrlRastreamento(String urlRastreamento) {
		this.urlRastreamento = urlRastreamento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdeventoscorreios")
	public EventosCorreios getEventoCorreios() {
		return eventoCorreios;
	}
	public void setEventoCorreios(EventosCorreios eventoCorreios) {
		this.eventoCorreios = eventoCorreios;
	}
	public Integer getOrdem() {
		return ordem;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	
	@Transient
	public String getDataAcompanhamentoTrans() {
		if(dataAcompanhamento != null){
			dataAcompanhamentoTrans = SinedDateUtils.toString(dataAcompanhamento);
		}
		return dataAcompanhamentoTrans;
	}
	public void setDataAcompanhamentoTrans(String dataAcompanhamentoTrans) {
		this.dataAcompanhamentoTrans = dataAcompanhamentoTrans;
	}
}
