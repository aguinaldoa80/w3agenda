package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Password;
import br.com.linkcom.neo.validation.annotation.Email;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.neo.validation.annotation.ValidationOverride;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_candidato", sequenceName = "sq_candidato")
public class Candidato implements Log{
	
	protected Integer cdcandidato;
	protected Sexo sexo;
	protected Estadocivil estadocivil;
	protected Candidatosituacao candidatosituacao;
	protected Cpf cpf;
	protected String nome;
	protected String email;
	protected String senha;
	protected br.com.linkcom.neo.types.Telefone telefone;
	protected br.com.linkcom.neo.types.Telefone celular;
	protected Date dtnascimento;
	protected String objetivo;
	protected ClassificacaoCandidato classificacaoCandidato;
	
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	protected Uf uf;
	protected Municipio municipio;
	protected String logradouro;
	protected String bairro;
	protected Cep cep;
	protected String caixapostal;
	protected String numero;
	protected String complemento;
	
	protected Grauinstrucao grauinstrucao;
	protected String curso;
	protected String instituicao;
	protected String formacaoprofissional;
	protected Cargo cargo;

	protected Boolean iscontratado = false;
	
	protected String entrevista;
	
	protected Arquivo arquivo;
		
	
	
	public Candidato() {
	}
	
	public Candidato(Integer cdcandidato){
		this.cdcandidato = cdcandidato;
	}
	
	/*
	 * CONSTRUTOR USADO NO updateListagemQuery
	 */

	public Candidato(
			Integer candidato_cdcandidato,
			String candidato_nome, 
			Sexo candidato_sexo, 
			Candidatosituacao candidato_cadidatosituacao,
			String candidato_situacao,
			Municipio candidato_municipio,
			String municipio_nome,
			String candidato_curso,
			Grauinstrucao candidato_grauinstrucao,
			String candidato_email,
			String candidato_instituicao,
			String candidato_objetivo,
			Cargo _cargo,
			String _cargoNome){
		
		this.cdcandidato = candidato_cdcandidato;
		this.nome = candidato_nome;
		this.sexo = candidato_sexo;
		this.candidatosituacao = candidato_cadidatosituacao;
		this.candidatosituacao.setDescricao(candidato_situacao);
		this.municipio = candidato_municipio;
		this.municipio.setNome(municipio_nome);
		this.curso = candidato_curso;
		this.grauinstrucao= candidato_grauinstrucao;
		this.email = candidato_email;
		this.instituicao = candidato_instituicao;
		this.objetivo = candidato_objetivo;
		this.cargo = _cargo;
		if (cargo != null)
			this.cargo.setNome(_cargoNome);
		
	}
	

	
	  //*************//
	 //**Get & Set**//
	//*************//
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_candidato")
	public Integer getCdcandidato() {
		return cdcandidato;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsexo")
	@DisplayName("Sexo")
	public Sexo getSexo() {
		return sexo;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdestadocivil")
	@DisplayName("Estado civil")
	public Estadocivil getEstadocivil() {
		return estadocivil;
	}
	
	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcandidatosituacao")
	@DisplayName("Situa��o")
	public Candidatosituacao getCandidatosituacao() {
		return candidatosituacao;
	}
	
	@DisplayName("CPF")
	public Cpf getCpf() {
		return cpf;
	}
	
	@Required
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	
	@MaxLength(50)
	@Email
	@DisplayName("E-mail")
	public String getEmail() {
		return email;
	}
	
	@Password
	@MaxLength(6)
	public String getSenha() {
		return senha;
	}
	
	@MaxLength(20)
	public br.com.linkcom.neo.types.Telefone getTelefone() {
		return telefone;
	}
	
	@MaxLength(20)
	public br.com.linkcom.neo.types.Telefone  getCelular() {
		return celular;
	}
	
	@DisplayName("Data Nascimento")
	public Date getDtnascimento() {
		return dtnascimento;
	}
	
	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@Required
	@MaxLength(200)
	public String getObjetivo() {
		return objetivo;
	}
	@Required
	@OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="cdmunicipio")
    @DisplayName("Munic�pio")
    @ValidationOverride(field="municipio.uf")
	public Municipio getMunicipio() {
		return municipio;
	}
	
	@Required
	@Transient
	public Uf getUf() {
		return uf;
	}
	
	@MaxLength(100)
	public String getLogradouro() {
		return logradouro;
	}
	
	@MaxLength(50)
	public String getBairro() {
		return bairro;
	}
	
	@MaxLength(10)
	@DisplayName("CEP")
	public Cep getCep() {
		return cep;
	}
	
	@MaxLength(50)
	@DisplayName("Caixa Postal")
	public String getCaixapostal() {
		return caixapostal;
	}
	
	@MaxLength(5)
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}
	
	@MaxLength(50)
	@DisplayName("Complemento")
	public String getComplemento() {
		return complemento;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgrauinstrucao")
	@DisplayName("Escolaridade")
	public Grauinstrucao getGrauinstrucao() {
		return grauinstrucao;
	}
	
	@MaxLength(100)
	public String getCurso() {
		return curso;
	}
	
	@MaxLength(100)
	@DisplayName("Institui��o")
	public String getInstituicao() {
		return instituicao;
	}
	
	@Required
	@DisplayName("Forma��o Profissional")
	public String getFormacaoprofissional() {
		return formacaoprofissional;
	}
	
	@DisplayName("Entrevista")
	public String getEntrevista() {
		return entrevista;
	}
	
	@DisplayName("Arquivo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}

	@DisplayName("Classifica��o Candidato")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdclassificacaocandidato")
	public ClassificacaoCandidato getClassificacaoCandidato() {
		return classificacaoCandidato;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setCdcandidato(Integer cdcandidato) {
		this.cdcandidato = cdcandidato;
	}
	
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	
	public void setEstadocivil(Estadocivil estadocivil) {
		this.estadocivil = estadocivil;
	}
	
	public void setCandidatosituacao(Candidatosituacao candidatosituacao) {
		this.candidatosituacao = candidatosituacao;
	}
	
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public void setTelefone(br.com.linkcom.neo.types.Telefone telefone) {
		this.telefone = telefone;
	}
	
	public void setCelular(br.com.linkcom.neo.types.Telefone celular) {
		this.celular = celular;
	}
	
	public void setDtnascimento(Date dtnascimento) {
		this.dtnascimento = dtnascimento;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}
	
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	
	public void setCep(Cep cep) {
		this.cep = cep;
	}
	
	public void setCaixapostal(String caixapostal) {
		this.caixapostal = caixapostal;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
		
	public void setGrauinstrucao(Grauinstrucao grauinstrucao) {
		this.grauinstrucao = grauinstrucao;
	}
	
	public void setCurso(String curso) {
		this.curso = curso;
	}
	
	public void setInstituicao(String instituicao) {
		this.instituicao = instituicao;
	}
	
	public void setFormacaoprofissional(String formacaoprofissional) {
		this.formacaoprofissional = formacaoprofissional;
	}
	
	
	
	@Transient
	public Boolean getIscontratado() {
		return iscontratado;
	}
	
	public void setIscontratado(Boolean iscontratado) {
		this.iscontratado = iscontratado;
	}
	public void setEntrevista(String entrevista) {
		this.entrevista = entrevista;
	}

	@DisplayName("Cargo")
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="cdcargo")
	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public void setClassificacaoCandidato(
			ClassificacaoCandidato classificacaoCandidato) {
		this.classificacaoCandidato = classificacaoCandidato;
	}
	
}
