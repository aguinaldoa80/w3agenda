package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name="sq_propriedaderuraltelefone", sequenceName="sq_propriedaderuraltelefone")
public class PropriedadeRuralTelefone {
	protected Integer cdPropriedadeRuralTelefone;
	protected PropriedadeRural propriedadeRural;
	protected Telefonetipo telefoneTipo;
	protected String telefone;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator="sq_propriedaderuraltelefone")
	public Integer getCdPropriedadeRuralTelefone() {
		return cdPropriedadeRuralTelefone;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdPropriedadeRural")
	public PropriedadeRural getPropriedadeRural() {
		return propriedadeRural;
	}
	
	@DisplayName("Tipo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtelefonetipo")
	public Telefonetipo getTelefoneTipo() {
		return telefoneTipo;
	}
	
	@DisplayName("Telefone")
	public String getTelefone() {
		return telefone;
	}
	
	public void setCdPropriedadeRuralTelefone(Integer cdPropriedadeRuralTelefone) {
		this.cdPropriedadeRuralTelefone = cdPropriedadeRuralTelefone;
	}
	public void setPropriedadeRural(PropriedadeRural propriedadeRural) {
		this.propriedadeRural = propriedadeRural;
	}
	public void setTelefoneTipo(Telefonetipo telefoneTipo) {
		this.telefoneTipo = telefoneTipo;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
}
