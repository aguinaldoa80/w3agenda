package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@SequenceGenerator(name = "sq_regimecontratacao", sequenceName = "sq_regimecontratacao")
public class Regimecontratacao {

	public static final Regimecontratacao AUTONOMO = new Regimecontratacao(2);
	public static final Regimecontratacao ESTAGIARIO = new Regimecontratacao(13);
	public static final Regimecontratacao EMPREGADO = new Regimecontratacao(16);
	public static final Regimecontratacao SOCIO = new Regimecontratacao(19);
	public static final Regimecontratacao TRANSPORTADOR = new Regimecontratacao(21);
	public static final Regimecontratacao TERCEIRIZADO = new Regimecontratacao(22);
	
	protected Integer cdregimecontratacao;
	protected String nome;
	
	public Regimecontratacao(){
	}
	public Regimecontratacao(Integer cdregimecontratacao){
		this.cdregimecontratacao = cdregimecontratacao;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_regimecontratacao")
	public Integer getCdregimecontratacao() {
		return cdregimecontratacao;
	}
	public void setCdregimecontratacao(Integer id) {
		this.cdregimecontratacao = id;
	}

	
	@Required
	@MaxLength(15)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Regimecontratacao) {
			Regimecontratacao rc = (Regimecontratacao) obj;
			return this.getCdregimecontratacao().equals(rc.getCdregimecontratacao());
		}
		return super.equals(obj);
	}
}
