package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_orcamentovalorcampoextra", sequenceName = "sq_orcamentovalorcampoextra")
public class Orcamentovalorcampoextra {
	
	protected Integer cdorcamentovalorcampoextra;
	protected Vendaorcamento vendaorcamento;
	protected Campoextrapedidovendatipo campoextrapedidovendatipo;
	protected String valor;

	@Id
	@GeneratedValue(generator = "sq_orcamentovalorcampoextra", strategy = GenerationType.AUTO)
	public Integer getCdorcamentovalorcampoextra() {
		return cdorcamentovalorcampoextra;
	}

	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdvendaorcamento")
	public Vendaorcamento getVendaorcamento() {
		return vendaorcamento;
	}

	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcampoextrapedidovendatipo")
	public Campoextrapedidovendatipo getCampoextrapedidovendatipo() {
		return campoextrapedidovendatipo;
	}

	@MaxLength(500)
	public String getValor() {
		return valor;
	}

	public void setCdorcamentovalorcampoextra(
			Integer cdorcamentovalorcampoextra) {
		this.cdorcamentovalorcampoextra = cdorcamentovalorcampoextra;
	}

	public void setVendaorcamento(Vendaorcamento vendaorcamento) {
		this.vendaorcamento = vendaorcamento;
	}

	public void setCampoextrapedidovendatipo(Campoextrapedidovendatipo campoextrapedidovendatipo) {
		this.campoextrapedidovendatipo = campoextrapedidovendatipo;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}
