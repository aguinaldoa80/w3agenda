package br.com.linkcom.sined.geral.bean;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Formula;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Mes;

@Entity
@SequenceGenerator(name = "sq_pessoamaterial", sequenceName = "sq_pessoamaterial")
public class Pessoamaterial {

	protected Integer cdpessoamaterial;
	protected Pessoa pessoa;
	protected Material material;
	protected Double disponibilidademensal;
	protected Mes mesinicial;
	protected Mes mesfinal;
	protected Double disponibilidadeanual;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pessoamaterial")
	public Integer getCdpessoamaterial() {
		return cdpessoamaterial;
	}
	public void setCdpessoamaterial(Integer cdpessoamaterial) {
		this.cdpessoamaterial = cdpessoamaterial;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Pessoa getPessoa() {
		return pessoa;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	@Required
	@DisplayName("Disponibilidade mensal")
	public Double getDisponibilidademensal() {
		return disponibilidademensal;
	}
	public void setDisponibilidademensal(Double disponibilidademensal) {
		this.disponibilidademensal = disponibilidademensal;
	}
	
	@Required
	@DisplayName("De")
	public Mes getMesinicial() {
		return mesinicial;
	}
	public void setMesinicial(Mes mesinicial) {
		this.mesinicial = mesinicial;
	}
	
	@Required
	@DisplayName("At�")
	public Mes getMesfinal() {
		return mesfinal;
	}
	public void setMesfinal(Mes mesfinal) {
		this.mesfinal = mesfinal;
	}
	
	@Formula("(select sum(pm.disponibilidademensal * ((pm.mesfinal + 1) - pm.mesinicial)) from Pessoamaterial pm where pm.cdmaterial = cdmaterial and pm.cdpessoa = cdpessoa)")
	@Basic(fetch=FetchType.LAZY)
	@DisplayName("Disponibilidade anual")
	public Double getDisponibilidadeanual() {
		return disponibilidadeanual;
	}
	public void setDisponibilidadeanual(Double disponibilidadeanual) {
		this.disponibilidadeanual = disponibilidadeanual;
	}	
}
