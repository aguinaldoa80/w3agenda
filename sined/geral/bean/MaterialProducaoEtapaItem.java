package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_materialproducaoetapaitem", sequenceName = "sq_materialproducaoetapaitem")
@DisplayName("Etapas")
public class MaterialProducaoEtapaItem {

	private Integer cdMaterialProducaoEtapaItem;
	private Producaoetapaitem producaoetapaitem;
	private Material material;
	private String descricao;
	
	public MaterialProducaoEtapaItem () {
		
	}
	
	public MaterialProducaoEtapaItem (Producaoetapaitem producaoetapaitem, String descricao) {
		this.producaoetapaitem = producaoetapaitem;
		this.descricao = descricao;	
	}

	@Id
	@DisplayName("C�digo")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialproducaoetapaitem")
	public Integer getCdMaterialProducaoEtapaItem() {
		return cdMaterialProducaoEtapaItem;
	}

	public void setCdMaterialProducaoEtapaItem(Integer cdMaterialProducaoEtapaItem) {
		this.cdMaterialProducaoEtapaItem = cdMaterialProducaoEtapaItem;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoetapaitem")
	public Producaoetapaitem getProducaoetapaitem() {
		return producaoetapaitem;
	}

	public void setProducaoetapaitem(Producaoetapaitem producaoetapaitem) {
		this.producaoetapaitem = producaoetapaitem;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
