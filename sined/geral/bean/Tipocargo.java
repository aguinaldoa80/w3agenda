package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@DisplayName("Tipo de cargo")
public class Tipocargo {
	
	public static final Tipocargo MOD = new Tipocargo(1);
	public static final Tipocargo MOI = new Tipocargo(2);
	
	protected Integer cdtipocargo;
	protected String nome;
	
	public Tipocargo() {
		
	}
	
	public Tipocargo(Integer cdtipocargo) {
		this.cdtipocargo = cdtipocargo;
	}
	
	@Id	
	public Integer getCdtipocargo() {
		return cdtipocargo;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCdtipocargo(Integer cdtipocargo) {
		this.cdtipocargo = cdtipocargo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdtipocargo == null) ? 0 : cdtipocargo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Tipocargo other = (Tipocargo) obj;
		if (cdtipocargo == null) {
			if (other.cdtipocargo != null)
				return false;
		} else if (!cdtipocargo.equals(other.cdtipocargo))
			return false;
		return true;
	}
	
	
}
