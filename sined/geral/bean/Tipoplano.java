package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_tipoplano", sequenceName = "sq_tipoplano")
@DisplayName("Tipo de plano")
public class Tipoplano 
{
	protected Integer cdtipoplano;
	protected String nome;
	
	public Tipoplano()
	{
		
	}
	public Tipoplano(Integer cdtipoplano)
	{
		this.cdtipoplano= cdtipoplano;
		
	}
	
	@Id
	public Integer getCdtipoplano() {
		return cdtipoplano;
	}

	@Required
	public String getNome() {
		return nome;
	}

	public void setCdtipoplano(Integer cdtipoplano) {
		this.cdtipoplano = cdtipoplano;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	
}
