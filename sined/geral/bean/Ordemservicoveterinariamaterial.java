package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.SinedUtil;


@Entity
@SequenceGenerator(name="sq_ordemservicoveterinariamaterial", sequenceName="sq_ordemservicoveterinariamaterial")
public class Ordemservicoveterinariamaterial {

	private Integer cdordemservicoveterinariamaterial;
	private Ordemservicoveterinaria ordemservicoveterinaria;
	private Material material;
	private Frequencia periodicidade;
	private Integer frequencia;
	private Double qtdeusada;
	private Boolean faturado;
	private Loteestoque loteestoque;
	private Boolean faturar;
	private Date dtinicio;
	private Hora hrinicio;
	private Boolean prazotermino;
	private Integer quantidadeiteracao;
	private Date dtproximaiteracao;
	private Hora hrproximaiteracao;
	private Date dtfimprevisaoiteracao;
	private Hora hrfimprevisaoiteracao;
	private Timestamp dtfim;
	private Money valortotal;
	private List<Reserva> listaReserva;
	private List<Aplicacao> listaAplicacao;
	
	private Material materialmestre;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ordemservicoveterinariamaterial")
	public Integer getCdordemservicoveterinariamaterial() {
		return cdordemservicoveterinariamaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemservicoveterinaria")
	public Ordemservicoveterinaria getOrdemservicoveterinaria() {
		return ordemservicoveterinaria;
	}
	@DisplayName("Material ou Servi�o")
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Intervalo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdperiodicidade")
	public Frequencia getPeriodicidade() {
		return periodicidade;
	}
	public Integer getFrequencia() {
		return frequencia;
	}
	@DisplayName("Quantidade Usada")
	@MaxLength(10)
	public Double getQtdeusada() {
		return qtdeusada;
	}
	@DisplayName("Lote")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="loteestoque")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	public Boolean getFaturar() {
		return faturar;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Hora getHrinicio() {
		return hrinicio;
	}
	public Boolean getPrazotermino() {
		return prazotermino;
	}
	public Integer getQuantidadeiteracao() {
		return quantidadeiteracao;
	}
	public Date getDtproximaiteracao() {
		return dtproximaiteracao;
	}
	public Hora getHrproximaiteracao() {
		return hrproximaiteracao;
	}
	public Date getDtfimprevisaoiteracao() {
		return dtfimprevisaoiteracao;
	}
	public Hora getHrfimprevisaoiteracao() {
		return hrfimprevisaoiteracao;
	}
	public Timestamp getDtfim() {
		return dtfim;
	}
	public Boolean getFaturado() {
		return faturado;
	}
	public Money getValortotal() {
		return valortotal;
	}
	
	@OneToMany(mappedBy="ordemservicoveterinariamaterial")
	public List<Reserva> getListaReserva() {
		return listaReserva;
	}
	
	public void setListaReserva(List<Reserva> listaReserva) {
		this.listaReserva = listaReserva;
	}
	
	@OneToMany(mappedBy="ordemservicoveterinariamaterial")
	public List<Aplicacao> getListaAplicacao() {
		return listaAplicacao;
	}
	
	public void setCdordemservicoveterinariamaterial(Integer cdordemservicoveterinariamaterial) {
		this.cdordemservicoveterinariamaterial = cdordemservicoveterinariamaterial;
	}
	public void setOrdemservicoveterinaria(Ordemservicoveterinaria ordemservicoveterinaria) {
		this.ordemservicoveterinaria = ordemservicoveterinaria;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setPeriodicidade(Frequencia periodicidade) {
		this.periodicidade = periodicidade;
	}
	public void setFrequencia(Integer frequencia) {
		this.frequencia = frequencia;
	}
	public void setQtdeusada(Double qtdeusada) {
		this.qtdeusada = qtdeusada;
	}
	public void setFaturado(Boolean faturado) {
		this.faturado = faturado;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	public void setFaturar(Boolean faturar) {
		this.faturar = faturar;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setHrinicio(Hora hrinicio) {
		this.hrinicio = hrinicio;
	}
	public void setPrazotermino(Boolean prazotermino) {
		this.prazotermino = prazotermino;
	}
	public void setQuantidadeiteracao(Integer quantidadeiteracao) {
		this.quantidadeiteracao = quantidadeiteracao;
	}
	public void setDtproximaiteracao(Date dtproximaiteracao) {
		this.dtproximaiteracao = dtproximaiteracao;
	}
	public void setHrproximaiteracao(Hora hrproximaiteracao) {
		this.hrproximaiteracao = hrproximaiteracao;
	}
	public void setDtfimprevisaoiteracao(Date dtfimprevisaoiteracao) {
		this.dtfimprevisaoiteracao = dtfimprevisaoiteracao;
	}
	public void setHrfimprevisaoiteracao(Hora hrfimprevisaoiteracao) {
		this.hrfimprevisaoiteracao = hrfimprevisaoiteracao;
	}
	public void setDtfim(Timestamp dtfim) {
		this.dtfim = dtfim;
	}
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}
	public void setListaAplicacao(List<Aplicacao> listaAplicacao) {
		this.listaAplicacao = listaAplicacao;
	}
	

	
	@Transient
	public Reserva getReservaMaterial(Material material) {
		if(material != null && Hibernate.isInitialized(listaReserva) && SinedUtil.isListNotEmpty(listaReserva)){
			for(Reserva reserva : listaReserva){
				if(material.equals(reserva.getMaterial())){
					return reserva;
				}
			}
		}
		return null;
	}
	
	@Transient
	public Material getMaterialmestre() {
		return materialmestre;
	}
	public void setMaterialmestre(Material materialmestre) {
		this.materialmestre = materialmestre;
	}
}