package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.validation.annotation.MaxLength;


@Entity
@SequenceGenerator(name="sq_ordemservicoveterinariarotina", sequenceName="sq_ordemservicoveterinariarotina")
public class Ordemservicoveterinariarotina {

	private Integer cdordemservicoveterinariarotina;
	private Ordemservicoveterinaria ordemservicoveterinaria;
	private Integer acada;
	private Frequencia frequencia;
	private Date dtinicio;
	private Hora hrinicio;
	private Boolean prazotermino;
	private Integer quantidadeiteracao;
	private Date dtproximaiteracao;
	private Hora hrproximaiteracao;
	private Date dtfimprevisaoiteracao;
	private Hora hrfimprevisaoiteracao;
	private Timestamp dtfim;
	private String observacao;
	private List<Aplicacao> listaAplicacao;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ordemservicoveterinariarotina")
	public Integer getCdordemservicoveterinariarotina() {
		return cdordemservicoveterinariarotina;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemservicoveterinaria")
	public Ordemservicoveterinaria getOrdemservicoveterinaria() {
		return ordemservicoveterinaria;
	}
	@MaxLength(9)
	@DisplayName("A cada")
	public Integer getAcada() {
		return acada;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfrequencia")
	@DisplayName("Intervalo")
	public Frequencia getFrequencia() {
		return frequencia;
	}
	@DisplayName("Data in�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("�s")
	public Hora getHrinicio() {
		return hrinicio;
	}
	@DisplayName("Esta atividade tem prazo para t�rmino")
	public Boolean getPrazotermino() {
		return prazotermino;
	}
	@DisplayName("N�mero de itera��es")
	@MaxLength(9)
	public Integer getQuantidadeiteracao() {
		return quantidadeiteracao;
	}
	@DisplayName("Pr�xima itera��o")
	public Date getDtproximaiteracao() {
		return dtproximaiteracao;
	}
	@DisplayName("�s")
	public Hora getHrproximaiteracao() {
		return hrproximaiteracao;
	}	
	@DisplayName("Data fim previsto")
	public Date getDtfimprevisaoiteracao() {
		return dtfimprevisaoiteracao;
	}
	@DisplayName("�s")
	public Hora getHrfimprevisaoiteracao() {
		return hrfimprevisaoiteracao;
	}	
	public Timestamp getDtfim() {
		return dtfim;
	}
	@DisplayName("Informa��es")
	@MaxLength(1000)
	public String getObservacao() {
		return observacao;
	}
	@OneToMany(mappedBy="ordemservicoveterinariarotina")
	public List<Aplicacao> getListaAplicacao() {
		return listaAplicacao;
	}
	
	public void setCdordemservicoveterinariarotina(Integer cdordemservicoveterinariarotina) {
		this.cdordemservicoveterinariarotina = cdordemservicoveterinariarotina;
	}
	public void setOrdemservicoveterinaria(Ordemservicoveterinaria ordemservicoveterinaria) {
		this.ordemservicoveterinaria = ordemservicoveterinaria;
	}
	public void setAcada(Integer acada) {
		this.acada = acada;
	}
	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setHrinicio(Hora hrinicio) {
		this.hrinicio = hrinicio;
	}
	public void setPrazotermino(Boolean prazotermino) {
		this.prazotermino = prazotermino;
	}
	public void setQuantidadeiteracao(Integer quantidadeiteracao) {
		this.quantidadeiteracao = quantidadeiteracao;
	}
	public void setDtproximaiteracao(Date dtproximaiteracao) {
		this.dtproximaiteracao = dtproximaiteracao;
	}
	public void setHrproximaiteracao(Hora hrproximaiteracao) {
		this.hrproximaiteracao = hrproximaiteracao;
	}
	public void setDtfimprevisaoiteracao(Date dtfimprevisaoiteracao) {
		this.dtfimprevisaoiteracao = dtfimprevisaoiteracao;
	}
	public void setHrfimprevisaoiteracao(Hora hrfimprevisaoiteracao) {
		this.hrfimprevisaoiteracao = hrfimprevisaoiteracao;
	}
	public void setDtfim(Timestamp dtfim) {
		this.dtfim = dtfim;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setListaAplicacao(List<Aplicacao> listaAplicacao) {
		this.listaAplicacao = listaAplicacao;
	}
}