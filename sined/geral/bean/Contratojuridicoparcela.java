package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_contratojuridicoparcela", sequenceName = "sq_contratojuridicoparcela")
public class Contratojuridicoparcela {
	
	protected Integer cdcontratojuridicoparcela;
	protected Contratojuridico contratojuridico;
	protected Date dtvencimento;
	protected Money valor;
	protected Boolean cobrada;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratojuridicoparcela")
	public Integer getCdcontratojuridicoparcela() {
		return cdcontratojuridicoparcela;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratojuridico")
	public Contratojuridico getContratojuridico() {
		return contratojuridico;
	}

	@Required
	@DisplayName("Vencimento")
	public Date getDtvencimento() {
		return dtvencimento;
	}

	@Required
	public Money getValor() {
		return valor;
	}

	@DisplayName("Cobrada?")
	public Boolean getCobrada() {
		return cobrada;
	}
	
	@Transient
	public String getDtvencimentoValor(){
		if (getDtvencimento()!=null && getValor()!=null){
			return new SimpleDateFormat("dd/MM/yyyy").format(getDtvencimento()) + " - " + getValor().toString();
		}else {
			return "";
		}
	}
	
	public void setCdcontratojuridicoparcela(Integer cdcontratojuridicoparcela) {
		this.cdcontratojuridicoparcela = cdcontratojuridicoparcela;
	}

	public void setContratojuridico(Contratojuridico contratojuridico) {
		this.contratojuridico = contratojuridico;
	}

	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public void setCobrada(Boolean cobrada) {
		this.cobrada = cobrada;
	}
	
}