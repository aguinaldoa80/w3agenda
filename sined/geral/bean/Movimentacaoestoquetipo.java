package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@SequenceGenerator(name = "sq_movimentacaoestoquetipo", sequenceName = "sq_movimentacaoestoquetipo")
public class Movimentacaoestoquetipo {
	
	public static Movimentacaoestoquetipo ENTRADA = new Movimentacaoestoquetipo(1, "Entrada");
	public static Movimentacaoestoquetipo SAIDA = new Movimentacaoestoquetipo(2, "Sa�da");
	
	protected Integer cdmovimentacaoestoquetipo;
	protected String nome;
	
	public Movimentacaoestoquetipo(){
	}
	
	public Movimentacaoestoquetipo(Integer cdmovimentacaoestoquetipo, String nome){
		this.cdmovimentacaoestoquetipo = cdmovimentacaoestoquetipo;
		this.nome = nome;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_movimentacaoestoquetipo")
	public Integer getCdmovimentacaoestoquetipo() {
		return cdmovimentacaoestoquetipo;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCdmovimentacaoestoquetipo(Integer cdmovimentacaoestoquetipo) {
		this.cdmovimentacaoestoquetipo = cdmovimentacaoestoquetipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdmovimentacaoestoquetipo == null) ? 0
						: cdmovimentacaoestoquetipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movimentacaoestoquetipo other = (Movimentacaoestoquetipo) obj;
		if (cdmovimentacaoestoquetipo == null) {
			if (other.cdmovimentacaoestoquetipo != null)
				return false;
		} else if (!cdmovimentacaoestoquetipo
				.equals(other.cdmovimentacaoestoquetipo))
			return false;
		return true;
	}
	
	
	
	
}
