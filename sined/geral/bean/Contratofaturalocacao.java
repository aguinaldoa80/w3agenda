package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Contratofaturalocacaosituacao;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.CentrocustoValorVO;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;


@Entity
@SequenceGenerator(name = "sq_contratofaturalocacao", sequenceName = "sq_contratofaturalocacao")
@JoinEmpresa("contratofaturalocacao.empresa")
public class Contratofaturalocacao implements Log {

	protected Integer cdcontratofaturalocacao;
	protected Contratofaturalocacaosituacao situacao = Contratofaturalocacaosituacao.EMITIDA;
	protected Empresa empresa;
	protected Integer numero;
	protected Date dtemissao = SinedDateUtils.currentDate();
	protected Date dtvencimento;
	protected Date dtcontrato;
	protected Cliente cliente;
	protected Contrato contrato;
	protected Endereco endereco;
	protected Endereco enderecofatura;
	protected String descricao;
	protected Money valortotal;
	protected Cfop cfop;
	protected String dadosadicionais;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Boolean indenizacao;
	protected Boolean ignorarnumeracao;
	
	protected List<Contratofaturalocacaoorigem> listaContratofaturalocacaoorigem = new ListSet<Contratofaturalocacaoorigem>(Contratofaturalocacaoorigem.class);
	protected List<Contratofaturalocacaodocumento> listaContratofaturalocacaodocumento = new ListSet<Contratofaturalocacaodocumento>(Contratofaturalocacaodocumento.class);
	protected List<Contratofaturalocacaohistorico> listaContratofaturalocacaohistorico = new ListSet<Contratofaturalocacaohistorico>(Contratofaturalocacaohistorico.class);
	protected List<Contratofaturalocacaomaterial> listaContratofaturalocacaomaterial = new ListSet<Contratofaturalocacaomaterial>(Contratofaturalocacaomaterial.class);
	
	// TRANSIENTE
	protected Boolean usarSequencial = Boolean.TRUE;
	protected List<Contrato> listaContrato = new ArrayList<Contrato>();
	protected String whereInContrato;
	protected List<CentrocustoValorVO> listaCentrocustoValorVO = new ListSet<CentrocustoValorVO>(CentrocustoValorVO.class);
	protected String whereInCddocumentoBoleto;
	
	public Contratofaturalocacao() {
	}
	
	public Contratofaturalocacao(Integer cdcontratofaturalocacao) {
		this.cdcontratofaturalocacao = cdcontratofaturalocacao;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratofaturalocacao")
	public Integer getCdcontratofaturalocacao() {
		return cdcontratofaturalocacao;
	}
	
	@JoinColumn(name="cdcontrato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contrato getContrato() {
		return contrato;
	}
	
	@Required
	@DisplayName("N�mero")
	public Integer getNumero() {
		return numero;
	}
	
	@Required
	@JoinColumn(name="cdempresa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Empresa getEmpresa() {
		return empresa;
	}

	@Required
	@DisplayName("Data de emiss�o")
	public Date getDtemissao() {
		return dtemissao;
	}
	
	@DisplayName("Data de vencimento")
	public Date getDtvencimento() {
		return dtvencimento;
	}

	@Required
	@JoinColumn(name="cdcliente")
	@ManyToOne(fetch=FetchType.LAZY)
	public Cliente getCliente() {
		return cliente;
	}

	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}

	@Required
	@DisplayName("Total")
	public Money getValortotal() {
		return valortotal;
	}

	@DisplayName("Dados adicionais")
	public String getDadosadicionais() {
		return dadosadicionais;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="contratofaturalocacao")
	public List<Contratofaturalocacaohistorico> getListaContratofaturalocacaohistorico() {
		return listaContratofaturalocacaohistorico;
	}
	
	@DisplayName("Materiais")
	@OneToMany(mappedBy="contratofaturalocacao")
	public List<Contratofaturalocacaomaterial> getListaContratofaturalocacaomaterial() {
		return listaContratofaturalocacaomaterial;
	}
	
	@OneToMany(mappedBy="contratofaturalocacao")
	public List<Contratofaturalocacaodocumento> getListaContratofaturalocacaodocumento() {
		return listaContratofaturalocacaodocumento;
	}
	
	@OneToMany(mappedBy="contratofaturalocacao")
	public List<Contratofaturalocacaoorigem> getListaContratofaturalocacaoorigem() {
		return listaContratofaturalocacaoorigem;
	}
	
	@DisplayName("Situa��o")
	public Contratofaturalocacaosituacao getSituacao() {
		return situacao;
	}
	
	@DisplayName("Local Entrega")
	@JoinColumn(name="cdendereco")
	@ManyToOne(fetch=FetchType.LAZY)
	public Endereco getEndereco() {
		return endereco;
	}
	
	@DisplayName("Local Fatura")
	@JoinColumn(name="cdenderecofatura")
	@ManyToOne(fetch=FetchType.LAZY)
	public Endereco getEnderecofatura() {
		return enderecofatura;
	}
	
	@DisplayName("CFOP")
	@JoinColumn(name="cdcfop")
	@ManyToOne(fetch=FetchType.LAZY)
	public Cfop getCfop() {
		return cfop;
	}
	
	@DisplayName("Data do contrato")
	public Date getDtcontrato() {
		return dtcontrato;
	}
	
	public void setListaContratofaturalocacaoorigem(
			List<Contratofaturalocacaoorigem> listaContratofaturalocacaoorigem) {
		this.listaContratofaturalocacaoorigem = listaContratofaturalocacaoorigem;
	}
	
	public void setDtcontrato(Date dtcontrato) {
		this.dtcontrato = dtcontrato;
	}
	
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	public void setEnderecofatura(Endereco enderecofatura) {
		this.enderecofatura = enderecofatura;
	}
	
	public void setSituacao(Contratofaturalocacaosituacao situacao) {
		this.situacao = situacao;
	}
	
	public void setListaContratofaturalocacaohistorico(
			List<Contratofaturalocacaohistorico> listaContratofaturalocacaohistorico) {
		this.listaContratofaturalocacaohistorico = listaContratofaturalocacaohistorico;
	}
	
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}
	
	public void setListaContratofaturalocacaodocumento(
			List<Contratofaturalocacaodocumento> listaContratofaturalocacaodocumento) {
		this.listaContratofaturalocacaodocumento = listaContratofaturalocacaodocumento;
	}
	
	public void setListaContratofaturalocacaomaterial(
			List<Contratofaturalocacaomaterial> listaContratofaturalocacaomaterial) {
		this.listaContratofaturalocacaomaterial = listaContratofaturalocacaomaterial;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setDtemissao(Date dtemissao) {
		this.dtemissao = dtemissao;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}

	public void setDadosadicionais(String dadosadicionais) {
		this.dadosadicionais = dadosadicionais;
	}

	public void setCdcontratofaturalocacao(Integer cdcontratofaturalocacao) {
		this.cdcontratofaturalocacao = cdcontratofaturalocacao;
	}
	
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	
	@Transient
	public Boolean getUsarSequencial() {
		return usarSequencial;
	}
	
	public void setUsarSequencial(Boolean usarSequencial) {
		this.usarSequencial = usarSequencial;
	}
	
	@Transient
	public List<Contrato> getListaContrato() {
		return listaContrato;
	}
	
	public void setListaContrato(List<Contrato> listaContrato) {
		this.listaContrato = listaContrato;
	}
	
	@Transient
	public String getUltimoHistorico(){
		String ultimoHistorico = "";		
		if (listaContratofaturalocacaohistorico!=null && !listaContratofaturalocacaohistorico.isEmpty()){
			Collections.sort(listaContratofaturalocacaohistorico, new Comparator<Contratofaturalocacaohistorico>() {
				@Override
				public int compare(Contratofaturalocacaohistorico o1, Contratofaturalocacaohistorico o2) {
					if (o1.getDtaltera()==null && o2.getDtaltera()==null){
						return 0;
					}else if (o1.getDtaltera()!=null && o2.getDtaltera()==null){
						return -1;
					}else if (o1.getDtaltera()==null && o2.getDtaltera()!=null){
						return 1;
					}else {
						return o1.getDtaltera().compareTo(o2.getDtaltera());
					}
				}
			});
			ultimoHistorico = Util.strings.emptyIfNull(listaContratofaturalocacaohistorico.get(listaContratofaturalocacaohistorico.size()-1).getObservacao());
		}
		return ultimoHistorico;
	}
	
	public Boolean getIndenizacao() {
		return indenizacao;
	}
	
	public void setIndenizacao(Boolean indenizacao) {
		this.indenizacao = indenizacao;
	}
	
	@Transient
	public String getWhereInContrato() {
		return whereInContrato;
	}
	public void setWhereInContrato(String whereInContrato) {
		this.whereInContrato = whereInContrato;
	}
	
	@Transient
	@DisplayName("N�mero")
	public String getNumeroFormatado(){
		return SinedUtil.getNumeroNotaFormatado(getNumero());
	}

	@DisplayName("Ignorar numera��o")
	public Boolean getIgnorarnumeracao() {
		return ignorarnumeracao;
	}
	public void setIgnorarnumeracao(Boolean ignorarnumeracao) {
		this.ignorarnumeracao = ignorarnumeracao;
	}
	
	@Transient
	public List<CentrocustoValorVO> getListaCentrocustoValorVO() {
		return listaCentrocustoValorVO;
	}
	
	public void setListaCentrocustoValorVO(
			List<CentrocustoValorVO> listaCentrocustoValorVO) {
		this.listaCentrocustoValorVO = listaCentrocustoValorVO;
	}
	
	public void addCentrocustoValorVO(CentrocustoValorVO centrocustoValorVO){
		if(listaCentrocustoValorVO == null) listaCentrocustoValorVO = new ArrayList<CentrocustoValorVO>();
		if(listaCentrocustoValorVO.contains(centrocustoValorVO)){
			CentrocustoValorVO centrocustoValorVOAux = listaCentrocustoValorVO.get(listaCentrocustoValorVO.indexOf(centrocustoValorVO));
			centrocustoValorVOAux.setValor(centrocustoValorVOAux.getValor().add(centrocustoValorVO.getValor()));
		} else {
			listaCentrocustoValorVO.add(centrocustoValorVO);
		}
	}
	
	@Transient
	public String getWhereInCddocumentoBoleto(){
		String whereInCddocumentoBoleto = "";
		
		if (listaContratofaturalocacaodocumento!=null){
			for (Contratofaturalocacaodocumento contratofaturalocacaodocumento: listaContratofaturalocacaodocumento){
				Documento documento = contratofaturalocacaodocumento.getDocumento();
				if (documento!=null 
						&& documento.getConta()!=null
						&& documento.getDocumentotipo()!=null
						&& Boolean.TRUE.equals(documento.getDocumentotipo().getBoleto())){
					whereInCddocumentoBoleto += documento.getCddocumento() + ",";
				}
			}
			
			if (!whereInCddocumentoBoleto.equals("")){
				whereInCddocumentoBoleto = whereInCddocumentoBoleto.substring(0, whereInCddocumentoBoleto.length()-1);
			}
		}
		
		return whereInCddocumentoBoleto;
	}
}