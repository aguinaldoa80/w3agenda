package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
//@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Tipotaxa implements Log{

	public static final Tipotaxa JUROS = new Tipotaxa(1, "Juros (a.d.)");
	public static final Tipotaxa MULTA = new Tipotaxa(2, "Multa");
	public static final Tipotaxa DESCONTO = new Tipotaxa(3, "Desconto");
	public static final Tipotaxa DESAGIO = new Tipotaxa(4, "Des�gio (a.d.)");
	public static final Tipotaxa TAXABOLETO = new Tipotaxa(5, "Taxa de Boleto");
	public static final Tipotaxa TERMO = new Tipotaxa(6, "Termo");
	public static final Tipotaxa TAXAMOVIMENTO = new Tipotaxa(7, "Taxa de movimento");
	public static final Tipotaxa TAXABAIXA_DESCONTO = new Tipotaxa(8, "Taxa de baixa (desconto)");
	public static final Tipotaxa TAXABAIXA_ACRESCIMO = new Tipotaxa(9, "Taxa de baixa (acr�scimo)");
	public static final Tipotaxa JUROS_MES = new Tipotaxa(10, "Juros (a.m.)");
	
	protected Contagerencial contacredito;
	protected Contagerencial contadebito;
	protected Centrocusto centrocusto;
	
	protected Integer cdtipotaxa;
	protected String nome;
	
//	CAMPOS DE LOG
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Tipotaxa(){}
	
	public Tipotaxa(Integer cdtipotaxa){
		this.cdtipotaxa = cdtipotaxa;
	}
	
	public Tipotaxa(Integer cdtipotaxa, String nome){
		this.cdtipotaxa = cdtipotaxa;
		this.nome = nome;
	}
	
	@Id
	public Integer getCdtipotaxa() {
		return cdtipotaxa;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setCdtipotaxa(Integer cdtipotaxa) {
		this.cdtipotaxa = cdtipotaxa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/**
	 * Retorna uma inst�ncia est�tica de um tipo de taxa.
	 * 
	 * @see
	 * @param tipoTaxa
	 * @return
	 * @author Hugo Ferreira
	 */
	public static Tipotaxa valueOf(Tipotaxa tipoTaxa) {
		if (tipoTaxa != null) {
			if (tipoTaxa.equals(JUROS)) {
				return JUROS;
			} else if (tipoTaxa.equals(MULTA)) {
				return MULTA;
			} else if (tipoTaxa.equals(DESAGIO)) {
				return DESAGIO;
			} else if (tipoTaxa.equals(DESCONTO)) {
				return DESCONTO;
			} else if (tipoTaxa.equals(TAXABOLETO)) {
				return TAXABOLETO;
			} else if (tipoTaxa.equals(TERMO)) {
				return TERMO;
			} else if (tipoTaxa.equals(TAXAMOVIMENTO)) {
				return TAXAMOVIMENTO;
			} else if (tipoTaxa.equals(TAXABAIXA_DESCONTO)) {
				return TAXABAIXA_DESCONTO;
			} else if (tipoTaxa.equals(TAXABAIXA_ACRESCIMO)) {
				return TAXABAIXA_ACRESCIMO;
			} else if (tipoTaxa.equals(JUROS_MES)) {
				return JUROS_MES;
			}
		}
		
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdtipotaxa == null) ? 0 : cdtipotaxa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tipotaxa other = (Tipotaxa) obj;
		if (cdtipotaxa == null) {
			if (other.cdtipotaxa != null)
				return false;
		} else if (!cdtipotaxa.equals(other.cdtipotaxa))
			return false;
		return true;
	}

	@DisplayName("Conta Gerencial de Cr�dito")
	@JoinColumn(name="cdcontacredito")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contagerencial getContacredito() {
		return contacredito;
	}

	public void setContacredito(Contagerencial contacredito) {
		this.contacredito = contacredito;
	}

	@DisplayName("Conta Gerencial de D�bito")
	@JoinColumn(name="cdcontadebito")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contagerencial getContadebito() {
		return contadebito;
	}

	public void setContadebito(Contagerencial contadebito) {
		this.contadebito = contadebito;
	}

	@DisplayName("Centro de Custo")
	@JoinColumn(name="cdcentrocusto")
	@ManyToOne(fetch=FetchType.LAZY)
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}

	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
