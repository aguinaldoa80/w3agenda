package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@SequenceGenerator(name = "sq_grauinstrucao", sequenceName = "sq_grauinstrucao")
public class Grauinstrucao {

	protected Integer cdgrauinstrucao;
	protected String nome;

	public static final Integer ANALFABETO = 1;
	public static final Integer QUARTA_SERIE_INCOMPLETA = 2;
	public static final Integer QUARTA_SERIE_COMPLETA = 3;
	public static final Integer OITAVA_SERIE = 4;
	public static final Integer PRIMEIRO_GRAU_COMPLETO = 5;
	public static final Integer SEGUNDO_GRAU_INCOMPLETO = 6;
	public static final Integer SEGUNDO_GRAU_COMPLETO = 7;
	public static final Integer SUPERIOR_INCOMPLETO = 8;
	public static final Integer SUPERIOR_COMPLETO = 9;
	public static final Integer POS_GRADUACAO = 10;
	public static final Integer DOUTORADO = 11;
	public static final Integer TECNICO_INCOMPLETO = 12;
	public static final Integer TECNICO_COMPLETO = 13;
	public static final Integer MESTRADO = 14;
	
	public Grauinstrucao() {
	}
	
	public Grauinstrucao(Integer cdgrauinstrucao) {
		this.cdgrauinstrucao = cdgrauinstrucao;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_grauinstrucao")
	public Integer getCdgrauinstrucao() {
		return cdgrauinstrucao;
	}
	public void setCdgrauinstrucao(Integer id) {
		this.cdgrauinstrucao = id;
	}

	
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	
	public void setNome(String nome) {
		this.nome = nome;
	}

}
