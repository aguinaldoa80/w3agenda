package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_configuracaotefterminal", sequenceName="sq_configuracaotefterminal")
public class Configuracaotefterminal {

	protected Integer cdconfiguracaotefterminal;
	protected Configuracaotef configuracaotef;
	protected String nome;
	protected String identificador;
	protected Integer codigo;
	protected Boolean impressora;
	
	@Id
	@GeneratedValue(generator="sq_configuracaotefterminal", strategy=GenerationType.AUTO)
	public Integer getCdconfiguracaotefterminal() {
		return cdconfiguracaotefterminal;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconfiguracaotef")
	public Configuracaotef getConfiguracaotef() {
		return configuracaotef;
	}

	@Required
	@MaxLength(200)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	@Required
	@MaxLength(200)
	public String getIdentificador() {
		return identificador;
	}
	
	@Required
	@DisplayName("C�digo")
	public Integer getCodigo() {
		return codigo;
	}
	
	@DisplayName("Impressora integrada?")
	public Boolean getImpressora() {
		return impressora;
	}
	
	public void setImpressora(Boolean impressora) {
		this.impressora = impressora;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public void setCdconfiguracaotefterminal(Integer cdconfiguracaotefterminal) {
		this.cdconfiguracaotefterminal = cdconfiguracaotefterminal;
	}

	public void setConfiguracaotef(Configuracaotef configuracaotef) {
		this.configuracaotef = configuracaotef;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
}
