package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(sequenceName="sq_notafiscalprodutoitementregamaterial", name="sq_notafiscalprodutoitementregamaterial")
public class Notafiscalprodutoitementregamaterial {
	protected Integer cdnotafiscalprodutoitementregamaterial;
	protected Notafiscalprodutoitem notafiscalprodutoitem;
	protected Entregamaterial entregamaterial;
	protected Double saldo;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_notafiscalprodutoitementregamaterial")
	public Integer getCdnotafiscalprodutoitementregamaterial() {
		return cdnotafiscalprodutoitementregamaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnotafiscalprodutoitem")
	public Notafiscalprodutoitem getNotafiscalprodutoitem() {
		return notafiscalprodutoitem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdentregamaterial")
	public Entregamaterial getEntregamaterial() {
		return entregamaterial;
	}
	public Double getSaldo() {
		return saldo;
	}
	
	
	public void setCdnotafiscalprodutoitementregamaterial(
			Integer cdnotafiscalprodutoitementregamaterial) {
		this.cdnotafiscalprodutoitementregamaterial = cdnotafiscalprodutoitementregamaterial;
	}
	public void setNotafiscalprodutoitem(Notafiscalprodutoitem notafiscalprodutoitem) {
		this.notafiscalprodutoitem = notafiscalprodutoitem;
	}
	public void setEntregamaterial(Entregamaterial entregamaterial) {
		this.entregamaterial = entregamaterial;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
}
