package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_rateioapuracaoempresa",sequenceName="sq_rateioapuracaoempresa")
public class Rateioapuracaoempresa {

	private Integer cdrateioapuracaoempresa;
	private Rateioapuracao rateioapuracao;
	private Empresa empresa;
	private Double percentual;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_rateioapuracaoempresa")
	public Integer getCdrateioapuracaoempresa() {
		return cdrateioapuracaoempresa;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	@Required
	@DisplayName("%")
	public Double getPercentual() {
		return percentual;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrateioapuracao")
	public Rateioapuracao getRateioapuracao() {
		return rateioapuracao;
	}

	public void setCdrateioapuracaoempresa(Integer cdrateioapuracaoempresa) {
		this.cdrateioapuracaoempresa = cdrateioapuracaoempresa;
	}

	public void setRateioapuracao(Rateioapuracao rateioapuracao) {
		this.rateioapuracao = rateioapuracao;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}
	
}
