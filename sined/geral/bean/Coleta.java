
package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVinculoExpedicao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@Entity
@SequenceGenerator(name = "sq_coleta", sequenceName = "sq_coleta")
@JoinEmpresa("coleta.empresa")
public class Coleta implements Log {
	
	protected Integer cdcoleta;
	protected Empresa empresa;
	protected TipoColeta tipo;
	protected Cliente cliente;
	protected Fornecedor fornecedor;
	protected Colaborador colaborador;
	protected String observacao;
	protected Pedidovenda pedidovenda;
	protected Date dtconfirmacaodevolucao;
	protected String motivodevolucao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<ColetaMaterial> listaColetaMaterial;
	protected List<EntregadocumentoColeta> listaEntregadocumentoColeta;
	protected List<NotafiscalprodutoColeta> listaNotafiscalprodutoColeta;
	protected List<Coletahistorico> listaColetahistorico;
	
	protected SituacaoVinculoExpedicao situacaoColeta;
	
	//Transientes
	public static enum TipoColeta {CLIENTE, FORNECEDOR};
	
	private String whereIn;
	private Boolean gerarNota = Boolean.TRUE;
	private Boolean fromProducaoordem = Boolean.FALSE;
	private Localarmazenagem localarmazenagem;
	private String motivoestorno;
	private String whereInProducaoordem;
	private Boolean radFormacompra = Boolean.TRUE;
	private Date dtvencimento;
	private Money valor;
	private Documentotipo documentotipo;
	private Contagerencial contagerencial;
	private Centrocusto centrocusto;
	private Projeto projeto;
	private Boolean contapaga;
	private Formapagamento formapagamento;
	private Conta vinculo;
	private Documento documento;
	private Date dtpagamento;
	private String identificadorpedidovenda;
	private String identificacaoexternapedidovenda;
	private Boolean notaSaidaCompleto;
	private Boolean isColetaIncompleta; 
	private String nomeVendendorPrincipal;
	
	public Coleta() {}
	
	public Coleta(Integer cdcoleta) {
		this.cdcoleta = cdcoleta;
	}
	
	public Coleta(Integer cdcoleta, Integer cdcolaborador, String nome_colaborador, String clienteFornecedorString, TipoColeta tipo){
		this.cdcoleta = cdcoleta;
		this.colaborador = new Colaborador(cdcolaborador, nome_colaborador);
		this.tipo = tipo;
	}
	
	public Coleta(Integer cdcoleta,SituacaoVinculoExpedicao situacaoColeta, Integer cdcolaborador, String nome_colaborador, String clienteFornecedorString, TipoColeta tipo, 
			Integer cdempresa, String nome_empresa, String identificacaoexternapedidovenda){
		this.cdcoleta = cdcoleta;
		this.colaborador = new Colaborador(cdcolaborador, nome_colaborador);
		this.tipo = tipo;
		if(cdempresa != null){
			empresa = new Empresa(cdempresa);
			empresa.setNome(nome_empresa);
		}
		this.identificacaoexternapedidovenda = identificacaoexternapedidovenda;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_coleta")
	@DisplayName("C�digo")
	public Integer getCdcoleta() {
		return cdcoleta;
	}
	public void setCdcoleta(Integer cdcoleta) {
		this.cdcoleta = cdcoleta;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Required
	public TipoColeta getTipo() {
		return tipo;
	}
	public void setTipo(TipoColeta tipo) {
		this.tipo = tipo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	@DisplayName("Coletor")
	public Colaborador getColaborador() {
		return colaborador;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@DisplayName("Confirma��o da devolu��o")
	public Date getDtconfirmacaodevolucao() {
		return dtconfirmacaodevolucao;
	}
	public void setDtconfirmacaodevolucao(Date dtconfirmacaodevolucao) {
		this.dtconfirmacaodevolucao = dtconfirmacaodevolucao;
	}
	
	@Transient
	@DisplayName("Pedido de venda")
	public String getIdentificadorpedidovenda() {
		if(pedidovenda != null){
			identificadorpedidovenda = pedidovenda.getIdentificador() != null? pedidovenda.getIdentificador(): pedidovenda.getCdpedidovenda().toString();
		}
		return identificadorpedidovenda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	@DisplayName("Pedido de Venda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	
	@MaxLength(1000)
	@DisplayName("Motivo da devolu��o")
	public String getMotivodevolucao() {
		return motivodevolucao;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public SituacaoVinculoExpedicao getSituacaoColeta() {
		return situacaoColeta;
	}
	public void setSituacaoColeta(SituacaoVinculoExpedicao situacaoColeta) {
		this.situacaoColeta = situacaoColeta;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setMotivodevolucao(String motivodevolucao) {
		this.motivodevolucao = motivodevolucao;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@OneToMany(mappedBy="coleta")
	@DisplayName("Materiais")
	public List<ColetaMaterial> getListaColetaMaterial() {
		return listaColetaMaterial;
	}
	public void setListaColetaMaterial(List<ColetaMaterial> listaColetaMaterial) {
		this.listaColetaMaterial = listaColetaMaterial;
	}
	
	@OneToMany(mappedBy="coleta")
	public List<EntregadocumentoColeta> getListaEntregadocumentoColeta() {
		return listaEntregadocumentoColeta;
	}
	public void setListaEntregadocumentoColeta(List<EntregadocumentoColeta> listaEntregadocumentoColeta) {
		this.listaEntregadocumentoColeta = listaEntregadocumentoColeta;
	}
	
	@OneToMany(mappedBy="coleta")
	public List<NotafiscalprodutoColeta> getListaNotafiscalprodutoColeta() {
		return listaNotafiscalprodutoColeta;
	}
	public void setListaNotafiscalprodutoColeta(
			List<NotafiscalprodutoColeta> listaNotafiscalprodutoColeta) {
		this.listaNotafiscalprodutoColeta = listaNotafiscalprodutoColeta;
	}
	
	@OneToMany(mappedBy="coleta")
	public List<Coletahistorico> getListaColetahistorico() {
		return listaColetahistorico;
	}
	
	public void setListaColetahistorico(
			List<Coletahistorico> listaColetahistorico) {
		this.listaColetahistorico = listaColetahistorico;
	}
	
	@Transient
	@DisplayName("Cliente / Fornecedor")
	public String getClienteFornecedorListagem() {
		if(this.cliente != null){
			return this.cliente.getNome();
		} else if(this.fornecedor != null){
			return this.fornecedor.getNome();
		}
		return null;
	}

	@Transient
	@DisplayName("Material(is)")
	public String getMateriaisListagem() {
		StringBuilder mat = new StringBuilder();
		for(ColetaMaterial cm : listaColetaMaterial){
			if(cm.getMaterial() != null && cm.getMaterial().getNome() != null){
				mat.append(cm.getMaterial().getNome());
				if(cm.getObservacao() != null && !cm.getObservacao().trim().equals("")){
					mat.append(" (").append(cm.getObservacao()).append(")");
				}
				
				if(SinedUtil.isListNotEmpty(cm.getListaColetaMaterialPedidovendamaterial())) {
					for(ColetaMaterialPedidovendamaterial itemServico : cm.getListaColetaMaterialPedidovendamaterial()) {
						if(itemServico.getPedidovendamaterial() != null && itemServico.getPedidovendamaterial().getCdpedidovendamaterial() != null){
							mat.append(" (").append(itemServico.getPedidovendamaterial().getCdpedidovendamaterial()).append(")");
						}				
					}
				}
//				if(cm.getPedidovendamaterial() != null && cm.getPedidovendamaterial().getCdpedidovendamaterial() != null){
//					mat.append(" (").append(cm.getPedidovendamaterial().getCdpedidovendamaterial()).append(")");
//				}
			}
			mat.append("<BR/>");
		}
		return mat.toString();
	}
	
	@Transient
	@DisplayName("Qtde.")
	public String getQtdeListagem() {
		StringBuilder mat = new StringBuilder();
		for(ColetaMaterial cm : listaColetaMaterial){
			Double quantidade = cm.getQuantidade();
			if(quantidade == null) quantidade = 0d;
			mat.append(SinedUtil.descriptionDecimal(quantidade));
			mat.append("<BR/>");
		}
		return mat.toString();
	}
	
	@Transient
	@DisplayName("Qtde. Devolvida")
	public String getQtdeDevolvidaListagem() {
		StringBuilder mat = new StringBuilder();
		for(ColetaMaterial cm : listaColetaMaterial){
			Double quantidadedevolvida = cm.getQuantidadedevolvida();
			if(quantidadedevolvida == null) quantidadedevolvida = 0d;
			mat.append(SinedUtil.descriptionDecimal(quantidadedevolvida));
			if(quantidadedevolvida != 0){
//				String motivodevolucao = cm.getMotivodevolucao() != null ? cm.getMotivodevolucao().getDescricao() : "Motivo n�o especificado";
				String motivodevolucao = "Motivo n�o especificado";
				if(Hibernate.isInitialized(cm.getListaColetamaterialmotivodevolucao()) && SinedUtil.isListNotEmpty(cm.getListaColetamaterialmotivodevolucao())){
					motivodevolucao = CollectionsUtil.listAndConcatenate(cm.getListaColetamaterialmotivodevolucao(), "motivodevolucao.descricao", ", ");
					motivodevolucao = motivodevolucao.replace("'", "\'");
				}
				mat.append("\t<a onmouseover=\"Tip('"+motivodevolucao+"')\">?</a><BR/>");
			}else
				mat.append("<BR/>");
		}
		return mat.toString();
	}
	
	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

	@Transient
	public Boolean getGerarNota() {
		return gerarNota;
	}
	
	@Transient
	public Boolean getFromProducaoordem() {
		return fromProducaoordem;
	}
	
	public void setFromProducaoordem(Boolean fromProducaoordem) {
		this.fromProducaoordem = fromProducaoordem;
	}
	
	public void setGerarNota(Boolean gerarNota) {
		this.gerarNota = gerarNota;
	}
	
	@Transient
	public boolean getDevolucaoConfirmada(){
		boolean confirmada = true;
		if(this.getDtconfirmacaodevolucao() == null){
			for (ColetaMaterial coletaMaterial : getListaColetaMaterial()) {
				if(coletaMaterial.getQuantidadedevolvida() != null && coletaMaterial.getQuantidadedevolvida() > 0){
					confirmada = false;
				}
			}
		}
		return confirmada;
		
	}
	
	@Transient
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	
	@Transient
	public String getMotivoestorno() {
		return motivoestorno;
	}
	
	public void setMotivoestorno(String motivoestorno) {
		this.motivoestorno = motivoestorno;
	}

	@Transient
	public String getWhereInProducaoordem() {
		return whereInProducaoordem;
	}

	public void setWhereInProducaoordem(String whereInProducaoordem) {
		this.whereInProducaoordem = whereInProducaoordem;
	}

	@Transient
	public Boolean getExistenotaassociada() {
		return (Hibernate.isInitialized(listaNotafiscalprodutoColeta) && SinedUtil.isListNotEmpty(listaNotafiscalprodutoColeta)) ||
				(Hibernate.isInitialized(listaEntregadocumentoColeta) && SinedUtil.isListNotEmpty(listaEntregadocumentoColeta));
	}
	
	@Transient
	public Boolean getExisteNotaEntadaAssociada() {
		if(Hibernate.isInitialized(listaNotafiscalprodutoColeta) && SinedUtil.isListNotEmpty(listaNotafiscalprodutoColeta)){
			for(NotafiscalprodutoColeta nfpc : listaNotafiscalprodutoColeta){
				if(nfpc.getNotafiscalproduto() != null && Tipooperacaonota.ENTRADA.equals(nfpc.getNotafiscalproduto().getTipooperacaonota()) &&
						!NotaStatus.CANCELADA.equals(nfpc.getNotafiscalproduto().getNotaStatus())){
					return true;
				}
			}
		}
		if(Hibernate.isInitialized(listaEntregadocumentoColeta) && SinedUtil.isListNotEmpty(listaEntregadocumentoColeta)){
			for(EntregadocumentoColeta edc : listaEntregadocumentoColeta){
				if(edc.getEntregadocumento() != null && !Entregadocumentosituacao.CANCELADA.equals(edc.getEntregadocumento().getEntregadocumentosituacao())){
					return true;
				}
			}
		}
		return false;
	}
	
	@Transient
	public Boolean getExisteNotaSaidaAssociada() {
		if(Hibernate.isInitialized(listaNotafiscalprodutoColeta) && SinedUtil.isListNotEmpty(listaNotafiscalprodutoColeta)){
			for(NotafiscalprodutoColeta nfpc : listaNotafiscalprodutoColeta){
				if(nfpc.getNotafiscalproduto() != null && Tipooperacaonota.SAIDA.equals(nfpc.getNotafiscalproduto().getTipooperacaonota()) &&
						!NotaStatus.CANCELADA.equals(nfpc.getNotafiscalproduto().getNotaStatus())){
					return true;
				}
			}
		}
		if(Hibernate.isInitialized(listaEntregadocumentoColeta) && SinedUtil.isListNotEmpty(listaEntregadocumentoColeta)){
			for(EntregadocumentoColeta edc : listaEntregadocumentoColeta){
				if(edc.getEntregadocumento() != null && Boolean.TRUE.equals(edc.getEntregadocumento().getExisteNotaDevolucao())){
					return true;
				}
			}
		}
		return false;
	}
	
	@Transient
	@DisplayName("Data de vencimento")
	public Date getDtvencimento() {
		return dtvencimento;
	}
	@Transient
	public Money getValor() {
		return valor;
	}

	@Transient
	public Boolean getRadFormacompra() {
		return radFormacompra;
	}
	@Transient
	@DisplayName("Tipo de documento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	@Transient
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@Transient
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@Transient
	public Projeto getProjeto() {
		return projeto;
	}
	@Transient
	@DisplayName("Esta conta j� foi paga? ")
	public Boolean getContapaga() {
		return contapaga;
	}
	@Transient
	@DisplayName("Forma de pagamento")
	public Formapagamento getFormapagamento() {
		return formapagamento;
	}
	@Transient
	@DisplayName("V�nculo")
	public Conta getVinculo() {
		return vinculo;
	}
	@Transient
	public Documento getDocumento() {
		return documento;
	}
	@Transient
	@DisplayName("Data de pagamento")
	public Date getDtpagamento() {
		return dtpagamento;
	}
	@Transient
	@DisplayName("Vendedor Principal")
	public String getNomeVendendorPrincipal() {
		return nomeVendendorPrincipal;
	}
	public void setNomeVendendorPrincipal(String nomeVendendorPrincipal) {
		this.nomeVendendorPrincipal = nomeVendendorPrincipal;
	}
	public void setRadFormacompra(Boolean radFormacompra) {
		this.radFormacompra = radFormacompra;
	}
	
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setContapaga(Boolean contapaga) {
		this.contapaga = contapaga;
	}
	public void setFormapagamento(Formapagamento formapagamento) {
		this.formapagamento = formapagamento;
	}
	public void setVinculo(Conta vinculo) {
		this.vinculo = vinculo;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setDtpagamento(Date dtpagamento) {
		this.dtpagamento = dtpagamento;
	}

	@Transient
	public String getMotivodevolucaoItens() {
		StringBuilder s = new StringBuilder();
		if(Hibernate.isInitialized(listaColetaMaterial) && SinedUtil.isListNotEmpty(listaColetaMaterial)){
			for(ColetaMaterial coletaMaterial : listaColetaMaterial){
//				if(coletaMaterial.getMotivodevolucao() != null && coletaMaterial.getMotivodevolucao().getDescricao() != null &&
//						!s.toString().contains(coletaMaterial.getMotivodevolucao().getDescricao())){
//					if(s.length() > 0) s.append(" | ");
//					s.append(coletaMaterial.getMotivodevolucao().getDescricao());
//				}
				if(Hibernate.isInitialized(coletaMaterial.getListaColetamaterialmotivodevolucao()) && SinedUtil.isListNotEmpty(coletaMaterial.getListaColetamaterialmotivodevolucao())){
					for (Coletamaterialmotivodevolucao coletamaterialmotivodevolucao: coletaMaterial.getListaColetamaterialmotivodevolucao()){
						if (!s.toString().contains(coletamaterialmotivodevolucao.getMotivodevolucao().getDescricao())){
							if(s.length() > 0) s.append(" | ");
							s.append(coletamaterialmotivodevolucao.getMotivodevolucao().getDescricao());
						}
					}
				}
			}
		}
		return s.toString();
	}

	@Transient
	public boolean getExisteNotaDevolucao() {
		if(Hibernate.isInitialized(listaNotafiscalprodutoColeta) && SinedUtil.isListNotEmpty(listaNotafiscalprodutoColeta)){
			for(NotafiscalprodutoColeta notaColeta : listaNotafiscalprodutoColeta){
				if(notaColeta.getDevolucao() != null && notaColeta.getDevolucao()){
					return true;
				}
			}
		}
		return false;
	}
	
	@Transient
	@DisplayName("Identificador Externo")
	public String getIdentificacaoexternapedidovenda() {
		return identificacaoexternapedidovenda;
	}
	
	public void setIdentificacaoexternapedidovenda(String identificacaoexternapedidovenda) {
		this.identificacaoexternapedidovenda = identificacaoexternapedidovenda;
	}

	@Transient
	public Boolean getNotaSaidaCompleto() {
		return notaSaidaCompleto;
	}

	public void setNotaSaidaCompleto(Boolean notaSaidaCompleto) {
		this.notaSaidaCompleto = notaSaidaCompleto;
	}
	@Transient
	public Boolean getIsColetaIncompleta() {
		return isColetaIncompleta;
	}
	public void setIsColetaIncompleta(Boolean isColetaIncompleta) {
		this.isColetaIncompleta = isColetaIncompleta;
	}

	@Transient
	public String getWhereInPedidoVendaMaterial() {
		if(Hibernate.isInitialized(getListaColetaMaterial()) && SinedUtil.isListNotEmpty(getListaColetaMaterial())){
			StringBuilder whereInPVM = new StringBuilder();
			for(ColetaMaterial coletaMaterial : getListaColetaMaterial()){
				if(SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())) {
					for(ColetaMaterialPedidovendamaterial itemServico : coletaMaterial.getListaColetaMaterialPedidovendamaterial()) {
						if(itemServico.getPedidovendamaterial() != null && itemServico.getPedidovendamaterial().getCdpedidovendamaterial() != null){
							whereInPVM.append(itemServico.getPedidovendamaterial().getCdpedidovendamaterial().toString()).append(",");
						}
					}
				}
//				if(coletaMaterial.getPedidovendamaterial() != null && coletaMaterial.getPedidovendamaterial().getCdpedidovendamaterial() != null){
//					whereInPVM.append(coletaMaterial.getPedidovendamaterial().getCdpedidovendamaterial().toString()).append(",");
//				}
			}
			if(whereInPVM.length() > 0){
				return whereInPVM.substring(0, whereInPVM.length()-1);
			}
		}
		return "";
	}
}
