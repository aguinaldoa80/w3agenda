package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
public class Planejamentosituacao{

	public static final Planejamentosituacao EM_ABERTO = new Planejamentosituacao(1, "Em aberto");
	public static final Planejamentosituacao AUTORIZADA = new Planejamentosituacao(2, "Autorizada");
	public static final Planejamentosituacao CANCELADA = new Planejamentosituacao(3, "Cancelada");
	
	protected Integer cdplanejamentosituacao;
	protected String nome;
	
	public Planejamentosituacao() {}
	
	public Planejamentosituacao(Integer cdplanejamentosituacao) {
		this.cdplanejamentosituacao = cdplanejamentosituacao;
	}
	
	public Planejamentosituacao(Integer cdplanejamentosituacao, String nome) {
		this.cdplanejamentosituacao = cdplanejamentosituacao;
		this.nome = nome;
	}

	@Id
	public Integer getCdplanejamentosituacao() {
		return cdplanejamentosituacao;
	}
	
	@DisplayName("Descri��o")
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCdplanejamentosituacao(Integer cdplanejamentosituacao) {
		this.cdplanejamentosituacao = cdplanejamentosituacao;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/**
	 * M�todo para pegar todas as situa��es sem precisar ir ao BD.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public static List<Planejamentosituacao> findAll(){
		List<Planejamentosituacao> lista = new ArrayList<Planejamentosituacao>();
			lista.add(EM_ABERTO);
			lista.add(AUTORIZADA);
			lista.add(CANCELADA);
		return lista;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdplanejamentosituacao == null) ? 0
						: cdplanejamentosituacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Planejamentosituacao other = (Planejamentosituacao) obj;
		if (cdplanejamentosituacao == null) {
			if (other.cdplanejamentosituacao != null)
				return false;
		} else if (!cdplanejamentosituacao.equals(other.cdplanejamentosituacao))
			return false;
		return true;
	}
	
	
	
	
	
	
}
