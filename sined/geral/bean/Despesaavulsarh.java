package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoDespesaAvulsaRH;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_despesaavulsarh", sequenceName = "sq_despesaavulsarh")
public class Despesaavulsarh implements Log {

	private Integer cddespesaavulsarh;
	private Date dtcancelado;
	private Date dtinsercao;
	private Date dtdeposito;
	private Fornecedor fornecedor;
	private SituacaoDespesaAvulsaRH situacao;
	private Money valor;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	private Despesaavulsarhmotivo despesaavulsarhmotivo;
	private List<Despesaavulsarhrateio> listaDespesaavulsarhrateio = new ListSet<Despesaavulsarhrateio>(Despesaavulsarhrateio.class); 
	private List<Despesaavulsarhhistorico> listaDespesaavulsarhhistorico = new ListSet<Despesaavulsarhhistorico>(Despesaavulsarhhistorico.class); 

	//Campos transientes
	private Money valorrestante;
	private Money valortotal;
	private String itensSelecionados;
	private Documentotipo documentotipo;
	private Empresa empresa;
	
	@Id
	@GeneratedValue(generator = "sq_despesaavulsarh", strategy = GenerationType.AUTO)
	public Integer getCddespesaavulsarh() {
		return cddespesaavulsarh;
	}

	public void setCddespesaavulsarh(Integer cddespesaavulsarh) {
		this.cddespesaavulsarh = cddespesaavulsarh;
	}

	@DisplayName("Data de cancelamento")
	public Date getDtcancelado() {
		return dtcancelado;
	}

	public void setDtcancelado(Date dtcancelado) {
		this.dtcancelado = dtcancelado;
	}

	@Required
	@DisplayName("Data de inser��o")
	public Date getDtinsercao() {
		return dtinsercao;
	}

	public void setDtinsercao(Date dtinsercao) {
		this.dtinsercao = dtinsercao;
	}

	@DisplayName("Data do dep�sito")
	public Date getDtdeposito() {
		return dtdeposito;
	}

	public void setDtdeposito(Date dtdeposito) {
		this.dtdeposito = dtdeposito;
	}

	@Required
	@DisplayName("Fornecedor")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	@Required
	@DisplayName("Situa��o")
	public SituacaoDespesaAvulsaRH getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoDespesaAvulsaRH situacao) {
		this.situacao = situacao;
	}

	@Required
	public Money getValor() {
		return valor;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	@DisplayName("�ltima altera��o")
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Required
	@DisplayName("Motivo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddespesaavulsarhmotivo")
	public Despesaavulsarhmotivo getDespesaavulsarhmotivo() {
		return despesaavulsarhmotivo;
	}

	public void setDespesaavulsarhmotivo(
			Despesaavulsarhmotivo despesaavulsarhmotivo) {
		this.despesaavulsarhmotivo = despesaavulsarhmotivo;
	}

	@DisplayName("Rateio")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="despesaavulsarh")
	public List<Despesaavulsarhrateio> getListaDespesaavulsarhrateio() {
		return listaDespesaavulsarhrateio;
	}

	public void setListaDespesaavulsarhrateio(
			List<Despesaavulsarhrateio> listaDespesaavulsarhrateio) {
		this.listaDespesaavulsarhrateio = listaDespesaavulsarhrateio;
	}

	@DisplayName("Hist�rico")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="despesaavulsarh")
	public List<Despesaavulsarhhistorico> getListaDespesaavulsarhhistorico() {
		return listaDespesaavulsarhhistorico;
	}

	public void setListaDespesaavulsarhhistorico(
			List<Despesaavulsarhhistorico> listaDespesaavulsarhhistorico) {
		this.listaDespesaavulsarhhistorico = listaDespesaavulsarhhistorico;
	}

	@Transient
	@DisplayName("Valor restante")
	public Money getValorrestante() {
		return valorrestante;
	}

	public void setValorrestante(Money valortotalaux) {
		this.valorrestante = valortotalaux;
	}

	@Transient
	@DisplayName("Valor total")
	public Money getValortotal() {
		return valortotal;
	}

	public void setValortotal(Money valoraux) {
		this.valortotal = valoraux;
	}
	
	@Transient
	public String getItensSelecionados() {
		return itensSelecionados;
	}
	
	public void setItensSelecionados(String itensSelecionados) {
		this.itensSelecionados = itensSelecionados;
	}

	@Transient
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}

	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}

	@Transient
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

}
