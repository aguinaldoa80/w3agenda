package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;

import br.com.linkcom.sined.geral.bean.enumeration.MaterialGrupoEnum;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;


@Entity
@DisplayName("Hist�rico")
@SequenceGenerator(name = "sq_materialgrupohistorico", sequenceName = "sq_materialgrupohistorico")
public class MaterialGrupoHistorico implements Log {

	protected Integer cdMaterialGrupoHistorico;
	protected Materialgrupo cdMaterialGrupo;
	protected MaterialGrupoEnum acao;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	
	@Id
	@GeneratedValue(generator="sq_materialgrupohistorico",strategy=GenerationType.AUTO)
	public Integer getCdMaterialGrupoHistorico() {
		return cdMaterialGrupoHistorico;
	}
	public void setCdMaterialGrupoHistorico(Integer cdMaterialGrupoHistorico) {
		this.cdMaterialGrupoHistorico = cdMaterialGrupoHistorico;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialgrupo")
	public Materialgrupo getCdMaterialGrupo() {
		return cdMaterialGrupo;
	}
	public void setCdMaterialGrupo(Materialgrupo cdMaterialGrupo) {
		this.cdMaterialGrupo = cdMaterialGrupo;
	}
	
	@DisplayName("Respons�vel")
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@DisplayName("A��o")
	public MaterialGrupoEnum getAcao() {
		return acao;
	}
	public void setAcao(MaterialGrupoEnum acao) {
		this.acao = acao;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	@DisplayName("Data de Altera��o")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
	
	
}
