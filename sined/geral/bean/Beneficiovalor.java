package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;

@DisplayName("Valores")
@Entity
@SequenceGenerator(name="sq_beneficiovalor",sequenceName="sq_beneficiovalor")
public class Beneficiovalor {
	
	private Integer cdbeneficiovalor;
	private Beneficio beneficio;
	private Date dtinicio;
	private Date dtfim;
	private Money valor;
	
	@Id
	@GeneratedValue(generator="sq_beneficiovalor", strategy=GenerationType.AUTO)
	public Integer getCdbeneficiovalor() {
		return cdbeneficiovalor;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbeneficio")	
	public Beneficio getBeneficio() {
		return beneficio;
	}
	@DisplayName("Data in�cio")
	@Required
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data fim")
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Valor")
	@Required
	public Money getValor() {
		return valor;
	}
	
	public void setCdbeneficiovalor(Integer cdbeneficiovalor) {
		this.cdbeneficiovalor = cdbeneficiovalor;
	}
	public void setBeneficio(Beneficio beneficio) {
		this.beneficio = beneficio;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}	
}