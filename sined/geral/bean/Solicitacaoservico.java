package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.SolicitacaoServicoPessoa;
import br.com.linkcom.sined.geral.bean.enumeration.Solicitacaoservicoprioridade;
import br.com.linkcom.sined.geral.bean.enumeration.Solicitacaoservicosituacao;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_solicitacaoservico",sequenceName="sq_solicitacaoservico")
@DisplayName("Solicita��o de servi�o")
public class Solicitacaoservico implements Log{
	
	protected Integer cdsolicitacaoservico;
	protected Cliente cliente;
	protected Endereco enderecocliente;
	protected Timestamp dtenvio;
	protected Solicitacaoservicosituacao solicitacaoservicosituacao = Solicitacaoservicosituacao.PENDENTE;
	protected Solicitacaoservicotipo solicitacaoservicotipo;
	protected Usuario requisitante;
	protected Projeto projeto;
	protected Solicitacaoservicoprioridade solicitacaoservicoprioridade;
	protected Timestamp dtinicio;
	protected Timestamp dtfim;
	protected String descricao;
	protected Arquivo arquivo;
	protected SolicitacaoServicoPessoa solicitacaoServicoPessoa;
	protected Fornecedor fornecedor;
	protected String outros;
	protected Boolean isEnderecoAvulso = false;
	protected String enderecoAvulso;
	
	//LISTAS
	protected List<Solicitacaoservicohistorico> listaHistorico = new ListSet<Solicitacaoservicohistorico>(Solicitacaoservicohistorico.class);
	protected Set<Solicitacaoservicorequisitado> listaRequisitado = new ListSet<Solicitacaoservicorequisitado>(Solicitacaoservicorequisitado.class);
	protected Set<Solicitacaoservicoitem> listaItem = new ListSet<Solicitacaoservicoitem>(Solicitacaoservicoitem.class);
	
	//LOG
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	//TRANSIENTES
	protected Departamento departamento;
	protected String horario;
	protected Oportunidade oportunidade;
	protected String historico;
	
	public Solicitacaoservico() {
	}
	
	public Solicitacaoservico(Integer cdsolicitacaoservico) {
		this.cdsolicitacaoservico = cdsolicitacaoservico;
	}
	
	@Id
	@DisplayName("Solicita��o")
	@GeneratedValue(generator="sq_solicitacaoservico",strategy=GenerationType.AUTO)
	public Integer getCdsolicitacaoservico() {
		return cdsolicitacaoservico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}

	@DisplayName("Enviado em")
	public Timestamp getDtenvio() {
		return dtenvio;
	}

	@Required
	@DisplayName("Situa��o")
	@Column(name="situacao")
	public Solicitacaoservicosituacao getSolicitacaoservicosituacao() {
		return solicitacaoservicosituacao;
	}

	@Required
	@DisplayName("Tipo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsolicitacaoservicotipo")
	public Solicitacaoservicotipo getSolicitacaoservicotipo() {
		return solicitacaoservicotipo;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrequisitante")
	public Usuario getRequisitante() {
		return requisitante;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}

	@Required
	@DisplayName("Prioridade")
	@Column(name="prioridade")
	public Solicitacaoservicoprioridade getSolicitacaoservicoprioridade() {
		return solicitacaoservicoprioridade;
	}

	@DisplayName("In�cio")
	public Timestamp getDtinicio() {
		return dtinicio;
	}

	@DisplayName("Fim")
	public Timestamp getDtfim() {
		return dtfim;
	}

	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	@DisplayName("Endere�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdenderecocliente")
	public Endereco getEnderecocliente() {
		return enderecocliente;
	}

	@DisplayName("Hist�rico")
	@Transient
	@MaxLength(1024)
	public String getHistorico() {
		return historico;
	}

	public void setHistorico(String historico) {
		this.historico = historico;
	}

	public void setEnderecocliente(Endereco enderecocliente) {
		this.enderecocliente = enderecocliente;
	}

	public void setCdsolicitacaoservico(Integer cdsolicitacaoservico) {
		this.cdsolicitacaoservico = cdsolicitacaoservico;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setDtenvio(Timestamp dtenvio) {
		this.dtenvio = dtenvio;
	}

	public void setSolicitacaoservicosituacao(
			Solicitacaoservicosituacao solicitacaoservicosituacao) {
		this.solicitacaoservicosituacao = solicitacaoservicosituacao;
	}

	public void setSolicitacaoservicotipo(
			Solicitacaoservicotipo solicitacaoservicotipo) {
		this.solicitacaoservicotipo = solicitacaoservicotipo;
	}

	public void setRequisitante(Usuario requisitante) {
		this.requisitante = requisitante;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public void setSolicitacaoservicoprioridade(
			Solicitacaoservicoprioridade solicitacaoservicoprioridade) {
		this.solicitacaoservicoprioridade = solicitacaoservicoprioridade;
	}

	public void setDtinicio(Timestamp dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setDtfim(Timestamp dtfim) {
		this.dtfim = dtfim;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	// LISTAS
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="solicitacaoservico")
	public List<Solicitacaoservicohistorico> getListaHistorico() {
		return listaHistorico;
	}
	
	@DisplayName("Requisitado")
	@OneToMany(mappedBy="solicitacaoservico")
	public Set<Solicitacaoservicorequisitado> getListaRequisitado() {
		return listaRequisitado;
	}
	
	@DisplayName("Campos adicionais")
	@OneToMany(mappedBy="solicitacaoservico")
	public Set<Solicitacaoservicoitem> getListaItem() {
		return listaItem;
	}
	
	public void setListaItem(Set<Solicitacaoservicoitem> listaItem) {
		this.listaItem = listaItem;
	}
	
	public void setListaRequisitado(
			Set<Solicitacaoservicorequisitado> listaRequisitado) {
		this.listaRequisitado = listaRequisitado;
	}
	
	public void setListaHistorico(
			List<Solicitacaoservicohistorico> listaHistorico) {
		this.listaHistorico = listaHistorico;
	}
	
	//LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	//TRANSIENTES
	@Transient
	public Departamento getDepartamento() {
		return departamento;
	}
	
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
	@Transient
	public Oportunidade getOportunidade() {
		return oportunidade;
	}
	
	public void setOportunidade(Oportunidade oportunidade) {
		this.oportunidade = oportunidade;
	}
	
	@Transient
	@DisplayName("Data de in�cio")
	public String getDtiniciostr(){
		if(getDtinicio() != null){
			return new SimpleDateFormat("dd/MM/yyyy hh:mm").format(getDtinicio());
		} else return null;
	}
	
	@Transient
	@DisplayName("Data de fim")
	public String getDtfimstr(){
		if(getDtfim() != null){
			return new SimpleDateFormat("dd/MM/yyyy hh:mm").format(getDtfim());
		} else return null;
	}
	
	@Transient
	public boolean getVencida(){
		return getDtfim() != null && getDtfim().getTime() < System.currentTimeMillis();	
	}
	
	@Transient
	public String getHorario() {
		return horario;
	}
	public void setHorario(String horario) {
		this.horario = horario;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	@DisplayName("Arquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	@DisplayName("Referente a")
	public SolicitacaoServicoPessoa getSolicitacaoServicoPessoa() {
		return solicitacaoServicoPessoa;
	}
	public void setSolicitacaoServicoPessoa(SolicitacaoServicoPessoa solicitacaoServicoPessoa) {
		this.solicitacaoServicoPessoa = solicitacaoServicoPessoa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	@DisplayName("Outros")
	public String getOutros() {
		return outros;
	}
	public void setOutros(String outros) {
		this.outros = outros;
	}
	
	@DisplayName("Endere�o Avulso")
	@MaxLength(1000)
	public String getEnderecoAvulso() {
		return enderecoAvulso;
	}
	public void setEnderecoAvulso(String enderecoAvulso) {
		this.enderecoAvulso = enderecoAvulso;
	}
	
	@DisplayName("Endere�o Avulso ?")
	public Boolean getIsEnderecoAvulso() {
		return isEnderecoAvulso;
	}
	public void setIsEnderecoAvulso(Boolean isEnderecoAvulso) {
		this.isEnderecoAvulso = isEnderecoAvulso;
	}
}
