package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_formularh", sequenceName="sq_formularh")
public class Formularh implements Log {
	
	protected Integer cdformularh;
	protected String nome;
	protected Boolean ativa;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Set<Formularhcargo> formularhcargo;
	protected List<Formularhitem> formularhitem;
	protected Set<Formularhvariavel> formularhvariavel;
	
	@Id
	@GeneratedValue(generator="sq_formularh",strategy=GenerationType.AUTO)
	public Integer getCdformularh() {
		return cdformularh;
	}
	@DisplayName("Descri��o")
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@DisplayName("Ativo")
	@Required
	public Boolean getAtiva() {
		return ativa;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}			
	@OneToMany(mappedBy="formularh")
	@DisplayName("Cargo")
	public Set<Formularhcargo> getFormularhcargo() {
		return formularhcargo;
	}
	@OneToMany(mappedBy="formularh")
	public List<Formularhitem> getFormularhitem() {
		return formularhitem;
	}	
	@OneToMany(mappedBy="formularh")
	public Set<Formularhvariavel> getFormularhvariavel() {
		return formularhvariavel;
	}
	public void setFormularhvariavel(Set<Formularhvariavel> formularhvariavel) {
		this.formularhvariavel = formularhvariavel;
	}
	public void setFormularhitem(List<Formularhitem> formularhitem) {
		this.formularhitem = formularhitem;
	}
	public void setCdformularh(Integer cdformularh) {
		this.cdformularh = cdformularh;
	}
	public void setFormularhcargo(Set<Formularhcargo> formularhcargo) {
		this.formularhcargo = formularhcargo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtiva(Boolean ativa) {
		this.ativa = ativa;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}