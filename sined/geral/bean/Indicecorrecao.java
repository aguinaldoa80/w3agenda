package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_indicecorrecao", sequenceName = "sq_indicecorrecao")
@DisplayName("�ndice de corre��o")
public class Indicecorrecao  implements Log{

	protected Integer cdindicecorrecao;
	protected String sigla;
	protected String descricao;
	
	protected Boolean tipocalculo = Boolean.TRUE;
	
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	protected List<Indicecorrecaovalor> listaIndicecorrecaovalor = new ListSet<Indicecorrecaovalor>(Indicecorrecaovalor.class);
	
	public Indicecorrecao() {
	}
	
	public Indicecorrecao(Integer cdindicecorrecao) {
		this.cdindicecorrecao = cdindicecorrecao;
	}
	
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_indicecorrecao")
	public Integer getCdindicecorrecao() {
		return cdindicecorrecao;
	}

	@Required
	@DescriptionProperty
	@MaxLength(10)
	public String getSigla() {
		return sigla;
	}

	@DisplayName("Descri��o")
	@MaxLength(80)
	public String getDescricao() {
		return descricao;
	}
	
	@DisplayName("Valores")
	@OneToMany(mappedBy="indicecorrecao")
	public List<Indicecorrecaovalor> getListaIndicecorrecaovalor() {
		return listaIndicecorrecaovalor;
	}
	@DisplayName("Tipo C�lculo")
	public Boolean getTipocalculo() {
		return tipocalculo;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdindicecorrecao(Integer cdindicecorrecao) {
		this.cdindicecorrecao = cdindicecorrecao;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setListaIndicecorrecaovalor(
			List<Indicecorrecaovalor> listaIndicecorrecaovalor) {
		this.listaIndicecorrecaovalor = listaIndicecorrecaovalor;
	}
	
	public void setTipocalculo(Boolean tipocalculo) {
		this.tipocalculo = tipocalculo;
	}
	
}
