package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_indicerecursogeral", sequenceName = "sq_indicerecursogeral")
public class Indicerecursogeral implements Log {

	protected Integer cdindicerecursogeral;
	protected Indice indice;
	protected Material material;
	protected Double qtde;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected String outro;
	protected Unidademedida unidademedida;
	
	protected Boolean tipo;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_indicerecursogeral")
	public Integer getCdindicerecursogeral() {
		return cdindicerecursogeral;
	}
	
	@Required
	@DisplayName("Qtde.")
	@MaxLength(9)
	public Double getQtde() {
		return qtde;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdindice")
	public Indice getIndice() {
		return indice;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	public void setCdindicerecursogeral(Integer id) {
		this.cdindicerecursogeral = id;
	}
	
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	
	public void setIndice(Indice indice) {
		this.indice = indice;
	}

	@MaxLength(50)
	public String getOutro() {
		return outro;
	}
	
	public void setOutro(String outro) {
		this.outro = outro;
	}
	
	@DisplayName("Unidade medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	
//	LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public Boolean getTipo() {
		return tipo;
	}
	public void setTipo(Boolean tipo) {
		this.tipo = tipo;
	}
	
}
