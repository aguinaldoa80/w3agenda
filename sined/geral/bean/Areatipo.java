package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_areatipo", sequenceName = "sq_areatipo")
public class Areatipo {
	
	protected Integer cdareatipo;
	protected String descricao;
	protected Area area;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_areatipo")
	public Integer getCdareatipo() {
		return cdareatipo;
	}
	@DisplayName("Descri��o")
	@MaxLength(50)
	@Required
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("�rea")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdarea")
	public Area getArea() {
		return area;
	}
	
	public void setCdareatipo(Integer cdareatipo) {
		this.cdareatipo = cdareatipo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setArea(Area area) {
		this.area = area;
	}
	
	

}
