package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Vendaorcamentosituacao {

	public static Vendaorcamentosituacao EM_ABERTO = new Vendaorcamentosituacao(1, "Em Aberto");
	public static Vendaorcamentosituacao AUTORIZADO = new Vendaorcamentosituacao(2, "Autorizado");
	public static Vendaorcamentosituacao CANCELADA = new Vendaorcamentosituacao(3, "Cancelado");
	public static Vendaorcamentosituacao REPROVADO = new Vendaorcamentosituacao(4, "Reprovado");
	
	protected Integer cdvendaorcamentosituacao;
	protected String descricao;
	
	public Vendaorcamentosituacao() {
	}
	
	public Vendaorcamentosituacao(Integer cdvendaorcamentosituacao, String descricao) {
		this.cdvendaorcamentosituacao = cdvendaorcamentosituacao;
		this.descricao = descricao;
	}
	
	@Id
	public Integer getCdvendaorcamentosituacao() {
		return cdvendaorcamentosituacao;
	}
	
	@DisplayName("Nome")
	@DescriptionProperty
	@MaxLength(20)
	public String getDescricao() {
		return descricao;
	}
	
	public void setCdvendaorcamentosituacao(Integer cdvendaorcamentosituacao) {
		this.cdvendaorcamentosituacao = cdvendaorcamentosituacao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdvendaorcamentosituacao == null) ? 0
						: cdvendaorcamentosituacao.hashCode());
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vendaorcamentosituacao other = (Vendaorcamentosituacao) obj;
		if (cdvendaorcamentosituacao == null) {
			if (other.cdvendaorcamentosituacao != null)
				return false;
		} else if (!cdvendaorcamentosituacao
				.equals(other.cdvendaorcamentosituacao))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		return true;
	}
	
}
