package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;

@Entity
@SequenceGenerator(name="sq_notafiscalservicoduplicata",sequenceName="sq_notafiscalservicoduplicata")
public class Notafiscalservicoduplicata {
	
	protected Integer cdnotafiscalservicoduplicata;
	protected NotaFiscalServico notafiscalservico;
	protected Documentotipo documentotipo;
	protected String numero;
	protected Date dtvencimento;
	protected Money valor;
	
	public Notafiscalservicoduplicata(){}
	
	public Notafiscalservicoduplicata(Notafiscalservicoduplicata bean){
		this.documentotipo = bean.getDocumentotipo();
		this.numero = bean.getNumero();
		this.dtvencimento = bean.getDtvencimento();
		this.valor = bean.getValor();
	}
	
	@Id
	@GeneratedValue(generator="sq_notafiscalservicoduplicata", strategy=GenerationType.AUTO)
	public Integer getCdnotafiscalservicoduplicata() {
		return cdnotafiscalservicoduplicata;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdnotafiscalservico")
	public NotaFiscalServico getNotafiscalservico() {
		return notafiscalservico;
	}
	
	@DisplayName("Forma de pagamento")
	@JoinColumn(name="cdformapagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}

	@DisplayName("Data de vencimento")
	public Date getDtvencimento() {
		return dtvencimento;
	}

	public Money getValor() {
		return valor;
	}

	public void setCdnotafiscalservicoduplicata(Integer cdnotafiscalservicoduplicata) {
		this.cdnotafiscalservicoduplicata = cdnotafiscalservicoduplicata;
	}

	public void setNotafiscalservico(NotaFiscalServico notafiscalservico) {
		this.notafiscalservico = notafiscalservico;
	}
	
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}
}