package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_clienteindicacao", sequenceName = "sq_clienteindicacao")
public class Clienteindicacao {
	
	protected Integer cdclienteindicacao;
	protected String descricao;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_clienteindicacao")
	public Integer getCdclienteindicacao() {
		return cdclienteindicacao;
	}
	
	@Required	
	@MaxLength(80)
	@DescriptionProperty
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	
	public void setCdclienteindicacao(Integer cdclienteindicacao) {
		this.cdclienteindicacao = cdclienteindicacao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}

}
