package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name="sq_tarefacolaborador", sequenceName="sq_tarefacolaborador")
public class Tarefacolaborador implements Log, PermissaoProjeto {
	
	protected Integer cdtarefacolaborador;
	protected Colaborador colaborador;
	protected Tarefa tarefa;
	protected Boolean exclusivo;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_tarefacolaborador", strategy=GenerationType.AUTO)
	public Integer getCdtarefacolaborador() {
		return cdtarefacolaborador;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	@DisplayName("Colaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}	
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtarefa")	
	public Tarefa getTarefa() {
		return tarefa;
	}

	public Boolean getExclusivo() {
		return exclusivo;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdtarefacolaborador(Integer cdtarefacolaborador) {
		this.cdtarefacolaborador = cdtarefacolaborador;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setExclusivo(Boolean exclusivo) {
		this.exclusivo = exclusivo;
	}
	
	
	public String subQueryProjeto(){
		return  "select tarefacolaboradorsubQueryProjeto.cdtarefacolaborador" +
				"from Tarefacolaborador tarefacolaboradorsubQueryProjeto " +
				"join tarefacolaboradorsubQueryProjeto.tarefa tarefasubQueryProjeto " +
				"join tarefasubQueryProjeto.planejamento planejamentosubQueryProjeto " +
				"join planejamentosubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
	
}
