package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_clienterelacao", sequenceName = "sq_clienterelacao")
public class Clienterelacao {
	
	protected Integer cdclienterelacao;
	protected Clienterelacaotipo clienterelacaotipo;
	protected Cliente cliente;
	protected Cliente clienteoutro;
	
	public Clienterelacao() {
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_clienterelacao")
	public Integer getCdclienterelacao() {
		return cdclienterelacao;
	}

	public void setCdclienterelacao(Integer cdclienterelacao) {
		this.cdclienterelacao = cdclienterelacao;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdclienterelacaotipo")
	@DisplayName("Endere�o")
	public Clienterelacaotipo getClienterelacaotipo() {
		return clienterelacaotipo;
	}
	public void setClienterelacaotipo(Clienterelacaotipo clienterelacaotipo) {
		this.clienterelacaotipo = clienterelacaotipo;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdclienteoutro")
	@DisplayName("Outro Cliente")
	public Cliente getClienteoutro() {
		return clienteoutro;
	}
	
	public void setClienteoutro(Cliente clienteoutro) {
		this.clienteoutro = clienteoutro;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
