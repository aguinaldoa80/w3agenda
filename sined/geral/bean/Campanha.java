package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_campanha", sequenceName = "sq_campanha")
public class Campanha implements Log{
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Integer cdcampanha;
	protected String nome;
	protected Date dtcriacao;
	protected Date dtprevisaofechamento;
	protected String observacao;
	
	protected Colaborador responsavel;
	protected Projeto projeto;
	protected Arquivo arquivo;
	
	protected Set<Campanhalead> listCampanhalead = new ListSet<Campanhalead>(Campanhalead.class);
	protected List<Campanhacontato> listCampanhacontato;
	protected List<Campanhahistorico> listCampanhahistorico;
	
	protected Campanhasituacao campanhasituacao;
	protected Campanhatipo campanhatipo;
	
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_campanha")
	public Integer getCdcampanha() {
		return cdcampanha;
	}

	@Required	
	@MaxLength(80)
	@DescriptionProperty
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}

	@Required	
	@DisplayName("Data de Cria��o")
	public Date getDtcriacao() {
		return dtcriacao;
	}

	@DisplayName("Previs�o de Fechamento")
	public Date getDtprevisaofechamento() {
		return dtprevisaofechamento;
	}

	@Transient
	@MaxLength(1024)
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	@Required	
	@DisplayName("Respons�vel")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdresponsavel")
	public Colaborador getResponsavel() {
		return responsavel;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}

	@DisplayName("Lead")
	@OneToMany(mappedBy="campanha")
	public Set<Campanhalead> getListCampanhalead() {
		return listCampanhalead;
	}

	@DisplayName("Conta")
	@OneToMany(mappedBy="campanha")
	public List<Campanhacontato> getListCampanhacontato() {
		return listCampanhacontato;
	}

	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="campanha")
	public List<Campanhahistorico> getListCampanhahistorico() {
		return listCampanhahistorico;
	}

	@Required
	@DisplayName("Situa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcampanhasituacao")
	public Campanhasituacao getCampanhasituacao() {
		return campanhasituacao;
	}

	@Required
	@DisplayName("Tipo da Campanha")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcampanhatipo")
	public Campanhatipo getCampanhatipo() {
		return campanhatipo;
	}
	
	@DisplayName("Anexo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	

	public void setCdcampanha(Integer cdcampanha) {
		this.cdcampanha = cdcampanha;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setDtcriacao(Date dtcriacao) {
		this.dtcriacao = dtcriacao;
	}

	public void setDtprevisaofechamento(Date dtprevisaofechamento) {
		this.dtprevisaofechamento = dtprevisaofechamento;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public void setListCampanhalead(Set<Campanhalead> listCampanhalead) {
		this.listCampanhalead = listCampanhalead;
	}

	public void setListCampanhacontato(List<Campanhacontato> listCampanhacontato) {
		this.listCampanhacontato = listCampanhacontato;
	}

	public void setListCampanhahistorico(
			List<Campanhahistorico> listCampanhahistorico) {
		this.listCampanhahistorico = listCampanhahistorico;
	}

	public void setCampanhasituacao(Campanhasituacao campanhasituacao) {
		this.campanhasituacao = campanhasituacao;
	}

	public void setCampanhatipo(Campanhatipo campanhatipo) {
		this.campanhatipo = campanhatipo;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
