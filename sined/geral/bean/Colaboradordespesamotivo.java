package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@SequenceGenerator(name = "sq_colaboradordespesamotivo", sequenceName = "sq_colaboradordespesamotivo")
@DisplayName("Tipo de remuneração")
@Entity
public class Colaboradordespesamotivo implements Log{

	protected Integer cdcolaboradordespesamotivo;
	protected String descricao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected List<ColaboradorDespesaMotivoItem> listaMotivoItem;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradordespesamotivo")
	public Integer getCdcolaboradordespesamotivo() {
		return cdcolaboradordespesamotivo;
	}
	
	public void setCdcolaboradordespesamotivo(Integer cdcolaboradordespesamotivo) {
		this.cdcolaboradordespesamotivo = cdcolaboradordespesamotivo;
	}
	
	@Required
	@DisplayName ("Descrição")
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@OneToMany(mappedBy="colaboradorDespesaMotivo", fetch=FetchType.LAZY)
	public List<ColaboradorDespesaMotivoItem> getListaMotivoItem() {
		return listaMotivoItem;
	}
	public void setListaMotivoItem(
			List<ColaboradorDespesaMotivoItem> listaMotivoItem) {
		this.listaMotivoItem = listaMotivoItem;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Colaboradordespesamotivo) {
			Colaboradordespesamotivo c = (Colaboradordespesamotivo) obj;
			return c.getCdcolaboradordespesamotivo().equals(this.getCdcolaboradordespesamotivo());
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if(cdcolaboradordespesamotivo != null)
			return cdcolaboradordespesamotivo.hashCode();
		return 0;
	}	
}
