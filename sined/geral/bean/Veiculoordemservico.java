package br.com.linkcom.sined.geral.bean;


import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.EnumSituacaomanutencao;
import br.com.linkcom.sined.geral.bean.view.Vwsituacaomanutencao;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_veiculoordemservico",sequenceName="sq_veiculoordemservico")
@DisplayName("Inspe��o\\Manuten��o\\Agendamento")
public class Veiculoordemservico implements Log{
	
	protected Integer cdveiculoordemservico;
	protected Veiculo veiculo;
	protected Date dtprevista ;
	protected Date dtrealizada;//data prevista
	protected Ordemservicotipo ordemservicotipo;
	protected List<Veiculoordemservicoitem> listaVeiculoordemservicoitem ;
	protected Long km;
	protected Double horimetro;
	private Boolean enviaemail = false;
	protected Vwsituacaomanutencao vwsituacaomanutencao;	
	protected Veiculouso veiculouso;
	
	//Log
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;

	//transient
	protected Integer diasPrevistos;//qtd dias previstos para manutencao
	protected Boolean opcaoVisual = null;
	protected Date dataInicio;
	protected Date dataFim;
	protected Boolean ultimaInspecao = null;
	protected List<Categoriaiteminspecao> listaCategoriaItemInspecao;
	protected List<Inspecaoitem> listaInspecaoitem;
	protected String value;
	protected Boolean pendente;
	protected String manutencao;
	protected Boolean vencida;
	protected Boolean itensListaOk = true;
	protected Boolean tipoinspecao;
	protected EnumSituacaomanutencao enumSituacaomanutencao;
	protected Boolean fromVeiculouso;
	
	public Veiculoordemservico() {}
	
	public Veiculoordemservico(Integer cdveiculoordemservico) {
		this.cdveiculoordemservico = cdveiculoordemservico;
	}
	@DisplayName("Tipo de inspe��o")
	public Boolean getTipoinspecao() {
		return tipoinspecao;
	}
	public void setTipoinspecao(Boolean tipoinspecao) {
		this.tipoinspecao = tipoinspecao;
	}
	
	@Transient
	public EnumSituacaomanutencao getEnumSituacaomanutencao() {
		return enumSituacaomanutencao;
	}
	
	public void setEnumSituacaomanutencao(EnumSituacaomanutencao enumSituacaomanutencao) {
		this.enumSituacaomanutencao = enumSituacaomanutencao;
	}
	
	@Transient
	@DisplayName("Todos os itens est�o OK?")
	public Boolean getItensListaOk() {
		return itensListaOk;
	}
	@Transient
	public List<Categoriaiteminspecao> getListaCategoriaItemInspecao() {
		return listaCategoriaItemInspecao;
	}
	
	@Transient
	public String getValue() {
		return value;
	}
	
	@Transient
	@MaxLength(3)
	public Integer getDiasPrevistos() {
		return diasPrevistos;
	}

	@Transient
	@DisplayName("�ltima inspe��o")
	public Boolean getUltimaInspecao() {
		return ultimaInspecao;
	}

	@Transient
	@DisplayName("Tipo de inspe��o")
	public Boolean getOpcaoVisual() {
		return opcaoVisual;
	}

	@Transient
	@DisplayName("Data in�cio")
	public Date getDataInicio() {
		return dataInicio;
	}
	@Transient
	@DisplayName("Data fim")
	public Date getDataFim() {
		return dataFim;
	}
	// fim transient
	
	@DisplayName("Data agendada")
	public Date getDtprevista() {
		return dtprevista;
	}
	
	@DisplayName("Data da inspe��o realizada")
	@Required
	public Date getDtrealizada() {
		return dtrealizada;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemservicotipo")
	public Ordemservicotipo getOrdemservicotipo() {
		return ordemservicotipo;
	}

	@OneToMany(mappedBy="ordemservico",fetch=FetchType.LAZY)
	public List<Veiculoordemservicoitem> getListaVeiculoordemservicoitem() {
		return listaVeiculoordemservicoitem;
	}
	
	@Id
	@DisplayName("")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_veiculoordemservico")
	public Integer getCdveiculoordemservico() {
		return cdveiculoordemservico;
	}
	
	@DisplayName("Ve�culo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculo")
	public Veiculo getVeiculo() {
		return veiculo;
	}
	@DisplayName("Km")
	@MaxLength(9)
	public Long getKm() {
		return km;
	}
	
	@DisplayName("Hor�metro")
	public Double getHorimetro() {
		return horimetro;
	}

	@DisplayName("Enviar e-mail")
	public Boolean getEnviaemail() {
		return enviaemail;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculoordemservico", insertable=false, updatable=false)
	public Vwsituacaomanutencao getVwsituacaomanutencao() {
		return vwsituacaomanutencao;
	}
	
	public void setCdveiculoordemservico(Integer cdveiculoordemservico) {
		this.cdveiculoordemservico = cdveiculoordemservico;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public void setOpcaoVisual(Boolean opcaoVisual) {
		this.opcaoVisual = opcaoVisual;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public void setListaVeiculoordemservicoitem(
			List<Veiculoordemservicoitem> listaVeiculoordemservicoitem) {
		this.listaVeiculoordemservicoitem = listaVeiculoordemservicoitem;
	}

	public void setDtprevista(Date dtprevista) {
		this.dtprevista = dtprevista;
	}

	public void setDtrealizada(Date dtrealizada) {
		this.dtrealizada = dtrealizada;
	}
	
	public void setDiasPrevistos(Integer diasPrevistos) {
		this.diasPrevistos = diasPrevistos;
	}
	public void setUltimaInspecao(Boolean ultimaInspecao) {
		this.ultimaInspecao = ultimaInspecao;
	}
	public void setOrdemservicotipo(Ordemservicotipo ordemservicotipo) {
		this.ordemservicotipo = ordemservicotipo;
	}
	
	public void setKm(Long km) {
		this.km = km;
	}
	public void setHorimetro(Double horimetro) {
		this.horimetro = horimetro;
	}
	public void setListaCategoriaItemInspecao(
			List<Categoriaiteminspecao> listaCategoriaItemInspecao) {
		this.listaCategoriaItemInspecao = listaCategoriaItemInspecao;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	@Transient
	public List<Inspecaoitem> getListaInspecaoitem() {
		return listaInspecaoitem;
	}
	
	public void setListaInspecaoitem(List<Inspecaoitem> listaInspecaoitem) {
		this.listaInspecaoitem = listaInspecaoitem;
	}
	
	
	//Log
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setEnviaemail(Boolean enviaemail) {
		this.enviaemail = enviaemail;
	}
	
	public void setVwsituacaomanutencao(Vwsituacaomanutencao vwsituacaomanutencao) {
		this.vwsituacaomanutencao = vwsituacaomanutencao;
	}
	
	@Transient
	public Boolean getPendente() {
		return pendente;
	}
	
	public void setPendente(Boolean pendente) {
		this.pendente = pendente;
	}
	
	@Transient
	@DisplayName("Manuten��o")
	public String getManutencao() {
		return manutencao;
	}
	
	public void setManutencao(String manutencao) {
		this.manutencao = manutencao;
	}
	
	@Transient
	public Boolean getVencida() {
		return vencida;
	}
	
	public void setVencida(Boolean vencida) {
		this.vencida = vencida;
	}
	
	public void setItensListaOk(Boolean itensListaOk) {
		this.itensListaOk = itensListaOk;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculouso")
	public Veiculouso getVeiculouso() {
		return veiculouso;
	}
	public void setVeiculouso(Veiculouso veiculouso) {
		this.veiculouso = veiculouso;
	}

	@Transient
	public Boolean getFromVeiculouso() {
		return fromVeiculouso;
	}
	public void setFromVeiculouso(Boolean fromVeiculouso) {
		this.fromVeiculouso = fromVeiculouso;
	}
}
