package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "sq_romaneioorigem", sequenceName = "sq_romaneioorigem")
public class Romaneioorigem {

	protected Integer cdromaneioorigem;
	protected Romaneio romaneio;
	protected Entrega entrega;
	protected Contrato contrato;
	protected Contrato contratofechamento;
	protected Requisicaomaterial requisicaomaterial;
	protected Tarefa tarefa;
	protected boolean estoque = false;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_romaneioorigem")
	public Integer getCdromaneioorigem() {
		return cdromaneioorigem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdromaneio")
	public Romaneio getRomaneio() {
		return romaneio;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentrega")
	public Entrega getEntrega() {
		return entrega;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontrato")
	public Contrato getContrato() {
		return contrato;
	}
	public boolean isEstoque() {
		return estoque;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratofechamento")
	public Contrato getContratofechamento() {
		return contratofechamento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtarefa")
	public Tarefa getTarefa() {
		return tarefa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrequisicaomaterial")
	public Requisicaomaterial getRequisicaomaterial() {
		return requisicaomaterial;
	}
	public void setRequisicaomaterial(Requisicaomaterial requisicaomaterial) {
		this.requisicaomaterial = requisicaomaterial;
	}
	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}
	public void setContratofechamento(Contrato contratofechamento) {
		this.contratofechamento = contratofechamento;
	}
	public void setEstoque(boolean estoque) {
		this.estoque = estoque;
	}
	public void setEntrega(Entrega entrega) {
		this.entrega = entrega;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public void setRomaneio(Romaneio romaneio) {
		this.romaneio = romaneio;
	}
	public void setCdromaneioorigem(Integer cdromaneioorigem) {
		this.cdromaneioorigem = cdromaneioorigem;
	}
	
}
