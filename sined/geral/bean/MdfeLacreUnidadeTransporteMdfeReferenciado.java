package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_mdfelacreunidadetransportemdfereferenciado", sequenceName = "sq_mdfelacreunidadetransportemdfereferenciado")
public class MdfeLacreUnidadeTransporteMdfeReferenciado {

	protected Integer cdMdfeLacreUnidadeTransporteMdfeReferenciado;
	protected Mdfe mdfe;
	protected String idUnidadeTransporte;
	protected String idLacre;
	protected String numeroLacre;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfelacreunidadetransportemdfereferenciado")
	public Integer getCdMdfeLacreUnidadeTransporteMdfeReferenciado() {
		return cdMdfeLacreUnidadeTransporteMdfeReferenciado;
	}
	public void setCdMdfeLacreUnidadeTransporteMdfeReferenciado(Integer cdMdfeLacreUnidadeTransporte) {
		this.cdMdfeLacreUnidadeTransporteMdfeReferenciado = cdMdfeLacreUnidadeTransporte;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@DisplayName("ID da unidade de transporte")
	public String getIdUnidadeTransporte() {
		return idUnidadeTransporte;
	}
	public void setIdUnidadeTransporte(String idUnidadeTransporte) {
		this.idUnidadeTransporte = idUnidadeTransporte;
	}
	
	@DisplayName("ID do lacre")
	public String getIdLacre() {
		return idLacre;
	}
	public void setIdLacre(String idLacre) {
		this.idLacre = idLacre;
	}
	
	@Required
	@MaxLength(value=20)
	@DisplayName("N�mero do lacre")
	public String getNumeroLacre() {
		return numeroLacre;
	}
	public void setNumeroLacre(String numeroLacre) {
		this.numeroLacre = numeroLacre;
	}
}
