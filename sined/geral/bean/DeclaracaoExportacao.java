package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.validator.Length;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.TipoConhecimentoDeEmbarque;
import br.com.linkcom.sined.geral.bean.enumeration.TipoDocumentoDeclaracao;
import br.com.linkcom.sined.geral.bean.enumeration.TipoNaturezaDaDeclaracao;
import br.com.linkcom.sined.util.Log;

@Entity
@DisplayName("Declara��o de Exporta��o")
@SequenceGenerator(name = "sq_declaracao_exportacao", sequenceName = "sq_declaracao_exportacao")
public class DeclaracaoExportacao implements Log{

	private Integer cdDeclaracaoExportacao;
	private Empresa empresa;
	private TipoDocumentoDeclaracao tipoDocumentoDeclaracao = TipoDocumentoDeclaracao.DECLARACAO_EXPORTACAO;
	private String numeroDeclaracao;
	private Date dtDeclaracao;
	private TipoNaturezaDaDeclaracao tipoNaturezaDeclaracao = TipoNaturezaDaDeclaracao.DIRETA;
	private Date dtRE;
	private String numeroRE;
	private Date dtAverbacaoDE;
	private String numeroEmbarque;
	private TipoConhecimentoDeEmbarque tipoConhecimentoEmbarque = TipoConhecimentoDeEmbarque.AWB;
	private Pais pais;
	private List<DeclaracaoExportacaoNota> listaDeclaracaoExportacaoNota;
	private List<DeclaracaoExportacaoHistorico> listaDeclaracaoExportacaoHistorico;
	private Timestamp dtaltera;
	private Integer cdusuarioaltera;
	private Date dtConhecimentoEmbarque;
	
	/////////////////////////////////////////////////////////////////////|
	//  INICIO DOS GET 											/////////|
	/////////////////////////////////////////////////////////////////////|
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_declaracao_exportacao")
	public Integer getCdDeclaracaoExportacao() {
		return cdDeclaracaoExportacao;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdempresa")
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("TIPO DOCUMENTO DECLARA��O")
	@Required
	public TipoDocumentoDeclaracao getTipoDocumentoDeclaracao() {
		return tipoDocumentoDeclaracao;
	}
	@DisplayName("N�MERO DA DECLARA��O")
	@Required
	@Length(max=14 , message="O campo 'N�MERO DA DECLARA��O' pode ser no m�ximo 14 caracteres")
	public String getNumeroDeclaracao() {
		return numeroDeclaracao;
	}
	@DisplayName("Data Declara��o")
	@Required
	public Date getDtDeclaracao() {
		return dtDeclaracao;
	}
	@DisplayName("NATUREZA DA DECLARA��O")
	@Required
	public TipoNaturezaDaDeclaracao getTipoNaturezaDeclaracao() {
		return tipoNaturezaDeclaracao;
	}
	@DisplayName("DATA DO (RE)")
	public Date getDtRE() {
		return dtRE;
	}
	@DisplayName("N�MERO (RE)")
	public String getNumeroRE() {
		return numeroRE;
	}
	@DisplayName("DATA AVERBA��O (DE)")
	public Date getDtAverbacaoDE() {
		return dtAverbacaoDE;
	}
	@DisplayName("N�MERO EMBARQUE")
	@Length(max=18, message="O campo 'N�MERO EMBARQUE' pode ser no m�ximo 12 caracteres")
	public String getNumeroEmbarque() {
		return numeroEmbarque;
	}
	@DisplayName("CONHECIMENTO EMBARQUE")
	@Required
	public TipoConhecimentoDeEmbarque getTipoConhecimentoEmbarque() {
		return tipoConhecimentoEmbarque;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdpais")
	@Required
	@DisplayName("Pa�s")
	public Pais getPais() {
		return pais;
	}
	
	
	@DisplayName("NF Produto")
	@OneToMany(mappedBy="declaracaoExportacao")
	@Required
	public List<DeclaracaoExportacaoNota> getListaDeclaracaoExportacaoNota() {
		return listaDeclaracaoExportacaoNota;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="declaracaoExportacao")
	public List<DeclaracaoExportacaoHistorico> getListaDeclaracaoExportacaoHistorico() {
		return listaDeclaracaoExportacaoHistorico;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	@DisplayName("Data do conhecimento de Embarque")
	public Date getDtConhecimentoEmbarque() {
		return dtConhecimentoEmbarque;
	}
	
	/////////////////////////////////////////////////////////////////////|
	//  INICIO DOS SET 											/////////|
	/////////////////////////////////////////////////////////////////////|
	
	public void setDtConhecimentoEmbarque(Date dtConhecimentoEmbarque) {
		this.dtConhecimentoEmbarque = dtConhecimentoEmbarque;
	}
	public void setCdDeclaracaoExportacao(Integer cdDeclaracaoExportacao) {
		this.cdDeclaracaoExportacao = cdDeclaracaoExportacao;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setTipoDocumentoDeclaracao(
			TipoDocumentoDeclaracao tipoDocumentoDeclaracao) {
		this.tipoDocumentoDeclaracao = tipoDocumentoDeclaracao;
	}
	public void setNumeroDeclaracao(String numeroDeclaracao) {
		this.numeroDeclaracao = numeroDeclaracao;
	}
	public void setDtDeclaracao(Date dtDeclaracao) {
		this.dtDeclaracao = dtDeclaracao;
	}
	public void setTipoNaturezaDeclaracao(
			TipoNaturezaDaDeclaracao tipoNaturezaDeclaracao) {
		this.tipoNaturezaDeclaracao = tipoNaturezaDeclaracao;
	}
	public void setDtRE(Date dtRE) {
		this.dtRE = dtRE;
	}
	public void setNumeroRE(String numeroRE) {
		this.numeroRE = numeroRE;
	}
	public void setDtAverbacaoDE(Date dtAverbacaoDE) {
		this.dtAverbacaoDE = dtAverbacaoDE;
	}
	public void setNumeroEmbarque(String numeroEmbarque) {
		this.numeroEmbarque = numeroEmbarque;
	}
	public void setTipoConhecimentoEmbarque(TipoConhecimentoDeEmbarque tipoConhecimentoEmbarque) {
		this.tipoConhecimentoEmbarque = tipoConhecimentoEmbarque;
	}
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	
	public void setListaDeclaracaoExportacaoNota(List<DeclaracaoExportacaoNota> listaDeclaracaoExportacaoNota) {
		this.listaDeclaracaoExportacaoNota = listaDeclaracaoExportacaoNota;
	}
	public void setListaDeclaracaoExportacaoHistorico(List<DeclaracaoExportacaoHistorico> listaDeclaracaoExportacaoHistorico) {
		this.listaDeclaracaoExportacaoHistorico = listaDeclaracaoExportacaoHistorico;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
/////////////////////////////////////////////////////////////////////|
//  INICIO DOS CAMPOS TRANSIENT								/////////|
/////////////////////////////////////////////////////////////////////|
	@Transient
	@DescriptionProperty(usingFields={"cdDeclaracaoExportacao", "dtDeclaracao"})
	public String getDescricaoSelecao() {
		if (this.getNumeroDeclaracao() != null){
		return this.cdDeclaracaoExportacao+ " - " +this.getNumeroDeclaracao();
		}
		return this.cdDeclaracaoExportacao+ " - " ;
	}
	
	
	
	/////////////////////////////////////|
	/////	hashCode e equals   /////////|
	/////////////////////////////////////|
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdDeclaracaoExportacao == null) ? 0
						: cdDeclaracaoExportacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeclaracaoExportacao other = (DeclaracaoExportacao) obj;
		if (cdDeclaracaoExportacao == null) {
			if (other.cdDeclaracaoExportacao != null)
				return false;
		} else if (!cdDeclaracaoExportacao.equals(other.cdDeclaracaoExportacao))
			return false;
		return true;
	}
	
}
