package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MaxValue;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@DisplayName("Itens da composi��o do or�amento")
@SequenceGenerator(name = "sq_recursocomposicao", sequenceName = "sq_recursocomposicao")
public class Recursocomposicao {
	
	protected Integer cdrecursocomposicao;
	protected Orcamento orcamento;	
	protected Composicaoorcamento composicaoorcamento;
	protected Tipoitemcomposicao tipoitemcomposicao;
	protected Formulacomposicao formulacomposicao;
	protected Tiporelacaorecurso tiporelacaorecurso;
	protected Tipodependenciarecurso tipodependenciarecurso;
	protected Tipoocorrencia tipoocorrencia;
	protected Material material;
	protected String nome;
	protected Unidademedida unidademedida;
	protected Double quantidade;
	protected Double quantidadecalculada;
	protected Double numocorrencia;
	protected Money custounitario;
	protected List<Dependenciacargocomposicao> listaDependenciacargocomposicao = new ArrayList<Dependenciacargocomposicao>();
	protected List<Dependenciafaixacomposicao> listaDependenciafaixacomposicao = new ArrayList<Dependenciafaixacomposicao>();
	protected Materialgrupo materialgrupo;
	protected Referenciacalculo referenciacalculo;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_recursocomposicao")	
	public Integer getCdrecursocomposicao() {
		return cdrecursocomposicao;
	}
	
	@DisplayName("Or�amento")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdorcamento")
	@Required
	public Orcamento getOrcamento() {
		return orcamento;
	}	
	
	@DisplayName("Composi��o do or�amento")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcomposicaoorcamento")
	public Composicaoorcamento getComposicaoorcamento() {
		return composicaoorcamento;
	}
	
	@DisplayName("Tipo de item de composi��o")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdtipoitemcomposicao")
	@Required	
	public Tipoitemcomposicao getTipoitemcomposicao() {
		return tipoitemcomposicao;
	}
	
	@DisplayName("F�rmula de c�lculo da quantidade dos recursos da composi��o")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdformulacomposicao")
	public Formulacomposicao getFormulacomposicao() {
		return formulacomposicao;
	}
	
	@DisplayName("Tipo de rela��o entre recursos")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdtiporelacaorecurso")
	@Required	
	public Tiporelacaorecurso getTiporelacaorecurso() {
		return tiporelacaorecurso;
	}
	
	@DisplayName("Tipo de depend�ncia entre recursos")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdtipodependenciarecurso")
	@Required	
	public Tipodependenciarecurso getTipodependenciarecurso() {
		return tipodependenciarecurso;
	}

	@DisplayName("Periodicidade de ocorr�ncia")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdtipoocorrencia")
	@Required
	public Tipoocorrencia getTipoocorrencia() {
		return tipoocorrencia;
	}
	
	@DisplayName("Material")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	
	@DisplayName("Nome")
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Unidade de medida")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	@MinValue(0)
	@MaxValue(999999999)	
	public Double getQuantidade() {
		return quantidade;
	}
	
	@DisplayName("Quantidade calculada")
	public Double getQuantidadecalculada() {
		return quantidadecalculada;
	}
	
	@DisplayName("Ocorr�ncias")
	public Double getNumocorrencia() {
		return numocorrencia;
	}
	
	@DisplayName("Custo unit�rio")	
	public Money getCustounitario() {
		return custounitario;
	}
	
	@OneToMany(mappedBy="recursocomposicao")
	@DisplayName("Rela��o de depend�ncia entre cargos")	
	public List<Dependenciacargocomposicao> getListaDependenciacargocomposicao() {
		return listaDependenciacargocomposicao;
	}
	
	@OneToMany(mappedBy="recursocomposicao")
	@DisplayName("Rela��o de faixas de depend�ncia")	
	public List<Dependenciafaixacomposicao> getListaDependenciafaixacomposicao() {
		return listaDependenciafaixacomposicao;
	}
	
	@DisplayName("Grupo de material")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdmaterialgrupo")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdreferenciacalculo")
	public Referenciacalculo getReferenciacalculo() {
		return referenciacalculo;
	}
	
	public void setReferenciacalculo(Referenciacalculo referenciacalculo) {
		this.referenciacalculo = referenciacalculo;
	}
	
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	
	public void setCdrecursocomposicao(Integer cdrecursocomposicao) {
		this.cdrecursocomposicao = cdrecursocomposicao;
	}
	
	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}	
	
	public void setComposicaoorcamento(Composicaoorcamento composicaoorcamento) {
		this.composicaoorcamento = composicaoorcamento;
	}
	
	public void setTipoitemcomposicao(Tipoitemcomposicao tipoitemcomposicao) {
		this.tipoitemcomposicao = tipoitemcomposicao;
	}
	
	public void setFormulacomposicao(Formulacomposicao formulacomposicao) {
		this.formulacomposicao = formulacomposicao;
	}
	
	public void setTiporelacaorecurso(Tiporelacaorecurso tiporelacaorecurso) {
		this.tiporelacaorecurso = tiporelacaorecurso;
	}
	
	public void setTipodependenciarecurso(Tipodependenciarecurso tipodependenciarecurso) {
		this.tipodependenciarecurso = tipodependenciarecurso;
	}
	
	public void setTipoocorrencia(Tipoocorrencia tipoocorrencia) {
		this.tipoocorrencia = tipoocorrencia;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}	
	
	public void setQuantidadecalculada(Double quantidadecalculada) {
		this.quantidadecalculada = quantidadecalculada;
	}

	public void setNumocorrencia(Double numocorrencia) {
		this.numocorrencia = numocorrencia;
	}

	public void setCustounitario(Money custounitario) {
		this.custounitario = custounitario;
	}

	public void setListaDependenciacargocomposicao(
			List<Dependenciacargocomposicao> listaDependenciacargocomposicao) {
		this.listaDependenciacargocomposicao = listaDependenciacargocomposicao;
	}
	
	public void setListaDependenciafaixacomposicao(
			List<Dependenciafaixacomposicao> listaDependenciafaixacomposicao) {
		this.listaDependenciafaixacomposicao = listaDependenciafaixacomposicao;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Recursocomposicao) {
			Recursocomposicao that = (Recursocomposicao) obj;
			return this.getCdrecursocomposicao().equals(that.getCdrecursocomposicao());
		}		
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if (cdrecursocomposicao != null) {
			return cdrecursocomposicao.hashCode();
		}
		return super.hashCode();
	}
	
	
	/***
	 * Transientes
	 */
	protected Money valortotal;	
	protected Double quantidadeTotal;
	
	@DisplayName("Valor total")
	@Transient
	public Money getValortotal() {
		return valortotal;
	}
	
	@Transient
	public Double getQuantidadeTotal() {
		return quantidadeTotal;
	}
	
	public void setQuantidadeTotal(Double quantidadeTotal) {
		this.quantidadeTotal = quantidadeTotal;
	}
	
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}	
}
