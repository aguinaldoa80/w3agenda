package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxValue;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@DisplayName("Rela��o entre cargos")
@SequenceGenerator(name = "sq_relacaocargo", sequenceName = "sq_relacaocargo")
public class Relacaocargo {
	
	protected Integer cdrelacaocargo;
	protected Orcamento orcamento;
	protected Tiporelacaorecurso tiporelacaorecurso;
	protected Tipodependenciarecurso tipodependenciarecurso;
	protected Double quantidade;
	protected Cargo cargo;
	protected List<Dependenciacargo> listaDependenciacargo = new ArrayList<Dependenciacargo>();
	protected List<Dependenciafaixa> listaDependenciafaixa = new ArrayList<Dependenciafaixa>(); 
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_relacaocargo")	
	public Integer getCdrelacaocargo() {
		return cdrelacaocargo;
	}
	
	@DisplayName("Or�amento")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdorcamento")
	@Required	
	public Orcamento getOrcamento() {
		return orcamento;
	}
	
	@DisplayName("Tipo de rela��o entre recursos")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdtiporelacaorecurso")
	@Required
	public Tiporelacaorecurso getTiporelacaorecurso() {
		return tiporelacaorecurso;
	}
	
	@DisplayName("Tipo de depend�ncia entre recursos")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdtipodependenciarecurso")
	@Required
	public Tipodependenciarecurso getTipodependenciarecurso() {
		return tipodependenciarecurso;
	}
	
	@MinValue(0)
	@MaxValue(999999999)	
	public Double getQuantidade() {
		return quantidade;
	}
	
	@DisplayName("Cargo")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	@Required
	public Cargo getCargo() {
		return cargo;
	}
	
	@OneToMany(mappedBy="relacaocargo")
	@DisplayName("Rela��o de depend�ncia entre cargos")	
	public List<Dependenciacargo> getListaDependenciacargo() {
		return listaDependenciacargo;
	}
	
	@OneToMany(mappedBy="relacaocargo")
	@DisplayName("Rela��o de faixas de depend�ncia")	
	public List<Dependenciafaixa> getListaDependenciafaixa() {
		return listaDependenciafaixa;
	}
	
	public void setCdrelacaocargo(Integer cdrelacaocargo) {
		this.cdrelacaocargo = cdrelacaocargo;
	}
	
	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
	
	public void setTiporelacaorecurso(Tiporelacaorecurso tiporelacaorecurso) {
		this.tiporelacaorecurso = tiporelacaorecurso;
	}
	
	public void setTipodependenciarecurso(Tipodependenciarecurso tipodependenciarecurso) {
		this.tipodependenciarecurso = tipodependenciarecurso;
	}
	
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	public void setListaDependenciacargo(List<Dependenciacargo> listaDependenciacargo) {
		this.listaDependenciacargo = listaDependenciacargo;
	}
	
	public void setListaDependenciafaixa(List<Dependenciafaixa> listaDependenciafaixa) {
		this.listaDependenciafaixa = listaDependenciafaixa;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Relacaocargo) {
			Relacaocargo that = (Relacaocargo) obj;
			return this.getCdrelacaocargo().equals(that.getCdrelacaocargo());
		}		
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if (cdrelacaocargo != null) {
			return cdrelacaocargo.hashCode();
		}
		return super.hashCode();
	}
}
