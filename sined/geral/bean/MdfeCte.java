package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_mdfecte", sequenceName = "sq_mdfecte")
public class MdfeCte {

	protected Integer cdMdfeCte;
	protected Mdfe mdfe;
	protected String identificadorCte;
	protected String chaveAcesso;
	protected String codigoBarra;
	protected Boolean indicadorReentrega;
	protected Municipio municipio;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfecte")
	public Integer getCdMdfeCte() {
		return cdMdfeCte;
	}
	public void setCdMdfeCte(Integer cdMdfeCte) {
		this.cdMdfeCte = cdMdfeCte;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@DisplayName("ID do CTe")
	public String getIdentificadorCte() {
		return identificadorCte;
	}
	public void setIdentificadorCte(String identificadorCte) {
		this.identificadorCte = identificadorCte;
	}
	
	@Required
	@MaxLength(value=44)
	@DisplayName("Chave de acesso")
	public String getChaveAcesso() {
		return chaveAcesso;
	}
	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}
	
	@MaxLength(value=36)
	@DisplayName("C�digo de barras")
	public String getCodigoBarra() {
		return codigoBarra;
	}
	public void setCodigoBarra(String codigoBarra) {
		this.codigoBarra = codigoBarra;
	}
	
	@DisplayName("Indicador de reentrega")
	public Boolean getIndicadorReentrega() {
		return indicadorReentrega;
	}
	public void setIndicadorReentrega(Boolean indicadorReentrega) {
		this.indicadorReentrega = indicadorReentrega;
	}
	
	@Transient
	public List<MdfeInformacaoUnidadeTransporteCte> getListaInformacaoUnidadeTransporteCte(MdfeCte mdfeCte, Mdfe mdfe) {
		List<MdfeInformacaoUnidadeTransporteCte> list = new ArrayList<MdfeInformacaoUnidadeTransporteCte>();
		if(SinedUtil.isListNotEmpty(mdfe.getListaInformacaoUnidadeTransporteCte())){
			for(MdfeInformacaoUnidadeTransporteCte item : mdfe.getListaInformacaoUnidadeTransporteCte()){
				if(item.getIdCte().equals(mdfeCte.getIdentificadorCte())){
					list.add(item);
				}
			}
		}
		return list;
	}
	
	@Transient
	public List<MdfeProdutoPerigosoCte> getListaProdutoPerigosoCte(MdfeCte mdfeCte, Mdfe mdfe) {
		List<MdfeProdutoPerigosoCte> list = new ArrayList<MdfeProdutoPerigosoCte>();
		if(SinedUtil.isListNotEmpty(mdfe.getListaInformacaoUnidadeTransporteCte())){
			for(MdfeProdutoPerigosoCte item : mdfe.getListaProdutoPerigosoCte()){
				if(item.getIdCte().equals(mdfeCte.getIdentificadorCte())){
					list.add(item);
				}
			}
		}
		return list;
	}
	
	@DisplayName("Munic�pio de descarregamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
}
