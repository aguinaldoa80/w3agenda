package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_notafiscalprodutoautorizacao", sequenceName = "sq_notafiscalprodutoautorizacao")
public class Notafiscalprodutoautorizacao {

	protected Integer cdnotafiscalprodutoautorizacao;
	protected Notafiscalproduto notafiscalproduto;
	protected Tipopessoa tipopessoa;
	protected Cnpj cnpj;
	protected Cpf cpf;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_notafiscalprodutoautorizacao")
	public Integer getCdnotafiscalprodutoautorizacao() {
		return cdnotafiscalprodutoautorizacao;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnotafiscalproduto")
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}

	@Required
	@DisplayName("Tipo de pessoa")
	public Tipopessoa getTipopessoa() {
		return tipopessoa;
	}

	public Cnpj getCnpj() {
		return cnpj;
	}

	public Cpf getCpf() {
		return cpf;
	}

	public void setCdnotafiscalprodutoautorizacao(
			Integer cdnotafiscalprodutoautorizacao) {
		this.cdnotafiscalprodutoautorizacao = cdnotafiscalprodutoautorizacao;
	}

	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}

	public void setTipopessoa(Tipopessoa tipopessoa) {
		this.tipopessoa = tipopessoa;
	}

	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}

	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	
	
}