package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.types.Money;

@Entity
@SequenceGenerator(name="sq_vendaorcamentofornecedorticketmedio", sequenceName="sq_vendaorcamentofornecedorticketmedio")
public class VendaOrcamentoFornecedorTicketMedio {

	private Integer cdVendaOrcamentoFornecedorTicketMedio;
	private Vendaorcamento vendaorcamento;
	private Fornecedor fornecedor;
	private Money valorTicketEsperado;
	private Money valorTicketAtingido;
	
	
	public VendaOrcamentoFornecedorTicketMedio(){
		
	}
	
	public VendaOrcamentoFornecedorTicketMedio(Vendaorcamento vendaOrcamento, Fornecedor fornecedor, Money valorTicketEsperado, Money valorTicketAtingido){
		this.vendaorcamento = vendaOrcamento;
		this.fornecedor = fornecedor;
		this.valorTicketEsperado = valorTicketEsperado;
		this.valorTicketAtingido = valorTicketAtingido;
	}
	
	@Id
	@GeneratedValue(generator="sq_vendaorcamentofornecedorticketmedio",strategy=GenerationType.AUTO)
	public Integer getCdVendaOrcamentoFornecedorTicketMedio() {
		return cdVendaOrcamentoFornecedorTicketMedio;
	}
	public void setCdVendaOrcamentoFornecedorTicketMedio(Integer cdVendaOrcamentoFornecedorTicketMedio) {
		this.cdVendaOrcamentoFornecedorTicketMedio = cdVendaOrcamentoFornecedorTicketMedio;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendaorcamento")
	public Vendaorcamento getVendaorcamento() {
		return vendaorcamento;
	}
	public void setVendaorcamento(Vendaorcamento venda) {
		this.vendaorcamento = venda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public Money getValorTicketEsperado() {
		return valorTicketEsperado;
	}
	public void setValorTicketEsperado(Money valorTicketEsperado) {
		this.valorTicketEsperado = valorTicketEsperado;
	}
	public Money getValorTicketAtingido() {
		return valorTicketAtingido;
	}
	public void setValorTicketAtingido(Money valorTicketAtingido) {
		this.valorTicketAtingido = valorTicketAtingido;
	}
}
