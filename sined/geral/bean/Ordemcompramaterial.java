package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

import com.ibm.icu.text.SimpleDateFormat;

@Entity
@SequenceGenerator(name = "sq_ordemcompramaterial", sequenceName = "sq_ordemcompramaterial")
@DisplayName("Ordem de Compra de Material")
public class Ordemcompramaterial implements Log{
	
	protected Integer cdordemcompramaterial;
	protected Ordemcompra ordemcompra;
	protected Material material;
	protected Contagerencial contagerencial;
	protected Frequencia frequencia;
	protected Double qtdefrequencia;
	protected Double qtdpedida;
	protected Double valor;
	protected Double icmsiss;
	protected Double ipi;
	protected Boolean icmsissincluso;
	protected Boolean ipiincluso;
	protected Unidademedida unidademedida;
	protected Date dtentrega;
	protected Centrocusto centrocusto;
	protected Projeto projeto;
	protected Localarmazenagem localarmazenagem;
	protected Set<Ordemcompraentregamaterial> listaOrdemcompraentregamaterial = new ListSet<Ordemcompraentregamaterial>(Ordemcompraentregamaterial.class);
	protected Set<Solicitacaocompraordemcompramaterial> listaSolicitacaocompraordemcompramaterial = new ListSet<Solicitacaocompraordemcompramaterial>(Solicitacaocompraordemcompramaterial.class);
	protected String observacao;
	protected Money valoroutrasdespesas;
	protected Money valorfrete;
	protected Double altura;
	protected Double largura;
	protected Double comprimento;
	protected Double pesototal;
	protected Double qtdvolume;
	protected Pedidovenda pedidovenda;
	protected Vendaorcamento vendaorcamento;
	protected Planejamentorecursogeral planejamentorecursogeral;
	
	//Transient
	protected String identificadortela;
	protected String dtentregaString;
	protected Solicitacaocompra solicitacaocompra;
	protected Double resto;
	protected Integer qtdeminima;
	protected String Centrocustoprojeto;
	protected Money descontotrans;
	protected String whereInOrdemcompramaterialItemGrade;
	protected String whereInMaterialItenGrade;
	protected String nomematerialOrdemcompramaterial;
	protected Boolean origemMaterialmestregrade;
	protected List<Ordemcompramaterial> listaOrdemcompramaterialByMaterialmestregrade;
	protected Boolean gerado;
	protected Integer cdplanejamentorecursogeral;
	protected Planejamento planejamento;
	protected String unidademedida_simbolo = new String();
	protected String codigoalternativo;
	
	//LOG
	protected Integer cdUsuarioAltera;
	protected Timestamp dtAltera;
	
	
	public Ordemcompramaterial(){
	}
	
	public Ordemcompramaterial(Integer cdordemcompramaterial){
		this.cdordemcompramaterial = cdordemcompramaterial;
	}
	
	public Ordemcompramaterial(Material material, Double qtdpedida, Double valor, Boolean icmsissincluso, 
			Double icmsiss, Boolean ipiincluso, Double ipi, Frequencia frequencia, Double qtdefrequencia, Unidademedida unidademedida){
		this.qtdpedida = qtdpedida == null ? 0.0 : qtdpedida;
		this.material = material;
		this.valor = valor;
		this.icmsissincluso = icmsissincluso;
		this.icmsiss = icmsiss;
		this.ipiincluso = ipiincluso;
		this.ipi = ipi;
		this.frequencia = frequencia;
		this.qtdefrequencia = qtdefrequencia;
		this.unidademedida = unidademedida;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ordemcompramaterial")
	@DisplayName("Compra")
	public Integer getCdordemcompramaterial() {
		return cdordemcompramaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemcompra")
	public Ordemcompra getOrdemcompra() {
		return ordemcompra;
	}
	@DisplayName("Material/Servi�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@DisplayName("Qtde. pedida")
	public Double getQtdpedida() {
		return qtdpedida;
	}
	@DisplayName("Valor Total")
	public Double getValor() {
		return valor;
	}
	@DisplayName("Incluso")
	public Boolean getIcmsissincluso() {
		return icmsissincluso;
	}
	@DisplayName("Incluso")
	public Boolean getIpiincluso() {
		return ipiincluso;
	}
	@DisplayName("ICMS/ISS")
	public Double getIcmsiss() {
		return icmsiss;
	}
	public Double getIpi() {
		return ipi;
	}
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfrequencia")
	public Frequencia getFrequencia() {
		return frequencia;
	}

	@Required
	public Double getQtdefrequencia() {
		return qtdefrequencia;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	@DisplayName("Unidade de medida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	@DisplayName("Local")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	
	@OneToMany(mappedBy="ordemcompramaterial")
	public Set<Ordemcompraentregamaterial> getListaOrdemcompraentregamaterial() {
		return listaOrdemcompraentregamaterial;
	}
	
	@OneToMany(mappedBy="ordemcompramaterial")
	public Set<Solicitacaocompraordemcompramaterial> getListaSolicitacaocompraordemcompramaterial() {
		return listaSolicitacaocompraordemcompramaterial;
	}

	public Double getAltura() {
		return altura;
	}

	public Double getLargura() {
		return largura;
	}

	public Double getComprimento() {
		return comprimento;
	}

	@DisplayName("Peso total")
	public Double getPesototal() {
		return pesototal;
	}
	
	@DisplayName("Qtde. Volume(s)")
	public Double getQtdvolume() {
		return qtdvolume;
	}
	
	public void setQtdvolume(Double qtdvolume) {
		this.qtdvolume = qtdvolume;
	}

	public void setAltura(Double altura) {
		this.altura = altura;
	}

	public void setLargura(Double largura) {
		this.largura = largura;
	}

	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}

	public void setPesototal(Double pesototal) {
		this.pesototal = pesototal;
	}
	
	public void setListaOrdemcompraentregamaterial(
			Set<Ordemcompraentregamaterial> listaOrdemcompraentregamaterial) {
		this.listaOrdemcompraentregamaterial = listaOrdemcompraentregamaterial;
	}
	public void setListaSolicitacaocompraordemcompramaterial(Set<Solicitacaocompraordemcompramaterial> listaSolicitacaocompraordemcompramaterial) {
		this.listaSolicitacaocompraordemcompramaterial = listaSolicitacaocompraordemcompramaterial;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	
	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}

	public void setQtdefrequencia(Double qtdefrequencia) {
		this.qtdefrequencia = qtdefrequencia;
	}

	public void setOrdemcompra(Ordemcompra ordemcompra) {
		this.ordemcompra = ordemcompra;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setQtdpedida(Double qtdpedida) {
		this.qtdpedida = qtdpedida;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setIcmsissincluso(Boolean icmsissincluso) {
		this.icmsissincluso = icmsissincluso;
	}
	public void setIpiincluso(Boolean ipiincluso) {
		this.ipiincluso = ipiincluso;
	}
	public void setIcmsiss(Double icmsiss) {
		this.icmsiss = icmsiss;
	}
	public void setIpi(Double ipi) {
		this.ipi = ipi;
	}
	public void setCdordemcompramaterial(Integer cdordemcompramaterial) {
		this.cdordemcompramaterial = cdordemcompramaterial;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	
	//============ TRANSIENT'S ==================
	
	protected Double qtdrestante;
	protected Double valorunitario;
	protected Frequencia frequenciatrans;
	protected Boolean verificarQtderestante = Boolean.FALSE;
	protected Double percentualicmsiss;
	protected Double percentualipi;
	protected Integer indexlista;
	protected String cdordemcompraParam;

	@Transient
	@DisplayName("Qtd. restante")
	public Double getQtdrestante() {
		return qtdrestante;
	}
	@Transient
	@DisplayName("Valor Unit.")
	public Double getValorunitario() {
		return valorunitario;
	}
	
	@Transient	
	public Frequencia getFrequenciatrans() {
		return getFrequencia();
	}
	
	@Transient
	@DisplayName("Frequ�ncia")
	public String getFrequenciaStr(){
		String string = "";
		Integer cdfrequencia = getFrequencia().getCdfrequencia();
		
		switch (cdfrequencia) {
		case 1: //UNICA
			string = "�nica";
			break;
		case 2: //DI�RIA
			string = getQtdefrequencia() + " dia(s)";
			break;
		case 3: //SEMANAL
			string = getQtdefrequencia() + " semana(s)";
			break;
		case 4: //QUINZENAL
			string = getQtdefrequencia() + " quinzena(s)";
			break;
		case 5: //MENSAL
			string = getQtdefrequencia() + " m�s(es)";
			break;
		case 6: //ANUAL
			string = getQtdefrequencia() + " ano(s)";
			break;
		case 7: //SEMESTRAL
			string = getQtdefrequencia() + " semestre(s)";
			break;
		case 8: //HORA
			string = getQtdefrequencia() + " hora(s)";
			break;
		case 9: //TRIMESTRE
			string = getQtdefrequencia() + " trimestre(s)";
			break;

		default:
			break;
		}
		return string;
	}
	
	@Transient
	public Boolean getVerificarQtderestante() {
		return verificarQtderestante;
	}
	
	@Transient
	public Double getPercentualicmsiss() {
		return percentualicmsiss;
	}

	@Transient
	public Double getPercentualipi() {
		return percentualipi;
	}
	
	@Transient
	public Integer getIndexlista() {
		return indexlista;
	}
	
	@Transient
	public String getCdordemcompraParam() {
		return cdordemcompraParam;
	}
	
	public void setCdordemcompraParam(String cdordemcompraParam) {
		this.cdordemcompraParam = cdordemcompraParam;
	}

	public void setIndexlista(Integer indexlista) {
		this.indexlista = indexlista;
	}

	public void setPercentualicmsiss(Double percentualicmsiss) {
		this.percentualicmsiss = percentualicmsiss;
	}

	public void setPercentualipi(Double percentualipi) {
		this.percentualipi = percentualipi;
	}

	public void setVerificarQtderestante(Boolean verificarQtderestante) {
		this.verificarQtderestante = verificarQtderestante;
	}

	public void setFrequenciatrans(Frequencia frequenciatrans) {
		if(frequenciatrans != null){
			setFrequencia(frequenciatrans);
		}
	}

	public void setQtdrestante(Double qtdrestante) {
		this.qtdrestante = qtdrestante;
	}
	
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
	@Required
	@DisplayName("Prazo de Entrega")
	public Date getDtentrega() {
		return dtentrega;
	}

	public void setDtentrega(Date dtEntrega) {
		this.dtentrega = dtEntrega;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}

	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	@Transient
	public String getDtentregaString() {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		if(dtentrega != null)
		dtentregaString = format.format(dtentrega);
		return dtentregaString;
	}
	public void setDtentregaString(String dtentregaString) {
		this.dtentregaString = dtentregaString;
	}
	@Transient
	@DisplayName("Qtde M�nima")
	public Integer getQtdeminima() {
		return qtdeminima;
	}
	public void setQtdeminima(Integer qtdeminima) {
		this.qtdeminima = qtdeminima;
	}

	@DisplayName("Observa��o")
	@MaxLength(500)
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@DisplayName("Valor outras desp.")
	@MaxLength(15)
	public Money getValoroutrasdespesas() {
		return valoroutrasdespesas;
	}
	public void setValoroutrasdespesas(Money valoroutrasdespesas) {
		this.valoroutrasdespesas = valoroutrasdespesas;
	}
	
	@DisplayName("Valor frete")
	@MaxLength(15)
	public Money getValorfrete() {
		return valorfrete;
	}
	public void setValorfrete(Money valorfrete) {
		this.valorfrete = valorfrete;
	}
	
	@Transient
	public String getCentrocustoprojeto() {
		if(this.Centrocustoprojeto == null){
			this.Centrocustoprojeto = "";
			
			if(this.centrocusto != null && this.centrocusto.getNome() != null)
				this.Centrocustoprojeto = this.centrocusto.getNome() + "<br>";
			
			if(this.projeto != null && this.projeto.getNome() != null){
				if(this.Centrocustoprojeto.isEmpty())
					this.Centrocustoprojeto = "<br>";
				else
					this.Centrocustoprojeto += this.projeto.getNome();
			}
		}
		
		return Centrocustoprojeto;
	}

	public void setCentrocustoprojeto(String centrocustoprojeto) {
		Centrocustoprojeto = centrocustoprojeto;
	}
	
	@Transient
	public Money getDescontotrans() {
		return descontotrans;
	}
	public void setDescontotrans(Money descontotrans) {
		this.descontotrans = descontotrans;
	}
	@Transient
	public String getIdentificadortela() {
		return identificadortela;
	}
	public void setIdentificadortela(String identificadortela) {
		this.identificadortela = identificadortela;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendaorcamento")
	public Vendaorcamento getVendaorcamento() {
		return vendaorcamento;
	}

	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}

	public void setVendaorcamento(Vendaorcamento vendaorcamento) {
		this.vendaorcamento = vendaorcamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdplanejamentorecursogeral")
	public Planejamentorecursogeral getPlanejamentorecursogeral() {
		return planejamentorecursogeral;
	}

	public void setPlanejamentorecursogeral(Planejamentorecursogeral planejamentorecursogeral) {
		this.planejamentorecursogeral = planejamentorecursogeral;
	}

	@Transient
	public String getWhereInOrdemcompramaterialItemGrade() {
		return whereInOrdemcompramaterialItemGrade;
	}
	@Transient
	public String getWhereInMaterialItenGrade() {
		return whereInMaterialItenGrade;
	}
	@Transient
	public String getNomematerialOrdemcompramaterial() {
		return nomematerialOrdemcompramaterial;
	}
	@Transient
	public Boolean getOrigemMaterialmestregrade() {
		return origemMaterialmestregrade;
	}
	@Transient
	public List<Ordemcompramaterial> getListaOrdemcompramaterialByMaterialmestregrade() {
		return listaOrdemcompramaterialByMaterialmestregrade;
	}

	public void setOrigemMaterialmestregrade(Boolean origemMaterialmestregrade) {
		this.origemMaterialmestregrade = origemMaterialmestregrade;
	}
	public void setWhereInMaterialItenGrade(String whereInMaterialItenGrade) {
		this.whereInMaterialItenGrade = whereInMaterialItenGrade;
	}
	public void setWhereInOrdemcompramaterialItemGrade(String whereInOrdemcompramaterialItemGrade) {
		this.whereInOrdemcompramaterialItemGrade = whereInOrdemcompramaterialItemGrade;
	}
	public void setNomematerialOrdemcompramaterial(String nomematerialOrdemcompramaterial) {
		this.nomematerialOrdemcompramaterial = nomematerialOrdemcompramaterial;
	}
	public void setListaOrdemcompramaterialByMaterialmestregrade(List<Ordemcompramaterial> listaOrdemcompramaterialByMaterialmestregrade) {
		this.listaOrdemcompramaterialByMaterialmestregrade = listaOrdemcompramaterialByMaterialmestregrade;
	}
	
	@Transient
	public Boolean getGerado() {
		return gerado;
	}
	@Transient
	public Integer getCdplanejamentorecursogeral() {
		return cdplanejamentorecursogeral;
	}
	@Transient
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	
	public void setGerado(Boolean gerado) {
		this.gerado = gerado;
	}
	public void setCdplanejamentorecursogeral(Integer cdplanejamentorecursogeral) {
		this.cdplanejamentorecursogeral = cdplanejamentorecursogeral;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}

	@Transient
	public Double getResto() {
		return resto;
	}
	public void setResto(Double resto) {
		this.resto = resto;
	}

	@Transient
	public Solicitacaocompra getSolicitacaocompra() {
		return solicitacaocompra;
	}

	public void setSolicitacaocompra(Solicitacaocompra solicitacaocompra) {
		this.solicitacaocompra = solicitacaocompra;
	}

	@Transient
	public String getUnidademedida_simbolo() {
		return unidademedida_simbolo;
	}

	public void setUnidademedida_simbolo(String unidademedida_simbolo) {
		this.unidademedida_simbolo = unidademedida_simbolo;
	}
	
	@Transient
	public String getCodigoalternativo() {
		return codigoalternativo;
	}

	public void setCodigoalternativo(Object codigoalternativo) {
		this.codigoalternativo = (String) codigoalternativo;
	}
	
	public void setCodigoalternativo(String codigoalternativo) {
		this.codigoalternativo = (String) codigoalternativo;
	}

	
}
