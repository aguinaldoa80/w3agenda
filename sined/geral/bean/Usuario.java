package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.types.Password;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MinLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
public class Usuario extends Pessoa implements User,Log {
	
	protected String login;
	protected String senha;
	protected Boolean bloqueado;
	protected Boolean trocasenha = Boolean.FALSE;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected List<Usuariopapel> listaUsuariopapel = new ArrayList<Usuariopapel>();
	protected List<Usuarioempresa> listaUsuarioempresa = new ArrayList<Usuarioempresa>();
	protected List<Usuarioprojeto> listaUsuarioprojeto = new ListSet<Usuarioprojeto>(Usuarioprojeto.class);
	protected List<Usuariodepartamento> listaUsuariodepartamento = new ArrayList<Usuariodepartamento>();
	protected List<Restricaoacessopessoa> listaRestricaoacessopessoa = new ArrayList<Restricaoacessopessoa>();
	protected String apelido;
	protected Arquivo rubrica;
	protected Arquivo imagemassinaturaemail;
	protected Boolean todosprojetos = true;
	protected Boolean todosdepartamentos = true;
	protected Money limitevaloraprovacaoautorizacao;
	protected Double limitepercentualdesconto;
	protected Boolean restricaoclientevendedor;
	protected Boolean restricaovendavendedor;
	protected Boolean restricaodespesaviagem;
	protected Boolean semprelogaroffline;
	protected Boolean restricaoFornecedorColaborador;
	protected Boolean restricaoempresacliente;
	protected Boolean restricaoempresafornecedor;
	protected Boolean restricaofornecedorproduto;
	protected Boolean salvarAbaixoMinimoVenda;
	protected Boolean salvarAbaixoMinimoPedido;
	protected Boolean salvarAbaixoMinimoOrcamento;
	protected String senhaterminal;
	protected Boolean restricaoaprovarpedidoclientedevedor;
	protected Boolean visualizaroutrasoportunidades;
	protected Double limitePercentualMarkupVenda;
	
	//Transiente
	protected List<Papel> listaPapel = new ArrayList<Papel>();
	protected List<Empresa> listaEmpresa = new ArrayList<Empresa>();
	protected String confirmasenha;
	protected String niveis;
	
	/*
	 * guarda o cd do usu�rio quando ele � um colaborador.
	 */
	protected Integer cdpessoaaux;
	
	protected String validaaux;
	
	public static Usuario ADMINISTRADOR = new Usuario(1);
	
	/*
	 * Construtores:
	 */
	public Usuario() {
		
	}
	
	public Usuario(Integer cdpessoa) {
		super(cdpessoa);
	}
	
	public Usuario(Integer cdpessoa, String nome, Boolean bloqueado, String login, String niveis) {
		super(cdpessoa, nome);
		this.bloqueado = bloqueado;
		this.login = login;
		this.niveis = niveis;
	}
	
	public Usuario(Integer cdpessoa, String nome, Boolean bloqueado, String login, String niveis, String email) {
		super(cdpessoa, nome, email);
		this.bloqueado = bloqueado;
		this.login = login;
		this.niveis = niveis;
	}
	
	public Boolean getBloqueado() {
		return bloqueado;
	}
	
	@Transient
	public Integer getCdpessoaaux() {
		return cdpessoaaux;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@Transient
	@Password
	@Required
	@MinLength(4)
	@DisplayName("Confirmar senha")
	public String getConfirmasenha() {
		return confirmasenha;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@Transient
	@DisplayName("N�vel")
	public List<Papel> getListaPapel() {
		return listaPapel;
	}
	
	@Transient
	@DisplayName("Empresa")
	public List<Empresa> getListaEmpresa() {
		return listaEmpresa;
	}

	@OneToMany(mappedBy="pessoa")
	public List<Usuariopapel> getListaUsuariopapel() {
		return listaUsuariopapel;
	}
	
	@OneToMany(mappedBy="usuario")
	public List<Usuarioempresa> getListaUsuarioempresa() {
		return listaUsuarioempresa;
	}
	
	@DisplayName("Projetos")
	@OneToMany(mappedBy="usuario")
	public List<Usuarioprojeto> getListaUsuarioprojeto() {
		return listaUsuarioprojeto;
	}
	
	@DisplayName("Departamentos")
	@OneToMany(mappedBy="usuario")
	public List<Usuariodepartamento> getListaUsuariodepartamento() {
		return listaUsuariodepartamento;
	}
	
	@Transient
	@DisplayName("Nome")
	public String getNomeaux(){
		if(nome.length()>20)
			return nome.substring(0,20);
		else return nome;
	}
	
	/* API - INICIO*/
	@Required
	@MaxLength(50)
	public String getLogin() {
		return login;
	}
	
	@Transient
	public String getPassword() {
		return senha;
	}
	
	@Required
	@MinLength(4)
	@Password
	public String getSenha() {
		return senha;
	}

	public Boolean getTrocasenha() {
		return trocasenha;
	}
	
	@Transient
	public String getValidaaux() {
		return validaaux;
	}
	
	@Transient
	@DisplayName("N�veis")
	public String getNiveis() {
		return niveis;
	}
	@MaxLength(50)
	@Required
	@DisplayName("Gostaria ser chamado de")
	public String getApelido() {
		return apelido;
	}
	
	@DisplayName("Rubrica")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getRubrica() {
		return rubrica;
	}
	
	@DisplayName("Imagem de assinatura p/ Email")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoassinaturaemail")
	public Arquivo getImagemassinaturaemail() {
		return imagemassinaturaemail;
	}

	@DisplayName("Todos projetos")
	public Boolean getTodosprojetos() {
		return todosprojetos;
	}
	
	@DisplayName("Todos departamentos (Solicita��o de compra)")
	public Boolean getTodosdepartamentos() {
		return todosdepartamentos;
	}
	@DisplayName("Limite valor Aprova��o/Autoriza��o")
	public Money getLimitevaloraprovacaoautorizacao() {
		return limitevaloraprovacaoautorizacao;
	}
	@DisplayName("Restri��o Cliente/Vendedor")
	public Boolean getRestricaoclientevendedor() {
		return restricaoclientevendedor;
	}
	
	@DisplayName("Limite percentual desconto")
	public Double getLimitepercentualdesconto() {
		return limitepercentualdesconto;
	}
	
	public void setLimitepercentualdesconto(Double limitepercentualdesconto) {
		this.limitepercentualdesconto = limitepercentualdesconto;
	}
	
	public void setTodosdepartamentos(Boolean todosdepartamentos) {
		this.todosdepartamentos = todosdepartamentos;
	}

	public void setTodosprojetos(Boolean todosprojetos) {
		this.todosprojetos = todosprojetos;
	}

	public void setApelido(String apelido) {
		this.apelido = StringUtils.trimToNull(apelido);
	}
	
	public void setBloqueado(Boolean bloqueado) {
		this.bloqueado = bloqueado;
	}
	
	public void setCdpessoaaux(Integer cdpessoaaux) {
		this.cdpessoaaux = cdpessoaaux;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setConfirmasenha(String confirmasenha) {
		this.confirmasenha = confirmasenha;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setListaUsuariodepartamento(
			List<Usuariodepartamento> listaUsuariodepartamento) {
		this.listaUsuariodepartamento = listaUsuariodepartamento;
	}

	public void setListaPapel(List<Papel> listaPapel) {
		this.listaPapel = listaPapel;
	}
	
	public void setListaEmpresa(List<Empresa> listaEmpresa) {
		this.listaEmpresa = listaEmpresa;
	}

	public void setListaUsuariopapel(List<Usuariopapel> listaUsuariopapel) {
		this.listaUsuariopapel = listaUsuariopapel;
	}
	
	public void setListaUsuarioempresa(List<Usuarioempresa> listaUsuarioempresa) {
		this.listaUsuarioempresa = listaUsuarioempresa;
	}
	
	public void setListaUsuarioprojeto(List<Usuarioprojeto> listaUsuarioprojeto) {
		this.listaUsuarioprojeto = listaUsuarioprojeto;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	public void setLogin(String login) {
		this.login = StringUtils.trimToNull(login);
	}
	public void setTrocasenha(Boolean trocasenha) {
		this.trocasenha = trocasenha;
	}

	public void setValidaaux(String validaaux) {
		this.validaaux = validaaux;
	}

	public void setNiveis(String niveis) {
		this.niveis = StringUtils.trimToNull(niveis);
	}
	
	public void setRubrica(Arquivo rubrica) {
		this.rubrica = rubrica;
	}
	
	public void setImagemassinaturaemail(Arquivo imagemassinaturaemail) {
		this.imagemassinaturaemail = imagemassinaturaemail;
	}
	
	public void setLimitevaloraprovacaoautorizacao(
			Money limitevaloraprovacaoautorizacao) {
		this.limitevaloraprovacaoautorizacao = limitevaloraprovacaoautorizacao;
	}

	public void setRestricaoclientevendedor(Boolean restricaoclientevendedor) {
		this.restricaoclientevendedor = restricaoclientevendedor;
	}

	@DisplayName("Restri��o Venda/Vendedor")
	public Boolean getRestricaovendavendedor() {
		return restricaovendavendedor;
	}

	public void setRestricaovendavendedor(Boolean restricaovendavendedor) {
		this.restricaovendavendedor = restricaovendavendedor;
	}

	@DisplayName("Restringir Despesa de Viagem")
	public Boolean getRestricaodespesaviagem() {
		return restricaodespesaviagem;
	}
	
	public void setRestricaodespesaviagem(Boolean restricaodespesaviagem) {
		this.restricaodespesaviagem = restricaodespesaviagem;
	}
	
	@DisplayName("Restri��o Acesso")
	@OneToMany(mappedBy="pessoa")
	public List<Restricaoacessopessoa> getListaRestricaoacessopessoa() {
		return listaRestricaoacessopessoa;
	}
	
	public void setListaRestricaoacessopessoa(List<Restricaoacessopessoa> listaRestricaoacessopessoa) {
		this.listaRestricaoacessopessoa = listaRestricaoacessopessoa;
	}

	@DisplayName("Sempre logar offline")
	public Boolean getSemprelogaroffline() {
		return semprelogaroffline;
	}

	public void setSemprelogaroffline(Boolean semprelogaroffline) {
		this.semprelogaroffline = semprelogaroffline;
	}
	
	@Transient
	public Boolean isLoginMathcerValidate(){
		Pattern pattern = Pattern.compile("^[a-zA-Z0-9.\\-\\_]+$");
		Matcher matcher = pattern.matcher(this.login);
		if (matcher.find()){
		  return true; 
		} 
		return false; 
	}
	/* API - FIM*/

	@DisplayName("Restri��o Fornecedor/Colaborador")
	public Boolean getRestricaoFornecedorColaborador() {
		return restricaoFornecedorColaborador;
	}

	public void setRestricaoFornecedorColaborador(
			Boolean restricaoFornecedorColaborador) {
		this.restricaoFornecedorColaborador = restricaoFornecedorColaborador;
	}

	@DisplayName("Salvar abaixo do m�nimo na venda")
	public Boolean getSalvarAbaixoMinimoVenda() {
		return salvarAbaixoMinimoVenda;
	}
	
	@DisplayName("Salvar abaixo do m�nimo no pedido")
	public Boolean getSalvarAbaixoMinimoPedido() {
		return salvarAbaixoMinimoPedido;
	}
	
	@DisplayName("Salvar abaixo do m�nimo no orcamento")
	public Boolean getSalvarAbaixoMinimoOrcamento() {
		return salvarAbaixoMinimoOrcamento;
	}

	public void setSalvarAbaixoMinimoVenda(Boolean salvarAbaixoMinimoVenda) {
		this.salvarAbaixoMinimoVenda = salvarAbaixoMinimoVenda;
	}

	public void setSalvarAbaixoMinimoPedido(Boolean salvarAbaixoMinimoPedido) {
		this.salvarAbaixoMinimoPedido = salvarAbaixoMinimoPedido;
	}

	public void setSalvarAbaixoMinimoOrcamento(Boolean salvarAbaixoMinimoOrcamento) {
		this.salvarAbaixoMinimoOrcamento = salvarAbaixoMinimoOrcamento;
	}
	
	@MaxLength(4)
	@DisplayName("C�digo de identifica��o do usu�rio (Senha de terminal)")
	public String getSenhaterminal() {
		return senhaterminal;
	}

	public void setSenhaterminal(String senhaterminal) {
		this.senhaterminal = senhaterminal;
	}

	@DisplayName("Restri��o aprovar pedido (Cliente devedor) ")
	public Boolean getRestricaoaprovarpedidoclientedevedor() {
		return restricaoaprovarpedidoclientedevedor;
	}

	public void setRestricaoaprovarpedidoclientedevedor(Boolean restricaoaprovarpedidoclientedevedor) {
		this.restricaoaprovarpedidoclientedevedor = restricaoaprovarpedidoclientedevedor;
	}
	
	@DisplayName("Restri��o Cliente/Empresa")
	public Boolean getRestricaoempresacliente() {
		return restricaoempresacliente;
	}

	public void setRestricaoempresacliente(Boolean restricaoempresacliente) {
		this.restricaoempresacliente = restricaoempresacliente;
	}

	@DisplayName("Restri��o Empresa/Fornecedor")
	public Boolean getRestricaoempresafornecedor() {
		return restricaoempresafornecedor;
	}

	public void setRestricaoempresafornecedor(Boolean restricaoempresafornecedor) {
		this.restricaoempresafornecedor = restricaoempresafornecedor;
	}
	
	@DisplayName("Restri��o Fornecedor/Produto")
	public Boolean getRestricaofornecedorproduto() {
		return restricaofornecedorproduto;
	}

	public void setRestricaofornecedorproduto(Boolean restricaofornecedorproduto) {
		this.restricaofornecedorproduto = restricaofornecedorproduto;
	}
	
	@DisplayName("Visualizar outras oportunidades")
	public Boolean getVisualizaroutrasoportunidades() {
		return visualizaroutrasoportunidades;
	}
	
	public void setVisualizaroutrasoportunidades(
			Boolean visualizaroutrasoportunidades) {
		this.visualizaroutrasoportunidades = visualizaroutrasoportunidades;
	}
	
	@DisplayName("Limite Percentual Markup de Venda")
	public Double getLimitePercentualMarkupVenda() {
		return limitePercentualMarkupVenda;
	}
	
	public void setLimitePercentualMarkupVenda(Double limitePercentualMarkupVenda) {
		this.limitePercentualMarkupVenda = limitePercentualMarkupVenda;
	}
}