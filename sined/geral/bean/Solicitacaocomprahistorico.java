package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_solicitacaocomprahistorico", sequenceName = "sq_solicitacaocomprahistorico")
@DisplayName("Solicitação Material")
public class Solicitacaocomprahistorico implements Log{

	protected Integer cdsolicitacaocomprahistorico;
	protected Solicitacaocompra solicitacaocompra;
	protected Solicitacaocompraacao solicitacaocompraacao;
	protected String observacao;
	protected Integer cdUsuarioAltera;
	protected Timestamp dtAltera;
	
	//========== TRANSIENT'S =========
	protected String ids;
	
	public Solicitacaocomprahistorico(){
	}

	public Solicitacaocomprahistorico(Solicitacaocompra solicitacaocompra, Solicitacaocompraacao solicitacaocompraacao){
		this.solicitacaocompra = solicitacaocompra;
		this.solicitacaocompraacao = solicitacaocompraacao;
		this.dtAltera = new Timestamp(System.currentTimeMillis());
		this.cdUsuarioAltera = ((Pessoa)Neo.getUser()).getCdpessoa();
	}
	
	public Solicitacaocomprahistorico(Solicitacaocompra solicitacaocompra, Solicitacaocompraacao solicitacaocompraacao, String observacao){
		this.solicitacaocompra = solicitacaocompra;
		this.solicitacaocompraacao = solicitacaocompraacao;
		this.observacao = observacao;
		this.dtAltera = new Timestamp(System.currentTimeMillis());
		this.cdUsuarioAltera = ((Pessoa)Neo.getUser()).getCdpessoa();
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_solicitacaocomprahistorico")
	public Integer getCdsolicitacaocomprahistorico() {
		return cdsolicitacaocomprahistorico;
	}
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsolicitacaocompra")
	public Solicitacaocompra getSolicitacaocompra() {
		return solicitacaocompra;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsolicitacaocompraacao")
	public Solicitacaocompraacao getSolicitacaocompraacao() {
		return solicitacaocompraacao;
	}
	@MaxLength(1000)
	public String getObservacao() {
		return observacao;
	}
	
	public void setSolicitacaocompra(Solicitacaocompra solicitacaocompra) {
		this.solicitacaocompra = solicitacaocompra;
	}
	public void setSolicitacaocompraacao(Solicitacaocompraacao solicitacaocompraacao) {
		this.solicitacaocompraacao = solicitacaocompraacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	public void setCdsolicitacaocomprahistorico(
			Integer cdsolicitacaocomprahistorico) {
		this.cdsolicitacaocomprahistorico = cdsolicitacaocomprahistorico;
	}

	@Transient
	@DisplayName("Responsável")
	public String getResponsavel() {
		if(cdUsuarioAltera != null)
			return TagFunctions.findUserByCd(cdUsuarioAltera);
		return null;
	}
	@Transient
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	
}
