package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_protocolo",sequenceName="sq_protocolo")
public class Protocolo implements Log{
	
	//Tabela Protocolo
	protected Integer cdprotocolo;
	protected Usuario remetente;
	protected Usuario destinatario;
	protected String assunto;
	protected Timestamp dtrecebimento;
	protected Timestamp dtenvio;
	//Tabela protocoloDocProcesso
	protected List<Protocolodocprocesso> listadocumento;
	//Tabela protocoloprovidencia
	protected List<Protocoloprovidencia> listaprovidencia;	
	//Transiente
	protected List<Providencia> listaProvidenciaTransient;
	
	//Log
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@DisplayName("Id")
	@Id
	@GeneratedValue(generator="sq_protocolo",strategy=GenerationType.AUTO)
	public Integer getCdprotocolo() {
		return cdprotocolo;
	}
	
	@Required	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdremetente")
	public Usuario getRemetente() {
		return remetente;
	}
	
	@Required
	@DisplayName("Destinatário")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddestinatario")
	public Usuario getDestinatario() {
		return destinatario;
	}
	
	@DescriptionProperty
	@MaxLength(100)
	@Required
	public String getAssunto() {
		return assunto;
	}
	
	@DisplayName("Lido em")
	public Timestamp getDtrecebimento() {
		return dtrecebimento;
	}
	
	@DisplayName("Enviado em")
	public Timestamp getDtenvio() {
		return dtenvio;
	}
	
	@DisplayName("Documentos")
	@OneToMany(mappedBy="protocolo")
	public List<Protocolodocprocesso> getListadocumento() {
		return listadocumento;
	}
	
	@DisplayName("Providências")
	@OneToMany(mappedBy="protocolo")
	public List<Protocoloprovidencia> getListaprovidencia() {
		return listaprovidencia;
	}
	
	@Transient
	@DisplayName("Providências")
	public List<Providencia> getListaProvidenciaTransient() {
		return listaProvidenciaTransient;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdprotocolo(Integer cdprotocolo) {
		this.cdprotocolo = cdprotocolo;
	}
	
	public void setRemetente(Usuario remetente) {
		this.remetente = remetente;
	}
	
	public void setDestinatario(Usuario destinatario) {
		this.destinatario = destinatario;
	}
	
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	
	public void setDtrecebimento(Timestamp dtrecebimento) {
		this.dtrecebimento = dtrecebimento;
	}
	
	public void setDtenvio(Timestamp dtenvio) {
		this.dtenvio = dtenvio;
	}
	
	public void setListadocumento(List<Protocolodocprocesso> listadocumento) {
		this.listadocumento = listadocumento;
	}
	
	public void setListaprovidencia(List<Protocoloprovidencia> listaprovidencia) {
		this.listaprovidencia = listaprovidencia;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setListaProvidenciaTransient(
			List<Providencia> listaProvidenciaTransient) {
		this.listaProvidenciaTransient = listaProvidenciaTransient;
	}

	
}
