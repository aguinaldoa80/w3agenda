package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tabelaimpostoitemtipo;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_tabelaimposto", sequenceName = "sq_tabelaimposto")
public class Tabelaimposto implements Log {

	protected Integer cdtabelaimposto;
	protected Empresa empresa;
	protected Arquivo arquivo;
	protected String chave;
	protected String versao;
	protected Date dtimportacao;
	protected Date dtvigenciainicio;
	protected Date dtvigenciafim;
	protected List<Tabelaimpostoitem> listaTabelaimpostoitem = new ListSet<Tabelaimpostoitem>(Tabelaimpostoitem.class);
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	// TRANSIENTE
	protected String identificadorSessao;

	@Id
	@GeneratedValue(generator = "sq_tabelaimposto", strategy = GenerationType.AUTO)
	public Integer getCdtabelaimposto() {
		return cdtabelaimposto;
	}
	
	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}

	@Required
	@MaxLength(100)
	public String getChave() {
		return chave;
	}

	@Required
	@MaxLength(20)
	@DisplayName("Vers�o")
	public String getVersao() {
		return versao;
	}

	@Required
	@DisplayName("Data de importa��o")
	public Date getDtimportacao() {
		return dtimportacao;
	}

	@Required
	@DisplayName("Vig�ncia in�cio")
	public Date getDtvigenciainicio() {
		return dtvigenciainicio;
	}

	@Required
	@DisplayName("Vig�ncia fim")
	public Date getDtvigenciafim() {
		return dtvigenciafim;
	}
	
	@OneToMany(mappedBy="tabelaimposto")
	public List<Tabelaimpostoitem> getListaTabelaimpostoitem() {
		return listaTabelaimpostoitem;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setListaTabelaimpostoitem(
			List<Tabelaimpostoitem> listaTabelaimpostoitem) {
		this.listaTabelaimpostoitem = listaTabelaimpostoitem;
	}

	public void setCdtabelaimposto(Integer cdtabelaimposto) {
		this.cdtabelaimposto = cdtabelaimposto;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public void setVersao(String versao) {
		this.versao = versao;
	}

	public void setDtimportacao(Date dtimportacao) {
		this.dtimportacao = dtimportacao;
	}

	public void setDtvigenciainicio(Date dtvigenciainicio) {
		this.dtvigenciainicio = dtvigenciainicio;
	}

	public void setDtvigenciafim(Date dtvigenciafim) {
		this.dtvigenciafim = dtvigenciafim;
	}
	
	@Transient
	@DescriptionProperty(usingFields="arquivo")
	public String getDescriptionCombo(){
		if(this.arquivo != null) return this.arquivo.getNome();
		return this.cdtabelaimposto + "";
	}
	
	@Transient
	public List<Tabelaimpostoitem> getListaTabelaimpostoitemProduto() {
		List<Tabelaimpostoitem> listaTabelaimpostoitemProduto = new ArrayList<Tabelaimpostoitem>();
		if(listaTabelaimpostoitem != null && listaTabelaimpostoitem.size() > 0){
			for (Tabelaimpostoitem tabelaimpostoitem : listaTabelaimpostoitem) {
				if(tabelaimpostoitem.getTipo() != null && tabelaimpostoitem.getTipo().equals(Tabelaimpostoitemtipo.PRODUTO)){
					listaTabelaimpostoitemProduto.add(tabelaimpostoitem);
				}
			}
		}
		return listaTabelaimpostoitemProduto;
	}
	
	public void setListaTabelaimpostoitemProduto(List<Tabelaimpostoitem> listaTabelaimpostoitemProduto){
		if(listaTabelaimpostoitem == null){
			listaTabelaimpostoitem = new ArrayList<Tabelaimpostoitem>();
		}
		listaTabelaimpostoitem.addAll(listaTabelaimpostoitemProduto);
	}
	
	@Transient
	public List<Tabelaimpostoitem> getListaTabelaimpostoitemServicoNBS() {
		List<Tabelaimpostoitem> listaTabelaimpostoitemServicoNBS = new ArrayList<Tabelaimpostoitem>();
		if(listaTabelaimpostoitem != null && listaTabelaimpostoitem.size() > 0){
			for (Tabelaimpostoitem tabelaimpostoitem : listaTabelaimpostoitem) {
				if(tabelaimpostoitem.getTipo() != null && tabelaimpostoitem.getTipo().equals(Tabelaimpostoitemtipo.SERVICO_NBS)){
					listaTabelaimpostoitemServicoNBS.add(tabelaimpostoitem);
				}
			}
		}
		return listaTabelaimpostoitemServicoNBS;
	}
	
	public void setListaTabelaimpostoitemServicoNBS(List<Tabelaimpostoitem> listaTabelaimpostoitemServicoNBS){
		if(listaTabelaimpostoitem == null){
			listaTabelaimpostoitem = new ArrayList<Tabelaimpostoitem>();
		}
		listaTabelaimpostoitem.addAll(listaTabelaimpostoitemServicoNBS);
	}
	
	@Transient
	public List<Tabelaimpostoitem> getListaTabelaimpostoitemServicoItem() {
		List<Tabelaimpostoitem> listaTabelaimpostoitemServicoItem = new ArrayList<Tabelaimpostoitem>();
		if(listaTabelaimpostoitem != null && listaTabelaimpostoitem.size() > 0){
			for (Tabelaimpostoitem tabelaimpostoitem : listaTabelaimpostoitem) {
				if(tabelaimpostoitem.getTipo() != null && tabelaimpostoitem.getTipo().equals(Tabelaimpostoitemtipo.SERVICO_ITEM)){
					listaTabelaimpostoitemServicoItem.add(tabelaimpostoitem);
				}
			}
		}
		return listaTabelaimpostoitemServicoItem;
	}
	
	public void setListaTabelaimpostoitemServicoItem(List<Tabelaimpostoitem> listaTabelaimpostoitemServicoItem){
		if(listaTabelaimpostoitem == null){
			listaTabelaimpostoitem = new ArrayList<Tabelaimpostoitem>();
		}
		listaTabelaimpostoitem.addAll(listaTabelaimpostoitemServicoItem);
	}
	
	@Transient
	public String getIdentificadorSessao() {
		return identificadorSessao;
	}
	
	public void setIdentificadorSessao(String identificadorSessao) {
		this.identificadorSessao = identificadorSessao;
	}

}