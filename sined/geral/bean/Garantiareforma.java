package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Garantiasituacao;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_garantiareforma", sequenceName="sq_garantiareforma")
@DisplayName("Garantia de Reforma")
public class Garantiareforma implements Log{

	private Integer cdgarantiareforma;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	private Empresa empresa;
	private Date dtgarantia;
	private Cliente cliente;
	private Pedidovenda pedidovenda;
	private Pedidovenda pedidovendaorigem;
	private String identificadorexternopedidovenda;
	private Colaborador vendedor;
	private Boolean pedidoexterno;
	private Garantiasituacao garantiasituacao;
	private List<Garantiareformaitem> listaGarantiareformaitem;
	private List<Garantiareformahistorico> listaGarantiareformahistorico;
	private Boolean impresso;
	private Garantiareformaresultado garantiareformaresultado;
	
	//Transient
	private Material servicogarantido;
	private String pneu;
	
	@Id
	@GeneratedValue(generator="sq_garantiareforma", strategy=GenerationType.AUTO)
	@DisplayName("Id")
	public Integer getCdgarantiareforma() {
		return cdgarantiareforma;
	}
	public void setCdgarantiareforma(Integer cdgarantiareforma) {
		this.cdgarantiareforma = cdgarantiareforma;
	}
	
	@Required
	@DisplayName("Empresa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@Required
	@DisplayName("Data de Garantia")
	public Date getDtgarantia() {
		return dtgarantia;
	}
	public void setDtgarantia(Date dtgarantia) {
		this.dtgarantia = dtgarantia;
	}
	
	@Required
	@DisplayName("Cliente")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@DisplayName("Pedido de Venda (Reclama��o)")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	
	@DisplayName("Pedido de Venda Origem")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendaorigem")
	public Pedidovenda getPedidovendaorigem() {
		return pedidovendaorigem;
	}
	public void setPedidovendaorigem(Pedidovenda pedidovendaorigem) {
		this.pedidovendaorigem = pedidovendaorigem;
	}
	
	@DisplayName("Id. Externo Pedido Venda")
	public String getIdentificadorexternopedidovenda() {
		return identificadorexternopedidovenda;
	}
	public void setIdentificadorexternopedidovenda(
			String identificadorexternopedidovenda) {
		this.identificadorexternopedidovenda = identificadorexternopedidovenda;
	}
	
	@Required
	@DisplayName("Vendedor")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendedor")
	public Colaborador getVendedor() {
		return vendedor;
	}
	public void setVendedor(Colaborador vendedor) {
		this.vendedor = vendedor;
	}
	
	@Required
	@DisplayName("Pedido externo?")
	public Boolean getPedidoexterno() {
		return pedidoexterno;
	}
	public void setPedidoexterno(Boolean pedidoexterno) {
		this.pedidoexterno = pedidoexterno;
	}
	
	@DisplayName("Situa��o")
	public Garantiasituacao getGarantiasituacao() {
		return garantiasituacao;
	}
	public void setGarantiasituacao(Garantiasituacao garantiasituacao) {
		this.garantiasituacao = garantiasituacao;
	}
	
	@DisplayName("Pneu")
	@OneToMany(mappedBy="garantiareforma")
	public List<Garantiareformaitem> getListaGarantiareformaitem() {
		return listaGarantiareformaitem;
	}
	
	public void setListaGarantiareformaitem(
			List<Garantiareformaitem> listaGarantiareformaitem) {
		this.listaGarantiareformaitem = listaGarantiareformaitem;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="garantiareforma")
	public List<Garantiareformahistorico> getListaGarantiareformahistorico() {
		return listaGarantiareformahistorico;
	}
	
	public void setListaGarantiareformahistorico(
			List<Garantiareformahistorico> listaGarantiareformahistorico) {
		this.listaGarantiareformahistorico = listaGarantiareformahistorico;
	}
	
	@Override
	public Integer getCdusuarioaltera() {
		return this.cdusuarioaltera;
	}
	@Override
	public Timestamp getDtaltera() {
		return this.dtaltera;
	}
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
		
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@DisplayName("Impresso")
	public Boolean getImpresso() {
		return impresso;
	}
	public void setImpresso(Boolean impresso) {
		this.impresso = impresso;
	}
	
	@DisplayName("Resultado da garantia")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgarantiareformaresultado")
	public Garantiareformaresultado getGarantiareformaresultado() {
		return garantiareformaresultado;
	}
	
	public void setGarantiareformaresultado(
			Garantiareformaresultado garantiareformaresultado) {
		this.garantiareformaresultado = garantiareformaresultado;
	}
	
	@Transient
	@DisplayName("Servi�o garantido")
	public Material getServicogarantido() {
		return servicogarantido;
	}
	public void setServicogarantido(Material servicogarantido) {
		this.servicogarantido = servicogarantido;
	}
	
	@Transient
	@DisplayName("Pneu")
	public String getPneu() {
		return pneu;
	}
	public void setPneu(String pneu) {
		this.pneu = pneu;
	}
}