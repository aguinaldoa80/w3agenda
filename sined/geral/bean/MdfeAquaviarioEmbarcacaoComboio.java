package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_mdfeaquaviarioembarcacaocomboio", sequenceName = "sq_mdfeaquaviarioembarcacaocomboio")
public class MdfeAquaviarioEmbarcacaoComboio {

	protected Integer cdMdfeAquaviarioEmbarcacaoComboio;
	protected Mdfe mdfe;
	protected String codigoEmbarcacao;
	protected String identificadorBalsa;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfeaquaviarioembarcacaocomboio")
	public Integer getCdMdfeAquaviarioEmbarcacaoComboio() {
		return cdMdfeAquaviarioEmbarcacaoComboio;
	}
	public void setCdMdfeAquaviarioEmbarcacaoComboio(
			Integer cdMdfeAquaviarioEmbarcacaoComboio) {
		this.cdMdfeAquaviarioEmbarcacaoComboio = cdMdfeAquaviarioEmbarcacaoComboio;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}

	@Required
	@MaxLength(value=10)
	@DisplayName("C�digo da embarca��o de comboio")
	public String getCodigoEmbarcacao() {
		return codigoEmbarcacao;
	}
	public void setCodigoEmbarcacao(String codigoEmbarcacao) {
		this.codigoEmbarcacao = codigoEmbarcacao;
	}
	
	@Required
	@MaxLength(value=60)
	@DisplayName("Identificador da balsa")
	public String getIdentificadorBalsa() {
		return identificadorBalsa;
	}
	public void setIdentificadorBalsa(String identificadorBalsa) {
		this.identificadorBalsa = identificadorBalsa;
	}
}
