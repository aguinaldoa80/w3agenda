package br.com.linkcom.sined.geral.bean;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_solicitacaocompraorigem", sequenceName = "sq_solicitacaocompraorigem")
@DisplayName("Requisição Origem")
public class Solicitacaocompraorigem implements Log{

	protected Integer cdsolicitacaocompraorigem;
	protected Material material;
	protected Projeto projeto;
	protected Usuario usuario;
	protected Requisicaomaterial requisicaomaterial;
	protected Orcamentomaterial orcamentomaterial;
	protected Tarefarecursogeral tarefarecursogeral;
	protected Planejamentorecursogeral planejamentorecursogeral;
	protected Pedidovenda pedidovenda;
	protected Producaoagenda producaoagenda;
	protected Vendaorcamento vendaorcamento;
	protected Solicitacaocompra solicitacaocompra;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	protected Set<Solicitacaocompra> listaSolicitacaocompra;
	
	public Solicitacaocompraorigem(){
	}
	
	public Solicitacaocompraorigem(Requisicaomaterial requisicaomaterial){
		this.requisicaomaterial = requisicaomaterial;
	}
	
	public Solicitacaocompraorigem(Orcamentomaterial orcamentomaterial){
		this.orcamentomaterial = orcamentomaterial;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_solicitacaocompraorigem")
	@DescriptionProperty
	public Integer getCdsolicitacaocompraorigem() {
		return cdsolicitacaocompraorigem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Usuario getUsuario() {
		return usuario;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrequisicaomaterial")
	public Requisicaomaterial getRequisicaomaterial() {
		return requisicaomaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdorcamentomaterial")
	public Orcamentomaterial getOrcamentomaterial() {
		return orcamentomaterial;
	}	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtarefarecursogeral")
	public Tarefarecursogeral getTarefarecursogeral() {
		return tarefarecursogeral;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdplanejamentorecursogeral")
	public Planejamentorecursogeral getPlanejamentorecursogeral() {
		return planejamentorecursogeral;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	@OneToMany(mappedBy="solicitacaocompraorigem")
	public Set<Solicitacaocompra> getListaSolicitacaocompra() {
		return listaSolicitacaocompra;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoagenda")
	public Producaoagenda getProducaoagenda() {
		return producaoagenda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendaorcamento")
	public Vendaorcamento getVendaorcamento() {
		return vendaorcamento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsolicitacaocompra")
	public Solicitacaocompra getSolicitacaocompra() {
		return solicitacaocompra;
	}

	public void setProducaoagenda(Producaoagenda producaoagenda) {
		this.producaoagenda = producaoagenda;
	}
	public void setVendaorcamento(Vendaorcamento vendaorcamento) {
		this.vendaorcamento = vendaorcamento;
	}
	public void setSolicitacaocompra(Solicitacaocompra solicitacaocompra) {
		this.solicitacaocompra = solicitacaocompra;
	}
	
	public void setListaSolicitacaocompra(
			Set<Solicitacaocompra> listaSolicitacaocompra) {
		this.listaSolicitacaocompra = listaSolicitacaocompra;
	}

	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public void setPlanejamentorecursogeral(
			Planejamentorecursogeral planejamentorecursogeral) {
		this.planejamentorecursogeral = planejamentorecursogeral;
	}
	public void setTarefarecursogeral(Tarefarecursogeral tarefarecursogeral) {
		this.tarefarecursogeral = tarefarecursogeral;
	}
	public void setOrcamentomaterial(Orcamentomaterial orcamentomaterial) {
		this.orcamentomaterial = orcamentomaterial;
	}	
	public void setCdsolicitacaocompraorigem(Integer cdsolicitacaocompraorigem) {
		this.cdsolicitacaocompraorigem = cdsolicitacaocompraorigem;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setRequisicaomaterial(Requisicaomaterial requisicaomaterial) {
		this.requisicaomaterial = requisicaomaterial;
	}

//	LOG
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	

}
