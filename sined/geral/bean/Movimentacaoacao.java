package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
public class Movimentacaoacao {

	public static Movimentacaoacao CANCELADA = new Movimentacaoacao(0, "Cancelada");
	public static Movimentacaoacao NORMAL = new Movimentacaoacao(1,"Normal");
	public static Movimentacaoacao CONCILIADA = new Movimentacaoacao(2, "Conciliada");
	
	public static Movimentacaoacao CRIADA = new Movimentacaoacao(101);	
	public static Movimentacaoacao ESTORNADA = new Movimentacaoacao(102);	
	public static Movimentacaoacao TRANSFERENCIA = new Movimentacaoacao(103);
	
	public static Movimentacaoacao CANCEL_MOVIMENTACAO = new Movimentacaoacao(104);
	public static Movimentacaoacao DEVOLUCAO_CHEQUE = new Movimentacaoacao(105);
	public static Movimentacaoacao RATEIO_ATUALIZADO = new Movimentacaoacao(106);
	
	public static Movimentacaoacao CUSTEIO = new Movimentacaoacao(107);
	
	
	protected Integer cdmovimentacaoacao;
	protected String nome;
	protected Boolean mostramovimentacao;
	
	public Movimentacaoacao(){}
	public Movimentacaoacao(Integer cdmovimentacaoacao){
		this.cdmovimentacaoacao = cdmovimentacaoacao;
	}
	public Movimentacaoacao(Integer cdmovimentacaoacao, String nome){
		this.cdmovimentacaoacao = cdmovimentacaoacao;
		this.nome = nome;
	}
	
	@Id
	public Integer getCdmovimentacaoacao() {
		return cdmovimentacaoacao;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public Boolean getMostramovimentacao() {
		return mostramovimentacao;
	}
	public void setCdmovimentacaoacao(Integer cdmovimentacaoacao) {
		this.cdmovimentacaoacao = cdmovimentacaoacao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setMostramovimentacao(Boolean mostramovimentacao) {
		this.mostramovimentacao = mostramovimentacao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdmovimentacaoacao == null) ? 0 : cdmovimentacaoacao
						.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Movimentacaoacao other = (Movimentacaoacao) obj;
		if (cdmovimentacaoacao == null) {
			if (other.cdmovimentacaoacao != null)
				return false;
		} else if (!cdmovimentacaoacao.equals(other.cdmovimentacaoacao))
			return false;
		return true;
	}
	
}
