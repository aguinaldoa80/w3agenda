package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_aplicacao", sequenceName="sq_aplicacao")
@DisplayName("Aplica��o")
public class Aplicacao {

	private Integer cdaplicacao;
	private Ordemservicoveterinariamaterial ordemservicoveterinariamaterial;
	private Ordemservicoveterinariarotina ordemservicoveterinariarotina;
	private Loteestoque loteestoque;
	private Date dtprevisto;
	private Hora hrprevisto;
	private Date data;
	private Hora hora;
	private String observacao;
	
	public Aplicacao(){
	}
	
	public Aplicacao(Integer cdaplicacao){
		this.cdaplicacao = cdaplicacao;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_aplicacao")
	public Integer getCdaplicacao() {
		return cdaplicacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemservicoveterinariamaterial")
	public Ordemservicoveterinariamaterial getOrdemservicoveterinariamaterial() {
		return ordemservicoveterinariamaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemservicoveterinariarotina")
	public Ordemservicoveterinariarotina getOrdemservicoveterinariarotina() {
		return ordemservicoveterinariarotina;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdloteestoque")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	@Required
	@DisplayName("Hor�rio previsto")
	public Date getDtprevisto() {
		return dtprevisto;
	}
	@Required
	public Hora getHrprevisto() {
		return hrprevisto;
	}
	@Required
	@DisplayName("Hor�rio")
	public Date getData() {
		return data;
	}
	@Required
	public Hora getHora() {
		return hora;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	public void setCdaplicacao(Integer cdaplicacao) {
		this.cdaplicacao = cdaplicacao;
	}
	public void setOrdemservicoveterinariamaterial(Ordemservicoveterinariamaterial ordemservicoveterinariamaterial) {
		this.ordemservicoveterinariamaterial = ordemservicoveterinariamaterial;
	}
	public void setOrdemservicoveterinariarotina(Ordemservicoveterinariarotina ordemservicoveterinariarotina) {
		this.ordemservicoveterinariarotina = ordemservicoveterinariarotina;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	public void setDtprevisto(Date dtprevisto) {
		this.dtprevisto = dtprevisto;
	}
	public void setHrprevisto(Hora hrprevisto) {
		this.hrprevisto = hrprevisto;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setHora(Hora hora) {
		this.hora = hora;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
}