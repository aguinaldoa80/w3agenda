package br.com.linkcom.sined.geral.bean.view;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.Formula;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Unidademedida;

@Entity
public class Vgerenciarmaterial {
	
	public static final String PRODUTO = "Produto";
	public static final String SERVICO = "Servi�o";
	public static final String PATRIMONIO = "Patrim�nio";
	public static final String EPI = "EPI";

	protected Integer cdmaterial;
	protected Integer cdmaterialmovimentacaoestoque;
	protected Integer cdmaterialgrupo;
	protected String nome;
	protected Integer cdmaterialclasse;
	protected Double qtdedisponivel;
	protected Integer tempoentrega;
	protected Localarmazenagem localarmazenagem;
	protected Double valorcustomaterial;
	protected Money valorcustomaterialTrans = new Money();
	protected Money total;
	protected String identificacao;
	protected Loteestoque loteestoque;
	protected Projeto projeto;
	protected Unidademedida unidadeMedida;
	protected Empresa empresa;
	protected Double qtdereservada;
	protected Double pesobruto;

	protected String concated;
	protected String concatedempresa;
	
	protected Double largura;
	protected Double comprimento;
	protected Double altura;
	
	//Transient
	protected Boolean checado;
	protected String materialclassenome;
	protected Double qtdeMininaProduto;
	protected String conversoes;
	protected String nomeunidademedida;
	protected String identificacaonome;
	protected Date dtmovimentacao;
	protected String dimensao;
	
	
	public Vgerenciarmaterial(){
	}
	
	public Vgerenciarmaterial(Integer cdmaterial, Integer cdmaterialgrupo, String nome, Integer cdmaterialclasse, Double qtdedisponivel, Integer tempoentrega, Integer cdlocalarmazenagem, String nomeLocal,Integer cdunidademedida) {
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.cdmaterialclasse = cdmaterialclasse;
		this.cdmaterialgrupo = cdmaterialgrupo;
		
		switch (cdmaterialclasse) {
		case 1:
			this.materialclassenome = PRODUTO;
			break;
		case 2:
			this.materialclassenome = PATRIMONIO;
			break;
		case 3:
			this.materialclassenome = EPI;
			break;
		case 4:
			this.materialclassenome = SERVICO;
			break;
		default:
			break;
		}
		this.qtdedisponivel = qtdedisponivel;
		this.tempoentrega = tempoentrega;
		this.localarmazenagem = new Localarmazenagem(cdlocalarmazenagem, nomeLocal);
		this.unidadeMedida= new Unidademedida(cdunidademedida);
	}
	
	public Vgerenciarmaterial(Integer cdmaterial, Integer cdmaterialgrupo, String nome,
			Integer cdmaterialclasse, Double qtdedisponivel, Double valorcustomaterial, Integer cdlocalarmazenagem, String nomeLocal, Double pesobruto) {
		
		this.valorcustomaterialTrans = valorcustomaterial != null ? new Money(valorcustomaterial, true) : new Money();
		
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.cdmaterialclasse = cdmaterialclasse;
		this.cdmaterialgrupo = cdmaterialgrupo;
		
		switch (cdmaterialclasse) {
		case 1:
			this.materialclassenome = PRODUTO;
			break;
		case 2:
			this.materialclassenome = PATRIMONIO;
			break;
		case 3:
			this.materialclassenome = EPI;
			break;
		case 4:
			this.materialclassenome = SERVICO;
			break;
		default:
			break;
		}
		
		this.qtdedisponivel = qtdedisponivel;
		this.valorcustomaterial = valorcustomaterial;
		this.localarmazenagem = new Localarmazenagem(cdlocalarmazenagem, nomeLocal);
		if(qtdedisponivel != null)
			this.total = this.valorcustomaterialTrans != null ? new Money(this.valorcustomaterialTrans.getValue().doubleValue()*qtdedisponivel): this.valorcustomaterialTrans;
		else 
			this.total = this.valorcustomaterialTrans != null ? this.valorcustomaterialTrans : new Money();
		this.pesobruto = pesobruto;
	
	}
	
	public Vgerenciarmaterial(Integer cdmaterial, Integer cdmaterialgrupo, String nome,
			Integer cdmaterialclasse, Double qtdedisponivel, Double valorcustomaterial, Integer cdlocalarmazenagem, String nomeLocal, Double pesobruto,Integer cdunidademedida,String simbolo,Double largura,Double altura,Double comprimento) {
		
		this.valorcustomaterialTrans = valorcustomaterial != null ? new Money(valorcustomaterial, true) : new Money();
		
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.cdmaterialclasse = cdmaterialclasse;
		this.cdmaterialgrupo = cdmaterialgrupo;
		
		switch (cdmaterialclasse) {
		case 1:
			this.materialclassenome = PRODUTO;
			break;
		case 2:
			this.materialclassenome = PATRIMONIO;
			break;
		case 3:
			this.materialclassenome = EPI;
			break;
		case 4:
			this.materialclassenome = SERVICO;
			break;
		default:
			break;
		}
		
		this.qtdedisponivel = qtdedisponivel;
		this.valorcustomaterial = valorcustomaterial;
		this.localarmazenagem = new Localarmazenagem(cdlocalarmazenagem, nomeLocal);
		if(qtdedisponivel != null)
			this.total = this.valorcustomaterialTrans != null ? new Money(this.valorcustomaterialTrans.getValue().doubleValue()*qtdedisponivel): this.valorcustomaterialTrans;
		else 
			this.total = this.valorcustomaterialTrans != null ? this.valorcustomaterialTrans : new Money();
		this.pesobruto = pesobruto;
		this.unidadeMedida = new Unidademedida(cdunidademedida,simbolo);
		this.largura = largura;
		this.altura = altura;
		this.comprimento = comprimento;
	}
	
	
	
	
	public Vgerenciarmaterial(Integer cdmaterial, Integer cdmaterialgrupo, String nome, String identificacao,
			Integer cdmaterialclasse, Double qtdedisponivel, Double valorcustomaterial, Integer cdlocalarmazenagem, String nomeLocal, Double pesobruto, Integer cdunidademedida,String simbolo,Double largura,Double altura,Double comprimento) {
		this.valorcustomaterialTrans = valorcustomaterial != null ? new Money(valorcustomaterial, true) : new Money();
		
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.cdmaterialclasse = cdmaterialclasse;
		this.cdmaterialgrupo = cdmaterialgrupo;
		this.identificacao = identificacao;
		
		switch (cdmaterialclasse) {
		case 1:
			this.materialclassenome = PRODUTO;
			break;
		case 2:
			this.materialclassenome = PATRIMONIO;
			break;
		case 3:
			this.materialclassenome = EPI;
			break;
		case 4:
			this.materialclassenome = SERVICO;
			break;
		default:
			break;
		}
		
		this.qtdedisponivel = qtdedisponivel;
		this.valorcustomaterial = valorcustomaterial;
		this.localarmazenagem = new Localarmazenagem(cdlocalarmazenagem, nomeLocal);
		if(qtdedisponivel != null)
			this.total = this.valorcustomaterialTrans != null ? new Money(this.valorcustomaterialTrans.getValue().doubleValue()*qtdedisponivel): this.valorcustomaterialTrans;
		else 
			this.total = this.valorcustomaterialTrans != null ? this.valorcustomaterialTrans : new Money();
		this.pesobruto = pesobruto;
		this.unidadeMedida = new Unidademedida(cdunidademedida,simbolo);
		this.largura = largura;
		this.altura = altura;
		this.comprimento = comprimento;
	}
	
	public Vgerenciarmaterial(Integer cdmaterial, Double qtdedisponivel) {
		this.cdmaterial = cdmaterial;		
		this.qtdedisponivel = qtdedisponivel;
	}
	
	
	public Vgerenciarmaterial(Integer cdmaterial, Integer cdmaterialgrupo, String nome, String identificacao,
			Integer cdmaterialclasse, Double qtdedisponivel, Double valorcustomaterial, Integer cdlocalarmazenagem, String nomeLocal,
			Integer cdloteestoque, String numeroLoteestoque, Date validadeLoteestoque, Double qtdereservada, Double pesobruto) {
		this.valorcustomaterialTrans = valorcustomaterial != null ? new Money(valorcustomaterial, true) : new Money();
		
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.cdmaterialclasse = cdmaterialclasse;
		this.cdmaterialgrupo = cdmaterialgrupo;
		this.identificacao = identificacao;
		
		switch (cdmaterialclasse) {
		case 1:
			this.materialclassenome = PRODUTO;
			break;
		case 2:
			this.materialclassenome = PATRIMONIO;
			break;
		case 3:
			this.materialclassenome = EPI;
			break;
		case 4:
			this.materialclassenome = SERVICO;
			break;
		default:
			break;
		}
		
		this.qtdedisponivel = qtdedisponivel;
		this.valorcustomaterial = valorcustomaterial;
		this.localarmazenagem = new Localarmazenagem(cdlocalarmazenagem, nomeLocal);
		if(qtdedisponivel != null)
			this.total = this.valorcustomaterialTrans != null ? new Money(this.valorcustomaterialTrans.getValue().doubleValue()*qtdedisponivel): this.valorcustomaterialTrans;
		else 
			this.total = this.valorcustomaterialTrans != null ? this.valorcustomaterialTrans : new Money();
		if(cdloteestoque != null){
			this.loteestoque = new Loteestoque(cdloteestoque, numeroLoteestoque, (validadeLoteestoque != null ? new java.sql.Date(validadeLoteestoque.getTime()) : null));
		}
		this.qtdereservada = qtdereservada;
		this.pesobruto = pesobruto;
	}
	/*
	 * teste teste
	 */
	public Vgerenciarmaterial(Integer cdmaterial, Integer cdmaterialgrupo, String nome, String identificacao,
			Integer cdmaterialclasse, Double qtdedisponivel, Double valorcustomaterial, Integer cdlocalarmazenagem, String nomeLocal,Double qtdereservada,
			Double pesobruto,Integer cdunidademedida,String simbolo,Double largura,Double altura,Double comprimento) {
		this.valorcustomaterialTrans = valorcustomaterial != null ? new Money(valorcustomaterial, true) : new Money();
		
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.cdmaterialclasse = cdmaterialclasse;
		this.cdmaterialgrupo = cdmaterialgrupo;
		this.identificacao = identificacao;
		
		switch (cdmaterialclasse) {
		case 1:
			this.materialclassenome = PRODUTO;
			break;
		case 2:
			this.materialclassenome = PATRIMONIO;
			break;
		case 3:
			this.materialclassenome = EPI;
			break;
		case 4:
			this.materialclassenome = SERVICO;
			break;
		default:
			break;
		}
		
		this.qtdedisponivel = qtdedisponivel;
		this.valorcustomaterial = valorcustomaterial;
		this.localarmazenagem = new Localarmazenagem(cdlocalarmazenagem, nomeLocal);
		if(qtdedisponivel != null)
			this.total = this.valorcustomaterialTrans != null ? new Money(this.valorcustomaterialTrans.getValue().doubleValue()*qtdedisponivel): this.valorcustomaterialTrans;
		else 
			this.total = this.valorcustomaterialTrans != null ? this.valorcustomaterialTrans : new Money();
		
		this.qtdereservada = qtdereservada;
		this.pesobruto = pesobruto;
		this.unidadeMedida = new Unidademedida(cdunidademedida,simbolo);
		this.largura = largura;
		this.altura = altura;
		this.comprimento = comprimento;
	}
	
	
	public Vgerenciarmaterial(Integer cdmaterial, Integer cdmaterialgrupo, String nome, String identificacao,
			Integer cdmaterialclasse, Double qtdedisponivel, Double valorcustomaterial, Integer cdlocalarmazenagem, String nomeLocal,
			Integer cdloteestoque, String numeroLoteestoque, Date validadeLoteestoque, Double qtdereservada, Double pesobruto,Integer cdunidademedida,String simbolo,Double largura,Double altura,Double comprimento) {
		this.valorcustomaterialTrans = valorcustomaterial != null ? new Money(valorcustomaterial, true) : new Money();
		
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.cdmaterialclasse = cdmaterialclasse;
		this.cdmaterialgrupo = cdmaterialgrupo;
		this.identificacao = identificacao;
		
		switch (cdmaterialclasse) {
		case 1:
			this.materialclassenome = PRODUTO;
			break;
		case 2:
			this.materialclassenome = PATRIMONIO;
			break;
		case 3:
			this.materialclassenome = EPI;
			break;
		case 4:
			this.materialclassenome = SERVICO;
			break;
		default:
			break;
		}
		
		this.qtdedisponivel = qtdedisponivel;
		this.valorcustomaterial = valorcustomaterial;
		this.localarmazenagem = new Localarmazenagem(cdlocalarmazenagem, nomeLocal);
		if(qtdedisponivel != null)
			this.total = this.valorcustomaterialTrans != null ? new Money(this.valorcustomaterialTrans.getValue().doubleValue()*qtdedisponivel): this.valorcustomaterialTrans;
		else 
			this.total = this.valorcustomaterialTrans != null ? this.valorcustomaterialTrans : new Money();
		if(cdloteestoque != null){
			this.loteestoque = new Loteestoque(cdloteestoque, numeroLoteestoque, (validadeLoteestoque != null ? new java.sql.Date(validadeLoteestoque.getTime()) : null));
		}
		this.qtdereservada = qtdereservada;
		this.pesobruto = pesobruto;
		this.unidadeMedida = new Unidademedida(cdunidademedida,simbolo);
		this.largura = largura;
		this.altura = altura;
		this.comprimento = comprimento;
	}
	
	public Vgerenciarmaterial(Integer cdmaterial, Integer cdmaterialgrupo, String nome, String identificacao,
			Integer cdmaterialclasse, Double qtdedisponivel, Double valorcustomaterial, Integer cdlocalarmazenagem, String nomeLocal,
			Double qtdereservada, Double pesobruto,Double largura,Double altura,Double comprimento) {
		this.valorcustomaterialTrans = valorcustomaterial != null ? new Money(valorcustomaterial, true) : new Money();
		
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.cdmaterialclasse = cdmaterialclasse;
		this.cdmaterialgrupo = cdmaterialgrupo;
		this.identificacao = identificacao;
		this.pesobruto = pesobruto;
		
		switch (cdmaterialclasse) {
		case 1:
			this.materialclassenome = PRODUTO;
			break;
		case 2:
			this.materialclassenome = PATRIMONIO;
			break;
		case 3:
			this.materialclassenome = EPI;
			break;
		case 4:
			this.materialclassenome = SERVICO;
			break;
		default:
			break;
		}
		
		this.qtdedisponivel = qtdedisponivel;
		this.valorcustomaterial = valorcustomaterial;
		this.localarmazenagem = new Localarmazenagem(cdlocalarmazenagem, nomeLocal);
		if(qtdedisponivel != null)
			this.total = this.valorcustomaterialTrans != null ? new Money(this.valorcustomaterialTrans.getValue().doubleValue()*qtdedisponivel): this.valorcustomaterialTrans;
		else 
			this.total = this.valorcustomaterialTrans != null ? this.valorcustomaterialTrans : new Money();
		this.qtdereservada = qtdereservada;
		this.largura = largura;
		this.altura = altura;
		this.comprimento = comprimento;
	}
	
	public Vgerenciarmaterial(Integer cdmaterial, Integer cdmaterialgrupo, String nome, String identificacao,
			Integer cdmaterialclasse, Double qtdedisponivel, Double valorcustomaterial, Integer cdlocalarmazenagem, String nomeLocal,
			Integer cdloteestoque, String numeroLoteestoque, Date validadeLoteestoque, Double pesobruto,Integer cdunidademedida,String simbolo,Double largura,Double altura,Double comprimento) {
		this.valorcustomaterialTrans = valorcustomaterial != null ? new Money(valorcustomaterial, true) : new Money();
		
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.cdmaterialclasse = cdmaterialclasse;
		this.cdmaterialgrupo = cdmaterialgrupo;
		this.identificacao = identificacao;
		
		switch (cdmaterialclasse) {
		case 1:
			this.materialclassenome = PRODUTO;
			break;
		case 2:
			this.materialclassenome = PATRIMONIO;
			break;
		case 3:
			this.materialclassenome = EPI;
			break;
		case 4:
			this.materialclassenome = SERVICO;
			break;
		default:
			break;
		}
		
		this.qtdedisponivel = qtdedisponivel;
		this.valorcustomaterial = valorcustomaterial;
		this.localarmazenagem = new Localarmazenagem(cdlocalarmazenagem, nomeLocal);
		this.pesobruto = pesobruto;
		if(qtdedisponivel != null)
			this.total = this.valorcustomaterialTrans != null ? new Money(this.valorcustomaterialTrans.getValue().doubleValue()*qtdedisponivel): this.valorcustomaterialTrans;
		else 
			this.total = this.valorcustomaterialTrans != null ? this.valorcustomaterialTrans : new Money();
		if(cdloteestoque != null){
			this.loteestoque = new Loteestoque(cdloteestoque, numeroLoteestoque, (validadeLoteestoque != null ? new java.sql.Date(validadeLoteestoque.getTime()) : null));
		}
		this.largura = largura;
		this.altura = altura;
		this.comprimento = comprimento;
	}
	public Vgerenciarmaterial(Integer cdmaterial, Integer cdmaterialgrupo, String nome, String identificacao,
			Integer cdmaterialclasse, Double qtdedisponivel, Double valorcustomaterial, Integer cdlocalarmazenagem, String nomeLocal,
			Integer cdloteestoque, String numeroLoteestoque, Date validadeLoteestoque, Double pesobruto) {
		this.valorcustomaterialTrans = valorcustomaterial != null ? new Money(valorcustomaterial, true) : new Money();
		
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.cdmaterialclasse = cdmaterialclasse;
		this.cdmaterialgrupo = cdmaterialgrupo;
		this.identificacao = identificacao;
		
		switch (cdmaterialclasse) {
		case 1:
			this.materialclassenome = PRODUTO;
			break;
		case 2:
			this.materialclassenome = PATRIMONIO;
			break;
		case 3:
			this.materialclassenome = EPI;
			break;
		case 4:
			this.materialclassenome = SERVICO;
			break;
		default:
			break;
		}
		
		this.qtdedisponivel = qtdedisponivel;
		this.valorcustomaterial = valorcustomaterial;
		this.localarmazenagem = new Localarmazenagem(cdlocalarmazenagem, nomeLocal);
		this.pesobruto = pesobruto;
		if(qtdedisponivel != null)
			this.total = this.valorcustomaterialTrans != null ? new Money(this.valorcustomaterialTrans.getValue().doubleValue()*qtdedisponivel): this.valorcustomaterialTrans;
		else 
			this.total = this.valorcustomaterialTrans != null ? this.valorcustomaterialTrans : new Money();
		if(cdloteestoque != null){
			this.loteestoque = new Loteestoque(cdloteestoque, numeroLoteestoque, (validadeLoteestoque != null ? new java.sql.Date(validadeLoteestoque.getTime()) : null));
		}
	}
	public Vgerenciarmaterial(Integer cdmaterial, Integer cdmaterialgrupo, String nome,
			Integer cdmaterialclasse, Double qtdedisponivel, Double valorcustomaterial, Integer cdlocalarmazenagem, String nomeLocal,
			Date dtmovimentacao, Double pesobruto ) {
		this.valorcustomaterialTrans = valorcustomaterial != null ? new Money(valorcustomaterial, true) : new Money();
		
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.cdmaterialclasse = cdmaterialclasse;
		this.cdmaterialgrupo = cdmaterialgrupo;
		
		switch (cdmaterialclasse) {
		case 1:
			this.materialclassenome = PRODUTO;
			break;
		case 2:
			this.materialclassenome = PATRIMONIO;
			break;
		case 3:
			this.materialclassenome = EPI;
			break;
		case 4:
			this.materialclassenome = SERVICO;
			break;
		default:
			break;
		}
		
		this.qtdedisponivel = qtdedisponivel;
		this.valorcustomaterial = valorcustomaterial;
		this.localarmazenagem = new Localarmazenagem(cdlocalarmazenagem, nomeLocal);
		if(qtdedisponivel != null)
			this.total = this.valorcustomaterialTrans != null ? new Money(this.valorcustomaterialTrans.getValue().doubleValue()*qtdedisponivel): this.valorcustomaterialTrans;
		else 
			this.total = this.valorcustomaterialTrans != null ? this.valorcustomaterialTrans : new Money();
		
		this.dtmovimentacao = dtmovimentacao;
		this.pesobruto = pesobruto;
	}
	public Vgerenciarmaterial(Integer cdmaterial, Integer cdmaterialgrupo, String nome,
			Integer cdmaterialclasse, Double qtdedisponivel, Double valorcustomaterial, Integer cdlocalarmazenagem, String nomeLocal,
			Date dtmovimentacao, Double pesobruto,Integer cdunidademedida,String simbolo,Double largura,Double altura,Double comprimento) {
		this.valorcustomaterialTrans = valorcustomaterial != null ? new Money(valorcustomaterial, true) : new Money();
		
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.cdmaterialclasse = cdmaterialclasse;
		this.cdmaterialgrupo = cdmaterialgrupo;
		
		switch (cdmaterialclasse) {
		case 1:
			this.materialclassenome = PRODUTO;
			break;
		case 2:
			this.materialclassenome = PATRIMONIO;
			break;
		case 3:
			this.materialclassenome = EPI;
			break;
		case 4:
			this.materialclassenome = SERVICO;
			break;
		default:
			break;
		}
		
		this.qtdedisponivel = qtdedisponivel;
		this.valorcustomaterial = valorcustomaterial;
		this.localarmazenagem = new Localarmazenagem(cdlocalarmazenagem, nomeLocal);
		if(qtdedisponivel != null)
			this.total = this.valorcustomaterialTrans != null ? new Money(this.valorcustomaterialTrans.getValue().doubleValue()*qtdedisponivel): this.valorcustomaterialTrans;
		else 
			this.total = this.valorcustomaterialTrans != null ? this.valorcustomaterialTrans : new Money();
		
		this.dtmovimentacao = dtmovimentacao;
		this.pesobruto = pesobruto;
		this.unidadeMedida = new Unidademedida(cdunidademedida,simbolo);
		this.altura = altura;
		this.largura = largura;
		this.comprimento = comprimento;
	}
	public Vgerenciarmaterial(Integer cdmaterial, String nome,	Double qtdedisponivel, Localarmazenagem localarmazenagem) {
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.qtdedisponivel = qtdedisponivel;
		this.localarmazenagem = localarmazenagem;
	}
	
	public Vgerenciarmaterial(Integer cdmaterial, String nome,	Double qtdedisponivel, Integer cdlocalarmazenagem, String nomelocal) {
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.qtdedisponivel = qtdedisponivel;
		if(cdlocalarmazenagem != null){
			this.localarmazenagem = new Localarmazenagem(cdlocalarmazenagem, nomelocal);
		}
	}

	@Id
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	
	public Integer getCdmaterialmovimentacaoestoque() {
		return cdmaterialmovimentacaoestoque;
	}
	@DisplayName("Material/Servi�o")
	public String getNome() {
		return nome;
	}
	public Integer getCdmaterialclasse() {
		return cdmaterialclasse;
	}
	@DisplayName("Qtde. dispon�vel")
	public Double getQtdedisponivel() {
		return qtdedisponivel;
	}
	@DisplayName("Tempo entrega")
	public Integer getTempoentrega() {
		return tempoentrega;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdloteestoque")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	@Transient
	public Boolean getChecado() {
		return checado;
	}
	@Transient
	@DisplayName("Classe")
	public String getMaterialclassenome() {
		return materialclassenome;
	}
	@Transient
	public Double getQtdeMininaProduto() {
		return qtdeMininaProduto;
	}
	public Integer getCdmaterialgrupo() {
		return cdmaterialgrupo;
	}
	@Transient
	public Money getTotal() {
		return total;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	public Double getLargura() {
		return largura;
	}
	
	public Double getComprimento() {
		return comprimento;
	}
	
	public Double getAltura() {
		return altura;
	}
	
	public void setAltura(Double altura) {
		this.altura = altura;
	}
	public void setLargura(Double largura) {
		this.largura = largura;
	}
	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setCdmaterialgrupo(Integer cdmaterialgrupo) {
		this.cdmaterialgrupo = cdmaterialgrupo;
	}
	public void setMaterialclassenome(String materialclassenome) {
		this.materialclassenome = materialclassenome;
	}
	public void setQtdeMininaProduto(Double qtdeMininaProduto) {
		this.qtdeMininaProduto = qtdeMininaProduto;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setCdmaterialmovimentacaoestoque(Integer cdmaterialmovimentacaoestoque) {
		this.cdmaterialmovimentacaoestoque = cdmaterialmovimentacaoestoque;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCdmaterialclasse(Integer cdmaterialclasse) {
		this.cdmaterialclasse = cdmaterialclasse;
	}
	public void setQtdedisponivel(Double qtdedisponivel) {
		this.qtdedisponivel = qtdedisponivel;
	}
	public void setTempoentrega(Integer tempoentrega) {
		this.tempoentrega = tempoentrega;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	public void setChecado(Boolean checado) {
		this.checado = checado;
	}
	public Double getValorcustomaterial() {
		return valorcustomaterial;
	}

	public void setValorcustomaterial(Double valorcustomaterial) {
		this.valorcustomaterial = valorcustomaterial;
	}
	public void setTotal(Money total) {
		this.total = total;
	}

	@Transient
	public String getConversoes() {
		return conversoes;
	}
	@Transient
	public String getNomeunidademedida() {
		return nomeunidademedida;
	}
	
	public void setConversoes(String conversoes) {
		this.conversoes = conversoes;
	}
	public void setNomeunidademedida(String nomeunidademedida) {
		this.nomeunidademedida = nomeunidademedida;
	}

	@DisplayName("Identificador")
	public String getIdentificacao() {
		return identificacao;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	@Formula("concat(cdmaterial, '-', cdlocalarmazenagem, '-', cdloteestoque, '-', cdmaterialclasse)")
	public String getConcated() {
		return concated;
	}
	
	@Formula("concat(cdmaterial, '-', cdlocalarmazenagem, '-', cdloteestoque, '-', cdmaterialclasse, '-', cdempresa)")
	public String getConcatedempresa() {
		return concatedempresa;
	}
	
	public void setConcatedempresa(String concatedempresa) {
		this.concatedempresa = concatedempresa;
	}
	
	public void setConcated(String concated) {
		this.concated = concated;
	}

	@Transient
	@DisplayName("Material/Servi�o")
	public String getIdentificacaonome() {
		StringBuilder identificacaonome = new StringBuilder();
		if(this.identificacao != null && !"".equals(this.identificacao)){
			identificacaonome.append(this.identificacao);
		}
		if(this.getNome() != null){
			if(!"".equals(identificacaonome.toString())) identificacaonome.append(" - ");
			identificacaonome.append(this.getNome());
		}
		return identificacaonome.toString();
	}

	@Transient
	public Date getDtmovimentacao() {
		return dtmovimentacao;
	}
	public void setDtmovimentacao(Date dtmovimentacao) {
		this.dtmovimentacao = dtmovimentacao;
	}

	@Transient
	@DisplayName("Qtde. Reservada")
	public Double getQtdereservada() {
		return qtdereservada;
	}
	public void setQtdereservada(Double qtdereservada) {
		this.qtdereservada = qtdereservada;
	}

	@Transient
	public Money getValorcustomaterialTrans() {
		return valorcustomaterialTrans;
	}
	public void setValorcustomaterialTrans(Money valorcustomaterialTrans) {
		this.valorcustomaterialTrans = valorcustomaterialTrans;
	}
	@Transient
	public String getDimensao() {
		return dimensao;
	}
	public void setDimensao(String dimenssao) {
		this.dimensao = dimenssao;
	}
	
	@DisplayName("Peso bruto")
	public Double getPesobruto() {
		return pesobruto;
	}
	public void setPesobruto(Double pesobruto) {
		this.pesobruto = pesobruto;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(Unidademedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	
}
