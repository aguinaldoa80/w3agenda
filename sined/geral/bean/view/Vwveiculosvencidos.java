package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.sined.geral.bean.Veiculo;

@Entity
public class Vwveiculosvencidos {
	
	public static final int TIPO_VENCIMENTO_1 = 1;
	public static final int TIPO_VENCIMENTO_2 = 2;
	public static final int TIPO_VENCIMENTO_3 = 3;
	public static final int TIPO_VENCIMENTO_4 = 4;
	
	private Integer cdveiculo;
	private Veiculo veiculo;
	private Integer tipovencimento;
	
	@Id
	public Integer getCdveiculo() {
		return cdveiculo;
	}
	public Integer getTipovencimento() {
		return tipovencimento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculo",insertable=false, updatable=false)
	public Veiculo getVeiculo() {
		return veiculo;
	}
	
	// setters
	public void setCdveiculo(Integer cdveiculo) {
		this.cdveiculo = cdveiculo;
	}
	
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
	public void setTipovencimento(Integer tipovencimento) {
		this.tipovencimento = tipovencimento;
	}
	

}
