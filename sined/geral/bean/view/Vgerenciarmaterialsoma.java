package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
public class Vgerenciarmaterialsoma {

	protected Integer cdmaterial;
	protected Double qtdedisponivel;
	
	@Id
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	
	@DisplayName("Qtde. disponível")
	public Double getQtdedisponivel() {
		return qtdedisponivel;
	}
	
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	
	public void setQtdedisponivel(Double qtdedisponivel) {
		this.qtdedisponivel = qtdedisponivel;
	}
}
