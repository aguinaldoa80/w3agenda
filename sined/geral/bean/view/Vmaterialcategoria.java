package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
public class Vmaterialcategoria {
	
	protected Integer cdmaterialcategoria;
	protected String nome;
	protected String identificador;
	protected Integer nivel;
	protected String arvorepai;
	
	public Vmaterialcategoria() {
	}
	
	public Vmaterialcategoria(String identificador) {
		this.identificador = identificador;
	}
	
	@Id
	public Integer getCdmaterialcategoria() {
		return cdmaterialcategoria;
	}
	public String getNome() {
		return nome;
	}
	@DisplayName("Identificador")
	public String getIdentificador() {
		return identificador;
	}
	@DisplayName("N�vel")
	public Integer getNivel() {
		return nivel;
	}
	public String getArvorepai() {
		return arvorepai;
	}
	public void setCdmaterialcategoria(Integer cdmaterialcategoria) {
		this.cdmaterialcategoria = cdmaterialcategoria;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}
	public void setArvorepai(String arvorepai) {
		this.arvorepai = arvorepai;
	}

}
