package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
public class Vcontagerencial {

	protected Integer cdcontagerencial;
	protected String nome;
	protected String identificador;
	protected Integer nivel;
	protected String arvorepai;
	
	@Id
	public Integer getCdcontagerencial() {
		return cdcontagerencial;
	}
	public String getNome() {
		return nome;
	}
	@DisplayName("Identificador")
	public String getIdentificador() {
		return identificador;
	}
	@DisplayName("N�vel")
	public Integer getNivel() {
		return nivel;
	}
	public String getArvorepai() {
		return arvorepai;
	}
	public void setArvorepai(String arvorepai) {
		this.arvorepai = arvorepai;
	}
	public void setCdcontagerencial(Integer cdcontagerencial) {
		this.cdcontagerencial = cdcontagerencial;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}

}
