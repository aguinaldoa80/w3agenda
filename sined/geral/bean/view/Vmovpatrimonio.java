package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;

@Entity
public class Vmovpatrimonio {
	
	protected Integer cdmovpatrimonio;
	protected Fornecedor fornecedor;
	protected Localarmazenagem localarmazenagem;
	protected Departamento departamento;
	protected Empresa empresa;
	protected Colaborador colaborador;
	protected Cliente cliente;
	
	@Id
	public Integer getCdmovpatrimonio() {
		return cdmovpatrimonio;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedororigem")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddepartamentoorigem")
	public Departamento getDepartamento() {
		return departamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresaorigem")
	public Empresa getEmpresa() {
		return empresa;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagemorigem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradororigem")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdclienteorigem")
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}



	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}



	public void setCdmovpatrimonio(Integer cdmovpatrimonio) {
		this.cdmovpatrimonio = cdmovpatrimonio;
	}
	
	
}
