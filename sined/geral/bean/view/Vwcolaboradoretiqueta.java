package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Vwcolaboradoretiqueta {
	
	private String id;
	private Integer cdpessoa;
	private String pessoa_nome;
	private Integer cdendereco;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String cep;
	private Integer cdmunicipio;
	private String municipio_nome;
	private Integer cduf;
	private String uf_sigla;
	private Integer cddepartamento;
	private String departamento_nome;
	private Integer cdsituacao;
	
	private String situacao_nome;
	
	@Id
	@Column(name="id", insertable=false, updatable=false)
	public String getId() {
		return id;
	}
	public Integer getCdendereco() {
		return cdendereco;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public String getCep() {
		return cep;
	}
	public Integer getCdmunicipio() {
		return cdmunicipio;
	}
	public String getMunicipio_nome() {
		return municipio_nome;
	}
	public Integer getCduf() {
		return cduf;
	}
	public String getUf_sigla() {
		return uf_sigla;
	}
	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public String getPessoa_nome() {
		return pessoa_nome;
	}
	public Integer getCddepartamento() {
		return cddepartamento;
	}
	public String getDepartamento_nome() {
		return departamento_nome;
	}
	public Integer getCdsituacao() {
		return cdsituacao;
	}
	public String getSituacao_nome() {
		return situacao_nome;
	}

	public void setId(String id) {
		this.id = id;
	}
	public void setCdendereco(Integer cdendereco) {
		this.cdendereco = cdendereco;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setCdmunicipio(Integer cdmunicipio) {
		this.cdmunicipio = cdmunicipio;
	}
	public void setMunicipio_nome(String municipio_nome) {
		this.municipio_nome = municipio_nome;
	}
	public void setCduf(Integer cduf) {
		this.cduf = cduf;
	}
	public void setUf_sigla(String uf_sigla) {
		this.uf_sigla = uf_sigla;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setPessoa_nome(String pessoa_nome) {
		this.pessoa_nome = pessoa_nome;
	}
	public void setCddepartamento(Integer cddepartamento) {
		this.cddepartamento = cddepartamento;
	}
	public void setDepartamento_nome(String departamento_nome) {
		this.departamento_nome = departamento_nome;
	}
	public void setCdsituacao(Integer cdsituacao) {
		this.cdsituacao = cdsituacao;
	}
	public void setSituacao_nome(String situacao_nome) {
		this.situacao_nome = situacao_nome;
	}
}
