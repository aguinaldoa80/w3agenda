package br.com.linkcom.sined.geral.bean.view;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.sined.geral.bean.Veiculo;

@Entity
public class Vwveiculoseminspecaovencido3 {
	
	private Integer cdveiculo;
	private Veiculo veiculo;
	private Date dtinspecao;
	
	@Id
	public Integer getCdveiculo() {
		return cdveiculo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculo",insertable=false, updatable=false)
	public Veiculo getVeiculo() {
		return veiculo;
	}
	
	public Date getDtinspecao() {
		return dtinspecao;
	}
	
	// setters
	public void setCdveiculo(Integer cdveiculo) {
		this.cdveiculo = cdveiculo;
	}
	
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
	public void setDtinspecao(Date dtinspecao) {
		this.dtinspecao = dtinspecao;
	}
}
