package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
public class Vclientesegmento {
	
	protected Integer cdcliente;
	protected String[] segmentos;

	@Id
	@JoinColumn(name="cdcliente", insertable=false, updatable=false)
	public Integer getCdcliente() {
		return cdcliente;
	}
	
	@DisplayName("Segmentos")
	public String[] getSegmentos() {
		return segmentos;
	}
	
	
	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}
	
	public void setSegmentos(String[] segmentos) {
		this.segmentos = segmentos;
	}
}
