package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Vprocessojuridico {

	protected Integer cdprocessojuridico;
	protected String descricao;
	
	@Id
	public Integer getCdprocessojuridico() {
		return cdprocessojuridico;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setCdprocessojuridico(Integer cdprocessojuridico) {
		this.cdprocessojuridico = cdprocessojuridico;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
