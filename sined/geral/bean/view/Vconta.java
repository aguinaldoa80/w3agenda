package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;

@Entity
public class Vconta {

	protected Integer cdconta;
	protected Money saldoatual;
	
	@Id
	public Integer getCdconta() {
		return cdconta;
	}
	@DisplayName("Saldo atual")
	public Money getSaldoatual() {
		return saldoatual;
	}
	
	
	public void setCdconta(Integer cdconta) {
		this.cdconta = cdconta;
	}
	public void setSaldoatual(Money saldoatual) {
		this.saldoatual = saldoatual;
	}
	
}
