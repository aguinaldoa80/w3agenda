package br.com.linkcom.sined.geral.bean.view;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendapagamento;

@Entity
public class Vwdocumentocomissaodesempenho {
	
	private String id;
	private Integer cddocumento;
	private Money valordocumento;
	private Money valoratualdocumento;
	private Date dtemissao;
	private Date dtvencimento;
	private Integer cddocumentoorigem;
	private Integer cdnotadocumento;
	private Integer cdvenda;
	private Date dtvenda;
	private Integer cdcontrato;
	private Integer cdcolaborador;
	private String nomecolaborador;
	private Integer cdcomissionamento;
	private Integer cdpessoa;
	private Integer cdcolaboradorcomissao;
	private Integer cddocumentoacao;
	private Money saldofinal;
	private Money valortotalcusto;
	private Boolean fromVenda;
	
	//TRANSIENTE
	private List<Documento> documentosNegociados;
	private Money valormarkupvenda;
	private Venda venda;
	private String whereInDocumentoVenda;
	
	@Id
	@Column(name="id", insertable=false, updatable=false)
	public String getId() {
		return id;
	}
	public Integer getCddocumento() {
		return cddocumento;
	}
	public Money getValordocumento() {
		return valordocumento;
	}
	public Money getValoratualdocumento() {
		return valoratualdocumento;
	}
	@DisplayName("Emiss�o")
	public Date getDtemissao() {
		return dtemissao;
	}
	@DisplayName("Vencimento")
	public Date getDtvencimento() {
		return dtvencimento;
	}
	public Integer getCddocumentoorigem() {
		return cddocumentoorigem;
	}
	public Integer getCdvenda() {
		return cdvenda;
	}
	public Integer getCdcontrato() {
		return cdcontrato;
	}
	public Integer getCdcolaborador() {
		return cdcolaborador;
	}
	@DisplayName("Colaborador")
	public String getNomecolaborador() {
		return nomecolaborador;
	}
	public Integer getCdcomissionamento() {
		return cdcomissionamento;
	}
	public Integer getCdpessoa() {
		return cdpessoa;
	}	
	public Integer getCdcolaboradorcomissao() {
		return cdcolaboradorcomissao;
	}	
	public Integer getCdnotadocumento() {
		return cdnotadocumento;
	}
	public Integer getCddocumentoacao() {
		return cddocumentoacao;
	}
	public Date getDtvenda() {
		return dtvenda;
	}	
	public Money getSaldofinal() {
		return saldofinal;
	}
	public void setSaldofinal(Money saldofinal) {
		this.saldofinal = saldofinal;
	}
	public void setDtvenda(Date dtvenda) {
		this.dtvenda = dtvenda;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	public void setValordocumento(Money valordocumento) {
		this.valordocumento = valordocumento;
	}
	public void setValoratualdocumento(Money valoratualdocumento) {
		this.valoratualdocumento = valoratualdocumento;
	}
	public void setDtemissao(Date dtemissao) {
		this.dtemissao = dtemissao;
	}
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}
	public void setCddocumentoorigem(Integer cddocumentoorigem) {
		this.cddocumentoorigem = cddocumentoorigem;
	}
	public void setCdvenda(Integer cdvenda) {
		this.cdvenda = cdvenda;
	}
	public void setCdcontrato(Integer cdcontrato) {
		this.cdcontrato = cdcontrato;
	}
	public void setCdcolaborador(Integer cdcolaborador) {
		this.cdcolaborador = cdcolaborador;
	}
	public void setNomecolaborador(String nomecolaborador) {
		this.nomecolaborador = nomecolaborador;
	}
	public void setCdcomissionamento(Integer cdcomissionamento) {
		this.cdcomissionamento = cdcomissionamento;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setCdcolaboradorcomissao(Integer cdcolaboradorcomissao) {
		this.cdcolaboradorcomissao = cdcolaboradorcomissao;
	}
	public void setCdnotadocumento(Integer cdnotadocumento) {
		this.cdnotadocumento = cdnotadocumento;
	}
	public void setCddocumentoacao(Integer cddocumentoacao) {
		this.cddocumentoacao = cddocumentoacao;
	}
	@DisplayName("Valor Total dos Custos")
	public Money getValortotalcusto() {
		return valortotalcusto;
	}
	public void setValortotalcusto(Money valortotalcusto) {
		this.valortotalcusto = valortotalcusto;
	}
	@Transient
	public List<Documento> getDocumentosNegociados() {
		return documentosNegociados;
	}
	
	public void setDocumentosNegociados(List<Documento> documentosNegociados) {
		this.documentosNegociados = documentosNegociados;
	}
	
	@Transient
	public Boolean getPago() {
		Boolean pago = null;
		if(this.cddocumento != null){
			pago = this.cddocumentoacao.equals(Documentoacao.BAIXADA.getCddocumentoacao());
			
			if(!pago && this.documentosNegociados != null && this.documentosNegociados.size() > 0){
				pago = true;
				for (Documento d : this.documentosNegociados) {
					if(!d.getDocumentoacao().equals(Documentoacao.BAIXADA)){
						pago = false;
						break;
					}
				}
			}
		}else if(getVenda() != null){
			if(getVenda().getListavendapagamento() != null && !getVenda().getListavendapagamento().isEmpty()){
				pago = null;
				for(Vendapagamento vendapagamento : getVenda().getListavendapagamento()){
					if(vendapagamento.getDocumento() != null && vendapagamento.getDocumento().getDocumentoacao() != null &&
							Documentoacao.BAIXADA.equals(vendapagamento.getDocumento().getDocumentoacao())){
						pago = true;
					}else {
						pago = false;
						break;
					}
				}
			}
		}
		
		return pago;
	}
	
	@Transient
	@DisplayName("Markup de Venda")
	public Money getValormarkupvenda(){
		valormarkupvenda = new Money();
		if(valordocumento != null && valortotalcusto != null)
			valormarkupvenda = valordocumento.subtract(valortotalcusto);
		return valormarkupvenda;
	}
	public void setValormarkupvenda(Money valormarkupvenda) {
		this.valormarkupvenda = valormarkupvenda;
	}
	
	public Boolean getFromVenda() {
		return fromVenda;
	}
	public void setFromVenda(Boolean fromVenda) {
		this.fromVenda = fromVenda;
	}
	
	@Transient
	public Venda getVenda() {
		return venda;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	@Transient
	public String getWhereInDocumentoVenda() {
		if(getVenda() != null){
			StringBuilder whereInDoc = new StringBuilder();
			if(getVenda().getListavendapagamento() != null && !getVenda().getListavendapagamento().isEmpty()){
				for(Vendapagamento vendapagamento : getVenda().getListavendapagamento()){
					if(vendapagamento.getDocumento() != null && vendapagamento.getDocumento().getCddocumento() != null){
						if(!whereInDoc.toString().equals("")) whereInDoc.append(", ");
						whereInDoc.append("<a href=\"javascript:visualizarDocumento("+vendapagamento.getDocumento().getCddocumento()+");\">"+vendapagamento.getDocumento().getCddocumento()+"</a>");
					}
				}
			}
			return whereInDoc.toString();
		}
		return whereInDocumentoVenda;
	}
	public void setWhereInDocumentoVenda(String whereInDocumentoVenda) {
		this.whereInDocumentoVenda = whereInDocumentoVenda;
	}
}
