package br.com.linkcom.sined.geral.bean.view;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.sined.geral.bean.Veiculo;

@Entity
public class Vwveiculosseminspecao3 {
	
	private Integer cdveiculo;
	private Veiculo veiculo;
	private Date dtagendamento;
	private Date dtinspecao;
	private Boolean tipoinspecao;
	
	@Id
	public Integer getCdveiculo() {
		return cdveiculo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculo",insertable=false, updatable=false)
	public Veiculo getVeiculo() {
		return veiculo;
	}
	
	public Boolean getTipoinspecao() {
		return tipoinspecao;
	}
	
	public void setTipoinspecao(Boolean tipoinspecao) {
		this.tipoinspecao = tipoinspecao;
	}
	
	// setters
	public void setCdveiculo(Integer cdveiculo) {
		this.cdveiculo = cdveiculo;
	}
	
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
	public Date getDtagendamento() {
		return dtagendamento;
	}
	
	public void setDtagendamento(Date dtagendamento) {
		this.dtagendamento = dtagendamento;
	}
	
	public Date getDtinspecao() {
		return dtinspecao;
	}
	
	public void setDtinspecao(Date dtinspecao) {
		this.dtinspecao = dtinspecao;
	}
}
