package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;

@Entity
public class Vwsaidafiscalimposto {
	
	private Integer cdvwsaidafiscalimposto;
	
	protected Integer cdnota;
	protected Integer cdcfop;
	protected String codigo;
	protected String descricaoresumida;
	protected String cfopstr;
	
	protected Money bcinss;
	protected Double inss;
	protected Money valorinss;
	protected Boolean incideinss;
	
	protected Money bcir;
	protected Double ir;
	protected Money valorir;
	protected Boolean incideir;
	
	protected Money bcicms;
	protected Double icms;
	protected Money valoricms;
	protected Boolean incideicms;
	
	protected Money bcicmsst;
	protected Double icmsst;
	protected Money valoricmsst;
	protected Boolean incideicmsst;
	
	protected Money bcpis;
	protected Double pis;
	protected Money valorpis;
	protected Boolean incidepis;
	
	protected Money bcipi;
	protected Double ipi;
	protected Money valoripi;
	protected Boolean incideipi;
	
	protected Money bccofins;
	protected Double cofins;
	protected Money valorcofins;
	protected Boolean incidecofins;
	
	protected Money bciss;
	protected Double iss;
	protected Money valoriss;
	protected Boolean incideiss;
	
	protected Money bccsll;
	protected Double csll;
	protected Money valorcsll;
	protected Boolean incidecsll;
	
	
	
	public Vwsaidafiscalimposto(){
	}
	
	public Vwsaidafiscalimposto(Integer cdvwsaidafiscalimposto, Integer cdnota, Integer cdcfop, String codigo, String descricaoresumida, 
			Long bcinss, Double inss, Boolean incideinss, Long bcir, Double ir, Boolean incideir, 
			Long bcicms, Double icms, Boolean incideicms, Long bcicmsst, Double icmsst, Boolean incideicmsst, 
			Long bcpis, Double pis,Boolean incidepis, Long bcipi, Double ipi, Boolean incideipi, 
			Long bccofins, Double cofins, Boolean incidecofins, Long bciss, Double iss, Boolean incideiss, 
			Long bccsll, Double csll, Boolean incidecsll) {
		this.cdvwsaidafiscalimposto = cdvwsaidafiscalimposto;
		
		this.cdnota = cdnota;
		this.cdcfop = cdcfop;
		this.codigo = codigo;
		this.descricaoresumida = descricaoresumida;
		
		this.bcinss = bcinss != null ? new Money(bcinss):new Money();
		this.inss = inss != null ? inss : 0.0;
		this.valorinss = this.bcinss.multiply(new Money(this.inss));
		this.incideinss = incideinss;
		
		this.bcir = bcir != null ? new Money(bcir):new Money();
		this.ir = ir != null ? ir:0.0;
		this.valorir = this.bcir.multiply(new Money(this.ir));
		this.incideir = incideir;
		
		this.bcicms = bcicms != null ? new Money(bcicms):new Money();
		this.icms = icms != null ? icms : 0.0;
		this.valoricms = this.bcicms.multiply(new Money(this.icms)); 
		this.incideicms = incideicms;
		
		this.bcicmsst = bcicmsst != null ? new Money(bcicmsst):new Money();
		this.icmsst = icmsst != null ? icmsst:0.0;
		this.valoricmsst = this.bcicmsst.multiply(new Money(this.icmsst));
		this.incideicmsst = incideicmsst;
		
		this.bcpis = bcpis != null ? new Money(bcpis):new Money();
		this.pis = pis != null ? pis:0.0;
		this.valorpis = this.bcpis.multiply(new Money(this.pis));
		this.incidepis = incidepis;
		
		this.bcipi = bcipi != null ? new Money(bcipi):new Money();
		this.ipi = ipi != null ? ipi:0.0;
		this.valoripi = this.bcipi.multiply(new Money(this.ipi));
		this.incideipi = incideipi;
		
		this.bccofins = bccofins != null ? new Money(bccofins):new Money();
		this.cofins = cofins != null ? cofins:0.0;
		this.valorcofins = this.bccofins.multiply(new Money(this.cofins));
		this.incidecofins = incidecofins;
		
		this.bciss = bciss != null ? new Money(bciss):new Money();
		this.iss = iss != null ? iss:0.0;
		this.valoriss = this.bciss.multiply(new Money(this.iss));
		this.incideiss = incideiss;
		
		this.bccsll = bccsll != null ? new Money(bccsll):new Money();
		this.csll = csll != null ? csll:0.0;
		this.valorcsll = this.bccsll.multiply(new Money(this.csll));
		this.incidecsll = incidecsll;
	}

	@Id
	@Column(name="cdvwsaidafiscalimposto", insertable=false, updatable=false)
	public Integer getCdvwsaidafiscalimposto() {
		return cdvwsaidafiscalimposto;
	}
	
	public void setCdvwsaidafiscalimposto(Integer cdvwsaidafiscalimposto) {
		this.cdvwsaidafiscalimposto = cdvwsaidafiscalimposto;
	}
	
	public Integer getCdnota() {
		return cdnota;
	}
	public Integer getCdcfop() {
		return cdcfop;
	}	
	public String getDescricaoresumida() {
		return descricaoresumida;
	}	
	public String getCodigo() {
		return codigo;
	}
	@Transient
	public String getCfopstr() {
		StringBuilder s = new StringBuilder();
		
		if(this.codigo != null && !"".equals(this.codigo)){
			s.append(this.codigo);
		}
		if(descricaoresumida != null && !"".equals(descricaoresumida)){
			if(!s.toString().equals("")) s.append(" - ");
			s.append(descricaoresumida);
		}		
		return s.toString();
	}

	
	@DisplayName("Base de C�lculo de INSS")
	public Money getBcinss() {
		return bcinss;
	}
	@DisplayName("Al�quota INSS (%)")
	public Double getInss() {
		return inss;
	}
	@Transient
	@DisplayName("Valor INSS")
	public Money getValorinss() {
		if(bcinss != null && inss != null)
			this.valorinss = this.bcinss.multiply(new Money(this.inss)).divide(new Money(100.0));
		return valorinss;
	}
	@DisplayName("Base de C�lculo de IR")
	public Money getBcir() {
		return bcir;
	}
	@DisplayName("Al�quota IR (%)")
	public Double getIr() {
		return ir;
	}
	@Transient
	@DisplayName("Valor IR")
	public Money getValorir() {
		if(bcir != null && ir != null)
			this.valorir = this.bcir.multiply(new Money(this.ir)).divide(new Money(100.0));
		return valorir;
	}
	@DisplayName("Base de C�lculo de ICMS")
	public Money getBcicms() {
		return bcicms;
	}
	@DisplayName("Al�quota ICMS (%)")
	public Double getIcms() {
		return icms;
	}
	@Transient
	@DisplayName("Valor ICMS")
	public Money getValoricms() {
		if(bcicms != null && icms != null)
			this.valoricms = this.bcicms.multiply(new Money(this.icms)).divide(new Money(100.0));
		return valoricms;
	}
	@DisplayName("Base de C�lculo de ICMSST")
	public Money getBcicmsst() {
		return bcicmsst;
	}
	@DisplayName("Al�quota ICMSST (%)")
	public Double getIcmsst() {
		return icmsst;
	}
	@Transient
	@DisplayName("Valor ICMSST")
	public Money getValoricmsst() {
		if(bcicmsst != null && icmsst != null)
			this.valoricmsst = this.bcicmsst.multiply(new Money(this.icmsst)).divide(new Money(100.0));
		return valoricmsst;
	}
	@DisplayName("Base de C�lculo de PIS")
	public Money getBcpis() {
		return bcpis;
	}
	@DisplayName("Al�quota PIS (%)")
	public Double getPis() {
		return pis;
	}
	@Transient
	@DisplayName("Valor PIS")
	public Money getValorpis() {
		if(bcpis != null && pis != null)
			this.valorpis = this.bcpis.multiply(new Money(this.pis)).divide(new Money(100.0));
		return valorpis;
	}
	@DisplayName("Base de C�lculo de IPI")
	public Money getBcipi() {
		return bcipi;
	}
	@DisplayName("Al�quota IPI (%)")
	public Double getIpi() {
		return ipi;
	}
	@Transient
	@DisplayName("Valor IPI")
	public Money getValoripi() {
		if(bcipi != null && ipi != null)
			this.valoripi = this.bcipi.multiply(new Money(this.ipi)).divide(new Money(100.0));
		return valoripi;
	}
	@DisplayName("Base de C�lculo de COFINS")
	public Money getBccofins() {
		return bccofins;
	}
	@DisplayName("Al�quota COFINS (%)")
	public Double getCofins() {
		return cofins;
	}
	@Transient
	@DisplayName("Valor COFINS")
	public Money getValorcofins() {
		if(bccofins != null && cofins != null)
			this.valorcofins = this.bccofins.multiply(new Money(this.cofins)).divide(new Money(100.0));
		return valorcofins;
	}
	@DisplayName("Base de C�lculo de ISS")
	public Money getBciss() {
		return bciss;
	}
	@DisplayName("Al�quota ISS (%)")
	public Double getIss() {
		return iss;
	}
	@Transient
	@DisplayName("Valor ISS")
	public Money getValoriss() {
		if(bciss != null && iss != null)
			this.valoriss = this.bciss.multiply(new Money(this.iss)).divide(new Money(100.0));
		return valoriss;
	}
	@DisplayName("Base de C�lculo de CSLL")
	public Money getBccsll() {
		return bccsll;
	}
	@DisplayName("Al�quota CSLL (%)")
	public Double getCsll() {
		return csll;
	}
	@Transient
	@DisplayName("Valor CSLL")
	public Money getValorcsll() {
		if(bccsll != null && csll != null)
			this.valorcsll = this.bccsll.multiply(new Money(this.csll)).divide(new Money(100.0));
		return valorcsll;
	}

	public void setCdnota(Integer cdnota) {
		this.cdnota = cdnota;
	}

	public void setCdcfop(Integer cdcfop) {
		this.cdcfop = cdcfop;
	}
	
	public void setDescricaoresumida(String descricaoresumida) {
		this.descricaoresumida = descricaoresumida;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setBcicms(Money bcicms) {
		this.bcicms = bcicms;
	}

	public void setIcms(Double icms) {
		this.icms = icms;
	}

	public void setValoricms(Money valoricms) {
		this.valoricms = valoricms;
	}

	public void setBcicmsst(Money bcicmsst) {
		this.bcicmsst = bcicmsst;
	}

	public void setIcmsst(Double icmsst) {
		this.icmsst = icmsst;
	}

	public void setValoricmsst(Money valoricmsst) {
		this.valoricmsst = valoricmsst;
	}

	public void setBcpis(Money bcpis) {
		this.bcpis = bcpis;
	}

	public void setPis(Double pis) {
		this.pis = pis;
	}

	public void setValorpis(Money valorpis) {
		this.valorpis = valorpis;
	}

	public void setBcipi(Money bcipi) {
		this.bcipi = bcipi;
	}

	public void setIpi(Double ipi) {
		this.ipi = ipi;
	}

	public void setValoripi(Money valoripi) {
		this.valoripi = valoripi;
	}

	public void setBccofins(Money bccofins) {
		this.bccofins = bccofins;
	}

	public void setCofins(Double cofins) {
		this.cofins = cofins;
	}

	public void setValorcofins(Money valorcofins) {
		this.valorcofins = valorcofins;
	}

	public void setBciss(Money bciss) {
		this.bciss = bciss;
	}

	public void setIss(Double iss) {
		this.iss = iss;
	}

	public void setValoriss(Money valoriss) {
		this.valoriss = valoriss;
	}

	public void setBccsll(Money bccsll) {
		this.bccsll = bccsll;
	}

	public void setCsll(Double csll) {
		this.csll = csll;
	}

	public void setValorcsll(Money valorcsll) {
		this.valorcsll = valorcsll;
	}

	public Boolean getIncideinss() {
		return incideinss;
	}

	public void setIncideinss(Boolean incideinss) {
		this.incideinss = incideinss;
	}

	public Boolean getIncideir() {
		return incideir;
	}

	public void setIncideir(Boolean incideir) {
		this.incideir = incideir;
	}

	public Boolean getIncideicms() {
		return incideicms;
	}

	public void setIncideicms(Boolean incideicms) {
		this.incideicms = incideicms;
	}

	public Boolean getIncideicmsst() {
		return incideicmsst;
	}

	public void setIncideicmsst(Boolean incideicmsst) {
		this.incideicmsst = incideicmsst;
	}

	public Boolean getIncidepis() {
		return incidepis;
	}

	public void setIncidepis(Boolean incidepis) {
		this.incidepis = incidepis;
	}

	public Boolean getIncideipi() {
		return incideipi;
	}

	public void setIncideipi(Boolean incideipi) {
		this.incideipi = incideipi;
	}

	public Boolean getIncidecofins() {
		return incidecofins;
	}

	public void setIncidecofins(Boolean incidecofins) {
		this.incidecofins = incidecofins;
	}

	public Boolean getIncideiss() {
		return incideiss;
	}

	public void setIncideiss(Boolean incideiss) {
		this.incideiss = incideiss;
	}

	public Boolean getIncidecsll() {
		return incidecsll;
	}

	public void setIncidecsll(Boolean incidecsll) {
		this.incidecsll = incidecsll;
	}

	public void setCfopstr(String cfopstr) {
		this.cfopstr = cfopstr;
	}

	public void setBcinss(Money bcinss) {
		this.bcinss = bcinss;
	}

	public void setInss(Double inss) {
		this.inss = inss;
	}

	public void setValorinss(Money valorinss) {
		this.valorinss = valorinss;
	}

	public void setBcir(Money bcir) {
		this.bcir = bcir;
	}

	public void setIr(Double ir) {
		this.ir = ir;
	}

	public void setValorir(Money valorir) {
		this.valorir = valorir;
	}
	
}
