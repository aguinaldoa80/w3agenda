package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.sined.geral.bean.Documento;

@Entity
public class Vwcountmovimentacaonormal {
	
	private Long countv;
	private Documento documento;
	private Integer cddocumento;
	
	public Long getCountv() {
		return countv;
	}
	
	@Id
	public Integer getCddocumento() {
		return cddocumento;
	}

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento",insertable=false, updatable=false)
	public Documento getDocumento() {
		return documento;
	}
	
	public void setCountv(Long countv) {
		this.countv = countv;
	}
	
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	
	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
}
