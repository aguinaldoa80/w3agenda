package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cfop;

public class Vwentregadocumentoimposto {
	
	protected Integer cdentregadocumento;
	protected Integer cdcfop;
	protected String codigo;
	protected String descricaoresumida;
	protected String descricaocompleta;
	
	protected Money bcicms;
	protected Money icms;
	protected Money valoricms;
	
	protected Money bcicmsst;
	protected Money icmsst;
	protected Money valoricmsst;
	
	protected Money bcfcp;
	protected Money fcp;
	protected Money valorfcp;
	
	protected Money bcfcpst;
	protected Money fcpst;
	protected Money valorfcpst;
	
	protected Money bcpis;
	protected Money pis;
	protected Money valorpis;
	
	protected Money bcipi;
	protected Money ipi;
	protected Money valoripi;
	
	protected Money bccofins;
	protected Money cofins;
	protected Money valorcofins;
	
	protected Money bciss;
	protected Money iss;
	protected Money valoriss;
	
	protected Money bccsll;
	protected Money csll;
	protected Money valorcsll;
	
	protected Money valorir;
	protected Money valorinss;
	protected Money valorimpostoimportacao;
	
	private Money valorIcmsDesonerado;
	
	public Vwentregadocumentoimposto(){
	}
	
	public Vwentregadocumentoimposto(Integer cdentregadocumento, Integer cdcfop, String codigo, String descricaoresumida, 
			String descricaocompleta, Long bcicms, Long icms, Long bcicmsst, Long icmsst, Long bcpis, Long pis,
			Long bcipi, Long ipi, Long bccofins, Long cofins, Long bciss, Long iss,
			Long bcfcp, Long fcp, Long bcfcpst, Long fcpst) {
		
		this.cdentregadocumento = cdentregadocumento;
		this.cdcfop = cdcfop;
		this.codigo = codigo;
		this.descricaoresumida = descricaoresumida;
		this.descricaocompleta = descricaocompleta;
		
		this.bcicms = bcicms != null ? new Money(bcicms):new Money();
		this.icms = icms != null ? new Money(icms):new Money();
		this.valoricms = this.bcicms.multiply(this.icms); 
		
		this.bcicmsst = bcicmsst != null ? new Money(bcicmsst):new Money();
		this.icmsst = icmsst != null ? new Money(icmsst):new Money();
		this.valoricmsst = this.bcicmsst.multiply(this.icmsst);
		
		this.bcfcp = bcfcp != null ? new Money(bcfcp):new Money();
		this.fcp = fcp != null ? new Money(fcp):new Money();
		this.valorfcp = this.bcfcp.multiply(this.fcp); 
		
		this.bcfcpst = bcfcpst != null ? new Money(bcfcpst):new Money();
		this.fcpst = fcpst != null ? new Money(fcpst):new Money();
		this.valorfcpst = this.bcfcpst.multiply(this.fcpst);
		
		this.bcpis = bcpis != null ? new Money(bcpis):new Money();
		this.pis = pis != null ? new Money(pis):new Money();
		this.valorpis = this.bcpis.multiply(this.pis);
		
		this.bcipi = bcipi != null ? new Money(bcipi):new Money();
		this.ipi = ipi != null ? new Money(ipi):new Money();
		this.valoripi = this.bcipi.multiply(this.ipi);
		
		this.bccofins = bccofins != null ? new Money(bccofins):new Money();
		this.cofins = cofins != null ? new Money(cofins):new Money();
		this.valorcofins = this.bccofins.multiply(this.cofins);
		
		this.bciss = bciss != null ? new Money(bciss):new Money();
		this.bciss = bciss != null ? new Money(iss):new Money();
		this.valoriss = this.bciss.multiply(this.iss);
	}
	
	public Vwentregadocumentoimposto(Cfop cfop, Money bcicms, Double icms, Money bcicmsst, Double icmsst, Money bcpis, Double pis,
			Money bcipi, Double ipi, Money bccofins, Double cofins, Money bciss, Double iss, Money bcfcp, Double fcp, Money bcfcpst, Double fcpst) {
		if(cfop != null){
			this.cdcfop = cfop.getCdcfop();
			this.codigo = cfop.getCodigo();
			this.descricaoresumida = cfop.getDescricaoresumida();
			this.descricaocompleta = cfop.getDescricaocompleta();
		}
		
		this.bcicms = bcicms != null ? bcicms:new Money();
		this.icms = icms != null ? new Money(icms/100):new Money();
		this.valoricms = this.bcicms.multiply(this.icms); 
		
		this.bcicmsst = bcicmsst != null ? bcicmsst:new Money();
		this.icmsst = icmsst != null ? new Money(icmsst/100):new Money();
		this.valoricmsst = this.bcicmsst.multiply(this.icmsst).divide(new Money(100));
		
		this.bcfcp = bcfcp != null ? bcfcp:new Money();
		this.fcp = fcp != null ? new Money(fcp/100):new Money();
		this.valorfcp = this.bcfcp.multiply(this.fcp); 
		
		this.bcfcpst = bcfcpst != null ? bcfcpst:new Money();
		this.fcpst = fcpst != null ? new Money(fcpst/100):new Money();
		this.valorfcpst = this.bcfcpst.multiply(this.fcpst).divide(new Money(100));
		
		this.bcpis = bcpis != null ? bcpis:new Money();
		this.pis = pis != null ? new Money(pis/100):new Money();
		this.valorpis = this.bcpis.multiply(this.pis);
		
		this.bcipi = bcipi != null ? bcipi:new Money();
		this.ipi = ipi != null ? new Money(ipi/100):new Money();
		this.valoripi = this.bcipi.multiply(this.ipi);
		
		this.bccofins = bccofins != null ? bccofins:new Money();
		this.cofins = cofins != null ? new Money(cofins/100):new Money();
		this.valorcofins = this.bccofins.multiply(this.cofins);
		
		this.bciss = bciss != null ? bciss:new Money();
		this.iss = iss != null ? new Money(iss/100):new Money();
		this.valoriss = this.bciss.multiply(this.iss);		
		
	}
	
	public Vwentregadocumentoimposto(Cfop cfop, Money bcicms, Double icms, Money bcicmsst, Double icmsst, Money bcpis, Double pis,
			Money bcipi, Double ipi, Money bccofins, Double cofins, Money bciss, Double iss, Money valorir, Money valorcsll, 
			Money valorinss, Money valorimpostoimportacao, Money bcfcp, Double fcp, Money bcfcpst, Double fcpst) {
		if(cfop != null){
			this.cdcfop = cfop.getCdcfop();
			this.codigo = cfop.getCodigo();
			this.descricaoresumida = cfop.getDescricaoresumida();
			this.descricaocompleta = cfop.getDescricaocompleta();
		}
		
		this.bcicms = bcicms != null ? bcicms:new Money();
		this.icms = icms != null ? new Money(icms/100):new Money();
		this.valoricms = this.bcicms.multiply(this.icms); 
		
		this.bcicmsst = bcicmsst != null ? bcicmsst:new Money();
		this.icmsst = icmsst != null ? new Money(icmsst/100):new Money();
		this.valoricmsst = this.bcicmsst.multiply(this.icmsst).divide(new Money(100));
		
		this.bcfcp = bcfcp != null ? bcfcp:new Money();
		this.fcp = fcp != null ? new Money(fcp/100):new Money();
		this.valorfcp = this.bcfcp.multiply(this.fcp); 
		
		this.bcfcpst = bcfcpst != null ? bcfcpst:new Money();
		this.fcpst = fcpst != null ? new Money(fcpst/100):new Money();
		this.valorfcpst = this.bcfcpst.multiply(this.fcpst).divide(new Money(100));
		
		this.bcpis = bcpis != null ? bcpis:new Money();
		this.pis = pis != null ? new Money(pis/100):new Money();
		this.valorpis = this.bcpis.multiply(this.pis);
		
		this.bcipi = bcipi != null ? bcipi:new Money();
		this.ipi = ipi != null ? new Money(ipi/100):new Money();
		this.valoripi = this.bcipi.multiply(this.ipi);
		
		this.bccofins = bccofins != null ? bccofins:new Money();
		this.cofins = cofins != null ? new Money(cofins/100):new Money();
		this.valorcofins = this.bccofins.multiply(this.cofins);
		
		this.bciss = bciss != null ? bciss:new Money();
		this.iss = iss != null ? new Money(iss/100):new Money();
		this.valoriss = this.bciss.multiply(this.iss);		
		
		this.valorir = valorir;
		this.valorcsll = valorcsll;
		this.valorinss = valorinss;
		this.valorimpostoimportacao = valorimpostoimportacao;
	}
	
	public Vwentregadocumentoimposto(Cfop cfop, Money bcicms, Double icms, Money valoricms, Money bcicmsst, Double icmsst, Money valoricmsst,
			Money bcpis, Double pis, Money valorpis, Money bcipi, Double ipi, Money valoripi, 
			Money bccofins, Double cofins, Money valorcofins, Money bciss, Double iss, Money valoriss, 
			Money valorir, Money valorcsll, Money valorinss, Money valorimpostoimportacao, 
			Money bcfcp, Double fcp, Money valorfcp, Money bcfcpst, Double fcpst, Money valorfcpst, Money valorIcmsDesonerado) {
		if(cfop != null){
			this.cdcfop = cfop.getCdcfop();
			this.codigo = cfop.getCodigo();
			this.descricaoresumida = cfop.getDescricaoresumida();
			this.descricaocompleta = cfop.getDescricaocompleta();
		}
		
		this.bcicms = bcicms != null ? bcicms:new Money();
		this.icms = icms != null ? new Money(icms/100):new Money();
		this.valoricms = valoricms != null ? valoricms : new Money(); 
		
		this.bcicmsst = bcicmsst != null ? bcicmsst:new Money();
		this.icmsst = icmsst != null ? new Money(icmsst/100):new Money();
		this.valoricmsst = valoricmsst != null ? valoricmsst : new Money();
		
		this.bcfcp = bcfcp != null ? bcfcp:new Money();
		this.fcp = fcp != null ? new Money(fcp/100):new Money();
		this.valorfcp = valorfcp != null ? valorfcp : new Money(); 
		
		this.bcfcpst = bcfcpst != null ? bcfcpst:new Money();
		this.fcpst = fcpst != null ? new Money(fcpst/100):new Money();
		this.valorfcpst = valorfcpst != null ? valorfcpst : new Money();
		
		this.bcpis = bcpis != null ? bcpis:new Money();
		this.pis = pis != null ? new Money(pis/100):new Money();
		this.valorpis = valorpis != null ? valorpis : new Money();
		
		this.bcipi = bcipi != null ? bcipi:new Money();
		this.ipi = ipi != null ? new Money(ipi/100):new Money();
		this.valoripi = valoripi != null ? valoripi : new Money();
		
		this.bccofins = bccofins != null ? bccofins:new Money();
		this.cofins = cofins != null ? new Money(cofins/100):new Money();
		this.valorcofins = valorcofins != null ? valorcofins : new Money();
		
		this.bciss = bciss != null ? bciss:new Money();
		this.iss = iss != null ? new Money(iss/100):new Money();
		this.valoriss = valoriss != null ? valoriss : new Money();		
		
		this.valorir = valorir;
		this.valorcsll = valorcsll;
		this.valorinss = valorinss;
		this.valorimpostoimportacao = valorimpostoimportacao;
		
		this.valorIcmsDesonerado = valorIcmsDesonerado;
	}


	public Integer getCdentregadocumento() {
		return cdentregadocumento;
	}
	public Integer getCdcfop() {
		return cdcfop;
	}
	public String getCodigo() {
		return codigo;
	}
	public String getDescricaoresumida() {
		return descricaoresumida;
	}
	public String getDescricaocompleta() {
		return descricaocompleta;
	}	
	@DisplayName("Base de C�lculo de ICMS")
	public Money getBcicms() {
		return bcicms;
	}
	@DisplayName("Al�quota ICMS (%)")
	public Money getIcms() {
		if(icms != null)
			return icms.multiply(new Money(100));
		return icms;
	}

	@Transient
	@DisplayName("Valor ICMS")
	public Money getValoricms() {
//		if(bcicms != null && icms != null)
//			this.valoricms = this.bcicms.multiply(this.icms);
		return valoricms;
	}
	@DisplayName("Base de C�lculo de ICMSST")
	public Money getBcicmsst() {
		return bcicmsst;
	}
	@DisplayName("Al�quota ICMSST (%)")
	public Money getIcmsst() {
		if(icmsst != null)
			return icmsst.multiply(new Money(100));
		return icmsst;
	}
	@Transient
	@DisplayName("Valor ICMS ST")
	public Money getValoricmsst() {
//		if(bcicmsst != null && icmsst != null)
//			this.valoricmsst = this.bcicmsst.multiply(this.icmsst);
		return valoricmsst;
	}
	@DisplayName("Base de C�lculo de PIS")
	public Money getBcpis() {
		return bcpis;
	}
	@DisplayName("Al�quota PIS (%)")
	public Money getPis() {
		if(pis != null)
			return pis.multiply(new Money(100));
		return pis;
	}
	@Transient
	@DisplayName("Valor PIS")
	public Money getValorpis() {
//		if(bcpis != null && pis != null)
//			this.valorpis = this.bcpis.multiply(this.pis);
		return valorpis;
	}
	@DisplayName("Base de C�lculo de IPI")
	public Money getBcipi() {
		return bcipi;
	}
	@DisplayName("Al�quota IPI (%)")
	public Money getIpi() {
		if(ipi != null)
			return ipi.multiply(new Money(100));
		return ipi;
	}
	@Transient
	@DisplayName("Valor IPI")
	public Money getValoripi() {
//		if(bcipi != null && ipi != null)
//			this.valoripi = this.bcipi.multiply(this.ipi);
		return valoripi;
	}
	@DisplayName("Base de C�lculo de COFINS")
	public Money getBccofins() {
		return bccofins;
	}
	@DisplayName("Al�quota COFINS (%)")
	public Money getCofins() {
		if(cofins != null)
			return cofins.multiply(new Money(100));
		return cofins;
	}
	@Transient
	@DisplayName("Valor COFINS")
	public Money getValorcofins() {
//		if(bccofins != null && cofins != null)
//			this.valorcofins = this.bccofins.multiply(this.cofins);
		return valorcofins;
	}
	@DisplayName("Base de C�lculo de ISS")
	public Money getBciss() {
		return bciss;
	}
	@DisplayName("Al�quota ISS (%)")
	public Money getIss() {
		if(iss != null)
			return iss.multiply(new Money(100));
		
		return iss;
	}
	@Transient
	@DisplayName("Valor ISS")
	public Money getValoriss() {
//		if(bciss != null && iss != null)
//			this.valoriss = this.bciss.multiply(this.iss);
		return valoriss;
	}
	@DisplayName("Base de C�lculo de CSLL")
	public Money getBccsll() {
		return bccsll;
	}
	@DisplayName("Al�quota CSLL (%)")
	public Money getCsll() {
		if(csll != null)
			return csll.multiply(new Money(100));
		return csll;
	}
	@Transient
	@DisplayName("Valor CSLL")
	public Money getValorcsll() {
//		if(bccsll != null && csll != null)
//			this.valorcsll = this.bccsll.multiply(this.csll);
		return valorcsll;
	}
	@Transient
	@DisplayName("Valor IR")
	public Money getValorir() {
		return valorir;
	}
	@Transient
	@DisplayName("Valor INSS")
	public Money getValorinss() {
		return valorinss;
	}
	@Transient
	@DisplayName("Valor Imposto Importa��o")
	public Money getValorimpostoimportacao() {
		return valorimpostoimportacao;
	}

	@DisplayName("Base de C�lculo de FCP")
	public Money getBcfcp() {
		return bcfcp;
	}

	public void setBcfcp(Money bcfcp) {
		this.bcfcp = bcfcp;
	}

	@DisplayName("Al�quota FCP (%)")
	public Money getFcp() {
		return fcp;
	}

	public void setFcp(Money fcp) {
		this.fcp = fcp;
	}

	@Transient
	@DisplayName("Valor FCP")
	public Money getValorfcp() {
		return valorfcp;
	}

	public void setValorfcp(Money valorfcp) {
		this.valorfcp = valorfcp;
	}

	@DisplayName("Base de C�lculo de FCP ST")
	public Money getBcfcpst() {
		return bcfcpst;
	}

	public void setBcfcpst(Money bcfcpst) {
		this.bcfcpst = bcfcpst;
	}

	@DisplayName("Al�quota FCP ST (%)")
	public Money getFcpst() {
		return fcpst;
	}

	public void setFcpst(Money fcpst) {
		this.fcpst = fcpst;
	}

	@Transient
	@DisplayName("Valor FCP ST")
	public Money getValorfcpst() {
		return valorfcpst;
	}
	
	@Transient
	@DisplayName("Valor ICMS Desonerado")
	public Money getValorIcmsDesonerado() {
		return valorIcmsDesonerado;
	}
	
	public void setValorIcmsDesonerado(Money valorIcmsDesonerado) {
		this.valorIcmsDesonerado = valorIcmsDesonerado;
	}
	
	public void setValorfcpst(Money valorfcpst) {
		this.valorfcpst = valorfcpst;
	}

	public void setCdentregadocumento(Integer cdentregadocumento) {
		this.cdentregadocumento = cdentregadocumento;
	}
	public void setCdcfop(Integer cdcfop) {
		this.cdcfop = cdcfop;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setDescricaoresumida(String descricaoresumida) {
		this.descricaoresumida = descricaoresumida;
	}
	public void setDescricaocompleta(String descricaocompleta) {
		this.descricaocompleta = descricaocompleta;
	}
	public void setBcicms(Money bcicms) {
		this.bcicms = bcicms;
	}
	public void setIcms(Money icms) {
		this.icms = icms;
	}
	public void setValoricms(Money valoricms) {
		this.valoricms = valoricms;
	}
	public void setBcicmsst(Money bcicmsst) {
		this.bcicmsst = bcicmsst;
	}
	public void setIcmsst(Money icmsst) {
		this.icmsst = icmsst;
	}
	public void setValoricmsst(Money valoricmsst) {
		this.valoricmsst = valoricmsst;
	}
	public void setBcpis(Money bcpis) {
		this.bcpis = bcpis;
	}
	public void setPis(Money pis) {
		this.pis = pis;
	}
	public void setValorpis(Money valorpis) {
		this.valorpis = valorpis;
	}
	public void setBcipi(Money bcipi) {
		this.bcipi = bcipi;
	}
	public void setIpi(Money ipi) {
		this.ipi = ipi;
	}
	public void setValoripi(Money valoripi) {
		this.valoripi = valoripi;
	}
	public void setBccofins(Money bccofins) {
		this.bccofins = bccofins;
	}
	public void setCofins(Money cofins) {
		this.cofins = cofins;
	}
	public void setValorcofins(Money valorcofins) {
		this.valorcofins = valorcofins;
	}
	public void setBciss(Money bciss) {
		this.bciss = bciss;
	}
	public void setIss(Money iss) {
		this.iss = iss;
	}
	public void setValoriss(Money valoriss) {
		this.valoriss = valoriss;
	}
	public void setBccsll(Money bccsll) {
		this.bccsll = bccsll;
	}
	public void setCsll(Money csll) {
		this.csll = csll;
	}
	public void setValorcsll(Money valorcsll) {
		this.valorcsll = valorcsll;
	}
	public void setValorir(Money valorir) {
		this.valorir = valorir;
	}
	public void setValorinss(Money valorinss) {
		this.valorinss = valorinss;
	}
	public void setValorimpostoimportacao(Money valorimpostoimportacao) {
		this.valorimpostoimportacao = valorimpostoimportacao;
	}
	
	@Transient
	public String getDescricaoCodigo(){
		if(codigo != null && descricaoresumida != null){
			return codigo + " - " + descricaoresumida; 
		} else {
			return "";
		}
	}
}
