package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.sined.geral.bean.Inspecaoitem;

@Entity
public class Vwitemvencido {
	
	private Integer cdveiculo;
	private Integer cditeminspecao;
	private Integer tipovencimento;
	private Inspecaoitem inspecaoitem;
	
	@Column(name="cdveiculo", insertable=false, updatable=false)
	public Integer getCdveiculo() {
		return cdveiculo;
	}
	
	@Id
	@Column(name="cditeminspecao", insertable=false, updatable=false)
	public Integer getCditeminspecao() {
		return cditeminspecao;
	}
	
	@Column(name="tipovencimento", insertable=false, updatable=false)
	public Integer getTipovencimento() {
		return tipovencimento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cditeminspecao",insertable=false, updatable=false)
	public Inspecaoitem getInspecaoitem() {
		return inspecaoitem;
	}	
	
	public void setCdveiculo(Integer cdveiculo) {
		this.cdveiculo = cdveiculo;
	}
	
	public void setCditeminspecao(Integer cditeminspecao) {
		this.cditeminspecao = cditeminspecao;
	}
	
	public void setTipovencimento(Integer tipovencimento) {
		this.tipovencimento = tipovencimento;
	}
	
	public void setInspecaoitem(Inspecaoitem inspecaoitem) {
		this.inspecaoitem = inspecaoitem;
	}	
}
