package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;

@Entity
public class Vwultimovalorcotacao {

	protected Integer id;
	protected String tipo;
	protected Fornecedor fornecedor;
	protected Material material;
	protected Money valor;
	
	@Id
	public Integer getId() {
		return id;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor",insertable=false, updatable=false)
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial",insertable=false, updatable=false)
	public Material getMaterial() {
		return material;
	}
	public Money getValor() {
		return valor;
	}
	public String getTipo() {
		return tipo;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}
