package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
public class Vwclienteetiqueta {
		private String id;
		private Integer cdendereco;
	    private String logradouro;
	    private String numero;
	    private String complemento;
	    private String bairro;
	    private String cep;
	    private Integer cdmunicipio;
	    private String municipio_nome;
	    private Integer cduf;
	    private String uf_sigla;
	    private Integer cdpessoa;
	    private String pessoa_nome;
	    private Integer cdenderecotipo;
	    private String enderecotipo_nome;
	    private Integer cdpessoacategoria;
	    private Integer cdcategoria;
	    private String categoria_nome;
	    private String pontoreferencia;
	    private String identificador;
	    
	    
	    @Id
		@Column(name="id", insertable=false, updatable=false)
	    public String getId() {
			return id;
		}
		public Integer getCdendereco() {
			return cdendereco;
		}
		
		@DisplayName("Logradouro")
		public String getLogradouro() {
			return logradouro;
		}
		public String getNumero() {
			return numero;
		}
		public String getComplemento() {
			return complemento;
		}
		public String getBairro() {
			return bairro;
		}
		public String getCep() {
			return cep;
		}
		public Integer getCdmunicipio() {
			return cdmunicipio;
		}
		
		@DisplayName("Munic�pio")
		public String getMunicipio_nome() {
			return municipio_nome;
		}
		public Integer getCduf() {
			return cduf;
		}
		
		@DisplayName("UF")
		public String getUf_sigla() {
			return uf_sigla;
		}
		
		@DisplayName("C�digo")
		public Integer getCdpessoa() {
			return cdpessoa;
		}
		
		@DisplayName("Cliente")
		public String getPessoa_nome() {
			return pessoa_nome;
		}
		public Integer getCdenderecotipo() {
			return cdenderecotipo;
		}
		
		@DisplayName("Tipo de endere�o")
		public String getEnderecotipo_nome() {
			return enderecotipo_nome;
		}
		public Integer getCdpessoacategoria() {
			return cdpessoacategoria;
		}
		public Integer getCdcategoria() {
			return cdcategoria;
		}
		public String getCategoria_nome() {
			return categoria_nome;
		}
		public String getPontoreferencia() {
			return pontoreferencia;
		}

		
		public void setId(String id) {
			this.id = id;
		}
		public void setCdendereco(Integer cdendereco) {
			this.cdendereco = cdendereco;
		}
		public void setLogradouro(String logradouro) {
			this.logradouro = logradouro;
		}
		public void setNumero(String numero) {
			this.numero = numero;
		}
		public void setComplemento(String complemento) {
			this.complemento = complemento;
		}
		public void setBairro(String bairro) {
			this.bairro = bairro;
		}
		public void setCep(String cep) {
			this.cep = cep;
		}
		public void setCdmunicipio(Integer cdmunicipio) {
			this.cdmunicipio = cdmunicipio;
		}
		public void setMunicipio_nome(String municipio_nome) {
			this.municipio_nome = municipio_nome;
		}
		public void setCduf(Integer cduf) {
			this.cduf = cduf;
		}
		public void setUf_sigla(String uf_sigla) {
			this.uf_sigla = uf_sigla;
		}
		public void setCdpessoa(Integer cdpessoa) {
			this.cdpessoa = cdpessoa;
		}
		public void setPessoa_nome(String pessoa_nome) {
			this.pessoa_nome = pessoa_nome;
		}
		public void setCdenderecotipo(Integer cdenderecotipo) {
			this.cdenderecotipo = cdenderecotipo;
		}
		public void setEnderecotipo_nome(String enderecotipo_nome) {
			this.enderecotipo_nome = enderecotipo_nome;
		}
		public void setCdpessoacategoria(Integer cdpessoacategoria) {
			this.cdpessoacategoria = cdpessoacategoria;
		}
		public void setCdcategoria(Integer cdcategoria) {
			this.cdcategoria = cdcategoria;
		}
		public void setCategoria_nome(String categoria_nome) {
			this.categoria_nome = categoria_nome;
		}
	    public void setPontoreferencia(String pontoreferencia) {
			this.pontoreferencia = pontoreferencia;
		}
		public String getIdentificador() {
			return identificador;
		}
		public void setIdentificador(String identificador) {
			this.identificador = identificador;
		}
}
