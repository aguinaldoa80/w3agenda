package br.com.linkcom.sined.geral.bean.view;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.sined.geral.bean.Veiculo;

@Entity
public class Vwrelatorioinspecao {
	
	private String cdrelatorioinspecao;//Convers�o para duas chaves prim�rias
	private String placa;
	private	String prefixo;
	private String nomemodelo;
	private Integer cditeminspecao;
	private String nomepessoa;
	private String nomeiteminspecao;
	private String nometipoiteminspecao;
	private Integer tipovencimento;
	private Integer kminspecao;
	private Double horimetroinspecao;
	private Date  dtmanutencao;
	private Boolean visual;
	private Integer calculakmatual;
	private Double calculahorimetroatual;
	private String nometipo;
	private String nomeultimoprojetousoveiculo;
	private String nomeultimocondutorusoveiculo;
	private Veiculo veiculo;
	
	@Id
	public String getCdrelatorioinspecao() {
		return cdrelatorioinspecao;
	}
	public String getNomemodelo() {
		return nomemodelo;
	}
	public String getNometipo() {
		return nometipo;
	}
	public String getPlaca() {
		return placa;
	}
	
	public String getPrefixo() {
		return prefixo;
	}

	public Integer getCditeminspecao() {
		return cditeminspecao;
	}
	
	public String getNomepessoa() {
		return nomepessoa;
	}
	
	public String getNomeiteminspecao() {
		return nomeiteminspecao;
	}
	
	public String getNometipoiteminspecao() {
		return nometipoiteminspecao;
	}
	
	public Integer getTipovencimento() {
		return tipovencimento;
	}
	
	public Integer getKminspecao() {
		return kminspecao;
	}
	
	public Date getDtmanutencao() {
		return dtmanutencao;
	}
	
	public Boolean getVisual() {
		return visual;
	}
	
	public Integer getCalculakmatual() {
		return calculakmatual;
	}
	
	public String getNomeultimoprojetousoveiculo() {
		return nomeultimoprojetousoveiculo;
	}
	
	public String getNomeultimocondutorusoveiculo() {
		return nomeultimocondutorusoveiculo;
	}
	
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	
	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}
	
	public void setCditeminspecao(Integer cditeminspecao) {
		this.cditeminspecao = cditeminspecao;
	}
	
	public void setNomepessoa(String nomepessoa) {
		this.nomepessoa = nomepessoa;
	}
	
	public void setNomeiteminspecao(String nomeiteminspecao) {
		this.nomeiteminspecao = nomeiteminspecao;
	}
	
	public void setNometipoiteminspecao(String nometipoiteminspecao) {
		this.nometipoiteminspecao = nometipoiteminspecao;
	}
	
	public void setTipovencimento(Integer tipovencimento) {
		this.tipovencimento = tipovencimento;
	}
	
	public void setKminspecao(Integer kminspecao) {
		this.kminspecao = kminspecao;
	}
	
	public void setDtmanutencao(Date dtmanutencao) {
		this.dtmanutencao = dtmanutencao;
	}
	
	public void setVisual(Boolean visual) {
		this.visual = visual;
	}
	
	public void setCalculakmatual(Integer calculakmatual) {
		this.calculakmatual = calculakmatual;
	}
	
	public void setNomeultimoprojetousoveiculo(String nomeultimoprojetousoveiculo) {
		this.nomeultimoprojetousoveiculo = nomeultimoprojetousoveiculo;
	}
	
	public void setNomeultimocondutorusoveiculo(String nomeultimocondutorusoveiculo) {
		this.nomeultimocondutorusoveiculo = nomeultimocondutorusoveiculo;
	}
	
	public void setNomemodelo(String nomemodelo) {
		this.nomemodelo = nomemodelo;
	}
	
	public void setNometipo(String nometipo) {
		this.nometipo = nometipo;
	}
	
	public void setCdrelatorioinspecao(String cdrelatorioinspecao) {
		this.cdrelatorioinspecao = cdrelatorioinspecao;
	}
	public Double getHorimetroinspecao() {
		return horimetroinspecao;
	}
	public void setHorimetroinspecao(Double horimetroinspecao) {
		this.horimetroinspecao = horimetroinspecao;
	}
	public Double getCalculahorimetroatual() {
		return calculahorimetroatual;
	}
	public void setCalculahorimetroatual(Double calculahorimetroatual) {
		this.calculahorimetroatual = calculahorimetroatual;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculo", insertable=false, updatable=false)
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
}