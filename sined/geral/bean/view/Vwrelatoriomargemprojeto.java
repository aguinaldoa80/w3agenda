package br.com.linkcom.sined.geral.bean.view;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.util.SinedDateUtils;

@Entity
public class Vwrelatoriomargemprojeto {
	
	protected String id;
	protected Projeto projeto;
	protected Money valor;
	protected Date dtref;
	protected Tipooperacao tipooperacao;
	
	public Vwrelatoriomargemprojeto() {
	}
	
	public Vwrelatoriomargemprojeto(Date dtref) {
		this.dtref = dtref;
	}
	
	@Id
	public String getId() {
		return id;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}

	public Money getValor() {
		return valor;
	}

	public Date getDtref() {
		return dtref;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipooperacao")
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}
	
	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public void setDtref(Date dtref) {
		this.dtref = dtref;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Vwrelatoriomargemprojeto))
			return false;
		final Vwrelatoriomargemprojeto other = (Vwrelatoriomargemprojeto) obj;
		if (dtref == null) {
			if (other.dtref != null)
				return false;
		} else if (!SinedDateUtils.equalsIgnoreHour(dtref, other.dtref))
			return false;
		return true;
	}

	
}
