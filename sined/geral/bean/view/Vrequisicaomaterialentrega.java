package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.Unidademedida;

@Entity
public class Vrequisicaomaterialentrega {

	protected Integer cdrequisicaomaterial;
	protected Boolean entregaromaneio;
	protected Double qtde;
	protected Unidademedida unidademedida;
	protected Localarmazenagem localentregamaterial;
	protected Localarmazenagem localpedido;
	
	protected Requisicaomaterial requisicaomaterial;
	
	public Vrequisicaomaterialentrega() {
	}
	
	public Vrequisicaomaterialentrega(Integer cdrequisicaomaterial, Boolean entregaromaneio) {
		this.cdrequisicaomaterial = cdrequisicaomaterial;
		this.entregaromaneio = entregaromaneio;
	}

	@Id
	public Integer getCdrequisicaomaterial() {
		return cdrequisicaomaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrequisicaomaterial", insertable=false, updatable=false)
	public Requisicaomaterial getRequisicaomaterial() {
		return requisicaomaterial;
	}
	public Boolean getEntregaromaneio() {
		return entregaromaneio;
	}
	public Double getQtde() {
		return qtde;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida", insertable=false, updatable=false)
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalentregamaterial", insertable=false, updatable=false)
	public Localarmazenagem getLocalentregamaterial() {
		return localentregamaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalpedido", insertable=false, updatable=false)
	public Localarmazenagem getLocalpedido() {
		return localpedido;
	}

	public void setCdrequisicaomaterial(Integer cdrequisicaomaterial) {
		this.cdrequisicaomaterial = cdrequisicaomaterial;
	}
	public void setEntregaromaneio(Boolean entregaromaneio) {
		this.entregaromaneio = entregaromaneio;
	}
	public void setRequisicaomaterial(Requisicaomaterial requisicaomaterial) {
		this.requisicaomaterial = requisicaomaterial;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setLocalentregamaterial(Localarmazenagem localentregamaterial) {
		this.localentregamaterial = localentregamaterial;
	}
	public void setLocalpedido(Localarmazenagem localpedido) {
		this.localpedido = localpedido;
	}
}
