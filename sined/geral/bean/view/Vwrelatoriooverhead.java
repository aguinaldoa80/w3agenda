package br.com.linkcom.sined.geral.bean.view;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Tipooperacao;

@Entity
public class Vwrelatoriooverhead {
	
	protected String id;
	protected Centrocusto centrocusto;
	protected Money valor;
	protected Date dtref;
	protected Empresa empresa;
	protected Tipooperacao tipooperacao;
	
	@Id
	public String getId() {
		return id;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}

	public Money getValor() {
		return valor;
	}

	public Date getDtref() {
		return dtref;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipooperacao")
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}
	
	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public void setDtref(Date dtref) {
		this.dtref = dtref;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
}
