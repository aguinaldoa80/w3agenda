package br.com.linkcom.sined.geral.bean.view;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;

@Entity
public class Vwformularioppp {
	
	private Integer cdpessoa;
	private String nome;
	private Cpf cpf;
	protected Set<Colaboradorcargo> listaColaboradorcargo;
	
	@Id
	@Column(name="cdpessoa", insertable=false, updatable=false)
	public Integer getCdpessoa() {
		return cdpessoa;
	}
	
	@Column(name="nome", insertable=false, updatable=false)
	public String getNome() {
		return nome;
	}
	
	@Column(name="cpf", insertable=false, updatable=false)
	public Cpf getCpf() {
		return cpf;
	}
	
	@OneToMany(mappedBy="colaborador")
	public Set<Colaboradorcargo> getListaColaboradorcargo() {
		return listaColaboradorcargo;
	}
	
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	public void setListaColaboradorcargo(
			Set<Colaboradorcargo> listaColaboradorcargo) {
		this.listaColaboradorcargo = listaColaboradorcargo;
	}
	
}
