package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Vmaterialrelacionado {

	protected Integer cdmaterial;
	protected Integer quantidade;
	
	@Id
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
}
