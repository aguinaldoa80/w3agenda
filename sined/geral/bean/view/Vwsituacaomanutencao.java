package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.sined.geral.bean.Veiculoordemservico;

@Entity
public class Vwsituacaomanutencao {
	
	private Integer cdsituacaomanutencao;
	private Integer cdveiculoordemservico;
	private Veiculoordemservico veiculoordemservico;
	
	public Integer getCdsituacaomanutencao() {
		return cdsituacaomanutencao;
	}
	@Id
	public Integer getCdveiculoordemservico() {
		return cdveiculoordemservico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculoordemservico",insertable=false, updatable=false)
	public Veiculoordemservico getVeiculoordemservico() {
		return veiculoordemservico;
	}
	
	public void setCdsituacaomanutencao(Integer cdsituacaomanutencao) {
		this.cdsituacaomanutencao = cdsituacaomanutencao;
	}
	
	public void setCdveiculoordemservico(Integer cdveiculoordemservico) {
		this.cdveiculoordemservico = cdveiculoordemservico;
	}
	
	public void setVeiculoordemservico(Veiculoordemservico veiculoordemservico) {
		this.veiculoordemservico = veiculoordemservico;
	}
	
}
