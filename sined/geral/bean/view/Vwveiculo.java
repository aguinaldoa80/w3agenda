package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Vwveiculo {
	
	private Integer cdveiculo;
	private Integer kmatual;
	private Integer kmreal;
	private Integer kmrodado;
	private Double horimetroatual;
	private Double horimetroreal;
	private Double horimetrorodado;
	
	@Id
	public Integer getCdveiculo() {
		return cdveiculo;
	}
	
	public Integer getKmatual() {
		return kmatual;
	}
	
	public Integer getKmreal() {
		return kmreal;
	}
	
	public Integer getKmrodado() {
		return kmrodado;
	}
	
	public void setKmatual(Integer kmatual) {
		this.kmatual = kmatual;
	}
	
	public void setCdveiculo(Integer cdveiculo) {
		this.cdveiculo = cdveiculo;
	}
	
	public void setKmreal(Integer kmreal) {
		this.kmreal = kmreal;
	}
	
	public void setKmrodado(Integer kmrodado) {
		this.kmrodado = kmrodado;
	}

	public Double getHorimetroatual() {
		return horimetroatual;
	}

	public Double getHorimetroreal() {
		return horimetroreal;
	}

	public Double getHorimetrorodado() {
		return horimetrorodado;
	}

	public void setHorimetroatual(Double horimetroatual) {
		this.horimetroatual = horimetroatual;
	}

	public void setHorimetroreal(Double horimetroreal) {
		this.horimetroreal = horimetroreal;
	}

	public void setHorimetrorodado(Double horimetrorodado) {
		this.horimetrorodado = horimetrorodado;
	}
}
