package br.com.linkcom.sined.geral.bean.view;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Vcontratoqtdelocacao {

	protected Integer cdcontrato;
	protected Date dtfimlocacao;
	protected Double qtde_romaneio;
	protected Double qtde_fechamento;
	
	@Id
	public Integer getCdcontrato() {
		return cdcontrato;
	}

	public Date getDtfimlocacao() {
		return dtfimlocacao;
	}

	public Double getQtde_romaneio() {
		return qtde_romaneio;
	}

	public Double getQtde_fechamento() {
		return qtde_fechamento;
	}

	public void setCdcontrato(Integer cdcontrato) {
		this.cdcontrato = cdcontrato;
	}

	public void setDtfimlocacao(Date dtfimlocacao) {
		this.dtfimlocacao = dtfimlocacao;
	}

	public void setQtde_romaneio(Double qtdeRomaneio) {
		qtde_romaneio = qtdeRomaneio;
	}

	public void setQtde_fechamento(Double qtdeFechamento) {
		qtde_fechamento = qtdeFechamento;
	}

}
