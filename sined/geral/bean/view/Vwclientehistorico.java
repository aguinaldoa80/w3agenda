package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Vwclientehistorico {
	
	private String id;
	private Integer pessoacategoria_cdpessoacategoria;
	private Integer pessoa_cdpessoa;
	private String pessoa_nome;
	private Integer categoria_cdcategoria;
	private String categoria_nome;
	private String identificador;
	
	
	
	@Id
	@Column(name="id", insertable=false, updatable=false)
	public String getId() {
		return id;
	}
	
	public Integer getPessoa_cdpessoa() {
		return pessoa_cdpessoa;
	}
	public Integer getPessoacategoria_cdpessoacategoria() {
		return pessoacategoria_cdpessoacategoria;
	}
	public String getPessoa_nome() {
		return pessoa_nome;
	}
	public Integer getCategoria_cdcategoria() {
		return categoria_cdcategoria;
	}
	public String getCategoria_nome() {
		return categoria_nome;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public void setPessoacategoria_cdpessoacategoria(
			Integer pessoacategoria_cdpessoacategoria) {
		this.pessoacategoria_cdpessoacategoria = pessoacategoria_cdpessoacategoria;
	}
	public void setPessoa_cdpessoa(Integer pessoa_cdpessoa) {
		this.pessoa_cdpessoa = pessoa_cdpessoa;
	}
	public void setPessoa_nome(String pessoa_nome) {
		this.pessoa_nome = pessoa_nome;
	}
	public void setCategoria_cdcategoria(Integer categoria_cdcategoria) {
		this.categoria_cdcategoria = categoria_cdcategoria;
	}
	public void setCategoria_nome(String categoria_nome) {
		this.categoria_nome = categoria_nome;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
}
