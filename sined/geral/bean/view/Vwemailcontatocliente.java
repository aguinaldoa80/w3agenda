package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;

@Entity
public class Vwemailcontatocliente {
	
	private String id;
	private Cliente cliente;
	private String emailcontatocliente;
	private Integer cdcontato;
	private String contatonome;
	
	@Id
	@Column(name="id", insertable=false, updatable=false)
	public String getId() {
		return id;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente",insertable=false, updatable=false)
	@DisplayName("Nome do cliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("E-mail")
	@JoinColumn(name="emailcontatocliente",insertable=false, updatable=false)
	public String getEmailcontatocliente() {
		return emailcontatocliente;
	}
	
	@JoinColumn(name="cdcontato",insertable=false, updatable=false)
	public Integer getCdcontato() {
		return cdcontato;
	}
	
	@DisplayName("Nome do contato")
	@JoinColumn(name="contatonome",insertable=false, updatable=false)
	public String getContatonome() {
		return contatonome;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setEmailcontatocliente(String emailcontatocliente) {
		this.emailcontatocliente = emailcontatocliente;
	}

	public void setCdcontato(Integer cdcontato) {
		this.cdcontato = cdcontato;
	}

	public void setContatonome(String contatonome) {
		this.contatonome = contatonome;
	}

}
