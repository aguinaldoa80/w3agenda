package br.com.linkcom.sined.geral.bean.view;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Tipooperacao;

@Entity
public class Vwmovimentacaomensal implements Comparable<Vwmovimentacaomensal> {
	
	private String id;
	private String mesano;
	private Tipooperacao tipooperacao;
	private Money valor;
	
	// Transiente
	private String descMesAno;
	private Date dataMov;
	
	@Id
	public String getId() {
		return id;
	}
	
	public String getMesano() {
		return mesano;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipooperacao")
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}
	
	public Money getValor() {
		return valor;
	}
	
	@Transient
	public String getDescMesAno() {
		return descMesAno;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setMesano(String mesano) {
		this.mesano = mesano;
	}
	
	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}
	
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	public void setDescMesAno(String descMesAno) {
		this.descMesAno = descMesAno;
	}

	@Transient
	public Date getDataMov() {
		return dataMov;
	}

	public void setDataMov(Date dataMov) {
		this.dataMov = dataMov;
	}

	public int compareTo(Vwmovimentacaomensal that) {
		if (this.getMesano() != null && that.getMesano() != null) {
			if (this.getMesano().length() == 7 && that.getMesano().length() == 7) {
				Integer thisMes = Integer.parseInt(this.getMesano().substring(0,2));
				Integer thisAno = Integer.parseInt(this.getMesano().substring(3,7)); 
				Integer thatMes = Integer.parseInt(that.getMesano().substring(0,2));
				Integer thatAno = Integer.parseInt(that.getMesano().substring(3,7)); 
				
				if (thisAno.equals(thatAno)) {
					return thisMes.compareTo(thatMes);
				}
				return thisAno.compareTo(thatAno);
			}
		}
		return 0;
	}
}
