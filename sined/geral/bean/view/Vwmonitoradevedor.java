package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
public class Vwmonitoradevedor {
	private Integer id;
	private String contrato_descricao; 
	private Integer cdcliente;
	private String nome_cliente;
	private Integer cdsituacao;
	private Integer cdempresa;
	private Integer cdcategoria;
	private Integer qtdecontas;
	
	//Transientes
	private String documentos;
	private String contasbloqueio;

	
	@Id
	@Column(name="id", insertable=false, updatable=false)
	public Integer getId() {
		return id;
	}
	@DisplayName("Contrato")
	public String getContrato_descricao() {
		return contrato_descricao;
	}
	public Integer getCdcliente() {
		return cdcliente;
	}
	@DisplayName("Cliente")
	public String getNome_cliente() {
		return nome_cliente;
	}
	public Integer getCdsituacao() {
		return cdsituacao;
	}
	public Integer getCdempresa() {
		return cdempresa;
	}
	public Integer getCdcategoria() {
		return cdcategoria;
	}
	
	@Transient
	@DisplayName("Contas atrasadas")
	public String getDocumentos() {
		return documentos;
	}
	public Integer getQtdecontas() {
		return qtdecontas;
	}
	
	@Transient
	@DisplayName("Contas do bloqueio")
	public String getContasbloqueio() {
		return contasbloqueio;
	}
	public void setContasbloqueio(String contasbloqueio) {
		this.contasbloqueio = contasbloqueio;
	}
	public void setQtdecontas(Integer qtdecontas) {
		this.qtdecontas = qtdecontas;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setContrato_descricao(String contrato_descricao) {
		this.contrato_descricao = contrato_descricao;
	}
	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}
	public void setNome_cliente(String nome_cliente) {
		this.nome_cliente = nome_cliente;
	}
	public void setCdsituacao(Integer cdsituacao) {
		this.cdsituacao = cdsituacao;
	}
	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}
	public void setCdcategoria(Integer cdcategoria) {
		this.cdcategoria = cdcategoria;
	}
	public void setDocumentos(String documentos) {
		this.documentos = documentos;
	}
}