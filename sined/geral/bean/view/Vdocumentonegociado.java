package br.com.linkcom.sined.geral.bean.view;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.sined.geral.bean.Documento;

@Entity
public class Vdocumentonegociado {

	protected Integer cddocumento;
	protected Integer cddocumentonegociado;
	protected Integer cddocumentoacaonegociado;
	protected Date dtvencimentonegociado;
	protected Date dtemissaonegociado;
	
	protected Documento documento;
	protected Documento documentonegociado;
	
	public Vdocumentonegociado() {
	}
	
	public Vdocumentonegociado(Integer cddocumento,
			Integer cddocumentonegociado, Integer cddocumentoacaonegociado,
			Date dtvencimentonegociado, Date dtemissaonegociado) {
		this.cddocumento = cddocumento;
		this.cddocumentonegociado = cddocumentonegociado;
		this.cddocumentoacaonegociado = cddocumentoacaonegociado;
		this.dtvencimentonegociado = dtvencimentonegociado;
		this.dtemissaonegociado = dtemissaonegociado;
	}



	@Id
	public Integer getCddocumento() {
		return cddocumento;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento", insertable=false, updatable=false)
	public Documento getDocumento() {
		return documento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentonegociado", insertable=false, updatable=false)
	public Documento getDocumentonegociado() {
		return documentonegociado;
	}

	public Integer getCddocumentonegociado() {
		return cddocumentonegociado;
	}

	public Integer getCddocumentoacaonegociado() {
		return cddocumentoacaonegociado;
	}

	public Date getDtvencimentonegociado() {
		return dtvencimentonegociado;
	}
	
	public Date getDtemissaonegociado() {
		return dtemissaonegociado;
	}
	
	public void setDtemissaonegociado(Date dtemissaonegociado) {
		this.dtemissaonegociado = dtemissaonegociado;
	}

	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	
	public void setDocumentonegociado(Documento documentonegociado) {
		this.documentonegociado = documentonegociado;
	}

	public void setCddocumentonegociado(Integer cddocumentonegociado) {
		this.cddocumentonegociado = cddocumentonegociado;
	}

	public void setCddocumentoacaonegociado(Integer cddocumentoacaonegociado) {
		this.cddocumentoacaonegociado = cddocumentoacaonegociado;
	}

	public void setDtvencimentonegociado(Date dtvencimentonegociado) {
		this.dtvencimentonegociado = dtvencimentonegociado;
	}

}
