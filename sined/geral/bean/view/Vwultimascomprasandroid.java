package br.com.linkcom.sined.geral.bean.view;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Vwultimascomprasandroid {
	
	private String id;
	private Integer cdpedidovenda;
	private Date dtpedidovenda;
	private Integer cdcliente;
	private Integer cdmaterial;
	private Double quantidade;
	private Integer cdunidademedida;
	private Double preco;
	private Integer cdpedidovendatipo;
	private Integer pedidovendasituacao;
	private Timestamp maxhistorico;
	
	@Id
	public String getId() {
		return id;
	}

	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}

	public Date getDtpedidovenda() {
		return dtpedidovenda;
	}

	public Integer getCdcliente() {
		return cdcliente;
	}

	public Integer getCdmaterial() {
		return cdmaterial;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public Integer getCdunidademedida() {
		return cdunidademedida;
	}

	public Double getPreco() {
		return preco;
	}

	public Integer getCdpedidovendatipo() {
		return cdpedidovendatipo;
	}
	
	public Integer getPedidovendasituacao() {
		return pedidovendasituacao;
	}
	
	public Timestamp getMaxhistorico() {
		return maxhistorico;
	}
	
	public void setMaxhistorico(Timestamp maxhistorico) {
		this.maxhistorico = maxhistorico;
	}
	
	public void setPedidovendasituacao(Integer pedidovendasituacao) {
		this.pedidovendasituacao = pedidovendasituacao;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}

	public void setDtpedidovenda(Date dtpedidovenda) {
		this.dtpedidovenda = dtpedidovenda;
	}

	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}
	
	public void setPreco(Double preco) {
		this.preco = preco;
	}

	public void setCdpedidovendatipo(Integer cdpedidovendatipo) {
		this.cdpedidovendatipo = cdpedidovendatipo;
	}
}
