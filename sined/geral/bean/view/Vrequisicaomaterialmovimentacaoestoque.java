package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.sined.geral.bean.Requisicaomaterial;

@Entity
public class Vrequisicaomaterialmovimentacaoestoque {

	protected Integer cdrequisicaomaterial;
	protected Requisicaomaterial requisicaomaterial;
	protected Double qtde;
	
	public Vrequisicaomaterialmovimentacaoestoque() {
	}
	
	public Vrequisicaomaterialmovimentacaoestoque(Integer cdrequisicaomaterial, Double qtde) {
		this.cdrequisicaomaterial = cdrequisicaomaterial;
		this.qtde = qtde;
	}

	@Id
	public Integer getCdrequisicaomaterial() {
		return cdrequisicaomaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrequisicaomaterial", insertable=false, updatable=false)
	public Requisicaomaterial getRequisicaomaterial() {
		return requisicaomaterial;
	}
	public Double getQtde() {
		return qtde;
	}

	public void setCdrequisicaomaterial(Integer cdrequisicaomaterial) {
		this.cdrequisicaomaterial = cdrequisicaomaterial;
	}
	public void setRequisicaomaterial(Requisicaomaterial requisicaomaterial) {
		this.requisicaomaterial = requisicaomaterial;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
}
