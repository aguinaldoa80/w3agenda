package br.com.linkcom.sined.geral.bean.view;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import br.com.linkcom.neo.types.Money;

@Entity
public class Vanalisecompra {

	protected Integer cdmaterial;
	protected Integer cdempresa;
	protected Integer cdfornecedor;
	protected Date dtentrega;
	protected Date dtentrada;
	protected Money valorunitario;
	protected Date dtcriacaoordemcompra;
	
	protected Money valorunitarioFornecedor;
	protected Money valorultimacompra;
	
	public Vanalisecompra(Integer cdmaterial, Integer cdfornecedor, Money valorultimacompra, 
			Money menorValorMaterial, Money menorValorFornecedor, Date dtcriacaoordemcompra){
		this.cdmaterial = cdmaterial;
		this.cdfornecedor = cdfornecedor;
		this.valorultimacompra = valorultimacompra;
		this.valorunitario = menorValorMaterial;
		this.valorunitarioFornecedor = menorValorFornecedor;
		this.dtcriacaoordemcompra = dtcriacaoordemcompra;
	}
	
	public Vanalisecompra(Integer cdmaterial, Integer cdfornecedor, Date dtentrega, Date dtentrada,
			Double valorunitario, Date dtcriacaoordemcompra) {
		this.cdmaterial = cdmaterial;
		this.cdfornecedor = cdfornecedor;
		this.dtentrega = dtentrega;
		this.dtentrada = dtentrada;
		this.valorunitario = valorunitario != null ? new Money(valorunitario) : null;
		this.dtcriacaoordemcompra = dtcriacaoordemcompra;
	}



	@Id
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Integer getCdempresa() {
		return cdempresa;
	}
	public Integer getCdfornecedor() {
		return cdfornecedor;
	}
	public Date getDtentrega() {
		return dtentrega;
	}
	public Date getDtentrada() {
		return dtentrada;
	}
	public Money getValorunitario() {
		return valorunitario;
	}
	public Date getDtcriacaoordemcompra() {
		return dtcriacaoordemcompra;
	}
	
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}
	public void setCdfornecedor(Integer cdfornecedor) {
		this.cdfornecedor = cdfornecedor;
	}
	public void setDtentrega(Date dtentrega) {
		this.dtentrega = dtentrega;
	}
	public void setDtentrada(Date dtentrada) {
		this.dtentrada = dtentrada;
	}
	public void setValorunitario(Money valorunitario) {
		this.valorunitario = valorunitario;
	}
	public void setDtcriacaoordemcompra(Date dtcriacaoordemcompra) {
		this.dtcriacaoordemcompra = dtcriacaoordemcompra;
	}

	@Transient
	public Money getValorunitarioFornecedor() {
		return valorunitarioFornecedor;
	}
	@Transient
	public Money getValorultimacompra() {
		return valorultimacompra;
	}

	public void setValorunitarioFornecedor(Money valorunitarioFornecedor) {
		this.valorunitarioFornecedor = valorunitarioFornecedor;
	}
	public void setValorultimacompra(Money valorultimacompra) {
		this.valorultimacompra = valorultimacompra;
	}
	
	@Transient
	public Date getDataEntradaOuEntrega(){
		if(dtentrada != null) return dtentrada;
		return dtentrega;
	}
}
