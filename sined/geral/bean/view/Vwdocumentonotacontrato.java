package br.com.linkcom.sined.geral.bean.view;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Vwdocumentonotacontrato {

	protected Integer cddocumento;
	protected Integer cdcontrato;
	protected Date dtvencimento;
	
	
	@Id
	@Column(name="cddocumento", insertable=false, updatable=false)
	public Integer getCddocumento() {
		return cddocumento;
	}
	public Integer getCdcontrato() {
		return cdcontrato;
	}
	public Date getDtvencimento() {
		return dtvencimento;
	}
	
	
	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	public void setCdcontrato(Integer cdcontrato) {
		this.cdcontrato = cdcontrato;
	}
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}
}
