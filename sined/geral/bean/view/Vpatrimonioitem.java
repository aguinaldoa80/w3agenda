package br.com.linkcom.sined.geral.bean.view;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Patrimonioitemsituacao;

@Entity
public class Vpatrimonioitem {

	protected Integer cdpatrimonioitem;
	protected Patrimonioitemsituacao situacao;
	
	@Id
	public Integer getCdpatrimonioitem() {
		return cdpatrimonioitem;
	}
	@DisplayName("Situa��o")
	public Patrimonioitemsituacao getSituacao() {
		return situacao;
	}
	
	public void setCdpatrimonioitem(Integer cdpatrimonioitem) {
		this.cdpatrimonioitem = cdpatrimonioitem;
	}
	public void setSituacao(Patrimonioitemsituacao situacao) {
		this.situacao = situacao;
	}
}
