package br.com.linkcom.sined.geral.bean.view;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.util.Log;

@Entity
public class Vwsaidafiscal implements Log{
	
	protected Integer cdnota;
	protected Integer cdnotatipo;
	protected Integer cdempresa;
	protected String empresanome;
	protected Integer cdcliente;
	protected String clientenome;
	protected Date dtemissao;
	protected String numero;
	protected String especie;
	protected String serie;
	protected Integer cdnotastatus;
	protected String notastatusnome;
	protected String modelo;
	protected Integer naturezaoperacaonfs;
	protected String naturezaoperacaonfp;
	protected Integer indicadortipopagamento;
	protected Money valortotalprodutos;
	protected Money valoroutrasdespesas;
	protected Money valordesconto;
	protected Money valordescontoincondicionado;
	protected Money valordescontocondicionado;
	protected Money valorfrete;
	protected Money valorseguro;
	protected Money valordocumento;
	
	protected Money valorImposto;
	
	protected List<Vwsaidafiscalimposto> listaImposto;
	
	
	public Vwsaidafiscal(){
	}
	
	public Vwsaidafiscal(Integer cdnota, Integer cdnotatipo, Integer cdempresa, String empresanome, Integer cdcliente, String clientenome, 
			Date dtemissao, String numero, String notastatusnome,
			String especie, String serie, Integer cdnotastatus, String modelo, Integer naturezaoperacaonfs, 
			String naturezaoperacaonfp, Integer indicadortipopagamento, Long valortotalproduto, Long valoroutrasdespesas, 
			Money valordesconto, Money valordescontoincondicionado, Money valorfrete, Money valorseguro, Money valordocumento) {
			
		this.cdnota = cdnota;
		this.cdnotatipo = cdnotatipo;
		this.cdempresa = cdempresa ;
		this.empresanome = empresanome;
		this.cdcliente = cdcliente;
		this.clientenome = clientenome;
		this.dtemissao = dtemissao;
		this.numero = numero;
		this.especie = especie;
		this.serie = serie;
		this.cdnotastatus = cdnotastatus;
		this.notastatusnome = notastatusnome;
		this.modelo = modelo;
		
		if(cdnotatipo != null){
			if(cdnotatipo.equals(NotaTipo.NOTA_FISCAL_PRODUTO.getCdNotaTipo())){
				this.naturezaoperacaonfp = naturezaoperacaonfp;
			}else {
				this.naturezaoperacaonfs = naturezaoperacaonfs;
			}
		}
		
		this.indicadortipopagamento = indicadortipopagamento;
		this.valortotalprodutos = valortotalprodutos != null ? new Money(valortotalprodutos) : new Money();
		this.valoroutrasdespesas = valoroutrasdespesas != null ? new Money(valoroutrasdespesas) : new Money();
		this.valordesconto = valordesconto != null ? new Money(valordesconto) : new Money();
		this.valordescontoincondicionado = valordescontoincondicionado!= null ? new Money(valordescontoincondicionado) : new Money();
		this.valorfrete = valorfrete != null ? new Money(valorfrete) : new Money();
		this.valorseguro = valorseguro != null ? new Money(valorseguro) : new Money();
		this.valordocumento = valordocumento != null ? new Money(valordocumento):  new Money();
	}

	@Id
	@Column(name="cdnota", insertable=false, updatable=false)
	public Integer getCdnota() {
		return cdnota;
	}

	public Integer getCdnotatipo() {
		return cdnotatipo;
	}

	public Integer getCdempresa() {
		return cdempresa;
	}

	public Integer getCdcliente() {
		return cdcliente;
	}

	public Date getDtemissao() {
		return dtemissao;
	}

	public String getNumero() {
		return numero;
	}

	public String getEspecie() {
		return especie;
	}

	public String getSerie() {
		return serie;
	}

	public Integer getCdnotastatus() {
		return cdnotastatus;
	}

	public String getModelo() {
		return modelo;
	}

	public Integer getNaturezaoperacaonfs() {
		return naturezaoperacaonfs;
	}

	public String getNaturezaoperacaonfp() {
		return naturezaoperacaonfp;
	}

	public Integer getIndicadortipopagamento() {
		return indicadortipopagamento;
	}

	public Money getValortotalprodutos() {
		return valortotalprodutos;
	}

	public Money getValoroutrasdespesas() {
		return valoroutrasdespesas;
	}

	public Money getValordesconto() {
		return valordesconto;
	}

	public Money getValordescontoincondicionado() {
		return valordescontoincondicionado;
	}

	public Money getValorfrete() {
		return valorfrete;
	}

	public Money getValorseguro() {
		return valorseguro;
	}

	public Money getValordocumento() {
		return valordocumento;
	}

	public void setCdnota(Integer cdnota) {
		this.cdnota = cdnota;
	}

	public void setCdnotatipo(Integer cdnotatipo) {
		this.cdnotatipo = cdnotatipo;
	}

	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}

	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}

	public void setDtemissao(Date dtemissao) {
		this.dtemissao = dtemissao;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setEspecie(String especie) {
		this.especie = especie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public void setCdnotastatus(Integer cdnotastatus) {
		this.cdnotastatus = cdnotastatus;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public void setNaturezaoperacaonfs(Integer naturezaoperacaonfs) {
		this.naturezaoperacaonfs = naturezaoperacaonfs;
	}

	public void setNaturezaoperacaonfp(String naturezaoperacaonfp) {
		this.naturezaoperacaonfp = naturezaoperacaonfp;
	}

	public void setIndicadortipopagamento(Integer indicadortipopagamento) {
		this.indicadortipopagamento = indicadortipopagamento;
	}

	public void setValortotalprodutos(Money valortotalprodutos) {
		this.valortotalprodutos = valortotalprodutos;
	}

	public void setValoroutrasdespesas(Money valoroutrasdespesas) {
		this.valoroutrasdespesas = valoroutrasdespesas;
	}

	public void setValordesconto(Money valordesconto) {
		this.valordesconto = valordesconto;
	}

	public void setValordescontoincondicionado(Money valordescontoincondicionado) {
		this.valordescontoincondicionado = valordescontoincondicionado;
	}

	public void setValorfrete(Money valorfrete) {
		this.valorfrete = valorfrete;
	}

	public void setValorseguro(Money valorseguro) {
		this.valorseguro = valorseguro;
	}

	public void setValordocumento(Money valordocumento) {
		this.valordocumento = valordocumento;
	}

	public String getEmpresanome() {
		return empresanome;
	}

	public String getClientenome() {
		return clientenome;
	}

	@Transient
	public List<Vwsaidafiscalimposto> getListaImposto() {
		return listaImposto;
	}

	public void setEmpresanome(String empresanome) {
		this.empresanome = empresanome;
	}

	public void setClientenome(String clientenome) {
		this.clientenome = clientenome;
	}

	public void setListaImposto(List<Vwsaidafiscalimposto> listaImposto) {
		this.listaImposto = listaImposto;
	}

	public String getNotastatusnome() {
		return notastatusnome;
	}

	public void setNotastatusnome(String notastatusnome) {
		this.notastatusnome = notastatusnome;
	}
	
	@Transient
	public Money getValorImposto() {
		return getValorImposto(BigDecimal.ROUND_HALF_UP);
	}	
	@Transient
	public Money getValorImposto(int round_mode) {
		BigDecimal totalImposto = new BigDecimal(0);
		
		if(this.getListaImposto() != null && !this.getListaImposto().isEmpty()){
			for(Vwsaidafiscalimposto vwsaidafiscalimposto : this.getListaImposto()){
				
				BigDecimal valorInssLocal = (vwsaidafiscalimposto.getValorinss() != null ? vwsaidafiscalimposto.getValorinss().getValue().abs().setScale(2, round_mode) : new BigDecimal(0));
				BigDecimal valorIrLocal = (vwsaidafiscalimposto.getValorir() != null ? vwsaidafiscalimposto.getValorir().getValue().abs().setScale(2, round_mode) : new BigDecimal(0));
				BigDecimal valorIssLocal = (vwsaidafiscalimposto.getValoriss() != null ? vwsaidafiscalimposto.getValoriss().getValue().abs().setScale(2, round_mode) : new BigDecimal(0));
				BigDecimal valorIcmsLocal = (vwsaidafiscalimposto.getValoricms() != null ? vwsaidafiscalimposto.getValoricms().getValue().abs().setScale(2, round_mode) : new BigDecimal(0));
				BigDecimal valorCofinsLocal = (vwsaidafiscalimposto.getValorcofins() != null ? vwsaidafiscalimposto.getValorcofins().getValue().abs().setScale(2, round_mode) : new BigDecimal(0));
				BigDecimal valorPisLocal = (vwsaidafiscalimposto.getValorpis() != null ? vwsaidafiscalimposto.getValorpis().getValue().abs().setScale(2, round_mode) : new BigDecimal(0));
				BigDecimal valorCsllLocal = (vwsaidafiscalimposto.getValorcsll() != null ? vwsaidafiscalimposto.getValorcsll().getValue().abs().setScale(2, round_mode) : new BigDecimal(0));
				
				if (valorInssLocal != null && vwsaidafiscalimposto.getIncideinss() != null && vwsaidafiscalimposto.getIncideinss())
					totalImposto = totalImposto.add(valorInssLocal);
				
				if (valorIrLocal != null && vwsaidafiscalimposto.getIncideir() != null && vwsaidafiscalimposto.getIncideir())
					totalImposto = totalImposto.add(valorIrLocal);
				
				if (valorIssLocal != null && vwsaidafiscalimposto.getIncideiss() != null && vwsaidafiscalimposto.getIncideiss())
					totalImposto = totalImposto.add(valorIssLocal);
				
				if (valorIcmsLocal != null && vwsaidafiscalimposto.getIncideicms() != null && vwsaidafiscalimposto.getIncideicms())
					totalImposto = totalImposto.add(valorIcmsLocal);
				
				if (valorCofinsLocal != null && vwsaidafiscalimposto.getIncidecofins() != null && vwsaidafiscalimposto.getIncidecofins())
					totalImposto = totalImposto.add(valorCofinsLocal);
				
				if (valorPisLocal != null && vwsaidafiscalimposto.getIncidepis() != null && vwsaidafiscalimposto.getIncidepis())
					totalImposto = totalImposto.add(valorPisLocal);
				
				if (valorCsllLocal != null && vwsaidafiscalimposto.getIncidecsll() != null && vwsaidafiscalimposto.getIncidecsll())
					totalImposto = totalImposto.add(valorCsllLocal);
						
				valorImposto = new Money (totalImposto.doubleValue());
			}
		}
		
		return valorImposto;
	}

	public void setValorImposto(Money valorImposto) {
		this.valorImposto = valorImposto;
	}
	
	public Money getValordescontocondicionado() {
		return valordescontocondicionado;
	}

	public void setValordescontocondicionado(Money valordescontocondicionado) {
		this.valordescontocondicionado = valordescontocondicionado;
	}
	
	@Transient
	public Money getValorTotalNota(){
		if((this.valordocumento == null || this.valordocumento.getValue().doubleValue() == 0) && 
				this.getCdnotatipo() != null && NotaTipo.NOTA_FISCAL_SERVICO.getCdNotaTipo().equals(this.getCdnotatipo()) ){
			Money totalNota = new Money(0D);
			
			Money valorTotalServicos = this.getValortotalprodutos();
			if (valorTotalServicos != null)
				totalNota = valorTotalServicos;
			
			Money valorImpostoLocal = this.getValorImposto();
			if (valorImpostoLocal != null)
				totalNota = totalNota.subtract(valorImpostoLocal);
			
			Money outrasRetencoes = this.getValoroutrasdespesas();
			if(outrasRetencoes != null)
				totalNota = totalNota.subtract(outrasRetencoes);
			
			Money descontoincondicionado = this.getValordescontoincondicionado();
			if(descontoincondicionado != null)
				totalNota = totalNota.subtract(descontoincondicionado);
			
			Money descontocondicionado = this.getValordescontocondicionado();
			if(descontocondicionado != null)
				totalNota = totalNota.subtract(descontocondicionado);
			
			return totalNota;
		}
		return valordocumento;
	}

	@Transient
	public Integer getCdusuarioaltera() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public Timestamp getDtaltera() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		// TODO Auto-generated method stub
		
	}
	public void setDtaltera(Timestamp dtaltera) {
		// TODO Auto-generated method stub
		
	}
	
}
