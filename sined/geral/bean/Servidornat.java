package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_servidornat", sequenceName = "sq_servidornat")
public class Servidornat{
	
	protected Integer cdservidornat;
	protected Servidor servidor;
	protected String instrucao;
	protected Boolean ativo = true;
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_servidornat")
	public Integer getCdservidornat() {
		return cdservidornat;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservidor")	
	public Servidor getServidor() {
		return servidor;
	}
	
	@Required
	@DisplayName("Instru��o")
	@MaxLength(2000)
	public String getInstrucao() {
		return instrucao;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}

	public void setCdservidornat(Integer cdservidornat) {
		this.cdservidornat = cdservidornat;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	public void setInstrucao(String instrucao) {
		this.instrucao = instrucao;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
