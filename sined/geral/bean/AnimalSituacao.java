package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@Table(name = "animalsituacao")
@SequenceGenerator(name = "sq_animalsituacao", sequenceName = "sq_animalsituacao")
public class AnimalSituacao implements Log {

	protected Integer cdanimalsituacao;
	protected String nome;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Boolean obito;
	protected Boolean principal;
	
	public AnimalSituacao() {
	}

	@Id
	@GeneratedValue(generator = "sq_animalsituacao", strategy = GenerationType.AUTO)
	@Column(name = "cdanimalsituacao")
	public Integer getCdanimalsituacao() {
		return cdanimalsituacao;
	}

	@Column(name = "nome")
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@Column(name = "obito")
	public Boolean getObito() {
		return obito;
	}
	
	public Boolean getPrincipal(){
		return principal;
	}

	public void setCdanimalsituacao(Integer cdanimalsituacao) {
		this.cdanimalsituacao = cdanimalsituacao;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setObito(Boolean obito) {
		this.obito = obito;
	}
	
	public void setPrincipal(Boolean principal){
		this.principal = principal;
	}
	
}
