package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_mdfeprodutoperigosomdfereferenciado", sequenceName = "sq_mdfeprodutoperigosomdfereferenciado")
public class MdfeProdutoPerigosoMdfeReferenciado {

	protected Integer cdMdfeProdutoPerigosoMdfeReferenciado;
	protected Mdfe mdfe;
	protected String idMdfeReferenciado;
	protected String idProdutoPerigoso; 
	protected String numeroOnuUn;
	protected String classe;
	protected String nomeApropriadoEmbarque;
	protected String grupoEmbalagem;
	protected String quantidadeTotalPorProdutos;
	protected String quantidadeAndTipoPorVolume;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfeprodutoperigosomdfereferenciado")
	public Integer getCdMdfeProdutoPerigosoMdfeReferenciado() {
		return cdMdfeProdutoPerigosoMdfeReferenciado;
	}
	public void setCdMdfeProdutoPerigosoMdfeReferenciado(Integer cdMdfeProdutoPerigoso) {
		this.cdMdfeProdutoPerigosoMdfeReferenciado = cdMdfeProdutoPerigoso;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@DisplayName("ID do MDF-e Referenciado")
	public String getIdMdfeReferenciado() {
		return idMdfeReferenciado;
	}
	public void setIdMdfeReferenciado(String idMdfeReferenciado) {
		this.idMdfeReferenciado = idMdfeReferenciado;
	}
	
	@DisplayName("ID do produto perigoso")
	public String getIdProdutoPerigoso() {
		return idProdutoPerigoso;
	}
	public void setIdProdutoPerigoso(String idProdutoPerigoso) {
		this.idProdutoPerigoso = idProdutoPerigoso;
	}
	
	@Required
	@MaxLength(value=4)
	@DisplayName("N�mero ONU/UN")
	public String getNumeroOnuUn() {
		return numeroOnuUn;
	}
	public void setNumeroOnuUn(String numeroOnuUn) {
		this.numeroOnuUn = numeroOnuUn;
	}
	
	@MaxLength(value=40)
	@DisplayName("Classe ou subclasse/divis�o, e risco subsidi�rio/risco secund�rio")
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	
	@MaxLength(value=150)
	@DisplayName("Nome apropriado para embarque do produto")
	public String getNomeApropriadoEmbarque() {
		return nomeApropriadoEmbarque;
	}
	public void setNomeApropriadoEmbarque(String nomeApropriadoEmbarque) {
		this.nomeApropriadoEmbarque = nomeApropriadoEmbarque;
	}
	
	@MaxLength(value=6)
	@DisplayName("Grupo de embalagem")
	public String getGrupoEmbalagem() {
		return grupoEmbalagem;
	}
	public void setGrupoEmbalagem(String grupoEmbalagem) {
		this.grupoEmbalagem = grupoEmbalagem;
	}
	
	@Required
	@MaxLength(value=20)
	@DisplayName("Quantidade total por produtos")
	public String getQuantidadeTotalPorProdutos() {
		return quantidadeTotalPorProdutos;
	}
	public void setQuantidadeTotalPorProdutos(String quantidadeTotalPorProdutos) {
		this.quantidadeTotalPorProdutos = quantidadeTotalPorProdutos;
	}
	
	@MaxLength(value=60)
	@DisplayName("Quantidade e tipo por volumes")
	public String getQuantidadeAndTipoPorVolume() {
		return quantidadeAndTipoPorVolume;
	}
	public void setQuantidadeAndTipoPorVolume(String quantidadeAndTipoPorVolume) {
		this.quantidadeAndTipoPorVolume = quantidadeAndTipoPorVolume;
	}
}
