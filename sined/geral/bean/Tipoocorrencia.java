package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@DisplayName("Periodicidade de ocorrência dos itens de composição")
public class Tipoocorrencia {

	public static final Tipoocorrencia MENSAL = new Tipoocorrencia(1);
	public static final Tipoocorrencia SEMANAL = new Tipoocorrencia(2);
	public static final Tipoocorrencia INFORMAR = new Tipoocorrencia(3);	
	
	protected Integer cdtipoocorrencia;
	protected String nome;
	
	public Tipoocorrencia() {
		
	}
	
	public Tipoocorrencia(Integer cdtiporelacaorecurso) {
		this.cdtipoocorrencia = cdtiporelacaorecurso;
	}
	
	@Id	
	public Integer getCdtipoocorrencia() {
		return cdtipoocorrencia;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCdtipoocorrencia(Integer cdtipoocorrencia) {
		this.cdtipoocorrencia = cdtipoocorrencia;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Tipoocorrencia){
			Tipoocorrencia that = (Tipoocorrencia) obj;
			return this.getCdtipoocorrencia().equals(that.getCdtipoocorrencia());
		}
		return super.equals(obj);
	}
}
