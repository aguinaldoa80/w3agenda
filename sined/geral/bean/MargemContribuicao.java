package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_margemcontribuicao", sequenceName = "sq_margemcontribuicao")
@DisplayName("Configura��o de Margem de Contribui��o")
public class MargemContribuicao implements Log {

	private Integer cdMargemContribuicao;
	private String nome;
	private Empresa empresa;
	private Boolean gastosEmpresa;
	private Money valorGastosEmpresa;
	private Boolean calcularPontoEquilibrio;
	private Double valorPontoEquilibrioAmarelo;
	private Double valorPontoEquilibrio;
	private Double mediaComissaoMaterial;
	private Boolean padrao;
	
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_margemcontribuicao")
	public Integer getCdMargemContribuicao() {
		return cdMargemContribuicao;
	}
	
	public void setCdMargemContribuicao(Integer cdMargemContribuicao) {
		this.cdMargemContribuicao = cdMargemContribuicao;
	}
	
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@DisplayName("Empresa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@DisplayName("Inserir gastos da empresa")
	public Boolean getGastosEmpresa() {
		return gastosEmpresa;
	}
	
	public void setGastosEmpresa(Boolean gastosEmpresa) {
		this.gastosEmpresa = gastosEmpresa;
	}

	@DisplayName("Gastos da empresa")
	public Money getValorGastosEmpresa() {
		return valorGastosEmpresa;
	}

	public void setValorGastosEmpresa(Money valorGastosEmpresa) {
		this.valorGastosEmpresa = valorGastosEmpresa;
	}

	@DisplayName("Calcular % Ponto de equil�brio")
	public Boolean getCalcularPontoEquilibrio() {
		return calcularPontoEquilibrio;
	}

	public void setCalcularPontoEquilibrio(Boolean calcularPontoEquilibrio) {
		this.calcularPontoEquilibrio = calcularPontoEquilibrio;
	}

	@Required
	@DisplayName("(%) Acima do ponto de equil�brio (Faixa amarela)")
	public Double getValorPontoEquilibrioAmarelo() {
		return valorPontoEquilibrioAmarelo;
	}

	public void setValorPontoEquilibrioAmarelo(Double valorPontoEquilibrioAmarelo) {
		this.valorPontoEquilibrioAmarelo = valorPontoEquilibrioAmarelo;
	}

	@DisplayName("(%) Ponto de equil�brio")
	public Double getValorPontoEquilibrio() {
		return valorPontoEquilibrio;
	}

	public void setValorPontoEquilibrio(Double valorPontoEquilibrio) {
		this.valorPontoEquilibrio = valorPontoEquilibrio;
	}

	@DisplayName("(%) M�dia de comiss�o por material")
	public Double getMediaComissaoMaterial() {
		return mediaComissaoMaterial;
	}
	
	public void setMediaComissaoMaterial(Double mediaComissaoMaterial) {
		this.mediaComissaoMaterial = mediaComissaoMaterial;
	}

	@DisplayName("Configura��o padr�o")
	public Boolean getPadrao() {
		return padrao;
	}

	public void setPadrao(Boolean padrao) {
		this.padrao = padrao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
