package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_operacaocontabilnaturezaoperacao", sequenceName = "sq_operacaocontabilnaturezaoperacao")
public class OperacaoContabilNaturezaOperacao {
	
	protected Integer cdoperacaoContabilNaturezaOperacao;
	protected Naturezaoperacao naturezaoperacao;
	protected Operacaocontabil operacaocontabil;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_operacaocontabilnaturezaoperacao")
	public Integer getCdoperacaoContabilNaturezaOperacao() {
		return cdoperacaoContabilNaturezaOperacao;
	}
	
	public void setCdoperacaoContabilNaturezaOperacao(
			Integer cdoperacaoContabilNaturezaOperacao) {
		this.cdoperacaoContabilNaturezaOperacao = cdoperacaoContabilNaturezaOperacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdoperacaocontabil")
	public Operacaocontabil getOperacaocontabil() {
		return operacaocontabil;
	}
	
	public void setOperacaocontabil(Operacaocontabil operacaocontabil) {
		this.operacaocontabil = operacaocontabil;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	@JoinColumn(name="cdnaturezaoperacao")
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}
	
	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}
	
}
