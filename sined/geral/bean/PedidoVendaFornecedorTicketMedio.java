package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.types.Money;

@Entity
@SequenceGenerator(name="sq_pedidovendafornecedorticketmedio", sequenceName="sq_pedidovendafornecedorticketmedio")
public class PedidoVendaFornecedorTicketMedio {

	private Integer cdPedidoVendaFornecedorTicketMedio;
	private Pedidovenda pedidoVenda;
	private Fornecedor fornecedor;
	private Money valorTicketEsperado;
	private Money valorTicketAtingido;

	
	
	public PedidoVendaFornecedorTicketMedio(){
		
	}
	
	public PedidoVendaFornecedorTicketMedio(Pedidovenda pedidoVenda, Fornecedor fornecedor, Money valorTicketEsperado, Money valorTicketAtingido){
		this.pedidoVenda = pedidoVenda;
		this.fornecedor = fornecedor;
		this.valorTicketEsperado = valorTicketEsperado;
		this.valorTicketAtingido = valorTicketAtingido;
	}
	
	@Id
	@GeneratedValue(generator="sq_pedidovendafornecedorticketmedio",strategy=GenerationType.AUTO)
	public Integer getCdPedidoVendaFornecedorTicketMedio() {
		return cdPedidoVendaFornecedorTicketMedio;
	}
	public void setCdPedidoVendaFornecedorTicketMedio(Integer cdPedidoVendaFornecedorTicketMedio) {
		this.cdPedidoVendaFornecedorTicketMedio = cdPedidoVendaFornecedorTicketMedio;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidoVenda() {
		return pedidoVenda;
	}
	public void setPedidoVenda(Pedidovenda venda) {
		this.pedidoVenda = venda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public Money getValorTicketEsperado() {
		return valorTicketEsperado;
	}
	public void setValorTicketEsperado(Money valorTicketEsperado) {
		this.valorTicketEsperado = valorTicketEsperado;
	}
	public Money getValorTicketAtingido() {
		return valorTicketAtingido;
	}
	public void setValorTicketAtingido(Money valorTicketAtingido) {
		this.valorTicketAtingido = valorTicketAtingido;
	}
}