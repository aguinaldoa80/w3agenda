package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name="sq_ordemcomprahistorico",sequenceName="sq_ordemcomprahistorico")
public class Ordemcomprahistorico implements Log{

	protected Integer cdordemcomprahistorico;
	protected Ordemcompra ordemcompra;
	protected Ordemcompraacao ordemcompraacao;
	protected String observacao;
	protected Fornecedor fornecedor;
	protected Contato contato;
	protected Localarmazenagem localarmazenagem;
	protected Empresa empresa;
	protected Date dtproximaentrega;
	protected Double valor;
	protected Money desconto;
	protected Money frete;
	protected Prazopagamento prazopagamento;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
//	TRANSIENTE
	protected Money total;
	
	public Ordemcomprahistorico(){
	}
	
	public Ordemcomprahistorico(Ordemcompra ordemcompra, Ordemcompraacao ordemcompraacao){
		this.ordemcompra = ordemcompra;
		this.ordemcompraacao = ordemcompraacao;
		this.dtaltera = new Timestamp(System.currentTimeMillis());
		this.cdusuarioaltera = ((Pessoa)Neo.getUser()).getCdpessoa();
	}
	
	public Ordemcomprahistorico(Ordemcompra ordemcompra, Ordemcompraacao ordemcompraacao, Ordemcompra antiga){
		this.ordemcompra = ordemcompra;
		this.ordemcompraacao = ordemcompraacao;
		this.dtaltera = new Timestamp(System.currentTimeMillis());
		this.cdusuarioaltera = ((Pessoa)Neo.getUser()).getCdpessoa();
		
		this.fornecedor = antiga.getFornecedor();
		this.contato = antiga.getContato();
		this.localarmazenagem = antiga.getLocalarmazenagem();
		this.empresa = antiga.getEmpresa();
		this.dtproximaentrega = antiga.getDtproximaentrega();
		this.valor = antiga.getValorProdutosAtualizado();
		this.desconto = antiga.getDesconto();
		this.frete = antiga.getFrete();
		this.prazopagamento = antiga.getPrazopagamento();
	}
	
	public Ordemcomprahistorico(Ordemcompra ordemcompra, Ordemcompraacao ordemcompraacao, String observacao){
		this.ordemcompra = ordemcompra;
		this.ordemcompraacao = ordemcompraacao;
		this.observacao = observacao;
		this.dtaltera = new Timestamp(System.currentTimeMillis());
		this.cdusuarioaltera = ((Pessoa)Neo.getUser()).getCdpessoa();
	}
	
	public Ordemcomprahistorico(String ids){
		this.ids = ids;
	}
	
	@Id
	@GeneratedValue(generator="sq_ordemcomprahistorico",strategy=GenerationType.AUTO)
	public Integer getCdordemcomprahistorico() {
		return cdordemcomprahistorico;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemcompra")
	public Ordemcompra getOrdemcompra() {
		return ordemcompra;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemcompraacao")
	@DisplayName("A��o")
	public Ordemcompraacao getOrdemcompraacao() {
		return ordemcompraacao;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@DisplayName("Data de altera��o")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontato")
	public Contato getContato() {
		return contato;
	}

	@DisplayName("Local de entrega")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	@DisplayName("Pr�xima entrega")
	public Date getDtproximaentrega() {
		return dtproximaentrega;
	}

	public Double getValor() {
		return valor;
	}

	public Money getDesconto() {
		return desconto;
	}

	public Money getFrete() {
		return frete;
	}

	@DisplayName("Prazo de pgto.")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprazopagamento")
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}

	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setDtproximaentrega(Date dtproximaentrega) {
		this.dtproximaentrega = dtproximaentrega;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}

	public void setFrete(Money frete) {
		this.frete = frete;
	}

	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}

	public void setCdordemcomprahistorico(Integer cdordemcomprahistorico) {
		this.cdordemcomprahistorico = cdordemcomprahistorico;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setOrdemcompra(Ordemcompra ordemcompra) {
		this.ordemcompra = ordemcompra;
	}
	public void setOrdemcompraacao(Ordemcompraacao ordemcompraacao) {
		this.ordemcompraacao = ordemcompraacao;
	}
	
	//==================== TRANSIENT'S ==================================
	
	protected String ids;
	protected String controller;
	
	@Transient
	public String getIds() {
		return ids;
	}
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	@Transient
	public String getController() {
		return controller;
	}
	
	public void setIds(String ids) {
		this.ids = ids;
	}
	
	@Transient
	@DisplayName("Valor total")
	public Double getTotal() {
		return valor + (this.getFrete() != null ? this.getFrete().getValue().doubleValue() : 0.0) - (this.getDesconto() != null ? this.getDesconto().getValue().doubleValue() : 0.0);
	}
	
	public void setTotal(Money total) {
		this.total = total;
	}
	public void setController(String controller) {
		this.controller = controller;
	}
	
}
