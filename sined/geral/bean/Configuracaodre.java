package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_configuracaodre", sequenceName = "sq_configuracaodre")
public class Configuracaodre implements Log{

	protected Integer cdconfiguracaodre;
	protected String descricao;
	protected Boolean ativo = Boolean.TRUE;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected String receitaliquida_label;
	protected String resultadobruto_label;
	protected String resultadoliquidoantesimpostos_label;
	protected String resultadoliquido_label;
	
	protected List<Configuracaodreimpostofinal> listaConfiguracaodreimpostofinal = new ListSet<Configuracaodreimpostofinal>(Configuracaodreimpostofinal.class);
	protected List<Configuracaodreitem> listaConfiguracaodreitem = new ListSet<Configuracaodreitem>(Configuracaodreitem.class);

	// TRANSIENTS
	protected Map<Integer, List<Configuracaodreitem>> mapConfiguracaodreitem = new HashMap<Integer, List<Configuracaodreitem>>();
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_configuracaodre")
	public Integer getCdconfiguracaodre() {
		return cdconfiguracaodre;
	}
	
	@Required
	@DescriptionProperty
	@MaxLength(200)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@Required
	public Boolean getAtivo() {
		return ativo;
	}
	
	@DisplayName("Impostos da DRE")
	@OneToMany(mappedBy="configuracaodre", fetch=FetchType.LAZY)
	public List<Configuracaodreimpostofinal> getListaConfiguracaodreimpostofinal() {
		return listaConfiguracaodreimpostofinal;
	}
	
	@OneToMany(mappedBy="configuracaodre", fetch=FetchType.LAZY)
	public List<Configuracaodreitem> getListaConfiguracaodreitem() {
		return listaConfiguracaodreitem;
	}
	
	@MaxLength(100)
	@DisplayName("Receita L�quida")
	public String getReceitaliquida_label() {
		return receitaliquida_label;
	}

	@MaxLength(100)
	@DisplayName("Lucro Bruto")
	public String getResultadobruto_label() {
		return resultadobruto_label;
	}

	@MaxLength(100)
	@DisplayName("Resultado antes Impostos")
	public String getResultadoliquidoantesimpostos_label() {
		return resultadoliquidoantesimpostos_label;
	}

	@MaxLength(100)
	@DisplayName("Resultado L�quido")
	public String getResultadoliquido_label() {
		return resultadoliquido_label;
	}

	public void setReceitaliquida_label(String receitaliquida_label) {
		this.receitaliquida_label = receitaliquida_label;
	}

	public void setResultadobruto_label(String resultadobruto_label) {
		this.resultadobruto_label = resultadobruto_label;
	}

	public void setResultadoliquidoantesimpostos_label(
			String resultadoliquidoantesimpostos_label) {
		this.resultadoliquidoantesimpostos_label = resultadoliquidoantesimpostos_label;
	}

	public void setResultadoliquido_label(String resultadoliquido_label) {
		this.resultadoliquido_label = resultadoliquido_label;
	}

	public void setListaConfiguracaodreitem(
			List<Configuracaodreitem> listaConfiguracaodreitem) {
		this.listaConfiguracaodreitem = listaConfiguracaodreitem;
	}
	
	public void setListaConfiguracaodreimpostofinal(
			List<Configuracaodreimpostofinal> listaConfiguracaodreimpostofinal) {
		this.listaConfiguracaodreimpostofinal = listaConfiguracaodreimpostofinal;
	}

	public void setCdconfiguracaodre(Integer cdconfiguracaodre) {
		this.cdconfiguracaodre = cdconfiguracaodre;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public Map<Integer, List<Configuracaodreitem>> getMapConfiguracaodreitem() {
		return mapConfiguracaodreitem;
	}
	
	public void setMapConfiguracaodreitem(
			Map<Integer, List<Configuracaodreitem>> mapConfiguracaodreitem) {
		this.mapConfiguracaodreitem = mapConfiguracaodreitem;
	}
	
}
