package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_materialkitflexivelformula", sequenceName = "sq_materialkitflexivelformula")
public class Materialkitflexivelformula {

	protected Integer cdmaterialkitflexivelformula;
	protected Materialkitflexivel materialkitflexivel;
	protected Integer ordem;
	protected String identificador;
	protected String formula;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialkitflexivelformula")
	public Integer getCdmaterialkitflexivelformula() {
		return cdmaterialkitflexivelformula;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialkitflexivel")
	public Materialkitflexivel getMaterialkitflexivel() {
		return materialkitflexivel;
	}
	@Required
	public Integer getOrdem() {
		return ordem;
	}
	@Required
	@MaxLength(5)
	public String getIdentificador() {
		return identificador;
	}
	@Required
	@DisplayName("F�rmula")
	public String getFormula() {
		return formula;
	}
	
	public void setCdmaterialkitflexivelformula(Integer cdmaterialkitflexivelformula) {
		this.cdmaterialkitflexivelformula = cdmaterialkitflexivelformula;
	}
	public void setMaterialkitflexivel(Materialkitflexivel materialkitflexivel) {
		this.materialkitflexivel = materialkitflexivel;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setFormula(String formula) {
		this.formula = formula;
	}
}
