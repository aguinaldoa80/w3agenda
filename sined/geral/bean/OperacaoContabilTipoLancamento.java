package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoLancamentoEnum;
import br.com.linkcom.sined.util.Log;

@Entity
public class OperacaoContabilTipoLancamento implements Log {
	public static final OperacaoContabilTipoLancamento CONTA_GERENCIAL_MOVIMENTACAO = new OperacaoContabilTipoLancamento(1, "Conta gerencial (movimenta��o)", MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_MOVIMENTACAO);
	public static final OperacaoContabilTipoLancamento EMISSAO_NF = new OperacaoContabilTipoLancamento(2, "Emiss�o de nota fiscal", MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF);
	public static final OperacaoContabilTipoLancamento RECOLHIMENTO_ISS_CLIENTE = new OperacaoContabilTipoLancamento(3, "Recolhimento de ISS pelo cliente", MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE);
	public static final OperacaoContabilTipoLancamento CONTA_RECEBER_BAIXADA_DINHEIRO = new OperacaoContabilTipoLancamento(4, "Conta a receber baixada (Dinheiro)", MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_DINHEIRO);
	public static final OperacaoContabilTipoLancamento CONTA_RECEBER_BAIXADA_CHEQUE = new OperacaoContabilTipoLancamento(5, "Conta a receber baixada (Cheque)", MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_CHEQUE);
	public static final OperacaoContabilTipoLancamento CONTA_RECEBER_BAIXADA_CARTAO = new OperacaoContabilTipoLancamento(6, "Conta a receber baixada (Cart�o)", MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_CARTAO);
	public static final OperacaoContabilTipoLancamento CONTA_RECEBER_BAIXADA_RETORNO_BANCARIO = new OperacaoContabilTipoLancamento(7, "Conta a receber baixada (Retorno banc�rio)", MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_RETORNO_BANCARIO);
	public static final OperacaoContabilTipoLancamento CONCILIACAO_CARTAO = new OperacaoContabilTipoLancamento(8, "Concilia��o de cart�o", MovimentacaocontabilTipoLancamentoEnum.CONCILIACAO_CARTAO);
	public static final OperacaoContabilTipoLancamento CREDITO_GERADO_RETORNO_BANCARIO = new OperacaoContabilTipoLancamento(9, "Cr�dito gerado pelo retorno banc�rio", MovimentacaocontabilTipoLancamentoEnum.CREDITO_GERADO_RETORNO_BANCARIO);
	public static final OperacaoContabilTipoLancamento JUROS_RECEBIDOS = new OperacaoContabilTipoLancamento(10, "Juros recebidos", MovimentacaocontabilTipoLancamentoEnum.JUROS_RECEBIDOS);
	public static final OperacaoContabilTipoLancamento DESCONTOS_CONCEDIDOS = new OperacaoContabilTipoLancamento(11, "Descontos concedidos", MovimentacaocontabilTipoLancamentoEnum.DESCONTOS_CONCEDIDOS);
	public static final OperacaoContabilTipoLancamento REGISTROS_ENTRADA_FISCAL = new OperacaoContabilTipoLancamento(12, "Registro de entrada fiscal", MovimentacaocontabilTipoLancamentoEnum.REGISTROS_ENTRADA_FISCAL);
	public static final OperacaoContabilTipoLancamento CONTA_PAGAR_BAIXADA_FORNECEDOR = new OperacaoContabilTipoLancamento(13, "Conta a pagar baixada (Fornecedor)", MovimentacaocontabilTipoLancamentoEnum.CONTA_PAGAR_BAIXADA_FORNECEDOR);
	public static final OperacaoContabilTipoLancamento JUROS_PAGOS = new OperacaoContabilTipoLancamento(14, "Juros pagos", MovimentacaocontabilTipoLancamentoEnum.JUROS_PAGOS);
	public static final OperacaoContabilTipoLancamento DESCONTOS_OBTIDOS = new OperacaoContabilTipoLancamento(15, "Descontos obtidos", MovimentacaocontabilTipoLancamentoEnum.DESCONTOS_OBTIDOS);
	public static final OperacaoContabilTipoLancamento DEPOSITO = new OperacaoContabilTipoLancamento(16, "Dep�sito", MovimentacaocontabilTipoLancamentoEnum.DEPOSITO);
	public static final OperacaoContabilTipoLancamento DEPOSITO_CHEQUE = new OperacaoContabilTipoLancamento(17, "Dep�sito de cheque", MovimentacaocontabilTipoLancamentoEnum.DEPOSITO_CHEQUE);
	public static final OperacaoContabilTipoLancamento SAQUE = new OperacaoContabilTipoLancamento(18, "Saque", MovimentacaocontabilTipoLancamentoEnum.SAQUE);
	public static final OperacaoContabilTipoLancamento SAQUE_CHEQUE = new OperacaoContabilTipoLancamento(19, "Saque com cheque", MovimentacaocontabilTipoLancamentoEnum.SAQUE_CHEQUE);
	public static final OperacaoContabilTipoLancamento TRANSFERENCIA_ENTRE_CONTAS = new OperacaoContabilTipoLancamento(20, "Transfer�ncia entre contas", MovimentacaocontabilTipoLancamentoEnum.TRANSFERENCIA_ENTRE_CONTAS);
	public static final OperacaoContabilTipoLancamento CONTA_RECEBER_BAIXADA_CREDITO_CONTA_CORRENTE = new OperacaoContabilTipoLancamento(21, "Conta a receber baixada (Cr�dito em conta corrente)", MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_CREDITO_CONTA_CORRENTE);
	public static final OperacaoContabilTipoLancamento VENDA = new OperacaoContabilTipoLancamento(22, "Venda", MovimentacaocontabilTipoLancamentoEnum.VENDA);
	public static final OperacaoContabilTipoLancamento CONTA_GERENCIAL_CONTA_PAGAR_RECEBER = new OperacaoContabilTipoLancamento(23, "Conta gerencial (conta a pagar/receber)", MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_CONTA_PAGAR_RECEBER);
	public static final OperacaoContabilTipoLancamento TRANSFERENCIA_ENTRE_CAIXAS = new OperacaoContabilTipoLancamento(24, "Transfer�ncia entre caixas", MovimentacaocontabilTipoLancamentoEnum.TRANSFERENCIA_ENTRE_CAIXAS);
	public static final OperacaoContabilTipoLancamento CONTA_RECEBER_BAIXADA_PARCIAL = new OperacaoContabilTipoLancamento(25, "Baixa parcial de conta a receber", MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_PARCIAL);
	public static final OperacaoContabilTipoLancamento MOVIMENTACAO_ESTOQUE_ORIGEM = new OperacaoContabilTipoLancamento(26, "Movimenta��o de estoque (origem)", MovimentacaocontabilTipoLancamentoEnum.MOVIMENTACAO_ESTOQUE_ORIGEM);
	public static final OperacaoContabilTipoLancamento CONTA_GERENCIAL_MOVIMENTACAO_ESTOQUE = new OperacaoContabilTipoLancamento(27, "Conta gerencial (movimenta��o de estoque)", MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_MOVIMENTACAO_ESTOQUE);
	public static final OperacaoContabilTipoLancamento CONTA_RECEBER_BAIXADA_ADIANTAMENTO = new OperacaoContabilTipoLancamento(28, "Adiantamento (Contas a receber)", MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_ADIANTAMENTO);
	public static final OperacaoContabilTipoLancamento CONTA_PAGAR_BAIXADA_ADIANTAMENTO = new OperacaoContabilTipoLancamento(29, "Adiantamento (Contas a pagar)", MovimentacaocontabilTipoLancamentoEnum.CONTA_PAGAR_BAIXADA_ADIANTAMENTO);
	
    public static final OperacaoContabilTipoLancamento RECOLHIMENTO_IR = new OperacaoContabilTipoLancamento(30, "Recolhimento de IR", MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_IR);
    public static final OperacaoContabilTipoLancamento RECOLHIMENTO_CSLL = new OperacaoContabilTipoLancamento(31, "Recolhimento de CSLL", MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_CSLL);
    public static final OperacaoContabilTipoLancamento RECOLHIMENTO_INSS = new OperacaoContabilTipoLancamento(32, "Recolhimento de INSS", MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_INSS);
    public static final OperacaoContabilTipoLancamento RECOLHIMENTO_PIS = new OperacaoContabilTipoLancamento(33, "Recolhimento de PIS", MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_PIS);
    public static final OperacaoContabilTipoLancamento RECOLHIMENTO_COFINS = new OperacaoContabilTipoLancamento(34, "Recolhimento de COFINS", MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_COFINS);
    public static final OperacaoContabilTipoLancamento RECOLHIMENTO_ICMS = new OperacaoContabilTipoLancamento(35, "Recolhimento de ICMS", MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ICMS);
    public static final OperacaoContabilTipoLancamento RECOLHIMENTO_II = new OperacaoContabilTipoLancamento(36, "Recolhimento de II", MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_II);
    public static final OperacaoContabilTipoLancamento RECOLHIMENTO_IPI = new OperacaoContabilTipoLancamento(37, "Recolhimento de IPI", MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_IPI);
    public static final OperacaoContabilTipoLancamento RECOLHIMENTO_ISS = new OperacaoContabilTipoLancamento(38, "Recolhimento de ISS", MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS);
    public static final OperacaoContabilTipoLancamento PROVISIONAMENTO = new OperacaoContabilTipoLancamento(39, "Provisionamento", MovimentacaocontabilTipoLancamentoEnum.PROVISIONAMENTO);
    public static final OperacaoContabilTipoLancamento DESPESA_COLABORADOR = new OperacaoContabilTipoLancamento(40, "Despesas do colaborador", MovimentacaocontabilTipoLancamentoEnum.DESPESA_COLABORADOR);
    public static final OperacaoContabilTipoLancamento REGISTROS_ENTRADA_FISCAL_COM_APROPRIACAO = new OperacaoContabilTipoLancamento(41, "Registro de entrada fiscal com apropria��o", MovimentacaocontabilTipoLancamentoEnum.REGISTROS_ENTRADA_FISCAL_COM_APROPRIACAO);
    public static final OperacaoContabilTipoLancamento CONTA_RECEBER_BAIXADA_POR_FORMA_PAGAMENTO  = new OperacaoContabilTipoLancamento(42, "Conta a receber baixada por forma de pagamento", MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_POR_FORMA_PAGAMENTO);
    
	private Integer cdOperacaoContabilTipoLancamento;
	private String nome;
	private MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento;
	private Integer ordem;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	public OperacaoContabilTipoLancamento() {
		
	}
	
	public OperacaoContabilTipoLancamento(Integer cdOperacaoContabilTipoLancamento, String nome, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento){
		this.cdOperacaoContabilTipoLancamento = cdOperacaoContabilTipoLancamento;
		this.nome = nome;
		this.movimentacaocontabilTipoLancamento = movimentacaocontabilTipoLancamento;
	}
	
	@Id
	public Integer getCdOperacaoContabilTipoLancamento() {
		return cdOperacaoContabilTipoLancamento;
	}
	
	@DescriptionProperty
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	
	public MovimentacaocontabilTipoLancamentoEnum getMovimentacaocontabilTipoLancamento() {
		return movimentacaocontabilTipoLancamento;
	}
	
	@Required
	@DisplayName("Ordem")
	public Integer getOrdem() {
		return ordem;
	}

	public void setCdOperacaoContabilTipoLancamento(Integer cdOperacaoContabilTipoLancamento) {
		this.cdOperacaoContabilTipoLancamento = cdOperacaoContabilTipoLancamento;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setMovimentacaocontabilTipoLancamento(MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento) {
		this.movimentacaocontabilTipoLancamento = movimentacaocontabilTipoLancamento;
	}
	
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdOperacaoContabilTipoLancamento == null) ? 0
						: cdOperacaoContabilTipoLancamento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OperacaoContabilTipoLancamento other = (OperacaoContabilTipoLancamento) obj;
		if (cdOperacaoContabilTipoLancamento == null) {
			if (other.cdOperacaoContabilTipoLancamento != null)
				return false;
		} else if (!cdOperacaoContabilTipoLancamento
				.equals(other.cdOperacaoContabilTipoLancamento))
			return false;
		return true;
	}

	public static List<OperacaoContabilTipoLancamento> getLista() {
		return Arrays.asList(
			EMISSAO_NF,
			RECOLHIMENTO_ISS_CLIENTE,
			REGISTROS_ENTRADA_FISCAL,
			REGISTROS_ENTRADA_FISCAL_COM_APROPRIACAO,
			CONTA_RECEBER_BAIXADA_ADIANTAMENTO,
			CONTA_RECEBER_BAIXADA_PARCIAL,
			CONTA_RECEBER_BAIXADA_DINHEIRO,
			CONTA_RECEBER_BAIXADA_CHEQUE,
			CONTA_RECEBER_BAIXADA_CARTAO,
			CONTA_RECEBER_BAIXADA_RETORNO_BANCARIO,
			CONTA_PAGAR_BAIXADA_ADIANTAMENTO,
			CONTA_PAGAR_BAIXADA_FORNECEDOR,
			JUROS_RECEBIDOS,
			DESCONTOS_CONCEDIDOS,
			JUROS_PAGOS,
			DESCONTOS_OBTIDOS,
			CONCILIACAO_CARTAO,
			CONTA_RECEBER_BAIXADA_CREDITO_CONTA_CORRENTE,
			DEPOSITO,
			DEPOSITO_CHEQUE,
			SAQUE,
			SAQUE_CHEQUE,
			TRANSFERENCIA_ENTRE_CONTAS,
			TRANSFERENCIA_ENTRE_CAIXAS,
			CREDITO_GERADO_RETORNO_BANCARIO,
			CONTA_GERENCIAL_MOVIMENTACAO,
			CONTA_GERENCIAL_CONTA_PAGAR_RECEBER,
			CONTA_GERENCIAL_MOVIMENTACAO_ESTOQUE,
			VENDA,
			MOVIMENTACAO_ESTOQUE_ORIGEM,
            RECOLHIMENTO_IR,
            RECOLHIMENTO_CSLL,
            RECOLHIMENTO_INSS,
            RECOLHIMENTO_PIS,
            RECOLHIMENTO_COFINS,
            RECOLHIMENTO_ICMS,
            RECOLHIMENTO_ISS,
            RECOLHIMENTO_II,
            RECOLHIMENTO_IPI,
            PROVISIONAMENTO,
            DESPESA_COLABORADOR,
            CONTA_RECEBER_BAIXADA_POR_FORMA_PAGAMENTO
		);
	}
	
	public static List<OperacaoContabilTipoLancamento> getListaOrdenada(List<OperacaoContabilTipoLancamento> listaTipoLancamento) {
		List<OperacaoContabilTipoLancamento> lista = new ArrayList<OperacaoContabilTipoLancamento>();
		lista.addAll(listaTipoLancamento);
		
		Collections.sort(lista, new Comparator<OperacaoContabilTipoLancamento>(){
			public int compare(OperacaoContabilTipoLancamento o1, OperacaoContabilTipoLancamento o2) {
				return o1.getOrdem().compareTo(o2.getOrdem());
			}
		});
		
		return lista;
	}
	

	@Transient
	public static OperacaoContabilTipoLancamento getByOrdinal(Integer codigo){
		if(codigo != null){
			for (OperacaoContabilTipoLancamento tipoLancamento : getLista()){
				if(codigo.equals(tipoLancamento.getCdOperacaoContabilTipoLancamento())){
					return tipoLancamento;
				}
			}
		}
		
		return null;	
	}

	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
