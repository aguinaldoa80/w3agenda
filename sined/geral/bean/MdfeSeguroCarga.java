package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.ResponsavelSeguro;

@Entity
@SequenceGenerator(name = "sq_mdfesegurocarga", sequenceName = "sq_mdfesegurocarga")
public class MdfeSeguroCarga {

	protected Integer cdMdfeSeguroCarga;
	protected Mdfe mdfe;
	protected String idSeguroCarga;
	protected ResponsavelSeguro responsavelSeguro;
	protected Tipopessoa tipoPessoa;
	protected Cpf cpf;
	protected Cnpj cnpj;
	protected String nomeSeguradora;
	protected Cnpj cnpjSeguradora;
	protected String numeroApolice;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfesegurocarga")
	public Integer getCdMdfeSeguroCarga() {
		return cdMdfeSeguroCarga;
	}
	public void setCdMdfeSeguroCarga(Integer cdMdfeSeguroCarga) {
		this.cdMdfeSeguroCarga = cdMdfeSeguroCarga;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@DisplayName("ID do Seguro de Carga")
	public String getIdSeguroCarga() {
		return idSeguroCarga;
	}
	public void setIdSeguroCarga(String idSeguroCarga) {
		this.idSeguroCarga = idSeguroCarga;
	}
	
	@Required
	@DisplayName("Respons�vel pelo seguro")
	public ResponsavelSeguro getResponsavelSeguro() {
		return responsavelSeguro;
	}
	public void setResponsavelSeguro(ResponsavelSeguro responsavelSeguro) {
		this.responsavelSeguro = responsavelSeguro;
	}
	
	@DisplayName("Tipo de pessoa")
	public Tipopessoa getTipoPessoa() {
		return tipoPessoa;
	}
	public void setTipoPessoa(Tipopessoa tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}
	
	public Cpf getCpf() {
		return cpf;
	}
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	
	public Cnpj getCnpj() {
		return cnpj;
	}
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
	
	@Required
	@MaxLength(value=30)
	@DisplayName("Nome da Seguradora")
	public String getNomeSeguradora() {
		return nomeSeguradora;
	}
	public void setNomeSeguradora(String nomeSeguradora) {
		this.nomeSeguradora = nomeSeguradora;
	}
	
	@Required
	@DisplayName("CNPJ")
	public Cnpj getCnpjSeguradora() {
		return cnpjSeguradora;
	}
	public void setCnpjSeguradora(Cnpj cnpjSeguradora) {
		this.cnpjSeguradora = cnpjSeguradora;
	}
	
	@MaxLength(value=20)
	@DisplayName("N�mero da Ap�lice")
	public String getNumeroApolice() {
		return numeroApolice;
	}
	public void setNumeroApolice(String numeroApolice) {
		this.numeroApolice = numeroApolice;
	}
}