package br.com.linkcom.sined.geral.bean;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_requisicaomaterial;
import br.com.linkcom.sined.geral.bean.auxiliar.ValidateLoteestoqueObrigatorioInterface;
import br.com.linkcom.sined.geral.bean.view.Vrequisicaomaterialentrega;
import br.com.linkcom.sined.geral.bean.view.Vrequisicaomaterialmovimentacaoestoque;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@Entity
@SequenceGenerator(name = "sq_requisicaomaterial", sequenceName = "sq_requisicaomaterial")
@DisplayName("Requisi��o de material")
@JoinEmpresa("requisicaomaterial.empresa")
public class Requisicaomaterial implements Log, PermissaoProjeto, ValidateLoteestoqueObrigatorioInterface{

	protected Integer cdrequisicaomaterial;
	protected List<Vrequisicaomaterialentrega> listaVrequisicaomaterialentrega;
	protected Vrequisicaomaterialmovimentacaoestoque vrequisicaomaterialmovimentacaoestoque;
	protected Integer identificador; 
	protected Integer cdUsuarioAltera;
	protected Double qtde;
	protected Frequencia frequencia;
	protected Double qtdefrequencia;
	protected Timestamp dtAltera;
	protected String descricao;
	protected String observacao;
	protected Date dtlimite;
	protected Departamento departamento;
	protected Material material;
	protected Materialclasse materialclasse;
	protected Localarmazenagem localarmazenagem;
	protected Requisicaoorigem requisicaoorigem;
	protected Centrocusto centrocusto;
	protected Projeto projeto;
	protected Empresa empresa;
	protected Colaborador colaborador;
	protected Arquivo arquivo;
	protected Double altura;
	protected Double largura;
	protected Double comprimento;
	protected Double pesototal;
	protected Double qtdvolume;
	
	protected Date dtcancelamento;
	protected Date dtautorizacao;
	protected Date dtbaixa;
	protected Date dtromaneiocompleto;
	protected Aux_requisicaomaterial aux_requisicaomaterial;
	
	protected Set<Solicitacaocompraorigem> listaSolicitacaocompraorigem;
	
	protected Date dtcriacao;
	
	//TRANSIENT
	protected Entrega entrega;
	protected Integer cdentrega;
	protected Unidademedida unidademedidatrans;
	protected Material materialmestregrade;
	protected List<Requisicaomaterial> listaMaterialitemmestregrade;
	protected String whereInCdpedidovenda;
	protected Integer cdpedidovenda;
	protected Integer cdproducaoagenda;
	protected Boolean romaneioGerado = Boolean.FALSE;
	protected Integer cdProducaoagenda;
	protected Double qtdeEntregue = 0d;
	protected Boolean isMateriaprimaProducaoagenda;
	protected Boolean gerado;
	protected Integer cdplanejamentorecursogeral;
	protected Planejamento planejamento;
	protected Boolean copiar;
	protected Loteestoque loteestoque;
	protected Boolean somenteBaixa;
	
	public Requisicaomaterial(){
	}
	
	public Requisicaomaterial(Integer cdrequisicaomaterial){
		this.cdrequisicaomaterial = cdrequisicaomaterial;
	}
	
	public Requisicaomaterial(Integer cdrequisicaomaterial, Departamento departamento, Colaborador colaborador, Entrega entrega, Material material){
		this.cdrequisicaomaterial = cdrequisicaomaterial;
		this.departamento = departamento;
		this.colaborador = colaborador;
		this.entrega = entrega;
		this.material = material;
	}
	
	public Requisicaomaterial(Integer cdrequisicaomaterial, Integer identificador, Colaborador colaborador, Integer cdentrega){
		this.cdrequisicaomaterial = cdrequisicaomaterial;
		this.identificador = identificador;
		this.colaborador = colaborador;
		this.cdentrega = cdentrega;
	}
	
	public Requisicaomaterial(Integer cdrequisicaomaterial, Material material, Colaborador colaborador, Localarmazenagem localarmazenagem, Empresa empresa){
		this.cdrequisicaomaterial = cdrequisicaomaterial;
		this.material = material;
		this.colaborador = colaborador;
		this.localarmazenagem = localarmazenagem;
		this.empresa = empresa;
	}

	public Requisicaomaterial(Integer cdrequisicaomaterial, Integer identificador, Material material, Colaborador colaborador, Localarmazenagem localarmazenagem, Empresa empresa){
		this.cdrequisicaomaterial = cdrequisicaomaterial;
		this.identificador = identificador;
		this.material = material;
		this.colaborador = colaborador;
		this.localarmazenagem = localarmazenagem;
		this.empresa = empresa;
	}
	
	public Requisicaomaterial(String historico, Date dtlimite, Date dtcriacao, Projeto projeto, Departamento departamento, Localarmazenagem localarmazenagem, Centrocusto centrocusto, 
							  Empresa empresa, Colaborador colaborador, Material material, Materialclasse materialclasse, Double qtde, Requisicaoorigem requisicaoorigem, 
							  Integer identificador, String observacao, Frequencia frequencia, Double qtdefrequnecia, Arquivo arquivo,
							  Double altura, Double largura, Double comprimento, Double pesototal, Double qtdvolume){
		this.descricao = historico;
		this.dtlimite = dtlimite;
		this.dtcriacao = dtcriacao;
		this.projeto = projeto;
		this.departamento = departamento;
		this.localarmazenagem = localarmazenagem;
		this.centrocusto = centrocusto;
		this.empresa = empresa;
		this.colaborador = colaborador;
		this.material = material;
		this.materialclasse = materialclasse;
		this.qtde = qtde;
		this.requisicaoorigem = requisicaoorigem;
		this.identificador = identificador;
		this.observacao = observacao;
		this.frequencia = frequencia;
		this.qtdefrequencia = qtdefrequnecia;
		this.arquivo = arquivo;		
		this.altura = altura;
		this.largura = largura;
		this.comprimento = comprimento;
		this.pesototal = pesototal;
		this.qtdvolume = qtdvolume;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_requisicaomaterial")
	@DisplayName("C�d. requisi��o")
	public Integer getCdrequisicaomaterial() {
		return cdrequisicaomaterial;
	}
	@OneToMany(mappedBy="requisicaomaterial")
	public List<Vrequisicaomaterialentrega> getListaVrequisicaomaterialentrega() {
		return listaVrequisicaomaterialentrega;
	}
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrequisicaomaterial", insertable=false, updatable=false)
	public Vrequisicaomaterialmovimentacaoestoque getVrequisicaomaterialmovimentacaoestoque() {
		return vrequisicaomaterialmovimentacaoestoque;
	}
	@Required
	@DisplayName("Quantidade")
	public Double getQtde() {
		return qtde;
	}
	@Required
	@DisplayName("Descri��o")
	@MaxLength(100)
	public String getDescricao() {
		return descricao;
	}
	@Required
	@DisplayName("Data limite")
	public Date getDtlimite() {
		return dtlimite;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddepartamento")
	public Departamento getDepartamento() {
		return departamento;
	}
	@Required
	@DisplayName("Material/Servi�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@Required
	@DisplayName("Necessidade")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialclasse")
	public Materialclasse getMaterialclasse() {
		return materialclasse;
	}
	@Required
	@DisplayName("Local (Destino)")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@DisplayName("Origem")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrequisicaoorigem")
	public Requisicaoorigem getRequisicaoorigem() {
		return requisicaoorigem;
	}
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	@DisplayName("Requisitante")
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Date getDtcancelamento() {
		return dtcancelamento;
	}
	public Date getDtautorizacao() {
		return dtautorizacao;
	}
	public Date getDtbaixa() {
		return dtbaixa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrequisicaomaterial", insertable=false, updatable=false)
	public Aux_requisicaomaterial getAux_requisicaomaterial() {
		return aux_requisicaomaterial;
	}
	@MaxLength(9)
	public Integer getIdentificador() {
		return identificador;
	}
	@MaxLength(100)
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	@Required
	@DisplayName("Frequ�ncia")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfrequencia")
	public Frequencia getFrequencia() {
		return frequencia;
	}

	@Required
	@DisplayName("Qtde. Freq.")
	public Double getQtdefrequencia() {
		return qtdefrequencia;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@OneToMany(mappedBy="requisicaomaterial")
	public Set<Solicitacaocompraorigem> getListaSolicitacaocompraorigem() {
		return listaSolicitacaocompraorigem;
	}
	
	public Date getDtromaneiocompleto() {
		return dtromaneiocompleto;
	}	
	@DisplayName("Data cria��o")
	public Date getDtcriacao() {
		return dtcriacao;
	}
	
	public Double getAltura() {
		return altura;
	}

	public Double getLargura() {
		return largura;
	}

	public Double getComprimento() {
		return comprimento;
	}

	@DisplayName("Peso total")
	public Double getPesototal() {
		return pesototal;
	}
	
	@DisplayName("Qtde. Volume(s)")
	public Double getQtdvolume() {
		return qtdvolume;
	}
	
	public void setQtdvolume(Double qtdvolume) {
		this.qtdvolume = qtdvolume;
	}

	public void setAltura(Double altura) {
		this.altura = altura;
	}

	public void setLargura(Double largura) {
		this.largura = largura;
	}

	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}

	public void setPesototal(Double pesototal) {
		this.pesototal = pesototal;
	}

	public void setDtcriacao(Date dtcriacao) {
		this.dtcriacao = dtcriacao;
	}

	public void setDtromaneiocompleto(Date dtromaneiocompleto) {
		this.dtromaneiocompleto = dtromaneiocompleto;
	}
	
	public void setListaSolicitacaocompraorigem(
			Set<Solicitacaocompraorigem> listaSolicitacaocompraorigem) {
		this.listaSolicitacaocompraorigem = listaSolicitacaocompraorigem;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}

	public void setQtdefrequencia(Double qtdefrequencia) {
		this.qtdefrequencia = qtdefrequencia;
	}

	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
	}
	public void setDtcancelamento(Date dtcancelamento) {
		this.dtcancelamento = dtcancelamento;
	}
	public void setDtautorizacao(Date dtautorizacao) {
		this.dtautorizacao = dtautorizacao;
	}
	public void setDtbaixa(Date dtbaixa) {
		this.dtbaixa = dtbaixa;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setRequisicaoorigem(Requisicaoorigem requisicaoorigem) {
		this.requisicaoorigem = requisicaoorigem;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setMaterialclasse(Materialclasse materialclasse) {
		this.materialclasse = materialclasse;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	public void setDtlimite(Date dtlimite) {
		this.dtlimite = dtlimite;
	}
	public void setDescricao(String descricao) {
		this.descricao = StringUtils.trimToNull(descricao);
	}
	public void setCdrequisicaomaterial(Integer cdrequisicaomaterial) {
		this.cdrequisicaomaterial = cdrequisicaomaterial;
	}
	public void setListaVrequisicaomaterialentrega(List<Vrequisicaomaterialentrega> listaVrequisicaomaterialentrega) {
		this.listaVrequisicaomaterialentrega = listaVrequisicaomaterialentrega;
	}
	public void setVrequisicaomaterialmovimentacaoestoque(Vrequisicaomaterialmovimentacaoestoque vrequisicaomaterialmovimentacaoestoque) {
		this.vrequisicaomaterialmovimentacaoestoque = vrequisicaomaterialmovimentacaoestoque;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setAux_requisicaomaterial(
			Aux_requisicaomaterial aux_requisicaomaterial) {
		this.aux_requisicaomaterial = aux_requisicaomaterial;
	}
	public void setObservacao(String observacao) {
		this.observacao = StringUtils.trimToNull(observacao);
	}
	
	//==================== TRANSIENT'S ===========================
	
	protected Materialgrupo materialgrupo;
	protected Materialtipo materialtipo;
	protected Double disponivel;
	protected Double qtderestante;
	protected Double qtdepedida;
	protected String unidademedida;
	protected List<Requisicaomaterial> requisicoes = new ListSet<Requisicaomaterial>(Requisicaomaterial.class); 
	protected Frequencia frequenciatrans;
	protected List<String> listaErros;
	
	protected String plaqueta;
	
	@Transient
	@DisplayName("Grupo")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@Transient
	@DisplayName("Tipo")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	@Transient
	@DisplayName("Qtde. dispon�vel")
	public Double getDisponivel() {
		return disponivel;
	}
	@Transient
	@MaxLength(50)
	public String getPlaqueta() {
		return plaqueta;
	}
	@Transient
	@DisplayName("Qtde. Entregue")
	public Double getQtdepedida() {
		return qtdepedida;
	}
	@Transient
	@DisplayName("Qtde. Restante")
	public Double getQtderestante() {
		return qtderestante;
	}
	@Transient
	public List<Requisicaomaterial> getRequisicoes() {
		return requisicoes;
	}
	@Transient
	@MaxLength(10)
	public String getUnidademedida() {
		return unidademedida;
	}
	
	@Transient
	public Frequencia getFrequenciatrans() {
		return frequenciatrans;
	}

	public void setFrequenciatrans(Frequencia frequenciatrans) {
		this.frequenciatrans = frequenciatrans;
	}

	public void setPlaqueta(String plaqueta) {
		this.plaqueta = plaqueta;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	public void setDisponivel(Double disponivel) {
		this.disponivel = disponivel;
	}
	public void setQtdepedida(Double qtdepedida) {
		this.qtdepedida = qtdepedida;
	}
	public void setQtderestante(Double qtderestante) {
		this.qtderestante = qtderestante;
	}
	public void setRequisicoes(List<Requisicaomaterial> requisicoes) {
		this.requisicoes = requisicoes;
	}
	public void setUnidademedida(String unidademedida) {
		this.unidademedida = unidademedida;
	}
	
	@Transient
	public Integer getCdentrega() {
		return cdentrega;
	}
	public void setCdentrega(Integer cdentrega) {
		this.cdentrega = cdentrega;
	}
	@Transient
	@DisplayName("Unidade de Medida")
	public Unidademedida getUnidademedidatrans() {
		return unidademedidatrans;
	}
	public void setUnidademedidatrans(Unidademedida unidademedidatrans) {
		this.unidademedidatrans = unidademedidatrans;
	}

	@Transient
	public Entrega getEntrega() {
		return entrega;
	}
	public void setEntrega(Entrega entrega) {
		this.entrega = entrega;
	}
	@Transient
	public Material getMaterialmestregrade() {
		return materialmestregrade;
	}
	@Transient
	public List<Requisicaomaterial> getListaMaterialitemmestregrade() {
		return listaMaterialitemmestregrade;
	}

	public void setMaterialmestregrade(Material materialmestregrade) {
		this.materialmestregrade = materialmestregrade;
	}
	public void setListaMaterialitemmestregrade(List<Requisicaomaterial> listaMaterialitemmestregrade) {
		this.listaMaterialitemmestregrade = listaMaterialitemmestregrade;
	}
	
	@Transient
	public String getDescricaoQtde(){
		String descricaoqtde = "";
		if(getQtde() != null)
			descricaoqtde += " Quantidade: " + getQtde();
		if(getQtderestante() != null)
			descricaoqtde += " Qtde. Restante: " + getQtderestante();
		if(getQtdepedida() != null)
			descricaoqtde += " Qtde. Entregue: " + getQtdepedida();
		if(getDisponivel() != null)
			descricaoqtde += " Qtde. dispon�vel: " + getDisponivel();
		return descricaoqtde;
	}

	public String subQueryProjeto() {
		return "select requisicaomaterialsubQueryProjeto.cdrequisicaomaterial " +
				"from Requisicaomaterial requisicaomaterialsubQueryProjeto " +
				"left outer join requisicaomaterialsubQueryProjeto.projeto projetosubQueryProjeto " +
				"where (projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ") " +
				"or projetosubQueryProjeto.cdprojeto is null)";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
	
	@Transient
	public String getWhereInCdpedidovenda() {
		return whereInCdpedidovenda;
	}
	
	public void setWhereInCdpedidovenda(String whereInCdpedidovenda) {
		this.whereInCdpedidovenda = whereInCdpedidovenda;
	}
	
	@Transient
	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}
	
	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
	
	@Transient
	public Boolean getRomaneioGerado() {
		return romaneioGerado;
	}
	
	public void setRomaneioGerado(Boolean romaneioGerado) {
		this.romaneioGerado = romaneioGerado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdrequisicaomaterial == null) ? 0 : cdrequisicaomaterial
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Requisicaomaterial other = (Requisicaomaterial) obj;
		if (cdrequisicaomaterial == null) {
			if (other.cdrequisicaomaterial != null)
				return false;
		} else if (!cdrequisicaomaterial.equals(other.cdrequisicaomaterial))
			return false;
		return true;
	}

	@Transient
	public Integer getCdProducaoagenda() {
		return cdProducaoagenda;
	}
	@Transient
	public Double getQtdeEntregue() {
		return qtdeEntregue;
	}

	public void setCdProducaoagenda(Integer cdProducaoagenda) {
		this.cdProducaoagenda = cdProducaoagenda;
	}
	public void setQtdeEntregue(Double qtdeEntregue) {
		this.qtdeEntregue = qtdeEntregue;
	}
	
	@Transient
	public String getIdentificadorOuCdrequisicaomaterial(){
		if(getIdentificador() != null){
			return getIdentificador().toString();
		}else if(getCdrequisicaomaterial() != null){
			return getCdrequisicaomaterial().toString();
		}else {
			return null;
		}
		
	}
	
	@Transient 
	public Boolean getExisteRomaneioPendente(){
		boolean existeRomaneioPendente = false;
		if(Hibernate.isInitialized(listaVrequisicaomaterialentrega) &&
				SinedUtil.isListNotEmpty(listaVrequisicaomaterialentrega)){
			for(Vrequisicaomaterialentrega vrequisicaomaterialentrega : listaVrequisicaomaterialentrega){
				if(vrequisicaomaterialentrega.getEntregaromaneio() != null && !vrequisicaomaterialentrega.getEntregaromaneio() && 
						vrequisicaomaterialentrega.getLocalentregamaterial() != null && 
						vrequisicaomaterialentrega.getLocalpedido() != null &&
						!vrequisicaomaterialentrega.getLocalentregamaterial().equals(vrequisicaomaterialentrega.getLocalpedido())){
					existeRomaneioPendente = true;
					break;
				}
			}
		}
		return existeRomaneioPendente;
	}
	
	@Transient 
	public Boolean getRomaneioObrigatorio(){
		boolean romaneioObrigatorio = false;
		if(Hibernate.isInitialized(listaVrequisicaomaterialentrega) &&
				SinedUtil.isListNotEmpty(listaVrequisicaomaterialentrega)){
			for(Vrequisicaomaterialentrega vrequisicaomaterialentrega : listaVrequisicaomaterialentrega){
				if(vrequisicaomaterialentrega.getEntregaromaneio() != null && 
						vrequisicaomaterialentrega.getLocalentregamaterial() != null && 
						vrequisicaomaterialentrega.getLocalpedido() != null &&
						!vrequisicaomaterialentrega.getLocalentregamaterial().equals(vrequisicaomaterialentrega.getLocalpedido())){
					romaneioObrigatorio = true;
					break;
				}
			}
		}
		return romaneioObrigatorio;
	}

	@Transient
	public Boolean getIsMateriaprimaProducaoagenda() {
		return isMateriaprimaProducaoagenda;
	}

	public void setIsMateriaprimaProducaoagenda(Boolean isMateriaprimaProducaoagenda) {
		this.isMateriaprimaProducaoagenda = isMateriaprimaProducaoagenda;
	}
	
	@Transient
	public Boolean getGerado() {
		return gerado;
	}
	@Transient
	public Integer getCdplanejamentorecursogeral() {
		return cdplanejamentorecursogeral;
	}
	@Transient
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	
	public void setGerado(Boolean gerado) {
		this.gerado = gerado;
	}
	public void setCdplanejamentorecursogeral(Integer cdplanejamentorecursogeral) {
		this.cdplanejamentorecursogeral = cdplanejamentorecursogeral;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}

	@Transient
	public Boolean getCopiar() {
		return copiar;
	}

	public void setCopiar(Boolean copiar) {
		this.copiar = copiar;
	}

	@Transient
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}

	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	
	@Transient
	public Boolean getSomenteBaixa() {
		return somenteBaixa;
	}
	
	public void setSomenteBaixa(Boolean somenteBaixa) {
		this.somenteBaixa = somenteBaixa;
	}

	@Transient
	public List<String> getListaErros() {
		return listaErros;
	}

	public void setListaErros(List<String> listaErros) {
		this.listaErros = listaErros;
	}
	
	
}
