package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;


@Entity
@SequenceGenerator(name = "sq_pedidovendapagamento", sequenceName = "sq_pedidovendapagamento")
public class Pedidovendapagamento {

	protected Integer cdpedidovendapagamento;
	protected Pedidovenda pedidovenda;
	protected Documento documento;
	protected Documentotipo documentotipo;
	protected Integer banco;
	protected Integer agencia;
	protected Integer conta;
	protected String numero;
	protected Money valororiginal;
	protected Date dataparcela;
	protected Money valorjuros;
	protected Documento documentoantecipacao;
	protected String emitente;
	protected String cpfcnpj;
	protected Cheque cheque;
	
	//Transient
	protected Date dataparcelaEmissaonota;
	protected String numeroEmissaonota;
	protected Conta contadestino;
	protected Boolean podeGerarCheque;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pedidovendapagamento")
	public Integer getCdpedidovendapagamento() {
		return cdpedidovendapagamento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoantecipacao")
	@DisplayName("Pagamento adiantado")
	public Documento getDocumentoantecipacao() {
		return documentoantecipacao;
	}	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdformapagamento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}
	public Integer getBanco() {
		return banco;
	}
	@DisplayName("Ag�ncia")
	public Integer getAgencia() {
		return agencia;
	}
	@MaxLength(9)
	public Integer getConta() {
		return conta;
	}
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}
	public Money getValororiginal() {
		return valororiginal;
	}
	@DisplayName("Vencimento")
	public Date getDataparcela() {
		return dataparcela;
	}
	
	public void setCdpedidovendapagamento(Integer cdpedidovendapagamento) {
		this.cdpedidovendapagamento = cdpedidovendapagamento;
	}
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public void setDocumentoantecipacao(Documento documentoantecipacao) {
		this.documentoantecipacao = documentoantecipacao;
	}
	public void setBanco(Integer banco) {
		this.banco = banco;
	}
	public void setAgencia(Integer agencia) {
		this.agencia = agencia;
	}
	public void setConta(Integer conta) {
		this.conta = conta;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setValororiginal(Money valororiginal) {
		this.valororiginal = valororiginal;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setDataparcela(Date dataparcela) {
		this.dataparcela = dataparcela;
	}
	
	@Transient
	public Date getDataparcelaEmissaonota() {
		return dataparcelaEmissaonota;
	}
	@Transient
	public String getNumeroEmissaonota() {
		return numeroEmissaonota;
	}
	public void setDataparcelaEmissaonota(Date dataparcelaEmissaonota) {
		this.dataparcelaEmissaonota = dataparcelaEmissaonota;
	}
	public void setNumeroEmissaonota(String numeroEmissaonota) {
		this.numeroEmissaonota = numeroEmissaonota;
	}
	
	@DisplayName("Juros Parcela")
	public Money getValorjuros() {
		return valorjuros;
	}
	public void setValorjuros(Money valorjuros) {
		this.valorjuros = valorjuros;
	}
	
	public String getEmitente() {
		return emitente;
	}
	public void setEmitente(String emitente) {
		this.emitente = emitente;
	}
	
	@DisplayName("CNPJ/CPF do emitente")
	public String getCpfcnpj() {
		return cpfcnpj;
	}

	public void setCpfcnpj(String cpfcnpj) {
		this.cpfcnpj = cpfcnpj;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcheque")
	@DisplayName("Cheque")
	public Cheque getCheque() {
		return cheque;
	}
	
	public void setCheque(Cheque cheque) {
		this.cheque = cheque;
	}
	
	@Transient
	public Conta getContadestino() {
		return contadestino;
	}
	
	public void setContadestino(Conta contadestino) {
		this.contadestino = contadestino;
	}
	
	@Transient
	public Boolean getPodeGerarCheque() {
		if(documentotipo == null || documentotipo.getCddocumentotipo() == null){
			podeGerarCheque = false;
		}else{
			podeGerarCheque = StringUtils.isNotEmpty(this.getNumero()) && (!Boolean.TRUE.equals(documentotipo.getCartao()));
		}
		return podeGerarCheque;
	}
}
