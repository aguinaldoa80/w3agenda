package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;


@Entity
@SequenceGenerator(name="sq_prontuariohistorico", sequenceName="sq_prontuariohistorico")
@DisplayName("Hist�rico")
public class Prontuariohistorico {

	private Integer cdprontuariohistorico;
	private Prontuario prontuario;
	private Timestamp data;
	private Usuario usuario;
	private Colaborador terapeuta;
	private Cliente paciente;
	private Integer numero;
	private Date dtinicio;
	private Hora hrinicio;
	private Date dtfim;
	private Hora hrfim;
	private String observacao;
	
	public Prontuariohistorico(){
	}
	
	public Prontuariohistorico(Integer cdprontuariohistorico){
		this.cdprontuariohistorico = cdprontuariohistorico;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_prontuariohistorico")
	public Integer getCdprontuariohistorico() {
		return cdprontuariohistorico;
	}
	@DisplayName("Terapeuta")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprontuario")
	public Prontuario getProntuario() {
		return prontuario;
	}
	@DisplayName("Data/Hora")
	public Timestamp getData() {
		return data;
	}
	@DisplayName("Respons�vel")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuario")
	public Usuario getUsuario() {
		return usuario;
	}
	@DisplayName("Terapeuta")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdterapeuta")
	public Colaborador getTerapeuta() {
		return terapeuta;
	}
	@DisplayName("Paciente")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpaciente")
	public Cliente getPaciente() {
		return paciente;
	}
	@DisplayName("N�mero")
	public Integer getNumero() {
		return numero;
	}
	@DisplayName("Data (in�cio)")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Hora (in�cio)")
	public Hora getHrinicio() {
		return hrinicio;
	}
	@DisplayName("Data (Fim)")
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Hora (Fim)")
	public Hora getHrfim() {
		return hrfim;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	public void setCdprontuariohistorico(Integer cdprontuariohistorico) {
		this.cdprontuariohistorico = cdprontuariohistorico;
	}
	public void setProntuario(Prontuario prontuario) {
		this.prontuario = prontuario;
	}
	public void setData(Timestamp data) {
		this.data = data;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setTerapeuta(Colaborador terapeuta) {
		this.terapeuta = terapeuta;
	}
	public void setPaciente(Cliente paciente) {
		this.paciente = paciente;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setHrinicio(Hora hrinicio) {
		this.hrinicio = hrinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setHrfim(Hora hrfim) {
		this.hrfim = hrfim;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
}