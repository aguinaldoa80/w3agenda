package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_formulacustoitem", sequenceName = "sq_formulacustoitem")
public class Formulacustoitem {

	protected Integer cdformulacustoitem;
	protected Formulacusto formulacusto;
	protected Integer ordem;
	protected String identificador;
	protected String formula;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_formulacustoitem")
	public Integer getCdformulacustoitem() {
		return cdformulacustoitem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdformulacusto")
	public Formulacusto getFormulacusto() {
		return formulacusto;
	}
	@Required
	public Integer getOrdem() {
		return ordem;
	}
	@Required
	@MaxLength(1)
	public String getIdentificador() {
		return identificador;
	}
	@Required
	@DisplayName("F�rmula")
	@MaxLength(2000)
	public String getFormula() {
		return formula;
	}

	public void setCdformulacustoitem(Integer cdformulacustoitem) {
		this.cdformulacustoitem = cdformulacustoitem;
	}
	public void setFormulacusto(Formulacusto formulacusto) {
		this.formulacusto = formulacusto;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setFormula(String formula) {
		this.formula = formula;
	}
}
