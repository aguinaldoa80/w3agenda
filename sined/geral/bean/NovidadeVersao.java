package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_novidadeversao", sequenceName = "sq_novidadeversao")
public class NovidadeVersao extends ReportTemplateFiltro {

	protected Integer cdnovidadeversao;
	protected String nome;
	protected String descricao;
	protected String versao;
	protected Date dtinicio;
	protected Date dtfim;
	protected Projeto projeto;
	private List<NovidadeVersaoFuncionalidade> listaNovidadeVersaoFuncionalidade;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Boolean ativo;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_novidadeversao")
	public Integer getCdnovidadeversao() {
		return cdnovidadeversao;
	}

	public String getNome() {
		return nome;
	}
	
	@DescriptionProperty
	public String getVersao() {
		return versao;
	}

	public String getDescricao() {
		return descricao;
	}
	
	public Date getDtfim() {
		return dtfim;
	}
	
	public Date getDtinicio() {
		return dtinicio;
	}
	

	@OneToMany(mappedBy="novidadeversao")
	public List<NovidadeVersaoFuncionalidade> getListaNovidadeVersaoFuncionalidade() {
		return listaNovidadeVersaoFuncionalidade;
	}
	
	public Timestamp getDtaltera() {
		return null;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}

	// SETs
	
	public void setCdnovidadeversao(Integer cdnovidadeversao) {
		this.cdnovidadeversao = cdnovidadeversao;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setVersao(String versao) {
		this.versao = versao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setListaNovidadeVersaoFuncionalidade(
			List<NovidadeVersaoFuncionalidade> listaNovidadeVersaoFuncionalidade) {
		this.listaNovidadeVersaoFuncionalidade = listaNovidadeVersaoFuncionalidade;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}