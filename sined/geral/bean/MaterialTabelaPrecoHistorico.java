package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@DisplayName("Hist�rico")
@SequenceGenerator(name = "sq_materialtabelaprecohistorico", sequenceName = "sq_materialtabelaprecohistorico")
public class MaterialTabelaPrecoHistorico implements Log {

	private Integer cdmaterialtabelaprecohistorico;
	private Materialtabelapreco materialtabelapreco;
	private String observacao;
	private Arquivo arquivoImportacaoProduto;
	private Timestamp dtaltera;
	private Integer cdusuarioaltera;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_materialtabelaprecohistorico")
	public Integer getCdmaterialtabelaprecohistorico() {
		return cdmaterialtabelaprecohistorico;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdmaterialtabelapreco")
	public Materialtabelapreco getMaterialtabelapreco() {
		return materialtabelapreco;
	}

	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	@DisplayName("Arquivo de imrpota��o")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdarquivoimportacaoproduto")
	public Arquivo getArquivoImportacaoProduto() {
		return arquivoImportacaoProduto;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setArquivoImportacaoProduto(Arquivo arquivoImportacaoProduto) {
		this.arquivoImportacaoProduto = arquivoImportacaoProduto;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCdmaterialtabelaprecohistorico(Integer cdmaterialtabelaprecohistorico) {
		this.cdmaterialtabelaprecohistorico = cdmaterialtabelaprecohistorico;
	}

	public void setMaterialtabelapreco(Materialtabelapreco materialtabelapreco) {
		this.materialtabelapreco = materialtabelapreco;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
}
