package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_localarmazenagemempresa", sequenceName = "sq_localarmazenagemempresa")
@DisplayName("Local Armazenagem empresa")
public class Localarmazenagemempresa implements Log{

	protected Integer cdlocalarmazenagemempresa;
	protected Localarmazenagem localarmazenagem;
	protected Empresa empresa;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_localarmazenagemempresa")
	public Integer getCdlocalarmazenagemempresa() {
		return cdlocalarmazenagemempresa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	@DescriptionProperty
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setCdlocalarmazenagemempresa(Integer cdlocalarmazenagemempresa) {
		this.cdlocalarmazenagemempresa = cdlocalarmazenagemempresa;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Localarmazenagemempresa) {
			Localarmazenagemempresa localarmazenagemempresa = (Localarmazenagemempresa) obj;
			return this.getEmpresa().equals(localarmazenagemempresa.getEmpresa());
		}
		return super.equals(obj);
	}
	
}