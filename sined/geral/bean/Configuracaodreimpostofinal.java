package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_configuracaodreimpostofinal", sequenceName = "sq_configuracaodreimpostofinal")
@DisplayName("Imposto considerado no final da DRE")
public class Configuracaodreimpostofinal {
	
	protected Integer cdconfiguracaodreimpostofinal; 
	protected Configuracaodre configuracaodre;
	protected String descricao;
	protected Double percentual;
	protected String formula;
	protected Integer ordem;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_configuracaodreimpostofinal")
	public Integer getCdconfiguracaodreimpostofinal() {
		return cdconfiguracaodreimpostofinal;
	}

	@Required
	@JoinColumn(name="cdconfiguracaodre")
	@ManyToOne(fetch=FetchType.LAZY)
	public Configuracaodre getConfiguracaodre() {
		return configuracaodre;
	}

	@Required
	@DisplayName("Descri��o")
	@MaxLength(200)
	public String getDescricao() {
		return descricao;
	}

	@DisplayName("Percentual (%)")
	public Double getPercentual() {
		return percentual;
	}
	
	@DisplayName("F�rmula")
	public String getFormula() {
		return formula;
	}
	
	public Integer getOrdem() {
		return ordem;
	}
	
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	
	public void setFormula(String formula) {
		this.formula = formula;
	}

	public void setCdconfiguracaodreimpostofinal(
			Integer cdconfiguracaodreimpostofinal) {
		this.cdconfiguracaodreimpostofinal = cdconfiguracaodreimpostofinal;
	}

	public void setConfiguracaodre(Configuracaodre configuracaodre) {
		this.configuracaodre = configuracaodre;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}
	
}
	