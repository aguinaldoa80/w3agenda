package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@DisplayName("M�o de Obra")
@SequenceGenerator(name = "sq_diarioobramdo", sequenceName = "sq_diarioobramdo")
public class Diarioobramdo {
	
	protected Integer cddiarioobramdo;
	protected Diarioobra diarioobra;
	protected Cargo cargo;
	protected Double quantidade;
	protected Double totalhora;
	
	@Id
	@DisplayName ("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_diarioobramdo")
	public Integer getCddiarioobramdo() {
		return cddiarioobramdo;
	}
		
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddiarioobra")
	public Diarioobra getDiarioobra() {
		return diarioobra;
	}	
	
	@Required	
	@DisplayName ("Cargo")	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	public Cargo getCargo() {
		return cargo;
	}
	
	@Required
	@DisplayName ("Quantidade")
	public Double getQuantidade() {
		return quantidade;
	}
	
	@Required
	@DisplayName ("Total de horas")
	public Double getTotalhora() {
		return totalhora;
	}
	
	public void setCddiarioobramdo(Integer cddiarioobramdo) {
		this.cddiarioobramdo = cddiarioobramdo;
	}	
	public void setDiarioobra(Diarioobra diarioobra) {
		this.diarioobra = diarioobra;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setTotalhora(Double totalhora) {
		this.totalhora = totalhora;
	}
}
