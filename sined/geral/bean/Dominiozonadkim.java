package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Password;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_dominiozonadkim", sequenceName = "sq_dominiozonadkim")
public class Dominiozonadkim implements Log{

	protected Integer cddominiozonadkim;
	protected Integer versao;
	protected String nome;
	protected String servico;
	protected String tiposervico;
	protected String chavepublica;
	protected String chaveprivada;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Dominio dominio;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_dominiozonadkim")
	public Integer getCddominiozonadkim() {
		return cddominiozonadkim;
	}

	@Required
	@MaxLength(9)
	@DisplayName("Vers�o")
	public Integer getVersao() {
		return versao;
	}
	
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	@Required
	@MaxLength(50)
	@DisplayName("Servi�o")
	public String getServico() {
		return servico;
	}
	
	@Required
	@MaxLength(50)
	@DisplayName("Tipo de servi�o")
	public String getTiposervico() {
		return tiposervico;
	}
		
	@Password
	@MaxLength(50)
	@DisplayName("Chave p�blica")
	public String getChavepublica() {
		return chavepublica;
	}
	
	@Password
	@MaxLength(50)
	@DisplayName("Chave privada")
	public String getChaveprivada() {
		return chaveprivada;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddominio")
	@DisplayName("Dom�nio principal")
	public Dominio getDominio() {
		return dominio;
	}
	
	public void setCddominiozonadkim(Integer cddominiozonadkim) {
		this.cddominiozonadkim = cddominiozonadkim;
	}

	public void setVersao(Integer versao) {
		this.versao = versao;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setServico(String servico) {
		this.servico = servico;
	}

	public void setTiposervico(String tiposervico) {
		this.tiposervico = tiposervico;
	}

	public void setChavepublica(String chavepublica) {
		this.chavepublica = chavepublica;
	}

	public void setChaveprivada(String chaveprivada) {
		this.chaveprivada = chaveprivada;
	}
	
	public void setDominio(Dominio dominio) {
		this.dominio = dominio;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}	
}
