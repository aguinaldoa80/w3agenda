package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Fornecimentocontratoitemtipo;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Itens")
@SequenceGenerator(name = "sq_fornecimentocontratoitem", sequenceName = "sq_fornecimentocontratoitem")
public class Fornecimentocontratoitem implements Log{

	protected Integer cdfornecimentocontratoitem;
	protected Fornecimentocontrato fornecimentocontrato;
	protected Fornecimentocontratoitemtipo fornecimentocontratoitemtipo;
	protected Material material;
	protected String descricao;
	protected Double qtde;
	protected Double valorequipamento;
	protected Double valor;
	protected String observacao;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;	

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_fornecimentocontratoitem")
	public Integer getCdfornecimentocontratoitem() {
		return cdfornecimentocontratoitem;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecimentocontrato")
	public Fornecimentocontrato getFornecimentocontrato() {
		return fornecimentocontrato;
	}
	@Required
	@DisplayName("Tipo")
	public Fornecimentocontratoitemtipo getFornecimentocontratoitemtipo() {
		return fornecimentocontratoitemtipo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	
	@DisplayName("Descri��o")
	@MaxLength(150)
	public String getDescricao() {
		return descricao;
	}

	@DisplayName("Qtde. prevista")
	@MaxLength(10)
	public Double getQtde() {
		return qtde;
	}
	@DisplayName("Valor equipamento")
	public Double getValorequipamento() {
		return valorequipamento;
	}
	public Double getValor() {
		return valor;
	}

	@DisplayName("Observa��o")
	@MaxLength(150)
	public String getObservacao() {
		return observacao;
	}

	
	public void setFornecimentocontrato(Fornecimentocontrato fornecimentocontrato) {
		this.fornecimentocontrato = fornecimentocontrato;
	}
	public void setFornecimentocontratoitemtipo(Fornecimentocontratoitemtipo fornecimentocontratoitemtipo) {
		this.fornecimentocontratoitemtipo = fornecimentocontratoitemtipo;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setDescricao(String descricao) {
		this.descricao = StringUtils.trimToNull(descricao);
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setValorequipamento(Double valorequipamento) {
		this.valorequipamento = valorequipamento;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setObservacao(String observacao) {
		this.observacao = StringUtils.trimToNull(observacao);
	}
	public void setCdfornecimentocontratoitem(Integer cdfornecimentocontratoitem) {
		this.cdfornecimentocontratoitem = cdfornecimentocontratoitem;
	}
	
//	LOG
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
}
