package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_regimetributacao", sequenceName = "sq_regimetributacao")
public class Regimetributacao implements Log{
	
	public static final Regimetributacao MICROEMPRESAMUNICIPAL = new Regimetributacao(1);
	public static final Regimetributacao ESTIMATIVA = new Regimetributacao(2);
	public static final Regimetributacao SOCIEDADE_PROFISSIONAIS = new Regimetributacao(3);
	public static final Regimetributacao COOPERATIVA = new Regimetributacao(4);
	public static final Regimetributacao MEI_SIMPLES_NACIONAL = new Regimetributacao(5);
	public static final Regimetributacao ME_EPP_SIMPLES_NACIONAL = new Regimetributacao(6);
	public static final Regimetributacao FATURAMENTO = new Regimetributacao(7);
	public static final Regimetributacao NOTA_FISCAL_AVULSA = new Regimetributacao(8);

	protected Integer cdregimetributacao;
	protected String descricao;
	protected String codigonfse;
	protected Integer codigonfe;
	protected Boolean ativo = Boolean.TRUE;
	
	//Log
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Regimetributacao() {
	}
	
	public Regimetributacao(Integer cdregimetributacao) {
		this.cdregimetributacao = cdregimetributacao;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_regimetributacao")
	public Integer getCdregimetributacao() {
		return cdregimetributacao;
	}
	
	@Required
	@MaxLength(100)
	@DescriptionProperty
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	@MaxLength(10)
	@DisplayName("C�digo para NFS-e")
	public String getCodigonfse() {
		return codigonfse;
	}

	public Boolean getAtivo() {
		return ativo;
	}
	
	@DisplayName("C�digo para NF-e")
	public Integer getCodigonfe() {
		return codigonfe;
	}
	
	public void setCodigonfe(Integer codigonfe) {
		this.codigonfe = codigonfe;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	public void setCdregimetributacao(Integer cdregimetributacao) {
		this.cdregimetributacao = cdregimetributacao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setCodigonfse(String codigonfse) {
		this.codigonfse = codigonfse;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdregimetributacao == null) ? 0 : cdregimetributacao
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Regimetributacao other = (Regimetributacao) obj;
		if (cdregimetributacao == null) {
			if (other.cdregimetributacao != null)
				return false;
		} else if (!cdregimetributacao.equals(other.cdregimetributacao))
			return false;
		return true;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}	
}
