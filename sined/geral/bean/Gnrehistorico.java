package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.GnreAcaoEnum;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name="sq_gnrehistorico",sequenceName="sq_gnrehistorico")
public class Gnrehistorico implements Log {

	protected Integer cdgnrehistorico;
	protected Gnre gnre;
	protected GnreAcaoEnum acao;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	@Id
	@GeneratedValue(generator="sq_gnrehistorico",strategy=GenerationType.AUTO)
	public Integer getCdgnrehistorico() {
		return cdgnrehistorico;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgnre")
	public Gnre getGnre() {
		return gnre;
	}

	@DisplayName("A��o")
	public GnreAcaoEnum getAcao() {
		return acao;
	}

	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdgnrehistorico(Integer cdgnrehistorico) {
		this.cdgnrehistorico = cdgnrehistorico;
	}

	public void setGnre(Gnre gnre) {
		this.gnre = gnre;
	}

	public void setAcao(GnreAcaoEnum acao) {
		this.acao = acao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
}