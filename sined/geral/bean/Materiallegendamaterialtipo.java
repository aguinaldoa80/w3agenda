package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_materiallegendamaterialtipo", sequenceName = "sq_materiallegendamaterialtipo")
public class Materiallegendamaterialtipo {
	
	protected Integer cdmateriallegendamaterialtipo;
	protected Materiallegenda materiallegenda;
	protected Materialtipo materialtipo;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materiallegendamaterialtipo")
	public Integer getCdmateriallegendamaterialtipo() {
		return cdmateriallegendamaterialtipo;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdmateriallegenda")
	public Materiallegenda getMateriallegenda() {
		return materiallegenda;
	}
	
	@Required
	@DisplayName("Tipo de material")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdmaterialtipo")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}

	public void setCdmateriallegendamaterialtipo(
			Integer cdmateriallegendamaterialtipo) {
		this.cdmateriallegendamaterialtipo = cdmateriallegendamaterialtipo;
	}

	public void setMateriallegenda(Materiallegenda materiallegenda) {
		this.materiallegenda = materiallegenda;
	}

	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	
}