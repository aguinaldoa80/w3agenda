package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRetornoAcaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRetornoSituacaoProtestoEnum;

@Entity
@SequenceGenerator(name="sq_bancoconfiguracaoretornoocorrencia",sequenceName="sq_bancoconfiguracaoretornoocorrencia")
public class BancoConfiguracaoRetornoOcorrencia {

	private Integer cdbancoconfiguracaoretornoocorrencia;
	private BancoConfiguracaoRetorno bancoConfiguracaoRetorno;
	private String codigo;
	private String descricao;
	private BancoConfiguracaoRetornoAcaoEnum acao;
	private BancoConfiguracaoRetornoSituacaoProtestoEnum situacaoProtesto;
	private String historico;	
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoconfiguracaoretornoocorrencia")
	public Integer getCdbancoconfiguracaoretornoocorrencia() {
		return cdbancoconfiguracaoretornoocorrencia;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaoretorno")
	@Required
	public BancoConfiguracaoRetorno getBancoConfiguracaoRetorno() {
		return bancoConfiguracaoRetorno;
	}
	
	@DisplayName("C�digo")
	@Required
	public String getCodigo() {
		return codigo;
	}
	
	@DisplayName("Descri��o")
	@DescriptionProperty
	@Required
	public String getDescricao() {
		return descricao;
	}
	
	@DisplayName("A��o")
	@Required
	public BancoConfiguracaoRetornoAcaoEnum getAcao() {
		return acao;
	}
	
	public BancoConfiguracaoRetornoSituacaoProtestoEnum getSituacaoProtesto() {
		return situacaoProtesto;
	}
	
	@DisplayName("Hist�rico")
	public String getHistorico() {
		return historico;
	}

	public void setCdbancoconfiguracaoretornoocorrencia(
			Integer cdbancoconfiguracaoretornoocorrencia) {
		this.cdbancoconfiguracaoretornoocorrencia = cdbancoconfiguracaoretornoocorrencia;
	}

	public void setBancoConfiguracaoRetorno(
			BancoConfiguracaoRetorno bancoConfiguracaoRetorno) {
		this.bancoConfiguracaoRetorno = bancoConfiguracaoRetorno;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setAcao(BancoConfiguracaoRetornoAcaoEnum acao) {
		this.acao = acao;
	}
	
	public void setSituacaoProtesto(BancoConfiguracaoRetornoSituacaoProtestoEnum situacaoProtesto) {
		this.situacaoProtesto = situacaoProtesto;
	}
	
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	
}
