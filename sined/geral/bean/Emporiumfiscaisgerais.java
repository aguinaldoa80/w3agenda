package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_emporiumfiscaisgerais", sequenceName = "sq_emporiumfiscaisgerais")
public class Emporiumfiscaisgerais {

	protected Integer cdemporiumfiscaisgerais;
	protected Date dtmovimento;
	protected String pdv;
	protected String reducoes;
	protected String ticket_inicial;
	protected String ticket_final;
	protected String clientes;
	protected String gt_inicial;
	protected String gt_final;
	protected String venda_bruta;
	protected String cancelados;
	protected String anulados;
	protected String desconto;
	protected String acrescimo;
	protected String venda_liquida;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_emporiumfiscaisgerais")
	public Integer getCdemporiumfiscaisgerais() {
		return cdemporiumfiscaisgerais;
	}

	public Date getDtmovimento() {
		return dtmovimento;
	}

	public String getPdv() {
		return pdv;
	}

	public String getReducoes() {
		return reducoes;
	}

	public String getTicket_inicial() {
		return ticket_inicial;
	}

	public String getTicket_final() {
		return ticket_final;
	}

	public String getClientes() {
		return clientes;
	}

	public String getGt_inicial() {
		return gt_inicial;
	}

	public String getGt_final() {
		return gt_final;
	}

	public String getVenda_bruta() {
		return venda_bruta;
	}

	public String getCancelados() {
		return cancelados;
	}

	public String getAnulados() {
		return anulados;
	}

	public String getDesconto() {
		return desconto;
	}

	public String getAcrescimo() {
		return acrescimo;
	}

	public String getVenda_liquida() {
		return venda_liquida;
	}

	public void setCdemporiumfiscaisgerais(Integer cdemporiumfiscaisgerais) {
		this.cdemporiumfiscaisgerais = cdemporiumfiscaisgerais;
	}

	public void setDtmovimento(Date dtmovimento) {
		this.dtmovimento = dtmovimento;
	}

	public void setPdv(String pdv) {
		this.pdv = pdv;
	}

	public void setReducoes(String reducoes) {
		this.reducoes = reducoes;
	}

	public void setTicket_inicial(String ticketInicial) {
		ticket_inicial = ticketInicial;
	}

	public void setTicket_final(String ticketFinal) {
		ticket_final = ticketFinal;
	}

	public void setClientes(String clientes) {
		this.clientes = clientes;
	}

	public void setGt_inicial(String gtInicial) {
		gt_inicial = gtInicial;
	}

	public void setGt_final(String gtFinal) {
		gt_final = gtFinal;
	}

	public void setVenda_bruta(String vendaBruta) {
		venda_bruta = vendaBruta;
	}

	public void setCancelados(String cancelados) {
		this.cancelados = cancelados;
	}

	public void setAnulados(String anulados) {
		this.anulados = anulados;
	}

	public void setDesconto(String desconto) {
		this.desconto = desconto;
	}

	public void setAcrescimo(String acrescimo) {
		this.acrescimo = acrescimo;
	}

	public void setVenda_liquida(String vendaLiquida) {
		venda_liquida = vendaLiquida;
	}
	
	
}
