package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;


@Entity
public class Servidorwirelesscliente implements Log{
	
	protected Servidor servidor;
	protected String mac;
	protected String sinal;
	protected Double rxbyte;
	protected Double rxpacket;
	protected Double rxrate;
	protected Double txbyte;
	protected Double txpacket;
	protected Double txrate;

	@DisplayName("Servidor")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservidor")
	public Servidor getServidor() {
		return servidor;
	}
	@Id
	public String getMac() {
		return mac;
	}
	public String getSinal() {
		return sinal;
	}
	public Double getRxbyte() {
		return rxbyte;
	}
	public Double getRxpacket() {
		return rxpacket;
	}
	public Double getRxrate() {
		return rxrate;
	}
	public Double getTxbyte() {
		return txbyte;
	}
	public Double getTxpacket() {
		return txpacket;
	}
	public Double getTxrate() {
		return txrate;
	}
	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	public void setSinal(String sinal) {
		this.sinal = sinal;
	}
	public void setRxbyte(Double rxbyte) {
		this.rxbyte = rxbyte;
	}
	public void setRxpacket(Double rxpacket) {
		this.rxpacket = rxpacket;
	}
	public void setRxrate(Double rxrate) {
		this.rxrate = rxrate;
	}
	public void setTxbyte(Double txbyte) {
		this.txbyte = txbyte;
	}
	public void setTxpacket(Double txpacket) {
		this.txpacket = txpacket;
	}
	public void setTxrate(Double txrate) {
		this.txrate = txrate;
	}

	@Transient
	public Integer getCdusuarioaltera() {
		return null;
	}
	@Transient
	public Timestamp getDtaltera() {
		return null;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
	}
	public void setDtaltera(Timestamp dtaltera) {
	}
		
}
