package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_bancoconfiguracaoretornorejeicao",sequenceName="sq_bancoconfiguracaoretornorejeicao")
public class BancoConfiguracaoRetornoRejeicao {

	private Integer cdbancoconfiguracaoretornorejeicao;
	private BancoConfiguracaoRetorno bancoConfiguracaoRetorno;
	private String ocorrencia;
	private String codigo;
	private String descricao;
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoconfiguracaoretornorejeicao")
	public Integer getCdbancoconfiguracaoretornorejeicao() {
		return cdbancoconfiguracaoretornorejeicao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaoretorno")
	@Required
	public BancoConfiguracaoRetorno getBancoConfiguracaoRetorno() {
		return bancoConfiguracaoRetorno;
	}
	
	@DisplayName("Ocorr�ncia")
	@MaxLength(3)
	public String getOcorrencia() {
		return ocorrencia;
	}

	@DisplayName("C�digo")
	@Required
	@MaxLength(3)
	public String getCodigo() {
		return codigo;
	}
	
	@DisplayName("Descri��o")
	@DescriptionProperty
	@Required
	public String getDescricao() {
		return descricao;
	}

	public void setCdbancoconfiguracaoretornorejeicao(
			Integer cdbancoconfiguracaoretornorejeicao) {
		this.cdbancoconfiguracaoretornorejeicao = cdbancoconfiguracaoretornorejeicao;
	}

	public void setBancoConfiguracaoRetorno(
			BancoConfiguracaoRetorno bancoConfiguracaoRetorno) {
		this.bancoConfiguracaoRetorno = bancoConfiguracaoRetorno;
	}


	public void setOcorrencia(String ocorrencia) {
		this.ocorrencia = ocorrencia;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
