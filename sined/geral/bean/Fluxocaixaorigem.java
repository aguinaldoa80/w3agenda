package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_fluxocaixaorigem", sequenceName="sq_fluxocaixaorigem")
@DisplayName("Fluxo de caixa")
public class Fluxocaixaorigem implements Log{

	protected Integer cdfluxocaixaorigem;
	protected Fluxocaixa fluxocaixa;
	protected Ordemcompra ordemcompra;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Fluxocaixaorigem(){
	}

	public Fluxocaixaorigem(Fluxocaixa fluxocaixa, Ordemcompra ordemcompra){
		this.fluxocaixa = fluxocaixa;
		this.ordemcompra = ordemcompra;
	}
	
	@Id
	@GeneratedValue(generator="sq_fluxocaixaorigem",strategy=GenerationType.AUTO)
	public Integer getCdfluxocaixaorigem() {
		return cdfluxocaixaorigem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemcompra")
	public Ordemcompra getOrdemcompra() {
		return ordemcompra;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfluxocaixa")
	public Fluxocaixa getFluxocaixa() {
		return fluxocaixa;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdfluxocaixaorigem(Integer cdfluxocaixaorigem) {
		this.cdfluxocaixaorigem = cdfluxocaixaorigem;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setFluxocaixa(Fluxocaixa fluxocaixa) {
		this.fluxocaixa = fluxocaixa;
	}
	public void setOrdemcompra(Ordemcompra ordemcompra) {
		this.ordemcompra = ordemcompra;
	}
}
