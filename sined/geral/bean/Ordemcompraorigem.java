package br.com.linkcom.sined.geral.bean;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_ordemcompraorigem", sequenceName = "sq_ordemcompraorigem")
@DisplayName("Requisição Origem")
public class Ordemcompraorigem {

	protected Integer cdordemcompraorigem;
	protected Ordemcompra ordemcompra;
	protected Solicitacaocompra solicitacaocompra;
	protected Planejamento planejamento;
		
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ordemcompraorigem")
	@DescriptionProperty
	public Integer getCdordemcompraorigem() {
		return cdordemcompraorigem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemcompra")
	public Ordemcompra getOrdemcompra() {
		return ordemcompra;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsolicitacaocompra")
	public Solicitacaocompra getSolicitacaocompra() {
		return solicitacaocompra;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdplanejamento")
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	
	public void setCdordemcompraorigem(Integer cdordemcompraorigem) {
		this.cdordemcompraorigem = cdordemcompraorigem;
	}
	public void setOrdemcompra(Ordemcompra ordemcompra) {
		this.ordemcompra = ordemcompra;
	}
	public void setSolicitacaocompra(Solicitacaocompra solicitacaocompra) {
		this.solicitacaocompra = solicitacaocompra;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
}
