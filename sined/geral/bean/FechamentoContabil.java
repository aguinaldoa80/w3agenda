package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_fechamentocontabil", sequenceName="sq_fechamentocontabil")
public class FechamentoContabil implements Log {
	protected Integer cdFechamentoContabil;
	protected Empresa empresa;
	protected Date dtfechamento;
	protected String motivo;
	protected String historico;
	
	protected List<FechamentoContabilHistorico> listaFechamentoContabilHistorico;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_fechamentocontabil")
	@DisplayName("C�digo")
	public Integer getCdFechamentoContabil() {
		return cdFechamentoContabil;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Required
	@DisplayName("Data")
	public Date getDtfechamento() {
		return dtfechamento;
	}
	
	@DisplayName("Motivo")
	public String getMotivo() {
		return motivo;
	}

	@DisplayName("Hist�rico")
	public String getHistorico() {
		return historico;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="fechamentoContabil")
	public List<FechamentoContabilHistorico> getListaFechamentoContabilHistorico() {
		return listaFechamentoContabilHistorico;
	}

	public void setCdFechamentoContabil(Integer cdFechamentoContabil) {
		this.cdFechamentoContabil = cdFechamentoContabil;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setDtfechamento(Date dtfechamento) {
		this.dtfechamento = dtfechamento;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public void setHistorico(String historico) {
		this.historico = historico;
	}

	public void setListaFechamentoContabilHistorico(List<FechamentoContabilHistorico> listaFechamentoContabilHistorico) {
		this.listaFechamentoContabilHistorico = listaFechamentoContabilHistorico;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
}
