package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_registroatividadehora", sequenceName = "sq_registroatividadehora")
public class Registroatividadehora {

	protected Integer cdregistroatividadehora;
	protected Timestamp inicio;
	protected Requisicao requisicao;
	protected Colaborador colaborador;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_registroatividadehora")
	public Integer getCdregistroatividadehora() {
		return cdregistroatividadehora;
	}
	public Timestamp getInicio() {
		return inicio;
	}
	@JoinColumn(name="cdrequisicao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Requisicao getRequisicao() {
		return requisicao;
	}
	@JoinColumn(name="cdcolaborador")
	@ManyToOne(fetch=FetchType.LAZY)
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setCdregistroatividadehora(Integer cdregistroatividadehora) {
		this.cdregistroatividadehora = cdregistroatividadehora;
	}
	public void setInicio(Timestamp inicio) {
		this.inicio = inicio;
	}
	public void setRequisicao(Requisicao requisicao) {
		this.requisicao = requisicao;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
}
