package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_dashboardespecificopapel", sequenceName = "sq_dashboardespecificopapel")
public class Dashboardespecificopapel {

	protected Integer cddashboardespecificopapel;
	protected Dashboardespecifico dashboardespecifico;
	protected Papel papel;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_dashboardespecificopapel")
	public Integer getCddashboardespecificopapel() {
		return cddashboardespecificopapel;
	}
	
	@Required
	@DisplayName("Dashboard específico")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddashboardespecifico")
	public Dashboardespecifico getDashboardespecifico() {
		return dashboardespecifico;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpapel")
	public Papel getPapel() {
		return papel;
	}
	
	public void setCddashboardespecificopapel(Integer id) {
		this.cddashboardespecificopapel = id;
	}

	public void setDashboardespecifico(Dashboardespecifico dashboardespecifico) {
		this.dashboardespecifico = dashboardespecifico;
	}
	
	public void setPapel(Papel papel) {
		this.papel = papel;
	}

}
