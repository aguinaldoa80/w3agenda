package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.Email;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;

@Entity
public class Contato extends Pessoa implements Log{

	protected String emailcontato;
	protected Municipio municipio;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Contatotipo contatotipo;
	protected Boolean receberboleto = false;
	protected Boolean responsavelos = false;
	protected Integer participacao;
	protected Integer quotas;
	protected Empresa empresa;
	protected Boolean ativo = Boolean.TRUE;
	protected String identidade;
	protected Boolean recebernf = false;
	protected List<PessoaContato> listaPessoa = new ListSet<PessoaContato>(PessoaContato.class);
	
//	TRANSIENTES
	protected Pessoa pessoa;
	protected Integer indexlista;
	protected Uf uf;
	protected Telefone telefonePrincipal;
	protected Telefone fax;
	protected Telefone celular;
	protected Fornecedor fornecedor;
	protected Contato contatotrans;
	protected String cdpessoaParam;
	protected Long idandroid;
	protected String telefonesSeparadosPorBarra;
	
	public Contato(){}
	
	public Contato(Integer cdpessoa){
		this.cdpessoa = cdpessoa;
	}
	@OneToMany(mappedBy="contato")
	public List<PessoaContato> getListaPessoa() {
		return listaPessoa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontatotipo")
	@DisplayName("Tipo contato")
	public Contatotipo getContatotipo() {
		return contatotipo;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@DisplayName("E-mail")
	@MaxLength(50)
	@Email
	public String getEmailcontato() {
		return emailcontato;
	}
	public String getIdentidade() {
		return identidade;
	}	
	public void setIdentidade(String identidade) {
		this.identidade = identidade;
	}
	public void setEmailcontato(String emailcontato) {
		this.emailcontato = StringUtils.trimToNull(emailcontato);
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setListaPessoa(List<PessoaContato> listaPessoa) {
		this.listaPessoa = listaPessoa;
	}
	
	@Transient
	public String getUnidade(){
		return getMunicipio() != null ? getMunicipio().getNome() + "/" + getMunicipio().getUf().getSigla() : "";
	}
	
	@Transient
	public String getTelefones(){
		
/*		Set<Telefone> lista = getListaTelefone();
		String string = "";
		String linha = "";
		if (lista != null && lista.size() > 0) {
			for (Telefone telefone : lista) {
				if (telefone.getTelefone() != null) {
					linha += telefone.getTelefone().toString();
				}
				if (telefone.getTelefonetipo() != null && telefone.getTelefonetipo().getNome() != null) {
					linha += "(" + telefone.getTelefonetipo().getNome() + ")";
				} 
				string += linha;
				if (!linha.equals("")) {
					string += "<br>";
					linha = "";
				} 
			}
			return string.substring(0, string.length()-4);
		}*/
		return getTelefones("<br>");
	}
	
	@Transient
	public String getTelefonesSeparadosPorBarra(){
		telefonesSeparadosPorBarra = getTelefones(" | ");
		return telefonesSeparadosPorBarra;
	}
	
	public void setTelefonesSeparadosPorBarra(String telefonesSeparadosPorBarra) {
		this.telefonesSeparadosPorBarra = telefonesSeparadosPorBarra;
	}
	
	@Transient
	public String getTelefones(String separador){
		Set<Telefone> lista = getListaTelefone();
		String string = "";
		String linha = "";
		if (lista != null && lista.size() > 0) {
			for (Telefone telefone : lista) {
				if (telefone.getTelefone() != null) {
					linha += telefone.getTelefone().toString();
				}
				if (telefone.getTelefonetipo() != null && telefone.getTelefonetipo().getNome() != null) {
					linha += "(" + telefone.getTelefonetipo().getNome() + ")";
				} 
				string += linha;
				if (!linha.equals("")) {
					string += separador;
					linha = "";
				} 
			}
			return string.substring(0, string.length()-separador.length());
		}
		return string;
	}
	
	@Transient
	public Integer getIndexlista() {
		return indexlista;
	}
	
	public void setIndexlista(Integer indexlista) {
		this.indexlista = indexlista;
	}
	
	@Transient
	public Uf getUf() {
		return uf;
	}
	
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	
	@Transient
	public Telefone getTelefonePrincipal() {
		return telefonePrincipal;
	}
	
	public void setTelefonePrincipal(Telefone telefonePrincipal) {
		this.telefonePrincipal = telefonePrincipal;
	}
	
	@Transient
	public Telefone getFax() {
		return fax;
	}
	
	public void setFax(Telefone fax) {
		this.fax = fax;
	}
	
	@Transient
	public Telefone getCelular() {
		return celular;
	}
	
	public void setCelular(Telefone celular) {
		this.celular = celular;
	}	
	
	@Transient
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	@Transient
	public Contato getContatotrans() {
		return contatotrans;
	}
	
	public void setContatotrans(Contato contatotrans) {
		this.contatotrans = contatotrans;
	}
	
	public void setContatotipo(Contatotipo contatotipo) {
		this.contatotipo = contatotipo;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Contato) {
			Contato contato = (Contato) obj;
			return this.getCdpessoa().equals(contato.getCdpessoa());
		}
		return super.equals(obj);
	}
	
	@Transient
	public String getCdpessoaParam() {
		return cdpessoaParam;
	}
	public void setCdpessoaParam(String cdpessoaParam) {
		this.cdpessoaParam = cdpessoaParam;
	}
	@DisplayName("Receber Boleto")
	public Boolean getReceberboleto() {
		return receberboleto;
	}
	public void setReceberboleto(Boolean receberboleto) {
		this.receberboleto = receberboleto;
	}
	@DisplayName("Respons�vel")
	public Boolean getResponsavelos() {
		return responsavelos;
	}
	public void setResponsavelos(Boolean responsavelos) {
		this.responsavelos = responsavelos;
	}
	
	@Transient
	public String getTelefonesHtml() {
		Set<br.com.linkcom.sined.geral.bean.Telefone> listaTelefone = getListaTelefone();
		StringBuilder telefones = new StringBuilder("");
		if (listaTelefone != null && !listaTelefone.isEmpty()) {
			for (br.com.linkcom.sined.geral.bean.Telefone telefone : listaTelefone) {
				if(telefone.getTelefonetipo() != null)
					telefones.append(telefone.getTelefone().toString() + "(" + telefone.getTelefonetipo().getNome()).append(")<BR>");
			}
		}
		return telefones.toString();
	}
	
	@DisplayName("Participa��o")
	@MaxLength(10)
	public Integer getParticipacao() {
		return participacao;
	}
	@DisplayName("Quotas")
	@MaxLength(10)
	public Integer getQuotas() {
		return quotas;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	@DisplayName("Restrito � Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setParticipacao(Integer participacao) {
		this.participacao = participacao;
	}
	public void setQuotas(Integer quotas) {
		this.quotas = quotas;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	@Transient
	public Long getIdandroid() {
		return idandroid;
	}
	public void setIdandroid(Long idandroid) {
		this.idandroid = idandroid;
	}
	
	@DisplayName("Receber NF")
	public Boolean getRecebernf() {
		return recebernf;
	}
	public void setRecebernf(Boolean recebernf) {
		this.recebernf = recebernf;
	}
	
	@Transient
	public Pessoa getPessoa() {
		return pessoa;
	}
	
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
}