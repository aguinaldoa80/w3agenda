package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_clientesegmento", sequenceName = "sq_clientesegmento")
public class ClienteSegmento {

	protected Integer cdclientesegmento;
	protected Segmento segmento;
	protected Cliente cliente;
	protected boolean principal;

	public ClienteSegmento() {
		
	}
	public ClienteSegmento(boolean principal) {
		this.principal = principal;
	}

	public ClienteSegmento(Contacrmsegmento ccs) {
		this.segmento = ccs.getSegmento();
	}
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_clientesegmento")
	public Integer getCdclientesegmento() {
		return cdclientesegmento;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsegmento")
	public Segmento getSegmento() {
		return segmento;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	@Required
	public boolean isPrincipal() {
		return principal;
	}

	public void setPrincipal(boolean principal) {
		this.principal = principal;
	}

	public void setCdclientesegmento(Integer cdclientesegmento) {
		this.cdclientesegmento = cdclientesegmento;
	}
	public void setSegmento(Segmento segmento) {
		this.segmento = segmento;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
}
