package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Email;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_ordemcompra;
import br.com.linkcom.sined.geral.bean.enumeration.Tipofrete;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.Pessoaquestionario;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoFornecedorEmpresa;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@Entity
@SequenceGenerator(name = "sq_ordemcompra", sequenceName = "sq_ordemcompra")
@DisplayName("Ordem de Compra")
@JoinEmpresa("ordemcompra.empresa")
public class Ordemcompra implements Log, PermissaoProjeto, PermissaoFornecedorEmpresa {

	protected Integer cdordemcompra;
	protected Integer cdUsuarioAltera;
	protected Timestamp dtAltera;

	protected Fornecedor fornecedor;
	protected Fornecedor fornecedoroptriangular;
	protected Endereco enderecoentrega;
	protected Cotacao cotacao;
	protected Localarmazenagem localarmazenagem;
	protected Empresa empresa;
	protected Prazopagamento prazopagamento;
	protected Contato contato;
	protected Rateio rateio;
	// protected Fluxocaixa fluxocaixa;
	protected Date dtcriacao = SinedDateUtils.currentDate();
	protected Documentotipo documentotipo;

	protected String observacao;
	protected Date dtproximaentrega;
	protected Money desconto;
	protected Money frete;
	protected Money valoricmsst;
	protected Boolean rateioverificado;
	protected String inspecao;

	protected List<Ordemcompramaterial> listaMaterial = new ArrayList<Ordemcompramaterial>();
	protected List<Ordemcomprahistorico> listaOrdemcomprahistorico;
	protected Set<Ordemcompraarquivo> listaArquivosordemcompra = new ListSet<Ordemcompraarquivo>(
			Ordemcompraarquivo.class);
	protected List<Fluxocaixaorigem> listaFluxocaixaorigem = new ListSet<Fluxocaixaorigem>(
			Fluxocaixaorigem.class);
	protected List<Ordemcompraorigem> listaOrdemcompraorigem;
	protected List<Ordemcompramaterial> listaMaterialTrans;
	protected Set<Ordemcompraentrega> listaOrdemcompraentrega;
	protected Set<Ordemcomprafornecimentocontrato> listaOrdemcomprafornecimentocontrato;

	protected Aux_ordemcompra aux_ordemcompra;
	protected Date dtcancelamento;
	protected Date dtpedido;
	protected Date dtautorizacao;
	protected Date dtaprova;
	protected Date dtbaixa;
	protected Boolean faturamentocliente;
	protected Tipofrete tipofrete;
	protected Garantia garantia;

	protected String observacoesinternas;
	protected Fornecedor transportador;
	protected Boolean ordemcompraantecipada;
	protected Pessoaquestionario pessoaquestionario;

	// TRANSIENTES
	protected String identificadortela;
	protected ImportacaoXmlNfeBean importacaoXmlNfeBean;
	protected String idsEntregas;
	protected Money valorprodutos;
	protected Money valortotal;
	protected List<Ordemcompramaterial> listaMaterialAux;
	protected Material material;
	protected String entregas;
	protected Ordemcomprahistorico ordemcomprahistorico;
	protected String controller;
	protected Boolean enviarpedidocomemail = Boolean.TRUE;
	protected String observacaoGrupomaterial;
	protected String dtsPagamentoPrevisto;
	protected Integer nivelEstrutura = 1;
	protected Double valorTotalCSV;
	protected Arquivo arquivoxmlnfe;
	protected Double saldo;
	protected Double porcentagemfrete;
	protected String chaveacesso;
	protected Integer cdpedidovenda;
	protected Empresa empresacalculorateio;
	protected String whereInSolicitacaocompra;
	protected String whereInPedidovenda;
	protected String whereIn;
	protected Boolean fromConferenciaXml;
	protected Boolean naoTemPermissao;
	protected Money valorTotalOutrasDespesas;
	protected Money valorTotalIpi;
	protected Money valorTotalIcms;
	protected Arquivo arquivoimportacao;
	protected boolean verificarOrdemCompraCriar = true;
	protected Boolean existItemGrade;
	protected Boolean receberEntregaByMaterialmestre;
	protected String htmlChaveacesso;
	protected String whereInProducaoagenda;
	protected Boolean erroSalvar = false;
	protected Boolean isFornecedorAvaliadoForOrdemCompra = false;
	protected String whereInOCSolicitarrestante;
	
	// Atibutos transients utilizados para enviar email
	protected List<Contato> listaDestinatarios = new ListSet<Contato>(
			Contato.class);
	protected List<Arquivo> listaArquivos = new ListSet<Arquivo>(Arquivo.class);
	protected String remetente;
	protected String assunto;
	protected String email;
	protected Boolean enviarordemcompra = Boolean.TRUE;
	protected Money valorTotalOrdemCompraReport;
	protected String rateioCSV = "";
	protected Boolean agrupamentoMesmoMaterial = Boolean.FALSE;
	protected List<String> listaErros;
	private String whereInCdvendaorcamento;
	protected String whereInPlanejamento;

	public Ordemcompra() {
	}

	public Ordemcompra(Integer cdordemcompra) {
		this.cdordemcompra = cdordemcompra;
	}

	public Ordemcompra(Integer cdfornecedor, Integer cdlocalarmazenagem) {
		Fornecedor fornecedor = new Fornecedor();
		fornecedor.setCdpessoa(cdfornecedor);

		Localarmazenagem localarmazenagem = new Localarmazenagem();
		localarmazenagem.setCdlocalarmazenagem(cdlocalarmazenagem);

		setFornecedor(fornecedor);
		setLocalarmazenagem(localarmazenagem);
	}

	public Ordemcompra(Integer cdordemcompra, String observacao,
			Fornecedor fornecedor) {
		this.cdordemcompra = cdordemcompra;
		this.observacao = observacao;
		this.fornecedor = fornecedor;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_ordemcompra")
	@DisplayName("Compra")
	public Integer getCdordemcompra() {
		return cdordemcompra;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdfornecedoroptriangular")
	@DisplayName("Fornecedor")
	public Fornecedor getFornecedoroptriangular() {
		return fornecedoroptriangular;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdenderecoentrega")
	@DisplayName("Endere�o")
	public Endereco getEnderecoentrega() {
		return enderecoentrega;
	}

	@DisplayName("Local")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdlocalarmazenagem")
	@Required
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	@MaxLength(1024)
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public Money getDesconto() {
		return desconto;
	}

	@Required
	@DisplayName("Prazo de pgto.")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdprazopagamento")
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}

	@DisplayName("Cota��o")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcotacao")
	public Cotacao getCotacao() {
		return cotacao;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdgarantia")
	public Garantia getGarantia() {
		return garantia;
	}

	@DisplayName("Pr�xima entrega")
	public Date getDtproximaentrega() {
		return dtproximaentrega;
	}

	@DisplayName("Materiais")
	@OneToMany(mappedBy = "ordemcompra")
	public List<Ordemcompramaterial> getListaMaterial() {
		return listaMaterial;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontato")
	public Contato getContato() {
		return contato;
	}

	@DisplayName("Tipo de documento")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cddocumentotipo")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}

	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}

	public Timestamp getDtaltera() {
		return dtAltera;
	}

	public Money getFrete() {
		return frete;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdordemcompra", insertable = false, updatable = false)
	public Aux_ordemcompra getAux_ordemcompra() {
		return aux_ordemcompra;
	}

	public Date getDtcancelamento() {
		return dtcancelamento;
	}

	@DisplayName("Dt. de compra")
	@Required
	public Date getDtcriacao() {
		return dtcriacao;
	}

	public Date getDtpedido() {
		return dtpedido;
	}

	public Date getDtautorizacao() {
		return dtautorizacao;
	}

	public Date getDtbaixa() {
		return dtbaixa;
	}

	@DisplayName("Hist�rico")
	@OneToMany(mappedBy = "ordemcompra")
	public List<Ordemcomprahistorico> getListaOrdemcomprahistorico() {
		return listaOrdemcomprahistorico;
	}

	@DisplayName("Arquivos")
	@OneToMany(mappedBy = "ordemcompra")
	public Set<Ordemcompraarquivo> getListaArquivosordemcompra() {
		return listaArquivosordemcompra;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdrateio")
	public Rateio getRateio() {
		return rateio;
	}

	@DisplayName("Rateio Verificado")
	public Boolean getRateioverificado() {
		return rateioverificado;
	}

	// @ManyToOne(fetch=FetchType.LAZY)
	// @JoinColumn(name="cdfluxocaixa")
	// public Fluxocaixa getFluxocaixa() {
	// return fluxocaixa;
	// }
	@DisplayName("Faturamento cliente")
	public Boolean getFaturamentocliente() {
		return faturamentocliente;
	}

	@DisplayName("Tipo do frete")
	public Tipofrete getTipofrete() {
		return tipofrete;
	}

	public Date getDtaprova() {
		return dtaprova;
	}

	@DisplayName("ICMS/ST")
	public Money getValoricmsst() {
		return valoricmsst;
	}

	@Transient
	public String getWhereIn() {
		return whereIn;
	}

	@OneToMany(mappedBy = "ordemcompra")
	public Set<Ordemcompraentrega> getListaOrdemcompraentrega() {
		return listaOrdemcompraentrega;
	}

	@OneToMany(mappedBy = "ordemcompra")
	public Set<Ordemcomprafornecimentocontrato> getListaOrdemcomprafornecimentocontrato() {
		return listaOrdemcomprafornecimentocontrato;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@DisplayName("Transportador")
	@JoinColumn(name = "cdtransportador")
	public Fornecedor getTransportador() {
		return transportador;
	}

	@DisplayName("Origem")
	@OneToMany(mappedBy = "ordemcompra")
	public List<Ordemcompraorigem> getListaOrdemcompraorigem() {
		return listaOrdemcompraorigem;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoaquestionario")
	public Pessoaquestionario getPessoaquestionario() {
		return pessoaquestionario;
	}

	public void setListaOrdemcompraorigem(
			List<Ordemcompraorigem> listaOrdemcompraorigem) {
		this.listaOrdemcompraorigem = listaOrdemcompraorigem;
	}

	public void setTransportador(Fornecedor transportador) {
		this.transportador = transportador;
	}

	public void setListaOrdemcompraentrega(
			Set<Ordemcompraentrega> listaOrdemcompraentrega) {
		this.listaOrdemcompraentrega = listaOrdemcompraentrega;
	}

	public void setListaOrdemcomprafornecimentocontrato(
			Set<Ordemcomprafornecimentocontrato> listaOrdemcomprafornecimentocontrato) {
		this.listaOrdemcomprafornecimentocontrato = listaOrdemcomprafornecimentocontrato;
	}

	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

	public void setValoricmsst(Money valoricmsst) {
		this.valoricmsst = valoricmsst;
	}

	public void setDtaprova(Date dtaprova) {
		this.dtaprova = dtaprova;
	}

	public void setGarantia(Garantia garantia) {
		this.garantia = garantia;
	}

	public void setTipofrete(Tipofrete tipofrete) {
		this.tipofrete = tipofrete;
	}

	public void setRateioverificado(Boolean rateioverificado) {
		this.rateioverificado = rateioverificado;
	}

	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}

	public void setDtcancelamento(Date dtcancelamento) {
		this.dtcancelamento = dtcancelamento;
	}

	public void setDtpedido(Date dtpedido) {
		this.dtpedido = dtpedido;
	}

	public void setDtautorizacao(Date dtautorizacao) {
		this.dtautorizacao = dtautorizacao;
	}

	public void setDtbaixa(Date dtbaixa) {
		this.dtbaixa = dtbaixa;
	}

	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}

	public void setCdordemcompra(Integer cdordemcompra) {
		this.cdordemcompra = cdordemcompra;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public void setFornecedoroptriangular(Fornecedor fornecedoroptriangular) {
		this.fornecedoroptriangular = fornecedoroptriangular;
	}

	public void setEnderecoentrega(Endereco enderecoentrega) {
		this.enderecoentrega = enderecoentrega;
	}

	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}

	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}

	public void setCotacao(Cotacao cotacao) {
		this.cotacao = cotacao;
	}

	public void setDtproximaentrega(Date dtproximaentrega) {
		this.dtproximaentrega = dtproximaentrega;
	}

	public void setListaMaterial(List<Ordemcompramaterial> listaMaterial) {
		this.listaMaterial = listaMaterial;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}

	public void setDtcriacao(Date dtcriacao) {
		this.dtcriacao = dtcriacao;
	}

	public void setFrete(Money frete) {
		this.frete = frete;
	}

	public void setAux_ordemcompra(Aux_ordemcompra aux_ordemcompra) {
		this.aux_ordemcompra = aux_ordemcompra;
	}

	public void setListaOrdemcomprahistorico(
			List<Ordemcomprahistorico> listaOrdemcomprahistorico) {
		this.listaOrdemcomprahistorico = listaOrdemcomprahistorico;
	}

	public void setListaArquivosordemcompra(
			Set<Ordemcompraarquivo> listaArquivosordemcompra) {
		this.listaArquivosordemcompra = listaArquivosordemcompra;
	}

	public void setPessoaquestionario(Pessoaquestionario pessoaquestionario) {
		this.pessoaquestionario = pessoaquestionario;
	}

	// public void setFluxocaixa(Fluxocaixa fluxocaixa) {
	// this.fluxocaixa = fluxocaixa;
	// }
	public void setFaturamentocliente(Boolean faturamentocliente) {
		this.faturamentocliente = faturamentocliente;
	}

	// ============ TRANSIENT'S ==================

	@Transient
	public Integer getNivelEstrutura() {
		return nivelEstrutura;
	}

	public void setNivelEstrutura(Integer nivelEstrutura) {
		this.nivelEstrutura = nivelEstrutura;
	}

	@Transient
	@DisplayName("Valor")
	public Money getValorprodutos() {
		return valorprodutos;
	}

	@Transient
	@DisplayName("Valor Total")
	public Money getValortotal() {
		return valortotal;
	}

	@Transient
	public String getIdsEntregas() {
		return idsEntregas;
	}

	@Transient
	public String getMateriais() {
		if (this.getListaMaterial() == null
				|| this.getListaMaterial().size() == 0)
			return "";
		if (this.getListaMaterial().size() > 10)
			return "Diversos";

		StringBuilder stringBuilder = new StringBuilder();
		for (Ordemcompramaterial ordemcompramaterial : this.getListaMaterial())
			stringBuilder.append(ordemcompramaterial.getMaterial()
					.getAutocompleteDescription()
					+ ", ");

		stringBuilder
				.delete(stringBuilder.length() - 2, stringBuilder.length());
		return stringBuilder.toString();
	}

	@Transient
	@DisplayName("Entregas listagem")
	public String getEntregasListagemCSV() {
		if (this.getListaOrdemcompraentrega() == null) {
			return "";
		} else if (this.getListaOrdemcompraentrega().size() > 10) {
			return "Diversos";
		} else {
			StringBuilder stringBuilder = new StringBuilder();
			int i = 1;

			for (Ordemcompraentrega ordemcompraentrega : this
					.getListaOrdemcompraentrega()) {
				Entrega entrega = ordemcompraentrega.getEntrega();
				if (entrega != null && entrega.getDtcancelamento() == null) {
					stringBuilder.append(i + "� Entrega "
							+ entrega.getCdentrega() + " ");

					if (entrega.getListaDocumentoorigem() != null
							&& entrega.getListaDocumentoorigem().size() > 0) {
						stringBuilder.append(" Conta(s) ");
						for (Documentoorigem documentoorigem : entrega
								.getListaDocumentoorigem())
							stringBuilder.append(documentoorigem.getDocumento()
									.getCddocumento()
									+ " ");
					}
					if (stringBuilder.indexOf(", ", stringBuilder.length() - 2) != -1)
						stringBuilder.delete(stringBuilder.length() - 2,
								stringBuilder.length());
					i++;
				}
			}

			return stringBuilder.toString();
		}
	}

	@Transient
	@DisplayName("Entregas listagem")
	public String getEntregasListagem() {
		if (this.getListaOrdemcompraentrega() == null) {
			return "";
		} else if (this.getListaOrdemcompraentrega().size() > 10) {
			return "Diversos";
		} else {
			StringBuilder stringBuilder = new StringBuilder();
			int i = 1;

			for (Ordemcompraentrega ordemcompraentrega : this
					.getListaOrdemcompraentrega()) {
				Entrega entrega = ordemcompraentrega.getEntrega();
				if (entrega != null && entrega.getDtcancelamento() == null) {
					stringBuilder
							.append(i
									+ "� Entrega <a href=\"#\" onclick=\"redirecionaEntrega("
									+ entrega.getCdentrega() + ")\">"
									+ entrega.getCdentrega() + "</a>");

					if (entrega.getListaDocumentoorigem() != null
							&& entrega.getListaDocumentoorigem().size() > 0) {
						stringBuilder.append(" Conta(s) ");
						for (Documentoorigem documentoorigem : entrega
								.getListaDocumentoorigem())
							stringBuilder
									.append("<a href=\"#\" onclick=\"redirecionaPagamento("
											+ documentoorigem.getDocumento()
													.getCddocumento()
											+ ")\">"
											+ documentoorigem.getDocumento()
													.getCddocumento()
											+ "</a>, ");
					}
					if (stringBuilder.indexOf(", ", stringBuilder.length() - 2) != -1)
						stringBuilder.delete(stringBuilder.length() - 2,
								stringBuilder.length());
					stringBuilder.append("<BR>");
					i++;
				}
			}

			return stringBuilder.toString();
		}
	}

	@Transient
	public String getEntregasReport() {
		if (this.getListaOrdemcompraentrega() == null) {
			return "";
		} else if (this.getListaOrdemcompraentrega().size() > 10) {
			return "Diversos";
		} else {
			StringBuilder stringBuilder = new StringBuilder();
			int i = 1;

			for (Ordemcompraentrega ordemcompraentrega : this
					.getListaOrdemcompraentrega()) {
				Entrega entrega = ordemcompraentrega.getEntrega();
				if (entrega != null && entrega.getDtcancelamento() == null) {
					stringBuilder.append(i + "� Entrega "
							+ entrega.getCdentrega());

					if (entrega.getListaDocumentoorigem() != null
							&& entrega.getListaDocumentoorigem().size() > 0) {
						stringBuilder.append(" Conta(s) ");
						for (Documentoorigem documentoorigem : entrega
								.getListaDocumentoorigem())
							stringBuilder.append(documentoorigem.getDocumento()
									.getCddocumento()
									+ ", ");
					}
					if (stringBuilder.indexOf(", ", stringBuilder.length() - 2) != -1)
						stringBuilder.delete(stringBuilder.length() - 2,
								stringBuilder.length());
					stringBuilder.append("<BR>");
					i++;
				}
			}

			return stringBuilder.toString();
		}
	}

	@Transient
	public List<Ordemcompramaterial> getListaMaterialAux() {
		return listaMaterialAux;
	}

	@Transient
	public Material getMaterial() {
		return material;
	}

	@Transient
	@DisplayName("Fornecedor")
	public String getFornecedorpercentual() {
		Integer percentual = this.aux_ordemcompra.getEntregapercentual() == null ? 0
				: this.aux_ordemcompra.getEntregapercentual();
		return this.fornecedor.getNome() + " (" + percentual + "%)";
	}

	@Transient
	public Ordemcomprahistorico getOrdemcomprahistorico() {
		return ordemcomprahistorico;
	}

	@Transient
	// M�todo igual o abaixo por�m soma impostos
	public Double getValorTotalOrdemCompraReport() {
		Double valorOrdemCompra = 0.0;
		for (Ordemcompramaterial ordemcompramaterial : this.getListaMaterial()) {
			valorOrdemCompra = valorOrdemCompra
					+ (ordemcompramaterial.getValor() != null ? ordemcompramaterial
							.getValor()
							: 0.0)
//					+ (ordemcompramaterial.getIcmsissincluso() != null
//							&& !ordemcompramaterial.getIcmsissincluso()
//							&& ordemcompramaterial.getIcmsiss() != null ? ordemcompramaterial
//							.getIcmsiss()
//							: 0.0)
//					+ (ordemcompramaterial.getIpiincluso() != null
//							&& !ordemcompramaterial.getIpiincluso()
//							&& ordemcompramaterial.getIpi() != null ? ordemcompramaterial
//							.getIpi()
//							: 0.0)
							;
			if (ordemcompramaterial.getValoroutrasdespesas() != null) {
				valorOrdemCompra = valorOrdemCompra
						+ ordemcompramaterial.getValoroutrasdespesas()
								.getValue().doubleValue();
			}
		}
		valorOrdemCompra = valorOrdemCompra
				+ (this.getValoricmsst() != null ? this.getValoricmsst()
						.getValue().doubleValue() : 0.0)
				+ (this.getFrete() != null ? this.getFrete().getValue()
						.doubleValue() : 0.0)
				+ (this.getValorTotalIpi() != null ? this.getValorTotalIpi().getValue()
						.doubleValue() : 0.0)
				+ (this.getValorTotalIcms() != null ? this.getValorTotalIcms().getValue()
						.doubleValue() : 0.0)
				- (this.getDesconto() != null ? this.getDesconto().getValue()
						.doubleValue() : 0.0);

		return valorOrdemCompra;
	}

	@Transient
	public Double getValorTotalOrdemCompra() {
		Double valorOrdemCompra = 0.0;
		for (Ordemcompramaterial ordemcompramaterial : this.getListaMaterial()) {
			valorOrdemCompra = valorOrdemCompra
					+ (ordemcompramaterial.getValor() != null ? ordemcompramaterial
							.getValor()
							: 0.0);

//			if (ordemcompramaterial.getIcmsissincluso() != null
//					&& !ordemcompramaterial.getIcmsissincluso()) {
//				valorOrdemCompra = valorOrdemCompra
//						+ (ordemcompramaterial.getIcmsiss() != null ? ordemcompramaterial
//								.getIcmsiss()
//								: 0.0);
//			}
//
//			if (ordemcompramaterial.getIpiincluso() != null
//					&& !ordemcompramaterial.getIpiincluso()) {
//				valorOrdemCompra = valorOrdemCompra
//						+ (ordemcompramaterial.getIpi() != null ? ordemcompramaterial
//								.getIpi()
//								: 0.0);
//			}
			if (ordemcompramaterial.getValoroutrasdespesas() != null) {
				valorOrdemCompra = valorOrdemCompra
						+ ordemcompramaterial.getValoroutrasdespesas()
								.getValue().doubleValue();
			}

		}
		valorOrdemCompra = SinedUtil.round(valorOrdemCompra, 2);
		
		valorOrdemCompra = valorOrdemCompra
				+ (this.getValoricmsst() != null ? this.getValoricmsst()
						.getValue().doubleValue() : 0.0)
				+ (this.getFrete() != null ? this.getFrete().getValue()
						.doubleValue() : 0.0)
				+ (this.getValorTotalIpi() != null ? this.getValorTotalIpi().getValue()
						.doubleValue() : 0.0)
				+ (this.getValorTotalIcms() != null ? this.getValorTotalIcms().getValue()
						.doubleValue() : 0.0)
				- (this.getDesconto() != null ? this.getDesconto().getValue()
						.doubleValue() : 0.0);

		return valorOrdemCompra;
	}

	@Transient
	public Money getValorTotalOrdemCompraMoney() {
		return new Money(getValorTotalOrdemCompra());
	}

	@Transient
	@DisplayName("Enviar e-mail")
	public Boolean getEnviarpedidocomemail() {
		return enviarpedidocomemail;
	}

	public void setIdsEntregas(String idsEntregas) {
		this.idsEntregas = idsEntregas;
	}

	public void setValorprodutos(Money valorprodutos) {
		this.valorprodutos = valorprodutos;
	}

	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}

	public void setListaMaterialAux(List<Ordemcompramaterial> listaMaterialAux) {
		this.listaMaterialAux = listaMaterialAux;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setOrdemcomprahistorico(
			Ordemcomprahistorico ordemcomprahistorico) {
		this.ordemcomprahistorico = ordemcomprahistorico;
	}

	@Transient
	public Double getValorProdutosAtualizado() {
		Double valorOrdemCompra = 0.0;
		for (Ordemcompramaterial ordemcompramaterial : this.getListaMaterial()) {
			valorOrdemCompra = valorOrdemCompra
					+ (ordemcompramaterial.getValor() != null ? ordemcompramaterial
							.getValor()
							: 0.0);
		}
		return valorOrdemCompra;
	}

	@Transient
	public List<Contato> getListaDestinatarios() {
		return listaDestinatarios;
	}

	@Transient
	@MaxLength(50)
	public String getAssunto() {
		return assunto;
	}

	@Transient
	public String getEmail() {
		return email;
	}

	@Transient
	public List<Arquivo> getListaArquivos() {
		return listaArquivos;
	}

	@Transient
	@MaxLength(50)
	@Email
	public String getRemetente() {
		return remetente;
	}

	@Transient
	public Boolean getEnviarordemcompra() {
		return enviarordemcompra;
	}

	@Transient
	public String getController() {
		return controller;
	}

	@Transient
	public String getObservacaoGrupomaterial() {
		return observacaoGrupomaterial;
	}

	@Transient
	public List<Ordemcompramaterial> getListaMaterialTrans() {
		return listaMaterialTrans;
	}

	public void setListaMaterialTrans(
			List<Ordemcompramaterial> listaMaterialTrans) {
		this.listaMaterialTrans = listaMaterialTrans;
	}

	public void setObservacaoGrupomaterial(String observacaoGrupomaterial) {
		this.observacaoGrupomaterial = observacaoGrupomaterial;
	}

	public void setListaArquivos(List<Arquivo> listaArquivos) {
		this.listaArquivos = listaArquivos;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setListaDestinatarios(List<Contato> listaDestinatarios) {
		this.listaDestinatarios = listaDestinatarios;
	}

	public void setRemetente(String remetente) {
		this.remetente = remetente;
	}

	public void setEnviarordemcompra(Boolean enviarordemcompra) {
		this.enviarordemcompra = enviarordemcompra;
	}

	public void setController(String controller) {
		this.controller = controller;
	}

	public void setEnviarpedidocomemail(Boolean enviarpedidocomemail) {
		this.enviarpedidocomemail = enviarpedidocomemail;
	}

	public String subQueryProjeto() {

		String idsProjetos = SinedUtil.getListaProjeto();

		String select = "select oc1.cdordemcompra " + "from Ordemcompra oc1 "
				+ "left outer join oc1.rateio r1 "
				+ "left outer join r1.listaRateioitem ri1 "
				+ "left outer join ri1.projeto p1 " + "where (" + "("
				+ "oc1.rateioverificado = true " + "and not exists ("
				+ "	select ri2.cdrateioitem " + "	from Rateioitem ri2 "
				+ "	join ri2.rateio r2 " + "	join ri2.projeto p2 "
				+ "	where r1.cdrateio = r2.cdrateio "
				+ "	and p2.cdprojeto not in ("
				+ idsProjetos
				+ ") "
				+ ") "
				+ ") "
				+ "or "
				+ "("
				+ "(oc1.rateioverificado = false or oc1.rateioverificado is null) "
				+ "and "
				+ "("
				+ "not exists ("
				+ "	select ri2.cdrateioitem "
				+ "	from Rateioitem ri2 "
				+ "	join ri2.rateio r2 "
				+ "	join ri2.projeto p2 "
				+ "	where r1.cdrateio = r2.cdrateio "
				+ "	and p2.cdprojeto not in ("
				+ idsProjetos
				+ ") "
				+ ") "
				+ "or r1 is null "
				+ "or ri1 is null "
				+ "or p1 is null "
				+ ") " + ") " + ")";

		return select;

		// return "select ordemcompra1.cdordemcompra " +
		// "from Ordemcompra ordemcompra1 " +
		// "left outer join ordemcompra1.rateio rateio1 " +
		// "left outer join rateio1.listaRateioitem listaRateioitem1 " +
		// "left outer join listaRateioitem1.projeto projeto1 " +
		// "where (ordemcompra1.rateioverificado = true and projeto1.cdprojeto in ("
		// + idsProjetos + ")) " +
		// " or " +
		// " (" +
		// " (ordemcompra1.rateioverificado = false or ordemcompra1.rateioverificado is null) "
		// +
		// " and " +
		// " (rateio1 is null or listaRateioitem1 is null or projeto1 is null or projeto1.cdprojeto in ("
		// + idsProjetos + "))" +
		// ") ";
	}	
	
	public String subQueryFornecedorEmpresa() {
		return "select ordemcompraSubQueryFornecedorEmpresa.cdordemcompra " +
				"from Ordemcompra ordemcompraSubQueryFornecedorEmpresa " +
				"left outer join ordemcompraSubQueryFornecedorEmpresa.fornecedor fornecedorSubQueryFornecedorEmpresa " +
				"where true = permissao_fornecedorempresa(fornecedorSubQueryFornecedorEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}

	public boolean useFunctionProjeto() {
		return false;
	}

	@Transient
	public String getDtsPagamentoPrevisto() {
		if (this.dtpedido != null && this.prazopagamento != null
				&& this.prazopagamento.getCdprazopagamento() != null) {
			StringBuilder sb = new StringBuilder("");
			Date dtAuxReferencia = new Date(this.dtpedido.getTime());
			Date dtAux = null;
			if (this.prazopagamento.getListaPagamentoItem() != null
					&& !this.prazopagamento.getListaPagamentoItem().isEmpty()) {
				for (Prazopagamentoitem prazopagamentoitem : this.prazopagamento
						.getListaPagamentoItem()) {
					if (prazopagamentoitem.getDias() != null)
						dtAux = SinedDateUtils.addDiasData(dtAuxReferencia,
								prazopagamentoitem.getDias());
					if (prazopagamentoitem.getMeses() != null)
						dtAux = SinedDateUtils.addMesData(dtAuxReferencia,
								prazopagamentoitem.getMeses());
					sb.append(SinedDateUtils.toString(dtAux)).append(", ");
				}
				sb.delete(sb.length() - 2, sb.length());
			}
			return sb.toString();
		}
		return dtsPagamentoPrevisto;
	}

	public void setDtsPagamentoPrevisto(String dtsPagamentoPrevisto) {
		this.dtsPagamentoPrevisto = dtsPagamentoPrevisto;
	}

	@OneToMany(mappedBy = "ordemcompra")
	public List<Fluxocaixaorigem> getListaFluxocaixaorigem() {
		return listaFluxocaixaorigem;
	}

	public void setListaFluxocaixaorigem(
			List<Fluxocaixaorigem> listaFluxocaixaorigem) {
		this.listaFluxocaixaorigem = listaFluxocaixaorigem;
	}

	@Transient
	public String getRateioCSV() {
		return rateioCSV;
	}

	public void setRateioCSV(String rateioCSV) {
		this.rateioCSV = rateioCSV;
	}

	@Transient
	public Double getValorTotalCSV() {
		return valorTotalCSV;
	}

	public void setValorTotalCSV(Double valorTotalCSV) {
		this.valorTotalCSV = valorTotalCSV;
	}

	@Transient
	public Arquivo getArquivoxmlnfe() {
		return arquivoxmlnfe;
	}

	public void setArquivoxmlnfe(Arquivo arquivoxmlnfe) {
		this.arquivoxmlnfe = arquivoxmlnfe;
	}

	@Transient
	public ImportacaoXmlNfeBean getImportacaoXmlNfeBean() {
		return importacaoXmlNfeBean;
	}

	public void setImportacaoXmlNfeBean(
			ImportacaoXmlNfeBean importacaoXmlNfeBean) {
		this.importacaoXmlNfeBean = importacaoXmlNfeBean;
	}

	@Transient
	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	@Transient
	public Double getPorcentagemfrete() {
		Double valorTotalAux = 0.0;
		for (Ordemcompramaterial material : listaMaterial) {
			if (material.getValor() != null)
				valorTotalAux += Double.parseDouble(material.getValor()
						.toString().replace(",", "."));
		}
		if (frete != null && valorTotalAux != null && valorTotalAux > 0) {
			String auxiliar = frete.toString().replace(".", "");
			Double valorParcial = Double
					.parseDouble(auxiliar.replace(",", "."));
			porcentagemfrete = (valorParcial * 100) / valorTotalAux;
		}
		return porcentagemfrete;
	}

	public void setPorcentagemfrete(Double porcentagemfrete) {
		this.porcentagemfrete = porcentagemfrete;
	}

	@Transient
	public String getChaveacesso() {
		return chaveacesso;
	}

	public void setChaveacesso(String chaveacesso) {
		this.chaveacesso = chaveacesso;
	}

	@Transient
	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}

	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}

	// novos campos
	@MaxLength(1024)
	@DisplayName("Observa��es Internas")
	public String getObservacoesinternas() {
		return observacoesinternas;
	}

	public void setObservacoesinternas(String observacoesInternas) {
		this.observacoesinternas = observacoesInternas;
	}

	@Transient
	public Empresa getEmpresacalculorateio() {
		return empresacalculorateio;
	}

	public void setEmpresacalculorateio(Empresa empresacalculorateio) {
		this.empresacalculorateio = empresacalculorateio;
	}

	@Transient
	public String getWhereInSolicitacaocompra() {
		return whereInSolicitacaocompra;
	}

	public void setWhereInSolicitacaocompra(String whereInSolicitacaocompra) {
		this.whereInSolicitacaocompra = whereInSolicitacaocompra;
	}

	@Transient
	public String getWhereInPedidovenda() {
		return whereInPedidovenda;
	}

	@Transient
	public String getWhereInCdvendaorcamento() {
		return whereInCdvendaorcamento;
	}

	public void setWhereInPedidovenda(String whereInPedidovenda) {
		this.whereInPedidovenda = whereInPedidovenda;
	}

	@Transient
	public Boolean getAgrupamentoMesmoMaterial() {
		return agrupamentoMesmoMaterial;
	}

	public void setAgrupamentoMesmoMaterial(Boolean agrupamentoMesmoMaterial) {
		this.agrupamentoMesmoMaterial = agrupamentoMesmoMaterial;
	}

	@Transient
	public Boolean getFromConferenciaXml() {
		return fromConferenciaXml;
	}

	public void setFromConferenciaXml(Boolean fromConferenciaXml) {
		this.fromConferenciaXml = fromConferenciaXml;
	}

	@Transient
	public Boolean getNaoTemPermissao() {
		return naoTemPermissao;
	}

	public void setNaoTemPermissao(Boolean naoTemPermissao) {
		this.naoTemPermissao = naoTemPermissao;
	}

	@Transient
	@DisplayName("Outras despesas")
	public Money getValorTotalOutrasDespesas() {
		Money valorTotalOutrasDespesas = new Money();
		for (Ordemcompramaterial ordemcompramaterial : this.getListaMaterial()) {
			if (ordemcompramaterial.getValoroutrasdespesas() != null) {
				valorTotalOutrasDespesas = valorTotalOutrasDespesas
						.add(ordemcompramaterial.getValoroutrasdespesas());
			}
		}

		return valorTotalOutrasDespesas;
	}

	public void setValorTotalOutrasDespesas(Money valorTotalOutrasDespesas) {
		this.valorTotalOutrasDespesas = valorTotalOutrasDespesas;
	}
	
	@Transient
	@DisplayName("IPI")
	public Money getValorTotalIpi() {
		Double valorTotalIpi = 0d;
		for (Ordemcompramaterial ordemcompramaterial : this.getListaMaterial()) {
			if (ordemcompramaterial.getIpi() != null && 
					(ordemcompramaterial.getIpiincluso() == null ||
					 !ordemcompramaterial.getIpiincluso())){
				valorTotalIpi += ordemcompramaterial.getIpi();
			}
		}

		return new Money(valorTotalIpi);
	}

	public void setValorTotalIpi(Money valorTotalIpi) {
		this.valorTotalIpi = valorTotalIpi;
	}
	
	@Transient
	@DisplayName("ICMS")
	public Money getValorTotalIcms() {
		Double valorTotalIcms = 0d;
		for (Ordemcompramaterial ordemcompramaterial : this.getListaMaterial()) {
			if (ordemcompramaterial.getIcmsiss() != null && 
					(ordemcompramaterial.getIcmsissincluso() == null ||
					 !ordemcompramaterial.getIcmsissincluso())){
				valorTotalIcms += ordemcompramaterial.getIcmsiss();
			}
		}

		return new Money(valorTotalIcms);
	}
	
	public void setValorTotalIcms(Money valorTotalIcms) {
		this.valorTotalIcms = valorTotalIcms;
	}

	@Transient
	public Arquivo getArquivoimportacao() {
		return arquivoimportacao;
	}

	public void setArquivoimportacao(Arquivo arquivoimportacao) {
		this.arquivoimportacao = arquivoimportacao;
	}

	@Transient
	public List<String> getListaErros() {
		return listaErros;
	}

	public void setListaErros(List<String> listaErros) {
		this.listaErros = listaErros;
	}

	@Transient
	public boolean isVerificarOrdemCompraCriar() {
		return verificarOrdemCompraCriar;
	}

	public void setVerificarOrdemCompraCriar(boolean verificarOrdemCompraCriar) {
		this.verificarOrdemCompraCriar = verificarOrdemCompraCriar;
	}

	@MaxLength(1000)
	@DisplayName("Inspe��o")
	public String getInspecao() {
		return inspecao;
	}

	public void setInspecao(String inspecao) {
		this.inspecao = inspecao;
	}

	@Transient
	public String getIdentificadortela() {
		return identificadortela;
	}

	public void setIdentificadortela(String identificadortela) {
		this.identificadortela = identificadortela;
	}

	public void setWhereInCdvendaorcamento(String whereInCdvendaorcamento) {
		this.whereInCdvendaorcamento = whereInCdvendaorcamento;
	}

	public Boolean getOrdemcompraantecipada() {
		return ordemcompraantecipada;
	}

	public void setOrdemcompraantecipada(Boolean ordemcompraantecipada) {
		this.ordemcompraantecipada = ordemcompraantecipada;
	}

	@Transient
	public Boolean getExistItemGrade() {
		return existItemGrade;
	}

	@Transient
	public Boolean getReceberEntregaByMaterialmestre() {
		return receberEntregaByMaterialmestre;
	}

	public void setReceberEntregaByMaterialmestre(
			Boolean receberEntregaByMaterialmestre) {
		this.receberEntregaByMaterialmestre = receberEntregaByMaterialmestre;
	}

	public void setExistItemGrade(Boolean existItemGrade) {
		this.existItemGrade = existItemGrade;
	}

	@Transient
	public String getHtmlChaveacesso() {
		return htmlChaveacesso;
	}

	public void setHtmlChaveacesso(String htmlChaveacesso) {
		this.htmlChaveacesso = htmlChaveacesso;
	}

	@Transient
	public String getWhereInProducaoagenda() {
		return whereInProducaoagenda;
	}

	public void setWhereInProducaoagenda(String whereInProducaoagenda) {
		this.whereInProducaoagenda = whereInProducaoagenda;
	}
	
	@Transient
	public Boolean getErroSalvar() {
		return erroSalvar;
	}
	
	public void setErroSalvar(Boolean erroSalvar) {
		this.erroSalvar = erroSalvar;
	}
	@Transient
	public Boolean getIsFornecedorAvaliadoForOrdemCompra() {
		if (this.pessoaquestionario != null && this.pessoaquestionario.getCdpessoaquestionario() != null 
				&& this.pessoaquestionario.getQuestionario() != null
				&& this.pessoaquestionario.getQuestionario().getCdquestionario() != null
				&& this.pessoaquestionario.getQuestionario().getTipopessoaquestionario() != null
				&& this.pessoaquestionario.getQuestionario().getTipopessoaquestionario().equals(Tipopessoaquestionario.FORNECEDOR)) {
			this.isFornecedorAvaliadoForOrdemCompra = true;
			
		}
		return isFornecedorAvaliadoForOrdemCompra;
	}
	public void setIsFornecedorAvaliadoForOrdemCompra(Boolean isFornecedorAvaliadoForOrdemCompra) {
		this.isFornecedorAvaliadoForOrdemCompra = isFornecedorAvaliadoForOrdemCompra;
	}

	@Transient
	public String getWhereInOCSolicitarrestante() {
		return whereInOCSolicitarrestante;
	}
	public void setWhereInOCSolicitarrestante(String whereInOCSolicitarrestante) {
		this.whereInOCSolicitarrestante = whereInOCSolicitarrestante;
	}

	@Transient
	public String getWhereInPlanejamento() {
		return whereInPlanejamento;
	}

	public void setWhereInPlanejamento(String whereInPlanejamento) {
		this.whereInPlanejamento = whereInPlanejamento;
	}
	
	

	
	
	
}