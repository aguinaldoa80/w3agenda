package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_dominiospf", sequenceName = "sq_dominiospf")
@DisplayName("SPF")
public class Dominiospf implements Log{
	
	protected Integer cddominiospf;
	protected Contratomaterial contratomaterial;
	protected Dominio dominio;
	protected Integer versao;
	protected String acao;
	protected Dominio dominioredirecionar;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
			
	protected Set<Dominiospfip> listaDominiospfip;
	
	//Transient
	protected Cliente cliente;
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_dominiospf")
	public Integer getCddominiospf() {
		return cddominiospf;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratomaterial")
	@DisplayName("Contrato de material")
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddominio")
	@DisplayName("Dom�nio principal")
	public Dominio getDominio() {
		return dominio;
	}

	@Required
	@DisplayName("Vers�o")
	@MaxLength(9)
	public Integer getVersao() {
		return versao;
	}

	@Required
	@DisplayName("A��o")
	@MaxLength(50)
	public String getAcao() {
		return acao;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddominioredirecionar")
	@DisplayName("Redirecionar")
	public Dominio getDominioredirecionar() {
		return dominioredirecionar;
	}

	@OneToMany(mappedBy="dominiospf")
	@DisplayName("SPF IP")
	public Set<Dominiospfip> getListaDominiospfip() {
		return listaDominiospfip;
	}
	
	public void setCddominiospf(Integer cddominiospf) {
		this.cddominiospf = cddominiospf;
	}

	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}

	public void setDominio(Dominio dominio) {
		this.dominio = dominio;
	}

	public void setVersao(Integer versao) {
		this.versao = versao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public void setDominioredirecionar(Dominio dominioredirecionar) {
		this.dominioredirecionar = dominioredirecionar;
	}

	public void setListaDominiospfip(Set<Dominiospfip> listaDominiospfip) {
		this.listaDominiospfip = listaDominiospfip;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}	
	
	@Transient
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
