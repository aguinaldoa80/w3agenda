package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_empresamodelocontrato", sequenceName = "sq_empresamodelocontrato")
public class Empresamodelocontrato implements Log {

	private Integer cdempresamodelocontrato;
	private Empresa empresa;
	private Arquivo arquivo;
	private String nome;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;

	public Empresamodelocontrato() {
		
	}
	
	public Empresamodelocontrato(int cdempresamodelocontrato) {

		this.cdempresamodelocontrato = cdempresamodelocontrato;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_empresamodelocontrato")
	public Integer getCdempresamodelocontrato() {
		return cdempresamodelocontrato;
	}

	@Required
	@JoinColumn(name="cdempresa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Empresa getEmpresa() {
		return empresa;
	}

	@Required
	@DisplayName("Contrato")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}

	@Required
	@MaxLength(100)
	@DisplayName("Modelo")
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdempresamodelocontrato(Integer cdempresamodelocontrato) {
		this.cdempresamodelocontrato = cdempresamodelocontrato;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdempresamodelocontrato == null) ? 0 : cdempresamodelocontrato.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Empresamodelocontrato other = (Empresamodelocontrato) obj;
		if (cdempresamodelocontrato == null) {
			if (other.cdempresamodelocontrato != null)
				return false;
		} else if (!cdempresamodelocontrato.equals(other.cdempresamodelocontrato))
			return false;
		return true;
	}

}
