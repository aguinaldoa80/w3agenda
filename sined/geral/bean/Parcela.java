package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_parcela",sequenceName="sq_parcela")
public class Parcela implements Log{

	protected Integer cdparcela;
	protected Documento documento;
	protected Parcelamento parcelamento;
	protected Integer ordem;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Parcela() {
	}
	
	public Parcela(Integer cdparcela, Integer ordem, Parcelamento parcelamento) {
		this.cdparcela = cdparcela;
		this.ordem = ordem;
		this.parcelamento = parcelamento;
	}
	@Id
	@GeneratedValue(generator="sq_parcela",strategy=GenerationType.AUTO)
	public Integer getCdparcela() {
		return cdparcela;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdparcelamento")
	public Parcelamento getParcelamento() {
		return parcelamento;
	}
	public Integer getOrdem() {
		return ordem;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdparcela(Integer cdparcela) {
		this.cdparcela = cdparcela;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setParcelamento(Parcelamento parcelamento) {
		this.parcelamento = parcelamento;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
