package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_mdfelocalcarregamento", sequenceName = "sq_mdfelocalcarregamento")
@DisplayName("Loca de carregamento")
public class MdfeLocalCarregamento {

	protected Integer cdMdfeLocalCarregamento;
	protected Mdfe mdfe;
	protected Municipio municipio;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfelocalcarregamento")
	public Integer getCdMdfeLocalCarregamento() {
		return cdMdfeLocalCarregamento;
	}
	public void setCdMdfeLocalCarregamento(Integer cdMdfeLocalCarregamento) {
		this.cdMdfeLocalCarregamento = cdMdfeLocalCarregamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@Required
	@DisplayName("Município de carregamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
}
