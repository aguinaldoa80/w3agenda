package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_materialpneumodelo", sequenceName = "sq_materialpneumodelo")
@DisplayName("Modelos Vinculados")
public class Materialpneumodelo {

	protected Integer cdmaterialpneumodelo;
	protected Material material;
	protected Pneumodelo pneumodelo;
	protected Pneumedida pneumedida;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialpneumodelo")
	public Integer getCdmaterialpneumodelo() {
		return cdmaterialpneumodelo;
	}	
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}	
	
	@DisplayName("Modelo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpneumodelo")
	public Pneumodelo getPneumodelo() {
		return pneumodelo;
	}
	
	@DisplayName("Medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpneumedida")
	public Pneumedida getPneumedida() {
		return pneumedida;
	}
	
	public void setPneumedida(Pneumedida pneumedida) {
		this.pneumedida = pneumedida;
	}
	public void setCdmaterialpneumodelo(Integer cdmaterialpneumodelo) {
		this.cdmaterialpneumodelo = cdmaterialpneumodelo;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setPneumodelo(Pneumodelo pneumodelo) {
		this.pneumodelo = pneumodelo;
	}
	
}
