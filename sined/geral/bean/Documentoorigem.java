package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_documentoorigem",sequenceName="sq_documentoorigem")
public class Documentoorigem implements Log{

	protected Integer cddocumentoorigem;
	protected Documento documento;
	protected Entrega entrega;
	protected Entregadocumento entregadocumento;
	protected Fornecimento fornecimento;
	protected Veiculodespesa veiculodespesa;
	protected Despesaviagem despesaviagemadiantamento;
	protected Despesaviagem despesaviagemacerto;
	protected Despesaviagem despesaviagemreembolso;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Venda venda;
	protected Contrato contrato;
	protected Contratojuridico contratojuridico;
	protected Agendamentoservicodocumento agendamentoservicodocumento;
	protected Colaboradordespesa colaboradordespesa;
	protected Despesaavulsarh despesaavulsarh;
	protected Agendamento agendamento;
	protected Pedidovenda pedidovenda;
	protected Entregadocumentoreferenciado entregadocumentoreferenciado;
	protected Coleta coleta;
	protected Ordemcompra ordemcompra;
	protected Boolean devolucao;
	protected Gnre gnre;
	
	public Documentoorigem(){
	}

	public Documentoorigem(Documento documento, Entrega entrega, Fornecimento fornecimento, Veiculodespesa veiculodespesa, 
						   Despesaviagem despesaviagemadiantamento, Despesaviagem despesaviagemacerto, Venda venda, Contrato contrato, Agendamentoservicodocumento agendamentoservicodocumento){
		this.documento = documento;
		this.entrega = entrega;
		this.fornecimento = fornecimento;
		this.veiculodespesa = veiculodespesa;
		this.despesaviagemadiantamento = despesaviagemadiantamento;
		this.despesaviagemacerto = despesaviagemacerto;
		this.venda = venda;
		this.contrato = contrato;
		this.agendamentoservicodocumento = agendamentoservicodocumento;
	}
	
	public Documentoorigem(Documento documento, Entrega entrega, Fornecimento fornecimento, Veiculodespesa veiculodespesa, 
			   			Despesaviagem despesaviagemadiantamento, Despesaviagem despesaviagemacerto, Pedidovenda pedidovenda, Contrato contrato, Agendamentoservicodocumento agendamentoservicodocumento){
		this.documento = documento;
		this.entrega = entrega;
		this.fornecimento = fornecimento;
		this.veiculodespesa = veiculodespesa;
		this.despesaviagemadiantamento = despesaviagemadiantamento;
		this.despesaviagemacerto = despesaviagemacerto;
		this.pedidovenda = pedidovenda;
		this.contrato = contrato;
		this.agendamentoservicodocumento = agendamentoservicodocumento;
	}
	
	public Documentoorigem(Documento documento, Entrega entrega, Fornecimento fornecimento, Veiculodespesa veiculodespesa,
			   Despesaviagem despesaviagemadiantamento, Despesaviagem despesaviagemacerto, Venda venda, Contrato contrato, 
			   Agendamentoservicodocumento agendamentoservicodocumento, Agendamento agendamento){
		this.documento = documento;
		this.entrega = entrega;
		this.fornecimento = fornecimento;
		this.veiculodespesa = veiculodespesa;
		this.despesaviagemadiantamento = despesaviagemadiantamento;
		this.despesaviagemacerto = despesaviagemacerto;
		this.venda = venda;
		this.contrato = contrato;
		this.agendamentoservicodocumento = agendamentoservicodocumento;
		this.agendamento = agendamento;
	}
	
	@Id
	@GeneratedValue(generator="sq_documentoorigem",strategy=GenerationType.AUTO)
	public Integer getCddocumentoorigem() {
		return cddocumentoorigem;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentrega")
	public Entrega getEntrega() {
		return entrega;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgnre")
	public Gnre getGnre() {
		return gnre;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecimento")
	public Fornecimento getFornecimento() {
		return fornecimento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddespesaviagemacerto")
	public Despesaviagem getDespesaviagemacerto() {
		return despesaviagemacerto;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddespesaviagemadiantamento")
	public Despesaviagem getDespesaviagemadiantamento() {
		return despesaviagemadiantamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddespesaviagemreembolso")
	public Despesaviagem getDespesaviagemreembolso() {
		return despesaviagemreembolso;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontrato")
	public Contrato getContrato() {
		return contrato;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratojuridico")
	public Contratojuridico getContratojuridico() {
		return contratojuridico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculodespesa")
	public Veiculodespesa getVeiculodespesa() {
		return veiculodespesa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdagendamentoservicodocumento")
	public Agendamentoservicodocumento getAgendamentoservicodocumento() {
		return agendamentoservicodocumento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradordespesa")
	public Colaboradordespesa getColaboradordespesa() {
		return colaboradordespesa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddespesaavulsarh")
	public Despesaavulsarh getDespesaavulsarh() {
		return despesaavulsarh;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregadocumento")
	public Entregadocumento getEntregadocumento() {
		return entregadocumento;
	}

	public void setEntregadocumento(Entregadocumento entregadocumento) {
		this.entregadocumento = entregadocumento;
	}

	public void setAgendamentoservicodocumento(
			Agendamentoservicodocumento agendamentoservicodocumento) {
		this.agendamentoservicodocumento = agendamentoservicodocumento;
	}

	public void setDespesaviagemacerto(Despesaviagem despesaviagemacerto) {
		this.despesaviagemacerto = despesaviagemacerto;
	}
	
	public void setDespesaviagemadiantamento(
			Despesaviagem despesaviagemadiantamento) {
		this.despesaviagemadiantamento = despesaviagemadiantamento;
	}
	
	public void setDespesaviagemreembolso(Despesaviagem despesaviagemreembolso) {
		this.despesaviagemreembolso = despesaviagemreembolso;
	}
	
	public void setFornecimento(Fornecimento fornecimento) {
		this.fornecimento = fornecimento;
	}
	
	public void setEntrega(Entrega entrega) {
		this.entrega = entrega;
	}	
	
	public void setContratojuridico(Contratojuridico contratojuridico) {
		this.contratojuridico = contratojuridico;
	}

	public void setCddocumentoorigem(Integer cddocumentoorigem) {
		this.cddocumentoorigem = cddocumentoorigem;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	
	public void setVeiculodespesa(Veiculodespesa veiculodespesa) {
		this.veiculodespesa = veiculodespesa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvenda")
	public Venda getVenda() {
		return venda;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	
	public void setColaboradordespesa(Colaboradordespesa colaboradordespesa) {
		this.colaboradordespesa = colaboradordespesa;
	}
	
	public void setDespesaavulsarh(Despesaavulsarh despesaavulsarh) {
		this.despesaavulsarh = despesaavulsarh;
	}
	
	//Log

	public Integer getCdusuarioaltera() {
		return this.cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return this.dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdagendamento")
	public Agendamento getAgendamento() {
		return agendamento;
	}

	public void setAgendamento(Agendamento agendamento) {
		this.agendamento = agendamento;
	}
	
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregadocumentoreferenciado")
	public Entregadocumentoreferenciado getEntregadocumentoreferenciado() {
		return entregadocumentoreferenciado;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcoleta")
	public Coleta getColeta() {
		return coleta;
	}
	
	public void setColeta(Coleta coleta) {
		this.coleta = coleta;
	}
	
	public void setEntregadocumentoreferenciado(
			Entregadocumentoreferenciado entregadocumentoreferenciado) {
		this.entregadocumentoreferenciado = entregadocumentoreferenciado;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemcompra")
	public Ordemcompra getOrdemcompra() {
		return ordemcompra;
	}

	public void setOrdemcompra(Ordemcompra ordemcompra) {
		this.ordemcompra = ordemcompra;
	}

	public Boolean getDevolucao() {
		return devolucao;
	}

	public void setDevolucao(Boolean devolucao) {
		this.devolucao = devolucao;
	}
	
	public void setGnre(Gnre gnre) {
		this.gnre = gnre;
	}
}
