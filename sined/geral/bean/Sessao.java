package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;


@Entity
@SequenceGenerator(name="sq_sessao",sequenceName="sq_sessao")
public class Sessao {
	
	protected Integer cdsessao;
	protected Usuario usuario;
	protected Timestamp datahora;
	protected Boolean entrada;
	protected String sid;
	protected String ip;
	protected String useragent;
	protected String resolucao;
	protected Boolean popupAviso;
	
	@Id
	@GeneratedValue(generator="sq_sessao", strategy=GenerationType.AUTO)	
	public Integer getCdsessao() {
		return cdsessao;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuario")
	public Usuario getUsuario() {
		return usuario;
	}

	public Timestamp getDatahora() {
		return datahora;
	}

	public Boolean getEntrada() {
		return entrada;
	}

	public String getSid() {
		return sid;
	}

	public String getIp() {
		return ip;
	}
	
	public String getUseragent() {
		return useragent;
	}
	
	public String getResolucao() {
		return resolucao;
	}
	
	public Boolean getPopupAviso() {
		return popupAviso;
	}
	
	public void setPopupAviso(Boolean popupAviso) {
		this.popupAviso = popupAviso;
	}
	
	public void setResolucao(String resolucao) {
		this.resolucao = resolucao;
	}

	public void setUseragent(String useragent) {
		this.useragent = useragent;
	}

	public void setCdsessao(Integer cdsessao) {
		this.cdsessao = cdsessao;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setDatahora(Timestamp datahora) {
		this.datahora = datahora;
	}

	public void setEntrada(Boolean entrada) {
		this.entrada = entrada;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
}
