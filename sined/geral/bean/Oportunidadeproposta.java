package br.com.linkcom.sined.geral.bean;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Entity
@SequenceGenerator(name= "sq_oportunidadeproposta", sequenceName= "sq_oportunidadeproposta")
public class Oportunidadeproposta {

	protected Integer cdoportunidadeproposta;
	protected Money valor;
	protected Date dataenvio;
	protected Date datavalidade;
	protected Boolean vigente;
	protected String observacao;
	protected Oportunidade oportunidade;
	protected Arquivo arquivo;
	
	
	public Oportunidadeproposta(){
		vigente = Boolean.TRUE;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_oportunidadeproposta")
	public Integer getCdoportunidadeproposta() {
		return cdoportunidadeproposta;
	}
	public Money getValor() {
		return valor;
	}
	@DisplayName("Data Envio")
	public Date getDataenvio() {
		return dataenvio;
	}
	@DisplayName("Data Validade")
	public Date getDatavalidade() {
		return datavalidade;
	}
	public Boolean getVigente() {
		return vigente;
	}
	@DisplayName("Observação")
	@MaxLength(value=200)
	public String getObservacao() {
		return observacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdoportunidade")
	public Oportunidade getOportunidade() {
		return oportunidade;
	}

	@JoinColumn(name="cdarquivo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public void setCdoportunidadeproposta(Integer cdoportunidadeproposta) {
		this.cdoportunidadeproposta = cdoportunidadeproposta;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setDataenvio(Date dataenvio) {
		this.dataenvio = dataenvio;
	}
	public void setDatavalidade(Date datavalidade) {
		this.datavalidade = datavalidade;
	}
	public void setVigente(Boolean vigente) {
		this.vigente = vigente;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setOportunidade(Oportunidade oportunidade) {
		this.oportunidade = oportunidade;
	}
}
