package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_contratoservico", sequenceName = "sq_contratoservico")
@DisplayName("Servi�o Vinculado")
public class Contratoservico implements Log{

	protected Integer cdcontratoservico;
	protected Contrato contrato;
	protected Material material;	
	protected Contratoservicostatus contratoservicostatus;
	protected String identificador;
	protected Date dtvalidade;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratoservico")
	public Integer getCdcontratoservico() {
		return cdcontratoservico;
	}
	
	@Required
	@JoinColumn(name="cdcontrato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contrato getContrato() {
		return contrato;
	}

	@Required
	@JoinColumn(name="cdmaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Produto")
	public Material getMaterial() {
		return material;
	}
	
	@Required
	@JoinColumn(name="cdcontratoservicostatus")
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Status")
	public Contratoservicostatus getContratoservicostatus() {
		return contratoservicostatus;
	}

	@Required
	@DisplayName("Identificador")
	@MaxLength(200)
	@DescriptionProperty
	public String getIdentificador() {
		return identificador;
	}

	@DisplayName("Ativo at�")
	public Date getDtvalidade() {
		return dtvalidade;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdcontratoservico(Integer cdcontratoservico) {
		this.cdcontratoservico = cdcontratoservico;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setContratoservicostatus(Contratoservicostatus contratoservicostatus) {
		this.contratoservicostatus = contratoservicostatus;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setDtvalidade(Date dtvalidade) {
		this.dtvalidade = dtvalidade;
	}
	
	
}
