package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MinLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Codigoregimetributario;
import br.com.linkcom.sined.geral.bean.enumeration.ExigibilidadeIssLavras;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.bean.enumeration.TipoTributacaoIssETransparencia;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.TributacaoDataPublic;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_configuracaonfe", sequenceName = "sq_configuracaonfe")
public class Configuracaonfe implements Log {
	
	// Dados gerais
	protected Integer cdconfiguracaonfe;
	protected Tipoconfiguracaonfe tipoconfiguracaonfe;
	protected Prefixowebservice prefixowebservice; 
	protected String descricao;
	
	protected Empresa empresa;
	
	//Para NFS que necessitam da imagem da prefeitura
	protected Arquivo logoNfse;
	protected String nomeprefeitura;
	protected String enderecoprefeitura;
	
	protected Endereco endereco;
	protected String telefone;
	protected Boolean padrao;
	protected Boolean ativo = Boolean.TRUE;
	
	// NF de servi�o
	protected ExigibilidadeIssLavras exigibilidadeisslavras;
	protected TipoTributacaoIssETransparencia tipotributacaoissetransparencia; 
	protected Date dtadesaosimples;
	protected Double aliqiss_snip;
	protected String loginparacatu;
	protected String senhaparacatu;
	protected String inscricaomunicipal;
	protected String serienfse;
	protected Integer contadorlotenfse;
	protected Boolean optantesimples;
	protected Boolean incentivadorcultural;
	protected Boolean issretidopelotomador;
	protected Boolean atualizacaodataemissao;
	protected Boolean atualizacaodatavencimento;
	protected Boolean enviarnumeronf;
	protected String senha;
	protected String frasesecreta;
	protected String codigochaveacesso;
	protected String token;
	protected TributacaoDataPublic tributacaodatapublic;

	// NF de produto
	protected String inscricaoestadual;
	protected String inscricaoestadualst;
	protected Codigoregimetributario crt;
	protected Integer contadorlotenfe;
	protected Integer contadornfe;
	protected Uf uf;
	protected String modelonfe;
	protected Integer contadorloteevento;
	protected Boolean atualizacaodataentradasaida;
	protected ReportTemplateBean templateemissaonfse;
	protected ReportTemplateBean templateemissaonfseespelho;
	
	// MDF-e
	protected Integer contadormdfe;
	
	// DD-e
	private Integer ultNsu;
	private Boolean cienciaAutomatica;
	private Configuracaonfe configuracaoNfeManifestoAutomatico;
	
	// NFC-e
	private String idToken;
	private String csc;

	// LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;

	// LISTAS
	protected Set<Arquivonf> listaArquivonf = new ListSet<Arquivonf>(Arquivonf.class);
	protected Set<ArquivoMdfe> listaArquivoMdfe = new ListSet<ArquivoMdfe>(ArquivoMdfe.class);
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_configuracaonfe")
	public Integer getCdconfiguracaonfe() {
		return cdconfiguracaonfe;
	}

	@Required
	@DisplayName("Tipo de configura��o")
	public Tipoconfiguracaonfe getTipoconfiguracaonfe() {
		return tipoconfiguracaonfe;
	}

	@Required
	@MaxLength(200)
	@DisplayName("Descri��o")
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}

	@DisplayName("Prefixo do Webservice")
	public Prefixowebservice getPrefixowebservice() {
		return prefixowebservice;
	}
	
	@DisplayName("Endere�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdendereco")
	public Endereco getEndereco() {
		return endereco;
	}

	@MaxLength(20)
	public String getTelefone() {
		return telefone;
	}

	@DisplayName("Padr�o")
	public Boolean getPadrao() {
		return padrao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	@MaxLength(20)
	@DisplayName("Inscri��o municipal")
	public String getInscricaomunicipal() {
		return inscricaomunicipal;
	}

	@DisplayName("Exigibilidade ISS")
	public ExigibilidadeIssLavras getExigibilidadeisslavras() {
		return exigibilidadeisslavras;
	}
	
	@DisplayName("Tributa��o")
	public TributacaoDataPublic getTributacaodatapublic() {
		return tributacaodatapublic;
	}

	@MaxLength(20)
	@DisplayName("S�rie NFS-e")
	public String getSerienfse() {
		return serienfse;
	}

	@DisplayName("Contador de lote de NFS-e")
	public Integer getContadorlotenfse() {
		return contadorlotenfse;
	}

	@DisplayName("Optante pelo simples")
	public Boolean getOptantesimples() {
		return optantesimples;
	}

	@DisplayName("Incentivador Cultural")
	public Boolean getIncentivadorcultural() {
		return incentivadorcultural;
	}

	@MaxLength(20)
	@DisplayName("Inscri��o estadual")
	public String getInscricaoestadual() {
		return inscricaoestadual;
	}

	@MaxLength(20)
	@DisplayName("Inscri��o estadual ST")
	public String getInscricaoestadualst() {
		return inscricaoestadualst;
	}

	@DisplayName("C�digo do regime tribut�rio")
	public Codigoregimetributario getCrt() {
		return crt;
	}

	@DisplayName("Contador de lote de NF-e")
	public Integer getContadorlotenfe() {
		return contadorlotenfe;
	}

	@DisplayName("UF")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cduf")
	public Uf getUf() {
		return uf;
	}

	@MaxLength(2)
	@DisplayName("Modelo de NF-e")
	public String getModelonfe() {
		return modelonfe;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("Contador de NF-e")
	public Integer getContadornfe() {
		return contadornfe;
	}

	@DisplayName("Contador de lote de evento")
	public Integer getContadorloteevento() {
		return contadorloteevento;
	}
	
	@DisplayName("Senha")
	public String getSenhaparacatu() {
		return senhaparacatu;
	}
	
	@DisplayName("Login")
	public String getLoginparacatu() {
		return loginparacatu;
	}
	
	@DisplayName("ISS Retido pelo Tomador")
	public Boolean getIssretidopelotomador() {
		return issretidopelotomador;
	}
	
	@DisplayName("Atualiza data de emiss�o")
	public Boolean getAtualizacaodataemissao() {
		return atualizacaodataemissao;
	}
	
	@DisplayName("Atualiza data de vencimento")
	public Boolean getAtualizacaodatavencimento() {
		return atualizacaodatavencimento;
	}
	
	@MaxLength(100)
	public String getSenha() {
		return senha;
	}
	
	@MaxLength(100)
	public String getToken() {
		return token;
	}
	
	@DisplayName("Frase secreta")
	public String getFrasesecreta() {
		return frasesecreta;
	}
	
	@DisplayName("Atualiza data e hora de entrada/sa�da")
	public Boolean getAtualizacaodataentradasaida() {
		return atualizacaodataentradasaida;
	}
	
	@DisplayName("Logo Prefeitura")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getLogoNfse() {
		return logoNfse;
	}
	
	@DisplayName("Enviar Numero NFS-e")
	public Boolean getEnviarnumeronf() {
		return enviarnumeronf;
	}
	
	@MaxLength(200)
	@DisplayName("Nome da Prefeitura")
	public String getNomeprefeitura() {
		return nomeprefeitura;
	}

	@MaxLength(500)
	@DisplayName("Endere�o da Prefeitura")
	public String getEnderecoprefeitura() {
		return enderecoprefeitura;
	}
	
	@DisplayName("Tipo de tributa��o")
	public TipoTributacaoIssETransparencia getTipotributacaoissetransparencia() {
		return tipotributacaoissetransparencia;
	}
	
	@DisplayName("Al�quota de ISS (Simples Nacional,Isen��o Parcial)")
	public Double getAliqiss_snip() {
		return aliqiss_snip;
	}
	
	@DisplayName("Data de Ades�o ao simples nacional")
	public Date getDtadesaosimples() {
		return dtadesaosimples;
	}
	
	@DisplayName("C�digo da Chave de Acesso")
	public String getCodigochaveacesso() {
		return codigochaveacesso;
	}
	
	public void setCodigochaveacesso(String codigochaveacesso) {
		this.codigochaveacesso = codigochaveacesso;
	}
	
	public void setAliqiss_snip(Double aliqiss_snip) {
		this.aliqiss_snip = aliqiss_snip;
	}
	
	public void setDtadesaosimples(Date dtadesaosimples) {
		this.dtadesaosimples = dtadesaosimples;
	}
	
	public void setTipotributacaoissetransparencia(
			TipoTributacaoIssETransparencia tipotributacaoissetransparencia) {
		this.tipotributacaoissetransparencia = tipotributacaoissetransparencia;
	}

	public void setNomeprefeitura(String nomeprefeitura) {
		this.nomeprefeitura = nomeprefeitura;
	}
	
	public void setEnderecoprefeitura(String enderecoprefeitura) {
		this.enderecoprefeitura = enderecoprefeitura;
	}

	public void setEnviarnumeronf(Boolean enviarnumeronf) {
		this.enviarnumeronf = enviarnumeronf;
	}
	
	public void setAtualizacaodataentradasaida(
			Boolean atualizacaodataentradasaida) {
		this.atualizacaodataentradasaida = atualizacaodataentradasaida;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public void setFrasesecreta(String frasesecreta) {
		this.frasesecreta = frasesecreta;
	}
	
	public void setAtualizacaodataemissao(Boolean atualizacaodataemissao) {
		this.atualizacaodataemissao = atualizacaodataemissao;
	}
	
	public void setAtualizacaodatavencimento(Boolean atualizacaodatavencimento) {
		this.atualizacaodatavencimento = atualizacaodatavencimento;
	}

	public void setIssretidopelotomador(Boolean issretidopelotomador) {
		this.issretidopelotomador = issretidopelotomador;
	}
	
	public void setLoginparacatu(String loginparacatu) {
		this.loginparacatu = loginparacatu;
	}
	
	public void setSenhaparacatu(String senhaparacatu) {
		this.senhaparacatu = senhaparacatu;
	}

	public void setContadorloteevento(Integer contadorloteevento) {
		this.contadorloteevento = contadorloteevento;
	}

	public void setContadornfe(Integer contadornfe) {
		this.contadornfe = contadornfe;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public void setPadrao(Boolean padrao) {
		this.padrao = padrao;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setInscricaomunicipal(String inscricaomunicipal) {
		this.inscricaomunicipal = inscricaomunicipal;
	}

	public void setExigibilidadeisslavras(
			ExigibilidadeIssLavras exigibilidadeisslavras) {
		this.exigibilidadeisslavras = exigibilidadeisslavras;
	}
	
	public void setTributacaodatapublic(
			TributacaoDataPublic tributacaodatapublic) {
		this.tributacaodatapublic = tributacaodatapublic;
	}

	public void setSerienfse(String serienfse) {
		this.serienfse = serienfse;
	}

	public void setContadorlotenfse(Integer contadorlotenfse) {
		this.contadorlotenfse = contadorlotenfse;
	}

	public void setOptantesimples(Boolean optantesimples) {
		this.optantesimples = optantesimples;
	}

	public void setIncentivadorcultural(Boolean incentivadorcultural) {
		this.incentivadorcultural = incentivadorcultural;
	}

	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}

	public void setInscricaoestadualst(String inscricaoestadualst) {
		this.inscricaoestadualst = inscricaoestadualst;
	}

	public void setCrt(Codigoregimetributario crt) {
		this.crt = crt;
	}

	public void setContadorlotenfe(Integer contadorlotenfe) {
		this.contadorlotenfe = contadorlotenfe;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public void setModelonfe(String modelonfe) {
		this.modelonfe = modelonfe;
	}
	
	public void setToken(String token) {
		this.token = token;
	}

	public void setPrefixowebservice(Prefixowebservice prefixowebservice) {
		this.prefixowebservice = prefixowebservice;
	}

	public void setTipoconfiguracaonfe(Tipoconfiguracaonfe tipoconfiguracaonfe) {
		this.tipoconfiguracaonfe = tipoconfiguracaonfe;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setCdconfiguracaonfe(Integer cdconfiguracaonfe) {
		this.cdconfiguracaonfe = cdconfiguracaonfe;
	}
	
	public void setLogoNfse(Arquivo logoNfse) {
		this.logoNfse = logoNfse;
	}
	
	@DisplayName("Template de emiss�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdreporttemplatenfse")
	public ReportTemplateBean getTemplateemissaonfse() {
		return templateemissaonfse;
	}
	public void setTemplateemissaonfse(ReportTemplateBean templateemissaonfse) {
		this.templateemissaonfse = templateemissaonfse;
	}
	
	@DisplayName("Template de emiss�o (Espelho)")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdreporttemplatenfseespelho")
	public ReportTemplateBean getTemplateemissaonfseespelho() {
		return templateemissaonfseespelho;
	}
	public void setTemplateemissaonfseespelho(
			ReportTemplateBean templateemissaonfseespelho) {
		this.templateemissaonfseespelho = templateemissaonfseespelho;
	}
	
	@DisplayName("Contador de MDF-e")
	public Integer getContadormdfe() {
		return contadormdfe;
	}
	
	public void setContadormdfe(Integer contadormdfe) {
		this.contadormdfe = contadormdfe;
	}
	
	@MaxLength(15)
	@DisplayName("�ltimo NSU")
	public Integer getUltNsu() {
		return ultNsu;
	}
	
	public void setUltNsu(Integer ultNsu) {
		this.ultNsu = ultNsu;
	}
	
	@DisplayName("Dar ci�ncia autom�tica?")
	public Boolean getCienciaAutomatica() {
		return cienciaAutomatica;
	}
	
	public void setCienciaAutomatica(Boolean cienciaAutomatica) {
		this.cienciaAutomatica = cienciaAutomatica;
	}
	
	@DisplayName("Configura��o NF-e do Manifesto Autom�tico")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconfiguracaonfemanifestoautomatico")
	public Configuracaonfe getConfiguracaoNfeManifestoAutomatico() {
		return configuracaoNfeManifestoAutomatico;
	}
	
	@DisplayName("Identificador do CSC")
	@MinLength(1)
	@MaxLength(6)
	public String getIdToken() {
		return idToken;
	}
	
	@DisplayName("CSC")
	@MinLength(36)
	@MaxLength(36)
	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}
	
	public String getCsc() {
		return csc;
	}
	
	public void setCsc(String csc) {
		this.csc = csc;
	}
	
	public void setConfiguracaoNfeManifestoAutomatico(Configuracaonfe configuracaoNfeManifestoAutomatico) {
		this.configuracaoNfeManifestoAutomatico = configuracaoNfeManifestoAutomatico;
	}
	
	// LOG
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	// LISTAS
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="configuracaonfe")
	public Set<Arquivonf> getListaArquivonf() {
		return listaArquivonf;
	}
	
	public void setListaArquivonf(Set<Arquivonf> listaArquivonf) {
		this.listaArquivonf = listaArquivonf;
	}
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="configuracaonfe")
	public Set<ArquivoMdfe> getListaArquivoMdfe() {
		return listaArquivoMdfe;
	}
	
	public void setListaArquivoMdfe(Set<ArquivoMdfe> listaArquivoMdfe) {
		this.listaArquivoMdfe = listaArquivoMdfe;
	}
	
	@Transient
	public String getEnderecoCep() {
		return getEndereco() != null && getEndereco().getCep() != null ? getEndereco().getCep().toString() : "";
	}
}
