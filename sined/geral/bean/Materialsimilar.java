package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_materialsimilar", sequenceName = "sq_materialsimilar")
@DisplayName("Materiais Similares")
public class Materialsimilar {

	protected Integer cdmaterialsimilar;
	protected Material material;
	protected Material materialsimilaritem;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialsimilar")
	public Integer getCdmaterialsimilar() {
		return cdmaterialsimilar;
	}	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}	
	@Required
	@DisplayName("Material")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialsimilaritem")
	public Material getMaterialsimilaritem() {
		return materialsimilaritem;
	}
	
	public void setCdmaterialsimilar(Integer cdmaterialsimilar) {
		this.cdmaterialsimilar = cdmaterialsimilar;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialsimilaritem(Material materialsimilaritem) {
		this.materialsimilaritem = materialsimilaritem;
	}
}
