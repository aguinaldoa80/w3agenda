package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_otrbandatipo", sequenceName = "sq_otrbandatipo")
public class OtrConstrucao implements Log{
	protected Integer cdOtrConstrucao;
	protected String construcao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_otrbandatipo")
	public Integer getCdOtrConstrucao() {
		return cdOtrConstrucao;
	}
	public void setCdOtrConstrucao(Integer cdOtrConstrucao) {
		this.cdOtrConstrucao = cdOtrConstrucao;
	}
	@DescriptionProperty
	public String getConstrucao() {
		return construcao;
	}
	public void setConstrucao(String construcao) {
		this.construcao = construcao;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	
	
}
