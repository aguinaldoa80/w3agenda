package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_indicecorrecaovalor", sequenceName = "sq_indicecorrecaovalor")
@DisplayName("�ndice corre��o valor")
public class Indicecorrecaovalor {

	protected Integer cdindicecorrecaovalor;
	protected Indicecorrecao indicecorrecao;
	protected Date mesano;
	protected Double percentual;
	
	protected String mesanoAux;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_indicecorrecaovalor")
	public Integer getCdindicecorrecaovalor() {
		return cdindicecorrecaovalor;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdindicecorrecao")
	public Indicecorrecao getIndicecorrecao() {
		return indicecorrecao;
	}

	@DisplayName("M�s/ano")
	public Date getMesano() {
		return mesano;
	}

	@Required
	@MaxLength(9)
	public Double getPercentual() {
		return percentual;
	}
	
	public void setCdindicecorrecaovalor(Integer cdindicecorrecaovalor) {
		this.cdindicecorrecaovalor = cdindicecorrecaovalor;
	}

	public void setIndicecorrecao(Indicecorrecao indicecorrecao) {
		this.indicecorrecao = indicecorrecao;
	}

	public void setMesano(Date mesano) throws ParseException {
		this.mesano = mesano;
	}

	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}
	
	@Required
	@DisplayName("M�s/ano")
	@Transient
	public String getMesanoAux() {
		if(this.mesano != null)
			return new SimpleDateFormat("MM/yyyy").format(this.mesano);
		return null;
	}
	
	public void setMesanoAux(String mesanoAux) {
		try {
			this.mesano = new Date(new SimpleDateFormat("MM/yyyy").parse(mesanoAux).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		};
	}
	
}
