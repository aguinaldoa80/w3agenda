package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_endereco", sequenceName = "sq_endereco")
public class Endereco implements Log{

	protected Integer cdendereco;
	protected Pessoa pessoa;
	protected Nota nota;
	protected Municipio municipio;
	protected String logradouro;
	protected String bairro;
	protected Cep cep;
	protected String caixapostal;
	protected String numero;
	protected String complemento;
	protected Enderecotipo enderecotipo;
	protected Pais pais;
	protected String pontoreferencia;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	protected Integer distancia;
	protected String descricao;
	protected Boolean substituirlogradouro = Boolean.FALSE;
	protected String inscricaoestadual;
	protected Double area;
	
	protected PropriedadeRural propriedadeRural;
	
	//Transient
	protected Integer cdmunicipioaux;
	protected Integer cdufaux;
	protected String logradouroCompleto;
	protected String descricaoCombo;
	protected Long idandroid;
	private Integer ordem;
	protected Uf uf;
	protected Boolean enderecoTransient;
	
	public Endereco(){}

	public Endereco(Integer cdendereco){
		this.cdendereco = cdendereco; 
	}
	
	public Endereco(String logradouro, String numero, String complemento,
				String caixapostal, Cep cep, String bairro, Municipio municipio, Uf uf) {
		this.logradouro = logradouro;
		this.numero = numero;
		this.complemento = complemento;
		this.caixapostal = caixapostal;
		this.cep = cep;
		this.bairro = bairro;
		this.municipio = municipio;
		this.uf = uf;
	}
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_endereco")
	public Integer getCdendereco() {
		return cdendereco;
	}

	@JoinColumn(name="cdpessoa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pessoa getPessoa() {
		return pessoa;
	}
	@JoinColumn(name="cdnota")
	@ManyToOne(fetch=FetchType.LAZY)
	public Nota getNota() {
		return nota;
	}
	
	@DisplayName("Munic�pio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	@Transient
	public Uf getUf() {
		return uf;
	}
	@MaxLength(100)
	public String getLogradouro() {
		return logradouro;
	}
	
	@MaxLength(100)
	public String getBairro() {
		return bairro;
	}
	
	@DisplayName("CEP")
	@MaxLength(10)
	public Cep getCep() {
		return cep;
	}
	
	@MaxLength(50)
	@DisplayName("Caixa-Postal")
	public String getCaixapostal() {
		return caixapostal;
	}
	
	@DisplayName("N�mero")
	@MaxLength(5)
	public String getNumero() {
		return numero;
	}
	
	@MaxLength(50)
	public String getComplemento() {
		return complemento;
	}
	@MaxLength(2000)
	@DisplayName("Ponto de refer�ncia")
	public String getPontoreferencia() {
		return pontoreferencia;
	}
	
	@Required
	@DisplayName("Tipo de endere�o")
	@JoinColumn(name="cdenderecotipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Enderecotipo getEnderecotipo() {
		return enderecotipo;
	}
	
	@DisplayName("Pa�s")
	@JoinColumn(name="cdpais")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pais getPais() {
		return pais;
	}
	
	@MaxLength(200)
	@DisplayName("Descri��o do endere�o")
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@DisplayName("Substituir logradouro pela descri��o do endere�o")
	public Boolean getSubstituirlogradouro() {
		return substituirlogradouro;
	}

	public void setSubstituirlogradouro(Boolean substituirlogradouro) {
		this.substituirlogradouro = substituirlogradouro;
	}

	@MaxLength(200)
	@DisplayName("Inscri��o estadual")
	public String getInscricaoestadual() {
		return inscricaoestadual;
	}

	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}
	
	public Double getArea() {
		return area;
	}
	
	public void setArea(Double area) {
		this.area = area;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}
	
	@Transient
	public Integer getCdmunicipioaux() {
		return cdmunicipioaux;
	}
	
	@Transient
	@DescriptionProperty(usingFields={"logradouro","numero", "complemento", "bairro", "municipio.nome", "uf.nome", "cep", "pontoreferencia", "substituirlogradouro", "descricao"})
	public String getDescricaoCombo(){
		this.logradouro = this.logradouro != null ? this.logradouro : "";
		this.numero = this.numero != null ? this.numero : "";
		this.complemento = this.complemento != null ? this.complemento : "";
		this.bairro = this.bairro != null ? this.bairro : "";
		this.cep = this.cep != null ? this.cep : new Cep("");
		this.pontoreferencia = this.pontoreferencia != null ? this.pontoreferencia : "";
		this.substituirlogradouro = this.substituirlogradouro != null && this.substituirlogradouro;
		this.descricao = this.descricao != null ? this.descricao : "";
				
		String nomemunicipio = this.municipio != null ? ", " + this.municipio.getNome() : "";
		String nomeuf = this.municipio != null && this.municipio.getUf() != null ? "/ " + this.municipio.getUf().getSigla() : "";
		String auxcep = this.cep != null && !this.cep.toString().equals("") ? " - " + this.cep.toString() : "";
		
		String endereco = "";
		if(this.substituirlogradouro && !this.descricao.equals(""))
			endereco += this.descricao;
		else endereco += this.logradouro;
		
		if (!this.numero.equals(""))
			endereco += ", " + this.numero;
		if (!this.complemento.equals(""))
			endereco += "/ " + this.complemento;
		if (!this.bairro.equals(""))
			endereco += ", " + this.bairro;
		if (!this.pontoreferencia.equals(""))
			endereco += " (" + this.pontoreferencia + ")";
		endereco += nomemunicipio + nomeuf + auxcep;
				
		return endereco;
	}
	
	@Transient
	public String getLogradouroCompleto(){
		String logradouroCompleto = "";
		if(this.substituirlogradouro != null && this.substituirlogradouro && this.descricao != null && !this.descricao.trim().equals("")){
			logradouroCompleto += this.descricao != null && !this.descricao.trim().equals("") ? this.descricao : "";
		} else {
			logradouroCompleto += this.logradouro != null && !this.logradouro.trim().equals("") ? this.logradouro : "";
		}
		logradouroCompleto += this.numero != null && !this.numero.trim().equals("") ? ", " + this.numero : "";
		logradouroCompleto += this.complemento != null && !this.complemento.trim().equals("") ? ", " + this.complemento : "";
		return logradouroCompleto;
	}
	
	@Transient
	public String getLogradouroCompletoComBairroSemMunicipio(){
		String logradouroCompleto = "";
		if(this.substituirlogradouro != null && this.substituirlogradouro && this.descricao != null && !this.descricao.trim().equals("")){
			logradouroCompleto += this.descricao != null && !this.descricao.trim().equals("") ? this.descricao : "";
		} else {
			logradouroCompleto += this.logradouro != null && !this.logradouro.trim().equals("") ? this.logradouro : "";
		}
		logradouroCompleto += this.numero != null && !this.numero.trim().equals("") ? ", " + this.numero : "";
		logradouroCompleto += this.complemento != null && !this.complemento.trim().equals("") ? ", " + this.complemento : "";
		logradouroCompleto += this.bairro != null && !this.bairro.trim().equals("") ? ", " + this.bairro : "";
		return logradouroCompleto;
	}
	
	@Transient
	public String getLogradouroCompletoComBairro(){
		String logradouroCompleto = "";
		if(this.substituirlogradouro != null && this.substituirlogradouro && this.descricao != null && !this.descricao.trim().equals("")){
			logradouroCompleto += this.descricao != null && !this.descricao.trim().equals("") ? this.descricao : "";
		} else {
			logradouroCompleto += this.logradouro != null && !this.logradouro.trim().equals("") ? this.logradouro : "";
		}
		logradouroCompleto += this.numero != null && !this.numero.trim().equals("") ? ", " + this.numero : "";
		logradouroCompleto += this.complemento != null && !this.complemento.trim().equals("") ? ", " + this.complemento : "";
		logradouroCompleto += this.bairro != null && !this.bairro.trim().equals("") ? ", " + this.bairro : "";
		logradouroCompleto += this.municipio != null ? ", " + this.municipio.getNome() : "";
		logradouroCompleto += this.municipio != null && this.municipio.getUf() != null ? "/ " + this.municipio.getUf().getSigla() : "";
		return logradouroCompleto;
	}
	
	@Transient
	public String getLogradouroCompletoComBairroCEP(){
		String logradouroCompleto = "";
		if(this.substituirlogradouro != null && this.substituirlogradouro && this.descricao != null && !this.descricao.trim().equals("")){
			logradouroCompleto += this.descricao != null && !this.descricao.trim().equals("") ? this.descricao : "";
		} else {
			logradouroCompleto += this.logradouro != null && !this.logradouro.trim().equals("") ? this.logradouro : "";
		}
		logradouroCompleto += this.numero != null && !this.numero.trim().equals("") ? ", " + this.numero : "";
		logradouroCompleto += this.complemento != null && !this.complemento.trim().equals("") ? ", " + this.complemento : "";
		logradouroCompleto += this.bairro != null && !this.bairro.trim().equals("") ? ", " + this.bairro : "";
		logradouroCompleto += this.municipio != null ? ", " + this.municipio.getNome() : "";
		logradouroCompleto += this.municipio != null && this.municipio.getUf() != null ? "/ " + this.municipio.getUf().getSigla() : "";
		logradouroCompleto += this.cep != null && !this.cep.toString().equals("") ? " - " + this.cep.toString() : "";
		return logradouroCompleto;
	}
	
	@Transient
	public String getLogradouroCompletoOrdemRetirada(){
		String logradouroCompleto = "";
		if(this.substituirlogradouro != null && this.substituirlogradouro && this.descricao != null && !this.descricao.trim().equals("")){
			logradouroCompleto += this.descricao != null && !this.descricao.trim().equals("") ? this.descricao : "";
		} else {
			logradouroCompleto += this.logradouro != null && !this.logradouro.trim().equals("") ? this.logradouro : "";
		}
		logradouroCompleto += this.numero != null && !this.numero.trim().equals("") ? ", " + this.numero : "";
		logradouroCompleto += this.complemento != null && !this.complemento.trim().equals("") ? ", " + this.complemento : "";
		logradouroCompleto += this.pontoreferencia != null && !this.pontoreferencia.trim().equals("") ? ", " + this.pontoreferencia : "";
		logradouroCompleto += this.bairro != null && !this.bairro.trim().equals("") ? " - " + this.bairro : "";
		logradouroCompleto += this.municipio != null ? "\n" + this.municipio.getNome() : "";
		logradouroCompleto += this.municipio != null && this.municipio.getUf() != null ? " / " + this.municipio.getUf().getSigla() : "";
		return logradouroCompleto;
	}
	
	@Transient
	public String getMunicipioUf(){
		String string = this.municipio != null ? this.municipio.getNome() : "";
		string += this.municipio != null && this.municipio.getUf() != null ? "/ " + this.municipio.getUf().getSigla() : "";
		return string;
	}
	
	@Transient
	public String getLogradouroCompletoComCep(){
		String logradouroCompleto = "";
		if(this.substituirlogradouro != null && this.substituirlogradouro && this.descricao != null && !this.descricao.trim().equals("")){
			logradouroCompleto += this.descricao != null && !this.descricao.trim().equals("") ? this.descricao : "";
		} else {
			logradouroCompleto += this.logradouro != null && !this.logradouro.trim().equals("") ? this.logradouro : "";
		}
		logradouroCompleto += this.numero != null && !this.numero.trim().equals("") ? ", " + this.numero : "";
		logradouroCompleto += this.complemento != null && !this.complemento.trim().equals("") ? ", " + this.complemento : "";
		logradouroCompleto += this.bairro != null && !this.bairro.trim().equals("") ? ", " + this.bairro : "";
		logradouroCompleto += this.municipio != null ? ", " + this.municipio.getNome() : "";
		logradouroCompleto += this.municipio != null && this.municipio.getUf() != null ? "/ " + this.municipio.getUf().getSigla() : "";
		logradouroCompleto += this.cep != null && !this.cep.toString().equals("") ? " - " + this.cep.toString() : "";
		return logradouroCompleto;
	}
	
	@Transient
	public String getLogradouroNumeroComplemento(){
		String logradouroCompleto = "";
		if(this.substituirlogradouro != null && this.substituirlogradouro && this.descricao != null && !this.descricao.trim().equals("")){
			logradouroCompleto += this.descricao != null && !this.descricao.trim().equals("") ? this.descricao : "";
		} else {
			logradouroCompleto += this.logradouro != null && !this.logradouro.trim().equals("") ? this.logradouro : "";
		}
		logradouroCompleto += this.numero != null && !this.numero.trim().equals("") ? ", " + this.numero : "";
		logradouroCompleto += this.complemento != null && !this.complemento.trim().equals("") ? ", " + this.complemento : "";
		return logradouroCompleto;
	}
	
	@Transient
	public String getLogradouroNumero(){
		String logradouroCompleto = "";
		if(this.substituirlogradouro != null && this.substituirlogradouro && this.descricao != null && !this.descricao.trim().equals("")){
			logradouroCompleto += this.descricao != null && !this.descricao.trim().equals("") ? this.descricao : "";
		} else {
			logradouroCompleto += this.logradouro != null && !this.logradouro.trim().equals("") ? this.logradouro : "";
		}
		logradouroCompleto += this.numero != null && !this.numero.trim().equals("") ? ", " + this.numero : "";
		return logradouroCompleto;
	}
	
	@DisplayName("Dist�ncia (Km)")
	public Integer getDistancia() {
		return distancia;
	}
	public void setDistancia(Integer distancia) {
		this.distancia = distancia;
	}

	public void setCdendereco(Integer cdendereco) {
		this.cdendereco = cdendereco;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public void setNota(Nota nota) {
		this.nota = nota;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	
	public void setCep(Cep cep) {
		this.cep = cep;
	}
	
	public void setCaixapostal(String caixapostal) {
		this.caixapostal = caixapostal;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	
	public void setPontoreferencia(String pontoreferencia) {
		this.pontoreferencia = pontoreferencia;
	}
	
	public void setEnderecotipo(Enderecotipo enderecotipo) {
		this.enderecotipo = enderecotipo;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	

	
	public void setCdmunicipioaux(Integer cdenderecoaux) {
		this.cdmunicipioaux = cdenderecoaux;
	}
	
	@Transient
	public Long getIdandroid() {
		return idandroid;
	}
	public void setIdandroid(Long idandroid) {
		this.idandroid = idandroid;
	}
	
	@Transient
	public Integer getOrdem() {
		return ordem;
	}
	
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}	

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Endereco)
			return this.cdendereco.equals(((Endereco)obj).cdendereco);
		return super.equals(obj);
	}
	
	@Transient
	public String getLogradouroCompletoComBairroSemMunicipioSemComplemento(){
		String logradouroCompleto = "";
		if(this.substituirlogradouro != null && this.substituirlogradouro && this.descricao != null && !this.descricao.trim().equals("")){
			logradouroCompleto += this.descricao != null && !this.descricao.trim().equals("") ? this.descricao : "";
		} else {
			logradouroCompleto += this.logradouro != null && !this.logradouro.trim().equals("") ? this.logradouro : "";
		}
		logradouroCompleto += this.numero != null && !this.numero.trim().equals("") ? ", " + this.numero : "";
		logradouroCompleto += this.bairro != null && !this.bairro.trim().equals("") ? ", " + this.bairro : "";
		return logradouroCompleto;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdPropriedadeRural")
	public PropriedadeRural getPropriedadeRural() {
		return propriedadeRural;
	}
	
	public void setPropriedadeRural(PropriedadeRural propriedadeRural) {
		this.propriedadeRural = propriedadeRural;
	}

	@Transient
	public Boolean getEnderecoTransient() {
		return enderecoTransient;
	}

	public void setEnderecoTransient(Boolean enderecoTransient) {
		this.enderecoTransient = enderecoTransient;
	}

	@Transient
	public Integer getCdufaux() {
		return cdufaux;
	}

	public void setCdufaux(Integer cdufaux) {
		this.cdufaux = cdufaux;
	}
	
	
}
