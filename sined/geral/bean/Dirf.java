package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Dirf {

	protected Integer cddirf;
	protected String nome;
	
	@Id
	public Integer getCddirf() {
		return cddirf;
	}
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	
	@Transient
	@DisplayName("DIRF")
	@DescriptionProperty(usingFields={"cddirf","nome"})
	public String getCodNome(){
		return cddirf+" - "+nome;
	}
	public void setCddirf(Integer cddirf) {
		this.cddirf = cddirf;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
