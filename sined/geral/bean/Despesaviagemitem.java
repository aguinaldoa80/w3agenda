package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_despesaviagemitem", sequenceName = "sq_despesaviagemitem")
public class Despesaviagemitem implements Log{

	protected Integer cddespesaviagemitem;
	protected Despesaviagem despesaviagem;
	protected Despesaviagemtipo despesaviagemtipo;
	protected Money valorprevisto = new Money(0);
	protected Money valorrealizado = new Money(0);
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected String documento;
	protected Integer quantidade;
	
	// TRANSIENTE
	protected String whereInItens;
	protected Money valorbase;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_despesaviagemitem")
	public Integer getCddespesaviagemitem() {
		return cddespesaviagemitem;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddespesaviagem")
	public Despesaviagem getDespesaviagem() {
		return despesaviagem;
	}
	
	@Required
	@DisplayName("Tipo de despesa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddespesaviagemtipo")
	public Despesaviagemtipo getDespesaviagemtipo() {
		return despesaviagemtipo;
	}
	
	@Required
	@DisplayName("Valor previsto")
	public Money getValorprevisto() {
		return valorprevisto;
	}
	
	@DisplayName("Valor realizado")
	public Money getValorrealizado() {
		return valorrealizado;
	}
	
	public void setDespesaviagem(Despesaviagem despesaviagem) {
		this.despesaviagem = despesaviagem;
	}
	
	public void setDespesaviagemtipo(Despesaviagemtipo despesaviagemtipo) {
		this.despesaviagemtipo = despesaviagemtipo;
	}
	
	public void setValorprevisto(Money valorprevisto) {
		this.valorprevisto = valorprevisto;
	}
	
	public void setValorrealizado(Money valorrealizado) {
		this.valorrealizado = valorrealizado;
	}
	
	public void setCddespesaviagemitem(Integer cddespesaviagemitem) {
		this.cddespesaviagemitem = cddespesaviagemitem;
	}
	
	
//	LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cddespesaviagemitem == null) ? 0 : cddespesaviagemitem
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Despesaviagemitem other = (Despesaviagemitem) obj;
		if (cddespesaviagemitem == null) {
			if (other.cddespesaviagemitem != null)
				return false;
		} else if (!cddespesaviagemitem.equals(other.cddespesaviagemitem))
			return false;
		return true;
	}

	@Transient
	public String getWhereInItens() {
		return whereInItens;
	}
	
	public void setWhereInItens(String whereInItens) {
		this.whereInItens = whereInItens;
	}
	
	@DisplayName("Documento")
	@MaxLength(50)
	public String getDocumento() {
		return documento;
	}
	
	@DisplayName("Quantidade")
	@MaxLength(9)
	public Integer getQuantidade() {
		return quantidade;
	}
	
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	
	@Transient
	@DisplayName("Valor Base")
	public Money getValorbase() {
		return valorbase;
	}
	
	public void setValorbase(Money valorbase) {
		this.valorbase = valorbase;
	}
}