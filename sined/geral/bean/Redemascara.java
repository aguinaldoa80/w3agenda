package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
public class Redemascara{

	protected Integer cdredemascara;
	protected String nome;
	protected Integer identificador;

	// Transient
	protected String mascara;
	
	@Id
	@DisplayName("Id")
	public Integer getCdredemascara() {
		return cdredemascara;
	}

	@Required
	@MaxLength(30)
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	
	public Integer getIdentificador() {
		return identificador;
	}

	public void setCdredemascara(Integer cdredemascara) {
		this.cdredemascara = cdredemascara;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
	}
	
	@Transient
	@DescriptionProperty
	@DisplayName("M�scara")
	public String getMascara() {
		return "/" + getCdredemascara() + " - " + getNome();
	}
	
	public void setMascara(String mascara) {
		this.mascara = mascara;
	}
}
