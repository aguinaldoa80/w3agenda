package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_locacaomaterialtipo", sequenceName = "sq_locacaomaterialtipo")
public class Locacaomaterialtipo implements Log{

	protected Integer cdlocacaomaterialtipo;
	protected String nome;
	protected Boolean ativo = Boolean.TRUE;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_locacaomaterialtipo")
	public Integer getCdlocacaomaterialtipo() {
		return cdlocacaomaterialtipo;
	}
	
	@Required
	@DisplayName("Descri��o")
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setCdlocacaomaterialtipo(Integer cdlocacaomaterialtipo) {
		this.cdlocacaomaterialtipo = cdlocacaomaterialtipo;
	}
	public void setNome(String nome) {
		this.nome = StringUtils.trimToNull(nome);;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdlocacaomaterialtipo == null) ? 0 : cdlocacaomaterialtipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Locacaomaterialtipo other = (Locacaomaterialtipo) obj;
		if (cdlocacaomaterialtipo == null) {
			if (other.cdlocacaomaterialtipo != null)
				return false;
		} else if (!cdlocacaomaterialtipo.equals(other.cdlocacaomaterialtipo))
			return false;
		return true;
	}
	
	
	
}
