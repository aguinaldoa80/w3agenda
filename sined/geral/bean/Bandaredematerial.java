package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_bandaredematerial", sequenceName = "sq_bandaredematerial")
public class Bandaredematerial{
	
	protected Integer cdbandaredematerial;
	protected Bandaredecomercial bandaredecomercial;
	protected Material material;
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bandaredematerial")
	public Integer getCdbandaredematerial() {
		return cdbandaredematerial;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cdbandaredecomercial")
	public Bandaredecomercial getBandaredecomercial() {
		return bandaredecomercial;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}

	public void setCdbandaredematerial(Integer cdbandaredematerial) {
		this.cdbandaredematerial = cdbandaredematerial;
	}

	public void setBandaredecomercial(Bandaredecomercial bandaredecomercial) {
		this.bandaredecomercial = bandaredecomercial;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}
	
}
