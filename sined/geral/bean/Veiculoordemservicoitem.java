package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_veiculoordemservicoitem", sequenceName="sq_veiculoordemservicoitem")
public class Veiculoordemservicoitem implements Log{

	protected Integer cdveiculoordemservicoitem;
	protected Inspecaoitem inspecaoitem;
	protected Veiculoordemservico ordemservico;
	protected Boolean status =  Boolean.FALSE;
	protected Veiculoordemservicoitem ordemservicoiteminspecao;//auto-relacionamento
	protected String motivocancela;
	protected Date dtcancelamento;
	protected Boolean ativo = Boolean.TRUE;
	protected String observacao;
	protected String localmanut;
	protected String descricao;
	//Log
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;

	public Veiculoordemservicoitem(){}
	
	public Veiculoordemservicoitem(Integer cdveiculoordemservicoitem){
		this.cdveiculoordemservicoitem = cdveiculoordemservicoitem;
	}
	public Veiculoordemservicoitem(Inspecaoitem inspecaoitem){
		this.inspecaoitem = inspecaoitem;
	}
	
	//***********************************************************************************************
	//transient
	protected List<Categoriaiteminspecao> listaCategoriaItemInspecao;
	protected List<Inspecaoitem> listaInspecaoitem;
	@Transient
	public List<Inspecaoitem> getListaItemInspecao() {
		return listaInspecaoitem;
	}
	@Transient
	public List<Categoriaiteminspecao> getListaCategoriaItemInspecao() {
		return listaCategoriaItemInspecao;
	}
	
	public void setListaItemInspecao(List<Inspecaoitem> listaInspecaoitem) {
		this.listaInspecaoitem = listaInspecaoitem;
	}
	public void setListaCategoriaItemInspecao(
			List<Categoriaiteminspecao> listaCategoriaItemInspecao) {
		this.listaCategoriaItemInspecao = listaCategoriaItemInspecao;
	}
	
	//************************************************************************************************
	
	@DisplayName("Situa��o")
	public Boolean getAtivo() {
		return ativo;
	}
	
	@DisplayName("Motivo")
	@MaxLength(100)
	public String getMotivocancela() {
		return motivocancela;
	}

	@DisplayName("Data")
	public Date getDtcancelamento() {
		return dtcancelamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemservicoiteminspecao")
	public Veiculoordemservicoitem getOrdemservicoiteminspecao() {
		return ordemservicoiteminspecao;
	}
	@DisplayName("Status")
	@DescriptionProperty
	public Boolean getStatus() {
		return status;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_veiculoordemservicoitem")
	public Integer getCdveiculoordemservicoitem() {
		return cdveiculoordemservicoitem;
	}

	@DisplayName("Item de Inspe��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdinspecaoitem")
	public Inspecaoitem getInspecaoitem() {
		return inspecaoitem;
	}

	@DisplayName("Ordem de Servi�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculoordemservico")
	public Veiculoordemservico getOrdemservico() {
		return ordemservico;
	}
	
	@DisplayName("Local da manuten��o")
	@MaxLength(50)
	@Required
	public String getLocalmanut() {
		return localmanut;
	}

	@DisplayName("Descri��o")
	@MaxLength(2000)
	public String getDescricao() {
		return descricao;
	}
	
	@DisplayName("Observa��es")
	@MaxLength(100)
	public String getObservacao() {
		return observacao;
	}

	public void setCdveiculoordemservicoitem(Integer cdveiculoordemservicoitem) {
		this.cdveiculoordemservicoitem = cdveiculoordemservicoitem;
	}
	public void setInspecaoitem(Inspecaoitem inspecaoitem) {
		this.inspecaoitem = inspecaoitem;
	}
	public void setOrdemservico(Veiculoordemservico ordemservico) {
		this.ordemservico = ordemservico;
	}
	public void setOrdemservicoiteminspecao(
			Veiculoordemservicoitem ordemservicoiteminspecao) {
		this.ordemservicoiteminspecao = ordemservicoiteminspecao;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public void setDtcancelamento(Date dtcancelamento) {
		this.dtcancelamento = dtcancelamento;
	}
	public void setMotivocancela(String motivocancela) {
		this.motivocancela = motivocancela;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public void setLocalmanut(String localmanut) {
		this.localmanut = localmanut;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	//Log
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
