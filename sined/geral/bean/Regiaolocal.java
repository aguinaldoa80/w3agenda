package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_regiaolocal", sequenceName = "sq_regiaolocal")
public class Regiaolocal {

	protected Integer cdregiaolocal;
	protected Regiao regiao;
	protected Cep cepinicio;
	protected Cep cepfinal;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_regiaolocal")
	public Integer getCdregiaolocal() {
		return cdregiaolocal;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdregiao")
	public Regiao getRegiao() {
		return regiao;
	}
	
	@Required
	@DisplayName("CEP De")
	public Cep getCepinicio() {
		return cepinicio;
	}
	
	@Required
	@DisplayName("CEP At�")
	public Cep getCepfinal() {
		return cepfinal;
	}
	
	public void setCdregiaolocal(Integer cdregiaolocal) {
		this.cdregiaolocal = cdregiaolocal;
	}
	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}
	public void setCepinicio(Cep cepinicio) {
		this.cepinicio = cepinicio;
	}
	public void setCepfinal(Cep cepfinal) {
		this.cepfinal = cepfinal;
	}
	
}
