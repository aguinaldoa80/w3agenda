package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.enumeration.EnumArquivoConciliacaoOperadoraStatusPagamento;
import br.com.linkcom.sined.geral.bean.enumeration.EnumArquivoConciliacaoOperadoraTipoTransacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name="sq_arquivoconciliacaooperadora", sequenceName="sq_arquivoconciliacaooperadora")
public class Arquivoconciliacaooperadora implements Log {
	
	private Integer cdarquivoconciliacaooperadora;
	private Integer estabelecimento;
	private String numeroro;
	private Date dataapresentacao;
	private Date datavenda;
	private EnumArquivoConciliacaoOperadoraTipoTransacao tipotransacao;
	private Date dataprevistapagamento;
	private Date dataenviobanco;
	private Double valorliquido;
	private Double valorvenda;
	private Integer parcela;
	private Integer totalparcela;
	private String codigoautoricacao;
	private String tid;
	private String nsudoc;
	private String motivorejeicao;
	private EnumArquivoConciliacaoOperadoraStatusPagamento statuspagamento;
	private Taxaparcela taxaparcela;
	private Documento documento;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	private Double valorbruto;
	private Empresa empresaestabelecimento;
	private Arquivo arquivo;
	
	//Transientes
	private Double valorliquidocalculado;
	private boolean diferencavalorliquidocalculado;
	private Money moneyvalorliquido;
	private Money moneyvalorvenda;
	private Money moneyvalorliquidocalculado;
	private Movimentacao movimentacao;
	private boolean conciliar = false;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivoconciliacaooperadora")
	public Integer getCdarquivoconciliacaooperadora() {
		return cdarquivoconciliacaooperadora;
	}
	
	@DisplayName("Estabelecimento")
	public Integer getEstabelecimento() {
		return estabelecimento;
	}
	
	@DisplayName("N�mero RO")
	public String getNumeroro() {
		return numeroro;
	}
	
	@DisplayName("Data de apresenta��o")
	public Date getDataapresentacao() {
		return dataapresentacao;
	}
	
	@DisplayName("Data da venda")
	public Date getDatavenda() {
		return datavenda;
	}
	
	@DisplayName("Tipo")
	public EnumArquivoConciliacaoOperadoraTipoTransacao getTipotransacao() {
		return tipotransacao;
	}
	
	@DisplayName("Data prevista de pgto")
	public Date getDataprevistapagamento() {
		return dataprevistapagamento;
	}
	
	@DisplayName("Data de envio ao banco")
	public Date getDataenviobanco() {
		return dataenviobanco;
	}
	
	@DisplayName("Valor l�quido")
	public Double getValorliquido() {
		return valorliquido;
	}
	
	@DisplayName("Valor da venda")
	public Double getValorvenda() {
		return valorvenda;
	}
	
	@DisplayName("Parcela")
	public Integer getParcela() {
		return parcela;
	}
	
	@DisplayName("Qte de parcelas")
	public Integer getTotalparcela() {
		return totalparcela;
	}
	
	@DisplayName("Codigo de autoriza��o")
	public String getCodigoautoricacao() {
		return codigoautoricacao;
	}
	
	@DisplayName("TID")
	public String getTid() {
		return tid;
	}
	
	@DisplayName("NSU/DOC")
	public String getNsudoc() {
		return nsudoc;
	}
	
	@DisplayName("Motivo de rejei��o")
	public String getMotivorejeicao() {
		return motivorejeicao;
	}
	
	@DisplayName("Situa��o")
	public EnumArquivoConciliacaoOperadoraStatusPagamento getStatuspagamento() {
		return statuspagamento;
	}
	
	@DisplayName("Taxa da parcela")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtaxaparcela")
	public Taxaparcela getTaxaparcela() {
		return taxaparcela;
	}
	
	@DisplayName("Documento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}
	
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public Double getValorbruto() {
		return valorbruto;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresaestabelecimento() {
		return empresaestabelecimento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@Transient
	@DisplayName("Valor l�quido calculado")
	public Double getValorliquidocalculado() {
		if (this.taxaparcela != null && this.taxaparcela.getTaxa() != null && this.documento != null && this.documento.getValor() != null) {
			this.valorliquidocalculado = this.documento.getValor().getValue().doubleValue() - (this.documento.getValor().getValue().doubleValue() * (this.taxaparcela.getTaxa()/100));
		}
		return valorliquidocalculado;
	}
	
	@Transient
	public boolean getDiferencavalorliquidocalculado() {
		this.diferencavalorliquidocalculado = false;
		if (getValorliquido() != null && getValorliquidocalculado() != null) {
			Double diferenca = getValorliquido() - getValorliquidocalculado();
			if (SinedUtil.round(diferenca, 2) > 0.01 || SinedUtil.round(diferenca, 2) < -0.01) {
				this.diferencavalorliquidocalculado = true;
			} else {
				this.diferencavalorliquidocalculado = false;
			}
		}
		return diferencavalorliquidocalculado;
	}
	
	@Transient
	public boolean getValorBrutoMenorValorMovimentacao() {
		boolean valorMenor = false;
		if (getMovimentacao() != null && getMovimentacao().getValor() != null && 
				getValorbruto() != null && 
				getValorbruto() < getMovimentacao().getValor().getValue().doubleValue()) {
			valorMenor = true;
		}
		return valorMenor;
	}
	
	@Transient
	@DisplayName("Valor liquido")
	public Money getMoneyvalorliquido() {
		if (getValorliquido() != null) {
			this.moneyvalorliquido = new Money(getValorliquido().doubleValue());
		}
		return moneyvalorliquido;
	}
	
	@Transient
	@DisplayName("Valor venda")
	public Money getMoneyvalorvenda() {
		if (getValorvenda() != null) {
			this.moneyvalorvenda = new Money(getValorvenda().doubleValue());
		}
		return moneyvalorvenda;
	}
	
	@Transient
	@DisplayName("Valor liquido calculado")
	public Money getMoneyvalorliquidocalculado() {
		if (getValorliquidocalculado() != null) {
			this.moneyvalorliquidocalculado = new Money(getValorliquidocalculado().doubleValue());
		}
		return moneyvalorliquidocalculado;
	}
	
	@Transient
	public boolean isConciliar() {
		if (this.documento != null 
				&& this.documento.getDocumentoacao() != null 
				&& this.documento.getDocumentoacao().equals(Documentoacao.BAIXADA)
				&& this.movimentacao != null
				&& this.movimentacao.getMovimentacaoacao() != null
				&& !this.movimentacao.getMovimentacaoacao().equals(Movimentacaoacao.CONCILIADA)) {
			this.conciliar = true;
		}
		return conciliar;
	}
	
	@Transient
	public Movimentacao getMovimentacao() {
		return movimentacao;
	}

	public void setCdarquivoconciliacaooperadora(Integer cdarquivoconciliacaooperadora) {
		this.cdarquivoconciliacaooperadora = cdarquivoconciliacaooperadora;
	}
	public void setEstabelecimento(Integer estabelecimento) {
		this.estabelecimento = estabelecimento;
	}
	public void setNumeroro(String numeroro) {
		this.numeroro = numeroro;
	}
	public void setDataapresentacao(Date dataapresentacao) {
		this.dataapresentacao = dataapresentacao;
	}
	public void setDatavenda(Date datavenda) {
		this.datavenda = datavenda;
	}
	public void setTipotransacao(EnumArquivoConciliacaoOperadoraTipoTransacao tipotransacao) {
		this.tipotransacao = tipotransacao;
	}
	public void setDataprevistapagamento(Date dataprevistapagamento) {
		this.dataprevistapagamento = dataprevistapagamento;
	}
	public void setDataenviobanco(Date dataenviobanco) {
		this.dataenviobanco = dataenviobanco;
	}
	public void setValorliquido(Double valorliquido) {
		this.valorliquido = valorliquido;
	}
	public void setValorvenda(Double valorvenda) {
		this.valorvenda = valorvenda;
	}
	public void setParcela(Integer parcela) {
		this.parcela = parcela;
	}
	public void setTotalparcela(Integer totalparcela) {
		this.totalparcela = totalparcela;
	}
	public void setCodigoautoricacao(String codigoautoricacao) {
		this.codigoautoricacao = codigoautoricacao;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public void setNsudoc(String nsudoc) {
		this.nsudoc = nsudoc;
	}
	public void setMotivorejeicao(String motivorejeicao) {
		this.motivorejeicao = motivorejeicao;
	}
	public void setStatuspagamento(EnumArquivoConciliacaoOperadoraStatusPagamento statuspagamento) {
		this.statuspagamento = statuspagamento;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setTaxaparcela(Taxaparcela taxaparcela) {
		this.taxaparcela = taxaparcela;
	}
	public void setValorliquidocalculado(Double valorliquidocalculado) {
		this.valorliquidocalculado = valorliquidocalculado;
	}
	public void setDiferencavalorliquidocalculado(boolean diferencavalorliquidocalculado) {
		this.diferencavalorliquidocalculado = diferencavalorliquidocalculado;
	}
	public void setMoneyvalorliquido(Money moneyvalorliquido) {
		this.moneyvalorliquido = moneyvalorliquido;
	}
	public void setMoneyvalorvenda(Money moneyvalorvenda) {
		this.moneyvalorvenda = moneyvalorvenda;
	}
	public void setMoneyvalorliquidocalculado(Money moneyvalorliquidocalculado) {
		this.moneyvalorliquidocalculado = moneyvalorliquidocalculado;
	}
	public void setValorbruto(Double valorbruto) {
		this.valorbruto = valorbruto;
	}
	public void setConciliar(boolean conciliar) {
		this.conciliar = conciliar;
	}
	public void setMovimentacao(Movimentacao movimentacao) {
		this.movimentacao = movimentacao;
	}
	public void setEmpresaestabelecimento(Empresa empresaestabelecimento) {
		this.empresaestabelecimento = empresaestabelecimento;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
}
