package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_servicowebalias", sequenceName = "sq_servicowebalias")
@DisplayName("Alias")
public class Servicowebalias{
	
	protected Integer cdservicowebalias;
	protected Servicoweb servicoweb;
	protected Dominiozona dominiozona;
	protected Boolean ativo;
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_servicowebalias")
	public Integer getCdservicowebalias() {
		return cdservicowebalias;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservicoweb")
	public Servicoweb getServicoweb() {
		return servicoweb;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddominiozona")
	@DisplayName("Zona")		
	public Dominiozona getDominiozona() {
		return dominiozona;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setCdservicowebalias(Integer cdservicowebalias) {
		this.cdservicowebalias = cdservicowebalias;
	}

	public void setServicoweb(Servicoweb servicoweb) {
		this.servicoweb = servicoweb;
	}

	public void setDominiozona(Dominiozona dominiozona) {
		this.dominiozona = dominiozona;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
}
