package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_ausenciacolaborador", sequenceName = "sq_ausenciacolaborador")
@DisplayName("Aus�ncia colaborador")
public class Ausenciacolaborador implements Log{
	
	protected Integer cdausenciacolaborador;
	protected Colaborador colaborador;
	protected Ausenciamotivo ausenciamotivo;
	protected String descricao;
	protected Date dtinicio;
	protected Date dtfim;
	protected Hora hrinicio;
	protected Hora hrfim;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected List<Ausenciacolaboradorhistorico> listausenciacolaboradorhistorico;
	
	//Transiente
	protected Cliente cliente;
	protected String descriptionHour;
	protected String dataString;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ausenciacolaborador")
	public Integer getCdausenciacolaborador() {
		return cdausenciacolaborador;
	}
	@Required
	@DescriptionProperty
	@MaxLength(100)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Motivo")
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdausenciamotivo")
	public Ausenciamotivo getAusenciamotivo() {
		return ausenciamotivo;
	}
	@Required
	@DisplayName("Data In�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data Fim")
	@Required
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Hora In�cio")
	public Hora getHrinicio() {
		return hrinicio;
	}
	@DisplayName("Hora Fim")
	public Hora getHrfim() {
		return hrfim;
	}
	@DisplayName("Hist�rico")
	@OneToMany(fetch = FetchType.LAZY, mappedBy="cdausenciacolaborador")
	public List<Ausenciacolaboradorhistorico> getListausenciacolaboradorhistorico() {
		return listausenciacolaboradorhistorico;
	}
	public void setListausenciacolaboradorhistorico(
			List<Ausenciacolaboradorhistorico> listausenciacolaboradorhistorico) {
		this.listausenciacolaboradorhistorico = listausenciacolaboradorhistorico;
	}

	public void setCdausenciacolaborador(Integer cdausenciacolaborador) {
		this.cdausenciacolaborador = cdausenciacolaborador;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setAusenciamotivo(Ausenciamotivo ausenciamotivo) {
		this.ausenciamotivo = ausenciamotivo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setHrfim(Hora hrfim) {
		this.hrfim = hrfim;
	}
	public void setHrinicio(Hora hrinicio) {
		this.hrinicio = hrinicio;
	}
	
	@Transient
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	@Transient
	public String getDescriptionHour() {
		return descriptionHour;
	}
	public void setDescriptionHour(String descriptionHour) {
		this.descriptionHour = descriptionHour;
	}
	@Transient
	public String getDataString() {
		return dataString;
	}
	public void setDataString(String dataString) {
		this.dataString = dataString;
	}
}
