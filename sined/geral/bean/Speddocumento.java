package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "sq_speddocumento", sequenceName = "sq_speddocumento")
public class Speddocumento {

	protected Integer cdspeddocumento;
	protected Spedarquivo spedarquivo;
	
	protected Entregadocumento entregadocumento;
	protected Documento documento;
	protected Nota nota;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_speddocumento")
	public Integer getCdspeddocumento() {
		return cdspeddocumento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdspedarquivo")
	public Spedarquivo getSpedarquivo() {
		return spedarquivo;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregadocumento")
	public Entregadocumento getEntregadocumento() {
		return entregadocumento;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnota")
	public Nota getNota() {
		return nota;
	}
	
	public void setNota(Nota nota) {
		this.nota = nota;
	}
	
	public void setCdspeddocumento(Integer cdspeddocumento) {
		this.cdspeddocumento = cdspeddocumento;
	}

	public void setSpedarquivo(Spedarquivo spedarquivo) {
		this.spedarquivo = spedarquivo;
	}

	public void setEntregadocumento(Entregadocumento entregadocumento) {
		this.entregadocumento = entregadocumento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	
}
