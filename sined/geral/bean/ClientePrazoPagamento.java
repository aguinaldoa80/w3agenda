package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_ClientePrazoPagamento", sequenceName = "sq_ClientePrazoPagamento")
public class ClientePrazoPagamento {
	
	protected Integer cdclienteprazopagamento;
	protected Cliente cliente;
	protected Prazopagamento prazopagamento;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ClientePrazoPagamento")
	public Integer getCdclienteprazopagamento() {
		return cdclienteprazopagamento;
	}
	
	@JoinColumn(name="cdpessoa")
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	public Cliente getCliente() {
		return cliente;
	}

	@JoinColumn(name="cdPrazoPagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	@DisplayName("Condi��o de Pagamento")
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setCdclienteprazopagamento(Integer cdclienteprazopagamento) {
		this.cdclienteprazopagamento = cdclienteprazopagamento;
	}

	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
}