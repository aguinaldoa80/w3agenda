package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MinLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.FormaemissaoMdfe;
import br.com.linkcom.sined.geral.bean.enumeration.MdfeSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Mdfetipoproprietarioveiculo;
import br.com.linkcom.sined.geral.bean.enumeration.ModalidadefreteMdfe;
import br.com.linkcom.sined.geral.bean.enumeration.TipoCarroceria;
import br.com.linkcom.sined.geral.bean.enumeration.TipoEmitenteMdfe;
import br.com.linkcom.sined.geral.bean.enumeration.TipoNavegacaoAquaviario;
import br.com.linkcom.sined.geral.bean.enumeration.TipoRodado;
import br.com.linkcom.sined.geral.bean.enumeration.TipoTransportadorMdfe;
import br.com.linkcom.sined.geral.bean.enumeration.UnidadeMedidaPesoBrutoCarga;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_mdfe", sequenceName = "sq_mdfe")
@DisplayName("MDF-e")
public class Mdfe implements Log{
	
	protected Integer cdmdfe;
	protected MdfeSituacao mdfeSituacao;
	protected String serie;
	protected String numero;
	protected Empresa empresa;
	protected Timestamp dataHoraEmissao;
	protected Timestamp dataHoraInicioViagem;
	protected Uf uf;
	protected TipoEmitenteMdfe tipoEmitenteMdfe;
	protected TipoTransportadorMdfe tipoTransportador;
	protected ModalidadefreteMdfe modalidadeFrete;
	protected FormaemissaoMdfe formaEmissao;
	protected Uf ufLocalDescarregamento;
	protected Uf ufLocalCarregamento;
	protected Money valorTotalCargaTransportada;
	protected UnidadeMedidaPesoBrutoCarga unidadeMedidaPesoBrutoCarga;
	protected Double pesoBrutoTotalCarga;
	protected Integer qtdeTotalCteRelacionada;
	protected Integer qtdeTotalNfeRelacionada;
	protected Integer qtdeTotalMdfeRelacionada;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Boolean projetoCanalVerde;
	protected Boolean carregamentoNoExterior;
	protected Boolean descarregamentoNoExterior;
	protected List<MdfeLocalCarregamento> listaLocalCarregamento;
	protected List<MdfeLocalDescarregamento> listaLocalDescarregamento;
	protected List<MdfeUfPercurso> listaUfPercurso;
	protected List<MdfeLacre> listaLacre;
	protected List<MdfeAutorizacaoDownload> listaAutorizacaoDownload;
	protected String infoAdicionaisFisco;
	protected String infoAdicionaisContribuinte;
	private String qrCode;
	
	//Rodovi�rio
	protected String rntrc;
	protected String codigoAgendamentoPorto;
	protected String codigoInternoVeiculo;
	protected TipoCarroceria tipoCarroceria;
	protected String placa;
	protected String tara;
	protected String renavam;
	protected Uf ufFreteRodoviario;
	protected TipoRodado tipoRodado;
	protected String capacidadeKgRodoviario;
	protected String capacidadeM3Rodoviario;
	protected Boolean pertenceEmpresaEmitente;
	protected Tipopessoa tipoPessoaProprietarioRodoviario;
	protected String nomeProprietarioRodoviario;
	protected Cpf cpfProprietarioRodoviario;
	protected Cnpj cnpjProprietarioRodoviario;
	protected String inscEstProprietarioRodoviario;
	protected Boolean isentoRodoviario;
	protected String rntrcProprietario;
	protected Uf ufProprietarioRodoviario;
	protected Mdfetipoproprietarioveiculo tipoProprietarioVeiculoRodoviario;
	protected List<MdfeRodoviarioCondutor> listaCondutor;
	protected List<MdfeRodoviarioReboque> listaReboque;
	protected List<MdfeRodoviarioCiot> listaCiot;
	protected List<MdfeRodoviarioContratante> listaContratante;
	protected List<MdfeRodoviarioValePedagio> listaValePedagio;
	
	
	//A�reo
	protected String marcaNacionalidadeAeronave;
	protected String marcaMatriculaAeronave;
	protected String numeroVoo;
	protected String aerodromoEmbarque;
	protected String aerodromoDestino;
	protected Date dataVoo;
	
	//Aquavi�rio
	protected String irinNavio;
	protected String codigoTipoEmbarcacao;
	protected String codigoEmbarcacao;
	protected String numeroViagemEmbarcacao;
	protected String nomeEmbarcacao;
	protected String codigoPortoEmbarque;
	protected String codigoPortoDestino;
	protected String portoTransbordo;
	protected TipoNavegacaoAquaviario tipoNavegacao;
	protected List<MdfeAquaviarioTerminalCarregamento> listaAquaviarioTerminalCarregamento;
	protected List<MdfeAquaviarioTerminalDescarregamento> listaAquaviarioTerminalDescarregamento;
	protected List<MdfeAquaviarioUnidadeCargaVazia> listaAquaviarioUnidadeCargaVazia;
	protected List<MdfeAquaviarioUnidadeTransporteVazia> listaAquaviarioUnidadeTransporteVazia;
	protected List<MdfeAquaviarioEmbarcacaoComboio> listaAquaviarioEmbarcacaoComboio;
	
	//Ferrovi�rio
	protected String prefixoTrem;
	protected Timestamp dataHoraLiberacaoTremOrigem;
	protected String origemTrem;
	protected String destinoTrem;
	protected Integer qtdeVagoesCarregados;
	protected List<MdfeFerroviarioVagao> listaVagao;
	
	
	protected List<MdfeCte> listaMdfeCte;
	protected List<MdfeInformacaoUnidadeTransporteCte> listaInformacaoUnidadeTransporteCte;
	protected List<MdfeInformacaoUnidadeCargaCte> listaInformacaoUnidadeCargaCte;
	protected List<MdfeLacreUnidadeTransporteCte> listaLacreUnidadeTransporteCte;
	protected List<MdfeLacreUnidadeCargaCte> listaLacreUnidadeCargaCte;
	protected List<MdfeProdutoPerigosoCte> listaProdutoPerigosoCte;

	protected List<MdfeNfe> listaNfe;
	protected List<MdfeInformacaoUnidadeTransporteNfe> listaInformacaoUnidadeTransporteNfe;
	protected List<MdfeInformacaoUnidadeCargaNfe> listaInformacaoUnidadeCargaNfe;
	protected List<MdfeLacreUnidadeTransporteNfe> listaLacreUnidadeTransporteNfe;
	protected List<MdfeLacreUnidadeCargaNfe> listaLacreUnidadeCargaNfe;
	protected List<MdfeProdutoPerigosoNfe> listaProdutoPerigosoNfe;
	
	protected List<MdfeReferenciado> listaMdfeReferenciado;
	protected List<MdfeInformacaoUnidadeTransporteMdfeReferenciado> listaInformacaoUnidadeTransporteMdfeReferenciado;
	protected List<MdfeInformacaoUnidadeCargaMdfeReferenciado> listaInformacaoUnidadeCargaMdfeReferenciado;
	protected List<MdfeLacreUnidadeTransporteMdfeReferenciado> listaLacreUnidadeTransporteMdfeReferenciado;
	protected List<MdfeLacreUnidadeCargaMdfeReferenciado> listaLacreUnidadeCargaMdfeReferenciado;
	protected List<MdfeProdutoPerigosoMdfeReferenciado> listaProdutoPerigosoMdfeReferenciado;
	
	protected List<MdfeSeguroCarga> listaSeguroCarga;
	protected List<MdfeSeguroCargaNumeroAverbacao> listaSeguroCargaNumeroAverbacao;
	
	//TRANSIENTS
	protected List<MdfeHistorico> listaHistoricoTrans;
	protected MdfeSituacao mdfeSituacaoHistoricoTrans;
	protected Boolean haveArquivoMdfe = Boolean.FALSE;
	protected String chaveAcesso;
	protected Boolean usarSequencial = Boolean.TRUE;
	
	public Mdfe() {}
	
	public Mdfe(Integer cdmdfe) {
		this.cdmdfe = cdmdfe;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfe")
	public Integer getCdmdfe() {
		return cdmdfe;
	}
	public void setCdmdfe(Integer cdmdfe) {
		this.cdmdfe = cdmdfe;
	}

	@DisplayName("Situa��o")
	public MdfeSituacao getMdfeSituacao() {
		return mdfeSituacao;
	}

	public void setMdfeSituacao(MdfeSituacao mdfeSituacao) {
		this.mdfeSituacao = mdfeSituacao;
	}

	@MaxLength(value=3)
	@Required
	@DisplayName("S�rie")
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}

	@MaxLength(value=9)
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Required
	@DisplayName("Empresa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Required
	@DisplayName("Data e hora de emiss�o")
	public Timestamp getDataHoraEmissao() {
		return dataHoraEmissao;
	}
	public void setDataHoraEmissao(Timestamp datahoraemissao) {
		this.dataHoraEmissao = datahoraemissao;
	}

	@DisplayName("Data e hora de in�cio de viagem")
	public Timestamp getDataHoraInicioViagem() {
		return dataHoraInicioViagem;
	}
	public void setDataHoraInicioViagem(Timestamp datahorainicioviagem) {
		this.dataHoraInicioViagem = datahorainicioviagem;
	}

	@Required
	@DisplayName("UF")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cduf")
	public Uf getUf() {
		return uf;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}

	@Required
	@DisplayName("Tipo de emitente")
	public TipoEmitenteMdfe getTipoEmitenteMdfe() {
		return tipoEmitenteMdfe;
	}
	public void setTipoEmitenteMdfe(TipoEmitenteMdfe tipoemitente) {
		this.tipoEmitenteMdfe = tipoemitente;
	}

	@DisplayName("Tipo de transportador")
	public TipoTransportadorMdfe getTipoTransportador() {
		return tipoTransportador;
	}
	public void setTipoTransportador(TipoTransportadorMdfe tipotransportador) {
		this.tipoTransportador = tipotransportador;
	}

	@Required
	@DisplayName("Modalidade de frete")
	public ModalidadefreteMdfe getModalidadeFrete() {
		return modalidadeFrete;
	}
	public void setModalidadeFrete(ModalidadefreteMdfe modalidadefrete) {
		this.modalidadeFrete = modalidadefrete;
	}

	@Required
	@DisplayName("Forma de emiss�o")
	public FormaemissaoMdfe getFormaEmissao() {
		return formaEmissao;
	}
	public void setFormaEmissao(FormaemissaoMdfe formaemissao) {
		this.formaEmissao = formaemissao;
	}
	
	@DisplayName("UF de Descarregamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cduflocaldescarregamento")
	public Uf getUfLocalDescarregamento() {
		return ufLocalDescarregamento;
	}
	public void setUfLocalDescarregamento(Uf ufLocalDescarregamento) {
		this.ufLocalDescarregamento = ufLocalDescarregamento;
	}
	
	@DisplayName("UF de Carregamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cduflocalcarregamento")
	public Uf getUfLocalCarregamento() {
		return ufLocalCarregamento;
	}
	public void setUfLocalCarregamento(Uf ufLocalCarregamento) {
		this.ufLocalCarregamento = ufLocalCarregamento;
	}

	@Required
	@DisplayName("Valor total da carga/Mercadorias transportadas")
	public Money getValorTotalCargaTransportada() {
		return valorTotalCargaTransportada;
	}
	public void setValorTotalCargaTransportada(Money valorTotalCargaTransportada) {
		this.valorTotalCargaTransportada = valorTotalCargaTransportada;
	}
	
	@Required
	@DisplayName("C�digo da Unidade de Medida do Peso Bruto da Carga / Mercadorias Transportadas")
	public UnidadeMedidaPesoBrutoCarga getUnidadeMedidaPesoBrutoCarga() {
		return unidadeMedidaPesoBrutoCarga;
	}
	public void setUnidadeMedidaPesoBrutoCarga(
			UnidadeMedidaPesoBrutoCarga unidadeMedidaPesoBrutoCarga) {
		this.unidadeMedidaPesoBrutoCarga = unidadeMedidaPesoBrutoCarga;
	}
	
	@Required
	@DisplayName("Peso bruto total da carga/Mercadorias transportadas")
	public Double getPesoBrutoTotalCarga() {
		return pesoBrutoTotalCarga;
	}
	public void setPesoBrutoTotalCarga(Double pesoBrutoTotalCarga) {
		this.pesoBrutoTotalCarga = pesoBrutoTotalCarga;
	}
	
	@Transient
	@DisplayName("Quantidade total de CTe relacionados no manifesto")
	public Integer getQtdeTotalCteRelacionada() {
		return qtdeTotalCteRelacionada;
	}
	public void setQtdeTotalCteRelacionada(Integer qtdeTotalCteRelacionada) {
		this.qtdeTotalCteRelacionada = qtdeTotalCteRelacionada;
	}
	
	@Transient
	@DisplayName("Quantidade total de MDFe relacionados no manifesto")
	public Integer getQtdeTotalMdfeRelacionada() {
		return qtdeTotalMdfeRelacionada;
	}
	public void setQtdeTotalMdfeRelacionada(Integer qtdeTotalMdfeRelacionada) {
		this.qtdeTotalMdfeRelacionada = qtdeTotalMdfeRelacionada;
	}
	
	@Transient
	@DisplayName("Quantidade total de NFe relacionados no manifesto")
	public Integer getQtdeTotalNfeRelacionada() {
		return qtdeTotalNfeRelacionada;
	}
	public void setQtdeTotalNfeRelacionada(Integer qtdeTotalNfeRelacionada) {
		this.qtdeTotalNfeRelacionada = qtdeTotalNfeRelacionada;
	}
	
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("Projeto canal verde")
	public Boolean getProjetoCanalVerde() {
		return projetoCanalVerde;
	}
	public void setProjetoCanalVerde(Boolean projetoCanalVerde) {
		this.projetoCanalVerde = projetoCanalVerde;
	}
	
	public Boolean getCarregamentoNoExterior() {
		return carregamentoNoExterior;
	}
	public void setCarregamentoNoExterior(Boolean carregamentoNoExterior) {
		this.carregamentoNoExterior = carregamentoNoExterior;
	}
	
	public Boolean getDescarregamentoNoExterior() {
		return descarregamentoNoExterior;
	}
	public void setDescarregamentoNoExterior(Boolean descarregamentoNoExterior) {
		this.descarregamentoNoExterior = descarregamentoNoExterior;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;		
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@DisplayName("Local de carregamento")
	@OneToMany(mappedBy="mdfe")
	public List<MdfeLocalCarregamento> getListaLocalCarregamento() {
		return listaLocalCarregamento;
	}
	public void setListaLocalCarregamento(
			List<MdfeLocalCarregamento> listaLocalcarregamento) {
		this.listaLocalCarregamento = listaLocalcarregamento;
	}
	
	@DisplayName("Local de descarregamento")
	@OneToMany(mappedBy="mdfe")
	public List<MdfeLocalDescarregamento> getListaLocalDescarregamento() {
		return listaLocalDescarregamento;
	}
	public void setListaLocalDescarregamento(
			List<MdfeLocalDescarregamento> listaLocalDescarregamento) {
		this.listaLocalDescarregamento = listaLocalDescarregamento;
	}
	
	@DisplayName("UF's do percurso")
	@OneToMany(mappedBy="mdfe")
	public List<MdfeUfPercurso> getListaUfPercurso() {
		return listaUfPercurso;
	}
	public void setListaUfPercurso(List<MdfeUfPercurso> listaUfPercurso) {
		this.listaUfPercurso = listaUfPercurso;
	}
	
	@DisplayName("Lacres")
	@OneToMany(mappedBy="mdfe")
	public List<MdfeLacre> getListaLacre() {
		return listaLacre;
	}
	public void setListaLacre(List<MdfeLacre> listaLacre) {
		this.listaLacre = listaLacre;
	}
	
	@DisplayName("Lacres")
	@OneToMany(mappedBy="mdfe")
	public List<MdfeAutorizacaoDownload> getListaAutorizacaoDownload() {
		return listaAutorizacaoDownload;
	}
	public void setListaAutorizacaoDownload(
			List<MdfeAutorizacaoDownload> listaAutorizacaoDownload) {
		this.listaAutorizacaoDownload = listaAutorizacaoDownload;
	}
	
	@MaxLength(value=2000)
	@DisplayName("Informa��es adicionais de interesse do fisco")
	public String getInfoAdicionaisFisco() {
		return infoAdicionaisFisco;
	}
	public void setInfoAdicionaisFisco(String infoadicionaisfisco) {
		this.infoAdicionaisFisco = infoadicionaisfisco;
	}
	
	@MaxLength(value=5000)
	@DisplayName("Informa��es adicionais de interesse do contribuinte")
	public String getInfoAdicionaisContribuinte() {
		return infoAdicionaisContribuinte;
	}
	public void setInfoAdicionaisContribuinte(String infoadicionaiscontribuinte) {
		this.infoAdicionaisContribuinte = infoadicionaiscontribuinte;
	}

	@DisplayName("RNTRC")
	@MaxLength(value=8)
	public String getRntrc() {
		return rntrc;
	}
	public void setRntrc(String rntrc) {
		this.rntrc = rntrc;
	}
	
	@MaxLength(value=16)
	@DisplayName("C�digo de Agendamento no Porto")
	public String getCodigoAgendamentoPorto() {
		return codigoAgendamentoPorto;
	}
	public void setCodigoAgendamentoPorto(String codigoAgendamentoPorto) {
		this.codigoAgendamentoPorto = codigoAgendamentoPorto;
	}
	
	@MaxLength(value=10)
	@DisplayName("C�digo Interno do Ve�culo")
	public String getCodigoInternoVeiculo() {
		return codigoInternoVeiculo;
	}
	public void setCodigoInternoVeiculo(String codigoInternoVeiculo) {
		this.codigoInternoVeiculo = codigoInternoVeiculo;
	}
	
	@DisplayName("Tipo de Carroceria")
	public TipoCarroceria getTipoCarroceria() {
		return tipoCarroceria;
	}
	public void setTipoCarroceria(TipoCarroceria tipoCarroceria) {
		this.tipoCarroceria = tipoCarroceria;
	}
	
	@MinLength(value=8)
	@MaxLength(value=8)
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	
	@MaxLength(value=6)
	@DisplayName("Tara (KG)")
	public String getTara() {
		return tara;
	}
	public void setTara(String tara) {
		this.tara = tara;
	}
	
	@MinLength(value=9)
	@MaxLength(value=11)
	@DisplayName("RENAVAM")
	public String getRenavam() {
		return renavam;
	}
	public void setRenavam(String renavam) {
		this.renavam = renavam;
	}
	
	@DisplayName("UF")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdUfFreteRodoviario")
	public Uf getUfFreteRodoviario() {
		return ufFreteRodoviario;
	}
	public void setUfFreteRodoviario(Uf ufFreteRodoviario) {
		this.ufFreteRodoviario = ufFreteRodoviario;
	}
	
	@DisplayName("Tipo de Rodado")
	public TipoRodado getTipoRodado() {
		return tipoRodado;
	}
	public void setTipoRodado(TipoRodado tipoRodado) {
		this.tipoRodado = tipoRodado;
	}
	
	@MaxLength(value=6)
	@DisplayName("Capacidade (KG)")
	public String getCapacidadeKgRodoviario() {
		return capacidadeKgRodoviario;
	}
	public void setCapacidadeKgRodoviario(String capacidadeKgRodoviario) {
		this.capacidadeKgRodoviario = capacidadeKgRodoviario;
	}
	
	@MaxLength(value=3)
	@DisplayName("Capacidade (M3)")
	public String getCapacidadeM3Rodoviario() {
		return capacidadeM3Rodoviario;
	}
	public void setCapacidadeM3Rodoviario(String capacidadeM3Rodoviario) {
		this.capacidadeM3Rodoviario = capacidadeM3Rodoviario;
	}
	
	@DisplayName("Ve�culo Pertence � Empresa Emitente do MDFe")
	public Boolean getPertenceEmpresaEmitente() {
		return pertenceEmpresaEmitente;
	}
	public void setPertenceEmpresaEmitente(Boolean pertenceEmpresaEmitente) {
		this.pertenceEmpresaEmitente = pertenceEmpresaEmitente;
	}
	
	@DisplayName("Tipo de Pessoa")
	public Tipopessoa getTipoPessoaProprietarioRodoviario() {
		return tipoPessoaProprietarioRodoviario;
	}
	public void setTipoPessoaProprietarioRodoviario(
			Tipopessoa tipoPessoaProprietarioRodoviario) {
		this.tipoPessoaProprietarioRodoviario = tipoPessoaProprietarioRodoviario;
	}
	
	@MaxLength(value=60)
	@DisplayName("Nome / Raz�o Social")
	public String getNomeProprietarioRodoviario() {
		return nomeProprietarioRodoviario;
	}
	public void setNomeProprietarioRodoviario(String nomeProprietarioRodoviario) {
		this.nomeProprietarioRodoviario = nomeProprietarioRodoviario;
	}
	
	public Cpf getCpfProprietarioRodoviario() {
		return cpfProprietarioRodoviario;
	}
	public void setCpfProprietarioRodoviario(Cpf cpfProprietarioRodoviario) {
		this.cpfProprietarioRodoviario = cpfProprietarioRodoviario;
	}
	
	public Cnpj getCnpjProprietarioRodoviario() {
		return cnpjProprietarioRodoviario;
	}
	public void setCnpjProprietarioRodoviario(Cnpj cnpjProprietarioRodoviario) {
		this.cnpjProprietarioRodoviario = cnpjProprietarioRodoviario;
	}
	
	@DisplayName("Inscri��o Estadual")
	public String getInscEstProprietarioRodoviario() {
		return inscEstProprietarioRodoviario;
	}
	public void setInscEstProprietarioRodoviario(
			String inscEstProprietarioRodoviario) {
		this.inscEstProprietarioRodoviario = inscEstProprietarioRodoviario;
	}
	
	@DisplayName("Isento")
	public Boolean getIsentoRodoviario() {
		return isentoRodoviario;
	}
	public void setIsentoRodoviario(Boolean isentoRodoviario) {
		this.isentoRodoviario = isentoRodoviario;
	}
	
	@DisplayName("RNTRC")
	@MaxLength(value=8)
	public String getRntrcProprietario() {
		return rntrcProprietario;
	}
	public void setRntrcProprietario(String rntrcProprietario) {
		this.rntrcProprietario = rntrcProprietario;
	}
	
	@DisplayName("UF")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdufproprietariorodoviario")
	public Uf getUfProprietarioRodoviario() {
		return ufProprietarioRodoviario;
	}
	public void setUfProprietarioRodoviario(Uf ufProprietarioRodoviario) {
		this.ufProprietarioRodoviario = ufProprietarioRodoviario;
	}
	
	@DisplayName("Tipo de Propriet�rio")
	public Mdfetipoproprietarioveiculo getTipoProprietarioVeiculoRodoviario() {
		return tipoProprietarioVeiculoRodoviario;
	}
	public void setTipoProprietarioVeiculoRodoviario(
			Mdfetipoproprietarioveiculo tipoProprietarioVeiculoRodoviario) {
		this.tipoProprietarioVeiculoRodoviario = tipoProprietarioVeiculoRodoviario;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeRodoviarioCondutor> getListaCondutor() {
		return listaCondutor;
	}
	public void setListaCondutor(List<MdfeRodoviarioCondutor> listaCondutor) {
		this.listaCondutor = listaCondutor;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeRodoviarioReboque> getListaReboque() {
		return listaReboque;
	}
	public void setListaReboque(List<MdfeRodoviarioReboque> listaReboque) {
		this.listaReboque = listaReboque;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeRodoviarioCiot> getListaCiot() {
		return listaCiot;
	}
	public void setListaCiot(List<MdfeRodoviarioCiot> listaCiot) {
		this.listaCiot = listaCiot;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeRodoviarioContratante> getListaContratante() {
		return listaContratante;
	}
	public void setListaContratante(List<MdfeRodoviarioContratante> listaContratante) {
		this.listaContratante = listaContratante;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeRodoviarioValePedagio> getListaValePedagio() {
		return listaValePedagio;
	}
	public void setListaValePedagio(List<MdfeRodoviarioValePedagio> listaValePedagio) {
		this.listaValePedagio = listaValePedagio;
	}
	
	@MaxLength(value=4)
	@DisplayName("Marca da nacionalidade da aeronave")
	public String getMarcaNacionalidadeAeronave() {
		return marcaNacionalidadeAeronave;
	}
	public void setMarcaNacionalidadeAeronave(String marcaNacionalidadeAeronave) {
		this.marcaNacionalidadeAeronave = marcaNacionalidadeAeronave;
	}
	
	@MaxLength(value=6)
	@DisplayName("Marca de matr�cula da aeronave")
	public String getMarcaMatriculaAeronave() {
		return marcaMatriculaAeronave;
	}
	public void setMarcaMatriculaAeronave(String marcamatriculaaeronave) {
		this.marcaMatriculaAeronave = marcamatriculaaeronave;
	}
	
	@MaxLength(value=9)
	@DisplayName("N�mero do v�o")
	public String getNumeroVoo() {
		return numeroVoo;
	}
	public void setNumeroVoo(String numerovoo) {
		this.numeroVoo = numerovoo;
	}
	
	@MaxLength(value=4)
	@DisplayName("Aer�dromo de embarque")
	public String getAerodromoEmbarque() {
		return aerodromoEmbarque;
	}
	public void setAerodromoEmbarque(String aerodromoembarque) {
		this.aerodromoEmbarque = aerodromoembarque;
	}
	
	@MaxLength(value=4)
	@DisplayName("Aer�dromo de destino")
	public String getAerodromoDestino() {
		return aerodromoDestino;
	}
	public void setAerodromoDestino(String aerodromodestino) {
		this.aerodromoDestino = aerodromodestino;
	}
	
	@DisplayName("Data do v�o")
	public Date getDataVoo() {
		return dataVoo;
	}
	public void setDataVoo(Date datavoo) {
		this.dataVoo = datavoo;
	}

	@MaxLength(value=10)
	@DisplayName("Irin do Navio")
	public String getIrinNavio() {
		return irinNavio;
	}
	public void setIrinNavio(String irinnavio) {
		this.irinNavio = irinnavio;
	}
	
	@MaxLength(value=2)
	@MinLength(value=2)
	@DisplayName("C�digo do tipo de embarca��o")
	public String getCodigoTipoEmbarcacao() {
		return codigoTipoEmbarcacao;
	}
	public void setCodigoTipoEmbarcacao(String codigotipoembarcacao) {
		this.codigoTipoEmbarcacao = codigotipoembarcacao;
	}
	
	@MaxLength(value=10)
	@DisplayName("C�digo da embarca��o")
	public String getCodigoEmbarcacao() {
		return codigoEmbarcacao;
	}
	public void setCodigoEmbarcacao(String codigoembarcacao) {
		this.codigoEmbarcacao = codigoembarcacao;
	}
	
	@MaxLength(value=10)
	@DisplayName("N�mero da viagem")
	public String getNumeroViagemEmbarcacao() {
		return numeroViagemEmbarcacao;
	}
	public void setNumeroViagemEmbarcacao(String numeroviagemembarcacao) {
		this.numeroViagemEmbarcacao = numeroviagemembarcacao;
	}
	
	@MaxLength(value=60)
	@DisplayName("Nome da embarca��o")
	public String getNomeEmbarcacao() {
		return nomeEmbarcacao;
	}
	public void setNomeEmbarcacao(String nomeembarcacao) {
		this.nomeEmbarcacao = nomeembarcacao;
	}
	
	@MaxLength(value=5)
	@DisplayName("C�digo do porto de embarque")
	public String getCodigoPortoEmbarque() {
		return codigoPortoEmbarque;
	}
	public void setCodigoPortoEmbarque(String codigoportoembarque) {
		this.codigoPortoEmbarque = codigoportoembarque;
	}
	
	@MaxLength(value=5)
	@DisplayName("C�digo do porto de destino")
	public String getCodigoPortoDestino() {
		return codigoPortoDestino;
	}
	public void setCodigoPortoDestino(String codigoportodestino) {
		this.codigoPortoDestino = codigoportodestino;
	}
	
	@MaxLength(value=60)
	@DisplayName("Porto de transbordo")
	public String getPortoTransbordo() {
		return portoTransbordo;
	}
	public void setPortoTransbordo(String portotransbordo) {
		this.portoTransbordo = portotransbordo;
	}
	
	@DisplayName("Tipo de navega��o")
	public TipoNavegacaoAquaviario getTipoNavegacao() {
		return tipoNavegacao;
	}
	public void setTipoNavegacao(TipoNavegacaoAquaviario tiponavegacao) {
		this.tipoNavegacao = tiponavegacao;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeAquaviarioTerminalCarregamento> getListaAquaviarioTerminalCarregamento() {
		return listaAquaviarioTerminalCarregamento;
	}
	public void setListaAquaviarioTerminalCarregamento(
			List<MdfeAquaviarioTerminalCarregamento> listaAquaviarioTerminalCarregamento) {
		this.listaAquaviarioTerminalCarregamento = listaAquaviarioTerminalCarregamento;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeAquaviarioTerminalDescarregamento> getListaAquaviarioTerminalDescarregamento() {
		return listaAquaviarioTerminalDescarregamento;
	}
	public void setListaAquaviarioTerminalDescarregamento(
			List<MdfeAquaviarioTerminalDescarregamento> listaAquaviarioTerminalDescarregamento) {
		this.listaAquaviarioTerminalDescarregamento = listaAquaviarioTerminalDescarregamento;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeAquaviarioUnidadeTransporteVazia> getListaAquaviarioUnidadeTransporteVazia() {
		return listaAquaviarioUnidadeTransporteVazia;
	}
	public void setListaAquaviarioUnidadeTransporteVazia(
			List<MdfeAquaviarioUnidadeTransporteVazia> listaAquaviarioUnidadeTransporteVazia) {
		this.listaAquaviarioUnidadeTransporteVazia = listaAquaviarioUnidadeTransporteVazia;
	}
	
	@MaxLength(value=10)
	@DisplayName("Prefixo do trem")
	public String getPrefixoTrem() {
		return prefixoTrem;
	}
	public void setPrefixoTrem(String prefixoTrem) {
		this.prefixoTrem = prefixoTrem;
	}

	@DisplayName("Data e Hora de Libera��o do Trem na Origem")
	public Timestamp getDataHoraLiberacaoTremOrigem() {
		return dataHoraLiberacaoTremOrigem;
	}
	public void setDataHoraLiberacaoTremOrigem(Timestamp dataHoraLiberacaoTremOrigem) {
		this.dataHoraLiberacaoTremOrigem = dataHoraLiberacaoTremOrigem;
	}
	
	@MaxLength(value=3)
	@DisplayName("Origem do trem")
	public String getOrigemTrem() {
		return origemTrem;
	}
	public void setOrigemTrem(String origemTrem) {
		this.origemTrem = origemTrem;
	}
	
	@MaxLength(value=3)
	@DisplayName("Destino do trem")
	public String getDestinoTrem() {
		return destinoTrem;
	}
	public void setDestinoTrem(String destinoTrem) {
		this.destinoTrem = destinoTrem;
	}
	
	@DisplayName("Quantidade de Vag�es Carregados")
	public Integer getQtdeVagoesCarregados() {
		return qtdeVagoesCarregados;
	}
	public void setQtdeVagoesCarregados(Integer qtdeVagoesCarregados) {
		this.qtdeVagoesCarregados = qtdeVagoesCarregados;
	}
	
	public String getQrCode() {
		return qrCode;
	}
	
	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeFerroviarioVagao> getListaVagao() {
		return listaVagao;
	}
	public void setListaVagao(List<MdfeFerroviarioVagao> listaVagao) {
		this.listaVagao = listaVagao;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeAquaviarioUnidadeCargaVazia> getListaAquaviarioUnidadeCargaVazia() {
		return listaAquaviarioUnidadeCargaVazia;
	}
	public void setListaAquaviarioUnidadeCargaVazia(
			List<MdfeAquaviarioUnidadeCargaVazia> listaAquaviarioUnidadeCargaVazia) {
		this.listaAquaviarioUnidadeCargaVazia = listaAquaviarioUnidadeCargaVazia;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeCte> getListaMdfeCte() {
		return listaMdfeCte;
	}
	public void setListaMdfeCte(List<MdfeCte> listaMdfeCte) {
		this.listaMdfeCte = listaMdfeCte;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeNfe> getListaNfe() {
		return listaNfe;
	}
	public void setListaNfe(List<MdfeNfe> listaNfe) {
		this.listaNfe = listaNfe;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeAquaviarioEmbarcacaoComboio> getListaAquaviarioEmbarcacaoComboio() {
		return listaAquaviarioEmbarcacaoComboio;
	}
	public void setListaAquaviarioEmbarcacaoComboio(
			List<MdfeAquaviarioEmbarcacaoComboio> listaAquaviarioEmbarcacaoComboio) {
		this.listaAquaviarioEmbarcacaoComboio = listaAquaviarioEmbarcacaoComboio;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeInformacaoUnidadeTransporteCte> getListaInformacaoUnidadeTransporteCte() {
		return listaInformacaoUnidadeTransporteCte;
	}
	public void setListaInformacaoUnidadeTransporteCte(
			List<MdfeInformacaoUnidadeTransporteCte> listaInformacaoUnidadeTransporteCte) {
		this.listaInformacaoUnidadeTransporteCte = listaInformacaoUnidadeTransporteCte;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeInformacaoUnidadeCargaCte> getListaInformacaoUnidadeCargaCte() {
		return listaInformacaoUnidadeCargaCte;
	}
	public void setListaInformacaoUnidadeCargaCte(
			List<MdfeInformacaoUnidadeCargaCte> listaInformacaoUnidadeCargaCte) {
		this.listaInformacaoUnidadeCargaCte = listaInformacaoUnidadeCargaCte;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeLacreUnidadeTransporteCte> getListaLacreUnidadeTransporteCte() {
		return listaLacreUnidadeTransporteCte;
	}
	public void setListaLacreUnidadeTransporteCte(
			List<MdfeLacreUnidadeTransporteCte> listaLacreUnidadeTransporteCte) {
		this.listaLacreUnidadeTransporteCte = listaLacreUnidadeTransporteCte;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeLacreUnidadeCargaCte> getListaLacreUnidadeCargaCte() {
		return listaLacreUnidadeCargaCte;
	}
	public void setListaLacreUnidadeCargaCte(
			List<MdfeLacreUnidadeCargaCte> listaLacreUnidadeCargaCte) {
		this.listaLacreUnidadeCargaCte = listaLacreUnidadeCargaCte;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeProdutoPerigosoCte> getListaProdutoPerigosoCte() {
		return listaProdutoPerigosoCte;
	}
	public void setListaProdutoPerigosoCte(
			List<MdfeProdutoPerigosoCte> listaProdutoPerigosoCte) {
		this.listaProdutoPerigosoCte = listaProdutoPerigosoCte;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeInformacaoUnidadeTransporteNfe> getListaInformacaoUnidadeTransporteNfe() {
		return listaInformacaoUnidadeTransporteNfe;
	}
	public void setListaInformacaoUnidadeTransporteNfe(
			List<MdfeInformacaoUnidadeTransporteNfe> listaInformacaoUnidadeTransporteNfe) {
		this.listaInformacaoUnidadeTransporteNfe = listaInformacaoUnidadeTransporteNfe;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeInformacaoUnidadeCargaNfe> getListaInformacaoUnidadeCargaNfe() {
		return listaInformacaoUnidadeCargaNfe;
	}
	public void setListaInformacaoUnidadeCargaNfe(
			List<MdfeInformacaoUnidadeCargaNfe> listaInformacaoUnidadeCargaNfe) {
		this.listaInformacaoUnidadeCargaNfe = listaInformacaoUnidadeCargaNfe;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeLacreUnidadeTransporteNfe> getListaLacreUnidadeTransporteNfe() {
		return listaLacreUnidadeTransporteNfe;
	}
	public void setListaLacreUnidadeTransporteNfe(
			List<MdfeLacreUnidadeTransporteNfe> listaLacreUnidadeTransporteNfe) {
		this.listaLacreUnidadeTransporteNfe = listaLacreUnidadeTransporteNfe;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeLacreUnidadeCargaNfe> getListaLacreUnidadeCargaNfe() {
		return listaLacreUnidadeCargaNfe;
	}
	public void setListaLacreUnidadeCargaNfe(
			List<MdfeLacreUnidadeCargaNfe> listaLacreUnidadeCargaNfe) {
		this.listaLacreUnidadeCargaNfe = listaLacreUnidadeCargaNfe;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeProdutoPerigosoNfe> getListaProdutoPerigosoNfe() {
		return listaProdutoPerigosoNfe;
	}
	public void setListaProdutoPerigosoNfe(
			List<MdfeProdutoPerigosoNfe> listaProdutoPerigosoNfe) {
		this.listaProdutoPerigosoNfe = listaProdutoPerigosoNfe;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeReferenciado> getListaMdfeReferenciado() {
		return listaMdfeReferenciado;
	}
	public void setListaMdfeReferenciado(
			List<MdfeReferenciado> listaMdfeReferenciado) {
		this.listaMdfeReferenciado = listaMdfeReferenciado;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeInformacaoUnidadeTransporteMdfeReferenciado> getListaInformacaoUnidadeTransporteMdfeReferenciado() {
		return listaInformacaoUnidadeTransporteMdfeReferenciado;
	}
	public void setListaInformacaoUnidadeTransporteMdfeReferenciado(
			List<MdfeInformacaoUnidadeTransporteMdfeReferenciado> listaInformacaoUnidadeTransporteMdfeReferenciado) {
		this.listaInformacaoUnidadeTransporteMdfeReferenciado = listaInformacaoUnidadeTransporteMdfeReferenciado;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeInformacaoUnidadeCargaMdfeReferenciado> getListaInformacaoUnidadeCargaMdfeReferenciado() {
		return listaInformacaoUnidadeCargaMdfeReferenciado;
	}
	public void setListaInformacaoUnidadeCargaMdfeReferenciado(
			List<MdfeInformacaoUnidadeCargaMdfeReferenciado> listaInformacaoUnidadeCargaMdfeReferenciado) {
		this.listaInformacaoUnidadeCargaMdfeReferenciado = listaInformacaoUnidadeCargaMdfeReferenciado;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeLacreUnidadeTransporteMdfeReferenciado> getListaLacreUnidadeTransporteMdfeReferenciado() {
		return listaLacreUnidadeTransporteMdfeReferenciado;
	}
	public void setListaLacreUnidadeTransporteMdfeReferenciado(
			List<MdfeLacreUnidadeTransporteMdfeReferenciado> listaLacreUnidadeTransporteMdfeReferenciado) {
		this.listaLacreUnidadeTransporteMdfeReferenciado = listaLacreUnidadeTransporteMdfeReferenciado;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeLacreUnidadeCargaMdfeReferenciado> getListaLacreUnidadeCargaMdfeReferenciado() {
		return listaLacreUnidadeCargaMdfeReferenciado;
	}
	public void setListaLacreUnidadeCargaMdfeReferenciado(
			List<MdfeLacreUnidadeCargaMdfeReferenciado> listaLacreUnidadeCargaMdfeReferenciado) {
		this.listaLacreUnidadeCargaMdfeReferenciado = listaLacreUnidadeCargaMdfeReferenciado;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeProdutoPerigosoMdfeReferenciado> getListaProdutoPerigosoMdfeReferenciado() {
		return listaProdutoPerigosoMdfeReferenciado;
	}
	public void setListaProdutoPerigosoMdfeReferenciado(
			List<MdfeProdutoPerigosoMdfeReferenciado> listaProdutoPerigosoMdfeReferenciado) {
		this.listaProdutoPerigosoMdfeReferenciado = listaProdutoPerigosoMdfeReferenciado;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeSeguroCargaNumeroAverbacao> getListaSeguroCargaNumeroAverbacao() {
		return listaSeguroCargaNumeroAverbacao;
	}
	public void setListaSeguroCargaNumeroAverbacao(
			List<MdfeSeguroCargaNumeroAverbacao> listaSeguroCargaNumeroAverbacao) {
		this.listaSeguroCargaNumeroAverbacao = listaSeguroCargaNumeroAverbacao;
	}
	
	@OneToMany(mappedBy="mdfe")
	public List<MdfeSeguroCarga> getListaSeguroCarga() {
		return listaSeguroCarga;
	}
	public void setListaSeguroCarga(List<MdfeSeguroCarga> listaSeguroCarga) {
		this.listaSeguroCarga = listaSeguroCarga;
	}
	
	@Transient
	@DisplayName("Hist�rico")
	public List<MdfeHistorico> getListaHistoricoTrans() {
		return listaHistoricoTrans;
	}
	public void setListaHistoricoTrans(List<MdfeHistorico> listaHistoricoTrans) {
		this.listaHistoricoTrans = listaHistoricoTrans;
	}
	
	@Transient
	public MdfeSituacao getMdfeSituacaoHistoricoTrans() {
		return mdfeSituacaoHistoricoTrans;
	}
	public void setMdfeSituacaoHistoricoTrans(
			MdfeSituacao mdfeSituacaoHistoricoTrans) {
		this.mdfeSituacaoHistoricoTrans = mdfeSituacaoHistoricoTrans;
	}

	@Transient
	public Boolean getHaveArquivoMdfe() {
		return haveArquivoMdfe;
	}

	public void setHaveArquivoMdfe(Boolean haveArquivoMdfe) {
		this.haveArquivoMdfe = haveArquivoMdfe;
	}

	@Transient
	public String getChaveAcesso() {
		return chaveAcesso;
	}

	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}
	
	@Transient
	public Boolean getUsarSequencial() {
		return usarSequencial;
	}
	
	public void setUsarSequencial(Boolean usarSequencial) {
		this.usarSequencial = usarSequencial;
	}
}