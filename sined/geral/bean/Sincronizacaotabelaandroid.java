package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Tabelaandroid;


@Entity
@SequenceGenerator(name = "sq_sincronizacaotabelaandroid", sequenceName = "sq_sincronizacaotabelaandroid")
public class Sincronizacaotabelaandroid {
	
	protected Integer cdsincronizacaotabelaandroid;
	protected Tabelaandroid tabelaandroid;
	protected Integer tabelaid;
	protected Timestamp dtalteracao;
	protected Boolean excluido;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_sincronizacaotabelaandroid")
	public Integer getCdsincronizacaotabelaandroid() {
		return cdsincronizacaotabelaandroid;
	}
	public Tabelaandroid getTabelaandroid() {
		return tabelaandroid;
	}
	public Integer getTabelaid() {
		return tabelaid;
	}
	public void setCdsincronizacaotabelaandroid(Integer cdsincronizacaotabelaandroid) {
		this.cdsincronizacaotabelaandroid = cdsincronizacaotabelaandroid;
	}
	public void setTabelaandroid(Tabelaandroid tabelaandroid) {
		this.tabelaandroid = tabelaandroid;
	}
	public void setTabelaid(Integer tabelaid) {
		this.tabelaid = tabelaid;
	}
	public Timestamp getDtalteracao() {
		return dtalteracao;
	}
	public void setDtalteracao(Timestamp dtalteracao) {
		this.dtalteracao = dtalteracao;
	}
	public Boolean isExcluido() {
		return excluido;
	}
	public void setExcluido(Boolean excluido) {
		this.excluido = excluido;
	}
}
