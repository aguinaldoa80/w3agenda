package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_entregadocumentofreterateio", sequenceName="sq_entregadocumentofreterateio")
public class Entregadocumentofreterateio {
	
	protected Integer cdentregadocumentofreterateio;
	protected Entregadocumento entregadocumento;
	protected Entregamaterial entregamaterial;
	protected Double percentual;
	protected Money valor;
	
	public Entregadocumentofreterateio(){
	}
	
	public Entregadocumentofreterateio(Entregamaterial entregamaterial){
		this.entregamaterial = entregamaterial;
	}
	
	public Entregadocumentofreterateio(Entregadocumento entregadocumento, Entregamaterial entregamaterial){
		this.entregadocumento = entregadocumento;
		this.entregamaterial = entregamaterial;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_entregadocumentofreterateio")
	public Integer getCdentregadocumentofreterateio() {
		return cdentregadocumentofreterateio;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregadocumento")
	public Entregadocumento getEntregadocumento() {
		return entregadocumento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregamaterial")
	public Entregamaterial getEntregamaterial() {
		return entregamaterial;
	}
	@Required
	@MaxLength(6)
	public Double getPercentual() {
		return percentual;
	}
	@Required
	public Money getValor() {
		return valor;
	}
	
	public void setCdentregadocumentofreterateio(Integer cdentregadocumentofreterateio) {
		this.cdentregadocumentofreterateio = cdentregadocumentofreterateio;
	}
	public void setEntregadocumento(Entregadocumento entregadocumento) {
		this.entregadocumento = entregadocumento;
	}
	public void setEntregamaterial(Entregamaterial entregamaterial) {
		this.entregamaterial = entregamaterial;
	}
	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Entregadocumentofreterateio) {
			Entregadocumentofreterateio entregadocumentofreterateio = (Entregadocumentofreterateio) obj;
			return entregadocumentofreterateio.getCdentregadocumentofreterateio().equals(this.getCdentregadocumentofreterateio());
		}
		return super.equals(obj);
	}
}