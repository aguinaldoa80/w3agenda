package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.LicenseManager;


@Entity
@SequenceGenerator(name = "sq_licenca", sequenceName = "sq_licenca")
public class Licenca{

	protected Integer cdlicenca;
	protected String item;
	
	// Transient
	protected String itemdescript;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_licenca")
	public Integer getCdlicenca() {
		return cdlicenca;
	}

	@Required
	@MaxLength(500)
	public String getItem() {
		return item;
	}
	
	public void setCdlicenca(Integer cdlicenca) {
		this.cdlicenca = cdlicenca;
	}
	
	public void setItem(String item) {
		this.item = item;
	}
	
	@Transient
	public String getItemdescript() {
		return LicenseManager.decript(getItem());
	}
	
	public void setItemdescript(String itemdescript) {
		this.itemdescript = itemdescript;
	}
	
}
