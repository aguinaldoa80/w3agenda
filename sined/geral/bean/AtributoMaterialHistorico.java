package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.enumeration.AtributosMaterialAcao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_atributomaterialhistorico", sequenceName = "sq_atributomaterialhistorico")
public class AtributoMaterialHistorico implements Log{

	protected Integer cdatributomaterialhistorico;
	protected String observacao;
	protected AtributosMaterialAcao acao;
	
	protected AtributosMaterial atributosMaterial;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_atributomaterialhistorico")
	public Integer getCdatributomaterialhistorico() {
		return cdatributomaterialhistorico;
	}
	public void setCdatributomaterialhistorico(Integer cdatributomaterialhistorico) {
		this.cdatributomaterialhistorico = cdatributomaterialhistorico;
	}
	
	@MaxLength(1024)
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@DisplayName("Atributos do Material")
	@JoinColumn(name="cdatributosmaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public AtributosMaterial getAtributosMaterial() {
		return atributosMaterial;
	}
	public void setAtributosMaterial(AtributosMaterial atributosMaterial) {
		this.atributosMaterial = atributosMaterial;
	}
	@DisplayName("Respons�vel")
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	@DisplayName("A��o")
	public AtributosMaterialAcao getAcao() {
		return acao;
	}
	public void setAcao(AtributosMaterialAcao acao) {
		this.acao = acao;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
}
