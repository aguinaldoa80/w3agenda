package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoagendasituacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@Entity
@SequenceGenerator(name = "sq_producaoagenda", sequenceName = "sq_producaoagenda")
@DisplayName("Agenda de produ��o")
@JoinEmpresa("producaoagenda.empresa")
public class Producaoagenda implements Log{
	
	protected Integer cdproducaoagenda; 
	protected Empresa empresa;
	protected Producaoagendatipo producaoagendatipo;
	protected Cliente cliente;
	protected Contrato contrato;
	protected Pedidovenda pedidovenda;
	protected Venda venda;
	protected Projeto projeto;
	protected Cemobra cemobra;
	protected Timestamp dtentrega;
	protected Localarmazenagem localbaixamateriaprima;
	protected Producaoagendasituacao producaoagendasituacao = Producaoagendasituacao.EM_ESPERA;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Set<Producaoordemmaterialorigem> listaProducaoordemmaterialorigem = new ListSet<Producaoordemmaterialorigem>(Producaoordemmaterialorigem.class);
	protected Set<Producaoagendamaterialitem> listaProducaoagendamaterialitem = new ListSet<Producaoagendamaterialitem>(Producaoagendamaterialitem.class);
	protected Set<Producaoagendamaterial> listaProducaoagendamaterial = new ListSet<Producaoagendamaterial>(Producaoagendamaterial.class);
	protected List<Producaoagendahistorico> listaProducaoagendahistorico = new ListSet<Producaoagendahistorico>(Producaoagendahistorico.class);
	protected List<Producaoagendaitemadicional> listaProducaoagendaitemadicional = new ListSet<Producaoagendaitemadicional>(Producaoagendaitemadicional.class);
	
	// TRANSIENTES
	protected Boolean pendenciafinanceira = Boolean.FALSE;
	protected Boolean comprasolicitada = Boolean.FALSE;
	protected Boolean expedicaopendente = Boolean.FALSE;
	protected Boolean contratocancelado = Boolean.FALSE;
	protected Boolean existereserva = Boolean.FALSE;
	protected String whereInSaidaMateriaprima;
	protected Integer cdmotivodevolucao;
	protected String observacaorecusa;
	protected List<Motivodevolucao> listaMotivodevolucao;
	
	protected Material materialmestreProduzir;
	protected Material materialProduzir;

	public Producaoagenda() {
	}
	
	public Producaoagenda(Integer cdproducaoagenda) {
		this.cdproducaoagenda = cdproducaoagenda;
	}

	public Producaoagenda(Integer cdproducaoagenda, Integer cdmaterialmestre, Integer cdmaterial) {
		this.cdproducaoagenda = cdproducaoagenda;
		if(cdmaterialmestre != null){
			this.materialmestreProduzir = new Material(cdmaterialmestre);
		}
		if(cdmaterial != null){
			this.materialProduzir = new Material(cdmaterial);
		}
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_producaoagenda")
	public Integer getCdproducaoagenda() {
		return cdproducaoagenda;
	}
	
	@DisplayName("Tipo de Produ��o")
	@JoinColumn(name="cdproducaoagendatipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoagendatipo getProducaoagendatipo() {
		return producaoagendatipo;
	}

	@JoinColumn(name="cdcliente")
	@ManyToOne(fetch=FetchType.LAZY)
	public Cliente getCliente() {
		return cliente;
	}
	
	@JoinColumn(name="cdcontrato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contrato getContrato() {
		return contrato;
	}
	
	@DisplayName("Pedido de venda")
	@JoinColumn(name="cdpedidovenda")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	
	@JoinColumn(name="cdprojeto")
	@ManyToOne(fetch=FetchType.LAZY)
	public Projeto getProjeto() {
		return projeto;
	}
	
	@Required
	@JoinColumn(name="cdempresa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Required
	@DisplayName("Data de entrega")
	public Timestamp getDtentrega() {
		return dtentrega;
	}
	
	@DisplayName("Local de baixa das mat�rias-primas")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalbaixamateriaprima")
	public Localarmazenagem getLocalbaixamateriaprima() {
		return localbaixamateriaprima;
	}

	@Required
	@DisplayName("Situa��o")
	public Producaoagendasituacao getProducaoagendasituacao() {
		return producaoagendasituacao;
	}
	
	@JoinColumn(name="cdcemobra")
	@ManyToOne(fetch=FetchType.LAZY)
	public Cemobra getCemobra() {
		return cemobra;
	}
	
	public void setCemobra(Cemobra cemobra) {
		this.cemobra = cemobra;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public void setCdproducaoagenda(Integer cdproducaoagenda) {
		this.cdproducaoagenda = cdproducaoagenda;
	}
	
	public void setProducaoagendatipo(Producaoagendatipo producaoagendatipo) {
		this.producaoagendatipo = producaoagendatipo;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}

	public void setDtentrega(Timestamp dtentrega) {
		this.dtentrega = dtentrega;
	}

	public void setLocalbaixamateriaprima(Localarmazenagem localbaixamateriaprima) {
		this.localbaixamateriaprima = localbaixamateriaprima;
	}

	public void setProducaoagendasituacao(
			Producaoagendasituacao producaoagendasituacao) {
		this.producaoagendasituacao = producaoagendasituacao;
	}
	
	// LISTAS
	
	@OneToMany(mappedBy="producaoagenda")
	public List<Producaoagendahistorico> getListaProducaoagendahistorico() {
		return listaProducaoagendahistorico;
	}
	
	@OneToMany(mappedBy="producaoagenda")
	public Set<Producaoagendamaterial> getListaProducaoagendamaterial() {
		return listaProducaoagendamaterial;
	}
	
	@OneToMany(mappedBy="producaoagenda")
	public Set<Producaoordemmaterialorigem> getListaProducaoordemmaterialorigem() {
		return listaProducaoordemmaterialorigem;
	}
	
	@OneToMany(mappedBy="producaoagenda")
	public Set<Producaoagendamaterialitem> getListaProducaoagendamaterialitem() {
		return listaProducaoagendamaterialitem;
	}
	
	public void setListaProducaoagendamaterialitem(
			Set<Producaoagendamaterialitem> listaProducaoagendamaterialitem) {
		this.listaProducaoagendamaterialitem = listaProducaoagendamaterialitem;
	}
	
	public void setListaProducaoordemmaterialorigem(
			Set<Producaoordemmaterialorigem> listaProducaoordemmaterialorigem) {
		this.listaProducaoordemmaterialorigem = listaProducaoordemmaterialorigem;
	}
	
	public void setListaProducaoagendahistorico(
			List<Producaoagendahistorico> listaProducaoagendahistorico) {
		this.listaProducaoagendahistorico = listaProducaoagendahistorico;
	}
	
	public void setListaProducaoagendamaterial(
			Set<Producaoagendamaterial> listaProducaoagendamaterial) {
		this.listaProducaoagendamaterial = listaProducaoagendamaterial;
	}

	//	LOG	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	
	// TRANSIENTE
	
	@Transient
	public Boolean getPendenciafinanceira() {
		return pendenciafinanceira;
	}
	
	@Transient
	public Boolean getComprasolicitada() {
		return comprasolicitada;
	}
	
	@Transient
	public Boolean getExpedicaopendente() {
		return expedicaopendente;
	}
	
	@Transient
	public Boolean getContratocancelado() {
		return contratocancelado;
	}
	
	@DisplayName("Venda")
	@JoinColumn(name="cdvenda")
	@ManyToOne(fetch=FetchType.LAZY)
	public Venda getVenda() {
		return venda;
	}
	
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	
	public void setPendenciafinanceira(Boolean pendenciafinanceira) {
		this.pendenciafinanceira = pendenciafinanceira;
	}
	
	public void setComprasolicitada(Boolean comprasolicitada) {
		this.comprasolicitada = comprasolicitada;
	}
	
	public void setExpedicaopendente(Boolean expedicaopendente) {
		this.expedicaopendente = expedicaopendente;
	}
	
	public void setContratocancelado(Boolean contratocancelado) {
		this.contratocancelado = contratocancelado;
	}
	
	@Transient
	public Material getMaterialmestreProduzir() {
		return materialmestreProduzir;
	}
	@Transient
	public Material getMaterialProduzir() {
		return materialProduzir;
	}

	@Transient
	public Boolean getExistereserva() {
		return existereserva;
	}
	
	public void setMaterialmestreProduzir(Material materialmestreProduzir) {
		this.materialmestreProduzir = materialmestreProduzir;
	}
	public void setMaterialProduzir(Material materialProduzir) {
		this.materialProduzir = materialProduzir;
	}
	
	public void setExistereserva(Boolean existereserva) {
		this.existereserva = existereserva;
	}
	
	@Transient
	public String getWhereInSaidaMateriaprima() {
		return whereInSaidaMateriaprima;
	}

	public void setWhereInSaidaMateriaprima(String whereInSaidaMateriaprima) {
		this.whereInSaidaMateriaprima = whereInSaidaMateriaprima;
	}
	
	@Transient
	public Integer getCdmotivodevolucao() {
		return cdmotivodevolucao;
	}

	@Transient
	public String getObservacaorecusa() {
		return observacaorecusa;
	}

	public void setCdmotivodevolucao(Integer cdmotivodevolucao) {
		this.cdmotivodevolucao = cdmotivodevolucao;
	}

	public void setObservacaorecusa(String observacaorecusa) {
		this.observacaorecusa = observacaorecusa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdproducaoagenda == null) ? 0 : cdproducaoagenda.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Producaoagenda other = (Producaoagenda) obj;
		if (cdproducaoagenda == null) {
			if (other.cdproducaoagenda != null)
				return false;
		} else if (!cdproducaoagenda.equals(other.cdproducaoagenda))
			return false;
		return true;
	}
	
	@Transient
	public List<Motivodevolucao> getListaMotivodevolucao() {
		return listaMotivodevolucao;
	}
	
	public void setListaMotivodevolucao(List<Motivodevolucao> listaMotivodevolucao) {
		this.listaMotivodevolucao = listaMotivodevolucao;
	}
	
	@OneToMany(mappedBy="producaoagenda")
	public List<Producaoagendaitemadicional> getListaProducaoagendaitemadicional() {
		return listaProducaoagendaitemadicional;
	}
	
	public void setListaProducaoagendaitemadicional(List<Producaoagendaitemadicional> listaProducaoagendaitemadicional) {
		this.listaProducaoagendaitemadicional = listaProducaoagendaitemadicional;
	}
}