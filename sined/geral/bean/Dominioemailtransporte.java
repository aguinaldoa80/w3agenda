package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
public class Dominioemailtransporte{

	protected Integer cddominioemailtransporte;
	protected String nome;

	@Id
	@DisplayName("Id")
	public Integer getCddominioemailtransporte() {
		return cddominioemailtransporte;
	}

	@Required
	@MaxLength(20)
	@DescriptionProperty
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}

	public void setCddominioemailtransporte(Integer cddominioemailtransporte) {
		this.cddominioemailtransporte = cddominioemailtransporte;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}	
}
