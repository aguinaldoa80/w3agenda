package br.com.linkcom.sined.geral.bean;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.CalculoMarkupItemInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.CalculoTicketMedioPorFornecedor;
import br.com.linkcom.sined.geral.bean.auxiliar.InclusaoLoteVendaInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.ValidateLoteestoqueObrigatorioInterface;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicms;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicmsst;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoicms;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_vendaorcamentomaterial", sequenceName = "sq_vendaorcamentomaterial")
public class Vendaorcamentomaterial implements PopupImpostoVendaInterface, CalculoMarkupItemInterface, ValidateLoteestoqueObrigatorioInterface, InclusaoLoteVendaInterface, CalculoTicketMedioPorFornecedor{
	
	protected Integer cdvendaorcamentomaterial;
	protected Integer ordem;
	protected Vendaorcamento vendaorcamento;
	protected Material material;
	protected Material materialmestre;
	protected Double quantidade;
	protected Double comprimento;
	protected Double comprimentooriginal;
	protected Double largura;
	protected Double altura;
	protected Double fatorconversao;
	protected Double qtdereferencia;
	protected Double preco;
	protected Money desconto;
	protected Unidademedida unidademedida;
	protected Date prazoentrega;
	protected String observacao;
	protected Loteestoque loteestoque;
	protected Double valorcustomaterial;
	protected Double valorvendamaterial;
	protected Double saldo;
	protected Double multiplicador = 1.0;
	protected Boolean opcional;
	private Fornecedor fornecedor;
	protected String identificadorinterno;
	protected Double percentualcomissaoagencia;
	protected Money valorimposto;
	protected Double ipi;
	protected Money valoripi;
	protected Tipocobrancaipi tipocobrancaipi;
	protected Tipocalculo tipocalculoipi;
	protected Money aliquotareaisipi;
	protected Grupotributacao grupotributacao;
	protected Double peso;
	protected Comissionamento comissionamento;
	protected List<Vendaorcamentomaterialseparacao> listaVendaorcamentomaterialseparacao = new ListSet<Vendaorcamentomaterialseparacao>(Vendaorcamentomaterialseparacao.class);
	protected Double percentualdesconto;
	protected Double valorMinimo;	
	protected MaterialFaixaMarkup materialFaixaMarkup;
	protected FaixaMarkupNome faixaMarkupNome;
	protected Money valorSeguro;
	protected Money outrasdespesas;
	
	protected Modalidadebcicms modalidadebcicms;
	protected Modalidadebcicmsst modalidadebcicmsst;
	protected Tipotributacaoicms tipotributacaoicms;
	protected Tipocobrancaicms tipocobrancaicms; 
	protected Money valorbcicms;
	protected Double icms;
	protected Money valoricms;
	protected Double percentualdesoneracaoicms;
	protected Money valordesoneracaoicms;
	protected Boolean abaterdesoneracaoicms;
	
	protected Money valorbcicmsst;
	protected Double icmsst;
	protected Money valoricmsst;
	
	protected Double reducaobcicmsst;
	protected Double margemvaloradicionalicmsst;
	
	protected Money valorbcfcp;
	protected Double fcp;
	protected Money valorfcp;
	protected Money valorbcfcpst;
	protected Double fcpst;
	protected Money valorfcpst;
	
	protected Boolean dadosicmspartilha;
	protected Money valorbcdestinatario;
	protected Money valorbcfcpdestinatario;
	protected Double fcpdestinatario;
	protected Double icmsdestinatario;
	protected Double icmsinterestadual;
	protected Double icmsinterestadualpartilha;
	protected Money valorfcpdestinatario;
	protected Money valoricmsdestinatario;
	protected Money valoricmsremetente;
	protected Double difal;
	protected Money valordifal;
	
	protected Cfop cfop;
	protected Ncmcapitulo ncmcapitulo;
	protected String ncmcompleto;
	
	//TRANSIENTES
	protected Money total;
	protected boolean achou = true;
	protected Double qtdedevolvida;
	protected Double qtdejadevolvida;
	protected Boolean existematerialsimilar;
	protected String historicoTroca;
	protected Integer ordemexibicao;
	protected Material materialAnterior;
	protected String identificadorespecifico;
	protected Double pesoLiquidoVendaOuMaterial;
	protected Produto produtotrans;
	
	protected Double valordescontocustovenda;
	protected Double percentualdescontocustovenda;
	protected Double margemcustovenda;
	protected Money totalproduto;
	protected Money totalImpostos;
	
	protected String descricaomaterialordemretirada;
	protected Integer indice;
	protected Boolean exibirIpiPopup;
	protected Boolean exibirIcmsPopup;
	protected Boolean exibirDifalPopup;
	protected Money valorBruto;
	
	protected String calcularmargempor;
	protected Boolean considerarDesconto;
	protected Boolean considerarValeCompra;
	protected Double valorvalecomprapropocional;
	protected Double descontoProporcional;
	protected Boolean considerarMultiplicadorCusto;
	protected Double valordescontosemvalecompra;
	protected Money totalprodutoReport;
	protected Money totalprodutoDescontoReport;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_vendaorcamentomaterial")
	public Integer getCdvendaorcamentomaterial() {
		return cdvendaorcamentomaterial;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendaorcamento")
	@DisplayName("Or�amento")
	public Vendaorcamento getVendaorcamento() {
		return vendaorcamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	@DisplayName("Produto")
	public Material getMaterial() {
		return material;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialmestre")
	public Material getMaterialmestre() {
		return materialmestre;
	}
	
	@Required
	@DisplayName("Unidade de medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	public Double getQuantidade() {
		return quantidade;
	}
	
	@DisplayName("Pre�o")
	public Double getPreco() {
		return preco;
	}
	
	@DisplayName("Prazo de entrega")
	public Date getPrazoentrega() {
		return prazoentrega;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	public Money getDesconto() {
		return desconto;
	}
	@DisplayName("Unidade de medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdloteestoque")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	
	public Double getMultiplicador() {
		return multiplicador;
	}
	
	public void setMultiplicador(Double multiplicador) {
		this.multiplicador = multiplicador;
	}

	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}

	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
	public void setCdvendaorcamentomaterial(Integer cdvendaorcamentomaterial) {
		this.cdvendaorcamentomaterial = cdvendaorcamentomaterial;
	}
	public void setVendaorcamento(Vendaorcamento vendaorcamento) {
		this.vendaorcamento = vendaorcamento;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialmestre(Material materialmestre) {
		this.materialmestre = materialmestre;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public void setPrazoentrega(Date prazoentrega) {
		this.prazoentrega = prazoentrega;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	// TRANSIENTES
	
	@Transient
	public Money getTotal() {
		return total;
	}
	public void setTotal(Money total) {
		this.total = total;
	}
	
	public Double getFatorconversao() {
		return fatorconversao;
	}
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}

	public void setFatorconversao(Double fatorconversao) {
		this.fatorconversao = fatorconversao;
	}
	
	@Transient
	public boolean isAchou() {
		return achou;
	}
	
	public void setAchou(boolean achou) {
		this.achou = achou;
	}

	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	@Transient
	public Double getQtdedevolvida() {
		return qtdedevolvida;
	}

	public void setQtdedevolvida(Double qtdedevolvida) {
		this.qtdedevolvida = qtdedevolvida;
	}
	@Transient
	public Double getQtdejadevolvida() {
		return qtdejadevolvida;
	}
	public void setQtdejadevolvida(Double qtdejadevolvida) {
		this.qtdejadevolvida = qtdejadevolvida;
	}

	@Transient
	public Produto getProdutotrans() {
		return produtotrans;
	}
	
	public void setProdutotrans(Produto produtotrans) {
		this.produtotrans = produtotrans;
	}

	public Double getComprimento() {
		return comprimento;
	}
	public Double getComprimentooriginal() {
		return comprimentooriginal;
	}
	public Double getLargura() {
		return largura;
	}
	public Double getAltura() {
		return altura;
	}

	public void setComprimentooriginal(Double comprimentooriginal) {
		this.comprimentooriginal = comprimentooriginal;
	}
	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}
	public void setLargura(Double largura) {
		this.largura = largura;
	}
	public void setAltura(Double altura) {
		this.altura = altura;
	}

	@Transient
	public String getDescricaomaterialordemretirada() {
		StringBuilder s = new StringBuilder();
		StringBuilder alturalarguracomprimento = new StringBuilder();
		if(this.getMaterial() != null && this.getMaterial().getNome() != null){
			s.append(this.getMaterial().getNome());
		}
		if(this.getAltura() != null || this.getLargura() != null || this.getComprimentooriginal() != null){
			if(this.getLargura() != null && this.getLargura() > 0){
				alturalarguracomprimento.append(new DecimalFormat("#.#######").format(this.getLargura()));
			}
			if(this.getAltura() != null && this.getAltura() > 0){
				if(!"".equals(alturalarguracomprimento.toString()))
					alturalarguracomprimento.append("x");
				alturalarguracomprimento.append(new DecimalFormat("#.#######").format(this.getAltura()));
			}
			if(this.getComprimentooriginal() != null && this.getComprimentooriginal() > 0){
				if(!"".equals(alturalarguracomprimento.toString()))
					alturalarguracomprimento.append("x");
				alturalarguracomprimento.append(new DecimalFormat("#.#######").format(this.getComprimentooriginal()));
			}
			s.append(" ").append(alturalarguracomprimento.toString());
		}
		if(this.getObservacao() != null && !"".equals(this.getObservacao())){
			s.append("\nObs: ").append(this.getObservacao());
		}
		
		return s.toString();
	}
	
	@Transient
	public Double getValordescontocustovenda() {
		Double valordesconto = 0.0;
		if(this.getMaterial() != null && this.getPreco() != null){
			if(this.getDesconto() != null && this.getConsiderarDesconto() != null && this.getConsiderarDesconto()){
				valordesconto += this.getDesconto().doubleValue();
			}
			if(this.getConsiderarDesconto() != null && this.getConsiderarDesconto()){
				valordesconto += this.getDescontoProporcional();
			}
		}		
		return valordesconto;
	}

	@Transient
	public Double getPercentualdescontocustovenda() {
		Double valordesconto = 0.0;
		if(this.getMaterial() != null && this.getPreco() != null && this.getQuantidade() != null){
			if(this.getValorvendamaterial() != null){
				if(this.getPreco() < this.getValorvendamaterial()){
					valordesconto = (this.getQuantidade()*this.getValorvendamaterial()) - this.getTotalproduto().getValue().doubleValue(); 
				}
			}else if(this.getMaterial().getValorvenda() != null){
				if(this.getPreco() < this.getMaterial().getValorvenda()){
					valordesconto = (this.getQuantidade()*this.getMaterial().getValorvenda()) - this.getTotalproduto().getValue().doubleValue(); 
				}
			}
			if(this.getDesconto() != null){
				valordesconto += this.getDesconto().getValue().doubleValue();
			}
			if(this.getDescontoProporcional() != null){
				valordesconto += this.getDescontoProporcional();
			}
			
			if(valordesconto > 0 && this.getQuantidade() > 0){
				valordesconto = 100 - ((this.getTotalproduto().getValue().doubleValue()-valordesconto)*100)/(this.getQuantidade()*this.getPreco());
			}
		}		
		return valordesconto;
	}

	@Transient
	public Double getPercentualdescontocustovendaReport() {
		if(getConsiderarDesconto() != null && getConsiderarDesconto()){
			return getPercentualdescontocustovenda();
		}
		return 0d;
	}
	
	@Transient
	public Double getMargemcustovenda() {
		Double margem = 100.0;
		if(this.getMaterial() != null && this.getMaterial().getValorcusto() != null && this.getMaterial().getValorcusto() > 0 && this.getPreco() != null){
			margem = (this.getPreco()-this.getMaterial().getValorcusto())*100/this.getMaterial().getValorcusto();
		}
		return margem;
	}

	public void setPercentualdescontocustovenda(Double percentualdescontocustovenda) {
		this.percentualdescontocustovenda = percentualdescontocustovenda;
	}

	public void setMargemcustovenda(Double margemcustovenda) {
		this.margemcustovenda = margemcustovenda;
	}

	@Transient
	public Money getTotalproduto() {
		Double total = 0.0;
		if(this.getPreco() != null){
			total  = this.getPreco() * (this.getQuantidade() != null ? this.getQuantidade() : 1.0);
		}
		return new Money(SinedUtil.round(BigDecimal.valueOf(total), 2).doubleValue());
	}
	
	@Transient
	public Money getTotalprodutoItemSemArredondamento() {
		Double total = 0.0;
		if(this.getPreco() != null){
			total  = this.getPreco() * (this.getQuantidade() != null ? this.getQuantidade() : 1.0);
		}
		return new Money(SinedUtil.round(BigDecimal.valueOf(total), 10).doubleValue());
	}

	@Transient
	public Boolean getExistematerialsimilar() {
		return existematerialsimilar;
	}
	public void setExistematerialsimilar(Boolean existematerialsimilar) {
		this.existematerialsimilar = existematerialsimilar;
	}

	public Double getValorcustomaterial() {
		return valorcustomaterial;
	}
	public void setValorcustomaterial(Double valorcustomaterial) {
		this.valorcustomaterial = valorcustomaterial;
	}

	public Double getValorvendamaterial() {
		return valorvendamaterial;
	}
	public void setValorvendamaterial(Double valorvendamaterial) {
		this.valorvendamaterial = valorvendamaterial;
	}
	
	@Transient
	public Double getFatorconversaoQtdereferencia(){
		Double valor = 1.0;
		if(this.fatorconversao != null)
			valor = (this.fatorconversao / (this.qtdereferencia != null ? this.qtdereferencia : 1.0));
		return valor;
	}
	
	@Transient
	public Double getQtdeMetrocunico(Double qtde){
		Double qtderelatorio = 1.0;
		if(qtde != null){
			qtderelatorio = qtde * 
					(this.getAltura() != null ? this.getAltura() : 1.0) * 
					(this.getLargura() != null ? this.getLargura() : 1.0) *
					(this.getComprimento() != null ? this.getComprimento() : 1.0);
		}
		return qtderelatorio;
	}
	
	@DisplayName("Fornecedor")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public String getIdentificadorinterno() {
		return identificadorinterno;
	}
	public void setIdentificadorinterno(String identificadorinterno) {
		this.identificadorinterno = identificadorinterno;
	}

	@OneToMany(mappedBy="vendaorcamentomaterial", fetch=FetchType.LAZY)
	public List<Vendaorcamentomaterialseparacao> getListaVendaorcamentomaterialseparacao() {
		return listaVendaorcamentomaterialseparacao;
	}
	
	public void setListaVendaorcamentomaterialseparacao(List<Vendaorcamentomaterialseparacao> listaVendaorcamentomaterialseparacao) {
		this.listaVendaorcamentomaterialseparacao = listaVendaorcamentomaterialseparacao;
	}
	
	@DisplayName("(%) Desconto")
	public Double getPercentualdesconto() {
		return percentualdesconto;
	}
	
	public void setPercentualdesconto(Double percentualdesconto) {
		this.percentualdesconto = percentualdesconto;
	}
	
	public Double getValorMinimo() {
		return valorMinimo;
	}
	
	public void setValorMinimo(Double valorMinimo) {
		this.valorMinimo = valorMinimo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialfaixamarkup")
	public MaterialFaixaMarkup getMaterialFaixaMarkup() {
		return materialFaixaMarkup;
	}
	
	public void setMaterialFaixaMarkup(MaterialFaixaMarkup materialFaixaMarkup) {
		this.materialFaixaMarkup = materialFaixaMarkup;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfaixamarkupnome")
	public FaixaMarkupNome getFaixaMarkupNome() {
		return faixaMarkupNome;
	}
	
	public void setFaixaMarkupNome(FaixaMarkupNome faixaMarkupNome) {
		this.faixaMarkupNome = faixaMarkupNome;
	}
	
	@DisplayName("Seguro")
	public Money getValorSeguro() {
		return valorSeguro;
	}
	public void setValorSeguro(Money valorSeguro) {
		this.valorSeguro = valorSeguro;
	}
	
	@DisplayName("Outras despesas")
	public Money getOutrasdespesas() {
		return outrasdespesas;
	}
	public void setOutrasdespesas(Money valorOutrasDespesas) {
		this.outrasdespesas = valorOutrasDespesas;
	}
	
	public Modalidadebcicms getModalidadebcicms() {
		return modalidadebcicms;
	}
	public void setModalidadebcicms(Modalidadebcicms modalidadebcicms) {
		this.modalidadebcicms = modalidadebcicms;
	}
	
	public Modalidadebcicmsst getModalidadebcicmsst() {
		return modalidadebcicmsst;
	}
	public void setModalidadebcicmsst(Modalidadebcicmsst modalidadebcicmsst) {
		this.modalidadebcicmsst = modalidadebcicmsst;
	}
	
	@Transient
	public String getFornecedorStr() {
		if (getFornecedor()!=null && getFornecedor().getNome()!=null){
			return getFornecedor().getNome();
		}else {
			return null;
		}
	}
	
	public Boolean getOpcional() {
		return opcional;
	}
	public void setOpcional(Boolean opcional) {
		this.opcional = opcional;
	}
	
	@Transient
	public String getHistoricoTroca() {
		return historicoTroca;
	}
	@Transient
	public Integer getOrdemexibicao() {
		return ordemexibicao;
	}
	public void setHistoricoTroca(String historicoTroca) {
		this.historicoTroca = historicoTroca;
	}
	public void setOrdemexibicao(Integer ordemexibicao) {
		this.ordemexibicao = ordemexibicao;
	}
	
	@Transient
	public Material getMaterialAnterior() {
		return materialAnterior;
	}

	public void setMaterialAnterior(Material materialAnterior) {
		this.materialAnterior = materialAnterior;
	}
	
	@Transient
	public String getIdentificadorespecifico() {
		return identificadorespecifico;
	}
	
	public void setIdentificadorespecifico(String identificadorespecifico) {
		this.identificadorespecifico = identificadorespecifico;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	@DisplayName("Percentual Comiss�o Ag�ncia")
	public Double getPercentualcomissaoagencia() {
		return percentualcomissaoagencia;
	}

	public void setPercentualcomissaoagencia(Double percentualcomissaoagencia) {
		this.percentualcomissaoagencia = percentualcomissaoagencia;
	}

	public Money getValorimposto() {
		return valorimposto;
	}

	public void setValorimposto(Money valorimposto) {
		this.valorimposto = valorimposto;
	}

	@DisplayName("Valor IPI")
	public Money getValoripi() {
		return valoripi;
	}
	public Double getIpi() {
		return ipi;
	}
	public Tipocobrancaipi getTipocobrancaipi() {
		return tipocobrancaipi;
	}
	public Tipocalculo getTipocalculoipi() {
		return tipocalculoipi;
	}
	public Money getAliquotareaisipi() {
		return aliquotareaisipi;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgrupotributacao")
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcomissionamento")
	public Comissionamento getComissionamento() {
		return comissionamento;
	}
	@DisplayName("Peso l�quido")
	public Double getPeso() {
		return peso;
	}

	public void setIpi(Double ipi) {
		this.ipi = ipi;
	}
	public void setTipocobrancaipi(Tipocobrancaipi tipocobrancaipi) {
		this.tipocobrancaipi = tipocobrancaipi;
	}
	public void setTipocalculoipi(Tipocalculo tipocalculoipi) {
		this.tipocalculoipi = tipocalculoipi;
	}
	public void setAliquotareaisipi(Money aliquotareaisipi) {
		this.aliquotareaisipi = aliquotareaisipi;
	}
	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}
	public void setValoripi(Money valoripi) {
		this.valoripi = valoripi;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	public void setComissionamento(Comissionamento comissionamento) {
		this.comissionamento = comissionamento;
	}

	@Transient
	public Double getPesoVendaOuMaterial(){
		return getPeso() != null ? getPeso() : (getMaterial() != null ? getMaterial().getPeso() : null);
	}
	
	@Transient
	public Double getPesoLiquidoVendaOuMaterial() {
		pesoLiquidoVendaOuMaterial = null;
		Double qtde = this.getQuantidade() == null || this.getQuantidade() == 0 ? 1 : this.getQuantidade();
		if(this.getPesoVendaOuMaterial() != null){
			Double pesoLiquido = this.getPesoVendaOuMaterial();
			if(this.getUnidademedida() != null && this.getMaterial() != null && 
					this.getMaterial().getUnidademedida() != null && 
					!this.getUnidademedida().equals(this.getMaterial().getUnidademedida())){
				pesoLiquido = pesoLiquido / this.getFatorconversaoQtdereferencia();
			}
			pesoLiquidoVendaOuMaterial = pesoLiquido * qtde;
		}
		return (pesoLiquidoVendaOuMaterial!=null)? SinedUtil.roundByParametro(pesoLiquidoVendaOuMaterial, 2): null;
	}
	public void setPesoLiquidoVendaOuMaterial(Double pesoLiquidoVendaOuMaterial) {
		this.pesoLiquidoVendaOuMaterial = pesoLiquidoVendaOuMaterial;
	}

	public Tipotributacaoicms getTipotributacaoicms() {
		return tipotributacaoicms;
	}

	public void setTipotributacaoicms(Tipotributacaoicms tipotributacaoicms) {
		this.tipotributacaoicms = tipotributacaoicms;
	}

	public Tipocobrancaicms getTipocobrancaicms() {
		return tipocobrancaicms;
	}

	public void setTipocobrancaicms(Tipocobrancaicms tipocobrancaicms) {
		this.tipocobrancaicms = tipocobrancaicms;
	}

	public Money getValorbcicms() {
		return valorbcicms;
	}

	public void setValorbcicms(Money valorbcicms) {
		this.valorbcicms = valorbcicms;
	}

	public Double getIcms() {
		return icms;
	}

	public void setIcms(Double icms) {
		this.icms = icms;
	}

	public Money getValoricms() {
		return valoricms;
	}

	public void setValoricms(Money valoricms) {
		this.valoricms = valoricms;
	}
	
	public Money getValordesoneracaoicms() {
		return valordesoneracaoicms;
	}
	
	public void setValordesoneracaoicms(Money valordesoneracaoicms) {
		this.valordesoneracaoicms = valordesoneracaoicms;
	}
	
	public Double getPercentualdesoneracaoicms() {
		return percentualdesoneracaoicms;
	}
	
	public void setPercentualdesoneracaoicms(Double percentualdesoneracaoicms) {
		this.percentualdesoneracaoicms = percentualdesoneracaoicms;
	}
	
	public Boolean getAbaterdesoneracaoicms() {
		return abaterdesoneracaoicms;
	}
	
	public void setAbaterdesoneracaoicms(Boolean abaterdesoneracaoicms) {
		this.abaterdesoneracaoicms = abaterdesoneracaoicms;
	}

	public Money getValorbcicmsst() {
		return valorbcicmsst;
	}

	public void setValorbcicmsst(Money valorbcicmsst) {
		this.valorbcicmsst = valorbcicmsst;
	}

	public Double getIcmsst() {
		return icmsst;
	}

	public void setIcmsst(Double icmsst) {
		this.icmsst = icmsst;
	}

	public Money getValoricmsst() {
		return valoricmsst;
	}

	public void setValoricmsst(Money valoricmsst) {
		this.valoricmsst = valoricmsst;
	}

	public Double getReducaobcicmsst() {
		return reducaobcicmsst;
	}

	public void setReducaobcicmsst(Double reducaobcicmsst) {
		this.reducaobcicmsst = reducaobcicmsst;
	}

	public Double getMargemvaloradicionalicmsst() {
		return margemvaloradicionalicmsst;
	}

	public void setMargemvaloradicionalicmsst(Double margemvaloradicionalicmsst) {
		this.margemvaloradicionalicmsst = margemvaloradicionalicmsst;
	}

	public Money getValorbcfcp() {
		return valorbcfcp;
	}

	public void setValorbcfcp(Money valorbcfcp) {
		this.valorbcfcp = valorbcfcp;
	}

	public Double getFcp() {
		return fcp;
	}

	public void setFcp(Double fcp) {
		this.fcp = fcp;
	}

	public Money getValorfcp() {
		return valorfcp;
	}

	public void setValorfcp(Money valorfcp) {
		this.valorfcp = valorfcp;
	}

	public Money getValorbcfcpst() {
		return valorbcfcpst;
	}

	public void setValorbcfcpst(Money valorbcfcpst) {
		this.valorbcfcpst = valorbcfcpst;
	}

	public Double getFcpst() {
		return fcpst;
	}

	public void setFcpst(Double fcpst) {
		this.fcpst = fcpst;
	}

	public Money getValorfcpst() {
		return valorfcpst;
	}

	public void setValorfcpst(Money valorfcpst) {
		this.valorfcpst = valorfcpst;
	}

	public Boolean getDadosicmspartilha() {
		return dadosicmspartilha;
	}

	public void setDadosicmspartilha(Boolean dadosicmspartilha) {
		this.dadosicmspartilha = dadosicmspartilha;
	}

	public Money getValorbcdestinatario() {
		return valorbcdestinatario;
	}

	public void setValorbcdestinatario(Money valorbcdestinatario) {
		this.valorbcdestinatario = valorbcdestinatario;
	}

	public Money getValorbcfcpdestinatario() {
		return valorbcfcpdestinatario;
	}

	public void setValorbcfcpdestinatario(Money valorbcfcpdestinatario) {
		this.valorbcfcpdestinatario = valorbcfcpdestinatario;
	}

	public Double getFcpdestinatario() {
		return fcpdestinatario;
	}

	public void setFcpdestinatario(Double fcpdestinatario) {
		this.fcpdestinatario = fcpdestinatario;
	}

	public Double getIcmsdestinatario() {
		return icmsdestinatario;
	}

	public void setIcmsdestinatario(Double icmsdestinatario) {
		this.icmsdestinatario = icmsdestinatario;
	}

	public Double getIcmsinterestadual() {
		return icmsinterestadual;
	}

	public void setIcmsinterestadual(Double icmsinterestadual) {
		this.icmsinterestadual = icmsinterestadual;
	}

	public Double getIcmsinterestadualpartilha() {
		return icmsinterestadualpartilha;
	}

	public void setIcmsinterestadualpartilha(Double icmsinterestadualpartilha) {
		this.icmsinterestadualpartilha = icmsinterestadualpartilha;
	}

	public Money getValorfcpdestinatario() {
		return valorfcpdestinatario;
	}

	public void setValorfcpdestinatario(Money valorfcpdestinatario) {
		this.valorfcpdestinatario = valorfcpdestinatario;
	}

	public Money getValoricmsdestinatario() {
		return valoricmsdestinatario;
	}

	public void setValoricmsdestinatario(Money valoricmsdestinatario) {
		this.valoricmsdestinatario = valoricmsdestinatario;
	}

	public Money getValoricmsremetente() {
		return valoricmsremetente;
	}

	public void setValoricmsremetente(Money valoricmsremetente) {
		this.valoricmsremetente = valoricmsremetente;
	}

	public Double getDifal() {
		return difal;
	}

	public void setDifal(Double difal) {
		this.difal = difal;
	}

	public Money getValordifal() {
		return valordifal;
	}

	public void setValordifal(Money valordifal) {
		this.valordifal = valordifal;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcfop")
	@DisplayName("CFOP")
	public Cfop getCfop() {
		return cfop;
	}

	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}

	@DisplayName("NCM")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdncmcapitulo")
	public Ncmcapitulo getNcmcapitulo() {
		return ncmcapitulo;
	}

	public void setNcmcapitulo(Ncmcapitulo ncmcapitulo) {
		this.ncmcapitulo = ncmcapitulo;
	}

	public String getNcmcompleto() {
		return ncmcompleto;
	}

	public void setNcmcompleto(String ncmcompleto) {
		this.ncmcompleto = ncmcompleto;
	}
	
	public void setValordescontocustovenda(Double valordescontocustovenda) {
		this.valordescontocustovenda = valordescontocustovenda;
	}

	public void setTotalproduto(Money totalproduto) {
		this.totalproduto = totalproduto;
	}

	public void setDescricaomaterialordemretirada(
			String descricaomaterialordemretirada) {
		this.descricaomaterialordemretirada = descricaomaterialordemretirada;
	}
	
	@Transient
	@Override
	public Integer getIndice() {
		return indice;
	}
	public void setIndice(Integer indice) {
		this.indice = indice;
	}
	
	@Transient
	public Money getTotalImpostos() {
		Money totalImpostos = new Money()
								.add(getValoripi())
								.add(getValoricms())
								.add(getValoricmsst())
								.add(getValorfcp())
								.add(getValorfcpst())
								.add(getValordifal());
		if(abaterdesoneracaoicms != null && abaterdesoneracaoicms){
			totalImpostos = totalImpostos.subtract(getValordesoneracaoicms());
		}
		return totalImpostos;
	}
	public void setTotalImpostos(Money totalImpostos) {
		this.totalImpostos = totalImpostos;
	}
	@Transient
	@Override
	public Boolean getExibirIpiPopup() {
		return exibirIpiPopup;
	}
	@Override
	public void setExibirIpiPopup(Boolean exibirIpiPopup) {
		this.exibirIpiPopup = exibirIpiPopup;
	}
	@Transient
	@Override
	public Boolean getExibirIcmsPopup() {
		return exibirIcmsPopup;
	}
	@Override
	public void setExibirIcmsPopup(Boolean exibirIcmsPopup) {
		this.exibirIcmsPopup = exibirIcmsPopup;
	}
	@Transient
	@Override
	public Boolean getExibirDifalPopup() {
		return exibirDifalPopup;
	}
	@Override
	public void setExibirDifalPopup(Boolean exibirDifalPopup) {
		this.exibirDifalPopup = exibirDifalPopup;
	}
	@Transient
	@Override
	public Money getValorBruto() {
		if(this.quantidade != null && this.preco != null){
			valorBruto = new Money((this.quantidade * this.preco));
		}

		return valorBruto;
	}

	@Transient
	public String getCalcularmargempor() {
		return calcularmargempor != null ? calcularmargempor : "Custo";
	}
	
	public void setCalcularmargempor(String calcularmargempor) {
		this.calcularmargempor = calcularmargempor;
	}

	@Transient
	public Boolean getConsiderarDesconto() {
		return considerarDesconto;
	}
	
	public void setConsiderarDesconto(Boolean considerarDesconto) {
		this.considerarDesconto = considerarDesconto;
	}
	
	@Transient
	public Boolean getConsiderarValeCompra() {
		return considerarValeCompra;
	}

	public void setConsiderarValeCompra(Boolean considerarValeCompra) {
		this.considerarValeCompra = considerarValeCompra;
	}
	
	@Transient
	public Double getValorvalecomprapropocional() {
		return valorvalecomprapropocional;
	}
	
	public void setValorvalecomprapropocional(Double valorvalecomprapropocional) {
		this.valorvalecomprapropocional = valorvalecomprapropocional;
	}

	@Transient
	public Double getDescontoProporcional() {
		return descontoProporcional;
	}
	
	public void setDescontoProporcional(Double descontoProporcional) {
		this.descontoProporcional = descontoProporcional;
	}
	
	@Transient
	public Boolean getConsiderarMultiplicadorCusto() {
		return considerarMultiplicadorCusto;
	}
	
	public void setConsiderarMultiplicadorCusto(
			Boolean considerarMultiplicadorCusto) {
		this.considerarMultiplicadorCusto = considerarMultiplicadorCusto;
	}
	
	@Transient
	public Double getValorcustoRelatorio(){
		Double valorretorno = 0.0;
		Double valorcusto = 0.0;
		Double multiplicador = 1d;
		
		if (this.getMultiplicador() != null && this.getMultiplicador() > 0) {
			multiplicador = this.getMultiplicador();
		}
		
		if (!Boolean.TRUE.equals(this.getConsiderarMultiplicadorCusto()) || (this.getMaterial() == null || Boolean.TRUE.equals(this.getMaterial().getMetrocubicovalorvenda()))) {
			multiplicador = 1d;
		}
		
		if (this.getValorcustomaterial() != null) {
			valorcusto = this.getValorcustomaterial() * multiplicador; 
			valorretorno = valorcusto;
		} else if(this.material != null && this.material.getValorcusto() != null) {
			valorcusto = this.material.getValorcusto() * multiplicador;
			if (valorcusto != null && this.getFatorconversao() != null && this.getQtdereferencia() != null) {
				valorcusto = valorcusto / getFatorconversaoQtdereferencia();
			}
			valorretorno = valorcusto;
		}
		
//		if(valorcusto != null && this.produtotrans != null){
//			boolean alturadiferente = false;
//			boolean larguradiferente = false;
//			boolean comprimentodiferente = false;
//			
//			/*if((this.produtotrans.getAltura() != null && this.altura == null) || (this.produtotrans.getAltura() == null && this.altura != null)){
//				alturadiferente = true;
//			} else */if(this.produtotrans.getAltura() != null && this.altura != null && !this.produtotrans.getAltura().equals(this.altura*1000)){
//				alturadiferente = true;
//			}
//			
//			/*if((this.produtotrans.getLargura() != null && this.largura == null) || (this.produtotrans.getLargura() == null && this.largura != null)){
//				larguradiferente = true;
//			} else */if(this.produtotrans.getLargura() != null && this.largura != null && !this.produtotrans.getLargura().equals(this.largura*1000)){
//				larguradiferente = true;
//			}
//			
//			/*if((this.produtotrans.getComprimento() != null && this.comprimento == null) || (this.produtotrans.getComprimento() == null && this.comprimento != null)){
//				comprimentodiferente = true;
//			} else */if(this.produtotrans.getComprimento() != null && this.comprimento != null && !this.produtotrans.getComprimento().equals(this.comprimento*1000)){
//				comprimentodiferente = true;
//			}
//			
//			if(alturadiferente || larguradiferente || comprimentodiferente){
//				if(valorcusto != null && this.getFatorconversao() != null && this.getQtdereferencia() != null){
//					valorcusto = valorcusto * getFatorconversaoQtdereferencia();
//				}
//				valorretorno = (this.altura != null && this.altura != 0 ? this.altura : 1.0) *
//						   (this.largura != null && this.largura != 0 ? this.largura : 1.0) *
//						   (this.comprimento != null && this.comprimento != 0 ? this.comprimento : 1.0) * 
//						   (valorcusto != null ? valorcusto : 0.0);
//			}
//		}

		return valorretorno;
	}
	
	@Transient
	public Double getValorvendaRelatorio(){
		Double valorretorno = 0.0;
		Double valorvenda = 0.0;
		Double multiplicador = 1d;
	
		if(this.getMultiplicador() != null && this.getMultiplicador() > 0){
			multiplicador = this.getMultiplicador();
		}
	
		if (this.getMaterial() != null && Boolean.TRUE.equals(this.getMaterial().getMetrocubicovalorvenda())) {
			multiplicador = 1d;
		}
	
		if (this.getValorvendamaterial() != null) {
			valorvenda = this.getValorvendamaterial() * multiplicador; 
			valorretorno = valorvenda;
		} else if(this.material != null && this.material.getValorvenda() != null) {
			valorvenda = this.material.getValorvenda() * multiplicador;
			valorretorno = valorvenda;
		}
	
//		if(valorvenda != null && this.produtotrans != null){
//			boolean alturadiferente = false;
//			boolean larguradiferente = false;
//			boolean comprimentodiferente = false;
//		
//			/*if((this.produtotrans.getAltura() != null && this.altura == null) || (this.produtotrans.getAltura() == null && this.altura != null)){
//				alturadiferente = true;
//			} else */if(this.produtotrans.getAltura() != null && this.altura != null && !this.produtotrans.getAltura().equals(this.altura*1000)){
//				alturadiferente = true;
//			}
//		
//			/*if((this.produtotrans.getLargura() != null && this.largura == null) || (this.produtotrans.getLargura() == null && this.largura != null)){
//				larguradiferente = true;
//			} else */if(this.produtotrans.getLargura() != null && this.largura != null && !this.produtotrans.getLargura().equals(this.largura*1000)){
//				larguradiferente = true;
//			}
//		
//			/*if((this.produtotrans.getComprimento() != null && this.comprimento == null) || (this.produtotrans.getComprimento() == null && this.comprimento != null)){
//				comprimentodiferente = true;
//			} else */if(this.produtotrans.getComprimento() != null && this.comprimento != null && !this.produtotrans.getComprimento().equals(this.comprimento*1000)){
//				comprimentodiferente = true;
//			}
//		
//			if(alturadiferente || larguradiferente || comprimentodiferente){
//				//obs: o valor de venda � salvo convertido
//				if(this.material.unidademedida != null && this.unidademedida != null && 
//						!this.material.unidademedida.equals(this.unidademedida) && 
//						this.fatorconversao != null && this.qtdereferencia != null){
//					valorvenda = valorvenda * getFatorconversaoQtdereferencia(); 
//				}
//				valorretorno = (this.altura != null && this.altura != 0 ? this.altura : 1.0) *
//						   (this.largura != null && this.largura != 0 ? this.largura : 1.0) *
//						   (this.comprimento != null && this.comprimento != 0 ? this.comprimento : 1.0) * 
//						   (valorvenda != null ? valorvenda : 0.0);
//			}
//		}
	
		return valorretorno;
	}
	
	@Transient
	public Double getQuantidadeRelatorio() {
		Double qtd = 1d;
		Double multiplicador = 1d;
		
		if(this.getMultiplicador() != null && this.getMultiplicador() > 0){
			multiplicador = this.getMultiplicador();
		}
		
		//desconsidera o multiplicador caso o metro cubico n�o esteja marcado no cadastro do material
		if (this.getMaterial() != null && !Boolean.TRUE.equals(this.getMaterial().getMetrocubicovalorvenda())) {
			multiplicador = 1d;
		}
		
		if(this.getQuantidade() != null){
			qtd = this.getQuantidade() *  multiplicador;
		}
		
		return qtd;
	}
	
	@Transient
	public Double getPrecoRelatorio(){
		Double precoretorno = 0.0;
		Double multiplicador = 1d;
		
		if(this.getMultiplicador() != null && this.getMultiplicador() > 0){
			multiplicador = this.getMultiplicador();
		}
		
		if (this.getMaterial() != null && Boolean.TRUE.equals(this.getMaterial().getMetrocubicovalorvenda())) {
			multiplicador = 1d;
		}
		
		if(this.getPreco() != null){
			precoretorno = this.getPreco() * multiplicador;
		}
		
		return precoretorno;
	}
	
	@Transient
	public Double getValordescontosemvalecompra() {
		if(this.getDesconto() != null)
			return (valordescontosemvalecompra != null ? valordescontosemvalecompra : 0d) + this.getDesconto().getValue().doubleValue();
		else
			return valordescontosemvalecompra;
	}
	
	public void setValordescontosemvalecompra(Double valordescontosemvalecompra) {
		this.valordescontosemvalecompra = valordescontosemvalecompra;
	}
	
	@Transient
	public Double getValordescontocustovendaitem() {
		Double valordesconto = 0d;
		if(this.getValorvendamaterial() != null){
			if(this.getPreco() < this.getValorvendamaterial()){
				valordesconto = (this.getQuantidade()*this.getValorvendamaterial()) - this.getTotalproduto().getValue().doubleValue(); 
			}
		}else if(this.getMaterial().getValorvenda() != null){
			Double valorvenda = this.getMaterial().getValorvenda();
			if(this.getFatorconversao() != null && this.getQtdereferencia() != null){
				valorvenda = valorvenda / getFatorconversaoQtdereferencia();
			}
			if(this.getPreco() < valorvenda){
				valordesconto = (this.getQuantidade()*valorvenda) - this.getTotalproduto().getValue().doubleValue(); 
			}
		}
		return valordesconto + getValordescontocustovenda();
	}
	
	@Transient
	public Money getTotalprodutoReport() {
		return totalprodutoReport;
	}
	
	public void setTotalprodutoReport(Money totalprodutoReport) {
		this.totalprodutoReport = totalprodutoReport;
	}

	@Transient
	public Money getTotalprodutoDescontoReport() {
		return totalprodutoDescontoReport;
	}
	
	public void setTotalprodutoDescontoReport(Money totalprodutoDescontoReport) {
		this.totalprodutoDescontoReport = totalprodutoDescontoReport;
	}

	@Transient
	public Double getValordescontocustovendaReport() {
		if(getConsiderarDesconto() != null && getConsiderarDesconto()){
			return getValordescontocustovenda();
		}
		return 0d;
	}
}
