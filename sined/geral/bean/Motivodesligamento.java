package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_motivodesligamento", sequenceName = "sq_motivodesligamento")
public class Motivodesligamento {

	protected Integer cdmotivodesligamento;
	protected String nome;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_motivodesligamento")
	public Integer getCdmotivodesligamento() {
		return cdmotivodesligamento;
	}
	public void setCdmotivodesligamento(Integer id) {
		this.cdmotivodesligamento = id;
	}

	
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	
	public void setNome(String nome) {
		this.nome = nome;
	}

}
