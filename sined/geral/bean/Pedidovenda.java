package br.com.linkcom.sined.geral.bean;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Formula;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.controller.Message;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.CalculoMarkupInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.InclusaoLoteVendaInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.PedidoVendaParametersBean;
import br.com.linkcom.sined.geral.bean.auxiliar.PneuItemVendaInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.TotalizacaoImpostoInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.ValoresPassiveisDeRateioInterface;
import br.com.linkcom.sined.geral.bean.enumeration.ContasFinanceiroSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Presencacompradornfe;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoagendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoPendencia;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaopedidosolicitacaocompra;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Sugestaovenda;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;


@Entity
@SequenceGenerator(name = "sq_pedidovenda", sequenceName = "sq_pedidovenda")
@DisplayName("Pedido de venda")
@JoinEmpresa("pedidovenda.empresa")
public class Pedidovenda implements Log, PermissaoProjeto, VendaMaterialProducaoInterface, TotalizacaoImpostoInterface, CalculoMarkupInterface, ValoresPassiveisDeRateioInterface{
	
	protected Integer cdpedidovenda;
	protected String identificador;
	protected String identificacaoexterna;
	protected Pedidovendatipo pedidovendatipo;
	protected Empresa empresa;
	protected Cliente cliente;
	protected Contato contato;
	protected Money valorusadovalecompra;
	protected Money valoraproximadoimposto;
	protected Colaborador colaborador;
	protected Date dtpedidovenda = SinedDateUtils.currentDate();
	protected Endereco endereco;
	protected String observacao;
	protected String observacaointerna;
	protected Presencacompradornfe presencacompradornfe;
	protected Pedidovendasituacao pedidovendasituacao;
	protected Prazopagamento prazopagamento;
	protected Indicecorrecao indicecorrecao;
	protected Conta conta;
	protected Documentotipo documentotipo;
	protected List<Pedidovendapagamento> listaPedidovendapagamento = new ListSet<Pedidovendapagamento>(Pedidovendapagamento.class);
	protected List<PedidoVendaNegociacao> listaPedidoVendaNegociacao = new ListSet<PedidoVendaNegociacao>(PedidoVendaNegociacao.class);
	protected List<Pedidovendamaterial> listaPedidovendamaterial = new ListSet<Pedidovendamaterial>(Pedidovendamaterial.class);
	protected List<Pedidovendahistorico> listaPedidovendahistorico = new ListSet<Pedidovendahistorico>(Pedidovendahistorico.class);
	protected Set<Documentoorigem> listaDocumentoorigem = new ListSet<Documentoorigem>(Documentoorigem.class);
	protected List<Pedidovendavalorcampoextra> listaPedidovendavalorcampoextra = new ListSet<Pedidovendavalorcampoextra>(Pedidovendavalorcampoextra.class);
	protected List<Pedidovendamaterialmestre> listaPedidovendamaterialmestre = new ListSet<Pedidovendamaterialmestre>(Pedidovendamaterialmestre.class);
	protected List<Coleta> listaColeta = new ListSet<Coleta>(Coleta.class);
	protected List<Venda> listaVenda = new ListSet<Venda>(Venda.class);
	protected List<Pedidovendapagamentorepresentacao> listaPedidovendapagamentorepresentacao = new ListSet<Pedidovendapagamentorepresentacao>(Pedidovendapagamentorepresentacao.class);
	protected List<Producaoagenda> listaProduocaoagenda = new ListSet<Producaoagenda>(Producaoagenda.class);
	protected List<PedidoVendaFornecedorTicketMedio> listaPedidoVendaFornecedorTicketMedio = new ListSet<PedidoVendaFornecedorTicketMedio>(PedidoVendaFornecedorTicketMedio.class);
	protected ResponsavelFrete frete;
	protected Fornecedor terceiro;
	protected Money valorfrete;
	protected Projeto projeto;
	protected Boolean ordemcompra;
	protected Localarmazenagem localarmazenagem;
	protected Money desconto;
	protected Double percentualdesconto;
	protected Money valorfinal;
	protected Money valorfinalMaisImpostos;
	protected Boolean restricaocliente;
	protected Money taxapedidovenda;
	protected Vendaorcamento vendaorcamento;
	protected Pedidovenda pedidovendaorigem;
	protected Fornecedor fornecedor;
	protected Boolean integrar;
	protected Boolean origemwebservice;
	protected Cliente clienteindicacao;
	protected Fornecedor parceiro;
	protected Money valordescontorepresentacao;
	protected Money valorbrutorepresentacao;
	protected Prazopagamento prazopagamentorepresentacao;
	protected Documentotipo documentotiporepresentacao;
	protected Conta contaboletorepresentacao;
	protected Fornecedor agencia;
	protected Integer identificadorcarregamento;
	protected String whereInOSV;
	protected Date dtprazoentregamax;
	protected Date prazoentregaMinimo;
	protected Boolean limitecreditoexcedido;
	protected Centrocusto centrocusto;
	protected SituacaoPendencia situacaoPendencia;
	protected Money valorFreteCIF;
	protected String observacaoPedidoVendaTipo;
	protected Boolean origemOtr;
	protected Endereco enderecoFaturamento;
	protected Boolean pedidoEcommerceVisualizado;
	
//	LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
//	Pedido Venda Offline
	protected Timestamp dtcriacaooffline;
	protected Timestamp dtsincronizacaooffline;
	protected String protocolooffline;
	protected Boolean emitirnotacoleta;
	

//	Controle no validateBean
	private String acao;
	private Boolean showCancelLink;
	
	////transientes
	private String codigo;
	private Material material;
	private Integer ordem;
	private Unidademedida unidademedida;
	private Double valor;
	private Double valormaximo;
	private Double valorminimo;
	private Double quantidades;
	private Double pesomedio;
	private Double comprimentos;
	private Double comprimentosoriginal;
	private Double larguras;
	private Double alturas;
	private Double fatorconversaocomprimento;
	private Double qtdereferenciacomprimento; 
	private Double margemarredondamento;
	private Double fatorconversao;
	private Double qtdereferencia;
	private Boolean vendapromocional;
	private Boolean kitflexivel;
	private Money ticketMedioCaculado;
	@SuppressWarnings("unused")
	private Money totalvenda = new Money();
	@SuppressWarnings("unused")
	private Money totalipi = new Money();
	protected Date dtpedidovendaEmissaonota;
	protected Boolean desfazerReserva;
	protected Boolean valorabaixominimoPedido;
	protected Producaoagendasituacao producaoagendasituacao;
	protected String produto_anotacoes;
	protected Integer percentualPedidovenda;
	protected Boolean existeColeta = Boolean.FALSE;
	protected Money saldovalecompra;
	private Pneu pneu;
	private Money ultimoValorVenda;
	private Money valorvalecompranovo;
	private ContasFinanceiroSituacao contasFinanceiroSituacao;
	
	@SuppressWarnings("unused")
	private Money totalparcela = new Money();
	@SuppressWarnings("unused")
	private Money totaljurosparcela;
	protected Date dtprazoentrega;
	protected Loteestoque loteestoque;
	protected Boolean prazomedio;
	protected Integer qtdeParcelas;
	private Money saldofinal = new Money();
	private Boolean existematerialsimilar;
	private Boolean existematerialsimilarColeta;

	protected List<Cliente> listaClienteOffline; //offline
	protected Boolean trocarvendedor;
	protected Colaborador colaboradoraux;
	protected Double qtdestoque;
	protected Double qtdereservada;
	private Double qtdestoqueoriginal;
	protected Double qtddisponivelAcimaestoque;
	protected String msgacimaestoque;
	protected String ids;
	protected String observacaohistorico;
	protected List<Sugestaovenda> listaSugestaovenda;
	protected Double limitepercentualdesconto;
	protected Boolean aprovacaopercentualdesconto;
	protected Situacaopedidosolicitacaocompra situacaopedidosolicitacaocompra;
	protected Boolean considerarvendamultiplos;
	protected Integer qtdeunidade;
	protected String descricaoCombo;
	protected Boolean confirmadoparceladiferenteproduto;
	protected Double valorcustomaterial;
	protected Double valorvendamaterial;
	protected Double totalpesobruto;
	protected Boolean aprovarpedidovenda;
	protected Double multiplicador;
	protected Boolean metrocubicovalorvenda;
	protected Boolean producaosemestoque;
	protected Boolean validaestoque;
	protected String identificacaomaterial;
	protected Material materialcoleta;
	protected String nomeCliente;
	protected Boolean documentoAposEmissaoNota;
	protected Boolean gerarnovasparcelas;
	protected Integer pagamentobanco;
	protected Integer pagamentoagencia;
	protected Integer pagamentoconta;
	protected String pagamentoprimeironumero;
	protected Money pagamentovalor;
	protected Date pagamentoDataUltimoVencimento;
	protected Double totalPesoBruto;
	protected Double totalPesoLiquido;
	protected Boolean isNotVerificarestoquePedidovenda = Boolean.FALSE;
	protected Boolean permitirvendasemestoque = Boolean.FALSE;
	protected Boolean identificadorAutomatico = Boolean.TRUE;
	protected String vendedorprincipal;
	protected Double percentualcomissaoagencia;
	@SuppressWarnings("unused")
	private Money totalparcelarepresentacao = new Money();
	private Money valortotalrepresentacao = new Money();
	protected Money percentualRepresentacao;
	protected List<Fornecedor> listaFornecedor;
	protected Boolean replicarmaterial = Boolean.FALSE;
	protected Boolean replicarPneu = Boolean.FALSE;
	protected Integer qtdereplicar;
	protected Integer qtdeReplicarPneu;
	protected Boolean valorSupeiorLimiteCredito;
	
	protected String whereInConfirmacao;
	protected String erroConfirmacao;
	protected String whereInCancelamento;
	
	protected Boolean registrarpesomedio;
	protected Double percentualdescontoTabelapreco;
	protected Pedidovendamaterial pedidovendamaterialgarantido;
	protected Money descontogarantiareforma;
	protected Garantiareformaitem garantiareformaitem;
	
	private Integer qtdepedidovendapagamento;
	private Integer qtdedocumento;
	
	private Money valorVendasRezalida;
	@SuppressWarnings("unused")
	private Money valorPendentes;
	private Date dtGerarValeCompra;
	private String descricaoValeCompra;
	private Tipooperacao opValeCompra;
	protected Date dtSaidaNotaFiscalProduto;
	protected Boolean fromWebService;
	protected PedidoVendaParametersBean parameters;
	protected List<Message> mensagemAposSalvar;
	protected List<Pneu> listaPneu = new ListSet<Pneu>(Pneu.class);
	protected Pneu pneuInclusao;
	protected List<PneuItemVendaInterface> listaItemAvulso;
	protected Money totalIcms;
	protected Money totalDesoneracaoIcms;
	protected Money totalIcmsSt;
	protected Money totalFcp;
	protected Money totalFcpSt;
	protected Money totalImpostos;
	protected Money totalValorSeguro;
	protected Money totalOutrasDespesas;
	protected Money totalDifal;
	protected Boolean incluirEnderecoFaturamento;
	protected Money valorSeguro;
	protected Money outrasdespesas;
	protected Double qtdeEstoqueMenosReservada;
	protected String menorDtVencimentoLote;
	protected Pedidovendasituacaoecommerce pedidovendasituacaoecommerce;
	protected String nomeSituacaoEcommerce;
	protected Boolean exibeIconeNaoVisualizadoEcommerce;
	
	public Pedidovenda() {
	}
	
	public Pedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}

	@Id
	@DisplayName("C�digo")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pedidovenda")
	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontato")
	@DisplayName("Contato")
	public Contato getContato() {
		return contato;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	@DisplayName("Respons�vel")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	@DisplayName("Tipo de pedido de venda")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendatipo")
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}
	
	@Required
	@DisplayName("Data do pedido de venda")
	public Date getDtpedidovenda() {
		return dtpedidovenda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdenderecoentrega")
	@DisplayName("Local de entrega")
	public Endereco getEndereco() {
		return endereco;
	}
	public Boolean getPedidoEcommerceVisualizado() {
		return pedidoEcommerceVisualizado;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	@DisplayName("Observa��es Internas")
	@MaxLength(1000)
	public String getObservacaointerna() {
		return observacaointerna;
	}
	
	@DisplayName("Situa��o")
	public Pedidovendasituacao getPedidovendasituacao() {
		return pedidovendasituacao;
	}
	
	@Transient
	@DisplayName("Prazo de entrega")
	public Date getDtprazoentrega() {
		return dtprazoentrega;
	}
	@Transient
	@DisplayName("Lote")	
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}

	@OneToMany(mappedBy="pedidovenda")
	@DisplayName("Produtos")
	public List<Pedidovendamaterial> getListaPedidovendamaterial() {
		return listaPedidovendamaterial;
	}
	
	@OneToMany(mappedBy="pedidovenda")
	@DisplayName("Hist�rico")
	public List<Pedidovendahistorico> getListaPedidovendahistorico() {
		return listaPedidovendahistorico;
	}
	
	@OneToMany(mappedBy="pedidovenda")
	public Set<Documentoorigem> getListaDocumentoorigem() {
		return listaDocumentoorigem;
	}
	
	@DisplayName("Campos adicionais")
	@OneToMany(mappedBy="pedidovenda")
	public List<Pedidovendavalorcampoextra> getListaPedidovendavalorcampoextra() {
		return listaPedidovendavalorcampoextra;
	}
	
	public String getIdentificador() {
		return identificador;
	}

	@DisplayName("Ident. externa")
	@MaxLength(200)
	public String getIdentificacaoexterna() {
		return identificacaoexterna;
	}
	
	@OneToMany(mappedBy="pedidovenda")
	@DisplayName("Pagamentos")
	public List<Pedidovendapagamento> getListaPedidovendapagamento() {
		return listaPedidovendapagamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprazopagamento")
	@DisplayName("Condi��o de Pagamento")
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconta")
	@DisplayName("Conta para o boleto")
	public Conta getConta() {
		return conta;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdindicecorrecao")
	@DisplayName("�ndice de corre��o")
	public Indicecorrecao getIndicecorrecao() {
		return indicecorrecao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdformapagamento")
	@DisplayName("Forma de pagamento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	@DisplayName("% Desconto")
	public Double getPercentualdesconto() {
		return percentualdesconto;
	}
	
	@OneToMany(mappedBy="pedidovenda")
	public List<Coleta> getListaColeta() {
		return listaColeta;
	}
	
	@OneToMany(mappedBy="pedidovenda")
	public List<Venda> getListaVenda() {
		return listaVenda;
	}
	
	@DisplayName("Indica��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdclienteindicacao")
	public Cliente getClienteindicacao() {
		return clienteindicacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdparceiro")
	@DisplayName("Parceiro")
	public Fornecedor getParceiro() {
		return parceiro;
	}

	@DisplayName("Origem da opera��o")
	public Presencacompradornfe getPresencacompradornfe() {
		return presencacompradornfe;
	}
	
	public void setPresencacompradornfe(
			Presencacompradornfe presencacompradornfe) {
		this.presencacompradornfe = presencacompradornfe;
	}
	
	public void setClienteindicacao(Cliente clienteindicacao) {
		this.clienteindicacao = clienteindicacao;
	}	
	
	public void setParceiro(Fornecedor parceiro) {
		this.parceiro = parceiro;
	}

	public void setListaColeta(List<Coleta> listaColeta) {
		this.listaColeta = listaColeta;
	}
	
	public void setListaVenda(List<Venda> listaVenda) {
		this.listaVenda = listaVenda;
	}
	
	public void setPercentualdesconto(Double percentualdesconto) {
		this.percentualdesconto = percentualdesconto;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	public Boolean getOrdemcompra() {
		return ordemcompra;
	}
	
	public void setOrdemcompra(Boolean ordemcompra) {
		this.ordemcompra = ordemcompra;
	}
	
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
		
	public void setIndicecorrecao(Indicecorrecao indicecorrecao) {
		this.indicecorrecao = indicecorrecao;
	}
	
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	
	public void setListaPedidovendapagamento(
			List<Pedidovendapagamento> listaPedidovendapagamento) {
		this.listaPedidovendapagamento = listaPedidovendapagamento;
	}
	public void setListaPedidoVendaNegociacao(List<PedidoVendaNegociacao> listaPedidoVendaNegociacao) {
		this.listaPedidoVendaNegociacao = listaPedidoVendaNegociacao;
	}
	public void setIdentificacaoexterna(String identificacaoexterna) {
		this.identificacaoexterna = identificacaoexterna;
	}
	
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
	public void setListaPedidovendahistorico(
			List<Pedidovendahistorico> listaPedidovendahistorico) {
		this.listaPedidovendahistorico = listaPedidovendahistorico;
	}
	
	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void setContato(Contato contato) {
		this.contato = contato;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public void setDtpedidovenda(Date dtpedidovenda) {
		this.dtpedidovenda = dtpedidovenda;
	}
	
	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}
	
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	public void setPedidoEcommerceVisualizado(Boolean pedidoEcommerceVisualizado) {
		this.pedidoEcommerceVisualizado = pedidoEcommerceVisualizado;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public void setObservacaointerna(String observacaointerna) {
		this.observacaointerna = observacaointerna;
	}

	public void setPedidovendasituacao(Pedidovendasituacao pedidovendasituacao) {
		this.pedidovendasituacao = pedidovendasituacao;
	}
	
	
	public void setDtprazoentrega(Date dtprazoentrega) {
		this.dtprazoentrega = dtprazoentrega;
	}
	
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	
	public void setListaPedidovendamaterial(
			List<Pedidovendamaterial> listaPedidovendamaterial) {
		this.listaPedidovendamaterial = listaPedidovendamaterial;
	}
	
	public void setListaDocumentoorigem(
			Set<Documentoorigem> listaDocumentoorigem) {
		this.listaDocumentoorigem = listaDocumentoorigem;
	}
	
	public void setListaPedidovendavalorcampoextra(List<Pedidovendavalorcampoextra> listaPedidovendavalorcampoextra) {
		this.listaPedidovendavalorcampoextra = listaPedidovendavalorcampoextra;
	}

//	LOG
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

//	Pedido Venda Offline
	
	public Timestamp getDtcriacaooffline() {
		return dtcriacaooffline;
	}

	public Timestamp getDtsincronizacaooffline() {
		return dtsincronizacaooffline;
	}
	@DisplayName("Protocolo de Pedido de Venda Offline")
	public String getProtocolooffline() {
		return protocolooffline;
	}
	@DisplayName("Emitir nota de coleta")
	public Boolean getEmitirnotacoleta() {
		return emitirnotacoleta;
	}

	public void setDtcriacaooffline(Timestamp dtcriacaooffline) {
		this.dtcriacaooffline = dtcriacaooffline;
	}

	public void setDtsincronizacaooffline(Timestamp dtsincronizacaooffline) {
		this.dtsincronizacaooffline = dtsincronizacaooffline;
	}

	public void setProtocolooffline(String protocolooffline) {
		this.protocolooffline = protocolooffline;
	}

	public void setEmitirnotacoleta(Boolean emitirnotacoleta) {
		this.emitirnotacoleta = emitirnotacoleta;
	}
	

//	TRANSIENT
	
	@Transient
	@DisplayName("C�digo")
	@MaxLength(14)
	public String getCodigo() {
		return codigo;
	}

	@Transient
	@DisplayName("Material")
	public Material getMaterial() {
		return material;
	}
	
	@Transient
	public Integer getOrdem() {
		return ordem;
	}

	@Transient
	@DisplayName("Valor")
	public Double getValor() {
		return valor;
	}

	@Transient
	@DisplayName("Valor m�ximo")
	public Double getValormaximo() {
		return valormaximo;
	}

	@Transient
	@DisplayName("Valor m�nimo")
	public Double getValorminimo() {
		return valorminimo;
	}

	@Transient
	@DisplayName("Quantidade")
	public Double getQuantidades() {
		return quantidades;
	}
	
	@Transient
	@DisplayName("Peso m�dio")
	public Double getPesomedio() {
		return pesomedio;
	}
	
	@Transient
	public Double getComprimentos() {
		return comprimentos;
	}

	@Transient
	@DisplayName("Total")
	public Money getTotalvenda() {
		return getTotalvenda(false);
	}
	
	@Transient
	@DisplayName("Total")
	public Money getTotalvenda(Boolean truncar) {
		Money valor = new Money(0);
		if(this.listaPedidovendamaterial != null && !this.listaPedidovendamaterial.isEmpty()){
			for (Pedidovendamaterial vendamaterial : this.listaPedidovendamaterial) {
				Double qtde = vendamaterial.getQuantidade() == null || vendamaterial.getQuantidade() == 0 ? 1 : vendamaterial.getQuantidade();
				Double multiplicador = vendamaterial.getMultiplicador() == null || vendamaterial.getMultiplicador() == 0 ? 1 : vendamaterial.getMultiplicador();
				if(vendamaterial.getPreco() != null){
					Double valorTruncado = 0d;
					if(truncar != null && truncar){
						valorTruncado = SinedUtil.roundFloor(BigDecimal.valueOf(vendamaterial.getPreco().doubleValue() * (qtde * multiplicador)), 2).doubleValue();
					}else {
                        valorTruncado = SinedUtil.round(BigDecimal.valueOf(vendamaterial.getPreco().doubleValue() * qtde * multiplicador), 2).doubleValue();
					}
					valor = valor.add(new Money(valorTruncado));
				}
				valor = valor.subtract(vendamaterial.getDesconto())
							.add(vendamaterial.getValorSeguro())
							.add(vendamaterial.getOutrasdespesas());
			}
		}
		
		valor = valor.add(this.valorfrete != null ? this.valorfrete : new Money(0.0));
		valor = valor.add(this.taxapedidovenda != null ? this.taxapedidovenda : new Money(0.0));
		valor = valor.subtract(this.desconto != null ? this.desconto : new Money(0.0));
		valor = valor.subtract(this.valorusadovalecompra != null ? this.valorusadovalecompra : new Money(0.0));
		return valor;
	}
	
	@Transient
	public Money getTotalvendaSemAredondamento() {
		Money valor = new Money(0);
		if(this.listaPedidovendamaterial != null && !this.listaPedidovendamaterial.isEmpty()){
			for (Pedidovendamaterial vendamaterial : this.listaPedidovendamaterial) {
				Double qtde = vendamaterial.getQuantidade() == null || vendamaterial.getQuantidade() == 0 ? 1 : vendamaterial.getQuantidade();
				Double multiplicador = vendamaterial.getMultiplicador() == null || vendamaterial.getMultiplicador() == 0 ? 1 : vendamaterial.getMultiplicador();
				if(vendamaterial.getPreco() != null){
					Double valorTotal = vendamaterial.getPreco().doubleValue() * (qtde * multiplicador);
					valor = valor.add(new Money(valorTotal));
				}
				valor = valor.subtract(vendamaterial.getDesconto())
							.add(vendamaterial.getOutrasdespesas())
							.add(vendamaterial.getValorSeguro());
			}
		}
		
		valor = valor.add(this.valorfrete != null ? this.valorfrete : new Money(0.0));
		valor = valor.add(this.taxapedidovenda != null ? this.taxapedidovenda : new Money(0.0));
		valor = valor.subtract(this.desconto != null ? this.desconto : new Money(0.0));
		valor = valor.subtract(this.valorusadovalecompra != null ? this.valorusadovalecompra : new Money(0.0));
		return valor;
	}
	
	@Transient
	public Money getTotalvendaMaisImpostos() {
		Money valor = getTotalvenda();
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterial)){
			for (Pedidovendamaterial item : this.listaPedidovendamaterial) {
				valor = valor.add(item.getValoripi())
							.add(item.getValoricmsst())
							.add(item.getValorfcpst());
				
				if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
					valor = valor.subtract(item.getValordesoneracaoicms());
				}
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterialmestre)){
			for (Pedidovendamaterialmestre item : this.listaPedidovendamaterialmestre) {
				valor = valor.add(item.getValoripi())
						.add(item.getValoricmsst())
						.add(item.getValorfcpst());
				
				if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
					valor = valor.subtract(item.getValordesoneracaoicms());
				}
			}
		}
		return valor;
	}
	
	@Transient
	@DisplayName("Total")
	public Money getTotalvendaSemValecompra() {
		Money valor = new Money(0);
		if(this.listaPedidovendamaterial != null && !this.listaPedidovendamaterial.isEmpty()){
			for (Pedidovendamaterial vendamaterial : this.listaPedidovendamaterial) {
				Double qtde = vendamaterial.getQuantidade() == null || vendamaterial.getQuantidade() == 0 ? 1 : vendamaterial.getQuantidade();
				Double multiplicador = vendamaterial.getMultiplicador() == null || vendamaterial.getMultiplicador() == 0 ? 1 : vendamaterial.getMultiplicador();
				
				valor = valor.add(new Money(vendamaterial.getPreco().doubleValue() * qtde * multiplicador))
							.subtract(vendamaterial.getDesconto())
							.add(vendamaterial.getOutrasdespesas())
							.add(vendamaterial.getValorSeguro());
			}
		}
		
		valor = valor.add(this.valorfrete != null ? this.valorfrete : new Money(0.0));
		valor = valor.add(this.taxapedidovenda != null ? this.taxapedidovenda : new Money(0.0));
		valor = valor.subtract(this.desconto != null ? this.desconto : new Money(0.0));
		return valor;
	}

	@Transient
	public Money getTotalparcela() {
		Money valor = new Money(0);
		if(this.listaPedidovendapagamento != null && !this.listaPedidovendapagamento.isEmpty()){
			for (Pedidovendapagamento pagamento : this.listaPedidovendapagamento) {
				valor = valor.add(pagamento.getValororiginal());
			}
		}		
		return valor;		
	}
	public void setTotalparcela(Money totalparcela) {
		this.totalparcela = totalparcela;
	}
	@Transient
	public Money getTotaljurosparcela() {
		Money valor = new Money(0);
		if(this.listaPedidovendapagamento != null && !this.listaPedidovendapagamento.isEmpty()){
			for (Pedidovendapagamento pagamento : this.listaPedidovendapagamento) {
				if(pagamento.getValorjuros() != null){
					valor = valor.add(pagamento.getValorjuros());
				}
			}
		}		
		return valor;		
	}
	public void setTotaljurosparcela(Money totaljurosparcela) {
		this.totaljurosparcela = totaljurosparcela;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	
	public void setValor(Double valor) {
		this.valor = valor;
	}

	public void setValormaximo(Double valormaximo) {
		this.valormaximo = valormaximo;
	}

	public void setValorminimo(Double valorminimo) {
		this.valorminimo = valorminimo;
	}

	public void setQuantidades(Double quantidades) {
		this.quantidades = quantidades;
	}
	
	public void setPesomedio(Double pesomedio) {
		this.pesomedio = pesomedio;
	}
	
	public void setComprimentos(Double comprimentos) {
		this.comprimentos = comprimentos;
	}

	public void setTotalvenda(Money totalvenda) {
		this.totalvenda = totalvenda;
	}
	
	@Transient
	public String getItensPedidoVendaString(){
		List<Pedidovendamaterial> listavendamaterial2 = getListaPedidovendamaterial();
		if (listavendamaterial2 != null && listavendamaterial2.size() > 5)
			return "Diversos";
		String string = "";
		if(listavendamaterial2 != null && listavendamaterial2.size()>0){
			for (Pedidovendamaterial pedidovendamaterial : listavendamaterial2) {
				string += (pedidovendamaterial.getQuantidade()!=null ? new DecimalFormat("#,##0.#########").format(pedidovendamaterial.getQuantidade()) : 0) + " - " + pedidovendamaterial.getMaterial().getNome() + " -  R$ " + SinedUtil.descriptionDecimal(pedidovendamaterial.getPreco(), true) + "<BR>";
			}
		}
		return string;
	}
	
	@DisplayName("Considerar Prazo m�dio")
	public Boolean getPrazomedio() {
		return prazomedio;
	}
	public void setPrazomedio(Boolean prazomedio) {
		this.prazomedio = prazomedio;
	}
	public Integer getQtdeParcelas() {
		return qtdeParcelas;
	}
	public void setQtdeParcelas(Integer qtdeParcelas) {
		this.qtdeParcelas = qtdeParcelas;
	}

	public Money getSaldofinal() {
		return saldofinal;
	}

	public void setSaldofinal(Money saldofinal) {
		this.saldofinal = saldofinal;
	}

	@Transient
	@DisplayName("Unidade medidas")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}

	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "frete")
	public ResponsavelFrete getFrete() {
		return frete;
	}	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdterceiro")
	public Fornecedor getTerceiro() {
		return terceiro;
	}	
	@DisplayName("Valor do frete")
	public Money getValorfrete() {
		return valorfrete;
	}
	

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendaorcamento")
	public Vendaorcamento getVendaorcamento() {
		return vendaorcamento;
	}
	
	public void setVendaorcamento(Vendaorcamento vendaorcamento) {
		this.vendaorcamento = vendaorcamento;
	}

	@DisplayName("Ag�ncia")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdagencia")
	public Fornecedor getAgencia() {
		return agencia;
	}	
	
	public void setAgencia(Fornecedor agencia) {
		this.agencia = agencia;
	}
	
	@Transient
	public List<Cliente> getListaClienteOffline() {
		return listaClienteOffline;
	}

	public void setListaClienteOffline(List<Cliente> listaClienteOffline) {
		this.listaClienteOffline = listaClienteOffline;
	}

	public void setFrete(ResponsavelFrete frete) {
		this.frete = frete;
	}
	public void setTerceiro(Fornecedor terceiro) {
		this.terceiro = terceiro;
	}
	public void setValorfrete(Money valorfrete) {
		this.valorfrete = valorfrete;
	}
	
	public String subQueryProjeto() {
		return "otimizacao_pedidovenda_projeto";
//		return "select pedidovendasubQueryProjeto.cdpedidovenda " +
//				"from Pedidovenda pedidovendasubQueryProjeto " +
//				"left outer join pedidovendasubQueryProjeto.projeto projetosubQueryProjeto " +
//				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ") ";
	}	

	public boolean useFunctionProjeto() {
		return true;
	}
	
	@Transient
	public Boolean getTrocarvendedor() {
		return trocarvendedor;
	}
	public void setTrocarvendedor(Boolean trocarvendedor) {
		this.trocarvendedor = trocarvendedor;
	}
	@Transient
	public Colaborador getColaboradoraux() {
		return colaboradoraux;
	}
	public void setColaboradoraux(Colaborador colaboradoraux) {
		this.colaboradoraux = colaboradoraux;
	}
	@DisplayName("Local")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}

	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}

	@Transient
	@DisplayName("Qtde Dispon�vel")
	public Double getQtdestoque() {
		return qtdestoque;
	}

	public void setQtdestoque(Double qtdestoque) {
		this.qtdestoque = qtdestoque;
	}
	
	@DisplayName("Desconto")
	public Money getDesconto() {
		return desconto;
	}
	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
	@Transient
	@DisplayName("Valor Final")
	public Money getValorfinal() {
		return valorfinal;
	}

	public void setValorfinal(Money valorfinal) {
		this.valorfinal = valorfinal;
	}
	@Transient
	public Boolean getVendapromocional() {
		return vendapromocional;
	}

	public void setVendapromocional(Boolean vendapromocional) {
		this.vendapromocional = vendapromocional;
	}
	
	@Transient
	public Boolean getKitflexivel() {
		return kitflexivel;
	}

	public void setKitflexivel(Boolean kitflexivel) {
		this.kitflexivel = kitflexivel;
	}

	//campo para saber se o pedido est� aguardando aprova��o por causa de libera��o do cliente
	public Boolean getRestricaocliente() {
		return restricaocliente;
	}

	public void setRestricaocliente(Boolean restricaocliente) {
		this.restricaocliente = restricaocliente;
	}
	
	@Transient
	@DisplayName("Qtde principal dispon�vel")
	public Double getQtdestoqueoriginal() {
		return qtdestoqueoriginal;
	}

	public void setQtdestoqueoriginal(Double qtdestoqueoriginal) {
		this.qtdestoqueoriginal = qtdestoqueoriginal;
	}
	@Transient
	public String getIds() {
		return ids;
	}
	@Transient
	public String getObservacaohistorico() {
		return observacaohistorico;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public void setObservacaohistorico(String observacaohistorico) {
		this.observacaohistorico = observacaohistorico;
	}

	@DisplayName("Taxa de Pedido de Venda")
	public Money getTaxapedidovenda() {
		return taxapedidovenda;
	}

	public void setTaxapedidovenda(Money taxapedidovenda) {
		this.taxapedidovenda = taxapedidovenda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	
	@Transient
	public List<Sugestaovenda> getListaSugestaovenda() {
		return listaSugestaovenda;
	}
	
	public void setListaSugestaovenda(List<Sugestaovenda> listaSugestaovenda) {
		this.listaSugestaovenda = listaSugestaovenda;
	}
	
	@Transient
	public Double getLimitepercentualdesconto() {
		return limitepercentualdesconto;
	}
	
	public void setLimitepercentualdesconto(Double limitepercentualdesconto) {
		this.limitepercentualdesconto = limitepercentualdesconto;
	}
	
	@Transient
	public Boolean getAprovacaopercentualdesconto() {
		return aprovacaopercentualdesconto;
	}
	public void setAprovacaopercentualdesconto(
			Boolean aprovacaopercentualdesconto) {
		this.aprovacaopercentualdesconto = aprovacaopercentualdesconto;
	}

	@Transient
	public Boolean getAprovarpedidovenda() {
		return aprovarpedidovenda;
	}
	public void setAprovarpedidovenda(Boolean aprovarpedidovenda) {
		this.aprovarpedidovenda = aprovarpedidovenda;
	}

	@Transient
	public Situacaopedidosolicitacaocompra getSituacaopedidosolicitacaocompra() {
		return situacaopedidosolicitacaocompra;
	}
	public void setSituacaopedidosolicitacaocompra(Situacaopedidosolicitacaocompra situacaopedidosolicitacaocompra) {
		this.situacaopedidosolicitacaocompra = situacaopedidosolicitacaocompra;
	}
	
	public Money getValorusadovalecompra() {
		return valorusadovalecompra;
	}
	public void setValorusadovalecompra(Money valorusadovalecompra) {
		this.valorusadovalecompra = valorusadovalecompra;
	}
	
	@DisplayName("Valor aproximado dos impostos (IBPT)")
	public Money getValoraproximadoimposto() {
		return valoraproximadoimposto;
	}
	public void setValoraproximadoimposto(Money valoraproximadoimposto) {
		this.valoraproximadoimposto = valoraproximadoimposto;
	}

	@DisplayName("Qtde Reservada")
	@Transient
	public Double getQtdereservada() {
		return qtdereservada;
	}
	public void setQtdereservada(Double qtdereservada) {
		this.qtdereservada = qtdereservada;
	}
	
	@Transient
	public Double getFatorconversao() {
		return fatorconversao;
	}
	public void setFatorconversao(Double fatorconversao) {
		this.fatorconversao = fatorconversao;
	}
	
	
	@Transient
	public Boolean getConsiderarvendamultiplos() {
		return considerarvendamultiplos;
	}
	@Transient
	public Integer getQtdeunidade() {
		return qtdeunidade;
	}
	public void setConsiderarvendamultiplos(Boolean considerarvendamultiplos) {
		this.considerarvendamultiplos = considerarvendamultiplos;
	}
	public void setQtdeunidade(Integer qtdeunidade) {
		this.qtdeunidade = qtdeunidade;
	}

	@DisplayName("Pedido de venda Origem")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendaorigem")
	public Pedidovenda getPedidovendaorigem() {
		return pedidovendaorigem;
	}
	public void setPedidovendaorigem(Pedidovenda pedidovendaorigem) {
		this.pedidovendaorigem = pedidovendaorigem;
	}
	
	@Transient
	public String getDescricaoCombo() {
		StringBuilder s = new StringBuilder();
		if(this.cdpedidovenda != null){
			s.append(this.cdpedidovenda);
		}
		if(this.cliente != null && this.cliente.getNome() != null){
			if(!"".equals(s.toString())) s.append(" - ");
			s.append(this.cliente.getNome());
		}
		return s.toString();
	}
	public void setDescricaoCombo(String descricaoCombo) {
		this.descricaoCombo = descricaoCombo;
	}

	@Transient
	public Boolean getConfirmadoparceladiferenteproduto() {
		return confirmadoparceladiferenteproduto;
	}
	public void setConfirmadoparceladiferenteproduto(Boolean confirmadoparceladiferenteproduto) {
		this.confirmadoparceladiferenteproduto = confirmadoparceladiferenteproduto;
	}
	@Transient
	public Double getValorcustomaterial() {
		return valorcustomaterial;
	}
	public void setValorcustomaterial(Double valorcustomaterial) {
		this.valorcustomaterial = valorcustomaterial;
	}
	
	@Transient
	public Double getTotalpesobruto() {
		Double totalpeso = 0.0;
		if(this.listaPedidovendamaterial != null && !this.listaPedidovendamaterial.isEmpty()){
			for (Pedidovendamaterial pvm : listaPedidovendamaterial) {
				if(pvm.getMaterial() != null && pvm.getMaterial().getPesobruto() != null)
					totalpeso +=  pvm.getMaterial().getPesobruto();
			}
		}
		return SinedUtil.roundByParametro(totalpeso, 2);
	}
	public void setTotalpesobruto(Double totalpesobruto) {
		this.totalpesobruto = totalpesobruto;
	}

	@Transient
	public Double getValorvendamaterial() {
		return valorvendamaterial;
	}
	public void setValorvendamaterial(Double valorvendamaterial) {
		this.valorvendamaterial = valorvendamaterial;
	}

	@Transient
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
	
	@Transient
	public String getDtpedidovendastr(){
		String datastr = "";
		if(this.dtpedidovenda != null) datastr = new SimpleDateFormat("dd/MM/yyyy").format(this.dtpedidovenda);
		return datastr;
	}

	@Transient
	public Double getComprimentosoriginal() {
		return comprimentosoriginal;
	}
	@Transient
	public Double getLarguras() {
		return larguras;
	}
	@Transient
	public Double getAlturas() {
		return alturas;
	}
	@Transient
	public Double getFatorconversaocomprimento() {
		return fatorconversaocomprimento;
	}
	@Transient
	public Double getQtdereferenciacomprimento() {
		return qtdereferenciacomprimento;
	}
	@Transient
	public Double getMargemarredondamento() {
		return margemarredondamento;
	}

	public void setComprimentosoriginal(Double comprimentosoriginal) {
		this.comprimentosoriginal = comprimentosoriginal;
	}
	public void setLarguras(Double larguras) {
		this.larguras = larguras;
	}
	public void setAlturas(Double alturas) {
		this.alturas = alturas;
	}
	public void setFatorconversaocomprimento(Double fatorconversaocomprimento) {
		this.fatorconversaocomprimento = fatorconversaocomprimento;
	}
	public void setQtdereferenciacomprimento(Double qtdereferenciacomprimento) {
		this.qtdereferenciacomprimento = qtdereferenciacomprimento;
	}
	public void setMargemarredondamento(Double margemarredondamento) {
		this.margemarredondamento = margemarredondamento;
	}

	@Transient
	public Double getMultiplicador() {
		return multiplicador;
	}
	
	public void setMultiplicador(Double multiplicador) {
		this.multiplicador = multiplicador;
	}
	
	@Transient
	public Boolean getMetrocubicovalorvenda() {
		return metrocubicovalorvenda;
	}
	
	public void setMetrocubicovalorvenda(Boolean metrocubicovalorvenda) {
		this.metrocubicovalorvenda = metrocubicovalorvenda;
	}
	
	@Transient
	public Date getDataentregaTrans(){
		java.sql.Date date = null;
		
		if(this.getListaPedidovendamaterial() != null && !this.getListaPedidovendamaterial().isEmpty()){
			for(Pedidovendamaterial pedidovendamaterial : this.getListaPedidovendamaterial()){
				if(pedidovendamaterial.getDtprazoentrega() != null){
					if(date == null){
						date = new java.sql.Date(pedidovendamaterial.getDtprazoentrega().getTime());
					}else if(SinedDateUtils.beforeIgnoreHour(date, (java.sql.Date)  new java.sql.Date(pedidovendamaterial.getDtprazoentrega().getTime()))){
						date = new java.sql.Date(pedidovendamaterial.getDtprazoentrega().getTime());
					}
				}
			}
		}
		
		return date;
	}
	
	@Transient
	public Date getMaiorDataentregaTrans(){
		java.sql.Date date = null;
		
		if(this.getListaPedidovendamaterial() != null && !this.getListaPedidovendamaterial().isEmpty()){
			for(Pedidovendamaterial pedidovendamaterial : this.getListaPedidovendamaterial()){
				if(pedidovendamaterial.getDtprazoentrega() != null){
					if(date == null){
						date = new java.sql.Date(pedidovendamaterial.getDtprazoentrega().getTime());
					}else if(SinedDateUtils.afterIgnoreHour(date, (java.sql.Date)  new java.sql.Date(pedidovendamaterial.getDtprazoentrega().getTime()))){
						date = new java.sql.Date(pedidovendamaterial.getDtprazoentrega().getTime());
					}
				}
			}
		}
		
		return date;
	}

	@Transient
	public String getAcao() {
		return acao;
	}
	@Transient
	public Boolean getShowCancelLink() {
		return showCancelLink;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}
	public void setShowCancelLink(Boolean showCancelLink) {
		this.showCancelLink = showCancelLink;
	}

	@Transient
	public Date getDtpedidovendaEmissaonota() {
		return dtpedidovendaEmissaonota;
	}
	public void setDtpedidovendaEmissaonota(Date dtpedidovendaEmissaonota) {
		this.dtpedidovendaEmissaonota = dtpedidovendaEmissaonota;
	}
	
	@Transient
	public Date getDtSaidaNotaFiscalProduto() {
		return dtSaidaNotaFiscalProduto;
	}
	public void setDtSaidaNotaFiscalProduto(Date dtSaidaNotaFiscalProduto) {
		this.dtSaidaNotaFiscalProduto = dtSaidaNotaFiscalProduto;
	}
	
	@Transient
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public Boolean getIntegrar() {
		return integrar;
	}
	public void setIntegrar(Boolean integrar) {
		this.integrar = integrar;
	}

	@Transient
	public Boolean getDesfazerReserva() {
		return desfazerReserva;
	}
	public void setDesfazerReserva(Boolean desfazerReserva) {
		this.desfazerReserva = desfazerReserva;
	}

	@Transient
	public Boolean getValorabaixominimoPedido() {
		return valorabaixominimoPedido;
	}
	public void setValorabaixominimoPedido(Boolean valorabaixominimoPedido) {
		this.valorabaixominimoPedido = valorabaixominimoPedido;
	}
	
	@Transient
	public String getIdentificacaomaterial() {
		return identificacaomaterial;
	}
	
	public void setIdentificacaomaterial(String identificacaomaterial) {
		this.identificacaomaterial = identificacaomaterial;
	}
	
	@Transient
	public String getCodigodtpedidovenda(){
		String s = "";
		if(getCdpedidovenda() != null){
			s = getCdpedidovenda().toString();
		}
		if(getDtpedidovenda() != null){
			if(s != null && !"".equals(s)) s += " - ";
			s += new SimpleDateFormat("dd/MM/yyyy").format(getDtpedidovenda());
		}
		return s;
	}

	@Transient
	public Producaoagendasituacao getProducaoagendasituacao() {
		return producaoagendasituacao;
	}

	public void setProducaoagendasituacao(Producaoagendasituacao producaoagendasituacao) {
		this.producaoagendasituacao = producaoagendasituacao;
	}
	
	@Transient
	public Material getMaterialcoleta() {
		return materialcoleta;
	}
	
	public void setMaterialcoleta(Material materialcoleta) {
		this.materialcoleta = materialcoleta;
	}
	
	@Transient
	@DisplayName("Anota��es")
	public String getProduto_anotacoes() {
		return produto_anotacoes;
	}

	public void setProduto_anotacoes(String produtoAnotacoes) {
		produto_anotacoes = produtoAnotacoes;
	}
	@OneToMany(mappedBy="pedidovenda")
	@DisplayName("Material Mestre")	
	public List<Pedidovendamaterialmestre> getListaPedidovendamaterialmestre() {
		return listaPedidovendamaterialmestre;
	}

	public void setListaPedidovendamaterialmestre(
			List<Pedidovendamaterialmestre> listaPedidovendamaterialmestre) {
		this.listaPedidovendamaterialmestre = listaPedidovendamaterialmestre;
	}
	@Transient
	public Boolean getProducaosemestoque() {
		return producaosemestoque;
	}
	@Transient
	public Boolean getValidaestoque() {
		return validaestoque;
	}

	public void setValidaestoque(Boolean validaestoque) {
		this.validaestoque = validaestoque;
	}
	public void setProducaosemestoque(Boolean producaosemestoque) {
		this.producaosemestoque = producaosemestoque;
	}
	
	@Transient
	public Integer getPercentualPedidovenda() {
		return percentualPedidovenda;
	}

	public void setPercentualPedidovenda(Integer percentualPedidovenda) {
		this.percentualPedidovenda = percentualPedidovenda;
	}
	
	
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	} 
	
	@Transient
	public String getNomeCliente() {
		if(getCliente() != null && getCliente().getNome() != null){
			nomeCliente = getCliente().getNome();
		}
		return nomeCliente;
	}
	
	@Transient
	public String getPedidoVendaAutocompleteForComissaoVendedor(){
		StringBuilder s = new StringBuilder();
		
		if(this.getCdpedidovenda() != null){
			s.append(this.getCdpedidovenda());
			
			if(this.getIdentificador() != null){					
				s.append(" - ").append(this.getIdentificador());
			}
			
			if(this.getNomeCliente() != null){
				s.append(" - ").append(this.getNomeCliente());
			}
		}
		
		return s.toString()!=null && !s.toString().isEmpty() ? s.toString() : "";
	}

	@Transient
	public Boolean getDocumentoAposEmissaoNota() {
		return documentoAposEmissaoNota;
	}

	public void setDocumentoAposEmissaoNota(Boolean documentoAposEmissaoNota) {
		this.documentoAposEmissaoNota = documentoAposEmissaoNota;
	}
	
	@Transient
	@DisplayName("Gerar novas parcelas")
	public Boolean getGerarnovasparcelas() {
		return gerarnovasparcelas;
	}

	public void setGerarnovasparcelas(Boolean gerarnovasparcelas) {
		this.gerarnovasparcelas = gerarnovasparcelas;
	}

	@Transient
	@DisplayName("Banco")
	public Integer getPagamentobanco() {
		return pagamentobanco;
	}
	@Transient
	@DisplayName("Ag�ncia")
	public Integer getPagamentoagencia() {
		return pagamentoagencia;
	}
	@Transient
	@DisplayName("Conta")
	public Integer getPagamentoconta() {
		return pagamentoconta;
	}
	@DisplayName("Situa��o Pendencia")
	public SituacaoPendencia getSituacaoPendencia() {
		return situacaoPendencia;
	}
	
	@DisplayName("Valor do frete CIF")
	public Money getValorFreteCIF() {
		return valorFreteCIF;
	}
	
	@DisplayName("Observa��o Espec�fica do Pedido de Venda")
	public String getObservacaoPedidoVendaTipo() {
		return observacaoPedidoVendaTipo;
	}
	
	@Transient
	@DisplayName("Primeiro N�mero")
	public String getPagamentoprimeironumero() {
		return pagamentoprimeironumero;
	}
	@Transient
	@DisplayName("Valor p/ Gerar Parcelas")
	public Money getPagamentovalor() {
		return pagamentovalor;
	}
	@Transient
	public Date getPagamentoDataUltimoVencimento() {
		return pagamentoDataUltimoVencimento;
	}

	public void setPagamentobanco(Integer pagamentobanco) {
		this.pagamentobanco = pagamentobanco;
	}
	public void setPagamentoagencia(Integer pagamentoagencia) {
		this.pagamentoagencia = pagamentoagencia;
	}
	public void setPagamentoconta(Integer pagamentoconta) {
		this.pagamentoconta = pagamentoconta;
	}
	public void setPagamentoprimeironumero(String pagamentoprimeironumero) {
		this.pagamentoprimeironumero = pagamentoprimeironumero;
	}
	public void setPagamentovalor(Money pagamentovalor) {
		this.pagamentovalor = pagamentovalor;
	}
	public void setPagamentoDataUltimoVencimento(Date pagamentoDataUltimoVencimento) {
		this.pagamentoDataUltimoVencimento = pagamentoDataUltimoVencimento;
	}
	
	@Transient
	public Boolean getExistematerialsimilar() {
		return existematerialsimilar;
	}
	@Transient
	public Boolean getExistematerialsimilarColeta() {
		return existematerialsimilarColeta;
	}
	public void setExistematerialsimilar(Boolean existematerialsimilar) {
		this.existematerialsimilar = existematerialsimilar;
	}	
	public void setExistematerialsimilarColeta(
			Boolean existematerialsimilarColeta) {
		this.existematerialsimilarColeta = existematerialsimilarColeta;
	}
	
	@Transient
	@DisplayName("Peso Bruto Total")
	public Double getTotalPesoBruto() {
		Double totalPesoBruto = 0.0;
		if(SinedUtil.isListNotEmpty(getListaPedidovendamaterial())){
			for(Pedidovendamaterial vendamaterial : getListaPedidovendamaterial()){
				Double qtde = vendamaterial.getQuantidade() == null || vendamaterial.getQuantidade() == 0 ? 1 : vendamaterial.getQuantidade();
				if(vendamaterial.getMaterial().getPesobruto() != null){
					Double pesoBruto = vendamaterial.getMaterial().getPesobruto();
					if(vendamaterial.getUnidademedida() != null && vendamaterial.getMaterial() != null && 
							vendamaterial.getMaterial().getUnidademedida() != null && 
							!vendamaterial.getUnidademedida().equals(vendamaterial.getMaterial().getUnidademedida())){
						pesoBruto = pesoBruto / vendamaterial.getFatorconversaoQtdereferencia();
					}
					totalPesoBruto +=  pesoBruto * qtde;
				}
			}
		}			
		return SinedUtil.roundByParametro(totalPesoBruto, 2);
	}
	public void setTotalPesoBruto(Double totalPesoBruto) {
		this.totalPesoBruto = totalPesoBruto;
	}
	
	@Transient
	@DisplayName("Peso L�quido Total")
	public Double getTotalPesoLiquido() {
		Double totalPesoLiquido = 0.0;
		if(SinedUtil.isListNotEmpty(getListaPedidovendamaterial())){
			for(Pedidovendamaterial vendamaterial : getListaPedidovendamaterial()){
				Double qtde = vendamaterial.getQuantidade() == null || vendamaterial.getQuantidade() == 0 ? 1 : vendamaterial.getQuantidade();
				if(vendamaterial.getPesoVendaOuMaterial() != null){
					Double pesoLiquido = vendamaterial.getPesoVendaOuMaterial();
					if(vendamaterial.getUnidademedida() != null && vendamaterial.getMaterial() != null && 
							vendamaterial.getMaterial().getUnidademedida() != null && 
							!vendamaterial.getUnidademedida().equals(vendamaterial.getMaterial().getUnidademedida())){
						pesoLiquido = pesoLiquido / vendamaterial.getFatorconversaoQtdereferencia();
					}
					totalPesoLiquido += pesoLiquido * qtde;
				}
			}
		}
		return SinedUtil.roundByParametro(totalPesoLiquido, 2);
	}
	public void setTotalPesoLiquido(Double totalPesoLiquido) {
		this.totalPesoLiquido = totalPesoLiquido;
	}

	@Transient
	public String getErroConfirmacao() {
		return erroConfirmacao;
	}
	public void setErroConfirmacao(String erroConfirmacao) {
		this.erroConfirmacao = erroConfirmacao;
	}

	@Transient
	public String getWhereInConfirmacao() {
		return whereInConfirmacao;
	}
	public void setWhereInConfirmacao(String whereInConfirmacao) {
		this.whereInConfirmacao = whereInConfirmacao;
	}
	
	@Transient
	public String getWhereInCancelamento() {
		return whereInCancelamento;
	}
	public void setWhereInCancelamento(String whereInCancelamento) {
		this.whereInCancelamento = whereInCancelamento;
	}

	@Transient
	@DisplayName("Qtde. acima estoque")
	public Double getQtddisponivelAcimaestoque() {
		return qtddisponivelAcimaestoque;
	}
	@Transient
	public String getMsgacimaestoque() {
		return msgacimaestoque;
	}

	public void setQtddisponivelAcimaestoque(Double qtddisponivelAcimaestoque) {
		this.qtddisponivelAcimaestoque = qtddisponivelAcimaestoque;
	}
	public void setMsgacimaestoque(String msgacimaestoque) {
		this.msgacimaestoque = msgacimaestoque;
	}
	
	@Transient
	public Boolean getIsNotVerificarestoquePedidovenda() {
		if(isNotVerificarestoquePedidovenda == null)
			return Boolean.FALSE;
		return isNotVerificarestoquePedidovenda;
	}
	public void setIsNotVerificarestoquePedidovenda(Boolean isNotVerificarestoquePedidovenda) {
		this.isNotVerificarestoquePedidovenda = isNotVerificarestoquePedidovenda;
	}

	@Transient
	public Boolean getPermitirvendasemestoque() {
		if(permitirvendasemestoque == null)
			return Boolean.FALSE;
		return permitirvendasemestoque;
	}

	public void setPermitirvendasemestoque(Boolean permitirvendasemestoque) {
		this.permitirvendasemestoque = permitirvendasemestoque;
	}

	@Transient
	public Boolean getExisteColeta() {
		return existeColeta;
	}
	public void setExisteColeta(Boolean existeColeta) {
		this.existeColeta = existeColeta;
	}
	
	@Transient
	public Boolean getIdentificadorAutomatico() {
		return identificadorAutomatico;
	}
	
	public void setIdentificadorAutomatico(Boolean identificadorAutomatico) {
		this.identificadorAutomatico = identificadorAutomatico;
	}

	@Transient
	@DisplayName("Vendedor Principal")
	public String getVendedorprincipal() {
		return vendedorprincipal;
	}
	
	public void setVendedorprincipal(String vendedorprincipal) {
		this.vendedorprincipal = vendedorprincipal;
	}
	
	@DisplayName("Origem E-commerce")
	public Boolean getOrigemwebservice() {
		return origemwebservice;
	}

	public void setOrigemwebservice(Boolean origemwebservice) {
		this.origemwebservice = origemwebservice;
	}
	
	@Transient
	public Money getSaldovalecompra() {
		return saldovalecompra;
	}
	
	public void setSaldovalecompra(Money saldovalecompra) {
		this.saldovalecompra = saldovalecompra;
	}

	@OneToMany(mappedBy="pedidovenda")
	@DisplayName("Comiss�o de representa��o")
	public List<Pedidovendapagamentorepresentacao> getListaPedidovendapagamentorepresentacao() {
		return listaPedidovendapagamentorepresentacao;
	}
	@OneToMany(mappedBy="pedidovenda")
	@DisplayName("Negocia��es")
	public List<PedidoVendaNegociacao> getListaPedidoVendaNegociacao() {
		return listaPedidoVendaNegociacao;
	}
	
	@OneToMany(mappedBy="pedidovenda")
	public List<Producaoagenda> getListaProduocaoagenda() {
		return listaProduocaoagenda;
	}
	
	public void setListaProduocaoagenda(List<Producaoagenda> listaProduocaoagenda) {
		this.listaProduocaoagenda = listaProduocaoagenda;
	}
	
	@OneToMany(mappedBy="pedidoVenda")
	public List<PedidoVendaFornecedorTicketMedio> getListaPedidoVendaFornecedorTicketMedio() {
		return listaPedidoVendaFornecedorTicketMedio;
	}
	
	public void setListaPedidoVendaFornecedorTicketMedio(List<PedidoVendaFornecedorTicketMedio> listaPedidoVendaFornecedorTicketMedio) {
		this.listaPedidoVendaFornecedorTicketMedio = listaPedidoVendaFornecedorTicketMedio;
	}

	public void setListaPedidovendapagamentorepresentacao(List<Pedidovendapagamentorepresentacao> listaPedidovendapagamentorepresentacao) {
		this.listaPedidovendapagamentorepresentacao = listaPedidovendapagamentorepresentacao;
	}
	
	@DisplayName("Condi��o de Pagamento")
	@JoinColumn(name="cdprazopagamentorepresentacao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Prazopagamento getPrazopagamentorepresentacao() {
		return prazopagamentorepresentacao;
	}
	@DisplayName("Forma de pagamento")
	@JoinColumn(name="cdformapagamentorepresentacao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Documentotipo getDocumentotiporepresentacao() {
		return documentotiporepresentacao;
	}
	@JoinColumn(name="cdcontarepresentacao")
	@DisplayName("Conta para Boleto")
	@ManyToOne(fetch=FetchType.LAZY)
	public Conta getContaboletorepresentacao() {
		return contaboletorepresentacao;
	}

	public void setPrazopagamentorepresentacao(Prazopagamento prazopagamentorepresentacao) {
		this.prazopagamentorepresentacao = prazopagamentorepresentacao;
	}
	public void setDocumentotiporepresentacao(Documentotipo documentotiporepresentacao) {
		this.documentotiporepresentacao = documentotiporepresentacao;
	}
	public void setContaboletorepresentacao(Conta contaboletorepresentacao) {
		this.contaboletorepresentacao = contaboletorepresentacao;
	}

	@Transient
	public Money getTotalparcelarepresentacao() {
		Money valor = new Money(0);
		if(Hibernate.isInitialized(this.listaPedidovendapagamentorepresentacao) && SinedUtil.isListNotEmpty(this.listaPedidovendapagamentorepresentacao)){
			for (Pedidovendapagamentorepresentacao pedidovendapagamentorepresentacao : this.listaPedidovendapagamentorepresentacao) {
				valor = valor.add(new Money(pedidovendapagamentorepresentacao.getValororiginal()));
			}
		}		
		return valor;
	}

	public void setTotalparcelarepresentacao(Money totalparcelarepresentacao) {
		this.totalparcelarepresentacao = totalparcelarepresentacao;
	}
	
	@DisplayName("Desconto da comiss�o")
	public Money getValordescontorepresentacao() {
		return valordescontorepresentacao;
	}
	@DisplayName("Valor da comiss�o bruta")
	public Money getValorbrutorepresentacao() {
		return valorbrutorepresentacao;
	}
	@Transient
	@DisplayName("Valor total da comiss�o")
	public Money getValortotalrepresentacao() {
		Money valor = new Money();
		
		if(getValorbrutorepresentacao() != null){
			valor = getValorbrutorepresentacao();
		}
		if(getValordescontorepresentacao() != null && getValordescontorepresentacao().getValue().doubleValue() > 0){
			valor = valor.subtract(getValordescontorepresentacao());
		}
		return valortotalrepresentacao;
	}

	public void setValordescontorepresentacao(Money valordescontorepresentacao) {
		this.valordescontorepresentacao = valordescontorepresentacao;
	}
	public void setValorbrutorepresentacao(Money valorbrutorepresentacao) {
		this.valorbrutorepresentacao = valorbrutorepresentacao;
	}
	public void setValortotalrepresentacao(Money valortotalrepresentacao) {
		this.valortotalrepresentacao = valortotalrepresentacao;
	}
	
	public Boolean getOrigemOtr() {
		return origemOtr;
	}
	public void setOrigemOtr(Boolean origemOtr) {
		this.origemOtr = origemOtr;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdenderecofaturamento")
	@DisplayName("Endere�o de faturamento")
	public Endereco getEnderecoFaturamento() {
		return enderecoFaturamento;
	}
	public void setEnderecoFaturamento(Endereco enderecoFaturamento) {
		this.enderecoFaturamento = enderecoFaturamento;
	}
	
	@Transient
	public List<Fornecedor> getListaFornecedor() {
		return listaFornecedor;
	}
	public void setListaFornecedor(List<Fornecedor> listaFornecedor) {
		this.listaFornecedor = listaFornecedor;
	}
	@Transient
	public Money getPercentualRepresentacao() {
		return percentualRepresentacao;
	}
	public void setPercentualRepresentacao(Money percentualRepresentacao) {
		this.percentualRepresentacao = percentualRepresentacao;
	}

	public Integer getIdentificadorcarregamento() {
		return identificadorcarregamento;
	}
	public void setIdentificadorcarregamento(Integer identificadorcarregamento) {
		this.identificadorcarregamento = identificadorcarregamento;
	}

	@Transient
	public Money getPercentualaplicadototal(){
		Double percentualaplicadototal = 0d;
		Double valordescontoitem = 0d;
		Double totalvenda = 0d;
		Double totaltabela = 0d;
		
		if(SinedUtil.isListNotEmpty(getListaPedidovendamaterial())){
			for(Pedidovendamaterial vendamaterial : getListaPedidovendamaterial()){
				
				if(vendamaterial != null && vendamaterial.getvalortabelaRelatorio() != null){
					totaltabela += vendamaterial.getvalortabelaRelatorio();
				}
				if(vendamaterial != null && vendamaterial.getConsiderarDesconto() != null && vendamaterial.getConsiderarDesconto() 
					&& vendamaterial.getValordescontocustopedidovendaitem() != null && vendamaterial.getValorvalecomprapropocional() != null
					&& ((vendamaterial.getConsiderarValeCompra() == null || !vendamaterial.getConsiderarValeCompra()) 
						|| (vendamaterial.getConsiderarValeCompra() != null && vendamaterial.getConsiderarValeCompra()))){
	
						valordescontoitem += vendamaterial.getValordescontocustopedidovendaitem();
				}
				if(vendamaterial.getConsiderarValeCompra() != null && vendamaterial.getConsiderarValeCompra() 
						&& (!vendamaterial.getConsiderarDesconto() || vendamaterial.getConsiderarDesconto() == null)
						&& vendamaterial.getValorvalecomprapropocional() != null){
					
					valordescontoitem += vendamaterial.getValorvalecomprapropocional();
				}
				if(vendamaterial.getTotalprodutoReport() != null){
					totalvenda += vendamaterial.getTotalprodutoReport().getValue().doubleValue();
				}
			}
			if(totaltabela != null && totaltabela > 0){
				percentualaplicadototal = ( valordescontoitem / totaltabela )* 100;
			}else if(totalvenda != null && totalvenda > 0){
				percentualaplicadototal = valordescontoitem / totalvenda * 100;
			}
		}
		
		return new Money(percentualaplicadototal);
	}

	@Transient
	public Double getPercentualcomissaoagencia() {
		return percentualcomissaoagencia;
	}

	public void setPercentualcomissaoagencia(Double percentualcomissaoagencia) {
		this.percentualcomissaoagencia = percentualcomissaoagencia;
	}

	@Transient
	@DisplayName("Replicar material ao incluir")
	public Boolean getReplicarmaterial() {
		return replicarmaterial;
	}
	
	@Transient
	public Boolean getReplicarPneu() {
		return replicarPneu;
	}
	public void setReplicarPneu(Boolean replicarPneu) {
		this.replicarPneu = replicarPneu;
	}

	@Transient
	@DisplayName("Quantidade de vezes")
	public Integer getQtdereplicar() {
		return qtdereplicar;
	}
	
	@Transient
	@DisplayName("Quantidade de vezes")
	public Integer getQtdeReplicarPneu() {
		return qtdeReplicarPneu;
	}
	public void setQtdeReplicarPneu(Integer qtdeReplicarPneu) {
		this.qtdeReplicarPneu = qtdeReplicarPneu;
	}
	
	public void setReplicarmaterial(Boolean replicarmaterial) {
		this.replicarmaterial = replicarmaterial;
	}

	public void setQtdereplicar(Integer qtdereplicar) {
		this.qtdereplicar = qtdereplicar;
	}

	@Transient
	public Double getQtdeTotalVolumesWms() {
		Double qtdeTotalVolumes = 0d;
		if(Hibernate.isInitialized(getListaPedidovendamaterial()) && SinedUtil.isListNotEmpty(getListaPedidovendamaterial())){
			for(Pedidovendamaterial pedidovendamaterial : getListaPedidovendamaterial()){
				if(pedidovendamaterial.getQtdevolumeswms() != null){
					qtdeTotalVolumes += pedidovendamaterial.getQtdevolumeswms();
				}
			}
		}
		return qtdeTotalVolumes;
	}

	@Transient
	@DisplayName("Valor Final + Impostos")
	public Money getValorfinalMaisImpostos() {
		return valorfinalMaisImpostos;
	}

	public void setValorfinalMaisImpostos(Money valorfinalMaisImpostos) {
		this.valorfinalMaisImpostos = valorfinalMaisImpostos;
	}
	
	@DisplayName ("Total IPI")
	@Transient
	public Money getTotalipi() {
		/*Money valor = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterial)){
			for (Pedidovendamaterial item : this.listaPedidovendamaterial) {
				valor = valor.add(item.getValoripi());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterialmestre)){
			for (Pedidovendamaterialmestre item : this.listaPedidovendamaterialmestre) {
				valor = valor.add(item.getValoripi());
			}
		}*/
		return totalipi;
	}
	
	public void setTotalipi(Money totalipi) {
		this.totalipi = totalipi;
	}

	@Transient
	public String getWhereInOSV() {
		return whereInOSV;
	}

	public void setWhereInOSV(String whereInOSV) {
		this.whereInOSV = whereInOSV;
	}

	@Formula("(select max(pi.dtprazoentrega) from Pedidovendamaterial pi where pi.cdpedidovenda = cdpedidovenda)")
	@Basic(fetch=FetchType.LAZY)
	public Date getDtprazoentregamax() {
		return dtprazoentregamax;
	}
	
	public void setDtprazoentregamax(Date dtprazoentregamax) {
		this.dtprazoentregamax = dtprazoentregamax;
	}
	
	@Transient
	public Date getPrazoentregaMinimo() {
		return prazoentregaMinimo;
	}
	
	public void setPrazoentregaMinimo(Date prazoentregaMinimo) {
		this.prazoentregaMinimo = prazoentregaMinimo;
	}

	public Boolean getLimitecreditoexcedido() {
		return limitecreditoexcedido;
	}

	public void setLimitecreditoexcedido(Boolean limitecreditoexcedido) {
		this.limitecreditoexcedido = limitecreditoexcedido;
	}
	
	@Transient
	public Boolean getValorSupeiorLimiteCredito() {
		return valorSupeiorLimiteCredito;
	}

	public void setValorSupeiorLimiteCredito(Boolean valorSupeiorLimiteCredito) {
		this.valorSupeiorLimiteCredito = valorSupeiorLimiteCredito;
	}
	
	@Transient
	public Pneu getPneu() {
		return pneu;
	}
	
	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}
	
	@Transient
	@DisplayName("�ltimo Valor")
	public Money getUltimoValorVenda() {
		return ultimoValorVenda;
	}
	
	public void setUltimoValorVenda(Money ultimoValorVenda) {
		this.ultimoValorVenda = ultimoValorVenda;
	}
	
	@Transient
	public Boolean getRegistrarpesomedio() {
		return registrarpesomedio;
	}
	public void setRegistrarpesomedio(Boolean registrarpesomedio) {
		this.registrarpesomedio = registrarpesomedio;
	}
	
	@Transient
	public Double getPercentualdescontoTabelapreco() {
		return percentualdescontoTabelapreco;
	}
	public void setPercentualdescontoTabelapreco(
			Double percentualdescontoTabelapreco) {
		this.percentualdescontoTabelapreco = percentualdescontoTabelapreco;
	}

	@Transient
	public Pedidovendamaterial getPedidovendamaterialgarantido() {
		return pedidovendamaterialgarantido;
	}
	public void setPedidovendamaterialgarantido(
			Pedidovendamaterial pedidovendamaterialgarantido) {
		this.pedidovendamaterialgarantido = pedidovendamaterialgarantido;
	}
	
	@Transient
	public Money getDescontogarantiareforma() {
		return descontogarantiareforma;
	}
	public void setDescontogarantiareforma(Money descontogarantiareforma) {
		this.descontogarantiareforma = descontogarantiareforma;
	}
	
	public void setSituacaoPendencia(SituacaoPendencia situacaoPendencia) {
		this.situacaoPendencia = situacaoPendencia;
	}
	
	public void setValorFreteCIF(Money valorFreteCIF) {
		this.valorFreteCIF = valorFreteCIF;
	}
	
	public void setObservacaoPedidoVendaTipo(String observacaoPedidoVendaTipo) {
		this.observacaoPedidoVendaTipo = observacaoPedidoVendaTipo;
	}
	
	//////////////////////////////////////|
	/////// Inicio campos @Transient     |
	//////////////////////////////////////|
	
	@Transient
	public boolean getExisteGarantia(){
		if(Hibernate.isInitialized(getListaPedidovendamaterial()) && SinedUtil.isListNotEmpty(getListaPedidovendamaterial())){
			for(Pedidovendamaterial pvm : getListaPedidovendamaterial()){
				if(pvm.getGarantiareformaitem() != null){
					return true;
				}
			}
		}
		return false;
	}
	
	@Transient
	public Money getValorVendasRezalida() {
		return valorVendasRezalida;
	}
	
	@Transient
	public Money getValorPendentes() {
		Money valorPendente = new Money();
		Money totalVenda = getTotalvenda();
		
		if(totalVenda != null){
			valorPendente = totalVenda;
		}
		if(getValorVendasRezalida() != null){
			valorPendente = valorPendente.subtract(getValorVendasRezalida());
		}
		
		return valorPendente;
	}
	
	@Transient
	public Date getDtGerarValeCompra() {
		return dtGerarValeCompra;
	}
	@Transient
	public String getDescricaoValeCompra() {
		return descricaoValeCompra;
	}

	@Transient
	public Garantiareformaitem getGarantiareformaitem() {
		return garantiareformaitem;
	}
	@Transient
	public Integer getQtdepedidovendapagamento() {
		return qtdepedidovendapagamento;
	}
	@Transient
	public Tipooperacao getOpValeCompra() {
		return opValeCompra;
	}
	
	@Transient
	public Integer getQtdedocumento() {
		return qtdedocumento;
	}
	public void setValorVendasRezalida(Money valorVendasRezalida) {
		this.valorVendasRezalida = valorVendasRezalida;
	}

	public void setGarantiareformaitem(Garantiareformaitem garantiareformaitem) {
		this.garantiareformaitem = garantiareformaitem;
	}
	public void setQtdepedidovendapagamento(Integer qtdepedidovendapagamento) {
		this.qtdepedidovendapagamento = qtdepedidovendapagamento;
	}
	
	public void setQtdedocumento(Integer qtdedocumento) {
		this.qtdedocumento = qtdedocumento;
	}
	
	public void setDescricaoValeCompra(String descricaoValeCompra) {
		this.descricaoValeCompra = descricaoValeCompra;
	}
	public void setDtGerarValeCompra(Date dtGerarValeCompra) {
		this.dtGerarValeCompra = dtGerarValeCompra;
	}
	public void setOpValeCompra(Tipooperacao opValeCompra) {
		this.opValeCompra = opValeCompra;
	}
	
	
	@Transient
	public Money getTotalvendaSemArredondamento() {
		Money valor = new Money(0);
		if(this.listaPedidovendamaterial != null && !this.listaPedidovendamaterial.isEmpty()){
			for (Pedidovendamaterial vendamaterial : this.listaPedidovendamaterial) {
				Double qtde = vendamaterial.getQuantidade() == null || vendamaterial.getQuantidade() == 0 ? 1 : vendamaterial.getQuantidade();
				Double multiplicador = vendamaterial.getMultiplicador() == null || vendamaterial.getMultiplicador() == 0 ? 1 : vendamaterial.getMultiplicador();
				if(vendamaterial.getPreco() != null){
					Double valorTotal = SinedUtil.round(BigDecimal.valueOf(vendamaterial.getPreco().doubleValue() * (qtde * multiplicador)), 10).doubleValue();
					valor = valor.add(new Money(valorTotal));
				}
				if(vendamaterial.getDesconto() != null){
					valor = valor.subtract(vendamaterial.getDesconto());
				}
			}
		}
		
		valor = valor.add(this.valorfrete != null ? this.valorfrete : new Money(0.0));
		valor = valor.add(this.taxapedidovenda != null ? this.taxapedidovenda : new Money(0.0));
		valor = valor.subtract(this.desconto != null ? this.desconto : new Money(0.0));
		valor = valor.subtract(this.valorusadovalecompra != null ? this.valorusadovalecompra : new Money(0.0));
		return valor;
	}
	
	@Transient
	public Money getValorvalecompranovo() {
		return valorvalecompranovo;
	}
	
	public void setValorvalecompranovo(Money valorvalecompranovo) {
		this.valorvalecompranovo = valorvalecompranovo;
	}
	
	@Transient
	public Boolean getFromWebService() {
		return fromWebService;
	}
	public void setFromWebService(Boolean fromWebService) {
		this.fromWebService = fromWebService;
	}
	@Transient
	public PedidoVendaParametersBean getParameters() {
		return parameters;
	}
	public void setParameters(PedidoVendaParametersBean parameters) {
		this.parameters = parameters;
	}
	@Transient
	@Override
	public Materialtabelapreco getMaterialtabelapreco() {
		return null;
	}

	@Transient
	@Override
	public List<VendaMaterialProducaoItemInterface> getItens() {
		List<VendaMaterialProducaoItemInterface> lista = new ArrayList<VendaMaterialProducaoItemInterface>();
		for(Pedidovendamaterial vm: this.getListaPedidovendamaterial()){
			lista.add(vm);
		}
		return lista;
	}
	@Transient
	public List<Message> getMensagemAposSalvar() {
		if(mensagemAposSalvar == null){
			mensagemAposSalvar = new ArrayList<Message>();
		}
		return mensagemAposSalvar;
	}
	public void setMensagemAposSalvar(List<Message> mensagemAposSalvar) {
		this.mensagemAposSalvar = mensagemAposSalvar;
	}
	
	@Transient
	public List<Pneu> getListaPneu() {
		if(listaPneu == null){
			listaPneu = new ListSet<Pneu>(Pneu.class);
		}
		return listaPneu;
	}
	public void setListaPneu(List<Pneu> listaPneu) {
		this.listaPneu = listaPneu;
	}
	
	@Transient
	public Pneu getPneuInclusao() {
		return pneuInclusao;
	}
	public void setPneuInclusao(Pneu pneuInclusao) {
		this.pneuInclusao = pneuInclusao;
	}
	
	@Transient
	public List<PneuItemVendaInterface> getListaPneuItemInterface(){
		List<PneuItemVendaInterface> lista = new ArrayList<PneuItemVendaInterface>();
		for(Pedidovendamaterial item: listaPedidovendamaterial){
			lista.add(item);
		}
		return lista;
	}
	
	@Transient
	public List<PneuItemVendaInterface> getListaItemAvulso() {
		return listaItemAvulso;
	}
	public void setListaItemAvulso(List<PneuItemVendaInterface> listaItemAvulso) {
		this.listaItemAvulso = listaItemAvulso;
	}
	
	@Transient
	public Money getTotalFcp() {
		/*totalFcp = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterial)){
			for (Pedidovendamaterial item : this.listaPedidovendamaterial) {
				totalFcp = totalFcp.add(item.getValorfcp());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterialmestre)){
			for (Pedidovendamaterialmestre item : this.listaPedidovendamaterialmestre) {
				totalFcp = totalFcp.add(item.getValorfcp());
			}
		}*/
		return totalFcp;
	}
	public void setTotalFcp(Money totalFcp) {
		this.totalFcp = totalFcp;
	}
	
	@Transient
	public Money getTotalFcpSt() {
		/*totalFcpSt = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterial)){
			for (Pedidovendamaterial item : this.listaPedidovendamaterial) {
				totalFcpSt = totalFcpSt.add(item.getValorfcpst());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterialmestre)){
			for (Pedidovendamaterialmestre item : this.listaPedidovendamaterialmestre) {
				totalFcpSt = totalFcpSt.add(item.getValorfcpst());
			}
		}*/
		return totalFcpSt;
	}
	public void setTotalFcpSt(Money totalFcpSt) {
		this.totalFcpSt = totalFcpSt;
	}
	
	@Transient
	public Money getTotalDesoneracaoIcms() {
		return totalDesoneracaoIcms;
	}
	
	public void setTotalDesoneracaoIcms(Money totalDesoneracaoIcms) {
		this.totalDesoneracaoIcms = totalDesoneracaoIcms;
	}
	
	@Transient
	public Money getTotalIcms() {
		/*totalIcms = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterial)){
			for (Pedidovendamaterial item : this.listaPedidovendamaterial) {
				totalIcms = totalIcms.add(item.getValoricms());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterialmestre)){
			for (Pedidovendamaterialmestre item : this.listaPedidovendamaterialmestre) {
				totalIcms = totalIcms.add(item.getValoricms());
			}
		}*/
		return totalIcms;
	}
	public void setTotalIcms(Money totalIcms) {
		this.totalIcms = totalIcms;
	}

	@Transient
	public Money getTotalIcmsSt() {
		/*totalIcmsSt = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterial)){
			for (Pedidovendamaterial item : this.listaPedidovendamaterial) {
				totalIcmsSt = totalIcmsSt.add(item.getValoricmsst());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterialmestre)){
			for (Pedidovendamaterialmestre item : this.listaPedidovendamaterialmestre) {
				totalIcmsSt = totalIcmsSt.add(item.getValoricmsst());
			}
		}*/
		return totalIcmsSt;
	}
	public void setTotalIcmsSt(Money totalIcmsSt) {
		this.totalIcmsSt = totalIcmsSt;
	}
	
	@Transient
	public Money getTotalImpostos() {
		totalImpostos = new Money();
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterial)){
			for (Pedidovendamaterial item : this.listaPedidovendamaterial) {
				totalImpostos = totalImpostos.add(item.getValoripi())
											.add(item.getValoricms())
											.add(item.getValoricmsst())
											.add(item.getValorfcp())
											.add(item.getValorfcpst())
											.add(item.getValordifal());
				
				if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
					totalImpostos = totalImpostos.subtract(item.getValordesoneracaoicms());
				}
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterialmestre)){
			for (Pedidovendamaterialmestre item : this.listaPedidovendamaterialmestre) {
				totalImpostos = totalImpostos.add(item.getValoripi())
											.add(item.getValoricms())
											.add(item.getValoricmsst())
											.add(item.getValorfcp())
											.add(item.getValorfcpst())
											.add(item.getValordifal());
				
				if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
					totalImpostos = totalImpostos.subtract(item.getValordesoneracaoicms());
				}
			}
		}
		return totalImpostos;
	}
	public void setTotalImpostos(Money totalImpostos) {
		this.totalImpostos = totalImpostos;
	}
	
	@Transient
	public Money getTotalOutrasDespesas() {
		totalOutrasDespesas = new Money();
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterial)){
			for (Pedidovendamaterial vendamaterial : this.listaPedidovendamaterial) {
				totalOutrasDespesas = totalOutrasDespesas.add(vendamaterial.getOutrasdespesas());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterialmestre)){
			for (Pedidovendamaterialmestre item : this.listaPedidovendamaterialmestre) {
				totalOutrasDespesas = totalOutrasDespesas.add(item.getOutrasdespesas());
			}
		}
		return totalOutrasDespesas;
	}
	public void setTotalOutrasDespesas(Money totalOutrasDespesas) {
		this.totalOutrasDespesas = totalOutrasDespesas;
	}
	
	@Transient
	public Money getTotalDifal() {
		return totalDifal;
	}
	@Override
	public void setTotalDifal(Money totalDifal) {
		this.totalDifal = totalDifal;
	}
	
	@Transient
	public Money getTotalValorSeguro() {
		totalValorSeguro = new Money();
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterial)){
			for (Pedidovendamaterial vendamaterial : this.listaPedidovendamaterial) {
				totalValorSeguro = totalValorSeguro.add(vendamaterial.getValorSeguro());
			}
		}
		if(SinedUtil.isListNotEmpty(listaPedidovendamaterialmestre)){
			for (Pedidovendamaterialmestre item : this.listaPedidovendamaterialmestre) {
				totalValorSeguro = totalValorSeguro.add(item.getValorSeguro());
			}
		}
		return totalValorSeguro;
	}
	public void setTotalValorSeguro(Money totalValorSeguro) {
		this.totalValorSeguro = totalValorSeguro;
	}
	
	@Transient
	public Boolean getIncluirEnderecoFaturamento() {
		incluirEnderecoFaturamento = Util.objects.isPersistent(this.getEnderecoFaturamento());
		return incluirEnderecoFaturamento;
	}
	public void setIncluirEnderecoFaturamento(Boolean incluirEnderecoFaturamento) {
		this.incluirEnderecoFaturamento = incluirEnderecoFaturamento;
	}
	@Transient
	public Money getValorSeguro() {
		return valorSeguro;
	}
	public void setValorSeguro(Money valorSeguro) {
		this.valorSeguro = valorSeguro;
	}
	@Transient
	public Money getOutrasdespesas() {
		return outrasdespesas;
	}
	public void setOutrasdespesas(Money outrasdespesas) {
		this.outrasdespesas = outrasdespesas;
	}
	
	@Transient
	@DisplayName ("Qtde dispon�vel")
	public Double getQtdeEstoqueMenosReservada() {
		return qtdeEstoqueMenosReservada;
	}
	public void setQtdeEstoqueMenosReservada(Double qtdeEstoqueMenosReservada) {
		this.qtdeEstoqueMenosReservada = qtdeEstoqueMenosReservada;
	}
	@Transient
	public Money getTotalvendaimpostos() {
		Money valor = getTotalvenda();
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterial)){
			for (Pedidovendamaterial item : this.listaPedidovendamaterial) {
				valor = valor.add(item.getValoripi())
							.add(item.getValoricmsst())
							.add(item.getValorfcpst());
				
				if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
					valor = valor.subtract(item.getValordesoneracaoicms());
				}
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaPedidovendamaterialmestre)){
			for (Pedidovendamaterialmestre item : this.listaPedidovendamaterialmestre) {
				valor = valor.add(item.getValoripi())
							.add(item.getValoricmsst())
							.add(item.getValorfcpst());
				
				if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
					valor = valor.subtract(item.getValordesoneracaoicms());
				}
			}
		}
		return valor;
	}
	
	@Transient
	public String getMenorDtVencimentoLote() {
		return menorDtVencimentoLote;
	}
	
	public void setMenorDtVencimentoLote(String menorDtVencimentoLote) {
		this.menorDtVencimentoLote = menorDtVencimentoLote;
	}
	
	@Transient
	public List<InclusaoLoteVendaInterface> getListaInclusaoLoteVendaInterface(){
		List<InclusaoLoteVendaInterface> lista = new ArrayList<InclusaoLoteVendaInterface>();
		for(Pedidovendamaterial pvm: this.listaPedidovendamaterial){
			lista.add(pvm);
		}
		return lista;
	}

	@Transient
	public ContasFinanceiroSituacao getContasFinanceiroSituacao() {
		return contasFinanceiroSituacao;
	}

	public void setContasFinanceiroSituacao(
			ContasFinanceiroSituacao contasFinanceiroSituacao) {
		this.contasFinanceiroSituacao = contasFinanceiroSituacao;
	}
	
	@Transient
	public Pedidovendasituacaoecommerce getPedidovendasituacaoecommerce() {
		return pedidovendasituacaoecommerce;
	}
	
	public void setPedidovendasituacaoecommerce(
			Pedidovendasituacaoecommerce pedidovendasituacaoecommerce) {
		this.pedidovendasituacaoecommerce = pedidovendasituacaoecommerce;
	}
	
	@Transient
	public String getNomeSituacaoEcommerce() {
		return nomeSituacaoEcommerce;
	}
	
	public void setNomeSituacaoEcommerce(String nomeSituacaoEcommerce) {
		this.nomeSituacaoEcommerce = nomeSituacaoEcommerce;
	}
	
	@Transient
	public Boolean getExibeIconeNaoVisualizadoEcommerce() {
		return exibeIconeNaoVisualizadoEcommerce;
	}
	public void setExibeIconeNaoVisualizadoEcommerce(Boolean exibeIconeNaoVisualizadoEcommerce) {
		this.exibeIconeNaoVisualizadoEcommerce = exibeIconeNaoVisualizadoEcommerce;
	}
	
	@DisplayName ("TICKET M�DIO CALCULADO (POR KG)")
	@Transient
	public Money getTicketMedioCaculado() {
		return ticketMedioCaculado;
	}

	public void setTicketMedioCaculado(Money ticketMedioCaculado) {
		this.ticketMedioCaculado = ticketMedioCaculado;
	}
}
