package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_despesaavulsarhrateio",sequenceName="sq_despesaavulsarhrateio")
public class Despesaavulsarhrateio implements Log {

	private Integer cddespesaavulsarhrateio;
	private Despesaavulsarh despesaavulsarh;
	private Centrocusto centrocusto;
	private Projeto projeto;
	private Contagerencial contagerencial;
	private Double percentual;
	private Money valor;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;

	@Id
	@GeneratedValue(generator = "sq_despesaavulsarhrateio", strategy = GenerationType.AUTO)
	public Integer getCddespesaavulsarhrateio() {
		return cddespesaavulsarhrateio;
	}

	public void setCddespesaavulsarhrateio(Integer cddespesaavulsarhrateio) {
		this.cddespesaavulsarhrateio = cddespesaavulsarhrateio;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddespesaavulsarh")
	public Despesaavulsarh getDespesaavulsarh() {
		return despesaavulsarh;
	}

	public void setDespesaavulsarh(Despesaavulsarh despesaavulsarh) {
		this.despesaavulsarh = despesaavulsarh;
	}

	@Required
	@DisplayName("Centro de custo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}

	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	@Required
	@DisplayName("Conta gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}

	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}

	@Required
	public Double getPercentual() {
		return percentual;
	}

	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}

	@Required
	public Money getValor() {
		return valor;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	@DisplayName("�ltima altera��o")
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
