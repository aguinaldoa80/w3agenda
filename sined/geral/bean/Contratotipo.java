package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator (name = "sq_contratotipo", sequenceName = "sq_contratotipo")
@DisplayName("Tipo de Contrato")
public class Contratotipo implements Log{

	protected Integer cdcontratotipo;
	protected String nome;
	protected Frequencia frequencia;
	protected Boolean locacao;
	protected Boolean faturamentoporitensdevolvidos;
	protected Boolean faturarhoratrabalhada;
	protected Boolean naorenovarperiodolocacaoautomatico;
	protected Boolean confirmarautomaticoprimeiroromaneio;
	protected Material material;
	protected Boolean medicao;
	protected Boolean faturamentowebservice;
	protected Pedidovendatipo pedidovendatipo;
	protected Boolean atualizarProximoVencimentoConfirmar = Boolean.TRUE;
	protected Boolean acrescimoproximofaturamento;
	protected Boolean faturamentoemlote;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera; 
	
	protected List<Contratotipodesconto> listacontratotipodesconto = new ListSet<Contratotipodesconto>(Contratotipodesconto.class);
	protected List<Agendainteracaocontratomodelo> listaagendainteracaocontratomodelo = new ListSet<Agendainteracaocontratomodelo>(Agendainteracaocontratomodelo.class);
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratotipo")
	public Integer getCdcontratotipo() {
		return cdcontratotipo;
	}
	
	@Required
	@DisplayName ("Nome")
	@MaxLength(100)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Periodicidade")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfrequencia")
	public Frequencia getFrequencia() {
		return frequencia;
	}
	
	@DisplayName("Loca��o")
	public Boolean getLocacao() {
		return locacao;
	}
	
	@DisplayName("Faturamento por itens devolvidos")
	public Boolean getFaturamentoporitensdevolvidos() {
		return faturamentoporitensdevolvidos;
	}

	@DisplayName("Material")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}

	@DisplayName("N�o renovar per�odo de loca��o autom�tico")
	public Boolean getNaorenovarperiodolocacaoautomatico() {
		return naorenovarperiodolocacaoautomatico;
	}

	@DisplayName("Confirmar autom�tico no primeiro romaneio")
	public Boolean getConfirmarautomaticoprimeiroromaneio() {
		return confirmarautomaticoprimeiroromaneio;
	}
	
	@DisplayName("Faturamento via webservice")
	public Boolean getFaturamentowebservice() {
		return faturamentowebservice;
	}

	@DisplayName("Tipo de pedido de venda (Faturamento)")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendatipo")
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}

	public void setFaturamentowebservice(Boolean faturamentowebservice) {
		this.faturamentowebservice = faturamentowebservice;
	}

	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}

	public void setNaorenovarperiodolocacaoautomatico(
			Boolean naorenovarperiodolocacaoautomatico) {
		this.naorenovarperiodolocacaoautomatico = naorenovarperiodolocacaoautomatico;
	}

	public void setConfirmarautomaticoprimeiroromaneio(
			Boolean confirmarautomaticoprimeiroromaneio) {
		this.confirmarautomaticoprimeiroromaneio = confirmarautomaticoprimeiroromaneio;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setLocacao(Boolean locacao) {
		this.locacao = locacao;
	}
	
	public void setFaturamentoporitensdevolvidos(Boolean faturamentoporitensdevolvidos) {
		this.faturamentoporitensdevolvidos = faturamentoporitensdevolvidos;
	}

	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}

	public void setCdcontratotipo(Integer cdcontratotipo) {
		this.cdcontratotipo = cdcontratotipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@DisplayName("Desconto")
	@OneToMany(mappedBy="contratotipo")
	public List<Contratotipodesconto> getListacontratotipodesconto() {
		return listacontratotipodesconto;
	}

	public void setListacontratotipodesconto(
			List<Contratotipodesconto> listacontratotipodesconto) {
		this.listacontratotipodesconto = listacontratotipodesconto;
	}

	@DisplayName("Faturar por Horas Trabalhadas")
	public Boolean getFaturarhoratrabalhada() {
		return faturarhoratrabalhada;
	}

	public void setFaturarhoratrabalhada(Boolean faturarhoratrabalhada) {
		this.faturarhoratrabalhada = faturarhoratrabalhada;
	}
	
	@DisplayName("Medi��o")
	public Boolean getMedicao() {
		return medicao;
	}
	
	public void setMedicao(Boolean medicao) {
		this.medicao = medicao;
	}
	
	@DisplayName("Atualizar pr�x. vencimento ao confirmar contrato")
	public Boolean getAtualizarProximoVencimentoConfirmar() {
		return atualizarProximoVencimentoConfirmar;
	}
	
	public void setAtualizarProximoVencimentoConfirmar(Boolean atualizarProximoVencimentoConfirmar) {
		this.atualizarProximoVencimentoConfirmar = atualizarProximoVencimentoConfirmar;
	}
	
	@DisplayName("Acr�scimo somente no pr�ximo faturamento?")
	public Boolean getAcrescimoproximofaturamento() {
		return acrescimoproximofaturamento;
	}
	public void setAcrescimoproximofaturamento(
			Boolean acrescimoproximofaturamento) {
		this.acrescimoproximofaturamento = acrescimoproximofaturamento;
	}
	
	@DisplayName("Faturamento em lote?")
	public Boolean getFaturamentoemlote() {
		return faturamentoemlote;
	}
	public void setFaturamentoemlote(Boolean faturamentoemlote) {
		this.faturamentoemlote = faturamentoemlote;
	}
	
	@DisplayName("Agenda de intera��o")
	@OneToMany(mappedBy="contratotipo")
	public List<Agendainteracaocontratomodelo> getListaagendainteracaocontratomodelo() {
		return listaagendainteracaocontratomodelo;
	}
	public void setListaagendainteracaocontratomodelo(
			List<Agendainteracaocontratomodelo> listaagendainteracaocontratomodelo) {
		this.listaagendainteracaocontratomodelo = listaagendainteracaocontratomodelo;
	}
}