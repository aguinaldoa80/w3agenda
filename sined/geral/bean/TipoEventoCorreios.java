package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
@SequenceGenerator(name="sq_tipoeventocorreios", sequenceName="sq_tipoeventocorreios")
public class TipoEventoCorreios {

	private Integer cdTipoEventoCorreios;
	private String descricao;
	
	@Id
	@GeneratedValue(generator="sq_tipoeventocorreios", strategy=GenerationType.AUTO)
	public Integer getCdTipoEventoCorreios() {
		return cdTipoEventoCorreios;
	}
	public void setCdTipoEventoCorreios(Integer cdTipoEventoCorreios) {
		this.cdTipoEventoCorreios = cdTipoEventoCorreios;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}