package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_documentonegociado", sequenceName = "sq_documentonegociado")
public class Documentonegociado{

	protected Integer cddocumentonegociado;

	public Documentonegociado() {}
	
	public Documentonegociado(Integer cddocumentonegociado) {
		this.cddocumentonegociado = cddocumentonegociado;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_documentonegociado")
	public Integer getCddocumentonegociado() {
		return cddocumentonegociado;
	}
	public void setCddocumentonegociado(Integer cddocumentonegociado) {
		this.cddocumentonegociado = cddocumentonegociado;
	}
}
