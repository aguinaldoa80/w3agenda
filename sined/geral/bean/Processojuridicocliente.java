package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Processojuridicoclienteenvolvido;

@Entity
@SequenceGenerator(name = "sq_processojuridicocliente", sequenceName = "sq_processojuridicocliente")
public class Processojuridicocliente {
	
	protected Integer cdprocessojuridicocliente;
	protected Processojuridico processojuridico;
	protected Processojuridicoclienteenvolvido envolvido;
	protected Cliente cliente;
	protected Partecontraria partecontraria;
	protected Processojuridicoclientetipo processojuridicoclientetipo;
	protected Boolean encabecador;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_processojuridicocliente")
	public Integer getCdprocessojuridicocliente() {
		return cdprocessojuridicocliente;
	}
	
	@DisplayName("Cliente")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Cliente getCliente() {
		return cliente;
	}
	
	@Required
	@DisplayName("Tipo")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdprocessojuridicoclientetipo")
	public Processojuridicoclientetipo getProcessojuridicoclientetipo() {
		return processojuridicoclientetipo;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdpartecontraria")
	public Partecontraria getPartecontraria() {
		return partecontraria;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprocessojuridico")
	public Processojuridico getProcessojuridico() {
		return processojuridico;
	}
	
	@Required
	@DisplayName("Envolvido")
	public Processojuridicoclienteenvolvido getEnvolvido() {
		return envolvido;
	}
	
	@DisplayName("Encabešador")
	public Boolean getEncabecador() {
		return encabecador;
	}

	public void setEncabecador(Boolean encabecador) {
		this.encabecador = encabecador;
	}
	public void setPartecontraria(Partecontraria partecontraria) {
		this.partecontraria = partecontraria;
	}
	public void setEnvolvido(Processojuridicoclienteenvolvido envolvido) {
		this.envolvido = envolvido;
	}
	public void setCdprocessojuridicocliente(Integer cdprocessojuridicocliente) {
		this.cdprocessojuridicocliente = cdprocessojuridicocliente;
	}
	public void setProcessojuridico(Processojuridico processojuridico) {
		this.processojuridico = processojuridico;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setProcessojuridicoclientetipo(
			Processojuridicoclientetipo processojuridicoclientetipo) {
		this.processojuridicoclientetipo = processojuridicoclientetipo;
	}
	
}
