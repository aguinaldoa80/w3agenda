package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;


@Entity
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
public class Interacaotipo{

	protected Integer cdinteracaotipo;
	protected String nome;
	
	@Id
	public Integer getCdinteracaotipo() {
		return cdinteracaotipo;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setCdinteracaotipo(Integer cdinteracaotipo) {
		this.cdinteracaotipo = cdinteracaotipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	

	

}
