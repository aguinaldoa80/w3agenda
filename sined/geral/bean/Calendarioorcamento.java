package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_calendarioorcamento", sequenceName = "sq_calendarioorcamento")
public class Calendarioorcamento{

	protected Integer cdcalendarioorcamento;
	protected Calendario calendario;
	protected Orcamento orcamento;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_calendarioorcamento")
	public Integer getCdcalendarioorcamento() {
		return cdcalendarioorcamento;
	}
	
	public void setCdcalendarioorcamento(Integer cdcalendarioorcamento) {
		this.cdcalendarioorcamento = cdcalendarioorcamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdorcamento")
	@Required
	public Orcamento getOrcamento() {
		return orcamento;
	}
	
	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcalendario")
	public Calendario getCalendario() {
		return calendario;
	}
	
	public void setCalendario(Calendario calendario) {
		this.calendario = calendario;
	}
	
}
