package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;


@Entity
@SequenceGenerator(name = "sq_pedidovendapagamentorepresentacao", sequenceName = "sq_pedidovendapagamentorepresentacao")
public class Pedidovendapagamentorepresentacao {

	protected Integer cdpedidovendapagamentorepresentacao;
	protected Pedidovenda pedidovenda;
	protected Documentotipo documentotipo;
	protected Documento documento;
	protected Date dataparcela;
	protected Money valororiginal;
	protected Fornecedor fornecedor;
	
	//TRANSIENT
	protected Date dataparcelaEmissaonota;
	protected List<Fornecedor> listaFornecedor;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pedidovendapagamentorepresentacao")
	public Integer getCdpedidovendapagamentorepresentacao() {
		return cdpedidovendapagamentorepresentacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdformapagamento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	public Money getValororiginal() {
		return valororiginal;
	}
	@DisplayName("Date de Vencimento")
	public Date getDataparcela() {
		return dataparcela;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	public void setCdpedidovendapagamentorepresentacao(Integer cdpedidovendapagamentorepresentacao) {
		this.cdpedidovendapagamentorepresentacao = cdpedidovendapagamentorepresentacao;
	}
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setDataparcela(Date dataparcela) {
		this.dataparcela = dataparcela;
	}
	public void setValororiginal(Money valororiginal) {
		this.valororiginal = valororiginal;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	@Transient
	public Date getDataparcelaEmissaonota() {
		return dataparcelaEmissaonota;
	}
	@Transient
	public List<Fornecedor> getListaFornecedor() {
		return listaFornecedor;
	}
	
	public void setDataparcelaEmissaonota(Date dataparcelaEmissaonota) {
		this.dataparcelaEmissaonota = dataparcelaEmissaonota;
	}
	public void setListaFornecedor(List<Fornecedor> listaFornecedor) {
		this.listaFornecedor = listaFornecedor;
	}
}
