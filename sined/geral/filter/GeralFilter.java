package br.com.linkcom.sined.geral.filter;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.linkcom.neo.core.config.AuthenticationConfig;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.AgendamentoService;
import br.com.linkcom.sined.geral.service.AgendamentoservicoService;
import br.com.linkcom.sined.geral.service.ApontamentoService;
import br.com.linkcom.sined.geral.service.AvisousuarioService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.ContratojuridicoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.RequisicaoService;
import br.com.linkcom.sined.geral.service.TarefaService;
import br.com.linkcom.sined.geral.service.VeiculoService;
import br.com.linkcom.sined.geral.service.VeiculousoService;
import br.com.linkcom.sined.geral.service.VersaoService;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

public class GeralFilter implements Filter {

	public static final String PARAMETRO_DATAULTIMOACESSO = "PARAMETRO_DATAULTIMOACESSO"; 
	public static final String SUPORTE_CHAT = "SUPORTE_CHAT"; 
	public static final String EXIBIR_INDICACAO_EMPRESA = "EXIBIR_INDICACAO_EMPRESA"; 
	public static final String URL_INDICACAO_EMPRESA = "URL_INDICACAO_EMPRESA"; 
	
	
	public static final String ATTR_LOGO = "cdlogomarca";
	public static final String ATTR_EMPRESA = "EMPRESA_LOGOMARCA_PRINCIPAL";

	public static final String ATTR_EMPRESASELECIONADA = "empresaSelecionada";
	public static final String ATTR_LISTAEMPRESA = "listaEmpresas";
	public static final String ATTR_FIRSTFILTROEMPRESASELECIONADA = "firstFiltroForEmpresaSelecionada";
	
	public static final String ATTR_VEICULO = "/Veiculo";
	public static final String ATTR_VEICULOUSO = "/Veiculouso";
	public static final String ATTR_AGENDAMENTOSERVICO = "/Agendamentoservico";
	
	public void init(FilterConfig config) throws ServletException {
	}
	
	public void destroy() {
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpReq = (HttpServletRequest)req;
		//LogomarcaFilter
		this.addLogoAttribute(httpReq);
		
		//Veiculos
		this.doAtualizacaoVeiculo(httpReq);
		
//		//Agendamento servi�o
//		this.doAtualizacaoAgendamentoServico(httpReq);
		
		//Lista de empresas
		if(SinedUtil.getUsuarioLogado() != null) {
			this.addListaEmpresaAttribute(httpReq);
		}
		
		String requestURI = httpReq.getRequestURI();
		String modulo = null;
		if (requestURI.matches("/.+?/.+?/.*")) {
			modulo = requestURI.split("/")[2];
		}
		
		if(modulo == null || !modulo.equals("pub")){
			try {
				//VersionFilter
				this.doVerificaVersao(req, res, httpReq);
			} catch (SinedException e) {
				AuthenticationConfig config = Neo.getApplicationContext().getConfig().getAuthenticationConfig();
				((HttpServletResponse) res).sendRedirect(config.getLoginPage() + "?errorversion=1");
				
				try {
					SinedUtil.enviaEmailAdmin();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
				return;	
			} catch (Exception e) {
				e.printStackTrace();
				throw new SinedException(e.getMessage());
			}
		}
		
		//MudancaDiaFilter
		this.doAtualizacao(httpReq);
		
		
		if(Neo.getUser() != null){
			httpReq.getSession().setAttribute("username", Neo.getUser().getLogin());
			httpReq.getSession().setAttribute("password", Neo.getUser().getPassword());
		}
		
		chain.doFilter(req, res);
	}
	
//	private void doAtualizacaoAgendamentoServico(HttpServletRequest httpReq) {
//		boolean atualiza = httpReq.getRequestURI().contains(ATTR_AGENDAMENTOSERVICO) ? true : false;
//		
//		if(atualiza && (httpReq.getParameter("ACAO") == null || httpReq.getParameter("ACAO").equals("listagem")))
//			AgendamentoservicoService.getInstance().updateAux();
//	}

	private void doAtualizacaoVeiculo(HttpServletRequest httpReq) {
		boolean atualiza = httpReq.getRequestURI().contains(ATTR_VEICULO) || httpReq.getRequestURI().contains(ATTR_VEICULOUSO) ? true : false;
		
		if(atualiza && (httpReq.getParameter("ACAO") == null || httpReq.getParameter("ACAO").equals("listagem"))){
			VeiculousoService.getInstance().updateAux();
			if(!httpReq.getRequestURI().contains(ATTR_VEICULOUSO))
				VeiculoService.getInstance().updateAux();
		}
	}

	private void addLogoAttribute(HttpServletRequest request){
		
		Empresa empresa = (Empresa)request.getSession().getAttribute(ATTR_EMPRESA);
		if (empresa == null) {
			empresa = EmpresaService.getInstance().loadArquivoPrincipal();
			request.getSession().setAttribute(ATTR_EMPRESA, empresa);
		}
		
		if(empresa != null){
			Arquivo arquivo = empresa.getLogomarcasistema();
			if(arquivo!= null){
				request.setAttribute(ATTR_LOGO, arquivo.getCdarquivo());
				DownloadFileServlet.addCdfile(request.getSession(), arquivo.getCdarquivo());
				return;
			}
		}
		
		request.removeAttribute(ATTR_LOGO);
	}
	
	private void doAtualizacao(HttpServletRequest request){
		if(Neo.getUser() != null){
			if (request.getSession().getAttribute(PARAMETRO_DATAULTIMOACESSO) == null){
				
				String valor = new SimpleDateFormat("dd/MM/yyyy").format(SinedDateUtils.currentDate());
				
				Parametrogeral parametrogeral = ParametrogeralService.getInstance().findByNome(PARAMETRO_DATAULTIMOACESSO);
				if (parametrogeral == null) {
					parametrogeral = new Parametrogeral();
					parametrogeral.setNome(PARAMETRO_DATAULTIMOACESSO);
					parametrogeral.setValor(valor);
					ParametrogeralService.getInstance().saveOrUpdate(parametrogeral);
					this.atualizaTudo(); 
				}
				
				Date data;
				try {
					data = SinedDateUtils.stringToDate(parametrogeral.getValor());
				} catch (ParseException e) { 
					throw new SinedException("N�o foi poss�vel atualizar as tabelas auxiliares."); 
				}
				
				if (SinedDateUtils.beforeIgnoreHour(data, SinedDateUtils.currentDate())) { 
					parametrogeral.setValor(valor);
					ParametrogeralService.getInstance().saveOrUpdate(parametrogeral);
					this.atualizaTudo(); 
				}
				
				if (request.getSession().getAttribute(SUPORTE_CHAT) == null){
					String valorAux =  ParametrogeralService.getInstance().getValorPorNome("SUPORTE_CHAT");
					request.getSession().setAttribute("SUPORTE_CHAT", valorAux);
				}
				
				
				if (request.getSession().getAttribute(EXIBIR_INDICACAO_EMPRESA) == null){
					try {
						request.getSession().setAttribute(EXIBIR_INDICACAO_EMPRESA, ParametrogeralService.getInstance().getValorPorNome(EXIBIR_INDICACAO_EMPRESA));
					} catch (SinedException e) {
						request.getSession().setAttribute(EXIBIR_INDICACAO_EMPRESA, "FALSE");
					}
					try {
						request.getSession().setAttribute(URL_INDICACAO_EMPRESA, ParametrogeralService.getInstance().getValorPorNome(URL_INDICACAO_EMPRESA));
					} catch (SinedException e) {
						request.getSession().setAttribute(URL_INDICACAO_EMPRESA, "");
					}
				}
				
				
				
				
				request.getSession().setAttribute(PARAMETRO_DATAULTIMOACESSO, parametrogeral.getValor());
			}
			
		}
	}
	
	private void atualizaTudo(){
		MaterialService.getInstance().criarContabilEmpresa();
		DocumentoService.getInstance().cancelaDocumentosAtrasados();
		AgendamentoService.getInstance().updateAux();
		DocumentoService.getInstance().atualizaAux();
		ContratoService.getInstance().updateAux();
		ContratoService.getInstance().atualizaDataFimContratos();
		ContratojuridicoService.getInstance().updateTudo();
		TarefaService.getInstance().atualizaSituacaoTodasTarefas();
		TarefaService.getInstance().atualizaDtfimTarefas();
		ApontamentoService.getInstance().verificaAtrasoLancamentoApontamento();	
		OportunidadeService.getInstance().atualizaCicloTodasOportunidade();
		ContratoService.getInstance().atualizaValorContratoFatorAjuste();
//		VeiculoService.getInstance().updateAux();		
		RequisicaoService.getInstance().enviaEmailResponsavelRequisicaoEmEspera();
		AgendamentoservicoService.getInstance().updateAux();	
		MovimentacaoService.getInstance().callProcedureAtualizaMovimentacaoNecessidade();
		ProjetoService.getInstance().atualizaSituacaoProjeto();
		ContratoService.getInstance().enviaEmailResponsavelContratoRenovacao();
		PedidovendaService.getInstance().cancelamentoDiario();
		ContratoService.getInstance().atualizaPeriodoLocacaoAutomatico();
		AvisousuarioService.getInstance().envioEmailAvisoDiario();
		
	}

	private void doVerificaVersao(ServletRequest req, ServletResponse res, HttpServletRequest httpReq) throws Exception {
		VersaoService.getInstance().doVerificaVersao(httpReq);
	}
	
	@SuppressWarnings("unchecked")
	private void addListaEmpresaAttribute(HttpServletRequest request){
		List<Empresa> lista = (List<Empresa>)request.getSession().getAttribute(ATTR_LISTAEMPRESA);
		if(lista == null || lista.isEmpty()){
			lista = new ListSet<Empresa>(Empresa.class);
			Empresa empresa = null;
			lista.add(empresa);
			lista.addAll(EmpresaService.getInstance().findAtivosByUsuario());
			request.getSession().setAttribute(ATTR_LISTAEMPRESA, lista);
		}
		// Sele��o autom�tica da empresa principal ou da primeira que o usu�rio tenha permiss�o.
//		if(request.getSession().getAttribute(ATTR_EMPRESASELECIONADA) == null && lista != null && !lista.isEmpty()){
//			Empresa empresa = lista.get(0);
//			for(Empresa e : lista){
//				if(e.getPrincipal()!= null && e.getPrincipal())
//					empresa = e;
//			}
//			request.getSession().setAttribute(ATTR_EMPRESASELECIONADA, empresa);
//		}
		
		//atributo (ATTR_FIRSTFILTROEMPRESASELECIONADA) criado para caso o usu�rio n�o selecione empresa
		if(request.getSession().getAttribute(ATTR_FIRSTFILTROEMPRESASELECIONADA) == null){
			request.getSession().setAttribute(ATTR_FIRSTFILTROEMPRESASELECIONADA, true);
			List<Empresa> listEmpresas = EmpresaService.getInstance().findAtivosByUsuario();
			if(listEmpresas != null && listEmpresas.size() == 1 && 
					"TRUE".equalsIgnoreCase(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.SELECIONAR_EMPRESA_UNICA_CABECALHO))) {
				request.getSession().setAttribute(ATTR_EMPRESASELECIONADA, listEmpresas.get(0));
			}
		}
	}
}
