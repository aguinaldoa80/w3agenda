package br.com.linkcom.sined.geral.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jasypt.util.password.StrongPasswordEncryptor;

import br.com.linkcom.neo.authorization.AuthenticationControlFilter;
import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.core.config.AuthenticationConfig;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.UsuarioDAO;
import br.com.linkcom.sined.geral.service.RestricaoacessopessoaService;
import br.com.linkcom.sined.geral.service.SessaoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.offline.UsuarioOfflineJSON;
import org.apache.commons.lang.StringUtils;

public class SinedFilter extends AuthenticationControlFilter {
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;       
	    HttpServletResponse httpServletResponse = (HttpServletResponse) response;
	    User user = Neo.getUser();
	    
	    //BUG do Chrome a partir da vers�o 57. (ERR_BLOCKED_BY_XSS_AUDITOR)
	    if (httpServletRequest.getHeader("User-Agent")!=null && httpServletRequest.getHeader("User-Agent").toLowerCase().contains("chrome")){
	    	httpServletResponse.addHeader("X-XSS-Protection", "0");
	    }
	    
	  //definindo parametros globais
		String userAgent = httpServletRequest.getHeader("user-agent").toLowerCase();
		HttpSession session = httpServletRequest.getSession();
		boolean ipad = userAgent.contains("ipad");
		boolean android = userAgent.contains("android");
		boolean ipod = userAgent.contains("ipod");
		boolean iphone = userAgent.contains("iphone");
		boolean chrome = userAgent.contains("chrome") || 
						(android && userAgent.contains("applewebkit") && !userAgent.contains("opera"));
		boolean firefox = userAgent.contains("firefox");
		boolean safari = userAgent.contains("safari");
		boolean windowsAntigo = false;
		
		
		if (userAgent.contains("windows nt ")){
			String strVersao = userAgent.split("windows nt ")[1];
			String auxVersao ="";
			for(int i =0; i < strVersao.length(); i++){
				if((StringUtils. isNumeric(strVersao.charAt(i)+"")) || (strVersao.charAt(i) == '.')){
					auxVersao += strVersao.charAt(i);
				}
				else {
					break;
				}
			}
			try{
				double versao = Double.parseDouble(auxVersao);
				if (versao <= 5.2){
					windowsAntigo = true;
				}
			}catch (Exception e){
				windowsAntigo = false; 
			}		
		}

		boolean iosSemSafari = (ipod || iphone || ipad) && (!safari);
		boolean androidSemChrome = android && (!chrome);
		boolean desktop = !(android || ipod || iphone || ipad);
		boolean browserNaoHomologado =  (!(chrome || firefox)) && desktop; 
		windowsAntigo = windowsAntigo && desktop;	
		
		session.setAttribute("ipad", ipad);
		session.setAttribute("android", android);
		session.setAttribute("ipod", ipod);
		session.setAttribute("iphone", iphone);
		session.setAttribute("ios", (ipod || iphone || ipad) );
		session.setAttribute("userAgent", userAgent );
		session.setAttribute("browserNaoHomologado",browserNaoHomologado);
		session.setAttribute("windowsAntigo", windowsAntigo);
		session.setAttribute("iosSemSafari", iosSemSafari);
		session.setAttribute("androidSemChrome", androidSemChrome);
		session.setAttribute("exibeAviso", windowsAntigo || browserNaoHomologado || iosSemSafari || androidSemChrome);
		
	    String param = httpServletRequest.getParameter("originatorRequest");
	    //verificando se a URL for do m�dulo offline, n�o colocar ela na sessao
	    if(param != null && !param.equals("") && !param.contains("offline") 
	    		&& httpServletRequest.getRequestURI().indexOf("offline")==-1){
	    	httpServletRequest.getSession().setAttribute("originator", param);
	    }
	    
		if (user != null && user instanceof Fornecedor) {
			if (httpServletRequest.getRequestURI().indexOf("/fornecedor/crud/Cotacaofornecedor") == -1) {
				String attribute = httpServletRequest.getContextPath()+"/fornecedor/crud/Cotacaofornecedor";
				httpServletResponse.sendRedirect(attribute);
				return;
			} else {
				chain.doFilter(request, response);
			}
		} else {
			if (httpServletRequest.getRequestURI().indexOf("/fornecedor/crud/Cotacaofornecedor") == -1) {
				try {
					if(user != null && httpServletRequest.getHeader("Referer") != null && 
							httpServletRequest.getHeader("Referer").endsWith("/pub/Login")){
						Usuario usuario = (Usuario) user;
						if(usuario != null && usuario.getSemprelogaroffline() != null && usuario.getSemprelogaroffline()){
							request.setAttribute("semprelogarofflineRedirect", true);
						}
					}
				} catch (Exception e) {}
				super.doFilter(request, response, chain);
			} else {
				if (user == null) {
					super.doFilter(request, response, chain);
				} else {
					dispachNoPermission(httpServletResponse);
				}
			}
		}
	}
	
	@Override
	public void doAuth(HttpServletRequest request,HttpServletResponse response, AuthenticationConfig config) throws IOException, ServletException {
		
		boolean loginAutomatico = "true".equals(NeoWeb.getRequestContext().getParameter("loginAutomatico"));
		
		if(loginAutomatico){
			String urlDestino = NeoWeb.getRequestContext().getParameter("urlDestino");
			if(urlDestino != null && !urlDestino.equals("")){
				NeoWeb.getRequestContext().getSession().setAttribute("originator", urlDestino);
			}
		} else {
			String requestURI = request.getRequestURI();
			String queryString = request.getQueryString() != null ? "?" + request.getQueryString() : "";
			String contextPath = request.getContextPath() != null ? request.getContextPath() : "";
			String originator = contextPath + requestURI.substring(contextPath.length()) + queryString;
			
			request.getSession().setAttribute("originator",originator);
			request.setAttribute("originatorRequest", originator);
		}
		
		User user = NeoWeb.getRequestContext().getUser();
		if (user == null) {
			dispachError(request, response,config,false);
		}
	}
	
	@Override
	public void dispachError(HttpServletRequest request, HttpServletResponse response, AuthenticationConfig config, Boolean error) throws IOException, ServletException{
    	if(error) request.setAttribute("login_error",true);
    	if(error) request.getSession().setAttribute("login_error", true);
    	if(request.getAttribute("login_bloqueado")!=null) request.getSession().setAttribute("login_bloqueado", true);
    	if(request.getAttribute("login_restrito_horario")!=null) request.getSession().setAttribute("login_restrito_horario", true);
    	if(request.getAttribute("login_restrito_dia")!=null) request.getSession().setAttribute("login_restrito_dia", true);
    	if (request.getParameter("tdrive")!=null && request.getParameter("tdrive").equals("1")){
    		if(error || request.getAttribute("login_bloqueado")!=null || request.getAttribute("login_restrito_horario")!=null || request.getAttribute("login_restrito_dia")!=null)
    			request.getRequestDispatcher("/jsp/login.jsp" + "?error=1").forward(request, response);
    		else request.getRequestDispatcher("/jsp/login.jsp").forward(request, response); 
    	}else {
    		//A URL da p�gina de login quando tem erro tem de ser diferente para exibir o erro de login
    		if(error || request.getAttribute("login_bloqueado")!=null || request.getAttribute("login_restrito_horario")!=null || request.getAttribute("login_restrito_dia")!=null)
    			response.sendRedirect(config.getLoginPage() + "?error=1");
    		else response.sendRedirect(config.getLoginPage());    		
    	}
    }
	
	
	@Override
	protected boolean verifyPassword(User user, String password) {
    	boolean passwordMatch = false;
		boolean updatePassword = false;
		
		StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
		
		// se o usu�rio existe e a senha foi passada
		if (user != null && user.getPassword() != null) {
			boolean loginAutomatico = "true".equals(NeoWeb.getRequestContext().getParameter("loginAutomatico"));
			
			if(loginAutomatico){
				String urlDestino = NeoWeb.getRequestContext().getParameter("urlDestino");
				if(urlDestino != null && !urlDestino.equals("")){
					NeoWeb.getRequestContext().getSession().setAttribute("originator", urlDestino);
				}
			}
			
			if(!loginAutomatico && user.getPassword().matches("[a-zA-Z0-9\\+/]{64}")) {
				// Jasypt Strong Encryption
				updatePassword = true;
				if (passwordEncryptor.checkPassword(password, user.getPassword())) {
					passwordMatch = true;
				}
			}
			else if(!loginAutomatico && user.getPassword().matches("[0-9a-f]{32}")) {
				// MD5 simples
				if (Util.crypto.makeHashMd5(password).equals(user.getPassword())) {
					passwordMatch = true;
				}
			}
			else {
				// Senha pura
				if(!loginAutomatico) updatePassword = true;
				if (user.getPassword().equals(password)) {
					passwordMatch = true;
				}
			}
		}

		if (passwordMatch) {
			String hashSenha = Util.crypto.makeHashMd5(password);
			if(updatePassword) {
				updatePasswordOnDataBase(user, hashSenha);
			}
		}
			
		return passwordMatch;
	}
	
	@Override
	public User updateUserInfo(User user, HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException{

		if (user instanceof Usuario){				
			Usuario usuario = UsuarioDAO.getInstance().load((Usuario)user);

			// N�o permite acessar o sistema se o usu�rio estiver bloqueado
			if (usuario.getBloqueado().equals(true)) {					
				request.setAttribute("login_bloqueado", true);
				return null;					
			}else if (RestricaoacessopessoaService.getInstance().isPossuiRestricaoAcessoHorario(usuario)){
				request.setAttribute("login_restrito_horario", true);
				return null;
			}else if (RestricaoacessopessoaService.getInstance().isPossuiRestricaoAcessoDia(usuario)){
				request.setAttribute("login_restrito_dia", true);
				return null;
			}else{
				usuario = UsuarioService.getInstance().carregaUsuario(usuario);
				user = usuario;
				// Cria o log de registro do login.
				SessaoService.getInstance().makeLogin(usuario);
				if(SinedUtil.isUserHasAction(usuario, "PEDIDO_VENDA_OFFLINE")){
					UsuarioOfflineJSON userOff = new UsuarioOfflineJSON();
					userOff.setLogin(usuario.getLogin());
					userOff.setHash( Util.crypto.makeHashMd5(usuario.getLogin()+request.getParameter("password")) );
					userOff.setCdpessoa(usuario.getCdpessoa());
					userOff.setCodigo(usuario.getCdpessoa().toString());
					userOff.setNome(usuario.getNome().toString());
					userOff.setRestricaoclientevendedor(usuario.getRestricaoclientevendedor());
					userOff.setTodosprojetos(usuario.getTodosprojetos());
					userOff.setSemprelogaroffline(usuario.getSemprelogaroffline() != null ? 
							usuario.getSemprelogaroffline() : false);
					request.getSession().setAttribute("usuarioOffline", View.convertToJson(userOff));
				}
			}				
		}
		
		return user;
	}
	
	@Override
	public void updatePasswordOnDataBase(User user, String hashSenha) {
		if (user instanceof Usuario) {
			Usuario usuario = (Usuario) user;
			usuario.setSenha(hashSenha);
			UsuarioService.getInstance().alterarSenhaUsuario(usuario,false);			
		}
	}
	
	@Override
	public boolean doLogin() {
		return true;
	}
}
