/*
 * Neo Framework http://www.neoframework.org
 * Copyright (C) 2007 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package br.com.linkcom.sined.geral.filter;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.authorization.AuthorizationDAO;
import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.DefaultWebRequestContext;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.util.LocaleUtils;
import br.com.linkcom.neo.view.SelecionarCadastrarServlet;
import br.com.linkcom.neo.view.menu.MenuTag;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.util.rest.CriptografiaUtil;

public class NeoFilter implements Filter {

	public void init(FilterConfig config) throws ServletException {
		
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		try{
			long inicio = System.currentTimeMillis();
	
			HttpServletRequest request = (HttpServletRequest) req;
			HttpServletResponse response = (HttpServletResponse) res;
			
			
			request.setAttribute("ctx", request.getContextPath());
			
			//cria o contexto de requisicao NEO
			NeoWeb.createRequestContext(request, response);
			
			if(SinedUtil.isAmbienteDesenvolvimento() && NeoWeb.getUser() == null){
				// Tempor�rio para desenvolvimento IZAP
				String token = request.getHeader("X-W3erp-Auth-Token");
				if(StringUtils.isNotBlank(token)){
					AuthorizationDAO authorizationDAO = (AuthorizationDAO) Neo.getApplicationContext().getBean("authorizationDAO");
					User user = authorizationDAO.findUserByLogin(CriptografiaUtil.decriptografar(token));
					if(user != null){
						user = UsuarioService.getInstance().carregaUsuario((Usuario)user);
						DefaultWebRequestContext requestContext = (DefaultWebRequestContext) NeoWeb.getRequestContext();
						requestContext.setUser(user);
					}
				}
			}
			
			String requestURI = request.getRequestURI();
			request.setAttribute("uriAtalho", requestURI);
			
			if(isLog(requestURI, request.getContextPath())){ 
				logRequisicao(inicio, true, request);
			}
			
			//I18N
			String locale = request.getParameter("locale");
			if (locale != null){
				request.getSession().setAttribute(LocaleUtils.LOCALE_SESSION_KEY, locale);
				request.getSession().setAttribute(MenuTag.MENU_CACHE_MAP, null);
			}
			
			//colocar um flag na requisi��o indicando que esta � uma p�gina selectone ou cadastrar
			String parameter = request.getParameter(SelecionarCadastrarServlet.INSELECTONE);
			if("true".equals(parameter)){
				request.setAttribute(SelecionarCadastrarServlet.INSELECTONE, true);
			}
			
			if(canMakeCache(requestURI)){
				response.addHeader("pragma", "no-cache");
				response.addHeader("cache-control", "no-cache");
				response.addHeader("expires", "0");
			}
			
			if (requestURI.matches("/.+?/.+?/.*")) {
				request.setAttribute("NEO_MODULO", requestURI.split("/")[2]);
			}
			
			chain.doFilter(request, response);
 
			if(isLog(requestURI, request.getContextPath())){
				logRequisicao(inicio, false, request);
			}
		}finally{
			//Removendo o contexto do ThreadLocal, sen�o haver� vazamento de mem�ria.
			Neo.removeRequestContext();
			Neo.removeApplicationContext();
		}
	}
	
	private static boolean isLog(String requestURI, String context){
		return !requestURI.startsWith(context + "/css/") &&
					!requestURI.startsWith(context + "/resource/") &&
					!requestURI.startsWith(context + "/imagens/") &&
					!requestURI.startsWith(context + "/graniteamf/") &&
					!requestURI.startsWith(context + "/fontawesome/") &&
					!requestURI.startsWith(context + "/js/") &&
					!requestURI.startsWith(context + "/flex/") &&
					!requestURI.startsWith(context + "/font/") &&
					!requestURI.startsWith(context + "/ajax/autocomplete") &&
					!requestURI.startsWith(context + "/DOWNLOADFILE/") &&
					!requestURI.startsWith(context + "/pub/ValidateSession") &&
					!requestURI.startsWith(context + "/pub/process/Curriculocandidato") &&
					!requestURI.startsWith(context + "/jws/libs/");
	}
	
	
	/**
	 * Gera o log no formato <timestamp>; <servidor>; <ip do usu�rio>; <login do usu�rio>; <id da sessao> ; <dom�nio do w3>; <URI>; <tempo do request>;
	 * @param timestampInicio
	 * @param request
	 */
	private void logRequisicao(long timestampInicio, boolean inicio, HttpServletRequest request) {
		String nomeMaquina = "Desconhecida.";
		try {  
		    InetAddress localaddr = InetAddress.getLocalHost();  
		    nomeMaquina = localaddr.getHostName();  
		} catch (UnknownHostException e3) {}
		
//		String url = request.getRequestURL().toString();
//		String[] urlDividida = url.split("/");
		//String servidor = urlDividida != null && urlDividida.length > 2 ? "(" + urlDividida[2] + ")" : "";

		
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
		    ipAddress = request.getRemoteAddr();
		}
		
		String login = "Indefinido";
		try{login = NeoWeb.getUser().getLogin();}catch(Exception e){}

		String idSessao = "";
		try{idSessao=request.getSession().getId();}catch(Exception e){}
		String dominio = SinedUtil.getSubdominioCliente(request);
		String requestURI = request.getRequestURI();
		
		String acao = getAcao(request);
		if(acao == null) acao = "";

		long fim = System.currentTimeMillis();
		long tempo = (fim - timestampInicio);
		String referer = request.getHeader("referer");
		
		StringBuilder linhaLog = new StringBuilder();
		linhaLog
		.append(timestampInicio).append("; ")
		.append(nomeMaquina).append("; ")
		.append(inicio ? "inicio" : "fim").append("; ")
		.append(ipAddress).append("; ")
		.append(login).append("; ")
		.append(idSessao).append("; ")
		.append(dominio).append("; ")
		.append(requestURI).append("; ")
		.append(referer).append("; ")
		.append(tempo).append("; ")
		.append(acao).append(";")
		;
		//<timestamp>; <servidor>; <inicio|fim>; <ip do usu�rio>; <login do usu�rio>; <id da sessao> ; <dom�nio do w3>; <URI>; <REFERER>; <tempo do request>; <ACAO>;
		System.out.println(linhaLog.toString());
	}
	
	public void destroy() {
	}
	
	/**
	 * Verifica se a requisi��o � imagem ou css ou js. Caso seja algum desses, ser� feito cache, caso contr�rio
	 * as requisi��es obrigatoriamente n�o podem sofrer efeito de cache.
	 * 
	 * @param url
	 * @return true - Caso a requisi��o n�o seja de imagem.
	 */
	private boolean canMakeCache(String url){
		String [] ext = new String[]{"gif","jpg","png","js","bmp","jpeg","png","css","pdf","woff2","woff","ttf","eot","svg"};
		if(url == null)
			return false;
		else{
			url = url.toLowerCase();
			for (String string : ext) {
				if(url.endsWith(string))
					return false;
			}
			return true;
		}
	}
	
	private String getAcao(HttpServletRequest request) {
		String param = request.getParameter("ACAO");
		
		if (param!=null && !param.trim().equals("")){
			return param;
		}else {
			//ACAO salvar
			Object attr = request.getAttribute("ACAO");
			if (attr!=null && !attr.toString().trim().equals("")){
				return attr.toString();
			}
		}
		
		return null;
	}
}