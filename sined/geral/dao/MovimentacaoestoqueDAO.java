package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Localarmazenagemempresa;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueOrigemEnum;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoLancamentoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.OrigemMovimentacaoEstoqueEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoitemsped;
import br.com.linkcom.sined.geral.service.LocalarmazenagemempresaService;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueorigemService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.MateriaprimaBean;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConsultarEstoqueBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.ConsultarEstoqueWSBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.TotalizadorEntradaSaida;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.DinamicaRebanhoFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MovimentacaoestoqueFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.report.bean.DinamicaRebanhoBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

public class MovimentacaoestoqueDAO extends GenericDAO<Movimentacaoestoque> {
	private LocalarmazenagemempresaService localarmazenagemempresaService;
	private MovimentacaoestoqueorigemService movimentacaoestoqueorigemService;
	private MaterialcategoriaService materialcategoriaService;
	
	public void setLocalarmazenagemempresaService(LocalarmazenagemempresaService localarmazenagemempresaService) {
		this.localarmazenagemempresaService = localarmazenagemempresaService;
	}
	public void setMovimentacaoestoqueorigemService(MovimentacaoestoqueorigemService movimentacaoestoqueorigemService) {
		this.movimentacaoestoqueorigemService = movimentacaoestoqueorigemService;
	}
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {
		this.materialcategoriaService = materialcategoriaService;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Movimentacaoestoque> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		MovimentacaoestoqueFiltro filtro = (MovimentacaoestoqueFiltro) _filtro;
		MovimentacaoEstoqueOrigemEnum origem = filtro.getOrigem();
		
		query
			.select("distinct movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.dtmovimentacao, movimentacaoestoque.qtde, " +
					"movimentacaoestoque.dtcancelamento, material.cdmaterial, material.nome, material.identificacao, tipo.nome, tipo.cdmovimentacaoestoquetipo, " +
					"local.nome, movimentacaoestoque.valor, loteestoque.numero, loteestoque.validade, unidademedida.nome," +
					"materialmestregrade.cdmaterial, materialmestregrade.identificacao, movimentacaoestoque.movimentacaoanimalmotivo, " +
					"materialgrupo.cdmaterialgrupo, materialgrupo.nome, materialgrupo.animal," +
					"rateio.cdrateio, loteestoque.cdloteestoque, loteestoque.validade, loteestoque.numero, " +
					"listaRateioitem.cdrateioitem,listaRateioitem.percentual,listaRateioitem.valor,listaRateioitem.observacao," +
					"projetoRateio.cdprojeto,projetoRateio.nome," +
					"centroCusto.cdcentrocusto,centroCusto.nome," +
					"contaGerencial.cdcontagerencial,contaGerencial.nome")
			.join("movimentacaoestoque.movimentacaoestoquetipo tipo")
			.join("movimentacaoestoque.material material")
			.leftOuterJoin("material.materialmestregrade materialmestregrade")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("movimentacaoestoque.localarmazenagem local")
			.leftOuterJoin("movimentacaoestoque.rateio rateio")
			.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
			.leftOuterJoin("listaRateioitem.projeto projetoRateio")
			.leftOuterJoin("listaRateioitem.centrocusto centroCusto")
			.leftOuterJoin("listaRateioitem.contagerencial contaGerencial")
			.leftOuterJoin("movimentacaoestoque.loteestoque loteestoque")
			.leftOuterJoin("movimentacaoestoque.projeto projeto")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.where("movimentacaoestoque.cdmovimentacaoestoque = ?", filtro.getCdmoviemtacaestoque())
			.where("movimentacaoestoque.dtmovimentacao >= ?", filtro.getDatade())
			.where("movimentacaoestoque.dtmovimentacao <= ?", filtro.getDataate())
			.where("coalesce(movimentacaoestoque.valor, 0) >= ?", filtro.getValorinicial())
			.where("coalesce(movimentacaoestoque.valor, 0) <= ?", filtro.getValorfinal())		
			.where("movimentacaoestoque.empresa = ?", filtro.getEmpresa())
			.where("local = ?", filtro.getLocalarmazenagem())
			.where("material.materialgrupo = ?", filtro.getMaterialgrupo())
			.where("material.materialtipo = ?", filtro.getMaterialtipo())
			.where("movimentacaoestoque.projeto = ?", filtro.getProjeto())
			.where("projeto.projetotipo = ?", filtro.getProjetotipo())
			.where("loteestoque = ?", filtro.getLoteestoque())
			.where("movimentacaoestoque.movimentacaoanimalmotivo = ?", filtro.getMovimentacaoanimalmotivo());
		
		if(filtro.getMaterialcategoria() != null){
			materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
			query
				.leftOuterJoin("material.materialcategoria materialcategoria")
				.leftOuterJoin("materialcategoria.vmaterialcategoria vmaterialcategoria")
				.openParentheses()
					.where("vmaterialcategoria.identificador like '"+ filtro.getMaterialcategoria().getIdentificador() +".%'")
				.or()
					.where("vmaterialcategoria.identificador like ? ", filtro.getMaterialcategoria().getIdentificador())
				.closeParentheses();
		}
		
		String whereInEmpresaUsuarioLogado = new SinedUtil().getListaEmpresa();
		if(whereInEmpresaUsuarioLogado != null){
			query
				.leftOuterJoin("local.listaempresa listaempresa")
				.leftOuterJoin("listaempresa.empresa empresa")
				.openParentheses()
					.whereIn("empresa.cdpessoa", whereInEmpresaUsuarioLogado)
					.or()
					.where("listaempresa is null")
				.closeParentheses()
				.ignoreJoin("listaempresa", "empresa");
		}
		
		//quando deve ser feito o join de movimentacaoestoqueorigem
		if(filtro.getFornecedor()!=null || filtro.getCliente()!=null || origem!=null){
			query.leftOuterJoin("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem");
		}
		
		//quando deve ser feito o join de venda
		if(filtro.getFornecedor()!=null || filtro.getCliente()!=null || MovimentacaoEstoqueOrigemEnum.VENDA.equals(origem)){
			query.leftOuterJoin("movimentacaoestoqueorigem.venda venda");
		}
		
		if(MovimentacaoEstoqueOrigemEnum.PEDIDOVENDA.equals(origem)){
			query.leftOuterJoin("movimentacaoestoqueorigem.pedidovenda pedidovenda");
		}
		
		if(MovimentacaoEstoqueOrigemEnum.INVENTARIO.equals(origem)){
			query.leftOuterJoin("movimentacaoestoqueorigem.inventario inventario");
		}
		
		if(filtro.getFornecedor()!=null || MovimentacaoEstoqueOrigemEnum.COMPRA.equals(origem)){
			query.leftOuterJoin("movimentacaoestoqueorigem.entrega entrega");
		}
		
		if(filtro.getFornecedor()!=null || filtro.getCliente()!=null){
			if(filtro.getFornecedor()!=null){
				query.leftOuterJoin("entrega.listaEntregadocumento entregadocumento")
				.ignoreJoin("listaEntregadocumento");
			}
			
			
			query
			.openParentheses()
				.where("venda.cliente=?", filtro.getCliente())
				.or()
				.where("venda.terceiro=?", filtro.getFornecedor());
				if(filtro.getFornecedor()!=null){ //verificacao para nao ocorrer erro por falta do join de entregadocumento
					query.or()
					.where("entregadocumento.fornecedor=?", filtro.getFornecedor());
				}
			query.closeParentheses();
			

		}
		
		if(filtro.getWhereInItemgrade() != null && !"".equals(filtro.getWhereInItemgrade())){
			query.whereIn("material.cdmaterial", filtro.getWhereInItemgrade() + 
					(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null ? 
							"," + filtro.getMaterial().getCdmaterial() : ""));
		}else {
			query.where("material = ?", filtro.getMaterial());
		}
		
		if(filtro.getListatipos() != null && filtro.getListatipos().size() > 0){
			query.whereIn("tipo", CollectionsUtil.listAndConcatenate(filtro.getListatipos(), "cdmovimentacaoestoquetipo",","));
		}
		
		if(filtro.getCanceladas() == null || !filtro.getCanceladas()){
			query.where("movimentacaoestoque.dtcancelamento is null");
		}
		
		if (filtro.getProduto() || filtro.getServico() || filtro.getEpi()) {
			query.openParentheses();
			if (filtro.getProduto()) {
				query.where("movimentacaoestoque.materialclasse = ?", Materialclasse.PRODUTO);
			}
			if(filtro.getServico()){
				if (filtro.getProduto()) {
					query.or();
				}
				query.where("movimentacaoestoque.materialclasse = ?", Materialclasse.SERVICO);
			}
			if (filtro.getEpi()) {
				if (filtro.getProduto() || filtro.getServico()) {
					query.or();
				}
				query.where("movimentacaoestoque.materialclasse = ?", Materialclasse.EPI);
			}
			query.closeParentheses();
		}
		
		if(origem!=null){
			
			if (MovimentacaoEstoqueOrigemEnum.COMPRA.equals(origem)){
				query.where("entrega is not null")
				.where("entrega.cdentrega = ?", filtro.getIdorigem());
			
			}else if (MovimentacaoEstoqueOrigemEnum.CONSUMO.equals(origem)){
				
				query.where("movimentacaoestoqueorigem.consumo=?", Boolean.TRUE);
			
			}else if (MovimentacaoEstoqueOrigemEnum.ROMANEIO.equals(origem)){
				query.leftOuterJoin("movimentacaoestoqueorigem.romaneio romaneio")
				.where("romaneio is not null")
				.where("romaneio.cdromaneio = ?", filtro.getIdorigem());
			
			}else if (MovimentacaoEstoqueOrigemEnum.ROMANEIO_MANUAL.equals(origem)){
				query.leftOuterJoin("movimentacaoestoqueorigem.romaneio romaneio")
				.where("movimentacaoestoqueorigem.romaneiomanual=?", Boolean.TRUE)
				.where("romaneio.cdromaneio = ?", filtro.getIdorigem());
			
			}else if (MovimentacaoEstoqueOrigemEnum.INVENTARIO.equals(origem)){
				query.where("inventario is not null");			
				query.where("inventario.cdinventario = ?", filtro.getIdorigem());
			}else if (MovimentacaoEstoqueOrigemEnum.VENDA.equals(origem)){
				query.where("venda is not null");			
				query.where("venda.cdvenda = ?", filtro.getIdorigem());
			
			}else if (MovimentacaoEstoqueOrigemEnum.PEDIDOVENDA.equals(origem)){
				query.where("pedidovenda is not null");			
				query.where("pedidovenda.cdpedidovenda = ?", filtro.getIdorigem());
			
			}else if (MovimentacaoEstoqueOrigemEnum.NOTA_SAIDA.equals(origem)){
				query.leftOuterJoin("movimentacaoestoqueorigem.notafiscalproduto notafiscalproduto")
				.where("notafiscalproduto is not null")			
				.where("notafiscalproduto.cdNota = ?", filtro.getIdorigem());
			}else if (MovimentacaoEstoqueOrigemEnum.PRODUCAOAGENDA.equals(origem)){
				query.leftOuterJoin("movimentacaoestoqueorigem.producaoagenda producaoagenda")
					 .leftOuterJoin("movimentacaoestoqueorigem.producaoordem producaoordem")
					 .openParentheses()
					 	.where("producaoagenda is not null")
					 	.or()
					 	.where("producaoordem is not null")
					 .closeParentheses()
					 .openParentheses()
					 	.where("producaoagenda.cdproducaoagenda = ?", filtro.getIdorigem())
					 	.or()
					 	.where("producaoordem.cdproducaoordem = ?", filtro.getIdorigem())
					 .closeParentheses();
			} else if(MovimentacaoEstoqueOrigemEnum.AJUSTE_AUTOMATICO_WMS.equals(origem)){
				query.where("movimentacaoestoqueorigem.ajusteautomaticowms is true");
			} else if(MovimentacaoEstoqueOrigemEnum.AJUSTE_MANUAL_WMS.equals(origem)){
				query.where("movimentacaoestoqueorigem.ajustemanualwms is true");
			}
		}
		
		
		query.orderBy("movimentacaoestoque.cdmovimentacaoestoque desc");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Movimentacaoestoque> query) {
		query
			.joinFetch("movimentacaoestoque.material material")
			.leftOuterJoinFetch("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
			.leftOuterJoinFetch("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
			.leftOuterJoinFetch("movimentacaoestoque.loteestoque loteestoque")
			.leftOuterJoinFetch("movimentacaoestoque.localarmazenagem localarmazenagem")
			.leftOuterJoinFetch("movimentacaoestoque.materialclasse materialclasse")
//			.leftOuterJoinFetch("movimentacaoestoqueorigem.coleta")
			.leftOuterJoinFetch("movimentacaoestoqueorigem.usuario")
			.leftOuterJoinFetch("movimentacaoestoqueorigem.entrega")
			.leftOuterJoinFetch("movimentacaoestoqueorigem.inventario")
			.leftOuterJoinFetch("movimentacaoestoqueorigem.requisicaomaterial")
			.leftOuterJoinFetch("movimentacaoestoqueorigem.ordemservicoveterinaria")
			.leftOuterJoinFetch("movimentacaoestoqueorigem.veiculodespesa veiculodespesa");
	}
	
	/** 
	 * M�todo que busca movimenta��es para cancelar
	 * 
	 * @param whereIn
	 * @author Tom�s Rabelo
	 */
	public List<Movimentacaoestoque> findMovimentacoes(String whereIn) {
		if (whereIn == null || whereIn.equals(""))
			throw new SinedException("Nenhum item selecionado.");
		
		return query()
			.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.dtmovimentacao, movimentacaoestoque.dtcancelamento, " +
					"vendasituacao.cdvendasituacao, inventario.cdinventario, inventario.inventarioSituacao, projeto.cdprojeto, empresa.cdpessoa, localarmazenagem.cdlocalarmazenagem")
			.leftOuterJoin("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
			.leftOuterJoin("movimentacaoestoqueorigem.venda venda")
			.leftOuterJoin("venda.vendasituacao vendasituacao")
			.leftOuterJoin("movimentacaoestoqueorigem.inventario inventario")
			.leftOuterJoin("movimentacaoestoque.projeto projeto")
			.leftOuterJoin("movimentacaoestoque.empresa empresa")
			.leftOuterJoin("movimentacaoestoque.localarmazenagem localarmazenagem")
			.whereIn("movimentacaoestoque.cdmovimentacaoestoque", whereIn)
			.list();
	}
	
	public List<Movimentacaoestoque> findMovimentacoesForQtdedisponivel(String whereIn) {
		if (whereIn == null || whereIn.equals(""))
			throw new SinedException("Nenhum item selecionado.");
		
		return query()
				.select("movimentacaoestoque.cdmovimentacaoestoque, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
						"movimentacaoestoque.qtde, materialclasse.cdmaterialclasse, material.cdmaterial, empresa.cdpessoa, loteestoque.cdloteestoque, " +
						"movimentacaoestoquetipo.cdmovimentacaoestoquetipo, projeto.cdprojeto,movimentacaoestoque.dtcancelamento")
				.leftOuterJoin("movimentacaoestoque.localarmazenagem localarmazenagem")
				.leftOuterJoin("movimentacaoestoque.materialclasse materialclasse")
				.leftOuterJoin("movimentacaoestoque.material material")
				.leftOuterJoin("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
				.leftOuterJoin("movimentacaoestoque.empresa empresa")
				.leftOuterJoin("movimentacaoestoque.projeto projeto")
				.leftOuterJoin("movimentacaoestoque.loteestoque loteestoque")
				.whereIn("movimentacaoestoque.cdmovimentacaoestoque", whereIn)
				.list();
	}
	
	/**
	 * M�todo que busca as movimentacoes da entreda
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Movimentacaoestoque> findMovimentacoesEntrega(String whereIn) {
		if (whereIn == null || whereIn.equals(""))
			throw new SinedException("Nenhum item selecionado.");
		
		return query()
			.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.dtcancelamento, entrega.cdentrega, entrega.dtbaixa ")
			.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
			.join("movimentacaoestoqueorigem.entrega entrega")
			.whereIn("movimentacaoestoque.cdmovimentacaoestoque", whereIn)
			.list();
	}
	
	/**
	 * Busca todas as movimenta��es n�o canceladas relacionadas a uma entregamaterial.
	 * @param entregamaterial
	 * @return
	 */
	public List<Movimentacaoestoque> findMovimentacoesNaoCanceladasPorEntrega(Entregamaterial entregamaterial) {
		if(entregamaterial == null || entregamaterial.getCdentregamaterial() == null)
			throw new IllegalArgumentException("o par�metro entregamaterial n�o pode ser nulo.");
		return query().select("movimentacaoestoque.cdmovimentacaoestoque, entregamaterial.cdentregamaterial ")
			.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
			.join("movimentacaoestoqueorigem.entregamaterial entregamaterial")
			.where("entregamaterial = ?", entregamaterial)
			.where("movimentacaoestoque.dtcancelamento is null")
			.list();
	}

	/**
	 * M�todo que cancela movimenta��es
	 * 
	 * @param whereIn
	 * @author Tom�s Rabelo
	 */
	public void doUpdateMovimentacoes(String whereIn) {
		if (whereIn == null || whereIn.equals(""))
			throw new SinedException("Nenhum item selecionado.");
		
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioLogado != null){
			Integer cdusuarioaltera = usuarioLogado.getCdpessoa();
			
			String sql = "update Movimentacaoestoque m set m.cdusuarioaltera = ?, m.dtaltera = ?, m.dtcancelamento = ? " +
						 "where m.cdmovimentacaoestoque in ("+whereIn+")";
			
			getHibernateTemplate().bulkUpdate(sql.toString(), new Object[]{cdusuarioaltera,hoje, hoje});
			
		} else {
			String sql = "update Movimentacaoestoque m set m.dtcancelamento = ? where m.cdmovimentacaoestoque in ("+whereIn+")";

			getHibernateTemplate().bulkUpdate(sql.toString(), new Object[]{hoje});
		}
		
		
	}
	
	/**
	 * Busca quantidade de saida que houve do material referente a requisi��o de material
	 * @param requisicaomaterial
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Long getQuantidadeEntregueDaRequisicaoMaterial(Requisicaomaterial requisicaomaterial) {
		Double result = newQueryBuilderSined(Double.class)
			.select("sum(movimentacaoestoque.qtde)")
			.from(Movimentacaoestoque.class)
			.setUseTranslator(false)
			.join("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
			.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
			.join("movimentacaoestoqueorigem.requisicaomaterial requisicaomaterial")
			.where("requisicaomaterial = ?", requisicaomaterial)
			.where("movimentacaoestoquetipo = ?", Movimentacaoestoquetipo.SAIDA)
			.where("movimentacaoestoque.dtcancelamento is null")
			.unique();
		
		if(result != null)
			return result.longValue();
		return null;
	}
	
	public Double getQtdDisponivelProdutoEpiServico(Material material,	Materialclasse materialclasse, Localarmazenagem localarmazenagem, Empresa empresa, Projeto projeto, Boolean naoconsiderargrade, Loteestoque loteestoque, Boolean naoConsiderarLote) {
		return getQtdDisponivelProdutoEpiServico(material, materialclasse, localarmazenagem, empresa, projeto, naoconsiderargrade, loteestoque, naoConsiderarLote, null, false, true);
	}

	/**
	 * M�todo que busca quantidade de material dispon�vel de acordo com sua classe e local de armazenagem.
	 * 
	 * @param material
	 * @param materialclasse
	 * @param localarmazenagem
	 * @return
	 * @author Tom�s T. Rabelo
	 * @param naoconsiderargrade 
	 */
	@SuppressWarnings("unchecked")
	public Double getQtdDisponivelProdutoEpiServico(Material material,	Materialclasse materialclasse, Localarmazenagem localarmazenagem, Empresa empresa, Projeto projeto, Boolean naoconsiderargrade, Loteestoque loteestoque, Boolean naoConsiderarLote, Date dtestoque, Boolean todosLocaisQuandoLocalNulo, Boolean trazerMovimentacaoSemEmpresa) {
		StringBuilder sql = new StringBuilder()
			.append("select material_disponivel_local("+material.getCdmaterial());

		if(materialclasse.getCdmaterialclasse().equals(Materialclasse.EPI.getCdmaterialclasse())){
			sql.append(",'EP',");
		} else if(materialclasse.getCdmaterialclasse().equals(Materialclasse.PRODUTO.getCdmaterialclasse())){
			sql.append(",'PO',");
		} else if(materialclasse.getCdmaterialclasse().equals(Materialclasse.SERVICO.getCdmaterialclasse())){
			sql.append(",'SE',");
		}else {
			return 0d;
		}
		
		if(localarmazenagem != null){
			sql.append(localarmazenagem.getCdlocalarmazenagem() + ", ");
		} else {
			sql.append("null, ");
		}
		
		if(empresa != null){
			sql.append(empresa.getCdpessoa() + ", ");
		} else {
			sql.append("null, ");
		}
		
		if(projeto != null){
			sql.append(projeto.getCdprojeto() + ", ");
		} else {
			sql.append("null, ");
		}
		
		sql.append(naoconsiderargrade != null ? naoconsiderargrade.toString() : "false")
		.append(", ");
		
		if(loteestoque != null && loteestoque.getCdloteestoque() != null){
			sql.append(loteestoque.getCdloteestoque());
		}else{
			sql.append("null");
		}
		
		sql.append(", ");
		sql.append(naoConsiderarLote != null ? naoConsiderarLote.toString() : "false");
		sql.append(", ");
		
		if(dtestoque != null){
			sql.append(SinedDateUtils.toStringPostgre(dtestoque));
		}else{
			sql.append("null");
		}
		
		sql.append(", ");
		sql.append(todosLocaisQuandoLocalNulo != null ? todosLocaisQuandoLocalNulo.toString() : "false");
		sql.append(", ");
		sql.append(trazerMovimentacaoSemEmpresa != null ? trazerMovimentacaoSemEmpresa.toString() : "false");
		
		sql.append(")");
		
		Double disponivel = 0.0;
//		Long disponivel = getJdbcTemplate().queryForLong(sql.toString());
		SinedUtil.markAsReader();
		List<Double> lista = null;
		try {
			lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Double(rs.getDouble("material_disponivel_local"));
				}
			});			
		} finally {
			SinedUtil.desmarkAsReader();
		}
		
		if(lista != null && !lista.isEmpty())
			disponivel = lista.get(0);
		
		return disponivel;
	}

	public List<Movimentacaoestoque> getListaEntradaDaEntrega(Entrega entrega) {
		return query()
			.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.qtde, loteestoque.cdloteestoque, loteestoque.numero, " +
					"material.cdmaterial, material.nome, materialLote.cdmaterial, listaLotematerial.validade")
			.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
			.join("movimentacaoestoqueorigem.entrega entrega")
			.leftOuterJoin("movimentacaoestoque.material material")
			.leftOuterJoin("movimentacaoestoque.loteestoque loteestoque")
			.leftOuterJoin("loteestoque.listaLotematerial listaLotematerial")
			.leftOuterJoin("listaLotematerial.material materialLote")
			.where("entrega = ?", entrega)
			.where("movimentacaoestoque.dtcancelamento is null")
			.list();
	}
	
	/**
	 * M�todo que veririca se a entrega possui movimentaca��o de estoque n�o cancelada
	 *
	 * @param entrega
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existEntradaByEntrega(Entrega entrega) {
		return newQueryBuilderSined(Long.class)
			.from(Movimentacaoestoque.class)
			.select("count(*)")
			.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
			.join("movimentacaoestoqueorigem.entrega entrega")
			.where("entrega = ?", entrega)
			.where("movimentacaoestoque.dtcancelamento is null")
			.unique() > 0;
	}
	
	public Double getQuantidadeDeMaterial(Movimentacaoestoquetipo movimentacaoestoquetipo, Material material, Localarmazenagem localarmazenagem, Empresa empresa, Boolean considerarEmpresa, Boolean filtroMaterialmestregrade, Projeto projeto, Boolean considerarProjeto) {
		String whereInEmpresa = empresa != null && empresa.getCdpessoa() != null ? empresa.getCdpessoa().toString() : "";
		return this.getQuantidadeDeMaterial(movimentacaoestoquetipo, material, localarmazenagem, whereInEmpresa, considerarEmpresa, filtroMaterialmestregrade, false, projeto, considerarProjeto, null);
	}
	
	/**
	 * M�todo que busca quantidade de material n�o retirado.
	 * 
	 * @param material
	 * @return
	 * @author Ramon Brazil
	 */
	public Double getQuantidadeDeMaterial(Movimentacaoestoquetipo movimentacaoestoquetipo, Material material, Localarmazenagem localarmazenagem, String whereInEmpresa, Boolean considerarEmpresa, Boolean filtroMaterialmestregrade, Boolean origemvenda, Projeto projeto, Boolean considerarProjeto, Loteestoque loteestoque) {
		
		QueryBuilder<Double> query = newQueryBuilderSined(Double.class).setUseReadOnly(false);
		
		query
			.select("sum(movimentacaoestoque.qtde)")
			.from(Movimentacaoestoque.class)
			.setUseTranslator(false)
			.join("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
			.join("movimentacaoestoque.material material")
			.leftOuterJoin("material.materialmestregrade materialmestregrade")
			.leftOuterJoin("movimentacaoestoque.localarmazenagem localarmazenagem")
//			.where("material = ?", material)
			.where("movimentacaoestoquetipo = ?", movimentacaoestoquetipo)
			.where("movimentacaoestoque.dtcancelamento is null");
		
			if(considerarProjeto != null && considerarProjeto){
				query.where("movimentacaoestoque.projeto = ?", projeto);
			}
		
			if(filtroMaterialmestregrade != null && filtroMaterialmestregrade){
				query.where("materialmestregrade = ?", material);
				if(origemvenda != null && origemvenda){
					query.where("coalesce(material.ativo, false) = true");
				}
			}else {
				query.where("material = ?", material);
			}
			if (localarmazenagem != null) {
				query.where("localarmazenagem = ?", localarmazenagem);
			} else{
				if(StringUtils.isNotEmpty(whereInEmpresa)){
					query.where("exists (select 1 from Localarmazenagem la " +
							"left outer join la.listaempresa le " +
							"left outer join le.empresa e " +
							"where la.cdlocalarmazenagem = localarmazenagem.cdlocalarmazenagem " +
							"and (e.cdpessoa in (" + whereInEmpresa + ") or le is null) )");
				}
			}
			
			if(considerarEmpresa != null && considerarEmpresa){
				if(StringUtils.isNotBlank(whereInEmpresa)){
					query.leftOuterJoin("movimentacaoestoque.empresa empresa");
					query.openParentheses()
						.whereIn("empresa.cdpessoa", whereInEmpresa)
						.or()
						.where("empresa is null")
						.closeParentheses();
				}
			}
			
			if(loteestoque != null){
				query.where("movimentacaoestoque.loteestoque = ?", loteestoque);
			}
		return query.unique();
	}
	/**
	 * M�todo que busca o material de uma venda.
	 * 
	 * @param material
	 * @return
	 * @author Ramon Brazil
	 */


	public Movimentacaoestoque findbyVenda(Venda venda, Material material) {
		if(venda == null || venda.getCdvenda() == null || material == null || material.getCdmaterial() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query() 
			.select("movimentacaoestoquetipo.nome, movimentacaoestoquetipo.cdmovimentacaoestoquetipo, materialclasse.cdmaterialclasse, material.cdmaterial," +
					"materialclasse.nome, movimentacaoestoque.qtde, movimentacaoestoque.dtmovimentacao," +
					"movimentacaoestoque.valor, movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoqueorigem.cdmovimentacaoestoqueorigem")
			.join("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
			.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
			.leftOuterJoin("movimentacaoestoqueorigem.venda venda")
			.join("movimentacaoestoque.material material")
			.join("movimentacaoestoque.materialclasse materialclasse")
			.where("venda = ?", venda)
			.where("material = ?", material)
			.where("movimentacaoestoque.dtcancelamento is null")
			.unique();
	}
	
	public List<Movimentacaoestoque> findByPedidovenda(Pedidovenda pedidovenda, Material material) {
		return query() 
			.select("movimentacaoestoquetipo.nome, movimentacaoestoquetipo.cdmovimentacaoestoquetipo, materialclasse.cdmaterialclasse, material.cdmaterial," +
					"materialclasse.nome, movimentacaoestoque.qtde, movimentacaoestoque.dtmovimentacao," +
					"movimentacaoestoque.valor, movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoqueorigem.cdmovimentacaoestoqueorigem")
			.join("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
			.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
			.join("movimentacaoestoque.material material")
			.join("movimentacaoestoque.materialclasse materialclasse")
			.where("movimentacaoestoqueorigem.pedidovenda = ?", pedidovenda)
			.where("material = ?", material)
			.where("movimentacaoestoque.dtcancelamento is null")
			.list();
	}

	/**
	 * M�todo que retorna a �ltima movimenta��o de estoque cadastrada no sistema.
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Date getDtUltimoCadastro() {
		return newQueryBuilderSined(Date.class)
			.from(Movimentacaoestoque.class)
			.setUseTranslator(false) 
			.select("max(movimentacaoestoque.dtmovimentacao)")
			.where("movimentacaoestoque.dtcancelamento is null")
			.unique();
	}

	/**
	 * M�todo que retorna a qtde. total de entradas ou saidas cadastradas no sistema
	 * 
	 * @param tipo
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Integer getQtdeTotalEntradaSaida(Movimentacaoestoquetipo tipo) {
		return newQueryBuilderSined(Long.class)
			.from(Movimentacaoestoque.class)
			.setUseTranslator(false) 
			.select("count(*)")
			.join("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
			.where("movimentacaoestoquetipo = ?", tipo)
			.where("movimentacaoestoque.dtcancelamento is null")
			.unique()
			.intValue();
	}

	/**
	 * M�todo que retorna a qtde. total de movimenta��es cadastradas no sistema
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Integer getQtdeTotalMovimentacoes() {
		return newQueryBuilderSined(Long.class)
			.from(Movimentacaoestoque.class)
			.setUseTranslator(false) 
			.select("count(*)")
			.where("movimentacaoestoque.dtcancelamento is null")
			.unique()
			.intValue();
	}
	
	/**
	 * 
	 * M�todo que busca uma lista de MovimentacaoEstoque � partir de um whereIn de Material.
	 *
	 * @name findListMovimentacaoEstoqueByWhereInMaterial
	 * @param whereIn
	 * @return
	 * @return List<Movimentacaoestoque>
	 * @author Thiago Augusto
	 * @date 03/07/2012
	 *
	 */
	public List<Movimentacaoestoque> findListMovimentacaoEstoqueByWhereInMaterial(List<String> listaCdMaterial, List<String> listaCdLocalArmazenagem, List<String> listaCdMaterialClasse){
		
		QueryBuilder<Movimentacaoestoque> query = query();
		query
			.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.qtde, movimentacaoestoquetipo.cdmovimentacaoestoquetipo, " +
					"material.cdmaterial, material.nome, material.valorcusto, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, materialclasse.cdmaterialclasse, movimentacaoestoqueorigem.cdmovimentacaoestoqueorigem")
			.leftOuterJoin("movimentacaoestoque.material material")
			.leftOuterJoin("movimentacaoestoque.materialclasse materialclasse")
			.leftOuterJoin("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
			.leftOuterJoin("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
			.leftOuterJoin("movimentacaoestoque.localarmazenagem localarmazenagem")
			.where("movimentacaoestoque.dtcancelamento is null");
				
		for (int i = 0; i < listaCdMaterial.size(); i++) {
			query.openParentheses();
			query.where("material = ? ", new Material(Integer.parseInt(listaCdMaterial.get(i))));
			if (listaCdLocalArmazenagem.get(i) != null) {
				query.where("localarmazenagem = ? ", new Localarmazenagem(Integer.parseInt(listaCdLocalArmazenagem.get(i))));
			} else {
				query.where("localarmazenagem is null");
			}
			if (listaCdMaterialClasse.get(i) != null) {
				query.where("materialclasse = ? ", new Materialclasse(Integer.parseInt(listaCdMaterialClasse.get(i))));
			} else {
				query.where("materialclasse is null ");
			}
			query.closeParentheses();
			int aux = i;
			aux++;
			if (aux < listaCdMaterial.size()){
				query.or();
			}
		}
		query.orderBy("material.cdmaterial, localarmazenagem.cdlocalarmazenagem");
		return query.list();
	}

	/**
	 * Busca as Movimentacaoestoque que tenha a origem as expedi��es passadas por par�metro.
	 *
	 * @param whereIn
	 * @return
	 * @since 17/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Movimentacaoestoque> findByExpedicao(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item foi selecionado.");
		}
		return query()
					.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.qtde")
					.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
					.join("movimentacaoestoqueorigem.expedicao expedicao")
					.whereIn("expedicao.cdexpedicao", whereIn)
					.list();
	}

	
	/**
	 * M�todo para o relat�rio de entrada/sa�da de estoque
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#updateListagemQuery(QueryBuilder<Movimentacaoestoque> query, FiltroListagem _filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Movimentacaoestoque> findForReport(MovimentacaoestoqueFiltro filtro) {
		QueryBuilder<Movimentacaoestoque> query = querySined();
		this.updateListagemQuery(query, filtro);
		query.ignoreJoin("");
		
		String[] leftJoinsArray = new String[]{
				"movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem",
				"movimentacaoestoque.loteestoque loteestoque",
				"movimentacaoestoqueorigem.venda venda", 
				"venda.cliente cliente", 
				"movimentacaoestoqueorigem.entrega entrega",
				"entrega.listaEntregadocumento entregadocumento",
				"entregadocumento.fornecedor fornecedor", 
				"movimentacaoestoqueorigem.romaneio romaneio",
				"material.unidademedida unidademedida"
				
			};
		query.leftOuterJoinIfNotExists(leftJoinsArray);
		
		query
			.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.dtmovimentacao, movimentacaoestoque.qtde, " +
					"movimentacaoestoque.dtcancelamento, material.cdmaterial, material.nome, tipo.nome, tipo.cdmovimentacaoestoquetipo, " +
					"local.nome, movimentacaoestoque.valor, " +
					"venda.cdvenda, cliente.nome, entrega.cdentrega, entregadocumento.cdentregadocumento, fornecedor.nome," +
					"entregamaterial.cdentregamaterial, materialdocumento.cdmaterial, romaneio.cdromaneio, material.identificacao, " +
					"unidademedida.nome, movimentacaoestoqueorigem.romaneiomanual, loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade," +
					"movimentacaoestoqueorigem.consumo, notaorigem.numero")
			.leftOuterJoin("entregadocumento.listaEntregamaterial entregamaterial")
			.leftOuterJoin("entregamaterial.material materialdocumento")
			.leftOuterJoin("movimentacaoestoqueorigem.notafiscalproduto notaorigem");
		
		return query.list();
	}
	
	
	/**
	 * 
	 * @param filtro
	 * @return
	 */
	public List<Movimentacaoestoque> findForCsv(MovimentacaoestoqueFiltro filtro) {
		QueryBuilder<Movimentacaoestoque> query = querySined();
		this.updateListagemQuery(query, filtro);
		query.ignoreJoin("");
		
		String[] leftJoinsArray = new String[]{
			"movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem",
			"movimentacaoestoque.loteestoque loteestoque",
			"movimentacaoestoqueorigem.venda venda", 
			"venda.cliente cliente", 
			"movimentacaoestoqueorigem.entrega entrega",
			"entrega.listaEntregadocumento entregadocumento",
			"entregadocumento.fornecedor fornecedor", 
			"movimentacaoestoqueorigem.romaneio romaneio",
			"material.unidademedida unidademedida"
		};
		query.leftOuterJoinIfNotExists(leftJoinsArray);
		
		query
			.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.dtmovimentacao, movimentacaoestoque.qtde, " +
					"movimentacaoestoque.dtcancelamento, material.cdmaterial, material.nome, tipo.nome, tipo.cdmovimentacaoestoquetipo, " +
					"local.nome, movimentacaoestoque.valor, " +
					"venda.cdvenda, cliente.nome, entrega.cdentrega, entregadocumento.cdentregadocumento, fornecedor.nome," +
					"romaneio.cdromaneio, material.identificacao, " +
					"unidademedida.nome, movimentacaoestoqueorigem.romaneiomanual, loteestoque.numero, loteestoque.validade," +
					"movimentacaoestoqueorigem.consumo, notaorigem.numero, entregamaterial.cdentregamaterial, materialdocumento.cdmaterial," +
					"materialmestregrade.cdmaterial, materialmestregrade.identificacao ")
		
			.leftOuterJoin("movimentacaoestoqueorigem.notafiscalproduto notaorigem")
			.leftOuterJoin("entregadocumento.listaEntregamaterial entregamaterial")
			.leftOuterJoin("entregamaterial.material materialdocumento");
		return query.list();
	}
	
	/**
	 * M�todo que retorna totoais de entrada e saida de acordo com o filtro
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<TotalizadorEntradaSaida> getTotalValoresEntradaSaida(MovimentacaoestoqueFiltro filtro){
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		StringBuilder sql = new StringBuilder();
		
		sql
		.append("select  tipo, "+
				"sum(qtde) as qtde, "+
				"sum(total) as total, "+
				"sum(pesototal) as pesototal, "+
				"sum(pesobrutototal) as pesobrutototal	"+
				"from ( ")

				
		.append("select distinct met.cdmovimentacaoestoquetipo as tipo, " +
					"me.cdmovimentacaoestoque, "+
					"(me.qtde) as qtde, " +
					"(me.qtde * me.valor) as total, " +
					"(me.qtde * COALESCE(m.peso, 0)) as pesototal, " +
					"(me.qtde * COALESCE(m.pesobruto, 0)) as pesobrutototal ")
			.append("from movimentacaoestoque me ")
			.append("join movimentacaoestoquetipo met on met.cdmovimentacaoestoquetipo = me.cdmovimentacaoestoquetipo ") 
			.append("join material m on m.cdmaterial = me.cdmaterial ")
			.append("left outer join movimentacaoestoqueorigem morigem on morigem.cdmovimentacaoestoqueorigem = me.cdmovimentacaoestoqueorigem ")
			.append("left outer join materialgrupo mg on mg.cdmaterialgrupo = m.cdmaterialgrupo ")
			.append("left outer join materialtipo mt on mt.cdmaterialtipo = m.cdmaterialtipo ")
			.append("left outer join localarmazenagem l on l.cdlocalarmazenagem = me.cdlocalarmazenagem ")
			.append("left outer join empresa e on e.cdpessoa = me.cdempresa ")
			.append("left outer join projeto p on p.cdprojeto = me.cdprojeto ")
			.append("left outer join loteestoque le on le.cdloteestoque = me.cdloteestoque ");
		
		if(filtro.getFornecedor()!= null || filtro.getCliente() != null) {
			sql.append("left outer join venda venda on morigem.cdvenda=venda.cdvenda ");
			
			if(filtro.getFornecedor() != null){
				sql.append("left outer join entrega entrega on entrega.cdentrega = morigem.cdentrega ")
				   .append("left outer join entregadocumento entregadoc on entrega.cdentrega=entregadoc.cdentrega ");
			}
		}
		
		sql.append("where 1 = 1 ");
		if(filtro.getCdmoviemtacaestoque() != null){
			sql.append(" and me.cdmovimentacaoestoque = " + filtro.getCdmoviemtacaestoque() + " ");
		}
		if(filtro.getDatade() != null){
			sql.append(" and me.dtmovimentacao >= to_date('"+ format.format(filtro.getDatade()) + "', 'DD/MM/YYYY') ");
		}
		if(filtro.getDataate() != null){
			sql.append(" and me.dtmovimentacao <= to_date('"+ format.format(filtro.getDataate()) + "', 'DD/MM/YYYY') ");
		}			
		if(filtro.getValorinicial() != null){
			sql.append(" and coalesce(me.valor,0) >= " + filtro.getValorinicial() + " ");
		}		
		if(filtro.getValorfinal() != null){
			sql.append(" and coalesce(me.valor,0) <= " + filtro.getValorfinal() + " ");
		}		
		if(filtro.getEmpresa() != null){
			sql.append(" and e.cdpessoa = " + filtro.getEmpresa().getCdpessoa() + " ");
		}
		if(filtro.getLocalarmazenagem() != null){
			sql.append(" and l.cdlocalarmazenagem = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem() + " ");
		}
		if(filtro.getMaterialgrupo() != null){
			sql.append(" and mg.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo() + " ");
		}
		if(filtro.getMaterialtipo() != null){
			sql.append(" and mt.cdmaterialtipo = " + filtro.getMaterialtipo().getCdmaterialtipo() + " ");
		}
		if(filtro.getMaterial() != null){
			sql.append(" and m.cdmaterial = " + filtro.getMaterial().getCdmaterial() + " ");
		}
		if(filtro.getProjeto() != null){
			sql.append(" and p.cdprojeto = " + filtro.getProjeto().getCdprojeto() + " ");
		}
		String projetos = SinedUtil.getListaProjeto();
		if(projetos != null && !projetos.trim().equals("")){
			sql.append(" and p.cdprojeto in (" + projetos + ") ");
		}
		if(filtro.getLoteestoque() != null){
			sql.append(" and le.cdloteestoque = " + filtro.getLoteestoque().getCdloteestoque() + " ");
		}
		
		if(filtro.getListatipos() != null && filtro.getListatipos().size() > 0){
			sql.append(" and met.cdmovimentacaoestoquetipo in  (" + CollectionsUtil.listAndConcatenate(filtro.getListatipos(), "cdmovimentacaoestoquetipo",",") + " ) ");
		}
		
		if(filtro.getCanceladas() == null || !filtro.getCanceladas()){
			sql.append(" and me.dtcancelamento is null ");
		}
		
		if (filtro.getProduto() || filtro.getServico() || filtro.getEpi()) {
			sql.append(" and ( ");
			if (filtro.getProduto() != null && filtro.getProduto()) {
				sql.append(" me.cdmaterialclasse = " + Materialclasse.PRODUTO.getCdmaterialclasse());
			}
			if(filtro.getServico() != null && filtro.getServico()){
				if (filtro.getProduto() != null && filtro.getProduto()) {
					sql.append(" or ");
				}
				sql.append(" me.cdmaterialclasse = " + Materialclasse.SERVICO.getCdmaterialclasse());
			}
			if (filtro.getEpi() != null && filtro.getEpi()) {
				if((filtro.getProduto() != null && filtro.getProduto() )|| (filtro.getServico() != null && filtro.getServico())) {
					sql.append(" or");
				}
				sql.append(" me.cdmaterialclasse = " + Materialclasse.EPI.getCdmaterialclasse());
			}
			sql.append(" ) ");
		}
		
		if(filtro.getOrigem() != null){
			MovimentacaoEstoqueOrigemEnum origem = filtro.getOrigem();
			
			
			if (MovimentacaoEstoqueOrigemEnum.COMPRA.equals(origem)){
				sql.append(" and (morigem.cdentrega is not null ");
				if(filtro.getIdorigem() != null){
					sql.append("and morigem.cdentrega = ").append(filtro.getIdorigem());
				}
				sql.append(") ");
			}else if (MovimentacaoEstoqueOrigemEnum.CONSUMO.equals(origem)){
				sql.append("  and (morigem.consumo = true) ");
			}else if (MovimentacaoEstoqueOrigemEnum.ROMANEIO.equals(origem)){
				sql.append(" and (morigem.cdromaneio is not null ");
				if(filtro.getIdorigem() != null){
					sql.append("and morigem.cdromaneio = ").append(filtro.getIdorigem());
				}
				sql.append(") ");
			}else if (MovimentacaoEstoqueOrigemEnum.ROMANEIO_MANUAL.equals(origem)){
				sql.append(" and (morigem.romaneiomanual = true ");
				if(filtro.getIdorigem() != null){
					sql.append("and morigem.cdromaneio = ").append(filtro.getIdorigem());
				}
				sql.append(") ");
			}else if (MovimentacaoEstoqueOrigemEnum.VENDA.equals(origem)){
				sql.append(" and (morigem.cdvenda is not null ");
				if(filtro.getIdorigem() != null){
					sql.append("and morigem.cdvenda = ").append(filtro.getIdorigem());
				}
				sql.append(") ");
			}else if (MovimentacaoEstoqueOrigemEnum.INVENTARIO.equals(origem)){
				sql.append(" and (morigem.cdinventario is not null ");
				if(filtro.getIdorigem() != null){
					sql.append("and morigem.cdinventario = ").append(filtro.getIdorigem());
				}
				sql.append(") ");
			}else if (MovimentacaoEstoqueOrigemEnum.PEDIDOVENDA.equals(origem)){
				sql.append(" and (morigem.cdpedidovenda is not null ");
				if(filtro.getIdorigem() != null){
					sql.append("and morigem.cdpedidovenda = ").append(filtro.getIdorigem());
				}
				sql.append(") ");
			}else if (MovimentacaoEstoqueOrigemEnum.NOTA_SAIDA.equals(origem)){
				sql.append(" and (morigem.cdNota is not null ");
				if(filtro.getIdorigem() != null){
					sql.append("and morigem.cdNota = ").append(filtro.getIdorigem());
				}
				sql.append(") ");
			}
		}

		if(filtro.getFornecedor()!= null){
			sql.append(" and (entregadoc.cdfornecedor = ")
				.append(filtro.getFornecedor().getCdpessoa())
				.append(" or venda.cdterceiro = ")
				.append(filtro.getFornecedor().getCdpessoa())
				.append(" )");
		}
		
		if(filtro.getCliente()!= null){
			sql.append(" and venda.cdcliente = ")
				.append(filtro.getCliente().getCdpessoa())
				.append(" ");
		}
		
		if(filtro.getMovimentacaoanimalmotivo()!=null){
			sql.append(" and me.movimentacaoanimalmotivo = ")
			.append(filtro.getMovimentacaoanimalmotivo().getIdMotivo())
			.append(" ");
		}
		sql.append(" ) itens "+
					"group by tipo ");
		
		SinedUtil.markAsReader();
		List<TotalizadorEntradaSaida> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new TotalizadorEntradaSaida(
									rs.getInt("tipo"), 
									rs.getDouble("qtde"), 
									rs.getDouble("total"),
									rs.getDouble("pesototal"),
									rs.getDouble("pesobrutototal"));
				}
			});
		
		if(lista != null && lista.size() == 1){
			TotalizadorEntradaSaida totalizadorEntradaSaida = lista.get(0);
			if(totalizadorEntradaSaida != null && totalizadorEntradaSaida.getMovimentacaoestoquetipo() != null){
				if(Movimentacaoestoquetipo.ENTRADA.equals(totalizadorEntradaSaida.getMovimentacaoestoquetipo())){
					TotalizadorEntradaSaida saida = new TotalizadorEntradaSaida(Movimentacaoestoquetipo.SAIDA.getCdmovimentacaoestoquetipo(), null, null, null, null);
					lista.add(saida);
				}else {
					TotalizadorEntradaSaida entrada = new TotalizadorEntradaSaida(Movimentacaoestoquetipo.ENTRADA.getCdmovimentacaoestoquetipo(), null, null, null, null);
					lista.add(entrada);
				}
			}
			
		}else if(lista == null || lista.isEmpty()){
			TotalizadorEntradaSaida entrada = new TotalizadorEntradaSaida(Movimentacaoestoquetipo.ENTRADA.getCdmovimentacaoestoquetipo(), null, null, null, null);
			TotalizadorEntradaSaida saida = new TotalizadorEntradaSaida(Movimentacaoestoquetipo.SAIDA.getCdmovimentacaoestoquetipo(), null, null, null, null);
			lista.add(saida);
			lista.add(entrada);
		}
		
		return lista;
	}

	/**
	 * M�todo que verifica se existe baixa da nota fiscal de produto
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean existMovimentacaoestoqueByNotas(String whereIn) {
		return newQueryBuilderSined(Long.class)
		.from(Movimentacaoestoque.class)
		.select("count(*)")
		.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
		.join("movimentacaoestoqueorigem.notafiscalproduto notafiscalproduto")
		.whereIn("notafiscalproduto.cdNota", whereIn)
		.where("movimentacaoestoque.dtcancelamento is null")
		.unique() > 0;
	}
	
	/**
	 * Busca as movimenta��es do estoque do romaneio passado por par�metro.
	 *
	 * @param romaneio
	 * @return
	 * @author Rodrigo Freitas
	 * @param movimentacaoestoquetipo 
	 * @since 11/12/2012
	 */
	public List<Movimentacaoestoque> findByRomaneio(Romaneio romaneio, Movimentacaoestoquetipo movimentacaoestoquetipo) {
		if(romaneio == null || romaneio.getCdromaneio() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoquetipo.cdmovimentacaoestoquetipo, " +
							"material.cdmaterial, material.valorindenizacao, material.valorcusto, movimentacaoestoque.valor, " +
							"movimentacaoestoque.qtde, empresa.cdpessoa, projeto.cdprojeto")
					.join("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
					.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
					.join("movimentacaoestoqueorigem.romaneio romaneio")
					.join("movimentacaoestoque.material material")
					.leftOuterJoin("movimentacaoestoque.empresa empresa")
					.leftOuterJoin("movimentacaoestoque.projeto projeto")
					.where("movimentacaoestoque.dtcancelamento is null")
					.where("romaneio = ?", romaneio)
					.where("movimentacaoestoquetipo = ?", movimentacaoestoquetipo)
					.list();
	}

	/**
	 * Busca as movimenta��es do estoque da entrega passada por par�metro.
	 *
	 * @param entrega
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/12/2012
	 */
	public List<Movimentacaoestoque> findByEntrega(Entrega entrega) {
		if(entrega == null || entrega.getCdentrega() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.qtde, " +
							"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
							"material.cdmaterial, material.nome, material.identificacao")
					.join("movimentacaoestoque.material material")
					.join("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
					.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
					.join("movimentacaoestoqueorigem.entrega entrega")
					.leftOuterJoin("movimentacaoestoque.localarmazenagem localarmazenagem")
					.where("movimentacaoestoquetipo = ?", Movimentacaoestoquetipo.ENTRADA)
					.where("movimentacaoestoque.dtcancelamento is null")
					.where("entrega = ?", entrega)
					.list();
	}
	
	/**
	* M�todo que busca as entradas de acordo com a entregamaterial
	*
	* @param whereIn
	* @return
	* @since 29/08/2014
	* @author Luiz Fernando
	*/
	public List<Movimentacaoestoque> findByEntregamaterial(String whereIn) {
		if(StringUtils.isEmpty(whereIn)){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.dtmovimentacao, movimentacaoestoque.valor, " +
							"material.cdmaterial, movimentacaoestoqueorigem.cdmovimentacaoestoqueorigem, entregamaterial.cdentregamaterial ," +
							"empresa.cdpessoa")
					.join("movimentacaoestoque.material material")
					.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
					.join("movimentacaoestoqueorigem.entregamaterial entregamaterial")
					.leftOuterJoin("movimentacaoestoque.empresa empresa")
					.where("movimentacaoestoque.movimentacaoestoquetipo = ?", Movimentacaoestoquetipo.ENTRADA)
					.where("movimentacaoestoque.dtcancelamento is null")
					.whereIn("entregamaterial.cdentregamaterial", whereIn)
					.list();
	}
	
	/**
	 * Busca as movimenta��es de estoque referente a uma nota de produto.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/09/2013
	 */
	public List<Movimentacaoestoque> findByNota(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item foi selecionado.");
		}
		return query()
					.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.qtde, " +
							"notafiscalproduto.cdNota, notafiscalproduto.numero")
					.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
					.join("movimentacaoestoqueorigem.notafiscalproduto notafiscalproduto")
					.whereIn("notafiscalproduto.cdNota", whereIn)
					.list();
	}
	
	public void deleteAllAnteriorIgual(Date dtestoque) {
		getJdbcTemplate().update("DELETE FROM MOVIMENTACAOESTOQUE WHERE DTMOVIMENTACAO <= ?", new Object[]{SinedDateUtils.dataToEndOfDay(dtestoque)});
	}
	
	public void deleteAllAnteriorIgualEmpresa(Date dtestoque, Empresa empresa) {
		getJdbcTemplate().update("DELETE FROM MOVIMENTACAOESTOQUE WHERE DTMOVIMENTACAO <= ? AND CDEMPRESA = ?", new Object[]{SinedDateUtils.dataToEndOfDay(dtestoque), empresa.getCdpessoa()});
	}
	
	/**
	 * 
	 * M�todo que busca uma lista de MovimentacaoEstoque de entrada para um determinado materia em intevalo de data.
	 *
	 * @param whereIn
	 * @return List<Movimentacaoestoque>
	 * @author Lucas Costa
	 * @date 24/03/2014
	 *
	 */
	public List<Movimentacaoestoque> findListMovimentacaoEstoqueByMaterialInData(Material material, Empresa empresa, Localarmazenagem localarmazenagem, Date dataDe, Date dataAte){
		
		QueryBuilder<Movimentacaoestoque> query = query();
		query
			.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.qtde, movimentacaoestoque.valor ")
			.where("movimentacaoestoque.dtcancelamento is null")
			.where("movimentacaoestoque.material = ? ", material)
			.where("movimentacaoestoque.empresa = ? ", empresa)
			.where("movimentacaoestoque.localarmazenagem = ? ", localarmazenagem)
			.where("movimentacaoestoque.dtmovimentacao >= ?", dataDe)
			.where("movimentacaoestoque.dtmovimentacao <= ?", dataAte)
			.where("movimentacaoestoque.movimentacaoestoquetipo = ? ", Movimentacaoestoquetipo.ENTRADA)
			.orderBy("material.cdmaterial");
		return query.list();
	}

	public List<Movimentacaoestoque> findForComprovanteColeta(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Pedido de venda n�o pode ser nulo");
		
		return query()
			.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.qtde, movimentacaoestoque.valor," +
					"movimentacaoestoque.dtmovimentacao, movimentacaoestoque.observacao, " +
					"movimentacaoestoqueorigem.cdmovimentacaoestoqueorigem," +
					"pedidovenda.cdpedidovenda, material.cdmaterial, material.nome, material.identificacao, " +
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
					"movimentacaoestoquetipo.cdmovimentacaoestoquetipo, movimentacaoestoquetipo.nome," +
					"loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade," +
					"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome," +
					"materialclasse.cdmaterialclasse, materialclasse.nome ")
			.join("movimentacaoestoque.material material")
			.join("material.unidademedida unidademedida")
			.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
			.join("movimentacaoestoqueorigem.pedidovenda pedidovenda")
			.leftOuterJoin("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
			.leftOuterJoin("movimentacaoestoque.loteestoque loteestoque")
			.leftOuterJoin("movimentacaoestoque.localarmazenagem localarmazenagem")
			.leftOuterJoin("movimentacaoestoque.materialclasse materialclasse")
			.where("pedidovenda  = ?", pedidovenda)
			.orderBy("material.cdmaterial")
			.list();
	}

	/**
	 * M�todo que busca as entradas do pedido de venda
	 *
	 * @param whereInPedidovenda
	 * @return
	 * @author Luiz Fernando
	 * @since 02/04/2014
	 */
	public List<Movimentacaoestoque> findByPedidovenda(String whereInPedidovenda) {
		if(whereInPedidovenda == null || "".equals(whereInPedidovenda))
			throw new SinedException("Par�metro inv�lido.");
		
		return query() 
			.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.qtde, " +
					"material.cdmaterial, movimentacaoestoque.valor, pedidovenda.cdpedidovenda, " +
					"movimentacaoestoqueorigem.cdmovimentacaoestoqueorigem ")
			.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
			.join("movimentacaoestoqueorigem.pedidovenda pedidovenda")
			.join("movimentacaoestoque.material material")
			.whereIn("pedidovenda.cdpedidovenda", whereInPedidovenda)
			.where("movimentacaoestoque.dtcancelamento is null")
			.list();
	}

	/**
	* M�todo que verifica se existe movimentaca��o de estoque para um determinado material
	*
	* @param material
	* @return
	* @since 14/07/2014
	* @author Luiz Fernando
	*/
	public boolean existeMovimentacaoEstoque(Material material) {
		if(material == null || material.getCdmaterial() == null)
			return false;
		
		QueryBuilderSined<Long> query = newQueryBuilderSined(Long.class);
		query.select("count(*)")
			 .from(Movimentacaoestoque.class)
			 .setUseTranslator(false)
			 .join("movimentacaoestoque.material material")
			 .where("material = ?", material);
		
		query.setUseWhereEmpresa(false);
		
		return query.unique() > 0;
	}
	
	/**
	* M�todo que verifica se existe movimentaca��o de estoque que n�o seja cancelada para um determinado material
	*
	* @param material
	* @return
	* @since 03/04/2020
	* @author Diogo Souza
	*/
	public boolean existeMovimentacaoEstoqueNaoCancelada(Material material) {
		if(material == null || material.getCdmaterial() == null)
			return false;
		
		QueryBuilderSined<Long> query = newQueryBuilderSined(Long.class);
		query.select("count(*)")
			 .from(Movimentacaoestoque.class)
			 .setUseTranslator(false)
			 .join("movimentacaoestoque.material material")
			 .where("material = ?", material)
			 .where(" movimentacaoestoque.dtcancelamento = null");
		
		query.setUseWhereEmpresa(false);
		
		return query.unique() > 0;
	}
	
	/**
	 * Busca as movimenta��es de estoque de uma agenda de produ��o
	 *
	 * @param whereInProducaoagenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/07/2014
	 */
	public List<Movimentacaoestoque> findByProducaoagenda(String whereInProducaoagenda) {
		return query()
					.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.qtde")
					.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
					.join("movimentacaoestoqueorigem.producaoagenda producaoagenda")
					.whereIn("producaoagenda.cdproducaoagenda", whereInProducaoagenda)
					.list();
	}

	/**
	 * Busca as movimenta��es de estoque de uma coleta
	 *
	 * @param coleta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/07/2014
	 */
	public List<Movimentacaoestoque> findByColeta(Coleta coleta) {
		return query()
				.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.qtde")
				.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
				.join("movimentacaoestoqueorigem.coleta coleta")
				.where("coleta = ?", coleta)
				.list();
	}

	/**
	* M�todo que retorna uma movimentacao de estoque com o total de quantidade e valor
	*
	* @param material
	* @param dtcustoinicial
	* @param qtdecustoinicial
	* @param valorcustoinicial
	* @return
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public Movimentacaoestoque getTotalQtdeValor(Material material, Date dtcustoinicial, Double qtdecustoinicial, Double valorcustoinicial) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		StringBuilder sql = new StringBuilder();
		sql.append("  select SUM(me.qtde) as qtdeTotal, SUM(COALESCE(me.valor, 0)*me.qtde) as valorTotal ")
			.append(" from Movimentacaoestoque me ")
			.append(" where me.cdmaterial = " + material.getCdmaterial())
			.append(" and me.cdmovimentacaoestoquetipo = " + Movimentacaoestoquetipo.ENTRADA.getCdmovimentacaoestoquetipo())
			.append(" and me.dtcancelamento is null ")
		;
		
		if(dtcustoinicial != null && qtdecustoinicial != null && valorcustoinicial != null){
			sql.append(" and me.dtmovimentacao >= to_date('" + new SimpleDateFormat("dd/MM/yyyy").format(dtcustoinicial) + "', 'dd/MM/yyyy') ");
		}

		SinedUtil.markAsReader();
		Movimentacaoestoque mov = (Movimentacaoestoque) getJdbcTemplate().queryForObject(sql.toString(), 
				new RowMapper() {
					public Movimentacaoestoque mapRow(ResultSet rs, int rowNum) throws SQLException {
						Movimentacaoestoque mov = new Movimentacaoestoque();
						mov.setQtde(rs.getDouble("qtdeTotal"));
						mov.setValor(rs.getDouble("valorTotal"));
						return mov;
					}
				});
		
		if(dtcustoinicial != null && qtdecustoinicial != null && valorcustoinicial != null){
			mov.setQtde((mov.getQtde() != null ? mov.getQtde() : 0d) + qtdecustoinicial);
			mov.setValor((mov.getValor() != null ? mov.getValor() : 0d) + (valorcustoinicial *  qtdecustoinicial));
		}
		return mov;
	}
	public Movimentacaoestoque buscarValorCustoMedio(Integer cdMovimentacaoEstoque) {
		return query().select("movimentacaoestoque.cdmovimentacaoestoque,movimentacaoestoque.valorcusto," +
				"rateio.cdrateio")
				.leftOuterJoin("movimentacaoestoque.rateio rateio")
				.where("movimentacaoestoque.cdmovimentacaoestoque =? ", cdMovimentacaoEstoque)
				.unique();
	}
	/**
	* M�todo que atualiza o valor da entrada do estoque
	*
	* @param valorEntrada
	* @param movimentacaoestoque
	* @since 28/08/2014
	* @author Luiz Fernando
	*/
	public void updateValorEntradaByMovimentacaoestoque(Double valorEntrada, Movimentacaoestoque movimentacaoestoque) {
		if(valorEntrada != null && movimentacaoestoque != null && movimentacaoestoque.getCdmovimentacaoestoque() != null){
			getJdbcTemplate().update("update movimentacaoestoque set valor = ? " +
					" where cdmovimentacaoestoque = ? ", new Object[]{valorEntrada, movimentacaoestoque.getCdmovimentacaoestoque()});
		}
	}
	
	
	
	/**
	* M�todo que faz o update no campo valor de custo da movimenta��o de estoque
	*
	* @param valorCusto
	* @param movimentacaoestoque
	* @since 24/09/2014
	* @author Luiz Fernando
	*/
	public void updateValorCustoEntradaByMovimentacaoestoque(Double valorCusto, Movimentacaoestoque movimentacaoestoque) {
		if(valorCusto != null && movimentacaoestoque != null && movimentacaoestoque.getCdmovimentacaoestoque() != null){
			getJdbcTemplate().update("update movimentacaoestoque set valorcusto = ? " +
					" where cdmovimentacaoestoque = ? ", new Object[]{valorCusto, movimentacaoestoque.getCdmovimentacaoestoque()});
		}
	}
	
	public void updateValorCustoPorEmpresaByMovimentacaoestoque(Double valorCustoPorEmpresa, Movimentacaoestoque movimentacaoestoque) {
		if(valorCustoPorEmpresa != null && movimentacaoestoque != null && movimentacaoestoque.getCdmovimentacaoestoque() != null) {
			getJdbcTemplate().update("update movimentacaoestoque set valorcustoporempresa = ? where cdmovimentacaoestoque = ? ", 
					new Object[]{valorCustoPorEmpresa, movimentacaoestoque.getCdmovimentacaoestoque()});
		}
	}
	
	/**
	* M�todo que carrega as movimenta��es com o material
	*
	* @param whereIn
	* @return
	* @since 29/08/2014
	* @author Luiz Fernando
	*/
	public List<Movimentacaoestoque> findMovimentacoesWithMaterial(String whereIn) {
		if (whereIn == null || whereIn.equals(""))
			throw new SinedException("Nenhum item selecionado.");
		
		return query()
			.select("movimentacaoestoque.cdmovimentacaoestoque, material.cdmaterial")
			.join("movimentacaoestoque.material material")
			.whereIn("movimentacaoestoque.cdmovimentacaoestoque", whereIn)
			.list();
	}

	/**
	* M�todo que busca as entradas/sa�das dos materiais para calcular a qtde disponivel
	*
	* @param material
	* @param dtinicio
	* @return
	* @since 22/09/2014
	* @author Luiz Fernando
	 * @param movimentacaoestoque 
	*/
	public List<Movimentacaoestoque> findByMaterial(Material material, Date dtinicio, Movimentacaoestoque movimentacaoestoque, Empresa empresa) {
		if(material == null || material.getCdmaterial() == null || dtinicio == null)
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Movimentacaoestoque> query = querySined()
				.setUseWhereEmpresa(false)
				.setUseWhereProjeto(false)
				.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.qtde, movimentacaoestoquetipo.cdmovimentacaoestoquetipo, " +
						"movimentacaoestoque.dtmovimentacao, movimentacaoestoque.valor, movimentacaoestoque.valorcusto, " +
						"empresa.cdpessoa, empresa.nomefantasia ")
				.join("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
				.join("movimentacaoestoque.material material")
				.leftOuterJoin("movimentacaoestoque.empresa empresa")
				.where("movimentacaoestoque.dtcancelamento is null")
				.where("material = ?", material)
				.where("movimentacaoestoque.dtmovimentacao >= ?", dtinicio)
				.openParentheses()
					.where("movimentacaoestoque.cdmovimentacaoestoque > ?", movimentacaoestoque != null ? movimentacaoestoque.getCdmovimentacaoestoque() : null)
					.or()
					.where("movimentacaoestoquetipo = ?", Movimentacaoestoquetipo.SAIDA, movimentacaoestoque != null)
				.closeParentheses()
				.orderBy("movimentacaoestoque.dtmovimentacao, movimentacaoestoque.cdmovimentacaoestoque");
		
		if(SinedUtil.isRateioMovimentacaoEstoque()){
			query.select(query.getSelect().getValue() + ", rateio.cdrateio, listaRateioitem.cdrateioitem, listaRateioitem.percentual, listaRateioitem.valor");
			query
				.leftOuterJoin("movimentacaoestoque.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem");
		}
		
		if(empresa != null) {
			query.where("empresa = ?", empresa);
		}
		
		return query
				.list();
	}
	
	/**
	* M�todo que busca a movimentacao de entrada anterior a passada por par�metro
	*
	* @param material
	* @param meAtual
	* @return
	* @since 24/09/2014
	* @author Luiz Fernando
	*/
	public Movimentacaoestoque getMovimentacaoAnterior(Material material, Movimentacaoestoque meAtual) {
		if(material == null || material.getCdmaterial() == null || meAtual == null || 
				meAtual.getDtmovimentacao() == null || meAtual.getCdmovimentacaoestoque() == null)
			return null;
		
		List<Movimentacaoestoque> lista = querySined()
				.setUseWhereEmpresa(false)
				.setUseWhereProjeto(false)
				.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.valorcusto, movimentacaoestoque.dtmovimentacao, movimentacaoestoque.qtde")
				.join("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
				.join("movimentacaoestoque.material material")
				.where("movimentacaoestoque.dtcancelamento is null")
				.where("material = ?", material)
				.where("movimentacaoestoquetipo = ?", Movimentacaoestoquetipo.ENTRADA)
				.where("movimentacaoestoque.dtmovimentacao < ?", meAtual.getDtmovimentacao())
				.where("movimentacaoestoque.dtmovimentacao >= material.dtcustoinicial")
				.where("movimentacaoestoque.cdmovimentacaoestoque < ?", meAtual.getCdmovimentacaoestoque())
				.where("movimentacaoestoque.valorcusto is not null")
				.orderBy("movimentacaoestoque.dtmovimentacao desc, movimentacaoestoque.cdmovimentacaoestoque desc")
				.setMaxResults(1)
				.list();
		
		return SinedUtil.isListNotEmpty(lista) ? lista.get(0) : null;
	}
	
	/**
	* M�todo que verifica se existe movimenta��o de estoque para a requisicao (Ordem de servi�o)
	*
	* @param requisicao
	* @return
	* @since 20/01/2015
	* @author Luiz Fernando
	*/
	public Boolean existeMovimentacaoestoqueByRequisicao(Requisicao requisicao) {
		return newQueryBuilderSined(Long.class)
			.from(Movimentacaoestoque.class)
			.select("count(*)")
			.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
			.join("movimentacaoestoqueorigem.requisicao requisicao")
			.where("requisicao = ?", requisicao)
			.where("movimentacaoestoque.dtcancelamento is null")
			.unique() > 0;
	}
	
	/**
	 * Busca os registros de invent�rio para o SPED
	 *
	 * @param filtro
	 * @return
	 */
	public List<Movimentacaoestoque> findForSpedRegistroK230(SpedarquivoFiltro filtro) {
		if(filtro == null){
			return null;
		}
		
		List<Tipoitemsped> listaTipoitemsped = new ArrayList<Tipoitemsped>();
		listaTipoitemsped.add(Tipoitemsped.PRODUTO_PROCESSO);
		listaTipoitemsped.add(Tipoitemsped.PRODUTO_ACABADO);
		
		return query()
				.select("movimentacaoestoque.cdmovimentacaoestoque, material.cdmaterial, material.identificacao, movimentacaoestoque.qtde, " +
						"movimentacaoestoque.dtmovimentacao, " +
						"producaoordem.cdproducaoordem, producaoordem.dtproducaoordem, listaProducaoMaterial.cdmaterial, " +
						"listaProducao.consumo, listaProducaoMaterial.tipoitemsped ")
				.join("movimentacaoestoque.material material")
				.join("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
				.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
				.join("movimentacaoestoqueorigem.producaoordem producaoordem")
				.join("material.listaProducao listaProducao")
				.join("listaProducao.material listaProducaoMaterial")
				.where("movimentacaoestoquetipo = ?", Movimentacaoestoquetipo.ENTRADA)
				.where("movimentacaoestoque.dtcancelamento is null")
				.where("producaoordem is not null")
				.where("movimentacaoestoque.empresa = ?", filtro.getEmpresa())
				.where("movimentacaoestoque.dtmovimentacao >= ?", filtro.getDtinicio())
				.where("movimentacaoestoque.dtmovimentacao <= ?", filtro.getDtfim())
				.whereIn("material.tipoitemsped", CollectionsUtil.listAndConcatenate(listaTipoitemsped, "id", ","))
				.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<MateriaprimaBean> findForSpedRegistroK235(SpedarquivoFiltro filtro, Material material, Producaoordem producaoordem) {
		if(filtro == null || producaoordem == null || material == null){
			return null;
		}
		
		StringBuilder sql = new StringBuilder();
		
		sql.append( " select distinct pommp.cdmaterial, m.identificacao, m.tipoitemsped, COALESCE(pommp.qtdeprevista, 0) as qtde ");
		sql.append( " from producaoordemmaterial pom");
		sql.append( " join producaoordemmaterialorigem pomo on pomo.cdproducaoordemmaterial = pom.cdproducaoordemmaterial");
		sql.append( " join producaoordemmaterialmateriaprima pommp on pommp.cdproducaoordemmaterial = pom.cdproducaoordemmaterial");
		sql.append( " join material m on m.cdmaterial = pommp.cdmaterial");
		sql.append( " where pomo.cdproducaoagenda in (select pomo.cdproducaoagenda");
		sql.append( "   						  from producaoordemmaterial pom ");
		sql.append( "                           join producaoordemmaterialorigem pomo on pomo.cdproducaoordemmaterial = pom.cdproducaoordemmaterial");
		sql.append( "                           where pom.cdproducaoordem = " + producaoordem.getCdproducaoordem());
		sql.append( " )");
		sql.append( " and pom.cdmaterial = " + material.getCdmaterial());
		sql.append( " ");
		sql.append( " UNION ALL ");
		sql.append( " ");
		sql.append( " select distinct pammp.cdmaterial, m.identificacao, m.tipoitemsped, COALESCE(pammp.qtdeprevista) as qtde ");
		sql.append( " from producaoagendamaterial pam");
		sql.append( " join producaoagendamaterialmateriaprima pammp on pammp.cdproducaoagendamaterial = pam.cdproducaoagendamaterial");
		sql.append( " join producaoordemmaterialorigem pomo on pomo.cdproducaoagendamaterial = pam.cdproducaoagendamaterial");
		sql.append( " join producaoordemmaterial pom on pom.cdproducaoordemmaterial = pomo.cdproducaoordemmaterial");
		sql.append( " join material m on m.cdmaterial = pammp.cdmaterial");
		sql.append( " where pomo.cdproducaoagenda in (select pomo.cdproducaoagenda");
		sql.append( "   						  from producaoordemmaterial pom ");
		sql.append( "                           join producaoordemmaterialorigem pomo on pomo.cdproducaoordemmaterial = pom.cdproducaoordemmaterial");
		sql.append( "                           where pom.cdproducaoordem = " + producaoordem.getCdproducaoordem());
		sql.append( " )");
		sql.append( " and pom.cdmaterial = " + material.getCdmaterial());
		sql.append( " and not exists (select pommp.cdproducaoordemmaterial ");
		sql.append( " 				from producaoordemmaterialmateriaprima pommp ");
		sql.append( "                 where pommp.cdproducaoordemmaterial = pom.cdproducaoordemmaterial");
		sql.append( " )");
		
		SinedUtil.markAsReader();
		List<MateriaprimaBean> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public MateriaprimaBean mapRow(ResultSet rs, int rowNum) throws SQLException {
				MateriaprimaBean estoque = new MateriaprimaBean();
				
				estoque.setMaterial(new Material(rs.getInt("cdmaterial")));
				estoque.getMaterial().setIdentificacao(rs.getString("identificacao"));
				estoque.getMaterial().setTipoitemsped(Tipoitemsped.values()[rs.getInt("tipoitemsped")]);
				estoque.setQtde(rs.getDouble("qtde"));
				
				return estoque;
			}
		});
		
		return lista;
	}
	
	/**
	 * @param entrega
	 * @return
	 * @since 09/06/2015
	 * @author Andrey Leonardo
	 */
	public Boolean existeMovimentacaoestoqueByEntrega(Entrega entrega, Boolean notCancelada){
		return newQueryBuilderSined(Long.class)
		.from(Movimentacaoestoque.class)
		.select("count(*)")
		.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
		.where("movimentacaoestoqueorigem.entrega = ?", entrega)
		.where("movimentacaoestoque.dtcancelamento is null", notCancelada != null && notCancelada)
		.unique() > 0;
	}
	/**
	 * M�todo que busca as movimenta��o registradas a partir de uma coleta.
	 * @param whereIn
	 * @since 30/06/2015
	 * @author Danilo Guimar�es
	 */
	public List<Movimentacaoestoque> findByColeta(String whereIn) {
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("O par�metro whereIn n�o pode ser nulo.");
			
		return query()
				.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.qtde, movimentacaoestoque.observacao, movimentacaoestoque.dtcancelamento")
				.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
				.join("movimentacaoestoqueorigem.coleta coleta")
				.whereIn("coleta.cdcoleta", whereIn)
				.where("movimentacaoestoque.dtcancelamento is null")
				.list();
	}
	
	/**
	 * M�todo que busca as movimenta��o registradas a partir de uma coleta avulsa e material.
	 * @param coleta
	 * @param material
	 * @since 30/06/2015
	 * @author Danilo Guimar�es
	 */
	public Movimentacaoestoque findByColetaAvulsaAndMaterial(Coleta coleta, Material material) {
		if(coleta == null || coleta.getCdcoleta() == null)
			throw new SinedException("O par�metro coleta n�o pode ser nulo.");
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("O par�metro material n�o pode ser nulo.");
			
		return query()
				.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.qtde")
				.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
				.join("movimentacaoestoqueorigem.coleta coleta")
				.where("coleta = ?", coleta)
				.where("movimentacaoestoque.material = ?", material)
				.where("movimentacaoestoque.dtcancelamento is null")
				.where("movimentacaoestoqueorigem.pedidovenda is null")
				.unique();
	}
	
	/**
	 * M�todo que cancela as movimenta��es ap�s exclus�o de uma coleta avulsa.
	 * @param cdmovimentacaoestoque
	 * @param dtCancelamento
	 * @param observacao
	 * @since 30/06/2015
	 * @author Danilo Guimar�es
	 */
	public void updateDataCancelamentoMovimentacao(Integer cdmovimentacaoestoque, Date dtCancelamento, String observacao){
		if(cdmovimentacaoestoque == null || dtCancelamento == null || StringUtils.isBlank(observacao))
			throw new SinedException("Os par�metros whereInCdMovimentacaoestoque, dtCancelamento e observacao n�o podem ser nulos.");
		
		getJdbcTemplate().update("UPDATE movimentacaoestoque SET dtcancelamento = ?, observacao = ? " +
									"WHERE cdmovimentacaoestoque = ? ", new Object[]{dtCancelamento, observacao, cdmovimentacaoestoque});
	}
	
	
	/**
	 * M�todo que atualiza a quantidade de material quando alterado em uma coleta avulsa.
	 * @param cdmovimentacaoestoque
	 * @param quantidade
	 * @since 30/06/2015
	 * @author Danilo Guimar�es
	 */
	public void updateQuantidadeMovimentacao(Integer cdmovimentacaoestoque, Double quantidade, Double valor){
		if(cdmovimentacaoestoque == null || quantidade == null || valor == null)
			throw new SinedException("O par�metros cdmovimentacoestoque, quantidade e valor n�o podem ser nulos.");
		
		getJdbcTemplate().update("UPDATE movimentacaoestoque SET qtde = ?, valor = ? " +
									"WHERE cdmovimentacaoestoque = ? ", new Object[]{quantidade, valor, cdmovimentacaoestoque});
	}

	public void updateCdRateio(Integer cdEstoque, Integer cdrateio) {
		getJdbcTemplate().update("UPDATE movimentacaoestoque SET cdrateio = ?" +
				"WHERE cdmovimentacaoestoque = ? ", new Object[]{cdrateio, cdEstoque});
	}
	/**
	* M�todo que retorna as movimenta��es de estoque da requisi��o
	*
	* @param whereIn
	* @param somenteRequisicao
	* @return
	* @since 04/09/2015
	* @author Luiz Fernando
	*/
	public List<Movimentacaoestoque> findByRequisicaomaterial(String whereIn, boolean somenteRequisicao) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Requisi��o de material n�o pode ser nula");
		
		QueryBuilder<Movimentacaoestoque> query = query()
				.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoquetipo.cdmovimentacaoestoquetipo, " +
						"material.cdmaterial, material.valorindenizacao, material.valorcusto, movimentacaoestoque.valor, " +
						"movimentacaoestoque.qtde, movimentacaoestoque.dtcancelamento ")
				.join("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
				.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
				.join("movimentacaoestoque.material material")
				.where("movimentacaoestoque.dtcancelamento is null")
				.whereIn("movimentacaoestoqueorigem.requisicaomaterial.cdrequisicaomaterial", whereIn);
		
		if(somenteRequisicao){
			query
				.leftOuterJoin("movimentacaoestoqueorigem.romaneio romaneio")
				.where("romaneio is null");
		}
		
		return query.list();
	}
	
	/**
	* M�todo que verifica se existe movimenta��o de estoque de acordo com a venda
	*
	* @param venda
	* @return
	* @since 07/10/2015
	* @author Luiz Fernando
	*/
	public Boolean existeMovimentacaoestoque(Venda venda, Expedicao expedicao) {
		return newQueryBuilderSined(Long.class)
			.from(Movimentacaoestoque.class)
			.select("count(*)")
			.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
			.where("movimentacaoestoqueorigem.venda = ?", venda)
			.where("movimentacaoestoqueorigem.expedicao = ?", expedicao, expedicao != null && expedicao.getCdexpedicao() != null)
			.where("movimentacaoestoque.dtcancelamento is null")
			.unique() > 0;
	}
	
	public Boolean existeMovimentacaoestoqueSomenteVenda(Venda venda) {
		return newQueryBuilderSined(Long.class)
			.from(Movimentacaoestoque.class)
			.select("count(*)")
			.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
			.where("movimentacaoestoqueorigem.venda = ?", venda)
			.where("movimentacaoestoqueorigem.expedicao is null")
			.where("movimentacaoestoque.dtcancelamento is null")
			.unique() > 0;
	}
	
	public List<Movimentacaoestoque> buscarPorCentroCusto(boolean csv, Centrocusto centroCusto, Date dtInicio, Date dtFim, Empresa empresa) {
		return query()
				.select("movimentacaoestoque.cdmovimentacaoestoque,rateio.cdrateio")
				.leftOuterJoin("movimentacaoestoque.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.where("listaRateioitem.projeto is null", !csv)
				.openParentheses()
					.where("movimentacaoestoque.empresa =?",empresa)
						.or()
					.where("movimentacaoestoque.empresa is null")
				.closeParentheses()
				.where("listaRateioitem.centrocusto = ?", centroCusto)
				.where("movimentacaoestoque.dtmovimentacao >= ?", dtInicio)
				.where("movimentacaoestoque.dtmovimentacao <= ?", dtFim)
				.where("movimentacaoestoque.dtcancelamento is null")
				.list();
	}
	/**
	* M�todo que carrega as movimenta��es de entrada da ordem de produ��o
	*
	* @param whereIn
	* @return
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public List<Movimentacaoestoque> findByProducaoordem(String whereIn, Movimentacaoestoquetipo movimentacaoestoquetipo, Boolean registrarproducao, Boolean estornoproducao) {
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.qtde, movimentacaoestoque.valor, " +
						"movimentacaoestoquetipo.cdmovimentacaoestoquetipo, " +
						"material.cdmaterial, materialclasse.cdmaterialclasse, " +
						"projeto.cdprojeto, empresa.cdpessoa, loteestoque.cdloteestoque, localarmazenagem.cdlocalarmazenagem, " +
						"producaoordem.cdproducaoordem, producaoagenda.cdproducaoagenda," +
						"producaoOrdemMaterial.cdproducaoordemmaterial ")
				.join("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
				.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
				.join("movimentacaoestoqueorigem.producaoordem producaoordem")
				.leftOuterJoin("movimentacaoestoqueorigem.producaoOrdemMaterial producaoOrdemMaterial")
				.join("movimentacaoestoque.material material")
				.join("movimentacaoestoque.materialclasse materialclasse")
				.leftOuterJoin("movimentacaoestoqueorigem.producaoagenda producaoagenda")
				.leftOuterJoin("movimentacaoestoque.projeto projeto")
				.leftOuterJoin("movimentacaoestoque.empresa empresa")
				.leftOuterJoin("movimentacaoestoque.loteestoque loteestoque")
				.leftOuterJoin("movimentacaoestoque.localarmazenagem localarmazenagem")
				.where("movimentacaoestoque.dtcancelamento is null")
				.whereIn("producaoordem.cdproducaoordem", whereIn)
				.where("movimentacaoestoquetipo = ?", movimentacaoestoquetipo)
				.where("coalesce(movimentacaoestoqueorigem.registrarproducao, false) = ?", registrarproducao)
				.where("coalesce(movimentacaoestoqueorigem.estornoproducao, false) = ?", estornoproducao)
				.list();
	}

	/**
	* M�todo que seta a data do cancelamento das movimenta��es vinculada a agenda de produ��o e ao material
	*
	* @param whereInProducaoagenda
	* @param whereInMaterial
	* @since 15/04/2016
	* @author Luiz Fernando
	*/
	public void updateDataCancelamentoMateriaprimaByProducaoagenda(String whereInProducaoagenda, String whereInMaterial) {
		if(StringUtils.isBlank(whereInProducaoagenda.toString()) || StringUtils.isBlank(whereInMaterial.toString())){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		String sql = "update Movimentacaoestoque set dtcancelamento = ? where cdmovimentacaoestoque in (" +
					 " select me.cdmovimentacaoestoque " +
					 " from movimentacaoestoque me " +
					 " join movimentacaoestoqueorigem meo on meo.cdmovimentacaoestoqueorigem = me.cdmovimentacaoestoqueorigem " +
					 " where meo.cdproducaoagenda in ("+whereInProducaoagenda+") and me.cdmaterial in ("+whereInMaterial+")" +
					 ")";

		getJdbcTemplate().update(sql, new Object[]{new Timestamp(System.currentTimeMillis())});
		
	}

	/**
	 * Busca o estoque atual da empresa
	 *
	 * @param empresa
	 * @param localarmazenagem 
	 * @return
	 * @author Rodrigo Freitas
	 * @param produtos 
	 * @since 28/04/2016
	 */
	@SuppressWarnings("unchecked")
	public List<ConsultarEstoqueWSBean> findForWSSOAP(Empresa empresa, Localarmazenagem localarmazenagem, String produtos) {
		StringBuilder sql = new StringBuilder()
			.append("SELECT m.cdmaterial, um.simbolo, m.valorcusto, sum(v.qtdedisponivel) AS estoqueatual ")
			.append("from vgerenciarmaterial v ")
			.append("join material m on m.cdmaterial = v.cdmaterial ")
			.append("join unidademedida um on um.cdunidademedida = m.cdunidademedida ")
			.append("where v.cdempresa = ").append(empresa.getCdpessoa()).append(" ");
		
		if(localarmazenagem != null && localarmazenagem.getCdlocalarmazenagem() != null){
			sql.append(" and v.cdlocalarmazenagem = ").append(localarmazenagem.getCdlocalarmazenagem()).append(" ");
		}
		
		if(produtos != null && !produtos.trim().equals("")){
			sql.append("and m.cdmaterial in (").append(produtos).append(") ");
		}
		
		sql.append("group by m.cdmaterial, um.simbolo, m.valorcusto");

		SinedUtil.markAsReader();
		List<ConsultarEstoqueWSBean> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public ConsultarEstoqueWSBean mapRow(ResultSet rs, int rowNum) throws SQLException {
				ConsultarEstoqueWSBean estoque = new ConsultarEstoqueWSBean();
				
				estoque.setCodigo(rs.getInt("cdmaterial"));
				estoque.setUnidademedida(rs.getString("simbolo"));
				estoque.setQuantidade(rs.getDouble("estoqueatual"));
				estoque.setCusto(rs.getDouble("valorcusto"));
				
				return estoque;
			}
		});
		
		return lista;
	}
	
	/**
	 * M�todo que gera movimenta��es de entrada/sa�da para igualar os estoques do ERP em rela��o ao WMS. 
	 * 
	 * @param bean
	 * @return
	 * @author Rafael Salvio
	 */
	public void ajusteEstoqueFromWms(ConsultarEstoqueBean bean) {
		Localarmazenagemempresa localarmazenagemempresa = localarmazenagemempresaService.findForAjusteEstoqueWMS(new Cnpj(bean.getCnpjDeposito()));
		if(localarmazenagemempresa == null || localarmazenagemempresa.getCdlocalarmazenagemempresa() == null 
			|| localarmazenagemempresa.getEmpresa() == null || localarmazenagemempresa.getEmpresa().getCdpessoa() == null
			|| localarmazenagemempresa.getLocalarmazenagem() == null || localarmazenagemempresa.getLocalarmazenagem().getCdlocalarmazenagem() == null){
			throw new SinedException("N�o foi poss�vel localizar o local de armazenagem e empresa para lan�amento de ajuste.");
		} else if(bean.getWhereInCdmaterial() == null || bean.getWhereInCdmaterial().trim().isEmpty() 
				|| bean.getQtdePorCdmaterial() == null || bean.getQtdePorCdmaterial().trim().isEmpty()){
			throw new SinedException("N�o foi poss�vel localizar a lista de materiais e suas respectivas quantidades para ajuste.");
		}
		
		final String[] arrayCdmaterial = bean.getWhereInCdmaterial().split(",");
		final String[] arrayQtdePorMaterial = bean.getQtdePorCdmaterial().split(",");
		final String[] arrayCpfPorMaterial = bean.getCpfPorCdmaterial().split(",");
		if(arrayCdmaterial.length != arrayQtdePorMaterial.length){
			throw new SinedException("A lista de materiais e a lista de quantidade por material possuem tamanhos diferentes.");
		} else if(arrayCdmaterial.length != arrayCpfPorMaterial.length){
			throw new SinedException("A lista de materiais e a lista de cpf por material possuem tamanhos diferentes.");
		}
		
		Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
		if(Boolean.TRUE.equals(bean.getAjusteAutomaticoERP())){
			movimentacaoestoqueorigem.setAjusteautomaticowms(Boolean.TRUE);
		} else{
			movimentacaoestoqueorigem.setAjustemanualwms(Boolean.TRUE);
		}
		movimentacaoestoqueorigemService.saveOrUpdate(movimentacaoestoqueorigem);
		
		getJdbcTemplate().batchUpdate("insert into movimentacaoestoque " +
													"(cdmovimentacaoestoque," +
													"cdmovimentacaoestoqueorigem," +
													"cdmaterial," +
													"qtde," +
													"cdmovimentacaoestoquetipo," +
													"cdusuarioaltera," +
													"dtaltera," +
													"dtmovimentacao," +
													"cdlocalarmazenagem," +
													"cdempresa," +
													"cdmaterialclasse," +
													"observacao)" +
													"VALUES(" +
													"nextval('sq_movimentacaoestoque')," +
													movimentacaoestoqueorigem.getCdmovimentacaoestoqueorigem() + "," +
													"?,?,?,(select cdpessoa from pessoa where cpf like ?),current_timestamp,current_date," +
													localarmazenagemempresa.getLocalarmazenagem().getCdlocalarmazenagem() + "," +
													localarmazenagemempresa.getEmpresa().getCdpessoa() + "," +
													Materialclasse.PRODUTO.getCdmaterialclasse() + "," +
													"'Ajuste " + (Boolean.TRUE.equals(bean.getAjusteAutomaticoERP()) ? "autom�tico" : "manual") + 
													" de estoque realizado pelo WMS')", new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Integer cdmaterial = Integer.parseInt(arrayCdmaterial[i]);
				Integer qtdeMaterial = Integer.parseInt(arrayQtdePorMaterial[i]);
				String cpfMaterial = arrayCpfPorMaterial[i];
				
				ps.setInt(1, cdmaterial);
				ps.setInt(2, Math.abs(qtdeMaterial));
				ps.setInt(3, qtdeMaterial < 0 
								? Movimentacaoestoquetipo.SAIDA.getCdmovimentacaoestoquetipo() 
								: Movimentacaoestoquetipo.ENTRADA.getCdmovimentacaoestoquetipo());
				ps.setString(4, cpfMaterial);
			}
			@Override
			public int getBatchSize() {
				return arrayCdmaterial.length;
			}
		});
	}
	
	public List<DinamicaRebanhoBean> findForDinamicaRebanho(DinamicaRebanhoFiltro filtro){
		String strLocalarmazenagem = filtro.getLocalarmazenagem() != null? filtro.getLocalarmazenagem().getCdlocalarmazenagem().toString(): "null";
		String strEmpresa = filtro.getEmpresa() != null? filtro.getEmpresa().getCdpessoa().toString(): "null";
		String strDataInicio = SinedDateUtils.toStringPostgre(filtro.getDtinicio());
		String strDataFim = SinedDateUtils.toStringPostgre(filtro.getDtfim());
		String trazerMovimentacoesSemEmpresa = filtro.getEmpresa() != null? "false": "true";
		StringBuilder sql = new StringBuilder()
		.append("Select a.material,  a.qtdenascimento, a.qtdemorte, a.qtdevenda, a.qtdeentradatransferencia, ")
		.append("       a.qtdesaidatransferencia, a.qtdecompra, a.qtdeentradamudancacategoria, a.qtdesaidamudancacategoria, ")
		.append("       a.cdmaterial,")
		.append("		(select material_disponivel_local(a.cdmaterial, 'PO', "+strLocalarmazenagem+","+strEmpresa+",null,true,null,true,'"+strDataInicio+"',true, "+trazerMovimentacoesSemEmpresa+")) as qtdeestoqueinicial, ")
		.append("		(select material_disponivel_local(a.cdmaterial, 'PO', "+strLocalarmazenagem+","+strEmpresa+",null,true,null,true,'"+strDataFim+"',true, "+trazerMovimentacoesSemEmpresa+")) as qtdeestoquefinal ")
		.append(" from(Select m.nome as material, m.cdmaterial, sum(CASE me.movimentacaoanimalmotivo when 3 then me.qtde else 0 end) as qtdenascimento, ")
		.append("	   		  sum(CASE me.movimentacaoanimalmotivo when 5 then me.qtde else 0 end) as qtdemorte, ")
		.append("       	  sum(CASE me.movimentacaoanimalmotivo when 4 then me.qtde else 0 end) as qtdevenda, ")
		.append("       	  sum(CASE me.movimentacaoanimalmotivo when 0 then ")
		.append("       			case me.cdmovimentacaoestoquetipo when 1 then me.qtde else 0 end ")
		.append("            		else 0 end) as qtdeentradatransferencia, ")
		.append("      		  sum(CASE me.movimentacaoanimalmotivo when 0 then ")
		.append("       			case me.cdmovimentacaoestoquetipo when 2 then me.qtde else 0 end ")
		.append("            		else 0 end) as qtdesaidatransferencia, ")
		.append("       	  sum(CASE me.movimentacaoanimalmotivo when 2 then me.qtde else 0 end) as qtdecompra, ")
		.append("       	  sum(CASE me.movimentacaoanimalmotivo when 1 then ")
		.append("       			case me.cdmovimentacaoestoquetipo when 1 then me.qtde else 0 end ")
		.append("            		else 0 end) as qtdeentradamudancacategoria, ")
		.append("       	  sum(CASE me.movimentacaoanimalmotivo when 1 then ")
		.append("       			case me.cdmovimentacaoestoquetipo when 2 then me.qtde else 0 end ")
		.append("            		else 0 end) as qtdesaidamudancacategoria ")
		.append("  		from movimentacaoestoque me ")
		.append("  		join material m on m.cdmaterial = me.cdmaterial ")
		.append("  		left outer join materialcategoria mc on mc.cdmaterialcategoria = m.cdmaterialcategoria ")
		.append("  		join materialgrupo mg on mg.cdmaterialgrupo = m.cdmaterialgrupo ")
		.append("  		left outer join vmaterialcategoria vmc on vmc.cdmaterialcategoria = mc.cdmaterialcategoria ")
		.append(" 		Where coalesce(mg.animal, false) = true ")
		.append("   	  and me.dtcancelamento is null ")
		.append("   	  and me.dtmovimentacao between '").append(strDataInicio).append("' and '").append(strDataFim).append("'");

		if (filtro.getEmpresa()!=null){
			sql.append("   	  and me.cdempresa = ").append(filtro.getEmpresa().getCdpessoa()).append(" ");			
		}
	
		if(filtro.getLocalarmazenagem() != null && filtro.getLocalarmazenagem().getCdlocalarmazenagem() != null){
			sql.append("   	  and me.cdlocalarmazenagem = ").append(strLocalarmazenagem).append(" ");
		}
		
		if(filtro.getMaterialcategoria() != null && filtro.getMaterialcategoria().getCdmaterialcategoria() != null){
			sql.append("   	  and vmc.identificador like ('").append(filtro.getMaterialcategoria().getIdentificador()).append("%')");
		}
		
		if(filtro.getMaterialgrupo() != null && filtro.getMaterialgrupo().getCdmaterialgrupo() != null){
			sql.append("   	  and mg.cdmaterialgrupo = ").append(filtro.getMaterialgrupo().getCdmaterialgrupo()).append(" ");
		}
		
		if(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null){
			sql.append("   	  and m.cdmaterial = ").append(filtro.getMaterial().getCdmaterial()).append(" ");
		}
		
		sql.append("   	Group By m.nome, m.cdmaterial) as a");

		SinedUtil.markAsReader();
		@SuppressWarnings("unchecked")
		List<DinamicaRebanhoBean> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public DinamicaRebanhoBean mapRow(ResultSet rs, int rowNum) throws SQLException {
				DinamicaRebanhoBean estoque = new DinamicaRebanhoBean();
				estoque.setMaterial(rs.getString("material"));
				estoque.setQtdeestoqueinicial(rs.getBigDecimal("qtdeestoqueinicial"));
				estoque.setQtdenascimento(rs.getBigDecimal("qtdenascimento"));
				estoque.setQtdemorte(rs.getBigDecimal("qtdemorte"));
				estoque.setQtdeentradatransferencia(rs.getBigDecimal("qtdeentradatransferencia"));
				estoque.setQtdesaidatransferencia(rs.getBigDecimal("qtdesaidatransferencia"));
				estoque.setQtdevenda(rs.getBigDecimal("qtdevenda"));
				estoque.setQtdecompra(rs.getBigDecimal("qtdecompra"));
				estoque.setQtdeentradamudancacategoria(rs.getBigDecimal("qtdeentradamudancacategoria"));
				estoque.setQtdesaidamudancacategoria(rs.getBigDecimal("qtdesaidamudancacategoria"));
				estoque.setQtdeestoquefinal(rs.getBigDecimal("qtdeestoquefinal"));
				

				return estoque;
			}
		});
		
		return lista;
	}
	
	public List<Movimentacaoestoque> findForIndenizacaoContrato(String whereIn, Material material){
		return query()
			.select("movimentacaoestoque.valorcusto, movimentacaoestoque.dtmovimentacao, movimentacaoestoque.qtde, contratofechamento.cdcontrato, contratofechamento.descricao, contratofechamento.identificador, " +
					"material.cdmaterial, material.identificacao, material.valorindenizacao, material.valorcusto, material.nome, cliente.nome, cliente.razaosocial")
			.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
			.join("movimentacaoestoqueorigem.romaneio romaneio")
			.join("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
			.join("romaneio.listaRomaneioorigem romaneioorigem")
			.join("romaneioorigem.contratofechamento contratofechamento")
			.join("contratofechamento.cliente cliente")
			.join("movimentacaoestoque.localarmazenagem localarmazenagem")
			.join("movimentacaoestoque.material material")
			.whereIn("contratofechamento.cdcontrato", whereIn)
			.where("material = ?", material)
			.where("localarmazenagem.indenizacao = ?", Boolean.TRUE)
			.where("movimentacaoestoque.dtcancelamento is null")
			.where("movimentacaoestoquetipo = ?", Movimentacaoestoquetipo.ENTRADA)
			.where("romaneio.indenizacao = ?", Boolean.TRUE)
			.where("exists (select 1 from Contratofaturalocacao cfl where cfl.contrato=contratofechamento and cfl.indenizacao=true)")
			.list();
	}
	
	public Movimentacaoestoque loadUltimaMovimentacaoestoque(Material material, Empresa empresa){
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
			
		QueryBuilder<Movimentacaoestoque> query = querySined()
			.setUseWhereEmpresa(false)
			.setUseWhereProjeto(false)
			.setUseWhereFornecedorEmpresa(false)
			.setUseWhereClienteEmpresa(false)
			.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.dtmovimentacao, movimentacaoestoque.valor, movimentacaoestoque.valorcusto, " +
					"material.cdmaterial, empresa.cdpessoa ")
			.leftOuterJoin("movimentacaoestoque.material material")
			.leftOuterJoin("movimentacaoestoque.empresa empresa")
			.where("material = ?", material)
			.where("movimentacaoestoque.movimentacaoestoquetipo = ?", Movimentacaoestoquetipo.ENTRADA)
			.where("movimentacaoestoque.dtcancelamento is null");
		
		if(empresa != null) {
			query.where("empresa = ?", empresa);
		}
		
		return query
				.orderBy("movimentacaoestoque.dtmovimentacao desc, movimentacaoestoque.cdmovimentacaoestoque desc")
				.setMaxResults(1)
				.unique();
	}
	
	public List<Movimentacaoestoque> findByInventario(String whereIn) {
		return query()
				.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.dtmovimentacao")
				.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
				.join("movimentacaoestoqueorigem.inventario inventario")
				.whereIn("inventario.cdinventario", whereIn)
				.where("movimentacaoestoque.dtcancelamento is null")
				.list();
	}
	
	public List<Movimentacaoestoque> findForGerarLancamentoContabilContaGerencial(GerarLancamentoContabilFiltro filtro, Contagerencial contagerencial, String whereNotIn, String whereNotInRatioitem, OrigemMovimentacaoEstoqueEnum origemMovimentacaoEstoque, boolean considerarRateio) {
		QueryBuilder<Movimentacaoestoque> query = query()
				.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.dtmovimentacao, movimentacaoestoque.valor, movimentacaoestoque.valorcusto, contacontabil.cdcontacontabil, " +
						"listaRateioitem.cdrateioitem, listaRateioitem.percentual, listaRateioitem.valor , contagerencial.cdcontagerencial, " +
						"contagerencial.nome, centrocusto.cdcentrocusto, centrocusto.nome, projeto.cdprojeto, projeto.nome, entregadocumento.numero, notafiscalproduto.numero, material.valorcusto")
				.leftOuterJoin("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
				.leftOuterJoin("movimentacaoestoque.material material")
				.leftOuterJoin("material.contaContabil contacontabil")
				.leftOuterJoin("movimentacaoestoqueorigem.entregamaterial entregamaterial")
				.leftOuterJoin("movimentacaoestoqueorigem.notafiscalproduto notafiscalproduto")
				.leftOuterJoin("entregamaterial.entregadocumento entregadocumento")
				.leftOuterJoin("movimentacaoestoque.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.leftOuterJoin("listaRateioitem.contagerencial contagerencial")
				.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
				.leftOuterJoin("listaRateioitem.projeto projeto")
				.where("movimentacaoestoque.empresa=?", filtro.getEmpresa())
				.where("movimentacaoestoque.dtmovimentacao>=?", filtro.getDtPeriodoInicio())
				.where("movimentacaoestoque.dtmovimentacao<=?", filtro.getDtPeriodoFim())
				.where("listaRateioitem.projeto=?", filtro.getProjeto())
				.where("contagerencial=?", contagerencial)
				.where("movimentacaoestoque.cdmovimentacaoestoque not in (" + whereNotIn +  ")", !whereNotIn.trim().equals(""))
				.where("listaRateioitem.cdrateioitem not in (" + whereNotInRatioitem +  ")", !whereNotInRatioitem.trim().equals(""));
				if(considerarRateio){
					query.where("not exists (select 1 from Movimentacaocontabilorigem mco where mco.movimentacaoestoque=movimentacaoestoque and mco.rateioitem=listaRateioitem and mco.movimentacaocontabil.tipoLancamento in (?,?))", new Object[]{MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_MOVIMENTACAO_ESTOQUE, MovimentacaocontabilTipoLancamentoEnum.MOVIMENTACAO_ESTOQUE_ORIGEM});
				} else{
					query.where("not exists (select 1 from Movimentacaocontabilorigem mco where mco.movimentacaoestoque=movimentacaoestoque and mco.movimentacaocontabil.tipoLancamento in (?,?))", new Object[]{MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_MOVIMENTACAO_ESTOQUE, MovimentacaocontabilTipoLancamentoEnum.MOVIMENTACAO_ESTOQUE_ORIGEM});
				}
		
		if(origemMovimentacaoEstoque != null){
			if(origemMovimentacaoEstoque.equals(OrigemMovimentacaoEstoqueEnum.CONSUMO_MATERIAL)){
				query.where("movimentacaoestoqueorigem.consumo = true");
			} else if(origemMovimentacaoEstoque.equals(OrigemMovimentacaoEstoqueEnum.REQUISICAO_MATERIAL)){
				query.where("movimentacaoestoqueorigem.requisicaomaterial is not null");
			} else if(origemMovimentacaoEstoque.equals(OrigemMovimentacaoEstoqueEnum.RECEBIMENTO_MATERIAL)) {
				query.where("movimentacaoestoqueorigem.entregamaterial is not null");
			} else if(origemMovimentacaoEstoque.equals(OrigemMovimentacaoEstoqueEnum.VENDA_MATERIAL)) {
				query.openParentheses()
				.where("movimentacaoestoqueorigem.venda is not null")
				.or()
				.where("movimentacaoestoqueorigem.pedidovenda is not null")
				.or()
				.where("movimentacaoestoqueorigem.expedicaoproducao is not null")
				.closeParentheses();
			} else if(origemMovimentacaoEstoque.equals(OrigemMovimentacaoEstoqueEnum.ORDEM_PRODUCAO_ENTRADA)) {
				query.where("movimentacaoestoque.movimentacaoestoquetipo =?", Movimentacaoestoquetipo.ENTRADA);
				query.openParentheses()
				.where("movimentacaoestoqueorigem.producaoordem is not null")
				.or()
				.where("movimentacaoestoqueorigem.producaoagenda is not null")
				.closeParentheses();
			} else if(origemMovimentacaoEstoque.equals(OrigemMovimentacaoEstoqueEnum.ORDEM_PRODUCAO_SAIDA)) {
				query.where("movimentacaoestoque.movimentacaoestoquetipo =?", Movimentacaoestoquetipo.SAIDA);
				query.openParentheses()
				.where("movimentacaoestoqueorigem.producaoordem is not null")
				.or()
				.where("movimentacaoestoqueorigem.producaoagenda is not null")
				.or()
				.where("movimentacaoestoqueorigem.expedicaoproducao is not null")
				.closeParentheses();
			} else if(origemMovimentacaoEstoque.equals(OrigemMovimentacaoEstoqueEnum.COLETA)){
				query.where("movimentacaoestoqueorigem.coleta is not null");
			} else if(origemMovimentacaoEstoque.equals(OrigemMovimentacaoEstoqueEnum.ORDEM_SERVICO_VETERINARIA)){
				query.where("movimentacaoestoqueorigem.ordemservicoveterinaria is not null");
			} else if(origemMovimentacaoEstoque.equals(OrigemMovimentacaoEstoqueEnum.REQUISICAO)){
				query.where("movimentacaoestoqueorigem.requisicao is not null");
			} else if(origemMovimentacaoEstoque.equals(OrigemMovimentacaoEstoqueEnum.AVULSO)){
				query.where("movimentacaoestoqueorigem.usuario is not null");
			} else if(origemMovimentacaoEstoque.equals(OrigemMovimentacaoEstoqueEnum.EXPEDICAO_AVULSA)){
				query
				.leftOuterJoin("movimentacaoestoqueorigem.expedicao expedicao")
				.leftOuterJoin("expedicao.listaExpedicaoitem listaExpedicaoitem")
				.where("listaExpedicaoitem.vendamaterial is null");
			}
		}

		return query.list();
	}
	
	public List<Movimentacaoestoque> findbyVendaList(Venda venda, Material material) {
		if(venda == null || venda.getCdvenda() == null || material == null || material.getCdmaterial() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query() 
			.select("movimentacaoestoquetipo.nome, movimentacaoestoquetipo.cdmovimentacaoestoquetipo, materialclasse.cdmaterialclasse, material.cdmaterial," +
					"materialclasse.nome, movimentacaoestoque.qtde, movimentacaoestoque.dtmovimentacao," +
					"movimentacaoestoque.valor, movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoqueorigem.cdmovimentacaoestoqueorigem")
			.join("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
			.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
			.leftOuterJoin("movimentacaoestoqueorigem.venda venda")
			.join("movimentacaoestoque.material material")
			.join("movimentacaoestoque.materialclasse materialclasse")
			.where("venda = ?", venda)
			.where("material = ?", material)
			.where("movimentacaoestoque.dtcancelamento is null")
			.list();
	}
	public boolean existeMovimentacaoEstoqueMaterialLote(Loteestoque loteestoque, Material material) {
			return new QueryBuilderSined<Long>(getHibernateTemplate())
				.from(Movimentacaoestoque.class)
				.select("count(*)")
				.join("movimentacaoestoque.material material ")
				.join("movimentacaoestoque.loteestoque loteestoque ")
				.where("loteestoque = ?", loteestoque)
				.where("material = ?", material)
				.where("movimentacaoestoque.dtcancelamento is null")
				.unique() > 0;
	}
	
	public List<Movimentacaoestoque> findMovimentacoesSaidaMateriasProducao(SpedarquivoFiltro filtro) {
		return querySined()
				.setUseReadOnly(true)
				.select("movimentacaoestoque.cdmovimentacaoestoque, producaoordem.cdproducaoordem, producaoagenda.cdproducaoagenda")
				.join("movimentacaoestoque.movimentacaoestoquetipo movimentacaoestoquetipo")
				.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
				.leftOuterJoin("movimentacaoestoqueorigem.producaoordem producaoordem")
				.leftOuterJoin("movimentacaoestoqueorigem.producaoagenda producaoagenda")
				.where("movimentacaoestoquetipo = ?", Movimentacaoestoquetipo.SAIDA)
				.where("movimentacaoestoque.dtcancelamento is null")
				.openParentheses()
					.where("producaoordem is not null")
					.or()
					.where("producaoagenda is not null")
				.closeParentheses()
				.where("movimentacaoestoque.empresa = ?", filtro.getEmpresa())
				.where("movimentacaoestoque.dtmovimentacao >= ?", filtro.getDtinicio())
				.where("movimentacaoestoque.dtmovimentacao <= ?", filtro.getDtfim())
				.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Movimentacaoestoque> findMateriasPrimasOrdemAvulsaForK235(SpedarquivoFiltro filtro, Integer cdProducaoOrdemOuAgenda, Integer cdMaterialProducao, List<Tipoitemsped> listaTipoitemsped) {
		StringBuilder sql = new StringBuilder();
		sql.append("select me.cdmaterial, me.dtmovimentacao, m.identificacao, sum(me.qtde) as qtde ");
		sql.append("from movimentacaoestoque me ");
		sql.append("join movimentacaoestoqueorigem meo on meo.cdmovimentacaoestoqueorigem = me.cdmovimentacaoestoqueorigem ");
		sql.append("join material m on m.cdmaterial = me.cdmaterial ");
		sql.append("where meo.cdproducaoordem = " + cdProducaoOrdemOuAgenda);
		sql.append(" and me.cdmovimentacaoestoquetipo = " + Movimentacaoestoquetipo.SAIDA.getCdmovimentacaoestoquetipo());
		sql.append(" and me.dtcancelamento is null ");
		sql.append(" and me.cdmaterial <> " + cdMaterialProducao);
		sql.append("and m.tipoitemsped in (" + CollectionsUtil.listAndConcatenate(listaTipoitemsped, "id", ",") + ") ");
		sql.append("and me.dtmovimentacao > '" + filtro.getDtinicio() + "' ");
		sql.append("and me.dtmovimentacao < '" + filtro.getDtfim() + "' ");	
		sql.append("group by me.cdmaterial, m.identificacao, me.dtmovimentacao ");
		sql.append("having (sum(me.qtde) > 0);");
		
		List<Movimentacaoestoque> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Movimentacaoestoque mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Movimentacaoestoque(rs.getInt("cdmaterial"), rs.getString("identificacao"), rs.getDate("dtmovimentacao"), rs.getDouble("qtde"));
			}
		});
		
		return lista;	
	}
	
	@SuppressWarnings("unchecked")
	public List<Movimentacaoestoque> findMateriasPrimasOrdemAgenda(SpedarquivoFiltro filtro, Integer cdProducaoOrdemOuAgenda, Integer cdMaterialProducao, List<Tipoitemsped> listaTipoitemsped) {
		StringBuilder sql = new StringBuilder();
		sql.append("select me.cdmaterial, me.dtmovimentacao, m.identificacao, sum(me.qtde) as qtde ");
		sql.append("from movimentacaoestoque me ");
		sql.append("join movimentacaoestoqueorigem meo on meo.cdmovimentacaoestoqueorigem = me.cdmovimentacaoestoqueorigem ");
		sql.append("join producaoordem po on po.cdproducaoordem = meo.cdproducaoordem ");
		sql.append("join producaoordemmaterial pom on pom.cdproducaoordem = po.cdproducaoordem ");
		sql.append("join producaoordemmaterialorigem pomo on pomo.cdproducaoordemmaterial = pom.cdproducaoordemmaterial ");
		sql.append("join producaoagenda pa on pa.cdproducaoagenda = pomo.cdproducaoagenda ");
		sql.append("join material m on m.cdmaterial = me.cdmaterial ");
		sql.append("where pa.cdproducaoagenda = " + cdProducaoOrdemOuAgenda);
		sql.append("and pom.cdproducaoordemmaterial = meo.cdproducaoordemmaterial ");
		sql.append(" and me.cdmovimentacaoestoquetipo = " + Movimentacaoestoquetipo.SAIDA.getCdmovimentacaoestoquetipo());
		sql.append(" and me.dtcancelamento is null ");
		sql.append(" and me.cdmaterial <> " + cdMaterialProducao);
		sql.append("and m.tipoitemsped in (" + CollectionsUtil.listAndConcatenate(listaTipoitemsped, "id", ",") + ") ");
		sql.append("and me.dtmovimentacao > '" + filtro.getDtinicio() + "' ");
		sql.append("and me.dtmovimentacao < '" + filtro.getDtfim() + "' ");		
		sql.append("group by me.cdmaterial, m.identificacao, me.dtmovimentacao ");
		sql.append("having (sum(me.qtde) > 0);");
		
		List<Movimentacaoestoque> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Movimentacaoestoque mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Movimentacaoestoque(rs.getInt("cdmaterial"),  rs.getString("identificacao"), rs.getDate("dtmovimentacao"), rs.getDouble("qtde"));
			}
		});
		
		return lista;	
	}

	public List<Movimentacaoestoque> findByNotaFiscalProdutoItem(String whereIn) {
		if(StringUtils.isEmpty(whereIn)){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.dtmovimentacao, movimentacaoestoque.valor, " +
							"material.cdmaterial, movimentacaoestoqueorigem.cdmovimentacaoestoqueorigem, notafiscalprodutoitem.cdnotafiscalprodutoitem")
					.join("movimentacaoestoque.material material")
					.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
					.join("movimentacaoestoqueorigem.notaFiscalProdutoItem notafiscalprodutoitem")
					.where("movimentacaoestoque.movimentacaoestoquetipo = ?", Movimentacaoestoquetipo.ENTRADA)
					.where("movimentacaoestoque.dtcancelamento is null")
					.whereIn("notafiscalprodutoitem.cdnotafiscalprodutoitem", whereIn)
					.list();
	}
	
	public List<Movimentacaoestoque> findForRegistroK210(SpedarquivoFiltro filtro, String whereIn) {
		List<Tipoitemsped> listaTipoitemsped = new ArrayList<Tipoitemsped>();
		listaTipoitemsped.add(Tipoitemsped.MERCADORIA_REVENDA);
		listaTipoitemsped.add(Tipoitemsped.MATERIA_PRIMA);
		listaTipoitemsped.add(Tipoitemsped.EMBALAGEM);
		listaTipoitemsped.add(Tipoitemsped.PRODUTO_ACABADO);
		listaTipoitemsped.add(Tipoitemsped.PRODUTO_PROCESSO);
		listaTipoitemsped.add(Tipoitemsped.SUBPRODUTO);
		listaTipoitemsped.add(Tipoitemsped.OUTROS_INSUMOS);		
		
		return querySined()
				.setUseReadOnly(true)
				.select("movimentacaoestoque.cdmovimentacaoestoque, movimentacaoestoque.qtde, material.cdmaterial, material.identificacao, material.tipoitemsped, " +
						"materialOrigem.cdmaterial, entregamaterial.cdentregamaterial")
				.join("movimentacaoestoque.material material")
				.join("movimentacaoestoque.materialOrigem materialOrigem")
				.join("movimentacaoestoque.movimentacaoestoqueorigem movimentacaoestoqueorigem")
				.join("movimentacaoestoqueorigem.entregamaterial entregamaterial")
				.leftOuterJoin("movimentacaoestoque.empresa empresa")
				.where("movimentacaoestoque.movimentacaoestoquetipo = ?", Movimentacaoestoquetipo.ENTRADA)
				.where("movimentacaoestoque.dtcancelamento is null")
				.openParentheses()
					.where("empresa = ?", filtro.getEmpresa())
					.or()
					.where("empresa is null")
				.closeParentheses()
				.where("movimentacaoestoque.dtmovimentacao >= ?", filtro.getDtinicio())
				.where("movimentacaoestoque.dtmovimentacao <= ?", filtro.getDtfim())
				.whereIn("materialOrigem.cdmaterial", whereIn)
				.whereIn("material.tipoitemsped", CollectionsUtil.listAndConcatenate(listaTipoitemsped, "id", ","))
				.orderBy("entregamaterial.cdentregamaterial")
				.list();			
	}
	public Movimentacaoestoque findWithLoteestoque(Movimentacaoestoque bean) {
		if(bean == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return  querySined()
				.setUseReadOnly(true)
				.select("movimentacaoestoque.cdmovimentacaoestoque, loteestoque.cdloteestoque, loteestoque.numero")
				.leftOuterJoin("movimentacaoestoque.loteestoque loteestoque")
				.where("movimentacaoestoque = ?", bean)
				.unique();
	}			
}