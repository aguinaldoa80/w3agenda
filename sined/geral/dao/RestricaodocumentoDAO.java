package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Restricao;
import br.com.linkcom.sined.geral.bean.Restricaodocumento;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RestricaodocumentoDAO extends GenericDAO<Restricaodocumento>{

	public List<Restricaodocumento> findByContratoUltimaRestricao(Contrato contrato) {
		if(contrato == null || contrato.getCdcontrato() == null) {
			throw new SinedException("Par�metro n�o pode ser nulo.");			
		}
		return query()
					.select("restricaodocumento.cdrestricaodocumento, documento.cddocumento, documentoacao.nome")
					.join("restricaodocumento.documento documento")
					.join("documento.documentoacao documentoacao")
					.join("restricaodocumento.restricao restricao")
					.where("restricao.contrato = ?", contrato)
					.where("restricao.cdrestricao = (select max(r.id) from Restricao r join r.contrato c where c.id = ?)", contrato.getCdcontrato())
					.list();
	}

	public void deletaDocumentos(Restricao restricao) {
		getJdbcTemplate().execute("DELETE FROM RESTRICAODOCUMENTO WHERE CDRESTRICAO = " + restricao.getCdrestricao());
	}
	
}
