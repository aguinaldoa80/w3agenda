package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresarepresentacao;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EmpresarepresentacaoDAO extends GenericDAO<Empresarepresentacao> {
	
	/**
	 * 
	 * @param empresa
	 * @param fornecedor
	 * @author Thiago Clemente
	 * 
	 */
	public boolean isEmpresaAndFornecedor(Empresa empresa, Fornecedor fornecedor){
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.join("empresarepresentacao.empresa empresa")
				.join("empresarepresentacao.fornecedor fornecedor")
				.where("empresa=?", empresa)
				.where("fornecedor=?", fornecedor)
				.unique()>0;
	}	
	
	/**
	 * Verifica se a empresa possui representação
	 * @param empresa
	 * @return
	 */
	public boolean isEmpresaRepresentacao(Empresa empresa){
		return newQueryBuilderWithFrom(Long.class)
	         	.select("count(*)")
	        	.join("empresarepresentacao.empresa empresa")
	        	.where("empresa=?", empresa)
	        	.unique()>0;
	}

	/**
	 * Busca a empresa representação pela empresa
	 * @param empresa
	 * @return
	 */
	public List<Empresarepresentacao> findByEmpresa(Empresa empresa) {
		return query()
		.select("empresarepresentacao.cdempresarepresentacao, empresarepresentacao.comissaovenda, fornecedor.cdpessoa")
		.join("empresarepresentacao.empresa empresa")
		.join("empresarepresentacao.fornecedor fornecedor")
		.where("empresarepresentacao.empresa = ?", empresa)
		.list();
	}
	
	/**
	* Método que retorna a fornecedor de representação
	*
	* @param empresa
	* @param fornecedor
	* @return
	* @since 13/02/2015
	* @author Luiz Fernando
	*/
	public List<Empresarepresentacao> findByEmpresaFornecedor(Empresa empresa, Fornecedor fornecedor) {
		if(empresa == null || fornecedor == null)
			throw new SinedException("Parâmetro inválido.");
		
		return query()
			.select("empresarepresentacao.cdempresarepresentacao, empresarepresentacao.comissaovenda, empresarepresentacao.repasseunico," +
					"contagerencial.cdcontagerencial, centrocusto.cdcentrocusto ")
			.leftOuterJoin("empresarepresentacao.contagerencial contagerencial")
			.leftOuterJoin("empresarepresentacao.centrocusto centrocusto")
			.where("empresarepresentacao.empresa = ?", empresa)
			.where("empresarepresentacao.fornecedor = ?", fornecedor)
			.list();
	}
}