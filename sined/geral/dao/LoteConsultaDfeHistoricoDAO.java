package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfeHistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class LoteConsultaDfeHistoricoDAO extends GenericDAO<LoteConsultaDfeHistorico> {

	public List<LoteConsultaDfeHistorico> procurarPorLoteConsultaDfe(LoteConsultaDfe loteConsultaDfe) {
		return query()
				.select("loteConsultaDfeHistorico.cdLoteConsultaDfeHistorico, loteConsultaDfeHistorico.situacaoEnum, " +
						"loteConsultaDfeHistorico.observacao, loteConsultaDfeHistorico.cdusuarioaltera, loteConsultaDfeHistorico.dtaltera ")
				.where("loteConsultaDfeHistorico.loteConsultaDfe = ?", loteConsultaDfe)
				.orderBy("loteConsultaDfeHistorico.cdLoteConsultaDfeHistorico desc")
				.list();
	}
}