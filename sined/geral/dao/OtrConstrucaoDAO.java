package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.OtrConstrucao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OtrConstrucaoDAO extends GenericDAO<OtrConstrucao>{

	public List<OtrConstrucao> findConstrucao(){
		return query()
				.select("otrConstrucao.cdOtrConstrucao, otrConstrucao.construcao")
				.list();
	}
}
