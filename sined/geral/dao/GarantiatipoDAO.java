package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Garantiatipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.GarantiatipoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("garantiatipo.descricao")
public class GarantiatipoDAO extends GenericDAO<Garantiatipo>{
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Garantiatipo> query) {
		query
		.select("garantiatipo.cdgarantiatipo, garantiatipo.descricao, garantiatipo.ativo, garantiatipo.geragarantia, "+
				"listaGarantiatipopercentual.percentual, listaGarantiatipopercentual.cdgarantiatipopercentual")
		.leftOuterJoin("garantiatipo.listaGarantiatipopercentual listaGarantiatipopercentual");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaGarantiatipopercentual");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Garantiatipo> query,
			FiltroListagem filtro) {
		GarantiatipoFiltro _filtro = (GarantiatipoFiltro) filtro;
		query
			.select("garantiatipo.cdgarantiatipo, garantiatipo.descricao, garantiatipo.ativo, garantiatipo.geragarantia")
			.whereLikeIgnoreAll("garantiatipo.descricao", _filtro.getDescricao())
			.where("garantiatipo.ativo = ?", _filtro.getAtivo())
			.where("garantiatipo.geragarantia = ?", _filtro.getGeragarantia())
			.orderBy("upper(retira_acento(garantiatipo.descricao))");
	}
	
	public List<Garantiatipo> findByDescricao(String descricao, Integer cdgarantiatipoIgnore){
		return query()
				.where("upper(garantiatipo.descricao) = ?", descricao.toUpperCase())
				.where("garantiatipo.cdgarantiatipo <> ?", cdgarantiatipoIgnore)
				.list();
	}
	
	public List<Garantiatipo> findForW3Producao(String whereIn){
		return query()
				.select("garantiatipo.cdgarantiatipo, garantiatipo.descricao, garantiatipo.geragarantia, garantiatipo.ativo")
				.whereIn("garantiatipo.cdgarantiatipo", whereIn)
				.list();
	}
}
