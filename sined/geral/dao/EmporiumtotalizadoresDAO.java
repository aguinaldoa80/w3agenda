package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Emporiumtotalizadores;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.EmporiumtotalizadoresFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EmporiumtotalizadoresDAO extends GenericDAO<Emporiumtotalizadores>{

	@Override
	public void updateListagemQuery(QueryBuilder<Emporiumtotalizadores> query, FiltroListagem _filtro) {
		EmporiumtotalizadoresFiltro filtro = (EmporiumtotalizadoresFiltro) _filtro;
		
		query
			.select("emporiumtotalizadores.cdemporiumtotalizadores, emporiumtotalizadores.data, emporiumtotalizadores.legenda, " +
					"emporiumtotalizadores.valorbcicms, emporiumtotalizadores.aliqicms, emporiumtotalizadores.valoricms, " +
					"emporiumpdv.descricao, tributacaoecf.codigoecf")
			.leftOuterJoin("emporiumtotalizadores.tributacaoecf tributacaoecf")
			.leftOuterJoin("emporiumtotalizadores.emporiumpdv emporiumpdv")
			.leftOuterJoin("emporiumpdv.empresa empresa")
			.where("emporiumtotalizadores.data >= ?", filtro.getData1())
			.where("emporiumtotalizadores.data <= ?", filtro.getData2())
			.where("emporiumpdv = ?", filtro.getEmporiumpdv())
			.where("empresa = ?", filtro.getEmpresa())
			.where("tributacaoecf = ?", filtro.getTributacaoecf())
			.orderBy("emporiumtotalizadores.data desc");
	}

	public List<Emporiumtotalizadores> findForSped(Emporiumpdv emporiumpdv, Date dtinicio, Date dtfim) {
		return query()
					.select("emporiumtotalizadores.cdemporiumtotalizadores, emporiumtotalizadores.data, emporiumtotalizadores.aliqicms, " +
							"emporiumtotalizadores.valorbcicms, emporiumtotalizadores.valoricms, tributacaoecf.cdtributacaoecf, emporiumtotalizadores.legenda, " +
							"tributacaoecf.codigototalizador, tributacaoecf.numerototalizador, tributacaoecf.codigototalizadorcancelada, " +
							"tributacaoecf.csticms, cfop.cdcfop, cfop.codigo, tributacaoecf.codigoecf")
					.leftOuterJoin("emporiumtotalizadores.tributacaoecf tributacaoecf")
					.leftOuterJoin("tributacaoecf.cfop cfop")
					.where("emporiumtotalizadores.emporiumpdv = ?", emporiumpdv)
					.where("emporiumtotalizadores.data >= ?", dtinicio)
					.where("emporiumtotalizadores.data <= ?", dtfim)
					.list();
	}

	public void deleteTotalizadores(Date data, Emporiumpdv emporiumpdv) {
		getHibernateTemplate().bulkUpdate("delete from Emporiumtotalizadores e where e.data = ? and e.emporiumpdv = ?", new Object[]{
			data, emporiumpdv
		});
	}

	public List<Emporiumtotalizadores> findForAcerto(Date dtinicio, Date dtfim) {
		return query()
					.select("emporiumtotalizadores.cdemporiumtotalizadores, emporiumtotalizadores.data, emporiumtotalizadores.aliqicms, " +
							"emporiumtotalizadores.valorbcicms, emporiumtotalizadores.valoricms, tributacaoecf.cdtributacaoecf, " +
							"tributacaoecf.codigototalizador, tributacaoecf.codigoecf, emporiumtotalizadores.legenda, " +
							"emporiumpdv.cdemporiumpdv, emporiumpdv.descricao, emporiumpdv.codigoemporium")
					.leftOuterJoin("emporiumtotalizadores.tributacaoecf tributacaoecf")
					.leftOuterJoin("emporiumtotalizadores.emporiumpdv emporiumpdv")
					.where("emporiumtotalizadores.data >= ?", dtinicio)
					.where("emporiumtotalizadores.data <= ?", dtfim)
					.orderBy("emporiumpdv.codigoemporium")
					.list();
	}

}
