package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailitem;
import br.com.linkcom.sined.geral.bean.Envioemailitemhistorico;
import br.com.linkcom.sined.geral.bean.enumeration.BounceTypeEnum;
import br.com.linkcom.sined.geral.bean.enumeration.EmailStatusEnum;
import br.com.linkcom.sined.geral.bean.enumeration.SendgridEvent;
import br.com.linkcom.sined.geral.service.EnvioemailService;
import br.com.linkcom.sined.geral.service.EnvioemailitemService;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.EmailItemWebHook;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EnvioemailitemhistoricoDAO extends GenericDAO<Envioemailitemhistorico>{

	public void atualizarStatus(EmailItemWebHook emailItemWebHook){
		if(emailItemWebHook.getCdemailitem() != null){
			Envioemailitemhistorico historico = new Envioemailitemhistorico();
			Envioemailitem envioemailitem = new Envioemailitem();
			
			envioemailitem.setCdenvioemailitem(emailItemWebHook.getCdemailitem());
			envioemailitem = EnvioemailitemService.getInstance().load(envioemailitem);
			
			if(envioemailitem != null){
			
				historico.setEnvioemailitem(envioemailitem);
				historico.setEvent(SendgridEvent.valueOf(emailItemWebHook.getEvent().toUpperCase()));
				historico.setSituacaoemail(EmailStatusEnum.fromSendgridEvent(historico.getEvent()));
				if(emailItemWebHook.getType() != null && !emailItemWebHook.getType().isEmpty()){
					historico.setType(BounceTypeEnum.valueOf(emailItemWebHook.getType().toUpperCase()));
				}
				historico.setAttempt(emailItemWebHook.getAttempt());
				historico.setResponse(emailItemWebHook.getResponse());
				historico.setUrlclicked(emailItemWebHook.getUrlclicked());
				historico.setStatus(emailItemWebHook.getStatus());
				historico.setStatus(emailItemWebHook.getReason());
				historico.setIp(emailItemWebHook.getIp());
				historico.setUseragent(emailItemWebHook.getUseragent());
				historico.setUltimaverificacao(new Timestamp(emailItemWebHook.getUltimaverificacao()));
				
				saveOrUpdate(historico);
				
				EmailStatusEnum statusEmail = historico.getSituacaoemail();
				if(statusEmail != null && envioemailitem.getSituacaoemail() != null && statusEmail.getValor() > envioemailitem.getSituacaoemail().getValor()){
					envioemailitem.setSituacaoemail(statusEmail);
				} else if(envioemailitem.getSituacaoemail() == null){
					envioemailitem.setSituacaoemail(statusEmail);
				}
				
				if(envioemailitem.getUltimaverificacao() == null || emailItemWebHook.getUltimaverificacao() > envioemailitem.getUltimaverificacao().getTime()){
					envioemailitem.setUltimaverificacao(new Timestamp(emailItemWebHook.getUltimaverificacao()));
				}
				
				EnvioemailitemService.getInstance().saveOrUpdate(envioemailitem);
				Envioemail envioemail = EnvioemailService.getInstance().load(envioemailitem.getEnvioemail());
				EmailStatusEnum statusEmailHeader = null;
				if(statusEmail != null && envioemail.getSituacaoemail() != null && statusEmail.getValor() > envioemail.getSituacaoemail().getValor()){
					statusEmailHeader = statusEmail;
				} else if(envioemail.getSituacaoemail() == null){
					statusEmailHeader = statusEmail;
				}
				if(statusEmailHeader != null){
					EnvioemailService.getInstance().updateStatusemail(envioemailitem.getEnvioemail(), statusEmailHeader);
				}
				if(envioemail.getUltimaverificacao() == null || emailItemWebHook.getUltimaverificacao() > envioemail.getUltimaverificacao().getTime()){
					EnvioemailService.getInstance().updateUltimaverificacao(envioemailitem.getEnvioemail(), new Timestamp(emailItemWebHook.getUltimaverificacao()));
				}
			}
		}
	}
	
	public List<Envioemailitemhistorico> findByEnvioemailitem(Envioemailitem envioemailitem){
		return query()
				.select("envioemailitemhistorico.situacaoemail, envioemailitemhistorico.attempt, envioemailitemhistorico.event, envioemailitemhistorico.response, envioemailitemhistorico.urlclicked, "+
						"envioemailitemhistorico.status, envioemailitemhistorico.reason, envioemailitemhistorico.type, envioemailitemhistorico.ip, "+
						"envioemailitemhistorico.useragent, envioemailitemhistorico.ultimaverificacao")
				.join("envioemailitemhistorico.envioemailitem envioemailitem")
				.where("envioemailitem = ?", envioemailitem)
				.orderBy("envioemailitemhistorico.ultimaverificacao")
				.list();
	}
}
