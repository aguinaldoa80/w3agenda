package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoconfirmacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentoconfirmacaoDAO extends GenericDAO<Documentoconfirmacao>{

	public List<Documentoconfirmacao> findByDocumentos(List<Documento> listaDocumento){
		return querySined()
				
				.select("documentoconfirmacao.cddocumentoconfirmacao, documentoconfirmacao.data, " +
						"documento.cddocumento")
				.join("documentoconfirmacao.documento documento")
				.whereIn("documento", CollectionsUtil.listAndConcatenate(listaDocumento, "cddocumento", ","))
				.list();
	}
	
}
