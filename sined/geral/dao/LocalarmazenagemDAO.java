package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.LocalarmazenagemFiltro;
import br.com.linkcom.sined.util.LocalarmazenagemBeanCacheUtil;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("localarmazenagem.nome")
public class LocalarmazenagemDAO extends GenericDAO<Localarmazenagem> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Localarmazenagem> query,	FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		LocalarmazenagemFiltro filtro = (LocalarmazenagemFiltro) _filtro;
		query
			.select("localarmazenagem.cdlocalarmazenagem, localarmazenagem.ativo, localarmazenagem.nome, endereco.logradouro, endereco.complemento, endereco.numero, " +
					"municipio.nome, uf.nome")
			.leftOuterJoin("localarmazenagem.endereco endereco")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.whereLikeIgnoreAll("localarmazenagem.nome", filtro.getNome())
			.where("uf =?", filtro.getUf())
			.where("municipio =?", filtro.getMunicipio())
			.where("localarmazenagem.ativo = ?", filtro.getAtivo())
			.ignoreJoin("listaempresa", "empresa");
		
		String whereInEmpresa = new SinedUtil().getListaEmpresa();
		if(StringUtils.isNotEmpty(whereInEmpresa)){
			query
				.leftOuterJoin("localarmazenagem.listaempresa listaempresa")
				.leftOuterJoin("listaempresa.empresa empresa")
				.openParentheses()
					.where("listaempresa is null")
					.or()
					.whereIn("empresa.cdpessoa", whereInEmpresa)
				.closeParentheses();
		}
	}
	
	@Override
	public void saveOrUpdate(Localarmazenagem bean) {
		super.saveOrUpdate(bean);
		if(bean.getCdlocalarmazenagem() != null){
			LocalarmazenagemBeanCacheUtil.remove(bean.getCdlocalarmazenagem().intValue());
		}
	}
	
	@Override
	public void saveOrUpdateNoUseTransaction(Localarmazenagem bean) {
		super.saveOrUpdateNoUseTransaction(bean);
		if(bean.getCdlocalarmazenagem() != null){
			LocalarmazenagemBeanCacheUtil.remove(bean.getCdlocalarmazenagem().intValue());
		}
	}
	
	@Override
	public void saveOrUpdateNoUseTransactionWithoutLog(Localarmazenagem bean) {
		super.saveOrUpdateNoUseTransactionWithoutLog(bean);
		if(bean.getCdlocalarmazenagem() != null){
			LocalarmazenagemBeanCacheUtil.remove(bean.getCdlocalarmazenagem().intValue());
		}
	}
	
	@Override
	public void saveOrUpdateWithoutLog(Localarmazenagem bean) {
		super.saveOrUpdateWithoutLog(bean);
		if(bean.getCdlocalarmazenagem() != null){
			LocalarmazenagemBeanCacheUtil.remove(bean.getCdlocalarmazenagem().intValue());
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Localarmazenagem> query) {
		query
			.leftOuterJoinFetch("localarmazenagem.endereco endereco")
			.leftOuterJoinFetch("endereco.municipio municipio")
			.leftOuterJoinFetch("municipio.uf uf")
			.leftOuterJoinFetch("localarmazenagem.listaempresa listaempresa")
			.leftOuterJoinFetch("listaempresa.empresa");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaempresa");
	}

	/**
	* M�todo que busca o local de armazenagem de acordo com a String passada por par�metro 
	*
	* @param q
	* @param ativo
	* @param whereInEmpresa
	* @return
	* @since 28/08/2015
	* @author Luiz Fernando
	*/
	public List<Localarmazenagem> findLocalarmazenagemAutocomplete(String q, Boolean ativo, String whereInEmpresa) {
		QueryBuilder<Localarmazenagem> query = querySined();
		query
			.select("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome ")
			.whereLikeIgnoreAll("localarmazenagem.nome", q)
			.where("localarmazenagem.ativo = ?", ativo);
		
		if(StringUtils.isNotEmpty(whereInEmpresa)){
			query
				.leftOuterJoin("localarmazenagem.listaempresa listaempresa")
				.leftOuterJoin("listaempresa.empresa empresa")
				.openParentheses()
					.where("listaempresa is null")
					.or()
					.whereIn("empresa.cdpessoa", whereInEmpresa)
				.closeParentheses();
		}
		
		return query
			.orderBy("localarmazenagem.nome")
			.autocomplete()
			.list();
	}
	
	/**
	* M�todo que verifica se o local permite estoque negativo
	*
	* @param localarmazenagem
	* @return
	* @since 22/06/2015
	* @author Luiz Fernando
	*/
	public boolean getPermitirestoquenegativo(Localarmazenagem localarmazenagem) {
		if (localarmazenagem == null || localarmazenagem.getCdlocalarmazenagem() == null) {
			return true;
		}
		
		Boolean valor = LocalarmazenagemBeanCacheUtil.get(localarmazenagem.getCdlocalarmazenagem().intValue());
		try {
			SinedUtil.markAsReader();
			if(valor==null){
				valor = newQueryBuilderWithFrom(Boolean.class)
						.select("localarmazenagem.permitirestoquenegativo")
						.where("localarmazenagem = ?", localarmazenagem)
						.setUseTranslator(false)
						.unique();
				LocalarmazenagemBeanCacheUtil.put(localarmazenagem.getCdlocalarmazenagem().intValue(), valor != null ? valor : false);
			}
		}finally {
			SinedUtil.desmarkAsReader();
		}
		return valor != null ? valor : false;
	}

	/**
	 * Busca a lista de locais para o combo no flex.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Localarmazenagem> findAtivosForFlex() {
		return querySined()
					.select("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome")
					.where("localarmazenagem.ativo = ?", Boolean.TRUE)
					.orderBy("localarmazenagem.nome")
					.list();
	}

	/**
	 * Carrega as informa��es dos locais de entrega com o endere�o preenchido.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Localarmazenagem> loadWithEndereco(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Locais de armazenagem n�o podem ser nulos.");
		}
		return querySined()
					.select("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, endereco.logradouro, endereco.numero, " +
							"endereco.complemento, endereco.bairro, endereco.cep, municipio.nome, uf.sigla")
					.leftOuterJoin("localarmazenagem.endereco endereco")
					.leftOuterJoin("endereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.whereIn("localarmazenagem.cdlocalarmazenagem", whereIn)
					.list();
	}
	
	/**
	 * Carrega uma lista de locais de armazenagem de uma determinada empresa.
	 * Utilizado na movimenta��o de estoque da venda
	 *
	 * @param empresa
	 * @return
	 * @author Marden Silva
	 */	
	public List<Localarmazenagem> loadForVenda(Empresa empresa) {
		if(empresa == null){
			throw new SinedException("O par�metro empresa n�o pode ser nulo.");
		}
		return querySined()
				.select("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome")
				.join("localarmazenagem.listaempresa localarmazenagemempresa")
				.where("localarmazenagemempresa.empresa = ?", empresa)
				.where("localarmazenagem.ativo = true")
				.list();				
	}
	
	/**
	 * Carrega a lista de localarmazenagem de acordo com a empresa
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Localarmazenagem> loadForEntrega(Empresa empresa) {
		return querySined()
				.select("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome")
				.leftOuterJoin("localarmazenagem.listaempresa localarmazenagemempresa")
				.where("localarmazenagemempresa.empresa = ?", empresa)
				.orderBy("localarmazenagem.nome")
				.list();				
	}

	/**
	* M�todo para carregar o municipio e uf do localarmazenagem
	*
	* @param localarmazenagem
	* @return
	* @since Sep 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public Localarmazenagem carregaUfMunicipio(Localarmazenagem localarmazenagem) {
		if(localarmazenagem == null || localarmazenagem.getCdlocalarmazenagem() == null){
			throw new SinedException("Par�metro inv�lido.");			
		}
		return querySined()
				.select("localarmazenagem.cdlocalarmazenagem, endereco.cdendereco, municipio.cdmunicipio, municipio.nome, uf.cduf, uf.nome, " +
						"endereco.numero, endereco.logradouro, endereco.bairro, endereco.cep ")
				.leftOuterJoin("localarmazenagem.endereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.where("localarmazenagem = ?", localarmazenagem)
				.unique();
	}

	/**
     * Busca os dados para o cache da tela de pedido de venda offline
     */
	public List<Localarmazenagem> findForPVOffline() {
		return query()
		.select("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, listaempresa.cdlocalarmazenagemempresa, empresa.cdpessoa")
		.leftOuterJoin("localarmazenagem.listaempresa listaempresa")
		.leftOuterJoin("listaempresa.empresa empresa")
		.list();
	}

	/**
	 * M�todo que retorna os Locais ativos de acordo com a empresa
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Localarmazenagem> findAtivosByEmpresa(Empresa empresa) {
		QueryBuilder<Localarmazenagem> query = querySined()
			.select("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome ")
			.leftOuterJoin("localarmazenagem.listaempresa listaempresa")
			.leftOuterJoin("listaempresa.empresa empresa");
		
		if(empresa != null && empresa.getCdpessoa() != null){
			query
				.openParentheses()
				.where("empresa = ?", empresa).or()
				.where("listaempresa is null")
				.closeParentheses();
		}
		
		query
			.where("localarmazenagem.ativo = true")
			.orderBy("localarmazenagem.nome");

		return query.list();
	}
	
	public List<Localarmazenagem> findByNome(String nome) {
		return querySined()
					.select("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome")
					.where("localarmazenagem.nome = ?", nome)
					.list();
	}

	/**
	 * Carrega o local de cliente de loca��o vinculado a empresa
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/10/2013
	 */
	public Localarmazenagem loadByEmpresaAndClienteLocacao(Empresa empresa) {
		return querySined()
					.select("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome")
					.leftOuterJoin("localarmazenagem.listaempresa listaempresa")
					.leftOuterJoin("listaempresa.empresa empresa")
					.where("empresa = ?", empresa)
					.where("localarmazenagem.ativo = true")
					.where("localarmazenagem.clientelocacao = true")
					.orderBy("localarmazenagem.nome")
					.setMaxResults(1)
					.unique();
	}
	
	/**
	 *
	 * @param empresa
	 * @param clienteLocacao
	 * @param principal
	 * @param indenizacao
	 * @author Thiago Clemente
	 * 
	 */
	public Localarmazenagem loadForRomaneioFromContrato(Empresa empresa, Boolean clienteLocacao, Boolean principal, Boolean indenizacao) {
		return querySined()
				.select("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome")
				.join("localarmazenagem.listaempresa listaempresa")
				.join("listaempresa.empresa empresa")
				.where("coalesce(localarmazenagem.clientelocacao,false)=?", clienteLocacao)
				.where("coalesce(localarmazenagem.principal,false)=?", principal)
				.where("coalesce(localarmazenagem.indenizacao,false)=?", indenizacao)
				.where("localarmazenagem.ativo=?", Boolean.TRUE)
				.where("empresa=?", empresa)
				.setMaxResults(1)
				.unique();
	}
	
	/**
	 * 
	 * @param cdlocalarmazenagemExcecao
	 * @param empresa
	 * @param principal
	 * @param indenizacao
	 * @author Thiago Clemente
	 * 
	 */
	public boolean verificarPrincipalIndenizacao(Integer cdlocalarmazenagemExcecao, Empresa empresa, Boolean principal, Boolean indenizacao){
		SinedUtil.markAsReader();
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.join("localarmazenagem.listaempresa listaempresa")
				.join("listaempresa.empresa empresa")
				.where("localarmazenagem.cdlocalarmazenagem<>?", cdlocalarmazenagemExcecao)
				.where("empresa=?", empresa)
				.where("localarmazenagem.principal=?", principal)
				.where("localarmazenagem.indenizacao=?", indenizacao)
				.unique().longValue()==0;
	}
	
	/**
	* M�todo que carrega o Local de armazenagem com as informa��es de estoque (percentual e mensagem) 
	*
	* @param localarmazenagem
	* @return
	* @since 04/08/2014
	* @author Luiz Fernando
	*/
	public Localarmazenagem loadWithInformacoesEstoque(Localarmazenagem localarmazenagem){
		if(localarmazenagem == null || localarmazenagem.getCdlocalarmazenagem() == null)
			throw new SinedException("Local n�o pode ser nulo");
		
		return querySined()
			.select("localarmazenagem.cdlocalarmazenagem, localarmazenagem.qtdeacimaestoque, localarmazenagem.msgacimaestoque, localarmazenagem.permitirestoquenegativo")
			.where("localarmazenagem = ?", localarmazenagem)
			.unique();
	}
	
	/**
	 * Busca os locais das empresas passadas por par�metro
	 *
	 * @param whereInEmpresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/02/2015
	 */
	public List<Localarmazenagem> findByEmpresa(Localarmazenagem localarmazenagem, String whereInEmpresa) {
		QueryBuilder<Localarmazenagem> query = querySined();
		
		query
			.select("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome")
			.leftOuterJoin("localarmazenagem.listaempresa listaempresa")
			.leftOuterJoin("listaempresa.empresa empresa");
		
		if(localarmazenagem != null){
			query
				.openParentheses()
					.where("localarmazenagem.ativo = ?", Boolean.TRUE)
					.or()
					.where("localarmazenagem = ?", localarmazenagem)
				.closeParentheses();
		} else {
			query
				.where("localarmazenagem.ativo = ?", Boolean.TRUE);
		}
		
		if(whereInEmpresa != null){
			query
			.openParentheses()
				.whereIn("empresa.cdpessoa", whereInEmpresa)
				.or()
				.where("listaempresa is null")
			.closeParentheses();
		}
		
		return query.list();
	}
	
	/**
	* M�todo que busca os locais de armazenagem
	*
	* @param whereIn
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<Localarmazenagem> findForAndroid(String whereIn) {
		return query()
		.select("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, localarmazenagem.principal, localarmazenagem.ativo")
		.whereIn("localarmazenagem.cdlocalarmazenagem", whereIn)
		.list();
	}

	/**
	* M�todo que verifica se o local de armazenagem est� vinculado a um pedido de venda ou venda nas situa��es de aguardando aprova��o ou prevista
	*
	* @param localarmazenagem
	* @return
	* @since 18/11/2016
	* @author Luiz Fernando
	*/
	public boolean existePedidoOuVendaPrevista(Localarmazenagem localarmazenagem) {
		if(localarmazenagem == null || localarmazenagem.getCdlocalarmazenagem() == null)
			throw new SinedException("Local n�o pode ser nulo.");
		SinedUtil.markAsReader();
		return newQueryBuilder(Long.class)
			.from(Localarmazenagem.class)
			.select("count(*)")
			.where("localarmazenagem = ?", localarmazenagem)
			.openParentheses()
				.where("exists (" +
						" select v.cdvenda " +
						" from Venda v " +
						" join v.localarmazenagem la " +
						" join v.vendasituacao vs " +
						" where vs.cdvendasituacao = " + Vendasituacao.AGUARDANDO_APROVACAO.getCdvendasituacao() + 
						" and la.cdlocalarmazenagem = localarmazenagem.cdlocalarmazenagem )")
				.or()
				.where("exists (" +
						" select pv.cdpedidovenda " +
						" from Pedidovenda pv " +
						" join pv.localarmazenagem la " +
						" where pv.pedidovendasituacao in (" + Pedidovendasituacao.AGUARDANDO_APROVACAO.ordinal() + "," + Pedidovendasituacao.PREVISTA.ordinal() + ") " + 
						" and la.cdlocalarmazenagem = localarmazenagem.cdlocalarmazenagem )")
			.closeParentheses()
			.unique() > 0;
	}
	
	/**
	* M�todo que verifica se a empresa pertence ao local.
	*
	* @param localarmazenagem
	* @param empresa
	* @return
	* @since 10/04/2017
	* @author Luiz Fernando
	*/
	public boolean isLocalEmpresa(Localarmazenagem localarmazenagem, Empresa empresa) {
		if(localarmazenagem == null || localarmazenagem.getCdlocalarmazenagem() == null || empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Local/Empresa n�o pode ser nulo.");
		SinedUtil.markAsReader();
		return newQueryBuilder(Long.class)
			.from(Localarmazenagem.class)
			.select("count(*)")
			.leftOuterJoin("localarmazenagem.listaempresa listaempresa")
			.leftOuterJoin("listaempresa.empresa empresa")
			.where("localarmazenagem = ?", localarmazenagem)
			.openParentheses()
				.where("listaempresa is null")
				.or()
				.where("empresa = ?", empresa)
			.closeParentheses()
			.unique() > 0;
	}
	
	public Localarmazenagem loadPrincipalByEmpresa(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null){
			return null;
		}
		Localarmazenagem localPrincipal = querySined()
											.select("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, localarmazenagem.principal, localarmazenagem.ativo")
											.join("localarmazenagem.listaempresa listaempresa")
											.where("listaempresa.empresa = ?", empresa)
											.where("ativo = ?", Boolean.TRUE)
											.where("principal = ?", Boolean.TRUE)
											.unique();

		return localPrincipal;
	}
	
	public Localarmazenagem loadPrincipalSemEmpresa() {
		return querySined()
					.select("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, localarmazenagem.principal, localarmazenagem.ativo")
					.where("principal = ?", Boolean.TRUE)
					.where("ativo = ?", Boolean.TRUE)
					.where("not exists(select 1 from Localarmazenagemempresa localarmazenagemempresa where localarmazenagemempresa.localarmazenagem = localarmazenagem)")
					.unique();
	}

	@Override
	public ListagemResult<Localarmazenagem> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Localarmazenagem> query = querySined();
		updateListagemQuery(query, filtro);
		query.orderBy("localarmazenagem.cdlocalarmazenagem");
		
		return new ListagemResult<Localarmazenagem>(query, filtro);
	}

	/**
	 * Carrega o local com o centro de custo.
	 *
	 * @param localarmazenagem
	 * @return
	 * @since 14/10/2019
	 * @author Rodrigo Freitas
	 */
	public Localarmazenagem loadWithCentrocusto(Localarmazenagem localarmazenagem) {
		if(localarmazenagem == null || localarmazenagem.getCdlocalarmazenagem() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return querySined()
				.select("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, centrocusto.cdcentrocusto")
				.leftOuterJoin("localarmazenagem.centrocusto centrocusto")
				.where("localarmazenagem = ?", localarmazenagem)
				.unique();
	}
}