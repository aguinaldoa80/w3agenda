package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Pedidovendasituacaoecommerce;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("pedidovendasituacaoecommerce.ordem, pedidovendasituacaoecommerce.nome")
public class PedidovendasituacaoecommerceDAO extends GenericDAO<Pedidovendasituacaoecommerce>{

	public List<Pedidovendasituacaoecommerce> loadForFiltro(String whereIn) {
		return query()
					.select("pedidovendasituacaoecommerce.cdpedidovendasituacaoecommerce, pedidovendasituacaoecommerce.idsecommerce,"+
							" pedidovendasituacaoecommerce.nome ")
					.whereIn("pedidovendasituacaoecommerce.cdpedidovendasituacaoecommerce", whereIn)
					.list();
	}
	
	public List<Pedidovendasituacaoecommerce> findByIdsecommerce(String idsecommerce) {
		return query()
					.select("pedidovendasituacaoecommerce.cdpedidovendasituacaoecommerce, pedidovendasituacaoecommerce.idsecommerce,"+
							" pedidovendasituacaoecommerce.nome, pedidovendasituacaoecommerce.letra, pedidovendasituacaoecommerce.corfundo")
					.whereLike("','||pedidovendasituacaoecommerce.idsecommerce||',' ", ","+idsecommerce+",")
					.list();
	}

}
