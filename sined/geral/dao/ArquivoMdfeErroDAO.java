package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.ArquivoMdfe;
import br.com.linkcom.sined.geral.bean.ArquivoMdfeErro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ArquivoMdfeErroDAO extends GenericDAO<ArquivoMdfeErro> {

	
	public List<ArquivoMdfeErro> findByArquivoMdfe(ArquivoMdfe arquivoMdfe) {
		if(arquivoMdfe == null || arquivoMdfe.getCdArquivoMdfe() == null){
			throw new SinedException("Id de arquivoMdfe n�o pode ser nulo.");
		}
		return query()
					.select("arquivoMdfeErro.cdArquivoMdfeErro, arquivoMdfeErro.mensagem, arquivoMdfeErro.correcao, " +
							"mdfe.cdmdfe, mdfe.numero, arquivoMdfeErro.data, arquivoMdfeErro.codigo ")
					.leftOuterJoin("arquivoMdfeErro.arquivoMdfe arquivoMdfe")
					.leftOuterJoin("arquivoMdfe.mdfe mdfe")
					.where("arquivoMdfe = ?", arquivoMdfe)
					.orderBy("arquivoMdfeErro.data desc, arquivoMdfeErro.mensagem")
					.list();
	}

}
