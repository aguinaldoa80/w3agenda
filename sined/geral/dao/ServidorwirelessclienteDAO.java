package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Servidorwirelesscliente;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.ServidorwirelessclienteFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ServidorwirelessclienteDAO extends GenericDAO<Servidorwirelesscliente>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Servidorwirelesscliente> query, FiltroListagem _filtro) {
		ServidorwirelessclienteFiltro filtro = (ServidorwirelessclienteFiltro)_filtro;
		query.leftOuterJoinFetch("servidorwirelesscliente.servidor servidor")
		 	 .where("servidorwirelesscliente.servidor = ?", filtro.getServidor())
		 	 .where("servidorwirelesscliente.mac = ?", filtro.getMac())
		 	 .where("servidorwirelesscliente.sinal = ?", filtro.getSinal());
	}
		
}
