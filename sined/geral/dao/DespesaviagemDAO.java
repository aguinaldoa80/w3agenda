package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaodespesaviagem;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.OrigemBean;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.enumeration.ReembolsoEnum;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.DespesaviagemFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DespesaviagemDAO extends GenericDAO<Despesaviagem> {
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaDespesaviagemitem");
		save.saveOrUpdateManaged("listaDespesaviagemprojeto");
		
		Despesaviagem despesaviagem = (Despesaviagem)save.getEntity();
		
//		Se a despesa de viagem estiver sendo criada setar a situa��o igual a 'EM ABERTO'.
		if (despesaviagem.getCddespesaviagem() == null)
			despesaviagem.setSituacaodespesaviagem(Situacaodespesaviagem.EM_ABERTO);
		
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Despesaviagem> query) {
		query
			.select("despesaviagem.cddespesaviagem, despesaviagem.origemviagem, despesaviagem.destinoviagem, despesaviagem.reembolsogerado, " +
					"despesaviagem.dtsaida, despesaviagem.dtretornoprevisto, despesaviagem.dtretornorealizado, despesaviagem.descricao, " +
					"despesaviagem.motivo, colaborador.cdpessoa, colaborador.nome, despesaviagem.observacao, despesaviagemtipo.cddespesaviagemtipo, " +
					"listaDespesaviagemitem.cddespesaviagemitem, listaDespesaviagemitem.valorprevisto, listaDespesaviagemitem.valorrealizado, " +
					"despesaviagem.situacaodespesaviagem, empresa.cdpessoa, listaDespesaviagemprojeto.cddespesaviagemprojeto, projeto.cdprojeto, " +
					"projeto.nome, listaDespesaviagemprojeto.percentual, listaDespesaviagemitem.documento, listaDespesaviagemitem.quantidade, " +
					"despesaviagem.despesaviagemdestino, despesaviagem.reembolsavel, " +
					"endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.bairro, endereco.cep, " +
					"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.nome, uf.sigla, " +
					"contacrm.cdcontacrm, contacrm.nome, cliente.cdpessoa, cliente.nome, fornecedor.cdpessoa, fornecedor.nome, " +
					"contacrmcontato.cdcontacrmcontato")
			.leftOuterJoin("despesaviagem.contacrm contacrm")
			.leftOuterJoin("despesaviagem.cliente cliente")
			.leftOuterJoin("despesaviagem.fornecedor fornecedor")
			.leftOuterJoin("despesaviagem.listaDespesaviagemitem listaDespesaviagemitem")
			.leftOuterJoin("despesaviagem.listaDespesaviagemprojeto listaDespesaviagemprojeto")
			.leftOuterJoin("despesaviagem.empresa empresa")
			.leftOuterJoin("despesaviagem.contacrmcontato contacrmcontato")
			.leftOuterJoin("despesaviagem.endereco endereco")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("listaDespesaviagemitem.despesaviagemtipo despesaviagemtipo")
			.leftOuterJoin("listaDespesaviagemprojeto.projeto projeto")
			.join("despesaviagem.colaborador colaborador")
			.orderBy("despesaviagemtipo.nome");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Despesaviagem> query,FiltroListagem _filtro) {
		DespesaviagemFiltro filtro = (DespesaviagemFiltro) _filtro;
		query
			.select("despesaviagem.cddespesaviagem, despesaviagem.origemviagem, despesaviagem.destinoviagem, " +
					"despesaviagem.dtsaida, despesaviagem.dtretornorealizado, colaborador.nome, despesaviagem.situacaodespesaviagem, " +
					"despesaviagem.despesaviagemdestino, despesaviagem.reembolsavel, despesaviagem.reembolsogerado")
			.join("despesaviagem.colaborador colaborador")
			.leftOuterJoin("despesaviagem.listaDespesaviagemprojeto listaDespesaviagemprojeto")
			.where("colaborador = ?", filtro.getColaborador())
			.where("despesaviagem.dtsaida >= ?", filtro.getDtsaida1())
			.where("despesaviagem.dtsaida <= ?", filtro.getDtsaida2())
			.where("despesaviagem.dtretornorealizado >= ?", filtro.getDtretornorealizado1())
			.where("despesaviagem.dtretornorealizado <= ?", filtro.getDtretornorealizado2())
			.where("listaDespesaviagemprojeto.projeto = ?", filtro.getProjeto())
			.where("despesaviagem.cddespesaviagem = ?", filtro.getCddespesaviagem())
			.where("despesaviagem.despesaviagemdestino = ?", filtro.getDespesaviagemdestino())
			.whereLikeIgnoreAll("despesaviagem.origemviagem", filtro.getOrigemviagem())
			.whereLikeIgnoreAll("despesaviagem.destinoviagem", filtro.getDestinoviagem())
			.orderBy("despesaviagem.cddespesaviagem desc");
		
		if(SinedUtil.getUsuarioLogado().getRestricaodespesaviagem() != null && SinedUtil.getUsuarioLogado().getRestricaodespesaviagem()){ 
			query.where("colaborador.cdpessoa = ?", SinedUtil.getUsuarioLogado().getCdpessoa());
		}
			
		if(filtro.getCliente()!=null){
			query.join("despesaviagem.cliente cliente")
				 .where("cliente =?", filtro.getCliente());
		}
		
		if(filtro.getFornecedor()!=null){
			query.join("despesaviagem.fornecedor fornecedor")
				 .where("fornecedor =?", filtro.getFornecedor());	
		}
		
		if(filtro.getContacrm()!=null){
			query.join("despesaviagem.contacrm contacrm")
				 .where("contacrm =?", filtro.getContacrm());	
		}
		
		if(filtro.getEndereco()!=null){
			query.join("despesaviagem.endereco endereco")
			 .where("endereco =?", filtro.getEndereco());	
		}
		
		if(filtro.getReembolso()!=null){
			if(filtro.getReembolso().equals(ReembolsoEnum.GERADOS)){
				query.where("despesaviagem.reembolsogerado = ? ", Boolean.TRUE);
			}
			if(filtro.getReembolso().equals(ReembolsoEnum.PENDENTES)){
				query.openParentheses()
					.where("despesaviagem.reembolsogerado = ? ", Boolean.FALSE)
					.or()
					.where("despesaviagem.reembolsogerado is null")
				.closeParentheses();
				query.where("despesaviagem.reembolsavel = ? ", Boolean.TRUE);
			}
			if(filtro.getReembolso().equals(ReembolsoEnum.SEM_REEMBOLSO)){
				query.openParentheses()
					.where("despesaviagem.reembolsavel = ? ", Boolean.FALSE)
					.or()
					.where("despesaviagem.reembolsavel is null")
				.closeParentheses();
			}
		}
		if(filtro.getListaSituacao() != null){
			query.whereIn("despesaviagem.situacaodespesaviagem", Situacaodespesaviagem.listAndConcatenate(filtro.getListaSituacao()));
		}	
	}
	
	/**
	 * Carrega a lista de despesaviagem com as listas necess�rias 
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 */
	public List<Despesaviagem> loadWithLista(String whereIn, String orderBy, boolean asc) {

		QueryBuilder<Despesaviagem> query = query()
					.select("despesaviagem.cddespesaviagem, despesaviagem.origemviagem, despesaviagem.destinoviagem, " +
							"despesaviagem.dtsaida, despesaviagem.dtretornorealizado, colaborador.nome, despesaviagem.situacaodespesaviagem, " +
							"despesaviagem.cddespesaviagem, listaDespesaviagemitem.cddespesaviagemitem, listaDespesaviagemitem.valorprevisto, " +
							"listaDespesaviagemitem.valorrealizado, despesaviagem.despesaviagemdestino, despesaviagem.reembolsavel, despesaviagem.reembolsogerado," +
							"cliente.cdpessoa, cliente.nome, fornecedor.cdpessoa, fornecedor.nome, contacrm.cdcontacrm, contacrm.nome, " +
							"despesaviagemtipo.cddespesaviagemtipo, despesaviagemtipo.nome, despesaviagem.projetos ")
					.join("despesaviagem.colaborador colaborador")
					.leftOuterJoin("despesaviagem.cliente cliente")
					.leftOuterJoin("despesaviagem.fornecedor fornecedor")
					.leftOuterJoin("despesaviagem.contacrm contacrm")
					.leftOuterJoin("despesaviagem.listaDespesaviagemitem listaDespesaviagemitem")
					.leftOuterJoin("listaDespesaviagemitem.despesaviagemtipo despesaviagemtipo")
					.whereIn("despesaviagem.cddespesaviagem", whereIn);
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("despesaviagem.cddespesaviagem DESC");
		}
		
		return query.list();
	}
	
	/**
	 * Retorna a lista de despesas de viagem para a verifica��o da situa��o delas.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Despesaviagem> findForVerificacao(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("despesaviagem.cddespesaviagem, despesaviagem.situacaodespesaviagem")
					.whereIn("despesaviagem.cddespesaviagem", whereIn)
					.list();
	}

	/**
	 * Faz update autorizando a(s) despesa(s) de viagem.
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void updateSituacao(String whereIn, Situacaodespesaviagem situacaodespesaviagem) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		getHibernateTemplate().bulkUpdate("update Despesaviagem d set d.situacaodespesaviagem = ? where d.id in ("+whereIn+")", 
				new Object[]{situacaodespesaviagem});	
	}

	/**
	 * Carrega a lista de despesas de viagem para que seja feita a realiza��o delas.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Despesaviagem> findForRealizacao(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("despesaviagem.cddespesaviagem, despesaviagem.situacaodespesaviagem, despesaviagem.dtsaida, " +
							"listaDespesaviagemitem.cddespesaviagemitem, despesaviagemtipo.cddespesaviagemtipo, despesaviagemtipo.nome," +
							"listaDespesaviagemitem.valorprevisto, colaborador.cdpessoa")
					.leftOuterJoin("despesaviagem.colaborador colaborador")
					.leftOuterJoin("despesaviagem.listaDespesaviagemitem listaDespesaviagemitem")
					.leftOuterJoin("listaDespesaviagemitem.despesaviagemtipo despesaviagemtipo")
					.whereIn("despesaviagem.cddespesaviagem", whereIn)
					.list();
	}

	/**
	 * Realiza a atualiza��o da data de retorno realizado.
	 *
	 * @param despesaviagem
	 * @author Rodrigo Freitas
	 */
	public void updateDtretornorealizado(Despesaviagem despesaviagem) {
		if (despesaviagem == null || despesaviagem.getCddespesaviagem() == null) {
			throw new SinedException("Despesa de viagem n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Despesaviagem d set d.dtretornorealizado = ? where d.id = ?", 
				new Object[]{despesaviagem.getDtretornorealizado(), despesaviagem.getCddespesaviagem()});
	}

	/**
	 * Carrega a despesa de viagem com o colaborador preenchido.
	 *
	 * @param despesaviagem
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Despesaviagem loadWithColaborador(Despesaviagem despesaviagem) {
		if (despesaviagem == null || despesaviagem.getCddespesaviagem() == null) {
			throw new SinedException("Despesa de viagem n�o pode ser nulo.");
		}
		return query()
					.select("despesaviagem.cddespesaviagem, colaborador.cdpessoa, colaborador.nome, empresa.cdpessoa, empresa.nome, empresa.razaosocial")
					.join("despesaviagem.colaborador colaborador")
					.leftOuterJoin("despesaviagem.empresa empresa")
					.where("despesaviagem = ?", despesaviagem)
					.unique();
	}

	/**
	 * Uma query para fazer o c�lculo para o acerto com o colaborador.
	 *
	 * @param despesaviagem
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public Double calculaDiferencaAcerto(Despesaviagem despesaviagem) {
		if (despesaviagem == null || despesaviagem.getCddespesaviagem() == null) {
			throw new SinedException("Despesa de viagem n�o pode ser nulo.");
		}
		SinedUtil.markAsReader();
		List<Double> listaUpdate = getJdbcTemplate().query(
							"SELECT SUM(REALIZADAS)-SUM(SOMA) AS DIFERENCA " +
							"FROM ( " +
							"SELECT COALESCE(SUM(M.VALOR / (SELECT CASE WHEN COUNT(*) > 0 THEN COUNT(*) ELSE 1 END FROM MOVIMENTACAOORIGEM MORIGEM2 WHERE MORIGEM2.CDMOVIMENTACAO = M.CDMOVIMENTACAO)),0) AS SOMA, 0 AS REALIZADAS " +
							"FROM MOVIMENTACAO M " +
							"JOIN MOVIMENTACAOORIGEM MO ON M.CDMOVIMENTACAO = MO.CDMOVIMENTACAO " +
							"JOIN MOVIMENTACAOACAO MOACAO ON M.CDMOVIMENTACAOACAO = MOACAO.CDMOVIMENTACAOACAO " +
							"WHERE MO.CDDESPESAVIAGEMADIANTAMENTO = " + despesaviagem.getCddespesaviagem() + " " +
							"AND MOACAO.CDMOVIMENTACAOACAO <> " + Movimentacaoacao.CANCELADA.getCdmovimentacaoacao() + " " +
							
							"UNION ALL " +
							
							"SELECT COALESCE(SUM(D.VALOR / (SELECT CASE WHEN COUNT(*) > 0 THEN COUNT(*) ELSE 1 END FROM DOCUMENTOORIGEM DORIGEM2 WHERE DORIGEM2.CDDOCUMENTO = D.CDDOCUMENTO)),0) AS SOMA, 0 AS REALIZADAS " +
							"FROM DOCUMENTO D " +
							"JOIN DOCUMENTOORIGEM DOC ON DOC.CDDOCUMENTO = D.CDDOCUMENTO " +
							"JOIN DOCUMENTOACAO DOCACAO ON DOCACAO.CDDOCUMENTOACAO = D.CDDOCUMENTOACAO " +
							"WHERE DOC.CDDESPESAVIAGEMADIANTAMENTO = " + despesaviagem.getCddespesaviagem() + " " +
							"AND DOCACAO.CDDOCUMENTOACAO <> " + Documentoacao.CANCELADA.getCddocumentoacao() + " " +
							
							"UNION ALL " +
							
							"SELECT 0 AS SOMA, COALESCE(SUM(DI.VALORREALIZADO),0) AS REALIZADAS " +
							"FROM DESPESAVIAGEM D " +
							"JOIN DESPESAVIAGEMITEM DI ON D.CDDESPESAVIAGEM = DI.CDDESPESAVIAGEM " +
							"WHERE D.CDDESPESAVIAGEM = " + despesaviagem.getCddespesaviagem() + " " +
							") QUERY", 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getDouble("DIFERENCA");
			}
		});
		
		return listaUpdate != null && listaUpdate.get(0) != null ? listaUpdate.get(0) : 0.00;
	}

	/**
	 * Retorna um boolean verificando se pode estornar as despesa de viagem ou n�o.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public Boolean podeEstornar(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		SinedUtil.markAsReader();
		List<Double> listaUpdate = getJdbcTemplate().query(
				"SELECT COUNT(*) AS CONTADOR  " +
				"FROM (   " +
				"SELECT 1   " +
				"FROM DESPESAVIAGEM D   " +
				"JOIN DOCUMENTOORIGEM DO1 ON DO1.CDDESPESAVIAGEMADIANTAMENTO = D.CDDESPESAVIAGEM   " +
				"JOIN DOCUMENTO DD1 ON DO1.CDDOCUMENTO = DD1.CDDOCUMENTO   " +
				"WHERE DD1.CDDOCUMENTOACAO <> " + Documentoacao.PREVISTA.getCddocumentoacao() + " " +
				"AND DD1.CDDOCUMENTOACAO <> " + Documentoacao.CANCELADA.getCddocumentoacao() + " " +
				"AND D.CDDESPESAVIAGEM IN ("+whereIn+")   " +
				        
				"UNION ALL   " +
				        
				"SELECT 1   " +
				"FROM DESPESAVIAGEM D   " +
				"JOIN DOCUMENTOORIGEM DO1 ON DO1.CDDESPESAVIAGEMACERTO = D.CDDESPESAVIAGEM   " +
				"JOIN DOCUMENTO DD1 ON DO1.CDDOCUMENTO = DD1.CDDOCUMENTO   " +
				"LEFT OUTER JOIN NOTADOCUMENTO ND1 ON DO1.CDDOCUMENTO = ND1.CDDOCUMENTO   " +
				"WHERE ((DD1.CDDOCUMENTOACAO <> " + Documentoacao.PREVISTA.getCddocumentoacao() + " " +
				"AND DD1.CDDOCUMENTOACAO <> " + Documentoacao.CANCELADA.getCddocumentoacao() + ") " +
				"OR ND1.CDDOCUMENTO IS NOT NULL) " + 
				"AND D.CDDESPESAVIAGEM IN ("+whereIn+")  " +

				"UNION ALL " +
				
				"SELECT 1 " +
				"FROM DESPESAVIAGEM D " +
				"JOIN MOVIMENTACAOORIGEM MO ON MO.CDDESPESAVIAGEMACERTO = D.CDDESPESAVIAGEM " +
				"JOIN MOVIMENTACAO M ON M.CDMOVIMENTACAO = MO.CDMOVIMENTACAO " +
				"WHERE M.CDMOVIMENTACAOACAO <> " + Movimentacaoacao.NORMAL.getCdmovimentacaoacao() + " " +
				" AND M.CDMOVIMENTACAOACAO <> " + Movimentacaoacao.CANCELADA.getCdmovimentacaoacao() + " " +
				"AND D.CDDESPESAVIAGEM IN ("+whereIn+") " +
				
				"UNION ALL " +
				
				"SELECT 1 " +
				"FROM DESPESAVIAGEM D " +
				"JOIN MOVIMENTACAOORIGEM MO ON MO.CDDESPESAVIAGEMADIANTAMENTO = D.CDDESPESAVIAGEM " +
				"JOIN MOVIMENTACAO M ON M.CDMOVIMENTACAO = MO.CDMOVIMENTACAO " +
				"WHERE M.CDMOVIMENTACAOACAO <> " + Movimentacaoacao.NORMAL.getCdmovimentacaoacao() + " " +
				" AND M.CDMOVIMENTACAOACAO <> " + Movimentacaoacao.CANCELADA.getCdmovimentacaoacao() + " " +
				"AND D.CDDESPESAVIAGEM IN ("+whereIn+") " +
				") QUERY", 
			new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getDouble("CONTADOR");
			}
		});
		
		return listaUpdate != null && listaUpdate.get(0) != null ? listaUpdate.get(0).equals(0.00) : false ;
	}

	/**
	 * Carrega a lista de adiantamentos de uma despesa de viagem
	 *
	 * @param form
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<OrigemBean> buildAdiantamentos(Despesaviagem form, String whereIn) {
		if ((form == null || form.getCddespesaviagem() == null) && (whereIn == null || whereIn.isEmpty())) {
			throw new SinedException("A despesa de viagem n�o pode ser nulo.");
		}
		String select1;
		String select2;
		String where1;
		String where2;
		if(whereIn != null && !whereIn.isEmpty()){
			select1 = "SELECT "+OrigemBean.CONTAPAGAR+" AS TYPEORIGEM, 'CONTA A PAGAR' AS TIPO, DOC.CDDESPESAVIAGEMADIANTAMENTO AS ID, D.CDDOCUMENTO AS DESCRICAO, D.VALOR AS VALOR ";
			select2 = "SELECT "+OrigemBean.TRANSFERENCIA+" AS TYPEORIGEM, 'MOVIMENTACAO' AS TIPO, MO.CDDESPESAVIAGEMADIANTAMENTO AS ID, M.CDMOVIMENTACAO AS DESCRICAO, M.VALOR AS VALOR ";
			where1 = "WHERE DOC.CDDESPESAVIAGEMADIANTAMENTO IN (" + whereIn + ") ";
			where2 = "WHERE MO.CDDESPESAVIAGEMADIANTAMENTO IN (" + whereIn + ") ";
		}else{
			select1 = "SELECT "+OrigemBean.CONTAPAGAR+" AS TYPEORIGEM, 'CONTA A PAGAR' AS TIPO, D.CDDOCUMENTO AS ID, D.CDDOCUMENTO AS DESCRICAO, D.VALOR AS VALOR ";
			select2 = "SELECT "+OrigemBean.TRANSFERENCIA+" AS TYPEORIGEM, 'MOVIMENTACAO' AS TIPO, M.CDMOVIMENTACAO AS ID, M.CDMOVIMENTACAO AS DESCRICAO, M.VALOR AS VALOR ";
			where1 = "WHERE DOC.CDDESPESAVIAGEMADIANTAMENTO = " + form.getCddespesaviagem() + " "; 
			where2 = "WHERE MO.CDDESPESAVIAGEMADIANTAMENTO = " + form.getCddespesaviagem() + " ";
		}
		SinedUtil.markAsReader();
		List<OrigemBean> listaUpdate = getJdbcTemplate().query(				
				select1 +
				"FROM DOCUMENTOORIGEM DOC " +
				"JOIN DOCUMENTO D ON D.CDDOCUMENTO = DOC.CDDOCUMENTO " +
				"JOIN DOCUMENTOACAO DOCACAO ON DOCACAO.CDDOCUMENTOACAO = D.CDDOCUMENTOACAO " +
				where1 +
				"AND DOCACAO.CDDOCUMENTOACAO <> " + Documentoacao.CANCELADA.getCddocumentoacao() + " " +
				
				"UNION ALL " +
				
				select2 +
				"FROM MOVIMENTACAOORIGEM MO " +
				"JOIN MOVIMENTACAO M ON M.CDMOVIMENTACAO = MO.CDMOVIMENTACAO " +
				"JOIN MOVIMENTACAOACAO MOACAO ON M.CDMOVIMENTACAOACAO = MOACAO.CDMOVIMENTACAOACAO " +
				where2 + 
				"AND MOACAO.CDMOVIMENTACAOACAO <> " + Movimentacaoacao.CANCELADA.getCdmovimentacaoacao(),
			new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				
				OrigemBean bean = new OrigemBean();
				bean.setTypeorigem(rs.getInt("TYPEORIGEM"));
				bean.setTipo(rs.getString("TIPO"));
				bean.setDescricao(rs.getString("DESCRICAO"));
				bean.setId(rs.getString("ID"));
				bean.setValor(new Money(rs.getInt("VALOR"),true).toString());
				bean.setValorMoney(new Money(rs.getInt("VALOR"), true));
				
				return bean;
			}
		});
		
		return listaUpdate;
	}
	
	@SuppressWarnings("unchecked")
	public Money getValorTotalAdiantamentos(String whereIn, Tipooperacao tipooperacao) {
		if (whereIn == null || "".equals(whereIn)) {
			throw new SinedException("Par�metro inv�lido.");
		}
		SinedUtil.markAsReader();
		List<Money> lista = getJdbcTemplate().query(
				"SELECT SUM(D.VALOR) AS VALOR " +
				"FROM DOCUMENTOORIGEM DOC " +
				"JOIN DOCUMENTO D ON D.CDDOCUMENTO = DOC.CDDOCUMENTO " +
				"JOIN DOCUMENTOACAO DOCACAO ON DOCACAO.CDDOCUMENTOACAO = D.CDDOCUMENTOACAO " +
				"WHERE DOC.CDDESPESAVIAGEMADIANTAMENTO IN (" + whereIn + ") " +
				"AND DOCACAO.CDDOCUMENTOACAO <> " + Documentoacao.CANCELADA.getCddocumentoacao() + " " +
				
				(tipooperacao != null ? "AND D.CDDOCUMENTOCLASSE = " + (Tipooperacao.TIPO_CREDITO.equals(tipooperacao) ? Documentoclasse.CD_RECEBER : Documentoclasse.CD_PAGAR): "") +
				
				" GROUP BY VALOR "+
				" UNION ALL " +
				
				"SELECT SUM(M.VALOR / (select count (*) from movimentacaoorigem movOrigem where movOrigem.cdmovimentacao = MO.CDMOVIMENTACAO)) AS VALOR " +
				"FROM MOVIMENTACAOORIGEM MO " +
				"JOIN MOVIMENTACAO M ON M.CDMOVIMENTACAO = MO.CDMOVIMENTACAO " +
				"JOIN MOVIMENTACAOACAO MOACAO ON M.CDMOVIMENTACAOACAO = MOACAO.CDMOVIMENTACAOACAO " +
				"WHERE MO.CDDESPESAVIAGEMADIANTAMENTO IN (" + whereIn + ") " +
				"AND MOACAO.CDMOVIMENTACAOACAO <> " + Movimentacaoacao.CANCELADA.getCdmovimentacaoacao() +
				
				(tipooperacao != null ? "AND M.CDTIPOOPERACAO = " + (Tipooperacao.TIPO_CREDITO.equals(tipooperacao) ? Tipooperacao.TIPO_CREDITO.getCdtipooperacao() : Tipooperacao.TIPO_DEBITO.getCdtipooperacao()): "") +
				" GROUP BY VALOR "				
				,
				new RowMapper() {
					public Money mapRow(ResultSet rs, int rowNum) throws SQLException {
						return new Money(new Money(rs.getDouble("VALOR"), true));
					}
				});
		
		return lista != null && lista.size() == 1 ? lista.get(0) : new Money();
	}

	/**
	 * Carrega a lista de acertos de uma despesa de viagem
	 *
	 * @param form
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<OrigemBean> buildAcertos(Despesaviagem form) {
		if (form == null || form.getCddespesaviagem() == null) {
			throw new SinedException("A despesa de viagem n�o pode ser nulo.");
		}
		SinedUtil.markAsReader();
		List<OrigemBean> listaUpdate = getJdbcTemplate().query(
				"SELECT "+OrigemBean.CONTAPAGAR+" AS TYPEORIGEM, 'CONTA A PAGAR' AS TIPO, D.CDDOCUMENTO AS ID, D.CDDOCUMENTO AS DESCRICAO, D.VALOR AS VALOR " +
				"FROM DOCUMENTOORIGEM DOC " +
				"JOIN DOCUMENTO D ON D.CDDOCUMENTO = DOC.CDDOCUMENTO " +
				"JOIN DOCUMENTOACAO DOCACAO ON DOCACAO.CDDOCUMENTOACAO = D.CDDOCUMENTOACAO " +
				"WHERE DOC.CDDESPESAVIAGEMACERTO = " + form.getCddespesaviagem() + " " +
				"AND D.CDDOCUMENTOCLASSE = " + Documentoclasse.CD_PAGAR + " " +
				"AND DOCACAO.CDDOCUMENTOACAO <> " + Documentoacao.CANCELADA.getCddocumentoacao() + " " +
				
				"UNION ALL " +
				
				"SELECT "+OrigemBean.CONTARECEBER+" AS TYPEORIGEM, 'CONTA A RECEBER' AS TIPO, D.CDDOCUMENTO AS ID, D.CDDOCUMENTO AS DESCRICAO, D.VALOR AS VALOR " +
				"FROM DOCUMENTOORIGEM DOC " +
				"JOIN DOCUMENTO D ON D.CDDOCUMENTO = DOC.CDDOCUMENTO " +
				"WHERE DOC.CDDESPESAVIAGEMACERTO = " + form.getCddespesaviagem() + " " +
				"AND D.CDDOCUMENTOCLASSE = " + Documentoclasse.CD_RECEBER + " " +
				"AND D.CDDOCUMENTOACAO <> " + Documentoacao.CANCELADA.getCddocumentoacao() + " " +
				
				"UNION ALL " +
				
				"SELECT "+OrigemBean.TRANSFERENCIA+" AS TYPEORIGEM, 'MOVIMENTACAO' AS TIPO, M.CDMOVIMENTACAO AS ID, M.CDMOVIMENTACAO AS DESCRICAO, M.VALOR AS VALOR " +
				"FROM MOVIMENTACAOORIGEM MO " +
				"JOIN MOVIMENTACAO M ON M.CDMOVIMENTACAO = MO.CDMOVIMENTACAO " +
				"JOIN MOVIMENTACAOACAO MOACAO ON M.CDMOVIMENTACAOACAO = MOACAO.CDMOVIMENTACAOACAO " +
				"WHERE MO.CDDESPESAVIAGEMACERTO = " + form.getCddespesaviagem() + " " + 
				"AND MOACAO.CDMOVIMENTACAOACAO <> " + Movimentacaoacao.CANCELADA.getCdmovimentacaoacao(),
			new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				
				OrigemBean bean = new OrigemBean();
				bean.setTypeorigem(rs.getInt("TYPEORIGEM"));
				bean.setTipo(rs.getString("TIPO"));
				bean.setDescricao(rs.getString("DESCRICAO"));
				bean.setId(rs.getString("ID"));
				bean.setValor(new Money(rs.getInt("VALOR"),true).toString());
				
				return bean;
			}
		});
		
		return listaUpdate;
	}
	
	@SuppressWarnings("unchecked")
	public Money getValorTotalAcertos(String whereIn, Tipooperacao tipooperacao) {
		if (whereIn == null || "".equals(whereIn)) {
			throw new SinedException("Par�metro inv�lido.");
		}
		SinedUtil.markAsReader();
		List<Money> lista = getJdbcTemplate().query(
				"SELECT SUM(D.VALOR) AS VALOR " +
				"FROM DOCUMENTOORIGEM DOC " +
				"JOIN DOCUMENTO D ON D.CDDOCUMENTO = DOC.CDDOCUMENTO " +
				"JOIN DOCUMENTOACAO DOCACAO ON DOCACAO.CDDOCUMENTOACAO = D.CDDOCUMENTOACAO " +
				"WHERE DOC.CDDESPESAVIAGEMACERTO IN (" + whereIn + ") " +
				"AND D.CDDOCUMENTOCLASSE = " + Documentoclasse.CD_PAGAR + " " +
				"AND DOCACAO.CDDOCUMENTOACAO <> " + Documentoacao.CANCELADA.getCddocumentoacao() + " " +
				
				(tipooperacao != null ? "AND D.CDDOCUMENTOCLASSE = " + (Tipooperacao.TIPO_CREDITO.equals(tipooperacao) ? Documentoclasse.CD_RECEBER : Documentoclasse.CD_PAGAR): "") +
					
				" GROUP BY VALOR "+
				" UNION ALL " +
				
				"SELECT SUM(M.VALOR / (select count (*) from movimentacaoorigem movOrigem where movOrigem.cdmovimentacao = MO.CDMOVIMENTACAO)) AS VALOR " +
				"FROM MOVIMENTACAOORIGEM MO " +
				"JOIN MOVIMENTACAO M ON M.CDMOVIMENTACAO = MO.CDMOVIMENTACAO " +
				"JOIN MOVIMENTACAOACAO MOACAO ON M.CDMOVIMENTACAOACAO = MOACAO.CDMOVIMENTACAOACAO " +
				"WHERE MO.CDDESPESAVIAGEMACERTO IN (" + whereIn + ") " +
				"AND MOACAO.CDMOVIMENTACAOACAO <> " + Movimentacaoacao.CANCELADA.getCdmovimentacaoacao() +
				
				(tipooperacao != null ? "AND M.CDTIPOOPERACAO = " + (Tipooperacao.TIPO_CREDITO.equals(tipooperacao) ? Tipooperacao.TIPO_CREDITO.getCdtipooperacao() : Tipooperacao.TIPO_DEBITO.getCdtipooperacao()): "") +
				
				" GROUP BY VALOR ",
				new RowMapper() {
					public Money mapRow(ResultSet rs, int rowNum) throws SQLException {
						return new Money(rs.getDouble("VALOR"), true);
					}
				});
		
		
		return lista != null && lista.size() == 1 ? lista.get(0) : new Money();
	}

	/**
	 * Busca as despesas de viagem para o acerto
	 *
	 * @param whereIn
	 * @return
	 * @since 04/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Despesaviagem> findForAcerto(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("despesaviagem.cddespesaviagem, despesaviagem.situacaodespesaviagem, despesaviagem.dtsaida, " +
							"listaDespesaviagemitem.cddespesaviagemitem, despesaviagemtipo.cddespesaviagemtipo, despesaviagemtipo.nome," +
							"listaDespesaviagemitem.valorprevisto, colaborador.cdpessoa")
					.leftOuterJoin("despesaviagem.colaborador colaborador")
					.leftOuterJoin("despesaviagem.listaDespesaviagemitem listaDespesaviagemitem")
					.leftOuterJoin("listaDespesaviagemitem.despesaviagemtipo despesaviagemtipo")
					.whereIn("despesaviagem.cddespesaviagem", whereIn)
					.list();
	}

	/**
	 * Busca a despesa de viagem para atualizar o rateio.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/10/2012
	 */
	public Despesaviagem loadForAtualizarRateio(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("despesaviagem.cddespesaviagem, despesaviagem.situacaodespesaviagem, despesaviagem.dtsaida, empresa.cdpessoa")
					.leftOuterJoin("despesaviagem.empresa empresa")
					.whereIn("despesaviagem.cddespesaviagem", whereIn)
					.unique();
	}
	
	/**
	 * M�todo que busca as despesas de viagem para atualizar o rateio.
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Despesaviagem> loadForAtualizarRateioByDespesaviagem(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("despesaviagem.cddespesaviagem, despesaviagem.situacaodespesaviagem, despesaviagem.dtsaida, empresa.cdpessoa, " +
							"centrocustodespesaviagem.cdcentrocusto, contagerencialcredito.cdcontagerencial, listaDespesaviagemprojeto.cddespesaviagemprojeto, " +
							"projeto.cdprojeto, projeto.nome, listaDespesaviagemprojeto.percentual, " +
							"contagerencialdedito.cdcontagerencial, contagerencial.cdcontagerencial, contagerencialcredito.nome," +
							"contagerencialdedito.nome, contagerencial.nome, centrocustoProjeto.cdcentrocusto, centrocustoProjeto.nome ")
					.leftOuterJoin("despesaviagem.empresa empresa")
					.leftOuterJoin("empresa.centrocustodespesaviagem centrocustodespesaviagem")
					.leftOuterJoin("empresa.contagerencialcredito contagerencialcredito")
					.leftOuterJoin("empresa.contagerencialdebito contagerencialdedito")
					.leftOuterJoin("despesaviagem.listaDespesaviagemprojeto listaDespesaviagemprojeto")
					.leftOuterJoin("listaDespesaviagemprojeto.projeto projeto")
					.leftOuterJoin("despesaviagem.listaDespesaviagemitem listaDespesaviagemitem")
					.leftOuterJoin("listaDespesaviagemitem.despesaviagemtipo despesaviagemtipo")
					.leftOuterJoin("despesaviagemtipo.contagerencial contagerencial")
					.leftOuterJoin("projeto.centrocusto centrocustoProjeto")
					.whereIn("despesaviagem.cddespesaviagem", whereIn)
					.list();
	}

	/**
	 * Busca a despesa de viagem para a gera��o do adiantamento.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/11/2012
	 */
	public Despesaviagem findForAdiatamento(String whereIn) {
		if (whereIn == null || whereIn.equals("") || whereIn.split(",").length > 1) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("despesaviagem.cddespesaviagem, despesaviagem.situacaodespesaviagem, despesaviagem.dtsaida, " +
							"listaDespesaviagemitem.cddespesaviagemitem, despesaviagemtipo.cddespesaviagemtipo, despesaviagemtipo.nome," +
							"listaDespesaviagemitem.valorprevisto")
					.leftOuterJoin("despesaviagem.listaDespesaviagemitem listaDespesaviagemitem")
					.leftOuterJoin("listaDespesaviagemitem.despesaviagemtipo despesaviagemtipo")
					.whereIn("despesaviagem.cddespesaviagem", whereIn)
					.unique();
	}
	
	/**
	 * M�todo que busca as despesas de viagem para a gera��o do adiantamento
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Despesaviagem> findForAdiatamentoByDespesaviagem(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("despesaviagem.cddespesaviagem, despesaviagem.situacaodespesaviagem, despesaviagem.dtsaida, " +
							"listaDespesaviagemitem.cddespesaviagemitem, despesaviagemtipo.cddespesaviagemtipo, despesaviagemtipo.nome," +
							"listaDespesaviagemitem.valorprevisto, colaborador.cdpessoa, colaborador.nome ")
					.leftOuterJoin("despesaviagem.listaDespesaviagemitem listaDespesaviagemitem")
					.leftOuterJoin("listaDespesaviagemitem.despesaviagemtipo despesaviagemtipo")
					.leftOuterJoin("despesaviagem.colaborador colaborador")
					.whereIn("despesaviagem.cddespesaviagem", whereIn)
					.list();
	}

	/**
	 * M�todo que carrega as despesas de viagens para criar a conta
	 *
	 * @param whereInDespesaviagem
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Despesaviagem> findForGerarAcerto(String whereInDespesaviagem) {
		return query()
			.select("despesaviagem.cddespesaviagem, despesaviagem.origemviagem, despesaviagem.destinoviagem, " +
					"despesaviagem.dtsaida, despesaviagem.dtretornoprevisto, despesaviagem.dtretornorealizado, despesaviagem.descricao, " +
					"despesaviagem.motivo, colaborador.cdpessoa, colaborador.nome, despesaviagem.observacao, despesaviagemtipo.cddespesaviagemtipo, " +
					"listaDespesaviagemitem.cddespesaviagemitem, listaDespesaviagemitem.valorprevisto, listaDespesaviagemitem.valorrealizado, " +
					"despesaviagem.situacaodespesaviagem, empresa.cdpessoa, listaDespesaviagemprojeto.cddespesaviagemprojeto, projeto.cdprojeto, " +
					"projeto.nome, listaDespesaviagemprojeto.percentual, centrocustodespesaviagem.cdcentrocusto, contagerencialdebito.cdcontagerencial," +
					"contagerencialcredito.cdcontagerencial, contagerencialcredito.nome, " +
					"contagerencial.cdcontagerencial, contagerencial.nome, " +
					"contagerencialdebitoadiantamento.cdcontagerencial, contagerencialdebitoadiantamento.nome, " +
					"centrocustoProjeto.cdcentrocusto, centrocustoProjeto.nome ")
			.leftOuterJoin("despesaviagem.listaDespesaviagemitem listaDespesaviagemitem")
			.leftOuterJoin("despesaviagem.listaDespesaviagemprojeto listaDespesaviagemprojeto")
			.leftOuterJoin("listaDespesaviagemitem.despesaviagemtipo despesaviagemtipo")
			.leftOuterJoin("despesaviagemtipo.contagerencial contagerencial")
			.leftOuterJoin("despesaviagem.empresa empresa")
			.leftOuterJoin("listaDespesaviagemprojeto.projeto projeto")
			.leftOuterJoin("empresa.centrocustodespesaviagem centrocustodespesaviagem")
			.leftOuterJoin("empresa.contagerencialcredito contagerencialcredito")
			.leftOuterJoin("empresa.contagerencialdebito contagerencialdebito")
			.leftOuterJoin("empresa.contagerencialdebitoadiantamento contagerencialdebitoadiantamento")
			.leftOuterJoin("projeto.centrocusto centrocustoProjeto")
			.join("despesaviagem.colaborador colaborador")
			.whereIn("despesaviagem.cddespesaviagem", whereInDespesaviagem)
			.list();
	}

	/**
	 * M�todo que busca as despesas de viagens de acordo com o projeto
	 *
	 * @param whereInProjeto
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Despesaviagem> findForGerarreceitalote(String whereInProjeto) {
		if (whereInProjeto == null || "".equals(whereInProjeto)) 
			throw new SinedException("Erro na passagem de par�metros.");
		
		return query()
			.select("despesaviagem.cddespesaviagem, despesaviagem.origemviagem, despesaviagem.destinoviagem, despesaviagem.descricao, " +
					"listaDespesaviagemitem.cddespesaviagemitem, listaDespesaviagemitem.valorprevisto, listaDespesaviagemitem.valorrealizado, " +
					"despesaviagem.situacaodespesaviagem, empresa.cdpessoa, listaDespesaviagemprojeto.cddespesaviagemprojeto, projeto.cdprojeto, " +
					"projeto.nome, listaDespesaviagemprojeto.percentual, centrocustodespesaviagem.cdcentrocusto, contagerencialdebito.cdcontagerencial," +
					"contagerencialcredito.cdcontagerencial ")
			.join("despesaviagem.listaDespesaviagemprojeto listaDespesaviagemprojeto")
			.join("listaDespesaviagemprojeto.projeto projeto")
			.leftOuterJoin("despesaviagem.listaDespesaviagemitem listaDespesaviagemitem")
			.leftOuterJoin("listaDespesaviagemitem.despesaviagemtipo despesaviagemtipo")
			.leftOuterJoin("despesaviagem.empresa empresa")
			.leftOuterJoin("empresa.centrocustodespesaviagem centrocustodespesaviagem")
			.leftOuterJoin("empresa.contagerencialcredito contagerencialcredito")
			.leftOuterJoin("empresa.contagerencialdebito contagerencialdebito")
			.where("despesaviagem.situacaodespesaviagem = ?", Situacaodespesaviagem.REALIZADA)
			.whereIn("projeto.cdprojeto", whereInProjeto)
			.list();
	}

	/**
	 * M�todo que busca o total de despesa de viagem do colaborador
	 *
	 * @param empresa
	 * @param colaborador
	 * @param dataGerada
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getTotalDespesaviagemByColaborador(Empresa empresa, Colaborador colaborador, Date dtreferencia1, Date dtreferencia2) {
		if(colaborador == null || colaborador.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Long> query = newQueryBuilder(Long.class)
			.select("SUM(COALESCE(item.valorrealizado, 0))")
			.from(Despesaviagem.class)
			.setUseTranslator(false)
			.join("despesaviagem.colaborador colaborador")
			.leftOuterJoin("despesaviagem.empresa empresa")
			.leftOuterJoin("despesaviagem.listaDespesaviagemitem item")
			.where("despesaviagem.situacaodespesaviagem = ?", Situacaodespesaviagem.REALIZADA)
			.where("colaborador = ?", colaborador)
			.openParentheses()
				.where("despesaviagem.dtretornorealizado is null")
				.or()
				.openParentheses()
					.where("despesaviagem.dtretornorealizado >= ?", dtreferencia1)
					.where("despesaviagem.dtretornorealizado <= ?", dtreferencia2)
				.closeParentheses()
			.closeParentheses()
			.openParentheses()
				.where("empresa = ?", empresa).or()
				.where("empresa is null")
			.closeParentheses();
		
		Long valor = query.unique();
		Money valortotal = new Money();
		if(valor != null)
			valortotal = new Money(valor/100);
			
		return valortotal; 
	}
	
	
	public List<Despesaviagem> findForCSV(DespesaviagemFiltro filtro){
		QueryBuilder<Despesaviagem> query = query();
		updateListagemQuery(query, filtro);
		query
		.select("despesaviagem.cddespesaviagem, despesaviagem.origemviagem, despesaviagem.destinoviagem, " +
				"despesaviagem.dtsaida, despesaviagem.dtretornorealizado, colaborador.nome, despesaviagem.situacaodespesaviagem," +
				"despesaviagemtipo.nome,listaDespesaviagemitem.valorprevisto,listaDespesaviagemitem.valorrealizado, " +
				"despesaviagem.despesaviagemdestino, despesaviagem.reembolsavel  ")
		.joinIfNotExists("despesaviagem.colaborador colaborador")
		.leftOuterJoinIfNotExists("despesaviagem.listaDespesaviagemprojeto listaDespesaviagemprojeto")
		.leftOuterJoinIfNotExists("listaDespesaviagemprojeto.projeto projeto")
		.leftOuterJoinIfNotExists("despesaviagem.listaDespesaviagemitem listaDespesaviagemitem")
		.leftOuterJoinIfNotExists("listaDespesaviagemitem.despesaviagemtipo despesaviagemtipo")
		.orderBy("despesaviagem.cddespesaviagem desc");
		return query.list();
	}

	public List<Despesaviagem> findForEnviarEmail(String whereIn) {
		QueryBuilder<Despesaviagem> query = query();
		query
		.select("despesaviagem.cddespesaviagem, empresa.email, empresa.emailfinanceiro")
		.join("despesaviagem.empresa empresa")
		.whereIn("despesaviagem.cddespesaviagem", whereIn);
		return query.list();
	}

	/**
	 * M�todo que busca os itens para a a��o Emitir Ficha
	 * @param whereIn
	 * @return
	 */
	public List<Despesaviagem> findForEmitirFichaDespesaViagem(String whereIn) {
		QueryBuilder<Despesaviagem> query = query();
		query
		.select("despesaviagem.cddespesaviagem, despesaviagem.origemviagem, despesaviagem.destinoviagem, despesaviagem.descricao, despesaviagem.motivo, " +
				"despesaviagem.dtsaida, despesaviagem.dtretornoprevisto, despesaviagem.dtretornorealizado, despesaviagem.situacaodespesaviagem,  " +
				"colaborador.nome, despesaviagemtipo.nome, listaDespesaviagemitem.documento, listaDespesaviagemitem.valorprevisto,listaDespesaviagemitem.valorrealizado, " +
				"listaDespesaviagemitem.quantidade, projeto.nome, listaDespesaviagemprojeto.percentual")
		.join("despesaviagem.colaborador colaborador")
		.leftOuterJoin("despesaviagem.listaDespesaviagemprojeto listaDespesaviagemprojeto")
		.leftOuterJoin("listaDespesaviagemprojeto.projeto projeto")
		.leftOuterJoin("despesaviagem.listaDespesaviagemitem listaDespesaviagemitem")
		.leftOuterJoin("listaDespesaviagemitem.despesaviagemtipo despesaviagemtipo")
		.whereIn("despesaviagem.cddespesaviagem", whereIn)
		.orderBy("despesaviagem.cddespesaviagem desc");
		
		return query.list();
	}

	/**
	 * M�todo que carrega a despesa viagem para calcular os totais
	 * @param filtro
	 * @return
	 */
	public List<Despesaviagem> findForPreencherTotal(DespesaviagemFiltro filtro) {
		QueryBuilder<Despesaviagem> query = query();
		this.updateListagemQuery(query, filtro);
		
		query.select("despesaviagem.cddespesaviagem, listaDespesaviagemitem.cddespesaviagemitem, listaDespesaviagemitem.valorprevisto, " +
					"listaDespesaviagemitem.valorrealizado");
		query.leftOuterJoin("despesaviagem.listaDespesaviagemitem listaDespesaviagemitem");
		query.setIgnoreJoinPaths(new HashSet<String>());
		
		return query.list();
	}

	/**
	* M�todo que carrega as despesas de viagem para criar o reembolso
	*
	* @param whereIn
	* @return
	* @since 02/07/2015
	* @author Luiz Fernando
	*/
	public List<Despesaviagem> findForReembolso(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		return query()
			.select("despesaviagem.cddespesaviagem, despesaviagem.reembolsavel, despesaviagem.situacaodespesaviagem, " +
					"despesaviagem.despesaviagemdestino, despesaviagem.dtsaida, despesaviagem.dtretornorealizado, " +
					"despesaviagem.reembolsogerado, " +
					"empresa.cdpessoa, empresa.nome, " +
					"cliente.cdpessoa, cliente.nome, " +
					"fornecedor.cdpessoa, fornecedor.nome, " +
					"listaDespesaviagemitem.valorrealizado, listaDespesaviagemitem.documento, " +
					"despesaviagemtipo.cddespesaviagemtipo, despesaviagemtipo.nome ")
			.leftOuterJoin("despesaviagem.empresa empresa")
			.leftOuterJoin("despesaviagem.cliente cliente")
			.leftOuterJoin("despesaviagem.fornecedor fornecedor")
			.leftOuterJoin("despesaviagem.listaDespesaviagemitem listaDespesaviagemitem")
			.leftOuterJoin("listaDespesaviagemitem.despesaviagemtipo despesaviagemtipo")
			.whereIn("despesaviagem.cddespesaviagem", whereIn)
			.list();
	}

	/**
	* M�todo que faz o update no campo reembolsogerado
	*
	* @param whereInDespesaviagemReembolso
	* @param reembolsogerado
	* @since 02/07/2015
	* @author Luiz Fernando
	*/
	public void updateReembolso(String whereInDespesaviagemReembolso, boolean reembolsogerado) {
		if (whereInDespesaviagemReembolso == null || whereInDespesaviagemReembolso.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		getHibernateTemplate().bulkUpdate("update Despesaviagem d set d.reembolsogerado = ? where d.id in ("+whereInDespesaviagemReembolso+")", 
				new Object[]{reembolsogerado});	
	}
	
	/**
	* M�todo que retorna as despesas de viagens para verifica a situa��o anterior
	*
	* @param whereIn
	* @return
	* @since 21/03/2016
	* @author Luiz Fernando
	*/
	public List<Despesaviagem> findForVerificacaoSituacaoAnterior(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("despesaviagem.cddespesaviagem, despesaviagem.situacaodespesaviagem, " +
							"listaDespesaviagemitem.valorrealizado, " +
							"listaDespesaviagemHistorico.acao, listaDespesaviagemHistorico.dtaltera ")
					.leftOuterJoin("despesaviagem.listaDespesaviagemitem listaDespesaviagemitem")
					.leftOuterJoin("despesaviagem.listaDespesaviagemHistorico listaDespesaviagemHistorico")
					.whereIn("despesaviagem.cddespesaviagem", whereIn)
					.orderBy("despesaviagem.cddespesaviagem, listaDespesaviagemHistorico.dtaltera desc ")
					.list();
	}
}
