package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Radiushistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RadiushistoricoDAO extends GenericDAO<Radiushistorico>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Radiushistorico> query, FiltroListagem _filtro) {
		query.where("dtfim is null")
			 .orderBy("usuario,dtinicio");
	}
	
	/**
	 * Preenche a data de fim da sess�o dos registros passados como par�metro
	 * @param lista
	 * @author Thiago Gon�alves
	 */
	public void confirmartempo(String lista){
		StringBuilder sql = new StringBuilder(
				"update Radiushistorico r " +
				"set r.dtfim = ? " +
				"where r.cdradiushistorico in (" + lista + ")");
		
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		Object[] fields = new Object[]{hoje};		
		getHibernateTemplate().bulkUpdate(sql.toString(), fields);		
	}
}
