package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Atividadetipoitem;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AtividadetipoitemDAO extends GenericDAO<Atividadetipoitem>{
	
	/**
	 * Retorna os itens de arquivo com o tipo de atividade
	 * @param tipo
	 * @return
	 * @author Taidson
	 * @since 01/10/2010
	 */
	public List<Atividadetipoitem> findByTipo(Atividadetipo tipo){
		if(tipo == null  || tipo.getCdatividadetipo() == null){
			throw new SinedException("Objeto Atividadetipo n�o pode ser nulo.");
		}
		return query()
	   		 .select("atividadetipoitem.cdatividadetipoitem, atividadetipoitem.nome, atividadetipoitem.obrigatorio")
	   		 		.where("atividadetipoitem.atividadetipo = ?", tipo)
	   		 		.orderBy("atividadetipoitem.ordem, atividadetipoitem.nome")
	   		 		.list();
	}

}
