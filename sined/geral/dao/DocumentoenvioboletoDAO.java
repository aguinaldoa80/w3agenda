package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Documentoenvioboleto;
import br.com.linkcom.sined.geral.bean.Documentoenvioboletoacao;
import br.com.linkcom.sined.geral.bean.Documentoenvioboletohistorico;
import br.com.linkcom.sined.geral.service.DocumentoenvioboletohistoricoService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.enumeration.Documentoenvioboletosituacaoagendamento;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.DocumentoenvioboletoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentoenvioboletoDAO extends GenericDAO<Documentoenvioboleto>{

	private DocumentoenvioboletohistoricoService documentoenvioboletohistoricoService;
	public void setDocumentoenvioboletohistoricoService(
			DocumentoenvioboletohistoricoService documentoenvioboletohistoricoService) {
		this.documentoenvioboletohistoricoService = documentoenvioboletohistoricoService;
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaDiascobranca");
		save.saveOrUpdateManaged("listaSituacao");
		save.saveOrUpdateManaged("listaCliente");
		save.saveOrUpdateManaged("listaHistorico");
		super.updateSaveOrUpdate(save);
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Documentoenvioboleto> query,
			FiltroListagem filtro) {
		DocumentoenvioboletoFiltro _filtro = (DocumentoenvioboletoFiltro)filtro;
		query
			.select("distinct documentoenvioboleto.cddocumentoenvioboleto, documentoenvioboleto.dtinicio, documentoenvioboleto.dtfim, " +
					"documentoenvioboleto.situacaoagendamento, empresa.cdpessoa, empresa.razaosocial, empresa.nome, empresa.nomefantasia")
			.leftOuterJoin("documentoenvioboleto.empresa empresa")
			.leftOuterJoin("documentoenvioboleto.listaCliente documentoenvioboletocliente")
			.leftOuterJoin("documentoenvioboleto.listaDiascobranca documentoenvioboletodiascobranca")
			.leftOuterJoin("documentoenvioboleto.listaSituacao documentoenvioboletosituacao")
			.leftOuterJoin("documentoenvioboletosituacao.documentoacao documentoacao")
			.leftOuterJoin("documentoenvioboletocliente.cliente cliente")
			.ignoreJoin("documentoenvioboletocliente", "documentoenvioboletodiascobranca", "documentoenvioboletosituacao", "documentoacao", "cliente");
		
		if(_filtro.getEmpresa() != null){
			query.where("empresa = ?", _filtro.getEmpresa());
		}
		if(_filtro.getCliente() != null){
			query.where("cliente = ?", _filtro.getCliente());
		}
		if(_filtro.getDtinicio() != null){
			query.where("documentoenvioboleto.dtinicio >= ?", _filtro.getDtinicio());
		}
		if(_filtro.getDtfim() != null){
			query.where("documentoenvioboleto.dtfim <= ?", _filtro.getDtfim());
		}
		if(_filtro.getSituacaoagendamento() != null){
			query.where("documentoenvioboleto.situacaoagendamento = ?", _filtro.getSituacaoagendamento());
		}
		if(SinedUtil.isListNotEmpty(_filtro.getListaAcao())){
			query.whereIn("documentoacao.cddocumentoacao", SinedUtil.listAndConcatenate(_filtro.getListaAcao(), "cddocumentoacao", ","));
		}
		super.updateListagemQuery(query, filtro);
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Documentoenvioboleto> query) {
		query
			.select("documentoenvioboleto.cddocumentoenvioboleto, documentoenvioboleto.dtinicio, documentoenvioboleto.dtfim, documentoenvioboleto.situacaoagendamento, " +
					"empresa.cdpessoa, empresa.razaosocial, empresa.nome, empresa.nomefantasia, " +
					"documentoenvioboletocliente.cddocumentoenvioboletocliente, " +
					"cliente.cdpessoa, cliente.razaosocial, cliente.nome, " +
					"documentoenvioboletodiascobranca.cddocumentoenvioboletodiascobranca, documentoenvioboletodiascobranca.qtdedias, " +
					"documentoenvioboletosituacao.cddocumentoenvioboletosituacao, " +
					"documentoacao.cddocumentoacao, documentoacao.nome")
			.leftOuterJoin("documentoenvioboleto.empresa empresa")
			.leftOuterJoin("documentoenvioboleto.listaCliente documentoenvioboletocliente")
			.leftOuterJoin("documentoenvioboleto.listaDiascobranca documentoenvioboletodiascobranca")
			.leftOuterJoin("documentoenvioboleto.listaSituacao documentoenvioboletosituacao")
			.leftOuterJoin("documentoenvioboletosituacao.documentoacao documentoacao")
			.leftOuterJoin("documentoenvioboletocliente.cliente cliente");
//		super.updateEntradaQuery(query);
	}
	
	public void cancelarAgendamento(String whereIn){
		List<Documentoenvioboleto> listaAgendamento = this.findAtivos(whereIn);
		if(SinedUtil.isListNotEmpty(listaAgendamento)){
			getJdbcTemplate().update("update documentoenvioboleto set situacaoagendamento = "+Documentoenvioboletosituacaoagendamento.CANCELADO.getValue()+" where cddocumentoenvioboleto in ("+CollectionsUtil.listAndConcatenate(listaAgendamento, "cddocumentoenvioboleto", ",")+")");
			for(Documentoenvioboleto bean: listaAgendamento){
				Documentoenvioboletohistorico historico = new Documentoenvioboletohistorico();
				historico.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
				historico.setDtaltera(SinedDateUtils.currentTimestamp());
				historico.setDocumentoenvioboletoacao(Documentoenvioboletoacao.CANCELADO);
				historico.setDocumentoenvioboleto(bean);
				documentoenvioboletohistoricoService.saveOrUpdate(historico);
			}			
		}
	}
	
	public List<Documentoenvioboleto> findAtivos(String whereIn){
		return query()
			.select("documentoenvioboleto.cddocumentoenvioboleto, documentoenvioboleto.situacaoagendamento")
			.whereIn("documentoenvioboleto.cddocumentoenvioboleto", whereIn)
			.whereIn("documentoenvioboleto.situacaoagendamento", Documentoenvioboletosituacaoagendamento.ATIVO.getValue().toString())
			.list();
	}
	
	public List<Documentoenvioboleto> findForConcluir(){
		return query()
			.select("documentoenvioboleto.cddocumentoenvioboleto, documentoenvioboleto.situacaoagendamento")
			.whereIn("documentoenvioboleto.situacaoagendamento", Documentoenvioboletosituacaoagendamento.ATIVO.getValue().toString())
			.where("documentoenvioboleto.dtfim <= ?", SinedDateUtils.currentDate())
			.list();
	}
	
	public List<Documentoenvioboleto> findForEnvioautomatico(){
		Date dataAtual = new Date(System.currentTimeMillis());
		return query()
			.select("documentoenvioboleto.cddocumentoenvioboleto, documentoenvioboleto.dtinicio, documentoenvioboleto.dtfim, documentoenvioboleto.situacaoagendamento, "+
				"cliente.cdpessoa, empresa.cdpessoa, "+
				"documentoacao.cddocumentoacao, "+
				"listaDiascobranca.cddocumentoenvioboletodiascobranca, listaDiascobranca.qtdedias")
			.leftOuterJoin("documentoenvioboleto.empresa empresa")
			.leftOuterJoin("documentoenvioboleto.listaCliente listaCliente")
			.leftOuterJoin("listaCliente.cliente cliente")
			.leftOuterJoin("documentoenvioboleto.listaSituacao listaSituacao")
			.leftOuterJoin("listaSituacao.documentoacao documentoacao")
			.leftOuterJoin("documentoenvioboleto.listaDiascobranca listaDiascobranca")
			.where("documentoenvioboleto.situacaoagendamento = ?", Documentoenvioboletosituacaoagendamento.ATIVO)
			.where("documentoenvioboleto.dtinicio <= ?", dataAtual)
			.openParentheses()
				.where("documentoenvioboleto.dtfim >= ?", dataAtual)
				.or()
				.where("documentoenvioboleto.dtfim is null")
			.closeParentheses()
			.list();
	}
	
	public void concluiAgendamentosVencidos(){
		List<Documentoenvioboleto> listaAgendamento = this.findForConcluir();
		if(SinedUtil.isListNotEmpty(listaAgendamento)){
			getJdbcTemplate().update("update documentoenvioboleto set situacaoagendamento = "+Documentoenvioboletosituacaoagendamento.CONCLUIDO.getValue()+" where cddocumentoenvioboleto in ("+CollectionsUtil.listAndConcatenate(listaAgendamento, "cddocumentoenvioboleto", ",")+")");
			for(Documentoenvioboleto bean: listaAgendamento){
				Documentoenvioboletohistorico historico = new Documentoenvioboletohistorico();
				historico.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
				historico.setDtaltera(SinedDateUtils.currentTimestamp());
				historico.setDocumentoenvioboletoacao(Documentoenvioboletoacao.CONCLUIDO);
				historico.setDocumentoenvioboleto(bean);
				documentoenvioboletohistoricoService.saveOrUpdate(historico);
			}			
		}
	}

	public boolean existeAgedamentoCancelado(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Documentoenvioboleto.class)
			.whereIn("documentoenvioboleto.cddocumentoenvioboleto", whereIn)
			.where("documentoenvioboleto.situacaoagendamento = ?", Documentoenvioboletosituacaoagendamento.CANCELADO)
			.unique() > 0;
	}

	@Override
	public ListagemResult<Documentoenvioboleto> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Documentoenvioboleto> query = query();
		updateListagemQuery(query, filtro);
		
		return new ListagemResult<Documentoenvioboleto>(query, filtro);
	}
}