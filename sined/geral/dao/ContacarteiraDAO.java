package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("contacarteira.carteira")
public class ContacarteiraDAO extends GenericDAO<Contacarteira>{
	
	public List<Contacarteira> findByConta(Conta conta){
		return query()
					.leftOuterJoin("contacarteira.bancoConfiguracaoRemessa bancoConfiguracaoRemessa")
					.leftOuterJoin("contacarteira.bancoConfiguracaoRetorno bancoConfiguracaoRetorno")
					.leftOuterJoin("contacarteira.conta conta")
					.where("conta = ?", conta)
					.list();
	}

	public List<Contacarteira> findByContaSemAprovacao(Conta conta){
		return query()
					.leftOuterJoin("contacarteira.bancoConfiguracaoRemessa bancoConfiguracaoRemessa")
					.leftOuterJoin("contacarteira.bancoConfiguracaoRetorno bancoConfiguracaoRetorno")
					.where("coalesce(contacarteira.aprovadoproducao, false) <> ?", Boolean.TRUE)
					.leftOuterJoin("contacarteira.conta conta")
					.where("conta = ?", conta)
					.list();
	}
	
	public List<Contacarteira> findByConta(Conta conta, Boolean ignoraCarteiraQueNaoGeraBoleto, Boolean ignoraCarteiraQueNaoAssociaContaReceber){
		if(conta==null || conta.getCdconta()==null){
			return null;
		}
		QueryBuilder<Contacarteira> qry = query()
					.leftOuterJoin("contacarteira.bancoConfiguracaoRemessa bancoConfiguracaoRemessa")
					.leftOuterJoin("contacarteira.bancoConfiguracaoRetorno bancoConfiguracaoRetorno")
					.leftOuterJoin("contacarteira.conta conta")
					.where("conta = ?", conta);
		
		if(Boolean.TRUE.equals(ignoraCarteiraQueNaoAssociaContaReceber)){
			qry.where("(contacarteira.naoassociarcontareceber is null or contacarteira.naoassociarcontareceber = false)");
		}
		
		if(Boolean.TRUE.equals(ignoraCarteiraQueNaoGeraBoleto)){
			qry.where("(contacarteira.naogerarboleto is null or contacarteira.naogerarboleto = false)");
		}
		return qry.list();
	}

	public Contacarteira isNossoNumeroIndividual(Integer cdcontacarteira) {
		if(cdcontacarteira == null){
			throw new SinedException("O par�metro cdcontacarteira n�o pode ser nulo.");
		}
		
		return querySined()
				
				.select("contacarteira.cdcontacarteira, contacarteira.nossonumeroindividual")
				.where("contacarteira.cdcontacarteira = ?", cdcontacarteira)
				.unique();
	}
	
}
