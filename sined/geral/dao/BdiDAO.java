package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Bdi;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class BdiDAO extends GenericDAO<Bdi>{
	
	/***
	 * Retorna o BDI com seus itens e composi��es de um determinado or�amento
	 * 
	 * @param orcamento
	 * @return List<Bdi>
	 * @throws SinedException - caso o par�metro or�amento seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public Bdi getByOrcamento(Orcamento orcamento) {
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		
		return
			query()
				.select("bdi.cdbdi, bdi.folga, bdi.incluimod, bdi.incluimoi, bdi.valorrecursogeraldireto, bdi.valoroutrastarefas, " +
						"orcamento.cdorcamento, orcamento.nome, bdi.incluioutrastarefas, bdi.incluirecursogeraldireto,  " +
						"bdi.incluiacrescimo, bdi.valoracrescimo, " +
						"bdicomposicaoorcamento.cdbdicomposicaoorcamento, bdicomposicaoorcamento.inclui, " +
						"composicaoorcamento.cdcomposicaoorcamento, " +
						"bdiitem.cdbdiitem, bdiitem.nome, bdiitem.valor, " +
						"bdiitempai.cdbdiitem, bdiitempai.nome, bdiitempai.valor, " +
						"contagerencialBdiitem.cdcontagerencial, contagerencialBdiitem.nome, " +
						"orcamento.cdorcamento, orcamento.calcularhistograma, " +
						"bdicalculotipo.cdbdicalculotipo," +
						"bditributo.cdbditributo, bditributo.nome, bditributo.valor, " +
						"bditributopai.cdbditributo, bditributopai.nome, bditributopai.valor, " +
						"contagerencialBditributo.cdcontagerencial, contagerencialBditributo.nome ")
				.leftOuterJoin("bdi.listaBdicomposicaoorcamento bdicomposicaoorcamento")
				.leftOuterJoin("bdicomposicaoorcamento.composicaoorcamento composicaoorcamento")
				.leftOuterJoin("bdi.listaBdiitem bdiitem")
				.leftOuterJoin("bdiitem.contagerencial contagerencialBdiitem")
				.leftOuterJoin("bdiitem.bdiitempai bdiitempai")
				.leftOuterJoin("bdi.listaBditributo bditributo")
				.leftOuterJoin("bditributo.bditributopai bditributopai")
				.leftOuterJoin("bditributo.contagerencial contagerencialBditributo")
				.join("bdi.orcamento orcamento")
				.leftOuterJoin("orcamento.bdicalculotipo bdicalculotipo")
				.where("orcamento = ?", orcamento)
				.orderBy("bdiitem.nome DESC")
				.unique();
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaBdicomposicaoorcamento");
		save.saveOrUpdateManaged("listaBdiitem");
		save.saveOrUpdateManaged("listaBditributo");
	}

	/**
	 * M�todo que retorno o bdi do orcamento
	 *
	 * @param orcamento
	 * @return
	 * @author Luiz Fernando
	 */
	public Bdi findByOrcamento(Orcamento orcamento) {
		if(orcamento == null || orcamento.getCdorcamento() == null)
			throw new SinedException("Par�metro inv�lido");
		
		return query()
				.select("bdi.cdbdi, bdiitem.cdbdiitem, bdiitem.valor ")
				.join("bdi.orcamento orcamento")
				.leftOuterJoin("bdi.listaBdiitem bdiitem")
				.where("orcamento = ?", orcamento)
				.unique();
	}
}
