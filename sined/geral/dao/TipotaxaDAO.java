package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("tipotaxa.nome")
public class TipotaxaDAO extends GenericDAO<Tipotaxa>{

	@Override
	public void updateListagemQuery(QueryBuilder<Tipotaxa> query,
			FiltroListagem filtro) {
		
		query.select("tipotaxa.cdtipotaxa, tipotaxa.nome");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Tipotaxa> query) {
		query.select("tipotaxa.cdtipotaxa, tipotaxa.nome, contacredito.cdcontagerencial, contacredito.nome, contadebito.cdcontagerencial, contadebito.nome," +
				"centrocusto.cdcentrocusto, centrocusto.nome")
			.leftOuterJoin("tipotaxa.contacredito contacredito")
			.leftOuterJoin("tipotaxa.contadebito contadebito")
			.leftOuterJoin("tipotaxa.centrocusto centrocusto");
	}
	
	/**
	 * 
	 * M�todo que carrega o tipotaxa no banco
	 *
	 *@author Thiago Augusto
	 *@date 14/03/2012
	 * @param tipotaxa
	 * @return
	 */
	public Tipotaxa getTipoTaxa(Tipotaxa tipotaxa){
		return query().select("tipotaxa.cdtipotaxa, tipotaxa.nome, contacredito.cdcontagerencial, contacredito.nome, contadebito.cdcontagerencial, contadebito.nome," +
			"centrocusto.cdcentrocusto, centrocusto.nome")
		.leftOuterJoin("tipotaxa.contacredito contacredito")
		.leftOuterJoin("tipotaxa.contadebito contadebito")
		.leftOuterJoin("tipotaxa.centrocusto centrocusto")
		.where("tipotaxa = ? ", tipotaxa)
		.unique();
	}

	@Override
	public ListagemResult<Tipotaxa> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Tipotaxa> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("tipotaxa.cdtipotaxa");
		
		return new ListagemResult<Tipotaxa>(query, filtro);
	}
}
