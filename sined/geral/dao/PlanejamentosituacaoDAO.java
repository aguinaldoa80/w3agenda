package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Planejamentosituacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("planejamentosituacao.nome")
public class PlanejamentosituacaoDAO extends GenericDAO<Planejamentosituacao> {

}
