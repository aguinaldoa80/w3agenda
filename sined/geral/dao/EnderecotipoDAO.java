package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("enderecotipo.nome")
public class EnderecotipoDAO extends GenericDAO<Enderecotipo>{

	public List<Enderecotipo> findForAndroid(String whereIn) {
		return query()
			.select("enderecotipo.cdenderecotipo, enderecotipo.nome")
			.whereIn("enderecotipo.cdenderecotipo", whereIn)
			.list();
	}
}
