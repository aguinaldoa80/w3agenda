package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.MaterialTabelaPrecoHistorico;
import br.com.linkcom.sined.geral.bean.Materialtabelapreco;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialTabelaPrecoHistoricoDAO extends GenericDAO<MaterialTabelaPrecoHistorico> {

	public List<MaterialTabelaPrecoHistorico> findByMaterialtabelapreco(Materialtabelapreco materialtabelapreco) {
		return query().leftOuterJoinFetch("materialTabelaPrecoHistorico.arquivoImportacaoProduto arquivoImportacaoProduto").where("materialTabelaPrecoHistorico.materialtabelapreco = ?", materialtabelapreco).orderBy("materialTabelaPrecoHistorico.dtaltera desc").list();
	}
}
