package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RateioitemDAO extends GenericDAO<Rateioitem>{
	
	/**
	 * Carrega lista de rateioitem a partir de um rateio.
	 * 
	 * @param rateio
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Rateioitem> findByRateio(Rateio rateio){
		if (rateio == null || rateio.getCdrateio() == null) {
			throw new SinedException("Rateio n�o pode ser nulo.");
		}
		return query()
					.select("rateioitem.cdrateioitem, rateioitem.valor, rateioitem.percentual, " +
							"contagerencial.nome, contagerencial.cdcontagerencial, projeto.nome, " +
							"projeto.cdprojeto, centrocusto.nome, centrocusto.cdcentrocusto, " +
							"vcontagerencial.identificador, vcontagerencial.arvorepai, " +
							"tipooperacao.cdtipooperacao")
							
					.join("rateioitem.contagerencial contagerencial")
					.join("contagerencial.vcontagerencial vcontagerencial")
					.join("rateioitem.centrocusto centrocusto")
					.leftOuterJoin("rateioitem.projeto projeto")
					.join("contagerencial.tipooperacao tipooperacao")
					.where("rateioitem.rateio = ?",rateio)
					.list();
	}

	/**
	 * Busca os itens de rateio dos documentos do whereIn.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Rateioitem> findByDocumentos(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Where In de documentos n�o pode ser nulo.");
		}
		return query()
					.select("rateioitem.cdrateioitem, rateioitem.valor, rateioitem.percentual, " +
							"contagerencial.nome, contagerencial.cdcontagerencial, projeto.nome, " +
							"projeto.cdprojeto, centrocusto.nome, centrocusto.cdcentrocusto")
							
					.join("rateioitem.contagerencial contagerencial")
					.join("rateioitem.centrocusto centrocusto")
					.leftOuterJoin("rateioitem.projeto projeto")
					.join("rateioitem.rateio rateio")
					.join("rateio.listaDocumento listaDocumento")
					.whereIn("listaDocumento.cddocumento",whereIn)
					.list();
	}

	/**
	 * M�todo que atualiza o valor do rateio item
	 * 
	 * @param rateioitem
	 * @author Tom�s Rabelo
	 */
	public void updateValorRateioItem(Rateioitem rateioitem) {
		if(rateioitem == null || rateioitem.getCdrateioitem() == null || rateioitem.getValor() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		String sql = "update Rateioitem ri " + "set ri.valor = ? where ri.id = ?";
		Object[] objetos = new Object[] { rateioitem.getValor(), rateioitem.getCdrateioitem()};
		
		getHibernateTemplate().bulkUpdate(sql, objetos);
	}

	/**
	 * M�todo que carrega os itens do rateio com o projeto e cliente do projeto
	 *
	 * @param rateio
	 * @return
	 * @author Luiz Fernando
	 * @since 02/12/2013
	 */
	public List<Rateioitem> findByRateioWithProjetoCliente(Rateio rateio) {
		if(rateio == null || rateio.getCdrateio() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("rateioitem.cdrateioitem, rateioitem.valor, rateioitem.percentual, " +
					"contagerencial.nome, contagerencial.cdcontagerencial, projeto.nome, " +
					"projeto.cdprojeto, centrocusto.nome, centrocusto.cdcentrocusto, " +
					"vcontagerencial.identificador, vcontagerencial.arvorepai, " +
					"cliente.cdpessoa, cliente.nome, cliente.tipopessoa, cliente.cnpj, cliente.cpf, cliente.email, " +
					"cliente.inscricaoestadual, listaEndereco.logradouro, listaEndereco.bairro, " +
					"listaEndereco.cep, listaEndereco.caixapostal, listaEndereco.numero, listaEndereco.complemento, " +
					"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.nome, uf.sigla, listaTelefone.telefone ")					
			.join("rateioitem.contagerencial contagerencial")
			.join("contagerencial.vcontagerencial vcontagerencial")
			.join("rateioitem.centrocusto centrocusto")
			.join("rateioitem.projeto projeto")
			.leftOuterJoin("projeto.cliente cliente")
			.leftOuterJoin("cliente.listaEndereco listaEndereco")
			.leftOuterJoin("listaEndereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("cliente.listaTelefone listaTelefone")
			.where("rateioitem.rateio = ?",rateio)
			.list();
	}

	/**
	 * M�todo que atualiza a porcentagem do rateio item
	 * 
	 * @param rateioitem
	 * @author Lucas Costa
	 */
	public void updateProcentagemRateioItem(Rateioitem rateioitem) {
		if(rateioitem == null || rateioitem.getCdrateioitem() == null || rateioitem.getValor() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		String sql = "update Rateioitem ri " + "set ri.percentual = ? where ri.id = ?";
		Object[] objetos = new Object[] { rateioitem.getPercentual(), rateioitem.getCdrateioitem()};
		
		getHibernateTemplate().bulkUpdate(sql, objetos);
	}

	/**
	 * Busca os itens de rateio para os relat�rios da listagem de fatura de loca��o.
	 * 	 
	 * @param whereInRateio
	 * @return
	 * @author Rodrigo Freitas
	 * @since 09/11/2017
	 */
	public List<Rateioitem> findForFaturaLocacao(String whereInRateio) {
		if(whereInRateio == null || whereInRateio.trim().equals("")){
			return null;
		}
		
		return query()
					.select("rateioitem.cdrateioitem, rateioitem.valor, centrocusto.cdcentrocusto, centrocusto.nome, rateio.cdrateio")
					.join("rateioitem.rateio rateio")
					.join("rateioitem.centrocusto centrocusto")
					.whereIn("rateio.cdrateio", whereInRateio)
					.orderBy("rateioitem.cdrateioitem")
					.list();
	}
	
}
