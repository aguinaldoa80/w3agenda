package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Cargoriscointensidade;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CargoriscointensidadeFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CargoriscointensidadeDAO extends GenericDAO<Cargoriscointensidade> {

	@Override
	public void updateListagemQuery(QueryBuilder<Cargoriscointensidade> query,	FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inválidos");
		}
		CargoriscointensidadeFiltro filtro = (CargoriscointensidadeFiltro) _filtro;
		query
		.select("cargoriscointensidade.cdcargoriscointensidade, cargoriscointensidade.nome")
		.whereLikeIgnoreAll("cargoriscointensidade.nome", filtro.getNome());
	}
	
	
	/* singleton */
	private static CargoriscointensidadeDAO instance;
	public static CargoriscointensidadeDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(CargoriscointensidadeDAO.class);
		}
		return instance;
	}
}