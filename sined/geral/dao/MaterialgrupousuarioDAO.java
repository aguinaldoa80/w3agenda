package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialgrupousuario;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialgrupousuarioDAO extends GenericDAO<Materialgrupousuario>{

	/**
	 * M�todo para obter uma lista de compradores por grupo de material.
	 * 
	 * @param materialgrupo
	 * @return 
	 * @author Marden Silva
	 */
	public List<Materialgrupousuario> findByMaterialGrupo(Materialgrupo materialgrupo) {
		if (materialgrupo == null || materialgrupo.getCdmaterialgrupo() == null) {
			throw new SinedException("O grupo de materiais n�o pode ser nulo.");
		}
		
		return query()
			.select("materialgrupousuario.cdmaterialgrupousuario, materialgrupousuario.cdmaterialgrupo, materialgrupousuario.usuario")
			.join("materialgrupousuario.materialgrupo materialgrupo")
			.join("materialgrupousuario.usuario usuario")
			.where("materialgrupousuario.materialgrupo = ?", materialgrupo)
			.list();		
	}

}
