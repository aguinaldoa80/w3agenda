package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Inutilizacaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Inutilizacaosituacao;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.InutilizacaonfeFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SintegraFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.SagefiscalFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class InutilizacaonfeDAO extends GenericDAO<Inutilizacaonfe> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Inutilizacaonfe> query, FiltroListagem _filtro) {
		InutilizacaonfeFiltro filtro = (InutilizacaonfeFiltro) _filtro;
		
		query
			.select("inutilizacaonfe.cdinutilizacaonfe, empresa.nome, empresa.razaosocial, empresa.nomefantasia, configuracaonfe.descricao, " +
					"inutilizacaonfe.dtinutilizacao, inutilizacaonfe.numinicio, inutilizacaonfe.numfim, " +
					"inutilizacaonfe.situacao, inutilizacaonfe.modeloDocumentoFiscalEnum ")
			.leftOuterJoin("inutilizacaonfe.empresa empresa")
			.leftOuterJoin("inutilizacaonfe.configuracaonfe configuracaonfe")
			.where("empresa = ?", filtro.getEmpresa())
			.where("inutilizacaonfe.modeloDocumentoFiscalEnum = ?", filtro.getModeloDocumentoFiscalEnum());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Inutilizacaonfe> query) {
		query
			.leftOuterJoinFetch("inutilizacaonfe.arquivoxmlenvio arquivoxmlenvio")
			.leftOuterJoinFetch("inutilizacaonfe.arquivoxmlretorno arquivoxmlretorno")
			;
	}

	/**
	 * Busca as inutiliza��es para cria��o do XML de envio.
	 *
	 * @param whereIn
	 * @return
	 * @since 01/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Inutilizacaonfe> findForSolicitacao(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Inutiliza��es n�o pode ser nulo.");
		}
		return query()
					.select("inutilizacaonfe.cdinutilizacaonfe, inutilizacaonfe.dtinutilizacao, inutilizacaonfe.numinicio, " +
							"inutilizacaonfe.numfim, inutilizacaonfe.justificativa, inutilizacaonfe.situacao, empresa.cnpj, " +
							"uf.cduf, uf.cdibge, uf.sigla, configuracaonfe.prefixowebservice, empresa.nome, empresa.razaosocial, " +
							"empresa.nomefantasia, configuracaonfe.descricao, " +
							"empresaconfiguracao.serienfe, empresaconfiguracao.nome, empresaconfiguracao.razaosocial, " +
							"empresaconfiguracao.nomefantasia")
					.leftOuterJoin("inutilizacaonfe.empresa empresa")
					.leftOuterJoin("inutilizacaonfe.configuracaonfe configuracaonfe")
					.leftOuterJoin("configuracaonfe.empresa empresaconfiguracao")
					.leftOuterJoin("configuracaonfe.uf uf")
					.whereIn("inutilizacaonfe.cdinutilizacaonfe", whereIn)
					.list();
	}
	
	/**
	 * Busca as inutiliza��es para o envio.
	 *
	 * @return
	 * @since 02/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Inutilizacaonfe> findForEnvio() {
		return query()
					.select("inutilizacaonfe.cdinutilizacaonfe, configuracaonfe.cdconfiguracaonfe, arquivoxmlenvio.cdarquivo")
					.join("inutilizacaonfe.arquivoxmlenvio arquivoxmlenvio")
					.join("inutilizacaonfe.configuracaonfe configuracaonfe")
					.where("inutilizacaonfe.situacao = ?", Inutilizacaosituacao.ENVIANDO)
					.openParentheses()
					.where("inutilizacaonfe.enviando = ?", Boolean.FALSE)
					.or()
					.where("inutilizacaonfe.enviando is null")
					.closeParentheses()
					.list();
	}
	
	/**
	 * Busca as inutiliza��es novas para o emissor.
	 *
	 * @param empresa
	 * @return
	 * @since 14/08/2012
	 * @author Rodrigo Freitas
	 */
	public List<Inutilizacaonfe> findInutilizacoesNovas(Empresa empresa) {
		return querySined()
					
					.select("inutilizacaonfe.cdinutilizacaonfe, configuracaonfe.cdconfiguracaonfe, arquivoxmlenvio.cdarquivo")
					.join("inutilizacaonfe.empresa empresa")
					.join("inutilizacaonfe.arquivoxmlenvio arquivoxmlenvio")
					.join("inutilizacaonfe.configuracaonfe configuracaonfe")
					.where("inutilizacaonfe.situacao = ?", Inutilizacaosituacao.ENVIANDO)
					.where("empresa = ?", empresa)
					.openParentheses()
					.where("inutilizacaonfe.enviando = ?", Boolean.FALSE)
					.or()
					.where("inutilizacaonfe.enviando is null")
					.closeParentheses()
					.list();
	}

	/**
	 * Atualiza o arquivo de envio da inutiliza��o de NF-e
	 *
	 * @param inutilizacaonfe
	 * @since 01/06/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoxmlenvio(Inutilizacaonfe inutilizacaonfe) {
		if(inutilizacaonfe == null || inutilizacaonfe.getCdinutilizacaonfe() == null){
			throw new SinedException("Inutiliza��o n�o pode ser nula.");
		}
		if(inutilizacaonfe.getArquivoxmlenvio() == null || inutilizacaonfe.getArquivoxmlenvio().getCdarquivo() == null){
			throw new SinedException("Arquivo de inutiliza��o n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Inutilizacaonfe i set i.arquivoxmlenvio = ? where i.id = ?", new Object[]{
				inutilizacaonfe.getArquivoxmlenvio(), inutilizacaonfe.getCdinutilizacaonfe()
		});
	}

	/**
	 * Atualiza a situa��o da inutiliza��o de NF-e
	 *
	 * @param inutilizacaonfe
	 * @param inutilizacaosituacao
	 * @since 02/06/2012
	 * @author Rodrigo Freitas
	 */
	public void updateSituacao(Inutilizacaonfe inutilizacaonfe, Inutilizacaosituacao inutilizacaosituacao) {
		if(inutilizacaonfe == null || inutilizacaonfe.getCdinutilizacaonfe() == null){
			throw new SinedException("Inutiliza��o n�o pode ser nula.");
		}
		if(inutilizacaosituacao == null){
			throw new SinedException("Situa��o n�o pode ser nula.");
		}
		getHibernateTemplate().bulkUpdate("update Inutilizacaonfe i set i.situacao = ? where i.id = ?", new Object[]{
				inutilizacaosituacao, inutilizacaonfe.getCdinutilizacaonfe()
		});
	}

	/**
	 * Marca a flag cancelando no Inutilizacaonfe.
	 *
	 * @param bean
	 * @return
	 * @since 02/06/2012
	 * @author Rodrigo Freitas
	 */
	public int marcarInutilizacao(String whereIn) {
		return getHibernateTemplate()
					.bulkUpdate("update Inutilizacaonfe i set i.enviando = ? where i.cdinutilizacaonfe in (" + whereIn + ") and (i.enviando = ? or i.enviando is null)", 
							new Object[]{Boolean.TRUE, Boolean.FALSE});
	}

	/**
	 * Atualiza o arquivo de retorno da inutiliza��o de NF-e
	 *
	 * @param inutilizacaonfe
	 * @since 02/06/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoxmlretorno(Inutilizacaonfe inutilizacaonfe) {
		if(inutilizacaonfe == null || inutilizacaonfe.getCdinutilizacaonfe() == null){
			throw new SinedException("Inutiliza��o n�o pode ser nula.");
		}
		if(inutilizacaonfe.getArquivoxmlretorno() == null || inutilizacaonfe.getArquivoxmlretorno().getCdarquivo() == null){
			throw new SinedException("Arquivo de inutiliza��o n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Inutilizacaonfe i set i.arquivoxmlretorno = ? where i.id = ?", new Object[]{
				inutilizacaonfe.getArquivoxmlretorno(), inutilizacaonfe.getCdinutilizacaonfe()
		});
	}

	/**
	 * Atualiza a data de processamento e protocolo da inutiliza��o de NF-e
	 *
	 * @param inutilizacaonfe
	 * @since 03/06/2012
	 * @author Rodrigo Freitas
	 */
	public void updateProtocoloDtprocessamento(Inutilizacaonfe inutilizacaonfe) {
		if(inutilizacaonfe == null || inutilizacaonfe.getCdinutilizacaonfe() == null){
			throw new SinedException("Inutiliza��o n�o pode ser nula.");
		}
		if(inutilizacaonfe.getDtprocessamento() == null){
			throw new SinedException("Data de processamento n�o pode ser nula.");
		}
		if(inutilizacaonfe.getProtocoloinutilizacao() == null){
			throw new SinedException("Protocolo de inutiliza��o n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Inutilizacaonfe i set i.dtprocessamento = ?, i.protocoloinutilizacao = ? where i.id = ?", new Object[]{
				inutilizacaonfe.getDtprocessamento(), inutilizacaonfe.getProtocoloinutilizacao(), inutilizacaonfe.getCdinutilizacaonfe()
		});
	}

	/**
	 * Atualiza o motivo de erro da inutiliza��o de NF-e
	 *
	 * @param inutilizacaonfe
	 * @since 03/06/2012
	 * @author Rodrigo Freitas
	 */
	public void updateMotivoerro(Inutilizacaonfe inutilizacaonfe) {
		if(inutilizacaonfe == null || inutilizacaonfe.getCdinutilizacaonfe() == null){
			throw new SinedException("Inutiliza��o n�o pode ser nula.");
		}
		if(inutilizacaonfe.getMotivoerro() == null){
			throw new SinedException("MOtiovo de erro n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Inutilizacaonfe i set i.motivoerro = ? where i.id = ?", new Object[]{
				inutilizacaonfe.getMotivoerro(), inutilizacaonfe.getCdinutilizacaonfe()
		});
	}

	/**
	* M�todo que busca as notas inutilizadas para o Sintegra
	*
	* @param filtro
	* @return
	* @since 19/09/2014
	* @author Luiz Fernando
	*/
	public List<Inutilizacaonfe> findForSIntegraRegistro50(SintegraFiltro filtro) {
		if(filtro == null)
			throw new SinedException("Filtro n�o pode ser nulo");
		
		return query()
			.select("inutilizacaonfe.cdinutilizacaonfe, inutilizacaonfe.dtinutilizacao, inutilizacaonfe.numinicio, " +
					"inutilizacaonfe.numfim, inutilizacaonfe.justificativa, inutilizacaonfe.situacao, empresa.cnpj, " +
					"uf.cdibge, configuracaonfe.prefixowebservice, empresa.nome, empresa.razaosocial, empresa.nomefantasia, configuracaonfe.descricao")
			.leftOuterJoin("inutilizacaonfe.empresa empresa")
			.leftOuterJoin("inutilizacaonfe.configuracaonfe configuracaonfe")
			.leftOuterJoin("configuracaonfe.uf uf")
			.where("inutilizacaonfe.dtinutilizacao >= ?", filtro.getDtinicio())
			.where("inutilizacaonfe.dtinutilizacao <= ?", filtro.getDtfim())
			.where("empresa = ?", filtro.getEmpresa())
			.where("inutilizacaonfe.situacao = ?", Inutilizacaosituacao.PROCESSADO_COM_SUCESSO)
			.list();
	}
	
	
	/**
	* M�todo que busca as notas inutilizadas para a tela de Numero Nfe n�o utilizada
	*
	* @param filtro
	* @return
	* @since 12/02/2015
	* @author Jo�o Vitor
	*/
	public List<Inutilizacaonfe> findForNumeroNfeNaoUtilizado(Empresa empresa) {
		if(empresa == null)
			throw new SinedException("Par�metro n�o pode ser nulo");
		
		return query()
			.select("inutilizacaonfe.cdinutilizacaonfe, inutilizacaonfe.numinicio, inutilizacaonfe.numfim")
			.leftOuterJoin("inutilizacaonfe.empresa empresa")
			.where("empresa = ?", empresa)
			.where("inutilizacaonfe.situacao = ?", Inutilizacaosituacao.GERADO)
			.list();
	}
	
	/**
	 * M�todo respons�vel por verificar se nota j� foi inutilizada
	 * @param id
	 * @return
	 * @since 01/03/2016
	 * @author C�sar
	 */
	public List<Inutilizacaonfe> validaInutilizacao(Integer id){
		
		if(id == null)
			throw new SinedException("Par�metro n�o pode ser nulo para pesquisa");
		
		return query()
				.select("cdinutilizacaonfe,dtinutilizacao,numinicio,numfim")
				.where("numinicio <= ?", id)
				.where("numfim >= ?",id)
				.list();
	}
	
	/**
	 * M�todo respons�vel por buscar notas na view Inutilizacao contidas no whereIn
	 * @param whereIn
	 * @return
	 * @since 01/03/2016
	 * @author C�sar
	 */
	@SuppressWarnings("unchecked")
	public List<Inutilizacaonfe> findForInutilizacao(String whereIn){
		
		if(whereIn == null || whereIn.isEmpty())
			throw new SinedException("Par�metro n�o pode ser nulo para pesquisa");
		
		String sql = "SELECT cdinutilizacaonfe,numeronfe,situacao from vinutilizacao where numeronfe in (" + whereIn +" );";	
		SinedUtil.markAsReader();
		List<Inutilizacaonfe> listaInutilizacaonfe = getJdbcTemplate().query(sql, 
		new RowMapper(){			
			public Object mapRow(ResultSet rs, int rowNum)	throws SQLException {
				return new Inutilizacaonfe(rs.getInt("cdinutilizacaonfe"), new Double(rs.getInt("numeronfe")), preencheInutilizacao((rs.getInt("situacao"))));
			}

			private Inutilizacaosituacao preencheInutilizacao(int i) {		
				switch (i) {
				case 0:
					return Inutilizacaosituacao.GERADO;
				case 1:
					return Inutilizacaosituacao.ENVIANDO;
				case 2:
					return Inutilizacaosituacao.PROCESSADO_COM_ERRO;
				case 3:
					return Inutilizacaosituacao.PROCESSADO_COM_SUCESSO;
				}
				return null;
			}
		});
		return listaInutilizacaonfe;
	}
	
	/**
	 * M�todo respons�vel por retorna id's e situa��es para valida��o de exclus�o
	 * @param whereIn
	 * @return
	 * @since 03/03/2016
	 * @author C�sar
	 */
	public List<Inutilizacaonfe> findForExcluirInutilizacao(String whereIn){
		if(whereIn == null || whereIn.isEmpty())
			throw new SinedException("Par�metro inv�lido para consulta");
		
		return query()
				.select("inutilizacaonfe.cdinutilizacaonfe, inutilizacaonfe.situacao")
				.whereIn("inutilizacaonfe.cdinutilizacaonfe", whereIn)
				.list();
	}

	/**
	 * Buscar informa��es da inutiliza��o para integra��o do SAGE
	 * 	 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 21/12/2017
	 */
	public List<Inutilizacaonfe> findForSageFiscal(SagefiscalFiltro filtro) {
		return querySined()
				
				.select("inutilizacaonfe.cdinutilizacaonfe, inutilizacaonfe.dtinutilizacao, inutilizacaonfe.numinicio, " +
						"inutilizacaonfe.numfim, inutilizacaonfe.justificativa, inutilizacaonfe.situacao, uf.sigla")
				.leftOuterJoin("inutilizacaonfe.configuracaonfe configuracaonfe")
				.leftOuterJoin("configuracaonfe.uf uf")
				.where("inutilizacaonfe.dtinutilizacao >= ?", filtro.getDtinicio())
				.where("inutilizacaonfe.dtinutilizacao <= ?", filtro.getDtfim())
				.where("inutilizacaonfe.empresa  = ?", filtro.getEmpresa())
				.where("inutilizacaonfe.situacao = ?", Inutilizacaosituacao.PROCESSADO_COM_SUCESSO)
				.list();
	}

	public List<Inutilizacaonfe> findForCsv(InutilizacaonfeFiltro filtro) {
		return querySined()
				
				.select("inutilizacaonfe.cdinutilizacaonfe, inutilizacaonfe.dtinutilizacao, inutilizacaonfe.numinicio, inutilizacaonfe.numfim, " +
						"empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
						"configuracaonfe.descricao, " +
						"inutilizacaonfe.situacao ")
				.leftOuterJoin("inutilizacaonfe.empresa empresa")
				.leftOuterJoin("inutilizacaonfe.configuracaonfe configuracaonfe")
				.where("inutilizacaonfe.empresa = ?", filtro.getEmpresa())
				.where("inutilizacaonfe.modeloDocumentoFiscalEnum = ?", filtro.getModeloDocumentoFiscalEnum())
				.list();
	}

	public List<Inutilizacaonfe> findForSpedRegC100(SpedarquivoFiltro filtro) {
		return querySined()
				
				.select("inutilizacaonfe.cdinutilizacaonfe, inutilizacaonfe.dtinutilizacao, inutilizacaonfe.numinicio, " +
						"inutilizacaonfe.numfim, inutilizacaonfe.justificativa, inutilizacaonfe.situacao, uf.sigla, inutilizacaonfe.serienfe")
				.leftOuterJoin("inutilizacaonfe.configuracaonfe configuracaonfe")
				.leftOuterJoin("configuracaonfe.uf uf")
				.where("inutilizacaonfe.dtinutilizacao >= ?", filtro.getDtinicio())
				.where("inutilizacaonfe.dtinutilizacao <= ?", filtro.getDtfim())
				.where("inutilizacaonfe.empresa  = ?", filtro.getEmpresa())
				.where("inutilizacaonfe.situacao = ?", Inutilizacaosituacao.PROCESSADO_COM_SUCESSO)
				.list();
	}
}
