package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendapagamento;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.GeracaocontareceberEnum;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VendapagamentoDAO extends GenericDAO<Vendapagamento> {

	/**
	 * M�todo que acha as vendas pagamento
	 * 
	 * @param venda
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Vendapagamento> findVendaPagamentoByVenda(Venda venda) {
		if(venda == null || venda.getCdvenda() == null){
			throw new SinedException("Par�metros inv�lidos!");
		}
		return query()
			.select("vendapagamento.cdvendapagamento, vendapagamento.numero, vendapagamento.valororiginal, vendapagamento.banco, vendapagamento.agencia, vendapagamento.autorizacao, vendapagamento.bandeira, vendapagamento.adquirente, " +
					"vendapagamento.conta, vendapagamento.dataparcela,vendapagamento.valorjuros, documento.cddocumento, documento.dtvencimento, documentotipo.cddocumentotipo, documentotipo.nome, " +
					"prazopagamento.cdprazopagamento, prazopagamento.nome, venda.cdvenda, documentotipo.antecipacao, documentotipo.exibirnavenda, " + 
					"documentoantecipacao.cddocumento, documentoantecipacao.numero, documentoantecipacao.valor, " +
					"aux_documento.cddocumento, aux_documento.valoratual, documento.valor, documentoacao.cddocumentoacao, cheque.cdcheque, vendapagamento.emitente, vendapagamento.cpfcnpj ")
			.join("vendapagamento.venda venda")
			.leftOuterJoin("vendapagamento.documento documento")
			.leftOuterJoin("documento.documentoacao documentoacao")
			.leftOuterJoin("documento.aux_documento aux_documento")
			.leftOuterJoin("vendapagamento.documentoantecipacao documentoantecipacao")
			.leftOuterJoin("vendapagamento.documentotipo documentotipo")
			.leftOuterJoin("venda.prazopagamento prazopagamento")
			.leftOuterJoin("vendapagamento.cheque cheque")
			.where("venda = ?", venda)
			.orderBy("vendapagamento.dataparcela, documento.cddocumento")
			.list();
	}

	/**
	 * M�todo que remove todas os pagamentos da venda
	 * 
	 * @param venda
	 * @author Tom�s Rabelo
	 */
	public void deleteAllFromVenda(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Par�metros inv�lidos.");
		getJdbcTemplate().execute("delete from vendapagamento where cdvenda = "+venda.getCdvenda());
	}

	/**
	 * 
	 * M�todo que faz o update do valor da vendaPagamento
	 *
	 *@author Thiago Augusto
	 *@date 17/02/2012
	 * @param bean
	 */
	public void updateValorVendaPagamento(Vendapagamento bean){
		getHibernateTemplate().bulkUpdate("update vendapagamento vp set vp.valororiginal = " + bean.getValororiginal() + " where vp.cdvendapagamento = " + bean.getCdvendapagamento());
	}

	/**
	 * M�todo que busca as informa��es do cheque da venda
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public Vendapagamento findForChequeByDocumento(Documento documento) {
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("vendapagamento.cdvendapagamento, vendapagamento.banco, vendapagamento.agencia, vendapagamento.agencia, " +
						"vendapagamento.conta, vendapagamento.numero, vendapagamento.dataparcela, vendapagamento.cheque")
				.leftOuterJoin("vendapagamento.venda venda")
				.leftOuterJoin("vendapagamento.documento documento")
				.join("vendapagamento.cheque cheque")
				.where("documento = ?", documento)
				.unique();
	}
	
	/**
	 * Busca uma vendapagamento com situa��o faturada e que fa�a refer�ncia ao documento especificado.
	 * 
	 * @param cdconta
	 * @return
	 * @author Rafael Salvio
	 * @since 16/04/2012
	 */
	public Vendapagamento verificaContaVendaFaturada(Integer cdconta) {
		return query()
					.select("venda.cdvenda")
					.join("vendapagamento.documento documento")
					.join("documento.documentoclasse documentoclasse")
					.join("vendapagamento.venda venda")
					.join("venda.vendasituacao situacao")
					.where("documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.where("documento.cddocumento = ?", cdconta)
					.where("situacao = ?",Vendasituacao.FATURADA)
					.unique();
	}
	

	/**
	 * Busca vendapagamento cuja venda n�o esteja cancelada e cujo documento fa�a refer�ncia ao fornecido.
	 * 
	 * @param cdconta
	 * @return
	 * @author Rafael Salvio
	 * @since 19/04/2012
	 */
	public Vendapagamento findByConta(Integer cdconta) {
		return query()
					.select("venda.cdvenda")
					.join("vendapagamento.documento documento")
					.join("documento.documentoclasse documentoclasse")
					.join("vendapagamento.venda venda")
					.join("venda.vendasituacao situacao")
					.where("documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.where("documento.cddocumento = ?", cdconta)
					.where("situacao != ?",Vendasituacao.CANCELADA)
					.unique();
	}
	
	/**
	 * M�todo que busca a vendapagamento com o cheque relacionado
	 *
	 * @param cheque
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Vendapagamento> findByCheque(Cheque cheque) {
		return query()
					.select("vendapagamento.cdvendapagamento, vendapagamento.dataparcela, venda.cdvenda ")
					.join("vendapagamento.venda venda")
					.join("vendapagamento.cheque cheque")
					.where("cheque = ?", cheque)
					.list();
	}

	/**
	 * M�todo que seta o cdcheque como nulo em vendapagamento
	 *
	 * @param whereIn
	 * @author Luiz Fernando
	 * @since 02/04/2014
	 */
	public void updateReferenciaCheque(String whereIn) {
		if(whereIn != null && !"".equals(whereIn)){
			getJdbcTemplate().update("update vendapagamento set cdcheque = NULL where cdvendapagamento in (" + whereIn + ")");
		}
	}

	/**
	* M�todo que carrega as parcelas da venda para a substituicao de cheque
	*
	* @param whereInCheque
	* @param documentoacao
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public List<Vendapagamento> findForSubstituicaoByCheque(String whereInCheque, Documentoacao... documentoacao) {
		if(StringUtils.isEmpty(whereInCheque) || documentoacao == null || documentoacao.length == 0)
			return new ArrayList<Vendapagamento>();
		
		String whereInSituacao = null;
		StringBuilder values = new StringBuilder();
		for (int i=0; i < documentoacao.length; i++) {
			values.append(documentoacao[i].getCddocumentoacao()).append(",");
		}
		whereInSituacao = values.toString().substring(0, values.toString().length()-1);
		
		return query()
				.select("vendapagamento.cdvendapagamento, venda.cdvenda, documento.cddocumento, cheque.cdcheque")
				.join("vendapagamento.venda venda")
				.join("vendapagamento.documento documento")
				.join("vendapagamento.cheque cheque")
				.whereIn("cheque.cdcheque", whereInCheque)
				.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
				.where("documento.documentoacao in (" + whereInSituacao + ")")
				.list();
	}

	/**
	* M�todo que remove o cheque da parcela da venda
	*
	* @param whereInVendapagamento
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public void updateVendapagamentoRemoveCheque(String whereInVendapagamento) {
		if(StringUtils.isNotEmpty(whereInVendapagamento)){
			getHibernateTemplate().bulkUpdate("update Vendapagamento set cdcheque = null where cdvendapagamento in (" + whereInVendapagamento + ")");
		}
	}

	/**
	* M�todo que atualiza o cheque da parcela da venda
	*
	* @param whereInVendapagamento
	* @param chequeSubstituto
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public void updateVendapagamentoChequeSubstituto(String whereInVendapagamento, Cheque chequeSubstituto) {
		if(StringUtils.isNotEmpty(whereInVendapagamento) && chequeSubstituto != null && chequeSubstituto.getCdcheque() != null){
			getHibernateTemplate().bulkUpdate(					
					"update Vendapagamento set cdcheque = ?, banco = ?, conta = ?, agencia = ?, numero = ? where cdvendapagamento in (" + whereInVendapagamento + ")"
					, new Object[]{chequeSubstituto.getCdcheque(), chequeSubstituto.getBanco(),
							chequeSubstituto.getConta(), chequeSubstituto.getAgencia(), chequeSubstituto.getNumero()}
			);
		}
	}
	
	/**
	* M�todo que verifica se existe documento vinculado a venda
	*
	* @param venda
	* @return
	* @since 20/05/2016
	* @author Luiz Fernando
	*/
	public boolean existeDocumento(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			return false;
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Vendapagamento.class)
			.join("vendapagamento.venda venda")
			.leftOuterJoin("vendapagamento.documento documento")
			.leftOuterJoin("vendapagamento.documentoantecipacao documentoantecipacao")
			.where("venda = ?", venda)
			.openParentheses()
				.where("documentoantecipacao is not null").or()
				.where("documento is not null")
			.closeParentheses()
			.unique() > 0;
	}

	/**
	 * Busca o n�mero de parcela a partir da nota
	 *
	 * @param nf
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/12/2016
	 */
	public Integer countParcelaVendaByNota(Nota nf) {
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Vendapagamento.class)
				.join("vendapagamento.venda venda")
				.join("venda.listaNotavenda listaNotavenda")
				.where("listaNotavenda.nota = ?", nf)
				.unique().intValue();
	}
	
	public void updateValororiginal(String whereInCdvendapagamento, Money valororiginal){
		getJdbcTemplate().update("update vendapagamento set valororiginal=" + valororiginal.toLong() + " where cdvendapagamento in (" + whereInCdvendapagamento + ")");
	}

	public void updateDocumento(Vendapagamento vendapagamento) {
		getJdbcTemplate().update("update vendapagamento set cddocumento = " + vendapagamento.getDocumento().getCddocumento() + " where cdvendapagamento = " + vendapagamento.getCdvendapagamento());
	}
	
	public List<Vendapagamento> buscarPorVendaDoClienteSemContaReceber(Cliente cliente){
		return query()
				.select("vendapagamento.cdvendapagamento,vendapagamento.valororiginal")
				.join("vendapagamento.venda venda")
				.leftOuterJoin("venda.pedidovenda pedidovenda")
				.join("venda.cliente cliente")
				.join("venda.pedidovendatipo pedidovendatipo")
				.join("venda.vendasituacao vendasituacao")
//				.join("pedidovendatipo.geracaocontareceberEnum geracaocontareceberEnum")
				.where("vendapagamento.documento is null")
				.where("vendapagamento.documentoantecipacao is null")
				.where("cliente.cdpessoa = ?", cliente.getCdpessoa())
				.where("pedidovendatipo.bonificacao <> true")
				.where("pedidovendatipo.geracaocontareceberEnum <> ?", GeracaocontareceberEnum.NAO_GERAR_CONTA)
				.openParentheses()
					.where("pedidovenda is null")
					.or()
					.where("not exists (select pvp.cdpedidovendapagamento from Pedidovendapagamento pvp join pvp.documento d join pvp.pedidovenda pv where pv.cdpedidovenda = pedidovenda.cdpedidovenda and d is not null)")
				.closeParentheses()
				.openParentheses()
					.where("vendasituacao = ?", Vendasituacao.REALIZADA)
					.or()
					.where("vendasituacao = ?", Vendasituacao.AGUARDANDO_APROVACAO)
				.closeParentheses()
				.list();
	}

	public void updateInfoCartao(Vendapagamento vendapagamento, String autorizacao, String bandeira, String adquirente) {
		if(vendapagamento == null || vendapagamento.getCdvendapagamento() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		getHibernateTemplate().bulkUpdate("update Vendapagamento v set v.autorizacao = ?, v.bandeira = ?, v.adquirente = ? where v = ?", new Object[]{
			autorizacao, bandeira, adquirente, vendapagamento
		});
	}

	public List<Vendapagamento> findByVendaForIntegracaoTEF(String whereInVenda) {
		if(StringUtils.isBlank(whereInVenda)){
			throw new SinedException("N�o foi encontrada nenhuma venda.");
		}
		return querySined()
					
					.select("vendapagamento.cdvendapagamento, vendapagamento.valororiginal, vendapagamento.dataparcela, vendapagamento.autorizacao, " +
							"vendapagamento.bandeira, vendapagamento.adquirente, " +
							"documentotipo.cddocumentotipo, documentotipo.nome, documentotipo.meiopagamentonfe, documentotipo.tipointegracaonfe," +
							"documentotipo.cnpjcredenciadoranfe, documentotipo.bandeiracartaonfe, " +
							"venda.cdvenda, venda.dtvenda, " +
							"cliente.cdpessoa, cliente.nome")
					.leftOuterJoin("vendapagamento.documentotipo documentotipo")
					.leftOuterJoin("vendapagamento.venda venda")
					.leftOuterJoin("venda.cliente cliente")
					.whereIn("venda.cdvenda", whereInVenda)
					.orderBy("venda.cdvenda, vendapagamento.cdvendapagamento")
					.list();
	}
}
