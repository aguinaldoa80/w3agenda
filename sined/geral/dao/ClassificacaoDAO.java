package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Classificacao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ClassificacaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("classificacao.vclassificacao.identificador")
public class ClassificacaoDAO extends GenericDAO<Classificacao>{

	@Override
	public void updateListagemQuery(QueryBuilder<Classificacao> query, FiltroListagem _filtro) {
		ClassificacaoFiltro filtro = (ClassificacaoFiltro) _filtro;
		query
			.select("classificacao.cdclassificacao, classificacao.nome, classificacao.formato, vclassificacao.nivel, vclassificacao.identificador")
			.join("classificacao.vclassificacao vclassificacao")
			.whereLikeIgnoreAll("classificacao.nome", filtro.getNome())
			.where("vclassificacao.identificador like ?||'%'", filtro.getIdentificador())
			.where("vclassificacao.nivel = ?",filtro.getNivel())
			.orderBy("vclassificacao.identificador");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Classificacao> query) {
		query.leftOuterJoinFetch("classificacao.classificacaosuperior classificacaosuperior")
			 .joinFetch("classificacao.vclassificacao vclassificacao");
	}
	
	public Classificacao carregaClassificacao(Classificacao bean) {
		if (bean == null || bean.getCdclassificacao() == null) {
			throw new SinedException("Classificacao n�o pode ser nula.");
		}
		return query()
					.select("vclassificacao.nivel")
					.join("classificacao.vclassificacao vclassificacao")
					.where("classificacao = ?",bean)
					.unique();
	}
	
	public List<Classificacao> findFilhos(Classificacao pai){
		if (pai == null || pai.getCdclassificacao() == null) {
			throw new SinedException("Classifica��o n�o pode ser nula.");
		}
		return query()
					.select("classificacao.item")
					.join("classificacao.classificacaosuperior pai")
					.where("pai = ?",pai)
					.orderBy("classificacao.item desc")
					.list();
	}
	
	public List<Classificacao> findRaiz(){
		return query()
					.select("classificacao.item")
					.where("classificacaosuperior is null")
					.orderBy("classificacao.item desc")
					.list();
	}
	
	@SuppressWarnings("unchecked")
	public Boolean haveRegister(Classificacao bean){
		if(bean == null || bean.getCdclassificacao() == null){
			throw new SinedException("Classifica��o n�o pode ser nulo.");
		}
		
		List<String> listaString = getJdbcTemplate().query("SELECT 'SELECT C1.CDCLASSIFICACAO FROM CLASSIFICACAO C1 JOIN ' || t.relname || ' ON ' || t.relname || '.' || a.attname || ' = C1.CDCLASSIFICACAO WHERE NOT EXISTS(SELECT C2.CDCLASSIFICACAO FROM CLASSIFICACAO C2 WHERE C2.CDCLASSIFICACAOSUPERIOR = C1.CDCLASSIFICACAO) AND C1.CDCLASSIFICACAO = XX' AS RELACIONAMENTO " +
																			"FROM pg_constraint c " +
																			"LEFT JOIN pg_class t  ON c.conrelid  = t.oid " +
																			"LEFT JOIN pg_class t2 ON c.confrelid = t2.oid " +
																			"LEFT JOIN pg_attribute a ON t.oid = a.attrelid AND a.attnum = c.conkey[1] " +
																			"LEFT JOIN pg_attribute a2 ON t2.oid = a2.attrelid AND a2.attnum = c.confkey[1] " +
																			"WHERE t2.relname = 'classificacao' " +
																			"AND t.relname<>'classificacao' " +
																			"AND c.contype = 'f'" , 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString("RELACIONAMENTO");
			}

		});
		
		String sql = "SELECT COUNT(*) FROM (";
		
		boolean first = false;
		for (String string : listaString) {
			if (first) {
				sql += " UNION ALL ";
			}
			sql += string;
			first = true;
		}
		
		sql += ") QUERY";
		
		sql = sql.replaceAll("XX", bean.getCdclassificacao().toString());
		
		int count = 0;
		
		try {
			count = getJdbcTemplate().queryForInt(sql);
		} catch (DataAccessException e) {
			return false;
		}
		
		return count != 0;
	}
	
	public List<Classificacao> findAutocompleteTodos(String q) {
		return query()
					.select("classificacao.cdclassificacao, classificacao.nome, vclassificacao.identificador, vclassificacao.arvorepai")
					.join("classificacao.vclassificacao vclassificacao")
					.openParentheses()
					.whereLikeIgnoreAll("classificacao.nome", q)
					.or()
					.whereLikeIgnoreAll("vclassificacao.arvorepai", q)
					.or()
					.where("vclassificacao.identificador like ?||'%'", q)
					.closeParentheses()
					.orderBy("vclassificacao.identificador")
					.list();
	}
	
	public List<Classificacao> findAutocompleteFolhas(String q) {
		return query()
					.select("classificacao.cdclassificacao, classificacao.nome, vclassificacao.identificador, vclassificacao.arvorepai")
					.join("classificacao.vclassificacao vclassificacao")
					.openParentheses()
						.whereLikeIgnoreAll("classificacao.nome", q)
						.or()
						.whereLikeIgnoreAll("vclassificacao.arvorepai", q)
						.or()
						.where("vclassificacao.identificador like ?||'%'", q)
					.closeParentheses()
					.where("not exists(select C2.cdclassificacao from Classificacao C2 where C2.classificacaosuperior = classificacao and C2.ativo = true)")
					.orderBy("vclassificacao.identificador")
					.list();
	}
	
	public List<Classificacao> findForTreeView(){
		return query()
			.joinFetch("classificacao.vclassificacao vclassificacao")
			.list();
	}
	
	public Classificacao loadWithIdentificador(Classificacao classificacao) {
		return query()
				.select("classificacao.cdclassificacao, classificacao.nome, vclassificacao.identificador, vclassificacao.arvorepai")
				.join("classificacao.vclassificacao vclassificacao")
				.where("classificacao = ?", classificacao)
				.unique();
	}
	
}
