package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Historicooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipohistoricooperacao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.HistoricooperacaoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class HistoricooperacaoDAO extends GenericDAO<Historicooperacao>{

	@Override
	public void updateListagemQuery(QueryBuilder<Historicooperacao> query,
			FiltroListagem _filtro) {
		HistoricooperacaoFiltro filtro = (HistoricooperacaoFiltro)_filtro;
		query
			.where("historicooperacao.tipohistoricooperacao = ?", filtro.getTipohistoricooperacao());
		super.updateListagemQuery(query, filtro);
	}
	
	public Historicooperacao findByTipo(Tipohistoricooperacao tipo){
		return query()
				.select("historicooperacao.cdhistoricooperacao, historicooperacao.historico, historicooperacao.historicocomplementar, " +
						"historicooperacao.tipohistoricooperacao, historicooperacao.permitircomplementomanual")
				.where("historicooperacao.tipohistoricooperacao = ?", tipo).unique();
	}
	
	public boolean isPossuiTipo(Tipohistoricooperacao tipo){
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.where("historicooperacao.tipohistoricooperacao=?", tipo)
				.unique()>0;
	}
}
