package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Producaoagendamaterialmateriaprima;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoagendamaterialmateriaprimaDAO extends GenericDAO<Producaoagendamaterialmateriaprima>{
	
	/**
	* M�todo que busca a lista de Materia prima alterada ao recalcular a produ��o
	*
	* @param whereIn
	* @return
	* @since 20/02/2015
	* @author Luiz Fernando
	*/
	public List<Producaoagendamaterialmateriaprima> findByProducaoagendamaterial(String whereIn){
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("producaoagendamaterialmateriaprima.cdproducaoagendamaterialmateriaprima, " +
						"producaoagendamaterialmateriaprima.qtdeprevista, producaoagendamaterialmateriaprima.naocalcularqtdeprevista, " +
						"producaoagendamaterial.cdproducaoagendamaterial, producaoagendamaterialmateriaprima.agitacao, producaoagendamaterialmateriaprima.quantidadepercentual, " +
						"producaoagendamaterialmateriaprima.fracaounidademedida, producaoagendamaterialmateriaprima.qtdereferenciaunidademedida, "+
						"material.cdmaterial, material.identificacao, material.nome, " +
						"materialproducao.cdmaterialproducao, " +
						"loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, " +
						"unidademedida.cdunidademedida, unidademedida.nome," +
						"producaoetapanome.cdproducaoetapanome, producaoetapanome.nome ")
				.join("producaoagendamaterialmateriaprima.producaoagendamaterial producaoagendamaterial")
				.join("producaoagendamaterialmateriaprima.material material")
				.leftOuterJoin("producaoagendamaterialmateriaprima.materialproducao materialproducao")
				.leftOuterJoin("producaoagendamaterialmateriaprima.loteestoque loteestoque")
				.leftOuterJoin("producaoagendamaterialmateriaprima.unidademedida unidademedida")
				.leftOuterJoin("producaoagendamaterialmateriaprima.producaoetapanome producaoetapanome")
				.whereIn("producaoagendamaterial.cdproducaoagendamaterial", whereIn)
				.list();
	}
}
