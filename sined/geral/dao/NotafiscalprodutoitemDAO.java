package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.QueryBuilder.Where;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.modulo.fiscal.controller.report.filtro.LivroregistrosaidaFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotafiscalprodutoitemDAO extends GenericDAO<Notafiscalprodutoitem>{

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Notafiscalprodutoitem notafiscalprodutoitem = (Notafiscalprodutoitem)save.getEntity();
		
		if(notafiscalprodutoitem.getListaNotafiscalprodutoitemnotaentrada() != null &&
				notafiscalprodutoitem.getListaNotafiscalprodutoitemnotaentrada().size() > 0){
			save.saveOrUpdateManaged("listaNotafiscalprodutoitemnotaentrada");
		}
		save.saveOrUpdateManaged("listaNotafiscalprodutoitemdi");
	}

	/**
	 * Apaga os itens que n�o est�o na lista ao salvar.
	 *
	 * @param bean
	 * @param whereIn
	 * @since 10/09/2012
	 * @author Rodrigo Freitas
	 */
	public void deleteNaoExistentes(Notafiscalproduto bean, String whereIn) {
		if (bean == null || bean.getCdNota() == null) {
			throw new SinedException("Nota n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("delete from Notafiscalprodutoitem n where n.cdnotafiscalprodutoitem not in ("+whereIn+") and n.cdnotafiscalproduto = " + bean.getCdNota());
	}
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Notafiscalprodutoitem> findForPdfListagem(String whereIn){
		QueryBuilder<Notafiscalprodutoitem> query = querySined()
				
				.select("notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.dtEmissao, notafiscalprodutoitem.valorbruto, " +
						"empresa.cdpessoa, " +
						"notafiscalprodutoitem.valorbcicms, notafiscalprodutoitem.icms, notafiscalprodutoitem.valoricms, " +
						"notafiscalprodutoitem.valorFiscalIcms, notafiscalprodutoitem.valorFiscalIpi, " +
						"notafiscalprodutoitem.valorbcipi, notafiscalprodutoitem.ipi, notafiscalprodutoitem.valoripi, uf.sigla, cfop.codigo")
				.join("notafiscalprodutoitem.notafiscalproduto notafiscalproduto")
				.join("notafiscalproduto.cliente cliente")
				.leftOuterJoin("notafiscalproduto.empresa empresa")
				.leftOuterJoin("cliente.listaEndereco listaEndereco")
				.leftOuterJoin("listaEndereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("notafiscalprodutoitem.cfop cfop");
		
		SinedUtil.quebraWhereIn("notafiscalproduto.cdNota", whereIn, query);
		return query.list();
	}

	/**
	 * 
	 * @param notafiscalproduto
	 * @return
	 */
	public List<Notafiscalprodutoitem> findByNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		return query()
			.select("material.nome, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, notafiscalprodutoitem.cdnotafiscalprodutoitem")
			.join("notafiscalprodutoitem.notafiscalproduto notafiscalproduto")
			.join("notafiscalprodutoitem.material material")
			.leftOuterJoin("notafiscalprodutoitem.localarmazenagem localarmazenagem")
			.where("notafiscalproduto = ?", notafiscalproduto)
			.list();
	}

	/**
	 * Atualiza os dados da tabela Notafiscalprodutoitem, a priore somente o localarmazenagem ser� atualizado.
	 * 
	 * @param cdNota
	 * @param cdnotafiscalprodutoitem
	 * @param localarmazenagem
	 */
	public void atualizaDados(Integer cdNota, Integer cdnotafiscalprodutoitem,Localarmazenagem localarmazenagem) {
		Integer cdlocalarmazenagem = null;
		if(localarmazenagem!=null && localarmazenagem.getCdlocalarmazenagem()!=null)
			cdlocalarmazenagem = localarmazenagem.getCdlocalarmazenagem();
			
		getJdbcTemplate().execute("update notafiscalprodutoitem set cdlocalarmazenagem = "+cdlocalarmazenagem+" "+
				" where cdnotafiscalprodutoitem ="+cdnotafiscalprodutoitem+" and cdnotafiscalproduto = " + cdNota);		
	}

	/**
	 * Busca todos os itens de nota fiscal para montagem do relat�rio de livro de registro de sa�da.
	 * 
	 * @param filtro
	 * @return
	 * @author Giovane Freitas
	 */
	public List<Notafiscalprodutoitem> findForLivroSaida(LivroregistrosaidaFiltro filtro) {

		QueryBuilder<Notafiscalprodutoitem> query = querySined()
				
			.select("notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.dtEmissao, notafiscalprodutoitem.valorbruto, " +
					"notafiscalprodutoitem.valorbcicms, notafiscalprodutoitem.icms, notafiscalprodutoitem.valoricms, " +
					"notafiscalprodutoitem.valorFiscalIcms, notafiscalprodutoitem.valorFiscalIpi, notafiscalprodutoitem.valorfrete, " +
					"notafiscalprodutoitem.valorbcicmsst, notafiscalprodutoitem.icmsst, notafiscalprodutoitem.valoricmsst, " +
					"notafiscalprodutoitem.valorbcipi, notafiscalprodutoitem.ipi, notafiscalprodutoitem.valoripi, " +
					"notafiscalprodutoitem.valordesconto, notafiscalprodutoitem.outrasdespesas, notafiscalprodutoitem.valorseguro, " +
					"notafiscalprodutoitem.valorunitario, notafiscalprodutoitem.qtde, " +
					"uf.sigla, cfop.codigo")
			.join("notafiscalprodutoitem.notafiscalproduto notafiscalproduto")
			.join("notafiscalproduto.cliente cliente")
			.join("notafiscalproduto.notaStatus notaStatus")
			.leftOuterJoin("cliente.listaEndereco listaEndereco")
			.leftOuterJoin("listaEndereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("notafiscalprodutoitem.cfop cfop")
			.where("notafiscalproduto.empresa = ?",filtro.getEmpresa())
			.where("notafiscalproduto.tipooperacaonota = ?", Tipooperacaonota.SAIDA)
			.openParentheses()
				.where("notaStatus = ?", NotaStatus.NFE_EMITIDA)
				.or()
				.where("notaStatus = ?", NotaStatus.NFE_LIQUIDADA)
			.closeParentheses()
			.openParentheses()
				.openParentheses()
					.where("notafiscalproduto.datahorasaidaentrada is not null")
					.where("notafiscalproduto.datahorasaidaentrada >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getDtinicio()))
					.where("notafiscalproduto.datahorasaidaentrada <= ?", SinedDateUtils.dateToTimestampFinalDia(filtro.getDtfim()))
				.closeParentheses()
			.or()
				.openParentheses()
					.where("notafiscalproduto.datahorasaidaentrada is null")
					.where("notafiscalproduto.dtEmissao >= ?", filtro.getDtinicio())
					.where("notafiscalproduto.dtEmissao <= ?", filtro.getDtfim())
				.closeParentheses()
			.closeParentheses();

		return query.list();
	}

	/**
	 * M�todo criado para atualizar o Cfop caso 
	 * o mesmo seja alterado em notas que possua carta de 
	 * corre��o
	 * @param cdnotafiscalprodutoitem
	 * @param cfop
	 */
	public void atualizaCfop(Integer cdnotafiscalprodutoitem, Cfop cfop) {
		getJdbcTemplate().update("update Notafiscalprodutoitem set cdcfop = ? where cdnotafiscalprodutoitem = ?" , new Object[]{cfop.getCdcfop(), cdnotafiscalprodutoitem});
	}
	
	/**
	* M�todo que busca a lista de itens da nota para gerar receita em lote
	*
	* @param whereIn
	* @return
	* @since 04/03/2015
	* @author Luiz Fernando
	*/
	public List<Notafiscalprodutoitem> loadForReceita(String whereIn) {
		return query()
					.select("notafiscalproduto.cdNota, notafiscalproduto.valor, " +
							"notafiscalprodutoitem.cdnotafiscalprodutoitem, notafiscalprodutoitem.valorbruto, " +
							"notafiscalprodutoitem.valorfrete, notafiscalprodutoitem.valordesconto, notafiscalprodutoitem.valorseguro, " +
							"notafiscalprodutoitem.outrasdespesas, notafiscalprodutoitem.valoricmsst, " +
							"notafiscalprodutoitem.valoripi, notafiscalprodutoitem.valorpis, notafiscalprodutoitem.valorcofins, " +
							"notafiscalprodutoitem.incluirvalorprodutos, notafiscalprodutoitem.incluiricmsvalor, " +
							"notafiscalprodutoitem.incluiripivalor, notafiscalprodutoitem.incluirpisvalor, " +
							"notafiscalprodutoitem.incluircofinsvalor, notafiscalprodutoitem.valordescontoincondicionadoservico, " +
							"notafiscalprodutoitem.valorissretido, " +
							"material.cdmaterial, contagerencialvenda.cdcontagerencial, centrocustovenda.cdcentrocusto," +
							"cfop.cdcfop, cfop.naoconsiderarreceita ")
					.join("notafiscalprodutoitem.notafiscalproduto notafiscalproduto")
					.join("notafiscalprodutoitem.material material")
					.leftOuterJoin("material.contagerencialvenda contagerencialvenda")
					.leftOuterJoin("material.centrocustovenda centrocustovenda")
					.leftOuterJoin("notafiscalprodutoitem.cfop cfop")
					.whereIn("notafiscalproduto.cdNota", whereIn)
					.orderBy("notafiscalproduto.cdNota")
					.list();
	}
	
	public Notafiscalprodutoitem loadForPopupVincularNotaEntrada(Notafiscalprodutoitem notafiscalprodutoitem){
		return query()
		.select("notafiscalprodutoitem.cdnotafiscalprodutoitem, notafiscalprodutoitem.ncmcompleto," +
				"notafiscalprodutoitem.qtde, material.cdmaterial")
				.join("notafiscalprodutoitem.material material")
				.where("notafiscalprodutoitem.cdnotafiscalprodutoitem = ?", notafiscalprodutoitem.getCdnotafiscalprodutoitem())
				.unique();
	}

	/**
	 * Atualiza os campos referente ao ICMS Interestadual
	 *
	 * @param notafiscalprodutoitem
	 * @author Rodrigo Freitas
	 * @since 13/06/2016
	 */
	public void updateICMSInterestadual(Notafiscalprodutoitem notafiscalprodutoitem) {
		String sql = "update Notafiscalprodutoitem n set " +
					 "n.dadosicmspartilha = true, " +
					 "n.valorbcdestinatario = " + (notafiscalprodutoitem.getValorbcdestinatario() != null ? SinedUtil.round(notafiscalprodutoitem.getValorbcdestinatario().getValue().doubleValue()*100d, 0) : "null") + ", " +
					 "n.valorbcfcpdestinatario = " + (notafiscalprodutoitem.getValorbcfcpdestinatario() != null ? SinedUtil.round(notafiscalprodutoitem.getValorbcfcpdestinatario().getValue().doubleValue()*100d, 0) : "null") + ", " +
					 "n.fcpdestinatario = " + (notafiscalprodutoitem.getFcpdestinatario() != null ? notafiscalprodutoitem.getFcpdestinatario() : "null") + ", " +
					 "n.icmsdestinatario = " + (notafiscalprodutoitem.getIcmsdestinatario() != null ? notafiscalprodutoitem.getIcmsdestinatario() : "null") + ", " +
					 "n.icmsinterestadual = " + (notafiscalprodutoitem.getIcmsinterestadual() != null ? notafiscalprodutoitem.getIcmsinterestadual() : "null") + ", " +
					 "n.icmsinterestadualpartilha = " + (notafiscalprodutoitem.getIcmsinterestadualpartilha() != null ? notafiscalprodutoitem.getIcmsinterestadualpartilha() : "null") + ", " +
					 "n.valorfcpdestinatario = " + (notafiscalprodutoitem.getValorfcpdestinatario() != null ? SinedUtil.round(notafiscalprodutoitem.getValorfcpdestinatario().getValue().doubleValue()*100d, 0) : "null") + ", " +
					 "n.valoricmsdestinatario = " + (notafiscalprodutoitem.getValoricmsdestinatario() != null ? SinedUtil.round(notafiscalprodutoitem.getValoricmsdestinatario().getValue().doubleValue()*100d, 0) : "null") + ", " +
					 "n.valoricmsremetente = " + (notafiscalprodutoitem.getValoricmsremetente() != null ? SinedUtil.round(notafiscalprodutoitem.getValoricmsremetente().getValue().doubleValue()*100d, 0) : "null") + 
					 "where n.id = " + notafiscalprodutoitem.getCdnotafiscalprodutoitem();
		
		getHibernateTemplate().bulkUpdate(sql);
	}
	
	public List<Notafiscalprodutoitem> findByWhereInCdnota(String whereInCdnota){
		QueryBuilder<Notafiscalprodutoitem> queryBuilder = query();
		queryBuilder
				.select("notafiscalproduto.cdNota,notafiscalproduto.numero," +
						"material.cdmaterial, material.nome, contagerencial.cdcontagerencial, contagerencial.nome," +
						"notafiscalprodutoitem.cdnotafiscalprodutoitem,notafiscalprodutoitem.qtde," +
						"contagerencialvenda.cdcontagerencial, contagerencialvenda.nome," +
						"cliente.cdpessoa,cliente.nome," +
						"localarmazenagem.cdlocalarmazenagem,localarmazenagem.nome," +
						"pedidovendamaterial.cdpedidovendamaterial," +
						"pedidovenda.cdpedidovenda," +
						"endereco.cdendereco," +
						"unidademedida.cdunidademedida,unidademedida.nome,unidademedida.simbolo")
				.join("notafiscalprodutoitem.notafiscalproduto notafiscalproduto")
				.join("notafiscalprodutoitem.material material")
				.leftOuterJoin("notafiscalprodutoitem.pedidovendamaterial pedidovendamaterial")
				.leftOuterJoin("pedidovendamaterial.pedidovenda pedidovenda")
				.leftOuterJoin("pedidovenda.endereco endereco")
				.leftOuterJoin("notafiscalproduto.cliente cliente")
				.leftOuterJoin("notafiscalprodutoitem.unidademedida unidademedida")
				.leftOuterJoin("notafiscalprodutoitem.localarmazenagem localarmazenagem")
				.leftOuterJoin("notafiscalprodutoitem.pneu pneu")
				.leftOuterJoin("material.contagerencial contagerencial")
				.leftOuterJoin("material.contagerencialvenda contagerencialvenda")
				.orderBy("notafiscalproduto.cdNota,notafiscalprodutoitem.cdnotafiscalprodutoitem");
					
		SinedUtil.quebraWhereIn("notafiscalproduto.cdNota", whereInCdnota, queryBuilder);
		queryBuilder = SinedUtil.setJoinsByComponentePneu(queryBuilder);
		
		return queryBuilder.list();
	}

	public List<Notafiscalprodutoitem> findForRateioFaturamento(Notafiscalproduto notafiscalproduto) {
		if(notafiscalproduto == null || notafiscalproduto.getCdNota() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("notafiscalprodutoitem.cdnotafiscalprodutoitem, notafiscalprodutoitem.valorbruto, " +
						"notafiscalproduto.cdNota, empresa.cdpessoa, contagerencialEmpresa.cdcontagerencial, centrocustoEmpresa.cdcentrocusto, " +
						"vendamaterial.cdvendamaterial, venda.cdvenda, centrocusto.cdcentrocusto, " +
						"material.cdmaterial, contagerencialvenda.cdcontagerencial, centrocustovenda.cdcentrocusto ")
				.join("notafiscalprodutoitem.notafiscalproduto notafiscalproduto")
				.join("notafiscalproduto.empresa empresa")
				.join("notafiscalprodutoitem.material material")
				.leftOuterJoin("notafiscalprodutoitem.vendamaterial vendamaterial")
				.leftOuterJoin("vendamaterial.venda venda")
				.leftOuterJoin("empresa.contagerencial contagerencialEmpresa")
				.leftOuterJoin("empresa.centrocusto centrocustoEmpresa")
				.leftOuterJoin("material.contagerencialvenda contagerencialvenda")
				.leftOuterJoin("venda.centrocusto centrocusto")
				.leftOuterJoin("material.centrocustovenda centrocustovenda")
				.where("notafiscalproduto = ?", notafiscalproduto)
				.list();
	}
	
	public Notafiscalprodutoitem findByVendaMaterial (Vendamaterial vendaMaterial){
		if(vendaMaterial == null || vendaMaterial.getCdvendamaterial() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("notafiscalprodutoitem.cdnotafiscalprodutoitem, vendamaterial.cdvendamaterial")
				.leftOuterJoin("notafiscalprodutoitem.vendamaterial vendamaterial")
				.where("vendamaterial =?", vendaMaterial)
				.unique();
	}
	
	public Notafiscalprodutoitem findByMaterialNotaFiscal (Notafiscalproduto nota, Material material){
		return query()
				.select("notafiscalprodutoitem.valorbcpis,notafiscalprodutoitem.pis,notafiscalprodutoitem.valorpis, notafiscalprodutoitem.valorbccofins,notafiscalprodutoitem.cofins,notafiscalprodutoitem.valorcofins, notafiscalprodutoitem.qtde")
				.where("notafiscalprodutoitem.notafiscalproduto=?",nota)
				.where("notafiscalprodutoitem.material = ?", material)
				.unique();
	}
}