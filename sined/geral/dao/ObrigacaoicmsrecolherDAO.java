package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Obrigacaoicmsrecolher;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.ObrigacaoicmsrecolherFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ObrigacaoicmsrecolherDAO extends GenericDAO<Obrigacaoicmsrecolher> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Obrigacaoicmsrecolher> query,FiltroListagem _filtro) {
		ObrigacaoicmsrecolherFiltro filtro = (ObrigacaoicmsrecolherFiltro) _filtro;
		
		query
			.select("codobrigacaoicmsrecolher.cdcodobrigacaoicmsrecolher, codobrigacaoicmsrecolher.codigo," +
					"codobrigacaoicmsrecolher.nome, codobrigacaoicmsrecolher.data, " +
					"obrigacaoicmsrecolher.cdobrigacaoicmsrecolher, obrigacaoicmsrecolher.valorobrigacao, " +
					"obrigacaoicmsrecolher.dtvencimento, obrigacaoicmsrecolher.mesreferencia," +
					"empresa.cdpessoa, empresa.nome, empresa.nomefantasia, uf.cduf, uf.sigla ")
			.leftOuterJoin("obrigacaoicmsrecolher.empresa empresa")
			.leftOuterJoin("obrigacaoicmsrecolher.uf uf")
			.leftOuterJoin("obrigacaoicmsrecolher.codobrigacaoicmsrecolher codobrigacaoicmsrecolher")
			.where("empresa = ?", filtro.getEmpresa())
			.where("uf = ?", filtro.getUf())
			.where("codobrigacaoicmsrecolher = ?", filtro.getCodobrigacaoicmsrecolher())
			.where("obrigacaoicmsrecolher.valorobrigacao >= ?", filtro.getValorobrigacaoInicio())
			.where("obrigacaoicmsrecolher.valorobrigacao <= ?", filtro.getValorobrigacaoFim())
			.where("obrigacaoicmsrecolher.dtvencimento >= ?", filtro.getDtvencimentoInicio())
			.where("obrigacaoicmsrecolher.dtvencimento <= ?", filtro.getDtvencimentoFim())
			.where("obrigacaoicmsrecolher.codigoreceita = ?", filtro.getCodigoreceita())
			.where("obrigacaoicmsrecolher.numeroprocesso = ?", filtro.getNumeroprocesso())
			.where("obrigacaoicmsrecolher.mesreferencia = ?", filtro.getMesreferencia())
		;
	}

	public List<Obrigacaoicmsrecolher> findForRegistroE116(SpedarquivoFiltro filtro, String siglaUf) {
		return query()
				.leftOuterJoinFetch("obrigacaoicmsrecolher.codobrigacaoicmsrecolher codobrigacaoicmsrecolher")
				.leftOuterJoinFetch("obrigacaoicmsrecolher.origemprocessosped origemprocessosped")
				.where("obrigacaoicmsrecolher.mesreferencia = ?", filtro.getMesanoAux())
				.where("obrigacaoicmsrecolher.uf.sigla = ?", siglaUf)
				.where("codobrigacaoicmsrecolher.codigo in ('000', '003', '004', '005', '006', '090')")
				.list();
	}
	
	public List<Obrigacaoicmsrecolher> findForRegistroE316(Empresa empresa, String siglaUf, Date data) {
		return query()
				.select("obrigacaoicmsrecolher.numeroprocesso, obrigacaoicmsrecolher.cdobrigacaoicmsrecolher, obrigacaoicmsrecolher.valorobrigacao, " +
						"obrigacaoicmsrecolher.dtvencimento, obrigacaoicmsrecolher.mesreferencia, " +
						"codobrigacaoicmsrecolher.cdcodobrigacaoicmsrecolher, codobrigacaoicmsrecolher.codigo," +
						"codobrigacaoicmsrecolher.nome, codobrigacaoicmsrecolher.data, obrigacaoicmsrecolher.codigoreceita, " +
						"origemprocessosped.cdorigemprocessosped, origemprocessosped.codigo, " +
						"obrigacaoicmsrecolher.textocomplementar ")
				.leftOuterJoin("obrigacaoicmsrecolher.codobrigacaoicmsrecolher codobrigacaoicmsrecolher")
				.leftOuterJoin("obrigacaoicmsrecolher.origemprocessosped origemprocessosped")
				.where("obrigacaoicmsrecolher.empresa = ?", empresa)
				.where("obrigacaoicmsrecolher.uf.sigla = ?", siglaUf)
				.where("codobrigacaoicmsrecolher.codigo in ('000', '003', '006', '090')")
				.where("obrigacaoicmsrecolher.mesreferencia = ?", data != null ? new SimpleDateFormat("MM/yyyy").format(data) : null)
				.list();
	}

	public List<Obrigacaoicmsrecolher> findForRegistroE250(Empresa empresa, Uf uf, Date data) {
		return query()
				.select("obrigacaoicmsrecolher.numeroprocesso, obrigacaoicmsrecolher.cdobrigacaoicmsrecolher, obrigacaoicmsrecolher.valorobrigacao, " +
						"obrigacaoicmsrecolher.dtvencimento, obrigacaoicmsrecolher.mesreferencia, " +
						"codobrigacaoicmsrecolher.cdcodobrigacaoicmsrecolher, codobrigacaoicmsrecolher.codigo," +
						"codobrigacaoicmsrecolher.nome, codobrigacaoicmsrecolher.data, obrigacaoicmsrecolher.codigoreceita, " +
						"origemprocessosped.cdorigemprocessosped, origemprocessosped.codigo, " +
						"obrigacaoicmsrecolher.textocomplementar ")
				.leftOuterJoin("obrigacaoicmsrecolher.codobrigacaoicmsrecolher codobrigacaoicmsrecolher")
				.leftOuterJoin("obrigacaoicmsrecolher.origemprocessosped origemprocessosped")
				.where("obrigacaoicmsrecolher.empresa = ?", empresa)
				.where("obrigacaoicmsrecolher.uf = ?", uf)
				.where("codobrigacaoicmsrecolher.codigo in ('001', '002', '006', '999')")
				.where("obrigacaoicmsrecolher.mesreferencia = ?", data != null ? new SimpleDateFormat("MM/yyyy").format(data) : null)
				.list();
	}

	@Override
	public ListagemResult<Obrigacaoicmsrecolher> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Obrigacaoicmsrecolher> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("obrigacaoicmsrecolher.cdobrigacaoicmsrecolher");
		return new ListagemResult<Obrigacaoicmsrecolher>(query, filtro);
	}
}
