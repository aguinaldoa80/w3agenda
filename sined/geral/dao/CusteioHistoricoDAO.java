package br.com.linkcom.sined.geral.dao;


import java.util.List;

import br.com.linkcom.sined.geral.bean.CusteioHistorico;
import br.com.linkcom.sined.geral.bean.CusteioProcessado;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CusteioHistoricoDAO  extends GenericDAO<CusteioHistorico> {

	
	
	public List<CusteioHistorico> findByCusteioProcessado(CusteioProcessado bean) {
		return query()
				.select("custeioHistorico.cdcusteiohistorico,custeioHistorico.situacaoCusteio,custeioHistorico.observacao," +
						"custeioHistorico.dtaltera,custeioHistorico.cdusuarioaltera," +
						"custeioProcessado.cdCusteioProcessado")
				.leftOuterJoin("custeioHistorico.custeioProcessado custeioProcessado")
				.where("custeioHistorico.custeioProcessado = ?",bean)
				.orderBy("custeioHistorico.dtaltera desc")
				.list();
	}

}
