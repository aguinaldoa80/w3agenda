package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.ClienteEmpresa;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Materialtabelapreco;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PessoaContatoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ClienteFiltro;
import br.com.linkcom.sined.modulo.crm.controller.process.bean.ControleInteracaoBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.ClienteCompletoReportBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ClienteVO;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.DominiointegracaoFiltro;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.VerificaPendenciaFinanceira;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;
import br.com.linkcom.sined.util.rest.android.cliente.ClienteRESTWSBean;


@DefaultOrderBy("cliente.nome")
public class ClienteDAO extends GenericDAO<Cliente>{
 
	/* singleton */
	private static ClienteDAO instance;
	public static ClienteDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ClienteDAO.class);
		}
		return instance;
	}

	private ArquivoDAO arquivoDAO;
	private EmpresaService empresaService;
	private PessoaContatoService pessoaContatoService;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setPessoaContatoService(PessoaContatoService pessoaContatoService) {
		this.pessoaContatoService = pessoaContatoService;
	}
	
	/**
	* M�todo que adiciona a condi��o de restri��o de cliente por empresa
	*
	* @param query
	* @param whereInEmpresa
	* @since 04/11/2016
	* @author Luiz Fernando
	*/
	private void addWhereInClienteByEmpresa(QueryBuilder<Cliente> query, String whereInEmpresa){
		if(SinedUtil.isRestricaoEmpresaClienteUsuarioLogado() && StringUtils.isNotBlank(whereInEmpresa)){
			query
				.leftOuterJoin("cliente.listaClienteEmpresa listaClienteEmpresa")
				.leftOuterJoin("listaClienteEmpresa.empresa empresa")
				.openParentheses()
					.where("empresa is null")
					.or()
					.whereIn("empresa.cdpessoa", whereInEmpresa)
				.closeParentheses();
		}
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Cliente> query,FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		}
		ClienteFiltro filtro = (ClienteFiltro)_filtro;

		query
			.select("distinct new br.com.linkcom.sined.geral.bean.Cliente(cliente.cdpessoa, cliente.nome, cliente.tipopessoa, cliente.ativo, cliente.email, cliente.identificador, coalesce(cliente.identificadorordenacao, cliente.cdpessoa))")
			.setUseTranslator(false)
			.where("cliente.tipopessoa = ?", filtro.getTipopessoa())
			.where("cliente.cnpj LIKE ''||?||'%'", filtro.getNumeromatriz())
			.where("cliente.cnpj = ?",filtro.getCnpj())
			.where("cliente.cpf = ?",filtro.getCpf())	
			.where("cliente.identificador = ?",filtro.getIdentificador())	
			.where("cliente.ativo = ?",filtro.getAtivo())
			.where("date_part('month', cliente.dtnascimento) >= ?",filtro.getNascimentoDe() != null ? filtro.getNascimentoDe().ordinal()+1 : null)
			.where("date_part('month', cliente.dtnascimento) <= ?",filtro.getNascimentoAte() != null ? filtro.getNascimentoAte().ordinal()+1 : null)
			.where("cliente.dtinsercao >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getDtinicioinsercao()))
			.where("cliente.dtinsercao <= ?", SinedDateUtils.dateToTimestampFinalDia(filtro.getDtfiminsercao()))
			.whereLikeIgnoreAll("cliente.nome", filtro.getNomefantasia())
			.whereLikeIgnoreAll("cliente.razaosocial", filtro.getRazaosocial());
		
		if(filtro.getCodigo()!=null){
			query
				.openParentheses()
					.where("cliente.identificador = ?", filtro.getCodigo().toString())	
					.or()
					.where("cliente.cdpessoa =?", filtro.getCodigo())
				.closeParentheses();
		}
		
		if(filtro.getClienteoutro() != null || filtro.getClienterelacaotipo() != null) {
			query.leftOuterJoin("cliente.listaClienterelacao listaClienterelacao")
				 .ignoreJoin("listaClienterelacao");
			if(filtro.getClienterelacaotipo() != null){
				query.leftOuterJoin("listaClienterelacao.clienterelacaotipo clienterelacaotipo")
					 .where("clienterelacaotipo = ?", filtro.getClienterelacaotipo())
					 .ignoreJoin("clienterelacaotipo");
			}
			if(filtro.getClienteoutro() != null){
				query
					.leftOuterJoin("listaClienterelacao.clienteoutro clienteoutro")
					.where("clienteoutro = ?", filtro.getClienteoutro())
					.ignoreJoin("clienteoutro");
			}
		}
		
		if(!StringUtils.isEmpty(filtro.getContato())){
			query
				.leftOuterJoin("cliente.listaContato listaContato")
				.leftOuterJoin("listaContato.contato contato")
				.whereLikeIgnoreAll("contato.nome", filtro.getContato())
				.ignoreJoin("contato");
		}
		
		if(!StringUtils.isEmpty(filtro.getTelefone())){
			query
				.leftOuterJoin("cliente.listaTelefone telefone")
				.whereLikeIgnoreAll("telefone.telefone",filtro.getTelefone())
				.ignoreJoin("telefone");
		}
		
		if(filtro.getClienteprofissao() != null){
			query
				.leftOuterJoin("cliente.clienteprofissao clienteprofissao")
				.where("clienteprofissao = ?",filtro.getClienteprofissao());
		}
		
		if(!StringUtils.isEmpty(filtro.getLogradouro()) || 
				!StringUtils.isEmpty(filtro.getBairro()) || 
				!StringUtils.isEmpty(filtro.getNumero()) || 
				filtro.getPais() != null || 
				filtro.getMunicipio() != null || 
				filtro.getUf() != null){
			query
				.leftOuterJoin("cliente.listaEndereco listaEndereco")
				.leftOuterJoin("listaEndereco.municipio municipio")
				.whereLikeIgnoreAll("listaEndereco.logradouro",filtro.getLogradouro())
				.whereLikeIgnoreAll("listaEndereco.bairro",filtro.getBairro())
				.where("listaEndereco.numero = ?",filtro.getNumero())
				.where("listaEndereco.pais = ?",filtro.getPais())
				.where("municipio = ?",filtro.getMunicipio())
				.where("municipio.uf = ?",filtro.getUf())
				.ignoreJoin("listaEndereco", "municipio");
		}
		
		
		if (filtro.getCategoria() != null && filtro.getCategoria().getCdcategoria() != null){
			Categoria categoriaAux = filtro.getCategoria();
			if (categoriaAux.getVcategoria() == null || categoriaAux.getVcategoria().getIdentificador() == null){
				categoriaAux = CategoriaService.getInstance().loadWithIdentificador(categoriaAux);
			}
			
			query
				.leftOuterJoin("cliente.listaPessoacategoria listaPessoacategoria")
				.leftOuterJoin("listaPessoacategoria.categoria categoria")
				.leftOuterJoin("categoria.vcategoria vcategoria")
				.where("vcategoria.identificador like ?||'%'", categoriaAux.getVcategoria().getIdentificador())
				.ignoreJoin("listaPessoacategoria", "categoria", "vcategoria");
		}
		
		if (StringUtils.isEmpty(filtro.getOrderBy())) {
			query.orderBy("cliente.nome");
		}
		
		if(filtro.getClientevendedor() != null || filtro.getAgencia()!= null || (filtro.getLimitaracessoclientevendedor() != null && filtro.getLimitaracessoclientevendedor())){ 
			query
			.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
			.leftOuterJoin("listaClientevendedor.colaborador colaboradorvendedor")
			.leftOuterJoin("listaClientevendedor.agencia agencia");

			if(filtro.getLimitaracessoclientevendedor() != null && filtro.getLimitaracessoclientevendedor()){
				query
				.openParentheses()
				.where("colaboradorvendedor.cdpessoa = ?", SinedUtil.getUsuarioLogado().getCdpessoa())
				.or()
				.where("agencia.cdpessoa = ?", SinedUtil.getUsuarioLogado().getCdpessoa())
				.closeParentheses();
			} else {
				query
				.openParentheses()
				.where("colaboradorvendedor = ?", filtro.getClientevendedor())
				.or()
				.where("agencia = ?",filtro.getAgencia())
				.closeParentheses();
			}
			query
			.ignoreJoin("listaClientevendedor", "colaboradorvendedor", "agencia");
		}
		
		if(filtro.getDtiniciovalidade()!=null || filtro.getDtfimvalidade()!=null){
			query
				.leftOuterJoin("cliente.listaClientearquivo listaClientearquivo")
				.where("listaClientearquivo.validade >= ?", filtro.getDtiniciovalidade())
				.where("listaClientearquivo.validade <= ?", filtro.getDtfimvalidade());
		}
		
		if(filtro.getSegmento()!=null){
			query.leftOuterJoin("cliente.listaClienteSegmento clienteSegmento")
			.where("clienteSegmento.segmento = ?", filtro.getSegmento());
		}
		
		if (SinedUtil.isRestricaoEmpresaClienteUsuarioLogado()){
			String whereInCdempresa = new SinedUtil().getListaEmpresa();
			if (whereInCdempresa!=null && !whereInCdempresa.trim().equals("")){
				query.leftOuterJoin("cliente.listaClienteEmpresa listaClienteEmpresa");
				query.openParentheses()
					.whereIn("listaClienteEmpresa.empresa.cdpessoa", whereInCdempresa)
					.or()
					.where("listaClienteEmpresa is null")
					.closeParentheses();
			}
		}
		
		if (filtro.getAssociacao()!=null){
			query.where("cliente.associacao=?", filtro.getAssociacao());
		}
		
		if (filtro.getEspecialidade()!=null){
			query.joinIfNotExists("cliente.listaClienteespecialidade listaClienteespecialidade");
			query.where("listaClienteespecialidade.especialidade=?", filtro.getEspecialidade());
		}
		
		if (filtro.getConselhoclasseprofissional()!=null){
			query.joinIfNotExists("cliente.listaClienteespecialidade listaClienteespecialidade");
			query.where("listaClienteespecialidade.especialidade.conselhoclasseprofissional=?", filtro.getConselhoclasseprofissional());
		}
		
		if (filtro.getRegistro()!=null){
			query.joinIfNotExists("cliente.listaClienteespecialidade listaClienteespecialidade");
			query.whereLikeIgnoreAll("listaClienteespecialidade.registro", filtro.getRegistro());
		}
	}
	
	
	/**
	 * Carrega informa��es para a listagem de cliente.
	 *
	 * @param whereIn
	 * @param orderby
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cliente> findListagem(String whereIn, String orderby, boolean asc){
		if (whereIn == null || whereIn.equals("")) {
			return new ArrayList<Cliente>();
		}
		QueryBuilder<Cliente> query = querySined()
									.setUseWhereClienteEmpresa(true)
									.select("cliente.cdpessoa,cliente.cnpj, cliente.cpf ,cliente.nome, " +
											"cliente.ativo,cliente.tipopessoa, categoria.nome, cliente.email," +
											"cliente.razaosocial, cliente.identificador")
									.leftOuterJoin("cliente.listaPessoacategoria listaPessoacategoria")
									.leftOuterJoin("listaPessoacategoria.categoria categoria")
									.whereIn("cliente.cdpessoa", whereIn);
		
		if (!StringUtils.isEmpty(orderby)) {
			query.orderBy(orderby+" "+(asc?"ASC":"DESC") + ", categoria.nome");
		} else {
			query.orderBy("cliente.nome, categoria.nome");
		}
		
		return query.list();
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Cliente> query) {
		((QueryBuilderSined<Cliente>) query).setUseWhereClienteEmpresa(true);
		
		query.leftOuterJoinFetch("cliente.listaDadobancario dadobancario")
			 .leftOuterJoinFetch("dadobancario.banco banco")
			 .leftOuterJoinFetch("cliente.listaEndereco listaEndereco")
			 .leftOuterJoinFetch("listaEndereco.municipio municipioEnd")
			 .leftOuterJoinFetch("municipioEnd.uf ufEnd")
			 .leftOuterJoinFetch("cliente.municipionaturalidade municipionaturalidade")
			 .leftOuterJoinFetch("municipionaturalidade.uf ufnaturalidade")
			 .leftOuterJoinFetch("cliente.sexo sexo")
			 .leftOuterJoinFetch("cliente.responsavelFinanceiro responsavelFinanceiro")		
			 .leftOuterJoinFetch("cliente.estadocivil estadocivil")		
			 .leftOuterJoinFetch("cliente.listaTelefone listaTelefone")
			 .leftOuterJoinFetch("cliente.listaRestricao listaRestricao")
			 .leftOuterJoinFetch("cliente.logocliente logocliente")
			 .leftOuterJoinFetch("cliente.listaClienteprocesso listaClienteprocesso")		
			 .leftOuterJoinFetch("cliente.listaPessoacategoria listaPessoacategoria")
			 .leftOuterJoinFetch("cliente.listaClienterelacao listaClienterelacao")			
			 .leftOuterJoinFetch("listaClienterelacao.clienteoutro clienteoutro")			
			 .leftOuterJoinFetch("listaPessoacategoria.categoria categoria")
			 .leftOuterJoinFetch("categoria.vcategoria vcategoria")
			 .leftOuterJoinFetch("cliente.contaContabil contacontabil")
			 .leftOuterJoinFetch("cliente.codigotributacao codigotributacao")
			 .leftOuterJoinFetch("cliente.operacaocontabil operacaocontabil")
			 .leftOuterJoinFetch("cliente.listaClientearquivo listaClientearquivo")
			 .leftOuterJoinFetch("listaClientearquivo.arquivo arquivo")
			 .leftOuterJoinFetch("cliente.listaClienteSegmento listaClienteSegmento")
			 .leftOuterJoinFetch("cliente.listaQuestionario listaQuestionario")
			 .leftOuterJoinFetch("cliente.listaClienteLicencaSipeagro listaClienteLicencaSipeagro")
			 .leftOuterJoinFetch("cliente.contaContabil contaContabil")
			 .orderBy("listaRestricao.dtrestricao");
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				
				Cliente cliente = (Cliente) save.getEntity();
				
				if(cliente.getLogocliente() != null){
					arquivoDAO.saveFile(cliente, "logocliente");
				}
				
				save.saveOrUpdateManaged("listaPessoacategoria");
				save.saveOrUpdateManaged("listaDadobancario");
				save.saveOrUpdateManaged("listaClientearquivo");
				if(cliente.getListaEndereco() != null){
					save.saveOrUpdateManaged("listaEndereco");
				}
				save.saveOrUpdateManaged("listaTelefone");
				save.saveOrUpdateManaged("listaRestricao");
				save.saveOrUpdateManaged("listaClienteprocesso");
				save.saveOrUpdateManaged("listaClientevendedor");
				save.saveOrUpdateManaged("listaClienterelacao", "cliente");
				save.saveOrUpdateManaged("listaValecompra", "cliente");
				save.saveOrUpdateManaged("listDocumentoTipo");
				save.saveOrUpdateManaged("listPrazoPagamento");
				save.saveOrUpdateManaged("listaClienteSegmento");
				save.saveOrUpdateManaged("listaClienteespecialidade");
				save.saveOrUpdateManaged("listaClienteLicencaSipeagro");
				
				if (SinedUtil.isRestricaoEmpresaClienteUsuarioLogado()){
					List<ClienteEmpresa> listaClienteEmpresaNaoAssociada = cliente.getListaClienteEmpresaNaoAssociada();
					
					if (listaClienteEmpresaNaoAssociada!=null && !listaClienteEmpresaNaoAssociada.isEmpty()){
						List<ClienteEmpresa> listaClienteEmpresa = cliente.getListaClienteEmpresa();
						if (listaClienteEmpresa==null){
							//listaClienteEmpresa = new ArrayList<ClienteEmpresa>(listaClienteEmpresaNaoAssociada);
							cliente.setListaClienteEmpresa(listaClienteEmpresaNaoAssociada);
						}else {
							listaClienteEmpresa.addAll(listaClienteEmpresaNaoAssociada);
						}
					}
					
					save.saveOrUpdateManaged("listaClienteEmpresa");
				}else {
					save.saveOrUpdateManaged("listaClienteEmpresa");
				}
				return null;
			}
		});
	}
	
	@Override
	public void saveOrUpdate(final Cliente cliente) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				List<PessoaContato> listaPessoaContato = cliente.getListaContato();
		
				saveOrUpdateNoUseTransaction(cliente);
				
				try{
					pessoaContatoService.saveOrUpdateListaPessoaContato(cliente, listaPessoaContato);
				} catch (SinedException e) {
					throw e;
				} catch (DataAccessException da) {
					throw new SinedException("Contato n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
				}
				return null;
			}
		});
	}

	/**
	 * Deleta somente o registro na tabela cliente. Conserva o da tabela pessoa.
	 * Usado para excluir um cliente que tamb�m � fornecedor.
	 *
	 * @param cliente
	 * @author Flavio
	 */
	public void deleteOnlyCliente(Cliente cliente){
		if(cliente==null){
			throw new SinedException("O par�metro cliente n�o pode ser null.");
		}
		String delete = "delete from cliente where cdpessoa = ?";
		Object[] campo = new Object[]{cliente.getCdpessoa()};
		getHibernateTemplate().update(delete, campo);
		getHibernateTemplate().flush();
	}

	/**
	 * M�todo para obter o n�mero de clientes cadastrados com o cnpj.
	 *
	 * @param bean
	 * @return
	 * @author Flavio Tavares
	 */
	public Integer countClienteCnpj(Cliente bean){
		if(bean==null){
			throw new SinedException("O par�metro bean n�o pode ser null.");
		}
		SinedUtil.markAsReader();
		return newQueryBuilderWithFrom(Integer.class)
			.select("count(*)")
			.where("cnpj=?",bean.getCnpj())
			.where("cdpessoa<>?",bean.getCdpessoa())
			.unique();
	}
	
	/**
	 * Metodo para obter lista de Clientes
	 * @param filtro
	 * @return
	 * @author Andre Brunelli
	 */
	public List<Cliente> findForReport(ClienteFiltro filtro) {
		if(filtro==null){
			throw new SinedException("A propriedade n�o pode ser nula");
		}
				
		QueryBuilder<Cliente> query = querySined();
		this.updateListagemQuery(query, filtro);
		
		if(filtro.getCategoria() == null){
			query
				.leftOuterJoin("cliente.listaPessoacategoria listaPessoacategoria")
				.leftOuterJoin("listaPessoacategoria.categoria categoria");
		}
		
		if(StringUtils.isEmpty(filtro.getTelefone())){
			query
				.leftOuterJoin("cliente.listaTelefone telefone");
		}
		
		if(StringUtils.isEmpty(filtro.getLogradouro()) &&
				StringUtils.isEmpty(filtro.getBairro()) &&
				StringUtils.isEmpty(filtro.getNumero()) &&
				filtro.getPais() == null &&
				filtro.getMunicipio() == null &&
				filtro.getUf() == null){
			query
				.leftOuterJoin("cliente.listaEndereco listaEndereco")
				.leftOuterJoin("listaEndereco.municipio municipio");
		}
		
		query.setIgnoreJoinPaths(new ListSet<String>(String.class));
		query
			.select("cliente.nome, cliente.cdpessoa, cliente.cnpj, cliente.razaosocial, cliente.cpf, cliente.ativo, " +
					"categoria.nome , telefone.telefone, telefonetipo.nome, cliente.tipopessoa, cliente.email, cliente.identificador, " +
					"listaEndereco.logradouro, listaEndereco.numero, listaEndereco.complemento, listaEndereco.bairro, municipio.cdmunicipio, " +
					"municipio.nome, listaEndereco.cep, cliente.dtnascimento, cliente.rg, uf.cduf, uf.sigla")
			.setUseTranslator(true)
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("telefone.telefonetipo telefonetipo")
			.orderBy("cliente.nome");
		
		
		
		return query.list();
	}
	
	/**
	 * Carrega a lista de propostas a partir de um whereIn passado por par�metro
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cliente> findForPropostaReport(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("String para where in n�o pode ser vazia nem nula.");
		}
		return querySined()
					
					.select("cliente.nome, listaProposta.cdproposta, propostasituacao.cdpropostasituacao")
					.leftOuterJoin("cliente.listaProposta listaProposta")
					.leftOuterJoin("listaProposta.propostasituacao propostasituacao")
					.whereIn("listaProposta.cdproposta", whereIn)
					.orderBy("cliente.nome")
					.list();
	}
	
	/**
	 * Carrega a lista de clientes para a gera��o do relat�rio completo.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<ClienteCompletoReportBean> findForReportCompleto(String whereIn, String whereInEmpresaUsuario) {
		SinedUtil.markAsReader();
		List<ClienteCompletoReportBean> lista = getJdbcTemplate().query(
			"select " +
			"p1.cdpessoa as id_cliente, " +
			"p1.nome as nome_cliente, " +
			"c1.razaosocial as razaosocial_cliente, " +
			"p1.tipopessoa as tipopessoa_cliente, " +
			"p1.cpf as cpf_cliente, " +
			"p1.cnpj as cnpj_cliente, " +
			"p1.email as email_cliente, " +
			"array_to_string(array(select t1.telefone from telefone t1 where t1.cdpessoa = p1.cdpessoa), '\n') as telefones_cliente, " +
			"array_to_string(array(select cat.nome from categoria cat join pessoacategoria pcat on pcat.cdcategoria = cat.cdcategoria where pcat.cdpessoa = p1.cdpessoa), '\n') as categorias_cliente, " +
			
			"p2.nome as nome_contato, " +
			"m2.nome as municipio_contato, " +
			"u2.sigla as uf_contato, " +
			"array_to_string(array(select t2.telefone from telefone t2 where t2.cdpessoa = p2.cdpessoa), '\n') as telefones_contato, " +
			"c2.emailcontato as email_contato, " +
			"p2.observacao as observacao_contato " +
			
			"from cliente c1 " +
			"join pessoacontato pc on pc.cdpessoa = c1.cdpessoa " +
			"join contato c2 on c2.cdpessoa = pc.cdcontato " +
			"join pessoa p1 on p1.cdpessoa = c1.cdpessoa " +
			"join pessoa p2 on p2.cdpessoa = c2.cdpessoa " +
			"left outer join municipio m2 on m2.cdmunicipio = c2.cdmunicipio " +
			"left outer join uf u2 on u2.cduf = m2.cduf " +
			
			"where p1.cdpessoa in (" + whereIn + ") " +
			
			getWhereClienteempresa("c1", true, null) +
			
			(whereInEmpresaUsuario != null && !"".equals(whereInEmpresaUsuario) ? 
					" and (c2.cdempresa is null or c2.cdempresa in (" + whereInEmpresaUsuario + ")) " 
					: 
					"") +
			
			"order by p1.nome, p1.cdpessoa, p2.nome", 
		new RowMapper() {
			public ClienteCompletoReportBean mapRow(ResultSet rs, int rowNum) throws SQLException {
				ClienteCompletoReportBean bean = new ClienteCompletoReportBean();
				
				Cpf cpfCliente = null;
				Cnpj cnpjCliente = null;
				Tipopessoa tipopessoaCliente = Tipopessoa.values()[rs.getInt("tipopessoa_cliente")];
				if(tipopessoaCliente.equals(Tipopessoa.PESSOA_FISICA)){
					String cpf = rs.getString("cpf_cliente");
					if(cpf != null && !cpf.equals("")){
						cpfCliente = new Cpf(cpf);
					}
				} else {
					String cnpj = rs.getString("cnpj_cliente");
					if(cnpj != null && !cnpj.equals("")){
						cnpjCliente = new Cnpj(cnpj);
					}
				}
				
				bean.setCdcliente(rs.getInt("id_cliente"));
				bean.setNome_cliente(rs.getString("nome_cliente"));
				bean.setRazaosocial_cliente(rs.getString("razaosocial_cliente"));
				bean.setTipopessoa_cliente(tipopessoaCliente);
				bean.setCpf_cliente(cpfCliente);
				bean.setCnpj_cliente(cnpjCliente);
				bean.setEmail_cliente(rs.getString("email_cliente"));
				bean.setTelefones_cliente(rs.getString("telefones_cliente"));
				bean.setCategorias_cliente(rs.getString("categorias_cliente"));
				
				bean.setNome_contato(rs.getString("nome_contato"));
				bean.setMunicipio_contato(rs.getString("municipio_contato"));
				bean.setUf_contato(rs.getString("uf_contato"));
				bean.setTelefones_contato(rs.getString("telefones_contato"));
				bean.setEmail_contato(rs.getString("email_contato"));
				bean.setObservacao_contato(rs.getString("observacao_contato"));
				
				return bean;
			}
		});
		return lista;
	}

	/**
	 * Renorna uma lista de cliente contendo apenas o campo Nome
	 * 
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Cliente> findDescricao(String whereIn) {
		return querySined()
			.select("cliente.nome")
			.whereIn("cliente.cdpessoa", whereIn)
			.list();
	}
	
	/**
	 * Carrega o cliente associado ao projeto. Se nenhum projeto for passado,
	 * retorna todos os clientes.
	 * 
	 * @param projeto
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Cliente> carregarClienteDoProjeto(Projeto projeto) {
		return querySined()
			.setUseWhereClienteEmpresa(true)
			.select("cliente.cdpessoa, cliente.nome")
			.leftOuterJoin("cliente.listaProjeto projeto")
			.where("projeto = ?", projeto)
			.orderBy("cliente.nome")
			.list();
	}
	
	/**
	 * Carrega um cliente com os seguintes dados:
	 * 	- Raz�o social
	 * 	- CNPJ/CPF
	 * 	- Inscri��o Estadual
	 * 	- Todos os endere�os associados (logradouro, numero, complemento, bairro, munic�pio, UF, CEP)
	 * 	- Todos os telefones associados
	 * 
	 * @param cliente
	 * @return
	 * @author Hugo Ferreira
	 */
	public Cliente carregarDadosCliente(Cliente cliente) {
		if (cliente == null || cliente.getCdpessoa() == null) {
			throw new SinedException("O par�metro cliente n�o pode ser nulo.");
		}
		
		QueryBuilder<Cliente> query = querySined();
		query
			.select("cliente.razaosocial, cliente.inscricaoestadual, cliente.cpf, cliente.cnpj, cliente.cdpessoa, cliente.tipopessoa, cliente.inscricaosuframa, " +
					"cliente.inscricaomunicipal, cliente.discriminardescontonota, cliente.discriminartaxaboleto, cliente.rg, " +
					"endereco.cdendereco, endereco.logradouro, endereco.numero, cliente.nome, cliente.email, endereco.inscricaoestadual, " +
					"endereco.complemento, endereco.bairro, endereco.pontoreferencia, endereco.cep, municipio.cdmunicipio, municipio.nome, uf.sigla, uf.cduf, " +
					"telefone.cdtelefone, telefone.telefone, enderecotipo.cdenderecotipo, cliente.naoconsiderartaxaboleto, endereco.descricao, endereco.substituirlogradouro, " +
					"telefonetipo.nome, telefonetipo.cdtelefonetipo, banco.cdbanco, banco.numero, cliente.observacao, cliente.infoadicionalfisco, " +
					"cliente.infoadicionalcontrib, cliente.inscricaoestadual, cliente.incidiriss, pais.cdpais, pais.nome, enderecotipo.cdenderecotipo, enderecotipo.nome, "+
					"cliente.contribuinteICMS, cliente.contribuinteicmstipo, cliente.identificador")
			.leftOuterJoin("cliente.listaTelefone telefone")
			.leftOuterJoin("telefone.telefonetipo telefonetipo")
			.leftOuterJoin("cliente.listaEndereco endereco")
			.leftOuterJoin("endereco.enderecotipo enderecotipo")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("endereco.pais pais")
			.leftOuterJoin("cliente.listaDadobancario dadobancario")
			.leftOuterJoin("dadobancario.banco banco")
			.orderBy("endereco.logradouro, endereco.numero")
			.where("cliente = ?", cliente);
		
		return query.unique();
	}
	
	/**
	 * M�todo que carrega o cliente com informa��es referente a nota
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public Cliente carregarInfNotaCliente(Cliente cliente) {
		if (cliente == null || cliente.getCdpessoa() == null) {
			throw new SinedException("O par�metro cliente n�o pode ser nulo.");
		}
		
		return querySined()
			.select("cliente.cdpessoa, cliente.infoadicionalfisco, cliente.infoadicionalcontrib")
			.where("cliente = ?", cliente)
			.unique();
	}
	
	/**
	 * Retorna uma lista de clientes que est�o vinculados a pelo menos um or�amento.
	 *
	 * @return lista de Cliente
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Cliente> findWithOrcamentoForFlex() {
		return querySined()
				.select("cliente.cdpessoa, cliente.nome")
				.join("cliente.listaOrcamento listaOrcamento")
				.orderBy("cliente.nome")
				.list();
	}

	/**
	 * Lista todos os clientes para o combo no flex.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cliente> findForComboFlex() {
		return querySined()
			.setUseWhereClienteEmpresa(true)
			.select("cliente.cdpessoa, cliente.nome")
			.orderBy("cliente.nome")
			.list();
	}

	/**
	 * Carrega informa��es para o boleto.
	 *
	 * @param cdpessoa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Cliente loadForBoleto(Integer cdpessoa) {
		return querySined()
					.select("cliente.razaosocial, cliente.nome, cliente.cpf, cliente.cnpj, cliente.identificador, endereco.logradouro, endereco.numero, " +
							"endereco.bairro, endereco.complemento, municipio.nome, uf.sigla, endereco.cep, endereco.caixapostal, endereco.cdendereco")
					.leftOuterJoin("cliente.listaEndereco endereco")
					.leftOuterJoin("endereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.where("cliente.cdpessoa = ?", cdpessoa)
					.unique();
	}

	/**
	 * M�todo que retorna os clientes pelo identificador
	 * 
	 * @param q
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Cliente> findAtivosAutocompleteByIdentificador(String q) {
		return querySined()
					.setUseWhereClienteEmpresa(true)
//					.autocomplete() //m�todo do flex
					.select("cliente.nome, cliente.cdpessoa")
					.whereLikeIgnoreAll("cliente.identificador", q)
					.where("cliente.ativo = ?", Boolean.TRUE)
					.orderBy("cliente.nome")
					.list();
	}
	
	
	/**
	 * Busca do autocomplete em SQL, por quest�es de performance
	 * @param q
	 * @param somenteAtivos
	 * @param whereCPF
	 * @param whereCNPJ
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private List<Cliente> findAutocompleteSQL(String q, boolean somenteAtivos, boolean whereCPF, boolean whereCNPJ, Colaborador colaborador, boolean inativoComContrato, String whereInEmpresa){
		Object[] params = new Object[]{};
		int qtParams = 0;
		q = Util.strings.tiraAcento(q.toUpperCase());
		if(!StringUtils.isNumeric(q)){
			whereCNPJ = false;
			whereCPF = false;
		}
		
		String sql = ""+ 
		" SELECT pessoa.nome, " + 
		"       cliente.identificador, " +
		"       cliente.cdpessoa, " +
		"       cliente.razaosocial, " +
		"       pessoa.cnpj, " +
		"       pessoa.cpf" +
		" FROM Cliente cliente " +
		" INNER JOIN Pessoa pessoa ON cliente.cdpessoa=pessoa.cdpessoa ";
		
		if(colaborador != null && colaborador.getCdpessoa() != null){
			sql +=  "LEFT OUTER JOIN Clientevendedor clientevendedor ON clientevendedor.cdcliente = pessoa.cdpessoa " +
					"WHERE (clientevendedor.cdclientevendedor is null OR clientevendedor.cdcolaborador = "+colaborador.getCdpessoa()+") ";
		}else{
			sql += "WHERE 1=1 ";
		}
		
		sql += getWhereClienteempresa("cliente", true, whereInEmpresa);
		
		sql += " AND (upper(retira_acento(pessoa.nome)) LIKE ('%'||?||'%') "; 
		qtParams++;
		
		if(whereCPF) {
			sql += "   or pessoa.cpf LIKE ('%'||?||'%') " ; 
			qtParams++;
		}
		if(whereCNPJ) {
			sql += "   or pessoa.cnpj LIKE ('%'||?||'%') " ; 
			qtParams++;
		}
		sql += ") ";
		
		if(somenteAtivos && !inativoComContrato) {
			sql += " AND cliente.ativo = true ";
		} else if(somenteAtivos && inativoComContrato){
			sql += " AND (cliente.ativo = true OR ";
			sql += " ((cliente.ativo is null OR cliente.ativo = false) AND exists(select contrato.cdcontrato from contrato where contrato.cdcliente = cliente.cdpessoa))) ";
		}
		
		sql += "  " +
		" union " +
		"  " +
		" SELECT pessoa.nome, " +
		"       cliente.identificador, " +
		"       cliente.cdpessoa, " +
		"       cliente.razaosocial, " +
		"       pessoa.cnpj, " +
		"       pessoa.cpf" +
		" FROM Cliente cliente " +
		" INNER JOIN Pessoa pessoa ON cliente.cdpessoa=pessoa.cdpessoa ";
	
		
		
		if(colaborador != null && colaborador.getCdpessoa() != null){
			sql +=  "LEFT OUTER JOIN Clientevendedor clientevendedor ON clientevendedor.cdcliente = cliente.cdpessoa " +
					"WHERE (clientevendedor.cdclientevendedor is null OR clientevendedor.cdcolaborador = "+colaborador.getCdpessoa()+") ";
		}else{
			sql += "WHERE 1=1 ";
		}
		
		sql += getWhereClienteempresa("cliente", true, whereInEmpresa);
		
		sql += " AND (upper(retira_acento(cliente.identificador)) LIKE ('%'||?||'%') " +
		"   or upper(retira_acento(cliente.razaosocial)) LIKE ('%'||?||'%') ";
		qtParams += 2;
		
		sql += ") ";
		
		if(somenteAtivos && !inativoComContrato) {
			sql += " AND cliente.ativo = true ";
		} else if(somenteAtivos && inativoComContrato){
			sql += " AND (cliente.ativo = true OR ";
			sql += " ((cliente.ativo is null OR cliente.ativo = false) AND exists(select contrato.cdcontrato from contrato where contrato.cdcliente = cliente.cdpessoa))) ";
		}
		
		sql += "   order by 1 " + 
		"   limit 30";
	
		for(int i=0; i < qtParams; i++ ){
			params = ArrayUtils.add(params, q);
		}

		SinedUtil.markAsReader();
		return (List<Cliente>) getJdbcTemplate().query(sql, params, new RowMapper() {
			public Cliente mapRow(ResultSet rs, int rowNum) throws SQLException {
				Cliente cliente = new Cliente();
				cliente.setCdpessoa(rs.getInt("cdpessoa"));
				cliente.setNome(rs.getString("nome"));
				cliente.setIdentificador(rs.getString("identificador"));
				cliente.setRazaosocial(rs.getString("razaosocial"));
				
				String cnpj = rs.getString("cnpj");
				if(StringUtils.isNotBlank(cnpj) && Cnpj.cnpjValido(cnpj)) cliente.setCnpj( new Cnpj(cnpj) );
				
				String cpf = rs.getString("cpf");
				if(StringUtils.isNotBlank(cpf) && Cpf.cpfValido(cpf)) cliente.setCpf( new Cpf(cpf) );
				
				return cliente;
			}
		});
	
	}
	
	/**
	* M�todo que cria a condi��o de restri��o de cliente por empresa
	*
	* @param aliasCliente
	* @param addAnd
	* @return
	* @since 31/10/2016
	* @author Luiz Fernando
	*/
	private String getWhereClienteempresa(String aliasCliente, boolean addAnd, String whereInEmpresa){
		String sql = "";
		if(SinedUtil.isRestricaoEmpresaClienteUsuarioLogado()){
			if(StringUtils.isBlank(aliasCliente)){
				throw new SinedException("Alias inv�lido para a condi��o de restri��o de cliente por empresa");
			}
			String whereInEmpresaUsuario = new SinedUtil().getListaEmpresa();
			if(StringUtils.isNotBlank(whereInEmpresaUsuario) || StringUtils.isNotBlank(whereInEmpresa)){
				sql += (addAnd ? " and " : "") + aliasCliente + ".cdpessoa in ( select cRestricaoEmpresa.cdpessoa from Cliente cRestricaoEmpresa " +
								" left outer join clienteempresa ceRestricaoEmpresa on ceRestricaoEmpresa.cdcliente = cRestricaoEmpresa.cdpessoa " +
								" where ceRestricaoEmpresa.cdempresa is null or ceRestricaoEmpresa.cdempresa in ("+ 
								(StringUtils.isNotBlank(whereInEmpresa) ? whereInEmpresa : whereInEmpresaUsuario) +
								")) ";
			}
		}
		return sql;
	}
	
	public List<Cliente> findAutocompleteByNomeOrIdentificadorWithContrato(String q, String whereInEmpresa) {
		if(ParametrogeralService.getInstance().getBoolean("useFunctionOnAutocomplete")){
			return findAutocompleteSQL(q, true, false, false, null, true, whereInEmpresa);
		} else{
			QueryBuilder<Cliente> query = querySined()
			
			.setUseWhereClienteEmpresa(true)
			.autocomplete()
			.select("cliente.nome, cliente.identificador, cliente.cdpessoa, cliente.razaosocial, cliente.cnpj, cliente.cpf")
			.openParentheses()
			.whereLikeIgnoreAll("cliente.identificador", q).or()
			.whereLikeIgnoreAll("cliente.nome", q).or()
			.closeParentheses()
			.openParentheses()
			.where("cliente.ativo = ?", Boolean.TRUE)
			.or()
			.where("exists(select c.cdcontrato from Contrato c where c.cliente = cliente)")
			.closeParentheses();
			
			addWhereInClienteByEmpresa(query, whereInEmpresa);
			
			return query
					.orderBy("cliente.nome")
					.list();
		}
	}
	
	public List<Cliente> findAutocompleteByNomeOrIdentificador(String q, String whereInEmpresa) {
		if(ParametrogeralService.getInstance().getBoolean("useFunctionOnAutocomplete")){
			return findAutocompleteSQL(q, true, false, false, null, false, whereInEmpresa);
		} else{
			QueryBuilder<Cliente> query = querySined()
			
			.setUseWhereClienteEmpresa(true)
			.autocomplete()
			.select("cliente.nome, cliente.identificador, cliente.cdpessoa, cliente.razaosocial, cliente.cnpj, cliente.cpf")
			.openParentheses()
				.whereLikeIgnoreAll("cliente.identificador", q).or()
				.whereLikeIgnoreAll("cliente.nome", q).or()
			.closeParentheses()
			.where("cliente.ativo = ?", Boolean.TRUE);
			
			addWhereInClienteByEmpresa(query, whereInEmpresa);
			
			return query
					.orderBy("cliente.nome")
					.list();
		}
	}
	
	public List<Cliente> findAutocompleteByNomeOrIdentificadorOrCpfOrCnpj(String q, String whereInEmpresa) {
		if(ParametrogeralService.getInstance().getBoolean("useFunctionOnAutocomplete")){
			return findAutocompleteSQL(q, true, true, true, null, false, whereInEmpresa);
		} else{
			QueryBuilder<Cliente> query = querySined()
			
			.setUseWhereClienteEmpresa(true)
			.autocomplete()
			.select("cliente.nome, cliente.identificador, cliente.cdpessoa, cliente.razaosocial, cliente.cnpj, cliente.cpf")
			.openParentheses()
				.whereLikeIgnoreAll("cliente.identificador", q).or()
				.whereLikeIgnoreAll("cliente.nome", q).or()
				.whereLikeIgnoreAll("cliente.cpf", q).or()
				.whereLikeIgnoreAll("cliente.cnpj", q).or()
			.closeParentheses()
			.where("cliente.ativo = ?", Boolean.TRUE);
			
			addWhereInClienteByEmpresa(query, whereInEmpresa);
			
			return query
				.orderBy("cliente.nome")
				.list();
		}
	}
	
	public List<Cliente> findAutocompleteByRazaosocialWithContrato(String q) {
		return querySined()
		
		.setUseWhereClienteEmpresa(true)
		.autocomplete()
		.select("cliente.nome, cliente.identificador, cliente.cdpessoa, cliente.razaosocial, cliente.cnpj, cliente.cpf")
		.whereLikeIgnoreAll("cliente.razaosocial", q)
		.openParentheses()
		.where("cliente.ativo = ?", Boolean.TRUE)
		.or()
		.where("exists(select c.cdcontrato from Contrato c where c.cliente = cliente)")
		.closeParentheses()
		.orderBy("cliente.nome")
		.list();
	}
	
	public List<Cliente> findAutocompleteByRazaosocial(String q) {
		return querySined()
		.setUseWhereClienteEmpresa(true)
		.autocomplete()
		.select("cliente.nome, cliente.identificador, cliente.cdpessoa, cliente.razaosocial, cliente.cnpj, cliente.cpf")
		.whereLikeIgnoreAll("cliente.razaosocial", q)
		.where("cliente.ativo = ?", Boolean.TRUE)
		.orderBy("cliente.nome")
		.list();
	}
	
	public List<Cliente> findAutocompleteByNomeRazaosocial (String q){
		if(ParametrogeralService.getInstance().getBoolean("useFunctionOnAutocomplete")){
			return findAutocompleteSQL(q, false, false, false, null, false, null);
		} else{
			return querySined()
					
					.setUseWhereClienteEmpresa(true)
					.openParentheses()
						.whereLikeIgnoreAll("cliente.nome", q)
						.or()
						.whereLikeIgnoreAll("cliente.razaosocial", q)
					.closeParentheses()
					.orderBy("cliente.nome, cliente.razaosocial")
					.list();
		}
	}
	
	/**
	 * M�todo que busca os cliente pelo nome ou identificador
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Cliente> findAutocompleteByNomeIdentificador(String q) {
		if(ParametrogeralService.getInstance().getBoolean("useFunctionOnAutocomplete")){
			return findAutocompleteSQL(q, true, false, false, null, false, null);
		} else{
			return querySined()
					
				.setUseWhereClienteEmpresa(true)
				.autocomplete()
				.select("cliente.nome, cliente.identificador, cliente.cdpessoa, cliente.razaosocial, cliente.cnpj, cliente.cpf")
				.openParentheses()
					.whereLikeIgnoreAll("cliente.identificador", q).or()
					.whereLikeIgnoreAll("cliente.nome", q).or()
					.whereLikeIgnoreAll("cliente.razaosocial", q)
				.closeParentheses()
				.where("cliente.ativo = ?", Boolean.TRUE)
				.orderBy("cliente.nome")
				.list();	
		}
	}
	
	/**
	 * M�todo que busca os cliente ativos e inativos, pelo nome ou identificador
	 *
	 * @param q
	 * @return
	 * @author Filipe Santos
	 */
	public List<Cliente> findAllAutocompleteByNomeIdentificador(String q) {
		if(ParametrogeralService.getInstance().getBoolean("useFunctionOnAutocomplete")){
			return findAutocompleteSQL(q, false, false, false, null, false, null);
		} else{
			return querySined()
			
			.setUseWhereClienteEmpresa(true)
			.autocomplete()
			.select("cliente.nome, cliente.identificador, cliente.cdpessoa")
			.openParentheses()
				.whereLikeIgnoreAll("cliente.identificador", q).or()
				.whereLikeIgnoreAll("cliente.nome", q)
			.closeParentheses()
			.orderBy("cliente.nome")
			.list();
		}
	}
	
	public List<Cliente> findAutocompleteByNomeIdentificadorCnpj(String q) {
		if(ParametrogeralService.getInstance().getBoolean("useFunctionOnAutocomplete")){
			return findAutocompleteSQL(q, false, false, true, null, false, null);		
		} else{
			return querySined()
			
			.setUseWhereClienteEmpresa(true)
			.autocomplete()
			.select("cliente.cdpessoa, cliente.nome, cliente.identificador, cliente.cdpessoa, cliente.cnpj, cliente.cpf, cliente.razaosocial")
			.openParentheses()
				.whereLikeIgnoreAll("cliente.identificador", q).or()
				.whereLikeIgnoreAll("cliente.nome", q).or()
				.whereLikeIgnoreAll("cliente.cnpj", q)
			.closeParentheses()
			.orderBy("cliente.nome")
			.list();
		}
	}
	
	/**
	 * M�todo que busca os clientes pelo nome/cpf/cnpj
	 * @param whereLike
	 * @return
	 */
	public List<Cliente> findClientesNomeCnpj(String q) {
		if(ParametrogeralService.getInstance().getBoolean("useFunctionOnAutocomplete")){
			return findAutocompleteSQL(q, false, true, true, null, false, null);
		} else{
			return querySined()
				.setUseWhereClienteEmpresa(true)
				.autocomplete()
				.select("cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj")
				.openParentheses()
				.whereLikeIgnoreAll("cliente.nome", q)
				.or()
				.whereLikeIgnoreAll("cliente.cpf", q)
				.or()
				.whereLikeIgnoreAll("cliente.cnpj", q)
				.closeParentheses()
				.orderBy("cliente.nome")
				.list();
		}
	}
	
	public List<Cliente> findClientesNomeCpfCnpjAtivos(String whereLike, String whereInEmpresa) {
		QueryBuilder<Cliente> query = querySined()
		.setUseWhereClienteEmpresa(true)
		.autocomplete()
		.select("cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj")
		.openParentheses()
		.whereLikeIgnoreAll("cliente.nome", whereLike)
		.where("cliente.ativo = ?", Boolean.TRUE)
		.or()
		.whereLikeIgnoreAll("cliente.cpf", whereLike)
		.or()
		.whereLikeIgnoreAll("cliente.cnpj", whereLike)
		.closeParentheses();
		
		addWhereInClienteByEmpresa(query, whereInEmpresa);
		
		return query
				.orderBy("cliente.nome")
				.list();
	}
	
	/**
	 * M�todo para o autocomplete da conta a receber.
	 * Favor n�o apagar!!
	 *
	 * @param q
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cliente> findAtivosAutocomplete(String q, String whereInEmpresa) {
		if(ParametrogeralService.getInstance().getBoolean("useFunctionOnAutocomplete")){
			return findAutocompleteSQL(q, true, false, false, null, false, whereInEmpresa);
		} else{
			QueryBuilder<Cliente> query = querySined()
					.setUseWhereClienteEmpresa(true)
					.autocomplete()
					.select("cliente.identificador, cliente.nome, cliente.cdpessoa, cliente.cpf, cliente.cnpj, cliente.razaosocial")
					.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
					.openParentheses()
						.whereLikeIgnoreAll("cliente.nome", q).or()
						.whereLikeIgnoreAll("cliente.razaosocial", q)
					.closeParentheses()
					.where("cliente.ativo = ?", Boolean.TRUE);
			
			addWhereInClienteByEmpresa(query, whereInEmpresa);
			
			return query
						.orderBy("cliente.nome")
						.list();
		}	
	}
	
	/**
	* M�todo que busca os clientes ativos de acordo com a restri��o cliente/vendedor
	*
	* @param q
	* @param usuario
	* @return
	* @since 22/10/2015
	* @author Luiz Fernando
	*/
	public List<Cliente> findAtivosEClienteDoUsuarioAutocomplete(String q, Usuario usuario, String whereInEmpresa) {
		if(ParametrogeralService.getInstance().getBoolean("useFunctionOnAutocomplete")){
			Colaborador colaborador = null;
			if(usuario != null && usuario.getCdpessoa() != null && usuario.getRestricaoclientevendedor() != null &&
					usuario.getRestricaoclientevendedor()){
				colaborador = new Colaborador(usuario.getCdpessoa());
			}
			return findAutocompleteSQL(q, true, false, false, colaborador, false, whereInEmpresa);
		} else{
			QueryBuilder<Cliente> query = querySined()
					.setUseWhereClienteEmpresa(true)
					.autocomplete()
					.select("cliente.identificador, cliente.nome, cliente.cdpessoa, cliente.cpf, cliente.cnpj, cliente.razaosocial");
			
			if(usuario != null && usuario.getCdpessoa() != null && usuario.getRestricaoclientevendedor() != null &&
					usuario.getRestricaoclientevendedor()){
				query
				.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor", true)
				.leftOuterJoin("listaClientevendedor.colaborador colaborador", true)
				.openParentheses()
				.where("listaClientevendedor is null").or()
				.where("colaborador.cdpessoa = ?", usuario.getCdpessoa())
				.closeParentheses();
			}
			
			addWhereInClienteByEmpresa(query, whereInEmpresa);
			
			return query.openParentheses()
						.whereLikeIgnoreAll("cliente.nome", q).or()
						.whereLikeIgnoreAll("cliente.razaosocial", q)
					.closeParentheses()
					.where("cliente.ativo = ?", Boolean.TRUE)		
					.orderBy("cliente.nome")
					.list();
		}
		
	}
	
	public List<Cliente> getClientesByEmpresa(Empresa empresa) {
		return querySined()
					.select("cliente.nome, cliente.cdpessoa")
					.leftOuterJoin("cliente.listaContrato listaContrato")
					.leftOuterJoin("listaContrato.empresa empresa")
					.where("empresa = ?", empresa)
					.orderBy("cliente.nome")
					.list();
	}

	/**
	 * M�todo que realiza busca geral em clientes de acordo com o par�metro informado na busca
	 * 
	 * @param busca
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Cliente> findClientesForBuscaGeral(String busca) {		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		QueryBuilder<Cliente> query = querySined()
		.setUseWhereClienteEmpresa(true)
		.select("cliente.cdpessoa, cliente.nome, contato.cdpessoa, contato.nome")
		.leftOuterJoin("cliente.listaContato listaContato")
		.leftOuterJoin("listaContato.contato contato")
		.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
		.leftOuterJoin("listaClientevendedor.colaborador colaboradorvendedor")
		.leftOuterJoin("listaClientevendedor.agencia agencia");

		if(SinedUtil.isRestricaoClienteVendedor(usuarioLogado)){
			query
			.openParentheses()
			.where("colaboradorvendedor.cdpessoa = ?", usuarioLogado.getCdpessoa())
			.or()
			.where("agencia.cdpessoa = ?", usuarioLogado.getCdpessoa())
			.closeParentheses();
		}
		return query.openParentheses()
			.whereLikeIgnoreAll("cliente.nome", busca).or()
			.whereLikeIgnoreAll("cliente.email", busca).or()
			.whereLikeIgnoreAll("cliente.razaosocial", busca).or()
			.whereLikeIgnoreAll("contato.nome", busca).or()
			.whereLikeIgnoreAll("contato.email", busca).or()
			.whereLikeIgnoreAll("contato.emailcontato", busca).or()
			.whereLikeIgnoreAll("contato.observacao", busca).or()
			.whereLikeIgnoreAll("cliente.observacao", busca)
		.closeParentheses()
		.orderBy("cliente.nome")
		.list();
	}

	/**
	 * M�todo que busca os clientes aniversariantes do m�s
	 * 
	 * @param mes
	 * @return
	 * @author Tom�s Rabelo
	 */
	@SuppressWarnings("unchecked")
	public List<Cliente> findClientesAniversariantesDoMesFlex(Integer mes) {
		
		String sql = "SELECT \"CDPESSOA\", \"NOME\", \"DTNASCIMENTO\" FROM ("+
					 "SELECT C.CDPESSOA AS \"CDPESSOA\", P.NOME AS \"NOME\", P.DTNASCIMENTO AS \"DTNASCIMENTO\" "+
					 "FROM CLIENTE C " +
					 "JOIN PESSOA P ON P.CDPESSOA = C.CDPESSOA "+
					 "WHERE EXTRACT(MONTH FROM P.DTNASCIMENTO) = "+mes+" " +
					 
					 getWhereClienteempresa("C", true, null) +
					 
					 "UNION " +

					 "SELECT CO.CDPESSOA AS \"CDPESSOA\", P.NOME || '\n  ' || COALESCE(C2.RAZAOSOCIAL, P2.NOME) AS \"NOME\", P.DTNASCIMENTO AS \"DTNASCIMENTO\" "+
					 "FROM CONTATO CO "+
					 "JOIN PESSOA P ON P.CDPESSOA = CO.CDPESSOA " +
					 "JOIN PESSOACONTATO PC ON PC.CDCONTATO = CO.CDPESSOA " +
					 "JOIN CLIENTE C2 ON C2.CDPESSOA = PC.CDPESSOA " +
					 "JOIN PESSOA P2 ON P2.CDPESSOA = PC.CDPESSOA "+
					 "WHERE EXTRACT(MONTH FROM P.DTNASCIMENTO) = "+mes+""+
					 getWhereClienteempresa("C2", true, null) +
					 ") foo ORDER BY EXTRACT(DAY FROM \"DTNASCIMENTO\")";
		
		SinedUtil.markAsReader();
		List<Cliente> list = getJdbcTemplate().query(
				sql, 
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						Cliente cliente = new Cliente();
						cliente.setCdpessoa(rs.getInt("CDPESSOA"));
						cliente.setNome(rs.getString("NOME"));
						cliente.setDtnascimento(SinedDateUtils.setDateProperty(rs.getDate("DTNASCIMENTO"), 12, Calendar.HOUR_OF_DAY));
						return cliente;
					}
				});
		return list;
//		return query()
//			.select("cliente.cdpessoa, cliente.nome, cliente.dtnascimento")
//			.where("extract(month from cliente.dtnascimento) = ?", mes)
//			.orderBy("extract(day from cliente.dtnascimento)")
//			.list();
	}

	/**
	 * Insere somente na tabela de cliente o cdpessoa.
	 *
	 * @param cliente
	 * @author Rodrigo Freitas
	 */
	public void insertSomenteCliente(Cliente cliente) {
		if(cliente == null || cliente.getCdpessoa() == null){
			throw new SinedException("Cliente n�o pode ser nulo.");
		}
		getJdbcTemplate().update("insert into Cliente (cdpessoa, ativo, identificador) values (?, true, ?)", new Object[]{cliente.getCdpessoa(), cliente.getIdentificador()});
	}
	
	public List<Cliente> findClientesContaReceberAtrasadaOuRestricoesEmAberto(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Lista de clientes n�o pode ser nula.");
		}
		
		return querySined()
			.select("cliente.cdpessoa, cliente.nome")
			.openParentheses()
			//Clientes com documentosAtrasados
			.where("cliente.id in (select c.id from Documento d " +
								  "join d.pessoa c " +
								  "join d.documentoclasse dc " +
								  "join d.documentoacao da " +
								  "where dc.id = "+Documentoclasse.CD_RECEBER+" and d.dtvencimento < TO_DATE('"+new SimpleDateFormat("dd/MM/yyyy").format(new Date(System.currentTimeMillis()))+"', 'DD/MM/YYYY') and " +
								  		"da.id <> "+Documentoacao.CANCELADA.getCddocumentoacao()+" and da.id <> "+Documentoacao.BAIXADA.getCddocumentoacao()+" and c.id in ("+whereIn+"))")
			.or()
			//Clientes com restri��es em aberto
			.where("cliente.id in (select c.id from Cliente c " +
								  "join c.listaRestricao lr " +
								  "where lr.dtliberacao is null and c.id in ("+whereIn+"))")
			.closeParentheses()
			.list();
	}

	/**
	 * M�todo que carrega o cliente e sua respectiva lista de restri��es
	 * 
	 * @param cliente
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Cliente carregaRestricoesCliente(Cliente cliente) {
		if(cliente == null || cliente.getCdpessoa() == null)
			throw new SinedException("Cliente parametros inv�lidos.");
		
		return querySined()
			.select("cliente.cdpessoa, cliente.nome, listaRestricao.cdrestricao, listaRestricao.dtrestricao, listaRestricao.motivorestricao")
			.join("cliente.listaRestricao listaRestricao")
			.where("cliente = ?", cliente)
			.where("listaRestricao.dtliberacao is null")
			.orderBy("listaRestricao.dtrestricao desc")
			.unique();
	}

	/**
	 * M�todo que carrega os clientes pela categoria
	 * 
	 * @param categoria
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Cliente> findByCategoria(Categoria categoria) {
		return querySined()
			.select("cliente.cdpessoa, cliente.nome")
			.join("cliente.listaPessoacategoria listaPessoacategoria")
			.join("listaPessoacategoria.categoria categoria")
			.where("categoria = ?", categoria)
			.orderBy("cliente.nome")
			.list();
	}

	/**
	 * Busca o cliente pelo CNPJ
	 *
	 * @param cnpj
	 * @return
	 * @since 22/06/2012
	 * @author Rodrigo Freitas
	 */
	public Cliente findByCnpj(Cnpj cnpj) {
		if(cnpj == null){
			throw new SinedException("O CNPJ n�o pode ser nulo.");
		}
		return querySined()
					.select("cliente.cdpessoa, cliente.nome, cliente.email, cliente.cnpj")
					.where("cliente.cnpj = ?", cnpj)
					.unique();
	}
	
	public Cliente findByCnpjSemEmpresa(Cnpj cnpj) {
		if(cnpj == null){
			throw new SinedException("O CNPJ n�o pode ser nulo.");
		}
		return querySined()
				.setUseWhereEmpresa(false)
				.setUseWhereClienteEmpresa(false)
				.select("cliente.cdpessoa, cliente.nome, cliente.email, cliente.cnpj")
				.where("cliente.cnpj = ?", cnpj)
				.unique();
	}
	
	/**
	 * Busca o cliente pelo CPF
	 *
	 * @param cpf
	 * @return
	 * @since 22/06/2012
	 * @author Rodrigo Freitas
	 */
	public Cliente findByCpf(Cpf cpf) {
		if(cpf == null){
			throw new SinedException("O CPF n�o pode ser nulo.");
		}
		return querySined()
					.select("cliente.cdpessoa, cliente.nome, cliente.email, cliente.cpf")
					.where("cliente.cpf = ?", cpf)
					.unique();
	}
	
	public Cliente findByCpfSemEmpresa(Cpf cpf) {
		if(cpf == null){
			throw new SinedException("O CPF n�o pode ser nulo.");
		}
		return querySined()
				.setUseWhereEmpresa(false)
				.setUseWhereClienteEmpresa(false)
				.select("cliente.cdpessoa, cliente.nome, cliente.email, cliente.cpf")
				.where("cliente.cpf = ?", cpf)
				.unique();
	}

	
	@SuppressWarnings("unchecked")
	public boolean verificaOutrosRegistros(String itens) {
		if(itens == null || itens.equals("")){
			throw new SinedException("Cliente n�o pode ser nulo.");
		}
		
		List<String> listaString = getJdbcTemplate().query("SELECT 'SELECT F1.CDPESSOA FROM CLIENTE F1 JOIN ' || t.relname || ' ON ' || t.relname || '.' || a.attname || ' = F1.CDPESSOA WHERE F1.CDPESSOA IN (XX)' AS RELACIONAMENTO " +
																"FROM pg_constraint c " +
																"LEFT JOIN pg_class t  ON c.conrelid  = t.oid " +
																"LEFT JOIN pg_class t2 ON c.confrelid = t2.oid " +
																"LEFT JOIN pg_attribute a ON t.oid = a.attrelid AND a.attnum = c.conkey[1] " +
																"LEFT JOIN pg_attribute a2 ON t2.oid = a2.attrelid AND a2.attnum = c.confkey[1] " +
																"WHERE (" +
																"t2.relname = 'pessoa' OR " +
																"t2.relname = 'fornecedor' OR " +
																"t2.relname = 'contato' OR " +
																"t2.relname = 'empresa' OR " +
																"t2.relname = 'cliente' OR " +
																"t2.relname = 'dadobancario' OR " +
																"t2.relname = 'colaborador' OR " +
																"t2.relname = 'usuario') " +
																"AND t.relname <> 'pessoa' " +
																"AND t.relname <> 'clientehistorico' " +
																"AND t.relname <> 'endereco' " +
																"AND t.relname <> 'telefone' " +
																"AND t.relname <> 'pessoacategoria' " +
																"AND t.relname <> 'contato' " +
																"AND t.relname <> 'dadobancario' " +
																"AND t.relname <> 'cliente' " +
																"AND c.contype = 'f'" , 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString("RELACIONAMENTO");
			}

		});
		
		String sql = "SELECT COUNT(*) FROM (";
		
		boolean first = false;
		for (String string : listaString) {
			if (first) {
				sql += " UNION ALL ";
			}
			sql += string;
			first = true;
		}
		
		sql += ") QUERY";
		
		sql = sql.replaceAll("XX", itens);
		
		int count = getJdbcTemplate().queryForInt(sql);
		
		return count != 0;
	}

	
	/**
	 * Carrega dados de clientes servico para o relat�rio Hist�rico de cliente.
	 * @param filtro
	 * @return
	 * @author Taidson
	 * @since 26/08/2010
	 */
	public List<Cliente>listaHistoricoCliente(String whereIn){
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("O par�metro whereIn n�o pode ser nulo.");
		}
		return querySined()
			.select("distinct cliente.cdpessoa, cliente.nome, cliente.email, cliente.tipopessoa, " +
					"cliente.cpf, cliente.cnpj")
			.whereIn("cliente.id",whereIn)
			.orderBy("cliente.nome")
			.list();

	}
	
	/**
	 * Retorna todos os cliente ativos
	 * @return
	 * @author Taidson
	 * @since 09/11/2010
	 */
	public List<Cliente> findAllAtivos(){
		return querySined()
			.setUseWhereClienteEmpresa(true)
			.select("distinct cliente.cdpessoa, cliente.nome")
			.where("cliente.ativo is true")
			.orderBy("cliente.nome")
			.list();
		
	}
	
	/**
	 * Carrega dados do respons�vel financeiro
	 * @param pessoa
	 * @return
	 * @author Taidson
	 * @since 05/01/2011
	 */
	public Cliente carregaDadosResponsavelFinanceiro(Pessoa pessoa) {
		if(pessoa == null || pessoa.getCdpessoa() == null)
			throw new SinedException("Cliente parametros inv�lidos.");
		
		return querySined()
			.select("cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj, responsavelFinanceiro.cdpessoa, responsavelFinanceiro.nome, " +
					"responsavelFinanceiro.cpf, responsavelFinanceiro.cnpj")
			.join("cliente.responsavelFinanceiro responsavelFinanceiro")
			.where("cliente.cdpessoa = ?", pessoa.getCdpessoa())
			.unique();
	}
	
	/**
	 * M�todo que verifica se o identificador j� foi cadastrado anteriormente
	 * 
	 * @param identificador
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeIdentificadorCliente(String identificador, Integer cdpessoa) {
		SinedUtil.markAsReader();
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(beanClass)
			.where("cliente.identificador = ?", identificador)
			.where("cliente.cdpessoa <> ?", cdpessoa)
			.unique() > 0;
	}
	
	/**
	 * Busca todos os registros para a exporta��o de arquivos gerenciais.
	 * @author Giovane Freitas <giovane.freitas@linkcom.com.br>
	 */
	public Iterator<Object[]> findForArquivoGerencial() {
		return newQueryBuilder(Object[].class)
			.from(Cliente.class)
			.select("cliente.cdpessoa, cliente.nome, municipio.nome, uf.sigla")
			.join("cliente.listaEndereco endereco")
			.join("endereco.municipio municipio")
			.join("municipio.uf uf")
	
			//A ordem para selecionar o endere�o deve ser a seguinte: "�nico", "Faturamento", "Cobran�a" e depois qualquer um.
			.orderBy("cliente.cdpessoa, municipio.cdmunicipio, endereco.enderecotipo.id")
			
			.iterate();
	}

	/**
	 * M�todo de autocomplete para buscar por nome ou raz�o social de cliente
	 * 
	 * @param q
	 * @return
	 */
	public List<Cliente> findClienteAtivosNomeRazaosocial(String q) {
		if(ParametrogeralService.getInstance().getBoolean("useFunctionOnAutocomplete")){
			return findAutocompleteSQL(q, true, false, false, null, false, null);
		}else{
			return querySined()
					.setUseWhereClienteEmpresa(true)
					.autocomplete()
					.select("cliente.cdpessoa, cliente.nome, cliente.razaosocial, cliente.cnpj, cliente.cpf")
					.openParentheses()
					.whereLikeIgnoreAll("cliente.nome", q)
					.or()
					.whereLikeIgnoreAll("cliente.razaosocial", q)
					.closeParentheses()
					.where("cliente.ativo = true")
					.orderBy("cliente.nome")
					.list();
		}
	}
	
	/**
	 * Busca o cliente pelo nome.
	 *
	 * @param nome
	 * @return
	 * @since 05/03/2012
	 * @author Rodrigo Freitas
	 */
	public Cliente findByNome(String nome) {
		if(nome == null || nome.equals("")){
			throw new SinedException("Nome n�o pode ser nulo.");
		}
		List<Cliente> lista = querySined()
					.select("cliente.cdpessoa, cliente.nome")
					.where("cliente.nome = ?", nome)
					.list();
		
		if(lista == null || lista.size() == 0){
			return null;
		} else if(lista.size() > 1){
			throw new SinedException("Existem mais de um cliente com o nome: " + nome);
		} else return lista.get(0);
	}
	
	/**
	 * M�todo que busca os dados do cliente para integra��o de cadastro da dom�nio
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Cliente> findForDominiointegracaoCadastro(DominiointegracaoFiltro filtro) {
		if(filtro == null || filtro.getDtinicio() == null || filtro.getDtfim() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return querySined()
				.select("cliente.cdpessoa, cliente.razaosocial, cliente.nome, cliente.tipopessoa, cliente.cnpj, cliente.cpf, " +
						"cliente.inscricaoestadual, cliente.inscricaomunicipal, endereco.caixapostal, " +
						"endereco.logradouro, endereco.bairro, endereco.complemento, endereco.cep, endereco.numero, " +
						"municipio.cdibge, municipio.nome, uf.nome, uf.sigla, pais.cdpais, telefone.telefone,  telefonetipo.cdtelefonetipo ")
				.leftOuterJoin("cliente.listaEndereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("endereco.pais pais")
				.leftOuterJoin("cliente.listaTelefone telefone")
				.leftOuterJoin("telefone.telefonetipo telefonetipo")
				.where("exists( " +
						"select 1 from Nota n " +
						"join n.cliente c " +
						"where n.dtEmissao >= to_date('"+ format.format(filtro.getDtinicio())+ "', 'DD/MM/YYYY') " +
						" and n.dtEmissao <= to_date('"+ format.format(filtro.getDtfim())+ "', 'DD/MM/YYYY') " +
						" and c.cdpessoa = cliente.cdpessoa " +
						") ")
				.list();
	}
	
	/**
	 * M�todo que verifica se usu�rio pode editar o cliente
	 *
	 * @param cliente
	 * @param usuario
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean isUsuarioPertenceRestricaoclientevendedor(Cliente cliente, Usuario usuario, Boolean limitarAcessoClienteVendedor) {
		if(cliente == null || cliente.getCdpessoa() == null || usuario == null || usuario.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilderSined<Long> query = newQueryBuilderSined(Long.class);
		query
			
			.select("count(*)")
			.from(Cliente.class)
			.setUseTranslator(false)
			.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
			.leftOuterJoin("listaClientevendedor.colaborador colaborador")
			.leftOuterJoin("listaClientevendedor.agencia agencia")
			.where("cliente.cdpessoa = ?", cliente.getCdpessoa())
			.openParentheses()
				.where("colaborador.cdpessoa = ?", usuario.getCdpessoa())
				.or()
				.where("agencia.cdpessoa = ?", usuario.getCdpessoa());
				if (limitarAcessoClienteVendedor == null || !limitarAcessoClienteVendedor) {
					query
					.or()
					.where("listaClientevendedor is null");
				} 
			query.closeParentheses();
			return query.unique() > 0;
	}
	
	/**
	 * M�todo que busca os cliente ativos e apenas daquele usu�rio
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Cliente> findClienteAtivosNomeRazaosocialEClienteDoUsuario(String q, Usuario usuario) {
		QueryBuilder<Cliente> query = querySined()
					.setUseWhereClienteEmpresa(true)
					.autocomplete()
					.select("distinct cliente.cdpessoa, cliente.nome, cliente.razaosocial, cliente.cnpj, cliente.cpf, cliente.identificador")
					.openParentheses()
						.whereLikeIgnoreAll("cliente.nome", q)
						.or()
						.whereLikeIgnoreAll("cliente.razaosocial", q)
						.or()
						.whereLikeIgnoreAll("cliente.identificador", q)
					.closeParentheses()
					//.where("autocomplete_cliente_pessoa('CLIENTE', '"+q+"', cliente) = true")
					.where("cliente.ativo = true");
		
		if(usuario != null && usuario.getCdpessoa() != null && usuario.getRestricaoclientevendedor() != null &&
				usuario.getRestricaoclientevendedor()){
			query
				.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor", true)
				.leftOuterJoin("listaClientevendedor.colaborador colaborador", true)
				.openParentheses()
					.where("listaClientevendedor is null").or()
					.where("colaborador.cdpessoa = ?", usuario.getCdpessoa())
				.closeParentheses();
		}
				
		query.orderBy("cliente.nome");
		
		return query.list();
	}
	
	/**
	 * M�todo que busca os cliente ativos e apenas daquele usu�rio
	 * Refatorado para que as consultas sejam nativas, por quest�es de desempenho
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 * @author Igor Silv�rio Costa
	 */
	public List<Cliente> findClienteAtivosNomeRazaosocialTelefoneEClienteDoUsuario(String q, String whereInEmpresa) {
		/**
		 * Metodo refatorado para executar queries nativas, por quest�o de desempenho
		 */
		Usuario usuario =  UsuarioService.getInstance().findForVerificarRestricaoClientevendedor(SinedUtil.getUsuarioLogado());
			boolean hasRestricaoClienteVendedor = (usuario != null && usuario.getCdpessoa() != null && usuario.getRestricaoclientevendedor() != null && usuario.getRestricaoclientevendedor());
			String sql = "";
			Object[] parametros;
			q = Util.strings.tiraAcento(q.toUpperCase());
			if(!hasRestricaoClienteVendedor){
				sql = " Select * from (" +
				" SELECT cliente.cdpessoa, pessoa.nome, cliente.razaosocial, pessoa.cnpj, pessoa.cpf, cliente.identificador, listatelefone.telefone" + 
				" FROM Cliente cliente " + 
				" INNER JOIN Pessoa pessoa ON cliente.cdpessoa=pessoa.cdpessoa " + 
				" LEFT OUTER JOIN Telefone listatelefone ON cliente.cdpessoa=listatelefone.cdpessoa " + 
				" WHERE upper(retira_acento(pessoa.nome)) LIKE ('%'||?||'%') " +  
				" AND cliente.ativo=TRUE " + 
	//			" AND listatelefone.cdtelefonetipo = ?" +
				getWhereClienteempresa("cliente", true, whereInEmpresa) +
				       
				" union " + 
		
				" SELECT cliente.cdpessoa, pessoa.nome, cliente.razaosocial, pessoa.cnpj, pessoa.cpf, cliente.identificador, listatelefone.telefone" + 
				" FROM Cliente cliente " + 
				" INNER JOIN Pessoa pessoa ON cliente.cdpessoa=pessoa.cdpessoa " + 
				" LEFT OUTER JOIN Telefone listatelefone ON cliente.cdpessoa=listatelefone.cdpessoa " + 
				" WHERE (upper(retira_acento(cliente.identificador)) LIKE ('%'||?||'%') " + 
				" or upper(retira_acento(cliente.razaosocial)) LIKE ('%'||?||'%')) " + 
				" AND cliente.ativo=TRUE " + 
	//			" AND listatelefone.cdtelefonetipo = ?" +
				getWhereClienteempresa("cliente", true, whereInEmpresa) +
				
				" union " + 
		
				" SELECT cliente.cdpessoa, pessoa.nome, cliente.razaosocial, pessoa.cnpj, pessoa.cpf, cliente.identificador, listatelefone.telefone" + 
				" FROM Cliente cliente " + 
				" INNER JOIN Pessoa pessoa ON cliente.cdpessoa=pessoa.cdpessoa " + 
				" LEFT OUTER JOIN Telefone listatelefone ON cliente.cdpessoa=listatelefone.cdpessoa  " +  
				" WHERE upper(retira_acento(listatelefone.telefone)) LIKE ('%'||?||'%') " + 
				" AND cliente.ativo=TRUE " +
				getWhereClienteempresa("cliente", true, whereInEmpresa) +
				" ) q" +
	//			" AND listatelefone.cdtelefonetipo = ?" + 
				       
				" ORDER BY 2, 1 " + 
				" LIMIT 90";
				parametros = new Object[]{q, q, q, q};
				parametros = new Object[]{q, q, q, q};
				
			}else{
				
				sql = " SELECT * FROM (" +
				" SELECT cliente.cdpessoa, pessoa.nome, cliente.razaosocial, pessoa.cnpj, pessoa.cpf, cliente.identificador, listatelefone.telefone" + 
				" FROM Cliente cliente " + 
				" INNER JOIN Pessoa pessoa ON cliente.cdpessoa=pessoa.cdpessoa " + 
				" LEFT OUTER JOIN Telefone listatelefone ON cliente.cdpessoa=listatelefone.cdpessoa " + 
				" LEFT OUTER JOIN clientevendedor listaClientevendedor on listaClientevendedor.cdcliente=cliente.cdpessoa " +
				" WHERE cliente.cdpessoa in (" +
					" SELECT cliente.cdpessoa " + 
					" FROM Cliente cliente " + 
					" INNER JOIN Pessoa pessoa ON cliente.cdpessoa=pessoa.cdpessoa " + 
					" LEFT OUTER JOIN Telefone listatelefone ON cliente.cdpessoa=listatelefone.cdpessoa " + 
					" WHERE upper(retira_acento(pessoa.nome)) LIKE ('%'||?||'%') " +  
					" AND cliente.ativo=TRUE " + 
//					" AND listatelefone.cdtelefonetipo = ?" +
					getWhereClienteempresa("cliente", true, whereInEmpresa) +
					
					" union " + 
					" SELECT cliente.cdpessoa " + 
					" FROM Cliente cliente " + 
					" INNER JOIN Pessoa pessoa ON cliente.cdpessoa=pessoa.cdpessoa " + 
					" LEFT OUTER JOIN Telefone listatelefone ON cliente.cdpessoa=listatelefone.cdpessoa " + 
					" WHERE (upper(retira_acento(cliente.identificador)) LIKE ('%'||?||'%') " + 
					" or upper(retira_acento(cliente.razaosocial)) LIKE ('%'||?||'%')) " + 
					" AND cliente.ativo=TRUE " +
//					" AND listatelefone.cdtelefonetipo = ?" +
					getWhereClienteempresa("cliente", true, whereInEmpresa) +
					
					" union " + 
					" SELECT cliente.cdpessoa " + 
					" FROM Cliente cliente " + 
					" INNER JOIN Pessoa pessoa ON cliente.cdpessoa=pessoa.cdpessoa " + 
					" LEFT OUTER JOIN Telefone listatelefone ON cliente.cdpessoa=listatelefone.cdpessoa " +  
					" WHERE upper(retira_acento(listatelefone.telefone)) LIKE ('%'||?||'%') " + 
					" AND cliente.ativo=TRUE " +
//					" AND listatelefone.cdtelefonetipo = ?" +
					getWhereClienteempresa("cliente", true, whereInEmpresa) +
				") AND ( listaClientevendedor.cdcolaborador is null or listaClientevendedor.cdcolaborador = ?)" +
				") q ORDER BY 2, 1 " +
				" LIMIT 90";
				
				parametros = new Object[]{q, q, q, q, usuario.getCdpessoa()};
			}
			
			SinedUtil.markAsReader();
			Cliente ultimoCliente = null;
			SqlRowSet rowSet = getJdbcTemplate().queryForRowSet(sql, parametros);
			List<Cliente> lista = new ArrayList<Cliente>();
			while(rowSet.next()){
				String cnpjStr = rowSet.getString("cnpj");
				String cpfStr = rowSet.getString("cpf");
				
				Cliente cliente = new Cliente();
				cliente.setCdpessoa(rowSet.getInt("cdpessoa"));
				
				if(ultimoCliente!=null && cliente.getCdpessoa().equals(ultimoCliente.getCdpessoa())){
					ultimoCliente.setTelefonesTransientForAutocomplete(ultimoCliente.getTelefonesTransientForAutocomplete() + " / " + rowSet.getString("telefone"));
					continue;
				}
				
				
				cliente.setNome(rowSet.getString("nome"));
				cliente.setRazaosocial(rowSet.getString("razaosocial"));
				if(cnpjStr!=null && Cnpj.cnpjValido(cnpjStr)) cliente.setCnpj(new Cnpj(cnpjStr));
				if(cpfStr!=null && Cpf.cpfValido(cpfStr)) cliente.setCpf(new Cpf(cpfStr));
				cliente.setIdentificador(rowSet.getString("identificador"));
				cliente.setTelefonesTransientForAutocomplete(rowSet.getString("telefone"));
				ultimoCliente = cliente;
				lista.add(cliente);
			}
			
			return lista;
	}
	
	/**
     *Busca os dados para o cache da tela de pedido de venda offline
     */
	public List<Cliente> findForPVOffline() {
		return newQueryBuilderWithFrom(Cliente.class)
		.select("cliente.cdpessoa, cliente.nome, listaEndereco.logradouro, listaEndereco.numero, listaEndereco.cdendereco, " +
				"listaEndereco.complemento, listaEndereco.bairro, municipio.nome, uf.nome, listaEndereco.cep, " +
				"listaEndereco.pontoreferencia, colaborador.cdpessoa, cliente.cpf, cliente.cnpj, cliente.razaosocial, " +
				"cliente.taxapedidovenda, listaPessoacategoria.cdpessoacategoria, categoria.cdcategoria ")
		.leftOuterJoin("cliente.listaEndereco listaEndereco")
		.leftOuterJoin("listaEndereco.municipio municipio")
		.leftOuterJoin("municipio.uf uf")
		.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
		.leftOuterJoin("listaClientevendedor.colaborador colaborador")
		.leftOuterJoin("cliente.listaPessoacategoria listaPessoacategoria")
		.leftOuterJoin("listaPessoacategoria.categoria categoria")
		.where("cliente.ativo = true")
		.list();
	}
	
	public List<Cliente> findForAndroid(String whereIn) {
		return newQueryBuilderWithFrom(Cliente.class)
		.select("cliente.cdpessoa, cliente.nome, cliente.email, cliente.creditolimitecompra, cliente.ativo, " +
				"listaEndereco.logradouro, listaEndereco.numero, listaEndereco.cdendereco, " +
				"listaEndereco.complemento, listaEndereco.bairro, municipio.nome, uf.nome, listaEndereco.cep, " +
				"listaEndereco.pontoreferencia, colaborador.cdpessoa, cliente.cpf, cliente.cnpj, cliente.razaosocial, cliente.tipopessoa, " +
				"cliente.taxapedidovenda, listaPessoacategoria.cdpessoacategoria, categoria.cdcategoria," +
				"listaTelefone.cdtelefone, listaTelefone.telefone, telefonetipo.cdtelefonetipo, telefonetipo.nome ")
		.leftOuterJoin("cliente.listaEndereco listaEndereco")
		.leftOuterJoin("listaEndereco.municipio municipio")
		.leftOuterJoin("municipio.uf uf")
		.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
		.leftOuterJoin("listaClientevendedor.colaborador colaborador")
		.leftOuterJoin("cliente.listaPessoacategoria listaPessoacategoria")
		.leftOuterJoin("listaPessoacategoria.categoria categoria")
		.leftOuterJoin("cliente.listaTelefone listaTelefone")
		.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
		.whereIn("cliente.cdpessoa", whereIn)
		.list();
	}
	
	/**
	 * M�todo que busca os vendedores do cliente
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public Cliente findVendedores(Cliente cliente) {
		if(cliente == null || cliente.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				.select("cliente.cdpessoa, listaClientevendedor.cdclientevendedor, colaborador.cdpessoa ")
				.join("cliente.listaClientevendedor listaClientevendedor")
				.join("listaClientevendedor.colaborador colaborador")
				.where("cliente = ?", cliente)
				.unique();
	}
	public List<Cliente> findByWherein(String whereIn) {
		return querySined()
			.select("cliente.cdpessoa, cliente.razaosocial, cliente.nome, cliente.cnpj, cliente.cpf")
			.whereIn("cliente.cdpessoa", whereIn)
			.list();
	}
	
	/**
	 * Busca informa��es de cliente para a tela de Ordem de servi�o
	 *
	 * @param cliente
	 * @return
	 * @since 22/06/2012
	 * @author Rodrigo Freitas
	 */
	public Cliente loadForRequisicaoInfo(Cliente cliente) {
		return querySined()
				.leftOuterJoinFetch("cliente.listaTelefone listaTelefone")
				.leftOuterJoinFetch("listaTelefone.telefonetipo telefonetipo")
				.leftOuterJoinFetch("cliente.listaPessoacategoria listaPessoacategoria")
				.leftOuterJoinFetch("listaPessoacategoria.categoria categoria")
				.leftOuterJoinFetch("cliente.listaClienteSegmento listaClienteSegmento")
				.leftOuterJoinFetch("listaClienteSegmento.segmento segmento")
				.where("cliente = ?", cliente)
				.unique();
	}
	
	/**
	 * Atualiza o campo de contabilidade centralizada para todas as filiais (Pelo prefixo do cnpj)
	 *
	 * @return void
	 * @since 28/06/2012
	 * @author Marden Silva
	 */
	public void updateContabilidadeCentralizada(Cliente cliente){
		if (cliente != null && cliente.getCnpj() != null && cliente.getCdpessoa() != null){
			String cnpj = cliente.getCnpj().getValue();
			if (cnpj.length() > 7){
				cnpj = cnpj.substring(0, 8);
				boolean contabilidadecentralizada = cliente.getContabilidadecentralizada() != null ? cliente.getContabilidadecentralizada() : false;
				getJdbcTemplate().update("UPDATE CLIENTE SET contabilidadecentralizada = ? " +
									     " WHERE CDPESSOA IN (SELECT p.cdpessoa FROM PESSOA p " + 
									     "                     WHERE p.cnpj LIKE ? || '%' AND " + 
									     "                           p.cdpessoa <> ?);",
									     new Object[]{contabilidadecentralizada, cnpj, cliente.getCdpessoa()});
			}
		}
	}
	
	/**
	 * Busca informa��es de cliente para o c�lculo de tributa��o (Valor acumulado de tributos cumulativos)
	 *
	 * @return Cliente
	 * @param cliente
	 * @since 28/06/2012
	 * @author Marden Silva
	 */
	public Cliente loadForCalculoTributacao(Cliente cliente){
		return querySined()
				.select("cliente.cdpessoa, cliente.cnpj, cliente.contabilidadecentralizada")
				.where("cliente = ?", cliente)
				.unique();
	}
	
	/**
	 * M�todo que carrega o cliente com a taxa de pedido de venda e o limite de cr�dito
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public Cliente carregarTaxapedidovendaCreditolimitecompraByCliente(Cliente cliente) {
		if(cliente == null || cliente.getCdpessoa() == null)
			throw new SinedException("Cliente n�o pode ser nulo.");
		
		return querySined()
				.select("cliente.cdpessoa, cliente.taxapedidovenda, cliente.creditolimitecompra")
				.where("cliente = ?", cliente)
				.unique();
	}
	
	public Cliente carregarCreditolimitecompraByCnpj(VerificaPendenciaFinanceira bean) {
		return querySined()
				
				.select("cliente.cdpessoa, cliente.creditolimitecompra, cliente.email, " +
						"contato.receberboleto, contato.responsavelos, contato.emailcontato, " +
						"categoria.categoriaClienteLogin, cliente.login, cliente.senha")
				.leftOuterJoin("cliente.listaPessoacategoria listaPessoacategoria")
				.leftOuterJoin("listaPessoacategoria.categoria categoria")
				.leftOuterJoin("cliente.listaContato listaContato")
				.leftOuterJoin("listaContato.contato contato")
				.where("cliente.cnpj = ?", bean.getCnpj() != null ? new Cnpj(bean.getCnpj()) : null)
				.where("cliente.cpf = ?", bean.getCpf() != null ? new Cpf(bean.getCpf()) : null)
				.unique();
	}
	
	public void updateCrt(Cliente cliente) {
		if(cliente == null || cliente.getCdpessoa() == null){
			throw new SinedException("Cliente n�o pode ser nulo.");
		}
		getJdbcTemplate().update("update Cliente set crt = ? where cdpessoa = ?", new Object[]{cliente.getCrt().getValue(), cliente.getCdpessoa()});
	}
	
	/**
	 * Busca os clientes com a mesma matriz de CNPJ.
	 *
	 * @param cnpj
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/11/2012
	 */
	@SuppressWarnings("unchecked")
	public List<ClienteVO> findByMatrizCNPJ(Cnpj cnpj) {
		if(cnpj == null){
			throw new SinedException("CNPJ n�o pode ser nulo.");
		}
		
		String sql = "select p.cdpessoa, p.nome " +
						"from pessoa p " +
						"where p.cnpj is not null " +
						"and p.cnpj <> '' " +
						"and substr(p.cnpj, 0, 9) like '" + cnpj.getValue().substring(0, 8) + "' " +
						"order by p.nome ";
		
		SinedUtil.markAsReader();
		List<ClienteVO> lista = getJdbcTemplate().query(sql, 
			new RowMapper() {
				public ClienteVO mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new ClienteVO(rs.getInt("cdpessoa"), rs.getString("nome"));
				}
			});
		
		return lista;
	}
	
	/**
	 * Carrega o Cliente com a conta gerencial
	 *
	 * @param pessoa
	 * @return
	 * @author Luiz Fernando
	 */
	public Cliente loadForMovimentacaocontabil(Pessoa pessoa) {
		if(pessoa == null || pessoa.getCdpessoa() == null)
			throw new SinedException("Pessoa n�o pode ser nula.");
		
		return querySined()
				.select("cliente.cdpessoa, contacontabil.cdcontacontabil")
				.leftOuterJoin("cliente.contaContabil contacontabil")
				.where("cliente.cdpessoa = ?", pessoa.getCdpessoa())
				.unique();
	}
	
	/**
	 * Verifica se h� cliente com o identificador e nome igual.
	 *
	 * @param identificador
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/02/2013
	 */
	public boolean haveClienteIdentificadorNome(String identificador, String nome) {
		SinedUtil.markAsReader();
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Cliente.class)
				.where("cliente.identificador = ?", identificador)
				.where("cliente.nome = ?", nome)
				.unique() > 0;
	}
	
	public List<Cliente> findForEnvioEmailCobranca(Contagerencial contagerencialMensalidade, Projeto projeto, SituacaoContrato[] situacoes) {
		
		QueryBuilder<Cliente> query = querySined()
										.setUseWhereEmpresa(false)
										.setUseWhereProjeto(false)
										.select("cliente.cdpessoa, cliente.nome, cliente.creditolimitecompra, cliente.email, cliente.cnpj, " +
												"cliente.cpf, contato.receberboleto, contato.responsavelos, contato.emailcontato")
										.leftOuterJoin("cliente.listaContato contato")
										.join("cliente.listaContrato contrato")
										.join("contrato.aux_contrato aux_contrato")
										.join("contrato.rateio rateio")
										.join("rateio.listaRateioitem rateioitem")
										.join("rateioitem.projeto projeto")
										.join("rateioitem.contagerencial contagerencial")
										.where("projeto = ?", projeto)
										.where("contagerencial = ?", contagerencialMensalidade);
		
		query.openParentheses();
		for (int i = 0; i < situacoes.length; i++) {
			query.where("aux_contrato.situacao = ?", situacoes[i]).or();
		}
		query.closeParentheses();
		
		return query.list();
	}
	
	public List<Cliente> findForIntegracaoEmporium(Integer idCliente) {
		return querySined()
					.select("cliente.cdpessoa, cliente.nome, cliente.razaosocial, cliente.email, " +
							"cliente.tipopessoa, cliente.cpf, cliente.cnpj, cliente.rg, cliente.inscricaoestadual, " +
							"endereco.cdendereco, endereco.logradouro, endereco.bairro, endereco.cep, endereco.numero, " +
							"endereco.complemento, endereco.pontoreferencia, " +
							"enderecotipo.cdenderecotipo, municipio.cdmunicipio, municipio.nome, uf.sigla")
					.leftOuterJoin("cliente.listaEndereco endereco")
					.leftOuterJoin("endereco.enderecotipo enderecotipo")
					.leftOuterJoin("endereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.where("cliente.cdpessoa = ?", idCliente)
					.list();
	}
	
	/**
	 * M�todo que busca os clientes da listgem de venda
	 *
	 * @param q
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Cliente> findAutocompleteRestricaoClienteVendedor(String q,	Colaborador colaborador,  boolean ignoreParametterUseFunctionOnAutocomplete) {
		if(ParametrogeralService.getInstance().getBoolean("useFunctionOnAutocomplete") && !ignoreParametterUseFunctionOnAutocomplete){
			return findAutocompleteSQL(q, false, false, false, colaborador, false, null);
		}else{
			QueryBuilder<Cliente> query = querySined()
					.setUseWhereClienteEmpresa(true)
					.autocomplete()
					.select("cliente.cdpessoa, cliente.identificador, cliente.nome, cliente.razaosocial, cliente.cnpj, cliente.cpf ")
					.openParentheses()
						.whereLikeIgnoreAll("cliente.nome", q)
						.or()
						.whereLikeIgnoreAll("cliente.razaosocial", q)
						.or()
						.whereLikeIgnoreAll("cliente.identificador", q)
					.closeParentheses();
			
			if(colaborador != null){
				query
				.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor", true)
				.leftOuterJoin("listaClientevendedor.colaborador colaborador", true)
				.openParentheses()
					.where("colaborador = ?", colaborador)
					.or()
					.where("listaClientevendedor is null")
				.closeParentheses();
			}
			return query
					.orderBy("cliente.nome")
					.list();
		}
	}
	
	public Cliente findForContratoRtf(Cliente cliente) {
		if(cliente == null || cliente.getCdpessoa() == null)
			throw new SinedException("Cliente n�o pode ser nulo.");
		
		return querySined()
				.leftOuterJoinFetch("cliente.estadocivil estadocivil")
				.leftOuterJoinFetch("cliente.clienteprofissao clienteprofissao")
				.leftOuterJoinFetch("cliente.listaEndereco listaEndereco")
				.leftOuterJoinFetch("listaEndereco.enderecotipo enderecotipo")
				.leftOuterJoinFetch("listaEndereco.municipio municipio")
				.leftOuterJoinFetch("municipio.uf uf")
				
				.leftOuterJoinFetch("cliente.listaTelefone telefone")
				.leftOuterJoinFetch("telefone.telefonetipo telefonetipo")
				.where("cliente = ?", cliente)
				.unique();
	}
	
	public Cliente loadForTributacao(Cliente cliente) {
		if(cliente == null || cliente.getCdpessoa() == null)
			throw new SinedException("Cliente n�o pode ser nulo.");
		
		return querySined()
				.select("cliente.cdpessoa, cliente.cpf, cliente.cnpj, cliente.nome, cliente.razaosocial"
					  + ", codigotributacao.cdcodigotributacao, codigotributacao.codigo, cliente.crt, cliente.incidiriss, cliente.tipopessoa "
					  + ", cliente.contribuinteICMS, cliente.contribuinteicmstipo, cliente.operacaonfe")
				.leftOuterJoin("cliente.codigotributacao codigotributacao")
				.where("cliente = ?", cliente)
				.unique();
	}
	
	public Cliente loadForProcessFromPainelInteracao(Cliente cliente) {
		if(cliente == null || cliente.getCdpessoa() == null)
			throw new SinedException("Cliente n�o pode ser nulo.");
		return querySined()
				.select("cliente.cdpessoa, cliente.cpf, cliente.cnpj, cliente.identificador, cliente.nome, cliente.razaosocial"
						+ ", telefone.cdtelefone, telefone.telefone")
				.leftOuterJoin("cliente.listaTelefone telefone")
				.where("cliente = ?", cliente)
				.unique();
	}
	
	public List<Cliente> findClientesComReferencia(String query) {
		return querySined()
				.setUseWhereClienteEmpresa(true)
				.autocomplete()
				.select("cliente.cdpessoa, cliente.nome")
				.whereLikeIgnoreAll("cliente.nome", query)
				.where("exists (select co.cdpessoa from Clienterelacao cr join cr.clienteoutro co where co.cdpessoa = cliente.cdpessoa )")
				.list();
	}
	
	/**
	 * M�todo que busca os clientes a partir dos campos pr�-definidos: Nome, Identificador, Cpf ou Cnpj
	 *
	 * @param q
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Cliente> findAllAutocompleteByNomeIdCpfCnpj(String q, String whereInEmpresa) {
		if(ParametrogeralService.getInstance().getBoolean("useFunctionOnAutocomplete")){
			return findAutocompleteSQL(q, false, true, true, null, false, whereInEmpresa);
		}else{
			QueryBuilder<Cliente> query = querySined()
					.setUseWhereClienteEmpresa(true)
					.autocomplete()
					.select("cliente.cdpessoa, cliente.identificador, cliente.nome, cliente.cnpj, cliente.cpf ")
					.openParentheses()
						.whereLikeIgnoreAll("cliente.nome", q)
						.or()
						.whereLikeIgnoreAll("cliente.identificador", q)
						.or()
						.whereLikeIgnoreAll("cliente.cnpj", q)
						.or()
						.whereLikeIgnoreAll("cliente.cpf", q)
					.closeParentheses();
			
			addWhereInClienteByEmpresa(query, whereInEmpresa);
					
			return query
					.orderBy("cliente.nome")
					.list();
		}
	}
	
	/**
	 * M�todo que busca os clientes a partir de todos os campos pr�-definidos
	 *
	 * @param q
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Cliente> findAllAutocompleteByAllFields(String q, String whereInEmpresa) {
		if(ParametrogeralService.getInstance().getBoolean("useFunctionOnAutocomplete")){
			return findAutocompleteSQL(q, false, true, true, null, false, whereInEmpresa);
		}else{
			QueryBuilder<Cliente> query =  querySined()
					.setUseWhereClienteEmpresa(true)
					.autocomplete()
					.select("cliente.cdpessoa, cliente.identificador, cliente.nome, cliente.razaosocial, cliente.cnpj, cliente.cpf ")
					.openParentheses()
						.whereLikeIgnoreAll("cliente.nome", q)
						.or()
						.whereLikeIgnoreAll("cliente.razaosocial", q)
						.or()
						.whereLikeIgnoreAll("cliente.identificador", q)
						.or()
						.whereLikeIgnoreAll("cliente.cnpj", q)
						.or()
						.whereLikeIgnoreAll("cliente.cpf", q)
					.closeParentheses();
			
			addWhereInClienteByEmpresa(query, whereInEmpresa);
					
			return query.orderBy("cliente.nome")
					.list();
		}
	}
	
	public List<Cliente> carregarDadosRelatorioAvaliativo(String whereIn){
		return  querySined()
					
					.setUseWhereClienteEmpresa(true)
					.select("cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj, " +
					"listaTelefone.telefone, listaEndereco.cep, listaEndereco.logradouro, listaEndereco.numero, listaEndereco.complemento, listaEndereco.bairro, municipio.nome," +
					"listaQuestionario.cdpessoaquestionario, listaQuestionario.pontuacao, listaQuestionario.situacao, listaQuestionario.cdusuarioaltera, listaQuestionario.dtaltera," +
					"listaPessoaQuestionarioQuestao.cdpessoaquestionarioquestao, listaPessoaQuestionarioQuestao.pontuacao, listaPessoaQuestionarioQuestao.sim, listaPessoaQuestionarioQuestao.resposta, " +
					"questionarioquestao.cdquestionarioquestao, questionarioquestao.questao, questionarioquestao.pontuacaosim, questionarioquestao.pontuacaonao, uf.sigla, " +
					"arquivo.cdarquivo, questionario.avaliarrecebimento, questionarioquestao.pontuacaoquestao")
					.leftOuterJoin("cliente.listaTelefone listaTelefone")
					.leftOuterJoin("cliente.listaEndereco listaEndereco")
					.leftOuterJoin("listaEndereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("cliente.listaQuestionario listaQuestionario")
					.leftOuterJoin("listaQuestionario.listaPessoaQuestionarioQuestao listaPessoaQuestionarioQuestao")
					.leftOuterJoin("listaPessoaQuestionarioQuestao.questionarioquestao questionarioquestao")
					.leftOuterJoin("listaQuestionario.questionario questionario")
					.leftOuterJoin("questionario.arquivo arquivo")
					.whereIn("cliente.cdpessoa", whereIn)
					.orderBy("questionarioquestao.questao")
					.list();
	}

	/**
	* M�todo que carrega o cliente para a emiss�o de fatura de loca��o
	*
	* @param whereIn
	* @return
	* @since 11/07/2014
	* @author Luiz Fernando
	*/
	public List<Cliente> findForFaturalocacao(String whereIn) {
		if (whereIn == null || whereIn.equals(""))
			return new ArrayList<Cliente>();
		
		return querySined()
				.select("cliente.cdpessoa, cliente.nome, cliente.razaosocial, cliente.inscricaoestadual, cliente.tipopessoa, " + 
						"cliente.cpf, cliente.cnpj, " +
						"listaEndereco.cdendereco, listaEndereco.logradouro, listaEndereco.bairro, listaEndereco.cep, " +
						"listaEndereco.caixapostal, listaEndereco.numero, listaEndereco.complemento, listaEndereco.pontoreferencia, " +
						"enderecotipo.cdenderecotipo, enderecotipo.nome, " + 
						"municipio.cdmunicipio, municipio.nome, " +
						"uf.cduf, uf.nome, uf.sigla, " +
						"listaTelefone.cdtelefone, listaTelefone.telefone, " +
						"telefonetipo.cdtelefonetipo, telefonetipo.nome")
				.leftOuterJoin("cliente.listaTelefone listaTelefone")
				.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
				.leftOuterJoin("cliente.listaEndereco listaEndereco")
				.leftOuterJoin("listaEndereco.enderecotipo enderecotipo")
				.leftOuterJoin("listaEndereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.whereIn("cliente.cdpessoa", whereIn)
				.list();
	}

	/**
	* M�todo que carrega os dados dos clientes para a emiss�o da ficha
	*
	* @param whereIn
	* @return
	* @since 17/07/2014
	* @author Lucas Costa
	*/
	public List<Cliente> findForEmitirFichaCliente(String whereIn) throws Exception{
		if(whereIn == null || whereIn.trim().isEmpty()){
			throw new Exception("Wherein n�o pode ser nulo.");
		}
		
		return querySined()
				
			.leftOuterJoinFetch("cliente.listaDadobancario dadobancario")
			.leftOuterJoinFetch("dadobancario.banco banco")
			.leftOuterJoinFetch("cliente.listaTelefone listaTelefone")
			.leftOuterJoinFetch("listaTelefone.telefonetipo telefonetipo")
			.leftOuterJoinFetch("cliente.listaEndereco listaEndereco")
			.leftOuterJoinFetch("listaEndereco.enderecotipo enderecotipo")
			.leftOuterJoinFetch("listaEndereco.municipio municipio")
			.leftOuterJoinFetch("municipio.uf uf")
			.leftOuterJoinFetch("listaEndereco.pais pais")
			.leftOuterJoinFetch("cliente.listaRestricao listaRestricao")
			.leftOuterJoinFetch("cliente.listaPessoacategoria listaPessoacategoria")
			.leftOuterJoinFetch("listaPessoacategoria.categoria categoria")
			.leftOuterJoinFetch("cliente.listaClientevendedor listaClientevendedor")
			.leftOuterJoinFetch("listaClientevendedor.colaborador colaborador")
			.leftOuterJoinFetch("cliente.listaValecompra listaValecompra")
			.leftOuterJoinFetch("cliente.ufinscricaoestadual ufinscricaoestadual")
			.leftOuterJoinFetch("cliente.operacaocontabil operacaocontabil")
			.leftOuterJoinFetch("cliente.grupotributacao grupotributacao")
			.leftOuterJoinFetch("cliente.contaContabil contacontabil")
			.leftOuterJoinFetch("cliente.pessoatratamento pessoatratamento")
			.leftOuterJoinFetch("cliente.estadocivil estadocivil")
			.leftOuterJoinFetch("cliente.profissaoconjuge profissaoconjuge")
			.leftOuterJoinFetch("cliente.profissaopai profissaopai")
			.leftOuterJoinFetch("cliente.profissaomae profissaomae")
			.leftOuterJoinFetch("cliente.tipoidentidade tipoidentidade")
			.leftOuterJoinFetch("cliente.sexo sexo")
			.leftOuterJoinFetch("cliente.municipionaturalidade municipionaturalidade")
			.leftOuterJoinFetch("municipionaturalidade.uf municipionaturalidade_uf")
			.leftOuterJoinFetch("cliente.grauinstrucao grauinstrucao")
			.leftOuterJoinFetch("cliente.responsavelFinanceiro responsavelFinanceiro")
			.leftOuterJoinFetch("cliente.clienteindicacao clienteindicacao")
			.leftOuterJoinFetch("cliente.codigotributacao codigotributacao")
			.leftOuterJoinFetch("cliente.clienteprofissao clienteprofissao")
			.leftOuterJoinFetch("cliente.listaClienterelacao listaClienterelacao")
			.leftOuterJoinFetch("listaClienterelacao.clienterelacaotipo clienterelacaotipo")
			.leftOuterJoinFetch("listaClienterelacao.clienteoutro clienteoutro")
			.whereIn("cliente.cdpessoa", whereIn)
			.list();

	}
	
	/**
	 * Carrega clientes atrav�s do cdendereco
	 * 
	 * @param cliente
	 * @return
	 * @author Jo�o Vitor
	 * @throws Exception 
	 */
	public List<Cliente> findClienteToEtiquetaBean(String whereIn) throws Exception {
		if(whereIn == null || whereIn.trim().isEmpty()){
			throw new Exception("WhereIn n�o pode ser nulo.");
		}
		return querySined()
				
		.select("cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj, cliente.dtnascimento, cliente.identificador, " +
		"cliente.estadocivil, cliente.clienteprofissao, cliente.sexo, telefonetipo.nome, listaTelefone.telefone ")
		.leftOuterJoin("cliente.estadocivil estadocivil")
		.leftOuterJoin("cliente.clienteprofissao profissao")
		.leftOuterJoin("cliente.sexo sexo")
		.leftOuterJoin("cliente.listaTelefone listaTelefone")
		.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
		.whereIn("cliente.cdpessoa", whereIn)
		.orderBy("cliente.nome")
		.list();
	}
	
	public void updateClienteAndroid(ClienteRESTWSBean command) {
		if(command != null && command.getCdpessoa() != null){
			getJdbcTemplate().update("UPDATE CLIENTE SET " +
					 " razaosocial = ?, " +
					 " inscricaoestadual = ? " +
				     " WHERE CDPESSOA = ?;",
				     new Object[]{command.getRazaosocial(),
								  command.getInscricaoestadual(),
								  command.getCdpessoa()});
			getJdbcTemplate().update("UPDATE PESSOA SET nome = ?, " +
					 " tipopessoa = ?, " +
					 " cpf = ?, " +
					 " cnpj = ?, " +
					 " email = ?, " +
					 " observacao = ? " +
				     " WHERE CDPESSOA = ?;",
				     new Object[]{command.getNome(), 
								  command.getTipopessoa(),
								  command.getCpf() != null ? br.com.linkcom.neo.util.StringUtils.soNumero(command.getCpf()) : null,
								  command.getCnpj() != null ? br.com.linkcom.neo.util.StringUtils.soNumero(command.getCnpj()) : null,
								  command.getEmail(),
								  command.getObservacao(),
								  command.getCdpessoa()});
			
		}
	}
	
	public List<Cliente> findForControleInteracao(ControleInteracaoBean bean){
		if(bean == null)
			throw new SinedException("Os dados da intera��o n�o podem ser nulos.");
		
		QueryBuilder<Cliente> query = querySined();
		
		query
			.select("cliente.cdpessoa, cliente.nome, listaTelefone.cdtelefone, listaTelefone.telefone, telefonetipo.cdtelefonetipo, telefonetipo.nome, " +
					"contato.cdpessoa, contato.nome, municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla, categoria.cdcategoria, categoria.nome, " +
					"contatotipo.cdcontatotipo, contatotipo.nome, contato.ativo")
			.leftOuterJoin("cliente.listaContato listaContato")
			.leftOuterJoin("listaContato.contato contato")
			.leftOuterJoin("contato.contatotipo contatotipo")
			.leftOuterJoin("cliente.listaTelefone listaTelefone")
			.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
			.leftOuterJoin("cliente.listaPessoacategoria listaPessoacategoria")
			.leftOuterJoin("listaPessoacategoria.categoria categoria")
			.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
			.where("cliente.ativo = ?", Boolean.TRUE)
			;
			
			if(bean.getCliente() != null && bean.getCliente().getCdpessoa()!= null){
				query.where("cliente = ?", bean.getCliente());
			}
			
			if(bean.getColaborador() != null){
				query.where("listaClientevendedor.colaborador = ?", bean.getColaborador());
			}
			
			if(bean.getCategoria() != null){
				query.where("categoria = ?", bean.getCategoria());
			}
			
			if(bean.getListaSituacaohistorico() != null && !bean.getListaSituacaohistorico().isEmpty()){
				String whereIn = CollectionsUtil.listAndConcatenate(bean.getListaSituacaohistorico(), "cdsituacaohistorico", ",");
				query.where("exists (select 1 from Clientehistorico ch join ch.situacaohistorico sh where ch.cliente.cdpessoa = cliente.cdpessoa and sh.cdsituacaohistorico in (" + whereIn + ") ) ");
			}
			
			if(bean.getUf() != null){
				query.join("cliente.listaEndereco listaEndereco")
					.join("listaEndereco.municipio municipio")
					.join("municipio.uf uf")
					.where("uf = ?", bean.getUf());
				if(bean.getMunicipio() != null){
					query.where("municipio = ?", bean.getMunicipio());
				} 
			} else{
				query.leftOuterJoin("cliente.listaEndereco listaEndereco")
					.leftOuterJoin("listaEndereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf");
			}
			
			if(bean.getOrdenacao() != null){
				query.orderBy(bean.getOrdenacao().getCampo());					
			} else{
				query.orderBy("cliente.nome");
			}
		return query.list();
	}
	
	public List<Cliente> findHistoricoForControleInteracao(String whereIn, String whereInHistoricosituacao, Date dataDe, Date dataAte){
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
			.select("cliente.cdpessoa, " +
					"listClientehistorico.cdclientehistorico, listClientehistorico.dtaltera, situacaohistorico.cdsituacaohistorico, situacaohistorico.descricao, " +
					"atividadetipo.cdatividadetipo, atividadetipo.naoexibirnopainel ")
			.join("cliente.listClientehistorico listClientehistorico")
			.leftOuterJoin("listClientehistorico.situacaohistorico situacaohistorico")
			.leftOuterJoin("listClientehistorico.atividadetipo atividadetipo")
			.whereIn("cliente.cdpessoa", whereIn)
			.whereIn("situacaohistorico.cdsituacaohistorico", whereInHistoricosituacao)
			.where("listClientehistorico.dtaltera >= ?", dataDe != null ? SinedDateUtils.dateToTimestampInicioDia(dataDe) : null)
			.where("listClientehistorico.dtaltera <= ?", dataAte != null ?SinedDateUtils.dateToTimestampFinalDia(dataAte) : null)
			.orderBy("listClientehistorico.dtaltera asc")
			.list();
	}
	
	public Cliente loadForSincronizacaoAA(Cliente cliente) {
		return querySined()
		.select("cliente.cdpessoa, cliente.nome, cliente.razaosocial, cliente.cnpj, cliente.site, cliente.email, " +
					"contrato.cdcontrato, contrato.identificador, contrato.descricao, contrato.dtinicio, contrato.dtfim, contratotipo.faturamentowebservice," +
					"contrato.vendedortipo, responsavel.email, listaClienteSegmento.cdclientesegmento, listaClienteSegmento.principal, segmento.cdsegmento, segmento.nome, " +
						
						"representante.nome, representante.cnpj, representante.site, representante.email, " +
							"telTipoRepresentante.cdtelefonetipo, telRepresentante.telefone, telTipoRepresentante.nome, " +
							"endTipoRepresentante.cdenderecotipo, endRepresentante.cep, endRepresentante.logradouro, endRepresentante.numero, endRepresentante.complemento, " +
							"endRepresentante.bairro, ufEndRepresentante.sigla, municipioEndRepresentante.nome, endRepresentante.caixapostal, " + 
					
					"telTipo.cdtelefonetipo, telTipo.nome, tel.telefone, " +
					"endTipo.cdenderecotipo, endereco.cep, endereco.logradouro, endereco.numero, endereco.complemento, " +
					"endereco.bairro, ufEnd.sigla, municipioEnd.nome, endereco.caixapostal, " +
					
					"contato.nome, contato.email, contato.observacao, contatotipo.nome, telContato.telefone, contatoTelTipo.cdtelefonetipo, contatoTelTipo.nome")
		.leftOuterJoin("cliente.listaContrato contrato")
		.leftOuterJoin("contrato.contratotipo contratotipo")
		
		//dados de cliente
		.leftOuterJoin("cliente.responsavelFinanceiro responsavel")
		.leftOuterJoin("cliente.listaTelefone tel")
		.leftOuterJoin("tel.telefonetipo telTipo")
		.leftOuterJoin("cliente.listaEndereco endereco")
		.leftOuterJoin("endereco.enderecotipo endTipo")
		.leftOuterJoin("endereco.municipio municipioEnd")
		.leftOuterJoin("municipioEnd.uf ufEnd")
		.leftOuterJoin("cliente.listaClienteSegmento listaClienteSegmento")
		.leftOuterJoin("listaClienteSegmento.segmento segmento")
		
		//dados de contato
//		.leftOuterJoin("cliente.cliente cliente")
		.leftOuterJoin("cliente.listaContato listaContato")
		.leftOuterJoin("listaContato.contato contato")
		.leftOuterJoin("contato.contatotipo contatotipo")
		.leftOuterJoin("contato.listaTelefone telContato")
		.leftOuterJoin("telContato.telefonetipo contatoTelTipo")
		
		//dados de representante
		.leftOuterJoin("contrato.vendedor representante")
		.leftOuterJoin("representante.listaTelefone telRepresentante")
		.leftOuterJoin("telRepresentante.telefonetipo telTipoRepresentante")
		.leftOuterJoin("representante.listaEndereco endRepresentante")
		.leftOuterJoin("endRepresentante.enderecotipo endTipoRepresentante")
		.leftOuterJoin("endRepresentante.municipio municipioEndRepresentante")
		.leftOuterJoin("municipioEndRepresentante.uf ufEndRepresentante")
		.where("cliente = ?", cliente)
		.unique();
	}
	
	/**
	* M�todo que busca o cliente pelo identificador
	*
	* @param identificador
	* @return
	* @since 05/10/2015
	* @author Luiz Fernando
	*/
	public List<Cliente> findByIdentificador(String identificador){
		return querySined()
				.setUseWhereClienteEmpresa(true)
				.select("cliente.cdpessoa, cliente.nome, cliente.identificador")
				.where("cliente.ativo = true")
				.where("cliente.identificador = ?", identificador)
				.list();
	}
	
	/**
	 * M�todo que carega os dados de um Cliente para preenchimento de um OportunidadeRTF.
	 * @param cliente
	 * @return
	 * @author Matheus Santos
	 */
	public Cliente loadForMakeOportunidadeRTF(Cliente cliente){
		if(cliente==null || cliente.getCdpessoa()==null)
			throw new RuntimeException("O par�metro cliente n�o pode ser nulo.");
			
		return querySined()
					.select("cliente.cdpessoa, cliente.cpf, cliente.cnpj, " +
							"endereco.bairro, endereco.logradouro, endereco.numero, endereco.cep, municipio.nome, endereco.complemento, " +
							"enderecotipo.cdenderecotipo, uf.sigla, telefones.cdtelefone, telefones.telefone, telefonetipo.cdtelefonetipo, telefonetipo.nome ")
					.leftOuterJoin("cliente.listaEndereco endereco")
					.leftOuterJoin("endereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("endereco.enderecotipo enderecotipo")
					.leftOuterJoin("cliente.listaTelefone telefones")
					.leftOuterJoin("telefones.telefonetipo telefonetipo")
					.where("cliente = ?", cliente)
					.unique();
	}
	
	/**
	 * M�todo que busca os clientes a serem enviados para o W3Producao.
	 * @param whereIn
	 * @return
	 */
	public List<Cliente> findForW3Producao(String whereIn){
		return querySined()
					.select("cliente.cdpessoa, cliente.nome")
					.whereIn("cliente.cdpessoa", whereIn)
					.list();
	}
	
	public List<Cliente> listaTelefoneCliente(String whereIn) {
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("O par�metro whereIn n�o pode ser nulo.");
		}
		return querySined()
					.select("cliente.cdpessoa, telefones.cdtelefone, telefones.telefone, telefonetipo.cdtelefonetipo, telefonetipo.nome")
					.leftOuterJoin("cliente.listaTelefone telefones")
					.leftOuterJoin("telefones.telefonetipo telefonetipo")
					.whereIn("cliente.cdpessoa", whereIn)
					.list();
	}
	public List<Cliente> listaPessoaCategoria(String whereIn) {
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("O par�metro whereIn n�o pode ser nulo.");
		}
		return querySined()
					.select("cliente.cdpessoa, listaPessoacategoria.cdpessoacategoria, categoria.cdcategoria, categoria.nome")
					.leftOuterJoin("cliente.listaPessoacategoria listaPessoacategoria")
					.leftOuterJoin("listaPessoacategoria.categoria categoria")
					.whereIn("cliente.cdpessoa", whereIn)
					.list();
	}
	public List<Cliente> listaClienteHistorico(String whereIn) {
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("O par�metro whereIn n�o pode ser nulo.");
		}
		List<Empresa> empresas = empresaService.findByUsuario();
		String whereInEmpresas = CollectionsUtil.listAndConcatenate(empresas, "cdpessoa", ",");
		return querySined()
					.select("cliente.cdpessoa, historico.cdclientehistorico,historico.dtaltera, historico.observacao, historico.situacao," +
							"historico.cdusuarioaltera, usuarioaltera.cdpessoa, usuarioaltera.nome,atividadetipo.cdatividadetipo, atividadetipo.nome, " +
							"empresa.nome")
					.leftOuterJoin("cliente.listClientehistorico historico")
					.leftOuterJoin("historico.usuarioaltera usuarioaltera")
					.leftOuterJoin("historico.atividadetipo atividadetipo")
					.whereIn("cliente.cdpessoa", whereIn)
					.leftOuterJoin("historico.empresahistorico empresa")
					.openParentheses()
						.where("empresa.cdpessoa is null")
						.or()
						.whereIn("empresa.cdpessoa", whereInEmpresas)
					.closeParentheses()
					.list();
	}
	public List<Cliente> listaRequisicaoCliente(String whereIn) {
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("O par�metro whereIn n�o pode ser nulo.");
		}
		return querySined()
					.select("cliente.cdpessoa, requisicao.cdrequisicao, requisicao.descricao")
					.leftOuterJoin("cliente.listaClienterequisicao requisicao")
					.whereIn("cliente.cdpessoa", whereIn)
					.list();
	}
	
	/**
	 * Busca os clientes que foram alterados desde a data passada por par�metro para a integra��o via SOAP.
	 *
	 * @param dataReferencia
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/04/2016
	 */
	public List<Cliente> findForWSSOAP(Timestamp dataReferencia) {
		if(dataReferencia == null){
			throw new SinedException("O par�metro Data de Refer�ncia n�o pode ser nulo.");
		}
		
		return querySined()
					.select("cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj, " +
							"endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, endereco.cep, " +
							"enderecotipo.cdenderecotipo, enderecotipo.nome, " +
							"municipio.cdmunicipio, municipio.nome, " +
							"uf.cduf, uf.sigla, " +
							"telefone.cdtelefone, telefone.telefone, " +
							"telefonetipo.cdtelefonetipo, telefonetipo.nome, " +
							"clientedocumentotipo.cdclientedocumentotipo, documentotipo.cddocumentotipo, " +
							"clienteprazopagamento.cdclienteprazopagamento, prazopagamento.cdprazopagamento," +
							"listaClientevendedor.principal, agencia.cdpessoa, colaborador.cdpessoa")
					.leftOuterJoin("cliente.listaEndereco endereco")
					.leftOuterJoin("cliente.listaTelefone telefone")
					.leftOuterJoin("cliente.listDocumentoTipo clientedocumentotipo")
					.leftOuterJoin("cliente.listPrazoPagamento clienteprazopagamento")
					.leftOuterJoin("clientedocumentotipo.documentotipo documentotipo")
					.leftOuterJoin("clienteprazopagamento.prazopagamento prazopagamento")
					.leftOuterJoin("endereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("telefone.telefonetipo telefonetipo")
					.leftOuterJoin("endereco.enderecotipo enderecotipo")
					.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
					.leftOuterJoin("listaClientevendedor.agencia agencia")
					.leftOuterJoin("listaClientevendedor.colaborador colaborador")
					.where("cliente.dtaltera >= ?", dataReferencia)
					.list();
	}
	
	/**
	 * M�todo respons�vel por carregar dados b�sicos do cliente para cria��o de Oportunidade
	 * @param cliente
	 * @return
	 * @since 13/07/2016
	 * @author C�sar
	 */
	public Cliente findForOportunidade(Cliente cliente){
		if(cliente == null || cliente.getCdpessoa() == null){
			throw new SinedException("N�o � poss�vel realizar pesquisa sem cliente");
		}
		return querySined()
				.select("cliente.cdpessoa, cliente.nome, cliente.razaosocial, " +
						"cliente.cpf, cliente.cnpj")
				.where("cliente = ?", cliente)
				.unique();
	}
	
	/**
	 * For�a a sincroniza��o das tabelas do w3produ��o
	 *
	 * @param cdcliente
	 * @author Rodrigo Freitas
	 * @since 28/09/2016
	 */
	public void atualizaW3producao(Integer cdcliente) {
		getJdbcTemplate().execute("update cliente set ativo = ativo where cdpessoa = " + cdcliente);
	}

	public void updateContaContabil(Cliente cliente, ContaContabil contacontabil){
		getJdbcTemplate().update("update cliente set cdcontacontabil=? where cdpessoa=?", new Object[]{contacontabil.getCdcontacontabil(), cliente.getCdpessoa()});
	}
	
	public List<Cliente> findForSAGE(Date dataBase){
		return querySined()
				.select("contacontabil.nome, contacontabil.codigoalternativo, vcontacontabil.nivel, vcontacontabil.identificador")
				.join("cliente.listClientehistorico listClientehistorico")
				.join("cliente.contaContabil contacontabil")
				.join("contacontabil.vcontacontabil vcontacontabil")
				.where("cast(listClientehistorico.dtaltera as date)>?", dataBase)
				.whereLikeIgnoreAll("listClientehistorico.observacao", "Cria��o do cliente")
				.list();
	}
	
	/**
	 * Busca informa��es de cliente para o template de informa��es do contribuinte
	 *
	 * @param cliente
	 * @return
	 * @since 25/11/2016
	 * @author Mairon Cezar
	 */
	public Cliente loadForInfoContribuinte(Cliente cliente) {
		return querySined().select("cliente.nome, cliente.razaosocial, cliente.cpf, cliente.cnpj, cliente.email, cliente.inscricaoestadual, "+
							 "cliente.inscricaomunicipal, cliente.infoadicionalcontrib, cliente.site, cliente.observacao, telefone.telefone, endereco.numero, "+
							"endereco.enderecotipo, contato.nome, contato.emailcontato, endereco.cep, endereco.numero, endereco.logradouro, endereco.complemento, "+
							"endereco.bairro, endereco.caixapostal, municipio.nome, uf.sigla, grupotributacao.infoadicionalcontrib, grupotributacao.nome ")
				.leftOuterJoin("cliente.listaTelefone telefone")
				.leftOuterJoin("cliente.listaEndereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("endereco.enderecotipo enderecotipo")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("cliente.listaContato listaContato")
				.leftOuterJoin("listaContato.contato contato")
				.leftOuterJoin("cliente.grupotributacao grupotributacao")
				.where("cliente = ?", cliente)
				.unique();
	}
	
	/**
	* M�todo que busca os cliente para a integra��o SAGE
	*
	* @param whereIn
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public List<Cliente> findForSAGE(String whereIn){
		QueryBuilder<Cliente> query = querySined()
				.select("cliente.cdpessoa, cliente.nome, cliente.dtaltera, cliente.razaosocial, cliente.cpf, cliente.cnpj, cliente.email, cliente.inscricaoestadual, "+
						"cliente.inscricaomunicipal, cliente.site, cliente.observacao, cliente.crt, telefone.telefone, "+
						"endereco.enderecotipo, contato.nome, contato.emailcontato, endereco.cep, endereco.numero, endereco.logradouro, endereco.complemento, "+
						"endereco.bairro, endereco.caixapostal, municipio.nome, municipio.cdibge, uf.sigla, pais.nome," +
						"listClientehistorico.dtaltera ")
				.leftOuterJoin("cliente.listaTelefone telefone")
				.leftOuterJoin("cliente.listaEndereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("endereco.enderecotipo enderecotipo")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("endereco.pais pais")
				.leftOuterJoin("cliente.listaContato listaContato")
				.leftOuterJoin("listaContato.contato contato")
				.leftOuterJoin("cliente.listClientehistorico listClientehistorico");
		
		SinedUtil.quebraWhereIn("cliente.cdpessoa", whereIn, query);
		
		return query
			.orderBy("cliente.cdpessoa, listClientehistorico.dtaltera")
			.list();
	}
	
	public List<Cliente> findForValidacaoSAGE(String whereIn){
		QueryBuilder<Cliente> query = querySined()
				.select("cliente.cdpessoa, cliente.nome");
		
		SinedUtil.quebraWhereIn("cliente.cdpessoa", whereIn, query);
		
		return query
			.orderBy("cliente.cdpessoa")
			.list();
	}
	
	public boolean isClientePermissaoEmpresa(Cliente cliente, String whereInEmpresa){
		if(cliente == null || cliente.getCdpessoa() == null || StringUtils.isBlank(whereInEmpresa)) return true;
		
		QueryBuilder<Cliente> query = querySined()
			.select("cliente.cdpessoa, cliente.nome")
			.where("cliente = ?", cliente);
		
		addWhereInClienteByEmpresa(query, whereInEmpresa);
		
		List<Cliente> lista = query.list();
		
		return lista != null && lista.size() > 0;
	}
	
	public Cliente findForValeCompra(Cliente cliente){
		return querySined()
			.select("cliente.cdpessoa, cliente.nome, cliente.email, cliente.cpf, cliente.cnpj, cliente.identificador, "+
					"valecompra.data, valecompra.valor, valecompra.identificacao, valecompra.cdvalecompra, tipooperacao.nome, "+
					"contato.nome, contato.responsavelos, cliente.razaosocial, pedidovenda.cdpedidovenda, venda.cdvenda, "+
					"coleta.cdcoleta, documento.cddocumento,"+
					"endereco.logradouro, endereco.bairro, endereco.numero, municipio.nome, uf.sigla,"+
					"telefone.telefone")	
			.leftOuterJoin("cliente.listaValecompra valecompra")		
			.leftOuterJoin("valecompra.tipooperacao tipooperacao")
			.leftOuterJoin("cliente.listaContato listaContato")
			.leftOuterJoin("listaContato.contato contato")
			.leftOuterJoin("valecompra.listaValecompraorigem origem")
			.leftOuterJoin("origem.pedidovenda pedidovenda")
			.leftOuterJoin("origem.venda venda")
			.leftOuterJoin("origem.coleta coleta")
			.leftOuterJoin("origem.documento documento")
			.leftOuterJoin("cliente.listaEndereco endereco")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("cliente.listaTelefone telefone")
			.where("cliente = ?", cliente)
			.unique();
	}
	
	public Cliente findForContato(Cliente cliente){
		return querySined()
			.select("cliente.cdpessoa, cliente.nome, contato.nome, telefone.telefone")
			.leftOuterJoin("cliente.listaContato listaContato")
			.leftOuterJoin("listaContato.contato contato")
			.leftOuterJoin("contato.listaTelefone telefone")
			.where("cliente = ?", cliente)
			.unique();
	}
	
	public List<Cliente> findWhereInCdvenda(String whereInCdvenda){
		return querySined()
				.select("cliente.nome, cliente.informacaovenda")
				.join("cliente.listaVenda listaVenda")
				.whereIn("listaVenda.cdvenda", whereInCdvenda)
				.list();
	}
	
	/**
	 * Busca os clientes dos leads passados por par�metro.
	 * 	 
	 * @param whereInLead
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/10/2017
	 */
	public List<Cliente> findByLead(String whereInLead) {
		return querySined()
					.select("cliente.cdpessoa, lead.cdlead")
					.join("cliente.listaLead lead")
					.whereIn("lead.cdlead", whereInLead)
					.list();
	}
	
	public List<Cliente> findForInventario(String whereIn) {
		QueryBuilder<Cliente> query = querySined()
				.select("cliente.cdpessoa, cliente.inscricaoestadual, enderecotipo.cdenderecotipo, uf.sigla ")
				.leftOuterJoin("cliente.listaEndereco endereco")
				.leftOuterJoin("endereco.enderecotipo enderecotipo")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf");
		
		SinedUtil.quebraWhereIn("cliente.cdpessoa", whereIn, query);
		
		return query
			.orderBy("cliente.cdpessoa")
			.list();
	}
	/**
	 * Carrega um cliente com seus endere�os.
	 * @param cliente
	 * @author Murilo
	 * @return
	 */
	public Cliente loadClienteWithEndereco(Cliente cliente) {
		return querySined().select("cliente.cdpessoa, endereco.cdendereco, enderecotipo.cdenderecotipo")
				.leftOuterJoin("cliente.listaEndereco endereco")
				.leftOuterJoin("endereco.enderecotipo enderecotipo")
				.where("cliente = ?", cliente).unique();
	}
	
	/**
	 * Carrega todos os clientes vinculados a determinada tabela de pre�os.
	 * @param Materialtabelapreco
	 * @author Mairon Cezar
	 * @return
	 */
	public List<Cliente> findByMaterialtabelapreco(Materialtabelapreco materialtabelapreco){
		return querySined()
			.select("cliente.cdpessoa, cliente.nome")
			.where("exists(Select tpc.cdmaterialtabelaprecocliente from Materialtabelaprecocliente tpc where tpc.cliente = cliente and tpc.materialtabelapreco = ?)", materialtabelapreco)
			.list();
	}
	
	/**
	 * Carrega os clientes que n�o possuem o usu�rio logado como vendedor(Clientevendedor)
	 * @param whereIn
	 * @author Mairon Cezar
	 * @return
	 */
	public List<Cliente> findClientesSemPermissaoClienteVendedor(String whereIn){
		return querySined()
			.select("cliente.cdpessoa, cliente.nome")
			.whereIn("cliente.cdpessoa", whereIn)
			.where("not exists(Select cv.cdclientevendedor from Clientevendedor cv join cv.colaborador c where cv.cliente = cliente and c.cdpessoa = "+SinedUtil.getCdUsuarioLogado()+")")
			.where("not exists(Select cv.cdclientevendedor from Clientevendedor cv join cv.agencia agencia where cv.cliente = cliente and agencia.cdpessoa = "+SinedUtil.getCdUsuarioLogado()+")")
			.list();
	}
	
	public Cliente loadForEmissaoNf(Cliente cliente){
		return query()
				.select("cliente.cdpessoa, cliente.cpf, cliente.cnpj, cliente.email, cliente.nome, cliente.razaosocial, cliente.inscricaoestadual, " +
						"endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.cep, endereco.bairro, endereco.complemento, enderecotipo.cdenderecotipo, " +
						"municipio.cdmunicipio, uf.cduf, pais.cdpais")
				.leftOuterJoin("cliente.listaEndereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("endereco.pais pais")
				.leftOuterJoin("endereco.enderecotipo enderecotipo")
				.where("cliente = ?", cliente)
				.unique();
	}
	
	public Integer getNovosClientesForAndroid(Integer cdusuario) {
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.where("(select min(ch2.cdclientehistorico) from Clientehistorico ch2 where ch2.cliente = cliente and cdusuarioaltera = ?) = (select min(ch2.cdclientehistorico) from Clientehistorico ch2 where ch2.cliente = cliente)", cdusuario)
				.where("(select min(ch2.cdclientehistorico) from Clientehistorico ch2 where ch2.cliente = cliente and date_trunc('month', dtaltera) = ?) = (select min(ch2.cdclientehistorico) from Clientehistorico ch2 where ch2.cliente = cliente)", SinedDateUtils.firstDateOfMonth())
				.unique().intValue();
	}
	
}