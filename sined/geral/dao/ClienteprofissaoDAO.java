package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Clienteprofissao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ClienteprofissaoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("clienteprofissao.nome")
public class ClienteprofissaoDAO extends GenericDAO<Clienteprofissao> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Clienteprofissao> query,
			FiltroListagem _filtro) {
		
		ClienteprofissaoFiltro filtro = (ClienteprofissaoFiltro) _filtro;
		
		query
			.select("clienteprofissao.cdclienteprofissao, clienteprofissao.nome")
			.whereLikeIgnoreAll("clienteprofissao.nome", filtro.getNome());
		
		super.updateListagemQuery(query, _filtro);
	}

}
