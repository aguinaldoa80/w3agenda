package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Requisicaohistorico;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RequisicaohistoricoDAO extends GenericDAO<Requisicaohistorico> {

	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	/**
	 * M�todo que carrega o hist�rico de requisi��es de uma determinada requisi��o
	 * 
	 * @param requisicao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Requisicaohistorico> findRequisicoesHistoricoByRequisicao(Requisicao requisicao) {
		return query()
			.select("requisicaohistorico.cdrequisicaohistorico, requisicaohistorico.observacao, requisicaohistorico.cdusuarioaltera, requisicaohistorico.dtaltera")
			.join("requisicaohistorico.requisicao requisicao")
			.where("requisicao = ?", requisicao)
			.list();
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		arquivoDAO.saveFile(save.getEntity(), "arquivo");	
	}
	
	/**
	 * 
	 * M�todo que verifica se o usu�rio logado pode editar a Ordem de Servi�o:
	 * O primeiro item do hist�rico foi lan�ado por o usu�rio logado
	 *  
	 * @author Thiago Clemente
	 * 
	 */
	public boolean isPodeEditarRequisicao(Requisicao requisicao){
		
		Colaborador colaboradorLogado = SinedUtil.getUsuarioComoColaborador();
		
		if (colaboradorLogado!=null){
			Requisicaohistorico requisicaohistorico = query()
														.select("requisicaohistorico.cdrequisicaohistorico")
														.join("requisicaohistorico.requisicao requisicao")
														.where("requisicao=?", requisicao)
														.where("requisicaohistorico.cdusuarioaltera=?", colaboradorLogado.getCdpessoa())
														.orderBy("requisicaohistorico.dtaltera asc")
														.setMaxResults(1)
														.unique();
			return requisicaohistorico!=null;
		}else {
			return false;
		}
	}
}