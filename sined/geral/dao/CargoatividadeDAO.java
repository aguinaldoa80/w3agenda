package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cargoatividade;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CargoatividadeDAO extends GenericDAO<Cargoatividade> {

	/**
	 * M�todo que carrega as atividades de acordo com o cargo
	 * 
	 * @param cargo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Cargoatividade> getAtividadesByCargo(Cargo cargo) {
		if(cargo == null || cargo.getCdcargo() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("cargoatividade.cdcargoatividade, cargoatividade.descricao")
			.join("cargoatividade.cargo cargo")
			.where("cargo = ?", cargo)
			.orderBy("cargoatividade.descricao desc")
			.list();
	}
}