package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtabelapreco;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MaterialtabelaprecoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

public class MaterialtabelaprecoDAO extends GenericDAO<Materialtabelapreco> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Materialtabelapreco> query, FiltroListagem _filtro) {
		MaterialtabelaprecoFiltro filtro = (MaterialtabelaprecoFiltro) _filtro;
		
		query
			.select("distinct " +
					"materialtabelapreco.cdmaterialtabelapreco, materialtabelapreco.nome, " +
					"materialtabelapreco.dtinicio, materialtabelapreco.dtfim")
			.whereLikeIgnoreAll("materialtabelapreco.nome", filtro.getNome())
			.where("materialtabelapreco.dtinicio <= ?", filtro.getDtfim());
		
		if(filtro.getFornecedor() != null){
			query
				.leftOuterJoin("material.listaFornecedor listaFornecedor", true)
				.where("listaFornecedor.fornecedor = ?", filtro.getFornecedor());
		}
		
		if(filtro.getMaterial() != null){
			query
				.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoitem listaMaterialtabelaprecoitem", true)
				.leftOuterJoin("listaMaterialtabelaprecoitem.material material", true)
				.where("material = ?", filtro.getMaterial());
		}
		if(filtro.getMaterialgrupo() != null){
			query
				.where("materialtabelapreco.materialgrupo = ?", filtro.getMaterialgrupo());
		}
		
		if(filtro.getEmpresa() != null){
			query
				.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoempresa listaMaterialtabelaprecoempresa", true)
				.where("listaMaterialtabelaprecoempresa.empresa = ?", filtro.getEmpresa());
		}
		
		if(filtro.getCliente() != null){
			query
				.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecocliente listaMaterialtabelaprecocliente", true)
				.where("listaMaterialtabelaprecocliente.cliente = ?", filtro.getCliente());
		}
		
		query
			.openParentheses()
				.openParentheses()
					.wherePeriodo("materialtabelapreco.dtinicio",  filtro.getDtinicio(),  filtro.getDtfim())
				.closeParentheses()
				.or()
				.openParentheses()
					.wherePeriodo("materialtabelapreco.dtfim",  filtro.getDtinicio(),  filtro.getDtfim())
				.closeParentheses()
			.closeParentheses();

		
		if(Boolean.TRUE.equals(filtro.getLimitarAcessoClienteVendedor())){
			query
			.openParentheses()
				.where("not exists(select tpc.cdmaterialtabelaprecocliente from Materialtabelaprecocliente tpc where tpc.materialtabelapreco = materialtabelapreco)")
				.or()
				.where("exists(select tpc.cdmaterialtabelaprecocliente from Materialtabelaprecocliente tpc join tpc.cliente c join c.listaClientevendedor cv left outer join cv.agencia agencia "+
						"left outer join cv.colaborador colaborador where tpc.materialtabelapreco = materialtabelapreco and (agencia.cdpessoa = "+SinedUtil.getCdUsuarioLogado()+" or colaborador.cdpessoa = "+SinedUtil.getCdUsuarioLogado()+"))")
			.closeParentheses();
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Materialtabelapreco> query) {
		query
			.select("materialtabelapreco.cdmaterialtabelapreco, materialtabelapreco.nome, materialtabelapreco.taxaacrescimo, materialtabelapreco.observacao, " +
					"materialtabelapreco.dtinicio, materialtabelapreco.dtfim, materialtabelapreco.dtaltera, " +
					"materialtabelapreco.tabelaprecotipo, materialtabelapreco.discriminardescontovenda, " +
					"materialtabelapreco.cdusuarioaltera, listaMaterialtabelaprecoitem.valor, " +
					"listaMaterialtabelaprecoitem.valorvendaminimo, listaMaterialtabelaprecoitem.valorvendamaximo, " +
					"material.cdmaterial, material.nome, material.identificacao, listaMaterialtabelaprecoitem.cdmaterialtabelaprecoitem, " +
					"cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj, pedidovendatipo.cdpedidovendatipo, pedidovendatipo.descricao, " +
					"prazopagamento.cdprazopagamento, listaMaterialtabelaprecocliente.cdmaterialtabelaprecocliente," +
					"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, listaMaterialtabelaprecoempresa.cdmaterialtabelaprecoempresa," +
					"comissionamento.cdcomissionamento, comissionamento.nome, comissionamento.comissionamentotipo, " + 
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, materialtabelapreco.descontopadrao, materialtabelapreco.acrescimopadrao, " +
					"frequencia.cdfrequencia, materialgrupo.cdmaterialgrupo, categoria.cdcategoria, categoria.nome, vcategoria.identificador," +
					"listaMaterialtabelaprecoitem.identificadorespecifico, listaMaterialtabelaprecoitem.identificadorfabricante, listaMaterialtabelaprecoitem.percentualdesconto, " +
					"comissionamentoItem.cdcomissionamento, comissionamentoItem.nome, comissionamentoItem.comissionamentotipo ")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecocliente listaMaterialtabelaprecocliente")
			.leftOuterJoin("listaMaterialtabelaprecocliente.cliente cliente")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoempresa listaMaterialtabelaprecoempresa")
			.leftOuterJoin("listaMaterialtabelaprecoempresa.empresa empresa")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoitem listaMaterialtabelaprecoitem")
			.leftOuterJoin("listaMaterialtabelaprecoitem.material material")
			.leftOuterJoin("listaMaterialtabelaprecoitem.unidademedida unidademedida")
			.leftOuterJoin("materialtabelapreco.prazopagamento prazopagamento")
			.leftOuterJoin("materialtabelapreco.pedidovendatipo pedidovendatipo")
			.leftOuterJoin("materialtabelapreco.frequencia frequencia")
			.leftOuterJoin("materialtabelapreco.materialgrupo materialgrupo")
			.leftOuterJoin("materialtabelapreco.categoria categoria")
			.leftOuterJoin("categoria.vcategoria vcategoria")
			.leftOuterJoin("materialtabelapreco.comissionamento comissionamento")
			.leftOuterJoin("listaMaterialtabelaprecoitem.comissionamento comissionamentoItem")
			
			;
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaMaterialtabelaprecoitem");
		save.saveOrUpdateManaged("listaMaterialtabelaprecocliente");
		save.saveOrUpdateManaged("listaMaterialtabelaprecoempresa");
	}

	public List<Materialtabelapreco> findTabelaCliente(Integer cdmaterialtabelapreco, Cliente cliente, Prazopagamento prazopagamento, Frequencia frequencia, Pedidovendatipo pedidovendatipo, Date dtinicio, Date dtfim, Materialgrupo materialgrupo, Categoria categoria, boolean consideraSomenteCategoria, Empresa... empresas) {
		QueryBuilderSined<Materialtabelapreco> query = querySined();
		query
			.select("distinct materialtabelapreco.cdmaterialtabelapreco, materialtabelapreco.nome")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecocliente listaMaterialtabelaprecocliente")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoempresa listaMaterialtabelaprecoempresa")
			.where("materialtabelapreco.cdmaterialtabelapreco <> ?", cdmaterialtabelapreco)
			;
		
		if(cliente != null && cliente.getCdpessoa() != null){
			query.where("listaMaterialtabelaprecocliente.cliente = ?", cliente);
		} else if(categoria == null){
			query.where("listaMaterialtabelaprecocliente is null");
		}
		
		if(prazopagamento != null && prazopagamento.getCdprazopagamento() != null){
			query.where("materialtabelapreco.prazopagamento = ?", prazopagamento);
		} else {
			query.where("materialtabelapreco.prazopagamento is null");
		}
		
		if(frequencia != null && frequencia.getCdfrequencia() != null){
			query.where("materialtabelapreco.frequencia = ?", frequencia);
		} else {
			query.where("materialtabelapreco.frequencia is null");
		}
		
		if(pedidovendatipo != null && pedidovendatipo.getCdpedidovendatipo() != null){
			query.where("materialtabelapreco.pedidovendatipo = ?", pedidovendatipo);
		}else {
			query.where("materialtabelapreco.pedidovendatipo is null");
		}
		
		if(categoria != null && categoria.getCdcategoria() != null){
			if(consideraSomenteCategoria){
				query.where("materialtabelapreco.categoria = ?", categoria);
			}else {
				query.openParentheses()
					.where("materialtabelapreco.categoria = ?", categoria)
					.or()
					.where("listaMaterialtabelaprecocliente.cliente.cdpessoa in (select c.cdpessoa from Cliente c join c.listaPessoacategoria listaPessoacategoria where listaPessoacategoria.categoria = ?)", categoria)
				.closeParentheses();
			}
		}else {
			query.where("materialtabelapreco.categoria is null");
		}
		
		if(materialgrupo != null && materialgrupo.getCdmaterialgrupo() != null){
			query.where("materialtabelapreco.materialgrupo = ?", materialgrupo);
		}else {
			query.where("materialtabelapreco.materialgrupo is null");
		}
		
		if(dtfim != null){
			query.where("materialtabelapreco.dtinicio <= ?", dtfim);
		}
		
		if(dtinicio != null){
			query
				.openParentheses()
					.where("materialtabelapreco.dtfim >= ?", dtinicio)
					.or()
					.where("materialtabelapreco.dtfim is null")
				.closeParentheses();
		}
		
		if(empresas != null && empresas.length > 0){
			query.where("listaMaterialtabelaprecoempresa.empresa.cdpessoa in (" + SinedUtil.listAndConcatenateIDs(Arrays.asList(empresas)) + ")");
		}else {
			query.where("listaMaterialtabelaprecoempresa is null");
		}

		return query.list();
	}

	public List<Materialtabelapreco> findTabelaMaterial(Integer cdmaterialtabelapreco, String whereInCliente, Material material, String identificador, Prazopagamento prazopagamento, Frequencia frequencia, Pedidovendatipo pedidovendatipo, Date dtinicio, Date dtfim, Materialgrupo materialgrupo, Categoria categoria, Empresa... empresas) {
		QueryBuilderSined<Materialtabelapreco> query = querySined();
		query
			.select("distinct materialtabelapreco.cdmaterialtabelapreco, materialtabelapreco.nome")
			.from(Materialtabelapreco.class)
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecocliente listaMaterialtabelaprecocliente")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoempresa listaMaterialtabelaprecoempresa")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoitem listaMaterialtabelaprecoitem")
			.where("materialtabelapreco.cdmaterialtabelapreco <> ?", cdmaterialtabelapreco)
			.where("listaMaterialtabelaprecoitem.material = ?", material)
			;
		
		if(dtfim != null){
			query.where("materialtabelapreco.dtinicio <= ?", dtfim);
		}
		
		if(dtinicio != null){
			query
				.openParentheses()
					.where("materialtabelapreco.dtfim >= ?", dtinicio)
					.or()
					.where("materialtabelapreco.dtfim is null")
				.closeParentheses();
		}
		
		if(empresas != null && empresas.length > 0){
			query.where("listaMaterialtabelaprecoempresa.empresa.cdpessoa in (" + SinedUtil.listAndConcatenateIDs(Arrays.asList(empresas)) + ")");
		}else {
			query.where("listaMaterialtabelaprecoempresa is null");
		}
		
		if(identificador != null){
			query.where("listaMaterialtabelaprecoitem.identificadorespecifico = ?", identificador);
		} else {
			query.where("listaMaterialtabelaprecoitem.identificadorespecifico is null");
		}
		
		if(StringUtils.isNotBlank(whereInCliente)){
			query.whereIn("listaMaterialtabelaprecocliente.cliente.cdpessoa", whereInCliente);
		} else {
			query.where("listaMaterialtabelaprecocliente is null");
		}
		
		if(prazopagamento != null && prazopagamento.getCdprazopagamento() != null){
			query.where("materialtabelapreco.prazopagamento = ?", prazopagamento);
		} else {
			query.where("materialtabelapreco.prazopagamento is null");
		}
		
		if(frequencia != null && frequencia.getCdfrequencia() != null){
			query.where("materialtabelapreco.frequencia = ?", frequencia);
		} else {
			query.where("materialtabelapreco.frequencia is null");
		}
		
		if(pedidovendatipo != null && pedidovendatipo.getCdpedidovendatipo() != null){
			query.where("materialtabelapreco.pedidovendatipo = ?", pedidovendatipo);
		}else {
			query.where("materialtabelapreco.pedidovendatipo is null");
		}
		
		if(categoria != null && categoria.getCdcategoria() != null){
			query.where("materialtabelapreco.categoria = ?", categoria);
		}else {
			query.where("materialtabelapreco.categoria is null");
		}
		
		if(materialgrupo != null && materialgrupo.getCdmaterialgrupo() != null){
			query.where("materialtabelapreco.materialgrupo = ?", materialgrupo);
		}else {
			query.where("materialtabelapreco.materialgrupo is null");
		}
		
		return query.list();
	}

	/**
	 * M�todo que busca a lista de materialtabelapreco para o combo
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 * @param listaCategoria 
	 */
	public List<Materialtabelapreco> findForComboVenda(Cliente cliente, Date dtvenda, Empresa empresa, List<Categoria> listaCategoria) {
		String whereInCategoria = null;
		if(listaCategoria != null && listaCategoria.size() > 0){
			whereInCategoria = CollectionsUtil.listAndConcatenate(listaCategoria, "cdcategoria", ",");
		}
		
		QueryBuilder<Materialtabelapreco> query = querySined()
			.select("materialtabelapreco.cdmaterialtabelapreco, materialtabelapreco.nome, cliente.cdpessoa, " +
					"listaMaterialtabelaprecocliente.cdmaterialtabelaprecocliente ")
			.leftOuterJoin("materialtabelapreco.categoria categoria")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecocliente listaMaterialtabelaprecocliente")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoempresa listaMaterialtabelaprecoempresa")
			.leftOuterJoin("listaMaterialtabelaprecocliente.cliente cliente")
			.leftOuterJoin("listaMaterialtabelaprecoempresa.empresa empresa")
			.openParentheses()
				.where("cliente = ?", cliente)
				.or()
				.whereIn("categoria.cdcategoria", whereInCategoria)
			.closeParentheses()
			.openParentheses()
				.where("empresa = ?", empresa)
				.or()
				.where("empresa is null")
			.closeParentheses()
			.where("materialtabelapreco.dtinicio <= ?", dtvenda)
			.openParentheses()
				.where("materialtabelapreco.dtfim is null")
				.or()
				.where("materialtabelapreco.dtfim >= ?", dtvenda)
			.closeParentheses();
		
		List<Materialtabelapreco> lista = query.list();
		
		if(lista != null && lista.size() > 0){
			return lista;
		}else {
			return querySined()
			.select("materialtabelapreco.cdmaterialtabelapreco, materialtabelapreco.nome, cliente.cdpessoa, " +
					"listaMaterialtabelaprecocliente.cdmaterialtabelaprecocliente ")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecocliente listaMaterialtabelaprecocliente")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoempresa listaMaterialtabelaprecoempresa")
			.leftOuterJoin("listaMaterialtabelaprecocliente.cliente cliente")
			.leftOuterJoin("listaMaterialtabelaprecoempresa.empresa empresa")
			.where("cliente is null")
			.where("materialtabelapreco.categoria is null")
			.openParentheses()
				.where("empresa = ?", empresa)
				.or()
				.where("empresa is null")
			.closeParentheses()
			.where("materialtabelapreco.dtinicio <= ?", dtvenda)
			.openParentheses()
				.where("materialtabelapreco.dtfim is null")
				.or()
				.where("materialtabelapreco.dtfim >= ?", dtvenda)
			.closeParentheses()
			.list();
		}
		
	}
	
	/**
	 * M�todo que busca os dados necess�rios para ajuste de precos de venda dos materiais de materialtabelaprecoitem.
	 * 
	 * @param whereIn
	 * @return
	 */
	public List<Materialtabelapreco> findForAjustarPreco(String whereIn){
		return querySined()
					.select("materialtabelapreco.cdmaterialtabelapreco, materialtabelapreco.nome, materialtabelapreco.tabelaprecotipo, materialtabelapreco.discriminardescontovenda, " +
							"listaMaterialtabelaprecoitem.cdmaterialtabelaprecoitem, material.cdmaterial, material.nome")
					.join("materialtabelapreco.listaMaterialtabelaprecoitem listaMaterialtabelaprecoitem")
					.join("listaMaterialtabelaprecoitem.material material")
					.whereIn("materialtabelapreco.cdmaterialtabelapreco", whereIn)
					.list();
	}
	
	/**
	 * M�todo que verifica se existe tabela de pre�o do cliente
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existTabelaByCliente(Cliente cliente) {
		if(cliente == null || cliente.getCdpessoa() == null)
			throw new SinedException("Cliente n�o pode ser nulo.");
		SinedUtil.markAsReader();
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Materialtabelapreco.class)
			.setUseTranslator(false)
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecocliente listaMaterialtabelaprecocliente")
			.leftOuterJoin("listaMaterialtabelaprecocliente.cliente cliente")
			.where("cliente = ?", cliente)
			.openParentheses()
				.where("materialtabelapreco.dtfim is null").or()
				.where("materialtabelapreco.dtfim >= ?", new Date(System.currentTimeMillis()))
			.closeParentheses()
			.unique() > 0;
	}
	
	public List<Materialtabelapreco> findForPVOffline() {
		return query()
		.select("materialtabelapreco.cdmaterialtabelapreco, materialtabelapreco.nome, materialtabelapreco.dtinicio, materialtabelapreco.dtfim,  materialtabelapreco.taxaacrescimo, " +
				"prazopagamento.cdprazopagamento, empresa.cdpessoa, cliente.cdpessoa, materialgrupo.cdmaterialgrupo, materialgrupo.nome, " +
				"pedidovendatipo.cdpedidovendatipo, categoria.cdcategoria, materialgrupo.cdmaterialgrupo, " +
				"listaMaterialtabelaprecoitem.valor, listaMaterialtabelaprecoitem.valorvendaminimo, listaMaterialtabelaprecoitem.valorvendamaximo, " +
				"material.cdmaterial, unidademedida.cdunidademedida, materialtabelapreco.descontopadrao, materialtabelapreco.acrescimopadrao ")
		.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoempresa listaMaterialtabelaprecoempresa")
		.leftOuterJoin("listaMaterialtabelaprecoempresa.empresa empresa")
		.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecocliente listaMaterialtabelaprecocliente")
		.leftOuterJoin("listaMaterialtabelaprecocliente.cliente cliente")
		.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoitem listaMaterialtabelaprecoitem")
		.leftOuterJoin("listaMaterialtabelaprecoitem.material material")
		.leftOuterJoin("listaMaterialtabelaprecoitem.unidademedida unidademedida")
		.leftOuterJoin("materialtabelapreco.pedidovendatipo pedidovendatipo")
		.leftOuterJoin("materialtabelapreco.prazopagamento prazopagamento")
		.leftOuterJoin("materialtabelapreco.categoria categoria")
		.leftOuterJoin("materialtabelapreco.materialgrupo materialgrupo")
		.list();
	}
	
	public List<Materialtabelapreco> findForAndroid(String whereIn) {
		return query()
		.select("materialtabelapreco.cdmaterialtabelapreco, materialtabelapreco.nome, materialtabelapreco.dtinicio, materialtabelapreco.dtfim,  " +
				"materialtabelapreco.taxaacrescimo, materialtabelapreco.descontopadrao, materialtabelapreco.acrescimopadrao, prazopagamento.cdprazopagamento, " +
				"materialgrupo.cdmaterialgrupo, pedidovendatipo.cdpedidovendatipo, categoria.cdcategoria, " +
				"materialtabelapreco.descontopadrao, materialtabelapreco.acrescimopadrao,materialtabelapreco.discriminardescontovenda," +
				"materialtabelapreco.tabelaprecotipo ")
		.leftOuterJoin("materialtabelapreco.pedidovendatipo pedidovendatipo")
		.leftOuterJoin("materialtabelapreco.prazopagamento prazopagamento")
		.leftOuterJoin("materialtabelapreco.categoria categoria")
		.leftOuterJoin("materialtabelapreco.materialgrupo materialgrupo")
		.whereIn("materialtabelapreco.cdmaterialtabelapreco", whereIn)
		.list();
	}

	/**
	 * M�todo que busca as tabelas de pre�o do cliente
	 *
	 * @param cliente
	 * @param dtvenda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Materialtabelapreco> findByCliente(Cliente cliente, Date dtvenda, Empresa empresa, Boolean ativo, Tipocalculo tipocalculo) {
		if(cliente == null || cliente.getCdpessoa() == null)
			throw new SinedException("Cliente n�o pode ser nulo.");
		
		return querySined()
			.select("materialtabelapreco.cdmaterialtabelapreco, materialtabelapreco.nome, cliente.cdpessoa, empresa.cdpessoa," +
					"materialtabelapreco.tabelaprecotipo, materialtabelapreco.discriminardescontovenda, " +
					"listaMaterialtabelaprecocliente.cdmaterialtabelaprecocliente, listaMaterialtabelaprecoitem.cdmaterialtabelaprecoitem," +
					"material.cdmaterial, material.nome, material.valorvenda, material.valorvendaminimo, material.valorvendamaximo, " +
					"unidademedida.cdunidademedida, unidademedida.nome, material.producao, material.valorcusto, material.identificacao, " +
					"listaMaterialtabelaprecoitem.valor, listaMaterialtabelaprecoitem.valorvendaminimo, unidademedidatabela.ativo, " +
					"listaMaterialtabelaprecoitem.valorvendamaximo, listaMaterialtabelaprecoitem.percentualdesconto, prazopagamento.cdprazopagamento, prazopagamento.nome," +
					"listaMaterialunidademedida.cdmaterialunidademedida, listaMaterialunidademedida.fracao, listaMaterialunidademedida.qtdereferencia, " +
					"unidademedidamaterial.cdunidademedida, unidademedidamaterial.nome, materialtabelapreco.dtinicio, " +
					"unidademedidatabela.cdunidademedida, unidademedidatabela.nome, material.peso, material.pesobruto," +
					"listaMaterialtabelaprecoitem.identificadorfabricante ")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecocliente listaMaterialtabelaprecocliente")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoempresa listaMaterialtabelaprecoempresa")
			.leftOuterJoin("listaMaterialtabelaprecocliente.cliente cliente")
			.leftOuterJoin("listaMaterialtabelaprecoempresa.empresa empresa")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoitem listaMaterialtabelaprecoitem")
			.leftOuterJoin("listaMaterialtabelaprecoitem.unidademedida unidademedidatabela")
			.leftOuterJoin("listaMaterialtabelaprecoitem.material material")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("materialtabelapreco.prazopagamento prazopagamento")
			.leftOuterJoin("material.listaMaterialunidademedida listaMaterialunidademedida")
			.leftOuterJoin("listaMaterialunidademedida.unidademedida unidademedidamaterial")
			.where("materialtabelapreco.tabelaprecotipo = ?", tipocalculo)
			.where("material.ativo = ?", ativo)
			.where("cliente = ?", cliente)
			.openParentheses()
				.where("empresa = ?", empresa)
				.or()
				.where("empresa is null")
			.closeParentheses()
			.where("materialtabelapreco.dtinicio <= ?", dtvenda)
			.openParentheses()
				.where("materialtabelapreco.dtfim is null")
				.or()
				.where("materialtabelapreco.dtfim >= ?", dtvenda)
			.closeParentheses()
			.list();
	}
	
	public Materialtabelapreco getTabelaPrecoAtual(Date data, Materialgrupo materialgrupo, List<Categoria> listaCategoria, Prazopagamento prazopagamento, Pedidovendatipo pedidovendatipo, Empresa empresa, Cliente cliente, boolean withoutMaterial) {
		if(!withoutMaterial){
			//caso o grupo de material e a categoria sejam nulos, a busca deve ser feita pelo material 
			if(materialgrupo == null && (listaCategoria == null || listaCategoria.isEmpty()))
				return null;
		}
		
		QueryBuilder<Materialtabelapreco> query = querySined();
		
		query
			.select("materialtabelapreco.cdmaterialtabelapreco, materialtabelapreco.taxaacrescimo, materialtabelapreco.descontopadrao, " +
					"materialtabelapreco.acrescimopadrao, frequencia.cdfrequencia, materialtabelapreco.tabelaprecotipo, materialtabelapreco.discriminardescontovenda")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoempresa listaMaterialtabelaprecoempresa")
			.leftOuterJoin("listaMaterialtabelaprecoempresa.empresa empresa")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecocliente listaMaterialtabelaprecocliente")
			.leftOuterJoin("listaMaterialtabelaprecocliente.cliente cliente")
			.leftOuterJoin("materialtabelapreco.prazopagamento prazopagamento")
			.leftOuterJoin("materialtabelapreco.materialgrupo materialgrupo")
			.leftOuterJoin("materialtabelapreco.categoria categoria")
			.leftOuterJoin("materialtabelapreco.pedidovendatipo pedidovendatipo")
			.leftOuterJoin("materialtabelapreco.frequencia frequencia")
			.where("materialtabelapreco.dtinicio <= ?", data)
			.openParentheses()
				.where("materialtabelapreco.dtfim >= ?", data)
				.or()
				.where("materialtabelapreco.dtfim is null")
			.closeParentheses();
		
		if(cliente != null && cliente.getCdpessoa() != null){
			query.where("cliente = ?", cliente);
		} else {
			query.where("cliente is null");
		}
		
		if(materialgrupo != null && materialgrupo.getCdmaterialgrupo() != null){
			query.where("materialgrupo = ?", materialgrupo);
		} else {
			query.where("materialgrupo is null");
		}
		
		if(listaCategoria != null && !listaCategoria.isEmpty()){
			query.whereIn("categoria.cdcategoria", CollectionsUtil.listAndConcatenate(listaCategoria, "cdcategoria", ","));
		} else {
			query.where("categoria is null");
		}
		
		if(prazopagamento != null && prazopagamento.getCdprazopagamento() != null){
			query.where("prazopagamento = ?", prazopagamento);
		} else {
			query.where("prazopagamento is null");
		}
		
		if(pedidovendatipo != null && pedidovendatipo.getCdpedidovendatipo() != null){
			query.where("pedidovendatipo = ?", pedidovendatipo);
		} else {
			query.where("pedidovendatipo is null");
		}
		
		if(empresa != null && empresa.getCdpessoa() != null){
			query.where("empresa = ?", empresa);
		} else {
			query.where("empresa is null");
		}
		
		List<Materialtabelapreco> lista = query.list();
		if(lista != null && lista.size() > 0){
			return lista.get(0);
		} else return null;
	}
	
	public List<Materialtabelapreco> findTabelaPrecoAtualOtimizado(Date data, Materialgrupo materialgrupo, List<Categoria> listaCategoria, boolean withoutMaterial){
		if(!withoutMaterial){
			//caso o grupo de material e a categoria sejam nulos, a busca deve ser feita pelo material 
			if(materialgrupo == null && (listaCategoria == null || listaCategoria.isEmpty()))
				return null;
		}
		
		return querySined()
			.select("materialtabelapreco.cdmaterialtabelapreco, materialtabelapreco.taxaacrescimo, materialtabelapreco.descontopadrao, " +
					"materialtabelapreco.acrescimopadrao, frequencia.cdfrequencia, cliente.cdpessoa, materialgrupo.cdmaterialgrupo, " +
					"materialtabelapreco.discriminardescontovenda, materialtabelapreco.tabelaprecotipo, "+
					"categoria.cdcategoria, prazopagamento.cdprazopagamento, pedidovendatipo.cdpedidovendatipo, empresa.cdpessoa")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoempresa listaMaterialtabelaprecoempresa")
			.leftOuterJoin("listaMaterialtabelaprecoempresa.empresa empresa")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecocliente listaMaterialtabelaprecocliente")
			.leftOuterJoin("listaMaterialtabelaprecocliente.cliente cliente")
			.leftOuterJoin("materialtabelapreco.prazopagamento prazopagamento")
			.leftOuterJoin("materialtabelapreco.materialgrupo materialgrupo")
			.leftOuterJoin("materialtabelapreco.categoria categoria")
			.leftOuterJoin("materialtabelapreco.pedidovendatipo pedidovendatipo")
			.leftOuterJoin("materialtabelapreco.frequencia frequencia")
			.where("materialtabelapreco.dtinicio <= ?", data)
			.openParentheses()
				.where("materialtabelapreco.dtfim >= ?", data)
				.or()
				.where("materialtabelapreco.dtfim is null")
			.closeParentheses()
			.list();
	}
	
	
	
	public Materialtabelapreco getTabelaPrecoAtualForContrato(Materialgrupo materialgrupo, List<Categoria> listaCategoria, Frequencia frequencia, Empresa empresa) {
		//caso o grupo de material e a categoria sejam nulos, a busca deve ser feita pelo material 
		if(materialgrupo == null && (listaCategoria == null || listaCategoria.isEmpty()))
			return null;
		
		QueryBuilder<Materialtabelapreco> query = querySined();
		Date current = SinedDateUtils.currentDate(); 
		
		query
			.select("materialtabelapreco.cdmaterialtabelapreco, materialtabelapreco.taxaacrescimo, materialtabelapreco.descontopadrao, materialtabelapreco.acrescimopadrao, " +
					"frequencia.cdfrequencia, materialtabelapreco.tabelaprecotipo, materialtabelapreco.discriminardescontovenda ")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoempresa listaMaterialtabelaprecoempresa")
			.leftOuterJoin("listaMaterialtabelaprecoempresa.empresa empresa")
			.leftOuterJoin("materialtabelapreco.materialgrupo materialgrupo")
			.leftOuterJoin("materialtabelapreco.categoria categoria")
			.leftOuterJoin("materialtabelapreco.frequencia frequencia")
			.where("materialtabelapreco.dtinicio <= ?", current)
			.openParentheses()
				.where("materialtabelapreco.dtfim >= ?", current)
				.or()
				.where("materialtabelapreco.dtfim is null")
			.closeParentheses();
		
		if(materialgrupo != null && materialgrupo.getCdmaterialgrupo() != null){
			query.where("materialgrupo = ?", materialgrupo);
		} else {
			query.where("materialgrupo is null");
		}
		
		if(listaCategoria != null && !listaCategoria.isEmpty()){
			query.whereIn("categoria.cdcategoria", CollectionsUtil.listAndConcatenate(listaCategoria, "cdcategoria", ","));
		} else {
			query.where("categoria is null");
		}
		
		if(frequencia != null && frequencia.getCdfrequencia() != null){
			query.where("frequencia = ?", frequencia);
		} else {
			query.where("frequencia is null");
		}
		
		if(empresa != null && empresa.getCdpessoa() != null){
			query.where("empresa = ?", empresa);
		} else {
			query.where("empresa is null");
		}
		
		List<Materialtabelapreco> lista = query.list();
		if(lista != null && lista.size() > 0){
			return lista.get(0);
		} else return null;
	}

	@Override
	public ListagemResult<Materialtabelapreco> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Materialtabelapreco> query = querySined();
		updateListagemQuery(query, filtro);
		query.orderBy("materialtabelapreco.cdmaterialtabelapreco");
		
		return new ListagemResult<Materialtabelapreco>(query, filtro);
	}
}
