package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Ordemservicoveterinaria;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OrdemservicoveterinariaDAO extends GenericDAO<Ordemservicoveterinaria> {

	/**
	 * M�todo que carrega a Lista de Ordemservicoveterinaria com os dados para Faturamento 
	 *
	 * @param whereIn OrdemServicoVeterinaria
	 * @author Marcos Lisboa
	 */
	public List<Ordemservicoveterinaria> carregaRequisicaoForFaturar(String whereIn) {
		return query()
		.select(" ordemservicoveterinaria.cdordemservicoveterinaria, empresa.cdpessoa, empresa.razaosocial, empresa.nomefantasia, cliente.cdpessoa, cliente.nome, cliente.cnpj, " +
				" cliente.cpf, cliente.tipopessoa, cliente.razaosocial, cliente.inscricaoestadual, ordemservicoveterinaria.descricao," +
				" listaOrdemservicoveterinariamaterial.cdordemservicoveterinariamaterial, listaOrdemservicoveterinariamaterial.qtdeusada," +
				" listaOrdemservicoveterinariamaterial.faturado, material.cdmaterial, material.identificacao, material.nome, material.valorvenda, material.obrigarlote, material.servico, material.vendapromocional, " +
				" material.servico, material.exibiritenskitvenda, unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
				" loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, responsavel.cdpessoa, responsavel.nome," +
				"listaOrdemservicoveterinariamaterial.valortotal, atividadetipoveterinaria.cdatividadetipoveterinaria, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome," +
				"listaMaterialrelacionado.cdmaterialrelacionado, listaMaterialrelacionado.quantidade, listaMaterialrelacionado.valorvenda, " +
				"materialpromocao.cdmaterial, materialpromocao.nome, materialpromocao.identificacao, materialpromocao.servico, materialpromocao.produto," +
				"unidademedidaPromocao.cdunidademedida, unidademedidaPromocao.nome, unidademedidaPromocao.simbolo ")						
		.leftOuterJoin("ordemservicoveterinaria.cliente cliente")
		.leftOuterJoin("ordemservicoveterinaria.empresa empresa")
		.leftOuterJoin("ordemservicoveterinaria.listaOrdemservicoveterinariamaterial listaOrdemservicoveterinariamaterial")
		.leftOuterJoin("listaOrdemservicoveterinariamaterial.material material")
		.leftOuterJoin("material.unidademedida unidademedida")
		.leftOuterJoin("listaOrdemservicoveterinariamaterial.loteestoque loteestoque")
		.leftOuterJoin("ordemservicoveterinaria.responsavel responsavel")
		.leftOuterJoin("ordemservicoveterinaria.atividadetipoveterinaria atividadetipoveterinaria")
		.leftOuterJoin("atividadetipoveterinaria.localarmazenagem localarmazenagem")
		.leftOuterJoin("material.listaMaterialrelacionado listaMaterialrelacionado")
		.leftOuterJoin("listaMaterialrelacionado.materialpromocao materialpromocao")
		.leftOuterJoin("materialpromocao.unidademedida unidademedidaPromocao")
		.whereIn("ordemservicoveterinaria.cdordemservicoveterinaria", whereIn)
		.where("listaOrdemservicoveterinariamaterial.faturar=?", Boolean.TRUE)
		.orderBy("cliente.cdpessoa, ordemservicoveterinaria.cdordemservicoveterinaria")
		.list();
	}

	/**
	 * M�todo que persiste o Status da Ordemservicoveterinaria 
	 *
	 * @param OrdemServicoVeterinaria
	 * @author Marcos Lisboa
	 */
	public void updateStatus(Ordemservicoveterinaria os) {
		getHibernateTemplate().bulkUpdate(
				"update Ordemservicoveterinaria o set o.requisicaoestado = ? where o.cdordemservicoveterinaria = ?",
				new Object[] {
						os.getRequisicaoestado(),
						os.getCdordemservicoveterinaria() });
	}

	/**
	* M�todo que desmarca a flag de faturado do item da ordem de servi�o veterin�ria de acordo com o pedido de venda
	*
	* @param whereInPedidoCancelado
	* @since 17/08/2016
	* @author Luiz Fernando
	*/
	public void updateItemFaturadoAposPedidoCancelado(String whereInPedidoCancelado) {
		if(StringUtils.isNotBlank(whereInPedidoCancelado)){
			String sql =	" update ordemservicoveterinariamaterial set faturado = null where cdordemservicoveterinariamaterial in ( " +
							" select distinct osvm.cdordemservicoveterinariamaterial " +
							" from pedidovendaordemservicoveterinaria pvosv " +
							" join ordemservicoveterinariamaterial osvm on osvm.cdordemservicoveterinaria = pvosv.cdordemservicoveterinaria " +
							" join pedidovendamaterial pvm on pvm.cdpedidovenda = pvosv.cdpedidovenda and ((pvm.cdmaterial = osvm.cdmaterial and pvm.cdmaterialmestre is NULL) or pvm.cdmaterialmestre = osvm.cdmaterial) " +
							" where pvm.cdpedidovenda in (" + whereInPedidoCancelado +") " +
							")";
			
			getJdbcTemplate().update(sql);
		}
	}
}