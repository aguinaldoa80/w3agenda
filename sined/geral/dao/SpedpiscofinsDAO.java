package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Spedpiscofins;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedpiscofinsFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class SpedpiscofinsDAO extends GenericDAO<Spedpiscofins> {

	@Override
	public void updateListagemQuery(QueryBuilder<Spedpiscofins> query, FiltroListagem _filtro) {
		SpedpiscofinsFiltro filtro = (SpedpiscofinsFiltro) _filtro;
		
		if(filtro.getEmpresa() == null) query.where("1=0");
		
		query
			.select("spedpiscofins.cdspedpiscofins, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
					"spedpiscofins.dtinicio, spedpiscofins.dtfim, arquivo.cdarquivo, arquivo.nome, spedpiscofins.dtaltera")
			.leftOuterJoin("spedpiscofins.arquivo arquivo")
			.leftOuterJoin("spedpiscofins.empresa empresa")
			.where("empresa = ?", filtro.getEmpresa())
			.where("spedpiscofins.dtinicio >= ?", filtro.getDtinicio())
			.where("spedpiscofins.dtfim <= ?", filtro.getDtfim())
			.where("spedpiscofins.indicadorincidenciatributaria = ?", filtro.getIndicadorincidenciatributaria())
			.where("spedpiscofins.metodoapropriacaocredito = ?", filtro.getMetodoapropriacaocredito())
			.orderBy("spedpiscofins.dtinicio desc");
	}
	
	/**
	 * M�todo para deletar o arquivo SPED PIS/COFINS de acordo com par�metro
	 *
	 * @param empresa
	 * @param dtinicio
	 * @param dtfim
	 * @author Luiz Fernando
	 */
	public void deleteByEmpresaDtinicioDtfim(Empresa empresa, Date dtinicio, Date dtfim) {
		getHibernateTemplate().bulkUpdate("delete from Spedpiscofins s where s.empresa.cdpessoa = ? and s.dtinicio = ? and s.dtfim = ?", new Object[]{empresa.getCdpessoa(), dtinicio, dtfim});
	}

	public List<Spedpiscofins> getSpedMesAnterior(Empresa e) {
		return query()
				.leftOuterJoin("spedpiscofins.empresa empresa")
				.where("empresa.cdempresa", e.getCdpessoa())
				.where("date_part('month', spedpiscofins.mesano) = ?", SinedDateUtils.getMes(SinedDateUtils.currentDate()) - 1)
				.where("date_part('year', spedpiscofins.mesano) = ?", SinedDateUtils.getAno(SinedDateUtils.currentDate()))
				.list();
	}
}
