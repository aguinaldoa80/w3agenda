package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompraacao;
import br.com.linkcom.sined.geral.bean.Ordemcomprahistorico;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OrdemcomprahistoricoDAO extends GenericDAO<Ordemcomprahistorico>{

	
	/**
	 * Carrega o hist�rico da ordem de compra para ser exibida na tela.
	 *
	 * @param ordemcomprahistorico
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Ordemcomprahistorico findForVisualizacao(Ordemcomprahistorico ordemcomprahistorico) {
		if (ordemcomprahistorico == null || ordemcomprahistorico.getCdordemcomprahistorico() == null) {
			throw new SinedException("Ordem de compra n�o pode ser nulo.");
		}
		return query()
					.select("fornecedor.cdpessoa, contato.cdpessoa, localarmazenagem.cdlocalarmazenagem, empresa.cdpessoa, " +
							"ordemcomprahistorico.dtproximaentrega, ordemcomprahistorico.frete, ordemcomprahistorico.desconto, " +
							"ordemcomprahistorico.valor, prazopagamento.cdprazopagamento")
					.leftOuterJoin("ordemcomprahistorico.fornecedor fornecedor")
					.leftOuterJoin("ordemcomprahistorico.contato contato")
					.leftOuterJoin("ordemcomprahistorico.localarmazenagem localarmazenagem")
					.leftOuterJoin("ordemcomprahistorico.empresa empresa")
					.leftOuterJoin("ordemcomprahistorico.prazopagamento prazopagamento")
					.where("ordemcomprahistorico = ?",ordemcomprahistorico)
					.unique();
	}

	public Ordemcomprahistorico getUsuarioLastAction(Ordemcompra ordemcompra, Ordemcompraacao acao) {
		return query()
					.select("ordemcomprahistorico.cdordemcomprahistorico, ordemcomprahistorico.cdusuarioaltera")
					.where("ordemcomprahistorico.cdordemcomprahistorico = (select max(o.cdordemcomprahistorico) " +
																			"from Ordemcomprahistorico o " +
																			"join o.ordemcompraacao as acao " +
																			"join o.ordemcompra as oc " +
																			"where acao.cdordemcompraacao = " + acao.getCdordemcompraacao() + " " +
																			"and oc.cdordemcompra = " + ordemcompra.getCdordemcompra() + ")")
					.unique();
	}
	
	/**
	 * 
	 * M�todo que busca a lista de cdusuarioaltera para imprimir no relat�rio de ordem de compra o usu�rio que criou e que alterou a OC.
	 *
	 *@author Thiago Augusto
	 *@date 19/03/2012
	 * @param ordemcompra
	 * @return
	 */
	public List<Ordemcomprahistorico> getListaOrdemCompraHistoricoForReport(Ordemcompra ordemcompra){
		String whereIn =  Ordemcompraacao.CRIADA.getCdordemcompraacao() + ", " + Ordemcompraacao.AUTORIZADA.getCdordemcompraacao() + ", " + Ordemcompraacao.APROVADA.getCdordemcompraacao();
		return query()
					.select("ordemcomprahistorico.cdordemcomprahistorico, ordemcomprahistorico.cdusuarioaltera, ordemcompraacao.cdordemcompraacao, ordemcompraacao.nome ")
					.leftOuterJoin("ordemcomprahistorico.ordemcompra ordemcompra")
					.leftOuterJoin("ordemcomprahistorico.ordemcompraacao ordemcompraacao")
					.where("ordemcompra = ?", ordemcompra)
					.whereIn("ordemcompraacao ", whereIn)
					.list();
	}

	/**
	 * Busca o primeiro hist�rico da ordem de compra.
	 *
	 * @param ordemcompra
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/02/2013
	 */
	public Ordemcomprahistorico getPrimeiroHistorico(Ordemcompra ordemcompra) {
		if (ordemcompra == null || ordemcompra.getCdordemcompra() == null) {
			throw new SinedException("Ordem de compra n�o pode ser nulo.");
		}
		return query()
					.select("ordemcomprahistorico.cdordemcomprahistorico, ordemcomprahistorico.dtaltera")
					.setMaxResults(1)
					.join("ordemcomprahistorico.ordemcompra ordemcompra")
					.where("ordemcompra = ?", ordemcompra)
					.orderBy("ordemcomprahistorico.dtaltera")
					.unique();
	}
	
}
