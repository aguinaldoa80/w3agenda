package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacaomaterial;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContratofaturalocacaomaterialDAO extends GenericDAO<Contratofaturalocacaomaterial>{

	public List<Contratofaturalocacaomaterial> findByContratofaturalocacao(Contratofaturalocacao contratofaturalocacao) {
		return query()
					.select("contratofaturalocacaomaterial.cdcontratofaturalocacaomaterial, contratofaturalocacaomaterial.valor, " +
							"material.cdmaterial, material.nome, material.identificacao, patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta")
					.leftOuterJoin("contratofaturalocacaomaterial.material material")
					.leftOuterJoin("contratofaturalocacaomaterial.patrimonioitem patrimonioitem")
					.where("contratofaturalocacaomaterial.contratofaturalocacao = ?", contratofaturalocacao)
					.list();
	}

}
