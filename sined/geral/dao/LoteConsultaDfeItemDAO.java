package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfeItem;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.LoteConsultaDfeItemStatusEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.SituacaoEmissorEnum;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class LoteConsultaDfeItemDAO extends GenericDAO<LoteConsultaDfeItem> {
	
	public void atualizarArquivoXml(LoteConsultaDfeItem loteConsultaDfeItem) {
		getHibernateTemplate().bulkUpdate("update LoteConsultaDfeItem l set l.arquivoXml = ? where l.id = ?", 
				new Object[] {loteConsultaDfeItem.getArquivoXml(), loteConsultaDfeItem.getCdLoteConsultaDfeItem()});
	}
	
	public void atualizarStatusEnum(LoteConsultaDfeItem loteConsultaDfeItem) {
		getHibernateTemplate().bulkUpdate("update LoteConsultaDfeItem l set l.statusEnum = ? where l.id = ?", 
				new Object[] {loteConsultaDfeItem.getStatusEnum(), loteConsultaDfeItem.getCdLoteConsultaDfeItem()});
	}

	public List<LoteConsultaDfeItem> temLoteConsultaDfeItemDiferenteDe(String selectedItens, SituacaoEmissorEnum situacaoEnum) {
		if (StringUtils.isEmpty(selectedItens) || situacaoEnum == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("loteConsultaDfeItem.cdLoteConsultaDfeItem, loteConsultaDfeItem.situacaoEnum ")
				.where("loteConsultaDfeItem.situacaoEnum <> ?", situacaoEnum)
				.whereIn("loteConsultaDfeItem.cdLoteConsultaDfeItem", selectedItens)
				.list();
	}

	public Boolean procurarEmpresasDoLoteConsultaDfeItem(String selectedItens) {
		if (StringUtils.isEmpty(selectedItens)) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("distinct empresa.cdpessoa ")
				.leftOuterJoin("loteConsultaDfeItem.loteConsultaDfe loteConsultaDfe")
				.leftOuterJoin("loteConsultaDfe.empresa empresa")
				.whereIn("loteConsultaDfeItem.cdLoteConsultaDfeItem", selectedItens)
				.list().size() > 1;
	}

	public LoteConsultaDfeItem procurarPorId(Integer cdLoteConsultaDfeItem) {
		return query()
				.select("loteConsultaDfeItem.cdLoteConsultaDfeItem, loteConsultaDfeItem.chaveAcesso, loteConsultaDfeItem.cnpj, loteConsultaDfeItem.cpf, " +
						"empresa.cdpessoa, " +
						"arquivoXml.cdarquivo ")
				.leftOuterJoin("loteConsultaDfeItem.loteConsultaDfe loteConsultaDfe")
				.leftOuterJoin("loteConsultaDfe.empresa empresa")
				.leftOuterJoin("loteConsultaDfeItem.arquivoXml arquivoXml")
				.where("loteConsultaDfeItem.cdLoteConsultaDfeItem = ?", cdLoteConsultaDfeItem)
				.unique();
	}

	public LoteConsultaDfeItem procurarPorChaveAcesso(String chaveAcesso) {
		if (chaveAcesso == null || StringUtils.isEmpty(chaveAcesso)) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("loteConsultaDfeItem.cdLoteConsultaDfeItem, loteConsultaDfeItem.chaveAcesso ")
				.where("loteConsultaDfeItem.chaveAcesso = ?", chaveAcesso)
				.unique();
	}
	
	public LoteConsultaDfeItem procurarPorChaveAcessoImportacao(String chaveAcesso) {
		if (chaveAcesso == null || StringUtils.isEmpty(chaveAcesso)) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("loteConsultaDfeItem.cdLoteConsultaDfeItem, loteConsultaDfeItem.statusEnum, tpEventoW3.codigo, " +
						"arquivoXmlCompleto.cdarquivo")
				.leftOuterJoin("loteConsultaDfeItem.arquivoXmlCompleto arquivoXmlCompleto")
				.leftOuterJoin("loteConsultaDfeItem.statusEnum statusEnum")
				.leftOuterJoin("loteConsultaDfeItem.tpEventoW3 tpEventoW3")
				.where("loteConsultaDfeItem.chaveAcesso = ?", chaveAcesso)
				.where("loteConsultaDfeItem.situacaoEnum = ?", SituacaoEmissorEnum.PROCESSADO_SUCESSO)
				.where("arquivoXmlCompleto is not null")
				.unique();
	}
	
	public List<LoteConsultaDfeItem> procurarPorLoteConsultaDfe(LoteConsultaDfe loteConsultaDfe) {
		if (loteConsultaDfe == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("loteConsultaDfeItem.cdLoteConsultaDfeItem, loteConsultaDfeItem.chaveAcesso, loteConsultaDfeItem.cpf, loteConsultaDfeItem.cnpj, loteConsultaDfeItem.razaoSocial, " +
						"loteConsultaDfeItem.ie, loteConsultaDfeItem.tipoOperacaoNota, loteConsultaDfeItem.valorNf, loteConsultaDfeItem.dhRecbto, " +
						"loteConsultaDfeItem.nuProt, loteConsultaDfeItem.statusEnum, loteConsultaDfeItem.coStat, loteConsultaDfeItem.motivo, " +
						"loteConsultaDfeItem.evento, loteConsultaDfeItem.dhEvento, loteConsultaDfeItem.dhRegEvento, " +
						"arquivoXml.cdarquivo, arquivoXml.nome, arquivoXml.tipoconteudo, listaManifestoDfe.cdManifestoDfe, " +
						"tpEvento.cdTipoEventoNfe, tpEvento.codigo, tpEvento.descricao ")
				.leftOuterJoin("loteConsultaDfeItem.arquivoXml arquivoXml")
				.leftOuterJoin("loteConsultaDfeItem.tpEvento tpEvento")
				.leftOuterJoin("loteConsultaDfeItem.listaManifestoDfe listaManifestoDfe")
				.where("loteConsultaDfeItem.loteConsultaDfe = ?", loteConsultaDfe)
				.list();
	}

	public LoteConsultaDfeItem procurarEmpresaPorId(Integer cdLoteConsultaDfeItem) {
		if (cdLoteConsultaDfeItem == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("distinct empresa.cdpessoa ")
				.leftOuterJoin("loteConsultaDfeItem.loteConsultaDfe loteConsultaDfe")
				.leftOuterJoin("loteConsultaDfe.empresa empresa")
				.where("loteConsultaDfeItem.cdLoteConsultaDfeItem = ?", cdLoteConsultaDfeItem)
				.unique();
	}

	public List<LoteConsultaDfeItem> procurarNfeStatusDiferenteDe(String whereIn, LoteConsultaDfeItemStatusEnum statusEnum) {
		if (StringUtils.isEmpty(whereIn) || statusEnum == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("loteConsultaDfeItem.cdLoteConsultaDfeItem, loteConsultaDfeItem.chaveAcesso ")
				.where("loteConsultaDfeItem.statusEnum != ?", statusEnum)
				.whereIn("loteConsultaDfeItem.cdLoteConsultaDfeItem", whereIn)
				.list();
	}
}
