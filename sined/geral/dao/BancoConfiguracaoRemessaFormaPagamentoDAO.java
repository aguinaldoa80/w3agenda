package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaFormaPagamento;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class BancoConfiguracaoRemessaFormaPagamentoDAO extends GenericDAO<BancoConfiguracaoRemessaFormaPagamento> {

}
