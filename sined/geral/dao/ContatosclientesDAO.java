package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Vcategoria;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ClienteFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContatosclientesDAO extends GenericDAO<Cliente>{
	
	private CategoriaService categoriaService;
	
	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Cliente> query,FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		}
		ClienteFiltro filtro = (ClienteFiltro)_filtro;
		
		Vcategoria vcategoria = new Vcategoria();
		
		if (filtro.getCategoria() != null){
			vcategoria = categoriaService.carregarIdentificador(filtro.getCategoria());
		}
	
		query
			.select("distinct cliente.cdpessoa, cliente.nome")
			.leftOuterJoin("cliente.listaPessoacategoria pessoacategoria")
			.leftOuterJoin("pessoacategoria.categoria categoria")
			.leftOuterJoin("categoria.vcategoria vcategoria")
			.join("cliente.listaContato listacontato")
			.orderBy("cliente.nome")
			.ignoreJoin("pessoacategoria", "categoria", "vcategoria", "listacontato");
			if(filtro.getCategoria() != null){
				query.where("vcategoria.identificador like ?", vcategoria.getIdentificador() + "%");
			}
			
			if(filtro.getCliente() != null){
				query.where("cliente.cdpessoa = ?", filtro.getCliente().getCdpessoa());
			}
			
			if(filtro.getAtivo() != null){
				query.where("cliente.cdpessoa in (select c.cdpessoa from Cliente c where c.ativo = " + (filtro.getAtivo() ? "true" : "false") + ")");			
			} else {
				query.where("cliente.cdpessoa in (select c.cdpessoa from Cliente c)");
			}
	}
}
