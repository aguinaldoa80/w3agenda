package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Bandarede;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.BandaredeFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("bandarede.valor")
public class BandaredeDAO extends GenericDAO<Bandarede>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Bandarede> query, FiltroListagem _filtro) {
		BandaredeFiltro filtro = (BandaredeFiltro)_filtro;
		query.select("bandarede.cdbandarede, bandarede.valor, bandarede.valorqueue, unidademedida.nome, unidademedidaqueue.nome")
			 .join("bandarede.unidademedida unidademedida")
			 .join("bandarede.unidademedidaqueue unidademedidaqueue")
			 .where("bandarede.valor=?", filtro.getValor())
			 .where("bandarede.unidademedida=?", filtro.getUnidademedida());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Bandarede> query) {
		query.leftOuterJoinFetch("bandarede.unidademedida unidademedida")
		   	 .leftOuterJoinFetch("bandarede.unidademedidaqueue unidademedidaqueue");
	}
}
