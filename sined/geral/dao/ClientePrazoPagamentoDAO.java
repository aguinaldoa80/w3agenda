package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.ClientePrazoPagamento;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;


public class ClientePrazoPagamentoDAO extends GenericDAO<ClientePrazoPagamento>{
	
	/**
	 * Lista todos os Prazos de Pagamento para um determinado Cliente.
	 * @author Marcos Lisboa
	 * @param cliente
	 * @param prazomedio
	 */
	public List<ClientePrazoPagamento> findByCliente(Cliente cliente) {
		if(cliente == null || cliente.getCdpessoa() == null)
			throw new SinedException("Cliente n�o pode ser nulo");
		return querySined()
			.select("clientePrazoPagamento.cdclienteprazopagamento, prazopagamento.cdprazopagamento, prazopagamento.nome, prazopagamento.prazomedio")
			.leftOuterJoin("clientePrazoPagamento.prazopagamento prazopagamento")
			.where("clientePrazoPagamento.cliente = ?", cliente)
		 	.list();
	}
	
	/**
	* M�todo que carrega os prazo de pagamento vinculado aos clientes
	*
	* @param whereIn
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<ClientePrazoPagamento> findForAndroid(String whereIn) {
		return query()
			.select("clientePrazoPagamento.cdclienteprazopagamento, prazopagamento.cdprazopagamento, cliente.cdpessoa ")
			.join("clientePrazoPagamento.prazopagamento prazopagamento")
			.join("clientePrazoPagamento.cliente cliente")
			.whereIn("clientePrazoPagamento.cdclienteprazopagamento", whereIn)
			.list();
	}
}