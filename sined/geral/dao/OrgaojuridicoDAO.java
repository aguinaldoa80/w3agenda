package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Orgaojuridico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("orgaojuridico.nome")
public class OrgaojuridicoDAO extends GenericDAO<Orgaojuridico> {
	
	
}
