package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitemnotaentrada;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotafiscalprodutoitemnotaentradaDAO extends GenericDAO<Notafiscalprodutoitemnotaentrada> {

	/**
	 * Busca os relacionamentos de entregamaterial com notafiscalprodutoitem
	 *
	 * @param whereInEntregamaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/04/2015
	 */
	public List<Notafiscalprodutoitemnotaentrada> findByEntregamaterial(String whereInEntregamaterial) {
		return query()
					.select("notafiscalprodutoitemnotaentrada.cdnotafiscalprodutoitemnotaentrada, " +
							"notafiscalprodutoitemnotaentrada.qtdereferencia, " +
							"entregamaterial.cdentregamaterial")
					.join("notafiscalprodutoitemnotaentrada.entregamaterial entregamaterial WITH entregamaterial.cdentregamaterial IN (" + whereInEntregamaterial + ")")
					.join("notafiscalprodutoitemnotaentrada.notafiscalprodutoitem notafiscalprodutoitem")
					.join("notafiscalprodutoitem.notafiscalproduto notafiscalproduto")
					.where("notafiscalproduto.notaStatus <> ?", NotaStatus.CANCELADA)
					.whereIn("entregamaterial.cdentregamaterial", whereInEntregamaterial)
					.list();
	}
	
}
