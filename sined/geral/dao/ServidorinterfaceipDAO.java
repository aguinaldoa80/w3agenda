package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Servidorinterfaceip;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.ServidorinterfaceipFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ServidorinterfaceipDAO extends GenericDAO<Servidorinterfaceip>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Servidorinterfaceip> query, FiltroListagem _filtro) {
		ServidorinterfaceipFiltro filtro = (ServidorinterfaceipFiltro)_filtro;
		query.leftOuterJoinFetch("servidorinterfaceip.servidorinterface servidorinterface")
			 .leftOuterJoinFetch("servidorinterface.servidor servidor")
		 	 .leftOuterJoinFetch("servidorinterfaceip.redeip redeip")
		 	 .leftOuterJoinFetch("redeip.rede rede")
		 	 .where("servidorinterface.servidor = ?", filtro.getServidor())
		 	 .where("servidorinterfaceip.servidorinterface = ?", filtro.getServidorinterface())
		 	 .where("servidorinterfaceip.redeip = ?", filtro.getRedeip())
			 .where("redeip.rede = ?", filtro.getRede());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Servidorinterfaceip> query) {
		query.leftOuterJoinFetch("servidorinterfaceip.servidorinterface servidorinterface")
			 .leftOuterJoinFetch("servidorinterface.servidor servidor")
	 	 	 .leftOuterJoinFetch("servidorinterfaceip.redeip redeip")
			 .leftOuterJoinFetch("redeip.rede rede");
	}
		
}
