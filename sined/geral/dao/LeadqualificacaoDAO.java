package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Leadqualificacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class LeadqualificacaoDAO extends GenericDAO<Leadqualificacao>{

	public Leadqualificacao findByNome(String nome) {
		if(nome == null || nome.equals("")){
			throw new SinedException("Nome da qualifica��o n�o pode ser nulo.");
		}
		
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		List<Leadqualificacao> lista = query()
			.select("leadqualificacao.cdleadqualificacao, leadqualificacao.nome")
			.where("UPPER(" + funcaoTiraacento + "(leadqualificacao.nome)) LIKE ?", Util.strings.tiraAcento(nome).toUpperCase())
			.list();
		
		if(lista != null && lista.size() > 1){
			throw new SinedException("Mais de uma qualifica��o encontrada.");
		} else if(lista != null && lista.size() == 1){
			return lista.get(0);
		} else return null;
	}
	

}
