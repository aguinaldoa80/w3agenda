package br.com.linkcom.sined.geral.dao;

import java.sql.Date;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.FechamentoFolha;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.FechamentoFolhaFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FechamentoFolhaDAO extends GenericDAO<FechamentoFolha> {

	@Override
	public void updateListagemQuery(QueryBuilder<FechamentoFolha> query, FiltroListagem _filtro) {
		FechamentoFolhaFiltro filtro = (FechamentoFolhaFiltro) _filtro;
		query
			.select("fechamentoFolha.cdFechamentoFolha, fechamentoFolha.dtInicio, fechamentoFolha.dtFim, " +
					"empresa.cdpessoa, empresa.nome, empresa.nomefantasia, empresa.razaosocial ")
			.leftOuterJoin("fechamentoFolha.empresa empresa")
			.where("fechamentoFolha.empresa = ?", filtro.getEmpresa())
			.where("fechamentoFolha.dtInicio >= ?", filtro.getDtInicio())
			.where("fechamentoFolha.dtFim <= ?", filtro.getDtFim())
			.orderBy("fechamentoFolha.dtInicio DESC");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<FechamentoFolha> query) {
		query
			.select("fechamentoFolha.cdFechamentoFolha, fechamentoFolha.dtInicio, fechamentoFolha.dtFim, " +
					"empresa.cdpessoa, empresa.nome, empresa.nomefantasia, empresa.razaosocial ")
			.leftOuterJoin("fechamentoFolha.empresa empresa");
	}

	public boolean existeFechamentoPeriodo(FechamentoFolha bean) {
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(FechamentoFolha.class)
			.where("fechamentoFolha.empresa =?", bean.getEmpresa())
			.where("fechamentoFolha.dtInicio = ?", bean.getDtInicio())
			.where("fechamentoFolha.dtFim = ?", bean.getDtFim())
			.where("fechamentoFolha <> ?", bean, bean.getCdFechamentoFolha()!=null)
			.unique() > 0;
	}
	
	public boolean existeFechamentoPeriodo(Empresa empresa, Date data) {
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(FechamentoFolha.class)
			.where("fechamentoFolha.empresa = ?", empresa)
			.where("fechamentoFolha.dtFim >= ?", data)
			.unique() > 0;
	}	
	
}