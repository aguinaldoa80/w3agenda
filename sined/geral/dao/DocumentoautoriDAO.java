package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoautori;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentoautoriDAO extends GenericDAO<Documentoautori>{

	/**
	 * Deleta a autoriza��o do documento estornado.
	 * 
	 * @param documento
	 * @author Rodrigo Freitas
	 */
	public void deleteAutorizacao(Documento documento) {
		if (documento == null || documento.getCddocumento() == null) {
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("DELETE FROM DOCUMENTOAUTORI WHERE CDDOCUMENTO = " + documento.getCddocumento());
	}

	public boolean hasAutorizacao(Documento documento) {
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.from(Documentoautori.class)
				.where("documento = ?", documento)
				.unique() > 0;
	}
}
