package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Tiporelacaorecurso;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("tiporelacaorecurso.nome")
public class TiporelacaorecursoDAO extends GenericDAO<Tiporelacaorecurso>{

	/**
	 * Carrega a lista de tipo de rela��o entre recursos para ser exibida no m�dulo de or�amentos em Flex.
	 *
	 * @return List<Tiporelacaorecurso>
	 * @author Rodrigo Alvarenga
	 */
	public List<Tiporelacaorecurso> findAllForFlex(){
		return query()
			.select("tiporelacaorecurso.cdtiporelacaorecurso, tiporelacaorecurso.nome")
			.orderBy("tiporelacaorecurso.nome")
			.list();
	}
}
