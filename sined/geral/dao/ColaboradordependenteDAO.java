package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradordependente;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("colaboradordependente.nome")
public class ColaboradordependenteDAO extends GenericDAO<Colaboradordependente> {

	@Override
	public void updateListagemQuery(QueryBuilder<Colaboradordependente> query, FiltroListagem _filtro) {
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Colaboradordependente> query) {
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
	}
	

	/* singleton */
	private static ColaboradordependenteDAO instance;
	public static ColaboradordependenteDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradordependenteDAO.class);
		}
		return instance;
	}
	
	/**
	 * M�todo que retorna a quantidade de dependentes do colaborador
	 *
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 */
	public Integer getQtdeDependentes(Colaborador colaborador) {
		if(colaborador == null || colaborador.getCdpessoa() == null)
			throw new SinedException("O par�metro colaborador n�o pode ser nulo.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Colaboradordependente.class)
			.join("colaboradordependente.colaborador colaborador")
			.where("colaborador = ?", colaborador)
			.unique().intValue();
	}
}