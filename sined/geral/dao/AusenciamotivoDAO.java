package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Ausenciamotivo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.AusenciamotivoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("ausenciamotivo.motivo")
public class AusenciamotivoDAO extends GenericDAO<Ausenciamotivo>{

	@Override
	public void updateListagemQuery(QueryBuilder<Ausenciamotivo> query, FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O parametro _filtro n�o pode ser null.");
		}
		
		AusenciamotivoFiltro filtro = (AusenciamotivoFiltro) _filtro;
		
		query
			.select("ausenciamotivo.cdausenciamotivo, ausenciamotivo.motivo")
			.whereLikeIgnoreAll("ausenciamotivo.motivo", filtro.getMotivo())
			.orderBy("ausenciamotivo.cdausenciamotivo desc");
	}

	/**
	 * M�todo que busca os motivos das ausencias para o combo no Flex 
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Ausenciamotivo> findForComboFlex() {
		return query()
				.select("ausenciamotivo.cdausenciamotivo, ausenciamotivo.motivo")
				.list();
	}
	
}
