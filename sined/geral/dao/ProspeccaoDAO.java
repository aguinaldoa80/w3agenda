package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Prospeccao;
import br.com.linkcom.sined.geral.service.InteracaoService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ProspeccaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProspeccaoDAO extends GenericDAO<Prospeccao> {
	
	private InteracaoService interacaoService;
	
	public void setInteracaoService(InteracaoService interacaoService) {
		this.interacaoService = interacaoService;
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Prospeccao> query) {
		query.leftOuterJoinFetch("prospeccao.interacao interacao")
			.leftOuterJoinFetch("interacao.listaInteracaoitem listaInteracaoitem")
			.leftOuterJoinFetch("listaInteracaoitem.arquivo arquivo")
			.leftOuterJoin("prospeccao.contato contato")
			.leftOuterJoin("prospeccao.colaborador colaborador")
			.join("prospeccao.cliente cliente")
			.orderBy("listaInteracaoitem.data");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Prospeccao> query, FiltroListagem _filtro) {
		ProspeccaoFiltro filtro = (ProspeccaoFiltro) _filtro;
		
		query
		.select("distinct cliente.nome, prospeccao.data, contato.nome, colaborador.nome, prospeccao.cdprospeccao, " +
//				"listaProposta.numero, listaProposta.sufixo, " +
				"prospeccao.descricao ")
		.join("prospeccao.cliente cliente")
		.leftOuterJoin("prospeccao.contato contato")
		.leftOuterJoin("prospeccao.colaborador colaborador")
		.leftOuterJoin("prospeccao.listaProposta listaProposta")
		.where("prospeccao.data >= ?",filtro.getDtInicio())
		.where("prospeccao.data <= ?",filtro.getDtFim())
		.where("cliente = ?",filtro.getCliente())
		.where("contato = ?", filtro.getContato())
		.where("colaborador = ?", filtro.getColaborador())
		.whereLikeIgnoreAll("prospeccao.descricao", filtro.getDescricao())
		.where("listaProposta.numero = ?",filtro.getNumero())
		.where("listaProposta.sufixo = ?",filtro.getAno())
		.orderBy("prospeccao.data DESC")
		.ignoreJoin("listaProposta")
		;
	}
	
	@Override
	public void saveOrUpdate(Prospeccao bean) {
		
		if (bean.getInteracao() != null) {
			interacaoService.saveOrUpdate(bean.getInteracao());
		}
		super.saveOrUpdate(bean);
	}
	
	
	/**
	 * Carrega a prospec��o com o cdinteracao preenchido. 
	 * 
	 * @param prospeccao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Prospeccao loadWithInteracao(Prospeccao prospeccao){
		if (prospeccao == null || prospeccao.getCdprospeccao() == null) {
			throw new SinedException("Prospec��o n�o pode ser nula.");
		}
		return query()
					.select("prospeccao.cdprospeccao, interacao.cdinteracao")
					.leftOuterJoin("prospeccao.interacao interacao")
					.where("prospeccao = ?",prospeccao)
					.unique();
	}

	/**
	 * Faz o update na intera��o da prospec��o.
	 * 
	 * @param prospeccao
	 * @author Rodrigo Freitas
	 */
	public void updateInteracao(Prospeccao prospeccao) {
		if (prospeccao == null || prospeccao.getCdprospeccao() == null) {
			throw new SinedException("Prospecc��o n�o pode ser nula.");
		}
		getHibernateTemplate().bulkUpdate("update Prospeccao p set interacao = ? where p.id = ?", 
				new Object[]{prospeccao.getInteracao(),prospeccao.getCdprospeccao()});
	}
	
	/**
	 * Carrega uma lista com a lista de proposta prenchida com numero e sufixo.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Prospeccao> loadWithListaProposta(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			return new ArrayList<Prospeccao>();
		}
		return query()
					.select("cliente.nome, prospeccao.data, contato.nome, colaborador.nome, prospeccao.cdprospeccao, " +
							"prospeccao.descricao, listaProposta.numero, listaProposta.sufixo")
					.join("prospeccao.cliente cliente")
					.leftOuterJoin("prospeccao.listaProposta listaProposta")
					.leftOuterJoin("prospeccao.contato contato")
					.leftOuterJoin("prospeccao.colaborador colaborador")
					.whereIn("prospeccao.cdprospeccao",whereIn)
					.list();
	}

	/**
	 * Carrega as informa��es a serem carregadas na cria��o de uma proposta.
	 * 
	 * @param prospeccao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Prospeccao loadForNew(Prospeccao prospeccao) {
		if (prospeccao == null || prospeccao.getCdprospeccao() == null) {
			throw new SinedException("Prospeccao n�o pode ser nulo.");
		}
		return query()
					.select("prospeccao.cdprospeccao, cliente.cdpessoa, cliente.nome, prospeccao.descricao, contato.cdpessoa")
					.join("prospeccao.cliente cliente")
					.leftOuterJoin("prospeccao.contato contato")
					.where("prospeccao = ?",prospeccao)
					.unique();
	}
	
	
	public Boolean verificaProspeccaoPorContato(Contato contato){
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.from(Prospeccao.class)
				.where("prospeccao.contato = ?", contato)
				.unique() > 0;
		
	}
}
