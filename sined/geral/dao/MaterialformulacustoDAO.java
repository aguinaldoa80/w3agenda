package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialformulacusto;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("materialformulacusto.ordem")
public class MaterialformulacustoDAO extends GenericDAO<Materialformulacusto>{

	/**
	* M�todo que busca a lista de f�rmula para c�lculo do custo
	*
	* @param material
	* @return
	* @since 18/05/2015
	* @author Luiz Fernando
	*/
	public List<Materialformulacusto> findByMaterial(Material material) {
		return query()
					.select("materialformulacusto.cdmaterialformulacusto, materialformulacusto.ordem, " +
							"materialformulacusto.identificador, materialformulacusto.formula")
					.where("materialformulacusto.material = ?", material)
					.orderBy("materialformulacusto.ordem, materialformulacusto.identificador")
					.list();
	}

	public void deleteFormulacustoByMaterial(Material material) {
		if(material != null && material.getCdmaterial() != null){
			getJdbcTemplate().execute("delete from materialformulacusto where cdmaterial = " + material.getCdmaterial());
		}
	}

}
