package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Grupotributacaonaturezaoperacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class GrupotributacaonaturezaoperacaoDAO extends GenericDAO<Grupotributacaonaturezaoperacao>{

	public List<Grupotributacaonaturezaoperacao> findByGrupotributacao(Grupotributacao grupotributacao){
		return query()
				.where("grupotributacaonaturezaoperacao.grupotributacao = ?", grupotributacao)
				.list();
	}
}
