package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.sined.geral.bean.Apontamento;
import br.com.linkcom.sined.geral.bean.ApontamentoTipo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorescala;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Requisicaoestado;
import br.com.linkcom.sined.geral.bean.Tipocargo;
import br.com.linkcom.sined.geral.bean.enumeration.ApontamentoSituacaoEnum;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.ApontamentoFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.DiarioObraRecursosReportBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.DiarioObraSubReportBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("apontamento.dtapontamento desc")
public class ApontamentoDAO extends GenericDAO<Apontamento>{

	@Override
	public void updateListagemQuery(QueryBuilder<Apontamento> query, FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		}
		
		ApontamentoFiltro filtro = (ApontamentoFiltro)_filtro;
		
		query
			.select("distinct apontamento.cdapontamento, apontamento.qtdehoras, apontamento.dtapontamento, " +
					"planejamento.descricao, tarefa.descricao, apontamento.descricao, colaborador.nome, projeto.nome, apontamento.situacao")
			.leftOuterJoin("apontamento.colaborador colaborador")
			.leftOuterJoin("apontamento.tarefa tarefa")
			.leftOuterJoin("apontamento.contrato contrato")
			.leftOuterJoin("apontamento.cliente cliente")
			.leftOuterJoin("apontamento.atividadetipo atividadetipo")
			.leftOuterJoin("apontamento.planejamento planejamento")
			.leftOuterJoin("apontamento.apontamentoTipo apontamentoTipo")
			.leftOuterJoin("planejamento.projeto projeto")
			.leftOuterJoin("apontamento.cargo cargo")
			.leftOuterJoin("apontamento.requisicao requisicao")
			.where("projeto = ?", filtro.getProjeto())
			.where("planejamento = ?", filtro.getPlanejamento())
			.where("tarefa = ?", filtro.getTarefa())
			.where("atividade = ?", filtro.getAtividade())
			.where("colaborador = ?", filtro.getColaborador())
			.where("cargo = ?", filtro.getCargo())
			.where("apontamento.dtapontamento >= ?", filtro.getDtapontamentoDe())
			.where("apontamento.dtapontamento <= ?", filtro.getDtapontamentoAte())
			.where("atividadetipo = ?", filtro.getAtividadetipo())
			.where("contrato = ?", filtro.getContrato())
			.where("requisicao = ?", filtro.getRequisicao())
			.orderBy("apontamento.dtapontamento desc");
		
		if(filtro.getCliente() != null){
			query
				.openParentheses()
					.where("cliente = ?", filtro.getCliente())
					.or()
					.where("projeto.cliente = ?", filtro.getCliente())
				.closeParentheses();
		}
		
		if (filtro.getListaSituacao()!=null && !filtro.getListaSituacao().isEmpty()){
			query.openParentheses();
			query.where("1=2");
			for (GenericBean genericBean: filtro.getListaSituacao()){
				query.or();
				query.where("apontamento.situacao=?", ApontamentoSituacaoEnum.valueOf(genericBean.getId().toString()));
			}
			query.closeParentheses();
		}
	}
	
	/**
	 * Busca os registros para a listagem.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @param asc 
	 * @param orderBy 
	 */
	public List<Apontamento> findListagem(String whereIn, String orderBy, boolean asc){
		QueryBuilder<Apontamento> query = query()
											.select("apontamento.cdapontamento, apontamento.dtapontamento, planejamento.descricao, tarefa.descricao, " +
													"colaborador.nome, apontamento.descricao, apontamento.qtdehoras, apontamentoTipo.cdapontamentotipo, " +
													"apontamentoHoras.hrinicio, apontamentoHoras.hrfim, projeto.nome, contrato.descricao, apontamento.situacao, " +
													"atividadetipo.cdatividadetipo, listaContratocolaborador.cdcontratocolaborador, colaboradorcontrato.cdpessoa, " +
													"listaContratocolaborador.valorhora ")
											.leftOuterJoin("apontamento.colaborador colaborador")
											.leftOuterJoin("apontamento.tarefa tarefa")
											.leftOuterJoin("apontamento.listaApontamentoHoras apontamentoHoras")
											.leftOuterJoin("apontamento.planejamento planejamento")
											.leftOuterJoin("planejamento.projeto projeto")
											.leftOuterJoin("apontamento.apontamentoTipo apontamentoTipo")
											.leftOuterJoin("apontamento.atividadetipo atividadetipo")
											.leftOuterJoin("apontamento.contrato contrato")
											.leftOuterJoin("contrato.listaContratocolaborador listaContratocolaborador")
											.leftOuterJoin("listaContratocolaborador.colaborador colaboradorcontrato")
											.whereIn("apontamento.cdapontamento", whereIn);
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("apontamento.dtapontamento desc, apontamentoHoras.hrinicio asc");
		}
		
		return query.list();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Apontamento> query) {
		query
			.select("apontamento.cdapontamento, apontamento.descricao, apontamento.dtapontamento, apontamento.qtdehoras, projeto.cdprojeto, projeto.nome, " +
					"planejamento.cdplanejamento, tarefa.cdtarefa, colaborador.cdpessoa, colaborador.nome, apontamentoTipo.cdapontamentotipo, apontamento.somentetotalhoras, " +
					"cargo.cdcargo, material.cdmaterial, material.nome, " +
				"apontamentoHoras.cdapontamentohoras, apontamentoHoras.hrinicio, apontamentoHoras.hrfim, atividadetipo.cdatividadetipo," +
				"contrato.cdcontrato, apontamento.descricao , apontamento.valorhora, tipocargo.cdtipocargo, requisicao.cdrequisicao, apontamento.situacao , cliente.cdpessoa, cliente.nome")
				
			.leftOuterJoin("apontamento.colaborador colaborador")
			.leftOuterJoin("apontamento.requisicao requisicao")
			.leftOuterJoin("apontamento.tarefa tarefa")
			.leftOuterJoin("apontamento.contrato contrato")
			.leftOuterJoin("apontamento.cliente cliente")
			.leftOuterJoin("apontamento.atividadetipo atividadetipo")
			.leftOuterJoin("apontamento.planejamento planejamento")
			.leftOuterJoin("planejamento.projeto projeto")
			.leftOuterJoin("apontamento.tipocargo tipocargo")
			.leftOuterJoin("apontamento.cargo cargo")
			.leftOuterJoin("apontamento.material material")
			.join("apontamento.apontamentoTipo apontamentoTipo")
			.leftOuterJoin("apontamento.listaApontamentoHoras apontamentoHoras")
		;
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaApontamentoHoras");
	}
	
	/**
	 * Carrega a lista de material que foi usada no projeto naquele dia.
	 *
	 * @param dia
	 * @param projeto
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<DiarioObraSubReportBean> findForDiarioObraMaterial(Date dia, Projeto projeto){
		if(dia == null || projeto == null || projeto.getCdprojeto() == null){
			throw new SinedException("Erro na passagem de parametro.");
		}
		SinedUtil.markAsReader();
		List<DiarioObraSubReportBean> listaUpdate = getJdbcTemplate().query(
				"SELECT M.NOME AS NOME, SUM(A.QTDEHORAS) AS QTDE " +
				"FROM APONTAMENTO A " +
				"JOIN MATERIAL M ON M.CDMATERIAL = A.CDMATERIAL " +
				"JOIN PLANEJAMENTO P ON P.CDPLANEJAMENTO = A.CDPLANEJAMENTO " +
				"WHERE P.CDPROJETO = " + projeto.getCdprojeto() + " " +
				"AND A.DTAPONTAMENTO = to_date('" + new SimpleDateFormat("dd/MM/yyyy").format(dia) + "', 'dd/mm/yyyy') " +
				"GROUP BY M.CDMATERIAL, M.NOME " +
				"ORDER BY M.NOME", 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				DiarioObraSubReportBean bean = new DiarioObraSubReportBean();
				
				bean.setNome(rs.getString("NOME"));
				Double result = rs.getDouble("QTDE");
				bean.setQtde(result != null ? result.intValue() : 0);
				
				return bean;
			}
		});
		
		return listaUpdate;
	}

	/**
	 * Carrega a lista de cargos que foi usada no projeto naquele dia.
	 *
	 * @param dia
	 * @param projeto
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<DiarioObraSubReportBean> findForDiarioObraCargo(Date dtdia,Projeto projeto) {
		if(dtdia == null || projeto == null || projeto.getCdprojeto() == null){
			throw new SinedException("Erro na passagem de parametro.");
		}
		SinedUtil.markAsReader();
		List<DiarioObraSubReportBean> listaUpdate = getJdbcTemplate().query(
				"SELECT C.NOME AS NOME, SUM(A.QTDEHORAS) AS QTDE " +
				"FROM APONTAMENTO A " +
				"JOIN CARGO C ON C.CDCARGO = A.CDCARGO " +
				"JOIN PLANEJAMENTO P ON P.CDPLANEJAMENTO = A.CDPLANEJAMENTO " +
				"WHERE P.CDPROJETO = " + projeto.getCdprojeto() + " " +
				"AND A.DTAPONTAMENTO = to_date('" + new SimpleDateFormat("dd/MM/yyyy").format(dtdia) + "', 'dd/mm/yyyy') " +
				"GROUP BY C.CDCARGO, C.NOME " +
				"ORDER BY C.NOME", 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				DiarioObraSubReportBean bean = new DiarioObraSubReportBean();
				
				bean.setNome(rs.getString("NOME"));
				Double result = rs.getDouble("QTDE");
				bean.setQtde(result != null ? result.intValue() : 0);
				
				return bean;
			}
		});
		
		return listaUpdate;
	}
	
	/**
	 * Retorna uma lista de apontamento
	 * 
	 * @param whereIn
	 * @return lista de apontamento
	 * @throws SinedException - quando o par�metro whereIn for nulo ou vazio.
	 * 
	 * @author Jo�o Paulo Zica
	 * 
	 */
	public List<Apontamento> findByWhereIn(String whereIn){
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Par�metro whereIn inv�lido.");
		}
		
		return 
			query()
			.select("apontamento.cdapontamento, apontamento.dtapontamento, apontamento.qtdehoras, projeto.cdprojeto, " +
				"planejamento.cdplanejamento, tarefa.cdtarefa, colaborador.cdpessoa, apontamentoTipo.cdapontamentotipo, " +
				"cargo.cdcargo, material.cdmaterial, materialgrupo.cdmaterialgrupo, materialtipo.cdmaterialtipo, " +
				"apontamentoHoras.cdapontamentohoras, apontamentoHoras.hrinicio, apontamentoHoras.hrfim, atividadetipo.cdatividadetipo," +
				"contrato.cdcontrato, cliente.cdpessoa")
				
			.leftOuterJoin("apontamento.colaborador colaborador")
			.leftOuterJoin("apontamento.tarefa tarefa")
			.leftOuterJoin("apontamento.contrato contrato")
			.leftOuterJoin("contrato.cliente cliente")
			.leftOuterJoin("apontamento.atividadetipo atividadetipo")
			.join("apontamento.planejamento planejamento")
			.join("planejamento.projeto projeto")
			.leftOuterJoin("apontamento.cargo cargo")
			.leftOuterJoin("apontamento.material material")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.materialtipo materialtipo")
			.join("apontamento.apontamentoTipo apontamentoTipo")
			
			.leftOuterJoin("apontamento.listaApontamentoHoras apontamentoHoras")
			.whereIn("apontamento.cdapontamento", whereIn)
			.list();
	}


	/**
	 * Soma a quantidade de horas apontadas para aquele colaborador no contrato.
	 *
	 * @param colaborador
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 * @param data 
	 */
	@SuppressWarnings("unchecked")
	public Double getTotalHoras(Colaboradorescala colaboradorescala) {
		String sql = "SELECT SUM(A.QTDEHORAS) AS SOMA " +
					"FROM APONTAMENTO A " +
					"WHERE A.CDCOLABORADOR = ? " + 
					"AND A.CDCONTRATO = ? " +
					"AND A.DTAPONTAMENTO = ? " +
					(colaboradorescala.getHorainicio() != null ? "AND (SELECT MIN(HRINICIO) FROM APONTAMENTOHORAS AH WHERE AH.CDAPONTAMENTO = A.CDAPONTAMENTO) >= ? " : "" )+
					(colaboradorescala.getHorafim() != null ? "AND (SELECT MIN(HRINICIO) FROM APONTAMENTOHORAS AH WHERE AH.CDAPONTAMENTO = A.CDAPONTAMENTO) <= ?" : "");
		
		Object[] arrayParam = null;
		
		if(colaboradorescala.getHorainicio() != null && colaboradorescala.getHorafim() != null){
			arrayParam = new Object[]{colaboradorescala.getColaborador().getCdpessoa(), 
					colaboradorescala.getContrato().getCdcontrato(), 
					colaboradorescala.getDtescala(),
					new Timestamp(colaboradorescala.getHorainicio().getTime()),
					new Timestamp(colaboradorescala.getHorafim().getTime())};
		} else if(colaboradorescala.getHorainicio() != null){
			arrayParam = new Object[]{colaboradorescala.getColaborador().getCdpessoa(), 
					colaboradorescala.getContrato().getCdcontrato(), 
					colaboradorescala.getDtescala(),
					new Timestamp(colaboradorescala.getHorainicio().getTime())};
		} else if(colaboradorescala.getHorafim() != null){
			arrayParam = new Object[]{colaboradorescala.getColaborador().getCdpessoa(), 
					colaboradorescala.getContrato().getCdcontrato(), 
					colaboradorescala.getDtescala(),
					new Timestamp(colaboradorescala.getHorafim().getTime())};
		} else {
			arrayParam = new Object[]{colaboradorescala.getColaborador().getCdpessoa(), 
					colaboradorescala.getContrato().getCdcontrato(), 
					colaboradorescala.getDtescala()};
		}
		

		SinedUtil.markAsReader();
		List<Double> listaUpdate = getJdbcTemplate().query(sql, arrayParam,
									new RowMapper() {
										public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
											return rs.getDouble("SOMA");
										}
							
									});
		
		if(listaUpdate == null || listaUpdate.size() == 0){
			return 0.0;
		} else {
			return listaUpdate.get(0);
		}
	}

	/**
	 * Retorna o total de horas de recursos gerais ou recursos humanos para a listagem de apontamento.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ApontamentoDAO#montaWhere
	 * 
	 * @param rh
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public Double getTotalRgHoras(ApontamentoFiltro filtro) {

		StringBuilder sql = new StringBuilder("SELECT SUM(A.QTDEHORAS) AS SOMA " +
											"FROM APONTAMENTO A " +
											"LEFT OUTER JOIN CONTRATO C ON C.CDCONTRATO = A.CDCONTRATO " +
											"LEFT OUTER JOIN PLANEJAMENTO PL ON PL.CDPLANEJAMENTO = A.CDPLANEJAMENTO " +
											"WHERE A.CDAPONTAMENTOTIPO = 2 ");
		
		List<Object> listaParametro = this.montaWhere(sql, filtro);

		SinedUtil.markAsReader();
		List<Double> listaUpdate = getJdbcTemplate().query(sql.toString(), listaParametro.toArray(),
					new RowMapper() {
						public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
							return rs.getDouble("SOMA");
						}
			
					});
		
		if(listaUpdate == null || listaUpdate.size() == 0){
			return 0.0;
		} else {
			return listaUpdate.get(0);
		}
	}
	
	@SuppressWarnings("unchecked")
	public Double getTotalRhQtde(ApontamentoFiltro filtro) {
		
		StringBuilder sql = new StringBuilder("SELECT SUM(A.QTDEHORAS) AS SOMA " +
				"FROM APONTAMENTO A " +
				"LEFT OUTER JOIN CONTRATO C ON C.CDCONTRATO = A.CDCONTRATO " +
				"LEFT OUTER JOIN PLANEJAMENTO PL ON PL.CDPLANEJAMENTO = A.CDPLANEJAMENTO " +
				"WHERE A.CDAPONTAMENTOTIPO = 3 ");
		
		List<Object> listaParametro = this.montaWhere(sql, filtro);

		SinedUtil.markAsReader();
		List<Double> listaUpdate = getJdbcTemplate().query(sql.toString(), listaParametro.toArray(),
				new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getDouble("SOMA");
			}
			
		});
		
		if(listaUpdate == null || listaUpdate.size() == 0){
			return 0.0;
		} else {
			return listaUpdate.get(0);
		}
	}
	
	/**
	 * Retorna o total de minutos de recursos gerais ou recursos humanos para a listagem de apontamento. 
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ApontamentoDAO#montaWhere
	 * 
	 * @param rh
	 * @param filtro
	 * @return
	 * @author Giovane Freitas
	 */
	@SuppressWarnings("unchecked")
	public long getTotalRhMinutos(ApontamentoFiltro filtro) {
		
		StringBuilder sql = new StringBuilder("SELECT SUM( EXTRACT (EPOCH FROM (AH.HRFIM - AH.HRINICIO) )::int/60 ) AS SOMA " +
				"FROM APONTAMENTO A " +
				"INNER JOIN APONTAMENTOHORAS AH ON AH.CDAPONTAMENTO = A.CDAPONTAMENTO " +
				"LEFT OUTER JOIN CONTRATO C ON C.CDCONTRATO = A.CDCONTRATO " +
				"LEFT OUTER JOIN PLANEJAMENTO PL ON PL.CDPLANEJAMENTO = A.CDPLANEJAMENTO " +
				"WHERE A.CDAPONTAMENTOTIPO = 1 " +
				"AND (A.SOMENTETOTALHORAS = FALSE OR A.SOMENTETOTALHORAS IS NULL) ");

		SinedUtil.markAsReader();
		List<Object> listaParametro = this.montaWhere(sql, filtro);
		long minutos_com_horario = getJdbcTemplate().queryForLong(sql.toString(), listaParametro.toArray());
		
		StringBuilder sql2 = new StringBuilder("SELECT SUM(A.QTDEHORAS) AS SOMA " +
				"FROM APONTAMENTO A " +
				"LEFT OUTER JOIN CONTRATO C ON C.CDCONTRATO = A.CDCONTRATO " +
				"LEFT OUTER JOIN PLANEJAMENTO PL ON PL.CDPLANEJAMENTO = A.CDPLANEJAMENTO " +
				"WHERE A.CDAPONTAMENTOTIPO = 1 " +
				"AND A.SOMENTETOTALHORAS = TRUE ");

		List<Object> listaParametro2 = this.montaWhere(sql2, filtro);

		SinedUtil.markAsReader();
		List<Double> listaUpdate = getJdbcTemplate().query(sql2.toString(), listaParametro2.toArray(),
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						return rs.getDouble("SOMA");
					}
				});
		
		Double horas_sem_horario;
		if(listaUpdate == null || listaUpdate.size() == 0){
			horas_sem_horario = 0.0;
		} else {
			horas_sem_horario = listaUpdate.get(0);
		}
		long minutos_sem_horario = new Double(horas_sem_horario * 60L).longValue();
		
		return minutos_com_horario + minutos_sem_horario;
	}

	/**
	 * Monta a condi��o de where do somat�rio de horas da listagem de apontamento.
	 *
	 * @param sql
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Object> montaWhere(StringBuilder sql, ApontamentoFiltro filtro) {
		
		List<Object> lista = new ArrayList<Object>();
		
		if(filtro.getCliente() != null && filtro.getCliente().getCdpessoa() != null){
			sql.append("AND C.CDCLIENTE = ? ");
			lista.add(filtro.getCliente().getCdpessoa());
		}
		
		if(filtro.getContrato() != null && filtro.getContrato().getCdcontrato() != null){
			sql.append("AND C.CDCONTRATO = ? ");
			lista.add(filtro.getContrato().getCdcontrato());
		}

		if(filtro.getAtividadetipo() != null && filtro.getAtividadetipo().getCdatividadetipo() != null){
			sql.append("AND A.CDATIVIDADETIPO = ? ");
			lista.add(filtro.getAtividadetipo().getCdatividadetipo());
		}
		
		if(filtro.getApontamentoTipo() != null && filtro.getApontamentoTipo().getCdapontamentotipo() != null){
			sql.append("AND A.CDAPONTAMENTOTIPO = ? ");
			lista.add(filtro.getApontamentoTipo().getCdapontamentotipo());
		}
		
		if(filtro.getProjeto() != null && filtro.getProjeto().getCdprojeto() != null){
			sql.append("AND PL.CDPROJETO = ? ");
			lista.add(filtro.getProjeto().getCdprojeto());
		}
		
		if(filtro.getPlanejamento() != null && filtro.getPlanejamento().getCdplanejamento() != null){
			sql.append("AND PL.CDPLANEJAMENTO = ? ");
			lista.add(filtro.getPlanejamento().getCdplanejamento());
		}
		
		if(filtro.getTarefa() != null && filtro.getTarefa().getCdtarefa() != null){
			sql.append("AND A.CDTAREFA = ? ");
			lista.add(filtro.getTarefa().getCdtarefa());
		}
		
		if(filtro.getCargo() != null && filtro.getCargo().getCdcargo() != null){
			sql.append("AND A.CDCARGO = ? ");
			lista.add(filtro.getCargo().getCdcargo());
		}
		
		if(filtro.getColaborador() != null && filtro.getColaborador().getCdpessoa() != null){
			sql.append("AND A.CDCOLABORADOR = ? ");
			lista.add(filtro.getColaborador().getCdpessoa());
		}
		
		if(filtro.getDtapontamentoDe() != null){
			sql.append("AND A.DTAPONTAMENTO >= ? ");
			lista.add(filtro.getDtapontamentoDe());
		}
		
		if(filtro.getDtapontamentoAte() != null){
			sql.append("AND A.DTAPONTAMENTO <= ? ");
			lista.add(filtro.getDtapontamentoAte());
		}
		
		if(filtro.getRequisicao() != null && filtro.getRequisicao().getCdrequisicao() != null){
			sql.append("AND A.CDREQUISICAO = ? ");
			lista.add(filtro.getRequisicao().getCdrequisicao());
		}
		
		return lista;
	}

	@SuppressWarnings("unchecked")
	public List<DiarioObraRecursosReportBean> getListaDiarioObra(Date dtdia, Projeto projeto, Tipocargo tipocargo) {
		String sql = "SELECT c.nome AS categoria, ah.hrfim AS fim, ah.hrinicio AS inicio, COUNT(ah.cdapontamentohoras) AS qtde " +
						"FROM apontamento a " +
						"JOIN apontamentohoras ah ON ah.cdapontamento = a.cdapontamento " +
						"JOIN planejamento p ON p.cdplanejamento = a.cdplanejamento " +
						"JOIN cargo c ON c.cdcargo = a.cdcargo " +
						"WHERE a.dtapontamento = '" + SinedDateUtils.toStringPostgre(dtdia) + "' " +
						"AND p.cdprojeto = " + projeto.getCdprojeto() + " " +
						"AND c.cdtipocargo = " + tipocargo.getCdtipocargo() + " " +
						"GROUP BY c.cdcargo, c.nome, ah.hrfim, ah.hrinicio " +
						"ORDER BY c.nome, ah.hrinicio, ah.hrfim, COUNT(ah.cdapontamentohoras)";

		SinedUtil.markAsReader();
		List<DiarioObraRecursosReportBean> listaUpdate = getJdbcTemplate().query(sql,
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new DiarioObraRecursosReportBean(rs.getString("categoria"), new Double(rs.getInt("qtde")), rs.getTimestamp("inicio"), rs.getTimestamp("fim"));
			}
		});
		
		return listaUpdate;
	}

	/**
	 * Verifica se existe um registro de Apontamento na data para o Colaborador.
	 *
	 * @param colaborador
	 * @param data
	 * @return
	 * @since Jun 10, 2011
	 * @author Rodrigo Freitas
	 */
	public Boolean existeByColaboradorData(Colaborador colaborador, Date data) {
		if(colaborador == null || colaborador.getCdpessoa() == null || data == null){
			throw new SinedException("Par�metros inv�lidos.");
		}
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.from(Apontamento.class)
					.where("apontamento.colaborador = ?", colaborador)
					.where("apontamento.dtapontamento = ?", data)
					.unique() > 0;
	}

	/**
	* M�todo que busca apontamento da tarefa
	*
	* @param cdtarefa
	* @return
	* @since Aug 16, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Apontamento> buscaApontamentoByTarefa(Integer cdtarefa, Integer cdpessoa) {
		if(cdtarefa == null || cdpessoa == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
				.select("apontamento.cdapontamento, apontamento.dtapontamento, tarefa.cdtarefa, tarefa.descricao, colaborador.cdpessoa, " +
						"empresa.cdpessoa, empresa.email")
				.leftOuterJoin("apontamento.colaborador colaborador")
				.leftOuterJoin("apontamento.tarefa tarefa")				
				.leftOuterJoin("tarefa.planejamento planejamento")
				.leftOuterJoin("planejamento.projeto projeto")
				.leftOuterJoin("projeto.empresa empresa")
				.where("tarefa.cdtarefa = ? ", cdtarefa)
				.where("colaborador.cdpessoa = ?", cdpessoa)
				.orderBy("apontamento.dtapontamento DESC")
				.list();
	}
	
	/**
	 * M�todo que retorna a lista de apontamentos de uma requisicao.
	 * Caso 'Colaborador' seja n�o nulo, filtra tamb�m por colaborador.
	 * 
	 * @param requisicao
	 * @param colaborador
	 * @return
	 * 
	 * @author Rafael Salvio Martins
	 */
	public List<Apontamento> getApontamentosByRequisicao(Requisicao requisicao, Colaborador colaborador){
		return query()
				.from(Apontamento.class)
				.select("apontamento.cdapontamento, apontamento.dtapontamento, apontamento.descricao, atividadetipo.cdatividadetipo, atividadetipo.nome, " +
						"listaApontamentoHoras.cdapontamentohoras, listaApontamentoHoras.hrinicio, listaApontamentoHoras.hrfim, " +
						"colaborador.cdpessoa, colaborador.nome, apontamento.cdautorizacaotrabalho")
				.join("apontamento.atividadetipo atividadetipo")
				.join("apontamento.colaborador colaborador")
				.join("apontamento.requisicao requisicao")
				.leftOuterJoin("apontamento.listaApontamentoHoras listaApontamentoHoras")
				.where("requisicao = ?", requisicao)
				.where("colaborador = ?", colaborador)
				.orderBy("apontamento.dtapontamento, listaApontamentoHoras.hrinicio")
				.list();
	}
	
	/**
	 * M�todo que remove m�ltiplas linhas da tabela apontamento
	 * @param whereIn
	 * 
	 * @author Rafael Salvio
	 */
	public void deleteMultiplosApontamentos(String whereIn){
		String sql = "DELETE FROM APONTAMENTO WHERE cdapontamento in ( " + whereIn + " );";
		getJdbcTemplate().execute(sql);
	}

	/**
	 * M�todo que busca os apontamentos do colaborador
	 *
	 * @param empresa
	 * @param colaborador
	 * @param dataGerada
	 * @return
	 * @author Luiz Fernando
	 * @param projeto 
	 */
	public List<Apontamento> findByColaborador(Empresa empresa, Colaborador colaborador, Date dtreferencia1, Date dtreferencia2, ApontamentoTipo apontamentoTipo, Projeto projeto, ApontamentoSituacaoEnum situacao) {
		if(colaborador == null || colaborador.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("apontamento.cdapontamento, apontamento.descricao, apontamento.dtapontamento, apontamento.qtdehoras, " +
					"colaborador.cdpessoa, colaborador.nome, " +
					"apontamentoTipo.cdapontamentotipo, apontamento.somentetotalhoras, cargo.cdcargo, material.cdmaterial, " +
					"material.nome, apontamentoHoras.cdapontamentohoras, apontamentoHoras.hrinicio, apontamentoHoras.hrfim, " +
					"atividadetipo.cdatividadetipo, contrato.cdcontrato, apontamento.descricao, cliente.cdpessoa, cliente.nome, " +
					"apontamento.valorhora, tipocargo.cdtipocargo, requisicao.cdrequisicao, " +
					"listaContratocolaborador.cdcontratocolaborador, colaboradorcontrato.cdpessoa, " +
					"listaContratocolaborador.valorhora, atividadetipocargo.cdatividadetipo, listaCargoatividade.valorhoradiferenciada, " +
					"projeto.cdprojeto, projeto.nome")
			.leftOuterJoin("apontamento.colaborador colaborador")
			.leftOuterJoin("apontamento.requisicao requisicao")
			.leftOuterJoin("apontamento.tarefa tarefa")
			.leftOuterJoin("apontamento.contrato contrato")
			.leftOuterJoin("contrato.empresa empresa")
			.leftOuterJoin("contrato.listaContratocolaborador listaContratocolaborador")
			.leftOuterJoin("listaContratocolaborador.colaborador colaboradorcontrato")
			.leftOuterJoin("contrato.cliente cliente")
			.leftOuterJoin("apontamento.atividadetipo atividadetipo")
			.leftOuterJoin("apontamento.tipocargo tipocargo")
			.leftOuterJoin("apontamento.planejamento planejamento")
			.leftOuterJoin("planejamento.projeto projeto")
			.leftOuterJoin("apontamento.cargo cargo")
			.leftOuterJoin("cargo.listaCargoatividade listaCargoatividade")
			.leftOuterJoin("listaCargoatividade.atividadetipo atividadetipocargo")
			.leftOuterJoin("apontamento.material material")
			.join("apontamento.apontamentoTipo apontamentoTipo")
			.leftOuterJoin("apontamento.listaApontamentoHoras apontamentoHoras")
			.where("projeto = ?", projeto)
			.where("apontamentoTipo = ?", apontamentoTipo)
			.where("colaborador = ?", colaborador)
			.where("dtapontamento >= ?", dtreferencia1)
			.where("dtapontamento <= ?", dtreferencia2)
			.where("apontamento.situacao = ?", situacao)
			.openParentheses()
				.where("contrato is null")
				.or()
				.openParentheses()
					.where("empresa = ?", empresa).or()
					.where("empresa is null")
				.closeParentheses()
			.closeParentheses()					
			.list();
	}
	
	/**
	 * 
	 * @param itens
	 * @author Thiago Clemente
	 * 
	 */
	public boolean autorizar(String itens){
		
		boolean isNaoResponsavelProjeto = newQueryBuilderWithFrom(Long.class)
											.select("count(*)")
											.join("apontamento.planejamento planejamento")
											.join("planejamento.projeto projeto")
											.join("projeto.colaborador colaborador")
											.whereIn("apontamento.cdapontamento", itens)
											.where("colaborador.cdpessoa<>?", SinedUtil.getUsuarioLogado().getCdpessoa())
											.unique()
											.longValue()>0;
											
		if (isNaoResponsavelProjeto){
			return false;
		}else {
			getHibernateTemplate().bulkUpdate("update Apontamento set situacao=? where id in (" + itens + ")", new Object[]{ApontamentoSituacaoEnum.AUTORIZADO});
			return true;
		}		
	}

	/**
	 * M�todo que atualiza a situa��o do apontamento
	 *
	 * @param situacao
	 * @param whereIn
	 * @author Luiz Fernando
	 */
	public void updateSituacao(ApontamentoSituacaoEnum situacao, String whereIn) {
		if(whereIn != null && !"".equals(whereIn)){
			getHibernateTemplate().bulkUpdate("update Apontamento set situacao = ? where id in (" + whereIn + ")", new Object[]{situacao});
		}
	}

	public List<Apontamento> findForFaturarByContrato(Contrato contrato) {
		if(contrato == null || contrato.getCdcontrato() == null)
			throw new SinedException("Contrato n�o pode ser nulo.");
		
		return query()
			.select("apontamento.cdapontamento, contrato.cdcontrato, requisicao.cdrequisicao ")
			.join("apontamento.requisicao requisicao")
			.join("requisicao.requisicaoestado requisicaoestado")
			.join("apontamento.contrato contrato")
			.join("apontamento.apontamentoTipo apontamentoTipo")
			.leftOuterJoin("apontamento.listaApontamentoHoras apontamentoHoras")
			.where("apontamentoTipo = ?", ApontamentoTipo.FUNCAO_HORA)
			.where("contrato = ?", contrato)
			.where("apontamento.situacao <> ?", ApontamentoSituacaoEnum.PAGO)
			.openParentheses()
				.where("requisicaoestado.cdrequisicaoestado = ?", Requisicaoestado.CONCLUIDA)
				.or()
				.where("requisicaoestado.cdrequisicaoestado = ?", Requisicaoestado.VISTO)
			.closeParentheses()					
			.list();
	}
	
	/**
	 * M�todo que busca o total de horas de apontamentos do contrato com ordem de servi�o na situa��o CONCLUIDA ou VISTO
	 *
	 * @param contrato
	 * @return
	 * @author Luiz Fernando
	 * @since 03/12/2013
	 */
	@SuppressWarnings("unchecked")
	public Double getTotalHoraByContrato(Contrato contrato) {
		if(contrato == null || contrato.getCdcontrato() == null)
			throw new SinedException("Contrato n�o pode ser nulo.");
		
		StringBuilder sql = new StringBuilder(
				" SELECT SUM(A.QTDEHORAS) AS SOMA " +
				" FROM APONTAMENTO A " +
				" JOIN CONTRATO C ON C.CDCONTRATO = A.CDCONTRATO " +
				" JOIN REQUISICAO R ON R.CDREQUISICAO = A.CDREQUISICAO " +
				" WHERE A.CDAPONTAMENTOTIPO = " + ApontamentoTipo.FUNCAO_HORA.getCdapontamentotipo() +
				" AND C.CDCONTRATO = " + contrato.getCdcontrato() +
				" AND ( R.CDREQUISICAOESTADO = " + Requisicaoestado.CONCLUIDA + " OR " +
				" R.CDREQUISICAOESTADO = " + Requisicaoestado.VISTO +
				" ) ");
		

		SinedUtil.markAsReader();
		List<Double> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getDouble("SOMA");
			}
		});
		
		return lista != null && !lista.isEmpty() ? lista.get(0) : 0.0;
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Apontamento> findAllForAtualizarRateio(Date dtinicio, Date dtfim){
		Date dtAtual = SinedDateUtils.currentDate();
		return query()
				.select("apontamento.cdapontamento, planejamento.cdplanejamento, projeto.cdprojeto, projeto.nome, " +
						"apontamentohoras.cdapontamentohoras, apontamentohoras.hrinicio, apontamentohoras.hrfim")
				.join("apontamento.planejamento planejamento")
				.join("planejamento.projeto projeto")
				.join("apontamento.listaApontamentoHoras apontamentohoras")
				.where("projeto.dtprojeto <= ?", dtAtual)
				.openParentheses()
					.where("projeto.dtfimprojeto >= ?", dtAtual)
					.or()
					.where("projeto.dtfimprojeto is null")
				.closeParentheses()
				.where("apontamento.dtapontamento >= ?", dtinicio)				
				.where("apontamento.dtapontamento <= ?", dtfim)
				.orderBy("projeto.cdprojeto")
				.list();
	}
}
