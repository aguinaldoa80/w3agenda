package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Producaoordemmaterialcampo;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoordemmaterialcampoDAO extends GenericDAO<Producaoordemmaterialcampo> {

	/**
	* M�todo que carrega os campos adicionais do item da ordem de produ��o 
	*
	* @param whereIn
	* @return
	* @since 23/02/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordemmaterialcampo> findByProducaoordemmaterial(String whereIn) {
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		return query()
					.select("producaoordemmaterialcampo.cdproducaoordemmaterialcampo, producaoordemmaterialcampo.valor, producaoordemmaterialcampo.valorboleano, " +
							"producaoetapanomecampo.nome, producaoetapanomecampo.obrigatorio, producaoetapanomecampo.tipo, " +
							"producaoetapanomecampo.cdproducaoetapanomecampo, producaoordemmaterial.cdproducaoordemmaterial ")
					.join("producaoordemmaterialcampo.producaoetapanomecampo producaoetapanomecampo")
					.join("producaoordemmaterialcampo.producaoordemmaterial producaoordemmaterial")
					.whereIn("producaoordemmaterial.cdproducaoordemmaterial", whereIn)
					.list();
	}
	
	
}
