package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.OperacaoContabilEventoPagamento;
import br.com.linkcom.sined.geral.bean.Operacaocontabil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OperacaoContabilEventoPagamentoDAO extends GenericDAO<OperacaoContabilEventoPagamento>{

	public List<OperacaoContabilEventoPagamento> findByOperacaoContabil(Operacaocontabil operacaoContabil){
		return query()
				.select("operacaoContabilEventoPagamento.cdOperacaoContabilEventoPagamento, evento.cdEventoPagamento, operacaoContabil.cdoperacaocontabil")
				.join("operacaoContabilEventoPagamento.evento evento")
				.join("operacaoContabilEventoPagamento.operacaoContabil operacaoContabil")
				.where("operacaoContabil=?", operacaoContabil)
				.list();
	}
}
