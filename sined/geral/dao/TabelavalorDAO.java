package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Tabelavalor;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.TabelavalorFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TabelavalorDAO extends GenericDAO<Tabelavalor> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Tabelavalor> query,FiltroListagem _filtro) {
		TabelavalorFiltro filtro = (TabelavalorFiltro) _filtro;
		
		query
			.select("tabelavalor.cdtabelavalor, tabelavalor.identificador, tabelavalor.descricao, tabelavalor.valor ")
			.where("tabelavalor.identificador = ?", filtro.getIdentificador())
			.whereLikeIgnoreAll("tabelavalor.descricao", filtro.getDescricao())
			.orderBy("tabelavalor.identificador")
		;
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				save.saveOrUpdateManaged("listaTabelavalorhistorico");
				
				return null;
			}
		});
	}

	/**
	* M�todo que busca as tabelas de valores de acordo com o whereIn de identificador
	*
	* @param whereInIdentificador
	* @return
	* @since 23/06/2015
	* @author Luiz Fernando
	*/
	public List<Tabelavalor> findForCalcularValorvenda(String whereInIdentificador) {
		return querySined()
				.setUseReadOnly(false)
				.select("tabelavalor.cdtabelavalor, tabelavalor.identificador, tabelavalor.valor")
				.whereIn("tabelavalor.identificador", whereInIdentificador)
				.list();
	}
	
	/**
	* M�todo que busca as tabelas de valores de acordo com o whereIn de identificador
	*
	* @param whereInIdentificador
	* @return
	* @since 23/09/2015
	* @author Luiz Fernando
	*/
	public List<Tabelavalor> findByIdentificador(String whereInIdentificador) {
		if(whereInIdentificador == null || whereInIdentificador.equals(""))
			return new ArrayList<Tabelavalor>();
		
		StringBuilder whereIn = new StringBuilder();
		for(String identificador : whereInIdentificador.split(",")){
			whereIn.append("'").append(identificador).append("',");
		}
		return query()
				.select("tabelavalor.cdtabelavalor, tabelavalor.identificador, tabelavalor.valor")
				.whereIn("tabelavalor.identificador", whereIn.substring(0, whereIn.length()-1))
				.list();
	}

	/**
	* M�todo que faz o update no campo valor da tabela de valor de acordo com o identificador
	*
	* @param identificador
	* @param valor
	* @since 23/09/2015
	* @author Luiz Fernando
	*/
	public void updateValorPorIdentificador(String identificador, Double valor) {
		getHibernateTemplate().bulkUpdate("update Tabelavalor t set t.valor = ? where t.identificador = ?", new Object[]{valor, identificador});
	}

	public List<Tabelavalor> findForAndroid(String whereIn) {
		return query()
				.select("tabelavalor.cdtabelavalor, tabelavalor.identificador, tabelavalor.valor")
				.whereIn("tabelavalor.cdtabelavalor", whereIn)
				.list();
	}
	
}
