package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaochaofabrica;
import br.com.linkcom.sined.geral.bean.enumeration.Producaochaofabricasituacao;
import br.com.linkcom.sined.modulo.producao.controller.crud.filter.ProducaochaofabricaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.rest.w3producao.producaochaofabrica.ProducaoChaoFabricaW3producaoRESTModel;

public class ProducaochaofabricaDAO extends GenericDAO<Producaochaofabrica>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Producaochaofabrica> query, FiltroListagem _filtro) {
		ProducaochaofabricaFiltro filtro = (ProducaochaofabricaFiltro) _filtro;
		
		query
			.select("producaochaofabrica.cdproducaochaofabrica, producaochaofabrica.producaochaofabricasituacao, producaoagenda.cdproducaoagenda, pneu.cdpneu")
			.join("producaochaofabrica.producaoagendamaterial producaoagendamaterial")
			.join("producaoagendamaterial.pneu pneu")
			.join("producaoagendamaterial.producaoagenda producaoagenda")
			.whereIn("pneu.cdpneu", filtro.getWhereInPneu())
			.whereIn("producaoagenda.cdproducaoagenda", filtro.getWhereInProducaoagenda())
			.orderBy("producaochaofabrica.cdproducaochaofabrica desc");
		
		if(StringUtils.isNotBlank(filtro.getWhereInPedidovenda())){
			query.join("producaoagenda.pedidovenda pedidovenda");
			query.whereIn("pedidovenda.cdpedidovenda", filtro.getWhereInPedidovenda());
		}
		
		if(filtro.getListaProducaochaofabricasituacao() != null && filtro.getListaProducaochaofabricasituacao().size() > 0){
			query.whereIn("producaochaofabrica.producaochaofabricasituacao", Producaochaofabricasituacao.listAndConcatenate(filtro.getListaProducaochaofabricasituacao()));
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<ProducaoChaoFabricaW3producaoRESTModel> findForW3producao(String whereInEmpresa) {
		String sql = "SELECT " +
						"pcf.cdproducaochaofabrica, " +
						"pa.cdpedidovenda, " +
						"pam.cdpedidovendamaterial, " +
						"pa.cdproducaoagenda, " +
						"pam.cdproducaoagendamaterial, " +
						"pam.cdmaterial, " +
						"pam.cdpneu," +
						"pa.cdcliente, " +
						"pam.cdproducaoetapa, " +
						"pvmg.cdmaterial as cdmaterialgarantido, " +
						"pvm.dtprazoentrega " +
						"FROM producaochaofabrica pcf " +
						"JOIN producaoagendamaterial pam ON pam.cdproducaoagendamaterial = pcf.cdproducaoagendamaterial " +
						"JOIN producaoagenda pa ON pa.cdproducaoagenda = pam.cdproducaoagenda " +
						"LEFT OUTER JOIN pedidovendamaterial pvm ON pvm.cdpedidovendamaterial = pam.cdpedidovendamaterial " +
						"LEFT OUTER JOIN pedidovendamaterial pvmg ON pvmg.cdpedidovendamaterial = pvm.cdpedidovendamaterialgarantido " +
						"WHERE pam.cdpneu IS NOT NULL " +
						(whereInEmpresa != null && !"".equals(whereInEmpresa) ? "AND pa.cdempresa in (" + whereInEmpresa + ") " : "") +
						"AND pcf.producaochaofabricasituacao = 0";

		SinedUtil.markAsReader();
		List<ProducaoChaoFabricaW3producaoRESTModel> lista = (List<ProducaoChaoFabricaW3producaoRESTModel>) getJdbcTemplate().query(sql, new RowMapper() {
			@Override
			public ProducaoChaoFabricaW3producaoRESTModel mapRow(ResultSet rs, int rowNum) throws SQLException {
				ProducaoChaoFabricaW3producaoRESTModel model = new ProducaoChaoFabricaW3producaoRESTModel();
				model.setCdproducaochaofabrica(rs.getInt("cdproducaochaofabrica"));
				model.setCdpedidovenda(rs.getObject("cdpedidovenda") != null ? rs.getInt("cdpedidovenda") : null);
				model.setCdpedidovendamaterial(rs.getObject("cdpedidovendamaterial") != null ? rs.getInt("cdpedidovendamaterial") : null);
				model.setCdproducaoagenda(rs.getInt("cdproducaoagenda"));
				model.setCdproducaoagendamaterial(rs.getInt("cdproducaoagendamaterial"));
				model.setCdmaterial(rs.getInt("cdmaterial"));
				model.setCdmaterialgarantido(rs.getObject("cdmaterialgarantido") != null ? rs.getInt("cdmaterialgarantido") : null);
				model.setCdpneu(rs.getInt("cdpneu"));
				model.setCdcliente(rs.getObject("cdcliente") != null ? rs.getInt("cdcliente") : null);
				model.setCdproducaoetapa(rs.getInt("cdproducaoetapa"));
				model.setDtprazoentrega(rs.getDate("dtprazoentrega"));
				return model;
			}
		});
		
		return lista;
	}

	/**
	 * Atualiza asitua��o do registro
	 *
	 * @param command
	 * @author Rodrigo Freitas
	 * @since 07/03/2016
	 */
	public void updateSituacao(Producaochaofabrica producaochaofabrica, Producaochaofabricasituacao producaochaofabricasituacao) {
		if(producaochaofabrica == null || producaochaofabrica.getCdproducaochaofabrica() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		getHibernateTemplate().bulkUpdate("update Producaochaofabrica p set p.producaochaofabricasituacao = ? where p.id = ?", new Object[]{
			producaochaofabricasituacao, producaochaofabrica.getCdproducaochaofabrica()
		});
	}

	/**
	 * Carrega informa��es para o retorno do ch�o de f�brica
	 *
	 * @param producaochaofabrica
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/07/2016
	 */
	public Producaochaofabrica loadForRetorno(Producaochaofabrica producaochaofabrica) {
		if(producaochaofabrica == null || producaochaofabrica.getCdproducaochaofabrica() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
					.select("producaochaofabrica.cdproducaochaofabrica, " +
							"producaoagendamaterial.cdproducaoagendamaterial, producaoagendamaterial.qtde, " +
							"producaoagenda.cdproducaoagenda, " +
							"pedidovendamaterial.cdpedidovendamaterial, pedidovendamaterial.dtprazoentrega, " +
							"pneu.cdpneu, " +
							"unidademedida.cdunidademedida, " +
							"material.cdmaterial, " +
							"empresa.cdpessoa, " +
							"localarmazenagemmateriaprima.cdlocalarmazenagem")
					.join("producaochaofabrica.producaoagendamaterial producaoagendamaterial")
					.join("producaoagendamaterial.producaoagenda producaoagenda")
					.join("producaoagendamaterial.pneu pneu")
					.leftOuterJoin("producaoagendamaterial.pedidovendamaterial pedidovendamaterial")
					.leftOuterJoin("producaoagendamaterial.unidademedida unidademedida")
					.leftOuterJoin("producaoagendamaterial.material material")
					.leftOuterJoin("producaoagenda.empresa empresa")
					.leftOuterJoin("empresa.localarmazenagemmateriaprima localarmazenagemmateriaprima")
					.where("producaochaofabrica = ?", producaochaofabrica)
					.unique();
	}

	/**
	 * Busca as infos a partir da agenda de produ��o
	 *
	 * @param producaoagenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 08/07/2016
	 */
	public List<Producaochaofabrica> findByProducaoagenda(Producaoagenda producaoagenda, boolean desmarkAsReader) {
		//feito isso para testar se o retorno do cho de fbrica est com algum problema de sincronizao
		//agenda de produo no est sendo atualizada para concluda aps concluso no cho de fbrica
		if(desmarkAsReader)	SinedUtil.desmarkAsReader();

		return querySined()
					.setUseReadOnly(false)
					.removeUseWhere()
					.select("producaochaofabrica.cdproducaochaofabrica, producaochaofabrica.producaochaofabricasituacao")
					.join("producaochaofabrica.producaoagendamaterial producaoagendamaterial")
					.where("producaoagendamaterial.producaoagenda = ?", producaoagenda)
					.list();
	}

	/**
	 * Atualiza a flag para impress�o autom�tica.
	 *
	 * @param producaochaofabrica
	 * @param flag
	 * @since 01/10/2019
	 * @author Rodrigo Freitas
	 */
	public void updateEmitirEtiquetaAutomatico(Producaochaofabrica producaochaofabrica, Boolean flag) {
		if(producaochaofabrica == null || producaochaofabrica.getCdproducaochaofabrica() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		getHibernateTemplate().bulkUpdate("update Producaochaofabrica p set p.emitiretiquetaautomatico = ? where p.id = ?", new Object[]{
			flag, producaochaofabrica.getCdproducaochaofabrica()
		});
	}
	
	public void updateEmitirEtiquetaAutomatico(String whereIn, Boolean flag) {
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Par�metro inv�lido.");
		}
		getHibernateTemplate().bulkUpdate("update Producaochaofabrica p set p.emitiretiquetaautomatico = ? where p.id in (" + whereIn + ")", new Object[]{
				flag
		});
	}

	/**
	 * Busca as etiquetas para impress�o autom�tica.
	 *
	 * @return
	 * @since 01/10/2019
	 * @author Rodrigo Freitas
	 */
	public List<Producaochaofabrica> findForEtiquetaAutomatica() {
		return querySined()
					
					.select("producaochaofabrica.cdproducaochaofabrica, producaochaofabrica.producaochaofabricasituacao, producaoagenda.cdproducaoagenda, pneu.cdpneu")
					.join("producaochaofabrica.producaoagendamaterial producaoagendamaterial")
					.join("producaoagendamaterial.producaoagenda producaoagenda")
					.join("producaoagendamaterial.pneu pneu")
					.where("producaochaofabrica.emitiretiquetaautomatico = ?", Boolean.TRUE)
					.orderBy("producaochaofabrica.cdproducaochaofabrica")
					.list();
	}

}
