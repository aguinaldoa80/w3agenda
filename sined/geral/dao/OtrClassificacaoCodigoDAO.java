package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.OtrClassificacaoCodigo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.OtrClassificacaoCodigoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OtrClassificacaoCodigoDAO extends GenericDAO<OtrClassificacaoCodigo>{
	
	
	@Override
	public void updateListagemQuery(QueryBuilder<OtrClassificacaoCodigo> query, FiltroListagem _filtro) {
		OtrClassificacaoCodigoFiltro filtro = (OtrClassificacaoCodigoFiltro) _filtro;	
		
			query
				.select("otrClassificacaoCodigo.cdOtrClassificacaoCodigo, otrClassificacaoCodigo.codigo, otrClassificacaoCodigo.otrDesenho, otrClassificacaoCodigo.otrServicoTipo, otrClassificacaoCodigo.codigoIntegracao," +
						" otrdesenho.cdOtrDesenho, otrservicotipo.cdOtrServicoTipo")
				.leftOuterJoin("otrClassificacaoCodigo.otrDesenho otrdesenho")
				.leftOuterJoin("otrClassificacaoCodigo.otrServicoTipo otrservicotipo")
				.whereLikeIgnoreAll("otrClassificacaoCodigo.codigo", filtro.getCodigo())
				.where("otrdesenho=?", filtro.getOtrDesenho())
				.where("otrservicotipo=?", filtro.getOtrServicoTipo());
	}
	
	
	@Override
	public void updateEntradaQuery(QueryBuilder<OtrClassificacaoCodigo> query) {
		query
			.select("otrClassificacaoCodigo.cdOtrClassificacaoCodigo, otrClassificacaoCodigo.codigo, otrClassificacaoCodigo.otrDesenho, otrClassificacaoCodigo.otrServicoTipo, otrClassificacaoCodigo.codigoIntegracao," +
					" otrdesenho.cdOtrDesenho, otrdesenho.desenho, otrservicotipo.cdOtrServicoTipo, otrservicotipo.tipoServico")
			.leftOuterJoin("otrClassificacaoCodigo.otrDesenho otrdesenho")
			.leftOuterJoin("otrClassificacaoCodigo.otrServicoTipo otrservicotipo");
		
		
	}
	
	public Boolean validaCodigo(String codigo, OtrClassificacaoCodigo bean){		 
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.from(OtrClassificacaoCodigo.class)
					.where("codigo= ?",codigo)
					.where("otrClassificacaoCodigo != ?", bean, Util.objects.isPersistent(bean))
					.unique() > 0;
	}
	
	public List<OtrClassificacaoCodigo> findAutocomplete(String codigo){
		return query()
				.select("otrClassificacaoCodigo.cdOtrClassificacaoCodigo , otrClassificacaoCodigo.codigo,otrdesenho.cdOtrDesenho, otrdesenho.desenho, otrservicotipo.cdOtrServicoTipo, otrservicotipo.tipoServico")
				.leftOuterJoin("otrClassificacaoCodigo.otrDesenho otrdesenho")
				.leftOuterJoin("otrClassificacaoCodigo.otrServicoTipo otrservicotipo")
				.whereLikeIgnoreAll("otrClassificacaoCodigo.codigo", codigo)
				.list();
		
	}
}
