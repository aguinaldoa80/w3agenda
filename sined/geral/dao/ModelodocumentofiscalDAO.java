package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Modelodocumentofiscal;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("modelodocumentofiscal.nome")
public class ModelodocumentofiscalDAO extends GenericDAO<Modelodocumentofiscal>{

	/**
	 * Carrega o modelo pelo c�digo
	 *
	 * @param codigo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/11/2014
	 */
	public Modelodocumentofiscal loadByCodigo(String codigo) {
		if(codigo == null || codigo.trim().equals("")) return null;
		
		List<Modelodocumentofiscal> lista = query()
			.select("modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, " +
					"modelodocumentofiscal.nome")
			.where("modelodocumentofiscal.codigo = ?", codigo)
			.list();
		
		if(lista != null && lista.size() == 1) return lista.get(0);
		if(lista != null && lista.size() == 0) throw new SinedException("Nenhum modelo encontrado com o c�digo " + codigo + ".");
		if(lista != null && lista.size() > 1) throw new SinedException("Mais de um modelo encontrado com o c�digo " + codigo + ".");
		return null;
	}

	/**
	 * Carrega o modelo de nota fiscal eletronica de servi�o
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/11/2014
	 */
	public Modelodocumentofiscal loadNotafiscalservicoeletronica() {
		List<Modelodocumentofiscal> lista = query()
			.select("modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, " +
					"modelodocumentofiscal.nome")
			.where("modelodocumentofiscal.notafiscalservicoeletronica = ?", Boolean.TRUE)
			.list();
		
		if(lista != null && lista.size() == 1) return lista.get(0);
		if(lista != null && lista.size() == 0) throw new SinedException("Nenhum modelo encontrado como nota fiscal de servi�o eletr�nica.");
		if(lista != null && lista.size() > 1) throw new SinedException("Mais de um modelo encontrado como nota fiscal de servi�o eletr�nica.");
		return null;
	}

	/**
	 * Carrega o modelo de nota fiscal eletronica de produto
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/11/2014
	 */
	public Modelodocumentofiscal loadNotafiscalprodutoeletronica() {
		List<Modelodocumentofiscal> lista = query()
			.select("modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, " +
					"modelodocumentofiscal.nome")
			.where("modelodocumentofiscal.notafiscalprodutoeletronica = ?", Boolean.TRUE)
			.list();
		
		if(lista != null && lista.size() == 1) return lista.get(0);
		if(lista != null && lista.size() == 0) throw new SinedException("Nenhum modelo encontrado como nota fiscal de produto eletr�nica.");
		if(lista != null && lista.size() > 1) throw new SinedException("Mais de um modelo encontrado como nota fiscal de produto eletr�nica.");
		return null;
	}
	
	/**
	 * Carrega o modelo de conhecimento de transporte
	 *
	 * @return
	 * @author Rafael Salvio
	 */
	public Modelodocumentofiscal loadConhecimentotransporteeletronico() {
		List<Modelodocumentofiscal> lista = query()
			.where("modelodocumentofiscal.conhecimentotransporteeletronico = ?", Boolean.TRUE)
			.list();
		
		if(lista != null && lista.size() == 1) return lista.get(0);
		if(lista != null && lista.size() == 0) throw new SinedException("Nenhum modelo de  cadastrado como conhecimento de transporte eletr�nico.");
		if(lista != null && lista.size() > 1) throw new SinedException("Mais de um modelo cadastrado como conhecimento de transporte eletr�nico.");
		return null;
	}
}
