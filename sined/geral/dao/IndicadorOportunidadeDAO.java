package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.sined.modulo.crm.controller.report.bean.IndicadorortunidadeBean;
import br.com.linkcom.sined.modulo.crm.controller.report.filter.IndicadoroportunidadeFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

import com.ibm.icu.text.SimpleDateFormat;

public class IndicadorOportunidadeDAO extends GenericDAO<IndicadorortunidadeBean>{
	
	/**
	 * 
	 * M�todo para carregar as intera��es de oportunidade aonde a situa��o alterada seja igual a true.
	 *
	 *@author Thiago Augusto
	 *@date 10/02/2012
	 * @param filtro
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<IndicadorortunidadeBean> carregarIndicadoresOportunidadeTrue(IndicadoroportunidadeFiltro filtro){
		if(filtro==null){
			throw new SinedException("O par�metro filtro n�o pode ser null.");
		}
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT DISTINCT ");
		sql.append("oh.cdoportunidadehistorico AS CDOPORTUNIDADEHISTORICO, o.nome AS NOME, o.valor AS VALOR, p.nome AS NOMERESPONSAVEL, ");
		sql.append("oh.dtaltera AS DATA, o.dtinicio AS INICIO, os.nome AS STATUS ");
		sql.append("FROM oportunidade o ");
		sql.append("JOIN oportunidadehistorico oh ON oh.cdoportunidade = o.cdoportunidade ");
		sql.append("JOIN oportunidadesituacao os ON os.cdoportunidadesituacao = oh.cdoportunidadesituacao ");
		sql.append("LEFT JOIN oportunidadematerial om ON om.cdoportunidade = o.cdoportunidade ");
		sql.append("JOIN usuario u ON oh.cdusuarioaltera = u.cdpessoa ");
		sql.append("JOIN pessoa p ON p.cdpessoa = u.cdpessoa ");
		sql.append("JOIN usuarioempresa ue ON ue.cdusuario = u.cdpessoa ");
		sql.append(" WHERE oh.situacaoalterada = TRUE ");
		if (filtro.getDtPeriodoDe() != null)
			sql.append(" AND oh.dtaltera >= '" + format.format(filtro.getDtref1()) + "'");
		if (filtro.getDtPeriodoAte() != null)
			sql.append(" AND oh.dtaltera <= '" + format.format(filtro.getDtref2()) + "'");
		if (filtro.getMaterial() != null)
			sql.append(" AND om.cdmaterial = " + filtro.getMaterial().getCdmaterial());
		if (filtro.getColaborador() != null)
			sql.append(" AND o.cdresponsavel = " + filtro.getColaborador().getCdpessoa());
		sql.append(" ORDER BY STATUS, DATA");

		SinedUtil.markAsReader();
		List<IndicadorortunidadeBean> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new IndicadorortunidadeBean(
						rs.getInt("CDOPORTUNIDADEHISTORICO"), 
						rs.getString("NOME"), 
						rs.getDouble("VALOR"), 
						rs.getString("NOMERESPONSAVEL"), 
						rs.getDate("DATA"), 
						rs.getDate("INICIO"), 
						rs.getString("STATUS")); 
			}
		});
		return lista;
	}
	
	/**
	 * 
	 * M�todo para carregar todas as intera��es de oportunidade.
	 *
	 *@author Thiago Augusto
	 *@date 15/02/2012
	 * @param filtro
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<IndicadorortunidadeBean> carregarIndicadoresOportunidade(IndicadoroportunidadeFiltro filtro){
		if(filtro==null){
			throw new SinedException("O par�metro filtro n�o pode ser null.");
		}
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT DISTINCT ");
		sql.append("oh.cdoportunidadehistorico AS CDOPORTUNIDADEHISTORICO, o.nome AS NOME, o.valor AS VALOR, p.nome AS NOMERESPONSAVEL, ");
		sql.append("oh.dtaltera AS DATA, o.dtinicio AS INICIO, os.nome AS STATUS ");
		sql.append("FROM oportunidade o ");
		sql.append("JOIN oportunidadehistorico oh ON oh.cdoportunidade = o.cdoportunidade ");
		sql.append("JOIN oportunidadesituacao os ON os.cdoportunidadesituacao = oh.cdoportunidadesituacao ");
		sql.append("LEFT JOIN oportunidadematerial om ON om.cdoportunidade = o.cdoportunidade ");
		sql.append("JOIN usuario u ON oh.cdusuarioaltera = u.cdpessoa ");
		sql.append("JOIN pessoa p ON p.cdpessoa = u.cdpessoa ");
		sql.append("JOIN usuarioempresa ue ON ue.cdusuario = u.cdpessoa ");
		if (filtro.getDtPeriodoDe() != null)
			sql.append(" WHERE oh.dtaltera >= '" + format.format(filtro.getDtref1()) + "'");
		if (filtro.getDtPeriodoAte() != null)
			sql.append(" AND oh.dtaltera <= '" + format.format(filtro.getDtref2()) + "'");
		if (filtro.getMaterial() != null)
			sql.append(" AND o.cdmaterial = " + filtro.getMaterial().getCdmaterial());
		if (filtro.getColaborador() != null)
			sql.append(" AND o.cdresponsavel = " + filtro.getColaborador().getCdpessoa());
		sql.append(" ORDER BY STATUS, DATA");

		SinedUtil.markAsReader();
		List<IndicadorortunidadeBean> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new IndicadorortunidadeBean(
						rs.getInt("CDOPORTUNIDADEHISTORICO"), 
						rs.getString("NOME"), 
						rs.getDouble("VALOR"), 
						rs.getString("NOMERESPONSAVEL"), 
						rs.getDate("DATA"), 
						rs.getDate("INICIO"), 
						rs.getString("STATUS")); 
			}
		});
		return lista;
	}

	/**
	 * 
	 * M�todo que busca a listagem detalhada dos indicadores selecionados
	 *
	 *@author Thiago Augusto
	 *@date 15/02/2012
	 * @param whereIn
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<IndicadorortunidadeBean> carregarListaIndicadoresPorCdOportunidadeHistorico(String whereIn){
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT DISTINCT oh.cdoportunidadehistorico AS CDOPORTUNIDADEHISTORICO, ");
		sql.append("o.nome AS NOME, o.valortotal AS VALOR, p.nome AS NOMERESPONSAVEL, oh.dtaltera AS DATA, ");
		sql.append("o.dtinicio AS INICIO, os.nome AS STATUS, oh.situacaoalterada AS SITUACAOALTERADA ");
		sql.append("FROM oportunidade o JOIN oportunidadehistorico oh ");
		sql.append("ON oh.cdoportunidade = o.cdoportunidade ");
		sql.append("JOIN oportunidadesituacao os ");
		sql.append("ON os.cdoportunidadesituacao = oh.cdoportunidadesituacao ");
		sql.append("JOIN usuario u ON oh.cdusuarioaltera = u.cdpessoa ");
		sql.append("JOIN pessoa p ON p.cdpessoa = u.cdpessoa ");
		sql.append("JOIN usuarioempresa ue ON ue.cdusuario = u.cdpessoa ");
		sql.append("WHERE oh.cdoportunidadehistorico in("+whereIn+") ");
		sql.append("ORDER BY STATUS, DATA");

		SinedUtil.markAsReader();
		List<IndicadorortunidadeBean> listaretorno = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new IndicadorortunidadeBean(
						rs.getInt("CDOPORTUNIDADEHISTORICO"), 
						rs.getString("NOME"), 
						rs.getDouble("VALOR"), 
						rs.getString("NOMERESPONSAVEL"), 
						rs.getDate("DATA"), 
						rs.getDate("INICIO"), 
						rs.getString("STATUS")); 
			}
		});
		return listaretorno;
	}
}
