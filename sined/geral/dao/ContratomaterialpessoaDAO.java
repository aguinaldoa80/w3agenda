package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Contratomaterialpessoa;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContratomaterialpessoaDAO extends GenericDAO<Contratomaterialpessoa> {

	/**
	 * Busca pelo contratomaterial.
	 *
	 * @param contratomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/01/2013
	 */
	public List<Contratomaterialpessoa> findByContratomaterial(Contratomaterial contratomaterial) {
		if(contratomaterial == null || contratomaterial.getCdcontratomaterial() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("contratomaterialpessoa.cdcontratomaterialpessoa, contratomaterialpessoa.qtde, " +
							"cargo.cdcargo, cargo.nome")
					.join("contratomaterialpessoa.cargo cargo")
					.join("contratomaterialpessoa.contratomaterial contratomaterial")
					.where("contratomaterial = ?", contratomaterial)
					.orderBy("cargo.nome")
					.list();
	}
	
}
