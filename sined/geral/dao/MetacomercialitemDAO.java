package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Metacomercial;
import br.com.linkcom.sined.geral.bean.Metacomercialitem;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MetacomercialitemDAO extends GenericDAO<Metacomercialitem> {
	
	public Boolean existsColaboradorPeriodo(Metacomercial metacomercial, Colaborador colaborador, Date dtinicio, Date dtfim) {
		if(dtinicio == null || colaborador == null || metacomercial == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
		.select("count(*)")
		.from(Metacomercialitem.class)
		.leftOuterJoin("metacomercialitem.metacomercial metacomercial")
		.leftOuterJoin("metacomercialitem.colaborador colaborador")
		.where("metacomercial <> ?", metacomercial.getCdmetacomercial() != null ? metacomercial : null)
		.where("metacomercial.dtinicio <= ?", metacomercial.getDtinicio())
		.openParentheses()
			.where("metacomercial.dtfim >= ?", metacomercial.getDtinicio())
			.or()
			.where("metacomercial.dtfim is null")
		.closeParentheses()					
		.where("colaborador = ?", colaborador)
		.unique() > 0;
	}

	public Boolean existsCargoPeriodo(Metacomercial metacomercial, Cargo cargo, Date dtinicio, Date dtfim) {
		if(dtinicio == null || cargo == null || metacomercial == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Metacomercialitem.class)
			.leftOuterJoin("metacomercialitem.metacomercial metacomercial")
			.leftOuterJoin("metacomercialitem.cargo cargo")
			.where("metacomercial <> ?", metacomercial.getCdmetacomercial() != null ? metacomercial : null)
			.where("metacomercial.dtinicio <= ?", metacomercial.getDtinicio())
			.openParentheses()
				.where("metacomercial.dtfim >= ?", metacomercial.getDtinicio())
				.or()
				.where("metacomercial.dtfim is null")
			.closeParentheses()					
			.where("cargo = ?", cargo)
			.unique() > 0;
	}
	
	/**
	 * M�todo que busca uma lista de colaboradores listados em uma metacomercial
	 * 
	 * @param metacomercial
	 * @author Danilo Guimar�es
	 */
	public List<Metacomercialitem> findForColaboradores(Metacomercial metacomercial){
		return query()
				.select("metacomercialitem.colaborador")
				.where("metacomercialitem.metacomercial = ?", metacomercial)
				.list();
	}
}
