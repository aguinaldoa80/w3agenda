package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Codigotributacao;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.MotivoCancelamentoFaturamento;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.BaixaestoqueEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Cumulativotipo;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.geral.bean.enumeration.GeracaocontareceberEnum;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NumeroNfeNaoUtilizadoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EvolucaoFaturamentoFiltro;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.ConsultarSituacaoNotaWSBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("nota.dtEmissao desc")
public class NotaDAO extends GenericDAO<Nota> {

	private NotaService notaService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private NotaFiscalServicoService notaFiscalServicoService;

	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Nota> query, FiltroListagem _filtro) {
		NotaFiltro filtro = (NotaFiltro) _filtro;
		
		if(filtro.getNumNota() != null && !filtro.getNumNota().equals("")){
			while(filtro.getNumNota().indexOf(",,") != -1){
				filtro.setNumNota(filtro.getNumNota().replaceAll(",,", ","));
			}
			
			if(filtro.getNumNota().substring(0, 1).equals(",")){
				filtro.setNumNota(filtro.getNumNota().substring(1, filtro.getNumNota().length()));
			}
			
			if(filtro.getNumNota().substring(filtro.getNumNota().length() - 1, filtro.getNumNota().length()).equals(",")){
				filtro.setNumNota(filtro.getNumNota().substring(0, filtro.getNumNota().length()-1));
			}
		}
		
		filtro.setForceCountAllFields(true);
		
		query
			.select("nota.cdNota, nota.numero, nota.dtEmissao, notaTipo.cdNotaTipo, notaTipo.nome," +
					"notaStatus.cdNotaStatus, notaStatus.nome, cliente.nome, projeto.nome, cliente.cpf, cliente.cnpj ")
	
			.leftOuterJoin("nota.notaTipo notaTipo")
			.leftOuterJoin("nota.projeto projeto")
			.leftOuterJoin("nota.empresa empresa")
			.leftOuterJoin("nota.cliente cliente")
			.leftOuterJoin("nota.notaStatus notaStatus")
			
			.where("empresa = ?", filtro.getEmpresa())
			.whereIn("nota.numero", filtro.getNumNota() != null && !filtro.getNumNota().equals("") ? "'" + filtro.getNumNota().replaceAll(",", "','") + "'" : null)
//			.where("nota.numero = ?", filtro.getNumNota())
			.where("nota.dtEmissao >= ?", filtro.getDtEmissaoInicio())
			.where("nota.dtEmissao <= ?", filtro.getDtEmissaoFim())
			.where("cliente = ?", filtro.getCliente())
			.where("projeto = ?", filtro.getProjeto())
			.whereIn("notaStatus.cdNotaStatus", filtro.getListaNotaStatus() != null ? CollectionsUtil.listAndConcatenate(filtro.getListaNotaStatus(), "cdNotaStatus", ",") : null)
			.whereIn("notaTipo.cdNotaTipo", filtro.getListaNotaTipo() != null ? CollectionsUtil.listAndConcatenate(filtro.getListaNotaTipo(), "cdNotaTipo", ",") : null);
		
		if (filtro.getDtPagamentoInicio() != null && filtro.getDtPagamentoFim() != null)
			query.where("nota.cdNota in (select n.cdNota from Nota n join n.listaNotaDocumento lnd join lnd.documento d join d.listaMovimentacaoOrigem lmo join lmo.movimentacao m where m.dtmovimentacao >= ? and m.dtmovimentacao <= ?)", new Object[]{filtro.getDtPagamentoInicio(), filtro.getDtPagamentoFim()});
		else if (filtro.getDtPagamentoInicio() != null)
			query.where("nota.cdNota in (select n.cdNota from Nota n join n.listaNotaDocumento lnd join lnd.documento d join d.listaMovimentacaoOrigem lmo join lmo.movimentacao m where m.dtmovimentacao >= ?)", filtro.getDtPagamentoInicio());
		else if (filtro.getDtPagamentoFim() != null)
			query.where("nota.cdNota in (select n.cdNota from Nota n join n.listaNotaDocumento lnd join lnd.documento d join d.listaMovimentacaoOrigem lmo join lmo.movimentacao m where m.dtmovimentacao <= ?)", filtro.getDtPagamentoFim());
		
		if(filtro.getOrderBy() == null || filtro.getOrderBy().equals("")){
			query.orderBy("nota.dtEmissao desc");
		}
		
		if(filtro.getMostraComNumero() != null){
			if(filtro.getMostraComNumero()){
				query
					.where("nota.numero is not null")
					.where("nota.numero <> ?", "");
			} else {
				query
					.openParentheses()
					.where("nota.numero is null")
					.or()
					.where("nota.numero = ?", "")
					.closeParentheses();
			}
		}
	}

	/**
	 * Retorna uma lista de Notas, cada uma com sua lista de NotaDocumento.
	 * 
	 * @param itensSelecionados : Deve ser uma string de id's separados por v�rgula
	 * @return List<Nota>
	 * @throws SinedException : Se o par�metro itensSelecionados for nulo.
	 * @author Hugo Ferreira
	 */
	public List<Nota> carregarItensParaAcao(String itensSelecionados) {
		if (StringUtils.isBlank(itensSelecionados)) {
			throw new SinedException("Par�metros inv�lidos. O campo itensSelecionados n�o pode ser nulo.");
		}

		QueryBuilder<Nota> query = query();

		query
			.select("nota.cdNota, nota.numero, notaStatus.cdNotaStatus, documento.cddocumento, documentoacao.cddocumentoacao, notaTipo.cdNotaTipo")
			.join("nota.notaStatus notaStatus")
			.leftOuterJoin("nota.notaTipo notaTipo")
			.leftOuterJoin("nota.listaNotaDocumento notaDocumento")
			.leftOuterJoin("notaDocumento.documento documento")
			.leftOuterJoin("documento.documentoacao documentoacao")
			.whereIn("nota.cdNota", itensSelecionados);

		return query.list();
	}

	/**
	 * Persiste as altera��es de estado de uma nota GARANTINDO a TRANSA��O.
	 * 
	 * @see #alterarStatusAcaoSemTransacao(Nota, NotaHistorico, boolean)
	 * 
	 * @param nota
	 * @param notaHistorico 
	 * @param cancelarDocumentos: informa se � para cancelar os documentos gerados pela
	 * 			nota. Note que n�o h� valida��o para cancelamento de documento. Este passo
	 * 			deve ser garantido antes de se invocar este m�todo
	 * 
	 * @author Hugo Ferreira
	 */
	public Boolean alterarStatusAcao(final Nota nota, final String observacao, final MotivoCancelamentoFaturamento motivoCancelamentoFaturamento) {
		Object sucesso = getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(final TransactionStatus status) {
				return notaService.alterarStatusAcaoSemTransacao(nota, observacao, motivoCancelamentoFaturamento);
			}
		});
		
		return (Boolean) sucesso;
	}

	/**
	 * Faz um update no campo notaStatus de uma Nota.
	 * 
	 * @param nota
	 * @param novoStatus
	 * @author Hugo Ferreira
	 */
	public void atualizaNotaStatus(Nota nota, NotaStatus novoStatus) {
		if (nota == null || nota.getCdNota() == null) {
			throw new SinedException("O par�metro Nota n�o pode ser nulo");
		}
		if (novoStatus == null || novoStatus.getCdNotaStatus() == null) {
			throw new SinedException("O par�metro novoStatus n�o pode ser nulo.");
		}

//		getHibernateTemplate().bulkUpdate("update Nota n set n.notaStatus = ? where n = ?", 
//				new Object[]{novoStatus, nota});
		
		getJdbcTemplate().execute("UPDATE NOTA SET CDNOTASTATUS = " + novoStatus.getCdNotaStatus() + " WHERE CDNOTA = " + nota.getCdNota());
	}
	
	/**
	 * Faz um update no campo notaStatus para LIQUIDADA e no numero de uma Nota.
	 * �til na liquida��o da nota.
	 * 
	 * @param nota
	 * @param novoStatus
	 * 
	 * @author Hugo Ferreira
	 */
	public void atualizaStatus(Nota nota) {
		if (nota == null || nota.getCdNota() == null) {
			throw new SinedException("O par�metro Nota n�o pode ser nulo");
		}
		
		NotaStatus notaStatus = NotaStatus.LIQUIDADA;
		if(nota.getNotaStatus() != null && nota.getNotaStatus().getCdNotaStatus() != null){
			if(nota.getNotaStatus().equals(NotaStatus.NFSE_EMITIDA)){
				notaStatus = NotaStatus.NFSE_LIQUIDADA;
			}
			if(nota.getNotaStatus().equals(NotaStatus.NFE_EMITIDA)){
				notaStatus = NotaStatus.NFE_LIQUIDADA;
			}
		}
		
		getHibernateTemplate().bulkUpdate("update Nota n set n.notaStatus = ? where n = ?",  new Object[]{notaStatus, nota});
	}
	
	/**
	 * Persiste as informa��es inerentes � liquida��o de uma nota fiscal
	 * GARANTINDO a TRANSA��O.
	 *
	 * @see br.com.linkcom.sined.geral.service.NotaService#salvarNotaLiquidadaSemTransacao(Nota, Documento)
	 * 
	 * @param nota
	 * @param contaReceber
	 * 
	 * @return true, se todas as opera��es forem executadas com sucesso;
	 * 		   false, caso alguma opera��o falhe.
	 * 
	 * @author Hugo Ferreira
	 */
	public boolean salvarNotaLiquidada(final List<Nota> nota, final Documento contaReceber) {
		Object object = transactionTemplate.execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				return notaService.salvarNotaLiquidadaSemTransacao(nota, contaReceber);
			}
		});
		return (Boolean)object;
	}


	/**
	 * Verifica se o whereIn passado possui alguma nota fiscal do tipo produto.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean haveNFProduto(String whereIn) {
		if (StringUtils.isBlank(whereIn)) {
			throw new SinedException("Par�metros inv�lidos. O campo whereIn n�o pode ser nulo.");
		}
		
		return newQueryBuilderSined(Long.class)
				.from(Nota.class)
				.select("count(*)")
				.setUseTranslator(false)
				.join("nota.notaTipo notaTipo")
				.where("notaTipo = ?",NotaTipo.NOTA_FISCAL_PRODUTO)
				.whereIn("nota.cdNota", whereIn)
				.unique() > 0;
	}


	/**
	 * Carrega as notas para verifica��o na a��o de gerar receita autom�tica.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Nota> findForReceita(String whereIn) {
		if (StringUtils.isBlank(whereIn)) {
			throw new SinedException("Par�metros inv�lidos. O campo whereIn n�o pode ser nulo.");
		}
		
		return query()
					.select("nota.cdNota, nota.numero, listaNotaContrato.cdNotaContrato, listaNotavenda.cdnotavenda, notaStatus.cdNotaStatus, " +
							"notaTipo.cdNotaTipo, cliente.cdpessoa, empresa.cdpessoa, prazopagamentofatura.cdprazopagamento," +
							"contaboleto.cdconta, contacarteira.cdcontacarteira ")
					.leftOuterJoin("nota.listaNotaContrato listaNotaContrato")
					.leftOuterJoin("nota.listaNotavenda listaNotavenda")
					.join("nota.notaStatus notaStatus")
					.join("nota.notaTipo notaTipo")
					.leftOuterJoin("nota.cliente cliente")
					.leftOuterJoin("nota.empresa empresa")
					.leftOuterJoin("nota.prazopagamentofatura prazopagamentofatura")
					.leftOuterJoin("nota.contaboleto contaboleto")
					.leftOuterJoin("nota.contacarteira contacarteira")
					.whereIn("nota.cdNota", whereIn)
					.list();
	}


	/**
	 * Liquida a nota.
	 *
	 * @param nota
	 * @author Rodrigo Freitas
	 */
	public void liquidarNota(Nota nota) {
		if (nota == null || nota.getCdNota() == null) {
			throw new SinedException("O par�metro Nota n�o pode ser nulo");
		}
		
		NotaStatus notaStatus = NotaStatus.LIQUIDADA;
		if(nota.getNotaStatus() != null && nota.getNotaStatus().getCdNotaStatus() != null){
			if(nota.getNotaStatus().equals(NotaStatus.NFSE_EMITIDA)){
				notaStatus = NotaStatus.NFSE_LIQUIDADA;
			}else if(nota.getNotaStatus().equals(NotaStatus.NFE_EMITIDA)){
				notaStatus = NotaStatus.NFE_LIQUIDADA;
			}
		}
		
		getJdbcTemplate().execute("UPDATE NOTA SET CDNOTASTATUS = " + notaStatus.getCdNotaStatus() + " WHERE CDNOTA = " + nota.getCdNota());
		nota.setNotaStatus(notaStatus);
	}


	/**
	 * Carrega informa��es para o relat�rio de evolu��o de faturamento
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Nota> findForEvolucao(EvolucaoFaturamentoFiltro filtro) {
		QueryBuilder<Nota> query = query()
					.select("nota.cdNota, notaTipo.cdNotaTipo, nota.dtEmissao")
					.join("nota.notaTipo notaTipo")
					.leftOuterJoin("nota.empresa empresa")
					.openParentheses()
						.where("nota.notaStatus = ?", NotaStatus.LIQUIDADA)
						.or()
						.where("nota.notaStatus = ?", NotaStatus.NFE_LIQUIDADA)
						.or()
						.where("nota.notaStatus = ?", NotaStatus.NFSE_LIQUIDADA)
					.closeParentheses()
					.where("nota.dtEmissao >= ?", filtro.getDtinicio())
					.where("nota.dtEmissao <= ?", filtro.getDtfim())
					.openParentheses()
						.where("nota.cdNota in (select nfp.cdNota from Notafiscalproduto nfp where nfp.modeloDocumentoFiscalEnum = ?)", ModeloDocumentoFiscalEnum.NFE)
						.or()
						.where("notaTipo = ?", NotaTipo.NOTA_FISCAL_SERVICO)
					.closeParentheses();
		
		if(filtro.isPrincipal()){
			query
				.openParentheses()
				.where("empresa = ?", filtro.getEmpresa())
				.or()
				.where("empresa = null")
				.closeParentheses();
		} else {
			query
			.where("empresa = ?", filtro.getEmpresa());
		}
		
		return query.list();
	}


	@SuppressWarnings("unchecked")
	public Money getValorCumulativoContrato(Contrato contrato, Faixaimpostocontrole controle) {
		
		String sql = "SELECT N.CDNOTA, N.CDNOTATIPO " +
					"FROM NOTA N " +
					"JOIN NOTACONTRATO NC ON NC.CDNOTA = N.CDNOTA " +
					"WHERE NC.CDCONTRATO = " + contrato.getCdcontrato() + " " +
					"AND N.CDNOTA > ( " +
					"SELECT COALESCE(MAX(NOTA.CDNOTA),0)  " +
					"FROM NOTA  ";
		
		if(controle.equals(Faixaimpostocontrole.ISS)){
			sql += "WHERE NOTA.IMPOSTOCUMULATIVOISS = true)";
		} else if(controle.equals(Faixaimpostocontrole.INSS)){
			sql += "WHERE NOTA.IMPOSTOCUMULATIVOINSS = true)";
		} else if(controle.equals(Faixaimpostocontrole.IR)){
			sql += "WHERE NOTA.IMPOSTOCUMULATIVOIR = true)";
		} else if(controle.equals(Faixaimpostocontrole.COFINS)){
			sql += "WHERE NOTA.IMPOSTOCUMULATIVOCOFINS = true)";
		} else if(controle.equals(Faixaimpostocontrole.CSLL)){
			sql += "WHERE NOTA.IMPOSTOCUMULATIVOCSLL = true)";
		} else if(controle.equals(Faixaimpostocontrole.PIS)){
			sql += "WHERE NOTA.IMPOSTOCUMULATIVOPIS = true)";
		} else if(controle.equals(Faixaimpostocontrole.ICMS)){
			sql += "WHERE NOTA.IMPOSTOCUMULATIVOICMS = true)";
		}
		
		SinedUtil.markAsReader();
		List<Nota> lista = getJdbcTemplate().query(sql, 
			new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					Nota nota = new Nota();
					
					nota.setCdNota(rs.getInt("CDNOTA"));
					nota.setNotaTipo(new NotaTipo(rs.getInt("CDNOTATIPO")));
					
					return nota;
				}
	
			});
		
		Money soma = new Money(0);
		
		Map<NotaTipo, List<Nota>> map = notaService.separarNotaPorTipo(lista);
		List<Nota> listaNfProduto = map.get(NotaTipo.NOTA_FISCAL_PRODUTO);
		List<Nota> listaNfServico = map.get(NotaTipo.NOTA_FISCAL_SERVICO);
		
		listaNfServico = notaFiscalServicoService.carregarInfoParaCalcularTotal(listaNfServico);
		listaNfProduto = notafiscalprodutoService.carregarInfoParaCalcularTotal(listaNfProduto);
		
		NotaFiscalServico nfs;
//		Notafiscalproduto nfp;
//		
//		for (Nota nota : listaNfProduto) {
//			nfp = (Notafiscalproduto) nota;
//			soma = soma.add(nfp.getValorTotalProdutos());
//		}
		
		for (Nota nota : listaNfServico) {
			nfs = (NotaFiscalServico) nota;
			soma = soma.add(nfs.getValorTotalServicos());
		}
		
		return soma;
	}
	
	@SuppressWarnings("unchecked")
	public Money getValorCumulativoMesCorrente(Faixaimpostocontrole controle, NotaTipo notatipo, Cliente cliente, Cumulativotipo cumulativotipo, Codigotributacao codigotributacao, Nota bean) {
		
		String cnpj = cliente.getCnpj() != null ? cliente.getCnpj().getValue() : null;
		if (cnpj != null && cnpj.length() > 7)
			cnpj = cnpj.substring(0,8);
		
		//Se for contabilidade centralizada, carrega as notas das filiais tamb�m
		boolean contabilidadecentralizada = cliente.getContabilidadecentralizada() != null && cnpj != null && cnpj.length() == 8 ? cliente.getContabilidadecentralizada() : false;
		
		String sql = "SELECT N.CDNOTA, N.CDNOTATIPO " +
					     "FROM NOTA N " +
					     "JOIN NOTAFISCALSERVICO NFS ON NFS.CDNOTA = N.CDNOTA " +
					     "JOIN NOTASTATUS NS ON NS.CDNOTASTATUS = N.CDNOTASTATUS " +
						 "JOIN CLIENTE C ON N.CDCLIENTE = C.CDPESSOA " +
						 "JOIN PESSOA P ON C.CDPESSOA = P.CDPESSOA ";
		
		sql +=	 "WHERE 1 = 1 " +
				 (cumulativotipo != null && cumulativotipo.equals(Cumulativotipo.DIA) ? "AND EXTRACT(DAY FROM DTEMISSAO) = EXTRACT(DAY FROM CURRENT_DATE) " : " ") +
				 "AND EXTRACT(MONTH FROM DTEMISSAO) = EXTRACT(MONTH FROM CURRENT_DATE) " +
				 "AND EXTRACT(YEAR FROM DTEMISSAO) = EXTRACT(YEAR FROM CURRENT_DATE) " +
				 "AND NS.CDNOTASTATUS NOT IN ( " + NotaStatus.CANCELADA.getCdNotaStatus() + ", " + NotaStatus.EM_ESPERA.getCdNotaStatus() + ") ";
		
		if(bean != null && bean.getCdNota() != null){
			sql += " AND N.CDNOTA <> " + bean.getCdNota() + " ";
		}
		if(codigotributacao!=null && codigotributacao.getCdcodigotributacao()!=null)
			sql += " AND NFS.CDCODIGOTRIBUTACAO = "+codigotributacao.getCdcodigotributacao()+" ";		
		
		if (contabilidadecentralizada)
			sql += "AND P.CNPJ LIKE '" + cnpj + "%' ";
		else	
			sql += "AND N.CDCLIENTE = " + cliente.getCdpessoa() + " ";
		
		if(controle.equals(Faixaimpostocontrole.ISS)){
			sql += " AND (NFS.INCIDEISS = false OR NFS.INCIDEISS IS NULL) ";
		} else if(controle.equals(Faixaimpostocontrole.INSS)){
			sql += " AND (NFS.INCIDEINSS = false OR NFS.INCIDEINSS IS NULL) ";
		} else if(controle.equals(Faixaimpostocontrole.IR)){
			sql += " AND (NFS.INCIDEIR = false OR NFS.INCIDEIR IS NULL) ";
		} else if(controle.equals(Faixaimpostocontrole.COFINS)){
			sql += " AND (NFS.INCIDECOFINS = false OR NFS.INCIDECOFINS IS NULL) ";
		} else if(controle.equals(Faixaimpostocontrole.CSLL)){
			sql += " AND (NFS.INCIDECSLL = false OR NFS.INCIDECSLL IS NULL) ";
		} else if(controle.equals(Faixaimpostocontrole.PIS)){
			sql += " AND (NFS.INCIDEPIS = false OR NFS.INCIDEPIS IS NULL) ";
		} else if(controle.equals(Faixaimpostocontrole.ICMS)){
			sql += " AND (NFS.INCIDEICMS = false OR NFS.INCIDEICMS IS NULL) ";
		}
						
		sql += "AND N.CDNOTA > ( " +
			    "SELECT COALESCE(MAX(N1.CDNOTA),0)  " +
				"FROM NOTA N1 " +
				"JOIN CLIENTE C1 ON N1.CDCLIENTE = C1.CDPESSOA " +
				"JOIN PESSOA P1 ON C1.CDPESSOA = P1.CDPESSOA " +				
				"WHERE ";
		
		if (contabilidadecentralizada)
			sql += " P1.CNPJ LIKE '" + cnpj + "%' ";
		else	
			sql += " N1.CDCLIENTE = " + cliente.getCdpessoa() + " ";
		
		if(controle.equals(Faixaimpostocontrole.ISS)){
			sql += " AND N1.IMPOSTOCUMULATIVOISS = true)";
		} else if(controle.equals(Faixaimpostocontrole.INSS)){
			sql += " AND N1.IMPOSTOCUMULATIVOINSS = true)";
		} else if(controle.equals(Faixaimpostocontrole.IR)){
			sql += " AND N1.IMPOSTOCUMULATIVOIR = true)";
		} else if(controle.equals(Faixaimpostocontrole.COFINS)){
			sql += " AND N1.IMPOSTOCUMULATIVOCOFINS = true)";
		} else if(controle.equals(Faixaimpostocontrole.CSLL)){
			sql += " AND N1.IMPOSTOCUMULATIVOCSLL = true)";
		} else if(controle.equals(Faixaimpostocontrole.PIS)){
			sql += " AND N1.IMPOSTOCUMULATIVOPIS = true)";
		} else if(controle.equals(Faixaimpostocontrole.ICMS)){
			sql += " AND N1.IMPOSTOCUMULATIVOICMS = true)";
		}		

		SinedUtil.markAsReader();
		List<Nota> lista = getJdbcTemplate().query(sql, 
			new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					Nota nota = new Nota();
					
					nota.setCdNota(rs.getInt("CDNOTA"));
					nota.setNotaTipo(new NotaTipo(rs.getInt("CDNOTATIPO")));
					
					return nota;
				}
	
			});
		
		Money soma = new Money(0);		
		Map<NotaTipo, List<Nota>> map = notaService.separarNotaPorTipo(lista);
		
		if (notatipo.equals(NotaTipo.NOTA_FISCAL_PRODUTO)){
			List<Nota> listaNfProduto = map.get(NotaTipo.NOTA_FISCAL_PRODUTO);
			listaNfProduto = notafiscalprodutoService.carregarInfoParaCalcularTotal(listaNfProduto);
			Notafiscalproduto nfp;			
			for (Nota nota : listaNfProduto) {
				nfp = (Notafiscalproduto) nota;
				soma = soma.add(nfp.getValorNota());
			}	
		} 
		
		if (notatipo.equals(NotaTipo.NOTA_FISCAL_SERVICO)){
			List<Nota> listaNfServico = map.get(NotaTipo.NOTA_FISCAL_SERVICO);			
			listaNfServico = notaFiscalServicoService.carregarInfoParaCalcularTotal(listaNfServico);
			NotaFiscalServico nfs;			
			for (Nota nota : listaNfServico) {
				nfs = (NotaFiscalServico) nota;
				soma = soma.add(nfs.getValorTotalServicos()!=null ? nfs.getValorTotalServicos() : new Money(0));
			}
		}		
		
		return soma;
	}	
	
	public boolean isValorCumulativoCobrado(Faixaimpostocontrole controle, NotaTipo notatipo, Cliente cliente, Cumulativotipo cumulativotipo, Nota nota){
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
									.select("count(*)")
									.from(Nota.class)
									.join("nota.cliente cliente")
									.join("nota.notaStatus notaStatus")
									.where("nota.notaTipo = ?", notatipo)
									.where("EXTRACT(DAY FROM nota.dtEmissao) = EXTRACT(DAY FROM CURRENT_DATE)", cumulativotipo != null && cumulativotipo.equals(Cumulativotipo.DIA))
									.where("EXTRACT(MONTH FROM nota.dtEmissao) = EXTRACT(MONTH FROM CURRENT_DATE)")
									.where("EXTRACT(YEAR FROM nota.dtEmissao) = EXTRACT(YEAR FROM CURRENT_DATE)")
									.where("notaStatus.cdNotaStatus <> ?", NotaStatus.CANCELADA.getCdNotaStatus());
		
		if(nota != null && nota.getCdNota() != null){
			query.where("nota.cdNota <> ?", nota.getCdNota());
		}
		
		String cnpj = cliente.getCnpj() != null ? cliente.getCnpj().getValue() : null;
		if (cnpj != null && cnpj.length() > 7)
			cnpj = cnpj.substring(0,8);
		
		//Se for contabilidade centralizada, carrega as notas das filiais tamb�m
		boolean contabilidadecentralizada = cliente.getContabilidadecentralizada() != null && cnpj != null && cnpj.length() == 8 ? cliente.getContabilidadecentralizada() : false;		
		
		if (contabilidadecentralizada)
			query.where("cliente.cnpj like '" + cnpj + "%'");
		else
			query.where("nota.cliente = ?", cliente);
		
		if(controle.equals(Faixaimpostocontrole.ISS)){
			query.where("nota.impostocumulativoiss = ?", Boolean.TRUE);
		} else if(controle.equals(Faixaimpostocontrole.INSS)){
			query.where("nota.impostocumulativoinss = ?", Boolean.TRUE);
		} else if(controle.equals(Faixaimpostocontrole.IR)){
			query.where("nota.impostocumulativoir = ?", Boolean.TRUE);
		} else if(controle.equals(Faixaimpostocontrole.COFINS)){
			query.where("nota.impostocumulativocofins = ?", Boolean.TRUE);
		} else if(controle.equals(Faixaimpostocontrole.CSLL)){
			query.where("nota.impostocumulativocsll = ?", Boolean.TRUE);
		} else if(controle.equals(Faixaimpostocontrole.PIS)){
			query.where("nota.impostocumulativopis = ?", Boolean.TRUE);
		} else if(controle.equals(Faixaimpostocontrole.ICMS)){
			query.where("nota.impostocumulativoicms = ?", Boolean.TRUE);
		}			

		return query.unique() > 0;
	}
	
	public Nota findForReceitaIndividual(Nota nota) {
		if(nota == null || nota.getCdNota() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
					.select("nota.cdNota, notaTipo.cdNotaTipo, nota.numero")
					.join("nota.notaTipo notaTipo")
					.where("nota = ?", nota)
					.unique();
	}
	
	public boolean haveNFDiferenteStatus(String whereIn, NotaStatus[] status) {
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
									.select("count(*)")
									.from(Nota.class)
									.setUseTranslator(false)									
									.whereIn("nota.cdNota", whereIn);
		
		for (int i = 0; i < status.length; i++) {
			query.where("nota.notaStatus <> ?", status[i]);
		}
		
		return query.unique() > 0;
	}
	
	public boolean haveNFWithStatus(String whereIn, NotaStatus[] status) {
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
									.select("count(*)")
									.from(Nota.class)
									.setUseTranslator(false)									
									.whereIn("nota.cdNota", whereIn);
		
		query.whereIn("nota.notaStatus", SinedUtil.listAndConcatenate(Arrays.asList(status), "cdNotaStatus", ","));
		
		return query.unique() > 0;
	}
	
	public List<Nota> findNotas(String whereIn) {
		return query()
					.select("nota.cdNota, nota.numero, notaStatus.cdNotaStatus,notaTipo.cdNotaTipo")
					.join("nota.notaStatus notaStatus")
					.leftOuterJoin("nota.notaTipo notaTipo")
					.whereIn("nota.cdNota", whereIn)
					.list();
	}
	
	/**
	 * Verifica se pode-se liquidar a nota.<BR>
	 * <BR>
	 * Condi��es:<BR>
	 * 	- A nota n�o pode estar com a situa��o diferente de EMITIDA
	 *
	 * @param nota
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean verificaLiquidar(Nota nota) {
		return newQueryBuilderSined(Long.class)
				.from(Nota.class)
				.setUseTranslator(false)
				.select("count(*)")
				.where("nota.notaStatus <> ?", NotaStatus.EMITIDA)
				.where("nota.notaStatus <> ?", NotaStatus.NFSE_EMITIDA)
				.where("nota.notaStatus <> ?", NotaStatus.NFE_EMITIDA)
				.where("nota = ?", nota)
				.unique().intValue() == 0;
	}
	
	/**
	 * M�todo que retorna a data da �ltima nota cadastrada no sistema
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Date getDtUltimoCadastro() {
		return newQueryBuilderSined(Date.class)
			.from(Nota.class)
			.setUseTranslator(false)
			.select("max(nota.dtEmissao)")
			.join("nota.notaStatus notaStatus")
			.where("notaStatus <> ?", NotaStatus.CANCELADA)
			.unique();
	}
	
	/**
	 * M�todo que retorna a qtde. total de notas emitidas cadastradas no sistema
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getQtdeNotasEmitidas() {
		return newQueryBuilderSined(Long.class)
			.from(Nota.class)
			.setUseTranslator(false)
			.select("count(*)")
			.openParentheses()
				.where("nota.notaStatus = ?", NotaStatus.EMITIDA).or()
				.where("nota.notaStatus = ?", NotaStatus.NFE_EMITIDA)
			.closeParentheses()
			.unique()
			.intValue();
	}
	
	/**
	 * M�todo que retorna a qtde. total de notas liquidadas cadastradas no sistema
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getQtdeNotasLiquidadas() {
		return newQueryBuilderSined(Long.class)
			.from(Nota.class)
			.setUseTranslator(false)
			.select("count(*)")
			.openParentheses()
				.where("nota.notaStatus = ?", NotaStatus.LIQUIDADA).or()
				.where("nota.notaStatus = ?", NotaStatus.NFSE_LIQUIDADA)
			.closeParentheses()
			.unique()
			.intValue();
	}
	
	/**
	 * M�todo que rertona a qtde. total de notas cadastradas no sistema
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getQtdeTotalNotas() {
		return newQueryBuilderSined(Long.class)
			.from(Nota.class)
			.setUseTranslator(false)
			.select("count(*)")
			.join("nota.notaStatus notaStatus")
			.where("notaStatus <> ?", NotaStatus.CANCELADA)
			.unique()
			.intValue();
	}
	
	/**
	 * M�todo que carrega dados da nota
	 * 
	 * @param nota
	 * @return
	 */
	public Nota carregaDadosNota(Nota nota) {
		return query()
		.select("nota.cdNota, nota.numero, nota.notaStatus, enderecoCliente.cdendereco, contaboleto.cdconta, documentotipo.cddocumentotipo, " +
				"contrato.cdcontrato, enderecocobranca.cdendereco, cliente.cdpessoa, endereco.cdendereco, enderecotipo.cdenderecotipo")
		.leftOuterJoin("nota.enderecoCliente enderecoCliente")
		.leftOuterJoin("nota.cliente cliente")
		.leftOuterJoin("cliente.listaEndereco endereco")
		.leftOuterJoin("endereco.enderecotipo enderecotipo")
		.leftOuterJoin("nota.contaboleto contaboleto")
		.leftOuterJoin("nota.documentotipo documentotipo")
		.leftOuterJoin("nota.listaNotaContrato notacontrato")
		.leftOuterJoin("notacontrato.contrato contrato")
		.leftOuterJoin("contrato.enderecocobranca enderecocobranca")
		.where("nota = ?", nota)
		.unique();
	}
	
	public void updateNumeroNota(Nota nota, Integer proxNum) {
		if(nota == null || nota.getCdNota() == null){
			throw new SinedException("Nota n�o pode ser nula.");
		}
		if(proxNum == null){
			throw new SinedException("O n�mero da nota n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE NOTA SET NUMERO = '" + proxNum + "' WHERE CDNOTA = " + nota.getCdNota());
	}

	/**
	 * Retorna uma nota caso seu estatus for cancelada.
	 * @param venda
	 * @return
	 * @author Taidson
	 * @since 13/01/2011
	 */
	public List<Nota> notaSituacaoCanceladaDenegada(Venda venda){
		return query()
					.select("nota.cdNota, notaStatus.cdNotaStatus, notaStatus.nome")
					.join("nota.listaNotavenda listaNotavenda")
					.join("listaNotavenda.venda venda")
					.join("nota.notaStatus notaStatus")
					.where("venda = ?", venda)
					.where("notaStatus <> ?", NotaStatus.CANCELADA)
					.where("notaStatus <> ?", NotaStatus.NFE_DENEGADA)
					.list();
	}
	
//	/**
//	 * Carrega todas as informa��es das notas e seus contratos 
//	 * para a gera��o de receita autom�tica.
//	 * 
//	 * @param cdNota
//	 * @return
//	 * @author Tom�s Rabelo
//	 */
//	public Nota loadForReceita(Integer cdNota) {
//		if(cdNota == null){
//			throw new SinedException("C�digo da nota n�o pode ser nulo.");
//		}
//		return query()
//					.select("nota.cdNota, cliente.cdpessoa, nota.dtEmissao, nota.numero, empresa.cdpessoa, conta.cdconta, rateio.cdrateio, taxa.cdtaxa, " +
//							"comissionamento.cdcomissionamento, comissionamento.nome, comissionamento.quantidade, comissao.cdcomissionamento, comissao.nome, comissao.quantidade, " +
//							"listaContratocolaborador.cdcontratocolaborador, colaborador.cdpessoa, vendedor.cdpessoa, " +
//							"contrato.cdcontrato")
//					.leftOuterJoin("nota.listaNotaContrato listaNotaContrato")
//					.leftOuterJoin("listaNotaContrato.contrato contrato")
//					.leftOuterJoin("nota.cliente cliente")
//					.leftOuterJoin("nota.empresa empresa")
//					.leftOuterJoin("nota.enderecoCliente enderecoCliente")
//					.leftOuterJoin("nota.notaStatus notaStatus")
//					.leftOuterJoin("contrato.conta conta")
//					.leftOuterJoin("contrato.rateio rateio")
//					.leftOuterJoin("contrato.taxa taxa")
//					.leftOuterJoin("contrato.vendedor vendedor")
//					.leftOuterJoin("contrato.comissionamento comissionamento")
//					.leftOuterJoin("contrato.listaContratocolaborador listaContratocolaborador")
//					.leftOuterJoin("listaContratocolaborador.colaborador colaborador")
//					.leftOuterJoin("listaContratocolaborador.comissionamento comissao")
//
//
//					.where("nota.cdNota = ?", cdNota)
//					.unique();
//	}
	
	/**
	 * M�todo que verifica se a nota foi gerada a partir de uma venda e se est� venda j� possui receita
	 * 
	 * @param nota
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean isNotaPossuiReceita(Nota nota, boolean considerarNotaDocumento) {
		boolean venda = newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Nota.class)
			.join("nota.listaNotavenda listaNotavenda")
			.join("listaNotavenda.venda venda")
			.join("venda.vendasituacao vendasituacao")
			.join("venda.listadocumentoorigem listadocumentoorigem")
			.where("vendasituacao <> ?", Vendasituacao.CANCELADA)
			.where("nota = ?", nota)
			.ignoreAllJoinsPath(true)
			.unique() > 0;
		
		boolean pedidovenda = false;
		if(!venda){
			pedidovenda = newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Nota.class)
				.join("nota.listaNotavenda listaNotavenda")
				.join("listaNotavenda.venda venda")
				.join("venda.pedidovenda pedidovenda")
				.join("venda.vendasituacao vendasituacao")
				.join("pedidovenda.listaDocumentoorigem listaDocumentoorigem")
				.where("vendasituacao <> ?", Vendasituacao.CANCELADA)
				.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.CANCELADO)
				.where("nota = ?", nota)
				.ignoreAllJoinsPath(true)
				.unique() > 0;
		}
		boolean notaDocumento = false;
		if(!venda && !pedidovenda && considerarNotaDocumento){
			notaDocumento = newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Nota.class)
			.join("nota.listaNotaDocumento listaNotaDocumento")
			.join("listaNotaDocumento.documento documento")
			.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
			.where("nota = ?", nota)
			.ignoreAllJoinsPath(true)
			.unique() > 0;
		}
			
		return venda || pedidovenda || notaDocumento;
	}
	
	/**
	 * M�todo que verifica se existe notas com situa��es NFS-e EMITIDA passadas por par�metro
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @param situacoes 
	 */
	public Boolean isSituacaoNE(String whereIn, NotaStatus[] situacoes) {
		if(whereIn == null || "".equals(whereIn)) throw new SinedException("Par�metro inv�lido");
		
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
						.select("count(*)")
						.from(Nota.class)
						.join("nota.notaStatus notaStatus")
						.whereIn("nota.cdNota", whereIn)
						.ignoreAllJoinsPath(true);
		
		if(situacoes != null){
			query.openParentheses();
			for (int i = 0; i < situacoes.length; i++) {
				query.where("notaStatus = ?", situacoes[i]).or();
			}
			query.closeParentheses();
		}
		
		return query.unique() > 0;
	}
	
	/**
	 * M�todo que verifica se existe origem da nota
	 *
	 * @param nota
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existOrigem(Nota nota) {
		Boolean existe = true;
		
		if(nota != null){
			existe = newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Nota.class)
			.leftOuterJoin("nota.listaNotavenda listaNotavenda")
			.leftOuterJoin("nota.listaNotaContrato listaNotaContrato")
			.openParentheses()
				.where("listaNotavenda is not null")
				.or()
				.where("listaNotaContrato is not null")
			.closeParentheses()
			.where("nota = ?", nota)
			.unique() > 0;
		}
		
		return existe;
	}
	
	/**
	 * Verifica se alguma nota tem origem de venda.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 16/07/2013
	 */
	public Boolean isOrigemVenda(String whereIn) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Nota.class)
					.join("nota.listaNotavenda listaNotavenda")
					.where("listaNotavenda is not null")
					.whereIn("nota.cdNota", whereIn)
					.unique() > 0;
	}
	
	/**
	 * M�todo que verifica se a(s) nota(s) possuiem origem
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existOrigem(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Nota n�o pode ser nula.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Nota.class)
			.leftOuterJoin("nota.listaNotavenda listaNotavenda")
			.leftOuterJoin("nota.listaNotaContrato listaNotaContrato")
			.openParentheses()
				.where("listaNotavenda is not null")
				.or()
				.where("listaNotaContrato is not null")
			.closeParentheses()
			.whereIn("nota.cdNota", whereIn)
			.unique() > 0;
	}
	
	public Boolean existOrigemVendaGeracaoContaAposEmissaoNota(String whereIn, boolean reserva) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Nota n�o pode ser nula.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Nota.class)
			.join("nota.listaNotavenda listaNotavenda")
			.join("listaNotavenda.venda venda")
			.join("venda.pedidovendatipo pedidovendatipo")
			.where("pedidovendatipo.geracaocontareceberEnum = ?", GeracaocontareceberEnum.APOS_EMISSAONOTA)
			.whereIn("nota.cdNota", whereIn)
			.where("coalesce(pedidovendatipo.reserva, false) = " + reserva)
			.unique() > 0;
	}
	
	/**
	 * M�todo que verifica se a nota tem origem e/ou o tipo de pedido seja ap�s a emiss�o da nota
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/11/2013
	 */
	public Boolean existOrigemForBaixaNota(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Nota n�o pode ser nula.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Nota.class)
			.leftOuterJoin("nota.listaNotaromaneio listaNotaromaneio")
			.leftOuterJoin("nota.listaNotavenda listaNotavenda")
			.leftOuterJoin("listaNotavenda.venda venda")
			.leftOuterJoin("venda.pedidovenda pedidovenda")
			.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
			.openParentheses()
				.openParentheses()
					.where("listaNotavenda is not null")
					.openParentheses()
						.openParentheses()
							.where("pedidovendatipo.baixaestoqueEnum <> ?", BaixaestoqueEnum.APOS_EMISSAONOTA)
							.where("pedidovendatipo.baixaestoqueEnum <> ?", BaixaestoqueEnum.NAO_BAIXAR)
						.closeParentheses()
						.or()
						.where("pedidovenda is null")
						.or()
						.where("pedidovendatipo is null")
					.closeParentheses()
				.closeParentheses()
				.or()
				.where("listaNotaromaneio is not null")
			.closeParentheses()
			.whereIn("nota.cdNota", whereIn)
			.unique() > 0;
	}
	
	/**
	 * Atualiza a data de emiss�o de uma nota.
	 *
	 * @param nota
	 * @param dtemissao
	 * @author Rodrigo Freitas
	 * @since 24/10/2012
	 */
	public void atualizaDataEmissao(Nota nota, Date dtemissao) {
		if(nota == null || nota.getCdNota() == null){
			throw new SinedException("Nota n�o pode ser nula.");
		}
		if(dtemissao == null){
			throw new SinedException("A data de emiss�o n�o pode ser nula.");
		}
		getJdbcTemplate().execute("UPDATE NOTA SET DTEMISSAO = '" + SinedDateUtils.toStringPostgre(dtemissao) + "' WHERE CDNOTA = " + nota.getCdNota());
	}
	
	/**
	 * Atualiza a flag para o cancelamento de venda.
	 *
	 * @param nota
	 * @param cancelarvenda
	 * @author Rodrigo Freitas
	 * @since 16/07/2013
	 */
	public void updateCancelamentoVenda(Nota nota, Boolean cancelarvenda) {
		getJdbcTemplate().execute("UPDATE NOTA SET CANCELARVENDA = " + cancelarvenda + " WHERE CDNOTA = " + nota.getCdNota());
	}
	
	/**
	 * 
	 * @param nota
	 * @return
	 */
	public Nota getNotaStatus(Nota nota) {
		return query()
		.select("nota.notaStatus")
		.where("nota = ?",nota)
		.unique();
	}
	
	/**
	 * Busca as notas para fazer o envio de e-mail de PDF e/ou XML para clientes
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/02/2014
	 */
	public List<Nota> findForEnvioNFCliente(String whereIn) {
		return query()
					.select("nota.cdNota, nota.numero, nota.notaimportadaxml, " +
							"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.email, " +
							"cliente.cdpessoa, cliente.nome, cliente.email, " +
							"contato.cdpessoa, contato.emailcontato, contato.nome, contato.receberboleto, contato.ativo, "+
							"contato.recebernf")
					.leftOuterJoin("nota.empresa empresa")
					.leftOuterJoin("nota.cliente cliente")
					.leftOuterJoin("cliente.listaContato listaContato")
					.leftOuterJoin("listaContato.contato contato")
					.whereIn("nota.cdNota", whereIn)
					.list();
	}
	
	public List<Nota> findForNotaRemessaLocacao (Contrato contrato){
		return query()
				.select("nota.numero, nota.dtEmissao")
				.leftOuterJoin("nota.listaNotaromaneio listaNotaromaneio")
				.leftOuterJoin("listaNotaromaneio.romaneio romaneio")
				.leftOuterJoin("romaneio.listaRomaneioorigem listaRomaneioorigem")
				.where("listaRomaneioorigem.contrato = ?", contrato)
				.list();
	}
	
	public List<Nota> findForNotaNumeroNfeNaoUtilizado(NumeroNfeNaoUtilizadoFiltro filtro){
		if (filtro == null || filtro.getEmpresa() == null || filtro.getEmpresa().getCdpessoa() == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
		.select("nota.cdNota, nota.dtEmissao, nota.notaStatus, nota.numeronota")
		.join("nota.listaArquivonfnota listaArquivonfnota")
		.join("listaArquivonfnota.arquivonf arquivonf")
		.whereIn("nota.notaStatus", NotaStatus.NFE_EMITIDA.getCdNotaStatus() +", " + NotaStatus.NFE_LIQUIDADA.getCdNotaStatus() + ", " + NotaStatus.CANCELADA.getCdNotaStatus() + ", " + NotaStatus.NFE_DENEGADA.getCdNotaStatus() + " ," + NotaStatus.NFE_CANCELANDO.getCdNotaStatus())
		.where("nota.notaTipo = ?", NotaTipo.NOTA_FISCAL_PRODUTO)
		.where("nota.dtEmissao >= ?", filtro.getDtinicioemissao())
		.where("nota.dtEmissao <= ?", filtro.getDtfimemissao())
		.where("nota.empresa = ?", filtro.getEmpresa())
		.where("arquivonf.configuracaonfe = ?", filtro.getConfiguracaonfe())
		.where("nota.numeronota IS NOT NULL")
		.list();
	}
	
	
	public List<Nota> findForNotaNumeroNfeNaoUtilizadoByNumero(String whereIn, NumeroNfeNaoUtilizadoFiltro filtro){
		if (StringUtils.isBlank(whereIn)){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
		.select("nota.cdNota, nota.numero, nota.dtEmissao, nota.notaStatus, nota.numeronota")
		.join("nota.listaArquivonfnota listaArquivonfnota")
		.join("listaArquivonfnota.arquivonf arquivonf")
		.where("nota.notaStatus NOT IN (" + NotaStatus.NFE_EMITIDA.getCdNotaStatus() +", " + NotaStatus.NFE_LIQUIDADA.getCdNotaStatus() + ", " + NotaStatus.CANCELADA.getCdNotaStatus() + ", " + NotaStatus.NFE_DENEGADA.getCdNotaStatus() + " ," + NotaStatus.NFE_CANCELANDO.getCdNotaStatus() + ")")
		.where("nota.notaTipo = ?", NotaTipo.NOTA_FISCAL_PRODUTO)
		.where("nota.dtEmissao >= ?", filtro.getDtinicioemissao())
		.where("nota.dtEmissao <= ?", filtro.getDtfimemissao())
		.where("nota.empresa = ?", filtro.getEmpresa())
		.where("arquivonf.configuracaonfe = ?", filtro.getConfiguracaonfe())
		.where("nota.numeronota IS NOT NULL")
		.whereIn("nota.numeronota", whereIn)
		.list();
	}
	
	/**
	* M�todo que carrega a nota com o cliente
	*
	* @param nota
	* @return
	* @since 22/10/2015
	* @author Luiz Fernando
	*/
	public Nota loadWithCliente(Nota nota) {
		if(nota == null || nota.getCdNota() == null)
			throw new SinedException("Nota n�o pode ser nulo.");
		
		return query()
				.select("nota.cdNota, nota.dtEmissao, nota.numero, cliente.cdpessoa, cliente.nome, cliente.cnpj, notaTipo.cdNotaTipo, empresa.cdpessoa")
				.join("nota.cliente cliente")
				.join("nota.empresa empresa")
				.join("nota.notaTipo notaTipo")
				.where("nota  = ?", nota)
				.unique();
	}
	
	@SuppressWarnings("unchecked")
	public List<ConsultarSituacaoNotaWSBean> findNotasForWSSOAP(Empresa empresa, Timestamp dataReferencia) {
		String sql = "select distinct " +
						"n.numero as numeronota, " +
						"pv.identificador as identificador, " +
						"n.cdnotatipo as tipo, " +
						"case when nfp.tipooperacaonota is not null then nfp.tipooperacaonota else 1 end as tipooperacao, " +
						"n.cdnotastatus as situacaonota, " +
						"n.dtemissao as dtemissao, " +
						"( " +
						"select max(cast(nh.dtaltera as date)) " +
						"from notahistorico nh " +
						"where nh.cdnota = n.cdnota " +
						"and nh.cdnotastatus = 3 " +
						") as dtcancelamento " +
						
						"from nota n " +
						
						"left join notavenda nv on nv.cdnota = n.cdnota " +
						"left join notafiscalprodutocoleta nc on nc.cdnotafiscalproduto = n.cdnota " +
                        "left join coleta cl on cl.cdcoleta = nc.cdcoleta " +
						"left join venda v on v.cdvenda = nv.cdvenda " +
						"left join pedidovenda pv on pv.cdpedidovenda = coalesce(v.cdpedidovenda,cl.cdpedidovenda) " +
						"left join notafiscalproduto nfp on nfp.cdnota = n.cdnota " +
						
						"where n.cdempresa = ? " +
						"and exists ( " +
						"select 1 " +
						"from notahistorico nh " +
						"where nh.cdnota = n.cdnota " +
						"and nh.dtaltera > ? " +
						")";

		SinedUtil.markAsReader();
		List<ConsultarSituacaoNotaWSBean> listaUpdate =  getJdbcTemplate().query(sql, new Object[]{empresa.getCdpessoa(), dataReferencia}, new RowMapper() {
			public ConsultarSituacaoNotaWSBean mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new ConsultarSituacaoNotaWSBean(
						rs.getString("numeronota"), 
						rs.getString("identificador"),
						rs.getInt("tipo"), 
						rs.getInt("tipooperacao"), 
						rs.getInt("situacaonota"), 
						rs.getDate("dtemissao"), 
						rs.getDate("dtcancelamento"));
			}
		});
		
		return listaUpdate;
	}
	
	/**
	* M�todo que busca as notas que tenham apenas documentos cancelados
	*
	* @param whereInDocumento
	* @return
	* @since 13/03/2017
	* @author Luiz Fernando
	*/
	public List<Nota> findLiquidadaComDocumentoCancelado(String whereInDocumento) {
		if(StringUtils.isBlank(whereInDocumento))
			throw new SinedException("Par�metro inv�lido.");
		
		List<NotaStatus> list = new ArrayList<NotaStatus>();
		list.add(NotaStatus.LIQUIDADA);
		list.add(NotaStatus.NFE_LIQUIDADA);
		list.add(NotaStatus.NFSE_LIQUIDADA);
		
		return query()
				.select("nota.cdNota, notaStatus.cdNotaStatus, documento.cddocumento ")
				.join("nota.notaStatus notaStatus")
				.join("nota.listaNotaDocumento listaNotaDocumento")
				.join("listaNotaDocumento.documento documento")
				.whereIn("notaStatus.cdNotaStatus", CollectionsUtil.listAndConcatenate(list, "cdNotaStatus", ","))
				.where("exists (select n.cdNota from NotaDocumento nd join nd.nota n join nd.documento d where n.cdNota = nota.cdNota and d.cddocumento in (" + whereInDocumento + "))")
				.where("not exists (select d.cddocumento from NotaDocumento nd join nd.nota n join nd.documento d join d.documentoacao dacao where n.cdNota = nota.cdNota and dacao.cddocumentoacao <> ?)", Documentoacao.CANCELADA.getCddocumentoacao())
				.list();
	}
	
	/**
	 * Verifica se existe retorno de canhoto para a nota passada por par�metro.
	 * 	 
	 * @param cdNota
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/10/2017
	 */
	public boolean haveRetornoCanhoto(Integer cdNota) {
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.from(Nota.class)
					.where("nota.retornocanhoto = ?", Boolean.TRUE)
					.where("nota.cdNota = ?", cdNota)
					.unique() > 0;
	}
	
	/**
	 * Atualiza a nota com retorno de canhoto true.
	 * 	 
	 * @param cdNota
	 * @author Rodrigo Freitas
	 * @since 30/10/2017
	 */
	public void updateRetornoCanhoto(Integer cdNota) {
		getJdbcTemplate().execute("UPDATE NOTA SET RETORNOCANHOTO = TRUE WHERE CDNOTA = " + cdNota);
	}
	
	/**
	 * Atualiza a nota com retorno de canhoto false.
	 * 	 
	 * @param cdNota
	 * @author Rodrigo Freitas
	 * @since 30/10/2017
	 */
	public void updateRetornoCanhotoFalse(Integer cdNota) {
		getJdbcTemplate().execute("UPDATE NOTA SET RETORNOCANHOTO = FALSE WHERE CDNOTA = " + cdNota);
	}
	
	/**
	 * Busca as notas para o estorno do registro do canhoto.
	 * 	 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/10/2017
	 */
	public List<Nota> findForEstornoCanhoto(String whereIn) {
		return query()
					.select("nota.cdNota, nota.numero, nota.retornocanhoto")
					.whereIn("nota.cdNota", whereIn)
					.orderBy("nota.numero")
					.list();
	}
	
	public void updateSerieNfe(Integer cdnotafiscalproduto, Integer serienfe){
		getJdbcTemplate().execute("UPDATE NOTA SET SERIENFE = "+serienfe+" WHERE CDNOTA = " + cdnotafiscalproduto);
	}
	
	public void updateSerieNfce(Integer cdnotafiscalproduto, Integer serieNfce) {
		getJdbcTemplate().execute("UPDATE NOTA SET SERIENFCE = " + serieNfce + " WHERE CDNOTA = " + cdnotafiscalproduto);
	}
}
