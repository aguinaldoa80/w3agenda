package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Processojuridicoandamento;
import br.com.linkcom.sined.geral.bean.Processojuridicoinstancia;
import br.com.linkcom.sined.modulo.juridico.controller.crud.filter.ProcessojuridicoandamentoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProcessojuridicoandamentoDAO extends GenericDAO<Processojuridicoandamento>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Processojuridicoandamento> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inválidos");
		}
		ProcessojuridicoandamentoFiltro filtro = (ProcessojuridicoandamentoFiltro) _filtro;
		query
			.select("processojuridicoandamento.cdprocessojuridicoandamento, processojuridicoandamento.data, " +
					"processojuridicoandamento.andamento, processojuridicoandamento.providencia, " +
					"processojuridicoandamento.complemento, " +
					"processojuridicoinstancia.cdprocessojuridicoinstancia, processojuridicoinstancia.numero, " +
					"processojuridico.cdprocessojuridico, processojuridico.sintese")
					
			.leftOuterJoin("processojuridicoandamento.processojuridicoinstancia processojuridicoinstancia")
			.leftOuterJoin("processojuridicoinstancia.processojuridico processojuridico")
			.leftOuterJoin("processojuridicoandamento.cliente cliente")
			.leftOuterJoin("processojuridicoandamento.partecontraria partecontraria")
			
			.where("processojuridicoinstancia = ?", filtro.getProcessojuridicoinstancia())
			.where("processojuridico = ?", filtro.getProcessojuridico())
			.where("cliente = ?", filtro.getCliente())
			.where("partecontraria = ?", filtro.getPartecontraria())
			.where("processojuridicoandamento.data >= ?", filtro.getDataDe())
			.where("processojuridicoandamento.data <= ?", filtro.getDataAte())
			;
		
		if(filtro.getProcessojuridico() == null && 
				filtro.getCliente() == null && 
				filtro.getPartecontraria() == null && 
				filtro.getDataDe() == null && 
				filtro.getDataAte() == null){
			query.where("1=0");
		}
		
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Processojuridicoandamento> query) {
		query
			.select("processojuridicoandamento.cdprocessojuridicoandamento, processojuridicoandamento.data," +
			"processojuridicoandamento.andamento, processojuridicoandamento.providencia, " +
			"processojuridicoinstancia.cdprocessojuridicoinstancia, processojuridicoinstancia.numero, vprocessojuridico.descricao, processojuridico.cdprocessojuridico, " +
			"processojuridicoandamento.cdusuarioaltera, processojuridicoandamento.dtaltera")
			.leftOuterJoin("processojuridicoandamento.processojuridicoinstancia processojuridicoinstancia")
			.leftOuterJoin("processojuridicoinstancia.processojuridico processojuridico")
			.leftOuterJoin("processojuridico.vprocessojuridico vprocessojuridico");
	}
	
	public List<Processojuridicoandamento> loadAndamento(Processojuridicoinstancia processojuridicoinstancia) {
		return query()
				.select("processojuridicoandamento.cdprocessojuridicoandamento, processojuridicoandamento.data," +
						"processojuridicoandamento.andamento, processojuridicoandamento.providencia, " +
						"processojuridicoandamento.complemento, processojuridicoandamento.identificador ")
				.where("processojuridicoandamento.processojuridicoinstancia = ?", processojuridicoinstancia)
				.list();		
	}
}
