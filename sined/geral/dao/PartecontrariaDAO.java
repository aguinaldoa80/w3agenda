package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Partecontraria;
import br.com.linkcom.sined.modulo.juridico.controller.crud.filter.PartecontrariaFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PartecontrariaDAO extends GenericDAO<Partecontraria> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Partecontraria> query, FiltroListagem _filtro) {
		PartecontrariaFiltro filtro = (PartecontrariaFiltro) _filtro;
		
		query
			.select("partecontraria.cdpartecontraria, partecontraria.nome, partecontraria.tipopessoa, partecontraria.cpf, partecontraria.cnpj")
			.whereLikeIgnoreAll("partecontraria.nome", filtro.getNome())
			.where("partecontraria.tipopessoa = ?", filtro.getTipopessoa())
			.where("partecontraria.cpf = ?", filtro.getCpf())
			.where("partecontraria.cnpj = ?", filtro.getCnpj());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Partecontraria> query) {
		query
			.leftOuterJoinFetch("partecontraria.municipio municipio")
			.leftOuterJoinFetch("municipio.uf uf");
	}
	
}
