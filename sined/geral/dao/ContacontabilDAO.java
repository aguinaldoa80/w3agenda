package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Composicaoorcamento;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.ContaContabilSaldoInicial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tarefaorcamento;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContagerencial;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.contabil.controller.crud.filter.ContacontabilFiltro;
import br.com.linkcom.sined.modulo.contabil.controller.report.bean.EmitirBalancoPatrimonialBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("contaContabil.nome")
public class ContacontabilDAO extends GenericDAO<ContaContabil> {
	
	private ParametrogeralService parametrogeralService;
	
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<ContaContabil> query) {
		query
			.leftOuterJoinFetch("contaContabil.contaContabilPai contaContabilPai")
			.leftOuterJoinFetch("contaContabil.tipooperacao tipooperacao")
			.leftOuterJoinFetch("contaContabil.operacaocontabil operacaocontabil")
			.leftOuterJoinFetch("contaContabil.vcontacontabil vcontacontabil")
			.leftOuterJoinFetch("contaContabil.empresa empresa");
	}

	@Override
	public void updateListagemQuery(QueryBuilder<ContaContabil> query,
			FiltroListagem _filtro) {
		ContacontabilFiltro filtro = (ContacontabilFiltro) _filtro;
		
		query.select(
				"contaContabil.cdcontacontabil, contaContabil.nome, tipooperacao.nome, contaContabil.ativo, contaContabil.formato,"
						+ "vcontacontabil.nivel, vcontacontabil.identificador, operacaocontabil.nome, operacaocontabil.cdoperacaocontabil, contaContabil.codigoalternativo, " +
						"empresa.cdpessoa, empresa.nome, empresa.nomefantasia")

				.join("contaContabil.vcontacontabil vcontacontabil")
				.leftOuterJoin("contaContabil.tipooperacao tipooperacao")
				.leftOuterJoin("contaContabil.operacaocontabil operacaocontabil")
				.leftOuterJoin("contaContabil.empresa empresa")
				.where("contaContabil.natureza = ?", filtro.getNatureza())
				.where("contaContabil.ativo = ?", filtro.getMostraativo())
				.where("contaContabil.geramovimentacao = ?", filtro.getMostramovimentacao())
				.whereLikeIgnoreAll("contaContabil.nome", filtro.getNome())
				.where("vcontacontabil.identificador like ?||'%'", filtro.getIdentificador())
				.where("tipooperacao = ?", Tipooperacao.TIPO_CONTABIL)
				.where("vcontacontabil.nivel = ?", filtro.getNivel())
				.where("operacaocontabil = ?", filtro.getOperacaocontabil())
				.where("contaContabil.codigoalternativo = ?", filtro.getCodigoalternativo())
				.where("contaContabil.natureza = ?", filtro.getNatureza());
		
		if(parametrogeralService.getBoolean(Parametrogeral.PLANO_CONTAS_CONTABIL_POR_EMPRESA)){
			query.where("empresa = ?", filtro.getEmpresa());
		}
		
		if(Boolean.TRUE.equals(filtro.getContaretificadora())){
			query.where("contaContabil.contaretificadora = true");
		}
		/*if(filtro.getContaretificadora()!=null){
			query.where("coalesce(contaContabil.contaretificadora, false) = ?" , filtro.getContaretificadora());
		}*/
		
		query.orderBy("vcontacontabil.identificador");
	}

	public ContaContabil carregaConta(ContaContabil bean) {
		if (bean == null || bean.getCdcontacontabil() == null) {
			throw new SinedException("Conta contabil n�o pode ser nula.");
		}
		
		return query()
				.select("tipooperacao.cdtipooperacao, vcontacontabil.nivel, contaContabil.cdcontacontabil, contaContabil.natureza, contaContabil.naturezaContaCompensacao, contaContabil.contaretificadora")
				.join("contaContabil.tipooperacao tipooperacao")
				.join("contaContabil.vcontacontabil vcontacontabil")
				.where("contaContabil = ?", bean)
				.unique();
	}

	public List<ContaContabil> findFilhos(ContaContabil pai) {
		if (pai == null || pai.getCdcontacontabil() == null) {
			throw new SinedException("Conta contabil n�o pode ser nula.");
		}
		return query().select("contaContabil.item, contaContabil.formato")
				.join("contaContabil.contaContabilPai pai")
				.where("pai = ?", pai).orderBy("contaContabil.item desc")
				.list();
	}

	public List<ContaContabil> findRaiz() {
		return query().select("contaContabil.item, contaContabil.formato")
				.where("contaContabilPai is null")
				.orderBy("contaContabil.item desc").list();
	}

	public List<ContaContabil> findAnaliticas(Tipooperacao tipooperacao) {
		if (tipooperacao == null || tipooperacao.getCdtipooperacao() == null) {
			throw new SinedException(
					"Os par�metros tipoperacao ou cdtipooperacao n�o podem ser null.");
		}
		return query()
				.select("contaContabil.cdcontacontabil, contaContabil.nome, pai.cdcontacontabil, "
						+ "v.arvorepai")
				.join("contaContabil.vcontacontabil v")
				.leftOuterJoin("contaContabil.contaContabilPai pai")
				.where("not exists(select C2.cdcontacontabil from contaContabil C2 "
						+ "where C2.contaContabilPai = contaContabil)")
				.where("contaContabil.ativo = ?", Boolean.TRUE)
				.where("contaContabil.tipooperacao = ?", tipooperacao)
				.orderBy("contaContabil.nome").list();

	}

	public List<ContaContabil> findAnaliticasByNatureza(
			Tipooperacao tipooperacao, String whereInNatureza) {
		if (tipooperacao == null || tipooperacao.getCdtipooperacao() == null) {
			throw new SinedException(
					"Os par�metros tipoperacao ou cdtipooperacao n�o podem ser null.");
		}
		return querySined()
				
				.select("contaContabil.cdcontacontabil, contaContabil.nome, pai.cdcontacontabil, "
						+ "v.arvorepai")
				.join("contaContabil.vcontacontabil v")
				.leftOuterJoin("contaContabil.contaContabilPai pai")
				.where("not exists(select C2.cdcontacontabil from contaContabil C2 "
						+ "where C2.contaContabilPai = contaContabil)")
				.where("contaContabil.ativo = ?", Boolean.TRUE)
				.where("contaContabil.tipooperacao = ?", tipooperacao)
				.whereIn("contaContabil.natureza", whereInNatureza)
				.orderBy("contaContabil.nome").list();

	}

	public List<ContaContabil> findByNatureza(String whereInNatureza) {
		return querySined()
				
				.select("contaContabil.cdcontacontabil, contaContabil.nome, pai.cdcontacontabil, "
						+ "v.arvorepai")
				.join("contaContabil.vcontacontabil v")
				.leftOuterJoin("contaContabil.contaContabilPai pai")
				.where("not exists(select C2.cdcontacontabil from contaContabil C2 "
						+ "where C2.contaContabilPai = contaContabil)")
				.where("contaContabil.ativo = ?", Boolean.TRUE)
				.whereIn("contaContabil.natureza", whereInNatureza)
				.orderBy("contaContabil.nome").list();

	}

	public List<ContaContabil> findAnaliticas(String whereIn,
			Tipooperacao tipooperacao) {
		if (tipooperacao == null || tipooperacao.getCdtipooperacao() == null) {
			throw new SinedException(
					"Os par�metros tipoperacao ou cdtipooperacao n�o podem ser null.");
		}
		return querySined()
				
				.select("contaContabil.cdcontacontabil, contaContabil.nome, pai.cdcontacontabil, "
						+ "v.arvorepai")
				.join("contaContabil.vcontacontabil v")
				.leftOuterJoin("contaContabil.contaContabilPai pai")
				.where("not exists(select C2.cdcontacontabil from contaContabil C2 "
						+ "where C2.contaContabilPai = contaContabil)")
				.where("contaContabil.ativo = ?", Boolean.TRUE)
				.where("contaContabil.tipooperacao = ?", tipooperacao)
				.whereLikeIgnoreAll("contaContabil.nome", whereIn)
				.orderBy("contaContabil.nome").list();
	}

	@SuppressWarnings("unchecked")
	public void updateRelacionamento(ContaContabil bean) {
		if (bean == null || bean.getCdcontacontabil() == null) {
			throw new SinedException("Conta gerencial n�o pode ser nulo.");
		}
		if (bean.getContaContabilPai() == null
				|| bean.getContaContabilPai().getCdcontacontabil() == null) {
			throw new SinedException("Conta gerencial pai n�o pode ser nulo.");
		}

		List<String> listaUpdate = getJdbcTemplate()
				.query("SELECT 'UPDATE ' || t.relname || ' SET ' ||  a.attname || ' = ? WHERE ' || a.attname || ' = ?' AS RELACIONAMENTO "
						+ "FROM pg_constraint c "
						+ "LEFT JOIN pg_class t  ON c.conrelid  = t.oid "
						+ "LEFT JOIN pg_class t2 ON c.confrelid = t2.oid "
						+ "LEFT JOIN pg_attribute a ON t.oid = a.attrelid AND a.attnum = c.conkey[1] "
						+ "LEFT JOIN pg_attribute a2 ON t2.oid = a2.attrelid AND a2.attnum = c.confkey[1] "
						+ "WHERE t2.relname = 'contacontabil' "
						+ "AND t.relname<>'contacontabil' "
						+ "AND c.contype = 'f'", new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						return rs.getString("RELACIONAMENTO");
					}

				});

		for (String string : listaUpdate) {
			getJdbcTemplate().update(
					string,
					new Object[] { bean.getCdcontacontabil(),
							bean.getContaContabilPai().getCdcontacontabil() });
		}
	}

	@SuppressWarnings("unchecked")
	public Boolean haveRegister(ContaContabil bean) {
		if (bean == null || bean.getCdcontacontabil() == null) {
			throw new SinedException("Conta cont�bil n�o pode ser nulo.");
		}

		List<String> listaString = getJdbcTemplate()
				.query("SELECT 'SELECT C1.cdcontacontabil FROM contaContabil C1 JOIN ' || t.relname || ' ON ' || t.relname || '.' || a.attname || ' = C1.cdcontacontabil WHERE NOT EXISTS(SELECT C2.cdcontacontabil FROM contaContabil C2 WHERE C2.cdcontacontabilPAI = C1.cdcontacontabil) AND C1.cdcontacontabil = XX' AS RELACIONAMENTO "
						+ "FROM pg_constraint c "
						+ "LEFT JOIN pg_class t  ON c.conrelid  = t.oid "
						+ "LEFT JOIN pg_class t2 ON c.confrelid = t2.oid "
						+ "LEFT JOIN pg_attribute a ON t.oid = a.attrelid AND a.attnum = c.conkey[1] "
						+ "LEFT JOIN pg_attribute a2 ON t2.oid = a2.attrelid AND a2.attnum = c.confkey[1] "
						+ "WHERE t2.relname = 'contacontabil' "
						+ "AND t.relname<>'contacontabil' "
						+ "AND c.contype = 'f'", new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						return rs.getString("RELACIONAMENTO");
					}

				});

		String sql = "SELECT COUNT(*) FROM (";

		boolean first = false;
		for (String string : listaString) {
			if (first) {
				sql += " UNION ALL ";
			}
			sql += string;
			first = true;
		}

		sql += ") QUERY";

		sql = sql.replaceAll("XX", bean.getCdcontacontabil().toString());

		int count = getJdbcTemplate().queryForInt(sql);

		return count != 0;
	}

	public List<ContaContabil> findByTipooperacao(Tipooperacao tipooperacao) {
		return query()
				.select("contaContabil.cdcontacontabil, contaContabil.nome, "
						+ "vcontacontabil.arvorepai, vcontacontabil.identificador, pai.cdcontacontabil")
				.join("contaContabil.vcontacontabil vcontacontabil")
				.leftOuterJoin("contaContabil.contaContabilPai pai")
				.where("contaContabil.tipooperacao = ?", tipooperacao)
				.where("contaContabil.ativo = ?", Boolean.TRUE)
				.orderBy("contaContabil.item, contaContabil.nome").list();
	}

	public List<ContaContabil> findByTipooperacaoNatureza( Tipooperacao tipooperacao, String whereInCdcontagerencial, String whereInNatureza, String whereNotInNatureza, Integer cdEmpresa) {
		QueryBuilder<ContaContabil> queryBuilder = query();
		queryBuilder
				.select("contaContabil.cdcontacontabil, contaContabil.nome, "
						+ "vcontacontabil.arvorepai, vcontacontabil.identificador, pai.cdcontacontabil, empresa.nome")
				.join("contaContabil.vcontacontabil vcontacontabil")
				.leftOuterJoin("contaContabil.empresa empresa")
				.leftOuterJoin("contaContabil.contaContabilPai pai")
				.where("contaContabil.tipooperacao = ?", tipooperacao)
				.where("contaContabil.ativo = ?", Boolean.TRUE)
				.whereIn("contaContabil.cdcontacontabil", whereInCdcontagerencial)
				.whereIn("contaContabil.natureza", whereInNatureza)
				.orderBy("vcontacontabil.identificador, contaContabil.nome");

		if(cdEmpresa != null && parametrogeralService.getBoolean(Parametrogeral.PLANO_CONTAS_CONTABIL_POR_EMPRESA)){
			queryBuilder.where("contaContabil.empresa.cdpessoa = ?", cdEmpresa);
		}
		
		if (whereNotInNatureza != null && !whereNotInNatureza.trim().equals("")) {
			queryBuilder.where("contaContabil.natureza not in ("
					+ whereNotInNatureza + ")");
		}

		return queryBuilder.list();
	}

	public List<ContaContabil> findForError(String listaContaContabil) {
		return query()
				.select("contaContabil.cdcontacontabil, contaContabil.nome")
				.whereIn("contaContabil.cdcontacontabil", listaContaContabil)
				.list();
	}

	public List<ContaContabil> findContasContabeis(String whereIn) {
		return query()
				.select("contaContabil.cdcontacontabil, contaContabil.nome, vcontacontabil.identificador")
				.join("contaContabil.vcontacontabil vcontacontabil")
				.whereIn("contaContabil.cdcontacontabil", whereIn)
				.list();
	}

	public ContaContabil findContaContabilVeiculo(Veiculo veiculo) {
		if (veiculo == null || veiculo.getCdveiculo() == null)
			throw new SinedException(
					"Parametros inv�lidos findContaGerencialVeiculo");

		return query()
				.select("contaContabil.cdcontacontabil, contaContabil.nome")
				.where("contaContabil.id = (select bp.contaContabil from Veiculo v "
						+ "join v.patrimonioitem pi "
						+ "join pi.bempatrimonio bp "
						+ "where v.id = "
						+ veiculo.getCdveiculo() + ")").unique();
	}

	public ContaContabil loadWithIdentificador(ContaContabil contacontabil) {
		return query()
				.select("contaContabil.cdcontacontabil, contaContabil.nome, vcontacontabil.identificador, vcontacontabil.arvorepai,"
						+ "contaContabil.codigoalternativo")
				.join("contaContabil.vcontacontabil vcontacontabil")
				.where("contaContabil = ?", contacontabil).unique();
	}

	public List<ContaContabil> findAllFilhos(ContaContabil cg) {
		return query()
				.select("contaContabil.cdcontacontabil, contaContabil.nome, contaContabil.contaretificadora, vcontacontabil.identificador")
				.join("contaContabil.vcontacontabil vcontacontabil")
				.where("vcontacontabil.identificador like '"
						+ cg.getVcontacontabil().getIdentificador() + "%'")
				.list();
	}

	@SuppressWarnings("unchecked")
	public ContaContabil findPaiNivel1(ContaContabil contacontabil) {

		String sql = "SELECT CG.cdcontacontabil, CG.NOME "
				+ "FROM contaContabil CG "
				+ "WHERE CG.cdcontacontabilPAI IS NULL "
				+ "AND CG.ITEM = (	SELECT CAST(replace(SUBSTR(AC.IDENTIFICADOR, 0, LENGTH(CG.FORMATO)+1), '.', '') AS INTEGER) "
				+ "FROM VCONTACONTABIL AC "
				+ "JOIN contaContabil C1 ON C1.cdcontacontabil = AC.cdcontacontabil "
				+ "WHERE AC.cdcontacontabil = "
				+ contacontabil.getCdcontacontabil() + ")";

		SinedUtil.markAsReader();
		List<ContaContabil> lista = getJdbcTemplate().query(sql,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						return new ContaContabil(rs.getInt("cdcontacontabil"),
								rs.getString("NOME"));
					}

				});

		if (lista != null && lista.size() > 0) {
			return lista.get(0);
		} else
			return null;
	}

	@SuppressWarnings("unchecked")
	public ContaContabil findByIdentificador(String identificador) {
		String sql = "SELECT CG.cdcontacontabil, CG.NOME "
				+ "FROM VCONTACONTABIL VCG "
				+ "JOIN contaContabil CG ON CG.cdcontacontabil = VCG.cdcontacontabil "
				+ "WHERE CG.ATIVO = TRUE AND VCG.IDENTIFICADOR = '"
				+ identificador + "' ";

		SinedUtil.markAsReader();
		List<ContaContabil> lista = getJdbcTemplate().query(sql,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						return new ContaContabil(rs.getInt("cdcontacontabil"),
								rs.getString("NOME"));
					}
				});

		if (lista != null && lista.size() > 0) {
			return lista.get(0);
		} else
			return null;
	}

	public List<ContaContabil> findForFluxoCaixaMensal(
			List<NaturezaContagerencial> listaNaturezaContagerencial) {
		QueryBuilder<ContaContabil> query = query();

		query.select(
				"contaContabil.cdcontacontabil, contaContabil.nome, contaContabil.ativo,"
						+ "vcontacontabil.nivel, vcontacontabil.identificador, tipooperacao.cdtipooperacao, "
						+ "tipooperacao.nome")
				.join("contaContabil.vcontacontabil vcontacontabil")
				.join("contaContabil.tipooperacao tipooperacao");

		if (listaNaturezaContagerencial != null
				&& !listaNaturezaContagerencial.isEmpty()) {
			query.whereIn("contaContabil.natureza", NaturezaContagerencial
					.listAndConcatenate(listaNaturezaContagerencial));
		}

		return query.orderBy("vcontacontabil.identificador").list();
	}

	public List<ContaContabil> findForReport(ContacontabilFiltro filtro) {
		if (filtro == null) {
			throw new SinedException("A propriedade n�o pode ser nula");
		}

		return query()
				.select("contaContabil.cdcontacontabil, contaContabil.nome, tipooperacao.nome, contaContabil.ativo,"
						+ "vcontacontabil.nivel, vcontacontabil.identificador, contaContabil.observacao")

				.join("contaContabil.vcontacontabil vcontacontabil")
				.leftOuterJoin("contaContabil.tipooperacao tipooperacao")
				.where("contaContabil.ativo = ?", filtro.getMostraativo())
				.where("contaContabil.geramovimentacao = ?",
						filtro.getMostramovimentacao())
				.whereLikeIgnoreAll("contaContabil.nome", filtro.getNome())
				.where("vcontacontabil.identificador like ?||'%'",
						filtro.getIdentificador())
				.where("tipooperacao = ?", filtro.getTipooperacao())
				.where("vcontacontabil.nivel = ?", filtro.getNivel())
				.where("contaContabil.natureza = ?", filtro.getNatureza())
				.orderBy("vcontacontabil.identificador").list();
	}

	public List<ContaContabil> findAutocomplete(String q, Tipooperacao tipooperacao, String whereInNatureza, String whereInCdcontagerencial, String nomeCampoEmpresa, String whereInEmpresa, Boolean selectPai) {
		QueryBuilder<ContaContabil> query = query()
				.select("contaContabil.cdcontacontabil, contaContabil.nome, vcontacontabil.identificador, vcontacontabil.arvorepai, contaContabil.codigoalternativo, empresa.nome")
				.join("contaContabil.vcontacontabil vcontacontabil")
				.leftOuterJoin("contaContabil.empresa empresa")
				.openParentheses()
					.whereLikeIgnoreAll("contaContabil.nome", q)
					.or()
					.whereLikeIgnoreAll("vcontacontabil.arvorepai", q)
					.or()
					.where("vcontacontabil.identificador like ?||'%'", q)
					.or()
					.whereLikeIgnoreAll("contaContabil.codigoalternativo", q)
				.closeParentheses()
				.where("contaContabil.ativo = ?", Boolean.TRUE)
				.where("contaContabil.tipooperacao = ?", tipooperacao)
				.whereIn("contaContabil.natureza", whereInNatureza)
				.whereIn("contaContabil.cdcontacontabil", whereInCdcontagerencial)
				.orderBy("vcontacontabil.identificador");
		
		if(whereInEmpresa != null && parametrogeralService.getBoolean(Parametrogeral.PLANO_CONTAS_CONTABIL_POR_EMPRESA)){
			query.whereIn("empresa.cdpessoa", whereInEmpresa);
		}
		
		if(selectPai == null || !selectPai){
			query.where("not exists(select C2.cdcontacontabil from ContaContabil C2 where C2.contaContabilPai = contaContabil and C2.ativo = true)");
		}
		
		return query.list();
	}

	@SuppressWarnings("unchecked")
	public List<ContaContabil> findInTarefaOrcamento(Orcamento orcamento,
			Tarefaorcamento tarefaorcamento) {

		String query = "SELECT CG.cdcontacontabil, CG.NOME, VCG.IDENTIFICADOR "
				+ "FROM TAREFAORCAMENTO TOR "
				+ "JOIN contaContabil CG ON CG.cdcontacontabil = TOR.cdcontacontabil "
				+ "JOIN VCONTACONTABIL VCG ON VCG.cdcontacontabil = CG.cdcontacontabil "
				+ "WHERE TOR.CDORCAMENTO = "
				+ orcamento.getCdorcamento()
				+ " "
				+ (tarefaorcamento != null
						&& tarefaorcamento.getCdtarefaorcamento() != null ? "AND TOR.CDTAREFAORCAMENTO <> "
						+ tarefaorcamento.getCdtarefaorcamento()
						: "");

		SinedUtil.markAsReader();
		List<ContaContabil> lista = getJdbcTemplate().query(query,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						return new ContaContabil(rs.getInt("cdcontacontabil"),
								rs.getString("nome"), rs
										.getString("identificador"));
					}
				});

		return lista;
	}

	@SuppressWarnings("unchecked")
	public List<ContaContabil> findInComposicao(Orcamento orcamento,
			Composicaoorcamento composicaoorcamento) {
		String query = "SELECT CG.cdcontacontabil, CG.NOME, VCG.IDENTIFICADOR "
				+ "FROM COMPOSICAOORCAMENTO CO "
				+ "JOIN contaContabil CG ON CG.cdcontacontabil = CO.cdcontacontabil "
				+ "JOIN VCONTACONTABIL VCG ON VCG.cdcontacontabil = CG.cdcontacontabil "
				+ "WHERE CO.CDORCAMENTO = "
				+ orcamento.getCdorcamento()
				+ " "
				+ (composicaoorcamento != null
						&& composicaoorcamento.getCdcomposicaoorcamento() != null ? "AND CO.CDCOMPOSICAOORCAMENTO <> "
						+ composicaoorcamento.getCdcomposicaoorcamento()
						: "");

		SinedUtil.markAsReader();
		List<ContaContabil> lista = getJdbcTemplate().query(query,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						return new ContaContabil(rs.getInt("cdcontacontabil"),
								rs.getString("nome"), rs
										.getString("identificador"));
					}
				});

		return lista;
	}

	public ContaContabil findByProjetoOrcamentoMOD(Projeto projeto) {
		if (projeto == null || projeto.getCdprojeto() == null) {
			throw new SinedException("Projeto n�o pode ser nulo.");
		}

		List<ContaContabil> lista = query()
				.select("contaContabil.cdcontacontabil, contaContabil.nome, "
						+ "vcontacontabil.arvorepai, vcontacontabil.identificador")
				.join("contaContabil.vcontacontabil vcontacontabil")
				.where("contaContabil.cdcontacontabil in ("
						+ "select c.cdcontacontabil " + "from Orcamento o "
						+ "join o.contacontabilmod c "
						+ "join o.listaPlanejamento p " + "join p.projeto pr "
						+ "where pr.id = ? " + ")", projeto.getCdprojeto())
				.list();

		if (lista == null || lista.size() == 0) {
			return null;
		} else {
			return lista.get(0);
		}
	}

	public Iterator<Object[]> findForArquivoGerencial() {
		return newQueryBuilder(Object[].class)
				.select("contaContabil.cdcontacontabil, contaContabil.nome, vcontacontabil.identificador")
				.from(ContaContabil.class)
				.join("contaContabil.vcontacontabil vcontacontabil").iterate();
	}

	public List<ContaContabil> findAutocomplete(String q) {
		return query()
				.select("contaContabil.cdcontacontabil, contaContabil.nome, vcontacontabil.identificador, vcontacontabil.arvorepai, empresa.nome")
				.join("contaContabil.vcontacontabil vcontacontabil")
				.leftOuterJoin("contaContabil.empresa empresa")
				.openParentheses()
				.whereLikeIgnoreAll("contaContabil.nome", q)
				.or()
				.whereLikeIgnoreAll("vcontacontabil.arvorepai", q)
				.or()
				.where("vcontacontabil.identificador like ?||'%'", q)
				.closeParentheses()

				.where("not exists(select C2.cdcontacontabil from contaContabil C2 where C2.contaContabilPai = contaContabil and C2.ativo = true)")
				.where("contaContabil.ativo = ?", Boolean.TRUE)
				.orderBy("vcontacontabil.identificador").list();
	}

	public List<ContaContabil> loadForSpedPiscofinsReg0500(Set<String> listaIdentificador, String whereInEmpresas) {
		QueryBuilder<ContaContabil> query = query()
				.select("contaContabil.cdcontacontabil, contaContabil.nome, contaContabil.tipooperacao, contaContabil.dtaltera, "
						+ "contaContabil.natureza, contaContabil.observacao, vcontacontabil.identificador, vcontacontabil.nivel, vcontacontabil.nome")
				.join("contaContabil.vcontacontabil vcontacontabil")
				.leftOuterJoin("contaContabil.empresa empresa");

		query.openParentheses();
		for (String identificador : listaIdentificador) {
			query.where("vcontacontabil.identificador=?", identificador);
			query.or();
		}
		query.closeParentheses();
		query.openParentheses()
				.where("empresa is null")
				.or()
				.whereIn("empresa.cdpessoa", whereInEmpresas)
			.closeParentheses();

		return query.list();

	}

	public Boolean isPai(ContaContabil pai) {
		if (pai == null || pai.getCdcontacontabil() == null) {
			throw new SinedException("Conta gerencial n�o pode ser nula.");
		}

		return newQueryBuilder(Long.class)
				.select("count(*)").from(ContaContabil.class)
				.where("contaContabilPai is null")
				.where("contaContabil = ?", pai).unique() > 0;
	}

	public List<ContaContabil> findForGerarMovimentacao(
			Tipooperacao tipooperacao) {
		return query()
				.select("contaContabil.cdcontacontabil")
				.join("contaContabil.tipooperacao tipooperacao")
				.openParentheses()
				.where("(contaContabil.natureza=? and tipooperacao=?)",
						new Object[] { NaturezaContagerencial.CONTA_GERENCIAL,
								tipooperacao })
				.or()
				.where("(contaContabil.natureza=? and tipooperacao=?)",
						new Object[] { NaturezaContagerencial.OUTRAS,
								tipooperacao }).closeParentheses().list();
	}

	public boolean verificarItem(ContaContabil contacontabil) {

		QueryBuilder<Long> queryBuilder = newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.leftOuterJoin(
						"contaContabil.contaContabilPai contaContabilPai")
				.leftOuterJoin(
						"contaContabil.empresa empresa")
				.where("coalesce(contaContabil.ativo,false)=?", Boolean.TRUE)
				.where("contaContabil.item=?", contacontabil.getItem());

		if (contacontabil.getPai() == null) {
			queryBuilder.where("contaContabilPai is null");
		} else {
			queryBuilder.where("contaContabilPai=?", contacontabil.getPai());
		}

		if (contacontabil.getCdcontacontabil() != null) {
			queryBuilder.where("contaContabil.cdcontacontabil<>?",
					contacontabil.getCdcontacontabil());
		}
		
		if (contacontabil.getEmpresa() != null && parametrogeralService.getBoolean(Parametrogeral.PLANO_CONTAS_CONTABIL_POR_EMPRESA)){
			queryBuilder.where("empresa.cdpessoa=?",
					contacontabil.getEmpresa().getCdpessoa());
		}

		return queryBuilder.unique() == 0;
	}

	@SuppressWarnings("unchecked")
	public List<ContaContabil> findListaCompleta() {
		StringBuilder query = new StringBuilder();
		query.append(
				"SELECT vcontacontabil.identificador, vcontacontabil.cdcontacontabil, contaContabil.nome, contaContabil.cdcontacontabilPai ")
				.append("FROM vcontacontabil ")
				.append("left join contacontabil contaContabil on contaContabil.cdcontacontabil = vcontacontabil.cdcontacontabil ")
				.append("ORDER BY vcontacontabil.identificador");

		SinedUtil.markAsReader();
		List<ContaContabil> lista = getJdbcTemplate().query(query.toString(),
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						ContaContabil cg = new ContaContabil(rs
								.getInt("cdcontacontabil"), rs
								.getString("nome"), rs
								.getString("identificador"));
						Integer cdcontacontabilpai = rs
								.getInt("cdcontacontabilpai");
						if (cdcontacontabilpai != null
								&& cdcontacontabilpai > 0) {
							cg.setContaContabilPai(new ContaContabil(
									cdcontacontabilpai));
						}
						return cg;
					}
				});

		return lista;
	}

	public void updateUsadocalculocustooperacional(String whereInContagerencial) {
		if (whereInContagerencial == null || whereInContagerencial.equals("")) {
			getHibernateTemplate()
					.bulkUpdate(
							"update contaContabil set usadocalculocustooperacionalproducao = null ");
		} else {
			getHibernateTemplate().bulkUpdate(
					"update contaContabil set usadocalculocustooperacionalproducao = true "
							+ " where cdcontacontabil in ("
							+ whereInContagerencial + ")");
			getHibernateTemplate().bulkUpdate(
					"update contaContabil set usadocalculocustooperacionalproducao = null "
							+ " where cdcontacontabil not in ("
							+ whereInContagerencial + ")");
		}

	}

	public void updateUsadocalculocustocomercial(String whereInContagerencial) {
		if (whereInContagerencial == null || whereInContagerencial.equals("")) {
			getHibernateTemplate()
					.bulkUpdate(
							"update contaContabil set usadocalculocustocomercialproducao = null ");
		} else {
			getHibernateTemplate().bulkUpdate(
					"update contaContabil set usadocalculocustocomercialproducao = true "
							+ " where cdcontacontabil in ("
							+ whereInContagerencial + ")");
			getHibernateTemplate().bulkUpdate(
					"update contaContabil set usadocalculocustocomercialproducao = null "
							+ " where cdcontacontabil not in ("
							+ whereInContagerencial + ")");
		}

	}

	public List<ContaContabil> getContagerencialUsadaCustoOperacional() {
		return query()
				.select("contaContabil.cdcontacontabil, contaContabil.nome")
				.where("coalesce(contaContabil.usadocalculocustooperacionalproducao, false) = true")
				.list();
	}

	public List<ContaContabil> getContagerencialUsadaCustoComercial() {
		return query()
				.select("contaContabil.cdcontacontabil, contaContabil.nome")
				.where("coalesce(contaContabil.usadocalculocustocomercialproducao, false) = true")
				.list();
	}
	
	public ContaContabil loadForImportacaoSaldoInicial(Integer codigoAlternativo, Integer cdContaContabil, String identificador, Empresa empresa) {
		
		QueryBuilder<ContaContabil> query = query();
		query.select("contaContabil.cdcontacontabil,  contaContabil.nome, contaContabil.formato, contaContabil.codigoalternativo, " +
					"vcontacontabil.nivel, vcontacontabil.identificador, vcontacontabil.arvorepai, vcontacontabil.nome")
				.join("contaContabil.vcontacontabil vcontacontabil")
				.where("contaContabil.codigoalternativo = ?", codigoAlternativo)
				.where("contaContabil.cdcontacontabil = ?", cdContaContabil)
				.where("vcontacontabil.identificador = ?", identificador);
		
		if(empresa != null && parametrogeralService.getBoolean(Parametrogeral.PLANO_CONTAS_CONTABIL_POR_EMPRESA)){
			query.openParentheses()
				.where("contaContabil.empresa is null")
				.or()
				.where("contaContabil.empresa = ?", empresa)
				.closeParentheses();
		}
		return query.unique();
	}
	
	public ContaContabil loadWithEmpresa(ContaContabil contaContabil) {
		if (contaContabil == null) {
			throw new SinedException("Conta cont�bil n�o pode ser nula.");
		}
		
		return query()
				.select("contaContabil.cdcontacontabil,  contaContabil.nome, contaContabil.formato, contaContabil.codigoalternativo, " +
						"empresa.cdpessoa")
				.leftOuterJoin("contaContabil.empresa empresa")
				.where("contaContabil = ?", contaContabil)
				.unique();
	}
	
	public void saveSaldoInicial(ContaContabilSaldoInicial bean){
		getHibernateTemplate().bulkUpdate("update ContaContabil cc set cc.saldoInicial = ?, cc.dtSaldoInicial = ? where cc = ?", new Object[] {bean.getSaldo(), bean.getDtSaldo(), bean.getContaContabil()});
	}
	
	public Money calculaSaldoAtual(ContaContabil conta, Date date, Empresa... empresas) {	
		if(conta==null || conta.getCdcontacontabil()==null)
			throw new SinedException("N�o foi informado o c�digo da conta");
		
		String sql = "select calcular_saldo_conta_analitica(CDCONTACONTABIL, ?, null, null, null) " +
						"from contacontabil where cdcontacontabil = "+conta.getCdcontacontabil() + " " +
						(empresas.length > 0 ? "and cdempresa in (" + SinedUtil.listAndConcatenateIDs(Arrays.asList(empresas)) + ")" : "");
		
		return this.queryForMoney(sql, new Object[]{date});
	}
	
	private Money queryForMoney(String sql, Object[] fields){

		SinedUtil.markAsReader();
		Long result = getJdbcTemplate().queryForLong(sql, fields);
		
		return new Money(result, true);
	}
	
	public List<EmitirBalancoPatrimonialBean> buscarTudoBalancoPatrimonial() {
		
		QueryBuilder<EmitirBalancoPatrimonialBean> query = newQueryBuilder(EmitirBalancoPatrimonialBean.class);
		
		List<NaturezaContagerencial> listaNaturezaContaGerencial = new ArrayList<NaturezaContagerencial>();
		listaNaturezaContaGerencial.add(NaturezaContagerencial.CONTAS_ATIVO);
		listaNaturezaContaGerencial.add(NaturezaContagerencial.CONTAS_PASSIVO);
		listaNaturezaContaGerencial.add(NaturezaContagerencial.CONTAS_COMPENSACAO);
		listaNaturezaContaGerencial.add(NaturezaContagerencial.PATRIMONIO_LIQUIDO);
		
		query
			.select("new br.com.linkcom.sined.modulo.contabil.controller.report.bean.EmitirBalancoPatrimonialBean(" +
						"contaContabil.cdcontacontabil, contaContabil.nome, contaContabil.contaretificadora, vcontacontabil.identificador)")
			.from(ContaContabil.class)
			.setUseTranslator(false)
			.join("contaContabil.vcontacontabil vcontacontabil")
			.where("contaContabil.ativo = true")
			.whereIn("contaContabil.natureza", NaturezaContagerencial.listAndConcatenate(listaNaturezaContaGerencial));
			query.orderBy("vcontacontabil.identificador");
		
		return query.list();
	}
	
	public ContaContabil loadWithClassificacaoAndNatureza(ContaContabil bean) {
		return query()
				.select("contaContabil.cdcontacontabil, contaContabil.nome, vcontacontabil.identificador")
				.join("contaContabil.vcontacontabil vcontacontabil")
				.where("contaContabil = ?", bean)
				.setMaxResults(1)
				.unique();
	}

	public void alteraFilhosRetificadora(String identificador, Integer cdempresa) {
		if(org.apache.commons.lang.StringUtils.isNotBlank(identificador)){
			String sql = "update contacontabil " +
					"set contaretificadora = true " +
					"where cdcontacontabil in (" +
					"select cc.cdcontacontabil " +
					"from contacontabil cc " +
					"join vcontacontabil vcc on vcc.cdcontacontabil = cc.cdcontacontabil " +
					"where vcc.identificador like '" + identificador + "%' ";
			if(cdempresa != null){
				sql = sql +  "and cc.cdempresa = " + cdempresa + ")";
			}else{
				sql = sql +  "and cc.cdempresa is null)";
			}
			
			getJdbcTemplate().update(sql);			
		}
	}
}
