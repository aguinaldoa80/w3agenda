package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Orcamentosituacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OrcamentosituacaoDAO extends GenericDAO<Orcamentosituacao>{

	/**
	 * M�todo para o combo no flex
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Orcamentosituacao> findForComboFlex() {
		return query()
				.select("orcamentosituacao.cdorcamentosituacao, orcamentosituacao.nome")				
				.orderBy("orcamentosituacao.cdorcamentosituacao")
				.list();
	}
}
