package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaohistorico;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContagerencial;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoAgendamento;
import br.com.linkcom.sined.geral.service.AgendamentoHistoricoService;
import br.com.linkcom.sined.geral.service.AgendamentoService;
import br.com.linkcom.sined.geral.service.ContapagarService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.DocumentoorigemService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaohistoricoService;
import br.com.linkcom.sined.geral.service.MovimentacaoorigemService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.AgendamentoFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.FluxocaixaFiltroReport;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

public class AgendamentoDAO extends GenericDAO<Agendamento> {
	private AgendamentoService agendamentoService;
	private AgendamentoHistoricoService agendamentoHistoricoService;
	private RateioService rateioService;
	private MovimentacaoService movimentacaoService;
	private MovimentacaoorigemService movimentacaoorigemService;
	private MovimentacaohistoricoService movimentacaohistoricoService;
	private ContapagarService contapagarService;
	private ContareceberService contareceberService;
	private DocumentoorigemService documentoorigemService;
	
	public void setAgendamentoService(AgendamentoService agendamentoService) {this.agendamentoService = agendamentoService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setContapagarService(ContapagarService contapagarService) {this.contapagarService = contapagarService;}
	public void setContareceberService(ContareceberService contareceberService) {this.contareceberService = contareceberService;}
	public void setMovimentacaoorigemService(MovimentacaoorigemService movimentacaoorigemService) {this.movimentacaoorigemService = movimentacaoorigemService;}
	public void setAgendamentoHistoricoService(AgendamentoHistoricoService agendamentoHistoricoService) {this.agendamentoHistoricoService = agendamentoHistoricoService;}
	public void setMovimentacaohistoricoService(MovimentacaohistoricoService movimentacaohistoricoService) {this.movimentacaohistoricoService = movimentacaohistoricoService;}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {this.documentoorigemService = documentoorigemService;}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Agendamento> query, FiltroListagem _filtro) {
		AgendamentoFiltro filtro = (AgendamentoFiltro) _filtro;
		
		query
			.select("distinct new br.com.linkcom.sined.geral.bean.Agendamento(agendamento.cdagendamento, agendamento.descricao, agendamento.dtproximo, agendamento.ativo, agendamento.valor," +
					"pessoa.cdpessoa, pessoa.nome, agendamento.outrospagamento, agendamento.tipopagamento, aux_agendamento.dtUltimaConsolidacao, " +
					"aux_agendamento.situacao, tipooperacao.cdtipooperacao, tipooperacao.nome, tipovinculo.cdtipovinculo, tipovinculo.nome, conta.nome, coalesce(agendamento.outrospagamento, pessoa.nome))")
			.setUseTranslator(false)		
			.join("agendamento.aux_agendamento aux_agendamento")
			.leftOuterJoin("agendamento.documentotipo tipodocumento")
			.join("agendamento.frequencia frequencia")
			.join("agendamento.tipooperacao tipooperacao")
			.join("agendamento.tipovinculo tipovinculo")
			.leftOuterJoin("agendamento.conta conta")
			.leftOuterJoin("agendamento.pessoa pessoa")
			.leftOuterJoin("agendamento.rateio rateio")
			.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
			.leftOuterJoin("listaRateioitem.projeto projeto")
			.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
			.where("agendamento.cdagendamento = ?", filtro.getIdagendamento())
			.where("agendamento.tipopagamento = ?", filtro.getTipopagamento())
			.where("tipooperacao = ?", filtro.getTipooperacao())
			.whereLikeIgnoreAll("agendamento.descricao", filtro.getDescricao())
			.where("frequencia = ?", filtro.getFrequencia())
			.where("agendamento.dtproximo >= ?", filtro.getDtVencimentoInicio())
			.where("agendamento.dtproximo <= ?", filtro.getDtVencimentoFim())
			.where("agendamento.valor >= ?", filtro.getValorInicio())
			.where("agendamento.valor <= ?", filtro.getValorFim())
			.where("agendamento.empresa = ?", filtro.getEmpresa())
			.where("agendamento.ativo = ?", filtro.getAtivo())
			.where("projeto = ?", filtro.getProjeto())
			.where("centrocusto = ?", filtro.getCentrocusto())
			.where("pessoa = ?",filtro.getCliente())
			.where("pessoa = ?",filtro.getFornecedor())
			.where("pessoa = ?",filtro.getColaborador())
			.where("agendamento.vinculoProvisionado = ?", filtro.getVinculoProvisionado())
			.ignoreJoin("rateio", "listaRateioitem", "projeto", "centrocusto");
		
		if(filtro.getProjetoAberto() != null && filtro.getProjeto() == null){
			if(filtro.getProjetoAberto()){
				query
					.openParentheses()
					.where("projeto.dtfimprojeto is null")
					.or()
					.where("projeto.dtfimprojeto > ?", SinedDateUtils.currentDate())
					.closeParentheses();
			} else {
				query
					.where("projeto.dtfimprojeto is not null")
					.where("projeto.dtfimprojeto <= ?", SinedDateUtils.currentDate());
			}
		}
		
		if(StringUtils.isNotBlank(filtro.getPessoa())){
			query.openParentheses();
				query.whereLikeIgnoreAll("pessoa.nome", filtro.getPessoa());
				query.or();
				query.whereLikeIgnoreAll("agendamento.outrospagamento", filtro.getPessoa());
			query.closeParentheses();
		}
		
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		if (!administrador){
			query
				.leftOuterJoinIfNotExists("agendamento.documentotipo documentotipo")
				.leftOuterJoinIfNotExists("documentotipo.listaDocumentotipopapel listaDocumentotipopapel")
				.leftOuterJoinIfNotExists("agendamento.vinculoProvisionado vinculoP")
				.leftOuterJoinIfNotExists("vinculoP.listaContapapel listaContapapelVinculoP");
			
			if (whereInPapel != null && !whereInPapel.equals("")){
				query.openParentheses();
				query.whereIn("listaDocumentotipopapel.papel.cdpapel", whereInPapel)
				.or()
				.where("listaDocumentotipopapel.papel.cdpapel is null");
				query.closeParentheses();
				
				query.openParentheses();
				query.whereIn("listaContapapelVinculoP.papel.cdpapel", whereInPapel)
				.or()
				.where("listaContapapelVinculoP.papel.cdpapel is null");
				query.closeParentheses();
			} else {
				query.where("listaDocumentotipopapel.papel.cdpapel is null");
				query.where("listaContapapelVinculoP.papel.cdpapel is null");
			}
		}
		
		
		List<SituacaoAgendamento> listaSituacao = filtro.getListaSituacao();
		if (listaSituacao != null && !listaSituacao.isEmpty()) {
			String list = "";
			for (int i = 0; i < listaSituacao.size(); i++) {
				Object o = listaSituacao.get(i);
				SituacaoAgendamento situacaoAgendamento = null;
				if (o instanceof String) {
					situacaoAgendamento = SituacaoAgendamento.valueOf((String)o);
				}else{
					situacaoAgendamento = (SituacaoAgendamento)o;
				}
				
				list += situacaoAgendamento.getValue() + ",";
			}
			query.whereIn("aux_agendamento.situacao", list.substring(0, (list.length()-1) ));
		}
		query.orderBy("agendamento.dtproximo ASC");
	}
	
	/**
	 * Obt�m uma lista de agendamentos de acordo com o filtro da listagem, sem restri��o de p�gina.
	 * A lista � usada para somar os valores dos agendamentos e exibir na listagem.
	 * 
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Agendamento> findForSomaListagem(AgendamentoFiltro filtro){
		QueryBuilder<Agendamento> query = query();
		this.updateListagemQuery(query, filtro);
		SinedUtil.markAsReader();
        return query.list();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Agendamento> query) {
		((QueryBuilderSined<Agendamento>) query).setUseWhereClienteEmpresa(true);
		((QueryBuilderSined<Agendamento>) query).setUseWhereFornecedorEmpresa(true);
		
		query
			.leftOuterJoinFetch("agendamento.tipooperacao tipooperacao")
//			.leftOuterJoinFetch("agendamento.pessoa pessoa")
//			.leftOuterJoinFetch("agendamento.empresa empresa")
			.leftOuterJoinFetch("agendamento.documentotipo documentotipo")
			.leftOuterJoinFetch("agendamento.frequencia frequencia")
			.leftOuterJoinFetch("agendamento.tipovinculo tipovinculo")
			.leftOuterJoinFetch("agendamento.conta conta")
			.leftOuterJoinFetch("agendamento.vinculoProvisionado vinculoProvisionado")
			.leftOuterJoinFetch("agendamento.rateio rateio")
			.leftOuterJoinFetch("agendamento.operacaocontabil operacaocontabil")
			.leftOuterJoinFetch("agendamento.formapagamento formapagamento")
			.leftOuterJoinFetch("agendamento.aux_agendamento aux_agendamento")
			;
	}
	
	@Override
	public Agendamento load(Agendamento bean) {
		if(bean == null){
			return null;
		}
		return query()
			.entity(bean)
			.joinFetch("agendamento.aux_agendamento aux_agendamento")
			.unique();
	}
	
	/**
	 * Faz com que o saveOrUpdate de rateio utilize a
	 * transa��o deste saveOrUpdate.
	 * 
	 * @see br.com.linkcom.sined.geral.service.RateioService#saveOrUpdateNoUseTransaction(br.com.linkcom.sined.geral.bean.Rateio)
	 * @author Hugo Ferreira
	 */
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaAgendamentoHistorico");
		Agendamento bean = (Agendamento) save.getEntity();
		if (bean.getRateio() != null) {
			rateioService.saveOrUpdateNoUseTransaction(bean.getRateio());
		}
	}

	/**
	 * <p>Carrega um bean de Agendamento com todas as propriedades necess�rias
	 * para uma consolida��o.</p>
	 * 
	 * @param cdAgendamento
	 * @return
	 * @author Hugo Ferreira
	 */
	public Agendamento carregarParaConsolidar(Integer cdAgendamento) {
		if(cdAgendamento == null){
			throw new SinedException("O par�metro cdAgendamento n�o pode ser null.");
		}
		
		StringBuilder stSelect = new StringBuilder();
		stSelect
			.append("agendamento.cdagendamento,agendamento.ativo,agendamento.cdusuarioaltera, ")
			.append("agendamento.descricao,agendamento.documento,agendamento.dtaltera, ")
			.append("agendamento.dtfim,agendamento.dtinicio,agendamento.dtproximo, ")
			.append("agendamento.iteracoes,agendamento.observacao,agendamento.valor, ")
			.append("agendamento.tipopagamento,agendamento.outrospagamento, ")
			.append("aux_agendamento.dtUltimaConsolidacao,aux_agendamento.situacao, ")
			.append("pessoa.cdpessoa, ")
			.append("empresa.cdpessoa, ")
			.append("tipoVinculo.cdtipovinculo, ")
			.append("conta.cdconta, conta.nome, contatipo.cdcontatipo, contatipo.nome, ")
			.append("documentoTipo.cddocumentotipo, documentoTipo.boleto, ")
			.append("rateio.cdrateio, ")
//			.append("rateioItem.cdrateioitem,rateioItem.percentual,rateioItem.valor,")
//			.append("centrocusto.cdcentrocusto,")
//			.append("projeto.cdprojeto,")
//			.append("contagerencial.cdcontagerencial,")
			.append("taxa.cdtaxa, ")
			.append("tipooperacao.cdtipooperacao, ")
			.append("documentoacao.cddocumentoacao,")
			.append("operacaocontabil.cdoperacaocontabil,")
			.append("operacaocontabil.nome,")
			.append("formapagamento.cdformapagamento, formapagamento.nome, ")
			.append("vinculoProvisionado.cdconta, vinculoProvisionado.nome, ")
			.append("listaEndereco.cdendereco, listaEndereco.numero, listaEndereco.logradouro, listaEndereco.bairro, ")
			.append("listaEndereco.cep, listaEndereco.complemento, listaEndereco.caixapostal, ")
			.append("enderecotipo.cdenderecotipo, municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla, uf.nome ")
			
			
		;
		
		QueryBuilderSined<Agendamento> query = querySined();
		return query
			
			.select(stSelect.toString())
			.join("agendamento.aux_agendamento aux_agendamento")
			.join("agendamento.tipovinculo tipoVinculo")
			.leftOuterJoin("agendamento.pessoa pessoa")
			.leftOuterJoin("agendamento.empresa empresa")
			.leftOuterJoin("agendamento.conta conta")
			.leftOuterJoin("agendamento.vinculoProvisionado vinculoProvisionado")
			.leftOuterJoin("conta.contatipo contatipo")
			.leftOuterJoin("agendamento.documentotipo documentoTipo")
			.leftOuterJoin("agendamento.rateio rateio")
			.leftOuterJoin("agendamento.documentoacao documentoacao")
//			.leftOuterJoin("rateio.listaRateioitem rateioItem")
//			.leftOuterJoin("rateioItem.centrocusto centrocusto")
//			.leftOuterJoin("rateioItem.projeto projeto")
//			.leftOuterJoin("rateioItem.contagerencial contagerencial")
			.leftOuterJoin("agendamento.taxa taxa")
			.leftOuterJoin("agendamento.tipooperacao tipooperacao")
			.leftOuterJoin("agendamento.operacaocontabil operacaocontabil")
			.leftOuterJoin("agendamento.formapagamento formapagamento")
			.leftOuterJoin("pessoa.listaEndereco listaEndereco")
			.leftOuterJoin("listaEndereco.enderecotipo enderecotipo")
			.leftOuterJoin("listaEndereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.where("agendamento.id = ?", cdAgendamento)
			.unique();
	}
	
	/**
	 * <p>Carrega um agendamento para salvar ap�s uma consolida��o.</p>
	 * 
	 * @param cdAgendamento
	 * @return
	 * @author Hugo Ferreira
	 */
	public Agendamento carregarParaSalvarConsolidar(Integer cdAgendamento) {
		if(cdAgendamento == null){
			throw new SinedException("O par�metro cdAgendamento n�o pode ser null.");
		}
		
		StringBuilder stSelect = new StringBuilder();
		stSelect
		.append("agendamento.cdagendamento,agendamento.ativo,agendamento.cdusuarioaltera,")
		.append("agendamento.descricao,agendamento.documento,agendamento.dtaltera,")
		.append("agendamento.dtfim,agendamento.dtinicio,agendamento.dtproximo,")
		.append("agendamento.iteracoes,agendamento.observacao,agendamento.valor,")
		.append("agendamento.tipopagamento,agendamento.outrospagamento,agendamento.frequencia,")
		.append("pessoa.cdpessoa,")
		.append("empresa.cdpessoa,")
		.append("tipoVinculo.cdtipovinculo,")
		.append("conta.cdconta,")
		.append("documentoTipo.cddocumentotipo,")
		.append("rateio.cdrateio,")
		.append("taxa.cdtaxa,")
		.append("tipooperacao.cdtipooperacao,")
		.append("agendamentoHistorico.cdAgendamentoHistorico,")
		.append("agendamentoHistorico.observacao,")
		.append("agendamentoHistorico.cdusuarioaltera,")
		.append("agendamentoHistorico.dtaltera,")
		.append("agendamentoAcao.cdAgendamentoAcao,")
		.append("documentoacao.cddocumentoacao, ")
		.append("formapagamento.cdformapagamento, formapagamento.nome, ")
		.append("operacaocontabil.cdoperacaocontabil , operacaocontabil.nome, ")
		.append("vinculoProvisionado.cdconta, vinculoProvisionado.nome")
		;
		
		QueryBuilder<Agendamento> query = query();
		return query
			.select(stSelect.toString())
			.join("agendamento.tipovinculo tipoVinculo")
			.leftOuterJoin("agendamento.operacaocontabil operacaocontabil")
			.leftOuterJoin("agendamento.pessoa pessoa")
			.leftOuterJoin("agendamento.empresa empresa")
			.leftOuterJoin("agendamento.conta conta")
			.leftOuterJoin("agendamento.documentotipo documentoTipo")
			.leftOuterJoin("agendamento.rateio rateio")
			.leftOuterJoin("agendamento.taxa taxa")
			.leftOuterJoin("agendamento.tipooperacao tipooperacao")
			.leftOuterJoin("agendamento.listaAgendamentoHistorico agendamentoHistorico")
			.leftOuterJoin("agendamentoHistorico.agendamentoAcao agendamentoAcao")
			.leftOuterJoin("agendamento.documentoacao documentoacao")
			.leftOuterJoin("agendamento.formapagamento formapagamento")
			.leftOuterJoin("agendamento.vinculoProvisionado vinculoProvisionado")
			.where("agendamento.id = ?", cdAgendamento)
			.unique();
	}

	/**
	 * Atualiza um agendamento ap�s ser consolildado.
	 * 
	 * @param agendamento
	 * @param vinculo
	 * @author Hugo Ferreira
	 */
	public void saveAgendamentoConsolidacao(final Agendamento agendamento, final Object vinculo) {
		Movimentacao movimentacao = (Movimentacao) getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				Object ret = null;
				if (vinculo instanceof Movimentacao) {
					Movimentacao mv = (Movimentacao) vinculo;
					Movimentacaoorigem movimentacaoorigem = new Movimentacaoorigem(null, mv, null, null, agendamento, null, null);
					Movimentacaohistorico mvHist = new Movimentacaohistorico(null, mv, Movimentacaoacao.CRIADA, null, null, null);
					
					movimentacaoService.verificaConciliaAutomaticoCaixa(mv);
					
					movimentacaoService.saveOrUpdateNoUseTransaction(mv); //Deve ser executado primeiro
					ret = mv;
					movimentacaoorigemService.saveOrUpdateNoUseTransaction(movimentacaoorigem);
					movimentacaohistoricoService.saveOrUpdateNoUseTransaction(mvHist);
				} else if (vinculo instanceof Documento) {
					Documento doc = (Documento) vinculo;
					
					Documentoorigem docorigem = new Documentoorigem(doc,null,null,null,null,null,null,null,null,agendamento);
					
					if (agendamento.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)) {
						contapagarService.saveOrUpdateNoUseTransaction(doc);
						documentoorigemService.saveOrUpdateNoUseTransaction(docorigem);
					} else {
						contareceberService.saveOrUpdateNoUseTransaction(doc);
						documentoorigemService.saveOrUpdateNoUseTransaction(docorigem);
					}
				}
				agendamentoHistoricoService.gerenciaHistorico(agendamento, vinculo); //Est� aqui porque precisa do cdVinculo
				agendamentoService.saveOrUpdateNoUseTransaction(agendamento);
				
				return ret;
			}			
		});
		if(movimentacao != null){
			movimentacaoService.callProcedureAtualizaMovimentacao(movimentacao);
		}
	}
	
	/**
	 * M�todo para obter lista de agendamentos para gera��o do relat�rio de fluxo de caixa.
	 * 
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Agendamento> findForReportFluxocaixa(FluxocaixaFiltroReport filtro){
		List<ItemDetalhe> listaProjeto = filtro.getListaProjeto();
		List<ItemDetalhe> listaCentrocusto = filtro.getListaCentrocusto();
		List<ItemDetalhe> listaProjetotipo = filtro.getListaProjetotipo();
		List<ItemDetalhe> listaEmpresa = filtro.getListaEmpresa();
		List<ItemDetalhe> listaFornecedor = filtro.getListaFornecedor();
		List<ItemDetalhe> listaCliente = filtro.getListaCliente();
		List<ItemDetalhe> listaConta = filtro.getListaContabancaria();
		
		boolean agendCredito = Util.booleans.isTrue(filtro.getAgendCredito());
		boolean agendDebito = Util.booleans.isTrue(filtro.getAgendDebito());
		
		if(!agendCredito && !agendDebito){
			return new ArrayList<Agendamento>();
		}
		
		QueryBuilderSined<Agendamento> q = querySined();
			q
			.select("agendamento.cdagendamento, agendamento.documento,agendamento.descricao,agendamento.valor,agendamento.dtfim," +
					"agendamento.dtproximo,agendamento.iteracoes,agendamento.tipopagamento, agendamento.outrospagamento," +
					"tipooperacao.cdtipooperacao,tipooperacao.nome," +
					"pessoa.cdpessoa,pessoa.nome, projeto.cdprojeto, projeto.nome, " +
					"frequencia.cdfrequencia,contatipo.cdcontatipo, rateioitem.valor, " +
					"contagerencial.cdcontagerencial, contagerencial.nome, conta.cdconta, conta.nome, " +
					"vinculoProvisionado.cdconta, vinculoProvisionado.nome, vinculoProvisionadoContatipo.cdcontatipo, centrocusto.cdcentrocusto")
			
			.join("agendamento.tipooperacao tipooperacao")
			.leftOuterJoin("agendamento.empresa empresa")
			.leftOuterJoin("agendamento.pessoa pessoa")
			.leftOuterJoin("agendamento.vinculoProvisionado vinculoProvisionado")
			.leftOuterJoin("vinculoProvisionado.contatipo vinculoProvisionadoContatipo")
			.leftOuterJoin("agendamento.conta conta")
			.leftOuterJoin("conta.contatipo contatipo")
			.join("agendamento.frequencia frequencia")
			
			.leftOuterJoin("agendamento.rateio rateio")
			.leftOuterJoin("rateio.listaRateioitem rateioitem")
			.leftOuterJoin("rateioitem.contagerencial contagerencial")
			.leftOuterJoin("rateioitem.projeto projeto")
			.leftOuterJoin("projeto.projetotipo projetotipo")
			.leftOuterJoin("rateioitem.centrocusto centrocusto")
			;
		
		/**
		 * Geral
		 */
		if(filtro.getMostrarContasInativadas() == null || !filtro.getMostrarContasInativadas()){
			q.openParentheses();
				q.openParentheses();
					q.where("vinculoProvisionado is null");
					q.where("conta is null");
				q.closeParentheses();
				
				q.or();
				
				q.openParentheses();
					q.where("vinculoProvisionado is not null");
					q.openParentheses();
						q.openParentheses();
							q.where("vinculoProvisionadoContatipo = ?", Contatipo.TIPO_CONTA_BANCARIA);
							q.where("vinculoProvisionado.ativo = true");
						q.closeParentheses();
						q.or();
						q.where("vinculoProvisionadoContatipo is null");
						q.or();
						q.where("vinculoProvisionadoContatipo <> ?", Contatipo.TIPO_CONTA_BANCARIA);
					q.closeParentheses();
				q.closeParentheses();
				
				q.or();
				
				q.openParentheses();
					q.where("conta is not null");
					q.openParentheses();
						q.openParentheses();
							q.where("contatipo = ?", Contatipo.TIPO_CONTA_BANCARIA);
							q.where("conta.ativo = true");
						q.closeParentheses();
						q.or();
						q.where("contatipo is null");
						q.or();
						q.where("contatipo <> ?", Contatipo.TIPO_CONTA_BANCARIA);
					q.closeParentheses();
				q.closeParentheses();
				
			q.closeParentheses();
		}
		
		q.openParentheses();
			q.where("agendamento.ativo = true");
			if(!filtro.getValoresAnteriores()){
				q.openParentheses();
					q.where("agendamento.dtfim is not null and agendamento.dtfim >= ?",filtro.getPeriodoDe());
					q.or();
					q.where("agendamento.dtfim is null");
				q.closeParentheses();
			}
			q.where("agendamento.dtproximo <= ?",filtro.getPeriodoAte());
			
			// Os agendamentos consolidados ou finalizados n�o entram no fluxo de caixa
			q.where("situacaoagendamento(current_date, agendamento.dtproximo, agendamento.dtfim) <> ?", SituacaoAgendamento.FINALIZADO.getValue());
			
			if(!agendCredito){
				q.where("tipooperacao <> ?", Tipooperacao.TIPO_CREDITO);
			}
			if(!agendDebito){
				q.where("tipooperacao <> ?", Tipooperacao.TIPO_DEBITO);
			}
		q.closeParentheses();
		
		/**
		 * Outros
		 */
		q.openParentheses();
			// WHEREIN LISTA DE CENTRO DE CUSTO
			if(SinedUtil.isListNotEmpty(listaCentrocusto)){
				q.whereIn("centrocusto.cdcentrocusto",CollectionsUtil.listAndConcatenate(listaCentrocusto, "centrocusto.cdcentrocusto", ","));
			}
			
			// WHEREIN LISTA DE TIPO DE PROJETO
			if (listaProjetotipo != null && listaProjetotipo.size() > 0) {
				q.whereIn("projetotipo.cdprojetotipo", CollectionsUtil.listAndConcatenate(listaProjetotipo, "projetotipo.cdprojetotipo", ","));
			}
			
			// WHERE DE PROEJTO
			String idProjeto = String.valueOf(filtro.getRadProjeto().getId());
			if(idProjeto.equals("escolherProjeto") && SinedUtil.isListNotEmpty(listaProjeto)){
				q.whereIn("projeto.cdprojeto", CollectionsUtil.listAndConcatenate(listaProjeto, "projeto.cdprojeto", ","));
			}else if(idProjeto.equals("comProjeto")){
				q.where("projeto is not null");
			}else if(filtro.getRadProjeto().getId().equals("semProjeto")){
				q.where("projeto is null");
			}
			
			// WHEREIN LISTA DE EMPRESAS
			if(listaEmpresa != null && !listaEmpresa.isEmpty()){
				q.whereIn("empresa.cdpessoa",CollectionsUtil.listAndConcatenate(listaEmpresa, "empresa.cdpessoa", ","));
			}
			
			// WHEREIN NA LISTA DE FORNECEDOR
			if(listaFornecedor != null && listaFornecedor.size() > 0 ){
				q.whereIn("pessoa.cdpessoa",CollectionsUtil.listAndConcatenate(listaFornecedor, "fornecedor.cdpessoa", ","));
			}
			
			// WHEREIN NA LISTA DE CLIENTE			
			if(listaCliente != null && listaCliente.size() > 0){
				q.whereIn("pessoa.cdpessoa",CollectionsUtil.listAndConcatenate(listaCliente, "cliente.cdpessoa", ","));
			}
			
			// WHEREIN NA LISTA DE CONTAS BANC�RIAS 			
			if(listaConta != null && listaConta.size() > 0){
				q.openParentheses();
				q.whereIn("vinculoProvisionado.cdconta",CollectionsUtil.listAndConcatenate(listaConta, "conta.cdconta", ","));
				q.or();
				q.whereIn("conta.cdconta",CollectionsUtil.listAndConcatenate(listaConta, "conta.cdconta", ","));
				q.closeParentheses();
			}
				
		q.closeParentheses();
		
		/**
		/**
		 * V�nculo
		 */
		q.openParentheses();
			
			// Considerar v�nculo em conta a pagar/receber
			boolean cons = Util.booleans.isTrue(filtro.getConsDocumentos());
			if(cons){
				q.where("conta.cdconta is null");
				q.or();
			}
		
			//WHEREIN LISTA DE CAIXA
			List<ItemDetalhe> listaCaixa = filtro.getListaCaixa();
			if(filtro.getRadCaixa() != null){
				if(Util.booleans.isFalse(filtro.getRadCaixa()) && listaCaixa != null && listaCaixa.size() > 0){
					// Op��o 'Escolher'
					q.openParentheses();
						q.whereIn("conta.cdconta", CollectionsUtil.listAndConcatenate(listaCaixa, "conta.cdconta", ","));
						q.or();
						q.whereIn("vinculoProvisionado.cdconta", CollectionsUtil.listAndConcatenate(listaCaixa, "conta.cdconta", ","));
					q.closeParentheses();
					
					q.or();
				}else{
					// Op��o 'Todas'
					q.openParentheses();
					q.where("contatipo.cdcontatipo = " + Contatipo.CAIXA);
					q.or();
					q.where("vinculoProvisionadoContatipo.cdcontatipo = " + Contatipo.CAIXA);
					q.closeParentheses();
					q.or();
				}
			}else{
				// Op��o 'Desconsiderar'
				q.openParentheses();
					q.where("contatipo is null");
					q.or();
					q.where("contatipo.cdcontatipo <> " + Contatipo.CAIXA);
				q.closeParentheses();
				q.openParentheses();
					q.where("vinculoProvisionadoContatipo is null");
					q.or();
					q.where("vinculoProvisionadoContatipo.cdcontatipo <> " + Contatipo.CAIXA);
				q.closeParentheses();
			}
			
			
			
			// WHEREIN LISTA DE CART�O DE CR�DITO
			List<ItemDetalhe> listaCartao = filtro.getListaCartao();
			if(filtro.getRadCartao() != null){
				if(Util.booleans.isFalse(filtro.getRadCartao()) && listaCartao != null && listaCartao.size() > 0){
					// Op��o 'Escolher'
					q.openParentheses();
						q.whereIn("conta.cdconta", CollectionsUtil.listAndConcatenate(listaCartao, "conta.cdconta", ","));
						q.or();
						q.whereIn("vinculoProvisionado.cdconta", CollectionsUtil.listAndConcatenate(listaCartao, "conta.cdconta", ","));
					q.closeParentheses();
					q.or();
				}else{
					// Op��o 'Todas'
					q.openParentheses();
						q.where("contatipo.cdcontatipo = " + Contatipo.CARTAO_DE_CREDITO);
						q.or();
						q.where("vinculoProvisionadoContatipo.cdcontatipo = " + Contatipo.CARTAO_DE_CREDITO);
					q.closeParentheses();
					q.or();
				}
			}else{
				// Op��o 'Desconsiderar'
				q.openParentheses();
					q.where("contatipo is null");
					q.or();
					q.where("contatipo.cdcontatipo <> " + Contatipo.CARTAO_DE_CREDITO);
				q.closeParentheses();
				q.openParentheses();
					q.where("vinculoProvisionadoContatipo is null");
					q.or();
					q.where("vinculoProvisionadoContatipo.cdcontatipo <> " + Contatipo.CARTAO_DE_CREDITO);
				q.closeParentheses();
			}
			
			
			// WHEREIN LISTA DE CONTA BANC�RIA
			if(filtro.getRadContbanc() != null){
				if(Util.booleans.isFalse(filtro.getRadContbanc()) && listaConta != null && listaConta.size() > 0){
					// Op��o 'Escolher'
					q.openParentheses();
					q.whereIn("conta.cdconta", CollectionsUtil.listAndConcatenate(listaConta, "conta.cdconta", ","));
					q.or();
					q.whereIn("vinculoProvisionado.cdconta", CollectionsUtil.listAndConcatenate(listaConta, "conta.cdconta", ","));
					q.closeParentheses();
				}else{
					// Op��o 'Todas'
					q.openParentheses();
					q.where("contatipo.cdcontatipo = " + Contatipo.CONTA_BANCARIA);
					q.or();
					q.where("vinculoProvisionadoContatipo.cdcontatipo = " + Contatipo.CONTA_BANCARIA);
					q.closeParentheses();
				}
			}else{
				// Op��o 'Desconsiderar'
				q.openParentheses();
					q.where("contatipo is null");
					q.or();
					q.where("contatipo.cdcontatipo <> " + Contatipo.CONTA_BANCARIA);
				q.closeParentheses();
				q.openParentheses();
					q.where("vinculoProvisionadoContatipo is null");
					q.or();
					q.where("vinculoProvisionadoContatipo.cdcontatipo <> " + Contatipo.CONTA_BANCARIA);
				q.closeParentheses();
			}
			
		q.closeParentheses();
		
//		WHEREIN PARA A LISTA DE NATUREZA DA CONTA GERENCIAL
		q.whereIn("contagerencial.natureza", NaturezaContagerencial.listAndConcatenate(filtro.getListanaturezaContaGerencial()));
		
		q.orderBy("agendamento.dtproximo");
		return q.list();
	}
	
	/**
	 * Chama a procedure que atualiza a tabela auxiliar do Agendamento.
	 *
	 * @author Rodrigo Freitas
	 */
	public void updateAux() {
		getJdbcTemplate().execute("SELECT ATUALIZA_AGENDAMENTO()");
	}
	
	/**
	 * M�todo que busca todos os contratos para busca geral
	 * 
	 * @param busca
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Agendamento> findAgendamentosForBuscaGeral(String busca) {
		return query()
			.select("agendamento.cdagendamento, agendamento.descricao")
			.openParentheses()
				.whereLikeIgnoreAll("agendamento.descricao", busca)
			.closeParentheses()
			.orderBy("agendamento.descricao")
			.list();
	}	

	
	/**
	 * M�todo que retorna o numero de iteracoes do agendamento
	 *
	 * @param agendamento
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public Integer numeroIteracoesByAgendamento(Agendamento agendamento) {
		if(agendamento == null || agendamento.getCdagendamento() == null)
			throw new SinedException("Par�metro inv�lido");
		
		StringBuilder sql = new StringBuilder();
		
		sql
			.append("select a.iteracoes as qtdeiteracoes ")
			.append("from agendamento a ")
			.append("where a.cdagendamento = " + agendamento.getCdagendamento() + " and a.iteracoes is not null;");
		
			
		List<Integer> lista = null;

		SinedUtil.markAsReader();
		lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Integer(rs.getInt("qtdeiteracoes"));
			}
		});
		
		return lista != null && lista.size() == 1 ? lista.get(0) : null;
	}
	
	/**
	 * M�todo que retorna a quantidade de itera��es j� realizada da conta a receber ou conta a pagar ou movimenta��o 
	 *
	 * @param agendamento
	 * @param movimentacao
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public Integer buscaQtdeIteracoes(Agendamento agendamento, Movimentacao movimentacao, Documento documento) {
		if(agendamento == null || agendamento.getCdagendamento() == null || (movimentacao == null && documento == null))
			throw new SinedException("Par�metros inv�lidos");
		
		StringBuilder sql = new StringBuilder();
		
		if(movimentacao != null)
			sql
				.append("select count(*) as qtdeiteracoes ")
				.append("from movimentacaoorigem mo ")
				.append("where mo.cdagendamento = " + agendamento.getCdagendamento() + "; ");
		else if(documento != null)
			sql
			.append("select count(*) as qtdeiteracoes ")
			.append("from documentoorigem docorigem ")
			.append("where docorigem.cdagendamento = " + agendamento.getCdagendamento() + "; ");
			
		List<Integer> lista = null;

		SinedUtil.markAsReader();
		lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Integer(rs.getInt("qtdeiteracoes"));
			}
		});
		
		return lista != null && lista.size() == 1 ? lista.get(0) : null;
	}
	
	public Agendamento loadTipooperacao(Agendamento agendamento){
		if(agendamento == null)
			throw new SinedException("Erro ao buscar Tipo Opera��o.");
		
		return query()
				.joinFetch("agendamento.tipooperacao tipoop")
				.where("agendamento = ?",agendamento)
				.unique();			
	}
	
	/**
	 * Retorna a quantidade de Agendamentos a consolidar
	 *
	 * @return
	 * @since 24/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdeAConsolidar() {
		
		return newQueryBuilder(Long.class)
				
				.from(Agendamento.class)
				.setUseTranslator(false) 
				.select("count(*)")
				.join("agendamento.aux_agendamento aux_agendamento")
				.where("aux_agendamento.situacao = ?", SituacaoAgendamento.A_CONSOLIDAR.getValue())
				.where("agendamento.ativo = ?", Boolean.TRUE)
				.unique()
				.intValue();
	}
	
	/**
	 * Retorna a quantidade de Agendamentos atrasados
	 *
	 * @return
	 * @since 24/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdeAtrasados() {
		return newQueryBuilder(Long.class)
		
		.from(Agendamento.class)
		.setUseTranslator(false) 
		.select("count(*)")
		.join("agendamento.aux_agendamento aux_agendamento")
		.where("aux_agendamento.situacao = ?", SituacaoAgendamento.ATRASADO.getValue())
		.where("agendamento.ativo = ?", Boolean.TRUE)
		.unique()
		.intValue();
	}
	
	/**
	 * M�todo que retorna um booleano que indica se existem agendamentos com data de pr�ximo vencimento referente a um per�odo fechado.
	 * @param whereIn
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaListaFechamento(String whereIn){
		if(whereIn == null || "".equals((whereIn)) )
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
				.setUseTranslator(Boolean.FALSE)
				.from(Agendamento.class)
				.select("count(*)")
				.whereIn("agendamento.cdagendamento", whereIn)
				.where("exists ( SELECT f.id " +
								" FROM Fechamentofinanceiro f " +
								" WHERE f.empresa = agendamento.empresa " +
								" AND f.datainicio <= agendamento.dtproximo " +
								" AND f.datalimite >= agendamento.dtproximo " +
								" AND f.ativo = ?)", new Object[]{Boolean.TRUE})
				.unique() > 0;
	}
	
	/**
	 * M�todo que carrega o agendamento com os dados da pessoa e empresa
	 *
	 * @param agendamento
	 * @return
	 * @author Luiz Fernando
	 */
	public Agendamento loadWithPessoa(Agendamento agendamento){
		return query()
				.select("agendamento.cdagendamento, empresa.cdpessoa, pessoa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, pessoa.nome ")
				.leftOuterJoin("agendamento.pessoa pessoa")
				.leftOuterJoin("agendamento.empresa empresa")
				.where("agendamento = ?", agendamento)
				.unique();
	}
	
	/**
	 * 
	 * @param whereIn
	 * @return
	 */
	public List<Agendamento> loadListAgendamentoEstorno(String whereIn) {
		return query()
			.select("agendamento.cdagendamento, aux_agendamento.situacao")
			.join("agendamento.aux_agendamento aux_agendamento")
			.whereIn("agendamento.cdagendamento", whereIn)
			.list();
	}
	
	/**
	 * 
	 * @param listaAgendamento
	 */
	public void estornarAgendamento(List<Agendamento> listaAgendamento) {
		getJdbcTemplate().update("update agendamento set dtfim = null where cdagendamento in ("+
				SinedUtil.listAndConcatenate(listaAgendamento, "cdagendamento", ",")+")");
		updateAux();
	}
	
	/**
	 * M�todo retorna os agendamentos para valida��o de acordo com centro de custo
	 * @param centrocusto
	 * @return
	 * @since 17/03/2016
	 * @author C�sar
	 */
	public List<Agendamento> findByCentroCustoValidacao(Centrocusto centrocusto){
		
		if(centrocusto == null)
			throw new SinedException("Par�metro Incorreto para Valida��o");
		
		return query()
				.select("agendamento.cdagendamento, aux_agendamento.cdagendamento , aux_agendamento.situacao")
				.join("agendamento.aux_agendamento aux_agendamento")
				.join("agendamento.rateio rateio")
				.join("rateio.listaRateioitem rateioitem")
				.where("rateioitem.centrocusto = ?", centrocusto)
				.list();
	}
	
	/**
	 * M�todo retorna os agendamentos para valida��o de acordo com a conta gerencial
	 * @param centrocusto
	 * @return
	 * @since 17/03/2016
	 * @author C�sar
	 */
	public List<Agendamento> findByContaGerencialValidacao(Contagerencial contagerencial){
		
		if(contagerencial == null)
			throw new SinedException("Par�metro Incorreto para Valida��o");
		
		return query()
				.select("agendamento.cdagendamento, aux_agendamento.cdagendamento , aux_agendamento.situacao")
				.join("agendamento.aux_agendamento aux_agendamento")
				.join("agendamento.rateio rateio")
				.join("rateio.listaRateioitem rateioitem")
				.where("rateioitem.contagerencial = ?", contagerencial)
				.list();
	}
	
	/**
	 * M�todo retorna os agendamentos para valida��o de acordo com a conta contabil
	 * @param centrocusto
	 * @return
	 * @since 29/10/2019	
	 * @author Arthur Gomes
	 */
	public List<Agendamento> findByContaGerencialValidacao(ContaContabil contacontabil){
		
		if(contacontabil == null)
			throw new SinedException("Par�metro Incorreto para Valida��o");
		
		return query()
				.select("agendamento.cdagendamento, aux_agendamento.cdagendamento , aux_agendamento.situacao")
				.join("agendamento.aux_agendamento aux_agendamento")
				.join("agendamento.rateio rateio")
				.join("rateio.listaRateioitem rateioitem")
				.where("rateioitem.contagerencial = ?", contacontabil)
				.list();
	}
	
	public List<Agendamento> findByDiasFaltantes(int i, Date data, Date dateToSearch) {
		Date proxima = SinedDateUtils.addDiasData(data, 2);
		Integer situacao = SituacaoAgendamento.FINALIZADO.getValue();
		return querySined()
				.setUseWhereClienteEmpresa(false)
				.setUseWhereEmpresa(false)
				.setUseWhereFornecedorEmpresa(false)
				.setUseWhereProjeto(false)
				.select("agendamento.cdagendamento, empresa.cdpessoa ")
				.join("agendamento.aux_agendamento aux_agendamento")
				.leftOuterJoin("agendamento.empresa empresa")
				.where("aux_agendamento.situacao <> ? ",situacao)
				.where("agendamento.dtproximo <= ? ", proxima )
				.where("agendamento.dtproximo >= ?  ", data)
				.where("agendamento.ativo = true")
				.list();
//		.where("agendamento.dtinicio >=  '?' ", dateToSearch)
	}
	public List<Agendamento> findByAtraso(Date data, Date dateToSearch) {
		return querySined()
				.setUseWhereClienteEmpresa(false)
				.setUseWhereEmpresa(false)
				.setUseWhereFornecedorEmpresa(false)
				.setUseWhereProjeto(false)
				.select("agendamento.cdagendamento, empresa.cdpessoa ")
				.join("agendamento.aux_agendamento aux_agendamento")
				.leftOuterJoin("agendamento.empresa empresa")
				.where("agendamento.dtproximo < ?", data)
				.where("aux_agendamento.situacao = ?", SituacaoAgendamento.ATRASADO.getValue())
				.wherePeriodo("agendamento.dtproximo", dateToSearch, SinedDateUtils.currentDate())
				.list();
	}
	public Agendamento findByCdAgendamento(Integer cdAgendamento) {
		return query()
				.select("agendamento.cdagendamento,aux_agendamento.situacao")
				.leftOuterJoin("agendamento.aux_agendamento aux_agendamento")
				.where("agendamento.cdagendamento = ?", cdAgendamento)
				.unique();
	}
	
	public void updateValor(Agendamento agendamento, Money valor){
		if(agendamento != null && agendamento.getCdagendamento() != null && valor != null){
			String sql = "update agendamento set valor = ? where cdagendamento = ? ";
			getJdbcTemplate().update(sql, new Object[]{valor.getValue().doubleValue()*100d, agendamento.getCdagendamento()});
		}
	}
	
	public List<Agendamento> findAutoCompleteWithEmpresa(String nome, Empresa empresa){
		return query()
				.autocomplete()
				.select("agendamento.cdagendamento, agendamento.descricao ")
				.whereLikeIgnoreAll("agendamento.descricao", nome)
				.where("agendamento.empresa = ?", empresa)
				.list();
					
	}
	public void updateDtproximo(Agendamento agendamento, Date dtProximo) {
		if(agendamento != null && agendamento.getCdagendamento() != null && dtProximo != null){
			String sql = "update agendamento set dtproximo = ? where cdagendamento = ? ";
			getJdbcTemplate().update(sql, new Object[]{dtProximo, agendamento.getCdagendamento()});
		}
	}
	public Agendamento findValorAgendamento(Agendamento agendamento) {
		return query()
				.select("agendamento.valor")
				.where("agendamento = ?",agendamento)
				.unique();
	}
	
	public Agendamento findAgendamento (Agendamento agendamento){
		return query()
				.select("agendamento.cdagendamento,agendamento.descricao, pessoa.cdpessoa, rateio.cdrateio, rateioitem.cdrateioitem, rateioitem.valor, rateioitem.percentual, contagerencial.cdcontagerencial, " +
						"centrocusto.cdcentrocusto, projeto.cdprojeto")
				.leftOuterJoin("agendamento.rateio rateio")
				.leftOuterJoin("agendamento.pessoa pessoa")
				.leftOuterJoin("rateio.listaRateioitem rateioitem")
				.leftOuterJoin("rateioitem.contagerencial contagerencial")
				.leftOuterJoin("rateioitem.centrocusto centrocusto")
				.leftOuterJoin("rateioitem.projeto projeto")
				.where("agendamento.cdagendamento=?",agendamento.getCdagendamento())
				.unique();
		
		
	}
}
