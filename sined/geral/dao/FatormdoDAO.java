package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Fatormdo;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.FatorMdoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("fatormdo.descricao")
public class FatormdoDAO extends GenericDAO<Fatormdo>{
	
	/**
	 * Carrega a lista de fatores de m�o-de-obra de um determinado or�amento.
	 *
	 * @param orcamento
	 * @return List<Fatormdo>
	 * @throws SinedException - caso o par�metro or�amento seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Fatormdo> findByOrcamentoForFlex(Orcamento orcamento){
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		return query()
			.select("fatormdo.cdfatormdo, fatormdo.descricao, fatormdo.total, fatormdo.formula")
			.join("fatormdo.orcamento orcamento")
			.where("orcamento = ?", orcamento)
			.orderBy("fatormdo.descricao")
			.list();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Fatormdo> query) {
		query
			.select("fatormdo.cdfatormdo, fatormdo.descricao, fatormdo.total, fatormdo.formula, " +
					"fatormdoitem.cdfatormdoitem, fatormdoitem.nome, fatormdoitem.sigla, fatormdoitem.valor")
			.leftOuterJoin("fatormdo.listaFatormdoitem fatormdoitem")
			.orderBy("fatormdoitem.sigla");
	}
	
	/**
	 * Busca os fatores de m�o-de-obra para a listagem do crud em flex.
	 *
	 * @param filtro
 	 * @return List<Fatormdo>
 	 * 
 	 * @throws SinedException - caso o or�amento seja nulo
	 * 
 	 * @author Rodrigo Alvarenga
	 */
	public List<Fatormdo> findForListagemFlex(FatorMdoFiltro filtro) {
		if (filtro.getOrcamento() == null || filtro.getOrcamento().getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		return 
			query()
				.select("fatormdo.cdfatormdo, fatormdo.descricao, fatormdo.total, fatormdo.formula")
				.join("fatormdo.orcamento orcamento")
				.where("orcamento = ?", filtro.getOrcamento())
				.whereLikeIgnoreAll("fatormdo.descricao", filtro.getDescricao())
				.orderBy("fatormdo.descricao")
				.list();
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaFatormdoitem");
	}
	
	@Override
	public void saveOrUpdate(Fatormdo bean) {
		try {
			super.saveOrUpdate(bean);
		}
		catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "UK_FATORMDO_1")) {
				throw new SinedException("J� existe um fator de m�o-de-obra cadastrado com a descri��o " + bean.getDescricao() + ".");
			}
			if (DatabaseError.isKeyPresent(e, "UK_FATORMDOITEM_1")) {
				throw new SinedException("Existem itens deste fator de m�o-de-obra cadastrados com a mesma sigla.");
			}
			if (DatabaseError.isKeyPresent(e, "UK_FATORMDOITEM_2")) {
				throw new SinedException("Existem itens deste fator de m�o-de-obra cadastrados com o mesmo nome.");
			}			
			throw new SinedException(e.getMessage());
		}
	}
}
