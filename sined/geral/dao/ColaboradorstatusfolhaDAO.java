package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Colaboradorstatusfolha;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColaboradorstatusfolhaDAO extends GenericDAO<Colaboradorstatusfolha> {

	@Override
	public void updateListagemQuery(QueryBuilder<Colaboradorstatusfolha> query, FiltroListagem _filtro) {
//		EtniaFiltro filtro = (EtniaFiltro) _filtro;
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Colaboradorstatusfolha> query) {
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
	}
	

	/* singleton */
	private static ColaboradorstatusfolhaDAO instance;
	public static ColaboradorstatusfolhaDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradorstatusfolhaDAO.class);
		}
		return instance;
	}
}