package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Pessoaatalho;
import br.com.linkcom.sined.geral.bean.Tela;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PessoaatalhoDAO extends GenericDAO<Pessoaatalho> {
	
	/**
	 * Retorna uma lista com os atalhos disponíveis para a pessoa
	 * passada como parâmetro.
	 * 
	 * @param pesssoa
	 * @return
	 * @since 30/03/2011
	 */
	public List<Pessoaatalho> findByPessoa(Pessoa pessoa) {
		return query()
			.select("pessoaatalho.cdpessoaatalho, " +
					"tela.cdtela, tela.descricao, tela.path")
			.join("pessoaatalho.pessoa pessoa")
			.join("pessoaatalho.tela tela")
			.where("pessoa = ?", pessoa)
			.orderBy("tela.descricao")
			.list();
	}
	
	/**
	 * Carrega um objeto de <code>Pesssoaatalho</code>
	 * a partir dos parâmetros passados.
	 * 
	 * @param pesssoa
	 * @param tela
	 * @return
	 * @since 30/03/2011
	 */
	public Pessoaatalho loadByPessoaTela(Pessoa pessoa, Tela tela) {
		return query()
			.join("pessoaatalho.pessoa pessoa")
			.join("pessoaatalho.tela tela")
			.where("pessoa = ?", pessoa)
			.where("tela = ?", tela)
			.unique();
	}
	
}
