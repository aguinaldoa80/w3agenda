package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Reserva;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ReservaDAO extends GenericDAO<Reserva>{

	
	public List<Reserva> findByProducaoagenda(String whereIn){
		if (StringUtils.isEmpty(whereIn)) {
			throw new SinedException("Par�metro Inv�lido.");
		}
		return query()
			.select("reserva.cdreserva, producaoagenda.cdproducaoagenda")
			.join("reserva.producaoagenda producaoagenda")
			.whereIn("producaoagenda.cdproducaoagenda", whereIn)
			.list();
	}
	
	public void deleteReservaByProducaoagenda(String whereIn){
		if (StringUtils.isEmpty(whereIn)) {
			throw new SinedException("Par�metro Inv�lido.");
		}
		getJdbcTemplate().execute("DELETE FROM reserva r WHERE r.cdproducaoagenda IN (" + whereIn + ") ");
	}
	
	public void deleteReservaByPedidovenda(String whereIn){
		if (StringUtils.isEmpty(whereIn)) {
			throw new SinedException("Par�metro Inv�lido.");
		}
		getJdbcTemplate().execute("DELETE FROM reserva r WHERE r.cdpedidovenda IN (" + whereIn + ") ");
	}
	
	/**
	 * M�todo que retorna a quantidade reservada do material
	 *
	 * @param material
	 * @param empresa
	 * @param localarmazenagem
	 * @return
	 * @author Jo�o Vitor
	 * @param empresa 
	 * @since 21/05/2015
	 */
	public Double getQtdeReservada(Material material, Empresa empresa, Localarmazenagem localarmazenagem, Loteestoque loteestoque, String whereNotInReserva){
		Double total = null;
		
		if(material == null || material.getCdmaterial() == null){
			throw new SinedException("Material n�o pode ser nulo");
		}
		
		 total =  newQueryBuilder(Double.class)
		.select("SUM(reserva.quantidade)")
		.setUseTranslator(false)
		.from(Reserva.class)
		.where("reserva.empresa=?", empresa)
		.where("reserva.material=?", material)
		.where("reserva.localarmazenagem=?", localarmazenagem)
		.where("reserva.loteestoque = ?", loteestoque)
		.whereIn("reserva.cdreserva not ", whereNotInReserva)
		.unique();
		
		return total != null ? total : 0d;
	}

	/**
	 * Remove as reservas das ordens de servi�o
	 *
	 * @param whereInOSV
	 * @author Rodrigo Freitas
	 * @since 14/11/2016
	 */
	public void deleteByOrdemservicoveterinaria(String whereInOSV) {
		if (StringUtils.isEmpty(whereInOSV)) {
			throw new SinedException("Par�metro Inv�lido.");
		}
		getJdbcTemplate().execute("DELETE FROM reserva r WHERE r.cdordemservicoveterinaria IN (" + whereInOSV + ") ");
	}
	
	public void deleteByOrdemservicoveterinariaMaterial(String whereInOSVM) {
		if (StringUtils.isNotBlank(whereInOSVM)) {
			getJdbcTemplate().execute("DELETE FROM reserva r WHERE r.cdreserva IN (select o.cdreserva from Ordemservicoveterinariamaterial o where o.cdordemservicoveterinariamaterial in (" + whereInOSVM + ") and o.cdreserva is not null) ");
		}
	}

	public Reserva loadByPedidovendamaterial(Integer cdpedidovendamaterial, Material material){
		return query()
			.select("reserva.cdreserva, pedidovendamaterial.cdpedidovendamaterial, reserva.quantidade")
			.join("reserva.pedidovendamaterial pedidovendamaterial")
			.where("pedidovendamaterial.cdpedidovendamaterial = ?", cdpedidovendamaterial)
			.where("reserva.material = ?", material)
			.unique();
	}
	
	public Reserva loadByVendamaterial(Integer cdvendamaterial, Material material){
		return query()
			.select("reserva.cdreserva, vendamaterial.cdvendamaterial, reserva.quantidade")
			.join("reserva.vendamaterial vendamaterial")
			.where("vendamaterial.cdvendamaterial = ?", cdvendamaterial)
			.where("reserva.material = ?", material)
			.unique();
	}
	
	public Reserva loadByExpedicaoitem(Integer cdexpedicaoitem, Material material){
		return query()
			.select("reserva.cdreserva, expedicaoitem.cdexpedicaoitem, reserva.quantidade")
			.join("reserva.expedicaoitem expedicaoitem")
			.where("expedicaoitem.cdexpedicaoitem = ?", cdexpedicaoitem)
			.where("reserva.material = ?", material)
			.unique();
	}
	
	public void desfazerReserva(Reserva bean, Double qtde){
		if(Util.objects.isPersistent(bean)){
			if(bean.getQuantidade().compareTo(qtde) > 0){
				this.updateQtdeReservada(bean, bean.getQuantidade().doubleValue() - qtde.doubleValue());
			}else{
				this.delete(bean);
			}
		}		
	}
	
	public void updateQtdeReservada(Reserva bean, Double newQtde){
		getHibernateTemplate().bulkUpdate("UPDATE Reserva r SET r.quantidade = ? WHERE r.cdreserva = "+bean.getCdreserva(), new Object[]{newQtde});
	}
	
	public List<Reserva> findByPedidovenda(Pedidovenda pedidovenda, Material material){
		return query()
			.select("reserva.cdreserva, pedidovenda.cdpedidovenda, reserva.quantidade")
			.join("reserva.pedidovenda pedidovenda")
			.where("pedidovenda = ?", pedidovenda)
			.where("reserva.material = ?", material)
			.list();
	}
	
	public List<Reserva> findByVenda(Venda venda, Material material){
		return query()
			.select("reserva.cdreserva, venda.cdvenda, reserva.quantidade")
			.join("reserva.venda venda")
			.join("reserva.material material")
			.where("venda = ?", venda)
			.where("material = ?", material)
			.list();
	}
	
	public Reserva findByVendamaterial(Vendamaterial vendamaterial, Material material){
		if(Util.objects.isNotPersistent(vendamaterial)){
			return null;
		}
		return query()
			.select("reserva.cdreserva, vendamaterial.cdvendamaterial, reserva.quantidade, loteestoque.cdloteestoque")
			.join("reserva.vendamaterial vendamaterial")
			.join("reserva.material material")
			.leftOuterJoin("reserva.loteestoque loteestoque")
			.where("vendamaterial = ?", vendamaterial)
			.where("material = ?", material)
			.setMaxResults(1)
			.unique();
	}
	
	public Reserva findByExpedicaoitem(Expedicaoitem expedicaoitem, Material material){
		if(Util.objects.isNotPersistent(expedicaoitem)){
			return null;
		}
		return query()
			.select("reserva.cdreserva, expedicaoitem.cdexpedicaoitem, reserva.quantidade, loteestoque.cdloteestoque")
			.join("reserva.expedicaoitem expedicaoitem")
			.join("reserva.material material")
			.leftOuterJoin("reserva.loteestoque loteestoque")
			.where("expedicaoitem = ?", expedicaoitem)
			.where("material = ?", material)
			.setMaxResults(1)
			.unique();
	}
	
	public List<Reserva> findByMaterial(Integer codMAterial, Integer cdEmpresa, Integer cdLocalarmazenagem){
		return query()
				.select("reserva.cdreserva, venda.cdvenda, expedicao.cdexpedicao, " +
						"pedidovenda.cdpedidovenda,reserva.quantidade," +
						"material.nome,material.cdmaterial,material.identificacao,empresa.nomefantasia")
				.join("reserva.empresa empresa")
				.leftOuterJoin("reserva.venda venda")
				.leftOuterJoin("reserva.pedidovenda pedidovenda")
				.leftOuterJoin("reserva.expedicao expedicao")
				.leftOuterJoin("reserva.material material")
				.where("material.cdmaterial = ?", codMAterial)
				.where("empresa.cdpessoa =?",cdEmpresa)
				.where("reserva.localarmazenagem.cdlocalarmazenagem = ?", cdLocalarmazenagem)
				.list();
	}

	public boolean existReservaPedidoVenda(Pedidovenda pv) {
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.leftOuterJoin("reserva.pedidovenda pedidovenda")
				.where("pedidovenda = ?", pv)
				.unique() > 0;
	}
	
}