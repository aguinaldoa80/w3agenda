package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaInstrucao;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRemessaTipoEnum;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class BancoConfiguracaoRemessaInstrucaoDAO extends GenericDAO<BancoConfiguracaoRemessaInstrucao> {

	public List<BancoConfiguracaoRemessaInstrucao> findByContacarteiraForRemessa(Contacarteira contacarteira){
		return query()
					.join("bancoConfiguracaoRemessaInstrucao.bancoConfiguracaoRemessa bancoConfiguracaoRemessa")
					.join("bancoConfiguracaoRemessa.listaContacarteira contacarteira")
					.where("contacarteira = ?", contacarteira)
					.where("bancoConfiguracaoRemessa.tipo = ?", BancoConfiguracaoRemessaTipoEnum.COBRANCA)
					.orderBy("bancoConfiguracaoRemessaInstrucao.codigo")
					.list();
	}	
	
	public List<BancoConfiguracaoRemessaInstrucao> findAllForRemessaPagamento(Contacarteira contacarteira){
		return query()
					.join("bancoConfiguracaoRemessaInstrucao.bancoConfiguracaoRemessa bancoConfiguracaoRemessa")
					.where("bancoConfiguracaoRemessa.tipo = ?", BancoConfiguracaoRemessaTipoEnum.COBRANCA)
					.orderBy("bancoConfiguracaoRemessaInstrucao.codigo")
					.list();
	}	
}
