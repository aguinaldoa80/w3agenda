package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Romaneioorigem;
import br.com.linkcom.sined.geral.bean.Romaneiosituacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RomaneioorigemDAO extends GenericDAO<Romaneioorigem> {

	/**
	 * Busca os romaneios a partir das requisi��es de material
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/04/2014
	 */
	public List<Romaneioorigem> findByRequisicaomaterial(String whereIn) {
		QueryBuilder<Romaneioorigem> query = query()
					.select("romaneioorigem.cdromaneioorigem, requisicaomaterial.cdrequisicaomaterial, " +
							"romaneio.cdromaneio")
					.join("romaneioorigem.requisicaomaterial requisicaomaterial")
					.join("romaneioorigem.romaneio romaneio")
					.where("romaneio.romaneiosituacao <> ?", Romaneiosituacao.CANCELADA);
		
		SinedUtil.quebraWhereIn("requisicaomaterial.cdrequisicaomaterial", whereIn, query);
		return query.list();
	}
	
	/**
	 * Busca os romaneios a partir da Entrega
	 *
	 * @param entrega
	 * @return
	 * @author Lucas Costa
	 * @since 05/11/2014
	 */
	public List<Romaneioorigem> findForValidaRomaneioEntrega(Entrega entrega) {
		if(entrega == null || entrega.getCdentrega()== null){
			throw new SinedException("Par�metro inv�lido.");	
		}
		
		return query()
			.select("romaneioorigem.cdromaneioorigem, entrega.cdentrega, romaneio.cdromaneio, "+
					"listaRomaneioitem.cdromaneioitem, listaRomaneioitem.qtde, material.cdmaterial, "+
					"localarmazenagemorigem.cdlocalarmazenagem, localarmazenagemdestino.cdlocalarmazenagem")
			.join("romaneioorigem.entrega entrega")
			.join("romaneioorigem.romaneio romaneio")
			.join("romaneio.localarmazenagemorigem localarmazenagemorigem")
			.join("romaneio.localarmazenagemdestino localarmazenagemdestino")
			.join("romaneio.listaRomaneioitem listaRomaneioitem")
			.join("listaRomaneioitem.material material")
			.where("romaneio.romaneiosituacao <> ?", Romaneiosituacao.CANCELADA)
			.where("entrega.cdentrega = ?", entrega.getCdentrega())
 			.list();
	}
	
}
