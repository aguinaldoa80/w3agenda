package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Solicitacaoservicotipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.SolicitacaoservicotipoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class SolicitacaoservicotipoDAO extends GenericDAO<Solicitacaoservicotipo>{

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listasolicitacaoitem");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Solicitacaoservicotipo> query) {
		 query.select("solicitacaoservicotipo.cdsolicitacaoservicotipo, solicitacaoservicotipo.nome," +
		 		"solicitacaoservicotipo.descricao, solicitacaoservicotipo.ativo, " +
		 		"listasolicitacaoitem.cdsolicitacaoservicotipoitem," +
		 		"listasolicitacaoitem.nome, listasolicitacaoitem.obrigatorio, solicitacaoservicotipo.dtaltera," +
		 		"solicitacaoservicotipo.cdusuarioaltera")
		 		.leftOuterJoin("solicitacaoservicotipo.listasolicitacaoitem listasolicitacaoitem");
	}
	 
	@Override
	public void updateListagemQuery(QueryBuilder<Solicitacaoservicotipo> query,	FiltroListagem _filtro) {
		SolicitacaoservicotipoFiltro filtro = (SolicitacaoservicotipoFiltro) _filtro;
		
		query.select("solicitacaoservicotipo.cdsolicitacaoservicotipo, solicitacaoservicotipo.nome," +
		 		"solicitacaoservicotipo.descricao, solicitacaoservicotipo.ativo")
		 		.whereLikeIgnoreAll("solicitacaoservicotipo.nome" , filtro.getNome())
		 		.where("solicitacaoservicotipo.ativo=?", filtro.getAtivo());
		 
		
	}
}
