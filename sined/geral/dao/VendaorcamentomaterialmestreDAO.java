package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterialmestre;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VendaorcamentomaterialmestreDAO extends GenericDAO<Vendaorcamentomaterialmestre>{

	public List<Vendaorcamentomaterialmestre> findByVendaOrcamento(Vendaorcamento vendaorcamento) {
		return query()
					.select("vendaorcamentomaterialmestre.cdvendaorcamentomaterialmestre, vendaorcamentomaterialmestre.altura, " +
							"vendaorcamentomaterialmestre.nomealternativo, vendaorcamentomaterialmestre.exibiritenskitflexivel, " +
							"vendaorcamentomaterialmestre.largura, vendaorcamentomaterialmestre.comprimento, vendaorcamentomaterialmestre.peso, " +
							"vendaorcamentomaterialmestre.preco, vendaorcamentomaterialmestre.dtprazoentrega, vendaorcamentomaterialmestre.desconto, " +
							"vendaorcamentomaterialmestre.qtde, material.cdmaterial, material.nome, material.identificacao, material.kitflexivel, vendaorcamento.cdvendaorcamento, " +
							"materialgrupo.cdmaterialgrupo, vendaorcamentomaterialmestre.observacao, vendaorcamentomaterialmestre.identificadorinterno, " +
							"unidademedida.cdunidademedida, unidademedida.nome, material.ncmcompleto, material.codigobarras, ncmcapitulo.cdncmcapitulo, ncmcapitulo.descricao, " +
							"vendaorcamentomaterialmestre.tipotributacaoicms, " +
							"vendaorcamentomaterialmestre.tipocobrancaicms, vendaorcamentomaterialmestre.valorbcicms, vendaorcamentomaterialmestre.icms, " +
							"vendaorcamentomaterialmestre.valoricms, vendaorcamentomaterialmestre.valorbcicmsst, vendaorcamentomaterialmestre.icmsst, " +
							"vendaorcamentomaterialmestre.valoricmsst, vendaorcamentomaterialmestre.reducaobcicmsst, vendaorcamentomaterialmestre.margemvaloradicionalicmsst, " +
							"vendaorcamentomaterialmestre.valorbcfcp, vendaorcamentomaterialmestre.fcp, vendaorcamentomaterialmestre.valorfcp, " +
							"vendaorcamentomaterialmestre.valorbcfcpst, vendaorcamentomaterialmestre.fcpst, vendaorcamentomaterialmestre.valorfcpst, " +
							"vendaorcamentomaterialmestre.valorbcdestinatario, vendaorcamentomaterialmestre.valorbcfcpdestinatario, vendaorcamentomaterialmestre.fcpdestinatario, " +
							"vendaorcamentomaterialmestre.icmsdestinatario, vendaorcamentomaterialmestre.icmsinterestadual, vendaorcamentomaterialmestre.icmsinterestadualpartilha, " +
							"vendaorcamentomaterialmestre.valorfcpdestinatario, vendaorcamentomaterialmestre.valoricmsdestinatario, vendaorcamentomaterialmestre.valoricmsremetente, " +
							"vendaorcamentomaterialmestre.percentualdesoneracaoicms, vendaorcamentomaterialmestre.valordesoneracaoicms, vendaorcamentomaterialmestre.abaterdesoneracaoicms, " +
							"vendaorcamentomaterialmestre.difal, vendaorcamentomaterialmestre.valordifal, ncmcapituloVenda.cdncmcapitulo, cfop.cdcfop, " +
							"vendaorcamentomaterialmestre.ncmcompleto, vendaorcamentomaterialmestre.modalidadebcicms, vendaorcamentomaterialmestre.modalidadebcicmsst, " +
							"vendaorcamentomaterialmestre.valoripi, vendaorcamentomaterialmestre.ipi, vendaorcamentomaterialmestre.tipocobrancaipi, " +
							"vendaorcamentomaterialmestre.tipoCalculoIpi, vendaorcamentomaterialmestre.aliquotaReaisIpi, " +
							"grupotributacao.cdgrupotributacao," +
							"materialtipo.cdmaterialtipo, materialcategoria.cdmaterialcategoria")
					.join("vendaorcamentomaterialmestre.vendaorcamento vendaorcamento")
					.join("vendaorcamentomaterialmestre.material material")
					.leftOuterJoin("vendaorcamentomaterialmestre.cfop cfop")
					.leftOuterJoin("vendaorcamentomaterialmestre.ncmcapitulo ncmcapituloVenda")
					.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
					.leftOuterJoin("material.materialgrupo materialgrupo")
					.leftOuterJoin("material.unidademedida unidademedida")
					.leftOuterJoin("vendaorcamentomaterialmestre.grupotributacao grupotributacao")
					.leftOuterJoin("material.materialtipo materialtipo")
					.leftOuterJoin("material.materialcategoria materialcategoria")
					.where("vendaorcamento = ?",vendaorcamento)
					.list();
	}

}
