package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Oportunidadeproposta;
import br.com.linkcom.sined.geral.bean.Oportunidadesituacao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.auxiliar.TotalizadorOportunidade;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.OportunidadeFiltro;
import br.com.linkcom.sined.modulo.crm.controller.report.filter.EmitirfunilvendasFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OportunidadeDAO extends GenericDAO<Oportunidade>{
	
	protected ArquivoDAO arquivoDAO;
	private EmpresaService empresaService;
	private UsuarioService usuarioService;
 	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Oportunidade> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		
		OportunidadeFiltro filtro = (OportunidadeFiltro)_filtro;
		query
			.select("distinct oportunidade.cdoportunidade, oportunidade.nome, oportunidade.ciclo, oportunidade.revendedor, " +
					"oportunidadesituacao.cdoportunidadesituacao, " +
					"oportunidadesituacao.nome, oportunidadesituacao.situacaoantesfinal, oportunidadesituacao.situacaofinal, " +
					"oportunidade.campanha, contacrm.nome, contacrm.cdcontacrm, responsavel.cdpessoa, responsavel.nome, oportunidade.dtInicio, oportunidade.dtretorno, " +
					"oportunidade.probabilidade, oportunidade.proximopasso, oportunidadefonte.nome, oportunidade.valortotal, " +
					"imagem.cdarquivo, cliente.cdpessoa, cliente.nome, cliente.email, oportunidade.tipopessoacrm, oportunidadesituacao.corletra, oportunidadesituacao.corfundo, oportunidadesituacao.letra ")
					.leftOuterJoin("oportunidade.contacrm contacrm")
					.leftOuterJoin("contacrm.responsavel responsavelcontacrm")
					.leftOuterJoin("contacrm.listcontacrmsegmento listcontacrmsegmento")
					.leftOuterJoin("responsavelcontacrm.endereco endereco")
					.leftOuterJoin("endereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("oportunidade.oportunidadesituacao oportunidadesituacao")
					.leftOuterJoin("oportunidadesituacao.imagem imagem")
					.leftOuterJoin("oportunidade.campanha campanha")
					.leftOuterJoin("oportunidade.listoportunidadehistorico listoportunidadehistorico")
//					.leftOuterJoin("oportunidade.projeto projeto")
					.leftOuterJoin("oportunidade.responsavel responsavel")
					.leftOuterJoin("oportunidade.listaOportunidadematerial listaOportunidadematerial")
					.leftOuterJoin("listaOportunidadematerial.material material")
					.leftOuterJoin("oportunidade.oportunidadefonte oportunidadefonte")
					.leftOuterJoin("oportunidade.cliente cliente")
					.leftOuterJoin("cliente.listaClienteSegmento listaClienteSegmento")
					.leftOuterJoin("oportunidade.propostacaixa propostacaixa")
//					.leftOuterJoin("responsavel.endereco endereco")
//					.leftOuterJoin("endereco.municipio municipio")
				.where("oportunidade.cdoportunidade = ?", filtro.getCdoportunidade())
				.whereLikeIgnoreAll("oportunidade.nome", filtro.getNome())
				.where("contacrm = ?", filtro.getContacrm())
				.where("listcontacrmcontato = ?", filtro.getContacrmcontato())
				.where("oportunidade.campanha = ?", filtro.getCampanha())
//				.where("oportunidadesituacao = ?", filtro.getOportunidadesituacao())				
				.where("oportunidade.probabilidade >= ?", filtro.getProbabilidadeinicio())
				.where("oportunidade.probabilidade <= ?", filtro.getProbabilidadefim())
				.where("oportunidade.dtretorno >= ?", filtro.getDtretornoinicio())
				.where("oportunidade.dtretorno <=?", filtro.getDtretornofim())
				.where("oportunidade.dtInicio >= ?", filtro.getDtInicioinicio())
				.where("oportunidade.dtInicio <= ?", filtro.getDtIniciofim())
				.where("oportunidade.oportunidadefonte = ?", filtro.getOportunidadefonte())
//				.where("oportunidade.ciclo = ?", filtro.getCiclo())
				.where("oportunidade.ciclo >= ?", filtro.getIniciociclo())
				.where("oportunidade.ciclo <= ?", filtro.getFimciclo())
				.where("oportunidade.qtdehistorico >= ?", filtro.getIniciointeracao())
				.where("oportunidade.qtdehistorico <= ?", filtro.getFiminteracao())
				.where("oportunidade.cliente = ?", filtro.getCliente())
				.where("oportunidade.contato = ?", filtro.getContato())
				.where("oportunidade.propostacaixa = ?", filtro.getPropostacaixa())
				.where("oportunidade.empresa = ?", filtro.getEmpresa())
				.where("oportunidade.concorrente = ?", filtro.getConcorrente())
//				.openParentheses()
//				.where("projeto = ?", filtro.getProjeto()).or()
				.where("material = ?", filtro.getMaterial())
//				.closeParentheses()
//				.where("municipio = ?", filtro.getMunicipio())
//				.where("municipio.uf = ?", filtro.getUf())
				.where("listoportunidadehistorico.dtaltera >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getDtiniciohistorico()))
				.where("listoportunidadehistorico.dtaltera <= ?", SinedDateUtils.dateToTimestampFinalDia(filtro.getDtfimhistorico()))
				.orderBy("oportunidade.probabilidade desc, oportunidadesituacao.cdoportunidadesituacao, oportunidade.nome")
				.ignoreJoin("listcontacrmcontato", "listoportunidadehistorico", "uf", "propostacaixa", "campanha", "listaOportunidadematerial", "material");
		
			List<Oportunidadesituacao> listaOportunidadesituacao = filtro.getListaOportunidadesituacao();
			if (listaOportunidadesituacao != null)
				query.whereIn("oportunidadesituacao.cdoportunidadesituacao", CollectionsUtil.listAndConcatenate(listaOportunidadesituacao,"cdoportunidadesituacao", ","));
		
			if(filtro.getColaborador() != null && filtro.getColaborador().getCdpessoa() != null){
				query.openParentheses().where("responsavel.cdpessoa = ?", filtro.getColaborador().getCdpessoa());
				if(filtro.getAgencia() != null && filtro.getAgencia().getCdpessoa() != null)
					query.or().where("responsavel.cdpessoa = ?", filtro.getAgencia().getCdpessoa());
				query.closeParentheses();
				
				if(filtro.getUf() != null){					
					query
						.where("responsavelcontacrm = ?", filtro.getColaborador())
						.where("uf = ?", filtro.getUf());
					if(filtro.getMunicipio() != null)
						query.where("municipio = ?", filtro.getMunicipio());
				}				
				
			}else if(filtro.getAgencia() != null && filtro.getAgencia().getCdpessoa() != null){
				query.where("responsavel.cdpessoa = ?", filtro.getAgencia().getCdpessoa());
			}
			
			if(filtro.getColaborador() == null && filtro.getUf() != null && filtro.getUf().getCduf() != null)					
				query.where("uf = ?", filtro.getUf());				
			
			if(filtro.getColaborador() == null && filtro.getMunicipio() != null && filtro.getMunicipio().getCdmunicipio() != null)
				query.where("municipio = ?", filtro.getMunicipio());

			if(SinedUtil.isRestricaoClienteVendedor(usuarioLogado) &&
				!usuarioService.isVisualizaOutrasOportunidades(usuarioLogado)){
				query.where("responsavel.cdpessoa = ?", usuarioLogado.getCdpessoa());
			}
			
			if (filtro.getSegmento() != null) {
				query
					.openParentheses()
						.where("listaClienteSegmento.segmento = ?", filtro.getSegmento())
						.or()
						.where("listcontacrmsegmento.segmento = ?", filtro.getSegmento())
					.closeParentheses();
			}
			
		super.updateListagemQuery(query, _filtro);
	}
	
	public List<Oportunidade> loadWithLista(String whereIn, String orderBy, boolean asc) {
		
		QueryBuilder<Oportunidade> query = query()
					.select("oportunidade.cdoportunidade, oportunidade.nome, oportunidade.valortotal, oportunidade.ciclo, contacrm.cdcontacrm, " +
							"contacrm.nome, listcontacrmcontato.cdcontacrmcontato, listcontacrmcontato.nome, " +
							"responsavel.cdpessoa, responsavel.nome, oportunidade.dtInicio, oportunidade.dtretorno, oportunidadesituacao.cdoportunidadesituacao, oportunidadesituacao.nome, " +
							"oportunidadesituacao.situacaofinal, oportunidadesituacao.situacaoantesfinal, oportunidadesituacao.corletra, oportunidadesituacao.corfundo, oportunidadesituacao.letra, " +
							"oportunidade.probabilidade, oportunidade.proximopasso, oportunidade.tipopessoacrm," +
							"listaOportunidadematerial.cdoportunidadematerial, listaOportunidadematerial.quantitade, listaOportunidadematerial.valorunitario," +
							"material.cdmaterial, material.nome, unidademedida.cdunidademedida, unidademedida.nome ")
						.leftOuterJoin("oportunidade.contacrm contacrm")
						.leftOuterJoin("contacrm.listcontacrmcontato listcontacrmcontato")
						.leftOuterJoin("oportunidade.oportunidadesituacao oportunidadesituacao")
						.leftOuterJoin("oportunidade.listoportunidadehistorico listoportunidadehistorico")
						.leftOuterJoin("oportunidade.listaOportunidadematerial listaOportunidadematerial")
						.leftOuterJoin("listaOportunidadematerial.material material")
						.leftOuterJoin("listaOportunidadematerial.unidademedida unidademedida")
						.leftOuterJoin("oportunidade.responsavel responsavel")
					.whereIn("oportunidade.cdoportunidade", whereIn);
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("oportunidade.probabilidade desc, oportunidadesituacao.cdoportunidadesituacao, oportunidade.nome");
		}
		
		return query.list();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Oportunidade> query) {
		query
			.select("oportunidade.cdoportunidade, oportunidade.nome, oportunidade.tiporesponsavel, oportunidade.valortotal, " +
					"oportunidade.ciclo, oportunidade.dtInicio, oportunidade.dtPrevisaotermino, oportunidade.revendedor," +
					"oportunidade.probabilidade, oportunidade.dtretorno, oportunidade.proximopasso, " + 
					"contacrm.cdcontacrm, contacrm.nome, " +
					"contacrmcontato.cdcontacrmcontato, contacrmcontato.nome, " +
					"responsavel.cdpessoa, responsavel.nome, " +
					"oportunidadesituacao.cdoportunidadesituacao, oportunidadesituacao.nome, oportunidadesituacao.corletra, oportunidadesituacao.corfundo, oportunidadesituacao.letra, " +
					"imagem.cdarquivo, imagem.nome, " +
					"listoportunidadehistorico.cdoportunidadehistorico, listoportunidadehistorico.observacao, " +
					"listoportunidadehistorico.dtaltera, listoportunidadehistorico.cdusuarioaltera, " +
					"oportunidadesituacao2.cdoportunidadesituacao, " +
					"arquivo.cdarquivo, arquivo.nome, " +
					"oportunidadefonte.cdoportunidadefonte, oportunidadefonte.nome, " +
					"campanha.cdcampanha, campanha.nome, " +
					"frequencia.cdfrequencia, frequencia.nome, " +
					"oportunidadesituacao2.cdoportunidadesituacao, oportunidadesituacao2.nome, lead.cdlead, " +
					"oportunidade.tipopessoacrm, cliente.cdpessoa, cliente.nome, " +
					"contato.cdpessoa , contato.nome, " +
					"propostacaixa.cdpropostacaixa, propostacaixa.nome, " +
					"listaOportunidadeitem.valor, propostacaixaitem.cdpropostacaixaitem, propostacaixaitem.obrigatorio, " +
					"propostacaixaitem.nome, listaOportunidadeitem.cdoportunidadeitem, " +
					"empresa.cdpessoa, oportunidade.identificador, " +
					"listaOportunidadematerial.cdoportunidadematerial, listaOportunidadematerial.valorunitario, listaOportunidadematerial.quantidade, listaOportunidadematerial.prazoentregamaterial, " +
					"listaOportunidadematerial.desconto, listaOportunidadematerial.comissao, " +
					"material.cdmaterial, material.nome, material.valorcusto, material.valorvenda, " +
					"unidademedida.cdunidademedida, unidademedida.nome,listaOportunidadeproposta.cdoportunidadeproposta," +
					"listaOportunidadeproposta.valor,listaOportunidadeproposta.dataenvio,listaOportunidadeproposta.datavalidade,listaOportunidadeproposta.vigente," +
					"listaOportunidadeproposta.observacao, arquivoproposta.cdarquivo, arquivoproposta.nome," +
					"listaOportunidadematerial.observacao, concorrente.cdconcorrente, concorrente.nome, " +
					"oportunidade.site, oportunidade.estrategiaMarketing, oportunidade.forcas, oportunidade.fraquezas ")
			.leftOuterJoin("oportunidade.empresa empresa")
			.leftOuterJoin("oportunidade.contacrmcontato contacrmcontato")
			.leftOuterJoin("oportunidade.contacrm contacrm")
			.leftOuterJoin("contacrm.lead lead")
			.leftOuterJoin("oportunidade.responsavel responsavel")
			.leftOuterJoin("oportunidade.concorrente concorrente")
			.leftOuterJoin("oportunidade.oportunidadesituacao oportunidadesituacao")
			.leftOuterJoin("oportunidadesituacao.imagem imagem")
			.leftOuterJoin("oportunidade.listaOportunidadeitem listaOportunidadeitem")
			.leftOuterJoin("listaOportunidadeitem.propostacaixaitem propostacaixaitem")
			.leftOuterJoin("oportunidade.listoportunidadehistorico listoportunidadehistorico")
			.leftOuterJoin("listoportunidadehistorico.oportunidadesituacao oportunidadesituacao2")
			.leftOuterJoin("listoportunidadehistorico.arquivo arquivo")
			.leftOuterJoin("oportunidade.oportunidadefonte oportunidadefonte")	
			.leftOuterJoin("oportunidade.listaOportunidadematerial listaOportunidadematerial")
			.leftOuterJoin("listaOportunidadematerial.material material")
			.leftOuterJoin("listaOportunidadematerial.unidademedida unidademedida")
			.leftOuterJoin("oportunidade.campanha campanha")
			.leftOuterJoin("oportunidade.cliente cliente")
			.leftOuterJoin("oportunidade.contato contato")
			.leftOuterJoin("oportunidade.frequencia frequencia")
			.leftOuterJoin("oportunidade.propostacaixa propostacaixa")
			.leftOuterJoin("oportunidade.listaOportunidadeproposta listaOportunidadeproposta")
			.leftOuterJoin("listaOportunidadeproposta.arquivo arquivoproposta")
			.orderBy("listoportunidadehistorico.dtaltera DESC, listaOportunidadematerial.cdoportunidadematerial");
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		
		Oportunidade oportunidade = (Oportunidade) save.getEntity();
		
		if(oportunidade != null && oportunidade.getListaOportunidadeproposta() != null){
			saveArquivos(oportunidade.getListaOportunidadeproposta());
		}
		
		save.saveOrUpdateManaged("listaOportunidadeproposta");
		save.saveOrUpdateManaged("listaOportunidadeitem");
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				
				save.saveOrUpdateManaged("listaOportunidadematerial");
				return null;
			}
		});
	}
	
	public void saveArquivos(List<Oportunidadeproposta> listaOportunidadeproposta){
		if(listaOportunidadeproposta == null){
			throw new SinedException("O par�metro oportunidade n�o pode ser null");
		}
		if(listaOportunidadeproposta != null && listaOportunidadeproposta.size() > 0){
			for (Oportunidadeproposta oportunidadeproposta : listaOportunidadeproposta) {
				if(oportunidadeproposta.getArquivo() != null)
				arquivoDAO.saveFile(oportunidadeproposta, "arquivo");
			}
		}
	}

	public List<Oportunidade> findByContacrm(Contacrm contacrm){
		if (contacrm == null || contacrm.getCdcontacrm() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		QueryBuilder<Oportunidade> query = query();
		this.updateEntradaQuery(query);
		return query			
				.where("contacrm = ?",contacrm)
				.orderBy("contacrm.cdcontacrm, listoportunidadehistorico.dtaltera desc")
				.list();
	}
	
	public List<Oportunidade> findByContacrmForCombo(Contacrm contacrm){
		if (contacrm == null || contacrm.getCdcontacrm() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("oportunidade.cdoportunidade, oportunidade.nome")
					.where("oportunidade.contacrm = ?",contacrm)
					.orderBy("oportunidade.nome")
					.list();
	}
	
	public List<Oportunidade> findForReport(OportunidadeFiltro filtro){
		QueryBuilder<Oportunidade> query = querySined();
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		
		this.updateListagemQuery(query, filtro);
		query.setIgnoreJoinPaths(new HashSet<String>());
		
		query
			.select("oportunidade.cdoportunidade, oportunidade.nome, contacrm.cdcontacrm, contato.nome, " +
					"contacrm.nome, listcontacrmcontato.cdcontacrmcontato, listcontacrmcontato.nome, " +
					"responsavel.cdpessoa, responsavel.nome, oportunidade.dtInicio, oportunidadesituacao.cdoportunidadesituacao, oportunidadesituacao.nome, " +
					"oportunidade.probabilidade, listcontacrmcontatoemail.email, listcontacrmcontatoemail.cdcontacrmcontatoemail, " +
					"listcontacrmcontatofone.cdcontacrmcontatofone, listcontacrmcontatofone.telefone, " +
					"listaEndereco.cdendereco, listaEndereco.logradouro, listaEndereco.bairro, listaEndereco.cep, " +
					"listaEndereco.caixapostal, listaEndereco.numero, listaEndereco.complemento, listaEndereco.pontoreferencia, " +
					"enderecotipo.cdenderecotipo, enderecotipo.nome, " + 
					"listamunicipio.cdmunicipio, listamunicipio.nome, " +
					"listauf.cduf, listauf.nome, listauf.sigla, " +
					"listaTelefone.cdtelefone, listaTelefone.telefone, " +
					"telefonetipo.cdtelefonetipo, telefonetipo.nome ")
			.leftOuterJoin("contacrm.listcontacrmcontato listcontacrmcontato")
			.leftOuterJoin("listcontacrmcontato.listcontacrmcontatofone listcontacrmcontatofone")						
			.leftOuterJoin("listcontacrmcontato.listcontacrmcontatoemail listcontacrmcontatoemail")
			.leftOuterJoin("cliente.listaContato pessoaContato")
			.leftOuterJoin("pessoaContato.contato contato")
			.leftOuterJoin("cliente.listaTelefone listaTelefone")
			.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
			.leftOuterJoin("cliente.listaEndereco listaEndereco")
			.leftOuterJoin("listaEndereco.enderecotipo enderecotipo")
			.leftOuterJoin("listaEndereco.municipio listamunicipio")
			.leftOuterJoin("listamunicipio.uf listauf");
			
			if(SinedUtil.isRestricaoClienteVendedor(usuarioLogado)){
				query.where("responsavel.cdpessoa = ?", usuarioLogado.getCdpessoa());
			}
		
		return query.orderBy("oportunidade.nome").list();
	}

	public List<Oportunidade> populaComboSituacao(Oportunidade oportunidade){
		return query()
		.select("oportunidade.cdoportunidade, oportunidadesituacao.cdoportunidadesituacao, oportunidadesituacao.nome ")
		.join("oportunidade.oportunidadesituacao oportunidadesituacao")
		.where("oportunidadesituacao.cdoportunidadesituacao > ?", oportunidade.getOportunidadesituacao().getCdoportunidadesituacao())
		.orderBy("oportunidadesituacao.cdoportunidadesituacao")
		.list();
		
	}

	/**
	 * Atualiza a data retorno da oportunidade para a data atual.
	 *
	 * @param oportunidade
	 * @author Rodrigo Freitas
	 */
	public void atualizaDataRetorno(Oportunidade oportunidade) {
		if(oportunidade == null || oportunidade.getCdoportunidade() == null){
			throw new SinedException("Oportunidade n�o pode ser nula.");
		}
		getHibernateTemplate().bulkUpdate("update Oportunidade o set o.dtretorno = ? where o.id = ?", 
				new Object[]{new Date(System.currentTimeMillis()), oportunidade.getCdoportunidade()});
	}
	/**
	 * Atualiza a data de retorno para a data selecionada.
	 * @param oportunidade
	 */
	public void atualizaDataRetornoOportunidade(Oportunidade oportunidade){
		if(oportunidade == null || oportunidade.getCdoportunidade() == null){
			throw new SinedException("Oportunidade n�o pode ser nula.");
		}
		if(oportunidade.getDtretorno() != null){
		getHibernateTemplate().bulkUpdate("update Oportunidade o set o.dtretorno = ? where o.id = ?", 
				new Object[]{oportunidade.getDtretorno(), oportunidade.getCdoportunidade()});
		}
	}

	/**
	 * Carrega informa��es da oportunidade para cria��o do agendamento.
	 *
	 * @param oportunidade
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Oportunidade loadForAgendamento(Oportunidade oportunidade) {
		if(oportunidade == null || oportunidade.getCdoportunidade() == null){
			throw new SinedException("Oportunidade n�o pode ser nula.");
		}
		return query()
					.select("oportunidade.cdoportunidade, oportunidade.nome, responsavel.cdpessoa, " +
							"responsavel.email, oportunidade.dtInicio, oportunidade.tiporesponsavel, " +
							"oportunidade.dtPrevisaotermino, oportunidade.dtretorno")
					.leftOuterJoin("oportunidade.responsavel responsavel")
//					.leftOuterJoin("oportunidade.projeto projeto")
					.where("oportunidade = ?", oportunidade)
					.unique();
	}
	
	/**
	 * Carrega informa��es da oportunidade para cria��o do contrato.
	 *
	 * @param oportunidade
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Oportunidade loadForContrato(Oportunidade oportunidade) {
		if(oportunidade == null || oportunidade.getCdoportunidade() == null){
			throw new SinedException("Oportunidade n�o pode ser nula.");
		}
		return query()
				.select("oportunidade.cdoportunidade, oportunidade.nome, oportunidadesituacao.nome, oportunidade.tiporesponsavel, " +
						"contacrm.tipo, contacrm.cpf, contacrm.cnpj, oportunidadesituacao.cdoportunidadesituacao, " +
						"oportunidadesituacao.situacaoantesfinal, oportunidadesituacao.situacaofinal, cliente.cdpessoa, " +
						"cliente.nome, cliente.cnpj, cliente.cpf, empresa.cdpessoa, empresa.nome, empresa.razaosocial, "+
						"listaOportunidadematerial.cdoportunidadematerial, listaOportunidadematerial.valorunitario, listaOportunidadematerial.quantidade, "+
						"material.cdmaterial, material.nome, responsavel.cdpessoa, responsavel.nome, responsavel.cpf, responsavel.cnpj ")
				.leftOuterJoin("oportunidade.oportunidadesituacao oportunidadesituacao")
				.leftOuterJoin("oportunidade.responsavel responsavel")
//				.leftOuterJoin("oportunidade.projeto projeto")
				.leftOuterJoin("oportunidade.contacrm contacrm")
				.leftOuterJoin("oportunidade.cliente cliente")
				.leftOuterJoin("oportunidade.empresa empresa")
				.leftOuterJoin("oportunidade.listaOportunidadematerial listaOportunidadematerial")
				.leftOuterJoin("listaOportunidadematerial.material material")
				.where("oportunidade = ?", oportunidade)
				.unique();	
	}
	
	/**
	 * Carrega informa��es da oportunidade para cria��o do cliente.
	 *
	 * @param oportunidade
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Oportunidade loadForCliente(Oportunidade oportunidade) {
		if(oportunidade == null || oportunidade.getCdoportunidade() == null){
			throw new SinedException("Oportunidade n�o pode ser nula.");
		}
		return query()
		.select("oportunidade.cdoportunidade, oportunidade.nome, oportunidade.tiporesponsavel, oportunidade.tipopessoacrm,contacrm.nome, " +
					"contacrm.tipo, contacrm.cpf, contacrm.cnpj, contacrm.cdcontacrm, " +
					"oportunidadesituacao.cdoportunidadesituacao, crmcontato.cdcontacrmcontato, crmcontato.nome, crmcontato.numero, " +
					"crmcontato.complemento, crmcontato.bairro, crmcontato.cep,lead.cdlead, municipiocontato.cdmunicipio, ufcontato.cduf, " +
					"oportunidadesituacao.nome, oportunidadesituacao.situacaoantesfinal, oportunidadesituacao.situacaofinal, " +
					"responsavel.cdpessoa, responsavel.nome, responsavel.cpf, responsavel.cnpj, segmento.cdsegmento ")
		.leftOuterJoin("oportunidade.oportunidadesituacao oportunidadesituacao")
		.leftOuterJoin("oportunidade.responsavel responsavel")
//		.leftOuterJoin("oportunidade.projeto projeto")
		.leftOuterJoin("oportunidade.contacrm contacrm")
		.leftOuterJoin("contacrm.lead lead")
		.leftOuterJoin("contacrm.listcontacrmsegmento listcontacrmsegmento")
		.leftOuterJoin("listcontacrmsegmento.segmento segmento")
		.leftOuterJoin("contacrm.listcontacrmcontato crmcontato")
		.leftOuterJoin("crmcontato.municipio municipiocontato")
		.leftOuterJoin("municipiocontato.uf ufcontato")
		.where("oportunidade = ?", oportunidade)
		.unique();
	}

	public void updateSituacao(Oportunidade oportunidade, Oportunidadesituacao situacao) {
		getHibernateTemplate().bulkUpdate("update Oportunidade o set o.oportunidadesituacao = ? where o.id = ?", 
				new Object[]{situacao,oportunidade.getCdoportunidade()});		
	}
	
	/**
	* M�todo que atualiza o valor total da oportunidade
	*
	* @param oportunidade
	* @param valortotal
	* @since 10/05/2017
	* @author Luiz Fernando
	*/
	public void updateValortotal(Oportunidade oportunidade, Money valortotal) {
		getHibernateTemplate().bulkUpdate("update Oportunidade o set o.valortotal = ? where o.id = ?", new Object[]{valortotal, oportunidade.getCdoportunidade()});		
	}
	
	/**
	 * Carrega dados para tela de Intera��o de vendas a partir de uma Oportunidade.
	 * @param oportunidade
	 * @return
	 * @author Taidson
	 * @since 03/09/2010
	 */
	public Oportunidade loadForInteracao(Oportunidade oportunidade) {
		if(oportunidade == null || oportunidade.getCdoportunidade() == null){
			throw new SinedException("Oportunidade n�o pode ser nula.");
		}
		return query()
		.select("oportunidade.cdoportunidade, oportunidade.nome, contacrm.nome, " +
					"contacrm.cdcontacrm, contacrmcontato.cdcontacrmcontato, contacrmcontato.nome")
		.leftOuterJoin("oportunidade.oportunidadesituacao oportunidadesituacao")
		.leftOuterJoin("oportunidade.contacrm contacrm")
		.leftOuterJoin("oportunidade.contacrmcontato contacrmcontato")
		.where("oportunidade = ?", oportunidade)
		.unique();
	}
	
	/**
	 * Verifica se a oportunidade est� na situa��o cancelada.
	 * @param whereIn
	 * @return
	 * @author Taidson
	 * @since 04/09/2010
	 */
	public boolean haveSituacaoCancelada(String whereIn){
		return newQueryBuilder(Long.class)
		.from(Oportunidade.class)
		.setUseTranslator(false)
		.select("count(*)")
		.leftOuterJoin("oportunidade.oportunidadesituacao oportunidadesituacao")
		.where("oportunidadesituacao = ?", Oportunidadesituacao.CANCELADA)
		.whereIn("oportunidade.cdoportunidade", whereIn)
		.unique() > 0;
	}
	
	/**
	 * Realiza busca geral em Oportunidade conforme par�metro da busca.
	 * @param busca
	 * @return
	 * @author Taidson
	 * @since 04/09/2010
	 */
	public List<Oportunidade> findOportunidadeForBuscaGeral(String busca){
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		
		QueryBuilder<Oportunidade> query = query()
		.select("oportunidade.cdoportunidade, oportunidade.nome, contacrm.nome")
		.leftOuterJoin("oportunidade.contacrm contacrm")
		.leftOuterJoin("contacrm.listcontacrmcontato listcontacrmcontato")
		.leftOuterJoin("oportunidade.responsavel responsavel")
		.leftOuterJoin("listcontacrmcontato.listcontacrmcontatoemail listcontacrmcontatoemail");
		
		if(SinedUtil.isRestricaoClienteVendedor(usuarioLogado)){
			query.where("responsavel.cdpessoa = ?", usuarioLogado.getCdpessoa());
		}
		
		return query.openParentheses()
		.whereLikeIgnoreAll("oportunidade.nome", busca)
		.or()
		.whereLikeIgnoreAll("contacrm.nome", busca)
		.or()
		.whereLikeIgnoreAll("listcontacrmcontatoemail.email", busca)
		.closeParentheses()
		.orderBy("oportunidade.nome")
		.list();
		
	}

	public List<Oportunidade> findForAgenda(Date data) {
		return query()
					.select("oportunidade.cdoportunidade, oportunidade.dtretorno, oportunidade.nome, contacrm.nome")
					.join("oportunidade.contacrm contacrm")
					.where("oportunidade.dtretorno = ?", data)
					.where("oportunidade.responsavel = ?", SinedUtil.getUsuarioComoColaborador())
					.list();
	}

	/**
	* M�todo para buscar as oportunidades que tenham situa��o diferente de cancelada e final, para atualizar o ciclo
	*
	* @return
	* @since Sep 27, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Oportunidade> findForAtualizarcicloByOportunidade() {
		return querySined()
					.setUseWhereEmpresa(false)
					.select("oportunidade.cdoportunidade, oportunidade.dtInicio, oportunidade.dtPrevisaotermino")
					.leftOuterJoin("oportunidade.oportunidadesituacao oportunidadesituacao")
					.openParentheses().where("oportunidadesituacao.situacaofinal is null").or()
					.where("oportunidadesituacao.situacaofinal = ?", Boolean.FALSE).closeParentheses()
					.openParentheses().where("oportunidadesituacao.situacaocancelada is null").or()
					.where("oportunidadesituacao.situacaocancelada = ?", Boolean.FALSE).closeParentheses()					
					.list();
					
	}
	
	/**
	* M�todo que atualiza o ciclo da oportunidade
	*
	* @param oportunidade
	* @param ciclo
	* @since Sep 27, 2011
	* @author Luiz Fernando F Silva
	*/
	public void updateCicloTodasOportunidade(Oportunidade oportunidade, Integer ciclo){
		if(oportunidade == null || oportunidade.getCdoportunidade() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		getHibernateTemplate().bulkUpdate("update Oportunidade o set o.ciclo = ? where o.cdoportunidade = ?",
				new Object[]{ciclo, oportunidade.getCdoportunidade()});
	}
	
	/**
	* M�todo para buscar os Cd's da oportunidade da listagem
	*
	* @param filtro
	* @return
	* @since Nov 7, 2011
	* @author Luiz Fernando F Silva
	*/
	@SuppressWarnings("unchecked")
	public String whereInCdoportunidadeListagem(OportunidadeFiltro filtro){		
		
		List<Integer> lista = new ArrayList<Integer>();
		StringBuilder sql = new StringBuilder();
		StringBuilder where = new StringBuilder();
		Boolean clausulaWhere = Boolean.FALSE;
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy"); 
		
		sql.append("SELECT DISTINCT o.cdoportunidade as cdoportunidade ")
			.append("FROM oportunidade o ");
		
		if(filtro.getMaterial() != null)
			sql.append("LEFT OUTER JOIN material m on m.cdmaterial = o.cdmaterial ");
		if(filtro.getContacrm() != null || filtro.getUf() != null || filtro.getMunicipio() != null){
			sql
				.append("LEFT OUTER JOIN contacrm ccrm on ccrm.cdcontacrm = o.cdcontacrm ")
				.append("LEFT OUTER JOIN colaborador presponsavel on presponsavel.cdpessoa = ccrm.cdresponsavel ");
		}
		if(filtro.getUf() != null || filtro.getMunicipio() != null){
			sql
				.append("LEFT OUTER JOIN endereco e on e.cdendereco = presponsavel.cdendereco ")			
				.append("LEFT OUTER JOIN municipio muni on muni.cdmunicipio = e.cdmunicipio ")
				.append("LEFT OUTER JOIN uf uf on uf.cduf = muni.cduf ");
		}
			
		sql.append("LEFT OUTER JOIN oportunidadesituacao op on op.cdoportunidadesituacao = o.cdoportunidadesituacao ");
		if(filtro.getCampanha() != null)
			sql.append("LEFT OUTER JOIN campanha c on c.cdcampanha = o.cdcampanha ");
//		if(filtro.getProjeto() != null)
//			sql.append("LEFT OUTER JOIN projeto p on p.cdprojeto = o.cdprojeto ");
		if(filtro.getColaborador() != null || filtro.getAgencia() != null)
			sql.append("LEFT OUTER JOIN pessoa respoportunidade on respoportunidade.cdpessoa = o.cdresponsavel ");
		if(filtro.getOportunidadefonte() != null)
			sql.append("LEFT OUTER JOIN oportunidadefonte of on of.cdoportunidadefonte = o.cdoportunidadefonte ");			
			
		
		if(filtro.getNome() != null && !filtro.getNome().equals("")){
			if (filtro.getNome().indexOf('?') > 0) {
				throw new IllegalArgumentException("N�o � permitido pesquisar o nome com o caracter '?'.");
			}
			String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
			if (funcaoTiraacento != null) {
				where.append((" UPPER(" + funcaoTiraacento + "(o.nome)) LIKE '%" + Util.strings.tiraAcento(filtro.getNome()).toUpperCase().replaceAll("'", "\\\\'") + "%'"));
			}
			else {
				where.append((" UPPER(o.nome) LIKE '%" + Util.strings.tiraAcento(filtro.getNome()).toUpperCase().replaceAll("'", "\\\\'") + "%'"));
			}
			
			clausulaWhere = Boolean.TRUE;			
		}
		if( filtro.getContacrm() != null && filtro.getContacrm().getCdcontacrm() != null){
			if(clausulaWhere)
				where.append(" and ");
			where.append(" ccrm.cdcontacrm = ").append(filtro.getContacrm().getCdcontacrm());
			clausulaWhere = Boolean.TRUE;
		}
		if(filtro.getContacrmcontato() != null && filtro.getContacrmcontato().getCdcontacrmcontato() != null){
			if(clausulaWhere)
				where.append(" and ");
			where
				.append(" ccrm.cdcontacrm in ( SELECT cc.cdcontacrm ")
				.append("FROM contacrm cc ")
				.append("LEFT OUTER JOIN contacrmcontato ccc on ccc.cdcontacrm = cc.cdcontacrm ")
				.append("where  ccc.cdcontacrmcontato = ").append(filtro.getContacrmcontato().getCdcontacrmcontato()).append(") ");
			clausulaWhere = Boolean.TRUE;
		}
		if(filtro.getCampanha() != null && filtro.getCampanha().getCdcampanha() != null){
			if(clausulaWhere)
				where.append(" and ");
			where.append(" c.cdcampanha = ").append(filtro.getCampanha().getCdcampanha());
			clausulaWhere = Boolean.TRUE;
		}
		if(filtro.getProbabilidadeinicio() != null ){
			if(clausulaWhere)
				where.append(" and ");
			where.append(" o.probabilidade >= ").append(filtro.getProbabilidadeinicio());
			clausulaWhere = Boolean.TRUE;
		}
		if(filtro.getProbabilidadefim() != null ){
			if(clausulaWhere)
				where.append(" and ");
			where.append(" o.probabilidade <= ").append(filtro.getProbabilidadefim());
			clausulaWhere = Boolean.TRUE;
		}		
		if(filtro.getDtretornoinicio() != null){
			if(clausulaWhere)
				where.append(" and ");
			where.append(" o.dtretorno >= to_date('").append(format.format(filtro.getDtretornoinicio())).append("', 'DD/MM/YYYY') ");
			clausulaWhere = Boolean.TRUE;
		}
		if(filtro.getDtretornofim() != null){
			if(clausulaWhere)
				where.append(" and ");
			where.append(" o.dtretorno <= to_date('").append(format.format(filtro.getDtretornofim())).append("', 'DD/MM/YYYY') ");
			clausulaWhere = Boolean.TRUE;
		}
		if(filtro.getDtInicioinicio() != null){
			if(clausulaWhere)
				where.append(" and ");
			where.append(" o.dtinicio >= to_date('").append(format.format(filtro.getDtInicioinicio())).append("', 'DD/MM/YYYY') ");
			clausulaWhere = Boolean.TRUE;
		}
		if(filtro.getDtIniciofim() != null){
			if(clausulaWhere)
				where.append(" and ");
			where.append(" o.dtinicio <= to_date('").append(format.format(filtro.getDtIniciofim())).append("', 'DD/MM/YYYY') ");
			clausulaWhere = Boolean.TRUE;
		}
		if(filtro.getOportunidadefonte() != null && filtro.getOportunidadefonte().getCdoportunidadefonte() != null){
			if(clausulaWhere)
				where.append(" and ");
			where.append(" of.cdoportunidadefonte = ").append(filtro.getOportunidadefonte().getCdoportunidadefonte());
			clausulaWhere = Boolean.TRUE;
		}
		
		/*if(filtro.getProjeto() != null && filtro.getProjeto().getCdprojeto() != null){
			if(clausulaWhere)
				where.append(" and ");			
			where.append(" p.cdprojeto = ").append(filtro.getProjeto().getCdprojeto());
			clausulaWhere = Boolean.TRUE;
		}else*/ 
		if(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null){
			if(clausulaWhere)
				where.append(" and ");			
			where.append(" m.cdmaterial = ").append(filtro.getMaterial().getCdmaterial());
			clausulaWhere = Boolean.TRUE;
		}
		
		if(filtro.getColaborador() != null && filtro.getColaborador().getCdpessoa() != null){
			if(clausulaWhere)
				where.append(" and ");
			where.append("( respoportunidade.cdpessoa = ").append(filtro.getColaborador().getCdpessoa());
			clausulaWhere = Boolean.TRUE;
			if(filtro.getAgencia() != null && filtro.getAgencia().getCdpessoa() != null)
				where.append(" or respoportunidade.cdpessoa = ").append(filtro.getAgencia().getCdpessoa());
			where.append(") ");
			
			if(filtro.getUf() != null && filtro.getUf().getCduf() != null){
				if(clausulaWhere)
					where.append(" and ");
				where
					.append("( presponsavel.cdpessoa = ").append(filtro.getColaborador().getCdpessoa())
					.append(" and uf.cduf = ").append(filtro.getUf().getCduf());
				clausulaWhere = Boolean.TRUE;
				if(filtro.getMunicipio() != null && filtro.getMunicipio().getCdmunicipio() != null)
					where.append(" and muni.cdmunicipio = ").append(filtro.getMunicipio().getCdmunicipio());
				where.append(") ");
			}				
			
		}else if(filtro.getAgencia() != null && filtro.getAgencia().getCdpessoa() != null){
			if(clausulaWhere)
				where.append(" and ");
			where.append(" respoportunidade.cdpessoa = ").append(filtro.getAgencia().getCdpessoa());
			clausulaWhere = Boolean.TRUE;
		}
		
		if(filtro.getColaborador() == null && filtro.getUf() != null){				
			if(filtro.getUf() != null && filtro.getUf().getCduf() != null){
				if(clausulaWhere)
					where.append(" and ");
				clausulaWhere = Boolean.TRUE;
				where.append(" uf.cduf = ").append(filtro.getUf().getCduf());
			}
		}
		if(filtro.getColaborador() == null && filtro.getMunicipio() != null && filtro.getMunicipio().getCdmunicipio() != null){			
			if(clausulaWhere)
				where.append(" and ");
			clausulaWhere = Boolean.TRUE;
			where
				.append(" muni.cdmunicipio = ").append(filtro.getMunicipio().getCdmunicipio());
			
		}
		
		List<Oportunidadesituacao> listaOportunidadesituacao = filtro.getListaOportunidadesituacao();
		if (listaOportunidadesituacao != null && !listaOportunidadesituacao.isEmpty()){
			if(clausulaWhere)
				where.append(" and ");
			where.append(" o.cdoportunidadesituacao in (").append(CollectionsUtil.listAndConcatenate(listaOportunidadesituacao,"cdoportunidadesituacao", ",")).append(") ");
			clausulaWhere = Boolean.TRUE;
		}
		
		if(clausulaWhere)
			sql.append(" WHERE ").append(where.toString()).append(";");
		
//		System.out.println("QUERY: " + sql.toString());		
		lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Integer(rs.getInt("cdoportunidade"));
			}
		});
		
		if(lista != null && !lista.isEmpty()){
			StringBuilder s = new StringBuilder();
			for(Integer cdoportunidade : lista){
				if(!s.toString().equals(""))
					s.append(",").append(cdoportunidade);
				else
					s.append(cdoportunidade);
			}
			
			return s.toString();
				
		}
		
		return "";
	}
	
	/**
	* M�todo para calcular os totais por Material/Projeto da listagem de acordo com o filtro
	*
	* @param filtro
	* @param whereIn
	* @return
	* @since Nov 7, 2011
	* @author Luiz Fernando F Silva
	*/
	@SuppressWarnings("unchecked")
	public List<TotalizadorOportunidade> calculaTotaisMaterialProjetoListagemOportunidade(OportunidadeFiltro filtro, String whereIn){
		
		StringBuilder sql = new StringBuilder();
		List<TotalizadorOportunidade> lista = new ArrayList<TotalizadorOportunidade>();
		List<Oportunidadesituacao> listaOportunidadesituacao = filtro.getListaOportunidadesituacao();
		sql
			.append("SELECT (coalesce((m.nome),'')) as nomematerialprojeto, sum(COALESCE(om.valorunitario,0) * COALESCE(om.quantidade,0)*100) as valor, sum(om.quantidade) as qtde ")
			.append("FROM oportunidade o ")
//			.append("LEFT OUTER JOIN projeto p on p.cdprojeto = o.cdprojeto ")
			.append("LEFT OUTER JOIN oportunidadematerial om on om.cdoportunidade = o.cdoportunidade ")
			.append("LEFT OUTER JOIN material m on m.cdmaterial = om.cdmaterial ")
			.append("WHERE (o.cdmaterial is not null) ")
			.append("and o.cdoportunidade in (").append(whereIn).append(") ");
		
		if(listaOportunidadesituacao != null && !listaOportunidadesituacao.isEmpty())
			sql.append(" and o.cdoportunidadesituacao in (").append(CollectionsUtil.listAndConcatenate(listaOportunidadesituacao,"cdoportunidadesituacao", ",")).append(") ");
		
		sql.append("GROUP BY m.nome; ");
		
//		System.out.println("QUERY: " + sql.toString());		
		lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new TotalizadorOportunidade(rs.getString("nomematerialprojeto"), rs.getDouble("valor"), rs.getInt("qtde"));
			}
		});
		
		return lista;
		
	}
	
	/**
	* M�todo para calcular os totais por Situa��o da listagem de acordo com o filtro
	*
	* @param filtro
	* @param whereIn
	* @return
	* @since Nov 7, 2011
	* @author Luiz Fernando F Silva
	*/
	@SuppressWarnings("unchecked")
	public List<TotalizadorOportunidade> calculaTotaisOportunidadesituacaoListagemOportunidade(OportunidadeFiltro filtro, String whereIn){
		
		StringBuilder sql = new StringBuilder();
		List<TotalizadorOportunidade> lista = new ArrayList<TotalizadorOportunidade>();
		List<Oportunidadesituacao> listaOportunidadesituacao = filtro.getListaOportunidadesituacao();
		
		sql
			.append("SELECT os.nome as nomeoportunidadesituacao, sum(o.valortotal) as valor , count(o.cdoportunidade) as qtde ")
			.append("FROM oportunidade o ")
			.append("LEFT OUTER JOIN oportunidadesituacao os on os.cdoportunidadesituacao = o.cdoportunidadesituacao ")
			.append("WHERE o.cdoportunidadesituacao is not null and ")
			.append("o.cdoportunidade in (").append(whereIn).append(") ");
		
		if(listaOportunidadesituacao != null && !listaOportunidadesituacao.isEmpty())
			sql.append("and o.cdoportunidadesituacao in (").append(CollectionsUtil.listAndConcatenate(listaOportunidadesituacao,"cdoportunidadesituacao", ",")).append(") ");
		
		sql.append("GROUP BY nomeoportunidadesituacao; ");
		
//		System.out.println("QUERY: " + sql.toString());		
		lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new TotalizadorOportunidade(rs.getString("nomeoportunidadesituacao"), rs.getDouble("valor"), rs.getInt("qtde"));
			}
		});
		
		return lista;
	}
	
	/**
	 * 
	 * M�todo que carrega a oportunidade para a tela de contacrm
	 *
	 *@author Thiago Augusto
	 *@date 13/01/2012
	 * @param bean
	 * @return
	 */
	public Oportunidade carregarOportunidade(Oportunidade bean){
		return query()
			.select("oportunidade.cdoportunidade, oportunidade.nome, oportunidade.dtInicio, oportunidadesituacao.nome, responsavel.nome")
			.leftOuterJoin("oportunidade.oportunidadesituacao oportunidadesituacao")
			.leftOuterJoin("oportunidade.responsavel responsavel")
			.where("oportunidade = ?", bean)
			.unique();
	}
	
	/**
	 * 
	 * M�todo para carregar a oportunidade completa
	 *
	 *@author Thiago Augusto
	 *@date 16/01/2012
	 * @param bean
	 * @return
	 */
	public Oportunidade carregarOportunidadeCompleta(Oportunidade bean){
		if(bean == null || bean.getCdoportunidade() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return querySined()
		
		.select("oportunidade.cdoportunidade, oportunidade.nome, oportunidade.tiporesponsavel, oportunidade.valortotal, " +
				"oportunidade.ciclo, oportunidade.dtInicio, oportunidade.dtPrevisaotermino, " +
				"oportunidade.probabilidade, oportunidade.dtretorno, oportunidade.proximopasso, " +
				"contacrm.cdcontacrm, contacrm.nome, " +
				"contacrmcontato.cdcontacrmcontato, contacrmcontato.nome, " +
				"responsavel.cdpessoa, responsavel.nome, " +
				"oportunidadesituacao.cdoportunidadesituacao, oportunidadesituacao.nome, " +
				"imagem.cdarquivo, imagem.nome, " +
				"listoportunidadehistorico.cdoportunidadehistorico, listoportunidadehistorico.observacao, " +
				"listoportunidadehistorico.dtaltera, listoportunidadehistorico.cdusuarioaltera, " +
				"oportunidadesituacao2.cdoportunidadesituacao, " +
				"arquivo.cdarquivo, arquivo.nome, " +
				"oportunidadefonte.cdoportunidadefonte, oportunidadefonte.nome, " +
//				"projeto.cdprojeto, projeto.nome, " +
				"campanha.cdcampanha, campanha.nome, " +
				"oportunidadesituacao2.cdoportunidadesituacao, oportunidadesituacao2.nome, " +
				"listaOportunidadematerial.cdoportunidadematerial, listaOportunidadematerial.quantidade, listaOportunidadematerial.valorunitario, " +
				"material.cdmaterial, material.nome, unidademedida.cdunidademedida, unidademedida.nome ")
		.leftOuterJoin("oportunidade.contacrm contacrm")
		.leftOuterJoin("oportunidade.contacrmcontato contacrmcontato")
		.leftOuterJoin("oportunidade.responsavel responsavel")
		.leftOuterJoin("oportunidade.oportunidadesituacao oportunidadesituacao")
		.leftOuterJoin("oportunidadesituacao.imagem imagem")
		.leftOuterJoin("oportunidade.listoportunidadehistorico listoportunidadehistorico")
		.leftOuterJoin("listoportunidadehistorico.oportunidadesituacao oportunidadesituacao2")
		.leftOuterJoin("listoportunidadehistorico.arquivo arquivo")
		.leftOuterJoin("oportunidade.oportunidadefonte oportunidadefonte")	
//		.leftOuterJoin("oportunidade.projeto projeto")
		.leftOuterJoin("oportunidade.listaOportunidadematerial listaOportunidadematerial")
		.leftOuterJoin("listaOportunidadematerial.material material")
		.leftOuterJoin("listaOportunidadematerial.unidademedida unidademedida")
		.leftOuterJoin("oportunidade.campanha campanha")
		.where("oportunidade = ?", bean)
		.orderBy("listoportunidadehistorico.dtaltera DESC").
		unique();
	}
	
	public Oportunidade findForEmissaoOportunidadeRTF(Oportunidade oportunidade) {
		if (oportunidade == null || oportunidade.getCdoportunidade() == null)
			throw new SinedException("Par�metros inv�lidos.");
				
		return query()
				.leftOuterJoinFetch("oportunidade.cliente cliente")
				.leftOuterJoinFetch("cliente.listaTelefone listaTelefoneCliente")
				.leftOuterJoinFetch("cliente.listaEndereco listaEndereco")
				.leftOuterJoinFetch("listaEndereco.municipio")
				.leftOuterJoinFetch("oportunidade.contato")
				.leftOuterJoinFetch("oportunidade.frequencia")
				.leftOuterJoinFetch("oportunidade.contacrm")
				.leftOuterJoinFetch("oportunidade.contacrmcontato")
//				.leftOuterJoinFetch("oportunidade.responsavel responsavel")
//				.leftOuterJoinFetch("responsavel.listaTelefone listaTelefone")
				.leftOuterJoinFetch("oportunidade.listoportunidadehistorico")
				.leftOuterJoinFetch("oportunidade.listaOportunidadeitem listaOportunidadeitem")
				.leftOuterJoinFetch("listaOportunidadeitem.propostacaixaitem propostacaixaitem")
				.where("oportunidade.id = ?", oportunidade.getCdoportunidade())
				.unique();
	}

	/**
	 * Retorna a quantidade de Oportunidades com data de retorno atrasadas
	 *
	 * @return
	 * @since 24/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdeRetornoAtrasado() {
		return newQueryBuilderSined(Long.class)
					
					.from(Oportunidade.class)
					.setUseTranslator(false) 
					.select("count(*)")
					.join("oportunidade.oportunidadesituacao oportunidadesituacao")
					.where("oportunidade.dtretorno < ?", SinedDateUtils.currentDateToBeginOfDay())
					.openParentheses()
					.where("oportunidadesituacao.situacaofinal = ?", Boolean.FALSE)
					.or()
					.where("oportunidadesituacao.situacaofinal is null")
					.closeParentheses()
					.unique()
					.intValue();
	}
	
	/**
	 * Retorna a quantidade de Oportunidades com data de retorno atrasadas
	 *
	 * @return
	 * @since 24/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdeRetornoHoje() {
		return newQueryBuilderSined(Long.class)
		
		.from(Oportunidade.class)
		.setUseTranslator(false) 
		.select("count(*)")
		.join("oportunidade.oportunidadesituacao oportunidadesituacao")
		.where("oportunidade.dtretorno = ?", SinedDateUtils.currentDateToBeginOfDay())
		.openParentheses()
		.where("oportunidadesituacao.situacaofinal = ?", Boolean.FALSE)
		.or()
		.where("oportunidadesituacao.situacaofinal is null")
		.closeParentheses()
		.unique()
		.intValue();
	}

	/**
	 * Carrega a oportunidade com a caixa de proposta preenchida.
	 *
	 * @param oportunidade
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/09/2013
	 */
	public Oportunidade loadWithPropostacaixa(Oportunidade oportunidade) {
		return query()
					.select("oportunidade.cdoportunidade, propostacaixa.cdpropostacaixa, arquivo.cdarquivo")
					.leftOuterJoin("oportunidade.propostacaixa propostacaixa")
					.leftOuterJoin("propostacaixa.arquivo arquivo")
					.entity(oportunidade)
					.unique();
	}

	/**
	 * M�todo que veririca se existe cliente diferente nas oportundiades
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 15/10/2013
	 */
	public Boolean isClienteDiferenteOportunidade(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		List<Integer> lista = newQueryBuilderSined(Integer.class)
		.from(Oportunidade.class)
		.setUseTranslator(false) 
		.select("distinct cliente.cdpessoa")
		.join("oportunidade.cliente cliente")
		.whereIn("oportunidade.cdoportunidade", whereIn)
		.list();
		
		if(lista != null && lista.size() > 1){
			return true;
		}
		return false;
	}

	/**
	 * M�todo que carrega as oportunidades para cria��o de venda
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 15/10/2013
	 */
	public List<Oportunidade> findForVenda(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
		.select("oportunidade.cdoportunidade, oportunidade.nome, oportunidade.valortotal, " +
				"oportunidade.dtInicio, responsavel.cdpessoa, responsavel.nome, cliente.cdpessoa, cliente.nome, " +
				"contato.cdpessoa, contato.nome, contacrm.cdcontacrm, contacrm.nome, " +
				"oportunidadesituacao.cdoportunidadesituacao, oportunidadesituacao.nome, empresa.cdpessoa, " +
				"material.cdmaterial, material.nome, material.identificacao, material.valorcusto, material.valorvenda, material.valorvendaminimo, " +
				"material.valorvendamaximo, listaMaterialunidademedida.cdmaterialunidademedida, listaMaterialunidademedida.fracao, " +
				"listaMaterialunidademedida.qtdereferencia, listaMaterialunidademedida.mostrarconversao, listaOportunidadematerial.observacao, " +
				"unidademedida.nome, unidademedida.cdunidademedida, unidademedidaprincipal.cdunidademedida, unidademedidaprincipal.nome," +
				"listaOportunidadematerial.cdoportunidadematerial, listaOportunidadematerial.valorunitario, listaOportunidadematerial.quantidade," +
				"unidademedidaoportunidade.cdunidademedida, unidademedidaoportunidade.nome ")
		.leftOuterJoin("oportunidade.responsavel responsavel")
		.leftOuterJoin("oportunidade.empresa empresa")
		.leftOuterJoin("oportunidade.cliente cliente")
		.leftOuterJoin("oportunidade.contacrm contacrm")
		.leftOuterJoin("oportunidade.contato contato")
		.leftOuterJoin("oportunidade.oportunidadesituacao oportunidadesituacao")
		.leftOuterJoin("oportunidade.listaOportunidadematerial listaOportunidadematerial")
		.leftOuterJoin("listaOportunidadematerial.material material")
		.leftOuterJoin("listaOportunidadematerial.unidademedida unidademedidaoportunidade")
		.leftOuterJoin("material.listaMaterialunidademedida listaMaterialunidademedida")
		.leftOuterJoin("listaMaterialunidademedida.unidademedida unidademedida")
		.leftOuterJoin("material.unidademedida unidademedidaprincipal")
		.whereIn("oportunidade.cdoportunidade", whereIn)
		.list();
	}

	public List<Oportunidade> findForComboFlex() {
		return query()
					.select("oportunidade.cdoportunidade, oportunidade.nome")
					.orderBy("oportunidade.nome")
					.list();
	}
	
	public List<Oportunidade> findForEmitirfunilvendas(EmitirfunilvendasFiltro filtro){
		return querySined()
					.select("oportunidade.cdoportunidade, oportunidade.probabilidade, oportunidade.valortotal, responsavel.cdpessoa, " +
							"responsavel.nome, cliente.cdpessoa, cliente.nome, contacrm.nome, contacrm.cdcontacrm")
					.leftOuterJoin("oportunidade.responsavel responsavel")
					.leftOuterJoin("oportunidade.cliente cliente")
					.leftOuterJoin("oportunidade.contacrm contacrm")
					.leftOuterJoin("oportunidade.listaOportunidadematerial listaOportunidadematerial")
					.leftOuterJoin("listaOportunidadematerial.material material")
					.where("oportunidade.dtInicio >= ?", filtro.getDtinicio())
					.where("oportunidade.dtInicio <= ?", filtro.getDtfim())
					.where("oportunidade.tiporesponsavel = ?", Tiporesponsavel.COLABORADOR)
					.where("responsavel = ?", filtro.getColaborador())
					.where("material = ?", filtro.getMaterial())
					.where("material.produto = ?", (filtro.getProduto() == true) ? filtro.getProduto() : null)
					.where("material.servico = ?", (filtro.getServico() == true) ? filtro.getServico() : null)
					.list();
	}
	
	/**
	 * M�todo respons�vel por pesquisar oportunidades de acordo com cliente para tela de intera��o.
	 * Nota.: S� s�o retornadas oportunidades que n�o est�o associadas a empresa ou que est�o associadas e o usu�rio possui acesso � empresa
	 * @param cliente
	 * @return
	 * @since 12/07/2016
	 * @author C�sar
	 */
	public List<Oportunidade> findForPainelInteracao (Cliente cliente, Empresa empresa){
		if(cliente == null || cliente.getCdpessoa() == null){
			throw new SinedException("N�o poss�vel localizar Oportunidade sem cliente.");
		}
		
		String whereInEmpresas = empresa != null? empresa.getCdpessoa().toString():
												CollectionsUtil.listAndConcatenate(empresaService.findByUsuario(), "cdpessoa", ",");
		return query()
					.select("oportunidade.cdoportunidade, oportunidade.proximopasso," +
							"situacao.cdoportunidadesituacao, situacao.nome")
					.leftOuterJoin("oportunidade.oportunidadesituacao situacao")
					.leftOuterJoin("oportunidade.empresa empresa")
					.where("oportunidade.cliente = ?", cliente)
					.openParentheses()
						.whereIn("empresa.cdpessoa", whereInEmpresas)
						.or()
						.where("empresa.cdpessoa is null")
					.closeParentheses()
					.list();
	}
	
	public String getWhereInOportunidadeFiltro(OportunidadeFiltro filtro) {
		QueryBuilder<Oportunidade> query = query();
		this.updateListagemQuery(query, filtro);
		query.select("oportunidade.cdoportunidade, oportunidade.ciclo");
		
		List<Oportunidade> lista = query.list();
		return SinedUtil.isListNotEmpty(lista) ? CollectionsUtil.listAndConcatenate(lista, "cdoportunidade", ",") : null;
	}
	
	public List<Oportunidade> findWithResponsavel(String whereIn){
		return query()
					.select("oportunidade.cdoportunidade, oportunidade.nome, responsavel.cdpessoa, responsavel.nome")
					.leftOuterJoin("oportunidade.responsavel responsavel")
					.whereIn("oportunidade.cdoportunidade", whereIn)
					.list();
	}
}
