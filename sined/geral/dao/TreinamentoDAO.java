package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Treinamento;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.TreinamentoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("treinamento.nome")
public class TreinamentoDAO extends GenericDAO<Treinamento> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Treinamento> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inválidos");
		}
		TreinamentoFiltro filtro = (TreinamentoFiltro) _filtro;
		query
			.select("treinamento.cdtreinamento, treinamento.nome, treinamento.ativo")
			.whereLikeIgnoreAll("treinamento.nome", filtro.getNome())
			.where("treinamento.ativo = ?", filtro.getMostrar())
			.orderBy("treinamento.ativo DESC, treinamento.nome");
	}

}
