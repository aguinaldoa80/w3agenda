package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregadocumentofreterateio;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EntregadocumentofreterateioDAO extends GenericDAO<Entregadocumentofreterateio> {
	
	public List<Entregadocumentofreterateio> findByEntregadocumento(Entregadocumento entregadocumento){
		return query()
				.select("entregadocumentofreterateio.cdentregadocumentofreterateio, entregadocumento1.cdentregadocumento, entregadocumento2.cdentregadocumento," +
						"entregadocumentofreterateio.percentual, entregadocumentofreterateio.valor," +
						"entregamaterial.cdentregamaterial, material.cdmaterial, material.identificacao," +
						"material.nome, unidademedida.simbolo, entregamaterial.qtde, entregamaterial.valorunitario")
				.join("entregadocumentofreterateio.entregadocumento entregadocumento1")
				.join("entregadocumentofreterateio.entregamaterial entregamaterial")
				.join("entregamaterial.entregadocumento entregadocumento2")
				.join("entregamaterial.material material")
				.join("material.unidademedida unidademedida")
				.where("entregadocumento1=?", entregadocumento)
				.orderBy("material.nome")
				.list();
	}	
	
	/**
	* M�todo que busca os rateios de frete para calcular o custo das entradas
	*
	* @param whereIn
	* @return
	* @since 30/03/2016
	* @author Luiz Fernando
	*/
	public List<Entregadocumentofreterateio> findForCalcularCusto(String whereIn){
		return query()
				.select("entregadocumentofreterateio.cdentregadocumentofreterateio, entregadocumento1.cdentregadocumento, entregadocumento2.cdentregadocumento," +
						"entregadocumentofreterateio.percentual, entregadocumentofreterateio.valor," +
						"entregamaterial.cdentregamaterial, material.cdmaterial, material.identificacao," +
						"material.nome, unidademedida.simbolo, entregamaterial.qtde, entregamaterial.valorunitario")
				.join("entregadocumentofreterateio.entregadocumento entregadocumento1")
				.join("entregadocumentofreterateio.entregamaterial entregamaterial")
				.join("entregamaterial.entregadocumento entregadocumento2")
				.join("entregamaterial.material material")
				.join("material.unidademedida unidademedida")
				.where("entregamaterial.cdentregamaterial in (select em.cdentregamaterial " +
															 "from Entregadocumentofreterateio edfr " +
															 "join edfr.entregamaterial em " +
															 "join em.entregadocumento ed " +
															 "where (edfr.entregadocumento.cdentregadocumento in ("+whereIn+") or ed.cdentregadocumento in ("+whereIn+")))")
				.orderBy("material.nome")
				.list();
	}
}
