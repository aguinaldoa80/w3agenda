package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Tabelavalor;
import br.com.linkcom.sined.geral.bean.Tabelavalorhistorico;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TabelavalorhistoricoDAO extends GenericDAO<Tabelavalorhistorico> {
	
	/**
	* M�todo que carrega o hist�rico da tabela de valor
	*
	* @param tabelavalor
	* @return
	* @since 23/06/2015
	* @author Luiz Fernando
	*/
	public List<Tabelavalorhistorico> findByTabelavalor(Tabelavalor tabelavalor) {
		if(tabelavalor == null || tabelavalor.getCdtabelavalor() == null){
			throw new SinedException("Tabela de valor n�o pode ser nula.");
		}
		return query()
					.select("tabelavalorhistorico.cdtabelavalorhistorico, tabelavalorhistorico.acao, " +
							"tabelavalorhistorico.observacao, tabelavalorhistorico.cdusuarioaltera, " +
							"tabelavalorhistorico.dtaltera, " +
							"usuarioaltera.cdpessoa, usuarioaltera.nome ")
					.leftOuterJoin("tabelavalorhistorico.usuarioaltera usuarioaltera")
					.where("tabelavalorhistorico.tabelavalor = ?", tabelavalor)
					.orderBy("tabelavalorhistorico.dtaltera desc")
					.list();
	}
}
