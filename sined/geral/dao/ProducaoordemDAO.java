package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.OrdemProducaoSped;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoetapaitem;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoordemsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoitemsped;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.modulo.producao.controller.crud.filter.ProducaoordemFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoordemDAO extends GenericDAO<Producaoordem> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Producaoordem> query, FiltroListagem _filtro) {
		ProducaoordemFiltro filtro = (ProducaoordemFiltro)_filtro;
		
		query
			.select("distinct producaoordem.cdproducaoordem, producaoordem.dtprevisaoentrega, producaoordem.producaoordemsituacao, " +
					"departamento.nome")
			.leftOuterJoin("producaoordem.departamento departamento")
			.where("producaoordem.empresa = ?", filtro.getEmpresa())
			.where("producaoordem.dtproducaoordem >= ?", filtro.getDtordemproducao1())
			.where("producaoordem.dtproducaoordem <= ?", filtro.getDtordemproducao2())
			.where("producaoordem.dtprevisaoentrega >= ?", filtro.getDtprevisaoentrega1())
			.where("producaoordem.dtprevisaoentrega <= ?", filtro.getDtprevisaoentrega2())
			.where("departamento = ?", filtro.getDepartamento())
			.orderBy("producaoordem.cdproducaoordem desc")
			.ignoreJoin("listaProducaoordemmaterial", "material", "listaProducaoordemmaterialorigem", "producaoagenda", "pedidovenda", "clientePV", "clientePA")
			;
		
		if(filtro.getListaProducaoordemsituacao() != null && filtro.getListaProducaoordemsituacao().size() > 0){
			query.whereIn("producaoordem.producaoordemsituacao", Producaoordemsituacao.listAndConcatenate(filtro.getListaProducaoordemsituacao()));
		}
		
		if(filtro.getCliente() != null || filtro.getCdpedidovenda() != null || filtro.getMaterial() != null || 
				filtro.getIdentificacaoexterna() != null || filtro.getExibirRegistrosComPerda() != null ||
				filtro.getCdpneu() != null || filtro.getCdpedidovendamaterial() != null){
			query.leftOuterJoin("producaoordem.listaProducaoordemmaterial listaProducaoordemmaterial");
			query.where("listaProducaoordemmaterial.material = ?", filtro.getMaterial());
			query.where("listaProducaoordemmaterial.pedidovendamaterial.cdpedidovendamaterial = ?", filtro.getCdpedidovendamaterial());
			query.where("listaProducaoordemmaterial.pneu.cdpneu = ?", filtro.getCdpneu());
			
			if(filtro.getCliente() != null || filtro.getCdpedidovenda() != null || filtro.getIdentificacaoexterna() != null){
				query.leftOuterJoin("listaProducaoordemmaterial.listaProducaoordemmaterialorigem listaProducaoordemmaterialorigem");
				query.leftOuterJoin("listaProducaoordemmaterialorigem.producaoagenda producaoagenda");
				query.leftOuterJoin("producaoagenda.pedidovenda pedidovenda");
	
				query.where("pedidovenda.cdpedidovenda = ?", filtro.getCdpedidovenda());
				query.where("pedidovenda.identificacaoexterna = ?", filtro.getIdentificacaoexterna());
				
				if(filtro.getCliente() != null){
					query
						.leftOuterJoin("pedidovenda.cliente clientePV")
						.leftOuterJoin("producaoagenda.cliente clientePA")
						.openParentheses()
							.where("clientePV = ?", filtro.getCliente())
							.or()
							.where("clientePA = ?", filtro.getCliente())
						.closeParentheses();
				}
			}
			
			if(filtro.getExibirRegistrosComPerda() != null){
				if(filtro.getExibirRegistrosComPerda()){
					query.where("listaProducaoordemmaterial.qtdeperdadescarte is not null");
				} else {
					query.where("listaProducaoordemmaterial.qtdeperdadescarte is null");
				}
			}
		}
	}
	
	/**
	 * M�todo que busca a lista de ordens de produ��o para a listagem do CRUD.
	 *
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 * @since 16/01/2013
	 */
	public List<Producaoordem> loadWithLista(String whereIn, String orderBy, boolean asc) {
		QueryBuilder<Producaoordem> query = query()
			.select("producaoordem.cdproducaoordem, producaoordem.dtprevisaoentrega, producaoordem.producaoordemsituacao, " +
					"departamento.nome, producaoetapanome.cdproducaoetapanome, producaoetapanome.nome, producaoagenda.cdproducaoagenda, pedidovenda.cdpedidovenda, " +
					"material.cdmaterial, material.nome, listaProducaoordemmaterial.observacao, contrato.cdcontrato, " +
					"pedidovendamaterial.cdpedidovendamaterial, pedidovenda.identificacaoexterna, listaProducaoordemmaterial.qtdeperdadescarte, " +
					"pneu.cdpneu ")
			.leftOuterJoin("producaoordem.departamento departamento")
			.leftOuterJoin("producaoordem.listaProducaoordemmaterial listaProducaoordemmaterial")
			.leftOuterJoin("listaProducaoordemmaterial.material material")
			.leftOuterJoin("listaProducaoordemmaterial.pneu pneu")
			.leftOuterJoin("listaProducaoordemmaterial.pedidovendamaterial pedidovendamaterial")
			.leftOuterJoin("listaProducaoordemmaterial.producaoetapaitem producaoetapaitem")
			.leftOuterJoin("producaoetapaitem.producaoetapanome producaoetapanome")
			.leftOuterJoin("listaProducaoordemmaterial.listaProducaoordemmaterialorigem listaProducaoordemmaterialorigem")
			.leftOuterJoin("listaProducaoordemmaterialorigem.producaoagenda producaoagenda")
			.leftOuterJoin("producaoagenda.pedidovenda pedidovenda")
			.leftOuterJoin("producaoagenda.contrato contrato")
			.whereIn("producaoordem.cdproducaoordem", whereIn);
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("producaoordem.cdproducaoordem DESC");
		}
		
		return query.list();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Producaoordem> query) {
		query
		.select("producaoordem.cdproducaoordem, producaoordem.producaoordemsituacao, producaoordem.dtproducaoordem, producaoordem.dtprevisaoentrega, " +
				"empresa.cdpessoa, empresa.nome, " +
				"material.cdmaterial, material.nome, material.identificacao, " +
				"unidademedida.cdunidademedida, unidademedida.nome, " +
				"listaProducaoordemmaterial.cdproducaoordemmaterial, listaProducaoordemmaterial.observacao, listaProducaoordemmaterial.qtde, listaProducaoordemmaterial.identificadorinterno, " +
				"listaProducaoordemmaterial.qtdeperdadescarte, listaProducaoordemmaterial.qtdeproduzido, listaProducaoordemmaterial.residuobanda, " +
				"garantiatipo.cdgarantiatipo, garantiatipopercentual.cdgarantiatipopercentual, " +
				"pedidovendamaterialgarantido.cdpedidovendamaterial, materialgarantido.cdmaterial, materialgarantido.nome, materialgarantido.identificacao, " +
				"producaoetapaitem.cdproducaoetapaitem, producaoetapa.cdproducaoetapa, producaoetapa.baixarEstoqueMateriaprima, " +
				"pedidovendamaterial.cdpedidovendamaterial, " +
				"departamento.nome, departamento.descricao, departamento.cddepartamento")
		.leftOuterJoin("producaoordem.empresa empresa")
		.leftOuterJoin("producaoordem.departamento departamento")
		.leftOuterJoin("producaoordem.listaProducaoordemmaterial listaProducaoordemmaterial")
		.leftOuterJoin("listaProducaoordemmaterial.material material")
		.leftOuterJoin("listaProducaoordemmaterial.producaoetapaitem producaoetapaitem")
		.leftOuterJoin("listaProducaoordemmaterial.producaoetapa producaoetapa")
		.leftOuterJoin("listaProducaoordemmaterial.pneu pneu")
		.leftOuterJoin("listaProducaoordemmaterial.unidademedida unidademedida")
		.leftOuterJoin("listaProducaoordemmaterial.garantiatipo garantiatipo")
		.leftOuterJoin("listaProducaoordemmaterial.garantiatipopercentual garantiatipopercentual")
		.leftOuterJoin("listaProducaoordemmaterial.pedidovendamaterial pedidovendamaterial")
		.leftOuterJoin("pedidovendamaterial.pedidovendamaterialgarantido pedidovendamaterialgarantido")
		.leftOuterJoin("pedidovendamaterialgarantido.material materialgarantido");
		query = SinedUtil.setJoinsByComponentePneu(query);
	}
	
//	@Override
//	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
//		save.saveOrUpdateManaged("listaProducaoordemmaterial");
//		save.saveOrUpdateManaged("listaProducaoordemitemadicional");
//		save.saveOrUpdateManaged("listaProducaoordemequipamento");
//	}

	/**
	 * M�todo que busca os registro para a gera��o do CSV.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Producaoordem> findForCsv(ProducaoordemFiltro filtro) {
		QueryBuilder<Producaoordem> query = querySined();
		this.updateListagemQuery(query, filtro);
		query.setIgnoreJoinPaths(new ListSet<String>(String.class));
		query.select(query.getSelect().getValue() + ", listaProducaoordemmaterial.qtde, listaProducaoordemmaterial.qtdeproduzido, material.nome");
		query
			.leftOuterJoinIfNotExists("producaoordem.listaProducaoordemmaterial listaProducaoordemmaterial")
			.leftOuterJoinIfNotExists("listaProducaoordemmaterial.material material");
		
		query.orderBy("producaoordem.cdproducaoordem");
		return query.list();
	}

	/**
	 * M�todo que busca a lista de Producaoordem com a situa��o preenchida.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Producaoordem> findWithSituacao(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("producaoordem.cdproducaoordem, producaoordem.producaoordemsituacao")
					.whereIn("producaoordem.cdproducaoordem", whereIn)
					.list();
	}

	/**
	 * M�todo que busca a lista de Producaoordem com a situa��o e departamento preenchida.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/01/2013
	 */
	public List<Producaoordem> findWithSituacaoDepartamento(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("producaoordem.cdproducaoordem, producaoordem.producaoordemsituacao, departamento.cddepartamento")
					.leftOuterJoin("producaoordem.departamento departamento")
					.whereIn("producaoordem.cdproducaoordem", whereIn)
					.list();
	}

	/**
	 * Busca a lista para a tela de confirma��o do registro de produ��o.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/01/2013
	 */
	public List<Producaoordem> findForRegistrarProducao(String whereIn) {
		return query()
					.select("producaoordem.cdproducaoordem, producaoordem.dtprevisaoentrega, producaoordem.producaoordemsituacao, " +
							"producaoordemmaterial.cdproducaoordemmaterial, producaoordemmaterial.qtde, producaoordemmaterial.qtdeperdadescarte, " +
							"producaoordemmaterial.qtdeproduzido, producaoordemmaterial.identificadorinterno, pneu.cdpneu,  " +
							"material.cdmaterial, material.nome, material.servico, unidademedida.cdunidademedida, producaoetapaitem.cdproducaoetapaitem, producaoetapanome.cdproducaoetapanome, producaoetapanome.nome, " +
							"producaoetapaitem.ordem, po.cdproducaoordem, producaoagenda.cdproducaoagenda, producaoetapa.cdproducaoetapa, " +
							"pedidovendamaterial.cdpedidovendamaterial, producaoordemmaterial.observacao, empresa.cdpessoa, " +
							"producaoetapaordem.cdproducaoetapa, producaoetapaordem.naogerarentradaconclusao, producaoetapaordem.naoagruparmaterial, producaoetapaordem.baixarEstoqueMateriaprima, " +
							"producaoagendamaterial.cdproducaoagendamaterial, producaoetapanome.permitirtrocarprodutofinal, producaoetapa.naoagruparmaterial, producaoetapa.baixarEstoqueMateriaprima," +
							"producaoetapanome.permitirgerargarantia, garantiatipo.geragarantia")
					.leftOuterJoin("producaoordem.listaProducaoordemmaterial producaoordemmaterial")
					.leftOuterJoin("producaoordem.empresa empresa")
					.leftOuterJoin("producaoordemmaterial.pedidovendamaterial pedidovendamaterial")
					.leftOuterJoin("producaoordemmaterial.producaoordem po")
					.leftOuterJoin("producaoordemmaterial.material material")
					.leftOuterJoin("producaoordemmaterial.unidademedida unidademedida")
					.leftOuterJoin("producaoordemmaterial.pneu pneu")
					.leftOuterJoin("producaoordemmaterial.producaoetapa producaoetapaordem")
					.leftOuterJoin("producaoordemmaterial.producaoetapaitem producaoetapaitem")
					.leftOuterJoin("producaoetapaitem.producaoetapanome producaoetapanome")
					.leftOuterJoin("producaoetapaitem.producaoetapa producaoetapa")
					.leftOuterJoin("producaoordemmaterial.listaProducaoordemmaterialorigem producaoordemmaterialorigem")
					.leftOuterJoin("producaoordemmaterialorigem.producaoagenda producaoagenda")
					.leftOuterJoin("producaoordemmaterialorigem.producaoagendamaterial producaoagendamaterial")
					.leftOuterJoin("producaoordemmaterial.garantiatipo garantiatipo")
					.whereIn("producaoordem.cdproducaoordem", whereIn)
					.list();
	}

	/**
	 * Atualiza a situa��o da ordem de produ��o.
	 *
	 * @param producaoordem
	 * @param producaoordemsituacao
	 * @author Rodrigo Freitas
	 * @since 15/01/2013
	 */
	public void updateSituacao(Producaoordem producaoordem, Producaoordemsituacao producaoordemsituacao) {
		getHibernateTemplate().bulkUpdate("update Producaoordem p set p.producaoordemsituacao = ? where p.id = ?", new Object[]{
				producaoordemsituacao, producaoordem.getCdproducaoordem()
		});
	}
	
	/**
	 * Atualiza a situa��o da ordem de produ��o.
	 *
	 * @param whereIn
	 * @param producaoordemsituacao
	 * @author Rodrigo Freitas
	 * @since 29/07/2014
	 */
	public void updateSituacao(String whereIn, Producaoordemsituacao producaoordemsituacao) {
		getHibernateTemplate().bulkUpdate("update Producaoordem p set p.producaoordemsituacao = ? where p.cdproducaoordem in (" + whereIn + ")", new Object[]{
				producaoordemsituacao
		});
	}

	/**
	 * Busca a lista de Ordem de produ��o para emiss�o via template
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/07/2015
	 */
	public List<Producaoordem> loadForReporTemplate(String whereIn) {
		if(whereIn == null || whereIn.equals(""))
			throw new SinedException("WhereIn de Ordem de Produ��o n�o pode ser nula.");
		
		return querySined()
				
		.select("producaoordem.cdproducaoordem, producaoordem.dtprevisaoentrega, producaoordem.producaoordemsituacao, " +
				"departamento.nome, producaoordem.dtproducaoordem, " +
				
				"listaProducaoordemmaterial.cdproducaoordemmaterial, " +
				
				"producaoetapaitem.cdproducaoetapaitem, producaoetapanome.cdproducaoetapanome, producaoetapanome.nome, producaoetapaitem.qtdediasprevisto, " +
				
				"listaProducaoordemmaterial.qtde, listaProducaoordemmaterial.qtdeproduzido, listaProducaoordemmaterial.observacao, " +
				"materialPOM.cdmaterial, materialPOM.nome, materialPOM.identificacao, materialgrupoPOM.cdmaterialgrupo, materialgrupoPOM.nome, " +
				
				"listaProducaoordemmaterialorigem.cdproducaoordemmaterialorigem, " +
				
				"listaProducaoagendamaterial.cdproducaoagendamaterial, listaProducaoagendamaterial.altura, listaProducaoagendamaterial.largura," +
				"listaProducaoagendamaterial.comprimento, listaProducaoagendamaterial.qtde, listaProducaoagendamaterial.observacao, " +
				"producaoagenda.cdproducaoagenda, producaoagenda.dtentrega, " +
				"clientePA.cdpessoa, clientePA.nome, " +
				"materialPAM.cdmaterial, materialPAM.nome, materialPAM.identificacao, " +
				"materialgrupoPAM.nome, materialgrupoPAM.cdmaterialgrupo, " +
				"producaoetapaPAM.cdproducaoetapa, producaoetapaPAM.nome, " +
				
				"pedidovenda.cdpedidovenda, pedidovenda.identificacaoexterna, pedidovenda.dtpedidovenda, listaPedidovendamaterial.cdpedidovendamaterial, " +
				"listaPedidovendamaterial.altura, listaPedidovendamaterial.largura, listaPedidovendamaterial.comprimento, " +
				"listaPedidovendamaterial.comprimentooriginal, listaPedidovendamaterial.quantidade, listaPedidovendamaterial.preco, " +
				"materialPVM.cdmaterial, materialPVM.identificacao, materialgrupoPVM.cdmaterialgrupo, materialgrupoPVM.nome, " +
				
				"venda.cdvenda, venda.dtvenda, listavendamaterial.cdvendamaterial, " +
				"listavendamaterial.altura, listavendamaterial.largura, listavendamaterial.comprimento, " +
				"listavendamaterial.comprimentooriginal, listavendamaterial.quantidade, listavendamaterial.preco, " +
				"materialVM.cdmaterial, materialVM.identificacao, materialgrupoVM.cdmaterialgrupo, materialgrupoVM.nome, " +
				
				"contrato.cdcontrato, listaContratomaterial.cdcontratomaterial, " +
				"servicoC.cdmaterial, servicoC.identificacao, materialgrupoC.cdmaterialgrupo, materialgrupoC.nome, " +
				"unidademedidaPAM.cdunidademedida, unidademedidaPAM.nome, " +
				
				"listaProducaoagendamaterialmateriaprima.cdproducaoagendamaterialmateriaprima, " +
				"listaProducaoagendamaterialmateriaprima.qtdeprevista, " +
				"listaProducaoagendamaterialmateriaprima.agitacao, " +
				"listaProducaoagendamaterialmateriaprima.quantidadepercentual, " +
				"material.nome, " +
				"loteestoque.numero, " +
				
				"unidademedida.cdunidademedida, unidademedida.nome, " +
				
				"pneu.cdpneu, pneu.serie, pneu.dot, pneu.numeroreforma, " +
				"pneumarca.cdpneumarca, pneumarca.nome, " +
				"pneumedida.cdpneumedida, pneumedida.nome, " +
				"pneumodelo.cdpneumodelo, pneumodelo.nome, " +
		 		"pneumaterialbanda.cdmaterial, pneumaterialbanda.identificacao, pneumaterialbanda.nome," +
		 		"pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome")
		
		.leftOuterJoin("producaoordem.departamento departamento")
		
		.leftOuterJoin("producaoordem.listaProducaoordemmaterial listaProducaoordemmaterial")
		.leftOuterJoin("listaProducaoordemmaterial.producaoetapaitem producaoetapaitem")
		.leftOuterJoin("producaoetapaitem.producaoetapanome producaoetapanome")
		
		.leftOuterJoin("listaProducaoordemmaterial.pneu pneu")
		.leftOuterJoin("pneu.pneumarca pneumarca")
	    .leftOuterJoin("pneu.pneumodelo pneumodelo")
	    .leftOuterJoin("pneu.pneumedida pneumedida")
	    .leftOuterJoin("pneu.materialbanda pneumaterialbanda")
	    .leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
		
		.leftOuterJoin("listaProducaoordemmaterial.material materialPOM")
		.leftOuterJoin("materialPOM.materialgrupo materialgrupoPOM")
		
		.leftOuterJoin("listaProducaoordemmaterial.listaProducaoordemmaterialorigem listaProducaoordemmaterialorigem")
		
		.leftOuterJoin("listaProducaoordemmaterialorigem.producaoagenda producaoagenda")
		.leftOuterJoin("producaoagenda.listaProducaoagendamaterial listaProducaoagendamaterial")
		.leftOuterJoin("listaProducaoagendamaterial.material materialPAM")
		.leftOuterJoin("listaProducaoagendamaterial.unidademedida unidademedidaPAM")
		.leftOuterJoin("materialPAM.materialgrupo materialgrupoPAM")
		.leftOuterJoin("listaProducaoagendamaterial.producaoetapa producaoetapaPAM")
		.leftOuterJoin("producaoagenda.cliente clientePA")
		.leftOuterJoin("listaProducaoagendamaterial.listaProducaoagendamaterialmateriaprima listaProducaoagendamaterialmateriaprima")
		.leftOuterJoin("listaProducaoagendamaterialmateriaprima.unidademedida unidademedida")
		.leftOuterJoin("listaProducaoagendamaterialmateriaprima.material material")
		.leftOuterJoin("listaProducaoagendamaterialmateriaprima.loteestoque loteestoque")
		
		.leftOuterJoin("producaoagenda.contrato contrato")
		.leftOuterJoin("contrato.listaContratomaterial listaContratomaterial")
		.leftOuterJoin("listaContratomaterial.servico servicoC")
		.leftOuterJoin("servicoC.materialgrupo materialgrupoC")
		
		.leftOuterJoin("producaoagenda.pedidovenda pedidovenda")
		.leftOuterJoin("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
		.leftOuterJoin("listaPedidovendamaterial.material materialPVM")
		.leftOuterJoin("materialPVM.materialgrupo materialgrupoPVM")
		
		.leftOuterJoin("producaoagenda.venda venda")
		.leftOuterJoin("venda.listavendamaterial listavendamaterial")
		.leftOuterJoin("listavendamaterial.material materialVM")
		.leftOuterJoin("materialVM.materialgrupo materialgrupoVM")
		
		.whereIn("producaoordem.cdproducaoordem", whereIn)
		.list();
	}
	
	/**
	 * M�todo que busca a lista de Producaoordem com a situa��o, departamento e coleta preenchidas.
	 *
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Producaoordem> findForDevolverColeta(String whereIn) {
		return this.findForDevolverColeta(whereIn, null);
	}
	
	/**
	 * M�todo que busca a lista de Producaoordem com a situa��o, departamento e coleta preenchidas.
	 *
	 * @param whereIn
	 * @param whereInProducaoordemmaterial
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Producaoordem> findForDevolverColeta(String whereIn, String whereInProducaoordemmaterial) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("producaoordem.cdproducaoordem, producaoordem.producaoordemsituacao, departamento.cddepartamento, " +
							"producaoordemmaterial.cdproducaoordemmaterial, producaoordemmaterialorigem.cdproducaoordemmaterialorigem, " +
							"producaoordemmaterial.qtde, producaoordemmaterial.qtdeperdadescarte, producaoordemmaterial.observacao, " +
							"producaoagenda.cdproducaoagenda, pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao, pedidovendamaterial.cdpedidovendamaterial, " +
							"material.cdmaterial, material.nome, material.identificacao, material.nomenf, material.valorcusto," +
							"material.produto, material.servico, material.epi, material.patrimonio, " +
							"materialgrupo.cdmaterialgrupo, materialgrupo.nome, " +
							"unidademedida.cdunidademedida, unidademedida.nome, unidademedidaMaterial.cdunidademedida, unidademedidaMaterial.nome, " +
							"cliente.cdpessoa, cliente.nome, empresa.cdpessoa, empresa.nome," +
							"producaoetapa.cdproducaoetapa, producaoetapa.baixarEstoqueMateriaprima, producaoetapa.baixarmateriaprimacascata," +
							"producaoagendamaterial.cdproducaoagendamaterial, " +
							"pneu.cdpneu, pneu.serie, pneu.dot, pneu.numeroreforma, " +
							"pneumarca.cdpneumarca, pneumarca.nome, " +
							"pneumedida.cdpneumedida, pneumedida.nome, " +
							"pneumodelo.cdpneumodelo, pneumodelo.nome, " +
							"pneumaterialbanda.cdmaterial, pneumaterialbanda.identificacao, pneumaterialbanda.nome, pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome ")
					.join("producaoordem.listaProducaoordemmaterial producaoordemmaterial")
					.leftOuterJoin("producaoordem.empresa empresa")
					.leftOuterJoin("producaoordemmaterial.listaProducaoordemmaterialorigem producaoordemmaterialorigem")
					.leftOuterJoin("producaoordemmaterialorigem.producaoagenda producaoagenda")
					.leftOuterJoin("producaoordemmaterialorigem.producaoagendamaterial producaoagendamaterial")
					.leftOuterJoin("producaoagenda.pedidovenda pedidovenda")
					.leftOuterJoin("producaoagenda.cliente cliente")
					.leftOuterJoin("producaoordem.departamento departamento")
					.leftOuterJoin("producaoordemmaterial.pedidovendamaterial pedidovendamaterial")
					.leftOuterJoin("producaoordemmaterial.material material")
					.leftOuterJoin("material.unidademedida unidademedidaMaterial")
					.leftOuterJoin("material.materialgrupo materialgrupo")
					.leftOuterJoin("producaoordemmaterial.unidademedida unidademedida")
					.leftOuterJoin("producaoordemmaterial.producaoetapa producaoetapa")
					.leftOuterJoin("producaoordemmaterial.pneu pneu")
					.leftOuterJoin("pneu.pneumarca pneumarca")
				    .leftOuterJoin("pneu.pneumodelo pneumodelo")
				    .leftOuterJoin("pneu.pneumedida pneumedida")
				    .leftOuterJoin("pneu.materialbanda pneumaterialbanda")
				    .leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
					.whereIn("producaoordem.cdproducaoordem", whereIn)
					.whereIn("producaoordemmaterial.cdproducaoordemmaterial", whereInProducaoordemmaterial)
					.list();
	}
	
	/**
	 * Verifica se existe ordens de produ��o na situa��o passada a paritr das agendas de produ��o
	 *
	 * @param whereIn
	 * @param situacoes
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/07/2014
	 */
	public boolean haveProducaoordemSituacaoByProducaoagenda(String whereIn, Producaoordemsituacao[] situacoes) {
		QueryBuilder<Long> query = newQueryBuilderWithFrom(Long.class)
					.select("count(*)")
					.join("producaoordem.listaProducaoordemmaterial producaoordemmaterial")
					.join("producaoordemmaterial.listaProducaoordemmaterialorigem producaoordemmaterialorigem")
					.join("producaoordemmaterialorigem.producaoagenda producaoagenda")
					.whereIn("producaoagenda.cdproducaoagenda", whereIn);
		
		if(situacoes != null && situacoes.length > 0){
			query.openParentheses();
			for (int i = 0; i < situacoes.length; i++) {
				query.where("producaoordem.producaoordemsituacao = ?", situacoes[i]).or();
			}
			query.closeParentheses();
		}
		
		return query.unique() > 0;
	}
	
	/**
	 * Busca as ordens de produ��o de agendas vinculadas
	 *
	 * @param whereInProducaoagenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/07/2014
	 */
	public List<Producaoordem> findByProducaoagenda(String whereInProducaoagenda) {
		return query()
					.select("producaoordem.cdproducaoordem, producaoordem.producaoordemsituacao, " +
							"producaoagenda.cdproducaoagenda ")
					.join("producaoordem.listaProducaoordemmaterial producaoordemmaterial")
					.join("producaoordemmaterial.listaProducaoordemmaterialorigem producaoordemmaterialorigem")
					.join("producaoordemmaterialorigem.producaoagenda producaoagenda")
					.whereIn("producaoagenda.cdproducaoagenda", whereInProducaoagenda)
					.list();
	}
	
	public boolean existeProducaoagenda(Producaoordem producaoordem) {
		if(producaoordem == null || producaoordem.getCdproducaoordem() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderWithFrom(Long.class)
			.select("count(*)")
			.join("producaoordem.listaProducaoordemmaterial producaoordemmaterial")
			.join("producaoordemmaterial.listaProducaoordemmaterialorigem producaoordemmaterialorigem")
			.join("producaoordemmaterialorigem.producaoagenda producaoagenda")
			.where("producaoordem = ?", producaoordem)
			.where("producaoordemmaterialorigem.producaoagenda is not null")
			.unique() > 0;
	}
	
	public Producaoordem findByPneuAndProducaoagendamaterial(Pneu pneu, Producaoagendamaterial producaoagendamaterial) {
		if(pneu == null || pneu.getCdpneu() == null || producaoagendamaterial == null || producaoagendamaterial.getCdproducaoagendamaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("producaoordem.cdproducaoordem, producaoordem.producaoordemsituacao")
			.join("producaoordem.listaProducaoordemmaterial producaoordemmaterial")
			.join("producaoordemmaterial.listaProducaoordemmaterialorigem producaoordemmaterialorigem")
			.join("producaoordemmaterialorigem.producaoagendamaterial producaoagendamaterial")
			.where("producaoordemmaterialorigem.producaoagendamaterial = ?", producaoagendamaterial)
			.where("producaoordemmaterial.pneu = ?", pneu)
			.where("producaoordem.producaoordemsituacao = ?", Producaoordemsituacao.CANCELADA)
			.orderBy("producaoordem.cdproducaoordem desc")
			.setMaxResults(1)
			.unique();
	}

	/**
	* M�todo que busca as ordens de produ��o com a qtde devolvida
	*
	* @param whereInProducaoordemmaterial
	* @return
	* @since 08/07/2015
	* @author Luiz Fernando
	*/
	public List<Producaoordem> findForCancelamentoQtdeDevolvida(String whereInProducaoordemmaterial) {
		if(whereInProducaoordemmaterial == null || whereInProducaoordemmaterial.equals(""))
			throw new SinedException("Par�metro inv�lido");
		
		return query()
				.select("producaoordem.cdproducaoordem, producaoordem.producaoordemsituacao, " +
						"listaProducaoordemmaterial.cdproducaoordemmaterial, listaProducaoordemmaterial.qtde, " +
						"listaProducaoordemmaterial.qtdeproduzido, listaProducaoordemmaterial.qtdeperdadescarte," +
						"listaProducaoordemmaterialorigem.cdproducaoordemmaterialorigem, " +
						"producaoagenda.cdproducaoagenda, producaoagenda.producaoagendasituacao ")
				.leftOuterJoin("producaoordem.listaProducaoordemmaterial listaProducaoordemmaterial")
				.leftOuterJoin("listaProducaoordemmaterial.listaProducaoordemmaterialorigem listaProducaoordemmaterialorigem")
				.leftOuterJoin("listaProducaoordemmaterialorigem.producaoagenda producaoagenda")
				.where("exists (  select 1 " +
								" from Producaoordemmaterial pom " +
								" where pom.producaoordem.cdproducaoordem = producaoordem.cdproducaoordem and  " +
								" pom.cdproducaoordemmaterial in ("+whereInProducaoordemmaterial+") ) ")
				.list();
	}

	/**
	 * Busca as ordens de produ��o vinculadas ao pneu
	 *
	 * @param pneu
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/07/2015
	 */
	public List<Producaoordem> findByPneu(Pneu pneu) {
		return query()
				.select("producaoordem.cdproducaoordem, producaoordem.producaoordemsituacao")
				.join("producaoordem.listaProducaoordemmaterial listaProducaoordemmaterial")
				.where("listaProducaoordemmaterial.pneu = ?", pneu)
				.list();
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param producaoagenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/08/2015
	 */
	public Date getMaxDateProducaoByProducaoagenda(Producaoagenda producaoagenda) {
		return newQueryBuilderSined(Date.class)
				.select("max(producaoordem.dtproducaoordem)")
				.setUseTranslator(false)
				.from(Producaoordem.class)
				.join("producaoordem.listaProducaoordemmaterial listaProducaoordemmaterial")
				.join("listaProducaoordemmaterial.listaProducaoordemmaterialorigem listaProducaoordemmaterialorigem")
				.where("listaProducaoordemmaterialorigem.producaoagenda = ?", producaoagenda)
				.unique();
	}
	
	/**
	* M�todo que busca as ordens de produ��o vinculados a coleta
	*
	* @param whereIn
	* @return
	* @since 15/09/2015
	* @author Luiz Fernando
	*/
	public List<Producaoordem> findForDevolverColetaByColeta(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("producaoordem.cdproducaoordem, producaoordem.producaoordemsituacao, departamento.cddepartamento, " +
							"producaoordemmaterial.cdproducaoordemmaterial, producaoordemmaterialorigem.cdproducaoordemmaterialorigem, " +
							"producaoagenda.cdproducaoagenda, pedidovenda.cdpedidovenda, pedidovendamaterial.cdpedidovendamaterial, " +
							"producaoetapa.cdproducaoetapa, producaoetapa.baixarEstoqueMateriaprima, producaoetapa.baixarmateriaprimacascata," +
							"producaoagendamaterial.cdproducaoagendamaterial, " +
							"pneu.cdpneu, pneu.serie, pneu.dot, pneu.numeroreforma, " +
							"pneumarca.cdpneumarca, pneumarca.nome, " +
							"pneumedida.cdpneumedida, pneumedida.nome, " +
							"pneumodelo.cdpneumodelo, pneumodelo.nome, " +
							"pneumaterialbanda.cdmaterial, pneumaterialbanda.identificacao, pneumaterialbanda.nome, pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome")
					.join("producaoordem.listaProducaoordemmaterial producaoordemmaterial")
					.join("producaoordemmaterial.listaProducaoordemmaterialorigem producaoordemmaterialorigem")
					.join("producaoordemmaterialorigem.producaoagenda producaoagenda")
					.join("producaoordemmaterialorigem.producaoagendamaterial producaoagendamaterial")
					.join("producaoagenda.pedidovenda pedidovenda")
					.leftOuterJoin("producaoordem.departamento departamento")
					.leftOuterJoin("producaoordemmaterial.pedidovendamaterial pedidovendamaterial")
					.leftOuterJoin("producaoordemmaterial.producaoetapa producaoetapa")
					.leftOuterJoin("producaoordemmaterial.pneu pneu")
					.leftOuterJoin("pneu.pneumarca pneumarca")
				    .leftOuterJoin("pneu.pneumodelo pneumodelo")
				    .leftOuterJoin("pneu.pneumedida pneumedida")
				    .leftOuterJoin("pneu.materialbanda pneumaterialbanda")
				    .leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
					.where("pedidovenda.cdpedidovenda in (" +
										" select pv.cdpedidovenda " +
										" from Coleta c " +
										" join c.pedidovenda pv " +
										" where c.cdcoleta in (" + whereIn + ") " +
										" ) ")
					.list();
	}

	/**
	 * Busca as ordens de produ��o baseadas no pedido de venda do par�metro.
	 *
	 * @param pedidovenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/11/2015
	 */
	public List<Producaoordem> findByPedidovenda(Pedidovenda pedidovenda) {
		return query()
					.select("producaoordem.cdproducaoordem, listaProducaoordemitemadicional.cdproducaoordemitemadicional, listaProducaoordemitemadicional.quantidade, " +
							"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
							"material.cdmaterial, material.nome, material.identificacao, material.valorvenda, material.valorvendamaximo, material.valorvendaminimo, " +
							"material.vendapromocional, material.kitflexivel, material.valorcusto, material.peso, material.pesobruto, " +
							"pneu.cdpneu ")
					.join("producaoordem.listaProducaoordemmaterial producaoordemmaterial")
					.join("producaoordemmaterial.listaProducaoordemmaterialorigem producaoordemmaterialorigem")
					.join("producaoordemmaterialorigem.producaoagenda producaoagenda")
					.join("producaoagenda.pedidovenda pedidovenda")
					.leftOuterJoin("producaoordemmaterial.pneu pneu")
					.leftOuterJoin("producaoordem.listaProducaoordemitemadicional listaProducaoordemitemadicional")
					.leftOuterJoin("listaProducaoordemitemadicional.material material")
					.leftOuterJoin("material.unidademedida unidademedida")
					.where("pedidovenda = ?", pedidovenda)
					.list();
	}

	/**
	* M�todo que busca as ordens de produ��o para estorno
	*
	* @param whereIn
	* @return
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public List<Producaoordem> findForEstornar(String whereIn) {
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		return query()
				.select("producaoordem.cdproducaoordem, producaoordem.producaoordemsituacao, " +
						"listaProducaoordemmaterial.cdproducaoordemmaterial, listaProducaoordemmaterial.qtdeperdadescarte, " +
						"material.cdmaterial, material.produto, material.servico, material.epi, pedidovendamaterial.cdpedidovendamaterial, " +
						"listaProducaoordemmaterialorigem.cdproducaoordemmaterialorigem, pedidovenda.cdpedidovenda, " +
						"producaoagenda.cdproducaoagenda, producaoagenda.producaoagendasituacao ")
				.join("producaoordem.listaProducaoordemmaterial listaProducaoordemmaterial")
				.join("listaProducaoordemmaterial.material material")
				.leftOuterJoin("listaProducaoordemmaterial.pedidovendamaterial pedidovendamaterial")
				.leftOuterJoin("listaProducaoordemmaterial.listaProducaoordemmaterialorigem listaProducaoordemmaterialorigem")
				.leftOuterJoin("listaProducaoordemmaterialorigem.producaoagenda producaoagenda")
				.leftOuterJoin("producaoagenda.pedidovenda pedidovenda")
				.whereIn("producaoordem.cdproducaoordem", whereIn)
				.orderBy("producaoordem.cdproducaoordem desc")
				.list();
	}
	
	/**
	* M�todo que retorna as ordens de produ��o que tem origem de uma etapa anterior  
	*
	* @param whereIn
	* @return
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public List<Producaoordem> findForCancelarProximasEtapas(String whereIn){
		String sql = "WITH RECURSIVE arvore_producaoordem (cdproducaoordem, producaoordemsituacao) AS " +
					 "( " +
					 "		SELECT cdproducaoordem, producaoordemsituacao " + 
					 "		FROM producaoordem po1  " +
					 "		WHERE po1.cdproducaoordem = " + whereIn +
					 "		UNION ALL " +
					 "		SELECT po2.cdproducaoordem, po2.producaoordemsituacao " +
					 "		FROM producaoordem po2 " +
					 "		INNER JOIN arvore_producaoordem arvore_po ON po2.cdproducaoordemanterior = arvore_po.cdproducaoordem " +
					 "		) " +
					 "		SELECT cdproducaoordem, producaoordemsituacao " +
					 "		FROM arvore_producaoordem " +
					 "		WHERE arvore_producaoordem.cdproducaoordem not in ("+whereIn+") and arvore_producaoordem.producaoordemsituacao <> " + Producaoordemsituacao.CANCELADA.getValue();

		SinedUtil.markAsReader();
		List<Producaoordem> lista = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Producaoordem(rs.getInt("cdproducaoordem"));
			}
		});
		
		
		if(SinedUtil.isListNotEmpty(lista)){
			lista = findForEstornar(SinedUtil.listAndConcatenate(lista, "cdproducaoordem", ","));
		}
		return lista;
	}
	
	/**
	* M�todo que carrega a ordem de produ��o com a ordem de produ��o anterior
	*
	* @param producaoordem
	* @return
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public Producaoordem loadWithProducaoordemanterior(Producaoordem producaoordem){
		if(producaoordem == null || producaoordem.getCdproducaoordem() == null)
			throw new SinedException("Ordem de produ��o n�o pode ser nula.");
		
		return query()
			.select("producaoordem.cdproducaoordem, producaoordemanterior.cdproducaoordem, producaoordemanterior.producaoordemsituacao ")
			.leftOuterJoin("producaoordem.producaoordemanterior producaoordemanterior")
			.where("producaoordem = ?", producaoordem)
			.unique();
	
	}
	
	/**
	* M�todo que busca as ordens de produ��o para registrar produ��o
	*
	* @param whereIn
	* @return
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordem> findForProducao(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("producaoordem.cdproducaoordem, producaoordem.producaoordemsituacao, producaoordem.dtprevisaoentrega, " +
							"producaoordemmaterial.cdproducaoordemmaterial, producaoordemmaterial.qtde, " +
							"material.cdmaterial, material.nome, " +
							"producaoetapa.cdproducaoetapa, producaoetapa.nome, producaoetapa.baixarmateriaprimacascata, listaProducaoetapaitem.cdproducaoetapaitem, " +
							"producaoetapa.naoagruparmaterial, producaoetapa.naogerarentradaconclusao, producaoetapaItem.baixarEstoqueMateriaprima, " +
							"producaoetapaItem.naoagruparmaterial, producaoetapaItem.naogerarentradaconclusao, " +
							"producaoetapanome.cdproducaoetapanome, producaoetapanome.nome, listaProducaoetapaitem.ordem, departamento.cddepartamento, departamento.nome, " +
							"listaProducaoetapaitem.qtdediasprevisto, producaoetapaitem.cdproducaoetapaitem, " +
							"producaoetapaItem.cdproducaoetapa,  producaoetapaItem.baixarmateriaprimacascata, producaoetapanomeItem.cdproducaoetapanome, producaoetapanomeItem.nome, listaProducaoetapaitemItem.cdproducaoetapaitem, " +
							"listaProducaoetapaitemItem.ordem, departamentoItem.cddepartamento, departamentoItem.nome, " +
							"listaProducaoetapaitemItem.qtdediasprevisto, producaoordemmaterial.observacao," +
							"pedidovendamaterial.cdpedidovendamaterial, empresa.cdpessoa, " +
							"producaoordemmaterialmateriaprima.cdproducaoordemmaterialmateriaprima, producaoordemmaterialmateriaprima.qtdeprevista, " +
							"producaoordemmaterialmateriaprimaMaterial.cdmaterial, producaoordemmaterialmateriaprimaMaterial.nome, " +
							"producaoordemmaterialmateriaprima.fracaounidademedida, producaoordemmaterialmateriaprima.qtdereferenciaunidademedida, " +
							"loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, " +
							"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.casasdecimaisestoque, " +
							"pneu.cdpneu, pneumaterialbanda.cdmaterial, pneumaterialbanda.nome, producaoetapanomeMateriaprima.cdproducaoetapanome, producaoetapanomeMateriaprima.nome," +
							"producaoagendamaterial.cdproducaoagendamaterial, materialPA.cdmaterial, " +
							"producaoagenda.cdproducaoagenda, localbaixamateriaprima.cdlocalarmazenagem, localbaixamateriaprima.nome," +
							"pedidovenda.cdpedidovenda, localarmazenagemPV.cdlocalarmazenagem, localarmazenagemPV.nome, " +
							"venda.cdvenda, localarmazenagemV.cdlocalarmazenagem, localarmazenagemV.nome ")
					.leftOuterJoin("producaoordem.empresa empresa")
					.leftOuterJoin("producaoordem.listaProducaoordemmaterial producaoordemmaterial")
					.leftOuterJoin("producaoordemmaterial.material material")
					.leftOuterJoin("producaoordemmaterial.pneu pneu")
					.leftOuterJoin("pneu.materialbanda pneumaterialbanda")
					.leftOuterJoin("producaoordemmaterial.pedidovendamaterial pedidovendamaterial")
					.leftOuterJoin("material.producaoetapa producaoetapa")
					.leftOuterJoin("producaoordemmaterial.producaoetapaitem producaoetapaitem")
					.leftOuterJoin("producaoetapa.listaProducaoetapaitem listaProducaoetapaitem")
					.leftOuterJoin("listaProducaoetapaitem.producaoetapanome producaoetapanome")
					.leftOuterJoin("listaProducaoetapaitem.departamento departamento")
					.leftOuterJoin("producaoordemmaterial.producaoetapa producaoetapaItem")
					.leftOuterJoin("producaoordemmaterial.listaProducaoordemmaterialmateriaprima producaoordemmaterialmateriaprima")
					.leftOuterJoin("producaoordemmaterialmateriaprima.loteestoque loteestoque")
					.leftOuterJoin("producaoordemmaterialmateriaprima.material producaoordemmaterialmateriaprimaMaterial")
					.leftOuterJoin("producaoordemmaterialmateriaprima.unidademedida unidademedida")
					.leftOuterJoin("producaoordemmaterialmateriaprima.producaoetapanome producaoetapanomeMateriaprima")
					.leftOuterJoin("producaoetapaItem.listaProducaoetapaitem listaProducaoetapaitemItem")
					.leftOuterJoin("listaProducaoetapaitemItem.producaoetapanome producaoetapanomeItem")
					.leftOuterJoin("listaProducaoetapaitemItem.departamento departamentoItem")
					.leftOuterJoin("producaoordemmaterial.listaProducaoordemmaterialorigem listaProducaoordemmaterialorigem")
					.leftOuterJoin("listaProducaoordemmaterialorigem.producaoagendamaterial producaoagendamaterial")
					.leftOuterJoin("producaoagendamaterial.material materialPA")
					.leftOuterJoin("listaProducaoordemmaterialorigem.producaoagenda producaoagenda")
					.leftOuterJoin("producaoagenda.localbaixamateriaprima localbaixamateriaprima")
					.leftOuterJoin("producaoagenda.venda venda")
					.leftOuterJoin("venda.localarmazenagem localarmazenagemV")
					.leftOuterJoin("producaoagenda.pedidovenda pedidovenda")
					.leftOuterJoin("pedidovenda.localarmazenagem localarmazenagemPV")
					.whereIn("producaoordem.cdproducaoordem", whereIn)
					.orderBy("listaProducaoetapaitemItem.ordem, listaProducaoetapaitem.ordem")
					.list();
	}

	/**
	 * Atualiza a ordem de produ��o anterior.
	 *
	 * @param producaoordem
	 * @param producaoordemanterior
	 * @author Rodrigo Freitas
	 * @since 08/07/2016
	 */
	public void updateProducaoordemanterior(Producaoordem producaoordem, Producaoordem producaoordemanterior) {
		getHibernateTemplate().bulkUpdate("update Producaoordem p set p.producaoordemanterior = ? where p = ?", new Object[]{producaoordemanterior, producaoordem});
	}
	
	public Producaoordem loadForGarantia(Producaoordem producaoordem){
		return query()
				.joinFetch("producaoordem.empresa empresa")
				.joinFetch("producaoordem.listaProducaoordemmaterial listaProducaoordemmaterial")
				.joinFetch("listaProducaoordemmaterial.producaoetapaitem producaoetapaitem")
				.joinFetch("producaoetapaitem.producaoetapanome producaoetapanome")
				.joinFetch("producaoetapaitem.producaoetapa producaoetapa")
				.joinFetch("listaProducaoordemmaterial.pneu pneu")
				.joinFetch("listaProducaoordemmaterial.garantiatipo garantiatipo")
				.leftOuterJoinFetch("listaProducaoordemmaterial.garantiatipopercentual garantiatipopercentual")
				.joinFetch("listaProducaoordemmaterial.pedidovendamaterial pedidovendamaterial")
				.joinFetch("pedidovendamaterial.pedidovenda pedidovenda")
				.leftOuterJoinFetch("pedidovenda.pedidovendatipo pedidovendatipo")
				.leftOuterJoinFetch("pedidovenda.prazopagamento prazopagamento")
				.join("pedidovenda.colaborador colaborador")
				.join("pedidovenda.cliente cliente")
				.leftOuterJoinFetch("pedidovenda.pedidovendaorigem pedidovendaorigem")
				.leftOuterJoinFetch("pedidovendaorigem.pedidovendatipo pedidovendatipo")
				.leftOuterJoinFetch("pedidovendaorigem.prazopagamento prazopagamento")
				.joinFetch("pedidovendamaterial.pedidovendamaterialgarantido pedidovendamaterialgarantido")
				.joinFetch("pedidovendamaterialgarantido.material materialgarantido")
				.leftOuterJoinFetch("materialgarantido.unidademedida unidademedida")
			    .leftOuterJoinFetch("producaoordem.listaProducaoordemconstatacao listaProducaoordemconstatacao")
			    .leftOuterJoinFetch("listaProducaoordemconstatacao.motivodevolucao motivodevolucao")
			    .where("producaoordem=?", producaoordem)
			    .unique();
	}

	public Producaoordem findByPneuAndEtapa(Pneu pneu, Producaoetapaitem ultimaEtapa) {
		return query()
				.select("producaoordem.cdproducaoordem,producaoordem.producaoordemsituacao," +
						"listaProducaoordemconstatacao.cdproducaoordemconstatacao,listaProducaoordemconstatacao.observacao," +
						"motivodevolucao.cdmotivodevolucao,motivodevolucao.constatacao,motivodevolucao.descricao,motivodevolucao.motivodevolucao")
				.leftOuterJoin("producaoordem.listaProducaoordemmaterial listaProducaoordemmaterial")
				.leftOuterJoin("producaoordem.listaProducaoordemconstatacao listaProducaoordemconstatacao")
				.leftOuterJoin("listaProducaoordemconstatacao.motivodevolucao motivodevolucao")
				.where("listaProducaoordemmaterial.pneu = ?",pneu)
				.where("listaProducaoordemmaterial.producaoetapaitem = ?",ultimaEtapa)
				.openParentheses()
					.where("producaoordem.producaoordemsituacao = ?",Producaoordemsituacao.CONCLUIDA)
						.or()
					.where("producaoordem.producaoordemsituacao = ?",Producaoordemsituacao.CANCELADA)
				.closeParentheses()
				.unique();
	}

	@SuppressWarnings("unchecked")
	public List<OrdemProducaoSped> findOrdensAvulsasForRegistroK230(SpedarquivoFiltro filtro) {
		
		List<Tipoitemsped> listaTipoitemsped = new ArrayList<Tipoitemsped>();
		listaTipoitemsped.add(Tipoitemsped.PRODUTO_PROCESSO);
		listaTipoitemsped.add(Tipoitemsped.PRODUTO_ACABADO);
		
		StringBuilder sql = new StringBuilder();
		sql.append("select po.cdproducaoordem, coalesce(po.dtproducaoordem, po.dtconclusaoordem) as dtproducaoordem, po.dtconclusaoordem, pom.cdmaterial, m.identificacao, po.producaoordemsituacao, ");
		sql.append("(select coalesce (sum (me.qtde), 0) from movimentacaoestoque me " +
				"join movimentacaoestoqueorigem meo on me.cdmovimentacaoestoqueorigem = meo.cdmovimentacaoestoqueorigem " +
				"where me.cdmaterial = pom.cdmaterial " +
				"and meo.cdproducaoordem = po.cdproducaoordem) as qtde ");
		sql.append("from producaoordem po ");
		sql.append("join producaoordemmaterial pom on pom.cdproducaoordem = po.cdproducaoordem ");
		sql.append("left outer join producaoordemmaterialorigem pomo on pomo.cdproducaoordemmaterial = pom.cdproducaoordemmaterial ");
		sql.append("left outer join material m on m.cdmaterial = pom.cdmaterial ");
		sql.append("where pomo.cdproducaoagenda is null ");
		sql.append("and (coalesce((po.dtproducaoordem >= '" + filtro.getDtinicio() +"' and po.dtproducaoordem <= '"+ filtro.getDtfim() + "'), ");
		sql.append("(po.dtconclusaoordem >= '" + filtro.getDtinicio() + "' and po.dtconclusaoordem <=  '"+ filtro.getDtfim() + "'))) ");
		sql.append("and po.producaoordemsituacao <> 1 ");
		sql.append("and po.cdempresa = " + filtro.getEmpresa().getCdpessoa());
		sql.append(" and m.tipoitemsped in (" + CollectionsUtil.listAndConcatenate(listaTipoitemsped, "id", ",") + ");");
		
		List<OrdemProducaoSped> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public OrdemProducaoSped mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new OrdemProducaoSped(rs.getInt("cdproducaoordem"), rs.getDate("dtproducaoordem"), rs.getDate("dtconclusaoordem"), rs.getInt("cdmaterial"), rs.getString("identificacao"), rs.getInt("producaoordemsituacao"), rs.getDouble("qtde"));
			}
		});
		
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	public List<OrdemProducaoSped> findOrdemOrigemAgendaForRegistroK230(SpedarquivoFiltro filtro, String whereIn) {
		
		List<Tipoitemsped> listaTipoitemsped = new ArrayList<Tipoitemsped>();
		listaTipoitemsped.add(Tipoitemsped.PRODUTO_PROCESSO);
		listaTipoitemsped.add(Tipoitemsped.PRODUTO_ACABADO);
		
		StringBuilder sql = new StringBuilder();
		sql.append("select pam.cdproducaoagenda, po.dtproducaoordem, po2.dtconclusaoordem, pam.cdmaterial, m.identificacao, pa.producaoagendasituacao, ");
		sql.append("(select coalesce (sum (me.qtde), 0) from movimentacaoestoque me " +
				"join movimentacaoestoqueorigem meo on me.cdmovimentacaoestoqueorigem = meo.cdmovimentacaoestoqueorigem " +
				"join producaoordemmaterial pom on pom.cdproducaoordemmaterial = meo.cdproducaoordemmaterial " +
				"where me.cdmaterial = pam.cdmaterial " +
				"and meo.cdproducaoordem = po2.cdproducaoordem) as qtde ");
		sql.append("from producaoagendamaterial pam ");
		sql.append("left outer join producaoagenda pa on pa.cdproducaoagenda = pam.cdproducaoagenda ");
		sql.append("left outer join producaoordem po on po.cdproducaoordem = pam.cdordemproducaoprimeiraetapa ");
		sql.append("left outer join producaoordem po2 on po2.cdproducaoordem = pam.cdordemproducaoultimaetapa ");
		sql.append("left outer join material m on m.cdmaterial = pam.cdmaterial ");
		
		sql.append("where ");
		if(!whereIn.isEmpty()){
			sql.append("(");
		}
		sql.append("((po.dtproducaoordem >= '" + filtro.getDtinicio() + "' and po.dtproducaoordem <= '" + filtro.getDtfim() + "') ");
		sql.append("or (po2.dtconclusaoordem >= '" + filtro.getDtinicio() + "' and po2.dtconclusaoordem <= '" + filtro.getDtfim() + "'))");
		
		if(!whereIn.isEmpty()){
			sql.append("or ((po.cdproducaoordem in (" + whereIn + ")) ");
			sql.append("or (po2.cdproducaoordem in (" + whereIn + ")))) ");			
		}
		sql.append("and po.producaoordemsituacao <> 3 ");
		sql.append("and po.cdempresa = " + filtro.getEmpresa().getCdpessoa());
		sql.append(" and m.tipoitemsped in (" + CollectionsUtil.listAndConcatenate(listaTipoitemsped, "id", ",") + ");");
		
		List<OrdemProducaoSped> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public OrdemProducaoSped mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new OrdemProducaoSped(rs.getInt("cdproducaoagenda"), rs.getDate("dtproducaoordem"), rs.getDate("dtconclusaoordem"), rs.getInt("cdmaterial"), rs.getString("identificacao"), rs.getInt("producaoagendasituacao"), rs.getDouble("qtde"));
			}
		});
		
		return lista;
	}

	public void updateDataConclusaoOrdemProducao(Producaoordem producaoordem, Date dtConclusao) {
		getHibernateTemplate().bulkUpdate("update Producaoordem p set p.dtconclusaoordem = ? where p = ?", new Object[]{dtConclusao, producaoordem});
	}
}
