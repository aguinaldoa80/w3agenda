package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Materialrequisicao;
import br.com.linkcom.sined.geral.bean.Requisicao;

public class MaterialrequisicaoDAO extends br.com.linkcom.sined.util.neo.persistence.GenericDAO<Materialrequisicao> {

	/**
	 * 
	 * M�todo que carrega a lista de MaterialRequisicao a partir de uma Requisicao.
	 *
	 * @name findListaMaterialRequisicaoByRequisicao
	 * @param requisicao
	 * @return
	 * @return List<Materialrequisicao>
	 * @author Thiago Augusto
	 * @date 31/05/2012
	 *
	 */
	public List<Materialrequisicao> findListaMaterialRequisicaoByRequisicao(Requisicao requisicao){
		return query()
					.select("materialrequisicao.cdmaterialrequisicao, materialrequisicao.quantidade, materialrequisicao.valortotal, " +
							"materialrequisicao.item, material.cdmaterial, material.nome, material.servico, materialrequisicao.observacao")
					.leftOuterJoin("materialrequisicao.requisicao requisicao")
					.leftOuterJoin("materialrequisicao.material material")
					.where("requisicao = ? ", requisicao)
					.list();
	}

	public void updateFaturadoByAutorizacao(String whereIn, String whereInMaterialrequisicao){
		if(whereIn != null && !whereIn.equals("") && StringUtils.isNotEmpty(whereInMaterialrequisicao))
			getJdbcTemplate().update("update Materialrequisicao mr set faturado = true where mr.cdmaterialrequisicao in (" + whereInMaterialrequisicao + ") and cdrequisicao in (" + whereIn + ")");
	}
	
	public void updateFaturadoByRequisicao(String whereIn){
		if(whereIn != null && !whereIn.equals(""))
			getJdbcTemplate().update("update Materialrequisicao mr set faturado = true where cdrequisicao in (" + whereIn + ")");
	}
	
	public void updateFaturado(String whereIn){
		if(whereIn != null && !whereIn.equals(""))
			getJdbcTemplate().update("update Materialrequisicao mr set faturado = true where cdmaterialrequisicao in (" + whereIn + ")");
	}
	
	public void updatePercentualFaturado(Materialrequisicao materialrequisicao, Double percentual){
		getHibernateTemplate().bulkUpdate("update Materialrequisicao m set m.percentualfaturado = ? where m.id = ?", new Object[]{
			percentual, materialrequisicao.getCdmaterialrequisicao()
		});
	}

	/**
	 * M�todo que atualiza o item da requisicao para n�o faturado
	 *
	 * @param whereIn
	 * @author Luiz Fernando
	 */
	public void updateMaterialrequisicaoFaturado(String whereIn) {
		if(whereIn != null && !"".equals(whereIn)){
			getJdbcTemplate().update("update Materialrequisicao mr set faturado = false where cdmaterialrequisicao in (" + whereIn + ")");
		}
	}
	
	/**
	 * M�todo que atualiza o item da requisicao com o id do item da nota de servico
	 *
	 * @param cdnotafiscalservicoitem
	 * @param cdmaterialrequisicao
	 * @author Luiz Fernando
	 */
	public void updateItemMaterialrequisicaoByItemNota(Integer cdnotafiscalservicoitem, Integer cdmaterialrequisicao) {
		if(cdnotafiscalservicoitem != null && cdmaterialrequisicao != null){
			getJdbcTemplate().update("update Materialrequisicao mr set cdnotafiscalservicoitem = " + cdnotafiscalservicoitem + " where cdmaterialrequisicao = " + cdmaterialrequisicao );
		}
	}
}