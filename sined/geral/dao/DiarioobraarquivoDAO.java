package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Diarioobraarquivo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DiarioobraarquivoDAO extends GenericDAO<Diarioobraarquivo>{
	
	/* singleton */
	private static DiarioobraarquivoDAO instance;
	public static DiarioobraarquivoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(DiarioobraarquivoDAO.class);
		}
		return instance;
	}

}
