package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Tarefaorcamentorh;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TarefaorcamentorhDAO extends GenericDAO<Tarefaorcamentorh>{

	public List<Tarefaorcamentorh> findByOrcamento(Orcamento orcamento) {
		return query()
					.select("tarefaorcamentorh.cdtarefaorcamentorh, cargo.cdcargo, cargo.custohora, " +
							"tarefaorcamento.cdtarefaorcamento")
					.leftOuterJoin("tarefaorcamentorh.cargo cargo")
					.join("tarefaorcamentorh.tarefaorcamento tarefaorcamento")
					.join("tarefaorcamento.orcamento orcamento")
					.where("orcamento = ?", orcamento)
					.list();
	}
}
