package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Diarioobra;
import br.com.linkcom.sined.geral.service.DiarioobraarquivoService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.DiarioObraFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

/**
 * @author Andre Brunelli
 */
public class DiarioobraDAO extends GenericDAO<Diarioobra> {
	
	protected DiarioobraarquivoService diarioobraarquivoService;
	
	public void setDiarioobraarquivoService(
			DiarioobraarquivoService diarioobraarquivoService) {
		this.diarioobraarquivoService = diarioobraarquivoService;
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Diarioobra diarioobra = (Diarioobra) save.getEntity();
		diarioobraarquivoService.saveArquivos(diarioobra);
		
		save.saveOrUpdateManaged("listaDiarioobraarquivo");
		save.saveOrUpdateManaged("listaMaodeobra");
		save.saveOrUpdateManaged("listMaterial");
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Diarioobra> query,	FiltroListagem _filtro) {
		DiarioObraFiltro filtro = (DiarioObraFiltro) _filtro;
		query
			.select("diarioobra.cddiarioobra, diarioobra.dtdia, diarioobra.dtprazocontratual, diarioobra.diasdecorridos, " +
					"diarioobra.diasrestantes, projeto.nome, diarioobra.dtprorrogacao, diarioobra.diasatraso") 
			.join("diarioobra.projeto projeto")
			.where("projeto = ?", filtro.getProjeto())
			.where("diarioobra.dtdia = ?", filtro.getDtdia())
			.orderBy("diarioobra.dtdia");
	}

	/**
	 * Carrega s lista de di�rio de obra para a emiss�o do relat�rio.
	 *
	 * @param diarioobra
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Diarioobra findForReport(Diarioobra diarioobra) {
		if(diarioobra == null || diarioobra.getCddiarioobra() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("diarioobra.cddiarioobra, diarioobra.dtdia, diarioobra.dtprazocontratual, diarioobra.diasdecorridos, " +
					"diarioobra.diasrestantes, projeto.cdprojeto, projeto.nome, diarioobra.dtprorrogacao, diarioobra.diasatraso, diarioobra.tempomanha, " +
					"diarioobra.tempotarde, diarioobra.temponoite, maodeobra.cddiarioobramdo, maodeobra.quantidade, maodeobra.totalhora, " +
					"cargo.cdcargo, cargo.nome, tipocargo.cdtipocargo")
					.join("diarioobra.projeto projeto")
					.join("diarioobra.listaMaodeobra maodeobra")
					.join("maodeobra.cargo cargo")
					.join("cargo.tipocargo tipocargo")
					.where("diarioobra = ?", diarioobra)
					.orderBy("diarioobra.dtdia")
					.unique();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Diarioobra> query) {
		query
			.select("diarioobra.cddiarioobra, diarioobra.dtprazocontratual, diarioobra.diasdecorridos, diarioobra.diasrestantes, " +
					"diarioobra.dtdia, diarioobra.tempomanha, diarioobra.tempotarde, diarioobra.dtaltera, diarioobra.temponoite, diarioobra.diasatraso, " +
					"diarioobra.dtprorrogacao, diarioobra.hrinicioparalizacaomanha, diarioobra.hrinicioparalizacaotarde, " +
					"diarioobra.hrinicioparalizacaonoite, diarioobra.hrfimparalizacaomanha, diarioobra.hrfimparalizacaotarde, " +
					"diarioobra.hrfimparalizacaonoite, projeto.cdprojeto, projeto.nome, maodeobra.cddiarioobramdo, maodeobra.quantidade, " +
					"maodeobra.totalhora, cargo.cdcargo, cargo.nome, tipocargo.cdtipocargo, diarioobramaterial.cddiarioobramaterial, " +
					"diarioobramaterial.quantidade, diarioobramaterial.totalhora, material.cdmaterial, material.nome, " +
					"listaDiarioobraarquivo.cddiarioobraarquivo, listaDiarioobraarquivo.descricao, arquivo.cdarquivo, arquivo.nome")
			.leftOuterJoin("diarioobra.listaMaodeobra maodeobra")
			.leftOuterJoin("diarioobra.projeto projeto")
			.leftOuterJoin("diarioobra.listMaterial diarioobramaterial")
			.leftOuterJoin("diarioobramaterial.material material")
			.leftOuterJoin("maodeobra.cargo cargo")
			.leftOuterJoin("cargo.tipocargo tipocargo")
			.leftOuterJoin("diarioobra.listaDiarioobraarquivo listaDiarioobraarquivo")
			.leftOuterJoin("listaDiarioobraarquivo.arquivo arquivo");
			
	}

	/**
	 * Busca a lista de di�rio de obra para o relat�rio.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/01/2014
	 */
	public List<Diarioobra> findForReport(String whereIn) {
		QueryBuilder<Diarioobra> query = querySined();
		this.updateEntradaQuery(query);
		return query
				.whereIn("diarioobra.cddiarioobra", whereIn)
				.list()	;
	}
	
}
