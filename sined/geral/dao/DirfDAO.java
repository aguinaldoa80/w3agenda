package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Dirf;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("dirf.cddirf")
public class DirfDAO extends GenericDAO<Dirf> {

}
