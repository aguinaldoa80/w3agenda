package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Colaboradorcargorisco;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColaboradorcargoriscoDAO extends GenericDAO<Colaboradorcargorisco> {

	/* singleton */
	private static ColaboradorcargoriscoDAO instance;
	public static ColaboradorcargoriscoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradorcargoriscoDAO.class);
		}
		return instance;
	}
}