package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Grauinstrucao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;


public class GrauinstrucaoDAO extends GenericDAO<Grauinstrucao> {
	
	/* singleton */
	private static GrauinstrucaoDAO instance;
	public static GrauinstrucaoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(GrauinstrucaoDAO.class);
		}
		return instance;
	}
		
}
