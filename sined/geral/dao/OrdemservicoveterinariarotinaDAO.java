package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Ordemservicoveterinaria;
import br.com.linkcom.sined.geral.bean.Ordemservicoveterinariarotina;
import br.com.linkcom.sined.util.IteracaoVeterinariaRetorno;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OrdemservicoveterinariarotinaDAO extends GenericDAO<Ordemservicoveterinariarotina> {
	
	public List<Ordemservicoveterinariarotina> findByOrdemservicoveterinaria(Ordemservicoveterinaria ordemservicoveterinaria){
		return query()
				.select("ordemservicoveterinariarotina.cdordemservicoveterinariarotina, ordemservicoveterinariarotina.acada, ordemservicoveterinariarotina.dtinicio," +
						"ordemservicoveterinariarotina.hrinicio, ordemservicoveterinariarotina.prazotermino, ordemservicoveterinariarotina.quantidadeiteracao," +
						"ordemservicoveterinariarotina.dtproximaiteracao, ordemservicoveterinariarotina.hrproximaiteracao, ordemservicoveterinariarotina.dtfimprevisaoiteracao," +
						"ordemservicoveterinariarotina.hrfimprevisaoiteracao, ordemservicoveterinariarotina.dtfim, ordemservicoveterinariarotina.observacao," +
						"atividaderotina.cdatividaderotina, atividaderotina.nome," +
						"frequencia.cdfrequencia, frequencia.nome")
				.join("ordemservicoveterinariarotina.atividaderotina atividaderotina")
				.leftOuterJoin("ordemservicoveterinariarotina.frequencia frequencia")
				.where("ordemservicoveterinariarotina.ordemservicoveterinaria=?", ordemservicoveterinaria)
				.orderBy("atividaderotina.nome")
				.list();
	}
	
	/**
	 * 
	 * @author Thiago Clemente
	 * 
	 */
	public List<Ordemservicoveterinariarotina> findForIteracao(){
		return query()
				.select("ordemservicoveterinariarotina.cdordemservicoveterinariarotina," +
						"ordemservicoveterinariarotina.dtinicio, ordemservicoveterinariarotina.hrinicio," +
						"ordemservicoveterinariarotina.prazotermino, ordemservicoveterinariarotina.quantidadeiteracao," +
						"ordemservicoveterinariarotina.dtproximaiteracao, ordemservicoveterinariarotina.hrproximaiteracao," +
						"ordemservicoveterinariarotina.dtfimprevisaoiteracao, ordemservicoveterinariarotina.hrfimprevisaoiteracao," +
						"ordemservicoveterinariarotina.dtfim, ordemservicoveterinariarotina.acada," +
						"frequencia.cdfrequencia, listaAplicacao.dtprevisto, listaAplicacao.hrprevisto")
				.leftOuterJoin("ordemservicoveterinariarotina.frequencia frequencia")
				.leftOuterJoin("ordemservicoveterinariarotina.listaAplicacao listaAplicacao")
				.where("ordemservicoveterinariarotina.dtfim is null")
				.list();
	}
	
	public void updateIteracao(Ordemservicoveterinariarotina ordemservicoveterinariarotina, IteracaoVeterinariaRetorno retorno){
		Timestamp hrproximaiteracao = null;
		if (retorno.getHrproximaiteracao()!=null){
			hrproximaiteracao = new Timestamp(retorno.getHrproximaiteracao().getTime());
		}
		
		Timestamp hrfimprevisaoiteracao = null;
		if (retorno.getHrfimprevisaoiteracao()!=null){
			hrfimprevisaoiteracao = new Timestamp(retorno.getHrfimprevisaoiteracao().getTime());
		}
		
		getJdbcTemplate().update("update ordemservicoveterinariarotina set dtproximaiteracao=?, hrproximaiteracao=?, dtfimprevisaoiteracao=?, hrfimprevisaoiteracao=? where cdordemservicoveterinariarotina=?", new Object[]{retorno.getDtproximaiteracao(), hrproximaiteracao, retorno.getDtfimprevisaoiteracao(), hrfimprevisaoiteracao, ordemservicoveterinariarotina.getCdordemservicoveterinariarotina()});
		
		if (retorno.getDtfim()==null){
			getJdbcTemplate().update("update ordemservicoveterinariarotina o set dtfim=null where cdordemservicoveterinariarotina=? and dtfim is not null", new Object[]{ordemservicoveterinariarotina.getCdordemservicoveterinariarotina()});
		}else {
			getJdbcTemplate().update("update ordemservicoveterinariarotina o set dtfim=? where cdordemservicoveterinariarotina=? and dtfim is null", new Object[]{retorno.getDtfim(), ordemservicoveterinariarotina.getCdordemservicoveterinariarotina()});
		}
	}	
}