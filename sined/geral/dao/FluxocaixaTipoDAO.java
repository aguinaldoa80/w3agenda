package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.FluxocaixaTipo;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.FluxocaixaTipoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("fluxocaixaTipo.nome")
public class FluxocaixaTipoDAO extends GenericDAO<FluxocaixaTipo>{

	@Override
	public void updateListagemQuery(QueryBuilder<FluxocaixaTipo> query,FiltroListagem _filtro) {
		FluxocaixaTipoFiltro filtro = (FluxocaixaTipoFiltro) _filtro;
		
		query
			.select("fluxocaixaTipo.cdfluxocaixatipo,fluxocaixaTipo.nome,fluxocaixaTipo.sigla")
			.whereLikeIgnoreAll("fluxocaixaTipo.nome", filtro.getDescricao())
			.whereLikeIgnoreAll("fluxocaixaTipo.sigla", filtro.getSigla())
		;
	}

	/**
	 * M�todo que busca fluxo de caixa do tipo 'Ordem de compra'.
	 * @return
	 * @author Tom�s Rabelo
	 */
	public FluxocaixaTipo getFluxoCaixaTipoOrdemCompra() {
		return query()
			.select("fluxocaixaTipo.cdfluxocaixatipo,fluxocaixaTipo.nome,fluxocaixaTipo.sigla")
			.where("fluxocaixaTipo.sigla = 'OC'")
//			.whereLikeIgnoreAll("fluxocaixaTipo.nome", "Ordem de compra")
			.unique();
	}

	/**
	 * M�todo que busca fluxo de caixa do tipo 'Contrato de fornecimento'.
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public FluxocaixaTipo getFluxoCaixaTipoFornecimentoContrato() {
		return query()
			.select("fluxocaixaTipo.cdfluxocaixatipo,fluxocaixaTipo.nome,fluxocaixaTipo.sigla")
			.where("fluxocaixaTipo.sigla = 'CF'")
	//		.whereLikeIgnoreAll("fluxocaixaTipo.nome", "Contrato de fornecimento")
			.unique();
	}
}
