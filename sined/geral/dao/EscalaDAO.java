package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Escala;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.EscalaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("escala.nome")
public class EscalaDAO extends GenericDAO<Escala> {

	@Override
	public void updateListagemQuery(QueryBuilder<Escala> query,FiltroListagem _filtro) {
		
		EscalaFiltro filtro = (EscalaFiltro) _filtro;
		
		query
			.select("escala.cdescala, escala.nome, escala.diastrabalho, escala.diasfolga, escala.ativo")
			.whereLikeIgnoreAll("escala.nome", filtro.getNome())
			.where("escala.ativo = ?", filtro.getMostrar());
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaescalahorario");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Escala> query) {
		query
			.select("escala.cdescala, escala.nome, escala.diastrabalho, escala.diasfolga, escala.naotrabalhadomingo, escala.horariotodosdias, " +
					"escala.naotrabalhaferiado, escala.observacao, escala.ativo, escala.cdusuarioaltera, escala.dtaltera, " +
					"listaescalahorario.cdescalahorario, listaescalahorario.horainicio, listaescalahorario.horafim, listaescalahorario.diasemana")
			.leftOuterJoin("escala.listaescalahorario listaescalahorario")
			.orderBy("listaescalahorario.diasemana, listaescalahorario.horainicio, listaescalahorario.horafim");
	}

	/**
	 * M�todo que retorna a escala de hor�rio de um determinado veiculo
	 * 
	 * @param veiculo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Escala findEscalaHorarioDoVeiculo(Veiculo veiculo) {
		return query()
			.select("escala.cdescala, escala.nome, escala.diastrabalho, escala.diasfolga, escala.naotrabalhadomingo, escala.horariotodosdias, " +
					"escala.naotrabalhaferiado, escalahorario.cdescalahorario, escalahorario.horainicio, escalahorario.horafim, escalahorario.diasemana")
			.join("escala.listaescalahorario escalahorario")
			.where("escala.id = (select v.escala.id from Veiculo v where v.id = "+veiculo.getCdveiculo()+")")
			.orderBy("escalahorario.diasemana, escalahorario.horainicio")
			.unique();
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param pessoa
	 * @return
	 * @author Tom�s Rabelo
	 *//*
	public Escala findEscalaDoColaborador(Colaborador colaborador, Date dtinicio, Date dtfim) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return query()
		.select("escala.cdescala, escala.nome, escala.diastrabalho, escala.diasfolga, escala.naotrabalhadomingo, escala.horariotodosdias, " +
				"escala.naotrabalhaferiado, escalahorario.cdescalahorario, escalahorario.horainicio, escalahorario.horafim, escalahorario.diasemana")
		.join("escala.listaescalahorario escalahorario")
		.where("escala.id in (select ee.id from Escalacolaborador ec join ec.escala ee join ec.colaborador ecc where ecc.id = "+colaborador.getCdpessoa()+" and " +
							"((ec.dtinicio >= to_date('" + sdf.format(dtinicio) + "', 'dd/mm/yyyy') and ec.dtfim <= to_date('" + sdf.format(dtfim) + "', 'dd/mm/yyyy')) or "+
							" (ec.dtinicio >= to_date('" + sdf.format(dtinicio) + "', 'dd/mm/yyyy') and ec.dtfim is null)))")
		.orderBy("escalahorario.horainicio, escalahorario.diasemana")
		.list().get(0);
	}*/

	/**
	 * M�todo que carrega a escala
	 * 
	 * @param escala
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Escala carregaEscala(Escala escala) {
		return query()
			.select("escala.cdescala, escala.nome, escala.diastrabalho, escala.diasfolga, escala.naotrabalhadomingo, escala.horariotodosdias, " +
					"escala.naotrabalhaferiado, escalahorario.cdescalahorario, escalahorario.horainicio, escalahorario.horafim, escalahorario.diasemana")
			.join("escala.listaescalahorario escalahorario")
			.where("escala = ?", escala)
			.orderBy("escalahorario.horainicio, escalahorario.diasemana")
			.unique();
	}

	/**
	 * M�todo que acha as escalas do colaborador
	 * 
	 * @param colaborador
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Escala> findByColaborador(Colaborador colaborador) {
		if(colaborador == null || colaborador.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("escala.cdescala, escala.nome")
			.where("escala.id in (select ece.id from Escalacolaborador ec join ec.escala ece join ec.colaborador ecc where ecc = ?)", colaborador)
			.list();
	}
	
	public List<Escala> carregaListaEscala(String whereIn) {
		return query()
			.select("escala.cdescala, escala.nome, escala.diastrabalho, escala.diasfolga, escala.naotrabalhadomingo, escala.horariotodosdias, " +
					"escala.naotrabalhaferiado, escalahorario.cdescalahorario, escalahorario.horainicio, escalahorario.horafim, escalahorario.diasemana")
			.join("escala.listaescalahorario escalahorario")
			.whereIn("escala.cdescala", whereIn, false)
			.orderBy("escalahorario.horainicio, escalahorario.diasemana")
			.list();
	}	
}
