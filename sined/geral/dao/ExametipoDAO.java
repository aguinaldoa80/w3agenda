package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Exametipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ExametipoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ExametipoDAO extends GenericDAO<Exametipo> {
	
	/**
	 * M�todo para localizar todos os dados de exametipo
	 * 
	 * @param cdexametipo
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public Exametipo findExameTipoByPk(Integer cdexametipo){
		if(cdexametipo == null)
			throw new SinedException("O par�metro cdexametipo n�o deve ser null.");
		
		return query()
			.select("exametipo.cdexametipo, exametipo.nome")
			.where("exametipo.cdexametipo = ?", cdexametipo)
			.unique();
	}
	/**
	 * M�todo para localizar todos os dados de exametipo
	 * 
	 * @param List<Exametipo>
	 * @return
	 * @author Orestes
	 */
	public List<Exametipo> findAll(List<Exametipo> lista){
		return query()
			.select("exametipo.cdexametipo, exametipo.nome")
			.whereIn("exametipo.cdexametipo", CollectionsUtil.listAndConcatenate(lista, "cdexametipo", ","))
			.list();
	}
	
	/* singleton */
	private static ExametipoDAO instance;
	public static ExametipoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ExametipoDAO.class);
		}
		return instance;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Exametipo> query,
			FiltroListagem _filtro) {
		
		ExametipoFiltro filtro = (ExametipoFiltro)_filtro;
		
		query
			.select("exametipo.cdexametipo, exametipo.nome")
			.whereLikeIgnoreAll("exametipo.nome",filtro.getNome());
	}

}