package br.com.linkcom.sined.geral.dao;

import java.util.Collections;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Orcamentovalorcampoextra;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OrcamentovalorcampoextraDAO extends GenericDAO<Orcamentovalorcampoextra>{

	/**
	 * Busca todos os campos extras de um determinado {@link Vendaorcamento}
	 * @param pedidovendatipo
	 * @return
	 */
	public List<Orcamentovalorcampoextra> findByVendaorcamento(Vendaorcamento bean) {

		if(bean == null  || bean.getCdvendaorcamento() == null){
			return Collections.emptyList();
		}
		
		return query()
	   		 .select("orcamentovalorcampoextra.cdorcamentovalorcampoextra, orcamentovalorcampoextra.valor," +
	   		 		"campoextrapedidovendatipo.cdcampoextrapedidovendatipo, campoextrapedidovendatipo.nome, " +
	   		 		"campoextrapedidovendatipo.ordem, campoextrapedidovendatipo.obrigatorio")
	   		 		.join("orcamentovalorcampoextra.campoextrapedidovendatipo campoextrapedidovendatipo")
	   		 		.where("orcamentovalorcampoextra.vendaorcamento = ?", bean)
	   		 		.orderBy("campoextrapedidovendatipo.ordem")
	   		 		.list();
	}
	
}
