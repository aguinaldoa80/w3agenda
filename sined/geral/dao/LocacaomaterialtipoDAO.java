package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Locacaomaterialtipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.LocacaomaterialtipoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("locacaomaterialtipo.nome")
public class LocacaomaterialtipoDAO extends GenericDAO<Locacaomaterialtipo> {

	@Override
	public void updateListagemQuery(QueryBuilder<Locacaomaterialtipo> query, FiltroListagem _filtro) {
		LocacaomaterialtipoFiltro filtro = (LocacaomaterialtipoFiltro) _filtro;
		query
			.select("locacaomaterialtipo.cdlocacaomaterialtipo, locacaomaterialtipo.nome, locacaomaterialtipo.ativo")
			.where("locacaomaterialtipo.ativo = ?", filtro.getAtivo())
			.whereLikeIgnoreAll("locacaomaterialtipo.nome", filtro.getNome())
			.orderBy("locacaomaterialtipo.nome");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Locacaomaterialtipo> query) {
		query
			.select("locacaomaterialtipo.cdlocacaomaterialtipo, locacaomaterialtipo.nome, locacaomaterialtipo.cdusuarioaltera, " +
				"locacaomaterialtipo.dtaltera, locacaomaterialtipo.ativo");
	}
	
}
