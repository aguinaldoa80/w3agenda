package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Comissionamentodesempenhoconsiderar;
import br.com.linkcom.sined.geral.bean.enumeration.Documentocomissaotipoperiodo;
import br.com.linkcom.sined.geral.bean.view.Vwdocumentocomissaodesempenho;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.VwdocumentocomissaodesempenhoFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.bean.ComissionamentodesempenhoReportBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VwdocumentocomissaodesempenhoDAO extends GenericDAO<Vwdocumentocomissaodesempenho>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Vwdocumentocomissaodesempenho> query, FiltroListagem _filtro) {
		VwdocumentocomissaodesempenhoFiltro filtro = (VwdocumentocomissaodesempenhoFiltro) _filtro;
		
		query
			.select("vwdocumentocomissaodesempenho.cddocumento, vwdocumentocomissaodesempenho.valordocumento, " +
					"vwdocumentocomissaodesempenho.valoratualdocumento, " +
					"vwdocumentocomissaodesempenho.dtvencimento, vwdocumentocomissaodesempenho.cddocumentoorigem, " +
					"vwdocumentocomissaodesempenho.cdnotadocumento, vwdocumentocomissaodesempenho.cdvenda, " +
					"vwdocumentocomissaodesempenho.cdcolaborador, vwdocumentocomissaodesempenho.nomecolaborador, " +
					"vwdocumentocomissaodesempenho.cdcomissionamento, vwdocumentocomissaodesempenho.cdpessoa, " +
					"vwdocumentocomissaodesempenho.cdcolaboradorcomissao, vwdocumentocomissaodesempenho.cdcontrato," +
					"vwdocumentocomissaodesempenho.cddocumentoacao, vwdocumentocomissaodesempenho.saldofinal," +
					"vwdocumentocomissaodesempenho.valortotalcusto, vwdocumentocomissaodesempenho.fromVenda ");
//			.where("dtvencimento >= ? ", filtro.getDtinicio())
//			.where("dtvencimento <= ?", filtro.getDtfim());
		if(filtro.getComissionamento() != null && filtro.getComissionamento().getCdcomissionamento() != null){
			query.where("vwdocumentocomissaodesempenho.cdcomissionamento = ?", filtro.getComissionamento().getCdcomissionamento());
		}
		if(filtro.getConsiderar() != null && Comissionamentodesempenhoconsiderar.SALDO_VENDA.equals(filtro.getConsiderar())){
			query
				.where("vwdocumentocomissaodesempenho.cdcontrato is null")
				.where("vwdocumentocomissaodesempenho.saldofinal is not null")
				.where("vwdocumentocomissaodesempenho.saldofinal > 0");
		}
		
		if(filtro.getConsiderar() != null && Comissionamentodesempenhoconsiderar.TOTAL_VENDA.equals(filtro.getConsiderar())){
			query.where("vwdocumentocomissaodesempenho.fromVenda = true");
			query.where("vwdocumentocomissaodesempenho.cdvenda is not null");
			
			if(filtro.getDtinicio() != null && filtro.getDtfim() != null){
				query.where("vwdocumentocomissaodesempenho.dtvenda >= ?", filtro.getDtinicio());
				query.where("vwdocumentocomissaodesempenho.dtvenda <= ?", filtro.getDtfim());
			}
		}else {
			query.where("vwdocumentocomissaodesempenho.fromVenda = false");
			if(filtro.getDtinicio() != null && filtro.getDtfim() != null){
				List<Movimentacaoacao> listaMovimentacaoacao = new ArrayList<Movimentacaoacao>();
				listaMovimentacaoacao.add(Movimentacaoacao.NORMAL);
				listaMovimentacaoacao.add(Movimentacaoacao.CONCILIADA);
				
				query.openParentheses();
				query.openParentheses();
//					query.where("vwdocumentocomissaodesempenho.cdvenda is null");
					query.openParentheses();
						query
							.openParentheses();
							if(Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
								query
								.where("vwdocumentocomissaodesempenho.dtvencimento >= ?", filtro.getDtinicio())
								.where("vwdocumentocomissaodesempenho.dtvencimento <= ?", filtro.getDtfim());
							}else if(Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo())){
								query
								.where("vwdocumentocomissaodesempenho.dtemissao >= ?", filtro.getDtinicio())
								.where("vwdocumentocomissaodesempenho.dtemissao <= ?", filtro.getDtfim());
							}else if(Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
								
								query.where("exists (select d.cddocumento " +
										"from Movimentacaoorigem mo " +
										"join mo.documento d " +
										"join mo.movimentacao m " +
										"join m.movimentacaoacao ma " +
										"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
										"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
										"and d.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
										"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(listaMovimentacaoacao, "cdmovimentacaoacao", ",") + ") " +
										")");
							}else {
								query.where("1=0");
							}
							query.closeParentheses();
						query.or();
						query.where("exists (select v.cddocumento " +
												"from Vdocumentonegociado v " +
												"where v.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
												(Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and v.dtvencimentonegociado >= '" + filtro.getDtinicio() + "' " +
													"and v.dtvencimentonegociado <= '" + filtro.getDtfim() + "' " : "") +
												(Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
														"and v.dtemissaonegociado >= '" + filtro.getDtinicio() + "' " +
														"and v.dtemissaonegociado <= '" + filtro.getDtfim() + "' " : "") +
												(Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
														"and exists (select d.cddocumento " +
																"from Movimentacaoorigem mo " +
																"join mo.documento d " +
																"join mo.movimentacao m " +
																"join m.movimentacaoacao ma " +
																"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
																"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
																"and d.cddocumento = v.cddocumento " +
																"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(listaMovimentacaoacao, "cdmovimentacaoacao", ",") + ") " +
																")" : "") +
												(!Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) &&
												 !Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo()) &&
												 !Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
														"and 1 = 0" : "") +
												")");
					query.closeParentheses();
				query.closeParentheses();
				
				if((filtro.getConsiderardtvenda() != null && filtro.getConsiderardtvenda()) || filtro.getDocumentocomissaotipoperiodo() == null){
					query.or();
					
					query.openParentheses();
						query.where("vwdocumentocomissaodesempenho.cdvenda is not null");
						query.where("vwdocumentocomissaodesempenho.dtvenda >= ?", filtro.getDtinicio());
						query.where("vwdocumentocomissaodesempenho.dtvenda <= ?", filtro.getDtfim());
					query.closeParentheses();
				}
				query.closeParentheses();
			}
		}
		
		if(filtro.getColaborador() != null && filtro.getColaborador().getCdpessoa() != null)
			query.where("cdcolaborador = ?", filtro.getColaborador().getCdpessoa());
		else{
			query.where("1=0");
		}
		
		if(filtro.getMostrar() != null && filtro.getMostrar().equals("Pagos com pend�ncia de repasse")){
			List<Documentoacao> lista = new ArrayList<Documentoacao>();
			lista.add(Documentoacao.BAIXADA);
			lista.add(Documentoacao.NEGOCIADA);
			
			query.where("not exists (select v.cddocumento " +
									"from Vdocumentonegociado v " +
									"where v.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
									"and v.cddocumentoacaonegociado not in(" + CollectionsUtil.listAndConcatenate(lista, "cddocumentoacao", ",") + "))");
			
			query.openParentheses();
			for (Documentoacao documentoacao : lista) {
				query.where("vwdocumentocomissaodesempenho.cddocumentoacao = ?", documentoacao.getCddocumentoacao()).or();
			}
			query.closeParentheses();
			
			query.where("vwdocumentocomissaodesempenho.cdcolaboradorcomissao is null");
			
//			query
//				.where("vwdocumentocomissaodesempenho.cddocumentoacao = ?", Documentoacao.BAIXADA.getCddocumentoacao())
//				.where("vwdocumentocomissaodesempenho.cdcolaboradorcomissao is null");
		}
		
		if(filtro.getConsiderar() != null && Comissionamentodesempenhoconsiderar.TOTAL_VENDA.equals(filtro.getConsiderar())){
			query.orderBy("vwdocumentocomissaodesempenho.dtvenda desc");
		}
	}

	/**
	 * M�todo que c�lcula o total dos documentos de acordo com o filtro
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Money findForTotalcomissionamentodesempenho(VwdocumentocomissaodesempenhoFiltro filtro) {
		if(filtro == null || filtro.getColaborador() == null || filtro.getColaborador().getCdpessoa() == null || 
				filtro.getDtinicio() == null || filtro.getDtfim() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Long> query = newQueryBuilder(Long.class);
		
		query
			.from(Vwdocumentocomissaodesempenho.class);
		
		if(filtro.getConsiderar() != null && Comissionamentodesempenhoconsiderar.SALDO_VENDA.equals(filtro.getConsiderar())){
			query.select("SUM(vwdocumentocomissaodesempenho.saldofinal)");
		}else if(filtro.getConsiderar() != null && Comissionamentodesempenhoconsiderar.MARKUP_VENDA.equals(filtro.getConsiderar())){
			query.select("SUM(vwdocumentocomissaodesempenho.valordocumento - vwdocumentocomissaodesempenho.valortotalcusto)");
		}else {
//			query.select("SUM(vwdocumentocomissaodesempenho.valordocumento)");
			query.select("SUM(vwdocumentocomissaodesempenho.valoratualdocumento)");
		}
		
		query		
//			.where("dtvencimento >= ? ", filtro.getDtinicio())
//			.where("dtvencimento <= ?", filtro.getDtfim())
			.where("cdpessoa = ?", filtro.getColaborador().getCdpessoa())
			.where("fromVenda <> true ");
		
		if(filtro.getComissionamento() != null && filtro.getComissionamento().getCdcomissionamento() != null){
			query.where("vwdocumentocomissaodesempenho.cdcomissionamento = ?", filtro.getComissionamento().getCdcomissionamento());
		}
		
		if(filtro.getConsiderar() != null && Comissionamentodesempenhoconsiderar.SALDO_VENDA.equals(filtro.getConsiderar())){
			query
				.where("vwdocumentocomissaodesempenho.cdcontrato is null")
				.where("vwdocumentocomissaodesempenho.saldofinal is not null")
				.where("vwdocumentocomissaodesempenho.saldofinal > 0");
		}
		
		if(filtro.getDtinicio() != null && filtro.getDtfim() != null){
			List<Movimentacaoacao> listaMovimentacaoacao = new ArrayList<Movimentacaoacao>();
			listaMovimentacaoacao.add(Movimentacaoacao.NORMAL);
			listaMovimentacaoacao.add(Movimentacaoacao.CONCILIADA);
			
			query.openParentheses();
			query.openParentheses();
//				query.where("vwdocumentocomissaodesempenho.cdvenda is null");
				query.openParentheses();
				query
					.openParentheses();
						if(Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
							query
							.where("vwdocumentocomissaodesempenho.dtvencimento >= ?", filtro.getDtinicio())
							.where("vwdocumentocomissaodesempenho.dtvencimento <= ?", filtro.getDtfim());
						}else if(Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo())){
							query
							.where("vwdocumentocomissaodesempenho.dtemissao >= ?", filtro.getDtinicio())
							.where("vwdocumentocomissaodesempenho.dtemissao <= ?", filtro.getDtfim());
						}else if(Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
							
							query.where("exists (select d.cddocumento " +
									"from Movimentacaoorigem mo " +
									"join mo.documento d " +
									"join mo.movimentacao m " +
									"join m.movimentacaoacao ma " +
									"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
									"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
									"and d.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
									"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(listaMovimentacaoacao, "cdmovimentacaoacao", ",") + ") " +
									")");
						}else {
							query.where("1=0");
						}
						query.closeParentheses();
					query.or();
					query.where("exists (select v.cddocumento " +
											"from Vdocumentonegociado v " +
											"where v.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
											(Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
												"and v.dtvencimentonegociado >= '" + filtro.getDtinicio() + "' " +
												"and v.dtvencimentonegociado <= '" + filtro.getDtfim() + "' " : "") +
											(Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and v.dtemissaonegociado >= '" + filtro.getDtinicio() + "' " +
													"and v.dtemissaonegociado <= '" + filtro.getDtfim() + "' " : "") +
											(Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and exists (select d.cddocumento " +
															"from Movimentacaoorigem mo " +
															"join mo.documento d " +
															"join mo.movimentacao m " +
															"join m.movimentacaoacao ma " +
															"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
															"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
															"and d.cddocumento = v.cddocumento " +
															"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(listaMovimentacaoacao, "cdmovimentacaoacao", ",") + ") " +
															")" : "") +
											(!Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) &&
											 !Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo()) &&
											 !Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and 1 = 0" : "") +
											")");
				query.closeParentheses();
			query.closeParentheses();
			
			if((filtro.getConsiderardtvenda() != null && filtro.getConsiderardtvenda()) || filtro.getDocumentocomissaotipoperiodo() == null){
				query.or();
				
				query.openParentheses();
					query.where("vwdocumentocomissaodesempenho.cdvenda is not null");
					query.where("vwdocumentocomissaodesempenho.dtvenda >= ?", filtro.getDtinicio());
					query.where("vwdocumentocomissaodesempenho.dtvenda <= ?", filtro.getDtfim());
				query.closeParentheses();
			}
			query.closeParentheses();
		}
		
		
		if(filtro.getMostrar() != null && filtro.getMostrar().equals("Pagos com pend�ncia de repasse")){
			List<Documentoacao> lista = new ArrayList<Documentoacao>();
			lista.add(Documentoacao.BAIXADA);
			lista.add(Documentoacao.NEGOCIADA);
			
			query.where("not exists (select v.cddocumento " +
									"from Vdocumentonegociado v " +
									"where v.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
									"and v.cddocumentoacaonegociado not in(" + CollectionsUtil.listAndConcatenate(lista, "cddocumentoacao", ",") + "))");
			
			query.openParentheses();
			for (Documentoacao documentoacao : lista) {
				query.where("vwdocumentocomissaodesempenho.cddocumentoacao = ?", documentoacao.getCddocumentoacao()).or();
			}
			query.closeParentheses();
			
			query.where("vwdocumentocomissaodesempenho.cdcolaboradorcomissao is null");
			
//			query
//				.where("vwdocumentocomissaodesempenho.cddocumentoacao = ?", Documentoacao.BAIXADA.getCddocumentoacao())
//				.where("vwdocumentocomissaodesempenho.cdcolaboradorcomissao is null");
		}
			
		Long total = query.setUseTranslator(false).unique();
		
		if (total != null){
			Money totalaux = new Money(total, false);
			if(totalaux.getValue().doubleValue() != 0)
				totalaux = totalaux.divide(new Money(100d));
			return totalaux;
		}else 
			return new Money();		
	}
	
	/**
	 * M�todo que c�lcula o total repassado de acordo com o filtro
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Money findForTotalrepassadocomissionamentodesempenho(VwdocumentocomissaodesempenhoFiltro filtro) {
		if(filtro == null || filtro.getColaborador() == null || filtro.getColaborador().getCdpessoa() == null ||
				filtro.getDtinicio() == null || filtro.getDtfim() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Long> query = newQueryBuilder(Long.class);
		
		query
			.from(Vwdocumentocomissaodesempenho.class);
		
		if(filtro.getConsiderar() != null && Comissionamentodesempenhoconsiderar.SALDO_VENDA.equals(filtro.getConsiderar())){
			query.select("SUM(vwdocumentocomissaodesempenho.saldofinal)");
		}else if(filtro.getConsiderar() != null && Comissionamentodesempenhoconsiderar.MARKUP_VENDA.equals(filtro.getConsiderar())){
			query.select("SUM(vwdocumentocomissaodesempenho.valordocumento - vwdocumentocomissaodesempenho.valortotalcusto)");
		}else {
//			query.select("SUM(vwdocumentocomissaodesempenho.valordocumento)");
			query.select("SUM(vwdocumentocomissaodesempenho.valoratualdocumento)");
		}
		
		if(filtro.getConsiderar() != null && Comissionamentodesempenhoconsiderar.SALDO_VENDA.equals(filtro.getConsiderar())){
			query
				.where("vwdocumentocomissaodesempenho.cdcontrato is null")
				.where("vwdocumentocomissaodesempenho.saldofinal is not null")
				.where("vwdocumentocomissaodesempenho.saldofinal > 0");
		}
		
		query		
//			.where("dtvencimento >= ? ", filtro.getDtinicio())
//			.where("dtvencimento <= ?", filtro.getDtfim())
			.where("cdpessoa = ?", filtro.getColaborador().getCdpessoa())
			.where("cdcolaboradorcomissao is not null ")
			.where("fromVenda <> true ");
		
		if(filtro.getComissionamento() != null && filtro.getComissionamento().getCdcomissionamento() != null){
			query.where("vwdocumentocomissaodesempenho.cdcomissionamento = ?", filtro.getComissionamento().getCdcomissionamento());
		}
		
		if(filtro.getDtinicio() != null && filtro.getDtfim() != null){
			List<Movimentacaoacao> listaMovimentacaoacao = new ArrayList<Movimentacaoacao>();
			listaMovimentacaoacao.add(Movimentacaoacao.NORMAL);
			listaMovimentacaoacao.add(Movimentacaoacao.CONCILIADA);
			
			query.openParentheses();
			query.openParentheses();
//				query.where("vwdocumentocomissaodesempenho.cdvenda is null");
				query.openParentheses();
				query
					.openParentheses();
						if(Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
							query
							.where("vwdocumentocomissaodesempenho.dtvencimento >= ?", filtro.getDtinicio())
							.where("vwdocumentocomissaodesempenho.dtvencimento <= ?", filtro.getDtfim());
						}else if(Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo())){
							query
							.where("vwdocumentocomissaodesempenho.dtemissao >= ?", filtro.getDtinicio())
							.where("vwdocumentocomissaodesempenho.dtemissao <= ?", filtro.getDtfim());
						}else if(Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
							
							query.where("exists (select d.cddocumento " +
									"from Movimentacaoorigem mo " +
									"join mo.documento d " +
									"join mo.movimentacao m " +
									"join m.movimentacaoacao ma " +
									"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
									"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
									"and d.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
									"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(listaMovimentacaoacao, "cdmovimentacaoacao", ",") + ") " +
									")");
						}else {
							query.where("1=0");
						}
						query.closeParentheses();
					query.or();
					query.where("exists (select v.cddocumento " +
											"from Vdocumentonegociado v " +
											"where v.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
											(Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
												"and v.dtvencimentonegociado >= '" + filtro.getDtinicio() + "' " +
												"and v.dtvencimentonegociado <= '" + filtro.getDtfim() + "' " : "") +
											(Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and v.dtemissaonegociado >= '" + filtro.getDtinicio() + "' " +
													"and v.dtemissaonegociado <= '" + filtro.getDtfim() + "' " : "") +
											(Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and exists (select d.cddocumento " +
															"from Movimentacaoorigem mo " +
															"join mo.documento d " +
															"join mo.movimentacao m " +
															"join m.movimentacaoacao ma " +
															"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
															"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
															"and d.cddocumento = v.cddocumento " +
															"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(listaMovimentacaoacao, "cdmovimentacaoacao", ",") + ") " +
															")" : "") +
											(!Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) &&
											 !Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo()) &&
											 !Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and 1 = 0" : "") +
											")");
				query.closeParentheses();
			query.closeParentheses();
			
			if((filtro.getConsiderardtvenda() != null && filtro.getConsiderardtvenda()) || filtro.getDocumentocomissaotipoperiodo() == null){
				query.or();
				
				query.openParentheses();
					query.where("vwdocumentocomissaodesempenho.cdvenda is not null");
					query.where("vwdocumentocomissaodesempenho.dtvenda >= ?", filtro.getDtinicio());
					query.where("vwdocumentocomissaodesempenho.dtvenda <= ?", filtro.getDtfim());
				query.closeParentheses();
			}
			query.closeParentheses();
		}
		
		
		if(filtro.getMostrar() != null && filtro.getMostrar().equals("Pagos com pend�ncia de repasse")){
			List<Documentoacao> lista = new ArrayList<Documentoacao>();
			lista.add(Documentoacao.BAIXADA);
			lista.add(Documentoacao.NEGOCIADA);
			
			query.where("not exists (select v.cddocumento " +
									"from Vdocumentonegociado v " +
									"where v.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
									"and v.cddocumentoacaonegociado not in(" + CollectionsUtil.listAndConcatenate(lista, "cddocumentoacao", ",") + "))");
			
			query.openParentheses();
			for (Documentoacao documentoacao : lista) {
				query.where("vwdocumentocomissaodesempenho.cddocumentoacao = ?", documentoacao.getCddocumentoacao()).or();
			}
			query.closeParentheses();
			
			query.where("vwdocumentocomissaodesempenho.cdcolaboradorcomissao is null");
			
//			query.where("vwdocumentocomissaodesempenho.cddocumentoacao = ?", Documentoacao.BAIXADA.getCddocumentoacao());
		}
			
		Long total = query.setUseTranslator(false).unique();
		
		if (total != null){
			Money totalaux = new Money(total, false);
			if(totalaux.getValue().doubleValue() != 0)
				totalaux = totalaux.divide(new Money(100d));
			return totalaux;
		}else 
			return new Money();		
	}
	
	/**
	 * M�todo que c�lcula o total a repassar de acordo com o filtro
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Money findForTotalarepassarcomissionamentodesempenho(VwdocumentocomissaodesempenhoFiltro filtro) {
		if(filtro == null || filtro.getColaborador() == null || filtro.getColaborador().getCdpessoa() == null ||
				filtro.getDtinicio() == null || filtro.getDtfim() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Long> query = newQueryBuilder(Long.class);
		
		query
			.from(Vwdocumentocomissaodesempenho.class);
		
		if(filtro.getConsiderar() != null && Comissionamentodesempenhoconsiderar.SALDO_VENDA.equals(filtro.getConsiderar())){
			query.select("SUM(vwdocumentocomissaodesempenho.saldofinal)");
		}else if(filtro.getConsiderar() != null && Comissionamentodesempenhoconsiderar.MARKUP_VENDA.equals(filtro.getConsiderar())){
			query.select("SUM(vwdocumentocomissaodesempenho.valordocumento - vwdocumentocomissaodesempenho.valortotalcusto)");
		}else {
//			query.select("SUM(vwdocumentocomissaodesempenho.valordocumento)");
			query.select("SUM(vwdocumentocomissaodesempenho.valoratualdocumento)");
		}
		
		query
//			.where("dtvencimento >= ? ", filtro.getDtinicio())
//			.where("dtvencimento <= ?", filtro.getDtfim())
			.where("cdpessoa = ?", filtro.getColaborador().getCdpessoa())
			.where("cdcolaboradorcomissao is null ")
			.where("fromVenda <> true ");
		
		if(filtro.getConsiderar() != null && Comissionamentodesempenhoconsiderar.SALDO_VENDA.equals(filtro.getConsiderar())){
			query
				.where("vwdocumentocomissaodesempenho.cdcontrato is null")
				.where("vwdocumentocomissaodesempenho.saldofinal is not null")
				.where("vwdocumentocomissaodesempenho.saldofinal > 0");
		}
		
		if(filtro.getComissionamento() != null && filtro.getComissionamento().getCdcomissionamento() != null){
			query.where("vwdocumentocomissaodesempenho.cdcomissionamento = ?", filtro.getComissionamento().getCdcomissionamento());
		}
		
		if(filtro.getDtinicio() != null && filtro.getDtfim() != null){
			List<Movimentacaoacao> listaMovimentacaoacao = new ArrayList<Movimentacaoacao>();
			listaMovimentacaoacao.add(Movimentacaoacao.NORMAL);
			listaMovimentacaoacao.add(Movimentacaoacao.CONCILIADA);
			
			query.openParentheses();
			query.openParentheses();
//				query.where("vwdocumentocomissaodesempenho.cdvenda is null");
				query.openParentheses();
				query
					.openParentheses();
						if(Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
							query
							.where("vwdocumentocomissaodesempenho.dtvencimento >= ?", filtro.getDtinicio())
							.where("vwdocumentocomissaodesempenho.dtvencimento <= ?", filtro.getDtfim());
						}else if(Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo())){
							query
							.where("vwdocumentocomissaodesempenho.dtemissao >= ?", filtro.getDtinicio())
							.where("vwdocumentocomissaodesempenho.dtemissao <= ?", filtro.getDtfim());
						}else if(Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
							
							query.where("exists (select d.cddocumento " +
									"from Movimentacaoorigem mo " +
									"join mo.documento d " +
									"join mo.movimentacao m " +
									"join m.movimentacaoacao ma " +
									"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
									"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
									"and d.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
									"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(listaMovimentacaoacao, "cdmovimentacaoacao", ",") + ") " +
									")");
						}else {
							query.where("1=0");
						}
						query.closeParentheses();
					query.or();
					query.where("exists (select v.cddocumento " +
											"from Vdocumentonegociado v " +
											"where v.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
											(Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
												"and v.dtvencimentonegociado >= '" + filtro.getDtinicio() + "' " +
												"and v.dtvencimentonegociado <= '" + filtro.getDtfim() + "' " : "") +
											(Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and v.dtemissaonegociado >= '" + filtro.getDtinicio() + "' " +
													"and v.dtemissaonegociado <= '" + filtro.getDtfim() + "' " : "") +
											(Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and exists (select d.cddocumento " +
															"from Movimentacaoorigem mo " +
															"join mo.documento d " +
															"join mo.movimentacao m " +
															"join m.movimentacaoacao ma " +
															"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
															"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
															"and d.cddocumento = v.cddocumento " +
															"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(listaMovimentacaoacao, "cdmovimentacaoacao", ",") + ") " +
															")" : "") +
											(!Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) &&
											 !Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo()) &&
											 !Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and 1 = 0" : "") +
											")");
				query.closeParentheses();
			query.closeParentheses();
			
			if((filtro.getConsiderardtvenda() != null && filtro.getConsiderardtvenda()) || filtro.getDocumentocomissaotipoperiodo() == null){
				query.or();
				
				query.openParentheses();
					query.where("vwdocumentocomissaodesempenho.cdvenda is not null");
					query.where("vwdocumentocomissaodesempenho.dtvenda >= ?", filtro.getDtinicio());
					query.where("vwdocumentocomissaodesempenho.dtvenda <= ?", filtro.getDtfim());
				query.closeParentheses();
			}
			query.closeParentheses();
		}
		
		
		if(filtro.getMostrar() != null && filtro.getMostrar().equals("Pagos com pend�ncia de repasse")){
			List<Documentoacao> lista = new ArrayList<Documentoacao>();
			lista.add(Documentoacao.BAIXADA);
			lista.add(Documentoacao.NEGOCIADA);
			
			query.where("not exists (select v.cddocumento " +
									"from Vdocumentonegociado v " +
									"where v.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
									"and v.cddocumentoacaonegociado not in(" + CollectionsUtil.listAndConcatenate(lista, "cddocumentoacao", ",") + "))");
			
			query.openParentheses();
			for (Documentoacao documentoacao : lista) {
				query.where("vwdocumentocomissaodesempenho.cddocumentoacao = ?", documentoacao.getCddocumentoacao()).or();
			}
			query.closeParentheses();
			
			query.where("vwdocumentocomissaodesempenho.cdcolaboradorcomissao is null");
			
//			query.where("vwdocumentocomissaodesempenho.cddocumentoacao = ?", Documentoacao.BAIXADA.getCddocumentoacao());		
		}
			
		Long total = query.setUseTranslator(false).unique();
		
		if (total != null){
			Money totalaux = new Money(total, false);
			if(totalaux.getValue().doubleValue() != 0)
				totalaux = totalaux.divide(new Money(100d));
			return totalaux;
		}else 
			return new Money();		
	}

	/**
	 * M�todo que retorna os cd's documentoorigem de acordo com o filtro
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public String findDocumentoorigem(VwdocumentocomissaodesempenhoFiltro filtro) {
		if(filtro == null || filtro.getColaborador() == null || filtro.getColaborador().getCdpessoa() == null || 
				filtro.getDtinicio() == null || filtro.getDtfim() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		StringBuilder s = new StringBuilder();
		List<Vwdocumentocomissaodesempenho> lista;
		QueryBuilder<Vwdocumentocomissaodesempenho> query = query()
			.select("vwdocumentocomissaodesempenho.cddocumentoorigem, vwdocumentocomissaodesempenho.cddocumento")
//			.where("dtvencimento >= ? ", filtro.getDtinicio())
//			.where("dtvencimento <= ?", filtro.getDtfim())
			.where("cdpessoa = ?", filtro.getColaborador().getCdpessoa())
			.where("cddocumentoorigem is not null ")
			.where("cdcolaboradorcomissao is null ")
			.where("fromVenda <> true ");
		
		if(filtro.getComissionamento() != null && filtro.getComissionamento().getCdcomissionamento() != null){
			query.where("vwdocumentocomissaodesempenho.cdcomissionamento = ?", filtro.getComissionamento().getCdcomissionamento());
		}
		
		if(filtro.getDtinicio() != null && filtro.getDtfim() != null){
			List<Movimentacaoacao> listaMovimentacaoacao = new ArrayList<Movimentacaoacao>();
			listaMovimentacaoacao.add(Movimentacaoacao.NORMAL);
			listaMovimentacaoacao.add(Movimentacaoacao.CONCILIADA);
			
			query.openParentheses();
			query.openParentheses();
//				query.where("vwdocumentocomissaodesempenho.cdvenda is null");
				query.openParentheses();
				query
					.openParentheses();
						if(Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
							query
							.where("vwdocumentocomissaodesempenho.dtvencimento >= ?", filtro.getDtinicio())
							.where("vwdocumentocomissaodesempenho.dtvencimento <= ?", filtro.getDtfim());
						}else if(Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo())){
							query
							.where("vwdocumentocomissaodesempenho.dtemissao >= ?", filtro.getDtinicio())
							.where("vwdocumentocomissaodesempenho.dtemissao <= ?", filtro.getDtfim());
						}else if(Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
							
							query.where("exists (select d.cddocumento " +
									"from Movimentacaoorigem mo " +
									"join mo.documento d " +
									"join mo.movimentacao m " +
									"join m.movimentacaoacao ma " +
									"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
									"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
									"and d.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
									"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(listaMovimentacaoacao, "cdmovimentacaoacao", ",") + ") " +
									")");
						}else {
							query.where("1=0");
						}
						query.closeParentheses();
					query.or();
					query.where("exists (select v.cddocumento " +
											"from Vdocumentonegociado v " +
											"where v.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
											(Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
												"and v.dtvencimentonegociado >= '" + filtro.getDtinicio() + "' " +
												"and v.dtvencimentonegociado <= '" + filtro.getDtfim() + "' " : "") +
											(Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and v.dtemissaonegociado >= '" + filtro.getDtinicio() + "' " +
													"and v.dtemissaonegociado <= '" + filtro.getDtfim() + "' " : "") +
											(Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and exists (select d.cddocumento " +
															"from Movimentacaoorigem mo " +
															"join mo.documento d " +
															"join mo.movimentacao m " +
															"join m.movimentacaoacao ma " +
															"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
															"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
															"and d.cddocumento = v.cddocumento " +
															"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(listaMovimentacaoacao, "cdmovimentacaoacao", ",") + ") " +
															")" : "") +
											(!Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) &&
											 !Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo()) &&
											 !Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and 1 = 0" : "") +
											")");
				query.closeParentheses();
			query.closeParentheses();
			
			if((filtro.getConsiderardtvenda() != null && filtro.getConsiderardtvenda()) || filtro.getDocumentocomissaotipoperiodo() == null){
				query.or();
				
				query.openParentheses();
					query.where("vwdocumentocomissaodesempenho.cdvenda is not null");
					query.where("vwdocumentocomissaodesempenho.dtvenda >= ?", filtro.getDtinicio());
					query.where("vwdocumentocomissaodesempenho.dtvenda <= ?", filtro.getDtfim());
				query.closeParentheses();
			}
			query.closeParentheses();
		}
		
		
		if(filtro.getMostrar() != null && filtro.getMostrar().equals("Pagos com pend�ncia de repasse")){
			List<Documentoacao> listaSit = new ArrayList<Documentoacao>();
			listaSit.add(Documentoacao.BAIXADA);
			listaSit.add(Documentoacao.NEGOCIADA);
			
			query.where("not exists (select v.cddocumento " +
									"from Vdocumentonegociado v " +
									"where v.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
									"and v.cddocumentoacaonegociado not in(" + CollectionsUtil.listAndConcatenate(listaSit, "cddocumentoacao", ",") + "))");
			
			query.openParentheses();
			for (Documentoacao documentoacao : listaSit) {
				query.where("vwdocumentocomissaodesempenho.cddocumentoacao = ?", documentoacao.getCddocumentoacao()).or();
			}
			query.closeParentheses();
			
			query.where("vwdocumentocomissaodesempenho.cdcolaboradorcomissao is null");
			
//			query.where("vwdocumentocomissaodesempenho.cddocumentoacao = ?", Documentoacao.BAIXADA.getCddocumentoacao());			
		}
		
		lista = query.list();
		
		if(lista != null && !lista.isEmpty()){
			for(Vwdocumentocomissaodesempenho desempenho : lista){
				if(desempenho.getCddocumentoorigem() != null)
					s.append(s != null && !"".equals(s.toString()) ? "," + desempenho.getCddocumentoorigem(): desempenho.getCddocumentoorigem());
			}
		}
		
		return s.toString();
	}
	
	/**
	 * M�todo que retorna os cd's notadocumento de acordo com o filtro
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public String findNotadocumento(VwdocumentocomissaodesempenhoFiltro filtro) {
		if(filtro == null || filtro.getColaborador() == null || filtro.getColaborador().getCdpessoa() == null || 
				filtro.getDtinicio() == null || filtro.getDtfim() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		StringBuilder s = new StringBuilder();
		List<Vwdocumentocomissaodesempenho> lista;
		
		QueryBuilder<Vwdocumentocomissaodesempenho> query = query()
			.select("vwdocumentocomissaodesempenho.cdnotadocumento, vwdocumentocomissaodesempenho.cddocumento")
//			.where("dtvencimento >= ? ", filtro.getDtinicio())
//			.where("dtvencimento <= ?", filtro.getDtfim())
			.where("cdpessoa = ?", filtro.getColaborador().getCdpessoa())
			.where("cdnotadocumento is not null ")
			.where("cdcolaboradorcomissao is null ")
			.where("fromVenda <> true ");
		
		if(filtro.getComissionamento() != null && filtro.getComissionamento().getCdcomissionamento() != null){
			query.where("vwdocumentocomissaodesempenho.cdcomissionamento = ?", filtro.getComissionamento().getCdcomissionamento());
		}
		
		if(filtro.getDtinicio() != null && filtro.getDtfim() != null){
			List<Movimentacaoacao> listaMovimentacaoacao = new ArrayList<Movimentacaoacao>();
			listaMovimentacaoacao.add(Movimentacaoacao.NORMAL);
			listaMovimentacaoacao.add(Movimentacaoacao.CONCILIADA);
			
			query.openParentheses();
			query.openParentheses();
//				query.where("vwdocumentocomissaodesempenho.cdvenda is null");
				query.openParentheses();
				query
					.openParentheses();
						if(Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
							query
							.where("vwdocumentocomissaodesempenho.dtvencimento >= ?", filtro.getDtinicio())
							.where("vwdocumentocomissaodesempenho.dtvencimento <= ?", filtro.getDtfim());
						}else if(Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo())){
							query
							.where("vwdocumentocomissaodesempenho.dtemissao >= ?", filtro.getDtinicio())
							.where("vwdocumentocomissaodesempenho.dtemissao <= ?", filtro.getDtfim());
						}else if(Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
							
							query.where("exists (select d.cddocumento " +
									"from Movimentacaoorigem mo " +
									"join mo.documento d " +
									"join mo.movimentacao m " +
									"join m.movimentacaoacao ma " +
									"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
									"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
									"and d.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
									"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(listaMovimentacaoacao, "cdmovimentacaoacao", ",") + ") " +
									")");
						}else {
							query.where("1=0");
						}
						query.closeParentheses();
					query.or();
					query.where("exists (select v.cddocumento " +
											"from Vdocumentonegociado v " +
											"where v.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
											(Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
												"and v.dtvencimentonegociado >= '" + filtro.getDtinicio() + "' " +
												"and v.dtvencimentonegociado <= '" + filtro.getDtfim() + "' " : "") +
											(Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and v.dtemissaonegociado >= '" + filtro.getDtinicio() + "' " +
													"and v.dtemissaonegociado <= '" + filtro.getDtfim() + "' " : "") +
											(Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and exists (select d.cddocumento " +
															"from Movimentacaoorigem mo " +
															"join mo.documento d " +
															"join mo.movimentacao m " +
															"join m.movimentacaoacao ma " +
															"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
															"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
															"and d.cddocumento = v.cddocumento " +
															"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(listaMovimentacaoacao, "cdmovimentacaoacao", ",") + ") " +
															")" : "") +
											(!Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) &&
											 !Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo()) &&
											 !Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and 1 = 0" : "") +
											")");
				query.closeParentheses();
			query.closeParentheses();
			
			if((filtro.getConsiderardtvenda() != null && filtro.getConsiderardtvenda()) || filtro.getDocumentocomissaotipoperiodo() == null){
				query.or();
				
				query.openParentheses();
					query.where("vwdocumentocomissaodesempenho.cdvenda is not null");
					query.where("vwdocumentocomissaodesempenho.dtvenda >= ?", filtro.getDtinicio());
					query.where("vwdocumentocomissaodesempenho.dtvenda <= ?", filtro.getDtfim());
				query.closeParentheses();
			}
			query.closeParentheses();
		}
		
		
		if(filtro.getMostrar() != null && filtro.getMostrar().equals("Pagos com pend�ncia de repasse")){
			List<Documentoacao> listaSit = new ArrayList<Documentoacao>();
			listaSit.add(Documentoacao.BAIXADA);
			listaSit.add(Documentoacao.NEGOCIADA);
			
			query.where("not exists (select v.cddocumento " +
									"from Vdocumentonegociado v " +
									"where v.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
									"and v.cddocumentoacaonegociado not in(" + CollectionsUtil.listAndConcatenate(listaSit, "cddocumentoacao", ",") + "))");
			
			query.openParentheses();
			for (Documentoacao documentoacao : listaSit) {
				query.where("vwdocumentocomissaodesempenho.cddocumentoacao = ?", documentoacao.getCddocumentoacao()).or();
			}
			query.closeParentheses();
			
			query.where("vwdocumentocomissaodesempenho.cdcolaboradorcomissao is null");
			
//			query.where("vwdocumentocomissaodesempenho.cddocumentoacao = ?", Documentoacao.BAIXADA.getCddocumentoacao());			
		}
		
		lista = query.list();
		if(lista != null && !lista.isEmpty()){
			for(Vwdocumentocomissaodesempenho desempenho : lista){
				if(desempenho.getCdnotadocumento() != null)
					s.append(s != null && !"".equals(s.toString()) ? "," + desempenho.getCdnotadocumento(): desempenho.getCdnotadocumento());
			}
		}
		
		return s.toString();
	}

	/**
	 * M�todo que busca os dados para o relat�rio
	 *
	 * @param _filtro
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<ComissionamentodesempenhoReportBean> findForReportComissionamentodesempenho(VwdocumentocomissaodesempenhoFiltro _filtro) {
		VwdocumentocomissaodesempenhoFiltro filtro = (VwdocumentocomissaodesempenhoFiltro) _filtro;
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		StringBuilder sql = new StringBuilder();
		
		sql
			.append("select nomecolaborador as nomevendedor, valordocumento, valoratualdocumento, cdvenda, cdcontrato, cdcolaboradorcomissao, cdcomissionamento ")
			.append("from vwdocumentocomissaodesempenho ")
			.append("where dtvencimento >= to_date('"+ format.format(filtro.getDtinicio())+ "', 'DD/MM/YYYY') and ") 
			.append("dtvencimento <= to_date('"+ format.format(filtro.getDtfim())+ "', 'DD/MM/YYYY') ");
		
		if(filtro.getComissionamento() != null && filtro.getComissionamento().getCdcomissionamento() != null){
			sql.append(" and vwdocumentocomissaodesempenho.cdcomissionamento = " + filtro.getComissionamento().getCdcomissionamento() + " ");
		}
		
		if(filtro.getColaborador() != null && filtro.getColaborador().getCdpessoa() != null)
			sql.append("and cdpessoa = " + filtro.getColaborador().getCdpessoa() + " ");
		else{
			sql.append("and 1=0 ");
		}
		
		if(filtro.getMostrar() != null && filtro.getMostrar().equals("Pagos com pend�ncia de repasse")){
			sql
				.append("and cddocumentoacao = " + Documentoacao.BAIXADA.getCddocumentoacao() + " ")
				.append("and cdcolaboradorcomissao is null ");
		}
		
		SinedUtil.markAsReader();
		List<ComissionamentodesempenhoReportBean> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new ComissionamentodesempenhoReportBean(rs.getString("nomevendedor"), 
							rs.getLong("valordocumento"), rs.getInt("cdvenda"), 
							rs.getInt("cdcontrato"), rs.getInt("cdcolaboradorcomissao"),rs.getInt("cdcomissionamento"));
				}
			});
		
		return lista;
	}
	
	/**
	 * M�todo que busca os dados para que possa ser calculado o valor da comiss�o 
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Vwdocumentocomissaodesempenho> findForCalcularValorComissao(VwdocumentocomissaodesempenhoFiltro filtro){
		QueryBuilder<Vwdocumentocomissaodesempenho> query = query()
			.select("vwdocumentocomissaodesempenho.cddocumento, vwdocumentocomissaodesempenho.valordocumento, " +
					"vwdocumentocomissaodesempenho.valoratualdocumento, " +
					"vwdocumentocomissaodesempenho.dtvencimento, vwdocumentocomissaodesempenho.cddocumentoorigem, " +
					"vwdocumentocomissaodesempenho.cdnotadocumento, vwdocumentocomissaodesempenho.cdvenda, " +
					"vwdocumentocomissaodesempenho.cdcolaborador, vwdocumentocomissaodesempenho.nomecolaborador, " +
					"vwdocumentocomissaodesempenho.cdcomissionamento, vwdocumentocomissaodesempenho.cdpessoa, " +
					"vwdocumentocomissaodesempenho.cdcolaboradorcomissao, vwdocumentocomissaodesempenho.cdcontrato, " +
					"vwdocumentocomissaodesempenho.saldofinal, vwdocumentocomissaodesempenho.valortotalcusto ")
//			.where("dtvencimento >= ? ", filtro.getDtinicio())
//			.where("dtvencimento <= ?", filtro.getDtfim())
			.setMaxResults(1);
		
		if(filtro.getComissionamento() != null && filtro.getComissionamento().getCdcomissionamento() != null){
			query.where("vwdocumentocomissaodesempenho.cdcomissionamento = ?", filtro.getComissionamento().getCdcomissionamento());
		}
		
		if(filtro.getColaborador() != null && filtro.getColaborador().getCdpessoa() != null)
			query.where("cdpessoa = ?", filtro.getColaborador().getCdpessoa());
		else{
			query.where("1=0");
		}
		
		if(filtro.getDtinicio() != null && filtro.getDtfim() != null){
			List<Movimentacaoacao> lista = new ArrayList<Movimentacaoacao>();
			lista.add(Movimentacaoacao.NORMAL);
			lista.add(Movimentacaoacao.CONCILIADA);
			
			query.openParentheses();
			query.openParentheses();
//				query.where("vwdocumentocomissaodesempenho.cdvenda is null");
				query.openParentheses();
				query
					.openParentheses();
						if(Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
							query
							.where("vwdocumentocomissaodesempenho.dtvencimento >= ?", filtro.getDtinicio())
							.where("vwdocumentocomissaodesempenho.dtvencimento <= ?", filtro.getDtfim());
						}else if(Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo())){
							query
							.where("vwdocumentocomissaodesempenho.dtemissao >= ?", filtro.getDtinicio())
							.where("vwdocumentocomissaodesempenho.dtemissao <= ?", filtro.getDtfim());
						}else if(Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
							
							query.where("exists (select d.cddocumento " +
									"from Movimentacaoorigem mo " +
									"join mo.documento d " +
									"join mo.movimentacao m " +
									"join m.movimentacaoacao ma " +
									"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
									"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
									"and d.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
									"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(lista, "cdmovimentacaoacao", ",") + ") " +
									")");
						}else {
							query.where("1=0");
						}
						query.closeParentheses();
					query.or();
					query.where("exists (select v.cddocumento " +
											"from Vdocumentonegociado v " +
											"where v.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
											(Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
												"and v.dtvencimentonegociado >= '" + filtro.getDtinicio() + "' " +
												"and v.dtvencimentonegociado <= '" + filtro.getDtfim() + "' " : "") +
											(Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and v.dtemissaonegociado >= '" + filtro.getDtinicio() + "' " +
													"and v.dtemissaonegociado <= '" + filtro.getDtfim() + "' " : "") +
											(Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and exists (select d.cddocumento " +
															"from Movimentacaoorigem mo " +
															"join mo.documento d " +
															"join mo.movimentacao m " +
															"join m.movimentacaoacao ma " +
															"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
															"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
															"and d.cddocumento = v.cddocumento " +
															"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(lista, "cdmovimentacaoacao", ",") + ") " +
															")" : "") +
											(!Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) &&
											 !Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo()) &&
											 !Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ?
													"and 1 = 0" : "") +
											")");
				query.closeParentheses();
			query.closeParentheses();
			
			if((filtro.getConsiderardtvenda() != null && filtro.getConsiderardtvenda()) || filtro.getDocumentocomissaotipoperiodo() == null){
				query.or();
				
				query.openParentheses();
					query.where("vwdocumentocomissaodesempenho.cdvenda is not null");
					query.where("vwdocumentocomissaodesempenho.dtvenda >= ?", filtro.getDtinicio());
					query.where("vwdocumentocomissaodesempenho.dtvenda <= ?", filtro.getDtfim());
				query.closeParentheses();
			}
			query.closeParentheses();
		}
		
		if(filtro.getMostrar() != null && filtro.getMostrar().equals("Pagos com pend�ncia de repasse")){
//			query
//				.where("vwdocumentocomissaodesempenho.cddocumentoacao = ?", Documentoacao.BAIXADA.getCddocumentoacao())
//				.where("vwdocumentocomissaodesempenho.cdcolaboradorcomissao is null");
			if(filtro.getMostrar() != null && filtro.getMostrar().equals("Pagos com pend�ncia de repasse")){
				List<Documentoacao> lista = new ArrayList<Documentoacao>();
				lista.add(Documentoacao.BAIXADA);
				lista.add(Documentoacao.NEGOCIADA);
				
				query.where("not exists (select v.cddocumento " +
										"from Vdocumentonegociado v " +
										"where v.cddocumento = vwdocumentocomissaodesempenho.cddocumento " +
										"and v.cddocumentoacaonegociado not in(" + CollectionsUtil.listAndConcatenate(lista, "cddocumentoacao", ",") + "))");
				
				query.openParentheses();
				for (Documentoacao documentoacao : lista) {
					query.where("vwdocumentocomissaodesempenho.cddocumentoacao = ?", documentoacao.getCddocumentoacao()).or();
				}
				query.closeParentheses();
				
				query.where("vwdocumentocomissaodesempenho.cdcolaboradorcomissao is null");
				
//				query
//					.where("vwdocumentocomissaodesempenho.cddocumentoacao = ?", Documentoacao.BAIXADA.getCddocumentoacao())
//					.where("vwdocumentocomissaodesempenho.cdcolaboradorcomissao is null");
			}
		}
		return query.list();
	}
	
	/**
	 * 
	 * M�todo que monta busca a listagem para o relat�rio de Comissionamento Desempenho.
	 *
	 * @name findForReportDesempenho
	 * @param _filtro
	 * @return
	 * @return List<ComissionamentodesempenhoReportBean>
	 * @author Thiago Augusto
	 * @date 24/08/2012
	 *
	 */
	public List<Vwdocumentocomissaodesempenho> findForReportDesempenho(VwdocumentocomissaodesempenhoFiltro _filtro){
		VwdocumentocomissaodesempenhoFiltro filtro = (VwdocumentocomissaodesempenhoFiltro) _filtro;
		this.findForListagem(filtro); 
		
		QueryBuilder<Vwdocumentocomissaodesempenho> query = querySined();
		this.updateListagemQuery(query, _filtro);
		query.select("vwdocumentocomissaodesempenho.cdcomissionamento, vwdocumentocomissaodesempenho.nomecolaborador, " +
		"vwdocumentocomissaodesempenho.cdcolaborador, vwdocumentocomissaodesempenho.valordocumento, " +
		"vwdocumentocomissaodesempenho.valoratualdocumento, vwdocumentocomissaodesempenho.cdvenda," +
		"vwdocumentocomissaodesempenho.fromVenda, vwdocumentocomissaodesempenho.cdcolaboradorcomissao ");
		return query.list();
	}
	
	/**
	 * M�todo que carrega a lista de comiss�o por desempenho para a cria��o da conta a pagar
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 * @since 27/05/2014
	 */
	public List<Vwdocumentocomissaodesempenho> findForContapagar(VwdocumentocomissaodesempenhoFiltro filtro){
		QueryBuilder<Vwdocumentocomissaodesempenho> query = query();
		this.updateListagemQuery(query, filtro);
		return query.list();
	}
}
