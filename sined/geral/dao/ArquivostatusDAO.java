package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Arquivostatus;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ArquivostatusDAO extends GenericDAO<Arquivostatus> {
	
	/**
	 * Retorna o tipo de arquivo como Aguardando resposta
	 * 
	 * @return arquitotipo
	 * @author Jo�o Paulo Zica
	 */
	public Arquivostatus findArquivoStatusAguardandoResposta(){
		return query()
					.where("arquivostatus.cdarquivostatus = ?", Arquivostatus.AGUARDANDO_RESPOSTA)
					.unique();
	}
	
	/* singleton */
	private static ArquivostatusDAO instance;
	public static ArquivostatusDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ArquivostatusDAO.class);
		}
		return instance;
	}
}