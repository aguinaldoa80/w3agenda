package br.com.linkcom.sined.geral.dao;

import java.text.SimpleDateFormat;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Vcategoria;
import br.com.linkcom.sined.geral.bean.enumeration.Tipobuscaconta;
import br.com.linkcom.sined.geral.bean.view.Vwclienteetiqueta;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.VwclienteetiquetaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VwclienteetiquetaDAO extends GenericDAO<Vwclienteetiqueta>{
	
	private CategoriaService categoriaService;
	
	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Vwclienteetiqueta> query,FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		}
		VwclienteetiquetaFiltro filtro = (VwclienteetiquetaFiltro)_filtro;
		
		Vcategoria vcategoria = new Vcategoria();
		
		if (filtro.getCategoria() != null){
			vcategoria = categoriaService.carregarIdentificador(filtro.getCategoria());
		}
	
		query
			.orderBy("pessoa_nome");
			if(filtro.getCategoria() != null){
				query.where("identificador like ?", vcategoria.getIdentificador() + "%");
			}
			
			if(filtro.getEnderecotipo() != null){
				query.where("cdenderecotipo = ?", filtro.getEnderecotipo().getCdenderecotipo());
			}
			
			if(filtro.getTipobuscaconta() != null){
				if(Tipobuscaconta.VENCIMENTO.equals(filtro.getTipobuscaconta())){
					if(filtro.getDtde() != null && filtro.getDtate() != null){
						SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy"); 
						query.where("exists(select d.pessoa.id from Documento d where " +
								"d.documentoacao != " + Documentoacao.CANCELADA.getCddocumentoacao() + 
								" and d.documentoacao != " + Documentoacao.NEGOCIADA.getCddocumentoacao() +
								"and d.dtvencimento >= to_date('"+ format.format(filtro.getDtde())+ "', 'DD/MM/YYYY')" +
								"and d.dtvencimento <= to_date('"+ format.format(filtro.getDtate())+ "', 'DD/MM/YYYY')" +
								"and vwclienteetiqueta.cdpessoa = d.pessoa.id)");
								
					}else{
						if(filtro.getDtde() != null){
							SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy"); 
							query.where("exists(select d.pessoa.id from Documento d where " +
									"d.documentoacao != " + Documentoacao.CANCELADA.getCddocumentoacao() + 
									" and d.documentoacao != " + Documentoacao.NEGOCIADA.getCddocumentoacao() +
									"and d.dtvencimento >= to_date('"+ format.format(filtro.getDtde())+ "', 'DD/MM/YYYY')" +
									"and vwclienteetiqueta.cdpessoa = d.pessoa.id)");
									
						}else if(filtro.getDtate() != null){
							SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy"); 
							query.where("exists(select d.pessoa.id from Documento d where " +
									"d.documentoacao != " + Documentoacao.CANCELADA.getCddocumentoacao() + 
									" and d.documentoacao != " + Documentoacao.NEGOCIADA.getCddocumentoacao() +
									"and d.dtvencimento <= to_date('"+ format.format(filtro.getDtate())+ "', 'DD/MM/YYYY')" +
									"and vwclienteetiqueta.cdpessoa = d.pessoa.id)");
									
						}
					}
				}else if(Tipobuscaconta.EMISSAO.equals(filtro.getTipobuscaconta())){
					if(filtro.getDtde() != null && filtro.getDtate() != null){
						SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy"); 
						query.where("exists(select d.pessoa.id from Documento d where " +
								"d.documentoacao != " + Documentoacao.CANCELADA.getCddocumentoacao() + 
								" and d.documentoacao != " + Documentoacao.NEGOCIADA.getCddocumentoacao() +
								"and d.dtemissao >= to_date('"+ format.format(filtro.getDtde())+ "', 'DD/MM/YYYY')" +
								"and d.dtemissao <= to_date('"+ format.format(filtro.getDtate())+ "', 'DD/MM/YYYY')" +
								"and vwclienteetiqueta.cdpessoa = d.pessoa.id)");
								
					}else {
						if(filtro.getDtde() != null){
							SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy"); 
							query.where("exists(select d.pessoa.id from Documento d where " +
									"d.documentoacao != " + Documentoacao.CANCELADA.getCddocumentoacao() + 
									" and d.documentoacao != " + Documentoacao.NEGOCIADA.getCddocumentoacao() +
									"and d.dtemissao >= to_date('"+ format.format(filtro.getDtde())+ "', 'DD/MM/YYYY')" +
									"and vwclienteetiqueta.cdpessoa = d.pessoa.id)");
									
						}else if(filtro.getDtate() != null){
							SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy"); 
							query.where("exists(select d.pessoa.id from Documento d where " +
									"d.documentoacao != " + Documentoacao.CANCELADA.getCddocumentoacao() + 
									" and d.documentoacao != " + Documentoacao.NEGOCIADA.getCddocumentoacao() +
									"and d.dtemissao <= to_date('"+ format.format(filtro.getDtate())+ "', 'DD/MM/YYYY')" +
									"and vwclienteetiqueta.cdpessoa = d.pessoa.id)");
									
						}
					}
				}
			}
			
			if(filtro.getAtivo() != null){
				query.where("cdpessoa in (select c.cdpessoa from Cliente c where c.ativo = " + (filtro.getAtivo() ? "true" : "false") + ")");			
			} else {
				query.where("cdpessoa in (select c.cdpessoa from Cliente c)");
			}
			
			if(filtro.getCategoria() == null || filtro.getTipoetiqueta() == null){
				query.where("1=0");
			}
	}
		/**
		 * Retorna uma nova lista de whereIn compat�vel com o cdendereco da tabela endere�o.
		 * @param whereIn
		 * @return
		 * @author Taidson
		 * @since 08/11/2010
		 */
		public List<Integer> novoWhereIn(String whereIn) {
			QueryBuilder<Integer> query = newQueryBuilder(Integer.class)
				.from(Vwclienteetiqueta.class)
				.select("distinct cdendereco")
				.whereIn("id", whereIn);
			return query.list();
		}
				
		/**
		 * Retorna uma nova lista de whereIn compat�vel com o cdpessoa da tabela endere�o.
		 * @param whereIn
		 * @return
		 * @author Jo�o Vitor	
		 * @since 14/10/2014
		 */
		public List<Integer> novoWhereInCdPessoa(String whereIn) {
			QueryBuilder<Integer> query = newQueryBuilder(Integer.class)
				.from(Vwclienteetiqueta.class)
				.select("distinct cdpessoa")
				.whereIn("id", whereIn);
			return query.list();
		}
}
