package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Especialidade;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.EspecialidadeFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;


@DefaultOrderBy("especialidade.descricao")
public class EspecialidadeDAO extends GenericDAO<Especialidade>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Especialidade> query, FiltroListagem _filtro) {
		EspecialidadeFiltro filtro = (EspecialidadeFiltro) _filtro;

		query
			.select("especialidade.cdespecialidade, especialidade.descricao, especialidade.ativo," +
					"conselhoclasseprofissional.descricao, conselhoclasseprofissional.sigla")
			.leftOuterJoin("especialidade.conselhoclasseprofissional conselhoclasseprofissional")
			.where("especialidade.cdespecialidade=?", filtro.getCdespecialidade())
			.whereLikeIgnoreAll("especialidade.descricao", filtro.getDescricao())
			.where("conselhoclasseprofissional=?", filtro.getConselhoclasseprofissional())
			.where("coalesce(especialidade.ativo, false)=?", filtro.getAtivo())
			.orderBy("especialidade.descricao");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Especialidade> query) {
		query.leftOuterJoinFetch("especialidade.conselhoclasseprofissional conselhoclasseprofissional");
	}
	
	public List<Especialidade> findAtivosAutocomplete(String q){
		return query()
				.whereLikeIgnoreAll("especialidade.descricao", q)
				.orderBy("especialidade.descricao")
				.autocomplete()
				.list();	
	}

	/**
	* M�todo que busca a especialidade de acordo com a descri��o
	*
	* @param descricao
	* @return
	* @since 05/03/2018
	* @author Luiz Fernando
	*/
	public Especialidade findByDescricao(String descricao) {
		return query()
			.select("especialidade.cdespecialidade, especialidade.descricao")
			.where("upper(especialidade.descricao) = ?", descricao.toUpperCase())
			.orderBy("especialidade.descricao")
			.setMaxResults(1)
			.unique();
	}
}