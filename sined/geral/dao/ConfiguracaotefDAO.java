package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Configuracaotef;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ConfiguracaotefDAO extends GenericDAO<Configuracaotef>{

	@Override
	public void updateListagemQuery(QueryBuilder<Configuracaotef> query, FiltroListagem _filtro) {
		query.leftOuterJoinFetch("configuracaotef.empresa empresa");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Configuracaotef> query) {
		query.leftOuterJoinFetch("configuracaotef.listaConfiguracaotefterminal listaConfiguracaotefterminal");
		query.leftOuterJoinFetch("configuracaotef.listaConfiguracaotefadquirente listaConfiguracaotefadquirente");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaConfiguracaotefterminal");
		save.saveOrUpdateManaged("listaConfiguracaotefadquirente");
	}

	public Configuracaotef loadByEmpresa(Empresa empresa) {
		return query()
				.leftOuterJoinFetch("configuracaotef.listaConfiguracaotefterminal listaConfiguracaotefterminal")
				.leftOuterJoinFetch("configuracaotef.listaConfiguracaotefadquirente listaConfiguracaotefadquirente")
				.leftOuterJoinFetch("listaConfiguracaotefadquirente.fornecedor fornecedor")
				.where("configuracaotef.empresa = ?", empresa)
				.orderBy("listaConfiguracaotefterminal.nome")
				.unique();
	}
	
}
