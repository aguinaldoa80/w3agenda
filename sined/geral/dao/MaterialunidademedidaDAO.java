package br.com.linkcom.sined.geral.dao;

import java.util.Collections;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialunidademedidaDAO extends GenericDAO<Materialunidademedida> {
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Materialunidademedida> findForAtualizarmaterial(String whereIn){		
		return query()
				.select("materialunidademedida.cdmaterialunidademedida, materialunidademedida.valorunitario")
				.join("materialunidademedida.material material")
				.whereIn("material.cdmaterial", whereIn)
				.list();
	}
	
	/**
	 * 
	 * @param materialunidademedida
	 * @author Thiago Clemente
	 * 
	 */
	public void updateValorunitario(Materialunidademedida materialunidademedida){
		if (materialunidademedida==null || materialunidademedida.getCdmaterialunidademedida()==null || materialunidademedida.getValorunitario()==null){
			throw new SinedException("Par�metros inv�lidos");
		}
		getJdbcTemplate().update("update materialunidademedida set valorunitario=? where cdmaterialunidademedida=?", new Object[]{materialunidademedida.getValorunitario(), materialunidademedida.getCdmaterialunidademedida()});
	}

	/**
	 * Retorna todas as unidades de convers�o do material.
	 * 
	 * @author Giovane Freitas
	 * @param material
	 * @return
	 */
	public List<Materialunidademedida> findByMaterial(Material material) {
		
		if (material == null || material.getCdmaterial() == null)
			return Collections.emptyList();

		return query()
			.joinFetch("materialunidademedida.unidademedida unidademedida")
			.where("materialunidademedida.material = ?", material)
			.orderBy("materialunidademedida.fracao desc, materialunidademedida.qtdereferencia")
			.list();
	}

	public List<Materialunidademedida> findForPVOffline() {
		return query()
			.select("materialunidademedida.cdmaterialunidademedida, materialunidademedida.valorunitario, " +
					"materialunidademedida.valormaximo, materialunidademedida.valorminimo, " +
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
					"materialunidademedida.fracao, materialunidademedida.qtdereferencia, " +
					"material.cdmaterial, material.nome")
			.join("materialunidademedida.unidademedida unidademedida")
			.join("materialunidademedida.material material")
			.list();
	}
	
	public List<Materialunidademedida> findForAndroid(String whereIn) {
		return query()
			.select("materialunidademedida.cdmaterialunidademedida, materialunidademedida.valorunitario, " +
					"materialunidademedida.valormaximo, materialunidademedida.valorminimo, " +
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
					"materialunidademedida.fracao, materialunidademedida.qtdereferencia, materialunidademedida.prioridadevenda, " +
					"material.cdmaterial, material.nome")
			.join("materialunidademedida.unidademedida unidademedida")
			.join("materialunidademedida.material material")
			.whereIn("materialunidademedida.cdmaterialunidademedida", whereIn)
			.list();
	}
	
	public List<Materialunidademedida> findForW3Producao(String whereIn){
		return query()
					.select("materialunidademedida.cdmaterialunidademedida, " +
							"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
							"materialunidademedida.fracao, materialunidademedida.qtdereferencia, materialunidademedida.prioridadeproducao, " +
							"material.cdmaterial, material.nome")
					.join("materialunidademedida.unidademedida unidademedida")
					.join("materialunidademedida.material material")
					.whereIn("materialunidademedida.cdmaterialunidademedida", whereIn)
					.list();
	}
	
	/**
	 * M�todo que carrega a lista de materialunidademedida a ser sincronizada com o WMS
	 * 
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Materialunidademedida> findByMaterialForSincronizacao(String whereIn){
		if(whereIn == null || whereIn.trim().isEmpty()){
			return null;
		}
		
		return query()
				.select("distinct materialunidademedida.cdmaterialunidademedida, materialunidademedida.fracao, " +
						"materialunidademedida.valorunitario, materialunidademedida.qtdereferencia, material.cdmaterial, " +
						"material.nome, unidademedida.cdunidademedida, unidademedida.simbolo")
				.join("materialunidademedida.material material")
				.join("materialunidademedida.unidademedida unidademedida")
				.whereIn("material.cdmaterial", whereIn)
				.list();
		
	}
}
