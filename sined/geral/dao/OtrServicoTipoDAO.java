package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.OtrServicoTipo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OtrServicoTipoDAO extends GenericDAO<OtrServicoTipo>{
	
	public List<OtrServicoTipo> findTipoServico (){
		return query()
				.select("otrServicoTipo.cdOtrServicoTipo, otrServicoTipo.tipoServico ")
				.orderBy("otrServicoTipo.tipoServico")
				.list();
	}
}
