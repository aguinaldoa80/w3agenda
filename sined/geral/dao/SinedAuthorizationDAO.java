package br.com.linkcom.sined.geral.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.orm.hibernate3.HibernateSystemException;

import br.com.linkcom.neo.authorization.Permission;
import br.com.linkcom.neo.authorization.Role;
import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.authorization.impl.AuthorizationDAOHibernate;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.exception.AuthorizationException;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Permissao;
import br.com.linkcom.sined.geral.bean.Tela;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Usuariopapel;
import br.com.linkcom.sined.util.LicenseManager;
import br.com.linkcom.sined.util.SinedUtil;

public class SinedAuthorizationDAO extends AuthorizationDAOHibernate {
	
	private static final Log log = LogFactory.getLog(SinedAuthorizationDAO.class);

	static final String usuarioClass = "pessoa";
	static final String usuarioLoginField = "login";

	public User findUserByLogin(String login) {
		SinedUtil.markAsReader();
		Usuario usuario = new QueryBuilder<Usuario>(getHibernateTemplate())
				.from(Usuario.class)
				.where("usuario.login = ?", login)
				.unique();
		if (usuario == null) {
			Fornecedor fornecedor = new QueryBuilder<Fornecedor>(getHibernateTemplate())
									.from(Fornecedor.class)
									.where("fornecedor.login = ?", login)
									.where("fornecedor.acesso = ?", Boolean.TRUE)
									.unique();
			return fornecedor;
		} else {
			return usuario;
		}
	}
	
	public User findUserByLoginAllInformations(User user) {
		if(user == null)
			throw new NullPointerException("O usu�rio n�o pode ser null");
		
		SinedUtil.markAsReader();
		return new QueryBuilder<Usuario>(getHibernateTemplate())
				.from(Usuario.class)
				.leftOuterJoinFetch("usuario.listaUsuariopapel usuariopapel")
				.leftOuterJoinFetch("usuariopapel.papel papel")
				.entity(user)
				.where("bloqueado=?",Boolean.FALSE)
				.unique();
	}

    public Role[] findUserRoles(User user) {
    	SinedUtil.markAsReader();
        List<Role> lista = new QueryBuilder<Role>(getHibernateTemplate())
				.select("papel")
				.from(Usuariopapel.class)
				.join("usuariopapel.papel papel")
				.where("usuariopapel.pessoa= ?", user)
				.list();
        return lista.toArray(new Role[lista.size()]);
	}

	public Permission findPermission(Role role, String controlName) {
		try {
			SinedUtil.markAsReader();
			Permission permission = new QueryBuilder<Permission>(getHibernateTemplate())
				                        .from(Permissao.class)
				                        .joinFetch("permissao.papel papel")
				                        .joinFetch("permissao.tela tela")
				                        .where("tela.path = ?", controlName)
				                        .where("papel = ?", role)
				                        .unique();
			
			if (permission instanceof Permissao) {
				Permissao permissao = (Permissao) permission;
				if (permission != null && !LicenseManager.telaLicenciada(controlName)){					
					Map<String, String> permissionmap = permission.getPermissionmap();
					Set<String> keySet = permissionmap.keySet();
					for (String string : keySet) {
						permissionmap.put(string, "false");
					}
					permissao.setPermissionMap(permissionmap);
					Papel papel = permissao.getPapel();
					if (papel != null)
						papel.setAdministrador(false);
				}
			}			
			
			return permission;
        } catch (HibernateSystemException e) {
            log.error("Erro: Existe mais de uma fun��o cadastrada com o caminho \"" + controlName + "\".", e);
            throw e;
        }
	}

	public Permission savePermission(String controlName, Role role, Map<String, String> permissionMap) {        
		Permissao permissao;
		Tela tela = new QueryBuilder<Tela>(getHibernateTemplate())
						.from(Tela.class)
						.where("tela.path = ?", controlName)
						.unique();
		if(tela == null){
			tela = new Tela();
			tela.setPath(controlName);
			if (controlName.contains("/")) {
				tela.setDescricao(controlName.substring(controlName.lastIndexOf('/') + 1));
			} else {
				tela.setDescricao(controlName);
			}
			hibernateTemplate.save(tela);
		}
		{
			//verificar se j� existe essa permissao no banco
			permissao = new QueryBuilder<Permissao>(getHibernateTemplate())
					.from(Permissao.class)
					.where("permissao.tela.path = ?", controlName)
					.where("permissao.papel = ?", role)
					.unique();
		}
		if(permissao == null){
			//criar a permissao
			permissao = new Permissao();
			Papel papel = (Papel) role;
			permissao.setPapel(papel);
			permissao.setTela(tela);
			permissao.setPermissionMap(permissionMap);
			hibernateTemplate.save(permissao);
			
		} else {
			//atualizar a permissao
			Papel papel = (Papel) role;
			permissao.setPapel(papel);
			permissao.setTela(tela);
			permissao.setPermissionMap(permissionMap);
			hibernateTemplate.update(permissao);
		}
		if (permissao.getTela() != null && permissao.getPapel() != null){
			if (!LicenseManager.telaLicenciada(permissao.getTela().getPath()))
				permissao.getPapel().setAdministrador(false);
		}
		return permissao;
    }

	@Override
	@SuppressWarnings("unchecked")
	public Role[] findAllRoles() {
        try {
        	SinedUtil.markAsReader();
			List find = hibernateTemplate
				.find("from "+Role.class.getName()+" order by nome");
			Object[] toArray = find.toArray(new Role[find.size()]);
			return (Role[]) toArray;
		} catch (Exception e) {
			throw new AuthorizationException("Problema ao excecutar query na classe "+AuthorizationDAOHibernate.class.getName()+". Query: from "+Role.class.getName());
		}
	}
	
	/***
	 * Verifica se o usu�rio est� logado
	 * 
	 * @return verdadeiro ou falso
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public Boolean isUsuarioLogado() {
		User user = Neo.getUser();
		return user != null;		
	}
}
