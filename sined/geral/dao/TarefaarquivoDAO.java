package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Tarefaarquivo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TarefaarquivoDAO extends GenericDAO<Tarefaarquivo> {
	
	/* singleton */
	private static TarefaarquivoDAO instance;
	public static TarefaarquivoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(TarefaarquivoDAO.class);
		}
		return instance;
	}
		
}
