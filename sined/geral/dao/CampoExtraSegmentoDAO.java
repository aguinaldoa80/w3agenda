package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.sined.geral.bean.CampoExtraSegmento;
import br.com.linkcom.sined.geral.bean.Segmento;

public class CampoExtraSegmentoDAO extends GenericDAO<CampoExtraSegmento> {

	public List<CampoExtraSegmento> findBySegmento(Segmento segmento) {
		return query()
				.select("campoExtraSegmento.cdcampoextrasegmento, campoExtraSegmento.nome, campoExtraSegmento.conteudoPadrao, " +
						"campoExtraSegmento.obrigatorio, campoExtraSegmento.ordem, segmento.cdsegmento ")
				.where("segmento = ?", segmento)
				.leftOuterJoin("campoExtraSegmento.segmento segmento")
				.orderBy("campoExtraSegmento.ordem")
				.list();
	}

}
