package br.com.linkcom.sined.geral.dao;

import java.awt.Image;
import java.io.ByteArrayInputStream;

import javax.imageio.ImageIO;
import javax.naming.InitialContext;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.exception.NeoException;
import br.com.linkcom.neo.persistence.FileDAO;
import br.com.linkcom.neo.types.File;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.util.SinedUtil;


public class ArquivoDAO extends FileDAO<Arquivo> {
	
	public Image loadAsImage(File file) {
		fillWithContents(file);
		try {
			return ImageIO.read(new ByteArrayInputStream(file.getContent()));
		}
		catch (Exception e) {
			throw new NeoException("Erro ao converter arquivo em imagem.");
		}
	}
	
	public String getCaminhoArquivoCompleto(Arquivo arquivo){
		return this.getNomeArquivo(arquivo);
	}
	
	public String getPathDir(){
		try{
			String path = InitialContext.doLookup("PATH_ARQUIVO_W3ERP");
			if(path != null && !"".equals(path.trim())){
				return path;
			}
		} catch (Exception e) {}
		
		return System.getProperty("user.home") + java.io.File.separator + "dados";
	}
	
	@Override
	protected String getSaveDir() {
		String clientNameForDir = SinedUtil.getClientNameForDir().replace(":8080", "").replace(":8443", "");
		return getPathDir() + java.io.File.separator + clientNameForDir + java.io.File.separator + "arquivos";
	}
	
	public String getCaminhoCertEmissor() {
		return getPathDir() + java.io.File.separator + "w3erp" + java.io.File.separator + "cert" + java.io.File.separator;
	}
	
	public String getCaminhoLogEmissor() {
		return getSaveDir() + java.io.File.separator + "logemissor" + java.io.File.separator;
	}
	
	public String getCaminhoLogoRtf(Arquivo arquivo){
		return getSaveDir() + java.io.File.separator + "logo" + arquivo.getCdfile() + "." + getExtensao();
	}
	
	/* singleton */
	private static ArquivoDAO instance;
	public static ArquivoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ArquivoDAO.class);
		}
		return instance;
	}

	public void deleteByIds(String whereIn) {
		getJdbcTemplate().execute("DELETE FROM ARQUIVO WHERE CDARQUIVO IN ("+whereIn+")");
	}
}
