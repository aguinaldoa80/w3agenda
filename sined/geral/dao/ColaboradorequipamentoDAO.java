package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorequipamento;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColaboradorequipamentoDAO extends GenericDAO<Colaboradorequipamento> {

	/**
	 * Atualiza a data de devolu��o com a data atual
	 *
	 * @param colaboradorequipamento
	 * @since 25/05/2012
	 * @author Rodrigo Freitas
	 */
	public void updateDtdevolucao(Colaboradorequipamento colaboradorequipamento) {
		getHibernateTemplate().bulkUpdate("update Colaboradorequipamento ce set ce.dtdevolucao = ? where ce.id = ?", new Object[]{
				SinedDateUtils.currentDate(), colaboradorequipamento.getCdcolaboradorequipamento()
		});
	}

	/**
	 * Carrega os dados do equipamento com EPI tambem
	 * 
	 * @param colaboradorequipamento
	 * @return
	 * @since 04/09/2013
	 * @author Luiz Romario Filho
	 */
	public Colaboradorequipamento loadWithEPI(Colaboradorequipamento colaboradorequipamento) {
		return query()
				.select("colaboradorequipamento.dtentrega, epi.nome, epi.nomereduzido, colaboradorequipamento.qtde, epi.caepi" 
						+ ", colaboradorequipamento.dtdevolucao, epi.cdmaterial, colaboradorequipamento.dtvencimento, colaboradorequipamento.certificadoaprovacao")
				.leftOuterJoin("colaboradorequipamento.epi epi")
				.entity(colaboradorequipamento)
				.unique();
	}
	
	
	/**
	 * 
	 * Busca uma lista com dados do equipamento com EPI
	 * 
	 * @param colaborador
	 * @return
	 * @since 25/07/2014
	 * @author Rafael Patr�cio
	 * 
	 */
	public List<Colaboradorequipamento> loadByColaborador(Colaborador colaborador){
		return query()
				.select("colaboradorequipamento.cdcolaboradorequipamento, colaboradorequipamento.dtentrega, colaboradorequipamento.dtdevolucao, " +
						"colaboradorequipamento.qtde, colaboradorequipamento.dtvencimento, epi.cdmaterial, epi.nome, colaborador.cdpessoa, colaboradorequipamento.certificadoaprovacao")
				.join("colaboradorequipamento.colaborador colaborador")
				.join("colaboradorequipamento.epi epi")
				.where("colaborador = ?", colaborador)
				.orderBy("colaboradorequipamento.dtentrega")
				.list();
	}
}
