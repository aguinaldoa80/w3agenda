package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoetapaitem;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialoperador;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialorigem;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialperdadescarte;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoordemsituacao;
import br.com.linkcom.sined.geral.service.ProducaoordemmaterialoperadorService;
import br.com.linkcom.sined.geral.service.ProducaoordemmaterialorigemService;
import br.com.linkcom.sined.geral.service.ProducaoordemmaterialperdadescarteService;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.TrocarProdutoFinalBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoordemmaterialDAO extends GenericDAO<Producaoordemmaterial> {
	
	private ProducaoordemmaterialoperadorService producaoordemmaterialoperadorService;
	private ProducaoordemmaterialperdadescarteService producaoordemmaterialperdadescarteService;
	private ProducaoordemmaterialorigemService producaoordemmaterialorigemService;
	
	public void setProducaoordemmaterialoperadorService(ProducaoordemmaterialoperadorService producaoordemmaterialoperadorService) {this.producaoordemmaterialoperadorService = producaoordemmaterialoperadorService;}
	public void setProducaoordemmaterialperdadescarteService(ProducaoordemmaterialperdadescarteService producaoordemmaterialperdadescarteService) {this.producaoordemmaterialperdadescarteService = producaoordemmaterialperdadescarteService;}
	public void setProducaoordemmaterialorigemService(ProducaoordemmaterialorigemService producaoordemmaterialorigemService) {this.producaoordemmaterialorigemService = producaoordemmaterialorigemService;}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Producaoordemmaterial producaoordemmaterial = (Producaoordemmaterial) save.getEntity();
		
		if(Hibernate.isInitialized(producaoordemmaterial.getListaProducaoordemmaterialmateriaprima()))
			save.saveOrUpdateManaged("listaProducaoordemmaterialmateriaprima");
		if(Hibernate.isInitialized(producaoordemmaterial.getListaProducaoordemmaterialperdadescarte()))
			save.saveOrUpdateManaged("listaProducaoordemmaterialperdadescarte");
	}
	
	/**
	 * Adiciona a quantidade passada por par�metro ao item da ordem de produ��o.
	 *
	 * @param producaoordemmaterial
	 * @param qtde
	 * @author Rodrigo Freitas
	 * @since 15/01/2013
	 */
	public void adicionaQtdeProduzido(Producaoordemmaterial producaoordemmaterial, Double qtde) {
		getHibernateTemplate().bulkUpdate("update Producaoordemmaterial p set p.qtdeproduzido = coalesce(p.qtdeproduzido, 0) + ? where p.id = ?", new Object[]{
			qtde, producaoordemmaterial.getCdproducaoordemmaterial()
		});
	}
	
	/**
	* M�todo que zera a quantidade produzida da ordem de produ��o
	*
	* @param whereIn
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public void zerarQtdeProduzido(String whereIn) {
		if(StringUtils.isNotBlank(whereIn)){
			getHibernateTemplate().bulkUpdate("update Producaoordemmaterial p set p.qtdeproduzido = 0 where p.id in (" + whereIn + ")");
		}
	}
	
	/**
	* M�todo que zera a quantidade de perda/descarte
	*
	* @param whereIn
	* @since 04/05/2018
	* @author Luiz Fernando
	*/
	public void zerarQtdePerdaDescarte(String whereIn) {
		if(StringUtils.isNotBlank(whereIn)){
			getHibernateTemplate().bulkUpdate("update Producaoordemmaterial p set p.qtdeperdadescarte = 0 where p.id in (" + whereIn + ")");
		}
	}

	/**
	 * Busca os materiais de uma ordem de produ��o.
	 *
	 * @param producaoordem
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/01/2013
	 */
	public List<Producaoordemmaterial> findByProducaoordem(Producaoordem producaoordem) {
		if(producaoordem == null || producaoordem.getCdproducaoordem() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("producaoordemmaterial.cdproducaoordemmaterial, producaoordemmaterial.qtde, " +
							"producaoordemmaterial.qtdeproduzido, producaoordemmaterial.qtdeperdadescarte")
					.join("producaoordemmaterial.producaoordem producaoordem")
					.where("producaoordem = ?", producaoordem)
					.list();
	}
	
	/**
	 * M�todo que busca as informa��es necess�rias para montagem do relat�rio de etiquetas de ordem de produ��o.
	 * 
	 * @param whereIn
	 * @param pageFrom
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Producaoordemmaterial> findAllForEtiquetaProducaoordem(String whereIn, String fromPage){
		QueryBuilder<Producaoordemmaterial> query =  querySined();
			query
				.select("distinct producaoordemmaterial.cdproducaoordemmaterial, producaoordemmaterial.qtde, material.cdmaterial, material.identificacao, " +
						"material.nome, departamento.cddepartamento, departamento.nome")
				.join("producaoordemmaterial.material material")
				.join("producaoordemmaterial.producaoordem producaoordem")
				.leftOuterJoin("producaoordem.departamento departamento");
			
		if(fromPage.equalsIgnoreCase("ProducaoAgenda")){
			query
				.join("producaoordemmaterial.listaProducaoordemmaterialorigem producaoordemmaterialorigem")
				.leftOuterJoin("producaoordemmaterialorigem.producaoagenda producaoagenda")
				.whereIn("producaoagenda.cdproducaoagenda", whereIn)
				.ignoreJoin("producaoordemmaterialorigem, producaoagenda");
		} else if(fromPage.equalsIgnoreCase("ProducaoOrdem")){
			query
				.whereIn("producaoordem.cdproducaoordem", whereIn);
		} else{
			throw new SinedException("N�o foi poss�vel verificar a p�gina de origem!");
		}
		
		return query.list();
	}

	/**
	 * M�todo que busca as informa��es necess�rias para montagem do relat�rio matricial de etiqueta de estoque.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Producaoordemmaterial> findAllForEtiquetaExpedicaoproducao(String whereIn){
		return querySined()
				
				.select("distinct producaoordem.cdproducaoordem, producaoordem.dtprevisaoentrega, producaoordemmaterial.cdproducaoordemmaterial, " +
						"producaoordemmaterial.qtde, listaProducaoordemmaterialorigem.cdproducaoordemmaterialorigem, " +
						"material.cdmaterial, material.identificacao, material.nome, producaoagenda.cdproducaoagenda, " +
						"cliente.cdpessoa, cliente.nome, contrato.cdcontrato, contrato.descricao, contrato.identificador, " +
						"contratotipo.cdcontratotipo, contratotipo.nome")
				.join("producaoordemmaterial.producaoordem producaoordem")
				.join("producaoordemmaterial.material material")
				.leftOuterJoin("producaoordemmaterial.listaProducaoordemmaterialorigem listaProducaoordemmaterialorigem")
				.leftOuterJoin("listaProducaoordemmaterialorigem.producaoagenda producaoagenda")
				.leftOuterJoin("producaoagenda.cliente cliente")
				.leftOuterJoin("producaoagenda.contrato contrato")
				.leftOuterJoin("contrato.contratotipo contratotipo")
				.orderBy("contrato.descricao, cliente.nome, material.nome")
				.whereIn("producaoordem.cdproducaoordem", whereIn)
				.list();
	}
	
	/**
	 * M�todo que carrega os dados para preenchimento do item de expedi��o a partir de uma etiqueta com c�digo de barras.
	 * 
	 * @param etiqueta
	 * @return
	 * @author Rafael Salvio
	 */
	public Producaoordemmaterial loadByEtiqueta(Integer etiqueta){
		return query()
				.select("producaoordemmaterial.cdproducaoordemmaterial, producaoordemmaterial.qtde, producaoordemmaterial.qtdeproduzido, " +
						"material.cdmaterial, material.identificacao, material.nome, " +
						"producaoordem.cdproducaoordem, producaoordem.dtprevisaoentrega, " +
						"listaProducaoordemmaterialorigem.cdproducaoordemmaterialorigem, producaoagenda.cdproducaoagenda, " +
						"cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj," +
						"contrato.cdcontrato, contrato.descricao, contrato.identificador")
				.join("producaoordemmaterial.producaoordem producaoordem")
				.join("producaoordemmaterial.material material")
				.leftOuterJoin("producaoordemmaterial.listaProducaoordemmaterialorigem listaProducaoordemmaterialorigem")
				.leftOuterJoin("listaProducaoordemmaterialorigem.producaoagenda producaoagenda")
				.leftOuterJoin("producaoagenda.cliente cliente")
				.leftOuterJoin("producaoagenda.contrato contrato")
				.orderBy("contrato.descricao, cliente.nome, material.nome")
				.where("producaoordemmaterial.cdproducaoordemmaterial = ?", etiqueta)
				.unique();
	}
	
	/**
 	 * Adiciona a quantidade de perda passada por par�metro ao item da ordem de produ��o.
	 * @param producaoordemmaterial
	 * @param qtdePerda
	 * @author Rafael Salvio
	 */
	public void adicionaQtdeperdadescarte(Producaoordemmaterial producaoordemmaterial, Double qtdePerda) {
		getHibernateTemplate().bulkUpdate("update Producaoordemmaterial p set p.qtdeperdadescarte = coalesce(p.qtdeperdadescarte, 0) + ? where p.id = ?", new Object[]{
				qtdePerda, producaoordemmaterial.getCdproducaoordemmaterial()
		});
	}
	
	/**
	 * Busca os itens da ordem de produ��o a partir dos itens do pedido de venda
	 *
	 * @param whereInPedidovendamaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/07/2014
	 */
	public List<Producaoordemmaterial> findByPedidovendamaterial(String whereInPedidovendamaterial) {
		return query()
					.select("producaoordemmaterial.cdproducaoordemmaterial, producaoordemmaterial.qtde, producaoordemmaterial.qtdeproduzido, " +
							"pedidovendamaterial.cdpedidovendamaterial, pedidovenda.cdpedidovenda, producaoordem.cdproducaoordem, producaoordem.producaoordemsituacao")
					.leftOuterJoin("producaoordemmaterial.pedidovendamaterial pedidovendamaterial")
					.leftOuterJoin("pedidovendamaterial.pedidovenda pedidovenda")
					.join("producaoordemmaterial.producaoordem producaoordem")
					.whereIn("pedidovendamaterial.cdpedidovendamaterial", whereInPedidovendamaterial)
					.list();
	}
	
	/**
	* M�todo que busca a �ltima ordem de produ��o
	*
	* @param producaoagenda
	* @param producaoetapaitem
	* @return
	* @since 03/12/2014
	* @author Luiz Fernando
	*/
	public Producaoordemmaterial buscarUltimaOrdemproducao(Producaoagenda producaoagenda, Producaoetapaitem producaoetapaitem) {
		List<Producaoordemmaterial> lista = query()
					.select("producaoordemmaterial.cdproducaoordemmaterial, producaoordemmaterial.qtde, producaoordemmaterial.qtdeproduzido")
					.join("producaoordemmaterial.listaProducaoordemmaterialorigem listaProducaoordemmaterialorigem")
					.join("listaProducaoordemmaterialorigem.producaoagenda producaoagenda")
					.join("producaoordemmaterial.producaoetapaitem producaoetapaitem")
					.where("producaoagenda = ?", producaoagenda)
					.where("producaoetapaitem = ?", producaoetapaitem)
					.list();
		
		return lista != null && lista.size() == 1 ? lista.get(0) : null;
	}

	/**
	* M�todo que carrega o item da ordem de produ��o para devolu��o
	*
	* @param producaoordemmaterial
	* @return
	* @since 22/01/2016
	* @author Luiz Fernando
	*/
	public Producaoordemmaterial loadForDevolucao(Producaoordemmaterial producaoordemmaterial) {
		if(producaoordemmaterial == null || producaoordemmaterial.getCdproducaoordemmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("producaoordemmaterial.cdproducaoordemmaterial, producaoordemmaterial.qtde, producaoordemmaterial.qtdeproduzido, producaoordem.cdproducaoordem")
			.join("producaoordemmaterial.producaoordem producaoordem")
			.where("producaoordemmaterial = ?", producaoordemmaterial)
			.unique();
	}
	
	/**
	 * M�todo respons�vel por retornar Producaoordemmaterial de acordo com WhereIn
	 * @param WhereIn
	 * @return
	 * @since 12/02/2016
	 * @author C�sar
	 */
	public List<Producaoordemmaterial> findByItensInterrompidos(String WhereIn){		
		if(WhereIn == null || WhereIn.isEmpty()){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
					.select("producaoordemmaterial.cdproducaoordemmaterial, producaoordemmaterial.qtdeperdadescarte,pedidovendamaterial.cdpedidovendamaterial")
					.leftOuterJoin("producaoordemmaterial.pedidovendamaterial pedidovendamaterial")
					.whereIn("pedidovendamaterial", WhereIn)
					.list();
	}
	
	/**
	* M�todo que retorna a lista de itens de ordem de produ��o 
	*
	* @param whereIn
	* @return
	* @since 09/03/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordemmaterial> findByProducaoordemmaterial(String whereIn) {
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
					.select("producaoordemmaterial.cdproducaoordemmaterial, producaoordemmaterial.qtde, " +
							"producaoordemmaterial.qtdeproduzido, producaoordemmaterial.qtdeperdadescarte")
					.whereIn("producaoordemmaterial.cdproducaoordemmaterial", whereIn)
					.list();
	}
	
	/**
	* M�todo que carrega os itens da ordem de produ��o para verificar exclus�o
	*
	* @param producaoordem
	* @param whereIn
	* @return
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordemmaterial> findForDelete(Producaoordem producaoordem, String whereIn) {
		if(whereIn == null || whereIn.equals("") || producaoordem == null || producaoordem.getCdproducaoordem() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("producaoordemmaterial.cdproducaoordemmaterial, producaoordem.cdproducaoordem")
					.join("producaoordemmaterial.producaoordem producaoordem")
					.where("producaoordemmaterial.cdproducaoordemmaterial not in (" +whereIn+")")
					.where("producaoordem = ?", producaoordem)
					.list();
	}
	
	/**
	* M�todo que carrega os itens da ordem de produ��o
	*
	* @param whereInProducaoordem
	* @param whereInProducaoordemmaterial
	* @return
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordemmaterial> findByProducaoordem(String whereInProducaoordem, String whereInProducaoordemmaterial) {
		if((whereInProducaoordem == null || whereInProducaoordem.equals("")) && (whereInProducaoordemmaterial == null || whereInProducaoordemmaterial.equals(""))){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("producaoordemmaterial.cdproducaoordemmaterial, producaoordemmaterial.qtde, producaoordem.cdproducaoordem, " +
							"material.cdmaterial, material.nome, material.identificacao, material.qtdereferencia, material.producao, " +
							"producaoordem.cdproducaoordem, producaoetapa.cdproducaoetapa, producaoetapa.corteproducao, producaoetapa.baixarEstoqueMateriaprima, producaoetapa.baixarmateriaprimacascata, " +
							"listaProducaoordemmaterialmateriaprima.cdproducaoordemmaterialmateriaprima, " +
							"listaProducaoordemmaterialmateriaprima.qtdeprevista, " +
							"materialMateriaprima.cdmaterial, materialMateriaprima.nome, materialMateriaprima.identificacao, " +
							"materialMateriaprima.servico, materialMateriaprima.produto, " +
							"materialMateriaprima.patrimonio, materialMateriaprima.epi," +
							"unidademedidaMateriaprima.cdunidademedida, unidademedidaMateriaprima.nome, unidademedidaMateriaprima.casasdecimaisestoque, empresa.cdpessoa," +
							"unidademedidaPAMMP.cdunidademedida, unidademedidaPAMMP.nome, unidademedidaPAMMP.casasdecimaisestoque, " +
							"listaProducaoordemmaterialmateriaprima.fracaounidademedida, listaProducaoordemmaterialmateriaprima.qtdereferenciaunidademedida, " +
							"materialproduto.altura, materialproduto.largura, materialproduto.comprimento, " +
							"pneumaterialbanda.cdmaterial, pneumaterialbanda.nome, pneumaterialbanda.identificacao," +
							"producaoetapanomePAMMP.cdproducaoetapanome, producaoetapanomePAMMP.nome," +
							"producaoordemmaterialorigem.cdproducaoordemmaterialorigem, producaoagendamaterial.cdproducaoagendamaterial, " +
							"producaoagendamaterial.altura, producaoagendamaterial.largura, producaoagendamaterial.comprimento," +
							"producaoetapaMaterial.cdproducaoetapa, producaoetapaMaterial.baixarEstoqueMateriaprima, producaoetapaMaterial.baixarmateriaprimacascata," +
							"producaoetapaitem.cdproducaoetapaitem, producaoetapanome.cdproducaoetapanome, producaoetapanome.nome  ")
					.join("producaoordemmaterial.producaoordem producaoordem")
					.join("producaoordemmaterial.material material")
					.leftOuterJoin("material.producaoetapa producaoetapaMaterial")
					.leftOuterJoin("material.materialproduto materialproduto")
					.leftOuterJoin("producaoordemmaterial.pneu pneu")
					.leftOuterJoin("pneu.materialbanda pneumaterialbanda")
					.leftOuterJoin("producaoordemmaterial.producaoetapa producaoetapa")
					.leftOuterJoin("producaoordemmaterial.producaoetapaitem producaoetapaitem")
					.leftOuterJoin("producaoetapaitem.producaoetapanome producaoetapanome")
					.leftOuterJoin("producaoordem.empresa empresa")
					.leftOuterJoin("producaoordemmaterial.listaProducaoordemmaterialmateriaprima listaProducaoordemmaterialmateriaprima")
					.leftOuterJoin("listaProducaoordemmaterialmateriaprima.material materialMateriaprima")
					.leftOuterJoin("materialMateriaprima.unidademedida unidademedidaMateriaprima")
					.leftOuterJoin("listaProducaoordemmaterialmateriaprima.unidademedida unidademedidaPAMMP")
					.leftOuterJoin("listaProducaoordemmaterialmateriaprima.producaoetapanome producaoetapanomePAMMP")
					.leftOuterJoin("producaoordemmaterial.listaProducaoordemmaterialorigem producaoordemmaterialorigem")
					.leftOuterJoin("producaoordemmaterialorigem.producaoagendamaterial producaoagendamaterial")
					.whereIn("producaoordem.cdproducaoordem", whereInProducaoordem)
					.whereIn("producaoordemmaterial.cdproducaoordemmaterial", whereInProducaoordemmaterial)
					.list();
	}
	
	/**
	* M�todo que retorna os itens da ordem de produ��o cancelada com vinculo com pedido de venda
	*
	* @param whereIn
	* @return
	* @since 18/04/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordemmaterial> findOrdemCanceladaPedidovenda(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		
		return query()
				.select("producaoordemmaterial.cdproducaoordemmaterial, producaoordem.cdproducaoordem, pedidovendamaterial.cdpedidovendamaterial, pedidovenda.cdpedidovenda ")
				.join("producaoordemmaterial.pedidovendamaterial pedidovendamaterial")
				.join("pedidovendamaterial.pedidovenda pedidovenda")
				.join("producaoordemmaterial.producaoordem producaoordem")
				.whereIn("producaoordem.cdproducaoordem", whereIn)
				.where("producaoordem.producaoordemsituacao = ?", Producaoordemsituacao.CANCELADA)
				.list();
	}
	
	public Producaoordemmaterial loadForProducaoTrocarProdutoFinal(Producaoordemmaterial producaoordemmaterial) {
		return query()
				.leftOuterJoinFetch("producaoordemmaterial.producaoordem producaoordem")
				.leftOuterJoinFetch("producaoordemmaterial.material material")
				.leftOuterJoinFetch("producaoordemmaterial.producaoetapa producaoetapa")
				.leftOuterJoinFetch("producaoordemmaterial.producaoetapaitem producaoetapaitem")
				.leftOuterJoinFetch("producaoordemmaterial.pedidovendamaterial pedidovendamaterial")
				.leftOuterJoinFetch("producaoordemmaterial.unidademedida unidademedida")
				.leftOuterJoinFetch("producaoordemmaterial.pneu pneu")
				.leftOuterJoinFetch("producaoordemmaterial.listaProducaoordemmaterialorigem listaProducaoordemmaterialorigem")
//				.leftOuterJoinFetch("producaoordemmaterial.listaCampo listaCampo")
//				.leftOuterJoinFetch("producaoordemmaterial.listaProducaoordemmaterialproduzido listaProducaoordemmaterialproduzido")
//				.leftOuterJoinFetch("producaoordemmaterial.listaProducaoordemmaterialmateriaprima listaProducaoordemmaterialmateriaprima")
//				.leftOuterJoinFetch("producaoordemmaterial.listaProducaoordemequipamento listaProducaoordemequipamento")
				.where("producaoordemmaterial=?", producaoordemmaterial)
				.unique();
	}
	
	public void transactionProducaoTrocarProdutoFinal(final Producaoordem producaoordem) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				if (producaoordem.getListaTrocarProdutoFinal()!=null){
					Producaoordemmaterial producaoordemmaterialTela = producaoordem.getProducaoordemmaterial();
					Producaoordemmaterial producaoordemmaterialBD = loadForProducaoTrocarProdutoFinal(producaoordemmaterialTela);
					
					for (TrocarProdutoFinalBean trocarProdutoFinalBean: producaoordem.getListaTrocarProdutoFinal()){
						Producaoordemmaterial producaoordemmaterialNovo = producaoordemmaterialBD.getClone();  
						producaoordemmaterialNovo.setCdproducaoordemmaterial(null);
						producaoordemmaterialNovo.setMaterial(trocarProdutoFinalBean.getMaterial());
						producaoordemmaterialNovo.setQtde(trocarProdutoFinalBean.getQtde());
						producaoordemmaterialNovo.setQtdeproduzido(0D);
						producaoordemmaterialNovo.setQtdeperdadescarte(0D);
						
						saveOrUpdate(producaoordemmaterialNovo);
						
						if(SinedUtil.isListNotEmpty(producaoordemmaterialNovo.getListaProducaoordemmaterialorigem())){
							for(Producaoordemmaterialorigem pomo : producaoordemmaterialNovo.getListaProducaoordemmaterialorigem()){
								Producaoordemmaterialorigem pomoNovo = new Producaoordemmaterialorigem();
								pomoNovo.setProducaoordemmaterial(producaoordemmaterialNovo);
								pomoNovo.setProducaoagenda(pomo.getProducaoagenda());
								pomoNovo.setProducaoagendamaterial(pomo.getProducaoagendamaterial());
								producaoordemmaterialorigemService.saveOrUpdateNoUseTransaction(pomoNovo);
							}
						}
					}
					
					delete(producaoordemmaterialTela);
				}
				
				return null;
			}			
		});
	}
	
	public void transactionProducaoOperador(final Producaoordemmaterial producaoordemmaterial) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				producaoordemmaterialoperadorService.delete(producaoordemmaterial);
				
				Double qtdeproduzido = 0D;
				Double qtdeperdadescarte = 0D;
				
				if (producaoordemmaterial.getListaProducaoordemmaterialoperador()!=null){
					for (Producaoordemmaterialoperador producaoordemmaterialoperador: producaoordemmaterial.getListaProducaoordemmaterialoperador()){
						producaoordemmaterialoperador.setProducaoordemmaterial(producaoordemmaterial);
						producaoordemmaterialoperadorService.saveOrUpdate(producaoordemmaterialoperador);
						
						if (producaoordemmaterialoperador.getQtdeproduzido()!=null){
							qtdeproduzido += producaoordemmaterialoperador.getQtdeproduzido();
						}
						
						if (producaoordemmaterialoperador.getQtdeperdadescarte()!=null){
							qtdeperdadescarte += producaoordemmaterialoperador.getQtdeperdadescarte();
							for(Producaoordemmaterialperdadescarte beanPerdadescarte: producaoordemmaterialoperador.getListaProducaoordemmaterialperdadescarte()){
								beanPerdadescarte.setProducaoordemmaterialoperador(producaoordemmaterialoperador);
								producaoordemmaterialperdadescarteService.saveOrUpdate(beanPerdadescarte);
							}
						}
					}
					
					getHibernateTemplate().bulkUpdate("update Producaoordemmaterial p set qtdeproduzido=?, qtdeperdadescarte=? where p=?", new Object[]{qtdeproduzido, qtdeperdadescarte, producaoordemmaterial});
					getHibernateTemplate().bulkUpdate("delete from Producaoordemmaterialperdadescarte pd where pd.producaoordemmaterial = ? and pd.producaoordemmaterialoperador is null", new Object[]{producaoordemmaterial});
				}
				
				return null;
			}			
		});
	}
	
	public Producaoordemmaterial loadWithProducaoetapaitem(Producaoordemmaterial bean){
		return query()
			.select("producaoordemmaterial.cdproducaoordemmaterial, producaoetapaitem.cdproducaoetapaitem, producaoetapaitem.ordem, "+
					"producaoetapanome.cdproducaoetapanome, producaoetapanome.nome, "+
					"producaoetapa.cdproducaoetapa, producaoetapa.baixarEstoqueMateriaprima, producaoetapa.baixarmateriaprimacascata, "+
					"producaoetapaitemProducaoetapa.cdproducaoetapa, producaoetapaitemProducaoetapa.baixarEstoqueMateriaprima, producaoetapaitemProducaoetapa.baixarmateriaprimacascata")
			.leftOuterJoin("producaoordemmaterial.producaoetapaitem producaoetapaitem")
			.leftOuterJoin("producaoetapaitem.producaoetapa producaoetapaitemProducaoetapa")
			.leftOuterJoin("producaoordemmaterial.producaoetapa producaoetapa")
			.leftOuterJoin("producaoetapaitem.producaoetapanome producaoetapanome")
			.where("producaoordemmaterial = ?", bean)
			.unique();
	}
}