package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.view.Vwformularioppp;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.FormularioPPPReportFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FormularioPPPDAO extends GenericDAO<Vwformularioppp> {

	@Override
	public void updateListagemQuery(QueryBuilder<Vwformularioppp> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inválidos");
		}
		FormularioPPPReportFiltro filtro = (FormularioPPPReportFiltro) _filtro;
		filtro.setForceCountAllFields(true);
		
		query
			.select("distinct vwformularioppp.cdpessoa, vwformularioppp.nome, vwformularioppp.cpf")
			.join("vwformularioppp.listaColaboradorcargo listaColaboradorcargo")
			.join("listaColaboradorcargo.cargo cargo")
			.leftOuterJoin("listaColaboradorcargo.empresa empresa")
			.leftOuterJoin("listaColaboradorcargo.projeto projeto")
			.where("cargo = ?", filtro.getCargo())
			.where("projeto = ?", filtro.getProjeto())
			.where("empresa = ?", filtro.getEmpresa())
			.where("listaColaboradorcargo.matricula = ?", filtro.getMatricula())
			.ignoreJoin("listaColaboradorcargo", "cargo", "projeto", "empresa");
		
		if(filtro.getColaborador() != null && filtro.getColaborador().getCdpessoa() != null)
			query.where("vwformularioppp.cdpessoa = ?", filtro.getColaborador().getCdpessoa());
	}
	
	@Override
	public ListagemResult<Vwformularioppp> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Vwformularioppp> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("vwformularioppp.cdpessoa");
		
		return new ListagemResult<Vwformularioppp>(query, filtro);
	}
}
