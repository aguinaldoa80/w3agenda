package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Documentoenvioboleto;
import br.com.linkcom.sined.geral.bean.Documentoenvioboletodiascobranca;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentoenvioboletodiascobrancaDAO extends GenericDAO<Documentoenvioboletodiascobranca> {

	/**
	 * Busca uma lista de Documentoenvioboletodiascobranca
	 * 	 
	 * @param documentoenvioboleto
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/07/2018
	 */
	public List<Documentoenvioboletodiascobranca> findByDocumentoenvioboleto(Documentoenvioboleto documentoenvioboleto) {
		if(documentoenvioboleto == null || documentoenvioboleto.getCddocumentoenvioboleto() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("documentoenvioboletodiascobranca.cddocumentoenvioboletodiascobranca, documentoenvioboletodiascobranca.qtdedias")
					.where("documentoenvioboletodiascobranca.documentoenvioboleto = ?", documentoenvioboleto)
					.orderBy("documentoenvioboletodiascobranca.qtdedias")
					.list();
	}
	
}
