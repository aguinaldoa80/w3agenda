package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.ExpedicaoAcompanhamentoEntrega;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ExpedicaoAcompanhamentoEntregaDAO extends GenericDAO<ExpedicaoAcompanhamentoEntrega>{

	public List<ExpedicaoAcompanhamentoEntrega> findByExpedicao(Expedicao expedicao){
		return querySined()
				.select("expedicaoAcompanhamentoEntrega.cdExpedicaoAcompanhamentoEntrega," +
						"expedicaoAcompanhamentoEntrega.dataAcompanhamento, expedicaoAcompanhamentoEntrega.nome, expedicaoAcompanhamentoEntrega.categoria, "+
						"expedicaoAcompanhamentoEntrega.tipoEvento, expedicaoAcompanhamentoEntrega.localizacao, expedicaoAcompanhamentoEntrega.codigo, "+
						"expedicaoAcompanhamentoEntrega.cidade, expedicaoAcompanhamentoEntrega.uf, expedicaoAcompanhamentoEntrega.descricaoEvento, "+
						"expedicaoAcompanhamentoEntrega.observacao, expedicaoAcompanhamentoEntrega.urlRastreamento, expedicaoAcompanhamentoEntrega.rastreamento, "+
						"expedicaoAcompanhamentoEntrega.ordem, expedicao.cdexpedicao")
				.join("expedicaoAcompanhamentoEntrega.expedicao expedicao")
				.where("expedicao = ?", expedicao)
				.orderBy("expedicaoAcompanhamentoEntrega.ordem desc")
				.list();
	}
	
	public List<ExpedicaoAcompanhamentoEntrega> findAcompanhamentosOriginadosDosCorreios(Expedicao expedicao){
		return querySined()
				.select("expedicaoAcompanhamentoEntrega.cdExpedicaoAcompanhamentoEntrega," +
						"expedicaoAcompanhamentoEntrega.dataAcompanhamento, expedicaoAcompanhamentoEntrega.nome, expedicaoAcompanhamentoEntrega.categoria, "+
						"expedicaoAcompanhamentoEntrega.tipoEvento, expedicaoAcompanhamentoEntrega.localizacao, expedicaoAcompanhamentoEntrega.codigo, "+
						"expedicaoAcompanhamentoEntrega.cidade, expedicaoAcompanhamentoEntrega.uf, expedicaoAcompanhamentoEntrega.descricaoEvento, "+
						"expedicaoAcompanhamentoEntrega.observacao, expedicaoAcompanhamentoEntrega.urlRastreamento, expedicaoAcompanhamentoEntrega.rastreamento, "+
						"expedicaoAcompanhamentoEntrega.ordem, expedicao.cdexpedicao")
				.join("expedicaoAcompanhamentoEntrega.expedicao expedicao")
				.where("expedicao = ?", expedicao)
				.orderBy("expedicaoAcompanhamentoEntrega.ordem desc")
				.list();
	}
}
