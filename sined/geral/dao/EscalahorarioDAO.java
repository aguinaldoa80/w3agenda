package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Escalacolaborador;
import br.com.linkcom.sined.geral.bean.Escalahorario;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.enumeration.DiaSemana;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EscalahorarioDAO extends GenericDAO<Escalahorario> {

	/**
	 * M�todo que retorna as escalas de hor�rio de um ve�culo
	 * 
	 * @param veiculo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Escalahorario> findEscalaHorarioDoVeiculo(Veiculo veiculo) {
		return query()
			.select("escalahorario.cdescalahorario, escalahorario.horainicio, escalahorario.horafim, escalahorario.diasemana")
			.join("escalahorario.escala escala")
			.where("escala.id = (select v.escala.id from Veiculo v where v.id = "+veiculo.getCdveiculo()+")")
			.list();
	}

	/**
	 * M�todo que retorna as escalas de hor�rio do colaborador, se houver data e as escala do colaborador houver dias de semana definidos,
	 * busca somente a escala de hor�rio referente ao dia da semana.
	 * 
	 * @param colaborador
	 * @param data
	 * @return
	 * @author Tom�s Rabelo
	 * @param material 
	 */
	public List<Escalahorario> findEscalaHorarioDoColaboradorNaData(Colaborador colaborador, Date data) {
		if(colaborador == null || colaborador.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos!");
		
		QueryBuilder<Escalacolaborador> query1 = newQueryBuilderSined(Escalacolaborador.class)
													.select("escala.id")
													.from(Escalacolaborador.class)
													.join("escalacolaborador.escala escala")
													.join("escalacolaborador.colaborador colaborador")
													.where("colaborador = ?", colaborador);
		
		QueryBuilder<Escalacolaborador> query2 = newQueryBuilderSined(Escalacolaborador.class)
													.select("escala.id")
													.from(Escalacolaborador.class)
													.join("escalacolaborador.escala escala")
													.join("escalacolaborador.colaborador colaborador")
													.where("colaborador = ?", colaborador)
													.where("escalacolaborador.exclusivo = true");
		
		Object[] param;
		if(data != null){
			query1
				.where("escalacolaborador.dtinicio <= ?", data)
				.openParentheses()
				.where("escalacolaborador.dtfim >= ?", data)
				.or()
				.where("escalacolaborador.dtfim is null")
				.closeParentheses();
			
			query2
				.where("escalacolaborador.dtinicio <= ?", data)
				.openParentheses()
				.where("escalacolaborador.dtfim >= ?", data)
				.or()
				.where("escalacolaborador.dtfim is null")
				.closeParentheses();
			
			param = new Object[]{colaborador, data, data};
		} else {
			param = new Object[]{colaborador};
		}
		
		
		QueryBuilder<Escalahorario> query = query()
			.select("escalahorario.cdescalahorario, escalahorario.horainicio, escalahorario.horafim, escalahorario.diasemana, " +
					"escala.cdescala, escala.naotrabalhadomingo, escala.naotrabalhaferiado")
			.join("escalahorario.escala escala")
			
			// FEITO ASSIM PARA QUE QUANDO O COLABORADOR TIVER UMA ESCALA EXCLUSIVA
			// APARECER SOMENTE OS HOR�RIOS DAQUELA ESCALA
			.openParentheses()
				.openParentheses()
					.where("escala.id in (" + query1.getQuery() + ")", param)
					.where("not exists (" + query2.getQuery() + ")", param)
				.closeParentheses()
				.or()
				.openParentheses()
					.where("escala.id in (" + query2.getQuery() + ")", param)
					.where("exists (" + query2.getQuery() + ")", param)
				.closeParentheses()
			.closeParentheses()
			;
		
//			.where("escala.id = (select c.escala.id from Colaborador c where c.id = "+colaborador.getCdpessoa()+")");
		
		if(data != null){
			Calendar dataAux = null;
			dataAux = Calendar.getInstance();
			dataAux.setTimeInMillis(data.getTime());
			
			query
				.openParentheses()
					.where("escalahorario.diasemana is null").or()
					.where("escalahorario.diasemana = ?", DiaSemana.values()[dataAux.get(Calendar.DAY_OF_WEEK)-1])
				.closeParentheses();
		}
		
		return query.orderBy("escalahorario.diasemana, escalahorario.horainicio, escalahorario.horafim").list();
	}
	
}
