package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorsituacao;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Ecom_PedidoVenda;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Regiao;
import br.com.linkcom.sined.geral.bean.Regiaolocal;
import br.com.linkcom.sined.geral.bean.Regiaovendedor;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.VendaSincronizacaoFlex;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendamaterialmestre;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.BaixaestoqueEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Comprovantestatus;
import br.com.linkcom.sined.geral.bean.enumeration.Emitircurvaabcranking;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacaoentrega;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.GeracaocontareceberEnum;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoLancamentoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.TipoVendedorEnum;
import br.com.linkcom.sined.geral.bean.enumeration.VendaSituacaoBoletoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Vendasituacaoentrega;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.EmporiumpdvService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.geral.service.VendamaterialmestreService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.SomatorioConfirmadoParcial;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.VendaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.VendaFormapagamentoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.VendasPorVendedorOutrosFaturamentosBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.VendasPorVendendorBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.AnaliseVendaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.ApuracaovendaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EmitiracompanhamentometaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EmitircustovendaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.QuantitativaDeVendasFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.RelatorioMargemContribuicaoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.VendaFormapagamentoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.VendasPorVendedorFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.CustooperacionalFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.EmitircurvaabcBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.EmitircurvaabcFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

import com.ibm.icu.math.BigDecimal;

public class VendaDAO extends GenericDAO<Venda> {
	
	private EmporiumpdvService emporiumpdvService;
	private MaterialcategoriaService materialcategoriaService;
	private EmpresaService empresaService;
	private VendamaterialmestreService vendamaterialmestreService;
	private CategoriaService categoriaService;
	
	public void setMaterialcategoriaService(
			MaterialcategoriaService materialcategoriaService) {
		this.materialcategoriaService = materialcategoriaService;
	}
	public void setEmporiumpdvService(EmporiumpdvService emporiumpdvService) {
		this.emporiumpdvService = emporiumpdvService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	} 
	public void setVendamaterialmestreService(VendamaterialmestreService vendamaterialmestreService) {
		this.vendamaterialmestreService = vendamaterialmestreService;
	}
	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Venda> query) {
		query
	     .leftOuterJoinFetch("venda.endereco endereco")
	     .leftOuterJoinFetch("venda.pedidovendatipo pedidovendatipo")
	     .leftOuterJoinFetch("venda.pedidovenda pedidovenda")
	     .leftOuterJoinFetch("venda.parceiro parceiro")
	     .leftOuterJoinFetch("venda.vendasituacao vendasituacao")
	     .leftOuterJoinFetch("venda.frete frete")
	     .leftOuterJoinFetch("venda.prazopagamento prazopagamento")
	     .leftOuterJoinFetch("venda.documentotipo documentotipo")
	     .leftOuterJoinFetch("venda.indicecorrecao indicecorrecao")
	     .leftOuterJoinFetch("venda.contaboleto contaboleto")
	     .leftOuterJoinFetch("endereco.municipio municipio")
	     .leftOuterJoinFetch("municipio.uf uf")
	     .leftOuterJoinFetch("venda.enderecoFaturamento enderecoFaturamento")
	     .leftOuterJoinFetch("enderecoFaturamento.municipio municipioFaturamento")
	     .leftOuterJoinFetch("municipioFaturamento.uf ufFaturamento")
	     .leftOuterJoinFetch("venda.listadocumentoorigem listadocumentoorigem")
	     .leftOuterJoinFetch("listadocumentoorigem.documento documento")
	     .leftOuterJoinFetch("venda.localarmazenagem localarmazenagem");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Venda> query,
			FiltroListagem _filtro) {
	
		
		VendaFiltro filtro = (VendaFiltro) _filtro;	
		
		if (filtro.getDtinicio() == null)
			query.where("1=0");		
		
		/**
		 * 	filtro dependente de Projeto (JOIN TEM QUE FICAR EM PRIMEIRO NA QUERY PARA OTIMIZA��O) 
		 */
		if(filtro.getProjeto() != null && filtro.getProjeto().getCdprojeto() != null){
			query.leftOuterJoin("venda.projeto projeto")
				.where("projeto = ?", filtro.getProjeto())
				.ignoreJoin("projeto");
		}
		
		query
		.select("distinct venda.cdvenda, venda.dtvenda, venda.valorfrete, venda.expedicaovendasituacao, venda.taxapedidovenda, venda.identificadorexterno, " +
				"venda.desconto, venda.taxavenda, venda.valorusadovalecompra, venda.expedicaovendasituacao, venda.identificador, venda.dtprazoentregamax, " +
				"vendasituacao.cdvendasituacao, vendasituacao.descricao, venda.limitecreditoexcedido, venda.sinalizadoSemReserva, venda.expedicaosituacao, " +
				"venda.entregaEcommerceRealizada, " +
				"empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.exibiripivenda, " +
				"cliente.nome, cliente.identificador, cliente.creditolimitecompra, " +
				"pedidovendatipo.vendasEcommerce, pedidovendatipo.cdpedidovendatipo, pedidovendatipo.gerarexpedicaovenda, pedidovendatipo.exibirindicadorentregavenda, " +
				"endereco.cdendereco, endereco.cep, endereco.complemento, endereco.numero, endereco.logradouro, endereco.descricao, endereco.substituirlogradouro, " +
				"municipio.cdmunicipio, municipio.nome, " +
				"uf.cduf, uf.nome, uf.sigla, "+
				"centrocusto.nome, centrocusto.cdcentrocusto, venda.vendaPendenciaNaoNegociada, pedidovenda.cdpedidovenda")
				
	     .leftOuterJoin("venda.empresa empresa")
	     .leftOuterJoin("venda.pedidovenda pedidovenda")
	     .leftOuterJoin("venda.vendasituacao vendasituacao")
	     .leftOuterJoin("venda.cliente cliente")
	     .leftOuterJoin("venda.endereco endereco")
	     .leftOuterJoin("endereco.municipio municipio")
	     .leftOuterJoin("municipio.uf uf")
	     .leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
	     .leftOuterJoin("venda.centrocusto centrocusto")
	     .leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")	     
	     .leftOuterJoin("venda.listavendamaterial vendamaterial")
		 .leftOuterJoin("vendamaterial.listaExpedicaoitem expedicaoitem")
		 .leftOuterJoin("expedicaoitem.expedicao expedicao")	     
	     .where("venda.localarmazenagem = ?", filtro.getLocalarmazenagem())
	     .where("venda.cliente = ?",filtro.getCliente())
	     .where("venda.clienteindicacao = ?",filtro.getClienteindicacao())
	     .where("venda.empresa = ?",filtro.getEmpresa())
	     .where("venda.dtvenda >= ?", filtro.getDtinicio())
	     .where("venda.dtvenda <= ?", filtro.getDtfim())
	     .whereIn("venda.cdvenda", filtro.getWhereIncdvenda())
	     .where("venda.vendaorcamento.cdvendaorcamento = ?", filtro.getCdvendaorcamento())
	     .where("venda.pedidovenda.cdpedidovenda = ?", filtro.getCdpedidovenda())
	     .where("venda.identificadorcarregamento = ?", filtro.getIdentificadorcarregamento())
	     .whereIn("venda.identificadorexterno", SinedUtil.montaWhereInStringVersaoNova(filtro.getIdentificacaoexterna(),";"))
	     .where("venda.prazopagamento = ?", filtro.getPrazopagamento())
	     .where("venda.identificador = ?", filtro.getIdentificador())
	     .where("coalesce(venda.sinalizadoSemReserva, false) = ?", filtro.getSinalizadoSemReserva())
	     .where("pedidovendatipo = ?",filtro.getPedidovendatipo())
	     .where("venda.expedicaosituacao = ?",filtro.getExpedicaosituacao())
	     .orderBy("venda.dtvenda desc, venda.cdvenda desc") 
	     .ignoreJoin("vendamaterial","expedicao", "expedicaoitem","material", "materialgrupo", "materialtipo", "materialcategoria", "listavendapagamento", 
	    		 "documentotipo", "documentotipoparcelas", "prazopagamento", "listaClientevendedor", "colaboradorcliente",
	    		 "listaVendavalorcampoextra", "materialmestregrade", "listaFornecedor", "fornecedorRepresentacao");
		
		if(filtro.getDocumentotipo() != null){
			query
				.leftOuterJoin("venda.listavendapagamento listavendapagamento")
			    .openParentheses()
				    .where("venda.documentotipo = ?", filtro.getDocumentotipo()).or()
				    .where("listavendapagamento.documentotipo = ?", filtro.getDocumentotipo())
			    .closeParentheses();
		}

		if(filtro.getNaoexibirresultado() != null && filtro.getNaoexibirresultado()){
			query.where("1=0");
		}
		
		preencheWhereQueryVenda(query, filtro);
		
		if(filtro.getEmporiumpdv() != null){
			Emporiumpdv emporiumpdv = emporiumpdvService.loadForIntegracaoEmporium(filtro.getEmporiumpdv());
			query.where("venda.empresa = ?", emporiumpdv != null ? emporiumpdv.getEmpresa() : null);
			
			Integer codigoemporium = null;
			try{
				codigoemporium = Integer.parseInt(emporiumpdv.getCodigoemporium());
			} catch (Exception e) {}
			
			query
				.join("venda.listaEmporiumvenda listaEmporiumvenda")
				.where("listaEmporiumvenda.pdv = ?", codigoemporium != null ? codigoemporium.toString() : null);
		
		}
		
		if(filtro.getRegiao() != null){
			Regiao regiao = filtro.getRegiao();
			if(regiao.getListaRegiaolocal() != null && regiao.getListaRegiaolocal().size() > 0){
				query.openParentheses();
				for (Regiaolocal regiaolocal : regiao.getListaRegiaolocal()) {
					query
						.openParentheses()
						.where("endereco.cep >= ?", regiaolocal.getCepinicio())
						.where("endereco.cep <= ?", regiaolocal.getCepfinal())
						.closeParentheses()
						.or();
				}
				query.closeParentheses();
			}
			if(Hibernate.isInitialized(regiao.getListaRegiaovendedor()) && SinedUtil.isListNotEmpty(regiao.getListaRegiaovendedor())){
				query.openParentheses();
				for (Regiaovendedor regiaovendedor : regiao.getListaRegiaovendedor()) {
					query.where("venda.colaborador = ?", regiaovendedor.getColaborador()).or();
				}
				query.closeParentheses();
			}
		}
		
		if(filtro.getDepartamento() != null){
			query
				.where("exists ( " +
						"select c.cdpessoa " +
						"from Colaboradorcargo cc " +
						"join cc.colaborador c " +
						"join cc.departamento d " +
						"where c.id = venda.colaborador.id " +
						"and d.id = " + filtro.getDepartamento().getCddepartamento() + " " +
						"and cc.dtfim is null " +
						" )");
		}
		
		if(filtro.getIsRestricaoVendaVendedor() != null && filtro.getIsRestricaoVendaVendedor()){ 
			Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
			if(colaborador == null) {
				filtro.setNaoexibirresultado(true);
			}else {
				if(!Boolean.TRUE.equals(filtro.getIsRestricaoClienteVendedor())){
					filtro.setColaborador(colaborador);
					query.where("venda.colaborador = ?", colaborador);					
				}else {
					if(filtro.getColaborador() == null || !filtro.getColaborador().equals(colaborador)){
						if(filtro.getColaborador() == null){
							query.openParentheses()
								.where("listaClientevendedor.colaborador = ?", colaborador)
								.or()
								.where("venda.colaborador = ?", colaborador)
							.closeParentheses();
						}else {
							query.openParentheses()
								.where("listaClientevendedor.colaborador = ?", colaborador)
								.where("venda.colaborador = ?", filtro.getColaborador())
							.closeParentheses();
						}
					}else {
						query.where("venda.colaborador = ?", colaborador);
					}
					
				}
			}
		}else {
			if (filtro.getColaborador() != null && filtro.getTipoVendedor() != null && TipoVendedorEnum.PRINCIPAL.equals(filtro.getTipoVendedor())) {
				query
					.openParentheses()
						.where("listaClientevendedor.colaborador = ?", filtro.getColaborador())
						.where("listaClientevendedor.principal = ?", Boolean.TRUE)
					.closeParentheses();
			} else {
				query.where("venda.colaborador = ?",filtro.getColaborador());
			}
		}
		
		List<Vendasituacao> listaVendasituacao = filtro.getListaVendasituacao();
		if (listaVendasituacao != null && !listaVendasituacao.isEmpty()){
			query.whereIn("venda.vendasituacao.cdvendasituacao", CollectionsUtil.listAndConcatenate(listaVendasituacao,"cdvendasituacao", ","));
		}
		
		if (filtro.getEnderecoStr()!=null && !filtro.getEnderecoStr().trim().equals("")){
			query.whereLikeIgnoreAll("(coalesce(endereco.logradouro,'') || coalesce(endereco.numero,'') || coalesce(endereco.complemento,'') || " +
									 " coalesce(endereco.bairro,'') || coalesce(endereco.cep,'') || coalesce(endereco.pontoreferencia,'') || " +
									 " coalesce(municipio.nome,'') || coalesce(uf.sigla,''))", filtro.getEnderecoStr());
		}
		
		
		if(filtro.getVendaSituacaoBoleto() != null){

			//Filtra apenas as vendas com boletos enviados
			if(filtro.getVendaSituacaoBoleto().equals(VendaSituacaoBoletoEnum.BOLETOSENVIADOS)){
				query
				.where(	"(( "+
							"select count(d.cddocumento) "+
							"from Documentoorigem dc "+
							"join dc.documento d " +
							"where "+
							"venda.cdvenda = dc.venda and "+
							"exists ("+
									 "select 1 "+
									 "from Documentohistorico dh "+
									 "where dh.documento = d.cddocumento and ( "+
									 	"dh.documentoacao = "+Documentoacao.ENVIO_BOLETO.getCddocumentoacao()+" or "+
									 	"dh.documentoacao = "+Documentoacao.BOLETO_GERADO.getCddocumentoacao()+" or "+
									 	"dh.documentoacao = "+Documentoacao.ENVIO_MANUAL_BOLETO.getCddocumentoacao()+
									 ") "+
									") "+
							"group by dc.venda "+
						") "+
						"= "+
						"( "+
							"select count(d.cddocumento) "+ 
						    "from Documentoorigem dc "+
						    "join dc.documento d " +
						    "where "+
						    "venda.cdvenda = dc.venda "+
						    "group by dc.venda "+
						"))");
	
			}
			//Filtra apenas as vendas com boletos n�o enviados
			else if(filtro.getVendaSituacaoBoleto().equals(VendaSituacaoBoletoEnum.BOLETOSNAOENVIADOS)){
				query
				.openParentheses()	
				.where(	"(( "+
							"select count(d.cddocumento) "+
							"from Documentoorigem dc "+
							"join dc.documento d "+
							"where "+
							"venda.cdvenda = dc.venda and "+
							"not exists ( "+
								"select 1 "+
								"from Documentohistorico dh "+
								"where dh.documento = d.cddocumento and ( "+
									"dh.documentoacao = "+Documentoacao.ENVIO_BOLETO.getCddocumentoacao()+" or "+
								 	"dh.documentoacao = "+Documentoacao.BOLETO_GERADO.getCddocumentoacao()+" or "+
								 	"dh.documentoacao = "+Documentoacao.ENVIO_MANUAL_BOLETO.getCddocumentoacao()+
								") "+
							") "+
							"group by dc.venda "+
							") > 0)")
				.or()
				.where ("not exists( "+
						"select 1 "+
						"from Documentoorigem ddc "+
						"where venda.cdvenda = ddc.venda)")
				.closeParentheses();
			}
		}
		
		query.where("vendamaterial.prazoentrega>=?", filtro.getPrazoentregainicio());
	    query.where("vendamaterial.prazoentrega<=?", filtro.getPrazoentregafim());
	    
	    if(filtro.getLimitecreditoexcedido() != null){
			query.where("COALESCE(venda.limitecreditoexcedido, false) = " + (filtro.getLimitecreditoexcedido() ? "true" : "false"));
		}
	    
	    if(filtro.getVendaPendenciaNaoNegociada() != null){
	    	if(filtro.getVendaPendenciaNaoNegociada()){
	    		query.where("venda.vendaPendenciaNaoNegociada is true");
	    	}else {
	    		query.where("venda.vendaPendenciaNaoNegociada is not true");
	    	}
	    }
	    
	    if(filtro.getNaoexibirresultado() != null && filtro.getNaoexibirresultado()){
			query.where("1=0");
		}

		if(filtro.getListaExpedicaosituacaoentrega() != null) {
			query.whereIn("expedicao.expedicaosituacaoentrega", Expedicaosituacaoentrega.listAndConcatenate(filtro.getListaExpedicaosituacaoentrega()));
		}

		if(filtro.getListaVendasituacaoentrega() != null) {
			Boolean entregaRealizada=false;
			Boolean entregaPendete=false;
			
			for(int i=0;i<filtro.getListaVendasituacaoentrega().size();i++){				
				if(((Object)filtro.getListaVendasituacaoentrega().get(i)).equals(Vendasituacaoentrega.REALIZADA.name()))
					entregaRealizada=true;
				else if(((Object)filtro.getListaVendasituacaoentrega().get(i)).equals(Vendasituacaoentrega.PENDENTE.name()))
					entregaPendete=true;
			}
			if(entregaPendete && entregaRealizada)
				query.where("((vendasituacao.cdvendasituacao = 2 and (venda.entregaEcommerceRealizada =false or venda.entregaEcommerceRealizada is null))"+
							" or venda.entregaEcommerceRealizada =true)");
			if(entregaPendete && !entregaRealizada)
				query.where("vendasituacao.cdvendasituacao = 2 and (venda.entregaEcommerceRealizada =false or venda.entregaEcommerceRealizada is null)" );
			else if(entregaRealizada && !entregaPendete)
				query.where("venda.entregaEcommerceRealizada =true");
			
		}

		
		
		
		
	}
	
	private void joinVendamaterialUpdatelistagem(QueryBuilder<? extends Object> query){
		query
		 .leftOuterJoinIfNotExists("venda.listavendamaterial vendamaterial")
		 .leftOuterJoinIfNotExists("vendamaterial.material material")
		 .leftOuterJoinIfNotExists("material.materialmestregrade materialmestregrade")
		 .leftOuterJoinIfNotExists("material.materialgrupo materialgrupo")
		 .leftOuterJoinIfNotExists("material.materialtipo materialtipo")
		 .leftOuterJoinIfNotExists("material.listaFornecedor listaFornecedor");
	}
	
	private void preencheWhereQueryVenda(QueryBuilder<? extends Object> query, VendaFiltro filtro){
		if(filtro.getCategoria() != null){
			query
				.leftOuterJoinIfNotExists("cliente.listaPessoacategoria listaPessoacategoria")
				.where("listaPessoacategoria.categoria = ?", filtro.getCategoria());
		}
		
		if(filtro.getMaterial() != null || filtro.getMaterialgrupo() != null || filtro.getMaterialtipo() != null || 
				filtro.getFornecedorRepresentacao() != null || filtro.getPrazoentregainicio()!=null ||
				filtro.getPrazoentregafim()!=null || filtro.getMaterialcategoria() != null){
			query.leftOuterJoinIfNotExists("venda.listavendamaterial vendamaterial");
			
			if(filtro.getMaterial() != null || filtro.getMaterialgrupo() != null ||
				filtro.getMaterialtipo() != null || filtro.getMaterialcategoria() != null){
				query.leftOuterJoinIfNotExists("vendamaterial.material material");
				
				if(filtro.getMaterialgrupo() != null){
					query.leftOuterJoinIfNotExists("material.materialgrupo materialgrupo");
					query.where("materialgrupo = ?", filtro.getMaterialgrupo());
				}
				
				if(filtro.getMaterialtipo() != null){
					query.leftOuterJoinIfNotExists("material.materialtipo materialtipo");
					query.where("materialtipo = ?", filtro.getMaterialtipo());
				}
				
				if(filtro.getMaterialcategoria() != null){
//					query.leftOuterJoinIfNotExists("material.materialcategoria materialcategoria");
//					query.where("materialcategoria = ?", filtro.getMaterialcategoria());
					
					materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
					query.whereIn("material.materialcategoria.cdmaterialcategoria", CollectionsUtil.listAndConcatenate(materialcategoriaService.findForFitlro(filtro.getMaterialcategoria().getIdentificador()), "cdmaterialcategoria", ","));
				}
				
				if(filtro.getMaterial() != null){
					query.leftOuterJoinIfNotExists("material.materialmestregrade materialmestregrade");
					
					query.openParentheses()
					.where("material = ?", filtro.getMaterial())
					.or()
					.where("vendamaterial.materialmestre = ?", filtro.getMaterial())
					.or()
					.where("materialmestregrade = ?", filtro.getMaterial());
					if(filtro.getMaterial().getVendapromocional() != null && filtro.getMaterial().getVendapromocional()){
						query.leftOuterJoin("vendamaterial.materialmestre materialmestre");
						query.ignoreJoin("materialmestre");
						query.or().where("materialmestre = ?", filtro.getMaterial());
					}
					query.closeParentheses();
				}
			}
			
			if(filtro.getFornecedorRepresentacao() != null){
				query.where("vendamaterial.fornecedor = ?", filtro.getFornecedorRepresentacao());
			}
		}
		
		if(filtro.getCampoextrapedidovendatipo() != null 
				&& filtro.getCampoextrapedidovendatipo().getCdcampoextrapedidovendatipo() != null 
				&& filtro.getValorCampoExtra() != null && !filtro.getValorCampoExtra().trim().isEmpty()){
			query
				.leftOuterJoinIfNotExists("venda.listaVendavalorcampoextra listaVendavalorcampoextra")
				.where("listaVendavalorcampoextra.campoextrapedidovendatipo = ?", filtro.getCampoextrapedidovendatipo())
				.whereLikeIgnoreAll("listaVendavalorcampoextra.valor", filtro.getValorCampoExtra());
		}
		
		List<String> whereInNotSstatusComprovante = new ArrayList<String>();
		if(Boolean.TRUE.equals(filtro.getComprovanteEmitido())){
			query.where("exists(select lvh.acao from Vendahistorico lvh where lvh.venda.cdvenda = venda.cdvenda and lvh.acao = ?)", Comprovantestatus.EMITIDO.toString());
		}else if(Boolean.FALSE.equals(filtro.getComprovanteEmitido())){
			whereInNotSstatusComprovante.add("'"+Comprovantestatus.EMITIDO.toString()+"'");
		}
		if(Boolean.TRUE.equals(filtro.getComprovanteEnviado())){
			query.where("exists(select lvh.acao from Vendahistorico lvh where lvh.venda.cdvenda = venda.cdvenda and lvh.acao = ?)", Comprovantestatus.ENVIADO.toString());
		}else if(Boolean.FALSE.equals(filtro.getComprovanteEnviado())){
			whereInNotSstatusComprovante.add("'"+Comprovantestatus.ENVIADO.toString()+"'");
		}

		if(!whereInNotSstatusComprovante.isEmpty()){
			query.where("not exists(select lvh.acao from Vendahistorico lvh where lvh.venda.cdvenda = venda.cdvenda and lvh.acao in ("+
					CollectionsUtil.concatenate(whereInNotSstatusComprovante, ",")+"))");
		}
		
		if(filtro.getListaPedidovendasituacaoecommerce() != null && filtro.getListaPedidovendasituacaoecommerce().size() > 0){
			String whereInIdSituacao = CollectionsUtil.listAndConcatenate(filtro.getListaPedidovendasituacaoecommerce(), "idsecommerce", ",");
			query.where("pedidovenda.cdpedidovenda is not null");
			query.where("exists(select ecp.id " +
							"from Ecom_PedidoVenda ecp " +
							"where ecp.pedidoVenda = pedidovenda " +
							"and ecp.id_situacao in (" + whereInIdSituacao + "))");
		}
	}
	
	/**
	 * Busca todas as vendas a partir da query da listagem para o resumo de tipo de documento.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/10/2012
	 */
	public List<Venda> findForResumoDocumentotipo(VendaFiltro filtro) {
		QueryBuilder<Venda> query = query();
		this.updateListagemQuery(query, filtro);
		this.joinVendamaterialUpdatelistagem(query);
		
		query.select("venda.cdvenda, documentotipoparcelas.cddocumentotipo, documentotipoparcelas.nome, listavendapagamento.valororiginal");
		query.leftOuterJoinIfNotExists("venda.listavendapagamento listavendapagamento");
		query.leftOuterJoinIfNotExists("listavendapagamento.documentotipo documentotipoparcelas");
		
		query.setIgnoreJoinPaths(new HashSet<String>());
		
		return query.list();
	}
	
	/**
	 *
	 * @param filtro
	 * @author Thiago Clemente
	 * 
	 */
	public List<Venda> findForResumoPedidovendatipo(VendaFiltro filtro) {
		QueryBuilder<Venda> query = query();
		this.updateListagemQuery(query, filtro);
		this.joinVendamaterialUpdatelistagem(query);
		
		query.select("venda.cdvenda, documentotipoparcelas.cddocumentotipo, documentotipoparcelas.nome, listavendapagamento.valororiginal, pedidovenda.cdpedidovenda, " +
					 "pedidovendatipo.cdpedidovendatipo, pedidovendatipo.descricao, venda.desconto, venda.valorusadovalecompra, venda.valorfrete, venda.taxapedidovenda, " +
					 "pedidovendatipoPV.cdpedidovendatipo, pedidovendatipoPV.descricao ");

		query.leftOuterJoinIfNotExists("venda.listavendapagamento listavendapagamento");
		query.leftOuterJoinIfNotExists("listavendapagamento.documentotipo documentotipoparcelas");
		query.leftOuterJoinIfNotExists("venda.pedidovendatipo pedidovendatipo");
		query.leftOuterJoinIfNotExists("venda.pedidovenda pedidovenda");
		query.leftOuterJoinIfNotExists("pedidovenda.pedidovendatipo pedidovendatipoPV");
		
		query.setIgnoreJoinPaths(new HashSet<String>());
		
		return query.list();
	}
	
	/**
	 * Busca todas as vendas a partir da query da listagem para o resumo de colaborador/grupo de material.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/12/2012
	 */
	public List<Venda> findForResumoColaboradorMaterialgrupo(VendaFiltro filtro) {
		QueryBuilder<Venda> query = query();
		this.updateListagemQuery(query, filtro);
		this.joinVendamaterialUpdatelistagem(query);
		
		query.select("venda.cdvenda, venda.desconto, venda.valorusadovalecompra, venda.valorfrete, venda.taxapedidovenda, materialgrupo.cdmaterialgrupo, " +
						"materialgrupo.nome, colaborador.cdpessoa, colaborador.nome, vendamaterial.preco, vendamaterial.quantidade, " +
						"vendamaterial.multiplicador, vendamaterial.desconto");
		query.leftOuterJoinIfNotExists("venda.colaborador colaborador");
		query.setIgnoreJoinPaths(new HashSet<String>());
		
		return query.list();
	}
	
	/**
	* M�todo para buscar todas as venda de acordo com o filtro, para ser calculado o total geral
	*
	* @param filtro
	* @return
	* @since Oct 31, 2011
	* @author Luiz Fernando F Silva
	*/
	public VendaFiltro findForCalculoTotalGeralVendas(VendaFiltro filtro){
		
		StringBuilder sql = new StringBuilder();
		
		sql
			.append("select sum(totalvenda) as totalvenda, sum(totalqtde) as totalqtde ")
			.append("from(select v.cdvenda, sum(vm.quantidade) as totalqtde, sum((vm.preco * coalesce(vm.quantidade,1) * coalesce(vm.multiplicador,1)) - (coalesce(vm.desconto,0)/100) + (coalesce(vm.valoripi,0)/100) ) + (coalesce(v.taxapedidovenda,0)/100) + (coalesce( v.valorfrete, 0)/100) - coalesce(v.desconto,0)/100 as totalvenda ")
			.append("from venda v ")
			.append("left outer join vendamaterial vm on vm.cdvenda = v.cdvenda ")
			.append("left outer join material m on m.cdmaterial = vm.cdmaterial ");
		
		sql.append(this.criaQueryTotalGeral(filtro, false, false));	
		
		sql.append(" group by v.cdvenda, v.valorfrete, v.desconto) query ");
		

		SinedUtil.markAsReader();
		VendaFiltro vendaFiltro = (VendaFiltro)getJdbcTemplate().queryForObject(sql.toString(), 
		new RowMapper() {
			public VendaFiltro mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new VendaFiltro(new Money(rs.getDouble("totalvenda")), rs.getDouble("totalqtde"));
			}
		});
		
		if(vendaFiltro != null)
			return vendaFiltro;
		else
			return new VendaFiltro();
	}
	
	/**
	 * M�todo que calcula o saldo total das vendas
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Money findForCalculoSaldoVendas(VendaFiltro filtro, Boolean bonificacao){
		StringBuilder sql = new StringBuilder();
		
		sql
			.append("select sum(saldofinal) as saldoprodutos ")
			.append("from(select distinct v.cdvenda, v.saldofinal as saldofinal ")
			.append("from venda v ")
			.append("left outer join vendamaterial vm on vm.cdvenda = v.cdvenda ")
			.append("left outer join material m on m.cdmaterial = vm.cdmaterial ");
		
		sql.append(this.criaQueryTotalGeral(filtro, true, bonificacao));	
		
		sql.append(" group by v.cdvenda, v.saldofinal ) query ");
		

		SinedUtil.markAsReader();		
		Money saldo = (Money)getJdbcTemplate().queryForObject(sql.toString(), 
		new RowMapper() {
			public Money mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Money(new Money(rs.getDouble("saldoprodutos"), true));
			}
		});
		
		if(saldo != null)
			return saldo;
		else
			return new Money();
	}
	
	/**
	 * M�todo que cria query para os totais na venda 
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	private String criaQueryTotalGeral(VendaFiltro filtro, Boolean saldoproduto, Boolean bonificacao ) {
		StringBuilder query = new StringBuilder();
		Boolean clausulaWhere = Boolean.FALSE; 
		
		//join
		query.append("left outer join cliente c on c.cdpessoa = v.cdcliente ");
//		query.append("left outer join clientevendedor clientev on clientev.cdcliente= c.cdpessoa ");
//		query.append("left outer join colaborador colcliente on colcliente.cdpessoa= clientev.cdcolaborador ");
		query.append("left outer join colaborador co on co.cdpessoa = v.cdcolaborador ");		
		
		if(filtro.getClienteindicacao() != null && filtro.getClienteindicacao().getCdpessoa() != null)
			query.append("left outer join cliente cindicacao on cindicacao.cdpessoa = v.cdclienteindicacao ");
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null)
			query.append("left outer join empresa e on e.cdpessoa = v.cdempresa ");
		if(filtro.getCdvendaorcamento() != null)
			query.append("left outer join vendaorcamento vo on vo.cdvendaorcamento = v.cdvendaorcamento ");
		if(filtro.getProjeto() != null && filtro.getProjeto().getCdprojeto() != null)
			query.append("left outer join projeto p on p.cdprojeto = v.cdprojeto ");
		if(filtro.getPrazopagamento() != null && filtro.getPrazopagamento().getCdprazopagamento() != null)
			query.append("left outer join prazopagamento pp on pp.cdprazopagamento = v.cdprazopagamento ");
		if(filtro.getRegiao() != null){
			query.append("left outer join endereco ed on ed.cdendereco = v.cdenderecoentrega ");
		}
		if (filtro.getCampoextrapedidovendatipo() != null 
				&& filtro.getCampoextrapedidovendatipo().getCdcampoextrapedidovendatipo() != null 
				&& filtro.getValorCampoExtra() != null && !filtro.getValorCampoExtra().trim().isEmpty()){
			query.append("left outer join vendavalorcampoextra vce on v.cdvenda = vce.cdvenda ");
					
		}
		
//		if(saldoproduto != null && saldoproduto){
//			query.append("left outer join pedidovenda pv on pv.cdpedidovenda = v.cdpedidovenda ");
//			query.append("left outer join pedidovendatipo pvt on pvt.cdpedidovendatipo = pv.cdpedidovendatipo ");
//		}
		
		if((filtro.getPedidovendatipo() != null && filtro.getPedidovendatipo().getCdpedidovendatipo() != null) || 
				(saldoproduto != null && saldoproduto)){
			query.append("left outer join pedidovendatipo pvt on pvt.cdpedidovendatipo = v.cdpedidovendatipo ");
		}

		//where
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy"); 
		if(filtro.getDtinicio() != null){
			query.append(" where v.dtvenda >= to_date('" + format.format(filtro.getDtinicio()) + "', 'DD/MM/YYYY') ");
			clausulaWhere = Boolean.TRUE;
		}
		
		if(filtro.getDtfim() != null){
			if(clausulaWhere)
				query.append(" and v.dtvenda <= to_date('" + format.format(filtro.getDtfim()) + "', 'DD/MM/YYYY') ");
			else{
				query.append(" where v.dtvenda <= to_date('" + format.format(filtro.getDtfim()) + "', 'DD/MM/YYYY') ");
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(StringUtils.isNotBlank(filtro.getWhereIncdvenda())){
			if(clausulaWhere)
				query.append(" and v.cdvenda in (" + filtro.getWhereIncdvenda() + ")");
			else{
				query.append(" where v.cdvenda in (" + filtro.getWhereIncdvenda() + ")");
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null){
			if(clausulaWhere)
				query.append(" and vm.cdmaterial = " + filtro.getMaterial().getCdmaterial());
			else{
				query.append(" where vm.cdmaterial = " + filtro.getMaterial().getCdmaterial());
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null){
			if(clausulaWhere)
				query.append(" and e.cdpessoa = " + filtro.getEmpresa().getCdpessoa());
			else{
				query.append(" where e.cdpessoa = " + filtro.getEmpresa().getCdpessoa());
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(filtro.getCliente() != null && filtro.getCliente().getCdpessoa() != null){
			if(clausulaWhere)
				query.append(" and c.cdpessoa = " + filtro.getCliente().getCdpessoa());
			else{
				query.append(" where c.cdpessoa = " + filtro.getCliente().getCdpessoa());
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(filtro.getClienteindicacao() != null && filtro.getClienteindicacao().getCdpessoa() != null){
			if(clausulaWhere)
				query.append(" and cindicacao.cdpessoa = " + filtro.getClienteindicacao().getCdpessoa());
			else{
				query.append(" where cindicacao.cdpessoa = " + filtro.getClienteindicacao().getCdpessoa());
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(filtro.getRegiao() != null){
			Regiao regiao = filtro.getRegiao();
			if(regiao.getListaRegiaolocal() != null && regiao.getListaRegiaolocal().size() > 0){
				if(clausulaWhere)
					query.append(" and ( ");
				else{
					query.append(" where ( ");
					clausulaWhere = Boolean.TRUE;
				}
				
				for (Regiaolocal regiaolocal : regiao.getListaRegiaolocal()) {
					query
						.append(" ( ")
						.append("ed.cep >= '").append(regiaolocal.getCepinicio()).append("' ")
						.append(" AND ed.cep <= '").append(regiaolocal.getCepfinal()).append("' ")
						.append(" ) OR ");
				}
				query.delete(query.length() - 3, query.length());
				query.append(" ) "); 
			}
		}
		
		if((filtro.getIsRestricaoVendaVendedor() != null && filtro.getIsRestricaoVendaVendedor()) || 
				(filtro.getIsRestricaoClienteVendedor() != null && filtro.getIsRestricaoClienteVendedor()) ){ 
			Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
			if(colaborador == null) {
				query.append(clausulaWhere ? " and " : " where ").append(" 1 = 0 ");
				clausulaWhere = Boolean.TRUE;
			}else {
				if(filtro.getIsRestricaoClienteVendedor()){
					query.append(clausulaWhere ? " and ( " : " where ( ");
					query.append(" exists (select 1 from cliente cRestricao " +
							"	left outer join clientevendedor cvRestricao on cvRestricao.cdcliente = cRestricao.cdpessoa " +
							"	where cRestricao.cdpessoa = v.cdcliente and (");
					query.append(" cvRestricao.cdcolaborador = " + colaborador.getCdpessoa() + " ");
					query.append(" or cvRestricao.cdcliente is null ) ))");
					clausulaWhere = Boolean.TRUE;					
				}
				if(filtro.getIsRestricaoVendaVendedor()){
					query.append(clausulaWhere ? " and " : " where ");
					query.append(" co.cdpessoa = " + colaborador.getCdpessoa());	
					clausulaWhere = Boolean.TRUE;
				}else if(filtro.getColaborador() != null){
					query.append(clausulaWhere ? " and " : " where ");
					query.append(" co.cdpessoa = " + filtro.getColaborador().getCdpessoa());	
					clausulaWhere = Boolean.TRUE;
				}
			}
		}else if(filtro.getColaborador() != null){
			query.append(clausulaWhere ? " and " : " where ");
			query.append(" co.cdpessoa = " + filtro.getColaborador().getCdpessoa());	
			clausulaWhere = Boolean.TRUE;
		}
		
		if(filtro.getMaterialgrupo() != null && filtro.getMaterialgrupo().getCdmaterialgrupo() != null){
			if(clausulaWhere)
				query.append(" and m.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo());
			else{
				query.append(" where m.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo());
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(filtro.getMaterialtipo() != null && filtro.getMaterialtipo().getCdmaterialtipo() != null){
			if(clausulaWhere)
				query.append(" and m.cdmaterialtipo = " + filtro.getMaterialtipo().getCdmaterialtipo());
			else{
				query.append(" where m.cdmaterialtipo = " + filtro.getMaterialtipo().getCdmaterialtipo());
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(filtro.getDocumentotipo() != null && filtro.getDocumentotipo().getCddocumentotipo() != null){
			if(clausulaWhere){
				query.append(" and (v.cdformapagamento = " + filtro.getDocumentotipo().getCddocumentotipo());
				query.append(" or v.cdvenda in (select vp.cdvenda from vendapagamento vp where vp.cdformapagamento = " + filtro.getDocumentotipo().getCddocumentotipo() + " and vp.cdvenda = v.cdvenda))");
			}else{
				query.append(" where (v.cdformapagamento = " + filtro.getDocumentotipo().getCddocumentotipo());
				query.append(" or v.cdvenda in (select vp.cdvenda from vendapagamento vp where vp.cdformapagamento = " + filtro.getDocumentotipo().getCddocumentotipo() + " and vp.cdvenda = v.cdvenda))");
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(filtro.getLocalarmazenagem() != null && filtro.getLocalarmazenagem().getCdlocalarmazenagem() != null){
			if(clausulaWhere)
				query.append(" and v.cdlocalarmazenagem = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem());
			else{
				query.append(" where v.cdlocalarmazenagem = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem());
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(filtro.getProjeto() != null && filtro.getProjeto().getCdprojeto() != null){
			if(clausulaWhere)
				query.append(" and p.cdprojeto = " + filtro.getProjeto().getCdprojeto());
			else{
				query.append(" where p.cdprojeto = " + filtro.getProjeto().getCdprojeto());
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(filtro.getIdentificadorcarregamento() != null){
			if(clausulaWhere)
				query.append(" and v.identificadorcarregamento = " + filtro.getIdentificadorcarregamento());
			else{
				query.append(" where v.identificadorcarregamento = " + filtro.getIdentificadorcarregamento());
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(filtro.getPrazopagamento() != null && filtro.getPrazopagamento().getCdprazopagamento() != null){
			if(clausulaWhere)
				query.append(" and pp.cdprazopagamento = " + filtro.getPrazopagamento().getCdprazopagamento());
			else{
				query.append(" where pp.cdprazopagamento = " + filtro.getPrazopagamento().getCdprazopagamento());
				clausulaWhere = Boolean.TRUE;
			}
		}
		if(filtro.getCdvendaorcamento() != null){
			if(clausulaWhere)
				query.append(" and vo.cdvendaorcamento = " + filtro.getCdvendaorcamento());
			else{
				query.append(" where vo.cdvendaorcamento = " + filtro.getCdvendaorcamento());
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(saldoproduto != null && saldoproduto){			
			if(bonificacao != null && bonificacao){
				if(clausulaWhere)
					query.append(" and pvt.bonificacao = true ");
				else{
					query.append(" where pvt.bonificacao = true ");
					clausulaWhere = Boolean.TRUE;
				}
			}else {
				if(clausulaWhere)
					query.append(" and (pvt.bonificacao is null or pvt.bonificacao = false ) ");
				else{
					query.append(" where (pvt.bonificacao is null or pvt.bonificacao = false ) ");
					clausulaWhere = Boolean.TRUE;
				}
			}
		}
		
		if(filtro.getPedidovendatipo() != null && filtro.getPedidovendatipo().getCdpedidovendatipo() != null){
			if(clausulaWhere)
				query.append(" and pvt.cdpedidovendatipo = " + filtro.getPedidovendatipo().getCdpedidovendatipo());
			else{
				query.append(" where pvt.cdpedidovendatipo = " + filtro.getPedidovendatipo().getCdpedidovendatipo());
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		List<Vendasituacao> listaVendasituacao = filtro.getListaVendasituacao();
		if(listaVendasituacao != null){
			if(clausulaWhere)
				query.append(" and v.cdvendasituacao in (" + CollectionsUtil.listAndConcatenate(listaVendasituacao,"cdvendasituacao", ",") + ") ");
			else{
				query.append(" where v.cdvendasituacao in (" + CollectionsUtil.listAndConcatenate(listaVendasituacao,"cdvendasituacao", ",") + ") ");
				clausulaWhere = Boolean.TRUE;
			}
		}	
		
		if(filtro.getCampoextrapedidovendatipo() != null && filtro.getCampoextrapedidovendatipo().getCdcampoextrapedidovendatipo() != null && filtro.getValorCampoExtra() != null && !filtro.getValorCampoExtra().trim().isEmpty()){
			if (clausulaWhere) {
				query.append(" and vce.cdcampoextrapedidovendatipo = '" + filtro.getCampoextrapedidovendatipo().getCdcampoextrapedidovendatipo() + "'");
				query.append(" and (upper(retira_acento(vce.valor)) like ('%' ||'" + filtro.getValorCampoExtra().toUpperCase() + "'|| '%')) ");
			} else {
				query.append(" where vce.cdcampoextrapedidovendatipo = " + filtro.getCampoextrapedidovendatipo());
				query.append(" and (upper(retira_acento(vce.valor)) like ('%' ||'" + filtro.getValorCampoExtra().toUpperCase() + "'|| '%')) ");
			}
		}
		
		return query.toString();
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {		
		Venda bean = (Venda) save.getEntity();
		if (bean.getVendasituacao().equals(Vendasituacao.REALIZADA) || (bean.getSalvarItens() != null && bean.getSalvarItens())){
			if (bean.getListavendamaterial() != null && !bean.getListavendamaterial().isEmpty())
				save.saveOrUpdateManaged("listavendamaterial");
			if (bean.getListaVendamaterialmestre() != null && !bean.getListaVendamaterialmestre().isEmpty())
				save.saveOrUpdateManaged("listaVendamaterialmestre");
		}else {
			if (Hibernate.isInitialized(bean.getListaVendamaterialmestre()) && bean.getListaVendamaterialmestre() != null && !bean.getListaVendamaterialmestre().isEmpty()){
				for(Vendamaterialmestre vendamaterialmestre : bean.getListaVendamaterialmestre()){
					vendamaterialmestreService.updateNomeAlternativo(vendamaterialmestre);
				}
			}
		}
		if(bean.getSalvarPagamentos() != null && bean.getSalvarPagamentos()){
			save.saveOrUpdateManaged("listavendapagamento");
		}
		if(Hibernate.isInitialized(bean.getListaVendaFornecedorTicketMedio())){
			save.saveOrUpdateManaged("listaVendaFornecedorTicketMedio");
		}
		if(Hibernate.isInitialized(bean.getListaVendavalorcampoextra()) && bean.getListaVendavalorcampoextra() != null && !bean.getListaVendavalorcampoextra().isEmpty())
			save.saveOrUpdateManaged("listaVendavalorcampoextra");
	}
	
	/**
	 * M�todo para carregar uma venda.
	 * 
	 * @param id
	 * @return
	 * @author Ramon Brazil
	 */
	public Venda loadVenda(Integer id){
		if(id == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
	
		.select("venda.cdvenda, venda.dtvenda, venda.dtestorno, venda.valorusadovalecompra, venda.valorfrete, vendamaterial.quantidade, vendamaterial.preco, vendamaterial.desconto, venda.desconto, venda.identificadorexterno, venda.identificador, " +
				"cliente.cdpessoa, cliente.nome, cliente.email, contato.cdpessoa, contato.nome, colaborador.cdpessoa, colaborador.nome, colaborador.email, venda.vendasituacao, venda.observacao, frete.cdResponsavelFrete, " +
				"frete.nome, terceiro.nome, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.inscricaoestadual, empresa.email, empresa.site, logomarca.cdarquivo, " +
				"contaboleto.cdconta, contaboleto.taxaboleto, endereco.logradouro, endereco.numero, endereco.bairro, endereco.cep, endereco.complemento, municipio.nome, municipio.uf," +
				"prazopagamento.cdprazopagamento, prazopagamento.nome, documentotipo.nome, empresa.observacaovenda, loteestoque.numero, loteestoque.validade, endereco.descricao, endereco.substituirlogradouro," +
				"vendaorcamento.cdvendaorcamento, vendaorcamentopedido.cdvendaorcamento, pedidovenda.cdpedidovenda, projeto.cdprojeto, projeto.nome, venda.taxapedidovenda," +
				"venda.observacaointerna, vendamaterial.ordem, vendamaterial.prazoentrega, nota.numero, nota.dtEmissao, pedidovendatipo.descricao, venda.dtaltera, materialbanda.cdmaterial, materialbanda.nome, materialbanda.identificacao, "+
				"campoextra.valor, campoextrapedidovendatipo.nome, vendamaterial.peso, vendamaterial.valoripi, vendamaterial.multiplicador, "+
				"colaborador.cpf, colaborador.cnpj, centrocusto.nome, centrocusto.cdcentrocusto, localarmazenagem.nome")
		.leftOuterJoin("venda.cliente cliente")
		.leftOuterJoin("venda.contato contato")
		.leftOuterJoin("venda.listavendamaterial vendamaterial")
		.leftOuterJoin("vendamaterial.pneu pneu")
		.leftOuterJoin("pneu.materialbanda materialbanda")
		.leftOuterJoin("vendamaterial.loteestoque loteestoque")
		.leftOuterJoin("venda.colaborador colaborador")
		.leftOuterJoin("venda.terceiro terceiro")
		.leftOuterJoin("venda.frete frete")
		.leftOuterJoin("venda.vendasituacao vendasituacao")
		.leftOuterJoin("venda.empresa empresa")
		.leftOuterJoin("empresa.logomarca logomarca")
		.leftOuterJoin("venda.contaboleto contaboleto")
		.leftOuterJoin("venda.endereco endereco")
		.leftOuterJoin("endereco.municipio municipio")
		.leftOuterJoin("municipio.uf uf")
		.leftOuterJoin("venda.prazopagamento prazopagamento")
		.leftOuterJoin("venda.documentotipo documentotipo")
		.leftOuterJoin("venda.vendaorcamento vendaorcamento")
		.leftOuterJoin("venda.pedidovenda pedidovenda")		
		.leftOuterJoin("pedidovenda.vendaorcamento vendaorcamentopedido")
		.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
		.leftOuterJoin("venda.projeto projeto")
		.leftOuterJoin("venda.listaNotavenda listaNotavenda")
		.leftOuterJoin("listaNotavenda.nota nota")
		.leftOuterJoin("venda.listaVendavalorcampoextra campoextra")
		.leftOuterJoin("campoextra.campoextrapedidovendatipo campoextrapedidovendatipo")
		.leftOuterJoin("venda.centrocusto centrocusto")
		.leftOuterJoin("venda.localarmazenagem localarmazenagem")
		
		.where("venda.cdvenda=?",id)
		.orderBy("vendamaterial.ordem")
		.unique();
	}
	
	/**
	 * M�todo que carrega v�rias vendas
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Venda> findVendaReport(String whereIn){
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()	
				.select("venda.cdvenda, venda.dtvenda, venda.dtestorno, venda.valorfrete, vendamaterial.ordem, vendamaterial.quantidade, vendamaterial.preco, vendamaterial.desconto, venda.desconto, venda.identificadorexterno, venda.identificador, " +
						"cliente.cdpessoa, cliente.razaosocial, cliente.inscricaoestadual, contato.nome, contato.cdpessoa, colaborador.cdpessoa, colaborador.nome, colaborador.email, venda.vendasituacao, venda.observacao, frete.cdResponsavelFrete, " +
						"frete.nome, terceiro.nome, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.inscricaoestadual, empresa.email, empresa.site, logomarca.cdarquivo, " +
						"contaboleto.cdconta, contaboleto.taxaboleto,  prazopagamento.nome, endereco.logradouro, endereco.numero, endereco.bairro, endereco.cep, " +
						"endereco.complemento, municipio.nome, municipio.uf, documentotipo.nome, empresa.observacaovenda, vendaorcamento.cdvendaorcamento," +
						"vendaorcamentopedido.cdvendaorcamento, pedidovenda.cdpedidovenda, loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade," +
						"projeto.cdprojeto, projeto.nome, venda.observacaointerna, vendamaterial.prazoentrega, pedidovendatipo.descricao, venda.dtaltera, " +
						"materialbanda.cdmaterial, materialbanda.nome, materialbanda.identificacao, venda.origemwebservice, venda.valorusadovalecompra, localarmazenagem.nome ")
				.leftOuterJoin("venda.cliente cliente")
				.leftOuterJoin("venda.contato contato")
				.leftOuterJoin("venda.listavendamaterial vendamaterial")
				.leftOuterJoin("venda.colaborador colaborador")
				.leftOuterJoin("venda.terceiro terceiro")
				.leftOuterJoin("venda.frete frete")
				.leftOuterJoin("venda.vendasituacao vendasituacao")
				.leftOuterJoin("venda.empresa empresa")
				.leftOuterJoin("empresa.logomarca logomarca")
				.leftOuterJoin("venda.contaboleto contaboleto")
				.leftOuterJoin("venda.prazopagamento prazopagamento")
				.leftOuterJoin("venda.documentotipo documentotipo")
				.leftOuterJoin("venda.endereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("venda.vendaorcamento vendaorcamento")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("venda.pedidovenda pedidovenda")
				.leftOuterJoin("pedidovenda.vendaorcamento vendaorcamentopedido")
				.leftOuterJoin("vendamaterial.loteestoque loteestoque")
				.leftOuterJoin("vendamaterial.pneu pneu")
				.leftOuterJoin("pneu.materialbanda materialbanda")
				.leftOuterJoin("venda.projeto projeto")
				.leftOuterJoin("venda.localarmazenagem localarmazenagem")
				.whereIn("venda.cdvenda",whereIn)
				.orderBy("vendamaterial.ordem")
				.list();
	}
	/**
	 * M�todo que carrega v�rias vendas
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Venda> findVendaComprovante(String whereIn){
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()	
				.select("venda.cdvenda, venda.dtvenda, venda.dtestorno, venda.valorfrete, vendamaterial.ordem, vendamaterial.quantidade, vendamaterial.preco, vendamaterial.desconto, venda.desconto, venda.identificadorexterno, venda.identificador, " +
						"cliente.cdpessoa, cliente.razaosocial, cliente.inscricaoestadual, colaborador.cdpessoa, colaborador.nome, colaborador.email, venda.vendasituacao, venda.observacao, frete.cdResponsavelFrete, " +
						"frete.nome, terceiro.nome, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.inscricaoestadual, empresa.email, empresa.site, logomarca.cdarquivo, " +
						"contaboleto.cdconta, contaboleto.taxaboleto,  prazopagamento.nome, endereco.logradouro, endereco.numero, endereco.bairro, endereco.cep, " +
						"endereco.complemento, municipio.nome, municipio.uf, documentotipo.nome, empresa.observacaovenda, vendaorcamento.cdvendaorcamento," +
						"vendaorcamentopedido.cdvendaorcamento, pedidovenda.cdpedidovenda, loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade," +
						"projeto.cdprojeto, projeto.nome, nota.numero, nota.dtEmissao, vendamaterial.peso ")
				.leftOuterJoin("venda.cliente cliente")
				.leftOuterJoin("venda.listavendamaterial vendamaterial")
				.leftOuterJoin("venda.colaborador colaborador")
				.leftOuterJoin("venda.terceiro terceiro")
				.leftOuterJoin("venda.frete frete")
				.leftOuterJoin("venda.vendasituacao vendasituacao")
				.leftOuterJoin("venda.empresa empresa")
				.leftOuterJoin("empresa.logomarca logomarca")
				.leftOuterJoin("venda.contaboleto contaboleto")
				.leftOuterJoin("venda.prazopagamento prazopagamento")
				.leftOuterJoin("venda.documentotipo documentotipo")
				.leftOuterJoin("venda.endereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("venda.vendaorcamento vendaorcamento")
				.leftOuterJoin("venda.pedidovenda pedidovenda")
				.leftOuterJoin("pedidovenda.vendaorcamento vendaorcamentopedido")
				.leftOuterJoin("vendamaterial.loteestoque loteestoque")
				.leftOuterJoin("venda.projeto projeto")
				.leftOuterJoin("venda.listaNotavenda listaNotavenda")
				.leftOuterJoin("listaNotavenda.nota nota")
				.whereIn("venda.cdvenda",whereIn)
				.orderBy("vendamaterial.ordem")
				.list();
	}
	
	
	/**
	 * Busca as vendas para valida��o no cancelamento.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public List<Venda> findForValidacaoCancelamento(String whereIn){
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()	
				.select("venda.cdvenda, venda.identificador")
				.whereIn("venda.cdvenda",whereIn)
				.list();
	}
	
	/**
	 * Atualiza os dados da venda alterados no crud
	 * @param venda
	 * @author Thiago Gona�ves
	 */
	public void updateVenda(Venda venda){
		String sql = "update venda set cdenderecoentrega = " + (venda.getEndereco() == null ? "null" : venda.getEndereco().getCdendereco()) +
					 " , cdusuarioaltera ="+venda.getCdusuarioaltera() +", dtaltera = ?, "+
					 "observacao = '" + venda.getObservacao() + "', frete = ?, terceiro = ?, valorfrete = ? where cdvenda = " + venda.getCdvenda();
		getJdbcTemplate().update(sql, new Object[]{venda.getDtaltera(), venda.getFrete(), venda.getTerceiro(), venda.getValorfrete()});
	}
		
	/**
	 * Aprova uma venda com situa��o "Or�amento"
	 * @param venda
	 * @author Thiago Gon�alves
	 * @param producao 
	 */
	public void aprovaOrcamento(Venda venda, boolean producao){
		String sql = "update venda set cdvendasituacao = " + (producao ? Vendasituacao.EMPRODUCAO.getCdvendasituacao() : Vendasituacao.REALIZADA.getCdvendasituacao()) + 
					 ", cdusuarioaltera = "+SinedUtil.getUsuarioLogado().getCdpessoa()+", dtaltera = ? where cdvenda = " + venda.getCdvenda();
		getJdbcTemplate().update(sql, new Object[]{new Date(System.currentTimeMillis())});
	}
	
	/**
	 * Atualiza a situa��o da venda
	 * @param venda
	 * @param vendasituacao
	 * @author Thiago Gon�alves
	 */
	public void atualizaSituacao(Venda venda, Vendasituacao vendasituacao){
		String sql = "update venda set cdvendasituacao = " + vendasituacao.getCdvendasituacao() + 
		 			 " where cdvenda = " + venda.getCdvenda();
		getJdbcTemplate().execute(sql);
	}
	
	/**
	 * Retorna a listagem de venda para o relat�rio de ordem de produ��o.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Venda> findForOrdemProducao(String whereIn) {
		return query()
					.select("venda.cdvenda, venda.dtvenda, colaborador.nome, cliente.nome, " +
							"cliente.cnpj, cliente.cpf")
					.leftOuterJoin("venda.cliente cliente")
					.leftOuterJoin("venda.colaborador colaborador")
					.whereIn("venda.cdvenda", whereIn)
					.list();
	}

	/**
	 * M�todo que retorna a data da �ltima venda cadastrada no sistema.
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public java.sql.Date getDtUltimoCadastro() {
		Timestamp data = newQueryBuilderSined(Timestamp.class)
			.from(Venda.class)
			.setUseTranslator(false) 
			.select("max(venda.dtvenda)")
			.join("venda.vendasituacao vendasituacao")
			.where("vendasituacao <> ?", Vendasituacao.CANCELADA)
			.unique();
		return data != null ? new Date(data.getTime()) : null;
	}

	/**
	 * M�todo que retorna o total de vendas cadastradas no sistema
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getQtdeTotalVenda() {
		return newQueryBuilderSined(Long.class)
			
			.from(Venda.class)
			.setUseTranslator(false) 
			.select("count(*)")
			.join("venda.vendasituacao vendasituacao")
			.where("vendasituacao <> ?", Vendasituacao.CANCELADA)
			.unique()
			.intValue();
	}
	
	/**
	 * Verifica se a situa��o da venda informada como par�metro � cancelada.
	 * @param venda
	 * @return
	 * @author Taidson
	 * @since 12/01/2011
	 */
	public boolean notaSituacaoCancelada(Venda venda){
		return newQueryBuilderSined(Long.class)
		.select("count(*)")
		.from(Venda.class)
		.join("venda.listaNotavenda listaNotavenda")
		.join("listaNotavenda.nota nota")
		.join("nota.notaStatus notaStatus")
		.where("venda = ?", venda)
		.where("notaStatus = ?", NotaStatus.CANCELADA)
		.unique() > 0;
	}
	
	/**
	 * Verifica se a situa��o da venda informada como par�metro � cancelada.
	 * @param venda
	 * @return
	 * @author Taidson
	 * @since 12/01/2011
	 */
	public boolean vendaSituacaoCancelada(Nota nota){
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Venda.class)
				.join("venda.listaNotavenda listaNotavenda")
				.join("listaNotavenda.nota nota")
				.where("nota = ?", nota)
				.where("venda.vendasituacao = ?", Vendasituacao.CANCELADA)
				.unique() > 0;
	}

	public List<Venda> findByCliente(Cliente cliente) {
		return query()
			.select("venda.cdvenda, venda.dtvenda, material.nome, listavendamaterial.preco, listavendamaterial.desconto, listavendamaterial.quantidade")
			.join("venda.listavendamaterial listavendamaterial")
			.join("listavendamaterial.material material")
			.where("venda.cliente=?", cliente)
			.list();
				
	}

	/**
	 * Verifica se tem alguma venda nas situa��es passadas por par�metro.
	 *
	 * @param situacoes
	 * @return
	 * @param whereIn 
	 * @author Tom�s Rabelo
	 */
	public boolean haveVendaSituacao(String whereIn, Boolean validateEndereco, Vendasituacao[] situacoes) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < situacoes.length; i++) {
			sb.append(situacoes[i].getCdvendasituacao());
			if((i + 1) < situacoes.length){
				sb.append(",");
			}
		}
		
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Venda.class)
					.join("venda.vendasituacao vendasituacao")
					.leftOuterJoin("venda.endereco endereco")
					.whereIn("venda.cdvenda", whereIn);
		
		if(validateEndereco)
					query.openParentheses()
						.whereIn("vendasituacao.cdvendasituacao", sb.toString()).or()
						.where("endereco is null")
					.closeParentheses();
		else
			query.whereIn("vendasituacao.cdvendasituacao", sb.toString());
		return query.unique() > 0;
	}
	
	public boolean isAprovando(String whereIn) {
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Venda.class)
				.leftOuterJoin("venda.endereco endereco")
				.whereIn("venda.cdvenda", whereIn)
				.where("coalesce(venda.aprovando, false) = true")
				.unique() > 0;
	}
	
	public boolean existeEmpresaDistintas(String whereIn) {
		if(StringUtils.isEmpty(whereIn)) return false;
		
		String sql = "select distinct v.cdempresa " +
					 "from venda v " +
				 	 "where v.cdvenda in (" + whereIn + ")";		
		
		@SuppressWarnings("unchecked")
		List<Integer> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getInt("cdempresa");
			}
		});
		
		return list != null && list.size() > 1;
	}
	
	public void updateAprovandoVenda(String whereIn, boolean aprovando){
		getJdbcTemplate().update("update venda set aprovando = " +  aprovando + " where cdvenda in (" + whereIn + ")");
	}

	/**
	 * M�todo que carrega as vendas para gerar nota fiscal produto
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Venda> findForCobranca(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		
		return query()
			.select("venda.cdvenda, venda.dtvenda, venda.identificadorexterno, venda.identificador, venda.observacao, venda.taxavenda, cliente.cdpessoa, endereco.cdendereco, enderecoFaturamento.cdendereco, listavendamaterial.cdvendamaterial, listavendamaterial.quantidade, listavendamaterial.preco, listavendamaterial.desconto, material.cdmaterial, " +
					"material.nomenf, material.produto, material.servico, material.patrimonio, material.epi, unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, venda.presencacompradornfe, " +
					"listadocumentoorigem.cddocumentoorigem ,documento.cddocumento, venda.desconto, venda.valorusadovalecompra, venda.valoraproximadoimposto, material.peso, material.pesobruto, " +
					"documento.dtvencimento, documento.valor, documento.numero, contaboleto.cdconta, contaboleto.nome, documentotipoVenda.exibirnavenda, documentotipoVenda.cddocumentotipo, documentotipoVenda.nome, " +
					"documentotipo.exibirnavenda, documentotipo.nome, frete.cdResponsavelFrete, frete.nome, venda.valorfrete, terceiro.cdpessoa, vendasituacao.cdvendasituacao, " +
					"empresa.cdpessoa, empresa.discriminarservicoqtdevalor, materialgrupo.cdmaterialgrupo, materialgrupo.nome, ncmcapitulo.cdncmcapitulo, material.ncmcompleto, material.nve, material.tributacaomunicipal, " +
					"materialgrupo.cdmaterialgrupo, ncmcapituloMestre.cdncmcapitulo, vendamaterialmestrematerial.ncmcompleto, vendamaterialmestrematerial.nve, vendamaterialmestrematerial.tributacaomunicipal, " +
					"prazopagamento.cdprazopagamento, prazopagamento.nome, prazopagamento.considerarjurosnota, prazopagamento.avista, " +
					"loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, codigoanp.cdcodigoanp, codigoanp.codigo, codigoanpMestre.cdcodigoanp, codigoanpMestre.codigo, cliente.cnpj, material.metrocubicovalorvenda, " +
					"material.servico, material.produto, material.epi, material.vendapromocional, material.patrimonio, material.nome, pedidovenda.cdpedidovenda, listavendamaterial.multiplicador, " +
					"vendamaterialmestre.cdvendamaterialmestre, vendamaterialmestre.altura, vendamaterialmestre.qtde, material.codigobarras, " +
					"vendamaterialmestre.valoripi, vendamaterialmestre.ipi, vendamaterialmestre.aliquotaReaisIpi, vendamaterialmestre.tipocobrancaipi, vendamaterialmestre.tipoCalculoIpi, " +
					"vendamaterialmestre.valoricms, vendamaterialmestre.icmsst, vendamaterialmestre.valoricmsst, vendamaterialmestre.fcp, vendamaterialmestre.valorfcp, " +
					"vendamaterialmestre.fcpst, vendamaterialmestre.valorfcpst, vendamaterialmestre.difal, vendamaterialmestre.valordifal, " +
					"vendamaterialmestre.percentualdesoneracaoicms, vendamaterialmestre.valordesoneracaoicms, vendamaterialmestre.abaterdesoneracaoicms, " +
					"vendamaterialmestre.largura, vendamaterialmestre.comprimento, vendamaterialmestrematerial.cdmaterial, vendamaterialmestrematerial.nome, vendamaterialmestrematerial.nomenf, " +
					"vendamaterialmestrematerial.cdmaterial, vendamaterialmestrematerial.producao, vendamaterialmestrematerial.vendapromocional, " +
					"vendamaterialmestreunidademedida.cdunidademedida, vendamaterialmestreunidademedida.nome, " +
					"vendamaterialmestrematerial.peso, vendamaterialmestrematerial.pesobruto, material.percentualimpostoecf, " +
					"vendamaterialmestre.nomealternativo, vendamaterialmestre.exibiritenskitflexivel, vendamaterialmestre.preco, vendamaterialmestre.qtde, " +
					"vendamaterialmestre.cdvendamaterialmestre, vendamaterialmestre.dtprazoentrega, vendamaterialmestre.desconto, " +
					"vendamaterialmestrematerial.produto, vendamaterialmestrematerial.servico, vendamaterialmestrematerial.epi, " +
					"vendamaterialmestrematerial.patrimonio, vendamaterialmestrematerial.kitflexivel, " +
					"grupoTributacaoMestre.cdgrupotributacao, " +
					"listavendamaterial.identificadorinterno, vendamaterialmestre.identificadorinterno, listavendamaterial.ordem, listavendamaterial.valorComissao, listavendamaterial.valorcustomaterial, " +
					"listavendamaterial.valoripi, listavendamaterial.ipi, listavendamaterial.tipocobrancaipi, listavendamaterial.tipocalculoipi, listavendamaterial.aliquotareaisipi, grupotributacao.cdgrupotributacao, " +
					"listavendamaterial.valoricms, listavendamaterial.icmsst, listavendamaterial.valoricmsst, listavendamaterial.fcp, listavendamaterial.valorfcp, " +
					"listavendamaterial.fcpst, listavendamaterial.valorfcpst, listavendamaterial.difal, listavendamaterial.valordifal, listavendamaterial.outrasdespesas, listavendamaterial.valorSeguro, " +
					"listavendamaterial.percentualdesoneracaoicms, listavendamaterial.valordesoneracaoicms, listavendamaterial.abaterdesoneracaoicms, " +
					"materialmestre.cdmaterial, materialmestre.nome, materialmestre.nomenf, materialmestre.producao, materialmestre.vendapromocional, materialmestre.kitflexivel, " +
					"pessoaconfiguracao.cdpessoaconfiguracao, transportador.cdpessoa, transportador.nome, " +
					"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.bonificacao, pedidovendatipo.reserva, pedidovendatipo.gerarexpedicaovenda, pedidovendatipo.gerarnotaretornovenda, " +
					"pedidovendatipo.comodato, pedidovendatipo.baixaestoqueEnum, pedidovendatipo.situacaoNota, pedidovendatipo.geracaocontareceberEnum, pedidovendatipo.gerarNotaIndustrializacaoRetorno, " +
					"pedidovendatipo.necessarioExpedicao, " +
					"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome," +
					"naturezaoperacaoexpedicao.cdnaturezaoperacao, naturezaoperacaoexpedicao.nome, " +
					"cfopindustrializacao.cdcfop, cfopindustrializacao.codigo, " +
					"cfopindustrializacao.descricaoresumida, cfopindustrializacao.descricaocompleta, " +
					"cfopretorno.cdcfop, cfopretorno.codigo, projeto.cdprojeto, " +
					"cfopretorno.descricaoresumida, cfopretorno.descricaocompleta, " +
					"listavendamaterial.identificadorespecifico, listavendamaterial.identificadorintegracao, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, listavendamaterial.observacao, listavendamaterial.peso, " +
					"pneu.cdpneu, pneu.serie, pneu.dot, pneu.numeroreforma, " +
					"pneumarca.cdpneumarca, pneumarca.nome, " +
					"pneumedida.cdpneumedida, pneumedida.nome, " +
					"pneumodelo.cdpneumodelo, pneumodelo.nome, " +
					"pneumaterialbanda.cdmaterial, pneumaterialbanda.identificacao, pneumaterialbanda.nome," +
					"pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome," +
					"colaborador.cdpessoa, colaborador.nome, colaborador.cnpj, colaborador.cpf, " +
					"pedidovendatipo.agruparContas, listavendapagamento.cdvendapagamento, pedidovendatipo.reservarApartir, pedidovendatipo.situacaoNota, venda.quantidadevolumes ")
			.join("venda.cliente cliente")
			.join("venda.vendasituacao vendasituacao")
			.leftOuterJoin("venda.colaborador colaborador")
			.leftOuterJoin("venda.prazopagamento prazopagamento")
			.leftOuterJoin("venda.projeto projeto")
			.leftOuterJoin("venda.empresa empresa")
			.leftOuterJoin("empresa.pessoaconfiguracao pessoaconfiguracao")
			.leftOuterJoin("pessoaconfiguracao.transportador transportador")
			.leftOuterJoin("venda.pedidovenda pedidovenda")
			.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
			.leftOuterJoin("pedidovendatipo.naturezaoperacao naturezaoperacao")
			.leftOuterJoin("pedidovendatipo.naturezaoperacaoexpedicao naturezaoperacaoexpedicao")
			.leftOuterJoin("pedidovendatipo.cfopindustrializacao cfopindustrializacao")
			.leftOuterJoin("pedidovendatipo.cfopretorno cfopretorno")
			.leftOuterJoin("venda.endereco endereco")
			.leftOuterJoin("venda.enderecoFaturamento enderecoFaturamento")
			.leftOuterJoin("venda.frete frete")
			.join("venda.listavendamaterial listavendamaterial")
			.leftOuterJoin("listavendamaterial.loteestoque loteestoque")
			.join("listavendamaterial.material material")
			.leftOuterJoin("listavendamaterial.materialmestre materialmestre")
			.leftOuterJoin("material.codigoanp codigoanp")
			.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.join("listavendamaterial.unidademedida unidademedida")
			.leftOuterJoin("listavendamaterial.pneu pneu")
		    .leftOuterJoin("pneu.pneumarca pneumarca")
		    .leftOuterJoin("pneu.pneumodelo pneumodelo")
		    .leftOuterJoin("pneu.pneumedida pneumedida")
		    .leftOuterJoin("pneu.materialbanda pneumaterialbanda")
		    .leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
			.leftOuterJoin("venda.terceiro terceiro")
			.leftOuterJoin("venda.listadocumentoorigem listadocumentoorigem")
			.leftOuterJoin("listadocumentoorigem.documento documento")
			.leftOuterJoin("documento.documentotipo documentotipo")
			.leftOuterJoin("venda.documentotipo documentotipoVenda")
			.leftOuterJoin("venda.contaboleto contaboleto")
			.leftOuterJoin("venda.listaVendamaterialmestre vendamaterialmestre")
			.leftOuterJoin("vendamaterialmestre.grupot" +
					"ributacao grupoTributacaoMestre")
			.leftOuterJoin("vendamaterialmestre.material vendamaterialmestrematerial")
			.leftOuterJoin("vendamaterialmestrematerial.unidademedida vendamaterialmestreunidademedida")
			.leftOuterJoin("vendamaterialmestrematerial.codigoanp codigoanpMestre")
			.leftOuterJoin("vendamaterialmestrematerial.ncmcapitulo ncmcapituloMestre")
			.leftOuterJoin("venda.localarmazenagem localarmazenagem")
			.leftOuterJoin("listavendamaterial.grupotributacao grupotributacao")
			.leftOuterJoin("venda.listavendapagamento listavendapagamento")
			.whereIn("venda.cdvenda", whereIn)
			.orderBy("upper(retira_acento(cliente.nome)), venda.cdvenda, listavendamaterial.ordem, material.nome")
			.list();
	}

	public void updateQtdVolumesVenda(String whereIn, Integer quantidadevolumes) {
		if(whereIn == null || whereIn.equals(""))
			throw new SinedException("Par�metros inv�lidos.");
		
		getHibernateTemplate().bulkUpdate("update Venda v set v.quantidadevolumes = ? where v.id in ("+whereIn+")", new Object[]{quantidadevolumes});
	}
	
	/**
	 * M�todo que muda a situa��o de v�rias vendas
	 * 
	 * @param whereIn
	 * @param situacao
	 * @author Tom�s Rabelo
	 */
	public void updateSituacaoVenda(String whereIn, Vendasituacao situacao) {
		if(whereIn == null || whereIn.equals(""))
			throw new SinedException("Par�metros inv�lidos.");
		
		getHibernateTemplate().bulkUpdate("update Venda v set v.vendasituacao = ? where v.id in ("+whereIn+")", new Object[]{situacao});
	}

	/**
	 * M�todo que busca os dados para o relat�rio de Ordem de retirada
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Venda> findForOrdemRetirada(String whereIn) {
		return query()
			.select("venda.cdvenda, venda.dtvenda, colaborador.nome, cliente.cdpessoa, cliente.nome, cliente.cnpj, cliente.cpf, " +
					"listavendamaterial.cdvendamaterial, listavendamaterial.quantidade, listavendamaterial.preco, listavendamaterial.desconto, " +
					"unidademedida.cdunidademedida, unidademedida.simbolo, unidademedida.nome, material.cdmaterial, material.nome, material.referencia, " +
					"localizacaoestoque.cdlocalizacaoestoque, localizacaoestoque.descricao, materialgrupo.cdmaterialgrupo, materialgrupo.nome, " +
					"listavendamaterial.altura, listavendamaterial.largura, " +
					"endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.inscricaoestadual, endereco.descricao, endereco.substituirlogradouro, " +
					"endereco.complemento, endereco.bairro, endereco.pontoreferencia, endereco.cep, municipio.cdmunicipio, municipio.nome, uf.sigla, " +
					"listavendamaterial.comprimento, listavendamaterial.comprimentooriginal, listavendamaterial.observacao, listavendamaterial.fatorconversao," +
					"venda.observacao, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.inscricaoestadual, logomarca.cdarquivo")
			.join("venda.empresa empresa")
			.join("venda.listavendamaterial listavendamaterial")
			.join("listavendamaterial.material material")
			.join("material.materialgrupo materialgrupo")
			.leftOuterJoin("empresa.logomarca logomarca")
			.leftOuterJoin("venda.cliente cliente")
			.leftOuterJoin("venda.endereco endereco")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("endereco.pais pais")
			.leftOuterJoin("venda.colaborador colaborador")
			.leftOuterJoin("material.localizacaoestoque localizacaoestoque")
			.leftOuterJoin("listavendamaterial.unidademedida unidademedida")
			.whereIn("venda.cdvenda", whereIn)
			.orderBy("venda.cdvenda, listavendamaterial.ordem")
			.list();
	}

	/**
	 * M�todo que busca os dados para o relat�rio An�lise de venda
	 * 
	 * @param filtro
	 * @return
	 * @author Marden Silva
	 */
	public List<Venda> findForAnaliseVenda(AnaliseVendaFiltro filtro, boolean mensal) {		
		List<Vendasituacao> listaSituacao = new ArrayList<Vendasituacao>();
		listaSituacao.add(Vendasituacao.CANCELADA);
		listaSituacao.add(Vendasituacao.AGUARDANDO_APROVACAO);
		
		QueryBuilderSined<Venda> query = querySined();
		query.select("venda.cdvenda, cliente.cdpessoa, cliente.nome, venda.dtvenda, " +
					 "listavendamaterial.cdvendamaterial, material.cdmaterial, " +
					 "material.nome, listavendamaterial.quantidade, cliente.identificador, material.identificacao")				
			 .leftOuterJoin("venda.cliente cliente")
			 .leftOuterJoin("venda.listavendamaterial listavendamaterial")
			 .leftOuterJoin("listavendamaterial.material material")
			 .leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
			 .where("material.materialgrupo = ?", filtro.getMaterialgrupo())
			 .where("material.materialtipo = ?", filtro.getMaterialtipo())
			 .where("venda.empresa = ?", filtro.getEmpresa())
			 .where("venda.cliente = ?", filtro.getCliente())
			 .where("venda.vendasituacao <> ?", Vendasituacao.CANCELADA)
			 .ignoreJoin("listaClientevendedor");
		
		if(filtro.getMaterialcategoria() != null){
			materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
			query.whereIn("material.materialcategoria.cdmaterialcategoria", CollectionsUtil.listAndConcatenate(materialcategoriaService.findForFitlro(filtro.getMaterialcategoria().getIdentificador()), "cdmaterialcategoria", ","));
		}
		
		if(filtro.getArrayColaborador() != null && filtro.getArrayColaborador().length > 0){
			if (filtro.getIsVendedorPrincipal() != null && filtro.getIsVendedorPrincipal()) {
				Boolean first = Boolean.TRUE;
				query.openParentheses();
				
				for (Colaborador colaborador : filtro.getArrayColaborador()) {
					if (!first) {
						query.or();
					}
					
					query
						.openParentheses()
							.where("listaClientevendedor.colaborador = ?", colaborador)
							.where("listaClientevendedor.principal = ?", Boolean.TRUE)
						.closeParentheses();
					
					first = Boolean.FALSE;
				}
				
				query.closeParentheses();
			} else {
				query.whereIn("venda.colaborador", SinedUtil.listAndConcatenate(filtro.getArrayColaborador(), "cdpessoa", ","));
			}
		}

		if(filtro.getCategoria() != null){
			query
				.leftOuterJoin("cliente.listaPessoacategoria listaPessoacategoria")
				.where("listaPessoacategoria.categoria = ?", filtro.getCategoria());
		}
			
		if(listaSituacao != null && !listaSituacao.isEmpty()){
			query.whereIn("venda.vendasituacao not", CollectionsUtil.listAndConcatenate(listaSituacao, "cdvendasituacao", ","));
		}
		
		if (!mensal && filtro.getClienteforaperiodo()) {
			query.where("venda.dtvenda <= ?", new Object[]{new Date(System.currentTimeMillis())});
		} else {	
			query.where("venda.dtvenda >= ?", filtro.getDtinicio())
				 .where("venda.dtvenda <= ?", filtro.getDtfim());
		}	
		
		if((filtro.getIsRestricaoVendaVendedor() != null && filtro.getIsRestricaoVendaVendedor()) || 
				 (filtro.getIsRestricaoClienteVendedor() != null && filtro.getIsRestricaoClienteVendedor())){
			query.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor");
			
			Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
			if(colaborador == null) {
				query.where(" 1 = 0 ");
			}else {
				if(filtro.getIsRestricaoClienteVendedor()){
					query
						.openParentheses()
							.where("listaClientevendedor.colaborador = ?", colaborador)
							.or()
							.where("listaClientevendedor is null")
						.closeParentheses();
				}
				if(filtro.getIsRestricaoVendaVendedor()){
					query.where("venda.colaborador = ?", colaborador);	
				}
			}
		}
		
		return query.orderBy("cliente.nome, venda.dtvenda desc").setUseTranslator(true).list();				
	}

	/**
	 * Metodo que busca a listagem que ser� apresentada no relat�rio
	 * 
	 * @return Venda
	 * @author Thiago Augusto
	 */
	public Venda findVenda(Venda _venda){
		QueryBuilder<Venda> qry = query();
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("venda.cdvenda, venda.dtvenda, ");
		sb.append("cliente.cdpessoa, cliente.nome, cliente.tipopessoa, cliente.cpf, cliente.cnpj, ");
		sb.append("listavendamaterial.cdvendamaterial, listavendamaterial.quantidade, listavendamaterial.preco, listavendamaterial.desconto,");
		sb.append("material.cdmaterial, material.nome, material.nome, material.codigofabricante,");
		sb.append("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, ");
		sb.append("endereco.cep, endereco.pontoreferencia, municipio.nome, uf.sigla, ");
		sb.append("grupoMaterial.cdmaterialgrupo, grupoMaterial.nome, ");
		sb.append("empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.cdpessoa");
		
		
		qry.select(sb.toString())
			 .leftOuterJoin("venda.cliente cliente")
			 .leftOuterJoin("venda.listavendamaterial listavendamaterial")
			 .leftOuterJoin("listavendamaterial.material material")
			 .leftOuterJoin("venda.empresa empresa")
			 .leftOuterJoin("venda.endereco endereco")
			 .leftOuterJoin("endereco.municipio municipio")
			 .leftOuterJoin("municipio.uf uf")
			 .leftOuterJoin("material.materialgrupo grupoMaterial")
			 .where("venda.cdvenda = ?", _venda.getCdvenda());
		return qry.unique();
		
	}
	
	/**
	 * Metodo que retorna a listagem que ser� apresentado no relat�rio. 
	 * 
	 * @return Venda
	 * @author Thiago Augusto
	 */
	public List<Venda> findForRelatorio(VendaFiltro filtro) {
        QueryBuilder<Venda> query = query();
        query.select("distinct venda.cdvenda, empresa.nome, empresa.razaosocial, empresa.nomefantasia, cliente.nome, venda.dtvenda, venda.valorfrete," +
                "endereco.cdendereco, endereco.cep, endereco.complemento, endereco.numero, endereco.logradouro, venda.taxapedidovenda, " +
                "municipio.cdmunicipio, municipio.nome, uf.cduf, uf.nome, uf.sigla, venda.vendasituacao, venda.desconto, venda.valorusadovalecompra, " +
                "venda.taxavenda, vendamaterial.observacao, vendamaterial.prazoentrega, venda.observacao," +
                "vendamaterial.cdvendamaterial, vendamaterial.quantidade, vendamaterial.preco, vendamaterial.valoripi, pedidovendatipo.cdpedidovendatipo, " +
                "vendamaterial.desconto, material.cdmaterial, material.nome, vendamaterial.observacao, vendamaterial.prazoentrega, " +
                "vendamaterial.valoripi, venda.observacao, colaborador.nome, nota.numero")
                .leftOuterJoin("venda.listavendamaterial vendamaterial")
                .leftOuterJoin("vendamaterial.material material")
                .leftOuterJoin("material.materialgrupo materialgrupo")
                .leftOuterJoin("material.materialtipo materialtipo")
                .leftOuterJoin("venda.empresa empresa")
                .leftOuterJoin("venda.vendasituacao vendasituacao")
                .leftOuterJoin("venda.cliente cliente")
                .leftOuterJoin("venda.colaborador colaborador")
                .leftOuterJoin("venda.endereco endereco")
                .leftOuterJoin("endereco.municipio municipio")
                .leftOuterJoin("municipio.uf uf")
                .leftOuterJoin("venda.documentotipo documentotipo")
                .leftOuterJoin("venda.localarmazenagem localarmazenagem")    
                .leftOuterJoin("venda.prazopagamento prazopagamento")    
                .leftOuterJoin("venda.listavendapagamento listavendapagamento")
                .leftOuterJoin("listavendapagamento.documentotipo documentotipoparcelas")
                .leftOuterJoin("venda.listadocumentoorigem listadocumentoorigem")
                .leftOuterJoin("listadocumentoorigem.documento documento")
                .leftOuterJoin("venda.projeto projeto")
                .leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
                .leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
                .leftOuterJoin("listaClientevendedor.colaborador colaboradorcliente")
                .leftOuterJoin("venda.listaNotavenda listaNotavenda")
                .leftOuterJoin("listaNotavenda.nota nota")
                .where("vendamaterial.fornecedor = ?", filtro.getFornecedorRepresentacao())
                .where("localarmazenagem = ?", filtro.getLocalarmazenagem())
//			    .where("material = ?", filtro.getMaterial())
//			    .where("materialgrupo = ?", filtro.getMaterialgrupo())
//			    .where("materialtipo = ?", filtro.getMaterialtipo())
			.where("cliente = ?",filtro.getCliente())
			.where("empresa = ?",filtro.getEmpresa())
			.where("colaborador = ?",filtro.getColaborador(), filtro.getTipoVendedor() == null)
			.where("venda.dtvenda >= ?", filtro.getDtinicio())
			.where("venda.dtvenda <= ?", filtro.getDtfim())
			.whereIn("venda.cdvenda", filtro.getWhereIncdvenda())
			.where("venda.identificadorcarregamento = ?", filtro.getIdentificadorcarregamento())
			.whereIn("venda.identificadorexterno", SinedUtil.montaWhereInStringVersaoNova(filtro.getIdentificacaoexterna(),";"))
			.where("venda.identificador = ?", filtro.getIdentificador())
			.where("prazopagamento = ?", filtro.getPrazopagamento())
			.where("projeto = ?", filtro.getProjeto())
			.where("pedidovendatipo = ?", filtro.getPedidovendatipo())
			.openParentheses()
			.where("documentotipo = ?", filtro.getDocumentotipo()).or()
			.where("documentotipoparcelas = ?", filtro.getDocumentotipo())
			.closeParentheses()
			.orderBy("venda.dtvenda desc, venda.cdvenda desc");
		
		preencheWhereQueryVenda(query, filtro);
		
		if((filtro.getIsRestricaoVendaVendedor() != null && filtro.getIsRestricaoVendaVendedor()) || 
				(filtro.getIsRestricaoClienteVendedor() != null && filtro.getIsRestricaoClienteVendedor()) ){ 
			Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
			if(colaborador == null) {
				filtro.setNaoexibirresultado(true);
			}else {
				if(filtro.getIsRestricaoClienteVendedor()){
					query
						.openParentheses()
							.where("colaboradorcliente = ?", colaborador)
							.or()
							.where("listaClientevendedor is null")
						.closeParentheses();
					
				}
				if(filtro.getIsRestricaoVendaVendedor()){
					query.where("colaborador = ?", colaborador);	
				}else {
					query.where("colaborador = ?", filtro.getColaborador());	
				}
			}
		} else {
			if (filtro.getColaborador() != null && filtro.getTipoVendedor() != null && TipoVendedorEnum.PRINCIPAL.equals(filtro.getTipoVendedor())) {
				query
					.openParentheses()
						.where("listaClientevendedor.colaborador = ?", filtro.getColaborador())
						.where("listaClientevendedor.principal = ?", Boolean.TRUE)
					.closeParentheses();
			} else {
				query.where("colaborador = ?",filtro.getColaborador());
			}
		}
		
		if(filtro.getNaoexibirresultado() != null && filtro.getNaoexibirresultado()){
			query.where("1=0");
		}
		
		if(filtro.getRegiao() != null){
			Regiao regiao = filtro.getRegiao();
			if(regiao.getListaRegiaolocal() != null && regiao.getListaRegiaolocal().size() > 0){
				query.openParentheses();
				for (Regiaolocal regiaolocal : regiao.getListaRegiaolocal()) {
					query
						.openParentheses()
						.where("endereco.cep >= ?", regiaolocal.getCepinicio())
						.where("endereco.cep <= ?", regiaolocal.getCepfinal())
						.closeParentheses()
						.or();
				}
				query.closeParentheses();
			}
		}
		
		if(filtro.getCampoextrapedidovendatipo() != null 
				&& filtro.getCampoextrapedidovendatipo().getCdcampoextrapedidovendatipo() != null 
				&& filtro.getValorCampoExtra() != null && !filtro.getValorCampoExtra().trim().isEmpty()){
			query.leftOuterJoinIfNotExists("venda.listaVendavalorcampoextra listaVendavalorcampoextra");
			query
				.where("listaVendavalorcampoextra.campoextrapedidovendatipo = ?", filtro.getCampoextrapedidovendatipo())
				.whereLikeIgnoreAll("listaVendavalorcampoextra.valor", filtro.getValorCampoExtra());
		}
		
		List<Vendasituacao> listaVendasituacao = filtro.getListaVendasituacao();
		if (listaVendasituacao != null)
			query.whereIn("venda.vendasituacao.cdvendasituacao", CollectionsUtil.listAndConcatenate(listaVendasituacao,"cdvendasituacao", ","));
				
		if (!StringUtils.isEmpty(filtro.getOrderBy())) {
			query.orderBy(filtro.getOrderBy()+" "+(filtro.isAsc()?"ASC":"DESC"));
		}
		
		return query.list();
	}
	/** M�todo que retorna ordenando da �ltima para a primeira data. 
	 * @author Thiago Augusto
	 * @param cliente
	 * @param isRestricaoVendaVendedor 
	 * @param isRestricaoClienteVendedor 
	 * @return
	 */
	public List<Venda> findByClienteOrderData(Cliente cliente, Boolean isRestricaoClienteVendedor, Boolean isRestricaoVendaVendedor, Empresa empresa) {
		String whereInEmpresas = empresa != null? empresa.getCdpessoa().toString():
			CollectionsUtil.listAndConcatenate(empresaService.findByUsuario(), "cdpessoa", ",");
		
		QueryBuilder<Venda> query = query()
				.select("venda.cdvenda, venda.dtvenda, vendasituacao.cdvendasituacao, material.nome, pedidovendatipo.descricao, " +
						"listavendamaterial.preco, listavendamaterial.desconto, listavendamaterial.quantidade")
				.join("venda.listavendamaterial listavendamaterial")
				.join("listavendamaterial.material material")
				.join("venda.colaborador colaborador")
				.join("venda.empresa empresa")
				.leftOuterJoin("venda.cliente cliente")
				.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
				.leftOuterJoin("listaClientevendedor.colaborador colaboradorcliente")
				.leftOuterJoin("venda.vendasituacao vendasituacao")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.where("cliente = ?", cliente)
				.where("vendasituacao <> ?", Vendasituacao.CANCELADA)
				.whereIn("empresa.cdpessoa", whereInEmpresas);
		
		if((isRestricaoClienteVendedor != null && isRestricaoClienteVendedor) || 
				(isRestricaoVendaVendedor != null && isRestricaoVendaVendedor) ){ 
			Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
			if(colaborador == null) {
				query.where("1=0");
			}else {
				if(!isRestricaoClienteVendedor){
					query.where("colaborador = ?", colaborador);					
				}else {
					query.openParentheses()
						.where("colaboradorcliente = ?", colaborador)
						.or()
						.where("colaborador = ?", colaborador)
					.closeParentheses();
				}
			}
		}
		
		return query
				.orderBy("venda.dtvenda DESC")
				.list();
				
	}

	/**
	 * M�todo que busca a �ltima venda com a forma de pagamento
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public Venda buscaUltimaVendaByCliente(Cliente cliente) {
		if(cliente == null || cliente.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("venda.cdvenda, documentotipo.cddocumentotipo, documentotipo.nome, documentotipo.antecipacao, prazopagamento.cdprazopagamento, prazopagamento.nome ")
				.join("venda.documentotipo documentotipo")
				.leftOuterJoin("venda.prazopagamento prazopagamento")
				.where("venda.cliente = ?", cliente)
				.orderBy("venda.cdvenda desc")
				.setMaxResults(1)				
				.unique();
	}

	/**
	 * M�todo que busca informa��es dos colaboradores/comissao para o comissionamento da venda
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Venda findForComissiaoVenda(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("venda.cdvenda, venda.valorusadovalecompra, listaVendamaterial.cdvendamaterial, material.cdmaterial, cliente.cdpessoa, empresa.cdpessoa, unidademedida.cdunidademedida, listaVendamaterial.qtdereferencia, listaVendamaterial.fatorconversao, " +
						"colaborador.cdpessoa, colaborador.nome, listaVendamaterial.preco, listaVendamaterial.desconto, listaVendamaterial.quantidade, listaVendamaterial.percentualcomissaoagencia, " +
						"venda.percentualdesconto, venda.desconto, prazopagamento.cdprazopagamento, prazopagamento.juros, venda.valorfrete, venda.taxapedidovenda, listaVendamaterial.multiplicador, listaVendamaterial.percentualdesconto, " +
						"listaVendamaterial.valorvendamaterial, pedidovenda.cdpedidovenda, pedidovendatipo.cdpedidovendatipo, pedidovendatipo.exibirescolhaageciavendas, pedidovendatipo.representacao, " +
						"fornecedor.cdpessoa, clienteindicacao.cdpessoa, fornecedorAgencia.cdpessoa, comissionamento.cdcomissionamento, comissionamento.considerardiferencapreco ")
				.leftOuterJoin("venda.listavendamaterial listaVendamaterial")
				.leftOuterJoin("venda.prazopagamento prazopagamento")
				.leftOuterJoin("venda.colaborador colaborador")
				.leftOuterJoin("venda.cliente cliente")
				.leftOuterJoin("venda.empresa empresa")
				.leftOuterJoin("venda.pedidovenda pedidovenda")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.join("listaVendamaterial.material material")
				.leftOuterJoin("listaVendamaterial.unidademedida unidademedida")
				.leftOuterJoin("listaVendamaterial.comissionamento comissionamento")
				.leftOuterJoin("listaVendamaterial.fornecedor fornecedor")
				.leftOuterJoin("venda.clienteindicacao clienteindicacao")
				.leftOuterJoin("venda.fornecedorAgencia fornecedorAgencia")
				.where("venda  = ?", venda)				
				.unique();
	}
	
	/**
	 * Fatura uma venda especificada atrav�s do cdvenda.
	 * @param cdvenda
	 * @author Rafael Salvio
	 */
	public void faturaVenda(Integer cdvenda){
		String sql = "update venda set cdvendasituacao = "+Vendasituacao.FATURADA.getCdvendasituacao()+", cdusuarioaltera ="+SinedUtil.getUsuarioLogado().getCdpessoa() +", dtaltera = ? where cdvenda = " + cdvenda;
		getJdbcTemplate().update(sql, new Object[]{new Date(System.currentTimeMillis())});
	}

	/**
	 * M�todo que busca as informa��es da lista de material da venda
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Venda findForRegistrarDevolucao(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("venda.cdvenda, venda.dtvenda, listavendamaterial.cdvendamaterial, listavendamaterial.quantidade, material.cdmaterial, venda.desconto, venda.valorusadovalecompra, " +
						"pedidovendatipo.baixaestoqueEnum, pedidovendatipo.situacaoNota, " +
						"material.nome, material.servico, vendasituacao.cdvendasituacao, vendasituacao.descricao, listavendamaterial.preco, listavendamaterial.desconto, empresa.cdpessoa," +
						"cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj, empresa.cpf, empresa.cnpj, empresa.cdpessoa, empresa.integracaowms, " +
						"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, projeto.cdprojeto, centrocusto.cdcentrocusto, " +
						"unidademedidavenda.cdunidademedida, unidademedidavenda.nome, unidademedidavenda.simbolo, " +
						"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
						"listavendamaterial.preco, listavendamaterial.altura, listavendamaterial.largura, loteestoque.cdloteestoque, loteestoque.numero, " +
						"listavendamaterial.comprimento, listavendamaterial.comprimentooriginal, listavendamaterial.fatorconversao, listavendamaterial.qtdereferencia," +
						"materialRateioEstoque.cdMaterialRateioEstoque, contaGerencialDevolucaoEntrada.cdcontagerencial, listavendamaterial.multiplicador ")
				.join("venda.cliente cliente")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.join("venda.listavendamaterial listavendamaterial")
				.join("listavendamaterial.material material")
				.join("listavendamaterial.unidademedida unidademedidavenda")
				.leftOuterJoin("listavendamaterial.loteestoque loteestoque")
				.join("material.unidademedida unidademedida")
				.join("venda.vendasituacao vendasituacao")
				.join("venda.empresa empresa")
				.leftOuterJoin("venda.localarmazenagem localarmazenagem")
				.leftOuterJoin("venda.projeto projeto")
				.leftOuterJoin("venda.centrocusto centrocusto")
				.leftOuterJoin("material.materialRateioEstoque materialRateioEstoque")
				.leftOuterJoin("materialRateioEstoque.contaGerencialDevolucaoEntrada contaGerencialDevolucaoEntrada")
				.whereIn("venda.cdvenda", whereIn)
				.unique();
	}
	
	/**
	 * M�todo que busca o vendedor do pedido de venda
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Colaborador findVendedorForVenda(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Par�metro inv�lido.");

		SinedUtil.markAsReader();
		Integer cdcolaborador = getJdbcTemplate()
					.queryForInt("select cdcolaborador from Venda where cdvenda = " + venda.getCdvenda() + " ;");
				
		return cdcolaborador != null ? new Colaborador(cdcolaborador) : null;
	}
	
	/**
	 * M�todo que verifica se a venda est� cancelada
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isVendaCancelada(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Venda.class)
			.join("venda.vendasituacao vendasituacao")
			.where("venda = ?", venda)
			.where("vendasituacao = ?", Vendasituacao.CANCELADA)
			.unique() > 0;
	}
	
	public Boolean isVendaFaturada(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.setUseTranslator(false)
				.from(Venda.class)
				.join("venda.vendasituacao vendasituacao")
				.where("venda = ?", venda)
				.where("vendasituacao = ?", Vendasituacao.FATURADA)
				.unique() > 0;
	}
	
	/**
	 * 
	 * M�todo que busca os dados necess�rios para gerar uma OS � partir do cdvenda.
	 *
	 * @name findVendaByCdVenda
	 * @param cdvenda
	 * @return
	 * @return Venda
	 * @author Thiago Augusto
	 * @date 21/05/2012
	 *
	 */
	public Venda findVendaByCdVenda(Integer cdvenda){
		if (cdvenda == null){
			throw new SinedException("O cdvenda n�o pode ser nulo.");
		}
		
		return query()
					.select("venda.cdvenda, cliente.cdpessoa, cliente.nome, cliente.cnpj, cliente.cpf, venda.desconto, " +
							"listavendamaterial.cdvendamaterial, listavendamaterial.quantidade, listavendamaterial.preco, listavendamaterial.desconto, listavendamaterial.prazoentrega, " +
							"material.cdmaterial, material.nome, material.nome, material.codigofabricante, listavendamaterial.qtdereferencia, listavendamaterial.fatorconversao, " +
							"unidademedida.cdunidademedida, unidademedida.simbolo, unidademedida.nome, listavendamaterial.observacao, listavendamaterial.multiplicador, " +
							"unidademedidavenda.cdunidademedida, unidademedidavenda.simbolo, unidademedidavenda.nome," +
							"colaborador.cdpessoa, colaborador.nome, empresa.cdpessoa, projeto.cdprojeto ")
						.join("venda.cliente cliente")
						.join("venda.colaborador colaborador")
						.leftOuterJoin("venda.empresa empresa")
						.leftOuterJoin("venda.projeto projeto")
						.leftOuterJoin("venda.listavendamaterial listavendamaterial")
						.leftOuterJoin("listavendamaterial.material material")
						.leftOuterJoin("material.unidademedida unidademedida")
						.leftOuterJoin("listavendamaterial.unidademedida unidademedidavenda")
						.where("venda.cdvenda = ? ", cdvenda)
						.orderBy("listavendamaterial.ordem")
						.unique();
	}

	/**
	 * M�todo que carrega informa��es da venda para relat�rio de devolu��o de material
	 *
	 * @param cdvenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Venda findForComprovantedevolucao(String cdvenda) {
		if(cdvenda == null || "".equals(cdvenda))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
		.select("venda.cdvenda, cliente.cdpessoa, cliente.nome ")
			.leftOuterJoin("venda.cliente cliente")
			.where("venda.cdvenda = ? ", Integer.parseInt(cdvenda))
			.unique();
	}

	/**
	 * M�todo que busca as vendas para o relat�rio de curva abc
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 * @since 09/12/2013
	 */
	public List<Venda> findForCurvaabcVendaReport(EmitircurvaabcFiltro filtro) {
		QueryBuilder<Venda> query = querySined();
		
		List<Vendasituacao> listaVendasituacao = new ArrayList<Vendasituacao>();
		listaVendasituacao.add(Vendasituacao.FATURADA);
		listaVendasituacao.add(Vendasituacao.REALIZADA);
		
		if(filtro.getMaterialcategoria() != null){
			Materialcategoria materialcategoria = materialcategoriaService.load(filtro.getMaterialcategoria(), "materialcategoria.cdmaterialcategoria, materialcategoria.vmaterialcategoria");
			filtro.setMaterialcategoria(materialcategoria);
		}
		
		query
			.select("venda.cdvenda, venda.dtvenda, venda.desconto, venda.valorusadovalecompra, venda.valorfrete, " +
					"vendamaterial.cdvendamaterial, vendamaterial.quantidade,vendamaterial.fatorconversao, " +
					"vendamaterial.qtdereferencia, vendamaterial.preco, vendamaterial.desconto, vendamaterial.multiplicador, " +
					"vendamaterial.fatorconversao, vendamaterial.qtdereferencia, vendamaterial.peso, vendamaterial.valorvendamaterial, vendamaterial.valorcustomaterial, " +
					"unidademedidamaterial.cdunidademedida, unidademedida.cdunidademedida, " +
					"material.epi, material.servico, material.produto, material.patrimonio," +
					"material.cdmaterial, material.nome, material.identificacao, material.valorvenda, material.peso, material.pesobruto," +
					"material.metrocubicovalorvenda, material.pesoliquidovalorvenda, material.valorcusto ")
			.leftOuterJoin("venda.vendasituacao vendasituacao")
			.leftOuterJoin("venda.listavendamaterial vendamaterial")
			.leftOuterJoin("vendamaterial.material material")
			.leftOuterJoin("material.unidademedida unidademedidamaterial")
			.leftOuterJoin("vendamaterial.unidademedida unidademedida")
			.where("venda.dtvenda >= ?", filtro.getDtInicio())
			.where("venda.dtvenda <= ?", filtro.getDtFim());
			
		if(listaVendasituacao != null && !listaVendasituacao.isEmpty()){
			query.whereIn("vendasituacao.cdvendasituacao", SinedUtil.listAndConcatenate(listaVendasituacao, "cdvendasituacao", ","));
		}
		
		addJoinWhereCurvaabcVendaReport(filtro, query);
		
		return query.list();				
	}

	/**
	 * M�todo que adiciona join e where na query de curva abc
	 *
	 * @param filtro
	 * @param query
	 * @author Luiz Fernando
	 * @since 09/12/2013
	 */
	private void addJoinWhereCurvaabcVendaReport(EmitircurvaabcFiltro filtro, QueryBuilder<Venda> query) {
		boolean joinCliente = false;
		boolean joinColaborador = false;
		boolean joinFornecedor = false;
		boolean joinMaterialgrupo = false;
		boolean joinMaterialtipo = false;
		boolean joinCidade = false;
		
		
		if(Emitircurvaabcranking.CLIENTE.equals(filtro.getEmitircurvaabcranking()) || filtro.getCliente() != null || filtro.getCategoria() != null || 
				Boolean.TRUE.equals(filtro.getIsVendedorPrincipal())){
			joinCliente = true;
		}
		if(Emitircurvaabcranking.VENDEDOR.equals(filtro.getEmitircurvaabcranking()) || filtro.getColaborador() != null){
			joinColaborador = true;
		}
		if(Emitircurvaabcranking.GRUPOMATERIAL.equals(filtro.getEmitircurvaabcranking()) || filtro.getMaterialgrupo() != null){
			joinMaterialgrupo = true;
		}
		if(Emitircurvaabcranking.FORNECEDOR.equals(filtro.getEmitircurvaabcranking()) || filtro.getFornecedor() != null){
			joinFornecedor = true;
		}
		if(Emitircurvaabcranking.TIPOMATERIAL.equals(filtro.getEmitircurvaabcranking()) || filtro.getMaterialtipo() != null){
			joinMaterialtipo = true;
		}
		if(Emitircurvaabcranking.CIDADE.equals(filtro.getEmitircurvaabcranking()) || filtro.getMunicipio()!=null || filtro.getUf() != null ){
			joinCidade = true;
		}
		
		if(filtro.getEmpresa() != null){
			query.leftOuterJoin("venda.empresa empresa")
			.where("empresa = ?", filtro.getEmpresa());
		}
		if(filtro.getProjeto() != null){
			query.leftOuterJoin("venda.projeto projeto")
			.where("projeto = ?", filtro.getProjeto());
		}
		
		if(joinCliente){
			query.select(query.getSelect().getValue() + ", cliente.cdpessoa, cliente.nome, cliente.identificador ");
			query.leftOuterJoin("venda.cliente cliente")
			.where("cliente = ?", filtro.getCliente());
			
			if(filtro.getCategoria() != null){
				if(filtro.getCategoria() != null){
					filtro.setCategoria(categoriaService.load(filtro.getCategoria(), "categoria.cdcategoria, categoria.vcategoria"));
				}
				
				query.leftOuterJoin("cliente.listaPessoacategoria listaPessoacategoria")
					 .leftOuterJoin("listaPessoacategoria.categoria categoria")
					 .leftOuterJoin("categoria.vcategoria vcategoria")
					 .where("(vcategoria.identificador like '" + filtro.getCategoria().getVcategoria().getIdentificador() + ".%' OR vcategoria.identificador like '" + filtro.getCategoria().getVcategoria().getIdentificador() + "')");
			}
		}
		
		if(joinFornecedor){
			query
				.select(query.getSelect().getValue() + ", fornecedor.cdpessoa, fornecedor.nome ")
				.leftOuterJoin("material.listaFornecedor listaFornecedor")
				.leftOuterJoin("listaFornecedor.fornecedor fornecedor");
		}
		if(joinMaterialgrupo){
			query
				.select(query.getSelect().getValue() + ", materialgrupo.cdmaterialgrupo, materialgrupo.nome ")
				.leftOuterJoin("material.materialgrupo materialgrupo");
		}
		if(joinMaterialtipo){
			query
				.select(query.getSelect().getValue() + ", materialtipo.cdmaterialtipo, materialtipo.nome ")
				.leftOuterJoin("material.materialtipo materialtipo");
		}
		if(filtro.getMaterialcategoria() != null){
			query
				.select(query.getSelect().getValue() + ", materialcategoria.cdmaterialcategoria, vmaterialcategoria.identificador ")
				.leftOuterJoin("material.materialcategoria materialcategoria")
				.leftOuterJoin("materialcategoria.vmaterialcategoria vmaterialcategoria")
				;
		}
		
		if(joinCidade){
			if(!joinCliente){
				joinCliente = true;
				query.leftOuterJoin("venda.cliente cliente");
			}
			query.select(query.getSelect().getValue() + ", municipio.cdmunicipio, municipio.nome, municipioVenda.cdmunicipio, municipioVenda.nome, listaEndereco.enderecotipo ")
			.leftOuterJoin("cliente.listaEndereco listaEndereco")
			.leftOuterJoin("listaEndereco.municipio municipio")
			.leftOuterJoinIfNotExists("venda.endereco enderecovenda")
			.leftOuterJoinIfNotExists("enderecovenda.municipio municipioVenda");
			
			if(filtro.getMunicipio()!=null || filtro.getUf() != null){
				query.openParentheses()
					.openParentheses()
						.where("municipioVenda is not null")
						.where("municipioVenda = ?", filtro.getMunicipio())
						.where("municipioVenda.uf = ?", filtro.getUf())
					.closeParentheses()
					.or()
					.openParentheses()
						.where("municipioVenda is null")
						.where("municipio = ?", filtro.getMunicipio())
						.where("municipio.uf = ?", filtro.getUf())
					.closeParentheses()
				.closeParentheses();
			}
		}
		
		if(Boolean.TRUE.equals(filtro.getConsiderarkitpromocional())){
			query
			.select(query.getSelect().getValue() + ", materialmestre.cdmaterial, materialmestre.nome, materialmestre.identificacao, materialmestre.vendapromocional," +
					"materialmestre.valorvenda, " +
					"vendamaterialmestre.cdvendamaterialmestre, vendamaterialmestre.altura, vendamaterialmestre.qtde, material.codigobarras, " +
					"vendamaterialmestre.largura, vendamaterialmestre.comprimento, vendamaterialmestrematerial.epi, vendamaterialmestrematerial.servico, vendamaterialmestrematerial.produto, vendamaterialmestrematerial.patrimonio, " +
					"vendamaterialmestrematerial.cdmaterial, vendamaterialmestrematerial.nome, vendamaterialmestrematerial.producao, vendamaterialmestrematerial.vendapromocional, " +
					"vendamaterialmestreunidademedida.cdunidademedida, vendamaterialmestreunidademedida.nome, " +
					"vendamaterialmestrematerial.peso, vendamaterialmestrematerial.pesobruto," +
					"vendamaterialmestrematerial.valorvenda, vendamaterialmestrematerial.valorcusto, vendamaterialmestrematerial.identificacao, " +
					"vendamaterialmestrematerial.valorvendaminimo, vendamaterialmestrematerial.valorvendamaximo, vendamaterialmestrematerial.nome, " +
					"vendamaterialmestrematerialgrupo.nome, vendamaterialmestrematerialgrupo.cdmaterialgrupo, " +
					"vendamaterialmestrematerialcategoria.cdmaterialcategoria, vendamaterialmestrematerialcategoria.descricao, " +
					"vendamaterialmestrematerialtipo.cdmaterialtipo, vendamaterialmestrematerialtipo.nome")
			.leftOuterJoin("vendamaterial.materialmestre materialmestre")
			.leftOuterJoin("venda.listaVendamaterialmestre vendamaterialmestre")
			.leftOuterJoin("vendamaterialmestre.material vendamaterialmestrematerial")
			.leftOuterJoin("vendamaterialmestrematerial.unidademedida vendamaterialmestreunidademedida")
			.leftOuterJoin("vendamaterialmestrematerial.materialgrupo vendamaterialmestrematerialgrupo")
			.leftOuterJoin("vendamaterialmestrematerial.materialtipo vendamaterialmestrematerialtipo")
			.leftOuterJoin("vendamaterialmestrematerial.materialcategoria vendamaterialmestrematerialcategoria");
		}else{
			query
				.select(query.getSelect().getValue() + ", materialrelacionado.valorvenda, materialrelacionado.quantidade, " +
						"materialfilho.identificacao, materialfilho.cdmaterial, materialfilho.nome, materialfilho.peso, materialfilho.epi, materialfilho.servico, materialfilho.patrimonio, materialfilho.produto, " +
						"materialtipofilho.cdmaterialtipo, materialtipofilho.nome, materialgrupopromocao.cdmaterialgrupo, materialgrupopromocao.nome, materialcategoriarelacionado.cdmaterialcategoria, materialcategoriarelacionado.descricao")
				.leftOuterJoin("material.listaMaterialrelacionado materialrelacionado")
				.leftOuterJoin("materialrelacionado.materialpromocao materialfilho")
				.leftOuterJoin("materialfilho.materialgrupo materialgrupopromocao")
				.leftOuterJoin("materialfilho.materialcategoria materialcategoriarelacionado")
				.leftOuterJoin("materialfilho.materialtipo materialtipofilho");
		}		
		
		if((filtro.getIsRestricaoVendaVendedor() != null && filtro.getIsRestricaoVendaVendedor()) || 
				(filtro.getIsRestricaoClienteVendedor() != null && filtro.getIsRestricaoClienteVendedor()) ){ 
			Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
			if(colaborador == null) {
				query.where("1=0");
			}else {
				if(filtro.getIsRestricaoClienteVendedor()){
					if(!joinCliente){
						joinCliente = true;
						query.leftOuterJoin("venda.cliente cliente");
					}
					query
						.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
						.leftOuterJoin("listaClientevendedor.colaborador colaboradorcliente")
						.openParentheses()
							.where("colaboradorcliente = ?", colaborador)
							.or()
							.where("listaClientevendedor is null")
						.closeParentheses();
					
				}
				query.select(query.getSelect().getValue() + ", colaborador.cdpessoa, colaborador.nome ");
				query.leftOuterJoin("venda.colaborador colaborador");
				if(filtro.getIsRestricaoVendaVendedor()){
					query.where("colaborador = ?", colaborador);	
				}else {
					query.where("colaborador = ?", filtro.getColaborador());	
				}
			}
		}else if(joinColaborador){
				if(!joinCliente){
					joinCliente = true;
					query.leftOuterJoin("venda.cliente cliente");
				}
				
				query.select(query.getSelect().getValue() + ", colaborador.cdpessoa, colaborador.nome ");
				
				if (filtro.getColaborador() != null && filtro.getIsVendedorPrincipal() != null && filtro.getIsVendedorPrincipal()) {
					query
						.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
						.leftOuterJoin("listaClientevendedor.colaborador colaborador");
					
					query
						.openParentheses()
							.where("listaClientevendedor.colaborador = ?", filtro.getColaborador())
							.where("listaClientevendedor.principal = ?", Boolean.TRUE)
						.closeParentheses();					
				} else {
					query.leftOuterJoin("venda.colaborador colaborador");
					query.where("colaborador = ?",filtro.getColaborador());
				}
		}
		
		if (filtro.getPedidovendatipo() != null){
			query.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				 .where("pedidovendatipo = ?", filtro.getPedidovendatipo());
		}
		
		if(filtro.getRegiao() != null){
			Regiao regiao = filtro.getRegiao();
			if(regiao.getListaRegiaolocal() != null && regiao.getListaRegiaolocal().size() > 0){
				query.select(query.getSelect().getValue() + ", enderecovenda.cdendereco, enderecovenda.cep, enderecovenda.logradouro ");
				query.leftOuterJoinIfNotExists("venda.endereco enderecovenda");
				
				query.openParentheses();
				for(Regiaolocal regiaolocal : regiao.getListaRegiaolocal()){
					query
					.openParentheses()
					.where("enderecovenda.cep >= ?", regiaolocal.getCepinicio())
					.where("enderecovenda.cep <= ?", regiaolocal.getCepfinal())
					.closeParentheses()
					.or();
				}
				query.closeParentheses();
			}
		}
		
		adicionaJoinWhereForMaterial(filtro, query);
		
	}

	/**
	 * M�todo adiciona o join e where para filtra por campos do material
	 *
	 * @param filtro
	 * @param query
	 * @author Luiz Fernando
	 * @since 09/12/2013
	 */
	private void adicionaJoinWhereForMaterial(EmitircurvaabcFiltro filtro,	QueryBuilder<Venda> query) {
		if(filtro.getMaterialgrupo() != null || filtro.getMaterialtipo() != null || 
				filtro.getMaterial() != null || filtro.getFornecedor() != null || 
				(filtro.getProduto() != null && filtro.getProduto()) ||
				(filtro.getServico() != null && filtro.getServico()) ||
				(filtro.getEpi() != null && filtro.getEpi()) ||
				(filtro.getPatrimonio() != null && filtro.getPatrimonio()) ||
				filtro.getMaterialcategoria() != null){
			
			StringBuilder selectForMaterial = new StringBuilder();
			String whereMaterialgrupo = "";
			String whereMaterialtipo = "";
			String whereMaterial = "";
			String whereFornecedor = "";
			String whereClasseMaterial = "";
			String whereMaterialcategoria = "";
			String joinFornecedor = "";
			String joinMaterialcategoria = "";
			
			if(filtro.getMaterialgrupo() != null){
				whereMaterialgrupo = " AND m.materialgrupo.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo() + " ";
			}
			if(filtro.getMaterialtipo()!=null){
				whereMaterialtipo += " AND m.materialtipo.cdmaterialtipo = " + filtro.getMaterialtipo().getCdmaterialtipo() + " ";
			}
			if(filtro.getMaterialcategoria() != null && filtro.getMaterialcategoria().getVmaterialcategoria() != null){
				joinMaterialcategoria += " left outer join m.materialcategoria mc ";
				joinMaterialcategoria += " left outer join mc.vmaterialcategoria vmc ";
				whereMaterialcategoria += " AND (vmc.identificador like '" + filtro.getMaterialcategoria().getVmaterialcategoria().getIdentificador() + ".%' OR vmc.identificador like '" + filtro.getMaterialcategoria().getVmaterialcategoria().getIdentificador() + "') ";
			}
			if(filtro.getMaterial()!=null){
				whereMaterial += " AND m.cdmaterial = " + filtro.getMaterial().getCdmaterial() + " ";
			}
			if(filtro.getFornecedor()!=null){
				joinFornecedor += " left outer join m.listaFornecedor mf ";
				joinFornecedor += " left outer join mf.fornecedor f ";
				whereFornecedor = " AND f.cdpessoa = " + filtro.getFornecedor().getCdpessoa() + " ";
			}
			if (filtro.getProduto() || filtro.getServico() || filtro.getEpi() || filtro.getPatrimonio()) {
				selectForMaterial.append(",material.produto, material.servico, material.epi, material.patrimonio ");
				whereClasseMaterial = " AND ";
				whereClasseMaterial += " ( ";
				if (filtro.getProduto()) {
					whereClasseMaterial += " m.produto = " + filtro.getProduto() + " ";
				}
				if (filtro.getServico()) {
					if (filtro.getProduto()) {
						whereClasseMaterial += " OR ";
					}
					whereClasseMaterial += " m.servico = " + filtro.getServico() + " ";
				}
				if (filtro.getEpi()) {
					if (filtro.getProduto() || filtro.getServico()) {
						whereClasseMaterial += " OR ";
					}
					whereClasseMaterial += " m.epi = " + filtro.getEpi() + " ";
				}
				if (filtro.getPatrimonio()) {
					if (filtro.getProduto() || filtro.getServico() || filtro.getEpi()) {
						whereClasseMaterial += " OR ";
					}
					whereClasseMaterial += " m.patrimonio = " + filtro.getPatrimonio() + " ";
				}
				whereClasseMaterial += " ) ";
			}
			
			query.select(query.getSelect().getValue() + selectForMaterial.toString());
			query.openParentheses();
			
			query.where(" exists (	  select vm.venda.cdvenda " +
								 	" from Vendamaterial vm " +
								 	" join vm.material m " +
								 	joinFornecedor +
								 	joinMaterialcategoria +
								 	" where vm.venda.cdvenda = venda.cdvenda " +
								 	whereMaterialgrupo + 
								 	whereMaterialtipo +
								 	whereMaterialcategoria +
								 	whereMaterial + 
								 	whereFornecedor +
								 	whereClasseMaterial + " ) ");
			if(Boolean.TRUE.equals(filtro.getConsiderarkitpromocional())){
				query.or();
				query.openParentheses();
				query.where(" exists (	  select vm.venda.cdvenda " +
					 	" from Vendamaterial vm " +
					 	" join vm.materialmestre m " +
					 	joinFornecedor +
					 	joinMaterialcategoria +
					 	" where vm.venda.cdvenda = venda.cdvenda " +
//					 	" and coalesce(m.exibiritenskitvenda, false) = true "+
					 	" and coalesce(m.vendapromocional, false) = true "+
					 	whereMaterialgrupo + 
					 	whereMaterialtipo +
					 	whereMaterialcategoria +
					 	whereMaterial + 
					 	whereFornecedor +
					 	whereClasseMaterial + " ) ");
				query.closeParentheses();
			}
			
			if(!Boolean.TRUE.equals(filtro.getConsiderarkitpromocional())){//Para kits que n�o discriminam itens na venda dever� ser considerada o materialrelacionado para localizar o item do kit 
				String whereMaterialgrupoKit = "";
				String whereMaterialtipoKit = "";
				String whereMaterialKit = "";
				String whereFornecedorKit = "";
				String whereMaterialcategoriaKit = "";
				String joinFornecedorKit = "";
				String joinMaterialcategoriaKit = "";
				
				if(filtro.getMaterialgrupo() != null){
					whereMaterialgrupoKit = " AND mp.materialgrupo.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo() + " ";
				}
				if(filtro.getMaterialtipo()!=null){
					whereMaterialtipoKit += " AND mp.materialtipo.cdmaterialtipo = " + filtro.getMaterialtipo().getCdmaterialtipo() + " ";
				}
				if(filtro.getMaterialcategoria() != null && filtro.getMaterialcategoria().getVmaterialcategoria() != null){
					joinMaterialcategoriaKit += " left outer join mp.materialcategoria mc ";
					joinMaterialcategoriaKit += " left outer join mc.vmaterialcategoria vmc ";
					whereMaterialcategoriaKit += " AND (vmc.identificador like '" + filtro.getMaterialcategoria().getVmaterialcategoria().getIdentificador() + ".%' OR vmc.identificador like '" + filtro.getMaterialcategoria().getVmaterialcategoria().getIdentificador() + "') ";
				}
				if(filtro.getMaterial()!=null){
					whereMaterialKit += " AND mp.cdmaterial = " + filtro.getMaterial().getCdmaterial() + " ";
				}
				if(filtro.getFornecedor()!=null){
					joinFornecedorKit += " left outer join mp.listaFornecedor mf ";
					joinFornecedorKit += " left outer join mf.fornecedor f ";
					whereFornecedorKit = " AND f.cdpessoa = " + filtro.getFornecedor().getCdpessoa() + " ";
				}
				
				query.or();
				query.where(" exists (	  select vm.venda.cdvenda " +
					 	" from Vendamaterial vm " +
					 	" join vm.material m " +
					 	" join m.listaMaterialrelacionado mr " +
					 	" join mr.materialpromocao mp " +
					 	joinFornecedorKit +
					 	joinMaterialcategoriaKit +
					 	" where vm.venda.cdvenda = venda.cdvenda " +
					 	whereMaterialgrupoKit + 
					 	whereMaterialtipoKit +
					 	whereMaterialcategoriaKit +
					 	whereMaterialKit + 
					 	whereFornecedorKit + " ) ");
			}
			
			query.closeParentheses();
			
		}
	}

	/**
	 * Busca os dados para a emiss�o do relat�rio de Curva ABC de venda.
	 *
	 * @param filtro, isForCSV
	 * @return
	 * @since 25/06/2012
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<EmitircurvaabcBean> findForCurvaabcVenda(EmitircurvaabcFiltro filtro) {
		if(filtro == null){
			throw new SinedException("O filtro n�o pode ser nulo.");
		}
		
		boolean considerarkitpromocional = filtro.getConsiderarkitpromocional() != null && filtro.getConsiderarkitpromocional() ? true : false;
		boolean incluirQueryKitpromocional = false;
		
		String select_superior = "SELECT " + 
								    "CD_CLASSIFICACAO, " +
								    "NOME_RANKING, " +
								    "SUM(VALOR_UNITARIO) AS VALOR_UNITARIO, " +
								    "SUM(QUANTIDADE) AS QUANTIDADE, " +
								    "(SUM(VALOR_PRODUTOS) - SUM(TOTAL_DESCONTO) + SUM(TOTAL_FRETE)) AS VALOR_CONSUMO, " +
								    "(SUM(VALOR_PESO)) AS PESO " +
								    "FROM (";
		
		String groupby_inferior = ") QUERY " +
									"GROUP BY NOME_RANKING, CD_CLASSIFICACAO " +
									"ORDER BY VALOR_CONSUMO DESC ";
		
		String select_valorprodutos = "";
		String select_valorprodutos_promocional = "";
		String select_descontofrete = "";
		String from_join_query_valorprodutos = "";
		String from_join_query_valorprodutos_promocional = "";
		String from_join_query_descontofrete = "";
		String groupby_query = "";
		String where_not_promocional = " AND M.VENDAPROMOCIONAL <> TRUE AND VM.CDMATERIALMESTRE IS NULL ";
		String where_promocional = " AND M.VENDAPROMOCIONAL = TRUE AND VM.CDMATERIALMESTRE IS NOT NULL ";
		
		String join_subquery = "";
		String where_subquery = "WHERE V2.CDVENDASITUACAO in (" + Vendasituacao.FATURADA.getCdvendasituacao() + "," + Vendasituacao.REALIZADA.getCdvendasituacao() + ") AND V2.CDVENDA=V.CDVENDA ";
		
		if(filtro.getEmpresa() != null){
			where_subquery += " AND (V2.CDEMPRESA = " + filtro.getEmpresa().getCdpessoa() + " OR V2.CDEMPRESA IS NULL) ";
		}
		if(filtro.getProjeto() != null){
			where_subquery += " AND V2.CDPROJETO = " + filtro.getProjeto().getCdprojeto() + " ";
		}
		if(filtro.getCliente() != null ){
			where_subquery += " AND V.CDCLIENTE = " + filtro.getCliente().getCdpessoa() + " ";
		}
		if(filtro.getCategoria() != null && filtro.getCategoria().getCdcategoria() != null){
			where_subquery += " AND PC.CDCATEGORIA = " + filtro.getCategoria().getCdcategoria() + " ";
		}
		
		if((filtro.getIsRestricaoVendaVendedor() != null && filtro.getIsRestricaoVendaVendedor()) || 
				(filtro.getIsRestricaoClienteVendedor() != null && filtro.getIsRestricaoClienteVendedor()) ){ 
			Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
			if(colaborador == null) {
				where_subquery += " AND 1 = 0 ";
			}else {
				if(filtro.getIsRestricaoClienteVendedor()){
					where_subquery += " AND ( CV.CDCOLABORADOR = " + colaborador.getCdpessoa() + 
										" OR CV IS NULL ) ";
				}
				if(filtro.getIsRestricaoVendaVendedor()){
					where_subquery += " AND V.CDCOLABORADOR = " + colaborador.getCdpessoa() + " ";
				}else if(filtro.getColaborador() != null) {
					where_subquery += " AND V.CDCOLABORADOR = " + filtro.getColaborador().getCdpessoa() + " ";	
				}
			}
		}else if(filtro.getColaborador() != null){
			where_subquery += " AND V.CDCOLABORADOR = " + filtro.getColaborador().getCdpessoa() + " ";
		}
		
		if(filtro.getDtInicio() != null || filtro.getDtFim() != null){
			if(filtro.getDtInicio() != null && filtro.getDtFim() != null){
				where_subquery += " AND V.DTVENDA BETWEEN '" + SinedDateUtils.toStringPostgre(filtro.getDtInicio()) + "' AND '" + SinedDateUtils.toStringPostgre(filtro.getDtFim()) + "' ";
			} else if(filtro.getDtInicio() != null){
				where_subquery += " AND V.DTVENDA >= '" + SinedDateUtils.toStringPostgre(filtro.getDtInicio()) + "' ";
			} else if(filtro.getDtFim() != null){
				where_subquery += " AND V.DTVENDA <= '" + SinedDateUtils.toStringPostgre(filtro.getDtFim()) + "' ";
			}
		}
		
		String strMaterialgrupo = "";
		String strMaterialtipo = "";
		String strMaterial = "";
		String strFornecedor = "";
		String strClasseMaterial = "";
		
		if(filtro.getMaterialgrupo() != null){
			strMaterialgrupo = "AND M.CDMATERIALGRUPO = " + filtro.getMaterialgrupo().getCdmaterialgrupo() + " ";
		}
		if(filtro.getMaterialtipo()!=null){
			strMaterialtipo = " AND M.CDMATERIALTIPO = " + filtro.getMaterialtipo().getCdmaterialtipo() + " ";
		}
		if(filtro.getMaterial()!=null){
			strMaterial = " AND M.CDMATERIAL = " + filtro.getMaterial().getCdmaterial() + " ";
		}
		if(filtro.getFornecedor()!=null){
			strFornecedor = " AND EXISTS (SELECT MF.CDMATERIALFORNECEDOR FROM MATERIALFORNECEDOR MF WHERE M.CDMATERIAL = MF.CDMATERIAL AND MF.CDPESSOA = " + filtro.getFornecedor().getCdpessoa() + ") ";
		}
		if(filtro.getProduto() != null && filtro.getProduto() ){
			strClasseMaterial += "";
		}
		
		if (filtro.getProduto() || filtro.getServico() || filtro.getEpi() || filtro.getPatrimonio()) {
			strClasseMaterial += " AND ( ";
			if (filtro.getProduto()) {
				strClasseMaterial += " M.PRODUTO = " + filtro.getProduto() + " ";
			}
			if (filtro.getServico()) {
				if (filtro.getProduto()) {
					strClasseMaterial += " OR ";
				}
				strClasseMaterial += " M.SERVICO = " + filtro.getServico() + " ";
			}
			if (filtro.getEpi()) {
				if (filtro.getProduto() || filtro.getServico()) {
					strClasseMaterial += " OR ";
				}
				strClasseMaterial += " M.EPI = " + filtro.getEpi() + " ";
			}
			if (filtro.getPatrimonio()) {
				if (filtro.getProduto() || filtro.getServico() || filtro.getEpi()) {
					strClasseMaterial += " OR ";
				}
				strClasseMaterial += " M.PATRIMONIO = " + filtro.getPatrimonio() + " ";
			}
			strClasseMaterial += " ) ";
		}
		
		if(filtro.getEmitircurvaabcranking() == null || Emitircurvaabcranking.PRODUTO.equals(filtro.getEmitircurvaabcranking())){
			select_valorprodutos =  " SELECT  " +
									"M.CDMATERIAL AS CD_CLASSIFICACAO, " +
								    "COALESCE(M.IDENTIFICACAO, '') || " +
								    "(CASE WHEN M.IDENTIFICACAO IS NOT NULL  " +
								    "    THEN ' - ' " +
								  	"	ELSE '' " +
									"END) || M.NOME AS NOME_RANKING, " +
								    "M.VALORVENDA AS VALOR_UNITARIO, " +
								    "sum(VM.QUANTIDADE) AS QUANTIDADE, " +
								    "sum((VM.QUANTIDADE * VM.PRECO) -(COALESCE(VM.DESCONTO, 0) / 100)) AS VALOR_PRODUTOS, " +
								    "sum((VM.QUANTIDADE * COALESCE(M.PESO, 0))) AS VALOR_PESO, " +
								    "0 AS TOTAL_DESCONTO, " +
								    "0 AS TOTAL_FRETE ";
			
			select_valorprodutos_promocional =  " SELECT  " +
												"M.CDMATERIAL AS CD_CLASSIFICACAO, " +
											    "COALESCE(M.IDENTIFICACAO, '') || " +
											    "(CASE WHEN M.IDENTIFICACAO IS NOT NULL  " +
											    "    THEN ' - ' " +
											  	"	ELSE '' " +
												"END) || M.NOME AS NOME_RANKING, " +
											    "M.VALORVENDA AS VALOR_UNITARIO, " +
											    "sum(VM.QUANTIDADE / VMR.QUANTIDADE) AS QUANTIDADE, " +
											    "sum((VM.QUANTIDADE * VM.PRECO) -(COALESCE(VM.DESCONTO, 0) / 100)) AS VALOR_PRODUTOS, " +
											    "sum((VM.QUANTIDADE * COALESCE(M.PESO, 0))) AS VALOR_PESO, " +
											    "0 AS TOTAL_DESCONTO, " +
											    "0 AS TOTAL_FRETE ";
								    
			
			select_descontofrete = " SELECT  " +
									"M.CDMATERIAL AS CD_CLASSIFICACAO, " +
								    "COALESCE(M.IDENTIFICACAO, '') || " +
								    "(CASE WHEN M.IDENTIFICACAO IS NOT NULL  " +
								    "    THEN ' - ' " +
								  	"	ELSE '' " +
									"END) || M.NOME AS NOME_RANKING, " +
								    "0 AS VALOR_UNITARIO, " +
								    "0 AS QUANTIDADE, " +
								    "0 AS VALOR_PRODUTOS, " +
								    "0 AS VALOR_PESO, " +
								    "SUM((COALESCE(V.DESCONTO, 0)/100) * ((COALESCE(VM.QUANTIDADE,0) * COALESCE(VM.PRECO,0) - COALESCE(VM.DESCONTO,0)) / (SELECT SUM(COALESCE(VMSUB.QUANTIDADE,0) * COALESCE(VMSUB.PRECO,0) - COALESCE(VMSUB.DESCONTO,0)) FROM VENDAMATERIAL VMSUB WHERE VMSUB.CDVENDA = V.CDVENDA))) AS TOTAL_DESCONTO, " +
								    "SUM((COALESCE(V.VALORFRETE, 0)/100) / (SELECT SUM(VMSUB.QUANTIDADE) FROM VENDAMATERIAL VMSUB WHERE VMSUB.CDVENDA = V.CDVENDA) * VM.QUANTIDADE) AS TOTAL_FRETE ";
			
			from_join_query_valorprodutos = " FROM VENDA V " + 
								"JOIN VENDAMATERIAL VM ON VM.CDVENDA = V.CDVENDA " +
								"LEFT OUTER JOIN MATERIAL M ON VM.CDMATERIAL = M.CDMATERIAL " +
//								"LEFT OUTER JOIN CLIENTEVENDEDOR CV ON CV.CDCLIENTE = V.CDCLIENTE " +
//								"LEFT OUTER JOIN PESSOACATEGORIA PC ON PC.CDPESSOA = V.CDCLIENTE " +
								"WHERE 1 = 1 " +
								(considerarkitpromocional ? where_not_promocional : "") + 
								strMaterialtipo +
								strMaterialgrupo +
								strMaterial +
								strFornecedor +
								strClasseMaterial;
			
			if(considerarkitpromocional){
				incluirQueryKitpromocional = true;
				from_join_query_valorprodutos_promocional = " FROM VENDA V " + 
								"JOIN VENDAMATERIAL VM ON VM.CDVENDA = V.CDVENDA " +
								"LEFT OUTER JOIN MATERIAL M ON VM.CDMATERIALMESTRE = M.CDMATERIAL " +
								"LEFT OUTER JOIN VMATERIALRELACIONADO VMR ON VMR.CDMATERIAL = VM.CDMATERIALMESTRE " +
								"WHERE 1 = 1 " +
								(considerarkitpromocional ? where_promocional : "") + 
								strMaterialtipo +
								strMaterialgrupo +
								strMaterial +
								strFornecedor +
								strClasseMaterial;
			}
			
			from_join_query_descontofrete = from_join_query_valorprodutos;
			
			groupby_query = " GROUP BY M.CDMATERIAL, M.NOME, M.IDENTIFICACAO, M.VALORVENDA ";
			
			join_subquery = "";
		}
		else if(Emitircurvaabcranking.CLIENTE.equals(filtro.getEmitircurvaabcranking())){
			
			select_valorprodutos = " SELECT CAST(NULL AS INTEGER) AS CD_CLASSIFICACAO, " +
								"P.NOME AS NOME_RANKING, " +
							    "sum(M.VALORVENDA) AS VALOR_UNITARIO, " +
							    "sum(VM.QUANTIDADE) AS QUANTIDADE, " +
							    "sum((VM.QUANTIDADE * VM.PRECO) -(COALESCE(VM.DESCONTO, 0) / 100)) AS VALOR_PRODUTOS, " +
							    "sum((VM.QUANTIDADE * COALESCE(M.PESO, 0))) AS VALOR_PESO, " +
							    "0 AS TOTAL_DESCONTO, " +
							    "0 AS TOTAL_FRETE  ";
			
			select_valorprodutos_promocional =  " SELECT CAST(NULL AS INTEGER) AS CD_CLASSIFICACAO, " +
								"P.NOME AS NOME_RANKING, " +
							    "sum(M.VALORVENDA) AS VALOR_UNITARIO, " +
							    "sum(VM.QUANTIDADE / VMR.QUANTIDADE) AS QUANTIDADE, " +
							    "sum((VM.QUANTIDADE * VM.PRECO) -(COALESCE(VM.DESCONTO, 0) / 100)) AS VALOR_PRODUTOS, " +
							    "sum((VM.QUANTIDADE * COALESCE(M.PESO, 0))) AS VALOR_PESO, " +
							    "0 AS TOTAL_DESCONTO, " +
							    "0 AS TOTAL_FRETE  ";
			
			select_descontofrete = " SELECT CAST(NULL AS INTEGER) AS CD_CLASSIFICACAO, " +
							       "P.NOME AS NOME_RANKING, " +
							       "0 AS VALOR_UNITARIO, " +
							       "0 AS QUANTIDADE, " +
							       "0 AS VALOR_PRODUTOS, " +
							       "0 AS VALOR_PESO, " +
							       "SUM(COALESCE(V.DESCONTO, 0)/100) AS TOTAL_DESCONTO, " +
							       "SUM(COALESCE(V.VALORFRETE, 0)/100) AS TOTAL_FRETE  ";
			
			from_join_query_valorprodutos = " FROM VENDA V " + 
				"JOIN PESSOA P ON P.CDPESSOA = V.CDCLIENTE " +
				"JOIN VENDAMATERIAL VM ON VM.CDVENDA = V.CDVENDA " +
				"LEFT OUTER JOIN MATERIAL M ON VM.CDMATERIAL = M.CDMATERIAL " +
//				"LEFT OUTER JOIN CLIENTEVENDEDOR CV ON CV.CDCLIENTE = V.CDCLIENTE " +
//				"LEFT OUTER JOIN PESSOACATEGORIA PC ON PC.CDPESSOA = V.CDCLIENTE " +
				"WHERE 1 = 1 " + (considerarkitpromocional ? where_not_promocional : "") + 
								 strMaterialtipo +
								 strMaterialgrupo +
								 strMaterial +
								 strFornecedor;
			
			if(considerarkitpromocional){
				incluirQueryKitpromocional = true;
				from_join_query_valorprodutos_promocional = " FROM VENDA V " + 
					"JOIN PESSOA P ON P.CDPESSOA = V.CDCLIENTE " +
					"JOIN VENDAMATERIAL VM ON VM.CDVENDA = V.CDVENDA " +
					"LEFT OUTER JOIN MATERIAL M ON VM.CDMATERIALMESTRE = M.CDMATERIAL " +
					"LEFT OUTER JOIN VMATERIALRELACIONADO VMR ON VMR.CDMATERIAL = M.CDMATERIAL " +
					"WHERE 1 = 1 " + (considerarkitpromocional ? where_promocional : "") + 
									 strMaterialtipo +
									 strMaterialgrupo +
									 strMaterial +
									 strFornecedor;
			}
			
			from_join_query_descontofrete = " FROM VENDA V " + 
					"JOIN PESSOA P ON P.CDPESSOA = V.CDCLIENTE " +
					"WHERE 1 = 1 ";
			
			groupby_query = " GROUP BY P.NOME ";
			
			join_subquery = " JOIN VENDAMATERIAL VM ON VM.CDVENDA = V2.CDVENDA " +
							" LEFT OUTER JOIN MATERIAL M ON VM.CDMATERIAL = M.CDMATERIAL ";
			
			where_subquery += strMaterialtipo +
								strMaterialgrupo +
								strMaterial +
								strFornecedor +
								strClasseMaterial;
		}
		else if(Emitircurvaabcranking.VENDEDOR.equals(filtro.getEmitircurvaabcranking())){
			
			select_valorprodutos = " SELECT CAST(NULL AS INTEGER) AS CD_CLASSIFICACAO, " +
								"P.NOME AS NOME_RANKING, " +
							    "sum(M.VALORVENDA) AS VALOR_UNITARIO, " +
							    "sum(VM.QUANTIDADE) AS QUANTIDADE, " +
							    "sum((VM.QUANTIDADE * VM.PRECO) -(COALESCE(VM.DESCONTO, 0) / 100)) AS VALOR_PRODUTOS, " +
							    "sum((VM.QUANTIDADE * COALESCE(M.PESO, 0))) AS VALOR_PESO, " +
							    "0 AS TOTAL_DESCONTO, " +
							    "0 AS TOTAL_FRETE  ";

			select_valorprodutos_promocional =  " SELECT CAST(NULL AS INTEGER) AS CD_CLASSIFICACAO, " +
								"P.NOME AS NOME_RANKING, " +
							    "sum(M.VALORVENDA) AS VALOR_UNITARIO, " +
							    "sum(VM.QUANTIDADE / VMR.QUANTIDADE) AS QUANTIDADE, " +
							    "sum((VM.QUANTIDADE * VM.PRECO) -(COALESCE(VM.DESCONTO, 0) / 100)) AS VALOR_PRODUTOS, " +
							    "sum((VM.QUANTIDADE * COALESCE(M.PESO, 0))) AS VALOR_PESO, " +
							    "0 AS TOTAL_DESCONTO, " +
							    "0 AS TOTAL_FRETE  ";
				
			select_descontofrete = " SELECT CAST(NULL AS INTEGER) AS CD_CLASSIFICACAO, " +
					       "P.NOME AS NOME_RANKING, " +
					       "0 AS VALOR_UNITARIO, " +
					       "0 AS QUANTIDADE, " +
					       "0 AS VALOR_PRODUTOS, " +
					       "0 AS VALOR_PESO, " +
					       "SUM(COALESCE(V.DESCONTO, 0)/100) AS TOTAL_DESCONTO, " +
					       "SUM(COALESCE(V.VALORFRETE, 0)/100) AS TOTAL_FRETE  ";
			
			from_join_query_valorprodutos = " FROM VENDA V " + 
			"JOIN PESSOA P ON P.CDPESSOA = V.CDCOLABORADOR " +
			"JOIN VENDAMATERIAL VM ON VM.CDVENDA = V.CDVENDA " +
			"LEFT OUTER JOIN MATERIAL M ON VM.CDMATERIAL = M.CDMATERIAL " +
			"LEFT OUTER JOIN VMATERIALRELACIONADO VMR ON VMR.CDMATERIAL = M.CDMATERIAL " +
//			"LEFT OUTER JOIN CLIENTEVENDEDOR CV ON CV.CDCLIENTE = V.CDCLIENTE " +
//			"LEFT OUTER JOIN PESSOACATEGORIA PC ON PC.CDPESSOA = V.CDCLIENTE " +
			"WHERE 1 = 1 " + (considerarkitpromocional ? where_not_promocional : "") +
							 strMaterialtipo +
							 strMaterialgrupo +
							 strMaterial +
							 strFornecedor;
			
			if(considerarkitpromocional){
				incluirQueryKitpromocional = true;
				from_join_query_valorprodutos_promocional = " FROM VENDA V " + 
					"JOIN PESSOA P ON P.CDPESSOA = V.CDCOLABORADOR " +
					"JOIN VENDAMATERIAL VM ON VM.CDVENDA = V.CDVENDA " +
					"LEFT OUTER JOIN MATERIAL M ON VM.CDMATERIALMESTRE = M.CDMATERIAL " +
					"LEFT OUTER JOIN VMATERIALRELACIONADO VMR ON VMR.CDMATERIAL = M.CDMATERIAL " +
					"WHERE 1 = 1 " + (considerarkitpromocional ? where_promocional : "") + 
									 strMaterialtipo +
									 strMaterialgrupo +
									 strMaterial +
									 strFornecedor;
			}
			
			from_join_query_descontofrete = " FROM VENDA V " + 
			"JOIN PESSOA P ON P.CDPESSOA = V.CDCOLABORADOR " +
			"WHERE 1 = 1 ";
			
			groupby_query = " GROUP BY P.NOME ";
			
			join_subquery = " JOIN VENDAMATERIAL VM ON VM.CDVENDA = V2.CDVENDA " +
					" LEFT OUTER JOIN MATERIAL M ON VM.CDMATERIAL = M.CDMATERIAL ";
			
			where_subquery += strMaterialtipo +
						strMaterialgrupo +
						strMaterial +
						strFornecedor +
						strClasseMaterial;
			
		}
		else if(Emitircurvaabcranking.FORNECEDOR.equals(filtro.getEmitircurvaabcranking())){
			
			select_valorprodutos = " SELECT CAST(NULL AS INTEGER) AS CD_CLASSIFICACAO, " +
						"P.NOME AS NOME_RANKING, " +
					    "sum(M.VALORVENDA) AS VALOR_UNITARIO, " +
					    "sum(VM.QUANTIDADE) AS QUANTIDADE, " +
					    "sum((VM.QUANTIDADE * VM.PRECO) -(COALESCE(VM.DESCONTO, 0) / 100)) AS VALOR_PRODUTOS, " +
					    "sum((VM.QUANTIDADE * COALESCE(M.PESO, 0))) AS VALOR_PESO, " +
					    "0 AS TOTAL_DESCONTO, " +
					    "0 AS TOTAL_FRETE  ";
			
			select_valorprodutos_promocional = " SELECT CAST(NULL AS INTEGER) AS CD_CLASSIFICACAO, " +
						"P.NOME AS NOME_RANKING, " +
					    "sum(M.VALORVENDA) AS VALOR_UNITARIO, " +
					    "sum(VM.QUANTIDADE / VMR.QUANTIDADE) AS QUANTIDADE, " +
					    "sum((VM.QUANTIDADE * VM.PRECO) -(COALESCE(VM.DESCONTO, 0) / 100)) AS VALOR_PRODUTOS, " +
					    "sum((VM.QUANTIDADE * COALESCE(M.PESO, 0))) AS VALOR_PESO, " +
					    "0 AS TOTAL_DESCONTO, " +
					    "0 AS TOTAL_FRETE  ";

			select_descontofrete = " SELECT CAST(NULL AS INTEGER) AS CD_CLASSIFICACAO, " +
			       "P.NOME AS NOME_RANKING, " +
			       "0 AS VALOR_UNITARIO, " +
			       "0 AS QUANTIDADE, " +
			       "0 AS VALOR_PRODUTOS, " +
			       "0 AS VALOR_PESO, " +
			       "SUM((COALESCE(V.DESCONTO, 0)/100) * ((COALESCE(VM.QUANTIDADE,0) * COALESCE(VM.PRECO,0) - COALESCE(VM.DESCONTO,0)) / (SELECT SUM(COALESCE(VMSUB.QUANTIDADE,0) * COALESCE(VMSUB.PRECO,0) - COALESCE(VMSUB.DESCONTO,0)) FROM VENDAMATERIAL VMSUB WHERE VMSUB.CDVENDA = V.CDVENDA))) AS TOTAL_DESCONTO, " +
			       "SUM((COALESCE(V.VALORFRETE, 0)/100) / (SELECT SUM(VMSUB.QUANTIDADE) FROM VENDAMATERIAL VMSUB WHERE VMSUB.CDVENDA = V.CDVENDA) * VM.QUANTIDADE) AS TOTAL_FRETE  ";
			
			from_join_query_valorprodutos = " FROM VENDA V " +
		    "JOIN VENDAMATERIAL VM ON VM.CDVENDA = V.CDVENDA " +
		    "JOIN MATERIAL M ON VM.CDMATERIAL = M.CDMATERIAL " +
		    "JOIN MATERIALFORNECEDOR MF ON MF.CDMATERIAL = M.CDMATERIAL " +
			"JOIN PESSOA P ON P.CDPESSOA = MF.CDPESSOA " +
			"LEFT OUTER JOIN VMATERIALRELACIONADO VMR ON VMR.CDMATERIAL = M.CDMATERIAL " +
//			"LEFT OUTER JOIN CLIENTEVENDEDOR CV ON CV.CDCLIENTE = V.CDCLIENTE " +
//			"LEFT OUTER JOIN PESSOACATEGORIA PC ON PC.CDPESSOA = V.CDCLIENTE " +
			"WHERE 1 = 1 " + (considerarkitpromocional ? where_not_promocional : "") + 
							 strMaterialtipo +
							 strMaterialgrupo +
							 strMaterial +
							 strFornecedor +
			"AND MF.FABRICANTE = TRUE ";
			
			if(considerarkitpromocional){
				incluirQueryKitpromocional = true;
				from_join_query_valorprodutos_promocional = " FROM VENDA V " +
			    "JOIN VENDAMATERIAL VM ON VM.CDVENDA = V.CDVENDA " +
			    "JOIN MATERIAL M ON VM.CDMATERIALMESTRE = M.CDMATERIAL " +
			    "JOIN MATERIALFORNECEDOR MF ON MF.CDMATERIAL = M.CDMATERIAL " +
				"JOIN PESSOA P ON P.CDPESSOA = MF.CDPESSOA " +
				"LEFT OUTER JOIN VMATERIALRELACIONADO VMR ON VMR.CDMATERIAL = M.CDMATERIAL " +
				"WHERE 1 = 1 " + (considerarkitpromocional ? where_promocional : "") + 
								 strMaterialtipo +
								 strMaterialgrupo +
								 strMaterial +
								 strFornecedor +
				"AND MF.FABRICANTE = TRUE ";
			}
			
			from_join_query_descontofrete = from_join_query_valorprodutos;
			
			groupby_query = " GROUP BY P.NOME ";
			
			join_subquery = "";
		}
		else if(Emitircurvaabcranking.GRUPOMATERIAL.equals(filtro.getEmitircurvaabcranking())){
			select_valorprodutos = " SELECT CAST(NULL AS INTEGER) AS CD_CLASSIFICACAO, " +
						"MG.NOME AS NOME_RANKING, " +
					    "sum(M.VALORVENDA) AS VALOR_UNITARIO, " +
					    "sum(VM.QUANTIDADE) AS QUANTIDADE, " +
					    "sum((VM.QUANTIDADE * VM.PRECO) -(COALESCE(VM.DESCONTO, 0) / 100)) AS VALOR_PRODUTOS, " +
					    "sum((VM.QUANTIDADE * COALESCE(M.PESO, 0))) AS VALOR_PESO, " +
					    "0 AS TOTAL_DESCONTO, " +
					    "0 AS TOTAL_FRETE  ";

			select_valorprodutos_promocional = " SELECT CAST(NULL AS INTEGER) AS CD_CLASSIFICACAO, " +
						"MG.NOME AS NOME_RANKING, " +
					    "sum(M.VALORVENDA) AS VALOR_UNITARIO, " +
					    "sum(VM.QUANTIDADE / VMR.QUANTIDADE) AS QUANTIDADE, " +
					    "sum((VM.QUANTIDADE * VM.PRECO) -(COALESCE(VM.DESCONTO, 0) / 100)) AS VALOR_PRODUTOS, " +
					    "sum((VM.QUANTIDADE * COALESCE(M.PESO, 0))) AS VALOR_PESO, " +
					    "0 AS TOTAL_DESCONTO, " +
					    "0 AS TOTAL_FRETE  ";
				
			select_descontofrete = " SELECT CAST(NULL AS INTEGER) AS CD_CLASSIFICACAO, " +
			       "MG.NOME AS NOME_RANKING, " +
			       "0 AS VALOR_UNITARIO, " +
			       "0 AS QUANTIDADE, " +
			       "0 AS VALOR_PRODUTOS, " +
			       "0 AS VALOR_PESO, " +
			       "SUM((COALESCE(V.DESCONTO, 0)/100) * ((COALESCE(VM.QUANTIDADE,0) * COALESCE(VM.PRECO,0) - COALESCE(VM.DESCONTO,0)) / (SELECT SUM(COALESCE(VMSUB.QUANTIDADE,0) * COALESCE(VMSUB.PRECO,0) - COALESCE(VMSUB.DESCONTO,0)) FROM VENDAMATERIAL VMSUB WHERE VMSUB.CDVENDA = V.CDVENDA))) AS TOTAL_DESCONTO, " +
			       "SUM((COALESCE(V.VALORFRETE, 0)/100) / (SELECT SUM(VMSUB.QUANTIDADE) FROM VENDAMATERIAL VMSUB WHERE VMSUB.CDVENDA = V.CDVENDA) * VM.QUANTIDADE) AS TOTAL_FRETE  ";
			
			from_join_query_valorprodutos = " FROM VENDA V " +
			"JOIN VENDAMATERIAL VM ON VM.CDVENDA = V.CDVENDA " +
			"JOIN MATERIAL M ON VM.CDMATERIAL = M.CDMATERIAL " +
			"JOIN MATERIALGRUPO MG ON MG.CDMATERIALGRUPO = M.CDMATERIALGRUPO " +
//			"LEFT OUTER JOIN CLIENTEVENDEDOR CV ON CV.CDCLIENTE = V.CDCLIENTE " +
//			"LEFT OUTER JOIN PESSOACATEGORIA PC ON PC.CDPESSOA = V.CDCLIENTE " +
			"WHERE 1 = 1 " + (considerarkitpromocional ? where_not_promocional : "") + 
			 				 strMaterialtipo +
							 strMaterialgrupo +
							 strMaterial +
							 strFornecedor;
			
			if(considerarkitpromocional){
				incluirQueryKitpromocional = true;
				from_join_query_valorprodutos_promocional = " FROM VENDA V " +
				"JOIN VENDAMATERIAL VM ON VM.CDVENDA = V.CDVENDA " +
				"JOIN MATERIAL M ON VM.CDMATERIALMESTRE = M.CDMATERIAL " +
				"JOIN MATERIALGRUPO MG ON MG.CDMATERIALGRUPO = M.CDMATERIALGRUPO " +
				"LEFT OUTER JOIN VMATERIALRELACIONADO VMR ON VMR.CDMATERIAL = VM.CDMATERIALMESTRE " +
				"WHERE 1 = 1 " + (considerarkitpromocional ? where_promocional : "") + 
				 				 strMaterialtipo +
								 strMaterialgrupo +
								 strMaterial +
								 strFornecedor;
			}
			
			from_join_query_descontofrete = from_join_query_valorprodutos;
			
			groupby_query = " GROUP BY MG.NOME ";
			
			join_subquery = "";
		}
		
		
		String select_from_subquery = " SELECT V2.CDVENDA FROM VENDA V2 "+
									  " LEFT OUTER JOIN CLIENTEVENDEDOR CV ON CV.CDCLIENTE = V2.CDCLIENTE " +
									  "LEFT OUTER JOIN PESSOACATEGORIA PC ON PC.CDPESSOA = V.CDCLIENTE ";
		String where = " AND V.CDVENDA IN (" + select_from_subquery + join_subquery + where_subquery + ") ";
		String whereNotDevolucao = " AND V.CDVENDA NOT IN (SELECT MD.CDVENDA FROM MATERIALDEVOLUCAO MD WHERE MD.CDVENDA=V.CDVENDA AND MD.CDMATERIAL = VM.CDMATERIAL )";
		
		if (filtro.getPedidovendatipo()!=null){
			where += " AND EXISTS (SELECT 1 FROM PEDIDOVENDA PV WHERE PV.CDPEDIDOVENDA=V.CDPEDIDOVENDA AND PV.CDPEDIDOVENDATIPO = " + filtro.getPedidovendatipo().getCdpedidovendatipo() + ")";
		}
		
		String wehreNotDevolucaoVendamaterial = " AND V.CDVENDA NOT IN (SELECT MD.CDVENDA " +
												" FROM MATERIALDEVOLUCAO MD " +
												"JOIN VENDAMATERIAL VMDEVOLUCAO ON VMDEVOLUCAO.CDVENDA = MD.CDVENDA AND VMDEVOLUCAO.CDMATERIAL = MD.CDMATERIAL " +
												"WHERE MD.CDVENDA = V.CDVENDA )";
		
		String sql = select_superior +
						select_valorprodutos +
						from_join_query_valorprodutos +
						where +
						whereNotDevolucao +
						groupby_query +
						" UNION ALL " +
						select_descontofrete +
						from_join_query_descontofrete +
						where +
						wehreNotDevolucaoVendamaterial +
						groupby_query +
						
						(considerarkitpromocional && incluirQueryKitpromocional ? 
							(" UNION ALL " +
							select_valorprodutos_promocional +
							from_join_query_valorprodutos_promocional +
							where +
							whereNotDevolucao +
							groupby_query ) : "" 
						) +
						
						groupby_inferior;

		SinedUtil.markAsReader();
		List<EmitircurvaabcBean> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				EmitircurvaabcBean listEmiticurvaabc = new EmitircurvaabcBean(
						rs.getInt("CD_CLASSIFICACAO"),
						rs.getString("NOME_RANKING"),
						rs.getDouble("VALOR_UNITARIO"),
						rs.getDouble("QUANTIDADE"),
						rs.getDouble("VALOR_CONSUMO"),
						rs.getDouble("PESO"));
			
				return listEmiticurvaabc;
			}
		});
			
		
		return list;
	}
	
//	private String criaClausulaWhere(Integer intEmpresa, String strProjeto, String strMaterialgrupo, String strCliente, String strColaborador, 
//			String strClasseMaterial, String dtinicio, String dtfim, String strFornecedor, String strMaterialtipo, String strMaterial){
//	
//		return " WHERE (V.CDEMPRESA = " + intEmpresa + " OR V.CDEMPRESA IS NULL) "
//			    + " AND V.CDVENDASITUACAO in (" + Vendasituacao.FATURADA.getCdvendasituacao() + "," + Vendasituacao.REALIZADA.getCdvendasituacao() + ") "
//			    + strProjeto
//			    + strMaterialgrupo
//			    + strCliente
//			    + strColaborador
//			    + strClasseMaterial
//			    + strMaterialtipo
//			    + strMaterial 
//			    + strFornecedor
//			    + " AND V.DTVENDA BETWEEN " + dtinicio + dtfim;
//	}

	/**
	 * Busca a listagem para a gera��o do CSV.
	 *
	 * @param filtro
	 * @return
	 * @since 25/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Venda> findForCsv(VendaFiltro filtro) {
		QueryBuilder<Venda> query = query();
		this.updateListagemQuery(query, filtro);
		this.joinVendamaterialUpdatelistagem(query);
		
		List<Venda> listaVenda = query.list();
		String whereIn = CollectionsUtil.listAndConcatenate(listaVenda, "cdvenda", ",");
		
		QueryBuilder<Venda> queryFinal = query()
			.select("venda.cdvenda, empresa.nome, empresa.razaosocial, empresa.nomefantasia, cliente.nome, cliente.cnpj, cliente.cpf, localarmazenagem.nome, venda.dtvenda, clienteindicacao.nome, " +
					"vendamaterial.quantidade, vendamaterial.preco, vendamaterial.desconto, vendamaterial.multiplicador, vendasituacao.cdvendasituacao, vendasituacao.descricao, " +
					"colaborador.nome, venda.valorfrete, venda.desconto, listavendahistorico.acao, listavendahistorico.dtaltera, listavendahistorico.observacao")
			.leftOuterJoin("venda.listavendamaterial vendamaterial")
			.leftOuterJoin("venda.empresa empresa")
			.leftOuterJoin("venda.localarmazenagem localarmazenagem")
			.leftOuterJoin("venda.vendasituacao vendasituacao")
			.leftOuterJoin("venda.cliente cliente")
			.leftOuterJoin("venda.colaborador colaborador")
			.leftOuterJoin("venda.listavendahistorico listavendahistorico")
			.leftOuterJoin("venda.clienteindicacao clienteindicacao")
			.whereIn("venda.cdvenda", whereIn);
		
		if((filtro.getNaoexibirresultado() != null && filtro.getNaoexibirresultado()) || (whereIn == null || "".equals(whereIn))){
			queryFinal.where("1=0");
		}
		
		return queryFinal
				.orderBy("venda.dtvenda desc, venda.cdvenda desc, listavendahistorico.dtaltera desc ")
				.list();
	}
	
	/**
	 * 
	 * M�todo que busca o identificador da venda.
	 *
	 * @name buscarIdentificadorVenda
	 * @param venda
	 * @return
	 * @return Integer
	 * @author Thiago Augusto
	 * @date 13/07/2012
	 *
	 */
	public String buscarIdentificadorVenda(Venda venda){
		Venda retorno = querySined()
		.select("venda.identificador")
		.where("venda = ?", venda)
		.unique();
		return retorno != null ? retorno.getIdentificador() : null;
	}
	
	/**
	 * Verifica se existe alguma venda com situa��o de expedi��o diferentes das passadas por par�metro.
	 *
	 * @param whereIn
	 * @param situacoes
	 * @return
	 * @since 17/07/2012
	 * @author Rodrigo Freitas
	 */
	public boolean haveVendaNotInExpedicaoSituacao(String whereIn, Expedicaovendasituacao[] situacoes) {
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Venda.class)
				.setUseTranslator(false)
				.whereIn("venda.cdvenda", whereIn);

		for (int i = 0; i < situacoes.length; i++) {
			query.where("venda.expedicaovendasituacao <> ?", situacoes[i]);
		}

		return query.unique() > 0;
	}
	
	/**
	 * Busca as vendas para preencher a Expedi��o
	 *
	 * @param whereIn
	 * @return
	 * @since 18/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Venda> findForExpedicao(String whereIn, String cdempresa) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		QueryBuilder<Venda> query = query()
					.select("venda.cdvenda, venda.dtvenda, venda.observacao, venda.valorfrete, venda.dtprazoentregamax, pedidovendatipo.cdpedidovendatipo, pedidovendatipo.criarExpedicaoComConferencia, " +
							"terceiro.cdpessoa, terceiro.nome, " +
							"empresa.cdpessoa, cliente.cdpessoa, cliente.nome, cliente.cpf, " +
							"cliente.cnpj, contato.cdpessoa, contato.nome, vendamaterial.cdvendamaterial, vendamaterial.quantidade, " +
							"material.cdmaterial, material.nome, material.identificacao, vendamaterial.comprimento, " +
							"vendamaterial.altura, vendamaterial.largura, vendamaterial.comprimentooriginal, " +
							"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, unidademedida.cdunidademedida, " +
							"unidademedida.nome, materialmestre.cdmaterial, materialmestre.vendapromocional, " +
							"material.vendapromocional, material.obrigarlote, contato.cdpessoa, contato.nome," +
							"loteEstoque.cdloteestoque, loteEstoque.numero, loteEstoque.validade ")
					.leftOuterJoin("venda.terceiro terceiro")
					.leftOuterJoin("venda.empresa empresa")
					.leftOuterJoin("venda.cliente cliente")
					.leftOuterJoin("venda.contato contato")
					.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
					.leftOuterJoin("venda.listavendamaterial vendamaterial")
					.leftOuterJoin("vendamaterial.material material")
					.leftOuterJoin("vendamaterial.loteestoque loteEstoque")
					.leftOuterJoin("vendamaterial.materialmestre materialmestre")
					.leftOuterJoin("venda.localarmazenagem localarmazenagem")
					.leftOuterJoin("vendamaterial.unidademedida unidademedida")
					.whereIn("venda.cdvenda", whereIn)
					.orderBy("venda.cdvenda, vendamaterial.ordem");
		
		if (!StringUtils.isEmpty(cdempresa)) {
			query
				.whereIn("empresa.cdpessoa", cdempresa);
		}
		
		return query.list();
	}

	public Venda findForValidadeVendaEcommerce(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nulo.");
		
		return query()
				.select("venda.cdvenda, pedidovendatipo.cdpedidovendatipo, pedidovendatipo.criarExpedicaoComConferencia, pedidovendatipo.vendasEcommerce")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.where("venda  = ?", venda)
				.unique();
	}
	
	/**
	 * Atualiza a situa��o da expedi��o na venda.
	 *
	 * @param venda
	 * @param expedicaovendasituacao
	 * @since 18/07/2012
	 * @author Rodrigo Freitas
	 */
	public void updateSituacaoExpedicaoVenda(Venda venda, Expedicaovendasituacao expedicaovendasituacao) {
		getHibernateTemplate().bulkUpdate("update Venda v set v.expedicaovendasituacao = ? where v.id = ?", new Object[]{
				expedicaovendasituacao, venda.getCdvenda()
		});
	}

	/**
	 * Busca os dados para a atualiza��o da situa��o da expedi��o na venda
	 *
	 * @param whereIn
	 * @return
	 * @since 18/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Venda> findForAtualizacaoExpedicao(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		return query()
					.select("venda.cdvenda, vendamaterial.cdvendamaterial, vendamaterial.quantidade, " +
							"materialmestre.cdmaterial, materialmestre.vendapromocional," +
							"material.cdmaterial, material.vendapromocional, material.servico")
					.join("venda.listavendamaterial vendamaterial")
					.join("vendamaterial.material material")
					.leftOuterJoin("vendamaterial.materialmestre materialmestre")
					.whereIn("venda.cdvenda", whereIn)
					.list();
	}

	/**
	 * M�todo que carrega a venda com o cliente
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Venda loadWithCliente(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nulo.");
		
		return query()
				.select("venda.cdvenda, cliente.cdpessoa, cliente.nome, cliente.cnpj")
				.join("venda.cliente cliente")
				.where("venda  = ?", venda)
				.unique();
	}
	
//	/**
//	 * M�todo que carrega os dados para relat�rio sint�tico de curva abc de venda.
//	 * 
//	 * @param filtro
//	 * @return
//	 * @author Rafael Salvio
//	 */
//	public List<Venda> findForCSVCurvaAbcVenda(EmitircurvaabcFiltro filtro){
//		if(filtro == null)
//			throw new SinedException("Filtro n�o pode ser nulo.");
//		
//		QueryBuilder<Venda> query = query()
//				.select("venda.cdvenda, venda.dtvenda, material.cdmaterial, material.nome, material.identificacao, material.peso, " +
//						"listavendamaterial.preco, listavendamaterial.desconto, listavendamaterial.quantidade, venda.desconto, venda.valorfrete, " +
//						"fornecedor.cdpessoa, fornecedor.nome, cliente.cdpessoa, cliente.nome, cliente.identificador, materialgrupo.cdmaterialgrupo, materialgrupo.nome," +
//						"colaborador.cdpessoa, colaborador.nome, materialmestre.cdmaterial, materialmestre.nome, materialmestre.identificacao, materialmestre.peso, " +
//						"materialmestre.vendapromocional, listaMaterialrelacionado.quantidade, materialpromocao.cdmaterial, material.metrocubicovalorvenda, " +
//						"listavendamaterial.fatorconversao, listavendamaterial.qtdereferencia, unidademedida.cdunidademedida, listavendamaterial.multiplicador, " +
//						"unidademedidamaterial.cdunidademedida")
//				.join("venda.listavendamaterial listavendamaterial")
//				.leftOuterJoin("venda.cliente cliente")
//				.leftOuterJoin("venda.colaborador colaborador")
//				.leftOuterJoin("listavendamaterial.material material")
//				.leftOuterJoin("listavendamaterial.unidademedida unidademedida")
//				.leftOuterJoin("material.unidademedida unidademedidamaterial")
//				.leftOuterJoin("listavendamaterial.materialmestre materialmestre")
//				.leftOuterJoin("material.materialgrupo materialgrupo")
//				.leftOuterJoin("material.listaFornecedor listaFornecedor")
//				.leftOuterJoin("listaFornecedor.fornecedor fornecedor")
//				.leftOuterJoin("cliente.listaPessoacategoria listaPessoacateroria")
//				.leftOuterJoin("listaPessoacateroria.categoria categoria")
//				.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
//				.leftOuterJoin("listaClientevendedor.colaborador colaboradorcliente")
//				.leftOuterJoin("materialmestre.listaMaterialrelacionado listaMaterialrelacionado")
//				.leftOuterJoin("listaMaterialrelacionado.materialpromocao materialpromocao")
//				.openParentheses()
//					.where("venda.vendasituacao = ?", Vendasituacao.FATURADA)
//				.or()
//					.where("venda.vendasituacao = ?", Vendasituacao.REALIZADA)
//				.closeParentheses()
//				.where("venda.projeto = ?", filtro.getProjeto())
//				.where("cliente = ?", filtro.getCliente())
//				.where("materialgrupo = ?", filtro.getMaterialgrupo())
//				.where("venda.dtvenda >= ?", filtro.getDtInicio())
//				.where("venda.dtvenda <= ?", filtro.getDtFim() != null ? filtro.getDtFim() : SinedDateUtils.currentDate())
//				.where("categoria = ?", filtro.getCategoria())
//				.where("(select count(*) " +
//					   " from Materialdevolucao md" +
//					   " join md.venda v" +
//					   " where v.cdvenda=venda.cdvenda and md.material.cdmaterial = material.cdmaterial)=0");		
//		
//		if(filtro.getEmpresa() != null){
//			query
//				.openParentheses()
//					.where("venda.empresa = ?", filtro.getEmpresa())
//				.or()
//					.where("venda.empresa is null")
//				.closeParentheses();
//		}
//		
//		if (filtro.getProduto() || filtro.getServico() || filtro.getEpi() || filtro.getPatrimonio()) {
//			query.openParentheses();
//			if (filtro.getProduto()) {
//				query.where("material.produto = ?", filtro.getProduto());
//			}
//			if (filtro.getServico()) {
//				if (filtro.getProduto()) {
//					query.or();
//				}
//				query.where("material.servico = ?", filtro.getServico());
//			}
//			if (filtro.getEpi()) {
//				if (filtro.getProduto() || filtro.getServico()) {
//					query.or();
//				}
//				query.where("material.epi = ?", filtro.getEpi());
//			}
//			if (filtro.getPatrimonio()) {
//				if (filtro.getProduto() || filtro.getServico()
//						|| filtro.getEpi()) {
//					query.or();
//				}
//				query.where("material.patrimonio = ?", filtro
//						.getPatrimonio());
//			}
//			query.closeParentheses();
//		}
//		
//		if((filtro.getIsRestricaoVendaVendedor() != null && filtro.getIsRestricaoVendaVendedor()) || 
//				(filtro.getIsRestricaoClienteVendedor() != null && filtro.getIsRestricaoClienteVendedor()) ){ 
//			Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
//			if(colaborador == null) {
//				query.where(" 1 = 0");
//			}else {
//				if(filtro.getIsRestricaoClienteVendedor()){
//					query
//						.openParentheses()
//							.where("colaboradorcliente = ?", colaborador)
//							.or()
//							.where("listaClientevendedor is null")
//						.closeParentheses();
//					
//				}
//				if(filtro.getIsRestricaoVendaVendedor()){
//					query.where("colaborador = ?", colaborador);	
//				}else {
//					query.where("colaborador = ?", filtro.getColaborador());	
//				}
//			}
//		}else {
//			query.where("colaborador = ?",filtro.getColaborador());
//		}
//		
//		if(filtro.getFornecedor()!=null){
//			query.where("fornecedor = ?",filtro.getFornecedor());
//		}
//		if(filtro.getMaterial()!=null){
//			query.where("material = ?",filtro.getMaterial());
//		}
//		if(filtro.getMaterialtipo()!=null){
//			query.join("material.materialtipo materialtipo");
//			query.where("materialtipo = ?",filtro.getMaterialtipo());
//		}
//		
//		if(Emitircurvaabcranking.FORNECEDOR.equals(filtro.getEmitircurvaabcranking())){
//			query.openParentheses();
//			query.where("listaFornecedor is null");
//			query.or();
//			query.where("listaFornecedor.fabricante=?", Boolean.TRUE);
//			query.closeParentheses();
//        }
//		
//		if (filtro.getPedidovendatipo()!=null){
//			query.join("venda.pedidovenda pedidovenda");
//			query.where("pedidovenda.pedidovendatipo=?", filtro.getPedidovendatipo());
//		}
//		
//		return query.list();
//	}

	/**
	 * M�todo que carrega o materialtabelapreco da venda
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Venda loadWithMaterialtabelapreco(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nula.");
		
		return query()
			.select("venda.cdvenda, materialtabelapreco.cdmaterialtabelapreco, materialtabelapreco.nome")
			.leftOuterJoin("venda.materialtabelapreco materialtabelapreco")
			.where("venda = ?", venda)
			.unique();
	}

	/**
	 * M�todo que carrega o vendamaterial com a venda
	 *
	 * @param venda
	 * @return
	 * @author Filipe Santos
	 */
	public Venda loadWithVendamaterial(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode sers nula.");
		
		return query()
			.select(" venda.cdvenda, venda.dtvenda, vendamaterial.cdvendamaterial, vendamaterial.quantidade, expedicaoitem.cdexpedicaoitem, " +
					" expedicaoitem.qtdeexpedicao, materialVenda.cdmaterial, materialExpedicao.cdmaterial, expedicao.cdexpedicao," +
					" materialVenda.nome, cliente.nome, cliente.cdpessoa, unidademedida.cdunidademedida, unidademedida.nome, " +
					" unidademedidaVendaMaterial.cdunidademedida, unidademedidaVendaMaterial.nome, localarmazenagem.cdlocalarmazenagem, " +
					" localarmazenagem.nome, contato.cdpessoa, contato.nome")
			.join("venda.listavendamaterial vendamaterial")
			.join("venda.cliente cliente")
			.join("vendamaterial.material materialVenda")
			.leftOuterJoin("materialVenda.unidademedida unidademedida")
			.leftOuterJoin("vendamaterial.listaExpedicaoitem expedicaoitem")
			.leftOuterJoin("vendamaterial.unidademedida unidademedidaVendaMaterial")
			.leftOuterJoin("expedicaoitem.material materialExpedicao")
			.leftOuterJoin("expedicaoitem.expedicao expedicao")
			.leftOuterJoin("venda.localarmazenagem localarmazenagem")
			.leftOuterJoin("venda.contato contato")
			.where("venda.cdvenda = ?", venda.getCdvenda())
			.openParentheses()
				.where("expedicao.expedicaosituacao <> ?",Expedicaoacao.CANCELADA)
				.or()
				.where("expedicao.cdexpedicao = null")
			.closeParentheses()
			.unique();
	}
	
	/**
	 * Busca as vendas dos documentos passados por par�metro.
	 *
	 * @param whereInDocumento
	 * @return
	 * @since 21/09/2012
	 * @author Rodrigo Freitas
	 */
	public List<Venda> findByDocumento(String whereInDocumento) {
		return query()
					.select("venda.cdvenda, venda.dtvenda, empresa.nome, empresa.serienfe")
					.join("venda.listadocumentoorigem listadocumentoorigem")
					.join("listadocumentoorigem.documento documento")
					.join("venda.empresa empresa")
					.whereIn("documento.cddocumento", whereInDocumento)
					.list();
	}
	
	/**
	 * M�todo que busca os dados da venda para o relat�rio de Custo x Venda
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Venda> findForEmitircustovenda(EmitircustovendaFiltro filtro) {
		if(filtro == null)
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilderSined<Venda> query = querySined();
					query
					.select("venda.cdvenda, venda.dtvenda, venda.valorusadovalecompra, colaborador.nome, cliente.cdpessoa, cliente.nome, cliente.cnpj, cliente.cpf, cliente.identificador, " +
							"listavendamaterial.cdvendamaterial, listavendamaterial.quantidade, listavendamaterial.preco, listavendamaterial.desconto, " +
							"unidademedida.cdunidademedida, unidademedida.simbolo, unidademedida.nome, unidademedida.casasdecimaisestoque, material.cdmaterial, material.identificacao, material.nome, material.referencia, " +
							"materialgrupo.cdmaterialgrupo, materialgrupo.nome, material.valorcusto, material.valorvenda, material.metrocubicovalorvenda, material.pesoliquidovalorvenda, " +
							"listavendamaterial.altura, listavendamaterial.largura, listavendamaterial.desconto, material.produto, listavendamaterial.saldo, " +
							"listavendamaterial.comprimento, listavendamaterial.comprimentooriginal, listavendamaterial.observacao, listavendamaterial.multiplicador, " +
							"unidademedidavenda.cdunidademedida, unidademedidavenda.nome, unidademedidavenda.casasdecimaisestoque, listavendamaterial.fatorconversao, listavendamaterial.qtdereferencia, " +
							"listavendamaterial.valorcustomaterial, listavendamaterial.valorvendamaterial, venda.desconto, listavendamaterial.desconto, pedidovenda.percentualdesconto," +
							"pedidovendatipo.bonificacao, pedidovendatipoVenda.cdpedidovendatipo, pedidovendatipoVenda.bonificacao ")
					.join("venda.empresa empresa")
					.join("venda.listavendamaterial listavendamaterial")
					.join("listavendamaterial.material material")
					.join("material.unidademedida unidademedida")
					.join("venda.vendasituacao vendasituacao")
					.leftOuterJoin("material.materialgrupo materialgrupo")
					.leftOuterJoin("venda.cliente cliente")
					.leftOuterJoin("venda.colaborador colaborador")
					.leftOuterJoin("listavendamaterial.unidademedida unidademedidavenda")
					.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
					.leftOuterJoin("listaClientevendedor.colaborador colaboradorcliente")
					.leftOuterJoin("venda.pedidovenda pedidovenda")
					.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
					.leftOuterJoin("venda.pedidovendatipo pedidovendatipoVenda");
		
		if(filtro.getWhereInVenda() != null && !"".equals(filtro.getWhereInVenda())){
			query
				.whereIn("venda.cdvenda", filtro.getWhereInVenda())
				.where("vendasituacao.cdvendasituacao <> ?", Vendasituacao.CANCELADA.getCdvendasituacao());
		}else {					
			query
				.where("empresa = ?", filtro.getEmpresa())
				.where("venda.dtvenda >= ?", filtro.getDtinicio())
				.where("venda.dtvenda <= ?", filtro.getDtfim())
				.where("cliente = ?", filtro.getCliente())
				.whereIn("venda.cdvenda", filtro.getWhereInVenda())
				.where("vendasituacao.cdvendasituacao <> ?", Vendasituacao.CANCELADA.getCdvendasituacao());
			
			if (SinedUtil.isListNotEmpty(filtro.getListapedidovendatipo())) {
				query
				.openParentheses()
				.whereIn("pedidovendatipoVenda.cdpedidovendatipo",CollectionsUtil.listAndConcatenate(filtro.getListapedidovendatipo(), "cdpedidovendatipo", ","))
				.or()
				.where("pedidovendatipoVenda is null")
				.closeParentheses();
			} else {
				query
				.openParentheses()
				.where("pedidovendatipoVenda.bonificacao <> true")
				.or()
				.where("pedidovendatipoVenda is null")
				.closeParentheses();
			}
			
			if(filtro.getIsRestricaoVendaVendedor() != null && filtro.getIsRestricaoVendaVendedor()){ 
				Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
				if(colaborador == null) {
					query.where("1 = 0");
				}else {
					if(filtro.getIsRestricaoClienteVendedor() == null || !filtro.getIsRestricaoVendaVendedor()){
						filtro.setColaborador(colaborador);
						query.where("colaborador = ?", colaborador);					
					}else {
						if(filtro.getColaborador() == null || !filtro.getColaborador().equals(colaborador)){
							if(filtro.getColaborador() == null){
								query.openParentheses()
									.where("colaboradorcliente = ?", colaborador)
									.or()
									.where("colaborador = ?", colaborador)
								.closeParentheses();
							}else {
								query.openParentheses()
									.where("colaboradorcliente = ?", colaborador)
									.where("colaborador = ?", filtro.getColaborador())
								.closeParentheses();
							}
						}else {
							query.where("colaborador = ?", colaborador);
						}
						
					}
				}
			} else {
				if (filtro.getColaborador() != null && filtro.getIsVendedorPrincipal() != null && filtro.getIsVendedorPrincipal()) {
					query
						.openParentheses()
							.where("listaClientevendedor.colaborador = ?", filtro.getColaborador())
							.where("listaClientevendedor.principal = ?", Boolean.TRUE)
						.closeParentheses();
				} else {
					query.where("colaborador = ?", filtro.getColaborador());
				}
			}
			
			if(Boolean.TRUE.equals(filtro.getVendedoresAtuais())){
				query.join("colaborador.colaboradorsituacao colaboradorsituacao")
				.leftOuterJoin("colaborador.listaContratocolaborador listaContratocolaborador")
				.leftOuterJoin("listaContratocolaborador.contrato contrato")
				.where("colaboradorsituacao.cdcolaboradorsituacao <> ?", Colaboradorsituacao.DEMITIDO)
				.orderBy("colaborador.nome");
			}
		}
		
		adicionaJoinWhereForMaterial(filtro, query);
		
		return query.list();
	}
	
	private void adicionaJoinWhereForMaterial(EmitircustovendaFiltro filtro, QueryBuilder<Venda> query) {
		if(filtro.getMaterial() != null || 
				(filtro.getProduto() != null && filtro.getProduto()) ||
				(filtro.getServico() != null && filtro.getServico()) ||
				(filtro.getEpi() != null && filtro.getEpi()) ||
				(filtro.getPatrimonio() != null && filtro.getPatrimonio()) ||
				filtro.getMaterialcategoria() != null){
			
			StringBuilder selectForMaterial = new StringBuilder();
			String whereMaterialtipo = "";
			String whereMaterial = "";
			String whereFornecedor = "";
			String whereClasseMaterial = "";
			String whereMaterialcategoria = "";
			
			if(filtro.getMaterialcategoria()!=null){
				selectForMaterial.append(",materialcategoria.cdmaterialcategoria ");
				query.leftOuterJoinIfNotExists("material.materialcategoria materialcategoria");
				whereMaterialcategoria += " AND m.materialcategoria.cdmaterialcategoria = " + filtro.getMaterialcategoria().getCdmaterialcategoria() + " ";
			}
			if(filtro.getMaterial()!=null){
				whereMaterial += " AND m.cdmaterial = " + filtro.getMaterial().getCdmaterial() + " ";
			}
			if (filtro.getProduto() || filtro.getServico() || filtro.getEpi() || filtro.getPatrimonio()) {
				selectForMaterial.append(",material.produto, material.servico, material.epi, material.patrimonio ");
				whereClasseMaterial = " AND ";
				whereClasseMaterial += " ( ";
				if (filtro.getProduto()) {
					whereClasseMaterial += " m.produto = " + filtro.getProduto() + " ";
				}
				if (filtro.getServico()) {
					if (filtro.getProduto()) {
						whereClasseMaterial += " OR ";
					}
					whereClasseMaterial += " m.servico = " + filtro.getServico() + " ";
				}
				if (filtro.getEpi()) {
					if (filtro.getProduto() || filtro.getServico()) {
						whereClasseMaterial += " OR ";
					}
					whereClasseMaterial += " m.epi = " + filtro.getEpi() + " ";
				}
				if (filtro.getPatrimonio()) {
					if (filtro.getProduto() || filtro.getServico() || filtro.getEpi()) {
						whereClasseMaterial += " OR ";
					}
					whereClasseMaterial += " m.patrimonio = " + filtro.getPatrimonio() + " ";
				}
				whereClasseMaterial += " ) ";
			}
			
			query.select(query.getSelect().getValue() + selectForMaterial.toString());
			query.where(" exists (	  select vm.venda.cdvenda " +
								 	" from Vendamaterial vm " +
								 	" join vm.material m " +
								 	" where vm.venda.cdvenda = venda.cdvenda " +
								 	whereMaterialtipo +
								 	whereMaterialcategoria +
								 	whereMaterial + 
								 	whereFornecedor +
								 	whereClasseMaterial + " ) ");
		}
	}

	/**
	 * Verifica se as vendas tem o cliete com a mesma matriz.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/11/2012
	 */
	public boolean mesmaMatrizVendas(String whereIn) {
		SinedUtil.markAsReader();
		long contador = getJdbcTemplate().queryForLong("select count(*) as contador " +
														"from ( " +
														"select substr(coalesce(p.cnpj, p.cpf), 0, 9) as matriz " +
														"from venda v " +
														"join pessoa p on p.cdpessoa = v.cdcliente " +
														"where v.cdvenda in (" + whereIn + ") " +
														"and (p.cnpj is not null or p.cpf is not null) " +
														"group by substr(coalesce(p.cnpj, p.cpf), 0, 9) " +
														") query");

		SinedUtil.markAsReader();
		long contadorCliente = getJdbcTemplate().queryForLong("select count(*) as contador " +
																"from ( " +
																"select cdpessoa as matriz " +
																"from venda v " +
																"join pessoa p on p.cdpessoa = v.cdcliente " +
																"where v.cdvenda in (" + whereIn + ") " +
																"group by cdpessoa " +
																") query");
		
		return contador == 1 && contadorCliente > 1;
	}
	
	/**
	 * Verifica se as vendas tem o mesmo cliente com valor frete e terceiros diferentes.
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 08/04/2014
	 */
	public boolean clienteIgualEFreteTerceiroDiferente(String whereIn) {
		SinedUtil.markAsReader();
		long contadorCdpessoa = getJdbcTemplate().queryForLong("select count(*) as contador " +
														"from ( " +
														"select queryVenda.cdcliente, count(queryVenda.cdcliente) " +
														"from ( " +
														"select v.cdcliente, v.cdterceiro " +
														"from venda v " +
														"where v.cdvenda in (" + whereIn + ") " +
														"group by v.cdcliente, v.cdterceiro ) queryVenda " +
														"group by queryVenda.cdcliente " +
														"having count(queryVenda.cdcliente) > 1 " +
														") query");

		SinedUtil.markAsReader();
		long contadorMatriz = getJdbcTemplate().queryForLong("select count(*) as contador " +
														"from ( " +
														"select queryVenda.matriz, count(queryVenda.matriz) " +
														"from ( " +
														"select substr(coalesce(p.cnpj, p.cpf), 0, 9) as matriz, v.cdterceiro " +
														"from venda v " +
														"join pessoa p on p.cdpessoa = v.cdcliente " +
														"where v.cdvenda in (" + whereIn + ") " +
														"group by substr(coalesce(p.cnpj, p.cpf), 0, 9), v.cdterceiro ) queryVenda " +
														"group by queryVenda.matriz " +
														"having count(queryVenda.matriz) > 1 " +
														") query");
		
		return contadorCdpessoa > 0 || contadorMatriz > 0;
	}

	/**
	 * Carrega a venda com o pedido de venda
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Venda loadWithPedidovenda(Venda venda){
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nula.");
		
		return query()
				.select("venda.cdvenda, pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao, " +
						"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.bonificacao, pedidovendatipo.reserva, pedidovendatipo.gerarexpedicaovenda, " +
						"pedidovendatipo.comodato, pedidovendatipo.baixaestoqueEnum, pedidovendatipo.gerarNotaIndustrializacaoRetorno, pedidovendatipo.gerarnotaretornovenda, " +
						"pedidovendatipo.referenciarnotaentradananotasaida, pedidovendatipo.consideraripiconta, pedidovendatipo.geracaocontareceberEnum, pedidovendatipo.naoatualizarvencimento, " +
						"pedidovendatipo.consideraripiconta, pedidovendatipo.consideraricmsstconta, pedidovendatipo.considerardesoneracaoconta, pedidovendatipo.considerarfcpstconta, " +
						"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome, naturezaoperacao.operacaonfe, naturezaoperacao.finalidadenfe, naturezaoperacao.razaosocial, " +
						"naturezaoperacaoservico.cdnaturezaoperacao, naturezaoperacaoservico.nome, naturezaoperacaoservico.operacaonfe, naturezaoperacaoservico.finalidadenfe, " +
						"notaTipo.cdNotaTipo, notaTipoServico.cdNotaTipo, " + 
						"cfopindustrializacao.cdcfop, cfopindustrializacao.codigo, " +
						"cfopindustrializacao.descricaoresumida, cfopindustrializacao.descricaocompleta, " +
						"cfopindustrializacao.naoconsiderarreceita, " +
						"cfopretorno.cdcfop, cfopretorno.codigo," +
						"cfopretorno.descricaoresumida, cfopretorno.descricaocompleta, cfopretorno.naoconsiderarreceita," +
						"templatenotaservico.cdreporttemplate, templatenotaservico.leiaute ")
				.leftOuterJoin("venda.pedidovenda pedidovenda")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("pedidovendatipo.naturezaoperacao naturezaoperacao")
				.leftOuterJoin("naturezaoperacao.notaTipo notaTipo")
				.leftOuterJoin("pedidovendatipo.naturezaoperacaoservico naturezaoperacaoservico")
				.leftOuterJoin("naturezaoperacaoservico.notaTipo notaTipoServico")
				.leftOuterJoin("pedidovendatipo.cfopindustrializacao cfopindustrializacao")
				.leftOuterJoin("pedidovendatipo.cfopretorno cfopretorno")
				.leftOuterJoin("pedidovendatipo.templatenotaservico templatenotaservico")
				.where("venda = ?", venda)
				.unique();
	}
	
	/**
	 * M�todo que carrega a venda com o pedido de venda e a empresa
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Venda loadWithPedidovendaAndEmpresa(Venda venda){
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nula.");
		
		return query()
				.select("venda.cdvenda, pedidovenda.cdpedidovenda, pedidovendatipo.cdpedidovendatipo, pedidovenda.pedidovendasituacao, " +
						"pedidovendatipo.bonificacao, pedidovendatipo.reserva, pedidovendatipo.comodato, pedidovendatipo.sincronizarComWMS, " +
						"empresa.cdpessoa, empresa.integracaowms, pedidovendatipo.confirmacaoManualWMS ")
				.leftOuterJoin("venda.pedidovenda pedidovenda")
				.leftOuterJoin("venda.empresa empresa")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.where("venda = ?", venda)
				.unique();
	}

	/**
	 * M�todo que busca as vendas do cliente com vale compra
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Venda> findWithValecompra(Cliente cliente) {
		if(cliente == null || cliente.getCdpessoa() == null)
			throw new SinedException("Cliente n�o pode ser nulo.");
		
		return query()
			.select("venda.cdvenda, venda.dtvenda, venda.valorusadovalecompra ")
			.join("venda.cliente cliente")
			.join("venda.vendasituacao vendasituacao")
			.where("vendasituacao.cdvendasituacao <> ?", Vendasituacao.CANCELADA.getCdvendasituacao())
			.where("cliente = ?", cliente)
			.list();
	}

	/**
	 * M�todo que retorna as vendas do pedido de venda
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Venda> findByPedidovenda(Pedidovenda pedidovenda, String select) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Pedido de venda n�o pode ser nulo.");
		
		QueryBuilderSined<Venda> query = querySined();
		preencheConsultaFindByPedidovenda(query, pedidovenda, select);
			
		return query
				.list();
	}
	
	public void preencheConsultaFindByPedidovenda(QueryBuilderSined<Venda> query, Pedidovenda pedidovenda){
		preencheConsultaFindByPedidovenda(query, pedidovenda, null);
	}
	
	public void preencheConsultaFindByPedidovenda(QueryBuilderSined<Venda> query, Pedidovenda pedidovenda, String select){
		if(StringUtils.isNotBlank(select)){
			query.select(select);
		}else {
			query.select("venda.cdvenda,venda.valorfrete,venda.taxapedidovenda,venda.desconto,venda.valorusadovalecompra, " +
					"vendasituacao.cdvendasituacao, pedidovenda.cdpedidovenda," +
					"vendamaterial.cdvendamaterial,vendamaterial.quantidade,vendamaterial.multiplicador," +
					"vendamaterial.preco,vendamaterial.desconto, vendamaterial.valorSeguro, vendamaterial.outrasdespesas" +
					"");
		}
		
		query
		.join("venda.vendasituacao vendasituacao")
		.join("venda.pedidovenda pedidovenda");
		
		if(select == null){
			query.leftOuterJoin("venda.listavendamaterial vendamaterial");
		}
		
		query
		.where("pedidovenda = ?", pedidovenda)
		.where("vendasituacao <> ?", Vendasituacao.CANCELADA);
	}
	
	/**
	 * M�todo que busca as vendas do or�amento
	 *
	 * @param vendaorcamento
	 * @return
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public List<Venda> findByOrcamento(Vendaorcamento vendaorcamento) {
		if(vendaorcamento == null || vendaorcamento.getCdvendaorcamento() == null)
			throw new SinedException("Or�amento n�o pode ser nulo.");
		
		return query()
				.select("venda.cdvenda, vendasituacao.cdvendasituacao, vendaorcamento.cdvendaorcamento ")
				.join("venda.vendasituacao vendasituacao")
				.join("venda.vendaorcamento vendaorcamento")
				.where("vendaorcamento = ?", vendaorcamento)
				.where("vendasituacao <> ?", Vendasituacao.CANCELADA)
				.list();
	}

	/**
	 * M�todo que busca o email do cliente e dos contatos
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Venda> findForEnviocomprovanteemail(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("venda.cdvenda, cliente.cdpessoa, cliente.nome, cliente.email, contato.cdpessoa, contato.nome, contato.emailcontato, "+
						"empresa.cdpessoa, empresa.razaosocial, empresa.nomefantasia, colaborador.cdpessoa, colaborador.nome, colaborador.email")
				.join("venda.cliente cliente")
				.join("venda.empresa empresa")
				.join("venda.colaborador colaborador")
				.leftOuterJoin("cliente.listaContato listaContato")
				.leftOuterJoin("listaContato.contato contato")
				.whereIn("venda.cdvenda", whereIn)
				.list();
	}
	
	/**
	 * M�todo que carrega os dados da venda para atualizar o saldo
	 *
	 * @param dtinicio
	 * @param dtfim
	 * @param cdvenda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Venda> findForAtualizarsaldo(String whereIn) {
		return query()
				.select("venda.cdvenda, vendamaterial.cdvendamaterial, vendamaterial.preco, vendamaterial.multiplicador, " +
						"vendamaterial.desconto, vendamaterial.quantidade, material.cdmaterial, " +
						"material.valorvendaminimo, material.valorvendamaximo, material.valorvenda, cliente.cdpessoa, prazopagamento.cdprazopagamento, " +
						"pedidovenda.cdpedidovenda, pedidovendatipo.cdpedidovendatipo, vendamaterial.fatorconversao," +
						"empresa.cdpessoa, unidademedida.cdunidademedida ")
				.join("venda.listavendamaterial vendamaterial")
				.join("vendamaterial.material material")
				.leftOuterJoin("vendamaterial.unidademedida unidademedida")
				.leftOuterJoin("venda.cliente cliente")
				.leftOuterJoin("venda.pedidovenda pedidovenda")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("venda.prazopagamento prazopagamento")
				.leftOuterJoin("venda.empresa empresa")
				.whereIn("venda.cdvenda", whereIn)
				.list();
	}
	
	/**
	 * M�todo que verifica se existe venda com o cliente e/ou grupo material
	 *
	 * @param cliente
	 * @param materialgrupo
	 * @param empresa
	 * @param venda
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existVendaByClienteMaterialgrupo(Cliente cliente, Materialgrupo materialgrupo, Empresa empresa, Venda venda, Pedidovenda pedidovenda) {
		if(cliente == null)
			throw new SinedException("Cliente n�o pode ser nulo");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Venda.class)
			.join("venda.cliente cliente")
			.leftOuterJoin("venda.pedidovenda pedidovenda")
			.leftOuterJoin("venda.vendasituacao vendasituacao")
			.leftOuterJoin("venda.empresa empresa")
			.leftOuterJoin("venda.listavendamaterial listavendamaterial")
			.leftOuterJoin("listavendamaterial.material material")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.where("venda <> ?", (venda != null && venda.getCdvenda() != null ? venda : null))
			.where("pedidovenda <> ?", (pedidovenda != null && pedidovenda.getCdpedidovenda() != null ? pedidovenda : null))
			.where("cliente = ?", cliente)
			.where("materialgrupo = ?", materialgrupo)
			.where("empresa = ?", empresa)
			.where("vendasituacao <> ?", Vendasituacao.CANCELADA)
			.unique() > 0;
	}

	/**
	 * M�todo que faz o update do saldo final do pedido de venda
	 *
	 * @param pedidovenda
	 * @param saldofinal
	 * @author Luiz Fernando
	 */
	public void updateSaldofinalByVenda(Venda venda, Money saldofinal) {
		if(venda != null && venda.getCdvenda() != null && saldofinal != null){
			getHibernateTemplate()
			.bulkUpdate("update Venda v set v.saldofinal = ? where v.id = " + venda.getCdvenda() + " ", 
					new Object[]{saldofinal});
		}		
	}

	/**
	 * M�todo que busca as vendas para acompanhamento de meta
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Venda> findForAcompanhamentometa(EmitiracompanhamentometaFiltro filtro) {
		QueryBuilderSined<Venda> query = querySined();
		
		query
			.select("venda.cdvenda, venda.dtvenda, vendamaterial.cdvendamaterial, vendamaterial.preco, vendamaterial.multiplicador, venda.taxapedidovenda, " +
					"vendamaterial.desconto, vendamaterial.quantidade, vendamaterial.fatorconversao, vendamaterial.qtdereferencia, " +
					"venda.valorfrete, venda.desconto, venda.valorusadovalecompra, venda.valorfrete, " +
					"colaborador.cdpessoa, colaborador.nome, listaColaboradorcargo.cdcolaboradorcargo, listaColaboradorcargo.dtfim, cargo.cdcargo, " +
					"material.cdmaterial, unidademedida.cdunidademedida, unidademedidamaterial.cdunidademedida," +
					"vendamaterial.altura, vendamaterial.comprimento, vendamaterial.largura, listaColaboradorcargo.matricula ")
			.join("venda.colaborador colaborador")
			.leftOuterJoin("colaborador.listaColaboradorcargo listaColaboradorcargo")
			.leftOuterJoin("listaColaboradorcargo.cargo cargo")
			.join("venda.listavendamaterial vendamaterial")
			.leftOuterJoin("vendamaterial.material material")
			.leftOuterJoin("vendamaterial.unidademedida unidademedida")
			.leftOuterJoin("material.unidademedida unidademedidamaterial")
			.leftOuterJoin("venda.empresa empresa")
			.leftOuterJoin("venda.vendasituacao vendasituacao")
			.leftOuterJoin("venda.pedidovenda pedidovenda")
			.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
			.leftOuterJoin("venda.cliente cliente")
			.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
			.where("venda.dtvenda >= ?", filtro.getDtreferenciaInicio())
			.where("venda.dtvenda <= ?", filtro.getDtreferenciaFim() != null ? filtro.getDtreferenciaFim() : filtro.getFirstDateMonth())
			.where("empresa = ?", filtro.getEmpresa())
			.where("vendasituacao <> ?", Vendasituacao.CANCELADA)
			.openParentheses()
				.where("pedidovendatipo is null")
				.or()
			.where("pedidovendatipo.comodato <> ?", Boolean.TRUE)
			.closeParentheses();
		
		
		if (filtro.getColaborador() != null && filtro.getIsVendedorPrincipal() != null && filtro.getIsVendedorPrincipal()) {
			query
				.openParentheses()
					.where("listaClientevendedor.colaborador = ?", filtro.getColaborador())
					.where("listaClientevendedor.principal = ?", Boolean.TRUE)
				.closeParentheses();
		} else {
			query.where("colaborador = ?", filtro.getColaborador());
		}
		
		return query.list();
	}

	/**
	 * M�todo que carrega a venda com pedido de venda 
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Venda loadForRecalcularcomissao(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nula.");
		
		return query()
				.select("venda.cdvenda, pedidovenda.cdpedidovenda, pedidovendatipo.cdpedidovendatipo, pedidovenda.pedidovendasituacao, " +
						"pedidovendatipo.bonificacao, pedidovendatipo.reserva, pedidovendatipo.geracaocontareceberEnum ")
				.leftOuterJoin("venda.pedidovenda pedidovenda")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.where("venda = ?", venda)
				.unique();
	}

	/**
	 * M�todo que busca as vendas que tenham origem de pedido de venda tipo comodato
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Venda> findPedidovendacomodato(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("venda.cdvenda, pedidovenda.cdpedidovenda, pedidovendatipo.cdpedidovendatipo, pedidovenda.pedidovendasituacao, " +
					"pedidovendatipo.comodato, pedidovendaorigem.cdpedidovenda ")
			.join("venda.pedidovenda pedidovenda")
			.join("pedidovenda.pedidovendatipo pedidovendatipo")
			.join("pedidovenda.pedidovendaorigem pedidovendaorigem")
			.whereIn("venda.cdvenda", whereIn)
			.where("pedidovendatipo.comodato = true")
			.list();
	}
	
	/**
	 * M�todo que busca as vendas que tenham origem de pedido que est�o relacionados a um pedido de venda (comodato)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Venda> findPedidovendaorigem(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("venda.cdvenda, pedidovenda.cdpedidovenda ")
			.join("venda.pedidovenda pedidovenda")
			.whereIn("venda.cdvenda", whereIn)
			.where("exists (select pvo.cdpedidovenda from venda v join v.pedidovenda pv " +
													"join pv.pedidovendaorigem pvo " +
													"where pvo.cdpedidovenda = pedidovenda.cdpedidovenda ) ")
			.list();
	}

	public List<Venda> findForTotalizacaoListagem(VendaFiltro filtro) {
		QueryBuilderSined<Venda> query = querySined();
		this.updateListagemQuery(query, filtro);
//		this.joinVendamaterialUpdatelistagem(query);
		
//		query.setIgnoreJoinPaths(new HashSet<String>());
		
		query
		.select("venda.cdvenda, venda.desconto");
//		.leftOuterJoin("venda.pedidovenda pedidovenda")
//		.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo");
	
		return query.list();
	}
	
	public List<Venda> findForTotalizacaoListagem(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			return null;
		
		QueryBuilder<Venda> query = query()
			.select("venda.cdvenda, venda.valorfrete, venda.taxapedidovenda, venda.desconto, venda.saldofinal, venda.taxapedidovenda, " +
					"vendamaterial.cdvendamaterial, vendamaterial.quantidade, vendamaterial.multiplicador, venda.dtvenda, " +
					"vendamaterial.preco, vendamaterial.desconto, vendamaterial.peso, material.peso, material.pesobruto, material.metrocubicovalorvenda, " +
					"pedidovendatipo.bonificacao, venda.valorusadovalecompra, vendamaterial.valoripi, " +
					"vendamaterial.fatorconversao, vendamaterial.qtdereferencia, unidademedida.cdunidademedida, unidademedidamaterial.cdunidademedida, " +
                    "vendamaterial.tipotributacaoicms, " +
                    "vendamaterial.tipocobrancaicms, vendamaterial.valorbcicms, vendamaterial.icms, " +
                    "vendamaterial.valoricms, vendamaterial.valorbcicmsst, vendamaterial.icmsst, " +
                    "vendamaterial.valoricmsst, vendamaterial.reducaobcicmsst, vendamaterial.margemvaloradicionalicmsst, " +
                    "vendamaterial.valorbcfcp, vendamaterial.fcp, vendamaterial.valorfcp, " +
                    "vendamaterial.valorbcfcpst, vendamaterial.fcpst, vendamaterial.valorfcpst, " +
                    "vendamaterial.valorbcdestinatario, vendamaterial.valorbcfcpdestinatario, vendamaterial.fcpdestinatario, " +
                    "vendamaterial.icmsdestinatario, vendamaterial.icmsinterestadual, vendamaterial.icmsinterestadualpartilha, " +
                    "vendamaterial.valorfcpdestinatario, vendamaterial.valoricmsdestinatario, vendamaterial.valoricmsremetente, " +
                    "vendamaterial.percentualdesoneracaoicms, vendamaterial.valordesoneracaoicms, vendamaterial.abaterdesoneracaoicms, " +
                    "vendamaterial.difal, vendamaterial.valordifal, ncmcapitulo.cdncmcapitulo, cfop.cdcfop, " +
                    "vendamaterial.ncmcompleto, vendamaterial.modalidadebcicms, vendamaterial.modalidadebcicmsst, vendamaterial.valorSeguro, vendamaterial.outrasdespesas, " +
					"emporiumvenda.cdemporiumvenda, emporiumvenda.pdv")
			.leftOuterJoin("venda.listaEmporiumvenda emporiumvenda") 
			.leftOuterJoin("venda.listavendamaterial vendamaterial") 
			.leftOuterJoin("vendamaterial.material material")
			.leftOuterJoin("vendamaterial.unidademedida unidademedida")
            .leftOuterJoin("vendamaterial.cfop cfop")
            .leftOuterJoin("vendamaterial.ncmcapitulo ncmcapitulo")
			.leftOuterJoin("material.unidademedida unidademedidamaterial")
			.leftOuterJoin("venda.pedidovenda pedidovenda")
			.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo");
		
		SinedUtil.quebraWhereIn("venda.cdvenda", whereIn, query);
			
		return query.list();
	}

	/**
	 * M�todo que remove a referencia do pedidovendamaterial da vendamaterial
	 *
	 * @param venda
	 * @author Luiz Fernando
	 */
	public void updateVendamaterialWithPedidovenda(Venda venda) {
		if(venda != null && venda.getCdvenda() != null){
			String sql = "update vendamaterial set cdpedidovendamaterial = NULL where cdvenda = " + venda.getCdvenda();
			getJdbcTemplate().execute(sql);
		}
	}

	/**
	 * M�todo que carrega a venda com informa��es do tipo de pedido
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Venda carregarInfNotaVenda(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nula.");
		
		return query()
				.select("venda.cdvenda, pedidovendatipo.cdpedidovendatipo, " +
						"pedidovendatipo.infoadicionalcontrib, pedidovendatipo.infoadicionalfisco, " +
						"venda.observacao, emporiumvenda.numero_ticket")
				.leftOuterJoin("venda.listaEmporiumvenda emporiumvenda")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.where("venda = ?", venda)
				.unique();
	}

	/**
	 * Busca pelo identificador da venda.
	 *
	 * @param identificadorPedidovenda
	 * @return
	 * @author Rodrigo Freitas
	 * @param identificadorPedidovenda 
	 * @since 02/07/2013
	 */
	public List<Venda> findByEmpresaIdentificador(String empresa_cnpj, String identificadorPedidovenda) {
		if(identificadorPedidovenda == null || identificadorPedidovenda.equals("")){
			return null;
		}
		return query()
				.select("venda.cdvenda, venda.dtvenda")
				.join("venda.empresa empresa")
				.where("empresa.cnpj = ?", empresa_cnpj != null ? new Cnpj(empresa_cnpj) : null)
				.where("venda.identificador = ?", identificadorPedidovenda)
				.where("venda.vendasituacao <> ? ", Vendasituacao.CANCELADA)
				.list();
	}
	
	/**
	 * M�todo que retorna as vendas com devolu��o
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Venda> findVendaDevolvida(String whereIn){
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				
				.select("venda.cdvenda, venda.dtvenda")
				.whereIn("venda.cdvenda", whereIn)
				.where("exists (SELECT v.cdvenda FROM Materialdevolucao md join md.venda v WHERE v.cdvenda = venda.cdvenda and md.dtcancelamento is null )")
				.list();
	}

	/**
	 * M�todo que verifica se a venda tem servicos
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean existVendaWithServico(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Venda.class)
			.join("venda.listavendamaterial listavendamaterial")
			.join("listavendamaterial.material material")
			.leftOuterJoin("listavendamaterial.materialmestre materialmestre")
			.whereIn("venda.cdvenda", whereIn)
			.openParentheses()
				.where("material.servico = true")
				.or()
				.where("material.tributacaomunicipal = true")
			.closeParentheses();
		
		if(!SinedUtil.isRecapagem()){
			query.openParentheses()
				.where("materialmestre is null")
				.or()
				.where("coalesce(materialmestre.kitflexivel, false) = true")
			.closeParentheses();
		}
				
			
		return query.unique() > 0;
	}
	
	public boolean existVendaApenasServico(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Venda.class)
			.join("venda.listavendamaterial listavendamaterial")
			.join("listavendamaterial.material material")
			.leftOuterJoin("listavendamaterial.materialmestre materialmestre")
			.whereIn("venda.cdvenda", whereIn)
			.openParentheses()
				.where("material.servico = true")
				.or()
				.where("material.tributacaomunicipal = true")
			.closeParentheses()
			.where("materialmestre is null", !SinedUtil.isRecapagem())
			.where("not exists (select v.cdvenda from Vendamaterial vm " +
							"join vm.venda v " +
							"join vm.material m where v.cdvenda = venda.cdvenda and m.servico <> true )")
			.unique() > 0;
	}
	
	public boolean existVendaApenasProduto(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Venda.class)
			.join("venda.listavendamaterial listavendamaterial")
			.join("listavendamaterial.material material")
			.leftOuterJoin("listavendamaterial.materialmestre materialmestre")
			.whereIn("venda.cdvenda", whereIn)
			.where("material.servico <> true")
			.where("material.tributacaomunicipal <> true")
			.where("materialmestre is null", !SinedUtil.isRecapagem())
			.where("not exists (select v.cdvenda from Vendamaterial vm " +
							"join vm.venda v " +
							"join vm.material m where v.cdvenda = venda.cdvenda and m.servico = true )")
			.unique() > 0;
	}
	
	/**
	* M�todo que verifica se existe venda com tipo de pedido para gerar receita ap�s emiss�o da nota
	*
	* @param whereIn
	* @return
	* @since 21/07/2014
	* @author Luiz Fernando
	*/
	public boolean existVendaReceitaAposEmissaoNota(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Venda.class)
			.join("venda.pedidovendatipo pedidovendatipo")
			.whereIn("venda.cdvenda", whereIn)
			.where("pedidovendatipo.geracaocontareceberEnum = ?", GeracaocontareceberEnum.APOS_EMISSAONOTA)
			.unique() > 0;
	}

	/**
	 * M�todo que verirfica se a venda tem produtos
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean existVendaWithProduto(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Venda.class)
			.join("venda.listavendamaterial listavendamaterial")
			.join("listavendamaterial.material material")
			.whereIn("venda.cdvenda", whereIn)
			.openParentheses()
				.where("material.servico <> true").or()
				.where("material.servico is null")
			.closeParentheses()
			.unique() > 0;
	}

	public void updateClienteEndereco(String whereInVenda, Cliente cliente, Endereco endereco) {
		getHibernateTemplate().bulkUpdate("update Venda v set v.cliente = ?, v.endereco = ? where v.cdvenda in (" + whereInVenda + ")", new Object[]{
			cliente, endereco
		});
		
	}
	
	/**
	 * M�todo que carrega a venda com os dados do material e unidade de convers�o
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 22/10/2013
	 */
	public List<Venda> findForResumoUnidademedidaQtde(String whereIn){
		if (whereIn == null || "".equals(whereIn)) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		QueryBuilder<Venda> query = query()
				.select("vendamaterial.cdvendamaterial, venda.cdvenda, " +
						"vendamaterial.preco, vendamaterial.desconto, vendamaterial.quantidade, vendamaterial.saldo, vendamaterial.qtdereferencia, " +
						"vendamaterial.comprimento, vendamaterial.altura, vendamaterial.comprimentooriginal, " +
						"vendamaterial.largura, vendamaterial.fatorconversao, vendamaterial.observacao, vendamaterial.producaosemestoque, " +
						"vendamaterial.valorcustomaterial, vendamaterial.valorvendamaterial, vendamaterial.multiplicador, " +
						"material.cdmaterial, material.produto, material.servico, material.extipi, unidademedida.simbolo, " +
						"material.patrimonio, material.epi, unidademedida.cdunidademedida, unidademedida.nome, " +
						"material.producao, material.peso, material.pesobruto, material.valorcusto, " + 
						"unidademedidamaterial.cdunidademedida, unidademedidamaterial.simbolo, " + 
						"material.valorvendaminimo, material.valorvendamaximo, loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, " +
						"material.qtdeunidade, material.considerarvendamultiplos, " +
						"material.metrocubicovalorvenda, material.pesoliquidovalorvenda," +
						"listaMaterialunidademedida.cdmaterialunidademedida, " +
						"unidademedida.cdunidademedida, listaMaterialunidademedida.fracao, listaMaterialunidademedida.qtdereferencia, " +
						"listaMaterialunidademedida.qtdereferencia, " +
						"listaMaterialunidademedida.valorunitario, unidademedidamaterial.cdunidademedida, " +
						"listaMaterialunidademedida.mostrarconversao, unidademedidamaterial.nome, " +
						"listaMaterialunidademedida.prioridadevenda, listaMaterialunidademedida.prioridadecompra, unidademedida.simbolo, unidademedidamaterial.simbolo," +
						"unidademedidavenda.cdunidademedida, unidademedidavenda.simbolo, unidademedidavenda.nome ")
				.join("venda.listavendamaterial vendamaterial")
				.join("vendamaterial.material material")
				.leftOuterJoin("material.unidademedida unidademedidamaterial")
				.leftOuterJoin("material.listaMaterialunidademedida listaMaterialunidademedida")
				.leftOuterJoin("listaMaterialunidademedida.unidademedida unidademedida")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("vendamaterial.unidademedida unidademedidavenda")
				.leftOuterJoin("vendamaterial.loteestoque loteestoque");
		
		SinedUtil.quebraWhereIn("venda.cdvenda", whereIn, query);
				
		return query.list();
	}
	
	/**
	 * M�todo que carrega a venda para troca de respons�vel
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public Venda loadForTrocarvendedor(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("enda n�o pode ser nulo");
		
		return query()
				.select("venda.cdvenda, colaborador.cdpessoa, colaborador.nome, cliente.cdpessoa, cliente.nome, " +
						"colaboradorcliente.cdpessoa, colaboradorcliente.nome ")
				.leftOuterJoin("venda.colaborador colaborador")
				.leftOuterJoin("venda.cliente cliente")
				.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
				.leftOuterJoin("listaClientevendedor.colaborador colaboradorcliente")
				.where("venda = ?", venda)
				.unique();
	}

	/**
	 * M�todo que atualiza o respons�vel da venda
	 *
	 * @param venda
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public void updateColaboradorByVenda(Venda venda) {
		if(venda != null && venda.getCdvenda() != null && venda.getColaborador() != null && 
				venda.getColaborador().getCdpessoa() != null){
			getHibernateTemplate().bulkUpdate("update Venda v set v.colaborador = ? where v.id in (" + venda.getCdvenda() + ")", 
					new Object[]{venda.getColaborador()});
		}		
	}
	
	/**
	 * M�todo que busca as vendas para apura��o
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 * @since 27/12/2013
	 */
	public List<Venda> findForApuracaovendaReport(ApuracaovendaFiltro filtro) {
		QueryBuilderSined<Venda> query = querySined();
		
		query
			.select("venda.cdvenda, venda.dtvenda, venda.desconto, venda.valorusadovalecompra, " +
					"vendamaterial.cdvendamaterial, vendamaterial.quantidade,vendamaterial.fatorconversao, " +
					"vendamaterial.qtdereferencia, vendamaterial.preco, vendamaterial.desconto, vendamaterial.multiplicador, " +
					"vendamaterial.fatorconversao, vendamaterial.qtdereferencia," +
					"vendamaterial.altura, vendamaterial.largura, vendamaterial.comprimento, vendamaterial.comprimentooriginal, " +
					"unidademedidamaterial.cdunidademedida, unidademedidamaterial.nome, unidademedidamaterial.simbolo, " +
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, vendamaterial.valorvendamaterial, " +
					"material.cdmaterial, material.nome, material.identificacao, material.valorvenda, material.peso, material.pesobruto," +
					"material.metrocubicovalorvenda, material.pesoliquidovalorvenda, material.valorcusto, vendamaterial.valorcustomaterial, " +
					"materialgrupo.cdmaterialgrupo, materialgrupo.nome, materialmestre.cdmaterial, materialmestre.nome, " +
					"materialmestre.identificacao, materialmestre.vendapromocional, materialmestre.valorvenda, materialmestre.valorcusto," +
					"materialgrupomestre.cdmaterialgrupo, materialgrupo.nome, unidademedidamestre.cdunidademedida, unidademedidamestre.nome," +
					"unidademedidamestre.simbolo " + 
					(filtro.getConsiderarfrete() != null && filtro.getConsiderarfrete() ? ", venda.valorfrete " : "")
					)
			.leftOuterJoin("venda.empresa empresa")
			.leftOuterJoin("venda.pedidovenda pedidovenda")
			.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
			.leftOuterJoin("venda.vendasituacao vendasituacao")
			.leftOuterJoin("venda.listavendamaterial vendamaterial")
			.leftOuterJoin("vendamaterial.material material")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.unidademedida unidademedidamaterial")
			.leftOuterJoin("vendamaterial.unidademedida unidademedida")
			.leftOuterJoin("vendamaterial.materialmestre materialmestre")
			.leftOuterJoin("materialmestre.materialgrupo materialgrupomestre")
			.leftOuterJoin("materialmestre.unidademedida unidademedidamestre")
			.leftOuterJoin("venda.colaborador colaborador")
			.leftOuterJoin("venda.cliente cliente")
			.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
			.where("venda.dtvenda >= ?", filtro.getDtinicio())
			.where("venda.dtvenda <= ?", filtro.getDtfim())
			.where("empresa = ?", filtro.getEmpresa())
			.openParentheses()
				.where("pedidovendatipo is null")
				.or()
				.where("pedidovendatipo.bonificacao <> true")
			.closeParentheses()
			.where("vendasituacao.cdvendasituacao <> ? ", Vendasituacao.CANCELADA.getCdvendasituacao())
			.where("vendasituacao.cdvendasituacao <> ? ", Vendasituacao.AGUARDANDO_APROVACAO.getCdvendasituacao())
			.ignoreJoin("listaClientevendedor");
		
		if(filtro.getDocumentotipo() != null){
			query
				.leftOuterJoin("venda.documentotipo documentotipo")
				.where("documentotipo = ?", filtro.getDocumentotipo());
		}
		if(filtro.getCliente() != null){
			query
				.where("cliente = ?", filtro.getCliente());
		}
		if(filtro.getColaborador() != null){
			if (filtro.getIsVendedorPrincipal() != null && filtro.getIsVendedorPrincipal()) {
				query
					.openParentheses()
						.where("listaClientevendedor.colaborador = ?", filtro.getColaborador())
						.where("listaClientevendedor.principal = ?", Boolean.TRUE)
					.closeParentheses();
			} else {
				query.where("colaborador = ?", filtro.getColaborador());
			}
		}
		
		adicionaJoinWhereForMaterial(filtro, query);
		
		return query.list();				
	}
	
	private void adicionaJoinWhereForMaterial(ApuracaovendaFiltro filtro, QueryBuilder<Venda> query) {
		if(filtro.getMaterialgrupo() != null || filtro.getMaterialcategoria() != null){
			
			StringBuilder selectForMaterial = new StringBuilder();
			String whereMaterialgrupo = "";
			String whereMaterialcategoria = "";
			
			if(filtro.getMaterialcategoria()!=null){
				selectForMaterial.append(",materialcategoria.cdmaterialcategoria, vmaterialcategoria.identificador ");
				query.leftOuterJoinIfNotExists("material.materialcategoria materialcategoria");
				query.leftOuterJoinIfNotExists("materialcategoria.vmaterialcategoria vmaterialcategoria");
				materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
				// query.whereIn("materialcategoria.cdmaterialcategoria", CollectionsUtil.listAndConcatenate(materialcategoriaService.findForFitlro(filtro.getMaterialcategoria().getIdentificador()), "cdmaterialcategoria", ","));
				whereMaterialcategoria += " AND m.materialcategoria.cdmaterialcategoria in (" + CollectionsUtil.listAndConcatenate(materialcategoriaService.findForFitlro(filtro.getMaterialcategoria().getIdentificador()), "cdmaterialcategoria", ",") + ")";
			}
			if(filtro.getMaterialgrupo()!=null){
				whereMaterialgrupo += " AND m.materialgrupo.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo() + " ";
			}
			
			query.select(query.getSelect().getValue() + selectForMaterial.toString());
			query.where(" exists (	  select vm.venda.cdvenda " +
								 	" from Vendamaterial vm " +
								 	" join vm.material m " +
								 	" where vm.venda.cdvenda = venda.cdvenda " +
								 	whereMaterialgrupo + 
								 	whereMaterialcategoria +" ) ");
		}
	}

	/**
	 * 
	 * @param param
	 * @return
	 */
	public  List<Venda> findVendaForAutocomplete(String param) {
		return query()
			.select("venda.cdvenda, venda.dtvenda, venda.identificador, cliente.nome")
			.join("venda.cliente cliente")
			.join("venda.listavendamaterial listavendamaterial")
			.openParentheses()
				.whereLikeIgnoreAll("venda.cdvenda",param)
				.or()
				.whereLikeIgnoreAll("cliente.nome",param)
			.closeParentheses()
			.list();
	}
	
	/**
	 * M�todo que busca a venda com a nota
	 *
	 * @param param
	 * @return
	 * @author Luiz Fernando
	 * @since 08/04/2014
	 */
	public  List<Venda> findVendaWithNotaForAutocomplete(String param, String cdempresa) {
		return query()
			.select("venda.cdvenda, venda.dtvenda, venda.identificador, cliente.nome, listaNotavenda.cdnotavenda, nota.numero, notaStatus.cdNotaStatus ")
			.join("venda.cliente cliente")
			.join("venda.listavendamaterial listavendamaterial")
			.join("venda.empresa empresa")
			.leftOuterJoin("venda.listaNotavenda listaNotavenda")
			.leftOuterJoin("listaNotavenda.nota nota")
			.leftOuterJoin("nota.notaStatus notaStatus")
			.openParentheses()
				.whereLikeIgnoreAll("venda.cdvenda",param)
				.or()
				.whereLikeIgnoreAll("cliente.nome",param)
				.or()
				.whereLikeIgnoreAll("venda.identificador",param)
				.or()
				.whereLikeIgnoreAll("venda.identificadorexterno",param)
			.closeParentheses()
			.whereIn("empresa.cdpessoa", cdempresa)
			.setMaxResults(100)
			.list();
	}

	/**
	 * 
	 * @param venda
	 * @return
	 */
	public Venda loadVendaForExpedicao(Venda venda) {		
		return query()
			.select(" venda.cdvenda, venda.dtvenda, cliente.cdpessoa, cliente.nome, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
					" contato.cdpessoa, contato.nome ")
			.join("venda.cliente cliente")
			.leftOuterJoin("venda.contato contato")
			.leftOuterJoin("venda.localarmazenagem localarmazenagem")
			.where("venda = ?",venda)
			.unique();
	}
	
	/**
	 * M�todo que busca as vendas da agenda de produ��o
	 *
	 * @param producaoagenda
	 * @return
	 * @author Luiz Fernando
	 * @since 31/01/2014
	 */
	public List<Venda> findByProducaoagenda(Producaoagenda producaoagenda) {
		if(producaoagenda == null || producaoagenda.getCdproducaoagenda() == null)
			throw new SinedException("Agenda de Produ��o n�o pode ser nula");
		
		return query()
				.select("venda.cdvenda, vendasituacao.cdvendasituacao")
				.join("venda.vendasituacao vendasituacao")
				.where("exists (select v.cdvenda " +
						"		from Producaoagenda pa " +
						"		join pa.venda v " +
						"		where v.cdvenda = venda.cdvenda and pa.cdproducaoagenda = " + producaoagenda.getCdproducaoagenda() + ") ")
				.list();
	}
	
	public List<Venda> findByProducaoagenda(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Agenda(s) de Produ��o(�es) n�o pode(m) ser nula(s)");
		
		return query()
				.select("venda.cdvenda, vendasituacao.cdvendasituacao")
				.join("venda.vendasituacao vendasituacao")
				.where("exists (select v.cdvenda " +
						"		from Producaoagenda pa " +
						"		join pa.venda v " +
						"		where v.cdvenda = venda.cdvenda and pa.cdproducaoagenda in (" + whereIn + ") ) ")
				.list();
	}

	/**
	 * Busca as vendas referentes �s expedi��es passadas via par�metros.
	 *
	 * @param whereInExpedicoes
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/02/2014
	 */
	public List<Venda> findByExpedicoes(String whereInExpedicoes) {
		return query()
					.select("venda.cdvenda, venda.dtvenda, expedicao.cdexpedicao, expedicao.expedicaosituacao")
					.join("venda.listavendamaterial vendamaterial")
					.join("vendamaterial.listaExpedicaoitem expedicaoitem")
					.join("expedicaoitem.expedicao expedicao")
					.whereIn("expedicao.cdexpedicao", whereInExpedicoes)
					.list();
	}

	public Venda loadWithVendasituacao(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nula.");
		
		return query()
			.select("venda.cdvenda, vendasituacao.cdvendasituacao")
			.leftOuterJoin("venda.vendasituacao vendasituacao")
			.where("venda = ?", venda)
			.unique();
	}

	public boolean existsProducaoagenda(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nula.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Venda.class)
			.where("venda = ?", venda)
			.where("exists (select v.cdvenda from Producaoagenda pa join pa.venda v where v.cdvenda = venda.cdvenda ) ")
			.unique() > 0;
	}
	
	/**
	 * Verifica se existe a venda da empresa com o identificador
	 *
	 * @param identificador
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/02/2014
	 */
	public boolean haveVendaByIdentificadorEmpresa(String identificador, Empresa empresa, boolean verificarCancelado) {
		return newQueryBuilderSinedWithFrom(Long.class)
					.setUseReadOnly(false)
					.select("count(*)")
					.where("venda.identificador = ?", identificador)
					.where("venda.empresa = ?", empresa)
					.where("venda.vendasituacao <> ? ", Vendasituacao.CANCELADA, !verificarCancelado)
					.unique() > 0;
	}

	/**
	 * Busca os dados do relat�rio de vendas por forma de pagamento
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/04/2014
	 */
	@SuppressWarnings("unchecked")
	public List<VendaFormapagamentoBean> findForRelatorioVendaFormapagamento(VendaFormapagamentoFiltro filtro) {
		String sql = "select v.dtvenda as dtvenda, " +
						"dt.cddocumentotipo as documentotipo_cddocumentotipo, " +
						"dt.nome as documentotipo_nome, " +
						"dt.percentual as documentotipo_percentual, " +
						"dt.taxavenda as documentotipo_taxavenda, " +
						"dt.diasreceber as documentotipo_diasreceber, " +
						"c.cdconta as contadestino_cdconta, " +
						"c.nome as contadestino_nome, " +
						"sum(vp.valororiginal)/100.0 as valorbruto " +
						
						"from vendapagamento vp " +
						
						"join venda v on v.cdvenda = vp.cdvenda " +
						"join documentotipo dt on dt.cddocumentotipo = vp.cdformapagamento " +
						"left outer join conta c on dt.cdcontadestino = c.cdconta " +
						
						"where v.cdvendasituacao <> 3 " +
						"and v.cdvendasituacao <> 4  " +
						"and v.cdvendasituacao <> 6 " +
						
						(filtro.getEmpresa() != null ? "and v.cdempresa = " + filtro.getEmpresa().getCdpessoa() + " " : "") +
						(filtro.getDtvenda1() != null ? "and v.dtvenda >= '" + SinedDateUtils.toStringPostgre(filtro.getDtvenda1()) + "' " : "") +
						(filtro.getDtvenda2() != null ? "and v.dtvenda <= '" + SinedDateUtils.toStringPostgre(filtro.getDtvenda2()) + "' " : "");
		
		if(filtro.getListaDocumentotipo() != null && filtro.getListaDocumentotipo().size() > 0){
			sql += "and ( ";
			for (Documentotipo documentotipo : filtro.getListaDocumentotipo()) {
				sql += "vp.cdformapagamento = " + documentotipo.getCddocumentotipo() + " or ";
			}
			sql += "0 = 1) ";
		}
		sql += "group by v.dtvenda, dt.cddocumentotipo, c.cdconta order by 1";

		SinedUtil.markAsReader();
		List<VendaFormapagamentoBean> lista = getJdbcTemplate().query(sql, 
			new RowMapper() {
				public VendaFormapagamentoBean mapRow(ResultSet rs, int rowNum) throws SQLException {
					VendaFormapagamentoBean bean = new VendaFormapagamentoBean();
					
					Integer contadestino_cdconta = rs.getObject("contadestino_cdconta") != null ? rs.getInt("contadestino_cdconta") : null;
					String contadestino_nome = rs.getString("contadestino_nome");
					Conta conta = null;
					if(contadestino_cdconta != null){
						conta = new Conta(contadestino_cdconta, contadestino_nome);
					}
					
					Integer documentotipo_cddocumentotipo = rs.getInt("documentotipo_cddocumentotipo");
					String documentotipo_nome = rs.getString("documentotipo_nome");
					Documentotipo documentotipo = new Documentotipo(documentotipo_cddocumentotipo, documentotipo_nome);
					documentotipo.setContadestino(conta);
					
					Boolean documentotipo_percentual = rs.getBoolean("documentotipo_percentual");
					Double documentotipo_taxavenda = rs.getDouble("documentotipo_taxavenda");
					
					Integer documentotipo_diasreceber = rs.getInt("documentotipo_diasreceber");
					if(documentotipo_diasreceber == null){
						documentotipo_diasreceber = 0;
					}
					
					Date dtvenda = rs.getDate("dtvenda");
					Double valorbruto = rs.getDouble("valorbruto");
					Double taxaadm = 0d;
					if(documentotipo_taxavenda != null && documentotipo_taxavenda > 0){
						if(documentotipo_percentual != null && documentotipo_percentual){
							taxaadm = valorbruto * (documentotipo_taxavenda / 100d);
						} else {
							taxaadm = documentotipo_taxavenda;
						}
					}
					
					bean.setDtprevisto(documentotipo_diasreceber > 0 ? SinedDateUtils.incrementaDia(dtvenda, documentotipo_diasreceber, null) : dtvenda);
					bean.setDtlancamento(dtvenda);
					bean.setDocumentotipo(documentotipo);
					bean.setValorbruto(valorbruto);
					bean.setTaxaadm(taxaadm);
					bean.setValorliquido(valorbruto - taxaadm);
					
					return bean;
				}
			}
		);
		
		return lista;
	}

	public boolean isVendaIndustrializacaoRetorno(String whereIn){
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.join("venda.pedidovenda pedidovenda")
				.join("pedidovenda.pedidovendatipo pedidovendatipo")
				.where("pedidovendatipo.gerarNotaIndustrializacaoRetorno=?", Boolean.TRUE)
				.whereIn("venda.cdvenda", whereIn)
				.unique()>0L;				
	}
	
	/**
	 * M�todo que carrega as vendas para a listagem de comissionamento por desempenho
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 27/05/2014
	 */
	public List<Venda> findForListagemComissionamentoDesempenho(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			return null;
		
		QueryBuilder<Venda> query = query()
			.select("venda.cdvenda, venda.valorfrete, venda.taxapedidovenda, venda.desconto, venda.taxapedidovenda, " +
					"vendamaterial.cdvendamaterial, vendamaterial.quantidade, vendamaterial.multiplicador, venda.dtvenda, " +
					"vendamaterial.preco, vendamaterial.desconto, venda.valorusadovalecompra," +
					"documento.cddocumento, documentoacao.cddocumentoacao ")
			.leftOuterJoin("venda.listavendamaterial vendamaterial")
			.leftOuterJoin("venda.listavendapagamento vendapagamento")
			.leftOuterJoin("vendapagamento.documento documento")
			.leftOuterJoin("documento.documentoacao documentoacao");
		
		SinedUtil.quebraWhereIn("venda.cdvenda", whereIn, query);
			
		return query.list();
	}
	
	/**
	* M�todo que carrega a venda com o or�amento
	*
	* @param venda
	* @return
	* @since 13/08/2014
	* @author Luiz Fernando
	*/
	public Venda loadWithOrcamento(Venda venda) {
		if(venda == null|| venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nula.");
		
		return query()
				.select("venda.cdvenda, vendaorcamento.cdvendaorcamento, vendaorcamentosituacao.cdvendaorcamentosituacao ")
				.leftOuterJoin("venda.vendaorcamento vendaorcamento")
				.leftOuterJoin("vendaorcamento.vendaorcamentosituacao vendaorcamentosituacao")
				.where("venda = ?", venda)
				.unique();
	}

	/**
	* M�todo que verifica se o identificador j� est� cadastrado
	*
	* @param identificador
	* @return boolean
	* @author Jo�o Vitor
	* @since 01/12/2014
	*/
	public Boolean isIdentificadorCadastrado(Venda venda) {
		if(venda == null){
			return false;
		}
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
		String dataVendaAtual = null;
		
		if (venda.getDtvenda() != null) {
			dataVendaAtual = simpleDateFormat.format(venda.getDtvenda());
		}
		
		return query()
		.select("venda.identificador")
		.leftOuterJoin("venda.empresa empresa")
		.where("venda.identificador = ?", venda.getIdentificador())
		.where("venda.identificador <> ''")
		.where("venda.identificador IS NOT NULL")
		.where("venda.cdvenda <> ?", venda.getCdvenda())
		.where("EXTRACT(YEAR FROM date(venda.dtvenda)) = ?", Integer.parseInt(dataVendaAtual))
		.where("empresa = ?", venda.getEmpresa())
		.unique() != null;
	}
	
	/**
	* M�todo que retorna o valor j� usado de vale compra do pedido de venda
	*
	* @param pedidovenda
	* @return
	* @since 12/01/2015
	* @author Luiz Fernando
	*/
	public Money getValorValeCompraJaUsado(Pedidovenda pedidovenda){
		Long total = null;
		if(pedidovenda != null && pedidovenda.getCdpedidovenda() != null){
			total =  newQueryBuilder(Long.class)
				.select("SUM(venda.valorusadovalecompra)")
				.setUseTranslator(false)
				.from(Venda.class)
				.join("venda.pedidovenda pedidovenda")
				.where("venda.vendasituacao <> ?", Vendasituacao.CANCELADA)
				.where("pedidovenda = ?", pedidovenda)
				.where("venda.valorusadovalecompra is not null")
				.where("venda.valorusadovalecompra > 0")
				.unique();
		}
		
		return total != null ? new Money(total, true) : new Money();
	}

	public boolean existeVendaNotIndustrializacaoRetorno(String whereInVenda) {
		if(StringUtils.isEmpty(whereInVenda))
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Venda.class)
			.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
			.whereIn("venda.cdvenda", whereInVenda)
			.openParentheses()
				.where("pedidovendatipo is null")
				.or()
				.where("referenciarnotaentradananotasaida is null")
				.or()
				.where("referenciarnotaentradananotasaida = false")
			.closeParentheses()
			.unique() > 0;
				
	}

/**
	* M�todo que verifica se existe venda sem pagamentos ou sem documentos
	*
	* @param whereIn
	* @return
	* @since 25/02/2015
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public boolean existeVendaSemDocumento(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		String sql = "select v.cdvenda AS CDVENDA, count(vp.cdvendapagamento) AS QTDE_VENDAPAGAMENTO, " +
				"count(vp.cddocumento) AS QTDE_DOCUMENTO " +
				"from venda v " +
				"left outer join vendapagamento vp on vp.cdvenda = v.cdvenda " +
				"where v.cdvenda in (" + whereIn + ")" +
				"group by v.cdvenda ";

		SinedUtil.markAsReader();
		List<Venda> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Venda venda = new Venda(rs.getInt("CDVENDA"));
				venda.setQtdevendapagamento(rs.getInt("QTDE_VENDAPAGAMENTO"));
				venda.setQtdedocumento(rs.getInt("QTDE_DOCUMENTO"));
				return venda;
			}
		});
		
		if(list == null || list.isEmpty())
			return true;
		
		boolean vendaSemDocumento = false; 
		for(Venda venda : list){
			if(venda.getQtdevendapagamento() == null || venda.getQtdevendapagamento() == 0){
				vendaSemDocumento = true;
				break;
			}
			if(venda.getQtdedocumento() == null || venda.getQtdedocumento() == 0){
				vendaSemDocumento = true;
				break;
			}
		}
		return vendaSemDocumento;
	}
	
	/**
	* M�todo que verifica se existe venda de representa��o
	*
	* @param whereIn
	* @return
	* @since 13/02/2015
	* @author Luiz Fernando
	*/
	public boolean existeVendaRepresentacao(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Venda.class)
			.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
			.whereIn("venda.cdvenda", whereIn)
			.where("pedidovendatipo.representacao = true")
			.unique() > 0;
	}
	
	/**
	* M�todo que carrega a venda com o local
	*
	* @param venda
	* @return
	* @since 27/02/2015
	* @author Luiz Fernando
	*/
	public Venda loadWithLocalAndProjeto(Venda venda){
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("venda.cdvenda, localarmazenagem.cdlocalarmazenagem, projeto.cdprojeto ")
			.leftOuterJoin("venda.localarmazenagem localarmazenagem")
			.leftOuterJoin("venda.projeto projeto")
			.where("venda = ?", venda)
			.unique();
	}

	/**
	* M�todo que retorna a quantidade vendida de material de produ��o
	*
	* @param filtro
	* @return
	* @since 18/05/2015
	* @author Luiz Fernando
	*/
	public Double getQtdeProducao(CustooperacionalFiltro filtro) {
		if(filtro == null || filtro.getEmpresa() == null || filtro.getDtinicio() == null || filtro.getDtfim() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		Double valor = newQueryBuilder(Double.class)
				.select("sum(coalesce(listavendamaterial.quantidade, 0)*coalesce(materialgrupo.rateiocustoproducao, 1))")
				.setUseTranslator(false)
				.from(Venda.class)
				.join("venda.listavendamaterial listavendamaterial")
				.join("listavendamaterial.material material")
				.join("material.materialgrupo materialgrupo")
				.where("venda.empresa = ?", filtro.getEmpresa())
				.where("venda.dtvenda >= ?", filtro.getDtinicio())
				.where("venda.dtvenda <= ?", filtro.getDtfim())
				.where("venda.vendasituacao <> ?", Vendasituacao.CANCELADA)
				.where("venda.vendasituacao <> ?", Vendasituacao.AGUARDANDO_APROVACAO)
				.where("listavendamaterial.material.producao = true")
				.unique();
		
		return valor;
	}
	
	/**
	* M�todo que retorna o valor vendido de material de produ��o
	*
	* @param filtro
	* @return
	* @since 21/05/2015
	* @author Luiz Fernando
	*/
	public Double getValorProducao(CustooperacionalFiltro filtro) {
		if(filtro == null || filtro.getEmpresa() == null || filtro.getDtinicio() == null || filtro.getDtfim() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		
		String sql = 
				" select sum(query.totalvenda) as total " +
				" from ( " +
				" select ( " +
				" (select sum((coalesce(vm.quantidade, 0) * coalesce(vm.preco, 0) * coalesce(vm.multiplicador, 1)) - coalesce(vm.desconto, 0)/100) " +
				" from vendamaterial vm " +
				" where vm.cdvenda = v.cdvenda " +
				" ) " +
				" + coalesce(v.valorfrete, 0)/100 " +
				" - coalesce(v.desconto, 0)/100 " +
				" - coalesce(v.valorusadovalecompra, 0)/100 " +
				" ) as totalvenda " +
				" from venda v " +
				" where v.dtvenda >= to_date('"+ format.format(filtro.getDtinicio())+ "', 'DD/MM/YYYY') " +
				" and v.dtvenda <= to_date('"+ format.format(filtro.getDtfim())+ "', 'DD/MM/YYYY') " +
				" and v.cdempresa = " + filtro.getEmpresa().getCdpessoa() +
				" and v.cdvendasituacao not in ( " + Vendasituacao.CANCELADA.getCdvendasituacao() + "," + 
													 Vendasituacao.AGUARDANDO_APROVACAO.getCdvendasituacao() + ") " + 
				" and exists (select vmaterial.cdvenda from vendamaterial vmaterial " +
				" join material m on m.cdmaterial = vmaterial.cdmaterial and m.producao = true " +
				" where vmaterial.cdvenda = v.cdvenda ) " +
				" ) query ";

		SinedUtil.markAsReader();
		BigDecimal valor = (BigDecimal)getJdbcTemplate().queryForObject(sql, 
				new RowMapper() {
					public BigDecimal mapRow(ResultSet rs, int rowNum) throws SQLException {
						return new BigDecimal(rs.getDouble("total"));
					}
				});
		
		return valor != null ? valor.doubleValue() : 0d;
	}

	/**
	 * Busca as vendas vinculadas ao pneu
	 *
	 * @param pneu
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/07/2015
	 */
	public List<Venda> findByPneu(Pneu pneu) {
		return query()
				.select("venda.cdvenda, venda.dtvenda")
				.join("venda.listavendamaterial listavendamaterial")
				.where("listavendamaterial.pneu = ?", pneu)
				.list();
	}
	
	/**
	* M�todo que busca as vendas de acordo com o identificador e empresa
	*
	* @param empresa
	* @param identificador
	* @return
	* @since 06/10/2015
	* @author Luiz Fernando
	*/
	public List<Venda> findByEmpresaIdentificador(Empresa empresa, String identificador) {
		if(StringUtils.isBlank(identificador)){
			return null;
		}
		return query()
				.select("venda.cdvenda, venda.identificador, venda.dtvenda, " +
						"cliente.cdpessoa, cliente.nome, cliente.identificador, " +
						"vendasituacao.cdvendasituacao ")
				.join("venda.cliente cliente")
				.join("venda.vendasituacao vendasituacao")
				.where("venda.empresa = ?", empresa)
				.where("venda.identificador = ?", identificador)
				.where("venda.vendasituacao <> ? ", Vendasituacao.CANCELADA)
				.list();
	}

	/**
	* M�todo que faz o update de cria��o finalizada na venda
	*
	* @param venda
	* @since 01/02/2016
	* @author Luiz Fernando
	*/
	public void updateCriacaofinalizada(Venda venda) {
		if(venda != null && venda.getCdvenda() != null){
			getHibernateTemplate().bulkUpdate("update Venda v set v.criacaofinalizada = true where v.cdvenda = " + venda.getCdvenda());
		}
	}

	/**
	* M�todo que verifica se a cra��o da venda foi finalizada
	*
	* @param whereIn
	* @return
	* @since 01/02/2016
	* @author Luiz Fernando
	*/
	public Boolean existeVendaComCriacaoNaoFinalizada(String whereIn) {
		if(StringUtils.isBlank(whereIn)){
			return false;
		}
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Venda.class)
			.whereIn("venda.cdvenda", whereIn)
			.where("coalesce(venda.criacaofinalizada, true) = false")
			.unique() > 0;
	}

	/**
	 * Busca a venda para a integra��o FLEX
	 * @param vendaFlex
	 * @return
	 */
	public Venda findForVendaSincronizacaoFlex(VendaSincronizacaoFlex vendaFlex) {
		if(vendaFlex == null || vendaFlex.getVenda() == null){
			return null;
		}
		
		return query()
				.select("venda.cdvenda, endereco.cdendereco, endereco.logradouro, endereco.bairro, endereco.numero, endereco.complemento, endereco.caixapostal, " +
						"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla, pais.cdpais, pais.nome, projeto.cdprojeto, projeto.nome, projeto.dtprojeto, " +
						"cliente.cdpessoa, cliente.nome, cliente.dtnascimento, cliente.pai, cliente.mae, cliente.cpf, cliente.rg, estadocivil.cdestadocivil, estadocivil.nome, sexo.cdsexo, sexo.nome, " +
						"sexo.cdsexo, cliente.orgaoemissorrg, listaTelefone.cdtelefone, listaTelefone.telefone, telefonetipo.cdtelefonetipo, telefonetipo.nome, cliente.email, municipionaturalidade.nome, cliente.nacionalidade, " + 
				        "material.cdmaterial, material.nome, material.identificacao")
				.leftOuterJoin("venda.endereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("endereco.pais pais")
				.leftOuterJoin("venda.projeto projeto")
				.leftOuterJoin("venda.cliente cliente")
				.leftOuterJoin("cliente.estadocivil estadocivil")
				.leftOuterJoin("cliente.sexo sexo")
				.leftOuterJoin("cliente.municipionaturalidade municipionaturalidade")
				.leftOuterJoin("cliente.listaTelefone listaTelefone")
				.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
				.leftOuterJoin("venda.listavendamaterial listavendamaterial")
				.leftOuterJoin("listavendamaterial.material material")
				.where("venda = ?", vendaFlex.getVenda())
				.unique();
	}
	
	/**
	 * Busca as venda de um determinado pedido de venda com o identificador de integra�o no item.
	 *
	 * @param pedidovenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/05/2016
	 */
	public List<Venda> findByPedidovendaWSSOAP(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Pedido de venda n�o pode ser nulo.");
		
		return query()
				.select("venda.cdvenda, vendamaterial.cdvendamaterial, vendamaterial.identificadorintegracao, vendasituacao.cdvendasituacao")
				.leftOuterJoin("venda.vendasituacao vendasituacao")
				.leftOuterJoin("venda.listavendamaterial vendamaterial")
				.where("venda.pedidovenda = ?", pedidovenda)
				.where("venda.vendasituacao <> ?", Vendasituacao.CANCELADA)
				.list();
	}
	
	/**
	* M�todo que carrega a venda para validar aprova��o
	*
	* @param venda
	* @return
	* @since 26/07/2016
	* @author Luiz Fernando
	*/
	public Venda loadValidarAprovacao(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nulo.");
		
		return query()
				.select("venda.cdvenda, venda.dtestorno, venda.desconto, " +
						"vendasituacao.cdvendasituacao, " +
						"cliente.cdpessoa, cliente.nome, " +
						"pedidovenda.cdpedidovenda, " +
						"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.validarestoquepedidonaaprovacao, pedidovendatipo.permitirvendasemestoque, " +
						"localarmazenagem.cdlocalarmazenagem, " +
						"empresa.cdpessoa, " +
						"projeto.cdprojeto, " +
						"listavendamaterial.cdvendamaterial, listavendamaterial.preco, listavendamaterial.desconto, listavendamaterial.quantidade, " +
						"material.cdmaterial, material.valorcusto")
				.join("venda.vendasituacao vendasituacao")
				.join("venda.cliente cliente")
				.leftOuterJoin("venda.pedidovenda pedidovenda")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("venda.localarmazenagem localarmazenagem")
				.leftOuterJoin("venda.empresa empresa")
				.leftOuterJoin("venda.projeto projeto")
				.leftOuterJoin("venda.listavendamaterial listavendamaterial")
				.leftOuterJoin("listavendamaterial.material material")
				.where("venda  = ?", venda)
				.unique();
	}
	
	/**
	* M�todo que carrega a venda com as informa��es para o template
	*
	* @param venda
	* @return
	* @since 21/09/2016
	* @author Luiz Fernando
	*/
	public Venda loadForTemplate(Venda venda){
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()	
				.select("venda.cdvenda, venda.dtvenda, venda.dtestorno, venda.valorfrete, venda.desconto, venda.identificadorexterno, venda.identificador, " +
						"venda.vendasituacao, venda.observacao, venda.observacaointerna, venda.origemwebservice, venda.dtaltera, " +
						"cliente.cdpessoa, cliente.razaosocial, cliente.inscricaoestadual, " +
						"contato.nome, contato.cdpessoa, " +
						"colaborador.cdpessoa, colaborador.nome, colaborador.email, " +
						"frete.cdResponsavelFrete, frete.nome, terceiro.nome, " +
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.inscricaoestadual, empresa.email, empresa.site, " +
						"empresa.observacaovenda, " +
						"contaboleto.cdconta, contaboleto.taxaboleto,  prazopagamento.nome, " +
						"endereco.logradouro, endereco.numero, endereco.bairro, endereco.cep, " +
						"endereco.complemento, municipio.nome, municipio.uf, documentotipo.nome, " +
						"vendaorcamento.cdvendaorcamento, vendaorcamentopedido.cdvendaorcamento, pedidovenda.cdpedidovenda, " +
						"projeto.cdprojeto, projeto.nome, pedidovendatipo.descricao, " +
						"templatenotaservico.cdreporttemplate, templatenotaservico.leiaute ")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("pedidovendatipo.templatenotaservico templatenotaservico")
				.leftOuterJoin("venda.cliente cliente")
				.leftOuterJoin("venda.contato contato")
				.leftOuterJoin("venda.colaborador colaborador")
				.leftOuterJoin("venda.terceiro terceiro")
				.leftOuterJoin("venda.frete frete")
				.leftOuterJoin("venda.vendasituacao vendasituacao")
				.leftOuterJoin("venda.empresa empresa")
				.leftOuterJoin("empresa.logomarca logomarca")
				.leftOuterJoin("venda.contaboleto contaboleto")
				.leftOuterJoin("venda.prazopagamento prazopagamento")
				.leftOuterJoin("venda.documentotipo documentotipo")
				.leftOuterJoin("venda.endereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("venda.vendaorcamento vendaorcamento")
				.leftOuterJoin("venda.pedidovenda pedidovenda")
				.leftOuterJoin("pedidovenda.vendaorcamento vendaorcamentopedido")
				.leftOuterJoin("venda.projeto projeto")
				.where("venda = ?", venda)
				.unique();
	}
	
	public List<Venda> findByFeedApplication(Empresa empresa, Cliente cliente) {
		if (cliente == null || cliente.getCdpessoa() == null){
			return null;
		}
		return query()
				.select("venda.cdvenda, venda.identificador, venda.dtvenda, cliente.nome, vendasituacao.cdvendasituacao, " +
						"listavendamaterial.observacao, listavendapagamento.cdvendapagamento ")
				.join("venda.empresa empresa")
				.join("venda.cliente cliente")
				.join("venda.listavendamaterial listavendamaterial")
				.join("venda.vendasituacao vendasituacao")
				.leftOuterJoin("venda.listavendapagamento listavendapagamento")
				.leftOuterJoin("venda.prazopagamento prazopagamento")
				.leftOuterJoin("prazopagamento.listaPagamentoItem listaPagamentoItem")
				.where("empresa=?", empresa)
				.where("cliente=?", cliente)
				.where("venda.vendasituacao <> ?", Vendasituacao.CANCELADA)
				.where("not exists (select 1 from NotaVenda nv join nv.venda v where v=venda)")
				.where("not exists (select 1 from Documentoorigem do join do.venda v where v=venda)")
				.orderBy("venda.dtvenda desc")
				.list();
	}

	/**
	 * M�todo que retorna os campos necess�rios para emiss�o do relat�rio de etiqueta de destinat�rio e remetente 
	 * @param whereIn
	 * @return
	 * @since 06/04/2017
	 * @author Mairon
	 */
	public List<Venda> findForEtiquetaDestinatarioRemetente(String whereIn){
		return query().select("venda.cdvenda, cliente.nome, cliente.razaosocial, cliente.cpf, cliente.cnpj, endcli.numero, "+
							  "endcli.complemento, endcli.logradouro, endcli.bairro, municipiocli.nome, "+
							  "estadocli.sigla, endcli.cep, "+
							  "empresa.nome, empresa.razaosocial, empresa.cpf, empresa.cnpj, enderecoemp.numero, "+
							  "enderecoemp.complemento, enderecoemp.logradouro, enderecoemp.bairro, municipioemp.nome, "+
							  "estadoemp.sigla, enderecoemp.cep")
		.join("venda.cliente cliente")
		.join("venda.empresa empresa")
		.leftOuterJoin("cliente.listaEndereco endcli")
		.leftOuterJoin("endcli.municipio municipiocli")
		.leftOuterJoin("municipiocli.uf estadocli")
		.leftOuterJoin("empresa.listaEndereco enderecoemp")
		.leftOuterJoin("enderecoemp.municipio municipioemp")
		.leftOuterJoin("municipioemp.uf estadoemp")
		.whereIn("venda.cdvenda", whereIn)
		.list();
	}
	
	/**
	 * M�todo que retorna as vendas vinculadas a uma nota
	 *
	 * @param param
	 * @return
	 * @author Mairon Cezar
	 * @since 13/04/2017
	 */
	public  List<Venda> findVendasByNota(Integer cdNota) {
		if(cdNota==null){
			return new ArrayList<Venda>();
		}
		
		return query()
			.select("venda.cdvenda, venda.dtvenda, venda.identificador, cliente.nome, listaNotavenda.cdnotavenda, nota.numero ")
			.join("venda.cliente cliente")
			.join("venda.listavendamaterial listavendamaterial")
			.join("venda.listaNotavenda listaNotavenda")
			.join("listaNotavenda.nota nota")
			.where("nota.cdNota = ?", cdNota)
			.list();
	}

	/**
	 * M�todo que retorna as vendas vinculadas �s notas passadas como par�metro no whereIn, onde o whereIn passado
	 * se refere ao campo numero da classe nota
	 *
	 * @param whereIn
	 * @return
	 * @author Mairon Cezar
	 * @since 13/04/2017
	 */
	public  List<Venda> findVendaWithNotaForExpedicao(String whereIn, String cdempresa) {
		return query()
			.select("venda.cdvenda, nota.numero, cliente.nome")
			.join("venda.cliente cliente")
			.join("venda.listaNotavenda listaNotavenda")
			.join("venda.empresa empresa")
			.join("listaNotavenda.nota nota")
			.whereIn("nota.numero", SinedUtil.montaWhereInStringVersaoNova(whereIn, ","))
			.whereIn("empresa.cdpessoa", cdempresa)
			.list();
	}

	/**
	 * M�todo que retorna as vendas vinculadas determinadas expedi��es. Ignora as vendas que tiveram os seus itens exclu�dos ou zerados na expedi��o.
	 *
	 * @param cdexpedicao
	 * @param venda
	 * @return
	 * @author Mairon Cezar
	 * @since 13/04/2017
	 */	
	public List<Venda> findByExpedicoesComItens(Integer cdexpedicao, Venda venda) {
		return query()
					.select("venda.cdvenda, venda.dtvenda")
					.join("venda.listavendamaterial vendamaterial")
					.join("vendamaterial.listaExpedicaoitem expedicaoitem")
					.join("expedicaoitem.expedicao expedicao")
					.where("expedicao.cdexpedicao = ?", cdexpedicao)
					.where("venda = ?", venda)
					.where("coalesce(expedicaoitem.qtdeexpedicao, 0) > 0")
					.list();
	}
	
	public Venda findForRateio(Venda venda) {
		return query()
			.select("venda.cdvenda, empresa.cdpessoa, contagerencial.cdcontagerencial, centrocusto.cdcentrocusto, centrocustoVenda.cdcentrocusto, " +
					"vendamaterial.cdvendamaterial, vendamaterial.quantidade, vendamaterial.preco, vendamaterial.desconto, vendamaterial.multiplicador, " +
					"material.cdmaterial, material.servico, material.tributacaomunicipal, contagerencialvenda.cdcontagerencial, centrocustovenda.cdcentrocusto, " +
					"materialmestre.cdmaterial, materialmestre.servico, materialmestre.tributacaomunicipal, contagerencialvendaMestre.cdcontagerencial, centrocustovendaMestre.cdcentrocusto ")
			.join("venda.empresa empresa")
			.leftOuterJoin("empresa.contagerencial contagerencial")
			.leftOuterJoin("empresa.centrocusto centrocusto")
			.join("venda.listavendamaterial vendamaterial")
			.join("vendamaterial.material material")
			.leftOuterJoin("venda.centrocusto centrocustoVenda")
			.leftOuterJoin("material.contagerencialvenda contagerencialvenda")
			.leftOuterJoin("material.centrocustovenda centrocustovenda")
			.leftOuterJoin("vendamaterial.materialmestre materialmestre")
			.leftOuterJoin("materialmestre.contagerencialvenda contagerencialvendaMestre")
			.leftOuterJoin("materialmestre.centrocustovenda centrocustovendaMestre")
			.where("venda = ?", venda)
			.unique();
	}
	
	public Venda findByVendamaterial(Vendamaterial vendamaterial){
		return query()
			.select("venda.cdvenda")
			.join("venda.listavendamaterial vendamaterial")
			.where("vendamaterial.cdvendamaterial = ?", vendamaterial.getCdvendamaterial())
			.unique();
	}
	
	public List<Venda> findByVendamaterial(String whereInVendaMaterial){
		return query()
				.select("venda.cdvenda")
				.join("venda.listavendamaterial vendamaterial")
				.whereIn("vendamaterial.cdvendamaterial", whereInVendaMaterial)
				.list();
	}
	
	public List<Venda> findForGerarLancamentoContabil(GerarLancamentoContabilFiltro filtro, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento){
		return query()
				.select("venda.cdvenda, venda.dtvenda, venda.identificadorexterno, venda.valorfrete, venda.taxapedidovenda, venda.desconto, venda.valorusadovalecompra," +
						"cliente.cdpessoa, cliente.nome," +
						"contaContabilCliente.cdcontacontabil, contaContabilCliente.nome," +
						"centrocusto.cdcentrocusto, centrocusto.nome," +
						"projeto.cdprojeto, projeto.nome," +
						"listavendamaterial.multiplicador, listavendamaterial.preco, listavendamaterial.quantidade, listavendamaterial.desconto," +
						"documento.descricao")
				.join("venda.empresa empresa")
				.join("venda.cliente cliente")
				.leftOuterJoin("cliente.contaContabil contaContabilCliente")
				.leftOuterJoin("venda.centrocusto centrocusto")
				.leftOuterJoin("venda.projeto projeto")
				.join("venda.listavendamaterial listavendamaterial")
				.leftOuterJoin("venda.listavendapagamento listavendapagamento")
				.leftOuterJoin("listavendapagamento.documento documento")
				.where("empresa=?", filtro.getEmpresa())
				.where("venda.dtvenda>=?", filtro.getDtPeriodoInicio())
				.where("venda.dtvenda<=?", filtro.getDtPeriodoFim())
				.where("venda.projeto=?", filtro.getProjeto())
				.where("venda.vendasituacao in (?)", new Object[]{Vendasituacao.REALIZADA})
				.where("not exists (select 1 from Movimentacaocontabilorigem mco where mco.venda=venda and mco.movimentacaocontabil.tipoLancamento=?)", new Object[]{movimentacaocontabilTipoLancamento})
				.list();
	}
	
	public Venda loadWithAllDocumentotipo(String cdvenda){
		if(cdvenda == null || "".equals(cdvenda))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("venda.cdvenda, " +
					"documentotipo.cddocumentotipo, documentotipo.nome, documentotipo.exibirnavenda, " +
					"vendapagamentodocumentotipo.cddocumentotipo, vendapagamentodocumentotipo.nome, vendapagamentodocumentotipo.exibirnavenda ")
			.join("venda.listavendamaterial listavendamaterial")
			.leftOuterJoin("venda.documentotipo documentotipo")
			.leftOuterJoin("venda.listavendapagamento vendapagamento") 
			.leftOuterJoin("vendapagamento.documentotipo vendapagamentodocumentotipo")
			.where("venda.cdvenda = ?", Integer.parseInt(cdvenda))
			.unique();		
	}
	public void findAllForReportQuantitativaDeVendas(QuantitativaDeVendasFiltro filtro) {
		// TODO Auto-generated method stub
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Venda> findByVendaSemContaReceber(Date dateToSearch) {
		StringBuilder sql = new StringBuilder();
		sql.append("select v.cdvenda, v.cdempresa ");
		sql.append("from venda v ");
		sql.append("join pedidovendatipo pvt on pvt.cdpedidovendatipo = v.cdpedidovendatipo ");
		sql.append("where pvt.geracaocontareceberenum <> 3 ");
		sql.append("	and v.cdvendasituacao = 6 ");
		sql.append("	and (v.cdpedidovenda is null or not exists (select doco.cdpedidovenda from documentoorigem doco where doco.cdpedidovenda = v.cdpedidovenda)) ");
		sql.append("	and not exists (select doco.cdvenda from documentoorigem doco where doco.cdvenda = v.cdvenda ");
		sql.append("	and (pvt.geracaocontareceberenum <> 0 or (pvt.geracaocontareceberenum = 0 and exists (select nv.cdvenda from notavenda nv where nv.cdvenda = v.cdvenda)))) ");
		sql.append("	and v.dtvenda between '" + dateToSearch + "' and current_date ");
		sql = new StringBuilder("select v.cdvenda,       v.cdempresa from venda v      join pedidovendatipo pvt on pvt.cdpedidovendatipo = v.cdpedidovendatipo where v.cdvenda = 11483");
		SinedUtil.markAsReader();
		List<Venda> vendaList = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			
			@Override
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Venda venda = new Venda(rs.getInt("cdvenda"));
				if(rs.getInt("cdempresa") > 0) venda.setEmpresa(new Empresa(rs.getInt("cdempresa")));	
				return venda;
			}
		});
		
		return vendaList;
	}
	
	private StringBuilder makeWhereVendasPorVendedor(StringBuilder sql, VendasPorVendedorFiltro filtro, Boolean outrosFaturamentos) {
		sql.append(" where 1 = 1 ");
		
		if (filtro.getCliente() != null) {
			sql.append("and v.cdcliente = " + filtro.getCliente().getCdpessoa() + " ");
		}
		
		if (filtro.getEmpresa() != null) {
			sql.append("and v.cdempresa = " + filtro.getEmpresa().getCdpessoa() + " ");
		}
		
		if (filtro.getDtinicio() != null && filtro.getDtfim() != null) {
			sql.append("and v.dtvenda between '" + filtro.getDtinicio() + "' and '" + filtro.getDtfim() + "' ");
		} else if (filtro.getDtinicio() != null) {
			sql.append("and v.dtvenda between '" + filtro.getDtinicio() + "' and '" + SinedDateUtils.lastDateOfMonth() + "' ");
		} else if (filtro.getDtfim() != null) {
			sql.append("and v.dtvenda between '" + SinedDateUtils.firstDateOfMonth() + "' and '" + filtro.getDtfim() + "' ");
		}
		
		if (filtro.getColaboradorList() != null && filtro.getColaboradorList().length > 0) {
			sql.append("and (");
			
			for (int i = 0 ; i < filtro.getColaboradorList().length ; i++) {
				if (i == filtro.getColaboradorList().length - 1) {
					sql.append("v.cdcolaborador = " + filtro.getColaboradorList()[i].getCdpessoa() + " ");
				} else {
					sql.append("v.cdcolaborador = " + filtro.getColaboradorList()[i].getCdpessoa() + " or ");
				}
			}
			
			sql.append(")");
		}
		
		if (filtro.getSituacaoList() != null && filtro.getSituacaoList().size() > 0) {
			sql.append("and (");
			
			for (int i = 0 ; i < filtro.getSituacaoList().size() ; i++) {
				if (i == filtro.getSituacaoList().size() - 1) {
					sql.append("v.cdvendasituacao = " + filtro.getSituacaoList().get(i).getCdvendasituacao() + " ");
				} else {
					sql.append("v.cdvendasituacao = " + filtro.getSituacaoList().get(i).getCdvendasituacao() + " or ");
				}
			}
			
			sql.append(")");
		}
		
		if (filtro.getRecapagem() && !outrosFaturamentos) {
			sql.append("and (vm.cdpneu is not null and m.producao = true)");
		}
		
		if (outrosFaturamentos) {
			sql.append("and (vm.cdpneu is null or m.producao is null or m.producao = false)");
		}
		
		return sql;
	}
	
	@SuppressWarnings("unchecked")
	public Double findTotalVendas(VendasPorVendedorFiltro filtro) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("select (z.total - z.descontoVenda - z.desconto + coalesce(z.valorIpi, 0)) as total ");
		sql.append("from (");
		sql.append("	select sum(x.descontoVenda) as descontoVenda, sum(x.total) as total, sum(x.valorIpi) as valorIpi, sum(x.desconto) as desconto ");
		sql.append("	from (");
		sql.append("		select sum(vm.preco * vm.quantidade) as total, round(sum(vm.valoripi / 100), 2) as valorIpi, ((v.desconto / 100) / (select sum(vn.quantidade) from vendamaterial vn where vn.cdvenda = v.cdvenda) * vm.quantidade) as descontoVenda, ");
		sql.append("			round(sum(coalesce(cast((vm.desconto / 100) as numeric), (((vm.percentualdesconto * 100) / vm.preco)))), 2) as desconto, v.cdvenda ");
		sql.append("		from venda v ");
		sql.append("		left join cliente l on l.cdpessoa = v.cdcliente ");
		sql.append("		left join empresa e on e.cdpessoa = v.cdempresa ");
		sql.append("		left join colaborador c on c.cdpessoa = v.cdcolaborador ");
		sql.append("		left join pessoa p on p.cdpessoa = c.cdpessoa ");
		sql.append("		left join vendamaterial vm on vm.cdvenda = v.cdvenda ");
		sql.append("		left join material m on m.cdmaterial = vm.cdmaterial ");
		sql.append("		left join materialgrupo mg on mg.cdmaterialgrupo = m.cdmaterialgrupo ");
		
		sql = this.makeWhereVendasPorVendedor(sql, filtro, false);
		
		sql.append("		group by descontoVenda, v.cdvenda ");
		sql.append("	) x ");
		sql.append(") z");

		SinedUtil.markAsReader();
		List<Double> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			
			@Override
			public Double mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getDouble("total");
			}
		});
		
		return lista.get(0);
	}
	
	public List<VendasPorVendedorOutrosFaturamentosBean> findForVendasPorVendedorOutrosFaturamentos(VendasPorVendedorFiltro filtro) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("select w.nome as nomeGrupo, w.quantidade, round((w.faturamentoTotal - w.descontoItem - w.descontoVenda + w.valorIpi) / w.quantidade, 2) as precoMedio, "+
					"	round(w.faturamentoTotal - w.descontoItem - w.descontoVenda + w.valorIpi, 2) as faturamentoTotal, "+
					"	round((w.descontoItem + w.descontoVenda) / w.quantidade, 2) as descontoMedio, "+
					"	round((((w.descontoItem + w.descontoVenda) / w.quantidade) / w.faturamentoTotal * 100), 2) as descontoMedioPercentagem "+
					"from ( "+
					"	select y.nome, sum(y.quantidade) as quantidade, sum(y.faturamentoTotal) as faturamentoTotal, sum(y.descontoItem) as descontoItem, "+
					"			sum(y.valorIpi) as valorIpi, sum(y.descontoVenda / y.quantidadeTotalVenda * y.quantidade) as descontoVenda "+
					"	from ( "+
					"		select z.cdvenda, z.nome, sum(z.quantidade) as quantidade, sum(z.faturamentoTotal) as faturamentoTotal, sum(z.descontoItem) as descontoItem,  "+
					"			z.valorIpi, z.descontoVenda, (select sum(vn.quantidade) from vendamaterial vn where vn.cdvenda = z.cdvenda) as quantidadeTotalVenda "+
					"		from ( "+
					"			select v.cdvenda, mg.nome, vm.quantidade, vm.quantidade * vm.preco as faturamentoTotal, "+
					"				round(coalesce(coalesce((vm.desconto / 100), (((vm.percentualdesconto * 100) / vm.preco))), 0), 2) as descontoItem, "+
					"				round(coalesce((vm.valoripi / 100), 0), 2) as valorIpi, round(coalesce((v.desconto / 100), 0), 2) as descontoVenda "+
					"			from vendamaterial vm "+
					"			left join venda v on v.cdvenda = vm.cdvenda "+
					"			left join material m on m.cdmaterial = vm.cdmaterial "+
					"			left join materialgrupo mg on mg.cdmaterialgrupo = m.cdmaterialgrupo "+
					"			left join colaborador c on c.cdpessoa = v.cdcolaborador "+
					"			left join pessoa p on p.cdpessoa = c.cdpessoa ");
		
		sql = this.makeWhereVendasPorVendedor(sql, filtro, true);
		
		sql.append("group by v.cdvenda, mg.nome, vm.quantidade, vm.preco, vm.desconto, vm.percentualdesconto, vm.valoripi "+
					"			) z "+
					"		group by z.cdvenda, z.nome, z.valorIpi, z.descontoVenda "+
					"	) y "+
					"	group by y.nome "+
					") w "+
					"order by w.nome");

		SinedUtil.markAsReader();
		@SuppressWarnings("unchecked")
		List<VendasPorVendedorOutrosFaturamentosBean> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			
			public VendasPorVendedorOutrosFaturamentosBean mapRow(ResultSet rs, int rowNum) throws SQLException {
				VendasPorVendedorOutrosFaturamentosBean v = new VendasPorVendedorOutrosFaturamentosBean();
				
				v.setNomeGrupo(rs.getString("nomeGrupo"));
				v.setQuantidade(rs.getDouble("quantidade"));
				v.setPrecoMedio(rs.getDouble("precoMedio"));
				v.setFaturaTotal(rs.getDouble("faturamentoTotal"));
				v.setDescontoMedio(rs.getDouble("descontoMedio"));
				v.setDescontoMedioPercentagem(rs.getDouble("descontoMedioPercentagem"));
				
				return v;
			}
		});
		
		return lista;
	}
	
	public List<VendasPorVendendorBean> findForVendasPorVendedor(VendasPorVendedorFiltro filtro) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("select w.vendedor, w.quantidade, round((w.faturamentoTotal - w.descontoItem - w.descontoVenda + w.valorIpi) / w.quantidade, 2) as precoMedio, "+
					"	round(w.faturamentoTotal - w.descontoItem - w.descontoVenda + w.valorIpi, 2) as faturamentoTotal, "+
					"	round((w.descontoItem + w.descontoVenda) / w.quantidade, 2) as descontoMedio, "+
					"	round((((w.descontoItem + w.descontoVenda) / w.quantidade) / w.faturamentoTotal * 100), 2) as descontoMedioPercentagem, "+
					"	round(w.comissao / w.quantidade, 2) as comissionamentoMedio, "+
					"	round((round(w.comissao / w.quantidade, 2) / (w.faturamentoTotal - w.descontoItem - w.descontoVenda + w.valorIpi) * 100), 2) as comissionamentoMedioPercentagem, "+
					
					(filtro.getRecapagem() ? " w.rateiocustoproducao as quantidadeConvertida, round((w.faturamentoTotal - w.descontoItem - w.descontoVenda + w.valorIpi) / w.rateiocustoproducao, 2) as precoMedioConvertida "
							: "0 as quantidadeConvertida, 0 as precoMedioConvertida ") +
							
					"from ( "+
					"	select y.vendedor, sum(y.quantidade) as quantidade, sum(y.faturamentoTotal) as faturamentoTotal, sum(y.descontoItem) as descontoItem, "+
					"			sum(y.valorIpi) as valorIpi, sum(y.descontoVenda / y.quantidadeTotalVenda * y.quantidade) as descontoVenda "+ 
					
					(filtro.getRecapagem() ? ", sum(y.rateiocustoproducao) as rateiocustoproducao " 
							: " ") + 
					
					", sum(y.comissao) as comissao "+
					"	from ( "+
					"		select z.cdvenda, z.vendedor, sum(z.quantidade) as quantidade, sum(z.faturamentoTotal) as faturamentoTotal, sum(z.descontoItem) as descontoItem,  "+
					"			z.valorIpi, z.descontoVenda, z.comissao, (select sum(vn.quantidade) from vendamaterial vn where vn.cdvenda = z.cdvenda) as quantidadeTotalVenda "+ 
					
					(filtro.getRecapagem() ? ", sum(z.rateiocustoproducao) as rateiocustoproducao " 
							: " ") +
							
					"		from ( "+
					"			select v.cdvenda, p.nome as vendedor, vm.quantidade, vm.quantidade * vm.preco as faturamentoTotal, "+
					"				round(coalesce(coalesce((vm.desconto / 100), (((vm.percentualdesconto * 100) / vm.preco))), 0), 2) as descontoItem, "+
					"				round(coalesce((vm.valoripi / 100), 0), 2) as valorIpi, round(coalesce((v.desconto / 100), 0), 2) as descontoVenda, "+
					
					(filtro.getRecapagem() ? "coalesce(mg.rateiocustoproducao * vm.quantidade, 0) as rateiocustoproducao, " 
							: " ") + 
					
					"round(coalesce(dc.valorcomissao / 100, 0), 2) as comissao "+
					"from vendamaterial vm "+
					"left join venda v on v.cdvenda = vm.cdvenda "+
					"left join material m on m.cdmaterial = vm.cdmaterial "+
					"left join materialgrupo mg on mg.cdmaterialgrupo = m.cdmaterialgrupo "+
					"left join colaborador c on c.cdpessoa = v.cdcolaborador "+
					"left join pessoa p on p.cdpessoa = c.cdpessoa "+
					"left join documentocomissao dc on dc.cdvenda = v.cdvenda ");
		
		sql = this.makeWhereVendasPorVendedor(sql, filtro, false);
		
		sql.append("group by v.cdvenda, p.nome, vm.quantidade, vm.preco, vm.desconto, vm.percentualdesconto, vm.valoripi " + 
		
					(filtro.getRecapagem() ? ", mg.rateiocustoproducao " 
							: "") + 
							
					"	, dc.valorcomissao "+
					"		) z "+
					"		group by z.cdvenda, z.vendedor, z.valorIpi, z.descontoVenda, z.comissao "+
					"	) y "+
					"	group by y.vendedor "+
					") w "+
					"order by w.vendedor");

		SinedUtil.markAsReader();
		@SuppressWarnings("unchecked")
		List<VendasPorVendendorBean> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			
			public VendasPorVendendorBean mapRow(ResultSet rs, int rowNum) throws SQLException {
				VendasPorVendendorBean v = new VendasPorVendendorBean();
				
				v.setVendedor(rs.getString("vendedor"));
				v.setQuantidade(rs.getDouble("quantidade"));
				v.setPrecoMedio(rs.getDouble("precoMedio"));
				v.setFaturamentoTotal(rs.getDouble("faturamentoTotal"));
				v.setDescontoMedio(rs.getDouble("descontoMedio"));
				v.setDescontoMedioPercentagem(rs.getDouble("descontoMedioPercentagem"));
				v.setComissionamentoMedio(rs.getDouble("comissionamentoMedio"));
				v.setComissionamentoMedioPercentagem(rs.getDouble("comissionamentoMedioPercentagem"));
				v.setQuantidadeConvertida(rs.getDouble("quantidadeConvertida"));
				v.setPrecoMedioConvertida(rs.getDouble("precoMedioConvertida"));
				
				return v;
			}
		});
		
		return lista;
	}
	
	public Venda loadWithPedidovendatipo(Venda venda){
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nulo.");
		
		return query()
				.select("venda.cdvenda, venda.dtestorno, " +
						"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.reserva, pedidovendatipo.situacaoNota, pedidovendatipo.reservarApartir, " +
						"pedidovendatipo.baixaestoqueEnum, pedidovendatipo.geracaocontareceberEnum, pedidovendatipo.gerarexpedicaovenda, pedidovendatipo.entregaFutura, " +
						"pedidovenda.cdpedidovenda, pedidovenda.situacaoPendencia, pedidovendatipo.vendasEcommerce, pedidovendatipo.enviarDadosNotaAutomaticamenteEcommerce")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("venda.pedidovenda pedidovenda")
				.where("venda  = ?", venda)
				.unique();		
	}
	
	public Venda loadForGerarReserva(Venda venda){
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nulo.");
		
		return query()
				.select("venda.cdvenda, venda.dtestorno, " +
						"vendasituacao.cdvendasituacao, vendasituacao.descricao, "+
						"empresa.cdpessoa, empresa.nome, "+
						"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.reserva, pedidovendatipo.gerarexpedicaovenda, "+
						"pedidovendatipo.reservarApartir, pedidovendatipo.situacaoNota, pedidovendatipo.baixaestoqueEnum, "+
						"vendamaterial.cdvendamaterial, vendamaterial.quantidade, "+
						"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, "+
						"loteestoque.cdloteestoque, loteestoque.numero, "+
						"material.cdmaterial, "+
						"unidademedidaVm.cdunidademedida, unidademedidaVm.nome, "+
						"unidademedida.cdunidademedida, unidademedida.nome")
				.join("venda.listavendamaterial vendamaterial")
				.leftOuterJoin("vendamaterial.unidademedida unidademedidaVm")
				.leftOuterJoin("venda.empresa empresa")
				.leftOuterJoin("venda.vendasituacao vendasituacao")
				.leftOuterJoin("venda.localarmazenagem localarmazenagem")
				.leftOuterJoin("vendamaterial.material material")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("vendamaterial.loteestoque loteestoque")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.where("venda  = ?", venda)
				.unique();		
	}
	
	public Venda loadForGerarReservaNaExpedicao(Venda venda, Expedicao expedicao){
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nulo.");
		if(expedicao == null || expedicao.getCdexpedicao() == null)
			throw new SinedException("Expedi��o n�o pode ser nulo.");
		
		return query()
				.select("venda.cdvenda, venda.dtestorno, " +
						"vendasituacao.cdvendasituacao, vendasituacao.descricao, "+
						"empresa.cdpessoa, empresa.nome, "+
						"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.reserva, pedidovendatipo.baixaestoqueEnum, pedidovendatipo.gerarexpedicaovenda, "+
						"vendamaterial.cdvendamaterial, vendamaterial.quantidade, "+
						"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, "+
						"loteestoque.cdloteestoque, loteestoque.numero, "+
						"material.cdmaterial, material.vendapromocional, materialExp.cdmaterial, "+
						"unidademedida.cdunidademedida, unidademedida.nome, "+
						"unidademedidaVm.cdunidademedida, unidademedidaVm.nome, "+
						"unidademedidaExp.cdunidademedida, unidademedidaExp.nome, "+
						"expedicaoitem.cdexpedicaoitem, expedicaoitem.qtdeexpedicao, "+
						"expedicao.cdexpedicao, loteEstoque.cdloteestoque")
				.join("venda.listavendamaterial vendamaterial")
				.leftOuterJoin("vendamaterial.unidademedida unidademedidaVm")
				.leftOuterJoin("venda.empresa empresa")
				.leftOuterJoin("venda.vendasituacao vendasituacao")
				.leftOuterJoin("venda.localarmazenagem localarmazenagem")
				.leftOuterJoin("vendamaterial.material material")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("vendamaterial.loteestoque loteestoque")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("vendamaterial.listaExpedicaoitem expedicaoitem")
				.leftOuterJoin("expedicaoitem.material materialExp")
				.leftOuterJoin("expedicaoitem.unidademedida unidademedidaExp")
				.leftOuterJoin("expedicaoitem.expedicao expedicao")
				.leftOuterJoin("expedicaoitem.loteEstoque loteEstoque")
				.where("venda  = ?", venda)
				.where("expedicao  = ?", expedicao)
				.unique();		
	}
	
	public List<Venda> findForGerarReservaNaExpedicao(String whereInExpedicoes){
		return query()
				.select("venda.cdvenda, venda.dtestorno, " +
						"vendasituacao.cdvendasituacao, vendasituacao.descricao, "+
						"empresa.cdpessoa, empresa.nome, "+
						"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.reserva, pedidovendatipo.baixaestoqueEnum, pedidovendatipo.gerarexpedicaovenda, "+
						"vendamaterial.cdvendamaterial, vendamaterial.quantidade, "+
						"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, "+
						"loteestoque.cdloteestoque, loteestoque.numero, "+
						"material.cdmaterial, "+
						"unidademedida.cdunidademedida, unidademedida.nome, "+
						"unidademedidaVm.cdunidademedida, unidademedidaVm.nome, "+
						"expedicaoitem.cdexpedicaoitem, expedicaoitem.qtdeexpedicao, "+
						"expedicao.cdexpedicao")
				.join("venda.listavendamaterial vendamaterial")
				.leftOuterJoin("vendamaterial.unidademedida unidademedidaVm")
				.leftOuterJoin("venda.empresa empresa")
				.leftOuterJoin("venda.vendasituacao vendasituacao")
				.leftOuterJoin("venda.localarmazenagem localarmazenagem")
				.leftOuterJoin("vendamaterial.material material")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("vendamaterial.loteestoque loteestoque")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("vendamaterial.listaExpedicaoitem expedicaoitem")
				.leftOuterJoin("expedicaoitem.expedicao expedicao")
				.whereIn("expedicao.cdexpedicao", whereInExpedicoes)
				.list();		
	}
	public List<Venda> recuperarVendasPorMaterial(int codMaterial) {
		return query().select("venda.cdvenda,pedidovenda.cdpedidovenda,venda.dtvenda,material.cdmaterial," +
				"material.nome,listaMaterial.quantidade")
				.leftOuterJoin("venda.listavendamaterial listaMaterial")
				.leftOuterJoin("venda.pedidovenda pedidovenda")
				.leftOuterJoin("venda.vendasituacao situacao")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("listaMaterial.material material")
				.where("material.cdmaterial = ?",+ codMaterial)
				.where("situacao.cdvendasituacao <> 2")
				.where("situacao.cdvendasituacao <> 4")
				.where("pedidovendatipo.reserva = true").list();
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Venda> findBoletoEnviadoGerado(String whereInVenda) {
		if(StringUtils.isBlank(whereInVenda))
			return new ArrayList<Venda>();
		
		List<Documentoacao> listaDocumentoacao = new ArrayList<Documentoacao>();
		listaDocumentoacao.add(Documentoacao.BOLETO_GERADO);
		listaDocumentoacao.add(Documentoacao.ENVIO_BOLETO);
		listaDocumentoacao.add(Documentoacao.ENVIO_MANUAL_BOLETO);
		
		
		String sql = "select doco.cdvenda " +
					" from documentoorigem doco " +
					" join documento d on d.cddocumento = doco.cddocumento " +
					" join documentohistorico dh on dh.cddocumento = d.cddocumento " +
					" where doco.cdvenda in (" + whereInVenda + ") " +
					" and dh.cddocumentoacao in (" + CollectionsUtil.listAndConcatenate(listaDocumentoacao, "cddocumentoacao", ",") + ") " +
					" and d.cddocumentoacao <> " + Documentoacao.CANCELADA.getCddocumentoacao() + "";

		SinedUtil.markAsReader();
		return getJdbcTemplate().query(sql, new RowMapper() {
					public Venda mapRow(ResultSet rs, int rowNum) throws SQLException {
						return new Venda(rs.getInt("cdvenda"));
					}
				}
			);
	}
	public List<Venda> findIdentificadoresExternos(String identificadorexterno, Integer id) {
		
		return query().select("venda.cdvenda,venda.identificadorexterno")
			.where("venda.identificadorexterno like ?", identificadorexterno)
			.where("venda.cdvenda <> ?", id)
			.list();	
	}
	
	public  List<Venda> existePedidoVenda(Integer cdvenda) {
		return query().select("venda.cdvenda,venda.identificadorexterno")
				.where("venda.cdvenda = ?", cdvenda)
				.where("venda.pedidovenda is not null")
				.list();
	}
	public boolean isOrigamOrcamento(Integer cdvendaorcamento) {
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Venda.class)
				.join("venda.vendaorcamento vendaorcamento")
				.where("venda.vendasituacao <> ?", Vendasituacao.CANCELADA)
				.where("vendaorcamento.cdvendaorcamento = ?",cdvendaorcamento)
				.unique() > 0;
	}
	
	public List<Venda> buscarValoresConfirmadosVendaByPedidoVenda(Pedidovenda pedidovenda) {
		return query()
			.select("venda.cdvenda, venda.taxapedidovenda, venda.desconto, venda.valorfrete, venda.valorusadovalecompra")
			.where("venda.pedidovenda = ?", pedidovenda)
			.list();
	}
	
	public SomatorioConfirmadoParcial somarValoresConfirmadosPedidoVenda(Pedidovenda pedidovenda) {
		try {
			StringBuilder sql = new StringBuilder();
			
			sql
				.append(" select cdpedidovenda, sum(taxapedidovenda) as taxapedidovenda, sum(desconto) as desconto, ")
				.append(" sum(valorfrete) as valorfrete, sum(valorusadovalecompra) as valorusadovalecompra ")
				.append(" from venda ")
				.append(" where cdpedidovenda = " + pedidovenda.getCdpedidovenda() + " ")
				.append(" and venda.cdvendasituacao <> ?" + Vendasituacao.CANCELADA.getCdvendasituacao())
				.append(" group by cdpedidovenda ");

			SinedUtil.markAsReader();
			return (SomatorioConfirmadoParcial) getJdbcTemplate().queryForObject(sql.toString(), new RowMapper() {
				public SomatorioConfirmadoParcial mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new SomatorioConfirmadoParcial(new Pedidovenda(rs.getInt("cdpedidovenda")), 
							new Money(rs.getDouble("taxapedidovenda"), true), 
							new Money(rs.getDouble("desconto"), true),
							new Money(rs.getDouble("valorusadovalecompra"), true),
							new Money(rs.getDouble("valorfrete"), true));
				}
			});
		} catch(Exception e) {
			return null;
		}
	}
	
	public List<Venda> findForRecalculoFaixaMarkup(String whereIn){
		return query()
				.select("venda.valorusadovalecompra, venda.desconto, " +
						"vendamaterial.percentualdesconto, vendamaterial.desconto, vendamaterial.cdvendamaterial, vendamaterial.preco, " +
						"vendamaterial.quantidade, vendamaterial.valorcustomaterial, vendamaterial.multiplicador, venda.taxapedidovenda, venda.valorfrete, " +
						"empresa.cdpessoa, " +
						"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.bonificacao, " +
						"material.cdmaterial, material.cadastrarFaixaMarkup")
				.join("venda.listavendamaterial vendamaterial")
				.leftOuterJoin("venda.empresa empresa")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.join("vendamaterial.material material")
				.whereIn("venda.cdvenda", whereIn)
				.list();
	}
	
	public Venda loadUltimaVendaNaoCanceladaByPedido(Pedidovenda pedidoVenda) {
		return query()
				.select("venda.cdvenda, pedidovenda.cdpedidovenda")
				.leftOuterJoin("venda.pedidovenda pedidovenda")
				.where("venda.vendasituacao <> ?", Vendasituacao.CANCELADA)
				.where("pedidovenda = ?", pedidoVenda)
				.orderBy("venda.cdvenda desc")
				.setMaxResults(1)
				.unique();
	}
	
	public List<Venda> findForComprovanteTEF(String whereIn) {
		return query()
					.select("venda.cdvenda, venda.dtvenda, venda.observacao, venda.valorfrete, venda.taxapedidovenda, venda.desconto, venda.valorusadovalecompra, " +
							"prazopagamento.cdprazopagamento, prazopagamento.nome, " +
							"endereco.cdendereco, endereco.substituirlogradouro, endereco.descricao, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, " +
							"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla, " +
							"vendamaterial.cdvendamaterial, vendamaterial.quantidade, vendamaterial.desconto, vendamaterial.preco, vendamaterial.multiplicador, vendamaterial.peso, " +
							"material.cdmaterial, material.cdmaterial, material.nome, material.pesobruto, material.peso, " +
							"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
							"vendapagamento.cdvendapagamento, vendapagamento.valororiginal, vendapagamento.dataparcela, " +
							"documentotipo.cddocumentotipo, documentotipo.nome, " +
							"empresa.cdpessoa, empresa.nome, empresa.cnpj, " +
							"cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj, cliente.tipopessoa, " +
							"enderecoEmpresa.cdendereco, enderecoEmpresa.substituirlogradouro, enderecoEmpresa.descricao, enderecoEmpresa.logradouro, enderecoEmpresa.numero, enderecoEmpresa.complemento, enderecoEmpresa.bairro, " +
							"municipioEmpresa.cdmunicipio, municipioEmpresa.nome, ufEmpresa.cduf, ufEmpresa.sigla, " +
							"telefoneEmpresa.telefone, telefoneEmpresa.cdtelefone, telefone.cdtelefone, telefone.telefone")
					.leftOuterJoin("venda.empresa empresa")
					.leftOuterJoin("empresa.listaTelefone telefoneEmpresa")
					.leftOuterJoin("empresa.listaEndereco enderecoEmpresa")
					.leftOuterJoin("enderecoEmpresa.municipio municipioEmpresa")
					.leftOuterJoin("municipioEmpresa.uf ufEmpresa")
					.leftOuterJoin("venda.prazopagamento prazopagamento")
					.leftOuterJoin("venda.cliente cliente")
					.leftOuterJoin("cliente.listaTelefone telefone")
					.leftOuterJoin("venda.endereco endereco")
					.leftOuterJoin("endereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("venda.listavendamaterial vendamaterial")
					.leftOuterJoin("vendamaterial.material material")
					.leftOuterJoin("vendamaterial.unidademedida unidademedida")
					.leftOuterJoin("venda.listavendapagamento vendapagamento")
					.leftOuterJoin("vendapagamento.documentotipo documentotipo")
					.whereIn("venda.cdvenda", whereIn)
					.orderBy("vendamaterial.ordem, vendapagamento.dataparcela")
					.list();
	}
	
	public List<Venda> carregarDadosMargemContribuicao(RelatorioMargemContribuicaoFiltro filtro) {
		StringBuilder subQuery = new StringBuilder();
		
		subQuery.append("select vm.venda.cdvenda ");
		subQuery.append("from Vendamaterial vm ");
		subQuery.append("left outer join vm.material m ");
		subQuery.append("left outer join m.listaFornecedor mf ");
		subQuery.append("left outer join m.materialgrupo mg ");
		subQuery.append("left outer join m.materialtipo mt ");
		subQuery.append("left outer join m.materialcategoria mc ");
		
		subQuery.append("where 1 = 1 and ");
		
		if (filtro.getMaterial() != null) {
			subQuery.append("m.cdmaterial = " + filtro.getMaterial().getCdmaterial() + " and ");
		}
		
		if (filtro.getMaterialGrupo() != null) {
			subQuery.append("mg.cdmaterialgrupo = " + filtro.getMaterialGrupo().getCdmaterialgrupo() + " and ");
		}
		
		if (filtro.getMaterialTipo() != null) {
			subQuery.append("mt.cdmaterialtipo = " + filtro.getMaterialTipo().getCdmaterialtipo() + " and ");
		}
		
		if (filtro.getMaterialCategoria() != null) {
			subQuery.append("mc.cdmaterialcategoria = " + filtro.getMaterialCategoria().getCdmaterialcategoria() + " and ");
		}
		
		if (filtro.getFornecedor() != null) {
			subQuery.append("mf.fornecedor.cdpessoa = " + filtro.getFornecedor().getCdpessoa() + " and ");
		}
		
		if (filtro.getCdVenda() != null) {
			subQuery.append("vm.venda.cdvenda = " + filtro.getCdVenda() + " and ");
		}
		
		String subQueryString = subQuery.substring(0, subQuery.length() - 4);
		
		subQueryString += "group by vm.venda.cdvenda ";
		
		QueryBuilder<Venda> query = query();
		
		query
			.select("venda.cdvenda, venda.valorfrete, venda.desconto, " +
				"listavendamaterial.cdvendamaterial, listavendamaterial.quantidade, listavendamaterial.preco, listavendamaterial.valoricms, listavendamaterial.valoricmsst, " +
				"listavendamaterial.valorfcp, listavendamaterial.valorfcpst, listavendamaterial.valoripi, listavendamaterial.valoricmsdestinatario, listavendamaterial.valoricmsdestinatario, " +
				"listavendamaterial.valoricmsremetente, listavendamaterial.valorfcpdestinatario, listavendamaterial.valordifal, listavendamaterial.custooperacional, " +
				"listavendamaterial.quantidade, listavendamaterial.preco, listavendamaterial.desconto, listavendamaterial.valorComissao, listavendamaterial.valorcustomaterial, " +
				"listavendamaterial.percentualdesoneracaoicms, listavendamaterial.valordesoneracaoicms, listavendamaterial.abaterdesoneracaoicms, " +
				"listaMaterialdevolucao.cdmaterialdevolucao ,listaMaterialdevolucao.preco, listaMaterialdevolucao.qtdedevolvida, " +
				"material.cdmaterial, material.nome, material.valorcusto, " +
				"materialgrupo.cdmaterialgrupo, materialgrupo.nome, " +
				"materialcategoria.cdmaterialcategoria, materialcategoria.descricao, " +
				"materialtipo.cdmaterialtipo, materialtipo.nome, " +
				"fornecedor.cdpessoa, fornecedor.tipopessoa, fornecedor.nome, fornecedor.razaosocial, " +
				"cliente.cdpessoa, cliente.nome, cliente.razaosocial, enderecoCliente.enderecotipo, municipioCliente.cdmunicipio, municipioCliente.nome, " +
				"colaborador.cdpessoa, colaborador.nome, " +
				"municipio.cdmunicipio, municipio.nome, " +
				"venda.taxavenda ")
			.leftOuterJoin("venda.empresa empresa")
			.leftOuterJoin("venda.cliente cliente")
			.leftOuterJoin("cliente.listaEndereco enderecoCliente")
			.leftOuterJoin("enderecoCliente.municipio municipioCliente")
			.leftOuterJoin("venda.vendasituacao vendasituacao")
			.leftOuterJoin("cliente.listaPessoacategoria listaPessoacategoria")
			.leftOuterJoin("listaPessoacategoria.categoria categoria")
			.leftOuterJoin("venda.colaborador colaborador")
			.leftOuterJoin("venda.endereco endereco")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("venda.listavendamaterial listavendamaterial")
			.leftOuterJoin("listavendamaterial.material material")
			.leftOuterJoin("material.listaFornecedor listaFornecedor")
			.leftOuterJoin("listaFornecedor.fornecedor fornecedor")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.materialtipo materialtipo")
			.leftOuterJoin("material.materialcategoria materialcategoria")
			.leftOuterJoin("listavendamaterial.listaMaterialdevolucao listaMaterialdevolucao")
			.where("empresa = ?", filtro.getEmpresa())
			.where("cliente = ?", filtro.getCliente())
			.where("categoria = ?", filtro.getCategoria())
			.where("venda.dtvenda >= ?", filtro.getDtInicio())
			.where("venda.dtvenda <= ?", filtro.getDtFim());
		
		List<Vendasituacao> listaVendasituacao = new ArrayList<Vendasituacao>();
		
		if (BooleanUtils.isTrue(filtro.getVendaFaturada())) {
			listaVendasituacao.add(Vendasituacao.FATURADA);
		}
		
		if (BooleanUtils.isTrue(filtro.getVendaRealizada())) {
			listaVendasituacao.add(Vendasituacao.REALIZADA);
		}
		
		//Se n�o marcar nenhuma das flags, deve trazer todas as vendas realizadas e faturadas (como se tivesse marcado as duas)
		if(BooleanUtils.isFalse(filtro.getVendaRealizada()) && BooleanUtils.isFalse(filtro.getVendaFaturada())){
			listaVendasituacao.add(Vendasituacao.FATURADA);
			listaVendasituacao.add(Vendasituacao.REALIZADA);
		}
		
		if (listaVendasituacao != null && !listaVendasituacao.isEmpty()) {
			query
				.whereIn("vendasituacao.cdvendasituacao", SinedUtil.listAndConcatenate(listaVendasituacao, "cdvendasituacao", ","));
		}
		
		query.whereIn("venda.cdvenda", subQueryString);
		
		return query
				.list();
	}
		
	public List<Venda> findByPedidovendaNaoNegociado(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Pedido de venda n�o pode ser nulo.");
		
		QueryBuilderSined<Venda> query =  querySined();
		preencheConsultaFindByPedidovenda(query, pedidovenda);
		
		query.select(query.getSelect().getValue() + ", pedidovendamaterial.cdpedidovendamaterial");
		query.join("vendamaterial.pedidovendamaterial pedidovendamaterial");
		
		return query
				.list();
	}
	
	public List<Venda> carregarDadosMargemContribuicaoVenda(RelatorioMargemContribuicaoFiltro filtro) {
		QueryBuilder<Venda> query = query();
		
		query
			.select("venda.cdvenda, " +
					"cliente.cdpessoa, cliente.nome, cliente.razaosocial, cliente.tipopessoa, " +
					"listavendamaterial.cdvendamaterial, listavendamaterial.quantidade, listavendamaterial.preco ")
			.leftOuterJoin("venda.cliente cliente")
			.leftOuterJoin("venda.listavendamaterial listavendamaterial")
			.whereIn("venda.cdvenda", filtro.getIdsVenda());
		
		return query
					.list();
	}
	
	public boolean existsVendaWithTipobaixa(String whereInVendas, BaixaestoqueEnum tipoBaixa){
		return newQueryBuilderSined(Long.class)
				.from(Venda.class)
				.setUseTranslator(false)
				.select("count(*)")
				.join("venda.pedidovendatipo pedidovendatipo")
				.where("pedidovendatipo.baixaestoqueEnum = ?", tipoBaixa)
				.whereIn("venda.cdvenda", whereInVendas)
				.unique()
				.intValue() > 0;
	}

	public Double getTicketMedioForAndroid(Integer cdusuario) {
		return newQueryBuilderWithFrom(Double.class)
				.setUseTranslator(false)
				.select("avg(vendamaterial.preco)")
				.join("venda.listavendamaterial vendamaterial")
				.where("venda.vendasituacao <> ?", Vendasituacao.CANCELADA)
				.where("venda.vendasituacao <> ?", Vendasituacao.AGUARDANDO_APROVACAO)
				.where("venda.vendasituacao <> ?", Vendasituacao.EMPRODUCAO)
				.where("venda.dtvenda >= ?", SinedDateUtils.firstDateOfMonth())
				.where("venda.dtvenda <= ?", SinedDateUtils.lastDateOfMonth())
				.where("venda.colaborador = ?", new Colaborador(cdusuario))
				.unique();
	}
	
	public void limparQuantidadeVolume(Venda venda) {
		if(Util.objects.isPersistent(venda)){
			getHibernateTemplate().bulkUpdate("update Venda v set v.quantidadevolumes = null where v = ?", venda);
		}
	}
	
	public List<Venda> findByExpedicao(String whereInExpedicao) {
		if(StringUtils.isEmpty(whereInExpedicao)){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		String sql = " SELECT distinct vm.cdvenda " +
			     " FROM expedicaoitem ei " + 
				 " join vendamaterial vm on vm.cdvendamaterial = ei.cdvendamaterial " + 
				 " WHERE ei.cdexpedicao in (" + whereInExpedicao + ") " + 
			     " order by vm.cdvenda " ;
		
		@SuppressWarnings("unchecked")
		List<Venda> lista = getJdbcTemplate().query(sql, new RowMapper() {			
			public Venda mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Venda(rs.getInt("cdvenda"));
			}
		});
		
		return lista;
	}
	
	public void updateCampo(Venda venda, String nomeCampo, Object valor) {
		if (Util.objects.isNotPersistent(venda)) {
			throw new SinedException("Par�metros inv�lidos.");
		}
		
		if(valor != null){
			getHibernateTemplate().bulkUpdate("update Venda venda set "+nomeCampo+" = ? where venda = ?", new Object[] {valor, venda});
		}else {
			getHibernateTemplate().bulkUpdate("update Venda venda set "+nomeCampo+" = null where venda = ?", new Object[] {venda});
		}
	}
	
	public int atualizaTransportadora(String whereInVendas, Fornecedor transportadora) {
		boolean atualiza = false;
		if(Util.objects.isNotPersistent(transportadora)){
			atualiza = CollectionsUtil.isListNotEmpty(query()
						.whereIn("venda.cdvenda", whereInVendas)
						.where("venda.terceiro is not null")
						.list());
		}else{
			atualiza = CollectionsUtil.isListNotEmpty(query()
					.whereIn("venda.cdvenda", whereInVendas)
					.where("venda.terceiro <> ?", transportadora)
					.list());
		}
		
		if(atualiza){
			if (transportadora == null) {
				return getHibernateTemplate().bulkUpdate("update Venda v set v.terceiro = null where v.id in (" + whereInVendas + ")");
			}
			
			return getHibernateTemplate().bulkUpdate("update Venda v set v.terceiro = ? where v.id in (" + whereInVendas + ") ", transportadora);
		}
		return 0;
	}
	
	public void insertVendaEntregueTabelaSincronizacaoEcommerce(Ecom_PedidoVenda bean) {
		getJdbcTemplate().execute("select sincronizacaotabelaecommerce_insert_update(9,false," + bean.getCdEcom_pedidovenda() + ");");
	}
	
	public Venda loadForValidacaoEntregaEcommerce(Integer id){
		if(id == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
	
		.select("venda.cdvenda, venda.dtvenda, venda.entregaEcommerceRealizada, " +
				"vendasituacao.cdvendasituacao, vendasituacao.descricao")
		.leftOuterJoin("venda.vendasituacao vendasituacao")
		
		.where("venda.cdvenda=?",id)
		.setMaxResults(1)
		.unique();
	}
}
