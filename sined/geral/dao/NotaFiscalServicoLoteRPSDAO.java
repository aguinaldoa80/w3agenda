package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoLoteRPS;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiscalServicoLoteRPSFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotaFiscalServicoLoteRPSDAO extends GenericDAO<NotaFiscalServicoLoteRPS> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<NotaFiscalServicoLoteRPS> query, FiltroListagem _filtro) {
		NotaFiscalServicoLoteRPSFiltro filtro = (NotaFiscalServicoLoteRPSFiltro) _filtro;
		
		query
			.select("distinct notaFiscalServicoLoteRPS.cdNotaFiscalServicoLoteRPS, notaFiscalServicoLoteRPS.datacriacao, " +
					"arquivo.cdarquivo, arquivo.nome, arquivotomador.cdarquivo, arquivotomador.nome")
			.join("notaFiscalServicoLoteRPS.arquivo arquivo")
			.join("notaFiscalServicoLoteRPS.arquivotomador arquivotomador")
			.leftOuterJoin("notaFiscalServicoLoteRPS.listaNotaFiscalServicoRPS notaFiscalServicoRPS")
			.leftOuterJoin("notaFiscalServicoRPS.notaFiscalServico notaFiscalServico")
			.where("cast(notaFiscalServico.numero AS long) >= ?", filtro.getNumNotaDe())
			.where("cast(notaFiscalServico.numero AS long) <= ?", filtro.getNumNotaAte())
			.where("notaFiscalServicoLoteRPS.datacriacao >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getDataCriacaoDe()))
			.where("notaFiscalServicoLoteRPS.datacriacao <= ?", SinedDateUtils.dateToTimestampFinalDia(filtro.getDataCriacaoAte()))
			.orderBy("notaFiscalServicoLoteRPS.datacriacao DESC")
			.ignoreJoin("notaFiscalServicoRPS", "notaFiscalServico");
		
		String paramId = NeoWeb.getRequestContext().getParameter("cdNotaFiscalServicoLoteRPS");
		if(paramId != null && !paramId.trim().equals("")){
			query.where("notaFiscalServicoLoteRPS.cdNotaFiscalServicoLoteRPS = ?", Integer.parseInt(paramId));
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<NotaFiscalServicoLoteRPS> query) {
		query
			.select("notaFiscalServicoLoteRPS.cdNotaFiscalServicoLoteRPS, notaFiscalServicoLoteRPS.datacriacao, " +
					"arquivo.cdarquivo, arquivo.nome, arquivotomador.cdarquivo, arquivotomador.nome, " +
					"notaFiscalServico.cdNota, notaFiscalServico.numero, notaFiscalServico.dtEmissao, " +
					"cliente.cdpessoa, cliente.nome")
			.join("notaFiscalServicoLoteRPS.arquivo arquivo")
			.join("notaFiscalServicoLoteRPS.arquivotomador arquivotomador")
			.leftOuterJoin("notaFiscalServicoLoteRPS.listaNotaFiscalServicoRPS notaFiscalServicoRPS")
			.leftOuterJoin("notaFiscalServicoRPS.notaFiscalServico notaFiscalServico")
			.leftOuterJoin("notaFiscalServico.cliente cliente");
	}
	
}
