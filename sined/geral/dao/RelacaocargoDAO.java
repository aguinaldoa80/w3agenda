package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Relacaocargo;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.RelacaoCargoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RelacaocargoDAO extends GenericDAO<Relacaocargo>{
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaDependenciacargo");
		save.saveOrUpdateManaged("listaDependenciafaixa");
	}
	
	/**
	 * Busca as rela��es entre os cargos a partir de um filtro.
	 *
	 * @param filtro
 	 * @return List<Relacaocargo>
 	 * 
 	 * @throws SinedException - caso o or�amento seja nulo
	 * 
 	 * @author Rodrigo Alvarenga
	 */
	public List<Relacaocargo> findForListagemFlex(RelacaoCargoFiltro filtro) {
		if (filtro.getOrcamento() == null || filtro.getOrcamento().getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		return 
			query()
				.select("relacaocargo.cdrelacaocargo, relacaocargo.quantidade, " +
						"orcamento.cdorcamento, orcamento.nome, orcamento.prazosemana, " +
						"tiporelacaorecurso.cdtiporelacaorecurso, tiporelacaorecurso.nome, " +
						"tipodependenciarecurso.cdtipodependenciarecurso, tipodependenciarecurso.nome, " +
						"cargoDependente.cdcargo, cargoDependente.nome, " +
						"listaDependenciacargo.cddependenciacargo, " +
						"cargoOrigem.cdcargo, cargoOrigem.nome, " +
						"listaDependenciafaixa.cddependenciafaixa, listaDependenciafaixa.faixade, listaDependenciafaixa.faixaate, listaDependenciafaixa.quantidade")						
				.join("relacaocargo.orcamento orcamento")
				.join("relacaocargo.tiporelacaorecurso tiporelacaorecurso")
				.join("relacaocargo.tipodependenciarecurso tipodependenciarecurso")
				.leftOuterJoin("relacaocargo.listaDependenciacargo listaDependenciacargo")
				.leftOuterJoin("listaDependenciacargo.cargo cargoOrigem")
				.leftOuterJoin("relacaocargo.listaDependenciafaixa listaDependenciafaixa")				
				.join("relacaocargo.cargo cargoDependente")
				.where("orcamento = ?", filtro.getOrcamento())
				.whereLikeIgnoreAll("cargoDependente.nome", filtro.getNomeCargo())
				.orderBy("cargoDependente.nome, cargoOrigem.nome, listaDependenciafaixa.faixade, listaDependenciafaixa.faixaate")
				.list();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Relacaocargo> query) {
		query
			.select("relacaocargo.cdrelacaocargo, relacaocargo.quantidade, " +
					"orcamento.cdorcamento, orcamento.nome, " +
					"tiporelacaorecurso.cdtiporelacaorecurso, tiporelacaorecurso.nome, " +
					"tipodependenciarecurso.cdtipodependenciarecurso, tipodependenciarecurso.nome, " +
					"cargoDependente.cdcargo, cargoDependente.nome, " +
					"listaDependenciacargo.cddependenciacargo, " +
					"cargoOrigem.cdcargo, cargoOrigem.nome, " +
					"listaDependenciafaixa.cddependenciafaixa, listaDependenciafaixa.faixade, listaDependenciafaixa.faixaate, listaDependenciafaixa.quantidade")
			.join("relacaocargo.orcamento orcamento")
			.join("relacaocargo.tiporelacaorecurso tiporelacaorecurso")
			.join("relacaocargo.tipodependenciarecurso tipodependenciarecurso")
			.leftOuterJoin("relacaocargo.listaDependenciacargo listaDependenciacargo")
			.leftOuterJoin("listaDependenciacargo.cargo cargoOrigem")
			.leftOuterJoin("relacaocargo.listaDependenciafaixa listaDependenciafaixa")
			.join("relacaocargo.cargo cargoDependente")
			.orderBy("cargoDependente.nome, cargoOrigem.nome, listaDependenciafaixa.faixade, listaDependenciafaixa.faixaate");
	}
	
	/**
	 * Carrega a lista de rela��es entre os cargos de um determinado or�amento.
	 *
	 * @param orcamento
	 * @return List<Relacaocargo>
	 * @throws SinedException - caso o par�metro orcamento seja nulo
	 * @author Rodrigo Alvarenga
	 */
	public List<Relacaocargo> findByOrcamento(Orcamento orcamento){
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("O par�metro or�amento n�o pode ser nulo.");
		}
		return query()
			.select("relacaocargo.cdrelacaocargo, relacaocargo.quantidade, " +
					"orcamento.cdorcamento, orcamento.nome, " +
					"tiporelacaorecurso.cdtiporelacaorecurso, tiporelacaorecurso.nome, " +
					"tipodependenciarecurso.cdtipodependenciarecurso, tipodependenciarecurso.nome, " +
					"cargoDependente.cdcargo, cargoDependente.nome, cargoDependente.totalhorasemana, cargoDependente.custohora, " +
					"tipocargo.cdtipocargo, " +
					"listaDependenciacargo.cddependenciacargo, " +
					"cargoOrigem.cdcargo, cargoOrigem.nome, " +
					"listaDependenciafaixa.cddependenciafaixa, listaDependenciafaixa.faixade, listaDependenciafaixa.faixaate, listaDependenciafaixa.quantidade")
			.join("relacaocargo.orcamento orcamento")
			.join("relacaocargo.tiporelacaorecurso tiporelacaorecurso")
			.join("relacaocargo.tipodependenciarecurso tipodependenciarecurso")
			.leftOuterJoin("relacaocargo.listaDependenciacargo listaDependenciacargo")
			.leftOuterJoin("listaDependenciacargo.cargo cargoOrigem")
			.leftOuterJoin("relacaocargo.listaDependenciafaixa listaDependenciafaixa")
			.join("relacaocargo.cargo cargoDependente")
			.leftOuterJoin("cargoDependente.tipocargo tipocargo")
			.where("orcamento = ?", orcamento)
			.orderBy("cargoDependente.nome, cargoOrigem.nome, listaDependenciafaixa.faixade, listaDependenciafaixa.faixaate")
			.list();
	}
	
	@Override
	public void saveOrUpdateNoUseTransaction(Relacaocargo bean) {
		try {
			super.saveOrUpdateNoUseTransaction(bean);
		}
		catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "UK_RELACAOCARGO_1")) {
				throw new SinedException("J� existe uma rela��o cadastrada para o cargo " + bean.getCargo().getNome() + ".");
			}
			if (DatabaseError.isKeyPresent(e, "UK_DEPENDENCIAFAIXA")) {
				throw new SinedException("Existem faixas de valores iguais.");
			}			
			throw new SinedException(e.getMessage());
		}
	}
}
