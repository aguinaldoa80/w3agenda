package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.HashSet;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Metacomercial;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MetacomercialFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MetacomercialDAO extends GenericDAO<Metacomercial> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Metacomercial> query, FiltroListagem _filtro) {
		MetacomercialFiltro filtro = (MetacomercialFiltro) _filtro;
		
		query
			.select("metacomercial.cdmetacomercial, metacomercial.nome, metacomercial.dtinicio, metacomercial.dtfim, metacomercial.meta")
			.leftOuterJoin("metacomercial.listaMetacomercialitem listaMetacomercialitem")
			.leftOuterJoin("listaMetacomercialitem.cargo cargo")
			.leftOuterJoin("listaMetacomercialitem.colaborador colaborador")
			.where("cargo = ?", filtro.getCargo())
			.where("colaborador = ?", filtro.getColaborador())
			.where("metacomercial.tipometacomercial = ?", filtro.getTipometacomercial())
			.whereLikeIgnoreAll("metacomercial.nome", filtro.getNome())
			.ignoreJoin("listaMetacomercialitem", "cargo", "colaborador");
		
		if(filtro.getDtfim() != null){
			query.where("metacomercial.dtinicio <= ?", filtro.getDtfim());
		}
		
		if(filtro.getDtinicio() != null){
			query
				.openParentheses()
					.where("metacomercial.dtfim >= ?", filtro.getDtinicio())
					.or()
					.where("metacomercial.dtfim is null")
				.closeParentheses();
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Metacomercial> query) {
		query
			.select("metacomercial.cdmetacomercial, metacomercial.nome, metacomercial.dtinicio, metacomercial.dtfim, metacomercial.meta, " +
					"metacomercial.tipometacomercial, listaMetacomercialitem.cdmetacomercialitem, cargo.cdcargo, colaborador.cdpessoa, " +
					"colaborador.nome, " +
					"metacomercial.dtaltera, metacomercial.cdusuarioaltera")
			.leftOuterJoin("metacomercial.listaMetacomercialitem listaMetacomercialitem")
			.leftOuterJoin("listaMetacomercialitem.cargo cargo")
			.leftOuterJoin("listaMetacomercialitem.colaborador colaborador");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaMetacomercialitem");
	}

	/**
	 * M�todo que verifica se existe meta no mesmo per�odo
	 *
	 * @param metacomercial
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existsMetaPeriodo(Metacomercial metacomercial) {
		
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Metacomercial.class)
			.leftOuterJoin("metacomercial.listaMetacomercialitem listaMetacomercialitem")
			.leftOuterJoin("listaMetacomercialitem.cargo cargo")
			.leftOuterJoin("listaMetacomercialitem.colaborador colaborador")
			.where("metacomercial <> ?", metacomercial.getCdmetacomercial() != null ? metacomercial : null)
			.openParentheses()
				.where("listaMetacomercialitem is null")
				.or()
				.openParentheses()
					.where("cargo is null")
					.where("colaborador is null")
				.closeParentheses()
			.closeParentheses();
		
		if(metacomercial.getDtinicio() != null){
			query
				.where("metacomercial.dtinicio <= ?", metacomercial.getDtinicio())
				.openParentheses()
					.where("metacomercial.dtfim >= ?", metacomercial.getDtinicio())
					.or()
					.where("metacomercial.dtfim is null")
				.closeParentheses();			
		}
		
		return query.unique() > 0;
	}

	/**
	 * M�todo que busca as metas de acordo com o per�odo
	 *
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Metacomercial> findForAcompanhamentometa(Date dtinicio, Date dtfim) {
		QueryBuilder<Metacomercial> query = query()
			.select("metacomercial.cdmetacomercial, metacomercial.nome, metacomercial.dtinicio, metacomercial.dtfim, metacomercial.meta, " +
					"metacomercial.tipometacomercial, listaMetacomercialitem.cdmetacomercialitem, cargo.cdcargo, colaborador.cdpessoa, " +
					"colaborador.nome ")
			.leftOuterJoin("metacomercial.listaMetacomercialitem listaMetacomercialitem")
			.leftOuterJoin("listaMetacomercialitem.cargo cargo")
			.leftOuterJoin("listaMetacomercialitem.colaborador colaborador");
	
		if(dtfim != null){
			query.where("metacomercial.dtinicio <= ?", dtfim);
		}
		
		if(dtinicio != null){
			query
				.openParentheses()
					.where("metacomercial.dtfim >= ?", dtinicio)
					.or()
					.where("metacomercial.dtfim is null")
				.closeParentheses();
		}
		
		return query.list();
	}
	
	/**
	 * M�todo que busca a lista de colaboradores para altera��o da meta
	 * 
	 * @param metacomercial, whereIn (cdcolaborador)
	 * @author Danilo Guimar�es
	 */
	public List<Metacomercial> findForMetaColaboradores (Metacomercial metacomercial, String whereIn){
		return query()
				.leftOuterJoin("metacomercial.listaMetacomercialitem listaMetacomercialitem")
				.where("listaMetacomercialitem.metacomercial <> ?", metacomercial)
				.where("metacomercial.dtinicio > ?", metacomercial.getDtfim())
				.whereIn("listaMetacomercialitem.colaborador", whereIn)
				.list();
	}
	
	public List<Metacomercial> findForCsv (MetacomercialFiltro filtro) {
		QueryBuilder<Metacomercial> query = query();
		updateListagemQuery(query, filtro);
		query
		.setIgnoreJoinPaths(new HashSet<String>());
		query
		.select("metacomercial.nome, metacomercial.dtinicio, metacomercial.dtfim, metacomercial.meta, listaMetacomercialitem.colaborador");
		
		return query.list();
	}
	
	public List<Metacomercial> findForMetaAtualizacao (String whereIn) {
		return query()
				.select("metacomercial.cdmetacomercial, metacomercial.nome, metacomercial.dtinicio, metacomercial.dtfim, metacomercial.meta, " +
					"metacomercial.tipometacomercial, listaMetacomercialitem.cdmetacomercialitem, cargo.cdcargo, colaborador.cdpessoa, " +
					"colaborador.nome, " +
					"metacomercial.dtaltera, metacomercial.cdusuarioaltera")
					.leftOuterJoin("metacomercial.listaMetacomercialitem listaMetacomercialitem")
					.leftOuterJoin("listaMetacomercialitem.cargo cargo")
					.leftOuterJoin("listaMetacomercialitem.colaborador colaborador")
					.whereIn("metacomercial.cdmetacomercial", whereIn)
				.list();
	}
	
}
