package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Atividade;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.AtividadeFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("atividade.descricao")
public class AtividadeDAO extends GenericDAO<Atividade>{

	
	@Override
	public void updateListagemQuery(QueryBuilder<Atividade> query,FiltroListagem _filtro) {
		AtividadeFiltro filtro = (AtividadeFiltro)_filtro;
		
		query
			.select("atividade.cdatividade,atividade.nome,atividade.nome,atividade.dtinicio,atividade.dtfim, atividade.descricao," +
					"tarefa.descricao,planejamento.descricao, projeto.nome, colaborador.nome,"  +
					"atividadetipo.nome") 
			.leftOuterJoin("atividade.tarefa tarefa")
			.leftOuterJoin("atividade.planejamento planejamento")
			.join("atividade.projeto projeto")
			.join("atividade.colaborador colaborador")
			.leftOuterJoin("atividade.atividadetipo atividadetipo")
			
			.whereLikeIgnoreAll("atividade.nome", filtro.getDescricao())
			.where("atividade.dtinicio >= ?",filtro.getDtinicio())
			.where("atividade.dtfim <= ?",filtro.getDtfim())
			.where("projeto = ?",filtro.getProjeto())
			.where("planejamento = ?",filtro.getPlanejamento())
			.where("tarefa = ?",filtro.getTarefa())
			.where("colaborador = ?",filtro.getColaborador())
		;
		
		if(filtro.getProjeto() == null){
			query.where("1=0");
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Atividade> query) {
		query.select("atividade.cdatividade,projeto.cdprojeto,planejamento.cdplanejamento," +
				"tarefa.cdtarefa,atividade.dtinicio,atividade.dtfim,atividade.descricao,atividade.nome," +
				"responsavel.cdpessoa,responsavel.nome," +
				"atividadetipo.cdatividadetipo");
		
		query.leftOuterJoin("atividade.tarefa tarefa");
		query.join("atividade.colaborador responsavel");
		query.leftOuterJoin("atividade.atividadetipo atividadetipo");
		query.leftOuterJoin("atividade.planejamento planejamento");
		query.join("atividade.projeto projeto");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
	}

	/**
	 * Obt�m lista de atividade para o relat�rio de atividades.
	 * A query � a mesma da listagem.
	 * 
	 * @see #updateListagemQuery(QueryBuilder, FiltroListagem)
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Atividade> findForReport(AtividadeFiltro filtro){
		QueryBuilder<Atividade> query = querySined();
		this.updateListagemQuery(query, filtro);
		return query.list(); 
	}

	/**
	 * Carrega a lista de atividades de um di�rio de obra.
	 *
	 * @param dtdia
	 * @param projeto
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Atividade> findByData(Date dtdia, Projeto projeto) {
		if(dtdia == null || projeto == null || projeto.getCdprojeto() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("atividade.cdatividade, atividade.nome, atividade.descricao, atividade.dtinicio, " +
							"atividade.dtfim, planejamento.cdplanejamento, tarefa.cdtarefa, colaborador.cdpessoa, " +
							"atividadetipo.cdatividadetipo, atividade.cdusuarioaltera, atividade.dtaltera, " +
							"atividadetipo.nome, projeto.cdprojeto")
					.join("atividade.atividadetipo atividadetipo")
					.join("atividade.colaborador colaborador")
					.leftOuterJoin("atividade.planejamento planejamento")
					.leftOuterJoin("atividade.projeto projeto")
					.leftOuterJoin("atividade.tarefa tarefa")
					.where("projeto = ?", projeto)
					.where("atividade.dtinicio <= ?",dtdia)
					.where("atividade.dtfim >= ?",dtdia)
					.orderBy("atividadetipo.nome, atividade.nome, atividade.descricao")
					.list();
	}
}

