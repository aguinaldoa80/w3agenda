package br.com.linkcom.sined.geral.dao;


import java.util.List;

import br.com.linkcom.sined.geral.bean.Operacaocontabil;
import br.com.linkcom.sined.geral.bean.Operacaocontabildebitocredito;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OperacaocontabildebitocreditoDAO extends GenericDAO<Operacaocontabildebitocredito>{
	
	/**
	 * 
	 * @param operacaocontabil
	 * @param propertyOperacaocontabil
	 * @author Thiago Clemente
	 * 
	 */
	public List<Operacaocontabildebitocredito> findByOperacaocontabil(Operacaocontabil operacaocontabil, String propertyOperacaocontabil){
		return query()
				.select("operacaocontabildebitocredito.cdoperacaocontabildebitocredito, operacaocontabildebitocredito.controle," +
						propertyOperacaocontabil + ".cdoperacaocontabil, contaContabil.cdcontacontabil, contaContabil.nome," +
						"operacaocontabildebitocredito.tipo, operacaocontabildebitocredito.formula, " +
						"operacaocontabildebitocredito.centrocusto, operacaocontabildebitocredito.projeto, operacaocontabildebitocredito.sped, operacaocontabildebitocredito.historico, " +
						"operacaocontabildebitocredito.codigointegracao ")
				.join("operacaocontabildebitocredito." + propertyOperacaocontabil + " " + propertyOperacaocontabil)
				.leftOuterJoin("operacaocontabildebitocredito.contaContabil contaContabil")
				.where(propertyOperacaocontabil + "=?", operacaocontabil)
				.orderBy("contaContabil.nome")
				.list();
	}	
}
