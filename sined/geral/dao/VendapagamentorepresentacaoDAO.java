package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendapagamentorepresentacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VendapagamentorepresentacaoDAO extends GenericDAO<Vendapagamentorepresentacao> {
	
	/**
	* M�todo que busca os pagamentos de representa��o
	*
	* @param venda
	* @return
	* @since 13/02/2015
	* @author Luiz Fernando
	*/
	public List<Vendapagamentorepresentacao> findVendaPagamentoRepresentacaoByVenda(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nula!");
		
		return query()
			.select("vendapagamentorepresentacao.cdvendapagamentorepresentacao, vendapagamentorepresentacao.valororiginal, " +
					"vendapagamentorepresentacao.dataparcela, " +
					"documentotipo.cddocumentotipo, documentotipo.nome, " +
					"prazopagamentorepresentacao.cdprazopagamento, prazopagamentorepresentacao.nome, " +
					"venda.cdvenda, " + 
					"aux_documento.cddocumento, aux_documento.valoratual, " +
					"documento.cddocumento, documento.dtvencimento, documento.valor," +
					"fornecedor.cdpessoa, fornecedor.nome ")
			.join("vendapagamentorepresentacao.venda venda")
			.leftOuterJoin("vendapagamentorepresentacao.documento documento")
			.leftOuterJoin("documento.aux_documento aux_documento")
			.leftOuterJoin("vendapagamentorepresentacao.documentotipo documentotipo")
			.leftOuterJoin("venda.prazopagamentorepresentacao prazopagamentorepresentacao")
			.leftOuterJoin("vendapagamentorepresentacao.fornecedor fornecedor")
			.where("venda = ?", venda)
			.orderBy("vendapagamentorepresentacao.dataparcela, documento.cddocumento")
			.list();
	}
}
