package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Chequehistorico;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ChequehistoricoDAO extends GenericDAO<Chequehistorico>{

	/**
	 * M�todo que verifica se existe hist�rico do cheque
	 *
	 * @param cheque
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existHistoricoByCheque(Cheque cheque) {
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Chequehistorico.class)	
			.join("chequehistorico.cheque cheque")
			.where("cheque = ?", cheque)
			.unique() > 0;
	}
	
	/**
	 * M�todo que busca o hist�rico do cheque
	 *
	 * @param cheque
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Chequehistorico> findByCheque(Cheque cheque){
		return query()
				.select("cheque.cdcheque, chequehistorico.cdchequehistorico, chequehistorico.cdusuarioaltera, chequehistorico.dtaltera, " +
						"chequehistorico.observacao, cheque.valor, chequehistorico.chequesituacao," +
						"chequedevolucaomotivo.cdchequedevolucaomotivo, chequedevolucaomotivo.codigo, chequedevolucaomotivo.descricao ")
				.join("chequehistorico.cheque cheque")
				.leftOuterJoin("chequehistorico.chequedevolucaomotivo chequedevolucaomotivo")
				.where("cheque = ?", cheque)
				.orderBy("chequehistorico.cdchequehistorico desc")
				.list();
		
	}

	/**
	* M�todo que retorna o hist�rico de acordo com a movimenta��o do cheque
	*
	* @param cheque
	* @param movimentacao
	* @return
	* @since 03/01/2018
	* @author Luiz Fernando
	*/
	public Chequehistorico findHistoricoDevolvido(Cheque cheque, Movimentacao movimentacao) {
		if(movimentacao == null || movimentacao.getCdmovimentacao() == null)
			return null;
		
		return query()
				.select("chequehistorico.cdchequehistorico, chequehistorico.chequesituacao")
				.where("chequehistorico.cheque = ?", cheque)
				.whereLikeIgnoreAll("chequehistorico.observacao", "javascript:visualizaMovimentacao("+movimentacao.getCdmovimentacao()+")")
				.orderBy("chequehistorico.dtaltera desc, chequehistorico.cdchequehistorico")
				.setMaxResults(1)
				.unique();
	}
	
}
