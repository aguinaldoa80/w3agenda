package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Garantia;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.GarantiaFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("upper(retira_acento(garantia.descricao))")
public class GarantiaDAO extends GenericDAO<Garantia> {

	@Override
	public void updateListagemQuery(QueryBuilder<Garantia> query, FiltroListagem _filtro) {
		GarantiaFiltro filtro = (GarantiaFiltro) _filtro;
		query
			.select("garantia.cdgarantia, garantia.descricao")
			.whereLikeIgnoreAll("garantia.descricao", filtro.getDescricao())
			.orderBy("upper(retira_acento(garantia.descricao))");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Garantia> query) {
		query
			.select("garantia.cdgarantia, garantia.descricao, garantia.cdusuarioaltera, garantia.dtaltera");
	}
	
}
