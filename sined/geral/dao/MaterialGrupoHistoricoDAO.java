package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.MaterialGrupoHistorico;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialGrupoHistoricoDAO extends GenericDAO<MaterialGrupoHistorico>{

	public List< MaterialGrupoHistorico> findByMaterialGrupoHistorico (Materialgrupo materialGrupo){
		return query()
				.select("materialGrupoHistorico.cdMaterialGrupoHistorico,materialGrupoHistorico.acao,materialGrupoHistorico.cdusuarioaltera,materialGrupoHistorico.dtaltera,materialGrupoHistorico.observacao")
				.leftOuterJoin("materialGrupoHistorico.cdMaterialGrupo cdmaterialgrupo")
				.where("materialGrupoHistorico.cdMaterialGrupo=?",materialGrupo)
				.orderBy("materialGrupoHistorico.cdMaterialGrupoHistorico DESC")
				.list();
	}
}
