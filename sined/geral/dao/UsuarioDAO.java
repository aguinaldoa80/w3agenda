package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.UsuarioFiltro;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("usuario.nome")
public class UsuarioDAO extends GenericDAO<Usuario> {

	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Usuario> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		UsuarioFiltro filtro = (UsuarioFiltro) _filtro;
		query
			.select("distinct new br.com.linkcom.sined.geral.bean.Usuario(" +
					"usuario.cdpessoa, usuario.nome, usuario.bloqueado, usuario.login, " +
					"cast(nomepapeisusuario(usuario.cdpessoa) as string), usuario.email" +
				")")
			.setUseTranslator(false)
			.ignoreAllJoinsPath(true)
			.leftOuterJoin("usuario.listaUsuarioempresa usuarioEmpresa")
			.leftOuterJoin("usuarioEmpresa.empresa empresa")
			.leftOuterJoin("usuario.listaUsuariopapel usuarioPapel")
			.leftOuterJoin("usuarioPapel.papel papel")
			.whereLikeIgnoreAll("usuario.nome", filtro.getNome())
			.whereLikeIgnoreAll("usuario.login", filtro.getLogin())
			.where("papel = ?", filtro.getPapel())
			.where("empresa = ?", filtro.getEmpresa())
			.where("usuario.bloqueado = ?",filtro.getBloqueado())			
			.orderBy("usuario.nome");
		;
	}
	

	@Override
	public void updateEntradaQuery(QueryBuilder<Usuario> query) {
		StringBuilder campos = new StringBuilder();
		campos.append("usuario.cdpessoa, usuario.senha, usuario.apelido, usuario.todosprojetos, usuario.todosdepartamentos,");
		campos.append("usuario.nome, usuario.cpf, usuario.cnpj, usuario.dtnascimento, usuario.restricaoaprovarpedidoclientedevedor, ");
		campos.append("usuario.email, usuario.bloqueado,usuario.login, usuario.semprelogaroffline, usuario.restricaoFornecedorColaborador, ");
		campos.append("papel.cdpapel, papel.nome, usuario.dtaltera, usuario.cdusuarioaltera, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, ");
		campos.append("arquivo.cdarquivo, arquivo.nome, arquivo.tipoconteudo, arquivo.tamanho,");
		campos.append("imagemassinaturaemail.cdarquivo, imagemassinaturaemail.nome, imagemassinaturaemail.tipoconteudo, imagemassinaturaemail.tamanho,");
		campos.append("usuarioprojeto.cdusuarioprojeto, projeto.cdprojeto, usuario.restricaodespesaviagem, ");
		campos.append("usuario.salvarAbaixoMinimoVenda, usuario.salvarAbaixoMinimoPedido, usuario.salvarAbaixoMinimoOrcamento, usuario.visualizaroutrasoportunidades, ");
		campos.append("usuariodepartamento.cdusuariodepartamento, departamento.cddepartamento, usuario.limitepercentualdesconto, usuario.restricaoempresacliente, usuario.restricaoempresafornecedor, ");
		campos.append("usuario.restricaofornecedorproduto, usuario.observacao, usuario.limitevaloraprovacaoautorizacao, usuario.restricaoclientevendedor, usuario.restricaovendavendedor, usuario.senhaterminal, ")
			  .append("usuario.limitePercentualMarkupVenda");
		
		query
			.select(campos.toString())
			.leftOuterJoin("usuario.rubrica arquivo")
			.leftOuterJoin("usuario.imagemassinaturaemail imagemassinaturaemail")
			.leftOuterJoin("usuario.listaUsuariopapel usuariopapel")
			.leftOuterJoin("usuariopapel.papel papel")
			.leftOuterJoin("usuario.listaUsuarioempresa usuarioempresa")
			.leftOuterJoin("usuarioempresa.empresa empresa")
			.leftOuterJoin("usuario.listaUsuariodepartamento usuariodepartamento")
			.leftOuterJoin("usuariodepartamento.departamento departamento")
			.leftOuterJoin("usuario.listaUsuarioprojeto usuarioprojeto")
			.leftOuterJoin("usuarioprojeto.projeto projeto");
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		
		save.saveOrUpdateManaged("listaUsuariopapel");
		save.saveOrUpdateManaged("listaUsuarioempresa");
		save.saveOrUpdateManaged("listaUsuarioprojeto");
		save.saveOrUpdateManaged("listaUsuariodepartamento");
		save.saveOrUpdateManaged("listaRestricaoacessopessoa");
		
		Usuario usuario = (Usuario) save.getEntity();
		if(usuario.getRubrica() != null){
			arquivoDAO.saveFile(usuario, "rubrica");
		}
		if(usuario.getImagemassinaturaemail() != null){
			arquivoDAO.saveFile(usuario, "imagemassinaturaemail");
		}
	}
	
	/**
	 * Encontra uma pessoa pelo e-mail
	 * @param email
	 * @param login
	 * @return null se n�o encontrar
	 * @author Jo�o Paulo Zica
	 */
	public Usuario carregarUsuarioByEmail(String email) {
		if (email ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		return querySined()
				
				.where("usuario.email=?",email)
				.unique();
	}
	/**
	 * Encontra uma pessoa pelo login
	 * @param login
	 * @return null se n�o encontrar
	 * @author Fl�vio Tavares
	 */
	public Usuario carregarUsuarioByLogin(String login) {
		if (login ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		return
		querySined()
		
		.where("usuario.login=?",login)
		.unique();
	}
	
	/**
	 * Carrega todos os dados de um usu�rio atrav�s da pk.
	 * 
	 * @param cdusuario
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public Usuario carregarUsuarioByPK(Integer cdusuario){
		if (cdusuario == null) {
			throw new SinedException("O par�metro cdusuario n�o pode ser null.");
		}
		return
		querySined()
			
			.leftOuterJoinFetch("usuario.listaUsuariopapel usuariopapel")
			.leftOuterJoinFetch("usuariopapel.papel papel")
			.where("usuario.cdpessoa=?", cdusuario)
			.unique();
	}
	
	/**
	 * M�todo para usu�rio que n�o s�o administradores e s� podem fazer altera��o de senha
	 * 
	 * @param usuario
	 * @author Jo�o Paulo Zica
	 */
	public void alterarSenhaUsuario(Usuario usuario){
		if (usuario ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		Usuario load = this.load(usuario);
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioLogado != null){
			load.setCdusuarioaltera(usuarioLogado.getCdpessoa());
			load.setDtaltera(new Timestamp(System.currentTimeMillis()));
		}
		
		load.setSenha(usuario.getSenha());
		save(load).execute();
		getHibernateTemplate().flush();
	}

	/**
	 * M�todo para salvar o usu�rio j� cadastrado no sistema como colaborador.
	 * @param usuario
	 * @author Jo�o Paulo Zica
	 * @author Hugo Ferreira
	 */
	public void salvaUsuarioColaborador(Usuario usuario){
		if (usuario ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		String senha = usuario.getSenha();
		String senhaTerminal = usuario.getSenhaterminal();
		Integer cdpessoa = usuario.getCdpessoa();
		Boolean bloqueado = usuario.getBloqueado();	
		String login = usuario.getLogin();
		getJdbcTemplate().update("insert into Usuario (cdpessoa, login, senha, senhaterminal, bloqueado, trocasenha, apelido, todosprojetos, todosdepartamentos ) values (?,?,?,?,?,?,?,?,?)",
				new Object[]{cdpessoa,login,senha, senhaTerminal,bloqueado,false,usuario.getApelido(),usuario.getTodosprojetos(),usuario.getTodosdepartamentos()});
	}
	
	/**
	 * M�todo para deletar o usu�rio que � colaborador
	 * 
	 * @param usuario
	 * @author Jo�o Paulo Zica
	 */
	public void deleteUsuario(Usuario usuario){
		if (usuario ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		getJdbcTemplate().update("delete from Usuario where cdpessoa = ?", new Object[]{usuario.getCdpessoa()});
	}
	
	/**
	 *  Verifica se a pessoa � colaborador e usu�rio do sistema
	 * @param pessoa
	 * @throws SinedException - Se o par�metro informado for null
	 * @return null se n�o encontrar
	 * @author Jo�o Paulo Zica
	 */
	public Usuario findUsuariocolaborador(Integer cdpessoa){
		if (cdpessoa == null) {
			throw new SinedException("Par�metro cdpessoa n�o pode ser null.");
		}
		return
			querySined()
				
				.where("usuario.cdpessoa = ?", cdpessoa)
				.setMaxResults(1)
				.unique();
	}

	/**
	 * M�todo para obter a listaUsuariopapel de um usu�rio. 
	 * 
	 * @param usuario
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Usuario carregaUsuario(Usuario usuario){
		if(usuario == null || usuario.getCdpessoa()==null){
			throw new SinedException("Par�metros inv�lidos em usuario.");
		}
		return querySined()
			
			.leftOuterJoinFetch("usuario.listaUsuariopapel usuariopapel")
			.leftOuterJoinFetch("usuariopapel.papel papel")
			.leftOuterJoinFetch("papel.listaDashboardpapel listaDashboardpapel")
			.leftOuterJoinFetch("papel.listaDashboardespecificopapel listaDashboardespecificopapel")
			.where("usuario = ?",usuario)
			.entity(usuario)
			.unique();
	}
	
	/**
	 * M�todo para obter a listaUsuarioempresa de um usu�rio. 
	 * 
	 * @param usuario
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Usuario carregaUsuarioWithEmpresa(Usuario usuario){
		if(usuario == null || usuario.getCdpessoa()==null){
			throw new SinedException("Par�metros inv�lidos em usuario.");
		}
		return querySined()
				
				.select("usuario.cdpessoa, usuario.nome, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
						"usuario.restricaoclientevendedor, usuario.restricaovendavendedor, usuarioempresa.cdusuarioempresa")
				.leftOuterJoin("usuario.listaUsuarioempresa usuarioempresa")
				.leftOuterJoin("usuarioempresa.empresa empresa")
				.where("usuario = ?", usuario)
				.unique();
	}
	
	/**
	 * <p>Fornece a senha de um usu�rio.</p>
	 * @param usuario
	 * @return
	 * @author Hugo Ferreira
	 */
	public String carregarSenha(Usuario usuario) {
		if (usuario == null || usuario.getCdpessoa() == null) {
			throw new SinedException("Par�metros inv�lidos em usu�rio.");
		}
		return newQueryBuilderWithFrom(String.class)
			.select("usuario.senha")
			.where("usuario = ?", usuario)
			.setUseTranslator(false)
			.unique();
	}

	@Override
	public Usuario load(Usuario bean) {
		if (bean == null || bean.getCdpessoa() == null) {
			return null;
		}
		return super.load(bean);
	}
	/**
	 * Fornece uma lista usu�rios do sistema.
	 * @param orderBy
	 * @return
	 * @author Ramon Brazil
	 */
	@Override
	public List<Usuario> findAll(String orderBy) {
		return super.findAll(orderBy);
	}
	
	/**
	 * Executa um saveOrUpdate sem criar transa��o com o banco.
	 * 
	 * @see #saveOrUpdateNoUseTransaction(Usuario, boolean)
	 * @param bean
	 * @author Hugo Ferreira
	 * @author Fl�vio Tavares
	 */
	public void saveOrUpdateNoUseTransaction(Usuario bean) {
		this.saveOrUpdateNoUseTransaction(bean, true);
	}
	
	/**
	 * Executa um saveOrUpdate sem criar transa��o com o banco.
	 * 
	 * @param bean
	 * @param updateSaveOrUpdate - se true o m�todo chama o m�todo {@link #updateSaveOrUpdate(SaveOrUpdateStrategy)} para 
	 * 						atualizar o save.
	 * 						- se false salva sem utilizar o {@link #updateSaveOrUpdate(SaveOrUpdateStrategy)}
	 * @author Hugo Ferreira
	 * @author Fl�vio Tavares
	 */
	public void saveOrUpdateNoUseTransaction(Usuario bean, boolean updateSaveOrUpdate) {
		((Log) bean).setDtaltera(new Timestamp(System.currentTimeMillis()));
		((Log) bean).setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		
		SaveOrUpdateStrategy save = save(bean);
		save.useTransaction(false);
		if(updateSaveOrUpdate) updateSaveOrUpdate(save);
		save.execute();
		getHibernateTemplate().flush();
	}
	
	/* singleton */
	private static UsuarioDAO instance;
	public static UsuarioDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(UsuarioDAO.class);
		}
		return instance;
	}


	/**
	 * Verifica se algum usu�rio tem o login do fornecedor passado por par�metro.
	 *
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean exiteLoginUsuario(Fornecedor bean) {
		if (bean == null || bean.getLogin() == null) {
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		}
		SinedUtil.markAsReader();
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Usuario.class)
					.setUseTranslator(false)
					.where("usuario.login = ?",bean.getLogin())
					.unique() > 0;
	}
	
	/**
	 * Verifica na base de dados se o email j� est� cadastrado para um usu�rio
	 * diferente do usu�rio que est� sendo salvo.
	 * 
	 * @param cdUsuario
	 * 		Se n�o for recebido (null), verifica apenas se o e-mail existe no sistema.
	 * 		Caso seja recebido, verifica se o e-mail existe para um usu�rio de c�digo
	 * 		diferente do informado.
	 * @param email
	 * @return
	 * @author Hugo Ferreira
	 */
	public boolean existeEmailCadastrado(Integer cdUsuario, String email) {
		if (StringUtils.isEmpty(email)) {
			throw new SinedException("O par�metro email � obrigat�rio.");
		}
		
		SinedUtil.markAsReader();
		QueryBuilder<Long> query = newQueryBuilder(Long.class);
		
		query
			.select("count(*)")
			.from(Usuario.class)
			.where("usuario.email = ?", email)
			.where("usuario.cdpessoa <> ?", cdUsuario);
		Long count = query.unique();
		
		return count != 0;
	}


	/**
	 * Carrega a lista de usu�rios s� com nome e cdpessoa preenchido.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Usuario> findForGerenciaPlanejamento(String whereIn) {
		return querySined()
					
					.removeUseWhere()
					.select("usuario.cdpessoa, usuario.nome")
					.whereIn("usuario.cdpessoa",whereIn)
					.list();
	}
	
	/**
	 * Retorna usu�rio com e-mail carregado
	 * @param usuario
	 * @return
	 * @author C�ntia Nogueira
	 */
	public Usuario loadWithEmail(Usuario usuario){
		if(usuario == null || usuario.getCdpessoa()==null){
			throw new SinedException("O objeto n�o pode ser nulo em UsuarioDAO");
		}
		
		
		return querySined()
		
		.select("usuario.cdpessoa, usuario.nome,usuario.email")		
		.where("usuario.cdpessoa=?", usuario.getCdpessoa())		
		.unique();
	}


	/**
	 * M�todo que busca os usu�rios desbloqueados mais o usu�rio em quest�o, 
	 * mesmo funcionamento do findAtivos
	 *  
	 * @param usuario
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Usuario> findUsuariosDesbloqueados(Usuario usuario) {
		return querySined()
			
			.select("usuario.cdpessoa, usuario.nome,listaUsuariodepartamento.departamento," +
					"listaUsuariodepartamento.cdusuariodepartamento," +
					"departamento.cddepartamento,departamento.nome,responsavel.cdpessoa,departamento.descricao")
			.leftOuterJoin("usuario.listaUsuariodepartamento listaUsuariodepartamento")
			.leftOuterJoin("listaUsuariodepartamento.departamento departamento")
			.leftOuterJoin("departamento.responsavel responsavel")
			.where("usuario.bloqueado = ?", Boolean.FALSE).or()
			.where("usuario = ?", usuario)
			.orderBy("usuario.nome")
			.list();
	}

	/**
	 * M�todo que carrega imagem da rubrica do usu�rio caso ele possua.
	 * 
	 * @param usuario
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Usuario carregaRubricaUsuario(Usuario usuario) {
		return querySined()
			
			.select("arquivo.cdarquivo, arquivo.nome, arquivo.tipoconteudo, arquivo.tamanho")
			.leftOuterJoin("usuario.rubrica arquivo")
			.where("usuario = ?", usuario)
			.unique();
	}
	
	/**
	 * M�todo que carrega imagem da assinatura para o email do usu�rio.
	 *
	 * @param usuario
	 * @return
	 * @author Luiz Fernando
	 */
	public Usuario carregaImagemassinaturaemailUsuario(Usuario usuario) {
		return querySined()
			
			.select("arquivo.cdarquivo, arquivo.nome, arquivo.tipoconteudo, arquivo.tamanho")
			.join("usuario.imagemassinaturaemail arquivo")
			.where("usuario = ?", usuario)
			.unique();
	}

	public Usuario carregaUsuarioWithProjeto(Usuario usuario) {
		if(usuario == null || usuario.getCdpessoa()==null){
			throw new SinedException("Par�metros inv�lidos em usuario.");
		}
		return querySined()
				
				.select("usuario.cdpessoa, projeto.cdprojeto")
				.leftOuterJoin("usuario.listaUsuarioprojeto listaUsuarioprojeto")
				.leftOuterJoin("listaUsuarioprojeto.projeto projeto")
				.where("usuario = ?",usuario)
				.unique();
	}

	/**
	 * M�todo que realiza busca geral nos usu�rios de acordo com o par�metro da pesquisa.
	 * 
	 * @param busca
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Usuario> findUsuariosForBuscaGeral(String busca) {
		return querySined()
			
			.select("usuario.cdpessoa, usuario.nome")
			.openParentheses()
				.whereLikeIgnoreAll("usuario.nome", busca).or()
				.whereLikeIgnoreAll("usuario.email", busca).or()
				.whereLikeIgnoreAll("usuario.apelido", busca)
			.closeParentheses()
			.orderBy("usuario.nome")
			.list();
	}
	
	/**
	 * 
	 * M�todo que busca o usu�rio com o seus n�veis de permiss�es
	 *
	 *@author Thiago Augusto
	 *@date 21/03/2012
	 * @param usuario
	 * @return
	 */
	public Usuario getUsuarioComPapel(Usuario usuario){
		if(usuario == null || usuario.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		return querySined()
					
					.select("usuario.cdpessoa, listaUsuariopapel.cdusuariopapel, papel.cdpapel, papel.nome, papel.administrador")
					.leftOuterJoin("usuario.listaUsuariopapel listaUsuariopapel")
					.leftOuterJoin("listaUsuariopapel.papel papel")
					.where("usuario.cdpessoa = ?", usuario.getCdpessoa())
					.unique();
	}

	/**
	 * M�todo que busca o valor limite de aprova��o e autoriza��o do usu�rio
	 *
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getLimitevaloraprovacaoautorizacao(Usuario usuario) {
		if(usuario == null || usuario.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Long> query = newQueryBuilder(Long.class)
			.from(Usuario.class)
			.select("SUM(usuario.limitevaloraprovacaoautorizacao)")
			.where("usuario.cdpessoa = ?", usuario.getCdpessoa());
		
		Long limitevalorautorizacaoaprovacao = query.setUseTranslator(false).unique();
		
		if (limitevalorautorizacaoaprovacao != null)
			return new Money(limitevalorautorizacaoaprovacao, true);
		else 
			return new Money();
	}
	
	/**
	 * M�todo que verifica se o usu�rio tem restri��o de cliente
	 *
	 * @param usuario
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isRestricaoClienteVendedor(Usuario usuario){
		if(usuario == null || usuario.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Usuario.class)
				.setUseTranslator(false)
				.where("usuario.cdpessoa = ?", usuario.getCdpessoa())
				.where("usuario.restricaoclientevendedor = true")
				.unique() > 0;
	}

	/**
	 * M�todo utilizado para buscar a informa��o se usu�rio tem restri��o clientevendedor
	 * (retorna null para proseguir com a consulta do autocomplete)
	 * 
	 *
	 * @param usuario
	 * @return
	 * @author Luiz Fernando
	 */
	public Usuario findForVerificarRestricaoClientevendedor(Usuario usuario) {
		if(usuario == null || usuario.getCdpessoa() == null)
			return null;
		
		return querySined()
				
				.select("usuario.cdpessoa, usuario.restricaoclientevendedor ")
				.where("usuario = ?", usuario)
				.unique();
	}
	/**
	 * M�todo para setar o usu�rio como bloqueado.
	 * @param usuario
	 * @author Thiers Euller
	 */
	public void updateBloqueado(Integer cdpessoa){
		if(cdpessoa == null){
			throw new SinedException("Par�metro cdpessoa n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("update Usuario set bloqueado = true where cdpessoa = " + cdpessoa);
	}
	
	/**
	 * M�todo que busca os usu�rios para envio de email de acordo com os par�metros
	 *
	 * @param keyacao
	 * @param empresa
	 * @param whereInProjeto
	 * @param whereInCdusuario
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Usuario> findForEnvioemailcompra(String keyacao, Empresa empresa, String whereInProjeto, String whereInCdusuario, String notWhereInCdusuario) {
		if(keyacao == null || "".equals(keyacao))
			throw new SinedException("Par�metro A��O n�o pode ser nulo.");
		
		QueryBuilder<Usuario> query = querySined()
				
				.select("usuario.cdpessoa, usuario.nome, usuario.email, usuario.bloqueado")
				.join("usuario.listaUsuariopapel listaUsuariopapel")
				.join("listaUsuariopapel.papel papel")
				.leftOuterJoin("papel.listaAcaopapel listaAcaopapel")
				.leftOuterJoin("listaAcaopapel.acao acao")
				.leftOuterJoin("usuario.listaUsuarioempresa listaUsuarioempresa")
				.leftOuterJoin("listaUsuarioempresa.empresa empresa")
				.leftOuterJoin("usuario.listaUsuarioprojeto listaUsuarioprojeto")
				.leftOuterJoin("listaUsuarioprojeto.projeto projeto")
				.where("acao.key = ?", keyacao);
		
		if(empresa != null && empresa.getCdpessoa() != null){
			query.where("empresa = ?", empresa);
		}
		
		if(whereInProjeto != null && !"".equals(whereInProjeto)){
			query.openParentheses()
				.whereIn("projeto.cdprojeto", whereInProjeto)
				.or()
				.where("coalesce(usuario.todosprojetos, false) = true")
			.closeParentheses();
		}
		
		if(whereInCdusuario != null && !"".equals(whereInCdusuario)){
			query
				.whereIn("usuario.cdpessoa", whereInCdusuario);
		}
		
		if (notWhereInCdusuario != null && !"".equals(notWhereInCdusuario)) {
			query
				.whereIn("usuario.cdpessoa", notWhereInCdusuario);
		}
		
		return query
				.openParentheses()
					.where("papel.administrador is null")
					.or()
					.where("papel.administrador = false")
				.closeParentheses()
				.list();
		
	}

	/**
	 * M�todo que retorna se o usu�rio tem restri��o de venda/vendedor
	 *
	 * @param usuario
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isRestricaoVendaVendedor(Usuario usuario) {
		if(usuario == null || usuario.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Usuario.class)
				.setUseTranslator(false)
				.where("usuario.cdpessoa = ?", usuario.getCdpessoa())
				.where("usuario.restricaovendavendedor = true")
				.unique() > 0;
	}
	
	/**
	 * M�todo que verifica se usu�rio tem permiss�o para uma determinada acao
	 *
	 * @param usuario
	 * @param acao
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean permiteAcao(Usuario usuario, String acao){
		if(acao == null || "".equals(acao)) return false;
		
		String sql = "select count(*) from usuariopapel up join papel p on (up.cdpapel = p.cdpapel) " +
				"join acaopapel ap on (p.cdpapel = ap.cdpapel) join acao a on (ap.cdacao = a.cdacao) " +
				"where a.key = '" + acao + "' and ap.permitido is TRUE and up.cdpessoa = " +
				usuario.getCdpessoa();

		SinedUtil.markAsReader();
		Integer count = getJdbcTemplate().queryForInt(sql.toString());
		
		if(count != null && count > 0) return true;
		return false;
	}

	/**
	 * M�todo que retorna true caso o usu�rio tenha a marca��o de sempre logar offline
	 *
	 * @param usuarioLogado
	 * @return
	 * @author Luiz Fernando
	 * @since 20/01/2014
	 */
	public Boolean isSemprelogaroffline(Usuario usuarioLogado) {
		if(usuarioLogado == null || usuarioLogado.getCdpessoa() == null)
			return false;
		
		SinedUtil.markAsReader();
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Usuario.class)
			.setUseTranslator(false)
			.where("usuario.cdpessoa = ?", usuarioLogado.getCdpessoa())
			.where("usuario.semprelogaroffline = true")
			.unique() > 0;
	}
	
	/**
	 * M�todo que retorna true caso o usu�rio tenha a marca��o de sempre logar offline
	 *
	 * @param login
	 * @return
	 * @author Luiz Fernando
	 * @since 22/01/2014
	 */
	public Boolean isSemprelogaroffline(String login) {
		if(login == null || "".equals(login))
			return false;
		
		SinedUtil.markAsReader();
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Usuario.class)
			.setUseTranslator(false)
			.where("usuario.login = ?", login)
			.where("usuario.semprelogaroffline = true")
			.unique() > 0;
	}
	
	/**
	 * M�todo que verifica se o usu�rio tem restri��o de fornecedor
	 *
	 * @param usuario
	 * @return
	 * @author Rafael Salvio
	 */
	public Boolean isRestricaoFornecedorColaborador(Usuario usuario){
		if(usuario == null || usuario.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		SinedUtil.markAsReader();
		return newQueryBuilderSined(Long.class)
				
				.select("count(*)")
				.from(Usuario.class)
				.setUseTranslator(false)
				.where("usuario.cdpessoa = ?", usuario.getCdpessoa())
				.where("usuario.restricaoFornecedorColaborador = true")
				.unique() > 0;
	}
	
	public List<Usuario> findForAndroid(String whereInUsuario){
		return querySined()
			
			.select("usuario.cdpessoa, usuario.nome, usuario.login, usuario.restricaoclientevendedor, usuario.restricaovendavendedor")
			.whereIn("usuario.cdpessoa", whereInUsuario)
			.list();
	}

	/**
	 * Verifica se o usu�rio logado possui permiss�o em uma determinada empresa
	 * @param empresa
	 * @return
	 */
	public boolean possuiPermissaoNaEmpresa(Empresa empresa) {
		if(empresa==null || empresa.getCdpessoa()==null)
			throw new RuntimeException("Par�metro empresa n�o pode ser nulo.");
		
		return querySined()
				
				.select("usuario.cdpessoa")
				.join("usuario.listaUsuarioempresa usuarioempresa")
				.where("usuario = ?", SinedUtil.getUsuarioLogado())
				.where("usuarioempresa.empresa = ?", empresa)
				.list().size()>0;
	}
	
	
	/**
	 * M�todo que busca os usu�rios a serem enviados para o W3Producao.
	 * @param whereIn
	 * @return
	 */
	public List<Usuario> findForW3Producao(String whereIn){
		return querySined()
					
					.select("usuario.cdpessoa, usuario.nome, usuario.login, usuario.senha, usuario.senhaterminal")
					.whereIn("usuario.cdpessoa", whereIn)
					.list();
	}

	/**
	 * Query para o autocomplete somente de usu�rios n�o bloquedos
	 * 
	 * @param q
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/03/2016
	 */
	public List<Usuario> findForAutocompleteNotBloqueados(String q) {
		return querySined()
					
					.select("usuario.cdpessoa, usuario.nome")
					.whereLikeIgnoreAll("usuario.nome", q)
					.openParentheses()
						.where("usuario.bloqueado = ?", Boolean.FALSE)
						.or()
						.where("usuario.bloqueado is null")
					.closeParentheses()
					.autocomplete()
					.orderBy("upper(usuario.nome)")
					.list();
	}
	public Usuario findById(Integer idExterno) {
		if(idExterno == null){
			throw new RuntimeException("Par�metros inv�lidos em usuario.");
		}
		return querySined()
					
					.select("usuario.cdpessoa, usuario.bloqueado, usuario.login, usuario.senha, usuario.nome, usuario.email, usuario.cpf, usuario.cnpj, usuario.tipopessoa")
					.where("usuario.id = ?",idExterno)
					.unique();
	}

	public List<Usuario> findForAviso(String whereInProjeto, String whereInEmpresa) {
		QueryBuilder<Usuario> query = querySined()
				
				.select("usuario.cdpessoa, usuario.nome")
				.leftOuterJoin("usuario.listaUsuarioempresa listaUsuarioempresa")
				.leftOuterJoin("usuario.listaUsuarioprojeto listaUsuarioprojeto")
				.leftOuterJoin("listaUsuarioprojeto.projeto projeto")
				.where("coalesce(usuario.bloqueado, true) = false");
		
		if(StringUtils.isNotBlank(whereInEmpresa)){
			query.whereIn("listaUsuarioempresa.empresa.cdpessoa", whereInEmpresa);
		}
		
		if(StringUtils.isNotBlank(whereInProjeto)){
			query.openParentheses()
				.where("coalesce(usuario.todosprojetos, false) = true")
				.or()
				.whereIn("projeto.cdprojeto", whereInProjeto)
			.closeParentheses();
		}
		
		return query.list();
	}
	
	public Boolean isVisualizaOutrasOportunidades(Usuario usuario){
		if(usuario == null || usuario.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		SinedUtil.markAsReader();
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Usuario.class)
				.setUseTranslator(false)
				.where("usuario.cdpessoa = ?", usuario.getCdpessoa())
				.where("usuario.visualizaroutrasoportunidades = true")
				.unique() > 0;
	}

	public boolean existeSenhaTerminal(Usuario usuario, String senhaterminal) {
		if(StringUtils.isBlank(senhaterminal)) return false;
		
		SinedUtil.markAsReader();
		return newQueryBuilderSined(Long.class)
				.removeUseWhere()
				.select("count(*)")
				.from(Usuario.class)
				.where("usuario <> ?", usuario, usuario != null && usuario.getCdpessoa() != null)
				.where("senhaterminal = ?", senhaterminal)
				.unique().longValue() > 0;
	}

	public Usuario findByCdpessoa(Integer cdpessoa) {
		
		return querySined()
				
				.where("usuario.cdpessoa = ?", cdpessoa)
				.unique();
	}
}