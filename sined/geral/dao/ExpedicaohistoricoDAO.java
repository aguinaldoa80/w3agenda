package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.Expedicaohistorico;
import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaoacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ExpedicaohistoricoDAO extends GenericDAO<Expedicaohistorico> {

	public Expedicaoacao ultimaAcao(Expedicao expedicao, Expedicaoacao... acoes){
		List<Integer> lista = new ArrayList<Integer>();
		for (int i = 0; i < acoes.length; i++) {
			lista.add(acoes[i].getValue());
		}
		Expedicaohistorico historico = querySined()
											.select("expedicaohistorico.expedicaoacao")
											.join("expedicaohistorico.expedicao expedicao")
											.where("expedicao = ?", expedicao)
											.whereIn("expedicaohistorico.expedicaoacao", CollectionsUtil.concatenate(lista, ","))
											.orderBy("expedicaohistorico.dtaltera desc")
											.unique();
		
		return historico != null? historico.getExpedicaoacao(): null;
	}

	public Expedicaohistorico getLastExpedicaohistoricoEmconferencia(Expedicaoitem expedicaoitem) {
		return querySined()
				.setMaxResults(1)
				.select("expedicaohistorico.cdexpedicaohistorico, expedicaohistorico.expedicaoacao")
				.openParentheses()
					.where("expedicaohistorico.expedicaoacao = ?", Expedicaoacao.CONFERENCIA_REALIZADA)
					.or()
					.where("expedicaohistorico.expedicaoacao = ?", Expedicaoacao.FINALIZAR_SEM_CONFERENCIA)
				.closeParentheses()
				.where("not exists(" +
							"select eh.cdexpedicaohistorico " +
							"from Expedicaohistorico eh " +
							"where eh.expedicao = expedicaohistorico.expedicao " +
							"and eh.cdexpedicaohistorico > expedicaohistorico.cdexpedicaohistorico " +
							"and eh.expedicaoacao = ?" +
						")", Expedicaoacao.CONFERENCIA_FINALIZADA)
				.where("exists(select 1 from ConferenciaMaterialExpedicao cme "+
							"   where cme.expedicaoHistorico = expedicaohistorico "+
							"	and cme.expedicaoitem = ?)", expedicaoitem)
				.orderBy("expedicaohistorico.cdexpedicaohistorico desc")
				.unique();
	}
}
