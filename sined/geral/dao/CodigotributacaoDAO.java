package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.BeanDescriptor;
import br.com.linkcom.neo.bean.BeanDescriptorFactoryImpl;
import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Codigotributacao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CodigotributacaoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CodigotributacaoDAO extends GenericDAO<Codigotributacao> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Codigotributacao> query, FiltroListagem _filtro) {
		CodigotributacaoFiltro filtro = (CodigotributacaoFiltro) _filtro;
		
		query
			.select("codigotributacao.cdcodigotributacao, codigotributacao.codigo, codigotributacao.descricao, " +
					"municipio.nome, uf.sigla, codigotributacao.ativo")
			.leftOuterJoin("codigotributacao.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.whereLikeIgnoreAll("codigotributacao.codigo", filtro.getCodigo())
			.whereLikeIgnoreAll("codigotributacao.descricao", filtro.getDescricao())
			.where("municipio = ?", filtro.getMunicipio())
			.where("uf = ?", filtro.getUf())
			.where("codigotributacao.ativo = ?", filtro.getAtivo())
			;
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Codigotributacao> query) {
		query
			.leftOuterJoinFetch("codigotributacao.listaCodigotributacaoitemlistaservico listaCodigotributacaoitemlistaservico")
			.leftOuterJoinFetch("codigotributacao.municipio municipio")
			.leftOuterJoinFetch("municipio.uf uf");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaCodigotributacaoitemlistaservico");
	}
	
	/**
	 * Carrega o codigo de tributa��o a partir do c�digo.
	 *
	 * @param codigo
	 * @return
	 * @since 30/03/2012
	 * @author Rodrigo Freitas
	 */
	public Codigotributacao loadByCodigo(String codigo) {
		return query()
					.select("codigotributacao.cdcodigotributacao, codigotributacao.codigo, codigotributacao.descricao")
					.where("translate(codigotributacao.codigo, '.-/', '') like ?", br.com.linkcom.neo.util.StringUtils.stringCheia(br.com.linkcom.neo.util.StringUtils.soNumero(codigo), "0", 9, false))
					.unique();
	}
	
	/**
	 * M�todo que busca o codigotributacao a partir do c�digo.
	 *
	 * @param codigo
	 * @return
	 * @author Luiz Fernando
	 */
	public Codigotributacao findByCodigo(String codigo) {
		return query()
					.select("codigotributacao.cdcodigotributacao, codigotributacao.codigo, codigotributacao.descricao")
					.where("codigotributacao.codigo = ?", codigo)
					.unique();
	}
	
	@Override
	public List<Codigotributacao> findForAutocomplete(String q, String propertyMatch, String propertyLabel, Boolean matchOption) {
		
		String alias = Util.strings.uncaptalize(beanClass.getSimpleName());
		BeanDescriptor<Codigotributacao> beanDescriptor = new BeanDescriptorFactoryImpl().createBeanDescriptor(null, beanClass);
		
		String[] labelPropertyName;
		String[] matchPropertyName;
		
		if(propertyMatch != null && !propertyMatch.equals("")){
			matchPropertyName = propertyMatch.split(",");
		} else {
			matchPropertyName = new String[]{alias + "." + beanDescriptor.getDescriptionPropertyName()};
		}
		
		if(propertyLabel != null && !propertyLabel.equals("")){
			labelPropertyName = propertyLabel.split(",");
		} else {
			labelPropertyName = new String[]{alias + "." + beanDescriptor.getDescriptionPropertyName()};
		}
		
		String idPropertyName = alias + "." + beanDescriptor.getIdPropertyName();
		
		QueryBuilder<Codigotributacao> query = query().select(idPropertyName + "," + StringUtils.join(labelPropertyName, ","));
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		
		query.openParentheses();
		for (int i = 0; i < matchPropertyName.length; i++) {
			if(matchOption){
				query
					.whereLikeIgnoreAll(matchPropertyName[i], q)
					.or();
			} else {
				if (funcaoTiraacento != null) {
					query.where("UPPER(" + funcaoTiraacento + "(" + matchPropertyName[i] + ")) LIKE ?||'%'", Util.strings.tiraAcento(q).toUpperCase());
				}
				else {
					query.where("UPPER(" + matchPropertyName[i] + ") LIKE ?||'%'", Util.strings.tiraAcento(q).toUpperCase());
				}
				query.or();
			}
		}		
		query.closeParentheses();
		
		query.setMaxResults(300);//Valor atual no arquivo neo.autocomplete.js
		
		query.where(alias + ".ativo = ?", Boolean.TRUE);
		
		return query.orderBy(StringUtils.join(labelPropertyName, ",")).list();	
	}

	/**
	 * Query para o autocomplete com munic�pio.
	 *
	 * @param s
	 * @return
	 * @since 25/09/2012
	 * @author Rodrigo Freitas
	 */
	public List<Codigotributacao> findForAutocompleteWithMunicipio(String s) {
		return query()
					.select("codigotributacao.cdcodigotributacao, codigotributacao.codigo, " +
							"codigotributacao.descricao, municipio.nome, uf.sigla")
					.join("codigotributacao.municipio municipio")
					.join("municipio.uf uf")
					.openParentheses()
					.whereLikeIgnoreAll("codigotributacao.codigo", s)
					.or()
					.whereLikeIgnoreAll("codigotributacao.descricao", s)
					.closeParentheses()
					.list();
	}

	@Override
	public ListagemResult<Codigotributacao> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Codigotributacao> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("codigotributacao.codigo");
		
		return new ListagemResult<Codigotributacao>(query, filtro);
	}
}
