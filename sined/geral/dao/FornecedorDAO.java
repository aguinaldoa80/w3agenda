
package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.FornecedorTipoEnum;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.PessoaContatoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.DominiointegracaoFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.FornecedorFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

@DefaultOrderBy("fornecedor.nome")
public class FornecedorDAO extends GenericDAO<Fornecedor>{

	
	/* singleton */
	private static FornecedorDAO instance;
	public static FornecedorDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(FornecedorDAO.class);
		}
		return instance;
	}
	
	private UsuarioService usuarioService;
	private PessoaContatoService pessoaContatoService;
	
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	public void setPessoaContatoService(PessoaContatoService pessoaContatoService) {
		this.pessoaContatoService = pessoaContatoService;
	}
	
	/**
	* M�todo que adiciona a condi��o de restri��o de fornecedor por empresa
	*
	* @param query
	* @param whereInEmpresa
	* @since 04/11/2016
	* @author Luiz Fernando
	*/
	private void addWhereInFornecedorByEmpresa(QueryBuilder<Fornecedor> query, String whereInEmpresa){
		if(StringUtils.isNotBlank(whereInEmpresa)){
			query
				.leftOuterJoin("fornecedor.listaFornecedorempresa listaFornecedorempresa")
				.leftOuterJoin("listaFornecedorempresa.empresa empresa")
				.openParentheses()
					.where("empresa is null")
					.or()
					.whereIn("empresa.cdpessoa", whereInEmpresa)
				.closeParentheses();
		}
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Fornecedor> query,FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		}
		FornecedorFiltro filtro = (FornecedorFiltro)_filtro;

		query
			.select("distinct fornecedor.cdpessoa, fornecedor.nome, fornecedor.ativo, fornecedor.tipopessoa ")
			.leftOuterJoin("fornecedor.listaPessoacategoria listaPessoacategoria")
			.leftOuterJoin("listaPessoacategoria.categoria categoria")
			.leftOuterJoin("categoria.vcategoria vcategoria")
			.leftOuterJoin("fornecedor.listaQuestionario listaQuestionario")
			.leftOuterJoin("fornecedor.listaPessoamaterial listaPessoamaterial")
			.where("fornecedor.cnpj LIKE ''||?||'%'", filtro.getNumeromatriz())
			.where("fornecedor.cnpj = ?", filtro.getCnpj())
			.where("fornecedor.cpf = ?", filtro.getCpf())		
			.where("fornecedor.ativo = ?",filtro.getAtivo())
			.whereLikeIgnoreAll("fornecedor.nome", filtro.getNomefantasia())
			.whereLikeIgnoreAll("fornecedor.razaosocial", filtro.getRazaosocial())
			.where("listaQuestionario.dtaltera>=?", filtro.getDtAvaliacaoInicio())
			.where("listaQuestionario.dtaltera<=?", filtro.getDtAvaliacaoFim())
			.ignoreJoin("listaPessoacategoria", "categoria", "vcategoria", "listaQuestionario");
		
		if(filtro.getMunicipio()!=null || filtro.getUf()!=null || StringUtils.isNotBlank(filtro.getBairro()) 
			|| StringUtils.isNotBlank(filtro.getLogradouro())){
			
			query.leftOuterJoin("fornecedor.listaEndereco endereco")
			.where("endereco.municipio=?", filtro.getMunicipio())
			.whereLikeIgnoreAll("endereco.logradouro", filtro.getLogradouro())
			.whereLikeIgnoreAll("endereco.bairro",filtro.getBairro())
			.ignoreJoin("listaEndereco");
			
			if(filtro.getUf()!=null){
				query.leftOuterJoin("endereco.municipio municipio")
				.where("municipio.uf=?", filtro.getUf());
			}
		}
		
		if(StringUtils.isNotBlank(filtro.getTelefone())){
			query.leftOuterJoin("fornecedor.listaTelefone telefone")
			.whereLikeIgnoreAll("telefone.telefone", filtro.getTelefone())
			.ignoreJoin("listaTelefone");
		}
		
		if(filtro.getFornecedorTipoEnum() != null){
			if(FornecedorTipoEnum.FABRICANTE.equals(filtro.getFornecedorTipoEnum())){
				query.where("fornecedor.fabricante is true");
			}
			if(FornecedorTipoEnum.TRANSPORTADOR.equals(filtro.getFornecedorTipoEnum())){
				query.where("fornecedor.transportador is true");
			}
		}
		
		if (filtro.getCategoria() != null && filtro.getCategoria().getCdcategoria() != null){
			Categoria categoriaAux = filtro.getCategoria();
			if (categoriaAux.getVcategoria() == null || categoriaAux.getVcategoria().getIdentificador() == null){
				categoriaAux = CategoriaService.getInstance().loadWithIdentificador(categoriaAux);
			}
			
			query.where("vcategoria.identificador like ?||'%'", categoriaAux.getVcategoria().getIdentificador());
		}
		
		if(filtro.getTipopessoa() != null && filtro.getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)){
			query.where("fornecedor.tipopessoa = 0");
		}
		if(filtro.getTipopessoa() != null && filtro.getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA)){
			query.where("fornecedor.tipopessoa = 1");
		}
		
		if(!StringUtils.isEmpty(filtro.getContato())){
			query
				.leftOuterJoin("fornecedor.listaContato listaContato")
				.leftOuterJoin("listaContato.contato contato")
				.whereLikeIgnoreAll("contato.nome", filtro.getContato())
				.ignoreJoin("contato");
		}
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioService.isRestricaoFornecedorColaborador(usuarioLogado)){
			query.where("("+
							"exists("+
								"select 1 from ColaboradorFornecedor cf "+
								"where "+
								"cf.fornecedor =  fornecedor.cdpessoa and "+
								"cf.colaborador = "+usuarioLogado.getCdpessoa()+
							") "+
							"or "+
							"not exists("+
								"select 1 from ColaboradorFornecedor cf "+
								"where "+
								"cf.fornecedor =  fornecedor.cdpessoa"+
							")"+
						")");
		}
		
		query.where("coalesce(fornecedor.associacao, false)=?", filtro.getAssociacao());
		query.where("coalesce(fornecedor.captador, false)=?", filtro.getCaptador());
		query.where("fornecedor.captadorassociacao=?", filtro.getCaptadorassociacao());
		query.where("fornecedor.sexo=?", filtro.getSexo());
		query.where("fornecedor.dtnascimento=?", filtro.getDtnascimento());
		
		if (StringUtils.isEmpty(filtro.getOrderBy())) {
			query.orderBy("fornecedor.nome");
		}
	}
	
	/**
	 * Carrega informa��es para a listagem de fornecedor.
	 *
	 * @param whereIn
	 * @param orderby
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Fornecedor> findListagem(String whereIn, String orderby, boolean asc){
		if (whereIn == null || whereIn.equals("")) {
			return new ArrayList<Fornecedor>();
		}
		QueryBuilder<Fornecedor> query = querySined()
									.select("fornecedor.cdpessoa,fornecedor.cnpj, fornecedor.cpf ,fornecedor.nome," +
											"fornecedor.ativo,fornecedor.tipopessoa,categoria.nome")
									.leftOuterJoin("fornecedor.listaPessoacategoria listaPessoacategoria")
									.leftOuterJoin("listaPessoacategoria.categoria categoria")
									.whereIn("fornecedor.cdpessoa", whereIn);
									
		
		if (!StringUtils.isEmpty(orderby)) {
			query.orderBy(orderby+" "+(asc?"ASC":"DESC") + ", categoria.nome");
		} else {
			query.orderBy("fornecedor.nome, categoria.nome");
		}
		
		return query.list();
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Fornecedor> query) {
		((QueryBuilderSined<Fornecedor>) query).setUseWhereFornecedorEmpresa(true);
		
		query.leftOuterJoinFetch("fornecedor.listaDadobancario dadobancario");
		query.leftOuterJoinFetch("dadobancario.banco banco");
		
		query.leftOuterJoinFetch("fornecedor.listaEndereco listaEndereco");
		query.leftOuterJoinFetch("listaEndereco.municipio municipioEnd");
		query.leftOuterJoinFetch("municipioEnd.uf ufEnd");
		
		query.leftOuterJoinFetch("fornecedor.listaTelefone listaTelefone");
		query.leftOuterJoinFetch("fornecedor.listaRestricao listaRestricao");
		
		query.leftOuterJoinFetch("fornecedor.listaPessoacategoria listaPessoacategoria");
		query.leftOuterJoinFetch("listaPessoacategoria.categoria categoria");
		query.leftOuterJoinFetch("categoria.vcategoria vcategoria");
		
		query.leftOuterJoinFetch("fornecedor.contagerencial");
		query.leftOuterJoinFetch("fornecedor.contagerencialcredito");
		query.leftOuterJoinFetch("fornecedor.contaContabil");
		
		query.leftOuterJoinFetch("fornecedor.naturezaoperacao naturezaoperacao");
		
		query.leftOuterJoinFetch("fornecedor.operacaocontabil operacaocontabil");
		query.leftOuterJoinFetch("fornecedor.captadorassociacao captadorassociacao");
		query.leftOuterJoinFetch("fornecedor.sexo sexo");
//		query.leftOuterJoinFetch("fornecedor.listaColaboradorFornecedor listaColaboradorFornecedor");
//		query.leftOuterJoinFetch("listaColaboradorFornecedor.colaborador colaborador");
	}  
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaPessoadap");
		save.saveOrUpdateManaged("listaPessoacategoria");
		save.saveOrUpdateManaged("listaDadobancario");
		save.saveOrUpdateManaged("listaEndereco");
		save.saveOrUpdateManaged("listaTelefone");
		save.saveOrUpdateManaged("listaRestricao");
		save.saveOrUpdateManaged("listaColaboradorFornecedor");
		save.saveOrUpdateManaged("listaPessoamaterial");
		save.saveOrUpdateManaged("listaFornecedorempresa");
	}	
	
	@Override
	public void saveOrUpdate(final Fornecedor fornecedor) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {

				List<PessoaContato> listaPessoaContato = fornecedor.getListaContato();
				
				saveOrUpdateNoUseTransaction(fornecedor);
				
				pessoaContatoService.saveOrUpdateListaPessoaContato(fornecedor, listaPessoaContato);
				
				return null;
			}
		});
	}

	/**
	 * M�todo para carregar dados de fornecedor.
	 * 
	 * @param bean
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Fornecedor carregaFornecedor(Fornecedor bean){
		if(!SinedUtil.isObjectValid(bean)){
			throw new SinedException("Par�metro fornecedor inv�lido.");
		}
		
		return querySined()
			.select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.identificador, fornecedor.razaosocial, fornecedor.cnpj, fornecedor.cpf, fornecedor.transportador, fornecedor.correios," +
					"fornecedor.valorfrete, fornecedor.inscricaoestadual, fornecedor.tipopessoa, fornecedor.email, " +
					"dadobancario.cddadobancario, dadobancario.agencia, dadobancario.conta, dadobancario.dvagencia, dadobancario.dvconta," +
					"dadobancario.operacao, banco.cdbanco, banco.nome, banco.numero, tipoconta.cdtipoconta, tipoconta.nome, " +
					"endereco.cdendereco, endereco.cep, endereco.complemento, endereco.numero, endereco.logradouro, endereco.bairro, " +
					"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.nome, uf.sigla, pais.cdpais, pais.nome, enderecotipo.cdenderecotipo, " +
					"telefone.telefone, telefonetipo.cdtelefonetipo")
					
			.leftOuterJoin("fornecedor.listaDadobancario dadobancario")
			.leftOuterJoin("dadobancario.banco banco")
			.leftOuterJoin("dadobancario.tipoconta tipoconta")
			.leftOuterJoin("fornecedor.listaTelefone telefone")
			.leftOuterJoin("telefone.telefonetipo telefonetipo")
			.leftOuterJoin("fornecedor.listaEndereco endereco")
			.leftOuterJoin("endereco.enderecotipo enderecotipo")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("endereco.pais pais")
			
			.where("fornecedor = ?", bean)
			.unique();
		
	}
	
	public Fornecedor carregaTiketMedio(Fornecedor bean){
		if(!SinedUtil.isObjectValid(bean)){
			//throw new SinedException("Par�metro fornecedor inv�lido.");
			return bean;
		}
		
		return querySined()
			.select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.identificador, fornecedor.valorTicketMedio")
			.where("fornecedor = ?", bean)
			.unique();
		
	}
	
	/**
	 * M�todo para obter o n�mero de fornecedores cadastrados com o cnpj.
	 *
	 * @param bean
	 * @return
	 * @author Flavio Tavares
	 */
	public Integer countFornecedorCnpj(Fornecedor bean){
		if(bean==null){
			throw new SinedException("O par�metro bean n�o pode ser null.");
		}
		SinedUtil.markAsReader();
		return newQueryBuilderWithFrom(Integer.class)
			.select("count(*)")
			.where("cdpessoa<>?",bean.getCdpessoa())
			.where("cnpj=?",bean.getCnpj())
			.unique();
	}

	/**
	 * Deleta somente o registro na tabela fornecedor. Conserva o da tabela pessoa.
	 * Usado para excluir um fornecedor que tamb�m � cliente.
	 *
	 * @param fornecedor
	 * @author Fl�vio Tavares
	 */
	public void deleteOnlyFornecedor(Fornecedor fornecedor){
		if(fornecedor==null){
			throw new SinedException("O par�metro fornecedor n�o pode ser null.");
		}
		String delete = "delete from fornecedor where cdpessoa = ?";
		Object[] campo = new Object[]{fornecedor.getCdpessoa()};
		getJdbcTemplate().update(delete, campo);

	}
	
	/**
	 * Renorna uma lista de fornecedor contendo apenas o campo Nome
	 * 
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Fornecedor> findDescricao(String whereIn) {
		return querySined()
			.select("fornecedor.nome")
			.whereIn("fornecedor.cdpessoa", whereIn)
			.list();
	}
	
	/**
	 * Verifica se algum fornecedor tem o login do usuario passado por par�metro.
	 *
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean exiteFornecedorLogin(Usuario bean) {
		if (bean == null || bean.getLogin() == null) {
			throw new SinedException("Usu�rio n�o pode ser nulo.");
		}
		SinedUtil.markAsReader();
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Fornecedor.class)
					.setUseTranslator(false)
					.where("fornecedor.login = ?",bean.getLogin())
					.unique() > 0;
	}
	
	/**
	 * M�todo que carrega os fornecedores de uma categoria espec�fica.
	 * Se n�o for passada nenhuma categoria, retorna todos os registros.
	 * 
	 * @param categoria
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Fornecedor> procurarFornecedoresPorCategoria(Categoria categoria) {
		return querySined()
			.select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.ativo")
			.leftOuterJoin("fornecedor.listaPessoacategoria listaPessoacategoria")
			.leftOuterJoin("listaPessoacategoria.categoria categoria")
			.where("fornecedor.ativo = ?")
			.where("categoria = ?", categoria)
			.list();
	}

	/**
	 * Carrega informa��es do boleto.
	 *
	 * @param cdpessoa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Fornecedor loadForBoleto(Integer cdpessoa) {
		return querySined()
					.select("fornecedor.nome, fornecedor.razaosocial, fornecedor.cpf, fornecedor.cnpj, " +
							"endereco.logradouro, endereco.numero, endereco.bairro, endereco.complemento, " +
							"municipio.nome, uf.sigla, endereco.cep, endereco.caixapostal, endereco.cdendereco")
					.leftOuterJoin("fornecedor.listaEndereco endereco")
					.leftOuterJoin("endereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.where("fornecedor.cdpessoa = ?", cdpessoa)
					.unique();
	}

	/**
	 * M�todo que realiza busca geral nos fornecedores de acordo com o par�metro de busca
	 * 
	 * @param busca
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Fornecedor> findFornecedoresForBuscaGeral(String busca) {
		return querySined()
			.setUseWhereFornecedorEmpresa(true)
			.select("fornecedor.cdpessoa, fornecedor.nome, contato.cdpessoa, contato.nome")
			.leftOuterJoin("fornecedor.listaContato listaContato")
			.leftOuterJoin("listaContato.contato contato")
			
			.openParentheses()
				.whereLikeIgnoreAll("fornecedor.razaosocial", busca).or()
				.whereLikeIgnoreAll("fornecedor.nome", busca).or()
				.whereLikeIgnoreAll("fornecedor.email", busca).or()
				.whereLikeIgnoreAll("contato.nome", busca).or()
				.whereLikeIgnoreAll("contato.email", busca).or()
				.whereLikeIgnoreAll("contato.observacao", busca).or()
				.whereLikeIgnoreAll("fornecedor.observacao", busca)
			.closeParentheses()
			.orderBy("fornecedor.nome")
			.list();
	}

	/**
	 * M�todo que busca de acordo com autocomplete
	 * @param whereLike
	 * @return
	 */
	public List<Fornecedor> findFornecedoresAutoComplete(String whereLike) {
		return querySined()
			.setUseWhereFornecedorEmpresa(true)
			.select("fornecedor.cdpessoa, fornecedor.identificador, fornecedor.nome")
			.whereLikeIgnoreAll("fornecedor.nome", whereLike)
			.where("fornecedor.ativo = ?",Boolean.TRUE)
			.orderBy("fornecedor.nome")
			.list();
	}

	/**
	 * M�todo que busca de acordo com autocomplete
	 * @param whereLike
	 * @return
	 */
	public List<Fornecedor> findAllFornecedoresAutoComplete(String whereLike) {
		return querySined()
			
			.setUseWhereFornecedorEmpresa(true)
			.select("fornecedor.cdpessoa, fornecedor.identificador, fornecedor.nome")
			.whereLikeIgnoreAll("fornecedor.nome", whereLike)
			.orderBy("fornecedor.nome")
			.list();
	}
	
	/**
	 * M�todo que busca fornecedores aniversariantes do m�s
	 * 
	 * @param mes
	 * @return
	 * @authro Tom�s Rabelo
	 */
	@SuppressWarnings("unchecked")
	public List<Fornecedor> findFornecedoresAniversariantesDoMesFlex(Integer mes) {
		String sql = "SELECT \"CDPESSOA\", \"NOME\", \"DTNASCIMENTO\" FROM ("+
					 "SELECT F.CDPESSOA AS \"CDPESSOA\", P.NOME AS \"NOME\", P.DTNASCIMENTO AS \"DTNASCIMENTO\" "+
					 "FROM FORNECEDOR F " +
					 "JOIN PESSOA P ON P.CDPESSOA = F.CDPESSOA "+
					 "WHERE EXTRACT(MONTH FROM P.DTNASCIMENTO) = "+mes+" " +
					 
					 "UNION " +
			
					"SELECT CO.CDPESSOA AS \"CDPESSOA\", P.NOME || '\n  ' || COALESCE(F2.RAZAOSOCIAL, P2.NOME) AS \"NOME\", P.DTNASCIMENTO AS \"DTNASCIMENTO\" "+
					"FROM CONTATO CO "+
					"JOIN PESSOA P ON P.CDPESSOA = CO.CDPESSOA " +
					"JOIN PESSOACONTATO PC ON PC.CDCONTATO = CO.CDPESSOA " +
					"JOIN FORNECEDOR F2 ON F2.CDPESSOA = PC.CDPESSOA " +
					"JOIN PESSOA P2 ON P2.CDPESSOA = PC.CDPESSOA "+
					 "WHERE EXTRACT(MONTH FROM P.DTNASCIMENTO) = "+mes+""+
					 ") foo ORDER BY EXTRACT(DAY FROM \"DTNASCIMENTO\")";
			
			SinedUtil.markAsReader();
			List<Fornecedor> list = getJdbcTemplate().query(
				sql, 
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						Fornecedor fornecedor = new Fornecedor();
						fornecedor.setCdpessoa(rs.getInt("CDPESSOA"));
						fornecedor.setNome(rs.getString("NOME"));
						fornecedor.setDtnascimento(SinedDateUtils.setDateProperty(rs.getDate("DTNASCIMENTO"), 12, Calendar.HOUR_OF_DAY));
						return fornecedor;
					}
				});
			return list;

//		return query()
//			.select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.dtnascimento")
//			.where("extract(month from fornecedor.dtnascimento) = ?", mes)
//			.orderBy("extract(day from fornecedor.dtnascimento)")
//			.list();
	}
	
	
	/**
	 * Insere somente na tabela de fornecedor o cdpessoa.
	 *
	 * @param cliente
	 * @author Rodrigo Freitas
	 */
	public void insertSomenteFornecedor(Fornecedor fornecedor) {
		if(fornecedor == null || fornecedor.getCdpessoa() == null){
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		}
		getJdbcTemplate().update("insert into Fornecedor (cdpessoa, ativo, acesso, identificador) values (?, true, false, ?)", new Object[]{fornecedor.getCdpessoa(), fornecedor.getIdentificador()});
	}

	@SuppressWarnings("unchecked")
	public boolean verificaOutrosRegistros(String itens) {
		if(itens == null || itens.equals("")){
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		}
		
		List<String> listaString = getJdbcTemplate().query("SELECT 'SELECT F1.CDPESSOA FROM FORNECEDOR F1 JOIN ' || t.relname || ' ON ' || t.relname || '.' || a.attname || ' = F1.CDPESSOA WHERE F1.CDPESSOA IN (XX)' AS RELACIONAMENTO " +
																"FROM pg_constraint c " +
																"LEFT JOIN pg_class t  ON c.conrelid  = t.oid " +
																"LEFT JOIN pg_class t2 ON c.confrelid = t2.oid " +
																"LEFT JOIN pg_attribute a ON t.oid = a.attrelid AND a.attnum = c.conkey[1] " +
																"LEFT JOIN pg_attribute a2 ON t2.oid = a2.attrelid AND a2.attnum = c.confkey[1] " +
																"WHERE (" +
																"t2.relname = 'pessoa' OR " +
																"t2.relname = 'fornecedor' OR " +
																"t2.relname = 'contato' OR " +
																"t2.relname = 'empresa' OR " +
																"t2.relname = 'dadobancario' OR " +
																"t2.relname = 'cliente' OR " +
																"t2.relname = 'colaborador' OR " +
																"t2.relname = 'usuario') " +
																"AND t.relname <> 'pessoa' " +
																"AND t.relname <> 'fornecedor' " +
																"AND t.relname <> 'dadobancario' " +
																"AND t.relname <> 'endereco' " +
																"AND t.relname <> 'contato' " +
																"AND t.relname <> 'fornecedorhistorico' " +
//																"AND t.relname <> 'contato' " +
//																"AND t.relname <> 'empresa' " +
//																"AND t.relname <> 'cliente' " +
//																"AND t.relname <> 'colaborador' " +
//																"AND t.relname <> 'usuario' " +
																"AND c.contype = 'f'" , 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString("RELACIONAMENTO");
			}

		});
		
		String sql = "SELECT COUNT(*) FROM (";
		
		boolean first = false;
		for (String string : listaString) {
			if (first) {
				sql += " UNION ALL ";
			}
			sql += string;
			first = true;
		}
		
		sql += ") QUERY";
		
		sql = sql.replaceAll("XX", itens);
		
		int count = getJdbcTemplate().queryForInt(sql);
		
		return count != 0;
	}
	
	/**
	 * M�todo respons�vel por carregar os dados da listagem de Forncedor para 
	 * arquivo CSV.
	 * @param _filtro
	 * @return
	 * @author Taidson
	 * @since 27/08/2010
	 */
	public ListagemResult<Fornecedor> listagemFornecedorCSV(FiltroListagem _filtro){
		
		FornecedorFiltro filtro = (FornecedorFiltro)_filtro;
		
		QueryBuilder<Fornecedor> query = querySined()
			.setUseWhereFornecedorEmpresa(true)
			.select("fornecedor.cdpessoa,fornecedor.nome, fornecedor.cpf, fornecedor.cnpj, " +
					"categoria.nome, listaTelefone.telefone, telefonetipo.nome, " +
					"fornecedor.tipopessoa, fornecedor.ativo")
			.leftOuterJoin("fornecedor.listaPessoacategoria listaPessoacategoria")
			.leftOuterJoin("listaPessoacategoria.categoria categoria")
			.leftOuterJoin("fornecedor.listaTelefone listaTelefone")
			.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
			.leftOuterJoin("fornecedor.listaQuestionario listaQuestionario")
			.where("fornecedor.ativo = ?",filtro.getAtivo())
			.where("categoria = ?",filtro.getCategoria())
			.whereLikeIgnoreAll("fornecedor.nome", filtro.getNomefantasia())
			.where("listaQuestionario.dtaltera>=?", filtro.getDtAvaliacaoInicio())
			.where("listaQuestionario.dtaltera<=?", filtro.getDtAvaliacaoFim())
			.orderBy("fornecedor.nome");
			if(filtro.getTipopessoa() != null && filtro.getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)){
				query.where("tipopessoa = 0");
			}
			if(filtro.getTipopessoa() != null && filtro.getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA)){
				query.where("tipopessoa = 1");
			}

			Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
			if(usuarioService.isRestricaoFornecedorColaborador(usuarioLogado)){
			query.where("("+
							"exists("+
								"select 1 from ColaboradorFornecedor cf "+
								"where "+
								"cf.fornecedor =  fornecedor.cdpessoa and "+
								"cf.colaborador = "+usuarioLogado.getCdpessoa()+
							") "+
							"or "+
							"not exists("+
								"select 1 from ColaboradorFornecedor cf "+
								"where "+
								"cf.fornecedor =  fornecedor.cdpessoa"+
							")"+
						")");
		}
			
		return new ListagemResult<Fornecedor>(query, filtro);

	}

	public void atualizaTipopessoa(Tipopessoa tipopessoa, Integer cdpessoa) {
		getJdbcTemplate().update("UPDATE PESSOA SET TIPOPESSOA = ? WHERE CDPESSOA = ?", new Object[]{tipopessoa.ordinal(), cdpessoa});
	}
	public void atualizaCnpj(Cnpj cnpj, Integer cdpessoa) {
		getJdbcTemplate().update("UPDATE PESSOA SET CNPJ = ? WHERE CDPESSOA = ?", new Object[]{cnpj.getValue(), cdpessoa});
	}
	public void atualizaCpf(Cpf cpf, Integer cdpessoa) {
		getJdbcTemplate().update("UPDATE PESSOA SET CPF = ? WHERE CDPESSOA = ?", new Object[]{cpf.getValue(), cdpessoa});
	}
	public void atualizaRazaosocial(String razaosocial, Integer cdpessoa) {
		getJdbcTemplate().update("UPDATE FORNECEDOR SET RAZAOSOCIAL = ? WHERE CDPESSOA = ?", new Object[]{razaosocial, cdpessoa});
	}
	public void atualizaNomefantasia(String nomefantasia, Integer cdpessoa) {
		getJdbcTemplate().update("UPDATE PESSOA SET NOME = ? WHERE CDPESSOA = ?", new Object[]{nomefantasia, cdpessoa});
	}
	public void atualizaInscricaoestadual(String ie, Integer cdpessoa) {
		getJdbcTemplate().update("UPDATE FORNECEDOR SET INSCRICAOESTADUAL = ? WHERE CDPESSOA = ?", new Object[]{ie, cdpessoa});
	}
	public void atualizaInscricaomunicipal(String im, Integer cdpessoa) {
		getJdbcTemplate().update("UPDATE FORNECEDOR SET INSCRICAOMUNICIPAL = ? WHERE CDPESSOA = ?", new Object[]{im, cdpessoa});
	}

	/**
	 * Carrega informa��es do "colaborador" contabilista para o registro 0100 do SPED Fiscal.
	 *
	 * @param fornecedor
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Fornecedor loadForSpedReg0100Contabilista(Fornecedor fornecedor) {
		if(fornecedor == null || fornecedor.getCdpessoa() == null){
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		}
		return querySined()
					.select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.cpf, fornecedor.email")
					.where("fornecedor = ?", fornecedor)
					.unique();
	}

	/**
	 * Carrega informa��es do escrit�rio contabilista para o registro 0100 do SPED Fiscal.
	 *
	 * @param fornecedor
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Fornecedor loadForSpedReg0100Escritorio(Fornecedor fornecedor) {
		if(fornecedor == null || fornecedor.getCdpessoa() == null){
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		}
		return querySined()
					.select("fornecedor.cdpessoa, fornecedor.cnpj, fornecedor.email, enderecotipo.cdenderecotipo, " +
							"endereco.cep, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, " +
							"municipio.cdibge, telefone.telefone, telefonetipo.cdtelefonetipo")
					.leftOuterJoin("fornecedor.listaEndereco endereco")
					.leftOuterJoin("endereco.municipio municipio")
					.leftOuterJoin("endereco.enderecotipo enderecotipo")
					.leftOuterJoin("fornecedor.listaTelefone telefone")
					.leftOuterJoin("telefone.telefonetipo telefonetipo")
					.where("fornecedor = ?", fornecedor)
					.unique();
	}
	
	/**
	 * M�todo que retorna todos os fornecedores que n�o foram selecionados
	 * 
	 * @param whereNotIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Fornecedor> findFornecedoresWhereNotIn(String whereNotIn) {
		QueryBuilder<Fornecedor> query = query()
			.select("fornecedor.cdpessoa, fornecedor.nome")
			.from(Fornecedor.class)
			.orderBy("fornecedor.nome");
		
		if(whereNotIn != null && !whereNotIn.equals(""))
			query.where("fornecedor.id not in("+whereNotIn+")");
		return query.list();
	}
	
	/**
	* M�todo que busca os fornecedores marcados como agenciavenda para o autocomplete 
	*
	* @param whereLike
	* @return
	* @since Sep 26, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Fornecedor> findFornecedoresAutoCompleteAgenciavenda(String whereLike) {
		return querySined()
			.setUseWhereFornecedorEmpresa(true)
			.select("fornecedor.cdpessoa, fornecedor.nome")
			.whereLikeIgnoreAll("fornecedor.nome", whereLike)
			.where("fornecedor.agenciavenda = ?", Boolean.TRUE)
			.orderBy("fornecedor.nome")
			.list();
	}
	
	/**
	 * Busca a {@link Contagerencial} que est� associada a um dado {@link Fornecedor}.
	 * 
	 * @author <a mailto="giovane.freitas@linkcom.com.br">Giovane Freitas</a>
	 * @since Oct 25, 2011
	 * @param fornecedor
	 * @return
	 */
	public Contagerencial findContaGerencial(Fornecedor fornecedor) {
		if (fornecedor == null || fornecedor.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		fornecedor = querySined()
			.joinFetch("fornecedor.contagerencial contagerencial")
			.joinFetch("contagerencial.vcontagerencial vcontagerencial")
			.entity(fornecedor)
			.unique();
		
		if (fornecedor != null)
			return fornecedor.getContagerencial();
		else
			return null;
	}
	
	public ContaContabil findContaContabil(Fornecedor fornecedor) {
		if (fornecedor == null || fornecedor.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		fornecedor = querySined()
			.joinFetch("fornecedor.contaContabil contacontabil")
			.joinFetch("contacontabil.vcontacontabil vcontacontabil")
			.entity(fornecedor)
			.unique();
		
		if (fornecedor != null)
			return fornecedor.getContaContabil();
		else
			return null;
	}
	
	public Contagerencial findContaGerencialCredito(Fornecedor fornecedor) {
		if (fornecedor == null || fornecedor.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		fornecedor = querySined()
						.select("fornecedor.cdpessoa, contagerencialcredito.cdcontagerencial")
			.join("fornecedor.contagerencialcredito contagerencialcredito")
			.where("fornecedor = ?", fornecedor)
			.unique();
		
		if (fornecedor != null)
			return fornecedor.getContagerencialcredito();
		else
			return null;
	}

	/**
	 * M�todo que verifica se o identificador j� foi cadastrado anteriormente
	 * 
	 * @param identificador
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeIdentificadorFornecedor(String identificador, Integer cdpessoa) {
		SinedUtil.markAsReader();
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(beanClass)
			.where("fornecedor.identificador = ?", identificador)
			.where("fornecedor.cdpessoa <> ?", cdpessoa)
			.unique() > 0;
	}

	/**
	 * Busca todos os registros para a exporta��o de arquivos gerenciais.
	 * @author Giovane Freitas <giovane.freitas@linkcom.com.br>
	 */
	public Iterator<Object[]> findForArquivoGerencial() {
		return newQueryBuilder(Object[].class)
			.from(Fornecedor.class)
			.select("fornecedor.cdpessoa, fornecedor.nome, municipio.nome, uf.sigla")
			.join("fornecedor.listaEndereco endereco")
			.join("endereco.municipio municipio")
			.join("municipio.uf uf")

			//A ordem para selecionar o endere�o deve ser a seguinte: "�nico", "Faturamento", "Cobran�a" e depois qualquer um.
			.orderBy("fornecedor.cdpessoa, municipio.cdmunicipio, endereco.enderecotipo.id")
			
			.iterate();
		
	}
	
	/**
	 * M�todo que busca as informa��es do contabilista para o Registro0100 do SPED PIS/COFINS
	 *
	 * @param fornecedor
	 * @return
	 * @author Luiz Fernando
	 */
	public Fornecedor loadForSpedPiscofinsReg0100Contabilista(Fornecedor fornecedor) {
		if(fornecedor == null || fornecedor.getCdpessoa() == null){
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		}
		return querySined()
					.select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.cpf")
					.where("fornecedor = ?", fornecedor)
					.unique();
	}
	
	/**
	 * M�todo que busca as informa��es do escrot�rio para o Registro0100 do SPED PIS/COFINS
	 *
	 * @param fornecedor
	 * @return
	 * @author Luiz Fernando
	 */
	public Fornecedor loadForSpedPiscofinsReg0100Escritorio(Fornecedor fornecedor) {
		if(fornecedor == null || fornecedor.getCdpessoa() == null){
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		}
		return querySined()
					.select("fornecedor.cdpessoa, fornecedor.cnpj, fornecedor.email, enderecotipo.cdenderecotipo, " +
							"endereco.cep, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, " +
							"municipio.cdibge, telefone.telefone, telefonetipo.cdtelefonetipo")
					.leftOuterJoin("fornecedor.listaEndereco endereco")
					.leftOuterJoin("endereco.municipio municipio")
					.leftOuterJoin("endereco.enderecotipo enderecotipo")
					.leftOuterJoin("fornecedor.listaTelefone telefone")
					.leftOuterJoin("telefone.telefonetipo telefonetipo")
					.where("fornecedor = ?", fornecedor)
					.unique();
	}
	
	/**
	 * M�todo que busca os fornecedores ativos para o autocompelte 
	 *
	 * @param whereLike
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Fornecedor> findFornecedoresAtivosAutoComplete(String whereLike, String whereInEmpresa) {
		QueryBuilder<Fornecedor> query = querySined()
			.setUseWhereFornecedorEmpresa(true)
			.select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.razaosocial, fornecedor.cpf, fornecedor.cnpj")
			.openParentheses()
				.whereLikeIgnoreAll("fornecedor.nome", whereLike)
				.or()
				.whereLikeIgnoreAll("fornecedor.razaosocial", whereLike)
				.or()
				.whereLikeIgnoreAll("fornecedor.cpf", whereLike!=null ? whereLike.replaceAll("\\.|\\/|\\-", "") : "")
				.or()
				.whereLikeIgnoreAll("fornecedor.cnpj", whereLike!=null ? whereLike.replaceAll("\\.|\\/|\\-", "") : "")
			.closeParentheses()
			.where("fornecedor.ativo = true");
		
		addWhereInFornecedorByEmpresa(query, whereInEmpresa);
		
		return query
			.orderBy("fornecedor.nome")
			.list();
	}
	
	/**
	 * M�todo que busca os fornecedores pelo nome ou cnpj 
	 * 
	 * @param whereLike
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Fornecedor> findFornecedoresNomeCnpj(String whereLike) {
		return querySined()
			.select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.razaosocial, fornecedor.cpf, fornecedor.cnpj")
			.openParentheses()
				.whereLikeIgnoreAll("fornecedor.nome", whereLike)
				.or()
				.whereLikeIgnoreAll("fornecedor.razaosocial", whereLike)
				.or()
				.whereLikeIgnoreAll("fornecedor.cpf", whereLike!=null ? whereLike.replaceAll("\\.|\\/|\\-", "") : "")
				.or()
				.whereLikeIgnoreAll("fornecedor.cnpj", whereLike!=null ? whereLike.replaceAll("\\.|\\/|\\-", "") : "")
			.closeParentheses()
			.orderBy("fornecedor.nome")
			.list();
	}

	/**
	 * M�todo que busca os fornecedores que s�o transportadora
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Fornecedor> findFornecedoresTransportadora(String whereInEmpresa) {
		QueryBuilder<Fornecedor> query = querySined()
				.setUseWhereFornecedorEmpresa(true)
				.select("fornecedor.cdpessoa, fornecedor.nome")
				.where("transportador = true");
		
		addWhereInFornecedorByEmpresa(query, whereInEmpresa);
				
		return query
				.orderBy("fornecedor.nome")
				.list();
	}
	
	/**
	 * M�todo que busca fornecedores que s�o transportadoras para auto complete.
	 * @param whereLike
	 * @return
	 * @author Thiers Euller
	 */
	public List<Fornecedor> findFornecedoresTransportadoraForAutocomplete(String whereLike, String whereInEmpresa) {
		QueryBuilder<Fornecedor> query = querySined()
			.setUseWhereFornecedorEmpresa(true);
		query
			.select("fornecedor.cdpessoa, fornecedor.nome")
			.whereLikeIgnoreAll("fornecedor.nome", whereLike)
			.where("transportador = true")
			.orderBy("fornecedor.nome");
		
		query = restringirUsuarioLogado(query);
		addWhereInFornecedorByEmpresa(query, whereInEmpresa);
		
		return query.list();
	}
	
	/**
	 * M�todo que busca fornecedores que n�o s�o transportadoras e fabricantes para auto complete.
	 * @param whereLike
	 * @return
	 */
	public List<Fornecedor> findFornecedoresParceiros(String whereLike, String whereInEmpresa) {
		QueryBuilder<Fornecedor> query = querySined()
			.setUseWhereFornecedorEmpresa(true);
		query
			.select("fornecedor.cdpessoa, fornecedor.nome")
			.whereLikeIgnoreAll("fornecedor.nome", whereLike)
			.where("coalesce(fornecedor.transportador, false) = false")
			.where("coalesce(fornecedor.fabricante, false) = false")
			.orderBy("fornecedor.nome");
		
		query = restringirUsuarioLogado(query);
		addWhereInFornecedorByEmpresa(query, whereInEmpresa);
		
		return query.list();
	}
	
	/**
	 * 
	 * M�todo que busca os dados para emitir o relat�rio avaliativo dos fornecedores selecionados na listagem.
	 *
	 *@author Thiago Augusto
	 *@date 07/03/2012
	 * @param whereIn
	 * @return
	 */
	public List<Fornecedor> carregarDadosRelatorioAvaliativo(String whereIn){
		return  querySined()
					.select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.cpf, fornecedor.cnpj, " +
					"listaTelefone.telefone, listaEndereco.cep, listaEndereco.logradouro, listaEndereco.numero, listaEndereco.complemento, listaEndereco.bairro, municipio.nome," +
					"listaQuestionario.cdpessoaquestionario, listaQuestionario.pontuacao, listaQuestionario.situacao, listaQuestionario.cdusuarioaltera, listaQuestionario.dtaltera," +
					"listaPessoaQuestionarioQuestao.cdpessoaquestionarioquestao, listaPessoaQuestionarioQuestao.pontuacao, listaPessoaQuestionarioQuestao.sim, listaPessoaQuestionarioQuestao.resposta, " +
					"questionarioquestao.cdquestionarioquestao, questionarioquestao.questao, questionarioquestao.pontuacaosim, questionarioquestao.pontuacaonao, uf.sigla, " +
					"arquivo.cdarquivo, questionario.avaliarrecebimento, questionarioquestao.pontuacaoquestao")
					.leftOuterJoin("fornecedor.listaTelefone listaTelefone")
					.leftOuterJoin("fornecedor.listaEndereco listaEndereco")
					.leftOuterJoin("listaEndereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("fornecedor.listaQuestionario listaQuestionario")
					.leftOuterJoin("listaQuestionario.listaPessoaQuestionarioQuestao listaPessoaQuestionarioQuestao")
					.leftOuterJoin("listaPessoaQuestionarioQuestao.questionarioquestao questionarioquestao")
					.leftOuterJoin("listaQuestionario.questionario questionario")
					.leftOuterJoin("questionario.arquivo arquivo")
					.whereIn("fornecedor.cdpessoa", whereIn)
					.orderBy("questionarioquestao.questao")
					.list();
	}
	
	/**
	 * M�todo que busca os dados do fornecedor para integra��o de cadastro da dom�nio 
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Fornecedor> findForDominiointegracaoCadastro(DominiointegracaoFiltro filtro) {
		if(filtro == null || filtro.getDtinicio() == null || filtro.getDtfim() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return querySined()
				.select("fornecedor.cdpessoa, fornecedor.razaosocial, fornecedor.nome, fornecedor.tipopessoa, fornecedor.cnpj, fornecedor.cpf, " +
						"fornecedor.inscricaoestadual, fornecedor.inscricaomunicipal, endereco.caixapostal, " +
						"endereco.logradouro, endereco.bairro, endereco.complemento, endereco.cep, endereco.numero, " +
						"municipio.cdibge, municipio.nome, uf.nome, uf.sigla, pais.cdpais, telefone.telefone,  telefonetipo.cdtelefonetipo ")
				.leftOuterJoin("fornecedor.listaEndereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("endereco.pais pais")
				.leftOuterJoin("fornecedor.listaTelefone telefone")
				.leftOuterJoin("telefone.telefonetipo telefonetipo")
				.where("exists(" +
						"select 1 from Entregadocumento ed " +
						"where ed.dtentrada >= to_date('"+ format.format(filtro.getDtinicio())+ "', 'DD/MM/YYYY') " +
						" and ed.dtentrada <= to_date('"+ format.format(filtro.getDtfim())+ "', 'DD/MM/YYYY') " +
						" and ed.fornecedor.cdpessoa = fornecedor.cdpessoa " +
						")")
				.list();
	}
	
	/**
	 * 
	 * M�todo que carrega o fornecedor pelo CPF ou pelo CNPJ
	 *
	 * @name getFornecedorByCpfCnpj
	 * @param numero
	 * @return
	 * @return Fornecedor
	 * @author Thiago Augusto
	 * @date 03/04/2012
	 *
	 */
	public Fornecedor getFornecedorByCpfCnpj(Cnpj cnjp, Cpf cpf){
		QueryBuilder<Fornecedor> query = querySined();
		query.select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.cpf, fornecedor.cnpj, fornecedor.tipopessoa, fornecedor.razaosocial, fornecedor.inscricaoestadual, fornecedor.inscricaomunicipal");
		
		if(cnjp != null)
			query.where("fornecedor.cnpj = ?", cnjp);
		else 
			query.where("fornecedor.cpf = ?", cpf);

		return query.unique();
	}
	
	public Fornecedor loadFornecedorForImportacaoXml(Fornecedor fornecedor){
		if(Util.objects.isNotPersistent(fornecedor)){
			return fornecedor;
		}
		QueryBuilder<Fornecedor> query = querySined();
		query.select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.cpf, fornecedor.cnpj, fornecedor.tipopessoa, fornecedor.razaosocial, fornecedor.inscricaoestadual, fornecedor.inscricaomunicipal");
		
		query.where("fornecedor = ?", fornecedor);

		return query.unique();
	}
	

	/**
	 * Carrega o fornecedor a partir de um CNPJ
	 *
	 * @param cnpj
	 * @return
	 * @since 26/05/2012
	 * @author Rodrigo Freitas
	 */
	public Fornecedor findByCnpj(Cnpj cnpj) {
		if(cnpj == null){
			throw new SinedException("O CNPJ n�o pode ser nulo.");
		}
		return querySined()
					.select("fornecedor.cdpessoa, fornecedor.nome")
					.where("fornecedor.cnpj = ?", cnpj)
					.unique();
	}
	
	/**
	 * Carrega o fornecedor a partir de um CPF
	 *
	 * @param cnpj
	 * @return
	 * @since 26/05/2012
	 * @author Rodrigo Freitas
	 */
	public Fornecedor findByCpf(Cpf cpf) {
		if(cpf == null){
			throw new SinedException("O CPF n�o pode ser nulo.");
		}
		return querySined()
					.select("fornecedor.cdpessoa, fornecedor.nome")
					.where("fornecedor.cpf = ?", cpf)
					.unique();
	}
	
	/**
	 * Busca o fornecedor pelo nome.
	 *
	 * @param nome
	 * @return
	 * @since 26/05/2012
	 * @author Rodrigo Freitas
	 */
	public Fornecedor findByNome(String nome) {
		if(nome == null || nome.equals("")){
			throw new SinedException("Nome n�o pode ser nulo.");
		}
		List<Fornecedor> lista = querySined()
					.select("fornecedor.cdpessoa, fornecedor.nome")
					.where("fornecedor.nome = ?", nome)
					.list();
		
		if(lista == null || lista.size() == 0){
			return null;
		} else if(lista.size() > 1){
			throw new SinedException("Existem mais de um fornecedor com o nome: " + nome);
		} else return lista.get(0);
	}

	/**
	 * Carrega o Fornecedor com a conta gerencial
	 *
	 * @param pessoa
	 * @return
	 * @author Luiz Fernando
	 */
	public Fornecedor loadForMovimentacaocontabil(Pessoa pessoa) {
		if(pessoa == null || pessoa.getCdpessoa() == null)
			throw new SinedException("Pessoa n�o pode ser nula.");
		
		return querySined()
				.select("fornecedor.cdpessoa, contacontabil.cdcontacontabil")
				.leftOuterJoin("fornecedor.contaContabil contacontabil")
				.where("fornecedor.cdpessoa = ?", pessoa.getCdpessoa())
				.unique();
	}

	/**
	 * M�todo que carrega as informa��es do fornecedor
	 *
	 * @param fornecedor
	 * @return
	 * @author Luiz Fernando
	 */
	public Fornecedor loadForOrdemcompraInfo(Fornecedor fornecedor) {
		if(fornecedor == null || fornecedor.getCdpessoa() == null)
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		
		return querySined()
			.select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.cnpj, fornecedor.cpf, fornecedor.email, " +
					"listaPessoacategoria.cdpessoacategoria, categoria.nome, telefonetipo.cdtelefonetipo, telefonetipo.nome, " +
					"listaTelefone.cdtelefone, listaTelefone.telefone, naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome ")
			.leftOuterJoin("fornecedor.listaTelefone listaTelefone")
			.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
			.leftOuterJoin("fornecedor.listaPessoacategoria listaPessoacategoria")
			.leftOuterJoin("listaPessoacategoria.categoria categoria")
			.leftOuterJoin("fornecedor.naturezaoperacao naturezaoperacao")
			.where("fornecedor = ?", fornecedor)
			.unique();
	}
	
	public Fornecedor loadForEntradaFiscalInfo(Fornecedor fornecedor) {
		if (fornecedor == null || fornecedor.getCdpessoa() == null)
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		return querySined().select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.cnpj, fornecedor.cpf")
				.where("fornecedor = ?", fornecedor).unique();
	}

	/**
	 * Verifica se h� fornecedor com o identificador e nome igual.
	 *
	 * @param identificador
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/02/2013
	 */
	public boolean haveFornecedorIdentificadorNome(String identificador, String nome) {
		SinedUtil.markAsReader();
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Fornecedor.class)
				.where("fornecedor.identificador = ?", identificador)
				.where("fornecedor.nome = ?", nome)
				.unique() > 0;
	}
	
	/**
	 * 
	 * @param empresa
	 * @param whereInMaterial
	 * @author Thiago Clemente
	 * 
	 */
	public List<Fornecedor> findForFaturamento(Empresa empresa, String whereInMaterial){
		return querySined()
				.setUseWhereFornecedorEmpresa(true)
				.select("fornecedor.cdpessoa, fornecedor.nome, material.cdmaterial")
				.join("fornecedor.listaEmpresarepresentacao listaEmpresarepresentacao")
				.join("listaEmpresarepresentacao.empresa empresa")
				.join("fornecedor.listaMaterialfornecedor listaMaterialfornecedor")
				.join("listaMaterialfornecedor.material material")
				.where("fornecedor.ativo=?", Boolean.TRUE)
				.where("empresa=?", empresa)
				.whereIn("material.cdmaterial", whereInMaterial)
				.orderBy("fornecedor.nome")
				.list();
	}
	
	public Fornecedor loadForTributacao(Fornecedor fornecedor) {
		if(fornecedor == null || fornecedor.getCdpessoa() == null)
			throw new SinedException("Cliente n�o pode ser nulo.");
		
		return querySined()
			.select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.identificador, fornecedor.razaosocial, fornecedor.cnpj, " +
					"fornecedor.cpf, fornecedor.transportador, " +
					"endereco.cdendereco, endereco.cep, endereco.complemento, endereco.numero, endereco.logradouro," +
					"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.nome, uf.sigla, pais.cdpais, enderecotipo.cdenderecotipo, " +
					"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome, naturezaoperacao.naoVincularMaterialFornecedor ")
			.leftOuterJoin("fornecedor.listaEndereco endereco")
			.leftOuterJoin("endereco.enderecotipo enderecotipo")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("endereco.pais pais")
			.leftOuterJoin("fornecedor.naturezaoperacao naturezaoperacao")
			
			.where("fornecedor = ?", fornecedor)
			.unique();
	}
	
	/**
	 * 
	 * @param cdpessoa
	 */
	public boolean isFornecedor(Integer cdpessoa){
		SinedUtil.markAsReader();
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.where("fornecedor.cdpessoa=?", cdpessoa)
				.unique()>0;
	}
	
	public boolean isPermissaoFornecedor(Fornecedor fornecedor){
		QueryBuilder<Fornecedor> query = querySined()
			.setUseWhereFornecedorEmpresa(true)
			.select("fornecedor.cdpessoa, fornecedor.identificador, fornecedor.nome")
			.where("fornecedor = ?", fornecedor);
		
		query = restringirUsuarioLogado(query);
		List<Fornecedor> lista = query.list();
		return SinedUtil.isListNotEmpty(lista);
	}
	
	public List<Fornecedor> loadAllVinculadosUsuarioLogado(String whereLike, boolean includeHaveOrdemcompra, String whereInEmpresa){
		QueryBuilder<Fornecedor> query = querySined()
			.setUseWhereFornecedorEmpresa(true);
		
		query
			.select("fornecedor.cdpessoa, fornecedor.identificador, fornecedor.nome")
			.openParentheses()
				.whereLikeIgnoreAll("fornecedor.nome", whereLike)
				.or()
				.whereLikeIgnoreAll("fornecedor.razaosocial", whereLike)
				.or()
				.whereLikeIgnoreAll("fornecedor.cpf", whereLike!=null ? whereLike.replaceAll("\\.|\\/|\\-", "") : "")
				.or()
				.whereLikeIgnoreAll("fornecedor.cnpj", whereLike!=null ? whereLike.replaceAll("\\.|\\/|\\-", "") : "")
			.closeParentheses()
			.orderBy("fornecedor.nome");
		if(includeHaveOrdemcompra){
			query
				.openParentheses()
					.where("fornecedor.ativo = ?",Boolean.TRUE)
					.or()
					.where("exists (select o.cdordemcompra from Ordemcompra o where o.fornecedor = fornecedor)")
				.closeParentheses();
		} else {
			query.where("fornecedor.ativo = ?",Boolean.TRUE);
		}
		query = restringirUsuarioLogado(query);
		addWhereInFornecedorByEmpresa(query, whereInEmpresa);
		return query.list();
	}
	
	private QueryBuilder<Fornecedor> restringirUsuarioLogado(QueryBuilder<Fornecedor> query){
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioService.isRestricaoFornecedorColaborador(usuarioLogado)){
			query.where("("+
							"exists("+
								"select 1 from ColaboradorFornecedor cf "+
								"where "+
								"cf.fornecedor =  fornecedor.cdpessoa and "+
								"cf.colaborador = "+usuarioLogado.getCdpessoa()+
							") "+
							"or "+
							"not exists("+
								"select 1 from ColaboradorFornecedor cf "+
								"where "+
								"cf.fornecedor =  fornecedor.cdpessoa"+
							")"+
						")");
		}
		
		return query;
	}
	
	public List<Fornecedor> findForAndroid(String whereIn) {
		return query()
		.select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.email, fornecedor.ativo, fornecedor.transportador, " +
				"listaEndereco.logradouro, listaEndereco.numero, listaEndereco.cdendereco, " +
				"listaEndereco.complemento, listaEndereco.bairro, municipio.nome, uf.nome, listaEndereco.cep, " +
				"listaEndereco.pontoreferencia, fornecedor.cpf, fornecedor.cnpj, fornecedor.razaosocial, fornecedor.tipopessoa, " +
				"fornecedor.email, listaTelefone.cdtelefone, listaTelefone.telefone, telefonetipo.cdtelefonetipo, telefonetipo.nome ")
		.leftOuterJoin("fornecedor.listaEndereco listaEndereco")
		.leftOuterJoin("listaEndereco.municipio municipio")
		.leftOuterJoin("municipio.uf uf")
		.leftOuterJoin("fornecedor.listaTelefone listaTelefone")
		.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
		.where("fornecedor.ativo = true")
		.whereIn("fornecedor.cdpessoa", whereIn)
		.list();
	}
	
	/**
	 * M�todo que busca os fornecedores que possuem 
	 * o marcador agenciavenda como true para
	 * o autocomplete de agencia na tela de venda 
	 * @param whereLike
	 * @return
	 */
	public List<Fornecedor> findFornecedoresAgenciaVenda(String whereLike) {
		return querySined()
			.setUseWhereFornecedorEmpresa(true)
			.select("fornecedor.cdpessoa, fornecedor.identificador, fornecedor.nome")
			.whereLikeIgnoreAll("fornecedor.nome", whereLike)
			.where("fornecedor.ativo = ?",Boolean.TRUE)
			.where("fornecedor.agenciavenda = ?",Boolean.TRUE)
			.orderBy("fornecedor.nome")
			.list();
	}
	
	public Fornecedor loadWithoutPermissao(Fornecedor bean, String campos) {
		if(bean==null || bean.getCdpessoa()==null){
			throw new SinedException("Os par�metros bean e cdpessoa n�o podem ser null.");
		}
		QueryBuilderSined<Fornecedor> query = querySined();
		
		if(campos!=null && !campos.equals("")){
			query.select(campos);
		}
		
		return query
		.setUseWhereEmpresa(false)
		.setUseWhereProjeto(false)
		.where("fornecedor.cdpessoa=?", bean.getCdpessoa())
		.unique();
	}

	public Fornecedor loadRepresentanteForWSArvoreAcesso(Pessoa vendedor) {
		if(vendedor==null || vendedor.getCdpessoa()==null){
			throw new RuntimeException("Par�metro vendedor n�o pode ser nulo.");
		}
		
		return querySined()
		.select("representante.nome, representante.razaosocial, representante.cnpj, representante.site, representante.email, " +
				"telTipoRepresentante.cdtelefonetipo, telRepresentante.telefone, " +
				"endTipoRepresentante.cdenderecotipo, endRepresentante.cep, endRepresentante.logradouro, endRepresentante.numero, endRepresentante.complemento, " +
				"endRepresentante.bairro, ufEndRepresentante.sigla, municipioEndRepresentante.nome, endRepresentante.caixapostal")
		.leftOuterJoin("fornecedor.listaTelefone telRepresentante")
		.leftOuterJoin("telRepresentante.telefonetipo telTipoRepresentante")
		.leftOuterJoin("representante.listaEndereco endRepresentante")
		.leftOuterJoin("endRepresentante.enderecotipo endTipoRepresentante")
		.leftOuterJoin("endRepresentante.municipio municipioEndRepresentante")
		.leftOuterJoin("municipioEndRepresentante.uf ufEndRepresentante")
		.where("fornecedor.cdpessoa = ?", vendedor.getCdpessoa())
		.unique();
	}
	
	public void updateContaContabil(Fornecedor fornecedor, ContaContabil contacontabil){
		getJdbcTemplate().update("update fornecedor set cdcontacontabil=? where cdpessoa=?", new Object[]{contacontabil.getCdcontacontabil(), fornecedor.getCdpessoa()});
	}
	
	public List<Fornecedor> findForSAGE(Date dataBase){
		return querySined()
				.select("contacontabil.nome, contacontabil.codigoalternativo, vcontacontabil.nivel, vcontacontabil.identificador")
				.join("fornecedor.historico historico")
				.join("fornecedor.contaContabil contacontabil")
				.join("contacontabil.vcontacontabil vcontacontabil")
				.where("cast(historico.dtaltera as date)>?", dataBase)
				.whereLikeIgnoreAll("historico.observacao", "Cria��o do fornecedor")
				.list();
	}
	
	/**
	* M�todo que busca os fornecedores para a integra��o SAGE
	*
	* @param whereIn
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public List<Fornecedor> findForSAGE(String whereIn){
		QueryBuilder<Fornecedor> query = querySined()
				.select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.dtaltera, fornecedor.razaosocial, fornecedor.cpf, fornecedor.cnpj, fornecedor.email, fornecedor.inscricaoestadual, "+
						"fornecedor.inscricaomunicipal, fornecedor.site, fornecedor.observacao, telefone.telefone, "+
						"endereco.enderecotipo, contato.nome, contato.emailcontato, endereco.cep, endereco.numero, endereco.logradouro, endereco.complemento, "+
						"endereco.bairro, endereco.caixapostal, municipio.nome, municipio.cdibge, uf.sigla, pais.nome, pais.cdbacen, " +
						"historico.dtaltera")
				.leftOuterJoin("fornecedor.listaTelefone telefone")
				.leftOuterJoin("fornecedor.listaEndereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("endereco.enderecotipo enderecotipo")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("endereco.pais pais")
				.leftOuterJoin("fornecedor.listaContato listaContato")
				.leftOuterJoin("listaContato.contato contato")
				.leftOuterJoin("fornecedor.historico historico");
		
		SinedUtil.quebraWhereIn("fornecedor.cdpessoa", whereIn, query);
		
		return query
			.orderBy("fornecedor.cdpessoa, historico.dtaltera")
			.list();
	}
	
	public List<Fornecedor> findForValidacaoSAGE(String whereIn){
		QueryBuilder<Fornecedor> query = querySined()
				.select("fornecedor.cdpessoa, fornecedor.nome");
		
		SinedUtil.quebraWhereIn("fornecedor.cdpessoa", whereIn, query);
		
		return query
			.orderBy("fornecedor.cdpessoa")
			.list();
	}
	
	public List<Fornecedor> findForInventario(String whereIn) {
		QueryBuilder<Fornecedor> query = querySined()
				.select("fornecedor.cdpessoa, fornecedor.inscricaoestadual, enderecotipo.cdenderecotipo, uf.sigla ")
				.leftOuterJoin("fornecedor.listaEndereco endereco")
				.leftOuterJoin("endereco.enderecotipo enderecotipo")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf");
		
		SinedUtil.quebraWhereIn("fornecedor.cdpessoa", whereIn, query);
		
		return query
			.orderBy("fornecedor.cdpessoa")
			.list();
	}
	
	public List<Fornecedor> findAssociacaoAtivoAutocomplete(String q) {
		return querySined()
				.select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.razaosocial")
				.where("fornecedor.associacao=?", Boolean.TRUE)
				.where("fornecedor.ativo=?", Boolean.TRUE)
				.whereLikeIgnoreAll("fornecedor.nome", q)
				.orderBy("fornecedor.nome")
				.autocomplete()
				.list();
	}	

	public List<Fornecedor> findCaptadorAtivoAutocomplete(String q) {
		return querySined()
				.select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.razaosocial")
				.where("fornecedor.captador=?", Boolean.TRUE)
				.where("fornecedor.ativo=?", Boolean.TRUE)
				.whereLikeIgnoreAll("fornecedor.nome", q)
				.orderBy("fornecedor.nome")
				.autocomplete()
				.list();
	}

	public Fornecedor loadFornecedorAssociacao(Fornecedor fornecedor) {
		if(fornecedor == null || fornecedor.getCdpessoa() == null)
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		
		return querySined()
				.select("fornecedor.cdpessoa, fornecedor.identificador, fornecedor.nome")
				.where("fornecedor = ?", fornecedor)
				.where("fornecedor.associacao = true")
				.unique();
	}

	public Fornecedor loadForAtualizacaoCampos(Fornecedor fornecedor) {
		if(fornecedor == null || fornecedor.getCdpessoa() == null)
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		
		return querySined()
			.setUseWhereEmpresa(false)
			.setUseWhereFornecedorEmpresa(false)
			.setUseWhereProjeto(false)
			.setUseWhereClienteEmpresa(false)
			.select("fornecedor.cdpessoa, fornecedor.identificador, fornecedor.nome, fornecedor.razaosocial, fornecedor.tipopessoa, " +
					"fornecedor.cnpj, fornecedor.cpf, fornecedor.inscricaomunicipal, fornecedor.inscricaoestadual ")
			.where("fornecedor = ?", fornecedor)
			.unique();
	}

	public List<Fornecedor> findCredenciadoras(String nome) {
		return querySined()
				.setUseWhereFornecedorEmpresa(true)
				.select("fornecedor.cdpessoa, fornecedor.identificador, fornecedor.nome")
				.where("fornecedor.credenciadora = ?", Boolean.TRUE)
				.whereLikeIgnoreAll("fornecedor.nome", nome)
				.orderBy("fornecedor.nome")
				.autocomplete()
				.list();
	}

	public Boolean isFabricante(Integer cdpessoa) {
		SinedUtil.markAsReader();
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.where("fornecedor.cdpessoa=?", cdpessoa)
				.where("fornecedor.fabricante is true")
				.unique()>0;
	}

	public List<Fornecedor> findFornecedoresFabricantesAtivosAutoComplete(String whereLike, boolean isFabricante, boolean isFornecedor) {
		QueryBuilder<Fornecedor> query = querySined()
				.setUseWhereFornecedorEmpresa(true)
				.select("fornecedor.cdpessoa, fornecedor.nome, fornecedor.razaosocial, fornecedor.cpf, fornecedor.cnpj")
				.openParentheses()
					.whereLikeIgnoreAll("fornecedor.nome", whereLike)
					.or()
					.whereLikeIgnoreAll("fornecedor.razaosocial", whereLike)
					.or()
					.whereLikeIgnoreAll("fornecedor.cpf", whereLike!=null ? whereLike.replaceAll("\\.|\\/|\\-", "") : "")
					.or()
					.whereLikeIgnoreAll("fornecedor.cnpj", whereLike!=null ? whereLike.replaceAll("\\.|\\/|\\-", "") : "")
				.closeParentheses()
				.where("fornecedor.ativo = true");
				if (!isFornecedor) {
					if(isFabricante){
						query.where("fornecedor.fabricante is true");
					}else {
						query.openParentheses()
							.where("fornecedor.fabricante is false")
							.or()
							.where("fornecedor.fabricante is null")
						.closeParentheses();
					}	
				}																		
			return query
				.orderBy("fornecedor.nome")
				.list();
	}	
	
	public Boolean existeCodigoParceiro (Integer codigoParceiro){
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Fornecedor.class)
				.where("fornecedor.codigoParceiro = ?",codigoParceiro)
				.unique() > 0;
	}
	
	public Fornecedor findByCodigoParceiro (Integer codigoParceiro){
		if(codigoParceiro == null){
			return null;
		}
		return querySined()
				.select("fornecedor.cdpessoa, fornecedor.nome")
				.where("fornecedor.codigoParceiro = ?",codigoParceiro)
				.setMaxResults(1)
				.unique();
	}
}