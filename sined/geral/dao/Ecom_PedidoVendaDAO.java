package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Ecom_PedidoVenda;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class Ecom_PedidoVendaDAO extends GenericDAO<Ecom_PedidoVenda>{

	public List<Ecom_PedidoVenda> findForCriarPedido(){
		return query()
				.leftOuterJoinFetch("ecom_PedidoVenda.pedidoItens pedidoItens")
				.leftOuterJoinFetch("ecom_PedidoVenda.pedidoCliente pedidoCliente")
				.where("coalesce(ecom_PedidoVenda.processado, false) = false")
				.list();
	}
	
	public void updateProcessado(Integer idEcomPedidoVenda, Integer cdpedidovenda){
		getJdbcTemplate().execute("update Ecom_PedidoVenda set processado = true, cdpedidovenda = "+cdpedidovenda+" where cdEcom_pedidovenda = "+idEcomPedidoVenda);
	}
	
	public Ecom_PedidoVenda findWithPedidoCriado(Ecom_PedidoVenda ecom_PedidoVenda){
		return query()
				.where("coalesce(ecom_PedidoVenda.processado, false) = true")
				.joinFetch("ecom_PedidoVenda.pedidoVenda pedidoVenda")
				.where("ecom_PedidoVenda = ?", ecom_PedidoVenda)
				.setMaxResults(1)
				.unique();
	}
	
	public Ecom_PedidoVenda loadByCdPedidoVenda(Integer cdpedidovenda){
		return querySined()
				.setUseReadOnly(false)
				.join("ecom_PedidoVenda.pedidoVenda pedidoVenda")
				.where("pedidoVenda.cdpedidovenda = ?", cdpedidovenda)
				.setMaxResults(1)
				.unique();
	}

	public void updateRastreamento(Ecom_PedidoVenda bean) {
		getHibernateTemplate().bulkUpdate("update Ecom_PedidoVenda e set e.rastreamento = ?, e.urlRastreamento = ? where e = ?", new Object[]{
			bean.getRastreamento(), bean.getUrlRastreamento(), bean
		});
	}
	
	public void updateRastreamentoExpedicao(Ecom_PedidoVenda bean) {
		//getHibernateTemplate().bulkUpdate("update Ecom_PedidoVenda e set e.rastreamento = ?, e.urlRastreamento = ?, e.valorCustoFrete = ?, e.dataPrevisaoEntrega = ?, e.transportadora = ? where e = ?", new Object[]{
			//	bean.getRastreamento(), bean.getUrlRastreamento(), bean.getValorCustoFrete(), bean.getDataPrevisaoEntrega(), bean.getTransportadora(), bean
		//});
		
		List<String> listUpdateEcompedidovenda = new ArrayList<String>();
		List<String> listUpdateExpedicao = new ArrayList<String>();
		if(StringUtils.isNotBlank(bean.getRastreamento())){
			listUpdateEcompedidovenda.add("rastreamento = '"+bean.getRastreamento()+"'");
			listUpdateExpedicao.add("rastreamento = '"+bean.getRastreamento()+"'");
		}
		if(StringUtils.isNotBlank(bean.getUrlRastreamento())){
			listUpdateEcompedidovenda.add("urlRastreamento = '"+bean.getUrlRastreamento()+"'");
			listUpdateExpedicao.add("urlRastreamento = '"+bean.getUrlRastreamento()+"'");
		}
		if(bean.getValorCustoFrete() != null){
			listUpdateEcompedidovenda.add("valorCustoFrete = "+bean.getValorCustoFrete().toLong());
			listUpdateExpedicao.add("valorCustoFrete = "+bean.getValorCustoFrete().toLong()+"");
		}
		if(bean.getDataPrevisaoEntrega() != null){
			listUpdateEcompedidovenda.add("dataPrevisaoEntrega = '"+SinedDateUtils.toStringPostgre(bean.getDataPrevisaoEntrega())+"'");
			listUpdateExpedicao.add("dataPrevisaoEntrega = '"+SinedDateUtils.toStringPostgre(bean.getDataPrevisaoEntrega())+"'");
		}
		if(StringUtils.isNotBlank(bean.getTransportadora())){
			listUpdateEcompedidovenda.add("transportadora = '"+bean.getTransportadora()+"'");
		}
		if(bean.getQuantidadevolumes() != null){
			listUpdateEcompedidovenda.add("quantidadevolumes = "+bean.getQuantidadevolumes());
		}
		if(Util.objects.isPersistent(bean.getTransportadoraTrans())){
			listUpdateExpedicao.add("cdtransportadora = "+bean.getTransportadoraTrans().getCdpessoa());
		}
		
		getJdbcTemplate().execute("update Ecom_PedidoVenda set "+CollectionsUtil.concatenate(listUpdateEcompedidovenda, ",")+ " where cdEcom_pedidovenda = "+bean.getCdEcom_pedidovenda());
		getJdbcTemplate().execute("update Expedicao set "+CollectionsUtil.concatenate(listUpdateExpedicao, ",")+ " where cdexpedicao = "+bean.getCdExpedicaoTrans());
		//getHibernateTemplate().bulkUpdate("update Expedicao e set e.rastreamento = ?, e.urlRastreamento = ?, e.valorCustoFrete = ?, e.dataPrevisaoEntrega = ?, e.transportadora = ? where e = ?", new Object[]{
			//	bean.getRastreamento(), bean.getUrlRastreamento(), bean.getValorCustoFrete(), bean.getDataPrevisaoEntrega(), bean.getTransportadoraTrans(), new Expedicao(bean.getCdExpedicaoTrans())
		//});
	}

	public void insertTabelaSincronizacaForRastreamento(Ecom_PedidoVenda bean) {
		getJdbcTemplate().execute("select sincronizacaotabelaecommerce_insert_update(7, false," + bean.getCdEcom_pedidovenda() + ");");
	}
	
	public void insertTabelaSincronizacaForRastreamentoForExpedicao(Ecom_PedidoVenda bean) {
		getJdbcTemplate().execute("select sincronizacaotabelaecommerce_insert_update(13, false," + bean.getCdEcom_pedidovenda() + ");");
	}

	public Ecom_PedidoVenda loadByVenda(Venda venda){
        return query()
        		.select("ecom_PedidoVenda.cdEcom_pedidovenda, pedidoVenda.cdpedidovenda")
        		.join("ecom_PedidoVenda.pedidoVenda pedidoVenda")
        		.where("exists(select 1 from Venda v join v.pedidovenda pv where pv = pedidoVenda and v = ?)", venda)
        		.setMaxResults(1)
        		.unique();
	}
	
	public void updateCampo(Ecom_PedidoVenda ecomPedidoVenda, String nomeCampo, Object valor) {
		if (Util.objects.isNotPersistent(ecomPedidoVenda)) {
			throw new SinedException("Par�metros inv�lidos.");
		}
		
		if(valor != null){
			getHibernateTemplate().bulkUpdate("update Ecom_PedidoVenda venda set "+nomeCampo+" = ? where venda = ?", new Object[] {valor, ecomPedidoVenda});
		}else {
			getHibernateTemplate().bulkUpdate("update Ecom_PedidoVenda venda set "+nomeCampo+" = null where venda = ?", new Object[] {ecomPedidoVenda});
		}
	}

	public List<Ecom_PedidoVenda> findByWhereInPedidovenda(String whereInPedidovenda) {
		return query()
					.select("ecom_PedidoVenda.cdEcom_pedidovenda, pedidoVenda.cdpedidovenda, ecom_PedidoVenda.id_situacao, ecom_PedidoVenda.nome_situacao")
					.join("ecom_PedidoVenda.pedidoVenda pedidoVenda")
					.whereIn("pedidoVenda.cdpedidovenda", whereInPedidovenda)
					.list();
	}

	public List<Ecom_PedidoVenda> findByWhereInVenda(String whereInVenda) {
		return query()
				.select("ecom_PedidoVenda.cdEcom_pedidovenda, pedidoVenda.cdpedidovenda, ecom_PedidoVenda.id_situacao, ecom_PedidoVenda.nome_situacao")
				.join("ecom_PedidoVenda.pedidoVenda pedidoVenda")
				.join("pedidoVenda.listaVenda venda")
				.whereIn("venda.cdvenda", whereInVenda)
				.list();
	}
	
	public List<Ecom_PedidoVenda> findWithContasEmAberto(){
		return query()
				.select("ecom_PedidoVenda.cdEcom_pedidovenda, pedidoVenda.cdpedidovenda, " +
						"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.vendasEcommerce, pedidovendatipo.geracaocontareceberEnum")
				.join("ecom_PedidoVenda.pedidoVenda pedidoVenda")
				.join("pedidoVenda.pedidovendatipo pedidovendatipo")
				.where("exists(select 1 from Documentoorigem do join do.pedidovenda pv join do.documento doc join doc.vinculoProvisionado vp join doc.documentoacao docAcao where pv = pedidoVenda and docAcao.cddocumentoacao in (1, 2) and vp.baixaautomaticafaturamento = true)")
				.where("pedidoVenda.pedidovendasituacao <> ?", Pedidovendasituacao.CANCELADO)
				.where("pedidovendatipo.vendasEcommerce = true")
				.whereIn("ecom_PedidoVenda.id_situacao", SinedUtil.WHEREIN_PROXIMAS_SITUACOES_APOS_PAGO)
				.list();
	}
}
