package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Documentoenvioboleto;
import br.com.linkcom.sined.geral.bean.Documentoenvioboletosituacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentoenvioboletosituacaoDAO extends GenericDAO<Documentoenvioboletosituacao>{

	/**
	 * Busca a lista de Documentoenvioboletosituacao
	 * 	 
	 * @param documentoenvioboleto
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/07/2018
	 */
	public List<Documentoenvioboletosituacao> findByDocumentoenvioboleto(Documentoenvioboleto documentoenvioboleto) {
		if(documentoenvioboleto == null || documentoenvioboleto.getCddocumentoenvioboleto() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("documentoenvioboletosituacao.cddocumentoenvioboletosituacao, documentoacao.cddocumentoacao, documentoacao.nome")
					.join("documentoenvioboletosituacao.documentoacao documentoacao")
					.where("documentoenvioboletosituacao.documentoenvioboleto = ?", documentoenvioboleto)
					.orderBy("documentoacao.cddocumentoacao")
					.list();
	}

}
