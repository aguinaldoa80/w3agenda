package br.com.linkcom.sined.geral.dao;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.ImportacaoProvisao;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ImportacaoProvisaoFiltro;

public class ImportacaoProvisaoDAO extends GenericDAO<ImportacaoProvisao>{
	
	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}

	@Override
	public void updateListagemQuery(QueryBuilder<ImportacaoProvisao> query, FiltroListagem _filtro) {
		ImportacaoProvisaoFiltro filtro = (ImportacaoProvisaoFiltro)_filtro;
		
		query
			.select("importacaoProvisao.cdImportacaoProvisao, importacaoProvisao.dtaltera, importacaoProvisao.cdusuarioaltera, " +
					"arquivo.cdarquivo, arquivo.nome")
			.leftOuterJoin("importacaoProvisao.arquivo arquivo");
		
		//query.where("importacaoProvisao.empresa = ?", filtro.getEmpresa());
		query.where("cast(importacaoProvisao.dtaltera as date) >= ?", filtro.getDtInicio())
			 .where("cast(importacaoProvisao.dtaltera as date) <= ?", filtro.getDtFim())
			 .orderBy("importacaoProvisao.dtaltera desc");
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				
				ImportacaoProvisao importacaoProvisao =  (ImportacaoProvisao) save.getEntity();
				
				if(importacaoProvisao.getArquivo() != null){
					arquivoDAO.saveFile(importacaoProvisao, "arquivo");
				}
				return null;
			}
		});
	}
	
}
