package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Importacaoresultado;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ImportacaoresultadoDAO extends GenericDAO<Importacaoresultado> {

	@Override
	public void updateListagemQuery(QueryBuilder<Importacaoresultado> query, FiltroListagem filtro) {
		query
			.leftOuterJoinFetch("importacaoresultado.arquivoimportado arquivoimportado")
			.leftOuterJoinFetch("importacaoresultado.arquivorelatorioresultado arquivorelatorioresultado");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Importacaoresultado> query) {
		query
			.leftOuterJoinFetch("importacaoresultado.arquivoimportado arquivoimportado")
			.leftOuterJoinFetch("importacaoresultado.arquivorelatorioresultado arquivorelatorioresultado");
	}
	
}
