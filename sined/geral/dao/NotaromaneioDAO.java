package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Notaromaneio;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotaromaneioDAO extends GenericDAO<Notaromaneio> {
	
	
	/**
	 * M�todo que carrega as notas vinculadas ao romaneio
	 *
	 * @param romaneio
	 * @return
	 * @author Lucas Costa
	 * @since 24/07/2014
	 */
	public List<Notaromaneio> findByRomaneio (Romaneio romaneio){
		if(romaneio == null || romaneio.getCdromaneio() == null){
			throw new SinedException("Venda n�o pode ser nula.");
		}
		return query()
			.select("notaromaneio.cdnotaromaneio, notaromaneio.nota")
			.leftOuterJoin("notaromaneio.nota nota")
			.where("notaromaneio.romaneio = ?", romaneio)
			.list();
	}

}