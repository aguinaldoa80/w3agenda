package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Grupotributacaoimposto;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("grupotributacaoimposto.faixaimposto.nome")
public class GrupotributacaoimpostoDAO extends GenericDAO<Grupotributacaoimposto>{

	public List<Grupotributacaoimposto> findByGrupotributacao(Grupotributacao grupotributacao){
		return query()
				.where("grupotributacaoimposto.grupotributacao = ?", grupotributacao)
				.list();
	}
	
	public Grupotributacaoimposto findByGrupotributacaoControle(Grupotributacao grupotributacao, Faixaimpostocontrole controle){
		return query()	
					.where("grupotributacaoimposto.grupotributacao = ?", grupotributacao)
					.where("grupotributacaoimposto.controle = ?", controle)
					.unique();
	}
	
}
