package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.ModeloDisponibilidade;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ModeloDisponibilidadeFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ModeloDisponibilidadeDAO extends GenericDAO<ModeloDisponibilidade>{
	@Override
	public void updateListagemQuery(QueryBuilder<ModeloDisponibilidade> query, FiltroListagem _filtro) {
		ModeloDisponibilidadeFiltro filtro = (ModeloDisponibilidadeFiltro) _filtro;
		
		query
		.select("modeloDisponibilidade.cdmodelodisponibilidade, modeloDisponibilidade.descricao")
		.whereLikeIgnoreAll("modeloDisponibilidade.descricao", filtro.getDescricao())
		.orderBy("modeloDisponibilidade.cdmodelodisponibilidade");
	}
}
