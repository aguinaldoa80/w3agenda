package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Ecom_PedidoVenda;
import br.com.linkcom.sined.geral.bean.Ecom_PedidoVendaPagamento;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class Ecom_PedidoVendaPagamentoDAO extends GenericDAO<Ecom_PedidoVendaPagamento>{

	public List<Ecom_PedidoVendaPagamento> findByPedidoVenda(Ecom_PedidoVenda pedidoVenda){
		return querySined()
				.where("ecom_PedidoVendaPagamento.ecomPedidoVenda = ?", pedidoVenda)
				.list();
	}
}
