package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Requisicaonota;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RequisicaonotaDAO extends GenericDAO<Requisicaonota> {

	/**
	 * Busca os registros de Requisicaonota a partir do whereIn de Nota
	 *
	 * @param whereInNota
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/06/2015
	 */
	public List<Requisicaonota> findByNota(String whereInNota) {
		return query()
					.select("requisicaonota.cdrequisicaonota, requisicao.cdrequisicao, materialrequisicao.cdmaterialrequisicao")
					.join("requisicaonota.materialrequisicao materialrequisicao")
					.join("requisicaonota.requisicao requisicao")
					.whereIn("requisicaonota.nota.cdNota", whereInNota)
					.list();
	}

	/**
	* M�todo que busca o vinculo entre a nota e o item da requisi��o para calcular o percentual faturado
	*
	* @param requisicao
	* @param whereInRequisicaomaterial
	* @return
	* @since 15/10/2015
	* @author Luiz Fernando
	*/
	public List<Requisicaonota> findForPercentualfaturado(Requisicao requisicao, String whereInRequisicaomaterial) {
		if(requisicao == null || requisicao.getCdrequisicao() == null || 
				StringUtils.isBlank(whereInRequisicaomaterial))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("requisicaonota.cdrequisicaonota, requisicao.cdrequisicao, materialrequisicao.cdmaterialrequisicao, " +
					"materialrequisicao.percentualfaturado, requisicaonota.percentualfaturado ")
			.join("requisicaonota.requisicao requisicao")
			.join("requisicaonota.materialrequisicao materialrequisicao")
			.where("requisicao = ?", requisicao)
			.whereIn("materialrequisicao.cdmaterialrequisicao", whereInRequisicaomaterial)
			.where("coalesce(requisicaonota.percentualfaturado, 0) > 0")
			.where("requisicaonota.nota.notaStatus <> ?", NotaStatus.CANCELADA)
			.list();
	}
	
}