package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clienteespecialidade;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ClienteespecialidadeDAO extends GenericDAO<Clienteespecialidade>{
	
	public List<Clienteespecialidade> findByCliente(Cliente cliente){
		return query()
				.select("clienteespecialidade.cdclienteespecialidade, cliente.cdpessoa, especialidade.cdespecialidade, especialidade.descricao," +
						"clienteespecialidade.registro")
				.join("clienteespecialidade.cliente cliente")
				.join("clienteespecialidade.especialidade especialidade")
				.where("cliente=?", cliente)
				.orderBy("especialidade.descricao")
				.list();
	}
}