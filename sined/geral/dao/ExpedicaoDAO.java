package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Ecom_PedidoVenda;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.EventosCorreios;
import br.com.linkcom.sined.geral.bean.EventosFinaisCorreios;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Regiao;
import br.com.linkcom.sined.geral.bean.Regiaolocal;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacaoentrega;
import br.com.linkcom.sined.geral.bean.enumeration.TipoVendedorEnum;
import br.com.linkcom.sined.geral.service.Ecom_PedidoVendaService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ExpedicaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ExpedicaoDAO extends GenericDAO<Expedicao> {
	
	private VendaService vendaService;
	private Ecom_PedidoVendaService ecom_PedidoVendaService;

	public void setEcom_PedidoVendaService(Ecom_PedidoVendaService ecom_PedidoVendaService) {this.ecom_PedidoVendaService = ecom_PedidoVendaService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}

	@Override
	public void updateListagemQuery(QueryBuilder<Expedicao> query, FiltroListagem _filtro) {
		ExpedicaoFiltro filtro = (ExpedicaoFiltro) _filtro;
		
		query
			.select("distinct expedicao.cdexpedicao, expedicao.identificadorcarregamento, expedicao.expedicaosituacao, " +
					"expedicao.expedicaosituacaoentrega ,expedicao.dtexpedicao, transportadora.nome")
			.leftOuterJoin("expedicao.transportadora transportadora")
			.leftOuterJoin("expedicao.empresa empresa")			
			.leftOuterJoin("expedicao.listaExpedicaoitem expedicaoitem")
			.leftOuterJoin("expedicaoitem.cliente cliente")
			.leftOuterJoin("expedicaoitem.enderecocliente enderecocliente")
			.leftOuterJoin("enderecocliente.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("expedicaoitem.vendamaterial vendamaterial")
			.leftOuterJoin("vendamaterial.venda venda")
			.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
			.leftOuterJoin("vendamaterial.venda venda")
			.leftOuterJoin("expedicao.eventosFinaisCorreios eventosFinaisCorreios")
			
			.where("expedicao.cdexpedicao = ?", filtro.getCdexpedicao())
			.where("expedicao.dtexpedicao >= ?", filtro.getDtexpedicao1())
			.where("expedicao.dtexpedicao <= ?", filtro.getDtexpedicao2())
			.where("expedicao.dataPrevisaoEntrega >= ?", filtro.getDtPrevisaoEntregaIni())
			.where("expedicao.dataPrevisaoEntrega <= ?", filtro.getDtPrevisaoEntregaFim())
			.where("expedicao.identificadorcarregamento = ?", filtro.getIdentificadorcarregamento())
			.where("expedicao.rastreamento = ?", filtro.getRastreamento())
			.where("empresa = ?", filtro.getEmpresa())
			.where("transportadora = ?", filtro.getTransportadora())
			.where("cliente = ?", filtro.getCliente())
			.where("uf = ?", filtro.getUf())
			.where("municipio = ?", filtro.getMunicipio())
			.where("venda.cdvenda = ?", filtro.getCodigovenda())
			.whereLikeIgnoreAll("cliente.razaosocial", filtro.getRazaoSocial())
			.where("cliente.tipopessoa = ?", filtro.getTipopessoa())
			.where("cliente.cpf = ?", filtro.getCpf())
			.where("cliente.cnpj = ?", filtro.getCnpj())
			.where("eventosFinaisCorreios = ?", filtro.getEventosFinaisCorreios())
			.orderBy("expedicao.cdexpedicao desc")
			.ignoreJoin("expedicaoitem", "cliente", "enderecocliente", "municipio", "uf", "vendamaterial", "venda", "listaClientevendedor");
		
		if(StringUtils.isNotEmpty(filtro.getIdentificadorVendaInicial()) || StringUtils.isNotEmpty(filtro.getIdentificadorVendaFinal())){
			String whereIdentificadorInicio = StringUtils.isNotEmpty(filtro.getIdentificadorVendaInicial())? filtro.getIdentificadorVendaInicial(): "v.identificador";
			String whereIdentificadorFim = StringUtils.isNotEmpty(filtro.getIdentificadorVendaFinal())? filtro.getIdentificadorVendaFinal(): "v.identificador";
			query.where("exists(select e.cdexpedicao from Expedicao e join e.listaExpedicaoitem ei join ei.vendamaterial vm join vm.venda v where e.cdexpedicao = expedicao.cdexpedicao and "+
					"v.identificador >= '"+whereIdentificadorInicio+"' and v.identificador <= '"+whereIdentificadorFim+"')");
		}
		
		if(filtro.getSelectedItensEtiqueta() != null && !filtro.getSelectedItensEtiqueta().equals("")){
			query.whereIn("expedicao.cdexpedicao", filtro.getSelectedItensEtiqueta());
		}
		
		if(filtro.getRegiao() != null){
			Regiao regiao = filtro.getRegiao();
			if(regiao.getListaRegiaolocal() != null && regiao.getListaRegiaolocal().size() > 0){
				query.openParentheses();
				for (Regiaolocal regiaolocal : regiao.getListaRegiaolocal()) {
					query
						.openParentheses()
						.where("enderecocliente.cep >= ?", regiaolocal.getCepinicio())
						.where("enderecocliente.cep <= ?", regiaolocal.getCepfinal())
						.closeParentheses()
						.or();
				}
				query.closeParentheses();
			}
		}
		
		if (filtro.getColaborador() != null && filtro.getTipoVendedor() != null && TipoVendedorEnum.PRINCIPAL.equals(filtro.getTipoVendedor())) {
			query
				.openParentheses()
					.where("listaClientevendedor.colaborador = ?", filtro.getColaborador())
					.where("listaClientevendedor.principal = ?", Boolean.TRUE)
				.closeParentheses();
		} else {
			query.where("venda.colaborador = ?",filtro.getColaborador());
		}

		if(filtro.getListaExpedicaosituacao() != null) {
			query.whereIn("expedicao.expedicaosituacao", Expedicaosituacao.listAndConcatenate(filtro.getListaExpedicaosituacao()));
		}
		
		if(filtro.getListaExpedicaosituacaoentrega() != null) {
			query.whereIn("expedicao.expedicaosituacaoentrega", Expedicaosituacaoentrega.listAndConcatenate(filtro.getListaExpedicaosituacaoentrega()));
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void updateEntradaQuery(QueryBuilder<Expedicao> query) {
		query
			.select("expedicao.cdexpedicao, expedicao.identificadorcarregamento, expedicao.dtexpedicao, expedicao.quantidadevolumes," +
					"expedicao.cdusuarioaltera, expedicao.dtaltera, expedicao.expedicaosituacao, expedicao.observacao, expedicao.expedicaosituacaoentrega, " +
					"expedicao.rastreamento, expedicao.urlRastreamento, expedicao.valorCustoFrete, expedicao.dataPrevisaoEntrega, " +
					"empresa.cdpessoa, empresa.nome, transportadora.cdpessoa, transportadora.nome," +
					"expedicaoitem.cdexpedicaoitem, expedicaoitem.dtexpedicaoitem, expedicaoitem.qtdeexpedicao, expedicaoitem.faturada, expedicaoitem.observacao," +
					"cliente.cdpessoa, cliente.nome, cliente.cnpj, cliente.cpf, " +
					"material.cdmaterial, material.identificacao, material.nome, material.obrigarlote, " +
					"unidademedidaMat.cdunidademedida, unidademedidaMat.nome, "+
					"vendamaterial.cdvendamaterial, vendamaterial.quantidade, venda.cdvenda, venda.observacao," +
					"loteEstoque.cdloteestoque, loteEstoque.numero, loteEstoque.validade, " +
					"loteEstoqueVendaMaterial.cdloteestoque, loteEstoqueVendaMaterial.numero, loteEstoqueVendaMaterial.validade, " +
					"enderecocliente.cdendereco, contatocliente.cdpessoa, vendamaterial.comprimento, vendamaterial.altura, vendamaterial.largura, " +
					"coletaMaterial.cdcoletamaterial," +
					"coleta.cdcoleta," +
					"expedicaohistorico.cdexpedicaohistorico, expedicaohistorico.dtaltera, " +
					"expedicaohistorico.cdusuarioaltera, expedicaohistorico.expedicaoacao, " +
					"pedidovenda.cdpedidovenda,pedidovenda.identificacaoexterna," +
					"expedicaohistorico.observacao, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome," +
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedidaVendaMaterial.cdunidademedida, unidademedidaVendaMaterial.nome, " +
					"expedicaoitem.ordementrega, expedicao.inspecaocompleta," +
					"notaFiscalProdutoItem.cdnotafiscalprodutoitem," +
					"notafiscalproduto.cdNota,notafiscalproduto.numero,notafiscalproduto.situacaoExpedicao," +
					"materialVendaMaterial.cdmaterial, materialVendaMaterial.vendapromocional, unidademedidamvm.cdunidademedida, unidademedidamvm.nome," +
					"expedicao.conferenciamateriais, venda.identificador, venda.identificadorexterno, expedicao.quantidadevolumes, " +
					"eventoCorreios.cdeventoscorreios, eventoCorreios.descricao, " +
					"eventosFinaisCorreios.cdeventosfinaiscorreios, eventosFinaisCorreios.nome, eventosFinaisCorreios.corLetra, eventosFinaisCorreios.corFundo, " +
					"eventosFinaisCorreios.letra, eventosFinaisCorreios.finalizadoSucesso")
			.leftOuterJoin("expedicao.listaExpedicaohistorico expedicaohistorico")
			.leftOuterJoin("expedicao.listaExpedicaoitem expedicaoitem")
			.leftOuterJoin("expedicaoitem.enderecocliente enderecocliente")
			.leftOuterJoin("expedicaoitem.contatocliente contatocliente")
			.leftOuterJoin("expedicaoitem.material material")
			.leftOuterJoin("material.unidademedida unidademedidaMat")
			.leftOuterJoin("expedicaoitem.pneu pneu")
			.leftOuterJoin("expedicaoitem.loteEstoque loteEstoque")
			.leftOuterJoin("expedicaoitem.coletaMaterial coletaMaterial")
			.leftOuterJoin("coletaMaterial.coleta coleta")
			.leftOuterJoin("coleta.pedidovenda pedidovenda")
			.leftOuterJoin("expedicaoitem.notaFiscalProdutoItem notaFiscalProdutoItem")
			.leftOuterJoin("notaFiscalProdutoItem.notafiscalproduto notafiscalproduto")
			.leftOuterJoin("expedicaoitem.unidademedida unidademedida")
			.leftOuterJoin("expedicaoitem.cliente cliente")
			.leftOuterJoin("expedicaoitem.vendamaterial vendamaterial")
			.leftOuterJoin("vendamaterial.loteestoque loteEstoqueVendaMaterial")
			.leftOuterJoin("vendamaterial.unidademedida unidademedidaVendaMaterial")
			.leftOuterJoin("expedicaoitem.localarmazenagem localarmazenagem")
			.leftOuterJoin("vendamaterial.venda venda")
			.leftOuterJoin("expedicao.empresa empresa")
			.leftOuterJoin("expedicao.transportadora transportadora")
			.leftOuterJoin("vendamaterial.material materialVendaMaterial")
			.leftOuterJoin("materialVendaMaterial.unidademedida unidademedidamvm")
			.leftOuterJoin("expedicao.eventoCorreios eventoCorreios")
			.leftOuterJoin("expedicao.eventosFinaisCorreios eventosFinaisCorreios")
			.orderBy("expedicaoitem.cdexpedicaoitem, expedicaohistorico.dtaltera desc");
		query = SinedUtil.setJoinsByComponentePneu(query);
	}

	/**
	 * M�todo para atualiza��o da situa��o da(s) expedi��o(��es) passadas por par�metro.
	 *
	 * @param whereIn
	 * @param situacao
	 * @since 16/07/2012
	 * @author Rodrigo Freitas
	 */
	public void updateSituacao(String whereIn, Expedicaosituacao situacao, Integer volumes) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		if(volumes != null){
			getHibernateTemplate().bulkUpdate("update Expedicao e set e.expedicaosituacao = ?, e.quantidadevolumes = ? where e.cdexpedicao in ("+whereIn+")", new Object[]{situacao, volumes});
		}else {
			getHibernateTemplate().bulkUpdate("update Expedicao e set e.expedicaosituacao = ? where e.cdexpedicao in ("+whereIn+")", new Object[]{situacao});
		}
		
		vendaService.verificaExpedicaoSituacao(whereIn, null);
	}
	
	public void updateSituacaoentrega(String whereIn, Expedicaosituacaoentrega situacao) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		getHibernateTemplate().bulkUpdate("update Expedicao e set e.expedicaosituacaoentrega = ? where e.cdexpedicao in ("+whereIn+")", new Object[]{situacao});		
		vendaService.verificaExpedicaoSituacao(whereIn, null);
	}

	/**
	 * Busca registro de Expedicao para a confirma��o.
	 *
	 * @param whereIn
	 * @return
	 * @since 16/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Expedicao> findForConfirmacao(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		return query()
					.select("expedicao.cdexpedicao, expedicao.urlRastreamento, empresa.cdpessoa, " +
							"expedicaoitem.cdexpedicaoitem, expedicaoitem.qtdeexpedicao, expedicaoitem.dtexpedicaoitem, " +
							"material.cdmaterial, material.producao, material.produto, material.epi, material.nome, material.servico, " +
							"vendamaterial.comprimento, vendamaterial.altura, vendamaterial.largura, materialproducao.servico, materialproducao.nome," +
							"materialproducao.cdmaterial, materialproducao.producao, materialproducao.produto, materialproducao.epi, " +
							"vendamaterial.cdvendamaterial, vendamaterial.quantidade, vendamaterial.preco, vendamaterial.desconto, " +
							"venda.cdvenda, localarmazenagem.cdlocalarmazenagem, listaProducao.consumo, vendamaterial.comprimento, " +
							"materialv.cdmaterial, materialv.nome, unidademedidav.cdunidademedida, vendamaterial.fatorconversao, " +
							"localarmazenagemItem.cdlocalarmazenagem, localarmazenagemItem.nome, vendamaterial.qtdereferencia, projeto.cdprojeto," +
							"loteestoque.cdloteestoque, loteEstoqueExpedicaoitem.cdloteestoque, materialmestregrade.cdmaterial," +
							"materialmestregrade.nome, materialgrupo.cdmaterialgrupo, materialgrupo.gradeestoquetipo," +
							"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.baixaestoqueEnum, pedidovendatipo.gerarexpedicaovenda, producaoetapa.cdproducaoetapa," +
							"pedidovendatipo.permitirvendasemestoque, " +
							"unidademedidaExp.cdunidademedida, " +
							"unidademedidaMat.cdunidademedida")
					.leftOuterJoin("expedicao.empresa empresa")
					.leftOuterJoin("expedicao.listaExpedicaoitem expedicaoitem")
					.leftOuterJoin("expedicaoitem.vendamaterial vendamaterial")
					.leftOuterJoin("expedicaoitem.localarmazenagem localarmazenagemItem")
					.leftOuterJoin("vendamaterial.loteestoque loteestoque")
					.leftOuterJoin("expedicaoitem.loteEstoque loteEstoqueExpedicaoitem")
					.leftOuterJoin("vendamaterial.material materialv")
					.leftOuterJoin("expedicaoitem.unidademedida unidademedidaExp")
					.leftOuterJoin("vendamaterial.unidademedida unidademedidav")
					.leftOuterJoin("vendamaterial.venda venda")
					.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
					.leftOuterJoin("venda.projeto projeto")
					.leftOuterJoin("venda.localarmazenagem localarmazenagem")
					.leftOuterJoin("expedicaoitem.material material")
					.leftOuterJoin("material.unidademedida unidademedidaMat")
					.leftOuterJoin("material.materialmestregrade materialmestregrade")
					.leftOuterJoin("material.materialgrupo materialgrupo")
					.leftOuterJoin("material.producaoetapa producaoetapa")
					.leftOuterJoin("material.listaProducao listaProducao")
					.leftOuterJoin("listaProducao.material materialproducao")
					.whereIn("expedicao.cdexpedicao", whereIn)
					.list();
	}
	
	public List<Expedicao> findForConfereincia(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		return query()
					.select("expedicao.cdexpedicao, expedicao.expedicaosituacao")
					.whereIn("expedicao.cdexpedicao", whereIn)
					.list();
	}

	/**
	 * Verifica se existe alguma expedi��o com situa��es diferentes das passadas por par�metro.
	 *
	 * @param whereIn
	 * @param situacoes
	 * @return
	 * @since 17/07/2012
	 * @author Rodrigo Freitas
	 */
	public boolean haveExpedicaoNotInSituacao(String whereIn, Expedicaosituacao[] situacoes) {
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Expedicao.class)
					.setUseTranslator(false)
					.whereIn("expedicao.cdexpedicao", whereIn);
		
		for (int i = 0; i < situacoes.length; i++) {
			query.where("expedicao.expedicaosituacao <> ?", situacoes[i]);
		}		
		
		return query.unique() > 0;
	}
	
	public boolean haveExpedicaoBySituacao(String whereIn, Expedicaosituacao situacao) {
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Expedicao.class)
					.setUseTranslator(false)
					.whereIn("expedicao.cdexpedicao", whereIn)
					.where("expedicao.expedicaosituacao = ?", situacao);
		
		return query.unique() > 0;
	}

	/**
	 * Retorna a quantidade j� expedida de um item determinado da venda.
	 *
	 * @param vendamaterial
	 * @return
	 * @since 18/07/2012
	 * @author Rodrigo Freitas
	 */
	public Double getQtdeExpedidaVendamaterial(Vendamaterial vendamaterial, Material material) {
		return newQueryBuilderSined(Double.class)
					.setUseReadOnly(false)
					.select("coalesce(sum(expedicaoitem.qtdeexpedicao),0.0)")
					.setUseTranslator(false)
					.from(Expedicaoitem.class)
					.join("expedicaoitem.expedicao expedicao")
					.join("expedicaoitem.vendamaterial vendamaterial")
					.join("expedicaoitem.material material")
					.where("vendamaterial = ?", vendamaterial)
					.where("material = ?", material)
					.where("expedicao.expedicaosituacao <> ?", Expedicaosituacao.CANCELADA)
					.unique();
	}

	/**
	 * Busca os registros para o Gerar PDF da listagem de Expedi��o.
	 *
	 * @param filtro
	 * @return
	 * @since 23/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Expedicao> findForGerarPDF(ExpedicaoFiltro filtro) {
		QueryBuilder<Expedicao> query = query();
		this.updateListagemQuery(query, filtro);
		query.select(query.getSelect().getValue() + ", venda.cdvenda");
		query.setIgnoreJoinPaths(new ListSet<String>(String.class));
		return query.list();
	}

	/**
	 * Busca os registro modificando a query do updateListagemQuery para a impress�o das etiquetas
	 *
	 * @param filtro
	 * @return
	 * @since 24/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Expedicao> findForEtiquetas(ExpedicaoFiltro filtro) {
		QueryBuilder<Expedicao> query = query();
		this.updateListagemQuery(query, filtro);

		query.setIgnoreJoinPaths(new HashSet<String>());
		
		query
			.select("expedicao.cdexpedicao, empresa.nome, empresa.razaosocial, empresa.nomefantasia, venda.cdvenda, " +
					"cliente.nome, contatocliente.nome, enderecocliente.logradouro, " +
					"enderecocliente.numero, enderecocliente.complemento, enderecocliente.bairro, " +
					"municipio.nome, uf.sigla, material.nome, material.cdmaterial, material.identificacao, " +
					"venda.cdvenda, vendamaterial.altura, vendamaterial.comprimento, vendamaterial.largura, " +
					"materialmestregrade.cdmaterial, materialmestregrade.identificacao, materialmestregrade.nome," +
					"expedicaoitem.qtdeexpedicao, vendamaterial.altura, vendamaterial.largura, vendamaterial.comprimento")
			.leftOuterJoin("expedicaoitem.contatocliente contatocliente")
			.leftOuterJoin("expedicaoitem.material material")
			.leftOuterJoin("material.materialmestregrade materialmestregrade")
			;
		
		return query.list();
	}

	/**
	 * Verifica se existe alguma expedi��o de origem da venda que n�o est� faturada.
	 *
	 * @param venda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/02/2013
	 */
	public boolean haveExpedicaoNaoFaturada(Venda venda) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Expedicao.class)
					.join("expedicao.listaExpedicaoitem expedicaoitem")
					.join("expedicaoitem.vendamaterial vendamaterial")
					.join("vendamaterial.venda venda")
					.where("venda = ?", venda)
					.openParentheses()
					.where("expedicaoitem.faturada is null")
					.or()
					.where("expedicaoitem.faturada = ?", Boolean.FALSE)
					.closeParentheses()
					.unique() > 0;
	}
	
	/**
	 * M�todo que retorna true caso a venda tenha expedi��o de acordo com a situa��o
	 *
	 * @param venda
	 * @param expedicaosituacao
	 * @return
	 * @author Luiz Fernando
	 * @since 29/01/2014
	 */
	public boolean haveExpedicaoByVenda(Venda venda, Integer cdExpedicaoExcecao, Expedicaosituacao expedicaosituacao) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Expedicao.class)
					.join("expedicao.listaExpedicaoitem expedicaoitem")
					.join("expedicaoitem.vendamaterial vendamaterial")
					.join("vendamaterial.venda venda")
					.where("venda = ?", venda)
					.where("expedicao.expedicaosituacao = ?", expedicaosituacao)
					.where("expedicao.cdexpedicao <> ?", cdExpedicaoExcecao)
					.unique() > 0;
	}
	
	public boolean haveExpedicaoByVendaNotInSituacao(Venda venda, Expedicao expedicaoExcecao, Expedicaosituacao... expedicaosituacao) {
		StringBuilder values = new StringBuilder();
		for (int i=0; i < expedicaosituacao.length; i++) {
			values.append(expedicaosituacao[i].getValue()).append(",");
		}
		String whereInSituacao = values.toString().substring(0, values.toString().length()-1);
		return newQueryBuilderSined(Long.class)
					.setUseReadOnly(false)
					.select("count(*)")
					.from(Expedicao.class)
					.join("expedicao.listaExpedicaoitem expedicaoitem")
					.where("expedicaoitem.vendamaterial.venda = ?", venda)
					.whereIn("not expedicao.expedicaosituacao", whereInSituacao)
					.where("expedicao <> ?", expedicaoExcecao)
					.unique() > 0;
					
	}
	
	/**
	 * M�todo que retorna true caso a venda/nota j� tenha gerado entrada/sa�da no estoque
	 *
	 * @param expedicaoitem
	 * @return
	 * @author Luiz Fernando
	 * @since 29/01/2014
	 */
	public boolean haveExpedicaoByVendaWithMovimentacaoestoque(Expedicaoitem expedicaoitem, Movimentacaoestoquetipo movimentacaoestoquetipo) {
		SinedUtil.markAsReader();
		long countNota = getJdbcTemplate().queryForLong(
						" select count(*) " +
						" from expedicaoitem ei " +
						" join vendamaterial vm on vm.cdvendamaterial = ei.cdvendamaterial " +
						" join notavenda nv on nv.cdvenda = vm.cdvenda " +
						" join movimentacaoestoqueorigem meo on meo.cdnota = nv.cdnota " +
						" join movimentacaoestoque me on me.cdmovimentacaoestoqueorigem = meo.cdmovimentacaoestoqueorigem " +
						" where ei.cdexpedicaoitem = " + expedicaoitem.getCdexpedicaoitem() +
						" and me.dtcancelamento is null " + 
						(movimentacaoestoquetipo != null ? " and me.cdmovimentacaoestoquetipo = " + movimentacaoestoquetipo.getCdmovimentacaoestoquetipo() : ""));

		SinedUtil.markAsReader();
		long countVenda = getJdbcTemplate().queryForLong(
						" select count(*) " +
						" from expedicaoitem ei " +
						" join vendamaterial vm on vm.cdvendamaterial = ei.cdvendamaterial " +
						" join movimentacaoestoqueorigem meo on meo.cdvenda = vm.cdvenda " +
						" join movimentacaoestoque me on me.cdmovimentacaoestoqueorigem = meo.cdmovimentacaoestoqueorigem " +
						" where ei.cdexpedicaoitem = " + expedicaoitem.getCdexpedicaoitem() +
						" and me.dtcancelamento is null " + 
						(movimentacaoestoquetipo != null ? " and me.cdmovimentacaoestoquetipo = " + movimentacaoestoquetipo.getCdmovimentacaoestoquetipo() : ""));
		
		return countNota > 0 || countVenda > 0;
	}

	/**
	 * M�todo que carrega as expedi��es para inspe��o
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 28/11/2013
	 */
	public List<Expedicao> carregaExpedicaoParaInspecao(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("O c�digo da expedi��o n�o pode ser nulo.");
		}
		return query()
			.select("expedicao.cdexpedicao, expedicao.inspecaocompleta, listaExpedicaoitem.cdexpedicaoitem, listaExpedicaoitem.qtdeexpedicao, material.cdmaterial, material.nome, material.identificacao, " +
					"listaInspecao.cdexpedicaoiteminspecao, listaInspecao.observacao, listaInspecao.conforme, inspecaoitem.cdinspecaoitem, inspecaoitem.nome, unidademedida.simbolo, " +
					"expedicaoitem.cdexpedicaoitem ")
			.join("expedicao.listaExpedicaoitem listaExpedicaoitem")
			.join("listaExpedicaoitem.listaInspecao listaInspecao")
			.join("listaInspecao.expedicaoitem expedicaoitem")
			.join("listaExpedicaoitem.material material")
			.join("material.unidademedida unidademedida")
			.leftOuterJoin("listaInspecao.inspecaoitem inspecaoitem")
			.whereIn("expedicao.cdexpedicao", whereIn)
			.orderBy("material.nome, inspecaoitem.nome")
			.list();
	}
	
	
	/**
	 * M�todo que atualiza o status da inspe��o da expedi��o
	 *
	 * @param expedicao
	 * @author Luiz Fernando
	 * @since 28/11/2013
	 */
	public void doUpdateInspecaoExpedicao(Expedicao expedicao) {
		Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		
		StringBuilder sql = new StringBuilder()
			.append("update Expedicao e set e.cdusuarioaltera = ?, e.dtaltera = ?, e.inspecaocompleta = ? " +
					"where e.cdexpedicao = "+expedicao.getCdexpedicao());
		
		getHibernateTemplate().bulkUpdate(sql.toString(), new Object[]{cdusuarioaltera,hoje, Boolean.TRUE});
	}
	
	public Expedicao findByCdExpedicao(Integer id) {
		if(id == null){
			throw new SinedException("Valor inv�lido.");
		}
		return query()
				.select("expedicao.cdexpedicao,expedicao.expedicaosituacao,expedicao.identificadorcarregamento")
				.where("expedicao.cdexpedicao = ?", id)
				.unique();
	}
	
	public Expedicao findByCdExpedicaoForEmail(Integer id) {
		if(id == null){
			throw new SinedException("Valor inv�lido.");
		}
		return query()
				.select("expedicao.cdexpedicao,expedicao.expedicaosituacao, expedicao.expedicaosituacao, expedicao.rastreamento, expedicao.urlRastreamento,"+
						"listaExpedicaoitem.cdexpedicaoitem, listaExpedicaoitem.dtexpedicaoitem, " +
						"listaExpedicaoitem.ordementrega, listaExpedicaoitem.faturada, listaExpedicaoitem.qtdeexpedicao," +
						"cliente.cdpessoa, cliente.email, cliente.nome, material.cdmaterial, material.nome," +
						"transportadora.nome, transportadora.transportador, transportadora.correios")						
				.join("expedicao.listaExpedicaoitem listaExpedicaoitem")
				.join("listaExpedicaoitem.cliente cliente")			
				.join("listaExpedicaoitem.material material")
				.leftOuterJoin("expedicao.transportadora transportadora")
				.where("expedicao.cdexpedicao = ?", id)
				.unique();
	}
	
	/**
	 * M�todo que busca as expedi��es para imprimir inspe��o
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 28/11/2013
	 */
	public List<Expedicao> findForRelatorioInspecaoExpedicao(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("expedicao.cdexpedicao, material.cdmaterial, material.nome, empresa.cdpessoa, empresa.nome, empresa.razaosocial, " +
					"material.identificacao, listaExpedicaoitem.qtdeexpedicao, unidademedida.simbolo, " +
					"listaInspecao.conforme, listaInspecao.observacao, inspecaoitem.nome")
			.join("expedicao.listaExpedicaoitem listaExpedicaoitem")
			.join("listaExpedicaoitem.listaInspecao listaInspecao")
			.join("listaExpedicaoitem.material material")
			.join("material.unidademedida unidademedida")
			.join("listaInspecao.inspecaoitem inspecaoitem")
			.leftOuterJoin("expedicao.empresa empresa")
			.whereIn("expedicao.cdexpedicao",whereIn)
			.orderBy("expedicao.cdexpedicao desc, material.nome, inspecaoitem.nome")
			.list();
	}

	/**
	 * 
	 * @param whereIn
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Expedicao> findByUnificacao(String whereIn) {
		QueryBuilder<Expedicao> query = query()
			.select("expedicao.cdexpedicao, expedicao.identificadorcarregamento, expedicao.dtexpedicao," +
					"empresa.cdpessoa, empresa.nome, empresa.razaosocial, transportadora.cdpessoa, transportadora.nome," +
					"listaExpedicaoitem.cdexpedicaoitem, listaExpedicaoitem.dtexpedicaoitem, " +
					"listaExpedicaoitem.ordementrega, listaExpedicaoitem.faturada, listaExpedicaoitem.qtdeexpedicao," +
					"cliente.cdpessoa, cliente.nome, material.cdmaterial, material.nome, vendamaterial.cdvendamaterial," +
					"vendamaterial.quantidade, unidademedida.cdunidademedida, unidademedida.nome, localarmazenagem.cdlocalarmazenagem, " +
					"localarmazenagem.nome, enderecocliente.cdendereco, enderecocliente.logradouro, enderecocliente.bairro, " +
					"enderecocliente.numero, enderecocliente.complemento, enderecocliente.cep, municipio.cdmunicipio, municipio.nome," +
					"uf.cduf, uf.sigla, venda.cdvenda, venda.identificador, coletaMaterial.cdcoletamaterial, notaFiscalProdutoItem.cdnotafiscalprodutoitem, " +
					"coleta.cdcoleta, loteEstoque.cdloteestoque, loteEstoque.numero, loteEstoque.validade ")
			.join("expedicao.empresa empresa")			
			.join("expedicao.listaExpedicaoitem listaExpedicaoitem")
			.join("listaExpedicaoitem.cliente cliente")			
			.join("listaExpedicaoitem.material material")			
			.leftOuterJoin("listaExpedicaoitem.unidademedida unidademedida")
			.leftOuterJoin("listaExpedicaoitem.vendamaterial vendamaterial")
			.leftOuterJoin("vendamaterial.venda venda")
			.leftOuterJoin("expedicao.transportadora transportadora")
			.leftOuterJoin("listaExpedicaoitem.localarmazenagem localarmazenagem")
			.leftOuterJoin("listaExpedicaoitem.enderecocliente enderecocliente")
			.leftOuterJoin("enderecocliente.municipio municipio")
			.leftOuterJoin("municipio.uf uf")		
			.leftOuterJoin("listaExpedicaoitem.coletaMaterial coletaMaterial")
			.leftOuterJoin("coletaMaterial.coleta coleta")
			.leftOuterJoin("listaExpedicaoitem.notaFiscalProdutoItem notaFiscalProdutoItem")
			.leftOuterJoin("listaExpedicaoitem.pneu pneu")
			.leftOuterJoin("listaExpedicaoitem.loteEstoque loteEstoque")
			.whereIn("expedicao.cdexpedicao", whereIn)
			.orderBy("expedicao.cdexpedicao, listaExpedicaoitem.ordementrega");
		query = SinedUtil.setJoinsByComponentePneu(query);
		return query.list();
	}
	
	/**
	 * 
	 * @param whereIn
	 * @return
	 */
	public List<Expedicao> carregaListaPorId(String whereIn){
		return query()
					.select("expedicao.cdexpedicao, expedicao.identificadorcarregamento, empresa.nome, empresa.razaosocial, transportadora.nome, expedicao.dtexpedicao")
					.join("expedicao.empresa empresa")
					.join("expedicao.transportadora transportadora")
					.whereIn("expedicao.cdexpedicao",whereIn)
					.list();
	}
	
	public List<Expedicao> findForEmissaoExpedicao(String whereIn){
		return query()
					.select("expedicao.cdexpedicao, transportadora.nome, expedicao.dtexpedicao, expedicao.observacao, " +
							"venda.cdvenda, venda.dtvenda, pedidovenda.cdpedidovenda, cliente.nome, material.cdmaterial, material.nome, material.identificacao, contatocliente.nome, " +
							"enderecocliente.logradouro, enderecocliente.numero, enderecocliente.bairro, enderecocliente.complemento, " +
							"municipio.nome, uf.sigla, vendamaterial.quantidade, listaExpedicaoitem.qtdeexpedicao, material.pesobruto, " +
							"venda.observacao, pedidovenda.observacao, nota.numero, notaStatus.cdNotaStatus, vendamaterial.observacao, " +
							"vendamaterial.fatorconversao, vendamaterial.qtdereferencia, unidademedidaMaterial.cdunidademedida, unidademedida.cdunidademedida, " +
							"unidademedidaConversao.cdunidademedida, listaMaterialunidademedida.fracao, listaMaterialunidademedida.qtdereferencia, " +
							"pneu.cdpneu,pneu.serie,pneu.dot,pneu.numeroreforma, pneu.descricao, " +
							"pneu.acompanhaRoda, pneu.lonas, otrPneuTipo.cdOtrPneuTipo, " +
							"coletaMaterial.cdcoletamaterial,coletaMaterial.quantidadedevolvida, " +
							"pneumarca.cdpneumarca,pneumarca.nome, " +
							"pneumodelo.cdpneumodelo,pneumodelo.nome, " +
							"pneumedida.cdpneumedida,pneumedida.nome, " +
							"materialbanda.cdmaterial,materialbanda.nome,coleta.cdcoleta, " +
							"pneuqualificacao.cdpneuqualificacao,pneuqualificacao.nome, " +
							"coleta.cdcoleta, " +
							"listaExpedicaoitem.ordementrega, " +
							"pedidovendaColeta.cdpedidovenda,pedidovendaColeta.identificacaoexterna, " +
							"loteEstoque.cdloteestoque, loteEstoque.numero, loteEstoque.validade, " +
							"materialmestregrade.cdmaterial, materialmestregrade.nome," +
							"listaTelefone.telefone, telefonetipo.cdtelefonetipo, telefonetipo.nome  ")
					.leftOuterJoin("expedicao.transportadora transportadora")
					.leftOuterJoin("expedicao.listaExpedicaoitem listaExpedicaoitem")
					.leftOuterJoin("listaExpedicaoitem.pneu pneu")
					.leftOuterJoin("listaExpedicaoitem.loteEstoque loteEstoque")
					.leftOuterJoin("pneu.pneumarca pneumarca")
					.leftOuterJoin("pneu.materialbanda materialbanda")
					.leftOuterJoin("pneu.pneumodelo pneumodelo")
					.leftOuterJoin("pneu.pneumedida pneumedida")
					.leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
					.leftOuterJoin("pneu.otrPneuTipo otrPneuTipo")
					.leftOuterJoin("listaExpedicaoitem.coletaMaterial coletaMaterial")
					.leftOuterJoin("coletaMaterial.coleta coleta")
					.leftOuterJoin("coleta.pedidovenda pedidovendaColeta")
					.leftOuterJoin("listaExpedicaoitem.cliente cliente")
					.leftOuterJoin("cliente.listaTelefone listaTelefone")
					.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
					.leftOuterJoin("listaExpedicaoitem.unidademedida unidademedida")
					.leftOuterJoin("listaExpedicaoitem.vendamaterial vendamaterial")
					.leftOuterJoin("vendamaterial.venda venda")
					.leftOuterJoin("venda.listaNotavenda notavenda")
					.leftOuterJoin("notavenda.nota nota")
					.leftOuterJoin("nota.notaStatus notaStatus")
					.leftOuterJoin("venda.pedidovenda pedidovenda")
					.leftOuterJoin("listaExpedicaoitem.material material")
					.leftOuterJoin("material.materialmestregrade materialmestregrade")
					.leftOuterJoin("material.unidademedida unidademedidaMaterial")
					.leftOuterJoin("material.listaMaterialunidademedida listaMaterialunidademedida")
					.leftOuterJoin("listaMaterialunidademedida.unidademedida unidademedidaConversao")
					.leftOuterJoin("listaExpedicaoitem.contatocliente contatocliente")
					.leftOuterJoin("listaExpedicaoitem.enderecocliente enderecocliente")
					.leftOuterJoin("enderecocliente.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.whereIn("expedicao.cdexpedicao",whereIn)
					.list();
	}
	
	/**
	 * 
	 * @param expedicao
	 * @return
	 */
	public Integer getOrdemEntregaByExpedicao(Expedicao expedicao) {
		SinedUtil.markAsReader();
		 Integer ordemEntrega = getJdbcTemplate().queryForInt(
				" select max(ordementrega) " +
				" from expedicaoitem " +
				" where cdexpedicao = " + expedicao.getCdexpedicao() +
				" and ordementrega is not null");		 
		 return ordemEntrega!=null?ordemEntrega:0;
	}
	
	public boolean mesmaMatrizExpedicao(String whereIn) {
		SinedUtil.markAsReader();
		long contador = getJdbcTemplate().queryForLong("select count(*) as contador " +
														"from ( " +
														"select substr(coalesce(p.cnpj, p.cpf), 0, 9) as matriz " +
														"from expedicaoitem ei " +
														"join vendamaterial vm on vm.cdvendamaterial = ei.cdvendamaterial "  +
														"join venda v on v.cdvenda = vm.cdvenda " +
														"join pessoa p on p.cdpessoa = v.cdcliente " +
														"where ei.cdexpedicao in (" + whereIn + ") " +
														"group by substr(coalesce(p.cnpj, p.cpf), 0, 9) " +
														") query");

		SinedUtil.markAsReader();
		long contadorCliente = getJdbcTemplate().queryForLong("select count(*) as contador " +
																"from ( " +
																"select cdpessoa as matriz " +
																"from expedicaoitem ei " +
																"join vendamaterial vm on vm.cdvendamaterial = ei.cdvendamaterial "  +
																"join venda v on v.cdvenda = vm.cdvenda " +
																"join pessoa p on p.cdpessoa = v.cdcliente " +
																"where v.cdvenda in (" + whereIn + ") " +
																"group by p.cdpessoa " +
																") query");
		
		return contador == 1 && contadorCliente > 1;
	}

	public List<Expedicao> findForNota(String whereInexpedicao) {
		if(whereInexpedicao == null || whereInexpedicao.equals(""))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("expedicao.cdexpedicao, expedicao.identificadorcarregamento, " +
						"expedicao.dtexpedicao, expedicao.observacao, expedicao.expedicaosituacao, expedicao.quantidadevolumes, " +
						"empresaE.cdpessoa, empresaE.nome, " +
						"transportadora.cdpessoa, transportadora.nome, " +
						"listaExpedicaoitem.cdexpedicaoitem, listaExpedicaoitem.dtexpedicaoitem, " +
						"listaExpedicaoitem.qtdeexpedicao, listaExpedicaoitem.faturada, " +
						"listaExpedicaoitem.ordementrega, listaExpedicaoitem.fatorconversao, " +
						"listaExpedicaoitem.qtdereferencia, loteEstoque.cdloteestoque, loteEstoque.numero, " +
						"notaFiscalProdutoItem.cdnotafiscalprodutoitem," +
						"vendamaterial.cdvendamaterial, vendamaterial.quantidade, vendamaterial.comprimento, " +
						"vendamaterial.comprimentooriginal, vendamaterial.largura, vendamaterial.altura, vendamaterial.fatorconversao, " +
						"vendamaterial.qtdereferencia, vendamaterial.preco, vendamaterial.desconto, vendamaterial.prazoentrega, " +
						"vendamaterial.observacao, vendamaterial.multiplicador, " +
						"venda.cdvenda, " +
						"materialVM.cdmaterial, materialVM.identificacao, materialVM.nome, materialVM.peso, materialVM.pesobruto, " +
						"unidademedidaVM.cdunidademedida, unidademedidaVM.nome, unidademedidaVM.simbolo, " +
						"loteestoqueVM.cdloteestoque, loteestoqueVM.numero, loteestoqueVM.validade, " +
						"clienteEI.cdpessoa, clienteEI.nome, clienteEI.cpf, clienteEI.cnpj, " +
						"materialEI.cdmaterial, materialEI.identificacao, materialEI.nome, materialEI.ncmcompleto, materialEI.nve, materialEI.peso, materialEI.pesobruto, " +
						"materialgrupoEI.cdmaterialgrupo, " +
						"localarmazenagemEI.cdlocalarmazenagem, localarmazenagemEI.nome, " +
						"enderecoclienteEI.cdendereco, " +
						"unidademedidaEI.cdunidademedida, unidademedidaEI.nome")
				.leftOuterJoin("expedicao.empresa empresaE")
				.leftOuterJoin("expedicao.transportadora transportadora")
				.leftOuterJoin("expedicao.listaExpedicaoitem listaExpedicaoitem")
				.leftOuterJoin("listaExpedicaoitem.notaFiscalProdutoItem notaFiscalProdutoItem")
				.leftOuterJoin("listaExpedicaoitem.vendamaterial vendamaterial")
				.leftOuterJoin("vendamaterial.venda venda")
				.leftOuterJoin("vendamaterial.material materialVM")
				.leftOuterJoin("vendamaterial.unidademedida unidademedidaVM")
				.leftOuterJoin("vendamaterial.loteestoque loteestoqueVM")
				.leftOuterJoin("listaExpedicaoitem.cliente clienteEI")
				.leftOuterJoin("listaExpedicaoitem.material materialEI")
				.leftOuterJoin("materialEI.materialgrupo materialgrupoEI")
				.leftOuterJoin("listaExpedicaoitem.localarmazenagem localarmazenagemEI")
				.leftOuterJoin("listaExpedicaoitem.enderecocliente enderecoclienteEI")
				.leftOuterJoin("listaExpedicaoitem.unidademedida unidademedidaEI")
				.leftOuterJoin("listaExpedicaoitem.loteEstoque loteEstoque")
				.whereIn("expedicao.cdexpedicao", whereInexpedicao)
				.list();
	}
	
	
	
	public Boolean existExpedicaoNotConfirmada(String whereIn) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Expedicao.class)
					.whereIn("expedicao.cdexpedicao", whereIn)
					.where("expedicao.expedicaosituacao <> ?", Expedicaosituacao.CONFIRMADA)
					.unique() > 0;
	}
	
	public void updateConferenciamateriaisExpedicao(String whereIn){
		if (whereIn==null || whereIn.equals("")){
			throw new SinedException("Par�metros inv�lidos.");
		}
		
		getHibernateTemplate().bulkUpdate("update Expedicao e set e.conferenciamateriais=? where e.cdexpedicao in (" + whereIn + ")", new Object[]{Boolean.TRUE});
	}
	
	public boolean isNaoPossuiConferenciaMateriais(String whereIn) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Expedicao.class)
					.whereIn("expedicao.cdexpedicao", whereIn)
					.where("coalesce(expedicao.conferenciamateriais, false)=?", Boolean.FALSE)
					.unique()>0;
	}

	/**
	 * Retorna a quantidade j� expedida de um item determinado da venda. Usado na unifica��o de expedi��o
	 *
	 * @param vendamaterial
	 * @param material
	 * @param expedicaoExcecao
	 * @return
	 * @since 15/05/2017
	 * @author Mairon Cezar
	 */
	public Double getQtdeExpedidaVendamaterialExceto(Vendamaterial vendamaterial, Material material, Expedicao expedicaoExcecao){
		return newQueryBuilderSined(Double.class)
		.select("coalesce(sum(expedicaoitem.qtdeexpedicao),0.0)")
		.setUseTranslator(false)
		.from(Expedicaoitem.class)
		.join("expedicaoitem.expedicao expedicao")
		.join("expedicaoitem.vendamaterial vendamaterial")
		.join("expedicaoitem.material material")
		.where("vendamaterial = ?", vendamaterial)
		.where("material = ?", material)
		.where("expedicao.expedicaosituacao <> ?", Expedicaosituacao.CANCELADA)
		.where("expedicao <> ?", expedicaoExcecao)
		.unique();		
	}
	
	/**
	 * M�todo que carrega as informa��es para a listagem
	 *
	 * @param whereIn
	 * @param filtro
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/06/2017
	 */
	public List<Expedicao> loadForListagem(String whereIn, ExpedicaoFiltro filtro, String orderBy, boolean asc) {
		QueryBuilder<Expedicao> query = query()
				.select("expedicao.cdexpedicao, expedicao.identificadorcarregamento, " +
						"expedicao.expedicaosituacao, expedicao.expedicaosituacaoentrega, transportadora.nome, expedicao.dtexpedicao, " +
						"expedicao.inspecaocompleta, venda.cdvenda, venda.identificador, venda.dtvenda,"+
						"cliente.tipopessoa, cliente.cdpessoa, cliente.razaosocial, cliente.nome, cliente.cpf, cliente.cnpj,"+
						"endereco.bairro, municipio.nome, uf.sigla, enderecotipo.cdenderecotipo, eventoCorreios.cdeventoscorreios, eventoCorreios.descricao, "+
						"eventosFinaisCorreios.cdeventosfinaiscorreios, eventosFinaisCorreios.nome, eventosFinaisCorreios.corLetra, eventosFinaisCorreios.corFundo, " +
						"eventosFinaisCorreios.letra, eventosFinaisCorreios.finalizadoSucesso")
				.leftOuterJoin("expedicao.transportadora transportadora")
				.leftOuterJoin("expedicao.listaExpedicaoitem expedicaoitem")
				.leftOuterJoin("expedicaoitem.cliente cliente")
				.leftOuterJoin("cliente.listaEndereco endereco")
                .leftOuterJoin("endereco.enderecotipo enderecotipo")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("expedicaoitem.vendamaterial vendamaterial")
				.leftOuterJoin("vendamaterial.venda venda")
				.leftOuterJoin("expedicao.eventoCorreios eventoCorreios")
				.leftOuterJoin("expedicao.eventosFinaisCorreios eventosFinaisCorreios")
				.whereIn("expedicao.cdexpedicao", whereIn);
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("expedicao.cdexpedicao DESC");
		}
		
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Expedicao> findReservaByExpedicoes(String whereInExpedicao){
		String sql = "select distinct(q.cdexpedicao) from "+
					"(select e.cdexpedicao "+
					"  from reserva r "+
					"  join expedicao e on e.cdexpedicao = r.cdexpedicao "+
					" where e.cdexpedicao in("+whereInExpedicao+") "+
					" union all "+
					" select ei.cdexpedicao "+
					"  from reserva r "+
					"  join vendamaterial vm on vm.cdvendamaterial = r.cdvendamaterial "+
					"  join expedicaoitem ei on ei.cdvendamaterial = vm.cdvendamaterial "+
					" where ei.cdexpedicao in("+whereInExpedicao+")" +
					") as q";
		SinedUtil.markAsReader();
		List<Expedicao> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Expedicao lista = new Expedicao(
						rs.getInt("cdexpedicao"));
			
				return lista;
			}
		});
		return list;
	}
	
	public List<Expedicao> findForImprimirSeparacao(String whereIn){
		if(StringUtils.isEmpty(whereIn)){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("expedicao.cdexpedicao, expedicao.identificadorcarregamento, expedicao.observacao, " +
						"material.cdmaterial, material.identificacao, material.nome, " +
						"materialfornecedor.fabricante, fornecedor.cdpessoa, fornecedor.nome, " +
						"listaExpedicaoitem.qtdeexpedicao, listaExpedicaoitem.observacao, " +
						"unidademedida.cdunidademedida, unidademedida.nome, " +
						"loteestoque.numero, loteestoque.validade, " +
						"listaLotematerial.cdlotematerial, listaLotematerial.validade, materialLote.cdmaterial, " +
						"empresa.cdpessoa, empresa.nome, empresa.nomefantasia, " +
						"venda.cdvenda, venda.identificador, venda.dtvenda, venda.observacao, " +
						"colaborador.cdpessoa, colaborador.nome, " +
						"frete.cdResponsavelFrete, frete.nome," +
						"endereco.cdendereco, endereco.numero, endereco.cep, endereco.logradouro, endereco.bairro, " +
						"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla, " +
						"cliente.cdpessoa, cliente.razaosocial, cliente.nome," +
						"enderecoCliente.cdendereco, enderecoCliente.numero, enderecoCliente.cep, enderecoCliente.logradouro, enderecoCliente.bairro, " +
						"clienteExpedicaoitem.cdpessoa, clienteExpedicaoitem.razaosocial, clienteExpedicaoitem.nome, " +
						"municipioCliente.cdmunicipio, municipioCliente.nome, ufCliente.cduf, ufCliente.sigla ")
				.leftOuterJoin("expedicao.empresa empresa")
				.leftOuterJoin("expedicao.listaExpedicaoitem listaExpedicaoitem")
				.leftOuterJoin("listaExpedicaoitem.cliente clienteExpedicaoitem")
				.leftOuterJoin("listaExpedicaoitem.enderecocliente enderecoCliente")
				.leftOuterJoin("enderecoCliente.municipio municipioCliente")				
				.leftOuterJoin("municipioCliente.uf ufCliente")
				.leftOuterJoin("listaExpedicaoitem.material material")
				.leftOuterJoin("material.listaFornecedor materialfornecedor")
				.leftOuterJoin("materialfornecedor.fornecedor fornecedor")
				.leftOuterJoin("listaExpedicaoitem.loteEstoque loteestoque")
				.leftOuterJoin("loteestoque.listaLotematerial listaLotematerial")
				.leftOuterJoin("listaLotematerial.material materialLote")
				.leftOuterJoin("listaExpedicaoitem.unidademedida unidademedida")
				.leftOuterJoin("listaExpedicaoitem.vendamaterial vendamaterial")
				.leftOuterJoin("vendamaterial.venda venda")
				.leftOuterJoin("venda.cliente cliente")
				.leftOuterJoin("venda.colaborador colaborador")
				.leftOuterJoin("venda.endereco endereco")				
				.leftOuterJoin("endereco.municipio municipio")				
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("venda.frete frete")
				.whereIn("expedicao.cdexpedicao", whereIn)
				.orderBy("expedicao.cdexpedicao, venda.cdvenda, cliente.cdpessoa, fornecedor.nome, material.nome ")
				.list();
	}

	public boolean existsIdentificadorCarregamentoEmpresa(Expedicao bean, Integer idProximoCarregamento, Empresa empresa) {
		if(idProximoCarregamento == null)
			return false;
		
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Expedicao.class)
				.leftOuterJoin("expedicao.empresa empresa")
				.where("expedicao <> ?", bean)
				.where("empresa = ?", empresa)
				.where("expedicao.identificadorcarregamento = ?", Long.parseLong(idProximoCarregamento.toString()))
				.where("expedicao.expedicaosituacao <> 3")
				.unique() > 0;
	}
	
	public List<Expedicao> findByExpedicaosituacao(String whereIn, Expedicaosituacao situacao){
		return querySined()
				.where("expedicao.expedicaosituacao = ?", situacao)
				.whereIn("expedicao.cdexpedicao", whereIn)
				.list();
	}
	
	public List<Expedicao> findForEstornar(String whereIn){
		if(StringUtils.isEmpty(whereIn)){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return querySined()
				.select("expedicao.cdexpedicao, expedicao.expedicaosituacao, listaExpedicaoitem.cdexpedicaoitem, " +
						"vendamaterial.cdvendamaterial, venda.cdvenda, venda.quantidadevolumes ")
				.leftOuterJoin("expedicao.listaExpedicaoitem listaExpedicaoitem")
				.leftOuterJoin("listaExpedicaoitem.vendamaterial vendamaterial")
				.leftOuterJoin("vendamaterial.venda venda")
				.whereIn("expedicao.cdexpedicao", whereIn)
				.list();
	}
	
	public Expedicao loadWithVendas(Expedicao expedicao){
		if(!Util.objects.isPersistent(expedicao)) {
			throw new SinedException("Expedi��o n�o pode ser nula.");
		}
		
		return querySined()
				.select("expedicaoItem.cdexpedicaoitem, vendamaterial.cdvendamaterial, venda.cdvenda, venda.quantidadevolumes, venda.identificador")
				.join("expedicao.listaExpedicaoitem expedicaoItem")
				.join("expedicaoItem.vendamaterial vendamaterial")
				.join("vendamaterial.venda venda")
				.where("expedicao = ?", expedicao)
				.unique();
	}
	
	public void limparQuantidadeVolume(Expedicao expedicao) {
		getHibernateTemplate().bulkUpdate("update Expedicao e set e.quantidadevolumes = null where e = ?", expedicao);
	}

	public List<Expedicao> findExpedicaoSituacaoByVenda(Venda venda, Expedicaosituacao expedicaosituacao) {
		return query()
				.select("expedicao.cdexpedicao, expedicao.expedicaosituacao,expedicao.expedicaosituacaoentrega")
				.join("expedicao.listaExpedicaoitem listaExpedicaoitem")
				.join("listaExpedicaoitem.vendamaterial vendamaterial")
				.join("vendamaterial.venda venda")
				.where("expedicao.expedicaosituacao <> ?", expedicaosituacao)
				.where("venda = ?", venda)
				.list();
	}
	
	public List<Expedicao> findForEtiquetaExpedicao(String whereIn){
		return query().select("expedicao.cdexpedicao, expedicao.quantidadevolumes,"+ 
					"empresa.nome, empresa.razaosocial, empresa.nomefantasia, venda.cdvenda, " +
					"cliente.tipopessoa, cliente.nome, cliente.razaosocial, contatocliente.nome, enderecocliente.logradouro, enderecocliente.complemento," +
					"enderecoempresa.numero, enderecoempresa.complemento, enderecoempresa.bairro, enderecoempresa.logradouro, enderecoempresa.cep, "+
				    "municipioempresa.nome, ufempresa.cduf, ufempresa.nome, ufempresa.sigla," +
				    "enderecocliente.numero, enderecocliente.cep, enderecocliente.complemento, enderecocliente.bairro, " +
					"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.nome, uf.sigla, venda.cdvenda, venda.identificador")
					
				.leftOuterJoin("expedicao.transportadora transportadora")
				.leftOuterJoin("expedicao.empresa empresa")
				.leftOuterJoin("empresa.listaEndereco enderecoempresa")
				.leftOuterJoin("enderecoempresa.municipio municipioempresa")
				.leftOuterJoin("municipioempresa.uf ufempresa")
				
				.leftOuterJoin("expedicao.listaExpedicaoitem expedicaoitem")
				.leftOuterJoin("expedicaoitem.cliente cliente")
				.leftOuterJoin("cliente.listaEndereco enderecocliente")
				.leftOuterJoin("enderecocliente.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				
				.leftOuterJoin("expedicaoitem.vendamaterial vendamaterial")
				.leftOuterJoin("vendamaterial.venda venda")
				.leftOuterJoin("expedicaoitem.contatocliente contatocliente")
		.whereIn("expedicao.cdexpedicao", whereIn)
		.list();
	}
	
	public void insertVendaEntregueTabelaSincronizacaoEcommerce(Ecom_PedidoVenda bean) {
		getJdbcTemplate().execute("select sincronizacaotabelaecommerce_insert_update(9,false," + bean.getCdEcom_pedidovenda() + ");");
	}
	
	public void insertExpedicaoCriadaTabelaSincronizacaoEcommerce(Venda venda) {
		Ecom_PedidoVenda bean = ecom_PedidoVendaService.loadByVenda(venda);
		getJdbcTemplate().execute("select sincronizacaotabelaecommerce_insert_update(12,false," + bean.getCdEcom_pedidovenda() + ");");
	}
	
	public List<Expedicao> findForRastreamentoCorreios(Empresa empresa, Expedicao expedicao){
		return querySined()
				.leftOuterJoin("expedicao.eventoCorreios eventoCorreios")
				.join("expedicao.transportadora transportadora")
				.where("transportadora.correios = true")
				.where("coalesce(expedicao.rastreamento, '') <> ''")
				.openParentheses()
					.where("eventoCorreios is null")
					.or()
					.where("coalesce(eventoCorreios.eventoFinal, false) <> true")
				.closeParentheses()
				.where("expedicao.expedicaosituacao <> ?", Expedicaosituacao.CANCELADA)
				.where("expedicao.empresa = ?", empresa)
				.where("expedicao = ?", expedicao)
				.list();
	}
	
	public Expedicao loadForRastreio(Expedicao expedicao){
		return querySined()
				.select("expedicao.cdexpedicao, expedicao.rastreamento, empresa.cdpessoa, empresa.usuarioCorreios, empresa.senhaCorreios, "+
						"transportadora.cdpessoa, transportadora.correios")
				.leftOuterJoin("expedicao.empresa empresa")
				.leftOuterJoin("expedicao.transportadora transportadora")
				.where("expedicao = ?", expedicao)
				.setMaxResults(1)
				.unique();
	}
	
	public void updateEventoCorreios(Expedicao expedicao, EventosCorreios eventoCorreios){
		getHibernateTemplate().bulkUpdate("update Expedicao e set e.eventoCorreios=? where e = ?", new Object[]{eventoCorreios, expedicao});
	}
	
	public void updateEventoFinaisCorreios(Expedicao expedicao, EventosFinaisCorreios eventoFinaisCorreios){
		getHibernateTemplate().bulkUpdate("update Expedicao e set e.eventosFinaisCorreios=? where e = ?", new Object[]{eventoFinaisCorreios, expedicao});
	}
	
	public Expedicao loadWithExpedicaoEntregaSituacao(Expedicao expedicao) {
		return querySined()
				.select("expedicao.cdexpedicao, expedicao.expedicaosituacaoentrega")
				.where("expedicao = ?", expedicao)
				.setMaxResults(1)
				.unique();
	}
}