package br.com.linkcom.sined.geral.dao;


import java.util.List;

import br.com.linkcom.sined.geral.bean.Movimentacaocontabil;
import br.com.linkcom.sined.geral.bean.Movimentacaocontabildebitocredito;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MovimentacaocontabildebitocreditoDAO extends GenericDAO<Movimentacaocontabildebitocredito>{
	
	/**
	 * 
	 * @param movimentacaocontabil
	 * @param propertyMovimentacaocontabil
	 * @author Thiago Clemente
	 * 
	 */
	public List<Movimentacaocontabildebitocredito> findByMovimentacaocontabil(Movimentacaocontabil movimentacaocontabil, String propertyMovimentacaocontabil){
		return query()
				.select("movimentacaocontabildebitocredito.cdmovimentacaocontabildebitocredito, movimentacaocontabildebitocredito.valor," +
						propertyMovimentacaocontabil + ".cdmovimentacaocontabil, contaContabil.cdcontacontabil, contaContabil.nome," +
						"centrocusto.cdcentrocusto, centrocusto.nome, projeto.cdprojeto, projeto.nome, movimentacaocontabildebitocredito.historico ")
				.join("movimentacaocontabildebitocredito." + propertyMovimentacaocontabil + " " + propertyMovimentacaocontabil)
				.join("movimentacaocontabildebitocredito.contaContabil contaContabil")
				.leftOuterJoin("movimentacaocontabildebitocredito.centrocusto centrocusto")
				.leftOuterJoin("movimentacaocontabildebitocredito.projeto projeto")
				.where(propertyMovimentacaocontabil + "=?", movimentacaocontabil)
				.orderBy("contaContabil.nome")
				.list();
	}
	
	public List<Movimentacaocontabildebitocredito> findWithIdentificadorByMovimentacaocontabil(Movimentacaocontabil movimentacaocontabil, String propertyMovimentacaocontabil){
		return query()
				.select("movimentacaocontabildebitocredito.cdmovimentacaocontabildebitocredito, movimentacaocontabildebitocredito.valor," +
						propertyMovimentacaocontabil + ".cdmovimentacaocontabil, contaContabil.cdcontacontabil, contaContabil.nome," +
						"contaContabil.codigoalternativo, vcontacontabildebito.identificador, centrocusto.cdcentrocusto, centrocusto.nome, " +
						"projeto.cdprojeto, projeto.nome, movimentacaocontabildebitocredito.historico ")
				.join("movimentacaocontabildebitocredito." + propertyMovimentacaocontabil + " " + propertyMovimentacaocontabil)
				.join("movimentacaocontabildebitocredito.contaContabil contaContabil")
				.leftOuterJoin("movimentacaocontabildebitocredito.centrocusto centrocusto")
				.leftOuterJoin("movimentacaocontabildebitocredito.projeto projeto")
				.leftOuterJoin("contaContabil.vcontacontabil vcontacontabildebito")
				.where(propertyMovimentacaocontabil + "=?", movimentacaocontabil)
				.orderBy("contaContabil.nome")
				.list();
	}
}
