package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.Embalagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EmbalagemDAO extends GenericDAO<Embalagem>{

	public Embalagem findEmbalagemByMaterialCodigoBarras(Material material, String codigoBarras, Unidademedida unidademedidaCodigoBarras) {
		return querySined()
				.setUseReadOnly(true)
				.select("unidadeMedidaEmbalagem.cdunidademedida")
				.join("embalagem.material material")
				.join("embalagem.unidadeMedida unidadeMedidaEmbalagem")
				.where("material = ?", material)
				.whereLike("embalagem.codigoBarras", codigoBarras)
				.where("unidadeMedidaEmbalagem = ?", unidademedidaCodigoBarras)
				.unique();
	}

}
