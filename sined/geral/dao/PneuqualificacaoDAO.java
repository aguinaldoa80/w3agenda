package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Pneuqualificacao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PneuqualificacaoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("pneuqualificacao.nome")
public class PneuqualificacaoDAO extends GenericDAO<Pneuqualificacao>{

	@Override
	public void updateListagemQuery(QueryBuilder<Pneuqualificacao> query, FiltroListagem _filtro) {
		PneuqualificacaoFiltro filtro = (PneuqualificacaoFiltro)_filtro;
		
		query.whereLikeIgnoreAll("pneuqualificacao.nome", filtro.getNome());
		query.where("coalesce(pneuqualificacao.garantia, false)=?", filtro.getGarantia());
		query.where("coalesce(pneuqualificacao.ativo, false)=?", filtro.getAtivo());
		query.orderBy("pneuqualificacao.nome");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Pneuqualificacao> query) {
		query.select("pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome, pneuqualificacao.garantia, pneuqualificacao.ativo");
	}
	
	public List<Pneuqualificacao> findForW3Producao(String whereIn){
		return query()
				.select("pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome, pneuqualificacao.garantia, pneuqualificacao.ativo")
				.whereIn("pneuqualificacao.cdpneuqualificacao", whereIn)
				.list();
	}
	
	/**
	* M�todo autocomplete para buscar qualifica��o de pneu
	*
	* @param q
	* @return
	* @since 25/10/2017
	* @author Mairon Cezar
	*/
	public List<Pneuqualificacao> findAutocomplete(String q) {
		return query()
			.select("pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome")
			.whereLikeIgnoreAll("pneuqualificacao.nome", q)
			.autocomplete()
			.list();
	}
}
