package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Empresaconfiguracaonf;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.EmpresaconfiguracaonfFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EmpresaconfiguracaonfDAO extends GenericDAO<Empresaconfiguracaonf>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Empresaconfiguracaonf> query, FiltroListagem _filtro) {
		EmpresaconfiguracaonfFiltro filtro = (EmpresaconfiguracaonfFiltro) _filtro;
		
		query
			.select("empresaconfiguracaonf.cdempresaconfiguracaonf, empresaconfiguracaonf.descricao")
			.whereLikeIgnoreAll("empresaconfiguracaonf.descricao", filtro.getDescricao());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Empresaconfiguracaonf> query) {
		query.leftOuterJoinFetch("empresaconfiguracaonf.listaCampo listaCampo").orderBy("listaCampo.linha, listaCampo.coluna");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaCampo");
	}
	
	public boolean verificaConfiguracao() {
		return newQueryBuilderSined(Long.class)
		.select("count(*)")
		.from(Empresaconfiguracaonf.class)
		.unique() > 0;
	}
	
	@Override
	public ListagemResult<Empresaconfiguracaonf> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Empresaconfiguracaonf> query = query();
		updateListagemQuery(query, filtro);
		
		return new ListagemResult<Empresaconfiguracaonf>(query, filtro);
	}
}
