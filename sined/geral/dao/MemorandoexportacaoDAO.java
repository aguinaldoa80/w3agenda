package br.com.linkcom.sined.geral.dao;
import java.util.List;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Memorandoexportacao;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MemorandoexportacaoDAO extends GenericDAO<Memorandoexportacao>{
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaitens");
	}
	
	/**
	 * @param whereIn
	 * @return
	 * @author Andrey Leonardo
	 */
	public List<Memorandoexportacao> loadByNotafiscalproduto(String whereIn){
		return query()
		.select("memorandoexportacao.cdmemorandoexportacao, memorandoexportacao.numero," +
				"memorandoexportacao.registroexportacao, memorandoexportacao.declaracaoexportacao," +
				"memorandoexportacao.dtdeclaracaoexportacao, memorandoexportacao.conhecimentoembarque," +
				"memorandoexportacao.dtregistroexportacao, memorandoexportacao.dtconhecimentoembarque," +
				"notafiscalproduto.cdNota, notafiscalproduto.numero, remetente.cdpessoa, remetente.razaosocial," +
				"memorandoexportacaonotafiscalprodutoitem.cdmemorandoexportacaonotafiscalprodutoitem," +
				"notaprodutoitem.cdnotafiscalprodutoitem")
				.leftOuterJoin("memorandoexportacao.notafiscalproduto notafiscalproduto")
				.leftOuterJoin("memorandoexportacao.listaitens memorandoexportacaonotafiscalprodutoitem")
				.leftOuterJoin("memorandoexportacaonotafiscalprodutoitem.notafiscalprodutoitem notaprodutoitem")
				.leftOuterJoin("memorandoexportacao.remetente remetente")
				.whereIn("notafiscalproduto.cdNota", whereIn)
				.list();
	}
	
	
	/**
	 * @param whereIn
	 * @return
	 * @author Andrey Leonardo
	 */
	public List<Memorandoexportacao> loadForMemorando(String whereIn){
		
		return query().select("memorandoexportacao.cdmemorandoexportacao,memorandoexportacao.numero, memorandoexportacao.registroexportacao," +
				"memorandoexportacao.declaracaoexportacao, memorandoexportacao.conhecimentoembarque, memorandoexportacao.dtregistroexportacao," +
				"memorandoexportacao.dtdeclaracaoexportacao, memorandoexportacao.dtconhecimentoembarque, memorandoexportacao.dtemissao, " +
				"notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.dtEmissao, empresa.cdpessoa, empresa.razaosocial," +
				"enderecoempresa.logradouro, enderecoempresa.bairro, ufempresa.cduf, ufempresa.sigla, municipioempresa.nome, municipioempresa.cdmunicipio," +
				"enderecoempresa.cdendereco, enderecoempresa.complemento, enderecoempresa.descricao, enderecoempresa.substituirlogradouro, empresa.cnpj," +
				"empresa.inscricaoestadual, cliente.cdpessoa, cliente.razaosocial, cliente.inscricaoestadual, paiscliente.nome," +
				"enderecocliente.cdendereco, enderecocliente.logradouro, enderecocliente.bairro, municipiocliente.cdmunicipio, municipiocliente.nome," +
				"enderecocliente.complemento, enderecocliente.descricao, enderecocliente.substituirlogradouro," +
				"ufcliente.cduf, ufcliente.sigla, cliente.cnpj, notafiscalprodutoitem.cdnotafiscalprodutoitem, notafiscalprodutoitem.qtde, " +
				"notafiscalprodutoitem.ncmcompleto, notafiscalprodutoitem.valorunitario, notafiscalprodutoitem.valorbruto, materialsaida.nome," +
				"unidademedidasaida.simbolo, fornecedor.cdpessoa, fornecedor.razaosocial, fornecedor.inscricaoestadual, fornecedor.cnpj, " +
				"enderecofornecedor.cdendereco, municipiofornecedor.cdmunicipio, enderecofornecedor.logradouro, enderecofornecedor.bairro, " +
				"enderecofornecedor.complemento, enderecofornecedor.descricao, enderecofornecedor.substituirlogradouro," +
				"municipiofornecedor.nome, uffornecedor.sigla, uffornecedor.cduf,entregadocumento.cdentregadocumento, entregadocumento.numero, entregadocumento.serie, entregadocumento.dtemissao," +
				"entregamaterial.cdentregamaterial, entregamaterial.qtde, modelodocumentofiscal.codigo, unidademedidaentregamaterial.simbolo, materialentregamaterial.ncmcompleto, materialentregamaterial.nome," +
				"transportador.cdpessoa, transportador.razaosocial, transportador.inscricaoestadual, transportador.cnpj, enderecotransportador.cdendereco," +
				"enderecotransportador.logradouro, enderecotransportador.bairro, enderecotransportador.complemento, enderecotransportador.descricao, " +
				"enderecotransportador.substituirlogradouro, municipiotransportador.cdmunicipio, municipiotransportador.nome, uftransportador.cduf," +
				"uftransportador.sigla, listaReferenciada.chaveacesso")
				.leftOuterJoin("memorandoexportacao.notafiscalproduto notafiscalproduto")
				.leftOuterJoin("memorandoexportacao.remetente fornecedor")
				.leftOuterJoin("notafiscalproduto.cliente cliente")
				.leftOuterJoin("notafiscalproduto.empresa empresa")
				.leftOuterJoin("notafiscalproduto.transportador transportador")
				.leftOuterJoin("notafiscalproduto.enderecotransportador enderecotransportador")
				.leftOuterJoin("enderecotransportador.municipio municipiotransportador")
				.leftOuterJoin("municipiotransportador.uf uftransportador")
				.leftOuterJoin("empresa.listaEndereco enderecoempresa")
				.leftOuterJoin("enderecoempresa.municipio municipioempresa")
				.leftOuterJoin("municipioempresa.uf ufempresa")				
				.leftOuterJoin("notafiscalproduto.enderecoCliente enderecocliente")
				.leftOuterJoin("enderecocliente.municipio municipiocliente")
				.leftOuterJoin("municipiocliente.uf ufcliente")
				.leftOuterJoin("enderecocliente.pais paiscliente")
				.leftOuterJoin("memorandoexportacao.listaitens memorandoexportacaonotafiscalprodutoitem")
				.leftOuterJoin("memorandoexportacaonotafiscalprodutoitem.notafiscalprodutoitem notafiscalprodutoitem")
				.leftOuterJoin("notafiscalprodutoitem.listaNotaprodutoitementregamaterial notaprodutoitementregamaterial")
				.leftOuterJoin("notaprodutoitementregamaterial.entregamaterial entregamaterial")
				.leftOuterJoin("entregamaterial.entregadocumento entregadocumento")				
				.leftOuterJoin("entregamaterial.material materialentregamaterial")
				.leftOuterJoin("materialentregamaterial.unidademedida unidademedidaentregamaterial")
				.leftOuterJoin("fornecedor.listaEndereco enderecofornecedor")
				.leftOuterJoin("enderecofornecedor.municipio municipiofornecedor")
				.leftOuterJoin("municipiofornecedor.uf uffornecedor")
				.leftOuterJoin("notafiscalprodutoitem.unidademedida unidademedidasaida")
				.leftOuterJoin("notafiscalprodutoitem.material materialsaida")
				.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
				.leftOuterJoin("notafiscalproduto.listaReferenciada listaReferenciada")
				.whereIn("memorandoexportacao.cdmemorandoexportacao", whereIn)
				.list();
	}
	
	/**
	 * @param whereIn
	 * @return
	 * @author Andrey Leonardo
	 */
	public List<Memorandoexportacao> loadForDeleteMemorandoexportacao(String whereIn){
		return query().select("memorandoexportacao.cdmemorandoexportacao, " +
				"memorandoexportacaonotafiscalprodutoitem.cdmemorandoexportacaonotafiscalprodutoitem")
		.leftOuterJoin("memorandoexportacao.listaitens memorandoexportacaonotafiscalprodutoitem")
		.whereIn("memorandoexportacao.cdmemorandoexportacao", whereIn)
		.list();
	}
	
	/**
	 * @param notafiscalproduto
	 * @return
	 * @author Andrey Leonardo
	 */
	public Boolean isExisteMemorandoexportacao(Notafiscalproduto notafiscalproduto){
		return newQueryBuilder(Long.class)
		.select("count(*)")
		.setUseTranslator(false)
		.from(beanClass)
		.join("memorandoexportacao.notafiscalproduto notafiscalproduto")
		.where("notafiscalproduto = ?", notafiscalproduto)
		.unique() > 0;
	}
}
