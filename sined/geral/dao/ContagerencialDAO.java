package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Composicaoorcamento;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tarefaorcamento;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.enumeration.AnaliseReceitaDespesaOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContagerencial;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.contabil.controller.report.bean.EmitirBalancoPatrimonialBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.AnaliseRecDespAnaliticoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.AnaliseRecDespBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.AnaliseReceitaDespesaReportFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.crud.bean.ApuracaoResultadoProjetoBean;
import br.com.linkcom.sined.modulo.projeto.controller.crud.bean.ApuracaoResultadoProjetoListagemBean;
import br.com.linkcom.sined.modulo.projeto.controller.crud.bean.Enumeration.ApuracaoResultadoProjetoListagemTipo;
import br.com.linkcom.sined.modulo.projeto.controller.process.filter.ApuracaoResultadoProjetoFiltro;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ContagerencialFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

@DefaultOrderBy("contagerencial.nome")
public class ContagerencialDAO extends GenericDAO<Contagerencial>{
	private ParametrogeralService parametrogeralService;
		
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Contagerencial> query) {
		query.leftOuterJoinFetch("contagerencial.contagerencialpai contagerencialpai")
			.leftOuterJoinFetch("contagerencial.tipooperacao tipooperacao")
			.leftOuterJoinFetch("contagerencial.operacaocontabil operacaocontabil")
			.joinFetch("contagerencial.vcontagerencial vcontagerencial");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Contagerencial> query, FiltroListagem _filtro) {
		ContagerencialFiltro filtro = (ContagerencialFiltro) _filtro;
		query
			.select("contagerencial.cdcontagerencial, contagerencial.nome, tipooperacao.nome, contagerencial.ativo, contagerencial.formato," +
					"vcontagerencial.nivel, vcontagerencial.identificador, operacaocontabil.nome, operacaocontabil.cdoperacaocontabil, contagerencial.codigoalternativo" )
					
			.join("contagerencial.vcontagerencial vcontagerencial")
			.leftOuterJoin("contagerencial.tipooperacao tipooperacao")
			.leftOuterJoin("contagerencial.operacaocontabil operacaocontabil")
			.where("contagerencial.natureza = ?",filtro.getNatureza())
			.where("contagerencial.ativo = ?",filtro.getMostraativo())
			.where("contagerencial.geramovimentacao = ?",filtro.getMostramovimentacao())
			.whereLikeIgnoreAll("contagerencial.nome", filtro.getNome())
			.where("vcontagerencial.identificador like ?||'%'", filtro.getIdentificador())
			.where("tipooperacao = ?",filtro.getTipooperacao())
			.where("tipooperacao <> ?", Tipooperacao.TIPO_CONTABIL)
			.where("vcontagerencial.nivel = ?",filtro.getNivel())
			.where("operacaocontabil = ?" , filtro.getOperacaocontabil())
			.where("contagerencial.codigoalternativo = ?" , filtro.getCodigoalternativo())
			.orderBy("vcontagerencial.identificador");
	}
	

	/**
	 * Carrega o tipo de opera��o de uma conta gerencial.
	 * 
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Contagerencial carregaConta(Contagerencial bean) {
		if (bean == null || bean.getCdcontagerencial() == null) {
			throw new SinedException("Conta gerencial n�o pode ser nula.");
		}
		return query()
					.select("tipooperacao.cdtipooperacao, vcontagerencial.nivel, contagerencial.natureza")
					.join("contagerencial.tipooperacao tipooperacao")
					.join("contagerencial.vcontagerencial vcontagerencial")
					.where("contagerencial = ?",bean)
					.unique();
	}
	
	/**
	 * Encontra a listagem dos filhos da conta gerencial pai fornecida.
	 * 
	 * @param pai
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contagerencial> findFilhos(Contagerencial pai){
		if (pai == null || pai.getCdcontagerencial() == null) {
			throw new SinedException("Conta gerencial n�o pode ser nula.");
		}
		return query()
					.select("contagerencial.item, contagerencial.formato")
					.join("contagerencial.contagerencialpai pai")
					.where("pai = ?",pai)
					.orderBy("contagerencial.item desc")
					.list();
	}
	
	
	/**
	 * Encontra somente as raizes da arvore.
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contagerencial> findRaiz(){
		return query()
					.select("contagerencial.item, contagerencial.formato")
					.where("contagerencialpai is null")
					.orderBy("contagerencial.item desc")
					.list();
	}
	
	
	/**
	 * Carrega a lista de contas gerenciais analiticas.
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 * @author Fl�vio Tavares
	 */
	public List<Contagerencial> findAnaliticas(Tipooperacao tipooperacao){
		if(tipooperacao == null || tipooperacao.getCdtipooperacao() == null){
			throw new SinedException("Os par�metros tipoperacao ou cdtipooperacao n�o podem ser null.");
		}
		return query()
					.select("contagerencial.cdcontagerencial, contagerencial.nome, pai.cdcontagerencial, " +
							"v.arvorepai")
					.join("contagerencial.vcontagerencial v")
					.leftOuterJoin("contagerencial.contagerencialpai pai")
					.where("not exists(select C2.cdcontagerencial from Contagerencial C2 " +
							"where C2.contagerencialpai = contagerencial)")
					.where("contagerencial.ativo = ?",Boolean.TRUE)
					.where("contagerencial.tipooperacao = ?",tipooperacao)
					.orderBy("contagerencial.nome")
					.list();
		
	}
	
	/**
	 * Carrega a lista de contas gerenciais analiticas de acordo com a natureza
	 *
	 * @param tipooperacao
	 * @param whereInNatureza
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contagerencial> findAnaliticasByNatureza(Tipooperacao tipooperacao, String whereInNatureza){
		if(tipooperacao == null || tipooperacao.getCdtipooperacao() == null){
			throw new SinedException("Os par�metros tipoperacao ou cdtipooperacao n�o podem ser null.");
		}
		return querySined()
					
					.select("contagerencial.cdcontagerencial, contagerencial.nome, pai.cdcontagerencial, " +
							"v.arvorepai")
					.join("contagerencial.vcontagerencial v")
					.leftOuterJoin("contagerencial.contagerencialpai pai")
					.where("not exists(select C2.cdcontagerencial from Contagerencial C2 " +
							"where C2.contagerencialpai = contagerencial)")
					.where("contagerencial.ativo = ?",Boolean.TRUE)
					.where("contagerencial.tipooperacao = ?",tipooperacao)
					.whereIn("contagerencial.natureza", whereInNatureza)
					.orderBy("contagerencial.nome")
					.list();
		
	}
	
	/**
	 * Carrega a lista de contas gerenciais de acordo com a natureza
	 *
	 * @param whereInNatureza
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contagerencial> findByNatureza(String whereInNatureza){
		return querySined()
					
					.select("contagerencial.cdcontagerencial, contagerencial.nome, pai.cdcontagerencial, " +
							"v.arvorepai")
					.join("contagerencial.vcontagerencial v")
					.leftOuterJoin("contagerencial.contagerencialpai pai")
					.where("not exists(select C2.cdcontagerencial from Contagerencial C2 " +
							"where C2.contagerencialpai = contagerencial)")
					.where("contagerencial.ativo = ?",Boolean.TRUE)
					.whereIn("contagerencial.natureza", whereInNatureza)
					.orderBy("contagerencial.nome")
					.list();
		
	}
	
	public List<Contagerencial> findAnaliticas(String whereIn, Tipooperacao tipooperacao) {
		if(tipooperacao == null || tipooperacao.getCdtipooperacao() == null){
			throw new SinedException("Os par�metros tipoperacao ou cdtipooperacao n�o podem ser null.");
		}
		return querySined()
			
			.select("contagerencial.cdcontagerencial, contagerencial.nome, pai.cdcontagerencial, " +
					"v.arvorepai")
			.join("contagerencial.vcontagerencial v")
			.leftOuterJoin("contagerencial.contagerencialpai pai")
			.where("not exists(select C2.cdcontagerencial from Contagerencial C2 " +
					"where C2.contagerencialpai = contagerencial)")
			.where("contagerencial.ativo = ?",Boolean.TRUE)
			.where("contagerencial.tipooperacao = ?",tipooperacao)
			.whereLikeIgnoreAll("contagerencial.nome", whereIn)
			.orderBy("contagerencial.nome")
			.list();
	}
	
	/**
	 * M�todo que faz o update do id do pai para o id do filho de todos os relacionamentos do BD.
	 * A query pega os relacionamentos dinamicamente, n�o � preciso atualizar a query a medida que colocar contagerencial em algum bean.
	 *
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public void updateRelacionamento(Contagerencial bean){
		if(bean == null || bean.getCdcontagerencial() == null){
			throw new SinedException("Conta gerencial n�o pode ser nulo.");
		}
		if (bean.getContagerencialpai() == null || bean.getContagerencialpai().getCdcontagerencial() == null) {
			throw new SinedException("Conta gerencial pai n�o pode ser nulo.");
		}
		
//		List<String> listaUpdate = getJdbcTemplate().query("SELECT 'UPDATE ' || A.TABLE_NAME || ' SET ' || C.COLUMN_NAME || ' = ? WHERE ' || A.TABLE_NAME || '.' || C.COLUMN_NAME || ' = ?' RELACIONAMENTO " +
//																	"FROM USER_CONSTRAINTS A, USER_CONSTRAINTS  B, USER_CONS_COLUMNS C " +
//																	"WHERE A.R_CONSTRAINT_NAME = B.CONSTRAINT_NAME " +
//																	"AND A.CONSTRAINT_NAME = C.CONSTRAINT_NAME " +
//																	"AND A.CONSTRAINT_TYPE = 'R' " +
//																	"AND B.CONSTRAINT_TYPE IN ('P', 'U') " +
//																	"AND B.TABLE_NAME = 'CONTAGERENCIAL' " +
//																	"AND A.TABLE_NAME <> 'CONTAGERENCIAL' " +
//																	"GROUP BY A.TABLE_NAME, C.COLUMN_NAME " +
//																	"ORDER BY 1",
		List<String> listaUpdate = getJdbcTemplate().query("SELECT 'UPDATE ' || t.relname || ' SET ' ||  a.attname || ' = ? WHERE ' || a.attname || ' = ?' AS RELACIONAMENTO " +
																"FROM pg_constraint c " +
																"LEFT JOIN pg_class t  ON c.conrelid  = t.oid " +
																"LEFT JOIN pg_class t2 ON c.confrelid = t2.oid " +
																"LEFT JOIN pg_attribute a ON t.oid = a.attrelid AND a.attnum = c.conkey[1] " +
																"LEFT JOIN pg_attribute a2 ON t2.oid = a2.attrelid AND a2.attnum = c.confkey[1] " +
																"WHERE t2.relname = 'contagerencial' " +
																"AND t.relname<>'contagerencial' " +
																"AND c.contype = 'f'", 
															new RowMapper() {
																public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
																	return rs.getString("RELACIONAMENTO");
																}
													
															});
		
		for (String string : listaUpdate) {
			getJdbcTemplate().update(string, new Object[]{bean.getCdcontagerencial(),bean.getContagerencialpai().getCdcontagerencial()});
		}
	}
	
	/**
	 * Verifica se a conta gerencial tem algum registro vinculado no banco de dados.
	 *
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public Boolean haveRegister(Contagerencial bean){
		if(bean == null || bean.getCdcontagerencial() == null){
			throw new SinedException("Conta gerencial n�o pode ser nulo.");
		}
		
//		List<String> listaString = getJdbcTemplate().query("SELECT 'SELECT C1.CDCONTAGERENCIAL FROM CONTAGERENCIAL C1 JOIN ' || A.TABLE_NAME || ' ON ' || A.TABLE_NAME || '.' || C.COLUMN_NAME || ' = C1.CDCONTAGERENCIAL WHERE NOT EXISTS(SELECT C2.CDCONTAGERENCIAL FROM CONTAGERENCIAL C2 WHERE C2.CDCONTAGERENCIALPAI = C1.CDCONTAGERENCIAL) AND C1.CDCONTAGERENCIAL = XX' AS RELACIONAMENTO " +
//																	"FROM USER_CONSTRAINTS A, USER_CONSTRAINTS  B, USER_CONS_COLUMNS C " +
//																	"WHERE A.R_CONSTRAINT_NAME = B.CONSTRAINT_NAME " +
//																	"AND A.CONSTRAINT_NAME = C.CONSTRAINT_NAME " +
//																	"AND A.CONSTRAINT_TYPE = 'R' " +
//																	"AND B.CONSTRAINT_TYPE IN ('P', 'U') " +
//																	"AND B.TABLE_NAME = 'CONTAGERENCIAL' " +
//																	"AND A.TABLE_NAME <> 'CONTAGERENCIAL' " +
//																	"GROUP BY A.TABLE_NAME, C.COLUMN_NAME " +
//																	"ORDER BY 1" , 
		List<String> listaString = getJdbcTemplate().query("SELECT 'SELECT C1.CDCONTAGERENCIAL FROM CONTAGERENCIAL C1 JOIN ' || t.relname || ' ON ' || t.relname || '.' || a.attname || ' = C1.CDCONTAGERENCIAL WHERE NOT EXISTS(SELECT C2.CDCONTAGERENCIAL FROM CONTAGERENCIAL C2 WHERE C2.CDCONTAGERENCIALPAI = C1.CDCONTAGERENCIAL) AND C1.CDCONTAGERENCIAL = XX' AS RELACIONAMENTO " +
																			"FROM pg_constraint c " +
																			"LEFT JOIN pg_class t  ON c.conrelid  = t.oid " +
																			"LEFT JOIN pg_class t2 ON c.confrelid = t2.oid " +
																			"LEFT JOIN pg_attribute a ON t.oid = a.attrelid AND a.attnum = c.conkey[1] " +
																			"LEFT JOIN pg_attribute a2 ON t2.oid = a2.attrelid AND a2.attnum = c.confkey[1] " +
																			"WHERE t2.relname = 'contagerencial' " +
																			"AND t.relname<>'contagerencial' " +
																			"AND c.contype = 'f'" , 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString("RELACIONAMENTO");
			}

		});
		
		String sql = "SELECT COUNT(*) FROM (";
		
		boolean first = false;
		for (String string : listaString) {
			if (first) {
				sql += " UNION ALL ";
			}
			sql += string;
			first = true;
		}
		
		sql += ") QUERY";
		
		sql = sql.replaceAll("XX", bean.getCdcontagerencial().toString());
		
		int count = getJdbcTemplate().queryForInt(sql);
		
		return count != 0;
	}


	
	/**
	 * M�todo para obter lista de Contas gerenciais pelo tipo de opera��o.
	 * 
	 * @param tipooperacao
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Contagerencial> findByTipooperacao(Tipooperacao tipooperacao){
		return
			query()
			.select("contagerencial.cdcontagerencial, contagerencial.nome, " +
					"vcontagerencial.arvorepai, vcontagerencial.identificador, pai.cdcontagerencial")
			.join("contagerencial.vcontagerencial vcontagerencial")
			.leftOuterJoin("contagerencial.contagerencialpai pai")
			.where("contagerencial.tipooperacao = ?",tipooperacao)
			.where("contagerencial.ativo = ?",Boolean.TRUE)
			.orderBy("contagerencial.item, contagerencial.nome")
			.list();
	}
	
	/**
	 * M�todo para obter lista de Contas gerenciais pelo tipo de opera��o e natureza.
	 *
	 * @param tipooperacao
	 * @param whereInCdcontagerencial
	 * @param whereInNatureza
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contagerencial> findByTipooperacaoNatureza(Tipooperacao tipooperacao, String whereInCdcontagerencial, String whereInNatureza, String whereNotInNatureza){
		QueryBuilderSined<Contagerencial> queryBuilder = querySined();
		queryBuilder
					.select("contagerencial.cdcontagerencial, contagerencial.nome, " +
					"vcontagerencial.arvorepai, vcontagerencial.identificador, pai.cdcontagerencial")
					.join("contagerencial.vcontagerencial vcontagerencial")
					.leftOuterJoin("contagerencial.contagerencialpai pai")
					.where("contagerencial.tipooperacao = ?",tipooperacao)
					.where("contagerencial.ativo = ?",Boolean.TRUE)
					.whereIn("contagerencial.cdcontagerencial", whereInCdcontagerencial)
					.whereIn("contagerencial.natureza", whereInNatureza)
					.orderBy("vcontagerencial.identificador, contagerencial.nome");
		
		if (whereNotInNatureza!=null && !whereNotInNatureza.trim().equals("")){
			queryBuilder.where("contagerencial.natureza not in (" + whereNotInNatureza + ")");
		}		
		
		return queryBuilder.list();
	}

	public List<Contagerencial> findForError(String listaContaGerencial) {
		return query()
			.select("contagerencial.cdcontagerencial, contagerencial.nome")
			.whereIn("contagerencial.cdcontagerencial", listaContaGerencial)
			.list();
	}
 
	/*###################################################################*/
	/*########## RELAT�RIO DE AN�LISE DE RECEITAS E DESPESAS ############*/
	/*###################################################################*/
	/**
	 * <p>Fornece os dados de origem para relat�rio de An�lise de Receitas e Despesas.
	 * Pega uma lista de movimenta��es que corresponda aos crit�rios do par�metro filtro.</p>
	 * 
	 * @see #criaQueryAnaliseRC(AnaliseReceitaDespesaReportFiltro)
	 * @param filtro
	 * @return
	 * @author Hugo Ferreira
	 */
	@SuppressWarnings("unchecked")
	public List<AnaliseRecDespBean> buscarParaRelatorioAnaliseReceitaDespesa(AnaliseReceitaDespesaReportFiltro filtro) {
		String query = this.criaQueryAnaliseRC(filtro);
		
		
		if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER) && filtro.getExibirMovimentacaoSemVinculoDocumento() != null && filtro.getExibirMovimentacaoSemVinculoDocumento()) {
			filtro.setCarregarMovimentacoes(Boolean.TRUE);
			
			StringBuilder queryUnion = new StringBuilder();
			String query2 = this.criaQueryAnaliseRC(filtro);
			
			queryUnion
				.append("SELECT query.indentificador AS indentificador, ")
				.append("query.cgnome AS cgnome, ")
				.append("query.operacao AS operacao, ")
				.append("query.cdcontagerencialpai AS cdcontagerencialpai, ")
				.append("query.cdcontagerencial AS cdcontagerencial, ")
				.append("sum(query.valor) AS valor ")
				.append("FROM ( " + query + " ")
				.append("UNION ALL " + query2 + " ) as query ")
				.append("GROUP BY query.indentificador, query.cgnome, query.operacao, query.cdcontagerencialpai, query.cdcontagerencial ")
				.append("ORDER BY query.indentificador");
			
			query = queryUnion.toString();
			filtro.setCarregarMovimentacoes(Boolean.FALSE);
		}
		
		SinedUtil.markAsReader();
		List<AnaliseRecDespBean> lista = getJdbcTemplate().query(query, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new AnaliseRecDespBean(rs.getString("indentificador"), rs.getString("cgnome"),rs.getString("operacao"), rs.getInt("cdcontagerencialpai"), rs.getInt("cdcontagerencial"), rs.getLong("valor"));
			}
		});
		
		return lista;
	}
	
	/**
	 * Monta a string SQL para buscar as contas gerenciais com seus respectivos
	 * somat�rios de valor de movimenta��es conciliadas.
	 * 
	 * @see #montaCondicoes(StringBuilder, AnaliseReceitaDespesaReportFiltro)
	 * @param filtro
	 * @return Uma String SQL
	 * @author Hugo Ferreira
	 */
	private String criaQueryAnaliseRC(AnaliseReceitaDespesaReportFiltro filtro) {
		StringBuilder query = new StringBuilder();

		query.append("SELECT vcontagerencial.identificador AS indentificador,");
		query.append("contagerencial.nome AS cgnome, contagerencial.cdcontagerencialpai, contagerencial.cdcontagerencial, ");
		query.append("UPPER(SUBSTR(tipooperacao.nome, 1, 1)) AS operacao,");
		query.append("SUM(rateioitem.valor) AS valor ");
		
		query.append(criaFrom(filtro));
		
		query.append("GROUP BY vcontagerencial.identificador, contagerencial.nome, contagerencial.cdcontagerencialpai , contagerencial.cdcontagerencial ,UPPER(SUBSTR(tipooperacao.nome, 1, 1)) ");
		
		if(!(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER) && filtro.getExibirMovimentacaoSemVinculoDocumento() != null && filtro.getExibirMovimentacaoSemVinculoDocumento())) {
			query.append("ORDER BY vcontagerencial.identificador");
		}
		
		return query.toString();
	}

	public String criaFrom(AnaliseReceitaDespesaReportFiltro filtro) {
		StringBuilder query = new StringBuilder();
		
		if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.MOVIMENTACOES) 
				|| (filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)
					&& filtro.getExibirMovimentacaoSemVinculoDocumento() != null 
					&& filtro.getExibirMovimentacaoSemVinculoDocumento()
					&& filtro.getCarregarMovimentacoes() != null 
					&& filtro.getCarregarMovimentacoes())){
			query.append(" FROM movimentacao ");
			query.append(" LEFT OUTER JOIN empresa ON movimentacao.cdempresa=empresa.cdpessoa ");
			query.append(" LEFT OUTER JOIN aux_movimentacao ON aux_movimentacao.cdmovimentacao=movimentacao.cdmovimentacao ");
			query.append(" LEFT OUTER JOIN rateio ON movimentacao.cdrateio=rateio.cdrateio ");
			query.append(" LEFT OUTER JOIN conta ON movimentacao.cdconta = conta.cdconta ");
//			query.append(" LEFT OUTER JOIN contaempresa ON conta.cdconta = contaempresa.cdconta ");
//			query.append(" LEFT OUTER JOIN empresa on contaempresa.cdempresa = empresa.cdpessoa ");
		} else if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)){
			query.append(" FROM documento ");
			query.append(" LEFT OUTER JOIN rateio ON documento.cdrateio=rateio.cdrateio ");
			query.append(" LEFT OUTER JOIN conta ON documento.cdvinculoprovisionado=conta.cdconta ");
			query.append(" LEFT OUTER JOIN empresa ON documento.cdempresa=empresa.cdpessoa ");
			query.append(" LEFT OUTER JOIN pessoa ON documento.cdpessoa=pessoa.cdpessoa ");
			query.append(" LEFT OUTER JOIN historicoantecipacao ON documento.cddocumento = historicoantecipacao.cddocumentoreferencia");
//			query.append(" LEFT OUTER JOIN pessoacategoria ON documento.cdpessoa=pessoacategoria.cdpessoa ");
//			query.append(" LEFT OUTER JOIN categoria ON pessoacategoria.cdcategoria=categoria.cdcategoria ");
		}
		
		query.append(" LEFT OUTER JOIN rateioitem ON rateio.cdrateio=rateioitem.cdrateio ");
		query.append(" LEFT OUTER JOIN contagerencial ON rateioitem.cdcontagerencial=contagerencial.cdcontagerencial ");
		query.append(" LEFT OUTER JOIN vcontagerencial ON contagerencial.cdcontagerencial=vcontagerencial.cdcontagerencial ");
		query.append(" LEFT OUTER JOIN tipooperacao ON contagerencial.cdtipooperacao=tipooperacao.cdtipooperacao ");
		query.append(" LEFT OUTER JOIN centrocusto ON rateioitem.cdcentrocusto=centrocusto.cdcentrocusto ");
		query.append(" LEFT OUTER JOIN projeto ON rateioitem.cdprojeto=projeto.cdprojeto ");

		this.montaCondicoes(query, filtro);

		return query.toString();
	}
	
	/**
	 * Adiciona na String SQL as condi��es, de acordo com o filtro.
	 * 
	 * @see #adicionaPeriodo(StringBuilder, Date)
	 * @see #adicionaListas(StringBuilder, AnaliseReceitaDespesaReportFiltro)
	 * @param query
	 * @param filtro
	 * @author Hugo Ferreira
	 */
	private void montaCondicoes(StringBuilder query, AnaliseReceitaDespesaReportFiltro filtro) {
		Boolean temFiltro = false;
		//Condi��es principais (obrigat�rias na query):
		
//		query.append(" WHERE (contagerencial.natureza = " + NaturezaContagerencial.CONTAS_RESULTADO.getValue() + ") ");
		query.append(" WHERE (contagerencial.natureza = " + NaturezaContagerencial.CONTA_GERENCIAL.getValue() + ") ");
		query.append(" and (contagerencial.ativo = true) ");
		
		if(filtro.getOrigem() == null || filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.MOVIMENTACOES)
				|| (filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)
					&& filtro.getExibirMovimentacaoSemVinculoDocumento() != null 
					&& filtro.getExibirMovimentacaoSemVinculoDocumento()
					&& filtro.getCarregarMovimentacoes() != null 
					&& filtro.getCarregarMovimentacoes())) {
			if (filtro.getMovConciliadas() != null && filtro.getMovConciliadas()) {
				temFiltro = true;
				query.append(" AND (movimentacao.cdmovimentacaoacao = ").append(Movimentacaoacao.CONCILIADA.getCdmovimentacaoacao().toString()).append(" ");
			}
			
			if (filtro.getMovNormais() != null && filtro.getMovNormais()) {
				if (temFiltro) {
					query.append(" OR ");
				} else {
					query.append(" AND (");
					temFiltro = true;
				}
				
				query.append("movimentacao.cdmovimentacaoacao = ").append(Movimentacaoacao.NORMAL.getCdmovimentacaoacao().toString()).append(" ");
			}
			
			if (temFiltro) {
				query.append(") ");
			}
			
			if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)
					&& filtro.getExibirMovimentacaoSemVinculoDocumento() != null 
					&& filtro.getExibirMovimentacaoSemVinculoDocumento()
					&& filtro.getCarregarMovimentacoes() != null 
					&& filtro.getCarregarMovimentacoes()) {
				query.append(" AND NOT EXISTS( ");
				query.append(" SELECT movimentacaoorigem.cdmovimentacaoorigem");
				query.append(" FROM movimentacaoorigem");
				query.append(" WHERE movimentacaoorigem.cdmovimentacao = movimentacao.cdmovimentacao");
				query.append(" AND movimentacaoorigem.cddocumento IS NOT NULL");
				query.append(" )");
			}
			
		} else if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)){
			if(filtro.getListaSituacaoDocumentos() != null && filtro.getListaSituacaoDocumentos().size() > 0){
				query.append(" AND documento.cddocumentoacao in (" + CollectionsUtil.listAndConcatenate(filtro.getListaSituacaoDocumentos(), "cddocumentoacao", ",") + ") ");
			}
		}
		//PERMISS�O DE PROJETOS
		if(NeoWeb.getUser() != null){
			Usuario usuario = (Usuario)NeoWeb.getUser();
			if(!usuario.getTodosprojetos()){
				if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.MOVIMENTACOES)
						|| (filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)
							&& filtro.getExibirMovimentacaoSemVinculoDocumento() != null 
							&& filtro.getExibirMovimentacaoSemVinculoDocumento()
							&& filtro.getCarregarMovimentacoes() != null 
							&& filtro.getCarregarMovimentacoes())){
					query.append(" AND movimentacao.cdmovimentacao in (" +
																		"select mSUBQUERY.cdmovimentacao " +
																		"from movimentacao mSUBQUERY " +
																		"join rateioitem riSUBQUERY on riSUBQUERY.cdrateio = mSUBQUERY.cdrateio " +
																		"where riSUBQUERY.cdprojeto in (" + SinedUtil.getListaProjeto() + ")" +
																		") ");
				}else if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)){
					query.append(" AND documento.cddocumento in (" +
																	"select dSUBQUERY.cddocumento " +
																	"from documento dSUBQUERY " +
																	"join rateioitem riSUBQUERY on riSUBQUERY.cdrateio = dSUBQUERY.cdrateio " +
																	"where riSUBQUERY.cdprojeto in (" + SinedUtil.getListaProjeto() + ")" +
																	") ");
				}
			}
		}

		this.adicionaPeriodo(query, filtro);
		this.adicionaReceitasDespesas(query, filtro.getReceitas(), filtro.getDespesas());
		this.adicionaListas(query, filtro);

	}
	
	/**
	 * Adiciona na String SQL se � para buscar somente registros de receitas ou de despesas.
	 * Se for as duas, n�o coloca nada.
	 * 
	 * @param query
	 * @param receitas
	 * @param despesas
	 * @author Hugo Ferreira
	 */
	private void adicionaReceitasDespesas(StringBuilder query, Boolean receitas, Boolean despesas) {
		if (!((receitas && despesas) || (!receitas && !despesas))) {
			Integer cdTipoOperacao = null;
			if (receitas)
				cdTipoOperacao = Tipooperacao.TIPO_CREDITO.getCdtipooperacao();
			else
				cdTipoOperacao = Tipooperacao.TIPO_DEBITO.getCdtipooperacao();
			
			query.append(" AND tipooperacao.cdtipooperacao = ").append(cdTipoOperacao).append(" ");
		}
	}

	/**
	 * Coloca na String SQL as cl�usulas referentes �s listas do filtro
	 * (Centro de custo, Projeto, Empresa, Cliente e Fornecedor).
	 * 
	 * @see #adicionaCentroCusto(StringBuilder, List, boolean)
	 * @see #adicionaProjeto(StringBuilder, List, GenericBean, boolean)
	 * @see #adicionaEmpresa(StringBuilder, List, boolean)
	 * @see #adicionaCliente(StringBuilder, List, boolean)
	 * @see #adicionaFornecedor(StringBuilder, List, boolean)
	 * @see #adicionaColaborador(StringBuilder, List, boolean)
	 * @see #adicionaOutrospagamento(StringBuilder, List, boolean)
	 * @param query
	 * @param filtro
	 * @author Hugo Ferreira
	 * @author Fl�vio Tavares
	 */
	private void adicionaListas(StringBuilder query, AnaliseReceitaDespesaReportFiltro filtro) {
//		if (this.todasListasVazias(filtro)) {
//			//N�o tem q fazer nada aqui se todas as listas forem vazias
//			return;
//		}
		
		List<ItemDetalhe> listaCentrocusto = filtro.getListaCentrocusto();
		List<ItemDetalhe> listaContagerencial = filtro.getListaContagerencial();
		List<ItemDetalhe> listaProjetotipo = filtro.getListaProjetotipo();
		List<ItemDetalhe> listaProjeto = filtro.getListaProjeto();
		List<ItemDetalhe> listaEmpresa = filtro.getListaEmpresa();
		List<ItemDetalhe> listaCliente = filtro.getListaCliente();
		List<ItemDetalhe> listaFornecedor = filtro.getListaFornecedor();
		List<ItemDetalhe> listaColaborador = filtro.getListaColaborador();
		List<ItemDetalhe> listaOutrospagamento = filtro.getListaOutrospagamento();
		List<ItemDetalhe> listaCategoria = filtro.getListaCategoria();
		
		/*
		 * Vari�vel que indica para as fun��es seguintes se elas devem ou n�o concatenar o
		 * conector "OR" na string SQL.
		 */
		boolean adicionaConector = false;
		
		query.append("AND (");
			query.append(" 1 = 1 ");
			adicionaConector = true;
			adicionaConector = this.adicionaCentroCusto(query, listaCentrocusto, adicionaConector);
			adicionaConector = this.buscaEAdicionaContagerencial(query, listaContagerencial, adicionaConector);
			adicionaConector = this.adicionaProjetotipo(query, listaProjetotipo, adicionaConector);
			adicionaConector = this.adicionaProjeto(query, listaProjeto, filtro.getRadProjeto(), adicionaConector);
			adicionaConector = this.adicionaEmpresa(query, listaEmpresa, adicionaConector, filtro);
			adicionaConector = this.adicionaCategoria(query, listaCategoria, adicionaConector);
			adicionaConector = this.adicionaClienteFornecedorColaboradorOutrospagamentos(query, listaCliente, listaFornecedor, listaColaborador, listaOutrospagamento, adicionaConector, filtro);
			adicionaConector = this.adicionaListaContabancariaCaixaCartao(query, filtro, adicionaConector);
//			adicionaConector = this.adicionaContabancaria(query, listaContabancaria, adicionaConector);
//			adicionaConector = this.adicionaContabancaria(query, listaCaixa, adicionaConector);
//			adicionaConector = this.adicionaContabancaria(query, listaCartao, adicionaConector);
//			adicionaConector = this.adicionaCliente(query, listaCliente, adicionaConector);
//			adicionaConector = this.adicionaFornecedor(query, listaFornecedor, adicionaConector);
//			adicionaConector = this.adicionaColaborador(query, listaColaborador, adicionaConector);
//			adicionaConector = this.adicionaOutrospagamento(query, listaOutrospagamento, adicionaConector);
		query.append(")");
	}
	
	private List<ItemDetalhe> listRecebidoPago(List<ItemDetalhe> lista, boolean recebido){
		List<ItemDetalhe> listaNova = new ArrayList<ItemDetalhe>();
		
		for (ItemDetalhe itemDetalhe : lista) {
			if(itemDetalhe.getRecebidode() && recebido){
				listaNova.add(itemDetalhe);
			} else if(!itemDetalhe.getRecebidode() && !recebido){
				listaNova.add(itemDetalhe);
			}
		}
		
		return listaNova;
	}
	
	private boolean adicionaClienteFornecedorColaboradorOutrospagamentos(StringBuilder query, List<ItemDetalhe> listaCliente, List<ItemDetalhe> listaFornecedor, 
																			List<ItemDetalhe> listaColaborador, List<ItemDetalhe> listaOutrospagamento, 
																			boolean adicionaConector, AnaliseReceitaDespesaReportFiltro filtro) {
		boolean usou = false;
		
		List<ItemDetalhe> listaClienteRecebido = listRecebidoPago(listaCliente, true);
		List<ItemDetalhe> listaColaboradorRecebido = listRecebidoPago(listaColaborador, true);
		List<ItemDetalhe> listaFornecedorRecebido = listRecebidoPago(listaFornecedor, true);
		List<ItemDetalhe> listaOutrospagamentoRecebido = listRecebidoPago(listaOutrospagamento, true);
		
		List<ItemDetalhe> listaClientePago = listRecebidoPago(listaCliente, false);
		List<ItemDetalhe> listaColaboradorPago = listRecebidoPago(listaColaborador, false);
		List<ItemDetalhe> listaFornecedorPago = listRecebidoPago(listaFornecedor, false);
		List<ItemDetalhe> listaOutrospagamentoPago = listRecebidoPago(listaOutrospagamento, false);
		
		usou = criaWhereCliente(query, adicionaConector, true, usou, listaClienteRecebido, filtro);
		usou = criaWhereCliente(query, adicionaConector, false, usou, listaClientePago, filtro);
		
		usou = criaWhereColaborador(query, adicionaConector, true, usou, listaColaboradorRecebido, filtro);
		usou = criaWhereColaborador(query, adicionaConector, false, usou, listaColaboradorPago, filtro);
		
		usou = criaWhereFornecedor(query, adicionaConector, true, usou, listaFornecedorRecebido, filtro);
		usou = criaWhereFornecedor(query, adicionaConector, false, usou, listaFornecedorPago, filtro);
		
		usou = criaWhereOutrospagamentos(query, adicionaConector, true, usou, listaOutrospagamentoRecebido, filtro);
		usou = criaWhereOutrospagamentos(query, adicionaConector, false, usou, listaOutrospagamentoPago, filtro);
		
		if(usou) {
			query.append(" ) "); 
			return true;
		}
		else return adicionaConector;
	}
	
	private boolean adicionaListaContabancariaCaixaCartao(StringBuilder query, AnaliseReceitaDespesaReportFiltro filtro,  boolean adicionaConector) {
		List<ItemDetalhe> listaContabancaria = filtro.getListaContabancaria(); 
		List<ItemDetalhe> listaCaixa = filtro.getListaCaixa(); 
		List<ItemDetalhe> listaCartao = filtro.getListaCartao();
		
		if(((listaContabancaria == null || listaContabancaria.isEmpty()) && filtro.getRadContbanc() != null && filtro.getRadContbanc()) && 
				((listaCaixa == null || listaCaixa.isEmpty()) && filtro.getRadCaixa() != null && filtro.getRadCaixa()) &&
				((listaCartao == null || listaCartao.isEmpty()) && filtro.getRadCartao() != null && filtro.getRadCartao())){
			if(filtro.getRadSemVinculo() != null && !filtro.getRadSemVinculo()){
				query.append(" AND conta.cdcontatipo is not null ");
			}
			return adicionaConector;
		}
		
		List<Contatipo> listaDesconsiderarContatipo = new ArrayList<Contatipo>();
		if(filtro.getRadContbanc() == null) listaDesconsiderarContatipo.add(Contatipo.TIPO_CONTA_BANCARIA);
		if(filtro.getRadCaixa() == null) listaDesconsiderarContatipo.add(Contatipo.TIPO_CAIXA);
		if(filtro.getRadCartao() == null) listaDesconsiderarContatipo.add(Contatipo.TIPO_CARTAO_CREDITO);
		
		List<Contatipo> listaConsiderarContatipo = new ArrayList<Contatipo>();
		if(filtro.getRadContbanc() != null && filtro.getRadContbanc()) listaConsiderarContatipo.add(Contatipo.TIPO_CONTA_BANCARIA);
		if(filtro.getRadCaixa() != null && filtro.getRadCaixa()) listaConsiderarContatipo.add(Contatipo.TIPO_CAIXA);
		if(filtro.getRadCartao() != null && filtro.getRadCartao()) listaConsiderarContatipo.add(Contatipo.TIPO_CARTAO_CREDITO);
		
		List<ItemDetalhe> listaAgrupada = new ArrayList<ItemDetalhe>();
		if(listaContabancaria != null && !listaContabancaria.isEmpty())
			listaAgrupada.addAll(listaContabancaria);
		if(listaCaixa != null && !listaCaixa.isEmpty())
			listaAgrupada.addAll(listaCaixa);
		if(listaCartao != null && !listaCartao.isEmpty())
			listaAgrupada.addAll(listaCartao);
		
		return adicionaContabancaria(query, listaAgrupada, listaConsiderarContatipo, listaDesconsiderarContatipo, adicionaConector, filtro.getRadSemVinculo());
	}
	
	private boolean adicionaContabancaria(StringBuilder query, List<ItemDetalhe> listaContabancaria, List<Contatipo> listaConsiderarContatipo, List<Contatipo> listaDesconsiderarContatipo, boolean adicionaConector, Boolean radSemVinculo) {
		if(listaContabancaria != null && !listaContabancaria.isEmpty()){
			if (adicionaConector) {
				query.append(" AND ");
			}
			String cds = CollectionsUtil.listAndConcatenate(listaContabancaria, "conta.cdconta", ",");
			query.append("( conta.cdconta IN (").append(cds).append(") ");

			if(listaConsiderarContatipo != null && !listaConsiderarContatipo.isEmpty()){
				String cdsContatipo = CollectionsUtil.listAndConcatenate(listaConsiderarContatipo, "cdcontatipo", ",");
				query.append(" OR conta.cdcontatipo IN (").append(cdsContatipo).append(") ");
			}
			
			if(radSemVinculo != null){
				if(radSemVinculo){
					query.append(" OR conta.cdconta is null ");
				}else {
					query.append(" OR conta.cdconta is not null ");
				}
			}
			query.append(") ");
			adicionaConector = true;
		}
		if(listaDesconsiderarContatipo != null && !listaDesconsiderarContatipo.isEmpty()){
			if (adicionaConector) {
				query.append(" AND ");
			}
			String cds = CollectionsUtil.listAndConcatenate(listaDesconsiderarContatipo, "cdcontatipo", ",");
			query.append("( conta.cdcontatipo not IN (").append(cds).append(") ");
			
			if(radSemVinculo != null){
				if(radSemVinculo){
					query.append(" OR conta.cdconta is null ");
				}else {
					query.append(" AND conta.cdconta is not null ");
				}
			}
			
			query.append(") ");
			adicionaConector = true;
		}
		return adicionaConector;
	}

	private boolean criaWhereOutrospagamentos(StringBuilder query, boolean adicionaConector, boolean recebido, boolean usou,
												List<ItemDetalhe> listaOutrospagamentoRecebido, AnaliseReceitaDespesaReportFiltro filtro) {
		if(listaOutrospagamentoRecebido != null && listaOutrospagamentoRecebido.size() > 0){
			if (adicionaConector && !usou) query.append(" AND ( ");
			if(usou) query.append(" OR ");
			
			String cdsOutros = CollectionsUtil.listAndConcatenate(listaOutrospagamentoRecebido, "outrospagamento", "','");
			
			if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.MOVIMENTACOES)){
				query
				.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
					.append(" FROM movimentacaoorigem mvorigem ")
					.append(" INNER JOIN documento doc ON mvorigem.cddocumento = doc.cddocumento ")
					.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
					.append(" AND UPPER(retira_acento(doc.outrospagamento)) IN (retira_acento('").append(cdsOutros.toUpperCase()).append("')) ")
					.append(" AND doc.cddocumentoclasse = ").append(recebido ? " 2 " : " 1 ")
					.append(" AND doc.tipopagamento = 3) ")
				.append(" OR ")
				.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
					.append(" FROM movimentacaoorigem mvorigem ")
					.append(" INNER JOIN agendamento ag ON mvorigem.cddocumento = ag.cdagendamento ")
					.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
					.append(" AND UPPER(retira_acento(ag.outrospagamento)) IN (retira_acento('").append(cdsOutros.toUpperCase()).append("')) ")
					.append(" AND ag.cdtipooperacao = ").append(recebido ? " 2 " : " 1 ")
					.append(" AND ag.tipopagamento = 3) ");
				
			} else if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)){
				query
					.append(" (UPPER(documento.outrospagamento) in ('").append(cdsOutros.toUpperCase()).append("') ")
					.append(" AND documento.cddocumentoclasse = ").append(recebido ? " 2 " : " 1 ")
					.append(" AND documento.tipopagamento = 3) ");
			}
			
			
			usou = true;
		}
		return usou;
	}

	private boolean criaWhereFornecedor(StringBuilder query, boolean adicionaConector, boolean recebido, boolean usou,
										List<ItemDetalhe> listaFornecedorRecebido, AnaliseReceitaDespesaReportFiltro filtro) {
		if(listaFornecedorRecebido != null && listaFornecedorRecebido.size() > 0){
			String cdsFornecedor = CollectionsUtil.listAndConcatenate(listaFornecedorRecebido, "fornecedor.cdpessoa", ",");
			
			if (adicionaConector && !usou) query.append(" AND ( ");
			if(usou) query.append(" OR ");
			
			if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.MOVIMENTACOES)){
				query
					.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
						.append(" FROM movimentacaoorigem mvorigem ")
						.append(" INNER JOIN documento doc ON mvorigem.cddocumento = doc.cddocumento ")
						.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
						.append(" AND doc.cdpessoa IN (").append(cdsFornecedor).append(") ")
						.append(" AND doc.cddocumentoclasse = ").append(recebido ? " 2 " : " 1 ")
						.append(" AND doc.tipopagamento = 2) ")
					.append(" OR ")
					.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
						.append(" FROM movimentacaoorigem mvorigem ")
						.append(" INNER JOIN agendamento ag ON mvorigem.cddocumento = ag.cdagendamento ")
						.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
						.append(" AND ag.cdpessoa IN (").append(cdsFornecedor).append(") ")
						.append(" AND ag.cdtipooperacao = ").append(recebido ? " 2 " : " 1 ")
						.append(" AND ag.tipopagamento = 2) ");
				
			} else if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)){
				query
					.append(" (pessoa.cdpessoa in (").append(cdsFornecedor).append(") ")
					.append(" AND documento.cddocumentoclasse = ").append(recebido ? " 2 " : " 1 ")
					.append(" AND documento.tipopagamento = 2) ");
			}
			
			usou = true;
		}
		return usou;
	}

	private boolean criaWhereColaborador(StringBuilder query, boolean adicionaConector, boolean recebido, boolean usou,
											List<ItemDetalhe> listaColaboradorRecebido, AnaliseReceitaDespesaReportFiltro filtro) {
		if(listaColaboradorRecebido != null && listaColaboradorRecebido.size() > 0){
			String cdsColaborador = CollectionsUtil.listAndConcatenate(listaColaboradorRecebido, "colaborador.cdpessoa", ",");
			
			if (adicionaConector && !usou) query.append(" AND ( ");
			if(usou) query.append(" OR ");
			
			if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.MOVIMENTACOES)){
				query
					.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
						.append(" FROM movimentacaoorigem mvorigem ")
						.append(" INNER JOIN documento doc ON mvorigem.cddocumento = doc.cddocumento ")
						.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
						.append(" AND doc.cdpessoa IN (").append(cdsColaborador).append(") ")
						.append(" AND doc.cddocumentoclasse = ").append(recebido ? " 2 " : " 1 ")
						.append(" AND doc.tipopagamento = 1) ")
					.append(" OR ")
					.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
						.append(" FROM movimentacaoorigem mvorigem ")
						.append(" INNER JOIN agendamento ag ON mvorigem.cddocumento = ag.cdagendamento ")
						.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
						.append(" AND ag.cdpessoa IN (").append(cdsColaborador).append(") ")
						.append(" AND ag.cdtipooperacao = ").append(recebido ? " 2 " : " 1 ")
						.append(" AND ag.tipopagamento = 1) ");
				
			} else if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)){
				query
					.append(" (pessoa.cdpessoa in (").append(cdsColaborador).append(") ")
					.append(" AND documento.cddocumentoclasse = ").append(recebido ? " 2 " : " 1 ")
					.append(" AND documento.tipopagamento = 1) ");
			}
			
			usou = true;
		}
		return usou;
	}

	private boolean criaWhereCliente(StringBuilder query, boolean adicionaConector, boolean recebido, boolean usou, 
										List<ItemDetalhe> listaClienteRecebido, AnaliseReceitaDespesaReportFiltro filtro) {
		
		if(listaClienteRecebido != null && listaClienteRecebido.size() > 0){
			String cdsCliente = CollectionsUtil.listAndConcatenate(listaClienteRecebido, "cliente.cdpessoa", ",");
			
			if (adicionaConector && !usou) query.append(" AND ( ");
			if(usou) query.append(" OR ");
			
			if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.MOVIMENTACOES)){
				query
					.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
						.append(" FROM movimentacaoorigem mvorigem ")
						.append(" INNER JOIN documento doc ON mvorigem.cddocumento = doc.cddocumento ")
						.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
						.append(" AND doc.cdpessoa IN (").append(cdsCliente).append(") ")
						.append(" AND doc.cddocumentoclasse = ").append(recebido ? " 2 " : " 1 ")
						.append(" AND doc.tipopagamento = 0) ")
					.append(" OR ")
					.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
						.append(" FROM movimentacaoorigem mvorigem ")
						.append(" INNER JOIN agendamento ag ON mvorigem.cddocumento = ag.cdagendamento ")
						.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
						.append(" AND ag.cdpessoa IN (").append(cdsCliente).append(") ")
						.append(" AND ag.cdtipooperacao = ").append(recebido ? " 2 " : " 1 ")
						.append(" AND ag.tipopagamento = 0) ");
				
			} else if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)){
				query
					.append(" (pessoa.cdpessoa in (").append(cdsCliente).append(") ")
					.append(" AND documento.cddocumentoclasse = ").append(recebido ? " 2 " : " 1 ")
					.append(" AND documento.tipopagamento = 0) ");
			}
			
			usou = true;
		}
		return usou;
	}

//	/**
//	 * Verifica se todas as listas s�o vazias.
//	 * 
//	 * @param filtro
//	 * @return <code>false</code>, se pelo menos uma lista contiver algum valor, ou <code>true</code>, se
//	 * 			todas as listas forem vazias ou nulas.
//	 * @author Hugo Ferreira
//	 * @author Fl�vio Tavares
//	 */
//	private boolean todasListasVazias(AnaliseReceitaDespesaReportFiltro filtro) {
//		List<ItemDetalhe> listaCentrocusto = filtro.getListaCentrocusto();
//		List<ItemDetalhe> listaProjeto = filtro.getListaProjeto();
//		List<ItemDetalhe> listaEmpresa = filtro.getListaEmpresa();
//		List<ItemDetalhe> listaCliente = filtro.getListaCliente();
//		List<ItemDetalhe> listaFornecedor = filtro.getListaFornecedor();
//		List<ItemDetalhe> listaColaborador = filtro.getListaColaborador();
//		List<ItemDetalhe> listaOutrospagamento = filtro.getListaOutrospagamento();
//		
//		if (listaCentrocusto != null && !listaCentrocusto.isEmpty()) return false;
//		if (listaProjeto != null && !listaProjeto.isEmpty()) return false;
//		if (listaEmpresa != null && !listaEmpresa.isEmpty()) return false;
//		if (listaCliente != null && !listaCliente.isEmpty()) return false;
//		if (listaFornecedor != null && !listaFornecedor.isEmpty()) return false;
//		if (listaColaborador != null && !listaColaborador.isEmpty()) return false;
//		if (listaOutrospagamento != null && !listaOutrospagamento.isEmpty()) return false;
//		
//		return true;
//	}
//	
	/**
	 * Coloca a cl�usula referente � data final do per�odo, se estiver presente no filtro.
	 * 
	 * @param query
	 * @param dtInicio 
	 * @param dtFim
	 * @author Hugo Ferreira
	 */
	private void adicionaPeriodo(StringBuilder query, AnaliseReceitaDespesaReportFiltro filtro) {
		if(filtro.getOrigem() == null 
			|| filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.MOVIMENTACOES)
			|| (filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)
				&& filtro.getExibirMovimentacaoSemVinculoDocumento() != null 
				&& filtro.getExibirMovimentacaoSemVinculoDocumento()
				&& filtro.getCarregarMovimentacoes() != null 
				&& filtro.getCarregarMovimentacoes())) {
			String whereData = filtro.getMovNormais() != null && filtro.getMovNormais() ? "coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)" : "movimentacao.dtbanco";
			query.append(" AND " + whereData + " >= '").append(SinedDateUtils.toStringPostgre(filtro.getDtInicio())).append("' ");
			if (filtro.getDtFim() != null) {
				query.append(" AND " + whereData + " <= '").append(SinedDateUtils.toStringPostgre(filtro.getDtFim())).append("' ");
			}
		} else if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)){
			/*
			query.append(" AND documento.dtvencimento >= '").append(SinedDateUtils.toStringPostgre(filtro.getDtInicio())).append("' ");
			if (filtro.getDtFim() != null) {
				query.append(" AND documento.dtvencimento <= '").append(SinedDateUtils.toStringPostgre(filtro.getDtFim())).append("' ");
			}
			*/
			if(filtro.getRadBuscavencimento() != null && filtro.getRadBuscavencimento()){
				query.append(" AND documento.dtvencimento >= '").append(SinedDateUtils.toStringPostgre(filtro.getDtInicio())).append("' ");
				if (filtro.getDtFim() != null) {
					query.append(" AND documento.dtvencimento <= '").append(SinedDateUtils.toStringPostgre(filtro.getDtFim())).append("' ");
				}
			}else if(Boolean.FALSE.equals(filtro.getRadBuscavencimento())){
				query.append(" AND documento.dtemissao >= '").append(SinedDateUtils.toStringPostgre(filtro.getDtInicio())).append("' ");
				if (filtro.getDtFim() != null) {
					query.append(" AND documento.dtemissao <= '").append(SinedDateUtils.toStringPostgre(filtro.getDtFim())).append("' ");
				}
			}else {
				query.append(" AND (" + getSqlDataBaixaAnaliseReceitaDespesaMovimentacao(filtro));
				if(filtro.getExibirMovimentacaoSemVinculo() !=null && filtro.getExibirMovimentacaoSemVinculo()){
					String parametro = parametrogeralService.getValorPorNome(Parametrogeral.DATA_PAGAMENTO_ANTECIPACAO);
					parametro = parametro != null ? br.com.linkcom.utils.StringUtils.tiraAcento(parametro).toUpperCase() : "";
					if ("BAIXA".equalsIgnoreCase(parametro)) {						
						query.append(" OR ( historicoantecipacao.datahora >= '").append(SinedDateUtils.toStringPostgre(filtro.getDtInicio())).append("' ");						
						if (filtro.getDtFim() != null) {
							query.append(" AND historicoantecipacao.datahora <= '").append(SinedDateUtils.toStringPostgre(filtro.getDtFim())).append("' ");
						}
						query.append(" ) "); 
					}				
					if ("ANTECIPACAO".equalsIgnoreCase(parametro)){
						query.append("documento.cddocumento in " +
										" (select historicoantecipacao.cddocumentoreferencia"+
										" from historicoantecipacao "+
										" where 1 = 1"+
										" and historicoantecipacao.cddocumento in "+
										" (select"+
										" documento.cddocumento"+
										" from"+
										" movimentacaoorigem"+
										" join documento on movimentacaoorigem.cddocumento = documento.cddocumento"+
										" join movimentacao on movimentacaoorigem.cdmovimentacao = movimentacao.cdmovimentacao"+
										" where"+
										" movimentacao.cdmovimentacaoacao <> 0"+
										(SinedDateUtils.toStringPostgre(filtro.getDtInicio()) != null ? "and coalesce(m.dtbanco, m.dtmovimentacao) >= '" + SinedDateUtils.toStringPostgre(filtro.getDtInicio()) + "' " : "") +
										(SinedDateUtils.toStringPostgre(filtro.getDtFim()) != null ? "and coalesce(m.dtbanco, m.dtmovimentacao) <= '" + SinedDateUtils.toStringPostgre(filtro.getDtFim()) + "' " : "") +				
										" ))");
					}				
				}
				query.append(" ) "); 
			}
		}
	}
	
	/**
	 * Insere na query, se houver valores, os CDs de Centro de custo
	 * 
	 * @param query
	 * @param listaCentrocusto
	 * @param adicionaConector indica se deve ser colocado o conector "OR" antes da senten�a
	 * @return
	 * 		<code>true</code>, se a cl�usula for adicionada � senten�a, ou o valor da vari�vel <code>adicionaConector</code>
	 * 		para que a pr�xima fun��o de adicionar lista insira corretamente o conector "OR".
	 * @author Hugo Ferreira
	 */
	private boolean adicionaCentroCusto(StringBuilder query, List<ItemDetalhe> listaCentrocusto, boolean adicionaConector) {
		if (listaCentrocusto == null || listaCentrocusto.isEmpty()) return adicionaConector;
		
		if (adicionaConector) query.append(" AND ");
		
		String cds = CollectionsUtil.listAndConcatenate(listaCentrocusto, "centrocusto.cdcentrocusto", ",");
		query.append(" rateioitem.cdcentrocusto IN (").append(cds).append(") ");
		
		return true;
	}
	
	/**
	 * M�todo que busca as contas gerenciais com o identificador e adiciona where na query
	 * 
	 * @param query
	 * @param listaContagerencialDetalhe
	 * @param adicionaConector
	 * @return
	 * 
	 * @author Luiz Fernando
	 */
	private boolean buscaEAdicionaContagerencial(StringBuilder query, List<ItemDetalhe> listaContagerencialDetalhe, boolean adicionaConector) {
		if (listaContagerencialDetalhe == null || listaContagerencialDetalhe.isEmpty()) return adicionaConector;
		
		List<Contagerencial> listaContagerencial = this.findWithIdentificador(CollectionsUtil.listAndConcatenate(listaContagerencialDetalhe, "contagerencial.cdcontagerencial", ","));
		if (listaContagerencial == null || listaContagerencial.isEmpty()) return adicionaConector;
		
		if (adicionaConector) query.append(" AND ");
		
		query.append(" ( ");
		boolean or = false;
		for(Contagerencial contagerencial : listaContagerencial){
			if(contagerencial.getCdcontagerencial() != null && contagerencial.getVcontagerencial() != null &&
					contagerencial.getVcontagerencial().getIdentificador() != null && !"".equals(contagerencial.getVcontagerencial().getIdentificador())){
				if(or) query.append(" or ");
				
				query
					.append("( contagerencial.cdcontagerencial = ").append(contagerencial.getCdcontagerencial()).append(" ")
					.append(" or ")
					.append(" vcontagerencial.identificador like '").append(contagerencial.getVcontagerencial().getIdentificador()).append("' || '%' ) ");
				
				or = true;
			}					
		}
		
		query.append(" ) ");
		return true;
	}
	
	private boolean adicionaProjetotipo(StringBuilder query, List<ItemDetalhe> listaProjetotipo, boolean adicionaConector) {
		if (listaProjetotipo == null || listaProjetotipo.isEmpty()) return adicionaConector;
		
		if (adicionaConector) query.append(" AND ");
		
		String cds = CollectionsUtil.listAndConcatenate(listaProjetotipo, "projetotipo.cdprojetotipo", ",");
		query.append(" projeto.cdprojeto IN (select cdprojeto from projeto where cdprojetotipo in (") .append(cds).append(")) ");
		
		return true;
	}
	
	/**
	 * Insere na query, se houver valores, os CDs de Projeto
	 * 
	 * @param query
	 * @param listaProjeto
	 * @param radProjeto
	 * @param adicionaConector indica se deve ser colocado o conector "OR" antes da senten�a
	 * @return
	 * 		<code>true</code>, se a cl�usula for adicionada � senten�a, ou o valor da vari�vel <code>adicionaConector</code>
	 * 		para que a pr�xima fun��o de adicionar lista insira corretamente o conector "OR".
	 * @author Hugo Ferreira
	 */
	private boolean adicionaProjeto(StringBuilder query, List<ItemDetalhe> listaProjeto, GenericBean radProjeto, boolean adicionaConector) {
		if ((listaProjeto == null || listaProjeto.isEmpty()) && (radProjeto == null || StringUtils.equals(radProjeto.getId().toString(), "todosProjetos"))) return adicionaConector;

		
		if (adicionaConector) query.append(" AND ");
		
		if (StringUtils.equals(radProjeto.getId().toString(), "comProjeto"))
			query.append(" projeto.cdprojeto IS NOT NULL ");
		else if (StringUtils.equals(radProjeto.getId().toString(), "semProjeto"))
			query.append(" projeto.cdprojeto IS NULL ");
		else {
			String cds = CollectionsUtil.listAndConcatenate(listaProjeto, "projeto.cdprojeto", ",");
			query.append(" projeto.cdprojeto IN (").append(cds).append(") ");
		}
		
		return true;
	}
	
	/**
	 * Insere na query, se houver valores, os CDs de Empresa
	 * 
	 * @param query
	 * @param listaEmpresa
	 * @param adicionaConector indica se deve ser colocado o conector "OR" antes da senten�a
	 * @return
	 * 		<code>true</code>, se a cl�usula for adicionada � senten�a, ou o valor da vari�vel <code>adicionaConector</code>
	 * 		para que a pr�xima fun��o de adicionar lista insira corretamente o conector "OR".
	 * @author Hugo Ferreira
	 */
	private boolean adicionaEmpresa(StringBuilder query, List<ItemDetalhe> listaEmpresa, boolean adicionaConector, AnaliseReceitaDespesaReportFiltro filtro) {
		if (listaEmpresa == null || listaEmpresa.isEmpty()) {
			String whereINEmpresas = new SinedUtil().getListaEmpresa();
			if(whereINEmpresas != null && !whereINEmpresas.equals("")){
				if (adicionaConector) {
					query.append(" AND ");
				}
				if(filtro.getOrigem() != null && filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.MOVIMENTACOES)
						|| (filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)
								&& filtro.getExibirMovimentacaoSemVinculoDocumento() != null 
								&& filtro.getExibirMovimentacaoSemVinculoDocumento()
								&& filtro.getCarregarMovimentacoes() != null 
								&& filtro.getCarregarMovimentacoes())){
					query
						.append(" ( movimentacao.cdempresa IN (" + whereINEmpresas + ") ")
						.append(" OR ")
						.append(" (movimentacao.cdempresa is null AND ")
						.append(" (EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
						.append(" FROM movimentacaoorigem mvorigem ")
						.append(" INNER JOIN documento doc ON mvorigem.cddocumento = doc.cddocumento ")
						.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
						.append(" AND (doc.cdempresa IN (" + whereINEmpresas + ") OR doc.cdempresa IS NULL)) ")
						.append(" OR ")
						.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
						.append(" FROM movimentacaoorigem mvorigem ")
						.append(" INNER JOIN agendamento ag ON mvorigem.cdagendamento = ag.cdagendamento ")
						.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
						.append(" AND (ag.cdempresa IN (" + whereINEmpresas + ") OR ag.cdempresa IS NULL)) ")
						.append(" OR ")
						.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
						.append(" FROM movimentacaoorigem mvorigem ")
						.append(" INNER JOIN despesaviagem dp ON (mvorigem.cddespesaviagemadiantamento = dp.cddespesaviagem or mvorigem.cddespesaviagemacerto = dp.cddespesaviagem) ")
						.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
						.append(" AND (dp.cdempresa IN (" + whereINEmpresas + ") OR dp.cdempresa IS NULL)) ")
						.append(" OR ")
						.append(" (EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
						.append(" FROM movimentacaoorigem mvorigem ")
//						.append(" INNER JOIN documento doc ON mvorigem.cddocumento = doc.cddocumento ")
						.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao and mvorigem.cddocumento is null and mvorigem.cdagendamento is null " +
								" and mvorigem.cddespesaviagemadiantamento is null and mvorigem.cddespesaviagemacerto is null) ")
						.append(" AND movimentacao.cdempresa is null AND exists (select 1 from Conta c2 left outer join contaempresa ce2 on ce2.cdconta = c2.cdconta ")
						.append(" where c2.cdconta = conta.cdconta and ")
						.append(" (ce2.cdempresa IN (")
						.append(whereINEmpresas)
						.append(") OR ce2.cdempresa is null))) ")
						.append(" OR ")
						.append("(NOT EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
						.append(" FROM movimentacaoorigem mvorigem ")
						.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao) ")
						.append(" AND movimentacao.cdempresa is null AND exists (select 1 from Conta c2 left outer join contaempresa ce2 on ce2.cdconta = c2.cdconta ")
						.append(" where c2.cdconta = conta.cdconta and ")
						.append(" (ce2.cdempresa IN (")
						.append(whereINEmpresas)
						.append(") OR ce2.cdempresa is null)))))) ");
				} else {
					query
						.append(" (empresa.cdpessoa IN (")
						.append(whereINEmpresas)
						.append(") OR empresa.cdpessoa is null) ");
				}
			}
		} else {
			if (adicionaConector) {
				query.append(" AND ");
			}
			String cds = CollectionsUtil.listAndConcatenate(listaEmpresa, "empresa.cdpessoa", ",");
			if(filtro.getOrigem() != null && filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.MOVIMENTACOES)
					|| (filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)
							&& filtro.getExibirMovimentacaoSemVinculoDocumento() != null 
							&& filtro.getExibirMovimentacaoSemVinculoDocumento()
							&& filtro.getCarregarMovimentacoes() != null 
							&& filtro.getCarregarMovimentacoes())) {
				query
					.append(" ( movimentacao.cdempresa IN (" + cds + ") ")
					.append(" OR ")
					.append("( (EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
					.append(" FROM movimentacaoorigem mvorigem ")
					.append(" INNER JOIN documento doc ON mvorigem.cddocumento = doc.cddocumento ")
					.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
					.append(" AND (doc.cdempresa IN (" + cds + ") OR doc.cdempresa IS NULL)) ")
					.append(" OR ")
					.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
					.append(" FROM movimentacaoorigem mvorigem ")
					.append(" INNER JOIN agendamento ag ON mvorigem.cdagendamento = ag.cdagendamento ")
					.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
					.append(" AND (ag.cdempresa IN (" + cds + ") OR ag.cdempresa IS NULL)) ")
					.append(" OR ")
					.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
					.append(" FROM movimentacaoorigem mvorigem ")
					.append(" INNER JOIN despesaviagem dp ON (mvorigem.cddespesaviagemadiantamento = dp.cddespesaviagem or mvorigem.cddespesaviagemacerto = dp.cddespesaviagem) ")
					.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
					.append(" AND (dp.cdempresa IN (" + cds + ") OR dp.cdempresa IS NULL)) ")
					.append(" OR ")
					.append(" (EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
					.append(" FROM movimentacaoorigem mvorigem ")
//					.append(" INNER JOIN documento doc ON mvorigem.cddocumento = doc.cddocumento ")
					.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao and mvorigem.cddocumento is null and mvorigem.cdagendamento is null " +
							" and mvorigem.cddespesaviagemadiantamento is null and mvorigem.cddespesaviagemacerto is null) ")
					.append(" AND movimentacao.cdempresa is null AND exists (select 1 from Conta c2 left outer join contaempresa ce2 on ce2.cdconta = c2.cdconta ")
					.append(" where c2.cdconta = conta.cdconta and ")
					.append(" (ce2.cdempresa IN (")
					.append(cds)
					.append(") OR ce2.cdempresa is null))) ")
					.append(" OR ")
					.append("(NOT EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
					.append(" FROM movimentacaoorigem mvorigem ")
					.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao) ")
					.append(" AND movimentacao.cdempresa is null AND exists (select 1 from Conta c2 left outer join contaempresa ce2 on ce2.cdconta = c2.cdconta ")
					.append(" where c2.cdconta = conta.cdconta and ")
					.append(" (ce2.cdempresa IN (")
					.append(cds)
					.append(") OR ce2.cdempresa is null)))))) ");
			} else {
				query.append(" empresa.cdpessoa IN (").append(cds).append(") ");
			}
		}
		return true;
	}
	
	/**
	 * Insere na query, se houver valores, os CDs de Empresa
	 * 
	 * @param query
	 * @param listaEmpresa
	 * @param adicionaConector indica se deve ser colocado o conector "OR" antes da senten�a
	 * @return
	 * 		<code>true</code>, se a cl�usula for adicionada � senten�a, ou o valor da vari�vel <code>adicionaConector</code>
	 * 		para que a pr�xima fun��o de adicionar lista insira corretamente o conector "OR".
	 * @author Hugo Ferreira
	 */
	private boolean adicionaCategoria(StringBuilder query, List<ItemDetalhe> listaCategoria, boolean adicionaConector) {
		if (listaCategoria != null && !listaCategoria.isEmpty()) {
			if (adicionaConector) {
				query.append(" AND ");
			}
			String cds = CollectionsUtil.listAndConcatenate(listaCategoria, "categoria.cdcategoria", ",");
//			query.append(" categoria.cdcategoria IN (").append(cds).append(") ");
			query.append(" exists (select pessoacategoria.cdpessoa " +
					"from Pessoacategoria " +
					"where pessoacategoria.cdpessoa = documento.cdpessoa and " +
					"pessoacategoria.cdcategoria IN (").append(cds).append(") )");
		}
		return true;
	}
	
//	/**
//	 * Insere na query, se houver valores, os CDs de Cliente
//	 * 
//	 * @param query
//	 * @param listaCliente
//	 * @param adicionaConector indica se deve ser colocado o conector "OR" antes da senten�a
//	 * @return
//	 * 		<code>true</code>, se a cl�usula for adicionada � senten�a, ou o valor da vari�vel <code>adicionaConector</code>
//	 * 		para que a pr�xima fun��o de adicionar lista insira corretamente o conector "OR".
//	 * @author Hugo Ferreira
//	 */
//	private boolean adicionaCliente(StringBuilder query, List<ItemDetalhe> listaCliente, boolean adicionaConector) {
//		if (listaCliente == null || listaCliente.isEmpty()) return adicionaConector;
//		
//		if (adicionaConector) query.append(" OR ");
//		
//		String cds = CollectionsUtil.listAndConcatenate(listaCliente, "cliente.cdpessoa", ",");
//		query
//			.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
//				.append(" FROM movimentacaoorigem mvorigem ")
//				.append(" INNER JOIN documento doc ON mvorigem.cddocumento = doc.cddocumento ")
//				.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
//				.append(" AND doc.cdpessoa IN (").append(cds).append(")) ")
//			.append(" OR ")
//			.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
//				.append(" FROM movimentacaoorigem mvorigem ")
//				.append(" INNER JOIN agendamento ag ON mvorigem.cddocumento = ag.cdagendamento ")
//				.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
//				.append( "AND ag.cdpessoa IN (").append(cds).append(")) ");
//		
//		return true;
//	}
	
//	/**
//	 * Insere na query, se houver valores, os CDs de Fornecedor
//	 * 
//	 * @param query
//	 * @param listaFornecedor
//	 * @param adicionaConector indica se deve ser colocado o conector "OR" antes da senten�a
//	 * @return
//	 * 		<code>true</code>, se a cl�usula for adicionada � senten�a, ou o valor da vari�vel <code>adicionaConector</code>
//	 * 		para que a pr�xima fun��o de adicionar lista insira corretamente o conector "OR".
//	 * @author Hugo Ferreira
//	 */
//	private boolean adicionaFornecedor(StringBuilder query, List<ItemDetalhe> listaFornecedor, boolean adicionaConector) {
//		if (listaFornecedor == null || listaFornecedor.isEmpty()) return adicionaConector;
//		
//		if (adicionaConector) query.append(" OR ");
//
//		String cds = CollectionsUtil.listAndConcatenate(listaFornecedor, "fornecedor.cdpessoa", ",");
//		query
//			.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
//				.append(" FROM movimentacaoorigem mvorigem ")
//				.append(" INNER JOIN documento doc ON mvorigem.cddocumento = doc.cddocumento ")
//				.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
//				.append(" AND doc.cdpessoa IN (").append(cds).append(")) ")
//			.append(" OR ")
//			.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
//				.append(" FROM movimentacaoorigem mvorigem ")
//				.append(" INNER JOIN agendamento ag ON mvorigem.cddocumento = ag.cdagendamento ")
//				.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
//				.append(" AND ag.cdpessoa IN (").append(cds).append(")) ");
//		
//		return true;
//	}

//	/**
//	 * Insere na query, se houver valores, os CDs de Colaborador
//	 * 
//	 * @param query
//	 * @param listaColaborador
//	 * @param adicionaConector indica se deve ser colocado o conector "OR" antes da senten�a
//	 * @return
//	 * 		<code>true</code>, se a cl�usula for adicionada � senten�a, ou o valor da vari�vel <code>adicionaConector</code>
//	 * 		para que a pr�xima fun��o de adicionar lista insira corretamente o conector "OR".
//	 * @author Hugo Ferreira
//	 * @author Fl�vio Tavares
//	 */
//	private boolean adicionaColaborador(StringBuilder query, List<ItemDetalhe> listaColaborador, boolean adicionaConector) {
//		if (listaColaborador == null || listaColaborador.isEmpty()) return adicionaConector;
//		
//		if (adicionaConector) query.append(" OR ");
//
//		String cds = CollectionsUtil.listAndConcatenate(listaColaborador, "colaborador.cdpessoa", ",");
//		query
//			.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
//				.append(" FROM movimentacaoorigem mvorigem ")
//				.append(" INNER JOIN documento doc ON mvorigem.cddocumento = doc.cddocumento ")
//				.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
//				.append(" AND doc.cdpessoa IN (").append(cds).append(")) ")
//			.append(" OR ")
//			.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
//				.append(" FROM movimentacaoorigem mvorigem ")
//				.append(" INNER JOIN agendamento ag ON mvorigem.cddocumento = ag.cdagendamento ")
//				.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
//				.append(" AND ag.cdpessoa IN (").append(cds).append(")) ");
//		
//		return true;
//	}
	
//	/**
//	 * Insere na query, se houver valores, os campos Outrospagamento
//	 * 
//	 * @param query
//	 * @param listaOutrospagamento
//	 * @param adicionaConector indica se deve ser colocado o conector "OR" antes da senten�a
//	 * @return
//	 * 		<code>true</code>, se a cl�usula for adicionada � senten�a, ou o valor da vari�vel <code>adicionaConector</code>
//	 * 		para que a pr�xima fun��o de adicionar lista insira corretamente o conector "OR".
//	 * @author Hugo Ferreira
//	 * @author Fl�vio Tavares
//	 */
//	private boolean adicionaOutrospagamento(StringBuilder query, List<ItemDetalhe> listaOutrospagamento, boolean adicionaConector) {
//		if (listaOutrospagamento == null || listaOutrospagamento.isEmpty()) return adicionaConector;
//		
//		if (adicionaConector) query.append(" OR ");
//
//		String cds = CollectionsUtil.listAndConcatenate(listaOutrospagamento, "outrospagamento", "','");
//		query
//			.append(" EXISTS (SELECT mvorigem.cdmovimentacaoorigem ")
//				.append(" FROM movimentacaoorigem mvorigem ")
//				.append(" INNER JOIN documento doc ON mvorigem.cddocumento = doc.cddocumento ")
//				.append(" WHERE mvorigem.cdmovimentacao = movimentacao.cdmovimentacao ")
//				.append(" AND UPPER(doc.outrospagamento) IN ('").append(cds.toUpperCase()).append("')) ")
//			;
//		
//		return true;
//	}
	
	/**
	 * Recupera uma lista de Contas Gerenciais cujo identificador esteja relacionado
	 * na string whereIn.
	 * 
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<AnaliseRecDespBean> buscarTudoEm(AnaliseReceitaDespesaReportFiltro filtro) {
		QueryBuilder<AnaliseRecDespBean> query = newQueryBuilder(AnaliseRecDespBean.class);
		
		query
			.select("new br.com.linkcom.sined.modulo.financeiro.controller.report.bean.AnaliseRecDespBean(" +
						"vcontagerencial.identificador, contagerencial.nome, contagerencial.cdcontagerencial, contagerencialpai.cdcontagerencial, UPPER(SUBSTRING(tipooperacao.nome, 1, 1)), COALESCE(contagerencial.ativo, false))")
			.from(Contagerencial.class)
			.setUseTranslator(false)
			.join("contagerencial.vcontagerencial vcontagerencial")
			.join("contagerencial.tipooperacao tipooperacao")
			.leftOuterJoin("contagerencial.contagerencialpai contagerencialpai")
			.where("contagerencial.ativo = true");
		
			if(filtro.getContassemlancamentos() && filtro.getTiporelatorio() != null) {
				query.where("contagerencial.contagerencialpai is not null");
				
			}
			
			if(filtro != null){
				query.openParentheses()
				.where("tipooperacao = ?", Tipooperacao.TIPO_DEBITO, filtro.getDespesas())
				.or()
				.where("tipooperacao = ?", Tipooperacao.TIPO_CREDITO, filtro.getReceitas())
				.closeParentheses();
			}
			query.orderBy("vcontagerencial.identificador");
		
		SinedUtil.markAsReader();
		return query.list();
	}
	/*###################################################################*/
	/*######## FIM RELAT�RIO DE AN�LISE DE RECEITAS E DESPESAS ##########*/
	/*###################################################################*/

	/**
	 * M�todo que busca as contas gerenciais do whereIn
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Contagerencial> findContasGerenciais(String whereIn) {
		return query()
			.select("contagerencial.cdcontagerencial, contagerencial.nome")
			.whereIn("contagerencial.cdcontagerencial", whereIn)
			.list();
	}

	/**
	 * M�todo que retorna a conta gerencial do Veiculo
	 * 
	 * @param veiculo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Contagerencial findContaGerencialVeiculo(Veiculo veiculo) {
		if(veiculo == null || veiculo.getCdveiculo() == null)
			throw new SinedException("Parametros inv�lidos findContaGerencialVeiculo");
		
		return query()
			.select("contagerencial.cdcontagerencial, contagerencial.nome")
			.where("contagerencial.id = (select bp.contagerencial from Veiculo v " +
										"join v.patrimonioitem pi " +
										"join pi.bempatrimonio bp " +
										"where v.id = "+veiculo.getCdveiculo()+")")
			.unique();
	}

	public Contagerencial loadWithIdentificador(Contagerencial contagerencial) {
		return query()
						.select("contagerencial.cdcontagerencial, contagerencial.nome, vcontagerencial.identificador, vcontagerencial.arvorepai," +
								"contagerencial.codigoalternativo")
						.join("contagerencial.vcontagerencial vcontagerencial")
						.where("contagerencial = ?", contagerencial)
						.unique();
	}
	
	private List<Contagerencial> findWithIdentificador(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
						.select("contagerencial.cdcontagerencial, contagerencial.nome, vcontagerencial.identificador, vcontagerencial.arvorepai," +
								"contagerencial.codigoalternativo")
						.join("contagerencial.vcontagerencial vcontagerencial")
						.whereIn("contagerencial.cdcontagerencial", whereIn)
						.list();
	}

	public List<Contagerencial> findAllFilhos(Contagerencial cg) {
		return query()
					.select("contagerencial.cdcontagerencial, contagerencial.nome, vcontagerencial.identificador")
					.join("contagerencial.vcontagerencial vcontagerencial")
					.where("vcontagerencial.identificador like '" + cg.getVcontagerencial().getIdentificador() + "%'")
					.list();
	}

	@SuppressWarnings("unchecked")
	public Contagerencial findPaiNivel1(Contagerencial contagerencial) {
		
		String sql = "SELECT CG.CDCONTAGERENCIAL, CG.NOME " +
					"FROM CONTAGERENCIAL CG " +
					"WHERE CG.CDCONTAGERENCIALPAI IS NULL " +
					"AND CG.ITEM = (	SELECT CAST(replace(SUBSTR(AC.IDENTIFICADOR, 0, LENGTH(CG.FORMATO)+1), '.', '') AS INTEGER) " +
										"FROM VCONTAGERENCIAL AC " +
										"JOIN CONTAGERENCIAL C1 ON C1.CDCONTAGERENCIAL = AC.CDCONTAGERENCIAL " +
										"WHERE AC.CDCONTAGERENCIAL = " + contagerencial.getCdcontagerencial() + ")";

		SinedUtil.markAsReader();
		List<Contagerencial> lista = getJdbcTemplate().query(sql, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Contagerencial(rs.getInt("CDCONTAGERENCIAL"), rs.getString("NOME"));
			}

		});
		
		if(lista != null && lista.size() > 0){
			return lista.get(0);
		} else return null;
	}
	
	@SuppressWarnings("unchecked")
	public Contagerencial findByIdentificador(String identificador) {
		String sql = "SELECT CG.CDCONTAGERENCIAL, CG.NOME " +
					"FROM VCONTAGERENCIAL VCG " +
					"JOIN CONTAGERENCIAL CG ON CG.CDCONTAGERENCIAL = VCG.CDCONTAGERENCIAL " +
					"WHERE CG.ATIVO = TRUE AND VCG.IDENTIFICADOR = '" + identificador + "' ";

		SinedUtil.markAsReader();
		List<Contagerencial> lista = getJdbcTemplate().query(sql, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Contagerencial(rs.getInt("CDCONTAGERENCIAL"), rs.getString("NOME"));
			}
		});
		
		if(lista != null && lista.size() > 0){
			return lista.get(0);
		} else return null;
	}
	
	/**
	 * M�todo que busca contas gerenciais para relat�rio de fluxo de caixa mensal agrupado por conta gerencial
	 * 
	 * @param filtro
	 * @return
	 * @author Marden Silva
	 */
	public List<Contagerencial> findForFluxoCaixaMensal(List<NaturezaContagerencial> listaNaturezaContagerencial){
		QueryBuilder<Contagerencial> query = query();
		
		query
			.select("contagerencial.cdcontagerencial, contagerencial.nome, contagerencial.ativo," +
					"vcontagerencial.nivel, vcontagerencial.identificador, tipooperacao.cdtipooperacao, " +
					"tipooperacao.nome")					
			.join("contagerencial.vcontagerencial vcontagerencial")
			.join("contagerencial.tipooperacao tipooperacao");
			
		if(listaNaturezaContagerencial != null && !listaNaturezaContagerencial.isEmpty()){
			query.whereIn("contagerencial.natureza", NaturezaContagerencial.listAndConcatenate(listaNaturezaContagerencial));
		}
		
		return query.orderBy("vcontagerencial.identificador")
			 .list();
	}
	
	/**
	 * M�todo que busca contas gerenciais para relat�rio de analise de despesas Excel
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Contagerencial> findForReport(ContagerencialFiltro filtro){
		if(filtro==null){
			throw new SinedException("A propriedade n�o pode ser nula");
		}
		
		return 
			query()
				.select("contagerencial.cdcontagerencial, contagerencial.nome, tipooperacao.nome, contagerencial.ativo," +
					"vcontagerencial.nivel, vcontagerencial.identificador, contagerencial.observacao")
					
				.join("contagerencial.vcontagerencial vcontagerencial")
				.leftOuterJoin("contagerencial.tipooperacao tipooperacao")
				.where("contagerencial.ativo = ?",filtro.getMostraativo())
				.where("contagerencial.geramovimentacao = ?",filtro.getMostramovimentacao())
				.whereLikeIgnoreAll("contagerencial.nome", filtro.getNome())
				.where("vcontagerencial.identificador like ?||'%'", filtro.getIdentificador())
				.where("tipooperacao = ?",filtro.getTipooperacao())
				.where("vcontagerencial.nivel = ?",filtro.getNivel())
				.where("contagerencial.natureza = ?", filtro.getNatureza())
				.orderBy("vcontagerencial.identificador")
			.list();
	}

	/**
	 * M�todo que busca contas gerenciais para relat�rio de analise de despesas Excel
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	@SuppressWarnings("unchecked")
	public List<AnaliseRecDespBean> buscarParaRelatorioAnaliseReceitaDespesaExcel(AnaliseReceitaDespesaReportFiltro filtro) {
		String query = this.criaQueryAnaliseRCExcel(filtro);
//		System.out.println("query: " + query);
		
		List<AnaliseRecDespBean> lista = null;
		
		if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER) 
				&& filtro.getExibirMovimentacaoSemVinculoDocumento() != null 
				&& filtro.getExibirMovimentacaoSemVinculoDocumento()) {
			filtro.setCarregarMovimentacoes(Boolean.TRUE);
			String query2 = this.criaQueryAnaliseRCExcel(filtro);
			
			String select = ""; 
			String groupBy = "";
			String orderBy = "";
			
			if(filtro.getTiporelatorio() != null && filtro.getTiporelatorio().equals("sinteticocentrocusto")){
				select = "SELECT query.cdcentrocusto as cdcentrocusto, " +
					"query.centrocustonome AS centrocustonome, " +
					"query.indentificador AS indentificador, " +
					"query.cdcontagerencial AS cdcontagerencial, " +
					"query.cgnome AS cgnome, " +
					"query.operacao AS operacao, " +
					"SUM(query.valor) AS valor ";
				
				groupBy = "GROUP BY query.cdcentrocusto, query.centrocustonome, query.indentificador, query.cdcontagerencial, query.cgnome, query.operacao ";
				orderBy = "ORDER BY indentificador";
			}else {
				select = "SELECT query.indentificador AS indentificador, " +
					"query.cgnome AS cgnome, " +
					"query.operacao AS operacao, " +
					"SUM(query.valor) AS valor, " +
					"query.mes AS mes, " +
					"query.ano AS ano ";
				
				groupBy = "GROUP BY query.indentificador, query.cgnome, query.operacao, query.mes, query.ano ";
				orderBy = "ORDER BY indentificador, ano, mes";
			}
			
			StringBuilder queryUnion = new StringBuilder(select);
			queryUnion.append("FROM ( " + query + " ");
			queryUnion.append("UNION ALL " + query2 + " ) as query ");
			queryUnion.append(groupBy);
			queryUnion.append(orderBy);
			
			query = queryUnion.toString();
			filtro.setCarregarMovimentacoes(Boolean.FALSE);
		}
		
		if(filtro.getTiporelatorio() != null && filtro.getTiporelatorio().equals("sinteticocentrocusto")){
			SinedUtil.markAsReader();
			lista = getJdbcTemplate().query(query, new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new AnaliseRecDespBean(rs.getInt("cdcentrocusto"), rs.getString("centrocustonome"), rs.getInt("cdcontagerencial"), rs.getString("indentificador"), rs.getString("cgnome"), rs.getString("operacao"), rs.getLong("valor"), null, null);
				}
			});
		} else {
			SinedUtil.markAsReader();
			lista = getJdbcTemplate().query(query, new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new AnaliseRecDespBean(rs.getString("indentificador"), rs.getString("cgnome"), rs.getString("operacao"), rs.getLong("valor"), rs.getInt("mes"), rs.getInt("ano"));
				}
			});
		}
		
		return lista;
	}

	/**
	 * M�todo que busca contas gerenciais para relat�rio de analise de despesas Excel
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	private String criaQueryAnaliseRCExcel(AnaliseReceitaDespesaReportFiltro filtro) {
		StringBuilder query = new StringBuilder();
		
		if(filtro.getTiporelatorio() != null && filtro.getTiporelatorio().equals("sinteticocentrocusto")){
			query.append("SELECT centrocusto.cdcentrocusto as cdcentrocusto, centrocusto.nome as centrocustonome, vcontagerencial.identificador AS indentificador, vcontagerencial.cdcontagerencial AS cdcontagerencial, ");
		}else {
			query.append("SELECT vcontagerencial.identificador AS indentificador, ");
		}
		query.append("contagerencial.nome AS cgnome, ");
		query.append("UPPER(SUBSTR(tipooperacao.nome, 1, 1)) AS operacao, ");
		query.append("SUM(rateioitem.valor) AS valor ");
		
		if(filtro.getTiporelatorio() == null || !filtro.getTiporelatorio().equals("sinteticocentrocusto")){
			if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.MOVIMENTACOES) 
					|| (filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)
							&& filtro.getExibirMovimentacaoSemVinculoDocumento() != null 
							&& filtro.getExibirMovimentacaoSemVinculoDocumento()
							&& filtro.getCarregarMovimentacoes() != null 
							&& filtro.getCarregarMovimentacoes())){
				if(Boolean.TRUE.equals(filtro.getMovNormais())){
					query.append(", extract(month from coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)) as mes ");
					query.append(", extract(year from coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)) as ano ");
				}else {
					query.append(", extract(month from movimentacao.dtbanco) as mes, extract(year from movimentacao.dtbanco) as ano ");
				}
			} else if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)){
				if(filtro.getRadBuscavencimento() != null && filtro.getRadBuscavencimento()){
					query.append(", extract(month from documento.dtvencimento) as mes, extract(year from documento.dtvencimento) as ano ");
				}else if(Boolean.FALSE.equals(filtro.getRadBuscavencimento())){
					query.append(", extract(month from documento.dtemissao) as mes, extract(year from documento.dtemissao) as ano ");
				}else {
					query.append(", extract(month from " + getSqlDataBaixaAnaliseReceitaDespesa() + ") as mes, extract(year from " + getSqlDataBaixaAnaliseReceitaDespesa() + ") as ano ");					
				}
			}
		}
		
		query.append(criaFrom(filtro));
		
		if(filtro.getTiporelatorio() != null && filtro.getTiporelatorio().equals("sinteticocentrocusto")){
			query.append("GROUP BY centrocusto.cdcentrocusto, centrocusto.nome, vcontagerencial.identificador, vcontagerencial.cdcontagerencial, contagerencial.nome, operacao ");
		}else {
			query.append("GROUP BY vcontagerencial.identificador, contagerencial.nome, operacao ");
		}
		
		Boolean criarOrderBy = !(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER) && filtro.getExibirMovimentacaoSemVinculoDocumento() != null && filtro.getExibirMovimentacaoSemVinculoDocumento());
		
		if(filtro.getTiporelatorio() == null || !filtro.getTiporelatorio().equals("sinteticocentrocusto")){
			if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.MOVIMENTACOES) 
					|| (filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)
							&& filtro.getExibirMovimentacaoSemVinculoDocumento() != null 
							&& filtro.getExibirMovimentacaoSemVinculoDocumento()
							&& filtro.getCarregarMovimentacoes() != null 
							&& filtro.getCarregarMovimentacoes())){
				if(Boolean.TRUE.equals(filtro.getMovNormais())){
					query.append(", extract(month from coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)) ");
					query.append(", extract(year from coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)) ");
					query.append( criarOrderBy ? "ORDER BY vcontagerencial.identificador, extract(year from coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)), extract(month from coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)) " : "");
				}else {
					query.append(", extract(month from movimentacao.dtbanco), extract(year from movimentacao.dtbanco) ");
					query.append( criarOrderBy ? "ORDER BY vcontagerencial.identificador, extract(year from movimentacao.dtbanco), extract(month from movimentacao.dtbanco)" : "");
				}
			} else if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)){
				if(filtro.getRadBuscavencimento() != null && filtro.getRadBuscavencimento()){
					query.append(", extract(month from documento.dtvencimento), extract(year from documento.dtvencimento) ");
					query.append( criarOrderBy ? "ORDER BY vcontagerencial.identificador, extract(year from documento.dtvencimento), extract(month from documento.dtvencimento)" : "");
				}else if(Boolean.FALSE.equals(filtro.getRadBuscavencimento())){
					query.append(", extract(month from documento.dtemissao), extract(year from documento.dtemissao) ");
					query.append( criarOrderBy ? "ORDER BY vcontagerencial.identificador, extract(year from documento.dtemissao), extract(month from documento.dtemissao)" : "");
				}else {
					query.append(", extract(month from " + getSqlDataBaixaAnaliseReceitaDespesa() + "), extract(year from " + getSqlDataBaixaAnaliseReceitaDespesa() + ") ");
					query.append( criarOrderBy ? "ORDER BY vcontagerencial.identificador, extract(year from " + getSqlDataBaixaAnaliseReceitaDespesa() + "), extract(month from " + getSqlDataBaixaAnaliseReceitaDespesa() + ")" : "");					
				}
			}
		} 
		else {
			query.append( criarOrderBy ? "ORDER BY vcontagerencial.identificador " : "");
		}
		
		return query.toString();
	}
	
	public String criarOrderBy(AnaliseReceitaDespesaReportFiltro filtro) {
		StringBuilder query = new StringBuilder();
		
		if(filtro.getTiporelatorio() == null || !filtro.getTiporelatorio().equals("sinteticocentrocusto")){
			if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.MOVIMENTACOES) 
					|| (filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)
							&& filtro.getExibirMovimentacaoSemVinculoDocumento() != null 
							&& filtro.getExibirMovimentacaoSemVinculoDocumento()
							&& filtro.getCarregarMovimentacoes() != null 
							&& filtro.getCarregarMovimentacoes())){
				if(Boolean.TRUE.equals(filtro.getMovNormais())){
					query.append("ORDER BY vcontagerencial.identificador, extract(year from coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)), extract(month from coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)) ");
				}else {
					query.append("ORDER BY vcontagerencial.identificador, extract(year from movimentacao.dtbanco), extract(month from movimentacao.dtbanco)");
				}
			} else if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)){
				if(filtro.getRadBuscavencimento() != null && filtro.getRadBuscavencimento()){
					query.append("ORDER BY vcontagerencial.identificador, extract(year from documento.dtvencimento), extract(month from documento.dtvencimento)");
				}else if(Boolean.FALSE.equals(filtro.getRadBuscavencimento())){
					query.append("ORDER BY vcontagerencial.identificador, extract(year from documento.dtemissao), extract(month from documento.dtemissao)");
				}else {
					query.append("ORDER BY vcontagerencial.identificador, extract(year from " + getSqlDataBaixaAnaliseReceitaDespesa() + "), extract(month from " + getSqlDataBaixaAnaliseReceitaDespesa() + ")");					
				}
			}
		} else {
			query.append("ORDER BY vcontagerencial.identificador ");
		}
		
		return query.toString();
	}

	/*public List<Contagerencial> findAutocomplete(String q, Tipooperacao tipooperacao) {
		return query()
					.select("contagerencial.cdcontagerencial, contagerencial.nome, vcontagerencial.identificador, vcontagerencial.arvorepai")
					.join("contagerencial.vcontagerencial vcontagerencial")
					
					.openParentheses()
					.whereLikeIgnoreAll("contagerencial.nome", q)
					.or()
					.whereLikeIgnoreAll("vcontagerencial.arvorepai", q)
					.or()
					.where("vcontagerencial.identificador like ?||'%'", q)
					.closeParentheses()
					
					.where("not exists(select C2.cdcontagerencial from Contagerencial C2 where C2.contagerencialpai = contagerencial and C2.ativo = true)")
					.where("contagerencial.ativo = ?", Boolean.TRUE)
					.where("contagerencial.tipooperacao = ?", tipooperacao)
					.orderBy("vcontagerencial.identificador")
					.list();
	}*/
	
	/**
	 * M�todo autocomplete de carrega as contas gerenciais de acordo com a natureza
	 *
	 * @param q
	 * @param tipooperacao
	 * @param whereInNatureza
	 * @param whereInCdcontagerencial
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contagerencial> findAutocomplete(String q, Tipooperacao tipooperacao, String whereInNatureza, String whereInCdcontagerencial) {
		return query()
					.select("contagerencial.cdcontagerencial, contagerencial.nome, vcontagerencial.identificador, vcontagerencial.arvorepai, contagerencial.codigoalternativo")
					.join("contagerencial.vcontagerencial vcontagerencial")
					
					.openParentheses()
					.whereLikeIgnoreAll("contagerencial.nome", q)
					.or()
					.whereLikeIgnoreAll("vcontagerencial.arvorepai", q)
					.or()
					.where("vcontagerencial.identificador like ?||'%'", q)
					.or()
					.whereLikeIgnoreAll("contagerencial.codigoalternativo", q)
					.closeParentheses()
					
					.where("not exists(select C2.cdcontagerencial from Contagerencial C2 where C2.contagerencialpai = contagerencial and C2.ativo = true)")
					.where("contagerencial.ativo = ?", Boolean.TRUE)
					.where("contagerencial.tipooperacao = ?", tipooperacao)
					.whereIn("contagerencial.natureza", whereInNatureza)
					.whereIn("contagerencial.cdcontagerencial", whereInCdcontagerencial)
					.orderBy("vcontagerencial.identificador")
					.list();
	}
	
	public List<Contagerencial> findAutocompleteTodos(String q, Tipooperacao tipooperacao) {
		return query()
					.select("contagerencial.cdcontagerencial, contagerencial.nome, vcontagerencial.identificador, vcontagerencial.arvorepai")
					.join("contagerencial.vcontagerencial vcontagerencial")
					
					.openParentheses()
					.whereLikeIgnoreAll("contagerencial.nome", q)
					.or()
					.whereLikeIgnoreAll("vcontagerencial.arvorepai", q)
					.or()
					.where("vcontagerencial.identificador like ?||'%'", q)
					.closeParentheses()
					
					.where("contagerencial.ativo = ?", Boolean.TRUE)
					.where("contagerencial.tipooperacao = ?", tipooperacao)
					.orderBy("vcontagerencial.identificador")
					.list();
	}

	@SuppressWarnings("unchecked")
	public List<Contagerencial> findInTarefaOrcamento(Orcamento orcamento, Tarefaorcamento tarefaorcamento) {
		
		String query = 	"SELECT CG.CDCONTAGERENCIAL, CG.NOME, VCG.IDENTIFICADOR " +
						"FROM TAREFAORCAMENTO TOR " +
						"JOIN CONTAGERENCIAL CG ON CG.CDCONTAGERENCIAL = TOR.CDCONTAGERENCIAL " +
						"JOIN VCONTAGERENCIAL VCG ON VCG.CDCONTAGERENCIAL = CG.CDCONTAGERENCIAL " +
						"WHERE TOR.CDORCAMENTO = " + orcamento.getCdorcamento() + " " +
						(tarefaorcamento != null && tarefaorcamento.getCdtarefaorcamento() != null ? "AND TOR.CDTAREFAORCAMENTO <> " + tarefaorcamento.getCdtarefaorcamento() : "");
		
//		System.out.println("QUERY: " + query);
		SinedUtil.markAsReader();
		List<Contagerencial> lista = getJdbcTemplate().query(query, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Contagerencial(rs.getInt("cdcontagerencial"), rs.getString("nome"), rs.getString("identificador"));
			}
		});
		
		return lista;
	}

	@SuppressWarnings("unchecked")
	public List<Contagerencial> findInComposicao(Orcamento orcamento, Composicaoorcamento composicaoorcamento) {
		String query = 	"SELECT CG.CDCONTAGERENCIAL, CG.NOME, VCG.IDENTIFICADOR " +
						"FROM COMPOSICAOORCAMENTO CO " +
						"JOIN CONTAGERENCIAL CG ON CG.CDCONTAGERENCIAL = CO.CDCONTAGERENCIAL " +
						"JOIN VCONTAGERENCIAL VCG ON VCG.CDCONTAGERENCIAL = CG.CDCONTAGERENCIAL " +
						"WHERE CO.CDORCAMENTO = " + orcamento.getCdorcamento() + " " +
						(composicaoorcamento != null && composicaoorcamento.getCdcomposicaoorcamento() != null ? "AND CO.CDCOMPOSICAOORCAMENTO <> " + composicaoorcamento.getCdcomposicaoorcamento() : "");
				
//		System.out.println("QUERY: " + query);
		SinedUtil.markAsReader();
		List<Contagerencial> lista = getJdbcTemplate().query(query, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Contagerencial(rs.getInt("cdcontagerencial"), rs.getString("nome"), rs.getString("identificador"));
			}
		});
		
		return lista;
	}

	public Contagerencial findByProjetoOrcamentoMOD(Projeto projeto) {
		if(projeto == null || projeto.getCdprojeto() == null){
			throw new SinedException("Projeto n�o pode ser nulo.");
		}
		
		List<Contagerencial> lista = query()
										.select("contagerencial.cdcontagerencial, contagerencial.nome, " +
												"vcontagerencial.arvorepai, vcontagerencial.identificador")
										.join("contagerencial.vcontagerencial vcontagerencial")
										.where("contagerencial.cdcontagerencial in (" +
													"select c.cdcontagerencial " +
													"from Orcamento o " +
													"join o.contagerencialmod c " +
													"join o.listaPlanejamento p " +
													"join p.projeto pr " +
													"where pr.id = ? " +
												")", projeto.getCdprojeto())
										.list();
		
		if(lista == null || lista.size() == 0){
			return null;
		} else {
			return lista.get(0);
		}
	}
	
	/**
	* M�todo que busca movimenta��es/contas a pagar e receber para o 
	* relat�rio anal�tico de an�lise Receita/Despesas de acordo com a passagem de par�metro
	* 
	* @param filtro
	* @return
	* @since Oct 4, 2011
	* @author Luiz Fernando F Silva
	*/
	@SuppressWarnings("unchecked")
	public List<AnaliseRecDespAnaliticoBean> findForRelatorioAnaliticoAnaliseRecDesp(AnaliseReceitaDespesaReportFiltro filtro){
		if(filtro == null)
			throw new SinedException("Par�metro inv�lido.");
		
		String query = this.criaQueryRelatorioAnaliticoAnaliseRecDesp(filtro);
		
//		System.out.println("QUERY: " + sql);
		List<AnaliseRecDespAnaliticoBean> lista = null;
		
		if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER) 
				&& filtro.getExibirMovimentacaoSemVinculoDocumento() != null 
				&& filtro.getExibirMovimentacaoSemVinculoDocumento()) {
			filtro.setCarregarMovimentacoes(Boolean.TRUE);
			String query2 = this.criaQueryRelatorioAnaliticoAnaliseRecDesp(filtro);
			
			String select = "SELECT query.indentificador as indentificador, " +
				"query.cgnome AS cgnome, " +
				"query.cdcontagerencial AS cdcontagerencial, " +
				"query.numeroduplicata AS numeroduplicata, " +
				"query.dtvencimento AS dtvencimento, " +
				"query.nomepessoa AS nomepessoa, " +
				"query.historico AS historico, " +
				"SUM(query.valorrateado) AS valorrateado, " +
				"SUM(query.valorrateadoCG) AS valorrateadoCG, " +
				"SUM(query.valorduplicata) AS valorduplicata, " +
				"query.operacao AS operacao, " +
				"query.numerodocumento AS numerodocumento, " +
				"query.vinculo AS vinculo ";
			
			String groupBy = "GROUP BY query.indentificador, query.cgnome, query.cdcontagerencial, query.numeroduplicata, query.dtvencimento, " +
					"query.nomepessoa, query.historico, query.operacao, query.numerodocumento, query.vinculo ";
			
			StringBuilder queryUnion = new StringBuilder(select);
			queryUnion.append("FROM ( " + query + " ");
			queryUnion.append("UNION ALL " + query2 + " ) as query ");
			queryUnion.append(groupBy);
			queryUnion.append("ORDER BY indentificador");
			
			query = queryUnion.toString();
			filtro.setCarregarMovimentacoes(Boolean.FALSE);
		}
		
		if(filtro.getOrigem() != null && filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.MOVIMENTACOES)){
			SinedUtil.markAsReader();
			lista = getJdbcTemplate().query(query, new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new AnaliseRecDespAnaliticoBean(rs.getString("indentificador"), rs.getString("cgnome"), rs.getInt("cdcontagerencial"), rs.getString("numeroduplicata"), 
							rs.getDate("dtmovimentacao"), rs.getDate("dtbanco"), rs.getString("nomepessoa"), rs.getString("historico"), rs.getLong("valorrateado"), rs.getLong("valorrateadoCG"),
							rs.getLong("valorduplicata"), rs.getString("operacao"), rs.getString("numerodocumento"), rs.getString("vinculo"));
				}
			});
		}else if(filtro.getOrigem() != null && filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)){
			SinedUtil.markAsReader();
			lista = getJdbcTemplate().query(query, new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new AnaliseRecDespAnaliticoBean(rs.getString("indentificador"), rs.getString("cgnome"), rs.getInt("cdcontagerencial"), rs.getString("numeroduplicata"), 
							rs.getDate("dtvencimento"), rs.getString("nomepessoa"), rs.getString("historico"), rs.getLong("valorrateado"), rs.getLong("valorrateadoCG"),
							rs.getLong("valorduplicata"), rs.getString("operacao"), rs.getString("numerodocumento"), rs.getString("vinculo"));
				}
			});
		}
		
		return lista;
	}
	
	public String criaQueryRelatorioAnaliticoAnaliseRecDesp(AnaliseReceitaDespesaReportFiltro filtro) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("select distinct vcontagerencial.identificador AS indentificador,");
		sql.append("contagerencial.nome AS cgnome,");
		sql.append("contagerencial.cdcontagerencial AS cdcontagerencial,");
		sql.append("UPPER(SUBSTR(tipooperacao.nome, 1, 1)) AS operacao,");
		
		String whereRateioProjetoCentrocusto = null;
		if(filtro.getListaProjeto() != null && !filtro.getListaProjeto().isEmpty()){
			whereRateioProjetoCentrocusto = " and ri.cdprojeto in ( " + CollectionsUtil.listAndConcatenate(filtro.getListaProjeto(), "projeto.cdprojeto", ",") + " ) ";
		}
		if(filtro.getListaCentrocusto() != null && !filtro.getListaCentrocusto().isEmpty()){
			whereRateioProjetoCentrocusto =  (whereRateioProjetoCentrocusto != null ? whereRateioProjetoCentrocusto : "") +
				" and ri.cdcentrocusto in ( " + CollectionsUtil.listAndConcatenate(filtro.getListaCentrocusto(), "centrocusto.cdcentrocusto", ",") + " ) ";
		}
		if(filtro.getListaProjetotipo() != null && !filtro.getListaProjetotipo().isEmpty()){
			whereRateioProjetoCentrocusto =  (whereRateioProjetoCentrocusto != null ? whereRateioProjetoCentrocusto : "") +
				" and ri.cdprojeto in (select cdprojeto from projeto where cdprojetotipo in ( " + CollectionsUtil.listAndConcatenate(filtro.getListaProjetotipo(), "projetotipo.cdprojetotipo", ",") + " )) ";
		}
		
		if(filtro.getOrigem() != null && filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.MOVIMENTACOES)
				|| (filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)
						&& filtro.getExibirMovimentacaoSemVinculoDocumento() != null 
						&& filtro.getExibirMovimentacaoSemVinculoDocumento()
						&& filtro.getCarregarMovimentacoes() != null 
						&& filtro.getCarregarMovimentacoes())){
			sql.append(" (select SUM(ri.valor) from rateioitem ri where ri.cdrateio = movimentacao.cdrateio " + (whereRateioProjetoCentrocusto != null ? whereRateioProjetoCentrocusto : "" )+ " ) AS valorrateado, ");
			sql.append("(select SUM(ri.valor) from rateioitem ri join contagerencial cg on cg.cdcontagerencial = ri.cdcontagerencial where ri.cdrateio = movimentacao.cdrateio " + (whereRateioProjetoCentrocusto != null ? whereRateioProjetoCentrocusto : "" )+ " and cg.cdcontagerencial = contagerencial.cdcontagerencial) AS valorrateadoCG, ");
			sql.append("movimentacao.cdmovimentacao as numeroduplicata, ");
			sql.append("movimentacao.historico as historico, ");
			sql.append("movimentacao.valor as valorduplicata, ");
			sql.append("movimentacao.checknum as numerodocumento, ");
			
			if((filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)
					&& filtro.getExibirMovimentacaoSemVinculoDocumento() != null 
					&& filtro.getExibirMovimentacaoSemVinculoDocumento()
					&& filtro.getCarregarMovimentacoes() != null 
					&& filtro.getCarregarMovimentacoes())) {
				sql.append("coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao) AS dtvencimento, ");
				sql.append("'Movimenta��o' AS nomepessoa, ");
			} else {
				sql.append("movimentacao.dtmovimentacao as dtmovimentacao, ");			
				sql.append("movimentacao.dtbanco as dtbanco, ");
				sql.append("array_to_string(ARRAY( ");
				sql.append("(select distinct coalesce(p.nome, d.outrospagamento) ");
				sql.append("from documento d ");
				sql.append("join movimentacaoorigem mo on mo.cddocumento = d.cddocumento ");
				sql.append("left outer join pessoa p on p.cdpessoa = d.cdpessoa ");
				sql.append("where mo.cdmovimentacao = movimentacao.cdmovimentacao) ");
				sql.append("), '\n') AS nomepessoa, ");
			}
			
			sql.append("conta.nome as vinculo");
			
			sql.append(" FROM movimentacao ");
			sql.append(" LEFT OUTER JOIN aux_movimentacao ON aux_movimentacao.cdmovimentacao=movimentacao.cdmovimentacao ");
			sql.append(" LEFT OUTER JOIN rateio ON movimentacao.cdrateio=rateio.cdrateio ");
			sql.append(" LEFT OUTER JOIN conta ON movimentacao.cdconta = conta.cdconta ");
			sql.append(" LEFT OUTER JOIN movimentacaoorigem ON movimentacaoorigem.cdmovimentacao = movimentacao.cdmovimentacao ");
	//		sql.append(" LEFT OUTER JOIN contaempresa ON conta.cdconta = contaempresa.cdconta ");
	//		sql.append(" LEFT OUTER JOIN empresa on contaempresa.cdempresa = empresa.cdpessoa ");
		}else if(filtro.getOrigem() != null && filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)){
			sql.append(" (select SUM(ri.valor) from rateioitem ri where ri.cdrateio = documento.cdrateio " + (whereRateioProjetoCentrocusto != null ? whereRateioProjetoCentrocusto : "" )+ " ) AS valorrateado, ");
			sql.append("(select SUM(ri.valor) from rateioitem ri join contagerencial cg on cg.cdcontagerencial = ri.cdcontagerencial where ri.cdrateio = documento.cdrateio " + (whereRateioProjetoCentrocusto != null ? whereRateioProjetoCentrocusto : "" )+ " and cg.cdcontagerencial = contagerencial.cdcontagerencial) AS valorrateadoCG, ");
			sql.append("documento.cddocumento as numeroduplicata, " );
			sql.append("documento.descricao as historico, ");
			sql.append("documento.valor as valorduplicata, ");
			sql.append("documento.numero as numerodocumento, ");
			sql.append("documento.dtvencimento as dtvencimento, ");
			sql.append("COALESCE(pessoa.nome, documento.outrospagamento) AS nomepessoa, ");
			sql.append("conta.nome as vinculo");
			
			sql.append(" FROM documento ");
			sql.append(" LEFT OUTER JOIN rateio ON documento.cdrateio=rateio.cdrateio ");
			sql.append(" LEFT OUTER JOIN conta ON documento.cdvinculoprovisionado=conta.cdconta ");
			sql.append(" LEFT OUTER JOIN empresa ON documento.cdempresa=empresa.cdpessoa ");
			sql.append(" LEFT OUTER JOIN pessoa ON documento.cdpessoa=pessoa.cdpessoa ");
			sql.append(" LEFT OUTER JOIN pessoacategoria ON documento.cdpessoa=pessoacategoria.cdpessoa ");
			sql.append(" LEFT OUTER JOIN categoria ON pessoacategoria.cdcategoria=categoria.cdcategoria ");
			sql.append(" LEFT OUTER JOIN historicoantecipacao ON documento.cddocumento = historicoantecipacao.cddocumentoreferencia");
		}
		
		sql.append(" LEFT OUTER JOIN rateioitem ON rateio.cdrateio=rateioitem.cdrateio ");
		sql.append(" LEFT OUTER JOIN contagerencial ON rateioitem.cdcontagerencial=contagerencial.cdcontagerencial ");
		sql.append(" LEFT OUTER JOIN vcontagerencial ON contagerencial.cdcontagerencial=vcontagerencial.cdcontagerencial ");
		sql.append(" LEFT OUTER JOIN tipooperacao ON contagerencial.cdtipooperacao=tipooperacao.cdtipooperacao ");
		sql.append(" LEFT OUTER JOIN centrocusto ON rateioitem.cdcentrocusto=centrocusto.cdcentrocusto ");
		sql.append(" LEFT OUTER JOIN projeto ON rateioitem.cdprojeto=projeto.cdprojeto ");
		
		this.montaCondicoes(sql, filtro);
		
		sql.append("GROUP BY vcontagerencial.identificador, contagerencial.nome, contagerencial.cdcontagerencial, UPPER(SUBSTR(tipooperacao.nome, 1, 1)), ");
		if(filtro.getOrigem() != null && filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.MOVIMENTACOES)
				|| (filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)
						&& filtro.getExibirMovimentacaoSemVinculoDocumento() != null 
						&& filtro.getExibirMovimentacaoSemVinculoDocumento()
						&& filtro.getCarregarMovimentacoes() != null 
						&& filtro.getCarregarMovimentacoes())){
			sql.append("movimentacao.historico, movimentacao.valor, movimentacao.dtmovimentacao, movimentacao.dtbanco, movimentacao.cdmovimentacao, contagerencial.cdcontagerencial, conta.nome ");
		}else if(filtro.getOrigem() != null && filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)){
			sql.append("documento.descricao, documento.valor, pessoa.nome, documento.outrospagamento, documento.dtvencimento, documento.cddocumento, conta.nome ");
		}
		
		if(!(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER) 
				&& filtro.getExibirMovimentacaoSemVinculoDocumento() != null && filtro.getExibirMovimentacaoSemVinculoDocumento())) {
			sql.append("ORDER BY vcontagerencial.identificador");
		}
		
		return sql.toString();
	}
	
	/**
	 * Busca todas a contas gerenciais para a exporta��o de arquivos gerenciais.
	 * @author Giovane Freitas <giovane.freitas@linkcom.com.br>
	 * @return 
	 */
	public Iterator<Object[]> findForArquivoGerencial() {
		return newQueryBuilder(Object[].class)
			.select("contagerencial.cdcontagerencial, contagerencial.nome, vcontagerencial.identificador")
			.from(Contagerencial.class)
			.join("contagerencial.vcontagerencial vcontagerencial")
			.iterate();
	}
	public List<Contagerencial> findAutocomplete(String q) {
		return query()
					.select("contagerencial.cdcontagerencial, contagerencial.nome, vcontagerencial.identificador, vcontagerencial.arvorepai")
					.join("contagerencial.vcontagerencial vcontagerencial")
					
					.openParentheses()
					.whereLikeIgnoreAll("contagerencial.nome", q)
					.or()
					.whereLikeIgnoreAll("vcontagerencial.arvorepai", q)
					.or()
					.where("vcontagerencial.identificador like ?||'%'", q)
					.closeParentheses()
					
					.where("not exists(select C2.cdcontagerencial from Contagerencial C2 where C2.contagerencialpai = contagerencial and C2.ativo = true)")
					.where("contagerencial.ativo = ?", Boolean.TRUE)
					.orderBy("vcontagerencial.identificador")
					.list();
	}

	public List<Contagerencial> loadForSpedPiscofinsReg0500(Set<String> listaIdentificador) {
		QueryBuilder<Contagerencial> query = query()
				.select("contagerencial.cdcontagerencial, contagerencial.nome, contagerencial.tipooperacao, contagerencial.dtaltera, " +
						"contagerencial.natureza, contagerencial.observacao, vcontagerencial.identificador, vcontagerencial.nivel, vcontagerencial.nome")
				.join("contagerencial.vcontagerencial vcontagerencial");
		
		query.openParentheses();
		for (String identificador: listaIdentificador){
			query.where("vcontagerencial.identificador=?", identificador);
			query.or();
		}
		query.closeParentheses();
		
		return query.list();
				
	}

	/**
	 * M�todo que verirfica se a conta possui filhos
	 *
	 * @param pai
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isPai(Contagerencial pai){
		if (pai == null || pai.getCdcontagerencial() == null) {
			throw new SinedException("Conta gerencial n�o pode ser nula.");
		}
		
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.from(Contagerencial.class)
			.where("contagerencialpai is null")
			.where("contagerencial = ?",pai)
			.unique() > 0;
	}
	
	public List<Contagerencial> findForGerarMovimentacao(Tipooperacao tipooperacao){
		return query()
			.select("contagerencial.cdcontagerencial")
			.join("contagerencial.tipooperacao tipooperacao")
			.openParentheses()
//				.openParentheses()
//					.where("contagerencial.natureza <> ?", NaturezaContagerencial.CONTA_GERENCIAL)
//					.where("contagerencial.natureza <> ?", NaturezaContagerencial.OUTRAS)
//				.closeParentheses()
//				.or()
				.where("(contagerencial.natureza=? and tipooperacao=?)", new Object[]{NaturezaContagerencial.CONTA_GERENCIAL, tipooperacao})
				.or()
				.where("(contagerencial.natureza=? and tipooperacao=?)", new Object[]{NaturezaContagerencial.OUTRAS, tipooperacao})
			.closeParentheses()
			.list();
	}
	
	/**
	 * 
	 * @param contagerencial
	 * @author Thiago Clemente
	 * 
	 */
	public boolean verificarItem(Contagerencial contagerencial){
		
		QueryBuilder<Long> queryBuilder = newQueryBuilderWithFrom(Long.class)
					.select("count(*)")
					.leftOuterJoin("contagerencial.contagerencialpai contagerencialpai")
					.where("coalesce(contagerencial.ativo,false)=?", Boolean.TRUE)
					.where("contagerencial.item=?", contagerencial.getItem());
							
		if (contagerencial.getPai()==null){
			queryBuilder.where("contagerencialpai is null");
		}else {
			queryBuilder.where("contagerencialpai=?", contagerencial.getPai());
		}
		
		if (contagerencial.getCdcontagerencial()!=null){
			queryBuilder.where("contagerencial.cdcontagerencial<>?", contagerencial.getCdcontagerencial());
		}
		
		return queryBuilder.unique()==0;
	}
	
	@SuppressWarnings("unchecked")
	public List<Contagerencial> findListaCompleta(){
		StringBuilder query = new StringBuilder();
		query.append("SELECT vcontagerencial.identificador, vcontagerencial.cdcontagerencial, contagerencial.nome, contagerencial.cdcontagerencialpai ")
			.append("FROM vcontagerencial ")
			.append("left join contagerencial contagerencial on contagerencial.cdcontagerencial = vcontagerencial.cdcontagerencial ")
			.append("ORDER BY vcontagerencial.identificador");
		

		SinedUtil.markAsReader();
		List<Contagerencial> lista = getJdbcTemplate().query(query.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Contagerencial cg = new Contagerencial(rs.getInt("cdcontagerencial"), rs.getString("nome"), rs.getString("identificador"));
				Integer cdcontagerencialpai = rs.getInt("cdcontagerencialpai");
				if(cdcontagerencialpai != null && cdcontagerencialpai > 0){
					cg.setContagerencialpai(new Contagerencial(cdcontagerencialpai));
				}
				return cg;
			}
		});
		
		return lista;
	}

	/**
	* M�todo que faz um update nas contas gerenciais utilizadas para custo operacional
	*
	* @param whereInContagerencial
	* @since 26/05/2015
	* @author Luiz Fernando
	*/
	public void updateUsadocalculocustooperacional(String whereInContagerencial) {
		if(whereInContagerencial == null || whereInContagerencial.equals("")){
			getHibernateTemplate().bulkUpdate("update Contagerencial set usadocalculocustooperacionalproducao = null ");
		}else {
			getHibernateTemplate().bulkUpdate("update Contagerencial set usadocalculocustooperacionalproducao = true " +
					" where cdcontagerencial in (" + whereInContagerencial + ")");
			getHibernateTemplate().bulkUpdate("update Contagerencial set usadocalculocustooperacionalproducao = null " +
					" where cdcontagerencial not in (" + whereInContagerencial + ")");
		}
		
	}
	
	/**
	* M�todo que faz um update nas contas gerenciais utilizadas para custo comercial
	*
	* @param whereInContagerencial
	* @since 16/09/2015
	* @author Luiz Fernando
	*/
	public void updateUsadocalculocustocomercial(String whereInContagerencial) {
		if(whereInContagerencial == null || whereInContagerencial.equals("")){
			getHibernateTemplate().bulkUpdate("update Contagerencial set usadocalculocustocomercialproducao = null ");
		}else {
			getHibernateTemplate().bulkUpdate("update Contagerencial set usadocalculocustocomercialproducao = true " +
					" where cdcontagerencial in (" + whereInContagerencial + ")");
			getHibernateTemplate().bulkUpdate("update Contagerencial set usadocalculocustocomercialproducao = null " +
					" where cdcontagerencial not in (" + whereInContagerencial + ")");
		}
		
	}
	
	/**
	* M�todo que busca as contas gerenciais utilizadas no custo operacional
	*
	* @return
	* @since 26/05/2015
	* @author Luiz Fernando
	*/
	public List<Contagerencial> getContagerencialUsadaCustoOperacional(){
		return query()
			.select("contagerencial.cdcontagerencial, contagerencial.nome")
			.where("coalesce(contagerencial.usadocalculocustooperacionalproducao, false) = true")
			.list();
	}
	
	/**
	 * M�todo que busca as contas gerenciais utilizadas no custo comercial
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/08/2015
	 */
	public List<Contagerencial> getContagerencialUsadaCustoComercial(){
		return query()
			.select("contagerencial.cdcontagerencial, contagerencial.nome")
			.where("coalesce(contagerencial.usadocalculocustocomercialproducao, false) = true")
			.list();
	}
	/**
	 * M�todo respons�vel por retorna tabela e campo que existe contagerencial
	 * @param id
	 * @return
	 * @since 04/02/2016
	 * @author C�sar
	 */
	@SuppressWarnings("unchecked")
	public List<GenericBean> ocorrenciaContaGerencial(Integer id){
		
		if(id == null){
			throw new SinedException("Par�metro incorreto para consulta.");
		}
		
		String query = 	"SELECT '�rea' as tabela,'Conta gerencial' as campo FROM AREA AR WHERE AR.CDCONTAGERENCIAL = " + id + 
						" UNION " +
						"SELECT 'Cargo' as tabela,'Conta gerencial' as campo FROM CARGO CAR WHERE CAR.CDCONTAGERENCIAL ="+ id +
						" UNION " +
						"SELECT 'Cliente' as tabela,'Conta gerencial' as campo FROM CLIENTE CLI WHERE CLI.CDCONTAGERENCIAL ="+ id + 
						" UNION " +
						"SELECT 'Colaborador' as tabela,'Conta gerencial' as campo FROM COLABORADOR COL WHERE COL.CDCONTAGERENCIAL ="+ id +
						" UNION " +
						"SELECT 'Composi��o Indereta do Or�amento' as tabela, 'Conta gerencial' as campo FROM COMPOSICAOORCAMENTO COMP WHERE COMP.CDCONTAGERENCIAL ="+ id +
						" UNION " +
						"SELECT 'Tipo de Despesa' as tabela,'Conta gerencial' as campo FROM DESPESAVIAGEMTIPO DES WHERE DES.CDCONTAGERENCIAL ="+ id + 
						" UNION " +
						"SELECT 'Tipo de Documento' as tabela,'Conta gerencial da taxa' as campo FROM DOCUMENTOTIPO DOC WHERE DOC.CDCONTAGERENCIAL ="+ id + 
						" UNION " +
						"SELECT 'Empresa' as tabela,'ICMS' as campo FROM EMPRESA EMP WHERE EMP.CDCONTAGERENCIALICMS =" + id +
						" UNION " +
						"SELECT 'Empresa' as tabela,'Ipi' as campo FROM EMPRESA EMP WHERE EMP.CDCONTAGERENCIALIPI =" + id + 
						" UNION " +
						"SELECT 'Empresa' as tabela,'ISS' as campo FROM EMPRESA EMP WHERE EMP.CDCONTAGERENCIALISS ="+ id + 
						" UNION " +
						"SELECT 'Empresa' as tabela,'PIS' as campo FROM EMPRESA EMP WHERE EMP.CDCONTAGERENCIALPIS ="+ id + 
						" UNION " +
						"SELECT 'Empresa' as tabela,'Cofins' as campo FROM EMPRESA EMP WHERE EMP.CDCONTAGERENCIALCOFINS ="+ id + 
						" UNION " +
						"SELECT 'Empresa' as tabela,'Frete' as campo FROM EMPRESA EMP WHERE EMP.CDCONTAGERENCIALFRETE ="+ id + 
						" UNION " +
						"SELECT 'Empresa' as tabela,'ICMS ST' as campo FROM EMPRESA EMP WHERE EMP.CDCONTAGERENCIALICMSST ="+ id + 
						" UNION " +
						"SELECT 'Empresa' as tabela,'Imposto Importa��o' as campo FROM EMPRESA EMP WHERE EMP.CDCONTAGERENCIALII ="+ id + 
						" UNION " +
						"SELECT 'Empresa' as tabela,'Conta gerencial da venda' as campo FROM EMPRESA EMP WHERE EMP.CDCONTAGERENCIALVENDA ="+ id +
						" UNION " +
						"SELECT 'Empresa' as tabela,'Conta gerencial cr�dito (acerto)' as campo FROM EMPRESA EMP WHERE EMP.CDCONTAGERENCIALCREDITO ="+ id + 
						" UNION " +
						"SELECT 'Empresa' as tabela,'Conta gerencial d�bito (acerto)' as campo FROM EMPRESA EMP WHERE EMP.CDCONTAGERENCIALDEBITO ="+ id + 
						" UNION " +
						"SELECT 'Empresa' as tabela,'Conta gerencial (Cr�dito)' as campo FROM EMPRESA EMPC WHERE EMPC.CDCONTAGERENCIALCREDITOTRANSFERENCIA ="+ id + 
						" UNION " +
						"SELECT 'Empresa' as tabela,'Conta gerencial (D�bito)' as campo FROM EMPRESA EMPD WHERE EMPD.CDCONTAGERENCIALDEBITOTRANSFERENCIA ="+ id + 
						" UNION " +
						"SELECT 'Representa��o' as tabela,'Conta Despesa' as campo FROM EMPRESAREPRESENTACAO EMREP WHERE EMREP.CDCONTAGERENCIAL ="+ id + 
						" UNION " +
						"SELECT 'Faixa de imposto' as tabela,'Conta Gerencial' as campo FROM FAIXAIMPOSTO FAI WHERE FAI.CDCONTAGERENCIAL ="+ id + 
						" UNION " +
						"SELECT 'Fornecedor' as tabela,'Conta gerencial' as campo FROM FORNECEDOR FORN WHERE FORN.CDCONTAGERENCIAL ="+ id + 
						" UNION " +
						"SELECT 'Fornecedor' as tabela,'Conta cont�bil' as campo FROM FORNECEDOR FORC WHERE FORC.CDCONTACONTABIL ="+ id + 
						" UNION " +
						"SELECT 'Fornecedor' as tabela,'Conta gerencial Cr�dito' as campo FROM FORNECEDOR FORCR WHERE FORCR.CDCONTAGERENCIALCREDITO ="+ id + 
						" UNION " +
						"SELECT 'Material' as tabela,'Conta gerencial compra' as campo FROM MATERIAL MAT WHERE MAT.CDCONTAGERENCIAL ="+ id + 
						" UNION " +
						"SELECT 'Material' as tabela,'Conta gerencial venda' as campo FROM MATERIAL MATV WHERE MATV.CDCONTAGERENCIALVENDA ="+ id + 
						" UNION " +
						"SELECT 'Tipo de Taxa' as tabela,'Conta Gerencial de Cr�dito' as campo FROM TIPOTAXA TXC WHERE TXC.CDCONTACREDITO ="+ id + 
						" UNION " +
						"SELECT 'Tipo de Taxa' as tabela,'Conta Gerencial de D�bito' as campo FROM TIPOTAXA TXD WHERE TXD.CDCONTADEBITO ="+ id + ";";			
						
						//Mapeando resultados no Bean Gen�rico
						SinedUtil.markAsReader();
						List<GenericBean> listaResultados = getJdbcTemplate().query(query, new RowMapper() {
							public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
								return new GenericBean(rs.getString("tabela"), rs.getString("campo"));
							}
						});
						return listaResultados;
	}
	
	private String getSqlDataBaixaAnaliseReceitaDespesa(){
		return " (select coalesce(m.dtbanco, m.dtmovimentacao) from movimentacao m join movimentacaoorigem mo on mo.cdmovimentacao=m.cdmovimentacao where m.cdmovimentacaoacao in (1, 2) and mo.cddocumento=documento.cddocumento order by coalesce(m.dtbanco, m.dtmovimentacao) desc limit 1) ";
	}
	
	private String getSqlDataBaixaAnaliseReceitaDespesaMovimentacao(AnaliseReceitaDespesaReportFiltro filtro){
		if(filtro.getDtFim()!=null){
			return " documento.cddocumento in( select mo.cddocumento from movimentacao m join movimentacaoorigem mo on mo.cdmovimentacao = m.cdmovimentacao where m.cdmovimentacaoacao in (1,2) and mo.cddocumento is not null and coalesce(m.dtbanco, m.dtmovimentacao) >= '"+ SinedDateUtils.toStringPostgre(filtro.getDtInicio()) + "' and coalesce(m.dtbanco, m.dtmovimentacao) <= '"+ SinedDateUtils.toStringPostgre(filtro.getDtFim()) +"')";
		}else{
			return " documento.cddocumento in( select mo.cddocumento from movimentacao m join movimentacaoorigem mo on mo.cdmovimentacao = m.cdmovimentacao where m.cdmovimentacaoacao in (1,2) and mo.cddocumento is not null and coalesce(m.dtbanco, m.dtmovimentacao) >= '"+ SinedDateUtils.toStringPostgre(filtro.getDtInicio()) + "')";
		}	
	}

	public List<Contagerencial> buscarUltimoNivelAtivos(Tipooperacao tipoOperacao, String strWhereIn) {
		
		QueryBuilder<Contagerencial> query = query()				
			.select("contagerencial.cdcontagerencial, contagerencial.nome, vcontagerencial.identificador, vcontagerencial.arvorepai")
			.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			.where("contagerencial.ativo = true");
		
		if(StringUtils.isNotBlank(strWhereIn)){
			query
			.whereIn("contagerencial.cdcontagerencial not ", strWhereIn, StringUtils.isNotBlank(strWhereIn));
		}
		
		query
			.where("not exists(select C2.cdcontagerencial from Contagerencial C2 where C2.contagerencialpai = contagerencial and C2.ativo = true)")
			.where("contagerencial.tipooperacao = ?",tipoOperacao)
			.where("contagerencial.natureza = ?",NaturezaContagerencial.CONTA_GERENCIAL)
			.orderBy("vcontagerencial.identificador ");
		
		return query.list();
		
	}

	/**
	 * Busca as informa��es para a apura��o de resultado de projeto
	 *
	 * @param filtro
	 * @return
	 * @since 10/09/2019
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<ApuracaoResultadoProjetoBean> findByApuracaoResultadoProjeto(ApuracaoResultadoProjetoFiltro filtro) {
		SinedUtil.markAsReader();
		return getJdbcTemplate().query("select q.cdcontagerencial, q.cdcontagerencialpai, q.identificador, q.nome, q.tem_filhos, q.nivel, " +
										"case when not tem_filhos then " +
										"coalesce((select sum(ri.valor)/100.0 " +
										"from rateioitem ri " +
										"where ri.cdcontagerencial = q.cdcontagerencial " +
										"and ri.cdprojeto = ? " +
										"and ( " +
											"exists ( " +
												"select 1 " +
												"from documento d " +
												"where d.cdrateio = ri.cdrateio " +
												"and d.cddocumentoacao not in (0,5,107) " +
												"and d.dtemissao >= ? " +
												"and d.dtemissao <= ? " +
											") " +
											"or " +
											"exists ( " +
												"select 1 " +
												"from movimentacao m " +
												"where m.cdrateio = ri.cdrateio " +
												"and m.cdmovimentacaoacao <> 0 " +
												"and coalesce(m.dtbanco, m.dtmovimentacao) >= ? " +
												"and coalesce(m.dtbanco, m.dtmovimentacao) <= ? " +
												"and not exists( " +
													"select 1 " +
													"from movimentacaoorigem mo " +
													"where mo.cdmovimentacao = m.cdmovimentacao " +
													"and	mo.cddocumento is not null " +
												") " +
											") " +
											"or " +
											"exists ( " +
												"select 1 " +
												"from movimentacaoestoque me " +
												"where me.cdrateio = ri.cdrateio " +
												"and me.dtcancelamento is null " +
												"and me.dtmovimentacao >= ? " +
												"and me.dtmovimentacao <= ? " +
											") " +
										") " +
										"), 0) " +
										"else 0 end as valor " +
										"from ( " +
										"select  " +
										"cg.cdcontagerencial, " +
										"cg.cdcontagerencialpai, " +
										"vcg.identificador, " +
										"cg.nome, " +
										"exists(select 1 from contagerencial cg2 where cg2.cdcontagerencialpai = cg.cdcontagerencial) as tem_filhos, " +
										"vcg.nivel " +
										"from contagerencial cg " +
										"join vcontagerencial vcg on vcg.cdcontagerencial = cg.cdcontagerencial " +
										"where cg.ativo = true " +
										//"and cg.natureza = 6 " +
										") q " +
										"order by q.identificador", new Object[]{
				filtro.getProjeto().getCdprojeto(),
				filtro.getDtinicio(),
				filtro.getDtfim(),
				filtro.getDtinicio(),
				filtro.getDtfim(),
				filtro.getDtinicio(),
				filtro.getDtfim()
		}, new RowMapper() {
			@Override
			public ApuracaoResultadoProjetoBean mapRow(ResultSet rs, int rowNum) throws SQLException {
				int nivel = rs.getInt("nivel");
				
				Integer cdcontagerencialpai = null;
				Object cdcontagerencialpai_Obj = rs.getObject("cdcontagerencialpai");
				if(cdcontagerencialpai_Obj != null){
					cdcontagerencialpai = (Integer) cdcontagerencialpai_Obj;
				}
				return new ApuracaoResultadoProjetoBean(
					rs.getInt("cdcontagerencial"), 
					rs.getString("identificador"), 
					rs.getString("nome"), 
					rs.getDouble("valor"), 
					cdcontagerencialpai,
					nivel == 1, 
					nivel == 1, 
					rs.getBoolean("tem_filhos")
				);
			}
		});
	}

	/**
	 * Busca os dados para a listagem do detalhamento.
	 *
	 * @param filtro
	 * @return
	 * @since 10/09/2019
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<ApuracaoResultadoProjetoListagemBean> findByApuracaoResultadoProjetoListagem(ApuracaoResultadoProjetoFiltro filtro) {
		SinedUtil.markAsReader();
		return getJdbcTemplate().query("select id, data, descricao, valor, tipo, iditem,  centrocustonome, pessoamaterial, numerodocumento, quantidade, unidademedidasimbolo " +
										"from( " +
											"select d.cddocumento as id, " +
											"d.dtemissao as data, " +
											"d.descricao as descricao, " +
											"ri.valor/100.0 as valor, " +
											"case when d.cddocumentoclasse = 1 then 0 else 1 end as tipo, " +
											"ri.cdrateioitem as iditem, " +
											"cc.nome as centrocustonome, " +
											"coalesce(p.nome, d.outrospagamento) as pessoamaterial, " +
											"d.numero as numerodocumento, " +
											"0.0 as quantidade, " +
											"'' as unidademedidasimbolo " +
											"from rateioitem ri " +
											"join documento d on d.cdrateio = ri.cdrateio " +
											"left join pessoa p on p.cdpessoa = d.cdpessoa " +
											"join centrocusto cc on cc.cdcentrocusto = ri.cdcentrocusto " +
											"where ri.cdcontagerencial = ? " +
											"and ri.cdprojeto = ? " +
											"and d.cddocumentoacao not in (0,5,107) " +
											"and d.dtemissao >= ? " +
											"and d.dtemissao <= ? " +
										"union all " +
											"select m.cdmovimentacao as id, " +
											"coalesce(m.dtbanco, m.dtmovimentacao) as data, " +
											"m.historico as descricao, " +
											"ri.valor/100.0 as valor, " +
											"2 as tipo, " +
											"ri.cdrateioitem as iditem, " +
											"cc.nome as centrocustonome, " +
											"'' as pessoamaterial, " +
											"'' as numerodocumento, " +
											"0.0 as quantidade, " +
											"'' as unidademedidasimbolo " +
											"from rateioitem ri " +
											"join movimentacao m on m.cdrateio = ri.cdrateio " +
											"join centrocusto cc on cc.cdcentrocusto = ri.cdcentrocusto " +
											"where ri.cdcontagerencial = ? " +
											"and ri.cdprojeto = ? " +
											"and m.cdmovimentacaoacao <> 0 " +
											"and coalesce(m.dtbanco, m.dtmovimentacao) >= ? " +
											"and coalesce(m.dtbanco, m.dtmovimentacao) <= ? " +
											"and not exists( " +
												"select 1 " +
												"from movimentacaoorigem mo " +
												"where mo.cdmovimentacao = m.cdmovimentacao " +
												"and	mo.cddocumento is not null " +
											") " +
										"union all " +
											"select me.cdmovimentacaoestoque as id, " +
											"me.dtmovimentacao as data, " +
											"me.observacao as descricao, " +
											"ri.valor/100.0 as valor, " +
											"3 as tipo, " +
											"ri.cdrateioitem as iditem, " +
											"cc.nome as centrocustonome, " +
											"mat.nome as pessoamaterial, " +
											"'' as numerodocumento, " +
											"me.qtde as quantidade, " +
											"um.simbolo as unidademedidasimbolo " +
											"from rateioitem ri " +
											"join movimentacaoestoque me on me.cdrateio = ri.cdrateio " +
											"join material mat on mat.cdmaterial = me.cdmaterial " +
											"join unidademedida um on um.cdunidademedida = mat.cdunidademedida " +
											"join centrocusto cc on cc.cdcentrocusto = ri.cdcentrocusto " +
											"where ri.cdcontagerencial = ? " +
											"and ri.cdprojeto = ? " +
											"and me.dtcancelamento is null " +
											"and me.dtmovimentacao >= ? " +
											"and me.dtmovimentacao <= ? " +
										") q " +
										"order by q.data, q.tipo, q.id", new Object[]{
			filtro.getContagerencial().getCdcontagerencial(),
			filtro.getProjeto().getCdprojeto(),
			filtro.getDtinicio(),
			filtro.getDtfim(),
			filtro.getContagerencial().getCdcontagerencial(),
			filtro.getProjeto().getCdprojeto(),
			filtro.getDtinicio(),
			filtro.getDtfim(),
			filtro.getContagerencial().getCdcontagerencial(),
			filtro.getProjeto().getCdprojeto(),
			filtro.getDtinicio(),
			filtro.getDtfim(),
		}, new RowMapper() {
			@Override
			public ApuracaoResultadoProjetoListagemBean mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new ApuracaoResultadoProjetoListagemBean(
						rs.getInt("id"), 
						rs.getDate("data"), 
						rs.getString("descricao"), 
						rs.getDouble("valor"), 
						ApuracaoResultadoProjetoListagemTipo.values()[rs.getInt("tipo")],
						rs.getInt("iditem"),
						rs.getString("centrocustonome"), 
						rs.getString("pessoamaterial"), 
						rs.getString("numerodocumento"), 
						rs.getDouble("quantidade"), 
						rs.getString("unidademedidasimbolo")
				);
			}
		});
	}
	
	public List<EmitirBalancoPatrimonialBean> buscarTudoBalancoPatrimonial() {
		QueryBuilder<EmitirBalancoPatrimonialBean> query = newQueryBuilder(EmitirBalancoPatrimonialBean.class);
		
		List<NaturezaContagerencial> listaNaturezaContaGerencial = new ArrayList<NaturezaContagerencial>();
		listaNaturezaContaGerencial.add(NaturezaContagerencial.CONTAS_ATIVO);
		listaNaturezaContaGerencial.add(NaturezaContagerencial.CONTAS_PASSIVO);
		listaNaturezaContaGerencial.add(NaturezaContagerencial.CONTAS_COMPENSACAO);
		listaNaturezaContaGerencial.add(NaturezaContagerencial.PATRIMONIO_LIQUIDO);
		
		query
			.select("new br.com.linkcom.sined.modulo.contabil.controller.report.bean.EmitirBalancoPatrimonialBean(" +
						"contacontabil.cdcontacontabil, contacontabil.nome, vcontacontabil.identificador)")
			.from(Contagerencial.class)
			.setUseTranslator(false)
			.join("contacontabil.vcontacontabil vcontacontabil")
			.where("contacontabil.ativo = true")
			.whereIn("contacontabil.natureza", NaturezaContagerencial.listAndConcatenate(listaNaturezaContaGerencial));
			query.orderBy("vcontacontabil.identificador");
		
		return query.list();
	}
	
	
}