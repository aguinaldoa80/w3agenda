package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Atividadetipomodelo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AtividadetipomodeloDAO extends GenericDAO<Atividadetipomodelo>{
	
	/**
	 * 
	 * @param atividadetipo
	 * @author Thiago Clemente
	 */
	public List<Atividadetipomodelo> findByAtividadetipo(Atividadetipo atividadetipo){
		return query()
				.select("atividadetipomodelo.cdatividadetipomodelo, atividadetipo.cdatividadetipo, reportTemplate.cdreporttemplate, reportTemplate.nome")
				.join("atividadetipomodelo.atividadetipo atividadetipo")
				.join("atividadetipomodelo.reportTemplate reportTemplate")
				.where("atividadetipo=?", atividadetipo)
				.orderBy("reportTemplate.nome")
				.list();
	}	
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Atividadetipomodelo> findForEmitirOS(String whereIn){
		return querySined()
				
				.select("atividadetipomodelo.cdatividadetipomodelo, atividadetipo.cdatividadetipo, atividadetipo.nome," +
						"reportTemplate.cdreporttemplate, reportTemplate.nome")
				.join("atividadetipomodelo.atividadetipo atividadetipo")
				.join("atividadetipomodelo.reportTemplate reportTemplate")
				.join("atividadetipo.listaRequisicao listaRequisicao")
				.whereIn("listaRequisicao.cdrequisicao", whereIn)
				.orderBy("atividadetipo.nome, reportTemplate.nome")
				.list();
	}	
}