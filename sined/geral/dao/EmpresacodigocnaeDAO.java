package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresacodigocnae;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EmpresacodigocnaeDAO extends GenericDAO<Empresacodigocnae> {

	public Boolean existsCnaeByEmpresa(Empresa empresa) {
		return query()
				.where("empresacodigocnae.empresa = ?", empresa)
				.list().size() > 0;
	}
	
	public List<Empresacodigocnae> findByEmpresa(Empresa empresa, String cnae) {
		return query()
				.leftOuterJoinFetch("empresacodigocnae.codigocnae codigocnae")
				.where("empresacodigocnae.empresa = ?", empresa)
				.openParentheses()
					.whereLikeIgnoreAll("codigocnae.cnae", cnae)
					.or()
					.whereLikeIgnoreAll("codigocnae.descricaocnae", cnae)
				.closeParentheses()
				.list();
	}
}
