package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Grupoemail;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.GrupoemailFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("grupoemail.nome")
public class GrupoemailDAO extends GenericDAO<Grupoemail>{

	@Override
	public void updateListagemQuery(QueryBuilder<Grupoemail> query,FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("_filtro n�o pode ser null.");
		}
		GrupoemailFiltro filtro = (GrupoemailFiltro) _filtro;
		
		query
			.select("grupoemail.cdgrupoemail, grupoemail.nome, grupoemail.ativo")
			.whereLikeIgnoreAll("grupoemail.nome", filtro.getNome())
			.where("grupoemail.ativo = ?", filtro.getAtivo())
		;
		
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Grupoemail> query) {
		query
			.select("grupoemail.cdgrupoemail, grupoemail.nome, grupoemail.ativo," +
					"lgc.cdgrupoemailcolaborador, colaborador.cdpessoa, colaborador.email")
			
			.leftOuterJoin("grupoemail.listaGrupoemailColaborador lgc")
			.leftOuterJoin("lgc.colaborador colaborador")
		;
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaGrupoemailColaborador");
	}
}
