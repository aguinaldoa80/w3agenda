package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Agendamentoservico;
import br.com.linkcom.sined.geral.bean.Agendamentoservicohistorico;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AgendamentoservicohistoricoDAO extends GenericDAO<Agendamentoservicohistorico> {

	/**
	 * M�todo que retorna os hist�ricos do agendamento servi�o que o usu�rio logado tem permiss�o de ver
	 * 
	 * @param agendamentoservico
	 * @param listaPapeis
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Agendamentoservicohistorico> findHistoricosDoAgendamentoPermissaoUsuario(Agendamentoservico agendamentoservico, List<Papel> listaPapeis) {
		if(agendamentoservico == null || agendamentoservico.getCdagendamentoservico() == null || listaPapeis == null || listaPapeis.isEmpty())
			throw new SinedException("Par�metros inv�lidos!");
		
		QueryBuilder<Agendamentoservicohistorico> query = query();
		query
			.select("agendamentoservicohistorico.cdagendamentoservicohistorico, agendamentoservicohistorico.observacao, agendamentoservicohistorico.dtaltera, " +
					"agendamentoservicohistorico.cdusuarioaltera, agendamentoservicoacao.cdagendamentoservicoacao, agendamentoservicoacao.descricao")
			.join("agendamentoservicohistorico.agendamentoservico agendamentoservico")
			.join("agendamentoservicohistorico.agendamentoservicoacao agendamentoservicoacao")
			.leftOuterJoin("agendamentoservicohistorico.listaagendamentohistoricopapel listaagendamentohistoricopapel")
			.leftOuterJoin("listaagendamentohistoricopapel.papel papel")
			.where("agendamentoservico = ?", agendamentoservico);
			
		if(!SinedUtil.isUsuarioLogadoAdministrador()){
			query
			.openParentheses()
				.where("listaagendamentohistoricopapel is null").or()
				.whereIn("papel.cdpapel", CollectionsUtil.listAndConcatenate(listaPapeis, "cdpapel", ","))
			.closeParentheses();
		}
		
		return query
			.orderBy("agendamentoservicohistorico.cdagendamentoservicohistorico desc")
			.list();
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaagendamentohistoricopapel");
	}
		
}
