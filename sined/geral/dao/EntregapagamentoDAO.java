package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregapagamento;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EntregapagamentoDAO extends GenericDAO<Entregapagamento> {

	/**
	 * Busca uma lista de entregapagamento de uma entrega.
	 *
	 * @param entrega
	 * @return
	 * @since 07/12/2011
	 * @author Rodrigo Freitas
	 */
	public List<Entregapagamento> findByEntregadocumento(Entregadocumento entregadocumento) {
		if(entregadocumento == null || entregadocumento.getCdentregadocumento() == null){
			throw new SinedException("Entregadocumento n�o pode ser nula.");
		}
		return query()
					.select("entregapagamento.cdentregapagamento, entregapagamento.dtvencimento, " +
							"entregapagamento.valor")
					.join("entregapagamento.entregadocumento entregadocumento")
					.where("entregadocumento = ?", entregadocumento)
					.list();
	}
	
	/**
	 * M�todo que remove os elementos de entregapagamento que n�o mais perten�am a uma entrega.
	 * 
	 * @param cdentrega
	 * @param whereNotIn
	 * @author Rafael Salvio
	 */
	public void deleteAllFromEntregaNotInWhereIn(Integer cdentrega, String whereNotIn){
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM entregapagamento WHERE cdentrega = "+cdentrega);
		if(whereNotIn != null){
			sql.append(" AND cdentregapagamento NOT IN ("+whereNotIn+")");
		}
		
		getJdbcTemplate().execute(sql.toString());
	}

	/**
	* M�todo que calcula o total de pagamentos da entrada fiscal
	*
	* @param whereIn
	* @return
	* @since 22/10/2015
	* @author Luiz Fernando
	*/
	public Money getTotalpagamento(String whereIn) {
		Long total = null;
		if(StringUtils.isNotBlank(whereIn)){
			QueryBuilder<Long> query = newQueryBuilder(Long.class)
				.select("SUM(entregapagamento.valor)")
				.setUseTranslator(false)
				.from(Entregapagamento.class);
			
			SinedUtil.quebraWhereIn("entregapagamento.entregadocumento.cdentregadocumento", whereIn, query);
			
			total = query.unique();
		}
		
		return total != null ? new Money(total, true) : new Money();
	}

}
