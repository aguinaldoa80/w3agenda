package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Propostacaixa;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PropostacaixaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("retira_acento(upper(propostacaixa.nome))")
public class PropostacaixaDAO extends GenericDAO<Propostacaixa> {
	
	protected ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Propostacaixa> query,FiltroListagem _filtro) {
		PropostacaixaFiltro filtro = (PropostacaixaFiltro) _filtro;
		
		query
			.select("distinct propostacaixa.cdpropostacaixa, propostacaixa.nome")
			.leftOuterJoin("propostacaixa.listaProposta proposta")
			.leftOuterJoin("proposta.cliente cliente")
			.whereLikeIgnoreAll("propostacaixa.nome", filtro.getNome())
			.where("proposta.numero = ?", filtro.getNumero())
			.where("proposta.sufixo = ?", filtro.getSufixo())
			.where("cliente = ?", filtro.getCliente())
			.orderBy("propostacaixa.cdpropostacaixa")
			.ignoreJoin("proposta", "cliente");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Propostacaixa> query) {
		query.select("propostacaixa.cdpropostacaixa, propostacaixa.nome, "+
				"listaitem.cdpropostacaixaitem, listaitem.nome, listaitem.obrigatorio, " +
				"arquivo.cdarquivo, arquivo.nome, arquivo.tipoconteudo, arquivo.tamanho, " +
				"listaitem.conteudopadrao, reporttemplate.cdreporttemplate, reporttemplate.nome")
		.leftOuterJoin("propostacaixa.listaPropostacaixatemplate propostacaixatemplate")
		.leftOuterJoin("propostacaixatemplate.reporttemplate reporttemplate")
		.leftOuterJoin("propostacaixa.listaitem listaitem")
		.leftOuterJoin("propostacaixa.arquivo arquivo");
	}
	
	/**
	 * <p>
	 * Recupera uma lista de caixas de proposta com suas respectivas propostas.
	 * Uma caixa pode ter, no m�ximo, 5 propostas.
	 * </p>
	 * 
	 * @param caixasSelecionadas
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Propostacaixa> findforRelatorioCapaCaixa(String caixasSelecionadas) {
		if (StringUtils.isEmpty(caixasSelecionadas)) throw new SinedException("Deve haver pelo menos uma caixa selecionada");

		return query()
				.select("propostacaixa.nome, proposta.numero, proposta.sufixo, proposta.descricao, cliente.nome")
				.join("propostacaixa.listaProposta proposta")
				.join("proposta.cliente cliente")
				.whereIn("propostacaixa.cdpropostacaixa", caixasSelecionadas)
				.orderBy("propostacaixa.nome ASC, proposta.numero ASC, cliente.nome ASC")
				.list();
	}
	/**
	 * Carrega todas as Propostas Caixa.
	 * @return
	 * @author Simon
	 */
	public List<Propostacaixa> findListaPropostasCaixa() {
		return query().select("propostacaixa.nome").list();
	}
	/**
	 * Carrega todas as proposta especificadas pelo par�metro.
	 * @param whereIn
	 * @return
	 * @author Simon
	 */
	public List<Propostacaixa> findListaPropostaCaixa(String whereIn) {
		if (whereIn == null) throw new SinedException("O par�metro n�o pode ser nulo.");
		if (whereIn.equals("")) throw new SinedException("Deve haver pelo menos uma caixa selecionada");
		return query()
			   .select("propostacaixa.cdpropostacaixa, propostacaixa.nome, proposta.cdproposta, proposta.descricao, proposta.numero, proposta.sufixo, cliente.nome")
			   .leftOuterJoin("propostacaixa.listaProposta proposta")
			   .leftOuterJoin("proposta.cliente cliente")
			   .whereIn("propostacaixa.cdpropostacaixa", whereIn)
			   .list();
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaitem");
		save.saveOrUpdateManaged("listaPropostacaixatemplate");
		
		Propostacaixa pc = (Propostacaixa) save.getEntity();
		if (pc.getArquivo() != null)
			arquivoDAO.saveFile(pc, "arquivo");
	}
}
