package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Versao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("versao.numero")
public class VersaoDAO extends GenericDAO<Versao> {
	
	/**
	 * 
	 * @author Glauco Cardoso
	 * @return Retorna a vers�o do banco de dados
	 */
	public Versao getVersao(){
		return querySined()
					
					.where("versao.numero = (select max(v.numero) from Versao v)")
					.setMaxResults(1)
					.unique();
		
		
//		List<Versao> lista = query().list();
//		if(lista!=null && !lista.isEmpty())
//			return lista.get(0);
//		else
//			return null;
	}
	
	/**
	 * M�todo que carrega todas as vers�es do flex
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Versao> findAllForFlex() {
		return query()
			.select("versao.cdversao, versao.numero, versao.descricao, versao.data")
			.orderBy("versao.data desc")
			.list();
	}
	
	/* singleton */
	private static VersaoDAO instance;
	public static VersaoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(VersaoDAO.class);
		}
		return instance;
	}
	
}
