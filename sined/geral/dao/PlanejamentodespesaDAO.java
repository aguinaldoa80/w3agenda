package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Planejamentodespesa;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PlanejamentodespesaDAO extends GenericDAO<Planejamentodespesa>{


	/**
	 * Carrega a lista de despesas de planejamento para o relat�rio de or�amento.
	 *
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Planejamentodespesa> findForOrcamento(Planejamento planejamento) {
		if (planejamento == null || planejamento.getCdplanejamento() == null) {
			throw new SinedException("Planejamento n�o pode ser nulo.");
		}
		return query()
					.select("vcontagerencial.identificador, contagerencial.nome, tipooperacao.nome, " +
							"planejamentodespesa.unitario, planejamentodespesa.qtde, fornecimentotipo.nome")
					.leftOuterJoin("planejamentodespesa.contagerencial contagerencial")
					.leftOuterJoin("contagerencial.tipooperacao tipooperacao")
					.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
					.join("planejamentodespesa.planejamento planejamento")
					.join("planejamentodespesa.fornecimentotipo fornecimentotipo")
					.where("planejamento = ?",planejamento)
					.list();
	}
	

	
}
