package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Tarefaorcamento;
import br.com.linkcom.sined.modulo.projeto.controller.process.bean.RecursosBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.TarefaOrcamentoReportFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TarefaorcamentoDAO extends GenericDAO<Tarefaorcamento>{

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaTarefaorcamentorg");
		save.saveOrUpdateManaged("listaTarefaorcamentorh");
	}
	
	
//	/**
//	 * Carrega a lista das tarefas filhas de uma tarefa.
//	 *
//	 * @param tarefa
//	 * @param planejamento
//	 * @return
//	 * @author Rodrigo Freitas
//	 */
//	public List<Tarefaorcamento> listaWithPai(Tarefaorcamento tarefa, Orcamento orcamento) {
//		QueryBuilder<Tarefaorcamento> query = query()
//					.select("tarefaorcamento.cdtarefaorcamento, tarefaorcamento.predecessoras, tarefaorcamento.id, orcamento.cdorcamento, tarefaorcamento.descricao, " +
//					"tarefaorcamento.qtde, indice.cdindice, indice.descricao, tarefaorcamento.dtinicio, " +
//					"tarefaorcamento.duracao, material.cdmaterial, material.nome, material.valorcusto, cargo.custohora," +
//					"listaTarefaorcamentorg.quantidade, listaTarefaorcamentorg.cdtarefaorcamentorg, cargo.cdcargo, cargo.nome, " +
//					"listaTarefaorcamentorh.quantidade, listaTarefaorcamentorh.cdtarefaorcamentorh, tarefapai.cdtarefaorcamento, tarefaorcamento.custototal")
//					.leftOuterJoin("tarefaorcamento.indice indice")
//					.join("tarefaorcamento.orcamento orcamento")
//					.leftOuterJoin("tarefaorcamento.listaTarefaorcamentorg listaTarefaorcamentorg")
//					.leftOuterJoin("listaTarefaorcamentorg.material material")
//					.leftOuterJoin("tarefaorcamento.listaTarefaorcamentorh listaTarefaorcamentorh")
//					.leftOuterJoin("listaTarefaorcamentorh.cargo cargo")
//					.leftOuterJoin("tarefaorcamento.tarefapai tarefapai")
//					.where("orcamento = ?", orcamento)
//					
//					.orderBy("tarefaorcamento.id, material.nome, cargo.nome");
//		
//		if(tarefa == null){
//			query.where("tarefapai is null");
//		} else {
//			query.where("tarefapai = ?", tarefa);
//		}
//		return query
//					.list();
//	}
	
	/**
	 * Carrega as tarefas de um determinado or�amento.
	 *
	 * @param tarefa
	 * @param planejamento
	 * @return
	 * @author Rodrigo Alvarenga
	 */
	public List<Tarefaorcamento> findTarefasByOrcamento(Orcamento orcamento) {
		QueryBuilder<Tarefaorcamento> query = query()
		.select("tarefaorcamento.cdtarefaorcamento, tarefaorcamento.predecessoras, tarefaorcamento.id, orcamento.cdorcamento, tarefaorcamento.descricao, tarefaorcamento.percentualacrescimo, tarefaorcamento.custoacrescido, " +
				"tarefaorcamento.qtde, indice.cdindice, indice.descricao, tarefaorcamento.dtinicio, " +
				"tarefaorcamento.duracao, material.cdmaterial, material.nome, material.valorcusto, material.valorvenda, cargo.custohora," +
				"listaTarefaorcamentorg.quantidade, listaTarefaorcamentorg.cdtarefaorcamentorg, cargo.cdcargo, cargo.nome, " +
				"listaTarefaorcamentorh.quantidade, listaTarefaorcamentorh.cdtarefaorcamentorh, tarefapai.cdtarefaorcamento, tarefaorcamento.custototal, " +
				"listaTarefaorcamentorg.outro, listaTarefaorcamentorg.tiporecursogeral, " +
				"unidademedidarg.cdunidademedida, unidademedidarg.nome, unidademedidarg.simbolo, " +
				"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo")
		.leftOuterJoin("tarefaorcamento.indice indice")
		.leftOuterJoin("tarefaorcamento.unidademedida unidademedida")
		.join("tarefaorcamento.orcamento orcamento")
		.leftOuterJoin("tarefaorcamento.listaTarefaorcamentorg listaTarefaorcamentorg")
		.leftOuterJoin("listaTarefaorcamentorg.material material")
		.leftOuterJoin("listaTarefaorcamentorg.unidademedida unidademedidarg")
		.leftOuterJoin("tarefaorcamento.listaTarefaorcamentorh listaTarefaorcamentorh")
		.leftOuterJoin("listaTarefaorcamentorh.cargo cargo")
		.leftOuterJoin("tarefaorcamento.tarefapai tarefapai")
		.where("orcamento = ?", orcamento)
		.orderBy("tarefaorcamento.id, material.nome, cargo.nome");

		return query.list();
	}

	/**
	 * Deleta todas as tarefas que n�o est�o no whereIn passado por par�metro.
	 *
	 * @param string
	 * @param planejamento
	 * @author Rodrigo Freitas
	 */
	public void deleteWhereNotIn(String string, Orcamento orcamento) {
		getJdbcTemplate().update("DELETE FROM TAREFAORCAMENTO WHERE TAREFAORCAMENTO.CDORCAMENTO = "+orcamento.getCdorcamento()+" AND TAREFAORCAMENTO.CDTAREFAORCAMENTO NOT IN ("+string+")");	
	}


	/**
	 * Verifica se tem tarefa do or�amento passado por par�metro.
	 *
	 * @param orcamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean haveWithOrcamento(Orcamento orcamento) {
		if(orcamento == null || orcamento.getCdorcamento() == null){
			throw new SinedException("Or�amento n�o pode ser nulo.");
		}
		return newQueryBuilder(Long.class)
					.from(Tarefaorcamento.class)
					.select("count(*)")
					.setUseTranslator(false)
					.where("tarefaorcamento.orcamento = ?", orcamento)
					.unique() > 0;
	}


	/**
	 * Deleta todas as tarefas do or�amento.
	 *
	 * @param orcamento
	 * @author Rodrigo Freitas
	 */
	public void deleteByOrcamento(Orcamento orcamento) {
		if(orcamento == null || orcamento.getCdorcamento() == null){
			throw new SinedException("Or�amento n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("DELETE FROM TAREFAORCAMENTO T WHERE T.CDORCAMENTO = " + orcamento.getCdorcamento());
	}


	/**
	 * Query para a gera��o do relat�rio de tarefa do or�amento.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<Tarefaorcamento> findForReport(TarefaOrcamentoReportFiltro filtro) {
		String sql = 
			"SELECT ID, CDTAREFAORCAMENTO, CDTAREFAPAI, DESCRICAO, SUM(VALORMAT + VALORMO + VALOR) AS VALOR, SUM(VALORACRESCIDO) AS VALORACRESCIDO, SUM(VALORMAT) AS VALORMAT, SUM(VALORMO) AS VALORMO " +

			"FROM (" +

			"(SELECT T.ID, T.CDTAREFAORCAMENTO, T.DESCRICAO, T.CDTAREFAPAI, 0 AS VALOR, 0 AS VALORACRESCIDO, (SELECT SUM(TRG.QUANTIDADE*COALESCE(RC.CUSTOUNITARIO/100, M.VALORCUSTO, 0)) FROM TAREFAORCAMENTORG TRG JOIN TAREFAORCAMENTO T2 ON T2.CDTAREFAORCAMENTO = TRG.CDTAREFAORCAMENTO LEFT OUTER JOIN MATERIAL M ON M.CDMATERIAL = TRG.CDMATERIAL LEFT OUTER JOIN RECURSOCOMPOSICAO RC ON (RC.CDMATERIAL = M.CDMATERIAL OR RC.NOME = TRG.OUTRO) WHERE T2.CDTAREFAORCAMENTO = T.CDTAREFAORCAMENTO AND RC.CDORCAMENTO = T.CDORCAMENTO) AS VALORMAT, 0 AS VALORMO " +
			"FROM TAREFAORCAMENTO T " +
			"WHERE T.CDORCAMENTO = " +  filtro.getOrcamento().getCdorcamento() + " " +
			(filtro.getIncluirrg() == null || filtro.getIncluirrg() ? "" : "AND 1 = 0 ") +
			"GROUP BY T.CDTAREFAORCAMENTO, T.DESCRICAO, T.ID, T.CDTAREFAPAI,T.CDORCAMENTO) " +

			"UNION ALL " +

			"SELECT T.ID, T.CDTAREFAORCAMENTO, T.DESCRICAO, T.CDTAREFAPAI, 0 AS VALOR, 0 AS VALORMAT, 0 AS VALORACRESCIDO, (SELECT SUM(TRH.QUANTIDADE*(COALESCE((RH.CUSTOHORA*COALESCE(FM.TOTAL, 1)),C.CUSTOHORA,0)/100)) FROM TAREFAORCAMENTORH TRH JOIN TAREFAORCAMENTO T2 ON T2.CDTAREFAORCAMENTO = TRH.CDTAREFAORCAMENTO JOIN CARGO C ON C.CDCARGO = TRH.CDCARGO LEFT OUTER JOIN ORCAMENTORECURSOHUMANO RH ON RH.CDCARGO = TRH.CDCARGO LEFT OUTER JOIN FATORMDO FM ON FM.CDFATORMDO = RH.CDFATORMDO WHERE T2.CDTAREFAORCAMENTO = T.CDTAREFAORCAMENTO AND RH.CDORCAMENTO = T.CDORCAMENTO) AS VALORMO " +
			"FROM TAREFAORCAMENTO T " +
			"WHERE T.CDORCAMENTO = " + filtro.getOrcamento().getCdorcamento() + " " +
			(filtro.getIncluirrg() == null || filtro.getIncluirrg() ? "" : "AND 1 = 0 ") +
			"GROUP BY T.CDTAREFAORCAMENTO, T.DESCRICAO, T.ID, T.CDTAREFAPAI, T.CDORCAMENTO " +

			"UNION ALL " +

			"(SELECT T.ID, T.CDTAREFAORCAMENTO, T.DESCRICAO, T.CDTAREFAPAI, T.CUSTOTOTAL AS VALOR, 0 AS VALORACRESCIDO, 0 AS VALORMAT, 0 AS VALORMO " +
			"FROM TAREFAORCAMENTO T " +
			"WHERE T.CDORCAMENTO = " + filtro.getOrcamento().getCdorcamento() + " " +
			"AND (SELECT COUNT(*) FROM TAREFAORCAMENTORG TRG WHERE TRG.CDTAREFAORCAMENTO = T.CDTAREFAORCAMENTO) = 0 " +
			"AND (SELECT COUNT(*) FROM TAREFAORCAMENTORH TRH WHERE TRH.CDTAREFAORCAMENTO = T.CDTAREFAORCAMENTO) = 0)" +

			"UNION ALL " +

			"(SELECT T.ID, T.CDTAREFAORCAMENTO, T.DESCRICAO, T.CDTAREFAPAI, 0 AS VALOR, T.CUSTOACRESCIDO AS VALORACRESCIDO, 0 AS VALORMAT, 0 AS VALORMO " +
			"FROM TAREFAORCAMENTO T " +
			"WHERE T.CDORCAMENTO = " + filtro.getOrcamento().getCdorcamento() + ")" +

			") QUERY " +
			"GROUP BY CDTAREFAORCAMENTO, DESCRICAO, CDTAREFAPAI, ID " +

			"ORDER BY ID";
			

		SinedUtil.markAsReader();
		List<Tarefaorcamento> listaUpdate = getJdbcTemplate().query(sql, 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Tarefaorcamento to = new Tarefaorcamento();
				
				to.setCdtarefaorcamento(rs.getInt("CDTAREFAORCAMENTO"));
				to.setDescricao(rs.getString("DESCRICAO"));
				to.setTarefapai(new Tarefaorcamento(rs.getInt("CDTAREFAPAI")));
				to.setPrecocusto(new Money(rs.getDouble("VALOR"), false));
				to.setPrecoacrescido(new Money(rs.getDouble("VALORACRESCIDO"), false));
				to.setPrecomat(new Money(rs.getDouble("VALORMAT"), false));
				to.setPrecomo(new Money(rs.getDouble("VALORMO"), false));
				
				return to;
			}
		});
		
		return listaUpdate;
	}
	
	@SuppressWarnings("unchecked")
	public List<RecursosBean> findForReportRecursos(Tarefaorcamento to) {
		String sql = 
			"SELECT NOME, UM, VALORUNITARIO, QTDE, VALORTOTAL " +
			"FROM ( " +
			
			"SELECT DISTINCT TRG.CDTAREFAORCAMENTORG, COALESCE(M.NOME, TRG.OUTRO) AS NOME, UMOUTRO.SIMBOLO AS UM, COALESCE(RC.CUSTOUNITARIO/100, M.VALORCUSTO, 0) AS VALORUNITARIO, TRG.QUANTIDADE AS QTDE, TRG.QUANTIDADE*(COALESCE(RC.CUSTOUNITARIO, M.VALORCUSTO, 0)/100) AS VALORTOTAL " +
			"FROM TAREFAORCAMENTORG TRG  " +
			"JOIN TAREFAORCAMENTO T ON T.CDTAREFAORCAMENTO = TRG.CDTAREFAORCAMENTO " +
			"LEFT OUTER JOIN UNIDADEMEDIDA UMOUTRO ON UMOUTRO.CDUNIDADEMEDIDA = TRG.CDUNIDADEMEDIDA " +
			"LEFT OUTER JOIN MATERIAL M ON M.CDMATERIAL = TRG.CDMATERIAL  " +
			"LEFT OUTER JOIN RECURSOCOMPOSICAO RC ON (RC.CDMATERIAL = M.CDMATERIAL OR RC.NOME = TRG.OUTRO)  " +
			"WHERE TRG.CDTAREFAORCAMENTO = " + to.getCdtarefaorcamento() + " " +
			"AND RC.CDORCAMENTO = T.CDORCAMENTO " +
			
			"UNION ALL " +
			
			"SELECT DISTINCT TRH.CDTAREFAORCAMENTORH, C.NOME AS NOME, 'H/H' AS UM, (COALESCE((RH.CUSTOHORA * COALESCE(FM.TOTAL, 1)), C.CUSTOHORA, 0) / 100) AS VALORUNITARIO, TRH.QUANTIDADE AS QTDE, TRH.QUANTIDADE *(COALESCE((RH.CUSTOHORA * COALESCE(FM.TOTAL, 1)), C.CUSTOHORA, 0) / 100) AS VALORTOTAL " +
			"FROM TAREFAORCAMENTORH TRH " +
			"JOIN TAREFAORCAMENTO T ON T.CDTAREFAORCAMENTO = TRH.CDTAREFAORCAMENTO " +
			"JOIN CARGO C ON C.CDCARGO = TRH.CDCARGO " +
			"LEFT OUTER JOIN ORCAMENTORECURSOHUMANO RH ON RH.CDCARGO = TRH.CDCARGO " +
			"LEFT OUTER JOIN FATORMDO FM ON FM.CDFATORMDO = RH.CDFATORMDO " +
			"WHERE TRH.CDTAREFAORCAMENTO = " + to.getCdtarefaorcamento() + " " +
			"AND T.CDORCAMENTO = RH.CDORCAMENTO " +
			
			") QUERY " +
			"ORDER BY NOME";
		

		SinedUtil.markAsReader();
		List<RecursosBean> listaUpdate = getJdbcTemplate().query(sql, 
				new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				RecursosBean bean = new RecursosBean();
				
				bean.setNome(rs.getString("NOME"));
				bean.setUnidademedida(rs.getString("UM"));
				bean.setQtde(rs.getDouble("QTDE"));
				bean.setValorunitario(rs.getDouble("VALORUNITARIO"));
				bean.setValortotal(rs.getDouble("VALORTOTAL"));
				
				return bean;
			}
		});
		
		return listaUpdate;
	}


	/**
	 * Busca as tarefas de um or�amento.
	 *
	 * @param orcamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefaorcamento> findByOrcamento(Orcamento orcamento) {
		if(orcamento == null || orcamento.getCdorcamento() == null){
			throw new SinedException("Or�amento n�o pode ser nulo.");
		}
		return query()
					.select("tarefaorcamento.cdtarefaorcamento, indice.cdindice, tarefaorcamento.descricao, " +
							"tarefaorcamento.qtde, tarefapai.cdtarefaorcamento, " +
							"tarefaorcamento.duracao, tarefaorcamento.dtinicio, tarefaorcamento.id, " +
							"tarefaorcamento.predecessoras, tarefaorcamento.custototal, listaTarefaorcamentorh.quantidade, cargo.cdcargo, " +
							"listaTarefaorcamentorg.quantidade, material.cdmaterial, orcamento.cdorcamento, " +
							"unidademedidarg.cdunidademedida, listaTarefaorcamentorg.tiporecursogeral, listaTarefaorcamentorg.outro, unidademedida.cdunidademedida")
					.join("tarefaorcamento.orcamento orcamento")
					.leftOuterJoin("tarefaorcamento.indice indice")
					.leftOuterJoin("indice.unidademedida unidademedida")
					.leftOuterJoin("tarefaorcamento.tarefapai tarefapai")
					.leftOuterJoin("tarefaorcamento.listaTarefaorcamentorh listaTarefaorcamentorh")
					.leftOuterJoin("listaTarefaorcamentorh.cargo cargo")
					.leftOuterJoin("tarefaorcamento.listaTarefaorcamentorg listaTarefaorcamentorg")
					.leftOuterJoin("listaTarefaorcamentorg.material material")
					.leftOuterJoin("listaTarefaorcamentorg.unidademedida unidademedidarg")
					.where("orcamento = ?", orcamento)
					.list();
	}
	
	public List<Tarefaorcamento> findByOrcamentoSemRecursos(Orcamento orcamento) {
		if(orcamento == null || orcamento.getCdorcamento() == null){
			throw new SinedException("Or�amento n�o pode ser nulo.");
		}
		return query()
					.select("tarefaorcamento.cdtarefaorcamento, tarefaorcamento.custototal")
					.join("tarefaorcamento.orcamento orcamento")
					.where("orcamento = ?", orcamento)
					.where("not exists (select trg.cdtarefaorcamentorg from Tarefaorcamentorg trg join trg.tarefaorcamento tor1 where tor1.cdtarefaorcamento = tarefaorcamento.cdtarefaorcamento)")
					.where("not exists (select trh.cdtarefaorcamentorh from Tarefaorcamentorh trh join trh.tarefaorcamento tor2 where tor2.cdtarefaorcamento = tarefaorcamento.cdtarefaorcamento)")
					.where("not exists (select tor.cdtarefaorcamento from Tarefaorcamento tor join tor.tarefapai tpai where tpai.cdtarefaorcamento = tarefaorcamento.cdtarefaorcamento)")
					.list();
	}


	/**
	 * M�todo que verifica se o or�amento possui tarefas
	 * 
	 * @param orcamento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Boolean orcamentoPossuiTarefa(Orcamento orcamento) {
		if(orcamento == null || orcamento.getCdorcamento() == null){
			throw new SinedException("Or�amento n�o pode ser nulo.");
		}
		return newQueryBuilderWithFrom(Long.class)
			.select("count (*)")
			.join("tarefaorcamento.orcamento orcamento")
			.where("orcamento = ?", orcamento)
			.unique() > 0L;
	}


	@SuppressWarnings("unchecked")
	public Double getTotalRecursosGeraisDireto(Orcamento orcamento) {
		String sql = 
		"SELECT SUM(TRG.QUANTIDADE * COALESCE(RC.CUSTOUNITARIO/100, M.VALORCUSTO, 0)) AS TOTAL " +
		"FROM TAREFAORCAMENTORG TRG " +
		"JOIN TAREFAORCAMENTO T ON T.CDTAREFAORCAMENTO = TRG.CDTAREFAORCAMENTO " +
		"LEFT OUTER JOIN MATERIAL M ON M.CDMATERIAL = TRG.CDMATERIAL " +
		"LEFT OUTER JOIN RECURSOCOMPOSICAO RC ON (RC.CDMATERIAL = M.CDMATERIAL OR RC.NOME = TRG.OUTRO) " +
		"WHERE T.CDORCAMENTO = " + orcamento.getCdorcamento() +
		"AND RC.CDORCAMENTO = " + orcamento.getCdorcamento();

		SinedUtil.markAsReader();
		List<Double> listaUpdate = getJdbcTemplate().query(sql, 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getDouble("TOTAL");
			}
		});
		
		return listaUpdate != null && listaUpdate.size() > 0 ? listaUpdate.get(0) : 0.0;
	}

	@SuppressWarnings("unchecked")
	public Double getTotalAcrescimo(Orcamento orcamento) {
		String sql = "SELECT " +
						"SUM( " +
						"(CASE  " +
						"	WHEN T.CUSTOACRESCIDO IS NOT NULL AND T.CUSTOACRESCIDO > 0 THEN T.CUSTOACRESCIDO  " +
						"	WHEN T.CUSTOTOTAL IS NOT NULL AND T.CUSTOTOTAL > 0 THEN T.CUSTOTOTAL " +
						"	ELSE 0 END) " +
						"-  " +
						"(CASE  " +
						"	WHEN T.CUSTOTOTAL IS NOT NULL AND T.CUSTOTOTAL > 0 THEN T.CUSTOTOTAL " +
						"	ELSE 0 END) " +
						") AS TOTAL " +
						"FROM TAREFAORCAMENTO T " +
						"WHERE T.CDORCAMENTO = " + orcamento.getCdorcamento() + " " +
						"AND NOT EXISTS (SELECT * FROM TAREFAORCAMENTO T2 WHERE T2.CDTAREFAPAI = T.CDTAREFAORCAMENTO) ";

		SinedUtil.markAsReader();
		List<Double> listaUpdate = getJdbcTemplate().query(sql, 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getDouble("TOTAL");
			}
		});
		
		return listaUpdate != null && listaUpdate.size() > 0 ? listaUpdate.get(0) : 0.0;
	}

	@SuppressWarnings("unchecked")
	public Double getTotalOutrasTarefas(Orcamento orcamento) {
		String sql = "SELECT SUM(T.CUSTOTOTAL) AS TOTAL " +
						"FROM TAREFAORCAMENTO T  " +
						"WHERE T.CDORCAMENTO = " + orcamento.getCdorcamento() + " " +
						"AND (SELECT COUNT(*) FROM TAREFAORCAMENTORG TRG WHERE TRG.CDTAREFAORCAMENTO = T.CDTAREFAORCAMENTO) = 0 " +
						"AND (SELECT COUNT(*) FROM TAREFAORCAMENTORH TRH WHERE TRH.CDTAREFAORCAMENTO = T.CDTAREFAORCAMENTO) = 0 " +
						"AND NOT EXISTS (SELECT * FROM TAREFAORCAMENTO T2 WHERE T2.CDTAREFAPAI = T.CDTAREFAORCAMENTO)";

		SinedUtil.markAsReader();
		List<Double> listaUpdate = getJdbcTemplate().query(sql, 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getDouble("TOTAL");
			}
		});
		
		return listaUpdate != null && listaUpdate.size() > 0 ? listaUpdate.get(0) : 0.0;
	}


	public Tarefaorcamento loadTarefa(Tarefaorcamento tarefa) {
		return query()
					.select("tarefaorcamento.cdtarefaorcamento, tarefaorcamento.qtde")
					.where("tarefaorcamento = ?", tarefa)
					.unique();
	}


	/**
	* M�todo que atualiza o prazo de orcamento
	*
	* @param orcamento
	* @since 06/03/2015
	* @author Luiz Fernando
	*/
	public void atualizarPrazoOrcamento(Orcamento orcamento) {
		if(orcamento != null && orcamento.getCdorcamento() != null && orcamento.getPrazosemana() != null){
			String sql = "update orcamento set prazosemana = ? where cdorcamento = ?";
			getJdbcTemplate().update(sql, new Object[]{orcamento.getPrazosemana(), orcamento.getCdorcamento()});

		}
	}
	
}
