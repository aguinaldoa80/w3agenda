package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Bempatrimonio;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MaterialFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("retira_acento(upper(bempatrimonio.nome))")
public class BempatrimonioDAO extends GenericDAO<Bempatrimonio> {

	@Override
	public void updateListagemQuery(QueryBuilder<Bempatrimonio> query, FiltroListagem _filtro) {
		MaterialFiltro filtro = (MaterialFiltro) _filtro;
		
		
		query
		.select("grupo.nome,  tipo.nome, bempatrimonio.cdmaterial, " +
				"bempatrimonio.nome, contagerencial.nome")
		.join("bempatrimonio.materialgrupo grupo")
		.join("bempatrimonio.unidademedida unidademedida")
		.join("bempatrimonio.contagerencial contagerencial")
		.leftOuterJoin("bempatrimonio.materialtipo tipo")
		.leftOuterJoin("bempatrimonio.listaMaterialempresa listaMaterialempresa")
		.leftOuterJoin("bempatrimonio.listaFornecedor listaFornecedor")
		.whereLikeIgnoreAll("bempatrimonio.nome", filtro.getNome())
		.whereLikeIgnoreAll("listaFornecedor.fornecedor.nome", filtro.getFornecedor())
		.where("contagerencial = ?", filtro.getContagerencial())
		.where("grupo = ?",filtro.getMaterialgrupo())
		.where("tipo = ?",filtro.getMaterialtipo())
		.where("listaMaterialempresa.empresa = ?",filtro.getEmpresa())
		.where("bempatrimonio.patrimonio = ?",Boolean.TRUE);
		
		if(filtro.getTipo() != null && !filtro.getTipo().equals("") && filtro.getCodigo() != null && !filtro.getCodigo().equals("")){
			if(filtro.getTipo().equals(MaterialFiltro.IDENTIFICACAO))
				query.whereLikeIgnoreAll("bempatrimonio.identificacao", filtro.getCodigo());
			else if(filtro.getTipo().equals(MaterialFiltro.CODIGO))
				query.where("bempatrimonio.cdmaterial = ?", Integer.valueOf(filtro.getCodigo()));
			else if(filtro.getTipo().equals(MaterialFiltro.CODIGO_ALTERNATIVO))
				query.whereLikeIgnoreAll("listaFornecedor.codigo", filtro.getCodigo());
			else if(filtro.getTipo().equals(MaterialFiltro.REFERENCIA))
				query.whereLikeIgnoreAll("bempatrimonio.referencia", filtro.getCodigo());
			else if(filtro.getTipo().equals(MaterialFiltro.CODIGO_BARRAS))
				query.whereLikeIgnoreAll("material.codigobarras", filtro.getCodigo());
			else if(filtro.getTipo().equals(MaterialFiltro.CODIGO_FABRICANTE))
				query.whereLikeIgnoreAll("material.codigofabricante", filtro.getCodigo());
				
		}
	}	
	
	/**
	 * Carrega um produto com todas as informa��es do Bempatrimonio.
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Bempatrimonio carregaPatrimonio(Material material) {
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		return query()
		.select("bempatrimonio.nomereduzido, bempatrimonio.valor, bempatrimonio.valormercado, " +
				"bempatrimonio.valorreferencia, bempatrimonio.vidautil, bempatrimonio.depreciacao, " +
				"bempatrimonio.observacao, bempatrimonio.cdmaterial, bempatrimonio.identificacao, bempatrimonio.nome")
		.where("bempatrimonio.cdmaterial = ?",material.getCdmaterial())
		.unique();
	}
	
	/**
	 * Deleta somente o registro da tabela Bempatrimonio.
	 * 
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void deletePatrimonio(Material material) {
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		String delete = "DELETE FROM BEMPATRIMONIO WHERE CDMATERIAL = ?";
		getJdbcTemplate().update(delete, new Object[]{material.getCdmaterial()});		
	}

	/**
	 * Insere um registro na tabela de Bempatrimonio.
	 * 
	 * @param produto
	 * @author Rodrigo Freitas
	 */
	public void insertPatrimonio(Bempatrimonio bem) {
		if (bem == null || bem.getCdmaterial() == null) {
			throw new SinedException("Patrim�nio n�o pode ser nulo.");
		}
		
		String insert = "INSERT INTO BEMPATRIMONIO (CDMATERIAL, NOMEREDUZIDO, VALOR, VALORMERCADO, VALORREFERENCIA, VIDAUTIL, " +
				"DEPRECIACAO, OBSERVACAO, CDUSUARIOALTERA, DTALTERA)" +
				" VALUES (?,?,?,?,?,?,?,?,?,?)";
		getJdbcTemplate().update(insert, new Object[]{	bem.getCdmaterial(),bem.getNomereduzido(),bem.getValor() != null ? bem.getValor().toLong() : null,bem.getValormercado() != null ? bem.getValormercado().toLong() : null,
														bem.getValorreferencia() != null ? bem.getValorreferencia().toLong() : null,bem.getVidautil(), bem.getDepreciacao() != null ? bem.getDepreciacao().toLong() : null,bem.getObservacao(),
														SinedUtil.getUsuarioLogado().getCdpessoa(),new Timestamp(System.currentTimeMillis())});
	}
	
	/**
	 * Faz update no bempatrimonio.
	 * 
	 * @param produto
	 * @author Rodrigo Freitas
	 */
	public void updatePatrimonio(Bempatrimonio bem){
		if (bem == null || bem.getCdmaterial() == null) {
			throw new SinedException("Patrim�nio n�o pode ser nulo.");
		}
		
		String update = "UPDATE BEMPATRIMONIO SET NOMEREDUZIDO = ?, VALOR = ?, VALORMERCADO = ?, VALORREFERENCIA = ?, VIDAUTIL = ?, " +
						"DEPRECIACAO = ?, OBSERVACAO = ?, CDUSUARIOALTERA = ?, DTALTERA = ? WHERE BEMPATRIMONIO.CDMATERIAL = ?";
		getJdbcTemplate().update(update, new Object[]{	bem.getNomereduzido(),bem.getValor().toLong(),bem.getValormercado().toLong(),
				bem.getValorreferencia().toLong(),bem.getVidautil(),bem.getDepreciacao().toLong(),bem.getObservacao(), 
				SinedUtil.getUsuarioLogado().getCdpessoa(),new Timestamp(System.currentTimeMillis()),bem.getCdmaterial()});
	}
	
	/**
	 * Retorna o n�mero de registros da tabela Bempatrimonio do cdmaterial passado.
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Long countPatrimonio(Material material){
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Bempatrimonio.class)
					.where("bempatrimonio.cdmaterial = ?",material.getCdmaterial())
					.unique();
	}
	
	/**
	 * Retorna o n�mero de registros que tem na tabela bempatrimonio com o 
	 * mesmo nome do patrimonio cadastrado.
	 * 
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Long verificaPatrimonio(Material bean) {
		if (bean == null || bean.getPatrimonio_nomereduzido() == null) {
			throw new SinedException("Nome reduzido do patrim�nio n�o pode ser nulo.");
		}
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(Bempatrimonio.class)
					.where("upper(bempatrimonio.nomereduzido) = ?",bean.getPatrimonio_nomereduzido().toUpperCase())
					.where("bempatrimonio.cdmaterial <> ?",bean.getCdmaterial())
					.unique();
	}

	/**
	 * Busca patrimonio pelo tipo e grupo
	 * 
	 * @param materialtipo
	 * @param materialgrupo
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Bempatrimonio> findByTipoGrupo(Materialgrupo materialgrupo, Materialtipo materialtipo) {
		return query()
			.select("bempatrimonio.cdmaterial, bempatrimonio.nome")
			.where("bempatrimonio.materialgrupo = ?",materialgrupo)
			.where("bempatrimonio.materialtipo = ?",materialtipo)
			.where("bempatrimonio.patrimonio = ?",Boolean.TRUE)
			.orderBy("bempatrimonio.nome")
			.list();
	}

	/**
	 * Busca os bem patrim�nios para o autocomplete
	 *
	 * @param q
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/09/2013
	 */
	public List<Bempatrimonio> findForAutocompltePatrimonio(String q) {
//		boolean recapagem = "TRUE".equalsIgnoreCase(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.RECAPAGEM));
		
		QueryBuilder<Bempatrimonio> queryBuilder = query()
					.openParentheses()
						.whereLikeIgnoreAll("bempatrimonio.identificacao", q)
						.or()
						.whereLikeIgnoreAll("bempatrimonio.nome", q)
						.or()
						.where("UPPER(retira_acento(listaMaterialnumeroserie.numero)) LIKE '%'||?||'%'", Util.strings.tiraAcento(q).toUpperCase())
					.closeParentheses()
					.leftOuterJoin("bempatrimonio.listaMaterialnumeroserie listaMaterialnumeroserie")
					.where("bempatrimonio.patrimonio = ?", Boolean.TRUE)
					.where("bempatrimonio.ativo = true")
					.orderBy("bempatrimonio.nome");
		
		String select = "bempatrimonio.cdmaterial, bempatrimonio.nome, bempatrimonio.identificacao, listaMaterialnumeroserie.numero";
		
//		if (recapagem){
//			select += ", listaMaterialnumeroserie.numero";
//			queryBuilder.leftOuterJoin("bempatrimonio.listaMaterialnumeroserie listaMaterialnumeroserie");
//		}
		
		queryBuilder.select(select);
		
		return queryBuilder.list();
	}
	
	/**
	 * @param query
	 * @param tipo
	 * @param grupo
	 * @author Murilo
	 * @return
	 */
	public List<Bempatrimonio> findBemPatrimonioAutoComplete(String query, Materialtipo tipo, Materialgrupo grupo) {
		QueryBuilder<Bempatrimonio> q = query()
				.select("bempatrimonio.cdmaterial, bempatrimonio.nome")
				.where("bempatrimonio.materialgrupo = ?",grupo)
				.where("bempatrimonio.materialtipo = ?",tipo)
				.where("bempatrimonio.patrimonio = ?",Boolean.TRUE)
				.whereLikeIgnoreAll("bempatrimonio.nome", query)
				.orderBy("bempatrimonio.nome");
		return q.list();
	}
}