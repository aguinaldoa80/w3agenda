package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Fatorajuste;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.FatorajusteFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FatorajusteDAO extends GenericDAO<Fatorajuste>{

	@Override
	public void updateListagemQuery(QueryBuilder<Fatorajuste> query, FiltroListagem _filtro) {
		FatorajusteFiltro filtro = (FatorajusteFiltro) _filtro;
		query.select("fatorajuste.cdfatorajuste, fatorajuste.sigla, fatorajuste.descricao")
		.whereLikeIgnoreAll("fatorajuste.sigla", filtro.getSigla())
		.whereLikeIgnoreAll("fatorajuste.descricao", filtro.getDescricao())
		.orderBy("fatorajuste.cdfatorajuste desc");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Fatorajuste> query) {
		query.select("fatorajuste.cdfatorajuste, fatorajuste.sigla, fatorajuste.descricao, fatorajuste.dtaltera, fatorajuste.cdusuarioaltera, " +
					 "listaFatorajustevalor.cdfatorajustevalor, listaFatorajustevalor.mesano, listaFatorajustevalor.valor")
			.leftOuterJoin("fatorajuste.listaFatorajustevalor listaFatorajustevalor")
			.orderBy("listaFatorajustevalor.mesano");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaFatorajustevalor");
		super.updateSaveOrUpdate(save);
	}

	
	/**
	* M�todo que carrega os dados principais do fator de ajuste
	*
	* @param fatorajuste
	* @return
	* @since Oct 14, 2011
	* @author Luiz Fernando F Silva
	*/
	public Fatorajuste carregaFatorajuste(Fatorajuste fatorajuste) {
		if(fatorajuste == null || fatorajuste.getCdfatorajuste() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("fatorajuste.cdfatorajuste, listaFatorajustevalor.cdfatorajustevalor, listaFatorajustevalor.mesano, " +
					"listaFatorajustevalor.valor")
			.join("fatorajuste.listaFatorajustevalor listaFatorajustevalor")
			.where("fatorajuste = ?", fatorajuste)
			.unique();
	}
		
}
