package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Garantiareformaresultado;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.GarantiareformaresultadoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class GarantiareformaresultadoDAO extends GenericDAO<Garantiareformaresultado>{

	@Override
	public void updateListagemQuery(QueryBuilder<Garantiareformaresultado> query,
			FiltroListagem filtro) {
		GarantiareformaresultadoFiltro _filtro = (GarantiareformaresultadoFiltro) filtro;
		query
			.select("garantiareformaresultado.cdgarantiareformaresultado, garantiareformaresultado.nome, garantiareformaresultado.ativo, "+
					"garantiareformaresultado.mensagem1, garantiareformaresultado.mensagem2, garantiareformaresultado.dtaltera, garantiareformaresultado.cdusuarioaltera")
			.whereLikeIgnoreAll("garantiareformaresultado.nome", _filtro.getNome())
			.where("garantiareformaresultado.ativo = ?", _filtro.getAtivo())
			.orderBy("upper(retira_acento(garantiareformaresultado.nome))");
	}
}
