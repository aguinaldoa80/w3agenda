package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Agendamentoservico;
import br.com.linkcom.sined.geral.bean.Agendamentoservicocancela;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Escala;
import br.com.linkcom.sined.geral.bean.Escalacolaborador;
import br.com.linkcom.sined.geral.bean.Escalahorario;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoAgendamentoServico;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.AgendamentoservicoFiltro;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.AgendamentoservicoapoioFiltro;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.filter.ColaboradoragendaservicoReportFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

public class AgendamentoservicoDAO extends GenericDAO<Agendamentoservico> {

	@Override
	public void updateListagemQuery(QueryBuilder<Agendamentoservico> query,	FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		AgendamentoservicoFiltro filtro = (AgendamentoservicoFiltro) _filtro;
		
		query.
			select("agendamentoservico.cdagendamentoservico, agendamentoservico.data, " +
					"cliente.cdpessoa, cliente.nome, " +
					"requisicao.cdrequisicao, requisicao.dtrequisicao, requisicao.ordemservico, " +
					"material.cdmaterial, material.nome, " +
					"colaborador.cdpessoa, colaborador.nome, " +
					"aux_agendamentoservico.situacao, " +
					"escalahorario.cdescalahorario, escalahorario.horainicio, escalahorario.horafim, escalahorario.diasemana")
					
			.join("agendamentoservico.cliente cliente")
			.join("agendamentoservico.material material")
			.join("agendamentoservico.colaborador colaborador")
			.join("agendamentoservico.escalahorario escalahorario")
			.join("agendamentoservico.aux_agendamentoservico aux_agendamentoservico")
			.leftOuterJoin("agendamentoservico.requisicao requisicao")
			.where("cliente = ?", filtro.getCliente())
			.where("requisicao = ?", filtro.getRequisicao())
			.where("material = ?", filtro.getMaterial())
			.where("agendamentoservico.agendamentoservicocancela = ?", filtro.getAgendamentoservicocancela())
			.where("agendamentoservico.empresa = ?", filtro.getEmpresa())
			.where("material.materialtipo = ?", filtro.getMaterialtipo())
			.where("agendamentoservico.data >= ?", filtro.getDtde())
			.where("agendamentoservico.data <= ?", filtro.getDtate());

		Usuario usuario = SinedUtil.getUsuarioLogado();
		if (usuario.getRestricaoclientevendedor() != null && usuario.getRestricaoclientevendedor()){
			query.where("colaborador = ?", usuario);	
		}
		else{
			query.where("colaborador = ?", filtro.getColaborador());
		}
		if(filtro.getCategoria() != null && filtro.getCategoria().getCdcategoria() != null)
			query.where("cliente.id in (SELECT ps.pessoa.id FROM Pessoacategoria ps WHERE ps.categoria.id = " + filtro.getCategoria().getCdcategoria() + ")");
		
		if(filtro.getListaSituacoes() != null) 
			query.whereIn("aux_agendamentoservico.situacao", SituacaoAgendamentoServico.listAndConcatenate(filtro.getListaSituacoes()));
		
		query.orderBy("agendamentoservico.cdagendamentoservico desc");
	}
		
	@Override
	public void updateEntradaQuery(QueryBuilder<Agendamentoservico> query) {
		query
			.select("agendamentoservico.cdagendamentoservico, agendamentoservico.data, agendamentoservico.dtaltera, agendamentoservico.cdusuarioaltera, empresa.cdpessoa, " +
					"cliente.cdpessoa, cliente.nome, materialtipo.cdmaterialtipo, materialtipo.nome, material.cdmaterial, material.qtdeunidade, colaborador.cdpessoa, " +
					"requisicao.cdrequisicao, requisicao.dtrequisicao, requisicao.ordemservico, " +
					"colaborador.nome, empresa.nome, empresa.razaosocial, empresa.nomefantasia, material.nome, " +
					"escalahorario.cdescalahorario, escalahorario.horainicio, escalahorario.horafim, escalahorario.diasemana, " +
					"listaagendamentosservicohistorico.cdagendamentoservicohistorico, listaagendamentosservicohistorico.observacao, listaagendamentosservicohistorico.dtaltera, " +
					"listaagendamentosservicohistorico.cdusuarioaltera, aux_agendamentoservico.situacao")
			.leftOuterJoin("agendamentoservico.empresa empresa")
			.leftOuterJoin("agendamentoservico.cliente cliente")
			.leftOuterJoin("agendamentoservico.requisicao requisicao")
			.leftOuterJoin("agendamentoservico.material material")
			.leftOuterJoin("material.materialtipo materialtipo")
			.leftOuterJoin("agendamentoservico.colaborador colaborador")
			.leftOuterJoin("agendamentoservico.aux_agendamentoservico aux_agendamentoservico")
			.leftOuterJoin("agendamentoservico.escalahorario escalahorario")
			.leftOuterJoin("agendamentoservico.listaagendamentosservicohistorico listaagendamentosservicohistorico");
	}

	/**
	 * M�todo que atualiza situa��o servi�os de agendamento
	 * 
	 * @Author Tom�s Rabelo
	 */
	public void updateAux() {
		getJdbcTemplate().execute("SELECT ATUALIZA_AGENDAMENTOSERVICO()");
	}

	/**
	 * M�todo que carrega os agendamentos servi�os para realizar a�oes de cancelar e finalizar
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Agendamentoservico> loadForAcoes(String whereIn) {
		if(whereIn == null || whereIn.equals(""))
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("agendamentoservico.cdagendamentoservico, aux_agendamentoservico.situacao")
			.join("agendamentoservico.aux_agendamentoservico aux_agendamentoservico")
			.whereIn("agendamentoservico.cdagendamentoservico", whereIn)
			.list();
	}

	/**
	 * M�todo que atualiza os agendamento servi�os para cancelado
	 * 
	 * @param whereIn
	 * @author Tom�s Rabelo
	 */
	public void cancelar(String whereIn, Agendamentoservicocancela agendamentoservicocancela) {
		if(agendamentoservicocancela != null && agendamentoservicocancela.getCdagendamentoservicocancela() != null){
			getHibernateTemplate().bulkUpdate("update Agendamentoservico agendamentoservico set agendamentoservico.dtcancelado = ?, agendamentoservico.agendamentoservicocancela = ? " + 
					  "where agendamentoservico.id in ("+whereIn+")", new Object[]{new Date(System.currentTimeMillis()), agendamentoservicocancela});
		} else {
			getHibernateTemplate().bulkUpdate("update Agendamentoservico agendamentoservico set agendamentoservico.dtcancelado = ? " + 
										  "where agendamentoservico.id in ("+whereIn+")", new Object[]{new Date(System.currentTimeMillis())});
		}
	}

	/**
	 * M�todo que atualiza os agendamento servi�os para finalizado
	 * 
	 * @param whereIn
	 * @author Tom�s Rabelo
	 */
	public void realizado(String whereIn) {
		getHibernateTemplate().bulkUpdate("update Agendamentoservico agendamentoservico set agendamentoservico.dtrealizado = ? " + 
									  "where agendamentoservico.id in ("+whereIn+")", new Object[]{new Date(System.currentTimeMillis())});
	}

	/**
	 * M�todo que retorna a quantidade de agendamentos de um determinado material em um determinado dia
	 * 
	 * @param data
	 * @return
	 * @author Tom�s Rabelo
	 * @param material 
	 */
	public Long qtdeAgendamentoServicoMaterial(Agendamentoservico agendamentoservico) {
		if(agendamentoservico.getMaterial() == null || agendamentoservico.getMaterial().getCdmaterial() == null|| agendamentoservico.getData() == null)
			throw new SinedException("Par�metros inv�lidos!");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Agendamentoservico.class)
			.join("agendamentoservico.colaborador colaborador")
			.join("agendamentoservico.material material")
			.join("agendamentoservico.aux_agendamentoservico aux_agendamentoservico")
			.leftOuterJoin("agendamentoservico.escalahorario escalahorario")
			.where("agendamentoservico.data = ?", agendamentoservico.getData())
			.where("agendamentoservico.material = ?", agendamentoservico.getMaterial())
			.where("agendamentoservico.colaborador = ?", agendamentoservico.getColaborador())
			.where("agendamentoservico <> ?", agendamentoservico.getCdagendamentoservico() != null ? agendamentoservico : null)
			.where("escalahorario = ?", agendamentoservico.getEscalahorario())
			.where("aux_agendamentoservico.situacao <> ?", SituacaoAgendamentoServico.CANCELADO)
			.unique();
	}

	/**
	 * M�todo que retorna todos agendamentos de servi�o dentro de um per�odo, de um material e colaborador
	 * 
	 * @param dtReferencia
	 * @param dtReferenciaLimite
	 * @param material
	 * @param colaborador
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Agendamentoservico> findAgendaServicoData(Date dtReferencia, Date dtReferenciaLimite, Material material, Colaborador colaborador) {
		return query()
			.select("agendamentoservico.cdagendamentoservico, agendamentoservico.data, colaborador.cdpessoa, colaborador.nome, cliente.cdpessoa, cliente.nome, " +
					"escalahorario.cdescalahorario, escalahorario.horainicio, escalahorario.horafim")
			.join("agendamentoservico.escalahorario escalahorario")
			.join("agendamentoservico.cliente cliente")
			.join("agendamentoservico.material material")
			.join("agendamentoservico.colaborador colaborador")
			.join("agendamentoservico.aux_agendamentoservico aux_agendamentoservico")
			.where("agendamentoservico.data >= ?", dtReferencia)
			.where("agendamentoservico.data <= ?", dtReferenciaLimite)
			.where("colaborador = ?", colaborador)
			.where("material = ?", material)
			.where("aux_agendamentoservico.situacao <> ?", SituacaoAgendamentoServico.CANCELADO)
			.list();
	}

	/**
	 * M�todo que retorna todos agendamentos de servi�o dentro de um per�odo, de um material e colaborador ou cliente
	 * 
	 * @param dtReferencia
	 * @param dtReferenciaAte
	 * @param agendamentoservicoapoioFiltro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Agendamentoservico> findAgendamentoServicoData(Date dtReferencia, Date dtReferenciaAte, AgendamentoservicoapoioFiltro agendamentoservicoapoioFiltro) {
		QueryBuilder<Agendamentoservico> query = query()
			.select("agendamentoservico.cdagendamentoservico, agendamentoservico.data, agendamentoservico.dtrealizado, agendamentoservico.dtcancelado, colaborador.cdpessoa, " +
					"colaborador.nome, cliente.cdpessoa, cliente.nome, escalahorario.cdescalahorario, escalahorario.horainicio, escalahorario.horafim, material.cdmaterial, material.nome, " +
					"empresa.cdpessoa, aux_agendamentoservico.situacao, aux_agendamentoservico.cdagendamentoservico, " +
					"requisicao.cdrequisicao, requisicao.dtrequisicao, requisicao.ordemservico ")
			.from(Agendamentoservico.class)
			.leftOuterJoin("agendamentoservico.escalahorario escalahorario")
			.leftOuterJoin("agendamentoservico.cliente cliente")
			.leftOuterJoin("agendamentoservico.material material")
			.leftOuterJoin("agendamentoservico.colaborador colaborador")
			.leftOuterJoin("agendamentoservico.empresa empresa")
			.leftOuterJoin("agendamentoservico.aux_agendamentoservico aux_agendamentoservico")
			.leftOuterJoin("agendamentoservico.requisicao requisicao")
			.where("agendamentoservico.data >= ?", dtReferencia)
			.where("agendamentoservico.data <= ?", dtReferenciaAte)
			.where("aux_agendamentoservico.situacao <> ?", SituacaoAgendamentoServico.CANCELADO);
		
		if(agendamentoservicoapoioFiltro.getColaborador() != null && agendamentoservicoapoioFiltro.getColaborador().getCdpessoa() != null)
			query.where("colaborador = ?", agendamentoservicoapoioFiltro.getColaborador());
		
		return query.list();
	}
	
	/**
	 * M�todo que retorna os agendamentos de servico para o relat�rio de acordo com os filtros informados 
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Agendamentoservico> findAgendamentosServicosReport(AgendamentoservicoFiltro filtro) {
		QueryBuilder<Agendamentoservico> query = querySined();
		updateListagemQuery(query, filtro);
		
		query
			.select("agendamentoservico.cdagendamentoservico, agendamentoservico.data, cliente.cdpessoa, cliente.nome, cliente.identificador, material.cdmaterial, material.nome, " +
					"colaborador.cdpessoa, colaborador.nome, aux_agendamentoservico.situacao, escalahorario.cdescalahorario, escalahorario.horainicio, " +
					"escalahorario.horafim, escalahorario.diasemana, pessoatratamento.nome, agendamentoservico.sessao," +
					"requisicao.cdrequisicao, requisicao.dtrequisicao ")
			.leftOuterJoinIfNotExists("material.materialtipo materialtipo")
			.leftOuterJoinIfNotExists("cliente.pessoatratamento pessoatratamento");
		
		String orderBy = "agendamentoservico.data, escalahorario.horainicio, escalahorario.horafim";
		
		if(filtro.getTipovisaoreport().equals(AgendamentoservicoFiltro.CLIENTE))
			query.orderBy("cliente.nome, "+orderBy);
		else
			query.orderBy("colaborador.nome, "+orderBy);
		
		return query.list();
	}
	
	
	/**
	 * Retorna dados de agendamentos de servi�os para o relat�rio de agendamentos de servi�os
	 * de acordo com o par�metros informados no filtro. 
	 * 
	 * @author Taidson
	 * @since 07/05/2010
	 * 
	 */
	public List<Agendamentoservico> findForRelatorioAgendaServico(ColaboradoragendaservicoReportFiltro filtro) {
		if (filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		
		return query()
			.select("agendamentoservico.cdagendamentoservico, agendamentoservico.data, cliente.cdpessoa, cliente.nome, material.cdmaterial, material.nome, " +
					"colaborador.cdpessoa, colaborador.nome, aux_agendamentoservico.situacao, escalahorario.cdescalahorario, escalahorario.horainicio, " +
					"escalahorario.horafim, escalahorario.diasemana, cliente.identificador, " +
					"requisicao.cdrequisicao, requisicao.dtrequisicao ")
			.join("agendamentoservico.empresa empresa")
			.join("agendamentoservico.cliente cliente")
			.join("agendamentoservico.material material")
			.join("material.materialtipo materialtipo")
			.join("agendamentoservico.colaborador colaborador")
			.join("agendamentoservico.escalahorario escalahorario")
			.join("agendamentoservico.aux_agendamentoservico aux_agendamentoservico")
			.leftOuterJoin("agendamentoservico.requisicao requisicao")
			.where("material = ?", filtro.getMaterial())
			.where("dtcancelado is null")
			.where("colaborador = ?", filtro.getColaborador())
			.where("agendamentoservico.data >= ?", filtro.getDtinicio())
			.where("agendamentoservico.data <= ?", filtro.getDtfim())
			.where("requisicao = ?", filtro.getRequisicao())
			//Taidson - 07/05/2010
			//Ordena��o imprescind�vel para o correto funcionamento do realat�rio 'Agendamentos de servi�os'.
			.orderBy("colaborador.nome, colaborador.cdpessoa, cliente.nome, cliente.cdpessoa, agendamentoservico.data")
			.list();
	}

	/**
	 * M�todo que chama procedure para atualizar as sess�es de um determinado colaborador e servi�o
	 * 
	 * @param colaborador
	 * @param material
	 * @author Tom�s Rabelo
	 * @param cliente 
	 */
	public void callProcedureAtualizaSessaoAgendamentoServico(Colaborador colaborador, Material material, Cliente cliente) {
		if(colaborador == null || colaborador.getCdpessoa() == null || material == null || material.getCdmaterial() == null || cliente == null || cliente.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos para procedure atualiza_agendamentoservico_sessao(?,?,?)"); 
		
		getJdbcTemplate().execute("SELECT atualiza_agendamentoservico_sessao(" + colaborador.getCdpessoa() + ", "+material.getCdmaterial()+", "+cliente.getCdpessoa()+")");
	}

	/**
	 * M�todo que verificar se o cliente j� foi alocado em algum servi�o na data e escala de hor�rio referida
	 * 
	 * @param cliente
	 * @param escalahorarioAux
	 * @param data
	 * @param agendamentoservico 
	 * @return
	 */
	public List<Agendamentoservico> verificaAgendamentoClienteHorario(Colaborador colaborador, Cliente cliente, Escalahorario escalahorarioAux, Date data, Agendamentoservico agendamentoservico) {
		return query()
			.select("agendamentoservico.cdagendamentoservico, material.cdmaterial, agendamentoservico.data, escalahorario.cdescalahorario, escalahorario.horainicio, escalahorario.horafim, colaborador.cdpessoa, colaborador.nome, cliente.cdpessoa")
			.from(Agendamentoservico.class)
			.join("agendamentoservico.escalahorario escalahorario")
			.join("agendamentoservico.material material")
			.join("agendamentoservico.cliente cliente")
			.join("agendamentoservico.colaborador colaborador")
			.where("cliente = ?", cliente)
			.where("colaborador = ?", colaborador)
			/*.where("escalahorario = ?", escalahorarioAux)*/
			.where("agendamentoservico.data = ?", data)
			.where("agendamentoservico.dtcancelado is null")
			.where("agendamentoservico <> ?", agendamentoservico != null && agendamentoservico.getCdagendamentoservico() != null ? agendamentoservico : null)
			.openParentheses()
				.openParentheses()
					.where("escalahorario.horainicio < ?", escalahorarioAux.getHorainicio())
					.where("escalahorario.horafim > ?", escalahorarioAux.getHorainicio())
				.closeParentheses().or()
				.openParentheses()
					.where("escalahorario.horainicio < ?", escalahorarioAux.getHorafim())
					.where("escalahorario.horafim > ?", escalahorarioAux.getHorafim())
				.closeParentheses().or()
				.openParentheses()
					.where("escalahorario.horainicio > ?", escalahorarioAux.getHorainicio())
					.where("escalahorario.horainicio < ?", escalahorarioAux.getHorafim())
				.closeParentheses().or()
				.openParentheses()
					.where("escalahorario.horafim > ?", escalahorarioAux.getHorainicio())
					.where("escalahorario.horafim < ?", escalahorarioAux.getHorafim())
				.closeParentheses().or()
				.openParentheses()
					.where("escalahorario.horainicio >= ?", escalahorarioAux.getHorainicio())
					.where("escalahorario.horafim <= ?", escalahorarioAux.getHorafim())
				.closeParentheses()
			.closeParentheses()
			.list();
	}

	/**
	 * M�todo que verifica se o Colaborador, e uma determinada escala, possui agendamentos em aberto ou agendado 
	 * 
	 * @param colaborador
	 * @param escala
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean verificaAgendamentoColaboradorEmAbertoOuAgendado(Colaborador colaborador, Escala escala) {
		if(colaborador == null || colaborador.getCdpessoa() == null || escala == null || escala.getCdescala() == null)
			throw new SinedException("Par�metros inv�lidos");
		
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.from(Agendamentoservico.class)
			.join("agendamentoservico.colaborador colaborador")
			.join("agendamentoservico.escalahorario escalahorario")
			.join("escalahorario.escala escala")
			.join("agendamentoservico.aux_agendamentoservico aux_agendamentoservico")
			.where("colaborador = ?", colaborador)
			.where("escala = ?", escala)
			.openParentheses()
				.where("aux_agendamentoservico.situacao = ?", SituacaoAgendamentoServico.AGENDADO).or()
				.where("aux_agendamentoservico.situacao = ?", SituacaoAgendamentoServico.EM_ANDAMENTO)
			.closeParentheses()
			.unique() > 0;
	}

	public void atualizaDataEscalahorario(Agendamentoservico agendamentoservico, Date data, Escalahorario escalahorario) {
		getHibernateTemplate().bulkUpdate("update Agendamentoservico agenda set agenda.data = ?, agenda.escalahorario = ? where agenda.id = ?", 
				new Object[]{data, escalahorario, agendamentoservico.getCdagendamentoservico()});
	}

	public boolean isEscalacolaboradorForaPeriodo(Colaborador colaborador, Escalacolaborador escalacolaborador) {
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.from(Agendamentoservico.class)
			.join("agendamentoservico.colaborador colaborador")
			.join("agendamentoservico.escalahorario escalahorario")
			.join("escalahorario.escala escala")
			.where("colaborador = ?", colaborador)
			.where("escala = ?", escalacolaborador.getEscala())
			.where("agendamentoservico.dtcancelado is null")
			.where("agendamentoservico.dtrealizado is null")
			.openParentheses()
				.where("agendamentoservico.data < ?", escalacolaborador.getDtinicio()).or()
			    .where("agendamentoservico.data > ?", escalacolaborador.getDtfim())
			.closeParentheses()
			.unique() > 0;
	}

	/**
	 * M�todo que busca agendamentos de servi�o pelo where in
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Agendamentoservico> findByWhereIn(String whereIn) {
		if(whereIn == null || whereIn.equals(""))
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("agendamentoservico.cdagendamentoservico, material.cdmaterial, cliente.cdpessoa, colaborador.cdpessoa, requisicao.cdrequisicao")
			.join("agendamentoservico.material material")
			.join("agendamentoservico.cliente cliente")
			.join("agendamentoservico.colaborador colaborador")
			.leftOuterJoin("agendamentoservico.requisicao requisicao")
			.whereIn("agendamentoservico.id", whereIn)
			.list();
	}

	public List<Agendamentoservico> findForAgenda(Date data) {
		return query()
					.select("agendamentoservico.cdagendamentoservico, agendamentoservico.data, cliente.nome, " +
							"material.nome, escalahorario.horainicio, escalahorario.horafim")
					.join("agendamentoservico.cliente cliente")
					.join("agendamentoservico.material material")
					.join("agendamentoservico.escalahorario escalahorario")
					.where("agendamentoservico.data = ?", data)
					.where("agendamentoservico.colaborador = ?", SinedUtil.getUsuarioComoColaborador())
					.list();
	}

	/**
	 * Busca a listagem para gerar o CSV
	 *
	 * @param filtro
	 * @return
	 * @since 02/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Agendamentoservico> findForCsv(AgendamentoservicoFiltro filtro) {
		QueryBuilder<Agendamentoservico> query = querySined();
		updateListagemQuery(query, filtro);
		
		query
			.select("agendamentoservico.cdagendamentoservico, agendamentoservico.data, cliente.cdpessoa, cliente.nome, material.cdmaterial, material.nome, " +
					"colaborador.cdpessoa, colaborador.nome, aux_agendamentoservico.situacao, escalahorario.cdescalahorario, escalahorario.horainicio, " +
					"escalahorario.horafim, escalahorario.diasemana, categoria.cdcategoria, categoria.nome")
			.leftOuterJoin("cliente.listaPessoacategoria pessoacategoria")
			.leftOuterJoin("pessoacategoria.categoria categoria")
			.orderBy("agendamentoservico.cdagendamentoservico desc");
		
		return query.list();
	}
	
	@SuppressWarnings({"unchecked", "rawtypes"})
	public Long getCountListagem(AgendamentoservicoFiltro filtro){
		QueryBuilder queryBuilder = newQueryBuilderWithFrom(Agendamentoservico.class);
		updateListagemQuery(queryBuilder, filtro);
		
		QueryBuilderSined<Long> countQueryBuilder = newQueryBuilderSined(Long.class);
        countQueryBuilder.select("count(agendamentoservico.cdagendamentoservico)");
		
        QueryBuilder.From from = queryBuilder.getFrom();
        countQueryBuilder.from(from);
        
        List<QueryBuilder.Join> joins = queryBuilder.getJoins();
        for (QueryBuilder.Join join: joins) {
			countQueryBuilder.join(join.getJoinMode(), false, join.getPath());
		}
        
        QueryBuilder.Where where = queryBuilder.getWhere();
		countQueryBuilder.where(where);
		countQueryBuilder.setUseTranslator(false);
		
		return countQueryBuilder.unique();
	}
}
