package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contaempresa;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContaempresaDAO extends GenericDAO<Contaempresa>{

	public List<Contaempresa> findForAndroid(String whereIn) {
		return query()
		.select("contaempresa.cdcontaempresa, conta.cdconta, empresa.cdpessoa")
		.join("contaempresa.conta conta")
		.join("contaempresa.empresa empresa")
		.whereIn("contaempresa.cdcontaempresa", whereIn)
		.list();
	}
}
