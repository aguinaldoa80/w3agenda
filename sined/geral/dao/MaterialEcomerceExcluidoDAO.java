package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.MaterialEcomerceExcluido;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialEcomerceExcluidoDAO extends GenericDAO<MaterialEcomerceExcluido>{

	public MaterialEcomerceExcluido loadByIdEcommerce(Integer idEcommerce){
		return querySined()
				.select("materialEcomerceExcluido.cdMaterialEcomerceExcluido, material.nome, material.cdmaterial, unidademedida.cdunidademedida")
				.join("materialEcomerceExcluido.material material")
				.join("material.unidademedida unidademedida")
				.where("materialEcomerceExcluido.idEcommerce = ?", idEcommerce)
				.setMaxResults(1)
				.unique();
	}
}
