package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoSegmento;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.BancoConfiguracaoRetornoSegmentoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class BancoConfiguracaoRetornoSegmentoDAO extends GenericDAO<BancoConfiguracaoRetornoSegmento> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<BancoConfiguracaoRetornoSegmento> query, FiltroListagem _filtro) {
		BancoConfiguracaoRetornoSegmentoFiltro filtro = (BancoConfiguracaoRetornoSegmentoFiltro) _filtro;
		query
			.joinFetch("bancoConfiguracaoRetornoSegmento.banco banco")
			.whereLikeIgnoreAll("bancoConfiguracaoRetornoSegmento.identificador", filtro.getIdentificador())
			.whereLikeIgnoreAll("bancoConfiguracaoRetornoSegmento.nome", filtro.getNome())
			.where("bancoConfiguracaoRetornoSegmento.tipo = ?", filtro.getTipoSegmento())
			.where("banco = ?", filtro.getBanco());
		super.updateListagemQuery(query, filtro);
	}
	
	public List<BancoConfiguracaoRetornoSegmento> findByBanco(Banco banco){
		return query()
				.joinFetch("bancoConfiguracaoRetornoSegmento.banco banco")
				.where("banco = ?", banco)
				.list();
	}
	
	/**
	 * M�todo respons�vel por retornar se existe cadastro do segmento
	 * @param segmento
	 * @return
	 * @since 09/06/2016
	 * @author C�sar
	 */
	public List<BancoConfiguracaoRetornoSegmento> findForImportacao (){
		return query().select("bancoConfiguracaoRetornoSegmento.cdbancoconfiguracaoretornosegmento, bancoConfiguracaoRetornoSegmento.nome," +
				"bancoConfiguracaoRetornoSegmento.identificador, bancoConfiguracaoRetornoSegmento.tipo, banco.cdbanco, banco.nome")
				.leftOuterJoin("bancoConfiguracaoRetornoSegmento.banco banco")
				.list();
	}
	
	/**
	 * M�todo respons�vel por retornar lista para exporta��o
	 * @param whereIn
	 * @return
	 * @since 09/06/2016
	 * @author C�sar
	 */
	public List<BancoConfiguracaoRetornoSegmento> findforWhereIn(String whereIn){
		if(whereIn == null || whereIn.isEmpty()){
			throw new SinedException("Par�metro Incorreto para pesquisa.");
		}
		return query().select("bancoConfiguracaoRetornoSegmento.cdbancoconfiguracaoretornosegmento, bancoConfiguracaoRetornoSegmento.nome," +
				"bancoConfiguracaoRetornoSegmento.tipo, bancoConfiguracaoRetornoSegmento.identificador, banco.cdbanco, banco.nome")
				.leftOuterJoin("bancoConfiguracaoRetornoSegmento.banco banco")
				.whereIn("bancoConfiguracaoRetornoSegmento.cdbancoconfiguracaoretornosegmento", whereIn)
				.list();
	}
	
	/**
	 * M�todo respons�vel por buscar BancoConfiguracaoRetornoSegmento iguais na base de dados
	 * @param bancoConfig
	 * @return
	 * @since 15/05/2016
	 * @author C�sar
	 */
	public List<BancoConfiguracaoRetornoSegmento> validateDuplicacdo(BancoConfiguracaoRetornoSegmento bancoConfig){
		if(bancoConfig == null || bancoConfig.getBanco() == null || bancoConfig.getBanco().getCdbanco() == null 
				|| bancoConfig.getTipo() == null || bancoConfig.getNome() == null || bancoConfig.getIdentificador() == null){

			throw new SinedException("Par�mentro incorreto para pesquisa.");
		}
		
		return query()
					.select("bancoConfiguracaoRetornoSegmento.cdbancoconfiguracaoretornosegmento, bancoConfiguracaoRetornoSegmento.nome")
					.where("bancoConfiguracaoRetornoSegmento.nome like ? ||'%'", bancoConfig.getNome())
					.where("bancoConfiguracaoRetornoSegmento.tipo = ?", bancoConfig.getTipo())
					.where("bancoConfiguracaoRetornoSegmento.banco = ?", bancoConfig.getBanco())
					.where("bancoConfiguracaoRetornoSegmento.identificador = ?", bancoConfig.getIdentificador())
					.orderBy("bancoConfiguracaoRetornoSegmento.cdbancoconfiguracaoretornosegmento")
					.list();
	}

	@Override
	public ListagemResult<BancoConfiguracaoRetornoSegmento> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<BancoConfiguracaoRetornoSegmento> query = query();
		updateListagemQuery(query, filtro);
		
		return new ListagemResult<BancoConfiguracaoRetornoSegmento>(query, filtro);
	}
}
