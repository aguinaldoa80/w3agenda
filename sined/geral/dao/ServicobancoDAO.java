package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Servicobanco;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.ServicobancoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ServicobancoDAO extends GenericDAO<Servicobanco>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Servicobanco> query, FiltroListagem _filtro) {
		ServicobancoFiltro filtro = (ServicobancoFiltro)_filtro;
		query.leftOuterJoinFetch("servicobanco.servicoservidor servicoservidor")
			 .leftOuterJoinFetch("servicoservidor.servidor servidor")
		 	 .leftOuterJoinFetch("servicobanco.contratomaterial contratomaterial")
		 	 .leftOuterJoinFetch("contratomaterial.contrato contrato")
		 	 .where("servicoservidor.servidor = ?", filtro.getServidor())
		 	 .where("servicobanco.servicoservidor = ?", filtro.getServicoservidor())
		 	 .whereLikeIgnoreAll("servicobanco.usuario", filtro.getUsuario())
		 	 .where("contrato.cliente = ?", filtro.getCliente())
			 .where("servicobanco.contratomaterial = ?", filtro.getContratomaterial())
			 .where("servicobanco.ativo = ?", filtro.getAtivo());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Servicobanco> query) {
		query.leftOuterJoinFetch("servicobanco.servicoservidor servicoservidor")
			 .leftOuterJoinFetch("servicoservidor.servidor servidor")
		 	 .leftOuterJoinFetch("servicobanco.contratomaterial contratomaterial")
		 	 .leftOuterJoinFetch("contratomaterial.contrato contrato");
	}

	public int countContratomaterial(Integer cdservicobanco, Contratomaterial contratomaterial) {
		
		QueryBuilder<Long> query = newQueryBuilder(Long.class);
		
		query
			.select("count(*)")
			.from(Servicobanco.class);
		if(cdservicobanco != null)
			query.where("servicobanco.cdservicobanco <> ?", cdservicobanco);
		if(contratomaterial != null)
			query.where("servicobanco.contratomaterial = ?", contratomaterial);
				
		return query
				.unique()
				.intValue();
		
	}
		
}
