package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Etapa;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("ordem")
public class EtapaDAO extends GenericDAO<Etapa>{
	
	public Etapa loadByCdetapa(Integer cdcodigo){
		return query()
				.where("etapa.cdetapa = ?",cdcodigo)
				.unique();
	}
	
	/*singleton*/
	
	public static EtapaDAO instance;

	public static EtapaDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(EtapaDAO.class);
		}
		return instance;
	}

}
