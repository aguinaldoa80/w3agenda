package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Producaoetapanomecampo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoetapanomecampoDAO extends GenericDAO<Producaoetapanomecampo> {

	public List<Producaoetapanomecampo> findForW3Producao(String whereIn){
		return query()
			.select("producaoetapanomecampo.cdproducaoetapanomecampo, producaoetapanomecampo.nome, " +
					"producaoetapanomecampo.tipo, producaoetapanomecampo.obrigatorio, " +
					"producaoetapanome.cdproducaoetapanome")
			.join("producaoetapanomecampo.producaoetapanome producaoetapanome")
			.whereIn("producaoetapanomecampo.cdproducaoetapanomecampo", whereIn)
			.list();
	}
	
}
