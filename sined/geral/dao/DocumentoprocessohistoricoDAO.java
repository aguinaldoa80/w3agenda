package br.com.linkcom.sined.geral.dao;


import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Documentoprocessohistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentoprocessohistoricoDAO extends GenericDAO<Documentoprocessohistorico>{	
	
	private ArquivoDAO arquivoDao;	
	
	public void setArquivoDao(ArquivoDAO arquivoDao) {
		this.arquivoDao = arquivoDao;
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		arquivoDao.saveFile(save.getEntity(), "arquivo");
		super.updateSaveOrUpdate(save);
	}

	/**
	 * 
	 * @param cdsHistorico
	 * @param cdsArquivo
	 */
	public void deleteByIds(final String cdsHistorico, final String cdsArquivo) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				getJdbcTemplate().execute("DELETE FROM DOCUMENTOPROCESSOHISTORICO WHERE CDDOCUMENTOPROCESSOHISTORICO IN ("+cdsHistorico+")");
				getJdbcTemplate().execute("DELETE FROM ARQUIVO WHERE CDARQUIVO IN ("+cdsArquivo+")");
				return null;
			}
		});		
	}

}
