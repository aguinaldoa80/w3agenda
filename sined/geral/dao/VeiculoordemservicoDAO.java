package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Ordemservicotipo;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculoordemservico;
import br.com.linkcom.sined.geral.bean.Veiculoordemservicoitem;
import br.com.linkcom.sined.geral.bean.Veiculouso;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocontroleveiculo;
import br.com.linkcom.sined.geral.service.VeiculoordemservicoitemService;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculoordemservicoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.VeiculoagendamentoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VeiculoordemservicoDAO extends GenericDAO<Veiculoordemservico>{
	
	private VeiculoordemservicoitemService veiculoordemservicoitemService;
	
	public void setVeiculoordemservicoitemService(
			VeiculoordemservicoitemService veiculoordemservicoitemService) {
		this.veiculoordemservicoitemService = veiculoordemservicoitemService;
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Veiculoordemservico> query, FiltroListagem _filtro) {
		VeiculoordemservicoFiltro filtro = (VeiculoordemservicoFiltro) _filtro;
		query
			.select("veiculo.placa,veiculo.cdveiculo, veiculo.prefixo, veiculo.colaborador, veiculoordemservico.cdveiculoordemservico," +
			"veiculoordemservico.dtprevista, veiculoordemservico.dtrealizada, ordemservicotipo.nome, ordemservicotipo.cdordemservicotipo")
			.leftOuterJoin("veiculoordemservico.veiculo veiculo")
			.leftOuterJoin("veiculoordemservico.ordemservicotipo ordemservicotipo")
			.where("ordemservicotipo.cdordemservicotipo=?", 1)
			.whereLikeIgnoreAll("veiculo.placa",filtro.getPlaca())
			.where("veiculoordemservico.dtrealizada >= ?",filtro.getDataInicio())
			.where("veiculoordemservico.dtrealizada <= ?",filtro.getDataFim())
			;
		super.updateListagemQuery(query, _filtro);
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Veiculoordemservico> query) {
		query
			.leftOuterJoinFetch("veiculoordemservico.ordemservicotipo ordemservicotipo")
			.leftOuterJoinFetch("veiculoordemservico.veiculo veiculo")
			.leftOuterJoinFetch("veiculo.patrimonioitem patrimonioitem")
			.leftOuterJoinFetch("patrimonioitem.bempatrimonio bempatrimonio")
			.leftOuterJoinFetch("veiculo.colaborador colaborador")
			.leftOuterJoinFetch("veiculoordemservico.listaVeiculoordemservicoitem listaVeiculoordemservicoitem")
			.leftOuterJoinFetch("listaVeiculoordemservicoitem.inspecaoitem inspecaoitem")
			.leftOuterJoinFetch("veiculoordemservico.veiculouso veiculouso")
			;
		
	}
	
	@Override
	public void saveOrUpdate(final Veiculoordemservico bean) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				
				if(bean.getCdveiculoordemservico() != null && bean.getListaVeiculoordemservicoitem() != null){
					String whereIn = "";
					for (Veiculoordemservicoitem item : bean.getListaVeiculoordemservicoitem()) {
						if(item != null && item.getCdveiculoordemservicoitem() != null){
							whereIn += item.getCdveiculoordemservicoitem() + ",";
						}
					}
					if(!whereIn.equals("")){
						whereIn = whereIn.substring(0, whereIn.length()-1);
						
						List<Integer> listaIDs = veiculoordemservicoitemService.getIdWhereNotIn(whereIn, bean);
						if(listaIDs != null && listaIDs.size() > 0){
							for (Integer integer : listaIDs) {
								callTriggerOrdemservicoitem(integer);
							}
						}
						
					}
				}
				
				
				VeiculoordemservicoDAO.super.saveOrUpdate(bean);
				return null;
			}
		});
	}
	
	public void callTriggerOrdemservicoitem(Integer cod) {
		getJdbcTemplate().execute("SELECT TRIGGER_VEICULOOSITEM("+cod+")");
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		save.useTransaction(false);
		save.saveOrUpdateManaged("listaVeiculoordemservicoitem");
	}

	/**
	 * <b>M�todo respons�vel em retornar uma data prevista a partir de uma ordem servico</b>
	 * @param ordemservico
	 * @author Biharck
	 * @return
	 */
	public Veiculoordemservico loadDtPrevista(Veiculoordemservico ordemservico){
		if(ordemservico == null || ordemservico.getCdveiculoordemservico() == null){
			throw new SinedException("O objeto n�o pode ser nulo");
		}
		return query()
				.select("veiculoordemservico.cdveiculoordemservico, veiculoordemservico.dtprevista, veiculoordemservico.dtrealizada")
				.where("veiculoordemservico = ?",ordemservico)
				.unique()
				;
	} 
	
//	public Ordemservico loadvisual(Ordemservico ordemservico){
//		if(ordemservico == null || ordemservico.getCdordemservico()==null){
//			throw new W3AutoException("O objeto n�o pode ser nulo");
//		}
//		return query()
//				.select("ordemservico.cdordemservico, ordemservico.tipoinspecao")
//				.where("ordemservico=?",ordemservico)
//				.unique()
//				;
//	} 
//	
//	/**
//	 * <b>Carrega um ordem servi�o completo</b>
//	
//	 * @author Jo�o Paulo Zica
//	 * @param ordemservico
//	 * @return
//	 */
//	public Veiculoordemservico carregaOrdemServicoCompleto(Veiculoordemservico ordemservico){
//		return query()
//			.leftOuterJoinFetch("veiculoordemservico.veiculo veiculo")
//			.leftOuterJoinFetch("veiculoordemservico.ordemservicotipo ordemservicotipo")
//			.leftOuterJoinFetch("veiculoordemservico.listaVeiculoordemservicoitem listaVeiculoordemservicoitem")
//			.unique();
//	}
	
	/**
	 * <b>Carrega um ordem servi�o com o ve�culo</b>
	 * @author Jo�o Paulo Zica
	 * @param ordemservico
	 * @return ordemservico
	 */
	public Veiculoordemservico loadWithVeiculo(Veiculoordemservico ordemservico){
		if (ordemservico == null || ordemservico.getCdveiculoordemservico() == null) {
			throw new SinedException("Par�metros incorretos. Ordemservico n�o pode ser null.");
		}
		return query()
			.select("veiculoordemservico.cdveiculoordemservico, veiculoordemservico.dtprevista, veiculoordemservico.dtrealizada, " +
					"veiculo.cdveiculo, veiculo.placa")
			.leftOuterJoin("veiculoordemservico.veiculo veiculo")
			.where("veiculoordemservico = ?",ordemservico)
			.unique();
	}
	
	/**
	 * <b>Atualiza a data realizada de uma ordem de servi�o</b>
	 * @author Jo�o Paulo Zica
	 * @param ordemservico
	 * @return
	 */
	public void saveOrdemServico(Integer cdordemservico, Date dtrealizada){
		if (cdordemservico == null) {
			throw new SinedException("Par�metros incorretos. Cdordemservico n�o pode ser null.");
		}
		if (dtrealizada == null) {
			throw new SinedException("Par�metros incorretos. Dtrealizada n�o pode ser null.");
		}
		getHibernateTemplate().bulkUpdate("update Veiculoordemservico ordemservico set ordemservico.dtrealizada = ? where ordemservico.cdveiculoordemservico = ?",
				new Object[]{dtrealizada, cdordemservico});
	}
	
	/**
	 * M�todo para buscar as ordens de sevi�os do tipo Agendamento 
	 * @author Ramon Brazil
	 * @param OrdemservicoFiltro
	 * @return List<Ordemservico>
	 */
	public List<Veiculoordemservico> carregaOrdemServico(VeiculoagendamentoFiltro filtro){
		return querySined()
				
		.select("veiculoordemservico.cdveiculoordemservico, veiculoordemservico.dtprevista, colaborador.cdpessoa, colaborador.nome," +
				"ordemservicotipo.cdordemservicotipo, ordemservicotipo.nome, veiculo.cdveiculo, veiculo.placa," +
				"veiculo.prefixo, veiculomodelo.cdveiculomodelo, veiculomodelo.nome")
		.leftOuterJoin("veiculoordemservico.ordemservicotipo ordemservicotipo")
		.leftOuterJoin("veiculoordemservico.veiculo veiculo")
		.leftOuterJoin("veiculo.colaborador colaborador")
		.leftOuterJoin("veiculo.veiculomodelo veiculomodelo")
		.where("veiculo = ?",filtro.getVeiculo())
		.where("colaborador = ?", filtro.getCooperado())
		.where("veiculoordemservico.dtprevista >=?",filtro.getDtagendamento())
		.where("veiculoordemservico.dtprevista <=?",filtro.getDtfim())
		.where("ordemservicotipo.cdordemservicotipo = 0")
		.orderBy("veiculoordemservico.dtprevista, colaborador.nome")
		.list();
	}
	
//	/**
//	 * <b>M�todo respons�vel em retonrar uma ordem de servico do tipo manuten��o a partir de uma inspe��o</b>
//	 * @param ordemservico
//	 * @author Biharck
//	 * @return Ordemservico
//	 */
//	public Ordemservico retornoManutencaoViaInspecao(Ordemservico ordemservico){
//		if(ordemservico==null || ordemservico.getCdordemservico()==null){
//			throw new W3AutoException("O objeto n�o pode ser nulo");
//		}
//		return query()
//			.select("ordemservico.cdordemservico, ordemservico.dtprevista, ordemservico.tiposervico,ordemservico.veiculo")
//			.where("ordemservico.dtprevista =?", ordemservico.getDtprevista())
//			.where("ordemservico.tiposervico = 2")
//			.where("ordemservico.veiculo =?", ordemservico.getVeiculo())
//			.unique()
//			;
//			
//	}
//	
	/**
	 * Encontra a ordem de servi�o do tipo manuten��o a partir de uma ordem de inspe��o.
	 * 
	 * @param ordemservico
	 * @author Pedro Gon�alves
	 * @return
	 */
	public Veiculoordemservico findOsManutencaoByOSInspecao(Veiculoordemservico ordemservico){
		
		if(ordemservico == null || ordemservico.getCdveiculoordemservico() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		
		return query()
				.select("distinct new br.com.linkcom.sined.geral.bean.Veiculoordemservico(osm.cdveiculoordemservico)")
				.from(Veiculoordemservico.class,"osm")
				.join("osm.listaVeiculoordemservicoitem osim")
				.join("osim.ordemservicoiteminspecao osii")
				.join("osii.ordemservico osi")
				.where("osi=?",ordemservico)
				.setUseTranslator(false)
				.unique();
	}
//	public Ordemservico mostrakm(Ordemservico ordemservico){
//		if(ordemservico==null || ordemservico.getCdordemservico()==null){
//			throw new W3AutoException("O objeto n�o pode ser nulo");
//		}
//		return query()
//			.select("ordemservico.km")
//			.where("ordemservico.cdordemservico=?", ordemservico.getCdordemservico())
//			.unique()
//			;
//	} 
	/**
	 * M�todo para localizar agendamentos de inspe��o num intervalo de data.
	 * @param dtReferencia
	 * @param dtReferenciaLimite
	 * @param veiculo
	 * @author Fernando Boldrini
	 * @return
	 */
	public List<Veiculoordemservico> listIntervaloAgendamento(
			Date dtReferencia, Date dtReferenciaLimite, Veiculo veiculo) {
		if(dtReferencia == null || dtReferenciaLimite == null || veiculo == null)
			throw new SinedException("Par�metros inv�lidos.");
		return query()
		.select("veiculoordemservico.cdveiculoordemservico, veiculoordemservico.dtprevista, ordemservicotipo.cdordemservicotipo")
		.from(Veiculoordemservico.class)
		.join("veiculoordemservico.ordemservicotipo ordemservicotipo")
		.join("veiculoordemservico.veiculo veiculo")
		.where("veiculo = ?", veiculo)
		.where("veiculoordemservico.dtprevista >= ?", dtReferencia)
		.where("veiculoordemservico.dtprevista <= ?", dtReferenciaLimite)
		.orderBy("ordemservicotipo.cdordemservicotipo")
		.list();
	}
	/**
	 * Busca ordem de servi�o que conflite com o intervalo de uso do ve�culo.
	 * @param veiculouso
	 * @return boolean
	 * @author Fernando Fernando
	 */

	public boolean existeOrdemServicoMesmaData(Veiculouso veiculouso) {
		return newQueryBuilder(Long.class)
		.select("count(*)")
		.setUseTranslator(false)
		.from(beanClass)
		.where("veiculo = ?", veiculouso.getVeiculo())			
		.where("veiculoordemservico.dtprevista >= ?", veiculouso.getDtiniciouso())
		.where("veiculoordemservico.dtprevista <= ?", veiculouso.getDtfimuso())
		.where("veiculoordemservico.dtrealizada >= ?", veiculouso.getDtiniciouso())
		.where("veiculoordemservico.dtrealizada <= ?", veiculouso.getDtfimuso())
		.unique() > 0;
	}

	/**
	 * M�todo que verifica se h� alguma manuten��o que ainda n�o foi realizada
	 * 
	 * @param form
	 * @return
	 * @author Tom�s Rabelo 
	 */
	public boolean inspecaoPossuiManutencoesEmAberto(Veiculoordemservico form) {
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(beanClass)
			.join("veiculoordemservico.listaVeiculoordemservicoitem listaVeiculoordemservicoitem")
			.where("listaVeiculoordemservicoitem.status = ?", Boolean.FALSE)
			.where("veiculoordemservico = ?", form)
			.unique() > 0;
	}

	/**
	 * M�todo que verifca se h� alguma inspe��o existente para o ve�culo na data informada
	 * 
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo 
	 */
	public boolean existeInspecaoData(Veiculoordemservico bean) {
		return newQueryBuilder(Long.class)
		.select("count(*)")
		.setUseTranslator(false)
		.from(beanClass)
		.join("veiculoordemservico.veiculo veiculo")
		.join("veiculoordemservico.ordemservicotipo ordemservicotipo")
		.where("veiculo = ?", bean.getVeiculo())
		.where("ordemservicotipo = ?", Ordemservicotipo.INSPECAO)
		.where("veiculoordemservico.dtprevista = ?", bean.getDtrealizada())
		.where("veiculoordemservico <> ?", bean.getCdveiculoordemservico() != null ? bean : null)
		.unique() > 0;
	}

	/**
	 * M�todo que busca as inspe�oes do uso do veiculo
	 *
	 * @param veiculouso
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Veiculoordemservico> findByVeiculouso(Veiculouso veiculouso) {
		if(veiculouso == null || veiculouso.getCdveiculouso() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("veiculoordemservico.cdveiculoordemservico, veiculoordemservico.dtprevista, veiculoordemservico.km ")
			.join("veiculoordemservico.veiculouso veiculouso")
			.where("veiculouso = ?", veiculouso)
			.list();
	}

	/**
	 * M�todo que busca as inpe��es
	 *
	 * @param whereInInspecao
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Veiculoordemservico> findForInspecao(String whereInInspecao) {
		if(whereInInspecao == null || "".equals(whereInInspecao))
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("veiculoordemservico.cdveiculoordemservico, veiculo.cdveiculo ")
			.join("veiculoordemservico.veiculo veiculo")
			.whereIn("veiculoordemservico.cdveiculoordemservico", whereInInspecao)
			.list();
	}
	
	/**
	 * M�todo que busca a ultima ordem de servi�o de um ve�culo atrav�s do cdveiculo
	 *
	 * @param veiculo
	 * @return Veiculoordemservico
	 * @author Jo�o Vitor
	 */
	public Veiculoordemservico findByVeiculo(Veiculo veiculo){
		if(veiculo == null || veiculo.getCdveiculo() == null){
			throw new SinedException("Ve�culo n�o pode ser nulo.");
		}
	  return query()
	  .select("veiculoordemservico.km, veiculoordemservico.horimetro, veiculoordemservico.dtrealizada")
	  .join("veiculoordemservico.veiculo veiculo")
	  .openParentheses()
		  .where("veiculoordemservico.ordemservicotipo = ?", Ordemservicotipo.INSPECAO)
		  .where("veiculoordemservico.dtrealizada IS NOT NULL")
		  .openParentheses()
		  	.openParentheses()
		  		.where("veiculo.tipocontroleveiculo = ?", Tipocontroleveiculo.HODOMETRO)
		  		.where("veiculoordemservico.km IS NOT NULL")
		  	.closeParentheses()
		  	.or()
		  	.openParentheses()
		  		.where("veiculo.tipocontroleveiculo = ?", Tipocontroleveiculo.HORIMETRO)
		  		.where("veiculoordemservico.horimetro IS NOT NULL")
		  	.closeParentheses()
		  .closeParentheses()
		  .where("veiculoordemservico.dtrealizada = (" +
		  			"SELECT MAX(veiculoordemservico.dtrealizada) " +
		  			"FROM Veiculoordemservico veiculoordemservico " +
		  			"WHERE veiculoordemservico.veiculo = ? " +
		  			"AND veiculoordemservico.ordemservicotipo.cdordemservicotipo = " + Ordemservicotipo.INSPECAO.getCdordemservicotipo() +
		  		")", veiculo)
	  .closeParentheses()
	  .where("veiculo = ?", veiculo)
	  .unique();
	}
	
	/**
	 * M�todo que busca a ultima ordem de servi�o do tipo Manuten��o de um ve�culo atrav�s do cdveiculo e categoriaiteminspecao
	 *
	 * @param veiculo
	 * @return Veiculoordemservico
	 * @author Jo�o Vitor
	 */
	public List<Veiculoordemservico> findUltimaManutencaoByVeiculoAndItemInspecao(Veiculo veiculo, String whereIn){
		if(veiculo == null || veiculo.getCdveiculo() == null || whereIn == null || whereIn == ""){
			throw new SinedException("Nenhum dos par�metros podem ser nulos.");
		}
	  return query()
	  .select("veiculoordemservico.km, veiculoordemservico.horimetro, veiculoordemservico.dtrealizada, listaVeiculoordemservicoitem.status, listaVeiculoordemservicoitem.cdveiculoordemservicoitem, " +
	  		"inspecaoitem.cdinspecaoitem, inspecaoitem.ativo, inspecaoitem.nome")
	  .join("veiculoordemservico.listaVeiculoordemservicoitem listaVeiculoordemservicoitem")
	  .join("listaVeiculoordemservicoitem.inspecaoitem inspecaoitem")
	  .join("veiculoordemservico.veiculo veiculo")
	  .where("veiculoordemservico.ordemservicotipo = ?", Ordemservicotipo.MANUTENCAO)
	  .where("veiculoordemservico.dtrealizada IS NOT NULL")
	  .openParentheses()
	  	.openParentheses()
	  		.where("veiculo.tipocontroleveiculo = ?", Tipocontroleveiculo.HODOMETRO)
	  		.where("veiculoordemservico.km IS NOT NULL")
	  	.closeParentheses()
	  	.or()
	  	.openParentheses()
	  		.where("veiculo.tipocontroleveiculo = ?", Tipocontroleveiculo.HORIMETRO)
	  		.where("veiculoordemservico.horimetro IS NOT NULL")
	  	.closeParentheses()
	  .closeParentheses()
	  .where("veiculoordemservico.dtrealizada = (" +
	  			"SELECT MAX(veiculoOrdemServico2.dtrealizada) " +
	  			"FROM Veiculoordemservico veiculoOrdemServico2 " +
	  			"JOIN veiculoOrdemServico2.listaVeiculoordemservicoitem listaVeiculoordemservicoitem2 " +
	  			"WHERE listaVeiculoordemservicoitem2.inspecaoitem = inspecaoitem " + 
	  			"AND veiculoOrdemServico2.veiculo = ? " +
	  		")", veiculo)
	  
	  .whereIn("listaVeiculoordemservicoitem.inspecaoitem", whereIn)
	  .where("listaVeiculoordemservicoitem.ativo = ?" , Boolean.TRUE)
	  .where("listaVeiculoordemservicoitem.status = ?", Boolean.FALSE)
	  .where("veiculo = ?", veiculo)
	  .list();
	}
}
