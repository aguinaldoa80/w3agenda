package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialformulamargemcontribuicao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialformulamargemcontribuicaoDAO extends GenericDAO<Materialformulamargemcontribuicao>{

	/**
	* M�todo que busca as formulas de margem de contribui��o de acordo com o material
	* 
	* @param material
	* @return
	* @since 24/06/2015
	* @author Luiz Fernando
	*/
	public List<Materialformulamargemcontribuicao> findByMaterial(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Materia n�o pode ser nulo.");
			
		return query()
					.select("materialformulamargemcontribuicao.cdmaterialformulamargemcontribuicao, materialformulamargemcontribuicao.ordem, " +
							"materialformulamargemcontribuicao.identificador, materialformulamargemcontribuicao.formula")
					.where("materialformulamargemcontribuicao.material = ?", material)
					.orderBy("materialformulamargemcontribuicao.ordem, materialformulamargemcontribuicao.identificador")
					.list();
	}

	/**
	* M�todo que deleta a formula de margem de contribui��o do material
	*
	* @param material
	* @since 06/07/2015
	* @author Luiz Fernando
	*/
	public void deleteFormulamargemcontribuicaoByMaterial(Material material) {
		if(material != null && material.getCdmaterial() != null){
			getJdbcTemplate().execute("delete from materialformulamargemcontribuicao where cdmaterial = " + material.getCdmaterial());
		}
	}
}
