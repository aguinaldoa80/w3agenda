package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.ProducaoagendaFiltroMostrar;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoagendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.modulo.producao.controller.crud.filter.ProducaoagendaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoagendaDAO extends GenericDAO<Producaoagenda> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Producaoagenda> query, FiltroListagem _filtro) {
		ProducaoagendaFiltro filtro = (ProducaoagendaFiltro)_filtro;
		
		query
			.select("distinct producaoagenda.cdproducaoagenda, cliente.cdpessoa, cliente.nome, contrato.cdcontrato, contrato.descricao, " +
					"contratotipo.nome, producaoagenda.dtentrega, producaoagenda.producaoagendasituacao, pedidovenda.cdpedidovenda, " +
					"pedidovenda.dtpedidovenda, aux_contrato.situacao, pedidovenda.identificacaoexterna")
			.leftOuterJoin("producaoagenda.contrato contrato")
			.leftOuterJoin("contrato.aux_contrato aux_contrato")
			.leftOuterJoin("contrato.contratotipo contratotipo")
			.leftOuterJoin("producaoagenda.pedidovenda pedidovenda")
			.leftOuterJoin("producaoagenda.cliente cliente")
			.leftOuterJoin("producaoagenda.listaProducaoagendamaterial listaProducaoagendamaterial")
			.leftOuterJoin("listaProducaoagendamaterial.material material")
			.where("cliente = ?", filtro.getCliente())
			.where("contrato = ?", filtro.getContrato())
			.where("producaoagenda.empresa = ?", filtro.getEmpresa())
			.where("producaoagenda.projeto = ?", filtro.getProjeto())
			.where("pedidovenda.cdpedidovenda = ?", filtro.getPedidovenda())
			.where("producaoagenda.dtentrega >= ?", filtro.getDtentrega1())
			.where("producaoagenda.dtentrega <= ?", filtro.getDtentrega2())
			.where("pedidovenda.identificacaoexterna = ?", filtro.getIdentificacaoexterna())
			.where("material = ?", filtro.getMaterial())
			.orderBy("producaoagenda.cdproducaoagenda desc")
			.ignoreJoin("listaProducaoagendamaterial", "material")
			;
		
		ProducaoagendaFiltroMostrar producaoagendaFiltroMostrar = filtro.getProducaoagendaFiltroMostrar();
		if(producaoagendaFiltroMostrar != null){
			if(producaoagendaFiltroMostrar.equals(ProducaoagendaFiltroMostrar.PENDENCIA_FINANCEIRA)){
				List<Documentoacao> listaDocumentoacaoNaoPermitido = new ArrayList<Documentoacao>();
				listaDocumentoacaoNaoPermitido.add(Documentoacao.CANCELADA);
				listaDocumentoacaoNaoPermitido.add(Documentoacao.BAIXADA);
				listaDocumentoacaoNaoPermitido.add(Documentoacao.NEGOCIADA);
				
				query
					.openParentheses()
						.where("cliente.cdpessoa is not null")
						.where("exists(" +
								"select d.cddocumento " +
								"from Documento d " +
								"join d.pessoa p " +
								"join d.documentoacao dacao " +
								"join d.documentoclasse dclasse " +
								"where p.cdpessoa = cliente.cdpessoa " +
								"and dacao.cddocumentoacao not in (" + CollectionsUtil.listAndConcatenate(listaDocumentoacaoNaoPermitido, "cddocumentoacao", ",") + ") " +
								"and dclasse.cddocumentoclasse = " + Documentoclasse.CD_RECEBER + " " +
								"and current_date > d.dtvencimento" +
								")")
					.closeParentheses();
				
			} else if(producaoagendaFiltroMostrar.equals(ProducaoagendaFiltroMostrar.COMPRA_SOLICITADA)){
				query
					.where("exists(" +
							"select solOri.cdsolicitacaocompraorigem " +
							"from Solicitacaocompraorigem solOri " +
							"join solOri.producaoagenda prod " +
							"where prod.cdproducaoagenda = producaoagenda.cdproducaoagenda " +
						")");
			} else if(producaoagendaFiltroMostrar.equals(ProducaoagendaFiltroMostrar.EXPEDICAO_PENDENTE)){
				// TODO 
			} else if(producaoagendaFiltroMostrar.equals(ProducaoagendaFiltroMostrar.CONTRATO_CANCELADO)){
				query
					.where("producaoagenda.producaoagendasituacao <> ?", Producaoagendasituacao.CANCELADA)
					.where("aux_contrato.situacao = ?", SituacaoContrato.CANCELADO);
			}
		}
		
		if(filtro.getListaProducaoagendasituacao() != null && filtro.getListaProducaoagendasituacao().size() > 0){
			query.whereIn("producaoagenda.producaoagendasituacao", Producaoagendasituacao.listAndConcatenate(filtro.getListaProducaoagendasituacao()));
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Producaoagenda> query) {
		query.select("pedidovenda.cdpedidovenda, pedidovenda.origemOtr, pedidovenda.dtpedidovenda, " +
					"contrato.cdcontrato, contrato.identificador, contratotipo.cdcontratotipo, contratotipo.nome, " +
					"cemobra.cdcemobra, empresa.cdpessoa, venda.cdvenda, venda.dtvenda, " +
					"producaoagendatipo.cdproducaoagendatipo, producaoagendatipo.nome, " +
					"producaoetapa.cdproducaoetapa, cliente.cdpessoa, projeto.cdprojeto, localbaixamateriaprima.cdlocalarmazenagem, localbaixamateriaprima.nome, " +
					"producaoagenda.dtentrega, producaoagenda.cdproducaoagenda, producaoagenda.producaoagendasituacao, " +
					"material.cdmaterial, material.nome, unidademedida.cdunidademedida, unidademedida.nome, " +
					"pedidovendamaterial.cdpedidovendamaterial, " +
					"listaProducaoagendamaterial.altura, listaProducaoagendamaterial.largura, listaProducaoagendamaterial.comprimento, "+
					"listaProducaoagendamaterial.cdproducaoagendamaterial, listaProducaoagendamaterial.observacao, " +
					"listaProducaoagendamaterial.qtde, producaoetapaItem.cdproducaoetapa, producaoetapaItem.baixarEstoqueMateriaprima, " +
					"servicogarantido.cdmaterial, servicogarantido.nome, " +
					"pedidovendamaterialgarantido.cdpedidovendamaterial ")
				.leftOuterJoin("producaoagenda.pedidovenda pedidovenda")
				.leftOuterJoin("producaoagenda.cemobra cemobra")
				.leftOuterJoin("producaoagenda.empresa empresa")
				.leftOuterJoin("producaoagenda.cliente cliente")
				.leftOuterJoin("producaoagenda.projeto projeto")
				.leftOuterJoin("producaoagenda.venda venda")
				.leftOuterJoin("producaoagenda.contrato contrato")
				.leftOuterJoin("producaoagenda.localbaixamateriaprima localbaixamateriaprima")
				.leftOuterJoin("contrato.contratotipo contratotipo")
				.leftOuterJoin("producaoagenda.listaProducaoagendamaterial listaProducaoagendamaterial")
				.leftOuterJoin("listaProducaoagendamaterial.producaoetapa producaoetapaItem")
				.leftOuterJoin("listaProducaoagendamaterial.material material")
				.leftOuterJoin("listaProducaoagendamaterial.pneu pneu")
				.leftOuterJoin("producaoagenda.producaoagendatipo producaoagendatipo")
				.leftOuterJoin("producaoagendatipo.producaoetapa producaoetapa")
				.leftOuterJoin("listaProducaoagendamaterial.unidademedida unidademedida")
				.leftOuterJoin("listaProducaoagendamaterial.pedidovendamaterial pedidovendamaterial")
				.leftOuterJoin("pedidovendamaterial.pedidovendamaterialgarantido pedidovendamaterialgarantido")
				.leftOuterJoin("pedidovendamaterialgarantido.material servicogarantido");
		query = SinedUtil.setJoinsByComponentePneu(query);
	}
	
//	@Override
//	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
//		save.saveOrUpdateManaged("listaProducaoagendamaterial");
//	}

	/**
	 * M�todo que busca os registro para a gera��o do CSV.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Producaoagenda> findForCsv(ProducaoagendaFiltro filtro) {
		QueryBuilder<Producaoagenda> query = querySined();
		this.updateListagemQuery(query, filtro);
		query.orderBy("producaoagenda.cdproducaoagenda");
		return query.list();
	}

	/**
	 * M�todo que busca a lista de Producaoagenda com a situa��o preenchida.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Producaoagenda> findWithSituacao(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("producaoagenda.cdproducaoagenda, producaoagenda.producaoagendasituacao, cemobra.cdcemobra")
					.leftOuterJoin("producaoagenda.cemobra cemobra")
					.whereIn("producaoagenda.cdproducaoagenda", whereIn)
					.list();
	}


	/**
	 * M�todo que busca a lista de Producaoagenda com a etapa e situa��o preenchida.
	 *
	 * @param whereIn
	 * @return
	 * @author Jo�o Vitor	
	 * @since 14/04/2015
	 */
	public List<Producaoagenda> findWithProducaoetapaAndSituacao(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("producaoagenda.cdproducaoagenda, producaoagenda.producaoagendasituacao, producaoagendatipo.cdproducaoagendatipo, producaoetapa.cdproducaoetapa, " +
							"producaoetapaagendaproducaomaterial.cdproducaoetapa, producaoetapamaterial.cdproducaoetapa, material.cdmaterial, listaProducaoagendamaterial.cdproducaoagendamaterial, " +
							"producaoetapaagendaproducaomaterial.baixarEstoqueMateriaprima ")
					.leftOuterJoin("producaoagenda.producaoagendatipo producaoagendatipo")
					.leftOuterJoin("producaoagendatipo.producaoetapa producaoetapa")
					.leftOuterJoin("producaoagenda.listaProducaoagendamaterial listaProducaoagendamaterial")
					.leftOuterJoin("listaProducaoagendamaterial.material material")
					.leftOuterJoin("material.producaoetapa producaoetapamaterial")
					.leftOuterJoin("listaProducaoagendamaterial.producaoetapa producaoetapaagendaproducaomaterial")
					.whereIn("producaoagenda.cdproducaoagenda", whereIn)
					.list();
	}
	
	
	/**
	 * M�todo que busca as Producaoagenda para verifica��o de compra solicitada.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Producaoagenda> findForVerificacaoSolicitacaocompra(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("producaoagenda.cdproducaoagenda, producaoagenda.producaoagendasituacao")
					.whereIn("producaoagenda.cdproducaoagenda", whereIn)
					.where("exists(" +
								"select solOri.cdsolicitacaocompraorigem " +
								"from Solicitacaocompraorigem solOri " +
								"join solOri.producaoagenda prod " +
								"where prod.cdproducaoagenda = producaoagenda.cdproducaoagenda " +
							")")
					.list();
	}

	/**
	 * M�todo que busca as Producaoagenda para a a��o de produzir.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/01/2013
	 */
	public List<Producaoagenda> findForProducao(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		QueryBuilder<Producaoagenda> query = query()
					.select("producaoagenda.cdproducaoagenda, producaoagenda.producaoagendasituacao, producaoagenda.dtentrega, " +
							"producaoagendamaterial.cdproducaoagendamaterial, producaoagendamaterial.qtde, " +
							"material.cdmaterial, material.nome, unidademedidaPA.cdunidademedida, unidademedidaPA.nome, " +
							"producaoetapa.cdproducaoetapa, producaoetapa.nome, producaoetapa.baixarmateriaprimacascata, listaProducaoetapaitem.cdproducaoetapaitem, " +
							"producaoetapa.naoagruparmaterial, producaoetapa.naogerarentradaconclusao, producaoetapaItem.baixarEstoqueMateriaprima, " +
							"producaoetapaItem.naoagruparmaterial, producaoetapaItem.naogerarentradaconclusao, " +
							"producaoetapanome.cdproducaoetapanome, producaoetapanome.nome, listaProducaoetapaitem.ordem, departamento.cddepartamento, departamento.nome, " +
							"listaProducaoetapaitem.qtdediasprevisto, " +
							"producaoetapaItem.cdproducaoetapa,  producaoetapaItem.baixarmateriaprimacascata, producaoetapanomeItem.cdproducaoetapanome, producaoetapanomeItem.nome, listaProducaoetapaitemItem.cdproducaoetapaitem, " +
							"listaProducaoetapaitemItem.ordem, departamentoItem.cddepartamento, departamentoItem.nome, " +
							"listaProducaoetapaitemItem.qtdediasprevisto," +
							"pedidovenda.cdpedidovenda, localarmazenagemPV.cdlocalarmazenagem, localarmazenagemPV.nome, " +
							"venda.cdvenda, localarmazenagemV.cdlocalarmazenagem, localarmazenagemV.nome, producaoagendamaterial.observacao," +
							"pedidovendamaterial.cdpedidovendamaterial, cemobra.cdcemobra, empresa.cdpessoa, " +
							"producaoagendamaterialmateriaprima.cdproducaoagendamaterialmateriaprima, producaoagendamaterialmateriaprima.qtdeprevista, " +
							"producaoagendamaterialmateriaprimaMaterial.cdmaterial, producaoagendamaterialmateriaprimaMaterial.nome, " +
							"producaoagendamaterialmateriaprima.fracaounidademedida, producaoagendamaterialmateriaprima.qtdereferenciaunidademedida, " +
							"loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, " +
							"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.casasdecimaisestoque, " +
							"localbaixamateriaprima.cdlocalarmazenagem, localbaixamateriaprima.nome," +
							"cliente.cdpessoa,cliente.nome," +
							"pedidovenda.cdpedidovenda,pedidovenda.identificacaoexterna")
					.leftOuterJoin("producaoagenda.empresa empresa")
					.leftOuterJoin("producaoagenda.cliente cliente")
					.leftOuterJoin("producaoagenda.localbaixamateriaprima localbaixamateriaprima")
					.leftOuterJoin("producaoagenda.listaProducaoagendamaterial producaoagendamaterial")
					.leftOuterJoin("producaoagendamaterial.material material")
					.leftOuterJoin("producaoagendamaterial.unidademedida unidademedidaPA")
					.leftOuterJoin("producaoagendamaterial.pneu pneu")
					.leftOuterJoin("producaoagendamaterial.pedidovendamaterial pedidovendamaterial")
					.leftOuterJoin("material.producaoetapa producaoetapa")
					.leftOuterJoin("producaoetapa.listaProducaoetapaitem listaProducaoetapaitem")
					.leftOuterJoin("listaProducaoetapaitem.producaoetapanome producaoetapanome")
					.leftOuterJoin("listaProducaoetapaitem.departamento departamento")
					.leftOuterJoin("producaoagendamaterial.producaoetapa producaoetapaItem")
					.leftOuterJoin("producaoagendamaterial.listaProducaoagendamaterialmateriaprima producaoagendamaterialmateriaprima")
					.leftOuterJoin("producaoagendamaterialmateriaprima.loteestoque loteestoque")
					.leftOuterJoin("producaoagendamaterialmateriaprima.material producaoagendamaterialmateriaprimaMaterial")
					.leftOuterJoin("producaoagendamaterialmateriaprima.unidademedida unidademedida")
					.leftOuterJoin("producaoetapaItem.listaProducaoetapaitem listaProducaoetapaitemItem")
					.leftOuterJoin("listaProducaoetapaitemItem.producaoetapanome producaoetapanomeItem")
					.leftOuterJoin("listaProducaoetapaitemItem.departamento departamentoItem")
					.leftOuterJoin("producaoagenda.cemobra cemobra")
					.leftOuterJoin("producaoagenda.venda venda")
					.leftOuterJoin("venda.localarmazenagem localarmazenagemV")
					.leftOuterJoin("producaoagenda.pedidovenda pedidovenda")
					.leftOuterJoin("pedidovenda.localarmazenagem localarmazenagemPV")
					.whereIn("producaoagenda.cdproducaoagenda", whereIn)
					.orderBy("listaProducaoetapaitemItem.ordem, listaProducaoetapaitem.ordem");
		query = SinedUtil.setJoinsByComponentePneu(query);
		return query.list();
	}

	/**
	 * Atualiza a situa��o de uma agenda de produ��o.
	 *
	 * @param producaoagenda
	 * @param producaoagendasituacao
	 * @author Rodrigo Freitas
	 * @since 11/01/2013
	 */
	public void updateSituacao(Producaoagenda producaoagenda, Producaoagendasituacao producaoagendasituacao) {
		getHibernateTemplate().bulkUpdate("update Producaoagenda p set p.producaoagendasituacao = ? where p.id = ?", new Object[]{
				producaoagendasituacao, producaoagenda.getCdproducaoagenda()
		});
	}
	
	/**
	 * Atualiza a situa��o de v�rias agendas de produ��o.
	 *
	 * @param whereIn
	 * @param producaoagendasituacao
	 * @author Rodrigo Freitas
	 * @since 29/07/2014
	 */
	public void updateSituacao(String whereIn, Producaoagendasituacao producaoagendasituacao) {
		getHibernateTemplate().bulkUpdate("update Producaoagenda p set p.producaoagendasituacao = ? where p.cdproducaoagenda in (" + whereIn + ")", new Object[]{
				producaoagendasituacao
		});
	}

	/**
	 * Busca as agendas de produ��o com as ordens preenchidas.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/01/2013
	 */
	public List<Producaoagenda> findForConclusao(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metros");
		}
		return query()
					.select("producaoagenda.cdproducaoagenda, producaoordem.cdproducaoordem, producaoordem.producaoordemsituacao")
					.join("producaoagenda.listaProducaoordemmaterialorigem listaProducaoordemmaterialorigem")
					.join("listaProducaoordemmaterialorigem.producaoordemmaterial producaoordemmaterial")
					.join("producaoordemmaterial.producaoordem producaoordem")
					.whereIn("producaoagenda.cdproducaoagenda", whereIn)
					.list();
	}

	/**
	 * M�todo que carrega a produ��o com o tipo
	 *
	 * @param producaoagenda
	 * @return
	 * @author Luiz Fernando
	 * @since 08/01/2014
	 */
	public Producaoagenda loadWithoutProducaoagendatipo(Producaoagenda producaoagenda) {
		if(producaoagenda == null || producaoagenda.getCdproducaoagenda() == null)
			throw new SinedException("Agenda de produ��o n�o pode ser nula.");
		
		return query()
					.select("producaoagenda.cdproducaoagenda, producaoagendatipo.cdproducaoagendatipo")
					.leftOuterJoin("producaoagenda.producaoagendatipo producaoagendatipo")
					.where("producaoagenda = ?", producaoagenda)
					.unique();
	}
	
	/**
	 * Busca os agendamentos de produ��o pelo contrato que est�o em espera.
	 *
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/03/2013
	 */
	public List<Producaoagenda> findEmEsperaByContrato(Contrato contrato) {
		if(contrato == null || contrato.getCdcontrato() == null){
			throw new SinedException("Contrato n�o pode ser nulo.");
		}
		return query()
					.select("producaoagenda.cdproducaoagenda, producaoagenda.producaoagendasituacao")
					.where("producaoagenda.contrato = ?", contrato)
					.where("producaoagenda.producaoagendasituacao = ?", Producaoagendasituacao.EM_ESPERA)
					.list();
	}

	/**
	 * M�todo que busca a agenda de produ��o do pedido de venda
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 * @since 17/01/2014
	 */
	public List<Producaoagenda> findByPedidovenda(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null){
			throw new SinedException("Pedido de venda n�o pode ser nulo.");
		}
		return query()
					.select("producaoagenda.cdproducaoagenda, producaoagenda.producaoagendasituacao, pedidovenda.pedidovendasituacao," +
							"listaProducaoagendaitemadicional.cdproducaoagendaitemadicional, listaProducaoagendaitemadicional.quantidade, " +
							"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
							"material.cdmaterial, material.nome, material.identificacao, material.valorvenda, material.valorvendamaximo, material.valorvendaminimo, " +
							"material.vendapromocional, material.kitflexivel, material.valorcusto, material.peso, material.pesobruto, " +
							"pneu.cdpneu, pneuAdicional.cdpneu")
					.join("producaoagenda.pedidovenda pedidovenda")
					.leftOuterJoin("producaoagenda.listaProducaoagendamaterial producaoagendamaterial")
					.leftOuterJoin("producaoagendamaterial.pneu pneu")
					.leftOuterJoin("producaoagenda.listaProducaoagendaitemadicional listaProducaoagendaitemadicional")
					.leftOuterJoin("listaProducaoagendaitemadicional.material material")
					.leftOuterJoin("listaProducaoagendaitemadicional.pneu pneuAdicional")
					.leftOuterJoin("material.unidademedida unidademedida")
					.where("pedidovenda = ?", pedidovenda)
					.list();
	}
	
	/**
	 * M�todo que busca a agenda de produ��o do pedido de venda
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/07/2014
	 */
	public List<Producaoagenda> findByPedidovenda(String whereIn) {
		return query()
				.select("producaoagenda.cdproducaoagenda, producaoagenda.producaoagendasituacao, pedidovenda.pedidovendasituacao")
				.join("producaoagenda.pedidovenda pedidovenda")
				.whereIn("pedidovenda.cdpedidovenda", whereIn)
				.list();
	}
	
	/**
	 * M�todo que busca a agenda de produ��o da venda
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 * @since 17/01/2014
	 */
	public List<Producaoagenda> findByVenda(Venda venda) {
		if(venda == null || venda.getCdvenda() == null){
			throw new SinedException("Venda n�o pode ser nulo.");
		}
		return querySined()
					
					.select("producaoagenda.cdproducaoagenda, producaoagenda.producaoagendasituacao")
					.where("producaoagenda.venda = ?", venda)
					.list();
	}

	/**
	 * M�todo que verifica se o pedido de venda tem agenda de produ��o
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 * @since 09/04/2014
	 */
	public boolean existProducaoagendaByPedidovenda(Pedidovenda pedidovenda) {
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Producaoagenda.class)
			.join("producaoagenda.pedidovenda pedidovenda")
			.where("pedidovenda = ?", pedidovenda)
			.where("producaoagenda.producaoagendasituacao <> ?", Producaoagendasituacao.CANCELADA)
			.unique() > 0;
	}
	
	/**
	* M�todo que carrega a agenda de produ��o com os materiais
	*
	* @param whereIn
	* @return
	* @since 24/06/2014
	* @author Luiz Fernando
	 * @param whereInMaterialMestre 
	*/
	@SuppressWarnings("unchecked")
	public List<Producaoagenda> findForProduzirByProducaoagenda(String whereIn, String whereInMaterialMestre) {
		if(whereIn == null || whereIn.equals("") || whereInMaterialMestre == null || whereInMaterialMestre.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		
		StringBuilder sql = new StringBuilder();
		sql.append("select b.cdproducaoagenda as CDPRODUCAOAGENDA, mp.cdmaterialmestre AS CDMATERIALMESTRE, a.cdmaterial AS CDMATERIAL ");
		sql.append("from producaoagendamaterialitem a ");
		sql.append("join materialproducao mp on mp.cdmaterial = a.cdmaterial ");
		sql.append("join producaoagendamaterial b on b.cdmaterial = mp.cdmaterialmestre ");
		sql.append("where a.cdproducaoagenda in (" + whereIn + ") and b.cdproducaoagenda = (" + whereIn + ") ");
		sql.append("and mp.cdmaterialmestre in (" + whereInMaterialMestre + ") ");
		sql.append("order by mp.cdmaterialmestre ");

		SinedUtil.markAsReader();
		List<Producaoagenda> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Producaoagenda mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Producaoagenda(rs.getInt("CDPRODUCAOAGENDA"), rs.getInt("CDMATERIALMESTRE"), rs.getInt("CDMATERIAL"));
			}
		});
		
		return lista;
	}
	

	/**
	 * Verifica se existe a agenda de produ��o dos pedidos de venda
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/07/2014
	 */
	public boolean haveProducaoagendaByPedidovendaNaoCancelada(String whereIn) {
		return newQueryBuilderWithFrom(Long.class)
					.select("count(*)")
					.join("producaoagenda.pedidovenda pedidovenda")
					.whereIn("pedidovenda.cdpedidovenda", whereIn)
					.where("producaoagenda.producaoagendasituacao <> ?", Producaoagendasituacao.CANCELADA)
					.unique() > 0;
	}

	/**
	 * Verifica se existe alguma agenda de produ��o nas situa��es passadas via par�metro.
	 *
	 * @param whereIn
	 * @param producaoagendasituacao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/07/2014
	 */
	public boolean haveProducaoagendaSituacao(String whereIn, Producaoagendasituacao[] producaoagendasituacao) {
		QueryBuilder<Long> query = newQueryBuilderWithFrom(Long.class)
			.select("count(*)")
			.whereIn("producaoagenda.cdproducaoagenda", whereIn);
		
		if(producaoagendasituacao != null && producaoagendasituacao.length > 0){
			query.openParentheses();
			for (int i = 0; i < producaoagendasituacao.length; i++) {
				query.where("producaoagenda.producaoagendasituacao = ?", producaoagendasituacao[i]).or();
			}
			query.closeParentheses();
		}
		
		return query.unique() > 0;
	}
	
	/**
	 * Verifica se existe alguma agenda de produ��o que n�o est� nas situa��es passadas via par�metro.
	 *
	 * @param whereIn
	 * @param producaoagendasituacao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/03/2016
	 */
	public boolean haveProducaoagendaNotInSituacao(String whereIn, Producaoagendasituacao[] producaoagendasituacao) {
		QueryBuilder<Long> query = newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.whereIn("producaoagenda.cdproducaoagenda", whereIn);
		
		if(producaoagendasituacao != null && producaoagendasituacao.length > 0){
			for (int i = 0; i < producaoagendasituacao.length; i++) {
				query.where("producaoagenda.producaoagendasituacao <> ?", producaoagendasituacao[i]);
			}
		}
		
		return query.unique() > 0;
	}
	
	/**
	* M�todo que verifica se existe agenda de produ��o diferente da situa��o passada por par�metro
	*
	* @param whereIn
	* @param producaoagendasituacao
	* @return
	* @since 03/12/2014
	* @author Luiz Fernando
	*/
	public boolean notProducaoagendaSituacao(String whereIn, Producaoagendasituacao producaoagendasituacao) {
		return newQueryBuilderWithFrom(Long.class)
			.select("count(*)")
			.whereIn("producaoagenda.cdproducaoagenda", whereIn)
			.where("producaoagenda.producaoagendasituacao <> ?", producaoagendasituacao)
			.unique() > 0;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see 
	*
	* @param whereIn
	* @return
	* @since 03/12/2014
	* @author Luiz Fernando
	*/
	public List<Producaoagenda> findForCalcularSobra(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("producaoagenda.cdproducaoagenda, producaoagenda.producaoagendasituacao, " +
						"listaProducaoagendamaterial.cdproducaoagendamaterial, listaProducaoagendamaterial.largura, " +
						"listaProducaoagendamaterial.altura, listaProducaoagendamaterial.comprimento, " +
						"listaProducaoagendamaterial.qtde, " +
						"material.cdmaterial, producaoetapa.cdproducaoetapa, producaoetapa.corteproducao," +
						"listaProducaoetapaitem.cdproducaoetapaitem, listaProducaoetapaitem.ordem ")
				.leftOuterJoin("producaoagenda.listaProducaoagendamaterial listaProducaoagendamaterial")
				.leftOuterJoin("listaProducaoagendamaterial.material material")
				.leftOuterJoin("listaProducaoagendamaterial.producaoetapa producaoetapa")
				.leftOuterJoin("producaoetapa.listaProducaoetapaitem listaProducaoetapaitem")
				.where("producaoagenda.producaoagendasituacao = ?", Producaoagendasituacao.EM_ANDAMENTO)
				.whereIn("producaoagenda.cdproducaoagenda", whereIn)
				.list();
	}
	
	/**
	* M�todo que carrega a agenda de produ��o com as dimensoes dos itens
	*
	* @param whereIn
	* @return
	* @since 02/04/2015
	* @author Luiz Fernando
	*/
	public List<Producaoagenda> findForMovimentacaoestoque(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("producaoagenda.cdproducaoagenda,  " +
						"listaProducaoagendamaterial.cdproducaoagendamaterial, listaProducaoagendamaterial.largura, " +
						"listaProducaoagendamaterial.altura, listaProducaoagendamaterial.comprimento, " +
						"listaProducaoagendamaterial.qtde, " +
						"material.cdmaterial, producaoetapa.cdproducaoetapa, producaoetapa.corteproducao ")
				.leftOuterJoin("producaoagenda.listaProducaoagendamaterial listaProducaoagendamaterial")
				.leftOuterJoin("listaProducaoagendamaterial.material material")
				.leftOuterJoin("listaProducaoagendamaterial.producaoetapa producaoetapa")
				.whereIn("producaoagenda.cdproducaoagenda", whereIn)
				.list();
	}
	
	/**
	 * M�todo que carrega os dados para listagem e csv de an�lise de custo
	 * @param empresa
	 * @param dtinicio
	 * @param dtfim
	 * @author Danilo Guimar�es
	 * @since 22/05/2015
	 */
	public List<Producaoagenda> findForAnaliseCusto(Empresa empresa, Date dtinicio, Date dtfim){
		return query()
				.select("producaoagenda.cdproducaoagenda, material.cdmaterial, material.nome, material.identificacao, material.valorcustomateriaprima, " +
						"material.valorvenda, material.valorcusto, materialgrupo.cdmaterialgrupo, materialgrupo.nome, materialgrupo.rateiocustoproducao, " +
						"listaProducaoagendamaterial.cdproducaoagendamaterial, listaProducaoagendamaterial.qtde, venda.cdvenda, " +
						"materialbandavenda.cdmaterial, materialbandavenda.nome, materialbandapedidovenda.cdmaterial, materialbandapedidovenda.nome, " +
						"materialvenda.cdmaterial, materialvenda.nome, materialpedidovenda.cdmaterial, materialpedidovenda.nome")
				.leftOuterJoin("producaoagenda.listaProducaoagendamaterial listaProducaoagendamaterial")
				.leftOuterJoin("listaProducaoagendamaterial.material material")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("producaoagenda.venda venda")
				.leftOuterJoin("producaoagenda.pedidovenda pedidovenda")
				.leftOuterJoin("venda.listavendamaterial listavendamaterial")
				.leftOuterJoin("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
				.leftOuterJoin("listavendamaterial.pneu pneuvenda")
				.leftOuterJoin("pneuvenda.materialbanda materialbandavenda")
				.leftOuterJoin("listavendamaterial.material materialvenda")
				.leftOuterJoin("listavendamaterial.pneu pneupedidovenda")
				.leftOuterJoin("pneupedidovenda.materialbanda materialbandapedidovenda")
				.leftOuterJoin("listaPedidovendamaterial.material materialpedidovenda")
				.where("producaoagenda.empresa = ?", empresa, empresa != null)
				.where("producaoagenda.dtaltera >= ?", dtinicio, dtinicio != null)
				.where("producaoagenda.dtaltera <= ?", dtfim, dtfim != null)
				.where("material.producao = ?", Boolean.TRUE)
				.where("producaoagenda.producaoagendasituacao <> ?", Producaoagendasituacao.CANCELADA)
				.list();
		
	}
	
	/**
	* M�todo que busca producaoagenda com a lista de Materia prima.
	*
	* @param whereIn
	* @return
	* @since 14/05/2015
	* @author Jo�o Vitor
	*/
	public List<Producaoagenda> findProducaoagendamateriaprimaByProducaoagenda(String whereIn){
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("producaoagenda.cdproducaoagenda, producaoagendamaterialmateriaprima.cdproducaoagendamaterialmateriaprima, producaoagendamaterial.qtde, " +
						"producaoagendamaterialmateriaprima.qtdeprevista, producaoagendamaterialmateriaprima.naocalcularqtdeprevista, " +
						"producaoagendamaterial.cdproducaoagendamaterial, producaoagendamaterialmateriaprima.agitacao, producaoagendamaterialmateriaprima.quantidadepercentual, " +
						"producaoagendamaterialmateriaprima.fracaounidademedida, producaoagendamaterialmateriaprima.qtdereferenciaunidademedida, " +
						"material.cdmaterial, material.nome, materialproducaoagenda.cdmaterial, materialproducaoagenda.nome, " +
						"loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, " +
						"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.casasdecimaisestoque, " +
						"empresa.cdpessoa, empresa.nome")
				.join("producaoagenda.listaProducaoagendamaterial producaoagendamaterial")
				.leftOuterJoin("producaoagendamaterial.material materialproducaoagenda")
				.leftOuterJoin("producaoagendamaterial.listaProducaoagendamaterialmateriaprima producaoagendamaterialmateriaprima")
				.leftOuterJoin("producaoagendamaterialmateriaprima.material material")
				.leftOuterJoin("producaoagendamaterialmateriaprima.loteestoque loteestoque")
				.leftOuterJoin("producaoagendamaterialmateriaprima.unidademedida unidademedida")
				.leftOuterJoin("producaoagenda.empresa empresa")
				.whereIn("producaoagenda.cdproducaoagenda", whereIn)
				.list();
	}

	/**
	 * Busca as agendas de produ��o vinculadas ao pneu passado por par�metro.
	 *
	 * @param pneu
	 * @param pedidoVenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 24/07/2015
	 */
	public List<Producaoagenda> findByPneu(Pneu pneu, Pedidovenda pedidoVenda) {
		return query()
					.select("producaoagenda.cdproducaoagenda, producaoagenda.producaoagendasituacao")
					.join("producaoagenda.listaProducaoagendamaterial producaoagendamaterial")
					.leftOuterJoin("producaoagenda.pedidovenda pedidovenda")
					.where("producaoagendamaterial.pneu = ?", pneu)
					.where("pedidovenda = ?", pedidoVenda)
					.list();
	}
	
	/**
	* M�todo que busca as agendas de produ��o de acordo com as ordens de produ��o
	*
	* @param whereInProducaoordem
	* @return
	* @since 16/10/2015
	* @author Luiz Fernando
	*/
	public List<Producaoagenda> findByProducaoordem(String whereInProducaoordem) {
		if(StringUtils.isBlank(whereInProducaoordem)){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
					.select("producaoagenda.cdproducaoagenda, producaoagenda.producaoagendasituacao, " +
							"pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao ")
					.join("producaoagenda.listaProducaoordemmaterialorigem listaProducaoordemmaterialorigem")
					.join("listaProducaoordemmaterialorigem.producaoordemmaterial producaoordemmaterial")
					.join("producaoordemmaterial.producaoordem producaoordem")
					.join("producaoagenda.pedidovenda pedidovenda")
					.whereIn("producaoordem.cdproducaoordem", whereInProducaoordem)
					.list();
	}

	/**
	* M�todo que verifica se o item � uma materia-prima de um item da agenda de produ��o
	*
	* @param material
	* @param producaoagenda
	* @return
	* @since 02/03/2016
	* @author Luiz Fernando
	*/
	public boolean isMateriaprima(Material material, Producaoagenda producaoagenda) {
		if(material == null || material.getCdmaterial() == null || producaoagenda == null || producaoagenda.getCdproducaoagenda() == null)
			throw new SinedException("Par�metro inv�lido.");
			
		String sql = 
			" SELECT COUNT(*) as QTDE_REGISTROS " +
			" FROM ( " +
			" SELECT COUNT(*) as QTDE_REGISTROS " +
			" FROM producaoagendamaterialmateriaprima pammp " +
			" JOIN producaoagendamaterial pam ON pam.cdproducaoagendamaterial = pammp.cdproducaoagendamaterial " +
			" where pammp.cdmaterial = " + material.getCdmaterial() + " and pam.cdproducaoagenda = " + producaoagenda.getCdproducaoagenda() + 
			" UNION ALL " +
			" SELECT COUNT(*) as QTDE_REGISTROS " +
			" FROM producaoagendamaterial pam " +
			" JOIN materialproducao mp on mp.cdmaterialmestre = pam.cdmaterial " +
			" where mp.cdmaterial = " + material.getCdmaterial() + " and pam.cdproducaoagenda = " + producaoagenda.getCdproducaoagenda() +
			" ) as query ";

		SinedUtil.markAsReader();
		Integer valor = (Integer)getJdbcTemplate().queryForObject(sql, 
				new RowMapper() {public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
						return new Integer(rs.getInt("QTDE_REGISTROS"));
					}
				});
		
		return valor != null && valor > 0;
	}

	/**
	* M�todo que busca a agenda de produ��o da primeira ordem de produ��o
	*
	* @param whereInProducaoordem
	* @return
	* @since 23/12/2015
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public List<Producaoagenda> findProducaoagendaForEstornoProducaoordem(String whereInProducaoordem) {
		if(StringUtils.isBlank(whereInProducaoordem)){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		String sql = " WITH RECURSIVE arvore_producaoordem (cdproducaoordem, cdproducaoordemanterior) AS " +
					" ( " +
					" SELECT po1.cdproducaoordem, po1.cdproducaoordemanterior " +
					" FROM producaoordem po1  " +
					" WHERE po1.cdproducaoordem =  " + whereInProducaoordem + 
					" UNION ALL " +
					" SELECT po2.cdproducaoordem, po2.cdproducaoordemanterior " +
					" FROM producaoordem po2 " +
					" INNER JOIN arvore_producaoordem arvore_po2 ON po2.cdproducaoordem = arvore_po2.cdproducaoordemanterior " +
					" ) " +
					" SELECT distinct pa.cdproducaoagenda " +
					" FROM arvore_producaoordem arvore_po " +
					" join producaoordemmaterial pom on pom.cdproducaoordem = arvore_po.cdproducaoordem " +
					" join producaoordemmaterialorigem pomo on pomo.cdproducaoordemmaterial = pom.cdproducaoordemmaterial " +
					" join producaoagenda pa on pa.cdproducaoagenda = pomo.cdproducaoagenda " +
					" where arvore_po.cdproducaoordemanterior is null and pa.producaoagendasituacao in (" + Producaoagendasituacao.CONCLUIDA.getValue() + ", " + Producaoagendasituacao.CANCELADA.getValue() + ")";

		SinedUtil.markAsReader();
		List<Producaoagenda> lista = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Producaoagenda(rs.getInt("cdproducaoagenda"));
			}
		});
		
		return lista;
	}

	/**
	 * Busca as agendas de produ��o para o envio para o ch�o de f�brica
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/03/2016
	 */
	public List<Producaoagenda> findForEnvioChaofabrica(String whereIn) {
		return query()
					.select("producaoagenda.cdproducaoagenda, producaoagendamaterial.cdproducaoagendamaterial, pneu.cdpneu, producaoetapa.cdproducaoetapa, " +
							"pedidovendamaterial.cdpedidovendamaterial")
					.leftOuterJoin("producaoagenda.listaProducaoagendamaterial producaoagendamaterial")
					.leftOuterJoin("producaoagendamaterial.pneu pneu")
					.leftOuterJoin("producaoagendamaterial.producaoetapa producaoetapa")
					.leftOuterJoin("producaoagendamaterial.pedidovendamaterial pedidovendamaterial")
					.whereIn("producaoagenda.cdproducaoagenda", whereIn)
					.list();
	}

	/**
	* M�todo que busca as agendas de produ��o para registrar o consumo de mat�ria-prima
	*
	* @param whereInProducaoordem
	* @return
	* @since 15/04/2016
	* @author Luiz Fernando
	*/
	public List<Producaoagenda> findForConsumoMateriaprima(String whereInProducaoordem) {
		if(StringUtils.isBlank(whereInProducaoordem)){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("producaoagenda.cdproducaoagenda, producaoagenda.producaoagendasituacao")
					.where("producaoagenda.cdproducaoagenda in (  select pa.cdproducaoagenda " +
																" from Producaoordemmaterial pom" +
																" join pom.listaProducaoordemmaterialorigem pomo " +
																" join pomo.producaoagenda pa" +
																" where pom.producaoordem.cdproducaoordem in ("+whereInProducaoordem+"))")
					.list();
	}
	
	public Producaoagenda loadProducaoagenda(Producaoagenda producaoagenda){
		if(producaoagenda == null || producaoagenda.getCdproducaoagenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("producaoagenda.cdproducaoagenda, empresa.cdpessoa, " +
						"localarmazenagemprodutofinal.cdlocalarmazenagem, localarmazenagemmateriaprima.cdlocalarmazenagem, " +
						"projeto.cdprojeto")
				.leftOuterJoin("producaoagenda.empresa empresa")
				.leftOuterJoin("empresa.localarmazenagemprodutofinal localarmazenagemprodutofinal")
				.leftOuterJoin("empresa.localarmazenagemmateriaprima localarmazenagemmateriaprima")
				.leftOuterJoin("producaoagenda.projeto projeto")
				.where("producaoagenda = ?", producaoagenda)
				.unique();
	}
}