package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Obslancamentofiscal;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ObslancamentofiscalFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ObslancamentofiscalDAO extends GenericDAO<Obslancamentofiscal> {

	@Override
	public void updateListagemQuery(QueryBuilder<Obslancamentofiscal> query, FiltroListagem _filtro) {
		
		ObslancamentofiscalFiltro filtro = (ObslancamentofiscalFiltro) _filtro;	
		
		query
			.select("obslancamentofiscal.cdobslancamentofiscal, obslancamentofiscal.descricao")
			.where("obslancamentofiscal.cdobslancamentofiscal = ?", filtro.getCodigo())
			.whereLikeIgnoreAll("obslancamentofiscal.descricao", filtro.getDescricao())
			.orderBy("obslancamentofiscal.cdobslancamentofiscal");
		
	}

	/**
	* M�todo que verifica se existe descri��o j� cadastrada
	*
	* @param obslancamentofiscal
	* @return
	* @since 06/01/2015
	* @author Luiz Fernando
	*/
	public boolean existeDescricao(Obslancamentofiscal obslancamentofiscal) {
		return newQueryBuilderSined(Long.class)
			.from(Obslancamentofiscal.class)
			.setUseTranslator(false) 
			.select("count(*)")
			.where("obslancamentofiscal <> ?", obslancamentofiscal, obslancamentofiscal.getCdobslancamentofiscal() != null)
			.where("retira_acento(upper(obslancamentofiscal.descricao)) = retira_acento(upper(?))", 
								obslancamentofiscal.getDescricao() != null ? obslancamentofiscal.getDescricao() : "")
			.unique()
			.longValue() > 0;
	}
		
}
