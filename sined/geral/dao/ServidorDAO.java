package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Ambienterede;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.bean.Servidor;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.ServidorFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("nome")
public class ServidorDAO extends GenericDAO<Servidor>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Servidor> query, FiltroListagem _filtro) {
		ServidorFiltro filtro = (ServidorFiltro)_filtro;
		query.leftOuterJoinFetch("servidor.ambienterede ambienterede")
		 	 .leftOuterJoinFetch("servidor.plataforma plataforma")
		 	 .leftOuterJoinFetch("servidor.tiponas tiponas")
		 	 .where("servidor.ambienterede = ?", filtro.getAmbienterede())
		 	 .where("servidor.plataforma = ?", filtro.getPlataforma());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Servidor> query) {
		query.select("servidor.cdservidor, servidor.nome, servidor.ip, servidor.porta, servidor.usuario, servidor.senha, servidor.segredonas, servidor.comunidadenas, servidor.portanas, " +
				"servidor.host, servidor.briefcase, servidor.cdusuarioaltera, servidor.dtaltera, ambienterede.cdambienterede, ambienterede.nome, plataforma.cdplataforma, plataforma.nome, " +
				"listaServicoservidor.cdservicoservidor, listaServicoservidor.nome, listaServicoservidor.primariodns, listaServicoservidor.primariocx, listaServicoservidor.parametro, " +
				"listaServicoservidor.ativo, servicoservidortipo.cdservicoservidortipo, listaServidornat.cdservidornat, listaServidornat.instrucao, listaServidornat.ativo, " +
				"listaServidorinterface.cdservidorinterface, listaServidorinterface.nome, listaServidorinterface.ativo, listaServidorfirewall.cdservidorfirewall, listaServidorfirewall.ordem, " +
				"listaServidorfirewall.acao, listaServidorfirewall.protocolo, listaServidorfirewall.iporigem, listaServidorfirewall.portaorigem, listaServidorfirewall.ipdestino, " +
				"servidorfirewallsentido.nome, listaServidorfirewall.portadestino, listaServidorfirewall.campo_interface, listaServidorfirewall.opcionais, listaServidorfirewall.ativo, servidorfirewallsentido.cdservidorfirewallsentido, " +
				"listaServidorrota.cdservidorrota, listaServidorrota.endereco, listaServidorrota.gateway, listaServidorrota.ativo, redemascara.cdredemascara, tiponas.cdtiponas")
				.join("servidor.ambienterede ambienterede")
				.join("servidor.plataforma plataforma")
				.leftOuterJoin("servidor.tiponas tiponas")
				.leftOuterJoin("servidor.listaServicoservidor listaServicoservidor")
				.leftOuterJoin("listaServicoservidor.servicoservidortipo servicoservidortipo")
				.leftOuterJoin("servidor.listaServidornat listaServidornat")
				.leftOuterJoin("servidor.listaServidorinterface listaServidorinterface")
				.leftOuterJoin("servidor.listaServidorfirewall listaServidorfirewall")
				.leftOuterJoin("servidor.listaServidorrota listaServidorrota")
				.leftOuterJoin("listaServidorrota.redemascara redemascara")
				.leftOuterJoin("listaServidorfirewall.servidorfirewallsentido servidorfirewallsentido");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaServicoservidor");
		save.saveOrUpdateManaged("listaServidornat");
		save.saveOrUpdateManaged("listaServidorinterface");
		save.saveOrUpdateManaged("listaServidorfirewall");
		save.saveOrUpdateManaged("listaServidorrota");
	}
	
	/**
	 * Retorna lista de servidores que possuam servi�os do tipo SMTP
	 * @param servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servidor> findByServicoSmtp(){
		Servicoservidortipo tipo = new Servicoservidortipo();
		tipo.setCdservicoservidortipo(Servicoservidortipo.SMTP);
		
		return query().select("distinct servidor.cdservidor, servidor.nome")
					.join("servidor.listaServicoservidor listaServicoservidor")
					.where("listaServicoservidor.servicoservidortipo = ?", tipo)
					.orderBy("servidor.nome asc")					
					.list();
	}
	
	/**
	 * Retorna lista de servidores que possuam servi�os do tipo FTP
	 * @param servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servidor> findByServicoFtp(){
		Servicoservidortipo tipo = new Servicoservidortipo();
		tipo.setCdservicoservidortipo(Servicoservidortipo.FTP);
		
		return query().select("distinct servidor.cdservidor, servidor.nome")
					.join("servidor.listaServicoservidor listaServicoservidor")
					.where("listaServicoservidor.servicoservidortipo = ?", tipo)
					.orderBy("servidor.nome asc")					
					.list();
	}

	/**
	 * Retorna lista de servidores que possuam servi�os do tipo Banco de dados
	 * @param servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servidor> findByServicoBancodados(){
		Servicoservidortipo tipo = new Servicoservidortipo();
		tipo.setCdservicoservidortipo(Servicoservidortipo.BANCO_DADOS);
		
		return query().select("distinct servidor.cdservidor, servidor.nome")
		.join("servidor.listaServicoservidor listaServicoservidor")
		.where("listaServicoservidor.servicoservidortipo = ?", tipo)
		.orderBy("servidor.nome asc")					
		.list();
	}

	/**
	 * Retorna lista de servidores de acordo com o tipo de servico e o ambiente de rede
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servidor> findByServicoAmbiente(Servicoservidortipo tipo, Ambienterede ambienterede){
		return query().select("distinct servidor.cdservidor, servidor.nome")
					.join("servidor.listaServicoservidor listaServicoservidor")
					.where("listaServicoservidor.servicoservidortipo = ?", tipo)
					.where("servidor.ambienterede = ?", ambienterede)
					.orderBy("servidor.nome asc")					
					.list();
	}
	
	/**
	 * Retorna o servidor marcado como BriefCase
	 * @return
	 * @author Thiago Gon�alves
	 */
	public Servidor findBriefcase(){
		return query().where("servidor.briefcase = ?", Boolean.TRUE)
					  .setMaxResults(1)
					  .unique();
	}

	/**
	 * M�todo que carrega os servidores que possui servi�o do tipo WEB
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Servidor> findForComboServicoWeb() {
		return query()
			.select("servidor.cdservidor, servidor.nome")
			.join("servidor.listaServicoservidor listaServicoservidor")
			.join("listaServicoservidor.servicoservidortipo servicoservidortipo")
			.where("servicoservidortipo = ?",new Servicoservidortipo(Servicoservidortipo.WEB))
			.list();
	}
	
	public Servidor findByIp(String ip) {
		List<Servidor> list = query()
			.select("servidor.cdservidor, servidor.nome")
			.where("servidor.ip = ?", ip)
			.list();
		
		if(list != null && list.size() > 0) return list.get(0);
		return null;
	}
}
