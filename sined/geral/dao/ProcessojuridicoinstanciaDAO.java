package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Processojuridicoinstancia;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("processojuridicoinstancia.numero")
public class ProcessojuridicoinstanciaDAO extends GenericDAO<Processojuridicoinstancia>{
	
	public Processojuridicoinstancia carregaInstancia(Integer cdprocessojuridicoinstancia) {
		if (cdprocessojuridicoinstancia == null ) {
			throw new SinedException("Processo Jur�dico Inst�ncia n�o pode ser nulo.");
		}
		return query()
					.select("processojuridicoinstancia.cdprocessojuridicoinstancia," +
							"processojuridicoinstancia.numero," +
							"processojuridico.cdprocessojuridico, processojuridico.sintese")
					.join("processojuridicoinstancia.processojuridico processojuridico")
					.where("processojuridicoinstancia.cdprocessojuridicoinstancia = ?", cdprocessojuridicoinstancia)
					.unique();
	}

	public List<Processojuridicoinstancia> listaDeInstancias(Integer cdprocessojuridico) {
		return 
		query()
			.select("processojuridicoinstancia.cdprocessojuridicoinstancia, processojuridicoinstancia.numero, " +
					"processojuridicoinstanciatipo.cdprocessojuridicoinstanciatipo, processojuridicoinstanciatipo.nome, processojuridico.cdprocessojuridico")
			.join("processojuridicoinstancia.processojuridicoinstanciatipo processojuridicoinstanciatipo")
			.join("processojuridicoinstancia.processojuridico processojuridico")
			.where("processojuridico.cdprocessojuridico = ?", cdprocessojuridico)
			.orderBy("processojuridicoinstanciatipo.cdprocessojuridicoinstanciatipo desc")
			.list();
	}
	
	public List<Processojuridicoinstancia> listaAtualizarandamento() {
		return query()
				.select("processojuridicoinstancia.cdprocessojuridicoinstancia, processojuridicoinstancia.numero, " +
						"processojuridicoinstancia.dtultimaatualizacao, vara.cdvara, uf.cduf, " +
						"uf.sigla, orgaojuridico.cdorgaojuridico, orgaojuridico.nome, orgaojuridico.endereco ")
				.join("processojuridicoinstancia.vara vara")
				.join("vara.orgaojuridico orgaojuridico")
				.join("vara.uf uf")
				.list();
	}

}
