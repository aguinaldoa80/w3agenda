package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Vcategoria;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CategoriaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;


@DefaultOrderBy("categoria.nome")
public class CategoriaDAO extends GenericDAO<Categoria>{

	@Override
	public void updateEntradaQuery(QueryBuilder<Categoria> query) {
		query.leftOuterJoinFetch("categoria.categoriapai categoriapai")
			.leftOuterJoinFetch("categoria.vcategoria vcategoria");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Categoria> query,FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		}
		CategoriaFiltro filtro = (CategoriaFiltro) _filtro;
		Vcategoria categoriaPai = new Vcategoria();
		if(filtro.getCategoriapai() != null){
			categoriaPai = carregarIdentificador(filtro.getCategoriapai());
		}
		query
			.select("categoria.cdcategoria, categoria.nome, vcategoria.cdcategoria, vcategoria.identificador")
			.leftOuterJoin("categoria.vcategoria vcategoria")
			.whereLikeIgnoreAll("categoria.nome",filtro.getNome());
		
		if (filtro.getCliente() || filtro.getFornecedor()){
			query.openParentheses()
				.where("categoria.cliente=?", filtro.getCliente())
				.or()
				.where("categoria.fornecedor=?", filtro.getFornecedor())
			.closeParentheses();
		}
		if (categoriaPai != null){
			query.whereLikeIgnoreAll("vcategoria.identificador", categoriaPai.getIdentificador());
		}
		query.orderBy("categoria.nome");
	}

	public List<Categoria> findByTipo(boolean isCliente, boolean isFornecedor) {
		
		QueryBuilder<Categoria> query = querySined()
			.joinFetch("categoria.vcategoria vcategoria");
		
		if (isCliente)
			query.where("categoria.cliente is true");
		if (isFornecedor)
			query.where("categoria.fornecedor is true");

		return query.list();
	}
	

	public Categoria loadWithIdentificador(Categoria categoria) {
		return querySined()
				.select("categoria.cdcategoria, categoria.nome, vcategoria.identificador, vcategoria.arvorepai")
				.join("categoria.vcategoria vcategoria")
				.where("categoria = ?", categoria)
				.unique();
	}


	public List<Categoria> findAutocompleteTodos(String q, Boolean isCliente, Boolean isFornecedor) {
		return querySined()
					.select("categoria.cdcategoria, categoria.nome, vcategoria.identificador, vcategoria.arvorepai")
					.join("categoria.vcategoria vcategoria")
					
					.openParentheses()
					.whereLikeIgnoreAll("categoria.nome", q)
					.or()
					.whereLikeIgnoreAll("vcategoria.arvorepai", q)
					.or()
					.where("vcategoria.identificador like ?||'%'", q)
					.closeParentheses()
					
					.openParentheses()
						.where("categoria.cliente=?", isCliente)
						.or()
						.where("categoria.fornecedor=?", isFornecedor)
					.closeParentheses()
					.orderBy("vcategoria.identificador")
					.list();
	}

	/**
	 * Encontra somente as raizes da arvore.
	 * 
	 * @return
	 * @author Giovane Freitas
	 */
	public List<Categoria> findRaiz(){
		return querySined()
					.select("categoria.item")
					.where("categoriapai is null")
					.orderBy("categoria.item desc")
					.list();
	}
	
	/**
	 * Encontra a listagem dos filhos da categoria pai fornecida.
	 * 
	 * @param pai
	 * @return
	 * @author Giovane Freitas
	 */
	public List<Categoria> findFilhos(Categoria pai){
		if (pai == null || pai.getCdcategoria() == null) {
			throw new SinedException("A categoria superior n�o pode ser nula.");
		}
		return querySined()
					.select("categoria.item")
					.join("categoria.categoriapai pai")
					.where("pai = ?",pai)
					.orderBy("categoria.item desc")
					.list();
	}

	/**
	 * 
	 * M�todo para carregar o identificador da categoria
	 *
	 *@author Thiago Augusto
	 *@date 24/02/2012
	 * @param pai
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Vcategoria carregarIdentificador(Categoria bean){
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT v.identificador AS IDENTIFICADOR ");
		sql.append("FROM vcategoria v ");
		sql.append("WHERE v.cdcategoria = " + bean.getCdcategoria());
		
		SinedUtil.markAsReader();
		List<Vcategoria> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Vcategoria(rs.getString("IDENTIFICADOR"));
			}
		});
		
		return SinedUtil.isListNotEmpty(lista) ? lista.get(0) : null;
	}

	/**
	 * Busca pelo nome da categoria.
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/02/2013
	 */
	public Categoria findByNome(String nome) {
		if(nome == null || nome.equals("")){
			throw new SinedException("Nome da categoria n�o pode ser nulo.");
		}
		
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		List<Categoria> lista = querySined()
			.select("categoria.cdcategoria, categoria.nome")
			.where("UPPER(" + funcaoTiraacento + "(categoria.nome)) LIKE ?", Util.strings.tiraAcento(nome).toUpperCase())
			.list();
		
		if(lista != null && lista.size() > 1){
			throw new SinedException("Mais de uma categoria encontrada.");
		} else if(lista != null && lista.size() == 1){
			return lista.get(0);
		} else return null;
	}

	public List<Categoria> findForAndroid(String whereIn) {
		return querySined()
				.select("categoria.cdcategoria, categoria.item, categoria.nome, categoria.cliente, categoria.fornecedor, " +
						"categoriapai.cdcategoria ")
				.leftOuterJoin("categoria.categoriapai categoriapai")
				.whereIn("categoria.cdcategoria", whereIn)
				.list();
	}

	/**
	 * Busca as categorias pela pessoa.
	 *
	 * @param pessoa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/11/2014
	 */
	public List<Categoria> findByPessoa(Pessoa pessoa) {
		return querySined()
					.select("categoria.cdcategoria, categoria.nome")
					.join("categoria.listaPessoacategoria pessoacategoria")
					.where("pessoacategoria.pessoa = ?", pessoa)
					.list();
	}
	
	/**
	 * Busca as categorias para tela de grupo de material.
	 *
	 * @return
	 * @author Jo�o Vitor
	 * @since 27/05/2015
	 */
	public List<Categoria> findForMaterialgrupocomissao() {
		return querySined()
				.select("categoria.cdcategoria, categoria.nome")
				.openParentheses()
					.where("COALESCE(categoria.fornecedor, FALSE) = ?", Boolean.TRUE)
					.or()
					.openParentheses()
						.where("COALESCE(categoria.cliente, FALSE) = ?", Boolean.FALSE)
						.where("COALESCE(categoria.fornecedor, FALSE) = ?", Boolean.FALSE)
					.closeParentheses()
				.closeParentheses()
				.list();
	}
}
