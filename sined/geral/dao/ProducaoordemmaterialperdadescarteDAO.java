package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialperdadescarte;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoordemmaterialperdadescarteDAO extends GenericDAO<Producaoordemmaterialperdadescarte> {

	public List<Producaoordemmaterialperdadescarte> findByProducaoordemmaterial(Producaoordemmaterial producaoordemmaterial){
		return query()
				.select("producaoordemmaterialperdadescarte.cdproducaoordemmaterialperdadescarte, producaoordemmaterialperdadescarte.qtdeperdadescarte, " +
						"motivodevolucao.cdmotivodevolucao, motivodevolucao.descricao, " +
						"operador.cdpessoa, operador.nome, " +
						"producaoordemmaterialoperador.cdproducaoordemmaterialoperador, producaoordemmaterialoperador.qtdeproduzido, producaoordemmaterialoperador.qtdeperdadescarte")
				.join("producaoordemmaterialperdadescarte.motivodevolucao motivodevolucao")
				.join("producaoordemmaterialperdadescarte.producaoordemmaterial producaoordemmaterial")
				.leftOuterJoin("producaoordemmaterialperdadescarte.producaoordemmaterialoperador producaoordemmaterialoperador")
				.leftOuterJoin("producaoordemmaterialoperador.operador operador")
				.where("producaoordemmaterial = ?", producaoordemmaterial)
				.list();
	}
	
	public void delete(Producaoordemmaterial producaoordemmaterial) {
		getHibernateTemplate().bulkUpdate("delete from Producaoordemmaterialperdadescarte p where p.producaoordemmaterial=?", producaoordemmaterial);
	}
	
	public void delete(Producaoordem producaoordem) {
		if(producaoordem != null && producaoordem.getCdproducaoordem() != null){
			getHibernateTemplate().bulkUpdate("delete from Producaoordemmaterialperdadescarte p where p.producaoordemmaterial.cdproducaoordemmaterial in (" +
				"select pom.cdproducaoordemmaterial from Producaoordemmaterial pom join pom.producaoordem po where po.cdproducaoordem = ?) ", producaoordem.getCdproducaoordem());
		}
	}
}
