package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Formulacusto;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.FormulacustoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FormulacustoDAO extends GenericDAO<Formulacusto> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Formulacusto> query,FiltroListagem _filtro) {
		FormulacustoFiltro filtro = (FormulacustoFiltro) _filtro;
		
		query
			.whereLikeIgnoreAll("formulacusto.nome", filtro.getNome())
			.orderBy("formulacusto.nome")
		;
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Formulacusto> query) {
		query
			.select("formulacusto.cdformulacusto, formulacusto.nome, formulacusto.ativo, formulacusto.cdusuarioaltera, formulacusto.dtaltera, " +
					"listaFormulacustoitem.cdformulacustoitem, listaFormulacustoitem.ordem, listaFormulacustoitem.identificador, " +
					"listaFormulacustoitem.formula")
			.leftOuterJoin("formulacusto.listaFormulacustoitem listaFormulacustoitem")
			.orderBy("listaFormulacustoitem.ordem");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.getEntity();
		save.saveOrUpdateManaged("listaFormulacustoitem");
	}
	
	/**
	* M�todo que verifica se existe uma f�rmula ativa.
	*
	* @param formulacusto
	* @return
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public boolean existsFormulaAtiva(Formulacusto formulacusto){
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Formulacusto.class)
			.setUseTranslator(false)
			.where("ativo = true")
			.where("formulacusto.cdformulacusto <> ?", formulacusto != null ? formulacusto.getCdformulacusto() : null)
			.unique() > 0;
	}

	/**
	* M�todo que carrega a formula ativa para calcular o custo da entrada do material
	*
	* @return
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public Formulacusto getFormulacustoForcalcularCustoEntradaMaterial() {
		QueryBuilder<Formulacusto> query = query();
		updateEntradaQuery(query);
		
		List<Formulacusto> lista = query
			.where("ativo = true")
			.list();
		
		return SinedUtil.isListNotEmpty(lista) ? lista.get(0) : null;
	}
}
