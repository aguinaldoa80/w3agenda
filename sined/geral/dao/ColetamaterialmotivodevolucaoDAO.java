package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.Coletamaterialmotivodevolucao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColetamaterialmotivodevolucaoDAO extends GenericDAO<Coletamaterialmotivodevolucao> {
	
	public void delete(ColetaMaterial coletaMaterial){
		getHibernateTemplate().bulkUpdate("delete from Coletamaterialmotivodevolucao c where c.coletaMaterial=?", coletaMaterial);
	}
}
