package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.OtrDesenho;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OtrDesenhoDAO extends GenericDAO<OtrDesenho>{
	
	public List<OtrDesenho> findDesenho (){
		return query()
				.select("otrDesenho.cdOtrDesenho, otrDesenho.desenho")
				.orderBy("otrDesenho.desenho")
				.list();
	}

}
