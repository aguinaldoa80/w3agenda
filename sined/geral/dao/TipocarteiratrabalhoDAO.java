package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Tipocarteiratrabalho;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("tipocarteiratrabalho.nome")
public class TipocarteiratrabalhoDAO extends GenericDAO<Tipocarteiratrabalho> {

}
