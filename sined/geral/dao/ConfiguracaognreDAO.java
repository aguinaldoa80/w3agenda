package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Configuracaognre;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ConfiguracaognreDAO extends GenericDAO<Configuracaognre> {
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Configuracaognre> query) {
		query.leftOuterJoinFetch("configuracaognre.fornecedor fornecedor");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Configuracaognre> query, FiltroListagem _filtro) {
		query.leftOuterJoinFetch("configuracaognre.empresa empresa");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaConfiguracaognrerateio");
		save.saveOrUpdateManaged("listaConfiguracaognreuf");
	}

	public boolean hasDuplicateByEmpresaAndCodigo(Configuracaognre configuracaognre) {
		return newQueryBuilderSinedWithFrom(Long.class)
				.select("count(*)")
				.where("configuracaognre.cdconfiguracaognre <> ?", configuracaognre.getCdconfiguracaognre())
				.where("configuracaognre.empresa = ?", configuracaognre.getEmpresa())
				.where("configuracaognre.codigoreceita = ?", configuracaognre.getCodigoreceita())
				.unique() > 0;
	}

	public List<Configuracaognre> findByEmpresa(Empresa empresa) {
		return query()
				.select("configuracaognre.cdconfiguracaognre, configuracaognre.codigoreceita")
				.where("configuracaognre.empresa = ?", empresa)
				.list();
	}

}
