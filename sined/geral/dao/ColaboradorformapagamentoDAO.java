package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Colaboradorformapagamento;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("colaboradorformapagamento.nome")
public class ColaboradorformapagamentoDAO extends GenericDAO<Colaboradorformapagamento> {

}
