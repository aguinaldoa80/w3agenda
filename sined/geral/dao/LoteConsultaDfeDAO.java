package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.SituacaoEmissorEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.TipoLoteConsultaDfeEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.LoteConsultaDfeFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class LoteConsultaDfeDAO extends GenericDAO<LoteConsultaDfe> {

	@Override
	public void updateListagemQuery(QueryBuilder<LoteConsultaDfe> query, FiltroListagem _filtro) {
		LoteConsultaDfeFiltro filtro = (LoteConsultaDfeFiltro) _filtro;
		query
			.select("loteConsultaDfe.cdLoteConsultaDfe, loteConsultaDfe.tipoLoteConsultaDfeEnum, empresa.cdpessoa, empresa.nomefantasia, " +
					"loteConsultaDfe.nsu, loteConsultaDfe.chaveAcesso, loteConsultaDfe.operacaoEnum, loteConsultaDfe.situacaoEnum ")
			.leftOuterJoin("loteConsultaDfe.empresa empresa")
			.where("loteConsultaDfe.tipoLoteConsultaDfeEnum = ?", filtro.getTipoLoteConsultaDfeEnum())
			.where("loteConsultaDfe.empresa = ?", filtro.getEmpresa())
			.where("loteConsultaDfe.nsu = ?", filtro.getNsu())
			.where("loteConsultaDfe.chaveAcesso = ?", filtro.getChaveAcesso())
			.where("loteConsultaDfe.dhResp >= ?", filtro.getDhRespInicio())
			.where("loteConsultaDfe.dhResp <= ?", filtro.getDhRespFim())
			.where("loteConsultaDfe.nsu >= ?", filtro.getNsuInicio())
			.where("loteConsultaDfe.nsu <= ?", filtro.getNsuFim());
		
		if (filtro.getTipoEventoNfe() != null) {
			query
				.where("listaManifestoDfeEvento.tpEvento = ?", filtro.getTipoEventoNfe())
				.leftOuterJoin("loteConsultaDfe.listaLoteConsultaDfeItem listaLoteConsultaDfeItem")
				.leftOuterJoin("listaLoteConsultaDfeItem.listaManifestoDfe listaManifestoDfe")
				.leftOuterJoin("listaManifestoDfe.listaManifestoDfeEvento listaManifestoDfeEvento")
				.ignoreJoin("listaLoteConsultaDfeItem", "listaManifestoDfe", "listaManifestoDfeEvento");
		}
			
		query
			.whereIn("loteConsultaDfe.situacaoEnum", filtro.getListaSituacaoEnum() != null ? SituacaoEmissorEnum.listAndConcatenate(filtro.getListaSituacaoEnum()) : null)
			.orderBy("loteConsultaDfe.cdLoteConsultaDfe desc");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<LoteConsultaDfe> query) {
		query
			.leftOuterJoinFetch("loteConsultaDfe.arquivoXml arquivoXml")
			.leftOuterJoinFetch("loteConsultaDfe.arquivoRetornoXml arquivoRetornoXml")
			.leftOuterJoinFetch("loteConsultaDfe.empresa empresa")
			.leftOuterJoinFetch("loteConsultaDfe.configuracaoNfe configuracaoNfe")
			.leftOuterJoinFetch("configuracaoNfe.uf uf")
			.leftOuterJoinFetch("configuracaoNfe.configuracaoNfeManifestoAutomatico configuracaoNfeManifestoAutomatico")
			.leftOuterJoinFetch("configuracaoNfeManifestoAutomatico.uf ufufAutomatico");
	}

	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		LoteConsultaDfe bean = (LoteConsultaDfe) save.getEntity();
		
		if (bean.getListaLoteConsultaDfeItem() != null && bean.getListaLoteConsultaDfeItem().size() > 0) {
			save.saveOrUpdateManaged("listaLoteConsultaDfeItem");
		}
		
		if (bean.getListaLoteConsultaDfeHistorico() != null && bean.getListaLoteConsultaDfeHistorico().size() > 0) {
			save.saveOrUpdateManaged("listaLoteConsultaDfeHistorico");
		}
	}
	
	public void atualizarArquivoXml(LoteConsultaDfe loteConsultaDfe) {
		getHibernateTemplate().bulkUpdate("update LoteConsultaDfe l set l.arquivoXml = ? where l.id = ?", 
				new Object[] {loteConsultaDfe.getArquivoXml(), loteConsultaDfe.getCdLoteConsultaDfe()});
	}
	
	public void atualizarSituacaoEnum(LoteConsultaDfe loteConsultaDfe) {
		getHibernateTemplate().bulkUpdate("update LoteConsultaDfe l set l.situacaoEnum = ? where l.id = ?", 
				new Object[] {loteConsultaDfe.getSituacaoEnum(), loteConsultaDfe.getCdLoteConsultaDfe()});
	}
	
	public void atualizarConfiguracaoNfe(LoteConsultaDfe loteConsultaDfe) {
		getHibernateTemplate().bulkUpdate("update LoteConsultaDfe l set l.configuracaoNfe = ? where l.id = ?", 
				new Object[] {loteConsultaDfe.getConfiguracaoNfe(), loteConsultaDfe.getCdLoteConsultaDfe()});
	}
	
	public void atualizarArquivoRetornoXml(LoteConsultaDfe loteConsultaDfe) {
		getHibernateTemplate().bulkUpdate("update LoteConsultaDfe l set l.arquivoRetornoXml = ? where l.id = ?", 
				new Object[] {loteConsultaDfe.getArquivoRetornoXml(), loteConsultaDfe.getCdLoteConsultaDfe()});
	}
	
	public void atualizarNsu(LoteConsultaDfe loteConsultaDfe) {
		getHibernateTemplate().bulkUpdate("update LoteConsultaDfe l set l.nsu = ? where l.id = ?", 
				new Object[] {loteConsultaDfe.getNsu(), loteConsultaDfe.getCdLoteConsultaDfe()});
	}

	public List<LoteConsultaDfe> procurarLoteConsultaDfeParaConsulta(Empresa empresa) {
		if (empresa == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return querySined()
				
				.select("loteConsultaDfe.cdLoteConsultaDfe, loteConsultaDfe.dhEnvio, configuracaoNfe.cdconfiguracaonfe, arquivoXml.cdarquivo")
				.leftOuterJoin("loteConsultaDfe.empresa empresa")
				.join("loteConsultaDfe.configuracaoNfe configuracaoNfe")
				.join("loteConsultaDfe.arquivoXml arquivoXml")
				.where("empresa = ?", empresa)
				.where("loteConsultaDfe.situacaoEnum = ?", SituacaoEmissorEnum.GERADO)
				.where("arquivoXml is not null")
				.where("loteConsultaDfe.dhEnvio < ?", SinedDateUtils.currentTimestamp())
				.openParentheses()
					.where("loteConsultaDfe.enviado is null")
					.or()
					.where("loteConsultaDfe.enviado = ?", Boolean.FALSE)
				.closeParentheses()
				.list();
	}

	public LoteConsultaDfe procurarPorId(Integer cdLoteConsultaDfe) {
		if (cdLoteConsultaDfe == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("loteConsultaDfe.cdLoteConsultaDfe, loteConsultaDfe.nsu, loteConsultaDfe.chaveAcesso, loteConsultaDfe.tipoLoteConsultaDfeEnum, " +
						"loteConsultaDfe.situacaoEnum, " +
						"empresa.cdpessoa, " +
						"arquivoXml.cdarquivo ")
				.leftOuterJoin("loteConsultaDfe.empresa empresa")
				.leftOuterJoin("loteConsultaDfe.arquivoXml arquivoXml")
				.where("loteConsultaDfe.cdLoteConsultaDfe = ?", cdLoteConsultaDfe)
				.unique();
	}

	public Integer marcarLoteConsultaDfe(String whereInLoteConsultaDfe, String flag) {
		return getHibernateTemplate()
				.bulkUpdate("update LoteConsultaDfe l set l." + flag + " = ? where l.cdLoteConsultaDfe in (" + whereInLoteConsultaDfe + ") and (l." + flag + " = ? or l." + flag + " is null)", 
							new Object[]{Boolean.TRUE, Boolean.FALSE});
	}

	public List<LoteConsultaDfe> temLoteConsultaDfeDiferenteDe(String selectedItens, SituacaoEmissorEnum situacaoEnum) {
		if (StringUtils.isEmpty(selectedItens) || situacaoEnum == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("loteConsultaDfe.cdLoteConsultaDfe, loteConsultaDfe.nsu")
				.where("loteConsultaDfe.situacaoEnum <> ?", situacaoEnum)
				.whereIn("loteConsultaDfe.cdLoteConsultaDfe", selectedItens)
				.list();
	}

	public LoteConsultaDfe procurarSituacao(LoteConsultaDfe bean) {
		if (bean == null || bean.getCdLoteConsultaDfe() == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("loteConsultaDfe.cdLoteConsultaDfe, loteConsultaDfe.situacaoEnum")
				.where("loteConsultaDfe.cdLoteConsultaDfe = ?", bean.getCdLoteConsultaDfe())
				.unique();
	}

	public List<LoteConsultaDfe> procurarLoteConsultaDfeNsuSeqPendente(Configuracaonfe configuracaonfe) {
		if (configuracaonfe == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("loteConsultaDfe.cdLoteConsultaDfe, loteConsultaDfe.situacaoEnum")
				.where("loteConsultaDfe.configuracaoNfe = ?", configuracaonfe)
				.where("loteConsultaDfe.situacaoEnum = ?", SituacaoEmissorEnum.GERADO)
				.where("loteConsultaDfe.tipoLoteConsultaDfeEnum = ?", TipoLoteConsultaDfeEnum.NSU_SEQUENCIA)
				.list();
	}

	public Boolean procurarEmpresasDoLoteConsultaDfe(String selectedItens) {
		if (StringUtils.isEmpty(selectedItens)) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("distinct empresa.cdpessoa ")
				.leftOuterJoin("loteConsultaDfe.empresa empresa")
				.whereIn("loteConsultaDfe.cdLoteConsultaDfe", selectedItens)
				.list()
				.size() > 1;
	}
}
