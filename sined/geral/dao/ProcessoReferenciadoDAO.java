package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.ProcessoReferenciado;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.ProcessoReferenciadoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedpiscofinsFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProcessoReferenciadoDAO extends GenericDAO<ProcessoReferenciado> {

	@Override
	public void updateListagemQuery(QueryBuilder<ProcessoReferenciado> query,
			FiltroListagem _filtro) {
		ProcessoReferenciadoFiltro filtro = (ProcessoReferenciadoFiltro) _filtro;
		
		query
			.select("empresa.cdpessoa,empresa.nomefantasia, processoReferenciado.cdProcessoReferenciado, processoReferenciado.numeroProcessoJudicial,processoReferenciado.dataSentenca, processoReferenciado.idSecaoJudiciaria,processoReferenciado.idVara,processoReferenciado.ativo,acaojudicialnatureza.cdAcaoJudicialNatureza,acaojudicialnatureza.codigo,acaojudicialnatureza.descricao")
			.leftOuterJoin("processoReferenciado.empresa empresa")
			.leftOuterJoin("processoReferenciado.acaoJudicialNatureza acaojudicialnatureza")
			.where("empresa", filtro.getEmpresa())
			.where("processoReferenciado.numeroProcessoJudicial", filtro.getNumeroProcessoJudicial())
			.where("processoReferenciado.dataSentenca", filtro.getDataSentenca())
			.where("processoReferenciado.ativo", filtro.getAtivo())
			.orderBy("processoReferenciado.dataSentenca DESC");
			
	}
	@Override
	public void updateEntradaQuery(QueryBuilder<ProcessoReferenciado> query) {
		query
			.select("processoReferenciado.cdProcessoReferenciado, processoReferenciado.numeroProcessoJudicial,processoReferenciado.idSecaoJudiciaria,processoReferenciado.idVara," +
					"processoReferenciado.descricao,processoReferenciado.ativo,processoReferenciado.dataSentenca,empresa.cdpessoa,empresa.nomefantasia, acaojudicialnatureza.cdAcaoJudicialNatureza,acaojudicialnatureza.codigo,acaojudicialnatureza.descricao,processoReferenciadoDocumento.cdProcessoReferenciadoDocumento, processoReferenciadoDocumento.tipoOperacao," +
					"processoReferenciadoDocumento.dataOperacao, processoReferenciadoDocumento.valorOperacao, processoReferenciadoDocumento.cstPis,processoReferenciadoDocumento.aliqPis," +
					"processoReferenciadoDocumento.cstCofins, processoReferenciadoDocumento.aliqCofins, processoReferenciadoDocumento.cstPisSuspensa,processoReferenciadoDocumento.bcPisSuspenso," +
					"processoReferenciadoDocumento.aliqPisSuspenso,processoReferenciadoDocumento.vlrPisSuspenso,processoReferenciadoDocumento.cstCofinsSuspenso,processoReferenciadoDocumento.bcCofinsSuspenso," +
					"processoReferenciadoDocumento.aliqCofinsSuspenso,processoReferenciadoDocumento.vlrCofinsSuspenso,processoReferenciadoDocumento.descricaoDocumento,processoReferenciadoDocumento.vlBcPis," +
					"processoReferenciadoDocumento.vlPis,processoReferenciadoDocumento.vlBcCofins,processoReferenciadoDocumento.vlCofins,processoReferenciadoDocumento.tipoCalculoPis,processoReferenciadoDocumento.tipoCalculoCofins," +
					"processoReferenciadoDocumento.qtdevendidapis,processoReferenciadoDocumento.qtdevendidaCofins,participante.cdpessoa, participante.nome,notaFiscalProduto.cdNota,notaFiscalProduto.numero," +
					"Entregadocumento.cdentregadocumento,Entregadocumento.numero,material.cdmaterial,material.nome,contaContabil.cdcontacontabil,contaContabil.nome")
			.leftOuterJoin("processoReferenciado.empresa empresa")
			.leftOuterJoin("processoReferenciado.acaoJudicialNatureza acaojudicialnatureza")
			.leftOuterJoin("processoReferenciado.listaProcessoReferenciadoDocumento processoReferenciadoDocumento")
			.leftOuterJoin("processoReferenciadoDocumento.participante participante")
			.leftOuterJoin("processoReferenciadoDocumento.notaFiscalProduto notaFiscalProduto")
			.leftOuterJoin("processoReferenciadoDocumento.entradaFiscal Entregadocumento")
			.leftOuterJoin("processoReferenciadoDocumento.material material")
			.leftOuterJoin("processoReferenciadoDocumento.contaContabil contaContabil");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaProcessoReferenciadoDocumento");
	}
	
	public List<ProcessoReferenciado>findForSpedPisconfinsReg1010 (SpedpiscofinsFiltro filtro){
		return query()
				.select("processoReferenciado.cdProcessoReferenciado, processoReferenciado.numeroProcessoJudicial,processoReferenciado.idSecaoJudiciaria,processoReferenciado.idVara," +
						"processoReferenciado.descricao,processoReferenciado.ativo,processoReferenciado.dataSentenca,acaojudicialnatureza.codigo,acaojudicialnatureza.descricao, processoReferenciadoDocumento.cdProcessoReferenciadoDocumento," +
						"processoReferenciadoDocumento.dataOperacao,processoReferenciadoDocumento.valorOperacao,processoReferenciadoDocumento.cstPis,processoReferenciadoDocumento.cstCofins,processoReferenciadoDocumento.cstPisSuspensa," +
						"processoReferenciadoDocumento.cstCofinsSuspenso,processoReferenciadoDocumento.aliqPis,processoReferenciadoDocumento.aliqCofins," +
						"processoReferenciadoDocumento.bcPisSuspenso,processoReferenciadoDocumento.aliqPisSuspenso,processoReferenciadoDocumento.vlrPisSuspenso,processoReferenciadoDocumento.bcCofinsSuspenso," +
						"processoReferenciadoDocumento.aliqCofinsSuspenso,processoReferenciadoDocumento.vlrCofinsSuspenso,processoReferenciadoDocumento.descricaoDocumento,processoReferenciadoDocumento.vlBcPis," +
						"processoReferenciadoDocumento.vlPis,processoReferenciadoDocumento.vlBcCofins,processoReferenciadoDocumento.vlCofins,notaFiscalProduto.cdNota,notaFiscalProdutoItem.chaveacessoexportacao," +
						"material.cdmaterial,material.identificacao,contaContabil.cdcontacontabil,contaContabil.nome,cliente.cdpessoa,fornecedor.cdpessoa,participante.cdpessoa,participante.cpf,participante.cnpj," +
						"EnderecoCliente.cdendereco,EnderecoCliente.inscricaoestadual, empresa.cdpessoa, empresa.cnpj")
				.leftOuterJoin("processoReferenciado.empresa empresa")
				.leftOuterJoin("processoReferenciado.acaoJudicialNatureza acaojudicialnatureza")
				.leftOuterJoin("processoReferenciado.listaProcessoReferenciadoDocumento processoReferenciadoDocumento")
				.leftOuterJoin("processoReferenciadoDocumento.notaFiscalProduto notaFiscalProduto")
				.leftOuterJoin("notaFiscalProduto.listaItens notaFiscalProdutoItem")
				.leftOuterJoin("notaFiscalProduto.enderecoCliente EnderecoCliente")
				.leftOuterJoin("processoReferenciadoDocumento.material material")
				.leftOuterJoin("processoReferenciadoDocumento.contaContabil contaContabil")
				.leftOuterJoin("processoReferenciadoDocumento.participante participante")
				.leftOuterJoin("participante.cliente cliente")
				.leftOuterJoin("participante.fornecedorBean fornecedor")
				.where("processoReferenciado.dataSentenca >= ?", filtro.getDtinicio())
				.where("processoReferenciado.dataSentenca <= ?", filtro.getDtfim())
				.list();
	}
}
