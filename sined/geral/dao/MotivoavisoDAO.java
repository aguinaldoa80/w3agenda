package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MotivoavisoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("motivoaviso.descricao")
public class MotivoavisoDAO extends GenericDAO<Motivoaviso>{

	@Override
	public void updateEntradaQuery(QueryBuilder<Motivoaviso> query) {
		query
			.select("motivoaviso.cdmotivoaviso, motivoaviso.motivo, motivoaviso.descricao, motivoaviso.avisoorigem, motivoaviso.ativo, " +
					"motivoaviso.cdusuarioaltera, motivoaviso.dtaltera, motivoaviso.email, motivoaviso.notificacao, motivoaviso.popup, " +
					"tipoaviso.cdtipoaviso, usuario.cdpessoa, papel.cdpapel, motivoaviso.emailDiario ")
			.leftOuterJoin("motivoaviso.tipoaviso tipoaviso")
			.leftOuterJoin("motivoaviso.papel papel")
			.leftOuterJoin("motivoaviso.usuario usuario");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Motivoaviso> query, FiltroListagem _filtro) {
		MotivoavisoFiltro filtro = (MotivoavisoFiltro) _filtro;
		query
			.select("motivoaviso.cdmotivoaviso, motivoaviso.descricao, tipoaviso.descricao, motivoaviso.email, motivoaviso.notificacao, " +
					"motivoaviso.popup, motivoaviso.emailDiario, motivoaviso.ativo ")
			.leftOuterJoin("motivoaviso.tipoaviso tipoaviso")
			.leftOuterJoin("motivoaviso.papel papel")
			.where("motivoaviso.avisoorigem = ?", filtro.getAvisoorigem())
			.where("motivoaviso.tipoaviso = ?", filtro.getTipoaviso())
			.where("papel = ?", filtro.getPapel())
			.where("motivoaviso.usuario = ?", filtro.getUsuario())
			.where("motivoaviso.ativo = ?",filtro.getAtivo())
			.orderBy("motivoaviso.descricao");
	}

	public Motivoaviso findByMotivo(MotivoavisoEnum motivo) {
		if(motivo == null) {
			return null;
		}
		
		return query()
			.select("motivoaviso.cdmotivoaviso, usuario.cdpessoa, papel.cdpapel, tipoaviso.cdtipoaviso, motivoaviso.email, motivoaviso.avisoorigem ")
			.join("motivoaviso.tipoaviso tipoaviso")
			.leftOuterJoin("motivoaviso.papel papel")
			.leftOuterJoin("motivoaviso.usuario usuario")
			.where("motivoaviso.motivo = ?", motivo)
			.where("coalesce(motivoaviso.ativo, false) = true")
			.unique();
	}
	
	public List<Motivoaviso> findAllForCreateAviso() {
		return query()
				.select("motivoaviso.cdmotivoaviso, motivoaviso.avisoorigem, motivoaviso.motivo, motivoaviso.ativo, tipoaviso.cdtipoaviso, papel.cdpapel ")
				.leftOuterJoin("motivoaviso.tipoaviso tipoaviso")
				.leftOuterJoin("motivoaviso.papel papel")
				.list();
	}

	public void updateChecksMotivoAviso(String selectedItens, String acao) {
		if (acao.equals("1")) {
			acao = "emailDiario";
		} else if (acao.equals("2")) {
			acao = "email";
		} else if (acao.equals("3")) {
			acao = "notificacao";
		} else if (acao.equals("4")) {
			acao = "popup";
		} else if (acao.equals("5")) {
			acao = "ativo";
		}
		
		getHibernateTemplate().bulkUpdate("update Motivoaviso set " + acao + " = false where cdmotivoaviso in (" + selectedItens + ")");
	}
}
