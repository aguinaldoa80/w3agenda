package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Leadsegmento;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class LeadsegmentoDAO extends GenericDAO<Leadsegmento>{
	
	public List<Leadsegmento> findByLead(Lead lead) {
		if (lead == null || lead.getCdlead() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return 
			query()
				.select("segmento.nome, segmento.cdsegmento, lead.cdlead")
				.join("leadsegmento.segmento segmento")
				.join("leadsegmento.lead lead")
				.where("lead = ?",lead)
				.orderBy("lead.cdlead")
				.list();
	}

}
