package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Formularhvariavel;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.FormularhvariavelFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FormularhvariavelDAO extends GenericDAO<Formularhvariavel>{
	
	public void updateListagemQuery(QueryBuilder<Formularhvariavel> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}		
		
		FormularhvariavelFiltro filtro = (FormularhvariavelFiltro) _filtro;
		
		query.leftOuterJoinFetch("formularhvariavel.formularh formularh");		
		
		if(filtro.getNome()!=null && !filtro.getNome().isEmpty())
			query.where("formularhvariavel.nome = ?",filtro.getNome());
		if(filtro.getAtiva()!=null)
			query.where("formularhvariavel.ativa = ?",filtro.getAtiva());
		if(filtro.getValor()!=null && filtro.getValor()!=0)
			query.where("formularhvariavel.valor = ?",filtro.getValor());
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save){
		save			
			.saveOrUpdateManaged("formularhvariavelintervalo");
	} 
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Formularhvariavel> query) {
		query
			.leftOuterJoinFetch("formularhvariavel.formularhvariavelintervalo formularhvariavelintervalo")
			.setUseTranslator(true);		
	}
	
	public List<Formularhvariavel> findByFiltro(FormularhvariavelFiltro filtro){
		
		if(filtro == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		QueryBuilder<Formularhvariavel> query = query();		
		
		query.leftOuterJoin("formularhvariavel.formularh formularh");
		
		if(filtro.getAtiva()!=null)
			query.where("formularhvariavel.ativa = ?",filtro.getAtiva());
		if(filtro.getNome()!=null && !filtro.getNome().isEmpty())
			query.where("formularhvariavel.nome = ?",filtro.getNome());
		if(filtro.getValor()!=null && filtro.getValor()!=0)
			query.where("formularhvariavel.valor = ?",filtro.getValor());
		return query.list();
	}
}