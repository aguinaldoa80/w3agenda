package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Servicoservidor;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.bean.Servidor;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ServicoservidorDAO extends GenericDAO<Servicoservidor>{
	
	/**
	 * Retorna lista de servi�os de acordo com o servidor
	 * @param servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servicoservidor> find(Servidor servidor){
		return query().select("servicoservidor.cdservicoservidor, servicoservidor.nome")
					.from(Servicoservidor.class)
					.where("servicoservidor.servidor = ?", servidor)
					.orderBy("servicoservidor.nome asc")
					.list();
	}
	
	/**
	 * Retorna lista de servi�os do tipo SMTP de acordo com o servidor
	 * @param servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servicoservidor> findSmtp(Servidor servidor){
		Servicoservidortipo tipo = new Servicoservidortipo();
		tipo.setCdservicoservidortipo(Servicoservidortipo.SMTP);
		
		return query().select("servicoservidor.cdservicoservidor, servicoservidor.nome")
					.from(Servicoservidor.class)
					.where("servicoservidor.servidor = ?", servidor)
					.where("servicoservidor.servicoservidortipo = ?", tipo)
					.orderBy("servicoservidor.nome asc")
					.list();
	}
	
	/**
	 * Retorna lista de servi�os do tipo FTP de acordo com o servidor
	 * @param servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servicoservidor> findFtp(Servidor servidor){
		Servicoservidortipo tipo = new Servicoservidortipo();
		tipo.setCdservicoservidortipo(Servicoservidortipo.FTP);
		
		return query().select("servicoservidor.cdservicoservidor, servicoservidor.nome")
		.from(Servicoservidor.class)
		.where("servicoservidor.servidor = ?", servidor)
		.where("servicoservidor.servicoservidortipo = ?", tipo)
		.orderBy("servicoservidor.nome asc")
		.list();
	}

	/**
	 * Retorna lista de servi�os do tipo Banco de dados de acordo com o servidor
	 * @param servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servicoservidor> findBancodados(Servidor servidor){
		Servicoservidortipo tipo = new Servicoservidortipo();
		tipo.setCdservicoservidortipo(Servicoservidortipo.BANCO_DADOS);
		
		return query().select("servicoservidor.cdservicoservidor, servicoservidor.nome")
		.from(Servicoservidor.class)
		.where("servicoservidor.servidor = ?", servidor)
		.where("servicoservidor.servicoservidortipo = ?", tipo)
		.orderBy("servicoservidor.nome asc")
		.list();
	}

	/**
	 * M�todo que busca o servi�o servidor do tipo web de um determinado servidor
	 * 
	 * @param servidor
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Servicoservidor findByServidorAndServicoServidorTipoWeb(Servidor servidor) {
		return query()
			.select("servicoservidor.cdservicoservidor")
			.join("servicoservidor.servidor servidor")
			.join("servicoservidor.servicoservidortipo servicoservidortipo")
			.where("servidor = ?", servidor)
			.where("servicoservidortipo = ?", new Servicoservidortipo(Servicoservidortipo.WEB))
			.unique();
			
	}
	
}