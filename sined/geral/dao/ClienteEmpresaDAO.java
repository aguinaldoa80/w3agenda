package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.ClienteEmpresa;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;


public class ClienteEmpresaDAO extends GenericDAO<ClienteEmpresa>{
	
	public List<ClienteEmpresa> findByCliente(Cliente cliente) {
		if(cliente == null || cliente.getCdpessoa() == null){
			return new ArrayList<ClienteEmpresa>();
		}
		return query()
				.select("clienteEmpresa.cdclienteempresa, cliente.cdpessoa, cliente.nome, empresa.cdpessoa, empresa.nome, empresa.nomefantasia")
				.join("clienteEmpresa.cliente cliente")
				.join("clienteEmpresa.empresa empresa")
				.where("cliente=?", cliente)
				.orderBy("empresa.nome")
			 	.list();
	}

	public boolean haveClienteEmpresa(ClienteEmpresa clienteEmpresa) {
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.where("clienteEmpresa.cliente = ?", clienteEmpresa.getCliente())
				.where("clienteEmpresa.empresa = ?", clienteEmpresa.getEmpresa())
				.unique() > 0;
	}
}