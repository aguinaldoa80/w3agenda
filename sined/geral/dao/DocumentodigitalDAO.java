package br.com.linkcom.sined.geral.dao;

import java.io.File;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Documentodigital;
import br.com.linkcom.sined.geral.bean.enumeration.Documentodigitalsituacao;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.DocumentodigitalFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentodigitalDAO extends GenericDAO<Documentodigital>{
	
	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Documentodigital> query, FiltroListagem _filtro) {
		DocumentodigitalFiltro filtro = (DocumentodigitalFiltro) _filtro;
		
		query
			.select("documentodigital.cddocumentodigital, documentodigital.tipo, documentodigital.situacao, " +
					"cliente.cdpessoa, cliente.nome, cliente.cnpj, cliente.cpf, cliente.razaosocial, " +
					"oportunidade.cdoportunidade, oportunidade.nome, arquivo.cdarquivo, arquivo.nome, " +
					"empresa.nomefantasia, documentodigitalmodelo.nome")
			.join("documentodigital.empresa empresa")
			.join("documentodigital.arquivo arquivo")
			.join("documentodigital.documentodigitalmodelo documentodigitalmodelo")
			.leftOuterJoin("documentodigital.cliente cliente")
			.leftOuterJoin("documentodigital.oportunidade oportunidade")
			.where("documentodigital.tipo = ?", filtro.getTipo())
			.where("documentodigital.documentodigitalmodelo = ?", filtro.getDocumentodigitalmodelo())
			.where("documentodigital.cliente = ?", filtro.getCliente())
			.where("documentodigital.oportunidade = ?", filtro.getOportunidade())
			.openParentheses()
				.whereLikeIgnoreAll("cliente.nome", filtro.getDescricaotipo()).or()
				.whereLikeIgnoreAll("oportunidade.nome", filtro.getDescricaotipo()).or()
			.closeParentheses()
			.orderBy("documentodigital.cddocumentodigital desc");
		
		if(filtro.getListaSituacao() != null) {
			query.whereIn("documentodigital.situacao", Documentodigitalsituacao.listAndConcatenate(filtro.getListaSituacao()));
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Documentodigital> query) {
		query
			.select("documentodigital.cddocumentodigital, documentodigital.tipo, documentodigital.situacao, documentodigital.hash, " +
					"documentodigitalmodelo.cddocumentodigitalmodelo, " +
					"cliente.cdpessoa, cliente.nome, cliente.cnpj, cliente.cpf, cliente.razaosocial, " +
					"oportunidade.cdoportunidade, oportunidade.nome, " +
					"arquivo.cdarquivo, arquivo.nome, empresa.cdpessoa, empresa.nomefantasia, " +
					"listaDocumentodigitaldestinatario.cddocumentodigitaldestinatario, " +
					"listaDocumentodigitaldestinatario.dtenvio, listaDocumentodigitaldestinatario.token, " +
					"listaDocumentodigitaldestinatario.dtaceite, listaDocumentodigitaldestinatario.ipaceite, listaDocumentodigitaldestinatario.useragentaceite, " +
					"documentodigitalusuario.cddocumentodigitalusuario, documentodigitalusuario.email, documentodigitalusuario.nome, documentodigitalusuario.cpf, documentodigitalusuario.dtnascimento, " +
					"listaDocumentodigitalobservador.cddocumentodigitalobservador, listaDocumentodigitalobservador.email")
			.join("documentodigital.arquivo arquivo")
			.join("documentodigital.empresa empresa")
			.join("documentodigital.documentodigitalmodelo documentodigitalmodelo")
			.leftOuterJoin("documentodigital.cliente cliente")
			.leftOuterJoin("documentodigital.oportunidade oportunidade")
			.leftOuterJoin("documentodigital.listaDocumentodigitalobservador listaDocumentodigitalobservador")
			.leftOuterJoin("documentodigital.listaDocumentodigitaldestinatario listaDocumentodigitaldestinatario")
			.leftOuterJoin("listaDocumentodigitaldestinatario.documentodigitalusuario documentodigitalusuario")
			;
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Documentodigital documentodigital = (Documentodigital) save.getEntity();
		arquivoDAO.saveFile(documentodigital, "arquivo");
		
		Arquivo arquivo = documentodigital.getArquivo();
		if(arquivo != null && arquivo.getCdarquivo() != null){
			try {
				String caminhoArquivoCompleto = ArquivoDAO.getInstance().getCaminhoArquivoCompleto(arquivo);
				String hash = SinedUtil.getFileChecksum(new File(caminhoArquivoCompleto));
				documentodigital.setHash(hash);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		save.saveOrUpdateManaged("listaDocumentodigitaldestinatario");
		save.saveOrUpdateManaged("listaDocumentodigitalobservador");
	}

	public List<Documentodigital> findForEnvioAceiteDigital(String whereIn, Integer cddocumentodigitaldestinatario) {
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("documentodigital.cddocumentodigital, documentodigital.tipo, documentodigital.situacao, " +
							"cliente.cdpessoa, cliente.nome, documentodigitalmodelo.cddocumentodigitalmodelo, " +
							"reporttemplate.cdreporttemplate, reporttemplate.leiaute, " +
							"oportunidade.cdoportunidade, oportunidade.nome, " +
							"empresa.cdpessoa, empresa.nomefantasia, empresa.nome, empresa.email, empresa.emailfinanceiro, " +
							"listaDocumentodigitaldestinatario.cddocumentodigitaldestinatario, documentodigitalusuario.email, listaDocumentodigitaldestinatario.dtenvio, " +
							"arquivo.cdarquivo, documentodigitalusuario.cddocumentodigitalusuario, listaDocumentodigitaldestinatario.token")
					.join("documentodigital.arquivo arquivo")
					.join("documentodigital.empresa empresa")
					.join("documentodigital.documentodigitalmodelo documentodigitalmodelo")
					.leftOuterJoin("documentodigitalmodelo.reporttemplate reporttemplate")
					.leftOuterJoin("documentodigital.cliente cliente")
					.leftOuterJoin("documentodigital.oportunidade oportunidade")
					.leftOuterJoin("documentodigital.listaDocumentodigitaldestinatario listaDocumentodigitaldestinatario")
					.leftOuterJoin("listaDocumentodigitaldestinatario.documentodigitalusuario documentodigitalusuario")
					.whereIn("documentodigital.cddocumentodigital", whereIn)
					.where("listaDocumentodigitaldestinatario.cddocumentodigitaldestinatario = ?", cddocumentodigitaldestinatario)
					.list();
	}

	public void updateSituacao(Documentodigital documentodigital, Documentodigitalsituacao documentodigitalsituacao) {
		getHibernateTemplate().bulkUpdate("update Documentodigital d set d.situacao = ? where d = ?", new Object[]{documentodigitalsituacao, documentodigital});
	}

	public Documentodigital loadForEnvioEmailObservadores(Documentodigital documentodigital) {
		return query()
					.select("documentodigital.cddocumentodigital, documentodigital.tipo, documentodigital.situacao, documentodigital.hash, " +
							"empresa.cdpessoa, empresa.nome, empresa.email, empresa.emailfinanceiro, " +
							"cliente.cdpessoa, cliente.nome, " +
							"oportunidade.cdoportunidade, oportunidade.nome, " +
							"documentodigitalmodelo.cddocumentodigitalmodelo, documentodigitalmodelo.nome, " +
							"arquivo.cdarquivo, arquivo.nome, " +
							"listaDocumentodigitalobservador.email, listaDocumentodigitalobservador.cddocumentodigitalobservador")
					.join("documentodigital.empresa empresa")
					.join("documentodigital.documentodigitalmodelo documentodigitalmodelo")
					.join("documentodigital.arquivo arquivo")
					.leftOuterJoin("documentodigital.listaDocumentodigitalobservador listaDocumentodigitalobservador")
					.leftOuterJoin("documentodigital.oportunidade oportunidade")
					.leftOuterJoin("documentodigital.cliente cliente")
					.where("documentodigital = ?", documentodigital)
					.unique();
	}

}
