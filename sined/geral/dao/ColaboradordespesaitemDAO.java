package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Colaboradordespesaitem;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColaboradordespesaitemDAO extends GenericDAO<Colaboradordespesaitem>{
	
	public List<Colaboradordespesaitem> findByColaboradordespesa(Integer cdcolaboradordespesa, Date dtreferencia1, Date dtreferencia2){
		QueryBuilder<Colaboradordespesaitem> query = query()
			.join("colaboradordespesaitem.colaboradordespesa colaboradordespesa")
			.join("colaboradordespesa.colaborador colaborador")
			.where("colaborador.cdpessoa = ?",cdcolaboradordespesa)
			.where("colaboradordespesa.dtinsercao >= ?", dtreferencia1)
			.where("colaboradordespesa.dtinsercao <= ?", dtreferencia2);
		
		return query.list();
	}
	
}
