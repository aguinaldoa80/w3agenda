package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.NotaVenda;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotaVendaDAO extends GenericDAO<NotaVenda>{

	/**
	 * 
	 * M�todo para buscar a lista de NotaVenda por nota.
	 *
	 *@author Thiago Augusto
	 *@date 05/01/2012
	 * @param nota
	 * @return
	 */
	public List<NotaVenda> findByNota(String whereIn){
		return querySined()
		.setUseReadOnly(false)
		.select("notaVenda.cdnotavenda, venda.cdvenda, nota.cdNota, notaStatus.cdNotaStatus, vendasituacao.cdvendasituacao, expedicao.cdexpedicao, expedicao.expedicaosituacao, " +
				"pedidovendatipo.cdpedidovendatipo, pedidovenda.cdpedidovenda, pedidovendatipo.geracaocontareceberEnum ")
		.leftOuterJoin("notaVenda.venda venda")
		.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
		.leftOuterJoin("venda.pedidovenda pedidovenda")
		.leftOuterJoin("notaVenda.nota nota")
		.leftOuterJoin("nota.notaStatus notaStatus")
		.leftOuterJoin("venda.vendasituacao vendasituacao")
		.leftOuterJoin("notaVenda.expedicao expedicao")
		.whereIn("nota.cdNota", whereIn)
		.list();
	}
	
	public List<NotaVenda> findByNota(Nota nota){
		if(nota == null || nota.getCdNota() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("notaVenda.cdnotavenda, venda.cdvenda, venda.observacao, nota.cdNota, notaStatus.cdNotaStatus, notaTipo.cdNotaTipo, notaTipo.nome, vendasituacao.cdvendasituacao")
			.join("notaVenda.venda venda")
			.join("notaVenda.nota nota")
			.leftOuterJoin("nota.notaStatus notaStatus")
			.leftOuterJoin("nota.notaTipo notaTipo")
			.leftOuterJoin("venda.vendasituacao vendasituacao")
			.where("nota = ?", nota)
			.list();
	}
	
	public Boolean existNotaVenda(Nota nota) {
		if(nota == null || nota.getCdNota() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(NotaVenda.class)
			.join("notaVenda.venda venda")
			.join("notaVenda.nota nota")
			.where("nota = ?", nota)
			.unique() > 0;
	}

	/**
	 * M�todo que verifica se a venda tem notas emitidas
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existNotaEmitida(Venda venda, NotaTipo notaTipo) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
			.setUseReadOnly(false)
			.select("count(*)")
			.from(NotaVenda.class)
			.join("notaVenda.venda venda")
			.join("notaVenda.nota nota")
			.join("nota.notaTipo notaTipo")
			.leftOuterJoin("nota.notaStatus notaStatus")
			.where("venda = ?", venda)
			.where("notaTipo = ?", notaTipo)
			.where("notaStatus <> ?", NotaStatus.CANCELADA)
			.where("notaStatus <> ?", NotaStatus.NFE_DENEGADA)
			.where("nota.cdNota not in (select nfp.cdNota from Notafiscalproduto nfp where coalesce(nfp.origemCupom, false) = true)")
			.unique() > 0;
	}
	
	/**
	* M�todo que verifica se a expedi��o tem notas emitidas
	*
	* @param expedicao
	* @param notaTipo
	* @return
	* @since 18/01/2017
	* @author Luiz Fernando
	*/
	public Boolean existeNotaEmitida(Expedicao expedicao, NotaTipo notaTipo) {
		if(expedicao == null || expedicao.getCdexpedicao() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(NotaVenda.class)
			.join("notaVenda.expedicao expedicao")
			.join("notaVenda.nota nota")
			.join("nota.notaTipo notaTipo")
			.leftOuterJoin("nota.notaStatus notaStatus")
			.where("expedicao = ?", expedicao)
			.where("notaTipo = ?", notaTipo)
			.where("notaStatus <> ?", NotaStatus.CANCELADA)
			.where("notaStatus <> ?", NotaStatus.NFE_DENEGADA)
			.unique() > 0;
	}
	
	/**
	 * M�todo que verifica se o documento tem notas emitidas
	 *
	 * @param documento
	 * @param notaTipo
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existNotaEmitida(Documento documento, NotaTipo notaTipo) {
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(NotaVenda.class)
			.join("notaVenda.venda venda")
			.join("notaVenda.nota nota")
			.join("nota.notaTipo notaTipo")
			.leftOuterJoin("nota.notaStatus notaStatus")
			.leftOuterJoin("venda.listavendapagamento listavendapagamento")
			.leftOuterJoin("listavendapagamento.documento documento")
			.where("documento = ?", documento)
			.where("notaTipo = ?", notaTipo)
			.where("notaStatus <> ?", NotaStatus.CANCELADA)
			.unique() > 0;
	}
	
	/**
	 * 
	 * 
	 * @param venda
	 * @return
	 */
	public List<NotaVenda> findByVenda(Venda venda, boolean includeDenegada) {
		return query()
			.select("nota.numero, nota.dtEmissao, nota.cdNota")
			.join("notaVenda.nota nota")
			.join("notaVenda.venda venda")
			.join("nota.notaStatus notaStatus")
			.where("notaStatus <> ?", NotaStatus.CANCELADA)
			.where("notaStatus <> ?", NotaStatus.NFSE_CANCELANDO)
			.where("notaStatus <> ?", NotaStatus.NFE_CANCELANDO)
			.where("notaStatus <> ?", NotaStatus.NFE_DENEGADA, !includeDenegada)
			.where("venda = ?",venda)
			.list();
	}

	public boolean isNotaIndustrializacaoRetorno(Nota nota){
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.join("notaVenda.nota nota")
				.join("notaVenda.venda venda")
				.join("venda.pedidovenda pedidovenda")
				.join("pedidovenda.pedidovendatipo pedidovendatipo")
				.where("pedidovendatipo.gerarNotaIndustrializacaoRetorno=?", Boolean.TRUE)
				.where("nota=?", nota)
				.unique()>0L;				
	}
	
	/**
	* M�todo que verifica se existe nota emitida vinculada a venda
	*
	* @param venda
	* @return
	* @since 07/10/2015
	* @author Luiz Fernando
	*/
	public Boolean existeNotaEmitida(Venda venda, boolean desmarkAsReader) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Par�metro inv�lido.");
				
		return newQueryBuilderSined(Long.class)
			.setUseReadOnly(!desmarkAsReader)
			.select("count(*)")
			.from(NotaVenda.class)
			.join("notaVenda.venda venda")
			.join("notaVenda.nota nota")
			.join("nota.notaTipo notaTipo")
			.leftOuterJoin("nota.notaStatus notaStatus")
			.where("venda = ?", venda)
			.where("notaStatus <> ?", NotaStatus.CANCELADA)
			.where("notaStatus <> ?", NotaStatus.NFE_DENEGADA)
			.where("nota.cdNota not in (select nfp.cdNota from Notafiscalproduto nfp where coalesce(nfp.origemCupom, false) = true)")
			.unique() > 0;
	}
	
	public Boolean existeNotaEmitida(Venda venda, String whereInExpedicao) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(NotaVenda.class)
			.join("notaVenda.venda venda")
			.join("notaVenda.nota nota")
			.join("nota.notaTipo notaTipo")
			.leftOuterJoin("nota.notaStatus notaStatus")
			.where("venda = ?", venda)
			.whereIn("notaVenda.expedicao.cdexpedicao", whereInExpedicao)
			.where("notaStatus <> ?", NotaStatus.CANCELADA)
			.where("notaStatus <> ?", NotaStatus.NFE_DENEGADA)
			.unique() > 0;
	}
	
	/**
	* M�todo que verifica se a venda tem nota de servi�o e produto
	*
	* @param venda
	* @return
	* @since 03/12/2015
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public Boolean existeNFsENFe(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		String sqlNFe = " select count(distinct nv.cdnota) as qtde " + 
					 " from notavenda nv " +
					 " join nota n on n.cdnota = nv.cdnota " +
					 " where nv.cdvenda = " + venda.getCdvenda() +
					 " and n.cdnotastatus <> " + NotaStatus.CANCELADA.getCdNotaStatus() +
					 " and n.cdnotatipo = " + NotaTipo.NOTA_FISCAL_PRODUTO.getCdNotaTipo() +
					 " group by nv.cdvenda ";

		SinedUtil.markAsReader();
		List<Integer> listaNFe = getJdbcTemplate().query(sqlNFe, 
				new RowMapper() {
					public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
						return new Integer(rs.getInt("qtde"));
					}
				});
		
		String sqlNFSe = " select count(distinct nv.cdnota) as qtde " + 
					 " from notavenda nv " +
					 " join nota n on n.cdnota = nv.cdnota " +
					 " where nv.cdvenda = " + venda.getCdvenda() +
					 " and n.cdnotastatus <> " + NotaStatus.CANCELADA.getCdNotaStatus() +
					 " and n.cdnotatipo = " + NotaTipo.NOTA_FISCAL_SERVICO.getCdNotaTipo() +
					 " group by nv.cdvenda ";

		SinedUtil.markAsReader();
		List<Integer> listaNFSe = getJdbcTemplate().query(sqlNFSe, 
				new RowMapper() {
					public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
						return new Integer(rs.getInt("qtde"));
					}
				});
		
		return listaNFe != null && listaNFe.size() > 0 && listaNFSe != null && listaNFSe.size() > 0;
	}

	@SuppressWarnings("unchecked")
	public String getWhereInVenda(Nota nota) {
		if(nota == null || nota.getCdNota() == null)
			return null;
		
		String sql = " select distinct nv.cdvenda " +
					 " from notavenda nv " +
					 " join nota n on n.cdnota = nv.cdnota " +
					 " where n.cdnota in (" + nota.getCdNota() + ") ";

		SinedUtil.markAsReader();
		List<Integer> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Integer(rs.getInt("cdvenda"));
			}
		});
		
		return list != null ? CollectionsUtil.concatenate(list, ",") : null;
	}
}
