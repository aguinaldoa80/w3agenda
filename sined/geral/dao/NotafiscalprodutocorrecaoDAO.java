package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutocorrecao;
import br.com.linkcom.sined.geral.bean.enumeration.Notafiscalprodutocorrecaosituacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotafiscalprodutocorrecaoDAO extends GenericDAO<Notafiscalprodutocorrecao> {

	@Override
	public void updateEntradaQuery(QueryBuilder<Notafiscalprodutocorrecao> query) {
		query
			.leftOuterJoinFetch("notafiscalprodutocorrecao.arquivoenvio arquivoenvio")
			.leftOuterJoinFetch("notafiscalprodutocorrecao.arquivoretorno arquivoretorno")
			.leftOuterJoinFetch("notafiscalprodutocorrecao.arquivoxml arquivoxml")
			.leftOuterJoinFetch("notafiscalprodutocorrecao.notafiscalproduto notafiscalproduto");
	}
	
	/**
	 * Busca as cartas de corre��es de uma NF.
	 *
	 * @param nf
	 * @return
	 * @since 17/09/2012
	 * @author Rodrigo Freitas
	 */
	public List<Notafiscalprodutocorrecao> findByNotafiscalproduto(Notafiscalproduto nf) {
		return query()
					.leftOuterJoinFetch("notafiscalprodutocorrecao.arquivoenvio arquivoenvio")
					.leftOuterJoinFetch("notafiscalprodutocorrecao.arquivoretorno arquivoretorno")
					.leftOuterJoinFetch("notafiscalprodutocorrecao.arquivoxml arquivoxml")
					.leftOuterJoin("notafiscalprodutocorrecao.notafiscalproduto notafiscalproduto")
					.where("notafiscalproduto = ?", nf)
					.orderBy("notafiscalprodutocorrecao.data desc")
					.list();
	}

	/**
	 * Marca a flag do emissor desktop.
	 *
	 * @param whereInCartacorrecao
	 * @since 17/09/2012
	 * @author Rodrigo Freitas
	 */
	public void marcarFlag(String whereInCartacorrecao) {
		getHibernateTemplate()
					.bulkUpdate("update Notafiscalprodutocorrecao i set i.enviando = ? where i.cdnotafiscalprodutocorrecao in (" + whereInCartacorrecao + ")", 
							new Object[]{Boolean.TRUE});
	}

	/**
	 * Busca as novas cartas de corre��o para ser enviada para a receita
	 *
	 * @param empresa
	 * @return
	 * @since 17/09/2012
	 * @author Rodrigo Freitas
	 */
	public List<Notafiscalprodutocorrecao> findNovasCorrecoes(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("A empresa n�o pode ser nula.");
		}
		return querySined()
					
					.select("notafiscalprodutocorrecao.cdnotafiscalprodutocorrecao, arquivoenvio.cdarquivo, notafiscalproduto.cdNota")
					.leftOuterJoin("notafiscalprodutocorrecao.arquivoenvio arquivoenvio")
					.leftOuterJoin("notafiscalprodutocorrecao.notafiscalproduto notafiscalproduto")
					.leftOuterJoin("notafiscalproduto.empresa empresa")
					.where("empresa = ?", empresa)
					.openParentheses()
					.where("notafiscalprodutocorrecao.enviando = ?", Boolean.FALSE)
					.or()
					.where("notafiscalprodutocorrecao.enviando is null")
					.closeParentheses()
					.list();
	}

	/**
	 * Atualiza o arquivo de retorno da receita
	 *
	 * @param notafiscalprodutocorrecao
	 * @since 18/09/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoretorno(Notafiscalprodutocorrecao notafiscalprodutocorrecao) {
		if(notafiscalprodutocorrecao == null || notafiscalprodutocorrecao.getCdnotafiscalprodutocorrecao() == null){
			throw new SinedException("Corre��o n�o pode ser nula.");
		}
		if(notafiscalprodutocorrecao.getArquivoretorno() == null || notafiscalprodutocorrecao.getArquivoretorno().getCdarquivo() == null){
			throw new SinedException("Arquivo de corre��o n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Notafiscalprodutocorrecao i set i.arquivoretorno = ? where i.id = ?", new Object[]{
				notafiscalprodutocorrecao.getArquivoretorno(), notafiscalprodutocorrecao.getCdnotafiscalprodutocorrecao()
		});
	}

	/**
	 * Atualiza o motivo da corre��o
	 *
	 * @param notafiscalprodutocorrecao
	 * @param motivo
	 * @since 18/09/2012
	 * @author Rodrigo Freitas
	 */
	public void updateMotivo(Notafiscalprodutocorrecao notafiscalprodutocorrecao, String motivo) {
		if(notafiscalprodutocorrecao == null || notafiscalprodutocorrecao.getCdnotafiscalprodutocorrecao() == null){
			throw new SinedException("Corre��o n�o pode ser nula.");
		}
		getHibernateTemplate().bulkUpdate("update Notafiscalprodutocorrecao i set i.motivo = ? where i.id = ?", new Object[]{
				motivo, notafiscalprodutocorrecao.getCdnotafiscalprodutocorrecao()
		});
	}

	/**
	 * Atualiza a situa��o da corre��o
	 *
	 * @param notafiscalprodutocorrecao
	 * @param situacao
	 * @since 18/09/2012
	 * @author Rodrigo Freitas
	 */
	public void updateSituacao(Notafiscalprodutocorrecao notafiscalprodutocorrecao, Notafiscalprodutocorrecaosituacao situacao) {
		if(notafiscalprodutocorrecao == null || notafiscalprodutocorrecao.getCdnotafiscalprodutocorrecao() == null){
			throw new SinedException("Corre��o n�o pode ser nula.");
		}
		getHibernateTemplate().bulkUpdate("update Notafiscalprodutocorrecao i set i.notafiscalprodutocorrecaosituacao = ? where i.id = ?", new Object[]{
				situacao, notafiscalprodutocorrecao.getCdnotafiscalprodutocorrecao()
		});
	}

	/**
	 * Atualiza o arquivo de distribui��o da corre��o
	 *
	 * @param notafiscalprodutocorrecao
	 * @since 18/09/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoxml(Notafiscalprodutocorrecao notafiscalprodutocorrecao) {
		if(notafiscalprodutocorrecao == null || notafiscalprodutocorrecao.getCdnotafiscalprodutocorrecao() == null){
			throw new SinedException("Corre��o n�o pode ser nula.");
		}
		if(notafiscalprodutocorrecao.getArquivoxml() == null || notafiscalprodutocorrecao.getArquivoxml().getCdarquivo() == null){
			throw new SinedException("Arquivo de corre��o n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Notafiscalprodutocorrecao i set i.arquivoxml = ? where i.id = ?", new Object[]{
				notafiscalprodutocorrecao.getArquivoxml(), notafiscalprodutocorrecao.getCdnotafiscalprodutocorrecao()
		});
	}

	/**
	 * Carrega o registro com a empresa preenchida.
	 *
	 * @param notafiscalprodutocorrecao
	 * @return
	 * @since 18/09/2012
	 * @author Rodrigo Freitas
	 */
	public Notafiscalprodutocorrecao loadWithEmpresa(Notafiscalprodutocorrecao notafiscalprodutocorrecao) {
		if(notafiscalprodutocorrecao == null || notafiscalprodutocorrecao.getCdnotafiscalprodutocorrecao() == null){
			throw new SinedException("Corre��o n�o pode ser nula.");
		}
		return query()
					.select("notafiscalprodutocorrecao.cdnotafiscalprodutocorrecao, empresa.cdpessoa")
					.join("notafiscalprodutocorrecao.notafiscalproduto notafiscalproduto")
					.join("notafiscalproduto.empresa empresa")
					.where("notafiscalprodutocorrecao = ?", notafiscalprodutocorrecao)
					.unique();
	}

	public Long getProximoContadorCartaCorrecao(Integer cdnotafiscalproduto) {
		SinedUtil.markAsReader();
		return getJdbcTemplate().queryForLong("SELECT MAX(nc.sequencialevento) + 1 FROM notafiscalprodutocorrecao nc WHERE nc.cdnotafiscalproduto = ? AND nc.notafiscalprodutocorrecaosituacao in (0,2)", new Object[]{cdnotafiscalproduto});
	}

	/**
	 * M�todo que verifica se a nota tem
	 * carta de corre��o
	 * @return
	 */
	public Boolean haveCartaCorrecao(Integer cdNota) {
		return newQueryBuilderSined(Long.class)
	    	.select("count(*)")
	    	.from(Notafiscalprodutocorrecao.class)
	    	.leftOuterJoin("notafiscalprodutocorrecao.notafiscalproduto notafiscalproduto")
	    	.where("notafiscalproduto.cdNota = ?", cdNota)
	     	.unique() > 0;
	}
}
