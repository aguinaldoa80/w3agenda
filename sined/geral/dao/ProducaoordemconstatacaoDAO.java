package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemconstatacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoordemconstatacaoDAO extends GenericDAO<Producaoordemconstatacao>{
	
	public List<Producaoordemconstatacao> findByProducaoordem(Producaoordem producaoordem) {
		return query()
				.select("producaoordemconstatacao.cdproducaoordemconstatacao, motivodevolucao.cdmotivodevolucao, producaoordemconstatacao.observacao, motivodevolucao.descricao")
				.leftOuterJoin("producaoordemconstatacao.motivodevolucao motivodevolucao")
				.where("producaoordemconstatacao.producaoordem=?", producaoordem)
				.list();
	}
	
	public List<Producaoordemconstatacao> findForDelete(Producaoordem producaoordem, String whereIn) {
		if(whereIn == null || whereIn.equals("") || producaoordem == null || producaoordem.getCdproducaoordem() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("producaoordemconstatacao.cdproducaoordemconstatacao, producaoordem.cdproducaoordem")
					.join("producaoordemconstatacao.producaoordem producaoordem")
					.where("producaoordemconstatacao.cdproducaoordemconstatacao not in (" +whereIn+")")
					.where("producaoordem = ?", producaoordem)
					.list();
	}
	
}
