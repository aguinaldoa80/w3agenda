package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("formapagamento.nome")
public class FormapagamentoDAO extends GenericDAO<Formapagamento> {
	
	
	/**
	 * Lista somente a forma de pagamento de cr�dito
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Formapagamento> findCredito(){
		return query()
					.select("formapagamento.cdformapagamento, formapagamento.nome")
					.where("formapagamento.credito = ?",Boolean.TRUE)
					.orderBy("formapagamento.nome")
					.list();
	}
	
	/**
	 * Lista somente a forma de pagamento de d�bito
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Formapagamento> findDebito(){
		return query()
					.select("formapagamento.cdformapagamento, formapagamento.nome")
					.where("formapagamento.debito = ?",Boolean.TRUE)
					.orderBy("formapagamento.nome")
					.list();
	}
	
	/* singleton */
	private static FormapagamentoDAO instance;
	public static FormapagamentoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(FormapagamentoDAO.class);
		}
		return instance;
	}

	/**
	 * Verifica se a flag de concilia��o autom�tica est� marcada.
	 *
	 * @param formapagamento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/07/2014
	 */
	public boolean isConciliacaoAuto(Formapagamento formapagamento) {
		if(formapagamento == null || formapagamento.getCdformapagamento() == null){
			throw new SinedException("A forma de pagamento n�o pode ser nula.");
		}
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Formapagamento.class)
					.where("formapagamento.conciliacaoautomatica = ?", Boolean.TRUE)
					.where("formapagamento = ?", formapagamento)
					.unique() > 0;
	}

	public List<Formapagamento> findForPVOffline() {
		return query()
			.select("formapagamento.cdformapagamento, formapagamento.nome")
			.list();
	}
	
	public List<Formapagamento> findForAndroid(String whereIn) {
		return query()
			.select("formapagamento.cdformapagamento, formapagamento.nome")
			.whereIn("formapagamento.cdformapagamento", whereIn)
			.list();
	}

	/**
	 * Lista somente a forma de pagamento de cr�dito e d�bito
	 * 
	 * @return
	 * @author Mairon Cezar
	 */
	public List<Formapagamento> findCreditoAndDebito(){
		return query()
					.select("formapagamento.cdformapagamento, formapagamento.nome")
					.openParentheses()
						.where("formapagamento.credito = ?",Boolean.TRUE)
						.or()
						.where("formapagamento.debito = ?",Boolean.TRUE)
					.closeParentheses()
					.orderBy("formapagamento.nome")
					.list();
	}
}
