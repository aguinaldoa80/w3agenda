package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Questionarioresposta;

public class QuestionariorespostaDAO extends GenericDAO<Questionarioresposta>{
	
	@Override
	public void saveOrUpdateNoUseTransaction(Questionarioresposta bean) {
		super.saveOrUpdateNoUseTransaction(bean);
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Questionarioresposta> query) {
		super.updateEntradaQuery(query);
	}
	
	public Questionarioresposta loadPontuacaoByResposta (String resposta){
		return query()
				.select("questionarioresposta.pontuacao")
				.where("questionarioresposta.resposta = ?", resposta)
				.unique();
	}

}
