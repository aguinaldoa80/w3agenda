package br.com.linkcom.sined.geral.dao;


import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.OperacaoContabilTipoLancamento;
import br.com.linkcom.sined.geral.bean.Operacaocontabil;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.OperacaocontabilFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("operacaocontabil.nome")
public class OperacaocontabilDAO extends GenericDAO<Operacaocontabil>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Operacaocontabil> query, FiltroListagem _filtro){
		
		OperacaocontabilFiltro filtro = (OperacaocontabilFiltro) _filtro;
		
		query
			.select("distinct operacaocontabil.cdoperacaocontabil, operacaocontabil.tipo, operacaocontabil.nome," +
					"tipoLancamento.cdOperacaoContabilTipoLancamento, tipoLancamento.nome, tipoLancamento.movimentacaocontabilTipoLancamento, " +
					"contagerencial.cdcontagerencial, contagerencial.nome")
			.leftOuterJoin("operacaocontabil.contagerencial contagerencial")
			.leftOuterJoin("operacaocontabil.tipoLancamento tipoLancamento")
			.whereLikeIgnoreAll("operacaocontabil.nome", filtro.getNome())
			.where("operacaocontabil.tipoLancamento=?", filtro.getTipoLancamento())
			.where("contagerencial=?", filtro.getContagerencial())
			.orderBy("operacaocontabil.nome");
		
		if (filtro.getContacontabildebito()!=null){
			query.where("exists (select 1" +
						"		 from Operacaocontabil ocdebito" +
						"		 join ocdebito.listaDebito listaDebito" +
						"		 join listaDebito.contaContabil contaContabil" +
						"		 where operacaocontabil.cdoperacaocontabil=ocdebito.cdoperacaocontabil" +
						"		 and contaContabil=?)", filtro.getContacontabildebito());
		}		
		
		if (filtro.getContacontabilcredito()!=null){
			query.where("exists (select 1" +
						"		 from Operacaocontabil occredito" +
						"		 join occredito.listaCredito listaCredito" +
						"		 join listaCredito.contaContabil contaContabil" +
						"		 where operacaocontabil.cdoperacaocontabil=occredito.cdoperacaocontabil" +
						"		 and contaContabil=?)", filtro.getContacontabilcredito());
		}		
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Operacaocontabil> query){
		query
			.select("operacaocontabil.cdoperacaocontabil, operacaocontabil.nome, operacaocontabil.tipo, " +
					"operacaocontabil.historico, operacaocontabil.dtaltera, operacaocontabil.cdusuarioaltera, empresa.nomefantasia , empresa.cdpessoa, " +
					"operacaocontabil.codigointegracao, contagerencial.cdcontagerencial, contagerencial.nome, operacaocontabil.agruparvalormesmaconta," +
					"operacaocontabil.origemMovimentacaoEstoque, operacaocontabil.movimentacaoFinanceiraVinculo, " +
					"tipoLancamento.cdOperacaoContabilTipoLancamento, tipoLancamento.nome, naturezaoperacao.cdnaturezaoperacao, listaOperacaoContabilNaturezaOperacao.cdoperacaoContabilNaturezaOperacao, " +
					"formaPagamento.cdformapagamento")
			.leftOuterJoin("operacaocontabil.contagerencial contagerencial")
			.leftOuterJoin("operacaocontabil.empresa empresa")
			.leftOuterJoin("operacaocontabil.formaPagamento formaPagamento")
			.leftOuterJoin("operacaocontabil.tipoLancamento tipoLancamento")
			.leftOuterJoin("operacaocontabil.listaOperacaoContabilNaturezaOperacao listaOperacaoContabilNaturezaOperacao")
			.leftOuterJoin("listaOperacaoContabilNaturezaOperacao.naturezaoperacao naturezaoperacao");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaDebito", "operacaocontabildebito");
		save.saveOrUpdateManaged("listaCredito", "operacaocontabilcredito");
		save.saveOrUpdateManaged("listaEvento");
		save.saveOrUpdateManaged("listaOperacaoContabilNaturezaOperacao", "operacaocontabil");
		super.updateSaveOrUpdate(save);
	}
	
	public Operacaocontabil load(Object bean, String property){
		return query()
				.select(property)
				.from(bean.getClass())
				.join(Util.strings.uncaptalize(bean.getClass().getSimpleName()) + "." + property + " " + property)
				.where(Util.strings.uncaptalize(bean.getClass().getSimpleName()) + "=?", bean)
				.unique();
	}
	
	public List<Operacaocontabil> findForAutocomplete(String q){
		return query()
				.openParentheses()
					.whereLikeIgnoreAll("operacaocontabil.cdoperacaocontabil", q)
					.or()
					.whereLikeIgnoreAll("operacaocontabil.nome", q)
				.closeParentheses()
				.orderBy("operacaocontabil.nome")
				.list();
	}
	
	public List<Operacaocontabil> findForGerarLancamentoContabil(GerarLancamentoContabilFiltro filtro){
		QueryBuilder<Operacaocontabil> queryBuilder = query();
		
		queryBuilder.select("operacaocontabil.cdoperacaocontabil, operacaocontabil.nome, operacaocontabil.tipo, operacaocontabil.origemMovimentacaoEstoque, operacaocontabil.movimentacaoFinanceiraVinculo, " +
							"operacaocontabil.historico, operacaocontabil.codigointegracao, operacaocontabil.agruparvalormesmaconta, contagerencial.cdcontagerencial, contagerencial.nome," +
							"listaDebito.cdoperacaocontabildebitocredito, listaDebito.tipo, listaDebito.controle, listaDebito.centrocusto, listaDebito.projeto, contacontabildebito.cdcontacontabil, contacontabildebito.nome, listaDebito.historico, listaDebito.formula, listaDebito.codigointegracao, " +
							"listaCredito.cdoperacaocontabildebitocredito, listaCredito.tipo, listaCredito.controle, listaCredito.centrocusto, listaCredito.projeto, contacontabilcredito.cdcontacontabil, contacontabilcredito.nome, listaCredito.historico, listaCredito.formula, listaCredito.codigointegracao, " +
							"formaPagamento.cdformapagamento, tipoLancamento.cdOperacaoContabilTipoLancamento, tipoLancamento.nome, tipoLancamento.movimentacaocontabilTipoLancamento, listaOperacaoContabilNaturezaOperacao.cdoperacaoContabilNaturezaOperacao, " +
							"naturezaoperacao.cdnaturezaoperacao ");
		queryBuilder.leftOuterJoin("operacaocontabil.tipoLancamento tipoLancamento");
		queryBuilder.leftOuterJoin("operacaocontabil.contagerencial contagerencial");
		queryBuilder.leftOuterJoin("operacaocontabil.listaDebito listaDebito");
		queryBuilder.leftOuterJoin("listaDebito.contaContabil contacontabildebito");
		queryBuilder.leftOuterJoin("operacaocontabil.listaCredito listaCredito");
		queryBuilder.leftOuterJoin("operacaocontabil.formaPagamento formaPagamento");
		queryBuilder.leftOuterJoin("listaCredito.contaContabil contacontabilcredito");
		queryBuilder.leftOuterJoin("operacaocontabil.empresa empresa");
		queryBuilder.leftOuterJoin("operacaocontabil.listaOperacaoContabilNaturezaOperacao listaOperacaoContabilNaturezaOperacao");
		queryBuilder.leftOuterJoin("listaOperacaoContabilNaturezaOperacao.naturezaoperacao naturezaoperacao");
		
		queryBuilder.openParentheses()
			.where("empresa = ?", filtro.getEmpresa())
			.or()
			.where("empresa is null")
		.closeParentheses();
		
//		List<OperacaoContabilTipoLancamento> listaTipoLancamento = OperacaoContabilTipoLancamento.getListaOrdenada(filtro.getListaTipoLancamento());
		
		if (SinedUtil.isListEmpty(filtro.getListaTipoLancamento())){
			queryBuilder.where("1=2");
		} else {
			queryBuilder.openParentheses();
			for (OperacaoContabilTipoLancamento tipoLancamento: filtro.getListaTipoLancamento()){
				queryBuilder.where("tipoLancamento = ?", tipoLancamento);
				queryBuilder.or();
			}
			queryBuilder.closeParentheses();			
		}
		
		return queryBuilder
				.orderBy("tipoLancamento.ordem, naturezaoperacao.cdnaturezaoperacao, operacaocontabil.cdoperacaocontabil")
				.list();
	}
	
	public boolean isPossuiTipoLancamento(Operacaocontabil operacaocontabil){
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.where("operacaocontabil.cdoperacaocontabil<>?", operacaocontabil.getCdoperacaocontabil())
				.where("operacaocontabil.tipoLancamento=?", operacaocontabil.getTipoLancamento())
				.unique()>0;
	}

	public List<Operacaocontabil> findForCSV(FiltroListagem filtro) {
		QueryBuilder<Operacaocontabil> query = query();
		updateListagemQuery(query, filtro);		
		return query.list();
	}
}