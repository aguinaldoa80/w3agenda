package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Fornecedorempresa;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FornecedorempresaDAO extends GenericDAO<Fornecedorempresa> {

	/**
	 * Carrega a lista de fornecedorempresa a partir de um fornecedor.
	 * 
	 * @param form
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Fornecedorempresa> findByFornecedor(Fornecedor form) {
		if (form == null || form.getCdpessoa() == null) {
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		}
		return query()
					.select("fornecedorempresa.cdfornecedorempresa, empresa.cdpessoa, empresa.nomefantasia")
					.join("fornecedorempresa.empresa empresa")
					.where("fornecedorempresa.fornecedor = ?",form)
					.list();
	}

	
	
	
}
