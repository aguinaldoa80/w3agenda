package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendaitemadicional;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoagendaitemadicionalDAO extends GenericDAO<Producaoagendaitemadicional> {

	public List<Producaoagendaitemadicional> findByProducaoagenda(Producaoagenda producaoagenda) {
		if(producaoagenda == null || producaoagenda.getCdproducaoagenda() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("producaoagendaitemadicional.cdproducaoagendaitemadicional, producaoagenda.cdproducaoagenda," +
							"material.cdmaterial, material.nome, producaoagendaitemadicional.quantidade, unidademedida.cdunidademedida, unidademedida.simbolo, pneu.cdpneu")
					.join("producaoagendaitemadicional.producaoagenda producaoagenda")
					.join("producaoagendaitemadicional.material material")
					.leftOuterJoin("producaoagendaitemadicional.pneu pneu")
					.leftOuterJoin("material.unidademedida unidademedida")
					.where("producaoagenda = ?", producaoagenda)
					.list();
	}
}
