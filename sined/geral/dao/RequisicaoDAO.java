package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Agendainteracao;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Materialrequisicao;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Requisicaoestado;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.MaterialrequisicaoService;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.RequisicaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

public class RequisicaoDAO extends br.com.linkcom.sined.util.neo.persistence.GenericDAO<Requisicao> {
	
	private EmpresaService empresaService;
	private MaterialrequisicaoService materialrequisicaoService;
	
	public void setMaterialrequisicaoService(
			MaterialrequisicaoService materialrequisicaoService) {
		this.materialrequisicaoService = materialrequisicaoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Requisicao> query,FiltroListagem _filtro) {
		RequisicaoFiltro filtro = (RequisicaoFiltro) _filtro;
		ArrayList<String> ignoreJoin =  new ArrayList<String>();

		if(filtro.getOrdemservico() != null && !filtro.getOrdemservico().equals("")){
			while(filtro.getOrdemservico().indexOf(",,") != -1){
				filtro.setOrdemservico(filtro.getOrdemservico().replaceAll(",,", ","));
			}
			
			if(filtro.getOrdemservico().substring(0, 1).equals(",")){
				filtro.setOrdemservico(filtro.getOrdemservico().substring(1, filtro.getOrdemservico().length()));
			}
			
			if(filtro.getOrdemservico().substring(filtro.getOrdemservico().length() - 1, filtro.getOrdemservico().length()).equals(",")){
				filtro.setOrdemservico(filtro.getOrdemservico().substring(0, filtro.getOrdemservico().length()-1));
			}
		}
		
		query.select("distinct requisicao.cdrequisicao, requisicao.ordemservico, requisicao.dtrequisicao, requisicao.dtprevisao, requisicao.descricao, " +
				"requisicaoprioridade.cdrequisicaoprioridade, requisicaoprioridade.descricao, requisicao.qtde, " +
				"requisicaoestado.cdrequisicaoestado, requisicaoestado.descricao, cliente.cdpessoa, " +
				"cliente.nome, contrato.cdcontrato, contrato.descricao, requisicao.faturar, clienteReq.nome, clienteReq.cdpessoa, " +
				"contato.nome, contato.cdpessoa, contato.emailcontato, notaFiscalServico.cdNota ")
		
		.join("requisicao.cliente clienteReq")
		.join("requisicao.requisicaoprioridade requisicaoprioridade")
		.join("requisicao.requisicaoestado requisicaoestado")
		.leftOuterJoin("requisicao.notaFiscalServico notaFiscalServico")
		.leftOuterJoin("requisicao.contato contato")
		.leftOuterJoin("requisicao.contrato contrato")
		.leftOuterJoin("contrato.cliente cliente");

		if(filtro.getColaboradorresponsavel() != null){
			query.join("requisicao.colaboradorresponsavel colaboradorresponsavel");
		}
		
		if(filtro.getEmpresa() != null){
			query.leftOuterJoin("requisicao.empresa empresa");
		}
		
		if(filtro.getTipo() != null){
			query.leftOuterJoin("requisicao.atividadetipo atividadetipo");
			
		}
		
		if(filtro.getObservacao() != null && !filtro.getObservacao().trim().isEmpty()){
			query.leftOuterJoin("requisicao.listaMateriaisrequisicao listaMateriaisrequisicao")
				.leftOuterJoin("requisicao.listaRequisicaohistorico listaRequisicaohistorico");
			ignoreJoin.add("listaMateriaisrequisicao");
			ignoreJoin.add("listaRequisicaohistorico");
		}else{
			if(filtro.getUsuariohistorico() != null){
				query.leftOuterJoin("requisicao.listaRequisicaohistorico listaRequisicaohistorico");
				ignoreJoin.add("listaRequisicaohistorico");
			}
			
			if(filtro.getMaterialservico() != null || filtro.getDtiniciodisponibilizacao() != null || filtro.getDtfimdisponibilizacao() != null
					|| filtro.getDtinicioretirada() != null || filtro.getDtfimretirada() != null){
				query.leftOuterJoin("requisicao.listaMateriaisrequisicao listaMateriaisrequisicao");
				ignoreJoin.add("listaMateriaisrequisicao");
			}
		}

		if(filtro.getAtividadetipoitem() != null && filtro.getValor() != null){
			query.leftOuterJoin("requisicao.listaItem listaItem")
				.leftOuterJoin("listaItem.atividadetipoitem atividadetipoitem");
			ignoreJoin.add("listaItem");
			ignoreJoin.add("atividadetipoitem");
		}

		if(filtro.getProjeto() != null && filtro.getProjeto().getCdprojeto() != null){
			query.leftOuterJoin("requisicao.projeto projetoReq")
				.leftOuterJoin("contrato.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.leftOuterJoin("listaRateioitem.projeto projeto");
			ignoreJoin.add("rateio");
			ignoreJoin.add("listaRateioitem");
			ignoreJoin.add("projeto");
		}
		
		if(!ignoreJoin.isEmpty()){
			query.ignoreJoin(ignoreJoin.toArray(new String[ignoreJoin.size()]));
		}
		
		if(filtro.getMostrarAutorizacao() != null){
			if(filtro.getMostrarAutorizacao()){
				query
					.where("(" +
							"select atestado.cdrequisicaoestado " +
							"from Autorizacaotrabalho at " +
							"join at.autorizacaoestado atestado " +
							"join at.requisicao atrequisicao " +
							"where atrequisicao.cdrequisicao = requisicao.cdrequisicao " +
							"and at.cdautorizacaotrabalho = (select max(at2.cdautorizacaotrabalho) from Autorizacaotrabalho at2 join at2.requisicao at2requisicao where at2requisicao.cdrequisicao = requisicao.cdrequisicao) " +
							") in (" + Requisicaoestado.CONCLUIDA + "," + Requisicaoestado.CANCELADA + ") ");
			} else {
				query
					.where("(" +
							"select atestado.cdrequisicaoestado " +
							"from Autorizacaotrabalho at " +
							"join at.autorizacaoestado atestado " +
							"join at.requisicao atrequisicao " +
							"where atrequisicao.cdrequisicao = requisicao.cdrequisicao " +
							"and at.cdautorizacaotrabalho = (select max(at2.cdautorizacaotrabalho) from Autorizacaotrabalho at2 join at2.requisicao at2requisicao where at2requisicao.cdrequisicao = requisicao.cdrequisicao) " +
							") not in (" + Requisicaoestado.CONCLUIDA + "," + Requisicaoestado.CANCELADA + ") ");
			}
		}
		if(filtro.getTipoiteracao() != null){
			query.where("exists (select at.cdautorizacaotrabalho " +
								"from Autorizacaotrabalho at " +
								"join at.requisicao req " +
								"where req.cdrequisicao = requisicao.cdrequisicao and at.tipoiteracao = ?)", filtro.getTipoiteracao());
		}
		if(filtro.getListasituacoes() != null && !filtro.getListasituacoes().isEmpty())
			query.whereIn("requisicaoestado", CollectionsUtil.listAndConcatenate(filtro.getListasituacoes(), "cdrequisicaoestado",","));
		if(filtro.getProjeto() != null && filtro.getProjeto().getCdprojeto() != null){
			query.openParentheses();
			query.where("projeto = ?", filtro.getProjeto());
			query.or();
			query.where("projetoReq = ?", filtro.getProjeto());
			query.closeParentheses();
		}
		if(filtro.getMostrar() != null){
			query.where("requisicao.faturar is true");
			query.whereIn("requisicaoestado.cdrequisicaoestado", Requisicaoestado.CONCLUIDA.toString() + " , " + Requisicaoestado.VISTO.toString());
		}
		if(filtro.getListaRequisicaoprioridade() != null && !filtro.getListaRequisicaoprioridade().isEmpty())
			query.whereIn("requisicaoprioridade", CollectionsUtil.listAndConcatenate(filtro.getListaRequisicaoprioridade(), "cdrequisicaoprioridade",","));
		
		if(filtro.getOrdemservico() != null && !filtro.getOrdemservico().equals("")){
			query
				.openParentheses()
				.whereIn("requisicao.ordemservico", SinedUtil.montaWhereInString(filtro.getOrdemservico()))
				.or()
				.whereIn("requisicao.cdrequisicao", SinedUtil.montaWhereInInteger(filtro.getOrdemservico()))
				.closeParentheses();
		}
		if(filtro.getAtividadetipoitem() != null && filtro.getValor() != null){
			query
				.where("atividadetipoitem = ?", filtro.getAtividadetipoitem())
				.whereLikeIgnoreAll("listaItem.valor", filtro.getValor());
		}
		if(filtro.getObservacao() != null && !filtro.getObservacao().equals("")){
			query
				.openParentheses()
				.whereLikeIgnoreAll("listaRequisicaohistorico.observacao", filtro.getObservacao())
				.or()
				.whereLikeIgnoreAll("listaMateriaisrequisicao.observacao", filtro.getObservacao())
				.closeParentheses();
		}
		
		query
			.where("requisicaoprioridade = ?", filtro.getRequisicaoprioridade())
			.where("empresa = ?", filtro.getEmpresa())
			.where("contrato =?", filtro.getContrato())
			.where("clienteReq = ?", filtro.getCliente())
			.whereLikeIgnoreAll("clienteReq.nome", filtro.getNomeCliente())
			.whereLikeIgnoreAll("listaMateriaisrequisicao.material.nome", filtro.getMaterialservico())
			.where("requisicao.dtrequisicao >= ?",filtro.getDtinicio())
			.where("requisicao.dtrequisicao <= ?",filtro.getDtfim())
			.where("requisicao.dtprevisao >= ?", filtro.getDtinicioprevisao())
			.where("requisicao.dtprevisao <= ?", filtro.getDtfimprevisao())
			.where("requisicao.dtconclusao >= ?",filtro.getDtinicioconclusao())
			.where("requisicao.dtconclusao <= ?",filtro.getDtfimconclusao())
			.where("colaboradorresponsavel = ?", filtro.getColaboradorresponsavel())
			.where("atividadetipo = ?", filtro.getTipo())
			.whereLikeIgnoreAll("contato.nome", filtro.getContato())
			.where("listaMateriaisrequisicao.dtdisponibilizacao >= ?", filtro.getDtiniciodisponibilizacao())
			.where("listaMateriaisrequisicao.dtdisponibilizacao <= ?", filtro.getDtfimdisponibilizacao())
			.where("listaMateriaisrequisicao.dtretirada >= ?", filtro.getDtinicioretirada())
			.where("listaMateriaisrequisicao.dtretirada <= ?", filtro.getDtfimretirada())
			.whereLikeIgnoreAll("requisicao.descricao", filtro.getDescricao())
			.where("requisicao.meiocontato = ?", filtro.getMeiocontato())
			.where("requisicao.classificacao = ?", filtro.getClassificacao())
			.orderBy("requisicao.dtrequisicao")
			;
			
		if(filtro.getUsuariohistorico() != null){
			query 
				.where("listaRequisicaohistorico.cdusuarioaltera = ?", filtro.getUsuariohistorico().getCdpessoa())
				.where("listaRequisicaohistorico.cdrequisicaohistorico = (SELECT MIN(R.cdrequisicaohistorico)" +
																			"FROM Requisicaohistorico R " +
																			"WHERE R.requisicao = requisicao)");
		}
		
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Requisicao> query) {
		query.select("requisicao.cdrequisicao, requisicao.dtrequisicao,requisicao.dtprevisao, requisicao.descricao, requisicao.dtconclusao, requisicao.observacaoInterna, " +
				"requisicao.cdusuarioaltera, requisicao.dtaltera, requisicaoprioridade.cdrequisicaoprioridade, requisicaoprioridade.descricao, " +
				"requisicao.dthorainicio, requisicao.dthorafim, agendainteracao.cdagendainteracao," +
				"requisicaoestado.cdrequisicaoestado, requisicaoestado.descricao, cliente.cdpessoa, " +
				"cliente.nome, contrato.cdcontrato, contrato.descricao, colaboradorresponsavel.cdpessoa," +
				"arquivo.cdarquivo, arquivo.nome, contato.cdpessoa, contato.nome, contato.emailcontato, requisicao.faturar, requisicao.qtde, " +
				"requisicaorelacionada.cdrequisicao, requisicaorelacionada.descricao, contratomaterial.cdcontratomaterial, listaRequisicaohistorico.cdrequisicaohistorico, " +
				"listaRequisicaohistorico.observacao, listaRequisicaohistorico.dtaltera, listaRequisicaohistorico.cdusuarioaltera, listaRequisicaohistorico.observacaointerna, cli.cdpessoa, cli.nome, " +
				"listaMateriaisrequisicao.cdmaterialrequisicao, listaMateriaisrequisicao.quantidade, material.cdmaterial, requisicao.ordemservico, " +
				"listaMateriaisrequisicao.dtdisponibilizacao, listaMateriaisrequisicao.dtretirada, listaMateriaisrequisicao.valortotal, " +
				"listaMateriaisrequisicao.item, listaMateriaisrequisicao.autorizacaoFaturamento, listaMateriaisrequisicao.faturado, listaMateriaisrequisicao.percentualfaturado, " +
				"unidademedida.simbolo, material.nome, material.patrimonio, material.valorvenda, atividadetipo.cdatividadetipo, atividadetipo.nome, " +
				"listaItem.cdrequisicaoitem, listaItem.valor, atividadetipoitem.cdatividadetipoitem, atividadetipoitem.nome, atividadetipoitem.obrigatorio, " +
				"endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, listaMateriaisrequisicao.observacao, " +
				"endereco.bairro, municipio.nome, municipio.cdibge, uf.sigla, endereco.cep, enderecotipo.cdenderecotipo, venda.cdvenda, " +
				"projeto.cdprojeto, projeto.nome, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, listaMateriaisrequisicao.cdnotafiscalservicoitem, "+
				"notaFiscalServico.cdNota, meiocontato.cdmeiocontato, meiocontato.nome, meiocontato.padrao, " +
				"classificacao.cdclassificacao, classificacao.nome, vclassificacao.identificador, listaMateriaisrequisicao.valorunitario, requisicao.versaocliente,arquivoMat.cdarquivo,arquivoMat.nome")
			.leftOuterJoin("requisicao.contrato contrato")
			.leftOuterJoin("requisicao.empresa empresa")
			.leftOuterJoin("requisicao.atividadetipo atividadetipo")
			.join("requisicao.requisicaoprioridade requisicaoprioridade")
			.join("requisicao.requisicaoestado requisicaoestado")
			.join("requisicao.cliente cli")
			.leftOuterJoin("contrato.cliente cliente")
			.leftOuterJoin("requisicao.contato contato") 
			.leftOuterJoin("requisicao.listaMateriaisrequisicao listaMateriaisrequisicao") 
			.leftOuterJoin("listaMateriaisrequisicao.material material")
			.leftOuterJoin("listaMateriaisrequisicao.arquivo arquivoMat") 
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("requisicao.requisicaorelacionada requisicaorelacionada") 
			.join("requisicao.colaboradorresponsavel colaboradorresponsavel")
			.leftOuterJoin("requisicao.listaRequisicaohistorico listaRequisicaohistorico")
			.leftOuterJoin("listaRequisicaohistorico.arquivo arquivo")
			.leftOuterJoin("requisicao.contratomaterial contratomaterial")
			.leftOuterJoin("requisicao.listaItem listaItem")
			.leftOuterJoin("listaItem.atividadetipoitem atividadetipoitem")
			.leftOuterJoin("requisicao.endereco endereco")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("endereco.enderecotipo enderecotipo")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("requisicao.venda venda")
			.leftOuterJoin("requisicao.agendainteracao agendainteracao")
			.leftOuterJoin("requisicao.projeto projeto")
			.leftOuterJoin("requisicao.notaFiscalServico notaFiscalServico")
			.leftOuterJoin("requisicao.meiocontato meiocontato")
			.leftOuterJoin("requisicao.classificacao classificacao")
			.leftOuterJoin("classificacao.vclassificacao vclassificacao")
			.orderBy("listaRequisicaohistorico.dtaltera desc, atividadetipoitem.ordem ");
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		Requisicao requisicao = (Requisicao)save.getEntity();
		
		if(requisicao.getListaMateriaisrequisicao()!=null && !requisicao.getListaMateriaisrequisicao().isEmpty()){
			List<Materialrequisicao> listMaterial = requisicao.getListaMateriaisrequisicao();
			materialrequisicaoService.saveArquivos(listMaterial);
		}	
				
		save.saveOrUpdateManaged("listaMateriaisrequisicao");
		save.saveOrUpdateManaged("listaItem");
	}
	
	/**
	 * M�todo para carregar as requisi��es pelos PK's passados como par�metro
	 * 
	 * @author Jo�o Paulo Zica
	 * @param whereIn
	 * @return
	 */
	public List<Requisicao> carregaRequisicao(String whereIn){
		return query()
			.select("requisicao.cdrequisicao, requisicao.qtde, requisicao.faturar, requisicaoestado.cdrequisicaoestado, requisicao.ordemservico, requisicao.observacaoInterna, " +
					"contrato.cdcontrato, contrato.descricao, cliente.cdpessoa, clienteReq.cdpessoa, clienteReq.nome, clienteReq.cnpj, clienteReq.cpf, " +
					"clienteReq.tipopessoa, clienteReq.razaosocial, clienteReq.inscricaoestadual, venda.cdvenda, requisicao.descricao, " +
					"listaItem.cdrequisicaoitem, listaItem.valor, atividadetipoitem.cdatividadetipoitem, atividadetipoitem.nome, atividadetipo.naoenviaremailcliente")			
			.leftOuterJoin("requisicao.cliente clienteReq")
			.leftOuterJoin("requisicao.contrato contrato")
			.leftOuterJoin("requisicao.venda venda")
			.leftOuterJoin("contrato.cliente cliente")
			.join("requisicao.requisicaoestado requisicaoestado")
			.leftOuterJoin("requisicao.listaItem listaItem")
			.leftOuterJoin("listaItem.atividadetipoitem atividadetipoitem")
			.leftOuterJoin("requisicao.atividadetipo atividadetipo")
//			.leftOuterJoin("contrato.cliente cliente")
//			.leftOuterJoin("requisicao.requisicaoprioridade requisicaoprioridade")
			.whereIn("requisicao.cdrequisicao", whereIn)
			.orderBy("clienteReq.cdpessoa, contrato.cdcontrato")
			.list();
	}
	
	
	/**
	 * M�todo para carregar a requisi��o
	 * 
	 * @author Jo�o Paulo Zica
	 * @param requisicao
	 * @return
	 */
	public Requisicao carregaRequisicao(Requisicao requisicao){
		return query()
			.select("requisicao.cdrequisicao, requisicao.dtrequisicao, requisicao.descricao, " +
				"requisicaoprioridade.cdrequisicaoprioridade, requisicaoprioridade.descricao, " +
				"requisicaoestado.cdrequisicaoestado, requisicaoestado.descricao, " +
				"contrato.cdcontrato, contrato.descricao, cliente.cdpessoa, cliente.nome, cliente.email, " +
				"colaboradorresponsavel.cdpessoa, colaboradorresponsavel.email, " +
				"contato.cdpessoa, contato.emailcontato, " +
				"cli.cdpessoa, cli.nome, cli.email, venda.cdvenda, atividadetipo.cdatividadetipo, atividadetipo.nome, " +
				"atividadetipo.mensagememail, atividadetipo.emailextraenvioos, empresa.cdpessoa, projeto.cdprojeto ")
			.leftOuterJoin("requisicao.cliente cli")
			.leftOuterJoin("requisicao.contrato contrato")
			.leftOuterJoin("contrato.cliente cliente")
			.leftOuterJoin("requisicao.venda venda")
			.leftOuterJoin("requisicao.colaboradorresponsavel colaboradorresponsavel")
			.leftOuterJoin("requisicao.requisicaoprioridade requisicaoprioridade")
			.leftOuterJoin("requisicao.requisicaoestado requisicaoestado")
			.leftOuterJoin("requisicao.contato contato")
			.leftOuterJoin("requisicao.atividadetipo atividadetipo")
			.leftOuterJoin("requisicao.empresa empresa")
			.leftOuterJoin("requisicao.projeto projeto")
			.where("requisicao =?", requisicao)
			.unique();
	}
	/**
	 * Faz update no estado da requisi��o.
	 * 
	 * @param requisicaoestado
	 * @param cd
	 * @author Jo�o Paulo Zica
	 */
	public void updateEstado(Requisicaoestado requisicaoestado, String cd, Date dtconclusao) {
		if(dtconclusao == null && requisicaoestado != null && Requisicaoestado.CONCLUIDA.equals(requisicaoestado.getCdrequisicaoestado())){
			dtconclusao = new Date(System.currentTimeMillis());
		}
		
		getJdbcTemplate().update("update Requisicao requisicao set cdrequisicaoestado = ?, dtconclusao = ? where cdrequisicao =?", new Object[]{requisicaoestado.getCdrequisicaoestado(), dtconclusao,Integer.parseInt(cd)});
	}
	
	/**
	 * Faz update no estado das requisi��es.
	 * 
	 * @param whereIn
	 * @param requisicaoestado
	 * @author Tom�s Rabelo
	 */
	public void updateEstados(String whereIn, Requisicaoestado requisicaoestado) {
		getJdbcTemplate().update("update Requisicao set cdrequisicaoestado = "+requisicaoestado.getCdrequisicaoestado()+" where cdrequisicao in ("+whereIn+")");
	}
	
	@SuppressWarnings("unchecked")
	public List<Requisicao> carregaDadosRequisicaoForCSV(RequisicaoFiltro filtro){
		StringBuilder sql = new StringBuilder(
					 "SELECT DISTINCT (R.CDREQUISICAO), R.ORDEMSERVICO, R.QTDE, R.DTREQUISICAO, R.DESCRICAO, RP.CDREQUISICAOPRIORIDADE, RP.DESCRICAO AS DESCRICAO1, ").append(
					 "RE.CDREQUISICAOESTADO, RE.DESCRICAO AS DESCRICAO2, CR.CDPESSOA, CR.NOME, CO.CDCONTRATO, CO.DESCRICAO AS DESCRICAO3, ").append(
					 "P1.CDPESSOA AS CDPESSOA1, P1.NOME AS DESCRICAO4, MR.CDMATERIALREQUISICAO, MR.DTRETIRADA, MR.DTDISPONIBILIZACAO, MR.QUANTIDADE, ").append(
					 "MR.VALORTOTAL, MR.OBSERVACAO, M.CDMATERIAL, M.NOME AS NOME1, 0 AS QTDE_ITERACAO, ").append(
					 "ENDE.NUMERO AS ENDE_NUMERO, ENDE.LOGRADOURO AS ENDE_LOGRADOURO, ENDE.CEP AS ENDE_CEP, ENDE.BAIRRO AS ENDE_BAIRRO, MUN.NOME AS MUN_NOME, UF.SIGLA AS UF_SIGLA, ").append(
					 "(SELECT AE1.DESCRICAO FROM AUTORIZACAOTRABALHO AUTOR ").append(
					 "JOIN REQUISICAOESTADO AE1 ON AE1.CDREQUISICAOESTADO = AUTOR.CDAUTORIZACAOESTADO ").append(
					 "WHERE AUTOR.CDREQUISICAO = R.CDREQUISICAO ").append(
					 "ORDER BY AUTOR.CDAUTORIZACAOTRABALHO DESC LIMIT 1) AS AUT, ").append(
					 "AT.NOME AS ATIVIDADETIPONOME, R.ORDEMSERVICO, PROJ.NOME AS PROJETO, PROJ.CDPROJETO ").append(
					 "FROM REQUISICAO R ").append(
					 "LEFT JOIN CONTRATO CO ON CO.CDCONTRATO = R.CDCONTRATO ").append(
					 "LEFT JOIN PROJETO PROJ ON R.CDPROJETO = PROJ.CDPROJETO ").append(
					 "LEFT JOIN PESSOA P ON P.CDPESSOA = CO.CDCLIENTE ").append(
					 "LEFT JOIN EMPRESA E ON E.CDPESSOA = R.CDEMPRESA ").append(
					 "LEFT JOIN PESSOA CON ON CON.CDPESSOA = R.CDCONTATO ").append(
					 "LEFT JOIN ENDERECO ENDE ON ENDE.CDENDERECO = R.CDENDERECO ").append(
					 "LEFT JOIN MUNICIPIO MUN ON MUN.CDMUNICIPIO = ENDE.CDMUNICIPIO ").append(
					 "LEFT JOIN UF UF ON UF.CDUF = MUN.CDUF ").append(
					 "JOIN PESSOA CR ON CR.CDPESSOA = R.CDCLIENTE ").append(
					 "JOIN REQUISICAOPRIORIDADE RP ON RP.CDREQUISICAOPRIORIDADE = R.CDREQUISICAOPRIORIDADE ").append(
					 "JOIN REQUISICAOESTADO RE ON RE.CDREQUISICAOESTADO = R.CDREQUISICAOESTADO ").append(
					 "JOIN PESSOA P1 ON P1.CDPESSOA = R.CDCOLABORADORRESPONSAVEL ").append(
					 "LEFT JOIN RATEIO RA ON CO.CDRATEIO = RA.CDRATEIO ").append(
					 "LEFT JOIN RATEIOITEM RI ON RA.CDRATEIO = RI.CDRATEIO ").append(
					 "LEFT JOIN MATERIALREQUISICAO MR ON R.CDREQUISICAO = MR.CDREQUISICAO ").append(
					 "LEFT JOIN MATERIAL M ON M.CDMATERIAL = MR.CDMATERIAL ").append(
					 "LEFT JOIN ATIVIDADETIPO AT ON AT.CDATIVIDADETIPO = R.CDATIVIDADETIPO ").append(
					 "LEFT JOIN REQUISICAOHISTORICO RH ON RH.CDREQUISICAO = R.CDREQUISICAO ").append(
					 "LEFT JOIN PROJETO P2 ON RI.CDPROJETO = P2.CDPROJETO ").append(" WHERE 1 = 1 ");

		if(filtro.getMostrarAutorizacao() != null){
			if(filtro.getMostrarAutorizacao()){
				sql.append(" AND (" +
							"SELECT ATESTADO.CDREQUISICAOESTADO " +
							"FROM AUTORIZACAOTRABALHO AT " +
							"JOIN REQUISICAOESTADO ATESTADO on ATESTADO.CDREQUISICAOESTADO=AT.CDAUTORIZACAOESTADO " +
							"JOIN REQUISICAO ATREQUISICAO on at.cdrequisicao=atrequisicao.cdrequisicao  " +
							"WHERE ATREQUISICAO.CDREQUISICAO = R.CDREQUISICAO " +
							"AND AT.CDAUTORIZACAOTRABALHO = (SELECT MAX(AT2.CDAUTORIZACAOTRABALHO) " +
							"FROM AUTORIZACAOTRABALHO AT2 " +
							"JOIN REQUISICAO AT2REQUISICAO ON AT2.CDREQUISICAO=AT2REQUISICAO.CDREQUISICAO " +
							"WHERE AT2REQUISICAO.CDREQUISICAO = R.CDREQUISICAO) " +
							") IN (" + Requisicaoestado.CONCLUIDA + "," + Requisicaoestado.CANCELADA + ") ");
			} else {
				sql.append(" AND (" +
						"SELECT ATESTADO.CDREQUISICAOESTADO " +
						"FROM AUTORIZACAOTRABALHO AT " +
						"JOIN REQUISICAOESTADO ATESTADO on ATESTADO.CDREQUISICAOESTADO=AT.CDAUTORIZACAOESTADO " +
						"JOIN REQUISICAO ATREQUISICAO on at.cdrequisicao=atrequisicao.cdrequisicao  " +
						"WHERE ATREQUISICAO.CDREQUISICAO = R.CDREQUISICAO " +
						"AND AT.CDAUTORIZACAOTRABALHO = (SELECT MAX(AT2.CDAUTORIZACAOTRABALHO) " +
						"FROM AUTORIZACAOTRABALHO AT2 " +
						"JOIN REQUISICAO AT2REQUISICAO ON AT2.CDREQUISICAO=AT2REQUISICAO.CDREQUISICAO " +
						"WHERE AT2REQUISICAO.CDREQUISICAO = R.CDREQUISICAO) " +
							") NOT IN (" + Requisicaoestado.CONCLUIDA + "," + Requisicaoestado.CANCELADA + ") ");
			}
		}
		if(filtro.getListasituacoes() != null && !filtro.getListasituacoes().isEmpty()){
			sql.append(" AND RE.CDREQUISICAOESTADO in (").append(CollectionsUtil.listAndConcatenate(filtro.getListasituacoes(), "cdrequisicaoestado",",")).append(") ");
		}
		if(filtro.getUsuariohistorico() != null && filtro.getUsuariohistorico().getCdpessoa() != null){
			sql.append(" AND RH.CDUSUARIOALTERA = ").append(filtro.getUsuariohistorico().getCdpessoa());
			sql.append(" AND RH.CDREQUISICAOHISTORICO = (SELECT MIN(RH2.CDREQUISICAOHISTORICO) " +
																			"FROM REQUISICAOHISTORICO RH2 " +
																			"WHERE RH2.CDREQUISICAO = R.CDREQUISICAO) ");
		}
		if(filtro.getColaboradorresponsavel() != null && filtro.getColaboradorresponsavel().getCdpessoa() != null){
			sql.append(" AND R.CDCOLABORADORRESPONSAVEL = "+filtro.getColaboradorresponsavel().getCdpessoa());
		}
		if(filtro.getTipo() != null && filtro.getTipo().getCdatividadetipo() != null){
			sql.append(" AND R.CDATIVIDADETIPO = " + filtro.getTipo().getCdatividadetipo());
		}
		if(filtro.getListaRequisicaoprioridade() != null && !filtro.getListaRequisicaoprioridade().isEmpty()){
			sql.append(" AND RP.CDREQUISICAOPRIORIDADE IN (").append(CollectionsUtil.listAndConcatenate(filtro.getListaRequisicaoprioridade(), "cdrequisicaoprioridade",",")).append(") ");
		}
		if(filtro.getContrato() != null && filtro.getContrato().getCdcontrato() != null){
			sql.append(" AND CO.CDCONTRATO = "+filtro.getContrato().getCdcontrato());
		}
		if(filtro.getCliente() != null && filtro.getCliente().getCdpessoa() != null){
			sql.append(" AND CR.CDPESSOA = "+filtro.getCliente().getCdpessoa());
		}
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null){
			sql.append(" AND R.CDEMPRESA = "+filtro.getEmpresa().getCdpessoa());
		}
		if(filtro.getProjeto() != null && filtro.getProjeto().getCdprojeto() != null){
			sql.append(" AND (P2.CDPROJETO = " + filtro.getProjeto().getCdprojeto() + " OR R.CDPROJETO = " + filtro.getProjeto().getCdprojeto() + ") ");
		}
		if(filtro.getDtinicio() != null){
			sql.append(" AND R.DTREQUISICAO >= to_date('" + new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtinicio())+"', 'dd/mm/yyyy') ");
		}
		if(filtro.getDtfim() != null){
			sql.append(" AND R.DTREQUISICAO <= to_date('" + new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtfim())+"', 'dd/mm/yyyy') ");
		}
		if(filtro.getDtinicioconclusao() != null){
			sql.append(" AND R.DTCONCLUSAO >= to_date('" + new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtinicioconclusao())+"', 'dd/mm/yyyy') ");
		}
		if(filtro.getDtfimconclusao() != null){
			sql.append(" AND R.DTCONCLUSAO <= to_date('" + new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtfimconclusao())+"', 'dd/mm/yyyy') ");
		}
		if(filtro.getDtinicioprevisao() != null){
			sql.append(" AND R.DTPREVISAO >= to_date('" + new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtinicioprevisao())+"', 'dd/mm/yyyy') ");
		}
		if(filtro.getDtfimprevisao() != null){
			sql.append(" AND R.DTPREVISAO <= to_date('" + new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtfimprevisao())+"', 'dd/mm/yyyy') ");
		}
		if(filtro.getDtiniciodisponibilizacao() != null && !filtro.getDtiniciodisponibilizacao().equals("")){
			sql.append(" AND MR.DTDISPONIBILIZACAO >= to_date('" + new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtiniciodisponibilizacao())+"', 'dd/mm/yyyy') ");

		}
		if(filtro.getDtfimdisponibilizacao() != null && !filtro.getDtfimdisponibilizacao().equals("")){
			sql.append(" AND MR.DTDISPONIBILIZACAO <= to_date('" + new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtfimdisponibilizacao())+"', 'dd/mm/yyyy') ");

		}
		if(filtro.getDtinicioretirada() != null && !filtro.getDtinicioretirada().equals("")){
			sql.append(" AND MR.DTRETIRADA >= to_date('" + new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtfimdisponibilizacao())+"', 'dd/mm/yyyy') ");

		}
		if(filtro.getDtfimretirada() != null && !filtro.getDtfimretirada().equals("")){
			sql.append(" AND MR.DTRETIRADA <= to_date('" + new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtfimdisponibilizacao())+"', 'dd/mm/yyyy') ");

		}
		if(filtro.getValor() != null && !filtro.getValor().equals("") && filtro.getAtividadetipoitem() != null && filtro.getAtividadetipoitem().getCdatividadetipoitem() != null){
			sql.append(" AND R.CDREQUISICAO IN ( SELECT RITEM.CDREQUISICAO " +
											" FROM REQUISICAOITEM AS RITEM " +
											" JOIN ATIVIDADETIPOITEM ATI ON ATI.CDATIVIDADETIPOITEM = RITEM.CDATIVIDADETIPOITEM " +
											" WHERE ATI.CDATIVIDADETIPOITEM = " + filtro.getAtividadetipoitem().getCdatividadetipoitem() +
											" AND LOWER(RITEM.VALOR) LIKE '%" + filtro.getValor().toLowerCase() + "%' ) ");
		}
		if(filtro.getObservacao() != null && !filtro.getObservacao().equals("")){
			sql.append(" AND R.CDREQUISICAO IN ( SELECT RH.CDREQUISICAO " +
											" FROM REQUISICAOHISTORICO AS RH " +
											" WHERE LOWER(RH.OBSERVACAO) LIKE '%" + filtro.getObservacao().toLowerCase() + "%' ) ");
		}
		if(filtro.getMaterialservico() != null && !filtro.getMaterialservico().equals("")){
			sql.append(" AND R.CDREQUISICAO IN ( SELECT MR.CDREQUISICAO " +
												" FROM MATERIALREQUISICAO AS MR, MATERIAL M " +
												" WHERE MR.CDMATERIAL = M.CDMATERIAL AND "+
												" LOWER(M.NOME) LIKE '%" + filtro.getMaterialservico().toLowerCase() + "%' ) ");

		}
		if (StringUtils.isNotBlank(filtro.getContato())) {
			sql.append(" AND CON.NOME LIKE '%").append(filtro.getContato()).append("%' ");
		}
		if (StringUtils.isNotBlank(filtro.getDescricao())) {
			sql.append(" AND R.DESCRICAO LIKE '%").append(filtro.getDescricao()).append("%' ");
		}
		if (filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null) {
			sql.append(" AND R.CDEMPRESA IN ("+filtro.getEmpresa().getCdpessoa()+") ");
		}
		if(filtro.getTipoiteracao() != null){
			sql.append("EXISTS (SELECT AT.CDAUTORIZACAOTRABALHO " +
								"FROM AUTORIZACAOTRABALHO AT " +
								"JOIN REQUISICAO ATREQUISICAO ON AT.CDREQUISICAO = ATREQUISICAO.CDREQUISICAO  " +
								"WHERE ATREQUISICAO.CDREQUISICAO = R.CDREQUISICAO AND AT.CDTIPOITERACAO = ").append(filtro.getTipoiteracao()).append(" ) ");
		}
		
		String listaEmpresa = new SinedUtil().getListaEmpresa();
		if (StringUtils.isNotBlank(listaEmpresa)) {
			sql.append(" AND (R.CDEMPRESA IN ("+listaEmpresa+") OR R.CDEMPRESA IS NULL) ");
		}
		
		if(filtro.getOrdemservico() != null && !filtro.getOrdemservico().equals("")){
			sql.append(" AND (R.ORDEMSERVICO IN ('"+ filtro.getOrdemservico() +"')  OR  R.CDREQUISICAO IN ("+ filtro.getOrdemservico() +") ) ");
		}
		sql.append(" ORDER BY R.CDREQUISICAO ");
		SinedUtil.markAsReader();
		List<Requisicao> list = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Requisicao r = new Requisicao(rs.getInt("CDREQUISICAO"), rs.getDate("DTREQUISICAO"), rs.getString("DESCRICAO"), rs.getDouble("QTDE"), rs.getInt("CDREQUISICAOPRIORIDADE"), 
						rs.getString("DESCRICAO1"), rs.getInt("CDREQUISICAOESTADO"), rs.getString("DESCRICAO2"), rs.getInt("CDPESSOA"), rs.getString("NOME"), 
						rs.getInt("CDCONTRATO"), rs.getString("DESCRICAO3"), rs.getInt("CDPESSOA1"), rs.getString("DESCRICAO4"), rs.getString("AUT"), 
						rs.getInt("CDMATERIALREQUISICAO"), rs.getDate("DTDISPONIBILIZACAO"), rs.getDate("DTRETIRADA"), rs.getInt("QUANTIDADE"),
						rs.getDouble("VALORTOTAL"), rs.getInt("CDMATERIAL"), rs.getString("NOME1"), rs.getString("ATIVIDADETIPONOME"), rs.getString("ORDEMSERVICO"),
						rs.getString("PROJETO"), rs.getInt("CDPROJETO"));
				
				r.setMaterialrequisicaoObs(rs.getString("PROJETO"));
				r.setMaterialrequisicaoQtdeIteracao(rs.getInt("QTDE_ITERACAO"));
				
				String endeNumero = rs.getString("ENDE_NUMERO");
				if(StringUtils.isNotBlank(endeNumero)){
					r.setEndereco(new Endereco());
					r.getEndereco().setNumero(endeNumero);
					r.getEndereco().setLogradouro(rs.getString("ENDE_LOGRADOURO"));
					r.getEndereco().setLogradouro(rs.getString("ENDE_BAIRRO"));
					
					String cep = rs.getString("ENDE_CEP");
					if(StringUtils.isNotBlank(cep)){
						r.getEndereco().setCep(new Cep(cep));
					}
					String mun = rs.getString("MUN_NOME");
					if(StringUtils.isNotBlank(mun)){
						r.getEndereco().setMunicipio(new Municipio());
						r.getEndereco().getMunicipio().setNome(mun);
						r.getEndereco().getMunicipio().setUf(new Uf(rs.getString("UF_SIGLA"), null));
					}
				}
				
				return r;
				
			}
		});
		return list;
		/*return query()
		.select("new Requisicao(requisicao.cdrequisicao, requisicao.dtrequisicao, requisicao.descricao, " +
				"requisicaoprioridade.cdrequisicaoprioridade, requisicaoprioridade.descricao," +
				"requisicaoestado.cdrequisicaoestado, requisicaoestado.descricao, cliente.cdpessoa, " +
				"cliente.nome, contrato.cdcontrato, contrato.descricao, (select ae.descricao from Autorizacaotrabalho at " +
																		"join at.autorizacaoestado ae " +
																		"order by at.requisicao desc))")
		
		.join("requisicao.contrato contrato")
		.join("contrato.cliente cliente")
		.join("requisicao.requisicaoprioridade requisicaoprioridade")
		.join("requisicao.requisicaoestado requisicaoestado")
		.join("requisicao.colaboradorresponsavel colaboradorresponsavel")
		.where("requisicaoprioridade = ?", filtro.getRequisicaoprioridade())
//		.where("requisicaoestado = ?", filtro.getRequisicaoestado())
		.whereIn("requisicaoestado", CollectionsUtil.listAndConcatenate(filtro.getListasituacoes(), "cdrequisicaoestado",","))
		.where("contrato =?", filtro.getContrato())
		.where("cliente = ?", filtro.getCliente())
		.where("requisicao.dtrequisicao >= ?",filtro.getDtinicio())
		.where("requisicao.dtrequisicao <= ?",filtro.getDtfim())
//		.where("colaboradorresponsavel.cdpessoa = ?", SinedUtil.getUsuarioLogado().getCdpessoa())
		.orderBy("requisicao.dtrequisicao")
		.setUseTranslator(false)
		.list();*/
	}
	
	@SuppressWarnings("unchecked")
	public Map<Integer, String> buscaSituacoesAutorizacoes(String whereIn){
		StringBuilder sql = new StringBuilder("SELECT R.CDREQUISICAO, ")
										.append("(SELECT AE1.DESCRICAO FROM AUTORIZACAOTRABALHO AUTOR ")
										.append("JOIN REQUISICAOESTADO AE1 ON AE1.CDREQUISICAOESTADO = AUTOR.CDAUTORIZACAOESTADO ")
										.append("WHERE AUTOR.CDREQUISICAO = R.CDREQUISICAO ")
										.append("ORDER BY AUTOR.CDAUTORIZACAOTRABALHO DESC LIMIT 1) AS AUT ")
										.append("FROM REQUISICAO R ")
										.append("WHERE R.CDREQUISICAO IN (").append(whereIn).append(") ");

		SinedUtil.markAsReader();
		List<Object[]> list = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Object[]{rs.getInt("CDREQUISICAO"), rs.getString("AUT")};
			}
		});
		
		Map<Integer, String> map = new HashMap<Integer, String>();
		for (Object[] objects : list) {
			map.put((Integer)objects[0], (String)objects[1]);
		}
		
		return map;
	}


	public List<Requisicao> findConcluidaByContrato(Contrato contrato, Integer cdrequisicao) {
		return query()
					.select("requisicao.cdrequisicao, requisicao.descricao")
					.join("requisicao.requisicaoestado requisicaoestado")
					.where("requisicao.cdrequisicao <> ?", cdrequisicao)
					.where("requisicao.contrato = ?", contrato)
					.where("requisicaoestado.cdrequisicaoestado = ?", Requisicaoestado.CONCLUIDA)
					.orderBy("requisicao.descricao")
					.list();
	}
	
	public List<Requisicao> findByCliente(Cliente cliente, Atividadetipo atividadetipo, Empresa empresa) {
		if(cliente == null || cliente.getCdpessoa() == null){
			throw new SinedException("Cliente n�o pode ser nulo.");
		}
		
		String whereInEmpresas = empresa != null? empresa.getCdpessoa().toString():
												CollectionsUtil.listAndConcatenate(empresaService.findByUsuario(), "cdpessoa", ",");
		return querySined()
					
					.select("requisicao.cdrequisicao, requisicao.descricao,requisicao.dtrequisicao,requisicao.dtprevisao,requisicaoestado.descricao,cliente.cdpessoa")
					.join("requisicao.requisicaoestado requisicaoestado")
					.join("requisicao.cliente cliente")
					.leftOuterJoin("requisicao.atividadetipo atividadetipo")
					.leftOuterJoin("requisicao.empresa empresa")
//					.leftOuterJoin("requisicao.contrato contrato")
//					.join("contrato.cliente cliente")
					.where("cliente = ?", cliente)
					.where("atividadetipo = ?", atividadetipo)
					.openParentheses()
						.where("empresa.cdpessoa is null")
						.or()
						.whereIn("empresa.cdpessoa", whereInEmpresas)
					.closeParentheses()
					//.where("requisicaoestado.cdrequisicaoestado = ?", Requisicaoestado.CONCLUIDA)
					.orderBy("requisicaoestado,dtrequisicao desc")
					.list();
	}
	
	/**
	 * Atualiza horas de uma determinada requisi��o.
	 * 
	 * @author Taidson
	 * @since 24/05/2010
	 * 
	 */
	public void atualizaHoras(Requisicao requisicao, Double qtdeHoras){
		String query = "update Requisicao set qtde = ? where cdrequisicao = ?" ;
		
		getHibernateTemplate().bulkUpdate(
				query, new Object[]{qtdeHoras, requisicao.getCdrequisicao()}
		);
	}
	 
	/**
	 * Retorna a quantidade de requisi��es (Em espera, Em andamento e Em teste) 
	 * vinculadas ao colaborador logado.
	 * @return
	 * @author Taidson
	 * @since 07/07/2010
	 */
	public Long qtdeRequisicoesForFlex(Colaborador colaborador){
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(Requisicao.class)
					.join("requisicao.requisicaoestado requisicaoestado")
					.where("requisicao.colaboradorresponsavel = ?", colaborador)
					.openParentheses()
					.where("requisicaoestado.cdrequisicaoestado = ?", 1)
					.or()
					.where("requisicaoestado.cdrequisicaoestado = ?", 2)
					.or()
					.where("requisicaoestado.cdrequisicaoestado = ?", 3)
					.closeParentheses()
					.unique();
	}
	
	
	/**
	 * Carrega dados da ordem servico para o relat�rio Hist�rico de cliente.
	 * @param filtro
	 * @return
	 * @author Taidson
	 * @since 18/08/2010
	 */
	public List<Requisicao> historicoCliente(String selectedItens, Empresa empresa, Date dtinicio, Date dtfim){
		String whereInEmpresas = empresa != null? empresa.getCdpessoa().toString():
			CollectionsUtil.listAndConcatenate(empresaService.findByUsuario(), "cdpessoa", ",");
		
		return querySined()
		.select("requisicao.cdrequisicao, cliente.cdpessoa, requisicao.dtrequisicao, requisicao.descricao," +
				"listaMateriaisrequisicao.cdmaterialrequisicao, listaMateriaisrequisicao.quantidade, " +
				"material.cdmaterial, material.nome, requisicaoestado.descricao, colaboradorresponsavel.nome, " +
				"listaRequisicaohistorico.cdrequisicaohistorico, listaRequisicaohistorico.observacao, " +
				"listaRequisicaohistorico.dtaltera, listaRequisicaohistorico.cdusuarioaltera, listaMateriaisrequisicao.valorunitario, "+
				"empresa.nome")
		 
		.leftOuterJoin("requisicao.cliente cliente")
		.leftOuterJoin("requisicao.listaMateriaisrequisicao listaMateriaisrequisicao") 
		.leftOuterJoin("listaMateriaisrequisicao.material material") 
		.join("requisicao.listaRequisicaohistorico listaRequisicaohistorico")
		.join("listaRequisicaohistorico.requisicao requisicaoHistorico")
		.leftOuterJoin("requisicaoHistorico.colaboradorresponsavel colaboradorresponsavel")
		.join("requisicao.requisicaoestado requisicaoestado")
		.leftOuterJoin("requisicao.empresa empresa")
		.whereIn("cliente.id", selectedItens)
		.openParentheses()
			.where("empresa.cdpessoa is null")
			.or()
			.whereIn("empresa.cdpessoa", whereInEmpresas)
		.closeParentheses()
		.where("listaRequisicaohistorico.dtaltera >= ?", SinedDateUtils.dateToTimestampInicioDia(dtinicio))
		.where("listaRequisicaohistorico.dtaltera <= ?", SinedDateUtils.dateToTimestampFinalDia(dtfim))
		.orderBy("requisicao.dtrequisicao")
		.list();
	}

	/**
	 * M�todo que carrega as requisi��es para relat�rio de emiss�o de fichas
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Requisicao> loadRequisicoesForReport(String whereIn) {
		return query()
			.select("requisicao.cdrequisicao, requisicao.descricao, requisicaoprioridade.cdrequisicaoprioridade, requisicaoprioridade.descricao, " +
					"requisicaoestado.cdrequisicaoestado, requisicaoestado.descricao, cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj, contato.cdpessoa, contato.nome, " +
					"endereco.cdendereco, endereco.logradouro, endereco.bairro, endereco.numero, endereco.complemento, endereco.caixapostal, endereco.cep, " +
					"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.nome, uf.sigla, " +
					"listaMateriaisrequisicao.cdmaterialrequisicao, listaMateriaisrequisicao.dtdisponibilizacao, " + 
					"listaMateriaisrequisicao.dtretirada, listaMateriaisrequisicao.quantidade, listaMateriaisrequisicao.valortotal, " +
					"material.cdmaterial, material.nome, listaItem.cdrequisicaoitem, listaItem.valor, " +
					"atividadetipoitem.cdatividadetipoitem, atividadetipoitem.nome, contrato.cdcontrato, " +
					"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.site, empresa.email," +
					"logomarca.cdarquivo, listaTelefone.cdtelefone, listaTelefone.telefone,requisicao.dtrequisicao," +
					"empresaReq.cdpessoa, empresaReq.nome, empresaReq.cnpj, empresaReq.site, empresaReq.email," +
					"listaTelefoneReq.cdtelefone, listaTelefoneReq.telefone, logomarcaReq.cdarquivo, requisicao.ordemservico, listaMateriaisrequisicao.valorunitario")
			.join("requisicao.requisicaoprioridade requisicaoprioridade")
			.join("requisicao.requisicaoestado requisicaoestado")
			.join("requisicao.cliente cliente")
			.leftOuterJoin("requisicao.listaMateriaisrequisicao listaMateriaisrequisicao")
			.leftOuterJoin("listaMateriaisrequisicao.material material")
			.leftOuterJoin("requisicao.listaItem listaItem")
			.leftOuterJoin("listaItem.atividadetipoitem atividadetipoitem")
			.leftOuterJoin("requisicao.endereco endereco")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			/*.leftOuterJoin("cliente.listaEndereco listaEndereco")
			.leftOuterJoin("listaEndereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")*/
			.leftOuterJoin("requisicao.contato contato")
			.leftOuterJoin("requisicao.contrato contrato")
			.leftOuterJoin("contrato.empresa empresa")
			.leftOuterJoin("empresa.logomarca logomarca")
			.leftOuterJoin("empresa.listaTelefone listaTelefone")
			.leftOuterJoin("requisicao.empresa empresaReq")
			.leftOuterJoin("empresaReq.logomarca logomarcaReq")
			.leftOuterJoin("empresaReq.listaTelefone listaTelefoneReq")
			.whereIn("requisicao.id", whereIn)
			.orderBy("requisicao.cdrequisicao")
			.list();
	}
	
	/**
	 * M�todo para carregar dados da requisi��o para o envio de email de altera��o da requisi��o
	 * 
	 * @author Marden Silva
	 * @param requisicao
	 * @return
	 */
	public Requisicao carregaDadosRequisicaoForEmail(Requisicao requisicao){
		return query()
			.select("requisicao.cdrequisicao, requisicao.descricao, " +
					"requisicaoestado.cdrequisicaoestado, requisicaoestado.descricao, " +
					"colaboradorresponsavel.cdpessoa, colaboradorresponsavel.email, " +
					"cliente.cdpessoa, cliente.email")
			.join("requisicao.cliente cliente")
			.leftOuterJoin("requisicao.requisicaoestado requisicaoestado")
			.leftOuterJoin("requisicao.colaboradorresponsavel colaboradorresponsavel")
			.where("requisicao =?", requisicao)
			.unique();
	}


	/**
	 * M�todo que faz o update do respons�vel da ordem de servi�o
	 *
	 * @param colaboradorresponsavel
	 * @param cdrequisicao
	 * @author Luiz Fernando
	 */
	public void updateResponsavel(Colaborador colaboradorresponsavel, Integer cdrequisicao) {
		if(colaboradorresponsavel == null || colaboradorresponsavel.getCdpessoa() == null || cdrequisicao == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		String query = "update Requisicao set cdcolaboradorresponsavel = ? where cdrequisicao = ?" ;
		
		getHibernateTemplate().bulkUpdate(
				query, new Object[]{colaboradorresponsavel.getCdpessoa(), cdrequisicao}
		);
	}


	/**
	 * M�todo para buscar as requisi��es que est�o em espera e que o respons�vel possua email
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Requisicao> findForEnviaEmailResponsavelRequisicaoEmEspera() {
		return query()
				.select("requisicao.cdrequisicao, requisicaoprioridade.cdrequisicaoprioridade, requisicao.descricao, requisicao.dtprevisao, " +
						"cliente.nome, requisicaoprioridade.descricao, contrato.descricao, " +
						"colaboradorresponsavel.nome, colaboradorresponsavel.email, colaboradorresponsavel.cdpessoa")
				.join("requisicao.colaboradorresponsavel colaboradorresponsavel")
				.join("requisicao.requisicaoestado requisicaoestado")
				.leftOuterJoin("requisicao.requisicaoprioridade requisicaoprioridade")
				.leftOuterJoin("requisicao.cliente cliente")
				.leftOuterJoin("requisicao.contrato contrato")
				.where("requisicaoestado.cdrequisicaoestado = ?", Requisicaoestado.EMESPERA)
				.where("colaboradorresponsavel.email is not null")
				.orderBy("colaboradorresponsavel.cdpessoa, requisicaoprioridade.cdrequisicaoprioridade, requisicao.dtprevisao desc")				
				.list();
	}


	/**
	 * M�todo que busca as requisi��es Em Espera para o painel de ordem de servi�o da p�gina principal 
	 *
	 * @return
	 * @author Luiz Fernando
	 * @param codigo 
	 * @param nome 
	 */
	public List<Requisicao> findForPainel(Integer prioridade) {		
		return querySined()
			
			.select("requisicao.cdrequisicao, requisicaoprioridade.cdrequisicaoprioridade, requisicao.descricao, requisicao.dtprevisao, " +
					"cliente.nome, requisicaoprioridade.descricao, contrato.descricao, " +
					"colaboradorresponsavel.nome, colaboradorresponsavel.email, colaboradorresponsavel.cdpessoa")
			.leftOuterJoin("requisicao.colaboradorresponsavel colaboradorresponsavel")
			.leftOuterJoin("requisicao.requisicaoestado requisicaoestado")
			.leftOuterJoin("requisicao.requisicaoprioridade requisicaoprioridade")
			.leftOuterJoin("requisicao.cliente cliente")
			.leftOuterJoin("requisicao.contrato contrato")			
			.where("requisicaoestado.cdrequisicaoestado = ?", Requisicaoestado.EMESPERA)			
			.where("colaboradorresponsavel = ?", SinedUtil.getUsuarioLogado())
			.where("requisicaoprioridade.cdrequisicaoprioridade = ?", prioridade)
			.list();
	}


	/**
	 * Retorna a quantidade de Ordens de Servi�o atrasadas.
	 *
	 * @return
	 * @since 23/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdeAtrasadas() {
		return newQueryBuilderSined(Long.class)
					
					.from(Requisicao.class)
					.setUseTranslator(false) 
					.select("count(*)")
					.join("requisicao.requisicaoestado requisicaoestado")
					.where("requisicaoestado.cdrequisicaoestado <> ?", Requisicaoestado.CONCLUIDA)
					.where("requisicaoestado.cdrequisicaoestado <> ?", Requisicaoestado.CANCELADA)
					.where("requisicao.dtprevisao < ?", SinedDateUtils.currentDate())
					.unique()
					.intValue();
	}	
	
	/**
	 * Retorna a quantidade de Ordens de Servi�o abertas.
	 *
	 * @return
	 * @since 23/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdeEmEspera() {
		return newQueryBuilderSined(Long.class)
					
					.from(Requisicao.class)
					.setUseTranslator(false) 
					.select("count(*)")
					.join("requisicao.requisicaoestado requisicaoestado")
					.where("requisicaoestado.cdrequisicaoestado = ?", Requisicaoestado.EMESPERA)
					.unique()
					.intValue();
	}	
	
	/**
	 * 
	 * M�todo que carrega a requisi��o com as vendas dela para fazer o update de status na hora do faturamento de uma Requisi��o.
	 *
	 * @name carregaRequisicaoParaUpdateDaVenda
	 * @param whereIn
	 * @return
	 * @return List<Requisicao>
	 * @author Thiago Augusto
	 * @date 30/05/2012
	 *
	 */
	public List<Requisicao> carregaRequisicaoParaUpdateDaVenda(String whereIn){
		return query()
			.select("requisicao.cdrequisicao, venda.cdvenda, requisicaoestado.cdrequisicaoestado")
			.leftOuterJoin("requisicao.requisicaoestado requisicaoestado")
			.leftOuterJoin("requisicao.venda venda")
			.whereIn("requisicao.cdrequisicao", whereIn)
			.list();
	}
	
	/**
	 * M�todo que carrega lista de requisi��es relacionadas a um cliente de determinada OS;
	 *
	 * @param q
	 * @return
	 * 
	 * @author Rafael Salvio
	 */
	public List<Requisicao> findRequisicaoRelacionadaAutocomplete(String q){
		WebRequestContext request = NeoWeb.getRequestContext();
		String cdcliente = (String) request.getSession().getAttribute("cdclienteRequisicao");
		
		if(cdcliente == null || cdcliente.equals(""))
			return new ArrayList<Requisicao>();
		Cliente cliente = new Cliente(Integer.valueOf(cdcliente));
		
		return query()
				.select("requisicao.cdrequisicao, requisicao.descricao")
				.join("requisicao.cliente cliente")
				.openParentheses()
				.whereLikeIgnoreAll("requisicao.cdrequisicao", q)
				.or()
				.whereLikeIgnoreAll("requisicao.descricao", q)
				.closeParentheses()
				.where("cliente = ?", cliente)
				.orderBy("requisicao.cdrequisicao")
				.list();
	}
	
	/**
	 * M�todo para carregar as requisi��es pelos PK's passados como par�metro
	 * 
	 * @param whereIn
	 * @return
	 * 
	 * @author Rafael Salvio
	 */
	public List<Requisicao> carregaRequisicaoForFaturar(String whereIn){
		return query()
			.select("requisicao.cdrequisicao, requisicao.qtde, requisicao.faturar, requisicaoestado.cdrequisicaoestado, requisicao.ordemservico, requisicao.descricao, " +
					"contrato.cdcontrato, contrato.descricao, empresa.cdpessoa, empresa.tributacaomunicipiocliente, empresacontrato.cdpessoa, empresacontrato.tributacaomunicipiocliente, cliente.cdpessoa, clienteReq.cdpessoa, clienteReq.nome, clienteReq.cnpj, clienteReq.cpf, " +
					"clienteReq.tipopessoa, clienteReq.razaosocial, clienteReq.inscricaoestadual, venda.cdvenda, requisicao.descricao, enderecoReq.cdendereco, municipioReq.cdmunicipio, " +
					"listaItem.cdrequisicaoitem, listaItem.valor, atividadetipoitem.cdatividadetipoitem, atividadetipoitem.nome, atividadetipoitem.ordem, atividadetipoitem.imprimeNota, " +
					"listaMateriaisrequisicao.cdmaterialrequisicao, listaMateriaisrequisicao.quantidade, listaMateriaisrequisicao.valortotal, municipio.cdmunicipio, " +
					"listaMateriaisrequisicao.item, listaMateriaisrequisicao.autorizacaoFaturamento, listaMateriaisrequisicao.faturado, material.produto, material.tributacaomunicipal, " +
					"material.cdmaterial, material.nome, material.nomenf, material.servico, enderecoReq.cdendereco, grupotributacao.cdgrupotributacao, " +
					"listaMateriaisrequisicao.observacao, projeto.cdprojeto, projeto.sigla, projeto.nome, projeto.cei, projeto.endereco, municipio.nome, municipio.cdibge, municipio.cdsiafi, uf.nome, uf.sigla, layoutdiscriminacaoservico.cdreporttemplate, layoutdiscriminacaoservico.leiaute, " +
					"codigocnae.cdcodigocnae, codigotributacao.cdcodigotributacao, itemlistaservico.cditemlistaservico, regimetributacao.cdregimetributacao, listaMateriaisrequisicao.percentualfaturado, " +
					"colaboradorresponsavel.cdpessoa, colaboradorresponsavel.nome, listaMateriaisrequisicao.valorunitario")			
			.leftOuterJoin("requisicao.endereco enderecoReq")
			.leftOuterJoin("enderecoReq.municipio municipioReq")
			.leftOuterJoin("requisicao.cliente clienteReq")
			.leftOuterJoin("clienteReq.layoutdiscriminacaoservico layoutdiscriminacaoservico")
			.leftOuterJoin("requisicao.projeto projeto")
			.leftOuterJoin("projeto.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("requisicao.contrato contrato")
			.leftOuterJoin("contrato.empresa empresacontrato")
			.leftOuterJoin("requisicao.empresa empresa")
			.leftOuterJoin("empresa.regimetributacao regimetributacao")
			.leftOuterJoin("requisicao.venda venda")
			.leftOuterJoin("contrato.cliente cliente")
			.join("requisicao.requisicaoestado requisicaoestado")
			.leftOuterJoin("requisicao.listaItem listaItem")
			.leftOuterJoin("listaItem.atividadetipoitem atividadetipoitem")
			.leftOuterJoin("requisicao.listaMateriaisrequisicao listaMateriaisrequisicao")
			.leftOuterJoin("listaMateriaisrequisicao.material material")
			.leftOuterJoin("material.grupotributacao grupotributacao")
			.leftOuterJoin("grupotributacao.codigocnae codigocnae")
			.leftOuterJoin("grupotributacao.codigotributacao codigotributacao")
			.leftOuterJoin("grupotributacao.itemlistaservico itemlistaservico")
			.leftOuterJoin("requisicao.colaboradorresponsavel colaboradorresponsavel")
			.whereIn("requisicao.cdrequisicao", whereIn)
			.orderBy("clienteReq.cdpessoa, requisicao.cdrequisicao")
			.list();
	}
	
	public List<Requisicao> carregaRequisicaoForFaturarAutorizacao(String whereIn){
		return query()
			.select("requisicao.cdrequisicao, requisicaoestado.cdrequisicaoestado, listaMateriaisrequisicao.cdmaterialrequisicao, " +
					"listaMateriaisrequisicao.quantidade, listaMateriaisrequisicao.valortotal, " +
					"listaMateriaisrequisicao.item, listaMateriaisrequisicao.autorizacaoFaturamento, " +
					"listaMateriaisrequisicao.faturado, listaMateriaisrequisicao.percentualfaturado, " +
					"material.cdmaterial, material.nome, material.servico, listaMateriaisrequisicao.valorunitario")			
			.leftOuterJoin("requisicao.listaMateriaisrequisicao listaMateriaisrequisicao")
			.leftOuterJoin("listaMateriaisrequisicao.material material")
			.leftOuterJoin("requisicao.requisicaoestado requisicaoestado")
			.whereIn("requisicao.cdrequisicao", whereIn)
			.orderBy("requisicao.cdrequisicao")
			.list();
	}

	/**
	 * M�todo que verifica se existe materiais na requisi��o
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existsMaterial(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Requisicao.class)
				.join("requisicao.listaMateriaisrequisicao listaMateriaisrequisicao")
				.whereIn("requisicao.cdrequisicao", whereIn)
				.unique() > 0;
	}

	/**
	 * M�todo que busca as requisi��es para registrar sa�da
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Requisicao> findForRegistrarsaida(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
		.select("requisicao.cdrequisicao, requisicao.qtde, requisicaoestado.cdrequisicaoestado, " +
				"contrato.cdcontrato, empresa.cdpessoa, venda.cdvenda, empresaReq.cdpessoa, pedidovendatipo.cdpedidovendatipo, pedidovendatipo.gerarexpedicaovenda, " +
				"listaMateriaisrequisicao.cdmaterialrequisicao, listaMateriaisrequisicao.quantidade, listaMateriaisrequisicao.valortotal, " +
				"listaMateriaisrequisicao.item, listaMateriaisrequisicao.autorizacaoFaturamento, listaMateriaisrequisicao.faturado, " +
				"material.cdmaterial, material.nome, material.servico, empresavenda.cdpessoa, material.epi, material.produto, material.patrimonio, listaMateriaisrequisicao.valorunitario")			
		.leftOuterJoin("requisicao.empresa empresaReq")
		.leftOuterJoin("requisicao.contrato contrato")
		.leftOuterJoin("contrato.empresa empresa")
		.leftOuterJoin("requisicao.venda venda")
		.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
		.leftOuterJoin("venda.empresa empresavenda")
		.join("requisicao.requisicaoestado requisicaoestado")
		.leftOuterJoin("requisicao.listaItem listaItem")
		.join("requisicao.listaMateriaisrequisicao listaMateriaisrequisicao")
		.join("listaMateriaisrequisicao.material material")
		.whereIn("requisicao.cdrequisicao", whereIn)
		.where("not exists (select  meo.requisicao.cdrequisicao from Movimentacaoestoque me join me.movimentacaoestoqueorigem meo where (meo.requisicao.cdrequisicao = requisicao.cdrequisicao or meo.venda.cdvenda = requisicao.venda.cdvenda) and me.dtcancelamento is null)")
		.list();
	}

	/**
	 * M�todo que busca as requisicoes com as notas relacionadas
	 *
	 * @param whereInNFSI
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Requisicao> findForCancelamentoNFS(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
			return query()
				.select("requisicao.cdrequisicao, requisicaoestado.cdrequisicaoestado, " +
						"listaMateriaisrequisicao.cdmaterialrequisicao, listaMateriaisrequisicao.faturado, listaMateriaisrequisicao.valorunitario")			
				.join("requisicao.requisicaoestado requisicaoestado")
				.join("requisicao.listaMateriaisrequisicao listaMateriaisrequisicao")
				.whereIn("requisicao.cdrequisicao", whereIn)
				.openParentheses()
					.where("requisicaoestado.cdrequisicaoestado = ?", Requisicaoestado.FATURADO).or()
					.where("requisicaoestado.cdrequisicaoestado = ?", Requisicaoestado.FATURADO_PARCIALMENTE)
				.closeParentheses()
				.list();
	}

	public List<Requisicao> findForApontamento(String autocomplete) {
		Integer cdrequisicao = null;				
		if(StringUtils.isNumeric(autocomplete)){
			cdrequisicao = Integer.parseInt(autocomplete);
		}		
		return query()
			.select("requisicao.cdrequisicao,requisicao.ordemservico,requisicao.dtrequisicao")
			.where("requisicao.cdrequisicao = ?", cdrequisicao)
			.or()
			.whereLike("requisicao.ordemservico", autocomplete)			
		.list();
	}
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Requisicao> findForEmitirOS(String whereIn){
		return query()
				.select("requisicao.cdrequisicao, requisicao.dtrequisicao, requisicao.dtprevisao, requisicao.dtconclusao," +
						"requisicao.ordemservico, requisicao.descricao, atividadetipo1.cdatividadetipo, atividadetipo1.nome, cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj, listaTelefone.telefone, listaTelefoneEmpresa.telefone," +
						"endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, municipio.nome, municipio.cdibge, uf.sigla, uf.nome, endereco.cep, enderecotipo.cdenderecotipo, endereco.pontoreferencia," +
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.cnpj, empresa.email, empresa.logomarca, contrato.descricao, projeto.nome, colaboradorresponsavel.nome, contato.nome, requisicaoprioridade.descricao, requisicaoestado.descricao," +
						"listaMateriaisrequisicao.item, listaMateriaisrequisicao.autorizacaoFaturamento, material.nome, listaMateriaisrequisicao.dtdisponibilizacao, listaMateriaisrequisicao.dtretirada," +
						"unidademedida.nome, listaMateriaisrequisicao.quantidade, material.valorcusto, listaMateriaisrequisicao.valortotal, listaMateriaisrequisicao.observacao," +
						"listaApontamento.dtapontamento, listaApontamentoHoras.hrinicio, listaApontamentoHoras.hrfim, listaApontamento.descricao, atividadetipo2.nome," +
						"listaRequisicaohistorico.dtaltera, listaRequisicaohistorico.observacao, arquivo.cdarquivo, arquivo.nome, usuarioAltera.nome," +
						"listaItem.valor, atividadetipoitem.nome, listaMateriaisrequisicao.valorunitario, classificacao.cdclassificacao, classificacao.nome, vclassificacao.identificador ")
				.join("requisicao.colaboradorresponsavel colaboradorresponsavel")
				.join("requisicao.cliente cliente")
				.leftOuterJoin("cliente.listaTelefone listaTelefone")
				.leftOuterJoin("requisicao.atividadetipo atividadetipo1")
				.leftOuterJoin("requisicao.requisicaoprioridade requisicaoprioridade")
				.leftOuterJoin("requisicao.requisicaoestado requisicaoestado")
				.leftOuterJoin("requisicao.empresa empresa")
				.leftOuterJoin("empresa.listaTelefone listaTelefoneEmpresa")
				.leftOuterJoin("empresa.logomarca logomarca")
				.leftOuterJoin("requisicao.contrato contrato")
				.leftOuterJoin("requisicao.projeto projeto")
				.leftOuterJoin("requisicao.contato contato")
				.leftOuterJoin("requisicao.endereco endereco")
				.leftOuterJoin("endereco.enderecotipo enderecotipo")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("requisicao.listaMateriaisrequisicao listaMateriaisrequisicao")
				.leftOuterJoin("listaMateriaisrequisicao.material material")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("requisicao.listaApontamento2 listaApontamento")
				.leftOuterJoin("listaApontamento.listaApontamentoHoras listaApontamentoHoras")
				.leftOuterJoin("listaApontamento.atividadetipo atividadetipo2")
				.leftOuterJoin("requisicao.listaRequisicaohistorico listaRequisicaohistorico")
				.leftOuterJoin("listaRequisicaohistorico.arquivo arquivo")
				.leftOuterJoin("listaRequisicaohistorico.usuarioAltera usuarioAltera")
				.leftOuterJoin("requisicao.listaItem listaItem")
				.leftOuterJoin("listaItem.atividadetipoitem atividadetipoitem")
				.leftOuterJoin("requisicao.classificacao classificacao")
				.leftOuterJoin("classificacao.vclassificacao vclassificacao")
				.whereIn("requisicao.cdrequisicao", whereIn)
				.orderBy("requisicao.cdrequisicao, atividadetipoitem.ordem ")
				.list();
	}
	
	public List<Requisicao> findForTotalizacaoListagem(RequisicaoFiltro filtro){
		QueryBuilderSined<Requisicao> query = querySined();
		this.updateListagemQuery(query, filtro);
		return query.list();
	}

	/**
	 * Calcula o valor total das OS na tela de listagem
	 * @param filtro
	 * @return
	 * @author Luiz Romario Filho
	 */
	public Money calculaTotal(RequisicaoFiltro filtro) {
		Money totalRequisicao = new Money();

		if (filtro != null) {
			QueryBuilderSined<Requisicao> query = querySined();
			this.updateListagemQuery(query, filtro);
			List<Requisicao> listaTotal = query.list();

			if (listaTotal != null && listaTotal.size() > 0) {
				QueryBuilder<Double> queryLong = newQueryBuilder(Double.class)
				.from(Requisicao.class)
				.select("sum(listaMateriaisrequisicao.valortotal)")
				.leftOuterJoin("requisicao.listaMateriaisrequisicao listaMateriaisrequisicao")
				.setUseTranslator(false);
				SinedUtil.quebraWhereIn("requisicao.cdrequisicao",CollectionsUtil.listAndConcatenate(listaTotal,"cdrequisicao", ","), queryLong);

				Double total = queryLong.unique();

				if (total != null)
					totalRequisicao = new Money(total, false);
			}
		}

		return totalRequisicao;
	}
	
	/**
	 * Calcula o total de horas trabalhadas na listagem de ordem de servi�o
	 * @param filtro
	 */
	public Double calculaTotalHorasTrabalhadas(RequisicaoFiltro filtro){
		if(filtro == null){
			throw new SinedException("RequisicaoFiltro n�o pode ser nulo");
		}
		Double total = null;
		QueryBuilderSined<Requisicao> query = querySined();
		this.updateListagemQuery(query, filtro);
		query.select("requisicao.cdrequisicao, requisicao.qtde");
		query.ignoreAllJoinsPath(true);
		List<Requisicao> listaTotal = query.list();
		
		if (listaTotal != null && listaTotal.size() > 0) {
			QueryBuilder<Double> queryLong = newQueryBuilder(Double.class)
			.from(Requisicao.class)
			.select("sum(requisicao.qtde)")
			.setUseTranslator(false);
			
			SinedUtil.quebraWhereIn("requisicao.cdrequisicao", SinedUtil.listAndConcatenate(listaTotal, "cdrequisicao", ","), queryLong);

			total = queryLong.unique();
		}
		return total;
	}
	
	/**
	 * Faz update na nota fiscal de servi�o da requisi��o.
	 * 
	 * @param cdrequisicao
	 * @param cdnota
	 * @author Lucas Costa
	 */
	public void updateNotaFiscalServico(Integer cdrequisicao, Integer cdnota){
		getJdbcTemplate().update("update Requisicao requisicao set cdnota = ? where cdrequisicao =?", new Object[]{cdnota, cdrequisicao});
	}

	public void updateQtde(Requisicao requisicao) {
		getHibernateTemplate().bulkUpdate("update Requisicao r set r.qtde = ? where r = ?", new Object[]{
				requisicao.getQtde(), requisicao
		});
	}
	/**
	 * Carrega as requisi��es com suas respectivas listas de materiais (Apenas produtos e servicos).
	 * 
	 * @param whereIn
	 * @author Rafael Salvio
	 */
	public List<Requisicao> loadWithListaMaterial(String whereIn) {
		if(whereIn == null || whereIn.trim().isEmpty()){
			throw new SinedException("Requisi��o n�o pode ser Nulo!");
		}
		
		return query()
			.select("requisicao.cdrequisicao,listaMateriaisrequisicao.cdmaterialrequisicao, material.cdmaterial, material.produto, material.servico, " +
					"cliente.cdpessoa, requisicaoestado.cdrequisicaoestado, listaMateriaisrequisicao.valorunitario, material.tributacaomunicipal ")
			.join("requisicao.listaMateriaisrequisicao listaMateriaisrequisicao")
			.join("listaMateriaisrequisicao.material material")
			.join("requisicao.cliente cliente")
			.join("requisicao.requisicaoestado requisicaoestado")
			.whereIn("requisicao.cdrequisicao", whereIn)
			.openParentheses()
				.where("material.produto = ?",Boolean.TRUE)
				.or()
				.where("material.servico = ?",Boolean.TRUE)
			.closeParentheses()
			.list();
	}

	/**
	 * Carrega a Lista de Requisi��es para Emiss�o de Nota Fiscal de Produtos/Servicos
	 * 
	 * @param requisicao
	 * @author Marcos Lisboa
	 */
	public List<Requisicao> loadForEmitirNotaFiscalProduto(String whereIn, String tipofaturamento) {
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Param�tro WhereIn(PK's) n�o pode ser Nulo!");
		
		QueryBuilder<Requisicao> query = query();
		query
		.select("requisicao.cdrequisicao, requisicao.qtde, requisicao.faturar, requisicaoestado.cdrequisicaoestado, requisicao.ordemservico, requisicao.descricao, " +
				"contrato.cdcontrato, contrato.descricao, empresa.cdpessoa, empresacontrato.cdpessoa, cliente.cdpessoa, clienteReq.cdpessoa, clienteReq.nome, clienteReq.cnpj, clienteReq.cpf, " +
				"clienteReq.tipopessoa, clienteReq.razaosocial, clienteReq.inscricaoestadual, venda.cdvenda, requisicao.descricao, enderecoReq.cdendereco, " +
				"listaItem.cdrequisicaoitem, listaItem.valor, atividadetipoitem.cdatividadetipoitem, atividadetipoitem.nome, atividadetipoitem.ordem, atividadetipoitem.imprimeNota, " +
				"listaMateriaisrequisicao.cdmaterialrequisicao, listaMateriaisrequisicao.quantidade, listaMateriaisrequisicao.valortotal, municipio.cdmunicipio, " +
				"listaMateriaisrequisicao.item, listaMateriaisrequisicao.autorizacaoFaturamento, listaMateriaisrequisicao.faturado, " +
				"material.cdmaterial, material.nome, material.nomenf, material.servico, material.valorvenda, material.ncmcompleto, " +
				"enderecoReq.cdendereco, grupotributacao.cdgrupotributacao, ncmcapitulo.cdncmcapitulo, ncmcapitulo.descricao, " +
				"listaMateriaisrequisicao.observacao, projeto.cdprojeto, projeto.sigla, projeto.nome, projeto.cei, projeto.endereco, municipio.nome, municipio.cdibge, municipio.cdsiafi, uf.nome, uf.sigla, layoutdiscriminacaoservico.cdreporttemplate, layoutdiscriminacaoservico.leiaute, " +
				"codigocnae.cdcodigocnae, codigotributacao.cdcodigotributacao, itemlistaservico.cditemlistaservico, regimetributacao.cdregimetributacao, listaMateriaisrequisicao.percentualfaturado, " +
				"colaboradorresponsavel.cdpessoa, colaboradorresponsavel.nome, unidademedida.cdunidademedida, unidademedida.nome, listaMateriaisrequisicao.valorunitario")			
		.leftOuterJoin("requisicao.endereco enderecoReq")
		.leftOuterJoin("requisicao.cliente clienteReq")
		.leftOuterJoin("clienteReq.layoutdiscriminacaoservico layoutdiscriminacaoservico")
		.leftOuterJoin("requisicao.projeto projeto")
		.leftOuterJoin("projeto.municipio municipio")
		.leftOuterJoin("municipio.uf uf")
		.leftOuterJoin("requisicao.contrato contrato")
		.leftOuterJoin("contrato.empresa empresacontrato")
		.leftOuterJoin("requisicao.empresa empresa")
		.leftOuterJoin("empresa.regimetributacao regimetributacao")
		.leftOuterJoin("requisicao.venda venda")
		.leftOuterJoin("contrato.cliente cliente")
		.join("requisicao.requisicaoestado requisicaoestado")
		.leftOuterJoin("requisicao.listaItem listaItem")
		.leftOuterJoin("listaItem.atividadetipoitem atividadetipoitem")
		.leftOuterJoin("requisicao.listaMateriaisrequisicao listaMateriaisrequisicao")
		.leftOuterJoin("listaMateriaisrequisicao.material material")
		.leftOuterJoin("material.unidademedida unidademedida")
		.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
		.leftOuterJoin("material.grupotributacao grupotributacao")
		.leftOuterJoin("grupotributacao.codigocnae codigocnae")
		.leftOuterJoin("grupotributacao.codigotributacao codigotributacao")
		.leftOuterJoin("grupotributacao.itemlistaservico itemlistaservico")
		.leftOuterJoin("requisicao.colaboradorresponsavel colaboradorresponsavel")
		.whereIn("requisicao.cdrequisicao", whereIn)
		.orderBy("clienteReq.cdpessoa, requisicao.cdrequisicao");
		
		if("produto".equalsIgnoreCase(tipofaturamento)){
			query.where("material.produto = ?",Boolean.TRUE);
		}else if("servico".equalsIgnoreCase(tipofaturamento)){
			query
				.openParentheses()
					.where("material.servico = ?", Boolean.TRUE)
					.or()
					.openParentheses()
						.where("material.produto = ?", Boolean.TRUE)
						.where("material.tributacaomunicipal = ?", Boolean.TRUE)
					.closeParentheses()
				.closeParentheses();
		}else{
			query.where("material.servico <> ?",Boolean.TRUE);
		}
		
		return query.list();
	}
	
	/**
	 * Busca as requisi��es para a a��o Enviar Email
	 * @param whereIn
	 * @return
	 */
	public List<Requisicao> findForEnviarEmail(String whereIn) {
		if(whereIn == null || "".equals(whereIn)){
			throw new SinedException("Par�metro inv�lido.");			
		}
		
		return query()
		.select("requisicao.cdrequisicao, requisicao.descricao, requisicao.dtprevisao, " +
				"cliente.cdpessoa, cliente.nome, cliente.email, listaRequisicaohistorico.cdrequisicaohistorico, listaRequisicaohistorico.observacao," +
				"atividadetipo.cdatividadetipo, atividadetipo.mensagememail")
        .leftOuterJoin("requisicao.cliente cliente")
        .leftOuterJoin("requisicao.listaRequisicaohistorico listaRequisicaohistorico")
        .leftOuterJoin("requisicao.atividadetipo atividadetipo")
        .whereIn("requisicao.cdrequisicao", whereIn)
        .list();
	}
	
	/**
	* M�todo que busca as OS para o agendamento de servi�o
	*
	* @param whereIn
	* @param cliente
	* @param colaborador
	* @param empresa
	* @param listaRequisicaoestado
	* @return
	* @since 01/07/2015
	* @author Luiz Fernando
	*/
	public List<Requisicao> findAutocompleteForAgendaServico(String whereIn, Cliente cliente, Colaborador colaborador, Empresa empresa, List<Requisicaoestado> listaRequisicaoestado) {
		String whereNotIn = null;
		if(SinedUtil.isListNotEmpty(listaRequisicaoestado)){
			whereNotIn = CollectionsUtil.listAndConcatenate(listaRequisicaoestado, "cdrequisicaoestado", ",");
		}
		
		return query()
			.autocomplete()
			.select("requisicao.cdrequisicao, requisicao.descricao, requisicao.dtrequisicao, requisicao.dtprevisao, " +
					"cliente.cdpessoa, cliente.nome, requisicao.ordemservico ")
	        .leftOuterJoin("requisicao.cliente cliente")
	        .whereLikeIgnoreAll("coalesce(requisicao.ordemservico, requisicao.cdrequisicao || '')", whereIn)
	        .where("cliente = ?", cliente)
	        .where("requisicao.colaboradorresponsavel = ?", colaborador)
	        .where("requisicao.empresa = ?", empresa)
	        .where("requisicao.requisicaoestado.cdrequisicaoestado not in (" + whereNotIn + ")", whereNotIn != null)
	        .orderBy("requisicao.dtrequisicao")
	        .list();
	}
	
	/**
	* M�todo que busca as OS para o agendamento de servi�o
	*
	* @param filtro
	* @return
	* @since 01/07/2015
	* @author Luiz Fernando
	*/
	public List<Requisicao> findForAgendaServicoListagemFlex(RequisicaoFiltro filtro) {
		if(filtro == null){
			throw new SinedException("Par�metro inv�lido.");			
		}
		
		return query()
			.select("requisicao.cdrequisicao, requisicao.descricao, requisicao.dtrequisicao, requisicao.dtprevisao, " +
					"cliente.cdpessoa, cliente.nome, requisicao.ordemservico ")
	        .leftOuterJoin("requisicao.cliente cliente")
	        .whereIn("coalesce(requisicao.ordemservico, requisicao.cdrequisicao || '')", StringUtils.isNotEmpty(filtro.getOrdemservico()) ? SinedUtil.montaWhereInString(filtro.getOrdemservico()) : null)
	        .where("requisicao.cliente = ?", filtro.getCliente())
	        .where("requisicao.empresa = ?", filtro.getEmpresa())
	        .where("requisicao.dtrequisicao >= ?", filtro.getDtinicio())
	        .where("requisicao.dtrequisicao <= ?", filtro.getDtfim())
	        .where("requisicao.colaboradorresponsavel = ?", filtro.getColaboradorresponsavel())
			.where("requisicao.requisicaoestado.cdrequisicaoestado <> ?", Requisicaoestado.CONCLUIDA)
			.where("requisicao.requisicaoestado.cdrequisicaoestado <> ?", Requisicaoestado.CANCELADA)
	        .orderBy("requisicao.dtrequisicao")
	        .list();
	}
	
	/**
	 * M�todo respons�vel por buscar as requisi��es relacionadas com agendainteracao
	 * @param agendainteracao
	 * @return
	 * @author C�sar
	 */
	public List<Requisicao> findForAgendaInteracaoHistorico(Agendainteracao agendainteracao){
		if(agendainteracao == null){
			throw new SinedException("Par�metro Inv�lido");
		}
		return query().select("requisicao.cdrequisicao, requisicao.descricao")		
				.where("agendainteracao = ? ", agendainteracao).list();
	}

	public List<Requisicao> findByAtrasado(Date data, Date dateToSearch) {
		return querySined()
				.setUseWhereClienteEmpresa(false)
				.setUseWhereEmpresa(false)
				.setUseWhereFornecedorEmpresa(false)
				.setUseWhereProjeto(false)
				.select("requisicao.cdrequisicao, empresa.cdpessoa,colaboradorresponsavel.cdpessoa")	
				.leftOuterJoin("requisicao.colaboradorresponsavel colaboradorresponsavel")
				.leftOuterJoin("requisicao.empresa empresa")
				.where("requisicao.dtprevisao < ?", data)
				.where("requisicao.requisicaoestado.cdrequisicaoestado != ?", Requisicaoestado.CANCELADA)
				.where("requisicao.requisicaoestado.cdrequisicaoestado != ?", Requisicaoestado.CONCLUIDA)
				.where("requisicao.requisicaoestado.cdrequisicaoestado != ?", Requisicaoestado.FATURADO)
				.where("requisicao.requisicaoestado.cdrequisicaoestado != ?", Requisicaoestado.VISTO)
				.wherePeriodo("requisicao.dtprevisao", dateToSearch, SinedDateUtils.currentDate())
				.list();
	}
}