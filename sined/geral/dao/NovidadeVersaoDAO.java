package br.com.linkcom.sined.geral.dao;

import java.util.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.NovidadeVersao;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConsultaNovidadeVersaoBean;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NovidadeVersaoDAO extends GenericDAO<NovidadeVersao> {
	
	public NovidadeVersao findByNovidadeVersao(ConsultaNovidadeVersaoBean bean) {
		Date dataCorrente = new Date();
		if(bean.getCdnovidadeVersao() != null){
			 return querySined()
						.setUseReadOnly(true)
						.select("novidadeVersao.cdnovidadeversao, novidadeVersao.versao, novidadeVersao.descricao,novidadeVersao.dtinicio, novidadeVersao.dtfim," +
								"funcionalidades.cdnovidadeversaofuncionalidade, funcionalidades.titulo, funcionalidades.funcionalidade, funcionalidades.urlBaseConhecimento," +
								"funcionalidades.urlVideo, funcionalidades.modulo, segmento.nome, projeto.nome")
						.leftOuterJoin("novidadeVersao.listaNovidadeVersaoFuncionalidade funcionalidades")
						.leftOuterJoin("novidadeVersao.projeto projeto")
						.leftOuterJoin("funcionalidades.segmento segmento")
						.where("novidadeVersao.cdnovidadeversao = ?", bean.getCdnovidadeVersao())
						.unique();
		}else if(!bean.getConsultaMenu()){
			 return querySined()
					.setUseReadOnly(true)
					.select("novidadeVersao.cdnovidadeversao, novidadeVersao.versao, novidadeVersao.descricao,novidadeVersao.dtinicio, novidadeVersao.dtfim," +
							"funcionalidades.cdnovidadeversaofuncionalidade, funcionalidades.titulo, funcionalidades.funcionalidade, funcionalidades.urlBaseConhecimento," +
							"funcionalidades.urlVideo, funcionalidades.modulo, segmento.nome, projeto.nome")
					.leftOuterJoin("novidadeVersao.listaNovidadeVersaoFuncionalidade funcionalidades")
					.leftOuterJoin("novidadeVersao.projeto projeto")
					.leftOuterJoin("funcionalidades.segmento segmento")
					.where("projeto.nome = ?", bean.getProjeto())
					.where("novidadeVersao.dtinicio <= ?", dataCorrente)
					.where("novidadeVersao.dtfim >= ?", dataCorrente)
					.where("novidadeVersao.ativo = ?", Boolean.TRUE)
					.unique();
		}else if(bean.getNovidadePeriodoAtual()){
			return querySined()
					.setUseReadOnly(true)
					.select("novidadeVersao.cdnovidadeversao, projeto.nome, novidadeVersao.versao, novidadeVersao.dtfim")
					.leftOuterJoin("novidadeVersao.projeto projeto")
					.where("projeto.nome = ?", bean.getProjeto())
					.where("novidadeVersao.dtinicio <= ?", dataCorrente)
					.where("novidadeVersao.dtfim >= ?", dataCorrente)
					.where("novidadeVersao.ativo = ?", Boolean.TRUE)
					.unique();	
		}else {
			return querySined()
				.setUseReadOnly(true)
				.select("novidadeVersao.cdnovidadeversao, projeto.nome, novidadeVersao.versao, novidadeVersao.dtfim")
				.leftOuterJoin("novidadeVersao.projeto projeto")
				.where("projeto.nome = ?", bean.getProjeto())
				.where("novidadeVersao.ativo = ?", Boolean.TRUE)
				.unique();
		}
	}

	public List<NovidadeVersao> findVersoesComNovidade(ConsultaNovidadeVersaoBean bean) {
		 return querySined()
					.setUseReadOnly(true)
					.select("novidadeVersao.cdnovidadeversao, novidadeVersao.versao")
					.leftOuterJoin("novidadeVersao.projeto projeto")
					.where("projeto.nome = ?", bean.getProjeto())
					.where("novidadeVersao.ativo = ?", Boolean.TRUE)
					.orderBy("novidadeVersao.versao desc")
					.list();
	}

	public NovidadeVersao findUltimaNovidadeVersao(ConsultaNovidadeVersaoBean bean) {
		 return querySined()
					.setUseReadOnly(true)
					.select("novidadeVersao.cdnovidadeversao, novidadeVersao.versao, novidadeVersao.descricao, novidadeVersao.dtinicio, novidadeVersao.dtfim," +
							"funcionalidades.cdnovidadeversaofuncionalidade, funcionalidades.titulo, funcionalidades.funcionalidade, funcionalidades.urlBaseConhecimento," +
							"funcionalidades.urlVideo, funcionalidades.modulo, segmento.nome, projeto.nome")
					.leftOuterJoin("novidadeVersao.listaNovidadeVersaoFuncionalidade funcionalidades")
					.leftOuterJoin("novidadeVersao.projeto projeto")
					.leftOuterJoin("funcionalidades.segmento segmento")
					.where("novidadeVersao.cdnovidadeversao = ?", bean.getCdnovidadeVersao())
					.unique();
	}
	
	public NovidadeVersao findCdNovidadeVersao(ConsultaNovidadeVersaoBean bean) {
		return querySined()
				.setUseReadOnly(true)
				.select("novidadeVersao.cdnovidadeversao, novidadeVersao.versao")
				.leftOuterJoin("novidadeVersao.projeto projeto")
				.where("projeto.nome = ?", bean.getProjeto())
				.where("novidadeVersao.ativo = ?", Boolean.TRUE)
				.orderBy("novidadeVersao.dtinicio desc")
				.setMaxResults(1)
				.unique();

	}
		
	
}