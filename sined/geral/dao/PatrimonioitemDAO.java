package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Bempatrimonio;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.enumeration.Patrimonioitemsituacao;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.PatrimonioitemFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PatrimonioitemDAO extends GenericDAO<Patrimonioitem> {

	private ArquivoDAO arquivoDAO;
	private MaterialcategoriaService materialcategoriaService;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {this.materialcategoriaService = materialcategoriaService;}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Patrimonioitem> query) {
		query.leftOuterJoinFetch("patrimonioitem.bempatrimonio bempatrimonio");
		query.leftOuterJoinFetch("patrimonioitem.vpatrimonioitem vpatrimonioitem");
		query.leftOuterJoinFetch("patrimonioitem.empresa empresa");
		query.leftOuterJoinFetch("patrimonioitem.foto foto");
		query.leftOuterJoinFetch("patrimonioitem.fornecimentocontrato fornecimentocontrato");
		query.leftOuterJoinFetch("patrimonioitem.listaHistorico listaHistorico");
		query.leftOuterJoinFetch("patrimonioitem.materialnumeroserie materialnumeroserie");
		
		query.orderBy("listaHistorico.dtaltera desc");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Patrimonioitem> query, FiltroListagem _filtro) {
		PatrimonioitemFiltro filtro = (PatrimonioitemFiltro) _filtro;
		query
			.select("distinct bempatrimonio.cdmaterial, bempatrimonio.nome, patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, " +
				"patrimonioitem.ativo, materialgrupo.nome, materialtipo.nome, bempatrimonio.valor, vpatrimonioitem.situacao," +
				"materialnumeroserie.numero, cliente.nome, localarmazenagem.nome")
			.join("patrimonioitem.vpatrimonioitem vpatrimonioitem")
			.leftOuterJoin("patrimonioitem.listaMovpatrimonio listaMovpatrimonio")
			.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
			.leftOuterJoin("bempatrimonio.materialgrupo materialgrupo")
			.leftOuterJoin("bempatrimonio.materialtipo materialtipo")
			.leftOuterJoin("patrimonioitem.materialnumeroserie materialnumeroserie")
			.leftOuterJoin("listaMovpatrimonio.localarmazenagem localarmazenagem")
			.leftOuterJoin("listaMovpatrimonio.cliente cliente")
			.where("bempatrimonio=?",filtro.getBempatrimonio())
			.where("materialtipo = ?",filtro.getMaterialtipo())
			.where("materialgrupo = ?",filtro.getMaterialgrupo())
//			.whereLikeIgnoreAll("patrimonioitem.plaqueta", filtro.getPlaqueta())
			.where("patrimonioitem.ativo = ?",filtro.getAtivo())
			.where("patrimonioitem.dtaquisicao >= ?", filtro.getDtaquisicaode())
			.where("patrimonioitem.dtaquisicao <= ?", filtro.getDtaquisicaoate())
			.where("listaMovpatrimonio.localarmazenagem = ?", filtro.getLocalarmazenagem())
			.where("listaMovpatrimonio.cliente = ?", filtro.getCliente())
			.where("patrimonioitem.materialnumeroserie= ?", filtro.getMaterialnumeroserie());
		
		if (filtro.getBooleanplaqueta() != null) {
			if (filtro.getBooleanplaqueta()) {
				query
					.openParentheses()
						.where("patrimonioitem.plaqueta is not null")
						.where("patrimonioitem.plaqueta <> ''")
					.closeParentheses();;
			} else {
				query
					.openParentheses()
						.where("patrimonioitem.plaqueta is null").or()
						.where("patrimonioitem.plaqueta = ''")
					.closeParentheses();
			}
		}
		
		List<Patrimonioitemsituacao> listaSituacao = filtro.getListaSituacao();
		if(listaSituacao != null){
			query.whereIn("vpatrimonioitem.situacao", Patrimonioitemsituacao.listAndConcatenate(listaSituacao));
		}
		
		if(filtro.getMaterialcategoria() != null){
			materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
			query
				.leftOuterJoin("bempatrimonio.materialcategoria materialcategoria")
				.leftOuterJoin("materialcategoria.vmaterialcategoria vmaterialcategoria")
				.openParentheses()
					.where("vmaterialcategoria.identificador like ? ||'.%'", filtro.getMaterialcategoria().getIdentificador())
				.or()
					.where("vmaterialcategoria.identificador like ? ", filtro.getMaterialcategoria().getIdentificador())
				.closeParentheses();
		}
		
		query
			.openParentheses()
				.where("listaMovpatrimonio is null")
				.or()
				.where("listaMovpatrimonio.dtmovimentacao = (select max(ultimaMov.dtmovimentacao) " +
																 "  from Movpatrimonio ultimaMov " +
																 " where ultimaMov.patrimonioitem = patrimonioitem " +
																 "   and ultimaMov.dtcancelamento is null) ")
				.or()
				.where("not exists(select 1 from Movpatrimonio ultimaMov " +
						" where ultimaMov.patrimonioitem = patrimonioitem " +
						"   and ultimaMov.dtcancelamento is null) ")
			.closeParentheses();
//		boolean recapagem = "TRUE".equalsIgnoreCase(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.RECAPAGEM));
//		
//		if (recapagem){
//			query.leftOuterJoin("bempatrimonio.listaMaterialnumeroserie listaMaterialnumeroserie");
//			query.whereLikeIgnoreAll("patrimonioitem.plaqueta", filtro.getPlaqueta());
//			query.or();
//			query.whereLikeIgnoreAll("listaMaterialnumeroserie.numero", filtro.getPlaqueta());
//			query.ignoreJoin("listaMaterialnumeroserie");
//		}else {
//			query.whereLikeIgnoreAll("patrimonioitem.plaqueta", filtro.getPlaqueta());
//		}
		
		query.openParentheses();
		query.whereLikeIgnoreAll("patrimonioitem.plaqueta", filtro.getPlaqueta());
		query.or();
		query.whereLikeIgnoreAll("materialnumeroserie.numero", filtro.getPlaqueta());
		query.closeParentheses();
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		Patrimonioitem patrimonioitem = (Patrimonioitem) save.getEntity();
		if (patrimonioitem.getFoto() != null) {
			arquivoDAO.saveFile(patrimonioitem, "foto");
		}
	}

	/**
	 * Carrega o patrimonioitem com a plaqueta preenchida.
	 * 
	 * @param patrimonioitem
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Patrimonioitem loadWithPlaqueta(Patrimonioitem patrimonioitem) {
		if (patrimonioitem == null || patrimonioitem.getCdpatrimonioitem() == null) {
			throw new SinedException("Patrim�nio n�o pode ser nulo.");
		}
		return query()
					.select("patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, bempatrimonio.cdmaterial, bempatrimonio.nome," +
							"materialnumeroserie.numero")
					.join("patrimonioitem.bempatrimonio bempatrimonio")
					.leftOuterJoin("patrimonioitem.materialnumeroserie materialnumeroserie")
					.where("patrimonioitem = ?",patrimonioitem)
					.unique();
	}
	
	/**
	 * Carrega a listagem de itens de patrimonio sem plaquetas dentro de um whereIn.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Patrimonioitem> findWithoutPlaqueta(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Itens de patrim�nio n�o pode ser nulo.");
		}
		return query()
					.select("patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, bempatrimonio.nome, bempatrimonio.cdmaterial")
					.join("patrimonioitem.bempatrimonio bempatrimonio")
					.openParentheses()
					.where("patrimonioitem.plaqueta is null")
					.or()
					.where("patrimonioitem.plaqueta = ''")
					.closeParentheses()
					.whereIn("patrimonioitem.cdpatrimonioitem", whereIn)
					.list();
	}
	
	/**
	 * M�todo que busca as plaquetas geradas pelo recebimento
	 *
	 * @param entrega
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Patrimonioitem> findByEntrega(Entrega entrega) {
		if (entrega == null || entrega.getCdentrega() == null) 
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, bempatrimonio.nome, bempatrimonio.cdmaterial")
				.join("patrimonioitem.bempatrimonio bempatrimonio")
				.join("patrimonioitem.listaMovpatrimonio listaMovpatrimonio")
				.join("listaMovpatrimonio.movpatrimonioorigem movpatrimonioorigem")
				.join("movpatrimonioorigem.entrega entrega")
				.where("patrimonioitem.plaqueta is not null")
				.where("entrega = ?", entrega)
				.list();
	}

	/**
	 * Atualiza a plaqueta do item de patrim�nio.
	 * 
	 * @param patrimonioitem
	 * @author Rodrigo Freitas
	 */
	public void updatePlaqueta(Patrimonioitem patrimonioitem) {
		if (patrimonioitem == null || patrimonioitem.getCdpatrimonioitem() == null) {
			throw new SinedException("Item de patrim�nio n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Patrimonioitem p set plaqueta = ? where p.id = ?", 
				new Object[]{patrimonioitem.getPlaqueta(),patrimonioitem.getCdpatrimonioitem()});
	}

	/**
	 * Verifica se a plaqueta existe. Se ela corresponde ao determinado material
	 * @param patrimonioitem
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Boolean isPlaquetaValida(Patrimonioitem patrimonioitem) {
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Patrimonioitem.class)
			.join("patrimonioitem.bempatrimonio bempatrimonio")
            .where("patrimonioitem.plaqueta = ?",patrimonioitem.getPlaqueta())
			.where("bempatrimonio = ?", patrimonioitem.getBempatrimonio())
			.unique() > 0;
	}

	/**
	 * Busca patrimonios itens pelo n�mero da plaqueta
	 * @param plaqueta
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Patrimonioitem> findPatrimonioItemByPlaqueta(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("O where in de plaquetas n�o pode ser nulo.");
		}
		whereIn = "'"+whereIn+"'";
		
		return query()
			.select("patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta")
			.whereIn("UPPER(patrimonioitem.plaqueta)", whereIn.toUpperCase())
			.list();
	}
	
	/**
	 * Busca patrimonios itens ativos pelo n�mero da plaqueta
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Patrimonioitem> findPatrimonioItemAtivosByPlaqueta(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("O where in de plaquetas n�o pode ser nulo.");
		}
		whereIn = "'"+whereIn+"'";
		
		return query()
		.select("patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, bempatrimonio.nome ")
		.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
		.whereIn("UPPER(patrimonioitem.plaqueta)", whereIn.toUpperCase())
		.where("patrimonioitem.ativo = ?", Boolean.TRUE)
		.list();
	}

	/**
	 * Carrega a lista de patrimonioitem a partir do filtro da listagem.
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Patrimonioitem> findForReport(PatrimonioitemFiltro filtro) {
		QueryBuilder<Patrimonioitem> query = querySined();
		
		query
			.select("bempatrimonio.nome, patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, " +
				"patrimonioitem.ativo, bempatrimonio.cdmaterial, bempatrimonio.valor, patrimonioitem.dtaquisicao, bempatrimonio.depreciacao," +
				"fornecimentocontrato.cdfornecimentocontrato, fornecimentocontrato.dtinicio, colaborador.cdpessoa, colaborador.nome, " +
				"fornecedor.cdpessoa, fornecedor.nome, empresa.cdpessoa, empresa.nome, empresa.razaosocial, " +
				"listaFornecimentocontratoitem.valor, listaFornecimentocontratoitem.valorequipamento, listaFornecimentocontratoitem.qtde, " +
				"material.cdmaterial, material.nome, listaMaterialnumeroserie.numero, materialnumeroserie.numero, cliente.nome, localarmazenagem.nome")
			.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
			.leftOuterJoin("patrimonioitem.materialnumeroserie materialnumeroserie")
			.leftOuterJoin("bempatrimonio.listaMaterialnumeroserie listaMaterialnumeroserie")
			.leftOuterJoin("patrimonioitem.fornecimentocontrato fornecimentocontrato")
			.leftOuterJoin("fornecimentocontrato.colaborador colaborador")
			.leftOuterJoin("fornecimentocontrato.fornecedor fornecedor")
			.leftOuterJoin("patrimonioitem.empresa empresa")
			.leftOuterJoin("fornecimentocontrato.listaFornecimentocontratoitem listaFornecimentocontratoitem")
			.leftOuterJoin("listaFornecimentocontratoitem.material material")
			.leftOuterJoin("patrimonioitem.listaMovpatrimonio listaMovpatrimonio")
			.leftOuterJoin("listaMovpatrimonio.localarmazenagem localarmazenagem")
			.leftOuterJoin("listaMovpatrimonio.cliente cliente")
			.where("bempatrimonio=?",filtro.getBempatrimonio())
			.whereLikeIgnoreAll("patrimonioitem.plaqueta", filtro.getPlaqueta())
			.where("patrimonioitem.ativo = ?",filtro.getAtivo())
			.where("listaMovpatrimonio.localarmazenagem = ?", filtro.getLocalarmazenagem())
			.where("listaMovpatrimonio.cliente = ?", filtro.getCliente());
	
		if (filtro.getBooleanplaqueta() != null) {
			if (filtro.getBooleanplaqueta()) {
				query
					.where("patrimonioitem.plaqueta is not null")
					.where("patrimonioitem.plaqueta <> ''");
			} else {
				query
					.openParentheses()
					.where("patrimonioitem.plaqueta is null")
					.or()
					.where("patrimonioitem.plaqueta = ''")
					.closeParentheses();
			}
		}
		
		query
			.openParentheses()
				.where("listaMovpatrimonio is null")
				.or()
				.where("listaMovpatrimonio.dtmovimentacao = (select max(ultimaMov.dtmovimentacao) " +
																 "  from Movpatrimonio ultimaMov " +
																 " where ultimaMov.patrimonioitem = patrimonioitem " +
																 "   and ultimaMov.dtcancelamento is null) ")
			.closeParentheses();
		
		String orderby = "";
		if(filtro.getOrderbymaterial() != null && filtro.getOrderbymaterial())
			orderby = "bempatrimonio.nome";
		if(filtro.getOrderbyplaqueta() != null && filtro.getOrderbyplaqueta())
			orderby = (!"".equals(orderby) ? (orderby + ", ") : "") + "patrimonioitem.plaqueta";
		
		if(!"".equals(orderby))
			query.orderBy(orderby);
		
		return query.list();
	}
	
	/**
	 * M�todo que busca os itens de patrimonio com a �ltima localiza��o
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Patrimonioitem> findForReportWithUltimolocal(String whereIn) {
		return querySined()
				
			.setUseTranslator(false)
			.select("new Patrimonioitem(patrimonioitem.cdpatrimonioitem, ultima_localizacao(patrimonioitem.cdpatrimonioitem)) ")
			.whereIn("patrimonioitem.cdpatrimonioitem",whereIn)
			.list();
	}

	/**
	 * Conta a quantidade disponivel de patrim�nio no local de armazenagem
	 * 
	 * @param material
	 * @return
	 * @author Tom�s Rabelo
	 * @param localarmazenagem 
	 */
	public Long getQtdDisponivelPatrimonio(Material material, Localarmazenagem localarmazenagem) {
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(beanClass)
			.join("patrimonioitem.bempatrimonio bempatrimonio")
			.join("patrimonioitem.listaMovpatrimonio listaMovpatrimonio")
			.leftOuterJoin("listaMovpatrimonio.localarmazenagem localarmazenagem")
			.where("listaMovpatrimonio.cdmovpatrimonio = (select max(m2.cdmovpatrimonio) from Movpatrimonio m2 where m2.patrimonioitem = listaMovpatrimonio.patrimonioitem and m2.dtcancelamento is null group by listaMovpatrimonio.patrimonioitem)")
			.where("bempatrimonio = ?", material)
			.where("localarmazenagem = ?",localarmazenagem)
			.where("patrimonioitem.ativo = ?", Boolean.TRUE)
			.unique();
	}

	/**
	 * Carrega a lista de itens de patrim�nio a partir de um whereIn 
	 * de entrega que n�o tenha plaqueta preenchida.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Patrimonioitem> findByEntregaWithoutPlaqueta(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Itens de patrim�nio n�o pode ser nulo.");
		}
		return query()
					.select("patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, bempatrimonio.nome, bempatrimonio.identificacao, bempatrimonio.cdmaterial")
					.join("patrimonioitem.bempatrimonio bempatrimonio")
					.join("patrimonioitem.listaMovpatrimonio listaMovpatrimonio")
					.join("listaMovpatrimonio.movpatrimonioorigem movpatrimonioorigem")
					.join("movpatrimonioorigem.entrega entrega")
					.whereIn("entrega.cdentrega", whereIn)
					.openParentheses()
					.where("patrimonioitem.plaqueta is null")
					.or()
					.where("patrimonioitem.plaqueta = ''")
					.closeParentheses()
					.list();
	}

	/**
	 * Carrega a lista de itens de patrim�nio a partir de um whereIn 
	 * de material que n�o tenha plaqueta preenchida no local especifico.
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Patrimonioitem> findMaterialSemPlaquetaNoLocal(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		String[] codigoMaterialLocal = whereIn.split(",");
		if(codigoMaterialLocal == null || codigoMaterialLocal.length != 3 || codigoMaterialLocal[0].equals("") || codigoMaterialLocal[1].equals("")){
			throw new SinedException("N�o foi recebido corretamente o c�digo do material e o c�digo do local armazenagem.");
		}
		
		return query()
			.select("patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, bempatrimonio.nome, bempatrimonio.cdmaterial")
			.join("patrimonioitem.bempatrimonio bempatrimonio")
			.join("patrimonioitem.listaMovpatrimonio listaMovpatrimonio")
			.join("listaMovpatrimonio.localarmazenagem localarmazenagem")
			.where("listaMovpatrimonio.cdmovpatrimonio = (select max(m2.cdmovpatrimonio) from Movpatrimonio m2 where m2.patrimonioitem = listaMovpatrimonio.patrimonioitem and m2.dtcancelamento is null group by listaMovpatrimonio.patrimonioitem)")
			.where("bempatrimonio.cdmaterial = ?", new Integer(codigoMaterialLocal[0]))
			.openParentheses()
			.where("patrimonioitem.plaqueta is null")
			.or()
			.where("patrimonioitem.plaqueta = ''")
			.closeParentheses()
			.where("localarmazenagem.cdlocalarmazenagem = ?", new Integer(codigoMaterialLocal[1]))
			.where("patrimonioitem.ativo = ?", Boolean.TRUE)
			.list();
	}
	
	/**
	 * M�todo para carregar um Patrimonioitem pela sua plaqueta e seu bempatrimonio.
	 * 
	 * @param patrimonioitem
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Patrimonioitem loadByPlaquetaPatrimonio(Patrimonioitem patrimonioitem){
		if(patrimonioitem == null || 
				!SinedUtil.isObjectValid(patrimonioitem.getBempatrimonio()) || 
				StringUtils.isBlank(patrimonioitem.getPlaqueta())){
			throw new SinedException("Par�metros inv�lidos.");
		}
		
		return 
			query()
			.select("patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, patrimonioitem.ativo, " +
					"bempatrimonio.cdmaterial")
			.join("patrimonioitem.bempatrimonio bempatrimonio")
			.where("UPPER(patrimonioitem.plaqueta) = ?", StringUtils.upperCase(patrimonioitem.getPlaqueta()))
			.where("bempatrimonio = ?", patrimonioitem.getBempatrimonio())
			.unique();
	}

	/**
	 * Verifica se existe a plaqueta.
	 *
	 * @param plaqueta
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean existePlaqueta(String plaqueta) {
		if(plaqueta == null || plaqueta.equals("")){
			throw new SinedException("Plaqueta n�o pode ser nula.");
		}
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Patrimonioitem.class)
					.setUseTranslator(false)
					.where("patrimonioitem.plaqueta = ?", plaqueta)
					.unique() > 0;
	}

	/**
	 * M�todo que busca todos os itens de patrim�nio que possuem material tipo igual a veiculo.
	 * 
	 * @param patrimonioitem
	 * @return
	 * @author Tom�s Rabelo
	 */
	@SuppressWarnings("unchecked")
	public List<Patrimonioitem> findComboTipoVeiculo(Patrimonioitem patrimonioitem) {
		String sql = "SELECT p.cdpatrimonioitem, p.plaqueta, m.nome " +
		 			 "FROM Patrimonioitem p " +
		 			 "JOIN Bempatrimonio b ON b.cdmaterial = p.cdbempatrimonio " +
		 			 "JOIN Material m ON b.cdmaterial = m.cdmaterial " +
		 			 "JOIN Materialtipo mt ON mt.cdmaterialtipo = m.cdmaterialtipo " +
		 			 "WHERE mt.veiculo = true AND "+
		 			 "p.ativo = true AND ";

		if(patrimonioitem != null && patrimonioitem.getCdpatrimonioitem() != null)
			sql += "(p.cdpatrimonioitem = " +patrimonioitem.getCdpatrimonioitem()+ " OR ";
		
		sql+="p.cdpatrimonioitem NOT IN (SELECT p.cdpatrimonioitem " +
									"FROM Veiculo v " +
									"WHERE v.cdpatrimonioitem = p.cdpatrimonioitem) ";
		
		if(patrimonioitem != null && patrimonioitem.getCdpatrimonioitem() != null)
			sql += ") ";
		
		sql+= "ORDER BY(m.nome)";

		SinedUtil.markAsReader();
		List<Patrimonioitem> list = getJdbcTemplate().query(sql,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						Patrimonioitem patrimonioitem = new Patrimonioitem();
						patrimonioitem.setCdpatrimonioitem(rs.getInt("CDPATRIMONIOITEM"));
						patrimonioitem.setPlaqueta(rs.getString("PLAQUETA"));
						patrimonioitem.setBempatrimonio(new Bempatrimonio(rs.getString("NOME")));
						
						return patrimonioitem;
					}
				});
	
		return list;
	}

	public List<Patrimonioitem> findForRomaneio(String whereIn) {
		if (whereIn == null || whereIn.equals("")) 
			throw new SinedException("Nenhum item selecionado.");
		
		return query()
			.select("patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, bempatrimonio.nome, bempatrimonio.cdmaterial, " +
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo")
			.join("patrimonioitem.bempatrimonio bempatrimonio")
			.join("bempatrimonio.unidademedida unidademedida")
			.whereIn("patrimonioitem.cdpatrimonioitem", whereIn)
			.list();
	}

	/**
	 * Busca os itens de patrim�nio de acordo com o material passado.
	 *
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/09/2013
	 */
	public List<Patrimonioitem> findByMaterial(Material material, Integer cdpatrimonioitem, Patrimonioitemsituacao... patrimonioitemsituacao) {
		if(material == null || material.getCdmaterial() == null){
			throw new SinedException("Material � obrigat�rio.");
		}
		QueryBuilder<Patrimonioitem> query = query()
					.select("patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, materialnumeroserie.numero")
					.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
					.leftOuterJoin("patrimonioitem.materialnumeroserie materialnumeroserie")
					.where("bempatrimonio = ?", material)
					.orderBy("patrimonioitem.plaqueta");
		
		
		if(patrimonioitemsituacao != null && patrimonioitemsituacao.length > 0){
			query.join("patrimonioitem.vpatrimonioitem vpatrimonioitem");
			query.openParentheses();
			query.openParentheses();
			for (int i = 0; i < patrimonioitemsituacao.length; i++) {
				query.where("vpatrimonioitem.situacao = ?", patrimonioitemsituacao[i]).or();
			}
			query.closeParentheses();
			query.or();
			query.where("patrimonioitem.cdpatrimonioitem = ?", cdpatrimonioitem);
			query.closeParentheses();
		}
		
		return query.list();
	}

	/**
	 * Faz a busca de itens de patrim�nio para o autocomplete
	 *
	 * @param q
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/09/2013
	 */
	public List<Patrimonioitem> findForAutocompletePatrimonioitem(String q) {
//		boolean recapagem = "TRUE".equalsIgnoreCase(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.RECAPAGEM));
		
		QueryBuilder<Patrimonioitem> queryBuilder = query()
					.join("patrimonioitem.bempatrimonio bempatrimonio")
					.openParentheses()
						.whereLikeIgnoreAll("patrimonioitem.plaqueta", q)
						.or()
						.whereLikeIgnoreAll("bempatrimonio.nome", q)
						.or()
						.where("UPPER(retira_acento(materialnumeroserie.numero)) LIKE '%'||?||'%'", Util.strings.tiraAcento(q).toUpperCase())
					.closeParentheses()
					.leftOuterJoin("patrimonioitem.materialnumeroserie materialnumeroserie")
					.orderBy("bempatrimonio.nome, patrimonioitem.plaqueta");
		
		String select = "patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, bempatrimonio.nome, materialnumeroserie.numero";
		
//		if (recapagem){
//			select += ", listaMaterialnumeroserie.numero";
//			queryBuilder.leftOuterJoin("bempatrimonio.listaMaterialnumeroserie listaMaterialnumeroserie");
//		}
		
		queryBuilder.select(select);
		
		return queryBuilder.list();
	}

	/**
	 * Atualiza o campo ativo da item de patrim�nio.
	 *
	 * @param patrimonioitem
	 * @param ativo
	 * @author Rodrigo Freitas
	 * @since 07/10/2013
	 */
	public void updateAtivo(Patrimonioitem patrimonioitem, Boolean ativo) {
		getHibernateTemplate().bulkUpdate("update Patrimonioitem p set ativo = ? where p = ?", new Object[]{
			ativo, patrimonioitem
		});
	}
	
	/**
	 * Busca a lista de patrimonioitem para enviar ao W3Producao.
	 * @param whereIn
	 * @return
	 */
	public List<Patrimonioitem> findForW3Producao(String whereIn){
		return query()
					.select("patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, patrimonioitem.ativo, bempatrimonio.cdmaterial")
					.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
					.whereIn("patrimonioitem.cdpatrimonioitem", whereIn)
					.list();
		
	}
	
	/**
	* M�todo autocomplete que busca o patrimonio
	*
	* @param q
	* @param material
	* @return
	* @since 24/02/2016
	* @author Luiz Fernando
	*/
	public List<Patrimonioitem> findForAutocompleteEquipamento(String q, Material material) {
		return query()
					.select("patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, bempatrimonio.nome")
					.join("patrimonioitem.bempatrimonio bempatrimonio")
					.where("bempatrimonio = ?", material)
					.openParentheses()
						.whereLikeIgnoreAll("patrimonioitem.plaqueta", q)
						.or()
						.whereLikeIgnoreAll("bempatrimonio.nome", q)
					.closeParentheses()
					.orderBy("bempatrimonio.nome, patrimonioitem.plaqueta")
					.list();
	}
	
	public List<Patrimonioitem> findByMaterial(Material material) {
		if (material == null || material.getCdmaterial() == null){
			throw new SinedException("Material � obrigat�rio.");
		}
		
		return query()
				.select("patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, listaMaterialnumeroserie.numero")
				.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
				.leftOuterJoin("bempatrimonio.listaMaterialnumeroserie listaMaterialnumeroserie")
				.where("bempatrimonio = ?", material)
				.orderBy("patrimonioitem.plaqueta")
				.list();
	}	

	public List<Patrimonioitem> findForCsvReport(PatrimonioitemFiltro filtro) {
		QueryBuilder<Patrimonioitem> query = querySined();
		updateListagemQuery(query, filtro);
		query.orderBy("patrimonioitem.cdpatrimonioitem");
		return query.list();
	}
	
	public List<Patrimonioitem> findByMaterial(Integer material, String plaqueta, Patrimonioitemsituacao patrimonioitemsituacao) {		
		return querySined()
				
				.select("patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, listaMaterialnumeroserie.numero, bempatrimonio.nome")
				.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
				.leftOuterJoin("bempatrimonio.listaMaterialnumeroserie listaMaterialnumeroserie")
				.leftOuterJoin("patrimonioitem.vpatrimonioitem vpatrimonioitem")
				.where("bempatrimonio.cdmaterial = ?", material)
				.whereLikeIgnoreAll("patrimonioitem.plaqueta", plaqueta)
				.where("vpatrimonioitem.situacao = ?", patrimonioitemsituacao)
				.orderBy("patrimonioitem.plaqueta")
				.list();
	}

	public List<Patrimonioitem> findByPlaqueta(String plaqueta) {
		if (plaqueta == null || "".equals(plaqueta)) {
			throw new SinedException("C�digo inv�lido.");
		}
		
		return querySined()
				
				.select("patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, vpatrimonioitem.situacao")
				.leftOuterJoin("patrimonioitem.vpatrimonioitem vpatrimonioitem")
				.where("patrimonioitem.plaqueta = ?", plaqueta)
				.list();
	}
	
	public List<Patrimonioitem> loadWithPlaqueta(String whereIn) {
		if (StringUtils.isBlank(whereIn)) {
			throw new SinedException("O whereIn n�o pode ser vazio ou nulo.");
		}
		return query()
			.select("patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, bempatrimonio.cdmaterial, bempatrimonio.nome, materialnumeroserie.numero")
			.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
			.leftOuterJoin("patrimonioitem.materialnumeroserie materialnumeroserie")
			.where("patrimonioitem.ativo = true")
			.whereIn("patrimonioitem.cdpatrimonioitem", whereIn)
			.list();
	}

}