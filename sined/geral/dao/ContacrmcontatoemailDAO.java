package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Contacrmcontatoemail;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContacrmcontatoemailDAO extends GenericDAO<Contacrmcontatoemail>{
	
	public List<Contacrmcontatoemail> findListEmailFromContacrmcontato(Contacrmcontato contacrmcontato){
		return 
			query()
				.select("contacrmcontatoemail.cdcontacrmcontatoemail, contacrmcontatoemail.email, " +
						"contacrmcontato.cdcontacrmcontato")
					.join("contacrmcontatoemail.contacrmcontato contacrmcontato")
					.where("contacrmcontato =?", contacrmcontato)
				.list();
	}
	
	public List<Contacrmcontatoemail> carregaContacrmcontatoemail(String whereIn) {
		return query()
				.select("contacrmcontatoemail.cdcontacrmcontatoemail, contacrmcontatoemail.email, " +
						"contacrmcontato.cdcontacrmcontato ")
					.join("contacrmcontatoemail.contacrmcontato contacrmcontato")
					.whereIn("contacrmcontato.cdcontacrmcontato", whereIn)
				.list();
	}

}
