package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratoarquivo;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContratoarquivoDAO extends GenericDAO<Contratoarquivo>{

	/* singleton */
	private static ContratoarquivoDAO instance;
	public static ContratoarquivoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ContratoarquivoDAO.class);
		}
		return instance;
	}
	
	/**
	 * Busca os registro de Contratoarquivo a partir do contrato passado por par�metro.
	 *
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/08/2015
	 */
	public List<Contratoarquivo> findByContrato(Contrato contrato) {
		if(contrato == null || contrato.getCdcontrato() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("contratoarquivo.cdcontratoarquivo, contratoarquivo.descricao, " +
							"contratoarquivo.dtarquivo, arquivo.cdarquivo, arquivo.nome")
					.join("contratoarquivo.arquivo arquivo")
					.where("contratoarquivo.contrato = ?", contrato)
					.list();
	}

}
