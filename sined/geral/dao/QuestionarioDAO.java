package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Questionario;
import br.com.linkcom.sined.geral.bean.Questionarioquestao;
import br.com.linkcom.sined.geral.bean.Tipopessoaquestionario;
import br.com.linkcom.sined.geral.service.QuestionarioquestaoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.QuestionarioFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class QuestionarioDAO extends GenericDAO<Questionario>{
	
	ArquivoDAO arquivoDAO;
	private QuestionarioquestaoService questionarioquestaoService;
	
	public void setQuestionarioquestaoService(
			QuestionarioquestaoService questionarioquestaoService) {
		this.questionarioquestaoService = questionarioquestaoService;
	}

	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Questionario> query, FiltroListagem _filtro) {
		
		QuestionarioFiltro filtro = (QuestionarioFiltro) _filtro;
		
		query
			.select("questionario.cdquestionario, questionario.nome, questionario.tipopessoaquestionario")
			.whereLikeIgnoreAll("questionario.nome", filtro.getNome())
			.where("questionario.tipopessoaquestionario = ?", filtro.getTipopessoaquestionario())
			.orderBy("questionario.cdquestionario");
		if(filtro.getAtividadetipo() != null){
			query.where("exists(select 1 from Questionarioquestao qq where qq.questionario = questionario and qq.atividadetipo = ?)", filtro.getAtividadetipo());
		}
	}	
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Questionario> query) {
		
		query
			.select("questionario.cdquestionario, questionario.nome, questionario.tipopessoaquestionario, questionario.mediapontuacao, questionario.ativo, questionario.quantidadeNotasMedia, " +
					"questionario.definirgrupo, listaquestionarioquestao.cdquestionarioquestao, listaquestionarioquestao.questao, listaquestionarioquestao.pontuacaosim, " +
					"listaquestionarioquestao.pontuacaonao, listaquestionarioquestao.tiporesposta, arquivo.cdarquivo, arquivo.nome, " +
					"questionario.avaliarrecebimento, questionario.obrigaavaliacaoreceber, listaquestionarioquestao.pontuacaoquestao, listaquestionarioquestao.exibirquestaoordemcompra, " +
					"listaquestionarioresposta.resposta, listaquestionarioresposta.pontuacao, listaquestionarioresposta.cdquestionarioresposta, listaQuestionariogrupo.cdquestionariogrupo, grupoquestionario.cdgrupoquestionario, " +
					"grupoquestionario.descricao, listaquestionarioquestao.obrigarresposta, atividadetipo.cdatividadetipo, atividadetipo.nome")
			.leftOuterJoin("questionario.listaquestionarioquestao listaquestionarioquestao")
			.leftOuterJoin("listaquestionarioquestao.listaquestionarioresposta listaquestionarioresposta")
			.leftOuterJoin("listaquestionarioquestao.atividadetipo atividadetipo")
			.leftOuterJoin("questionario.listaQuestionariogrupo listaQuestionariogrupo")
			.leftOuterJoin("listaQuestionariogrupo.grupoquestionario grupoquestionario")
			.leftOuterJoin("questionario.arquivo arquivo")
			.orderBy("listaquestionarioquestao.cdquestionarioquestao, listaQuestionariogrupo.cdquestionariogrupo");
			
	}

	@Override
	public void saveOrUpdate(Questionario bean) {
		if(bean.getArquivo() !=null){
			arquivoDAO.saveFile(bean, "arquivo");
		}
		if (bean instanceof br.com.linkcom.sined.util.Log){
			((br.com.linkcom.sined.util.Log) bean).setDtaltera(new Timestamp(System.currentTimeMillis()));
			if(Neo.isInRequestContext() && Neo.getUser() != null && Neo.getUser() instanceof br.com.linkcom.sined.geral.bean.Usuario){
				((br.com.linkcom.sined.util.Log) bean).setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			}
		}
		
		SaveOrUpdateStrategy save = save(bean);
		updateSaveOrUpdate(save);
		getHibernateTemplate().flush();
	}

	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				save.saveOrUpdateManaged("listaQuestionariogrupo");
				
				Questionario questionario = (Questionario) save.getEntity();
				if (questionario.getArquivo() != null){
					arquivoDAO.saveFile(questionario, "arquivo");
				}
				
				save.execute();
				
				if(questionario.getListaquestionarioquestao() != null){
					String whereIn = "";
					for(Questionarioquestao qq : questionario.getListaquestionarioquestao()){
						qq.setQuestionario(questionario);
						questionarioquestaoService.saveOrUpdateNoUseTransaction(qq);
						whereIn += qq.getCdquestionarioquestao() + ", ";
					}
					whereIn = !whereIn.isEmpty() ? whereIn.substring(0, whereIn.length()-2) : "";
					//Deleta os itens de questionarioquestao pr�-existentes no banco (somente os que est�o no banco e n�o est�o na lista)
					if(whereIn != null && !whereIn.equals("")){
						getJdbcTemplate().execute("DELETE FROM questionarioquestao qq " +
											"WHERE qq.cdquestionarioquestao NOT IN (" + whereIn + ") " +
											"AND qq.cdquestionario = " + questionario.getCdquestionario());
					}
					
				} else {//Corrige o erro de n�o apagar as quest�es do question�rio se todas forem selecionadas ao mesmo tempo ou no caso de question�rios com apenas uma quest�o.
					List<Questionarioquestao> listaQuestionarioquestao = questionarioquestaoService.loadQuestoes(questionario);
					if (SinedUtil.isListNotEmpty(listaQuestionarioquestao)) {
						getJdbcTemplate().execute("DELETE FROM questionarioquestao qq " +
								"WHERE qq.cdquestionarioquestao IN (" + SinedUtil.listAndConcatenate(listaQuestionarioquestao, "cdquestionarioquestao", ",") + ") " +
								"AND qq.cdquestionario = " + questionario.getCdquestionario());
					}
				}
				return null;
			}
		});
	}
	
	/**
	 * 
	 * M�todo que busca a lista de todos os question�rios cadastrados no sistema.
	 *
	 *@author Thiago Augusto
	 *@date 05/03/2012
	 * @return
	 */
	public List<Questionario> carregarListaQuestionario(){
		return query().list();
	}
	
	/**
	 * 
	 * M�todo para carregar o questionario para fazer a avalia��o pelo tipo de pessoa
	 *
	 *@author Thiago Augusto
	 *@date 05/03/2012
	 * @param tipopessoaquestionario
	 * @return
	 */
	public Questionario carregaQuestionarioPorTipoPessoa(Tipopessoaquestionario tipopessoaquestionario){
		return query()
		.select("questionario.cdquestionario, questionario.nome, questionario.tipopessoaquestionario, questionario.mediapontuacao, questionario.ativo, questionario.definirgrupo, " +
				"listaquestionarioquestao.cdquestionarioquestao, listaquestionarioquestao.questao, listaquestionarioquestao.pontuacaosim, " +
				"listaquestionarioquestao.pontuacaonao, listaquestionarioquestao.tiporesposta, questionario.avaliarrecebimento, questionario.obrigaavaliacaoreceber, " +
				"listaquestionarioquestao.pontuacaoquestao, listaquestionarioresposta.resposta, listaquestionarioresposta.pontuacao, " +
				"listaQuestionariogrupo.cdquestionariogrupo, grupoquestionario.cdgrupoquestionario, grupoquestionario.descricao, grupoquestionario.mediapontuacao, " +
				"listaquestionarioquestaogrupoquestionario.cdquestionarioquestao, listaquestionarioquestaogrupoquestionario.questao, listaquestionarioquestaogrupoquestionario.pontuacaosim, " +
				"listaquestionarioquestaogrupoquestionario.pontuacaonao, listaquestionarioquestaogrupoquestionario.tiporesposta, listaquestionarioquestaogrupoquestionario.exibirquestaoordemcompra, " +
				"listaquestionariorespostagrupoquestionario.cdquestionarioresposta, listaquestionariorespostagrupoquestionario.resposta, listaquestionariorespostagrupoquestionario.pontuacao, " +
				"listaquestionarioquestao.obrigarresposta, listaquestionarioquestaogrupoquestionario.obrigarresposta, " +
				"atividadetipo.cdatividadetipo, atividadetipogrupo.cdatividadetipo")
		.leftOuterJoin("questionario.listaquestionarioquestao listaquestionarioquestao")
		.leftOuterJoin("listaquestionarioquestao.atividadetipo atividadetipo")
		.leftOuterJoin("listaquestionarioquestao.listaquestionarioresposta listaquestionarioresposta")
		.leftOuterJoin("questionario.listaQuestionariogrupo listaQuestionariogrupo")
		.leftOuterJoin("listaQuestionariogrupo.grupoquestionario grupoquestionario")
		.leftOuterJoin("grupoquestionario.atividadetipo atividadetipogrupo")
		.leftOuterJoin("grupoquestionario.listaquestionarioquestao listaquestionarioquestaogrupoquestionario")
		.leftOuterJoin("listaquestionarioquestaogrupoquestionario.listaquestionarioresposta listaquestionariorespostagrupoquestionario")
		.where("questionario.tipopessoaquestionario = ?", tipopessoaquestionario)
		.where("questionario.ativo = true")
		.openParentheses()
		.where("grupoquestionario.ativo = true")
		.or()
		.where("grupoquestionario.ativo IS NULL")
		.closeParentheses()
		.openParentheses()//ABRE PRIMEIRO PARENTESE
		.openParentheses()//ABRE SEGUNDO PARENTESE
		.where("questionario.definirgrupo = false")
		.openParentheses()//ABRE TERCEIRO PARENTESE
		.where("listaquestionarioquestao.exibirquestaoordemcompra = ?", Boolean.FALSE)
		.or()
		.where("listaquestionarioquestao IS NULL")
		.closeParentheses()//FECHA TERCEIRO PARENTESE
		.closeParentheses()//FECHA SEGUNDO PARENTESE
		.or()
		.openParentheses()//ABRE SEGUNDO PARENTESE
		.where("questionario.definirgrupo = true")
		.openParentheses()//ABRE TERCEIRO PARENTESE
		.where("listaquestionarioquestaogrupoquestionario.exibirquestaoordemcompra = ?", Boolean.FALSE)
		.or()
		.where("listaquestionarioquestaogrupoquestionario IS NULL")
		.closeParentheses()//FECHA TERCEIRO PARENTESE
		.closeParentheses()//FECHA SEGUNDO PARENTESE
		.closeParentheses()//FECHA O PRIMEIRO PARENTESE
		.orderBy("listaquestionarioquestao.cdquestionarioquestao, listaQuestionariogrupo.cdquestionariogrupo, listaquestionarioquestaogrupoquestionario.cdquestionarioquestao")
		.unique();
	}
	
	/**
	 * 
	 * M�todo para buscar o Bean de Questionario.
	 *
	 *@author Thiago Augusto
	 *@date 06/03/2012
	 * @param bean
	 * @return
	 */
	public Questionario carregarQuestionarioPorId(Questionario bean){
		return query()
				.where("questionario = ? ", bean)
				.unique();
	}
	
	/**
	 * 
	 * M�todo para carregar o questionario para fazer a avalia��o pelo tipo de pessoa para ordem de compra
	 *
	 *@author Jo�o Vitor
	 *@date 02/02/2015
	 * @param tipopessoaquestionario
	 * @return
	 */
	public Questionario carregaQuestionarioPorTipoPessoaForOrdemCompra(Tipopessoaquestionario tipopessoaquestionario){
		return query()
		.select("questionario.cdquestionario, questionario.nome, questionario.tipopessoaquestionario, questionario.mediapontuacao, questionario.ativo, questionario.definirgrupo, " +
				"listaquestionarioquestao.cdquestionarioquestao, listaquestionarioquestao.questao, listaquestionarioquestao.pontuacaosim, " +
				"listaquestionarioquestao.pontuacaonao, listaquestionarioquestao.tiporesposta, listaquestionarioquestao.exibirquestaoordemcompra, questionario.avaliarrecebimento, questionario.obrigaavaliacaoreceber, " +
				"listaquestionarioquestao.pontuacaoquestao, listaquestionarioresposta.resposta, listaquestionarioresposta.pontuacao, " +
				"listaQuestionariogrupo.cdquestionariogrupo, grupoquestionario.cdgrupoquestionario, grupoquestionario.descricao, grupoquestionario.mediapontuacao, " +
				"listaquestionarioquestaogrupoquestionario.cdquestionarioquestao, listaquestionarioquestaogrupoquestionario.questao, listaquestionarioquestaogrupoquestionario.pontuacaosim, " +
				"listaquestionarioquestaogrupoquestionario.pontuacaonao, listaquestionarioquestaogrupoquestionario.tiporesposta, listaquestionarioquestaogrupoquestionario.exibirquestaoordemcompra, " +
				"listaquestionariorespostagrupoquestionario.cdquestionarioresposta, listaquestionariorespostagrupoquestionario.resposta, listaquestionariorespostagrupoquestionario.pontuacao ")
		.leftOuterJoin("questionario.listaquestionarioquestao listaquestionarioquestao")
		.leftOuterJoin("listaquestionarioquestao.listaquestionarioresposta listaquestionarioresposta")
		.leftOuterJoin("questionario.listaQuestionariogrupo listaQuestionariogrupo")
		.leftOuterJoin("listaQuestionariogrupo.grupoquestionario grupoquestionario")
		.leftOuterJoin("grupoquestionario.listaquestionarioquestao listaquestionarioquestaogrupoquestionario")
		.leftOuterJoin("listaquestionarioquestaogrupoquestionario.listaquestionarioresposta listaquestionariorespostagrupoquestionario")
		.where("questionario.tipopessoaquestionario = ?", tipopessoaquestionario)
		.where("questionario.ativo = true")
		.openParentheses()
		.where("grupoquestionario.ativo = true")
		.or()
		.where("grupoquestionario.ativo IS NULL")
		.closeParentheses()
		.openParentheses()//ABRE PRIMEIRO PARENTESE 1
		.openParentheses()//ABRE SEGUNDO PARENTESE 1
		.where("questionario.definirgrupo = false")
		.openParentheses()//ABRE TERCEIRO PARENTESE 1
		.where("listaquestionarioquestao.exibirquestaoordemcompra = ?", Boolean.TRUE)
		.or()
		.where("listaquestionarioquestao IS NULL")
		.closeParentheses()//FECHA TERCEIRO PARENTESE 1
		.closeParentheses()//FECHA SEGUNDO PARENTESE 1
		.or()
		.openParentheses()//ABRE SEGUNDO PARENTESE 2
		.where("questionario.definirgrupo = true")
		.openParentheses()//ABRE TERCEIRO PARENTESE 2
		.where("listaquestionarioquestaogrupoquestionario.exibirquestaoordemcompra = ?", Boolean.TRUE)
		.or()
		.where("listaquestionarioquestaogrupoquestionario IS NULL")
		.closeParentheses()//FECHA TERCEIRO PARENTESE 2
		.closeParentheses()//FECHA SEGUNDO PARENTESE 2
		.closeParentheses()//FECHA O PRIMEIRO PARENTESE 1
		.orderBy("listaquestionarioquestao.cdquestionarioquestao, listaQuestionariogrupo.cdquestionariogrupo, listaquestionarioquestaogrupoquestionario.cdquestionarioquestao")
		.unique();
	}
	
}