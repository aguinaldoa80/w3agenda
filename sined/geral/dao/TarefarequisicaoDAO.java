package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Tarefarequisicao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TarefarequisicaoDAO extends GenericDAO<Tarefarequisicao>{

	/**
	 * M�todo para buscar as ordem de servi�os geradas a partir da tarefa
	 *
	 * @param tarefa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Tarefarequisicao> findByTarefa(Tarefa tarefa){
		if(tarefa == null || tarefa.getCdtarefa() == null)
			throw new SinedException("Par�metro inv�lido");
		
		return query()
					.select("requisicao.cdrequisicao, requisicao.descricao, requisicaoestado.cdrequisicaoestado, " +
							"requisicaoestado.descricao, colaboradorresponsavel.cdpessoa, colaboradorresponsavel.nome ")
					.join("tarefarequisicao.requisicao requisicao")
					.leftOuterJoin("requisicao.colaboradorresponsavel colaboradorresponsavel")
					.leftOuterJoin("requisicao.requisicaoestado requisicaoestado")
					.join("tarefarequisicao.tarefa tarefa")
					.where("tarefa = ?", tarefa)
					.list();
	}
}
