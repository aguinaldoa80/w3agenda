package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Ncmcapitulo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("ncmcapitulo.cdncmcapitulo")
public class NcmcapituloDAO extends GenericDAO<Ncmcapitulo> {

}
