package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfeHistorico;
import br.com.linkcom.sined.util.SinedException;

public class ManifestoDfeHistoricoDAO extends GenericDAO<ManifestoDfeHistorico> {

	public List<ManifestoDfeHistorico> procurarPorManifestoDfe(ManifestoDfe manifestoDfe) {
		if (manifestoDfe == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("manifestoDfeHistorico.cdManifestoDfeHistorico, manifestoDfeHistorico.observacao, manifestoDfeHistorico.situacaoEnum, tipoEventoNfe.codigo, tipoEventoNfe.descricao," +
						"manifestoDfeHistorico.dtaltera, manifestoDfeHistorico.cdusuarioaltera")
				.leftOuterJoin("manifestoDfeHistorico.tipoEventoNfe tipoEventoNfe")
				.where("manifestoDfeHistorico.manifestoDfe = ?", manifestoDfe)
				.orderBy("manifestoDfeHistorico.cdManifestoDfeHistorico desc")
				.list();
	}
}