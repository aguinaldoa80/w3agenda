package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Contribuinteicmstipo;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Operacao;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.GrupotributacaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("grupotributacao.nome")
public class GrupotributacaoDAO extends GenericDAO<Grupotributacao>{

	@Override
	public void updateListagemQuery(QueryBuilder<Grupotributacao> query, FiltroListagem _filtro) {
		GrupotributacaoFiltro filtro = (GrupotributacaoFiltro) _filtro;

		query
			.select("grupotributacao.cdgrupotributacao, grupotributacao.nome, cfopgrupo.codigo, grupotributacao.tributadoicms, " +
					"grupotributacao.operacao, cfopgrupo.descricaoresumida")
			.leftOuterJoin("grupotributacao.cfopgrupo cfopgrupo")
			.whereLikeIgnoreAll("grupotributacao.nome", filtro.getNome())
			.where("cfopgrupo = ?", filtro.getCfopgrupo())
			.where("grupotributacao.operacao = ?", filtro.getOperacao())
			.where("grupotributacao.ativo = ?", filtro.getAtivo())
			.where("grupotributacao.localDestinoNfe = ?", filtro.getLocalDestinoNfe())
			.where("grupotributacao.contribuinteIcmsTipo = ?", filtro.getContribuinteIcmsTipo())
			.orderBy("grupotributacao.nome")
			.ignoreJoin("listaGrupotributacaoempresa", "empresa", "listaGrupotributacaomaterialgrupo", "materialgrupo", 
					"listaGrupotributacaonaturezaoperacao", "naturezaoperacao");
		
		if((filtro.getTributadoicms() == null || !filtro.getTributadoicms()) ||
		   (filtro.getTributadoissqn() == null || !filtro.getTributadoissqn())){
			if(filtro.getTributadoicms() != null && filtro.getTributadoicms()){
				query.where("grupotributacao.tributadoicms = true");
			}
			if(filtro.getTributadoissqn() != null && filtro.getTributadoissqn())
				query.where("grupotributacao.tributadoicms = false");
		}
		
		if(filtro.getEmpresa() != null){
			query
				.leftOuterJoin("grupotributacao.listaGrupotributacaoempresa listaGrupotributacaoempresa")
				.leftOuterJoin("listaGrupotributacaoempresa.empresa empresa")
				.where("empresa = ?", filtro.getEmpresa());
		}
		if(filtro.getMaterialgrupo() != null){
			query
				.leftOuterJoin("grupotributacao.listaGrupotributacaomaterialgrupo listaGrupotributacaomaterialgrupo")
				.leftOuterJoin("listaGrupotributacaomaterialgrupo.materialgrupo materialgrupo")
				.where("materialgrupo = ?", filtro.getMaterialgrupo());
		}
		if(filtro.getNaturezaoperacao() != null){
			query
				.leftOuterJoin("grupotributacao.listaGrupotributacaonaturezaoperacao listaGrupotributacaonaturezaoperacao")
				.leftOuterJoin("listaGrupotributacaonaturezaoperacao.naturezaoperacao naturezaoperacao")
				.where("naturezaoperacao = ?", filtro.getNaturezaoperacao());
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Grupotributacao> query) {
		query
			.select("grupotributacao.cdgrupotributacao, grupotributacao.nome, grupotributacao.operacao, grupotributacao.condicao, grupotributacao.tributacaomunicipiocliente, grupotributacao.tributacaomunicipioprojeto, " +
					"grupotributacao.origemprodutoicms, grupotributacao.tributadoicms, grupotributacao.incluirissvalor, grupotributacao.incluiricmsvalor, grupotributacao.incluiripivalor, grupotributacao.incluirpisvalor, " +
					"grupotributacao.incluircofinsvalor, grupotributacao.tipotributacaoicms, grupotributacao.tipocobrancaicms, " +
					"grupotributacao.modalidadebcicms, grupotributacao.calcularbcicmsretidoanteriormente, " +
					"grupotributacao.cest, grupotributacao.modalidadebcicmsst, grupotributacao.reducaobcicmsst, grupotributacao.margemvaloradicionalicmsst, " +
					"grupotributacao.reducaobcicms, grupotributacao.bcoperacaopropriaicms, uficmsst.cduf, uficmsst.nome,  grupotributacao.motivodesoneracaoicms, grupotributacao.percentualdesoneracaoicms, grupotributacao.abaterdesoneracaoicms, " + 
					"grupotributacao.aliquotacreditoicms, grupotributacao.tipocobrancaipi, " + 
					"grupotributacao.tipocalculoipi, grupotributacao.aliquotareaisipi, grupotributacao.tipocobrancapis, " + 
					"grupotributacao.tipocalculopis, grupotributacao.aliquotareaispis, " + 
					"grupotributacao.tipocobrancacofins, grupotributacao.valorFiscalIcms, grupotributacao.valorFiscalIpi, grupotributacao.codigoenquadramentoipi, " + 
					"grupotributacao.tipocalculocofins, grupotributacao.aliquotareaiscofins, grupotributacao.calcularDifal, " +
					"grupotributacao.tipotributacaoiss, grupotributacao.incentivofiscal, grupotributacao.exibirmensagemstpaga, " +
					"grupotributacao.exibirmensagemstpagatotal, grupotributacao.stfixa, grupotributacao.operacaonfe, " +
					"grupotributacao.infoadicionalfisco, grupotributacao.infoadicionalcontrib, " +
					"grupotributacao.aliquotaicmsst, grupotributacao.buscarfaixaimpostoaliquotaicmsst, " +
					"grupotributacao.buscarfaixaimpostomargemvaloradicionalicmsst, grupotributacao.contribuinteIcmsTipo, grupotributacao.localDestinoNfe, " +
					"grupotributacao.formulabcicms, grupotributacao.formulabcicmsdestinatario, grupotributacao.formulabcst, grupotributacao.formulavalorst, grupotributacao.formulabcipi, " +
					"grupotributacao.cdusuarioaltera, grupotributacao.naoconsideraricmsst, grupotributacao.dtaltera, " +
					"uficmsst.sigla, cfopgrupo.cdcfop, cfopgrupo.codigo, cfopgrupo.descricaoresumida, cfopgrupo.descricaocompleta, " +
					"listaGrupotributacaoempresa.cdgrupotributacaoempresa, empresa.cdpessoa, " +
					"listaGrupotributacaonaturezaoperacao.cdgrupotributacaonaturezaoperacao, naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.simplesremessa, " +
					"listaGrupotributacaoncm.cdgrupotributacaoNcm, listaGrupotributacaoncm.ncm, " +
					"listaGrupotributacaomaterialgrupo.cdgrupotributacaomaterialgrupo, materialgrupo.cdmaterialgrupo, " +
					"listaGrupotributacaoimposto.cdgrupotributacaoimposto, listaGrupotributacaoimposto.aliquotafixa, " +
					"listaGrupotributacaoimposto.basecalculopercentual, listaGrupotributacaoimposto.codigonaturezareceita, " +
					"listaGrupotributacaoimposto.controle, listaGrupotributacaoimposto.buscaraliquotafixafaixaimposto, listaGrupotributacaoimposto.retencao, " +
					"grupotributacao.naturezabcc, grupotributacao.ativo," +
					"codigocnae.cdcodigocnae, codigocnae.descricaocnae, codigocnae.cnae, " +
					"codigotributacao.cdcodigotributacao, codigotributacao.descricao, codigotributacao.codigo," +
					"itemlistaservico.cditemlistaservico, itemlistaservico.descricao, itemlistaservico.codigo," +
					"municipioincidenteiss.cdmunicipio, municipioincidenteiss.nome, ufMunicipioIncidenteIss.sigla, ufMunicipioIncidenteIss.cduf, ufMunicipioIncidenteIss.nome, " +
					"listaGrupotributacaoMaterialTipo.cdGrupotributacaoMaterialTipo, materialTipo.cdmaterialtipo, materialTipo.nome")
			.leftOuterJoin("grupotributacao.listaGrupotributacaoimposto listaGrupotributacaoimposto")
			.leftOuterJoin("grupotributacao.listaGrupotributacaoempresa listaGrupotributacaoempresa")
			.leftOuterJoin("listaGrupotributacaoempresa.empresa empresa")
			.leftOuterJoin("grupotributacao.listaGrupotributacaonaturezaoperacao listaGrupotributacaonaturezaoperacao")
			.leftOuterJoin("grupotributacao.listaGrupotributacaoncm listaGrupotributacaoncm")
			.leftOuterJoin("listaGrupotributacaonaturezaoperacao.naturezaoperacao naturezaoperacao")
			.leftOuterJoin("grupotributacao.listaGrupotributacaomaterialgrupo listaGrupotributacaomaterialgrupo")
			.leftOuterJoin("listaGrupotributacaomaterialgrupo.materialgrupo materialgrupo")
			.leftOuterJoin("grupotributacao.listaGrupotributacaoMaterialTipo listaGrupotributacaoMaterialTipo")
			.leftOuterJoin("listaGrupotributacaoMaterialTipo.materialTipo materialTipo")
			.leftOuterJoin("grupotributacao.cfopgrupo cfopgrupo")
			.leftOuterJoin("grupotributacao.uficmsst uficmsst")
			.leftOuterJoin("grupotributacao.codigocnae codigocnae")
			.leftOuterJoin("grupotributacao.codigotributacao codigotributacao")
			.leftOuterJoin("grupotributacao.itemlistaservico itemlistaservico")
			.leftOuterJoin("grupotributacao.municipioincidenteiss municipioincidenteiss")
			.leftOuterJoin("municipioincidenteiss.uf ufMunicipioIncidenteIss");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save
			.saveOrUpdateManaged("listaGrupotributacaoimposto")
			.saveOrUpdateManaged("listaGrupotributacaoempresa")
			.saveOrUpdateManaged("listaGrupotributacaonaturezaoperacao")
			.saveOrUpdateManaged("listaGrupotributacaomaterialgrupo")
			.saveOrUpdateManaged("listaGrupotributacaoncm")
			.saveOrUpdateManaged("listaGrupotributacaoMaterialTipo");
	}

	public List<Grupotributacao> findGrupotributacao(Empresa empresa, Operacao operacao, Boolean tributadoicms, Naturezaoperacao naturezaoperacao, Materialgrupo materialgrupo, Boolean considerarTributadoicms, String ncm, Materialtipo materialTipo, Localdestinonfe localDestinoNfe, Contribuinteicmstipo contribuinteIcmsTipo, Operacaonfe operacaonfe) {
		QueryBuilder<Grupotributacao> query = query();
		updateEntradaQuery(query);
		query.where("grupotributacao.ativo = true");
		
		query
			.openParentheses()
				.where("grupotributacao.operacao = ?", operacao)
				.or()
				.where("grupotributacao.operacao is null")
			.closeParentheses();
	
		if(considerarTributadoicms != null && considerarTributadoicms){
			if(tributadoicms != null && tributadoicms){
				query.where("grupotributacao.tributadoicms = true");
			}else {
				query.openParentheses()
					.where("grupotributacao.tributadoicms = false").or()
					.where("grupotributacao.tributadoicms is null")
				.closeParentheses();
			}
		}
		
		if(empresa != null && empresa.getCdpessoa() != null){
			query
			.openParentheses()
				.where("empresa = ?", empresa).or()
				.where("empresa is null")
			.closeParentheses();
		}
		
		if(ncm != null){
			query
			.openParentheses()
			.where("listaGrupotributacaoncm.ncm = ?", ncm).or()
			.where("listaGrupotributacaoncm.ncm is null")
			.closeParentheses();
		}
		if(materialgrupo != null){
			query
			.openParentheses()
				.where("materialgrupo = ?", materialgrupo).or()
				.where("materialgrupo is null")
			.closeParentheses();
		}
		if(naturezaoperacao != null && naturezaoperacao.getCdnaturezaoperacao() != null){
			query
			.openParentheses()
				.where("naturezaoperacao = ?", naturezaoperacao).or()
				.where("naturezaoperacao is null")
			.closeParentheses();
		}
		
		if(materialTipo != null && materialTipo.getCdmaterialtipo() != null) {
			query
			.openParentheses()
				.where("materialTipo = ?", materialTipo).or()
				.where("materialTipo is null")
			.closeParentheses();
		}
		if(localDestinoNfe != null) {
			query
			.openParentheses()
				.where("grupotributacao.localDestinoNfe = ?", localDestinoNfe).or()
				.where("grupotributacao.localDestinoNfe is null")
			.closeParentheses();
		}
		if(contribuinteIcmsTipo != null) {
			query
			.openParentheses()
				.where("grupotributacao.contribuinteIcmsTipo = ?", contribuinteIcmsTipo).or()
				.where("grupotributacao.contribuinteIcmsTipo is null")
			.closeParentheses();
		}
		if(operacaonfe != null){
			query
			.openParentheses()
				.where("grupotributacao.operacaonfe = ?", operacaonfe).or()
				.where("grupotributacao.operacaonfe is null")
			.closeParentheses();
		}
		
		query.orderBy("grupotributacao.nome");
		return query.list();
	}

	public Grupotributacao loadWithInfAdicional(Grupotributacao grupotributacao) {
		return query()
			.select("grupotributacao.cdgrupotributacao, grupotributacao.infoadicionalfisco, grupotributacao.infoadicionalcontrib")
			.where("grupotributacao = ?", grupotributacao)
			.unique();
	}
	
	public List<Grupotributacao> findAtivosForImportarXMLNFSe(Empresa empresa){
		String whereInEmpresas = null;
		try {
			if(empresa == null){
				whereInEmpresas = new SinedUtil().getListaEmpresa();
			} else {
				whereInEmpresas = empresa.getCdpessoa() + "";
			}
		} catch (Exception e) {}
		
		return query()
				.select("grupotributacao.cdgrupotributacao, grupotributacao.nome")
				.leftOuterJoin("grupotributacao.listaGrupotributacaoempresa listaGrupotributacaoempresa")
				.leftOuterJoin("listaGrupotributacaoempresa.empresa empresa")
				.where("grupotributacao.ativo = ?", Boolean.TRUE)
				.where("grupotributacao.tributadoicms <> ?", Boolean.TRUE)
				.openParentheses()
					.whereIn("empresa.cdpessoa", whereInEmpresas)
					.or()
					.where("empresa.cdpessoa is null")
				.closeParentheses()
				.orderBy("grupotributacao.nome")				
				.list();
	}

	/**
	* M�todo que carrega o grupo de tributa��o para preencher formula de icmsst
	*
	* @param whereIn
	* @return
	* @since 29/06/2015
	* @author Luiz Fernando
	*/
	public List<Grupotributacao> findForBaseCalculoICMSSTJavascript(String whereIn) {
		if(whereIn == null || whereIn.equals(""))
			throw new SinedException("Par�metro inv�lido");
		
		return query()
				.select("grupotributacao.cdgrupotributacao, grupotributacao.formulabcicms, grupotributacao.formulabcicmsdestinatario, grupotributacao.formulabcst, grupotributacao.formulavalorst, " +
						"grupotributacao.formulabcipi, uficmsst.cduf, uficmsst.sigla ")
				.leftOuterJoin("grupotributacao.uficmsst uficmsst")
				.whereIn("grupotributacao.cdgrupotributacao", whereIn)
				.list();
	}

	public List<Grupotributacao> findGrupoTributacaoImportacaoSped(Grupotributacao grupoTributacao) {
		if(grupoTributacao == null)
			throw new SinedException("Par�metro inv�lido");
		
		return query()
				.select("grupotributacao.cdgrupotributacao")
				.leftOuterJoin("grupotributacao.cfopgrupo cfopgrupo")
				.where("cfopgrupo.cdcfop = ?", grupoTributacao.getCfopgrupo().getCdcfop())
				.list();
	}
	
}
