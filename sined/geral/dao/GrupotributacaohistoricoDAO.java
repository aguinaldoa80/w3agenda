package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Grupotributacaohistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class GrupotributacaohistoricoDAO  extends GenericDAO<Grupotributacaohistorico>{

	/**
	 * Busca todos os registros de histórico de um determinado {@link Grupotributacao}
	 * 
	 * @author Giovane Freitas
	 * @param grupotributacao
	 * @return
	 */
	public List<Grupotributacaohistorico> findByGrupotributacao(Grupotributacao grupotributacao) {

		return query()
				.where("grupotributacaohistorico.grupotributacao = ?", grupotributacao)
				.orderBy("grupotributacaohistorico.dtaltera desc")
				.list();
	}

}
