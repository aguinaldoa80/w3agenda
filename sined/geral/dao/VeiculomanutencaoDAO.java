package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Ordemservicotipo;
import br.com.linkcom.sined.geral.bean.Veiculoordemservico;
import br.com.linkcom.sined.geral.bean.enumeration.EnumSituacaomanutencao;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculomanutencaoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VeiculomanutencaoDAO extends GenericDAO<Veiculoordemservico> {

	
	@Override
	public void updateListagemQuery(QueryBuilder<Veiculoordemservico> query, FiltroListagem _filtro) {
		VeiculomanutencaoFiltro filtro = (VeiculomanutencaoFiltro) _filtro;
		query
			.select("distinct veiculoordemservico.cdveiculoordemservico, veiculo.cdveiculo, " +
					"veiculo.placa, veiculo.prefixo, colaborador.cdpessoa, colaborador.nome, veiculoordemservico.dtrealizada, veiculoordemservico.dtprevista, " +
					"vwsituacaomanutencao.cdsituacaomanutencao")
			.join("veiculoordemservico.vwsituacaomanutencao vwsituacaomanutencao")
			.join("veiculoordemservico.veiculo veiculo")
			.leftOuterJoin("veiculo.colaborador colaborador")
			.where("colaborador=?",filtro.getColaborador())
			.whereLikeIgnoreAll("veiculo.placa",filtro.getPlaca())
			.whereLikeIgnoreAll("veiculo.prefixo",filtro.getPrefixo())
			.where("vwsituacaomanutencao.cdsituacaomanutencao = ?",filtro.getEnumSituacaomanutencao()!=null? filtro.getEnumSituacaomanutencao().getIdSitucao():null);
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Veiculoordemservico> query) {
		query
			.joinFetch("veiculoordemservico.vwsituacaomanutencao vwsituacaomanutencao")
			.leftOuterJoinFetch("veiculoordemservico.listaVeiculoordemservicoitem listaVeiculoordemservicoitem")	
			.leftOuterJoinFetch("veiculoordemservico.veiculo veiculo")
			.leftOuterJoinFetch("veiculo.veiculomodelo veiculomodelo")
			.leftOuterJoinFetch("veiculo.colaborador colaborador");
	}

	/**
	 * M�todo que verifica se ja h� manuten��o cadastrada para o ve�culo na data cadastrada
	 * 
	 * @param bean
	 * @return
	 * @Auhtor Tom�s Rabelo
	 */
	public boolean existeManutencaoData(Veiculoordemservico bean) {
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(beanClass)
			.join("veiculoordemservico.veiculo veiculo")
			.join("veiculoordemservico.ordemservicotipo ordemservicotipo")
			.join("veiculoordemservico.vwsituacaomanutencao vwsituacaomanutencao")
			.where("veiculo = ?", bean.getVeiculo())
			.where("ordemservicotipo = ?", Ordemservicotipo.MANUTENCAO)
			.where("veiculoordemservico.dtprevista = ?", bean.getDtprevista())
			.where("vwsituacaomanutencao.cdsituacaomanutencao <> ?", EnumSituacaomanutencao.CANCELADA.getIdSitucao())
			.where("veiculoordemservico <> ?", bean.getCdveiculoordemservico() != null ? bean : null)
			.unique() > 0;
	}
	
	@Override
	public ListagemResult<Veiculoordemservico> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Veiculoordemservico> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("veiculoordemservico.cdveiculoordemservico");
		
		return new ListagemResult<Veiculoordemservico>(query, filtro);
	}
}
