package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Categoriaveiculo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CategoriaveiculoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("categoriaveiculo.nome")
public class CategoriaveiculoDAO extends GenericDAO<Categoriaveiculo> {

	@Override
	public void updateListagemQuery(QueryBuilder<Categoriaveiculo> query, FiltroListagem _filtro) {
		
		CategoriaveiculoFiltro filtro = (CategoriaveiculoFiltro) _filtro;
		
		query
			.select("categoriaveiculo.cdcategoriaveiculo, categoriaveiculo.nome")
			.whereLikeIgnoreAll("categoriaveiculo.nome", filtro.getNome());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Categoriaveiculo> query) {
		query
			.leftOuterJoinFetch("categoriaveiculo.listaCategoriaiteminspecao listaCategoriaiteminspecao")
			.leftOuterJoinFetch("listaCategoriaiteminspecao.inspecaoitem inspecaoitem")
			;
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaCategoriaiteminspecao");
	}
	
	/**
	 * M�todo respons�vel por carregar uma lista de categorias partir das categorias informadas.
	 * @author Jo�o Vitor
	 * @param whereIn 
	 * @param orderBy 
	 * @param asc
	 * @return uma lista de categoria com seus itens de inspe��o
	 */
	public List<Categoriaveiculo> loadCategoriaveiculoWithListaItemInspecao(String whereIn, String orderBy, Boolean asc){
		String ordenacao = "";
		if(orderBy!=null && !orderBy.equals("")){
			ordenacao = orderBy + (asc?" asc":" desc");
		}
		return query()
			.select("categoriaveiculo.cdcategoriaveiculo, categoriaveiculo.nome, categoriaveiculo.tipocontroleveiculo, listaCategoriaiteminspecao.cdcategoriaiteminspecao, " +
					"listaCategoriaiteminspecao.venckm, listaCategoriaiteminspecao.vencperiodo, listaCategoriaiteminspecao.venchorimetro, " +
					"inspecaoitem.nome, inspecaoitem.cdinspecaoitem, inspecaoitem.visual, inspecaoitemtipo.nome")
			.join("categoriaveiculo.listaCategoriaiteminspecao listaCategoriaiteminspecao")
			.join("listaCategoriaiteminspecao.inspecaoitem inspecaoitem")
			.join("inspecaoitem.inspecaoitemtipo inspecaoitemtipo")
			.whereIn("categoriaveiculo", whereIn)
			.orderBy(ordenacao)
			.list();
	}
	
	
}
