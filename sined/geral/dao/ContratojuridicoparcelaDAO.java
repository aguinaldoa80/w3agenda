package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contratojuridico;
import br.com.linkcom.sined.geral.bean.Contratojuridicoparcela;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContratojuridicoparcelaDAO extends GenericDAO<Contratojuridicoparcela>{

	public void updateParcelaCobrada(Contratojuridico contratojuridico) {
		if(contratojuridico == null || contratojuridico.getCdcontratojuridico() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		getHibernateTemplate().bulkUpdate("update Contratojuridicoparcela cp set cp.cobrada = ? where cp.id = " +
										  "(select min(cp1.cdcontratojuridicoparcela) from Contratojuridico c " +
										  "join c.listaContratojuridicoparcela cp1 where c.id = "+contratojuridico.getCdcontratojuridico()+
										  " and (cp1.cobrada = false or cp1.cobrada is null))", 
				new Object[]{Boolean.TRUE});
	}
	
	/**
	 * 
	 * @param contratojuridico
	 * @author Thiago Clemente
	 * 
	 */
	public List<Contratojuridicoparcela> findByContratojuridico(Contratojuridico contratojuridico){
		return query()
				.select("contratojuridicoparcela.cdcontratojuridicoparcela, contratojuridicoparcela.dtvencimento," +
						"contratojuridicoparcela.valor")
				.join("contratojuridicoparcela.contratojuridico contratojuridico")
				.where("contratojuridico=?", contratojuridico)
				.orderBy("contratojuridicoparcela.dtvencimento")
				.list();
	}
}
