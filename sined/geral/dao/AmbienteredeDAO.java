package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Ambienterede;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("ambienterede.nome")
public class AmbienteredeDAO extends GenericDAO<Ambienterede>{
	
	/**
	 * Retorna um objeto de acordo com o nome informado
	 * @param nome
	 * @return
	 * @author Thiago Gon�alves
	 */
	public Ambienterede findByNome(String nome){
		return query().whereLikeIgnoreAll("nome", nome)
					  .setMaxResults(1)
					  .unique();
	}
	
}
