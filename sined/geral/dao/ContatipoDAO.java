package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("contatipo.nome")
public class ContatipoDAO extends GenericDAO<Contatipo> {

	/**
	 * Retorna uma Contatipo a partir de um agendamento. Pode retornar null.
	 * 
	 * @param agendamento
	 * @return
	 * @author Hugo Ferreira
	 */
	public Contatipo carregarPor(Agendamento agendamento) {
		if (agendamento == null || agendamento.getCdagendamento() == null) {
			return null;
		}
		return query()
			.select("contatipo.cdcontatipo, contatipo.nome")
			.leftOuterJoin("contatipo.listaConta conta")
			.leftOuterJoin("conta.listaAgendamento agendamento")
			.where("agendamento = ?", agendamento)
			.unique();
	}
	
	/**
	 * M�todo para buscar os tipos de conta por empresa.
	 * 
	 * @param empresa
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Contatipo> findByEmpresa(Empresa empresa){
		return 
			query()
			.select("contatipo.cdcontatipo,contatipo.nome")
			.join("contatipo.listaConta conta")
			.leftOuterJoin("conta.listaContaempresa listaContaempresa")
			.leftOuterJoin("listaContaempresa.empresa empresa")
			.openParentheses()
				.where("empresa = ?",empresa)
				.or()
				.where("listaContaempresa is null")
			.closeParentheses()
			.list();
	}

	/**
	 * M�todo que busca contatipo para o combo de acordo com o par�metro 
	 *
	 * @param whereInContatipo
	 * @param whereInEmpresas
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contatipo> findForComboMovimentacao(String whereInContatipo, String whereInEmpresas) {
		QueryBuilder<Contatipo> query = query()
			.select("contatipo.cdcontatipo,contatipo.nome, conta.cdconta, conta.nome, empresa.cdpessoa")
			.join("contatipo.listaConta conta")
			.leftOuterJoin("conta.listaContaempresa listaContaempresa")
			.leftOuterJoin("listaContaempresa.empresa empresa");
		
		if(whereInEmpresas != null && !"".equals(whereInEmpresas)){
			query.openParentheses()
				.whereIn("empresa.cdpessoa",whereInEmpresas)
				.or()
				.where("listaContaempresa is null")
			.closeParentheses();
		}
		
		return query
				.whereIn("contatipo.cdcontatipo", whereInContatipo)
				.list();
	}
	
	public List<Contatipo> findForAndroid(String whereIn) {
		return query()
		.select("contatipo.cdcontatipo, contatipo.nome")
		.whereIn("contatipo.cdcontatipo", whereIn)
		.list();
	}

}
