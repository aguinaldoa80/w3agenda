package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendahistorico;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoagendahistoricoDAO extends GenericDAO<Producaoagendahistorico> {

	/**
	 * Busca a lista de hist�rico de acordo com a Producaoagenda.
	 *
	 * @param producaoagenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Producaoagendahistorico> findByProducaoagenda(Producaoagenda producaoagenda) {
		if(producaoagenda == null || producaoagenda.getCdproducaoagenda() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("producaoagendahistorico.cdproducaoagendahistorico, producaoagendahistorico.dtaltera, " +
							"producaoagendahistorico.cdusuarioaltera, producaoagendahistorico.observacao")
					.join("producaoagendahistorico.producaoagenda producaoagenda")
					.where("producaoagenda = ?", producaoagenda)
					.orderBy("producaoagendahistorico.dtaltera desc, producaoagendahistorico.cdproducaoagendahistorico desc")
					.list();
	}
	
}
