package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Pessoamaterial;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PessoamaterialDAO extends GenericDAO<Pessoamaterial>{
	
	/**
	 * Busca pessoamaterial de uma pessoa
	 *
	 * @param pessoa
	 * @return
	 * @author Mairon
	 * @since 25/11/2016
	 */
	public List<Pessoamaterial> findByPessoa(Pessoa pessoa){
		return query()
					.select("pessoamaterial.cdpessoamaterial, pessoamaterial.disponibilidademensal, pessoamaterial.disponibilidadeanual,"+
							"pessoamaterial.mesinicial, pessoamaterial.mesfinal, material.cdmaterial, material.nome, unidademedida.cdunidademedida, unidademedida.nome")
					.where("pessoamaterial.pessoa = ?", pessoa)
					.join("pessoamaterial.material material")
					.join("material.unidademedida unidademedida")
					.orderBy("material.nome, pessoamaterial.mesinicial")
					.list();
	}

}
