package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Tarefaorcamentorg;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TarefaorcamentorgDAO extends GenericDAO<Tarefaorcamentorg>{

	/**
	 * Busca os recursos gerais das tarefas de um determinado or�amento
	 *
	 * @param orcamento
 	 * @return List<Tarefaorcamentorg>
 	 * 
 	 * @throws SinedException - caso o or�amento seja nulo
	 * 
 	 * @author Rodrigo Alvarenga
	 */	
	public List<Tarefaorcamentorg> findByOrcamentoFlex(Orcamento orcamento) {
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		return 
			query()
				.select("tarefaorcamentorg.cdtarefaorcamentorg, tarefaorcamentorg.quantidade, " +
						"material.cdmaterial, material.nome, material.valorcusto, material.valorvenda, " +
						"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
						"tarefaorcamentorg.tiporecursogeral, tarefaorcamentorg.outro, unidademedidarg.cdunidademedida, " +
						"unidademedidarg.nome, unidademedidarg.simbolo")				
				.leftOuterJoin("tarefaorcamentorg.unidademedida unidademedidarg")
				.leftOuterJoin("tarefaorcamentorg.material material")
				.leftOuterJoin("material.unidademedida unidademedida")				
				.join("tarefaorcamentorg.tarefaorcamento tarefaorcamento")
				.join("tarefaorcamento.orcamento orcamento")
				.where("orcamento = ?",orcamento)
				.list();
	}

	public List<Tarefaorcamentorg> findByOrcamento(Orcamento orcamento) {
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		return 
			query()
				.select("tarefaorcamentorg.cdtarefaorcamentorg, tarefaorcamentorg.quantidade, " +
						"tarefaorcamentorg.tiporecursogeral, tarefaorcamentorg.outro, " +
						"material.cdmaterial, material.nome, material.valorcusto, material.valorvenda")				
				.leftOuterJoin("tarefaorcamentorg.material material")
				.join("tarefaorcamentorg.tarefaorcamento tarefaorcamento")
				.join("tarefaorcamento.orcamento orcamento")
				.where("orcamento = ?",orcamento)
				.list();
	}

}
