package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecimentocontrato;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.AcompanhamentoEconomicoReportFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.FornecimentocontratoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

@DefaultOrderBy("fornecimentocontrato.descricao")
public class FornecimentocontratoDAO extends GenericDAO<Fornecimentocontrato> {
	
	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Fornecimentocontrato> query, FiltroListagem _filtro) {
		FornecimentocontratoFiltro filtro = (FornecimentocontratoFiltro) _filtro;
		query
			.select("fornecimentocontrato.cdfornecimentocontrato, fornecimentocontrato.descricao, fornecimentocontrato.dtinicio, " +
					"fornecimentocontrato.dtfim, fornecedor.nome, projeto.nome, fornecimentocontrato.valor, fornecimentocontrato.ativo, " +
					"fornecimentotipo.nome")
			.leftOuterJoin("fornecimentocontrato.projeto projeto")
			.leftOuterJoin("fornecimentocontrato.fornecimentotipo fornecimentotipo")
			.leftOuterJoin("fornecimentocontrato.fornecedor fornecedor")
			.where("fornecedor = ?",filtro.getFornecedor())
			.whereLikeIgnoreAll("fornecimentocontrato.descricao", filtro.getDescricao())
			.where("fornecimentotipo = ?",filtro.getFornecimentotipo())
			.where("fornecimentocontrato.ativo = ?",filtro.getAtivo())
			.where("fornecimentocontrato.centrocusto = ?",filtro.getCentrocusto())
			.where("projeto = ?",filtro.getProjeto())
			.where("fornecimentocontrato.empresa = ?", filtro.getEmpresa());
		if(filtro.getDtinicio() != null && filtro.getDtfim() != null){
			query
				.openParentheses()
				.openParentheses()
				.where("fornecimentocontrato.dtinicio <= ?",filtro.getDtfim())
				.where("fornecimentocontrato.dtfim >= ?",filtro.getDtinicio())
				.closeParentheses()
				.or()
				.openParentheses()
				.where("fornecimentocontrato.dtinicio <= ?",filtro.getDtfim())
				.where("fornecimentocontrato.dtfim is null")
				.closeParentheses()
				.closeParentheses();
		} else if(filtro.getDtinicio() != null){
			query
				.openParentheses()
				.where("fornecimentocontrato.dtfim >= ?",filtro.getDtinicio())
				.or()
				.where("fornecimentocontrato.dtfim is null")
				.closeParentheses();
		} else if(filtro.getDtfim() != null){
			query
				.where("fornecimentocontrato.dtinicio <= ?",filtro.getDtfim());
		}
			
			query.orderBy("fornecimentocontrato.cdfornecimentocontrato desc");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Fornecimentocontrato> query) {
		((QueryBuilderSined<Fornecimentocontrato>) query).setUseWhereFornecedorEmpresa(true);
		
		query
			.select("fornecimentocontrato.cdfornecimentocontrato, fornecimentocontrato.descricao, fornecedor.nome, fornecedor.cdpessoa, " +
					"centrocusto.cdcentrocusto, projeto.cdprojeto, contagerencial.cdcontagerencial, fornecimentocontrato.valor, prazopagamento.cdprazopagamento, " +
					"fornecimentocontrato.dtproximovencimento, fornecimentocontrato.dtinicio, fornecimentocontrato.dtfim, fornecimentocontrato.observacao, " +
					"fornecimentocontrato.ativo, fornecimentotipo.cdfornecimentotipo, fornecimentocontrato.dtaltera, fornecimentocontrato.cdusuarioaltera, " +
					"fornecimentocontrato.ir, fornecimentocontrato.incideir, fornecimentocontrato.iss, fornecimentocontrato.incideiss, fornecimentocontrato.pis, fornecimentocontrato.incidepis, " +
					"fornecimentocontrato.cofins, fornecimentocontrato.incidecofins, fornecimentocontrato.csll, fornecimentocontrato.incidecsll, fornecimentocontrato.inss, fornecimentocontrato.incideinss, " +
					"fornecimentocontrato.icms, fornecimentocontrato.incideicms, fornecimentocontrato.codigogps, empresa.cdpessoa, " +
					"listaFornecimentocontratoitem.descricao, listaFornecimentocontratoitem.qtde, listaFornecimentocontratoitem.observacao, listaFornecimentocontratoitem.cdfornecimentocontratoitem, " +
					"fluxocaixa.cdfluxocaixa, rateio.cdrateio, colaborador.cdpessoa, colaborador.nome, " +
					"arquivo.cdarquivo, arquivo.nome, listaFornecimentocontratoitem.fornecimentocontratoitemtipo, material.cdmaterial, material.nome, " +
					"listaFornecimentocontratoitem.valorequipamento, listaFornecimentocontratoitem.valor ")
			.leftOuterJoin("fornecimentocontrato.listaFornecimentocontratoitem listaFornecimentocontratoitem")
			.leftOuterJoin("fornecimentocontrato.colaborador colaborador")
			.leftOuterJoin("fornecimentocontrato.fornecedor fornecedor")
			.join("fornecimentocontrato.centrocusto centrocusto")
			.leftOuterJoin("fornecimentocontrato.projeto projeto")
			.join("fornecimentocontrato.contagerencial contagerencial")
			.join("fornecimentocontrato.prazopagamento prazopagamento")
			.leftOuterJoin("fornecimentocontrato.fornecimentotipo fornecimentotipo")
			.leftOuterJoin("fornecimentocontrato.fluxocaixa fluxocaixa")
			.leftOuterJoin("fluxocaixa.rateio rateio")
			.leftOuterJoin("fornecimentocontrato.arquivo arquivo")
			.leftOuterJoin("listaFornecimentocontratoitem.material material")
			.leftOuterJoin("fornecimentocontrato.empresa empresa");
	}	
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaFornecimentocontratoitem");
		
		Fornecimentocontrato fornecimentocontrato = (Fornecimentocontrato) save.getEntity();
		if (fornecimentocontrato.getArquivo() != null) {
			arquivoDAO.saveFile(fornecimentocontrato, "arquivo");
		}
	}

	/**
	 * M�todo que busca informa��es para a tela de fornecimento
	 * 
	 * @param fornecimentocontrato
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Fornecimentocontrato carregaFornecimentocontrato(Fornecimentocontrato fornecimentocontrato) {
		return query()
				.select("fornecimentocontrato.valor, listaFornecimentocontratoitem.qtde, listaFornecimentocontratoitem.descricao, material.nome, listaFornecimentocontratoitem.fornecimentocontratoitemtipo")
				.leftOuterJoin("fornecimentocontrato.listaFornecimentocontratoitem listaFornecimentocontratoitem")
				.leftOuterJoin("listaFornecimentocontratoitem.material material")
				.where("fornecimentocontrato = ?", fornecimentocontrato)
				.unique();
	}
	
	/**
	 * Respons�vel por atualizar a data do proximo vencimento do contrato de fornecimento.
	 * @param fornecimentocontrato
	 * @author Taidson
	 * @since 25/06/2010
	 */
	public void updateDtproximovencimento(Fornecimentocontrato fornecimentocontrato) {
		if (fornecimentocontrato == null || fornecimentocontrato.getCdfornecimentocontrato() == null) {
			throw new SinedException("Fornecimento contrato n�o podem ser nulo.");
		}
		getJdbcTemplate().update("update fornecimentocontrato " +
				"set dtproximovencimento = ?, dtfim = ? where cdfornecimentocontrato = ?", 
				new Object[]{fornecimentocontrato.getDtproximovencimento(),
						fornecimentocontrato.getDtfim(),
						fornecimentocontrato.getCdfornecimentocontrato()}); 
	}

	/**
	 * M�todo que calcula o valor de fornecimentos.
	 * 
	 * @param filtro
	 * @param contagerencial
	 * @return
	 * 
	 * @author Giovane Freitas
	 */
	public Money getValorTotalProjeto(AcompanhamentoEconomicoReportFiltro filtro, Contagerencial contagerencial) {
		if(filtro == null || filtro.getProjeto() == null || filtro.getProjeto().getCdprojeto() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		QueryBuilder<Long> query = newQueryBuilder(Long.class)
			.from(Fornecimentocontrato.class)
			.select("SUM(fornecimentocontrato.valor)")
			.leftOuterJoin("fornecimentocontrato.contagerencial contagerencial")
			
			//listando TODAS as contas gerenciais filhas
			.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			.openParentheses()
				.where("vcontagerencial.identificador like '" + contagerencial.getVcontagerencial().getIdentificador() + "%'")
				.or()
				.where("fornecimentocontrato.contagerencial = ?", contagerencial)
			.closeParentheses()

			//lista apenas as contas gerenciais iguais
			//.where("contagerencial = ?", contagerencial)
			
			.where("fornecimentocontrato.projeto = ?", filtro.getProjeto())
			.where("fornecimentocontrato.dtinicio <= ?", filtro.getDtFim());
		
		Long totalFornecimento = query.setUseTranslator(false).unique();
		
		if (totalFornecimento != null)
			return new Money(totalFornecimento, true);
		else 
			return new Money();
	}
	
	public List<Fornecimentocontrato> findForCSV(){
		return querySined()
				
		.select("fornecimentocontrato.cdfornecimentocontrato, fornecimentocontrato.descricao, fornecedor.nome, fornecedor.cdpessoa, " +
				"centrocusto.cdcentrocusto, centrocusto.nome, projeto.cdprojeto, projeto.nome, contagerencial.cdcontagerencial, contagerencial.nome, fornecimentocontrato.valor, prazopagamento.cdprazopagamento, prazopagamento.nome," +
				"fornecimentocontrato.dtproximovencimento, fornecimentocontrato.dtinicio, fornecimentocontrato.dtfim, fornecimentocontrato.observacao, " +
				"fornecimentocontrato.ativo, fornecimentotipo.cdfornecimentotipo, fornecimentotipo.nome, fornecimentocontrato.dtaltera, fornecimentocontrato.cdusuarioaltera, " +
				"fornecimentocontrato.ir, fornecimentocontrato.incideir, fornecimentocontrato.iss, fornecimentocontrato.incideiss, fornecimentocontrato.pis, fornecimentocontrato.incidepis, " +
				"fornecimentocontrato.cofins, fornecimentocontrato.incidecofins, fornecimentocontrato.csll, fornecimentocontrato.incidecsll, fornecimentocontrato.inss, fornecimentocontrato.incideinss, " +
				"fornecimentocontrato.icms, fornecimentocontrato.incideicms, fornecimentocontrato.codigogps, " +
				"listaFornecimentocontratoitem.descricao, listaFornecimentocontratoitem.qtde, listaFornecimentocontratoitem.observacao, listaFornecimentocontratoitem.cdfornecimentocontratoitem, " +
				"fluxocaixa.cdfluxocaixa, fluxocaixa.descricao")
		.leftOuterJoin("fornecimentocontrato.listaFornecimentocontratoitem listaFornecimentocontratoitem")
		.leftOuterJoin("fornecimentocontrato.fornecedor fornecedor")
		.join("fornecimentocontrato.centrocusto centrocusto")
		.leftOuterJoin("fornecimentocontrato.projeto projeto")
		.join("fornecimentocontrato.contagerencial contagerencial")
		.join("fornecimentocontrato.prazopagamento prazopagamento")
		.leftOuterJoin("fornecimentocontrato.fornecimentotipo fornecimentotipo")
		.leftOuterJoin("fornecimentocontrato.fluxocaixa fluxocaixa")
		.where("fornecimentocontrato.ativo = ?", Boolean.TRUE)
		.list();
	}

	/**
	 * M�todo que carrega os contratos de fornecimento
	 *
	 * @param listaFornecimentocontrato
	 * @param ativo
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Fornecimentocontrato> findForCombo(List<Fornecimentocontrato> listaFornecimentocontrato, Boolean ativo) {
		return query()
			.select("fornecimentocontrato.cdfornecimentocontrato, fornecimentocontrato.descricao")
			.openParentheses()
				.where("fornecimentocontrato.ativo = ?", ativo)
				.or()
				.whereIn("fornecimentocontrato.cdfornecimentocontrato", SinedUtil.listAndConcatenate(listaFornecimentocontrato, "cdfornecimentocontrato", ","))
			.closeParentheses()
			.list();
	}

	/**
	 * 
	 * @param fornecimentocontrato
	 * @return
	 */
	public boolean exibirMsgContratoDuplicado(Fornecimentocontrato fornecimentocontrato) {
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Fornecimentocontrato.class)
			.leftOuterJoin("fornecimentocontrato.fornecedor fornecedor")
			.leftOuterJoin("fornecimentocontrato.empresa empresa")
			.where("fornecedor = ?", fornecimentocontrato.getFornecedor())
			.where("empresa = ?", fornecimentocontrato.getEmpresa())
			.where("valor = ?", fornecimentocontrato.getValor())
			.where("cdfornecimentocontrato <> ?", fornecimentocontrato.getCdfornecimentocontrato())
			.unique().intValue() > 0;
	}
	
	/**
	 * M�todo que carrega os contratos de fornecimento por empresa
	 *
	 * @param empresa
	 * @param ativo
	 * @return
	 * @author Lucas Costa
	 */
	public List<Fornecimentocontrato> findByEmpresa(Empresa empresa) {
		return query()
			.select("fornecimentocontrato.cdfornecimentocontrato, fornecimentocontrato.descricao")
			.where("fornecimentocontrato.ativo = ?", Boolean.TRUE)
			.openParentheses()
				.where("fornecimentocontrato.empresa = ?", empresa)
				.or()
				.where("fornecimentocontrato.empresa is null", (empresa !=null))
			.closeParentheses()
			.list();
	}
}
