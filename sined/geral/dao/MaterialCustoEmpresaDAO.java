package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialCustoEmpresa;
import br.com.linkcom.sined.util.SinedException;

public class MaterialCustoEmpresaDAO extends br.com.linkcom.sined.util.neo.persistence.GenericDAO<MaterialCustoEmpresa> {
	
	public List<MaterialCustoEmpresa> findByMaterial(Material material, Empresa empresa) {
		QueryBuilder<MaterialCustoEmpresa> query = query()
				.select("materialCustoEmpresa.cdMaterialCustoEmpresa, materialCustoEmpresa.quantidadeReferencia, materialCustoEmpresa.dtInicio, " +
						"materialCustoEmpresa.custoInicial, materialCustoEmpresa.custoMedio, materialCustoEmpresa.valorUltimaCompra, " +
						"empresa.cdpessoa, empresa.nomefantasia, " +
						"material.cdmaterial, material.nome")
				.leftOuterJoin("materialCustoEmpresa.material material")
				.leftOuterJoin("materialCustoEmpresa.empresa empresa")
				.where("material = ?", material)
				.where("empresa = ?", empresa);
		
		return query.list();
	}
	
	public void updateValorCusto(MaterialCustoEmpresa materialCustoEmpresa, Double valorCustoMedio) {
		if(materialCustoEmpresa != null && materialCustoEmpresa.getCdMaterialCustoEmpresa() != null && valorCustoMedio != null){
			getJdbcTemplate().update("update materialCustoEmpresa set custoMedio = ? " +
					" where cdMaterialCustoEmpresa = ? ", new Object[]{valorCustoMedio, materialCustoEmpresa.getCdMaterialCustoEmpresa()});
		}
	}
	
	public void updateQuantidadeReferencia(MaterialCustoEmpresa materialCustoEmpresa, Double quantidade) {
		if(materialCustoEmpresa != null && materialCustoEmpresa.getCdMaterialCustoEmpresa() != null && quantidade != null){
			getJdbcTemplate().update("update materialCustoEmpresa set quantidadeReferencia = ? " +
					" where cdMaterialCustoEmpresa = ? ", new Object[]{quantidade, materialCustoEmpresa.getCdMaterialCustoEmpresa()});
		}
	}
	
	public void updateValorUltimaCompra(MaterialCustoEmpresa materialCustoEmpresa, Double valorUltimaCompra) {
		if(materialCustoEmpresa != null && materialCustoEmpresa.getCdMaterialCustoEmpresa() != null && valorUltimaCompra != null){
			getJdbcTemplate().update("update materialCustoEmpresa set valorUltimaCompra = ? " +
					" where cdMaterialCustoEmpresa = ? ", new Object[]{valorUltimaCompra, materialCustoEmpresa.getCdMaterialCustoEmpresa()});
		}
	}
	
	public MaterialCustoEmpresa loadforSpedPisCofins(Material material, Empresa empresa) {
		if(material == null || material.getCdmaterial() == null || empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				
				.select("materialCustoEmpresa.cdMaterialCustoEmpresa, contaContabil.cdcontacontabil, vcontacontabil.identificador")
				.leftOuterJoin("materialCustoEmpresa.empresa empresa")
				.leftOuterJoin("materialCustoEmpresa.material material")
				.leftOuterJoin("materialCustoEmpresa.contaContabil contaContabil")
				.leftOuterJoin("contaContabil.vcontacontabil vcontacontabil")
				.where("material = ?", material)
				.where("empresa = ?", empresa)
				.unique();
	}

	public MaterialCustoEmpresa loadForLancamentoContabil(Material material, Empresa empresa) {
		if(Util.objects.isNotPersistent(material) || Util.objects.isNotPersistent(material)){
			return null;
		}
		QueryBuilder<MaterialCustoEmpresa> query = query()
				.select("materialCustoEmpresa.cdMaterialCustoEmpresa, materialCustoEmpresa.quantidadeReferencia, materialCustoEmpresa.dtInicio, " +
						"materialCustoEmpresa.custoInicial, materialCustoEmpresa.custoMedio, materialCustoEmpresa.valorUltimaCompra, " +
						"empresa.cdpessoa, empresa.nomefantasia, " +
						"material.cdmaterial, material.nome, " +
						"contaContabil.cdcontacontabil")
				.leftOuterJoin("materialCustoEmpresa.material material")
				.leftOuterJoin("materialCustoEmpresa.empresa empresa")
				.leftOuterJoin("materialCustoEmpresa.contaContabil contaContabil")
				.where("material = ?", material)
				.where("empresa = ?", empresa);
		
		return query.setMaxResults(1)
					.unique();
	}
}
