package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Dominiozonadkim;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.DominiozonadkimFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DominiozonadkimDAO extends GenericDAO<Dominiozonadkim>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Dominiozonadkim> query, FiltroListagem _filtro) {
		DominiozonadkimFiltro filtro = (DominiozonadkimFiltro)_filtro;
		query.leftOuterJoinFetch("dominiozonadkim.dominio dominio")
			 .leftOuterJoinFetch("dominio.contratomaterial contratomaterial")
			 .leftOuterJoinFetch("contratomaterial.contrato contrato")			 
			 .where("contrato.cliente = ?", filtro.getCliente())
			 .where("contrato = ?", filtro.getContrato())
			 .where("dominio = ?", filtro.getDominio())
			 .whereLikeIgnoreAll("dominiozonadkim.nome", filtro.getNome());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Dominiozonadkim> query) {
		query.leftOuterJoinFetch("dominiozonadkim.dominio dominio")
		     .leftOuterJoinFetch("dominio.contratomaterial contratomaterial")
		     .leftOuterJoinFetch("contratomaterial.contrato contrato");
	}
}
