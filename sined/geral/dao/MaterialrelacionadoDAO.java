package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialrelacionado;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialrelacionadoDAO extends GenericDAO<Materialrelacionado> {

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaMaterialrelacionadoformula");
	}
	
	public void deleteWhereNotInMaterial(Material material, String whereIn) {
		if(whereIn != null && !whereIn.equals("")){
			getHibernateTemplate().bulkUpdate("delete from Materialrelacionado mr where mr.material = ? and mr.cdmaterialrelacionado not in (" + whereIn + ")", new Object[]{material});
		} else {
			getHibernateTemplate().bulkUpdate("delete from Materialrelacionado mr where mr.material = ?", new Object[]{material});
		}
	}
	
	/**
	 * Busca os materiais da promo��o
	 *
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Materialrelacionado> findByPromocao(Material material) {
		if(material == null || material.getCdmaterial() == null){
			throw new SinedException("Material n�o pode ser nulo.");
		}
		return query()
					.select("materialpromocao.cdmaterial, materialpromocao.nome, materialrelacionado.valorvenda, materialrelacionado.quantidade, " +
							"materialpromocao.produto, materialpromocao.servico, materialpromocao.tributacaoestadual, materialpromocao.valorcusto, " +
							"contagerencialvenda.cdcontagerencial, centrocustovenda.cdcentrocusto, vcontagerencialvenda.identificador, " +
							"vcontagerencialvenda.arvorepai, contagerencialvenda.nome")
					.join("materialrelacionado.material material")
					.join("materialrelacionado.materialpromocao materialpromocao")
					.leftOuterJoin("materialpromocao.contagerencialvenda contagerencialvenda")
					.leftOuterJoin("contagerencialvenda.vcontagerencial vcontagerencialvenda")
					.leftOuterJoin("materialpromocao.centrocustovenda centrocustovenda")
					.where("material = ?", material)
					.orderBy("materialpromocao.nome")
					.list();
	}

	/**
	 * Busca os dados para o cache da tela de pedido de venda offline 
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Materialrelacionado> findForPVOffline() {
		return query()
			.select("materialrelacionado.cdmaterialrelacionado, materialpromocao.cdmaterial, materialrelacionado.ordemexibicao, " +
					"materialrelacionado.valorvenda, materialrelacionado.quantidade, material.cdmaterial ")
			.join("materialrelacionado.material material")
			.join("materialrelacionado.materialpromocao materialpromocao")
			.list();
	}
	
	public List<Materialrelacionado> findForAndroid(String whereIn) {
		return query()
			.select("materialrelacionado.cdmaterialrelacionado, materialpromocao.cdmaterial, materialrelacionado.ordemexibicao, " +
					"materialrelacionado.valorvenda, materialrelacionado.quantidade, material.cdmaterial ")
			.join("materialrelacionado.material material")
			.join("materialrelacionado.materialpromocao materialpromocao")
			.whereIn("materialrelacionado.cdmaterialrelacionado", whereIn)
			.list();
	}

}
