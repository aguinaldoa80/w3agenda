package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialdevolucao;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MaterialdevolucaoFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.report.bean.MaterialdevolucaoQueryBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialdevolucaoDAO extends GenericDAO<Materialdevolucao> {
	
	private MaterialcategoriaService materialcategoriaService;
	
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {this.materialcategoriaService = materialcategoriaService;}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Materialdevolucao> query, FiltroListagem _filtro) {
		MaterialdevolucaoFiltro filtro = (MaterialdevolucaoFiltro) _filtro;
		
		query
			.select("materialdevolucao.cdmaterialdevolucao, materialdevolucao.preco, materialdevolucao.qtdevendida, " +
					"materialdevolucao.qtdedevolvida, materialdevolucao.placaveiculo, materialdevolucao.numeronotadevolucao, " +
					"venda.cdvenda, material.cdmaterial, material.nome, materialdevolucao.cdusuarioaltera,materialdevolucao.dtcancelamento," +
					" materialdevolucao.dtaltera, loteestoqueVM.cdloteestoque, loteestoqueVM.validade, loteestoqueVM.numero, " +
					"loteestoquePVM.cdloteestoque, loteestoquePVM.validade, loteestoquePVM.numero")
			.leftOuterJoin("materialdevolucao.venda venda")
			.leftOuterJoin("materialdevolucao.pedidovenda pedidovenda")
			.leftOuterJoin("materialdevolucao.material material")
			.leftOuterJoin("materialdevolucao.pedidovendamaterial pedidovendamaterial")
			.leftOuterJoin("materialdevolucao.vendamaterial vendamaterial")
			.leftOuterJoin("pedidovendamaterial.loteestoque loteestoquePVM")
			.leftOuterJoin("vendamaterial.loteestoque loteestoqueVM")
			.leftOuterJoin("venda.empresa empresa")
			.leftOuterJoin("venda.colaborador colaborador")
			.where("material = ?", filtro.getMaterial())
			.where("empresa = ?", filtro.getEmpresa())
			.where("colaborador = ?", filtro.getColaborador())
			.where("coalesce(loteestoqueVM, loteestoquePVM) = ?", filtro.getLoteestoque())
			.where("venda.dtvenda >= ?", filtro.getDtiniciovenda())
			.where("venda.dtvenda <= ?", filtro.getDtfimvenda())
			.where("materialdevolucao.dtaltera >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getDtiniciodevolucao()))
			.where("materialdevolucao.dtaltera <= ?", SinedDateUtils.dateToTimestampFinalDia(filtro.getDtfimdevolucao()))
			.whereIn("venda.cdvenda", filtro.getWhereInCdVenda())
			.whereIn("pedidovenda.cdpedidovenda", filtro.getWhereInCdPedidoVenda())
			.where("materialdevolucao.dtcancelamento is null", filtro.getCanceladas() == null || !filtro.getCanceladas());
		
		if(filtro.getMaterialcategoria() != null){
			materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
			query
				.leftOuterJoin("material.materialcategoria materialcategoria")
				.leftOuterJoin("materialcategoria.vmaterialcategoria vmaterialcategoria")
				.openParentheses()
					.where("vmaterialcategoria.identificador like ? ||'.%'", filtro.getMaterialcategoria().getIdentificador())
				.or()
					.where("vmaterialcategoria.identificador like ? ", filtro.getMaterialcategoria().getIdentificador())
				.closeParentheses();
				
		}
		
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaMaterialdevolucaohistorico");
	}	
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Materialdevolucao> query) {
		query
			.select("materialdevolucao.cdmaterialdevolucao, materialdevolucao.preco, materialdevolucao.qtdevendida, materialdevolucao.dtcancelamento, " +
					"materialdevolucao.qtdedevolvida, materialdevolucao.placaveiculo, materialdevolucao.numeronotadevolucao, " +
					"venda.cdvenda, material.cdmaterial, material.nome, material.identificacao, materialdevolucao.cdusuarioaltera, materialdevolucao.dtaltera, " +
					"listaMaterialdevolucaohistorico.cdmaterialdevolucaohistorico, listaMaterialdevolucaohistorico.acao, " +
					"listaMaterialdevolucaohistorico.observacao, usuario.cdpessoa, usuario.nome," +
					"listaMaterialdevolucaohistorico.dtaltera, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
					"unidademedida.cdunidademedida, unidademedidavenda.cdunidademedida,materialdevolucao.valeCompra," +
					"pedidovenda.cdpedidovenda, movimentacaoestoque.cdmovimentacaoestoque  ")
			.leftOuterJoin("materialdevolucao.listaMaterialdevolucaohistorico listaMaterialdevolucaohistorico")
			.leftOuterJoin("materialdevolucao.venda venda")
			.leftOuterJoin("materialdevolucao.pedidovenda pedidovenda")
			.leftOuterJoin("materialdevolucao.material material")
			.leftOuterJoin("materialdevolucao.localarmazenagem localarmazenagem")
			.leftOuterJoin("listaMaterialdevolucaohistorico.usuario usuario")
			.leftOuterJoin("materialdevolucao.unidademedida unidademedida")
			.leftOuterJoin("materialdevolucao.unidademedidavenda unidademedidavenda")
			.leftOuterJoin("materialdevolucao.movimentacaoestoque movimentacaoestoque")
			.orderBy("listaMaterialdevolucaohistorico.dtaltera desc");
	}
	
	/**
	 * M�todo que retorna a quantidade j� devolvida
	 *
	 * @param venda
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Double getQtdeJaDevolvida(Venda venda, Material material, Vendamaterial vendamaterial, Pedidovendamaterial pedidovendamaterial, boolean converterParaUnidadePrincipal) {
		if(pedidovendamaterial == null && (venda == null || venda.getCdvenda() == null || material == null || material.getCdmaterial() == null))
			return 0.0;
		
		QueryBuilder<Double> query = newQueryBuilder(Double.class)
			.from(Materialdevolucao.class)
			.select(converterParaUnidadePrincipal ? "SUM( " +
					" case when unidademedida.cdunidademedida <> unidademedidaMaterial.cdunidademedida and vendamaterial.fatorconversao > 0 and vendamaterial.qtdereferencia > 0 " +
					" then (materialdevolucao.qtdedevolvida / (vendamaterial.fatorconversao / vendamaterial.qtdereferencia)) " +
					" else COALESCE(qtde_unidadeprincipal(material.cdmaterial, unidademedida.cdunidademedida, materialdevolucao.qtdedevolvida),0) " +
					" end ) " : "SUM(COALESCE(materialdevolucao.qtdedevolvida, 0))")
			.join("materialdevolucao.material material")
			.join("material.unidademedida unidademedidaMaterial")
			.join("materialdevolucao.venda venda")
			.join("materialdevolucao.unidademedida unidademedida")
			.join("materialdevolucao.vendamaterial vendamaterial")
			.where("materialdevolucao.dtcancelamento is null")
			.where("vendamaterial = ?", vendamaterial);
			
		if(pedidovendamaterial != null){
			query.where("vendamaterial.pedidovendamaterial = ?", pedidovendamaterial);
		}else {
			query.where("venda = ?", venda);
			query.where("material = ?", material);
		}
			
		Double qtdeJaDevolvida = query.setUseTranslator(false).unique();
		
		if (qtdeJaDevolvida != null)
			return qtdeJaDevolvida;
		else 
			return 0.0;
	}
	
	/**
	* M�todo que retorna a quantidade ja devolvido dos itens da venda
	*
	* @param venda
	* @param material
	* @param vendamaterial
	* @param converterParaUnidadePrincipal
	* @return
	* @since 02/12/2016
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public List<Materialdevolucao> findWithQtdeJaDevolvida(Venda venda, Material material, Vendamaterial vendamaterial, boolean converterParaUnidadePrincipal, String whereInCdvenda) {
		String sql = " select vendamaterial.cdvendamaterial, material.cdmaterial, materialdevolucao.cdvenda, " + (converterParaUnidadePrincipal ? "SUM( " +
								" case when materialdevolucao.cdunidademedida <> material.cdunidademedida and vendamaterial.fatorconversao > 0 and vendamaterial.qtdereferencia > 0 " +
								" then (materialdevolucao.qtdedevolvida / (vendamaterial.fatorconversao / vendamaterial.qtdereferencia)) " +
								" else COALESCE(qtde_unidadeprincipal(material.cdmaterial, materialdevolucao.cdunidademedida, materialdevolucao.qtdedevolvida),0) " +
								" end ) " : "SUM(COALESCE(materialdevolucao.qtdedevolvida, 0)) ") + " AS qtdedevolvida " +
					" from Materialdevolucao materialdevolucao " +
					" join material material on material.cdmaterial = materialdevolucao.cdmaterial " +
					" join vendamaterial vendamaterial on vendamaterial.cdvendamaterial = materialdevolucao.cdvendamaterial " +
					" where 1= 1 " +
					(venda != null ? " and materialdevolucao.cdvenda = " + venda.getCdvenda() : "" )+
					(whereInCdvenda != null ? " and materialdevolucao.cdvenda in (" + whereInCdvenda + ") " : "" )+
					(material != null ? " and material.cdmaterial = " + material.getCdmaterial() : "" ) +
					(vendamaterial != null ? " and vendamaterial.cdvendamaterial = " + vendamaterial.getCdvendamaterial() : "" ) +
					" group by vendamaterial.cdvendamaterial, material.cdmaterial, materialdevolucao.cdvenda";

		SinedUtil.markAsReader();
		List<Materialdevolucao> lista = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Materialdevolucao(rs.getInt("cdvendamaterial"), rs.getInt("cdmaterial"), rs.getDouble("qtdedevolvida"), rs.getInt("cdvenda"));
			}
		});
		
		return lista;
	}
	
	/**
	* M�todo que verifica se existe devolu��o com unidade de medida diferente
	*
	* @param venda
	* @param material
	* @param vendamaterial
	* @param unidademedida
	* @return
	* @since 28/11/2016
	* @author Luiz Fernando
	*/
	public boolean existeDevolucaoUnidadeDiferente(Venda venda,	Material material, Vendamaterial vendamaterial,	Unidademedida unidademedida) {
		if(venda == null || venda.getCdvenda() == null || material == null || material.getCdmaterial() == null || unidademedida == null || unidademedida.getCdunidademedida() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilder(Long.class)
			.from(Materialdevolucao.class)
			.select("count(*)")
			.where("materialdevolucao.venda = ?", venda)
			.where("materialdevolucao.material = ?", material)
			.where("materialdevolucao.vendamaterial = ?", vendamaterial)
			.where("materialdevolucao.unidademedida <> ?", unidademedida)
			.where("materialdevolucao.dtcancelamento is null")
			.unique() > 0;
	}
	
	/**
	* M�todo que retorna a quantidade j� devolvida do pedido de venda
	*
	* @param pedidovenda
	* @param material
	* @param pedidovendamaterial
	* @return
	* @since 12/02/2016
	* @author Luiz Fernando
	*/
	public Double getQtdeJaDevolvida(Pedidovenda pedidovenda, Material material, Pedidovendamaterial pedidovendamaterial) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null || material == null || material.getCdmaterial() == null)
			return 0.0;
		
		QueryBuilder<Double> query = newQueryBuilder(Double.class)
			.from(Materialdevolucao.class)
			.select("SUM(COALESCE(qtde_unidadeprincipal(material.cdmaterial, unidademedida.cdunidademedida, materialdevolucao.qtdedevolvida),0))")
			.join("materialdevolucao.material material")
			.join("materialdevolucao.pedidovenda pedidovenda")
			.join("materialdevolucao.unidademedida unidademedida")
			.where("pedidovenda = ?", pedidovenda)
			.where("material = ?", material)
			.where("materialdevolucao.pedidovendamaterial = ?", pedidovendamaterial);
	
			
		Double qtdeJaDevolvida = query.setUseTranslator(false).unique();
		
		if (qtdeJaDevolvida != null)
			return qtdeJaDevolvida;
		else 
			return 0.0;
	}
	
	public boolean existeDevolucaoUnidadeDiferente(Pedidovenda pedidovenda,	Material material, Pedidovendamaterial pedidovendamaterial,	Unidademedida unidademedida) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null || material == null || material.getCdmaterial() == null || unidademedida == null || unidademedida.getCdunidademedida() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilder(Long.class)
			.from(Materialdevolucao.class)
			.select("count(*)")
			.where("materialdevolucao.pedidovenda = ?", pedidovenda)
			.where("materialdevolucao.material = ?", material)
			.where("materialdevolucao.pedidovendamaterial = ?", pedidovendamaterial)
			.where("materialdevolucao.unidademedida <> ?", unidademedida)
			.unique() > 0;
	}

	/**
	 * M�todo que busca os materiais devolvidos
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<MaterialdevolucaoQueryBean> findForComprovantedevolucao(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		String sql = "SELECT " +
							"e.cdpessoa as cdempresa, " +
							"e.nome as empresa_nome, " +
							"c.nome as cliente_nome, " +
							"md.cdvenda as cdvenda, " +
							"m.cdmaterial as cdmaterial, " +
							"m.nome as material_nome, " +
							"md.qtdedevolvida as qtdedevolvida, " +
							"md.preco as preco " +
						"FROM materialdevolucao md " +
						"JOIN material m ON m.cdmaterial = md.cdmaterial " +
						"LEFT OUTER JOIN venda v ON v.cdvenda = md.cdvenda " +
						"LEFT OUTER JOIN pessoa e ON e.cdpessoa = v.cdempresa " +
						"LEFT OUTER JOIN pessoa c ON c.cdpessoa = v.cdcliente " +
						"WHERE md.cdmaterialdevolucao in (" + whereIn + ") " +
						"ORDER BY v.cdvenda, m.nome";

		SinedUtil.markAsReader();
		List<MaterialdevolucaoQueryBean> lista = getJdbcTemplate().query(sql, 
			new RowMapper() {
				public MaterialdevolucaoQueryBean mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new MaterialdevolucaoQueryBean(
								rs.getInt("cdempresa"),
								rs.getString("empresa_nome"),
								rs.getString("cliente_nome"),
								rs.getInt("cdvenda"),
								rs.getInt("cdmaterial"),
								rs.getString("material_nome"),
								rs.getDouble("qtdedevolvida"),
								rs.getDouble("preco")
							);
				}
			}
		);
		
		return lista;
	}

	/**
	 * Busca as devolu��es para o preenchimento do recebimento.
	 *
	 * @param whereIn
	 * @return
	 * @since 19/09/2012
	 * @author Rodrigo Freitas
	 */
	public List<Materialdevolucao> findForEntrega(String whereIn) {
		return query()
					.select("materialdevolucao.cdmaterialdevolucao, materialdevolucao.numeronotadevolucao, materialdevolucao.preco, " +
							"materialdevolucao.qtdedevolvida,materialdevolucao.qtdevendida,unidademedidavenda.cdunidademedida, material.cdmaterial, material.nome, localarmazenagem.cdlocalarmazenagem," +
							"unidademedida.cdunidademedida,venda.cdvenda,movimentacaoestoque.cdmovimentacaoestoque," +
							"vendamaterial.cdvendamaterial,vendamaterial.quantidade," +
							"loteestoqueVM.cdloteestoque, loteestoqueVM.numero, loteestoqueVM.validade, " +
							"loteestoquePVM.cdloteestoque, loteestoquePVM.numero, loteestoquePVM.validade ")
					.join("materialdevolucao.material material")
					.leftOuterJoin("materialdevolucao.movimentacaoestoque movimentacaoestoque")
					.leftOuterJoin("materialdevolucao.localarmazenagem localarmazenagem")
					.leftOuterJoin("materialdevolucao.unidademedida unidademedida")
					.leftOuterJoin("materialdevolucao.unidademedidavenda unidademedidavenda")
					.leftOuterJoin("materialdevolucao.vendamaterial vendamaterial")
					.leftOuterJoin("materialdevolucao.venda venda")
					.leftOuterJoin("vendamaterial.loteestoque loteestoqueVM")
					.leftOuterJoin("materialdevolucao.pedidovendamaterial pedidovendamaterial")
					.leftOuterJoin("pedidovendamaterial.loteestoque loteestoquePVM")
					.whereIn("materialdevolucao.cdmaterialdevolucao", whereIn)
					.list();
	}


	/**
	 * M�todo que verifica se existe alguma devolu��o de material da venda passada por par�metro
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existDevolucaoByVenda(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nula");
		
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Materialdevolucao.class)
					.join("materialdevolucao.venda venda")
					.where("venda = ?", venda)
					.unique() > 0;
	}

	/**
	 * M�todo que busca os dados de devolu��o de material para o CSV
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Materialdevolucao> findForCsv(MaterialdevolucaoFiltro filtro) {
		QueryBuilder<Materialdevolucao> query = querySined();
		this.updateListagemQuery(query, filtro);
		
		query.select("materialdevolucao.cdmaterialdevolucao, materialdevolucao.preco, materialdevolucao.qtdevendida, " +
				"materialdevolucao.qtdedevolvida, venda.cdvenda, material.cdmaterial, material.nome, materialdevolucao.dtaltera, " +
				"listaMaterialdevolucaohistorico.cdmaterialdevolucaohistorico, listaMaterialdevolucaohistorico.observacao, " +
				"listaMaterialdevolucaohistorico.dtaltera, venda.dtvenda, colaborador.nome ")
			.leftOuterJoin("materialdevolucao.listaMaterialdevolucaohistorico listaMaterialdevolucaohistorico");
		
		return query.list();
	}
	
	/**
	 * M�todo que retorna a quantidade devolvida de acordo com o filtro da listagem
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Double getTotalQtdeDevolvidaListagem(MaterialdevolucaoFiltro filtro) {
		if(filtro == null)
			return 0.0;
		
		QueryBuilder<Double> query = newQueryBuilder(Double.class)
			.from(Materialdevolucao.class)
			.select("SUM(materialdevolucao.qtdedevolvida)")
			.leftOuterJoin("materialdevolucao.venda venda")
			.leftOuterJoin("materialdevolucao.material material")
			.leftOuterJoin("venda.empresa empresa")
			.leftOuterJoin("venda.colaborador colaborador")
			.leftOuterJoin("materialdevolucao.pedidovenda pedidovenda")
			.where("material = ?", filtro.getMaterial())
			.where("empresa = ?", filtro.getEmpresa())
			.where("colaborador = ?", filtro.getColaborador())
			.where("venda.dtvenda >= ?", filtro.getDtiniciovenda())
			.where("venda.dtvenda <= ?", filtro.getDtfimvenda())
			.where("materialdevolucao.dtaltera >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getDtiniciodevolucao()))
			.where("materialdevolucao.dtaltera <= ?", SinedDateUtils.dateToTimestampFinalDia(filtro.getDtfimdevolucao()))
			.whereIn("venda.cdvenda", filtro.getWhereInCdVenda())
			.whereIn("pedidovenda.cdpedidovenda", filtro.getWhereInCdPedidoVenda())
			.where("materialdevolucao.dtcancelamento is null", filtro.getCanceladas() == null || !filtro.getCanceladas());
			
		Double qtdeJaDevolvida = query.setUseTranslator(false).unique();
		
		if (qtdeJaDevolvida != null)
			return qtdeJaDevolvida;
		else 
			return 0.0;
	}
	
	/**
	 * M�todo que retorna a quantidade devolvida de acordo com as vendas
	 *
	 * @param whereInVenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getValortotalDevolvidaListagemVenda(String whereInVenda) {
		if(whereInVenda == null || "".equals(whereInVenda))
			return new Money();
		
		QueryBuilder<Double> query = newQueryBuilder(Double.class)
			.from(Materialdevolucao.class)
			.select("SUM(round(COALESCE(materialdevolucao.preco, 0)*COALESCE(materialdevolucao.qtdedevolvida,1), 2))")
			.leftOuterJoin("materialdevolucao.venda venda")
			.where("materialdevolucao.dtcancelamento IS NULL");
		
		SinedUtil.quebraWhereIn("venda.cdvenda", whereInVenda, query);
		
		Double valortotalDevolvida = query.setUseTranslator(false).unique();
		
		if (valortotalDevolvida != null)
			return new Money(valortotalDevolvida);
		else 
			return new Money();
	}
	
	/**
	 * M�todo que retorna todos as devolu��es de material por venda, sem a convers�o para a unidade principal
	 *
	 * @param vendaWhereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/01/2015
	 */
	public List<Materialdevolucao> getListaMaterialdevolucaoByVendaSemConversaoUnidadeprincipal(String vendaWhereIn) {
		if(vendaWhereIn == null || vendaWhereIn.trim().isEmpty())
			return null;
		
		QueryBuilder<Materialdevolucao> query = query()
			.select("venda.cdvenda, material.cdmaterial, materialgrupo.cdmaterialgrupo, materialdevolucao.qtdedevolvida, materialdevolucao.qtdevendida")
			.join("materialdevolucao.material material")
			.join("material.materialgrupo materialgrupo")
			.join("materialdevolucao.venda venda");
		
		SinedUtil.quebraWhereIn("venda.cdvenda", vendaWhereIn, query);
		
		return query
			.orderBy("venda.cdvenda, material.cdmaterial, materialgrupo.cdmaterialgrupo")
			.list();
	}

	/**
	* M�todo que faz update na data de cancelamento da devolu��o de material
	*
	* @param whereIn
	* @since 20/04/2016
	* @author Luiz Fernando
	*/
	public void updateDtcancelamento(String whereIn) {
		if (StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioLogado != null){
			Integer cdusuarioaltera = usuarioLogado.getCdpessoa();
			
			String sql = "update Materialdevolucao m set m.cdusuarioaltera = ?, m.dtaltera = ?, m.dtcancelamento = ? " +
						 "where m.cdmaterialdevolucao in ("+whereIn+")";
			
			getHibernateTemplate().bulkUpdate(sql.toString(), new Object[]{cdusuarioaltera,hoje, hoje});
		} else {
			String sql = "update Materialdevolucao m set m.dtcancelamento = ? where m.cdmaterialdevolucao in ("+whereIn+")";

			getHibernateTemplate().bulkUpdate(sql.toString(), new Object[]{hoje});
		}
		
	}
	
	/**
	* M�todo que busca as devolu��es de material para cancelamento
	*
	* @param whereIn
	* @return
	* @since 20/04/2016
	* @author Luiz Fernando
	*/
	public List<Materialdevolucao> findForCancelamento(String whereIn) {
		if (StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("materialdevolucao.cdmaterialdevolucao, materialdevolucao.dtcancelamento, materialdevolucao.numeronotadevolucao, " +
						"venda.cdvenda, movimentacaoestoque.cdmovimentacaoestoque,entregadocumento.cdentregadocumento,entregadocumento.numero," +
						"entregadocumento.entregadocumentosituacao,cliente.cdpessoa,materialdevolucao.preco,materialdevolucao.qtdedevolvida,materialdevolucao.valeCompra, " +
						"movimentacaoestoque.dtmovimentacao, empresa.cdpessoa, projeto.cdprojeto, localarmazenagem.cdlocalarmazenagem ")
				.join("materialdevolucao.venda venda")
				.join("venda.cliente cliente")
				.leftOuterJoin("materialdevolucao.movimentacaoestoque movimentacaoestoque")
				.leftOuterJoin("movimentacaoestoque.empresa empresa")
				.leftOuterJoin("movimentacaoestoque.projeto projeto")
				.leftOuterJoin("movimentacaoestoque.localarmazenagem localarmazenagem")
				.leftOuterJoin("materialdevolucao.entregadocumento entregadocumento")
				.whereIn("materialdevolucao.cdmaterialdevolucao", whereIn)
				.list();
		
	}
	
	public List<Materialdevolucao> findForCancelamento(Material material, Venda venda, Vendamaterial vendamaterial) {
		if (material == null || venda == null || venda.getCdvenda() == null || vendamaterial == null || vendamaterial.getCdvendamaterial() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
				.select("materialdevolucao.cdmaterialdevolucao, materialdevolucao.dtcancelamento, materialdevolucao.numeronotadevolucao, " +
						"venda.cdvenda, movimentacaoestoque.cdmovimentacaoestoque")
				.join("materialdevolucao.venda venda")
				.leftOuterJoin("materialdevolucao.movimentacaoestoque movimentacaoestoque")
				.where("materialdevolucao.material = ?", material)
				.where("venda = ?", venda)
				.where("materialdevolucao.vendamaterial = ?", vendamaterial)
				.list();
		
	}
}
