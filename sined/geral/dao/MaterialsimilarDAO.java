package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialsimilar;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialsimilarDAO extends GenericDAO<Materialsimilar> {
	
	/**
	 * M�todo que verifica se existe material similar para o material passado por par�metro
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existMaterialsimilar(Material material){
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo");
		
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Materialsimilar.class)
					.join("materialsimilar.material material")
					.where("material = ?", material)
					.unique() > 0;
	}

	public List<Materialsimilar> findForPVOffline() {
		return query()
				.select("materialsimilar.cdmaterialsimilar, material.cdmaterial, materialsimilaritem.cdmaterial ")
				.join("materialsimilar.material material")
				.join("materialsimilar.materialsimilaritem materialsimilaritem")
				.list();
	}
	
	public List<Materialsimilar> findForAndroid(String whereIn) {
		return query()
				.select("materialsimilar.cdmaterialsimilar, material.cdmaterial, materialsimilaritem.cdmaterial ")
				.join("materialsimilar.material material")
				.join("materialsimilar.materialsimilaritem materialsimilaritem")
				.whereIn("materialsimilar.cdmaterialsimilar", whereIn)
				.list();
	}
	
	/**
	 * Busca os materiais similares a serem enviados ao W3Producao
	 * @param whereIn
	 * @return
	 */
	public List<Materialsimilar> findForW3Producao(String whereIn){
		return query()
				.select("materialsimilar.cdmaterialsimilar, material.cdmaterial, materialsimilaritem.cdmaterial")
				.leftOuterJoin("materialsimilar.material material")
				.leftOuterJoin("materialsimilar.materialsimilaritem materialsimilaritem")
				.whereIn("materialsimilar.cdmaterialsimilar", whereIn)
				.list();
	}
}
