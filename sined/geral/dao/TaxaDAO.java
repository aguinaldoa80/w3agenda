package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TaxaDAO extends GenericDAO<Taxa>{
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Taxa> query) {
		query.leftOuterJoinFetch("taxa.listaTaxaitem listaTaxaitem");
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				save.saveOrUpdateManaged("listaTaxaitem");
				return null;
			}
		});
	}
	
	/**
	 * M�todo para carregar as Taxas de um documento.
	 * 
	 * @param documento
	 * @return
	 * @author Fl�vio
	 */
	public List<Taxa> findByDocmento(String whereIn){
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		QueryBuilder<Taxa> query =  
			querySined()
			
			.select("taxa.cdtaxa, taxaitem.cdtaxaitem, taxaitem.percentual,taxaitem.valor, taxaitem.gerarmensagem, taxaitem.dtlimite, taxaitem.motivo, tipotaxa.cdtipotaxa," +
					"tipotaxa.nome, contacredito.cdcontagerencial, contacredito.nome, contadebito.cdcontagerencial, contadebito.nome, " +
					"centrocusto.cdcentrocusto, centrocusto.nome, taxaitem.aodia, taxaitem.tipocalculodias, taxaitem.dias")
			.leftOuterJoin("taxa.listaTaxaitem taxaitem")
			.leftOuterJoin("taxaitem.tipotaxa tipotaxa")
			.leftOuterJoin("tipotaxa.contacredito contacredito")
			.leftOuterJoin("tipotaxa.contadebito contadebito")
			.leftOuterJoin("tipotaxa.centrocusto centrocusto")
			.join("taxa.listaDocumento documento");
		
		SinedUtil.quebraWhereIn("documento.cddocumento", whereIn, query);
		
		return query
				.orderBy("tipotaxa.nome")
				.list();
	}
	
	/**
	 * Executa um saveOrUpdate sem transa��o com o banco.
	 * 
	 * @param bean
	 * @author Hugo Ferreira
	 */
	public void saveOrUpdateNoUseTransaction(Taxa bean) {
		SaveOrUpdateStrategy save = save(bean);
		save.useTransaction(false);
		updateSaveOrUpdate(save);
		save.execute();
		getHibernateTemplate().flush();
	}

	/**
	 * Carrega a taxa com seus itens preenchidos.
	 *
	 * @param taxa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Taxa findTaxa(Taxa taxa) {
		if(taxa == null || taxa.getCdtaxa() == null){
			throw new SinedException("Taxa n�o pode ser nula.");
		}
		return query()
					.select("taxa.cdtaxa, taxaitem.percentual,taxaitem.valor, taxaitem.dtlimite, tipotaxa.cdtipotaxa," +
					"tipotaxa.nome, taxaitem.aodia")
					.leftOuterJoin("taxa.listaTaxaitem taxaitem")
					.leftOuterJoin("taxaitem.tipotaxa tipotaxa")
					.where("taxa = ?", taxa)
					.orderBy("tipotaxa.nome")
					.unique();
	}
	
	/**
	* M�todo que retorna a taxa do documento
	*
	* @param documento
	* @return
	* @since 07/12/2015
	* @author Luiz Fernando
	*/
	public Taxa findForArquivoretornoByDocumento(Documento documento){
		if(documento == null || documento.getCddocumento() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("taxa.cdtaxa, taxaitem.cdtaxaitem, taxaitem.dtlimite, tipotaxa.cdtipotaxa, tipotaxa.nome")
				.join("taxa.listaTaxaitem taxaitem")
				.join("taxaitem.tipotaxa tipotaxa")
				.join("taxa.listaDocumento documento")
				.where("documento = ?", documento)
				.unique();
	}
}
