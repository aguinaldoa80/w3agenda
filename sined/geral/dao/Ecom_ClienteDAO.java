package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.sined.geral.bean.Ecom_Cliente;

public class Ecom_ClienteDAO extends GenericDAO<Ecom_Cliente>{

	public List<Ecom_Cliente> findForPedido(Ecom_Cliente bean){
		return query()
				.leftOuterJoinFetch("ecom_Cliente.endereco endereco")
				.leftOuterJoinFetch("ecom_Cliente.telefones telefones")
				.where("ecom_Cliente = ?", bean)
				.list();
	}
}
