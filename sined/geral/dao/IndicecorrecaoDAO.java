package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Indicecorrecao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.IndicecorrecaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("indicecorrecao.sigla")
public class IndicecorrecaoDAO extends GenericDAO<Indicecorrecao> {

	@Override
	public void updateListagemQuery(QueryBuilder<Indicecorrecao> query,	FiltroListagem _filtro) {
		IndicecorrecaoFiltro filtro = (IndicecorrecaoFiltro) _filtro;
		query.select("indicecorrecao.cdindicecorrecao, indicecorrecao.sigla, indicecorrecao.descricao")
		.whereLikeIgnoreAll("indicecorrecao.sigla", filtro.getSigla())
		.whereLikeIgnoreAll("indicecorrecao.descricao", filtro.getDescricao())
		.orderBy("indicecorrecao.cdindicecorrecao desc");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Indicecorrecao> query) {
		query.select("indicecorrecao.cdindicecorrecao, indicecorrecao.sigla, indicecorrecao.descricao, indicecorrecao.dtaltera, indicecorrecao.cdusuarioaltera, " +
					 "listaIndicecorrecaovalor.cdindicecorrecaovalor, listaIndicecorrecaovalor.mesano, listaIndicecorrecaovalor.percentual, indicecorrecao.tipocalculo")
			.leftOuterJoin("indicecorrecao.listaIndicecorrecaovalor listaIndicecorrecaovalor")
			.orderBy("listaIndicecorrecaovalor.mesano");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaIndicecorrecaovalor");
		super.updateSaveOrUpdate(save);
	}

	/**
	 * M�todo que carrega os dados principais �ndice de corre��o
	 * 
	 * @param indicecorrecao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Indicecorrecao carregaIndiceCorrecao(Indicecorrecao indicecorrecao) {
		if(indicecorrecao == null || indicecorrecao.getCdindicecorrecao() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("indicecorrecao.cdindicecorrecao, indicecorrecao.tipocalculo, listaIndicecorrecaovalor.cdindicecorrecaovalor, listaIndicecorrecaovalor.mesano, " +
					"listaIndicecorrecaovalor.percentual")
			.join("indicecorrecao.listaIndicecorrecaovalor listaIndicecorrecaovalor")
			.where("indicecorrecao = ?", indicecorrecao)
			.unique();
	}

	public List<Indicecorrecao> findForPVOffline() {
		return query()
		.select("indicecorrecao.cdindicecorrecao, indicecorrecao.descricao, indicecorrecao.sigla")
		.list();
	}
	
	public List<Indicecorrecao> findForAndroid(String whereIn) {
		return query()
		.select("indicecorrecao.cdindicecorrecao, indicecorrecao.descricao, indicecorrecao.sigla")
		.whereIn("indicecorrecao.cdindicecorrecao", whereIn)
		.list();
	}
	
	public List<Indicecorrecao> findForVenda() {
		return query()
				.select("indicecorrecao.cdindicecorrecao, indicecorrecao.descricao, indicecorrecao.sigla")
				.list();
	}
		
}
