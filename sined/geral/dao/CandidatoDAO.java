package br.com.linkcom.sined.geral.dao;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.sined.geral.bean.Candidato;
import br.com.linkcom.sined.geral.bean.Candidatosituacao;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.CandidatoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("candidato.nome")
public class CandidatoDAO extends GenericDAO<Candidato>{

	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Candidato> query, FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O parametro _filtro n�o pode ser null.");
		}
		
		CandidatoFiltro filtro = (CandidatoFiltro) _filtro;
		
		query
				.select("distinct new br.com.linkcom.sined.geral.bean.Candidato(" +
						"candidato.cdcandidato, candidato.nome, sexo, candidatosituacao, candidatosituacao.descricao," +
						"municipio, municipio.nome, candidato.curso, grauinstrucao, candidato.email, " +
						"candidato.instituicao, candidato.objetivo, cargo, cargo.nome)")
				.setUseTranslator(false)
				.leftOuterJoin("candidato.sexo sexo")
				.leftOuterJoin("candidato.candidatosituacao candidatosituacao")
				.leftOuterJoin("candidato.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("candidato.grauinstrucao grauinstrucao")
				.leftOuterJoin("candidato.cargo cargo")
				.whereLikeIgnoreAll("candidato.nome", filtro.getNome())
				.whereLikeIgnoreAll("candidato.formacaoprofissional", filtro.getFormacaoprofissional())
				.whereLikeIgnoreAll("candidato.objetivo", filtro.getObjetivo())
				.where("sexo = ?", filtro.getSexo())
				.where("municipio = ?", filtro.getMunicipio())
				.where("uf = ?", filtro.getUf())
				.where("grauinstrucao = ?", filtro.getGrauinstrucao())
				.where("candidato.curso = ?", filtro.getCurso())
				.where("candidato.dtaltera >= ?", filtro.getDtalterainicio())
    			.where("candidato.dtaltera <= ?", filtro.getDtalterafim())
    			.where("cargo = ?", filtro.getCargo())
    			.where("candidato.classificacaoCandidato = ?", filtro.getClassificacaoCandidato())
				;
		
			if(filtro.getListaCandidatoSituacao() != null){
				String valorSituacao = "";
				for(Candidatosituacao candidatoSituacao : filtro.getListaCandidatoSituacao()){
					if(candidatoSituacao != null){
						valorSituacao += candidatoSituacao.getCdcandidatosituacao() + ",";
					}
				}
				query.whereIn("candidatosituacao.cdcandidatosituacao", valorSituacao.substring(0, (valorSituacao.length()-1) ));
			}
			
//			query.orderBy("candidato.nome");
			
		}
	
	
	public Candidato carregaCandidatoContratado(Candidato candidato){
		return query()
					.leftOuterJoinFetch("candidato.sexo sexo")
					.leftOuterJoinFetch("candidato.estadocivil estadocivil")
					.leftOuterJoinFetch("candidato.candidatosituacao candidatosituacao")
					.leftOuterJoinFetch("candidato.municipio municipio")
					.leftOuterJoinFetch("municipio.uf uf")
					.leftOuterJoinFetch("candidato.grauinstrucao grauinstrucao")
					.where("candidato =?", candidato)
				.unique();
		
		
	}
	
	public Candidato carregaCandidatoFromCpf(Cpf candidatoCpf){
		return query()
					.select("candidato.cdcandidato, candidato.nome, sexo.cdsexo, estadocivil.cdestadocivil, " +
							"candidatosituacao.cdcandidatosituacao, candidato.cpf, candidato.email, candidato.objetivo, " +
							"candidato.telefone, candidato.celular, municipio.cdmunicipio, municipio.uf, candidato.senha, " +
							"candidato.dtnascimento, candidato.logradouro, candidato.numero, candidato.complemento," +
							"candidato.caixapostal, candidato.cep, candidato.bairro, grauinstrucao.cdgrauinstrucao, " +
							"candidato.curso, candidato.instituicao, candidato.formacaoprofissional, candidato.dtaltera, " +
							"cargo.cdcargo, cargo.nome")
					.leftOuterJoin("candidato.sexo sexo")
					.leftOuterJoin("candidato.cargo cargo")
					.leftOuterJoin("candidato.estadocivil estadocivil")
					.leftOuterJoin("candidato.candidatosituacao candidatosituacao")
					.leftOuterJoin("candidato.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("candidato.grauinstrucao grauinstrucao")
					.where("candidato.cpf = ?", candidatoCpf)
				.unique();
		
	}
	
	public void alterarSenhaCandidato(Candidato candidato){
		if (candidato ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		Candidato load = this.load(candidato);
		
		load.setSenha(candidato.getSenha());
		save(load).execute();
		getHibernateTemplate().flush();
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {		
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				Candidato candidato = (Candidato)save.getEntity();
				if(candidato.getArquivo()!= null)
					arquivoDAO.saveFile(save.getEntity(), "arquivo");
				return null;
			}			
		});		
	}
	@Override
	public void updateEntradaQuery(QueryBuilder<Candidato> query) {
		query.leftOuterJoinFetch("candidato.arquivo arquivo");
	}

	@Override
	public ListagemResult<Candidato> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Candidato> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("candidato.cdcandidato");
		
		return new ListagemResult<Candidato>(query, filtro);
	}
}
