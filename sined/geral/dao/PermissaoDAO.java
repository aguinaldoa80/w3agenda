package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Permissao;
import br.com.linkcom.sined.geral.bean.Usuariopapel;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PermissaoDAO extends GenericDAO<Permissao> {

	@Override
	public void updateListagemQuery(QueryBuilder<Permissao> query, FiltroListagem filtro) {
		query
			.select("permissao.cdpermissao, permissao.papel, permissao.tela, permissao.stringpermissao")
			.leftOuterJoinFetch("permissao.papel papel");
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Permissao> query) {
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
	}
	

	/**
	 * Verifica se o papel possui permiss�o ao m�dulo.
	 * 
	 * @author Pedro Gon�alves
	 * @param path
	 * @return Total de url's encontradas
	 */
	public Long findPermissaoModulo(List<Usuariopapel> listUsuariopapel, String path) {
		if(path == null || listUsuariopapel == null){
			throw new SinedException("Par�metros inv�lidos.");
		}
		
		SinedUtil.markAsReader();
		return newQueryBuilderWithFrom(Long.class)
			.select("count(*)")
			.leftOuterJoin("permissao.tela tela")
			.leftOuterJoin("permissao.papel papel")
		   	.where("UPPER(tela.path) LIKE ?||'%'", path.toUpperCase())
		   	.whereLike("permissao.stringpermissao", "true")
			.whereIn("papel.cdpapel",CollectionsUtil.listAndConcatenate(listUsuariopapel, "papel.cdpapel",","))
			.setUseTranslator(false)
			.unique();
	}
	
	/* singleton */
	private static PermissaoDAO instance;
	public static PermissaoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(PermissaoDAO.class);
		}
		return instance;
	}
}