package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Instrutor;
import br.com.linkcom.sined.geral.bean.Turma;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.TurmaFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;


public class TurmaDAO extends GenericDAO<Turma> {
	
	
	@Override
	public void updateListagemQuery(QueryBuilder<Turma> query, FiltroListagem _filtro) {
		TurmaFiltro filtro = (TurmaFiltro) _filtro;
		query
			.select("distinct turma.cdturma, treinamento.nome, instrutor.nome, turma.dtinicio, turma.dtfim")
			.join("turma.treinamento treinamento")
			.leftOuterJoin("turma.instrutor instrutor")
			.leftOuterJoin("turma.listaTurmaparticipante turmaparticipante")
			.leftOuterJoin("turmaparticipante.colaborador colaborador")
			.leftOuterJoin("colaborador.listaColaboradorcargo cargo")
			.where("instrutor = ?",filtro.getInstrutor())
			.where("treinamento = ?",filtro.getTreinamento())
			.where("turma.dtinicio <= ?",filtro.getDtfim())
			.where("turma.dtfim >= ?",filtro.getDtinicio())
			.where("colaborador = ?",filtro.getColaborador())
			.whereLikeIgnoreAll("turma.empresa", filtro.getEmpresa())
			.orderBy("turma.dtinicio DESC")
			.ignoreJoin("turmaparticipante","colaborador","cargo");
		
		if (filtro.getDepartamento() != null) {
			query
				.where("cargo.dtfim is null")
				.where("cargo.departamento = ?",filtro.getDepartamento());
				
		}
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaTurmaparticipante");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Turma> query) {
		query
			.leftOuterJoinFetch("turma.instrutor instrutor")
			.leftOuterJoinFetch("turma.treinamento treinamento")
			.leftOuterJoinFetch("turma.listaTurmaparticipante listaTurmaparticipante")
			.leftOuterJoinFetch("listaTurmaparticipante.colaborador colaborador")
			.leftOuterJoinFetch("colaborador.listaColaboradorcargo cargo")
			.where("cargo.dtfim is null");
	}

	/* singleton */
	private static TurmaDAO instance;
	public static TurmaDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(TurmaDAO.class);
		}
		return instance;
	}
	
	/**
	 * Carrega a lista de turmas que est�o conflitantes com o per�odo da turma passada por par�metro.
	 * 
	 * @param colaborador
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Long findTurmaConflitante(Colaborador colaborador, Turma bean) {
		if (colaborador == null || colaborador.getCdpessoa() == null) {
			throw new SinedException("Colaborador n�o pode ser nulo.");
		}
		if (bean == null || bean.getDtfim() == null || bean.getDtinicio() == null) {
			throw new SinedException("Per�odo da turma n�o pode ser nulo;");
		}
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Turma.class)
				.setUseTranslator(false)
				.leftOuterJoin("turma.listaTurmaparticipante turmaparticipante")
				.leftOuterJoin("turmaparticipante.colaborador colaborador")
				.where("colaborador = ?",colaborador)
				.where("turma.dtinicio <= ?",bean.getDtfim())
				.where("turma.dtfim >= ?",bean.getDtinicio())
				.where("turma.horainicio <= ?",bean.getHorafim())
				.where("turma.horafim >= ?",bean.getHorainicio());
		
		if (bean.getCdturma() != null) {
			query.where("turma <> ?",bean);
		}
		
		return query.unique();
	}

	/**
	 * Carrega a lista de turmas que est�o conflitantes com o per�odo da turma passada por par�metro.
	 * 
	 * @param instrutor
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Long findTurmaConflitante(Instrutor instrutor, Turma bean) {
		if (instrutor == null || instrutor.getCdinstrutor() == null) {
			throw new SinedException("Ministrador n�o pode ser nulo.");
		}
		if (bean == null || bean.getDtfim() == null || bean.getDtinicio() == null) {
			throw new SinedException("Per�odo da turma n�o pode ser nulo;");
		}
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Turma.class)
				.setUseTranslator(false)
				.where("turma.instrutor = ?",instrutor)
				.where("turma.dtinicio = ?",bean.getDtfim())
				.where("turma.dtfim = ?",bean.getDtinicio())
				.where("turma.horainicio <= ?",bean.getHorainicio())
				.where("turma.horafim >=?",bean.getHorafim());
		
		if (bean.getCdturma() != null) {
			query.where("turma <> ?",bean);
		}
		
		return query.unique();
	}

	/**
	 * Carrega a lista de turmas de um treinamento para confec��o do relat�rio.
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Turma> findForReport(TurmaFiltro filtro) {
		if (filtro == null) {
			throw new SinedException("Filtro n�o pode ser nulo.");
		}
		QueryBuilder<Turma> query = querySined()
								.select("turma.dtinicio, turma.dtfim, treinamento.nome, " +
										"instrutor.nome, turma.empresa, turma.cargahoraria")
								.join("turma.treinamento treinamento")
								.leftOuterJoin("turma.instrutor instrutor")
								.leftOuterJoin("turma.listaTurmaparticipante turmaparticipante")
								.leftOuterJoin("turmaparticipante.colaborador colaborador")
								.leftOuterJoin("colaborador.listaColaboradorcargo cargo")
								.leftOuterJoin("cargo.departamento departamento")
								.where("instrutor = ?",filtro.getInstrutor())
								.where("treinamento = ?",filtro.getTreinamento())
								.where("turma.dtinicio <= ?",filtro.getDtfim())
								.where("turma.dtfim >= ?",filtro.getDtinicio())
								.where("colaborador = ?",filtro.getColaborador())
								.orderBy("treinamento.nome");
		
		if (filtro.getDepartamento() != null) {
			query
				.where("cargo.dtfim is null")
				.where("departamento = ?",filtro.getDepartamento());
		}
		
		return query.list();
	}

	/**
	 * M�todo para obter lista de turma para o combo Mais A��es na listagem de Colaborador.
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Turma> findForListagemColaborador(){
		return
			query()
			.select("turma.cdturma,turma.dtinicio,treinamento.nome")
			.join("turma.treinamento treinamento")
			.where("turma.dtfim >= ?",SinedDateUtils.currentDate())
			.list();
	}

	public Turma findTurmaForListaPresenca(Integer cdturma) {
		if (cdturma == null) {
			throw new SinedException("Cdturma n�o pode ser nulo.");
		}
		QueryBuilder<Turma> query = query()
								.select("turma.dtinicio, turma.dtfim, treinamento.nome, " +
										"instrutor.nome, turma.empresa, turma.cargahoraria, colaborador.nome")
								.join("turma.treinamento treinamento")
								.leftOuterJoin("turma.instrutor instrutor")
								.leftOuterJoin("turma.listaTurmaparticipante turmaparticipante")
								.leftOuterJoin("turmaparticipante.colaborador colaborador")
								.leftOuterJoin("colaborador.listaColaboradorcargo cargo")
								.leftOuterJoin("cargo.departamento departamento")
								.where("turma.cdturma = ?", cdturma)
								.orderBy("colaborador.nome");

		return query.unique();
	}
	
}