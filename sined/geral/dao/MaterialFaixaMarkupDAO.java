package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialFaixaMarkup;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialFaixaMarkupDAO extends GenericDAO<MaterialFaixaMarkup>{

	public List<MaterialFaixaMarkup> findByMaterial(Material material){
		if(material == null || material.getCdmaterial() == null){
			return null;
		}
		return querySined()
				.removeUseWhere()
				.setUseReadOnly(false)
				.select("materialFaixaMarkup.cdMaterialFaixaMarkup, materialFaixaMarkup.markup, materialFaixaMarkup.isValorMinimo, materialFaixaMarkup.isValorIdeal, materialFaixaMarkup.valorVenda, " +
						"faixaMarkupNome.cdFaixaMarkupNome, faixaMarkupNome.nome, faixaMarkupNome.cor, " +
						"material.cdmaterial, material.nome, " +
						"unidadeMedida.cdunidademedida, unidadeMedida.nome, unidadeMedida.simbolo, " +
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cpf, empresa.cnpj")
				.join("materialFaixaMarkup.faixaMarkupNome faixaMarkupNome")
				.leftOuterJoin("materialFaixaMarkup.empresa empresa")
				.join("materialFaixaMarkup.material material")
				.leftOuterJoin("materialFaixaMarkup.unidadeMedida unidadeMedida")
				.where("materialFaixaMarkup.material = ?", material)
				.orderBy("unidadeMedida.nome, faixaMarkupNome.nome")
				.list();
	}
	
    public List<MaterialFaixaMarkup> findForVenda(Material material, Unidademedida unidademedida, Empresa empresa){
		if(Util.objects.isNotPersistent(material) || Util.objects.isNotPersistent(unidademedida)){
			return null;
		}
		return query()
				.select("materialFaixaMarkup.cdMaterialFaixaMarkup, materialFaixaMarkup.markup, materialFaixaMarkup.isValorMinimo, materialFaixaMarkup.isValorIdeal, materialFaixaMarkup.valorVenda, " +
						"faixaMarkupNome.cdFaixaMarkupNome, faixaMarkupNome.nome, faixaMarkupNome.cor, " +
						"unidadeMedida.cdunidademedida, unidadeMedida.nome, unidadeMedida.simbolo, " +
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.cpf, empresa.cnpj, " +
						"material.cdmaterial")
				.join("materialFaixaMarkup.faixaMarkupNome faixaMarkupNome")
				.leftOuterJoin("materialFaixaMarkup.empresa empresa")
				.join("materialFaixaMarkup.material material")
				.leftOuterJoin("materialFaixaMarkup.unidadeMedida unidadeMedida")
				.where("material = ?", material)
				.where("unidadeMedida = ?", unidademedida)
				.openParentheses()
					.where("empresa is null")
					.or()
					.where("empresa = ?", empresa)
				.closeParentheses()
				.orderBy("materialFaixaMarkup.markup desc")
				.list();
	}
	
    public List<MaterialFaixaMarkup> findOrdenado(Material material, Unidademedida unidademedida, Empresa empresa){
        return findOrdenado(material, unidademedida, empresa, false);
	}
	
    public List<MaterialFaixaMarkup> findOrdenado(Material material, Unidademedida unidademedida, Empresa empresa, Boolean desc){
		if(material == null || material.getCdmaterial() == null){
			return null;
		}
		return query()
				.select("materialFaixaMarkup.cdMaterialFaixaMarkup, materialFaixaMarkup.markup, materialFaixaMarkup.isValorMinimo, materialFaixaMarkup.isValorIdeal, materialFaixaMarkup.valorVenda, " +
						"faixaMarkupNome.cdFaixaMarkupNome, faixaMarkupNome.nome, faixaMarkupNome.cor, " +
						"unidadeMedida.cdunidademedida, unidadeMedida.nome, unidadeMedida.simbolo, " +
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.cpf, empresa.cnpj")
				.join("materialFaixaMarkup.faixaMarkupNome faixaMarkupNome")
				.join("materialFaixaMarkup.material material")
				.leftOuterJoin("materialFaixaMarkup.empresa empresa")
				.leftOuterJoin("materialFaixaMarkup.unidadeMedida unidadeMedida")
				.where("material = ?", material)
                .where("unidadeMedida = ?", unidademedida)
				.openParentheses()
					.where("empresa is null")
					.or()
					.where("empresa = ?", empresa)
				.closeParentheses()
				.orderBy("materialFaixaMarkup.markup" + (desc ? " desc" : ""))
				.list();
	}
	
    public List<MaterialFaixaMarkup> findOrderByValorVenda(Material material, Unidademedida unidadeMedida, Empresa empresa){
        return this.findOrderByValorVenda(material, unidadeMedida, empresa, false);
	}
	
    public List<MaterialFaixaMarkup> findOrderByValorVenda(Material material, Unidademedida unidadeMedida, Empresa empresa, Boolean desc){
		if(material == null || material.getCdmaterial() == null){
			return null;
		}
		return query()
				.select("materialFaixaMarkup.cdMaterialFaixaMarkup, materialFaixaMarkup.markup, materialFaixaMarkup.isValorMinimo, materialFaixaMarkup.isValorIdeal, materialFaixaMarkup.valorVenda, " +
						"faixaMarkupNome.cdFaixaMarkupNome, faixaMarkupNome.nome, faixaMarkupNome.cor, " +
						"unidadeMedida.cdunidademedida, unidadeMedida.nome, unidadeMedida.simbolo, " +
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.cpf, empresa.cnpj")
				.join("materialFaixaMarkup.faixaMarkupNome faixaMarkupNome")
				.join("materialFaixaMarkup.material material")
				.leftOuterJoin("materialFaixaMarkup.empresa empresa")
				.leftOuterJoin("materialFaixaMarkup.unidadeMedida unidadeMedida")
				.where("material = ?", material)
                .where("unidadeMedida = ?", unidadeMedida)
				.openParentheses()
					.where("empresa is null")
					.or()
					.where("empresa = ?", empresa)
				.closeParentheses()
				.orderBy("materialFaixaMarkup.valorVenda" + (desc ? " desc" : ""))
				.list();
	}
	
	public MaterialFaixaMarkup getMarkupValorMinimo(Material material, Unidademedida unidadeMedida, Empresa empresa){
		if(material == null || material.getCdmaterial() == null){
			return null;
		}
		return query()
				.select("materialFaixaMarkup.cdMaterialFaixaMarkup, materialFaixaMarkup.markup, materialFaixaMarkup.isValorMinimo, materialFaixaMarkup.isValorIdeal, materialFaixaMarkup.valorVenda, " +
						"faixaMarkupNome.cdFaixaMarkupNome, faixaMarkupNome.nome, faixaMarkupNome.cor, " +
						"unidadeMedida.cdunidademedida, unidadeMedida.nome, unidadeMedida.simbolo, " +
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.cpf, empresa.cnpj")
				.join("materialFaixaMarkup.faixaMarkupNome faixaMarkupNome")
				.join("materialFaixaMarkup.material material")
				.leftOuterJoin("materialFaixaMarkup.empresa empresa")
				.leftOuterJoin("materialFaixaMarkup.unidadeMedida unidadeMedida")
				.where("material = ?", material)
				.where("materialFaixaMarkup.isValorMinimo = ?", Boolean.TRUE)
				.where("unidadeMedida = ?", unidadeMedida)
				.openParentheses()
					.where("empresa is null")
					.or()
					.where("empresa = ?", empresa)
				.closeParentheses()
				.setMaxResults(1)
				.unique();
	}
	
	public MaterialFaixaMarkup getMarkupValorIdeal(Material material, Unidademedida unidadeMedida, Empresa empresa){
		if(material == null || material.getCdmaterial() == null){
			return null;
		}
		return query()
				.select("materialFaixaMarkup.cdMaterialFaixaMarkup, materialFaixaMarkup.markup, materialFaixaMarkup.isValorMinimo, materialFaixaMarkup.isValorIdeal, materialFaixaMarkup.valorVenda, " +
						"faixaMarkupNome.cdFaixaMarkupNome, faixaMarkupNome.nome, faixaMarkupNome.cor, " +
						"unidadeMedida.cdunidademedida, unidadeMedida.nome, unidadeMedida.simbolo, " +
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.cpf, empresa.cnpj")
				.join("materialFaixaMarkup.faixaMarkupNome faixaMarkupNome")
				.join("materialFaixaMarkup.material material")
				.leftOuterJoin("materialFaixaMarkup.empresa empresa")
				.join("materialFaixaMarkup.unidadeMedida unidadeMedida")
				.where("material = ?", material)
				.where("materialFaixaMarkup.isValorIdeal = ?", Boolean.TRUE)
				.where("unidadeMedida = ?", unidadeMedida)
				.openParentheses()
					.where("empresa is null")
					.or()
					.where("empresa = ?", empresa)
				.closeParentheses()
				.setMaxResults(1)
				.unique();
	}
	
	public List<MaterialFaixaMarkup> findMarkupValorMinimo(String whereInMaterial, Empresa empresa){
		if(StringUtils.isBlank(whereInMaterial)){
			return null;
		}
		return query()
				.select("materialFaixaMarkup.cdMaterialFaixaMarkup, materialFaixaMarkup.markup, materialFaixaMarkup.isValorMinimo, materialFaixaMarkup.isValorIdeal, materialFaixaMarkup.valorVenda, " +
						"material.cdmaterial, material.nome, " +
						"faixaMarkupNome.cdFaixaMarkupNome, faixaMarkupNome.nome, faixaMarkupNome.cor, " +
						"unidadeMedida.cdunidademedida, unidadeMedida.nome, unidadeMedida.simbolo, " +
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.cpf, empresa.cnpj")
				.join("materialFaixaMarkup.faixaMarkupNome faixaMarkupNome")
				.join("materialFaixaMarkup.material material")
				.leftOuterJoin("materialFaixaMarkup.empresa empresa")
				.leftOuterJoin("materialFaixaMarkup.unidadeMedida unidadeMedida")
				.whereIn("material.cdmaterial", whereInMaterial)
				.where("materialFaixaMarkup.isValorMinimo = ?", Boolean.TRUE)
/*				.openParentheses()
					.where("empresa is null")
					.or()
					.where("empresa = ?", empresa)
				.closeParentheses()*/
				.list();
	}
	
	public List<MaterialFaixaMarkup> findMarkupValorIdeal(String whereInMaterial, Empresa empresa){
		if(StringUtils.isBlank(whereInMaterial)){
			return null;
		}
		return query()
				.select("materialFaixaMarkup.cdMaterialFaixaMarkup, materialFaixaMarkup.markup, materialFaixaMarkup.isValorMinimo, materialFaixaMarkup.isValorIdeal, materialFaixaMarkup.valorVenda, " +
						"material.cdmaterial, material.nome, " +
						"faixaMarkupNome.cdFaixaMarkupNome, faixaMarkupNome.nome, faixaMarkupNome.cor, " +
						"unidadeMedida.cdunidademedida, unidadeMedida.nome, unidadeMedida.simbolo, " +
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.cpf, empresa.cnpj")
				.join("materialFaixaMarkup.faixaMarkupNome faixaMarkupNome")
				.join("materialFaixaMarkup.material material")
				.leftOuterJoin("materialFaixaMarkup.empresa empresa")
				.leftOuterJoin("materialFaixaMarkup.unidadeMedida unidadeMedida")
				.whereIn("material.cdmaterial", whereInMaterial)
				.where("materialFaixaMarkup.isValorIdeal = ?", Boolean.TRUE)
/*				.openParentheses()
					.where("empresa is null")
					.or()
					.where("empresa = ?", empresa)
				.closeParentheses()*/
				.list();
	}	
	
	public void updatePercentualMarkup(MaterialFaixaMarkup faixa, Double markup) {
		if(faixa != null && faixa.getCdMaterialFaixaMarkup() != null && markup != null){
			getJdbcTemplate().update("update materialfaixamarkup set markup = ? " +
					" where cdmaterialfaixamarkup = ? ", new Object[]{markup, faixa.getCdMaterialFaixaMarkup()});
		}
	}
	
	public void updateValorVenda(MaterialFaixaMarkup faixa, Double valorVenda) {
		if(faixa != null && faixa.getCdMaterialFaixaMarkup() != null && valorVenda != null){
			getJdbcTemplate().update("update materialfaixamarkup set valorvenda = ? " +
					" where cdmaterialfaixamarkup = ? ", new Object[]{valorVenda, faixa.getCdMaterialFaixaMarkup()});
		}
	}
}
