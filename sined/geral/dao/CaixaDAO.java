package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CaixaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CaixaDAO extends GenericDAO<Conta> {

	@Override
	public void updateEntradaQuery(QueryBuilder<Conta> query) {
		query
			.leftOuterJoinFetch("conta.listaContaempresa listaContaempresa")
			.leftOuterJoinFetch("listaContaempresa.empresa empresa")
			.leftOuterJoinFetch("conta.listaContapapel listaContapapel")
			.leftOuterJoinFetch("listaContapapel.papel papel")
			.leftOuterJoinFetch("conta.operacaocontabil operacaocontabil")
			.leftOuterJoinFetch("conta.contaContabil");
	}	
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaContaempresa");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Conta> query,FiltroListagem _filtro) {
		if(_filtro == null)
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		CaixaFiltro filtro = (CaixaFiltro) _filtro;
		
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		query
			.select("conta.cdconta, conta.nome, conta.ativo, conta.desconsiderarsaldoinicialfluxocaixa, " +
					"view.saldoatual, listaContapapel.cdcontapapel, papel.cdpapel, papel.nome")
			.join("conta.vconta view")
			.join("conta.contatipo contatipo")
			.leftOuterJoin("conta.listaContaempresa listaContaempresa")
			.leftOuterJoin("listaContaempresa.empresa empresa")
			.leftOuterJoin("conta.listaContapapel listaContapapel")
			.leftOuterJoin("listaContapapel.papel papel")
			
			.where("contatipo = ?",new Contatipo(Contatipo.CAIXA))
			.whereLikeIgnoreAll("conta.nome", filtro.getNome())
			.where("conta.ativo = ?",filtro.getAtivo())
			.where("empresa = ?",filtro.getEmpresa())
			.where("coalesce(conta.desconsiderarsaldoinicialfluxocaixa, false) = ?",filtro.getDesconsiderarsaldoinicialfluxocaixa());
			
			if (!administrador){
				if (whereInPapel != null && !whereInPapel.equals("")){
					query.openParentheses();
					query.whereIn("papel.cdpapel", whereInPapel)
					.or()
					.where("papel.cdpapel is null");
					query.closeParentheses();
				} else {
					query.where("papel.cdpapel is null");
				}
			}
		
			query.orderBy("conta.nome")
			.ignoreAllJoinsPath(true)
			;
	}
	
	/**
	 * M�todo para obter a quantidade de caixas pelo nome.
	 * 
	 * @param bean
	 * @return Integer
	 * @author Fl�vio Tavares
	 */
	public Integer countCaixaByDescricao(Conta bean){
		if(bean==null || bean.getNome() == null){
			throw new SinedException("Os par�metros bean e nome n�o podem ser null.");
		}
		Long l = newQueryBuilderWithFrom(Long.class)
			.select("count(*)")
			.where("conta.contatipo.cdcontatipo = ?",Contatipo.CAIXA)
			.where("upper(conta.nome)=?", bean.getNome().toUpperCase().trim())
			.where("conta.cdconta <> ?",bean.getCdconta())
			.unique();
		return l.intValue();
	}
	
//	@Override
//	public void saveOrUpdate(Conta bean) {
//		if(bean.getCdconta() != null){
//			Timestamp dtaltera = new Timestamp(System.currentTimeMillis());
//			Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
//			
//			String sql = "update Conta " +
//							"set nome = ?," + 
//							"ativo = ?," + 
//							"saldo = ?," + 
//							"dtsaldo = ?," + 
//							"conciliacaoautomatica = ?," + 
//							"cdusuarioaltera = ?," + 
//							"dtaltera = ?," + 
//							"cdoperacaocontabil = " + (bean.getOperacaocontabil() != null && bean.getOperacaocontabil().getCdoperacaocontabil() != null ? bean.getOperacaocontabil().getCdoperacaocontabil() : null)+
//							",cdcontagerencial = " + (bean.getContagerencial() != null && bean.getContagerencial().getCdcontagerencial() != null ? bean.getContagerencial().getCdcontagerencial() : null)+
//							" where cdconta = ?";
//			
//			Object[] fields = new Object[]{
//						bean.getNome(),
//						bean.getAtivo(),
//						bean.getSaldo(),
//						bean.getDtsaldo(),
//						bean.getConciliacaoautomatica(),
//						cdusuarioaltera,
//						dtaltera,
//						bean.getCdconta()
//					};
//			
//			getHibernateTemplate().bulkUpdate(sql,fields);
//			
//		}else{
//			super.saveOrUpdate(bean);
//		}
//	}

	/**
	 * Verifica se a conta passada por par�metro � do tipo CAIXA e
	 * se � concilia��o autom�tica.
	 *
	 * @param conta
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean isCaixaAndConciliacaoAuto(Conta conta) {
		if(conta == null || conta.getCdconta() == null){
			throw new SinedException("A conta n�o pode ser nula.");
		}
		return newQueryBuilderSined(Long.class)
					.setUseWhereClienteEmpresa(false)
					.setUseWhereEmpresa(false)
					.setUseWhereFornecedorEmpresa(false)
					.setUseWhereProjeto(false)
					.select("count(*)")
					.from(Conta.class)
					.where("conta.contatipo = ?", new Contatipo(Contatipo.CAIXA))
					.where("conta.conciliacaoautomatica = ?", Boolean.TRUE)
					.where("conta = ?", conta)
					.unique() > 0;
	}
}
