package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.ApontamentoTipo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("nome")
public class ApontamentoTipoDAO extends GenericDAO<ApontamentoTipo>{

}
