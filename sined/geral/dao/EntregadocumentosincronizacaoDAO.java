package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Entregadocumentosincronizacao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.EntregadocumentosincronizacaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EntregadocumentosincronizacaoDAO extends GenericDAO<Entregadocumentosincronizacao> {

	@Override
	public void updateListagemQuery(QueryBuilder<Entregadocumentosincronizacao> query, FiltroListagem _filtro) {
		EntregadocumentosincronizacaoFiltro filtro = (EntregadocumentosincronizacaoFiltro) _filtro;
		
		query
			.select("entregadocumentosincronizacao.cdentregadocumentosincronizacao, entregadocumentosincronizacao.sincronizado, " +
					"fornecedor.nome, entrega.cdentrega, entregadocumentosincronizacao.devolucao, cliente.nome, entregadocumento.cdentregadocumento, " +
					"entregadocumentosincronizacao.dtsincronizacao, entregadocumentosincronizacao.dttentativasincronizacao ")
			.leftOuterJoin("entregadocumentosincronizacao.fornecedor fornecedor")
			.leftOuterJoin("entregadocumentosincronizacao.cliente cliente")
			.leftOuterJoin("entregadocumentosincronizacao.entregadocumento entregadocumento")
			.leftOuterJoin("entregadocumento.entrega entrega")
			.where("fornecedor = ?", filtro.getFornecedor())
			.whereIn("entrega.cdentrega", filtro.getWhereInCdEntrega());
		
		if(filtro.getSincronizado() != null && filtro.getSincronizado()){
			query.where("entregadocumentosincronizacao.sincronizado = true");
		}else if(filtro.getSincronizado() != null && !filtro.getSincronizado()){
			query.where("entregadocumentosincronizacao.sincronizado = false");
		}		
		query.orderBy("entregadocumentosincronizacao.cdentregadocumentosincronizacao desc");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Entregadocumentosincronizacao> query) {
		query
			.select("entregadocumentosincronizacao.cdentregadocumentosincronizacao, entregadocumentosincronizacao.sincronizado, " +
					"entregadocumentosincronizacao.fornecedorNome, entregadocumentosincronizacao.fornecedorNatureza, " +
					"entregadocumentosincronizacao.fornecedorDocumento, entregadocumento.cdentregadocumento, " +
					"fornecedor.cdpessoa, cliente.cdpessoa, entrega.cdentrega, entregamaterialsincronizacao.cdentregamaterialsincronizacao, " +
					"entregamaterialsincronizacao.materialDescricao, entregamaterialsincronizacao.materialReferencia, " +
					"entregamaterialsincronizacao.entregamaterialValorUnitario, entregamaterialsincronizacao.entregamaterialLote, " +
					"entregamaterialsincronizacao.entregamaterialQuantidade, entregamaterialsincronizacao.materialcorId, " +
					"entregamaterialsincronizacao.materialDescricaoEmbalagem, entregamaterialsincronizacao.materialAltura, " +
					"entregamaterialsincronizacao.materialLargura, entregamaterialsincronizacao.materialComprimento, " +
					"entregamaterialsincronizacao.materialQuantidadeVolume, entregamaterialsincronizacao.materialgrupoNome, " +
					"entregamaterialsincronizacao.materialmestreId, entregadocumentosincronizacao.dtsincronizacao, " +
					"entregadocumentosincronizacao.dttentativasincronizacao, entregamaterialsincronizacao.materialPeso, " +
					"material.cdmaterial, material.identificacao, materialgrupo.cdmaterialgrupo, " +
					"entregadocumentosincronizacao.cdentregadocumentosincronizacao, " +
					"entregadocumentosincronizacao.notaEntradaDataChegada, " +
					"entregadocumentosincronizacao.notaEntradaDataEmissao, " +
					"entregadocumentosincronizacao.notaEntradaDataLancamento, " +
					"entregadocumentosincronizacao.notaEntradaDeposito, " +
					"entregadocumentosincronizacao.notaEntradaNumero, " +
					"entregadocumentosincronizacao.notaEntradaPlacaVeiculo, " +
					"entregadocumentosincronizacao.notaEntradaTipo, " +
					"entregadocumentosincronizacao.notaEntradaTransportadora, entregadocumentosincronizacao.devolucao," +
					"entregadocumentosincronizacao.devolucaoefetuada ")			
			.leftOuterJoin("entregadocumentosincronizacao.fornecedor fornecedor")
			.leftOuterJoin("entregadocumentosincronizacao.cliente cliente")
			.leftOuterJoin("entregadocumentosincronizacao.entregadocumento entregadocumento")
			.leftOuterJoin("entregadocumento.entrega entrega")
			.leftOuterJoin("entregadocumentosincronizacao.listaEntregamaterialsincronizacao entregamaterialsincronizacao")
			.leftOuterJoin("entregamaterialsincronizacao.material material")
			.leftOuterJoin("entregamaterialsincronizacao.materialgrupo materialgrupo");
	
		super.updateEntradaQuery(query);
	}
	
	/**
	 * M�todo que verifica se existe entrega n�o sincronizada
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existEntregadocumentoNaoSincronizado() {
		Integer numero = newQueryBuilder(Integer.class)
				.select("entregadocumentosincronizacao.cdentregadocumentosincronizacao")
				.setUseTranslator(false)
				.from(Entregadocumentosincronizacao.class)
				.leftOuterJoin("entregadocumentosincronizacao.listaEntregamaterialsincronizacao entregamaterialsincronizacao")
				.leftOuterJoin("entregamaterialsincronizacao.material material")
				.leftOuterJoin("entregamaterialsincronizacao.materialgrupo materialgrupo")
				.where("coalesce(materialgrupo.sincronizarwms, false) = true")
				.openParentheses()
					.where("entregadocumentosincronizacao.sincronizado = false ")
					.or()
					.where("entregadocumentosincronizacao.sincronizado is null ")
				.closeParentheses()
				.setMaxResults(1)
				.unique();
		
		return numero != null;
	}
	
	/**
	 * M�todo que busca as informa��es da entregasincronizacao para fazer a sincronica��o com o wms
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumentosincronizacao> findForSincronizacaoEntregadocumentoPendente() {
		return query()
			.select("entregadocumentosincronizacao.cdentregadocumentosincronizacao, entregadocumentosincronizacao.sincronizado, " +
					"entregadocumentosincronizacao.fornecedorNome, entregadocumentosincronizacao.fornecedorNatureza, " +
					"entregadocumentosincronizacao.fornecedorDocumento, " +
					"fornecedor.cdpessoa, entrega.cdentrega, entregamaterialsincronizacao.cdentregamaterialsincronizacao, " +
					"entregamaterialsincronizacao.materialDescricao, entregamaterialsincronizacao.materialReferencia,  " +
					"entregamaterialsincronizacao.materialDescricaoEmbalagem, entregamaterialsincronizacao.entregamaterialQuantidade, " +
					"entregamaterialsincronizacao.materialQuantidadeVolume, entregamaterialsincronizacao.materialgrupoNome, " +
					"entregamaterialsincronizacao.materialPeso, " +
					"entregamaterialsincronizacao.materialAltura, entregamaterialsincronizacao.materialLargura, " +
					"entregamaterialsincronizacao.materialmestreId, entregamaterialsincronizacao.materialcorId, " +
					"entregamaterialsincronizacao.materialComprimento, entregadocumentosincronizacao.dtsincronizacao, " +
					"entregamaterialsincronizacao.entregamaterialValorUnitario, entregamaterialsincronizacao.entregamaterialLote, " +
					"entregadocumentosincronizacao.dttentativasincronizacao, cliente.cdpessoa, entregadocumento.cdentregadocumento, " +
					"material.cdmaterial, material.identificacao, materialgrupo.cdmaterialgrupo, entregadocumentosincronizacao.cdentregadocumentosincronizacao, " +
					"entregadocumentosincronizacao.notaEntradaDataChegada, entregadocumentosincronizacao.notaEntradaDataEmissao, " +
					"entregadocumentosincronizacao.notaEntradaDataLancamento, entregadocumentosincronizacao.notaEntradaDeposito, " +
					"entregadocumentosincronizacao.notaEntradaNumero, entregadocumentosincronizacao.notaEntradaPlacaVeiculo, " +
					"entregadocumentosincronizacao.notaEntradaTipo, entregadocumentosincronizacao.notaEntradaTransportadora, " +
					"entregadocumentosincronizacao.devolucao, produto.cdmaterial, produto.altura, produto.altura, produto.comprimento, " +
					"materialtipo.cdmaterialtipo, arquivoLegenda.cdarquivo")			
			.leftOuterJoin("entregadocumentosincronizacao.entregadocumento entregadocumento")
			.leftOuterJoin("entregadocumento.entrega entrega")
			.leftOuterJoin("entregadocumentosincronizacao.fornecedor fornecedor")
			.leftOuterJoin("entregadocumentosincronizacao.cliente cliente")
			.leftOuterJoin("entregadocumentosincronizacao.listaEntregamaterialsincronizacao entregamaterialsincronizacao")
			.leftOuterJoin("entregamaterialsincronizacao.material material")
			.leftOuterJoin("material.materialtipo materialtipo")
			.leftOuterJoin("materialtipo.arquivoLegenda arquivoLegenda")
			.leftOuterJoin("entregamaterialsincronizacao.materialgrupo materialgrupo")
			.leftOuterJoin("material.materialproduto produto")
			.where("coalesce(materialgrupo.sincronizarwms, false) = true")
			.openParentheses()
				.where("entregadocumentosincronizacao.sincronizado = false")
				.or()
				.where("entregadocumentosincronizacao.sincronizado is null")
			.closeParentheses()
			.list();
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save
			.saveOrUpdateManaged("listaEntregamaterialsincronizacao");
		
		super.updateSaveOrUpdate(save);
	}

		

	/**
	 * M�todo que seta a entregasincronizacao como sincronizado
	 *
	 * @param entregasincronizacao
	 * @author Luiz Fernando
	 */
	public void updateEntregadocumentosincronizacaoSincronizado(Entregadocumentosincronizacao eds) {
		if(eds == null || eds.getCdentregadocumentosincronizacao() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		Timestamp dataSincronizacao = new Timestamp(System.currentTimeMillis());
		
		getHibernateTemplate().bulkUpdate("update Entregadocumentosincronizacao set sincronizado = ?, dtsincronizacao = ? where cdentregadocumentosincronizacao = ?",
				new Object[]{Boolean.TRUE, dataSincronizacao, eds.getCdentregadocumentosincronizacao()});
	}

	/**
	 * M�todo que seta a data da tentativa de sincroniza��o
	 *
	 * @param entregasincronizacao
	 * @author Luiz Fernando
	 */
	public void updateEntregadocumentosincronizacaoTentativaDeSincronizacao(Entregadocumentosincronizacao eds) {
		if(eds == null || eds.getCdentregadocumentosincronizacao() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		Timestamp dataSincronizacao = new Timestamp(System.currentTimeMillis());
		
		getHibernateTemplate().bulkUpdate("update Entregadocumentosincronizacao set dttentativasincronizacao = ? where cdentregadocumentosincronizacao = ?",
				new Object[]{dataSincronizacao, eds.getCdentregadocumentosincronizacao()});
	}

	/**
	 * M�todo que busca a entregasincronizacao de acordo com o par�metro 
	 *
	 * @param entregasincronizacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Entregadocumentosincronizacao findByEntregadocumentosincronizacao(Entregadocumentosincronizacao eds) {
		if(eds == null || eds.getCdentregadocumentosincronizacao() == null)
			throw new SinedException();
		
		return query()
				.select("entregadocumentosincronizacao.cdentregadocumentosincronizacao, entregadocumento.cdentregadocumento, entregadocumentosincronizacao.devolucao ")
				.leftOuterJoin("entregadocumentosincronizacao.entregadocumento entregadocumento")
				.where("entregadocumentosincronizacao = ?", eds)
				.unique();
	}

	/**
	 * M�todo que busca os dados da entregasincronizacao para fazer a devolu��o
	 *
	 * @param cdentregasincronizacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Entregadocumentosincronizacao findForDevolucaoMaterial(Integer cdentregadocumentosincronizacao) {
		if(cdentregadocumentosincronizacao == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("entregadocumentosincronizacao.cdentregadocumentosincronizacao, entregadocumentosincronizacao.sincronizado, " +
					"entregamaterialsincronizacao.cdentregamaterialsincronizacao, " +
					"entregamaterialsincronizacao.materialDescricao, entregamaterialsincronizacao.materialReferencia, " +
					"entregamaterialsincronizacao.materialDescricaoEmbalagem, entregamaterialsincronizacao.entregamaterialQuantidade, " +
					"entregamaterialsincronizacao.materialQuantidadeVolume, entregamaterialsincronizacao.materialgrupoNome, " +
					"entregadocumentosincronizacao.dtsincronizacao, entregamaterialsincronizacao.entregamaterialValorUnitario, " +
					"entregamaterialsincronizacao.entregamaterialLote, " +
					"entregadocumentosincronizacao.dttentativasincronizacao, cliente.cdpessoa, " +
					"material.cdmaterial, materialgrupo.cdmaterialgrupo, entregadocumentosincronizacao.devolucao, " +
					"venda.cdvenda, empresa.cdpessoa, entregadocumentosincronizacao.cdentregadocumentosincronizacao, " +
					"entregadocumentosincronizacao.notaEntradaPlacaVeiculo, entregadocumentosincronizacao.notaEntradaNumero, " +
					"vendamaterial.cdvendamaterial, vendamaterial.quantidade, materialvenda.cdmaterial, " +
					"projeto.cdprojeto, localarmazenagem.cdlocalarmazenagem, materialdevolucao.cdmaterialdevolucao, " +
					"materialdevolucao.qtdedevolvida, loteestoque.cdloteestoque, " +
					
					"pedidovenda.cdpedidovenda, empresaPV.cdpessoa, " + 
					"pedidovendamaterial.cdpedidovendamaterial, pedidovendamaterial.quantidade, " +
					"projetoPV.cdprojeto, localarmazenagemPV.cdlocalarmazenagem, materialpedidovenda.cdmaterial, loteestoquePV.cdloteestoque "
				
				)							
				.leftOuterJoin("entregadocumentosincronizacao.venda venda")
				.leftOuterJoin("venda.empresa empresa")
				.leftOuterJoin("entregadocumentosincronizacao.cliente cliente")
				.leftOuterJoin("entregadocumentosincronizacao.listaEntregamaterialsincronizacao entregamaterialsincronizacao")
				.leftOuterJoin("entregamaterialsincronizacao.material material")
				.leftOuterJoin("entregamaterialsincronizacao.materialdevolucao materialdevolucao")
				.leftOuterJoin("materialdevolucao.vendamaterial vendamaterialMD")
				.leftOuterJoin("vendamaterialMD.loteestoque loteestoque")
				.leftOuterJoin("entregamaterialsincronizacao.materialgrupo materialgrupo")
				.leftOuterJoin("venda.listavendamaterial vendamaterial")
				.leftOuterJoin("vendamaterial.material materialvenda")
				.leftOuterJoin("venda.projeto projeto")
				.leftOuterJoin("venda.localarmazenagem localarmazenagem")
				
				.leftOuterJoin("materialdevolucao.pedidovendamaterial pedidovendamaterialMD")
				.leftOuterJoin("pedidovendamaterialMD.loteestoque loteestoquePV")
				.leftOuterJoin("entregadocumentosincronizacao.pedidovenda pedidovenda")
				.leftOuterJoin("pedidovenda.empresa empresaPV")
				.leftOuterJoin("pedidovenda.listaPedidovendamaterial pedidovendamaterial")
				.leftOuterJoin("pedidovendamaterial.material materialpedidovenda")
				.leftOuterJoin("pedidovenda.projeto projetoPV")
				.leftOuterJoin("pedidovenda.localarmazenagem localarmazenagemPV")
				.where("entregadocumentosincronizacao.cdentregadocumentosincronizacao = ?", cdentregadocumentosincronizacao)
				.where("entregadocumentosincronizacao.devolucao = true")
				.unique();
	}

	/**
	 * M�todo atualiza o campo devolu��o efetuada da entregasincronizacao
	 * Obs: para saber se foi feito a devolu��o do material
	 *
	 * @param entregasincronizacao
	 * @author Luiz Fernando
	 */
	public void updateEntregadocumentosincronizacaoDevolucaoefetuada(Entregadocumentosincronizacao eds) {
		if(eds != null && eds.getCdentregadocumentosincronizacao() != null){
			getHibernateTemplate().bulkUpdate("update Entregadocumentosincronizacao set devolucaoefetuada = true where cdentregadocumentosincronizacao = ?",
				new Object[]{eds.getCdentregadocumentosincronizacao()});
		}
	}
}
