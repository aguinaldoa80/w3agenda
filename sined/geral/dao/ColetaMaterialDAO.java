package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColetaMaterialDAO extends GenericDAO<ColetaMaterial> {

	/**
 	 * Adiciona a quantidade passada por par�metro � quantidade devolvida do item da coleta.
 	 * 
	 * @param coletaMaterial
	 * @param qtde
	 * @author Rafael Salvio
	 */
	public void adicionaQtdeDevolvida(ColetaMaterial coletaMaterial, Double qtde) {
		getHibernateTemplate().bulkUpdate("update ColetaMaterial cm set cm.quantidadedevolvida = coalesce(cm.quantidadedevolvida, 0) + ? " +
											"where cm.cdcoletamaterial = ?", new Object[]{
			qtde, coletaMaterial.getCdcoletamaterial()
		});
	}
	
	public void updateQtdeDevolvidaNota(ColetaMaterial coletaMaterial, Double qtde) {
		getHibernateTemplate().bulkUpdate("update ColetaMaterial cm set cm.quantidadedevolvidanota = coalesce(cm.quantidadedevolvidanota, 0) + ? " +
											"where cm.cdcoletamaterial = ?", new Object[]{
			qtde, coletaMaterial.getCdcoletamaterial()
		});
	}
	
	/**
	 * Adiciona a quantidade passada por par�metro � quantidade comprada do item da coleta
	 *
	 * @param coletaMaterial
	 * @param qtde
	 * @author Rodrigo Freitas
	 * @since 20/08/2014
	 */
	public void adicionaQtdeComprada(ColetaMaterial coletaMaterial, Double qtde) {
		getHibernateTemplate().bulkUpdate("update ColetaMaterial cm set cm.quantidadecomprada = coalesce(cm.quantidadecomprada, 0) + ? " +
											"where cm.cdcoletamaterial = ?", new Object[]{
				qtde, coletaMaterial.getCdcoletamaterial()
		});
	}
	
	/**
	 * M�todo que busca as coletamateriais com suas respectivas quantidades para utiliza��o na montagem de agendamento de produ��o
	 * 
	 * @param whereInPV
	 * @return
	 * @throws Exception
	 * @author Rafael Salvio
	 */
	public List<ColetaMaterial> findByPedidovendaForProducao(String whereInPV, String whereInPVM) throws Exception{
		if(whereInPV == null || whereInPV.isEmpty()){
			throw new Exception("Par�metro 'whereIn' n�o pode ser nulo.");
		}
		
		if("".equals(whereInPVM)){
			whereInPVM = null;
		}
		
		return query()
				.select("coletaMaterial.cdcoletamaterial, coletaMaterial.quantidade, coletaMaterial.quantidadedevolvida, " +
						"pedidovenda.cdpedidovenda, pedidovendamaterialServicos.cdpedidovendamaterial, pedidovendamaterialServicos.quantidade")
//				.join("coletaMaterial.pedidovendamaterial pedidovendamaterial")
//				.join("pedidovendamaterial.pedidovenda pedidovenda")
				.leftOuterJoin("coletaMaterial.listaColetaMaterialPedidovendamaterial listaColetaMaterialPedidovendamaterial")
				.leftOuterJoin("listaColetaMaterialPedidovendamaterial.pedidovendamaterial pedidovendamaterialServicos")
				.join("pedidovendamaterialServicos.pedidovenda pedidovenda")
				.whereIn("pedidovenda.cdpedidovenda", whereInPV)
				.whereIn("pedidovendamaterialServicos.cdpedidovendamaterial", whereInPVM)
				.list();
	}

	/**
	 * Carrega informa��es para a devolu��o
	 *
	 * @param coletaMaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/07/2014
	 */
	public ColetaMaterial loadForDevolucao(ColetaMaterial coletaMaterial) {
		return query()
					.select("coleta.cdcoleta, coletaMaterial.cdcoletamaterial, coletaMaterial.quantidadedevolvida, coletaMaterial.quantidade, " +
							"material.cdmaterial, material.nome, coletaMaterial.observacao, " +
							"pedidovendamaterialServicos.cdpedidovendamaterial")
//					.leftOuterJoin("coletaMaterial.pedidovendamaterial pedidovendamaterial")
					.leftOuterJoin("coletaMaterial.material material")
					.leftOuterJoin("coletaMaterial.listaColetaMaterialPedidovendamaterial listaColetaMaterialPedidovendamaterial")
					.leftOuterJoin("listaColetaMaterialPedidovendamaterial.pedidovendamaterial pedidovendamaterialServicos")
					.join("coletaMaterial.coleta coleta")
					.entity(coletaMaterial)
					.unique();
	}

	/**
	 * Verifica se existe coleta do item do pedido de venda
	 *
	 * @param pedidovendamaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/07/2014
	 */
	public Boolean haveColetaMaterialByPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		return newQueryBuilderWithFrom(Long.class)
					.select("count(*)")
					.where("coletaMaterial.pedidovendamaterial = ?", pedidovendamaterial)
					.unique() > 0;
	}
	
	public Boolean haveColetaMaterialByPedidovendamaterialOtr(Pedidovendamaterial pedidovendamaterial) {
		return newQueryBuilderWithFrom(Long.class)
					.select("count(*)")
					.join("coletaMaterial.listaColetaMaterialPedidovendamaterial listaColetaMaterialPedidovendamaterial")
					.join("listaColetaMaterialPedidovendamaterial.pedidovendamaterial pvm")
					.where("pvm = ?", pedidovendamaterial)
					.unique() > 0;
	}

	/**
	 * Busca todos os itens das coletas
	 *
	 * @param whereInColeta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/07/2014
	 */
	public List<ColetaMaterial> findByColeta(String whereInColeta) {
		QueryBuilder<ColetaMaterial> query = query()
					.select("coleta.cdcoleta, coletaMaterial.cdcoletamaterial, coletaMaterial.quantidadedevolvida, coletaMaterial.quantidadedevolvidanota, coletaMaterial.quantidade, " +
//							"pedidovendamaterial.cdpedidovendamaterial, " +
							"material.cdmaterial, material.nome, material.valorcusto, coletaMaterial.observacao, " +
//							"pedidovendamaterial.quantidade, " +
							"vendamaterial.cdvendamaterial, vendamaterial.quantidade, venda.cdvenda, vendasituacao.cdvendasituacao, " +
							"coletaMaterial.quantidadecomprada, coleta.tipo, coletaMaterial.valorunitario, " +
							"cliente.cdpessoa, cliente.nome, cliente.identificador, empresa.cdpessoa," +
							"pedidovenda.cdpedidovenda,pedidovenda.identificacaoexterna," +
							"contagerencial.cdcontagerencial," +
							"localarmazenagem.cdlocalarmazenagem,localarmazenagem.nome," +
							"unidademedida.cdunidademedida,unidademedida.nome,unidademedida.simbolo, " +
							"pedidovendamaterialServicos.cdpedidovendamaterial, pedidovendamaterialServicos.quantidade")
//					.leftOuterJoin("coletaMaterial.pedidovendamaterial pedidovendamaterial")
//					.leftOuterJoin("pedidovendamaterial.listaVendamaterial vendamaterial")
					.leftOuterJoin("coletaMaterial.listaColetaMaterialPedidovendamaterial listaColetaMaterialPedidovendamaterial")
					.leftOuterJoin("listaColetaMaterialPedidovendamaterial.pedidovendamaterial pedidovendamaterialServicos")
					.leftOuterJoin("pedidovendamaterialServicos.listaVendamaterial vendamaterial")
					.leftOuterJoin("vendamaterial.venda venda")
					.leftOuterJoin("venda.vendasituacao vendasituacao")
					.leftOuterJoin("coletaMaterial.material material")
					.leftOuterJoin("coletaMaterial.pneu pneu")
					.join("coletaMaterial.coleta coleta")
					.leftOuterJoin("coleta.cliente cliente")
					.leftOuterJoin("coleta.pedidovenda pedidovenda")
					.leftOuterJoin("material.unidademedida unidademedida")
					.leftOuterJoin("coletaMaterial.localarmazenagem localarmazenagem")
					.leftOuterJoin("material.contagerencial contagerencial")
					.leftOuterJoin("coleta.empresa empresa")
					.whereIn("coleta.cdcoleta", whereInColeta)
					.orderBy("coleta.cdcoleta,coletaMaterial.cdcoletamaterial");
		SinedUtil.setJoinsByComponentePneu(query);
		return query.list();
	}
	
	/**
	* M�todo que carrega os itens da coleta de acordo com os itens do pedido de venda
	*
	* @param whereInPedidovendamaterial
	* @return
	* @since 17/06/2015
	* @author Luiz Fernando
	*/
	public List<ColetaMaterial> findByPedidovendamaterial(String whereInPedidovendamaterial) {
		if(StringUtils.isEmpty(whereInPedidovendamaterial))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("coletaMaterial.cdcoletamaterial, coletaMaterial.quantidade, coletaMaterial.quantidadedevolvida, coletaMaterial.valorunitario, " +
//					"pedidovendamaterial.cdpedidovendamaterial, " +
					"coleta.cdcoleta, " +
					"material.cdmaterial, material.servico, material.produto, material.epi, material.nome, " +
					"pneu.cdpneu, " +
					"localarmazenagem.cdlocalarmazenagem, pedidovendamaterialServicos.cdpedidovendamaterial, pedidovendamaterialServicos.identificadorintegracao")
//			.leftOuterJoin("coletaMaterial.pedidovendamaterial pedidovendamaterial")
			.leftOuterJoin("coletaMaterial.material material")
			.leftOuterJoin("coletaMaterial.pneu pneu")
			.leftOuterJoin("coletaMaterial.coleta coleta")
			.leftOuterJoin("coletaMaterial.localarmazenagem localarmazenagem")
			.leftOuterJoin("coletaMaterial.listaColetaMaterialPedidovendamaterial listaColetaMaterialPedidovendamaterial")
			.leftOuterJoin("listaColetaMaterialPedidovendamaterial.pedidovendamaterial pedidovendamaterialServicos")
			.whereIn("pedidovendamaterialServicos.cdpedidovendamaterial", whereInPedidovendamaterial)
//			.whereIn("pedidovendamaterial.cdpedidovendamaterial", whereInPedidovendamaterial)
			.list();
	}

	/**
	* M�todo que adiciona o motivo da devolu��o na coleta
	*
	* @param coletaMaterial
	* @param motivodevolucao
	* @since 08/07/2015
	* @author Luiz Fernando
	*/
//	public void adicionaMotivodevolucao(ColetaMaterial coletaMaterial, Motivodevolucao motivodevolucao) {
//		if(coletaMaterial != null && coletaMaterial.getCdcoletamaterial() != null && 
//				motivodevolucao != null && motivodevolucao.getCdmotivodevolucao() != null){
//			String sql = " update coletamaterial set cdmotivodevolucao = " + motivodevolucao.getCdmotivodevolucao() + 
//						 " where cdcoletamaterial = " + coletaMaterial.getCdcoletamaterial() ;
//			getJdbcTemplate().update(sql);
//		}
//	}

	/**
	* M�todo que atualiza a quantidade devolvida ap�s o estorno da coleta
	*
	* @param coletaMaterial
	* @param quantidadeestorno
	* @since 15/10/2015
	* @author Luiz Fernando
	*/
	public void updateQtdeDevolvidaAposEstornoDevolucao(ColetaMaterial coletaMaterial, Double quantidadeestorno) {
		if(coletaMaterial == null || coletaMaterial.getCdcoletamaterial() == null || quantidadeestorno == null)
			throw new SinedException("Par�metro inv�lido.");
		
		String sql = " update coletamaterial set quantidadedevolvida = quantidadedevolvida - " + quantidadeestorno + 
					 " where cdcoletamaterial = " + coletaMaterial.getCdcoletamaterial() ;
		getJdbcTemplate().update(sql);
		
	}
	
	/**
	* M�todo que carrega os itens da coleta de acordo com o pedido de venda
	*
	* @param pedidoVenda
	* @return
	* @since 03/02/2016
	* @author C�sar
	*/
	public List<ColetaMaterial> findByIensInterropidos (String WhereIn) {
		if(WhereIn == null || WhereIn.isEmpty())
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("coletaMaterial.cdcoletamaterial, coletaMaterial.quantidadedevolvida, " +
					"pedidovendamaterialServicos.cdpedidovendamaterial")
//			.leftOuterJoin("coletaMaterial.pedidovendamaterial pedidovendamaterial")	
			.leftOuterJoin("coletaMaterial.listaColetaMaterialPedidovendamaterial listaColetaMaterialPedidovendamaterial")
			.leftOuterJoin("listaColetaMaterialPedidovendamaterial.pedidovendamaterial pedidovendamaterialServicos")
			.whereIn("pedidovendamaterialServicos.cdpedidovendamaterial", WhereIn)
			.list();
	}
	
	public ColetaMaterial findForExpedicao (Integer cdColetaMaterial) {
		return query()
				.select("coletaMaterial.cdcoletamaterial,coletaMaterial.quantidade," +
						"coleta.cdcoleta," +
						"material.cdmaterial")
				.leftOuterJoin("coletaMaterial.coleta coleta")
				.leftOuterJoin("coletaMaterial.material material")
				.where("coletaMaterial.cdcoletamaterial = ?", cdColetaMaterial)
				.unique();
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaColetaMaterialDevolucao");
		save.saveOrUpdateManaged("listaColetamaterialmotivodevolucao");
		save.saveOrUpdateManaged("listaColetaMaterialPedidovendamaterial");
	}
	
	public ColetaMaterial loadColetaByPneuAndMaterialcoleta(Pedidovenda pedidoVenda, Pneu pneu, Material materialColeta){
		return query()
				.select("coletaMaterial.cdcoletamaterial, coleta.cdcoleta")
				.join("coletaMaterial.coleta coleta")
				.join("coletaMaterial.material material")
				.join("coleta.pedidovenda pedidovenda")
				.join("coletaMaterial.pneu pneu")
				.where("pedidovenda = ?", pedidoVenda)
				.where("pneu = ?", pneu)
				.where("material = ?", materialColeta)
				.setMaxResults(1)
				.unique();
	}

	public void updateValorUnitario(ColetaMaterial coletaMaterial, Double valorunitario) {
		if(coletaMaterial == null || coletaMaterial.getCdcoletamaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		getHibernateTemplate().bulkUpdate("update ColetaMaterial cm set cm.valorunitario = ? where cm = ?", new Object[]{
			valorunitario, coletaMaterial
		});
	}
}
