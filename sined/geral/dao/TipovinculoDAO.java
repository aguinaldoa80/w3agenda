package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Tipovinculo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("tipovinculo.nome")
public class TipovinculoDAO extends GenericDAO<Tipovinculo> {

	/**
	 * <p>Recupera uma lista de Tipovinculo cujo Tipooperacao seja igual ao
	 * passado como par�metro.</p>
	 * 
	 * @param tipoOperacao
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Tipovinculo> buscarPor(Tipooperacao tipoOperacao) {
		return querySined()
			
			.where("tipovinculo.tipooperacao = ?", tipoOperacao)
			.orderBy("tipovinculo.nome")
			.list();
	}
}
