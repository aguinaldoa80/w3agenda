package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.MdfeLocalDescarregamento;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MdfeLocalDescarregamentoDAO extends GenericDAO<MdfeLocalDescarregamento>{

	public List<MdfeLocalDescarregamento> findByMdfe(Mdfe mdfe){
		return query()
			.select("mdfeLocalDescarregamento.cdMdfeLocalDescarregamento, municipio.nome, municipio.cdmunicipio, municipio.cdibge, "+
					"uf.cduf, uf.sigla, uf.nome")
			.join("mdfeLocalDescarregamento.municipio municipio")
			.join("municipio.uf uf")
			.where("mdfeLocalDescarregamento.mdfe = ?", mdfe)
			.list();
	}
}
