package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Sindicato;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.SindicatoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("sindicato.nome")
public class SindicatoDAO extends GenericDAO<Sindicato> {
	

	@Override
	public void updateListagemQuery(QueryBuilder<Sindicato> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		SindicatoFiltro filtro = (SindicatoFiltro) _filtro;
		query
			.select("sindicato.cdsindicato, sindicato.nome, sindicato.codigofolha, sindicato.ativo")
			.whereLikeIgnoreAll("sindicato.nome", filtro.getNome())
			.whereLike("cast(sindicato.codigofolha as string)",filtro.getCodigofolha() == null ? null : filtro.getCodigofolha().toString())
			.where("sindicato.ativo = ?", filtro.getMostrar())
			.orderBy("sindicato.nome")
			;
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Sindicato> query) {
		StringBuilder campos  = new StringBuilder();
		campos.append("sindicato.cdsindicato, sindicato.nome, sindicato.descricao, sindicato.codigofolha, ");
		campos.append("sindicato.cdusuarioaltera, sindicato.dtaltera, sindicato.ativo,");
		campos.append("cargo.nome, cargo.cdcargo, colaboradorcargo.cdcolaboradorcargo");
		query
			.select(campos.toString())
			.leftOuterJoin("sindicato.listaColaboradorcargo colaboradorcargo")
			.leftOuterJoin("colaboradorcargo.cargo cargo");
	}

	/**
	 * Busca o sindicato pelo seu c�digo folha.
	 *
	 * @param codigofolha
	 * @return
	 * @since Jul 5, 2011
	 * @author Rodrigo Freitas
	 */
	public Sindicato findByCodigofolha(Integer codigofolha) {
		if(codigofolha == null || codigofolha.equals("")){
			throw new SinedException("C�digo folha n�o pode ser nulo.");
		}
		return query()
					.select("sindicato.cdsindicato, sindicato.nome")
					.where("sindicato.codigofolha = ?", codigofolha)
					.unique();
	}
	

}
