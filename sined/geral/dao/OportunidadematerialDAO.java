package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Oportunidadematerial;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OportunidadematerialDAO extends GenericDAO<Oportunidadematerial>{

	/**
	 * M�todo que carrega a lista de materiais da oportunidade
	 *
	 * @param oportunidade
	 * @return
	 * @author Luiz Fernando
	 * @since 22/10/2013
	 */
	public List<Oportunidadematerial> findByOportunidade(Oportunidade oportunidade) {
		if(oportunidade == null || oportunidade.getCdoportunidade() == null)
			throw new SinedException("Oportunidade n�o pode ser nula.");
		
		return query()
			.select("oportunidadematerial.cdoportunidadematerial, oportunidadematerial.quantidade, oportunidadematerial.valorunitario, " +
					"oportunidade.cdoportunidade, material.cdmaterial, material.nome, material.ncmcompleto, material.prazogarantia, " +
					"ncmcapitulo.descricao, unidademedida.cdunidademedida, unidademedida.nome, " +
					"oportunidadematerial.prazoentregamaterial, materialcaracteristica.cdmaterialcaracteristica, materialcaracteristica.nome," +
					"oportunidadematerial.observacao")
			.join("oportunidadematerial.oportunidade oportunidade")
			.leftOuterJoin("oportunidadematerial.material material")
			.leftOuterJoin("oportunidadematerial.unidademedida unidademedida")
			.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
			.leftOuterJoin("material.listaCaracteristica materialcaracteristica")
			.where("oportunidade = ?", oportunidade)
			.orderBy("oportunidadematerial.cdoportunidadematerial")
			.list();
	}

	public List<Oportunidadematerial> findByOportunidade(String whereIn) {
		if (whereIn == null || whereIn.trim().equals("")) {
			throw new SinedException("Oportunidade n�o pode ser nulo.");
		}
		QueryBuilder<Oportunidadematerial> query =
		 querySined()
				.select("oportunidadematerial.cdoportunidadematerial, oportunidadematerial.quantidade, oportunidadematerial.valorunitario, "
						+ "oportunidade.cdoportunidade, material.cdmaterial, material.nome, unidademedida.cdunidademedida, unidademedida.nome ")
				.join("oportunidadematerial.oportunidade oportunidade")
				.leftOuterJoin("oportunidadematerial.material material")
				.leftOuterJoin("oportunidadematerial.unidademedida unidademedida")
				.orderBy("oportunidadematerial.cdoportunidadematerial");
		SinedUtil.quebraWhereIn("oportunidade.cdoportunidade", whereIn, query);
		return query.list();
	}

}
