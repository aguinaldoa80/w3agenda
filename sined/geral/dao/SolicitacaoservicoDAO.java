package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Solicitacaoservico;
import br.com.linkcom.sined.geral.bean.enumeration.SolicitacaoServicoPessoa;
import br.com.linkcom.sined.geral.bean.enumeration.Solicitacaoservicosituacao;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.SolicitacaoservicoFiltro;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.filter.EventoservicointernoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("solicitacaoservico.cdsolicitacaoservico desc")
public class SolicitacaoservicoDAO extends GenericDAO<Solicitacaoservico> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Solicitacaoservico> query, FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("Filtro n�o pode ser nulo.");
		}
		SolicitacaoservicoFiltro filtro = (SolicitacaoservicoFiltro) _filtro;
		
		query
			.select("distinct solicitacaoservico.cdsolicitacaoservico, solicitacaoservico.solicitacaoservicosituacao, " +
					"solicitacaoservico.solicitacaoservicoprioridade, requisitante.nome, solicitacaoservicotipo.nome, " +
					"solicitacaoservico.dtinicio, solicitacaoservico.dtfim, solicitacaoservico.descricao, " +
					"cliente.nome, enderecocliente.cdendereco, solicitacaoservico.dtenvio, arquivo.cdarquivo, " +
					"arquivo.nome, arquivo.tipoconteudo, arquivo.tamanho, arquivo.dtmodificacao")
			.leftOuterJoin("solicitacaoservico.requisitante requisitante")
			.leftOuterJoin("solicitacaoservico.projeto projeto") 
			.leftOuterJoin("solicitacaoservico.enderecocliente enderecocliente") 
			.leftOuterJoin("solicitacaoservico.listaRequisitado listaRequisitado")
			.leftOuterJoin("listaRequisitado.colaborador requisitado")
			.leftOuterJoin("solicitacaoservico.cliente cliente")
			.leftOuterJoin("solicitacaoservico.solicitacaoservicotipo solicitacaoservicotipo")
			.leftOuterJoin("solicitacaoservico.arquivo arquivo")
			.leftOuterJoin("solicitacaoservico.fornecedor fornecedor")
			.where("solicitacaoservico.cdsolicitacaoservico = ?", filtro.getCdsolicitacaoservico())
			.where("solicitacaoservicotipo = ?", filtro.getSolicitacaoservicotipo())
			.where("requisitante = ?", filtro.getRequisitante())
			.where("projeto = ?", filtro.getProjeto())
			.where("requisitado = ?", filtro.getRequisitado())
			.where("solicitacaoservico.solicitacaoservicoprioridade = ?", filtro.getSolicitacaoservicoprioridade())
			.where("solicitacaoservico.dtenvio >= ?", filtro.getDtenviode())
			.where("solicitacaoservico.dtenvio <= ?", filtro.getDtenvioate())
			.where("solicitacaoservico.dtinicio >= ?", filtro.getDtiniciode())
			.where("solicitacaoservico.dtinicio <= ?", filtro.getDtinicioate())
			.where("solicitacaoservico.dtfim >= ?", filtro.getDtfimde())
			.where("solicitacaoservico.dtfim <= ?", filtro.getDtfimate())
			.whereIn("solicitacaoservico.solicitacaoservicosituacao", Solicitacaoservicosituacao.listAndConcatenate(filtro.getListaSituacao()))
			.ignoreJoin("listaRequisitado", "requisitado");
		
		if(SolicitacaoServicoPessoa.CLIENTE.equals(filtro.getSolicitacaoServicoPessoa()))
			query.where("cliente = ?", filtro.getCliente());
		else if(SolicitacaoServicoPessoa.FORNECEDOR.equals(filtro.getSolicitacaoServicoPessoa()))
			query.where("fornecedor = ?", filtro.getFornecedor());
		else if(SolicitacaoServicoPessoa.OUTRO.equals(filtro.getSolicitacaoServicoPessoa()))
			query.whereLikeIgnoreAll("solicitacaoservico.outros", filtro.getOutros());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Solicitacaoservico> query) {
		query
			.select("solicitacaoservico.cdsolicitacaoservico, cliente.cdpessoa, cliente.nome, requisitante.cdpessoa, requisitante.nome, " +
					"solicitacaoservico.solicitacaoservicoprioridade, solicitacaoservico.dtinicio, solicitacaoservico.dtfim, " +
					"solicitacaoservico.descricao, solicitacaoservico.dtenvio, " +
					"listaRequisitado.cdsolicitacaoservicorequisitado, listaRequisitado.email, colaborador.cdpessoa, colaborador.nome, " +
					"listaItem.cdsolicitacaoservicoitem, listaItem.valor, solicitacaoservicotipo.cdsolicitacaoservicotipo, " +
					"solicitacaoservicotipo.nome, projeto.cdprojeto, projeto.nome, solicitacaoservicotipoitem.obrigatorio," +
					"solicitacaoservicotipoitem.nome, solicitacaoservicotipoitem.cdsolicitacaoservicotipoitem, " +
					"enderecocliente.cdendereco, enderecocliente.logradouro, enderecocliente.bairro, enderecocliente.complemento, " +
					"enderecocliente.pontoreferencia, municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla, arquivo.cdarquivo, " +
					"arquivo.nome, arquivo.tipoconteudo, arquivo.tamanho, arquivo.dtmodificacao, " +
					"solicitacaoservico.solicitacaoServicoPessoa, solicitacaoservico.outros, solicitacaoservico.isEnderecoAvulso, solicitacaoservico.enderecoAvulso, " +
					"fornecedor.cdpessoa, fornecedor.nome")
			.leftOuterJoin("solicitacaoservico.cliente cliente")
			.leftOuterJoin("solicitacaoservico.listaRequisitado listaRequisitado")
			.leftOuterJoin("listaRequisitado.colaborador colaborador")
			.leftOuterJoin("solicitacaoservico.requisitante requisitante")
			.leftOuterJoin("solicitacaoservico.solicitacaoservicotipo solicitacaoservicotipo")
			.leftOuterJoin("solicitacaoservico.listaItem listaItem")
			.leftOuterJoin("listaItem.solicitacaoservicotipoitem solicitacaoservicotipoitem")
			.leftOuterJoin("solicitacaoservico.projeto projeto")
			.leftOuterJoin("solicitacaoservico.enderecocliente enderecocliente")
			.leftOuterJoin("enderecocliente.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("solicitacaoservico.arquivo arquivo")
			.leftOuterJoin("solicitacaoservico.fornecedor fornecedor");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaRequisitado");
		save.saveOrUpdateManaged("listaItem");
	}

	/**
	 * Faz a atualiza��o da data de envio da solicita��o de servi�o para a data atual.
	 *
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void updateDataEnvio(Solicitacaoservico bean) {
		getHibernateTemplate().bulkUpdate("update Solicitacaoservico s set s.dtenvio = ? where s.id = ?", 
				new Object[]{new Timestamp(System.currentTimeMillis()), bean.getCdsolicitacaoservico()});
	}

	/**
	 * Verifica se tem alguma solicita��o de servi�o com situa��o diferente das que s�o passadas por par�metro.
	 *
	 * @param whereIn
	 * @param situacao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean haveSolicitacaoSituacaoDiferente(String whereIn, Solicitacaoservicosituacao[] situacao) {
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
				.from(Solicitacaoservico.class)
				.setUseTranslator(false)
				.select("count(*)")
				.whereIn("solicitacaoservico.cdsolicitacaoservico", whereIn);
		
		for (int i = 0; i < situacao.length; i++) {
			query.where("solicitacaoservico.solicitacaoservicosituacao <> ?", situacao[i]);
		}
		
		return query.unique() > 0;
	}

	/**
	 * Atualiza a situa��o da solicita��o de servi�o passada por par�metro.
	 *
	 * @param whereIn
	 * @param situacao
	 * @author Rodrigo Freitas
	 */
	public void updateSituacao(String whereIn, Solicitacaoservicosituacao situacao) {
		getHibernateTemplate()
			.bulkUpdate("update Solicitacaoservico s set s.solicitacaoservicosituacao = ? where s.id in (" + whereIn + ")", situacao);
	}

	/**
	 * Carrega as informa��es da solicita��o de servi�o para o envio de e-mail.
	 *
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Solicitacaoservico loadForEmailConcluir(Solicitacaoservico bean) {
		if(bean == null || bean.getCdsolicitacaoservico() == null){
			throw new SinedException("A solicita��o de servi�o n�o pode ser nula.");
		}
		return query()
					.select("solicitacaoservico.cdsolicitacaoservico, requisitante.email, solicitacaoservicotipo.nome")
					.leftOuterJoin("solicitacaoservico.requisitante requisitante")
					.leftOuterJoin("solicitacaoservico.solicitacaoservicotipo solicitacaoservicotipo")
					.where("solicitacaoservico = ?", bean)
					.unique();
	}
	
	public List<Solicitacaoservico> findForRelatorioEvento(EventoservicointernoFiltro filtro){
		if(filtro==null){
			throw new SinedException("O filtro n�o pode ser nulo em ProtocoloDAO");
		}	
		return querySined()
			   
		       .select("solicitacaoservico.cdsolicitacaoservico, solicitacaoservicotipo.cdsolicitacaoservicotipo," +
						"solicitacaoservicotipo.nome, solicitacaoservico.solicitacaoservicosituacao," +
						"solicitacaoservico.dtenvio, requisitante.cdpessoa, requisitante.nome, colaborador.cdpessoa, colaborador.nome")
				.leftOuterJoin("solicitacaoservico.solicitacaoservicotipo solicitacaoservicotipo")
				.leftOuterJoin("solicitacaoservico.requisitante requisitante")
				.leftOuterJoin("solicitacaoservico.listaRequisitado listaRequisitado")
				.leftOuterJoin("listaRequisitado.colaborador colaborador")
				.whereLikeIgnoreAll("solicitacaoservicotipo.nome", filtro.getAssunto())
				.whereLikeIgnoreAll("requisitante.nome", filtro.getRemetente())
				.whereLikeIgnoreAll("colaborador.nome", filtro.getDestinatario())				
				.where("solicitacaoservico.dtenvio >= ?", filtro.getInicio())
				.where("solicitacaoservico.dtenvio <= ?", filtro.getFim())
				.where("solicitacaoservico.solicitacaoservicotipo = ?",filtro.getTipo())
				.orderBy("solicitacaoservico.dtenvio asc")
				.list();
	}
	
	/**
	 * Retorna as solicita��es de servi�os requisitadas ao colaborador logado.
	 * @param dataString
	 * @return
	 * @author Taidson
	 * @since 02/07/2010
	 */
	public List<Solicitacaoservico> findForAgendaRequisitadoFlex(Colaborador colaborador, java.sql.Date data){
		Date dataInicio =	SinedDateUtils.addDiasData(data, 1);
		return query()
		.select("solicitacaoservico.cdsolicitacaoservico, solicitacaoservico.descricao, " +
				"solicitacaoservico.dtinicio, solicitacaoservico.dtfim")
		.leftOuterJoin("solicitacaoservico.requisitante requisitante")
		.leftOuterJoin("solicitacaoservico.listaRequisitado listaRequisitado")
		.leftOuterJoin("listaRequisitado.colaborador requisitado")
		.where("requisitado = ?", colaborador)
		.where("solicitacaoservico.dtinicio < ?", dataInicio)
		.where("solicitacaoservico.dtfim >= ?", data)
		.list();
	}
	
	/**
	 * Retorna a quantidade de solicita��es de servi�os pendentes requisitadas ao colaborador logado.
	 * @return
	 * @author Taidson
	 * @since 07/07/2010
	 */
	public Long solicitacoesPendentesForFlex(Colaborador colaborador){
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Solicitacaoservico.class)
					.leftOuterJoin("solicitacaoservico.listaRequisitado listaRequisitado")
					.leftOuterJoin("listaRequisitado.colaborador requisitado")
					.where("requisitado = ?", colaborador)
					.where("solicitacaoservico.solicitacaoservicosituacao = ?",Solicitacaoservicosituacao.PENDENTE)
					.unique();
	}

	public List<Solicitacaoservico> findForRelatorio(SolicitacaoservicoFiltro filtro) {
		QueryBuilder<Solicitacaoservico> query = querySined();
		if (!StringUtils.isEmpty(filtro.getOrderBy())) {
			query.orderBy(filtro.getOrderBy()+" "+(filtro.isAsc()?"ASC":"DESC"));
		}
		
		updateListagemQuery(query, filtro);
		return query.list();
	}

	/**
	 * M�todo que carrega dos dados da solicita��o de servico para enviar solicita��o por email
	 *
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 */
	public Solicitacaoservico loadForEnviarSolicitacao(Solicitacaoservico bean) {
		if(bean == null || bean.getCdsolicitacaoservico() == null) throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("solicitacaoservico.cdsolicitacaoservico, solicitacaoservico.solicitacaoservicoprioridade, " +
						"listaRequisitado.cdsolicitacaoservicorequisitado, listaRequisitado.email, colaborador.cdpessoa, " +
						"colaborador.nome, solicitacaoservicotipo.cdsolicitacaoservicotipo ")
				.leftOuterJoin("solicitacaoservico.listaRequisitado listaRequisitado")
				.leftOuterJoin("listaRequisitado.colaborador colaborador")
				.leftOuterJoin("solicitacaoservico.solicitacaoservicotipo solicitacaoservicotipo")
				.where("solicitacaoservico = ?", bean)
				.unique();
	}

	@Override
	public ListagemResult<Solicitacaoservico> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Solicitacaoservico> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("solicitacaoservico.cdsolicitacaoservico");
		
		return new ListagemResult<Solicitacaoservico>(query, filtro);
	}
}
