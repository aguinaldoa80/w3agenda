package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Solicitacaoservicotipo;
import br.com.linkcom.sined.geral.bean.Solicitacaoservicotipoitem;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class SolicitacaoservicotipoitemDAO extends GenericDAO<Solicitacaoservicotipoitem>{
   
	/**
	 * Retorna a lista de tipoitem da solicitacao tipp
	 * @param tipo
	 * @return
	 * @author C�ntia Nogueira
	 */
	public List<Solicitacaoservicotipoitem> findByTipo(Solicitacaoservicotipo tipo){
	  if(tipo == null  || tipo.getCdsolicitacaoservicotipo() == null){
		  throw new SinedException("Objeto n�o pode ser nulo em SolicitacaoservicotipoitemDAO.");
	  }
		return query()
	   		 .select("solicitacaoservicotipoitem.cdsolicitacaoservicotipoitem, solicitacaoservicotipoitem.nome, solicitacaoservicotipoitem.obrigatorio")
	   		 		.where("solicitacaoservicotipoitem.solicitacaoservicotipo = ?", tipo)
	   		 		.list();
   }
}
