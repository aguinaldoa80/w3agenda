package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.AjusteIpi;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.AjusteIpiFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AjusteIpiDAO extends GenericDAO<AjusteIpi> {

	@Override
	public void updateListagemQuery(QueryBuilder<AjusteIpi> query, FiltroListagem _filtro) {
		AjusteIpiFiltro filtro = (AjusteIpiFiltro) _filtro;
		
		query
			.select("ajusteIpi.cdAjusteIpi, ajusteIpi.dtFatoGerador, ajusteIpi.valor, ajusteIpi.tipoAjuste, " +
					"empresa.nome, empresa.razaosocial, empresa.nomefantasia, codigoAjuste.codigo, codigoAjuste.descricao ")
			.leftOuterJoin("ajusteIpi.empresa empresa")
			.leftOuterJoin("ajusteIpi.codigoAjuste codigoAjuste")
			.where("empresa = ?", filtro.getEmpresa())
			.where("ajusteIpi.tipoAjuste = ?", filtro.getTipoAjuste()) 
			.where("codigoAjuste = ?", filtro.getCodigoAjuste())
			.where("ajusteIpi.dtFatoGerador = ?", filtro.getDtFatoGerador());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<AjusteIpi> query) {
		query
			.leftOuterJoinFetch("ajusteIpi.codigoAjuste codigoAjuste")
			.leftOuterJoinFetch("ajusteIpi.ajusteIpiDocumentoList ajusteIpiDocumentoList")
			.leftOuterJoinFetch("ajusteIpiDocumentoList.pessoa pessoa")
			.leftOuterJoinFetch("ajusteIpiDocumentoList.material material");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("ajusteIpiDocumentoList");
	}
	
	public List<AjusteIpi> findForApuracaoIpi(SpedarquivoFiltro filtro) {
		return query()
				.select("ajusteIpi.cdAjusteIpi, ajusteIpi.dtFatoGerador, ajusteIpi.valor, ajusteIpi.origemDocumento, ajusteIpi.tipoAjuste, ajusteIpi.numeroDocumento, ajusteIpi.descricaoComplementar, " +
						"empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
						"codigoAjuste.codigo, codigoAjuste.descricao, " +
						"ajusteIpiDocumentoList.cdAjusteIpiDocumento, ajusteIpiDocumentoList.numero, ajusteIpiDocumentoList.serie, ajusteIpiDocumentoList.subserie, " +
						"ajusteIpiDocumentoList.modeloDocumento, ajusteIpiDocumentoList.chaveAcesso, ajusteIpiDocumentoList.tipo, ajusteIpiDocumentoList.dtEmissao, ajusteIpiDocumentoList.valor, " +
						"pessoa.cdpessoa, " +
						"material.cdmaterial," +
						"listaEndereco.cdendereco ")
				.leftOuterJoin("ajusteIpi.empresa empresa")
				.leftOuterJoin("ajusteIpi.codigoAjuste codigoAjuste")
				.leftOuterJoin("ajusteIpi.ajusteIpiDocumentoList ajusteIpiDocumentoList")
				.leftOuterJoin("ajusteIpiDocumentoList.pessoa pessoa")
				.leftOuterJoin("pessoa.listaEndereco listaEndereco")
				.leftOuterJoin("ajusteIpiDocumentoList.material material")
				.where("empresa = ?", filtro.getEmpresa())
				.where("ajusteIpi.dtFatoGerador >= ?", filtro.getDtinicio())
				.where("ajusteIpi.dtFatoGerador <= ?", filtro.getDtfim())
				.list();
	}
}
