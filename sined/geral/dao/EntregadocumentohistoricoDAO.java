package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregadocumentohistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EntregadocumentohistoricoDAO extends GenericDAO<Entregadocumentohistorico> {

	/**
	 * M�todo busca os hist�ricos da entregadocumento
	 *
	 * @param entregadocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumentohistorico> findByEntregadocumento(Entregadocumento entregadocumento) {
		if(entregadocumento == null || entregadocumento.getCdentregadocumento() == null)
			throw new SinedException("Entregadocumentohistorico n�o pode ser nulo");
				
		return query()
				.select("entregadocumentohistorico.cdentregadocumentohistorico, entregadocumento.cdentregadocumento, " +
						"entregadocumentohistorico.entregadocumentosituacao, entregadocumentohistorico.dtaltera, " +
						"entregadocumentohistorico.cdusuarioaltera, entregadocumentohistorico.observacao")
				.join("entregadocumentohistorico.entregadocumento entregadocumento")
				.where("entregadocumento = ?", entregadocumento)
				.orderBy("entregadocumentohistorico.dtaltera desc")
				.list();
	}
	
	/**
	 * M�todo que busca o hit�rico com os dados da nota
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumentohistorico> findForDevolucaoEntregadocumento(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido");
				
		return query()
				.select("entregadocumentohistorico.cdentregadocumentohistorico, entregadocumento.cdentregadocumento, " +
						"notafiscalproduto.cdNota, material.cdmaterial, listaItens.qtde")
				.join("entregadocumentohistorico.entregadocumento entregadocumento")
				.join("entregadocumentohistorico.notafiscalproduto notafiscalproduto")
				.join("notafiscalproduto.notaStatus notaStatus")
				.join("notafiscalproduto.listaItens listaItens")
				.join("listaItens.material material")
				.where("notaStatus <> ?", NotaStatus.CANCELADA)
				.whereIn("entregadocumento.cdentregadocumento", whereIn)
				.orderBy("entregadocumento.cdentregadocumento desc")
				.list();
	}
	
	/**
	 * M�todo que remove os elementos de entregadocumentohistorico que n�o mais perten�am a uma entrega.
	 * 
	 * @param cdentrega
	 * @param whereNotIn
	 * @author Rafael Salvio
	 */
	public void deleteAllFromEntregaNotInWhereIn(Integer cdentrega, String whereNotIn){
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM entregadocumentohistorico WHERE cdentrega = "+cdentrega);
		if(whereNotIn != null){
			sql.append(" AND cdentregadocumentohistorico NOT IN ("+whereNotIn+")");
		}
		
		getJdbcTemplate().execute(sql.toString());
	}
	
}
