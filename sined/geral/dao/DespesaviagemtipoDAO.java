package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Despesaviagemtipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.DespesaviagemtipoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("upper(retira_acento(despesaviagemtipo.nome))")
public class DespesaviagemtipoDAO extends GenericDAO<Despesaviagemtipo> {

	@Override
	public void updateListagemQuery(QueryBuilder<Despesaviagemtipo> query, FiltroListagem _filtro) {
		DespesaviagemtipoFiltro filtro = (DespesaviagemtipoFiltro) _filtro;
		query
			.select("despesaviagemtipo.cddespesaviagemtipo, despesaviagemtipo.ativo, despesaviagemtipo.nome")
			.whereLikeIgnoreAll("despesaviagemtipo.nome", filtro.getNome())
			.where("despesaviagemtipo.ativo = ?",filtro.getAtivo())
			.orderBy("upper(retira_acento(despesaviagemtipo.nome))");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Despesaviagemtipo> query) {
		query
			.select("despesaviagemtipo.cddespesaviagemtipo ,despesaviagemtipo.nome, despesaviagemtipo.ativo," +
					"despesaviagemtipo.cdusuarioaltera, despesaviagemtipo.dtaltera, despesaviagemtipo.valorbase," +
					"contagerencial.cdcontagerencial, contagerencial.nome, contagerencial.codigoalternativo, vcontagerencial.identificador," +
					"vcontagerencial.arvorepai")
			.leftOuterJoin("despesaviagemtipo.contagerencial contagerencial")
			.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial");
	}
	
}
