package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetorno;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRemessaTipoEnum;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.BancoConfiguracaoRetornoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class BancoConfiguracaoRetornoDAO extends GenericDAO<BancoConfiguracaoRetorno> {

	@Override
	public void updateEntradaQuery(QueryBuilder<BancoConfiguracaoRetorno> query) {
		query
			.select("bancoConfiguracaoRetorno.cdbancoconfiguracaoretorno, bancoConfiguracaoRetorno.nome, bancoConfiguracaoRetorno.tamanho, " +
					"bancoConfiguracaoRetorno.tipo, bancoConfiguracaoRetorno.cdusuarioaltera, bancoConfiguracaoRetorno.dtaltera," +
					"bancoConfiguracaoRetorno.criarMovimentacao, " + 
					"banco.cdbanco, banco.nome, banco.numero, " + 
					"bancoConfiguracaoRetornoSegmento.cdbancoconfiguracaoretornosegmento, bancoConfiguracaoRetornoSegmento.nome, " +
					"bancoConfiguracaoRetornoSegmento.tipo, bancoConfiguracaoRetornoSegmento.identificador, " +
					"bancoConfiguracaoRetornoSegmento2.cdbancoconfiguracaoretornosegmento, bancoConfiguracaoRetornoSegmento2.nome, " +
					"bancoConfiguracaoRetornoSegmento2.tipo, bancoConfiguracaoRetornoSegmento2.identificador, " +
					"bancoSegmento.cdbanco, bancoSegmento.nome, " +
					"bancoConfiguracaoRetornoCampo.cdbancoconfiguracaoretornocampo, bancoConfiguracaoRetornoCampo.campo, " + 
					"bancoConfiguracaoRetornoCampo.posIni, bancoConfiguracaoRetornoCampo.tamanho, " +
					"bancoConfiguracaoRetornoCampo.qtdeDecimal, bancoConfiguracaoRetornoCampo.formatoData, bancoConfiguracaoRetornoCampo.calculado, " +
					"bancoConfiguracaoRetornoOcorrencia.cdbancoconfiguracaoretornoocorrencia, bancoConfiguracaoRetornoOcorrencia.codigo, " +
					"bancoConfiguracaoRetornoOcorrencia.descricao, bancoConfiguracaoRetornoOcorrencia.acao, " +
					"bancoConfiguracaoRetornoOcorrencia.historico, bancoConfiguracaoRetornoOcorrencia.situacaoProtesto," +
					"bancoConfiguracaoRetornoRejeicao.cdbancoconfiguracaoretornorejeicao, bancoConfiguracaoRetornoRejeicao.codigo, " + 
					"bancoConfiguracaoRetornoRejeicao.descricao, bancoConfiguracaoRetornoRejeicao.ocorrencia," +
					"bancoConfiguracaoRetorno.criarMovimentacao, bancoConfiguracaoRetorno.incluirJurosMultaDescontoTaxasDocumento," +
					"bancoConfiguracaoRetornoIdentificador.tipo,  bancoConfiguracaoRetornoIdentificador.cdbancoConfiguracaoRetornoIdentificador, bancoConfiguracaoRetornoIdentificador.posIni, bancoConfiguracaoRetornoIdentificador.tamanho, bancoConfiguracaoRetornoSegmentoDetalhe.cdBancoConfiguracaoRetornoSegmentoDetalhe,"+
					"bancoConfiguracaoRetorno.atualizarjurosmulta, bancoConfiguracaoRetorno.considerardatamovimentacao")
			.leftOuterJoin("bancoConfiguracaoRetorno.banco banco")			
			.leftOuterJoin("bancoConfiguracaoRetorno.listaBancoConfiguracaoRetornoCampo bancoConfiguracaoRetornoCampo")
			.leftOuterJoin("bancoConfiguracaoRetornoCampo.bancoConfiguracaoRetornoSegmento bancoConfiguracaoRetornoSegmento")
			.leftOuterJoin("bancoConfiguracaoRetornoSegmento.banco bancoSegmento")
			.leftOuterJoin("bancoConfiguracaoRetorno.listaBancoConfiguracaoRetornoOcorrencia bancoConfiguracaoRetornoOcorrencia")
			.leftOuterJoin("bancoConfiguracaoRetorno.listaBancoConfiguracaoRetornoRejeicao bancoConfiguracaoRetornoRejeicao")
			.leftOuterJoin("bancoConfiguracaoRetorno.listaBancoConfiguracaoRetornoIdentificador bancoConfiguracaoRetornoIdentificador")
			.leftOuterJoin("bancoConfiguracaoRetorno.listaBancoConfiguracaoRetornoSegmentoDetalhe bancoConfiguracaoRetornoSegmentoDetalhe")
			.leftOuterJoin("bancoConfiguracaoRetornoSegmentoDetalhe.bancoConfiguracaoRetornoSegmento bancoConfiguracaoRetornoSegmento2")
			.orderBy("bancoConfiguracaoRetornoCampo.campo");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<BancoConfiguracaoRetorno> query, FiltroListagem _filtro) {
		BancoConfiguracaoRetornoFiltro filtro = (BancoConfiguracaoRetornoFiltro) _filtro;
		
		query
			.select("bancoConfiguracaoRetorno.cdbancoconfiguracaoretorno, bancoConfiguracaoRetorno.nome, banco.cdbanco, " + 
					"bancoConfiguracaoRetorno.tamanho, bancoConfiguracaoRetorno.tipo, banco.numero, banco.nome ")
			.leftOuterJoin("bancoConfiguracaoRetorno.banco banco")
			.whereLikeIgnoreAll("bancoConfiguracaoRetorno.nome", filtro.getNome())
			.where("bancoConfiguracaoRetorno.banco = ?", filtro.getBanco());
	}
	
/*	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.useTransaction(true);
		save.saveOrUpdateManaged("listaBancoConfiguracaoRetornoCampo");
		save.saveOrUpdateManaged("listaBancoConfiguracaoRetornoOcorrencia");
		save.saveOrUpdateManaged("listaBancoConfiguracaoRetornoRejeicao");
		save.saveOrUpdateManaged("listaBancoConfiguracaoRetornoIdentificador");
		save.saveOrUpdateManaged("listaBancoConfiguracaoRetornoSegmentoDetalhe");
		super.updateSaveOrUpdate(save);
	}*/
	
	public List<BancoConfiguracaoRetorno> findByBancoAndTamanhoAndTipo(Banco banco, Integer tamanho, BancoConfiguracaoRemessaTipoEnum tipo){
		QueryBuilder<BancoConfiguracaoRetorno> query = query();
		updateEntradaQuery(query);
		return query
				.where("bancoConfiguracaoRetorno.banco = ?", banco)
				.where("bancoConfiguracaoRetorno.tamanho = ?", tamanho)
				.where("bancoConfiguracaoRetorno.tipo = ?", tipo)
				.list();
	}
	
	/**
	 * M�todo respons�vel por retornar BancoConfiguracaoRetorno para exporta��o
	 * @param whereIn
	 * @return
	 * @since 08/06/2016
	 * @author C�sar
	 */
	public List<BancoConfiguracaoRetorno> findforWhereIn(String whereIn){
		if(whereIn == null || whereIn.isEmpty()){
			throw new SinedException("");
		}
		return query().select("bancoConfiguracaoRetorno.cdbancoconfiguracaoretorno, bancoConfiguracaoRetorno.nome, banco.cdbanco, banco.nome," +
			"bancoConfiguracaoRetorno.tamanho, bancoConfiguracaoRetorno.tipo, campos.cdbancoconfiguracaoretornocampo, campos.campo," +
			"campos.posIni, campos.tamanho, campos.qtdeDecimal, campos.formatoData, campos.calculado, ocorrencia.cdbancoconfiguracaoretornoocorrencia," +
			"ocorrencia.codigo, ocorrencia.descricao, ocorrencia.acao, ocorrencia.historico, rejeicao.cdbancoconfiguracaoretornorejeicao," +
			"rejeicao.codigo,rejeicao.descricao, segmento.cdbancoconfiguracaoretornosegmento, segmento.nome, segmento.tipo, segmento.identificador," +
			"bancoseg.cdbanco, bancoseg.nome")
			.leftOuterJoin("bancoConfiguracaoRetorno.banco banco")
			.leftOuterJoin("bancoConfiguracaoRetorno.listaBancoConfiguracaoRetornoCampo campos")
			.leftOuterJoin("campos.bancoConfiguracaoRetornoSegmento segmento")
			.leftOuterJoin("segmento.banco bancoseg")
			.leftOuterJoin("bancoConfiguracaoRetorno.listaBancoConfiguracaoRetornoOcorrencia ocorrencia")
			.leftOuterJoin("bancoConfiguracaoRetorno.listaBancoConfiguracaoRetornoRejeicao rejeicao")
			.whereIn("bancoConfiguracaoRetorno.cdbancoconfiguracaoretorno", whereIn)
			.list();
	}

	@Override
	public ListagemResult<BancoConfiguracaoRetorno> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<BancoConfiguracaoRetorno> query = query();
		updateListagemQuery(query, filtro);
		
		return new ListagemResult<BancoConfiguracaoRetorno>(query, filtro);
	}
}