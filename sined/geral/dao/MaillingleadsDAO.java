package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.MaillingleadsFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaillingleadsDAO extends GenericDAO<Lead> {
 
	@Override
	public void updateListagemQuery(QueryBuilder<Lead> query, FiltroListagem _filtro) {
		
		MaillingleadsFiltro filtro = (MaillingleadsFiltro) _filtro;
		query
			.select("distinct lead.cdlead, lead.nome, lead.empresa, responsavel.cdpessoa, responsavel.nome")
			.leftOuterJoin("lead.responsavel responsavel")
			.leftOuterJoin("lead.leadqualificacao leadqualificacao")
			.leftOuterJoin("lead.leadsituacao leadsituacao")
			.leftOuterJoin("lead.listLeadsegmento listLeadsegmento")
			.leftOuterJoin("listLeadsegmento.segmento segmento")
			.leftOuterJoin("lead.listLeadcampanha listLeadcampanha")
			.leftOuterJoin("listLeadcampanha.campanha campanha")
			.leftOuterJoin("lead.listaContacrm listaContacrm")
			.leftOuterJoin("lead.cliente cliente")
			.openParentheses()
				.where("exists (select le.cdleademail from Leademail le join le.lead l where l.cdlead = lead.cdlead)")
				.or()
				.openParentheses()
					.where("lead.email is not null")
					.where("lead.email <> ''")
				.closeParentheses()
			.closeParentheses()
			.where("responsavel = ?", filtro.getResponsavel())
			.where("leadqualificacao = ?", filtro.getLeadqualificacao())
			.where("leadsituacao = ?", filtro.getLeadsituacao())
			.where("campanha = ?", filtro.getCampanha())
			.where("listaContacrm is null")
			.where("cliente is null ")
			.ignoreJoin("listLeadsegmento", "segmento", "listLeadcampanha", "campanha", "listaContacrm", "cliente")
			;
		
		if(filtro.getListasegmento() != null && !filtro.getListasegmento().isEmpty())
			query.whereIn("segmento.cdsegmento", CollectionsUtil.listAndConcatenate(filtro.getListasegmento(), "cdsegmento",","));
		
	}

	/**
	 * Query da listagem com as listas carregadas
	 *
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/10/2012
	 */
	public List<Lead> findListagem(String whereIn, String orderBy, boolean asc) {
		QueryBuilder<Lead> query = query()
										.select("lead.cdlead, lead.nome, lead.email, lead.empresa, responsavel.cdpessoa, responsavel.nome, listleademail.cdleademail, listleademail.email")
										.leftOuterJoin("lead.responsavel responsavel")
										.leftOuterJoin("lead.listleademail listleademail")
										.whereIn("lead.cdlead", whereIn)
										;
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("lead.cdlead DESC");
		}
		
		return query.list();
	}
	
}
