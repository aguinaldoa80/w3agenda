package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class UnidademedidaconversaoDAO extends GenericDAO<Unidademedidaconversao> {

	/**
	 * Retorna um lista de unidademedidaconversao associada � uma determinada unidade de medida.
	 * @param unidademedida
	 * @return
	 * @author Taidson
	 * @since 30/12/2010
	 */
	public List<Unidademedidaconversao> conversoesByUnidademedida(Unidademedida unidademedida){
		return query()
			.select("unidademedidaconversao.cdunidademedidaconversao, unidademedidaconversao.fracao, unidademedidaconversao.qtdereferencia, " +
					"unidademedida.cdunidademedida, unidademedidarelacionada.cdunidademedida, unidademedida.casasdecimaisestoque, " +
					"unidademedidarelacionada.casasdecimaisestoque ")
			.join("unidademedidaconversao.unidademedida unidademedida")
			.join("unidademedidaconversao.unidademedidarelacionada unidademedidarelacionada")
			.where("unidademedida.ativo = ?", Boolean.TRUE)
			.where("unidademedida = ?", unidademedida)
			.list();
	}
	
	/**
	 * Retorna as unidades e unidades relacionadas 
	 * que coincidem com a unidade passada como par�metro.
	 * @param unidademedida
	 * @return
	 * @author Taidson
	 * @since 06/01/2011
	 */
	public List<Unidademedidaconversao> unidadeAndUnidadeRelacionada(Unidademedida unidademedida){
		return query()
		.select("unidademedidaconversao.cdunidademedidaconversao, unidademedida.cdunidademedida, " +
				"unidademedida.nome")
		.leftOuterJoin("unidademedidaconversao.unidademedida unidademedida")
		.leftOuterJoin("unidademedidaconversao.unidademedidarelacionada unidademedidarelacionada")
		.where("unidademedida.ativo = ?", Boolean.TRUE)
		.openParentheses()
		.where("unidademedida = ?", unidademedida)
		.or()
		.where("unidademedidarelacionada = ?", unidademedida)
		.closeParentheses()
		.list();
	}

	/**
	 * M�todo que busca as unidades relacionadas da unidade de medida passada como par�metro
	 *
	 * @param unidademedida
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Unidademedidaconversao> findUnidademedidarelacionadaByUnidademedida(Unidademedida unidademedida) {
		if(unidademedida == null || unidademedida.getCdunidademedida() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("unidademedidaconversao.cdunidademedidaconversao, unidademedidarelacionada.cdunidademedida, " +
						"unidademedidarelacionada.nome ")
				.join("unidademedidaconversao.unidademedida unidademedida")
				.join("unidademedidaconversao.unidademedidarelacionada unidademedidarelacionada")
				.where("unidademedida = ?", unidademedida)
				.list();
	}
	
	/**
	* M�todo que busca as unidade para validar as convers�es
	*
	* @param unidademedida
	* @param unidademedidarelacionada
	* @return
	* @since 12/04/2016
	* @author Luiz Fernando
	*/
	public List<Unidademedidaconversao> findForValidacaoConversao(Unidademedida unidademedida, Unidademedida unidademedidarelacionada){
		return query()
			.select("unidademedidaconversao.cdunidademedidaconversao, unidademedidaconversao.fracao, unidademedidaconversao.qtdereferencia, " +
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedidarelacionada.cdunidademedida, unidademedidarelacionada.nome ")
			.join("unidademedidaconversao.unidademedida unidademedida")
			.join("unidademedidaconversao.unidademedidarelacionada unidademedidarelacionada")
			.where("unidademedida.ativo = ?", Boolean.TRUE)
			.where("unidademedida = ?", unidademedidarelacionada)
			.where("unidademedidarelacionada = ?", unidademedida)
			.list();
	}

}
