package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Agendainteracao;
import br.com.linkcom.sined.geral.bean.Agendainteracaohistorico;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Agendainteracaosituacao;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.AgendainteracaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AgendainteracaoDAO extends GenericDAO<Agendainteracao>{
	
	private EmpresaService empresaService;
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Agendainteracao> query, FiltroListagem _filtro) {
		AgendainteracaoFiltro filtro = (AgendainteracaoFiltro) _filtro;
		
		query
			.select("agendainteracao.cdagendainteracao, agendainteracao.dtproximainteracao, agendainteracao.agendainteracaorelacionado, " +
					"agendainteracao.tipoagendainteracao, agendainteracao.dtcancelamento, agendainteracao.dtconclusao, " +
					"cliente.cdpessoa, cliente.nome, cliente.ativo, contacrm.cdcontacrm, contacrm.nome, " +
					"contrato.cdcontrato, contrato.descricao, " +
					"material.cdmaterial, material.nome, empresa.nome, " +
					"responsavel.cdpessoa, responsavel.nome")
			.leftOuterJoin("agendainteracao.material material")
			.leftOuterJoin("agendainteracao.contrato contrato")
			.leftOuterJoin("agendainteracao.cliente cliente")
			.leftOuterJoin("agendainteracao.contacrm contacrm")
			.leftOuterJoin("agendainteracao.empresahistorico empresa")
			.leftOuterJoin("agendainteracao.responsavel responsavel")
			.where("cliente = ?", filtro.getCliente())
			.where("cliente.ativo = ?", filtro.getAtivo())
			.where("material = ?", filtro.getMaterial())
			.where("contrato = ?", filtro.getContrato())
			.where("responsavel = ?", filtro.getResponsavel())
			.where("agendainteracao.dtproximainteracao >= ?", filtro.getDtproximainteracaode())
			.where("agendainteracao.dtproximainteracao <= ?", filtro.getDtproximainteracaoate())
			.where("agendainteracao.tipoagendainteracao = ?", filtro.getTipoagendainteracao())
			.where("agendainteracao.contacrm = ?", filtro.getContacrm())
			.orderBy("coalesce(cliente.nome,contacrm.nome)");
		
		String whereInEmpresas = filtro.getEmpresahistorico() != null? filtro.getEmpresahistorico().getCdpessoa().toString():
																	CollectionsUtil.listAndConcatenate(empresaService.findByUsuario(), "cdpessoa", ",");
		query
			.openParentheses()
			.whereIn("empresa.cdpessoa", whereInEmpresas);
		if(filtro.getEmpresahistorico() == null){
			query.or()
				 .where("empresa.cdpessoa is null");
		}
		query.closeParentheses();			
		
		if(filtro.getMaterial() == null && filtro.getAgendainteracaorelacionado() != null && filtro.getAgendainteracaorelacionado().getValue() == 0){
			query.where("material.cdmaterial is not null");
		}else if(filtro.getContrato() == null && filtro.getAgendainteracaorelacionado() != null && filtro.getAgendainteracaorelacionado().getValue() == 1){
			query.where("contrato.cdcontrato is not null");
		}
		
		int itens = 0;
		List<Agendainteracaosituacao> listasituacao = new ArrayList<Agendainteracaosituacao>();
		if(filtro.getListaSituacao() != null){
			for (int i = 0; i < filtro.getListaSituacao().size(); i++){
				itens++;
				if(filtro.getListaSituacao().get(i) instanceof Agendainteracaosituacao){
					listasituacao.add(filtro.getListaSituacao().get(i));
				}else{
					listasituacao.add(Agendainteracaosituacao.valueOf(((Object) filtro.getListaSituacao().get(i)).toString()));
				}
			}
		}

		if(itens > 0 && itens < 5){
			query.openParentheses();
			if(listasituacao.contains(Agendainteracaosituacao.NORMAL)){	
				query.or()	
				.openParentheses()
				.where("(extract (day from dtproximainteracao - now()) + 1 > 5)")
				.where("agendainteracao.dtcancelamento is null")
				.where("agendainteracao.dtconclusao is null")
				.closeParentheses();
			}
			if(listasituacao.contains(Agendainteracaosituacao.ATENCAO)){		
				query.or()	
				.openParentheses()
				.where("(extract (day from dtproximainteracao - now()) + 1 <= 5)")
				.where("(extract (day from dtproximainteracao - now()) >= 0)")
				.where("agendainteracao.dtcancelamento is null")
				.where("agendainteracao.dtconclusao is null")
				.closeParentheses();
				
			}
			if(listasituacao.contains(Agendainteracaosituacao.ATRASADO)){	
				query.or()		
				.openParentheses()
				.where("(extract (day from dtproximainteracao - now()) < 0)")
				.where("agendainteracao.dtcancelamento is null")
				.where("agendainteracao.dtconclusao is null")
				.closeParentheses();
			}
			if(listasituacao.contains(Agendainteracaosituacao.CANCELADO)){	
				query.or()
				.where("agendainteracao.dtcancelamento is not null");
			}
			if(listasituacao.contains(Agendainteracaosituacao.CONCLUIDO)){	
				query.or()
				.openParentheses()
				.where("agendainteracao.dtcancelamento is null")
				.where("agendainteracao.dtconclusao is not null")
				.closeParentheses();
			}
			query.closeParentheses();
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Agendainteracao> query) {
		query
			.leftOuterJoinFetch("agendainteracao.contrato contrato")
			.leftOuterJoinFetch("agendainteracao.atividadetipo atividadetipo")
			.leftOuterJoinFetch("agendainteracao.material material")
			.leftOuterJoinFetch("agendainteracao.responsavel responsavel")
//			.leftOuterJoinFetch("agendainteracao.cliente cliente")
			.leftOuterJoinFetch("agendainteracao.contacrm contacrm")
			;
	}
	
	public List<Agendainteracao> findListagem(String whereIn, String orderBy, boolean asc) {
		if (whereIn == null || whereIn.equals("")) {
			return new ArrayList<Agendainteracao>();
		}
		QueryBuilder<Agendainteracao> query = query()
				.select("agendainteracao.cdagendainteracao, agendainteracao.dtproximainteracao, agendainteracao.agendainteracaorelacionado, " +
						"agendainteracao.tipoagendainteracao, agendainteracao.dtcancelamento, agendainteracao.dtconclusao, " +
						"cliente.cdpessoa, cliente.nome, cliente.ativo, contacrm.cdcontacrm, contacrm.nome, " +
						"contrato.cdcontrato, contrato.descricao, " +
						"material.cdmaterial, material.nome, empresa.nome, contatocontacrm.nome, telefone.telefone, contato.nome, " +
						"telefonecliente.telefone, responsavel.cdpessoa, responsavel.nome")
				.leftOuterJoin("agendainteracao.material material")
				.leftOuterJoin("agendainteracao.contrato contrato")
				.leftOuterJoin("agendainteracao.cliente cliente")
				.leftOuterJoin("agendainteracao.contacrm contacrm")
				.leftOuterJoin("agendainteracao.empresahistorico empresa")
				.leftOuterJoin("contacrm.listcontacrmcontato contatocontacrm")
				.leftOuterJoin("contatocontacrm.listcontacrmcontatofone telefone")
				.leftOuterJoin("cliente.listaContato listaContato")
				.leftOuterJoin("listaContato.contato contato")
				.leftOuterJoin("contato.listaTelefone telefonecliente")
				.leftOuterJoin("agendainteracao.responsavel responsavel")
				.whereIn("agendainteracao.cdagendainteracao", whereIn);

		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy + " " + (asc ? "ASC" : "DESC"));
		} else {
			query.orderBy("coalesce(cliente.nome,contacrm.nome)");
		}

		return query.list();
	}
	
	/**
	 * Grava a data de conclus�o nos itens selecionados.
	 *
	 * @param itensSelecionados
	 * @author Rodrigo Freitas
	 * @since 02/07/2015
	 */
	public void updateDataConclusao(String itensSelecionados) {
		getHibernateTemplate().bulkUpdate("update Agendainteracao a set a.dtconclusao = ? where a.id in (" + itensSelecionados + ")", 
				new Date(System.currentTimeMillis()));
	}
	
	/**
	 * Grava a data de cancelamento nos itens selecionados.
	 * @param itensSelecionados
	 * @author Taidson
	 * @since 28/09/2010
	 */
	public void updateDataCancelamento(String itensSelecionados) {
		getHibernateTemplate().bulkUpdate("update Agendainteracao a set a.dtcancelamento = ? where a.id in (" + itensSelecionados + ")", 
				new Date(System.currentTimeMillis()));
	}
	
	/**
	 * Grava a data da pr�xima intera��o.
	 * @param itensSelecionados
	 * @author Taidson
	 * @since 28/09/2010
	 */
	public void updateDataProxInteracao(Integer cdagendainteracao, Date dtproximainteracao) {
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		getHibernateTemplate().bulkUpdate("update Agendainteracao a set a.dtproximainteracao = ?, cdusuarioaltera = coalesce(?, cdusuarioaltera), dtaltera = ? where a.id = (" + cdagendainteracao + ")", 
				new Object[]{dtproximainteracao, (usuarioLogado != null ? usuarioLogado.getCdpessoa() : null), new Timestamp(System.currentTimeMillis())});
	}
	
	
	/**
	 * Carrega dados para tela de Intera��o de vendas a partir de uma Oportunidade.
	 * @param agendainteracao
	 * @return
	 * @author Taidson
	 * @since 04/10/2010
	 */
	public Agendainteracao loadForInteracao(Agendainteracao agendainteracao) {
		if(agendainteracao == null || agendainteracao.getCdagendainteracao() == null){
			throw new SinedException("Agendainteracao n�o pode ser nula.");
		}
		return query()
		.select("agendainteracao.cdagendainteracao, cliente.cdpessoa, cliente.nome, empresahistorico.cdpessoa, empresahistorico.nome ")
		.join("agendainteracao.cliente cliente")
		.leftOuterJoin("agendainteracao.empresahistorico empresahistorico")
		.where("agendainteracao = ?", agendainteracao)
		.unique();
	}
	
	/**
	 * Carrega dados para a tela de cria��o de nova OS.
	 * @param agendainteracao
	 * @return
	 * @author Rafael Salvio
	 */
	@SuppressWarnings("unchecked")
	public Agendainteracao loadForGerarOS(Agendainteracao agendainteracao){
		if(agendainteracao == null || agendainteracao.getCdagendainteracao() == null){
			throw new SinedException("Agendainteracao n�o pode ser nula.");
		}
		
		StringBuilder sql = new StringBuilder();
		
		sql
			.append("SELECT agenda.cdagendainteracao, cliente.cdpessoa, cliente.nome, responsavel.cdpessoa, responsavel.nome, " +
					"c.cdcontrato, c.descricao, m.cdmaterial, m.nome")
			.append(" FROM agendainteracao agenda ")
			.append(" JOIN pessoa cliente ON cliente.cdpessoa = agenda.cdcliente ")
			.append(" JOIN pessoa responsavel ON responsavel.cdpessoa = agenda.cdresponsavel ")
			.append(" LEFT OUTER JOIN contrato c ON c.cdcontrato = agenda.cdcontrato ")
			.append(" LEFT OUTER JOIN material m ON m.cdmaterial = agenda.cdmaterial ")
			.append(" WHERE agenda.cdagendainteracao = "+ agendainteracao.getCdagendainteracao().toString());
		
		SinedUtil.markAsReader();
		List<Agendainteracao> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Agendainteracao bean = new Agendainteracao(rs.getInt(1));
				
				bean.setCliente(new Cliente(rs.getInt(2)));
				bean.getCliente().setNome(rs.getString(3));
				
				bean.setResponsavel(new Colaborador(rs.getInt(4)));
				bean.getResponsavel().setNome(rs.getString(5));
				
				if(rs.getInt(6) > 0){
					bean.setContrato(new Contrato(rs.getInt(6)));
					bean.getContrato().setDescricao(rs.getString(7));
				}
				if(rs.getInt(8) > 0){
					bean.setMaterial(new Material(rs.getInt(8)));
					bean.getMaterial().setNome(rs.getString(9));
				}
				return bean;
			}
		});
		return lista.get(0);
	}

	/**
	 * Busca os registros de Agendainteracao para exibi��o na portlet.
	 *
	 * @param data
	 * @return
	 * @since 09/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Agendainteracao> findForAgendaPortlet(Date data) {
		if(data == null){
			throw new SinedException("Data n�o pode ser nula.");
		}
		return query()
					.select("agendainteracao.cdagendainteracao, cliente.nome, material.nome, contrato.descricao")
					.join("agendainteracao.cliente cliente")
					.leftOuterJoin("agendainteracao.contrato contrato")
					.leftOuterJoin("agendainteracao.material material")
					.leftOuterJoin("agendainteracao.responsavel responsavel")
					.where("responsavel.cdpessoa = ?", SinedUtil.getUsuarioLogado().getCdpessoa())
					.where("agendainteracao.dtproximainteracao = ?", data)
					.where("agendainteracao.dtcancelamento is null")
					.where("agendainteracao.dtconclusao is null")
					.orderBy("cliente.nome, material.nome, contrato.descricao")
					.list();
	}
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Agendainteracao> findForRelatorio(String whereIn){
		return querySined()
				.select("cliente.cdpessoa, cliente.nome, agendainteracao.agendainteracaorelacionado, material.nome, contrato.descricao," +
						"responsavel.cdpessoa, responsavel.nome, agendainteracao.dtproximainteracao, periodicidade.cdfrequencia, periodicidade.nome," +
						"agendainteracao.dtcancelamento, agendainteracao.dtconclusao, agendainteracao.qtdefrequencia," +
						"listaEndereco.cdendereco, listaEndereco.logradouro, listaEndereco.numero, listaEndereco.complemento, listaEndereco.bairro, municipio.nome, municipio.cdibge, uf.sigla, uf.nome, listaEndereco.cep, enderecotipo.cdenderecotipo, listaEndereco.pontoreferencia")
				.leftOuterJoin("agendainteracao.cliente cliente")
				.leftOuterJoin("agendainteracao.material material")
				.leftOuterJoin("agendainteracao.contrato contrato")
				.leftOuterJoin("agendainteracao.responsavel responsavel")
				.leftOuterJoin("agendainteracao.periodicidade periodicidade")
				.leftOuterJoin("cliente.listaEndereco listaEndereco")
				.leftOuterJoin("listaEndereco.enderecotipo enderecotipo")
				.leftOuterJoin("listaEndereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.whereIn("agendainteracao.cdagendainteracao", whereIn)
				.list();
	}

	/**
	* M�todo que retorna proxima intera��o de cobran�a
	*
	* @param whereInCliente
	* @return
	* @since 23/08/2016
	* @author Luiz Fernando
	*/
	public List<Agendainteracao> findProximaCobranca(Empresa empresa, String whereInCliente) {
		if(StringUtils.isBlank(whereInCliente))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("new br.com.linkcom.sined.geral.bean.Agendainteracao(cliente.cdpessoa, min(agendainteracao.dtproximainteracao))")
				.setUseTranslator(false)
				.join("agendainteracao.cliente cliente")
				.join("agendainteracao.atividadetipo atividadetipo")
				.where("atividadetipo.cobranca = true")
				.where("agendainteracao.dtcancelamento is null")
				.where("agendainteracao.dtconclusao is null")
				.where("agendainteracao.empresahistorico = ?", empresa)
				.whereIn("cliente.cdpessoa", whereInCliente)
				.groupBy("cliente.cdpessoa")
				.list();
	}

	/**
	 * Busca por cliente e empresa
	 *
	 * @param empresa
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/05/2017
	 */
	public List<Agendainteracao> findByEmpresaCliente(Empresa empresa, Cliente cliente) {
		return query()
					.select("agendainteracao.cdagendainteracao, agendainteracao.dtproximainteracao, agendainteracao.descricao")
					.where("agendainteracao.cliente = ?", cliente)
					.where("agendainteracao.empresahistorico = ?", empresa)
					.orderBy("agendainteracao.dtproximainteracao desc")
					.list();
	}

	public List<Agendainteracao> findByAtrasado(Date data, Date dateToSearch) {
		return query()
				.leftOuterJoinFetch("agendainteracao.responsavel responsavel")
				.where("agendainteracao.dtproximainteracao < ?", data)
				.wherePeriodo("agendainteracao.dtproximainteracao", dateToSearch, SinedDateUtils.currentDate())
				.list();
	}
	
	public List<Agendainteracao> findForAndroid(String whereIn, String orderBy, boolean asc) {
		if (whereIn == null || whereIn.equals("")) {
			return new ArrayList<Agendainteracao>();
		}
		QueryBuilder<Agendainteracao> query = query()
				.select("agendainteracao.cdagendainteracao, agendainteracao.dtproximainteracao, " +
						"cliente.cdpessoa, cliente.nome, cliente.ativo, endereco.logradouro, " +
						"endereco.numero, endereco.bairro, endereco.complemento, " +
						"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.nome, uf.sigla, " +
						"telefone.telefone, " +
//						"agendainteracaohistorico.cdagendainteracaohistorico, " +
//						"agendainteracaohistorico.observacao, agendainteracaohistorico.dtaltera, " +
//						"agendainteracaohistorico.cdusuarioaltera, agendainteracaohistorico.interacaoapp, " +
						"empresa.nome, contatocontacrm.nome, telefone.telefone, contato.nome, " +
						"telefonecliente.telefone, responsavel.cdpessoa, responsavel.nome")
				//.leftOuterJoin("agendainteracao.material material")
				//.leftOuterJoin("agendainteracao.contrato contrato")
				.leftOuterJoin("agendainteracao.cliente cliente")
				.leftOuterJoin("agendainteracao.contacrm contacrm")
				.leftOuterJoin("agendainteracao.empresahistorico empresa")
				.leftOuterJoin("contacrm.listcontacrmcontato contatocontacrm")
				.leftOuterJoin("contatocontacrm.listcontacrmcontatofone telefone")
				.leftOuterJoin("cliente.listaContato listaContato")
				.leftOuterJoin("listaContato.contato contato")
				.leftOuterJoin("contato.listaTelefone telefonecliente")
				.leftOuterJoin("agendainteracao.responsavel responsavel")
				.leftOuterJoin("cliente.listaEndereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				//.leftOuterJoin("agendainteracao.listaAgendainteracaohistorico agendainteracaohistorico")
				.whereIn("empresa.cdpessoa", whereIn)
				.orderBy("agendainteracao.dtproximainteracao");
				//.orderBy("agendainteracaohistorico.cdagendainteracaohistorico");

				
//		if (!StringUtils.isEmpty(orderBy)) {
//			query.orderBy(orderBy + " " + (asc ? "ASC" : "DESC"));
//		} else {
//			query.orderBy("coalesce(cliente.nome,contacrm.nome)");
//		}

		return query.list();
	}
	
	
}
