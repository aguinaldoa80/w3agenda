package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Etnia;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("etnia.nome")
public class EtniaDAO extends GenericDAO<Etnia> {

	@Override
	public void updateListagemQuery(QueryBuilder<Etnia> query, FiltroListagem _filtro) {
//		EtniaFiltro filtro = (EtniaFiltro) _filtro;
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Etnia> query) {
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
	}
	

	/* singleton */
	private static EtniaDAO instance;
	public static EtniaDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(EtniaDAO.class);
		}
		return instance;
	}
}