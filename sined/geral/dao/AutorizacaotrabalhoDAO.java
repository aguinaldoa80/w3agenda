package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Autorizacaotrabalho;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AutorizacaotrabalhoDAO extends GenericDAO<Autorizacaotrabalho> {

	ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				arquivoDAO.saveFile(save.getEntity(), "arquivo");			
				return null;
			}		
		});
	}
	
	/**
	 * M�todo que retorna as autoriza��es de trabalho geradas por uma requisi��o
	 * 
	 * @author Marden
	 * @since 29/03/2012
	 */	
	public List<Autorizacaotrabalho> findByRequisicao(Requisicao requisicao){
		return query()
		.select("autorizacaotrabalho.cdautorizacaotrabalho, colaborador.nome, " +
				"autorizacaotrabalho.estimativa, autorizacaotrabalho.peso, autorizacaotrabalho.dtprevisao, " +
				"situacao.descricao, etapa.descricao, etapa.ordem")
		.join("autorizacaotrabalho.colaboradorresponsavel colaborador")
		.join("autorizacaotrabalho.autorizacaoestado situacao")
		.join("autorizacaotrabalho.etapa etapa")
		.where("autorizacaotrabalho.requisicao = ?", requisicao)
		.orderBy("autorizacaotrabalho.cdautorizacaotrabalho")
		.list();			
	}
	
	/**
	 * M�todo retorna o total de horas relacionada a uma determinada requisi��o
	 * 
	 * @author Taidson
	 * @since 24/05/2010
	 */	
	public Double calculaHoras(Requisicao requisicao){
		String sql = "select sum(ap.qtdehoras) from autorizacaotrabalho at " +
				"join apontamento ap on (at.cdautorizacaotrabalho = ap.cdautorizacaotrabalho) " +
				"where at.cdrequisicao = " + requisicao.getCdrequisicao();
		SinedUtil.markAsReader();
		return (Double)getJdbcTemplate().queryForObject(sql, Double.class);
	}
}