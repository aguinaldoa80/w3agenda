package br.com.linkcom.sined.geral.dao;


import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.AtributosMaterial;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.AtributosMaterialFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AtributosMaterialDAO extends GenericDAO<AtributosMaterial> {
	@Override
	public void updateListagemQuery(QueryBuilder<AtributosMaterial> query,
			FiltroListagem _filtro) {
		AtributosMaterialFiltro filtro = (AtributosMaterialFiltro)_filtro;
		query
		.select("atributosMaterial.nome ,  atributosMaterial.cdatributosmaterial ")
			.whereLikeIgnoreAll("atributosMaterial.nome", filtro.getNome());
		super.updateListagemQuery(query, _filtro);
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<AtributosMaterial> query) {
		query
			.select("atributosMaterial.nome, atributosMaterial.cdatributosmaterial, listaAtributosMaterialHistorico.cdatributomaterialhistorico, listaAtributosMaterialHistorico.observacao, listaAtributosMaterialHistorico.dtaltera, " +
				"listaAtributosMaterialHistorico.cdusuarioaltera, listaAtributosMaterialHistorico.acao")
			.leftOuterJoin("atributosMaterial.listaAtributosMaterialHistorico listaAtributosMaterialHistorico")
			.orderBy("listaAtributosMaterialHistorico.dtaltera DESC");
		super.updateEntradaQuery(query);
	}
	
	public Boolean existeAtributo (AtributosMaterial atributoMaterial){
		
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(AtributosMaterial.class)
				.whereEqualIgnoreAll("atributosMaterial.nome",atributoMaterial.getNome())
				.unique() > 0;
	}
}
