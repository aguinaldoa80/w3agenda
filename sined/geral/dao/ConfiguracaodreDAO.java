package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Configuracaodre;
import br.com.linkcom.sined.modulo.contabil.controller.process.bean.GerarDREListagemBean;
import br.com.linkcom.sined.modulo.contabil.controller.process.filter.GerarDREFiltro;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ConfiguracaodreFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("configuracaodre.descricao")
public class ConfiguracaodreDAO extends GenericDAO<Configuracaodre> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Configuracaodre> query, FiltroListagem _filtro) {
		ConfiguracaodreFiltro filtro = (ConfiguracaodreFiltro) _filtro;
		
		query
			.select("configuracaodre.cdconfiguracaodre, configuracaodre.descricao, configuracaodre.ativo")
			.whereLikeIgnoreAll("configuracaodre.descricao", filtro.getDescricao())
			.where("configuracaodre.ativo = ?", filtro.getAtivo());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Configuracaodre> query) {
		query
			.select("configuracaodre.cdconfiguracaodre, configuracaodre.descricao, configuracaodre.ativo, " +
					"configuracaodre.receitaliquida_label, configuracaodre.resultadobruto_label, configuracaodre.resultadoliquidoantesimpostos_label, configuracaodre.resultadoliquido_label, " +
					"listaConfiguracaodreimpostofinal.cdconfiguracaodreimpostofinal, listaConfiguracaodreimpostofinal.descricao, listaConfiguracaodreimpostofinal.percentual, " +
					"listaConfiguracaodreimpostofinal.formula, listaConfiguracaodreimpostofinal.ordem, " +
					"listaConfiguracaodreitem.cdconfiguracaodreitem, " +
					"configuracaodretipo.cdconfiguracaodretipo, configuracaodretipo.descricao, configuracaodretipo.configuracaodreseparacao, configuracaodretipo.configuracaodreoperacao, " +
					"contacontabil.cdcontacontabil, contacontabil.nome, " +
					"vcontacontabil.identificador, vcontacontabil.arvorepai")
			.leftOuterJoin("configuracaodre.listaConfiguracaodreimpostofinal listaConfiguracaodreimpostofinal")
			.leftOuterJoin("configuracaodre.listaConfiguracaodreitem listaConfiguracaodreitem")
			.leftOuterJoin("listaConfiguracaodreitem.configuracaodretipo configuracaodretipo")
			.leftOuterJoin("listaConfiguracaodreitem.contacontabil contacontabil")
			.leftOuterJoin("contacontabil.vcontacontabil vcontacontabil")
			;
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaConfiguracaodreimpostofinal");
		save.saveOrUpdateManaged("listaConfiguracaodreitem");
	}

	/**
	 * Carrega informa��es para a gera��o de DRE.
	 *
	 * @param configuracaodre
	 * @return
	 * @since 13/12/2019
	 * @author Rodrigo Freitas
	 */
	public Configuracaodre loadForDRE(Configuracaodre configuracaodre) {
		QueryBuilder<Configuracaodre> query = querySined();
		this.updateEntradaQuery(query);
		return query.entity(configuracaodre).unique();
	}

	/**
	 * Busca informa��es para a listagem de contas da DRE
	 *
	 * @param filtro
	 * @return
	 * @since 17/12/2019
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<GerarDREListagemBean> gerarDREListagem(GerarDREFiltro filtro) {
//		String sql = "select cdcontacontabil, identificador, nome, sum(valor) as valor, tem_filhos " +
//						"from ( " +
//						"select  " +
//						"cc.cdcontacontabil, " +
//						"vcc.identificador,  " +
//						"cc.nome, " +
//						"coalesce(( " +
//						"select sum( " +
//							"case " +
//								"when mci.cdmovimentacaocontabilcredito is not null then (mci.valor/100.0) " +
//								"when mci.cdmovimentacaocontabildebito is not null then ((mci.valor/100.0) * -1.0) " +
//							"end " +
//						") as valor " +
//						"from movimentacaocontabildebitocredito mci " +
//						"join contacontabil cc2 on cc2.cdcontacontabil = mci.cdcontacontabil " +
//						"join vcontacontabil vcc2 on vcc2.cdcontacontabil = cc.cdcontacontabil " +
//						"join movimentacaocontabil mc on (mc.cdmovimentacaocontabil = mci.cdmovimentacaocontabilcredito or mc.cdmovimentacaocontabil = mci.cdmovimentacaocontabildebito) " + 
//						"where 1 = 1 " +
//						"and mc.dtlancamento >= ? " +
//						"and mc.dtlancamento <= ? " +
//						"and mc.cdempresa = ? " +
//						"and mci.cdcontacontabil = cc.cdcontacontabil " +
//						"), 0) as valor, " +
//						"exists(select 1 from contacontabil cc2 where cc2.cdcontacontabilpai = cc.cdcontacontabil) as tem_filhos " +
//						"from contacontabil cc " +
//						"join vcontacontabil vcc on vcc.cdcontacontabil = cc.cdcontacontabil " +
//						"where exists ( " +
//							"select 1 " +
//							"from configuracaodreitem ci " +
//							"join contacontabil cc2 on cc2.cdcontacontabil = ci.cdcontacontabil " +
//							"join vcontacontabil vcc2 on vcc2.cdcontacontabil = cc2.cdcontacontabil " +
//							"where vcc.identificador like (vcc2.identificador || '%') " +
//							"and ci.cdconfiguracaodretipo = ? " +
//							"and ci.cdconfiguracaodre = ? " +
//						") " +
//						"union all " +
//						"select  " +
//						"cc.cdcontacontabil, " +
//						"vcc.identificador,  " +
//						"cc.nome, " +
//						"0 as valor, " +
//						"exists(select 1 from contacontabil cc2 where cc2.cdcontacontabilpai = cc.cdcontacontabil) as tem_filhos " +
//						"from contacontabil cc " +
//						"join vcontacontabil vcc on vcc.cdcontacontabil = cc.cdcontacontabil " +
//						") query " +
//						"group by cdcontacontabil, identificador, nome, tem_filhos " +
//						"order by identificador";
//		return (List<GerarDREListagemBean>) getJdbcTemplate().query(sql, new Object[]{
//			filtro.getDtinicio(),
//			filtro.getDtfim(),
//			filtro.getEmpresa().getCdpessoa(),
//			filtro.getConfiguracaodretipo().getCdconfiguracaodretipo(),
//			filtro.getConfiguracaodre().getCdconfiguracaodre()
//		}, new RowMapper() {
//			@Override
//			public GerarDREListagemBean mapRow(ResultSet rs, int rowNum) throws SQLException {
//				return new GerarDREListagemBean(
//						rs.getInt("cdcontacontabil"), 
//						rs.getString("identificador"), 
//						rs.getString("nome"), 
//						new Money(rs.getDouble("valor")),
//						rs.getBoolean("tem_filhos"));
//			}
//		});
		
		
		String sql = "select  " +
						"cc.cdcontacontabil, " +
						"vcc.identificador,  " +
						"cc.nome, " +
						"calcular_saldo_conta_analitica(cc.cdcontacontabil, ?, ?, ?, ?) / 100.0 as valor, " +
						"exists(select 1 from contacontabil cc2 where cc2.cdcontacontabilpai = cc.cdcontacontabil) as tem_filhos " +
						"from contacontabil cc " +
						"join vcontacontabil vcc on vcc.cdcontacontabil = cc.cdcontacontabil " +
						"where exists ( " +
							"select 1 " +
							"from configuracaodreitem ci " +
							"join contacontabil cc2 on cc2.cdcontacontabil = ci.cdcontacontabil " +
							"join vcontacontabil vcc2 on vcc2.cdcontacontabil = cc2.cdcontacontabil " +
							"where vcc.identificador like (vcc2.identificador || '%') " +
							"and ci.cdconfiguracaodretipo = ? " +
							"and ci.cdconfiguracaodre = ? " +
						") " +
						"order by vcc.identificador";
		SinedUtil.markAsReader();
		return (List<GerarDREListagemBean>) getJdbcTemplate().query(sql, new Object[]{
			filtro.getDtfim(),
			filtro.getEmpresa().getCdpessoa(),
			filtro.getCentrocusto() != null ? filtro.getCentrocusto().getCdcentrocusto() : null,
			filtro.getProjeto() != null ? filtro.getProjeto().getCdprojeto() : null,					
			filtro.getConfiguracaodretipo().getCdconfiguracaodretipo(),
			filtro.getConfiguracaodre().getCdconfiguracaodre()
		}, new RowMapper() {
			@Override
			public GerarDREListagemBean mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new GerarDREListagemBean(
						rs.getInt("cdcontacontabil"), 
						rs.getString("identificador"), 
						rs.getString("nome"), 
						new Money(rs.getDouble("valor")),
						rs.getBoolean("tem_filhos"));
			}
		});
	}
	
}
