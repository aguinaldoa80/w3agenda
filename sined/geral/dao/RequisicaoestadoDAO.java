package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Requisicaoestado;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RequisicaoestadoDAO extends GenericDAO<Requisicaoestado>{
	
	@Override
	public List<Requisicaoestado> findAll() {
		return query()
				.orderBy("descricao")
				.list();
	}
	
	public Requisicaoestado findByCodigo(int cdcodigo) {
		return query()
		.where("requisicaoestado.cdrequisicaoestado = ? ",cdcodigo)
		.unique();
	}
	
	/*singleton*/
	
	public static RequisicaoestadoDAO instance;
	
	public static RequisicaoestadoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(RequisicaoestadoDAO.class);
		}
		return instance;
	}

}
