package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoreferenciada;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotafiscalprodutoreferenciadaDAO extends GenericDAO<Notafiscalprodutoreferenciada> {

	public List<Notafiscalprodutoreferenciada> findByNotafiscalproduto(Notafiscalproduto nota) {
		if(nota == null || nota.getCdNota() == null){
			throw new SinedException("Nota n�o pode ser nula.");
		}
		return query()
					.select("notafiscalprodutoreferenciada.cdnotafiscalprodutoreferenciada, notafiscalprodutoreferenciada.tiponotareferencia, " +
							"notafiscalprodutoreferenciada.chaveacesso, uf.cduf, uf.cdibge, notafiscalprodutoreferenciada.mesanoemissao, " +
							"notafiscalprodutoreferenciada.tipopessoa, notafiscalprodutoreferenciada.cnpj, notafiscalprodutoreferenciada.cpf, " +
							"notafiscalprodutoreferenciada.ie, notafiscalprodutoreferenciada.modeloNFe, notafiscalprodutoreferenciada.serie, " +
							"notafiscalprodutoreferenciada.numero")
					.leftOuterJoin("notafiscalprodutoreferenciada.uf uf")
					.where("notafiscalprodutoreferenciada.notafiscalproduto = ?", nota)
					.list();
	}

	
}
