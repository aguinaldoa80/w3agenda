package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Estadocivil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("estadocivil.nome")
public class EstadocivilDAO extends GenericDAO<Estadocivil> {
	
	/* singleton */
	private static EstadocivilDAO instance;
	public static EstadocivilDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(EstadocivilDAO.class);
		}
		return instance;
	}
		
}
