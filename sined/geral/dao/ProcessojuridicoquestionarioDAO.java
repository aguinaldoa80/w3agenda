package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Processojuridicoquestionario;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProcessojuridicoquestionarioDAO extends GenericDAO<Processojuridicoquestionario> {
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Processojuridicoquestionario> findForFichaProcessoJuridico(String whereIn) {
		return query()
				.select("processojuridico.cdprocessojuridico, processojuridicoquestionario.ordem," +
						"processojuridicoquestionario.pergunta, processojuridicoquestionario.resposta")
				.join("processojuridicoquestionario.processojuridico processojuridico")
				.whereIn("processojuridico.cdprocessojuridico", whereIn)
				.list();
	}	
}