package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Acaopapel;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Usuariopapel;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AcaopapelDAO extends GenericDAO<Acaopapel> {
	
	/**
	 * Pega todos os a��o papel a partir de um papel especificado.
	 * @param papel
	 * @return
	 * @author Pedro Gon�alves
	 */
	public List<Acaopapel> findAllBy(Papel papel){
		if(papel == null || papel.getCdpapel() == null)
			throw new SinedException("Par�metros incorretos.");
		
		return query()
				.select("acaopapel.cdacaopapel, acao.cdacao,acao.key, acao.descricao, acaopapel.permitido, acao.modulo")
				.join("acaopapel.acao acao")
				.join("acaopapel.papel papel")
				.where("papel=?",papel)
				.list();
	}
	
	/**
	 * Pega todos os a��o papel a partir de um papel especificado.
	 * @param papel
	 * @return
	 * @author Pedro Gon�alves
	 */
	public List<Acaopapel> findAllPermissionsByPapel(List<Usuariopapel> lista){
		if(lista == null)
			throw new SinedException("Par�metros incorretos.");
		return query()
				.select("acaopapel.cdacaopapel, acao.cdacao, acao.descricao,acao.key, acaopapel.permitido")
				.join("acaopapel.acao acao")
				.join("acaopapel.papel papel")
				.whereIn("papel.cdpapel",CollectionsUtil.listAndConcatenate(lista, "papel.cdpapel",","))
				.list();
	}
	
	
}
