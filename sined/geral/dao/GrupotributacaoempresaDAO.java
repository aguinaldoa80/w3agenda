package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Grupotributacaoempresa;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class GrupotributacaoempresaDAO extends GenericDAO<Grupotributacaoempresa>{

	public List<Grupotributacaoempresa> findByGrupotributacao(Grupotributacao grupotributacao){
		return query()
				.where("grupotributacaoempresa.grupotributacao = ?", grupotributacao)
				.list();
	}
}
