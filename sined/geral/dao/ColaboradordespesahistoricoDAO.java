package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Colaboradordespesa;
import br.com.linkcom.sined.geral.bean.Colaboradordespesahistorico;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColaboradordespesahistoricoDAO extends GenericDAO<Colaboradordespesahistorico>{
		

		public List<Colaboradordespesahistorico> finByColaboradordespesahistorico(Colaboradordespesa colaboradordespesa) {
			if(colaboradordespesa == null || colaboradordespesa.getCdcolaboradordespesa() == null){
				throw new SinedException("Colaboradordespesa n�o pode ser nulo");
			}		
			return query()
					.select("colaboradordespesa.cdcolaboradordespesa, colaboradordespesahistorico.cdcolaboradordespesahistorico, " +
							"colaboradordespesahistorico.acao, colaboradordespesahistorico.dtaltera, " +
							"colaboradordespesahistorico.observacao, colaboradordespesahistorico.cdusuarioaltera ")
					.join("colaboradordespesahistorico.colaboradordespesa colaboradordespesa")
					.where("colaboradordespesa = ?", colaboradordespesa)
					.list();
		}
}
