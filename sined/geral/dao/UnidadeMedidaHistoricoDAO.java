package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.UnidadeMedidaHistorico;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class UnidadeMedidaHistoricoDAO extends GenericDAO<UnidadeMedidaHistorico> {
	
	public List<UnidadeMedidaHistorico> findByUnidadeMedidaHistorico(Unidademedida unidademedida) {
		return query()
					.select("unidadeMedidaHistorico.cdUnidadeMedidaHistorico, unidadeMedidaHistorico.unidadeMedidaConversaoHistorico, unidadeMedidaHistorico.cdusuarioaltera, " +
							"unidadeMedidaHistorico.cdusuarioaltera, unidadeMedidaHistorico.dtaltera, unidadeMedidaHistorico.observacao, unidadeMedidaHistorico.acao, unidadeMedidaHistorico.nome," +
							"unidadeMedidaHistorico.simbolo, unidadeMedidaHistorico.ativo, unidadeMedidaHistorico.casasdecimaisestoque, unidadeMedidaHistorico.etiquetaunica")
					.where("unidadeMedidaHistorico.unidadeMedida = ?", unidademedida)
					.orderBy("unidadeMedidaHistorico.dtaltera desc")
					.list();
	}

}
