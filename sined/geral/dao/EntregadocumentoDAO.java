package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoLancamentoEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EntregadocumentoDAO extends GenericDAO<Entregadocumento> {

	/**
	 * 
	 * M�todo que busca a EntregaDocumento com a Empresa a partir de uma Entrega.
	 *
	 * @name findListEntregaDocumentoByEntrega
	 * @param entrega
	 * @return
	 * @return List<Entregadocumento>
	 * @author Thiago Augusto
	 * @date 03/08/2012
	 *
	 */
	public List<Entregadocumento> findListEntregaDocumentoByEntrega(Entrega entrega){
		return query()
				.select("entregadocumento.cdentregadocumento, entregadocumento.valor, empresa.cdpessoa")
				.leftOuterJoin("entregadocumento.empresa empresa")
				.leftOuterJoin("entregadocumento.entrega entrega")
				.where("entrega = ? ", entrega)
				.list();
	}
	
	/**
	 * 
	 * M�todo que busca natureza da opera��o.
	 *
	 * @name findNatop
	 * @param cdentrega
	 * @return String
	 * @author Cleiton Amorim
	 * @date 16/12/2013
	 *
	 */		
	public String findNatop(Integer cdentrega){
		String natop = null;
		if (cdentrega != null){
		  natop = query()
			  .select("entregadocumento.natop")
			  .join("entregadocumento.entrega entrega")
			  .where("entrega.cdentrega = ?", cdentrega)		  
			  .unique().getNatop();			
		}
	    return natop;
	}
	
	/**
	 * 
	 * M�todo que busca as EntregaDocumento a partir de uma Entrega para valida��o dos Romaneio da Entrega.
	 *
	 * @name findListForValidaRomaneioEntrega
	 * @param entrega
	 * @return List<Entregadocumento>
	 * @author Lucas Costa
	 * @date 05/11/2014
	 *
	 */
	public List<Entregadocumento> findListForValidaRomaneioEntrega(Entrega entrega){
		if(entrega == null || entrega.getCdentrega()== null){
			throw new SinedException("Par�metro inv�lido.");	
		}
		
		return query()
				.select("entregadocumento.cdentregadocumento, listaEntregamaterial.cdentregamaterial, listaEntregamaterial.qtde, material.cdmaterial")
				.join("entregadocumento.entrega entrega")
				.join("entregadocumento.listaEntregamaterial listaEntregamaterial")
				.join("listaEntregamaterial.material material")
				.where("entrega = ? ", entrega)
				.list();
	}
	
	public List<Entregadocumento> findForGerarLancamentoContabil(GerarLancamentoContabilFiltro filtro, String whereInNaturezaOperacao){
		return query()
				.select("entregadocumento.cdentregadocumento, entregadocumento.dtemissao, entregadocumento.dtentrada, " +
						"entregadocumento.numero, entregadocumento.natop, entregadocumento.indpag," +
						"entregadocumento.valoroutrasdespesas, entregadocumento.valordesconto, entregadocumento.valorfrete," +
						"entregadocumento.valoricmsst, entregadocumento.valoripi, entregadocumento.valorfretepeso," +
						"entregadocumento.valortaxacoleta, entregadocumento.valortaxaentrega, entregadocumento.valorseccat," +
						"entregadocumento.valorpedagio, entregadocumento.emitente, entregadocumento.vincularOutrosDocumentos," +
						"entregadocumento.infoadicionalfisco, entregadocumento.infoadicionalcontrib, entregadocumento.transportadora," +
						"entregadocumento.placaveiculo, entregadocumento.valorseguro, entregadocumento.valor, entregadocumento.chaveacesso," +
						"entregadocumento.entregadocumentosituacao, entregadocumento.tipotitulo, entregadocumento.numerotitulo," +
						"entregadocumento.descricaotitulo, entregadocumento.simplesremessa, entregadocumento.retornowms," +
						"empresa.geracaospedpiscofins, empresa.geracaospedicmsipi, empresa.presencacompradornfe," +
						"rateio.cdrateio, listaRateioitem.cdrateioitem, listaRateioitem.percentual, listaRateioitem.valor," +
						"centrocusto.cdcentrocusto, centrocusto.nome," +
						"listaEntregamaterial.cdentregamaterial, listaEntregamaterial.qtde, listaEntregamaterial.comprimento," +
						"listaEntregamaterial.valorunitario, listaEntregamaterial.cdusuarioaltera," +
						"listaEntregamaterial.dtaltera,listaEntregamaterial.cprod,listaEntregamaterial.xprod," +
						"listaEntregamaterial.valorproduto,listaEntregamaterial.valordesconto," +
						"listaEntregamaterial.valorfrete,listaEntregamaterial.valorseguro," +
						"listaEntregamaterial.valoroutrasdespesas,listaEntregamaterial.csticms," +
						"listaEntregamaterial.valorbcicms,listaEntregamaterial.icms,listaEntregamaterial.valoricms," +
						"listaEntregamaterial.valorbcicmsst,listaEntregamaterial.icmsst,listaEntregamaterial.valoricmsst," +
						"listaEntregamaterial.valormva,listaEntregamaterial.naoconsideraricmsst,listaEntregamaterial.cstipi," +
						"listaEntregamaterial.valorbcipi,listaEntregamaterial.ipi,listaEntregamaterial.valoripi," +
						"listaEntregamaterial.cstpis,listaEntregamaterial.valorbcpis,listaEntregamaterial.pis," +
						"listaEntregamaterial.pisretido,listaEntregamaterial.qtdebcpis,listaEntregamaterial.valorunidbcpis," +
						"listaEntregamaterial.valorpis,listaEntregamaterial.valorpisretido,listaEntregamaterial.cstcofins," +
						"listaEntregamaterial.valorbccofins,listaEntregamaterial.cofins,listaEntregamaterial.cofinsretido," +
						"listaEntregamaterial.qtdebccofins,listaEntregamaterial.valorunidbccofins,listaEntregamaterial.valorcofins," +
						"listaEntregamaterial.valorcofinsretido,listaEntregamaterial.valorcsll,listaEntregamaterial.valorir," +
						"listaEntregamaterial.valorinss,listaEntregamaterial.valorimpostoimportacao,listaEntregamaterial.valorbciss," +
						"listaEntregamaterial.iss,listaEntregamaterial.valoriss,listaEntregamaterial.tipotributacaoiss," +
						"listaEntregamaterial.clistservico,listaEntregamaterial.naturezabcc,listaEntregamaterial.valorFiscalIcms," +
						"listaEntregamaterial.valorFiscalIpi, listaEntregamaterial.faturamentocliente, "+
						"listaEntregamaterial.valorbcii, listaEntregamaterial.valoriiItem, "+
						"listaEntregamaterial.valorDespesasAduaneiras, listaEntregamaterial.iof, listaEntregamaterial.tipolote," +
						"material.cdmaterial, material.nome, material.identificacao, material.nomenf," +
						"cfop.cdcfop, cfop.codigo, cfop.descricaoresumida, cfop.descricaocompleta, cfop.naoconsiderarreceita," +
						"municipioiss.cdmunicipio, municipioiss.nome, uf.cduf, uf.nome," +
						"grupotributacao.cdgrupotributacao, grupotributacao.nome, grupotributacao.formulabcst, grupotributacao.formulavalorst," +
						"uficmsst.cduf, uficmsst.sigla," +
						"fornecedor.nome, fornecedor.razaosocial," +
						"contaContabilFornecedor.cdcontacontabil, contaContabilFornecedor.nome," +
						"operacaocontabilpagamento.cdoperacaocontabil, operacaocontabilpagamento.nome, operacaocontabilpagamento.tipo," +
						"tipoLancamento.cdOperacaoContabilTipoLancamento, tipoLancamento.nome, tipoLancamento.movimentacaocontabilTipoLancamento, " +
						"operacaocontabilpagamento.historico, operacaocontabilpagamento.codigointegracao, operacaocontabilpagamento.agruparvalormesmaconta," +
						"listaDebito.cdoperacaocontabildebitocredito, listaDebito.tipo, listaDebito.controle, listaDebito.centrocusto, listaDebito.projeto, listaDebito.historico, contacontabildebito.cdcontacontabil, contacontabildebito.nome, listaDebito.formula, listaDebito.codigointegracao, " +
						"listaCredito.cdoperacaocontabildebitocredito, listaCredito.tipo, listaCredito.controle, listaCredito.centrocusto, listaCredito.projeto, listaCredito.historico, contacontabilcredito.cdcontacontabil, contacontabilcredito.nome, listaCredito.formula, listaCredito.codigointegracao, " +
						"centrocustovenda.cdcentrocusto, centrocustovenda.nome, " +
						"contaContabilMaterial.cdcontacontabil, contaContabilMaterial.nome," +
						"projeto.cdprojeto, projeto.nome")
				.leftOuterJoin("entregadocumento.empresa empresa")		
				.leftOuterJoin("entregadocumento.fornecedor fornecedor")
				.leftOuterJoin("fornecedor.contaContabil contaContabilFornecedor")
				.leftOuterJoin("entregadocumento.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
				.leftOuterJoin("listaRateioitem.projeto projeto")
				.leftOuterJoin("entregadocumento.listaEntregamaterial listaEntregamaterial")
				.leftOuterJoin("listaEntregamaterial.material material")
				.leftOuterJoin("material.contaContabil contaContabilMaterial")
				.leftOuterJoin("listaEntregamaterial.cfop cfop")
				.leftOuterJoin("listaEntregamaterial.municipioiss municipioiss")
				.leftOuterJoin("municipioiss.uf uf")
				.leftOuterJoin("listaEntregamaterial.grupotributacao grupotributacao")
				.leftOuterJoin("grupotributacao.uficmsst uficmsst")
				.leftOuterJoin("material.centrocustovenda centrocustovenda")
				.leftOuterJoin("entregadocumento.naturezaoperacao naturezaoperacao")
				.leftOuterJoin("naturezaoperacao.operacaocontabilpagamento operacaocontabilpagamento")
				.leftOuterJoin("operacaocontabilpagamento.listaDebito listaDebito")
				.leftOuterJoin("listaDebito.contaContabil contacontabildebito")
				.leftOuterJoin("operacaocontabilpagamento.listaCredito listaCredito")
				.leftOuterJoin("listaCredito.contaContabil contacontabilcredito")
				.leftOuterJoin("operacaocontabilpagamento.tipoLancamento tipoLancamento")
				.where("empresa=?", filtro.getEmpresa())
				.whereIn("naturezaoperacao.cdnaturezaoperacao", whereInNaturezaOperacao)
				.where("coalesce(entregadocumento.dtentrada, entregadocumento.dtemissao)>=?", filtro.getDtPeriodoInicio())
				.where("coalesce(entregadocumento.dtentrada, entregadocumento.dtemissao)<=?", filtro.getDtPeriodoFim())
				.where("listaRateioitem.projeto=?", filtro.getProjeto())
				.where("not exists(select 1 from EntregaDocumentoApropriacao eda where eda.entregaDocumento = entregadocumento)")
				.where("entregadocumento.entregadocumentosituacao not in (?)", Entregadocumentosituacao.CANCELADA)
				.where("not exists (select 1 from Movimentacaocontabilorigem mco where mco.entregadocumento=entregadocumento and mco.movimentacaocontabil.tipoLancamento=?)", new Object[]{MovimentacaocontabilTipoLancamentoEnum.REGISTROS_ENTRADA_FISCAL})
				.list();
	}
	
	public List<Entregadocumento> findForGerarLancamentoContabilWithApropriacao(GerarLancamentoContabilFiltro filtro, String whereInNaturezaOperacao){
		return query()
				.select("entregadocumento.cdentregadocumento, entregadocumento.dtemissao, entregadocumento.dtentrada, " +
						"entregadocumento.numero, entregadocumento.natop, entregadocumento.indpag," +
						"entregadocumento.valoroutrasdespesas, entregadocumento.valordesconto, entregadocumento.valorfrete," +
						"entregadocumento.valoricmsst, entregadocumento.valoripi, entregadocumento.valorfretepeso," +
						"entregadocumento.valortaxacoleta, entregadocumento.valortaxaentrega, entregadocumento.valorseccat," +
						"entregadocumento.valorpedagio, entregadocumento.emitente, entregadocumento.vincularOutrosDocumentos," +
						"entregadocumento.infoadicionalfisco, entregadocumento.infoadicionalcontrib, entregadocumento.transportadora," +
						"entregadocumento.placaveiculo, entregadocumento.valorseguro, entregadocumento.valor, entregadocumento.chaveacesso," +
						"entregadocumento.entregadocumentosituacao, entregadocumento.tipotitulo, entregadocumento.numerotitulo," +
						"entregadocumento.descricaotitulo, entregadocumento.simplesremessa, entregadocumento.retornowms," +
						"empresa.geracaospedpiscofins, empresa.geracaospedicmsipi, empresa.presencacompradornfe," +
						"rateio.cdrateio, listaRateioitem.cdrateioitem, listaRateioitem.percentual, listaRateioitem.valor," +
						"centrocusto.cdcentrocusto, centrocusto.nome," +
						"listaEntregamaterial.cdentregamaterial, listaEntregamaterial.qtde, listaEntregamaterial.comprimento," +
						"listaEntregamaterial.valorunitario, listaEntregamaterial.cdusuarioaltera," +
						"listaEntregamaterial.dtaltera,listaEntregamaterial.cprod,listaEntregamaterial.xprod," +
						"listaEntregamaterial.valorproduto,listaEntregamaterial.valordesconto," +
						"listaEntregamaterial.valorfrete,listaEntregamaterial.valorseguro," +
						"listaEntregamaterial.valoroutrasdespesas,listaEntregamaterial.csticms," +
						"listaEntregamaterial.valorbcicms,listaEntregamaterial.icms,listaEntregamaterial.valoricms," +
						"listaEntregamaterial.valorbcicmsst,listaEntregamaterial.icmsst,listaEntregamaterial.valoricmsst," +
						"listaEntregamaterial.valormva,listaEntregamaterial.naoconsideraricmsst,listaEntregamaterial.cstipi," +
						"listaEntregamaterial.valorbcipi,listaEntregamaterial.ipi,listaEntregamaterial.valoripi," +
						"listaEntregamaterial.cstpis,listaEntregamaterial.valorbcpis,listaEntregamaterial.pis," +
						"listaEntregamaterial.pisretido,listaEntregamaterial.qtdebcpis,listaEntregamaterial.valorunidbcpis," +
						"listaEntregamaterial.valorpis,listaEntregamaterial.valorpisretido,listaEntregamaterial.cstcofins," +
						"listaEntregamaterial.valorbccofins,listaEntregamaterial.cofins,listaEntregamaterial.cofinsretido," +
						"listaEntregamaterial.qtdebccofins,listaEntregamaterial.valorunidbccofins,listaEntregamaterial.valorcofins," +
						"listaEntregamaterial.valorcofinsretido,listaEntregamaterial.valorcsll,listaEntregamaterial.valorir," +
						"listaEntregamaterial.valorinss,listaEntregamaterial.valorimpostoimportacao,listaEntregamaterial.valorbciss," +
						"listaEntregamaterial.iss,listaEntregamaterial.valoriss,listaEntregamaterial.tipotributacaoiss," +
						"listaEntregamaterial.clistservico,listaEntregamaterial.naturezabcc,listaEntregamaterial.valorFiscalIcms," +
						"listaEntregamaterial.valorFiscalIpi, listaEntregamaterial.faturamentocliente, "+
						"listaEntregamaterial.valorbcii, listaEntregamaterial.valoriiItem, "+
						"listaEntregamaterial.valorDespesasAduaneiras, listaEntregamaterial.iof, listaEntregamaterial.tipolote," +
						"material.cdmaterial, material.nome, material.identificacao, material.nomenf," +
						"cfop.cdcfop, cfop.codigo, cfop.descricaoresumida, cfop.descricaocompleta, cfop.naoconsiderarreceita," +
						"municipioiss.cdmunicipio, municipioiss.nome, uf.cduf, uf.nome," +
						"grupotributacao.cdgrupotributacao, grupotributacao.nome, grupotributacao.formulabcst, grupotributacao.formulavalorst," +
						"uficmsst.cduf, uficmsst.sigla," +
						"fornecedor.nome, fornecedor.razaosocial," +
						"contaContabilFornecedor.cdcontacontabil, contaContabilFornecedor.nome," +
						"operacaocontabilpagamento.cdoperacaocontabil, operacaocontabilpagamento.nome, operacaocontabilpagamento.tipo," +
						"tipoLancamento.cdOperacaoContabilTipoLancamento, tipoLancamento.nome, tipoLancamento.movimentacaocontabilTipoLancamento, " +
						"operacaocontabilpagamento.historico, operacaocontabilpagamento.codigointegracao, operacaocontabilpagamento.agruparvalormesmaconta," +
						"listaDebito.cdoperacaocontabildebitocredito, listaDebito.tipo, listaDebito.controle, listaDebito.centrocusto, listaDebito.projeto, listaDebito.historico, contacontabildebito.cdcontacontabil, contacontabildebito.nome, listaDebito.formula, listaDebito.codigointegracao, " +
						"listaCredito.cdoperacaocontabildebitocredito, listaCredito.tipo, listaCredito.controle, listaCredito.centrocusto, listaCredito.projeto, listaCredito.historico, contacontabilcredito.cdcontacontabil, contacontabilcredito.nome, listaCredito.formula, listaCredito.codigointegracao, " +
						"centrocustovenda.cdcentrocusto, centrocustovenda.nome, " +
						"contaContabilMaterial.cdcontacontabil, contaContabilMaterial.nome," +
						"projeto.cdprojeto, projeto.nome")
				.leftOuterJoin("entregadocumento.empresa empresa")		
				.leftOuterJoin("entregadocumento.fornecedor fornecedor")
				.leftOuterJoin("fornecedor.contaContabil contaContabilFornecedor")
				.leftOuterJoin("entregadocumento.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
				.leftOuterJoin("listaRateioitem.projeto projeto")
				.leftOuterJoin("entregadocumento.listaEntregamaterial listaEntregamaterial")
				.leftOuterJoin("listaEntregamaterial.material material")
				.leftOuterJoin("material.contaContabil contaContabilMaterial")
				.leftOuterJoin("listaEntregamaterial.cfop cfop")
				.leftOuterJoin("listaEntregamaterial.municipioiss municipioiss")
				.leftOuterJoin("municipioiss.uf uf")
				.leftOuterJoin("listaEntregamaterial.grupotributacao grupotributacao")
				.leftOuterJoin("grupotributacao.uficmsst uficmsst")
				.leftOuterJoin("material.centrocustovenda centrocustovenda")
				.leftOuterJoin("entregadocumento.naturezaoperacao naturezaoperacao")
				.leftOuterJoin("naturezaoperacao.operacaocontabilpagamento operacaocontabilpagamento")
				.leftOuterJoin("operacaocontabilpagamento.listaDebito listaDebito")
				.leftOuterJoin("listaDebito.contaContabil contacontabildebito")
				.leftOuterJoin("operacaocontabilpagamento.listaCredito listaCredito")
				.leftOuterJoin("listaCredito.contaContabil contacontabilcredito")
				.leftOuterJoin("operacaocontabilpagamento.tipoLancamento tipoLancamento")
				.where("empresa=?", filtro.getEmpresa())
				.whereIn("naturezaoperacao.cdnaturezaoperacao", whereInNaturezaOperacao)
				.where("coalesce(entregadocumento.dtentrada, entregadocumento.dtemissao)>=?", filtro.getDtPeriodoInicio())
				.where("coalesce(entregadocumento.dtentrada, entregadocumento.dtemissao)<=?", filtro.getDtPeriodoFim())
				.where("listaRateioitem.projeto=?", filtro.getProjeto())
				.where("exists(select 1 from EntregaDocumentoApropriacao eda where eda.entregaDocumento = entregadocumento)")
				.where("entregadocumento.entregadocumentosituacao not in (?)", Entregadocumentosituacao.CANCELADA)
				.where("not exists (select 1 from Movimentacaocontabilorigem mco where mco.entregadocumento=entregadocumento and mco.movimentacaocontabil.tipoLancamento=?)", new Object[]{MovimentacaocontabilTipoLancamentoEnum.REGISTROS_ENTRADA_FISCAL_COM_APROPRIACAO})
				.list();
	}
		
	public List<Entregadocumento>findForAutoCompleteProcessoReferenciado(String numero){
		return query()
				.select("entregadocumento.cdentregadocumento,entregadocumento.numero")
				.whereLikeIgnoreAll("entregadocumento.numero =", numero)
				.list();
	}
}