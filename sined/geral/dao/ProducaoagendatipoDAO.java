package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Producaoagendatipo;
import br.com.linkcom.sined.modulo.producao.controller.crud.filter.ProducaoagendatipoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoagendatipoDAO extends GenericDAO<Producaoagendatipo>{

	@Override
	public void updateListagemQuery(QueryBuilder<Producaoagendatipo> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		
		ProducaoagendatipoFiltro filtro = (ProducaoagendatipoFiltro)_filtro;
		
		query.whereLikeIgnoreAll("producaoagendatipo.nome", filtro.getNome());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Producaoagendatipo> query) {
		query
			.select("producaoagendatipo.cdproducaoagendatipo, producaoagendatipo.nome, producaoetapa.cdproducaoetapa")
			.leftOuterJoin("producaoagendatipo.producaoetapa producaoetapa");
	}

	public Producaoagendatipo loadWithProducaoetapa(Producaoagendatipo producaoagendatipo) {
		if(producaoagendatipo == null || producaoagendatipo.getCdproducaoagendatipo() == null)
			throw new SinedException("Tipo de Produ��o n�o pode ser nulo.");
		
		return query()
			.select("producaoagendatipo.cdproducaoagendatipo, producaoagendatipo.nome, producaoetapa.cdproducaoetapa")
			.leftOuterJoin("producaoagendatipo.producaoetapa producaoetapa")
			.where("producaoagendatipo = ?", producaoagendatipo)
			.unique();
	}

	@Override
	public ListagemResult<Producaoagendatipo> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Producaoagendatipo> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("producaoagendatipo.cdproducaoagendatipo");
		
		return new ListagemResult<Producaoagendatipo>(query, filtro);
	}
}
