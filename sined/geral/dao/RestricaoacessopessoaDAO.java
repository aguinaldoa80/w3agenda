package br.com.linkcom.sined.geral.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Restricaoacessopessoa;
import br.com.linkcom.sined.geral.bean.enumeration.DiaSemana;
import br.com.linkcom.sined.geral.bean.enumeration.EnumTipoRestricaoAcessoPessoa;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RestricaoacessopessoaDAO extends GenericDAO<Restricaoacessopessoa>{
	
	/**
	 * 
	 * @param pessoa
	 * @author Thiago Clemente
	 * 
	 */
	public List<Restricaoacessopessoa> findByPessoa(Pessoa pessoa){
		return querySined()
				
				.select("restricaoacessopessoa.cdrestricaoacessopessoa, restricaoacessopessoa.tipo, restricaoacessopessoa.hrinicio, restricaoacessopessoa.hrfim," +
						"pessoa.cdpessoa, restricaoacessopessoa.diasemana")
				.join("restricaoacessopessoa.pessoa pessoa")
				.where("pessoa=?", pessoa)
				.orderBy("restricaoacessopessoa.hrinicio, restricaoacessopessoa.hrfim")
				.list();
	}
	
	/**
	 * 
	 * @param pessoa
	 * @author Thiago Clemente
	 * 
	 */
	public boolean isPossuiRestricaoAcessoHorario(Pessoa pessoa){
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.MONTH, 0);
		calendar.set(Calendar.YEAR, 1970);
		
		Hora hora = new Hora(calendar.getTimeInMillis());
		
		SinedUtil.markAsReader();
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.join("restricaoacessopessoa.pessoa pessoa")
				.where("restricaoacessopessoa.tipo=?", EnumTipoRestricaoAcessoPessoa.HORARIO)
				.where("pessoa=?", pessoa)
				.openParentheses()
					.where("restricaoacessopessoa.hrinicio is null")
					.or()
					.where("restricaoacessopessoa.hrinicio<=?", hora)
				.closeParentheses()
				.openParentheses()
					.where("restricaoacessopessoa.hrfim is null")
					.or()
					.where("restricaoacessopessoa.hrfim>=?", hora)
				.closeParentheses()
				.setUseTranslator(false)
				.unique()>0;
	}	
	
	/**
	 * 
	 * @param pessoa
	 * @author Filipe Santos
	 * 
	 */
	public boolean isPossuiRestricaoAcessoDia(Pessoa pessoa){
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date(System.currentTimeMillis()));
		
		Integer day_of_week = calendar.get(Calendar.DAY_OF_WEEK);
		DiaSemana diaSemana = DiaSemana.values()[day_of_week-1];
		
		SinedUtil.markAsReader();
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.join("restricaoacessopessoa.pessoa pessoa")
				.where("restricaoacessopessoa.tipo=?", EnumTipoRestricaoAcessoPessoa.DIA)
				.where("restricaoacessopessoa.diasemana=?", diaSemana)
				.where("pessoa=?", pessoa)
				.unique()>0;
	}
	
}