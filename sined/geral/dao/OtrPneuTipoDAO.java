package br.com.linkcom.sined.geral.dao;




import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.OtrPneuTipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.OtrPneuTipoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OtrPneuTipoDAO extends GenericDAO<OtrPneuTipo>  {

	@Override
	public void updateListagemQuery(QueryBuilder<OtrPneuTipo> query, FiltroListagem _filtro) {	
		OtrPneuTipoFiltro filtro = (OtrPneuTipoFiltro) _filtro;
		
		query
			.select("otrPneuTipo.cdOtrPneuTipo, otrPneuTipo.nome, otrPneuTipo.codigoIntegracao,construcao.cdOtrConstrucao,otrpneutipoclassificacaocodigo.cdOtrPneuTipoOtrClassificacaoCodigo, " +
					"otrpneutipoclassificacaocodigo.otrClassificacaoCodigo, otrclassificacaocodigo.cdOtrClassificacaoCodigo , otrclassificacaocodigo.codigo")
			.leftOuterJoin("otrPneuTipo.otrConstrucao construcao")
			.leftOuterJoin("otrPneuTipo.listaOtrPneuTipoOtrClassificacaoCodigo otrpneutipoclassificacaocodigo")
			.leftOuterJoin("otrpneutipoclassificacaocodigo.otrClassificacaoCodigo otrclassificacaocodigo")
			.whereLikeIgnoreAll("otrPneuTipo.nome", filtro.getNome())
			.whereLikeIgnoreAll("otrclassificacaocodigo.codigo", filtro.getCodigo());
			
	}
	
	
	@Override
	public void updateEntradaQuery(QueryBuilder<OtrPneuTipo> query) {
		query
		.select("otrPneuTipo.cdOtrPneuTipo, otrPneuTipo.nome, otrPneuTipo.codigoIntegracao," +
				"construcao.cdOtrConstrucao, construcao.construcao," +
				"listaOtrPneuTipoOtrClassificacaoCodigo.cdOtrPneuTipoOtrClassificacaoCodigo, " +
				"listaOtrPneuTipoOtrClassificacaoCodigo.otrClassificacaoCodigo," +
				"otrclassificacaocodigo.cdOtrClassificacaoCodigo, otrclassificacaocodigo.codigo, " +
				"otrdesenho.cdOtrDesenho, otrservicotipo.cdOtrServicoTipo")
		.leftOuterJoin("otrPneuTipo.otrConstrucao construcao")
		.leftOuterJoin("otrPneuTipo.listaOtrPneuTipoOtrClassificacaoCodigo listaOtrPneuTipoOtrClassificacaoCodigo")
		.leftOuterJoin("listaOtrPneuTipoOtrClassificacaoCodigo.otrClassificacaoCodigo otrclassificacaocodigo")
		.leftOuterJoin("otrclassificacaocodigo.otrServicoTipo otrservicotipo")
		.leftOuterJoin("otrclassificacaocodigo.otrDesenho otrdesenho");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaOtrPneuTipoOtrClassificacaoCodigo");
	}
	
	public List<OtrPneuTipo> findAutocomplete(String nome){
		return query()
				.select("otrPneuTipo.cdOtrPneuTipo, otrPneuTipo.nome")
				.whereLikeIgnoreAll("otrPneuTipo.nome", nome)
				.orderBy("otrPneuTipo.nome")
				.list();
	}
	
	
}
