package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Colaboradorcategoriacnh;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColaboradorcategoriacnhDAO extends GenericDAO<Colaboradorcategoriacnh> {

	@Override
	public void updateListagemQuery(QueryBuilder<Colaboradorcategoriacnh> query, FiltroListagem _filtro) {
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Colaboradorcategoriacnh> query) {
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
	}
	

	/* singleton */
	private static ColaboradorcategoriacnhDAO instance;
	public static ColaboradorcategoriacnhDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradorcategoriacnhDAO.class);
		}
		return instance;
	}
}