package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MaterialcategoriaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.suprimento.MaterialCategoriaTreeFiltro;

@DefaultOrderBy("materialcategoria.descricao")
public class MaterialcategoriaDAO extends GenericDAO<Materialcategoria>{

	@Override
	public void updateEntradaQuery(QueryBuilder<Materialcategoria> query) {
		query.joinFetch("materialcategoria.vmaterialcategoria vmaterialcategoria")
			.leftOuterJoinFetch("materialcategoria.materialcategoriapai materialcategoriapai")
			.leftOuterJoinFetch("materialcategoriapai.vmaterialcategoria vmaterialcategoriapai");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Materialcategoria> query, FiltroListagem _filtro) {
		MaterialcategoriaFiltro filtro = (MaterialcategoriaFiltro) _filtro;
		
		query
		.select("materialcategoria.cdmaterialcategoria, materialcategoria.descricao, materialcategoria.ativo, " +
				"vmaterialcategoria.identificador, vmaterialcategoria.cdmaterialcategoria")
		.join("materialcategoria.vmaterialcategoria vmaterialcategoria")
			.where("vmaterialcategoria.identificador like ?||'%'", filtro.getIdentificador())
			.whereLikeIgnoreAll("materialcategoria.descricao", filtro.getDescricao())
			.where("materialcategoria.ativo = ?", filtro.getAtivo())
			.orderBy("vmaterialcategoria.identificador");
	}
	
	public List<Materialcategoria> findRaiz(){
		return query()
				.select("materialcategoria.item, materialcategoria.formato")
				.where("materialcategoriapai is null")
				.orderBy("materialcategoria.item desc")
				.list();
	}
	
	public List<Materialcategoria> findFilhos(Materialcategoria pai){
		if (pai == null || pai.getCdmaterialcategoria() == null) {
			throw new SinedException("A categoria superior n�o pode ser nula.");
		}
		return query()
				.select("materialcategoria.item")
				.leftOuterJoin("materialcategoria.materialcategoriapai pai")
				.where("pai = ?",pai)
				.orderBy("materialcategoria.item desc")
				.list();
	}
	
	public List<Materialcategoria> findAutocomplete(String q, boolean somenteAtivos) {
		return query()
					.select("materialcategoria.cdmaterialcategoria, materialcategoria.descricao, " +
							"vmaterialcategoria.identificador, vmaterialcategoria.arvorepai")
					.join("materialcategoria.vmaterialcategoria vmaterialcategoria")
					
					.openParentheses()
					.whereLikeIgnoreAll("materialcategoria.descricao", q)
					.or()
					.whereLikeIgnoreAll("vmaterialcategoria.identificador", q)
					.closeParentheses()
					
					.where("materialcategoria.ativo = ?", Boolean.TRUE, somenteAtivos)
					.orderBy("vmaterialcategoria.identificador")
					.list();
	}	
	
	public List<Materialcategoria> findAutocompleteTreeView(MaterialCategoriaTreeFiltro filtro) {
		return query()
				.select("materialcategoria.cdmaterialcategoria, materialcategoria.descricao, vmaterialcategoria.identificador, vmaterialcategoria.arvorepai")
				.join("materialcategoria.vmaterialcategoria vmaterialcategoria")
				.openParentheses()
					.whereLikeIgnoreAll("materialcategoria.descricao", filtro.getQ())
					.or()
					.whereLikeIgnoreAll("vmaterialcategoria.identificador", filtro.getQ())
				.closeParentheses()
				.where("coalesce(materialcategoria.ativo, false)=?", filtro.getAtivo())
				.where("not exists (select 1 from Materialcategoria mc where mc.materialcategoriapai=materialcategoria and mc.ativo=true)", Boolean.TRUE.equals(filtro.getUltimoNivel()))
				.orderBy("vmaterialcategoria.identificador")
				.list();
	}	
	
	public List<Materialcategoria> findForTreeView(MaterialCategoriaTreeFiltro filtro) {
		return query()
				.select("materialcategoria.cdmaterialcategoria, materialcategoria.descricao, vmaterialcategoria.identificador, vmaterialcategoria.arvorepai," +
						"materialcategoriapai.cdmaterialcategoria")
				.join("materialcategoria.vmaterialcategoria vmaterialcategoria")
				.leftOuterJoin("materialcategoria.materialcategoriapai materialcategoriapai")
				.where("coalesce(materialcategoria.ativo, false)=?", filtro.getAtivo())
				.orderBy("vmaterialcategoria.identificador")
				.list();
	}
	
	/**
	 * M�todo criado para facilitar o filtro de Categoria do material. Pois em alguns filtros, a query estava ficando muito lenta
	 * 
	 * @param identificador
	 * @author Thiago Clemente
	 * 
	 */
	public List<Materialcategoria> findForFitlro(String identificador) {
		if (identificador==null || identificador.trim().equals("")){
			return new ArrayList<Materialcategoria>();
		}
		return query()
				.select("materialcategoria.cdmaterialcategoria")
				.join("materialcategoria.vmaterialcategoria vmaterialcategoria")
				.openParentheses()
					.where("vmaterialcategoria.identificador like ? ||'.%'", identificador)
					.or()
					.where("vmaterialcategoria.identificador like ? ", identificador)
				.closeParentheses()
				.list();
	}
}