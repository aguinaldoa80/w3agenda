package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Indicecorrecao;
import br.com.linkcom.sined.geral.bean.Indicecorrecaovalor;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class IndicecorrecaovalorDAO extends GenericDAO<Indicecorrecaovalor> {

	/**
	 * M�todo que retorna a lista de �ndice corre��o valor de um �ndice de corre��o
	 * 
	 * @param indicecorrecao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Indicecorrecaovalor> findByIndiceCorrecao(Indicecorrecao indicecorrecao) {
		if(indicecorrecao == null || indicecorrecao.getCdindicecorrecao() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("indicecorrecaovalor.cdindicecorrecaovalor, indicecorrecaovalor.mesano, indicecorrecaovalor.percentual")
			.join("indicecorrecaovalor.indicecorrecao indicecorrecao")
			.where("indicecorrecao = ?", indicecorrecao)
			.orderBy("indicecorrecaovalor.mesano")
			.list();
	}

	
		
}
