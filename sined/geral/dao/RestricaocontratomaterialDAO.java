package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Restricao;
import br.com.linkcom.sined.geral.bean.Restricaocontratomaterial;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RestricaocontratomaterialDAO extends GenericDAO<Restricaocontratomaterial>{
	
	public Restricaocontratomaterial verificaDesbloqueio(Contratomaterial contratomaterial){
		return query()
			.select("restricaocontratomaterial.cdrestricaocontratomaterial, restricao.cdrestricao," +
					"restricaocontratomaterial.dtbloqueio, restricaocontratomaterial.dtdesbloqueio," +
					"restricao.cdsituacaorestricao, contratomaterial.cdcontratomaterial")
			.join("restricaocontratomaterial.contratomaterial contratomaterial")
			.join("restricaocontratomaterial.restricao restricao")
			.where("contratomaterial = ?", contratomaterial)
			.where("dtbloqueio is not null and dtdesbloqueio is null")
			.unique();
	}
	
	
	public Restricaocontratomaterial loadServicoBloqueadoDesbloqueado(Contratomaterial contratomaterial, Restricao restricao){
		return query()
			.select("restricaocontratomaterial.cdrestricaocontratomaterial, restricao.cdrestricao," +
					"restricaocontratomaterial.dtbloqueio, restricaocontratomaterial.dtdesbloqueio," +
					"restricao.cdsituacaorestricao, contratomaterial.cdcontratomaterial")
			.join("restricaocontratomaterial.restricao restricao")
			.join("restricaocontratomaterial.contratomaterial contratomaterial")
			.join("contratomaterial.servico servico")
			.where("contratomaterial = ?", contratomaterial)
			.where("restricao = ?", restricao)
			.unique();
	}
	

	public Restricaocontratomaterial loadServicoBloqueadoDesbloqueado(Integer cdcontratomaterial, Integer cdrestricaocontratomaterial){
		QueryBuilder<Restricaocontratomaterial> query = query()
		.select("restricaocontratomaterial.cdrestricaocontratomaterial, restricao.cdrestricao," +
				"restricaocontratomaterial.dtbloqueio, restricaocontratomaterial.dtdesbloqueio," +
				"restricao.cdsituacaorestricao, contratomaterial.cdcontratomaterial, " +
				"contrato.cdcontrato, cliente.cdpessoa")
		.join("restricaocontratomaterial.restricao restricao")
		.join("restricaocontratomaterial.contratomaterial contratomaterial")
		.join("contratomaterial.servico servico")
		.join("contratomaterial.contrato contrato")
		.join("contrato.cliente cliente")
		.where("contratomaterial.id = ?", cdcontratomaterial)
		.where("restricaocontratomaterial.id = ?", cdrestricaocontratomaterial);
		
		return query.unique();
	}
	
	public List<Restricaocontratomaterial> restricoesContratoMaterialDesbloquear(Restricao restricao){
		return query()
			.joinFetch("restricaocontratomaterial.restricao restricao")
			.where("restricao = ?", restricao)
			.list();
	}
	
}
