package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.sined.geral.bean.ControleOrcamentoHistorico;
import br.com.linkcom.sined.geral.bean.Controleorcamento;


public class ControleOrcamentoHistoricoDAO extends GenericDAO<ControleOrcamentoHistorico> {
	
	public List<ControleOrcamentoHistorico> findByControleOrcamento(Controleorcamento controleorcamento) {
		return query()		
			.select("controleOrcamentoHistorico.dataaltera, controleOrcamentoHistorico.acaoexecutada, controleOrcamentoHistorico.responsavel, controleOrcamentoHistorico.observacao")
			.leftOuterJoin("controleOrcamentoHistorico.controleorcamento controleorcamento")
			.where("controleorcamento = ?", controleorcamento)
			.list();
	}
	
}
