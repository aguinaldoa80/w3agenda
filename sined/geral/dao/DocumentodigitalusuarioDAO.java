package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Documentodigitalusuario;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.DocumentodigitalusuarioFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("documentodigitalusuario.email")
public class DocumentodigitalusuarioDAO extends GenericDAO<Documentodigitalusuario> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Documentodigitalusuario> query, FiltroListagem _filtro) {
		DocumentodigitalusuarioFiltro filtro = (DocumentodigitalusuarioFiltro) _filtro;
		query
			.select("documentodigitalusuario.cddocumentodigitalusuario, documentodigitalusuario.email, documentodigitalusuario.nome")
			.whereLikeIgnoreAll("documentodigitalusuario.email", filtro.getEmail())
			.whereLikeIgnoreAll("documentodigitalusuario.nome", filtro.getNome());
	}

	/**
	 * Busca o usu�rio de documento digital a partir do e-mail
	 *
	 * @param email
	 * @return
	 * @since 08/03/2019
	 * @author Rodrigo Freitas
	 */
	public Documentodigitalusuario loadByEmail(String email) {
		return query()
					.select("documentodigitalusuario.cddocumentodigitalusuario, documentodigitalusuario.email, documentodigitalusuario.nome, " +
							"documentodigitalusuario.cpf, documentodigitalusuario.dtnascimento")
					.where("documentodigitalusuario.email = ?", email)
					.unique();
	}
	
}

