package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Registroatividadehora;
import br.com.linkcom.sined.geral.bean.Requisicao;

public class RegistroatividadehoraDAO extends GenericDAO<Registroatividadehora> {

	/**
	* M�todo que busca o registro de atividade do usuario
	*
	* @param colaborador
	* @param requisicao
	* @return
	* @since 21/10/2015
	* @author Luiz Fernando
	*/
	public Registroatividadehora findByUsuarioRequisicao(Colaborador colaborador, Requisicao requisicao) {
		return query()
					.select("registroatividadehora.cdregistroatividadehora, registroatividadehora.inicio, " +
							"colaborador.cdpessoa, requisicao.cdrequisicao ")
					.join("registroatividadehora.colaborador colaborador")
					.join("registroatividadehora.requisicao requisicao")
					.where("colaborador = ?", colaborador)
					.where("requisicao = ?", requisicao, requisicao != null && requisicao.getCdrequisicao() != null)
					.unique();
	}
}
