package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Campanha;
import br.com.linkcom.sined.geral.bean.Campanhasituacao;
import br.com.linkcom.sined.geral.bean.Campanhatipo;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.CampanhaFiltro;
import br.com.linkcom.sined.modulo.crm.controller.process.vo.CampanhaCSVBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("campanha.nome")
public class CampanhaDAO extends GenericDAO<Campanha>{
	
	private ArquivoDAO arquivoDAO;	
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	
	@Override
	public void updateListagemQuery(QueryBuilder<Campanha> query, FiltroListagem _filtro) {
		
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		
		CampanhaFiltro filtro = (CampanhaFiltro)_filtro;
		
		query
			.select("campanha.cdcampanha, campanha.nome, responsavel.nome, projeto.cdprojeto, projeto.nome, " +
					"campanhatipo.cdcampanhatipo, campanhatipo.nome, campanhasituacao.cdcampanhasituacao, " +
					"campanhasituacao.nome, campanha.dtcriacao, campanha.dtprevisaofechamento ")
					.leftOuterJoin("campanha.responsavel responsavel")
					.leftOuterJoin("campanha.campanhasituacao campanhasituacao")
					.leftOuterJoin("campanha.campanhatipo campanhatipo")
					.leftOuterJoin("campanha.projeto projeto")
				.whereLikeIgnoreAll("campanha.nome", filtro.getNome())
				.where("responsavel = ?", filtro.getResponsavel())
				.where("campanhatipo = ?", filtro.getCampanhatipo())
				.where("projeto = ?", filtro.getProjeto())
				.where("dtcriacao >= ?", filtro.getDtcriacaoinicio())
				.where("dtcriacao <= ?", filtro.getDtcriacaofim())
				;
		
			List<Campanhasituacao> listaCampanhasituacao = filtro.getListaCampanhasituacao();
			if (listaCampanhasituacao != null)
				query.whereIn("campanhasituacao.cdcampanhasituacao", CollectionsUtil.listAndConcatenate(listaCampanhasituacao,"cdcampanhasituacao", ","));
		
		super.updateListagemQuery(query, _filtro);
	}
	
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				Campanha campanha = (Campanha) save.getEntity();
				if(campanha.getArquivo() != null){
					arquivoDAO.saveFile(save.getEntity(), "arquivo");
				}
				save.saveOrUpdateManaged("listCampanhacontato");
				save.saveOrUpdateManaged("listCampanhalead");
				return null;
			}
		});
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Campanha> query) {
		query
		.select("campanha.cdcampanha, campanha.nome, campanha.dtcriacao, campanha.dtprevisaofechamento, " +
				"responsavel.cdpessoa, responsavel.nome, " +
				"projeto.cdprojeto, projeto.nome, " +
				"listCampanhahistorico.cdcampanhahistorico, listCampanhahistorico.observacao, listCampanhahistorico.dtaltera, " +
				"listCampanhahistorico.cdusuarioaltera, " +
				"campanhasituacao.cdcampanhasituacao, campanhasituacao.nome, campanhatipo.cdcampanhatipo, campanhatipo.nome, " +
				"arquivo.cdarquivo, arquivo.nome ")					
			.leftOuterJoin("campanha.responsavel responsavel")
			.leftOuterJoin("campanha.projeto projeto")
			.leftOuterJoin("campanha.campanhasituacao campanhasituacao")
			.leftOuterJoin("campanha.campanhatipo campanhatipo")
			.leftOuterJoin("campanha.arquivo arquivo")
			.leftOuterJoin("campanha.listCampanhahistorico listCampanhahistorico")
			.orderBy("listCampanhahistorico.dtaltera DESC");
			
	}
	
	public Campanha carregaCampanha(String whereIn) {
		return query()
				.select("campanha.cdcampanha, campanha.nome ")
					.whereIn("campanha.cdcampanha", whereIn)
				.unique();
	}
	
	public List<Campanha> findByContaForCombo(Oportunidade oportunidade){
		if (oportunidade == null || oportunidade.getCdoportunidade() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("campanha.cdcampanha, campanha.nome")
					.where("campanha.oportunidade = ?",oportunidade)
					.orderBy("campanha.nome")
					.list();
	}
	public List<Campanha> findByContacrmForCombo(Contacrm contacrm) {
		return query()
					.select("campanha.cdcampanha, campanha.nome")
					.join("campanha.listCampanhacontato listCampanhacontato")
					.join("listCampanhacontato.contacrmcontato contacrmcontato")
					.join("contacrmcontato.contacrm contacrm")
					.where("contacrm = ?", contacrm)
					.orderBy("campanha.nome")
					.list();
	}
	@SuppressWarnings("unchecked")
	public List<CampanhaCSVBean> findForCSV(String whereIn) {
		String sql = "select * from ( " +
					"select ccc.nome as nome, mail.email as email " +
					"from contacrmcontato ccc " + 
					"JOIN campanhacontato cc on ccc.cdcontacrmcontato = cc.cdcontacrmcontato " +
					"join contacrmcontatoemail mail on ccc.cdcontacrmcontato = mail.cdcontacrmcontato " +
					"where cc.cdcampanha in (" + whereIn + ") " +
					"union ALL " +
					"select lead.nome as nome, lm.email as email " +
					"from lead " +
					"join campanhalead cl on lead.cdlead = cl.cdlead " +
					"join leademail lm on lead.cdlead = lm.cdlead " +
					"where cl.cdcampanha in (" + whereIn + ") " +
					" ) query " +
					"order by nome";
		SinedUtil.markAsReader();
		List<CampanhaCSVBean> listaUpdate = getJdbcTemplate().query(sql, 
			new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new CampanhaCSVBean(rs.getString("nome"), rs.getString("email"));
				}
	
			});
		return listaUpdate;
	}

	public List<Campanha> findByCampanhatipo(Campanhatipo campanhatipo) {
		return query()
					.select("campanha.cdcampanha, campanha.nome")
					.where("campanha.campanhatipo = ?", campanhatipo)
					.orderBy("campanha.nome")
					.list();
	}


	public Campanha findByName(String campanha) {
		return query()
				.select("campanha.cdcampanha, campanha.nome")
				.where("trim(upper(campanha.nome)) = ?", campanha.toUpperCase().trim())
				.unique();
	}
	
	@Override
	public ListagemResult<Campanha> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Campanha> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("campanha.cdcampanha");
		
		return new ListagemResult<Campanha>(query, filtro);
	}
}
