package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Garantiareforma;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Valecompra;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ValecompraDAO extends GenericDAO<Valecompra> {
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Valecompra valecompra = (Valecompra)save.getEntity();
		if(valecompra.getListaValecompraorigem() != null && 
				valecompra.getListaValecompraorigem().size() > 0){
			save.saveOrUpdateManaged("listaValecompraorigem");
		}
	}
	
	public List<Valecompra> findForAndroid(String whereIn) {
		return querySined()
				.select("valecompra.cdvalecompra, valecompra.valor, tipooperacao.cdtipooperacao, tipooperacao.nome, " +
						"valecompra.identificacao, valecompra.data, documento.cddocumento, venda.cdvenda, " +
						"cliente.cdpessoa ")
				.join("valecompra.tipooperacao tipooperacao")
				.join("valecompra.cliente cliente")
				.leftOuterJoin("valecompra.listaValecompraorigem valecompraorigem")
				.leftOuterJoin("valecompraorigem.documento documento")
				.leftOuterJoin("valecompraorigem.venda venda")
				.orderBy("valecompra.data, valecompra.cdvalecompra")
				.whereIn("valecompra.cdvalecompra", whereIn)
				.list();
	}

	/**
	 * Busca os vales compras do cliente
	 *
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/09/2014
	 */
	public List<Valecompra> findByCliente(Cliente cliente) {
		return querySined()
					.select("valecompra.cdvalecompra, valecompra.valor, tipooperacao.cdtipooperacao, tipooperacao.nome, " +
							"valecompra.identificacao, valecompra.data, documento.cddocumento, venda.cdvenda,materialDevolucao.cdmaterialdevolucao, pedidovenda.cdpedidovenda," +
							"coleta.cdcoleta, garantiareforma.cdgarantiareforma ")
					.join("valecompra.tipooperacao tipooperacao")
					.leftOuterJoin("valecompra.listaValecompraorigem valecompraorigem")
					.leftOuterJoin("valecompraorigem.documento documento")
					.leftOuterJoin("valecompraorigem.venda venda")
					.leftOuterJoin("valecompraorigem.pedidovenda pedidovenda")
					.leftOuterJoin("valecompraorigem.coleta coleta")
					.leftOuterJoin("valecompraorigem.materialDevolucao materialDevolucao")
					.leftOuterJoin("valecompraorigem.garantiareforma garantiareforma")
					.where("valecompra.cliente = ?", cliente)
					.orderBy("valecompra.data, valecompra.cdvalecompra")
					.list();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param valecompra
	 * @param valor
	 * @author Rafael Salvio
	 */
	public void updateSaldo(Valecompra valecompra, Money valor){
		if(valecompra == null || valecompra.getCdvalecompra() == null){
			throw new SinedException("Par�metro valecompra n�o pode ser nulo");
		}
		if(valor == null){
			throw new SinedException("Par�metro valor n�o pode ser nulo");
		}
		String sql = "update Valecompra set valecompra.valor = (valecompra.valor - ?) where valecompra.cdvalecompra = ?";
		getHibernateTemplate().bulkUpdate(sql, new Object[]{valor, valecompra.getCdvalecompra()});
	}
	
	/**
	 * Busca os vales compras de cr�dito do cliente
	 *
	 * @param cliente
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Valecompra> findCreditoByCliente(Cliente cliente) {
		return querySined()
					.select("valecompra.cdvalecompra, valecompra.valor, valecompra.identificacao")
					.where("valecompra.cliente = ?", cliente)
					.where("valecompra.tipooperacao = ?", Tipooperacao.TIPO_CREDITO)
					.orderBy("valecompra.identificacao")
					.list();
	}

	/**
	* M�todo que busca valecompra gerado no pedido de venda
	*
	* @param pedidovenda
	* @return
	* @since 28/04/2015
	* @author Luiz Fernando
	*/
	public List<Valecompra> findByPedidovenda(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				.select("valecompra.cdvalecompra, valecompra.valor ")
				.join("valecompra.listaValecompraorigem listaValecompraorigem")
				.join("listaValecompraorigem.pedidovenda pedidovenda")
				.where("pedidovenda = ?", pedidovenda)
				.orderBy("valecompra.cdvalecompra desc")
				.list();
	}
	
	public List<Valecompra> findByPedidovenda(Pedidovenda pedidovenda, Tipooperacao tipooperacao) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				.select("valecompra.cdvalecompra, valecompra.valor ")
				.join("valecompra.listaValecompraorigem listaValecompraorigem")
				.join("listaValecompraorigem.pedidovenda pedidovenda")
				.where("pedidovenda = ?", pedidovenda)
				.where("valecompra.tipooperacao = ?", tipooperacao)
				.orderBy("valecompra.cdvalecompra desc")
				.list();
	}
	
	/**
	* M�todo que faz o update no valor do vale compra
	*
	* @param valecompra
	* @param valor
	* @since 28/04/2015
	* @author Luiz Fernando
	*/
	public void updateSaldoByValecompra(Valecompra valecompra, Money valor){
		if(valecompra == null || valecompra.getCdvalecompra() == null){
			throw new SinedException("Par�metro valecompra n�o pode ser nulo");
		}
		if(valor == null){
			throw new SinedException("Par�metro valor n�o pode ser nulo");
		}
		
		String sql = "update Valecompra set valor = ? where cdvalecompra = ?";
		getHibernateTemplate().bulkUpdate(sql, new Object[]{valor, valecompra.getCdvalecompra()});
	}
	
	/**
	* M�todo que retorna o vale compra vinculado a venda
	*
	* @param venda
	* @param tipooperacao
	* @return
	* @since 15/01/2016
	* @author Luiz Fernando
	*/
	public List<Valecompra> findByVenda(Venda venda, Tipooperacao tipooperacao) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				.setUseReadOnly(false)
				.select("valecompra.cdvalecompra, valecompra.valor ")
				.join("valecompra.listaValecompraorigem listaValecompraorigem")
				.join("listaValecompraorigem.venda venda")
				.where("venda = ?", venda)
				.where("valecompra.tipooperacao = ?", tipooperacao)
				.orderBy("valecompra.cdvalecompra desc")
				.list();
	}

	/**
	* M�todo que busa o saldo de vale compra dos clientes
	*
	* @param whereIn
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public List<GenericBean> findSaldoValecompra(String whereIn) {
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		String sql = " select vc.cdcliente as cdcliente, sum(CASE WHEN vc.cdtipooperacao = 1 THEN -COALESCE(vc.valor, 0) ELSE COALESCE(vc.valor, 0) END) as saldovalecompra " +
					 " from valecompra vc " +
					 " where vc.cdcliente in (" + whereIn + ") " +
					 " group by vc.cdcliente ";
		SinedUtil.markAsReader();
		List<GenericBean> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new GenericBean(rs.getInt("cdcliente"), rs.getDouble("saldovalecompra")/100d);
			}
		});
		
		return list;
	}
	
	/**
	* M�todo que retorna o vale compra vinculado � garantia de reforma
	*
	* @param garantiareforma
	* @param tipooperacao
	* @return
	* @since 20/10/2017
	* @author Mairon Cezar
	*/
	public List<Valecompra> findByGarantiareforma(Garantiareforma garantiareforma, Tipooperacao tipooperacao) {
		if(garantiareforma == null || garantiareforma.getCdgarantiareforma() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				.select("valecompra.cdvalecompra, valecompra.valor, tipooperacao.cdtipooperacao, tipooperacao.nome ")
				.join("valecompra.listaValecompraorigem listaValecompraorigem")
				.join("listaValecompraorigem.garantiareforma garantiareforma")
				.join("valecompra.tipooperacao tipooperacao")
				.where("listaValecompraorigem.garantiareforma = ?", garantiareforma)
				.where("valecompra.tipooperacao = ?", tipooperacao)
				.orderBy("valecompra.cdvalecompra desc")
				.list();
	}
}