package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Leademail;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class LeademailDAO extends GenericDAO<Leademail>{
	
	public List<Leademail> findByLead(Lead lead) {
		if (lead == null || lead.getCdlead() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return 
			query()
				.select("leademail.email, lead.cdlead, leademail.contato")
				.join("leademail.lead lead")
				.where("lead = ?",lead)
				.orderBy("lead.cdlead")
				.list();
	}

}
