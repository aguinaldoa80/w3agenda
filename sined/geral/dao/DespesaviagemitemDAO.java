package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Despesaviagemitem;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DespesaviagemitemDAO extends GenericDAO<Despesaviagemitem> {

	/**
	 * Update em todos os itens de uma despesa de viagem setando o valorreazlizado nulo.
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void updateZerarValoresRealizadas(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de parâmetros.");
		}
		getJdbcTemplate().execute("UPDATE DESPESAVIAGEMITEM SET VALORREALIZADO = NULL WHERE CDDESPESAVIAGEM IN ("+whereIn+")");		
		getJdbcTemplate().execute("UPDATE DESPESAVIAGEM SET DTRETORNOREALIZADO = NULL WHERE CDDESPESAVIAGEM IN ("+whereIn+")");		
	}

	/**
	 * Busca os itens da despesa de viagem para a realização.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/11/2012
	 */
	public List<Despesaviagemitem> findForSaveRealizacao(String whereIn) {
		return query()
					.select("despesaviagemitem.cddespesaviagemitem, despesaviagemitem.valorrealizado, despesaviagemitem.valorprevisto, " +
							"despesaviagemtipo.cddespesaviagemtipo, despesaviagemtipo.nome")
					.join("despesaviagemitem.despesaviagemtipo despesaviagemtipo")
					.whereIn("despesaviagemitem.cddespesaviagemitem", whereIn)
					.list();
	}

	/**
	 * Atualiza o valor realizado do item da despesa de viagem.
	 *
	 * @param despesaviagemitem
	 * @param valorrealizado
	 * @author Rodrigo Freitas
	 * @since 03/12/2012
	 */
	public void updateValorRealizado(Despesaviagemitem despesaviagemitem, Money valorrealizado) {
		if(despesaviagemitem == null || despesaviagemitem.getCddespesaviagemitem() == null){
			throw new SinedException("Erro na passagem de parâmetros.");
		}
		getHibernateTemplate().bulkUpdate("update Despesaviagemitem it set it.valorrealizado = ? where it.id = ?", new Object[]{
				valorrealizado, despesaviagemitem.getCddespesaviagemitem()
		});
	}

	
}
