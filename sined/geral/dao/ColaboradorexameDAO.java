package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorexame;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColaboradorexameDAO extends GenericDAO<Colaboradorexame> {

	/* singleton */
	private static ColaboradorexameDAO instance;
	public static ColaboradorexameDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradorexameDAO.class);
		}
		return instance;
	}
	
	public List<Colaboradorexame> findExamesByColaborador(Colaborador colaborador){
		return query()
				.select("colaboradorexame.cdcolaboradorexame, colaboradorexame.dtexame, exametipo.cdexametipo, exametipo.nome, " +
						"examenatureza.cdexamenatureza, examenatureza.nome")
				.leftOuterJoin("colaboradorexame.exametipo exametipo")
				.leftOuterJoin("colaboradorexame.examenatureza examenatureza")
				.where("colaboradorexame.colaborador = ?", colaborador)
				.list();
	}
}