package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("prazopagamentoitem.cdprazopagamentoitem")
public class PrazopagamentoitemDAO extends GenericDAO<Prazopagamentoitem> {

	/**
	 * Fornece os itens de um prazo de pagamento.
	 * 
	 * @param prazo
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Prazopagamentoitem> findByPrazo(Prazopagamento prazo) {
		if(prazo == null || prazo.getCdprazopagamento() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
				.select("prazopagamentoitem.parcela, prazopagamentoitem.dias, prazopagamentoitem.meses, " +
						"prazopagamento.dataparceladiautil, prazopagamento.dataparcelaultimodiautilmes ")
				.where("prazopagamentoitem.prazopagamento = ?", prazo)
				.leftOuterJoin("prazopagamentoitem.prazopagamento prazopagamento")
				.orderBy("prazopagamentoitem.parcela")
				.list();
	}
	/**
	 * Fornece a quntidade de parcelas de um prazo de pagamento.
	 * 
	 * @param prazo
	 * @return
	 * @author Ramon Brazil
	 */
	public Long countParcelas(Prazopagamento prazo) {
		if (prazo == null) 
			return 0L;
		else
			return newQueryBuilderSined(Long.class)
						.select("count(*)")
						.from(Prazopagamentoitem.class)
						.where("prazopagamentoitem.prazopagamento = ?", prazo)
						.unique();
	}
	/**
	 * Fornece o dia e o mes de um prazo de pagamento de acordo com o numero da parcela.
	 * 
	 * @param prazo
	 * @param parcela
	 * @return
	 * @author Ramon Brazil
	 */
	public Prazopagamentoitem findByPrazoOfParcela(Prazopagamento prazo, Integer parcela) {
		return query()
			.select("prazopagamentoitem.parcela, prazopagamentoitem.dias, prazopagamentoitem.meses")
			.where("prazopagamentoitem.prazopagamento = ?", prazo)
			.where("prazopagamentoitem.parcela=?",parcela)
			.unique();
	}
	
	/**
	 * Retorna dados das parcelas de prazo de pagamento
	 * para calculo da data de proximo vencimento do contrato de fornecimento. 
	 * @param prazo
	 * @param numParcela
	 * @return
	 * @author Taidson
	 * @since 28/06/2010
	 */	
	public Prazopagamentoitem obtemDadosParcela(Prazopagamento prazo, Integer numParcela){
		return
			query()
			.select("prazopagamentoitem.cdprazopagamentoitem, prazopagamentoitem.parcela, prazopagamentoitem.dias, " +
					"prazopagamentoitem.meses, prazopagamento.cdprazopagamento")
			.leftOuterJoin("prazopagamentoitem.prazopagamento prazopagamento")
			.where("prazopagamentoitem.parcela = ?",numParcela)
			.where("prazopagamento = ?", prazo)
			.unique();
	}
	
	
	/**
	 * Retorna a quantidade de parcelas existentes em um determinado prazo de pagamento.
	 * @param prazopagamento
	 * @return
	 * @author Taidson
	 * @since 28/06/2010
	 */
	public Integer qtdeParcelasPrazoPagamento(Prazopagamento prazopagamento) {
		return newQueryBuilderSined(Long.class)
			.from(Prazopagamentoitem.class)
			.setUseTranslator(false) 
			.select("count(*)")
			.where("prazopagamentoitem.prazopagamento = ?", prazopagamento)
			.unique()
			.intValue();
	}
	
	
	public List<Prazopagamentoitem> findForPVOffline() {
		return query()
		.orderBy("prazopagamentoitem.prazopagamento, prazopagamentoitem.parcela")
		.list();
	}
	
	public List<Prazopagamentoitem> findForAndroid(String whereIn) {
		return query()
		.whereIn("prazopagamentoitem.cdprazopagamentoitem", whereIn)
		.orderBy("prazopagamentoitem.prazopagamento, prazopagamentoitem.parcela")
		.list();
	}
	
}
