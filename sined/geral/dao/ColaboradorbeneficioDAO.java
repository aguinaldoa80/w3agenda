package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorbeneficio;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColaboradorbeneficioDAO extends GenericDAO<Colaboradorbeneficio>{
	
	public List<Colaboradorbeneficio> findByColaborador(Colaborador colaborador){
		return query()
				.select("colaboradorbeneficio.cdcolaboradorbeneficio, colaboradorbeneficio.quantidade, beneficio.cdbeneficio, beneficio.nome")
				.join("colaboradorbeneficio.colaborador colaborador")
				.join("colaboradorbeneficio.beneficio beneficio")
				.where("colaborador=?", colaborador)
				.orderBy("beneficio.nome")
				.list();
	}
}
