package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.sined.geral.bean.PedidoVendaTipoHistorico;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.util.SinedException;
public class PedidoVendaTipoHistoricoDAO extends GenericDAO<PedidoVendaTipoHistorico>{
	

	public List<PedidoVendaTipoHistorico> findByPedidoVendaTipo(Pedidovendatipo pedidovendatipo) {
		if (pedidovendatipo == null || pedidovendatipo.getCdpedidovendatipo() == null) {
			throw new SinedException("O tipo de pedido de venda n�o pode ser nulo.");
		}
		return query()
				.select("pedidoVendaTipoHistorico.cdPedidoVendaTipoHistorico, pedidoVendaTipoHistorico.observacao, "
						+ "pedidoVendaTipoHistorico.cdusuarioaltera, pedidoVendaTipoHistorico.pedidoVendaTipoAcao, "
						+ "pedidoVendaTipoHistorico.dtaltera, pedidovendatipo.cdpedidovendatipo")
				.leftOuterJoin("pedidoVendaTipoHistorico.pedidovendatipo pedidovendatipo")
				.where("pedidovendatipo = ?", pedidovendatipo)
				.orderBy("pedidoVendaTipoHistorico.dtaltera desc").list()
				;
	}

}
