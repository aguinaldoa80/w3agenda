package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Campanhatipo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("campanhatipo.nome")
public class CampanhatipoDAO extends GenericDAO<Campanhatipo>{
	
}
