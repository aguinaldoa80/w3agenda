package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Atividadetiporesponsavel;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AtividadetiporesponsavelDAO extends GenericDAO<Atividadetiporesponsavel> {
	
	/**
	 * 
	 * @param atividadetipo
	 * @author Mairon Cezar
	 */
	public List<Atividadetiporesponsavel> findByAtividadetipo(Atividadetipo atividadetipo){
		return query()
				.select("atividadetiporesponsavel.cdatividadetiporesponsavel, atividadetipo.cdatividadetipo, "+
						"projeto.cdprojeto,	projeto.nome, colaborador.cdpessoa, colaborador.nome")
				.join("atividadetiporesponsavel.atividadetipo atividadetipo")
				.leftOuterJoin("atividadetiporesponsavel.projeto projeto")
				.join("atividadetiporesponsavel.colaborador colaborador")
				.where("atividadetipo=?", atividadetipo)
				.list();
	}
	
	/**
	 * 
	 * @param atividadetipo
	 * @param projeto
	 * @author Mairon Cezar
	 */
	public Atividadetiporesponsavel findByAtividadetipoAndProjeto(Atividadetipo atividadetipo, Projeto projeto){
		return query()
				.select("atividadetiporesponsavel.cdatividadetiporesponsavel, atividadetipo.cdatividadetipo, "+
						"projeto.cdprojeto,	projeto.nome, colaborador.cdpessoa, colaborador.nome")
				.join("atividadetiporesponsavel.atividadetipo atividadetipo")
				.leftOuterJoin("atividadetiporesponsavel.projeto projeto")
				.join("atividadetiporesponsavel.colaborador colaborador")
				.where("atividadetipo=?", atividadetipo)
				.where("projeto=?", projeto)
				.unique();
	}
}
