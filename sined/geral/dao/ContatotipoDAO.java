package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Contatotipo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("contatotipo.nome")
public class ContatotipoDAO extends GenericDAO<Contatotipo> {

	public List<Contatotipo> findForAndroid(String whereIn) {
		return query()
				.select("contatotipo.cdcontatotipo, contatotipo.nome, contatotipo.socio")
				.whereIn("contatotipo.cdcontatotipo", whereIn)
				.list();
	}		
}
