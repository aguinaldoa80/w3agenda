package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Situacaohistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class SituacaohistoricoDAO extends GenericDAO<Situacaohistorico>{

	/**
	* M�todo que busca as situa��es do hist�rico
	*
	* @param whereIn
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<Situacaohistorico> findForAndroid(String whereIn) {
		return query()
			.select("situacaohistorico.cdsituacaohistorico, situacaohistorico.descricao")
			.whereIn("situacaohistorico.cdsituacaohistorico", whereIn)
			.list();
	}
}
