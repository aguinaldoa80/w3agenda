package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Emporiumvenda;
import br.com.linkcom.sined.geral.bean.Emporiumvendahistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EmporiumvendahistoricoDAO extends GenericDAO<Emporiumvendahistorico>{

	/**
	 * Busca o histórico pela venda do ECF.
	 *
	 * @param emporiumvenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 16/04/2013
	 */
	public List<Emporiumvendahistorico> findByEmporiumvenda(Emporiumvenda emporiumvenda) {
		return query()
					.where("emporiumvendahistorico.emporiumvenda = ?", emporiumvenda)
					.orderBy("emporiumvendahistorico.dtaltera desc")
					.list();
	}
	
}
