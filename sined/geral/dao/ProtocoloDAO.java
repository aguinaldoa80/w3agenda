package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Protocolo;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Usuariopapel;
import br.com.linkcom.sined.geral.service.PapelService;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.ProtocoloFiltro;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.filter.EventoservicointernoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProtocoloDAO extends GenericDAO<Protocolo>{
	private PapelService papelService;	
	

	public void setPapelService(PapelService papelService) {
		this.papelService = papelService;
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Protocolo> query) {
		query.select("protocolo.cdprotocolo, protocolo.assunto,protocolo.dtrecebimento," +
				"protocolo.dtenvio, remetente.cdpessoa, remetente.nome,remetente.email,destinatario.email," +
				"destinatario.cdpessoa, destinatario.nome," +
				"listadocumento.cdprotocolodocprocesso, documentoprocesso.cddocumentoprocesso,documentoprocesso.arquivo," +
				"documentoprocesso.titulo, listaprovidencia.cdprotocoloprovidencia, providencia.cdprovidencia, providencia.nome," +
				"protocolo.cdusuarioaltera,	protocolo.dtaltera")
				.leftOuterJoin("protocolo.remetente remetente")
				.leftOuterJoin("protocolo.destinatario destinatario")				
				.leftOuterJoin("protocolo.listadocumento listadocumento")
				.leftOuterJoin("listadocumento.documentoprocesso documentoprocesso")
				.leftOuterJoin("protocolo.listaprovidencia listaprovidencia")
				.leftOuterJoin("listaprovidencia.providencia providencia");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Protocolo> query,	FiltroListagem _filtro) {
		Usuario usuario;
		boolean administrador;
		try{
			 usuario = (Usuario)Neo.getUser();
			 List<Papel> listaPapel = new ArrayList<Papel>();
			 for(Usuariopapel usuariopapel: usuario.getListaUsuariopapel()){
				 listaPapel.add(usuariopapel.getPapel());
			 }
			 administrador =papelService.isAdministrador(listaPapel);
		}catch(Exception e){
			throw new SinedException("Erro. Usu�rio logado inv�lido em DocumentoprocessoDAO");
		}
		
		ProtocoloFiltro filtro = (ProtocoloFiltro) _filtro;
		
		query
		 .select("distinct protocolo.cdprotocolo,protocolo.assunto,protocolo.dtenvio,protocolo.dtrecebimento, " +
		 		"remetente.cdpessoa,remetente.nome, destinatario.cdpessoa, destinatario.nome")
		 .leftOuterJoin("protocolo.remetente remetente")
		 .leftOuterJoin("protocolo.destinatario destinatario")	
		 .where("protocolo.cdprotocolo=?", filtro.getNumProtocolo())
		 .where("protocolo.remetente=?", filtro.getRemetente())
		 .where("protocolo.destinatario=?", filtro.getDestinatario())
		 .where("protocolo.dtenvio>=?", filtro.getInicio())	
		
		 .whereLikeIgnoreAll("protocolo.assunto",filtro.getAssunto())
		 .orderBy("protocolo.cdprotocolo desc");
		if(!administrador){
				query.openParentheses()
				 .where("protocolo.remetente =?", usuario)
				 .or()
				 .where("protocolo.destinatario =?", usuario)	
			 .closeParentheses();
		
		}
		if(filtro.getFim()!=null){
				Timestamp fim = new Timestamp(SinedDateUtils.incrementDate(filtro.getFim(), 1, Calendar.DATE).getTime());
				fim.setTime(fim.getTime()-(86400000));			
			 query.
			 where("protocolo.dtenvio<=?", fim);
		}
		//Resovendo problema no leftouterjoin com listas
		if(filtro.getTitulo()!=null && filtro.getTitulo().length()!=0){
			query .leftOuterJoin("protocolo.listadocumento listadocumento")
			.whereLikeIgnoreAll("listadocumento.documentoprocesso.titulo", filtro.getTitulo())
			.ignoreJoin("listadocumento","documentoprocesso");
		}
		if(filtro.getProvidencia()!= null){
			query
			.leftOuterJoin("protocolo.listaprovidencia listaprovidencia")
			.where("listaprovidencia.providencia=?", filtro.getProvidencia())
			.ignoreJoin("listaprovidencia");	
			
		}
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		
		save.saveOrUpdateManaged("listadocumento")
		.saveOrUpdateManaged("listaprovidencia");		
	}
	
	/**
	 * Atualiza a dada de recebimento como a data atual
	 * @param bean
	 * @author C�ntia Nogueira
	 */
	public void recebeProtocolo(Protocolo bean){
		if(bean == null || bean.getCdprotocolo() == null){
			throw new SinedException("Objeto n�o pode ser nulo em SolicitacaoDAO");
		}
		
		getHibernateTemplate().bulkUpdate("update Protocolo set dtrecebimento=? " +
				"where cdprotocolo=?", 
		new Object[] {new Date(System.currentTimeMillis()), bean.getCdprotocolo()});
	}
	
	
	/**
	 * Fun��o para a gera��o do relat�rio de eventos
	 * @param filtro
	 * @return
	 * @author C�ntia Nogueira
	 */
	public List<Protocolo> findForRelatorioEvento(EventoservicointernoFiltro filtro){
		if(filtro==null){
			throw new SinedException("O filtro n�o pode ser nulo em ProtocoloDAO");
		}		
		
		return querySined()
				
			.select("protocolo.cdprotocolo,protocolo.assunto,protocolo.dtenvio,protocolo.dtrecebimento,  " +
			"remetente.cdpessoa,remetente.nome, destinatario.cdpessoa, destinatario.nome")
			.join("protocolo.remetente remetente")
			.join("protocolo.destinatario destinatario")
			.leftOuterJoin("protocolo.listadocumento listadocumento")
			.whereLikeIgnoreAll("protocolo.assunto",filtro.getAssunto() )
			.whereLikeIgnoreAll("remetente.nome", filtro.getRemetente())
			.whereLikeIgnoreAll("destinatario.nome", filtro.getDestinatario())
			.where("protocolo.dtenvio>=?", filtro.getInicio())
			.where("protocolo.dtenvio<=?", filtro.getFim())
			.orderBy("protocolo.dtenvio asc")
			.list();
			
	
	}

}
