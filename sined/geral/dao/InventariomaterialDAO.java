package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Inventario;
import br.com.linkcom.sined.geral.bean.Inventariomaterial;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioTipo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoitemsped;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.InventariomaterialFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.GerarInventarioFiltroBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class InventariomaterialDAO extends GenericDAO<Inventariomaterial>{
	
	private MaterialcategoriaService materialcategoriaService;
	
	public void setMaterialcategoriaService(
			MaterialcategoriaService materialcategoriaService) {
		this.materialcategoriaService = materialcategoriaService;
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Inventariomaterial> query) {
		query
			.select("inventariomaterial.cdinventariomaterial, inventariomaterial.qtde, inventariomaterial.qtdeOrigem, inventariomaterial.valorUnitario, " +
					"inventariomaterial.indicadorpropriedade, inventariomaterial.tipopessoainventario, inventariomaterial.contaanaliticacontabil, " +
					"material.cdmaterial, material.nome, pessoa.cdpessoa, pessoa.nome, inventario.cdinventario, inventario.dtinventario,inventariomaterial.gerarBlocoK,inventariomaterial.gerarBlocoH, " +
					"loteestoque.cdloteestoque, loteestoque.numero")
			.join("inventariomaterial.inventario inventario")
			.join("inventariomaterial.material material")
			.leftOuterJoin("inventariomaterial.pessoa pessoa")
			.leftOuterJoin("inventariomaterial.loteestoque loteestoque")
			;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Inventariomaterial> query, FiltroListagem _filtro) {
		InventariomaterialFiltro filtro = (InventariomaterialFiltro) _filtro;
		
		query
			.select("inventariomaterial.cdinventariomaterial, inventariomaterial.qtde, inventariomaterial.valorUnitario, " +
					"inventariomaterial.indicadorpropriedade, inventariomaterial.tipopessoainventario, inventariomaterial.contaanaliticacontabil,inventariomaterial.gerarBlocoK,inventariomaterial.gerarBlocoH, " +
					"material.cdmaterial, material.nome, pessoa.cdpessoa, pessoa.nome, inventario.cdinventario, inventario.dtinventario")
			.join("inventariomaterial.inventario inventario")
			.join("inventariomaterial.material material")
			.leftOuterJoin("inventariomaterial.pessoa pessoa")
			.where("inventariomaterial.material = ?", filtro.getMaterial())
			.where("inventariomaterial.inventario = ?", filtro.getInventario())
			.where("inventariomaterial.indicadorpropriedade = ?", filtro.getIndicadorpropriedade())
			.where("coalesce(inventariomaterial.gerarBlocoK, false) = ?", filtro.getGerarBlocoK())
			.where("coalesce(inventariomaterial.gerarBlocoH, false) = ?", filtro.getGerarBlocoH())
			;
		
		if(filtro.getMaterialcategoria() != null){
			materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
			query
				.leftOuterJoinIfNotExists("material.materialcategoria materialcategoria")
				.leftOuterJoinIfNotExists("materialcategoria.vmaterialcategoria vmaterialcategoria")
				.openParentheses()
					.where("vmaterialcategoria.identificador like ? ||'.%'", filtro.getMaterialcategoria().getIdentificador())
				.or()
					.where("vmaterialcategoria.identificador like ? ", filtro.getMaterialcategoria().getIdentificador())
				.closeParentheses();
		}
	}

	/**
	 * Busca os materiais a partir de um invent�rio
	 *
	 * @param inventario
	 * @return
	 * @since 03/10/2019
	 * @author Rodrigo Freitas
	 */
	public List<Inventariomaterial> findByInventario(Inventario inventario) {
		if(inventario == null || inventario.getCdinventario() == null){
			throw new SinedException("Invent�rio n�o pode ser nulo.");
		}
		return query()
					.select("inventariomaterial.cdinventariomaterial, inventariomaterial.qtde, inventariomaterial.qtdeOrigem, inventariomaterial.valorUnitario, " +
							"inventariomaterial.indicadorpropriedade, inventariomaterial.tipopessoainventario, inventariomaterial.contaanaliticacontabil, " +
							"material.cdmaterial, material.nome, pessoa.cdpessoa, pessoa.nome, loteestoque.cdloteestoque, loteestoque.numero, " +
							"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo")
					.join("inventariomaterial.material material")
					.join("material.unidademedida unidademedida")
					.leftOuterJoin("inventariomaterial.pessoa pessoa")
					.leftOuterJoin("inventariomaterial.loteestoque loteestoque")
					.where("inventariomaterial.inventario = ?", inventario)
					.orderBy("material.nome")
					.list();
	}

	/**
	 * Busca o estoque para gera��o de invent�rio
	 *
	 * @param filtro
	 * @return
	 * @since 04/10/2019
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<Inventariomaterial> findForGerarInventario(GerarInventarioFiltroBean filtro) {
		Date dtinventario = filtro.getDtinventario();
		
		StringBuilder sql = new StringBuilder();
		sql.append("select * ");
		sql.append("from ( ");
		sql.append("select m.referencia, m.cdmaterial, m.identificacao, m.nome as nome_material, m.peso, m.pesobruto, mg.gerarblocoh, mg.gerarblocok, ");
		sql.append("le.cdloteestoque, le.numero, lm.validade, ");
		sql.append("um.cdunidademedida, um.nome as nome_unidademedida, um.simbolo, ");
		sql.append("material_custo(m.cdmaterial, '" + SinedDateUtils.toStringPostgre(dtinventario) + "') as valorcusto, ");
		sql.append("material_disponivel_local(m.cdmaterial, 'PO',  ");
		
		if(filtro.getLocalarmazenagem() != null){
			sql.append(filtro.getLocalarmazenagem().getCdlocalarmazenagem()).append(", ");
		} else sql.append("null, ");
		
		if(filtro.getEmpresa() != null){
			sql.append(filtro.getEmpresa().getCdpessoa()).append(", ");
		} else sql.append("null, ");
		
		if(filtro.getProjeto() != null){
			sql.append(filtro.getProjeto().getCdprojeto()).append(", ");
		} else sql.append("null, ");
		
		sql.append("true, lm.cdloteestoque, false, '" + SinedDateUtils.toStringPostgre(dtinventario) + "', true, false, false) as qtdedisponivel ");
		sql.append("from material m ");
		sql.append("left join lotematerial lm on lm.cdmaterial = m.cdmaterial ");
		sql.append("left join loteestoque le on le.cdloteestoque = lm.cdloteestoque ");
		sql.append("left join materialgrupo mg on mg.cdmaterialgrupo = m.cdmaterialgrupo ");
		sql.append("join unidademedida um on um.cdunidademedida = m.cdunidademedida ");
		sql.append("where m.ativo = true ");
		if(InventarioTipo.ESTOQUEESTRUTURADO.equals(filtro.getInventarioTipo())){
			sql.append("and m.tipoitemsped not in (" + Tipoitemsped.USO_CONSUMO.ordinal() + ", " + Tipoitemsped.ATIVO_IMOBILIZADO.ordinal() + ", " + Tipoitemsped.OUTRAS.ordinal() + ", " + Tipoitemsped.SERVICOS.ordinal() + ")");			
		}
		sql.append("order by m.nome ");
		sql.append(") query ");
		sql.append("where query.qtdedisponivel > 0");
		
		
		SinedUtil.markAsReader();
		return getJdbcTemplate().query(sql.toString(), new RowMapper(){
			@Override
			public Inventariomaterial mapRow(ResultSet rs, int rowNum) throws SQLException {
				Integer cdunidademedida = rs.getInt("cdunidademedida");
				String nome_unidademedida = rs.getString("nome_unidademedida");
				String simbolo = rs.getString("simbolo");
				
				Unidademedida unidademedida = new Unidademedida(cdunidademedida, nome_unidademedida, simbolo);
				
				String referencia = rs.getString("referencia");
				Integer cdmaterial = rs.getInt("cdmaterial");
				String identificacao = rs.getString("identificacao");
				String nome_material = rs.getString("nome_material");
				Double peso = rs.getDouble("peso");
				Double pesobruto = rs.getDouble("pesobruto");
				
				Material material = new Material(cdmaterial, referencia, identificacao, nome_material, peso, pesobruto, unidademedida);
				
				Loteestoque loteestoque = null;
				if(rs.getObject("cdloteestoque") != null){
					Integer cdloteestoque = rs.getInt("cdloteestoque");
					String numero = rs.getString("numero");
					Date validade = rs.getDate("validade");
					
					loteestoque = new Loteestoque(cdloteestoque, numero, validade);
				}
				
				Double valorcusto = rs.getDouble("valorcusto");
				Double qtdedisponivel = rs.getDouble("qtdedisponivel");
				
				Inventariomaterial im = new Inventariomaterial(material, loteestoque, valorcusto, qtdedisponivel);
				
				im.setGerarBlocoH(rs.getBoolean("gerarblocoh"));
				im.setGerarBlocoK(rs.getBoolean("gerarblocok"));
				return im;
			}
		});
	}

	public Inventariomaterial findGerarBloco (String whereIn){
		return query()
				.select("inventariomaterial.cdinventariomaterial, inventariomaterial.gerarBlocoH, inventariomaterial.gerarBlocoK")
				.whereIn("inventariomaterial.cdinventariomaterial", whereIn)
				.unique();
	}
}
