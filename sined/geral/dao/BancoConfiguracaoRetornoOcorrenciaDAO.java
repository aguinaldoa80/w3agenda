package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoOcorrencia;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("bancoConfiguracaoRetornoOcorrencia.descricao")
public class BancoConfiguracaoRetornoOcorrenciaDAO extends GenericDAO<BancoConfiguracaoRetornoOcorrencia> {

	
}
