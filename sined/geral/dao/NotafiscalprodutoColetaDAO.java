package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.NotafiscalprodutoColeta;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotafiscalprodutoColetaDAO extends GenericDAO<NotafiscalprodutoColeta> {

	/**
	 * Verifica se existe alguma nota de devolu��o vinculada �s coletas que n�o est�o canceladas.
	 *
	 * @param whereInColeta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/10/2015
	 */
	public boolean haveNotaDevolucaoNaoCanceladaByColeta(String whereInColeta, String whereInPedidovendamaterial) {
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(NotafiscalprodutoColeta.class)
				.join("notafiscalprodutoColeta.notafiscalproduto notafiscalproduto")
				.join("notafiscalproduto.listaItens listaItens")
				.where("notafiscalprodutoColeta.devolucao = ?", Boolean.TRUE)
				.whereIn("notafiscalprodutoColeta.coleta.cdcoleta", whereInColeta)
				.whereIn("listaItens.pedidovendamaterial.cdpedidovendamaterial", whereInPedidovendamaterial)
				.where("notafiscalproduto.notaStatus <> ?", NotaStatus.CANCELADA)
				.unique() > 0;
	}
	
	public List<NotafiscalprodutoColeta> findNotaDevolucao(String whereInColeta, String whereInPedidovendamaterial, Boolean considerarSomenteDevolucao) {
		return query()
				.select("notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.tipooperacaonota")
				.from(NotafiscalprodutoColeta.class)
				.join("notafiscalprodutoColeta.notafiscalproduto notafiscalproduto")
				.join("notafiscalproduto.listaItens listaItens")
				.where("notafiscalprodutoColeta.devolucao = ?", Boolean.TRUE, Boolean.TRUE.equals(considerarSomenteDevolucao))
				.whereIn("notafiscalprodutoColeta.coleta.cdcoleta", whereInColeta)
				.whereIn("listaItens.pedidovendamaterial.cdpedidovendamaterial", whereInPedidovendamaterial)
				.where("notafiscalproduto.notaStatus <> ?", NotaStatus.CANCELADA)
				.list();
	}
	
	public Boolean existsColetaInNotaFiscalProduto(Integer cdnotafiscalproduto) {
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.from(NotafiscalprodutoColeta.class)
				.leftOuterJoin("notafiscalprodutoColeta.notafiscalproduto notafiscalproduto")
				.where("notafiscalproduto.cdNota = ?", cdnotafiscalproduto)
				.unique() > 0;
	}
	
	@SuppressWarnings("unchecked")
	public String getWhereInColeta(Notafiscalproduto nota) {
		if(nota == null || nota.getCdNota() == null)
			return null;
		
		String sql = " select distinct nfpc.cdcoleta " +
					 " from notafiscalprodutocoleta nfpc " +
					 " join nota n on n.cdnota = nfpc.cdnotafiscalproduto " +
					 " where n.cdnota in (" + nota.getCdNota() + ") ";

		SinedUtil.markAsReader();
		List<Integer> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Integer(rs.getInt("cdcoleta"));
			}
		});
		
		return list != null ? CollectionsUtil.concatenate(list, ",") : null;
	}
	
	@SuppressWarnings("unchecked")
	public String getWhereInNotaColeta(Notafiscalproduto nota, String whereInColeta) {
		if(StringUtils.isBlank(whereInColeta))
			return null;
		
		String sql = " select distinct n.numero " +
					 " from notafiscalprodutocoleta nfpc " +
					 " join nota n on n.cdnota = nfpc.cdnotafiscalproduto " +
					 " where nfpc.cdcoleta in (" + whereInColeta + ") " + 
					 (nota != null && nota.getCdNota() != null ? " and n.cdnota <> " + nota.getCdNota() : "");

		SinedUtil.markAsReader();
		List<String> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString("numero");
			}
		});
		
		return list != null ? CollectionsUtil.concatenate(list, ",") : null;
	}
	
	/**
	* Verifica se existe alguma nota de retorno vinculado ao pedido de venda da coleta.
	*
	*
	* @param whereInColeta
	* @return
	* @since 03/10/2017
	* @author Luiz Fernando
	*/
	public boolean haveNotaRetornoPedidovendaOrVenda(String whereInColeta) {
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(NotafiscalprodutoColeta.class)
				.join("notafiscalprodutoColeta.notafiscalproduto notafiscalproduto")
				.leftOuterJoin("notafiscalproduto.listaNotafiscalprodutovendaretorno listaNotafiscalprodutovendaretorno")
				.leftOuterJoin("notafiscalproduto.listaNotafiscalprodutopedidovendaretorno listaNotafiscalprodutopedidovendaretorno")
				.whereIn("notafiscalprodutoColeta.coleta.cdcoleta", whereInColeta)
				.where("notafiscalproduto.notaStatus <> ?", NotaStatus.CANCELADA)
				.openParentheses()
					.where("listaNotafiscalprodutovendaretorno is not null")
					.or()
					.where("listaNotafiscalprodutopedidovendaretorno is not null")
				.closeParentheses()
				.unique() > 0;
	}
}
