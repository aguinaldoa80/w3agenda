package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ComissionamentoPorFaixasFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.bean.ComissionamentoPorFaixasTotalizadorReportBean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ComissionamentoPorFaixasDAO extends GenericDAO<Documentocomissao> {
	@Override
	public void updateListagemQuery(QueryBuilder<Documentocomissao> query,
			FiltroListagem _filtro) {
		ComissionamentoPorFaixasFiltro filtro = (ComissionamentoPorFaixasFiltro) _filtro;
		
		query
			.select("documentocomissao.cddocumentocomissao, documentocomissao.valorcomissao, " +
					"vendedor.cdpessoa, vendedor.nome, " +
					"venda.cdvenda, venda.valorfrete, venda.taxapedidovenda, venda.desconto, venda.valorusadovalecompra, venda.dtvenda, " +
					"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.descricao, " +
					"comissionamento.cdcomissionamento, comissionamento.nome, " +
					"colaborador.nome, colaborador.cdpessoa, " +
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
					"material.cdmaterial, material.identificacao, material.nome, " +
					"faixaMarkupNome.cdFaixaMarkupNome, faixaMarkupNome.nome, " +
					"colaboradorcomissao.cdcolaboradorcomissao, colabcomissaoDocumento.cddocumento, colabcomissaoDocumentoacao.cddocumentoacao")
			.leftOuterJoin("documentocomissao.venda venda")
			.leftOuterJoin("documentocomissao.nota nota")
			.join("documentocomissao.vendamaterial vendamaterial")
			.leftOuterJoin("documentocomissao.comissionamento comissionamento")
			.leftOuterJoin("documentocomissao.pessoa vendedor")
			.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
			.leftOuterJoin("venda.colaborador colaborador")
			.leftOuterJoin("vendamaterial.faixaMarkupNome faixaMarkupNome")
			.leftOuterJoin("vendamaterial.material material")
			.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
			.leftOuterJoin("colaboradorcomissao.documento colabcomissaoDocumento")
			.leftOuterJoin("colabcomissaoDocumento.documentoacao colabcomissaoDocumentoacao")
			.join("vendamaterial.unidademedida unidademedida")
			.where("venda.cliente = ?", filtro.getCliente())
			.where("vendedor = ?", filtro.getColaborador())
			.where("pedidovendatipo = ?", filtro.getPedidovendatipo())
			.where("venda.dtvenda >= ?", filtro.getDtinicio())
			.where("venda.dtvenda <= ?", filtro.getDtfim());
		
		if(filtro.getPago() != null && filtro.getPago()) {
			query.where("colaboradorcomissao is not null");
		}
		
		query.orderBy("venda.dtvenda DESC");
	}
	
	public List<Documentocomissao> findForSomaTotaisListagem(ComissionamentoPorFaixasFiltro filtro) {
		QueryBuilder<Documentocomissao> query = query();
		updateListagemQuery(query, filtro);
		
		return query.list();
	}
	
	public List<Documentocomissao> findForPdf(ComissionamentoPorFaixasFiltro filtro){
		QueryBuilder<Documentocomissao> query = querySined();
		updateListagemQuery(query, filtro);
		
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<ComissionamentoPorFaixasTotalizadorReportBean> findTotalComissaoPorColaborador(String whereInDocumentoComissao){
		if(StringUtils.isBlank(whereInDocumentoComissao)){
			return new ArrayList<ComissionamentoPorFaixasTotalizadorReportBean>();
		}
		try {
			StringBuilder sql = new StringBuilder();
			
			sql
				.append("select sum(documentocomissao.valorcomissao) as valorComissao, colaborador.cdpessoa, pessoa.nome")
				.append("  from documentocomissao ")
				.append("  join venda on venda.cdvenda = documentocomissao.cdvenda ")
				.append("  join colaborador on colaborador.cdpessoa = venda.cdcolaborador ")
				.append("  join pessoa on colaborador.cdpessoa = pessoa.cdpessoa ")
				.append(" where documentocomissao.cddocumentocomissao in (" + whereInDocumentoComissao + ") ")
				.append(" group by colaborador.cdpessoa, pessoa.nome ");

			SinedUtil.markAsReader();
			return (List<ComissionamentoPorFaixasTotalizadorReportBean>)getJdbcTemplate().query(sql.toString(), new RowMapper() {
				public ComissionamentoPorFaixasTotalizadorReportBean mapRow(ResultSet rs, int rowNum) throws SQLException {
					ComissionamentoPorFaixasTotalizadorReportBean bean = new ComissionamentoPorFaixasTotalizadorReportBean();
					bean.setColaborador(rs.getString("nome"));
					bean.setValorTotalComissao(new Money(rs.getDouble("valorComissao"), true));
					bean.setCdPessoa(rs.getInt("cdpessoa"));
					
					return bean;
				}
			});
		} catch(Exception e) {
			return new ArrayList<ComissionamentoPorFaixasTotalizadorReportBean>();
		}		
	}
}
