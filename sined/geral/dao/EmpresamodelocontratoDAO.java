package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresamodelocontrato;
import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EmpresamodelocontratoDAO extends GenericDAO<Empresamodelocontrato> {

	/**
	 * Busca os modelos de contratos que est�o associados aos Materiais/Servidos
	 * de um determinado contrato.
	 * 
	 * @author Giovane Freitas
	 * @param contrato
	 * @return
	 */
	public List<Empresamodelocontrato> findByContrato(Contrato contrato) {
		if (contrato == null || contrato.getCdcontrato() == null)
			throw new SinedException("Par�metros inv�lidos para busca.");


		Contrato contratoAux = newQueryBuilder(Contrato.class)
				.select("contrato.cdcontrato, contratomaterial.cdcontratomaterial, servico.cdmaterial, " +
						"empresamodelocontrato.cdempresamodelocontrato, empresamodelocontrato.nome")
				.from(Contrato.class)
				.join("contrato.listaContratomaterial contratomaterial")
				.join("contratomaterial.servico servico")
				.join("servico.empresamodelocontrato empresamodelocontrato")
				.where("contrato.id = ?", contrato.getCdcontrato())
				.unique();
		
		List<Empresamodelocontrato> resultado = new ArrayList<Empresamodelocontrato>();
		
		if (contratoAux != null && contratoAux.getListaContratomaterial() != null){
			for (Contratomaterial item : contratoAux.getListaContratomaterial()){
				if (item.getServico().getEmpresamodelocontrato() != null && !resultado.contains(item.getServico().getEmpresamodelocontrato())){
					resultado.add(item.getServico().getEmpresamodelocontrato());
				}
			}
		}
		
		return resultado;
	}

	/**
	 * Busca os modelos de contratos que est�o associados a uma determinada
	 * empresa.
	 * 
	 * @author Giovane Freitas
	 * @param empresa
	 * @return
	 */
	public List<Empresamodelocontrato> findByEmpresa(Empresa empresa) {
		if (empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos para busca.");

		return query()
				.where("empresamodelocontrato.empresa = ?", empresa)
				.list();
	}

	/**
	 * Busca o modelo de contrato que est� associado a uma determinada
	 * proposta atrav�s de uma empresa.
	 * 
	 * @author Thiago Augusto
	 * @param empresa
	 * @return
	 */
	public List<Empresamodelocontrato> findByProposta(Proposta proposta) {
		
		Proposta propostaAux = newQueryBuilder(Proposta.class)
			.from(Proposta.class)
			.select("proposta.cdproposta, empresa.cdpessoa," +
					"empresamodelocontrato.cdempresamodelocontrato," +
					"empresamodelocontrato.nome")
			.join("proposta.empresa empresa")
			.join("empresa.listaEmpresamodelocontrato empresamodelocontrato")
			.where("proposta.cdproposta = ?", proposta.getCdproposta())
			.unique();

		List<Empresamodelocontrato> resultado = new ArrayList<Empresamodelocontrato>();
		
		if (propostaAux != null && propostaAux.getEmpresa() != null)
			resultado.addAll(propostaAux.getEmpresa().getListaEmpresamodelocontrato());
			return resultado;
	}
}
