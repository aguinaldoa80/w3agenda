package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("documentoacao.cddocumentoacao")
public class DocumentoacaoDAO extends GenericDAO<Documentoacao>{
	
	
	/**
	 * Obt�m lista de <b>documentoacao</b> a serem exibidas em documento.
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documentoacao> findAcoesForDocumento(){
		return querySined()
			
			.select("documentoacao.cddocumentoacao,documentoacao.nome")
			.where("documentoacao.mostradocumento = ?", Boolean.TRUE)
			.orderBy("documentoacao.cddocumentoacao")
			.list();
	}

	
	@Override
	public List<Documentoacao> findForCombo() {
		return querySined()
			
			.select("documentoacao.cddocumentoacao,documentoacao.nome")
			.where("documentoacao.mostradocumento = ?", Boolean.TRUE)
			.orderBy("documentoacao.cddocumentoacao")
			.list();
	}
	
	/**
	 * M�todo para carregar uma lista de Documentoacao com o WHEREIN e os campos informados.
	 * 
	 * @param whereIn
	 * @param campos
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documentoacao> loadAsList(String whereIn, String campos){
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("O par�metro whereIn n�o pode ser null ou vazio.");
		}
		QueryBuilder<Documentoacao> q = query();
		if(campos != null && !campos.equals("")){
			q.select(campos);
		}
		return 
			q
			.whereIn("documentoacao.cddocumentoacao", whereIn)
			.list();
	}
	
}
