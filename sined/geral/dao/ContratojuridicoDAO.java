package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Contratojuridico;
import br.com.linkcom.sined.geral.bean.Processojuridico;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContratojuridico;
import br.com.linkcom.sined.geral.service.ContratojuridicoparcelaService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.TaxaService;
import br.com.linkcom.sined.modulo.juridico.controller.crud.filter.ContratojuridicoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContratojuridicoDAO extends GenericDAO<Contratojuridico>{
	
	private RateioService rateioService;
	private TaxaService taxaService;
	private ContratojuridicoparcelaService contratojuridicoparcelaService;
	
	public void setTaxaService(TaxaService taxaService) {
		this.taxaService = taxaService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setContratojuridicoparcelaService(ContratojuridicoparcelaService contratojuridicoparcelaService) {
		this.contratojuridicoparcelaService = contratojuridicoparcelaService;
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Contratojuridico> query, FiltroListagem _filtro) {
		ContratojuridicoFiltro filtro = (ContratojuridicoFiltro) _filtro;
		
		query	
			.select("contratojuridico.cdcontratojuridico, contratojuridico.descricao, contratojuridico.dtinicio, contratojuridico.honorario, " +
			"contratojuridico.valor, cliente.nome, empresa.nome, empresa.razaosocial, empresa.nomefantasia, aux_contratojuridico.dtproximovencimento, aux_contratojuridico.situacao ")
			.leftOuterJoin("contratojuridico.aux_contratojuridico aux_contratojuridico")
			.leftOuterJoin("contratojuridico.cliente cliente")
			.leftOuterJoin("contratojuridico.empresa empresa")
			.leftOuterJoin("contratojuridico.contratotipo contratotipo")
			.where("cliente = ?", filtro.getClientenome())
			.where("cliente = ?", filtro.getClienterazaosocial())
			.whereLikeIgnoreAll("contratojuridico.descricao", filtro.getDescricao())
			.where("contratojuridico.dtinicio >= ?", filtro.getDtinicioDe())
			.where("contratojuridico.dtinicio <= ?", filtro.getDtinicioAte())
			.where("empresa = ?", filtro.getEmpresa())
			.where("contratotipo = ?", filtro.getContratotipo());
			
		if(filtro.getListaSituacao() != null){
			query.whereIn("aux_contratojuridico.situacao", SituacaoContratojuridico.listAndConcatenate(filtro.getListaSituacao()));
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Contratojuridico> query) {
		query
			.leftOuterJoin("contratojuridico.taxa taxa")
			.leftOuterJoin("contratojuridico.rateio rateio")
			.leftOuterJoinFetch("contratojuridico.aux_contratojuridico aux_contratojuridico")
			.leftOuterJoinFetch("contratojuridico.listaContratojuridicoparcela contratojuridicoparcela")
			.orderBy("contratojuridicoparcela.dtvencimento");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Contratojuridico contratojuridico = (Contratojuridico)save.getEntity();
		
		save.saveOrUpdateManaged("listaContratojuridicoparcela");
		
		if(contratojuridico.getRateio() != null){
			rateioService.saveOrUpdateNoUseTransaction(contratojuridico.getRateio());
		}
		
		if(contratojuridico.getTaxa() != null){
			taxaService.saveOrUpdateNoUseTransaction(contratojuridico.getTaxa());
		}
	}
	
	public void updateAux(Integer cdcontratojuridico) {
		getJdbcTemplate().execute("SELECT atualiza_contratojuridico("+cdcontratojuridico+")");
	}
	
	public void updateTudo() {
		getJdbcTemplate().execute("SELECT atualiza_todos_contratojuridico()");
	}
	
	public List<Contratojuridico> findForReceita(String whereIn) {
		return query()
					.select("contratojuridico.cdcontratojuridico, contratojuridico.descricao, contratojuridico.valorcausa, " +
							"contratojuridico.tipohonorario, contratojuridico.honorario, " +
							"empresa.cdpessoa, conta.cdconta, contacarteira.msgboleto1, contacarteira.msgboleto2, " +
							"cliente.cdpessoa, contratojuridicoparcela.valor, contratojuridicoparcela.dtvencimento, contratojuridicoparcela.cdcontratojuridicoparcela, " +
							"contratojuridicoparcela.cobrada, contagerencial.cdcontagerencial, centrocusto.cdcentrocusto, " +
							"projeto.cdprojeto, rateioitem.valor, rateioitem.percentual, " +
							"taxaitem.valor, taxaitem.percentual, taxaitem.dtlimite, taxaitem.aodia, tipotaxa.cdtipotaxa, tipotaxa.nome, " +
							"aux_contratojuridico.situacao, aux_contratojuridico.dtproximovencimento")
					.leftOuterJoin("contratojuridico.aux_contratojuridico aux_contratojuridico")
					.leftOuterJoin("contratojuridico.empresa empresa")
					.leftOuterJoin("contratojuridico.conta conta")
					.leftOuterJoin("conta.listaContacarteira contacarteira")
					.leftOuterJoin("contratojuridico.cliente cliente")
					.leftOuterJoin("contratojuridico.listaContratojuridicoparcela contratojuridicoparcela")
					.leftOuterJoin("contratojuridico.taxa taxa")
					.leftOuterJoin("taxa.listaTaxaitem taxaitem")
					.leftOuterJoin("taxaitem.tipotaxa tipotaxa")
					.leftOuterJoin("contratojuridico.rateio rateio")
					.leftOuterJoin("rateio.listaRateioitem rateioitem")
					.leftOuterJoin("rateioitem.contagerencial contagerencial")
					.leftOuterJoin("rateioitem.centrocusto centrocusto")
					.leftOuterJoin("rateioitem.projeto projeto")
					.whereIn("contratojuridico.cdcontratojuridico", whereIn)
					.list();
	}
	
	public void atualizaDataSentenca(Contratojuridico contratojuridico) {
		getHibernateTemplate().bulkUpdate("update Contratojuridico c set c.dtsentenca = ? where c.id = ?", new Object[]{
				SinedDateUtils.currentDate(), contratojuridico.getCdcontratojuridico()
		});
	}
	
	/**
	 * M�todo que retorna o contrato para a emiss�o do relatorio, baseado no modelo RTF
	 * 
 	 * @author Thiago Clemente
	**/
	public Contratojuridico loadForEmissaoContratoRTF(Contratojuridico contratojuridico) {
		
		Contratojuridico result = query()
				.leftOuterJoinFetch("contratojuridico.empresa empresa")
				.leftOuterJoinFetch("empresa.logomarca logomarca")
				.leftOuterJoinFetch("contratojuridico.cliente cliente")
				//.leftOuterJoinFetch("contratojuridico.listaContratojuridicoparcela listaContratojuridicoparcela")
				.leftOuterJoinFetch("cliente.estadocivil estadocivil")
				.leftOuterJoinFetch("cliente.clienteprofissao clienteprofissao")
				.leftOuterJoinFetch("cliente.listaEndereco listaEndereco")
				.leftOuterJoinFetch("listaEndereco.enderecotipo enderecotipo")
				.leftOuterJoinFetch("listaEndereco.municipio municipio")
				.leftOuterJoinFetch("municipio.uf uf")
				.leftOuterJoinFetch("cliente.listaTelefone telefone")
				.leftOuterJoinFetch("telefone.telefonetipo telefonetipo")
				.leftOuterJoinFetch("contratojuridico.contratotipo contratotipo")			
				.where("contratojuridico = ?", contratojuridico)
				.unique();
		
		if (result != null){
			result.setListaContratojuridicoparcela(contratojuridicoparcelaService.findByContratojuridico(result));
		}
		
		return result;
	}
	
	public boolean haveContratoNaoCancelado(Processojuridico pj) {
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.from(Contratojuridico.class)
				.join("contratojuridico.aux_contratojuridico aux_contratojuridico")
				.where("contratojuridico.processojuridico = ?", pj)
				.where("aux_contratojuridico.situacao <> ?", SituacaoContratojuridico.CANCELADO)
				.unique() > 0;
	}
	
	public void cancelar(String whereIn) {
		getHibernateTemplate().bulkUpdate("update Contratojuridico cj set cj.dtcancelado = ? where cj.cdcontratojuridico in (" + whereIn + ")", SinedDateUtils.currentDate());
	}	
}