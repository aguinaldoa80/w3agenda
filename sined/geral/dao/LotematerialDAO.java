package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Lotematerial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class LotematerialDAO extends GenericDAO<Lotematerial> {
	
	/**
	 * Busca todos os materiais por lote
	 *
	 * @param loteestoque
	 * @param material
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 * @since 22/07/2015
	 */
	public List<Lotematerial> findAllByLoteestoqueMaterial(Loteestoque loteestoque, Material material) {
		if(loteestoque == null || loteestoque.getCdloteestoque() == null){
			throw new SinedException("Par�metro n�o pode ser nulo.");
		}
		
		return query()
				.select("lotematerial.cdlotematerial, lotematerial.validade, loteestoque.cdloteestoque, material.cdmaterial, material.nome, lotematerial.codigoAgregacao, lotematerial.fabricacao ")
				.leftOuterJoin("lotematerial.loteestoque loteestoque")
				.leftOuterJoin("lotematerial.material material")
				.where("lotematerial.loteestoque = ?", loteestoque)
				.where("lotematerial.material = ?", material)
				.list();
	}
	
	/**
	 * M�todo que retorna true caso exista lotematerial para o material selecionado e false caso contr�rio.
	 * 
	 * @param material
	 * @return
	 * @author Rafael Salvio
	 */
	public boolean existeLotematerial(Material material){
		if(material == null || material.getCdmaterial() == null){
			return false;
		}
		
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.from(Lotematerial.class)
			.setUseTranslator(false)
			.where("lotematerial.material = ?", material)
			.unique() > 0;
		
	}
	
	/**
	* M�todo que verifica se o material est� inclu�do no lote
	*
	* @param loteestoque
	* @param material
	* @return
	* @since 23/09/2016
	* @author Luiz Fernando
	*/
	public boolean existeMaterialNoLote(Loteestoque loteestoque, Material material){
		if(material == null || material.getCdmaterial() == null){
			return false;
		}
		
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.from(Lotematerial.class)
			.setUseTranslator(false)
			.where("lotematerial.loteestoque = ?", loteestoque)
			.where("lotematerial.material = ?", material)
			.unique() > 0;
	}

	public boolean existeNumeroFornecedorMaterial(Loteestoque loteestoque, Material material) {
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.from(Lotematerial.class)
				.setUseTranslator(false)
				.join("lotematerial.loteestoque loteestoque")
				.where("loteestoque <> ?", loteestoque, Util.objects.isPersistent(loteestoque))
				.where("lotematerial.material = ?", material)
				.where("loteestoque.fornecedor = ?", loteestoque.getFornecedor())
				.where("upper(loteestoque.numero) = upper(?)", loteestoque.getNumero())
				.unique() > 0;
	}
}
