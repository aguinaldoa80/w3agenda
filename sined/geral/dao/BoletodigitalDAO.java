package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Boletodigital;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BoletoDigitalRetornoBean;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class BoletodigitalDAO extends GenericDAO<Boletodigital>{

	public Boletodigital loadByDocumento(Documento documento) {
		return query()
					.where("boletodigital.documento = ?", documento)
					.unique();
	}

	public List<Boletodigital> findForRetornoBaixadas(BoletoDigitalRetornoBean bean) {
		return query()
				.select("boletodigital.cdboletodigital, boletodigital.id, boletodigital.status, boletodigital.nossonumero, " +
						"boletodigital.registrado, boletodigital.valorliquidado, boletodigital.valorpago, boletodigital.pagamento, " +
						"boletodigital.cancelamento, boletodigital.vencimento, " +
						"documento.cddocumento, documento.dtvencimento, documento.valor, " +
						"documentoacao.cddocumentoacao, documentoacao.nome, " +
						"pessoa.cdpessoa, pessoa.nome, " +
						"aux_documento.valoratual")
				.join("boletodigital.documento documento")
				.join("documento.pessoa pessoa")
				.join("documento.documentoacao documentoacao")
				.join("documento.aux_documento aux_documento")
				.where("boletodigital.pagamento is not null")
				.where("documento.documentoacao <> ?", Documentoacao.BAIXADA)
				.where("documento.empresa = ?", bean.getEmpresa())
				.where("documento.conta = ?", bean.getConta())
				.where("documento.contacarteira = ?", bean.getContacarteira())
				.list();
	}

}
