package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterial;
import br.com.linkcom.sined.geral.bean.Vendaorcamentosituacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VendaorcamentomaterialDAO extends GenericDAO<Vendaorcamentomaterial> {
	
	/**
	 * Busca os materiais de uma determinada vendaorcamento.
	 *
	 * @param vendaorcamento
	 * @return
	 * @author Ramon Brazil
	 */
	public List<Vendaorcamentomaterial> findByMaterial(Vendaorcamento vendaorcamento, String orderBy){
		if (vendaorcamento == null || vendaorcamento.getCdvendaorcamento() == null) {
			throw new SinedException("Vendaorcamento n�o pode ser nulo.");
		}
		QueryBuilder<Vendaorcamentomaterial> query = query()
				.select("vendaorcamentomaterial.cdvendaorcamentomaterial, vendaorcamentomaterial.ordem, material.nome, material.identificacao, vendaorcamentomaterial.preco, vendaorcamentomaterial.desconto, vendaorcamentomaterial.percentualdesconto, vendaorcamentomaterial.prazoentrega, material.cdmaterial, material.produto, material.servico, " +
						"material.extipi, material.ncmcompleto, material.codigobarras, ncmcapitulo.cdncmcapitulo, ncmcapitulo.descricao, unidademedida.simbolo, " +
						"material.patrimonio, material.epi, vendaorcamentomaterial.quantidade, vendaorcamentomaterial.multiplicador, unidademedida.cdunidademedida, unidademedida.nome, " +
						" material.producao, material.pesoliquidovalorvenda, material.peso, material.pesobruto, material.valorcusto, material.vendapromocional, material.tributacaoipi, " +
						"vendaorcamentomaterial.tipotributacaoicms, " +
						"vendaorcamentomaterial.tipocobrancaicms, vendaorcamentomaterial.valorbcicms, vendaorcamentomaterial.icms, " +
						"vendaorcamentomaterial.valoricms, vendaorcamentomaterial.valorbcicmsst, vendaorcamentomaterial.icmsst, " +
						"vendaorcamentomaterial.valoricmsst, vendaorcamentomaterial.reducaobcicmsst, vendaorcamentomaterial.margemvaloradicionalicmsst, " +
						"vendaorcamentomaterial.valorbcfcp, vendaorcamentomaterial.fcp, vendaorcamentomaterial.valorfcp, " +
						"vendaorcamentomaterial.valorbcfcpst, vendaorcamentomaterial.fcpst, vendaorcamentomaterial.valorfcpst, " +
						"vendaorcamentomaterial.percentualdesoneracaoicms, vendaorcamentomaterial.valordesoneracaoicms, vendaorcamentomaterial.abaterdesoneracaoicms, " +
						"vendaorcamentomaterial.valorbcdestinatario, vendaorcamentomaterial.valorbcfcpdestinatario, vendaorcamentomaterial.fcpdestinatario, " +
						"vendaorcamentomaterial.icmsdestinatario, vendaorcamentomaterial.icmsinterestadual, vendaorcamentomaterial.icmsinterestadualpartilha, " +
						"vendaorcamentomaterial.valorfcpdestinatario, vendaorcamentomaterial.valoricmsdestinatario, vendaorcamentomaterial.valoricmsremetente, " +
						"vendaorcamentomaterial.difal, vendaorcamentomaterial.valordifal, vendaorcamentomaterial.valorSeguro, vendaorcamentomaterial.outrasdespesas, " +
						"cfop.cdcfop, " +
						"vendaorcamentomaterial.ncmcompleto, vendaorcamentomaterial.modalidadebcicms, vendaorcamentomaterial.modalidadebcicmsst, " +
						"vendaorcamentomaterial.saldo, vendaorcamentomaterial.qtdereferencia, vendaorcamentomaterial.valorimposto, vendaorcamentomaterial.valorMinimo, " +
						"unidademedidamaterial.cdunidademedida, unidademedidamaterial.nome, unidademedidamaterial.simbolo, vendaorcamentomaterial.opcional, vendaorcamentomaterial.valoripi, vendaorcamentomaterial.ipi, vendaorcamentomaterial.tipocobrancaipi, vendaorcamentomaterial.aliquotareaisipi, vendaorcamentomaterial.tipocalculoipi, grupotributacao.cdgrupotributacao, " + 
						"materialgrupo.cdmaterialgrupo, materialgrupo.nome, material.obrigarlote, material.kitflexivel, " +
						"materialmestregrade.cdmaterial, materialmestregrade.nome, materialmestregrade.identificacao," +
						"material.valorvendaminimo, material.valorvendamaximo, vendaorcamentomaterial.comprimento, vendaorcamentomaterial.altura, vendaorcamentomaterial.comprimentooriginal, " +
						"vendaorcamentomaterial.largura, vendaorcamentomaterial.fatorconversao, vendaorcamentomaterial.observacao, loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade," +
						"material.qtdeunidade, material.considerarvendamultiplos, vendaorcamentomaterial.valorcustomaterial, vendaorcamentomaterial.valorvendamaterial, material.valorcusto, material.valorvenda, material.metrocubicovalorvenda, material.pesoliquidovalorvenda," +
						"arquivo.cdarquivo, arquivoMestre.cdarquivo, materialmestre.cdmaterial, materialmestre.ncmcompleto, materialmestre.nome, materialmestre.producao, materialmestre.vendapromocional, materialmestre.kitflexivel, fornecedor.cdpessoa, fornecedor.nome, listaCaracteristica.cdmaterialcaracteristica, listaCaracteristica.nome, fornecedor.razaosocial, fornecedor.cnpj, fornecedor.cpf, fornecedor.tipopessoa," +
						"materialunidademedida.cdmaterialunidademedida, materialunidademedida.fracao, materialunidademedida.qtdereferencia, unidademedidaconversao.cdunidademedida, unidademedidaconversao.nome, unidademedidaconversao.simbolo, " +
						"vendaorcamentomaterialseparacao.cdvendaorcamentomaterialseparacao, vendaorcamentomaterialseparacao.quantidade, unidademedidaseparacao.cdunidademedida, unidademedidaseparacao.nome, unidademedidaseparacao.simbolo, "+
						"listaCaracteristicaMestre.cdmaterialcaracteristica, listaCaracteristicaMestre.nome, " +
						"materialmestregrupo.cdmaterialgrupo, materialmestregrupo.nome, vendaorcamentomaterial.identificadorinterno, " +
						"vendaorcamentomaterial.percentualcomissaoagencia, vendaorcamentomaterial.peso, comissionamento.cdcomissionamento, " +
						"localizacaoestoque.cdlocalizacaoestoque, localizacaoestoque.descricao, " +
						"ncmcapituloItem.cdncmcapitulo, "+
						"materialFaixaMarkup.cdMaterialFaixaMarkup, " +
						"faixaMarkupNome.nome, faixaMarkupNome.cor, faixaMarkupNome.cdFaixaMarkupNome," +
						"materialtipo.cdmaterialtipo, materialcategoria.cdmaterialcategoria ")
				.join("vendaorcamentomaterial.vendaorcamento vendaorcamento")
				.join("vendaorcamentomaterial.material material")
				.leftOuterJoin("material.materialtipo materialtipo")
				.leftOuterJoin("material.materialcategoria materialcategoria")
				.leftOuterJoin("vendaorcamentomaterial.materialmestre materialmestre")
				.leftOuterJoin("material.unidademedida unidademedidamaterial")
				.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
				.leftOuterJoin("vendaorcamentomaterial.ncmcapitulo ncmcapituloItem")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("materialmestre.materialgrupo materialmestregrupo")
				.leftOuterJoin("material.localizacaoestoque localizacaoestoque")
				.join("vendaorcamentomaterial.unidademedida unidademedida")
				.leftOuterJoin("vendaorcamentomaterial.loteestoque loteestoque")
				.leftOuterJoin("material.arquivo arquivo")
				.leftOuterJoin("materialmestre.arquivo arquivoMestre")
				.leftOuterJoin("vendaorcamentomaterial.fornecedor fornecedor")
				.leftOuterJoin("material.listaCaracteristica listaCaracteristica")
				.leftOuterJoin("materialmestre.listaCaracteristica listaCaracteristicaMestre")
				.leftOuterJoin("material.listaMaterialunidademedida materialunidademedida")
				.leftOuterJoin("materialunidademedida.unidademedida unidademedidaconversao")
				.leftOuterJoin("vendaorcamentomaterial.listaVendaorcamentomaterialseparacao vendaorcamentomaterialseparacao")
				.leftOuterJoin("vendaorcamentomaterialseparacao.unidademedida unidademedidaseparacao")
				.leftOuterJoin("vendaorcamentomaterial.grupotributacao grupotributacao")
				.leftOuterJoin("vendaorcamentomaterial.comissionamento comissionamento")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.leftOuterJoin("vendaorcamentomaterial.materialFaixaMarkup materialFaixaMarkup")
				.leftOuterJoin("vendaorcamentomaterial.faixaMarkupNome faixaMarkupNome")
				.leftOuterJoin("vendaorcamentomaterial.cfop cfop")
				.where("vendaorcamento = ?",vendaorcamento);
				
			if(orderBy != null && !"".equals(orderBy)){
				query.orderBy(orderBy);
			}else {
				query.orderBy("vendaorcamentomaterial.ordem, material.nome");
			}
			
			return query.list();
	}
	
	/**
	 * Busca a lista de venda material para o relat�rio de ordem de produ��o.
	 *
	 * @param vendaorcamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Vendaorcamentomaterial> findForOrdemProducao(Vendaorcamento vendaorcamento) {
		if (vendaorcamento == null || vendaorcamento.getCdvendaorcamento() == null) {
			throw new SinedException("Vendaorcamento n�o pode ser nulo.");
		}
		return 
			query()
				.select("vendaorcamentomaterial.cdvendaorcamentomaterial, vendaorcamentomaterial.quantidade, vendaorcamentomaterial.multiplicador, material.cdmaterial, " +
						"vendaorcamentomaterial.prazoentrega, unidademedida.cdunidademedida, unidademedida.simbolo, vendaorcamentomaterial.comprimento, vendaorcamentomaterial.comprimentooriginal, " +
						"vendaorcamentomaterial.altura, vendaorcamentomaterial.largura, material.identificacao, vendaorcamentomaterial.fatorconversao, vendaorcamentomaterial.observacao ")
				.leftOuterJoin("vendaorcamentomaterial.vendaorcamento vendaorcamento")
				.leftOuterJoin("vendaorcamentomaterial.material material")
				.leftOuterJoin("vendaorcamentomaterial.unidademedida unidademedida")
				.where("vendaorcamento = ?", vendaorcamento)
				.list();
	}
	
	/**
	 * M�todo que busca os itens do or�amento
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 16/01/2014
	 */
	public List<Vendaorcamentomaterial> findForMontarGrade(String whereIn) {
		return query()
			.select("vendaorcamentomaterial.cdvendaorcamentomaterial, vendaorcamentomaterial.quantidade, " +
					"material.cdmaterial, material.nome, material.identificacao, materialmestregrade.cdmaterial," +
					"vendaorcamentomaterial.altura, vendaorcamentomaterial.largura," +
					"vendaorcamentomaterial.comprimento, vendaorcamentomaterial.comprimentooriginal, " +
					"vendaorcamentomaterial.preco, vendaorcamentomaterial.multiplicador ")
			.join("vendaorcamentomaterial.material material")
			.leftOuterJoin("material.materialmestregrade materialmestregrade")
			.whereIn("vendaorcamentomaterial.cdvendaorcamentomaterial", whereIn)
			.list();
	}

	/**
	 * M�todo que remove todos os materiais da vendaorcamento
	 * 
	 * @param vendaorcamento
	 * @author Tom�s Rabelo
	 * 
	 */
	public void deleteAllFromVendaorcamento(Vendaorcamento vendaorcamento) {
		if(vendaorcamento == null || vendaorcamento.getCdvendaorcamento() == null)
			throw new SinedException("Par�metros inv�lidos.");
		getJdbcTemplate().execute("delete from vendaorcamentomaterial where cdvendaorcamento = "+vendaorcamento.getCdvendaorcamento());
	}
	
	/**
	 * Busca uma lista de vendaorcamentohistorico para determinado cliente de acordo com o material requisitado.
	 * 
	 * @param cliente,material
	 * @return
	 * @since 12/04/2012
	 * @author Rafael Salvio
	 */
	public List<Vendaorcamentomaterial> getUltimoValorMaterialByCliente(Material material, Cliente cliente, Empresa empresa){
		if(cliente == null || cliente.getCdpessoa() == null || material == null || material.getCdmaterial() == null){
			throw new SinedException("Par�metros n�o podem ser nulos.");
		}
		return query()
				.select("material.cdmaterial, vendaorcamentomaterial.preco, vendaorcamentomaterial.desconto, vendaorcamento.dtorcamento," +
						"unidademedida.cdunidademedida, vendaorcamentomaterial.qtdereferencia, vendaorcamentomaterial.fatorconversao, " +
						"unidademedidaVOM.cdunidademedida ")
				.from(Vendaorcamentomaterial.class,"vendaorcamentomaterial")
				.join("vendaorcamentomaterial.material material")
				.join("material.unidademedida unidademedida")
				.join("vendaorcamentomaterial.vendaorcamento vendaorcamento")
				.join("vendaorcamentomaterial.unidademedida unidademedidaVOM")
				.where("vendaorcamento.cliente = ?", cliente)
				.where("vendaorcamento.empresa = ?", empresa)
				.where("vendaorcamentomaterial.material = ?", material)
				.where("vendaorcamentomaterial.materialmestre is null")
				.where("vendaorcamento.vendaorcamentosituacao <> ?",Vendaorcamentosituacao.CANCELADA)
				.orderBy("vendaorcamento.dtorcamento desc, vendaorcamento.cdvendaorcamento desc")
				.list();
	}

	/**
	 * M�todo que busca a vendaorcamentomaterial com os dados necess�rios para gerar a comiss�o do representante
	 *
	 * @param vendaorcamento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Vendaorcamentomaterial> findForComissaoRepresentante(Vendaorcamento vendaorcamento) {
		if(vendaorcamento == null || vendaorcamento.getCdvendaorcamento() == null)
			throw new SinedException("Vendaorcamento n�o pode ser nula.");
		
		return query()
				.select("vendaorcamentomaterial.cdvendaorcamentomaterial, material.cdmaterial, listaFornecedor.cdmaterialfornecedor, " +
						"fornecedor.cdpessoa, vendaorcamentomaterialfornecedor.cdpessoa, vendaorcamentomaterialfornecedor.nome, vendaorcamentomaterial.quantidade, vendaorcamentomaterial.multiplicador, vendaorcamentomaterial.preco, vendaorcamentomaterial.desconto, vendaorcamento.cdvendaorcamento, empresa.cdpessoa, " +
						"materialgrupo.cdmaterialgrupo ")
				.join("vendaorcamentomaterial.vendaorcamento vendaorcamento")
				.join("vendaorcamentomaterial.material material")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("material.listaFornecedor listaFornecedor")
				.leftOuterJoin("listaFornecedor.fornecedor fornecedor")
				.leftOuterJoin("vendaorcamentomaterial.fornecedor vendaorcamentomaterialfornecedor")
				.join("vendaorcamento.empresa empresa")
				.where("vendaorcamento  = ?", vendaorcamento)
				.list();
	}
	
	public boolean haveVendaOrcamentoMaterial(Material material) {
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Vendaorcamentomaterial.class)
					.where("vendaorcamentomaterial.cdmaterial = ?", material.getCdmaterial());
		return query.unique() > 0;
	}
}