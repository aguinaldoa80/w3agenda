package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorsituacao;
import br.com.linkcom.sined.geral.bean.Comunicado;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.ComunicadoFiltro;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.filter.EventoservicointernoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ComunicadoDAO extends GenericDAO<Comunicado>{
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listadestinatario");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Comunicado> query) {
		query.
		select("comunicado.cdcomunicado, comunicado.assunto, remetente.nome," +
				"remetente.cdpessoa, comunicado.descricao, comunicado.dtenvio, comunicado.dtaltera," +
				"comunicado.cdusuarioaltera, listadestinatario.cdcomunicadodestinatario," +
				"colaborador.cdpessoa, colaborador.nome, listadestinatario.email")
				.join("comunicado.remetente remetente")
				.leftOuterJoin("comunicado.listadestinatario listadestinatario")
				.leftOuterJoin("listadestinatario.colaborador colaborador");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Comunicado> query,	FiltroListagem _filtro) {
		ComunicadoFiltro filtro = (ComunicadoFiltro) _filtro;
		Timestamp fim=null;
		if(filtro.getFim()!=null){			
			fim = new Timestamp(SinedDateUtils.incrementDate(filtro.getFim(), 1, Calendar.DATE).getTime());
			fim.setTime(fim.getTime()-(86400000));
		}
		
		Colaborador destinatario = filtro.getDestinatario();

		query
		   .select("comunicado.cdcomunicado, comunicado.assunto, remetente.nome," +
				"remetente.cdpessoa, comunicado.dtenvio")
				.join("comunicado.remetente remetente")
				.join("comunicado.listadestinatario destinatario")
				.join("destinatario.colaborador colaborador")
				.leftOuterJoin("colaborador.colaboradorsituacao colaboradorsituacao")
				.where("comunicado.cdcomunicado=?", filtro.getNumero())
				.where("comunicado.remetente=?", filtro.getRemetente())
				.whereLikeIgnoreAll("comunicado.assunto", filtro.getAssunto())
				.where("comunicado.dtenvio>=?", filtro.getInicio())
				.where("comunicado.dtenvio<=?", fim)
				.where("colaborador = ?", destinatario)
				.where("colaboradorsituacao.cdcolaboradorsituacao <> ?", Colaboradorsituacao.DEMITIDO)
				.orderBy("comunicado.cdcomunicado desc");
		
				
	}
	
	/**
	 * Fun��o para a gera��o do relat�rio de eventos
	 * @param filtro
	 * @return
	 * @author C�ntia Nogueira
	 */
	public List<Comunicado> findForFiltroEvento(EventoservicointernoFiltro filtro){
		if(filtro==null){
			throw new SinedException("O filtro n�o pode ser nulo em ComunicadoDAO");
		}
		return querySined()
				
		.select("comunicado.cdcomunicado,comunicado.dtenvio, comunicado.assunto, remetente.cdpessoa," +
				"remetente.nome, destinatario.nome, destinatario.cdpessoa, remetente.email")
				.join("comunicado.remetente remetente")
				.leftOuterJoin("comunicado.listadestinatario listadestinatario")
				.leftOuterJoin("listadestinatario.colaborador destinatario")
				.whereLikeIgnoreAll("comunicado.assunto", filtro.getAssunto())
				.where("comunicado.dtenvio>=?", filtro.getInicio())
				.where("comunicado.dtenvio<=?", filtro.getFim())
				.whereLikeIgnoreAll("remetente.nome", filtro.getRemetente())
				.whereLikeIgnoreAll("destinatario.nome", filtro.getDestinatario())	
				.orderBy("comunicado.dtenvio asc")
				.list();
	}

}
