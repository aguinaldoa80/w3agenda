package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitementregamaterial;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotafiscalprodutoitementregamaterialDAO extends GenericDAO<Notafiscalprodutoitementregamaterial>{
	
	@Deprecated
	public Notafiscalprodutoitementregamaterial loadByNotafiscalprodutoitem(Notafiscalprodutoitem notafiscalprodutoitem){
		return query()
		.select("notafiscalprodutoitementregamaterial.cdnotafiscalprodutoitementregamaterial, " +
				"notafiscalprodutoitem.cdnotafiscalprodutoitem, entregamaterial.cdentregamaterial," +
				"entregadocumento.numero, notafiscalproduto.cdNota")
				.join("notafiscalprodutoitementregamaterial.notafiscalprodutoitem notafiscalprodutoitem")				
				.join("notafiscalprodutoitementregamaterial.entregamaterial entregamaterial")
				.join("notafiscalprodutoitem.notafiscalproduto notafiscalproduto")
				.join("entregamaterial.entregadocumento entregadocumento")
				.where("notafiscalprodutoitem = ?", notafiscalprodutoitem)
				.unique();
	}
	
	public List<Notafiscalprodutoitementregamaterial> loadListaByNotafiscalprodutoitem(Notafiscalprodutoitem notafiscalprodutoitem){
		return query()
		.select("notafiscalprodutoitementregamaterial.cdnotafiscalprodutoitementregamaterial, " +
				"notafiscalprodutoitem.cdnotafiscalprodutoitem, entregamaterial.cdentregamaterial," +
				"entregadocumento.numero, notafiscalproduto.cdNota")
				.join("notafiscalprodutoitementregamaterial.notafiscalprodutoitem notafiscalprodutoitem")
				.join("notafiscalprodutoitem.notafiscalproduto notafiscalproduto")
				.join("notafiscalprodutoitementregamaterial.entregamaterial entregamaterial")
				.join("entregamaterial.entregadocumento entregadocumento")
				.where("notafiscalprodutoitem = ?", notafiscalprodutoitem)
				.list();
	}
}
