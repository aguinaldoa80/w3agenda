package br.com.linkcom.sined.geral.dao;

import java.util.Date;

import br.com.linkcom.sined.geral.bean.ConsultaEventoCorreios;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ConsultaEventoCorreiosDAO extends GenericDAO<ConsultaEventoCorreios>{

	public ConsultaEventoCorreios loadByData(String objeto, Date data){
		return querySined()
				.where("objeto = ?", objeto)
				.where("dtConsulta = ?", data)
				.setMaxResults(1)
				.unique();
	}
	
	
	public void updateConsultaManual(String objeto, Date data){
		getHibernateTemplate().bulkUpdate("update ConsultaEventoCorreios cec set cec.consultaManualRealizada = true where cec.objeto = ? and cec.dtConsulta = ?", new Object[]{objeto, data});
	}
}
