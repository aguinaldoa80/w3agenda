package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendahistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PedidovendahistoricoDAO extends GenericDAO<Pedidovendahistorico>{

	/**
	 * Busca a lista de histórico a partir de um pedido de vanda.
	 *
	 * @param pedidovenda
	 * @return
	 * @since 07/11/2011
	 * @author Rodrigo Freitas
	 */
	public List<Pedidovendahistorico> findByPedidovenda(Pedidovenda pedidovenda) {
		return query()
					.where("pedidovendahistorico.pedidovenda = ?", pedidovenda)
					.orderBy("pedidovendahistorico.dtaltera desc")
					.list();
	}

	public List<Pedidovendahistorico> findForAndroid(String whereIn, String whereInPedidovenda) {
		return query()
		.select("pedidovendahistorico.cdpedidovendahistorico, pedidovendahistorico.acao, pedidovendahistorico.observacao, pedidovendahistorico.dtaltera, " +
				"pedidovendahistorico.cdusuarioaltera, pedidovenda.cdpedidovenda")
		.join("pedidovendahistorico.pedidovenda pedidovenda")
		.whereIn("pedidovendahistorico.cdpedidovendahistorico", whereIn)
		.whereIn("pedidovenda.cdpedidovenda", whereInPedidovenda)
		.list();
	}

	public boolean isExistsHistoricoAprovacao(Pedidovenda bean){
		boolean exists = Util.collections.isListNotEmpty(querySined()
							.join("pedidovendahistorico.pedidovenda pedidovenda")
							.whereEqualIgnoreAll("pedidovendahistorico.observacao", Pedidovendahistorico.VISUALIZADO)
							.where("pedidovenda = ?", bean)
							.list());
		return exists;
	}
}
