package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Regimecontratacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("regimecontratacao.nome")
public class RegimecontratacaoDAO extends GenericDAO<Regimecontratacao> {
	
	/* singleton */
	private static RegimecontratacaoDAO instance;
	public static RegimecontratacaoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(RegimecontratacaoDAO.class);
		}
		return instance;
	}
		
}
