package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Pessoaconfiguracao;
import br.com.linkcom.sined.geral.bean.enumeration.Pessoaconfiguracaotipo;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PessoaconfiguracaoDAO extends GenericDAO<Pessoaconfiguracao> {

	public Pessoaconfiguracao loadPessoaconfiguracao(Pessoaconfiguracao pessoaconfiguracao) {
		if(pessoaconfiguracao == null || pessoaconfiguracao.getCdpessoaconfiguracao() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("pessoaconfiguracao.cdpessoaconfiguracao, pessoaconfiguracao.tipo, " +
						"pessoa.cdpessoa, pessoa.nome, " +
						"transportador.cdpessoa, transportador.nome, transportador.razaosocial")
				.join("pessoaconfiguracao.pessoa pessoa")
				.leftOuterJoin("pessoaconfiguracao.transportador transportador")
				.where("pessoaconfiguracao = ?", pessoaconfiguracao)
				.unique();
	}
	
	public Pessoaconfiguracao loadPessoaconfiguracaoByPessoa(Pessoa pessoa, Pessoaconfiguracaotipo tipo) {
		if(pessoa == null || pessoa.getCdpessoa() == null || tipo == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("pessoaconfiguracao.cdpessoaconfiguracao, pessoaconfiguracao.tipo, " +
						"pessoa.cdpessoa, pessoa.nome, " +
						"transportador.cdpessoa, transportador.nome, transportador.razaosocial")
				.join("pessoaconfiguracao.pessoa pessoa")
				.leftOuterJoin("pessoaconfiguracao.transportador transportador")
				.where("pessoaconfiguracao.pessoa = ?", pessoa)
				.where("pessoaconfiguracao.tipo = ?", tipo)
				.unique();
	}	
	
}
