package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.sined.geral.bean.Materialvenda;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoitemsped;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Sugestaovenda;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialvendaDAO extends GenericDAO<Materialvenda> {

	/**
	 * Busca as sugest�es para as telas de pedido de venda e venda.
	 *
	 * @param codigos
	 * @return
	 * @since 01/08/2012
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<Sugestaovenda> findForSugestao(String codigos) {
		if (codigos!=null){
			codigos = codigos.replaceAll("[a-z]", "");
			codigos = codigos.replaceAll("[A-Z]", "");
		}		
		SinedUtil.markAsReader();
		return getJdbcTemplate().query(
				"select m2.cdmaterial, m2.nome as nome_material, m2.valorvenda, um.cdunidademedida, um.nome as nome_unidademedida, " +
				" m2.valorvendaminimo, m2.valorvendamaximo, m2.identificacao, m2.vendapromocional, m2.valorcusto, " +
				" m2.qtdeunidade, m2.considerarvendamultiplos, m2.peso, m2.pesobruto, m2.obrigarlote " +

				"from material m " +
				
				"join materialvenda mv on mv.cdmaterial = m.cdmaterial " +
				"join material m2 on m2.cdmaterial = mv.cdmaterialvendaitem " +
				"left outer join unidademedida um on um.cdunidademedida = m2.cdunidademedida " +
				
				"where m.cdmaterial in (" + codigos + ") " +
				"and m2.cdmaterial not in (" + codigos + ") " + 
				"and (m2.tipoitemsped is null or m2.tipoitemsped <> " + Tipoitemsped.USO_CONSUMO.ordinal() + ") ", 
			new RowMapper() {
				public Sugestaovenda mapRow(ResultSet rs, int rowNum) throws SQLException {
					int cdmaterial = rs.getInt("cdmaterial");
					String nome_material = rs.getString("nome_material");
					String identificacao = rs.getString("identificacao");
					boolean vendapromocional = rs.getBoolean("vendapromocional");
					
					int cdunidademedida = rs.getInt("cdunidademedida");
					String nome_unidademedida = rs.getString("nome_unidademedida"); 
					
					Double valorvenda = rs.getDouble("valorvenda");
					Double valorcustomaterial = rs.getDouble("valorcusto");
					Double valorvendaminimo = rs.getDouble("valorvendaminimo");
					Double valorvendamaximo = rs.getDouble("valorvendamaximo");
					Integer qtdeunidade = rs.getInt("qtdeunidade");
					Boolean considerarvendamultiplos = rs.getBoolean("considerarvendamultiplos");
					Double peso = rs.getDouble("peso");
					Double pesobruto = rs.getDouble("pesobruto");
					Boolean obrigarLote = rs.getBoolean("obrigarlote");
					
					Sugestaovenda sugestaovenda = new Sugestaovenda();
					sugestaovenda.setCdmaterial(cdmaterial);
					sugestaovenda.setNome_material(nome_material);
					sugestaovenda.setIdentificacao_material(identificacao);
					sugestaovenda.setQtde(1.0);
					sugestaovenda.setCdunidademedida(cdunidademedida);
					sugestaovenda.setNome_unidademedida(nome_unidademedida);
					sugestaovenda.setValor(valorvenda);
					sugestaovenda.setValorvendamaximo(valorvendamaximo);
					sugestaovenda.setValorvendaminimo(valorvendaminimo);
					sugestaovenda.setVendapromocional(vendapromocional);
					sugestaovenda.setQtdeunidade(qtdeunidade);
					sugestaovenda.setConsiderarvendamultiplos(considerarvendamultiplos);
					sugestaovenda.setValorcustomaterial(valorcustomaterial);
					sugestaovenda.setValorvendamaterial(valorvenda);
					sugestaovenda.setPeso(peso);
					sugestaovenda.setPesobruto(pesobruto);
					sugestaovenda.setMaterialObrigaLote(obrigarLote);
					return sugestaovenda;
				}
			});
	}

	/**
	 * Busca os dados para o cache da tela de pedido de venda offline
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Materialvenda> findForPVOffline() {
		return query()
				.select("materialvenda.cdmaterialvenda, material.cdmaterial, materialvendaitem.cdmaterial, materialvendaitem.nome, " +
						"materialvendaitem.valorvenda, materialvendaitem.valorvendaminimo, materialvendaitem.valorvendamaximo, " +
						"materialvendaitem.vendapromocional, unidademedida.cdunidademedida, unidademedida.nome ")
				.join("materialvenda.material material")
				.join("materialvenda.materialvendaitem materialvendaitem")
				.leftOuterJoin("materialvendaitem.unidademedida unidademedida")
				.list();
	}
}