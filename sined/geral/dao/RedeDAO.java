package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Rede;
import br.com.linkcom.sined.geral.bean.Servidorinterface;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.RedeFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RedeDAO extends GenericDAO<Rede>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Rede> query, FiltroListagem _filtro) {
		RedeFiltro filtro = (RedeFiltro)_filtro;
		query.select("rede.cdrede, rede.enderecoip, bandaredecomercial.nome, rede.ativo")
			 .join("rede.bandaredecomercial bandaredecomercial")
			 .where("rede.enderecoip = ?", filtro.getEnderecoip())
			 .where("rede.bandaredecomercial = ?", filtro.getBandaredecomercial());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Rede> query) {
		query.leftOuterJoinFetch("rede.bandaredecomercial bandaredecomercial")
			 .leftOuterJoinFetch("rede.redemascara redemascara")
			 .leftOuterJoinFetch("rede.listaRedeip listaRedeip")
			 .leftOuterJoinFetch("rede.listaServidorinterface listaServidorinterface")
			 .leftOuterJoinFetch("listaServidorinterface.servidorinterface servidorinterface")
			 .leftOuterJoinFetch("servidorinterface.servidor servidor")
			 .orderBy("listaRedeip.ip");
			 
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaServidorinterface");
		save.saveOrUpdateManaged("listaRedeip");
	}
	
	/**
	 * Retorna uma lista de redes de acordo com a interface
	 * @param cliente
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Rede> findByInterface(Servidorinterface servidorinterface){
		List<Rede> list = query().select("distinct rede.cdrede, rede.enderecoip")
								 .leftOuterJoin("rede.listaServidorinterface listaServidorinterface")
								 .where("listaServidorinterface.servidorinterface = ?", servidorinterface)
								 .list();		
		return list;
	}
	
	public List<Rede> findByInterfaceAndContratoMaterial(Servidorinterface servidorinterface, Contratomaterial contratomaterial){
		QueryBuilder<Rede> query = query().select("distinct rede.cdrede, rede.enderecoip")
								 .leftOuterJoin("rede.listaServidorinterface listaServidorinterface")
								 .where("listaServidorinterface.servidorinterface = ?", servidorinterface);
		
		if(contratomaterial != null){
			query.join("rede.bandaredecomercial bandaredecomercial")
				.join("bandaredecomercial.listaMaterial listaMaterial")
				.join("listaMaterial.material material")
				.join("material.listaContratomaterial contratomaterial")
				.where("contratomaterial = ?", contratomaterial);
		}
		
		return  query.list();
		
	}
}
