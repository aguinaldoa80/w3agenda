package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Arquivoponto;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ArquivopontoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ArquivopontoDAO extends GenericDAO<Arquivoponto>{

	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Arquivoponto> query, FiltroListagem _filtro) {
		
		ArquivopontoFiltro filtro = (ArquivopontoFiltro) _filtro;
		
		query
			.select("arquivoponto.cdarquivoponto, arquivo.cdarquivo, arquivo.nome, arquivosalario.cdarquivo, arquivosalario.nome, " +
					"arquivoponto.dtinicio, arquivoponto.dtfim, arquivoponto.processado, arquivoponto.observacao")
			.join("arquivoponto.arquivo arquivo")
			.leftOuterJoin("arquivoponto.arquivosalario arquivosalario")
			.where("arquivoponto.projeto = ?", filtro.getProjeto())
			.where("arquivoponto.usuarioinsercao = ?", filtro.getUsuarioinsercao())
			.where("arquivoponto.usuarioprocessado = ?", filtro.getUsuarioprocessado())
			.where("arquivoponto.processado = ?", filtro.getProcessado())
			;
		
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Arquivoponto> query) {
		query
			.leftOuterJoinFetch("arquivoponto.arquivosalario arquivosalario")
			.leftOuterJoinFetch("arquivoponto.arquivo arquivo")
			.leftOuterJoinFetch("arquivoponto.usuarioinsercao usuarioinsercao")
			.leftOuterJoinFetch("arquivoponto.usuarioprocessado usuarioprocessado")
			;
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				Arquivoponto arquivoponto = (Arquivoponto)save.getEntity();
				
				if(arquivoponto.getArquivo() != null)
					arquivoDAO.saveFile(save.getEntity(), "arquivo");
				
				if(arquivoponto.getArquivosalario() != null)
					arquivoDAO.saveFile(save.getEntity(), "arquivosalario");
				
				return null;
			}			
		});		
	}

	/**
	 * Busca a lista de Arquivoponto para o processamento do Arquivo.
	 *
	 * @param whereIn
	 * @return
	 * @since Jun 10, 2011
	 * @author Rodrigo Freitas
	 */
	public List<Arquivoponto> findForProcessamento(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Parâmetros inválidos.");
		}
		return query()
					.select("arquivoponto.cdarquivoponto, arquivoponto.processado, arquivo.cdarquivo, " +
							"projeto.cdprojeto, projeto.nome, arquivosalario.cdarquivo")
					.join("arquivoponto.arquivo arquivo")
					.leftOuterJoin("arquivoponto.arquivosalario arquivosalario")
					.leftOuterJoin("arquivoponto.projeto projeto")
					.whereIn("arquivoponto.cdarquivoponto", whereIn)
					.list();
	}

	/**
	 * Atualiza as informações de um arquivo de ponto para o processamento.
	 *
	 * @param ids
	 * @since Jun 10, 2011
	 * @author Rodrigo Freitas
	 */
	public void updateProcessamento(String ids) {
		getHibernateTemplate().bulkUpdate("update Arquivoponto a set a.dtprocessamento = ?, a.usuarioprocessado = ?, a.processado = ? where a.id in (" + ids + ")", 
			new Object[]{new Timestamp(System.currentTimeMillis()), SinedUtil.getUsuarioLogado(), Boolean.TRUE});
	}
}
