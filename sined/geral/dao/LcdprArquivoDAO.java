package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.LcdprArquivo;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.LcdprArquivoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class LcdprArquivoDAO extends GenericDAO<LcdprArquivo> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<LcdprArquivo> query, FiltroListagem _filtro) {
		LcdprArquivoFiltro filtro = (LcdprArquivoFiltro) _filtro;
		
		query
			.select("lcdprArquivo.cdLcdprArquivo, colaborador.nome, colaborador.cdpessoa, lcdprArquivo.dtinicio, lcdprArquivo.dtfim, " +
					"arquivo.nome, arquivo.cdarquivo")
			.leftOuterJoin("lcdprArquivo.colaborador colaborador")
			.leftOuterJoin("lcdprArquivo.arquivo arquivo")
			.where("lcdprArquivo.dtinicio >= ?", filtro.getDtinicio())
			.where("lcdprArquivo.dtfim <= ?", filtro.getDtfim())
			.where("colaborador = ?", filtro.getColaborador())
			.orderBy("lcdprArquivo.dtinicio");
	}

	public LcdprArquivo findLcdprArquivo(LcdprArquivoFiltro filtro) {
		return query()
				.select("lcdprArquivo.cdLcdprArquivo, colaborador.cdpessoa")
				.leftOuterJoin("lcdprArquivo.colaborador colaborador")
				.where("colaborador = ?", filtro.getColaborador())
				.where("lcdprArquivo.dtinicio = ?", filtro.getDtinicio())
				.where("lcdprArquivo.dtfim = ?", filtro.getDtfim())
				.unique();
	}
}
