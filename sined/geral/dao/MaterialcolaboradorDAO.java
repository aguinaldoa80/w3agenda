package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Materialcolaborador;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialcolaboradorDAO extends GenericDAO<Materialcolaborador> {

	public List<Materialcolaborador> findMaterialcolaboradorByVenda(String whereIn, Colaborador colaborador) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("material.cdmaterial, materialcolaborador.cdmaterialcolaborador, colaborador.cdpessoa, materialcolaborador.percentual, " +
						"materialcolaborador.valor, documentotipo.cddocumentotipo, materialgrupo.cdmaterialgrupo, " +
						"listaMaterialgrupocomissaovenda.cdmaterialgrupocomissaovenda, cargo.cdcargo, documentotipo2.cddocumentotipo, " +
						"comissionamento.cdcomissionamento, comissionamento.comissionamentotipo, colaborador2.cdpessoa, " +
						"comissionamento.deduzirvalor, comissionamento.quantidade, comissionamento.percentual, comissionamento.valor, " +
						"listaComissionamentopessoa.cdcomissionamentopessoa, colaborador.cdpessoa, colaborador.nome, " +
						"listaComissionamentopessoa.recebecomissao, comissionamento.tipobccomissionamento, " +
						"comissionamento.considerardiferencapagamento, comissionamento.criteriocomissionamento")
				.join("materialcolaborador.material material")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("materialcolaborador.colaborador colaborador")
				.leftOuterJoin("materialcolaborador.documentotipo documentotipo")
				.leftOuterJoin("materialgrupo.listaMaterialgrupocomissaovenda listaMaterialgrupocomissaovenda")
				.leftOuterJoin("listaMaterialgrupocomissaovenda.cargo cargo")
				.leftOuterJoin("listaMaterialgrupocomissaovenda.documentotipo documentotipo2")
				.leftOuterJoin("listaMaterialgrupocomissaovenda.comissionamento comissionamento")
				.leftOuterJoin("comissionamento.listaComissionamentopessoa listaComissionamentopessoa")
				.leftOuterJoin("listaComissionamentopessoa.colaborador colaborador2")
				.openParentheses()
				.where("colaborador = ?", colaborador).or()
				.where("colaborador is null")				
				.closeParentheses()
				.whereIn("material.cdmaterial", whereIn)
				.list();
	}
}
