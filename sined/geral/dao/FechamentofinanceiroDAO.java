package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fechamentofinanceiro;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.FechamentofinanceiroFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FechamentofinanceiroDAO extends GenericDAO<Fechamentofinanceiro> {

	@Override
	public void updateListagemQuery(QueryBuilder<Fechamentofinanceiro> query, FiltroListagem _filtro) {
		FechamentofinanceiroFiltro filtro = (FechamentofinanceiroFiltro) _filtro;
		query
			.select("fechamentofinanceiro.cdfechamentofinanceiro, fechamentofinanceiro.datainicio, fechamentofinanceiro.datalimite, " +
					"fechamentofinanceiro.motivo, fechamentofinanceiro.dtaltera, pessoa.nome, fechamentofinanceiro.ativo")
			.leftOuterJoin("fechamentofinanceiro.responsavel pessoa")
			.where("fechamentofinanceiro.empresa = ?", filtro.getEmpresa())
			.where("fechamentofinanceiro.contatipo = ?", filtro.getContatipo())
			.where("fechamentofinanceiro.conta = ?", filtro.getConta())
			.where("fechamentofinanceiro.datalimite >= ?", filtro.getDatalimiteDe())
			.where("fechamentofinanceiro.datalimite <= ?", filtro.getDatalimiteAte())
			.where("fechamentofinanceiro.ativo = ?", filtro.getAtivo())
			.orderBy("fechamentofinanceiro.datalimite DESC");
		
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		if(!administrador){
			query
				.leftOuterJoinIfNotExists("fechamentofinanceiro.conta conta")
				.leftOuterJoinIfNotExists("conta.listaContapapel listaContapapel");
		
			if (whereInPapel != null && !whereInPapel.equals("")){
				query.openParentheses();
					query.whereIn("listaContapapel.papel.cdpapel", whereInPapel)
					.or()
					.where("listaContapapel.papel.cdpapel is null");
				query.closeParentheses();
			} else {
				query.where("listaContapapel.papel.cdpapel is null");
			}
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Fechamentofinanceiro> query) {
		query.leftOuterJoinFetch("fechamentofinanceiro.empresa empresa");
		query.leftOuterJoinFetch("fechamentofinanceiro.contatipo contatipo");
		query.leftOuterJoinFetch("fechamentofinanceiro.conta conta");
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaFechamentoFinanceiroHistorico");
	}
	
	/**
	 * M�todo que retorna a datalimite, cuja data � a mais atual, lan�ada para determinada empresa.
	 * 
	 * @param empresa
	 * @param conta 
	 * @return
	 */
	public Date loadUltimaDatalimiteByEmpresa(Empresa empresa, Conta conta){
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Par�metro inv�lido para empresa.");
		}
		
		return newQueryBuilderSined(Date.class)
				.setUseTranslator(false)
				.select("max(fechamentofinanceiro.datalimite)")
				.from(Fechamentofinanceiro.class)
				.where("fechamentofinanceiro.empresa = ?", empresa)
				.openParentheses()
					.where("fechamentofinanceiro.conta = ?", conta)
					.or()
					.where("fechamentofinanceiro.conta is null")
				.closeParentheses()
				.unique();
	}
	
	/**
	 * Busca a lista de fechamento financeiro
	 *
	 * @param empresa
	 * @param conta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 09/07/2014
	 */
	public List<Fechamentofinanceiro> findByEmpresaConta(List<Empresa> listaEmpresa, Conta conta){
		QueryBuilder<Fechamentofinanceiro> query = query()
					.select("fechamentofinanceiro.cdfechamentofinanceiro, fechamentofinanceiro.datalimite, fechamentofinanceiro.datainicio")
					.openParentheses()
						.where("fechamentofinanceiro.conta = ?", conta)
						.or()
						.where("fechamentofinanceiro.conta is null")
					.closeParentheses()
					.where("fechamentofinanceiro.ativo = ?", Boolean.TRUE);
		
		if(listaEmpresa != null && listaEmpresa.size() > 0){
			query.openParentheses();
			for (Empresa empresa : listaEmpresa) {
				query.where("fechamentofinanceiro.empresa = ?", empresa).or();
			}
			query.where("fechamentofinanceiro.empresa is null");
			query.closeParentheses();
		}
		
		return query.list();
	}
	
	/**
	 * M�todo que busca o fechamento financeiro anterior ao que vai ser salvo
	 * @param cdfechamentofinanceiro
	 * @return
	 */
	public Fechamentofinanceiro findFechamentoFinanceiroAntigoByCdFechamento(Integer cdfechamentofinanceiro) {		
		
		QueryBuilder<Fechamentofinanceiro> query = query();
		query
			.select("fechamentofinanceiro.ativo, fechamentofinanceiro.datainicio, fechamentofinanceiro.datalimite, conta.cdconta, conta.nome")
			.leftOuterJoin("fechamentofinanceiro.conta conta")
			.where("fechamentofinanceiro.cdfechamentofinanceiro = ?", cdfechamentofinanceiro);
		
		return query.unique();
	}

	/**
	 * Verifica a duplicidade de fechamento financeiro
	 *
	 * @param fechamentofinanceiro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 24/03/2015
	 */
	public boolean haveFechamento(Fechamentofinanceiro fechamentofinanceiro) {
		QueryBuilder<Long> query = newQueryBuilderWithFrom(Long.class);
		
		query
			.select("count(*)")
			.where("fechamentofinanceiro.datainicio = ?", fechamentofinanceiro.getDatainicio())
			.where("fechamentofinanceiro.datalimite = ?", fechamentofinanceiro.getDatalimite())
			.where("fechamentofinanceiro.ativo = ?", Boolean.TRUE);
		
		if(fechamentofinanceiro.getCdfechamentofinanceiro() != null){
			query.where("fechamentofinanceiro.cdfechamentofinanceiro <> ?", fechamentofinanceiro.getCdfechamentofinanceiro());
		}
		
		if(fechamentofinanceiro.getEmpresa() != null){
			query.where("fechamentofinanceiro.empresa = ?", fechamentofinanceiro.getEmpresa());
		} else {
			query.where("fechamentofinanceiro.empresa is null");
		}
		
		if(fechamentofinanceiro.getContatipo() != null){
			query.where("fechamentofinanceiro.contatipo = ?", fechamentofinanceiro.getContatipo());
		} else {
			query.where("fechamentofinanceiro.contatipo is null");
		}
		
		if(fechamentofinanceiro.getConta() != null){
			query.where("fechamentofinanceiro.conta = ?", fechamentofinanceiro.getConta());
		} else {
			query.where("fechamentofinanceiro.conta is null");
		}
		
		return query.unique() > 0;
	}
	
	/**
	* M�todo que verifica a duplicidade de fechamento financeiro em que a data in�cio ou a data limite perten�a 
	* a um intervalo de datas de fechamento existente
	*
	* @param fechamentofinanceiro
	* @return
	* @since 12/05/2015
	* @author Luiz Fernando
	*/
	public boolean haveFechamentoPeriodo(Fechamentofinanceiro fechamentofinanceiro) {
		QueryBuilder<Long> query = newQueryBuilderWithFrom(Long.class);
		
		query
			.select("count(*)")
			.where("fechamentofinanceiro.ativo = ?", Boolean.TRUE)
			.openParentheses()
				.openParentheses()
					.where("fechamentofinanceiro.datainicio <= ?", fechamentofinanceiro.getDatainicio())
					.where("fechamentofinanceiro.datalimite >= ?", fechamentofinanceiro.getDatainicio())
				.closeParentheses()
				.or()
				.openParentheses()
					.where("fechamentofinanceiro.datainicio <= ?", fechamentofinanceiro.getDatalimite())
					.where("fechamentofinanceiro.datalimite >= ?", fechamentofinanceiro.getDatalimite())
				.closeParentheses()
				.openParentheses()
					.where("? <= fechamentofinanceiro.datainicio", fechamentofinanceiro.getDatainicio())
					.where("? >= fechamentofinanceiro.datainicio", fechamentofinanceiro.getDatalimite())
				.closeParentheses()
				.or()
				.openParentheses()
					.where("? <= fechamentofinanceiro.datalimite", fechamentofinanceiro.getDatainicio())
					.where("? >= fechamentofinanceiro.datalimite", fechamentofinanceiro.getDatalimite())
				.closeParentheses()
			.closeParentheses();
		
		if(fechamentofinanceiro.getCdfechamentofinanceiro() != null){
			query.where("fechamentofinanceiro.cdfechamentofinanceiro <> ?", fechamentofinanceiro.getCdfechamentofinanceiro());
		}
		
		if(fechamentofinanceiro.getEmpresa() != null){
			query.where("fechamentofinanceiro.empresa = ?", fechamentofinanceiro.getEmpresa());
		} else {
			query.where("fechamentofinanceiro.empresa is null");
		}
		
		if(fechamentofinanceiro.getContatipo() != null){
			query.where("fechamentofinanceiro.contatipo = ?", fechamentofinanceiro.getContatipo());
		} else {
			query.where("fechamentofinanceiro.contatipo is null");
		}
		
		if(fechamentofinanceiro.getConta() != null){
			query.where("fechamentofinanceiro.conta = ?", fechamentofinanceiro.getConta());
		} else {
			query.where("fechamentofinanceiro.conta is null");
		}
		
		return query.unique() > 0;
	}
	
	public boolean haveFechamentoPeriodo(Empresa empresa, Date datainicio, Date datalimite, Conta conta) {
		QueryBuilder<Long> query = newQueryBuilderWithFrom(Long.class);
		
		query
			.select("count(*)")
			.where("fechamentofinanceiro.ativo = ?", Boolean.TRUE)
			.where("fechamentofinanceiro.empresa = ?", empresa)
			.openParentheses()
				.openParentheses()
					.where("fechamentofinanceiro.datainicio <= ?", datainicio)
					.where("fechamentofinanceiro.datalimite >= ?", datainicio)
				.closeParentheses()
				.or()
				.openParentheses()
					.where("fechamentofinanceiro.datainicio <= ?", datalimite)
					.where("fechamentofinanceiro.datalimite >= ?", datalimite)
				.closeParentheses()
				.openParentheses()
					.where("? <= fechamentofinanceiro.datainicio", datainicio)
					.where("? >= fechamentofinanceiro.datainicio", datalimite)
				.closeParentheses()
				.or()
				.openParentheses()
					.where("? <= fechamentofinanceiro.datalimite", datainicio)
					.where("? >= fechamentofinanceiro.datalimite", datalimite)
				.closeParentheses()
			.closeParentheses()
			.where("fechamentofinanceiro.conta = ?", conta);		
		
		return query.unique() > 0;
	}

	public Boolean haveFechamentoGeralMesAnterior(Date data, Empresa empresa) {
		QueryBuilder<Long> query = newQueryBuilderWithFrom(Long.class);
		
		query
			.where("fechamentofinanceiro.contatipo is null")
			.where("fechamentofinanceiro.conta is null")
			.where("fechamentofinanceiro.empresa = ?", empresa)
			.openParentheses()
				.openParentheses()
					.where("date_part('MONTH', fechamentofinanceiro.datainicio) = ?", SinedDateUtils.getMes(data) - 1)
					.where("date_part('YEAR', fechamentofinanceiro.datainicio) = ?", SinedDateUtils.getAno(data))
				.closeParentheses()
				.or()
				.openParentheses()
					.where("date_part('MONTH', fechamentofinanceiro.datalimite) = ?", SinedDateUtils.getMes(data) - 1)
					.where("date_part('YEAR', fechamentofinanceiro.datalimite) = ?", SinedDateUtils.getAno(data))
				.closeParentheses()
			.closeParentheses();
		
		return query.list().size() > 0;
	}
	
	public Boolean haveFechamentoContaMesAnterior(Date data, Conta conta) {
		QueryBuilder<Long> query = newQueryBuilderWithFrom(Long.class);
		
		query
			.where("fechamentofinanceiro.conta = ?", conta)
			.openParentheses()
				.where("date_part('MONTH', fechamentofinanceiro.datainicio) = ?", SinedDateUtils.getMes(data) - 1)
				.where("date_part('YEAR', fechamentofinanceiro.datainicio) = ?", SinedDateUtils.getAno(data))
				.or()
				.where("date_part('MONTH', fechamentofinanceiro.datalimite) = ?", SinedDateUtils.getMes(data) - 1)
				.where("date_part('YEAR', fechamentofinanceiro.datalimite) = ?", SinedDateUtils.getAno(data))
			.closeParentheses();
		
		return query.list().size() > 0;
	}

	@Override
	public ListagemResult<Fechamentofinanceiro> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Fechamentofinanceiro> query = query();
		updateListagemQuery(query, filtro);
		return new ListagemResult<Fechamentofinanceiro>(query, filtro);
	}
}