package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Bandarederegra;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("bandarederegra.bandaredeupload")
public class BandarederegraDAO extends GenericDAO<Bandarederegra>{
	
}
