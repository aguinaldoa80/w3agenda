package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Sintegra;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SintegraFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class SintegraDAO extends GenericDAO<Sintegra> {

	@Override
	public void updateListagemQuery(QueryBuilder<Sintegra> query, FiltroListagem _filtro) {
		SintegraFiltro filtro = (SintegraFiltro) _filtro;
		
		if(filtro.getEmpresa() == null) query.where("1=0");
		
		query
			.select("sintegra.cdsintegra, empresa.nome, empresa.razaosocial, empresa.nomefantasia, sintegra.convenio, sintegra.naturezaoperacao, " +
					"sintegra.finalidadearquivo, sintegra.dtinicio, sintegra.dtfim, " +
					"arquivo.cdarquivo, arquivo.nome, sintegra.dtaltera")
			.leftOuterJoin("sintegra.arquivo arquivo")
			.leftOuterJoin("sintegra.empresa empresa")
			.where("empresa = ?", filtro.getEmpresa())
			.where("sintegra.dtinicio >= ?", filtro.getDtinicio())
			.where("sintegra.dtfim <= ?", filtro.getDtfim())
			.where("sintegra.convenio = ?", filtro.getConvenio())
			.where("sintegra.naturezaoperacao = ?", filtro.getNaturezaoperacao())
			.where("sintegra.finalidadearquivo = ?", filtro.getFinalidadearquivo())
			.orderBy("sintegra.dtinicio desc");
	}
	
	/**
	 * M�todo para deletar o arquivo SINTEGRA de acordo com par�metro
	 *
	 * @param empresa
	 * @param dtinicio
	 * @param dtfim
	 * @author Luiz Fernando
	 */
	public void deleteByEmpresaDtinicioDtfim(SintegraFiltro filtro) {
		if(filtro != null && filtro.getEmpresa() != null && filtro.getConvenio() != null &&
				filtro.getNaturezaoperacao() != null && filtro.getFinalidadearquivo() != null)
			getHibernateTemplate().bulkUpdate("delete from Sintegra s " +
					"where s.empresa.cdpessoa = ? and s.convenio = ? and s.naturezaoperacao = ? " +
					"and s.finalidadearquivo = ? and s.dtinicio = ? and s.dtfim = ?", 
					new Object[]{filtro.getEmpresa().getCdpessoa(), filtro.getConvenio(),
								 filtro.getNaturezaoperacao(), filtro.getFinalidadearquivo(),
								 filtro.getDtinicio(), filtro.getDtfim()});
	}
}
