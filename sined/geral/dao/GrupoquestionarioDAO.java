package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Grupoquestionario;
import br.com.linkcom.sined.geral.service.QuestionarioquestaoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.GrupoquestionarioFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class GrupoquestionarioDAO extends GenericDAO<Grupoquestionario> {

	QuestionarioquestaoService questionarioquestaoService;
	
	public void setQuestionarioquestaoService(QuestionarioquestaoService questionarioquestaoService) {
		this.questionarioquestaoService = questionarioquestaoService;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Grupoquestionario> query, FiltroListagem _filtro) {
		GrupoquestionarioFiltro filtro = (GrupoquestionarioFiltro) _filtro;
		query
			.select("grupoquestionario.cdgrupoquestionario, grupoquestionario.descricao, grupoquestionario.ativo ")
			.whereLikeIgnoreAll("grupoquestionario.descricao", filtro.getDescricao())
			.where("grupoquestionario.ativo = ?", filtro.getAtivo())
			.where("grupoquestionario.atividadetipo = ?", filtro.getAtividadetipo())
			.orderBy("grupoquestionario.cdgrupoquestionario");
	}	
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Grupoquestionario> query) {
		query
			.select("grupoquestionario.cdgrupoquestionario, grupoquestionario.descricao, grupoquestionario.mediapontuacao, grupoquestionario.ativo, " +
					"listaquestionarioquestao.cdquestionarioquestao, listaquestionarioquestao.questao, listaquestionarioquestao.pontuacaosim, " +
					"listaquestionarioquestao.pontuacaonao, listaquestionarioquestao.tiporesposta, " +
					"listaquestionarioquestao.pontuacaoquestao, listaquestionarioquestao.exibirquestaoordemcompra, listaquestionarioquestao.obrigarresposta, " +
					"listaquestionarioresposta.resposta, listaquestionarioresposta.pontuacao, listaquestionarioresposta.cdquestionarioresposta, " +
					"atividadetipo.nome, atividadetipo.cdatividadetipo")
			.leftOuterJoin("grupoquestionario.listaquestionarioquestao listaquestionarioquestao")
			.leftOuterJoin("grupoquestionario.atividadetipo atividadetipo")
			.leftOuterJoin("listaquestionarioquestao.listaquestionarioresposta listaquestionarioresposta");
	}
	
	/**
	 * 
	 * M�todo que busca grupoquestionario atrav�s da descricao
	 *
	 *@param grupoquestionario
	 *@author Jo�o Vitor
	 *@date 10/03/2015
	 *@return Grupoquestionario
	 * 
	 */
	public Grupoquestionario findGrupoquestionarioByDescricao(Grupoquestionario grupoquestionario){
		if (StringUtils.isBlank(grupoquestionario.getDescricao())) {
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
		.select("grupoquestionario.cdgrupoquestionario, grupoquestionario.descricao, grupoquestionario.ativo ")
		.whereLikeIgnoreAll("grupoquestionario.descricao", grupoquestionario.getDescricao())
		.unique();
	}
	
	public List<Grupoquestionario> carregaGrupoquestionarioForCalculoMediaAprovacao(String whereIn){
		return query()
		.select("grupoquestionario.cdgrupoquestionario, grupoquestionario.mediapontuacao ")
		.whereIn("grupoquestionario.cdgrupoquestionario", whereIn)
		.list();
	}
}
