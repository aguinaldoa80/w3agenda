package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Composicaoorcamento;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Recursocomposicao;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.OrcamentoRecursoGeralFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RecursocomposicaoDAO extends GenericDAO<Recursocomposicao>{
	
	/**
	 * Retorna uma lista de recursos de uma determinada composi��o de or�amento
	 *
	 * @param composicaoorcamento
	 * @return lista de Recursocomposicao
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Recursocomposicao> findByComposicaoOrcamentoForFlex(Composicaoorcamento composicaoorcamento) {
		if (composicaoorcamento == null || composicaoorcamento.getCdcomposicaoorcamento() == null) {
			return new ArrayList<Recursocomposicao>();
		}
		return 
			query()
				.select("recursocomposicao.cdrecursocomposicao, recursocomposicao.nome")					
				.join("recursocomposicao.composicaoorcamento composicaoorcamento")
				.where("composicaoorcamento = ?", composicaoorcamento)
				.orderBy("recursocomposicao.nome")
				.list();
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.useTransaction(false);
		save.saveOrUpdateManaged("listaDependenciacargocomposicao");
		save.saveOrUpdateManaged("listaDependenciafaixacomposicao");
	}
	
	/**
	 * Busca os recursos gerais diretos de um or�amento
	 *
	 * @param orcamento
 	 * @return List<Recursocomposicao>
 	 * 
 	 * @throws SinedException - caso o or�amento seja nulo
	 * 
 	 * @author Rodrigo Alvarenga
	 */
	public List<Recursocomposicao> findRecursoDiretoByOrcamento(Orcamento orcamento) {
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		
		return 
			query()
				.select(
					"recursocomposicao.cdrecursocomposicao, recursocomposicao.nome, recursocomposicao.quantidadecalculada, " +
					"recursocomposicao.numocorrencia, recursocomposicao.custounitario, " +
					"material.cdmaterial, material.nome, " +
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
					"recursocomposicao.tipoitemcomposicao, recursocomposicao.tiporelacaorecurso, " +
					"recursocomposicao.tipodependenciarecurso, recursocomposicao.tipoocorrencia")
				.join("recursocomposicao.orcamento orcamento")
				.leftOuterJoin("recursocomposicao.material material")
				.join("recursocomposicao.unidademedida unidademedida")
				.where("recursocomposicao.composicaoorcamento IS NULL")
				.where("orcamento = ?", orcamento)
				.orderBy("recursocomposicao.nome")
				.list();
	}	
	
	/**
	 * Busca os recursos gerais indiretos de um or�amento
	 *
	 * @param orcamento
 	 * @return List<Recursocomposicao>
 	 * 
 	 * @throws SinedException - caso o or�amento seja nulo
	 * 
 	 * @author Rodrigo Alvarenga
	 */
	public List<Recursocomposicao> findRecursoIndiretoByOrcamento(Orcamento orcamento) {
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		
		return 
			query()
				.select(
					"recursocomposicao.cdrecursocomposicao, recursocomposicao.nome, recursocomposicao.quantidade, recursocomposicao.quantidadecalculada, recursocomposicao.numocorrencia, recursocomposicao.custounitario, " +
					"material.cdmaterial, material.valorvenda, " +
					"formulacomposicao.cdformulacomposicao, " +						
					"tiporelacaorecurso.cdtiporelacaorecurso, " +
					"tipodependenciarecurso.cdtipodependenciarecurso, " +
					"tipoocorrencia.cdtipoocorrencia, " +
					"dependenciacargocomposicao.cddependenciacargocomposicao, " +
					"cargoOrigem.cdcargo, cargoOrigem.nome, " +
					"dependenciafaixacomposicao.cddependenciafaixacomposicao, dependenciafaixacomposicao.faixade, dependenciafaixacomposicao.faixaate, dependenciafaixacomposicao.quantidade")
				.leftOuterJoin("recursocomposicao.material material")
				.leftOuterJoin("recursocomposicao.formulacomposicao formulacomposicao")
				.join("recursocomposicao.composicaoorcamento composicaoorcamento")
				.join("composicaoorcamento.orcamento orcamento")
				.join("recursocomposicao.tiporelacaorecurso tiporelacaorecurso")
				.join("recursocomposicao.tipodependenciarecurso tipodependenciarecurso")
				.join("recursocomposicao.tipoocorrencia tipoocorrencia")
				.leftOuterJoin("recursocomposicao.listaDependenciacargocomposicao dependenciacargocomposicao")
				.leftOuterJoin("dependenciacargocomposicao.cargo cargoOrigem")
				.leftOuterJoin("recursocomposicao.listaDependenciafaixacomposicao dependenciafaixacomposicao")
				.where("orcamento = ?", orcamento)
				.openParentheses()
					.where("composicaoorcamento.materialseguranca IS NULL")
					.or()
					.where("composicaoorcamento.materialseguranca = ?",false)
				.closeParentheses()
				.orderBy("recursocomposicao.nome, cargoOrigem.nome, dependenciafaixacomposicao.faixade, dependenciafaixacomposicao.faixaate")
				.list();
	}	
	
	/**
	 * Busca os recursos gerais do or�amento a partir de um filtro
	 *
	 * @param filtro
 	 * @return List<Recursocomposicao>
 	 * 
 	 * @throws SinedException - caso o or�amento seja nulo
	 * 
 	 * @author Rodrigo Alvarenga
	 */
	public List<Recursocomposicao> findForListagemFlex(OrcamentoRecursoGeralFiltro filtro) {
		if (filtro.getOrcamento() == null || filtro.getOrcamento().getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		
		List<Recursocomposicao> listaRecursoComposicao = new ArrayList<Recursocomposicao>();
		QueryBuilder<Recursocomposicao> queryListagem;
		
		if (filtro.getRecursoDireto()) {
			queryListagem = 
				query()
					.select(
						"recursocomposicao.cdrecursocomposicao, recursocomposicao.nome, recursocomposicao.quantidadecalculada, " +
						"recursocomposicao.numocorrencia, recursocomposicao.custounitario, " +
						"material.cdmaterial, material.nome, " +
						"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
						"vcontagerencial.identificador, contagerencial.nome, tipooperacao.nome, referenciacalculo.cdreferenciacalculo, referenciacalculo.nome")
					.leftOuterJoin("recursocomposicao.orcamento orcamento")
					.leftOuterJoin("recursocomposicao.material material")
					.leftOuterJoin("recursocomposicao.referenciacalculo referenciacalculo")
					.leftOuterJoin("material.contagerencial contagerencial")
					.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
					.leftOuterJoin("contagerencial.tipooperacao tipooperacao")
					.leftOuterJoin("recursocomposicao.unidademedida unidademedida")
					.where("orcamento = ?", filtro.getOrcamento())
					.whereLikeIgnoreAll("recursocomposicao.nome", filtro.getNomeRecurso())
					.where("recursocomposicao.composicaoorcamento IS NULL")
					.where("recursocomposicao.quantidadecalculada IS NOT NULL")					
					.orderBy("recursocomposicao.nome");
			
			listaRecursoComposicao.addAll(queryListagem.list());
		}
		
		if (filtro.getRecursoComposicao()) {
			queryListagem = 
				query()
					.select(
						"recursocomposicao.cdrecursocomposicao, recursocomposicao.nome, recursocomposicao.quantidadecalculada, " +
						"recursocomposicao.numocorrencia, recursocomposicao.custounitario, " +
						"material.cdmaterial, material.nome, " +
						"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
						"vcontagerencial.identificador, contagerencial.nome, tipooperacao.nome, referenciacalculo.cdreferenciacalculo, referenciacalculo.nome")
					.leftOuterJoin("recursocomposicao.composicaoorcamento composicaoorcamento")
					.leftOuterJoin("composicaoorcamento.orcamento orcamento")
					.leftOuterJoin("recursocomposicao.material material")
					.leftOuterJoin("recursocomposicao.referenciacalculo referenciacalculo")
					.leftOuterJoin("material.contagerencial contagerencial")
					.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
					.leftOuterJoin("contagerencial.tipooperacao tipooperacao")
					.leftOuterJoin("recursocomposicao.unidademedida unidademedida")					
					.where("orcamento = ?", filtro.getOrcamento())
					.where("composicaoorcamento = ?", filtro.getComposicaoOrcamento())
					.whereLikeIgnoreAll("recursocomposicao.nome", filtro.getNomeRecurso())
					.where("recursocomposicao.quantidadecalculada IS NOT NULL")
					.orderBy("recursocomposicao.nome");
			
			listaRecursoComposicao.addAll(queryListagem.list());			
		}
		return 
			listaRecursoComposicao;
	}
	
	/**
	 * Atualiza o custo unit�rio do material ou item presente no recursocomposicao passado como par�metro
	 * 
	 * @param recursocomposicao
	 * @return 
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public void atualizaCustoUnitarioForFlex(final Recursocomposicao recursocomposicao) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				String sqlUpdate;
				Object[] fields;
				if (recursocomposicao.getMaterial() != null) {
					sqlUpdate = 
						"UPDATE Recursocomposicao " +
						"SET custounitario = ? " +
						"WHERE material.id = ?";
					
					fields = new Object[]{recursocomposicao.getCustounitario(), recursocomposicao.getMaterial().getCdmaterial()};					
				}
				else {
					sqlUpdate = 
						"UPDATE Recursocomposicao " +
						"SET custounitario = ? " +
						"WHERE material.id IS NULL " +
						"AND nome = ? " +
						"AND unidademedida.id = ?";
					
					fields = new Object[]{recursocomposicao.getCustounitario(), recursocomposicao.getNome(), recursocomposicao.getUnidademedida().getCdunidademedida()};					
					
				}
				getHibernateTemplate().bulkUpdate(sqlUpdate, fields);				
				
				return null;
			}
		});
	}
	
	/**
	 * Atualiza a quantidade calculada e o n�mero de ocorr�ncias dos recursos presentes na lista passada como par�metro
	 * 
	 * @param listaRecursoGeral
	 * @return 
	 * @throws SinedException - caso o par�metro seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public void atualizaQuantidadeCalculadaRecursoForFlex(List<Recursocomposicao> listaRecursoGeral) {
		if (listaRecursoGeral == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}
		Object[] fields;
		
		for (Recursocomposicao recursoGeral : listaRecursoGeral) {
			if(recursoGeral.getQuantidadecalculada() != null || recursoGeral.getNumocorrencia() != null){
				String sqlUpdate = "UPDATE Recursocomposicao " +
				"SET " +
				(recursoGeral.getQuantidadecalculada() != null ? " quantidadecalculada = ? " : " ") +
				(recursoGeral.getQuantidadecalculada() != null && recursoGeral.getNumocorrencia() != null ? " ," : "") +
				(recursoGeral.getNumocorrencia() != null ? " numocorrencia = ? " : " ") +
				"WHERE cdrecursocomposicao = ?";
				
				if(recursoGeral.getQuantidadecalculada() != null && recursoGeral.getNumocorrencia() == null){
					fields = new Object[]{recursoGeral.getQuantidadecalculada(),recursoGeral.getCdrecursocomposicao()};
				}else if(recursoGeral.getQuantidadecalculada() == null && recursoGeral.getNumocorrencia() != null){
					fields = new Object[]{recursoGeral.getNumocorrencia(), recursoGeral.getCdrecursocomposicao()};
				}else {
					fields = new Object[]{recursoGeral.getQuantidadecalculada(), recursoGeral.getNumocorrencia(), recursoGeral.getCdrecursocomposicao()};
				}
				getHibernateTemplate().bulkUpdate(sqlUpdate, fields);
			}
		}
	}	
	
	/**
	 * Insere/atualiza os recursos das composi��es do or�amento presentes na lista listaRecursoComposicaoForUpdate e
	 * exclui os recursos presentes na lista listaRecursoComposicaoForDelete
	 * 
	 * N�o utiliza TRANSACTION. Os m�todos que o chamam que devem utilizar.
	 * 
	 * @param listaRecursoComposicaoForUpdate
	 * @param listaRecursoComposicaoForDelete
	 * @return
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public void saveListaRecursoComposicao(List<Recursocomposicao> listaRecursoComposicaoForUpdate, List<Recursocomposicao> listaRecursoComposicaoForDelete) {		
		if (listaRecursoComposicaoForUpdate == null || listaRecursoComposicaoForDelete == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}		

		//Apaga os registros
		for (Recursocomposicao recursoComposicaoForDelete : listaRecursoComposicaoForDelete) {
			recursoComposicaoForDelete.setOrcamento(null); //Seta null porque sen�o n�o deixa apagar.
			recursoComposicaoForDelete.setComposicaoorcamento(null); //Seta null porque sen�o n�o deixa apagar.
			delete(recursoComposicaoForDelete);
		}

		//Insere/atualiza os registros
		for (Recursocomposicao recursoComposicaoForUpdatde : listaRecursoComposicaoForUpdate) {
			saveOrUpdate(recursoComposicaoForUpdatde);
		}
	}
	
	/**
	 * Busca os recursos gerais de uma determinada composi��o do or�amento
	 *
	 * @param composicaoorcamento
 	 * @return List<Recursocomposicao>
 	 * 
 	 * @throws SinedException - caso o par�metro composicaoorcamento seja nulo
	 * 
 	 * @author Rodrigo Alvarenga
	 */
	public List<Recursocomposicao> findRecursoByComposicao(Composicaoorcamento composicaoorcamento) {
		if (composicaoorcamento == null || composicaoorcamento.getCdcomposicaoorcamento() == null) {
			throw new SinedException("A composi��o do or�amento n�o pode ser nula.");
		}
		
		return
			query()
				.select(
					"recursocomposicao.cdrecursocomposicao, recursocomposicao.nome, recursocomposicao.quantidadecalculada, " +
					"recursocomposicao.numocorrencia, recursocomposicao.custounitario")
				.join("recursocomposicao.composicaoorcamento composicaoorcamento")
				.join("composicaoorcamento.orcamento orcamento")				
				.where("composicaoorcamento = ?", composicaoorcamento)
				.orderBy("recursocomposicao.nome")
				.list();
	}
	
	@Override
	public void saveOrUpdate(Recursocomposicao bean) {
		try {
			super.saveOrUpdate(bean);
		}
		catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "UK_DEPENDENCIAFAIXACOMPOSICAO")) {
				throw new SinedException("Existem faixas de valores iguais.");
			}
			throw new SinedException(e.getMessage());
		}
	}

	public void updateCustoUnitario(Recursocomposicao recursocomposicao) {
		if(recursocomposicao.getCdrecursocomposicao() == null){
			throw new SinedException("O id do recurso composi��o n�o pode ser nulo.");
		}
		
		Object[] fields = new Object[]{recursocomposicao.getCustounitario(), recursocomposicao.getCdrecursocomposicao()};
		String sqlUpdate = "UPDATE Recursocomposicao SET custounitario = ? WHERE cdrecursocomposicao = ?";
		
		getHibernateTemplate().bulkUpdate(sqlUpdate, fields);
	}
}
