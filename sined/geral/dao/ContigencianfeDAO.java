package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Contigencianfe;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ContigencianfeFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("contigencianfe.dtentrada desc")
public class ContigencianfeDAO extends GenericDAO<Contigencianfe>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Contigencianfe> query, FiltroListagem _filtro) {
		ContigencianfeFiltro filtro = (ContigencianfeFiltro) _filtro;
		
		query
			.select("contigencianfe.cdcontigencianfe, uf.nome, uf.sigla, contigencianfe.dtentrada, contigencianfe.justificativa, " +
					"contigencianfe.ativo, contigencianfe.tipocontigencia, contigencianfe.dtsaida")
			.join("contigencianfe.uf uf")
			.where("contigencianfe.ativo = ?", filtro.getAtivo())
			.where("contigencianfe.uf = ?", filtro.getUf());
	}

	/**
	 * Busca os resgistros ativos a partir da UF
	 *
	 * @param uf
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/05/2015
	 */
	public List<Contigencianfe> findAtivosByUf(Uf uf) {
		Timestamp currentTimestamp = SinedDateUtils.currentTimestamp();
		return query()
					.select("contigencianfe.cdcontigencianfe, uf.nome, uf.sigla, contigencianfe.dtentrada, contigencianfe.dtsaida, " +
							"contigencianfe.justificativa, contigencianfe.ativo, contigencianfe.tipocontigencia")
					.join("contigencianfe.uf uf")
					.where("contigencianfe.ativo = ?", Boolean.TRUE)
					.where("contigencianfe.uf = ?", uf)
					.where("contigencianfe.dtentrada <= ?", currentTimestamp)
					.where("contigencianfe.dtsaida >= ?", currentTimestamp)
					.list();
	}

}
