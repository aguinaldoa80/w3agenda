package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Ausenciacolaborador;
import br.com.linkcom.sined.geral.bean.Ausenciacolaboradorhistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AusenciacolaboradorhistoricoDAO extends GenericDAO<Ausenciacolaboradorhistorico>{

	public List<Ausenciacolaboradorhistorico> findByAusenciacolaborador(Ausenciacolaborador ausenciacolaborador) {
		return 
			query()
				.select("ausenciacolaboradorhistorico.cdausenciacolaboradorhistorico, ausenciacolaborador.cdausenciacolaborador, " +
						"ausenciacolaboradorhistorico.dtaltera, ausenciacolaboradorhistorico.cdusuarioaltera, ausenciacolaboradorhistorico.acao")
				.leftOuterJoin("ausenciacolaboradorhistorico.ausenciacolaborador ausenciacolaborador")
				.where("ausenciacolaborador=?", ausenciacolaborador)
				.list();
		
	}
	

}
