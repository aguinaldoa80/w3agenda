package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Clienterelacao;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ClienterelacaoDAO extends GenericDAO<Clienterelacao>{

	public List<Clienterelacao> findByColaborador(Colaborador colaborador){
		return query().select("clienterelacao.cdclienterelacao, cliente.nome, cliente.cdpessoa, " +
				"relacao.cdclienterelacaotipo, relacao.descricao")
		.leftOuterJoin("clienterelacao.clienteoutro cliente")
		.leftOuterJoin("clienterelacao.colaborador colaborador")
		.leftOuterJoin("clienterelacao.clienterelacaotipo relacao")
		.where("colaborador = ?", colaborador)
		.list();
	}
//	
//	public void delete(Colaborador bean, List<Clienterelacao> id){
//		String ids = "";
//		int tamanho = 0;
//		for (Clienterelacao clienterelacao : id) {
//			if (clienterelacao.getCdclienterelacao() != null){
//				ids += clienterelacao.getCdclienterelacao().toString();
//				tamanho ++;
//				if(tamanho < id.size())
//					ids += ", ";
//			}
//		}
//		if (!ids.equals(""))
//			getHibernateTemplate().bulkUpdate("DELETE Clienterelacao cli WHERE cli.colaborador = ? AND cli.cdclienterelacao NOT IN ("+ids+")", bean);
//	}
//	
//	public void saveOrUpdate(List<Clienterelacao> bean){
//		for (Clienterelacao clienterelacao : bean) {
//			saveOrUpdate(clienterelacao);
//		}
//	}
}
