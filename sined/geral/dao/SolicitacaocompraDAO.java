package br.com.linkcom.sined.geral.dao;

import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.AUTORIZADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.CANCELADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.ESTORNAR;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.NAO_AUTORIZADA;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Orcamentomaterial;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.OrigemSuprimentos;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Solicitacaocompraorigem;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.enumeration.SolicitacaocompraFiltroMostrar;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.EmitirprazomediocompraFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.SolicitacaocompraFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class SolicitacaocompraDAO extends GenericDAO<Solicitacaocompra> {
	
	private ParametrogeralService parametrogeralService;
	private ArquivoDAO arquivoDAO;
	private UsuarioService usuarioService;
	private MaterialcategoriaService materialcategoriaService;
	
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {this.materialcategoriaService = materialcategoriaService;}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Solicitacaocompra> query,	FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		SolicitacaocompraFiltro filtro = (SolicitacaocompraFiltro) _filtro;
		
		if(filtro.getIdentificador() != null && !filtro.getIdentificador().equals("")){
			while(filtro.getIdentificador().indexOf(",,") != -1){
				filtro.setIdentificador(filtro.getIdentificador().replaceAll(",,", ","));
			}
			
			if(filtro.getIdentificador().substring(0, 1).equals(",")){
				filtro.setIdentificador(filtro.getIdentificador().substring(1, filtro.getIdentificador().length()));
			}
			
			if(filtro.getIdentificador().substring(filtro.getIdentificador().length() - 1, filtro.getIdentificador().length()).equals(",")){
				filtro.setIdentificador(filtro.getIdentificador().substring(0, filtro.getIdentificador().length()-1));
			}
		}
		
		query
			.select("solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.identificador, solicitacaocompra.descricao, " +
					"solicitacaocompra.qtde, solicitacaocompra.dtlimite, solicitacaocompra.dtautorizacao, solicitacaocompra.observacao, " +
					"solicitacaocompra.pedido, solicitacaocompra.entrega, solicitacaocompra.romaneio, solicitacaocompra.dtsolicitacao, " +
					"material.cdmaterial, material.nome, material.identificacao, material.tempoentrega, " +
					"materialmestregrade.cdmaterial, materialmestregrade.nome, materialmestregrade.identificacao, " +
					"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
					"usuario.nome, " +
					"materialorigem.cdmaterial, materialorigem.nome, " +
					"orcamentomaterial.descricao, " +
					"projetoorigem.nome, " +
					"requisicaomaterial.cdrequisicaomaterial, " +
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
					"projeto.cdprojeto, projeto.nome, " +
					"departamento.cddepartamento, departamento.nome, " +
					"aux_solicitacaocompra.situacaosuprimentos, aux_solicitacaocompra.pendenciaqtde, " +
					"aux_solicitacaocompra.cotacao, aux_solicitacaocompra.ordemcompra, aux_solicitacaocompra.entrega")
					
			.join("solicitacaocompra.aux_solicitacaocompra aux_solicitacaocompra")
			.join("solicitacaocompra.localarmazenagem localarmazenagem")
			.join("solicitacaocompra.material material")
			.leftOuterJoin("material.materialmestregrade materialmestregrade")
			.leftOuterJoin("solicitacaocompra.solicitacaocompraorigem solicitacaocompraorigem")
			.leftOuterJoin("solicitacaocompraorigem.usuario usuario")
			.leftOuterJoin("solicitacaocompraorigem.projeto projetoorigem")
			.leftOuterJoin("solicitacaocompraorigem.material materialorigem")
			.leftOuterJoin("solicitacaocompraorigem.requisicaomaterial requisicaomaterial")
			.leftOuterJoin("solicitacaocompraorigem.orcamentomaterial orcamentomaterial")
			.leftOuterJoin("solicitacaocompra.projeto projeto")
			.leftOuterJoin("solicitacaocompra.departamento departamento")
			.leftOuterJoin("solicitacaocompra.unidademedida unidademedida")
			.whereLikeIgnoreAll("solicitacaocompra.descricao", filtro.getDescricao())
			.whereIn("solicitacaocompra.identificador", filtro.getIdentificador())
			.where("solicitacaocompra.dtlimite >= ?", filtro.getDatalimitede())
			.where("solicitacaocompra.dtlimite <= ?", filtro.getDatalimiteate())
			.where("material.materialgrupo = ?",filtro.getMaterialgrupo())
			.where("material.materialtipo = ?",filtro.getMaterialtipo())
			.where("solicitacaocompra.centrocusto = ?", filtro.getCentrocusto())
			.where("solicitacaocompra.empresa = ?", filtro.getEmpresa())
			.where("solicitacaocompra.projeto = ?", filtro.getProjeto())
			.where("solicitacaocompra.localarmazenagem = ?", filtro.getLocalarmazenagem())
			.where("solicitacaocompra.departamento = ?", filtro.getDepartamento());
		
		if(filtro.getMaterialcategoria() != null){
			materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
			query
				.leftOuterJoin("material.materialcategoria materialcategoria")
				.leftOuterJoin("materialcategoria.vmaterialcategoria vmaterialcategoria")
				.openParentheses()
					.where("vmaterialcategoria.identificador like ? ||'.%'", filtro.getMaterialcategoria().getIdentificador())
				.or()
					.where("vmaterialcategoria.identificador like ? ", filtro.getMaterialcategoria().getIdentificador())
				.closeParentheses();
			
		}

		if(filtro.getColaborador() != null && !filtro.getColaborador().equals("")){
			query
				.join("solicitacaocompra.colaborador colaborador", true)
				.whereLikeIgnoreAll("colaborador.nome", filtro.getColaborador());
		}
		
		if(filtro.getMostrarpendenciaqtde() != null && filtro.getMostrarpendenciaqtde()){
			query
				.where("aux_solicitacaocompra.pendenciaqtde = ?", filtro.getMostrarpendenciaqtde())
				.where("aux_solicitacaocompra.situacaosuprimentos = ?", Situacaosuprimentos.EM_PROCESSOCOMPRA);
		}
		
		if(filtro.getMaterialnomecodigo() != null && !filtro.getMaterialnomecodigo().equals("")){
			filtro.setMaterialnomecodigo(filtro.getMaterialnomecodigo().replace("?", ""));			
			if(!filtro.getMaterialnomecodigo().equals("")){	
				if(SinedUtil.validaNumeros(filtro.getMaterialnomecodigo())){
					query.openParentheses()
					.whereLikeIgnoreAll("material.nome", filtro.getMaterialnomecodigo())
					.or()
					.where("material.cdmaterial = " + filtro.getMaterialnomecodigo())
					.closeParentheses();
				}else{
					query.whereLikeIgnoreAll("material.nome", filtro.getMaterialnomecodigo());
				}
			}
		}
		
		if(filtro.getListaclasses() != null){
			query
				.join("solicitacaocompra.materialclasse materialclasse", true)
				.whereIn("materialclasse.cdmaterialclasse", CollectionsUtil.listAndConcatenate(filtro.getListaclasses(), "cdmaterialclasse",","));
		}
		
		if(filtro.getMostrargrupomaterial() != null && !filtro.getMostrargrupomaterial() && SinedUtil.haveGrupoMaterialUsuario()){
			query
				.joinIfNotExists("material.materialgrupo materialgrupo")
				.whereIn("materialgrupo.cdmaterialgrupo", SinedUtil.getWhereInGrupoMaterialUsuario());
		}
		
		if(filtro.getMostrarvinculoordemcompra() != null){
			if(filtro.getMostrarvinculoordemcompra()){
				query
					.openParentheses()
					.where("exists (select oco.cdordemcompraorigem from Ordemcompraorigem oco where oco.solicitacaocompra = solicitacaocompra)")
					.or()
					.where("exists (select oc.cdordemcompra from Ordemcompra oc join oc.cotacao c join c.listaOrigem origem where origem.solicitacaocompra = solicitacaocompra)")
					.closeParentheses();
			}else {
				query
					.where("not exists (select oco.cdordemcompraorigem from Ordemcompraorigem oco where oco.solicitacaocompra = solicitacaocompra)")
					.where("not exists (select oc.cdordemcompra from Ordemcompra oc join oc.cotacao c join c.listaOrigem origem where origem.solicitacaocompra = solicitacaocompra)");
			}
				
		}
		
		if(filtro.getListaorigens() != null) {
			query.openParentheses();
				for (String origem : filtro.getListaorigens()) {
					if(origem.equals(OrigemSuprimentos.PROJETO.name())) {
						query.where("solicitacaocompraorigem.projeto is not null").or();
					} else if(origem.equals(OrigemSuprimentos.REPOSICAO.name())) {
						query.where("solicitacaocompraorigem.material is not null").or();
					} else if(origem.equals(OrigemSuprimentos.USUARIO.name())) {
						query.where("solicitacaocompraorigem.usuario is not null").or();
					} else if(origem.equals(OrigemSuprimentos.REQUISICAO.name())) {
						query.where("solicitacaocompraorigem.requisicaomaterial is not null").or();
					} else if(origem.equals(OrigemSuprimentos.LISTAMATERIAL.name())) {
						query.where("solicitacaocompraorigem.orcamentomaterial is not null").or();
					}
				}
				query.closeParentheses();
		}
		
		if(filtro.getSolicitacaocompraFiltroMostrar() != null){
			if(filtro.getSolicitacaocompraFiltroMostrar().equals(SolicitacaocompraFiltroMostrar.SOLICITACOES_PENDENTES)){
				query.where("aux_solicitacaocompra.situacaosuprimentos <> ?", Situacaosuprimentos.BAIXADA);
				query.where("exists (" +
								"select s.cdsolicitacaocompra " +
								"from Solicitacaocompra s " +
								"join s.material m " +
								"where s.cdsolicitacaocompra = solicitacaocompra.cdsolicitacaocompra " +
								"and solicitacaocompra.dtlimite + coalesce(m.tempoentrega, 0) < current_date " +
							")");
			} else if(filtro.getSolicitacaocompraFiltroMostrar().equals(SolicitacaocompraFiltroMostrar.SOLICITACOES_ATRASADAS)){
				String prazodiasStr = parametrogeralService.getValorPorNome(Parametrogeral.PRAZO_DIAS_INICIO_COMPRA);
				Integer prazodias = 0;
				try{
					prazodias = Integer.parseInt(prazodiasStr);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				if(prazodias > 0){
					query
						.openParentheses()
						.where("aux_solicitacaocompra.situacaosuprimentos = ?", Situacaosuprimentos.EM_ABERTO)
						.or()
						.where("aux_solicitacaocompra.situacaosuprimentos = ?", Situacaosuprimentos.AUTORIZADA)
						.closeParentheses();
					query.where("(current_date - solicitacaocompra.dtsolicitacao) > ?", new Double(prazodias));
				}
			}
		}
		
		if(!SinedUtil.getUsuarioLogado().getTodosdepartamentos()){
			query
				.openParentheses()
				.where("departamento.cddepartamento in (" +
												"select d.cddepartamento " +
												"from Usuariodepartamento ud " +
												"join ud.usuario u " +
												"join ud.departamento d " +
												"where u.cdpessoa = " + SinedUtil.getUsuarioLogado().getCdpessoa() + 
												")")
				.or()
				.where("departamento is null")
				.closeParentheses();
		}
		
		if(filtro.getOrdemcompragerada() != null && filtro.getOrdemcompragerada()){
			query
				.openParentheses()
					.where("exists (SELECT scorigem.cdsolicitacaocompra FROM Ordemcompraorigem oco " +
						"JOIN oco.solicitacaocompra scorigem " +
						"JOIN oco.ordemcompra ocorigem " +
						"JOIN ocorigem.aux_ordemcompra aux_ocorigem " +
						"where scorigem.cdsolicitacaocompra = solicitacaocompra.cdsolicitacaocompra and " +
						"aux_ocorigem.situacaosuprimentos <> " + Situacaosuprimentos.CANCELADA.getCodigo() + ") ")
					.or()
					.where("exists (SELECT scorigem.cdsolicitacaocompra FROM Ordemcompra oc " +
						"JOIN oc.cotacao c " +
						"JOIN c.listaOrigem co " +
						"JOIN co.solicitacaocompra scorigem " +
						"JOIN oc.aux_ordemcompra aux_ocorigem " +
						"where scorigem.cdsolicitacaocompra = solicitacaocompra.cdsolicitacaocompra and " +
						"aux_ocorigem.situacaosuprimentos <> " + Situacaosuprimentos.CANCELADA.getCodigo() + ") ")
				.closeParentheses();
		}
		
		if(filtro.getListasituacoes() != null) {
			query.whereIn("aux_solicitacaocompra.situacaosuprimentos", Situacaosuprimentos.listAndConcatenate(filtro.getListasituacoes()));
		}
		
		if(filtro.getEtapaProcessoCompra() != null){
			switch (filtro.getEtapaProcessoCompra()) {
				case RECEBIMENTO:
					query.where("aux_solicitacaocompra.entrega != null");
					break;
				case ORDEM_COMRPA:
					query.where("aux_solicitacaocompra.entrega = null");
					query.where("aux_solicitacaocompra.ordemcompra != null");
					break;
				case COTACAO:
					query.where("aux_solicitacaocompra.entrega = null");
					query.where("aux_solicitacaocompra.ordemcompra = null");
					query.where("aux_solicitacaocompra.cotacao != null");
					break;
				default:
					break;
			}
		}
		
		query.orderBy("solicitacaocompra.identificador desc");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Solicitacaocompra> query) {
		query
			.select("solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.identificador, solicitacaocompra.descricao, solicitacaocompra.cdusuarioaltera, solicitacaocompra.dtsolicitacao, " +
					"solicitacaocompra.dtaltera, solicitacaocompra.qtde, solicitacaocompra.observacao, solicitacaocompra.dtlimite, solicitacaocompra.dtcancelamento, " +
					"solicitacaocompra.dtbaixa, solicitacaocompra.dtautorizacao, solicitacaocompra.pedido, solicitacaocompra.entrega, solicitacaocompra.romaneio, " +
					"material.cdmaterial, material.nome, materialmestregrade.nome, materialmestregrade.cdmaterial, materialmestregrade.identificacao, " +
					"solicitacaocompraorigem.cdsolicitacaocompraorigem, solicitacaocompraacao.cdsolicitacaocompraacao, solicitacaocompraacao.nome, " +
					"solicitacaocompraOrigem.cdsolicitacaocompra, usuario.cdpessoa, materialorigem.cdmaterial, requisicaomaterial.cdrequisicaomaterial, requisicaomaterial.identificador, orcamentomaterial.cdorcamentomaterial, colaborador.cdpessoa, " +
					"cotacao.cdcotacao, aux_cotacao.situacaosuprimentos, aux_cotacao.cdcotacao, listaSolicitacaocomprahistorico.dtaltera, listaSolicitacaocomprahistorico.observacao, " +
					"listaSolicitacaocomprahistorico.cdsolicitacaocomprahistorico, materialclasse.cdmaterialclasse, materialgrupo.cdmaterialgrupo, " +
					"materialtipo.cdmaterialtipo, material.tempoentrega, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, centrocusto.cdcentrocusto, colaborador.nome, solicitacaocompra.faturamentocliente, " +
					"projeto.cdprojeto, projeto.nome, departamento.cddepartamento, departamento.nome, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, listaSolicitacaocomprahistorico.cdusuarioaltera," +
					"frequencia.cdfrequencia, solicitacaocompra.qtdefrequencia, unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
					"planejamento.cdplanejamento, planejamento.descricao, planejamentorecursogeral.cdplanejamentorecursogeral, tarefa.cdtarefa, tarefa.descricao, tarefarecursogeral.cdtarefarecursogeral, " +
					"contagerencial.cdcontagerencial, contagerencial.nome, scmaterialgrupo.cdmaterialgrupo, scmaterialgrupo.nome, " +
					"contagerencialMaterial.cdcontagerencial, contagerencialMaterial.nome, vcontagerencialMaterial.arvorepai, vcontagerencialMaterial.identificador, " +
					"unidademedidamaterial.cdunidademedida, unidademedidamaterial.nome, pedidovenda.cdpedidovenda, aux_solicitacaocompra.pendenciaqtde, aux_solicitacaocompra.situacaosuprimentos," +
					"ordemcompra.cdordemcompra, aux_ordemcompra.situacaosuprimentos, producaoagenda.cdproducaoagenda, solicitacaocompra.dtnaoautorizacao, " +
					"arquivo.cdarquivo, arquivo.nome, solicitacaocompra.altura, solicitacaocompra.comprimento, solicitacaocompra.largura, solicitacaocompra.pesototal, solicitacaocompra.qtdvolume ") 
				
			.join("solicitacaocompra.frequencia frequencia")
			.join("solicitacaocompra.material material")
			.leftOuterJoin("material.materialmestregrade materialmestregrade")
			.leftOuterJoin("material.contagerencial contagerencialMaterial")
			.leftOuterJoin("contagerencialMaterial.vcontagerencial vcontagerencialMaterial")
			.join("material.materialgrupo materialgrupo")
			.join("material.unidademedida unidademedidamaterial")
			.leftOuterJoin("solicitacaocompra.unidademedida unidademedida")
			.join("solicitacaocompra.materialclasse materialclasse")
			.join("solicitacaocompra.localarmazenagem localarmazenagem")
			.join("solicitacaocompra.solicitacaocompraorigem solicitacaocompraorigem")
			.join("solicitacaocompra.aux_solicitacaocompra aux_solicitacaocompra")
			.leftOuterJoin("solicitacaocompra.listaSolicitacaocomprahistorico listaSolicitacaocomprahistorico")
			.leftOuterJoin("listaSolicitacaocomprahistorico.solicitacaocompraacao solicitacaocompraacao")
			.leftOuterJoin("solicitacaocompraorigem.usuario usuario")
			.leftOuterJoin("material.materialtipo materialtipo")
			.leftOuterJoin("solicitacaocompraorigem.producaoagenda producaoagenda")
			.leftOuterJoin("solicitacaocompraorigem.material materialorigem")
			.leftOuterJoin("solicitacaocompraorigem.requisicaomaterial requisicaomaterial")
			.leftOuterJoin("solicitacaocompraorigem.orcamentomaterial orcamentomaterial")
			.leftOuterJoin("solicitacaocompraorigem.tarefarecursogeral tarefarecursogeral")
			.leftOuterJoin("solicitacaocompraorigem.solicitacaocompra solicitacaocompraOrigem")
			.leftOuterJoin("tarefarecursogeral.tarefa tarefa")
			.leftOuterJoin("solicitacaocompraorigem.planejamentorecursogeral planejamentorecursogeral")
			.leftOuterJoin("planejamentorecursogeral.planejamento planejamento")
			.leftOuterJoin("solicitacaocompra.colaborador colaborador")
			.leftOuterJoin("solicitacaocompra.centrocusto centrocusto")
			.leftOuterJoin("solicitacaocompra.empresa empresa")
			.leftOuterJoin("solicitacaocompra.projeto projeto")
			.leftOuterJoin("solicitacaocompra.departamento departamento")
			.leftOuterJoin("solicitacaocompra.listaOrdemcompraorigem listaOrdemcompraorigem")
			.leftOuterJoin("listaOrdemcompraorigem.ordemcompra ordemcompra")
			.leftOuterJoin("ordemcompra.aux_ordemcompra aux_ordemcompra")
			.leftOuterJoin("solicitacaocompra.listaCotacaoorigem listaCotacaoorigem")
			.leftOuterJoin("listaCotacaoorigem.cotacao cotacao")
			.leftOuterJoin("cotacao.aux_cotacao aux_cotacao")
			.leftOuterJoin("solicitacaocompra.contagerencial contagerencial")
			.leftOuterJoin("solicitacaocompra.materialgrupo scmaterialgrupo")
			.leftOuterJoin("solicitacaocompraorigem.pedidovenda pedidovenda")
			.leftOuterJoin("solicitacaocompra.arquivo arquivo")
			.orderBy("listaSolicitacaocomprahistorico.cdsolicitacaocomprahistorico desc");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Solicitacaocompra solicitacaocompra = (Solicitacaocompra) save.getEntity();
		arquivoDAO.saveFile(solicitacaocompra, "arquivo");
	}

	/**
	 * Busca as informa��es para a listagem do CSV.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Solicitacaocompra> findForCsv(SolicitacaocompraFiltro filtro){
		QueryBuilder<Solicitacaocompra> query = querySined();
		updateListagemQuery(query, filtro);
		query.setIgnoreJoinPaths(new ListSet<String>(String.class));
		query
			.select("solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.identificador, solicitacaocompra.descricao, solicitacaocompra.qtde, solicitacaocompra.dtsolicitacao, " +
					"solicitacaocompra.dtlimite, solicitacaocompra.dtautorizacao, solicitacaocompra.observacao, solicitacaocompra.pedido, solicitacaocompra.entrega, " +
					"solicitacaocompra.romaneio," +
					"material.cdmaterial, material.nome, material.identificacao, " +
					"localarmazenagem.nome, " +
					"usuario.nome, " +
					"materialorigem.cdmaterial, materialorigem.nome, " +
					"orcamentomaterial.descricao, " +
					"projetoorigem.nome, " +
					"requisicaomaterial.cdrequisicaomaterial, " +
					"aux_solicitacaocompra.situacaosuprimentos, " +
					"projeto.cdprojeto, projeto.nome, " +
					"departamento.cddepartamento, departamento.nome, " +
					"unidademedida.cdunidademedida, unidademedida.simbolo, " +
					"materialgrupo.cdmaterialgrupo, materialgrupo.nome, " +
					"listaCotacaoorigem.cdcotacaoorigem, " +
					"cotacao.cdcotacao, cotacao.dtcancelamento, cotacao.cdusuarioaltera, " +
					"centrocusto.cdcentrocusto, centrocusto.nome, " +
					"colaborador.cdpessoa, colaborador.nome, "+
					"materialgrupousuario.cdmaterialgrupousuario, grupousuario.nome, " +
					"localarmazenagem.cdlocalarmazenagem, aux_cotacao.situacaosuprimentos, aux_ordemcompra.situacaosuprimentos, ordemcompra.cdordemcompra, " +
					"listaOrdemcompraCotacao.cdordemcompra, aux_ordemcompraCotacao.situacaosuprimentos, solicitacaocompra.pesototal, " +
					"unidademedida.cdunidademedida, unidademedida.nome, " +
					"unidademedidaMaterial.cdunidademedida, unidademedidaMaterial.nome ")
			
			.leftOuterJoinIfNotExists("material.materialgrupo materialgrupo")
			.leftOuterJoinIfNotExists("solicitacaocompra.centrocusto centrocusto")
			.leftOuterJoinIfNotExists("solicitacaocompra.colaborador colaborador")
			.leftOuterJoin("solicitacaocompra.listaCotacaoorigem listaCotacaoorigem")
			.leftOuterJoin("listaCotacaoorigem.cotacao cotacao")
			.leftOuterJoin("cotacao.aux_cotacao aux_cotacao")
			.leftOuterJoin("cotacao.listaOrdemcompra listaOrdemcompraCotacao")
			.leftOuterJoin("listaOrdemcompraCotacao.aux_ordemcompra aux_ordemcompraCotacao")
			.leftOuterJoin("solicitacaocompra.listaOrdemcompraorigem listaOrdemcompraorigem")
			.leftOuterJoin("listaOrdemcompraorigem.ordemcompra ordemcompra")
			.leftOuterJoin("ordemcompra.aux_ordemcompra aux_ordemcompra")
			.leftOuterJoin("materialgrupo.listaMaterialgrupousuario materialgrupousuario")
			.leftOuterJoin("materialgrupousuario.usuario grupousuario")
			.leftOuterJoin("material.unidademedida unidademedidaMaterial");
		return query.list();
	}
	
	/**
	 * Carrega a lista de solicita��es de compra para realizar uma cota��o.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Solicitacaocompra> findForCotacao(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Lista de solicita��es n�o pode ser nulo.");
		}
		return query()
					.select("solicitacaocompra.cdsolicitacaocompra, material.cdmaterial, materialclasse.cdmaterialclasse, localarmazenagem.cdlocalarmazenagem, " +
							"solicitacaocompra.qtde, solicitacaocompra.descricao, frequencia.cdfrequencia, solicitacaocompra.qtdefrequencia," +
							"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, unidademedidamaterial.cdunidademedida," +
							"unidademedidamaterial.nome, unidademedidamaterial.simbolo, solicitacaocompra.observacao")
					.leftOuterJoin("solicitacaocompra.material material")
					.leftOuterJoin("material.unidademedida unidademedidamaterial")
					.leftOuterJoin("solicitacaocompra.frequencia frequencia")
					.leftOuterJoin("solicitacaocompra.materialclasse materialclasse")
					.leftOuterJoin("solicitacaocompra.localarmazenagem localarmazenagem")
					.leftOuterJoin("solicitacaocompra.unidademedida unidademedida")
					.whereIn("solicitacaocompra.cdsolicitacaocompra", whereIn)
					.list();
	}
	
	/**
	 * Retorna o n�mero de solicita��es de compra daquela cota��o.
	 * 
	 * @param cotacao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Solicitacaocompra> findByCotacao(Cotacao cotacao){
		if (cotacao == null || cotacao.getCdcotacao() == null) {
			throw new SinedException("Cota��o n�o pode ser nulo.");
		}
		return query()
					.select("solicitacaocompra.cdsolicitacaocompra, projeto.nome, solicitacaocompra.qtde, solicitacaocompra.identificador, solicitacaocompra.descricao, " +
							"material.cdmaterial, material.nome, solicitacaocompra.localarmazenagem")
					.leftOuterJoin("solicitacaocompra.projeto projeto")
					.leftOuterJoin("solicitacaocompra.material material")
					.join("solicitacaocompra.listaCotacaoorigem cotacaoorigem")
					.join("cotacaoorigem.cotacao cotacao")
					.where("cotacao = ?",cotacao)
					.orderBy("solicitacaocompra.cdsolicitacaocompra")
					.list();
	
	}
	
	/**
	 * Retorna o n�mero de solicita��es de compra daquela cota��o.
	 * 
	 * @param cotacao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Solicitacaocompra> findByCotacaoForBaixaEntrega(Cotacao cotacao, Material material, Localarmazenagem localarmazenagem){
		if (cotacao == null || cotacao.getCdcotacao() == null) {
			throw new SinedException("Cota��o n�o pode ser nulo.");
		}
		return query()
					.select("solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.qtde, materialclasse.cdmaterialclasse, materialclasse.nome")
					.join("solicitacaocompra.listaCotacaoorigem cotacaoorigem")
					.leftOuterJoin("solicitacaocompra.materialclasse materialclasse")
					.join("cotacaoorigem.cotacao cotacao")
					.where("cotacao = ?",cotacao)
					.where("solicitacaocompra.material = ?",material)
					.where("solicitacaocompra.localarmazenagem = ?",localarmazenagem)
					.orderBy("solicitacaocompra.cdsolicitacaocompra")
					.list();
	
	}


	/**
	 * Carrega a lista de solicita��es que tem cota��o.
	 * 
	 * @param ids
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Solicitacaocompra> findForVerificacao(String ids) {
		if (ids == null || ids.equals("")) {
			throw new SinedException("IDs das solicita��es de compra n�o pode ser nulo.");
		}
		return query()
				.select("solicitacaocompra.cdsolicitacaocompra, cotacao.cdcotacao")
				.join("solicitacaocompra.listaCotacaoorigem cotacaoorigem")
				.join("cotacaoorigem.cotacao cotacao")
				.join("cotacao.aux_cotacao aux_cotacao")
				.whereIn("solicitacaocompra.cdsolicitacaocompra", ids)
				.where("aux_cotacao.situacaosuprimentos <> ?",Situacaosuprimentos.CANCELADA)
				.list();	
	}

	/**
	 * Retorna todas as requisi��es de material onde for igual aos PK's 
	 * concatenadas na String whereIn
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Solicitacaocompra> findSolicitacoes(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		return query()
			.select("solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.identificador, solicitacaocompra.descricao, solicitacaocompra.dtbaixa, solicitacaocompra.qtde, " +
					"aux_solicitacaocompra.situacaosuprimentos, requisicaomaterial.cdrequisicaomaterial, empresa.cdpessoa, unidademedida.cdunidademedida, " +
					"unidademedida.nome, unidademedida.simbolo, material.cdmaterial, material.nome, materialgrupo.cdmaterialgrupo, materialgrupo.nome, materialgrupo.email," +
					"colaborador.cdpessoa, colaborador.nome, unidademedidamaterial.cdunidademedida, unidademedidamaterial.nome, tarefarecursogeral.cdtarefarecursogeral, " +
					"tarefa.cdtarefa, tarefa.descricao, solicitacaocompraorigem.cdsolicitacaocompraorigem, projeto.cdprojeto ")
			.join("solicitacaocompra.aux_solicitacaocompra aux_solicitacaocompra")
			.join("solicitacaocompra.solicitacaocompraorigem solicitacaocompraorigem")
			.join("solicitacaocompra.material material")
			.leftOuterJoin("solicitacaocompra.colaborador colaborador")
			.join("material.unidademedida unidademedidamaterial")
			.join("material.materialgrupo materialgrupo")
			.leftOuterJoin("solicitacaocompra.empresa empresa")
			.leftOuterJoin("solicitacaocompraorigem.requisicaomaterial requisicaomaterial")
			.leftOuterJoin("solicitacaocompra.unidademedida unidademedida")
			.leftOuterJoin("solicitacaocompraorigem.tarefarecursogeral tarefarecursogeral")
			.leftOuterJoin("tarefarecursogeral.tarefa tarefa")
			.leftOuterJoin("solicitacaocompra.projeto projeto")
			.whereIn("solicitacaocompra.cdsolicitacaocompra", whereIn)
			.list();
	}

	/**
	 * Da update na(s) solicita��o(�es) de acordo com a situa��o da solicita��o
	 * 
	 * @param whereIn
	 * @param situacao
	 * @author Tom�s Rabelo
	 */
	public void doUpdateSituacaoSolicitacoes(String whereIn, Situacaosuprimentos situacao) {
		if (situacao == null)
			throw new SinedException("Status da solicita��o n�o pode ser nulo.");
		
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		
		StringBuilder sql = new StringBuilder()
			.append("update Solicitacaocompra s set s.cdusuarioaltera = ?, s.dtaltera = ?, ");
		
		if(situacao.equals(AUTORIZADA)){
			sql.append("s.dtautorizacao = ?");
		} else if(situacao.equals(CANCELADA)) {
			sql.append("s.dtcancelamento = ?");
		} else if(situacao.equals(ESTORNAR)){
			sql.append("s.dtautorizacao = null, s.dtcancelamento = null, s.dtbaixa = null, s.dtnaoautorizacao = null");
		} else if(situacao.equals(NAO_AUTORIZADA)){
			sql.append("s.dtnaoautorizacao = ?");
		} else{
			sql.append("s.dtbaixa = ?");
		}
		
		sql.append(" where s.cdsolicitacaocompra in ("+whereIn+")");
		
		getHibernateTemplate().bulkUpdate(sql.toString(), situacao.equals(ESTORNAR) ? new Object[]{cdusuarioaltera,hoje} : new Object[]{cdusuarioaltera,hoje, hoje});
	}

	/**
	 * M�todo que busca a solicita��o da requisica��o que esta "ativa" ou seja n�o esta cancelada.
	 * 
	 * @param form
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Solicitacaocompra> getSolicitacoesDaRequisicao(Requisicaomaterial form, Boolean ativa) {
		QueryBuilder<Solicitacaocompra> query = query()
			.select("solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.identificador")
			.join("solicitacaocompra.solicitacaocompraorigem solicitacaocompraorigem")
			.join("solicitacaocompraorigem.requisicaomaterial requisicaomaterial")
			.where("requisicaomaterial = ?", form);
		
		if(ativa != null && ativa){
			query.where("solicitacaocompra.dtcancelamento is null");
		}
		
		return query.list();
	}

	/**
	 * M�todo que verifica se existe alguma outra solicita��o "ativa" (diferente de cancelada) gerada a partir
	 * da requisica��o de material
	 * 
	 * @param solicitacaocompra
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeSolicitacaoGeradaRequisicaoDiferenteDeCancelada(Solicitacaocompra solicitacaocompra) {
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(beanClass)
			.join("solicitacaocompra.aux_solicitacaocompra aux_solicitacaocompra")
			.join("solicitacaocompra.solicitacaocompraorigem solicitacaocompraorigem")
			.join("solicitacaocompraorigem.requisicaomaterial requisicaomaterial")
			.where("aux_solicitacaocompra.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
			.where("requisicaomaterial = ?", solicitacaocompra.getSolicitacaocompraorigem().getRequisicaomaterial())
			.unique() > 0;
	}

	/**
	 * M�todo que busca solicita��es escolhidas para inserir em uma cota��o
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Solicitacaocompra> findSolicitacoesParaInserirCotacao(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		return query()
			.select("solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.dtbaixa, solicitacaocompra.qtde, aux_solicitacaocompra.situacaosuprimentos, " +
					"localarmazenagem.cdlocalarmazenagem, material.cdmaterial, materialclasse.cdmaterialclasse, " +
					"frequencia.cdfrequencia, solicitacaocompra.qtdefrequencia, unidademedida.cdunidademedida ")
			.join("solicitacaocompra.aux_solicitacaocompra aux_solicitacaocompra")
			.join("solicitacaocompra.localarmazenagem localarmazenagem")
			.join("solicitacaocompra.frequencia frequencia")
			.join("solicitacaocompra.material material")
			.join("solicitacaocompra.materialclasse materialclasse")
			.join("solicitacaocompra.unidademedida unidademedida")
			.whereIn("solicitacaocompra.cdsolicitacaocompra", whereIn)
			.list();
	}

	/**
	 * M�todo que busca o �ltimo identificador utilizado
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getUltimoIdentificador() {
		return getJdbcTemplate().queryForInt("select nextval('sq_identificadorsolicitacaocompra')");
//		return newQueryBuilderSined(Integer.class)
//					.setUseWhereProjeto(false)
//					.select("max(identificador)")
//					.from(Solicitacaocompra.class)
//					.unique();
	}
	
	/**
	 * Chama a procedure de atualiza��o da tabela auxiliar de solicita��o de compra
	 *
	 * @param requisicaomaterial
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaSolicitacaocompra(Solicitacaocompra solicitacaocompra){
		if (solicitacaocompra == null || solicitacaocompra.getCdsolicitacaocompra() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		getJdbcTemplate().execute("SELECT ATUALIZA_SOLICITACAOCOMPRA(" + solicitacaocompra.getCdsolicitacaocompra() + ")");
	}

	/**
	 * Verifica se a lista de material tem alguma solicita��o de compra com situa��o diferente de autorizada e em aberto.
	 *
	 * @param orcamentomaterial
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean haveSolicitacao(Orcamentomaterial orcamentomaterial) {
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(Solicitacaocompra.class)
					.join("solicitacaocompra.solicitacaocompraorigem origem")
					.join("solicitacaocompra.aux_solicitacaocompra aux_solicitacaocompra")
					.where("origem.orcamentomaterial = ?",orcamentomaterial)
					.where("aux_solicitacaocompra.situacaosuprimentos <> ?",Situacaosuprimentos.AUTORIZADA)
					.where("aux_solicitacaocompra.situacaosuprimentos <> ?",Situacaosuprimentos.EM_ABERTO)
					.where("aux_solicitacaocompra.situacaosuprimentos <> ?",Situacaosuprimentos.CANCELADA)
					.unique() > 0;
	}

	/**
	 * M�todo que da update na solicita��o de compra colocando ou retirando o campo pedido
	 * 
	 * @param solicitacaocompra
	 * @param situacaosuprimentos
	 * @author Tom�s Rabelo
	 * @param coluna 
	 */
	public void doUpdateCompraSolicitacao(Solicitacaocompra solicitacaocompra, Integer coluna) {
		if(solicitacaocompra.getCdsolicitacaocompra() == null)
			throw new SinedException("A PK da solicita��o de compra n�o pode ser nula.");
		
		Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		
		String valorColuna = null;
		String sql = "update Solicitacaocompra s set s.cdusuarioaltera = ?, s.dtaltera = ?, ";
		
		if(coluna.equals(Solicitacaocompra.PEDIDO)){
			sql += "s.pedido = ? ";
			valorColuna = solicitacaocompra.getPedido();
		} else if(coluna.equals(Solicitacaocompra.ENTREGA)){
			sql += "s.entrega = ?";
			valorColuna = solicitacaocompra.getEntrega();
		} else if(coluna.equals(Solicitacaocompra.ROMANEIO)){
			 sql += "s.romaneio = ?";
			 valorColuna = solicitacaocompra.getRomaneio();
		} 
		
		if(valorColuna == null)
			sql = sql.substring(0, sql.length()-2)+"null ";
		
		sql +="where s.cdsolicitacaocompra = ?";
		
		if(valorColuna == null)
			getHibernateTemplate().bulkUpdate(sql, new Object[]{cdusuarioaltera,hoje,solicitacaocompra.getCdsolicitacaocompra()});
		else	
			getHibernateTemplate().bulkUpdate(sql, new Object[]{cdusuarioaltera,hoje,valorColuna,solicitacaocompra.getCdsolicitacaocompra()});
	}

	/**
	 * M�todo que busca as solicita��es de compra que possuem um local de entrega diferente do local de entrega da solicita��o de compra. 
	 * 
	 * @param localarmazenagemorigem
	 * @param localarmazenagemdestino
	 * @return
	 * @author Tom�s Rabelo
	 */
	@SuppressWarnings("unchecked")
	public List<Solicitacaocompra> getSolicitacoesParaRomaneio(Localarmazenagem localarmazenagemorigem, Localarmazenagem localarmazenagemdestino) {
		SinedUtil.markAsReader();
		List<Solicitacaocompra> list = getJdbcTemplate().query(
				"SELECT DISTINCT(S.CDSOLICITACAOCOMPRA), S.QTDE, M.NOME, UM.SIMBOLO, P.NOME " +
				"FROM SOLICITACAOCOMPRA S " +
				"LEFT JOIN PROJETO P ON P.CDPROJETO = S.CDPROJETO " + 
				"JOIN MATERIAL M ON M.CDMATERIAL = S.CDMATERIAL " +
				"JOIN LOCALARMAZENAGEM LAS ON LAS.CDLOCALARMAZENAGEM = S.CDLOCALARMAZENAGEM " + 
//				"JOIN UNIDADEMEDIDA UM ON UM.CDUNIDADEMEDIDA = M.CDUNIDADEMEDIDA " +
				"JOIN UNIDADEMEDIDA UM ON UM.CDUNIDADEMEDIDA = S.CDUNIDADEMEDIDA " +
				"JOIN COTACAOORIGEM CO ON CO.CDSOLICITACAOCOMPRA = S.CDSOLICITACAOCOMPRA " + 
				"JOIN COTACAO C ON C.CDCOTACAO = CO.CDCOTACAO " +
				"JOIN ORDEMCOMPRA OC ON OC.CDCOTACAO = C.CDCOTACAO " +
				"JOIN ENTREGA E ON E.CDORDEMCOMPRA = OC.CDORDEMCOMPRA " +
				"JOIN AUX_ENTREGA AE ON AE.CDENTREGA = E.CDENTREGA " +
				"WHERE E.CDLOCALARMAZENAGEM = "+localarmazenagemorigem.getCdlocalarmazenagem()+" AND " +
				"AE.SITUACAO <> 9 AND " +
				"E.CDENTREGA IN (" + 
				"SELECT E1.CDENTREGA " +  
				"FROM ENTREGA E1 " +
				"JOIN ENTREGAMATERIAL EM ON EM.CDENTREGA = E1.CDENTREGA " +
				"JOIN MATERIAL M1 ON M1.CDMATERIAL = EM.CDMATERIAL " +
				"JOIN ORDEMCOMPRA OC1 ON E1.CDORDEMCOMPRA = OC1.CDORDEMCOMPRA " +
				"JOIN COTACAO C1 ON C1.CDCOTACAO = OC1.CDCOTACAO " +
				"JOIN COTACAOFORNECEDOR CF ON CF.CDCOTACAO = C1.CDCOTACAO " +
				"JOIN COTACAOFORNECEDORITEM CFI ON CFI.CDCOTACAOFORNECEDOR = CF.CDCOTACAOFORNECEDOR " + 
				"WHERE CFI.CDLOCALARMAZENAGEMSOLICITACAO = "+localarmazenagemdestino.getCdlocalarmazenagem()+" AND " +
				"M1.CDMATERIAL = CFI.CDMATERIAL AND " +
				"M.CDMATERIAL = CFI.CDMATERIAL AND " +
				"E1.CDLOCALARMAZENAGEM = E.CDLOCALARMAZENAGEM)"
				,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						Solicitacaocompra solicitacaocompra = new Solicitacaocompra();
						solicitacaocompra.setCdsolicitacaocompra(rs.getInt("CDSOLICITACAOCOMPRA"));
						solicitacaocompra.setQtde(rs.getDouble("QTDE"));
						solicitacaocompra.setMaterial(new Material(rs.getString("NOME")));
//						solicitacaocompra.getMaterial().setUnidademedida(new Unidademedida(rs.getString("SIMBOLO")));
						solicitacaocompra.setUnidademedida(new Unidademedida(rs.getString("SIMBOLO")));
						solicitacaocompra.setProjeto(new Projeto(rs.getString("NOME")));
						
						return solicitacaocompra;
					}
				});
			
		return list;
	}

	/**
	 * Busca as solicita��es de compra das entregas, que possuiem local de entrega diferentes.
	 * 
	 * @param whereInEntrega
	 * @return
	 * @author Tom�s Rabelo
	 */
	@SuppressWarnings("unchecked")
	public List<Solicitacaocompra> getSolicitacoesDasEntregas(String whereInEntrega) {
		SinedUtil.markAsReader();
		List<Solicitacaocompra> list = getJdbcTemplate().query(
				"SELECT DISTINCT(S.CDSOLICITACAOCOMPRA), S.QTDE, S.DESCRICAO, M.NOME, COALESCE(UM.SIMBOLO, UMM.SIMBOLO) AS SIMBOLO "+ 
				"FROM SOLICITACAOCOMPRA S "+
				"JOIN MATERIAL M ON M.CDMATERIAL = S.CDMATERIAL "+
				"LEFT OUTER JOIN UNIDADEMEDIDA UMM ON UMM.CDUNIDADEMEDIDA = M.CDUNIDADEMEDIDA " +
				"LEFT OUTER JOIN UNIDADEMEDIDA UM ON UM.CDUNIDADEMEDIDA = S.CDUNIDADEMEDIDA "+
				"JOIN COTACAOORIGEM CO ON CO.CDSOLICITACAOCOMPRA = S.CDSOLICITACAOCOMPRA "+ 
				"JOIN COTACAO C ON C.CDCOTACAO = CO.CDCOTACAO "+
				"JOIN ORDEMCOMPRA OC ON OC.CDCOTACAO = C.CDCOTACAO "+
				"JOIN ENTREGA E ON E.CDORDEMCOMPRA = OC.CDORDEMCOMPRA "+
				"JOIN ENTREGADOCUMENTO ED ON ED.CDENTREGA = E.CDENTREGA "+
				"JOIN ENTREGAMATERIAL EM ON EM.CDENTREGADOCUMENTO = ED.CDENTREGADOCUMENTO "+
				"WHERE S.CDLOCALARMAZENAGEM <> E.CDLOCALARMAZENAGEM AND "+ 
				"EM.CDMATERIAL = S.CDMATERIAL AND "+
				"E.CDENTREGA IN ("+whereInEntrega+") AND "+ 
				"S.CDSOLICITACAOCOMPRA NOT IN ( "+
					"SELECT S1.CDSOLICITACAOCOMPRA "+
					"FROM ROMANEIOSOLICITACAOITEM RSI "+
					"JOIN SOLICITACAOCOMPRA S1 ON S1.CDSOLICITACAOCOMPRA = RSI.CDSOLICITACAOCOMPRA "+ 
					"WHERE S1.CDSOLICITACAOCOMPRA = S.CDSOLICITACAOCOMPRA "+
					"GROUP BY S1.CDSOLICITACAOCOMPRA HAVING SUM(RSI.QUANTIDADEFORNECIDA) > S.QTDE)"
				,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						Solicitacaocompra solicitacaocompra = new Solicitacaocompra();
						solicitacaocompra.setCdsolicitacaocompra(rs.getInt("CDSOLICITACAOCOMPRA"));
						solicitacaocompra.setQtde(rs.getDouble("QTDE"));
						solicitacaocompra.setDescricao(rs.getString("DESCRICAO"));
						solicitacaocompra.setMaterial(new Material(rs.getString("NOME")));
//						solicitacaocompra.getMaterial().setUnidademedida(new Unidademedida(rs.getString("SIMBOLO")));
						solicitacaocompra.setUnidademedida(new Unidademedida(rs.getString("SIMBOLO")));
						
						return solicitacaocompra;
					}
				});
			
		return list;
	}
	
	public List<Solicitacaocompra> findSolicitacaoCompra(String whereIn, Integer cdsolicitacaocompra, Boolean mesmaSituacao) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		QueryBuilder<Solicitacaocompra> qry = querySined();
		qry
				.select("solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.identificador, solicitacaocompra.descricao, solicitacaocompra.dtlimite, " +
						"solicitacaocompra.observacao, projeto.cdprojeto, projeto.nome, centrocusto.cdcentrocusto, aux_solicitacaocompra.situacaosuprimentos, " +
						"aux_solicitacaocompra.pendenciaqtde, solicitacaocompra.qtde, " +
						"centrocusto.nome, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, material.cdmaterial, " +
						"material.nome, material.identificacao, material.qtdeunidade, unidademedida.cdunidademedida, unidademedida.simbolo, " +
						"unidademedidaMaterial.cdunidademedida, unidademedidaMaterial.simbolo, " +
						"frequencia.cdfrequencia, frequencia.nome," +
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia," +
						"logomarca.cdarquivo, logomarca.nome ")
				.leftOuterJoin("solicitacaocompra.aux_solicitacaocompra aux_solicitacaocompra")
				.leftOuterJoin("solicitacaocompra.projeto projeto")
				.leftOuterJoin("solicitacaocompra.centrocusto centrocusto")
				.leftOuterJoin("solicitacaocompra.localarmazenagem localarmazenagem")
				.leftOuterJoin("solicitacaocompra.frequencia frequencia")
				.leftOuterJoin("solicitacaocompra.empresa empresa")
				.leftOuterJoin("empresa.logomarca logomarca")
				.leftOuterJoin("solicitacaocompra.material material")
				.leftOuterJoin("material.unidademedida unidademedidaMaterial")
				.leftOuterJoin("solicitacaocompra.unidademedida unidademedida");
		
		if(mesmaSituacao)
			qry.where("solicitacaocompra.identificador in (select sol.identificador from Solicitacaocompra sol join sol.aux_solicitacaocompra auxs where sol.id = "+cdsolicitacaocompra+" and auxs.situacaosuprimentos = aux_solicitacaocompra.situacaosuprimentos)");
		else
			qry.whereIn("solicitacaocompra.identificador", whereIn);
		
		return qry.list();
	}
	
	/**
	 * M�todo que verifica se h� solicita��es de compra com a mesma identifica��o e situa��o diferente
	 * 
	 * @param solicitacaocompra
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Boolean existeSolicitacaoComSituacaoDiferente(Solicitacaocompra solicitacaocompra) {
		if(solicitacaocompra == null || solicitacaocompra.getCdsolicitacaocompra() == null){
			throw new SinedException("Solicitacaocompra n�o pode ser nula.");
		}
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Solicitacaocompra.class)
			.join("solicitacaocompra.aux_solicitacaocompra aux_solicitacaocompra")
			.where("solicitacaocompra.identificador in (select sol.identificador from Solicitacaocompra sol join sol.aux_solicitacaocompra auxs where sol = ? and auxs.situacaosuprimentos <> aux_solicitacaocompra.situacaosuprimentos)", solicitacaocompra)
			.unique() > 0;
	}
	
	/**
	 * M�todo que busca solicitacaocompra para enviar email
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Solicitacaocompra> findForEnvioemailcompra(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro n�o podem ser nulo.");
		
		return query()
				.select("solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.identificador, empresa.cdpessoa, empresa.email, " +
						"projeto.cdprojeto, materialgruposol.cdmaterialgrupo, materialgrupo.cdmaterialgrupo, material.cdmaterial, " +
						"listaMaterialgrupousuario.cdmaterialgrupousuario, usuario.cdpessoa, " +
						"listaMaterialgrupousuariosol.cdmaterialgrupousuario, usuariosol.cdpessoa ")
				.leftOuterJoin("solicitacaocompra.empresa empresa")
				.leftOuterJoin("solicitacaocompra.projeto projeto")
				.leftOuterJoin("solicitacaocompra.materialgrupo materialgruposol")
				.leftOuterJoin("materialgruposol.listaMaterialgrupousuario listaMaterialgrupousuariosol")
				.leftOuterJoin("listaMaterialgrupousuariosol.usuario usuariosol")
				.leftOuterJoin("solicitacaocompra.material material")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("materialgrupo.listaMaterialgrupousuario listaMaterialgrupousuario")
				.leftOuterJoin("listaMaterialgrupousuario.usuario usuario")
				.whereIn("solicitacaocompra.cdsolicitacaocompra", whereIn)
				.list();
	}

	/**
	 * M�todo que verifica se tem solicita��ocom situa��o diferente de Em Aberto
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existSolicitacaoDiferenteEmAberto(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
			.setUseTranslator(Boolean.FALSE)
			.select("count(*)")
			.from(Solicitacaocompra.class)
			.join("solicitacaocompra.aux_solicitacaocompra aux_solicitacaocompra")
			.whereIn("solicitacaocompra.cdsolicitacaocompra", whereIn)
			.where("aux_solicitacaocompra.situacaosuprimentos <> ?", Situacaosuprimentos.EM_ABERTO)
			.unique() > 0;
	}
	
	/**
	 * Carrega os dados da solicitaca��o para gerar ordem de compra
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Solicitacaocompra> findForOrdemcompra(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Lista de solicita��es n�o pode ser nulo.");
		}
		return query()
					.select("solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.identificador, material.cdmaterial, material.nome, materialclasse.cdmaterialclasse, materialclasse.nome, " +
							"material.produto, material.patrimonio, material.epi, material.servico, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
							"solicitacaocompra.qtde, solicitacaocompra.descricao, frequencia.cdfrequencia, frequencia.nome, solicitacaocompra.qtdefrequencia," +
							"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, unidademedidamaterial.cdunidademedida," +
							"unidademedidamaterial.nome, unidademedidamaterial.simbolo, contagerencial.cdcontagerencial, contagerencial.nome," +
							"centrocusto.cdcentrocusto, centrocusto.nome, projeto.cdprojeto, projeto.nome, solicitacaocompra.dtlimite, material.tempoentrega," +
							"solicitacaocompra.observacao, empresa.cdpessoa, empresa.nome, empresa.razaosocial, solicitacaocompra.faturamentocliente, solicitacaocompra.altura, " +
							"solicitacaocompra.largura, solicitacaocompra.comprimento, solicitacaocompra.pesototal, solicitacaocompra.qtdvolume, vcontagerencial.identificador, " +
							"vcontagerencial.arvorepai, " +
							"contagerencialSol.cdcontagerencial, contagerencialSol.nome, vcontagerencialSol.identificador, vcontagerencialSol.arvorepai ")
					.leftOuterJoin("solicitacaocompra.material material")
					.leftOuterJoin("material.contagerencial contagerencial")
					.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
					.leftOuterJoin("material.unidademedida unidademedidamaterial")
					.leftOuterJoin("solicitacaocompra.frequencia frequencia")
					.leftOuterJoin("solicitacaocompra.materialclasse materialclasse")
					.leftOuterJoin("solicitacaocompra.localarmazenagem localarmazenagem")
					.leftOuterJoin("solicitacaocompra.unidademedida unidademedida")
					.leftOuterJoin("solicitacaocompra.centrocusto centrocusto")
					.leftOuterJoin("solicitacaocompra.projeto projeto")
					.leftOuterJoin("solicitacaocompra.empresa empresa")
					.leftOuterJoin("solicitacaocompra.contagerencial contagerencialSol")
					.leftOuterJoin("contagerencialSol.vcontagerencial vcontagerencialSol")
					.whereIn("solicitacaocompra.cdsolicitacaocompra", whereIn)
					.list();
	}
	
	/**
	 * M�todo que retorna a quantidade de solicita��o de compra do pedido de venda
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Double getQteSolicitacaoByPedidovenda(Pedidovenda pedidovenda) {
		
		Long qtde = newQueryBuilderSined(Long.class)
						.select("count(*)")
						.from(Solicitacaocompra.class)
						.join("solicitacaocompra.solicitacaocompraorigem solicitacaocompraorigem")
						.join("solicitacaocompra.aux_solicitacaocompra aux_solicitacaocompra")
						.where("aux_solicitacaocompra.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
						.where("solicitacaocompraorigem.pedidovenda = ?", pedidovenda)
						.unique();
		return qtde != null ? qtde.doubleValue() : 0.0;
	}
	
	/**
	 * M�todo que retorna a qtde solicitada do or�amento
	 *
	 * @param vendaorcamento
	 * @return
	 * @author Luiz Fernando
	 * @since 13/09/2013
	 */
	public Double getQteSolicitacaoByOrcamento(Vendaorcamento vendaorcamento) {
		
		Long qtde = newQueryBuilderSined(Long.class)
						.select("count(*)")
						.from(Solicitacaocompra.class)
						.join("solicitacaocompra.solicitacaocompraorigem solicitacaocompraorigem")
						.join("solicitacaocompraorigem.vendaorcamento vendaorcamento")
						.join("solicitacaocompra.aux_solicitacaocompra aux_solicitacaocompra")
						.where("aux_solicitacaocompra.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
						.where("vendaorcamento = ?", vendaorcamento)
						.unique();
		return qtde != null ? qtde.doubleValue() : 0.0;
	}

	/**
	 * M�todo que retorna a quantidade de solicita��o de compra baixada do pedido de venda
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Double getQteSolicitacaoBaixadaByPedidovenda(Pedidovenda pedidovenda) {
		Long qtde = newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Solicitacaocompra.class)
				.join("solicitacaocompra.solicitacaocompraorigem solicitacaocompraorigem")
				.join("solicitacaocompra.aux_solicitacaocompra aux_solicitacaocompra")
				.where("solicitacaocompraorigem.pedidovenda = ?", pedidovenda)
				.where("aux_solicitacaocompra.situacaosuprimentos = ?", Situacaosuprimentos.BAIXADA)
				.unique();
		return qtde != null ? qtde.doubleValue() : 0.0;
	}
	
	/**
	 * M�todo que retorna a qtde comprada do or�amento
	 *
	 * @param vendaorcamento
	 * @return
	 * @author Luiz Fernando
	 * @since 13/09/2013
	 */
	public Double getQteSolicitacaoBaixadaByOrcamento(Vendaorcamento vendaorcamento) {
		Long qtde = newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Solicitacaocompra.class)
				.join("solicitacaocompra.solicitacaocompraorigem solicitacaocompraorigem")
				.join("solicitacaocompra.aux_solicitacaocompra aux_solicitacaocompra")
				.join("solicitacaocompraorigem.vendaorcamento vendaorcamento")
				.where("vendaorcamento = ?", vendaorcamento)
				.where("aux_solicitacaocompra.situacaosuprimentos = ?", Situacaosuprimentos.BAIXADA)
				.unique();
		return qtde != null ? qtde.doubleValue() : 0.0;
	}

	/**
	 * Busca a quantidade restante que falta a solicita��o de compra.
	 *
	 * @param cdsolicitacaocompra
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/11/2012
	 */
	@SuppressWarnings("unchecked")
	public Double getQtdeRestanteSolicitacaoOrdemcompra(Integer cdsolicitacaocompra) {
		String sql = "SELECT qtde_unidadeprincipal(SC.CDMATERIAL, SC.CDUNIDADEMEDIDA, SC.QTDE) - (SELECT sum(QTDE) " +
					    "FROM " +
					    "( " +
					    "SELECT qtde_unidadeprincipal(OCM.CDMATERIAL, OCM.CDUNIDADEMEDIDA, SCOCM.QTDE) AS QTDE " +
					    "FROM ORDEMCOMPRAMATERIAL OCM " +
					    "JOIN ORDEMCOMPRA OC ON OC.CDORDEMCOMPRA = OCM.CDORDEMCOMPRA " +
					    "JOIN ORDEMCOMPRAORIGEM OCORI ON OCORI.CDORDEMCOMPRA = OC.CDORDEMCOMPRA " +
					    "JOIN AUX_ORDEMCOMPRA AUX ON AUX.CDORDEMCOMPRA = OC.CDORDEMCOMPRA " +
					    "JOIN SOLICITACAOCOMPRAORDEMCOMPRAMATERIAL SCOCM ON SCOCM.CDORDEMCOMPRAMATERIAL = OCM.CDORDEMCOMPRAMATERIAL AND SCOCM.CDSOLICITACAOCOMPRA = SC.CDSOLICITACAOCOMPRA " +
					    "WHERE OCORI.CDSOLICITACAOCOMPRA = SC.CDSOLICITACAOCOMPRA " +
					    "AND OCM.CDMATERIAL = SC.CDMATERIAL " +
					    "AND AUX.situacao <> 9 " +
					
					    "UNION ALL " +
					
					    "SELECT qtde_unidadeprincipal(OCM.CDMATERIAL, OCM.CDUNIDADEMEDIDA, SCOCM.QTDE) AS QTDE " +
					    "FROM ORDEMCOMPRAMATERIAL OCM " +
					    "JOIN ORDEMCOMPRA OC ON OC.CDORDEMCOMPRA = OCM.CDORDEMCOMPRA " +
					    "JOIN COTACAOORIGEM COTORI ON COTORI.CDCOTACAO = OC.CDCOTACAO " +
					    "JOIN AUX_ORDEMCOMPRA AUX ON AUX.CDORDEMCOMPRA = OC.CDORDEMCOMPRA " +
					    "JOIN SOLICITACAOCOMPRAORDEMCOMPRAMATERIAL SCOCM ON SCOCM.CDORDEMCOMPRAMATERIAL = OCM.CDORDEMCOMPRAMATERIAL AND SCOCM.CDSOLICITACAOCOMPRA = SC.CDSOLICITACAOCOMPRA " +
					    "WHERE COTORI.CDSOLICITACAOCOMPRA = SC.CDSOLICITACAOCOMPRA " +
					    "AND OCM.CDMATERIAL = SC.CDMATERIAL " +
					    "AND AUX.situacao <> 9 " +
					    ") QUERY)" +
					    " - " +
					    "(SELECT COALESCE(sum(COALESCE(qtde_unidadeprincipal(SC2.CDMATERIAL, SC2.CDUNIDADEMEDIDA, SC2.QTDE),0)), 0) " +
					    "FROM SOLICITACAOCOMPRA SC2 " +
					    "JOIN SOLICITACAOCOMPRAORIGEM SCO ON SCO.CDSOLICITACAOCOMPRAORIGEM = SC2.CDSOLICITACAOCOMPRAORIGEM " +
					    "JOIN AUX_SOLICITACAOCOMPRA AUX ON AUX.CDSOLICITACAOCOMPRA = SC2.CDSOLICITACAOCOMPRA " +
					    "WHERE SCO.CDSOLICITACAOCOMPRA = " + cdsolicitacaocompra +
					    "AND AUX.situacao <> 9) " +
					    " AS QUANTIDADE " +
						"FROM SOLICITACAOCOMPRA SC " +
						"WHERE SC.CDSOLICITACAOCOMPRA = " + cdsolicitacaocompra;

		SinedUtil.markAsReader();
		List<Double> listaUpdate = getJdbcTemplate().query(sql, 
			new RowMapper() {
				public Double mapRow(ResultSet rs, int rowNum) throws SQLException {
					return rs.getDouble("QUANTIDADE");
				}
			});
		
		if(listaUpdate == null || listaUpdate.size() == 0) return null;
		else return listaUpdate.get(0);
	}

	/**
	 * Busca as solicita��es de compra que passa pela cota��o a partir do item da ordem de compra.
	 *
	 * @param ordemcompramaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/12/2012
	 */
	public List<Solicitacaocompra> findSolicitacaocompraByOrdemcompramaterialComCotacaoForRomaneio(Ordemcompramaterial ordemcompramaterial) {
		if(ordemcompramaterial == null || ordemcompramaterial.getCdordemcompramaterial() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.qtde, localarmazenagem.cdlocalarmazenagem, " +
							"localarmazenagem.nome, material.cdmaterial, material.nome, material.identificacao, " +
							"materialclasse.cdmaterialclasse, materialclasse.nome")
					.join("solicitacaocompra.materialclasse materialclasse")
					.join("solicitacaocompra.material material")
					.join("solicitacaocompra.localarmazenagem localarmazenagem")
					.join("solicitacaocompra.listaCotacaoorigem cotacaoorigem")
					.join("cotacaoorigem.cotacao cotacao")
					.join("cotacao.listaOrdemcompra ordemcompra")
					.join("ordemcompra.listaMaterial ordemcompramaterial")
					.where("ordemcompramaterial = ?", ordemcompramaterial)
					.where("material = ?", ordemcompramaterial.getMaterial(), ordemcompramaterial.getMaterial() != null)
					.list();
	}

	/**
	 * Busca as solicita��es de compra N�O passa pela cota��o a partir do item da ordem de compra.
	 *
	 * @param ordemcompramaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/12/2012
	 */
	public List<Solicitacaocompra> findSolicitacaocompraByOrdemcompramaterialSemCotacaoForRomaneio(Ordemcompramaterial ordemcompramaterial) {
		if(ordemcompramaterial == null || ordemcompramaterial.getCdordemcompramaterial() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.qtde, localarmazenagem.cdlocalarmazenagem, " +
							"localarmazenagem.nome, material.cdmaterial, material.nome, material.identificacao, " +
							"materialclasse.cdmaterialclasse, materialclasse.nome")
					.join("solicitacaocompra.materialclasse materialclasse")
					.join("solicitacaocompra.material material")
					.join("solicitacaocompra.localarmazenagem localarmazenagem")
					.join("solicitacaocompra.listaOrdemcompraorigem ordemcompraorigem")
					.join("ordemcompraorigem.ordemcompra ordemcompra")
					.join("ordemcompra.listaMaterial ordemcompramaterial")
					.where("ordemcompramaterial = ?", ordemcompramaterial)
					.where("material = ?", ordemcompramaterial.getMaterial(), ordemcompramaterial.getMaterial() != null)
					.list();
	}

	@SuppressWarnings("unchecked")
	public Double getQtdeComprada(Solicitacaocompra solicitacaocompra) {
		
		String sql = "select sum(qtde) as qtde " +

						"from ( " +
						
						"select ocem.qtde as qtde " +
						
						"from entregamaterial em " +
						
						"join entregadocumento ed on ed.cdentregadocumento = em.cdentregadocumento " +
						"join entrega e on e.cdentrega = ed.cdentrega " +
						
						"join ordemcompraentregamaterial ocem on ocem.cdentregamaterial = em.cdentregamaterial " +
						"join ordemcompramaterial ocm on ocm.cdordemcompramaterial = ocem.cdordemcompramaterial " +
						"join ordemcompra oc on oc.cdordemcompra = ocm.cdordemcompra " +
						"join cotacaoorigem co on co.cdcotacao = oc.cdcotacao " +
						"join solicitacaocompra sc on sc.cdsolicitacaocompra = co.cdsolicitacaocompra " +
						
						"where sc.cdsolicitacaocompra = " + solicitacaocompra.getCdsolicitacaocompra() + " " +
						"and sc.cdmaterial = em.cdmaterial " +
						"and sc.cdlocalarmazenagem = coalesce(em.cdlocalarmazenagem, e.cdlocalarmazenagem) " +
						
						"union all " +
						
						"select ocem.qtde as qtde " +
						
						"from entregamaterial em " +
						
						"join entregadocumento ed on ed.cdentregadocumento = em.cdentregadocumento " +
						"join entrega e on e.cdentrega = ed.cdentrega " +
						
						"join ordemcompraentregamaterial ocem on ocem.cdentregamaterial = em.cdentregamaterial " +
						"join ordemcompramaterial ocm on ocm.cdordemcompramaterial = ocem.cdordemcompramaterial " +
						"join ordemcompra oc on oc.cdordemcompra = ocm.cdordemcompra " +
						"join ordemcompraorigem oco on oco.cdordemcompra = oc.cdordemcompra " +
						"join solicitacaocompra sc on sc.cdsolicitacaocompra = oco.cdsolicitacaocompra " +
						
						"where sc.cdsolicitacaocompra = " + solicitacaocompra.getCdsolicitacaocompra() + " " +
						"and sc.cdmaterial = em.cdmaterial " +
						"and sc.cdlocalarmazenagem = coalesce(em.cdlocalarmazenagem, e.cdlocalarmazenagem) " +
						
						") query";

		SinedUtil.markAsReader();
		List<Double> listaUpdate = getJdbcTemplate().query(sql, 
		new RowMapper() {
			public Double mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getDouble("QTDE");
			}
		});
		
		return listaUpdate != null && listaUpdate.size() > 0 ? listaUpdate.get(0) : 0d;
	}

	/**
	 * M�todo que carrega os dados da solicitacao de compra para relat�rio - prazo m�dio de compra
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Solicitacaocompra> findForReportPrazomediocompra(EmitirprazomediocompraFiltro filtro) {
		QueryBuilder<Solicitacaocompra> query = query();
		
		query
			.select("solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.identificador, solicitacaocompra.dtlimite, " +
					"solicitacaocompra.dtbaixa, solicitacaocompra.dtautorizacao, solicitacaocompra.dtsolicitacao, " +
					"material.cdmaterial, material.nome, material.tempoentrega, material.metaprazomediocompra, " +
					"material.toleranciaprazocompra, colaborador.cdpessoa, colaborador.nome, " +
					"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
					"listaCotacaoorigem.cdcotacaoorigem, cotacao.cdcotacao, " +
					"listaOrdemcompra.cdordemcompra, listaOrdemcompra.dtcriacao, listaOrdemcompra.dtpedido, " +
					"listaOrdemcompra.dtautorizacao, listaOrdemcompra.dtaprova, listaOrdemcompra.dtbaixa, " +
					"listaOrdemcompraentrega.cdordemcompraentrega, entrega.dtentrega, " +
					
					"listaOrdemcompraorigem.cdordemcompraorigem, " +
					"ordemcompra.cdordemcompra, ordemcompra.dtcriacao, ordemcompra.dtpedido, " +
					"ordemcompra.dtautorizacao, ordemcompra.dtaprova, ordemcompra.dtbaixa, " +
					"listaOrdemcompraentrega2.cdordemcompraentrega, entrega2.dtentrega, " +
					"ordemcompraacao.cdordemcompraacao, ordemcompraacao.nome, " +
					"listaOrdemcomprahistorico.dtaltera, listaOrdemcomprahistorico.cdusuarioaltera,  " +
					
					"listaSolicitacaocomprahistorico.dtaltera, listaSolicitacaocomprahistorico.cdusuarioaltera, " +
					"solicitacaocompraacao.cdsolicitacaocompraacao, solicitacaocompraacao.nome, " +
					"ordemcompraacao2.cdordemcompraacao, ordemcompraacao2.nome, " +
					"listaOrdemcomprahistorico2.dtaltera, listaOrdemcomprahistorico2.cdusuarioaltera," +
					
					"logomarca.cdarquivo ")
			.leftOuterJoin("solicitacaocompra.empresa empresa")
			.leftOuterJoin("empresa.logomarca logomarca")
			.leftOuterJoin("solicitacaocompra.material material")
			.leftOuterJoin("solicitacaocompra.localarmazenagem localarmazenagem")
			.leftOuterJoin("solicitacaocompra.colaborador colaborador")
			.leftOuterJoin("solicitacaocompra.listaSolicitacaocomprahistorico listaSolicitacaocomprahistorico")
			.leftOuterJoin("listaSolicitacaocomprahistorico.solicitacaocompraacao solicitacaocompraacao")
			
			.leftOuterJoin("solicitacaocompra.listaCotacaoorigem listaCotacaoorigem")
			.leftOuterJoin("listaCotacaoorigem.cotacao cotacao")
			.leftOuterJoin("cotacao.listaOrdemcompra listaOrdemcompra")
			.leftOuterJoin("listaOrdemcompra.fornecedor fornecedor")
			.leftOuterJoin("listaOrdemcompra.listaOrdemcomprahistorico listaOrdemcomprahistorico")
			.leftOuterJoin("listaOrdemcomprahistorico.ordemcompraacao ordemcompraacao")
			.leftOuterJoin("listaOrdemcompra.listaOrdemcompraentrega listaOrdemcompraentrega")
			.leftOuterJoin("listaOrdemcompraentrega.entrega entrega")
			
			.leftOuterJoin("solicitacaocompra.listaOrdemcompraorigem listaOrdemcompraorigem")
			.leftOuterJoin("listaOrdemcompraorigem.ordemcompra ordemcompra")
			.leftOuterJoin("ordemcompra.fornecedor fornecedor2")
			.leftOuterJoin("ordemcompra.listaOrdemcomprahistorico listaOrdemcomprahistorico2")
			.leftOuterJoin("listaOrdemcomprahistorico2.ordemcompraacao ordemcompraacao2")
			.leftOuterJoin("ordemcompra.listaOrdemcompraentrega listaOrdemcompraentrega2")
			.leftOuterJoin("listaOrdemcompraentrega2.entrega entrega2")
			
			.where("solicitacaocompra.dtsolicitacao >= ?", filtro.getDtInicio())
			.where("solicitacaocompra.dtsolicitacao <= ?", filtro.getDtFim())
			.where("material = ?", filtro.getMaterial())
			.where("localarmazenagem = ?", filtro.getLocalarmazenagem());
			
			if(filtro.getMaterialcategoria() != null){
				materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
				query
				.leftOuterJoin("material.materialcategoria materialcategoria")
				.leftOuterJoin("materialcategoria.vmaterialcategoria vmaterialcategoria")
				.openParentheses()
					.where("vmaterialcategoria.identificador like ? ||'.%'", filtro.getMaterialcategoria().getIdentificador())
				.or()
					.where("vmaterialcategoria.identificador like ? ", filtro.getMaterialcategoria().getIdentificador())
				.closeParentheses();
			}
						
			query.openParentheses()
				.where("fornecedor = ?", filtro.getFornecedor())
				.or()
				.where("fornecedor2 = ?", filtro.getFornecedor())
			.closeParentheses();
				
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioService.isRestricaoFornecedorColaborador(usuarioLogado)){
			query
				.openParentheses()
				.where("("+
							"exists("+
								"select 1 from ColaboradorFornecedor cf "+
								"where "+
								"cf.fornecedor =  fornecedor and "+
								"cf.colaborador = "+usuarioLogado.getCdpessoa()+
							") "+
							"or "+
							"(" +
								"not exists("+
									"select 1 from ColaboradorFornecedor cf "+
									"where "+
									"cf.fornecedor =  fornecedor"+
								") and " +
								"fornecedor != null"+
							")"+
						")")
				.or()
				.where("("+
							"exists("+
								"select 1 from ColaboradorFornecedor cf "+
								"where "+
								"cf.fornecedor =  fornecedor2 and "+
								"cf.colaborador = "+usuarioLogado.getCdpessoa()+
							") "+
							"or "+
							"(" +
								"not exists("+
									"select 1 from ColaboradorFornecedor cf "+
									"where "+
									"cf.fornecedor =  fornecedor2"+
								") and " +
								"fornecedor2 != null"+
							")"+
								
						")")					
				.closeParentheses();
		}
		
		if(SinedUtil.isRestricaoEmpresaFornecedorUsuarioLogado()){
			query.openParentheses()
				.where("true = permissao_fornecedorempresa(fornecedor.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "')")
				.or()
				.where("true = permissao_fornecedorempresa(fornecedor2.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "')")
			.closeParentheses();
		}
		return query.list();
	}
	
	/**
	* M�todo que carrega as solicitacoes de compra para o resumo de quantidade e peso
	*
	* @param filtro
	* @return
	* @since 06/01/2015
	* @author Luiz Fernando
	*/
	public List<Solicitacaocompra> findForResumo(SolicitacaocompraFiltro filtro) {
		QueryBuilder<Solicitacaocompra> query = query();
		this.updateListagemQuery(query, filtro);
		
		query.select("solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.qtde, solicitacaocompra.pesototal, " +
				"unidademedida.cdunidademedida, unidademedida.nome, " +
				"unidademedidaMaterial.cdunidademedida, unidademedidaMaterial.nome ");
		query.leftOuterJoin("material.unidademedida unidademedidaMaterial");
		query.setIgnoreJoinPaths(new HashSet<String>());
		
		return query.list();
	}
	
	/**
	* M�todo que carrega as solicita��es com as origens
	*
	* @param whereIn
	* @return
	* @since 03/09/2015
	* @author Luiz Fernando
	*/
	public List<Solicitacaocompra> findWithOrigem(String whereIn) {
		return query()
				.select("solicitacaocompra.cdsolicitacaocompra, solicitacaocompraorigem.cdsolicitacaocompraorigem, requisicaomaterial.cdrequisicaomaterial ")
				.leftOuterJoin("solicitacaocompra.solicitacaocompraorigem solicitacaocompraorigem")
				.leftOuterJoin("solicitacaocompraorigem.requisicaomaterial requisicaomaterial")
				.whereIn("solicitacaocompra.cdsolicitacaocompra", whereIn)
				.list();
	}
	
	/**
	* M�todo que faz o update do vinculo de origem com a solicita��o de compra
	*
	* @param solicitacaocompra
	* @param solicitacaocompraorigem
	* @since 03/09/2015
	* @author Luiz Fernando
	*/
	public void updateVinculoSolicitacaocompraorigem(Solicitacaocompra solicitacaocompra, Solicitacaocompraorigem solicitacaocompraorigem) {
		if(solicitacaocompra != null && solicitacaocompra.getCdsolicitacaocompra() != null && solicitacaocompraorigem != null){
			String sql = "update Solicitacaocompra sc set sc.solicitacaocompraorigem = ? where sc.cdsolicitacaocompra = ? ";
			getHibernateTemplate().bulkUpdate(sql, new Object[]{solicitacaocompraorigem, solicitacaocompra.getCdsolicitacaocompra()});
		}
	}
	
	/**
	* M�todo que carrega a solicita��o de compra com o identificador
	*
	* @param whereIn
	* @return
	* @since 10/09/2015
	* @author Luiz Fernando
	*/
	public List<Solicitacaocompra> findWithIdentificador(String whereIn) {
		return query()
			.select("solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.identificador")
			.whereIn("solicitacaocompra.cdsolicitacaocompra", whereIn)
			.list();
	}
	
	/**
	* M�todo que faz update no campo pendenciaqtde da tabela aux_solicitacaocompra
	*
	* @param whereIn
	* @param pendenciaQtde
	* @since 14/09/2015
	* @author Luiz Fernando
	*/
	public void doUpdatePendenciaQtde(String whereIn, Boolean pendenciaQtde) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		String sql = "update Aux_solicitacaocompra aux_s set pendenciaqtde = ?  where aux_s.cdsolicitacaocompra in ("+whereIn+")";
		getHibernateTemplate().bulkUpdate(sql.toString(), new Object[]{pendenciaQtde});
	}
	
	/**
	* M�todo que carrega a solicita��o de compra com a unidade de medida
	*
	* @param whereIn
	* @return
	* @since 21/09/2015
	* @author Luiz Fernando
	*/
	public List<Solicitacaocompra> findWithUnidademedida(String whereIn) {
		return query()
			.select("solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.identificador, solicitacaocompra.qtde, unidademedida.cdunidademedida, " +
					"material.cdmaterial, unidademedidaM.cdunidademedida ")
			.join("solicitacaocompra.unidademedida unidademedida")
			.join("solicitacaocompra.material material")
			.join("material.unidademedida unidademedidaM")
			.whereIn("solicitacaocompra.cdsolicitacaocompra", whereIn)
			.list();
	}
	
	/**
	* M�todo que retorna as solicita��es de compra que tenham ordem de compra em aberto
	*
	* @param whereIn
	* @return
	* @since 25/09/2015
	* @author Luiz Fernando
	*/
	public List<Solicitacaocompra> findForBaixar(String whereIn) {
		return query()
				.select("solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.identificador")
				.whereIn("solicitacaocompra.cdsolicitacaocompra", whereIn)
				.where("not exists (SELECT scorigem.cdsolicitacaocompra FROM Ordemcompraorigem oco " +
					"JOIN oco.solicitacaocompra scorigem " +
					"JOIN oco.ordemcompra ocorigem " +
					"JOIN ocorigem.aux_ordemcompra aux_ocorigem " +
					"where scorigem.cdsolicitacaocompra = solicitacaocompra.cdsolicitacaocompra and " +
					"aux_ocorigem.situacaosuprimentos <> " + Situacaosuprimentos.BAIXADA.getCodigo() + " ) ")
				.where("not exists (SELECT scorigem.cdsolicitacaocompra FROM Ordemcompra oc " +
					"JOIN oc.cotacao c " +
					"JOIN c.listaOrigem co " +
					"JOIN co.solicitacaocompra scorigem " +
					"JOIN oc.aux_ordemcompra aux_ocorigem " +
					"where scorigem.cdsolicitacaocompra = solicitacaocompra.cdsolicitacaocompra and " +
					"aux_ocorigem.situacaosuprimentos <> " + Situacaosuprimentos.BAIXADA.getCodigo() + ") ")
				.list();
	}

	/**
	* M�todo que busca os materiais com as solicita��es e ordens de compra do projeto
	*
	* @param whereInProjeto
	* @param whereInMaterial
	* @param whereInSolicitacaocompra
	* @param whereInOrdemcompramaterial
	* @return
	* @since 18/03/2016
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public List<Solicitacaocompra> findForBloquearQtdeCompra(String whereInProjeto, String whereInMaterial, String whereInSolicitacaocompra, String whereInOrdemcompramaterial) {
		if(StringUtils.isBlank(whereInProjeto)){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		String sql = 	" select query.cdmaterial as cdmaterial, query.cdprojeto as cdprojeto, sum(query.qtdetotal) as qtdetotal " +
						" from ( " +
						" select sc.cdmaterial as cdmaterial, p.cdprojeto as cdprojeto, sum(sc.qtde) as qtdetotal " +
						" from solicitacaocompra sc " +
						" join projeto p on p.cdprojeto = sc.cdprojeto " +
						" join aux_solicitacaocompra aux_sc on aux_sc.cdsolicitacaocompra = sc.cdsolicitacaocompra and aux_sc.situacao in (" + Situacaosuprimentos.EM_ABERTO.getCodigo() + "," + Situacaosuprimentos.AUTORIZADA.getCodigo() + ")" +
						" where sc.cdprojeto in (" + whereInProjeto + ") " +
						" and sc.cdmaterial in (" + whereInMaterial + ")" +
						(StringUtils.isNotBlank(whereInSolicitacaocompra) ? " and sc.cdsolicitacaocompra not in (" + whereInSolicitacaocompra + ")"  : "") +
						" group by sc.cdmaterial, p.cdprojeto " + 
						
						" UNION ALL " +
						
						" select sc.cdmaterial as cdmaterial, p.cdprojeto as cdprojeto, sum(sc.qtde) as qtdetotal " +
						" from solicitacaocompra sc " +
						" join projeto p on p.cdprojeto = sc.cdprojeto " +
						" join aux_solicitacaocompra aux_sc on aux_sc.cdsolicitacaocompra = sc.cdsolicitacaocompra and aux_sc.situacao in (" + Situacaosuprimentos.EM_PROCESSOCOMPRA.getCodigo() + "," + Situacaosuprimentos.BAIXADA.getCodigo() + ")" +
						" where sc.cdprojeto in (" + whereInProjeto + ") and " +
						" sc.cdmaterial in (" + whereInMaterial + ") and " +
						" ( " +
						" (aux_sc.situacao = " + Situacaosuprimentos.EM_PROCESSOCOMPRA.getCodigo() + " and " +
						"  exists (select cfisc.cdsolicitacaocompra " + 
						"  		  from cotacaofornecedor cf " +
						"         join aux_cotacao aux_c on aux_c.cdcotacao = cf.cdcotacao and aux_c.situacao in (" + Situacaosuprimentos.EM_ABERTO.getCodigo() + "," + Situacaosuprimentos.EM_COTACAO.getCodigo() + ")" +
						"         join cotacaofornecedoritem cfi on cfi.cdcotacaofornecedor = cf.cdcotacaofornecedor " +
						"         join cotacaofornecedoritemsolicitacaocompra cfisc on cfisc.cdcotacaofornecedoritem = cfi.cdcotacaofornecedoritem " +
						"         where cfisc.cdsolicitacaocompra = sc.cdsolicitacaocompra) " +
						" ) or " +
						" (aux_sc.situacao = " + Situacaosuprimentos.BAIXADA.getCodigo() + " and " +
						"	not exists (select cfisc.cdsolicitacaocompra " + 
						"  		  from cotacaofornecedoritemsolicitacaocompra cfisc " +
						"         where cfisc.cdsolicitacaocompra = sc.cdsolicitacaocompra) " +
						"	and " +
						"	not exists (select ocorigem.cdsolicitacaocompra " + 
						"  		  from ordemcompraorigem ocorigem " +
						"         where ocorigem.cdsolicitacaocompra = sc.cdsolicitacaocompra) " +
						" ) " +
						" ) " +
						" group by sc.cdmaterial, p.cdprojeto " +
						
						" UNION ALL " +
						
						" select ocm.cdmaterial as cdmaterial, p.cdprojeto as cdprojeto, sum(ocm.qtdpedida) as qtdetotal " +
						" from ordemcompramaterial ocm " +
						" join projeto p on p.cdprojeto = ocm.cdprojeto " +
						" join ordemcompra oc on oc.cdordemcompra = ocm.cdordemcompra " +
						" join aux_ordemcompra aux_oc on aux_oc.cdordemcompra = oc.cdordemcompra and aux_oc.situacao <> " + Situacaosuprimentos.CANCELADA.getCodigo() +
						" where ocm.cdprojeto in (" + whereInProjeto + ") " +
						" and ocm.cdmaterial in (" + whereInMaterial + ")" +
						(StringUtils.isNotBlank(whereInOrdemcompramaterial) ? " and ocm.cdordemcompramaterial not in (" + whereInOrdemcompramaterial + ")"  : "") +
						" group by ocm.cdmaterial, p.cdprojeto " +
						") query " +
						"group by query.cdmaterial, query.cdprojeto ";

		SinedUtil.markAsReader();
		List<Solicitacaocompra> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Solicitacaocompra bean = new Solicitacaocompra();
				bean.setMaterial(new Material(rs.getInt("cdmaterial")));
				bean.setQtde(rs.getDouble("qtdetotal"));
				bean.setProjeto(new Projeto(rs.getInt("cdprojeto")));
				return bean;
			}
		});
		
		return list;
	}
	
	public List<Solicitacaocompra> findByAtrasadoAprovacao(Date dateToSearch) {
		return querySined()
				.setUseWhereClienteEmpresa(false)
				.setUseWhereEmpresa(false)
				.setUseWhereFornecedorEmpresa(false)
				.setUseWhereProjeto(false)
				.select("solicitacaocompra.cdsolicitacaocompra, empresa.cdpessoa")
				.leftOuterJoin("solicitacaocompra.aux_solicitacaocompra aux_solicitacaocompra")
				.leftOuterJoin("solicitacaocompra.empresa empresa")
				.where("aux_solicitacaocompra.situacaosuprimentos = ?", Situacaosuprimentos.EM_ABERTO)
				.wherePeriodo("solicitacaocompra.dtsolicitacao", dateToSearch, SinedDateUtils.currentDate())
				.list();
	}
	
	public List<Solicitacaocompra> findByAtrasadoEntrega(Date dateToSearch) {
		return querySined()
			.setUseWhereClienteEmpresa(false)
			.setUseWhereEmpresa(false)
			.setUseWhereFornecedorEmpresa(false)
			.setUseWhereProjeto(false)
			.select("solicitacaocompra.cdsolicitacaocompra, empresa.cdpessoa, solicitacaocompra.dtlimite, material.tempoentrega ")
			.leftOuterJoin("solicitacaocompra.aux_solicitacaocompra aux_solicitacaocompra")
			.leftOuterJoin("solicitacaocompra.empresa empresa")
			.leftOuterJoin("solicitacaocompra.material material")
			.where("aux_solicitacaocompra.situacaosuprimentos = ?", Situacaosuprimentos.AUTORIZADA)
			.wherePeriodo("solicitacaocompra.dtsolicitacao", dateToSearch, SinedDateUtils.currentDate())
			.list();
	}

}
