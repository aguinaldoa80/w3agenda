package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Colaboradorsituacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("colaboradorsituacao.nome")
public class ColaboradorsituacaoDAO extends GenericDAO<Colaboradorsituacao> {

	@Override
	public void updateListagemQuery(QueryBuilder<Colaboradorsituacao> query, FiltroListagem _filtro) {
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Colaboradorsituacao> query) {
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
	}
	
	/* singleton */
	private static ColaboradorsituacaoDAO instance;
	public static ColaboradorsituacaoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradorsituacaoDAO.class);
		}
		return instance;
	}
}