package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Dadobancario;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DadobancarioDAO extends GenericDAO<Dadobancario> {

	@Override
	public void updateListagemQuery(QueryBuilder<Dadobancario> query, FiltroListagem _filtro) {
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Dadobancario> query) {
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
	}
	

	/* singleton */
	private static DadobancarioDAO instance;
	public static DadobancarioDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(DadobancarioDAO.class);
		}
		return instance;
	}

	public void deletaColaborador(Colaborador colaborador) {
		getJdbcTemplate().execute("DELETE FROM DADOBANCARIO WHERE CDPESSOA = " + colaborador.getCdpessoa());
	}
	
	/**
	 * 
	 * M�todo que carrega os dados banc�rios para a gera��o do SISPAG.
	 *
	 * @name getDadoBancarioByPessoa
	 * @param pessoa
	 * @return
	 * @return Dadobancario
	 * @author Thiago Augusto
	 * @date 17/05/2012
	 *
	 */
	public Dadobancario findDadoBancarioByPessoa(Pessoa pessoa){
		return query()
					.select("dadobancario.agencia, dadobancario.conta, dadobancario.dvconta, banco.numero")
					.leftOuterJoin("dadobancario.pessoa pessoa")
					.leftOuterJoin("dadobancario.banco banco")
					.where("pessoa = ? ", pessoa)
					.unique();
	}
}