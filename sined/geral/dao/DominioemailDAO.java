package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Dominio;
import br.com.linkcom.sined.geral.bean.Dominioemail;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.DominioemailFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DominioemailDAO extends GenericDAO<Dominioemail>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Dominioemail> query, FiltroListagem _filtro) {
		DominioemailFiltro filtro = (DominioemailFiltro)_filtro;
		query.leftOuterJoinFetch("dominioemail.servicoservidor servicoservidor")
			 .leftOuterJoinFetch("servicoservidor.servidor servidor")
			 .leftOuterJoinFetch("dominioemail.dominioemailtransporte dominioemailtransporte")
			 .leftOuterJoinFetch("dominioemail.dominio dominio")			 
			 .leftOuterJoinFetch("dominioemail.dominiozonadestino dominiozonadestino")			 
		 	 .where("servicoservidor.servidor = ?", filtro.getServidor())
		 	 .where("dominioemail.servicoservidor = ?", filtro.getServicoservidor())
		 	 .where("dominioemail.dominio = ?", filtro.getDominio());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Dominioemail> query) {
		query.leftOuterJoinFetch("dominioemail.servicoservidor servicoservidor")
			 .leftOuterJoinFetch("servicoservidor.servidor servidor")
			 .leftOuterJoinFetch("dominioemail.dominioemailtransporte dominioemailtransporte")
			 .leftOuterJoinFetch("dominioemail.dominio dominio")			 
			 .leftOuterJoinFetch("dominioemail.dominiozonadestino dominiozona");
	}

	/**
	 * M�todo que verifica se eixtem DominioEmail do dom�nio
	 * 
	 * @param dominio
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeDominioEmail(Dominio dominio) {
		if(dominio == null || dominio.getCddominio() == null)
			throw new SinedException("Parametros inv�lidos");
		return newQueryBuilderWithFrom(Long.class)
		.select("count(*)")
		.join("dominioemail.dominio dominio")
		.where("dominio = ?", dominio)
		.where("dominioemail.ativo = ?", Boolean.TRUE)
		.setUseTranslator(false)
		.unique()
		.longValue()>0;
	}
	
}
