package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Dependente;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DependenteDAO extends GenericDAO<Dependente> {
	
	@Override
	public void saveOrUpdate(Dependente bean) {
		super.saveOrUpdate(bean);
	}
	/* singleton */
	private static DependenteDAO instance;
	public static DependenteDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(DependenteDAO.class);
		}
		return instance;
	}

}
