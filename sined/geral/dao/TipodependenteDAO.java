package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Tipodependente;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("tipodependente.cdtipodependente")
public class TipodependenteDAO extends GenericDAO<Tipodependente> {
	
	/* singleton */
	private static TipodependenteDAO instance;
	public static TipodependenteDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(TipodependenteDAO.class);
		}
		return instance;
	}
		
}
