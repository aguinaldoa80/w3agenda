package br.com.linkcom.sined.geral.dao;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Contratotipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ContratotipoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("contratotipo.nome")
public class ContratotipoDAO extends GenericDAO<Contratotipo>{

	@Override
	public void updateListagemQuery(QueryBuilder<Contratotipo> query,
			FiltroListagem _filtro) {
		
		ContratotipoFiltro filtro = (ContratotipoFiltro) _filtro;
		
		query
			.select("contratotipo.cdcontratotipo, contratotipo.nome, frequencia.cdfrequencia, frequencia.nome, contratotipo.locacao,"+
					"contratotipo.acrescimoproximofaturamento, contratotipo.faturamentoemlote")
			.leftOuterJoin("contratotipo.frequencia frequencia")
			.whereLikeIgnoreAll("contratotipo.nome", filtro.getNome())
			.where("frequencia = ?", filtro.getFrequencia())
			.orderBy("contratotipo.nome");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Contratotipo> query) {
		query.select("contratotipo.cdcontratotipo, contratotipo.nome, contratotipo.locacao, contratotipo.faturamentoporitensdevolvidos, contratotipo.faturarhoratrabalhada, " +
				"listacontratotipodesconto.cdcontratotipodesconto, listacontratotipodesconto.diasantesvencimento, contratotipo.dtaltera, contratotipo.cdusuarioaltera, " +
				"listacontratotipodesconto.forma, listacontratotipodesconto.valor, listacontratotipodesconto.limite," +
				"material.cdmaterial, material.identificacao, material.nome, frequencia.cdfrequencia, frequencia.nome, " +
				"contratotipo.naorenovarperiodolocacaoautomatico, contratotipo.confirmarautomaticoprimeiroromaneio, contratotipo.medicao," +
				"contratotipo.faturamentowebservice, pedidovendatipo.cdpedidovendatipo, pedidovendatipo.descricao," +
				"contratotipo.atualizarProximoVencimentoConfirmar, contratotipo.acrescimoproximofaturamento, contratotipo.faturamentoemlote, " +
				"listaagendainteracaocontratomodelo.referencia, listaagendainteracaocontratomodelo.diasparacomecar, listaagendainteracaocontratomodelo.descricao, " +
				"listaagendainteracaocontratomodelo.frequencia, listaagendainteracaocontratomodelo.pessoatipo, " +
				"atividadetipo.nome, atividadetipo.cdatividadetipo, periodicidade.nome, periodicidade.cdfrequencia");
		query.leftOuterJoin("contratotipo.listacontratotipodesconto listacontratotipodesconto");
		query.leftOuterJoin("contratotipo.frequencia frequencia");
		query.leftOuterJoin("contratotipo.material material");
		query.leftOuterJoin("contratotipo.pedidovendatipo pedidovendatipo");
		query.leftOuterJoin("contratotipo.listaagendainteracaocontratomodelo listaagendainteracaocontratomodelo");
		query.leftOuterJoin("listaagendainteracaocontratomodelo.periodicidade periodicidade");
		query.leftOuterJoin("listaagendainteracaocontratomodelo.atividadetipo atividadetipo");
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				save.saveOrUpdateManaged("listacontratotipodesconto");
				save.saveOrUpdateManaged("listaagendainteracaocontratomodelo");
				return null;
			}
		});
	}
}
