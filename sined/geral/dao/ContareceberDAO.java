package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Arquivoconciliacaooperadora;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentoenvioboleto;
import br.com.linkcom.sined.geral.bean.Documentoenvioboletodiascobranca;
import br.com.linkcom.sined.geral.bean.Documentoenvioboletosituacao;
import br.com.linkcom.sined.geral.bean.Documentonegociado;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.DocumentocomissaoOrigem;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.TaxaService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.DocumentoFiltro;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.VerificaPendenciaFinanceira;
import br.com.linkcom.sined.modulo.rh.controller.crud.DocumentocomissaovendedorCrud;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("documento.cddocumento desc")
public class ContareceberDAO extends GenericDAO<Documento> {
	
	private ArquivoDAO arquivoDAO;
	private TaxaService taxaService;
	private RateioService rateioService;
	private DocumentoDAO documentoDAO;
	
	public void setTaxaService(TaxaService taxaService) {
		this.taxaService = taxaService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setDocumentoDAO(DocumentoDAO documentoDAO) {
		this.documentoDAO = documentoDAO;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Documento> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		DocumentoFiltro filtro = (DocumentoFiltro) _filtro;
		filtro.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
		
		documentoDAO.updateListagemQuery(query, _filtro);
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Documento> query) {
//		query.leftOuterJoinFetch("documento.pessoa pessoa");
//		query.leftOuterJoin("documento.empresa empresa");
//		query.leftOuterJoin("empresa.escritoriocontabilista escritoriocontabilista");
		query.joinFetch("documento.aux_documento aux_documento");
		query.leftOuterJoinFetch("documento.conta conta");
		query.leftOuterJoinFetch("documento.contacarteira contacarteira");
		query.leftOuterJoinFetch("documento.indicecorrecao indicecorrecao");
		query.leftOuterJoinFetch("documento.documentotipo documentotipo");			
		query.leftOuterJoinFetch("documento.documentoacao documentoacao");
		query.leftOuterJoinFetch("documento.listaDocumentohistorico listaDocumentohistorico");

		query.leftOuterJoinFetch("documento.endereco endereco");
		query.leftOuterJoinFetch("endereco.municipio municipio");
		query.leftOuterJoinFetch("municipio.uf uf");
		
		query.leftOuterJoinFetch("documento.indicecorrecao indicecorrecao");
		query.leftOuterJoinFetch("documento.listaParcela parcela");
		query.leftOuterJoinFetch("parcela.parcelamento parcelamento");
		query.leftOuterJoinFetch("documento.imagem imagem");
		query.leftOuterJoinFetch("documento.cheque cheque");
		query.leftOuterJoinFetch("parcelamento.prazopagamento prazopagamento");
		query.leftOuterJoinFetch("documento.operacaocontabil operacaocontabil");
	}
		
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaApropriacao");
		save.saveOrUpdateManaged("listaDocumentohistorico");
		Documento bean = (Documento) save.getEntity();
		
		bean.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
		
		Taxa taxas = bean.getTaxa();
		if(taxas != null){
			taxaService.saveOrUpdateNoUseTransaction(taxas);
		}
		
		Rateio rateio = bean.getRateio();
		if(rateio != null){
			rateioService.saveOrUpdateNoUseTransaction(rateio);
		}
		
		if (bean.getImagem() != null)
			arquivoDAO.saveFile(bean, "imagem");
	}
	
	/**
	 * Carrega a conta a receber para a exibi��o na tela de arquivo de retorno.
	 *
	 * @param documento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Documento loadForRetorno(Documento documento) {
		if(documento == null || documento.getCddocumento() == null){
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		return querySined()
					.select("documento.cddocumento, pessoa.nome, documento.outrospagamento, " +
							"aux_documento.valoratual, documentoacao.cddocumentoacao, documentoacao.nome")
					.leftOuterJoin("documento.pessoa pessoa")
					.join("documento.documentoacao documentoacao")
					.join("documento.aux_documento aux_documento")
					.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.where("documento = ?", documento)
					.unique();
	}
	
	public Documento loadForRetorno(String documentoNumero, boolean buscaNossonumero, boolean buscaNumero, boolean nossoNumeroNulo, Money valorDocBusca) {
		if(documentoNumero == null || "".equals(documentoNumero)){
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		
		QueryBuilder<Documento> query = querySined()
					.select("documento.cddocumento, pessoa.nome, documento.outrospagamento, " +
							"aux_documento.valoratual, documentoacao.cddocumentoacao, documentoacao.nome")
					.leftOuterJoin("documento.pessoa pessoa")
					.join("documento.documentoacao documentoacao")
					.join("documento.aux_documento aux_documento")
					.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.where("documentoacao <> ?", Documentoacao.CANCELADA);
		
		if(buscaNossonumero){
			query.where("documento.nossonumero = ?", documentoNumero);
		}else if(buscaNumero){
			query.where("documento.numero = ?", documentoNumero);
			query.where("documento.valor = ?", valorDocBusca);
		}else {
			Integer cddocumento = 0;
			try {
				cddocumento = Integer.parseInt(documentoNumero);
			} catch (Exception e) {return null;}
			query.where("documento.cddocumento = ?", cddocumento);
			if(nossoNumeroNulo)
				query.where("COALESCE(documento.nossonumero, '') = ''");
		}
		
		List<Documento> lista = query.list();
		
		return lista != null && lista.size() == 1 ? lista.get(0) : null;
	}
	
	/**
	 * Busca as contas a receber que tenha o cddocumento ou nossonumero.
	 *
	 * @param nossonumero
	 * @return
	 * @since 18/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Documento> findForRetornoConfiguravel(String nossonumero) {
		Documento documento = new Documento(Integer.parseInt(nossonumero));
		
		return querySined()
				.select("documento.cddocumento, pessoa.nome, documento.outrospagamento, documento.somentenossonumero, " +
						"aux_documento.valoratual, documentoacao.cddocumentoacao, documentoacao.nome, documento.nossonumero ")
				.leftOuterJoin("documento.pessoa pessoa")
				.join("documento.documentoacao documentoacao")
				.join("documento.aux_documento aux_documento")
				.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
				.openParentheses()
				.where("documento = ?", documento)
				.or()
				.where("documento.nossonumero = ?", nossonumero)
				.closeParentheses()
				.list();
	}
	
	/**
	 * Busca as contas a receber que tenha o cddocumento ou nossonumero e a conta do filtro.
	 *
	 * @param nossonumero
	 * @param conta
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForRetornoConfiguravel(String nossonumero, Conta conta) {
		Documento documento = null;
		try {
			documento = new Documento(Integer.parseInt(nossonumero));
		} catch (Exception e) {}
		
		return querySined()
				.select("documento.cddocumento, pessoa.nome, documento.outrospagamento, documento.somentenossonumero, " +
						"aux_documento.valoratual, documentoacao.cddocumentoacao, documentoacao.nome, documento.nossonumero ")
				.leftOuterJoin("documento.pessoa pessoa")
				.join("documento.documentoacao documentoacao")
				.leftOuterJoin("documento.conta conta")
				.join("documento.aux_documento aux_documento")
				.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
				.where("documentoacao <> ?", Documentoacao.CANCELADA)
				.where("conta = ?", conta)
				.openParentheses()
				.where("documento = ?", documento)
				.or()
				.where("documento.nossonumero = ?", nossonumero)
				.closeParentheses()
				.list();
	}
		
	/**
	 * M�todo que realiza busca geral em contas a receber de acordo com o par�metro da busca
	 * 
	 * @param busca
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Documento> findContasReceberForBuscaGeral(String busca) {
		return querySined()
			.select("documento.cddocumento, documento.descricao, documentoclasse.cddocumentoclasse")
			.join("documento.documentoclasse documentoclasse")
			.where("documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
			.openParentheses()
				.whereLikeIgnoreAll("documento.descricao", busca)
			.closeParentheses()
			.orderBy("documento.descricao")
			.list();
	}
	
	/**
	 * Retorna uma lista de contas a partir de uma lista de documentos. 
	 * - WHERE <cddocumento> IN (<cddocumento>,<cddocumento>,<cddocumento>,...) 
	 * 
	 * @param whereIn
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Documento> findContas(String whereIn){
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Documento n�o pode ser nulo nem string vazia.");
		}
		return querySined()
					.select("documento.cddocumento, documento.valor, documento.pessoa, documento.documentoacao, documento.outrospagamento, " +
							"agendamentoservicodocumento.cdagendamentoservicodocumento, " +
							"conta.cdconta, contacarteira.cdcontacarteira, taxa.cdtaxa, tipotaxa.cdtipotaxa, tipotaxa.nome, " +
							"listaTaxaitem.percentual, listaTaxaitem.valor, listaTaxaitem.dtlimite, listaTaxaitem.dias, listaTaxaitem.motivo")
					.leftOuterJoin("documento.pessoa pessoa")
					.join("documento.documentoacao documentoacao")
					.leftOuterJoin("documento.listaDocumentoOrigem documentoorigem")
					.leftOuterJoin("documentoorigem.agendamentoservicodocumento agendamentoservicodocumento")
					.leftOuterJoin("documento.conta conta")
					.leftOuterJoin("documento.contacarteira contacarteira")
					.leftOuterJoin("conta.taxa taxa")
					.leftOuterJoin("taxa.listaTaxaitem listaTaxaitem")
					.leftOuterJoin("listaTaxaitem.tipotaxa tipotaxa")
					.whereIn("documento.cddocumento", whereIn)
					.list();
	}
	
	/**
	 * M�todo que verifica se um determinado cliente possui contas a receber em atraso
	 * 
	 * @param cliente
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean clientePossuiContasAtrasadas(Cliente cliente) {
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Documento.class)
			.join("documento.documentoclasse documentoclasse")
			.join("documento.documentoacao documentoacao")
			.join("documento.pessoa pessoa")
			.where("documento.dtvencimento < ?", new Date(System.currentTimeMillis()))
			.where("documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
			.where("pessoa = ?", cliente)
			.where("documentoacao <> ?", Documentoacao.CANCELADA)
			.where("documentoacao <> ?", Documentoacao.BAIXADA)
			.where("documentoacao <> ?", Documentoacao.NEGOCIADA)
			.unique() > 0;
	}
	
	/**
	 * Retorna a soma do valor a pagar (relat�rio agendamentos de servi�os) de colaborador 
	 * 
	 * @author Taidson
	 * @since 07/05/2010
	 * 
	 */
	public Long contaReceberSoma(Colaborador colaborador, Date dtInicio, Date dtFim){
		String sql = "select sum(valor) from documento d " +
			"join colaborador c on (d.cdpessoa = c.cdpessoa) " +
			"join documentoclasse dc on (d.cddocumentoclasse = dc.cddocumentoclasse) " +
			"where d.cdpessoa = " + colaborador.getCdpessoa() +
			" and dc.cddocumentoclasse = 2" +
			" and d.dtemissao >= '"+ dtInicio +"'" + " and d.dtemissao <= '"+ dtFim +"'"; 
		return this.queryForLong(sql);
	}

	private Long queryForLong(String sql){
		SinedUtil.markAsReader();
		Long result = getJdbcTemplate().queryForLong(sql);
		return new Long(result);
	}
	
	/**
	 * Retorna a soma do valor devido vinculada ao cliente. 
	 * 
	 * @author Taidson
	 * @since 10/05/2010
	 * 
	 */
	public Long valorDevido(Cliente cliente, Date dtInicio, Date dtFim){
		String sql = "select sum(valor) from documento d " +
			"join documentoclasse dc on (d.cddocumentoclasse = dc.cddocumentoclasse) " +
			"join documentoacao da on (d.cddocumentoacao = da.cddocumentoacao) " +
			"join pessoa p on (d.cdpessoa = p.cdpessoa) " +
			"where d.cdpessoa = " + cliente.getCdpessoa() +
			" and dc.cddocumentoclasse = 2" +
			" and d.dtemissao >= '"+ dtInicio +"'" + " and d.dtemissao <= '"+ dtFim +"'"; 
		return this.queryForLong(sql);
	}

	/**
	 * Retorna a soma do valor baixados das contas a receber vinculadas ao cliente. 
	 * 
	 * @author Taidson
	 * @since 10/05/2010
	 * 
	 */
	public Long valorRecebido(Cliente cliente, Date dtInicio, Date dtFim){
		String sql = "select sum(valor) from documento d " +
		"join documentoclasse dc on (d.cddocumentoclasse = dc.cddocumentoclasse) " +
		"join documentoacao da on (d.cddocumentoacao = da.cddocumentoacao) " +
		"join pessoa p on (d.cdpessoa = p.cdpessoa) " +
		"where d.cdpessoa = " + cliente.getCdpessoa() +
		" and dc.cddocumentoclasse = 2 and da.cddocumentoacao = 6" +
		" and d.dtemissao >= '"+ dtInicio +"'" + " and d.dtemissao <= '"+ dtFim +"'";
		return this.queryForLong(sql);
	}

	/**
	 * Altera o cliente do documento passado como par�metro.
	 * @param documento
	 * @param pessoa
	 * @author Taidson
	 * @since 20/07/2010
	 */
	public void mudarCliente(Documento documento, Pessoa pessoa) {
		if (documento == null || documento.getCddocumento() == null){
			throw new SinedException("Par�metros inv�lidos na fun��o mudan�a de cliente.");
		}
		String sql = "UPDATE Documento" +
					 " SET cdpessoa = " + pessoa.getCdpessoa()  +
					 " WHERE cddocumento = (" + documento.getCddocumento() + ")";
		getJdbcTemplate().execute(sql);
	}
	
	@SuppressWarnings("unchecked")
	public Documento findUltimaReceita(Contrato contrato) {
		
		String sql = 	"SELECT d.cddocumento, d.cddocumentoacao, d.dtemissao " +
						"FROM documento d " +
						"LEFT OUTER JOIN notadocumento nd ON nd.cddocumento = d.cddocumento " +
						"LEFT OUTER JOIN notacontrato nc ON nc.cdnota = nd.cdnota " +
						"WHERE nc.cdcontrato = " + contrato.getCdcontrato() + " " +
						"ORDER BY d.dtemissao DESC " +
						"LIMIT 1";
		SinedUtil.markAsReader();
		List<Documento> lista = getJdbcTemplate().query(sql, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Documento(
						rs.getInt("cddocumento"),
						rs.getInt("cddocumentoacao"),
						rs.getDate("dtemissao")
						);
			}
		});
		
		if(lista == null || lista.size() == 0){
			return null;
		} 
			
		return lista.get(0);
	}
	public void atualizaValor(Documento doc, Money valor) {
		getHibernateTemplate().bulkUpdate("update Documento d set d.valor = ? where d.id = ?", new Object[]{valor, doc.getCddocumento()});
	}
	
	/**
	 * Verifica de uma determinada conta a receber est� baixada.
	 * 
	 * @param cdconta
	 * @return
	 * @author Taidson
	 * @since 15/12/2010
	 */
	public boolean verficaContaBaixadaDefinitiva(Integer cdconta) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Documento.class)
					.join("documento.documentoclasse documentoclasse")
					.join("documento.documentoacao documentoacao")
					.where("documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.where("documento.cddocumento = ?", cdconta)
					.openParentheses()
					.where("documentoacao = ?", Documentoacao.BAIXADA)
					.or()
					.where("documentoacao = ?", Documentoacao.DEFINITIVA)
					.closeParentheses()
					.unique() > 0;
	}
	
	/**
	 * M�todo que gera um n� para a conta a receber no momento da baixa
	 * 
	 * @param documento
	 * @param proxNum
	 * @author Tom�s Rabelo
	 */
	public void updateNumeroContaReceber(Documento documento, Integer proxNum) {
		if(documento == null || documento.getCddocumento() == null){
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		if(proxNum == null){
			throw new SinedException("O n�mero do documento n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE DOCUMENTO SET NUMERO = '" + proxNum + "' WHERE CDDOCUMENTO = " + documento.getCddocumento());
	}
	
	public Long getNextNossoNumero(Conta conta){
		if(conta == null || conta.getCdconta() == null){
			throw new SinedException("Conta n�o pode ser nulo.");
		}
		if(conta.getNossonumeroinicial() == null){
			throw new SinedException("N�mero do sequencial inicial da conta banc�ria n�o pode ser nulo.");
		}
		String sql = "select coalesce(max(nossonumero :: integer)+1, " + conta.getNossonumeroinicial() + ") " +
		             "  from documento " +
		             " where cdconta = " + conta.getCdconta();
		return this.queryForLong(sql);
	}
	
	/**
	 * Busca a lista de documentos a partir de v�rias vendas.
	 *
	 * @param whereIn
	 * @return
	 * @since 10/04/2012
	 * @author Rodrigo Freitas
	 */
	public List<Documento> findByVendas(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Venda n�o podem ser nulas.");
		}
		return querySined()
					.select("documento.cddocumento, documento.descricao, documento.documentoacao, documento.dtvencimento, documento.valor")
					.join("documento.listaDocumentoOrigem documentoorigem")
					.join("documentoorigem.venda venda")
					.whereIn("venda.cdvenda", whereIn)
					.orderBy("documento.dtvencimento")
					.list();
	}
	
	/**
	 * Busca a lista de documentos a partir de v�rios pedidos de venda.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/11/2014
	 */
	public List<Documento> findByPedidovenda(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Venda n�o podem ser nulas.");
		}
		return querySined()
					.select("documento.cddocumento, documento.descricao, documento.documentoacao, documento.dtvencimento, documento.valor")
					.join("documento.listaDocumentoOrigem documentoorigem")
					.join("documentoorigem.pedidovenda pedidovenda")
					.whereIn("pedidovenda.cdpedidovenda", whereIn)
					.orderBy("documento.dtvencimento")
					.list();
	}
	
	/**
	 * Busca a lista de documentos a partir de v�rias vendas
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/08/2014
	 */
	@SuppressWarnings("unchecked")
	public List<Documento> findByVendasForBoleto(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Venda n�o podem ser nulas.");
		}
		
		String sql = "select distinct d.cddocumento, d.valor " +
						" from documento d " +
						" left outer join documentotipo doctipo on doctipo.cddocumentotipo = d.cddocumentotipo " +
						" left outer join vdocumentonegociado vw on vw.cddocumentonegociado = d.cddocumento " +
						" left outer join documentoorigem dori on (dori.cddocumento = vw.cddocumento or dori.cddocumento = d.cddocumento) " +
						" left outer join venda v on v.cdvenda = dori.cdvenda " +
						" where d.cddocumentoacao in (1,2) " +
						" and doctipo.boleto = true " +
						" and v.cdvenda in (" + whereIn + ")";
		SinedUtil.markAsReader();
		return getJdbcTemplate().query(sql, new RowMapper() {
			public Documento mapRow(final ResultSet rs, final int rowNum) throws SQLException {
				return new Documento(rs.getInt("cddocumento"));
			}
		});
	}
	
	@SuppressWarnings("unchecked")
	public List<Documento> findByVendasForNegociar(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Venda n�o podem ser nulas.");
		}
		
		String sql = "select distinct d.cddocumento, d.valor " +
						" from documento d " +
						" left outer join documentotipo doctipo on doctipo.cddocumentotipo = d.cddocumentotipo " +
						" left outer join vdocumentonegociado vw on vw.cddocumentonegociado = d.cddocumento " +
						" left outer join documentoorigem dori on (dori.cddocumento = vw.cddocumento or dori.cddocumento = d.cddocumento) " +
						" left outer join venda v on v.cdvenda = dori.cdvenda " +
						" where d.cddocumentoacao in (1,2) " +
						" and v.cdvenda in (" + whereIn + ")";
		SinedUtil.markAsReader();
		return getJdbcTemplate().query(sql, new RowMapper() {
			public Documento mapRow(final ResultSet rs, final int rowNum) throws SQLException {
				return new Documento(rs.getInt("cddocumento"));
			}
		});
	}
	
	/**
	 * M�todo que busca as conta com origem da venda
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findContasOrigemVendas(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Documentos n�o podem ser nulos.");
		}
		return querySined()
					.select("documento.cddocumento, documento.descricao, venda.cdvenda, documentoorigem.cddocumentoorigem")
					.join("documento.listaDocumentoOrigem documentoorigem")
					.join("documentoorigem.venda venda")
					.whereIn("documento.cddocumento", whereIn)
					.list();
	}

	/**
	 * M�todo que verifica a pend�ncia financeira dos clientes passados por par�metro.
	 * Retorna uma list de array de objetos:
	 * obj[0] -> cdpessoa
	 * obj[1] -> boolean (existe ou n�o pend�ncia financeira)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> verificaPendenciaFinanceiraByClientes(String whereIn){
		List<Documentoacao> listaDocumentoacaoNaoPermitido = new ArrayList<Documentoacao>();
		listaDocumentoacaoNaoPermitido.add(Documentoacao.CANCELADA);
		listaDocumentoacaoNaoPermitido.add(Documentoacao.BAIXADA);
		listaDocumentoacaoNaoPermitido.add(Documentoacao.NEGOCIADA);
		SinedUtil.markAsReader();
		return getJdbcTemplate().query(
				"SELECT P.CDPESSOA AS CDPESSOA, EXISTS( " +
				"SELECT D.CDDOCUMENTO " +
				"FROM DOCUMENTO D " +
				"WHERE D.CDPESSOA = P.CDPESSOA " +
				"AND D.CDDOCUMENTOACAO NOT IN (" + CollectionsUtil.listAndConcatenate(listaDocumentoacaoNaoPermitido, "cddocumentoacao", ",") + ") " +
				"AND D.CDDOCUMENTOCLASSE = " + Documentoclasse.CD_RECEBER + " " +
				"AND CURRENT_DATE > D.DTVENCIMENTO " +
				") AS PENDENCIAFINANCEIRA " +
				"FROM PESSOA P " +
				"WHERE P.CDPESSOA IN (" + whereIn + ") ", 
			new RowMapper() {
				public Object[] mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Object[]{
						rs.getInt("CDPESSOA"), rs.getBoolean("PENDENCIAFINANCEIRA")
					};
				}
			});
	}
	
	/**
	 * M�todo que verifica contas a receber atrasadas a mais de 5 dias
	 *
	 * @param cpfcnpj
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isPendenciaFinanceiraByCpfCnpj(VerificaPendenciaFinanceira bean) {
		if(bean == null || ( (bean.getCnpj() == null || "".equals(bean.getCnpj())) && (bean.getCpf() == null || "".equals(bean.getCpf())) ) )
			throw new SinedException("Par�metro inv�lido.");
		
		if(bean.getCnpj() != null || !"".equals(bean.getCnpj())){
			return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Documento.class)
				.setUseTranslator(false)
				.join("documento.documentoacao documentoacao")
				.join("documento.documentoclasse documentoclasse")
				.join("documento.pessoa pessoa")
				.where("pessoa.cnpj = ?", new Cnpj(bean.getCnpj()))
				.where("(CURRENT_DATE - documento.dtvencimento) > 5")
				.where("documentoacao <> ?", Documentoacao.CANCELADA)
				.where("documentoacao <> ?", Documentoacao.BAIXADA)
				.where("documentoacao <> ?", Documentoacao.NEGOCIADA)
				.where("documentoclasse.cddocumentoclasse = ?", Documentoclasse.CD_RECEBER)
				.unique() > 0;
		}else if(bean.getCpf() != null && !"".equals(bean.getCpf())){
			return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Documento.class)
			.setUseTranslator(false)
			.join("documento.documentoacao documentoacao")
			.join("documento.documentoclasse documentoclasse")
			.join("documento.pessoa pessoa")
			.where("pessoa.cpf = ?", new Cpf(bean.getCpf()))
			.where("(CURRENT_DATE - documento.dtvencimento) > 5")
			.where("documentoacao <> ?", Documentoacao.CANCELADA)
			.where("documentoacao <> ?", Documentoacao.BAIXADA)
			.where("documentoacao <> ?", Documentoacao.NEGOCIADA)
			.where("documentoclasse.cddocumentoclasse = ?", Documentoclasse.CD_RECEBER)
			.unique() > 0;
		}else{
			return false;
		}
	}
	
	public List<Documento> getPendenciaFinanceiraByCpfCnpj(String cnpj, String cpf, Contagerencial contagerencial, Projeto projetoW3erp, Projeto projetoSindis, Projeto projetoSupergov) {
		if((cnpj == null || "".equals(cnpj)) && (cpf == null || "".equals(cpf)))
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
					.setUseWhereEmpresa(false)
					.setUseWhereProjeto(false)
					.select("documento.cddocumento, documento.dtvencimento, aux_documento.valoratual, projeto.cdprojeto")
					.join("documento.documentoacao documentoacao")
					.join("documento.aux_documento aux_documento")
					.join("documento.documentoclasse documentoclasse")
					.join("documento.pessoa pessoa")
					.join("documento.rateio rateio")
					.join("rateio.listaRateioitem rateioitem")
					.join("rateioitem.contagerencial contagerencial")
					.join("rateioitem.projeto projeto")
					.where("contagerencial = ?", contagerencial)
					.openParentheses()
						.where("projeto = ?", projetoSindis)
						.or()
						.where("projeto = ?", projetoW3erp)
						.or()
						.where("projeto = ?", projetoSupergov)
					.closeParentheses()
					.where("pessoa.cnpj = ?", cnpj != null ? new Cnpj(cnpj) : null)
					.where("pessoa.cpf = ?", cpf != null ? new Cpf(cpf) : null)
					.where("(CURRENT_DATE - documento.dtvencimento) > 1")
					.where("documentoacao <> ?", Documentoacao.CANCELADA)
					.where("documentoacao <> ?", Documentoacao.BAIXADA)
					.where("documentoacao <> ?", Documentoacao.NEGOCIADA)
					.where("documentoclasse.cddocumentoclasse = ?", Documentoclasse.CD_RECEBER)
					.orderBy("documento.dtvencimento")
					.list();
	}
	
	/**
	 * Retorna contas atrasadas conforme documentos inforamdos como par�mentro
	 * @param whereIn
	 * @return
	 * @author Taidson
	 * @author 24/11/2010
	 */
	public List<Documento> getPendenciaFinanceiraByContrato(Contrato contrato){
		return querySined()
					.select("documento.cddocumento, documento.dtvencimento, aux_documento.valoratual")
					.join("documento.documentoacao documentoacao")
					.join("documento.aux_documento aux_documento")
					.join("documento.documentoclasse documentoclasse")
					.where("(CURRENT_DATE - documento.dtvencimento) > 1")
					.where("documentoacao <> ?", Documentoacao.CANCELADA)
					.where("documentoacao <> ?", Documentoacao.BAIXADA)
					.where("documentoacao <> ?", Documentoacao.NEGOCIADA)
					.where("documentoclasse.cddocumentoclasse = ?", Documentoclasse.CD_RECEBER)
					.orderBy("documento.dtvencimento")
					.list();
	}
	
	public void atualizaContaNegociada(Documento documento, Documentonegociado documentonegociado){
		if(documentonegociado == null || documentonegociado.getCddocumentonegociado() == null){
			throw new SinedException("Par�metro inv�lido para o documento negociado.");
		}
		if(documento == null || documento.getCddocumento() == null){
			throw new SinedException("O documento n�o pode ser nulo.");
		}
		
		getJdbcTemplate().execute("UPDATE DOCUMENTO SET CDDOCUMENTONEGOCIADO = "+documentonegociado.getCddocumentonegociado()+" WHERE CDDOCUMENTO = "+documento.getCddocumento());
	}
	
	/**
	 * Busca a lista de Contas a receber para recalcular a comiss�o.
	 *
	 * @param whereIn
	 * @param dtrecalculoDe
	 * @param dtrecalculoAte
	 * @return
	 * @since 19/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Documento> findForRecalculoComissao(String whereIn, java.sql.Date dtrecalculoDe, java.sql.Date dtrecalculoAte) {
		return querySined()
					.select("documento.cddocumento, documentoacao.cddocumentoacao, documento.valor, contrato.cdcontrato, comissionamento.cdcomissionamento, " +
							"aux_documento.valoratual, comissionamento.tipobccomissionamento, documento.dtemissao")
					.join("documento.listaDocumentoOrigem documentoorigem")
					.join("documento.documentoacao documentoacao")
					.leftOuterJoin("documento.listaNotaDocumento notadocumento")
					.join("documentoorigem.contrato contrato")
					.leftOuterJoin("contrato.comissionamento comissionamento")
					.join("documento.aux_documento aux_documento")
					.whereIn("contrato.cdcontrato", whereIn)
					.where("notadocumento is null")
					.where("documento.dtemissao >= ?", dtrecalculoDe)
					.where("documento.dtemissao <= ?", dtrecalculoAte)
					.orderBy("documento.dtemissao, documento.cddocumento")
					.where("documento.documentoacao <> 0")
					.list();
	}
	
	/**
	 * Busca a lista de Contas a receber vinculadas a nota com origem do contrato  para recalcular a comiss�o.
	 *
	 * @param whereIn
	 * @param dtrecalculoDe
	 * @param dtrecalculoAte
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForRecalculoComissaoComNota(String whereIn, java.sql.Date dtrecalculoDe, java.sql.Date dtrecalculoAte) {
		return querySined()
					.select("documento.cddocumento, documentoacao.cddocumentoacao, documento.valor, contrato.cdcontrato, comissionamento.cdcomissionamento, notadocumento.cdNotaDocumento, " +
							"nota.cdNota, notacontrato.cdNotaContrato, contrato.cdcontrato, aux_documento.valoratual, comissionamento.tipobccomissionamento, notaTipo.cdNotaTipo," +
							"documento.dtemissao ")
					.join("documento.listaNotaDocumento notadocumento")
					.join("notadocumento.nota nota")
					.join("nota.listaNotaContrato notacontrato")
					.leftOuterJoin("nota.notaTipo notaTipo")
					.join("notacontrato.contrato contrato")
					.join("documento.aux_documento aux_documento")
					.leftOuterJoin("contrato.comissionamento comissionamento")
					.join("documento.documentoacao documentoacao")
					.whereIn("contrato.cdcontrato", whereIn)
					.where("documento.dtemissao >= ?", dtrecalculoDe)
					.where("documento.dtemissao <= ?", dtrecalculoAte)
					.where("documentoacao <> 0")
					.orderBy("documento.dtemissao, documento.cddocumento")
					.list();
	}
	
	/**
	 * M�todo que retorna o valor total das contas a receber do cliente que estejam com o valor total em aberto (DIFERENTE DE BAIXADA, BAIXADA PARCIAL, NEGOCIADA E CANCELADA) 
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getValorDebitoContasComValorTotalEmAbertoByCliente(Cliente cliente, boolean contasAtrasadas) {
		Long total = null;
		if(cliente != null && cliente.getCdpessoa() != null){
			QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
				.select("SUM(aux_documento.valoratual)")
				.setUseTranslator(false)
				.from(Documento.class)
				.join("documento.aux_documento aux_documento")
				.join("documento.pessoa pessoa")
				.join("documento.documentoacao documentoacao")
				.join("documento.documentoclasse documentoclasse");
			if(contasAtrasadas){
				query.where("(CURRENT_DATE - documento.dtvencimento) > 1");
			}
			
			total = query
					.where("pessoa.cdpessoa = ?", cliente.getCdpessoa())
					.where("documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.where("documentoacao <> ?", Documentoacao.BAIXADA)
					.where("documentoacao <> ?", Documentoacao.BAIXADA_PARCIAL)
					.where("documentoacao <> ?", Documentoacao.CANCELADA)
					.where("documentoacao <> ?", Documentoacao.NEGOCIADA)
					.unique();
		}
		
		if(total != null)
			return new Money(total, true);
		else
			return new Money();
	}
	
	/**
	 * M�todo que busca a lista de Contas a Receber para recalcular a comiss�o ap�s a baixa.
	 *
	 * @param whereInCddocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForRecalculoComissaoDocumentoBaixado(String whereInCddocumento) {
		if(whereInCddocumento == null || "".equals(whereInCddocumento))
			return null;
			
		return querySined()
			.select("documento.cddocumento, documento.valor, contrato.cdcontrato, comissionamento.cdcomissionamento, aux_documento.valoratual")
			.join("documento.listaDocumentoOrigem documentoorigem")
			.join("documentoorigem.contrato contrato")
			.leftOuterJoin("documento.listaNotaDocumento notadocumento")
			.join("documento.aux_documento aux_documento")
			.leftOuterJoin("contrato.comissionamento comissionamento")
			.whereIn("documento.cddocumento", whereInCddocumento)
			.where("notadocumento is null")
			.where("documento.documentoacao <> 0")
			.list();
	}
	
	/**
	 * M�todo que busca a lista de Contas a Receber com nota para recalcular a comiss�o ap�s a baixa.
	 *
	 * @param whereInCddocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForRecalculoComissaoDocumentoBaixadoComNota(String whereInCddocumento) {
		return querySined()
					.select("documento.cddocumento, documento.valor, contrato.cdcontrato, comissionamento.cdcomissionamento, notadocumento.cdNotaDocumento, " +
							"nota.cdNota, notacontrato.cdNotaContrato, contrato.cdcontrato, aux_documento.valoratual, notaTipo.cdNotaTipo ")
					.join("documento.listaNotaDocumento notadocumento")
					.join("notadocumento.nota nota")
					.join("nota.notaTipo notaTipo")
					.join("nota.listaNotaContrato notacontrato")
					.join("notacontrato.contrato contrato")
					.join("documento.aux_documento aux_documento")
					.leftOuterJoin("contrato.comissionamento comissionamento")
					.whereIn("documento.cddocumento", whereInCddocumento)
					.where("documento.documentoacao <> 0")
					.list();
	}
	
	/**
	 * M�todo que retorna os documentos com o cdpessoa
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForPermitirAssociar(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
		.select("documento.cddocumento, pessoa.cdpessoa ")
		.join("documento.pessoa pessoa")
		.whereIn("documento.cddocumento", whereIn)
		.list();
	}
	
	/**
	 * M�todo que carrega o documento com o valor atual
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public Documento loadWithValoratual(Documento documento){
		if(documento == null || documento.getCddocumento() == null){
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		
		return querySined()
			.select("documento.cddocumento, documento.valor, aux_documento.cddocumento, aux_documento.valoratual")
			.join("documento.aux_documento aux_documento")
			.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
			.where("documento = ?", documento)
			.unique();
	}
	
	/**
	 * M�todo que busca os documentos para cria��o de vale compra
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForValecompra(String whereIn){
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Documento n�o pode ser nulo nem string vazia.");
		}
		return querySined()
					.select("documento.cddocumento, pessoa.cdpessoa, pessoa.nome, documento.valor, documento.tipopagamento, auxdocumento.valoratual")
					.leftOuterJoin("documento.pessoa pessoa")
					.join("documento.documentoclasse documentoclasse")
					.join("documento.aux_documento auxdocumento")
					.whereIn("documento.cddocumento", whereIn)
					.list();
	}

	/**
	 * Busca a lista de documentos para auto complete usado na tela de DocumentocomissaoVendedor.
	 *
	 * @param param
	 * @return
	 * @author Lucas Costa
	 */
	public List<Documento> findForAutoCompleteDocComVendedor(String param) {

		DocumentocomissaoOrigem documentocomissaoOrigem = (DocumentocomissaoOrigem) NeoWeb.getRequestContext().getSession().getAttribute(DocumentocomissaovendedorCrud.ORIGEM_TIPO_COMISSAO_VENDEDOR);
		if(documentocomissaoOrigem==null) return null;
		
		QueryBuilder<Documento> query = querySined()
		.autocomplete()
		.select("documento.cddocumento, documento.descricao, documento.numero")
		.join("documento.listaDocumentoOrigem documentoorigem")
		.openParentheses()
			.whereLikeIgnoreAll("documento.numero",param)
			.or()
			.whereLikeIgnoreAll("documento.descricao",param)
		.closeParentheses()
		.orderBy("documento.numero");
		
		Object documentoComissaoTipoObj = NeoWeb.getRequestContext().getSession().getAttribute(DocumentocomissaovendedorCrud.ORIGEM_COMISSAO_VENDEDOR);
		if (documentocomissaoOrigem.equals(DocumentocomissaoOrigem.VENDA)){
			query.where("documentoorigem.venda = ?", (Venda) documentoComissaoTipoObj);
		}else if (documentocomissaoOrigem.equals(DocumentocomissaoOrigem.CONTRATO)){
			query.where("documentoorigem.contrato = ? ", (Contrato) documentoComissaoTipoObj);
		}else if (documentocomissaoOrigem.equals(DocumentocomissaoOrigem.PEDIDOVENDA)){
			query.where("documentoorigem.pedidovenda = ? ", (Pedidovenda) documentoComissaoTipoObj);
		} 
		
		return query.list();
	}
	
	/**
	 * Caso a conta a receber seja estornada, retorna o valor original.
	 *
	 * @param documento, valorPrevista, descriacaoCorrecao
	 * @author Lucas Costa
	 * @since 17/11/2014
	 */
	public void voltaValorIndiceCorrecao(Documento documento, Money valorPrevista, String descriacao){
		if(documento == null || documento.getCddocumento() == null){
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		//descriacao = descriacao.replaceAll("\"", "");
		Object[] objetos = null;
		String sql = null;
		if (valorPrevista != null){
			sql = "UPDATE Documento SET descricao = ?, valor = ?, descriacaoCorrecao = NULL, valorPrevista = NULL WHERE cddocumento = ?";
			objetos = new Object[] {descriacao, valorPrevista, documento.getCddocumento()};
		}
		else{
			sql = "UPDATE Documento SET descricao = ?, descriacaoCorrecao = NULL, valorPrevista = NULL WHERE cddocumento = ?";
			objetos = new Object[] {descriacao, documento.getCddocumento()};			
		}
		getHibernateTemplate().bulkUpdate(sql, objetos);
	}

	/**
	 * Salva a descri��o e o valor do documento antes da aplica��o do indice de corre��o
	 * @param documento
	 * @author Lucas Costa
	 */
	public void updateDadosAntesIndeceCorrecao (Documento documento){
		if (documento == null || documento.getCddocumento() == null ||
			documento.getValorPrevista() == null || documento.getDescriacaoCorrecao() == null) {
			throw new SinedException("Par�metros inv�lidos.");
		}

		String sql = null;
		Object[] objetos = null;
		sql = "update Documento d set d.valorPrevista = ?, d.descriacaoCorrecao = ? where d.id = ?";
		objetos = new Object[] { documento.getValorPrevista(), documento.getDescriacaoCorrecao(), documento.getCddocumento() };
		getHibernateTemplate().bulkUpdate(sql, objetos);
	}
	
	/**
	 * M�todo que carrega o documento para associacao com o arquivo da operadora de cartoes
	 *
	 * @param documento
	 * @return
	 * @author Jo�o Vitor
	 */
	public Documento findForAssociacaoArquivoConciliacaoOperadora(String whereIn){
		if(whereIn == null){
			throw new SinedException("whereIn n�o pode ser nulo.");
		}
		
		List<Documento> listaDocumento = querySined()
			.select("documento.cddocumento, pessoa.cdpessoa ")
			.join("documento.pessoa pessoa")
			.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
			.where("documento.numero = ?", whereIn)
			.list();
		
		if (SinedUtil.isListNotEmpty(listaDocumento) && listaDocumento.size() == 1) {
			return listaDocumento.get(0);
		} else {
			return null;
		}
	}
	
	
	/**
	 * M�todo que carrega lista de documentos para associacao manual com o arquivo da operadora de cartoes
	 *
	 * @param documento
	 * @return
	 * @author Jo�o Vitor
	 */
	public List<Documento> findListaDocumentosForAssociacaoArquivoOperadora(Arquivoconciliacaooperadora arquivoconciliacaooperadora){
		if (arquivoconciliacaooperadora == null) {
			throw new SinedException("Par�metro Inv�lido");
		}
		return querySined()
		.select("documento.cddocumento, documento.descricao, documento.dtvencimento, documento.valor, documento.dtemissao, documento.outrospagamento, " +
				"pessoa.cdpessoa, pessoa.nome ")
		.leftOuterJoin("documento.pessoa pessoa")
		.where("documento.dtvencimento = ?", arquivoconciliacaooperadora.getDataprevistapagamento())
		.where("documento.dtemissao = ?", arquivoconciliacaooperadora.getDatavenda())
		.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
		.where("NOT EXISTS(SELECT 1 FROM Arquivoconciliacaooperadora aco " +
				"WHERE aco.documento.cddocumento = documento.cddocumento)")
		.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Documento> findByNota(String cdnota){
		
		StringBuilder sql = new StringBuilder();
		
		sql
			.append("SELECT DOCUMENTO.CDDOCUMENTO")
			.append(" FROM DOCUMENTO ")
			.append("WHERE CDDOCUMENTOACAO IN ("+Documentoacao.PREVISTA.getCddocumentoacao().toString()+","+Documentoacao.DEFINITIVA.getCddocumentoacao().toString()+")"+
					"  AND CDDOCUMENTO IN (SELECT ND.CDDOCUMENTO FROM NOTADOCUMENTO ND WHERE ND.CDNOTA = " + cdnota + " " +
					"						UNION ALL " +
					"					   SELECT DOCO.CDDOCUMENTO "+
					"						 FROM NOTAVENDA NV "+
					"						 JOIN DOCUMENTOORIGEM DOCO ON DOCO.CDVENDA = NV.CDVENDA "+
					"						WHERE NV.CDNOTA = " + cdnota + 
					"						)");
		SinedUtil.markAsReader();
		List<Documento> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Documento bean = new Documento(rs.getInt(1));

				return bean;
			}
		});
		return lista;
	}
	public List<Documento> findByAtrasado(Date data, java.sql.Date dateToSearch) {
		List<Documentoacao> listaSituacao = new ArrayList<Documentoacao>();
		listaSituacao.add(Documentoacao.PREVISTA);
		listaSituacao.add(Documentoacao.DEFINITIVA);
		
		return querySined()
				.setUseWhereClienteEmpresa(false)
				.setUseWhereEmpresa(false)
				.setUseWhereFornecedorEmpresa(false)
				.setUseWhereProjeto(false)
				.select("documento.cddocumento, documento.numero, empresa.cdpessoa")
				.join("documento.documentoclasse documentoclasse")
				.join("documento.documentoacao documentoacao")
				.leftOuterJoin("documento.empresa empresa")
				.where("documento.dtvencimento < ?", data)
				.where("documentoclasse.cddocumentoclasse = ?", Documentoclasse.CD_RECEBER)
				.whereIn("documentoacao.cddocumentoacao", CollectionsUtil.listAndConcatenate(listaSituacao, "cddocumentoacao", ","))
				.wherePeriodo("documento.dtvencimento", dateToSearch, SinedDateUtils.currentDate())
				.list();
	}
	/**
	 * 
	 * @param whereIn
	 * @param acoes
	 * @return
	 */
	public List<Documento> findForAntecipacao(String whereIn, Boolean antecipado, Documentoacao... acoes) {
		if (StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Documento> query = querySined()
				.select("documento.cddocumento, documento.valor, documento.documentotipo, documento.dtemissao, documento.dtvencimento," +
						"documento.tipopagamento, documento.outrospagamento, documento.numero, documento.descricao, pessoa.cdpessoa ")
				.join("documento.pessoa pessoa")
				.join("documento.documentotipo documentotipo")
				.where("coalesce(documento.antecipado, false) = ?", antecipado);
		
		if(acoes != null){
			List<Documentoacao> listaDocumentoacao = new ArrayList<Documentoacao>();
			for(Documentoacao da: acoes) listaDocumentoacao.add(da);
			
			query.whereIn("documento.documentoacao.cddocumentoacao", CollectionsUtil.listAndConcatenate(listaDocumentoacao, "cddocumentoacao", ","));
		}
				
		return query
				.whereIn("documento.cddocumento", whereIn)
				.list();
	}
	
	/**
	 * @param documento
	 */
	public void updateCampoAntecipado(Documento documento){
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		getHibernateTemplate().bulkUpdate("update Documento documento set documento.antecipado = ?, documento.dtantecipacao = ? where documento.cddocumento= ?", 
				new Object[] {documento.getAntecipado(), documento.getDtantecipacao(), documento.getCddocumento()});
	}
	
	@SuppressWarnings("unchecked")
	public List<Documento> findForEnvioautomaticoAgendadoBoleto(Documentoenvioboleto documentoenvioboleto, String whereInCliente){
		String whereInDocumentoacao = "";
		for(Documentoenvioboletosituacao situacao: documentoenvioboleto.getListaSituacao()){
			whereInDocumentoacao += (situacao.getDocumentoacao().getCddocumentoacao()+" ");
		}
		whereInDocumentoacao = whereInDocumentoacao.trim().replaceAll(" ", ",");
		StringBuilder qry = new StringBuilder();
		for(Documentoenvioboletodiascobranca diascobranca: documentoenvioboleto.getListaDiascobranca()){
			
			String dataatualMaisDiascobranca = SinedDateUtils.toStringPostgre(SinedDateUtils.addDiasData(SinedDateUtils.currentDate(), diascobranca.getQtdedias()));
			qry.append("Select cddocumento");
			qry.append("  from Documento");
			qry.append(" where cdempresa = "+documentoenvioboleto.getEmpresa().getCdpessoa());
			qry.append("   and dtvencimento = '"+dataatualMaisDiascobranca+"'");
			qry.append("   and cddocumentoclasse = "+Documentoclasse.CD_RECEBER);
			qry.append("   and cddocumentoacao in("+whereInDocumentoacao+")");
			if(Util.strings.isNotEmpty(whereInCliente))
				qry.append("   and cdpessoa in("+whereInCliente+")");
			qry.append("   and proximo_dia_util('"+dataatualMaisDiascobranca+"', cdempresa) = '"+dataatualMaisDiascobranca+"'");
				
			qry.append(" union ");
						   
			qry.append("Select cddocumento");
			qry.append("  from Documento");
			qry.append(" where cdempresa = "+documentoenvioboleto.getEmpresa().getCdpessoa());
			qry.append("   and '"+dataatualMaisDiascobranca+"' <= dtvencimento");
			qry.append("   and cddocumentoclasse = "+Documentoclasse.CD_RECEBER);
			qry.append("   and cddocumentoacao in("+whereInDocumentoacao+")");
			if(Util.strings.isNotEmpty(whereInCliente))
				qry.append("   and cdpessoa in("+whereInCliente+")");
			qry.append("   and proximo_dia_util('"+dataatualMaisDiascobranca+"', cdempresa) > dtvencimento");
			if(documentoenvioboleto.getListaDiascobranca().indexOf(diascobranca) < documentoenvioboleto.getListaDiascobranca().size() - 1){
				qry.append(" union ");
			}
		}
		SinedUtil.markAsReader();
		List<Documento> listaDocumento = getJdbcTemplate().query(qry.toString(), new RowMapper() {
											@Override
											public Documento mapRow(ResultSet rs, int rowNum) throws SQLException {
												Documento doc = new Documento(rs.getInt("cddocumento"));
												return doc;
											}
										});

		return listaDocumento;
	}
	
	public List<Documento> findSituacaoDocumentoByPedidovenda(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.equals("")){
			throw new SinedException("Venda n�o podem ser nulas.");
		}
		return querySined()
					.select("documento.cddocumento,documento.documentoacao")
					.join("documento.listaDocumentoOrigem documentoorigem")
					.join("documentoorigem.pedidovenda pedidovenda")
					.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
					.where("pedidovenda = ?", pedidovenda)
					.list();
	}
	
	
	public void cancelaContaReceberVinculadaPedidoVendaCancelado(Documento contaReceber){
		if(Util.objects.isNotPersistent(contaReceber)){
			throw new SinedException("Conta a receber n�o podem ser nulas.");
		}
		getHibernateTemplate().bulkUpdate("update Documento d set d.documentoacao = ? where d.id = ?", new Object[]{Documentoacao.CANCELADA, contaReceber.getCddocumento()});
	}
}