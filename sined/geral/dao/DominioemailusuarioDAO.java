package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Dominioemailusuario;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.DominioemailusuarioFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DominioemailusuarioDAO extends GenericDAO<Dominioemailusuario>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Dominioemailusuario> query, FiltroListagem _filtro) {
		DominioemailusuarioFiltro filtro = (DominioemailusuarioFiltro)_filtro;
		query.select("dominioemailusuario.cddominioemailusuario, dominioemailusuario.dominio, dominioemailusuario.usuario, " +
					 "contrato.descricao, contrato.cliente")
			 .leftOuterJoin("dominioemailusuario.contratomaterial contratomaterial")
		 	 .leftOuterJoin("contratomaterial.contrato contrato")		 	 
		 	 .leftOuterJoin("contrato.cliente cliente")		 	 
		 	 .leftOuterJoin("dominioemailusuario.dominio dominio")		 	 
		 	 .where("contrato.cliente = ?", filtro.getCliente())
		 	 .where("dominioemailusuario.contratomaterial = ?", filtro.getContratomaterial())
		 	 .where("dominioemailusuario.dominio = ?", filtro.getDominio())
		 	 .whereLikeIgnoreAll("dominioemailusuario.usuario", filtro.getUsuario());
			 			 
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Dominioemailusuario> query) {
		query.leftOuterJoinFetch("dominioemailusuario.contratomaterial contratomaterial")
		 	 .leftOuterJoinFetch("contratomaterial.contrato contrato")		 	 
		 	 .leftOuterJoinFetch("dominioemailusuario.dominio dominio");
	}
	
	/**
	 * Retorna lista de acordo com o filtro
	 * @param filtro
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Dominioemailusuario> findByFiltro(DominioemailusuarioFiltro filtro){
		return query().leftOuterJoinFetch("dominioemailusuario.contratomaterial contratomaterial")
				 	  .leftOuterJoinFetch("contratomaterial.contrato contrato")		 	 
				 	  .leftOuterJoinFetch("contrato.cliente cliente")		 	 
				 	  .leftOuterJoinFetch("dominioemailusuario.dominio dominio")		 	 
				 	  .where("dominioemailusuario.contratomaterial = ?", filtro.getContratomaterial())
				 	  .where("dominioemailusuario.dominio = ?", filtro.getDominio())
				 	  .list(); 
	}

	/**
	 * M�todo que apaga a coluna tempor�ria
	 * 
	 * @param bean
	 * @author Tom�s Rabelo
	 */
	public void updateCampoTemporario(Dominioemailusuario bean) {
		if(bean == null || bean.getCddominioemailusuario() == null)
			throw new SinedException("Erro parametros.");
		
		getJdbcTemplate().execute("UPDATE DOMINIOEMAILUSUARIO SET TEMPORARIO = NULL WHERE CDDOMINIOEMAILUSUARIO = " + bean.getCddominioemailusuario());
//		getHibernateTemplate().update("update Dominioemailusuario deu set deu.temporario = "+null+" where deu.id = "+bean.getCddominioemailusuario());
	}

	public int countContratomaterial(Integer cddominioemailusuario, Contratomaterial contratomaterial) {
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.from(Dominioemailusuario.class)
					.where("dominioemailusuario.cddominioemailusuario <> ?", cddominioemailusuario)
					.where("dominioemailusuario.contratomaterial = ?", contratomaterial)
					.unique()
					.intValue();
	}	
}
