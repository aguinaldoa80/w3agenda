package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Dominio;
import br.com.linkcom.sined.geral.bean.Dominiozona;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;



public class DominiozonaDAO extends GenericDAO<Dominiozona>{

	/**
	 * Retorna lista de dominiozona de acordo com o dominio
	 * @param dominio
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Dominiozona> findByDominio(Dominio dominio){
		return query().where("dominiozona.dominio = ?", dominio)
					  .list();		
	}
	
	/**
	 * Retorna lista de zonas de dom�nio que possuam servi�os do tipo SMTP
	 * @param servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Dominiozona> findByServicoCaixaPostal(){
		Servicoservidortipo tipo = new Servicoservidortipo();
		tipo.setCdservicoservidortipo(Servicoservidortipo.CAIXAPOSTAL);
		
		return query().select("distinct dominiozona.cddominiozona, dominiozona.subdominio")
					.join("dominiozona.servicoservidor servicoservidor")
					.where("servicoservidor.servicoservidortipo = ?", tipo)
					.orderBy("dominiozona.subdominio asc")					
					.list();
	}
}
 