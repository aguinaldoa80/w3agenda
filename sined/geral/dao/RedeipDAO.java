package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Rede;
import br.com.linkcom.sined.geral.bean.Redeip;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RedeipDAO extends GenericDAO<Redeip>{
	
	public List<Redeip> findByContratoMaterialRede(Integer cdcontratomaterialrede) {
			return query()
				.leftOuterJoin("redeip.listaContratomaterialrede listaContratomaterialrede")
				.where("listaContratomaterialrede.cdcontratomaterialrede = ? ",cdcontratomaterialrede)
				.list();
	}

	public List<Redeip> findByRedeContratomaterialrede(Rede rede, Integer cdcontratomaterialrede) {
		QueryBuilder<Redeip> query = query()
					.select("redeip.cdredeip, redeip.ip")
					.join("redeip.rede rede")
					.leftOuterJoin("redeip.listaContratomaterialrede listaContratomaterialrede")
					.openParentheses()
					.where("rede = ?", rede)
					.where("not exists (select r1.cdredeip from Contratomaterialrede cr join cr.redeip r1 where r1.cdredeip = redeip.cdredeip)")
					.where("not exists (select r2.cdredeip from Servidorinterfaceip si join si.redeip r2 where r2.cdredeip = redeip.cdredeip)")
					.closeParentheses();
		
		
		if(cdcontratomaterialrede != null){
			query
				.or()
				.openParentheses()
				.where("listaContratomaterialrede.cdcontratomaterialrede = ? ",cdcontratomaterialrede)
				.closeParentheses();
		}
		
		return query.list();
	}
	
	
}
