package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.Campolistagemusuario;
import br.com.linkcom.sined.geral.bean.Tela;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;


public class CampolistagemusuarioDAO extends GenericDAO<Campolistagemusuario>{

	/**
	* M�todo para atualizar a ordem dos campos da listagem e a as colunas exibir/ocultar 
	*
	* @param campolistagemusuario
	* @since Oct 28, 2011
	* @author Luiz Fernando F Silva
	*/
	public void atualizaCamposOrdemUsuario(Campolistagemusuario campolistagemusuario){
		if(campolistagemusuario == null || campolistagemusuario.getCdcampolistagemusuario() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		getHibernateTemplate().bulkUpdate("update Campolistagemusuario clu set clu.camposordem = ?, clu.camposexibicao = ? where clu.cdcampolistagemusuario = ?",
				new Object[]{campolistagemusuario.getCamposordem(),
							 campolistagemusuario.getCamposexibicao(),
							 campolistagemusuario.getCdcampolistagemusuario()});
	}
	
	/**
	* M�todo que busca a ordem e os campos a serem exibidosna listagem definidos pelo usu�rio  
	*
	* @param tela
	* @param usuario
	* @return
	* @since Oct 28, 2011
	* @author Luiz Fernando F Silva
	*/
	public Campolistagemusuario findByTelaUsuario(Tela tela, Usuario usuario) {
		if(tela == null || tela.getCdtela() == null || usuario == null || usuario.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
				.select("campolistagemusuario.cdcampolistagemusuario, campolistagemusuario.camposordem, campolistagemusuario.camposexibicao, " +
						"tela.cdtela, usuario.cdpessoa")
				.join("campolistagemusuario.tela tela")
				.join("campolistagemusuario.usuario usuario")
				.where("tela = ?", tela)
				.where("usuario = ?", usuario)
				.unique();
	}
}
