package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.sined.geral.bean.VendaSincronizacaoFlex;
import br.com.linkcom.sined.util.SinedException;

public class VendaSincronizacaoFlexDAO extends GenericDAO<VendaSincronizacaoFlex>{

	/**
	 * Busca as vendas Sincronizacao Flex para o job de integração
	 * @return
	 */
	public List<VendaSincronizacaoFlex> findForIntegracaoFlex() {
		return query()
				.select("vendaSincronizacaoFlex.cdvendasincronizacaoflex, vendaSincronizacaoFlex.nomeProcesso, venda.cdvenda")
				.leftOuterJoin("vendaSincronizacaoFlex.venda venda")
				.where("vendaSincronizacaoFlex.dtsincronizacao is null")
				.list();
	}

	/**
	 * Atualiza a data de sincronização da VendaSincronizacaoFlex caso
	 * a integração tenha sido realizada
	 * @param vendaFlex
	 */
	public void atualizaDtSincronizacao(VendaSincronizacaoFlex vendaFlex) {
		if(vendaFlex == null || vendaFlex.getCdvendasincronizacaoflex() == null)
			throw new SinedException("Parâmetros inválidos.");
		
		getHibernateTemplate().bulkUpdate("update VendaSincronizacaoFlex v set v.dtsincronizacao = ? where v.cdvendasincronizacaoflex = "+vendaFlex.getCdvendasincronizacaoflex()+"", new Object[]{new Date(System.currentTimeMillis())});
		
	}

}
