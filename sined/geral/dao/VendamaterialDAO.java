package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.FaixaMarkupNome;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.QuantitativaDeVendasFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VendamaterialDAO extends GenericDAO<Vendamaterial> {
	
	/**
	 * Busca os materiais de v�rias vendas
	 *
	 * @param whereInVenda
	 * @param orderBy
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/02/2014
	 */
	public List<Vendamaterial> findByVenda(String whereInVenda, String orderBy){
		if (whereInVenda == null || whereInVenda.trim().equals("")) {
			throw new SinedException("Venda n�o pode ser nulo.");
		}
		
		return queryFindByVenda(orderBy)	
					.whereIn("venda.cdvenda", whereInVenda)
					.list();
	}
	
	
	/**
	 * Busca os materiais de uma determinada venda.
	 *
	 * @param venda
	 * @return
	 * @author Ramon Brazil
	 */
	public List<Vendamaterial> findByVenda(Venda venda, String orderBy){
		if (venda == null || venda.getCdvenda() == null) {
			throw new SinedException("Venda n�o pode ser nulo.");
		}
		
		return queryFindByVenda(orderBy)	
					.where("venda = ?",venda)
					.list();
	}
	
	public List<Vendamaterial> findForExcelByVenda(Venda venda){
		if (venda == null || venda.getCdvenda() == null) {
			throw new SinedException("Venda n�o pode ser nulo.");
		}
		
		return query()
					.select("vendamaterial.cdvendamaterial, vendamaterial.quantidade, vendamaterial.preco, vendamaterial.observacao, vendamaterial.prazoentrega, "
						  + "vendamaterial.valoripi, vendamaterial.multiplicador, vendamaterial.desconto, vendamaterial.valorMinimo, vendamaterial.valorSeguro,"
						  + "vendamaterial.outrasdespesas, material.cdmaterial, material.nome, "
						  + "faixaMarkupNome.cdFaixaMarkupNome, faixaMarkupNome.nome")
					.join("vendamaterial.material material")
					.leftOuterJoin("vendamaterial.faixaMarkupNome faixaMarkupNome")
					.where("vendamaterial.venda = ?",venda)
					.list();
	}
	
	public List<Vendamaterial> findForTotalizacaoVenda(Venda venda){
		if (venda == null || venda.getCdvenda() == null) {
			throw new SinedException("Venda n�o pode ser nulo.");
		}
		
		return query()
					.select("vendamaterial.cdvendamaterial, vendamaterial.quantidade, vendamaterial.preco, vendamaterial.observacao, vendamaterial.prazoentrega, "
						  + "vendamaterial.valoripi, vendamaterial.multiplicador, vendamaterial.desconto")
					.where("vendamaterial.venda = ?",venda)
					.list();
	}

	/**
	 * Centraliza��o da query que busca os itens da venda
	 *
	 * @param orderBy
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/02/2014
	 */
	private QueryBuilder<Vendamaterial> queryFindByVenda(String orderBy) {
		QueryBuilder<Vendamaterial> query = querySined().removeUseWhere()
				.select("vendamaterial.cdvendamaterial, vendamaterial.ordem, material.nome, material.identificacao, vendamaterial.preco, vendamaterial.desconto, vendamaterial.percentualdesconto, vendamaterial.prazoentrega, material.cdmaterial, material.produto, material.servico, " +
						"material.nomenf, material.extipi, material.ncmcompleto, material.codigobarras, localizacaoestoque.descricao, ncmcapitulo.cdncmcapitulo, ncmcapitulo.descricao, unidademedida.simbolo, " +
						"material.patrimonio, material.epi, vendamaterial.quantidade, unidademedida.cdunidademedida, unidademedida.nome, vendamaterial.valorimposto, vendamaterial.cdproducaoordemitemadicional, vendamaterial.cdproducaoagendaitemadicional, " +
						"material.obrigarlote, material.producao, material.vendapromocional, material.pesoliquidovalorvenda, material.peso, material.pesobruto, material.valorcusto, material.nve, material.tributacaoipi, material.tributacaomunicipal, material.tributacaoestadual, " +
						"materialgrupo.cdmaterialgrupo, vendamaterial.valoripi, vendamaterial.ipi, vendamaterial.tipocobrancaipi, vendamaterial.aliquotareaisipi, vendamaterial.tipocalculoipi, grupotributacao.cdgrupotributacao, " +
						"vendamaterial.tipotributacaoicms, " +
						"vendamaterial.tipocobrancaicms, vendamaterial.valorbcicms, vendamaterial.icms, " +
						"vendamaterial.valoricms, vendamaterial.valorbcicmsst, vendamaterial.icmsst, " +
						"vendamaterial.valoricmsst, vendamaterial.reducaobcicmsst, vendamaterial.margemvaloradicionalicmsst, " +
						"vendamaterial.valorbcfcp, vendamaterial.fcp, vendamaterial.valorfcp, " +
						"vendamaterial.valorbcfcpst, vendamaterial.fcpst, vendamaterial.valorfcpst, " +
						"vendamaterial.valorbcdestinatario, vendamaterial.valorbcfcpdestinatario, vendamaterial.fcpdestinatario, " +
						"vendamaterial.icmsdestinatario, vendamaterial.icmsinterestadual, vendamaterial.icmsinterestadualpartilha, " +
						"vendamaterial.valorfcpdestinatario, vendamaterial.valoricmsdestinatario, vendamaterial.valoricmsremetente, " +
						"vendamaterial.difal, vendamaterial.valordifal, ncmcapituloItem.cdncmcapitulo, cfop.cdcfop, " +
						"vendamaterial.percentualdesoneracaoicms, vendamaterial.valordesoneracaoicms, vendamaterial.abaterdesoneracaoicms, " +
						"vendamaterial.ncmcompleto, vendamaterial.modalidadebcicms, vendamaterial.modalidadebcicmsst, " +
						" vendamaterial.saldo, vendamaterial.qtdereferencia, vendamaterial.custooperacional, vendamaterial.percentualcomissaoagencia, vendamaterial.peso, vendamaterial.descontogarantiareforma, vendamaterial.valorMinimo, " +
						"unidademedidamaterial.cdunidademedida, unidademedidamaterial.simbolo, " + 
						"materialmestregrade.cdmaterial, materialmestregrade.nome, materialmestregrade.identificacao," +
						"materialgrupo.cdmaterialgrupo, materialgrupo.nome, material.exibiritenskitvenda, " +
						"materialcoleta.cdmaterial, materialcoleta.nome, material.percentualimpostoecf, " +
						"material.valorvendaminimo, material.valorvendamaximo, vendamaterial.comprimento, vendamaterial.altura, vendamaterial.comprimentooriginal, " +
						"vendamaterial.largura, vendamaterial.fatorconversao, vendamaterial.observacao, loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, " +
						"codigoanp.cdcodigoanp, codigoanp.codigo, material.qtdeunidade, material.considerarvendamultiplos, material.patrimonio, " +
						"vendamaterial.valorcustomaterial, vendamaterial.valorvendamaterial, vendamaterial.multiplicador, material.metrocubicovalorvenda, material.pesoliquidovalorvenda, vendamaterial.producaosemestoque, " +
						"pedidovendamaterial.quantidade, pedidovendamaterial.cdpedidovendamaterial, pedidovendamaterial.desconto, pedidovendamaterial.descontogarantiareforma, arquivo.cdarquivo, arquivoMestre.cdarquivo, materialmestre.cdmaterial, materialmestre.nome, materialmestre.producao, materialmestre.vendapromocional, materialmestre.kitflexivel, fornecedor.cdpessoa, fornecedor.nome, patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, " +
						"listaCaracteristica.cdmaterialcaracteristica, listaCaracteristica.nome, vendamaterial.opcional, fornecedor.razaosocial, fornecedor.cnpj, fornecedor.cpf, fornecedor.tipopessoa, venda.cdvenda, venda.dtvenda, fornecedor.inscricaoestadual, " +
						"listaCaracteristicaMestre.cdmaterialcaracteristica, listaCaracteristicaMestre.nome, " +
						"materialunidademedida.cdmaterialunidademedida, materialunidademedida.fracao, materialunidademedida.qtdereferencia, unidademedidaconversao.cdunidademedida, unidademedidaconversao.nome, unidademedidaconversao.simbolo, " +
						"vendamaterialseparacao.cdvendamaterialseparacao, vendamaterialseparacao.quantidade, unidademedidaseparacao.cdunidademedida, unidademedidaseparacao.nome, unidademedidaseparacao.simbolo, "+
						"materialmestregrupo.cdmaterialgrupo, materialmestregrupo.nome, vendamaterial.identificadorespecifico, vendamaterial.identificadorintegracao, vendamaterial.identificadorinterno, vendamaterial.percentualrepresentacao, vendamaterial.pesomedio, " +
						"materialmestre.codigobarras, " +
						"comissionamento.cdcomissionamento, " +
						"vendamaterial.valorSeguro, vendamaterial.outrasdespesas, " +
						"garantiareformaitem.cdgarantiareformaitem, garantiareforma.cdgarantiareforma, garantiareformapedido.cdgarantiareforma, " +
						"garantiareformaitempedido.cdgarantiareformaitem, pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome, " +
						"materialFaixaMarkup.cdMaterialFaixaMarkup, " +
						"faixaMarkupNome.nome, faixaMarkupNome.cor, faixaMarkupNome.cdFaixaMarkupNome," +
						"materialtipo.cdmaterialtipo, materialcategoria.cdmaterialcategoria, pneu.lonas, pneu.acompanhaRoda, otrpneutipo.cdOtrPneuTipo")
				.join("vendamaterial.venda venda")
				.join("vendamaterial.material material")
				.leftOuterJoin("material.materialtipo materialtipo")
				.leftOuterJoin("material.materialcategoria materialcategoria")
				.leftOuterJoin("vendamaterial.materialcoleta materialcoleta")
				.leftOuterJoin("vendamaterial.materialmestre materialmestre")
				.leftOuterJoin("vendamaterial.patrimonioitem patrimonioitem")
				.leftOuterJoin("vendamaterial.pneu pneu")
				.leftOuterJoin("vendamaterial.ncmcapitulo ncmcapituloItem")
				.leftOuterJoin("pneu.otrPneuTipo otrpneutipo")
				.leftOuterJoin("material.codigoanp codigoanp")
				.leftOuterJoin("material.unidademedida unidademedidamaterial")
				.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("materialmestre.materialgrupo materialmestregrupo")
				.join("vendamaterial.unidademedida unidademedida")
				.leftOuterJoin("vendamaterial.loteestoque loteestoque")
				.leftOuterJoin("vendamaterial.pedidovendamaterial pedidovendamaterial")
				.leftOuterJoin("material.arquivo arquivo")
				.leftOuterJoin("materialmestre.arquivo arquivoMestre")
				.leftOuterJoin("vendamaterial.fornecedor fornecedor")
				.leftOuterJoin("material.listaCaracteristica listaCaracteristica")
				.leftOuterJoin("materialmestre.listaCaracteristica listaCaracteristicaMestre")
				.leftOuterJoin("material.listaMaterialunidademedida materialunidademedida")
				.leftOuterJoin("materialunidademedida.unidademedida unidademedidaconversao")
				.leftOuterJoin("vendamaterial.listaVendamaterialseparacao vendamaterialseparacao")
				.leftOuterJoin("vendamaterialseparacao.unidademedida unidademedidaseparacao")
				.leftOuterJoin("vendamaterial.grupotributacao grupotributacao")
				.leftOuterJoin("vendamaterial.comissionamento comissionamento")
				.leftOuterJoin("vendamaterial.garantiareformaitem garantiareformaitem")
				.leftOuterJoin("garantiareformaitem.garantiareforma garantiareforma")
				.leftOuterJoin("pedidovendamaterial.garantiareformaitem garantiareformaitempedido")
				.leftOuterJoin("garantiareformaitempedido.garantiareforma garantiareformapedido")
				.leftOuterJoin("material.localizacaoestoque localizacaoestoque")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.leftOuterJoin("vendamaterial.materialFaixaMarkup materialFaixaMarkup")
				.leftOuterJoin("vendamaterial.faixaMarkupNome faixaMarkupNome")
				.leftOuterJoin("vendamaterial.cfop cfop")
				;
		
		query = SinedUtil.setJoinsByComponentePneu(query);
		if(orderBy != null && !"".equals(orderBy)){
			query.orderBy(orderBy);
		}else {
			query.orderBy("vendamaterial.ordem, material.nome");
		}
		return query;
	}
	
	/**
	 * M�todo que busca os itens da venda considerando a devolu��o
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 * @since 17/09/2013
	 */
	public List<Vendamaterial> findForResumo(String whereInVenda){
		if (whereInVenda == null || whereInVenda.trim().equals("")) {
			throw new SinedException("Venda n�o pode ser nulo.");
		}
		
		return query()
				.select("vendamaterial.cdvendamaterial,  " +
						"vendamaterial.preco, vendamaterial.desconto, vendamaterial.quantidade, vendamaterial.saldo, vendamaterial.qtdereferencia, " +
						"vendamaterial.comprimento, vendamaterial.altura, vendamaterial.comprimentooriginal, " +
						"vendamaterial.largura, vendamaterial.fatorconversao, vendamaterial.observacao, vendamaterial.producaosemestoque, " +
						"vendamaterial.valorcustomaterial, vendamaterial.valorvendamaterial, vendamaterial.multiplicador, " +
                        "vendamaterial.tipotributacaoicms, " +
                        "vendamaterial.tipocobrancaicms, vendamaterial.valorbcicms, vendamaterial.icms, " +
                        "vendamaterial.valoricms, vendamaterial.valorbcicmsst, vendamaterial.icmsst, " +
                        "vendamaterial.valoricmsst, vendamaterial.reducaobcicmsst, vendamaterial.margemvaloradicionalicmsst, " +
                        "vendamaterial.valorbcfcp, vendamaterial.fcp, vendamaterial.valorfcp, " +
                        "vendamaterial.valorbcfcpst, vendamaterial.fcpst, vendamaterial.valorfcpst, " +
                        "vendamaterial.valorbcdestinatario, vendamaterial.valorbcfcpdestinatario, vendamaterial.fcpdestinatario, " +
                        "vendamaterial.icmsdestinatario, vendamaterial.icmsinterestadual, vendamaterial.icmsinterestadualpartilha, " +
                        "vendamaterial.valorfcpdestinatario, vendamaterial.valoricmsdestinatario, vendamaterial.valoricmsremetente, " +
                        "vendamaterial.percentualdesoneracaoicms, vendamaterial.valordesoneracaoicms, vendamaterial.abaterdesoneracaoicms, " +
                        "vendamaterial.difal, vendamaterial.valordifal, ncmcapitulo.cdncmcapitulo, cfop.cdcfop, " +
                        "vendamaterial.ncmcompleto, vendamaterial.modalidadebcicms, vendamaterial.modalidadebcicmsst, vendamaterial.valorSeguro, vendamaterial.outrasdespesas, " +
						"material.cdmaterial, material.produto, material.servico, material.extipi, unidademedida.simbolo, " +
						"material.patrimonio, material.epi, unidademedida.cdunidademedida, unidademedida.nome, " +
						"material.producao, material.vendapromocional, material.peso, material.pesobruto, material.valorcusto, vendamaterial.peso, " + 
						"unidademedidamaterial.cdunidademedida, unidademedidamaterial.simbolo, " + 
						"material.valorvendaminimo, material.valorvendamaximo, loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, " +
						"material.qtdeunidade, material.considerarvendamultiplos, " +
						"material.metrocubicovalorvenda, material.pesoliquidovalorvenda, venda.cdvenda ")
				.join("vendamaterial.venda venda")
				.join("vendamaterial.material material")
				.leftOuterJoin("material.unidademedida unidademedidamaterial")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.join("vendamaterial.unidademedida unidademedida")
				.leftOuterJoin("vendamaterial.loteestoque loteestoque")
                .leftOuterJoin("vendamaterial.cfop cfop")
                .leftOuterJoin("vendamaterial.ncmcapitulo ncmcapitulo")
				.whereIn("venda.cdvenda", whereInVenda)
//				.where("(select count(*) " +
//					   " from Materialdevolucao md" +
//					   " join md.venda v" +
//					   " where v.cdvenda=venda.cdvenda and md.material.cdmaterial = material.cdmaterial)=0")
				.list();
	}
	
	/**
	 * Busca a lista de venda material para o relat�rio de ordem de produ��o.
	 *
	 * @param venda
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Vendamaterial> findForOrdemProducao(Venda venda) {
		if (venda == null || venda.getCdvenda() == null) {
			throw new SinedException("Venda n�o pode ser nulo.");
		}
		return 
			query()
				.select("vendamaterial.cdvendamaterial, vendamaterial.quantidade, material.cdmaterial, vendamaterial.prazoentrega, " +
						"vendamaterial.prazoentrega, unidademedida.cdunidademedida, unidademedida.simbolo, vendamaterial.comprimento, vendamaterial.comprimentooriginal, " +
						"vendamaterial.altura, vendamaterial.largura, material.identificacao, vendamaterial.fatorconversao, vendamaterial.observacao ")
				.leftOuterJoin("vendamaterial.venda venda")
				.leftOuterJoin("vendamaterial.material material")
				.leftOuterJoin("vendamaterial.unidademedida unidademedida")
				.where("venda = ?", venda)
				.orderBy("vendamaterial.ordem")
				.list();
	}

	/**
	 * M�todo que remove todos os materiais da venda
	 * 
	 * @param venda
	 * @author Tom�s Rabelo
	 * 
	 */
	public void deleteAllFromVenda(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Par�metros inv�lidos.");
		getJdbcTemplate().execute("delete from vendamaterial where cdvenda = "+venda.getCdvenda());
	}
	
	/**
	 * Busca uma lista de vendahistorico para determinado cliente de acordo com o material requisitado.
	 * 
	 * @param cliente,material
	 * @return
	 * @since 12/04/2012
	 * @author Rafael Salvio
	 */
	public List<Vendamaterial> getUltimoValorMaterialByCliente(Material material, Cliente cliente, Empresa empresa, Unidademedida unidadeMedida){
		if(cliente == null || cliente.getCdpessoa() == null || material == null || material.getCdmaterial() == null){
			throw new SinedException("Par�metros n�o podem ser nulos.");
		}
		return query()
				.select("material.cdmaterial, vendamaterial.quantidade, vendamaterial.preco, vendamaterial.desconto, " +
						"venda.dtvenda, venda.cdvenda, pedidovendatipo.cdpedidovendatipo, pedidovendatipo.descricao, " +
						"unidademedida.cdunidademedida, vendamaterial.qtdereferencia, vendamaterial.fatorconversao, " +
						"unidademedidaVOM.cdunidademedida ")
				.from(Vendamaterial.class,"vendamaterial")
				.join("vendamaterial.material material")
				.join("material.unidademedida unidademedida")
				.join("vendamaterial.venda venda")
				.join("vendamaterial.unidademedida unidademedidaVOM")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.where("venda.cliente = ?", cliente)
				.where("venda.empresa = ?", empresa)
				.where("vendamaterial.material = ?", material)
				.where("vendamaterial.materialmestre is null")
				.where("vendamaterial.unidademedida = ?", unidadeMedida)
				.where("venda.vendasituacao <> ?",Vendasituacao.CANCELADA)
				.where("venda.vendasituacao <> ?",Vendasituacao.AGUARDANDO_APROVACAO)
				.orderBy("venda.dtvenda desc , venda.cdvenda desc")
				.list();
	}

	/**
	 * M�todo que busca a vendamaterial com os dados necess�rios para gerar a comiss�o do representante
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Vendamaterial> findForComissaoRepresentante(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nula.");
		
		return query()
				.select("vendamaterial.cdvendamaterial, material.cdmaterial, listaFornecedor.cdmaterialfornecedor, " +
						"fornecedor.cdpessoa, vendamaterial.quantidade, vendamaterialfornecedor.cdpessoa, vendamaterialfornecedor.nome, vendamaterial.preco, vendamaterial.desconto, venda.cdvenda, empresa.cdpessoa, " +
						"materialgrupo.cdmaterialgrupo ")
				.join("vendamaterial.venda venda")
				.join("vendamaterial.material material")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("material.listaFornecedor listaFornecedor")
				.leftOuterJoin("listaFornecedor.fornecedor fornecedor")
				.leftOuterJoin("vendamaterial.fornecedor vendamaterialfornecedor")
				.join("venda.empresa empresa")
				.where("venda  = ?", venda)
				.list();
	}
	
	/**
	 * M�todo que busca o �ltimo valor de venda do material
	 *
	 * @param cdmaterial
	 * @return
	 * @author Luiz Fernando
	 * @param cdempresa 
	 */
	@SuppressWarnings("unchecked")
	public Double getUltimoValor(Integer cdmaterial, Integer cdempresa) {
		
		String sql = 	"SELECT vm.preco AS PRECO " +
						"FROM vendamaterial vm " +
						"JOIN venda v on v.cdvenda = vm.cdvenda " +
						"WHERE vm.cdmaterial = " + cdmaterial + " " +
						(cdempresa != null ? " and v.cdempresa = " + cdempresa + " " : "") +
						"ORDER BY v.dtvenda desc limit 1";
		

		SinedUtil.markAsReader();
		List<Double> lista = getJdbcTemplate().query(sql, 
			new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return rs.getDouble("PRECO");
				}
	
			});
		
		if(lista == null || lista.size() == 0){
			return null;
		} else {
			return lista.get(0);
		}
	}

	/**
	 * Busca os itens para gerar produ��o
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 22/07/2013
	 */
	public List<Vendamaterial> findByVendaProducao(String whereIn) {
		QueryBuilder<Vendamaterial> query = query()
				.select("vendamaterial.cdvendamaterial, material.nome, vendamaterial.quantidade, " +
						"vendamaterial.altura, vendamaterial.largura, vendamaterial.comprimento, " +
						"material.cdmaterial, material.servico, material.patrimonio, " +
						"material.produto, material.epi, venda.cdvenda, cliente.cdpessoa, " +
						"producaoetapa.cdproducaoetapa, vendamaterial.altura, vendamaterial.largura, vendamaterial.comprimento," +
						"pedidovendatipo.cdpedidovendatipo, producaoagendatipo.cdproducaoagendatipo, " +
						"producaoetapaPedido.cdproducaoetapa, empresa.cdpessoa, " +
						"unidademedida.cdunidademedida, unidademedida.simbolo, unidademedida.nome ")
				.join("vendamaterial.venda venda")
				.join("venda.cliente cliente")
				.join("vendamaterial.material material")
				.leftOuterJoin("venda.empresa empresa")
				.leftOuterJoin("material.producaoetapa producaoetapa")
				.leftOuterJoin("vendamaterial.unidademedida unidademedida")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("pedidovendatipo.producaoagendatipo producaoagendatipo")
				.leftOuterJoin("producaoagendatipo.producaoetapa producaoetapaPedido")
				.leftOuterJoin("vendamaterial.pneu pneu")
			    .where("material.producao = ?", Boolean.TRUE)
				.where("vendamaterial.producaosemestoque = ?", Boolean.TRUE)
				.whereIn("venda.cdvenda", whereIn)
				.orderBy("venda.cdvenda, vendamaterial.ordem");
		query = SinedUtil.setJoinsByComponentePneu(query);
		boolean isOtr = ParametrogeralService.getInstance().getBoolean(Parametrogeral.HABILITAR_CONFIGURACAO_OTR);
		if(isOtr){
			query = query.openParentheses()
						.where("coalesce(venda.origemOtr, false) = false")
						.or()
							.openParentheses()
							.where("coalesce(venda.origemOtr, false) = true")
							.where("material.producao = true")
							.closeParentheses()
						.closeParentheses();
		}
		return query.list();
	}
	
	public void updateSaldo(Vendamaterial vm, Double saldo) {
		getHibernateTemplate().bulkUpdate("update Vendamaterial vm set vm.saldo = ? where vm.id = ?", new Object[]{saldo, vm.getCdvendamaterial()});
	}


	public Vendamaterial findMaiorDataEntrega(Venda venda) {
		if(venda == null || venda.getCdvenda() == null){
			throw new SinedException("Venda n�o pode ser nula.");
		}
		return query()
			.select("vendamaterial.cdvendamaterial, vendamaterial.prazoentrega")
			.where("vendamaterial.prazoentrega = (select max(vendamaterial.prazoentrega) from vendamaterial vm where vm.cdvendamaterial = vendamaterial.cdvendamaterial)")
			.where("vendamaterial.venda = ?", venda)
			.unique();
	}
	
	/**
	 * Busca o material e a quantidade para a popup de industrializa��o
	 *
	 * @param whereInVenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/04/2015
	 */
	public List<Vendamaterial> findByVendaForIndustrializacao(String whereInVenda) {
		return query()
					.select("vendamaterial.cdvendamaterial, material.cdmaterial, material.nome, vendamaterial.quantidade")
					.join("vendamaterial.material material")
					.whereIn("vendamaterial.venda.cdvenda", whereInVenda)
					.list();
	}
	
	/**
	 * M�todo que busca os materiais vendidos por per�odo para c�lculo do valor m�dio de venda.
	 * @param dtinicio
	 * @param dtfim
	 * @author Danilo Guimar�es
	 */
	public List<Vendamaterial> findByMaterialAndDataForAnaliseCusto(Material material, Date dtinicio, Date dtfim){
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo.");
		if(dtinicio == null)
			throw new SinedException("dtinicio n�o pode ser nulo.");
		if(dtfim == null)
			dtfim = SinedDateUtils.currentDate();
		
		return query()
				.select("vendamaterial.cdvendamaterial, vendamaterial.preco, vendamaterial.quantidade")
				.join("vendamaterial.venda venda")
				.where("venda.vendasituacao <> ?", Vendasituacao.CANCELADA)
				.where("vendamaterial.material = ?", material)
				.wherePeriodo("venda.dtvenda", dtinicio, dtfim)
				.list();
	}

	/**
	* M�todo que faz o update do pneu no item da venda
	*
	* @param vendamaterial
	* @param pneu
	* @since 03/02/2016
	* @author Luiz Fernando
	*/
	public void updatePneu(Vendamaterial vendamaterial, Pneu pneu) {
		if(vendamaterial != null && vendamaterial.getCdvendamaterial() != null && pneu != null && pneu.getCdpneu() != null){
			getHibernateTemplate().bulkUpdate("update Vendamaterial vm set vm.pneu = ? where vm.id = ?", new Object[]{pneu, vendamaterial.getCdvendamaterial()});
		}
	}
	
	/**
	* M�todo que remove o pneu do item da venda
	*
	* @param vendamaterial
	* @since 11/02/2016
	* @author Luiz Fernando
	*/
	public void removePneu(Vendamaterial vendamaterial) {
		if(vendamaterial != null && vendamaterial.getCdvendamaterial() != null){
			getHibernateTemplate().bulkUpdate("update Vendamaterial vm set vm.pneu = null where vm.pneu is not null and vm.id = ?", new Object[]{vendamaterial.getCdvendamaterial()});
		}
	}
	
	public List<Vendamaterial> findForConferenciaMateriaisFromVenda(String whereIn) {
		return query()
				.select("vendamaterial.cdvendamaterial, vendamaterial.quantidade, venda.cdvenda, material.cdmaterial, material.nome, material.codigobarras, " +
						"material.desconsiderarcodigobarras, unidademedida.cdunidademedida, unidademedida.nome, " +
						"listaEmbalagem.cdembalagem, listaEmbalagem.codigoBarras, " +
						"unidadeMedidaEmbalagem.cdunidademedida, unidadeMedidaEmbalagem.nome, " +
						"listaExpedicaoitem.cdexpedicaoitem, listaExpedicaoitem.qtdeexpedicao, " +
						"expedicao.cdexpedicao, expedicao.expedicaosituacao, " +
						"material.desconsiderarcodigobarras, pedidovendatipo.cdpedidovendatipo, pedidovendatipo.criarExpedicaoComConferencia, " +
						"loteestoque.cdloteestoque, loteestoque.numero")
				.join("vendamaterial.venda venda")
				.join("vendamaterial.material material")
				.leftOuterJoin("vendamaterial.loteestoque loteestoque")
				.leftOuterJoin("vendamaterial.unidademedida unidademedida")
				.leftOuterJoin("material.listaEmbalagem listaEmbalagem")
				.leftOuterJoin("listaEmbalagem.unidadeMedida unidadeMedidaEmbalagem")
				.leftOuterJoin("vendamaterial.listaExpedicaoitem listaExpedicaoitem")
				.leftOuterJoin("listaExpedicaoitem.expedicao expedicao")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.whereIn("venda.cdvenda", whereIn)
				.where("coalesce(pedidovendatipo.criarExpedicaoComConferencia, false) = true", ParametrogeralService.getInstance().getBoolean(Parametrogeral.SEPARACAO_EXPEDICAO))
				.orderBy("venda.cdvenda")
				.list();
	}
	
	public List<Vendamaterial> findForConferenciaMateriaisFromExpedicao(String whereIn) {
		return query()
				.select("vendamaterial.cdvendamaterial, vendamaterial.quantidade, venda.cdvenda, material.cdmaterial, material.nome, material.codigobarras, " +
						"unidademedida.cdunidademedida, unidademedida.nome ")
				.join("vendamaterial.venda venda")
				.join("vendamaterial.material material")
				.join("vendamaterial.listaExpedicaoitem listaExpedicaoitem")
				.join("listaExpedicaoitem.expedicao expedicao")
				.leftOuterJoin("vendamaterial.unidademedida unidademedida")
				.whereIn("expedicao.cdexpedicao", whereIn)
				.orderBy("venda.cdvenda")
				.list();
	}

	/**
	* M�todo que retorna a lista de itens adicionais da ordem de produ��o adicionados na venda
	*
	* @param whereInProducaoordemitemadicional
	* @return
	* @since 04/10/2016
	* @author Luiz Fernando
	*/
	public List<Vendamaterial> findByItensAdicionais(String whereInProducaoordemitemadicional, String whereInProducaoagendaitemadicional) {
		if(StringUtils.isBlank(whereInProducaoordemitemadicional) && StringUtils.isBlank(whereInProducaoagendaitemadicional))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("vendamaterial.cdvendamaterial, vendamaterial.quantidade, vendamaterial.cdproducaoordemitemadicional")
				.join("vendamaterial.venda venda")
				.where("venda.vendasituacao <> ? ", Vendasituacao.CANCELADA)
				.whereIn("vendamaterial.cdproducaoordemitemadicional", whereInProducaoordemitemadicional)
				.whereIn("vendamaterial.cdproducaoagendaitemadicional", whereInProducaoagendaitemadicional)
				.list();
	}
	
	public void limpaGarantiareformaitem(Vendamaterial vendamaterial){
		String sql = "update vendamaterial set cdgarantiareformaitem = null " + 
					 " where cdvendamaterial = " + vendamaterial.getCdvendamaterial();
		getJdbcTemplate().execute(sql);		
	}
	
	public List<Vendamaterial> findForNotaDevolucaoColetaByVenda(String whereInVenda){
		if(StringUtils.isBlank(whereInVenda)) return new ArrayList<Vendamaterial>();
		
		return query()
			.select("vendamaterial.cdvendamaterial, vendamaterial.observacao, " +
					"pedidovendamaterial.cdpedidovendamaterial, venda.cdvenda, " +
					"loteestoque.cdloteestoque, loteestoque.numero ")
			.join("vendamaterial.venda venda")
			.join("vendamaterial.pedidovendamaterial pedidovendamaterial")
			.leftOuterJoin("vendamaterial.loteestoque loteestoque")
			.whereIn("venda.cdvenda", whereInVenda)
			.list();
	}

	
	public List<Vendamaterial> findAllForReportQuantitativaDeVendas(QuantitativaDeVendasFiltro filtro) {
		QueryBuilder<Vendamaterial> query = query();
		query.select(
				"vendamaterial.cdvendamaterial, vendamaterial.quantidade, vendamaterial.largura, vendamaterial.altura, vendamaterial.comprimento, vendamaterial.valorvendamaterial, vendamaterial.preco, vendamaterial.fatorconversao, vendamaterial.qtdereferencia, " +
				"vmunidademedida.cdunidademedida, vmunidademedida.simbolo, " +
				"venda.dtvenda, material.cdmaterial, material.identificacao, material.nome," +
				"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo"
		)
		.join("vendamaterial.venda venda")
		.join("vendamaterial.material material")
		.join("material.unidademedida unidademedida")
		.join("vendamaterial.unidademedida vmunidademedida")
		.where("venda.dtvenda >= ?", SinedDateUtils.dateToBeginOfDay(filtro.getDtInicio()))
		.where("venda.dtvenda <= ?", SinedDateUtils.dataToEndOfDay(filtro.getDtFim()))
		.where("venda.vendasituacao <> ?", Vendasituacao.CANCELADA)
		.where("material.materialgrupo = ?", filtro.getMaterialgrupo())
		.where("material.materialtipo = ?", filtro.getMaterialtipo())
		.where("venda.empresa = ?", filtro.getEmpresa());
		return query.list();
	}
	
	public Vendamaterial loadByPedidovendamaterial(Pedidovendamaterial pedidovendamaterial, Venda venda){
		return query()
		.select("vendamaterial.cdvendamaterial, vendamaterial.observacao, " +
				"pedidovendamaterial.cdpedidovendamaterial, venda.cdvenda, " +
				"loteestoque.cdloteestoque, loteestoque.numero ")
		.join("vendamaterial.venda venda")
		.join("vendamaterial.pedidovendamaterial pedidovendamaterial")
		.leftOuterJoin("vendamaterial.loteestoque loteestoque")
		.where("pedidovendamaterial = ?", pedidovendamaterial)
		.where("venda = ?", venda)
		.unique();
	}
	
	public void updateFaixaMarkup(Vendamaterial vm, FaixaMarkupNome faixaMarkup) {
		getHibernateTemplate().bulkUpdate("update Vendamaterial vm set vm.faixaMarkupNome = ? where vm = ?", new Object[]{faixaMarkup, vm});
	}
	
	/*
	 * vendamaterial = getQuantidade getMultiplicador getPreco getDesconto
	 * venda = valorfrete taxapedidovenda desconto valorusadovalecompra
	 */
	public List<Vendamaterial> findTotalVendaByVenda(Venda venda) {
		if (venda == null || venda.getCdvenda() == null) {
			throw new SinedException("Venda n�o pode ser nulo.");
		}
		
		return query()
				.select("vendamaterial.quantidade, vendamaterial.multiplicador, vendamaterial.preco, vendamaterial.desconto, " +
						"venda.valorfrete, venda.taxapedidovenda, venda.desconto, venda.valorusadovalecompra ")
				.join("vendamaterial.venda venda")
				.where("venda = ?",venda)
				.list();
	}
	
	public Vendamaterial loadForValidacaoServico(Vendamaterial vendaMaterial) {
		if (Util.objects.isNotPersistent(vendaMaterial)) {
			throw new SinedException("Vendamaterial n�o pode ser nulo.");
		}
		
		return query()
				.select("vendamaterial.cdvendamaterial, material.servico, material.tributacaomunicipal")
				.join("vendamaterial.material material")
				.where("vendamaterial = ?", vendaMaterial)
				.setMaxResults(1)
				.unique();
	}

	public void updateValorComissao(Vendamaterial vm) {
		if (vm == null || vm.getCdvendamaterial() == null || vm.getValorComissao() == null) {
			throw new SinedException("Par�metros inv�lidos.");
		}
		
		getHibernateTemplate().bulkUpdate("update Vendamaterial vm set valorComissao = ? where vm = ?", new Object[] { vm.getValorComissao(), vm });
	}
	
	public Vendamaterial loadWithLoteEstoque(Vendamaterial vm){
		return querySined()
				.select("vendamaterial.cdvendamaterial, material.cdmaterial, loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade")
				.join("vendamaterial.loteestoque loteestoque")
				.join("vendamaterial.material material")
				.where("vendamaterial = ?", vm)
				.setMaxResults(1)
				.unique();
	}
	
	public boolean isEntregaFutura(Vendamaterial vm){
		if(Util.objects.isNotPersistent(vm)){
			return false;
		}
		List<Vendamaterial> lista = querySined()
										.join("vendamaterial.venda venda")
										.join("venda.pedidovendatipo pedidovendatipo")
										.where("vendamaterial = ?", vm)
										.where("pedidovendatipo.entregaFutura = true")
										.list();
				
		return SinedUtil.isListNotEmpty(lista);
	}
	
	public void updateCampo(Vendamaterial vm, String nomeCampo, Object valor) {
		if (Util.objects.isNotPersistent(vm)) {
			throw new SinedException("Par�metros inv�lidos.");
		}
		
		getHibernateTemplate().bulkUpdate("update Vendamaterial vm set "+nomeCampo+" = ? where vm = ?", new Object[] { valor, vm });
	}
}