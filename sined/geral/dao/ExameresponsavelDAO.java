package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Exameresponsavel;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ExameresponsavelFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ExameresponsavelDAO extends GenericDAO<Exameresponsavel> {

	
	@Override
	public void updateEntradaQuery(QueryBuilder<Exameresponsavel> query) {
		query
			.leftOuterJoinFetch("exameresponsavel.listaExameconvenioresponsavel exameconvenioresponsavel");
	}
	
	
	@Override
	public void updateListagemQuery(QueryBuilder<Exameresponsavel> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		ExameresponsavelFiltro filtro = (ExameresponsavelFiltro) _filtro;
		query
		.select("exameresponsavel.cdexameresponsavel, exameresponsavel.nome, exameresponsavel.nit, exameresponsavel.registroconselho")
		.whereLikeIgnoreAll("exameresponsavel.nome", filtro.getNome());
	}
	
	/* singleton */
	private static ExameresponsavelDAO instance;
	public static ExameresponsavelDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ExameresponsavelDAO.class);
		}
		return instance;
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {		
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				save.saveOrUpdateManaged("listaExameconvenioresponsavel");
				return null;
			}			
		});		
	}	

	/**
	 * Retorna todos os registros de Cl�nica.
	 * Permite tamb�m a especifica��o dos campos do select e defini��o de ordena��o.
	 * @param campos
	 * @param orderBy
	 * @return
	 * @author Taidson
	 * @since 22/09/2010
	 */
	public List<Exameresponsavel> findAll(String campos, String orderBy) {
		QueryBuilder<Exameresponsavel> queryBuilder = query();
		if (campos != null && !campos.trim().equals("")) {
			queryBuilder.select(campos);
		}
		if (orderBy != null && !orderBy.trim().equals("")) {
			queryBuilder.orderBy(orderBy);
		}
		
		return queryBuilder.list();
	}
}