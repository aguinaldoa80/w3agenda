package br.com.linkcom.sined.geral.dao;


import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.view.Vwsaidafiscal;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.VwsaidafiscalFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VwsaidafiscalDAO extends GenericDAO<Vwsaidafiscal> {
		
	
	@Override
	public void updateListagemQuery(QueryBuilder<Vwsaidafiscal> query, FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		}
		
		VwsaidafiscalFiltro filtro = (VwsaidafiscalFiltro) _filtro;
		
		query
			.orderBy("dtemissao desc");
		
		String listaEmpresa = new SinedUtil().getListaEmpresa();
		if(listaEmpresa != null && !listaEmpresa.equals("")){
			query
				.openParentheses()
					.where("vwsaidafiscal.cdempresa in ( " + listaEmpresa + ")").or()
					.where("vwsaidafiscal.cdempresa is null")
				.closeParentheses();
		}
		
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null){
			query.where("cdempresa = ?", filtro.getEmpresa().getCdpessoa());			
		}
		
		if(filtro.getDtemissaoinicio() != null){
			query.where("dtemissao >= ?", filtro.getDtemissaoinicio());
		}
		if(filtro.getDtemissaofim() != null){
			query.where("dtemissao <= ?", filtro.getDtemissaofim());
		}
		if(filtro.getCliente() != null && filtro.getCliente().getCdpessoa() != null){
			query.where("cdcliente = ?", filtro.getCliente().getCdpessoa());
		}
		if(filtro.getNumero() != null && !"".equals(filtro.getNumero())){
			query.where("numero = ?", filtro.getNumero());
		}
		if(filtro.getListanotastatus() != null){
			query.whereIn("cdnotastatus", CollectionsUtil.listAndConcatenate(filtro.getListanotastatus(), "cdNotaStatus", ","));
		}
		
	}
	
	public List<Vwsaidafiscal> findForCsv(VwsaidafiscalFiltro filtro) {
		QueryBuilder<Vwsaidafiscal> query = query();
		this.updateListagemQuery(query, filtro);
		return query.list();
	}
}