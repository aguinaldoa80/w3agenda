package br.com.linkcom.sined.geral.dao;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MaterialtipoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("materialtipo.nome")
public class MaterialtipoDAO extends GenericDAO<Materialtipo> {

	@Override
	public void updateListagemQuery(QueryBuilder<Materialtipo> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		MaterialtipoFiltro filtro = (MaterialtipoFiltro) _filtro;
		query
			.select("materialtipo.cdmaterialtipo, materialtipo.nome, materialtipo.veiculo, materialtipo.ativo")
			.whereLikeIgnoreAll("materialtipo.nome", filtro.getNome())
			.where("materialtipo.ativo = ?", filtro.getAtivo());
	}
	/**
	 * M�todo para obter uma lista de tipos por planejamento.
	 * 
	 * @param planejamento
	 * @return
	 * @author Ramon Brazil
	 */
	public List<Materialtipo> findByTipo(Planejamento planejamento){
		if(planejamento == null || planejamento.getCdplanejamento() == null){
			throw new SinedException("Os par�metros planejamento ou cdplanejamento n�o podem ser null.");
		}
		
		return 
			query()
			.select("materialtipo.cdmaterialtipo, materialtipo.nome")
			.join("materialtipo.listaMaterial material")
			.join("material.listaTarefarecursogeral tcg")
			.join("tcg.tarefa tarefa")
			.join("tarefa.planejamento planejamento")
			.where("planejamento = ?",planejamento)
			.orderBy("materialtipo.nome")
			.list();
	}
	
	public List<Materialtipo> findByServicoservidortipo(Servicoservidortipo[] servicoservidortipos) {
		QueryBuilder<Materialtipo> query = query()
												.select("materialtipo.cdmaterialtipo, materialtipo.nome")
												.join("materialtipo.listaServico listaServico");
		
		query.openParentheses();
		for (int i = 0; i < servicoservidortipos.length; i++) {
			query.where("listaServico = ?", servicoservidortipos[i]).or();
		}
		query.closeParentheses();
		
		return query.list();
	}
	
	/**
	 * M�todo que busca todos os materiais tipos que possuem material do tipo servi�o relacionado
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 * @param materialtipo 
	 */
	public List<Materialtipo> findMaterialTipoPossuiMaterialServico(Materialtipo materialtipo) {
		return query()
			.select("materialtipo.cdmaterialtipo, materialtipo.nome")
			.openParentheses()
				.where("materialtipo.ativo = ?", Boolean.TRUE)
				.where("materialtipo.id in (select materialtipo.id from Material material " +
					   "join material.materialtipo materialtipo where material.servico = ?)", Boolean.TRUE)
			.closeParentheses().or()
			.where("materialtipo = ?", materialtipo)
			.orderBy("materialtipo.nome desc")
			.list();
	}
	
	/**
	 * M�todo que busca o tipo de material de acordo com a altura e largura
	 *
	 * @param altura
	 * @param largura
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Materialtipo> buscaMaterialtipoIntervaloAlturaLargura(Double altura, Double largura) {
		if(altura == null || largura == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("materialtipo.cdmaterialtipo, materialtipo.nome, materialtipo.largurade, materialtipo.alturade, " +
					"materialtipo.larguraate, materialtipo.alturaate")
			.where("materialtipo.alturade <= ?", altura)
			.where("materialtipo.alturaate >= ?", altura)
			.where("materialtipo.largurade <= ?", largura)
			.where("materialtipo.larguraate >= ?", largura)
			.where("materialtipo.ativo = true")
			.list();		
	}
	
	/**
	 * Busca o tipo de material pelo nome passado por par�metro para a importa��o de dados.
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/01/2013
	 */
	public Materialtipo findByNomeForImportacao(String nome) {
		if(nome == null || nome.equals("")){
			throw new SinedException("Nome do tipo de material n�o pode ser nulo.");
		}
		
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		List<Materialtipo> lista = query()
			.select("materialtipo.cdmaterialtipo, materialtipo.nome")
			.where("UPPER(" + funcaoTiraacento + "(materialtipo.nome)) LIKE ?", Util.strings.tiraAcento(nome).toUpperCase())
			.list();
		
		if(lista != null && lista.size() > 1){
			throw new SinedException("Mais de um tipo de material encontrado.");
		} else if(lista != null && lista.size() == 1){
			return lista.get(0);
		} else return null;
	}
	
	/**
	 * M�todo que realiza insere um cdarquivo no cdmaterialtipo repassado.
	 * 
	 * @param cdmaterialtipo
	 * @author Rafael Salvio
	 * @throws Exception 
	 */
	public void bulkUpdateCdarquivolegenda(Map<Integer,Integer> mapa) throws Exception{
		if(mapa == null || mapa.isEmpty()){
			return;
		}
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		String sql = "UPDATE materialtipo SET cdarquivolegenda = ? WHERE cdmaterialtipo = ?";
		for(Integer cdmaterialtipo : mapa.keySet()){
			Integer cdarquivolegenda = mapa.get(cdmaterialtipo);
			jdbcTemplate.update(sql, new Object[]{cdarquivolegenda, cdmaterialtipo});
		}
		
	}

	@Override
	public ListagemResult<Materialtipo> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Materialtipo> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("materialtipo.cdmaterialtipo");
		
		return new ListagemResult<Materialtipo>(query, filtro);
	}
}
