package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Contratomaterialrede;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.ContratomaterialredeFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContratomaterialredeDAO extends GenericDAO<Contratomaterialrede>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Contratomaterialrede> query, FiltroListagem _filtro) {
		ContratomaterialredeFiltro filtro = (ContratomaterialredeFiltro)_filtro;
		query.select("contratomaterialrede.cdcontratomaterialrede, contratomaterialrede.mac, servidor.ambienterede," +
					 "servidorinterface.servidor, servidorinterface.nome, contratomaterialrede.ipautomatico, " +
					 "contratomaterialrede.usuario, contrato.cliente, redeip.ip, contratomaterialrede.ativo")
			 .leftOuterJoin("contratomaterialrede.servidorinterfacepreferencial servidorinterface")
			 .leftOuterJoin("contratomaterialrede.enderecocliente enderecocliente")
			 .leftOuterJoin("servidorinterface.servidor servidor")
			 .leftOuterJoin("servidor.ambienterede ambienterede")
			 .leftOuterJoin("contratomaterialrede.servicoservidortipo servicoservidortipo")
		 	 .leftOuterJoin("contratomaterialrede.contratomaterial contratomaterial")
		 	 .leftOuterJoin("contratomaterial.contrato contrato")		 	 
		 	 .leftOuterJoin("contratomaterialrede.redeip redeip")
		 	 .leftOuterJoin("redeip.rede rede")
		 	 .where("contrato.cliente = ?", filtro.getCliente())
		 	 .where("contratomaterialrede.contratomaterial = ?", filtro.getContratomaterial())
			 .where("contratomaterialrede.mac = ?", filtro.getMac())
			 .where("contratomaterialrede.servicoservidortipo = ?", filtro.getServicoservidortipo())
			 .where("ambienterede = ?", filtro.getAmbienterede())
			 .where("servidor = ?", filtro.getServidor())
			 .where("servidorinterface = ?", filtro.getServidorinterfacepreferencial())
			 .where("rede = ?", filtro.getRede())
			 .where("redeip.ip = ?", filtro.getRedeip())
			 .where("contratomaterialrede.ativo = ?", filtro.getAtivo())
			 .whereLikeIgnoreAll("enderecocliente.logradouro", filtro.getEnderecocliente())
			 .whereLikeIgnoreAll("contratomaterialrede.usuario", filtro.getUsuario())
			 ;
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Contratomaterialrede> query) {
		query
			.select("contratomaterialrede.cdcontratomaterialrede, contratomaterial.cdcontratomaterial, cliente.cdpessoa, " +
					"contratomaterialrede.mac, servicoservidortipo.cdservicoservidortipo, servidorinterface.cdservidorinterface, " +
					"contratomaterialrede.restringirinterface, contratomaterialrede.ipautomatico, contratomaterialrede.usuario, " +
					"contratomaterialrede.senha, contratomaterialrede.cdusuarioaltera, contratomaterialrede.dtaltera, redeip.cdredeip, " +
					"rede.cdrede, servidor.cdservidor, ambienterede.cdambienterede, enderecocliente.cdendereco, contratomaterialrede.ativo")
			.leftOuterJoin("contratomaterialrede.servidorinterfacepreferencial servidorinterface")
			.leftOuterJoin("servidorinterface.servidor servidor")
			.leftOuterJoin("servidor.ambienterede ambienterede")
			.leftOuterJoin("contratomaterialrede.servicoservidortipo servicoservidortipo")
			.leftOuterJoin("contratomaterialrede.contratomaterial contratomaterial")
			.leftOuterJoin("contratomaterial.contrato contrato")		 	 
			.leftOuterJoin("contrato.cliente cliente")		 	 
			.leftOuterJoin("contratomaterialrede.enderecocliente enderecocliente")		 	 
			.leftOuterJoin("contratomaterialrede.redeip redeip")
			.leftOuterJoin("redeip.rede rede");
	}

	public int countContratomaterial(Integer cdcontratomaterialrede, Contratomaterial contratomaterial) {
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.from(Contratomaterialrede.class)
					.where("contratomaterialrede.cdcontratomaterialrede <> ?", cdcontratomaterialrede)
					.where("contratomaterialrede.contratomaterial = ?", contratomaterial)
					.unique()
					.intValue();
	}

	/**
	* M�todo para gerar relat�rio com os campos do filtro
	*
	* @param contratomaterialredeFiltro
	* @return
	* @since Jul 20, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Contratomaterialrede> findForReport(ContratomaterialredeFiltro contratomaterialredeFiltro) {
		if(contratomaterialredeFiltro == null){
			throw new SinedException("Par�metro inv�lido");
		}
		
		return query()
				.select("contratomaterialrede.cdcontratomaterialrede, contratomaterialrede.mac, cliente.cdpessoa, " +
				 "ambienterede.cdambienterede, ambienterede.nome, servidor.cdservidor, servidorinterface.nome, contratomaterialrede.ipautomatico, " +
				 "contratomaterialrede.usuario, contrato.cliente, redeip.ip, contratomaterialrede.ativo, " +
				 "endereco.cdendereco, endereco.logradouro, endereco.numero, contratomaterial.cdcontratomaterial, " +
				 "endereco.bairro, servicoservidortipo.cdservicoservidortipo, servico.cdmaterial, servico.nome, " +
				 "servicoservidortipo.nome, servidor.cdservidor, servidor.nome, uf.cduf, uf.sigla, municipio.cdmunicipio, municipio.nome")				 
				 .leftOuterJoin("contratomaterialrede.enderecocliente endereco")
				 .leftOuterJoin("contratomaterialrede.servidorinterfacepreferencial servidorinterface")				 				 			 
				 .leftOuterJoin("servidorinterface.servidor servidor")
				 .leftOuterJoin("servidor.ambienterede ambienterede")
				 .leftOuterJoin("contratomaterialrede.servicoservidortipo servicoservidortipo")
			 	 .leftOuterJoin("contratomaterialrede.contratomaterial contratomaterial")			 	 
			 	 .leftOuterJoin("contratomaterial.contrato contrato")
			 	 .leftOuterJoin("contrato.cliente cliente")
			 	 .leftOuterJoin("contratomaterial.servico servico")
			 	 .leftOuterJoin("contratomaterialrede.redeip redeip")
			 	 .leftOuterJoin("redeip.rede rede")
			 	 .leftOuterJoin("endereco.municipio municipio")
			 	 .leftOuterJoin("municipio.uf uf")
			 	 .where("contrato.cliente = ?", contratomaterialredeFiltro.getCliente())
			 	 .where("contratomaterialrede.contratomaterial = ?", contratomaterialredeFiltro.getContratomaterial())
				 .where("contratomaterialrede.mac = ?", contratomaterialredeFiltro.getMac())
				 .where("contratomaterialrede.servicoservidortipo = ?", contratomaterialredeFiltro.getServicoservidortipo())
				 .where("ambienterede = ?", contratomaterialredeFiltro.getAmbienterede())
				 .where("servidor = ?", contratomaterialredeFiltro.getServidor())
				 .where("servidorinterface = ?", contratomaterialredeFiltro.getServidorinterfacepreferencial())
				 .where("rede = ?", contratomaterialredeFiltro.getRede())
				 .where("redeip.ip = ?", contratomaterialredeFiltro.getRedeip())
				 .where("contratomaterialrede.ativo = ?", contratomaterialredeFiltro.getAtivo())
				 .whereLikeIgnoreAll("enderecocliente.logradouro", contratomaterialredeFiltro.getEnderecocliente())
				 .whereLikeIgnoreAll("contratomaterialrede.usuario", contratomaterialredeFiltro.getUsuario())
				 .list();
					
	}

}
