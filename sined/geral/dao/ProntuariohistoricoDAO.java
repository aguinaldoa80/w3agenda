package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.Prontuariohistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProntuariohistoricoDAO extends GenericDAO<Prontuariohistorico> {
	
	@Override
	public Prontuariohistorico load(Prontuariohistorico bean) {
		return query()
				.select("prontuariohistorico.cdprontuariohistorico, prontuario.cdprontuario, terapeuta.cdpessoa, terapeuta.nome, paciente.cdpessoa," +
						"paciente.nome, prontuariohistorico.numero, prontuariohistorico.dtinicio, prontuariohistorico.hrinicio, prontuariohistorico.dtfim," +
						"prontuariohistorico.hrfim, prontuariohistorico.observacao, usuario.nome")
				.join("prontuariohistorico.prontuario prontuario")
				.join("prontuariohistorico.terapeuta terapeuta")
				.join("prontuariohistorico.paciente paciente")
				.join("prontuariohistorico.usuario usuario")
				.where("prontuariohistorico=?", bean)
				.unique();
	}
}
