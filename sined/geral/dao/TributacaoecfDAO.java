package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Tributacaoecf;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TributacaoecfDAO extends GenericDAO<Tributacaoecf>{

	@Override
	public void updateEntradaQuery(QueryBuilder<Tributacaoecf> query) {
		query.leftOuterJoinFetch("tributacaoecf.cfop cfop");
	}
	
}
