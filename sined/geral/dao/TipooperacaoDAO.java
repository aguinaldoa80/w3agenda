package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("tipooperacao.nome")
public class TipooperacaoDAO extends GenericDAO<Tipooperacao>{
	
	public List<Tipooperacao> find(String whereIn){
		return querySined()
				
				.whereIn("tipooperacao.cdtipooperacao", whereIn)
				.orderBy("tipooperacao.nome")
				.list();
	}	
}
