package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Documentoenvioboleto;
import br.com.linkcom.sined.geral.bean.Documentoenvioboletohistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentoenvioboletohistoricoDAO extends GenericDAO<Documentoenvioboletohistorico>{

	public List<Documentoenvioboletohistorico> findByDocumentoenvioboleto(Documentoenvioboleto documentoenvioboleto){
		return query()
				.select("documentoenvioboletohistorico.cddocumentoenvioboletohistorico, documentoenvioboletohistorico.documentoenvioboletoacao, "+
					"documentoenvioboleto.cddocumentoenvioboleto, documentoenvioboletohistorico.cdusuarioaltera, documentoenvioboletohistorico.dtaltera, "+
					"usuarioaltera.cdpessoa, usuarioaltera.nome")
				.join("documentoenvioboletohistorico.documentoenvioboleto documentoenvioboleto")
				.leftOuterJoin("documentoenvioboletohistorico.usuarioaltera usuarioaltera")
				.where("documentoenvioboleto = ?", documentoenvioboleto)
				.list();
	}
}
