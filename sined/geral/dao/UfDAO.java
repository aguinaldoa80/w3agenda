package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.UfFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("uf.sigla")
public class UfDAO extends GenericDAO<Uf> {
	
	/* singleton */
	private static UfDAO instance;
	public static UfDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(UfDAO.class);
		}
		return instance;
	}
	
	/**
	 * Busca a UF pela sigla.
	 *
	 * @param sigla
	 * @return
	 * @since Jul 5, 2011
	 * @author Rodrigo Freitas
	 */
	public Uf findBySigla(String sigla) {
		if(sigla == null || sigla.equals("")){
			throw new SinedException("Sigla n�o pode ser nula.");
		}
		return query()
					.select("uf.cduf, uf.sigla")
					.whereLikeIgnoreAll("uf.sigla",sigla)
					.unique();
	}

/**
	 * Metodo que carrega a Uf
	 *
	 * @name findForUf
	 * @param uf
	 * @return
	 * @return Uf
	 * @author Thiago Augusto
	 * @date 16/04/2012
	 *
	 */
	public Uf findForCodigo(Integer cdibge){
		if (cdibge == null){
			throw new SinedException("C�digo n�o pode ser nulo.");
		}
		return query()
		.where("uf.cdibge = ?", cdibge)
		.unique();
	}


	/**
     * Busca os dados para o cache da tela de pedido de venda offline
     * @author Igor Silv�rio Costa
	 * @date 20/04/2012
	 * 
	 */
	public List<Uf> findForPVOffline() {
		return query()
		.select("uf.cduf, uf.nome, uf.sigla")
		.list();
	}
	
	public List<Uf> findForAndroid(String whereIn) {
		return query()
		.select("uf.cduf, uf.nome, uf.sigla")
		.whereIn("uf.cduf", whereIn)
		.list();
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Uf> query, FiltroListagem _filtro) {
		if( _filtro == null)
			throw new SinedException("Par�metro inv�lido");
		
		UfFiltro filtro = (UfFiltro) _filtro;
		query
			.whereLikeIgnoreAll("uf.nome", filtro.getNome())
			.whereLikeIgnoreAll("uf.sigla", filtro.getSigla());
	}
}
