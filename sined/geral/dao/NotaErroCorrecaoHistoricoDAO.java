package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.sined.geral.bean.Notaerrocorrecao;
import br.com.linkcom.sined.geral.bean.NotaErroCorrecaoHistorico;


public class NotaErroCorrecaoHistoricoDAO extends GenericDAO<NotaErroCorrecaoHistorico> {
	
	public List<NotaErroCorrecaoHistorico> findByNotaerrocorrecaoHistorico(Notaerrocorrecao notaerrocorrecao) {
		return query()		
			.select("notaerrocorrecaoHistorico.dataaltera, notaerrocorrecaoHistorico.acaoexecutada, notaerrocorrecaoHistorico.responsavel, notaerrocorrecaoHistorico.observacao")
			.leftOuterJoin("notaerrocorrecaoHistorico.notaerrocorrecao notaerrocorrecao")
			.where("notaerrocorrecao = ?", notaerrocorrecao)
			.list();
	}
	
}