package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Agendamentoservicocancela;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("retira_acento(upper(agendamentoservicocancela.nome))")
public class AgendamentoservicocancelaDAO extends GenericDAO<Agendamentoservicocancela> {
	
}
