package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Veiculodespesa;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.service.DocumentohistoricoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.TaxaService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.RelatorioMargemContribuicaoFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.DocumentoFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.AcompanhamentoEconomicoReportFiltro;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportarFolhaPagamentoBean;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.ImportarFolhaPagamentoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("documento.cddocumento desc")
public class ContapagarDAO extends GenericDAO<Documento> {
	
	private TaxaService taxaService;
	private RateioService rateioService;
	private DocumentoDAO documentoDAO;
	private ArquivoDAO arquivoDAO;
	private DocumentohistoricoService documentohistoricoService;
	
	public void setTaxaService(TaxaService taxaService) {
		this.taxaService = taxaService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setDocumentoDAO(DocumentoDAO documentoDAO) {
		this.documentoDAO = documentoDAO;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService) {
		this.documentohistoricoService = documentohistoricoService;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Documento> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}		
		DocumentoFiltro filtro = (DocumentoFiltro) _filtro;
		filtro.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
		
		documentoDAO.updateListagemQuery(query, _filtro);
		
		documentoDAO.addRestricaoFornecedorEmpresa(query);
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Documento> query) {
		query.leftOuterJoinFetch("documento.aux_documento aux_documento");
//		query.leftOuterJoinFetch("documento.pessoa pessoa");
//		query.leftOuterJoin("documento.empresa empresa");
		query.joinFetch("documento.aux_documento aux_documento");
		query.leftOuterJoinFetch("documento.documentotipo documentotipo");			
		query.leftOuterJoinFetch("documento.documentoacao documentoacao");
		query.leftOuterJoinFetch("documento.listaDocumentohistorico listaDocumentohistorico");
		
		query.leftOuterJoinFetch("documento.listaParcela parcela");
		query.leftOuterJoinFetch("parcela.parcelamento parcelamento");
		query.leftOuterJoinFetch("parcelamento.prazopagamento prazopagamento");
		query.leftOuterJoinFetch("documento.listaDocumentoOrigem listaDocumentoOrigem");
		query.leftOuterJoinFetch("documento.imagem imagem");
		query.leftOuterJoinFetch("documento.cheque cheque");
		query.leftOuterJoinFetch("documento.operacaocontabil operacaocontabil");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaApropriacao");
		save.saveOrUpdateManaged("listaDocumentohistorico");
		final Documento bean = (Documento) save.getEntity();
		bean.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
		
		Taxa taxas = bean.getTaxa();
		if(taxas != null){
			taxaService.saveOrUpdateNoUseTransaction(taxas);
		}
		
		Rateio rateio = bean.getRateio();
		if(rateio != null){
			rateioService.saveOrUpdateNoUseTransaction(rateio);
		}
		
		if(SinedUtil.isListNotEmpty(bean.getListaNotaDocumento())){
			save.saveOrUpdateManaged("listaNotaDocumento");
		}
		
		if (bean.getImagem() != null)
			arquivoDAO.saveFile(bean, "imagem");
	}
	
	/**
	 * Faz o update do status das contas a pagar.
	 * Salva o usuario logado e a data corrente na atualiza��o.
	 * 
	 * @param whereIn
	 * @param documentoacao
	 * @author Rodrigo Freitas
	 * @author Fl�vio Tavares
	 */
	public void updateStatusConta(Documentoacao documentoacao, String whereIn, Documentoacao ... condicaoAcao) throws SinedException{
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Documento n�o pode ser nulo nem string vazia.");
		}
		if (documentoacao == null || documentoacao.getCddocumentoacao() == null) {
			throw new SinedException("DocumentoAcao n�o pode ser nulo.");
		}
		
		Integer cdusuarioaltera = SinedUtil.getUsuarioLogado() != null ? SinedUtil.getUsuarioLogado().getCdpessoa() : null; 
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
			
		StringBuilder sql = new StringBuilder(
				"update Documento d " +
				"set d.documentoacao.id = ?, " +
				(cdusuarioaltera != null ? "d.cdusuarioaltera = ?, " : " ") + 
				"d.dtaltera = ? " +
				"where d.id in ("+whereIn+")"
		);
			
		List<Documentoacao> listaCondicao = new ArrayList<Documentoacao>();
		if(condicaoAcao != null && condicaoAcao.length > 0){
			for (int i = 0; i < condicaoAcao.length; i++) {
				if(condicaoAcao[i] != null) listaCondicao.add(condicaoAcao[i]);
			}
			if(listaCondicao != null && listaCondicao.size() > 0){
				String idsCondicao = CollectionsUtil.listAndConcatenate(listaCondicao, "cddocumentoacao", ",");
				sql.append(" and d.documentoacao.id in ("+ idsCondicao +")");
			}
		}
		Object[] fields;
		if(cdusuarioaltera != null){
			fields = new Object[]{documentoacao.getCddocumentoacao(),cdusuarioaltera,hoje};
		} else {
			fields = new Object[]{documentoacao.getCddocumentoacao(),hoje};
		}
			
		int rows = getHibernateTemplate().bulkUpdate(sql.toString(), fields);
		
		if(rows == 0){
			throw new SinedException("Nenhum registro atualizado.");
		}
	}
	
	/**
	 * Retorna uma lista de contas a partir de uma lista de documentos. 
	 * - WHERE <cddocumento> IN (<cddocumento>,<cddocumento>,<cddocumento>,...) 
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Documento> findContas(String whereIn){
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Documento n�o pode ser nulo nem string vazia.");
		}
		return query()
					.select("documento.cddocumento, documentoacao.cddocumentoacao, documento.valor," +
							"venda.cdvenda, pessoa.cdpessoa, pessoa.nome, documento.outrospagamento," +
							"documento.tipopagamento,empresa.nomefantasia,empresa.cdpessoa,documentotipo.cddocumentotipo," +
							"documentotipo.nome,documento.numero,documento.dtemissao,documento.dtvencimento," +
							"documento.valor,rateio.cdrateio,rateioitem.cdrateioitem,rateioitem.observacao," +
							"rateioitem.valor,rateioitem.percentual,projeto.cdprojeto,projeto.nome," +
							"centrocusto.nome,centrocusto.cdcentrocusto,contagerencial.nome,contagerencial.cdcontagerencial")
					.join("documento.documentoacao documentoacao")
					.leftOuterJoin("documento.pessoa pessoa")
					.leftOuterJoin("documento.empresa empresa")
					.leftOuterJoin("documento.rateio rateio")
					.leftOuterJoin("rateio.listaRateioitem rateioitem")
					.leftOuterJoin("rateioitem.centrocusto centrocusto")
					.leftOuterJoin("rateioitem.contagerencial contagerencial")
					.leftOuterJoin("rateioitem.projeto projeto")
					.leftOuterJoin("documento.documentotipo documentotipo")
					.leftOuterJoin("documento.listaDocumentoOrigem documentoorigem")
					.leftOuterJoin("documentoorigem.venda venda")
					.whereIn("documento.cddocumento", whereIn)
					.list();
	}
	
	/**
	 * Retorna uma lista de contas a partir de uma lista de documentos. 
	 * - WHERE <cddocumento> IN (<cddocumento>,<cddocumento>,<cddocumento>,...) 
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Documento> findContasPreenchidas(String whereIn){
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Documento n�o pode ser nulo nem string vazia.");
		}
		return query()
					.select("documento.cddocumento, pessoa.cdpessoa, pessoa.nome, documento.cddocumento, documento.numero, documento.descricao, " +
							"documento.valor, documento.dtvencimento, documento.dtemissao, documentoacao.cddocumentoacao, documento.outrospagamento, documento.tipopagamento, " +
							"documentotipo.cddocumentotipo, documentotipo.antecipacao, documentotipo.gerarmovimentacaoseparadataxa, " +
							"tipotaxacontareceber.cdtipotaxa, documentotipo.taxavenda, contagerencialtaxa.cdcontagerencial, " +
							"listaDocumentohistorico.valor, listaDocumentohistorico.dtaltera, listaDocumentoautori.valor,rateio.cdrateio," +
							"documentoclasse.cddocumentoclasse, empresa.cdpessoa, empresa.nome, empresa.nomefantasia, auxdocumento.valoratual, cheque.cdcheque, cheque.numero," +
							"cheque.valor, cheque.agencia, cheque.banco, cheque.emitente, cheque.conta, cheque.dtbompara, operacaocontabil.cdoperacaocontabil," +
							"operacaocontabil.nome, vinculoProvisionado.cdconta, vinculoProvisionado.nome, taxa.cdtaxa, taxaitem.cdtaxaitem," +
							"taxaitem.percentual, taxaitem.aodia, taxaitem.valor, taxaitem.tipocalculodias, taxaitem.dias, taxaitem.dtlimite, tipotaxa.cdtipotaxa," +
							"tipotaxa.nome, formapagamentocredito.cdformapagamento, formapagamentocredito.nome, formapagamentodebito.cdformapagamento, formapagamentodebito.nome, "+
							"parcela.ordem, parcelamento.iteracoes, contabancariaCheque.cdconta, contabancariaCheque.nome")
					.leftOuterJoin("documento.pessoa pessoa")
					.leftOuterJoin("documento.empresa empresa")
					.join("documento.documentoclasse documentoclasse")
					.join("documento.aux_documento auxdocumento")
					.leftOuterJoin("documento.listaDocumentohistorico listaDocumentohistorico")
					.join("documento.documentoacao documentoacao")
					.leftOuterJoin("documento.listaDocumentoautori listaDocumentoautori")
					.leftOuterJoin("documento.rateio rateio")
					.leftOuterJoin("documento.cheque cheque")
					.leftOuterJoin("cheque.contabancaria contabancariaCheque")
					.leftOuterJoin("documento.operacaocontabil operacaocontabil")
					.leftOuterJoin("documento.vinculoProvisionado vinculoProvisionado")
					.leftOuterJoin("documento.documentotipo documentotipo")
					.leftOuterJoin("documentotipo.tipotaxacontareceber tipotaxacontareceber")
					.leftOuterJoin("documentotipo.contagerencialtaxa contagerencialtaxa")
					.leftOuterJoin("documentotipo.formapagamentocredito formapagamentocredito")
					.leftOuterJoin("documentotipo.formapagamentodebito formapagamentodebito")
					.leftOuterJoin("documento.taxa taxa")
					.leftOuterJoin("taxa.listaTaxaitem taxaitem")
					.leftOuterJoin("taxaitem.tipotaxa tipotaxa")
					.leftOuterJoin("documento.listaParcela parcela")
					.leftOuterJoin("parcela.parcelamento parcelamento")
					.whereIn("documento.cddocumento", whereIn)
					.orderBy("documento.cddocumento, listaDocumentohistorico.dtaltera desc")
					.list();
	}
	
	/**
	 * Faz o update do status e da dtcartorio das contas a pagar.
	 * 
	 * @param whereIn
	 * @param documentoacao
	 * @author Rodrigo Freitas
	 */
	public void updateCartorio(String whereIn, Date dtcartorio, Documentoacao acaoProtesto) {
		if (StringUtils.isBlank(whereIn)) {
			throw new SinedException("Documento n�o pode ser nulo nem string vazia.");
		}
		if (dtcartorio == null) {
			throw new SinedException("Data de cart�rio n�o pode ser nula.");
		}
		if (acaoProtesto == null) {
			throw new SinedException("Situa��o do protesto n�o pode ser nula.");
		}
		getHibernateTemplate().bulkUpdate("update Documento d set d.dtcartorio = ?, d.documentoacaoprotesto = ? " +
				"where d.id in ("+whereIn+") and (d.dtcartorio is null or " +
				" (d.documentoacaoprotesto is null or d.documentoacaoprotesto <> ?) ) ", new Object[]{dtcartorio, acaoProtesto, acaoProtesto});
		
	}
	
	/**
	 * Carrega a lista de documentos a partir de um whereIn para baixar as contas.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Documento> findListaDocumentoBaixarConta(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("WhereIn n�o pode ser nulo ou vazio.");
		}
		return query()
					.select("documento.cddocumento, documento.numero, documento.descricao, documento.dtvencimento, documento.tipopagamento, documento.outrospagamento," +
							"documento.dtemissao, documento.valor, documento.codigobarras, pessoa.cdpessoa, endereco.logradouro, " +
							"pessoa.nome, dadobancario.conta, municipio.nome, uf.sigla, endereco.numero, " +
							"endereco.complemento, endereco.cep, dadobancario.dvconta, dadobancario.dvagencia, dadobancario.agencia, " +
							"pessoa.cnpj, pessoa.cpf, banco.nome, banco.numero, taxa.cdtaxa, documento.dtvencimento, documentotipo.cddocumentotipo, " +
							"documentotipo.taxavenda, centrocusto.cdcentrocusto, cheque.cdcheque ")
					
					.leftOuterJoin("documento.documentotipo documentotipo")
					.leftOuterJoin("documentotipo.centrocusto centrocusto")
					.leftOuterJoin("documento.pessoa pessoa")
					.leftOuterJoin("documento.taxa taxa")
					.leftOuterJoin("pessoa.listaEndereco endereco")
					.leftOuterJoin("endereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("pessoa.listaDadobancario dadobancario")
					.leftOuterJoin("dadobancario.banco banco")
					.leftOuterJoin("documento.cheque cheque")
					.whereIn("documento.cddocumento", whereIn)
					.list();
	}
	
	/**
	 * Atualiza a data de vencimento de um documento.
	 * 
	 * @param whereIn
	 * @param data
	 * @author Rodrigo Freitas
	 */
	public void updateDataVencimento(String whereIn, Date data) {

		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Documento n�o pode ser nulo nem string vazia.");
		}
		if (data == null) {
			throw new SinedException("Data n�o pode ser nulo.");
		}
		
		Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		
//		ATUALIZA A DATA DE VENCIMENTO ORIGINAL CASO ELA SEJA NULA
//		List<Documento> listaDocumento = documentoDAO.carregaDocumentos(whereIn);
//		for (Documento documento : listaDocumento) {
//			if (documento.getDtvencimentooriginal() == null){
//				documento.setDtvencimentooriginal(documento.getDtvencimento());
//				getHibernateTemplate().bulkUpdate("update Documento d set d.dtvencimentooriginal = ?, d.cdusuarioaltera = ?, d.dtaltera = ? where d.id = "+documento.getCddocumento()+" ", 
//						new Object[]{documento.getDtvencimentooriginal(),cdusuarioaltera,hoje});
//			}
//		}
		
		
		getHibernateTemplate().bulkUpdate("update Documento d set d.dtvencimentoantiga = d.dtvencimento, d.dtvencimento = ?, d.cdusuarioaltera = ?, d.dtaltera = ? where d.id in ("+whereIn+")",
				new Object[]{data,cdusuarioaltera,hoje});
	}
	
	/**
	 * M�todo que busca as contas a pagar de acordo com par�metro informado na busca
	 * 
	 * @param busca
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Documento> findContasPagarForBuscaGeral(String busca) {
		return query()
			.select("documento.cddocumento, documento.descricao, documentoclasse.cddocumentoclasse")
			.join("documento.documentoclasse documentoclasse")
			.where("documentoclasse = ?", Documentoclasse.OBJ_PAGAR)
			.openParentheses()
				.whereLikeIgnoreAll("documento.descricao", busca)
			.closeParentheses()
			.orderBy("documento.descricao")
			.list();
	}
	
	/**
	 * Retorna a soma do valor repassado (relat�rio de agendamentos de servi�os) vinculada ao colaborador. 
	 * 
	 * @author Taidson
	 * @since 10/05/2010
	 * 
	 */
	public Long contaPagarSoma(Colaborador colaborador, Date dtInicio, Date dtFim){
		String sql = "select sum(valor) from documento d " +
				"join colaborador c on (d.cdpessoa = c.cdpessoa) " +
				"join documentoclasse dc on (d.cddocumentoclasse = dc.cddocumentoclasse) " +
				"where d.cdpessoa = " + colaborador.getCdpessoa() +
				" and dc.cddocumentoclasse = 1" + 
				" and d.dtemissao >= '"+ dtInicio +"'" + " and d.dtemissao <= '"+ dtFim +"'";
		return this.queryForLong(sql);
	}
		
	private Long queryForLong(String sql){
		SinedUtil.markAsReader();
		Long result = getJdbcTemplate().queryForLong(sql);
		return new Long(result);
	}
	
	/**
	 * Verifica se usu�rio tem permiss�o em autorizar contat a pagar
	 * @param usuario
	 * @return
	 * @author Taidson
	 * @since 09/06/2010
	 */
	public Boolean permiteAutorizarContaPagar(Usuario usuario){
		String sql ="select count(*) from usuariopapel up join papel p on (up.cdpapel = p.cdpapel) " +
				"join acaopapel ap on (p.cdpapel = ap.cdpapel) join acao a on (ap.cdacao = a.cdacao) " +
				"where a.key = 'AUTORIZAR_CONTAPAGAR' and ap.permitido is TRUE and up.cdpessoa = " +
				usuario.getCdpessoa();

		SinedUtil.markAsReader();
		Integer count = getJdbcTemplate().queryForInt(sql.toString());
		
		if(count != null && count > 0) return true;
		return false;
	}
	
	/**
	 * Busca o somat�rio do valor total gasto em um projeto pela contagerencial e dtFim.
	 *
	 * @param filtro
	 * @param contagerencial
	 * @return
	 * @since 13/10/2011
	 * @author Rodrigo Freitas
	 */
	public Money getValorTotalProjeto(AcompanhamentoEconomicoReportFiltro filtro, Contagerencial contagerencial) {
		if(filtro == null || filtro.getProjeto() == null || filtro.getProjeto().getCdprojeto() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		List<Documentoacao> listaAcao = new ArrayList<Documentoacao>();
		listaAcao.add(Documentoacao.PREVISTA);
		listaAcao.add(Documentoacao.DEFINITIVA);
		listaAcao.add(Documentoacao.AUTORIZADA);
		listaAcao.add(Documentoacao.AUTORIZADA_PARCIAL);
		listaAcao.add(Documentoacao.BAIXADA);
		
		QueryBuilder<Long> query = newQueryBuilder(Long.class)
			.from(Documento.class)
			.select("SUM(rateioitem.valor)")
			.join("documento.documentoacao documentoacao")
			.leftOuterJoin("documento.listaDocumentoOrigem documentoorigem")
			.leftOuterJoin("documento.rateio rateio")
			.leftOuterJoin("rateio.listaRateioitem rateioitem")
			.leftOuterJoin("rateioitem.contagerencial contagerencial")
			.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			
			.openParentheses()
				.where("vcontagerencial.identificador like '" + contagerencial.getVcontagerencial().getIdentificador() + "%'")
				.or()
				.where("rateioitem.contagerencial = ?", contagerencial)
			.closeParentheses()

			.where("rateioitem.projeto = ?", filtro.getProjeto())
			
			.where("documento.documentoclasse = ?", Documentoclasse.OBJ_PAGAR)
			.where("documentoorigem.entrega is null")
			.whereIn("documentoacao.cddocumentoacao", CollectionsUtil.listAndConcatenate(listaAcao, "cddocumentoacao", ","))
			.where("documento.dtvencimento <= ?", filtro.getDtFim())
			;
		
				
		Long totalMaterial = query.setUseTranslator(false).unique();
		
		if (totalMaterial != null)
			return new Money(totalMaterial, true);
		else 
			return new Money();
	}
	
	/**
	 * M�todo que retorna o total das contas vinculadas ao cheque
	 *
	 * @param documento
	 * @param cheque
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getTotalDocumentoByCheque(Documento documento, Cheque cheque) {
		QueryBuilder<Long> query = newQueryBuilder(Long.class)
			.from(Documento.class)
			.select("SUM(documento.valor)")
			.leftOuterJoin("documento.cheque cheque")
			.join("documento.documentoacao documentoacao")
			.where("documento.documentoclasse = ?", Documentoclasse.OBJ_PAGAR)
			.where("documentoacao.cddocumentoacao <> ?", Documentoacao.CANCELADA.getCddocumentoacao())
			.where("cheque = ?", cheque);
		
		if(documento != null && documento.getCddocumento() != null){
			query.where("documento.cddocumento <> ?", documento.getCddocumento());
		}
	
		Long total = query.setUseTranslator(false).unique();
		
		if (total != null)
			return new Money(total, true);
		else 
			return new Money();
	}
	
	/**
	 * M�todo que carrega a conta a pagar para registrar entrada fiscal
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public Documento loadForRegistrarEntradafiscal(Documento documento){
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Documento n�o pode ser nulo.");
		
		return query()
				.select(
						"documentotipo.cddocumentotipo, documento.dtemissao, documento.dtvencimento, documento.descricao, " +
						"pessoa.cdpessoa, pessoa.nome, documento.numero, documento.valor, documento.tipopagamento, " +
						"documento.outrospagamento, aux_documento.valoratual, documento.dtvencimentooriginal, " +
						"documento.descricao, documento.cddocumento, documento.dtcartorio, documentoacao.cddocumentoacao, documentoacao.nome, " +
						"documentoclasse.cddocumentoclasse, rateio.cdrateio, item.cdrateioitem, item.valor, item.percentual, " +
						"cc.cdcentrocusto, cg.cdcontagerencial, proj.cdprojeto, indicecorrecao.cdindicecorrecao, empresa.cdpessoa, " +
						"listaParcela.cdparcela, docparcela.cddocumento, docparcela.valor, parcelamento.cdparcelamento, " +
						"docparcela.dtvencimento, prazopagamento.cdprazopagamento, prazopagamento.nome ")

				.join("documento.aux_documento aux_documento")
				.join("documento.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem item")
				.leftOuterJoin("item.centrocusto cc")
				.leftOuterJoin("item.projeto proj")
				.leftOuterJoin("item.contagerencial cg")
				.leftOuterJoin("documento.indicecorrecao indicecorrecao")
				.leftOuterJoin("documento.empresa empresa")
				.leftOuterJoin("documento.documentotipo documentotipo")
				.join("documento.documentoacao documentoacao")
				.join("documento.documentoclasse documentoclasse")
				.leftOuterJoin("documento.pessoa pessoa")
				.leftOuterJoin("documento.listaParcela listaParcela")
				.leftOuterJoin("listaParcela.documento docparcela")
				.leftOuterJoin("listaParcela.parcelamento parcelamento")
				.leftOuterJoin("parcelamento.prazopagamento prazopagamento")
				.where("documento = ?", documento)
				.where("documentoclasse = ?", Documentoclasse.OBJ_PAGAR)
				.unique();
	}
	
	/**
	 * M�todo que verifica se o documento tem origem de entrega ou entrada fiscal avulsa
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isOrigemEntregaOrEntradafiscalAvulsa(Documento documento) {
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Documento n�o pode ser nulo.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Documento.class)
			.join("documento.listaDocumentoOrigem listaDocumentoOrigem")
			.leftOuterJoin("listaDocumentoOrigem.entrega entrega")
			.leftOuterJoin("listaDocumentoOrigem.entregadocumento entregadocumento")
			.where("documento = ?", documento)
			.openParentheses()
				.where("entrega is not null")
				.or()
				.where("entregadocumento is not null")
			.closeParentheses()
			.unique() > 0;
	}
	
	public List<Documento> findForUpdateBaixaByDocumentoCancelado(String whereInDocumento) {
		if(whereInDocumento == null || "".equals(whereInDocumento))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("documento.cddocumento, listaDocumentoOrigem.cddocumentoorigem, fornecimento.cdfornecimento")
				.join("documento.listaDocumentoOrigem listaDocumentoOrigem")
				.join("listaDocumentoOrigem.fornecimento fornecimento")
				.whereIn("documento.cddocumento", whereInDocumento)
				.where("fornecimento.baixado = true")
				.list();
	}
	
	/**
	 * M�todo que carrega as contas a pagar e seus respectivos rateios para atualiza��o.
	 * 
	 * @param whereInDocumento
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Documento> findForAtualizarRateio(String whereInDocumento){
		if(whereInDocumento == null || "".equals(whereInDocumento))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("documento.cddocumento, documento.valor, rateio.cdrateio, " +
					"rateioitem.cdrateioitem, centrocusto.cdcentrocusto, contagerencial.cdcontagerencial")
			.join("documento.rateio rateio")
			.join("rateio.listaRateioitem rateioitem")
			.join("rateioitem.centrocusto centrocusto")
			.join("rateioitem.contagerencial contagerencial")
			.whereIn("documento.cddocumento", whereInDocumento)
			.list();
	}
	
	/**
	* M�todo que busca os documentos com origem de adiantamentos de despesas de viagem
	*
	* @param whereIn
	* @return
	* @since 16/02/2016
	* @author Luiz Fernando
	*/
	public List<Documento> findAdiantamentoByDespesaviagem(String whereIn) {
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("documento.cddocumento, despesaviagemadiantamento.cddespesaviagem ")
				.join("documento.listaDocumentoOrigem listaDocumentoOrigem")
				.join("listaDocumentoOrigem.despesaviagemadiantamento despesaviagemadiantamento")
				.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
				.where("exists ( select docOrigem.cddocumentoorigem " +
								"from Documentoorigem docOrigem " +
								"join docOrigem.documento doc " +
								"join docOrigem.despesaviagemadiantamento dva " +
								"where dva.cddespesaviagem in (" + whereIn + ") and doc.cddocumento = documento.cddocumento) ")
				.list();
	}
	
	public void saveTransactionImportarFolhaPagamento(final ImportarFolhaPagamentoFiltro filtro, final List<ImportarFolhaPagamentoBean> listaBean){
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				for (ImportarFolhaPagamentoBean bean: listaBean){
					Documento documento = new Documento();
					documento.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
					documento.setEmpresa(bean.getEmpresa());
					documento.setDocumentotipo(filtro.getDocumentotipo());
					documento.setTipopagamento(Tipopagamento.COLABORADOR);
					documento.setPessoa(bean.getColaborador());
					documento.setDescricao(bean.getDescricao());
					documento.setDtemissao(SinedDateUtils.currentDate());
					documento.setDtvencimento(filtro.getDtvencimento());
					documento.setValor(bean.getValor());
					documento.setDocumentoacao(Documentoacao.DEFINITIVA);
					
					Rateioitem rateioitem = new Rateioitem();
					rateioitem.setContagerencial(bean.getContagerencial());
					rateioitem.setCentrocusto(bean.getCentrocusto());
					rateioitem.setValor(bean.getValor());
					rateioitem.setPercentual(100D);
					
					Rateio rateio = new Rateio();
					rateio.setListaRateioitem(new ListSet<Rateioitem>(Rateioitem.class));
					rateio.getListaRateioitem().add(rateioitem);
					
					documento.setRateio(rateio);
					
					saveOrUpdateNoUseTransaction(documento);
					
					Documentohistorico documentohistorico = documentohistoricoService.geraHistoricoDocumento(documento);
					documentohistoricoService.saveOrUpdateNoUseTransaction(documentohistorico);
				}
				
				return null;
			}
		});
	}
	
	public List<Documento> findByDiasFaltantes(Date data) {
		List<Documentoacao> listaSituacao = new ArrayList<Documentoacao>();
		listaSituacao.add(Documentoacao.PREVISTA);
		listaSituacao.add(Documentoacao.DEFINITIVA);
		listaSituacao.add(Documentoacao.AUTORIZADA);
		
		return query()
				.where("documento.dtvencimento <= ?", SinedDateUtils.addDiasData(data, 2))
				.where("documento.dtvencimento >= ?", data)
				.where("documentoclasse.cddocumentoclasse = ?", Documentoclasse.CD_PAGAR)
				.whereIn("documentoacao.cddocumentoacao", CollectionsUtil.listAndConcatenate(listaSituacao, "cddocumentoacao", ","))
				.join("documento.documentoclasse documentoclasse")
				.join("documento.documentoacao documentoacao")
				.list();
	}
	
	public List<Documento> findByAtrasado(Date data, Date dateToSearch) {
		List<Documentoacao> listaSituacao = new ArrayList<Documentoacao>();
		listaSituacao.add(Documentoacao.PREVISTA);
		listaSituacao.add(Documentoacao.DEFINITIVA);
		listaSituacao.add(Documentoacao.AUTORIZADA);
		
		return querySined()
				.setUseWhereClienteEmpresa(false)
				.setUseWhereEmpresa(false)
				.setUseWhereFornecedorEmpresa(false)
				.setUseWhereProjeto(false)
				.select("documento.cddocumento, documento.numero, empresa.cdpessoa")
				.join("documento.documentoclasse documentoclasse")
				.join("documento.documentoacao documentoacao")
				.leftOuterJoin("documento.empresa empresa")
				.where("documento.dtvencimento < ?", data)
				.where("documentoclasse.cddocumentoclasse = ?", Documentoclasse.CD_PAGAR)
				.whereIn("documentoacao.cddocumentoacao", CollectionsUtil.listAndConcatenate(listaSituacao, "cddocumentoacao", ","))
				.wherePeriodo("documento.dtvencimento", dateToSearch, SinedDateUtils.currentDate())
				.list();
	}
	public boolean verificaContaCanceladaVeiculoDespesa(Veiculodespesa veiculodespesa) {
		if(veiculodespesa == null || veiculodespesa.getCdveiculodespesa() == null)
			throw new SinedException("O despesa do ve�culo n�o pode ser nula.");
		
		return newQueryBuilderSined(Long.class)
				
			.select("count(*)")
			.from(Documento.class)
			.join("documento.listaDocumentoOrigem listaDocumentoOrigem")
			.join("documento.documentoacao documentoacao")
			.where("listaDocumentoOrigem.veiculodespesa = ?", veiculodespesa)
			.where("documentoacao.cddocumentoacao <> 0")
			.unique() > 0;
	}
	
	public Money buscarGastoEmpresa(RelatorioMargemContribuicaoFiltro filtro) {
		List<Documentoacao> listaSituacao = new ArrayList<Documentoacao>();
		listaSituacao.add(Documentoacao.DEFINITIVA);
		listaSituacao.add(Documentoacao.AUTORIZADA);
		
		Money gastosEmpresa = new Money();
		
		Long valor = new QueryBuilder<Long>(getHibernateTemplate())
				.select("sum(listaRateioitem.valor)")
				.setUseTranslator(false)
				.from(Documento.class)
				.leftOuterJoin("documento.empresa empresa")
				.leftOuterJoin("documento.documentoacao documentoacao")
				.leftOuterJoin("documento.documentoclasse documentoclasse")
				.leftOuterJoin("documento.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.leftOuterJoin("listaRateioitem.contagerencial contagerencial")
				.where("documentoclasse.cddocumentoclasse = ?", Documentoclasse.CD_PAGAR)
				.whereIn("documentoacao.cddocumentoacao", CollectionsUtil.listAndConcatenate(listaSituacao, "cddocumentoacao", ","))
				.openParentheses()
					.where("empresa = ?", filtro.getEmpresa())
					.or()
					.where("empresa is null")
				.closeParentheses()
				.wherePeriodo("documento.dtvencimento", filtro.getDtInicio(), filtro.getDtFim())
				.openParentheses()
					.where("contagerencial.usadocalculocustooperacionalproducao = true")
					.or()
					.where("contagerencial.usadocalculocustocomercialproducao = true")
					.closeParentheses()
				.unique();
		
		if (valor != null && valor != 0) {
			gastosEmpresa = new Money(valor.doubleValue() / 100);
		}
		
		return gastosEmpresa;
	}
}