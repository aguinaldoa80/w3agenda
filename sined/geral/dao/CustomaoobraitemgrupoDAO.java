package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Customaoobraitemgrupo;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.CustomaoobraitemgrupoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CustomaoobraitemgrupoDAO extends GenericDAO<Customaoobraitemgrupo> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Customaoobraitemgrupo> query, FiltroListagem _filtro) {
		CustomaoobraitemgrupoFiltro filtro = (CustomaoobraitemgrupoFiltro) _filtro;
		
		query
			.whereLikeIgnoreAll("customaoobraitemgrupo.descricao", filtro.getDescricao())
			.where("customaoobraitemgrupo.ativo = ?", filtro.getAtivo());
	}
	
}
