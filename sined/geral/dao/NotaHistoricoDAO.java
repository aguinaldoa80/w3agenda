package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotaHistoricoDAO extends GenericDAO<NotaHistorico>{
	
	/**
	 * Fornece o hist�rico completo de uma nota, ordenado de forma com que as �ltimas
	 * a��es apare�am primeiro.
	 * 
	 * @param nota
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<NotaHistorico> carregarListaHistorico(Nota nota) {
		if (nota == null || nota.getCdNota() == null) {
			throw new SinedException("A nota n�o pode ser nula");
		}
		
		return query()
			.joinFetch("notaHistorico.notaStatus notaStatus")
			.leftOuterJoinFetch("notaHistorico.motivoCancelamentoFaturamento motivoCancelamentoFaturamento")
			.where("notaHistorico.nota = ?", nota)
			.orderBy("notaHistorico.dtaltera DESC, notaHistorico.cdNotaHistorico DESC")
			.list();
	}
	
	/**
	 * 
	 * M�todo para carregar a Notahistorico
	 *
	 *@author Thiago Augusto
	 *@date 05/01/2012
	 * @param notaHistorico
	 * @return
	 */
	public List<NotaHistorico> carregarNotaHistorico(String whereIn){
		return query().select("notaHistorico.cdNotaHistorico, nota.cdNota")
		.leftOuterJoin("notaHistorico.nota nota")
		.whereIn("notaHistorico", whereIn)
		.list();
	}

	/**
	 * Busca os hist�ricos de cancelamento na receita para o resumo do faturamento
	 *
	 * @param whereInNota
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/07/2014
	 */
	public List<NotaHistorico> findForResumo(String whereInNota) {
		if(whereInNota == null || "".equals(whereInNota))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("nota.cdNota, notaStatus.cdNotaStatus, notaHistorico.observacao")
				.join("notaHistorico.nota nota")
				.join("notaHistorico.notaStatus notaStatus")
				.whereIn("nota.cdNota", whereInNota)
				.openParentheses()
				.where("notaStatus = ?", NotaStatus.NFSE_CANCELANDO).or()
				.where("notaStatus = ?", NotaStatus.CANCELADA)
				.closeParentheses()
				.orderBy("notaHistorico.dtaltera DESC")
				.list();
	}
	
	/**
	* M�todo que carrega o �ltimo his�rico da nota
	*
	* @param nota
	* @return
	* @since 22/03/2017
	* @author Luiz Fernando
	*/
	public NotaHistorico carregarUltimoHistorico(Nota nota, NotaStatus notaStatus) {
		if (nota == null || nota.getCdNota() == null) {
			throw new SinedException("A nota n�o pode ser nula");
		}
		
		return query()
					.where("notaHistorico.nota = ?", nota)
					.where("notaHistorico.notaStatus = ?", notaStatus)
					.orderBy("notaHistorico.dtaltera DESC, notaHistorico.cdNotaHistorico DESC")
					.setMaxResults(1)
					.unique();
	}

	/**
	* M�todo que faz update na observa��o do hist�rico da nota
	*
	* @param notaHistorico
	* @param observacao
	* @since 22/03/2017
	* @author Luiz Fernando
	*/
	public void updateObservacao(NotaHistorico notaHistorico, String observacao) {
		if(notaHistorico != null && notaHistorico.getCdNotaHistorico() != null && StringUtils.isNotBlank(observacao)){
			getHibernateTemplate().bulkUpdate("update NotaHistorico nh set observacao = ? where nh.cdNotaHistorico = ?", new Object[] {observacao, notaHistorico.getCdNotaHistorico()});
		}
	}
}
