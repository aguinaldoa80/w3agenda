package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.ContaContabilSaldoInicial;
import br.com.linkcom.sined.modulo.contabil.controller.process.filter.ContaContabilSaldoInicialFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContaContabilSaldoInicialDAO extends GenericDAO<ContaContabilSaldoInicial>{

	@Override
	public void updateListagemQuery(QueryBuilder<ContaContabilSaldoInicial> query, FiltroListagem _filtro) {
		ContaContabilSaldoInicialFiltro filtro = (ContaContabilSaldoInicialFiltro)_filtro;
		query.select("contaContabilSaldoInicial.cdContaContabilSaldoInicial, contaContabilSaldoInicial.codigoContaContabil, " +
					"contaContabilSaldoInicial.codigoAlternativoContaContabil, contaContabilSaldoInicial.descricao, contaContabilSaldoInicial.saldo, contaContabilSaldoInicial.dtSaldo, " +
					"contaContabilSaldoInicial.dtaltera, contaContabilSaldoInicial.cdusuarioaltera, " +
					"contaContabilSaldoInicial.identificador, " +
					"contaContabil.cdcontacontabil, contaContabil.nome, " +
					"arquivo.cdarquivo, arquivo.nome, arquivo.tipoconteudo, arquivo.tamanho, arquivo.dtmodificacao")
				.join("contaContabilSaldoInicial.arquivo arquivo")
				.join("contaContabilSaldoInicial.contaContabil contaContabil");
		query.where("contaContabil.empresa = ?", filtro.getEmpresa());
		query.where("cast(contaContabilSaldoInicial.dtaltera as date) >= ?", filtro.getDtInicio());
		query.where("cast(contaContabilSaldoInicial.dtaltera as date) <= ?", filtro.getDtFim());
		query.orderBy("contaContabilSaldoInicial.dtaltera desc");

		super.updateListagemQuery(query, _filtro);
	}
}
