package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.TermoSistema;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.TermoSistemaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TermoSistemaDAO extends GenericDAO<TermoSistema>{

	@Override
	public void updateListagemQuery(QueryBuilder<TermoSistema> query,FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inválidos");
		}
		TermoSistemaFiltro filtro = (TermoSistemaFiltro)_filtro;
		query
			.select("termoSistema.cdtermosistema, termoSistema.termo, termoSistema.novotermo, termoSistema.urlcadastro")
			.where("termoSistema.termo = ?", filtro.getTermo())
			.where("termoSistema.novotermo = ?", filtro.getNovotermo())
			.whereLikeIgnoreAll("termoSistema.urlcadastro",filtro.getUrlcadastro());
	}
}
