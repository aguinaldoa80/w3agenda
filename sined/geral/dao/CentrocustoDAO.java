package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CentrocustoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("centrocusto.nome")
public class CentrocustoDAO extends GenericDAO<Centrocusto> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Centrocusto> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		CentrocustoFiltro filtro = (CentrocustoFiltro) _filtro;
		query
			.select("centrocusto.cdcentrocusto, centrocusto.nome, centrocusto.ativo,centrocusto.participarCusteio")
			.whereLikeIgnoreAll("centrocusto.nome", filtro.getNome())
			.where("centrocusto.ativo = ?", filtro.getAtivo())
			.orderBy("centrocusto.nome");
	}
	
	public List<Centrocusto> findAtivosFluxoCaixa(String whereIn) {
		return query()
			.select("centrocusto.cdcentrocusto, centrocusto.nome")
			.whereIn("centrocusto.cdcentrocusto", whereIn)
			.where("coalesce(centrocusto.ativo, false) = true")
			.list();
	}

	public List<Centrocusto> findForError(String listaCentrocusto) {
		return query()
			.select("centrocusto.cdcentrocusto, centrocusto.nome")
			.whereIn("centrocusto.cdcentrocusto", listaCentrocusto)
			.list();
	}
	
	/**
	 * Renorna uma lista de centro de custo contendo apenas o campo Nome
	 * 
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Centrocusto> findDescricao(String whereIn) {
		return query()
			.select("centrocusto.nome")
			.whereIn("centrocusto.cdcentrocusto", whereIn)
			.list();
	}

	/* singleton */
	private static CentrocustoDAO instance;
	public static CentrocustoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(CentrocustoDAO.class);
		}
		return instance;
	}

	/**
	 * Carrega a lista de centro de custo para exibi��o no flex.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Centrocusto> findAllForFlex() {
		return query()
					.select("centrocusto.nome, centrocusto.cdcentrocusto")
					.orderBy("centrocusto.nome")
					.list();
	}

	/**
	 * Busca todos os registros para a exporta��o de arquivos gerenciais.
	 * @author Giovane Freitas <giovane.freitas@linkcom.com.br>
	 */
	public Iterator<Centrocusto> findForArquivoGerencial() {
		return query().iterate();
	}
	
	/**
	 * M�todo respons�vel por retorna tabela e campo que existe centro de custo relacionado
	 * @param id
	 * @return
	 * @since 04/02/2016
	 * @author C�sar
	 */
	@SuppressWarnings("unchecked")
	public List<GenericBean> ocorrenciaCentrocusto(Integer id){
		
		if(id == null){
			throw new SinedException("Par�metro incorreto para consulta.");
		}
		
		String query = 	"SELECT '�rea' as tabela,'Centro de custo' as campo FROM AREA AR WHERE AR.CDCONTAGERENCIAL = " + id + 
						" UNION " +						
						"SELECT 'Tipo de Documento' as tabela,'Centro de custo da taxa' as campo FROM DOCUMENTOTIPO DOC WHERE DOC.CDCENTROCUSTO ="+ id + 
						" UNION " +
						"SELECT 'Empresa' as tabela,'Centro de custo da venda' as campo FROM EMPRESA EMPCV WHERE EMPCV.CDCENTROCUSTOVENDA ="+ id +
						" UNION " +
						"SELECT 'Empresa' as tabela,'Centro de custo de Despesa de Viagem' as campo FROM EMPRESA EMPCDV WHERE EMPCDV.CDCENTROCUSTODESPESAVIAGEM ="+ id +
						" UNION " +
						"SELECT 'Empresa' as tabela,'Centro de custo  em Transfer�ncia Interna' as campo FROM EMPRESA EMPCC WHERE EMPCC.CDCENTROCUSTOTRANSFERENCIA ="+ id +
						" UNION " +
						"SELECT 'Empresa' as tabela,'Centro de custo do ve�culo uso' as campo FROM EMPRESA EMPRCV WHERE EMPRCV.CDCENTROCUSTOVEICULOUSO ="+ id +
						" UNION " +
						"SELECT 'Representa��o' as tabela,'Centro Custo Despesa' as campo FROM EMPRESAREPRESENTACAO EMPREP WHERE EMPREP.CDCENTROCUSTO ="+ id +
						" UNION " +
						"SELECT 'Material' as tabela,'Centro de custo venda' as campo FROM MATERIAL MAT WHERE MAT.CDCENTROCUSTOVENDA ="+ id +
						" UNION " +
						"SELECT 'Tipo de Taxa' as tabela,'Centro de Custo' as campo FROM TIPOTAXA TIP WHERE TIP.CDCENTROCUSTO ="+ id ;			
									
						//Mapeando resultados no Bean Gen�rico  / Consulta
						SinedUtil.markAsReader();
						List<GenericBean> listaResultados = getJdbcTemplate().query(query, new RowMapper() {
							public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
								return new GenericBean(rs.getString("tabela"), rs.getString("campo"));
							}
						});
						return listaResultados;
	}
	
	/**
	 * M�todo respons�vel por buscar saldo referente a data de refer�ncia
	 * @param centrocusto
	 * @param datereferencia
	 * @param empresas
	 * @return
	 * @since 28/11/2016
	 * @author C�sar
	 */
	public Money calculaSaldoAtualCentroCusto(Centrocusto centrocusto, Date datereferencia, Integer cdcontatipo, Integer cdconta, Empresa... empresas){
		if(datereferencia == null || centrocusto.getCdcentrocusto() == null){
			throw new SinedException("Par�metro incorreto para busca de saldo.");
		}
		String cdusuariologado = SinedUtil.getUsuarioLogado() != null ? SinedUtil.getUsuarioLogado().getCdpessoa().toString() :"NULL";
		String sql = "select saldoatual_centrocusto(cdcentrocusto, ? , "  + cdusuariologado + ", null, "+ cdcontatipo + ","+ cdconta + ")" +
					 "from centrocusto where cdcentrocusto = " + centrocusto.getCdcentrocusto() + " " +
					 (empresas != null && empresas.length > 0 ? "and cdempresa in (" + SinedUtil.listAndConcatenateIDs(Arrays.asList(empresas)) + ")" : "");
		return this.queryForMoney(sql, new Object[]{datereferencia});
	}
	
	/**
	 * Consulta SQL que resulta em um valor Money.
	 * � esperado que a consulta resulte em um valor long, o mesmo ser� transformado em Money, considerando duas casas decimais.
	 * 
	 * @param sql
	 * @param fields
	 * @return 
	 * @author Fl�vio Tavares
	 */
	private Money queryForMoney(String sql, Object[] fields){
		SinedUtil.markAsReader();
		Long result = getJdbcTemplate().queryForLong(sql, fields);
		return new Money(result, true);
	}
	
	public List<Centrocusto> findByListaCodigoalternativo(List<Integer> listaCodigoalternativo){
		QueryBuilder<Centrocusto> query = query();
		
		query.openParentheses();
		for (Integer codigoalternativo: listaCodigoalternativo){
			query.where("centrocusto.codigoalternativo=?", codigoalternativo);
			query.or();
		}
		query.closeParentheses();
		
		return query.list();
	}

	public List<Centrocusto> buscarParaCusteio() {
		return query()
				.select("centrocusto.cdcentrocusto,centrocusto.nome,centrocusto.codigoalternativo")
				.where("centrocusto.participarCusteio =?", Boolean.TRUE)
				.list();
	}

	public Centrocusto buscarParaProcessarCusteio(Integer id) {
		return query()
				.select("centrocusto.cdcentrocusto,centrocusto.nome,centrocusto.codigoalternativo")
				.where("centrocusto.participarCusteio =?", Boolean.TRUE)
				.where("centrocusto.cdcentrocusto = ?", id)
				.unique();
	}
}
