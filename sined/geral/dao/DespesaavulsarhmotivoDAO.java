package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Despesaavulsarhmotivo;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DespesaavulsarhmotivoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DespesaavulsarhmotivoDAO  extends GenericDAO<Despesaavulsarhmotivo>{

	@Override
	public void updateListagemQuery(QueryBuilder<Despesaavulsarhmotivo> query,
			FiltroListagem _filtro) {

		super.updateListagemQuery(query, _filtro);
		
		DespesaavulsarhmotivoFiltro filtro = (DespesaavulsarhmotivoFiltro) _filtro;
		query.whereLikeIgnoreAll("descricao", filtro.getDescricao());
	}
	
}
