package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.MotivoReprovacaoFaturamento;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MotivoReprovacaoFaturamentoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
public class MotivoReprovacaoFaturamentoDAO extends GenericDAO<MotivoReprovacaoFaturamento>{
	
	@Override
	public void updateListagemQuery(
			QueryBuilder<MotivoReprovacaoFaturamento> query,
			FiltroListagem _filtro) {
		MotivoReprovacaoFaturamentoFiltro filtro = (MotivoReprovacaoFaturamentoFiltro)_filtro;
		
		query.select("motivoReprovacaoFaturamento.cdmotivoreprovacaofaturamento, " +
				"motivoReprovacaoFaturamento.descricao,motivoReprovacaoFaturamento.ativo")
		.whereLikeIgnoreAll("motivoReprovacaoFaturamento.descricao", filtro.getDescricao())
		.where("motivoReprovacaoFaturamento.ativo = ?", filtro.getAtivo());
	}

	public List<MotivoReprovacaoFaturamento> findByFiltro(MotivoReprovacaoFaturamentoFiltro filtro) {
		
		return query()
				.whereLikeIgnoreAll("motivoReprovacaoFaturamento.descricao", filtro.getDescricao())
				.where("motivoReprovacaoFaturamento.ativo = ?", filtro.getAtivo())
				.list();
		
	}

	public List<MotivoReprovacaoFaturamento> findAllAtivos() {
		
		return query()
				.select("motivoReprovacaoFaturamento.cdmotivoreprovacaofaturamento, " +
				"motivoReprovacaoFaturamento.descricao,motivoReprovacaoFaturamento.ativo")
				.where("motivoReprovacaoFaturamento.ativo = true")
				.list();
	}

}
