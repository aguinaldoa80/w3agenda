package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Taxaparcela;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.TaxaparcelaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TaxaparcelaDAO extends GenericDAO<Taxaparcela> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Taxaparcela> query, FiltroListagem filtro) { 
		TaxaparcelaFiltro taxaparcelaFiltro = (TaxaparcelaFiltro) filtro;
		query.where("numeroparcelas = ?", taxaparcelaFiltro.getNumeroparcelas());
	}
	
	public boolean isTaxaParcelaCadastrada(Taxaparcela taxaparcela){
		if (taxaparcela == null || taxaparcela.getNumeroparcelas() == null) {
			return false;
		}
		
		List<Taxaparcela> listataxaparcela = query()
		.where("numeroparcelas = ?", taxaparcela.getNumeroparcelas())
		.where("taxaparcela.cdtaxaparcela <> ?", taxaparcela.getCdtaxaparcela())
		.list();
		
		return SinedUtil.isListNotEmpty(listataxaparcela);	
	}
	
	public Taxaparcela findTaxaParcelaByParcela(Integer numParcela){
		if (numParcela == null) {
			throw new SinedException("Numero de parcelas n�o pode ser nulo.");
		}
		
		List<Taxaparcela> listataxaparcela = query().where("numeroparcelas = ?", numParcela).list();
		
		if(SinedUtil.isListNotEmpty(listataxaparcela)){
			return listataxaparcela.get(0);
		} else {
			return null; 
		} 
	}
	
}
