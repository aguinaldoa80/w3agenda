package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.PneuSegmento;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PneuSegmentoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;




public class PneuSegmentoDAO extends GenericDAO<PneuSegmento> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<PneuSegmento> query,FiltroListagem _filtro) {
		PneuSegmentoFiltro filtro = (PneuSegmentoFiltro) _filtro;
		
		query
			.select("pneuSegmento.cdPneuSegmento , pneuSegmento.nome, pneuSegmento.ativo, pneuSegmento.otr, pneuSegmento.principal")
			.whereLikeIgnoreAll("pneuSegmento.nome", filtro.getNome())
			.where("pneuSegmento.ativo = ?", filtro.getAtivo())
			.where("pneuSegmento.otr = ?", filtro.getOtr())
			.orderBy("pneuSegmento.nome");
		
	}
	
	public Boolean validaPrincipalAtivo (PneuSegmento pneuSegmento){
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.from(PneuSegmento.class)
					.where("principal = true")
					.where("pneuSegmento != ?", pneuSegmento, Util.objects.isPersistent(pneuSegmento))
					.unique() > 0;
	}

	public List<PneuSegmento> findPneuSegmento(){
		return query()
				.select("pneuSegmento.cdPneuSegmento, pneuSegmento.nome")
				.where("pneuSegmento.ativo = true")
				.list();
		
	}
	
	public PneuSegmento findPneuSegmentoCd(String whereIn){
		return query()
				.select("pneuSegmento.cdPneuSegmento, pneuSegmento.nome, pneuSegmento.otr")
				.whereIn("pneuSegmento.cdPneuSegmento ",whereIn)
				.unique();
				
		
	}
/*	* M�todo autocomplete para buscar segmento de pneu
	*
	* @param q
	* @return
	* @since 19/04/2020
	* @author Mairon Cezar
	*/
	public List<PneuSegmento> findAutocomplete(String q) {
		return query()
			.select("pneuSegmento.cdPneuSegmento, pneuSegmento.nome, "+
					"pneuSegmento.tipoPneuOtr, pneuSegmento.marca, pneuSegmento.descricao, pneuSegmento.lonas, "+
					"pneuSegmento.modelo, pneuSegmento.medida, pneuSegmento.qualificacaoPneu, pneuSegmento.serieFogo, "+
					"pneuSegmento.dot, pneuSegmento.numeroReformas, pneuSegmento.bandaCamelbak, pneuSegmento.ativo, "+
					"pneuSegmento.principal, pneuSegmento.acompanhaRoda, pneuSegmento.otr")
			.whereLikeIgnoreAll("pneuSegmento.nome", q)
			.where("pneuSegmento.ativo = true")
			.autocomplete()
			.orderBy("pneuSegmento.nome")
			.list();
	}
	
	public PneuSegmento loadForHabilitarCampos(PneuSegmento pneuSegmento) {
		return querySined()
			
			.select("pneuSegmento.cdPneuSegmento, pneuSegmento.nome, "+
					"pneuSegmento.tipoPneuOtr, pneuSegmento.marca, pneuSegmento.descricao, pneuSegmento.lonas, "+
					"pneuSegmento.modelo, pneuSegmento.medida, pneuSegmento.qualificacaoPneu, pneuSegmento.serieFogo, "+
					"pneuSegmento.dot, pneuSegmento.numeroReformas, pneuSegmento.bandaCamelbak, pneuSegmento.ativo, pneuSegmento.acompanhaRoda, "+
					"pneuSegmento.principal, pneuSegmento.otr")
			.where("pneuSegmento = ?", pneuSegmento)
			.unique();
	}
	
	public PneuSegmento loadPrincipal() {
		return query()
			.select("pneuSegmento.cdPneuSegmento, pneuSegmento.nome, "+
					"pneuSegmento.tipoPneuOtr, pneuSegmento.marca, pneuSegmento.descricao, pneuSegmento.lonas, "+
					"pneuSegmento.modelo, pneuSegmento.medida, pneuSegmento.qualificacaoPneu, pneuSegmento.serieFogo, "+
					"pneuSegmento.dot, pneuSegmento.numeroReformas, pneuSegmento.bandaCamelbak, pneuSegmento.ativo, "+
					"pneuSegmento.principal, pneuSegmento.acompanhaRoda, pneuSegmento.otr")
			.where("pneuSegmento.principal = true")
			.where("pneuSegmento.ativo = true")
			.setMaxResults(1)
			.unique();
	}
}
