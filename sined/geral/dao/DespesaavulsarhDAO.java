package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Despesaavulsarh;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoDespesaAvulsaRH;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DespesaavulsarhFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DespesaavulsarhDAO extends GenericDAO<Despesaavulsarh> {

	@Override
	public void updateListagemQuery(QueryBuilder<Despesaavulsarh> query, FiltroListagem _filtro) {
		
		query.select("despesaavulsarh.cddespesaavulsarh, despesaavulsarh.dtinsercao, " +
				"despesaavulsarh.dtdeposito, despesaavulsarh.situacao, motivo.descricao, " +
				"fornecedor.cdpessoa, fornecedor.nome");
		query.join("despesaavulsarh.despesaavulsarhmotivo motivo");
		query.join("despesaavulsarh.fornecedor fornecedor");
		
		DespesaavulsarhFiltro filtro = (DespesaavulsarhFiltro) _filtro;
		query.where("despesaavulsarh.dtinsercao >= ?", filtro.getDtinsercaoinicial());
		query.where("despesaavulsarh.dtinsercao <= ?", SinedDateUtils.dataToEndOfDay(filtro.getDtinsercaofinal()));
		query.where("despesaavulsarh.dtdeposito >= ?", filtro.getDtdepositoinicial());
		query.where("despesaavulsarh.dtdeposito <= ?", SinedDateUtils.dataToEndOfDay(filtro.getDtdepositofinal()));
		
		List<SituacaoDespesaAvulsaRH> listaSituacao = filtro.getListaSituacao();
		if (listaSituacao != null && !listaSituacao.isEmpty()) {
			String list = "";
			for (int i = 0; i < listaSituacao.size(); i++) {
				Object o = listaSituacao.get(i);
				SituacaoDespesaAvulsaRH situacao = null;
				if (o instanceof String) {
					situacao = SituacaoDespesaAvulsaRH.valueOf((String)o);
				}else{
					situacao = (SituacaoDespesaAvulsaRH)o;
				}
				
				list += situacao.getValue() + ",";
			}
			query.whereIn("despesaavulsarh.situacao", list.substring(0, (list.length()-1) ));
		}
		
		query.where("motivo = ?", filtro.getDespesaavulsarhmotivo());
		query.where("fornecedor = ?", filtro.getFornecedor());
		
		super.updateListagemQuery(query, _filtro);
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Despesaavulsarh> query) {
		
		query.select("despesaavulsarh.cddespesaavulsarh, despesaavulsarh.dtcancelado, " +
				"despesaavulsarh.dtinsercao, despesaavulsarh.dtdeposito, despesaavulsarh.situacao, " +
				"despesaavulsarh.valor, despesaavulsarh.cdusuarioaltera, despesaavulsarh.dtaltera, " +
				"fornecedor.cdpessoa, fornecedor.identificador, fornecedor.nome, fornecedor.cnpj, fornecedor.cpf, " +
				"motivo.cddespesaavulsarhmotivo, motivo.descricao, " +
				"rateio.cddespesaavulsarhrateio, rateio.percentual, rateio.valor, rateio.cdusuarioaltera, " +
				"rateio.dtaltera, contagerencial.cdcontagerencial, projeto.cdprojeto, centrocusto.cdcentrocusto," +
				"historico.cddespesaavulsarhhistorico, historico.dtaltera, historico.observacao, " +
				"historico.acao, usuario.cdpessoa, usuario.nome");
	
		query.join("despesaavulsarh.fornecedor fornecedor");
		query.leftOuterJoin("despesaavulsarh.listaDespesaavulsarhrateio rateio");
		query.leftOuterJoin("despesaavulsarh.despesaavulsarhmotivo motivo");
		query.leftOuterJoin("rateio.contagerencial contagerencial");
		query.leftOuterJoin("rateio.projeto projeto");
		query.leftOuterJoin("rateio.centrocusto centrocusto");
		query.leftOuterJoin("despesaavulsarh.listaDespesaavulsarhhistorico historico");
		query.leftOuterJoin("historico.usuario usuario");
		query.orderBy("historico.dtaltera desc");
		super.updateEntradaQuery(query);
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		super.updateSaveOrUpdate(save);
		
		Despesaavulsarh despesa = (Despesaavulsarh) save.getEntity();
		if (despesa.getListaDespesaavulsarhrateio() != null && Hibernate.isInitialized(despesa.getListaDespesaavulsarhrateio())){
			save.saveOrUpdateManaged("listaDespesaavulsarhrateio");
		}
	}

	/**
	 * Busca todas as {@link Despesaavulsarh} a partir de uma lista de IDs.
	 * 
	 * @author <a mailto="giovane.freitas@linkcom.com.br">Giovane Freitas</a>
	 * @since Oct 22, 2011
	 * @param itensSelecionados Uma lista de IDs separados por v�rgula.
	 * @param carregarDependencias 
	 * @return Uma lista de {@link Despesaavulsarh}.
	 */
	public List<Despesaavulsarh> findByIds(String itensSelecionados, boolean carregarDependencias) {
		QueryBuilder<Despesaavulsarh> query = querySined()
			.whereIn("despesaavulsarh.cddespesaavulsarh", itensSelecionados);
		
		if (carregarDependencias){
			query.leftOuterJoinFetch("despesaavulsarh.despesaavulsarhmotivo");
			query.leftOuterJoinFetch("despesaavulsarh.fornecedor");
			query.leftOuterJoinFetch("despesaavulsarh.listaDespesaavulsarhrateio rateio");
			query.leftOuterJoinFetch("rateio.projeto");
			query.leftOuterJoinFetch("rateio.centrocusto");
			query.leftOuterJoinFetch("rateio.contagerencial");
		}
		
		return query.list();
	}
	
	public List<Despesaavulsarh> findForCSV(DespesaavulsarhFiltro filtro, boolean carregarDependencias) {
		QueryBuilder<Despesaavulsarh> query = querySined()
			.where("despesaavulsarh.dtinsercao >= ?", filtro.getDtinsercaoinicial())
			.where("despesaavulsarh.dtinsercao <= ?", SinedDateUtils.dataToEndOfDay(filtro.getDtinsercaofinal()))
			.where("despesaavulsarh.dtdeposito >= ?", filtro.getDtdepositoinicial())
			.where("despesaavulsarh.dtdeposito <= ?", SinedDateUtils.dataToEndOfDay(filtro.getDtdepositofinal()));
		
		if (carregarDependencias){
			query.leftOuterJoinFetch("despesaavulsarh.despesaavulsarhmotivo motivo");
			query.leftOuterJoinFetch("despesaavulsarh.fornecedor fornecedor");
			query.leftOuterJoinFetch("despesaavulsarh.listaDespesaavulsarhrateio rateio");
			query.leftOuterJoinFetch("rateio.projeto projeto");
			query.leftOuterJoinFetch("rateio.centrocusto centrocusto");
			query.leftOuterJoinFetch("rateio.contagerencial contagerencial");
		}
		
		List<SituacaoDespesaAvulsaRH> listaSituacao = filtro.getListaSituacao();
		if (listaSituacao != null && !listaSituacao.isEmpty()) {
			String list = "";
			for (int i = 0; i < listaSituacao.size(); i++) {
				Object o = listaSituacao.get(i);
				SituacaoDespesaAvulsaRH situacao = null;
				if (o instanceof String) {
					situacao = SituacaoDespesaAvulsaRH.valueOf((String)o);
				}else{
					situacao = (SituacaoDespesaAvulsaRH)o;
				}
				
				list += situacao.getValue() + ",";
			}
			query.whereIn("despesaavulsarh.situacao", list.substring(0, (list.length()-1) ));
		}
		
		query.where("motivo = ?", filtro.getDespesaavulsarhmotivo());
		query.where("fornecedor = ?", filtro.getFornecedor());

		return query.list();
	}

}
