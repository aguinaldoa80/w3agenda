package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.AgendamentoHistorico;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AgendamentoHistoricoDAO extends GenericDAO<AgendamentoHistorico>{
	
	/**
	 * Fornece o hist�rico de um agendamento ordenado de forma com que as �ltimas
	 * a��es apare�am primeiro.
	 * 
	 * @param agendamento
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<AgendamentoHistorico> carregarListaHistorico(Agendamento agendamento) {
		if (agendamento == null || agendamento.getCdagendamento() == null) {
			throw new SinedException("O agendamento n�o pode ser nulo.");
		}

		return query()
			.joinFetch("agendamentoHistorico.agendamentoAcao agendamentoAcao")
			.where("agendamentoHistorico.agendamento = ?", agendamento)
			.orderBy("agendamentoHistorico.cdAgendamentoHistorico DESC")
			.list();
	}
	
}
