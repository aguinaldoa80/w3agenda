package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Conselhoclasseprofissional;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ConselhoclasseprofissionalFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;


@DefaultOrderBy("conselhoclasseprofissional.descricao")
public class ConselhoclasseprofissionalDAO extends GenericDAO<Conselhoclasseprofissional>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Conselhoclasseprofissional> query, FiltroListagem _filtro) {
		ConselhoclasseprofissionalFiltro filtro = (ConselhoclasseprofissionalFiltro) _filtro;

		query
			.select("conselhoclasseprofissional.cdconselhoclasseprofissional, conselhoclasseprofissional.descricao," +
					"conselhoclasseprofissional.sigla, conselhoclasseprofissional.ativo")
			.whereLikeIgnoreAll("conselhoclasseprofissional.descricao", filtro.getDescricao())
			.whereLikeIgnoreAll("conselhoclasseprofissional.sigla", filtro.getSigla())
			.where("coalesce(conselhoclasseprofissional.ativo, false)=?", filtro.getAtivo())
			.orderBy("conselhoclasseprofissional.descricao");
	}	
}