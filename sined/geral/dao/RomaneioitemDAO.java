package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.Romaneioitem;
import br.com.linkcom.sined.geral.bean.Romaneiosituacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RomaneioitemDAO extends GenericDAO<Romaneioitem> {

	/**
	 * Retorna a quantidade total de materiais a partir da requisição de material
	 *
	 * @param requisicaomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/04/2014
	 */
	public Double getQtdeRomaneioByRequisicaomaterial(Requisicaomaterial requisicaomaterial) {
		return newQueryBuilderWithFrom(Double.class)
				.setUseTranslator(false)
				.select("sum(romaneioitem.qtde)")
				.join("romaneioitem.romaneio romaneio")
				.join("romaneio.listaRomaneioorigem romaneioorigem")
				.where("romaneioorigem.requisicaomaterial = ?", requisicaomaterial)
				.where("romaneio.romaneiosituacao <> ?", Romaneiosituacao.CANCELADA)
				.unique();
	}

}
