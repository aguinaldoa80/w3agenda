package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("frequencia.cdfrequencia")
public class FrequenciaDAO extends GenericDAO<Frequencia>{

	/**
	 * M�todo para obter uma lista de frequ�ncias sem o registro "�nica".
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Frequencia> findForContas(){
		return querySined()
			
			.select("frequencia.cdfrequencia,frequencia.nome")
			.where("frequencia.cdfrequencia <> ?",Frequencia.UNICA)
			.where("frequencia.todastelas = ?", Boolean.TRUE)
			.list();
	}
	
	/**
	 * Carrega a listagem de frequ�ncia para agendamento e contrato.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Frequencia> findForAgendamentoContrato(){
		return querySined()
					
					.select("frequencia.cdfrequencia,frequencia.nome")
					.where("frequencia.todastelas = ?", Boolean.TRUE)
					.list();
	}
}
