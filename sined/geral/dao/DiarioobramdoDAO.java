package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Diarioobramdo;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DiarioobramdoDAO extends GenericDAO<Diarioobramdo>{

	/**
	* Busca a lista de mao de obra pelo c�digo do diarioobra
	*
	* @param cddiarioobra
	* @return
	* @since Jul 6, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Diarioobramdo> findListDiarioobramdo(Integer cddiarioobra){
		if(cddiarioobra == null || cddiarioobra.equals("")){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("diarioobramdo.cddiarioobramdo, diarioobra.cddiarioobra, cargo.cdcargo, " +
							"diarioobramdo.quantidade, diarioobramdo.totalhora")
					.join("diarioobramdo.diarioobra diarioobra")
					.join("diarioobramdo.cargo cargo")
					.where("diarioobramdo.diarioobra.cddiarioobra = ?", cddiarioobra)
					.list();
	}
}
