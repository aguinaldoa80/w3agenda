package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.Sessao;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class SessaoDAO extends GenericDAO<Sessao> {

	public void insertSessao(Sessao sessao){
		getJdbcTemplate().update("INSERT INTO SESSAO (CDSESSAO, CDUSUARIO, DATAHORA, ENTRADA, SID, IP, USERAGENT, RESOLUCAO) VALUES (nextval('SQ_SESSAO'), ?, ?, ?, ?, ?, ?, ?)", 
				new Object[]{sessao.getUsuario().getCdpessoa(), sessao.getDatahora(), sessao.getEntrada(), sessao.getSid(), sessao.getIp(), sessao.getUseragent(), sessao.getResolucao()});
	}

	public Boolean getUsuarioJaAbriuPopupAviso() {
		return query()
				.where("sessao.datahora > cast(current_date as timestamp)")
				.where("sessao.popupAviso is not null")
				.where("sessao.usuario = ?", SinedUtil.getUsuarioLogado())
				.list().size() > 0;
	}

	public Sessao findByUsuario() {
		return query()
					.where("sessao.usuario = ?", SinedUtil.getUsuarioLogado())
					.orderBy("sessao.datahora desc")
					.setMaxResults(1)
					.unique();
	}
	
	public void updatePopupAviso(Sessao sessao) {
		getJdbcTemplate().update("update sessao set popupaviso = true where cdsessao = ?", new Object[]{sessao.getCdsessao()});
	}
}
