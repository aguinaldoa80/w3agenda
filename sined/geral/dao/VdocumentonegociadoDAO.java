package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.view.Vdocumentonegociado;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VdocumentonegociadoDAO extends GenericDAO<Vdocumentonegociado>{

	/**
	 * Busca a lista de Vdocumentonegociado a partir de v�rios documentos
	 *
	 * @param whereInDocumentos
	 * @return
	 * @since 15/06/2012
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<Vdocumentonegociado> findByDocumentos(String whereInDocumentos) {
		List<Documentoacao> lista = new ArrayList<Documentoacao>();
		lista.add(Documentoacao.CANCELADA);
		lista.add(Documentoacao.NEGOCIADA);
		
		SinedUtil.markAsReader();
		List<Vdocumentonegociado> listaUpdate = 
			getJdbcTemplate().query(
				"SELECT v.cddocumento, v.cddocumentonegociado, v.cddocumentoacaonegociado, v.dtemissaonegociado, v.dtvencimentonegociado " +
				"FROM vdocumentonegociado v " +
				"WHERE v.cddocumento in (" + whereInDocumentos + ") " +
				"AND v.cddocumentoacaonegociado not in (" + CollectionsUtil.listAndConcatenate(lista, "cddocumentoacao", ",") + ") ", 
			new RowMapper() {
				public Vdocumentonegociado mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Vdocumentonegociado(
							rs.getInt("cddocumento"), 
							rs.getInt("cddocumentonegociado"), 
							rs.getInt("cddocumentoacaonegociado"), 
							rs.getDate("dtvencimentonegociado"), 
							rs.getDate("dtemissaonegociado"));
				}
	
			});
		
		return listaUpdate;
	}
	
	@SuppressWarnings("unchecked")
	public List<Vdocumentonegociado> findByDocumentosnegociados(String whereInDocumentosnegociados) {
		SinedUtil.markAsReader();
		List<Vdocumentonegociado> listaUpdate = 
				getJdbcTemplate().query(
						"SELECT v.cddocumento, v.cddocumentonegociado, v.cddocumentoacaonegociado, v.dtemissaonegociado, v.dtvencimentonegociado " +
						"  FROM documentonegociadoitem dni " +
						"  JOIN documento d on d.cddocumentonegociado = dni.cddocumentonegociado " +
						"  JOIN vdocumentonegociado v on v.cddocumento = d.cddocumento " +
						" WHERE dni.cddocumento in (" + whereInDocumentosnegociados + ") " +
						"   AND dni.cddocumento = v.cddocumentonegociado ",
						new RowMapper() {
							public Vdocumentonegociado mapRow(ResultSet rs, int rowNum) throws SQLException {
								return new Vdocumentonegociado(
										rs.getInt("cddocumento"), 
										rs.getInt("cddocumentonegociado"), 
										rs.getInt("cddocumentoacaonegociado"), 
										rs.getDate("dtvencimentonegociado"), 
										rs.getDate("dtemissaonegociado"));
							}							
						});
		
		return listaUpdate;
	}
	
	@SuppressWarnings("unchecked")
	public List<Vdocumentonegociado> findByDocumentosnegociadosRaiz(String whereInParcelas){
		if (whereInParcelas!=null && !whereInParcelas.trim().equals("")){
			SinedUtil.markAsReader();
			List<Vdocumentonegociado> listaDocsNegociados = 
					getJdbcTemplate().query(
							"SELECT v.cddocumento, v.cddocumentonegociado, v.cddocumentoacaonegociado, v.dtemissaonegociado, v.dtvencimentonegociado " +
									"FROM vdocumentonegociado v " +
									"WHERE v.cddocumentonegociado in (" + whereInParcelas + ") " +
									"and not exists(select 1 " +
									"		from vdocumentonegociado " +
									"       where vdocumentonegociado.cddocumentonegociado = v.cddocumento)",
									new RowMapper() {
								public Vdocumentonegociado mapRow(ResultSet rs, int rowNum) throws SQLException {
									return new Vdocumentonegociado(
											rs.getInt("cddocumento"), 
											rs.getInt("cddocumentonegociado"), 
											rs.getInt("cddocumentoacaonegociado"), 
											rs.getDate("dtvencimentonegociado"), 
											rs.getDate("dtemissaonegociado"));
								}							
							});
			
			return listaDocsNegociados;
		}else {
			return new ArrayList<Vdocumentonegociado>();
		}
	}
}
