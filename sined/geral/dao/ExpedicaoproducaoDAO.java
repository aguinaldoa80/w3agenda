package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Expedicaoproducao;
import br.com.linkcom.sined.geral.bean.Regiao;
import br.com.linkcom.sined.geral.bean.Regiaolocal;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacao;
import br.com.linkcom.sined.modulo.producao.controller.crud.filter.ExpedicaoproducaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ExpedicaoproducaoDAO extends GenericDAO<Expedicaoproducao> {
	
	
	@Override
	public void updateListagemQuery(QueryBuilder<Expedicaoproducao> query,
			FiltroListagem _filtro) {
		ExpedicaoproducaoFiltro filtro = (ExpedicaoproducaoFiltro) _filtro;
		
		query
			.select("distinct expedicaoproducao.cdexpedicaoproducao, expedicaoproducao.identificadorcarregamento, " +
					"expedicaoproducao.expedicaosituacao, transportadora.nome, expedicaoproducao.dtexpedicao, " +
					"motorista.nome, veiculo.placa, bempatrimonio.nome")
			.leftOuterJoin("expedicaoproducao.transportadora transportadora")
			.leftOuterJoin("expedicaoproducao.motorista motorista")
			.leftOuterJoin("expedicaoproducao.veiculo veiculo")
			.leftOuterJoin("veiculo.patrimonioitem patrimonioitem")
			.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
			.leftOuterJoin("expedicaoproducao.listaExpedicaoproducaoitem expedicaoproducaoitem")
			.leftOuterJoin("expedicaoproducaoitem.cliente cliente")
			.leftOuterJoin("expedicaoproducaoitem.contrato contrato")
			.leftOuterJoin("expedicaoproducaoitem.enderecocliente enderecocliente")
			.leftOuterJoin("enderecocliente.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			
			.where("expedicaoproducao.cdexpedicaoproducao = ?", filtro.getCdexpedicaoproducao())
			.where("expedicaoproducao.dtexpedicao >= ?", filtro.getDtexpedicao1())
			.where("expedicaoproducao.dtexpedicao <= ?", filtro.getDtexpedicao2())
			.where("expedicaoproducao.identificadorcarregamento = ?", filtro.getIdentificadorcarregamento())
			.where("transportadora = ?", filtro.getTransportadora())
			.where("cliente = ?", filtro.getCliente())
			.where("contrato = ?", filtro.getContrato())
			.where("veiculo = ?", filtro.getVeiculo())
			.where("motorista = ?", filtro.getMotorista())
			.where("uf = ?", filtro.getUf())
			.where("municipio = ?", filtro.getMunicipio())
			.orderBy("expedicaoproducao.cdexpedicaoproducao desc")
			.ignoreJoin("expedicaoproducaoitem","cliente","enderecocliente","municipio","uf","contrato")
			;
		
		if(filtro.getSelectedItensEtiqueta() != null && !filtro.getSelectedItensEtiqueta().equals("")){
			query.whereIn("expedicaoproducao.cdexpedicaoproducao", filtro.getSelectedItensEtiqueta());
		}
		
		if(filtro.getRegiao() != null){
			Regiao regiao = filtro.getRegiao();
			if(regiao.getListaRegiaolocal() != null && regiao.getListaRegiaolocal().size() > 0){
				query.openParentheses();
				for (Regiaolocal regiaolocal : regiao.getListaRegiaolocal()) {
					query
						.openParentheses()
						.where("enderecocliente.cep >= ?", regiaolocal.getCepinicio())
						.where("enderecocliente.cep <= ?", regiaolocal.getCepfinal())
						.closeParentheses()
						.or();
				}
				query.closeParentheses();
			}
		}
		
		if(filtro.getListaExpedicaosituacao() != null) {
			query.whereIn("expedicaoproducao.expedicaosituacao", Expedicaosituacao.listAndConcatenate(filtro.getListaExpedicaosituacao()));
		}
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Expedicaoproducao> query) {
		query
			.select("expedicaoproducao.cdexpedicaoproducao, expedicaoproducao.identificadorcarregamento, expedicaoproducao.dtexpedicao, " +
					"expedicaoproducao.cdusuarioaltera, expedicaoproducao.dtaltera, expedicaoproducao.expedicaosituacao, " +
					"transportadora.cdpessoa, transportadora.nome, expedicaoproducaoitem.cdexpedicaoproducaoitem, expedicaoproducaoitem.etiqueta, " +
					"expedicaoproducaoitem.dtexpedicaoproducaoitem, expedicaoproducaoitem.qtdeexpedicaoproducao, expedicaoproducaoitem.qtdeproduzida, " +
					"cliente.cdpessoa, cliente.nome, cliente.cnpj, cliente.cpf, material.cdmaterial, material.identificacao, material.nome, " +
					"contrato.cdcontrato, contrato.descricao, enderecocliente.cdendereco, contatocliente.cdpessoa, " +
					"expedicaoproducaohistorico.cdexpedicaoproducaohistorico, expedicaoproducaohistorico.dtaltera, " +
					"expedicaoproducaohistorico.cdusuarioaltera, expedicaoproducaohistorico.expedicaoacao, " +
					"expedicaoproducaohistorico.observacao, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
					"motorista.cdpessoa, motorista.nome, veiculo.cdveiculo")
			.leftOuterJoin("expedicaoproducao.motorista motorista")
			.leftOuterJoin("expedicaoproducao.veiculo veiculo")
			.leftOuterJoin("expedicaoproducao.listaExpedicaoproducaohistorico expedicaoproducaohistorico")
			.leftOuterJoin("expedicaoproducao.listaExpedicaoproducaoitem expedicaoproducaoitem")
			.leftOuterJoin("expedicaoproducaoitem.enderecocliente enderecocliente")
			.leftOuterJoin("expedicaoproducaoitem.contatocliente contatocliente")
			.leftOuterJoin("expedicaoproducaoitem.material material")
			.leftOuterJoin("expedicaoproducaoitem.cliente cliente")
			.leftOuterJoin("expedicaoproducaoitem.contrato contrato")
			.leftOuterJoin("expedicaoproducaoitem.localarmazenagem localarmazenagem")
			.leftOuterJoin("expedicaoproducao.transportadora transportadora")
			.orderBy("expedicaoproducaoitem.cdexpedicaoproducaoitem, expedicaoproducaohistorico.dtaltera desc")
			;
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaExpedicaoproducaoitem");
	}

	/**
	 * M�todo para atualiza��o da situa��o da(s) expedi��o(��es) passadas por par�metro.
	 *
	 * @param whereIn
	 * @param situacao
	 * @since 16/07/2012
	 * @author Rodrigo Freitas
	 */
	public void updateSituacao(String whereIn, Expedicaosituacao situacao) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		getHibernateTemplate().bulkUpdate("update Expedicaoproducao e set e.expedicaosituacao = ? where e.cdexpedicaoproducao in (" + whereIn + ")", situacao);
	}

	/**
	 * Busca registro de Expedicao para a confirma��o.
	 *
	 * @param whereIn
	 * @return
	 * @since 16/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Expedicaoproducao> findForConfirmacao(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		return query()
					.select("expedicaoproducao.cdexpedicaoproducao, " +
							"expedicaoproducaoitem.cdexpedicaoproducaoitem, expedicaoproducaoitem.qtdeexpedicaoproducao, " +
							"expedicaoproducaoitem.dtexpedicaoproducaoitem, " +
							"material.cdmaterial, material.producao, material.produto, material.epi, " +
							"materialproducao.cdmaterial, materialproducao.producao, materialproducao.produto, materialproducao.epi, " +
							"listaProducao.consumo, " +
							"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome")
					.leftOuterJoin("expedicaoproducao.listaExpedicaoproducaoitem expedicaoproducaoitem")
					.leftOuterJoin("expedicaoproducaoitem.localarmazenagem localarmazenagem")
					.leftOuterJoin("expedicaoproducaoitem.material material")
					.leftOuterJoin("material.listaProducao listaProducao")
					.leftOuterJoin("listaProducao.material materialproducao")
					.whereIn("expedicaoproducao.cdexpedicaoproducao", whereIn)
					.list();
	}

	/**
	 * Verifica se existe alguma expedi��o com situa��es diferentes das passadas por par�metro.
	 *
	 * @param whereIn
	 * @param situacoes
	 * @return
	 * @since 17/07/2012
	 * @author Rodrigo Freitas
	 */
	public boolean haveExpedicaoNotInSituacao(String whereIn, Expedicaosituacao[] situacoes) {
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Expedicaoproducao.class)
					.setUseTranslator(false)
					.whereIn("expedicaoproducao.cdexpedicaoproducao", whereIn);
		
		for (int i = 0; i < situacoes.length; i++) {
			query.where("expedicaoproducao.expedicaosituacao <> ?", situacoes[i]);
		}		
		
		return query.unique() > 0;
	}

	

	/**
	 * Busca os registros para o Gerar PDF da listagem de Expedi��o.
	 *
	 * @param filtro
	 * @return
	 * @since 23/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Expedicaoproducao> findForGerarPDF(ExpedicaoproducaoFiltro filtro) {
		QueryBuilder<Expedicaoproducao> query = querySined();
		this.updateListagemQuery(query, filtro);
		return query.list();
	}
	
	/**
	 * M�todo que carrega os dados necess�rios para gerar nota fiscal de produto a partir das expedi��es selecionadas.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Expedicaoproducao> findForGerarNotaFiscalProduto(String whereIn){
		return query()
				.select("expedicaoproducao.cdexpedicaoproducao, expedicaoproducao.dtexpedicao, expedicaoproducaoitem.cdexpedicaoproducaoitem, " +
						"expedicaoproducaoitem.qtdeexpedicaoproducao, expedicaoproducaoitem.dtexpedicaoproducaoitem, material.cdmaterial, " +
						"material.valorvenda, material.valorcusto, material.valorfrete, material.nomenf, material.identificacao, material.nome, " +
						"unidademedida.cdunidademedida, unidademedida.simbolo, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
						"cliente.cdpessoa, cliente.nome, enderecocliente.cdendereco, enderecocliente.logradouro, enderecocliente.numero, " +
						"enderecocliente.complemento, enderecocliente.caixapostal, enderecocliente.bairro, enderecocliente.cep, uf.cduf, uf.sigla, " +
						"municipio.cdmunicipio, municipio.nome")
				.join("expedicaoproducao.listaExpedicaoproducaoitem expedicaoproducaoitem")
				.join("expedicaoproducaoitem.material material")
				.join("material.unidademedida unidademedida")
				.leftOuterJoin("expedicaoproducaoitem.localarmazenagem localarmazenagem")
				.leftOuterJoin("expedicaoproducaoitem.cliente cliente")
				.leftOuterJoin("expedicaoproducaoitem.enderecocliente enderecocliente")
				.leftOuterJoin("enderecocliente.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.whereIn("expedicaoproducao.cdexpedicaoproducao", whereIn)
				.list();
	}
}
