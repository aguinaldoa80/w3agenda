package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Movpatrimonioorigem;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MovpatrimonioorigemDAO extends GenericDAO<Movpatrimonioorigem> {

	/**
	 * M�todo que verifica se j� existe alguma movimenta��o com a entrega de origem,
	 * pois se houver � porque esta sendo duplicado o registro.
	 * 
	 * @param entrega
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeMovPatrimonioComOrigemEntrega(Entrega entrega) {
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Movpatrimonioorigem.class)
			.join("movpatrimonioorigem.entrega entrega")
			.setUseTranslator(false)
			.where("entrega = ?",entrega)
			.unique() > 0;
	}

}
