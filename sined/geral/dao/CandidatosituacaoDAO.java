package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Candidatosituacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("candidatosituacao.cdcandidatosituacao")
public class CandidatosituacaoDAO extends GenericDAO<Candidatosituacao>{
	
	
	@Override
	public List<Candidatosituacao> findForCombo(String... extraFields) {
		return 
			query()
			.select("candidatosituacao.cdcandidatosituacao, candidatosituacao.descricao")
			.orderBy("candidatosituacao.descricao")
			.list();
	}
	
	/* singleton */
	private static CandidatosituacaoDAO instance;
	public static CandidatosituacaoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(CandidatosituacaoDAO.class);
		}
		return instance;
	}

}
