package br.com.linkcom.sined.geral.dao;


import java.util.List;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.ColaboradorOcorrencia;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColaboradorOcorrenciaDAO extends GenericDAO<ColaboradorOcorrencia> {

	/**
	* M�todo que carrega a lista de ocorr�ncia do colaborador
	*
	* @param colaborador
	* @return
	* @since 22/10/2014
	* @author Luiz Fernando
	*/
	public List<ColaboradorOcorrencia> findByColaborador(Colaborador colaborador) {
		if(colaborador == null || colaborador.getCdpessoa() == null)
			throw new SinedException("Colaborador n�o pode ser nulo.");
		
		return query()
				.select("colaboradorOcorrencia.cdcolaboradorocorrencia, colaboradorOcorrencia.descricao, " +
						"colaboradorOcorrencia.dtocorrencia, " +
						"colaboradorOcorrenciaTipo.cdcolaboradorocorrenciatipo, colaboradorOcorrenciaTipo.nome, " +
						"responsavel.cdpessoa, responsavel.nome ")
				.join("colaboradorOcorrencia.colaborador colaborador")
				.leftOuterJoin("colaboradorOcorrencia.colaboradorOcorrenciaTipo colaboradorOcorrenciaTipo")
				.leftOuterJoin("colaboradorOcorrencia.responsavel responsavel")
				.where("colaborador = ?", colaborador)
				.list();
	}
}
