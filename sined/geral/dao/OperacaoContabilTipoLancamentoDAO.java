package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.OperacaoContabilTipoLancamento;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.OperacaoContabilTipoLancamentoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OperacaoContabilTipoLancamentoDAO extends GenericDAO<OperacaoContabilTipoLancamento> {
	
	public List<OperacaoContabilTipoLancamento> buscarOrdenados(String whereIn) {
		return query()
				.select("operacaoContabilTipoLancamento.cdOperacaoContabilTipoLancamento, operacaoContabilTipoLancamento.nome, operacaoContabilTipoLancamento.movimentacaocontabilTipoLancamento ")
				.whereIn("operacaoContabilTipoLancamento.cdOperacaoContabilTipoLancamento", whereIn)
				.orderBy("operacaoContabilTipoLancamento.ordem")
				.list();
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<OperacaoContabilTipoLancamento> query, FiltroListagem _filtro) {
		OperacaoContabilTipoLancamentoFiltro filtro = (OperacaoContabilTipoLancamentoFiltro) _filtro;
		
		query
			.select("operacaoContabilTipoLancamento.cdOperacaoContabilTipoLancamento, operacaoContabilTipoLancamento.nome, operacaoContabilTipoLancamento.ordem, operacaoContabilTipoLancamento.movimentacaocontabilTipoLancamento")
			.whereLikeIgnoreAll("operacaoContabilTipoLancamento.nome", filtro.getNome())
			.orderBy("operacaoContabilTipoLancamento.ordem");
	}
} 
