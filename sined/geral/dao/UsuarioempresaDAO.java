package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Usuarioempresa;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("usuarioempresa.empresa.nomefantasia")
public class UsuarioempresaDAO extends GenericDAO<Usuarioempresa> {


	/* singleton */
	private static UsuarioempresaDAO instance;
	public static UsuarioempresaDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(UsuarioempresaDAO.class);
		}
		return instance;
	}
	public List<Usuarioempresa> findByEmpresa(String itens) {
		return querySined()
					.setUseWhereEmpresa(false)
					.select("usuarioempresa.cdusuarioempresa, empresa.cdpessoa, usuario.cdpessoa")
					.join("usuarioempresa.empresa empresa")
					.join("usuarioempresa.usuario usuario")
					.whereIn("empresa.cdpessoa", itens)
					.list();
	}
	
	public List<Usuarioempresa> findForAndroid(String whereInUsuarioempresa, String whereInUsuario){
		return query()
			.select("usuarioempresa.cdusuarioempresa, empresa.cdpessoa, empresa.nome, empresa.razaosocial, usuario.cdpessoa")
			.join("usuarioempresa.empresa empresa")
			.join("usuarioempresa.usuario usuario")
			.whereIn("usuarioempresa.cdusuarioempresa", whereInUsuarioempresa)
			.whereIn("usuario.cdpessoa", whereInUsuario)
			.list();
	}
	

	/**
	 * Verifica se existe algum registro
	 *
	 * @param usuario
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 16/12/2016
	 */
	public boolean haveRegistro(Usuario usuario, Empresa empresa) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Usuarioempresa.class)
					.where("usuarioempresa.empresa = ?", empresa)
					.where("usuarioempresa.usuario = ?", usuario)
					.unique() > 0 ;
	}
}