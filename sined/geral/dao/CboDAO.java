package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Cbo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("Cbo.nome")
public class CboDAO extends GenericDAO<Cbo> {

	@Override
	public List<Cbo> findAll() {
		QueryBuilder<Cbo> list= query()
		.select("cbo.cdcbo, cbo.nome")
		.from(Cbo.class)
		.orderBy("cbo.nome, cbo.cdcbo");
		return list.list();
	}
	
	/**
	 * Insere o CBO
	 *
	 * @param cbo
	 * @since 02/05/2012
	 * @author Rodrigo Freitas
	 */
	public void insertCBO(Cbo cbo){
		getJdbcTemplate().execute("INSERT INTO CBO (CDCBO, NOME) VALUES (" + cbo.getCdcbo() + ",'" + cbo.getNome() + "')");
	}
	
}
