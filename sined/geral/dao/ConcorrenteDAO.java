package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Concorrente;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ConcorrenteFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("concorrente.nome")
public class ConcorrenteDAO extends GenericDAO<Concorrente>{

	@Override
	public void updateListagemQuery(QueryBuilder<Concorrente> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		ConcorrenteFiltro filtro = (ConcorrenteFiltro) _filtro;
		query
			.select("concorrente.cdconcorrente, concorrente.nome, concorrente.telefone, concorrente.site, " +
					"concorrente.email, concorrente.produtos")
			.whereLikeIgnoreAll("concorrente.nome", filtro.getNome())
			.orderBy("concorrente.nome");
	}
	/**
	 * 
	 * M�todo que carrega o bean de concorrente.
	 *
	 * @name carregaConcorrente
	 * @param concorrente
	 * @return
	 * @return Concorrente
	 * @author Thiago Augusto
	 * @date 18/06/2012
	 *
	 */
	public Concorrente carregaConcorrente(Concorrente concorrente){
		return query()
				.where("concorrente = ? ", concorrente)
				.unique();
	}

	public Concorrente findByName(String campanhaStr) {
		return query()
				.where("trim(upper(concorrente.nome)) = ?", campanhaStr.toUpperCase().trim())
				.unique();
	}
	
	@Override
	public ListagemResult<Concorrente> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Concorrente> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("concorrente.cdconcorrente");
		
		return new ListagemResult<Concorrente>(query, filtro);
	}
}
