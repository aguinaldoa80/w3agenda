package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Riscotrabalhotipo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RiscotrabalhotipoDAO extends GenericDAO<Riscotrabalhotipo> {

	/* singleton */
	private static RiscotrabalhotipoDAO instance;
	public static RiscotrabalhotipoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(RiscotrabalhotipoDAO.class);
		}
		return instance;
	}
}