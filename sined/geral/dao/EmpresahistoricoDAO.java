package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresahistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EmpresahistoricoDAO extends GenericDAO<Empresahistorico> {

	/**
	 * Busca todos os registros de histórico de uma determinada {@link Empresa}
	 * 
	 * @author Giovane Freitas
	 * @param empresa
	 * @return
	 */
	public List<Empresahistorico> findByEmpresa(Empresa empresa) {

		return query()
			.where("empresahistorico.empresa = ?", empresa)
			.list();
	}

	/**
	 * Carrega um histórico para visualização.
	 * 
	 * @author Giovane Freitas
	 * @param empresahistorico
	 * @return
	 */
	public Empresahistorico loadForVisualizacao(Empresahistorico empresahistorico) {

		return query()
			.leftOuterJoinFetch("empresahistorico.municipiosped municipiosped")
			.leftOuterJoinFetch("municipiosped.uf uf")
			.leftOuterJoinFetch("empresahistorico.naturezaoperacao naturezaoperacao")
			.leftOuterJoinFetch("empresahistorico.regimetributacao regimetributacao")
			.leftOuterJoinFetch("empresahistorico.codigotributacao codigotributacao")
			.leftOuterJoinFetch("empresahistorico.itemlistaservico itemlistaservico")
			.leftOuterJoinFetch("empresahistorico.escritoriocontabilista escritoriocontabilista")
			.leftOuterJoinFetch("empresahistorico.colaboradorcontabilista colaboradorcontabilista")
			.entity(empresahistorico)
			.unique();
	}

	

}
