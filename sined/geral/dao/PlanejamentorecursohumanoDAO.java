package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Planejamentorecursohumano;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PlanejamentorecursohumanoDAO extends GenericDAO<Planejamentorecursohumano>{

	/**
	 * Carrega a lista de recursos humanos de planejamento.
	 *
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Planejamentorecursohumano> findByPlanejamento(Planejamento planejamento) {
		if (planejamento == null || planejamento.getCdplanejamento() == null) {
			throw new SinedException("Planejamento n�o pode ser nulo.");
		}
		return query()
					.select("planejamentorecursohumano.qtde, cargo.cdcargo, cargo.custohora")
					.join("planejamentorecursohumano.cargo cargo")
					.join("planejamentorecursohumano.planejamento planejamento")
					.where("planejamento = ?", planejamento)
					.list();
	}

	/**
	 * Carrega a lista de recursos humanos de planejamento para o relat�rio de or�amento.
	 *
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Planejamentorecursohumano> findForOrcamento(Planejamento planejamento) {
		if (planejamento == null || planejamento.getCdplanejamento() == null) {
			throw new SinedException("Planejamento n�o pode ser nulo.");
		}
		return query()
					.select("vcontagerencial.identificador, contagerencial.nome, tipooperacao.nome, " +
							"cargo.custohora, cargo.cdcargo, cargo.nome, planejamentorecursohumano.qtde")
					.join("planejamentorecursohumano.cargo cargo")
					.join("planejamentorecursohumano.planejamento planejamento")
					.leftOuterJoin("cargo.contagerencial contagerencial")
					.leftOuterJoin("contagerencial.tipooperacao tipooperacao")
					.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
					.where("planejamento = ?",planejamento)
					.list();
	}

}
