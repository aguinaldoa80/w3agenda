package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PedidovendatipoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("pedidovendatipo.descricao")
public class PedidovendatipoDAO extends GenericDAO<Pedidovendatipo> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Pedidovendatipo> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		PedidovendatipoFiltro filtro = (PedidovendatipoFiltro) _filtro;
		query
			.select("pedidovendatipo.cdpedidovendatipo, pedidovendatipo.descricao, pedidovendatipo.sigla, pedidovendatipo.bonificacao," +
					"naturezaoperacao.nome")
			.leftOuterJoin("pedidovendatipo.naturezaoperacao naturezaoperacao")
			.whereLikeIgnoreAll("pedidovendatipo.descricao", filtro.getDescricao())
			.where("naturezaoperacao=?", filtro.getNaturezaoperacao());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Pedidovendatipo> query) {
		query.leftOuterJoinFetch("pedidovendatipo.naturezaoperacao naturezaoperacao");
		query.leftOuterJoinFetch("pedidovendatipo.naturezaoperacaoexpedicao naturezaoperacaoexpedicao");
		query.leftOuterJoinFetch("pedidovendatipo.listaCampoextrapedidovenda campoextrapedidovenda");
		query.leftOuterJoinFetch("pedidovendatipo.cfopindustrializacao cfopindustrializacao");
		query.leftOuterJoinFetch("pedidovendatipo.cfopretorno cfopretorno");
		query.leftOuterJoinFetch("pedidovendatipo.naturezaoperacaosaidafiscal naturezaoperacaosaidafiscal");
		query.leftOuterJoinFetch("pedidovendatipo.naturezaoperacaoservico naturezaoperacaoservico");
		query.leftOuterJoinFetch("pedidovendatipo.projetoprincipal projetoprincipal");
		query.leftOuterJoinFetch("pedidovendatipo.naturezaoperacaonotafiscalentrada naturezaoperacaonotafiscalentrada");
		query.leftOuterJoinFetch("pedidovendatipo.documentoacao documentoacao");
		super.updateEntradaQuery(query);
	}
	
	/**
     * Busca os dados para o cache da tela de pedido de venda offline
     * @author Igor Silv�rio Costa
	 * @date 20/04/2012
	 * 
	 */
	public List<Pedidovendatipo> findForPVOffline() {
		return query()
		.select("pedidovendatipo.cdpedidovendatipo, pedidovendatipo.descricao, pedidovendatipo.bonificacao, pedidovendatipo.obrigarPrazoEntrega")
		.list();
	}
	
	public List<Pedidovendatipo> findForAndroid(String whereIn) {
		return query()
		.select("pedidovendatipo.cdpedidovendatipo, pedidovendatipo.descricao, pedidovendatipo.bonificacao, pedidovendatipo.comodato, pedidovendatipo.obrigarPrazoEntrega," +
				"pedidovendatipo.obrigarProjeto, projetoprincipal.cdprojeto")
		.leftOuterJoin("pedidovendatipo.projetoprincipal projetoprincipal")
		.whereIn("pedidovendatipo.cdpedidovendatipo", whereIn)
		.list();
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaCampoextrapedidovenda");
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendatipoDAO#findReserva()
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovendatipo> findReserva() {
		return querySined()
			.select("pedidovendatipo.cdpedidovendatipo, pedidovendatipo.descricao, pedidovendatipo.reserva, " +
					"pedidovendatipo.dtaltera")
			.where("pedidovendatipo.reserva = true")
			.list();
	}

	/**
	 * M�todo para carregamento do PedidoVendaTipo espec�fico para OrdemServicoVeterinaria
	 *
	 * @author Marcos Lisboa
	 * @return Pedidovendatipo
	 */
	public Pedidovendatipo carregarTipoOSVeterinaria(){
		return querySined()
		.select("pedidovendatipo.cdpedidovendatipo, pedidovendatipo.descricao, pedidovendatipo.reserva")
		.where("retira_acento(upper(pedidovendatipo.descricao)) = 'ORDEM DE SERVI�O VETERINARIA'")
		.unique();
	}
	
	/**
	* M�todo que verifica se existe tipo de pedido de venda marcado como principal 
	*
	* @param pedidovendatipo
	* @return
	* @since 20/10/2016
	* @author Luiz Fernando
	*/
	public Boolean existePedidovendatipoPrincipal(Pedidovendatipo pedidovendatipo){
		SinedUtil.markAsReader();
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Pedidovendatipo.class)			
			.where("pedidovendatipo <> ?", pedidovendatipo, pedidovendatipo != null && pedidovendatipo.getCdpedidovendatipo() != null)
			.where("pedidovendatipo.principal = true ")
			.unique() > 0;
			
	}

	/**
	* M�todo que carrega o tipo de pedido marcado como principal
	*
	* @return
	* @since 20/10/2016
	* @author Luiz Fernando
	*/
	public Pedidovendatipo loadPrincipal() {
		List<Pedidovendatipo> lista = querySined()
				.where("pedidovendatipo.principal = true")
				.list();
		
		return lista != null && lista.size() == 1 ? lista.get(0) : null;
	}

	/**
	* M�todo que carrega o tipo de pedido de venda com a situa��o da conta a receber
	*
	* @param pedidovendatipo
	* @return
	* @since 25/11/2016
	* @author Luiz Fernando
	*/
	public Pedidovendatipo loadWithSituacaoDocumento(Pedidovendatipo pedidovendatipo) {
		if(pedidovendatipo == null || pedidovendatipo.getCdpedidovendatipo() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				.select("pedidovendatipo.cdpedidovendatipo, documentoacao.cddocumentoacao ")
				.leftOuterJoin("pedidovendatipo.documentoacao documentoacao")
				.where("pedidovendatipo = ?", pedidovendatipo)
				.unique();
	}

	@Override
	public ListagemResult<Pedidovendatipo> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Pedidovendatipo> query = querySined();
		updateListagemQuery(query, filtro);
		query.orderBy("pedidovendatipo.cdpedidovendatipo");
		
		return new ListagemResult<Pedidovendatipo>(query, filtro);
	}

	public Boolean existeVendaOrcamentoPedido(Integer cdpedidovendatipo) {
		SinedUtil.markAsReader();
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.setUseTranslator(false)
				.from(Pedidovendatipo.class)
				.openParentheses()
				.where("exists (select vo.cdvendaorcamento from Vendaorcamento vo join vo.pedidovendatipo pvt where pvt.cdpedidovendatipo = ? )", cdpedidovendatipo)
				.or()
				.where("exists (select v.cdvenda from Venda v join v.pedidovendatipo pvt where pvt.cdpedidovendatipo = ? )", cdpedidovendatipo)
				.or()
				.where("exists (select pv.cdpedidovenda from Pedidovenda pv join pv.pedidovendatipo pvt where pvt.cdpedidovendatipo = ? )", cdpedidovendatipo)
				.closeParentheses()
				.unique() > 0;
				
				
	}

	public void desmarcarCriarExpedicaoComConferencia() {
		getJdbcTemplate().update("update pedidovendatipo set criarExpedicaoComConferencia = false;");
	}
	
	public Boolean existsPedidovendaWithTicketMedio(){
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Pedidovendatipo.class)			
			.where("pedidovendatipo.validarTicketMedioPorFornecedor = true ")
			.unique() > 0;
			
	}
}
