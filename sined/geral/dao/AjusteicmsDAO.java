package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Ajusteicms;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.AjusteIcmsTipo;
import br.com.linkcom.sined.geral.bean.enumeration.TipoAjuste;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.AjusteicmsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaoicmsFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AjusteicmsDAO extends GenericDAO<Ajusteicms>{

	@Override
	public void updateListagemQuery(QueryBuilder<Ajusteicms> query, FiltroListagem _filtro) {
		AjusteicmsFiltro filtro = (AjusteicmsFiltro) _filtro;
		
		query
			.select("ajusteicms.cdajusteicms, ajusteicms.dtfatogerador, ajusteicms.valor, " +
					"empresa.nome, empresa.razaosocial, empresa.nomefantasia, ajusteicmstipo.descricao, ajusteicmstipo.tipoajuste, " +
					"uf.sigla")
			.leftOuterJoin("ajusteicms.empresa empresa")
			.leftOuterJoin("ajusteicms.ajusteicmstipo ajusteicmstipo")
			.leftOuterJoin("ajusteicms.uf uf")
			.where("empresa = ?", filtro.getEmpresa())
			.where("ajusteicmstipo.tipoajuste = ?", filtro.getTipoajuste()) 
			.where("ajusteicmstipo = ?", filtro.getAjusteicmstipo())
			.where("uf = ?", filtro.getUf())
			.where("ajusteicms.dtfatogerador = ?", filtro.getDtfatogerador());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Ajusteicms> query) {
		query
			.leftOuterJoinFetch("ajusteicms.ajusteicmstipo ajusteicmstipo")
			.leftOuterJoinFetch("ajusteicmstipo.uf uf")
			.leftOuterJoinFetch("ajusteicms.listaAjusteicmsprocesso listaAjusteicmsprocesso")
			.leftOuterJoinFetch("ajusteicms.listaAjusteicmsdocumento listaAjusteicmsdocumento")
			.leftOuterJoinFetch("listaAjusteicmsdocumento.pessoa pessoa")
			.leftOuterJoinFetch("listaAjusteicmsdocumento.modelonf modelonf")
			.leftOuterJoinFetch("listaAjusteicmsdocumento.material material");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaAjusteicmsprocesso");
		save.saveOrUpdateManaged("listaAjusteicmsdocumento");
	}

	/**
	 * Carrega infos para preenchimento do SPED
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/02/2014
	 */
	public List<Ajusteicms> findForSPEDRegE111(SpedarquivoFiltro filtro, String siglaUf) {
		QueryBuilder<Ajusteicms> query = query();
		this.updateEntradaQuery(query);
		return query
					.where("ajusteicmstipo.apuracao = ?", AjusteIcmsTipo.ICMS)
					.where("ajusteicms.empresa = ?", filtro.getEmpresa())
					.where("ajusteicms.uf.sigla = ?", siglaUf)
					.where("ajusteicms.dtfatogerador >= ?", filtro.getDtinicio())
					.where("ajusteicms.dtfatogerador <= ?", filtro.getDtfim())
					.list();
	}
	
	/**
	 * M�todo que carrega as informa��es do ajuste de icms para o SPED
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 * @since 17/03/2014
	 */
	public List<Ajusteicms> findForSPEDRegE220(SpedarquivoFiltro filtro, Uf uf) {
		QueryBuilder<Ajusteicms> query = query();
		this.updateEntradaQuery(query);
		return query
					.where("ajusteicmstipo.apuracao = ?", AjusteIcmsTipo.ICMS_ST)
					.where("ajusteicms.empresa = ?", filtro.getEmpresa())
					.where("ajusteicms.uf = ?", uf)
					.where("ajusteicms.dtfatogerador >= ?", filtro.getDtinicio())
					.where("ajusteicms.dtfatogerador <= ?", filtro.getDtfim())
					.list();
	}
	
	public List<Ajusteicms> findForSPEDRegE311(SpedarquivoFiltro filtro, String siglaUf) {
		QueryBuilder<Ajusteicms> query = query();
		this.updateEntradaQuery(query);
		return query
					.openParentheses()
						.where("ajusteicmstipo.apuracao = ?", AjusteIcmsTipo.ICMS_DIFAL)
						.or()
						.where("ajusteicmstipo.apuracao = ?", AjusteIcmsTipo.ICMS_FCP)
					.closeParentheses()
					.where("ajusteicms.empresa = ?", filtro.getEmpresa())
					.where("ajusteicmstipo.uf.sigla = ?", siglaUf)
					.where("ajusteicms.dtfatogerador >= ?", filtro.getDtinicio())
					.where("ajusteicms.dtfatogerador <= ?", filtro.getDtfim())
					.list();
	}
	
	public List<Ajusteicms> findForApuracaoICMSDebito(ApuracaoicmsFiltro filtro){
		return querySined()
				
		.select("ajusteicms.cdajusteicms, ajusteicms.dtfatogerador, ajusteicms.valor, ajusteicmstipo.tipoajuste")
		.leftOuterJoin("ajusteicms.empresa empresa")
		.leftOuterJoin("ajusteicms.ajusteicmstipo ajusteicmstipo")
		.leftOuterJoin("ajusteicms.uf uf")
		.where("empresa = ?", filtro.getEmpresa())
		.where("ajusteicmstipo.tipoajuste = ?", TipoAjuste.OUTROS_DEBITOS) 
		.where("ajusteicmstipo.apuracao = ?", AjusteIcmsTipo.ICMS)
		.where("ajusteicms.dtfatogerador >= ?", filtro.getDtinicio())
		.where("ajusteicms.dtfatogerador <= ?", filtro.getDtfim())
		.list();
	}
	
	public List<Ajusteicms> findForApuracaoICMSCredito(ApuracaoicmsFiltro filtro){
		return querySined()
				
		.select("ajusteicms.cdajusteicms, ajusteicms.dtfatogerador, ajusteicms.valor, ajusteicmstipo.tipoajuste")
		.leftOuterJoin("ajusteicms.empresa empresa")
		.leftOuterJoin("ajusteicms.ajusteicmstipo ajusteicmstipo")
		.leftOuterJoin("ajusteicms.uf uf")
		.where("empresa = ?", filtro.getEmpresa())
		.where("ajusteicmstipo.tipoajuste = ?", TipoAjuste.OUTROS_CREDITOS) 
		.where("ajusteicmstipo.apuracao = ?", AjusteIcmsTipo.ICMS)
		.where("ajusteicms.dtfatogerador >= ?", filtro.getDtinicio())
		.where("ajusteicms.dtfatogerador <= ?", filtro.getDtfim())
		.list();
	}

	public List<Ajusteicms> findForApuracaoICMSCredito09(SpedarquivoFiltro filtro) {
		return query()
				.select("uf.sigla, ajusteicmstipo.apuracao, ajusteicmstipo.tipoajuste, ajusteicmstipo.codigo, listaAjusteicmsdocumento.serie, " +
						"listaAjusteicmsdocumento.numero, listaAjusteicmsdocumento.valorajuste, listaAjusteicmsdocumento.tipoUtilizacaoCredito, " +
						"modelonf.codigo, ajusteicms.cdajusteicms ")
				.leftOuterJoin("ajusteicms.empresa empresa")
				.leftOuterJoin("ajusteicms.ajusteicmstipo ajusteicmstipo")
				.leftOuterJoin("ajusteicmstipo.uf uf")
				.leftOuterJoin("ajusteicms.listaAjusteicmsdocumento listaAjusteicmsdocumento")
				.leftOuterJoin("listaAjusteicmsdocumento.modelonf modelonf")
				.where("empresa = ?", filtro.getEmpresa())
				.where("ajusteicmstipo.apuracao = ?", AjusteIcmsTipo.ICMS)
				.where("ajusteicmstipo.tipoajuste = ?", TipoAjuste.CONTROLE_ICMS_EXTRA)
				.where("ajusteicms.dtfatogerador >= ?", filtro.getDtinicio())
				.where("ajusteicms.dtfatogerador <= ?", filtro.getDtfim())
				.list();
	}

	@Override
	public ListagemResult<Ajusteicms> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Ajusteicms> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("ajusteicms.cdajusteicms");
		return new ListagemResult<Ajusteicms>(query, filtro); 
	}
}
