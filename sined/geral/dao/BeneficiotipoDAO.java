package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Beneficiotipo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("beneficiotipo.nome")
public class BeneficiotipoDAO extends GenericDAO<Beneficiotipo>{
}
