package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Pneumarca;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("pneumarca.nome")
public class PneumarcaDAO extends GenericDAO<Pneumarca>{
	
	/**
	* M�todo autocomplete para buscar marca de pneu
	*
	* @param q
	* @return
	* @since 23/10/2015
	* @author Luiz Fernando
	*/
	public List<Pneumarca> findAutocomplete(String q) {
		return query()
			.select("pneumarca.cdpneumarca, pneumarca.nome")
			.whereLikeIgnoreAll("pneumarca.nome", q)
			.autocomplete()
			.orderBy("pneumarca.nome")
			.list();
	}
	
	/**
	 * Busca a lista de pneumarca para enviar para o W3Producao
	 * @param whereIn
	 * @return
	 */
	public List<Pneumarca> findForW3Producao(String whereIn){
		return query()
					.select("pneumarca.cdpneumarca, pneumarca.nome")
					.whereIn("pneumarca.cdpneumarca", whereIn)
					.list();
	}

	/**
	 * Busca a marca de pneu pelo nome.
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/04/2016
	 */
	public Pneumarca loadByNome(String nome) {
		if(nome == null || nome.trim().equals("")){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
				.select("pneumarca.cdpneumarca, pneumarca.nome")
				.where("upper(pneumarca.nome) like ?", nome.toUpperCase())
				.unique();
	}
	
	/**
	* M�todo que busca as marcas dos pneus
	*
	* @param whereIn
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<Pneumarca> findForAndroid(String whereIn) {
		return query()
		.select("pneumarca.cdpneumarca, pneumarca.nome")
		.whereIn("pneumarca.cdpneumarca", whereIn)
		.list();
	}

	@Override
	public ListagemResult<Pneumarca> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Pneumarca> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("pneumarca.cdpneumarca");
		
		return new ListagemResult<Pneumarca>(query, filtro);
	}
	
	
	public Pneumarca findForCd(String whereIn) {
		return query()
		.select("pneumarca.cdpneumarca, pneumarca.nome")
		.whereIn("pneumarca.cdpneumarca", whereIn)
		.unique();
	}
}
