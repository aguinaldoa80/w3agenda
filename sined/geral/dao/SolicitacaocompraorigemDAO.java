package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.Solicitacaocompraorigem;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class SolicitacaocompraorigemDAO extends GenericDAO<Solicitacaocompraorigem> {

	/**
	* M�todo que remove o id da tabela tarefarecursogeral da origem da solicita��o de compra
	*
	* @param solicitacaocompraorigem
	* @since 11/06/2015
	* @author Luiz Fernando
	*/
	public void removeVinculoOrigemRecustogeral(Solicitacaocompraorigem solicitacaocompraorigem) {
		if(solicitacaocompraorigem != null && solicitacaocompraorigem.getCdsolicitacaocompraorigem() != null){
			String sql = "update solicitacaocompraorigem set cdtarefarecursogeral = null where cdsolicitacaocompraorigem = " +
				solicitacaocompraorigem.getCdsolicitacaocompraorigem();
			getJdbcTemplate().update(sql);
		}
	}
}
