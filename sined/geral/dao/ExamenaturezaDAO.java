package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Examenatureza;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ExamenaturezaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ExamenaturezaDAO extends GenericDAO<Examenatureza> {

	@Override
	public void updateListagemQuery(QueryBuilder<Examenatureza> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inválidos");
		}
		ExamenaturezaFiltro filtro = (ExamenaturezaFiltro) _filtro;
		query
		.select("examenatureza.cdexamenatureza, examenatureza.nome")
		.whereLikeIgnoreAll("examenatureza.nome", filtro.getNome());
	}
	
	/* singleton */
	private static ExamenaturezaDAO instance;
	public static ExamenaturezaDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ExamenaturezaDAO.class);
		}
		return instance;
	}
}