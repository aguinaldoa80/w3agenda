package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Vara;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.VaraFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("vara.nome")
public class VaraDAO extends GenericDAO<Vara>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Vara> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		VaraFiltro filtro = (VaraFiltro) _filtro;
		query
			.select("vara.cdvara, vara.nome, municipio.nome, uf.sigla")
			.leftOuterJoin("vara.municipio municipio")
			.leftOuterJoin("vara.uf uf")
			.leftOuterJoin("vara.orgaojuridico orgaojuridico")
			.whereLikeIgnoreAll("vara.nome", filtro.getNome())
			.where("uf = ?", filtro.getUf())
			.where("orgaojuridico = ?", filtro.getOrgaojuridico())
			.where("municipio = ?", filtro.getMunicipio())
			.orderBy("vara.nome");
		
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Vara> query) {
		query
			.select("vara.cdvara, vara.nome, municipio.cdmunicipio, municipio.nome, uf.cduf, vara.cdusuarioaltera, vara.dtaltera, " +
					"orgaojuridico.cdorgaojuridico, orgaojuridico.nome")
			.leftOuterJoin("vara.municipio municipio")
			.leftOuterJoin("vara.orgaojuridico orgaojuridico")
			.leftOuterJoin("vara.uf uf");
	}

	/**
	 * Obt�m todas as varas de uma UF ou todas que n�o possuem UF.
	 * @author Taidson Santos
	 */
	public List<Vara> findByVaraUf(Uf uf, Municipio municipio){
		if(uf == null || uf.getCduf() == null){
			return 
			query()
			.select("vara.nome,vara.cdvara")
			.where("vara.uf.id is null")
			.list();
		}else if(municipio == null || municipio.getCdmunicipio() == null){
			return 
			query()
			.select("vara.nome,vara.cdvara")
			.join("vara.uf uf")
			.where("uf = ?", uf)
			.where("vara.municipio.id is null")
			.list();
		}else{
			return 
			query()
			.select("vara.nome,vara.cdvara")
			.join("vara.uf uf")
			.join("vara.municipio municipio")
			.where("uf = ?", uf)
			.list();
		}
	}
	
	/**
	 * Obt�m todas as varas de um Munic�pio ou todas que n�o possuem Munic�pio.
	 * @author Taidson Santos
	 */
	public List<Vara> findByMunicipio(Municipio municipio){
		if(municipio == null || municipio.getCdmunicipio() == null){
			return 
			query()
			.select("vara.cdvara, vara.nome, municipio.cdmunicipio,municipio.nome")
			.join("vara.municipio municipio")
			.where("municipio.id is null")
			.orderBy("vara.nome")
			.list();
		}else{
			
			return 
			query()
			.select("vara.cdvara, vara.nome, municipio.cdmunicipio,municipio.nome")
			.join("vara.municipio municipio")
			.where("municipio = ?",municipio)
			.orderBy("vara.nome")
			.list();
		}
	}
	
	/**
	 * Obtem o objeto Vara com UF e Munic�pio correspondentes.
	 * @author Taidson Santos
	 * @since 22/06/2010
	 */
	public Vara findByVaraComarcaUf(Vara vara) {
		return
		query()
			.select("vara.cdvara, vara.nome, municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla")
			.leftOuterJoin("vara.municipio municipio")
			.leftOuterJoin("vara.uf uf")
			.where("vara = ?", vara)
			.unique();
	}
}
