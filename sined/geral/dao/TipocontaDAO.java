package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Tipoconta;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TipocontaDAO extends GenericDAO<Tipoconta> {
	
	
	/**
	 * M�todo para obter itens do combo para o tipo de conta poupan�a.
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Tipoconta> findTipoPoupanca(){
		return 
			query()
			.where("cdtipoconta = 2")
			.list();
	}
	
	/**
	 * M�todo para obter itens do combo para o tipo de conta corrente.
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Tipoconta> findTipoCorrente(){
		return 
			query()
			.where("cdtipoconta = 1")
			.list();
	}
	
	/* singleton */
	private static TipocontaDAO instance;
	public static TipocontaDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(TipocontaDAO.class);
		}
		return instance;
	}
		
}
