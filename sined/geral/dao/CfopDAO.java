package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Cfopescopo;
import br.com.linkcom.sined.geral.bean.Cfoptipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CfopFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("cfop.codigo")
public class CfopDAO extends GenericDAO<Cfop> {

	@Override
	public void updateListagemQuery(QueryBuilder<Cfop> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		CfopFiltro filtro = (CfopFiltro) _filtro;
		query
			.select("distinct cfop.cdcfop, cfop.codigo, cfop.descricaoresumida, cfoptipo.descricao, cfopescopo.descricao")
			.join("cfop.cfoptipo cfoptipo")
			.join("cfop.cfopescopo cfopescopo")
			.whereLikeIgnoreAll("cfop.codigo", filtro.getCodigo())
			.whereLikeIgnoreAll("cfop.descricaoresumida", filtro.getDescricaoresumida())
			.where("cfoptipo = ?", filtro.getCfoptipo())
			.where("cfopescopo = ?", filtro.getCfopescopo());
		
		if (filtro.getNaturezaoperacao()!=null){
			query.join("cfop.listaNaturezaoperacaocfop listaNaturezaoperacaocfop");
			query.join("listaNaturezaoperacaocfop.naturezaoperacao naturezaoperacao");
			query.where("naturezaoperacao=?", filtro.getNaturezaoperacao());
			query.ignoreJoin("listaNaturezaoperacaocfop", "naturezaoperacao");
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Cfop> query) {
		query
			.select("cfop.cdcfop, cfop.codigo, cfop.descricaoresumida, cfop.descricaocompleta, cfop.atualizarvalor, cfop.naoconsiderarreceita, " +
					"cfop.cdusuarioaltera, cfop.dtaltera, cfoptipo.cdcfoptipo, cfopescopo.cdcfopescopo, cfop.incluirvalorfinal," +
					"listacfoprelacionado.cdcfoprelacionado, cfopdestino.cdcfop, cfopdestino.descricaocompleta, cfopdestino.descricaoresumida," +
					"contaGerencial.cdcontagerencial,contaGerencial.nome")
			.join("cfop.cfoptipo cfoptipo")
			.join("cfop.cfopescopo cfopescopo")
			.leftOuterJoin("cfop.listacfoprelacionado listacfoprelacionado")
			.leftOuterJoin("cfop.contaGerencial contaGerencial")
			.leftOuterJoin("listacfoprelacionado.cfopdestino cfopdestino");
	}

	public List<Cfop> findByTipoEscopo(Cfoptipo cfoptipo, Cfopescopo cfopescopo) {
		return query()
					.select("cfop.cdcfop, cfop.descricaoresumida, cfop.descricaocompleta, cfop.codigo")
					.join("cfop.cfoptipo cfoptipo")
					.join("cfop.cfopescopo cfopescopo")
					.where("cfoptipo = ?", cfoptipo)
					.where("cfopescopo = ?", cfopescopo)
					.orderBy("cfop.codigo")
					.list();
	}

	public Cfop findByCodigo(String codigo) {
		return query()
					.select("cfop.cdcfop, cfop.descricaoresumida, cfop.descricaocompleta, cfop.codigo")
					.where("cfop.codigo like ?", codigo)
					.unique();
	}
	
	/**
	 * 
	 * M�todo para carregar o Cfop � partir de um Cfop
	 *
	 * @name carregarCfop
	 * @param cfop
	 * @return
	 * @return Cfop
	 * @author Thiago Augusto
	 * @date 05/04/2012
	 *
	 */
	public Cfop carregarCfop(Cfop cfop) {
		return query()
		.select("cfop.cdcfop, cfop.descricaoresumida, cfop.descricaocompleta, cfop.codigo, cfop.naoconsiderarreceita")
		.where("cfop= ?", cfop)
		.unique();
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listacfoprelacionado", "cfop");
	}
	
	
	/**
	 * 
	 * M�todo que busca o Cfop com usas refer�ncias
	 *
	 * @name buscarReferenciasCFOP
	 * @param cfop
	 * @return
	 * @return Cfop
	 * @author Thiago Augusto
	 * @date 16/04/2012
	 *
	 */
	public Cfop buscarReferenciasCFOP(Cfop cfop){
		return query()
					.select("cfop.cdcfop, cfop.codigo, cfop.descricaoresumida, cfop.naoconsiderarreceita, " +
							"listacfoprelacionado.cdcfoprelacionado, cfopdestino.cdcfop, " +
							"cfopdestino.descricaoresumida, cfopdestino.codigo, cfopdestino.naoconsiderarreceita ")
					.leftOuterJoin("cfop.listacfoprelacionado listacfoprelacionado")
					.leftOuterJoin("listacfoprelacionado.cfopdestino cfopdestino")
					.where("cfop = ?", cfop)
					.orderBy("cfopdestino.codigo")
					.unique();
	}

	public List<Cfop> findAutoCompleteEntrada(String q) {
		return query()
				.select("cfop.cdcfop, cfop.descricaoresumida, cfop.descricaocompleta, cfop.codigo, cfop.naoconsiderarreceita")
				.leftOuterJoin("cfop.cfoptipo cfoptipo")
				.openParentheses()
				.whereLikeIgnoreAll("cfop.codigo", q)
				.or()
				.whereLikeIgnoreAll("cfop.descricaoresumida", q)
				.closeParentheses()
				.where("cfoptipo = ?", Cfoptipo.ENTRADA)
				.list();
	}
	
	public List<Cfop> findAutoComplete(String q) {
		return query()
				.select("cfop.cdcfop, cfop.descricaoresumida, cfop.descricaocompleta, cfop.codigo")
				.openParentheses()
					.whereLikeIgnoreAll("cfop.codigo", q)
				.or()
					.whereLikeIgnoreAll("cfop.descricaoresumida", q)
				.closeParentheses()
				.list();
	}
}
