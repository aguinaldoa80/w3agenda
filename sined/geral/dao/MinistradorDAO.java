package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Instrutor;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.MinistradorFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("upper(retira_acento(instrutor.nome))")
public class MinistradorDAO extends GenericDAO<Instrutor> {

	
	@Override
	public void updateListagemQuery(QueryBuilder<Instrutor> query, FiltroListagem _filtro) {
		MinistradorFiltro filtro = (MinistradorFiltro) _filtro;
		
		query
			.whereLikeIgnoreAll("instrutor.nome", filtro.getNome())
			.where("instrutor.ativo = ?",filtro.getAtivo())
			.orderBy("instrutor.nome");
	}

	/* singleton */
	private static MinistradorDAO instance;
	public static MinistradorDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(MinistradorDAO.class);
		}
		return instance;
	}
	
}
