package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Ausenciacolaborador;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.AusenciacolaboradorFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AusenciacolaboradorDAO extends GenericDAO<Ausenciacolaborador>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Ausenciacolaborador> query, FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O parametro _filtro n�o pode ser null.");
		}
		
		AusenciacolaboradorFiltro filtro = (AusenciacolaboradorFiltro) _filtro;
		
		query
			.select("ausenciacolaborador.cdausenciacolaborador, ausenciacolaborador.descricao, colaborador.cdpessoa, colaborador.nome, " +
					"ausenciamotivo.cdausenciamotivo, ausenciamotivo.motivo, ausenciacolaborador.dtinicio, ausenciacolaborador.dtfim")
			.join("ausenciacolaborador.colaborador colaborador")
			.join("ausenciacolaborador.ausenciamotivo ausenciamotivo")
			.join("colaborador.listaColaboradorcargo listacargo")
			.whereLikeIgnoreAll("ausenciacolaborador.descricao", filtro.getDescricao())
			.where("ausenciacolaborador.dtinicio >= ?", filtro.getDtde())
			.where("ausenciacolaborador.dtfim <= ?", filtro.getDtate())
			.where("ausenciamotivo = ?", filtro.getAusenciamotivo())
			.where("colaborador = ?", filtro.getColaborador())
			.where("listacargo.cargo = (select max(listaColaboradorcargo.cargo) from colaborador.listaColaboradorcargo listaColaboradorcargo)")
			.where("listacargo.empresa = ?", filtro.getEmpresa())
			.orderBy("ausenciacolaborador.cdausenciacolaborador desc");
		
//		if(filtro.getColaborador() == null){
//			query.where("1=0");
//		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Ausenciacolaborador> query) {
		query 
		.select("ausenciacolaborador.cdausenciacolaborador, ausenciacolaborador.descricao, colaborador.cdpessoa, colaborador.nome, " +
				"ausenciacolaborador.hrinicio, ausenciacolaborador.hrfim, ausenciamotivo.cdausenciamotivo, ausenciamotivo.motivo, " +
				"ausenciacolaborador.dtinicio, ausenciacolaborador.dtfim, " +
				"listausenciacolaboradorhistorico.cdausenciacolaboradorhistorico, listausenciacolaboradorhistorico.cdusuarioaltera, " +
				"listausenciacolaboradorhistorico.dtaltera, listausenciacolaboradorhistorico.acao ")
		.join("ausenciacolaborador.colaborador colaborador")
		.join("ausenciacolaborador.ausenciamotivo ausenciamotivo")
		.leftOuterJoin("ausenciacolaborador.listausenciacolaboradorhistorico listausenciacolaboradorhistorico");
	}

	/**
	 * M�todo que busca as aus�ncias do colaborador em um per�odo de tempo.
	 * 
	 * @param colaborador
	 * @param dtReferencia
	 * @param dtReferenciaAte
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Ausenciacolaborador> findAusenciasDoColaborador(Colaborador colaborador, Date datede, Date dateate) {
		return query()
		.select("ausenciacolaborador.cdausenciacolaborador, ausenciacolaborador.descricao, ausenciacolaborador.dtinicio, ausenciacolaborador.dtfim, ausenciacolaborador.hrinicio, ausenciacolaborador.hrfim")
		.join("ausenciacolaborador.colaborador colaborador")
		.where("colaborador = ?", colaborador)
		.openParentheses()
			.openParentheses()
				.where("ausenciacolaborador.dtinicio <= ?", datede)
				.where("ausenciacolaborador.dtfim >= ?", datede)
			.closeParentheses().or()
			.openParentheses()
				.where("ausenciacolaborador.dtinicio <= ?", datede)
				.where("ausenciacolaborador.dtfim is null")
			.closeParentheses().or()
			.openParentheses()
				.where("ausenciacolaborador.dtinicio <= ?", dateate)
				.where("ausenciacolaborador.dtfim >= ?", dateate)
			.closeParentheses().or()
			.openParentheses()
				.where("ausenciacolaborador.dtinicio <= ?", dateate)
				.where("ausenciacolaborador.dtfim is null")
			.closeParentheses().or()
			.openParentheses()
				.where("ausenciacolaborador.dtinicio >= ?", datede)
				.where("ausenciacolaborador.dtinicio <= ?", dateate)
			.closeParentheses().or()
			.openParentheses()
				.where("ausenciacolaborador.dtfim >= ?", datede)
				.where("ausenciacolaborador.dtfim <= ?", dateate)
			.closeParentheses().or()
		.closeParentheses()
		.list();
	}

	/**
	 * 
	 * M�todo que busca a listagem de AusenciaColaborador para o report.
	 *
	 * @name findForReport
	 * @param filtro
	 * @return
	 * @return List<Ausenciacolaborador>
	 * @author Thiago Augusto
	 * @date 13/08/2012
	 *
	 */
	public List<Ausenciacolaborador> findForReport(AusenciacolaboradorFiltro filtro){
		return querySined()
			.select("ausenciacolaborador.cdausenciacolaborador, ausenciacolaborador.descricao, colaborador.cdpessoa, colaborador.nome, " +
				"ausenciamotivo.cdausenciamotivo, ausenciamotivo.motivo, ausenciacolaborador.dtinicio, ausenciacolaborador.dtfim")
			.join("ausenciacolaborador.colaborador colaborador")
			.join("ausenciacolaborador.ausenciamotivo ausenciamotivo")
			.join("colaborador.listaColaboradorcargo listacargo")
			.whereLikeIgnoreAll("ausenciacolaborador.descricao", filtro.getDescricao())
			.where("ausenciacolaborador.dtinicio >= ?", filtro.getDtde())
			.where("ausenciacolaborador.dtfim <= ?", filtro.getDtate())
			.where("ausenciamotivo = ?", filtro.getAusenciamotivo())
			.where("colaborador = ?", filtro.getColaborador())
			.where("listacargo.cargo = (select max(listaColaboradorcargo.cargo) from colaborador.listaColaboradorcargo listaColaboradorcargo)")
			.where("listacargo.empresa = ?", filtro.getEmpresa())
		    .orderBy("ausenciacolaborador.cdausenciacolaborador desc")
		    .list();
	}
	
}
