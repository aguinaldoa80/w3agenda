package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.Colaboradordespesa;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Despesaavulsarh;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Fornecimento;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentoorigemDAO extends GenericDAO<Documentoorigem>{

	/**
	 * M�todo que busca a lista de contas a pagar geradas pela entrega
	 * 
	 * @param entrega
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Documentoorigem> getListaDocumentosDaEntrega(Entrega entrega) {
		if (entrega == null || entrega.getCdentrega() == null){
			throw new SinedException("Entrega n�o pode ser nulo.");
		}
		return query()
			.select("documento.cddocumento, documento.dtvencimento, aux_documento.valoratual")
			.join("documentoorigem.documento documento")
			.join("documento.aux_documento aux_documento")
			.join("documentoorigem.entrega entrega")			
			.where("entrega = ?", entrega)
			.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
			.orderBy("documento.cddocumento")
			.list();
	}
	
	/**
	 * M�todo que busca a lista de contas a pagar geradas pelaentrada fiscal vinculada a um recebimento
	 * 
	 * @param wherein
	 * @return
	 * @author Cleiton
	 */
	public List<Documentoorigem> getListaDocumentosDaEntradaFiscal(String whereIn) {
		return query()
			.select("documento.cddocumento, documento.dtvencimento, aux_documento.valoratual")
			.join("documentoorigem.documento documento")
			.join("documento.aux_documento aux_documento")
			.join("documentoorigem.entrega entrega")
			.join("entrega.listaEntregadocumento entregadocumento")
			.whereIn("entregadocumento.cdentregadocumento", whereIn)
			.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
			.orderBy("documento.cddocumento")
			.list();
	}
	
	public List<Documentoorigem> getListaDocumentosDaEntradaFiscalCancelar(String whereIn) {
		return query()
				.select("documento.cddocumento, documento.dtvencimento, aux_documento.valoratual")
				.join("documentoorigem.documento documento")
				.join("documento.aux_documento aux_documento")
				.join("documentoorigem.entregadocumento entregadocumento")
				.whereIn("entregadocumento.cdentregadocumento", whereIn)
				.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
				.orderBy("documento.cddocumento")
				.list();
	}
	
	/**
	 * M�todo que verifica se existe documento n�o cancelado relacionado a entrega 
	 *
	 * @param entrega
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existDocumentosByEntrega(Entrega entrega) {
		if (entrega == null || entrega.getCdentrega() == null) {
			throw new SinedException("Entrega n�o pode ser nulo.");
		}
		return newQueryBuilderSined(Long.class)
			.from(Documentoorigem.class)
			.select("count(*)")
			.join("documentoorigem.documento documento")
			.join("documento.aux_documento aux_documento")
			.join("documentoorigem.entrega entrega")
			.where("entrega = ?", entrega)
			.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
			.unique() > 0;
	}

	/**
	 * M�todo que busca a lista de contas a pagar geradas pelo fornecimento
	 * 
	 * @param fornecimento
	 * @return
	 */
	public List<Documentoorigem> getListaDocumentosDoFornecimento(Fornecimento fornecimento) {
		if (fornecimento == null || fornecimento.getCdfornecimento() == null) {
			throw new SinedException("Entrega n�o pode ser nulo.");
		}
		return query()
			.select("documento.cddocumento")
			.join("documentoorigem.documento documento")
			.join("documentoorigem.fornecimento fornecimento")
			.where("fornecimento = ?", fornecimento)
			.orderBy("documento.cddocumento")
			.list();
	}
	/**
	 * M�todo que busca a lista de contas a receber geradas por uma venda
	 * 
	 * @param venda
	 * @param whereInDocumento
	 * @return
	 * @author Ramon Brazil
	 */
	public List<Documentoorigem> getListaDocumentosDeVenda(Venda venda, String whereInDocumento) {
		if (venda == null || venda.getCdvenda() == null) {
			throw new SinedException("Venda n�o pode ser nulo.");
		}		
		return query()
			.select("documentoorigem.cddocumentoorigem, documento.cddocumento, documento.dtvencimento, documento.valor, documento.numero, documentotipo.nome, " +
					"documentotipo.cddocumentotipo, indicecorrecao.cdindicecorrecao, indicecorrecao.sigla, conta.cdconta, conta.nome, " +
					"documentoacao.cddocumentoacao, documentoacao.nome")
			.join("documentoorigem.documento documento")
			.join("documento.documentotipo documentotipo")
			.leftOuterJoin("documento.indicecorrecao indicecorrecao")
			.leftOuterJoin("documento.conta conta")
			.join("documento.documentoacao documentoacao")
			.join("documentoorigem.venda venda")
			.where("venda = ?", venda)
			.whereIn("documento.cddocumento", whereInDocumento)
			.orderBy("documento.cddocumento")
			.list();
	}
	
	public List<Documentoorigem> findDocumentoByVenda(String whereInVenda) {
		if (StringUtils.isBlank(whereInVenda)) {
			throw new SinedException("Venda n�o pode ser nulo.");
		}		
		QueryBuilder<Documentoorigem> query = query()
			.select("documentoorigem.cddocumentoorigem, documento.cddocumento, venda.cdvenda")
			.join("documentoorigem.documento documento")
			.join("documentoorigem.venda venda");
		
		SinedUtil.quebraWhereIn("venda.cdvenda", whereInVenda, query);
		
		return query
			.orderBy("documento.cddocumento")
			.list();
	}
	
	/**
	 * M�todo que busca a lista de contas a receber geradas por um pedido de venda
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentoorigem> getListaDocumentosDePedidovenda(Pedidovenda pedidovenda) {
		if (pedidovenda == null || pedidovenda.getCdpedidovenda() == null) {
			throw new SinedException("Pedido de venda n�o pode ser nulo.");
		}		
		return query()
			.select("documentoorigem.cddocumentoorigem, documento.cddocumento, documento.dtvencimento, documento.valor, documento.numero, documentotipo.nome, " +
					"documentotipo.cddocumentotipo, indicecorrecao.cdindicecorrecao, indicecorrecao.sigla, conta.cdconta, conta.nome, " +
					"documentoacao.cddocumentoacao, documentoacao.nome")
			.join("documentoorigem.documento documento")
			.join("documento.documentotipo documentotipo")
			.leftOuterJoin("documento.indicecorrecao indicecorrecao")
			.leftOuterJoin("documento.conta conta")
			.join("documento.documentoacao documentoacao")
			.join("documentoorigem.pedidovenda pedidovenda")
			.where("pedidovenda = ?", pedidovenda)
			.orderBy("documento.cddocumento")
			.list();
	}
	
	/**
	 * M�todo que verifica se j� existe algum documento com a entrega de origem,
	 * pois se houver � porque esta sendo duplicado o registro.
	 * 
	 * @param entrega
	 * @return
	 */
	public boolean existeDocumentoComOrigemEntrega(Entrega entrega) {
		return existeDocumentoComOrigemEntrega(entrega);
	}

	/**
	 * Retorna uma lista de documentoorigem contendo documentos relacionados � contrato
	 * @param contrato
	 * @return
	 * @author Taidson
	 * @since 24/11/2010
	 */
	public List<Documentoorigem> contasContrato(Contrato contrato) {
		if (contrato == null || contrato.getCdcontrato() == null) {
			throw new SinedException("Contrato n�o pode ser nulo.");
		}		
		return query()
			.select("documento.cddocumento")
			.join("documentoorigem.documento documento")
			.join("documentoorigem.contrato contrato")
			.where("contrato = ?", contrato)
			.orderBy("documento.cddocumento")
			.list();
	}

	/**
	 * M�todo que busca os documentos origem de um determinado documento
	 * 
	 * @param documento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Documentoorigem> findByDocumento(Documento documento) {
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("documentoorigem.cddocumentoorigem, documento.cddocumento, entrega.cdentrega, fornecimento.cdfornecimento, veiculodespesa.cdveiculodespesa, " +
					"despesaviagemadiantamento.cddespesaviagem, despesaviagemacerto.cddespesaviagem, venda.cdvenda, contrato.cdcontrato, agendamentoservicodocumento.cdagendamentoservicodocumento")
			.join("documentoorigem.documento documento")
			.leftOuterJoin("documentoorigem.entrega entrega")
			.leftOuterJoin("documentoorigem.fornecimento fornecimento")
			.leftOuterJoin("documentoorigem.veiculodespesa veiculodespesa")
			.leftOuterJoin("documentoorigem.despesaviagemadiantamento despesaviagemadiantamento")
			.leftOuterJoin("documentoorigem.despesaviagemacerto despesaviagemacerto")
			.leftOuterJoin("documentoorigem.venda venda")
			.leftOuterJoin("documentoorigem.contrato contrato")
			.leftOuterJoin("documentoorigem.agendamentoservicodocumento agendamentoservicodocumento")
			.where("documento = ?", documento)
			.list();
	}
	
	public List<Documentoorigem> findByDocumento(String whereIn) {
		if(whereIn == null || whereIn.equals(""))
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
				.select("documentoorigem.cddocumentoorigem, documento.cddocumento, despesaviagemadiantamento.cddespesaviagem, despesaviagemacerto.cddespesaviagem, venda.cdvenda ")
						.join("documentoorigem.documento documento")
						.leftOuterJoin("documentoorigem.despesaviagemadiantamento despesaviagemadiantamento")
						.leftOuterJoin("documentoorigem.despesaviagemacerto despesaviagemacerto")
						.leftOuterJoin("documentoorigem.venda venda")
						.whereIn("documento.cddocumento", whereIn)
						.list();
	}

	public Documentoorigem contaColaboradordespesa(Colaboradordespesa colaboradordespesa) {
		if(colaboradordespesa == null || colaboradordespesa.getCdcolaboradordespesa() == null){
			throw new SinedException("Par�metros inv�lidos.");
		}
		
		return query()
					.select("documentoorigem.cddocumentoorigem, colaboradordespesa.cdcolaboradordespesa, documento.cddocumento, " +
							"documentoacao.cddocumentoacao, documentoacao.nome")
					.join("documentoorigem.documento documento")
					.join("documentoorigem.colaboradordespesa colaboradordespesa")
					.join("documento.documentoacao documentoacao")					
					.where("colaboradordespesa.cdcolaboradordespesa = ?", colaboradordespesa.getCdcolaboradordespesa())
					.unique();
					
	}

	/**
	 * Recupera as contas geradas a partir de uma {@link Despesaavulsarh}.
	 * 
	 * @author <a mailto="giovane.freitas@linkcom.com.br">Giovane Freitas</a>
	 * @since Oct 22, 2011
	 * @param despesaavulsarh
	 * @return
	 */
	public Documentoorigem contaDespesaavulsarh(Despesaavulsarh despesaavulsarh) {
		if(despesaavulsarh == null || despesaavulsarh.getCddespesaavulsarh() == null){
			throw new SinedException("Par�metros inv�lidos.");
		}
		
		return query()
					.select("documentoorigem.cddocumentoorigem, despesaavulsarh.cddespesaavulsarh, documento.cddocumento, " +
							"documentoacao.cddocumentoacao, documentoacao.nome")
					.join("documentoorigem.documento documento")
					.join("documentoorigem.despesaavulsarh despesaavulsarh")
					.join("documento.documentoacao documentoacao")					
					.where("despesaavulsarh.cddespesaavulsarh = ?", despesaavulsarh.getCddespesaavulsarh())
					.unique();
	}

	/**M�todo que busca o n�mero, valor e data de vencimento para a lista de duplicatas da NF.
	 * @author Thiago Augusto
	 * @param venda
	 * @return
	 */
	public List<Documentoorigem> buscarListaParaDuplicatas(Venda venda){
		return query().select("documento.cddocumento, documento.numero, documento.valor, documento.dtvencimento, documentotipo.cddocumentotipo")
		.leftOuterJoin("documentoorigem.documento documento")
		.leftOuterJoin("documentoorigem.venda venda")
		.leftOuterJoin("documento.documentotipo documentotipo")
		.where("venda = ?", venda)
		.orderBy("documento.dtvencimento, documento.cddocumento ")
		.list();
	}
	
	/**
	 * 
	 * M�todo que carrega o DocumentoOrigem � partir de um Documento e retorna o bean 
	 * com o VendaSituacao para a verifica��o na tela de Conta a Receber para fazer o cancelamento da Conta.
	 *
	 * @name findDocumentoOrigemByDocumento
	 * @param documento
	 * @return
	 * @return Documentoorigem
	 * @author Thiago Augusto
	 * @date 11/05/2012
	 *
	 */
	public Documentoorigem findDocumentoOrigemByDocumento(Documento documento){
		return query()
				.select("documentoorigem.cddocumentoorigem, venda.cdvenda, vendasituacao.cdvendasituacao, vendasituacao.descricao")
				.leftOuterJoin("documentoorigem.documento documento")
				.leftOuterJoin("documentoorigem.venda venda")
				.leftOuterJoin("venda.vendasituacao vendasituacao")
				.where("documento = ? ", documento)
				.unique();
	}

	/**
	 * Verifica se a conta a receber passada por par�metro � de origem de uma venda.
	 *
	 * @param documento
	 * @return
	 * @since 21/09/2012
	 * @author Rodrigo Freitas
	 */
	public boolean isOrigemVenda(Documento documento) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Documentoorigem.class)
					.leftOuterJoin("documentoorigem.documento documento")
					.leftOuterJoin("documentoorigem.venda venda")
					.where("venda is not null")
					.where("documento = ?", documento)
					.unique() > 0;
	}
	
	/**
	 * M�todo que carrega os documentos referenciados na entregadocumento
	 * 
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Documentoorigem> getlistaDocumentosReferenciados(String whereIn){
		if(whereIn == null || whereIn.isEmpty()){
			return null;
		}
		
		return query()
				.select("documentoorigem.cddocumentoorigem, documento.cddocumento, entregadocumentoreferenciado.cdentregadocumentoreferenciado, " +
						"documentoacao.cddocumentoacao, documentoacao.nome")
				.join("documentoorigem.documento documento")
				.join("documento.documentoacao documentoacao")
				.join("documentoorigem.entregadocumentoreferenciado entregadocumentoreferenciado")
				.whereIn("entregadocumentoreferenciado.cdentregadocumentoreferenciado", whereIn)
				.list();
	}

	/**
	 * Verifica se existe algum documento vinculada a coleta sem estar cancelada.
	 *
	 * @param coleta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/07/2014
	 */
	public boolean haveDocumentoByColetaNotCancelada(Coleta coleta) {
		return newQueryBuilderWithFrom(Long.class)
					.select("count(*)")
					.join("documentoorigem.documento documento")
					.where("documentoorigem.coleta = ?", coleta)
					.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
					.unique() > 0;
	}
	
	/**
	 * Recupera o numero do Agendamento correspondente ao documento informado
	 * 
	 * @param documento
	 * @return
	 * @author Joao Vitor
	 * @since 10/10/2014
	 */
	public Agendamento findDocumentoOrigemAgendamentoByDocumento(Documento documento){
		if(documento == null || documento.getCddocumento() == null){
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		
		Documentoorigem documentoorigem = query()
				.select("agendamento.cdagendamento, agendamento.valor, agendamento.descricao, agendamento.dtproximo")
				.join("documentoorigem.agendamento agendamento")
				.where("documentoorigem.documento = ?", documento)
				.unique();
		
		if(documentoorigem != null) return documentoorigem.getAgendamento();
		return null;
	}
	
	/**
	* M�todo que busca os documento com origem vinculado a alguma despesa de viagem
	*
	* @param whereIn
	* @return
	* @since 21/03/2016
	* @author Luiz Fernando
	*/
	public List<Documentoorigem> findDocumentoWithDespesaviagem(String whereIn) {
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("documentoorigem.cddocumentoorigem, documento.cddocumento, documentoclasse.cddocumentoclasse, " +
					"despesaviagemadiantamento.cddespesaviagem, despesaviagemacerto.cddespesaviagem, despesaviagemreembolso.cddespesaviagem ")
			.join("documentoorigem.documento documento")
			.join("documento.documentoclasse documentoclasse")
			.leftOuterJoin("documentoorigem.despesaviagemadiantamento despesaviagemadiantamento")
			.leftOuterJoin("documentoorigem.despesaviagemacerto despesaviagemacerto")
			.leftOuterJoin("documentoorigem.despesaviagemreembolso despesaviagemreembolso")
			.whereIn("documento.cddocumento", whereIn)
			.openParentheses()
				.where("despesaviagemadiantamento is not null")
				.or()
				.where("despesaviagemacerto is not null")
				.or()
				.where("despesaviagemreembolso is not null")
			.closeParentheses()
			.list();
	}
	
	public List<Documentoorigem> findDocumentoWithGnre(String whereIn) {
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
				.select("documentoorigem.cddocumentoorigem, documento.cddocumento, documentoclasse.cddocumentoclasse, gnre.cdgnre")
						.join("documentoorigem.documento documento")
						.join("documento.documentoclasse documentoclasse")
						.join("documentoorigem.gnre gnre")
						.whereIn("documento.cddocumento", whereIn)
						.where("gnre is not null")
						.list();
	}
	
	/**
	* M�todo que verifica se existe documento vinculado a entrada fiscal ou ao recebimento da entrada fiscal
	*
	* @param entregadocumento
	* @return
	* @since 01/11/2016
	* @author Luiz Fernando
	*/
	public Boolean existeDocumentosNaoCanceladoEntradafiscal(Entregadocumento entregadocumento) {
		if (entregadocumento == null || entregadocumento.getCdentregadocumento() == null) {
			throw new SinedException("Entrada fiscal n�o pode ser nula.");
		}
		return newQueryBuilderSined(Long.class)
			.from(Documentoorigem.class)
			.select("count(*)")
			.join("documentoorigem.documento documento")
			.join("documento.aux_documento aux_documento")
			.leftOuterJoin("documentoorigem.entrega entrega")
			.leftOuterJoin("entrega.listaEntregadocumento listaEntregadocumento")
			.leftOuterJoin("documentoorigem.entregadocumento entregadocumento")
			.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
			.openParentheses()
				.where("listaEntregadocumento = ?", entregadocumento)
				.or()
				.where("entregadocumento = ?", entregadocumento)
			.closeParentheses()
			.unique() > 0;
	}
	
	public List<Documentoorigem> findByOrdemcompra(String whereIn) {
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("documentoorigem.cddocumentoorigem, documento.cddocumento, documentoacao.cddocumentoacao, ordemcompra.cdordemcompra ")
			.join("documentoorigem.documento documento")
			.join("documento.documentoacao documentoacao")
			.join("documentoorigem.ordemcompra ordemcompra")
			.whereIn("ordemcompra.cdordemcompra", whereIn)
			.list();
	}
	
	public boolean existeDocumentoorigemEntradafiscal(Documento documento, Entregadocumento entregadocumento) {
		return  newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Documentoorigem.class)
			.join("documentoorigem.documento documento")
			.join("documento.documentoacao documentoacao")
			.join("documentoorigem.entregadocumento entregadocumento")
			.setUseTranslator(false)
			.where("entregadocumento = ?", entregadocumento)
			.where("documento = ?", documento)
			.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
			.unique() > 0;
	}
	
	public boolean existeDocumentoorigemVenda(Venda venda) {
		return  newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Documentoorigem.class)
			.join("documentoorigem.documento documento")
			.join("documento.documentoacao documentoacao")
			.join("documentoorigem.venda venda")
			.setUseTranslator(false)
			.where("venda = ?", venda)
			.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
			.unique() > 0;
	}
}
