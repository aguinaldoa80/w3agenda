package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.OtrPneuTipo;
import br.com.linkcom.sined.geral.bean.OtrPneuTipoOtrClassificacaoCodigo;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OtrPneuTipoOtrClassificacaoCodigoDAO extends GenericDAO<OtrPneuTipoOtrClassificacaoCodigo>{
	
	public List<OtrPneuTipoOtrClassificacaoCodigo> findListOtrPneuTipoOtrClassificacaoCodigo(OtrPneuTipo otrPneuTipo){
		
		if (otrPneuTipo == null || otrPneuTipo.getCdOtrPneuTipo() == null) {
			throw new SinedException("Par�metro inv�lido");
		}
		
		return querySined()
				
				.select("otrPneuTipoOtrClassificacaoCodigo.cdOtrPneuTipoOtrClassificacaoCodigo, otrClassificacaoCodigo.cdOtrClassificacaoCodigo, " +
						"otrDesenho.cdOtrDesenho, otrDesenho.desenho, otrServicoTipo.cdOtrServicoTipo, otrServicoTipo.tipoServico")
				.leftOuterJoin("otrPneuTipoOtrClassificacaoCodigo.otrClassificacaoCodigo otrClassificacaoCodigo")
				.leftOuterJoin("otrClassificacaoCodigo.otrDesenho otrDesenho")
				.leftOuterJoin("otrClassificacaoCodigo.otrServicoTipo otrServicoTipo")
				.where("otrPneuTipoOtrClassificacaoCodigo.otrPneuTipo = ?", otrPneuTipo)
				.list();
	}	
}
