package br.com.linkcom.sined.geral.dao;

import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Controleorcamento;
import br.com.linkcom.sined.geral.bean.Controleorcamentoitem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Controleorcamentosituacao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ControleorcamentoitemFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

import com.ibm.icu.text.SimpleDateFormat;

public class ControleorcamentoitemDAO extends GenericDAO<Controleorcamentoitem>{

	/**
	 * M�todo para buscar a lista de Controleorcamentoitem pelas contas gerenciais e exerc�cio (ano)
	 *
	 * @param whereInContagerencial
	 * @param whereInCentrocusto 
	 * @param exercicio
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Controleorcamentoitem> findByContagerencialWhereIn(String whereInContagerencial, String whereInCentrocusto,Date dtExercicio, Integer cdempresa) {
		if(whereInContagerencial == null ||   "".equals(whereInContagerencial) || whereInCentrocusto == null ||"".equals(whereInCentrocusto) ||dtExercicio == null){
			throw new SinedException("Par�metros inv�lidos");
		}
		
//		String mesExercicio = new SimpleDateFormat("MM").format(dtExercicio);
//		String anoExercicio = new SimpleDateFormat("yyyy").format(dtExercicio);
		
		return querySined()
				
				.select("controleorcamentoitem.cdcontroleorcamentoitem, contagerencial.cdcontagerencial, contagerencial.nome, " +
						"contagerencialpai.cdcontagerencial, contagerencialpai.nome, vcontagerencial.cdcontagerencial, " +
						"vcontagerencial.nome, vcontagerencial.arvorepai, controleorcamentoitem.valor, controleorcamentoitem.mesano, " +
						"controleorcamento.tipolancamento, centrocusto.cdcentrocusto, centrocusto.nome ")
				.leftOuterJoin("controleorcamentoitem.centrocusto centrocusto")
				.join("controleorcamentoitem.contagerencial contagerencial")
				.leftOuterJoin("contagerencial.contagerencialpai contagerencialpai")
				.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
				.join("controleorcamentoitem.controleorcamento controleorcamento")
				.leftOuterJoin("controleorcamento.empresa empresa")
				.openParentheses()
					.whereIn("contagerencial.cdcontagerencial", whereInContagerencial)
					.or()
					.whereIn("centrocusto.cdcentrocusto",  whereInCentrocusto)
				.closeParentheses()
				.where("controleorcamento.situacao = ?", Controleorcamentosituacao.APROVADO)
				.where("controleorcamento.dtExercicioInicio <= ?", dtExercicio)
				.where("controleorcamento.dtExercicioFim >= ?", dtExercicio)				
				
//				.where("extract(year from controleorcamento.dtExercicioInicio) >= ?", Integer.parseInt(anoExercicio))
//				.where("extract(year from controleorcamento.dtExercicioFim) <= ?", Integer.parseInt(anoExercicio))
//				.openParentheses()
//					.where("extract(month from controleorcamento.dtExercicioFim) <= ?", Integer.parseInt(mesExercicio))
//						.or()
//					.where("extract(year from controleorcamento.dtExercicioFim) < ?", Integer.parseInt(anoExercicio))
//				.closeParentheses()
//				.openParentheses()
//					.where("extract(month from controleorcamento.dtExercicioInicio) >= ?", Integer.parseInt(mesExercicio))
//						.or()
//					.where("extract(year from controleorcamento.dtExercicioInicio) > ?", Integer.parseInt(anoExercicio))
//				.closeParentheses()
				
				.where(cdempresa != null ? "empresa.cdpessoa = " + cdempresa : "empresa.cdpessoa is null")
				.list();
	}
	
	public List<Controleorcamentoitem> findByControleOrcamento(Controleorcamento controleorcamento) {
		return query()		
			.select("controleorcamentoitem.cdcontroleorcamentoitem, controleorcamentoitem.mesano, controleorcamentoitem.valor, " +
					"controleorcamento.cdcontroleorcamento, orcamentoProjeto.cdprojeto, orcamentoProjetoTipo.cdprojetotipo, " +
					"centrocusto.cdcentrocusto, centrocusto.nome, contagerencial.cdcontagerencial, contagerencial.nome, " +
					"projeto.cdprojeto, projeto.nome, projetotipo.nome, projetotipo.cdprojetotipo")
			.leftOuterJoin("controleorcamentoitem.controleorcamento controleorcamento")
			.leftOuterJoin("controleorcamento.projeto orcamentoProjeto")
			.leftOuterJoin("controleorcamento.projetoTipo orcamentoProjetoTipo")
			.leftOuterJoin("controleorcamentoitem.contagerencial contagerencial")
			.leftOuterJoin("controleorcamentoitem.centrocusto centrocusto")
			.leftOuterJoin("controleorcamentoitem.projeto projeto")
			.leftOuterJoin("controleorcamentoitem.projetoTipo projetotipo")		
			.where("controleorcamento = ?", controleorcamento)
			.list();
	}
	
	/**
	 * M�todo para buscar o valor or�ado da conta gerencial
	 *
	 * @param contagerencial
	 * @param empresa
	 * @param exercicio
	 * @return
	 * @author Luiz Fernando
	 */
	public Controleorcamentoitem findForValorOr�adoByContagerencial(Contagerencial contagerencial, Centrocusto centrocusto, Empresa empresa, Date dtExercicio, Tipooperacao tipooperacao, Projeto projeto) {
		if(contagerencial == null ||contagerencial.getCdcontagerencial() == null || dtExercicio == null){
			throw new SinedException("Par�metros inv�lidos");
		}
		String mesExercicio = new SimpleDateFormat("MM").format(dtExercicio);
		String anoExercicio = new SimpleDateFormat("yyyy").format(dtExercicio);
		QueryBuilder<Controleorcamentoitem> query = query()
				.select("controleorcamentoitem.cdcontroleorcamentoitem, controleorcamentoitem.valor, " +
						"controleorcamento.cdcontroleorcamento, controleorcamento.dtExercicioFim, controleorcamento.dtExercicioInicio, controleorcamento.tipolancamento ")
				.join("controleorcamentoitem.controleorcamento controleorcamento")
				.join("controleorcamentoitem.contagerencial contagerencial")
				.leftOuterJoin("controleorcamento.empresa empresa")
				.leftOuterJoin("controleorcamentoitem.centrocusto centrocusto")
				.leftOuterJoin("controleorcamentoitem.projeto projeto")
				.where("contagerencial.tipooperacao = ?", tipooperacao)
				.where("extract(year from controleorcamento.dtExercicioInicio) <= ?", Integer.parseInt(anoExercicio))
				.where("extract(year from controleorcamento.dtExercicioFim) >= ?", Integer.parseInt(anoExercicio))
				.openParentheses()
					.where("extract(month from controleorcamento.dtExercicioFim) >= ?", Integer.parseInt(mesExercicio))
					.where("extract(year from controleorcamento.dtExercicioFim) >= ?", Integer.parseInt(anoExercicio))
				.closeParentheses()
				.openParentheses()
					.where("extract(month from controleorcamento.dtExercicioInicio) <= ?", Integer.parseInt(mesExercicio))
					.where("extract(year from controleorcamento.dtExercicioInicio) <= ?", Integer.parseInt(anoExercicio))
				.closeParentheses()
				.where("empresa = ?", empresa)
				.where("contagerencial = ?", contagerencial);
		
		if(centrocusto != null && centrocusto.getCdcentrocusto() != null){
			query.openParentheses()
					.where("centrocusto = ?", centrocusto)
					.or()
					.where("centrocusto is null")
				.closeParentheses();
		}
		if (projeto != null && projeto.getCdprojeto() != null){
			query.openParentheses()
					.where("projeto = ?", projeto)
					.or()
					.where("projeto is null")
				.closeParentheses();
		}
		
		return query.unique();
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Controleorcamentoitem> query, FiltroListagem _filtro) {
		ControleorcamentoitemFiltro filtro = (ControleorcamentoitemFiltro) _filtro;
		query.
			select("controleorcamentoitem.cdcontroleorcamentoitem, controleorcamentoitem.mesano, controleorcamentoitem.valor, " +
					"vcontagerencial.nome, vcontagerencial.identificador, projeto.nome, projetoTipo.nome, projetoTipo.cdprojetotipo, centrocusto.nome, " +
					"controleorcamento.descricao")
			.leftOuterJoin("controleorcamentoitem.contagerencial contagerencial")
			.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			.leftOuterJoin("controleorcamentoitem.centrocusto centrocusto")
			.leftOuterJoin("controleorcamentoitem.projeto projeto")
			.leftOuterJoin("controleorcamentoitem.projetoTipo projetoTipo")
			.leftOuterJoin("controleorcamentoitem.controleorcamento controleorcamento")
			.where("contagerencial = ?", filtro.getContraGerencial())
			.where("projeto = ?", filtro.getProjeto())
			.where("centrocusto = ?", filtro.getCentroCusto())
			.where("controleorcamento = ?", filtro.getControleOrcamento())
			.where("controleorcamentoitem.mesano = ?", filtro.getMesAnoDate());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Controleorcamentoitem> query) {
		query.
			select("controleorcamentoitem.cdcontroleorcamentoitem, controleorcamentoitem.mesano, controleorcamentoitem.valor, " +
					"controleorcamento.cdcontroleorcamento, controleorcamento.descricao, controleorcamento.situacao, controleorcamento.tipolancamento, " +
					"orcamentoProjeto.cdprojeto, orcamentoProjetoTipo.cdprojetotipo, " +
					"vcontagerencial.nome,  vcontagerencial.identificador, centrocusto.cdcentrocusto, centrocusto.nome, contagerencial.cdcontagerencial, " +
					"projeto.cdprojeto, projeto.nome, projetotipo.nome, projetotipo.cdprojetotipo")
			.leftOuterJoin("controleorcamentoitem.controleorcamento controleorcamento")
			.leftOuterJoin("controleorcamento.projeto orcamentoProjeto")
			.leftOuterJoin("controleorcamento.projetoTipo orcamentoProjetoTipo")
			.leftOuterJoin("controleorcamentoitem.contagerencial contagerencial")
			.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			.leftOuterJoin("controleorcamentoitem.centrocusto centrocusto")
			.leftOuterJoin("controleorcamentoitem.projeto projeto")
			.leftOuterJoin("controleorcamentoitem.projetoTipo projetotipo");
	}

	public Boolean verificaContaGerencialMensal(Controleorcamentoitem bean) {
		
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
		
			.select("count(*)")
			.from(Controleorcamentoitem.class)
			.join("controleorcamentoitem.contagerencial contagerencial")
			.join("controleorcamentoitem.controleorcamento controleorcamento")
			.leftOuterJoin("controleorcamentoitem.centrocusto centrocusto")
			.leftOuterJoin("controleorcamentoitem.projeto projeto")
			.leftOuterJoin("controleorcamentoitem.projetoTipo projetoTipo")
			.where("controleorcamento = ?", bean.getControleorcamento())
			.where("controleorcamentoitem.mesano = ?", bean.getMesano())
			.where("contagerencial = ?", bean.getContagerencial());
			
			if(bean.getCdcontroleorcamentoitem() != null){
				query
				.where("controleorcamentoitem <> ?", bean);				
			}
			
			if(bean.getCentrocusto() != null){
				query
				.where("centrocusto = ?", bean.getCentrocusto());
			}else {
				query
				.where("centrocusto is null");
			}
			if(bean.getProjeto() != null){
				query
				.where("projeto = ?", bean.getProjeto());
			}else {
				query
				.where("projeto is null");
			}
			if(bean.getProjetoTipo() != null){
				query
				.where("projetoTipo = ?", bean.getProjetoTipo());
			}else {
				query
				.where("projetoTipo is null");
			}
			
			return query.unique() > 0;
	}

	public Boolean verificaContaGerencialAnual(Controleorcamentoitem bean) {
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)				
				.select("count(*)")
				.from(Controleorcamentoitem.class)
				.join("controleorcamentoitem.contagerencial contagerencial")
				.join("controleorcamentoitem.controleorcamento controleorcamento")
				.leftOuterJoin("controleorcamentoitem.centrocusto centrocusto")
				.leftOuterJoin("controleorcamentoitem.projeto projeto")
				.leftOuterJoin("controleorcamentoitem.projetoTipo projetoTipo")
				.where("controleorcamento = ?", bean.getControleorcamento())
				.where("contagerencial = ?", bean.getContagerencial());
				
		if(bean.getCdcontroleorcamentoitem() != null){
			query
			.where("controleorcamentoitem <> ?", bean);
		}
		
		if(bean.getCentrocusto() != null) {
			query.
			where("centrocusto = ?", bean.getCentrocusto());
		}else {
			query
			.where("centrocusto is null");
		}
		if(bean.getProjeto() != null){
			query
			.where("projeto = ?", bean.getProjeto());
		}else {
			query
			.where("projeto is null");
		}
		if(bean.getProjetoTipo() != null){
			query
			.where("projetoTipo = ?", bean.getProjetoTipo());
		}else {
			query
			.where("projetoTipo is null");
		}
		
		return query.unique() > 0;
	}
	
}
