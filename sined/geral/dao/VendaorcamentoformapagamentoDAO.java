package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentoformapagamento;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VendaorcamentoformapagamentoDAO extends GenericDAO<Vendaorcamentoformapagamento> {

	/**
	 * Busca os valores da forma de pagamento a partir da vendaorcamento.
	 *
	 * @param vendaorcamento
	 * @return
	 * @since 27/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Vendaorcamentoformapagamento> findByVendaorcamento(Vendaorcamento vendaorcamento) {
		if(vendaorcamento == null || vendaorcamento.getCdvendaorcamento() == null){
			throw new SinedException("Vendaorcamento n�o pode ser nula.");
		}
		return query()
					.select("vendaorcamentoformapagamento.cdvendaorcamentoformapagamento, vendaorcamentoformapagamento.valorparcela, " +
							"vendaorcamentoformapagamento.valortotal, vendaorcamentoformapagamento.prazomedio, " +
							"vendaorcamentoformapagamento.qtdeparcelas, prazopagamento.cdprazopagamento, " +
							"vendaorcamentoformapagamento.dtparcela, prazopagamento.nome")
					.join("vendaorcamentoformapagamento.vendaorcamento vendaorcamento")
					.join("vendaorcamentoformapagamento.prazopagamento prazopagamento")
					.where("vendaorcamento = ?", vendaorcamento)
					.list();
	}

}
