package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Declaracaoimportacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DeclaracaoimportacaoDAO extends GenericDAO<Declaracaoimportacao> {
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaDeclaracaoimportacaoadicao");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Declaracaoimportacao> query) {
		query
			.leftOuterJoinFetch("declaracaoimportacao.listaDeclaracaoimportacaoadicao listaDeclaracaoimportacaoadicao")
			.leftOuterJoinFetch("declaracaoimportacao.uf uf")
			.leftOuterJoinFetch("declaracaoimportacao.ufadquirente ufadquirente")
			;
	}

	/**
	 * Consulta para exibir na tela de NF de produto.
	 *
	 * @param declaracaoimportacao
	 * @return
	 * @since 11/09/2012
	 * @author Rodrigo Freitas
	 */
	public Declaracaoimportacao loadForConsulta(Declaracaoimportacao declaracaoimportacao) {
		if(declaracaoimportacao == null || declaracaoimportacao.getCddeclaracaoimportacao() == null){
			throw new SinedException("Declara��o n�o pode ser nula.");
		}
		return query()
					.leftOuterJoinFetch("declaracaoimportacao.uf uf")
					.where("declaracaoimportacao = ?", declaracaoimportacao)
					.unique();
	}
	
}
