package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Emporiumvenda;
import br.com.linkcom.sined.geral.bean.Emporiumvendaitem;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EmporiumvendaitemDAO extends GenericDAO<Emporiumvendaitem>{

	public List<Emporiumvendaitem> findByEmporiumvenda(Emporiumvenda emporiumvenda) {
		return query()
					.where("emporiumvendaitem.emporiumvenda = ?", emporiumvenda)
					.list();
	}

}
