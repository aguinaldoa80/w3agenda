
package br.com.linkcom.sined.geral.dao;

import java.util.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.ContratosincronizacaoAA;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContratosincronizacaoAADAO extends GenericDAO<ContratosincronizacaoAA> {

	public List<ContratosincronizacaoAA> findNaoSincronizados() {
		return query()
		.select("cliente.cdpessoa, contratosincronizacaoAA.id, contratosincronizacaoAA.cdcontratosincronizacaoaa")
		.join("contratosincronizacaoAA.cliente cliente")
//		.where("cliente.cdpessoa = 185")
		.where("contratosincronizacaoAA.dtsincronizacao is null")
		.list();
	}
	
	public void updateSincronizacao(Integer cdempresa, Integer id){
		if(cdempresa == null || id == null)
			throw new RuntimeException("Os campos cdcliente e id n�o podem ser nulos.");
		getJdbcTemplate().update("UPDATE contratosincronizacaoaa SET id = ?, dtsincronizacao = ? WHERE cdcliente = ?", new Object[]{id, new Date(), cdempresa});
	}
	
	public void updateError(Integer cdempresa, String mensagem){
		if(cdempresa == null )
			throw new RuntimeException("O campo cdcliente n�o pode ser nulo.");
		
		getJdbcTemplate().update("UPDATE contratosincronizacaoaa SET mensagem = ? WHERE cdcliente = ?", new Object[] {mensagem, cdempresa});
	}
	
	public ContratosincronizacaoAA findByCdCliente(Integer cdcliente) {
		if(cdcliente == null )
			throw new RuntimeException("O campo cdcliente n�o pode ser nulo.");
		
		return query()
		.select("cliente.cdpessoa, contratosincronizacaoAA.id, contratosincronizacaoAA.cdcontratosincronizacaoaa, contratosincronizacaoAA.mensagem, " +
				"contratosincronizacaoAA.dtsincronizacao")
		.join("contratosincronizacaoAA.cliente cliente")
		.where("cliente.cdpessoa = ?", cdcliente)
		.where("contratosincronizacaoAA.dtsincronizacao is null")
		.unique();
	}

}