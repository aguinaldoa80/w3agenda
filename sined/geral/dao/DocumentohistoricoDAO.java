package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("documentohistorico.dtaltera desc")
public class DocumentohistoricoDAO extends GenericDAO<Documentohistorico>{

	
	@Override
	public void updateEntradaQuery(QueryBuilder<Documentohistorico> query) {
		query.leftOuterJoinFetch("documentohistorico.documentoacao documentoacao");
		query.leftOuterJoinFetch("documentohistorico.documento documento");
	}
	
	/**
	 * M�todo para obter os dados do historico de uma conta para visualiza��o.
	 * 
	 * @param documentohistorico
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Documentohistorico findForVisualizacao(Documentohistorico documentohistorico){
		if(documentohistorico == null || documentohistorico.getCddocumentohistorico() == null){
			throw new SinedException("A propriedade cddocumentohistorico em documentohistorico n�o pode ser null.");
		}
		return 
			query()
			.select("documentoacao.nome,documentotipo.nome,documentohistorico.descricao,documentohistorico.numero," +
					"pessoa.cdpessoa,pessoa.nome,documentohistorico.dtemissao,documentohistorico.dtvencimento," +
					"documentohistorico.valor,documentohistorico.observacao,documentohistorico.cdusuarioaltera,documentohistorico.dtaltera," +
					"documentohistorico.tipopagamento, documentohistorico.outrospagamento")
			
			.leftOuterJoin("documentohistorico.documentoacao documentoacao")
			.leftOuterJoin("documentohistorico.documentotipo documentotipo")
			.leftOuterJoin("documentohistorico.pessoa pessoa")
			
			.where("documentohistorico = ?", documentohistorico)		
			.unique();
	}
	
	/**
	 * Fornece o hist�rico de um documento ordenado de forma com que as �ltimas
	 * a��es apare�am primeiro.
	 * 
	 * @param agendamento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documentohistorico> carregarHistorico(Documento documento) {
		if(documento == null || documento.getCddocumento() == null){
			throw new SinedException("Os par�metros documento ou cddocumento n�o podem ser null.");
		}
		return querySined()
				
			.select("documentohistorico.cddocumentohistorico,documentohistorico.dtaltera,documentohistorico.cdusuarioaltera," +
					"documentohistorico.observacao, documentoacao.cddocumentoacao,documentoacao.nome")
			.join("documentohistorico.documentoacao documentoacao")
			.where("documentohistorico.documento = ?", documento)
			.orderBy("documentohistorico.cddocumentohistorico DESC")
			.list();
	}

	/**
	 * M�todo que busca a pen�ltima Documentoacao de um documento diferente de 'PROTESTADA'
	 * 
	 * 
	 * @param documento
	 * @return
	 * @author Rodrigo Freitas
	 * @author Fl�vio Tavares - Altera��o do retorno para Documentohistorico, para evitar NullPointer
	 */
	public Documentohistorico findPenultimaAcao(Documento documento) {
		if (documento == null || documento.getCddocumento() == null) {
			throw new SinedException("Documento n�o pode ser nulo");
		}
		
		return query()
					.select("documentoacao.cddocumentoacao, documentoacao.nome")
					.leftOuterJoin("documentohistorico.documentoacao documentoacao")
					.setPageNumberAndSize(0, 1)
					.openParentheses()
					.where("documentoacao = ?",Documentoacao.AUTORIZADA)
					.or()
					.where("documentoacao = ?",Documentoacao.AUTORIZADA_PARCIAL)
					.closeParentheses()
					.where("documentohistorico.documento = ?", documento)
					.unique();
	}

	
	/**
	 * M�todo que verifica se j� foi enviado o boleto desse documento
	 * 
	 * 
	 * @param documento
	 * @return
	 * @author Lucas Costa
	 */
	public Boolean possuiBoletoEnviado (Documento documento) {
		if(documento == null || documento.getCddocumento() == null){
			throw new SinedException("Os par�metros documento ou cddocumento n�o podem ser null.");
		}
		
		return newQueryBuilder(Long.class)
		.select("count(*)")
		.from(Documentohistorico.class)
		.join("documentohistorico.documentoacao documentoacao")
		.where("documentohistorico.documento = ?", documento)
		.where("documentohistorico.documentoacao = ?", Documentoacao.ENVIO_BOLETO)
		.unique() > 0;
		
	}

	/**
	 * Atualiza a observa��o do historico de acordo com a observa��o 
	 * preenchida na tela de conta a receber
	 * @param documento
	 * @param observacaoHistorico
	 */
	public void updateObservacaoDocumento(Documento documento, String observacaoHistorico) {
		if(documento == null || documento.getCddocumento() == null){
			throw new SinedException("Os par�metros documento ou cddocumento n�o podem ser null.");
		}
		
		String sql = null;
		Object[] objetos = null;
		sql = "update Documentohistorico dh set dh.observacao = ? where dh.cddocumentohistorico = (select MAX(dh2.cddocumentohistorico) from Documentohistorico dh2 where dh2.documentoacao in (" 
			+ Documentoacao.ALTERADA.getCddocumentoacao() + "," +  Documentoacao.ALTERADA_PELO_USUARIO.getCddocumentoacao() + ") and dh2.documento = ?)";
		objetos = new Object[] {observacaoHistorico, documento};
		getHibernateTemplate().bulkUpdate(sql, objetos);
	}
	
	/**
	* M�todo que busca os hist�ricos dos documentos
	*
	* @param whereIn
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<Documentohistorico> findForAndroid(String whereIn) {
		return query()
			.select("documentohistorico.cddocumentohistorico, documentohistorico.observacao, documentohistorico.dtaltera, " +
					"documento.cddocumento, usuarioaltera.nome ")
			.leftOuterJoin("documentohistorico.documento documento")
			.leftOuterJoin("documentohistorico.usuarioaltera usuarioaltera ")
			.whereIn("documentohistorico.cddocumentohistorico", whereIn)
			.list();
	}
	
	public Documentohistorico loadUltimoHistorico(Documento documento) {
		if(documento == null || documento.getCddocumento() == null){
			throw new SinedException("Os par�metros documento ou cddocumento n�o podem ser null.");
		}
		return query()
			.select("documentohistorico.cddocumentohistorico,documentohistorico.dtaltera,documentohistorico.cdusuarioaltera," +
					"documentohistorico.observacao, documentoacao.cddocumentoacao,documentoacao.nome")
			.join("documentohistorico.documentoacao documentoacao")
			.where("documentohistorico.documento = ?", documento)
			.orderBy("documentohistorico.cddocumentohistorico DESC")
			.setMaxResults(1)
			.unique();
	}
	
	public void updateObservacaoDocumento(Documentohistorico documentohistorico, String observacaoHistorico) {
		if(documentohistorico == null || documentohistorico.getCddocumentohistorico() == null || observacaoHistorico == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		String sql = null;
		Object[] objetos = null;
		sql = "update Documentohistorico dh set dh.observacao = ? where dh.cddocumentohistorico = ?)";
		objetos = new Object[] {observacaoHistorico, documentohistorico.getCddocumentohistorico()};
		getHibernateTemplate().bulkUpdate(sql, objetos);
	}
}
