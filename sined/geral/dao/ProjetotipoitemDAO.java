package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Projetotipo;
import br.com.linkcom.sined.geral.bean.Projetotipoitem;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProjetotipoitemDAO extends GenericDAO<Projetotipoitem>{

	/**
	 * Busca os itens a partir do tipo
	 *
	 * @param projetotipo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/06/2013
	 */
	public List<Projetotipoitem> findByProjetotipo(Projetotipo projetotipo) {
		return query()
					.select("projetotipoitem.cdprojetotipoitem, projetotipoitem.nome, projetotipoitem.obrigatorio")
					.join("projetotipoitem.projetotipo projetotipo")
					.where("projetotipo = ?", projetotipo)
					.list();
	}

}
