package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratocolaborador;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContratocolaboradorDAO extends GenericDAO<Contratocolaborador> {

	/**
	 * M�todo que carrega a lista de colaboradores do contrato
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 07/04/2014
	 */
	public List<Contratocolaborador> findForEntradaByContrato(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Erro na passagem de par�metro.");
			
		QueryBuilder<Contratocolaborador> query = query()
				.select("contratocolaborador.cdcontratocolaborador, contratocolaborador.dtinicio, contratocolaborador.dtfim, contratocolaborador.ordemcomissao, " +
					"contratocolaborador.valorhora, colaborador.cdpessoa, colaborador.nome, " +
					"escala.cdescala, " +
					"contratocolaborador.horainicio, contratocolaborador.horafim, " +
					"comissao.cdcomissionamento, comissao.nome, comissao.valor, comissao.percentual, comissao.quantidade ")
				.join("contratocolaborador.contrato contrato")
				.leftOuterJoin("contratocolaborador.colaborador colaborador")
				.leftOuterJoin("contratocolaborador.escala escala")
				.leftOuterJoin("contratocolaborador.comissionamento comissao");
		
		SinedUtil.quebraWhereIn("contrato.cdcontrato", whereIn, query);
		return query.list();
	}
	
	/**
	 * Carrega todos os <code>Contratocolaborador</code> de um 
	 * <code>Contrato</code> passado por par�metro.
	 *
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contratocolaborador> findByContrato(Contrato contrato) {
		if(contrato == null || contrato.getCdcontrato() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.joinFetch("contratocolaborador.contrato contrato")
					.joinFetch("contratocolaborador.colaborador colaborador")
					.where("contrato = ?", contrato)
					.list();
	}

	/**
	 * Carrega os registros de <code>Contratocolaborador</code> a partir do colaborador.
	 *
	 * @param colaborador
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contratocolaborador> findByColaboradorContrato(Colaborador colaborador, Contrato contrato) {
		if(colaborador == null || colaborador.getCdpessoa() == null){
			throw new SinedException("Colaborador n�o pode ser nulo.");
		}
		return query()
					.select("contratocolaborador.cdcontratocolaborador, contratocolaborador.dtinicio, contratocolaborador.dtfim, contratocolaborador.horainicio, contratocolaborador.horafim," +
							"escala.cdescala, escala.diastrabalho, escala.diasfolga, escala.naotrabalhaferiado, escala.naotrabalhadomingo, " +
							"colaborador.cdpessoa, contrato.cdcontrato, empresa.cdpessoa")
					.join("contratocolaborador.escala escala")
					.join("contratocolaborador.colaborador colaborador")
					.join("contratocolaborador.contrato contrato")
					.leftOuterJoin("contrato.empresa empresa")
					.where("contratocolaborador.colaborador = ?", colaborador)
					.where("contratocolaborador.contrato = ?", contrato)
					.list();
	}

	/**
	 * M�todo que retorna os colaboradores cadastrados no contrato
	 * 
	 * @param whereIn
	 * @param whereInColaborador
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Contratocolaborador> findColaboradorNoContrato(String whereIn, String whereInColaborador) {
		if(whereIn == null || whereIn.equals("") || whereInColaborador == null || whereInColaborador.equals(""))
			throw new SinedException("Par�metros inv�lidos.");

		return query()
			.select("contratocolaborador.cdcontratocolaborador, contrato.cdcontrato, colaborador.cdpessoa, colaborador.nome")
			.join("contratocolaborador.contrato contrato")
			.join("contratocolaborador.colaborador colaborador")
			.whereIn("colaborador.id", whereInColaborador)
			.whereIn("contrato.id", whereIn)
			.list();
	}
	
	/**
	 * M�todo que busca contratocolaborador para calcular a comiss�o 
	 * 
	 * @param contrato
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contratocolaborador> findContratocolaboradorByContrato(Contrato contrato) {
		if(contrato == null || contrato.getCdcontrato() == null)
			throw new SinedException("Par�metros inv�lidos.");

		return query()
			.select("contratocolaborador.cdcontratocolaborador, colaborador.cdpessoa, contratocolaborador.ordemcomissao, " +
					"comissionamento.cdcomissionamento, comissionamento.valor, comissionamento.percentual, colaborador.cdpessoa, " +
					"comissionamento.deduzirvalor, contratocolaborador.valorhora")
			.join("contratocolaborador.contrato contrato")
			.join("contratocolaborador.colaborador colaborador")
			.join("contratocolaborador.comissionamento comissionamento")
			.where("contrato = ?", contrato)
			.orderBy("contratocolaborador.ordemcomissao")
			.list();
	}

}
