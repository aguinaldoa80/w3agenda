package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Area;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.AreaFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("retira_acento(upper(area.descricao))")
public class AreaDAO extends GenericDAO<Area> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Area> query, FiltroListagem _filtro) {
		AreaFiltro filtro = (AreaFiltro) _filtro;
		query
			.select("area.cdarea, area.descricao, area.ativo, projeto.nome")
			.leftOuterJoin("area.projeto projeto")
			.whereLikeIgnoreAll("area.descricao", filtro.getDescricao())
			.where("projeto = ?",filtro.getProjeto())
			.where("area.ativo = ?",filtro.getAtivo())
			.orderBy("area.descricao");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Area> query) {
		query
			.select("area.cdarea, area.descricao, area.ativo, projeto.cdprojeto, area.cdusuarioaltera, area.dtaltera, " +
					"areatipo.cdareatipo, areatipo.descricao, areaquestionario.cdareaquestionario, areaquestionario.ordem, " +
					"areaquestionario.pergunta, areaquestionario.tiporesposta, contagerencial.cdcontagerencial, contagerencial.nome, " +
					"vcontagerencial.identificador, vcontagerencial.arvorepai, centrocusto.cdcentrocusto")
			.leftOuterJoin("area.centrocusto centrocusto")
			.leftOuterJoin("area.contagerencial contagerencial")
			.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			.leftOuterJoin("area.projeto projeto")
			.leftOuterJoin("area.listaAreatipo areatipo")
			.leftOuterJoin("area.listaAreaquestionario areaquestionario")
			.orderBy("areaquestionario.ordem");
	}
	
	/**
	 * Carrega a lista de �rea para exibir na parte em flex.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Area> findAllForFlex(){
		return query()
					.select("area.cdarea, area.descricao")
					.orderBy("area.descricao")
					.list();
	}
	
	public List<Area> verificaAreaProjeto(Area area){
		return query()
		.select("area.descricao, projeto.cdprojeto")
		.leftOuterJoin("area.projeto projeto")
		.where("area.cdarea <> ?", area.getCdarea())
		.list();
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaAreatipo");
		save.saveOrUpdateManaged("listaAreaquestionario");
	}
	
}
