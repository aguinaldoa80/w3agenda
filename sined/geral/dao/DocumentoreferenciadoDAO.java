package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Documentoreferenciado;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.Documentoreferenciadoimposto;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.DocumentoreferenciadoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentoreferenciadoDAO extends GenericDAO<Documentoreferenciado> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Documentoreferenciado> query,FiltroListagem _filtro) {
		DocumentoreferenciadoFiltro filtro = (DocumentoreferenciadoFiltro) _filtro;
		
		query
			.select("documentoreferenciado.cddocumentoreferenciado, documentoreferenciado.nome, documentoreferenciado.modelodocumento, documentotipo.nome, fornecedor.nome ")
			.leftOuterJoin("documentoreferenciado.documentotipo documentotipo")
			.leftOuterJoin("documentoreferenciado.fornecedor fornecedor")
			.whereLikeIgnoreAll("documentoreferenciado.nome", filtro.getNome())
			.where("documentoreferenciado.empresa = ?", filtro.getEmpresa())
			.where("documentoreferenciado.modelodocumento = ?", filtro.getModelodocumento())
			.where("fornecedor = ?", filtro.getFornecedor())
			.where("documentoreferenciado.contagerencial = ?", filtro.getContagerencial())
			.where("documentoreferenciado.centrocusto = ?", filtro.getCentrocusto())
			.where("documentoreferenciado.projeto = ?", filtro.getProjeto())
			.orderBy("documentoreferenciado.nome");
		;
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Documentoreferenciado> query) {
		query
			.select("documentoreferenciado.cddocumentoreferenciado, documentoreferenciado.nome, documentoreferenciado.modelodocumento, empresa.cdpessoa, " +
					"documentoreferenciado.diavencimento, documentoreferenciado.imposto, " +
					"documentotipo.cddocumentotipo, documentotipo.nome, " +
					"fornecedor.cdpessoa, fornecedor.nome, fornecedor.cpf, fornecedor.cnpj, " +
					"contagerencial.cdcontagerencial, contagerencial.nome," +
					"centrocusto.cdcentrocusto, centrocusto.nome," +
					"projeto.cdprojeto, projeto.nome ")
			.leftOuterJoin("documentoreferenciado.empresa empresa")
			.leftOuterJoin("documentoreferenciado.documentotipo documentotipo")
			.leftOuterJoin("documentoreferenciado.fornecedor fornecedor")
			.leftOuterJoin("documentoreferenciado.contagerencial contagerencial")
			.leftOuterJoin("documentoreferenciado.centrocusto centrocusto")
			.leftOuterJoin("documentoreferenciado.projeto projeto");
	}

	public List<Documentoreferenciado> findByImposto(List<Documentoreferenciadoimposto> listaDocumentoreferenciadoimpostos, Empresa empresa) {
		if(SinedUtil.isListEmpty(listaDocumentoreferenciadoimpostos)){
			return new ArrayList<Documentoreferenciado>();
		}
			
		QueryBuilder<Documentoreferenciado> query = query();
		updateEntradaQuery(query);
		
		return query
				.whereIn("documentoreferenciado.imposto", Documentoreferenciadoimposto.listAndConcatenate(listaDocumentoreferenciadoimpostos))
				.where("documentoreferenciado.empresa = ?", empresa)
				.list();
	}

	public boolean existeEmpresaImposto(Documentoreferenciado documentoreferenciado) {
		if(documentoreferenciado == null || documentoreferenciado.getEmpresa() == null || documentoreferenciado.getImposto() == null)
			throw new SinedException("Par�metro inv�lido");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Documentoreferenciado.class)
			.where("documentoreferenciado.empresa = ?", documentoreferenciado.getEmpresa())
			.where("documentoreferenciado.imposto = ?", documentoreferenciado.getImposto())
			.where("documentoreferenciado.cddocumentoreferenciado <> ?", documentoreferenciado.getCddocumentoreferenciado())
			.unique() > 0;
	}
}
