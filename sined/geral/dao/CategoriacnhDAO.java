package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Categoriacnh;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CategoriacnhDAO extends GenericDAO<Categoriacnh> {
	
	/* singleton */
	private static CategoriacnhDAO instance;
	public static CategoriacnhDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(CategoriacnhDAO.class);
		}
		return instance;
	}
		
}
