package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Dominio;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.DominioFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DominioDAO extends GenericDAO<Dominio>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Dominio> query, FiltroListagem _filtro) {
		DominioFiltro filtro = (DominioFiltro)_filtro;
		query.leftOuterJoinFetch("dominio.dominioprincipal dominioprincipal")
		 	 .leftOuterJoinFetch("dominio.contratomaterial contratomaterial")
		 	 .leftOuterJoinFetch("contratomaterial.contrato contrato")		 	 
		 	 .where("dominio.dominioprincipal = ?", filtro.getDominioprincipal())
			 .where("dominio.contratomaterial = ?", filtro.getContratomaterial())
			 .where("contrato.cliente = ?", filtro.getCliente())
			 .whereLikeIgnoreAll("dominio.nome", filtro.getNome());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Dominio> query) {
		query.leftOuterJoinFetch("dominio.dominioprincipal dominioprincipal")
		 	 .leftOuterJoinFetch("dominio.contratomaterial contratomaterial")
		 	 .leftOuterJoinFetch("contratomaterial.contrato contrato")
		 	 .leftOuterJoinFetch("dominio.listaEmailalias listaEmailalias")
		 	 .leftOuterJoinFetch("dominio.listaDominiozona listaDominiozona")
		 	 .leftOuterJoinFetch("listaDominiozona.servicoservidor servicoservidor")
		 	 .leftOuterJoinFetch("servicoservidor.servidor servidor");
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaDominiozona");
		save.saveOrUpdateManaged("listaEmailalias");
	}

	/**
	 * Retorna uma lista de dom�nios de acordo com o contrato especificado
	 * @param contrato
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Dominio> findByContrato(Contrato contrato){
		return query().leftOuterJoin("dominio.contratomaterial contratomaterial")
					  .where("contratomaterial.contrato = ?", contrato)
					  .list();
	}
	
	/**
	 * Retorna lista com os dom�nios que podem ser utilizados em e-mail
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Dominio> listEmail(Cliente cliente){
		return query().leftOuterJoin("dominio.contratomaterial contratomaterial")
					  .leftOuterJoin("contratomaterial.contrato contrato")
					  .where("dominio.email = ?", Boolean.TRUE)
					  .openParentheses()
					  	.where("dominio.publico = ?", Boolean.TRUE)
					  	.or()
					  	.where("contrato.cliente = ?", cliente)
					  .closeParentheses()					  
					  .list();
	}

	public int countContratomaterial(Integer cddominio, Contratomaterial contratomaterial) {
		return newQueryBuilder(Long.class)
						.select("count(*)")
						.from(Dominio.class)
						.where("dominio.cddominio <> ?", cddominio)
						.where("dominio.contratomaterial = ?", contratomaterial)
						.unique()
						.intValue();
	}
	
}
