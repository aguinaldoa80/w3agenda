package br.com.linkcom.sined.geral.dao;


import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.view.Vwsaidafiscalimposto;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VwsaidafiscalimpostoDAO extends GenericDAO<Vwsaidafiscalimposto> {

	public List<Vwsaidafiscalimposto> findVwsaidafiscalimpostoByCdnota(String whereInCdnota) {
		if (whereInCdnota==null || whereInCdnota.trim().equals("")){
			return null;
		}
		
		QueryBuilder<Vwsaidafiscalimposto> queryBuilder = query();
		
		SinedUtil.quebraWhereIn("cdnota", whereInCdnota, queryBuilder);
		
		return queryBuilder.list();
	}	
}
