package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Veiculocombustivel;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculotipomarcacorcombustivelFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("veiculocombustivel.descricao")
public class VeiculocombustivelDAO extends GenericDAO<Veiculocombustivel> {

	@Override
	public void updateListagemQuery(QueryBuilder<Veiculocombustivel> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inválidos");
		}
		VeiculotipomarcacorcombustivelFiltro filtro = (VeiculotipomarcacorcombustivelFiltro) _filtro;
		query
			.select("veiculocombustivel.cdveiculocombustivel, veiculocombustivel.descricao")
			.whereLikeIgnoreAll("veiculocombustivel.descricao", filtro.getDescricao());
	}
	
	
}
