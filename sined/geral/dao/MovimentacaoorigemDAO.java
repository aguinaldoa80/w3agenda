package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MovimentacaoorigemDAO extends GenericDAO<Movimentacaoorigem>{

	/**
	 * M�todo para obter lista de <code>Movimentacaoorigem</code> pela <code>Movimentacao</code>.
	 * 
	 * @param movimentacao
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Movimentacaoorigem> findByMovimentacao(Movimentacao movimentacao){
		if(movimentacao == null || movimentacao.getCdmovimentacao() == null){
			throw new SinedException("Os par�metros movimentacao ou cdmovimentacao n�o podem ser null.");
		}
		
		return
			query()
			.select("movimentacaoorigem.cdmovimentacaoorigem, movimentacaoorigem.movimentacao, movimentacaoorigem.cdusuarioaltera," +
					"movimentacaoorigem.dtaltera, movimentacaoorigem.agendamento,movimentacaoorigem.conta," +
					"documento.cddocumento, aux_documento.valoratual, documento.tipopagamento, pessoaDoc.cdpessoa," +
					"empresa.cdpessoa, documentoclasse.cddocumentoclasse, " +
					"agendamento.cdagendamento, " +
					"conta.cdconta, " +
					"despesaviagemadiantamento.cddespesaviagem, despesaviagemadiantamento.descricao, " +
					"despesaviagemacerto.cddespesaviagem, despesaviagemacerto.descricao, " +
					"movimentacaodevolucao.cdmovimentacao, movimentacaorelacionada.cdmovimentacao," +
					"movimentacaorelacionadacartaocredito.cdmovimentacao ")
			
			.join("movimentacaoorigem.movimentacao movimentacao")
			.leftOuterJoin("movimentacaoorigem.movimentacaodevolucao movimentacaodevolucao")
			.leftOuterJoin("movimentacaoorigem.movimentacaorelacionada movimentacaorelacionada")
			.leftOuterJoin("movimentacaoorigem.documento documento")
			.leftOuterJoin("documento.documentoclasse documentoclasse")
			.leftOuterJoin("documento.empresa empresa")
			.leftOuterJoin("documento.pessoa pessoaDoc")
			.leftOuterJoin("documento.aux_documento aux_documento")
			.leftOuterJoin("movimentacaoorigem.agendamento agendamento")
			.leftOuterJoin("movimentacaoorigem.conta conta")
			.leftOuterJoin("movimentacaoorigem.despesaviagemadiantamento despesaviagemadiantamento")
			.leftOuterJoin("movimentacaoorigem.despesaviagemacerto despesaviagemacerto")
			.leftOuterJoin("movimentacaoorigem.movimentacaorelacionadacartaocredito movimentacaorelacionadacartaocredito")
			.where("movimentacao = ?",movimentacao)
			.list();
	}

	/**
	 * Deleta a movimentacao origem do documento.
	 *
	 * @param documento
	 * @author Rodrigo Freitas
	 */
	public void deleteFromDocumento(Documento documento) {
		if (documento == null || documento.getCddocumento() == null) {
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("DELETE FROM MOVIMENTACAOORIGEM M WHERE M.CDDOCUMENTO = "+documento.getCddocumento());
	}

	/**
	 * 
	 * @param movimentacao
	 * @return
	 */
	public List<Movimentacaoorigem> somaDosDocumentos(Movimentacao movimentacao){
		
		return query()
			.joinFetch("movimentacaoorigem.documento documento")
			.where("movimentacaoorigem.movimentacao = ?",movimentacao)
		.list();
	}

	/**
	 * 
	 * @param whereIn
	 * @return
	 */
	public List<Movimentacaoorigem> findByAgendamento(List<Agendamento> listaAgendamento) {
		return query()
			.select("movimentacaoorigem.cdmovimentacaoorigem, movimentacaoacao.cdmovimentacaoacao, movimentacaoacao.nome, agendamento.cdagendamento, " +
					"movimentacao.cdmovimentacao")
			.join("movimentacaoorigem.movimentacao movimentacao")
			.join("movimentacaoorigem.agendamento agendamento")
			.join("movimentacao.movimentacaoacao movimentacaoacao")
			.whereIn("agendamento.cdagendamento", SinedUtil.listAndConcatenate(listaAgendamento, "cdagendamento", ","))
			.list();
	}
	
	/**
	* M�todo que carrega a origem da movimenta��o para substitui��o de cheque
	*
	* @param whereIn
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public List<Movimentacaoorigem> findForSubstituicaoByMovimentacao(String whereIn){
		if(StringUtils.isEmpty(whereIn))
			return new ArrayList<Movimentacaoorigem>();
		
		return
			query()
			.select("movimentacaoorigem.cdmovimentacaoorigem, movimentacao.cdmovimentacao, " +
					"agendamento.cdagendamento, conta.cdconta, documento.cddocumento," +
					"despesaviagemadiantamento.cddespesaviagem, despesaviagemacerto.cddespesaviagem ")
			.join("movimentacaoorigem.movimentacao movimentacao")
			.leftOuterJoin("movimentacaoorigem.documento documento")
			.leftOuterJoin("movimentacaoorigem.agendamento agendamento")
			.leftOuterJoin("movimentacaoorigem.conta conta")
			.leftOuterJoin("movimentacaoorigem.despesaviagemadiantamento despesaviagemadiantamento")
			.leftOuterJoin("movimentacaoorigem.despesaviagemacerto despesaviagemacerto")
			.whereIn("movimentacao.cdmovimentacao", whereIn)
			.orderBy("movimentacao.cdmovimentacao")
			.list();
	}
	
	/**
	* M�todo que retorna true caso o documento tenha mais de uma movimenta��o financeira com origem de mais de um documento
	*
	* @param whereIn
	* @param incluiBaixadaParcial
	* @return
	* @since 15/06/2016
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public boolean existeOrigemVariosDocumentos(String whereIn, boolean incluiFiltroBaixadaParcial){
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Par�metro inv�lido");
		}
		
		String sql = 
			" select mo.cdmovimentacao as cdmovimentacao, count(mo.cddocumento) " +
			" from movimentacaoorigem mo " +
			" join documento d on d.cddocumento = mo.cddocumento " +
			" join movimentacao m on m.cdmovimentacao = mo.cdmovimentacao " +
			" where d.cddocumento in (select mo2.cddocumento from movimentacaoorigem mo2 where mo2.cdmovimentacao in (" + whereIn + ")) " +
			" and d.cddocumentoclasse = " + Documentoclasse.CD_RECEBER +
			(incluiFiltroBaixadaParcial ? " and d.cddocumentoacao <> " + Documentoacao.BAIXADA_PARCIAL.getCddocumentoacao() : "") + 
			" and m.cdmovimentacaoacao <> " + Movimentacaoacao.CANCELADA.getCdmovimentacaoacao() +
			" and mo.cdmovimentacao not in (" + whereIn + ")" +
			" group by mo.cdmovimentacao " +
			" having count(mo.cddocumento) > 1 ";

		SinedUtil.markAsReader();
		List<Movimentacao> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Movimentacao(rs.getInt("cdmovimentacao"));
			}
		});
		
		return list != null && list.size() > 0;
	}
	
	public void updateMovimentacaorelacionada(Movimentacao movimentacao, Documento documento, Movimentacao movimentacaorelacionada){
		getJdbcTemplate().execute("update Movimentacaoorigem set cdmovimentacaorelacionada = "+movimentacaorelacionada.getCdmovimentacao().toString()+" where cdmovimentacao = "+movimentacao.getCdmovimentacao().toString()+" and cddocumento = "+documento.getCddocumento().toString());	}



	public List<Movimentacaoorigem> buscarMovimentacaoDocumento(Documento doc){
		
		return query()
				.select("movimentacaoorigem.cdmovimentacaoorigem,documento.cddocumento,movimentacao.cdmovimentacao," +
						"movimentacao.valor,origem.cdmovimentacaoorigem, formapagamento.cdformapagamento,formapagamento.nome")
				.join("movimentacaoorigem.documento documento")
				.join("movimentacaoorigem.movimentacao movimentacao")
				.join("movimentacao.movimentacaoacao movimentacaoacao")
				.join("movimentacao.listaMovimentacaoorigem origem")
				.join("movimentacao.formapagamento formapagamento")
				.where("movimentacaoacao <> ?",Movimentacaoacao.CANCELADA)
				.where("documento.cddocumento = ?", doc.getCddocumento())
				.list();
		
	}


}
