package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Leadtelefone;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class LeadtelefoneDAO extends GenericDAO<Leadtelefone>{
	
	public List<Leadtelefone> findByLead(Lead lead) {
		if (lead == null || lead.getCdlead() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return 
			query()
				.select("leadtelefone.cdleadtelefone, leadtelefone.telefone, " +
						"telefonetipo.cdtelefonetipo, telefonetipo.nome")
				.join("leadtelefone.telefonetipo telefonetipo")
				.where("leadtelefone.lead = ?",lead)
				.orderBy("telefonetipo.nome")
				.list();
	}

}
