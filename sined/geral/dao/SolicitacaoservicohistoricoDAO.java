package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Solicitacaoservico;
import br.com.linkcom.sined.geral.bean.Solicitacaoservicohistorico;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("solicitacaoservicohistorico.dtaltera desc")
public class SolicitacaoservicohistoricoDAO extends GenericDAO<Solicitacaoservicohistorico> {

	public List<Solicitacaoservicohistorico> findBySolicitacaoservico(Solicitacaoservico form) {
		if(form == null || form.getCdsolicitacaoservico() == null){
			throw new SinedException("A solicita��o de servi�o n�o pode ser nula.");
		}
		return query()
					.where("solicitacaoservicohistorico.solicitacaoservico = ?", form)
					.orderBy("solicitacaoservicohistorico.dtaltera desc")
					.list();
	}
	
}
