package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.BancoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("banco.nome")
public class BancoDAO extends GenericDAO<Banco> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Banco> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		BancoFiltro filtro = (BancoFiltro) _filtro;
		query
			.select("banco.cdbanco, banco.nome, banco.numero, banco.ativo")
			.whereLikeIgnoreAll("banco.nome", filtro.getNome())
			.where("banco.numero = ?", filtro.getNumero())
			.where("banco.ativo = ?", filtro.getMostrar())
			.orderBy("banco.ativo DESC, banco.nome");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Banco> query) {
		query.leftOuterJoinFetch("banco.listaBancoretorno listaBancoretorno")
		.leftOuterJoinFetch("banco.listaBancoformapagamento listaBancoformapagamento")
		.leftOuterJoinFetch("banco.listaBancotipopagamento listaBancotipopagamento");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaBancoretorno");
		save.saveOrUpdateManaged("listaBancoformapagamento");
		save.saveOrUpdateManaged("listaBancotipopagamento");
	}
	
	/* singleton */
	private static BancoDAO instance;
	public static BancoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(BancoDAO.class);
		}
		return instance;
	}

	public Banco findByNumero(Integer numero) {
		if(numero == null){
			throw new SinedException("N�mero do banco n�o pode ser nulo.");
		}
		return query()
					.select("banco.cdbanco, banco.nome, banco.numero")
					.where("banco.numero = ?", numero)
					.unique();
	}

	public List<Banco> findForAndroid(String whereIn) {
		return query()
			.select("banco.cdbanco, banco.nome")
			.whereIn("banco.cdbanco", whereIn)
			.list();
	}

}
