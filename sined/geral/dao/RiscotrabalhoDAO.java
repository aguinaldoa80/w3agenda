package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Riscotrabalho;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.RiscotrabalhoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RiscotrabalhoDAO extends GenericDAO<Riscotrabalho> {

	@Override
	public void updateListagemQuery(QueryBuilder<Riscotrabalho> query,	FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		RiscotrabalhoFiltro filtro = (RiscotrabalhoFiltro) _filtro;
		query
		.select("riscotrabalho.cdriscotrabalho, riscotrabalho.nome, riscotrabalhotipo.nome")
		.join("riscotrabalho.riscotrabalhotipo riscotrabalhotipo")
		.where("riscotrabalhotipo = ?", filtro.getRiscotrabalhotipo())
		.whereLikeIgnoreAll("riscotrabalho.nome", filtro.getNome());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Riscotrabalho> query) {
		query
		.select("riscotrabalho.cdriscotrabalho, riscotrabalho.nome, riscotrabalho.cdusuarioaltera, riscotrabalho.dtaltera, " +
				"riscotrabalhotipo.cdriscotrabalhotipo")
		.join("riscotrabalho.riscotrabalhotipo riscotrabalhotipo");
	}
	
	/**
	 * M�todo que carrega os riscos de trabalho de um cargo
	 * 
	 * @param cargo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Riscotrabalho> getRiscosByCargo(Cargo cargo) {
		if(cargo == null || cargo.getCdcargo() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("riscotrabalho.cdriscotrabalho, riscotrabalho.nome")
			.where("riscotrabalho.cdriscotrabalho in (select rt.id from Cargomaterialseguranca cms " +
													 "join cms.cargo c " +
													 "join cms.riscotrabalho rt " +
													 "where c.id = "+cargo.getCdcargo()+")")
			.orderBy("riscotrabalho.nome desc")
			.list();
	}
}