package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.LoteestoqueFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.PosicaoEstoqueFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.RelatorioLoteEstoqueFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.report.bean.PosicaoEstoqueBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.report.bean.RelatorioLoteEstoqueBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class LoteestoqueDAO extends GenericDAO<Loteestoque> {
	
	private MaterialcategoriaService materialcategoriaService;
	
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {this.materialcategoriaService = materialcategoriaService;}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Loteestoque> query,FiltroListagem _filtro) {
		LoteestoqueFiltro filtro = (LoteestoqueFiltro) _filtro;
		
		query
			.select("distinct loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, fornecedor.nome, aux_loteestoque.cdloteestoque, aux_loteestoque.validade")
			.leftOuterJoin("loteestoque.fornecedor fornecedor")
			.leftOuterJoin("loteestoque.aux_loteestoque aux_loteestoque")
			.leftOuterJoin("loteestoque.listaLotematerial lotematerial")
			.leftOuterJoin("lotematerial.material material")
			.whereLikeIgnoreAll("loteestoque.numero", filtro.getNumero())
			.where("lotematerial.fabricacao >= ?", filtro.getDtinicioFab())
			.where("lotematerial.fabricacao <= ?", filtro.getDtfimFab())
			.where("lotematerial.validade >= ?", filtro.getDtinicio())
			.where("lotematerial.validade <= ?", filtro.getDtfim())
			.where("lotematerial.material = ?", filtro.getMaterial())
			.where("fornecedor = ?", filtro.getFornecedor())
			.orderBy("aux_loteestoque.validade, loteestoque.cdloteestoque");

			if(filtro.getMaterialcategoria() != null){
				materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
				query
					.leftOuterJoin("material.materialcategoria materialcategoria")
					.leftOuterJoin("materialcategoria.vmaterialcategoria vmaterialcategoria")
					.openParentheses()
						.where("vmaterialcategoria.identificador like ? ||'.%'", filtro.getMaterialcategoria().getIdentificador())
					.or()
						.where("vmaterialcategoria.identificador like ? ", filtro.getMaterialcategoria().getIdentificador())
					.closeParentheses();
			}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Loteestoque> query) {
		query	
			.select("loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, fornecedor.cdpessoa, fornecedor.nome")
			.leftOuterJoin("loteestoque.fornecedor fornecedor");
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaLotematerial");
	}
	/**
	 * M�todo para o autocomplete 
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<Loteestoque> findAutocomplete(String q, String whereInCdmaterial, String whereInFornecedor) {
		StringBuilder sql = new StringBuilder();
		
		sql
			.append("select distinct query.cdloteestoque, query.numero, min(query.validade) as validade ")
			.append("from (")
			.append("select le.cdloteestoque, le.numero, min(lm.validade) as validade ")
			.append("from lotematerial lm ")
			.append("join loteestoque le on le.cdloteestoque = lm.cdloteestoque ");
		
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		if (funcaoTiraacento != null) {
			sql.append(" where UPPER(" + funcaoTiraacento + "(le.numero)) LIKE '%'||'"+Util.strings.tiraAcento(q).toUpperCase()+"'||'%'");
		}
		else {
			sql.append(" where UPPER(le.numero) LIKE '%'||'"+Util.strings.tiraAcento(q).toUpperCase()+"'||'%'");
		}
		
		if(StringUtils.isNotBlank(whereInCdmaterial)){
			sql.append(" and lm.cdmaterial in (" + whereInCdmaterial + ") ");
		}
		
		if(StringUtils.isNotBlank(whereInFornecedor)){
			sql.append(" and le.cdfornecedor in (" + whereInFornecedor + ") ");
		}
		
		sql	
			.append("group by lm.cdmaterial, le.cdloteestoque, le.numero ")
			.append("order by lm.cdmaterial, min(lm.validade) ")
			.append(") query ")
			.append("group by query.cdloteestoque, query.numero ")
			.append("limit 100");

		SinedUtil.markAsReader();
		List<Loteestoque> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Loteestoque(rs.getInt("cdloteestoque"), rs.getString("numero"), rs.getDate("validade"));
				}
			});
			
		return lista;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.LoteestoqueDAO#findAutocompleteDisponiveis(String q, Movimentacaoestoque movimentacaoestoque)
	 *
	 * @param q
	 * @param movimentacaoestoque
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Loteestoque> findAutocompleteDisponiveis(String q, Material material) {
		if(material == null || material.getCdmaterial() == null){
			return new ListSet<Loteestoque>(Loteestoque.class);
		}else {
			return query()	
				.select("loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, " +
						"lotematerial.cdlotematerial, lotematerial.validade, material.cdmaterial, material.nome")
				.join("loteestoque.listaLotematerial lotematerial")
				.join("lotematerial.material material")
				.whereLikeIgnoreAll("loteestoque.numero", q)
				.where("material.cdmaterial = ?", material.getCdmaterial())
				.list();
		}
	}
	
	/**
	 * M�todo que retorna os lotes com 
	 *
	 * @param material
	 * @param empresa
	 * @param considerarEmpresa
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<Loteestoque> findQtdeLoteestoque(String whereInMaterial, Localarmazenagem localarmazenagem, Empresa empresa, boolean considerarQtdeDisponivel, Boolean considerarEmpresa, String whereInLoteestoque, Boolean considerarQtdeReservada, Boolean agruparPorLocalAndLocal){
		StringBuilder sql = new StringBuilder();
		String materiais = SinedUtil.quebraWhereInSubQuery("vg.cdmaterial", whereInMaterial);
		String qtdeReservada = "";
		
		if(Boolean.TRUE.equals(considerarQtdeReservada)){
			if(agruparPorLocalAndLocal){
				qtdeReservada = " - qtdereservada(vg.cdmaterial, vg.cdlocalarmazenagem, vg.cdempresa, le.cdloteestoque) ";
			}else{
				qtdeReservada = " - coalesce((select SUM (re.quantidade) from reserva re where re.cdmaterial = vg.cdmaterial and re.cdloteestoque = le.cdloteestoque), 0) ";
			}
		}
		
		if(agruparPorLocalAndLocal){
			
		}
		
		String campoValidadeMin = "(select min(lm.validade) from lotematerial lm where lm.cdloteestoque = le.cdloteestoque and lm.cdmaterial = vg.cdmaterial and lm.validade is not null)";
		sql
			.append("select distinct vg.cdmaterial, SUM(vg.qtdedisponivel) " + qtdeReservada + " as qtdedisponivel, le.cdloteestoque, le.numero, " + campoValidadeMin + " as validade ")
			.append("from vgerenciarmaterial vg ")
			.append("left outer join empresa e on e.cdpessoa = vg.cdempresa ")
			.append("join loteestoque le on le.cdloteestoque = vg.cdloteestoque and vg.cdloteestoque is not null ");
		
		if(localarmazenagem == null && empresa != null){
			sql.append("left outer join localarmazenagemempresa lae on lae.cdlocalarmazenagem = vg.cdlocalarmazenagem ");
		}
		
		sql
			.append("where " + materiais + " ")
			.append("and exists (select ma.cdmaterial from material ma where ma.cdmaterial = vg.cdmaterialmovimentacaoestoque and ma.ativo = true) ");
		
		if(localarmazenagem != null){
			sql.append(" and vg.cdlocalarmazenagem = " + localarmazenagem.getCdlocalarmazenagem() + " ");
		} else if(empresa != null){
			sql.append("and (lae.cdempresa = " + empresa.getCdpessoa() + " or lae is null )");
		}
		
		if(considerarEmpresa != null && considerarEmpresa){
			if(empresa != null && empresa.getCdpessoa() != null){
				if(empresa != null){
					sql
						.append(" and ( e.cdpessoa = " + empresa.getCdpessoa() + " or e is null) ");
				}
			}
		}
		if(StringUtils.isNotBlank(whereInLoteestoque)){
			sql.append(" and le.cdloteestoque in (" + whereInLoteestoque + ") ");
		}
		
		sql.append(" group by vg.cdmaterial, le.cdloteestoque, le.numero, le.validade ");
		if(agruparPorLocalAndLocal){
			qtdeReservada = ", vg.cdlocalarmazenagem, vg.cdempresa) ";
		}
		if(considerarQtdeDisponivel)
			sql.append(" having (SUM(vg.qtdedisponivel) > 0) ");
		
		sql.append("order by vg.cdmaterial, " + campoValidadeMin);
		
		List<Loteestoque> lista = null;
		SinedUtil.markAsReader();
		try {
			lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Loteestoque(rs.getInt("cdmaterial"), rs.getInt("cdloteestoque"), rs.getString("numero"), rs.getDate("validade"), 
							rs.getDouble("qtdedisponivel"));
				}
			});
		} finally {
			SinedUtil.desmarkAsReader();
		}
		
		
		return lista;
	}
	
	public List<Loteestoque> findForAutocompleteTrocaLote(String str, String whereInMaterial, Localarmazenagem localarmazenagem, Empresa empresa, Boolean considerarEmpresa){
		StringBuilder sql = new StringBuilder();
		String qtdeReservada = " - qtdereservada(vg.cdmaterial, vg.cdlocalarmazenagem, vg.cdempresa, le.cdloteestoque) ";
		
		
		String campoValidadeMin = "(select min(lm.validade) from lotematerial lm where lm.cdloteestoque = le.cdloteestoque and lm.cdmaterial = vg.cdmaterial)";
		sql
			.append("select distinct vg.cdmaterial, SUM(vg.qtdedisponivel) " + qtdeReservada + " as qtdedisponivel, le.cdloteestoque, le.numero, " + campoValidadeMin + " as validade ")
			.append("from vgerenciarmaterial vg ")
			.append("left outer join empresa e on e.cdpessoa = vg.cdempresa ")
			.append("join loteestoque le on le.cdloteestoque = vg.cdloteestoque and vg.cdloteestoque is not null ");
		
		if(localarmazenagem == null && empresa != null){
			sql.append("left outer join localarmazenagemempresa lae on lae.cdlocalarmazenagem = vg.cdlocalarmazenagem ");
		}
		
		sql.append("where UPPER(retira_acento(le.numero)) LIKE '%" + Util.strings.tiraAcento(str).toUpperCase() + "%' ")
			.append(" and vg.cdmaterial in (").append(whereInMaterial).append(") ")
			.append("and exists (select ma.cdmaterial from material ma where ma.cdmaterial = vg.cdmaterialmovimentacaoestoque and ma.ativo = true) ");
		
		if(localarmazenagem != null){
			sql.append(" and vg.cdlocalarmazenagem = " + localarmazenagem.getCdlocalarmazenagem() + " ");
		} else if(empresa != null){
			sql.append("and (lae.cdempresa = " + empresa.getCdpessoa() + " or lae is null )");
		}
		
		if(considerarEmpresa != null && considerarEmpresa){
			if(empresa != null && empresa.getCdpessoa() != null){
				if(empresa != null){
					sql
						.append(" and ( e.cdpessoa = " + empresa.getCdpessoa() + " or e is null) ");
				}
			}
		}
		
		sql.append(" group by vg.cdmaterial, le.cdloteestoque, le.numero, le.validade , vg.cdlocalarmazenagem, vg.cdempresa ");
		
		sql.append(" having (SUM(vg.qtdedisponivel) > 0) ");
		
		sql.append("order by vg.cdmaterial, " + campoValidadeMin);
		
		List<Loteestoque> lista = null;
		SinedUtil.markAsReader();
		try {
			lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Loteestoque(rs.getInt("cdmaterial"), rs.getInt("cdloteestoque"), rs.getString("numero"), rs.getDate("validade"), 
							rs.getDouble("qtdedisponivel"));
				}
			});
		} finally {
			SinedUtil.desmarkAsReader();
		}
		
		
		return lista;
	}

	/**
	 * M�todo que retorna a qtde dispon�vel do lote
	 *
	 * @param material
	 * @param localarmazenagem
	 * @param empresa
	 * @param loteestoque
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public Double getQtdeLoteestoque(Material material,	Localarmazenagem localarmazenagem, Empresa empresa,	Loteestoque loteestoque, Boolean filtroMaterialmestregrade) {
		StringBuilder sql = new StringBuilder();
		
		if(material == null || loteestoque == null)
			throw new SinedException("Par�metro inv�lido.");
		
		sql
			.append("select distinct SUM(vg.qtdedisponivel) as qtdedisponivel, le.cdloteestoque, le.numero, le.validade ")
			.append("from vgerenciarmaterial vg ")
			.append("left outer join empresa e on e.cdpessoa = vg.cdempresa ")
			.append("join loteestoque le on le.cdloteestoque = vg.cdloteestoque ");
		
		if(filtroMaterialmestregrade != null && filtroMaterialmestregrade)
			sql.append("join material m on m.cdmaterial = vg.cdmaterialmovimentacaoestoque ");
		
		sql.append("where le.cdloteestoque = " + loteestoque.getCdloteestoque() + " ");
		
		if(filtroMaterialmestregrade != null && filtroMaterialmestregrade){
			sql.append(" and m.cdmaterialmestregrade = " + material.getCdmaterial() + " ");
		} else {
			sql.append(" and vg.cdmaterialmovimentacaoestoque = " + material.getCdmaterial() + " ");
		}
		
		if(localarmazenagem != null)
			sql.append(" and vg.cdlocalarmazenagem = " + localarmazenagem.getCdlocalarmazenagem() + " ");
		
		if(empresa != null){
			sql
				.append(" and ( e.cdpessoa = " + empresa.getCdpessoa() + " or e is null) ");
		}
		sql.append(" group by le.cdloteestoque, le.numero, le.validade ");
		
		SinedUtil.markAsReader();
		List<Loteestoque> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Loteestoque(rs.getInt("cdloteestoque"), rs.getString("numero"), rs.getDate("validade"), 
							rs.getDouble("qtdedisponivel"));
				}
			});
		
		if(lista != null && !lista.isEmpty()){
			return lista.get(0).getQtde();
		}else {
			return 0.0;
		}
	}

	/**
	 * M�todo que carrega o lote
	 *
	 * @param loteestoque
	 * @return
	 * @author Luiz Fernando
	 */
	public Loteestoque carregaLoteestoque(Loteestoque loteestoque) {
		if(loteestoque == null || loteestoque.getCdloteestoque() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("loteestoque.cdloteestoque, loteestoque.numero,  loteestoque.validade")
				.where("loteestoque = ?", loteestoque)
				.unique();
	}
	
	/**
	 * M�todo que retorna o loteestoque pelos campos 'numero' e 'fornecedor' 
	 * ou o primeiro loteestoque encontrado para um 'numero' caso n�o haja 'fornecedor'
	 * 
	 * @param numero
	 * @param fornecedor
	 * @return
	 * @author Rafael Salvio
	 */
	public Loteestoque findByNumeroAndFornecedor(String numero, Fornecedor fornecedor){
		if(numero == null || numero.trim().isEmpty())
			throw new SinedException("Par�metro 'numero' inv�lido.");
		
		QueryBuilder<Loteestoque> query = query()
			.whereLikeIgnoreAll("loteestoque.numero", numero);
		
		if(fornecedor != null && fornecedor.getCdpessoa() != null){
			query.where("loteestoque.fornecedor = ?", fornecedor);
		}
		
		List<Loteestoque> result = query.list();
		
		if(result != null && !result.isEmpty())
			return result.get(0);
		
		return null;
	}
	
	public Loteestoque findByNumeroAndFornecedorForImportacaoXml(String numero, Fornecedor fornecedor){
		if(numero == null || numero.trim().isEmpty())
			throw new SinedException("Par�metro 'numero' inv�lido.");
		
		QueryBuilder<Loteestoque> query = query()
									.select("loteestoque.cdloteestoque, loteestoque.numero, "+
											"listaLotematerial.validade, listaLotematerial.validade, listaLotematerial.fabricacao, listaLotematerial.codigoAgregacao, "+
											"material.cdmaterial, material.nome")	
									.whereEqualsIgnoreSimbols("loteestoque.numero", numero)
									.leftOuterJoin("loteestoque.listaLotematerial listaLotematerial")
									.leftOuterJoin("listaLotematerial.material material");
		
		if(fornecedor != null && fornecedor.getCdpessoa() != null){
			query.where("loteestoque.fornecedor = ?", fornecedor);
		}
		
		List<Loteestoque> result = query.list();
		
		if(result != null && !result.isEmpty())
			return result.get(0);
		
		return null;
	}
	
	/**
	 * M�todo que carrega os lotes de determinado material.
	 * 
	 * @param material
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Loteestoque> findByMaterial(Material material){
		if(material == null || material.getCdmaterial() == null){
			return new ListSet<Loteestoque>(Loteestoque.class);
		}
		
		return query()
					.select("loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade")	
					.join("loteestoque.listaLotematerial lotematerial")
					.where("lotematerial.material = ?", material)
					.list();
	}
	
	/**
	 * M�todo que retorna os lotes com suas respectivas quantidades
	 *
	 * @param material
	 * @param localarmazenagem
	 * @param empresa
	 * @return
	 * @author Rafael Salvio
	 */
	@SuppressWarnings("unchecked")
	public List<Loteestoque> findQtdeLoteestoqueForAutocomplete(String lote, Material material, Localarmazenagem localarmazenagem, Empresa empresa, boolean considerarQtdeDisponivel, Boolean considerarEmpresa){
		StringBuilder sql = new StringBuilder();
			
		sql
			.append("select distinct vg.cdmaterial, SUM(vg.qtdedisponivel) as qtdedisponivel, le.cdloteestoque, le.numero, lm.validade ")
			.append("from vgerenciarmaterial vg ")
			.append("left outer join empresa e on e.cdpessoa = vg.cdempresa ")
			.append("join loteestoque le on le.cdloteestoque = vg.cdloteestoque and vg.cdloteestoque is not null ")
			.append("join lotematerial lm on le.cdloteestoque = lm.cdloteestoque and vg.cdmaterial = lm.cdmaterial ");
		
		if(localarmazenagem == null && empresa != null){
			sql.append("left outer join localarmazenagemempresa lae on lae.cdlocalarmazenagem = vg.cdlocalarmazenagem ");
		}
		
		sql	
			.append("where upper(le.numero) like ('%" + lote.toUpperCase() + "%') ")
			.append(" and vg.cdmaterial = " + material.getCdmaterial())
			.append(" and exists (select ma.cdmaterial from material ma where ma.cdmaterial = vg.cdmaterialmovimentacaoestoque and ma.ativo = true) ");

		if(localarmazenagem != null){
			sql.append(" and vg.cdlocalarmazenagem = " + localarmazenagem.getCdlocalarmazenagem() + " ");
		} else if(empresa != null){
			sql.append(" and (lae.cdempresa = " + empresa.getCdpessoa() + " or lae is null )");
		}
		
		if(considerarEmpresa != null && considerarEmpresa){
			if(empresa != null && empresa.getCdpessoa() != null){
				if(empresa != null){
					sql
						.append(" and ( e.cdpessoa = " + empresa.getCdpessoa() + " or e is null) ");
				}
			}
		}
		sql.append(" group by vg.cdmaterial, le.cdloteestoque, le.numero, lm.validade ");
		if(considerarQtdeDisponivel){
			sql.append(" having (SUM(vg.qtdedisponivel) > 0) ");			
		}else {
			sql
				.append("union ")
				.append("select lm.cdmaterial, 0 as qtdedisponivel, le.cdloteestoque, le.numero, lm.validade ")
				.append("from loteestoque le ")
				.append("join lotematerial lm on lm.cdloteestoque = le.cdloteestoque ")
				.append("where upper(le.numero) like ('%" + lote.toUpperCase() + "%') ")
				.append("and lm.cdmaterial = " + material.getCdmaterial())	
				.append(" and not exists (select 1 from movimentacaoestoque me where me.cdmaterial = lm.cdmaterial and me.cdloteestoque = le.cdloteestoque and me.dtcancelamento is null) ");			
		}
		
		//sql.append("order by vg.cdmaterial, lm.validade ");
		sql.append("order by cdmaterial, validade ");
		SinedUtil.markAsReader();
		
		List<Loteestoque> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Loteestoque(rs.getInt("cdmaterial"), rs.getInt("cdloteestoque"), rs.getString("numero"), rs.getDate("validade"), 
							rs.getDouble("qtdedisponivel"));
				}
			});
		
		return lista;
	}
	
	/**
	 * M�todo que retorna a lista de lotes de acordo com um whereIn de numeros;
	 * 
	 * @param whereIn
	 * @param whereInCnpj
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Loteestoque> findByWhereInNumeroAndFornecedor(String whereIn, String whereInCnpj){
		if(whereIn == null || whereIn.trim().isEmpty()){
			return null;
		}
		
		return query()
				.select("loteestoque.cdloteestoque, loteestoque.numero, lotematerial.cdlotematerial, lotematerial.loteestoque, lotematerial.validade, " +
						"material.cdmaterial, fornecedor.cdpessoa, fornecedor.cnpj")
				.leftOuterJoin("loteestoque.listaLotematerial lotematerial")
				.leftOuterJoin("lotematerial.material material")
				.join("loteestoque.fornecedor fornecedor")
				.whereIn("loteestoque.numero", whereIn)
				.whereIn("fornecedor.cnpj", whereInCnpj)
				.list();
	}
	
	public List<Loteestoque> findWithLotematerial(String whereIn){
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		return query()
				.select("loteestoque.cdloteestoque, listaLotematerial.cdlotematerial, listaLotematerial.validade, material.cdmaterial, material.nome, listaLotematerial.fabricacao, listaLotematerial.codigoAgregacao ")
				.join("loteestoque.listaLotematerial listaLotematerial")
				.join("listaLotematerial.material material")
				.whereIn("loteestoque.cdloteestoque", whereIn)
				.orderBy("material.nome, listaLotematerial.validade")
				.list();
	}
	
	/**
	* M�todo que retorna os lotes vinculados ao material
	*
	* @param whereInMaterial
	* @return
	* @since 28/12/2016
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public List<Loteestoque> findLoteByMaterial(String whereInMaterial, String numero, boolean autocomplete){
		if(StringUtils.isBlank(whereInMaterial)){
			return new ArrayList<Loteestoque>();
		}
		StringBuilder sql = new StringBuilder();
		
		sql
			.append(" select lm.cdmaterial, le.cdloteestoque, le.numero, min(lm.validade) as validade ")
			.append(" from lotematerial lm ")
			.append(" join loteestoque le on le.cdloteestoque = lm.cdloteestoque ")
			.append(" where lm.cdmaterial in (" + whereInMaterial + ") ");
		
		if(StringUtils.isNotBlank(numero)){
			String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
			if (funcaoTiraacento != null) {
				sql.append(" and UPPER(" + funcaoTiraacento + "(le.numero)) LIKE '%'||'"+Util.strings.tiraAcento(numero).toUpperCase()+"'||'%'");
			}
			else {
				sql.append(" and UPPER(le.numero) LIKE '%'||'"+Util.strings.tiraAcento(numero).toUpperCase()+"'||'%'");
			}
		}
		
		sql
			.append(" group by lm.cdmaterial, le.cdloteestoque, le.numero ")
			.append(" order by lm.cdmaterial, min(lm.validade) ");
		
		if(autocomplete){
			sql.append("limit 30");
		}

		SinedUtil.markAsReader();
		List<Loteestoque> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Loteestoque(rs.getInt("cdmaterial"), rs.getInt("cdloteestoque"), rs.getString("numero"), rs.getDate("validade"), null);
				}
			});
		
		return lista;
	}
	
	
	/**
	* M�todo que carrega o lote com o n�mero e validade
	*
	* @param loteestoque
	* @param material
	* @return
	* @since 17/04/2017
	* @author Luiz Fernando
	*/
	public Loteestoque loadWithNumeroValidade(Loteestoque loteestoque, Material material, boolean menorValidade) {
		if(loteestoque == null || loteestoque.getCdloteestoque() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		StringBuilder sql = new StringBuilder();
		
		String validade = " null ";
		if(material != null && material.getCdmaterial() != null){
			if(menorValidade){
				validade = " (select min(lm.validade) from lotematerial lm where lm.cdmaterial = " + material.getCdmaterial() + " and lm.validade is not null) ";
			}else{
				validade = " (select min(lm.validade) from lotematerial lm where lm.cdmaterial = " + material.getCdmaterial() + " and lm.validade is not null and lm.cdloteestoque = le.cdloteestoque) ";
			}
		}
		
		sql
			.append("select le.cdloteestoque, le.numero, " + validade + " as validade ")
			.append("from Loteestoque le ")
			.append("where le.cdloteestoque = " + loteestoque.getCdloteestoque());

		SinedUtil.markAsReader();
		Loteestoque bean = (Loteestoque) getJdbcTemplate().queryForObject(sql.toString(), new RowMapper() {
			public Loteestoque mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Loteestoque(rs.getInt("cdloteestoque"), rs.getString("numero"), rs.getDate("validade"));
			}
		});
			
		return bean;
	}
	
	public List<Loteestoque> findLoteestoqueForCsvReport(LoteestoqueFiltro filtro) {
		QueryBuilder<Loteestoque> query = querySined();
		updateListagemQuery(query, filtro);
		return query.list();
	}
	
	public Loteestoque findWithLotematerial(Loteestoque loteestoque){
		if(loteestoque == null)
			throw new SinedException("Par�metro inv�lido");
		
		return query()
				.select("loteestoque.cdloteestoque, listaLotematerial.cdlotematerial, listaLotematerial.validade, material.cdmaterial, material.nome, listaLotematerial.fabricacao, listaLotematerial.codigoAgregacao ")
				.join("loteestoque.listaLotematerial listaLotematerial")
				.join("listaLotematerial.material material")
				.where("loteestoque = ?", loteestoque)
				.unique();
	}

	public Date buscarDtVencimentoLote(Integer cdmaterial, Integer cdpessoa, Integer cdlocalarmazenagem) {
		if(cdmaterial == null)
			throw new SinedException("Par�metro inv�lido");
		
		StringBuilder sql = new StringBuilder();
		
		sql
			.append("select lm.validade ")
			.append("from vgerenciarmaterial vg ")
			.append("join loteestoque le on le.cdloteestoque = vg.cdloteestoque and vg.cdloteestoque is not null ")
			.append("join lotematerial lm on lm.cdloteestoque = le.cdloteestoque and lm.cdmaterial = vg.cdmaterial ")
			.append("left outer join empresa e on e.cdpessoa = vg.cdempresa ")
			.append("left outer join localarmazenagemempresa lae on lae.cdlocalarmazenagem = vg.cdlocalarmazenagem ")
			.append("where lm.cdmaterial = " + cdmaterial)
			.append(" and e.cdpessoa = " + cdpessoa);
		if(cdlocalarmazenagem != null){
			sql.append(" and vg.cdlocalarmazenagem = " + cdlocalarmazenagem);
		}
			sql.append(" and lm.validade is not null")
			.append(" and (vg.qtdedisponivel) > 0 ")
			.append("order by lm.validade asc, lm.cdlotematerial asc ")
			.append("limit 1");
		
		SinedUtil.markAsReader();
		Date data = null;
		try {
			data = (Date) getJdbcTemplate().queryForObject(sql.toString(), new RowMapper() {
				public Date mapRow(ResultSet rs, int rowNum) throws SQLException {
					return rs.getDate("validade");
				}
			});			
		} catch (Exception e) {}
		
		return data;
	}

	@SuppressWarnings("unchecked")
	public List<Loteestoque> buscarLotesMaterialDisponivel(final Integer cdmaterial, Integer cdlocalarmazenagem, Integer cdpessoa) {
		if(cdmaterial == null)
			throw new SinedException("Par�metro inv�lido");
		
		StringBuilder sql = new StringBuilder();
		
		sql
			.append("select vg.cdloteestoque, le.numero, sum(vg.qtdedisponivel) as qtdedisponivel, lm.validade, ")
			.append("qtdereservada(vg.cdmaterial, vg.cdlocalarmazenagem, e.cdpessoa, vg.cdloteestoque) ")			
			.append("from vgerenciarmaterial vg ")
			.append("join loteestoque le on le.cdloteestoque = vg.cdloteestoque and vg.cdloteestoque is not null ")
			.append("join lotematerial lm on lm.cdloteestoque = le.cdloteestoque and lm.cdmaterial = vg.cdmaterial ")
			.append("left outer join empresa e on e.cdpessoa = vg.cdempresa ")
			.append("left outer join localarmazenagem lae on lae.cdlocalarmazenagem = vg.cdlocalarmazenagem ")
			.append("where lm.cdmaterial = " + cdmaterial)			
			.append(" and e.cdpessoa = " + cdpessoa);
			if(cdlocalarmazenagem != null){
				sql.append(" and vg.cdlocalarmazenagem = " + cdlocalarmazenagem);
			}
			sql.append(" and lm.validade is not null")
			//.append(" and lm.validade > to_date('"+new SimpleDateFormat("dd/MM/yyyy").format(new Date(System.currentTimeMillis()))+"', 'dd/mm/yyyy')")
			.append(" group by vg.cdmaterial, vg.cdloteestoque, le.numero, lm.validade, vg.cdlocalarmazenagem, e.cdpessoa ")
			.append("having sum(vg.qtdedisponivel) - qtdereservada(vg.cdmaterial, vg.cdlocalarmazenagem, e.cdpessoa, vg.cdloteestoque) > 0 " )
			.append("order by lm.validade;");
		
		SinedUtil.markAsReader();
		List<Loteestoque> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Loteestoque(cdmaterial, rs.getInt("cdloteestoque"), rs.getString("numero"), rs.getDate("validade"), 
							rs.getDouble("qtdedisponivel"), rs.getDouble("qtdereservada"));
				}
			});
		
		return lista;
	}

	public Double buscarQtdeDisponivelMaterialLotes(Integer cdmaterial, Integer cdlocalarmazenagem, Integer cdpessoa, Integer cdLoteestoque) {
		if(cdmaterial == null)
			throw new SinedException("Par�metro inv�lido");
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("select sum(vg.qtdedisponivel) - sum(qtdereservada(vg.cdmaterial, vg.cdlocalarmazenagem, e.cdpessoa, vg.cdloteestoque)) as qtde ")
		.append("from vgerenciarmaterial vg ")
		.append("join loteestoque le on le.cdloteestoque = vg.cdloteestoque and vg.cdloteestoque is not null ")
		.append("join lotematerial lm on lm.cdloteestoque = le.cdloteestoque and lm.cdmaterial = vg.cdmaterial ")
		.append("left outer join empresa e on e.cdpessoa = vg.cdempresa ")
		.append("left outer join localarmazenagem lae on lae.cdlocalarmazenagem = vg.cdlocalarmazenagem ")
		.append("where lm.cdmaterial = " + cdmaterial)
		.append(" and e.cdpessoa = " + cdpessoa);
		if(cdlocalarmazenagem != null){
			sql.append(" and vg.cdlocalarmazenagem = " + cdlocalarmazenagem);
		}
		
		if(cdLoteestoque != null){
			sql.append(" and vg.cdloteestoque = " + cdLoteestoque);
		}
		
//		sql.append(" and lm.validade is not null");
		//sql.append(" group by vg.cdmaterial, vg.cdlocalarmazenagem, e.cdpessoa, vg.cdloteestoque");
		
		
		SinedUtil.markAsReader();
		Double qtdeDisponivel = null;
		try {
			qtdeDisponivel = (Double) getJdbcTemplate().queryForObject(sql.toString(), new RowMapper() {
				public Double mapRow(ResultSet rs, int rowNum) throws SQLException {
					return rs.getDouble("qtde");
				}
			});			
		} catch (Exception e) {}
		
		return qtdeDisponivel;
		
	}
	
	public List<Loteestoque> findByMaterialAndFornecedor(Material material, Fornecedor fornecedor){
		if(material == null || material.getCdmaterial() == null){
			return new ListSet<Loteestoque>(Loteestoque.class);
		}
		
		return querySined()
					.setUseReadOnly(false)
					.select("loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, lotematerial.validade")	
					.join("loteestoque.listaLotematerial lotematerial")
					.leftOuterJoin("loteestoque.fornecedor fornecedor")
					.where("lotematerial.material = ?", material)
					.where("fornecedor = ?", fornecedor)
					.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<RelatorioLoteEstoqueBean> findForRelatorioEstoque(RelatorioLoteEstoqueFiltro filtro) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("select m.cdmaterial as CD_Material, m.identificacao as ID_Material, ")
		.append("vm.nome Nome_Material, um.simbolo as Unidade_Medida, ")
		.append("vm.cdloteestoque as ID_Lote, le.numero as Numero_Lote, lm.validade as Validade_Lote, ")
		.append("f.nome as Nome_Fornecedor, mt.nome as Tipo_Material, la.nome as Local_Armazenagem, ")
		.append("sum(vm.qtdedisponivel) as Qtde_Disponivel,  ")
		.append("qtdereservada(vm.cdmaterial, vm.cdlocalarmazenagem, vm.cdempresa, vm.cdloteestoque) as Qtde_Reservada, ")
		.append("e.nomefantasia as Empresa, nf.nome as Nome_Fabricante ")
		.append("from vgerenciarmaterial vm ")
		.append("left join loteestoque le on le.cdloteestoque = vm.cdloteestoque ")
		.append("left join lotematerial lm on lm.cdloteestoque = le.cdloteestoque ")
		.append("left join material m on m.cdmaterial = vm.cdmaterial ")
		.append("left join materialfornecedor mf on mf.cdmaterial = vm.cdmaterial and mf.fabricante is true  ")
		.append("left join localarmazenagem la on la.cdlocalarmazenagem = vm.cdlocalarmazenagem ")
		.append("left join unidademedida um on um.cdunidademedida = vm.cdunidademedida ")
		.append("left join materialtipo mt on mt.cdmaterialtipo = m.cdmaterialtipo ")
		.append("left join pessoa f on f.cdpessoa = le.cdfornecedor ")
		.append("left join empresa e on e.cdpessoa = vm.cdempresa ")
		.append("left join pessoa nf on nf.cdpessoa = mf.cdpessoa  ")
		.append("where lm.validade >= '" + filtro.getDtInicio() + "' ")
		.append("and lm.validade <= '" + filtro.getDtFim()  + "' ");
		if(filtro.getEmpresa() != null){
			sql.append("and vm.cdempresa = " + filtro.getEmpresa().getCdpessoa());			
		}
		if(filtro.getFornecedorLote() != null){
			sql.append(" and le.cdfornecedor = " + filtro.getFornecedorLote().getCdpessoa());
		}
		if(filtro.getLocalarmazenagem() != null){
			sql.append(" and vm.cdlocalarmazenagem = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem());
		}
		if(filtro.getMaterialgrupo() != null){
			sql.append(" and m.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo());
		}
		if(filtro.getMaterialtipo() != null){
			sql.append(" and m.cdmaterialtipo = " + filtro.getMaterialtipo().getCdmaterialtipo());
		}
		if(filtro.getMaterialcategoria() != null){
			sql.append(" and m.cdmaterialcategoria = " + filtro.getMaterialcategoria().getCdmaterialcategoria());
		}
		if(filtro.getMaterial() != null){
			sql.append(" and vm.cdmaterial = " + filtro.getMaterial().getCdmaterial());
		}
		if(filtro.getLoteestoque() != null){
			sql.append(" and vm.cdloteestoque = " + filtro.getLoteestoque().getCdloteestoque());
		}
		if(filtro.getFabricanteMaterial() != null){
			sql.append(" and mf.cdpessoa = " + filtro.getFabricanteMaterial().getCdpessoa());
		}
		sql.append(" group by 1, 5,10,2,3,4,6,7,8,9,12,13,14 ")
		.append("order by 1,7;");
		
		SinedUtil.markAsReader();
		List<RelatorioLoteEstoqueBean> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new RelatorioLoteEstoqueBean(rs.getInt("CD_Material"), rs.getString("ID_Material"), rs.getString("Nome_Material"),
						 rs.getString("Unidade_Medida"), rs.getInt("ID_Lote"),  rs.getString("Numero_Lote"), rs.getDate("Validade_Lote"),
						 rs.getString("Nome_Fornecedor"), rs.getString("Tipo_Material"), rs.getString("Local_Armazenagem"),
						 rs.getDouble("Qtde_Disponivel"), rs.getDouble("Qtde_Reservada"),  rs.getString("Empresa"), 
						 rs.getString("Nome_Fabricante"));
			}
		});
			
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	public List<PosicaoEstoqueBean> findForRelatorioPosicaoEstoque(PosicaoEstoqueFiltro filtro) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("select m.cdmaterial as CD_Material, m.identificacao as ID_Material, "); 
		sql.append("vm.nome as Nome_Material, um.simbolo as Unidade_Medida,  ");
		sql.append("mt.nome as Tipo_Material, mc.descricao as Material_Categoria, e.nome as Empresa, "); 
		sql.append("f.nome as Nome_Fornecedor, "); 
		sql.append("(select codigo from materialfornecedor where cdpessoa = le.cdfornecedor and cdmaterial = vm.cdmaterial limit 1) as Codigo_Fornecedor, ");  
		sql.append("nf.nome as Nome_Fabricante, mf.codigo as Codigo_Fabricante, "); 
		sql.append("la.nome as Local_Armazenagem, m.codigobarras as CODIGO_BARRAS, ");
		sql.append("sum(vm.qtdedisponivel) as Qtde_Disponivel, ");  
		sql.append("qtdereservada(vm.cdmaterial, vm.cdlocalarmazenagem, vm.cdempresa, vm.cdloteestoque) as Qtde_Reservada, ");
		sql.append("m.valorcusto as Custo_Unitario, mg.nome as Material_Grupo, (m.valorcusto * "); 
		sql.append("(sum(vm.qtdedisponivel))) as Custo_Total ");
		sql.append("from vgerenciarmaterial vm ");
		sql.append("left join loteestoque le on le.cdloteestoque = vm.cdloteestoque ");
		sql.append("left join lotematerial lm on lm.cdloteestoque = le.cdloteestoque ");
		sql.append("						and lm.cdmaterial = vm.cdmaterial ");
		sql.append("left join material m on m.cdmaterial = vm.cdmaterial ");
		sql.append("left join materialcategoria mc on mc.cdmaterialcategoria = m.cdmaterialcategoria ");
		sql.append("left join materialfornecedor mf on mf.cdmaterial = vm.cdmaterial and mf.fabricante is true "); 
		sql.append("left join localarmazenagem la on la.cdlocalarmazenagem = vm.cdlocalarmazenagem ");
		sql.append("left join unidademedida um on um.cdunidademedida = vm.cdunidademedida ");
		sql.append("left join materialtipo mt on mt.cdmaterialtipo = m.cdmaterialtipo ");
		sql.append("left join materialgrupo mg on m.cdmaterialgrupo = mg.cdmaterialgrupo ");
		sql.append("left join pessoa f on f.cdpessoa = le.cdfornecedor ");
		sql.append("left join pessoa e on e.cdpessoa = vm.cdempresa ");
		sql.append("left join pessoa nf on nf.cdpessoa = mf.cdpessoa  ");
		sql.append(" where 1 = 1 ");
		if(filtro.getEmpresa() != null){
			sql.append("and vm.cdempresa = " + filtro.getEmpresa().getCdpessoa());			
		}
		if(filtro.getFornecedorLote() != null){
			sql.append(" and le.cdfornecedor = " + filtro.getFornecedorLote().getCdpessoa());
		}
		if(filtro.getLocalarmazenagem() != null){
			sql.append(" and vm.cdlocalarmazenagem = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem());
		}
		if(filtro.getMaterialgrupo() != null){
			sql.append(" and m.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo());
		}
		if(filtro.getMaterialtipo() != null){
			sql.append(" and m.cdmaterialtipo = " + filtro.getMaterialtipo().getCdmaterialtipo());
		}
		if(filtro.getMaterialcategoria() != null){
			sql.append(" and m.cdmaterialcategoria = " + filtro.getMaterialcategoria().getCdmaterialcategoria());
		}
		if(filtro.getMaterial() != null){
			sql.append(" and vm.cdmaterial = " + filtro.getMaterial().getCdmaterial());
		}
		if(filtro.getLoteestoque() != null){
			sql.append(" and vm.cdloteestoque = " + filtro.getLoteestoque().getCdloteestoque());
		}
		if(filtro.getFabricanteMaterial() != null){
			sql.append(" and mf.cdpessoa = " + filtro.getFabricanteMaterial().getCdpessoa());
		}
		sql.append(" group by 1,2,3,4,5,6,7,8,9,10,11,12,13,15,16,17 ")
			.append(" having 1 = 1");
		if(filtro.getIntervaloQuantidadeInicial() != null){
			sql.append(" and sum(vm.qtdedisponivel) >= ").append(filtro.getIntervaloQuantidadeInicial());
		}
		if(filtro.getIntervaloQuantidadeFinal() != null){
			sql.append(" and sum(vm.qtdedisponivel) <= ").append(filtro.getIntervaloQuantidadeFinal());
		}
		sql.append("order by 1");
		List<PosicaoEstoqueBean> lista = null; 
		SinedUtil.markAsReader();
		try {
			lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new PosicaoEstoqueBean(rs.getInt("CD_Material"), rs.getString("ID_Material"), rs.getString("Nome_Material"),
							 rs.getString("Unidade_Medida"), null,  "", null,
							 rs.getString("Nome_Fornecedor"), rs.getString("Tipo_Material"), rs.getString("Local_Armazenagem"),
							 rs.getDouble("Qtde_Disponivel"), rs.getDouble("Qtde_Reservada"),  rs.getString("Empresa"), 
							 rs.getString("Nome_Fabricante"), rs.getString("Material_Categoria"), rs.getString("Material_Grupo"), rs.getString("CODIGO_BARRAS"), 
							 rs.getDouble("Custo_Unitario"), rs.getDouble("Custo_Total"), rs.getString("Codigo_Fabricante"));
				}
			});	
		} finally {
			SinedUtil.desmarkAsReader();
		}
		
			
		return lista;
	}
}
