package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Fornecimentocontrato;
import br.com.linkcom.sined.geral.bean.Ordemcomprafornecimentocontrato;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OrdemcomprafornecimentocontratoDAO extends GenericDAO<Ordemcomprafornecimentocontrato>{

	/**
	 * M�todo que busca as ordens de compra do contrato de fornecimento
	 *
	 * @param contrato
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Ordemcomprafornecimentocontrato> findByFornecimentocontrato(Fornecimentocontrato fornecimentocontrato) {
		if(fornecimentocontrato == null || fornecimentocontrato.getCdfornecimentocontrato() == null){
			throw new SinedException("Contrato de fornecimento n�o pode ser nulo.");
		}
		return query()
				.select("ordemcomprafornecimentocontrato.cdordemcomprafornecimentocontrato, ordemcompra.cdordemcompra, fornecimentocontrato.cdfornecimentocontrato")
				.join("ordemcomprafornecimentocontrato.ordemcompra ordemcompra")
				.join("ordemcomprafornecimentocontrato.fornecimentocontrato fornecimentocontrato")
				.where("fornecimentocontrato = ?",fornecimentocontrato)
				.list();
	}

}
