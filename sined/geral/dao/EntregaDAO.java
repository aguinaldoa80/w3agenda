package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Romaneiosituacao;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.EmitircurvaabcBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.TotalizadorEntrega;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.EmitircurvaabcFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.EntregaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

public class EntregaDAO extends GenericDAO<Entrega> {
		
	private UsuarioService usuarioService;
	private MaterialcategoriaService materialcategoriaService;
	private ParametrogeralService parametrogeralService;
	
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {this.materialcategoriaService = materialcategoriaService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Entrega> query) {
		query
			.select("entrega.cdentrega, aux_entrega.situacaosuprimentos, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
					"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, entrega.dtentrega, entrega.inspecaocompleta, " +
					"entrega.faturamentocliente, listaEntregadocumento.cdentregadocumento, listaEntregadocumento.numero, " +
					"listaEntregadocumento.valor, listaEntregadocumento.fornecedorAvaliado, fornecedor.cdpessoa, fornecedor.nome, " +
					"ordemcompra.cdordemcompra, arquivoxml.cdarquivo, arquivoxml.nome, entrega.dtbaixa," +
					"entregadocumentoreferenciado.cdentregadocumentoreferenciado, entregadocumentoreferenciado.modelodocumento, " +
					"entregadocumentoreferenciado.numero, entregadocumentoreferenciado.autenticacaobancaria, entregadocumentoreferenciado.valor, " +
					"entregadocumentoreferenciado.dtvencimento, entregadocumentoreferenciado.dtpagamento, edruf.cduf, edruf.sigla, "+
					"entregahistorico.cdentregahistorico, entregahistorico.dtaltera, " +
					"entregahistorico.cdusuarioaltera, entregahistorico.entregaacao ")
			.join("entrega.aux_entrega aux_entrega")
			.join("entrega.localarmazenagem localarmazenagem")
			.join("entrega.listaEntregadocumento listaEntregadocumento")
			.leftOuterJoin("listaEntregadocumento.listaEntregadocumentoreferenciado entregadocumentoreferenciado")
			.leftOuterJoin("entrega.listaEntregahistorico entregahistorico")
			.leftOuterJoin("entregadocumentoreferenciado.uf edruf")
			.leftOuterJoin("listaEntregadocumento.fornecedor fornecedor")
			.leftOuterJoin("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")
			.leftOuterJoin("listaOrdemcompraentrega.ordemcompra ordemcompra")
			.leftOuterJoin("entrega.empresa empresa")
			.leftOuterJoin("entrega.arquivoxml arquivoxml")
			.orderBy("entregahistorico.dtaltera desc");

	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Entrega> query,FiltroListagem _filtro) {
		EntregaFiltro filtro = (EntregaFiltro) _filtro;
		query
			.select("distinct entrega.cdentrega, entrega.dtentrega, aux_entrega.situacaosuprimentos, entrega.inspecaocompleta, " +
					"localarmazenagem.nome, entrega.romaneio ")
			.join("entrega.listaEntregadocumento entregadocumento")
			.join("entrega.aux_entrega aux_entrega")	
			.leftOuterJoin("entrega.localarmazenagem localarmazenagem")
			.where("entrega.cdentrega = ?",filtro.getIdentrega())
			.where("localarmazenagem = ?",filtro.getLocalarmazenagem())
			.where("entrega.dtentrega >= ?",filtro.getDtentrega1())
			.where("entrega.dtentrega <= ?",filtro.getDtentrega2())
			.where("entregadocumento.dtemissao >= ?",filtro.getDtemissao1())
			.where("entregadocumento.dtemissao <= ?",filtro.getDtemissao2())
			.where("entregadocumento.empresa = ?",filtro.getEmpresa())
			.where("entregadocumento.numero = ?", filtro.getNumerodocumento())
			
			.where("exists (select en.cdentrega from Entrega en join en.listaEntregadocumento doc " +
					"join doc.listaEntregamaterial where en.cdentrega =  entrega.cdentrega)")
			
			.ignoreJoin("listaEntregamaterial", "material", "listaOrdemcompraentrega", "fornecedor","listaEntregadocumento","entregadocumento",
					"ordemcompra", "rateio", "listaRateioitem", "projeto", "loteestoque","materialcategoria","vmaterialcategoria");
			
		
		if(filtro.getMaterial() != null || filtro.getMaterialgrupo() != null || filtro.getMaterialtipo() != null || filtro.getLoteestoque() != null 
				|| (filtro.getMaterialcategoria() != null)){
			query
				.leftOuterJoin("entregadocumento.listaEntregamaterial listaEntregamaterial")
				.leftOuterJoin("listaEntregamaterial.material material");
				if(filtro.getLoteestoque() != null){
					query
						.leftOuterJoin("listaEntregamaterial.loteestoque loteestoque")
						.where("loteestoque = ?",filtro.getLoteestoque());
				}
				if(filtro.getMaterial() != null){
					query
						.where("material = ?", filtro.getMaterial());
				}
				if(filtro.getMaterialgrupo() != null){
					query
						.where("material.materialgrupo = ?",filtro.getMaterialgrupo());
				}
				if(filtro.getMaterialtipo() != null){
					query
						.where("material.materialtipo = ?",filtro.getMaterialtipo());
				}
				if(filtro.getMaterialcategoria() != null){
					materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
					query
						.leftOuterJoin("material.materialcategoria materialcategoria")
						.leftOuterJoin("materialcategoria.vmaterialcategoria vmaterialcategoria")
						.openParentheses()
							.where("vmaterialcategoria.identificador like ? ||'.%'", filtro.getMaterialcategoria().getIdentificador())
						.or()
							.where("vmaterialcategoria.identificador like ? ", filtro.getMaterialcategoria().getIdentificador())
						.closeParentheses();
				}
		}
		
		if(filtro.getCdordemcompra() != null){
			query
				.leftOuterJoin("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")
				.leftOuterJoin("listaOrdemcompraentrega.ordemcompra ordemcompra")
				.where("ordemcompra.cdordemcompra = ?",filtro.getCdordemcompra());
		}
		
		if(filtro.getFornecedor() != null){
			query
				.leftOuterJoin("entregadocumento.fornecedor fornecedor")
				.where("fornecedor = ?", filtro.getFornecedor());
		}
		
		if(filtro.getProjeto() != null || filtro.getCentrocusto() != null){
			query
				.leftOuterJoin("entregadocumento.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.leftOuterJoin("listaRateioitem.projeto projeto")
				.where("listaRateioitem.centrocusto = ?",filtro.getCentrocusto())
				.where("projeto = ?", filtro.getProjeto());
		}
		
		
		if (filtro.getListaSituacao() != null) {
			query.whereIn("aux_entrega.situacaosuprimentos", Situacaosuprimentos.listAndConcatenate(filtro.getListaSituacao()));
		}
		
		boolean usuarioComRestricao = SinedUtil.getUsuarioLogado().getRestricaoFornecedorColaborador();
		Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
		if(usuarioComRestricao){
			query.where("not exists (select ed.cdentregadocumento" +
									" from entregadocumento ed" +
									" join ed.entrega ent" +
									" join ed.fornecedor f" +
									" join f.listaColaboradorFornecedor lcf " +
									" where ent.cdentrega = entrega.cdentrega " +
									" and not exists (select cf2.cdcolaboradorfornecedor" +
														" from ColaboradorFornecedor cf2 " +
														" where cf2.cdcolaboradorfornecedor = lcf.cdcolaboradorfornecedor" +
														" and cf2.colaborador = ?))", colaborador);
		}
		
		query.orderBy("entrega.cdentrega desc");
	}
	
	/**
	 * Carrega a lista de entrega para ser exibida na listagem
	 *
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Entrega> loadWithLista(String whereIn, String orderBy, boolean asc) {
		QueryBuilder<Entrega> query = query()
			.select("entrega.cdentrega, entrega.inspecaocompleta, fornecedor.nome, entrega.dtentrega, " +
					"listaEntregadocumento.valordesconto, listaEntregadocumento.valorfrete, localarmazenagem.nome, listaEntregadocumento.dtentrada, " +
					"listaEntregadocumento.dtemissao, listaEntregadocumento.valorseguro, listaEntregadocumento.valoroutrasdespesas, aux_entrega.situacaosuprimentos, entrega.dtentrega," +
					"listaEntregamaterial.qtde, listaEntregamaterial.valorunitario, material.nome, " +
					"listaEntregamaterial.csticms, cfop.codigo,listaEntregadocumento.valor, " +
					"entrega.romaneio, listaEntregamaterial.valorbcicms, listaEntregamaterial.valoricms, " +
					"listaEntregamaterial.valorbcicmsst, listaEntregamaterial.valoricmsst, listaEntregamaterial.valoroutrasdespesas, " +
					"listaEntregamaterial.valorseguro, listaEntregadocumento.tipotitulo, listaEntregadocumento.descricaotitulo, " +
					"listaEntregadocumento.numerotitulo, listaEntregamaterial.valoripi, listaEntregamaterial.valorpis, listaEntregamaterial.valorcofins, " +
					"listaEntregadocumento.serie, listaEntregadocumento.numero, " +
					"listaEntregadocumento.dtentrada, responsavelfrete.nome, ordemcompra.cdordemcompra, listaEntregadocumento.fornecedorAvaliado, " +
					"listaEntregadocumento.retornowms, listaEntregamaterial.valordesconto, listaEntregamaterial.faturamentocliente, " +
					"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.nome, rateioitem.valor, " +
					"contagerencial.nome ")
			.leftOuterJoin("entrega.localarmazenagem localarmazenagem")
			.join("entrega.listaEntregadocumento listaEntregadocumento")
			.leftOuterJoin("listaEntregadocumento.rateio rateio")
			.leftOuterJoin("rateio.listaRateioitem rateioitem")
			.leftOuterJoin("rateioitem.contagerencial contagerencial")
			.leftOuterJoin("listaEntregadocumento.modelodocumentofiscal modelodocumentofiscal")
			.leftOuterJoin("listaEntregadocumento.fornecedor fornecedor")
			.leftOuterJoin("listaEntregadocumento.responsavelfrete responsavelfrete")
			.join("listaEntregadocumento.listaEntregamaterial listaEntregamaterial")
			.leftOuterJoin("listaEntregamaterial.cfop cfop")
			.leftOuterJoin("listaEntregamaterial.material material")
			.leftOuterJoin("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")
			.leftOuterJoin("listaOrdemcompraentrega.ordemcompra ordemcompra")
			.join("entrega.aux_entrega aux_entrega")
			.whereIn("entrega.cdentrega", whereIn);
		
		
//		Material, CFOP, CST, quantidade e valor
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		}else{
			query.orderBy("entrega.cdentrega desc");
		}
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioService.isRestricaoFornecedorColaborador(usuarioLogado)){
			query.where("("+
							"exists("+
								"select 1 from ColaboradorFornecedor cf "+
								"where "+
								"cf.fornecedor =  fornecedor and "+
								"cf.colaborador = "+usuarioLogado.getCdpessoa()+
							") "+
							"or "+
							"not exists("+
								"select 1 from ColaboradorFornecedor cf "+
								"where "+
								"cf.fornecedor =  fornecedor"+
							")"+
						")");
		}
		
		return query.list();
	}
	
	/**
	 * Busca as informa��es para a listagem do CSV.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Entrega> findForCsv(EntregaFiltro filtro){
		QueryBuilderSined<Entrega> query = (QueryBuilderSined<Entrega>) querySined();
		query.setUseWhereFornecedorEmpresa(true);
		updateListagemQuery(query, filtro);
		return query.list();
	}
	
	/**
	 * Carrega a lista a partir de uma ordem de compra.
	 * 
	 * @param ordemcompra
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Entrega> findByOrdemcompra(Ordemcompra ordemcompra){
		if (ordemcompra == null || ordemcompra.getCdordemcompra() == null) {
			throw new SinedException("Ordem de compra n�o pode ser nulo.");
		}
		return query()
					.select("entrega.cdentrega, listaEntregadocumento.cdentregadocumento, entrega.dtentrega, entrega.dtcancelamento, entregamaterial.qtde, entregamaterial.valorunitario," +
							"listaEntregadocumento.valorfrete, listaEntregadocumento.valordesconto, material.cdmaterial, documento.cddocumento, entregamaterial.valorfrete, entregamaterial.valordesconto")
					.leftOuterJoin("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")
					.leftOuterJoin("listaOrdemcompraentrega.ordemcompra ordemcompra")
					.leftOuterJoin("entrega.listaEntregadocumento listaEntregadocumento")
					.leftOuterJoin("listaEntregadocumento.listaEntregamaterial entregamaterial")
					.leftOuterJoin("entregamaterial.material material")
					.leftOuterJoin("entrega.listaDocumentoorigem listaDocumentoorigem")
					.leftOuterJoin("listaDocumentoorigem.documento documento")
					.where("ordemcompra = ?", ordemcompra)
					.where("entrega.dtcancelamento is null")
					.orderBy("entrega.dtentrega desc")
					.list();
	}
	
	/**
	 * Carrega a lista de entregas com a situa��o preenchidas.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @author Tom�s Rabelo
	 */
	public List<Entrega> loadWithSituacao(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Entregas n�o podem ser nulas.");
		}
		return querySined()
			.setUseReadOnly(false)
			.select("entrega.cdentrega, entrega.dtentrega, entrega.inspecaocompleta, aux_entrega.situacaosuprimentos, listadocumento.numero, ordemcompra.cdordemcompra, " +
					"material.cdmaterial, listaEntregamaterial.qtde, entrega.faturamentocliente, fornecedor.cdpessoa, fornecedor.nome, localarmazenagem.cdlocalarmazenagem, " +
					"cotacao.cdcotacao, listaEntregadocumento.cdentregadocumento, listaEntregadocumento.simplesremessa, listaEntregadocumento.fornecedorAvaliado, empresa.cdpessoa ")
			.join("entrega.aux_entrega aux_entrega")
			.join("entrega.listaEntregadocumento listaEntregadocumento")
			.join("listaEntregadocumento.fornecedor fornecedor")
			.join("entrega.localarmazenagem localarmazenagem")
			.join("listaEntregadocumento.listaEntregamaterial listaEntregamaterial")
			.join("listaEntregamaterial.material material")
			.leftOuterJoin("listaEntregadocumento.listadocumento listadocumento")
			.leftOuterJoin("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")
			.leftOuterJoin("listaOrdemcompraentrega.ordemcompra ordemcompra")
			.leftOuterJoin("ordemcompra.cotacao cotacao")
			.leftOuterJoin("entrega.empresa empresa")
			.whereIn("entrega.cdentrega", whereIn)
			.list();
	}

	/**
	 * Atualiza as datas das entregas passadas por par�metro.
	 * 
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void updateCancelada(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Entregas n�o podem ser nulas.");
		}
		getHibernateTemplate().bulkUpdate("update Entrega e set dtcancelamento = ? where e.id in ("+whereIn+")", 
				new Date(System.currentTimeMillis()));
		
		getHibernateTemplate().bulkUpdate("update Entregadocumento ed set entregadocumentosituacao = ? where ed.entrega.cdentrega in ("+whereIn+")", 
				Entregadocumentosituacao.CANCELADA);
		
	}

	/**
	 * Atualiza as entregas fazendo o estorno das mesmas.
	 * 
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void updateEstorno(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Entregas n�o podem ser nulas.");
		}
		getHibernateTemplate().bulkUpdate("update Entrega e set dtcancelamento = null where e.id in ("+whereIn+")");
		
		getHibernateTemplate().bulkUpdate("update Entregadocumento ed set entregadocumentosituacao = ? where ed.entrega.cdentrega in ("+whereIn+")", 
				Entregadocumentosituacao.REGISTRADA);
	}

	/**
	 * Carrega a entrega para o faturamento.
	 * 
	 * @param cdentrega
	 * @return
	 * @author Rodrigo Freitas
	 * @author Tom�s Rabelo
	 */
	public Entrega findForFaturar(String cdentrega) {
		if (cdentrega == null || cdentrega.equals("")) {
			throw new SinedException("Entrega n�o pode ser nula.");
		}
		return query()
			.select("entrega.cdentrega, entrega1.cdentrega, listaEntregadocumento.valordesconto, listaEntregadocumento.numero, listaEntregadocumento.cdentregadocumento," +
					" listaEntregadocumento.dtemissao, listaEntregadocumento.valorfrete, listaEntregadocumento.simplesremessa, listaEntregadocumento.dtentrada, " +
					"empresa.cdpessoa, listadocumento.numero, fornecedor.cdpessoa, listadocumento.dtemissao, rateio1.cdrateio," +
					"listadocumento.dtvencimento, listadocumento.valor, contagerencial.cdcontagerencial, rateio.cdrateio, " +
					"listaEntregamaterial.valorunitario, listaEntregamaterial.qtde, listaEntregamaterial.valordesconto, listaEntregamaterial.valorfrete," +
					" prazopagamento.cdprazopagamento, documentoantecipacao.cddocumento, documentoantecipacao.valor, " +
					"ordemcompra.desconto, ordemcompra.frete, material.cdmaterial, empresaOrdem.cdpessoa, documentotipo.cddocumentotipo, contadestino.cdconta, listaRateioitem.valor," +
					"empresa2.cdpessoa, empresa2.nome, " +
					"listaApropriacao.cdEntregaDocumentoApropriacao, listaApropriacao.mesAno, listaApropriacao.valor")
			.join("entrega.listaEntregadocumento listaEntregadocumento")
			.leftOuterJoin("listaEntregadocumento.empresa empresa2")
			.leftOuterJoin("listaEntregadocumento.entrega entrega1")
			.leftOuterJoin("listaEntregadocumento.prazopagamento prazopagamento")
			.leftOuterJoin("listaEntregadocumento.documentotipo documentotipo")
			.leftOuterJoin("documentotipo.contadestino contadestino")
			.leftOuterJoin("listaEntregadocumento.listadocumento listadocumento")
			.leftOuterJoin("listadocumento.documentoantecipacao documentoantecipacao")
			.leftOuterJoin("listaEntregadocumento.listaEntregamaterial listaEntregamaterial")
			.leftOuterJoin("listaEntregamaterial.material material")
			.leftOuterJoin("material.contagerencial contagerencial")
			.leftOuterJoin("listaEntregadocumento.listaApropriacao listaApropriacao")
			.leftOuterJoin("entrega.empresa empresa")
			.leftOuterJoin("listaEntregadocumento.fornecedor fornecedor")
			.leftOuterJoin("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")
			.leftOuterJoin("listaOrdemcompraentrega.ordemcompra ordemcompra")
			.leftOuterJoin("ordemcompra.empresa empresaOrdem")
			.leftOuterJoin("ordemcompra.rateio rateio")
			.leftOuterJoin("listaEntregadocumento.rateio rateio1")
			.leftOuterJoin("rateio1.listaRateioitem listaRateioitem")
			.where("entrega.cdentrega = ?", new Integer(cdentrega))
			.orderBy("listadocumento.dtvencimento, listadocumento.cdentregapagamento")
			.unique();
	}
	
	/**
	* Retorna Lista de Entregas para o faturamento
	*
	* @param whereInCdEntrega
	* @return
	* @since Jul 14, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Entrega> findForFaturarList(String whereInCdEntrega) {
		if (whereInCdEntrega == null || whereInCdEntrega.equals("")) {
			throw new SinedException("Entrega n�o pode ser nula.");
		}
		return query()
			.select("entrega.cdentrega, entrega.desconto, entrega.acrescimo, entrega.dtemissao, entrega.frete, empresa.cdpessoa, listadocumento.numero, fornecedor.cdpessoa, listadocumento.dtemissao, " +
					"listadocumento.dtvencimento, listadocumento.valor, contagerencial.cdcontagerencial, rateio.cdrateio, " +
					"c1.cdcentrocusto, p1.cdprojeto, listaEntregamaterial.valorunitario, listaEntregamaterial.qtde, prazopagamento.cdprazopagamento, " +
					"ordemcompra.desconto, ordemcompra.frete, material.cdmaterial, empresaOrdem.cdpessoa, documentotipo.cddocumentotipo")
			.leftOuterJoin("entrega.prazopagamento prazopagamento")
			.leftOuterJoin("entrega.documentotipo documentotipo")
			.leftOuterJoin("entrega.listadocumento listadocumento")
			.leftOuterJoin("entrega.listaEntregamaterial listaEntregamaterial")
			.leftOuterJoin("listaEntregamaterial.material material")
			.leftOuterJoin("material.contagerencial contagerencial")
			.leftOuterJoin("entrega.empresa empresa")
			.leftOuterJoin("entrega.fornecedor fornecedor")
			.leftOuterJoin("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")
			.leftOuterJoin("listaOrdemcompraentrega.ordemcompra ordemcompra")
			.leftOuterJoin("ordemcompra.empresa empresaOrdem")
			.leftOuterJoin("ordemcompra.rateio rateio")
			.leftOuterJoin("entrega.centrocusto c1")
			.leftOuterJoin("entrega.projeto p1")
			.whereIn("entrega.cdentrega", whereInCdEntrega)
			.orderBy("listadocumento.cdentregapagamento")
			.list();
	}

	/**
	 * Carrega a lista de entregas com dados para a baixa das entregas.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Entrega> findForBaixa(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Entregas n�o podem ser nulas.");
		}
		return query()
					.select("entrega.cdentrega, listaEntregadocumento.cdentregadocumento, material.cdmaterial, material.produto, material.patrimonio, material.servico, material.epi, " +
							"localarmazenagem.cdlocalarmazenagem, material.nome, material.identificacao, localarmazenagem.nome, listaEntregamaterial.qtde, listaEntregamaterial.valorunitario, " +
							"listaEntregamaterial.cdentregamaterial, fornecedor.cdpessoa, materialclasse.cdmaterialclasse, materialclasse.nome, listaEntregamaterial.comprimento, " +
							"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, localarmazenagemmaterial.cdlocalarmazenagem, projeto.cdprojeto, projeto.nome, " +
							"centrocusto.cdcentrocusto, centrocusto.nome, loteestoque.cdloteestoque, " +
							"listaEntregamaterial.valoripi, listaEntregamaterial.valoricmsst, entrega.dtentrega, listaEntregadocumento.dtentrada," +
							"materialgrupo.cdmaterialgrupo, materialgrupo.gradeestoquetipo, materialmestregrade.cdmaterial, materialmestregrade.nome," +
							"unidademedida.cdunidademedida, unidademedidacomercial.cdunidademedida, unidademedidacomercial.nome, unidademedidacomercial.simbolo," +
							"unidademedidacomercial.casasdecimaisestoque, listaEntregamaterial.whereInMaterialItenGrade, listaEntregamaterial.tipolote, " +
							"listaEntregamaterial.pesomedio, cfop.cdcfop, cfop.codigo, contaGerencialCfop.cdcontagerencial, " +
							"materialRateioEstoque.cdMaterialRateioEstoque, contaGerencialEntrada.cdcontagerencial, " +
							"listaMaterialrelacionado.cdmaterialrelacionado, listaMaterialrelacionado.desmontarKitRecebimento, listaMaterialrelacionado.quantidade, " +
							"listaMaterialrelacionado.porcentagemCustoUnitario, listaMaterialrelacionado.porcentagemCustoGeral, materialRelacionado.cdmaterial, " +
							"materialRelacionado.nome, materialRelacionado.identificacao, material.vendapromocional, unidademedidaMaterialRelacionado.cdunidademedida")
					.join("entrega.listaEntregadocumento listaEntregadocumento")
					.leftOuterJoin("listaEntregadocumento.listaEntregamaterial listaEntregamaterial")
					.leftOuterJoin("entrega.empresa empresa")
					.leftOuterJoin("listaEntregamaterial.material material")
					.leftOuterJoin("material.unidademedida unidademedida")
					.leftOuterJoin("material.listaMaterialrelacionado listaMaterialrelacionado")
					.leftOuterJoin("listaMaterialrelacionado.materialpromocao materialRelacionado")
					.leftOuterJoin("materialRelacionado.unidademedida unidademedidaMaterialRelacionado")
					.leftOuterJoin("material.materialRateioEstoque materialRateioEstoque")
					.leftOuterJoin("materialRateioEstoque.contaGerencialEntrada contaGerencialEntrada")
					.leftOuterJoin("listaEntregamaterial.unidademedidacomercial unidademedidacomercial")
					.leftOuterJoin("material.materialgrupo materialgrupo")
					.leftOuterJoin("material.materialmestregrade materialmestregrade")
					.leftOuterJoin("listaEntregamaterial.materialclasse materialclasse")
					.leftOuterJoin("listaEntregamaterial.localarmazenagem localarmazenagemmaterial")
					.leftOuterJoin("listaEntregamaterial.projeto projeto")
					.leftOuterJoin("listaEntregamaterial.centrocusto centrocusto")
					.leftOuterJoin("listaEntregamaterial.loteestoque loteestoque")
					.leftOuterJoin("listaEntregamaterial.cfop cfop")
					.leftOuterJoin("cfop.contaGerencial contaGerencialCfop")
					.join("entrega.localarmazenagem localarmazenagem")
					.join("listaEntregadocumento.fornecedor fornecedor")
					.whereIn("entrega.cdentrega", whereIn)
					.list();
		
//		.select("entregamaterial.cdentregamaterial, material.cdmaterial, unidademedida.cdunidademedida, entregamaterial.qtde," +
//				"unidademedidacomercial.cdunidademedida, loteestoque.cdloteestoque," +
//				"entregamaterial.valoripi,entregamaterial.valorfrete,entregamaterial.valoricmsst,entregamaterial.valorseguro,"+
//				"entregamaterial.valoroutrasdespesas,entregamaterial.valordesconto")
//		.join("entregamaterial.entregadocumento entregadocumento")
//		.join("entregadocumento.entrega entrega")
//		.join("entregamaterial.material material")
//		.join("material.unidademedida unidademedida")
//		.join("entregamaterial.unidademedidacomercial unidademedidacomercial")
//		.leftOuterJoin("entregamaterial.loteestoque loteestoque")
//		.where("entrega = ?", entrega)
//		.where("material = ?", material)
	}

	/**
	 * Faz o update na data da baixa das entregas.
	 *
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void updateBaixa(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Entregas n�o podem ser nulas.");
		}
		getHibernateTemplate().bulkUpdate("update Entrega e set dtbaixa = ? where e.id in ("+whereIn+")", 
				new Date(System.currentTimeMillis()));
	}
	
	/**
	 * M�todo que atualiza o dtbaixa das entregas
	 *
	 * @param dtbaixa
	 * @param whereIn
	 * @author Luiz Fernando
	 */
	public void updateDtBaixa(Date dtbaixa, String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Entregas n�o podem ser nulas.");
		}
		getHibernateTemplate().bulkUpdate("update Entrega e set dtbaixa = " + (dtbaixa == null ? "null" : dtbaixa) + " where e.id in ("+whereIn+")");
	}

	/**
	 * Verifica se tem alguma entrega n�o baixada no whereIn.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean entregaNotBaixada(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Par�metro incorreto.");
		}
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(Entrega.class)
					.join("entrega.aux_entrega aux_entrega")
					.where("aux_entrega.situacaosuprimentos <> ?", Situacaosuprimentos.BAIXADA)
					.whereIn("entrega.cdentrega", whereIn)
					.unique() > 0;
	}

	/**
	 * M�todo que busca todas as entregas da ordem de compra menos a que est� em quest�o.
	 * 
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Entrega> findEntregasDaOrdemCompra(Entrega entrega, Ordemcompra ordemcompra) {
		return query()
			.select("entrega.cdentrega, entrega.dtcancelamento, entregamaterial.qtde, material.cdmaterial")
			.join("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")
			.join("listaOrdemcompraentrega.ordemcompra ordemcompra")
			.join("entrega.listaEntregamaterial entregamaterial")
			.join("entregamaterial.material material")
			.where("ordemcompra = ?", ordemcompra)
			.where("entrega <> ?", entrega)
			.where("entrega.dtcancelamento is null")
			.list();
	}

	/**
	 * M�todo que carrega a lista contendo os materiais e os itens da inspe��o
	 * 
	 * @param cdentrega
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Entrega carregaEntregaParaInspecao(String cdentrega) {
		if(cdentrega == null || cdentrega.equals("")){
			throw new SinedException("O c�digo da entrega n�o pode ser nulo.");
		}
		return query()
			.select("entrega.cdentrega, listaEntregadocumento.cdentregadocumento, entrega.inspecaocompleta, listaEntregamaterial.cdentregamaterial, listaEntregamaterial.qtde, material.cdmaterial, material.nome, material.identificacao, " +
					"listaInspecao.cdentregamaterialinspecao, listaInspecao.observacao, listaInspecao.conforme, inspecaoitem.cdinspecaoitem, inspecaoitem.nome, unidademedida.simbolo, " +
					"entregamaterial.cdentregamaterial, listaEntregadocumento.numero, fornecedor.nome, fornecedor.cdpessoa")
			.join("entrega.listaEntregadocumento listaEntregadocumento")
			.join("listaEntregadocumento.listaEntregamaterial listaEntregamaterial")
			.join("listaEntregamaterial.listaInspecao listaInspecao")
			.join("listaInspecao.entregamaterial entregamaterial")
			.join("listaEntregamaterial.material material")
			.join("material.unidademedida unidademedida")
			.leftOuterJoin("listaInspecao.inspecaoitem inspecaoitem")
			.leftOuterJoin("listaEntregadocumento.fornecedor fornecedor")
			.where("entrega.cdentrega = ?", new Integer(cdentrega))
			.orderBy("material.nome, inspecaoitem.nome")
			.unique();
	}

	/**
	 * M�todo que atualiza a entrega passando a ter inspe�� completa
	 * 
	 * @param entrega
	 * @author Tom�s Rabelo
	 */
	public void doUpdateInspecaoEntrega(Entrega entrega) {
		Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		
		StringBuilder sql = new StringBuilder()
			.append("update Entrega e set e.cdusuarioaltera = ?, e.dtaltera = ?, e.inspecaocompleta = ? " +
					"where e.cdentrega = "+entrega.getCdentrega());
		
		getHibernateTemplate().bulkUpdate(sql.toString(), new Object[]{cdusuarioaltera,hoje, Boolean.TRUE});
	}

	/**
	 * M�todo que gera relat�rio contendo informa��es das inspe��es das entregas
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Entrega> gerarRelatorioInspecaoEntrega(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item foi selecionado.");
		}
		return querySined()
				
			.select("entrega.cdentrega, listaEntregadocumento.cdentregadocumento, material.cdmaterial, material.nome, material.identificacao, listaEntregamaterial.qtde, unidademedida.simbolo, " +
					"listaInspecao.conforme, listaInspecao.observacao, inspecaoitem.nome")
			.join("entrega.listaEntregadocumento listaEntregadocumento")
			.join("listaEntregadocumento.listaEntregamaterial listaEntregamaterial")
			.join("listaEntregamaterial.listaInspecao listaInspecao")
			.join("listaEntregamaterial.material material")
			.join("material.unidademedida unidademedida")
			.join("listaInspecao.inspecaoitem inspecaoitem")
			.whereIn("entrega.cdentrega",whereIn)
			.orderBy("entrega.cdentrega desc, material.nome, inspecaoitem.nome")
			.list();
	}
	
	/**
	 * Chama a procedure de atualiza��o da tabela auxiliar de entrega
	 *
	 * @param cotacao
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaEntrega(Entrega entrega){
		if (entrega == null || entrega.getCdentrega() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		getJdbcTemplate().execute("SELECT ATUALIZA_ENTREGA(" + entrega.getCdentrega() + ", '" + NeoWeb.getRequestContext().getServletRequest().getContextPath() + "')");
	}

	/**
	 * M�todo que carrega todas as entregas da cota��o em quest�o.
	 * 
	 * @param cotacao
	 * @param entrega
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Entrega> getEntregasDaCotacao(Cotacao cotacao, Entrega entrega) {
		Entrega entrega2 = null;
		if(entrega.getCdentrega() != null){
			entrega2 = entrega;
		}
		
		return query() 
			.select("entrega.cdentrega, listaEntregadocumento.cdentregadocumento, material.cdmaterial, listaEntregamaterial.qtde, cotacao.cdcotacao")
			.join("entrega.listaEntregadocumento listaEntregadocumento")
			.join("listaEntregadocumento.listaEntregamaterial listaEntregamaterial")
			.join("entrega.aux_entrega aux_entrega")
			.join("listaEntregamaterial.material material")
			.join("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")
			.join("listaOrdemcompraentrega.ordemcompra ordemcompra")
			.join("ordemcompra.cotacao cotacao")
			.where("cotacao = ?", cotacao)
			.where("aux_entrega.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
			.where("entrega <> ?", entrega2)
			.list();
	}

	/**
	 * M�todo que busca as entregas selecionadas na listagem para gerar romaneio
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Entrega> getEntregasParaRomaneio(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item foi selecionado.");
		}
		
		return query()
			.select("entrega.cdentrega, localarmazenagem.cdlocalarmazenagem, entrega.romaneio")
			.join("entrega.localarmazenagem localarmazenagem")
			.whereIn("entrega.cdentrega", whereIn)
			.list();
	}

	/**
	 * M�todo que busca todos os locais de entrega das solicita��es referentes as entregas.
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	@SuppressWarnings("unchecked")
	public List<Localarmazenagem> getLocaisArmazenamentoDasSolicitacoesReferentesAsEntregas(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item foi selecionado.");
		}

		SinedUtil.markAsReader();
		List<Localarmazenagem> list = getJdbcTemplate().query(
				"SELECT DISTINCT(L.CDLOCALARMAZENAGEM), L.NOME "+
				"FROM ENTREGA E "+
				"JOIN ENTREGAMATERIAL EM ON EM.CDENTREGA = E.CDENTREGA "+
				"JOIN MATERIAL M ON M.CDMATERIAL = EM.CDMATERIAL "+
				"JOIN ORDEMCOMPRA O ON O.CDORDEMCOMPRA = E.CDORDEMCOMPRA "+ 
				"JOIN COTACAO C ON C.CDCOTACAO = O.CDCOTACAO "+
				"JOIN COTACAOFORNECEDOR CF ON CF.CDCOTACAO = C.CDCOTACAO "+
				"JOIN COTACAOFORNECEDORITEM CFI ON CFI.CDCOTACAOFORNECEDOR = CF.CDCOTACAOFORNECEDOR "+ 
				"JOIN LOCALARMAZENAGEM L ON L.CDLOCALARMAZENAGEM = CFI.CDLOCALARMAZENAGEMSOLICITACAO "+ 
				"JOIN MATERIAL MCFI ON MCFI.CDMATERIAL = CFI.CDMATERIAL " +
				"WHERE E.CDENTREGA IN ("+whereIn+") AND " +
				"E.CDFORNECEDOR = CF.CDFORNECEDOR AND "+
				"M.CDMATERIAL = MCFI.CDMATERIAL AND "+
				"CFI.CDLOCALARMAZENAGEM <> CFI.CDLOCALARMAZENAGEMSOLICITACAO"
				,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						Localarmazenagem localarmazenagem = new Localarmazenagem(rs.getInt("CDLOCALARMAZENAGEM"), rs.getString("NOME"));
						return localarmazenagem;
					}
				});
		return list;
	}

	/**
	 * M�todo que busca as entregas utilizadas para gerar o romaneio
	 * 
	 * @param whereInEntregas
	 * @param whereInSolicitacoes
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Entrega> getEntregasUtilizadasParaGerarRomaneio(String whereInEntregas, String whereInSolicitacoes) {
		if(whereInEntregas == null || whereInEntregas.equals("")){
			throw new SinedException("Nenhuma entrega.");
		}
		if(whereInSolicitacoes == null || whereInSolicitacoes.equals("")){
			throw new SinedException("Nenhuma solicita��o.");
		}
		
		return query()
			.select("entrega.cdentrega, empresa.cdpessoa")
			.join("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")
			.join("listaOrdemcompraentrega.ordemcompra ordemcompra")
			.join("ordemcompra.cotacao cotacao")
			.join("cotacao.listaOrigem listaOrigem")
			.join("listaOrigem.solicitacaocompra solicitacaocompra")
			.leftOuterJoin("entrega.empresa empresa")
			.whereIn("solicitacaocompra.cdsolicitacaocompra", whereInSolicitacoes)
			.whereIn("entrega.cdentrega", whereInEntregas)
			.list();
	}

	/**
	 * M�todo que calcula a quantidade de material que foi entregue referente a ordem de compra.
	 * 
	 * @param ordemcompra
	 * @param material
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Long getQtdeEntregueDoMaterialDaOrdemCompra(Ordemcompra ordemcompra, Material material) {
		return newQueryBuilderWithFrom(Long.class)
			.select("count(*)")
			.join("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")
			.join("listaOrdemcompraentrega.ordemcompra ordemcompra")
			.join("entrega.listaEntregamaterial listaEntregamaterial")
			.join("entrega.aux_entrega aux_entrega")
			.join("listaEntregamaterial.material material")
			.where("ordemcompra = ?", ordemcompra)
			.where("material = ?", material)
			.where("aux_entrega.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
			.unique();
	}
	
	@SuppressWarnings("unchecked")
	public List<EmitircurvaabcBean> listEntregaForCurvaABC (EmitircurvaabcFiltro filtro, boolean forPdf){
		if(filtro == null || filtro.getEmpresa() == null){
			throw new SinedException("O campo 'Empresa' n�o pode ser nulo.");
		}
		
		String inicio = new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtInicio());
		
		String strProjeto = "";
		Integer intEmpresa = 0;
		String dtinicio = "";
		String dtfim = "";
		String strFornecedor = "";
		String strMaterialgrupo = "";
		String strClasseMaterial = "";
		String strDevolucao = "";
		
		if(filtro.getEmpresa() != null){
			intEmpresa = filtro.getEmpresa().getCdpessoa();
		}

		if(filtro.getProjeto() != null){
			strProjeto = "AND (ET.CDPROJETO = " + filtro.getProjeto().getCdprojeto() + " OR " + 
							"EXISTS( SELECT RI.CDRATEIOITEM FROM RATEIOITEM RI WHERE ED.CDRATEIO = RI.CDRATEIO AND RI.CDPROJETO = " + filtro.getProjeto().getCdprojeto() + " )) ";
		}
		if(filtro.getFornecedor() != null ){
			strFornecedor = "AND ED.CDFORNECEDOR = " + filtro.getFornecedor().getCdpessoa() + " ";
		}
		if(filtro.getMaterialgrupo() != null){
			strMaterialgrupo = "AND M.CDMATERIALGRUPO = " + filtro.getMaterialgrupo().getCdmaterialgrupo() + " ";
		}
		
		if(filtro.getDtInicio() != null){
			dtinicio = " TO_DATE('" + inicio + "', 'dd/mm/yyyy') ";
			
		}

		if(filtro.getDtFim() != null){
			String fim = new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtFim());
			dtfim = " AND TO_DATE('" + fim + "', 'dd/mm/yyyy') ";
			
		}else {
			dtfim = " AND TO_DATE('31/12/2222', 'dd/mm/yyyy') ";
		}
		
		if(filtro.getProduto() != null && filtro.getProduto() ){
			strClasseMaterial += "";
		}
		
		if (filtro.getProduto() || filtro.getServico() || filtro.getEpi() || filtro.getPatrimonio()) {
			strClasseMaterial += " AND ( ";
			if (filtro.getProduto()) {
				strClasseMaterial += " M.PRODUTO = " + filtro.getProduto() + " ";
			}
			if (filtro.getServico()) {
				if (filtro.getProduto()) {
					strClasseMaterial += " OR ";
				}
				strClasseMaterial += " M.SERVICO = " + filtro.getServico() + " ";
			}
			if (filtro.getEpi()) {
				if (filtro.getProduto() || filtro.getServico()) {
					strClasseMaterial += " OR ";
				}
				strClasseMaterial += " M.EPI = " + filtro.getEpi() + " ";
			}
			if (filtro.getPatrimonio()) {
				if (filtro.getProduto() || filtro.getServico() || filtro.getEpi()) {
					strClasseMaterial += " OR ";
				}
				strClasseMaterial += " M.PATRIMONIO = " + filtro.getPatrimonio() + " ";
			}
			strClasseMaterial += " ) ";
		}
		
		strDevolucao = "AND (SELECT COUNT(*) FROM ENTREGADOCUMENTOHISTORICO EDH WHERE ED.CDENTREGADOCUMENTO=EDH.CDENTREGADOCUMENTO AND LOWER(EDH.OBSERVACAO) LIKE '%/faturamento/crud/notafiscalproduto?acao=consultar&cdnota=%')=0";
		
		String groupBy = "";
		String valorConsumo;
		boolean valordocumento_curvaabc = "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.VALORDOCUMENTO_CURVAABC));
		
		if (valordocumento_curvaabc){
			valorConsumo = "COALESCE(ED.VALOR, 0) / 100";
		}else {
//			valorConsumo = "qtde_unidadeprincipal(EM.CDMATERIAL, EM.CDUNIDADEMEDIDACOMERCIAL, EM.QTDE) * valor_unidadeprincipal(EM.CDMATERIAL, EM.CDUNIDADEMEDIDACOMERCIAL, EM.VALORUNITARIO)";
			valorConsumo = "COALESCE(EM.QTDE, 1) * COALESCE(EM.VALORUNITARIO, 0)";
		}		
		
		String select = " M.CDMATERIAL AS CD_CLASSIFICACAO,  COALESCE(M.IDENTIFICACAO, '') || (CASE WHEN M.IDENTIFICACAO IS NOT NULL THEN ' - ' ELSE '' END) || M.NOME AS NOME_MATERIAL,  valor_unidadeprincipal(EM.CDMATERIAL, EM.CDUNIDADEMEDIDACOMERCIAL, EM.VALORUNITARIO) AS VALOR_UNITARIO,  qtde_unidadeprincipal(EM.CDMATERIAL, EM.CDUNIDADEMEDIDACOMERCIAL, EM.QTDE) AS QUANTIDADE, " + valorConsumo + " AS VALOR_CONSUMO, " 
			+ " ET.DTENTREGA AS DTINICIO, ET.CDEMPRESA AS CD_EMPRESA, ET.CDPROJETO AS CD_PROJETO, M.PESO AS PESO " ;
		
		if(forPdf){
			groupBy = " GROUP BY M.CDMATERIAL, M.NOME, M.IDENTIFICACAO, M.VALORCUSTO, M.PESO ";
			select = " M.CDMATERIAL AS CD_CLASSIFICACAO,  COALESCE(M.IDENTIFICACAO, '') || (CASE WHEN M.IDENTIFICACAO IS NOT NULL THEN ' - ' ELSE '' END) || M.NOME AS NOME_MATERIAL,  M.VALORCUSTO AS VALOR_UNITARIO,  sum(qtde_unidadeprincipal(EM.CDMATERIAL, EM.CDUNIDADEMEDIDACOMERCIAL, EM.QTDE)) AS QUANTIDADE, sum(" + valorConsumo + ") AS VALOR_CONSUMO, " 
				+ " NULL AS DTINICIO, NULL AS CD_EMPRESA, NULL AS CD_PROJETO, M.PESO AS PESO" ;
		}
		
		String strFornecedorVinculados = "";
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioService.isRestricaoFornecedorColaborador(usuarioLogado)){
			strFornecedorVinculados += (" AND ("+
							"EXISTS("+
								"SELECT 1 FROM COLABORADORFORNECEDOR CF "+
								"WHERE "+
								"CF.CDFORNECEDOR =  ED.CDFORNECEDOR AND "+
								"CF.CDCOLABORADOR = "+usuarioLogado.getCdpessoa()+
							") "+
							"OR "+
							"NOT EXISTS("+
								"SELECT 1 FROM COLABORADORFORNECEDOR CF "+
								"WHERE "+
								"CF.CDFORNECEDOR = ED.CDFORNECEDOR"+
							")"+
						")");
		}
		
		String strjoinsCategoria = "";
		String strMaterialCategoria = "";
		if(filtro.getMaterialcategoria() != null 
				&& filtro.getMaterialcategoria().getIdentificador() != null){
			
			strjoinsCategoria  = " LEFT OUTER JOIN MATERIALCATEGORIA MC ON MC.CDMATERIALCATEGORIA = M.CDMATERIALCATEGORIA						" + 
				  "LEFT OUTER JOIN VMATERIALCATEGORIA VC ON VC.CDMATERIALCATEGORIA = MC.CDMATERIALCATEGORIA										" ;
			strMaterialCategoria = " AND (VC.IDENTIFICADOR LIKE '"+ filtro.getMaterialcategoria().getIdentificador() +".%' " +
					" OR VC.IDENTIFICADOR LIKE '"+ filtro.getMaterialcategoria().getIdentificador() +"' )";
		}
		
		String whereInEmpresa = new SinedUtil().getListaEmpresa();
		
		String sql = "SELECT " 
					+ select
				    + " FROM ENTREGA ET "
				    + " JOIN AUX_ENTREGA AUX_ET ON AUX_ET.CDENTREGA = ET.CDENTREGA "
				    + " JOIN ENTREGADOCUMENTO ED ON ET.CDENTREGA = ED.CDENTREGA "
				    + " JOIN ENTREGAMATERIAL EM ON ED.CDENTREGADOCUMENTO = EM.CDENTREGADOCUMENTO " 
				    + " LEFT OUTER JOIN MATERIAL M ON EM.CDMATERIAL = M.CDMATERIAL "
				    + strjoinsCategoria
				    + " WHERE (ET.CDEMPRESA = " + intEmpresa + " OR (ET.CDEMPRESA IS NULL AND ED.CDEMPRESA = " + intEmpresa + ")) "
				    + " AND AUX_ET.SITUACAO IN (" + Situacaosuprimentos.FATURADA.ordinal() + "," + Situacaosuprimentos.BAIXADA.ordinal() + ") " 
				    + (SinedUtil.isRestricaoEmpresaFornecedorUsuarioLogado() ?
				    		" AND ED.CDFORNECEDOR in ( select fRestricao.cdpessoa " +
				    							"from Fornecedor fRestricao " +
				    							"left outer join fornecedorempresa feRestricao on feRestricao.cdfornecedor= fRestricao.cdpessoa " +
				    							"where feRestricao.cdempresa is null or feRestricao.cdempresa in (" + whereInEmpresa + ")) " : "")
				    + strMaterialCategoria
				    + strProjeto
				    + strMaterialgrupo
				    + strFornecedor
				    + strClasseMaterial
				    + strDevolucao
				    + strFornecedorVinculados
				    + " AND ET.DTENTREGA BETWEEN " + dtinicio + dtfim
//					+ (forPdf  ? " GROUP BY M.CDMATERIAL, M.NOME, M.IDENTIFICACAO, M.VALORCUSTO, M.PESO " : "")
				    + groupBy
					+ " ORDER BY M.NOME ";
		
//		System.out.println(sql);
		SinedUtil.markAsReader();
		List<EmitircurvaabcBean> list = getJdbcTemplate().query(sql ,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						EmitircurvaabcBean listEmiticurvaabc = new EmitircurvaabcBean(
								rs.getInt("CD_CLASSIFICACAO"),
								rs.getString("NOME_MATERIAL"),
								rs.getDouble("VALOR_UNITARIO"),
								rs.getDouble("QUANTIDADE"),
								rs.getDouble("VALOR_CONSUMO"),
								rs.getDate("DTINICIO"),
								rs.getInt("CD_EMPRESA"),
								rs.getInt("CD_PROJETO"),
								rs.getDouble("PESO"));
						
						return listEmiticurvaabc;
					}
				});
		return list;
		
	}

	/**
	 * Cria um m�todo que pega todas as entrega para o relat�rio da listagem.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Entrega> findForPdfListagem(EntregaFiltro filtro) {
		QueryBuilderSined<Entrega> query = (QueryBuilderSined<Entrega>) querySined();
		query.setUseWhereFornecedorEmpresa(true);
		updateListagemQuery(query, filtro);
		return query.list();
	}

	public List<Entrega> findByEntrega(String whereIn) {
		if(whereIn == null){
			throw new SinedException("Par�metros inv�lido");
		}
		return query()
					.select("entrega.cdentrega, listaEntregadocumento.cdentregadocumento, fornecedor.nome, listaEntregadocumento.retornowms, empresa.cdpessoa, empresa.integracaowms, empresaEntregaDocumento.cdpessoa, " +
							"listaEntregamaterial.cdentregamaterial, material.cdmaterial, materialgrupo.cdmaterialgrupo, materialgrupo.sincronizarwms")
					.join("entrega.listaEntregadocumento listaEntregadocumento")
					.leftOuterJoin("listaEntregadocumento.fornecedor fornecedor")
					.leftOuterJoin("entrega.empresa empresa")
					.leftOuterJoin("listaEntregadocumento.empresa empresaEntregaDocumento")
					.leftOuterJoin("listaEntregadocumento.listaEntregamaterial listaEntregamaterial")
					.leftOuterJoin("listaEntregamaterial.material material")
					.leftOuterJoin("material.materialgrupo materialgrupo")
					.whereIn("entrega.cdentrega", whereIn)
					.list();
	}
	
	/**
	* M�todo que retorna a lista de entregas para faturar
	*
	* @param whereIn
	* @return
	* @since Jul 11, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Entrega> listEntregaForFaturar(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Entregas n�o podem ser nula.");
		}
		return query()
			.select("entrega.cdentrega, listaEntregadocumento.valordesconto, listaEntregadocumento.dtemissao, listaEntregadocumento.valorfrete, " +
					"empresa.cdpessoa, listadocumento.numero, fornecedor.cdpessoa, listadocumento.dtemissao, listaEntregadocumento.numero, listaEntregadocumento.dtentrada, " +
					"listadocumento.dtvencimento, listadocumento.valor, contagerencial.cdcontagerencial, rateio.cdrateio, " +
					"listaEntregamaterial.valorunitario, listaEntregamaterial.qtde, listaEntregamaterial.valordesconto, listaEntregamaterial.valorfrete, " +
					"prazopagamento.cdprazopagamento, " +
					"entrega1.cdentrega, " +
					"ordemcompra.desconto, ordemcompra.frete, material.cdmaterial, empresaOrdem.cdpessoa, documentotipo.cddocumentotipo, contadestino.cdconta, " +
					"rateio2.cdrateio, listaEntregadocumento.cdentregadocumento, documentoantecipacao.cddocumento, documentoantecipacao.valor," +
					"empresa2.cdpessoa, empresa2.nome ")
			.join("entrega.listaEntregadocumento listaEntregadocumento")
			.leftOuterJoin("listaEntregadocumento.empresa empresa2")
			.leftOuterJoin("listaEntregadocumento.entrega entrega1")
			.leftOuterJoin("listaEntregadocumento.prazopagamento prazopagamento")
			.leftOuterJoin("listaEntregadocumento.documentotipo documentotipo")
			.leftOuterJoin("documentotipo.contadestino contadestino")
			.leftOuterJoin("listaEntregadocumento.listadocumento listadocumento")
			.leftOuterJoin("listadocumento.documentoantecipacao documentoantecipacao")
			.leftOuterJoin("listaEntregadocumento.listaEntregamaterial listaEntregamaterial")
			.leftOuterJoin("listaEntregamaterial.material material")
			.leftOuterJoin("material.contagerencial contagerencial")
			.leftOuterJoin("entrega.empresa empresa")
			.leftOuterJoin("listaEntregadocumento.fornecedor fornecedor")
			.leftOuterJoin("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")
			.leftOuterJoin("listaOrdemcompraentrega.ordemcompra ordemcompra")
			.leftOuterJoin("ordemcompra.empresa empresaOrdem")
			.leftOuterJoin("ordemcompra.rateio rateio")
			.leftOuterJoin("listaEntregadocumento.rateio rateio2")
			.whereIn("entrega.cdentrega", whereIn)
			.orderBy("listadocumento.cdentregapagamento")
			.list();
		
	}
	
	public List<Entrega> findForWms(String ids) {
		if (ids == null || ids.equals("")) {
			throw new SinedException("Entregas n�o podem ser nula.");
		}
		StringBuilder sb = new StringBuilder();
		sb.append("entrega.cdentrega, entrega.dtentrega, entrega.dtemissao, entrega.placaveiculo, entrega.transportadora, ");
		sb.append("fornecedor.cdpessoa, fornecedor.tipopessoa, fornecedor.cpf, fornecedor.cnpj, fornecedor.nome, ");
		sb.append("materialgrupo.cdmaterialgrupo, materialgrupo.nome, ");
		sb.append("material.cdmaterial, material.nome, material.codigofabricante, ");
		sb.append("entregamaterial.qtde, entregamaterial.valorunitario, ");
		sb.append("entregadocumento.numero");
		
		return query()
					.select(sb.toString())
					.join("entrega.fornecedor fornecedor")
					.join("entrega.listaEntregadocumento entregadocumento")
					.join("entrega.listaEntregamaterial entregamaterial")
					.join("entregamaterial.material material")
					.join("material.materialgrupo materialgrupo")
					.whereIn("entrega.cdentrega", ids)
					.list();
	}
		
	
	/**
	 * M�todo que busca as entregas que est�o relacionadas com a ordem de compra
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entrega> findRateioOrdemcompraByEntrega(String whereIn) {
		if(whereIn == null){
			throw new SinedException("Par�metros inv�lido");
		}
		return query()
					.select("entrega.cdentrega, ordemcompra.cdordemcompra, rateio.cdrateio, item.cdrateioitem, projeto.cdprojeto, item.percentual ")
					.join("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")
					.join("listaOrdemcompraentrega.ordemcompra ordemcompra")
					.join("ordemcompra.rateio rateio")
					.join("rateio.listaRateioitem item")
					.leftOuterJoin("item.projeto projeto")
					.whereIn("entrega.cdentrega", whereIn)
					.list();
	}
	
	/**
	 * M�todo que busca as entregas com o rateio dos documentos
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entrega> findRateioByEntrega(String whereIn) {
		if(whereIn == null){
			throw new SinedException("Par�metros inv�lido");
		}
		return query()
					.select("entrega.cdentrega, entregadocumento.cdentregadocumento, rateio.cdrateio, item.cdrateioitem, projeto.cdprojeto, item.percentual ")
					.leftOuterJoin("entrega.listaEntregadocumento entregadocumento")
					.join("entregadocumento.rateio rateio")
					.join("rateio.listaRateioitem item")
					.leftOuterJoin("item.projeto projeto")
					.whereIn("entrega.cdentrega", whereIn)
					.list();
	}
	
	/**
	 * M�todo que busca os id's das entregas com saldo
	 *
	 * @param material
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @param cdentregadocumento 
	 */
	@SuppressWarnings("unchecked")
	public String findEntregaMenorDataEntradaByMaterial(Material material, String whereIn, Integer cdentregadocumento){
		if(material == null || material.getCdmaterial() == null){
			return null;
		}
		
		StringBuilder sql = new StringBuilder();
		
		sql
			.append("select ed.cdentregadocumento ") 
			.append("from entregadocumento ed ")
			.append("join entregamaterial em on em.cdentregadocumento = ed.cdentregadocumento ")
			.append("where em.cdmaterial = " + material.getCdmaterial() + " and ed.dtentrada is not null ")
			.append("and COALESCE(em.qtdeutilizada,0) < (select em1.qtde from entregamaterial em1 where em1.cdentregamaterial = em.cdentregamaterial)  ")
			.append("and (em.csticms = " + Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO.getValue())
			.append(" or em.csticms = " + Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO_PARTILHA.getValue() + " ")
			.append(" or em.csticms = " + Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.getValue() + ") ");
		
		if(whereIn != null && !"".equals(whereIn)){
			sql
			.append("and ed.cdentrega not in ( " + whereIn + ") "); 
		}
		
		if(cdentregadocumento != null){
			sql.append(" and  ed.cdentregadocumento = " + cdentregadocumento + " ");
		}
			
		sql.append("order by ed.dtentrada ");
		
		List<Integer> lista = null;		
		SinedUtil.markAsReader();
		lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getInt("cdentregadocumento");
			}
		});
		
		StringBuilder cds = new StringBuilder();
		if(lista != null && !lista.isEmpty()){
			for(Integer id : lista){
				if(!"".equals(cds.toString())) 
					cds.append(",");
				cds.append(id);
			}
		}
		return cds.toString();
	}
	
	/**
	 * M�todo que atualiza o campo romaneio da Entrega
	 *
	 * @param entrega
	 * @param romaneio
	 * @author Luiz Fernando
	 */
	public void atualizaRomaneioByEntrega(Entrega entrega, Boolean romaneio) {
		if(entrega != null && entrega.getCdentrega() != null && romaneio != null){
			getHibernateTemplate().bulkUpdate("update Entrega set romaneio = ? where cdentrega = ?",
					new Object[]{romaneio, entrega.getCdentrega()});
		}
	}

	public void updateCdarquivo(Entrega entrega, Arquivo arquivoxml) {
		if(entrega != null && entrega.getCdentrega() != null && arquivoxml != null && arquivoxml.getCdarquivo() != null){
			getHibernateTemplate().bulkUpdate("update Entrega set cdarquivoxml = ? where cdentrega = ?",
										new Object[]{arquivoxml.getCdarquivo(), entrega.getCdentrega()});
		}
	}

	/**
	 * Atualiza o campo romaneio de entrega.
	 *
	 * @param entrega
	 * @param romaneio
	 * @author Rodrigo Freitas
	 * @since 11/12/2012
	 */
	public void updateRomaneio(Entrega entrega, Boolean romaneio) {
		if(romaneio != null){
			getHibernateTemplate().bulkUpdate("update Entrega e set e.romaneio = ? where e.id = ?", new Object[]{
				romaneio, entrega.getCdentrega()
			});
		} else {
			
			getHibernateTemplate().bulkUpdate("update Entrega e set e.romaneio = null where e.id = ?", new Object[]{
				entrega.getCdentrega()
			});
		}
	}

	/**
	 * Busca as entregas que necessitam de romaneio.
	 *
	 * @param ids
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/12/2012
	 */
	public List<Entrega> findEntregasNecessitamRomaneio(String ids) {
		if(ids == null || ids.equals("")){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("entrega.cdentrega, entrega.romaneio")
					.whereIn("entrega.cdentrega", ids)
					.where("entrega.romaneio = ?", Boolean.FALSE)
					.list();
	}

	/**
	 * Busca as entregas para a valida��o da gera��o do romaneio.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/12/2012
	 */
	public List<Entrega> findForValidacaoGerarRomaneio(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("entrega.cdentrega, entrega.romaneio, aux_entrega.situacaosuprimentos")
					.leftOuterJoin("entrega.aux_entrega aux_entrega")
					.whereIn("entrega.cdentrega", whereIn)
					.list();
	}

	/**
	 * Carrega a entrega com a empresa.
	 *
	 * @param entrega
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/12/2012
	 */
	public Entrega loadWithEmpresa(Entrega entrega) {
		return query()
					.select("entrega.cdentrega, empresa.cdpessoa, empresaOrdem.cdpessoa")
					.leftOuterJoin("entrega.empresa empresa")
					.leftOuterJoin("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")
					.leftOuterJoin("listaOrdemcompraentrega.ordemcompra ordemcompra")
					.leftOuterJoin("ordemcompra.empresa empresaOrdem")
					.entity(entrega)
					.unique();
	}

	/**
	 * M�todo que busca as entregas com os materiais
	 *
	 * @param whereInEntrega
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entrega> findForAtualizarvalorcustomaterial(String whereInEntrega) {
		if(whereInEntrega == null || "".equals(whereInEntrega))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("entrega.cdentrega, listaEntregadocumento.cdentregadocumento, listaEntregamaterial.cdentregamaterial, material.cdmaterial, listaEntregamaterial.faturamentocliente")
				.join("entrega.listaEntregadocumento listaEntregadocumento")
				.join("listaEntregadocumento.listaEntregamaterial listaEntregamaterial")
				.join("listaEntregamaterial.material material")
				.whereIn("entrega.cdentrega", whereInEntrega)
				.list();
	}
	
	public TotalizadorEntrega findForListagemTotalizador(EntregaFiltro filtro){
		QueryBuilder<Entrega> query = query();
		this.updateListagemQuery(query, filtro);
		
		List<?> list = query
			.setUseTranslator(false)
			.select("aux_entrega.situacaosuprimentos, entregadocumento.valor, entregadocumento.cdentregadocumento ")
			.groupBy("aux_entrega.situacaosuprimentos, entregadocumento.cdentregadocumento ")
			.orderBy("aux_entrega.situacaosuprimentos")
			.list();
		
		Money totalAberto = new Money(0);	
		Money totalFaturado = new Money(0);	
		Money totalBaixado = new Money(0);	
		Money totalCancelado = new Money(0);
		
		for (Object obj : list) {
			if(obj instanceof Object[]){
				Object[] tupla = (Object[])obj;
				
				Object situacaoObj = tupla[0];
				Object valorObj = tupla[1];
				
				if(situacaoObj instanceof Situacaosuprimentos){
					Situacaosuprimentos situacao = (Situacaosuprimentos) situacaoObj;
					
					if(valorObj instanceof Money){
						Money valor = (Money)valorObj;
						
						if(situacao.equals(Situacaosuprimentos.EM_ABERTO)){
							totalAberto = totalAberto.add(valor);
						} else if(situacao.equals(Situacaosuprimentos.FATURADA)){
							totalFaturado = totalFaturado.add(valor);
						} else if(situacao.equals(Situacaosuprimentos.BAIXADA)){
							totalBaixado = totalBaixado.add(valor);
						} else if(situacao.equals(Situacaosuprimentos.CANCELADA)){
							totalCancelado = totalCancelado.add(valor);
						} 
					}
				}
			}
		}
		
		
		TotalizadorEntrega totalizadorEntrega = new TotalizadorEntrega();
		totalizadorEntrega.setTotalAberto(totalAberto);
		totalizadorEntrega.setTotalBaixado(totalBaixado);
		totalizadorEntrega.setTotalCancelado(totalCancelado);
		totalizadorEntrega.setTotalFaturado(totalFaturado);
		return totalizadorEntrega;
	}

	/**
	* M�todo que retorna a entrega com a marca��o de romaneio gerado mas n�o existe romaneo em aberto ou baixado vinculado
	*
	* @param whereInRomaneio
	* @return
	* @since 10/09/2015
	* @author Luiz Fernando
	*/
	public List<Entrega> findEntregaRomaneioGeradoSemRomaneio(String whereInRomaneio) {
		if(StringUtils.isEmpty(whereInRomaneio))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("entrega.cdentrega, entrega.romaneio")
				.where("entrega.romaneio = true")
				.where("entrega.cdentrega in (select e.cdentrega " +
								" from Romaneio r " +
								" join r.listaRomaneioorigem ro " +
								" join ro.entrega e " +
								" where r.cdromaneio in (" + whereInRomaneio + ") ) ")
				.where("not exists (select e.cdentrega " +
								" from Romaneio r " +
								" join r.listaRomaneioorigem ro " +
								" join ro.entrega e " +
								" join r.romaneiosituacao rs " +
								" where e.cdentrega = entrega.cdentrega " +
								" and rs.cdromaneiosituacao <> " + Romaneiosituacao.CANCELADA.getCdromaneiosituacao() + ") ")
				.list();
	}
	
	/**
	* M�todo que retorna as entregas dos romaneios
	*
	* @param whereInRomaneio
	* @return
	* @since 18/09/2015
	* @author Luiz Fernando
	*/
	public List<Entrega> findEntregaRomaneioGerado(String whereInRomaneio) {
		if(StringUtils.isEmpty(whereInRomaneio))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("entrega.cdentrega, entrega.romaneio")
				.where("entrega.romaneio = true")
				.where("entrega.cdentrega in (select e.cdentrega " +
								" from Romaneio r " +
								" join r.listaRomaneioorigem ro " +
								" join ro.entrega e " +
								" where r.cdromaneio in (" + whereInRomaneio + ") ) ")
				.list();
	}

	/**
	* M�todo que busca as entregas que n�o est�o canceladas
	*
	* @param ids
	* @return
	* @since 25/09/2015
	* @author Luiz Fernando
	*/
	public List<Entrega> findEntregaDifernteCancelada(String ids, Material material) {
		if (ids == null || ids.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		return query()
					.select("entrega.cdentrega, aux_entrega.situacaosuprimentos")
					.join("entrega.aux_entrega aux_entrega")
					.join("entrega.listaEntregadocumento listaEntregadocumento")
					.join("listaEntregadocumento.listaEntregamaterial listaEntregamaterial")
					.whereIn("entrega.cdentrega", ids)
					.where("aux_entrega.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
					.where("listaEntregamaterial.material = ?", material)
					.list();
	}
	/**
	 * M�todo que carrega materiais v�nculados com entrega para pop up de emiss�o de etiquetas
	 * @param whereIn
	 * @return
	 * @author C�sar
	 * @since 17/12/2015
	 */
	public List<Entrega> buscarMaterialForPopUpEtiquetaEntrega(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
			.select("entrega.cdentrega,entregamaterial.cdentregamaterial,entregamaterial.qtde,material.cdmaterial, material.nome," +
					"unidademedida.cdunidademedida,unidademedida.etiquetaunica, loteestoque.numero, loteestoque.cdloteestoque, empresa.nome," +
					"empresa.razaosocial, listaEntregamateriallote.qtde, loteestoqueMultipos.cdloteestoque, loteestoqueMultipos.numero,"+
					"loteestoqueMultipos.validade, entregamaterial.tipolote, entregadocumento.cdentregadocumento")
			.leftOuterJoin("entrega.listaEntregadocumento entregadocumento")
			.leftOuterJoin("entregadocumento.listaEntregamaterial entregamaterial")
			.leftOuterJoin("entregamaterial.material material")
			.leftOuterJoin("entregamaterial.loteestoque loteestoque")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("entrega.empresa empresa")
			.leftOuterJoin("entregamaterial.listaEntregamateriallote listaEntregamateriallote")
			.leftOuterJoin("entregamaterial.loteestoque loteestoqueMultipos")
			.whereIn("entrega.cdentrega", whereIn)
			.list();
	}
	
	/**
	* M�todo que retorna true caso exista recebimento nas situa��es passadas por par�metro
	*
	* @param whereInEntregadocumento
	* @param situacoes
	* @return
	* @since 01/11/2016
	* @author Luiz Fernando
	*/
	public boolean haveEntregaSituacao(String whereInEntregadocumento, Situacaosuprimentos... situacoes) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < situacoes.length; i++) {
			sb.append(situacoes[i].getCodigo());
			if((i + 1) < situacoes.length){
				sb.append(",");
			}
		}
		
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Entrega.class)
					.join("entrega.listaEntregadocumento listaEntregadocumento")
					.whereIn("listaEntregadocumento.cdentregadocumento", whereInEntregadocumento)
					.whereIn("entrega.aux_entrega.situacaosuprimentos", sb.toString())
					.unique() > 0;
	}
	
	@SuppressWarnings("unchecked")
	public List<Entrega> findByEntradaSemRegistroFinanceiro(Date dateToSearch) {
		StringBuilder sql = new StringBuilder();
		sql.append("select e.cdentrega, e.cdempresa ");
		sql.append("from entrega e ");
		sql.append("join aux_entrega ae on ae.cdentrega = e.cdentrega ");
		sql.append("join entregadocumento eo on eo.cdentrega = e.cdentrega ");
		sql.append("where ae.situacao = 0 and not(eo.simplesremessa != true or e.faturamentocliente = true) ");
		sql.append("	and e.dtentrega between '" + dateToSearch + "' and current_date");

		SinedUtil.markAsReader();
		List<Entrega> entregaList = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			
			@Override
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Entrega e = new Entrega(rs.getInt("cdentrega"));
				if(rs.getInt("cdempresa") > 0) e.setEmpresa(new Empresa(rs.getInt("cdempresa")));
				return e;
			}
		});
		
		return entregaList;
	}
}