package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MaterialgrupoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("materialgrupo.nome")
public class MaterialgrupoDAO extends GenericDAO<Materialgrupo> {
	
	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Materialgrupo> query) {
		query
			.select("materialgrupo.cdmaterialgrupo, materialgrupo.nome, materialgrupo.ativo, materialgrupo.locacao, materialgrupo.email, " +
					"materialgrupo.valormvaespecifico, materialgrupo.papelimune, materialgrupo.sincronizarwms, materialgrupo.dtaltera, materialgrupo.cdusuarioaltera, " +
					"arquivo.cdarquivo, arquivo.nome, listaMaterialgrupousuario.cdmaterialgrupousuario, usuario.cdpessoa, usuario.nome, " +
					"listaMaterialgrupocomissaovenda.cdmaterialgrupocomissaovenda, listaMaterialgrupocomissaovenda.comissaopara, " +
					"cargo.cdcargo, cargo.nome, categoria.cdcategoria, categoria.nome, " +
					"comissionamento.cdcomissionamento, comissionamento.nome, documentotipo.cddocumentotipo, documentotipo.nome, " +
					"materialgrupo.gradeestoquetipo, materialgrupo.coleta, " +
					"pedidovendatipo.cdpedidovendatipo, materialgrupo.pesquisasomentemestre , fornecedor.cdpessoa, fornecedor.nome, " +
					"materialgrupo.bebidaalcoolica, materialgrupo.rateiocustoproducao, materialgrupo.animal, materialgrupo.registrarpesomedio, " +
					"materialgrupo.medicamento, materialgrupo.baseCalculoComissao, materialgrupo.gerarBlocoH, materialgrupo.gerarBlocoK, materialgrupo.incluirLoteNaNfe, materialgrupo.produtoControlado ")
			.leftOuterJoin("materialgrupo.arquivo arquivo")
			.leftOuterJoin("materialgrupo.listaMaterialgrupousuario listaMaterialgrupousuario")
			.leftOuterJoin("listaMaterialgrupousuario.usuario usuario")
			.leftOuterJoin("materialgrupo.listaMaterialgrupocomissaovenda listaMaterialgrupocomissaovenda")
			.leftOuterJoin("listaMaterialgrupocomissaovenda.cargo cargo")
			.leftOuterJoin("listaMaterialgrupocomissaovenda.categoria categoria")
			.leftOuterJoin("listaMaterialgrupocomissaovenda.comissionamento comissionamento")
			.leftOuterJoin("listaMaterialgrupocomissaovenda.documentotipo documentotipo")
			.leftOuterJoin("listaMaterialgrupocomissaovenda.pedidovendatipo pedidovendatipo")
			.leftOuterJoin("listaMaterialgrupocomissaovenda.fornecedor fornecedor");
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				
				Materialgrupo materialgrupo = (Materialgrupo) save.getEntity();
				if(materialgrupo.getArquivo() != null){
					arquivoDAO.saveFile(materialgrupo, "arquivo");
				}
				save.saveOrUpdateManaged("listaMaterialgrupousuario");
				save.saveOrUpdateManaged("listaMaterialgrupocomissaovenda");
				save.saveOrUpdateManaged("listaMaterialgrupotributacao");
				return null;
			}
		});
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Materialgrupo> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		MaterialgrupoFiltro filtro = (MaterialgrupoFiltro) _filtro;
		query
			.select("materialgrupo.cdmaterialgrupo, materialgrupo.nome, materialgrupo.ativo, materialgrupo.gerarBlocoH, materialgrupo.gerarBlocoK")
			.whereLikeIgnoreAll("materialgrupo.nome", filtro.getNome())
			.where("materialgrupo.ativo = ?", filtro.getAtivo())
			.where("materialgrupo.gerarBlocoH = ?", filtro.getGerarBlocoH())
			.where("materialgrupo.gerarBlocoK = ?", filtro.getGerarBlocok());
	}
	/**
	 * M�todo para obter uma lista de grupos por planejamento.
	 * 
	 * @param planejamento
	 * @return
	 * @author Ramon Brazil
	 */
	public List<Materialgrupo> findByGrupo(Planejamento planejamento){
		if(planejamento == null || planejamento.getCdplanejamento() == null){
			throw new SinedException("Os par�metros planejamento ou cdplanejamento n�o podem ser null.");
		}
		
		return 
			query()
			.select("materialgrupo.cdmaterialgrupo, materialgrupo.nome")
			.join("materialgrupo.listaMaterial material")
			.join("material.listaTarefarecursogeral tcg")
			.join("tcg.tarefa tarefa")
			.join("tarefa.planejamento planejamento")
			.where("planejamento = ?",planejamento)
			.orderBy("materialgrupo.nome")
			.list();
	}

	/**
	 * Busca a lista de grupos de materiais para exibi��o em flex.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Materialgrupo> findAllForFlex() {
		return query()
					.select("materialgrupo.cdmaterialgrupo, materialgrupo.nome")
					.orderBy("materialgrupo.nome")
					.list();
	}


	/**
	* Busca a lista de grupo de materiais ativos
	*
	* @return
	* @since Jul 11, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Materialgrupo> findAtivos() {
		
		return query()
					.select("materialgrupo.cdmaterialgrupo, materialgrupo.nome, materialgrupo.ativo")
					.where("ativo is not null")
					.where("ativo = true")
					.orderBy("upper(tiraacento(materialgrupo.nome))")
					.list();
					
	}


	public List<Materialgrupo> findByUsuario(Usuario usuario) {
		if(usuario == null || usuario.getCdpessoa() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
					.select("materialgrupo.cdmaterialgrupo, materialgrupo.nome")
					.join("materialgrupo.listaMaterialgrupousuario listaMaterialgrupousuario")
					.where("listaMaterialgrupousuario.usuario = ?", usuario)
					.list();
	}

	/**
	 * M�todo que busca o grupo de material com a lista de tributa��o por empresa
	 *
	 * @param materialgrupo
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Materialgrupo loadTributacao(Materialgrupo materialgrupo, Empresa empresa) {
		if (materialgrupo == null || materialgrupo.getCdmaterialgrupo() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		return query()
				.select(" materialgrupo.cdmaterialgrupo, " 
						+ "empresa.cdpessoa, "
						+ "cfop.cdcfop, cfop.codigo, cfop.descricaocompleta")
				.where("materialgrupo = ?", materialgrupo)
				.unique();
	}

	/**
	 * Busca o grupo de material pelo nome passado por par�metro para a importa��o de dados.
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/01/2013
	 */
	public Materialgrupo findByNomeForImportacao(String nome) {
		if(nome == null || nome.equals("")){
			throw new SinedException("Nome do grupo de material n�o pode ser nulo.");
		}
		
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		List<Materialgrupo> lista = query()
			.select("materialgrupo.cdmaterialgrupo, materialgrupo.nome")
			.where("UPPER(" + funcaoTiraacento + "(materialgrupo.nome)) LIKE ?", Util.strings.tiraAcento(nome).toUpperCase())
			.list();
		
		if(lista != null && lista.size() > 1){
			throw new SinedException("Mais de um grupo de material encontrado.");
		} else if(lista != null && lista.size() == 1){
			return lista.get(0);
		} else return null;
	}

	/**
	 * M�todo que busca os grupos de material
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Materialgrupo> findForImportarproducao() {
		return query()
				.select("materialgrupo.cdmaterialgrupo, materialgrupo.nome")
				.list();
	}

	public List<Materialgrupo> findForPVOffline() {
		return query()
			.select("materialgrupo.cdmaterialgrupo, materialgrupo.nome")
			.list();
	}
	
	public List<Materialgrupo> findForAndroid(String whereIn) {
		return query()
			.select("materialgrupo.cdmaterialgrupo, materialgrupo.nome")
			.whereIn("materialgrupo.cdmaterialgrupo", whereIn)
			.list();
	}
	
	/**
	 * Busca a lista de materialgrupo para enviar para o W3Producao
	 * @param whereIn
	 * @return
	 */
	public List<Materialgrupo> findForW3Producao(String whereIn){
		return query()
					.select("materialgrupo.cdmaterialgrupo, materialgrupo.nome")
					.whereIn("materialgrupo.cdmaterialgrupo", whereIn)
					.list();
	}

	/**
	* M�todo que busca grupo de material para a integra��o SAGE
	*
	* @param whereIn
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public List<Materialgrupo> findForSagefiscal(String whereIn) {
		return query()
			.select("materialgrupo.cdmaterialgrupo, materialgrupo.nome")
			.whereIn("materialgrupo.cdmaterialgrupo", whereIn)
			.list();
	}
	
	public List<Materialgrupo> findForAnimal(){
		return query()
		.select("materialgrupo.cdmaterialgrupo, materialgrupo.nome")
		.where("materialgrupo.animal = ?", Boolean.TRUE)
		.list();		
	}

	@Override
	public ListagemResult<Materialgrupo> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Materialgrupo> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("materialgrupo.cdmaterialgrupo");
		
		return new ListagemResult<Materialgrupo>(query, filtro);
	}
	public Materialgrupo findGerarBloco (String whereIn){
		return query()
				.select("materialgrupo.cdmaterialgrupo, materialgrupo.gerarBlocoH, materialgrupo.gerarBlocoK")
				.whereIn("materialgrupo.cdmaterialgrupo", whereIn)
				.unique();
	}
	
	public Materialgrupo findforGerarBloco (Integer cdMaterialGrupo){
		return query()
				.select("materialgrupo.cdmaterialgrupo, materialgrupo.gerarBlocoH, materialgrupo.gerarBlocoK")
				.where("materialgrupo.cdmaterialgrupo=?", cdMaterialGrupo)
				.unique();
	}
	
	public Materialgrupo findWithGradeEstoque (Materialgrupo materialGrupo){
		return query()
				.select("materialgrupo.cdmaterialgrupo, materialgrupo.gradeestoquetipo")
				.where("materialgrupo=?", materialGrupo)
				.unique();
	}
}
