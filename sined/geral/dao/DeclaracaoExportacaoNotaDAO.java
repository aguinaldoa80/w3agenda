package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.sined.geral.bean.DeclaracaoExportacaoNota;


public class DeclaracaoExportacaoNotaDAO extends GenericDAO<DeclaracaoExportacaoNota>{

	public List<DeclaracaoExportacaoNota> buscarNotasVinculadas(String whereInNotas) {
		return query()
				.select("declaracaoExportacaoNota.cdDeclaracaoExportacaoNota," +
						"notaFiscalProduto.cdNota,notaFiscalProduto.numero," +
						"declaracaoExportacao.cdDeclaracaoExportacao,declaracaoExportacao.numeroDeclaracao")
				.leftOuterJoin("declaracaoExportacaoNota.notaFiscalProduto notaFiscalProduto")
				.leftOuterJoin("declaracaoExportacaoNota.declaracaoExportacao declaracaoExportacao")
				.whereIn("notaFiscalProduto.cdNota", whereInNotas)
				.list();
	}
	
}
