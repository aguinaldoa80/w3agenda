package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregadocumentofrete;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EntregadocumentofreteDAO extends GenericDAO<Entregadocumentofrete> {
	
	public List<Entregadocumentofrete> findByEntregadocumento(Entregadocumento entregadocumento){
		return query()
				.select("entregadocumentofrete.cdentregadocumentofrete, entregadocumento.cdentregadocumento," +
						"entregadocumentovinculo.cdentregadocumento, entregadocumentovinculo.numero, entregadocumentovinculo.dtentrada," +
						"entregadocumentovinculo.dtemissao, entregadocumentovinculo.situacaodocumento, " +
						"entregadocumentovinculo.valor, " +
						"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.nome")
				.join("entregadocumentofrete.entregadocumento entregadocumento")
				.join("entregadocumentofrete.entregadocumentovinculo entregadocumentovinculo")
				.leftOuterJoin("entregadocumentovinculo.modelodocumentofiscal modelodocumentofiscal")
				.where("entregadocumento=?", entregadocumento)
				.orderBy("entregadocumentovinculo.numero, entregadocumentovinculo.dtentrada, entregadocumentovinculo.dtemissao")
				.list();
	}
	
	public void delete(Integer cdentregadocumento, String whereNotIn){
		if (whereNotIn.equals("")){
			getJdbcTemplate().execute("delete from entregadocumentofrete where cdentregadocumento=" + cdentregadocumento);
		}else {
			getJdbcTemplate().execute("delete from entregadocumentofrete where cdentregadocumento=" + cdentregadocumento + " and cdentregadocumentofrete not in (" + whereNotIn + ")");
		}		
	}
	
	
	public List<Entregadocumentofrete> findByEntregadocumentoForMemorandoexportacao(Entregadocumento entregadocumento){
		return query()
				.select("entregadocumentofrete.cdentregadocumentofrete, entregadocumento.cdentregadocumento," +
						"entregadocumentovinculo.cdentregadocumento, entregadocumentovinculo.numero," +
						"entregadocumentovinculo.dtemissao, entregadocumentovinculo.serie," +						
						"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo")
				.leftOuterJoin("entregadocumentofrete.entregadocumento entregadocumento")
				.leftOuterJoin("entregadocumentofrete.entregadocumentovinculo entregadocumentovinculo")
				.leftOuterJoin("entregadocumentovinculo.modelodocumentofiscal modelodocumentofiscal")
				.where("entregadocumento=?", entregadocumento)				
				.list();
	}
}