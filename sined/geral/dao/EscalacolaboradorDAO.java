package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Escalacolaborador;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EscalacolaboradorDAO extends GenericDAO<Escalacolaborador> {

	/**
	 * M�todo que retorna as escalas do colaborador
	 * 
	 * @param colaborador
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Escalacolaborador> findByColaborador(Colaborador colaborador) {
		return query()
			.select("escalacolaborador.cdescalacolaborador, escalacolaborador.dtinicio, escalacolaborador.dtfim, " +
					"escala.cdescala, escala.nome, colaborador.cdpessoa, escalacolaborador.exclusivo")
			.join("escalacolaborador.colaborador colaborador")
			.join("escalacolaborador.escala escala")
			.where("colaborador = ?", colaborador)
			.orderBy("escalacolaborador.dtinicio")
			.list();
	}

	/**
	 * M�todo que retorna as escalas de hor�rio do colaborador no per�odo
	 * 
	 * @param colaborador
	 * @param datede
	 * @param dateate
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Escalacolaborador findEscalaHorarioDoColaboradorDoPeriodo(Colaborador colaborador, Date datede, Date dateate) {
		return query()
		.select("escalacolaborador.cdescalacolaborador, escalacolaborador.dtinicio, escalacolaborador.dtfim, escala.cdescala, escala.nome, escala.diastrabalho, escala.diasfolga, escala.naotrabalhadomingo, escala.horariotodosdias, " +
				"escala.naotrabalhaferiado, escalahorario.cdescalahorario, escalahorario.horainicio, escalahorario.horafim, escalahorario.diasemana")
		.join("escalacolaborador.colaborador colaborador")
		.join("escalacolaborador.escala escala")
		.join("escala.listaescalahorario escalahorario")
		.where("colaborador = ?", colaborador)
		.openParentheses()
			.openParentheses()
				.where("escalacolaborador.dtinicio <= ?", datede)
				.where("escalacolaborador.dtfim >= ?", datede)
			.closeParentheses().or()
			.openParentheses()
				.where("escalacolaborador.dtinicio <= ?", datede)
				.where("escalacolaborador.dtfim is null")
			.closeParentheses().or()
			.openParentheses()
				.where("escalacolaborador.dtinicio <= ?", dateate)
				.where("escalacolaborador.dtfim >= ?", dateate)
			.closeParentheses().or()
			.openParentheses()
				.where("escalacolaborador.dtinicio <= ?", dateate)
				.where("escalacolaborador.dtfim is null")
			.closeParentheses()
		.closeParentheses()
		.orderBy("escalacolaborador.dtinicio, escalahorario.horainicio, escalahorario.diasemana")
		.list().get(0);
	}

	public List<Escalacolaborador> findListaEscalaHorarioDoColaboradorDoPeriodo(Colaborador colaborador, Date datede, Date dateate) {
		return query()
		.select("escalacolaborador.cdescalacolaborador, escalacolaborador.dtinicio, escalacolaborador.dtfim, escala.cdescala, escala.nome, escala.diastrabalho, escala.diasfolga, escala.naotrabalhadomingo, escala.horariotodosdias, " +
				"escala.naotrabalhaferiado, escalahorario.cdescalahorario, escalahorario.horainicio, escalahorario.horafim, escalahorario.diasemana, escalacolaborador.exclusivo")
		.join("escalacolaborador.colaborador colaborador")
		.join("escalacolaborador.escala escala")
		.join("escala.listaescalahorario escalahorario")
		.where("colaborador = ?", colaborador)
		.openParentheses()
			.openParentheses()
				.where("escalacolaborador.dtinicio <= ?", datede)
				.where("escalacolaborador.dtfim >= ?", datede)
			.closeParentheses().or()
			.openParentheses()
				.where("escalacolaborador.dtinicio <= ?", datede)
				.where("escalacolaborador.dtfim is null")
			.closeParentheses().or()
			.openParentheses()
				.where("escalacolaborador.dtinicio <= ?", dateate)
				.where("escalacolaborador.dtfim >= ?", dateate)
			.closeParentheses().or()
			.openParentheses()
				.where("escalacolaborador.dtinicio <= ?", dateate)
				.where("escalacolaborador.dtfim is null")
			.closeParentheses().or()
			.openParentheses()
				.where("escalacolaborador.dtinicio >= ?", datede)
				.where("escalacolaborador.dtinicio <= ?", dateate)
			.closeParentheses().or()
			.openParentheses()
				.where("escalacolaborador.dtfim >= ?", datede)
				.where("escalacolaborador.dtfim <= ?", dateate)
			.closeParentheses().or()
		.closeParentheses()
		.orderBy("escalacolaborador.dtinicio, escalahorario.horainicio, escalahorario.diasemana")
		.list();
	}
	
	/**
	 * M�todo que busca a �ltima escala do colaborador
	 *
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 */
	public Escalacolaborador findUltimaEscalaByColaborador(Colaborador colaborador) {
		if(colaborador == null || colaborador.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("escalacolaborador.cdescalacolaborador, escalacolaborador.dtfim ")
				.join("escalacolaborador.colaborador colaborador")
				.where("colaborador = ?", colaborador)
				.orderBy("escalacolaborador.dtinicio desc")
				.setMaxResults(1)
				.unique();
	}

	/**
	 * M�todo que atualiza a data final da �ltima escala do colaborador
	 *
	 * @param escalacolaborador
	 * @author Luiz Fernando
	 */
	public void atualizaDataFimUltimaEscalacolaborador(Escalacolaborador escalacolaborador) {
		if(escalacolaborador == null || escalacolaborador.getCdescalacolaborador() == null || 
				escalacolaborador.getDtfim() == null) 
			throw new SinedException("Par�metros inv�lidos");		
		
		getHibernateTemplate().bulkUpdate("update Escalacolaborador ec set ec.dtfim = ? where ec.cdescalacolaborador = ?",
										new Object[]{escalacolaborador.getDtfim(), escalacolaborador.getCdescalacolaborador()});
		
	}
}
