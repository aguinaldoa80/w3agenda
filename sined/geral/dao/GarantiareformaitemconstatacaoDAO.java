package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.Garantiareformaitem;
import br.com.linkcom.sined.geral.bean.Garantiareformaitemconstatacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class GarantiareformaitemconstatacaoDAO extends GenericDAO<Garantiareformaitemconstatacao>{
	
	public void delete(Garantiareformaitem garantiareformaitem){
		getHibernateTemplate().bulkUpdate("delete from Garantiareformaitemconstatacao g where g.garantiareformaitem=?", new Object[]{garantiareformaitem});
	}
}
