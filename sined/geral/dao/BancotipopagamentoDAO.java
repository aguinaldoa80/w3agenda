package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.BancoTipoPagamento;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class BancotipopagamentoDAO extends GenericDAO<BancoTipoPagamento>{
	
	public List<BancoTipoPagamento> findByBanco(Banco banco){
		return query().where("banco = ?", banco).orderBy("descricao").list();
	}
	
	public BancoTipoPagamento findByCdTipoPagamento(int id){
		return query().where("cdbancotipopagamento = ?", id).unique();
	}

}
