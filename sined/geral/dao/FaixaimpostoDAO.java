package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.QueryBuilder.Where;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Codigotributacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Faixaimposto;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.FaixaimpostoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("faixaimposto.nome")
public class FaixaimpostoDAO extends GenericDAO<Faixaimposto> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Faixaimposto> query, FiltroListagem _filtro) {
		
		FaixaimpostoFiltro filtro = (FaixaimpostoFiltro) _filtro;
		
		query
			.select("distinct faixaimposto.cdfaixaimposto, faixaimposto.nome, faixaimposto.controle, faixaimposto.valoraliquota, faixaimposto.ativo, " +
					"municipio.nome, uf.sigla, ufdestino.sigla")
			.leftOuterJoin("faixaimposto.municipio municipio")
			.leftOuterJoin("faixaimposto.uf uf")
			.leftOuterJoin("faixaimposto.ufdestino ufdestino")
			.leftOuterJoin("faixaimposto.listaFaixaimpostocliente listaFaixaimpostocliente")
			.leftOuterJoin("faixaimposto.listaFaixaimpostoempresa listaFaixaimpostoempresa")
			.where("faixaimposto.valoraliquota >= ?", filtro.getValoraliquota1())
			.where("faixaimposto.valoraliquota <= ?", filtro.getValoraliquota2())
			.where("faixaimposto.ativo = ?", filtro.getAtivo())
			.whereLikeIgnoreAll("faixaimposto.nome", filtro.getNome())
			.whereIn("faixaimposto.controle", Faixaimpostocontrole.listAndConcatenate(filtro.getListaControle()))
			.where("uf = ?", filtro.getUf())
			.where("municipio = ?", filtro.getMunicipio())
			.where("ufdestino = ?", filtro.getUfdestino())
			.where("listaFaixaimpostoempresa.empresa = ?", filtro.getEmpresa())
			.where("listaFaixaimpostocliente.cliente = ?", filtro.getCliente())
			.ignoreJoin("listaFaixaimpostoempresa", "listaFaixaimpostocliente");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaFaixaimpostoempresa");
		save.saveOrUpdateManaged("listaFaixaimpostocliente");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Faixaimposto> query) {
		query
			.select("faixaimposto.cdfaixaimposto, faixaimposto.nome, faixaimposto.controle, faixaimposto.valoraliquota, faixaimposto.basecalculopercentual, " +
					"faixaimposto.valorminimoincidir, faixaimposto.valorminimocobrar, faixaimposto.cumulativo, faixaimposto.cumulativotipo, faixaimposto.ativo, " +
					"municipio.cdmunicipio, municipio.nome, uf.sigla, uf.cduf, faixaimposto.icmsst, faixaimposto.mva, contagerencial.cdcontagerencial, contagerencial.nome, " +
					"faixaimposto.centrocusto, ufdestino.cduf, codigotributacao.cdcodigotributacao, codigotributacao.codigo, codigotributacao.descricao, codigotributacaomunicipio.nome, " +
					"codigotributacaouf.sigla, faixaimposto.exibiremissaonfse, faixaimposto.aliquotaretencao, empresa.cdpessoa, " +
					"faixaimposto.observacao, cliente.cdpessoa, cliente.nome ")
			.leftOuterJoin("faixaimposto.listaFaixaimpostocliente listaFaixaimpostocliente")
			.leftOuterJoin("faixaimposto.listaFaixaimpostoempresa listaFaixaimpostoempresa")
			.leftOuterJoin("listaFaixaimpostoempresa.empresa empresa")
			.leftOuterJoin("listaFaixaimpostocliente.cliente cliente")
			.leftOuterJoin("faixaimposto.municipio municipio")
			.leftOuterJoin("faixaimposto.uf uf")
			.leftOuterJoin("faixaimposto.ufdestino ufdestino")
			.leftOuterJoin("faixaimposto.codigotributacao codigotributacao")
			.leftOuterJoin("codigotributacao.municipio codigotributacaomunicipio")
			.leftOuterJoin("codigotributacaomunicipio.uf codigotributacaouf")
			.leftOuterJoin("faixaimposto.contagerencial contagerencial")
			.leftOuterJoin("faixaimposto.centrocusto centrocusto");
	}
	
	/**
	 * Carrega as faixas de impostos ativas por controle e uf de destino.
	 *
	 * @param controle
	 * @param municipio
	 * @param empresa 
	 * @param cliente
	 * @param uf
	 * @param ufDestino
	 * @return
	 * @author Marden Silva
	 */
	public Faixaimposto findByControleAndUf(Empresa empresa, Cliente cliente, Faixaimpostocontrole controle, Uf uf, Uf ufDestino){
		if(controle == null){
			throw new SinedException("Erro na passagem de par�metro: Controle n�o pode ser nulo.");
		}
		
		QueryBuilder<Faixaimposto> query = query()
					.select("faixaimposto.cdfaixaimposto, faixaimposto.controle, faixaimposto.valoraliquota, " +
							"faixaimposto.icmsst, faixaimposto.mva, faixaimposto.basecalculopercentual, " +
							"faixaimposto.valorminimocobrar, faixaimposto.valorminimoincidir, faixaimposto.cumulativo, faixaimposto.cumulativotipo")
					.leftOuterJoin("faixaimposto.listaFaixaimpostocliente listaFaixaimpostocliente")
					.leftOuterJoin("faixaimposto.listaFaixaimpostoempresa listaFaixaimpostoempresa")
					.openParentheses()
						.where("listaFaixaimpostoempresa is null").or()
						.where("listaFaixaimpostoempresa.empresa = ?", empresa)
					.closeParentheses()
					.openParentheses()
						.where("listaFaixaimpostocliente is null").or()
						.where("listaFaixaimpostocliente.cliente = ?", cliente)
					.closeParentheses()
					.where("faixaimposto.controle = ?", controle)					
					.where("faixaimposto.ativo = ?", Boolean.TRUE);
		
		if (uf != null && uf.getCduf() != null){
			query.where("faixaimposto.uf = ?", uf);
		} else {
			query.where("faixaimposto.uf is null");
		}
		
		if (ufDestino != null && ufDestino.getCduf() != null){
			query.where("faixaimposto.ufdestino = ?", ufDestino);
		} else {
			query.where("faixaimposto.ufdestino is null");
		}
		
		Faixaimposto unique = query.unique(); 
		
		if (unique == null){
			QueryBuilder<Faixaimposto> query2 = query()
				.select("faixaimposto.cdfaixaimposto, faixaimposto.controle, faixaimposto.valoraliquota, " +
						"faixaimposto.icmsst, faixaimposto.mva, faixaimposto.basecalculopercentual, " +
						"faixaimposto.valorminimocobrar, faixaimposto.valorminimoincidir, faixaimposto.cumulativo, faixaimposto.cumulativotipo")		
				.leftOuterJoin("faixaimposto.listaFaixaimpostocliente listaFaixaimpostocliente")
				.leftOuterJoin("faixaimposto.listaFaixaimpostoempresa listaFaixaimpostoempresa")
				.openParentheses()
					.where("listaFaixaimpostoempresa is null").or()
					.where("listaFaixaimpostoempresa.empresa = ?", empresa)
				.closeParentheses()
				.openParentheses()
					.where("listaFaixaimpostocliente is null").or()
					.where("listaFaixaimpostocliente.cliente = ?", cliente)
				.closeParentheses()
				.where("faixaimposto.controle = ?", controle)					
				.where("faixaimposto.ativo = ?", Boolean.TRUE)
				.where("faixaimposto.ufdestino is null")
				.where("faixaimposto.uf is null");
			
			unique = query2.unique();
		}
		
		return unique;
	}
	
	/**
	 * Carrega as faixas de impostos ativas por controle.
	 *
	 * @param municipio
	 * @return
	 * @author Marden Silva
	 */
	public List<Faixaimposto> findByControle(Empresa empresa, Cliente cliente, Faixaimpostocontrole controle){
		if(controle == null){
			throw new SinedException("Erro na passagem de par�metro: Controle n�o pode ser nulo.");
		}
		
		return query()
					.leftOuterJoin("faixaimposto.listaFaixaimpostocliente listaFaixaimpostocliente")
					.leftOuterJoin("faixaimposto.listaFaixaimpostoempresa listaFaixaimpostoempresa")
					.openParentheses()
						.where("listaFaixaimpostoempresa is null").or()
						.where("listaFaixaimpostoempresa.empresa = ?", empresa)
					.closeParentheses()
					.openParentheses()
						.where("listaFaixaimpostocliente is null").or()
						.where("listaFaixaimpostocliente.cliente = ?", cliente)
					.closeParentheses()
					.where("faixaimposto.controle = ?", controle)
					.where("faixaimposto.ativo = ?", Boolean.TRUE)
					.list();
	}

	/**
	 * Carrega as faixas de impostos do municipio e do controle passado.
	 *
	 * @param controle
	 * @param municipio
	 * @return
	 * @author Rodrigo Freitas
	 * @param codigotributacao 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Faixaimposto findByControleMunicipio(Empresa empresa, Cliente cliente, Faixaimpostocontrole controle, Municipio municipio, Codigotributacao codigotributacao) {
		if(controle == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		
		String select = "faixaimposto.cdfaixaimposto, faixaimposto.controle, faixaimposto.valoraliquota, " +
						"faixaimposto.icmsst, faixaimposto.mva, faixaimposto.aliquotaretencao, faixaimposto.basecalculopercentual, " +
						"faixaimposto.valorminimocobrar, faixaimposto.valorminimoincidir, faixaimposto.cumulativo, " +
						"faixaimposto.cumulativotipo, faixaimposto.exibiremissaonfse";
		
		Faixaimposto unique = null;
		
		if(municipio == null){
			unique = queryBaseControleMunicipio(empresa, cliente, controle, select)
						.where("municipio is null")
						.where("faixaimposto.ativo = ?", Boolean.TRUE)
						.unique();
		} else {
			QueryBuilder<Faixaimposto> query = queryBaseControleMunicipio(empresa, cliente, controle, select);
			
			if(municipio.getCdmunicipio() != null){
				query.where("municipio = ?", municipio);
				query.where("faixaimposto.ativo = ?", Boolean.TRUE);
			} else if(municipio.getCdibge() != null){
				query.where("municipio.cdibge = ?", municipio.getCdibge());
				query.where("faixaimposto.ativo = ?", Boolean.TRUE);
			}
			
			if(codigotributacao != null){
				Where whereSemCodigotributacao = query.getWhere().clone();
				
				query.where("codigotributacao = ?", codigotributacao);
				query.where("faixaimposto.ativo = ?", Boolean.TRUE);
				unique = query.unique();
				
				if(unique == null){
					query.setWhere(whereSemCodigotributacao);
					query.where("codigotributacao is null");
					query.where("faixaimposto.ativo = ?", Boolean.TRUE);
					unique = query.unique();
				}
			} else {
				unique = query.unique();
			}
			
			if(unique == null){
				unique = queryBaseControleMunicipio(empresa, cliente, controle, select)
							.where("municipio is null")
							.where("faixaimposto.ativo = ?", Boolean.TRUE)
							.unique();
			}
		}				
		
		return unique;
	}

	private QueryBuilder<Faixaimposto> queryBaseControleMunicipio(Empresa empresa, Cliente cliente, Faixaimpostocontrole controle, String select) {
		return query()
					.select(select)
					.leftOuterJoin("faixaimposto.listaFaixaimpostocliente listaFaixaimpostocliente")
					.leftOuterJoin("faixaimposto.listaFaixaimpostoempresa listaFaixaimpostoempresa")
					.leftOuterJoin("faixaimposto.municipio municipio")
					.leftOuterJoin("faixaimposto.codigotributacao codigotributacao")
					.openParentheses()
						.where("listaFaixaimpostoempresa is null").or()
						.where("listaFaixaimpostoempresa.empresa = ?", empresa)
					.closeParentheses()
					.openParentheses()
						.where("listaFaixaimpostocliente is null").or()
						.where("listaFaixaimpostocliente.cliente = ?", cliente)
					.closeParentheses()
					.where("faixaimposto.controle = ?", controle)
					.where("faixaimposto.ativo = ?", Boolean.TRUE);
	}

	/**
	 * Verifica se exite uma faixa de imposto para aquele cliente
	 *
	 * @param cdfaixaimposto
	 * @param controle
	 * @param uf
	 * @param ufdestino
	 * @param municipio
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 * @param municipio 
	 * @since 19/07/2016
	 */
	public boolean haveFaixaimpostoIgualCliente(Integer cdfaixaimposto, Faixaimpostocontrole controle, Uf uf, Uf ufdestino, Municipio municipio, Cliente cliente) {
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.leftOuterJoin("faixaimposto.listaFaixaimpostocliente listaFaixaimpostocliente")
				.where("listaFaixaimpostocliente.cliente = ?", cliente)
				.where("faixaimposto.controle = ?", controle)
				.where("faixaimposto.uf = ?", uf)
				.where("faixaimposto.ufdestino = ?", ufdestino)
				.where("faixaimposto.municipio = ?", municipio)
				.where("faixaimposto.cdfaixaimposto <> ?", cdfaixaimposto)
				.unique() > 0;
	}
		
}
