package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Radiusaccounting;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RadiusaccountingDAO extends GenericDAO<Radiusaccounting>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Radiusaccounting> query, FiltroListagem _filtro) {
		query.where("acctstoptime is null")
			 .orderBy("username,acctstarttime");
	}
	
	/**
	 * Preenche a data de fim da sess�o dos registros passados como par�metro
	 * @param lista
	 * @author Thiago Gon�alves
	 */
	public void confirmartempo(String lista){
		StringBuilder sql = new StringBuilder(
				"update Radiusaccounting r " +
				"set r.acctstoptime = ? " +
				"where r.radacctid in (" + lista + ")");
		
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		Object[] fields = new Object[]{hoje};		
		getHibernateTemplate().bulkUpdate(sql.toString(), fields);		
	}
}
