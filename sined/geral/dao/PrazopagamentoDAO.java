package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PrazopagamentoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("prazopagamento.nome")
public class PrazopagamentoDAO extends GenericDAO<Prazopagamento> {

	@Override
	public void updateListagemQuery(QueryBuilder<Prazopagamento> query,	FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		PrazopagamentoFiltro filtro = (PrazopagamentoFiltro) _filtro;
		query
			.select("prazopagamento.cdprazopagamento, prazopagamento.nome, prazopagamento.ativo, prazopagamento.avista")
			.whereLikeIgnoreAll("prazopagamento.nome", filtro.getNome())
			.where("prazopagamento.ativo = ?", filtro.getAtivo())			
			.where("valorminimo = ?", filtro.getValorminimo());
		
		if(filtro.getPrazomedio() != null && filtro.getPrazomedio())
			query.where("prazopagamento.prazomedio = ?", filtro.getPrazomedio());
		
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Prazopagamento> query) {
		query
			.joinFetch("prazopagamento.listaPagamentoItem listaPagamentoItem")
			.orderBy("listaPagamentoItem.parcela");
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				save.saveOrUpdateManaged("listaPagamentoItem");
				return null;
			}
		});
	}

	/**
	 * Retorna o n�mero de itens de um prazo de pagamento.
	 * 
	 * @param prazo
	 * @return
	 * @author Hugo Ferreira
	 */
	public Long countItens(Prazopagamento prazo) {
		SinedUtil.markAsReader();
		QueryBuilder<Long> query = newQueryBuilder(Long.class);
		
		return query
			.select("count(*)")
			.from(Prazopagamentoitem.class)
			.where("prazopagamentoitem.prazopagamento = ?", prazo)
			.unique();
	}



	/**
	 * M�todo que busca o prazo de pagamento �nico
	 * 
	 * @return
	 * @author Tom�s T. Rabelo
	 */
	public Prazopagamento getPrazoPagamentoUnico() {
		return querySined()
			.select("prazopagamento.cdprazopagamento, prazopagamento.nome")
			.where("prazopagamento.nome = '�nica'")
			.where("prazopagamento.ativo = ?", Boolean.TRUE)
			.unique();
	}
	
	/**
	 * M�todo para carregar todas as propriedades de prazo de pagamento, bem como seus itens.
	 * 
	 * @param prazopagamento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Prazopagamento loadAll(Prazopagamento prazopagamento){
		if(!SinedUtil.isObjectValid(prazopagamento)){
			throw new SinedException("Prazopagamento inv�lido.");
		}
		
		return querySined()
			.select("prazopagamento.cdprazopagamento, prazopagamento.nome, prazopagamento.ativo, prazopagamento.tipoiniciodata, prazopagamento.avista, " +
					"item.cdprazopagamentoitem, item.parcela, item.dias, item.meses,prazopagamento.parcelasiguaisjuros, prazopagamento.juros")
			.leftOuterJoin("prazopagamento.listaPagamentoItem item")
			.where("prazopagamento = ?", prazopagamento)
			.unique();
	}

	/**
	 * M�todo que carrega todos os prazos de pagamento que est�o ativos no sistema
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Prazopagamento> findAtivosForFlex() {
		return querySined()
			.select("prazopagamento.cdprazopagamento, prazopagamento.nome")
			.where("prazopagamento.ativo = ?", Boolean.TRUE)
			.orderBy("prazopagamento.cdprazopagamento")
			.list();
	}
	
	/**
	 * M�todo que retorna todos os prazos pagamento
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Prazopagamento> findAllForFlex() {
		return querySined()
			.select("prazopagamento.cdprazopagamento, prazopagamento.nome, prazopagamento.avista")
			.orderBy("prazopagamento.nome")
			.list();
	}

	/**
	* M�todo que retorna o juros do prazo de pagamento
	*
	* @param prazopagamento
	* @return
	* @since Jul 21, 2011
	* @author Luiz Fernando F Silva
	*/
	public Prazopagamento findForPrazopagamentoJuros(Prazopagamento prazopagamento) {
		if(prazopagamento == null || prazopagamento.getCdprazopagamento() == null){
			throw new SinedException("Par�metro inv�lido"); 
		}
		
		return querySined()
					.select("prazopagamento.cdprazopagamento, prazopagamento.juros")
					.where("prazopagamento = ?", prazopagamento)
					.unique();
	}

	/**
	 * M�todo que busca a forma de pagamento de acordo com par�metro
	 *
	 * @param prazomedio
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Prazopagamento> findByPrazomedio(Boolean prazomedio) {
		if(prazomedio != null && prazomedio)
			return querySined()
						.select("prazopagamento.cdprazopagamento, prazopagamento.nome, prazopagamento.avista, prazopagamento.prazomedio")
						.where("prazomedio = ?", prazomedio)
						.where("ativo = true")
						.openParentheses()
							.where("prazopagamento.processoDeVenda is null").or()
						    .where("prazopagamento.processoDeVenda = true").or()
						    .openParentheses()
								.where("prazopagamento.processoDeVenda = false")
								.where("prazopagamento.processoDeCompra = false")
						    .closeParentheses()
					    .closeParentheses()
						.orderBy("prazopagamento.nome")
						.list();
		else
			return querySined()
						.select("prazopagamento.cdprazopagamento, prazopagamento.nome, prazopagamento.avista, prazopagamento.prazomedio")
						.openParentheses()
							.where("prazomedio = false").or()
							.where("prazomedio is null")
						.closeParentheses()
						.where("ativo = true")
						.openParentheses()
							.where("prazopagamento.processoDeVenda is null").or()
							.where("prazopagamento.processoDeVenda = true").or()
							.openParentheses()
								.where("prazopagamento.processoDeVenda = false")
								.where("prazopagamento.processoDeCompra = false")
							.closeParentheses()
						.closeParentheses()
						.orderBy("prazopagamento.nome")
						.list();
	}
	
	/**
	* M�todo que busca os prazos de pagamento para o or�amento
	*
	* @return
	* @since 19/03/2015
	* @author Luiz Fernando
	*/
	public List<Prazopagamento> findByPrazomedioOrcamento() {
		return querySined()
			.select("prazopagamento.cdprazopagamento, prazopagamento.nome, prazopagamento.avista, prazopagamento.prazomedio")
			.where("ativo = true")
			.openParentheses()
				.where("prazopagamento.processoDeVenda is null").or()
				.where("prazopagamento.processoDeVenda = true").or()
				.openParentheses()
					.where("prazopagamento.processoDeVenda = false")
					.where("prazopagamento.processoDeCompra = false")
				.closeParentheses()
			.closeParentheses()
			.orderBy("prazopagamento.nome")
			.list();
	}
	
	
	/**
	 * 
	 * M�todo que carrega o prazo de pagamento � partir do cdPrazoPagamento
	 *
	 *@author Thiago Augusto
	 *@date 02/04/2012
	 * @param cdPrazoPagamento
	 * @return
	 */
	public Prazopagamento findForCdPrazoPagamento(Integer cdPrazoPagamento){
		return querySined()
				.leftOuterJoinFetch("prazopagamento.listaPagamentoItem listaPagamentoItem")
				.where("prazopagamento.cdprazopagamento = ?", cdPrazoPagamento)
				.unique();
	}
	
	/**
     * Busca os dados para o cache da tela de pedido de venda offline
     * @author Igor Silv�rio Costa
	 * @date 20/04/2012
	 * 
	 */
	public List<Prazopagamento> findForPVOffline() {
		return query()
		.orderBy("prazopagamento.nome")
		.list();
	}
	
	public List<Prazopagamento> findForAndroid(String whereIn) {
		return query()
		.orderBy("prazopagamento.nome")
		.whereIn("prazopagamento.cdprazopagamento", whereIn)
		.list();
	}
	
	/**
	 * 
	 * M�todo que busca o valor do desconto m�ximo de um prazo de pagamento.
	 *
	 * @name buscaValormaximoDesconto
	 * @param bean
	 * @return
	 * @return Prazopagamento
	 * @author Thiago Augusto
	 * @date 12/06/2012
	 *
	 */
	public Prazopagamento buscaValormaximoDesconto(Prazopagamento bean){
		return querySined()
					.select("prazopagamento.cdprazopagamento, prazopagamento.valordescontomaximo")
					.where("prazopagamento = ? ", bean)
					.unique();
	}
	
	/**
	 * Busca os prazos de pagamento do processo de compra
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/09/2014
	 */
	public List<Prazopagamento> findForProcessoCompra(){
		return querySined()
					.select("prazopagamento.cdprazopagamento, prazopagamento.nome, prazopagamento.avista")
					.where("ativo = true")
					.openParentheses()
						.where("prazopagamento.processoDeCompra is null").or()
					    .where("prazopagamento.processoDeCompra = true").or()
					    .openParentheses()
							.where("prazopagamento.processoDeCompra = false")
							.where("prazopagamento.processoDeVenda = false")
					    .closeParentheses()
				    .closeParentheses()
					.orderBy("prazopagamento.nome")
					.list();
	}

	/**
	* M�todo que busca o prazo de pagamento de acordo com a quantidade de parcelas
	*
	* @param qtdeParcelas
	* @return
	* @since 17/02/2016
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public List<Prazopagamento> findByQtdeParcelas(Integer qtdeParcelas) {
		if(qtdeParcelas == null){
			throw new SinedException("Par�metro inv�lido");
		}
		
		String sql = " select p.cdprazopagamento, p.nome " +
					 " from prazopagamento p " +
					 " where (select count(*) from prazopagamentoitem ppi where ppi.cdprazopagamento = p.cdprazopagamento) = " + qtdeParcelas;
		
		SinedUtil.markAsReader();
		List<Prazopagamento> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Prazopagamento(rs.getInt("cdprazopagamento"),rs.getString("nome"));
			}
		});
		
		return list;
	}

	/**
	 * Busca prazos de pagamento para o WS SOAP
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/04/2016
	 */
	public List<Prazopagamento> findForWSSOAP() {
		return querySined()
					.select("prazopagamento.cdprazopagamento, prazopagamento.nome")
					.orderBy("prazopagamento.nome")
					.list();
	}
	
	/**
	 * Busca prazos de pagamento do cliente
	 *
	 * @return
	 * @author Thiago Clemente
	 * @since 27/05/2016
	 * 
	 */
	public List<Prazopagamento> findByCliente(Cliente cliente) {
		return querySined()
				.select("prazopagamento.cdprazopagamento, prazopagamento.nome")
				.join("prazopagamento.listaClientePrazoPagamento listaClientePrazoPagamento")
				.join("listaClientePrazoPagamento.cliente cliente")
				.where("cliente=?", cliente)
				.orderBy("prazopagamento.nome")
				.list();
	}
	
	/**
	 * Busca os prazos de pagamento do processo de venda
	 *
	 * @return
	 * @author Mairon Cezar
	 * @since 05/10/2017
	 */
	public List<Prazopagamento> findForProcessoVenda(){
		return querySined()
					.select("prazopagamento.cdprazopagamento, prazopagamento.nome, prazopagamento.avista")
					.where("ativo = true")
					.where("coalesce(prazopagamento.processoDeVenda, false) = true")
					.orderBy("prazopagamento.nome")
					.list();
	}
	
	public List<Prazopagamento> findAtivosForCombo(Prazopagamento prazopagamento) {
		return querySined()
			.select("prazopagamento.cdprazopagamento, prazopagamento.nome")
			.openParentheses()
				.where("prazopagamento.ativo = ?", Boolean.TRUE)
				.or()
				.where("prazopagamento = ?", prazopagamento)
			.closeParentheses()
			.orderBy("prazopagamento.nome")
			.list();
	}

	@Override
	public ListagemResult<Prazopagamento> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Prazopagamento> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("prazopagamento.cdprazopagamento");
		
		return new ListagemResult<Prazopagamento>(query, filtro);
	}

	public Prazopagamento findByPrazoPagamento(Prazopagamento prazopagamento) {
		if (prazopagamento == null || prazopagamento.getCdprazopagamento() == null) {
			throw new SinedException("Par�metro inv�lido");
		}
		
		return querySined()
				.select("prazopagamento.cdprazopagamento, prazopagamento.nome, prazopagamento.avista, prazopagamento.prazomedio")
				.where("prazopagamento = ?", prazopagamento)
				.unique();
	}
}