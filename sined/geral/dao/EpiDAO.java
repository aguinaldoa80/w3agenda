package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Epi;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EpiDAO extends GenericDAO<Epi> {
	
	/**
	 * Carrega um epi com todas as informa��es do epi.
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Epi carregaEpi(Material material) {
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		return query()
					.select("epi.nomereduzido, epi.observacao, epi.duracao, epi.caepi")
					.where("epi.cdmaterial = ?",material.getCdmaterial())
					.unique();
	}
	
	/**
	 * Deleta somente o registro da tabela Epi.
	 * 
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void deleteEpi(Material material) {
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		String delete = "DELETE FROM EPI WHERE CDMATERIAL = ?";
		getJdbcTemplate().update(delete, new Object[]{material.getCdmaterial()});		
	}

	/**
	 * Insere um registro na tabela de Epi.
	 * 
	 * @param produto
	 * @author Rodrigo Freitas
	 */
	public void insertEpi(Epi epi) {
		if (epi == null || epi.getCdmaterial() == null) {
			throw new SinedException("Epi n�o pode ser nulo.");
		}
		
		String insert = "INSERT INTO EPI (CDMATERIAL, NOMEREDUZIDO, DURACAO, OBSERVACAO, CDUSUARIOALTERA, DTALTERA, CAEPI)" +
				" VALUES (?,?,?,?,?,?,?)";
		getJdbcTemplate().update(insert, new Object[]{epi.getCdmaterial(),epi.getNomereduzido(),epi.getDuracao(),
													epi.getObservacao(), SinedUtil.getUsuarioLogado().getCdpessoa()
													,new Timestamp(System.currentTimeMillis()),epi.getCaepi()});
	}

	/**
	 * Faz update no epi.
	 * 
	 * @param produto
	 * @author Rodrigo Freitas
	 */
	public void updateEpi(Epi epi) {
		if (epi == null || epi.getCdmaterial() == null) {
			throw new SinedException("Epi n�o pode ser nulo.");
		}
		
		String update = "UPDATE EPI SET NOMEREDUZIDO = ?, DURACAO = ?, OBSERVACAO = ?, " +
						"CDUSUARIOALTERA = ?, DTALTERA = ?, CAEPI = ? WHERE EPI.CDMATERIAL = ?";
		getJdbcTemplate().update(update, new Object[]{epi.getNomereduzido(), epi.getDuracao(),
				epi.getObservacao(), SinedUtil.getUsuarioLogado().getCdpessoa()
				,new Timestamp(System.currentTimeMillis()),epi.getCaepi(),epi.getCdmaterial()});
	}
	
	/**
	 * Retorna o n�mero de registros da tabela Epi do cdmaterial passado.
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Long countEpi(Material material){
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Epi.class)
					.where("epi.cdmaterial = ?",material.getCdmaterial())
					.unique();
	}
	
	
	/**
	 * Retorna o n�mero de registros que tem na tabela epi com o 
	 * mesmo nome do epi cadastrado.
	 * 
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Long verificaEpi(Material bean) {
		if (bean == null || bean.getEpi_nomereduzido() == null) {
			throw new SinedException("Nome reduzido do epi n�o pode ser nulo.");
		}
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(Epi.class)
					.where("upper(epi.nomereduzido) = ?",bean.getEpi_nomereduzido().toUpperCase())
					.where("epi.cdmaterial <> ?",bean.getCdmaterial())
					.unique();
	}

	/**
	 * Busca todos os epis.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Epi> findAllEpi() {
		return query()
					.select("epi.cdmaterial, epi.nome")
					.where("epi.epi = ?", Boolean.TRUE)
					.orderBy("epi.nome")
					.list();
	}
	
	/**
	 * Busca todos os epis ativos.
	 *
	 * @return
	 * @author Mairon Cezar
	 */
	public List<Epi> findAllEpisAtivos() {
		return query()
					.select("epi.cdmaterial, epi.nome")
					.where("epi.epi = ?", Boolean.TRUE)
					.where("coalesce(epi.ativo, true) = ?", Boolean.TRUE)
					.orderBy("epi.nome")
					.list();
	}
	
}
