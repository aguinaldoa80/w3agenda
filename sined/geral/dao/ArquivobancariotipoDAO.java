package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Arquivobancariotipo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("arquivobancariotipo.nome")
public class ArquivobancariotipoDAO extends GenericDAO<Arquivobancariotipo>{

}
