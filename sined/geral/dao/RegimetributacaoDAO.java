package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Regimetributacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("regimetributacao.descricao")
public class RegimetributacaoDAO extends GenericDAO<Regimetributacao>{

	/**
	 * M�todo que busca o regime de tributa��o pelo codigonfse
	 *
	 * @param regimeEspecialTributacao
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Regimetributacao> findByCodigocnaexml(String regimeEspecialTributacao) {
		if(regimeEspecialTributacao == null || "".equals(regimeEspecialTributacao))
			throw new SinedException("C�digo do regime de tributa��o n�o pode ser nulo. ");
		
		return query()
				.select("regimetributacao.cdregimetributacao, regimetributacao.codigonfse, regimetributacao.codigonfe")
				.where("regimetributacao.codigonfse = ?", regimeEspecialTributacao)
				.where("regimetributacao.ativo = true")
				.list();
	}

	@Override
	public ListagemResult<Regimetributacao> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Regimetributacao> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("regimetributacao.cdregimetributacao");
		
		return new ListagemResult<Regimetributacao>(query, filtro);
	}
}
