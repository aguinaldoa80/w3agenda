package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.sined.geral.bean.Arquivonf;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel.TipoComprovante;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresamodelocomprovanteorcamento;
import br.com.linkcom.sined.geral.bean.Empresamodelocontrato;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Pessoaconfiguracao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.service.PessoaconfiguracaoService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.EmpresaFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedpiscofinsFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

@DefaultOrderBy("empresa.nomefantasia")
public class EmpresaDAO extends GenericDAO<Empresa> {
	private ArquivoDAO arquivoDAO;
	private PessoaconfiguracaoService pessoaconfiguracaoService;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setPessoaconfiguracaoService(PessoaconfiguracaoService pessoaconfiguracaoService) {
		this.pessoaconfiguracaoService = pessoaconfiguracaoService;
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Empresa> query, FiltroListagem _filtro) {
		EmpresaFiltro filtro = (EmpresaFiltro) _filtro;
		query
			.select("empresa.cdpessoa,empresa.nome,empresa.razaosocial,empresa.cpf,empresa.cnpj,empresa.principal,empresa.ativo,empresa.nomefantasia,empresa.propriedadeRural")
			.where("empresa.ativo = ?",filtro.getAtivo())
			.whereLikeIgnoreAll("empresa.nome", filtro.getNome())
			.whereLikeIgnoreAll("empresa.razaosocial", filtro.getRazaosocial())
			.where("empresa.propriedadeRural = ?", filtro.getPropriedadeRural());
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				
				Empresa empresa = (Empresa) save.getEntity();
				if(empresa.getLogomarca() != null){
					arquivoDAO.saveFile(empresa, "logomarca");
				}
				if(empresa.getLogomarcasistema() != null){
					arquivoDAO.saveFile(empresa, "logomarcasistema");
				}
				
				save.saveOrUpdateManaged("listaPessoadap");
				save.saveOrUpdateManaged("listaEndereco");
				save.saveOrUpdateManaged("listaTelefone");
				save.saveOrUpdateManaged("listaEmpresamodelocontrato");
				save.saveOrUpdateManaged("listaEmpresacodigocnae");
				save.saveOrUpdateManaged("listaEmpresarepresentacao");
				save.saveOrUpdateManaged("listaEmpresaproprietario");
				save.saveOrUpdateManaged("listaEmpresamodelocomprovanteorcamento");

				if(empresa.getListaEmpresamodelocontrato() != null){
					for (Empresamodelocontrato item : empresa.getListaEmpresamodelocontrato()){
						if (item.getCdempresamodelocontrato() == null)
							arquivoDAO.saveFile(item, "arquivo");
					}
				}
				if(empresa.getListaEmpresamodelocomprovanteorcamento() != null){
					for (Empresamodelocomprovanteorcamento item : empresa.getListaEmpresamodelocomprovanteorcamento()){
						if (item.getCdempresamodelocomprovanteorcamento() == null)
							arquivoDAO.saveFile(item, "arquivo");
					}
				}
				
				return null;
			}
		});
	}
	
	public void saveOrUpdate(final Empresa bean) {
		Pessoaconfiguracao pessoaconfiguracao = pessoaconfiguracaoService.preenchePessoaconfiguracao(bean);
		
		super.saveOrUpdate(bean);
		
		if(pessoaconfiguracao != null){
			pessoaconfiguracao.setPessoa(new Pessoa(bean.getCdpessoa()));
			pessoaconfiguracaoService.saveOrUpdate(pessoaconfiguracao);
			bean.setPessoaconfiguracao(pessoaconfiguracao);
			updatePessoaconfiguracao(bean, pessoaconfiguracao);
		}
	};
	
	
	private void updatePessoaconfiguracao(Empresa empresa,	Pessoaconfiguracao pessoaconfiguracao) {
		if(empresa != null && empresa.getCdpessoa() != null){
			String sql = "UPDATE empresa SET cdpessoaconfiguracao = " + (pessoaconfiguracao != null ? pessoaconfiguracao.getCdpessoaconfiguracao() : "null") + 
						 " WHERE cdpessoa = " + empresa.getCdpessoa();
			getJdbcTemplate().execute(sql);
		}
		
	}
	
//	@Override
//	public Empresa loadForEntrada(Empresa bean) {
//	
//		Empresa empresa = super.loadForEntrada(bean);
//		
//		//Os itens est�o vindo repetidos
//		if (empresa.getListaEmpresamodelocontrato() != null){
//			HashSet<Empresamodelocontrato> itens = new HashSet<Empresamodelocontrato>();
//			itens.addAll(empresa.getListaEmpresamodelocontrato());
//			empresa.setListaEmpresamodelocontrato(new ArrayList<Empresamodelocontrato>(itens));
//		}
//		
//		return empresa;
//	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Empresa> query) {
		query
		.select(
				"empresa.cdpessoa, empresa.cpf, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.email, empresa.emailordemcompra, empresa.site, empresa.propriedadeRural, " + 
				"empresa.cnpj, empresa.inscricaomunicipal, empresa.inscricaoestadual, empresa.inscricaoestadualst, empresa.emailfinanceiro, empresa.formaEnvioBoleto,  " +
				"empresa.principal, empresa.ativo, empresa.integracaowms, empresa.integracaourlwms, empresa.calcularQuantidadeTransporte, empresa.codigoEmpresaDominioIntegracao, " +
				"empresa.proximoNumNfce, empresa.serieNfce, empresa.incentivoFiscalIss, empresa.proximoidentificadorcarregamento, empresa.usuarioCorreios, empresa.senhaCorreios, " +
				"empresa.cdusuarioaltera, empresa.dtaltera, empresa.presencacompradornfe, empresa.considerarIdPneu, reportboleto.cdreporttemplate, reportboleto.nome, " +
				"responsavel.cdpessoa, responsavel.nome, responsavel.cnpj, responsavel.cpf, " +
				"listaTelefone.cdtelefone, listaTelefone.telefone, telefonetipo.cdtelefonetipo, telefonetipo.nome, " +
				"listaEmpresacodigocnae.cdempresacodigocnae, listaEmpresacodigocnae.principal, " +
				"codigocnae.cdcodigocnae, codigocnae.cnae, codigocnae.descricaocnae, " +
				"escritoriocontabilista.cdpessoa, escritoriocontabilista.nome, escritoriocontabilista.cpf, escritoriocontabilista.cnpj, " +
				"colaboradorcontabilista.cdpessoa, colaboradorcontabilista.nome, colaboradorcontabilista.cpf, colaboradorcontabilista.cnpj, " +
				"empresa.crccontabilista, empresa.indiceperfilsped, empresa.indiceatividadesped, empresa.indiceatividadespedpiscofins, " +
				"ufsped.cduf, ufsped.nome, ufsped.sigla, ufsped.cdibge, " +
				"municipiosped.cdmunicipio, municipiosped.nome, municipiosped.cdibge, municipiosped.cdsiafi, municipiosped.codigotom, " +
				"empresa.proximonumnf, empresa.proximonummdfe, empresa.proximonumnfproduto, empresa.serienfe, " +
				"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.descricao, naturezaoperacao.codigonfse, naturezaoperacao.simplesremessa, " +
				"regimetributacao.cdregimetributacao, regimetributacao.descricao, regimetributacao.codigonfse, " +
				"empresa.codigotributacao, empresa.naopreencherdtsaida, " +
				"itemlistaservico.cditemlistaservico, itemlistaservico.codigo, itemlistaservico.descricao, " +
				"empresa.crt, empresa.cstpisnfs, empresa.cstcofinsnfs, empresa.tributacaomunicipiocliente, " +
				"empresa.proximoidentificadorpedidovenda, empresa.exibiripivenda, " +
				"centrocusto.cdcentrocusto, centrocusto.nome, centrocusto.ativo, " +
				"contagerencial.cdcontagerencial, contagerencial.nome, contagerencial.item, contagerencial.natureza, " +
				"empresa.proximoidentificadorvenda, empresa.proximoidentificadorvendaorcamento, " +
				"contagerencialicms.cdcontagerencial, contagerencialicms.nome, contagerencialicms.item, contagerencialicms.natureza, " +
				"contagerencialicmsst.cdcontagerencial, contagerencialicmsst.nome, contagerencialicmsst.item, contagerencialicmsst.natureza, " +
				"contagerencialipi.cdcontagerencial, contagerencialipi.nome, contagerencialipi.item, contagerencialipi.natureza, " +
				"contagerencialiss.cdcontagerencial, contagerencialiss.nome, contagerencialiss.item, contagerencialiss.natureza, " +
				"contagerencialpis.cdcontagerencial, contagerencialpis.nome, contagerencialpis.item, contagerencialpis.natureza, " +
				"contagerencialcofins.cdcontagerencial, contagerencialcofins.nome, contagerencialcofins.item, contagerencialcofins.natureza, " +
				"contagerencialfrete.cdcontagerencial, contagerencialfrete.nome, contagerencialfrete.item, contagerencialfrete.natureza, " +
				"contagerencialii.cdcontagerencial, contagerencialii.nome, contagerencialii.item, contagerencialii.natureza, " +
				"empresa.proximonumfaturalocacao, empresa.proximonumanimal, " +
				"centrocustoveiculouso.cdcentrocusto, centrocustoveiculouso.nome, centrocustoveiculouso.ativo, " +
				"comprovanteConfiguravelPedidoVenda.cdcomprovanteconfiguravel, comprovanteConfiguravelPedidoVenda.descricao, comprovanteConfiguravelPedidoVenda.descricao, " +
				"comprovanteConfiguravelVenda.cdcomprovanteconfiguravel, comprovanteConfiguravelVenda.descricao, comprovanteConfiguravelVenda.descricao, " +
				"comprovanteConfiguravelOrcamento.cdcomprovanteconfiguravel, comprovanteConfiguravelOrcamento.descricao, comprovanteConfiguravelOrcamento.descricao, " +
				"comprovanteConfiguravelColeta.cdcomprovanteconfiguravel, comprovanteConfiguravelColeta.descricao, " +
				"listaEndereco.cdendereco, listaEndereco.logradouro, listaEndereco.bairro, listaEndereco.caixapostal, listaEndereco.cep, " +
				"listaEndereco.numero, listaEndereco.complemento, listaEndereco.pontoreferencia, listaEndereco.distancia, " +
				"ufEnd.cduf, ufEnd.sigla, ufEnd.nome, ufEnd.cdibge, " +
				"municipioEnd.cdmunicipio, municipioEnd.nome, municipioEnd.cdibge, municipioEnd.cdsiafi, municipioEnd.codigotom, " +
				"enderecotipoEnd.cdenderecotipo, enderecotipoEnd.nome, " +
				"empresa.observacaoreciboreceber, empresa.observacaorecibopagar, empresa.observacaovenda, empresa.textoboleto, empresa.textoautenticidadeboleto, " +
				"empresa.textoinfcontribuinte, empresa.termorecapagem, empresa.exibirIcmsVenda, contagerencialcredito.cdcontagerencial, contagerencialcredito.nome, " +
				"contagerencialdebitoadiantamento.cdcontagerencial, contagerencialdebitoadiantamento.nome, " +
				"contagerencialdebito.cdcontagerencial, contagerencialdebito.nome, " +
				"centrocustodespesaviagem.cdcentrocusto, centrocustodespesaviagem.nome, " +
				"listaEmpresamodelocontrato.cdempresamodelocontrato, listaEmpresamodelocontrato.nome, " +
				"arquivoModelo.cdarquivo, arquivoModelo.nome, arquivoModelo.tipoconteudo, " +
				"listaEmpresarepresentacao.cdempresarepresentacao, listaEmpresarepresentacao.comissaovenda, listaEmpresarepresentacao.repasseunico, " +
				"fornecedor.cdpessoa, fornecedor.nome, materialgrupo.cdmaterialgrupo, materialgrupo.nome, " +
				"logomarca.cdarquivo, logomarca.nome, logomarca.tipoconteudo, " +
				"logomarcasistema.cdarquivo, logomarcasistema.nome, logomarcasistema.tipoconteudo, empresa.integracaopdv, empresa.proximoidentificadorcontrato, " +
				"empresa.exibirIcmsVenda, empresa.exibirDifalVenda, " +
				"empresa.marcanf, empresa.especienf, empresa.proximonumoportunidade, empresa.proximonumeroordemservico, empresa.proximonumeromatricula, " +
				"pessoaconfiguracao.cdpessoaconfiguracao, transportador.cdpessoa, transportador.nome, " +
				"empresa.geracaospedpiscofins, empresa.geracaospedicmsipi, empresa.discriminarservicoqtdevalor, empresa.permitirInclusaoMaterialServico, " +
				"contagerencialRepresentacao.cdcontagerencial, centrocustoRepresentacao.cdcentrocusto, " +
				"contabancariacontareceber.cdconta, contacarteiracontareceber.cdcontacarteira, " +
				"centrocustotransferencia.cdcentrocusto, empresa.diageracaospedpiscofins, empresa.diageracaospedicmsipi, " +
				"contagerencialdebitotransferencia.cdcontagerencial, contagerencialdebitotransferencia.nome, " +
				"contagerencialcreditotransferencia.cdcontagerencial, contagerencialcreditotransferencia.nome, " +
				"contagerencialdevolucaocredito.cdcontagerencial, contagerencialdevolucaocredito.nome, " +
				"contagerencialdevolucaodebito.cdcontagerencial, contagerencialdevolucaodebito.nome, " +
				"clientefeedapplication.cdpessoa, materialfeedapplication.cdmaterial, pedidovendatipofeedapplication.cdpedidovendatipo, " +
				"prazopagamentofeedapplication.cdprazopagamento, documentotipofeedapplication.cddocumentotipo, localarmazenagemmateriaprima.cdlocalarmazenagem, localarmazenagemmateriaprima.nome, empresa.integracaocontabil, "+
				"localarmazenagemprodutofinal.cdlocalarmazenagem, localarmazenagemprodutofinal.nome, empresa.tipopessoa, " +
				"listaEmpresaproprietario.cdempresaproprietario, listaEmpresaproprietario.principal, " +
				"empresaProp.nome, empresaProp.cdpessoa, colaboradorProp.cdpessoa, colaboradorProp.nome, " +
				"contabaixadinheiro.cdconta, contabaixadinheiro.nome, " +
				"contabaixacheque.cdconta, contabaixacheque.nome, " +
				"contabaixacreditoconta.cdconta, contabaixacreditoconta.nome, " +
				"contaGerencialOutrasDespesas.cdcontagerencial, contaGerencialOutrasDespesas.nome, contaGerencialOutrasDespesas.item, contaGerencialOutrasDespesas.natureza, " +
				"contaGerencialSeguro.cdcontagerencial, contaGerencialSeguro.nome, contaGerencialSeguro.item, contaGerencialSeguro.natureza, " +
				"contadevolucao.cdconta, contadevolucao.nome, " +
				"modeloorcamento.cdempresamodelocomprovanteorcamento, modeloorcamento.nome,modeloorcamento.tipoComprovante, arquivoorcamento.cdarquivo, arquivoorcamento.nome, arquivoorcamento.tipoconteudo")
			.leftOuterJoin("empresa.listaEmpresacodigocnae listaEmpresacodigocnae")
			.leftOuterJoin("listaEmpresacodigocnae.codigocnae codigocnae")
			.leftOuterJoin("empresa.listaEmpresamodelocontrato listaEmpresamodelocontrato")
			.leftOuterJoin("listaEmpresamodelocontrato.arquivo arquivoModelo")
			.leftOuterJoin("empresa.cdreporttemplateboleto reportboleto")
			.leftOuterJoin("empresa.logomarca logomarca")
			.leftOuterJoin("empresa.municipiosped municipiosped")
			.leftOuterJoin("empresa.ufsped ufsped")
			.leftOuterJoin("empresa.codigotributacao codigotributacao")
			.leftOuterJoin("empresa.itemlistaservico itemlistaservico")
			.leftOuterJoin("empresa.logomarcasistema logomarcasistema")
			.leftOuterJoin("empresa.centrocusto centrocusto")
			.leftOuterJoin("empresa.contagerencial contagerencial")
			.leftOuterJoin("empresa.escritoriocontabilista escritoriocontabilista")
			.leftOuterJoin("empresa.colaboradorcontabilista colaboradorcontabilista")
			.leftOuterJoin("empresa.centrocustoveiculouso centrocustoveiculouso")
			.leftOuterJoin("empresa.listaEndereco listaEndereco")
			.leftOuterJoin("listaEndereco.municipio municipioEnd")
			.leftOuterJoin("municipioEnd.uf ufEnd")
			.leftOuterJoin("listaEndereco.enderecotipo enderecotipoEnd")
			.leftOuterJoin("empresa.listaTelefone listaTelefone")
			.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
			.leftOuterJoin("empresa.responsavel responsavel")
			.leftOuterJoin("empresa.listaEmpresarepresentacao listaEmpresarepresentacao")
			.leftOuterJoin("listaEmpresarepresentacao.fornecedor fornecedor")
			.leftOuterJoin("listaEmpresarepresentacao.materialgrupo materialgrupo")
			.leftOuterJoin("listaEmpresarepresentacao.contagerencial contagerencialRepresentacao")
			.leftOuterJoin("listaEmpresarepresentacao.centrocusto centrocustoRepresentacao")
			.leftOuterJoin("empresa.contagerencialicms contagerencialicms")
			.leftOuterJoin("empresa.contagerencialipi contagerencialipi")
			.leftOuterJoin("empresa.contagerencialiss contagerencialiss")
			.leftOuterJoin("empresa.contagerencialpis contagerencialpis")
			.leftOuterJoin("empresa.contagerencialcofins contagerencialcofins")
			.leftOuterJoin("empresa.contagerencialfrete contagerencialfrete")
			.leftOuterJoin("empresa.contagerencialicmsst contagerencialicmsst")
			.leftOuterJoin("empresa.contagerencialii contagerencialii")
			.leftOuterJoin("empresa.comprovanteConfiguravelPedidoVenda comprovanteConfiguravelPedidoVenda")
			.leftOuterJoin("empresa.comprovanteConfiguravelVenda comprovanteConfiguravelVenda")
			.leftOuterJoin("empresa.comprovanteConfiguravelOrcamento comprovanteConfiguravelOrcamento")
			.leftOuterJoin("empresa.comprovanteConfiguravelColeta comprovanteConfiguravelColeta")
			.leftOuterJoin("empresa.naturezaoperacao naturezaoperacao")
			.leftOuterJoin("empresa.regimetributacao regimetributacao")
			.leftOuterJoin("empresa.contagerencialcredito contagerencialcredito")
			.leftOuterJoin("empresa.contagerencialdebito contagerencialdebito")
			.leftOuterJoin("empresa.contagerencialdebitoadiantamento contagerencialdebitoadiantamento")
			.leftOuterJoin("empresa.pessoaconfiguracao pessoaconfiguracao")
			.leftOuterJoin("pessoaconfiguracao.transportador transportador")
			.leftOuterJoin("empresa.centrocustodespesaviagem centrocustodespesaviagem")
			.leftOuterJoin("empresa.contabancariacontareceber contabancariacontareceber")
			.leftOuterJoin("empresa.contacarteiracontareceber contacarteiracontareceber")
			.leftOuterJoin("empresa.contagerencialdebitotransferencia contagerencialdebitotransferencia")
			.leftOuterJoin("empresa.contagerencialcreditotransferencia contagerencialcreditotransferencia")
			.leftOuterJoin("empresa.centrocustotransferencia centrocustotransferencia")
			.leftOuterJoin("empresa.contagerencialdevolucaocredito contagerencialdevolucaocredito")
			.leftOuterJoin("empresa.contagerencialdevolucaodebito contagerencialdevolucaodebito")
			.leftOuterJoin("empresa.clientefeedapplication clientefeedapplication")
			.leftOuterJoin("empresa.materialfeedapplication materialfeedapplication")
			.leftOuterJoin("empresa.pedidovendatipofeedapplication pedidovendatipofeedapplication")
			.leftOuterJoin("empresa.prazopagamentofeedapplication prazopagamentofeedapplication")
			.leftOuterJoin("empresa.documentotipofeedapplication documentotipofeedapplication")
			.leftOuterJoin("empresa.localarmazenagemprodutofinal localarmazenagemprodutofinal")
			.leftOuterJoin("empresa.localarmazenagemmateriaprima localarmazenagemmateriaprima")
			.leftOuterJoin("empresa.listaEmpresaproprietario listaEmpresaproprietario")
			.leftOuterJoin("listaEmpresaproprietario.empresa empresaProp")
			.leftOuterJoin("listaEmpresaproprietario.colaborador colaboradorProp")
			.leftOuterJoin("empresa.contabaixadinheiro contabaixadinheiro")
			.leftOuterJoin("empresa.contabaixacheque contabaixacheque")
			.leftOuterJoin("empresa.contabaixacreditoconta contabaixacreditoconta")
			.leftOuterJoin("empresa.contadevolucao contadevolucao")
			.leftOuterJoin("empresa.listaEmpresamodelocomprovanteorcamento modeloorcamento")
			.leftOuterJoin("empresa.contaGerencialOutrasDespesas contaGerencialOutrasDespesas")
			.leftOuterJoin("empresa.contaGerencialSeguro contaGerencialSeguro")
			.leftOuterJoin("modeloorcamento.arquivo arquivoorcamento")
			;
	}
	
//	/**
//	 * M�todo sobrescrito para colocar nos combos, por padr�o, os registros ativos de empresa.
//	 * 
//	 * @author Hugo Ferreira
//	 */
//	@Override
//	@SuppressWarnings("unchecked")
//	public List<Empresa> findForCombo(String... extraFields) {
//		List<Empresa> findForComboCache = (List<Empresa>) NeoWeb.getRequestContext().getAttribute(NEO_COMBO_CACHE);
//		
//		if(extraFields != null && extraFields.length > 0){
//			QueryBuilder<Empresa> newQueryBuilder = newQueryBuilderWithCache(beanClass);
//			
//			return newQueryBuilder
//						.select(getComboSelect(extraFields))
//						.from(beanClass)
//						.orderBy(orderBy)
//						.where("ativo = ?",Boolean.TRUE)
//						.list();
//		} else {
//			if(queryFindForCombo == null){
//				initQueryFindForCombo();
//			}
//			if (findForComboCache == null) {
//				//inicializa o cache caso nao tenha sido criado.
//				findForComboCache = new ArrayList<Empresa>();
//				
//				HibernateTemplate hbmTemplate = getHibernateTemplate();
//				Cache cache = ReflectionCacheFactory.getReflectionCache().getAnnotation(beanClass, Cache.class);
//				if(cache != null) {
//					hbmTemplate.setCacheQueries(true);
//					hbmTemplate.setQueryCacheRegion("findforcombo");
//				}
//				findForComboCache = hbmTemplate.find(queryFindForCombo);
//				if(cache != null) {
//					hbmTemplate.setCacheQueries(false);
//				}
//				findForComboCache = translatorQueryFindForCombo.translate(findForComboCache);
//
//				NeoWeb.getRequestContext().setAttribute(NEO_COMBO_CACHE, findForComboCache);
//			}
//			return findForComboCache;	
//		}
//	}
	
	/**
	 * Carrega a empresa marcada como principal.
	 * 
	 * @return
	 * @author Hugo Ferreira
	 */
	public Empresa loadPrincipal(boolean useWhereEmpresa) {
		return querySined()
			
			.setUseWhereEmpresa(useWhereEmpresa)
			.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.principal, logomarca.cdarquivo, logomarcasistema.cdarquivo, empresa.tipopessoa, " +
					"empresa.cpf, empresa.cnpj, empresa.site, empresa.propriedadeRural, " +
					"empresa.inscricaoestadual, empresa.email, empresa.inscricaomunicipal, empresa.emailordemcompra, empresa.textoboleto, empresa.emailfinanceiro, cdreporttemplateboleto.cdreporttemplate, " +
					"listaTelefone.cdtelefone, listaTelefone.telefone, empresa.proximoidentificadorvenda, empresa.proximoidentificadorvendaorcamento, empresa.proximoidentificadorpedidovenda, " +
					"empresa.estabelecimento, empresa.integracaocontabil," +
	                "listaEmpresaproprietario.cdempresaproprietario, listaEmpresaproprietario.principal, " +
	                "colaborador.cdpessoa, colaborador.nome, colaborador.cpf, colaborador.cnpj ")
			.leftOuterJoin("empresa.logomarca logomarca")
			.leftOuterJoin("empresa.logomarcasistema logomarcasistema")
			.leftOuterJoin("empresa.listaTelefone listaTelefone")
			.leftOuterJoin("empresa.cdreporttemplateboleto cdreporttemplateboleto")
			.leftOuterJoin("empresa.listaEmpresaproprietario listaEmpresaproprietario")
			.leftOuterJoin("listaEmpresaproprietario.colaborador colaborador")
			.where("empresa.principal = ?", Boolean.TRUE)
			.unique();
	}
	
	public Empresa loadPrincipalEmitirValeCompra(boolean useWhereEmpresa){
		return querySined()
				
				.setUseWhereEmpresa(useWhereEmpresa)
				.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.principal, empresa.tipopessoa, " +
						"empresa.cpf, empresa.cnpj, empresa.site, empresa.propriedadeRural, " +
						"empresa.inscricaoestadual, empresa.email, empresa.inscricaomunicipal, empresa.emailordemcompra, empresa.textoboleto, empresa.emailfinanceiro, cdreporttemplateboleto.cdreporttemplate, " +
						"listaTelefone.cdtelefone, listaTelefone.telefone, empresa.proximoidentificadorvenda, empresa.proximoidentificadorvendaorcamento, empresa.proximoidentificadorpedidovenda, " +
						"empresa.estabelecimento, empresa.integracaocontabil," +
		                "listaEmpresaproprietario.cdempresaproprietario, listaEmpresaproprietario.principal, " +
		                "colaborador.cdpessoa, colaborador.nome, colaborador.cpf, colaborador.cnpj,"+
		                "endereco.logradouro, endereco.bairro, endereco.numero, municipio.nome, uf.sigla, "+
		                "telefone.telefone,"+
		                "logomarca.cdarquivo")				
				.leftOuterJoin("empresa.listaTelefone listaTelefone")
				.leftOuterJoin("empresa.cdreporttemplateboleto cdreporttemplateboleto")
				.leftOuterJoin("empresa.listaEmpresaproprietario listaEmpresaproprietario")
				.leftOuterJoin("listaEmpresaproprietario.colaborador colaborador")	
				.leftOuterJoin("empresa.listaEndereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("empresa.listaTelefone telefone")
				.leftOuterJoin("empresa.logomarca logomarca")
				.where("empresa.principal = ?", Boolean.TRUE)
				.unique();
		
		
	}
	
	public Empresa loadEmpresa(Empresa empresa) {
		return querySined()
			
			.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.principal, logomarca.cdarquivo, logomarcasistema.cdarquivo, empresa.tipopessoa, empresa.cpf, empresa.cnpj, empresa.site, " +
					"empresa.inscricaoestadual, empresa.email, empresa.inscricaomunicipal, empresa.emailordemcompra, empresa.textoboleto, empresa.emailfinanceiro, " +
					"listaTelefone.cdtelefone, listaTelefone.telefone, empresa.proximoidentificadorvenda, empresa.proximoidentificadorvendaorcamento, empresa.proximoidentificadorpedidovenda, " +
					"empresa.estabelecimento, empresa.integracaocontabil, empresa.propriedadeRural, " +
					"listaEmpresaproprietario.cdempresaproprietario, listaEmpresaproprietario.principal, " +
	                "colaborador.cdpessoa, colaborador.nome, colaborador.cpf, colaborador.cnpj ")
			.leftOuterJoin("empresa.logomarca logomarca")
			.leftOuterJoin("empresa.logomarcasistema logomarcasistema")
			.leftOuterJoin("empresa.listaTelefone listaTelefone")
			.leftOuterJoin("empresa.listaEmpresaproprietario listaEmpresaproprietario")
			.leftOuterJoin("listaEmpresaproprietario.colaborador colaborador")
			.where("empresa = ?", empresa)
			.unique();
	}

	/**
	 * Retorna a raz�o social da empresa principal
	 * @return
	 * @author Thiago Gon�alves
	 */
	public String razaoEmpresaPrincipal() {
		Empresa empresa = querySined()
							.setUseWhereEmpresa(false)
							.select("empresa.nome, empresa.razaosocial")
							.from(Empresa.class)
							.where("empresa.principal = ?", Boolean.TRUE)
							.unique();
		
		if (empresa != null){
			return StringUtils.isNotEmpty(empresa.getRazaosocial()) ? empresa.getRazaosocial() : empresa.getNome();
		}else 
			return "";
	}
	
	public String cnpjEmpresaPrincipal() {
		Empresa empresa = querySined()
								
								.setUseWhereEmpresa(false)
								.select("empresa.cnpj")
								.from(Empresa.class)
								.where("empresa.principal = ?", Boolean.TRUE)
								.unique();
						
		if (empresa != null && empresa.getCnpj() != null)
			return empresa.getCnpj().getValue();
		else 
			return "";
	}
	
	public String cpfEmpresaPrincipal() {
		Empresa empresa = querySined()
				.setUseWhereEmpresa(false)
				.select("empresa.cpf")
				.from(Empresa.class)
				.where("empresa.principal = ?", Boolean.TRUE)
				.unique();
		
		if (empresa != null && empresa.getCpf() != null)
			return empresa.getCpf().getValue();
		else 
			return "";
	}
	
	/**
	 * Carrega o arquivo da empresa principal.
	 * 
	 * @author Fl�vio Tavares
	 */
	public Empresa loadArquivoPrincipal(){
		return querySined()
			
			.setUseWhereEmpresa(false)
			.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, logomarca.cdarquivo, logomarcasistema.cdarquivo")
			.leftOuterJoin("empresa.logomarca logomarca")
			.leftOuterJoin("empresa.logomarcasistema logomarcasistema")
			.where("empresa.principal = ?", Boolean.TRUE)
			.unique();
	}
	
	/**
	 * Carrega a lista de empresas ativas sem o filtro de empresas.
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Empresa> findAtivosSemFiltro(){
		return querySined()
					.setUseWhereEmpresa(false)
					.select("empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cdpessoa, empresa.cnpj, empresa.cpf ")
					.where("ativo = ?",Boolean.TRUE)
					.orderBy("empresa.nomefantasia")
					.list();
	}
	
	public List<Empresa> findAtivosSemFiltroProprietarioRural(){
		return querySined()
					.setUseWhereEmpresa(false)
					.select("empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cdpessoa, empresa.cnpj, empresa.cpf, empresa.propriedadeRural, colaborador.cpf, listaEmpresaproprietario.principal ")
					.leftOuterJoin("empresa.listaEmpresaproprietario listaEmpresaproprietario")
					.leftOuterJoin("listaEmpresaproprietario.colaborador colaborador")
					.where("ativo = ?",Boolean.TRUE)
					.orderBy("empresa.nomefantasia")
					.list();
	}
	
	public List<Empresa> findAtivosSemFiltro(Empresa empresa){
		return querySined()
		.setUseWhereEmpresa(false)
		.select("empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cdpessoa, empresa.cnpj")
		.openParentheses()
		.where("ativo = ?",Boolean.TRUE)
		.or()
		.where("empresa = ?", empresa)
		.closeParentheses()
		.orderBy("empresa.nomefantasia")
		.list();
	}
	
	/**
	 * Faz um update no campo "principal".<br>
	 * Tem que ser feito com o JDBC template porque o bulkupdate n�o funciona em
	 * heran�a de tabelas.
	 * 
	 * @param connection
	 * @param cdEmpresa - se null, atualiza todas as empresas
	 * @author Hugo Ferreira
	 * @author Fl�vio Tavares
	 */
	public void updatePrincipal(Integer cdEmpresa, Boolean principal) {
//		if (cdEmpresa == null) throw new SinedException("O par�metro 'cdEmpresa' n�o pode ser nulo");
		if (principal == null) throw new SinedException("O par�metro 'principal' n�o pode ser nulo");
		
		String sql = "UPDATE empresa " +
			  		 "SET principal = " + (principal ? "1" : "0");
		
		if(cdEmpresa != null)
			sql += " WHERE cdpessoa = " + cdEmpresa;
			
		getJdbcTemplate().execute(sql);
	}
	
	/**
	 * Carrega uma empresa com o arquivo de logomarca
	 * 
	 * @param bean
	 * @return
	 * @author Hugo Ferreira
	 */
	public Empresa loadComArquivo(Empresa bean) {
		if (bean == null || bean.getCdpessoa() == null) {
			throw new SinedException("Os par�metros bean ou bean.cdpessoa n�o podem ser nulos.");
		}
		return query()
			.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, logomarca.cdarquivo, logomarcasistema.cdarquivo, empresa.cdpessoa, " +
					"empresa.cnpj, empresa.inscricaomunicipal, empresa.inscricaoestadual, empresa.email, empresa.discriminarservicoqtdevalor, empresa.crt, " +
					"regimetributacao.cdregimetributacao, regimetributacao.descricao, regimetributacao.codigonfse, regimetributacao.codigonfe ")
			.leftOuterJoin("empresa.logomarca logomarca")
			.leftOuterJoin("empresa.logomarcasistema logomarcasistema")
			.leftOuterJoin("empresa.regimetributacao regimetributacao")
			.where("empresa = ?", bean)
			.unique();
	}
	
	/**
	 * Renorna uma lista de empresa contendo apenas o campo Nome
	 * 
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Empresa> findDescricao(String whereIn) {
		return query()
			.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia")
			.whereIn("empresa.cdpessoa", whereIn)
			.list();
	}

	/**
	 * Lista as empresa para o combo do flex.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Empresa> findAtivosForFlex() {
		return query()
				.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia")
				.where("empresa.ativo = ?", Boolean.TRUE)
				.orderBy("empresa.nomefantasia")
				.list();
	}
	
	/**
	 * Atualiza o pr�ximo identificador da venda
	 *
	 * @param empresa
	 * @param identificador
	 * @author Rodrigo Freitas
	 * @since 01/09/2014
	 */
	public void updateProximoIdentificadorVenda(Empresa empresa, Integer identificador) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE EMPRESA SET PROXIMOIDENTIFICADORVENDA = " + identificador + " WHERE CDPESSOA = " + empresa.getCdpessoa());
	}
	
	/**
	 * Atualiza o pr�ximo identificador do or�amento
	 *
	 * @param empresa
	 * @param identificador
	 * @author Rodrigo Freitas
	 * @since 01/09/2014
	 */
	public void updateProximoIdentificadorVendaorcamento(Empresa empresa, Integer identificador) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE EMPRESA SET PROXIMOIDENTIFICADORVENDAORCAMENTO = " + identificador + " WHERE CDPESSOA = " + empresa.getCdpessoa());
	}
	
	/**
	 * Atualiza o pr�ximo identificador do pedido de venda
	 *
	 * @param empresa
	 * @param identificador
	 * @author Rodrigo Freitas
	 * @since 01/09/2014
	 */
	public void updateProximoIdentificadorPedidovenda(Empresa empresa, Integer identificador) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE EMPRESA SET PROXIMOIDENTIFICADORPEDIDOVENDA = " + identificador + " WHERE CDPESSOA = " + empresa.getCdpessoa());
	}
	
	/**
	 * Busca o �ltimo identificador de venda cadastrado na Empresa
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/09/2014
	 */
	public Integer carregaProximoIdentificadorVenda(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		return query()
					.select("empresa.cdpessoa, empresa.proximoidentificadorvenda")
					.where("empresa = ?", empresa)
					.unique().getProximoidentificadorvenda();
	}
	
	/**
	 * Busca o �ltimo identificador de pedido de venda cadastrado na Empresa
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/09/2014
	 */
	public Integer carregaProximoIdentificadorPedidovenda(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		return query()
					.select("empresa.cdpessoa, empresa.proximoidentificadorpedidovenda")
					.where("empresa = ?", empresa)
					.unique().getProximoidentificadorpedidovenda();
	}
	
	/**
	 * Busca o �ltimo identificador de or�amento cadastrado na Empresa
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/09/2014
	 */
	public Integer carregaProximoIdentificadorVendaorcamento(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		return query()
					.select("empresa.cdpessoa, empresa.proximoidentificadorvendaorcamento")
					.where("empresa = ?", empresa)
					.unique().getProximoidentificadorvendaorcamento();
	}

	/**
	 * Carrega o pr�ximo n�mero de NF da empresa
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Integer carregaProxNumNFProduto(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		return query()
					.select("empresa.cdpessoa, empresa.proximonumnfproduto")
					.where("empresa = ?", empresa)
					.unique().getProximonumnfproduto();
	}
	
	public Integer carregaProxNumNFConsumidor(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		return query()
					.select("empresa.cdpessoa, empresa.proximoNumNfce")
					.where("empresa = ?", empresa)
					.unique().getProximoNumNfce();
	}
	
	public Integer carregaProxNumMdfe(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		
		return query()
					.select("empresa.cdpessoa, empresa.proximonummdfe")
					.where("empresa = ?", empresa)
					.unique().getProximonummdfe();
	}
	
	/**
	 * Atualiza o pr�ximo numero de NF da empresa.
	 *
	 * @param empresa
	 * @param proximoNumero
	 * @author Rodrigo Freitas
	 */
	public void updateProximoNumNFProduto(Empresa empresa, Integer proximoNumero) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE EMPRESA SET PROXIMONUMNFPRODUTO = " + proximoNumero + " WHERE CDPESSOA = " + empresa.getCdpessoa());
	}
	
	public void updateProximoNumNFConsumidor(Empresa empresa, Integer proximoNumero) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE EMPRESA SET PROXIMONUMNFCE = " + proximoNumero + " WHERE CDPESSOA = " + empresa.getCdpessoa());
	}
	
	public void updateProximoNumMdfe(Empresa empresa, Integer proximoNumero) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		
		getJdbcTemplate().execute("UPDATE EMPRESA SET PROXIMONUMMDFE = " + proximoNumero + " WHERE CDPESSOA = " + empresa.getCdpessoa());
	}
	
	/**
	 * Carrega o pr�ximo n�mero de NF da empresa
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Integer carregaProxNumNF(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		return query()
					.select("empresa.cdpessoa, empresa.proximonumnf")
					.where("empresa = ?", empresa)
					.unique().getProximonumnf();
	}
	
	public Integer carregaProxIdentificadorContrato(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		return query()
		.select("empresa.cdpessoa, empresa.proximoidentificadorcontrato")
		.where("empresa = ?", empresa)
		.unique().getProximoidentificadorcontrato();
	}
	
//	/**
//	 * Carrega o pr�ximo n�mero Conta a receber
//	 *
//	 * @param empresa
//	 * @return
//	 * @author Tom�s Rabelo
//	 */
//	public Integer carregaProxNumContaReceber(Empresa empresa) {
//		if(empresa == null || empresa.getCdpessoa() == null){
//			throw new SinedException("Empresa n�o pode ser nulo.");
//		}
//		return query()
//		.select("empresa.cdpessoa, empresa.proximonumcontareceber")
//		.where("empresa = ?", empresa)
//		.unique().getProximonumcontareceber();
//	}
	
	
	/**
	 * Carrega o pr�ximo n�mero de Animal
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Romario Filho
	 * @since 12/08/2013
	 */
	public Integer carregaProxNumAnimal(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		return query()
		.select("empresa.cdpessoa, empresa.proximonumanimal")
		.where("empresa = ?", empresa)
		.unique().getProximonumanimal();
	}

	/**
	 * Atualiza o pr�ximo numero de NF da empresa.
	 *
	 * @param empresa
	 * @param proximoNumero
	 * @author Rodrigo Freitas
	 */
	public void updateProximoNumNF(Empresa empresa, Integer proximoNumero) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE EMPRESA SET PROXIMONUMNF = " + proximoNumero + " WHERE CDPESSOA = " + empresa.getCdpessoa());
	}
	
	public void updateProximoIdentificadorContrato(Empresa empresa, Integer proximoNumero) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE EMPRESA SET proximoidentificadorcontrato = " + proximoNumero + " WHERE CDPESSOA = " + empresa.getCdpessoa());
	}

//	/**
//	 * Atualiza o pr�ximo numero da conta a receber da empresa.
//	 *
//	 * @param empresa
//	 * @param proximoNumero
//	 * @author Tom�s Rabelo
//	 */
//	public void updateProximoNumContaReceber(Empresa empresa, Integer proximoNumero) {
//		if(empresa == null || empresa.getCdpessoa() == null){
//			throw new SinedException("Empresa n�o pode ser nulo.");
//		}
//		getJdbcTemplate().execute("UPDATE EMPRESA SET PROXIMONUMCONTARECEBER = " + proximoNumero + " WHERE CDPESSOA = " + empresa.getCdpessoa());
//	}
	
	/**
	 * Atualiza o pr�ximo numero de animal da empresa.
	 *
	 * @param empresa
	 * @param proximoNumero
	 * @author Luiz Romario Filho
	 * @since 12/08/2013
	 */
	public void updateProximoNumAnimal(Empresa empresa, Integer proximoNumero) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE EMPRESA SET PROXIMONUMANIMAL = " + proximoNumero + " WHERE CDPESSOA = " + empresa.getCdpessoa());
	}
	
	/**
	 * M�todo que retorna centro de custo utilizado em veiculo uso no modulo veiculos
	 * 
	 * @param empresa
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Empresa findCentroCustoVeiculoUso(Empresa empresa) {
		return query()
			.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, centrocustoveiculouso.cdcentrocusto")
			.leftOuterJoin("empresa.centrocustoveiculouso centrocustoveiculouso")
			.where("empresa = ?", empresa)
			.unique();
	}

	/**
	 * Busca as empresa das notas passadas por par�metro.
	 *
	 * @param whereIn
	 * @return
	 * @since 30/01/2012
	 * @author Rodrigo Freitas
	 */
	public List<Empresa> findByNotas(String whereIn) {
		return query()
				.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj")
				.join("empresa.listaNota nota")
				.whereIn("nota.cdNota", whereIn)
				.list();
	}
	
	public List<Empresa> findByVendas(String whereIn) {
		return query()
				.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj")
				.join("empresa.listaVenda venda")
				.whereIn("venda.cdvenda", whereIn)
				.list();
	}
	
	public List<Empresa> findByMdfe(String whereIn) {
		return query()
				.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj")
				.join("empresa.listaMdfe mdfe")
				.whereIn("mdfe.cdmdfe", whereIn)
				.list();
	}

	public Empresa loadForRecibo(Empresa empresa) {
		return query()
					.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, municipio.nome, uf.sigla, enderecotipo.cdenderecotipo, " +
							"listaEndereco.cdendereco, listaEndereco.logradouro, listaEndereco.bairro, listaEndereco.cep, listaEndereco.caixapostal, " +
							"listaEndereco.numero, listaEndereco.complemento, logomarca.cdarquivo, empresa.observacaoreciboreceber," +
							"empresa.observacaorecibopagar")
					.leftOuterJoin("empresa.listaEndereco listaEndereco")
					.leftOuterJoin("empresa.logomarca logomarca")
					.leftOuterJoin("listaEndereco.municipio municipio")
					.leftOuterJoin("listaEndereco.enderecotipo enderecotipo")
					.leftOuterJoin("municipio.uf uf")
					.where("empresa = ?", empresa)
					.unique();
	}

	/**
	 * M�todo que retorna a empresa da ordem de compra
	 * 
	 * @param ordemcompra
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Empresa findEmpresaOrdemCompra(Ordemcompra ordemcompra){
		if(ordemcompra == null || ordemcompra.getCdordemcompra() == null)
			throw new SinedException("Par�metros inv�lidos!");
		
		return query()
			.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia")
			.where("empresa.id = (select oc.empresa.id from Ordemcompra oc where oc.id = "+ordemcompra.getCdordemcompra()+")")
			.unique();
	}

	/**
	 * M�todo que carrega par�metros da empresa
	 * 
	 * @param empresa
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Empresa carregaEmpresa(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos!");
		
		return query()
			.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.cpf, empresacodigocnae.principal, codigocnae.cnae, logomarca.cdarquivo ")
			.leftOuterJoin("empresa.listaEmpresacodigocnae empresacodigocnae")
			.leftOuterJoin("empresacodigocnae.codigocnae codigocnae")
			.leftOuterJoin("empresa.logomarca logomarca")
			.where("empresa = ?", empresa)
			.unique();
	}
	
	public Empresa carregaEmpresaComProprietarioRural(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos!");
		
		return querySined()
				.removeUseWhere()
				.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.principal, empresa.tipopessoa, " +
						"empresa.cpf, empresa.cnpj, empresa.propriedadeRural, " +
						"empresa.inscricaoestadual, empresa.email, empresa.inscricaomunicipal, empresa.emailordemcompra, " +
		                "listaEmpresaproprietario.cdempresaproprietario, listaEmpresaproprietario.principal, " +
		                "colaborador.cdpessoa, colaborador.nome, colaborador.cpf, colaborador.cnpj, " +
		                "empresacodigocnae.principal, codigocnae.cnae, logomarca.cdarquivo ")
				.leftOuterJoin("empresa.listaEmpresaproprietario listaEmpresaproprietario")
				.leftOuterJoin("listaEmpresaproprietario.colaborador colaborador")
				.leftOuterJoin("empresa.listaEmpresacodigocnae empresacodigocnae")
				.leftOuterJoin("empresacodigocnae.codigocnae codigocnae")
				.leftOuterJoin("empresa.logomarca logomarca")
				.where("empresa = ?", empresa)
				.unique();
	}
	
	public Empresa loadWithEndereco(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos!");
		
		return querySined()
					.setUseWhereClienteEmpresa(false)
					.setUseWhereEmpresa(false)
					.setUseWhereFornecedorEmpresa(false)
					.setUseWhereProjeto(false)
					.select("empresa.cdpessoa, listaEndereco.cdendereco, municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla, municipio.cdibge, " +
							"listaEndereco.logradouro, listaEndereco.numero, listaEndereco.bairro, listaEndereco.complemento, listaEndereco.cep, enderecotipo.cdenderecotipo, empresa.presencacompradornfe, pais.cdpais ")
					.leftOuterJoin("empresa.listaEndereco listaEndereco")
					.leftOuterJoin("listaEndereco.municipio municipio")
					.leftOuterJoin("listaEndereco.enderecotipo enderecotipo")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("listaEndereco.pais pais")
					.where("empresa = ?", empresa)
					.unique();
	}

	public Empresa loadWithEnderecoTelefone(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos!");
		
		return query()
					.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cpf, empresa.tipopessoa, empresa.cnpj, empresa.email, " +
							"empresa.emailordemcompra, empresa.inscricaoestadual, " +
							"listaEndereco.cdendereco, listaEndereco.cep, listaEndereco.logradouro, " +
							"listaEndereco.numero, listaEndereco.complemento, listaEndereco.bairro, municipio.cdmunicipio, municipio.nome, " +
							"uf.cduf, uf.sigla, municipio.cdibge, telefone.telefone, telefonetipo.cdtelefonetipo, " +
							"logomarca.cdarquivo")
					.leftOuterJoin("empresa.listaEndereco listaEndereco")
					.leftOuterJoin("listaEndereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("empresa.listaTelefone telefone")
					.leftOuterJoin("telefone.telefonetipo telefonetipo")
					.leftOuterJoin("empresa.logomarca logomarca")
					.where("empresa = ?", empresa)
					.unique();
	}
	
	public Empresa loadPrincipalWithEndereco() {
		return query()
					.select("empresa.cdpessoa, listaEndereco.cdendereco, municipio.cdmunicipio, municipio.nome, uf.sigla, uf.cduf, pais.cdpais ")
					.leftOuterJoin("empresa.listaEndereco listaEndereco")
					.leftOuterJoin("listaEndereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("listaEndereco.pais pais")
					.where("empresa.principal = ?", Boolean.TRUE)
					.unique();
	}

	/**
	 * Carrega informa��es para o SPED para o Registro 0000.
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Empresa loadForSpedReg0000(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos!");
		
		return query()
					.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.inscricaoestadual, empresa.inscricaoestadualst, " +
							"empresa.indiceperfilsped, empresa.indiceatividadesped, empresa.indiceatividadespedpiscofins, ufsped.sigla, municipiosped.cdibge")
					.leftOuterJoin("empresa.ufsped ufsped")
					.leftOuterJoin("empresa.municipiosped municipiosped")
					.where("empresa = ?", empresa)
					.unique();
	}

	/**
	 * Carrega informa��es para o SPED para o Registro 0005.
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Empresa loadForSpedReg0005(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos!");
		
		return query()
					.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.email, endereco.cep, endereco.logradouro, " +
							"endereco.numero, endereco.complemento, endereco.bairro, telefone.telefone, telefonetipo.cdtelefonetipo")
					.leftOuterJoin("empresa.listaEndereco endereco")
					.leftOuterJoin("empresa.listaTelefone telefone")
					.leftOuterJoin("telefone.telefonetipo telefonetipo")
					.where("empresa = ?", empresa)
					.unique();
	}

	/**
	 * Carrega informa��es para o SPED para o Registro 0100.
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Empresa loadForSpedReg0100(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos!");
		
		return query()
					.select("empresa.cdpessoa, colaboradorcontabilista.cdpessoa, escritoriocontabilista.cdpessoa, empresa.crccontabilista")
					.leftOuterJoin("empresa.colaboradorcontabilista colaboradorcontabilista")
					.leftOuterJoin("empresa.escritoriocontabilista escritoriocontabilista")
					.where("empresa = ?", empresa)
					.unique();
	}

	/**
	 * M�todo que retorna a empresa do documento
	 * 
	 * @param documento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Empresa findByDocumento(Documento documento) {
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Par�metros inv�lidos!");
		
		return query()
			.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia")
			.where("empresa.id = (select e.id from Documento d join d.empresa e where d = ?)", documento)
			.unique();
	}

	/**
	* M�todo que busca o munic�pio da empresa principal
	*
	* @return
	* @since Aug 30, 2011
	* @author Luiz Fernando F Silva
	*/
	public Empresa buscaMunicipioEmpresaPrincipal() {
		return querySined()
					
					.select("empresa.cdpessoa, listaEndereco.cdendereco, municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla")
					.join("empresa.listaEndereco listaEndereco")
					.join("listaEndereco.municipio municipio")
					.join("municipio.uf uf")
					.where("empresa.principal = ?", Boolean.TRUE)
					.unique();
	}
		
	public Empresa carregarEmpresa(Empresa bean){
		return query()
		.leftOuterJoin("empresa.pessoa pessoa")
		.where("cdpessoa", bean.getCdpessoa()).unique();
	}
	/**M�todo para buscar site e e-mail da empresa para a gera��o do relat�rio de venda.
	 * @author Thiago Augusto
	 * @param empresa
	 * @return
	 */
	public Empresa buscarEmalSiteEmpresa(Empresa empresa){
		return query().select("empresa.cdpessoa, empresa.site, empresa.email")
		.where("empresa = ?", empresa).unique();
	}
	
	/**
	 * M�todo que busca as informa��es para o Registro0100 do SPED PIS/COFINS
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa loadForSpedPiscofinsReg0100(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos!");
		
		return query()
					.select("empresa.cdpessoa, colaboradorcontabilista.cdpessoa, escritoriocontabilista.cdpessoa, empresa.crccontabilista")
					.leftOuterJoin("empresa.colaboradorcontabilista colaboradorcontabilista")
					.leftOuterJoin("empresa.escritoriocontabilista escritoriocontabilista")
					.where("empresa = ?", empresa)
					.unique();
	}

	/**
	 * M�todo que busca as informa��es para o Registro0140 do SPED PIS/COFINS
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa loadForSpedPiscofinsReg0140(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos!");
		
		return query()
					.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.inscricaoestadual, empresa.inscricaomunicipal, " +
							"ufsped.sigla, municipiosped.cdibge ")
					.leftOuterJoin("empresa.municipiosped municipiosped")
					.leftOuterJoin("empresa.ufsped ufsped")
					.where("empresa = ?", empresa)
					.unique();
	}
	
	public List<Empresa> findForSpedPiscofinsReg0140(SpedpiscofinsFiltro filtro) {
		if(filtro == null || filtro.getEmpresa() == null || filtro.getEmpresa().getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos!");
		
		return query()
					.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.inscricaoestadual, empresa.inscricaomunicipal, " +
							"ufsped.sigla, municipiosped.cdibge ")
					.leftOuterJoin("empresa.municipiosped municipiosped")
					.leftOuterJoin("empresa.ufsped ufsped")
					.whereIn("empresa", filtro.getWhereInEmpresaFiliais())
					.list();
	}
	
	/**
	 * M�todo que busca o CNPJ de acordo com a empresa 
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public String cnpjEmpresa(Empresa empresa) {
		if(empresa == null) throw new SinedException("Par�metro inv�lido.");
		
		Empresa e = querySined()
								.setUseWhereEmpresa(false)
								.select("empresa.cnpj")
								.from(Empresa.class)
								.where("empresa = ?", empresa)
								.unique();
						
		if (e != null)
		return e.getCnpj().getValue();
		else 
		return "";
	}
	
	/**
	 * Busca Empresa pelo CNPJ
	 * @param cnpj
	 * @return Empresa
	 */
	public Empresa findByCnpj(Cnpj cnpj) {
		return query()
		.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.inscricaoestadual, empresa.inscricaomunicipal, " +
				"ufsped.sigla, municipiosped.cdibge, contagerencial.cdcontagerencial ")
		.leftOuterJoin("empresa.municipiosped municipiosped")
		.leftOuterJoin("empresa.ufsped ufsped")
		.leftOuterJoin("empresa.contagerencial contagerencial")
		.where("empresa.cnpj = ?", cnpj)
		.unique();
	}


	/**
	 * Carrega a empresa do arquivo passado por par�metro.
	 *
	 * @param arquivonf
	 * @return
	 * @since 15/02/2012
	 * @author Rodrigo Freitas
	 */
	public Empresa loadByArquivonf(Arquivonf arquivonf) {
		if(arquivonf == null || arquivonf.getCdarquivonf() == null){
			throw new SinedException("Arquivo de NF n�o pode ser nulo.");
		}
		return query()
					.select("empresa.cdpessoa, empresa.cnpj, empresa.inscricaomunicipal")
					.join("empresa.listaArquivonf arquivonf")
					.where("arquivonf = ?", arquivonf)
					.unique();
	}


	/**
	 * M�todo que busca as informa��es da empresa para gerar arquivo SIntegra
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa findForSintegra(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cpf, empresa.cnpj, municipio.nome, uf.sigla, responsavel.cdpessoa, responsavel.nome, " +
						"listaTelefone.telefone, empresa.inscricaoestadual, listaEndereco.logradouro, listaEndereco.bairro, listaEndereco.numero, " +
						"listaEndereco.complemento, listaEndereco.cep, ufsped.sigla, empresa.indiceatividadesped ")
				.leftOuterJoin("empresa.listaEndereco listaEndereco")
				.leftOuterJoin("empresa.municipiosped municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("empresa.listaTelefone listaTelefone")
				.leftOuterJoin("empresa.ufsped ufsped")
				.leftOuterJoin("empresa.responsavel responsavel")
				.where("empresa = ? ", empresa)
				.unique();
	}

	/**
	 * M�todo que busca a empresa principal para verificar p�ndencia financeira
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa getEmpresaPrincipalForPendenciafinanceira(Cnpj cnpjlinkcom) {
		return querySined()
			
			.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cpf, empresa.cnpj")			
			.where("empresa.principal = ?", Boolean.TRUE)
			.openParentheses()
				.where("empresa.cnpj is null")
				.or()
				.where("empresa.cnpj <> ?", cnpjlinkcom)
			.closeParentheses()
			.unique();
	}
	
	/**
	 * M�todo que busca a conta gerencial e centro de custo da empresa
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa loadComContagerencialCentrocusto(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cpf, empresa.cnpj, contagerencial.cdcontagerencial, centrocusto.cdcentrocusto, "+
					"contagerencial.nome, vcontagerencial.cdcontagerencial, contagerencial.cdcontagerencial, centrocusto.cdcentrocusto, centrocusto.nome")
			.leftOuterJoin("empresa.contagerencial contagerencial")
			.leftOuterJoin("empresa.centrocusto centrocusto")
			.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			.where("empresa = ?", empresa)
			.unique();
	}
	
	/**
	 * M�todo que verifica se a empresa passada como par�metro tem integra��o com o WMS
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isIntegracaoWms(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		SinedUtil.markAsReader();
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Empresa.class)			
			.where("empresa = ?", empresa)
			.where("empresa.integracaowms = true ")
			.unique() > 0;
			
	}
	
	/**
     * Busca os dados para o cache da tela de pedido de venda offline
     */
	public List<Empresa> findForPVOffline() {
		return query()
		.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia")
		.list();
	}
	
	public List<Empresa> findForAndroid(String whereIn) {
		return query()
		.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.termorecapagem ")
		.whereIn("empresa.cdpessoa", whereIn)
		.list();
	}
	
	/**
	 * M�todo que busca a url de integra��o com o wms
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public String getUrlIntegracaoWms(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		String url = newQueryBuilder(String.class)
			.select("empresa.integracaourlwms")
			.setUseTranslator(false)
			.from(Empresa.class)
			.where("empresa = ?", empresa)
			.unique();
		
		return url != null ? url : "";
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param usuario
	 * @param empresa
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Empresa> findAtivosByUsuario(){
		return querySined()
			
			.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.principal, empresa.nomefantasia")
			.where("empresa.ativo = true")
			.where("empresa in ( select ue.empresa " +
											" from Usuarioempresa ue " +
											" where ue.usuario = ? ) ",SinedUtil.getUsuarioLogado())
			.orderBy("empresa.nomefantasia")
			.list();
	}
	
	/**
	 * M�todo que busca as empresas do usuario
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Empresa> findByUsuario(){
		return querySined()
			
			.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.principal")
			.where("empresa in ( select ue.empresa " +
											" from Usuarioempresa ue " +
											" where ue.usuario = ? ) ",SinedUtil.getUsuarioLogado())
			.orderBy("empresa.nomefantasia")
			.list();
	}

	/**
	 * Retorna o comprovante configuravel da empresa, caso ela o tenha definido
	 * @param tipoComprovante Tipo do comprovante desejado
	 * @param empresa
	 * @return
	 */
	public ComprovanteConfiguravel findComprovaConfiguravelByTipoAndEmpresa(TipoComprovante tipoComprovante, Empresa empresa) {
		
		QueryBuilder<Empresa> query = querySined()
			
			.select("empresa.cdpessoa, c.cdcomprovanteconfiguravel, " +
					"c.descricao, " +
					"c.layout, c.venda, " +
					"c.pedidoVenda, " +
					"c.orcamento, c.ativo, " +
					"c.txt ")
		.entity(empresa);
		
		if(tipoComprovante.equals(TipoComprovante.COLETA))
			query.leftOuterJoin("empresa.comprovanteConfiguravelColeta c");
		else if(tipoComprovante.equals(TipoComprovante.ORCAMENTO))
			query.leftOuterJoin("empresa.comprovanteConfiguravelOrcamento c");
		else if(tipoComprovante.equals(TipoComprovante.PEDIDO_VENDA))
			query.leftOuterJoin("empresa.comprovanteConfiguravelPedidoVenda c");
		else 
			query.leftOuterJoin("empresa.comprovanteConfiguravelVenda c");
		
		query.where("c.ativo = true");
		
		empresa = query.unique();
		
		if(empresa == null){
			return null;
		}else if(tipoComprovante.equals(TipoComprovante.COLETA))
			return empresa.getComprovanteConfiguravelColeta();
		else if(tipoComprovante.equals(TipoComprovante.ORCAMENTO))
			return empresa.getComprovanteConfiguravelOrcamento();
		else if(tipoComprovante.equals(TipoComprovante.PEDIDO_VENDA))
			return empresa.getComprovanteConfiguravelPedidoVenda();
		else return empresa.getComprovanteConfiguravelVenda();
	}

	public boolean possuiComprovanteConfiguravel(TipoComprovante tipoComprovante, Empresa empresa, boolean alternativo) {
		SinedUtil.markAsReader();
		QueryBuilder<Long> query = newQueryBuilder(Long.class)
										.select("count(*)")
										.setUseTranslator(false)
										.from(Empresa.class);
		
		
		if(alternativo){
			String condicao = "";
			if(tipoComprovante.equals(TipoComprovante.COLETA)){
				query.leftOuterJoin("empresa.comprovanteConfiguravelColeta c");
				condicao = " and cSub.coleta = true ";
			}else if(tipoComprovante.equals(TipoComprovante.ORCAMENTO)){
				query.leftOuterJoin("empresa.comprovanteConfiguravelOrcamento c");
				condicao = " and cSub.orcamento = true ";
			}else if(tipoComprovante.equals(TipoComprovante.PEDIDO_VENDA)){
				query.leftOuterJoin("empresa.comprovanteConfiguravelPedidoVenda c");
				condicao = " and cSub.pedidoVenda = true ";
			}else{ 
				query.leftOuterJoin("empresa.comprovanteConfiguravelVenda c");
				condicao = " and cSub.venda = true ";
			}
			
			query.where("exists ( select cSub.cdcomprovanteconfiguravel " +
								" from ComprovanteConfiguravel cSub " +
								" where cSub.ativo = true " +
								" and cSub.cdcomprovanteconfiguravel <> c.cdcomprovanteconfiguravel " +
								condicao +
								")");
		}else {
			if(tipoComprovante.equals(TipoComprovante.COLETA))
				query.join("empresa.comprovanteConfiguravelColeta c");
			else if(tipoComprovante.equals(TipoComprovante.ORCAMENTO))
				query.join("empresa.comprovanteConfiguravelOrcamento c");
			else if(tipoComprovante.equals(TipoComprovante.PEDIDO_VENDA))
				query.join("empresa.comprovanteConfiguravelPedidoVenda c");
			else query.join("empresa.comprovanteConfiguravelVenda c");
			
			query.where("c.ativo = true");
		}
		
		return query.unique() > 0 ;
	}
	
	/**
	 * 
	 * M�todo que busca o pr�ximo identificador da venda.
	 *
	 * @name buscarProximoIdentificadorVenda
	 * @param bean
	 * @return
	 * @return Integer
	 * @author Thiago Augusto
	 * @date 12/07/2012
	 *
	 */
	public Empresa buscarProximoIdentificadorVenda(Empresa bean){
		return querySined()
			
			.select("empresa.cdpessoa, empresa.proximoidentificadorvenda")
			.where("empresa = ? ", bean)
			.unique();
	}
	
	/**
	 * M�todo que retorna o pr�ximo identificador do pedido de venda
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa buscarProximoIdentificadorPedidovenda(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Empresa n�o pode ser nula.");
		
		return querySined()
			
			.select("empresa.cdpessoa, empresa.proximoidentificadorpedidovenda")
			.where("empresa = ? ", empresa)
			.unique();
	}
	
	/**
	 * M�todo que carrega a empresa com os representantes
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa loadComRepresentantes(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Empresa n�o pode ser nula.");
		
		return querySined()
				
				.select("empresa.cdpessoa, empresarepresentacao.cdempresarepresentacao, fornecedor.cdpessoa, fornecedor.nome, " +
						"empresarepresentacao.comissaovenda, empresarepresentacao.repasseunico, fornecedor.email, " +
						"materialgrupo.cdmaterialgrupo, contagerencial.cdcontagerencial, centrocusto.cdcentrocusto," +
						"contagerencialV.cdcontagerencial, centrocustoV.cdcentrocusto ")
				.join("empresa.listaEmpresarepresentacao empresarepresentacao")
				.join("empresarepresentacao.fornecedor fornecedor")
				.leftOuterJoin("empresarepresentacao.materialgrupo materialgrupo")
				.leftOuterJoin("empresarepresentacao.contagerencial contagerencial")
				.leftOuterJoin("empresarepresentacao.centrocusto centrocusto")
				.leftOuterJoin("empresa.contagerencial contagerencialV")
				.leftOuterJoin("empresa.centrocusto centrocustoV")
				.where("empresa = ?", empresa)
				.unique();
	}

	/**
	 * M�todo que carrega a empresa com as contas gerenciais de impostos e frete
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa loadComContagerenciaiscompra(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Empresa n�o pode ser nula.");
		
		return querySined()
				
				.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
						"contagerencialicms.cdcontagerencial, contagerencialicms.nome, " +
						"contagerencialipi.cdcontagerencial, contagerencialipi.nome, " +
						"contagerencialiss.cdcontagerencial, contagerencialiss.nome, " +
						"contagerencialpis.cdcontagerencial, contagerencialpis.nome, " +
						"contagerencialcofins.cdcontagerencial, contagerencialcofins.nome, " +
						"contagerencialfrete.cdcontagerencial, contagerencialfrete.nome, " +
						"contagerencialii.cdcontagerencial, contagerencialii.nome, " +
						"contagerencialicmsst.cdcontagerencial, contagerencialicmsst.nome ")
				.leftOuterJoin("empresa.contagerencialicms contagerencialicms")
				.leftOuterJoin("empresa.contagerencialipi contagerencialipi")
				.leftOuterJoin("empresa.contagerencialiss contagerencialiss")
				.leftOuterJoin("empresa.contagerencialpis contagerencialpis")
				.leftOuterJoin("empresa.contagerencialcofins contagerencialcofins")
				.leftOuterJoin("empresa.contagerencialfrete contagerencialfrete")
				.leftOuterJoin("empresa.contagerencialii contagerencialii")
				.leftOuterJoin("empresa.contagerencialicmsst contagerencialicmsst")
				.where("empresa = ?", empresa)
				.unique();
	}
	
	/**
	 * M�todo que busca as informa��es da empresa para a gera��o do arquivo de remessa
	 *
	 * @param empresa
	 * @return
	 * @author Marden Silva
	 */
	public Empresa loadEmpresaForRemessaConfiguravel(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				
				.setUseWhereEmpresa(false)
				.setUseWhereProjeto(false)
				.setUseWhereClienteEmpresa(false)
				.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cpf, empresa.cnpj, municipio.nome, uf.sigla, " +
						"empresa.inscricaoestadual, listaEndereco.logradouro, listaEndereco.bairro, listaEndereco.numero, " +
						"listaEndereco.complemento, listaEndereco.cep ")
				.leftOuterJoin("empresa.listaEndereco listaEndereco")
				.leftOuterJoin("listaEndereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.where("empresa = ? ", empresa)
				.unique();
	}

	/**
	 * Busca informa��es complementares do contribuinte da empresa
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa buscarInfContribuinte(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Empresa n�o pode ser nula.");
		
		return querySined()
				
				.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.textoinfcontribuinte, empresa.presencacompradornfe")
				.where("empresa = ? ", empresa)
				.unique();
	}
	
	public Empresa loadForArquivoSEFIP(Empresa empresa) {
		return querySined()
				
				.setUseWhereEmpresa(false)
				.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, responsavel.nome, escritoriocontabilista.nome," +
						"escritoriocontabilista.cnpj, colaboradorcontabilista.nome, endereco.cdendereco, endereco.logradouro, endereco.numero," +
						"endereco.complemento, endereco.bairro, municipio.nome, municipio.cdibge, uf.sigla, uf.nome, endereco.cep," +
						"endereco.pontoreferencia, telefone.telefone")
				.leftOuterJoin("empresa.responsavel responsavel")
				.leftOuterJoin("empresa.escritoriocontabilista escritoriocontabilista")
				.leftOuterJoin("empresa.colaboradorcontabilista colaboradorcontabilista")
				.leftOuterJoin("escritoriocontabilista.listaEndereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("escritoriocontabilista.listaTelefone telefone")
				.where("empresa=?", empresa)
				.unique();
	}
	
	public Empresa loadForArquivoDIRF(Empresa empresa) {
		return querySined()
				
				.setUseWhereEmpresa(false)
				.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, " +
						"responsavel.nome, responsavel.cpf, " +
						"colaboradorcontabilista.nome, colaboradorcontabilista.cpf, colaboradorcontabilista.email, " +
						"telefone.telefone, telefonetipo.cdtelefonetipo")
				.leftOuterJoin("empresa.responsavel responsavel")
				.leftOuterJoin("empresa.colaboradorcontabilista colaboradorcontabilista")
				.leftOuterJoin("colaboradorcontabilista.listaTelefone telefone")
				.leftOuterJoin("telefone.telefonetipo telefonetipo")
				.where("empresa = ?", empresa)
				.unique();
	}

	/**
	 * M�todo que atualiza o pr�ximo n�mero de fatura de loca��o da empresa
	 *
	 * @param empresa
	 * @param proximoNumero
	 * @author Luiz Fernando
	 */
	public void updateProximoNumFaturalocacao(Empresa empresa, Integer proximoNumero) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE EMPRESA SET PROXIMONUMFATURALOCACAO = " + proximoNumero + " WHERE CDPESSOA = " + empresa.getCdpessoa());
	}
	

	public void updateProximoNumOportunidade(Empresa empresa, Integer proxNum) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE EMPRESA SET PROXIMONUMOPORTUNIDADE = " + proxNum + " WHERE CDPESSOA = " + empresa.getCdpessoa());
	}
	
	/**
	 * M�todo que carrega a empresa para o documento
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa loadForDocumento(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Empresa n�o pode ser nula.");
		
		return querySined()
				
				.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, colaboradorcontabilista.cdpessoa, escritoriocontabilista.cdpessoa, empresa.crccontabilista")
				.leftOuterJoin("empresa.colaboradorcontabilista colaboradorcontabilista")
				.leftOuterJoin("empresa.escritoriocontabilista escritoriocontabilista")
				.where("empresa = ?", empresa)
				.unique();
	}

	public List<Empresa> findFiliais(Empresa matriz) {
		if(matriz == null || matriz.getCdpessoa() == null)
			throw new SinedException("Empresa n�o pode ser nula.");
		
		return querySined()
				
				.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.inscricaoestadual, empresa.inscricaoestadualst, " +
						"empresa.indiceperfilsped, empresa.indiceatividadesped, empresa.indiceatividadespedpiscofins, ufsped.sigla, municipiosped.cdibge")
				.leftOuterJoin("empresa.ufsped ufsped")
				.leftOuterJoin("empresa.municipiosped municipiosped")
				.where("empresa.cdpessoa <> ?", matriz.getCdpessoa())
				.where("empresa.cnpj is not null")
				.where("SUBSTR(empresa.cnpj, 0, 7) in (SELECT SUBSTR(e.cnpj, 0, 7) FROM empresa e where e.cnpj is not null and e.cdpessoa = " + matriz.getCdpessoa() + " )")
				.list();
	}

	public List<Empresa> findForIntegracaoEmporium() {
		return querySined()
					
					.setUseWhereEmpresa(false)
					.select("empresa.cdpessoa, empresa.integracaopdv")
					.where("empresa.integracaopdv is not null")
					.where("empresa.integracaopdv <> ''")
					.list();
	}

	public Empresa loadForVenda(Empresa empresa) {
		return querySined()
					
					.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, municipio.nome, uf.sigla, enderecotipo.cdenderecotipo, " +
							"listaEndereco.cdendereco, listaEndereco.logradouro, listaEndereco.bairro, listaEndereco.cep, listaEndereco.caixapostal, " +
							"listaEndereco.numero, listaEndereco.complemento, logomarca.cdarquivo, empresa.observacaoreciboreceber," +
							"empresa.observacaorecibopagar, empresa.exibiripivenda, pessoaconfiguracao.cdpessoaconfiguracao, transportador.cdpessoa, empresa.discriminarservicoqtdevalor")
					.leftOuterJoin("empresa.listaEndereco listaEndereco")
					.leftOuterJoin("empresa.logomarca logomarca")
					.leftOuterJoin("listaEndereco.municipio municipio")
					.leftOuterJoin("listaEndereco.enderecotipo enderecotipo")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("empresa.pessoaconfiguracao pessoaconfiguracao")
					.leftOuterJoin("pessoaconfiguracao.transportador transportador")
					.where("empresa = ?", empresa)
					.unique();
	}

	/**
	 * M�todo que busca a empresa pelo cnpj
	 *
	 * @param cnpj
	 * @return
	 * @author Luiz Fernando
	 */
	public Empresa carregaEmpresaByCnpj(Cnpj cnpj) {
		if(cnpj == null)
			throw new SinedException("Cnpj n�o pode ser nulo.");
		
		return querySined()
				
				.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia")
				.where("empresa.cnpj = ?", cnpj)
				.unique();
	}

	/**
	 * M�todo que verifica se n�o deve preencher a data de sa�da da nota

	 * @param empresa
	 * @return
	 */
	public Boolean getNaopreencherdtsaidaNota(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			return false;
		
		SinedUtil.markAsReader();
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Empresa.class)			
			.where("empresa = ?", empresa)
			.where("empresa.naopreencherdtsaida = true ")
			.unique() > 0;
	}
	
	/**
	 * 
	 * @param empresa
	 * @param proximonumeroordemservico
	 * @author Thiago Clemente
	 * 
	 */
	public void updateProximonumeroordemservico(Empresa empresa, Integer proximonumeroordemservico){
		getJdbcTemplate().update("update empresa e set proximonumeroordemservico=? where e.cdpessoa=?", new Object[]{proximonumeroordemservico, empresa.getCdpessoa()});
	}
	
	/**
	 * 
	 * @param empresa
	 * @author Thiago Clemente
	 * 
	 */
	public boolean isRepresentacao(Empresa empresa){
		SinedUtil.markAsReader();
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.join("empresa.listaEmpresarepresentacao listaEmpresarepresentacao")
				.where("empresa=?", empresa)
				.unique().longValue()>0;
	}

	/**
	 * 
	 * @param empresa
	 * @return
	 */
	public Empresa getTransportador(Empresa empresa) {
		return querySined()
			
			.select("empresa.cdpessoa,transportador.cdpessoa, pessoaconfiguracao.cdpessoaconfiguracao, " +
					"transportador.razaosocial,transportador.nome,transportador.tipopessoa," +
					"transportador.inscricaoestadual,transportador.cpf,transportador.cnpj," +
					"municipio.cdmunicipio, uf.cduf, municipio.cdibge, uf.cdibge,endereco.logradouro,endereco.bairro,endereco.cep,endereco.caixapostal,endereco.numero," +
					"endereco.complemento,enderecotipo.cdenderecotipo")
			.leftOuterJoin("empresa.pessoaconfiguracao pessoaconfiguracao")
			.leftOuterJoin("pessoaconfiguracao.transportador transportador")
			.leftOuterJoin("transportador.listaEndereco endereco")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("endereco.enderecotipo enderecotipo")
			.leftOuterJoin("municipio.uf uf")
			.where("empresa = ?",empresa)
			.unique();
	}

	public Empresa getEmpresaByDocumentos(String whereIn) {
		List<Empresa> list = querySined()
					
					.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.formaEnvioBoleto ")
					.where("empresa.cdpessoa in (select e.id from Documento d join d.empresa e where d.cddocumento in (" + whereIn + "))")
					.list();
		
		if(list != null && list.size() == 1){
			return list.get(0);
		} else return null;
	}
	
	/**
	 * 
	 * @param cdpessoa
	 */
	public boolean isEmpresa(Integer cdpessoa){
		SinedUtil.markAsReader();
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.where("empresa.cdpessoa=?", cdpessoa)
				.unique()>0;
	}
	
	/**
	 * M�todo que carrega a empresa com o municipio fiscal
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 * @since 12/11/2013
	 */
	public Empresa loadWithMunicipioFiscal(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Empresa n�o pode ser nula.");
		
		return querySined()
				
				.select("empresa.cdpessoa, ufsped.cduf, municipiosped.cdmunicipio, municipiosped.nome")
				.leftOuterJoin("empresa.municipiosped municipiosped")
				.leftOuterJoin("empresa.ufsped ufsped")
				.where("empresa = ?", empresa)
				.unique();
	}

	/**
	 * M�todo que verifica se existe um cdpessoa na tabela empresa
	 *
	 * @param cdpessoa
	 * @return
	 * @author Luiz Fernando
	 * @since 13/12/2013
	 */
	public Boolean existCdpessoaTabelaEmpresa(Integer cdpessoa) {
		boolean exist = false;
		String sql = "SELECT COUNT(*) AS SOMA FROM EMPRESA P WHERE P.CDPESSOA = " + cdpessoa;

		SinedUtil.markAsReader();
		@SuppressWarnings("unchecked")
		List<Integer> lista = getJdbcTemplate().query(sql,
				new RowMapper() {
					public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
						return rs.getInt("SOMA");
						
					}

				});

		if(lista != null && !lista.isEmpty()){
			exist = lista.get(0) > 0;
		}
		return exist;
	}
	
	public Boolean existCdpessoaTabelaFornecedorCliente(String whereIn) {
		boolean exist = false;
		String sql = "SELECT COUNT(*) AS SOMA FROM CLIENTE C WHERE C.CDPESSOA IN (" + whereIn + ")";
		String sql2 = "SELECT COUNT(*) AS SOMA FROM FORNECEDOR F WHERE F.CDPESSOA IN (" + whereIn + ")";

		SinedUtil.markAsReader();
		@SuppressWarnings("unchecked")
		List<Integer> listaCliente = getJdbcTemplate().query(sql,
				new RowMapper() {
			public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getInt("SOMA");
				
			}
			
		});
		SinedUtil.markAsReader();
		@SuppressWarnings("unchecked")
		List<Integer> listaFornecedor = getJdbcTemplate().query(sql2,
				new RowMapper() {
			public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getInt("SOMA");
				
			}
			
		});
		
		if((listaCliente != null && !listaCliente.isEmpty())||(listaFornecedor != null && !listaFornecedor.isEmpty())){
			exist = listaCliente.get(0) > 0;
		}
		return exist;
	}
	
	/**
	 * M�todo que faz o insert do cdpessoa na tabela empresa
	 *
	 * @param cdpessoa
	 * @author Luiz Fernando
	 * @since 13/12/2013
	 */
	public void insertEmpresaByCdpessoa(Integer cdpessoa) {
		if(cdpessoa != null){
			getJdbcTemplate().update("INSERT INTO EMPRESA (CDPESSOA, NOMEFANTASIA, ATIVO, PRINCIPAL) " +
					"SELECT P.CDPESSOA, P.NOME, TRUE, FALSE FROM PESSOA P WHERE P.CDPESSOA = " + cdpessoa);
		}
		
	}
	
	/**
	 * M�todo que carrega a conta e carteira padr�es vinculados � uma empresa.
	 * 
	 * @param empresa
	 * @return
	 * @author Rafael Salvio
	 * @throws Exception 
	 */
	public Empresa loadWithContaECarteira(Empresa empresa) throws Exception{
		QueryBuilder<Empresa> query =  querySined()
				
				.select("empresa.cdpessoa, conta.cdconta, conta.nome, contacarteira.cdcontacarteira, contacarteira.carteira")
				.leftOuterJoin("empresa.contabancariacontareceber conta")
				.leftOuterJoin("empresa.contacarteiracontareceber contacarteira");
		
		if(empresa == null || empresa.getCdpessoa() == null){
			query.where("empresa.principal = ?", Boolean.TRUE);
		}else{
			query.entity(empresa);
		}
		
		return query.unique();		
	}

	/**
	 * Carrega a empresa com os dados de transfer�ncia
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/09/2014
	 */
	public Empresa loadWithDadosTransferencia(Empresa empresa) {
		return querySined()
					
					.select("empresa.cdpessoa, contagerencialdebitotransferencia.cdcontagerencial, " +
							"contagerencialcreditotransferencia.cdcontagerencial, centrocustotransferencia.cdcentrocusto")
					.leftOuterJoin("empresa.contagerencialdebitotransferencia contagerencialdebitotransferencia")
					.leftOuterJoin("empresa.contagerencialcreditotransferencia contagerencialcreditotransferencia")
					.leftOuterJoin("empresa.centrocustotransferencia centrocustotransferencia")
					.entity(empresa)
					.unique();
	}
	
	/**
	 * Busca o �ltimo n�mero de matr�cula cadastrado na Empresa
	 *
	 * @param empresa
	 * @return
	 * @author Jo�o Vitor
	 * @since 04/12/2014
	 */
	public Integer carregaProximoNumeroMatricula(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		return querySined()
					
					.select("empresa.cdpessoa, empresa.proximonumeromatricula")
					.where("empresa = ?", empresa)
					.unique().getProximonumeromatricula();
	}
	
	/**
	 * Atualiza o pr�ximo numero da matricula
	 *
	 * @param empresa
	 * @param identificador
	 * @author Jo�o Vitor
	 * @since 04/12/2014
	 */
	public void updateProximoNumeroMatricula(Empresa empresa, Integer matricula) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE EMPRESA SET proximonumeromatricula = " + matricula + " WHERE CDPESSOA = " + empresa.getCdpessoa());
	}
	
	public void saveOrUpdateNumeroEstabelecimento(Empresa empresa){
		String sql = "update empresa " 
			+ "set estabelecimento = ? "
			+ "where cdpessoa = ?";
		Object[] fields = new Object[] {
				empresa.getEstabelecimento(),
				empresa.getCdpessoa()};
		getJdbcTemplate().update(sql, fields);
	}

	/**
	* M�todo que retorna as empresa com integra��o com wms
	*
	* @return
	* @since 28/09/2015
	* @author Luiz Fernando
	*/
	public List<Empresa> findEmpresaIntegracaoWMS() {
		return querySined()
			
			.select("empresa.cdpessoa, empresa.nome, empresa.nomefantasia")
			.where("empresa.integracaowms = true")
			.where("coalesce(empresa.integracaourlwms, '') <> ''")
			.list();
	}

	/**
	* M�todo que carrega a empresa com os dados de importa��o de venda
	*
	* @param empresa
	* @return
	* @since 06/10/2015
	* @author Luiz Fernando
	*/
	public Empresa loadForImportacaoFeedApplication(Empresa empresa) {
		return querySined()
				
				.select("empresa.cdpessoa, empresa.nome, empresa.nomefantasia, " +
						"clientefeedapplication.cdpessoa, clientefeedapplication.nome, " +
						"materialfeedapplication.cdmaterial, materialfeedapplication.nome, materialfeedapplication.identificacao, " +
						"materialfeedapplication.valorvenda, materialfeedapplication.valorvendamaximo, " +
						"materialfeedapplication.valorvendaminimo, materialfeedapplication.valorcusto, " +
						"unidademedida.cdunidademedida, unidademedida.nome, " +
						"pedidovendatipofeedapplication.cdpedidovendatipo, " +
						"prazopagamentofeedapplication.cdprazopagamento, " +
						"documentotipofeedapplication.cddocumentotipo ")
				.leftOuterJoin("empresa.clientefeedapplication clientefeedapplication")
				.leftOuterJoin("empresa.materialfeedapplication materialfeedapplication")
				.leftOuterJoin("materialfeedapplication.unidademedida unidademedida")
				.leftOuterJoin("empresa.pedidovendatipofeedapplication pedidovendatipofeedapplication")
				.leftOuterJoin("empresa.prazopagamentofeedapplication prazopagamentofeedapplication")
				.leftOuterJoin("empresa.documentotipofeedapplication documentotipofeedapplication")
				.where("empresa = ?", empresa)
				.unique();
	}

	/**
	* M�todo que faz update dos campos de importa��o de venda
	*
	* @param empresa
	* @since 06/10/2015
	* @author Luiz Fernando
	*/
	public void updateConfiguracaoImportacao(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Empresa n�o pode ser nula");
		
		String sql = " UPDATE empresa e SET " +
		(empresa.getClientefeedapplication() != null ? (" cdclientefeedapplication = " + empresa.getClientefeedapplication().getCdpessoa() + ", ") : "" ) +
		(empresa.getMaterialfeedapplication() != null ? (" cdmaterialfeedapplication = " + empresa.getMaterialfeedapplication().getCdmaterial() + ", ") : "" )  +
		(empresa.getPedidovendatipofeedapplication() != null ? (" cdpedidovendatipofeedapplication = " + empresa.getPedidovendatipofeedapplication().getCdpedidovendatipo() + ", ") : "" )  +
		(empresa.getPrazopagamentofeedapplication() != null ? (" cdprazopagamentofeedapplication = " + empresa.getPrazopagamentofeedapplication().getCdprazopagamento() + ", ") : "" )  +
		(empresa.getDocumentotipofeedapplication() != null ? (" cddocumentotipofeedapplication = " + empresa.getDocumentotipofeedapplication().getCddocumentotipo() + " ") : "" )  +
					 " WHERE e.cdpessoa = " + empresa.getCdpessoa();
		
		getJdbcTemplate().execute(sql);
	}
	
	/**
	 * M�todo que busca as empresas a serem enviadas para o W3Producao.
	 * @param whereIn
	 * @return
	 */
	public List<Empresa> findForW3Producao(String whereIn){
		return querySined()
						
					.select("empresa.cdpessoa, empresa.nome, empresa.nomefantasia")
					.whereIn("empresa.cdpessoa", whereIn)
					.list();
	}

	/**
	* M�todo que busca as empresas com informa��es de transfer�ncia (conta gerencial / centro de custo)
	*
	* @return
	* @since 28/03/2016
	* @author Luiz Fernando
	*/
	public List<Empresa> findEmpresaWithContagerecialCentrocustoTransferencia() {
		return querySined()
				
				.select("empresa.cdpessoa, " +
						"centrocustotransferencia.cdcentrocusto, centrocustotransferencia.nome, " +
						"contagerencialcreditotransferencia.cdcontagerencial, contagerencialcreditotransferencia.nome, " +
						"contagerencialdebitotransferencia.cdcontagerencial, contagerencialdebitotransferencia.nome ")
				.leftOuterJoin("empresa.centrocustotransferencia centrocustotransferencia")
				.leftOuterJoin("empresa.contagerencialcreditotransferencia contagerencialcreditotransferencia")
				.leftOuterJoin("empresa.contagerencialdebitotransferencia contagerencialdebitotransferencia")
				.openParentheses()
					.where("centrocustotransferencia is not null")
					.or()
					.where("contagerencialcreditotransferencia is not null")
					.or()
					.where("contagerencialdebitotransferencia is not null")
				.closeParentheses()
				.list();
	}
	
	/**
	* M�todo que carrega a empresa principal com informa��es de transfer�ncia (conta gerencial / centro de custo)
	*
	* @return
	* @since 28/03/2016
	* @author Luiz Fernando
	*/
	public Empresa loadPrincipalWithContagerecialCentrocustoTransferencia() {
		return querySined()
					
					.select("empresa.cdpessoa, " +
							"centrocustotransferencia.cdcentrocusto, centrocustotransferencia.nome, " +
							"contagerencialcreditotransferencia.cdcontagerencial, contagerencialcreditotransferencia.nome, " +
							"contagerencialdebitotransferencia.cdcontagerencial, contagerencialdebitotransferencia.nome ")
					.leftOuterJoin("empresa.centrocustotransferencia centrocustotransferencia")
					.leftOuterJoin("empresa.contagerencialcreditotransferencia contagerencialcreditotransferencia")
					.leftOuterJoin("empresa.contagerencialdebitotransferencia contagerencialdebitotransferencia")
					.where("empresa.principal = ?", Boolean.TRUE)
					.unique();
	}
	
	public Empresa loadPrincipalWithContagerecialDevolucao() {
		return querySined()
					
					.select("empresa.cdpessoa, " +
							"contagerencialdevolucaocredito.cdcontagerencial, contagerencialdevolucaocredito.nome, " +
							"contagerencialdevolucaodebito.cdcontagerencial, contagerencialdevolucaodebito.nome ")
					.leftOuterJoin("empresa.contagerencialdevolucaocredito contagerencialdevolucaocredito")
					.leftOuterJoin("empresa.contagerencialdevolucaodebito contagerencialdevolucaodebito")
					.where("empresa.principal = ?", Boolean.TRUE)
					.unique();
	}

	/**
	 * Busca a empresa pelo CNPJ para o Webservice SOAP
	 *
	 * @param cnpj
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/04/2016
	 */
	public Empresa findByCnpjWSSOAP(Cnpj cnpj) {
		return querySined()
				
				.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj")
				.where("empresa.cnpj = ?", cnpj)
				.unique();
	}
	
	/**
	 * Busca a empresa pelo Nome para ImportarcolaboradoresFPWProcess
	 *
	 * @param nome
	 * @return
	 * @author Diogo souza
	 * @since 18/05/2020
	 */
	public Empresa findByEmpresaNome(String nome) {
		//String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		
		if(nome == null || "".equals(nome))
			throw new SinedException("O nome da empresa n�o pode ser nula");
		
		return query()
				.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj")
					.openParentheses()
							.whereLikeIgnoreAll("empresa.nome", nome)
						.or()
							.whereLikeIgnoreAll("empresa.nomefantasia", nome)
						.or()
							.whereLikeIgnoreAll("empresa.razaosocial", nome)
					.closeParentheses()
				.unique();
	}
	
	
	/**
	* M�todo que verifica se a empresa ir� exibir ipi no processo de venda
	*
	* @param empresa
	* @return
	* @since 18/06/2016
	* @author Luiz Fernando
	*/
	public boolean exibirIpiVenda(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		SinedUtil.markAsReader();
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Empresa.class)			
			.where("empresa = ?", empresa)
			.where("empresa.exibiripivenda = true ")
			.unique() > 0; 
	}
	
	/**
	* M�todo que verifica se a empresa ir� exibir icms no processo de venda
	*
	* @param empresa
	* @return
	* @since 30/06/2020
	* @author Mairon Cezar
	*/
	public boolean exibirIcmsVenda(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return new QueryBuilder<Long>(getHibernateTemplate())
			.select("count(*)")
			.setUseTranslator(false)
			.from(Empresa.class)			
			.where("empresa = ?", empresa)
			.where("empresa.exibirIcmsVenda = true ")
			.unique() > 0; 
	}
	
	/**
	* M�todo que verifica se a empresa ir� exibir difal no processo de venda
	*
	* @param empresa
	* @return
	* @since 30/06/2020
	* @author Mairon Cezar
	*/
	public boolean exibirDifalVenda(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return new QueryBuilder<Long>(getHibernateTemplate())
			.select("count(*)")
			.setUseTranslator(false)
			.from(Empresa.class)			
			.where("empresa = ?", empresa)
			.where("empresa.exibirDifalVenda = true ")
			.unique() > 0; 
	}

	/**
	* M�todo que verifica se existe alguma empresa com exibir ipi marcado
	*
	* @return
	* @since 23/06/2016
	* @author Luiz Fernando
	*/
	public boolean existeEmpresaExibirIpi() {
		SinedUtil.markAsReader();
		return newQueryBuilder(Long.class)
		.select("count(*)")
		.setUseTranslator(false)
		.from(Empresa.class)			
		.where("empresa.exibiripivenda = true ")
		.unique() > 0; 
	}
	
	/**
	* M�todo que verifica se existe alguma empresa com exibir icms marcado
	*
	* @return
	* @since 30/06/2020
	* @author Mairon Cezar
	*/
	public boolean existeEmpresaExibirIcms() {
		return new QueryBuilder<Long>(getHibernateTemplate())
		.select("count(*)")
		.setUseTranslator(false)
		.from(Empresa.class)			
		.where("empresa.exibirIcmsVenda = true ")
		.unique() > 0; 
	}
	
	/**
	* M�todo que verifica se existe alguma empresa com exibir difal marcado
	*
	* @return
	* @since 30/06/2020
	* @author Mairon Cezar
	*/
	public boolean existeEmpresaExibirDifal() {
		return new QueryBuilder<Long>(getHibernateTemplate())
		.select("count(*)")
		.setUseTranslator(false)
		.from(Empresa.class)			
		.where("empresa.exibirDifalVenda = true ")
		.unique() > 0; 
	}
	
	public boolean existeEmpresa(Empresa empresa) {
		SinedUtil.markAsReader();
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.setUseTranslator(false)
				.from(Empresa.class)			
				.where("empresa.cdpessoa = ?", empresa.getCdpessoa())
				.unique() > 0; 
	}
	
	
	public void insertEmpresaCliente(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		}
		getJdbcTemplate().update("insert into Empresa (cdpessoa, ativo, principal) values (?, true, false)", new Object[]{empresa.getCdpessoa()});
	}

	
	/**
	* M�todo que carrega a empresa com informa��es para a nota
	*
	* @param empresa
	* @return
	* @since 06/10/2016
	* @author Luiz Fernando
	*/
	public Empresa loadForInfNota(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				
				.select(
						"empresa.cdpessoa, empresa.serienfe, empresa.nome, " +
						"listaEmpresacodigocnae.cdempresacodigocnae, listaEmpresacodigocnae.principal, " +
						"codigocnae.cdcodigocnae, codigocnae.cnae, codigocnae.descricaocnae " )
				.leftOuterJoin("empresa.listaEmpresacodigocnae listaEmpresacodigocnae")
				.leftOuterJoin("listaEmpresacodigocnae.codigocnae codigocnae")
				.where("empresa = ?", empresa)
				.unique();
	}

	/**
	* M�todo que carrega a empresa com as informa��es para o template
	*
	* @param empresa
	* @return
	* @since 03/10/2016
	* @author Luiz Fernando
	*/
	public Empresa loadForTemplate(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
			
			.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, " +
					"empresa.inscricaomunicipal, empresa.inscricaoestadual, empresa.email, empresa.site, " +
					"listaEndereco.cdendereco, listaEndereco.logradouro, listaEndereco.bairro, listaEndereco.cep, " +
					"listaEndereco.numero, listaEndereco.complemento, municipio.nome, uf.sigla, enderecotipo.cdenderecotipo, " +
					"listaTelefone.cdtelefone, listaTelefone.telefone, telefonetipo.cdtelefonetipo, telefonetipo.nome ")
			.leftOuterJoin("empresa.listaEndereco listaEndereco")
			.leftOuterJoin("listaEndereco.municipio municipio")
			.leftOuterJoin("listaEndereco.enderecotipo enderecotipo")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("empresa.listaTelefone listaTelefone")
			.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
			.where("empresa = ?", empresa)
			.unique();
	}
	
	/**
	 * Busca informa��es da empresa para o template de informa��es do contribuinte
	 *
	 * @param empresa
	 * @return
	 * @since 25/11/2016
	 * @author Mairon Cezar
	 */
	public Empresa loadForInfoContribuinte(Empresa empresa) {
		return querySined()
				
				.select("empresa.nome, empresa.razaosocial, empresa.cpf, empresa.cnpj, empresa.email, empresa.inscricaoestadual, "+
				 "empresa.inscricaomunicipal, empresa.textoinfcontribuinte, empresa.site, telefone.telefone, endereco.numero, "+
				"endereco.enderecotipo, endereco.cep, endereco.numero, endereco.logradouro, endereco.complemento, "+
				"endereco.bairro, municipio.nome, endereco.caixapostal, uf.sigla")
	.leftOuterJoin("empresa.listaTelefone telefone")
	.leftOuterJoin("empresa.listaEndereco endereco")
	.leftOuterJoin("endereco.municipio municipio")
	.leftOuterJoin("municipio.uf uf")
	.where("empresa = ?", empresa)
	.unique();
	}
	
	public List<Empresa> findByListaCnpj(List<Cnpj> listaCnpj){
		QueryBuilderSined<Empresa> query = querySined().setUseWhereEmpresa(false);
		query.select("empresa.cdpessoa, empresa.cnpj");
		
		query.openParentheses();
		for (Cnpj cnpj: listaCnpj){
			query.where("empresa.cnpj=?", cnpj);
			query.or();
		}
		query.closeParentheses();
		
		return query.list();
	}
	
	public List<Empresa> findAllContagerencialVenda() {
		return querySined()
				
				.select("empresa.cdpessoa, contagerencial.cdcontagerencial, contagerencial.nome")
				.join("empresa.contagerencial contagerencial")				
				.list();
	}

	/**
	* M�todo que carrega a empresa para a integra��o SAGE
	*
	* @param empresa
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public Empresa loadForSageFiscal(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				
			.setUseWhereEmpresa(false)
			.setUseWhereProjeto(false)
			.setUseWhereClienteEmpresa(false)
			.select("empresa.cdpessoa, empresa.nome, empresa.nomefantasia, empresa.indiceatividadespedpiscofins, empresa.crt, empresa.integracaocontabil ")
			.where("empresa = ?", empresa)
			.unique();
	}
	
	public List<Empresa> findByCampo(String campo, Object valorCampo){
		return querySined()
		
		.select("empresa.cdpessoa, empresa.nome")
		.where("empresa."+campo+" = ?", valorCampo)				
		.list();		
	}

	public Empresa loadForBaixa(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
			
			.setUseWhereEmpresa(false)
			.setUseWhereProjeto(false)
			.setUseWhereClienteEmpresa(false)
			.select("empresa.cdpessoa, " + 
					"contabaixadinheiro.cdconta, contabaixadinheiro.nome, contatipoDinheiro.cdcontatipo, " +
					"contabaixacheque.cdconta, contabaixacheque.nome, contatipoCheque.cdcontatipo, " +
					"contabaixacreditoconta.cdconta, contabaixacreditoconta.nome, contatipoCreditoconta.cdcontatipo ")
			.leftOuterJoin("empresa.contabaixadinheiro contabaixadinheiro")
			.leftOuterJoin("contabaixadinheiro.contatipo contatipoDinheiro")
			.leftOuterJoin("empresa.contabaixacheque contabaixacheque")
			.leftOuterJoin("contabaixacheque.contatipo contatipoCheque")
			.leftOuterJoin("empresa.contabaixacreditoconta contabaixacreditoconta")
			.leftOuterJoin("contabaixacreditoconta.contatipo contatipoCreditoconta")
			.where("empresa = ?", empresa)
			.unique();
	}
	public Empresa loadWithModeloorcamentoRTF(Empresa empresa){
		return querySined()
				.select("empresa.cdpessoa, listaEmpresamodelocomprovanteorcamento.cdempresamodelocomprovanteorcamento, listaEmpresamodelocomprovanteorcamento.nome," +
						"listaEmpresamodelocomprovanteorcamento.tipoComprovante, "+
						"arquivo.cdarquivo, arquivo.nome, arquivo.tipoconteudo")
				.leftOuterJoin("empresa.listaEmpresamodelocomprovanteorcamento listaEmpresamodelocomprovanteorcamento")
				.leftOuterJoin("listaEmpresamodelocomprovanteorcamento.arquivo arquivo")
				.where("empresa = ?", empresa)
				.orderBy("upper(listaEmpresamodelocomprovanteorcamento.nome)")
				.unique();
	}
	

	
	public Empresa loadWithLocalproducao(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
			
			.setUseWhereEmpresa(false)
			.setUseWhereProjeto(false)
			.setUseWhereClienteEmpresa(false)
			.select("empresa.cdpessoa, " + 
					"localarmazenagemmateriaprima.cdlocalarmazenagem, localarmazenagemmateriaprima.nome, " +
					"localarmazenagemprodutofinal.cdlocalarmazenagem, localarmazenagemprodutofinal.nome  ")
			.leftOuterJoin("empresa.localarmazenagemmateriaprima localarmazenagemmateriaprima")
			.leftOuterJoin("empresa.localarmazenagemprodutofinal localarmazenagemprodutofinal")
			.where("empresa = ?", empresa)
			.unique();
	}

	@Override
	public ListagemResult<Empresa> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Empresa> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("empresa.cdpessoa");
		
		return new ListagemResult<Empresa>(query, filtro);
	}
	public List<Empresa> findEmpresasConfiguradasNfe(Empresa empresa) {
		return querySined()
				
				.select("empresa.cdpessoa, empresa.nomefantasia")
				.openParentheses()
				.where("exists(select e.cdpessoa " +
						"from Configuracaonfe cnfe " +
						"join cnfe.empresa e " +
						"where e.cdpessoa = empresa.cdpessoa " +
						"and coalesce(cnfe.ativo, false) = true " +
						"and cnfe.tipoconfiguracaonfe = ?)", Tipoconfiguracaonfe.DDFE)
				.or()
				.where("empresa =?" ,empresa)
				.closeParentheses()
				.list();
				
				
	}
	public Empresa loadWithProprietario(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Empresa n�o pode ser nula");
		
		return querySined()
				
				.removeUseWhere()
				.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.principal, empresa.tipopessoa, " +
						"empresa.cpf, empresa.cnpj, empresa.propriedadeRural, " +
						"empresa.inscricaoestadual, empresa.email, empresa.inscricaomunicipal, empresa.emailordemcompra, " +
		                "listaEmpresaproprietario.cdempresaproprietario, listaEmpresaproprietario.principal, " +
		                "colaborador.cdpessoa, colaborador.nome, colaborador.cpf, colaborador.cnpj ")
				.leftOuterJoin("empresa.listaEmpresaproprietario listaEmpresaproprietario")
				.leftOuterJoin("listaEmpresaproprietario.colaborador colaborador")
				.where("empresa = ?", empresa)
				.unique();
	}
	
	public List<Empresa> findForCriarContabilEmpresa(Material material) {
		return querySined()
				
				.select("empresa.cdpessoa, empresa.nome")
				.where("coalesce(empresa.ativo, false) = true")
				.where("empresa.cdpessoa not in (select e.cdpessoa from MaterialCustoEmpresa mce join mce.material m join mce.empresa e where m.cdmaterial = ?)", material.getCdmaterial())
				.list();
	}
	
	public List<Empresa> findFazendas() {
		return querySined()
				
				.select("empresa.cdpessoa, empresa.nomefantasia")
				.where("empresa.propriedadeRural = true")
				.orderBy("empresa.nome")
				.list();
	}
	
	public Integer carregaProximoIdentificadorCarregamento(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		return query()
					.select("empresa.cdpessoa, empresa.proximoidentificadorcarregamento")
					.where("empresa = ?", empresa)
					.unique().getProximoidentificadorcarregamento();
	}
	
	public void updateProximoIdentificadorCarregamento(Empresa empresa, Integer identificador) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE EMPRESA SET PROXIMOIDENTIFICADORCARREGAMENTO = " + identificador + " WHERE CDPESSOA = " + empresa.getCdpessoa());
	}
	
	public Empresa loadEmpresaForEntradaeSaida(Integer cdEmpresa){
		return query()				
				.select("empresa.cdpessoa, empresa.nomefantasia")
				.where("empresa.cdpessoa = ?", cdEmpresa)
				.unique();						
	}
	
	public List<Empresa> findWithConfiguracaoCorreios(){
		return querySined()
				.select("empresa.nome, empresa.cdpessoa, empresa.usuarioCorreios, empresa.senhaCorreios")
				.where("coalesce(empresa.usuarioCorreios, '') <> ''")
				.where("coalesce(empresa.senhaCorreios, '') <> ''")
				.list();
	}
}