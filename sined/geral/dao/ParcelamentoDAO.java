package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Agendamentoservicodocumento;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregadocumentohistorico;
import br.com.linkcom.sined.geral.bean.Entregapagamento;
import br.com.linkcom.sined.geral.bean.Fornecimento;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Parcela;
import br.com.linkcom.sined.geral.bean.Parcelamento;
import br.com.linkcom.sined.geral.bean.Veiculodespesa;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaodespesaviagem;
import br.com.linkcom.sined.geral.service.DespesaviagemService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentoorigemService;
import br.com.linkcom.sined.geral.service.EntradafiscalService;
import br.com.linkcom.sined.geral.service.EntregaService;
import br.com.linkcom.sined.geral.service.EntregadocumentoService;
import br.com.linkcom.sined.geral.service.EntregadocumentohistoricoService;
import br.com.linkcom.sined.geral.service.FornecimentoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.VeiculodespesaService;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ParcelamentoDAO extends GenericDAO<Parcelamento>{
	
	private DocumentoService documentoService;
	private RateioService rateioService;
	private DocumentoorigemService documentoorigemService;
	private FornecimentoService fornecimentoService;
	private DespesaviagemService despesaviagemService;
	private EntregaService entregaService;
	private ArquivoDAO arquivoDAO;
	private EntradafiscalService entradafiscalService;
	private EntregadocumentohistoricoService entregadocumentohistoricoService;
	private EntregadocumentoService entregadocumentoService;
	private VeiculodespesaService veiculodespesaService;
	
	public void setEntregaService(EntregaService entregaService) {
		this.entregaService = entregaService;
	}
	public void setDespesaviagemService(
			DespesaviagemService despesaviagemService) {
		this.despesaviagemService = despesaviagemService;
	}
	public void setFornecimentoService(FornecimentoService fornecimentoService) {
		this.fornecimentoService = fornecimentoService;
	}
	public void setDocumentoorigemService(
			DocumentoorigemService documentoorigemService) {
		this.documentoorigemService = documentoorigemService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setEntradafiscalService(
			EntradafiscalService entradafiscalService) {
		this.entradafiscalService = entradafiscalService;
	}
	public void setEntregadocumentohistoricoService(
			EntregadocumentohistoricoService entregadocumentohistoricoService) {
		this.entregadocumentohistoricoService = entregadocumentohistoricoService;
	}
	public void setEntregadocumentoService(
			EntregadocumentoService entregadocumentoService) {
		this.entregadocumentoService = entregadocumentoService;
	}
	public void setVeiculodespesaService(
			VeiculodespesaService veiculodespesaService) {
		this.veiculodespesaService = veiculodespesaService;
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		Object entrega = getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				Parcelamento parcelamento = (Parcelamento) save.getEntity();
				Set<Parcela> listaParcela = parcelamento.getListaParcela();
				
				Entrega entrega = null;
				Entregadocumento entregadocumento = null;
				StringBuilder obsEntregadocumento = new StringBuilder();
				if(listaParcela != null){
					for (Parcela parcela : listaParcela) {
						Documento bean = parcela.getDocumento();
						rateioService.limpaReferenciaRateio(bean.getRateio());
						
						Coleta coleta = bean.getColeta();
						entrega = null;
						List<Entrega> listaEntregas = new ArrayList<Entrega>();
						if (bean.getEntrega() != null){
							entrega = bean.getEntrega();
						}
						if(bean.getWhereInEntrega() != null){
							listaEntregas = entregaService.listEntregaForFaturar(bean.getWhereInEntrega());
						}
						String whereInEntregadocumento = bean.getWhereInEntregadocumento();
						if(Util.strings.isNotEmpty(whereInEntregadocumento) && whereInEntregadocumento.split(",").length == 1){
							entregadocumento = new Entregadocumento(Integer.parseInt(whereInEntregadocumento));
							entregadocumento = entregadocumentoService.load(entregadocumento);
						}
						
						Agendamentoservicodocumento agendamentoservicodocumento = null;
						if(bean.getAgendamentoservicodocumento() != null){
							agendamentoservicodocumento = bean.getAgendamentoservicodocumento();
						}
						
						Boolean adiantamentoDespesa = null;
						String whereInDespesaviagem = null;
						if(bean.getWhereInDespesaviagem() != null && !bean.getWhereInDespesaviagem().equals("")) {
							whereInDespesaviagem = bean.getWhereInDespesaviagem();
							adiantamentoDespesa = bean.getAdiatamentodespesa();
						}
						
						String whereInDespesaviagemReembolso = null;
						if(bean.getWhereInDespesaviagemReembolso() != null && !bean.getWhereInDespesaviagemReembolso().equals("")) {
							whereInDespesaviagemReembolso = bean.getWhereInDespesaviagemReembolso();
						}
						
						String[] ids = null;
						if (bean.getIdsFornecimento() != null && !bean.getIdsFornecimento().equals("")) {
							ids = bean.getIdsFornecimento().split(",");
						}
						
						String whereInOrdemcompra = bean.getWhereInOrdemcompra();
						
						if (bean.getImagem() != null){
							arquivoDAO.saveFile(bean, "imagem");
						}
						
						if (Boolean.TRUE.equals(bean.getFromDespesaVeiculo()) && StringUtils.isNotBlank(bean.getIdsVeiculosDespesa())) {
							StringBuilder sb = new StringBuilder();
							sb.append("Origem: Despesa de ve�culo  ");
							if(bean.getIdsVeiculosDespesa() != null && bean.getIdsVeiculosDespesa().split(",").length > 0){
								for(String idVeiculoDespesa :  bean.getIdsVeiculosDespesa().split(",")){
									sb.append("<a href=\"javascript:visualizarVeiculodespesa("+ idVeiculoDespesa +");\">"+ idVeiculoDespesa +"</a>, ");
								}
								sb.replace(sb.length()-2, sb.length(), "");
								bean.setObservacaoHistorico(sb.toString());
							}
						}
						
						if(StringUtils.isNotBlank(bean.getObservacaoHistorico())){
							bean.getListaDocumentohistorico().clear();
							documentoService.atualizaHistoricoDocumento(bean, Boolean.TRUE);
						}
						
						documentoService.saveOrUpdate(bean);
						
						Documentoorigem documentoorigem = null;
						
						if(coleta != null){
							documentoorigem = new Documentoorigem();
							documentoorigem.setDocumento(bean);
							documentoorigem.setColeta(coleta);
							documentoorigemService.saveOrUpdate(documentoorigem);
						}
						
						if (entrega != null) {
							documentoorigem = new Documentoorigem();
							documentoorigem.setDocumento(bean);
							documentoorigem.setEntrega(entrega);
							documentoorigemService.saveOrUpdate(documentoorigem);
						}
						
						if(listaEntregas != null && !listaEntregas.isEmpty()){
							for(Entrega e: listaEntregas){
								if(!e.equals(entrega)){//Pra n�o duplicar registro de Documentoorigem
									documentoorigem = new Documentoorigem();
									documentoorigem.setDocumento(bean);
									documentoorigem.setEntrega(e);
									documentoorigemService.saveOrUpdate(documentoorigem);
									
									entregaService.callProcedureAtualizaEntrega(e);
								}
							}
							entrega = new Entrega();
							entrega.setWhereInEntregas(bean.getWhereIn());
						}						
						
						if(agendamentoservicodocumento != null){
							documentoorigem = new Documentoorigem();
							documentoorigem.setDocumento(bean);
							documentoorigem.setAgendamentoservicodocumento(agendamentoservicodocumento);
							documentoorigemService.saveOrUpdate(documentoorigem);
						}
						
						if(whereInDespesaviagem != null){
							String[] idsDespesas = whereInDespesaviagem.split(",");
							for (int i = 0; i < idsDespesas.length; i++) {
								documentoorigem = new Documentoorigem();
								documentoorigem.setDocumento(bean);
								if (adiantamentoDespesa) {
									documentoorigem.setDespesaviagemadiantamento(new Despesaviagem(Integer.parseInt(idsDespesas[i])));
								} else {
									documentoorigem.setDespesaviagemacerto(new Despesaviagem(Integer.parseInt(idsDespesas[i])));
									despesaviagemService.updateSituacao(idsDespesas[i], Situacaodespesaviagem.BAIXADA);
								}
								documentoorigemService.saveOrUpdate(documentoorigem);
							}
						}
						
						String[] idsDespesa = null;
						if (bean.getIdsVeiculosDespesa() != null && !bean.getIdsVeiculosDespesa().equals("")) {
							idsDespesa = bean.getIdsVeiculosDespesa().split(",");
						}

						if (idsDespesa != null && idsDespesa.length > 0) {
							StringBuilder whereIn = new StringBuilder();
							for (int i = 0; i < idsDespesa.length; i++) {
								documentoorigem = new Documentoorigem();
								documentoorigem.setDocumento(bean);
								documentoorigem.setVeiculodespesa(new Veiculodespesa(Integer.parseInt(idsDespesa[i])));
								documentoorigemService.saveOrUpdate(documentoorigem);
								whereIn.append(idsDespesa[i]+",");
							}
							veiculodespesaService.updateFaturado(whereIn.delete(whereIn.length()-1, whereIn.length()).toString());
						}
						
						if(StringUtils.isNotBlank(whereInOrdemcompra)){
							for(String idOrdemcompra : whereInOrdemcompra.split(",")){
								documentoorigem = new Documentoorigem();
								documentoorigem.setDocumento(bean);
								documentoorigem.setOrdemcompra(new Ordemcompra(Integer.parseInt(idOrdemcompra)));
								documentoorigemService.saveOrUpdate(documentoorigem);
							}
						}
						
						if(whereInDespesaviagemReembolso != null){
							despesaviagemService.updateReembolso(whereInDespesaviagemReembolso, true);
							
							String[] idsDespesas = whereInDespesaviagemReembolso.split(",");
							for (int i = 0; i < idsDespesas.length; i++) {
								documentoorigem = new Documentoorigem();
								documentoorigem.setDocumento(bean);
								documentoorigem.setDespesaviagemreembolso(new Despesaviagem(Integer.parseInt(idsDespesas[i])));
								documentoorigemService.saveOrUpdate(documentoorigem);
							}
						}
						
						if (ids != null && ids.length > 0) {
							for (int i = 0; i < ids.length; i++) {
								documentoorigem = new Documentoorigem();
								documentoorigem.setDocumento(bean);
								documentoorigem.setFornecimento(new Fornecimento(Integer.parseInt(ids[i])));
								documentoorigemService.saveOrUpdate(documentoorigem);
								
								fornecimentoService.updateBaixado(documentoorigem.getFornecimento());
							}
						}
						
						if(entregadocumento != null){
							documentoorigem = new Documentoorigem();
							documentoorigem.setEntregadocumento(entregadocumento);
							documentoorigem.setDocumento(bean);
							documentoorigemService.saveOrUpdate(documentoorigem);
								
							if(!obsEntregadocumento.toString().equals("")) obsEntregadocumento.append(", ");
							obsEntregadocumento.append("<a href=\"javascript:visualizaContapagar(" + bean.getCddocumento() +");\">"+ bean.getCddocumento() +"</a> ");
						}
					}
				}
				save.saveOrUpdateManaged("listaParcela");
				
				if(entregadocumento != null){
					List<Entregadocumento> listaEntregadocumento = entradafiscalService.findForFaturar(entregadocumento.getCdentregadocumento().toString());
					if(SinedUtil.isListNotEmpty(listaEntregadocumento)){
						for(Entregapagamento ep: listaEntregadocumento.get(0).getListadocumento()){
							Documento doc = ep.getDocumentoantecipacao();
							if(doc != null && doc.getCddocumento() != null){
								if(!obsEntregadocumento.toString().equals("")) obsEntregadocumento.append(", ");
								obsEntregadocumento.append("<a href=\"javascript:visualizaContapagar(" + doc.getCddocumento() +");\">"+ doc.getCddocumento() +"</a> ");
							}
						}
					}
					Entregadocumentohistorico entregadocumentohistorico = new Entregadocumentohistorico();
					entregadocumentohistorico.setEntregadocumento(entregadocumento);
					entregadocumentohistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
					entregadocumentohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
					entregadocumentohistorico.setEntregadocumentosituacao(Entregadocumentosituacao.FATURADA);
					entregadocumentohistorico.setObservacao("Conta a pagar: " + obsEntregadocumento.toString() +"</a>");
					entregadocumentohistoricoService.saveOrUpdate(entregadocumentohistorico);
				}
				return entrega;
			}
		});
		
		if (entrega != null) {			
			if (entrega instanceof Entrega) {				
				if(((Entrega)entrega).getWhereInEntregas() != null){
					String[] cdEntrega = ((Entrega)entrega).getWhereInEntregas().split(",");
					for(int i = 0; i < cdEntrega.length; i++){
						Entrega e = new Entrega();						
						e.setCdentrega((Integer.parseInt(cdEntrega[i])));
						entregaService.callProcedureAtualizaEntrega(e);
					}
					
				}else{
					entregaService.callProcedureAtualizaEntrega((Entrega)entrega);
				}
			}
		}
	}
	
	/**
	 * M�todo para obter um parcelamento atrav�s de um documento.
	 * 
	 * @param documento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Parcelamento findByDocumento(Documento documento){
		if(documento == null){
			throw new SinedException("O par�metro documento n�o pode ser null.");
		}
		return query()
			.select("parcelamento.cdparcelamento, parcelamento.prazopagamento, parcelamento.iteracoes, " +
					"parcela.cdparcela, parcela.ordem")
			.from(Parcelamento.class, "parcelamento")
			
			.where("exists (" +
					"select p.cdparcela " +
					"from Parcela p " +
					"join p.parcelamento parc " +
					"join p.documento doc " +
					"where parc.cdparcelamento = parcelamento.cdparcelamento " +
					"and doc.cddocumento = " + documento.getCddocumento() + " " +
					")")
			
			.join("parcelamento.listaParcela parcela")
			.unique();
	}
	
}
