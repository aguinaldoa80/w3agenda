package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Permissao;
import br.com.linkcom.sined.geral.bean.Tela;
import br.com.linkcom.sined.geral.service.PermissaoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PapelFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("papel.nome")
public class PapelDAO extends GenericDAO<Papel> {

	private PermissaoService permissaoService;
	
	public void setPermissaoService(PermissaoService permissaoService) {
		this.permissaoService = permissaoService;
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Papel> query) {
		query.leftOuterJoinFetch("papel.listaDashboardpapel listaDashboardpapel");
		query.leftOuterJoinFetch("papel.listaDashboardespecificopapel listaDashboardespecificopapel");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaDashboardpapel");
		save.saveOrUpdateManaged("listaDashboardespecificopapel");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Papel> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		PapelFiltro filtro = (PapelFiltro) _filtro;
		query
			.whereLikeIgnoreAll("papel.nome", filtro.getNome())
			.orderBy("papel.nome");
	}

	/**
	 * M�todo para salvar como padr�o acesso a todos os n�veis a usu�rio.
	 * 
	 * @author Jo�o Paulo Zica
	 * @param papel
	 */
	public void savePermissaoTelaUsuario(Papel papel){ 
		if (papel == null) {
			throw new SinedException("O par�metro cdpapel n�o pode ser null.");
		}
		Permissao permissao = new Permissao();
		permissao.setPapel(papel);
		permissao.setStringpermissao("update=true;delete=true;read=true;create=true");
		permissao.setTela(new Tela(Tela.USUARIO));
		permissaoService.saveOrUpdate(permissao); 
	}
	
	
	
	@Override
	public List<Papel> findAll() {
		return query()
			.orderBy("papel.nome")
			.list();
	}



	/* singleton */
	private static PapelDAO instance;
	public static PapelDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(PapelDAO.class);
		}
		return instance;
	}

	/**
	 * M�todo que busca os papeis que tem permiss�o a executar a a��o
	 * 
	 * @param key
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Papel> carregaPapeisPermissaoAcao(String key) {
		if(key == null || key.equals(""))
			throw new SinedException("Par�metros incorretos.");
		
		return query()
			.select("papel.cdpapel")
			.join("papel.listaAcaopapel listaAcaopapel")
			.join("listaAcaopapel.acao acao")
			.where("acao.key = ?", key)
			.list();
	}

	/**
	 * M�todo que busca os papeis que tem permiss�o de criar de acordo com o caminho da tela
	 * 
	 * @param pathPermissao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Papel> carregaPapeisPermissaoUpdate(String pathPermissao) {
		if(pathPermissao == null || pathPermissao.equals(""))
			throw new SinedException("Par�metros incorretos.");
		
		return query()
			.select("papel.cdpapel")
			.join("papel.listaPermissoes listaPermissoes")
			.join("listaPermissoes.tela tela")
			.where("tela.path = ?", pathPermissao)
			.whereLikeIgnoreAll("listaPermissoes.stringpermissao", "update=true")
			.list();
	}		
}