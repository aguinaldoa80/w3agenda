package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Sincronizacaotabelaandroid;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.rest.android.downloadbean.DownloadBeanRESTWSBean;

public class SincronizacaotabelaandroidDAO extends GenericDAO<Sincronizacaotabelaandroid> {

	public List<Sincronizacaotabelaandroid> findForAndroid(DownloadBeanRESTWSBean command, Boolean excluido, Long data) {
		QueryBuilder<Sincronizacaotabelaandroid> query = query()
				.where("dtalteracao >= ?", new Timestamp(command.getDataUltimaAtualizacao()), data == null && command.getDataUltimaAtualizacao() != null)
				.where("coalesce(sincronizacaotabelaandroid.excluido, false) = ?", excluido)
				.orderBy("dtalteracao");
		
		if(data == null){
			query.setMaxResults(100);
		}else {
			query.where("dtalteracao = ?", new Timestamp(data));
		}
		return query.list();
	}
	
}
