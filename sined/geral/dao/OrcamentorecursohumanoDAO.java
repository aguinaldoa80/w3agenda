package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Orcamentorecursohumano;
import br.com.linkcom.sined.geral.bean.Tipocargo;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.OrcamentoRecursoHumanoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OrcamentorecursohumanoDAO extends GenericDAO<Orcamentorecursohumano>{
	
	/**
	 * Busca os recursos humanos do or�amento a partir de um filtro
	 *
	 * @param filtro
 	 * @return List<Orcamentorecursohumano>
 	 * 
 	 * @throws SinedException - caso o or�amento seja nulo
	 * 
 	 * @author Rodrigo Alvarenga
	 */
	public List<Orcamentorecursohumano> findRecursoHumano(OrcamentoRecursoHumanoFiltro filtro) {
		if (filtro.getOrcamento() == null || filtro.getOrcamento().getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		
		return
			query()
				.select(
					"orcamentorecursohumano.cdorcamentorecursohumano, orcamentorecursohumano.totalhoras, orcamentorecursohumano.custohora, " +
					"cargo.cdcargo, cargo.nome, cargo.totalhorasemana, tipocargo.cdtipocargo, tipocargo.nome, " +
					"fatormdo.cdfatormdo, fatormdo.descricao, fatormdo.total, fatormdo.formula," +
					"contagerencial.cdcontagerencial ")
				.join("orcamentorecursohumano.cargo cargo")
				.join("cargo.tipocargo tipocargo")
				.join("orcamentorecursohumano.orcamento orcamento")
				.leftOuterJoin("orcamentorecursohumano.fatormdo fatormdo")
				.leftOuterJoin("cargo.contagerencial contagerencial")
				.where("orcamento = ?", filtro.getOrcamento())
				.where("tipocargo = ?", filtro.getTipoCargo())
				.whereLikeIgnoreAll("cargo.nome", filtro.getNomeCargo())
				.orderBy("cargo.nome")
				.list();
	}	
	
	/**
	 * Atualiza o custo do recurso humano passado como par�metro
	 * 
	 * @param orcamentoRecursoHumano
	 * @return 
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public void atualizaGastoRecursoHumanoForFlex(final Orcamentorecursohumano orcamentoRecursoHumano) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				String sqlUpdate = 
					"UPDATE Orcamentorecursohumano " +
					"SET custohora = ?, " +
					"fatormdo.id = ? " +
					"WHERE cdorcamentorecursohumano = ?";
				
				Object[] fields = new Object[]{orcamentoRecursoHumano.getCustohora(), orcamentoRecursoHumano.getFatormdo().getCdfatormdo(), orcamentoRecursoHumano.getCdorcamentorecursohumano()};					
				
				getHibernateTemplate().bulkUpdate(sqlUpdate, fields);				
				
				return null;
			}
		});
	}
	
	/***
	 * Insere/atualiza/apaga os registros em Orcamentorecursohumano baseado na listaPeriodoOrcamentoCargo
	 * 
	 * @param listaOrcamentoRecursoHumanoForUpdate
	 * @param listaOrcamentoRecursoHumanoForDelete
	 * @return 
	 * @throws SinedException - caso um dos par�metros seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public void atualizaOrcamentoRecursoHumanoFlex(final List<Orcamentorecursohumano> listaOrcamentoRecursoHumanoForUpdate, final List<Orcamentorecursohumano> listaOrcamentoRecursoHumanoForDelete) {
		if (listaOrcamentoRecursoHumanoForUpdate == null || listaOrcamentoRecursoHumanoForDelete == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}		
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {

				//Apaga os registros
				for (Orcamentorecursohumano orcamentoRecursoHumanoForDelete : listaOrcamentoRecursoHumanoForDelete) {
					orcamentoRecursoHumanoForDelete.setOrcamento(null);
					delete(orcamentoRecursoHumanoForDelete);
				}

				//Insere/atualiza os registros				
				for (Orcamentorecursohumano orcamentoRecursoHumanoForUpdate : listaOrcamentoRecursoHumanoForUpdate) {
					saveOrUpdate(orcamentoRecursoHumanoForUpdate);
				}
				return null;
			}
		});
	}


	/**
	 * Carrega os recursos humanos de um or�amento.
	 *
	 * @param orcamento
	 * @param mod
	 * @param moi
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Orcamentorecursohumano> findByOrcamento(Orcamento orcamento, Boolean mod, Boolean moi) {
		if(orcamento == null || orcamento.getCdorcamento() == null){
			throw new SinedException("Or�amento n�o pode ser nulo.");
		}
		QueryBuilder<Orcamentorecursohumano> query = query()
					.select("orcamentorecursohumano.totalhoras, cargo.cdcargo")
					.leftOuterJoin("orcamentorecursohumano.cargo cargo")
					.leftOuterJoin("cargo.tipocargo tipocargo")
					.join("orcamentorecursohumano.orcamento orcamento")
					.where("orcamento = ?", orcamento);
		if(mod && moi){
			query
				.openParentheses()
				.where("tipocargo = ?", Tipocargo.MOD)
				.or()
				.where("tipocargo = ?", Tipocargo.MOI)
				.closeParentheses();
		} else if(mod && !moi){
			query.where("tipocargo = ?", Tipocargo.MOD);
		} else if(moi && !mod){
			query.where("tipocargo = ?", Tipocargo.MOI);
		}
		
		return query.list();
	}

	public List<Orcamentorecursohumano> findByCargoOrcamento(Orcamento orcamento) {
		if(orcamento == null || orcamento.getCdorcamento() == null){
			throw new SinedException("Or�amento n�o pode ser nulo.");
		}
		return query()
					.select("orcamentorecursohumano.custohora, orcamentorecursohumano.cdorcamentorecursohumano, " +
							"cargo.cdcargo, fatormdo.total")
					.leftOuterJoin("orcamentorecursohumano.cargo cargo")
					.leftOuterJoin("orcamentorecursohumano.orcamento orcamento")
					.leftOuterJoin("orcamentorecursohumano.fatormdo fatormdo")
					.where("orcamento = ?", orcamento)
					.list();
	}	
}
