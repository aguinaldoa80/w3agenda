package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.ClienteDocumentoTipo;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;


public class ClienteDocumentoTipoDAO extends GenericDAO<ClienteDocumentoTipo>{
	
	/**
	 * Lista todos os Documento Tipo para um determinado Cliente
	 * @author Marcos Lisboa
	 */
	public List<ClienteDocumentoTipo> findByCliente(Cliente cliente) {
		if(cliente == null || cliente.getCdpessoa() == null)
			throw new SinedException("Cliente n�o pode ser nulo");
		return querySined()
			.select("clienteDocumentoTipo.cdclientedocumentotipo, documentotipo.cddocumentotipo, documentotipo.nome")
			.leftOuterJoin("clienteDocumentoTipo.documentotipo documentotipo")
			.where("clienteDocumentoTipo.cliente = ?", cliente)
			.list();

	}
	
	/**
	* M�todo que carrega o tipo de documento do cliente de acordo com a permiss�o do usu�rio
	*
	* @param cliente
	* @param isAdmin
	* @param whereInPapel
	* @return
	* @since 13/05/2015
	* @author Luiz Fernando
	*/
	public List<ClienteDocumentoTipo> findByClienteUsuario(Cliente cliente, Usuario usuario) {
		if(cliente == null || cliente.getCdpessoa() == null)
			throw new SinedException("Cliente n�o pode ser nulo");
		boolean isAdmin = SinedUtil.isUsuarioAdministrador(usuario);
		String whereInPapel = SinedUtil.getWhereInCdPapel(usuario);
		QueryBuilder<ClienteDocumentoTipo> query = querySined()
			.select("clienteDocumentoTipo.cdclientedocumentotipo, documentotipo.cddocumentotipo, documentotipo.nome, documentotipo.exibirnavenda, documentotipo.exibirnacompra")
			.leftOuterJoin("clienteDocumentoTipo.documentotipo documentotipo")
			.where("clienteDocumentoTipo.cliente = ?", cliente);
			
		if (!isAdmin){
			query.leftOuterJoin("documentotipo.listaDocumentotipopapel listaDocumentotipopapel");
			
			if (StringUtils.isNotEmpty(whereInPapel)){
				query
					.openParentheses()
						.whereIn("listaDocumentotipopapel.papel.cdpapel", whereInPapel)
						.or()
						.where("listaDocumentotipopapel.papel is null")
					.closeParentheses();
			} else {
				query.where("listaDocumentotipopapel.papel is null");
			}
		}
		
		return query.list();
	}
	
	/**
	* M�todo que busca os tipos de documento vinculados ao clientes
	*
	* @param whereIn
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<ClienteDocumentoTipo> findForAndroid(String whereIn) {
		return query()
			.select("clienteDocumentoTipo.cdclientedocumentotipo, cliente.cdpessoa, documentotipo.cddocumentotipo ")
			.join("clienteDocumentoTipo.cliente cliente")
			.join("clienteDocumentoTipo.documentotipo documentotipo")
			.whereIn("clienteDocumentoTipo.cdclientedocumentotipo", whereIn)
			.list();
	}
}