package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregahistorico;
import br.com.linkcom.sined.geral.bean.enumeration.Entregaacao;
import br.com.linkcom.sined.geral.service.Entregahistoricoservice;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EntregahistoricoDAO extends GenericDAO<Entregahistorico> {
	
	public void criaHistoricoEntregas(List<Entrega> listaentregas, Entregaacao entregaacao){
		final List<Entrega> entregas = listaentregas;
		final Entregaacao acao = entregaacao;
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				for(Entrega entrega: entregas){
					Entregahistoricoservice.getInstance().criaHistoricoEntrega(entrega, acao);
				}
				return null;
			}
		});
	}
	
	public void criaHistoricoEntrega(Entrega entrega, Entregaacao entregaacao){
		Entregahistorico bean = new Entregahistorico();
		bean.setEntrega(entrega);
		bean.setEntregaacao(entregaacao);
		this.saveOrUpdate(bean);
	}

}
