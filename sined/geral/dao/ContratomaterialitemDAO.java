package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Contratomaterialitem;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContratomaterialitemDAO extends GenericDAO<Contratomaterialitem> {

	/**
	 * Busca pelo contratomaterial.
	 *
	 * @param contratomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/01/2013
	 */
	public List<Contratomaterialitem> findByContratomaterial(Contratomaterial contratomaterial) {
		if(contratomaterial == null || contratomaterial.getCdcontratomaterial() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("contratomaterialitem.cdcontratomaterialitem, contratomaterialitem.qtde, " +
							"material.cdmaterial, material.identificacao, material.nome, " +
							"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
							"materialgrupo.cdmaterialgrupo, materialgrupo.nome")
					.join("contratomaterialitem.material material")
					.join("material.materialgrupo materialgrupo")
					.join("contratomaterialitem.contratomaterial contratomaterial")
					.leftOuterJoin("contratomaterialitem.localarmazenagem localarmazenagem")
					.where("contratomaterial = ?", contratomaterial)
					.orderBy("materialgrupo.nome, material.nome")
					.list();
	}
	
}
