package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Interacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class InteracaoDAO extends GenericDAO<Interacao>{
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaInteracaoitem");			
	}
	
	
}
