package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Projetodespesa;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProjetodespesaDAO extends GenericDAO<Projetodespesa> {

	/**
	 * M�todo para obter uma lista de despesas por projeto.
	 * 
	 * @param projeto
	 * @param dataAte 
	 * @return 
	 * @author Marden Silva
	 */
	public List<Projetodespesa> findByProjetoData(Projeto projeto, Date dataAte) {
		if (projeto == null || projeto.getCdprojeto() == null) {
			throw new SinedException("O projeto n�o pode ser nulo.");
		}
		
		QueryBuilder<Projetodespesa> query = query()
			.select("projetodespesa.cdprojetodespesa, projetodespesa.valortotal, projetodespesa.valorhora, " +
					"projeto.cdprojeto, projeto.nome, " +
					"contagerencial.cdcontagerencial, contagerencial.nome, vcontagerencial.identificador, " +
					"vcontagerencial.nome ")
			.join("projetodespesa.projeto projeto")
			.join("projetodespesa.contagerencial contagerencial")
			.join("contagerencial.vcontagerencial vcontagerencial")
			.where("projetodespesa.projeto = ?", projeto)
			.orderBy("contagerencial.nome");
		
		if(dataAte != null){
			query
				.openParentheses()
				.where("projetodespesa.mesano <= ?", dataAte)
				.or()
				.where("projetodespesa.mesano is null")
				.closeParentheses();
		}
		
		return query.list();		
	}
	/**
	 * M�todo que retorna as despesas acumuladas por projetos ativos.
	 * 
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Projetodespesa> findAllForAtualizarRateio(){
		Date dtAtual = SinedDateUtils.currentDate();
		return query()
				.select("distinct new br.com.linkcom.sined.geral.bean.Projetodespesa(projeto.cdprojeto, projeto.nome, sum(projetodespesa.valortotal))")
				.setUseTranslator(false)
				.join("projetodespesa.projeto projeto")
				.where("projeto.dtprojeto <= ?", dtAtual)
				.openParentheses()
					.where("projeto.dtfimprojeto >= ?", dtAtual)
					.or()
					.where("projeto.dtfimprojeto is null")
				.closeParentheses()
				.groupBy("projeto.cdprojeto, projeto.nome")
				.list();
	}
}
