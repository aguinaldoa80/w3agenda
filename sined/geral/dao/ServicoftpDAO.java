package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Servicoftp;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.ServicoftpFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ServicoftpDAO extends GenericDAO<Servicoftp>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Servicoftp> query, FiltroListagem _filtro) {
		ServicoftpFiltro filtro = (ServicoftpFiltro)_filtro;
		query.leftOuterJoinFetch("servicoftp.servicoservidor servicoservidor")
			 .leftOuterJoinFetch("servicoservidor.servidor servidor")
		 	 .leftOuterJoinFetch("servicoftp.contratomaterial contratomaterial")
		 	 .leftOuterJoinFetch("contratomaterial.contrato contrato")
		 	 .where("servicoservidor.servidor = ?", filtro.getServidor())
		 	 .where("servicoftp.servicoservidor = ?", filtro.getServicoservidor())
		 	 .where("contrato.cliente = ?", filtro.getCliente())
			 .where("servicoftp.contratomaterial = ?", filtro.getContratomaterial())
			 .where("servicoftp.ativo = ?", filtro.getAtivo())
			 .whereLikeIgnoreAll("servicoftp.usuario", filtro.getUsuario());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Servicoftp> query) {
		query.leftOuterJoinFetch("servicoftp.servicoservidor servicoservidor")
			 .leftOuterJoinFetch("servicoservidor.servidor servidor")
	 	 	 .leftOuterJoinFetch("servicoftp.contratomaterial contratomaterial")
			 .leftOuterJoinFetch("contratomaterial.contrato contrato");
	}

	public int countContratomaterial(Integer cdservicoftp, Contratomaterial contratomaterial) {
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.from(Servicoftp.class)
					.where("servicoftp.cdservicoftp <> ?", cdservicoftp)
					.where("servicoftp.contratomaterial = ?", contratomaterial)
					.unique()
					.intValue();
	}
		
}
