package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.ResponsavelFrete;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("responsavelFrete.cdnfe")
public class ResponsavelFreteDAO extends GenericDAO<ResponsavelFrete> {

	public List<ResponsavelFrete> findForPVOffline() {
		return query()
				.select("responsavelFrete.cdResponsavelFrete, responsavelFrete.nome, responsavelFrete.cdnfe")
				.list();
	}
	
	public List<ResponsavelFrete> findForAndroid(String whereIn) {
		return query()
				.select("responsavelFrete.cdResponsavelFrete, responsavelFrete.nome, responsavelFrete.cdnfe")
				.whereIn("responsavelFrete.cdResponsavelFrete", whereIn)
				.list();
	}

}
