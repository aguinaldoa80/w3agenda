package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.Coletahistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColetahistoricoDAO extends GenericDAO<Coletahistorico>{

	/**
	 * Busca o histórico de uma coleta
	 *
	 * @param coleta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/07/2014
	 */
	public List<Coletahistorico> findByColeta(Coleta coleta) {
		return query()
					.select("coletahistorico.cdcoletahistorico, coletahistorico.dtaltera, coletahistorico.cdusuarioaltera, " +
							"coletahistorico.acao, coletahistorico.observacao, coleta.cdcoleta," +
							"coletaMaterial.cdcoletamaterial, motivodevolucao.cdmotivodevolucao, motivodevolucao.descricao, cmc.cdcoleta")
					.leftOuterJoin("coletahistorico.coleta coleta")
					.leftOuterJoin("coleta.listaColetaMaterial coletaMaterial")
//					.leftOuterJoin("coletaMaterial.motivodevolucao motivodevolucao")
					.leftOuterJoin("coletaMaterial.coleta cmc")
					.leftOuterJoin("coletaMaterial.listaColetamaterialmotivodevolucao listaColetamaterialmotivodevolucao")
					.leftOuterJoin("listaColetamaterialmotivodevolucao.motivodevolucao motivodevolucao")
					.where("coletahistorico.coleta = ?", coleta)
					.orderBy("coletahistorico.dtaltera DESC")
					.list();
	}

	
}
