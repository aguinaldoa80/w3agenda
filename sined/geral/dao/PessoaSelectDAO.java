package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.UsuarioFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PessoaSelectDAO extends GenericDAO<Pessoa> {
		
	@Override
	public void updateListagemQuery(QueryBuilder<Pessoa> query, FiltroListagem _filtro) {
		UsuarioFiltro filtro = (UsuarioFiltro)_filtro;
		query
			.select("pessoa.cdpessoa, pessoa.nome, pessoa.email")
			.openParentheses()
			.where("exists (select c.id from Colaborador c where c.id = pessoa.id)")
			.or()
			.where("exists (select u.id from Usuario u where u.id = pessoa.id)")
			.closeParentheses()
			.whereLikeIgnoreAll("pessoa.nome", filtro.getNome())
			.orderBy("pessoa.nome");
	}
	
}
