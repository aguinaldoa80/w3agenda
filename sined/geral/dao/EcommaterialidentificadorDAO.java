package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Configuracaoecom;
import br.com.linkcom.sined.geral.bean.Ecommaterialidentificador;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EcommaterialidentificadorDAO extends GenericDAO<Ecommaterialidentificador>{

	/**
	 * Busca pelo material
	 *
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public Ecommaterialidentificador findByMaterial(Material material, Configuracaoecom configuracaoecom) {
		return query()
					.where("ecommaterialidentificador.material = ?", material)
					.where("ecommaterialidentificador.configuracaoecom = ?", configuracaoecom)
					.unique();
	}

	/**
	 * Deleta pelo identificador
	 *
	 * @param identificador
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public void deleteByIdentificador(String identificador, Configuracaoecom configuracaoecom) {
		getJdbcTemplate().execute("DELETE FROM ECOMMATERIALIDENTIFICADOR WHERE IDENTIFICADOR = '" + identificador + "' AND CDCONFIGURACAOECOM = " + configuracaoecom.getCdconfiguracaoecom());
	}

	/**
	 * Busca pelo identificador.
	 *
	 * @param identificador
	 * @param configuracaoecom 
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public Ecommaterialidentificador findByIdentificador(String identificador, Configuracaoecom configuracaoecom) {
		List<Ecommaterialidentificador> lista = query()
			.select("ecommaterialidentificador.cdecommaterialidentificador, material.cdmaterial, unidademedida.cdunidademedida")
			.join("ecommaterialidentificador.material material")
			.join("material.unidademedida unidademedida")
			.where("ecommaterialidentificador.configuracaoecom = ?", configuracaoecom)
			.where("ecommaterialidentificador.identificador = ?", identificador)
			.list();
		
		if(lista != null && lista.size() == 1) return lista.get(0);
		return null;
	}

}
