package br.com.linkcom.sined.geral.dao;

 
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.NeoFormater;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivobancario;
import br.com.linkcom.sined.geral.bean.Arquivobancariodocumento;
import br.com.linkcom.sined.geral.bean.Arquivobancariomovimentacao;
import br.com.linkcom.sined.geral.bean.Arquivobancariosituacao;
import br.com.linkcom.sined.geral.bean.Arquivobancariotipo;
import br.com.linkcom.sined.geral.bean.Calendario;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Contatotipo;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Indicecorrecao;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Parcelamento;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.AnaliseReceitaDespesaOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRemessaInstrucaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Formafaturamento;
import br.com.linkcom.sined.geral.bean.enumeration.MailingDevedorContasProtestadas;
import br.com.linkcom.sined.geral.bean.enumeration.MailingDevedorMostrar;
import br.com.linkcom.sined.geral.bean.enumeration.MailingDevedorVisaoMostrar;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoLancamentoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContagerencial;
import br.com.linkcom.sined.geral.bean.enumeration.OrdenacaoMailingDevedor;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.SimNaoSomenteEnum;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoremessaEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculodias;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.bean.enumeration.W3erpBankStatusIntegracao;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ArquivobancarioService;
import br.com.linkcom.sined.geral.service.ArquivobancariodocumentoService;
import br.com.linkcom.sined.geral.service.ArquivobancariomovimentacaoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.TaxaService;
import br.com.linkcom.sined.geral.service.TaxaitemService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.ArquivobancarioProcessFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.DocumentoFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRemessaBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRemessaPagamentoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRetornoDocumentoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BoletoDigitalRemessaBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.GerarArquivoRemessaBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.MailingDevedorBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.SelecionarDocumentoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ContaReceberPagarBeanReport;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.AnaliseReceitaDespesaReportFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.FluxocaixaFiltroReport;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.AcompanhamentoEconomicoReportFiltro;
import br.com.linkcom.sined.util.DateUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

public class DocumentoDAO extends GenericDAO<Documento> {
	
	private ContagerencialDAO contagerencialDAO;
	private TaxaService taxaService;
	private RateioService rateioService;
	private ArquivobancarioService arquivobancarioService;
	private ArquivobancariodocumentoService arquivobancariodocumentoService;
	private ArquivoService arquivoService;
	private ArquivoDAO arquivoDAO;
//	private CategoriaService categoriaService;
	private TaxaitemService taxaitemService;
	private ParametrogeralService parametrogeralService;
	private VendaService vendaService;
	private ArquivobancariomovimentacaoService arquivobancariomovimentacaoService;
	private EmpresaService empresaService;
	private MovimentacaoDAO movimentacaoDAO;
	
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setContagerencialDAO(ContagerencialDAO contagerencialDAO) {this.contagerencialDAO = contagerencialDAO;}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	public void setTaxaService(TaxaService taxaService) {this.taxaService = taxaService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setArquivobancarioService(ArquivobancarioService arquivobancarioService) {this.arquivobancarioService = arquivobancarioService;}
	public void setArquivobancariodocumentoService(ArquivobancariodocumentoService arquivobancariodocumentoService) {this.arquivobancariodocumentoService = arquivobancariodocumentoService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
//	public void setCategoriaService(CategoriaService categoriaService) {this.categoriaService = categoriaService;}
	public void setTaxaitemService(TaxaitemService taxaitemService) {this.taxaitemService = taxaitemService;}
	public void setArquivobancariomovimentacaoService(ArquivobancariomovimentacaoService arquivobancariomovimentacaoService) {this.arquivobancariomovimentacaoService = arquivobancariomovimentacaoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setMovimentacaoDAO(MovimentacaoDAO movimentacaoDAO) {this.movimentacaoDAO = movimentacaoDAO;}
	
	@SuppressWarnings("unchecked")
	@Override
	public void updateListagemQuery(QueryBuilder<Documento> query, FiltroListagem _filtro) {
		DocumentoFiltro filtro = (DocumentoFiltro) _filtro;
		
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		boolean parametroArrayPessoa = ParametrogeralService.getInstance().getBoolean("parametroArrayPessoa");
		Integer idContrato = null;
		String cdNota = NeoWeb.getRequestContext().getParameter("idContrato");
		if(cdNota != null && !cdNota.equals("")){
			idContrato = Integer.parseInt(cdNota);
		}
		Integer idPedidovenda = null;
		Integer idVenda = null;
		String idVendastr = NeoWeb.getRequestContext().getParameter("idVenda");
		if(idVendastr != null && !idVendastr.equals("")){
			idVenda = Integer.parseInt(idVendastr);
			
			Venda venda = vendaService.loadWithPedidovenda(new Venda(idVenda));
			if(venda != null && venda.getPedidovenda() != null){
				idPedidovenda = venda.getPedidovenda().getCdpedidovenda();
			}
		}

		
		List<String> ignoreJoins = new ArrayList<String>();
		ignoreJoins.add("parcela");
		ignoreJoins.add("parcelamento");
		
		//Ao alterar os campos desse select, favor conferir se o campo (select cli.razaosocial from Cliente cli where cli.cdpessoa = pessoa.cdpessoa)
		//continuar� na posi��o 5. 
		//Caso essa posi��o seja alterada, alterar o valor de 'order="5"' da property <t:property name="cliente.razaosocial" label="Recebimento de" order="5"/>
		//no arquivo contareceberListagem.jsp 
		//E alterar no m�todo loadForListagem desta mesma classe
		query
			.select(
					"distinct new br.com.linkcom.sined.geral.bean.Documento(" +
					"documento.cddocumento, " +
					"documento.dtemissao, " +
					"documento.dtvencimento, " +
					"documento.dtcompetencia, " +
					"(select cli.razaosocial from Cliente cli where cli.cdpessoa = pessoa.cdpessoa), " +
					"coalesce(pessoa.nome,documento.outrospagamento)," +
					"trim(coalesce(documento.numero,'')), " +
					"documento.descricao, " +
					"documentoacao.cddocumentoacao, " +
					"aux_documento.valoratual, " +
					"documento.valor," +
					"documento.dtvencimentoantiga" +
			")")
			.setUseTranslator(false)
			.join("documento.aux_documento aux_documento")
			.join("documento.documentoacao documentoacao")
			;
		
		/**
		 * 	filtros dependentes de Rateio, RateioItem e Projeto (ESTES JOINS TEM QUE FICAR EM PRIMEIRO NA QUERY PARA OTIMIZA��O) 
		 */
		if(filtro.getProjeto()!=null || filtro.getProjetoAberto()!=null || filtro.getProjetotipo() != null){
			query
				.leftOuterJoinIfNotExists("documento.rateio rateio")
				.leftOuterJoinIfNotExists("rateio.listaRateioitem item");
			
			ignoreJoins.add("rateio");
			ignoreJoins.add("item");
			ignoreJoins.add("projeto");
			
			if(filtro.getProjeto()!=null || filtro.getProjetoAberto()!=null || filtro.getProjetotipo() != null){
				query
					.leftOuterJoinIfNotExists("item.projeto projeto")
					.where("item.projeto = ?", filtro.getProjeto());
			}
			
			if(filtro.getProjetotipo() != null){
				query
					.leftOuterJoinIfNotExists("projeto.projetotipo projetotipo")
					.where("projetotipo = ?", filtro.getProjetotipo());
			}
			
			if (filtro.getProjetoAberto() != null && filtro.getProjeto() == null) {
				if (filtro.getProjetoAberto()) {
					query
						.openParentheses()
						.where("projeto.dtfimprojeto is null")
						.or()
						.where("projeto.dtfimprojeto > ?",SinedDateUtils.currentDate())
						.closeParentheses();
				} else {
					query
						.where("projeto.dtfimprojeto is not null")
						.where("projeto.dtfimprojeto <= ?", SinedDateUtils.currentDate());
				}
			}
		}
		
		//Por quest�es de otimiza��o da query no Postgres, � utilizado o WITH para limitar a quantidade de registros no join de document.pessoa, quando � feita a pesquisa por pessoa
		String pessoaString = "";
		String pessoaCpf = "";
		String pessoaCnpj = "";
		if(StringUtils.isNotBlank(filtro.getPessoa())){
			String cpfCnpjSoNumero = br.com.linkcom.neo.util.StringUtils.soNumero(filtro.getPessoa());
			if(StringUtils.isNotBlank(cpfCnpjSoNumero) && Cpf.cpfValido(cpfCnpjSoNumero)){
				pessoaCpf = cpfCnpjSoNumero;
				query.leftOuterJoinIfNotExists("documento.pessoa pessoa");
				query.where("pessoa.cpf = ?",new Cpf(pessoaCpf));
			} else if(StringUtils.isNotBlank(cpfCnpjSoNumero) && Cnpj.cnpjValido(cpfCnpjSoNumero)){
				pessoaCnpj = cpfCnpjSoNumero;
				query.leftOuterJoinIfNotExists("documento.pessoa pessoa");
				query.where("pessoa.cnpj = ?",new Cnpj(pessoaCnpj));
			} else{
				pessoaString = Util.strings.tiraAcento(filtro.getPessoa().replaceAll("'", "''").toUpperCase());
				if(parametroArrayPessoa){
					query.leftOuterJoinIfNotExists("documento.pessoa pessoa WITH (retorna_array_pessoa('"+pessoaString+"', pessoa.cdpessoa )=true) ");
				} else {
					query.leftOuterJoinIfNotExists("documento.pessoa pessoa WITH (UPPER(" + funcaoTiraacento + "(pessoa.nome)) LIKE '%'||'"+pessoaString+"'||'%') or (subquery_documento_listagem_fornecedor_cliente('"+pessoaString+"', pessoa.cdpessoa)=true) ");
				}	
			}
		} else {
			query.leftOuterJoinIfNotExists("documento.pessoa pessoa");
		}
		
		if(filtro.getCategoria() != null){
			query
				.leftOuterJoinIfNotExists("pessoa.listaPessoacategoria listaPessoacategoria")
				.where("listaPessoacategoria.categoria = ?", filtro.getCategoria());
		}
		
		try{
			Integer idAg = null;
			String cdAg = NeoWeb.getRequestContext().getParameter("cdagendamentoservicodocumento");
			if(cdAg != null && !cdAg.equals("")){
				idAg = Integer.parseInt(cdAg);
			}
			if(idAg != null){
				query.where("documento.cddocumento in (select d.cddocumento from Documento d join d.listaDocumentoOrigem lo join lo.agendamentoservicodocumento ag where ag.cdagendamentoservicodocumento = " + idAg + ")");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/**
		 * filtros dependentes de taxa, taxaitem e tipotaxa 
		 */
		if(filtro.getTipotaxa() != null || StringUtils.isNotBlank(filtro.getMotivo())){
			query
				.leftOuterJoinIfNotExists("documento.taxa taxa")
				.leftOuterJoinIfNotExists("taxa.listaTaxaitem ti");
			
			if(StringUtils.isNotBlank(filtro.getMotivo()))
				query.where("ti.motivo = ?", filtro.getMotivo());
			
			if(filtro.getTipotaxa()!=null){
				query.where("ti.tipotaxa = ?", filtro.getTipotaxa());
			}
		}
		
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		/**
		 * Filtros dependentes de movimentacaoorigem, movimentacao e cheque
		 */
		if(StringUtils.isNotBlank(filtro.getCheque()) || !administrador){
			query
				.leftOuterJoinIfNotExists("documento.listaMovimentacaoOrigem listaMovimentacaoOrigem")
				.leftOuterJoinIfNotExists("listaMovimentacaoOrigem.movimentacao movimentacao")
				.leftOuterJoinIfNotExists("movimentacao.conta contaMov")
				.leftOuterJoinIfNotExists("contaMov.listaContapapel listaContapapelMov");
			
			if (whereInPapel != null && !whereInPapel.equals("")){
				query.openParentheses();
					query.whereIn("listaContapapelMov.papel.cdpapel", whereInPapel)
					.or()
					.where("listaContapapelMov.papel.cdpapel is null");
				query.closeParentheses();
			} else {
				query.where("listaContapapelMov.papel.cdpapel is null");
			}
			
			if(StringUtils.isNotBlank(filtro.getCheque())){
				query
				.leftOuterJoinIfNotExists("movimentacao.cheque cheque")
				.leftOuterJoinIfNotExists("documento.cheque chequedocumento")
				.openParentheses()
					.where("cheque.numero = ?", filtro.getCheque())
					.or()
					.where("chequedocumento.numero = ?", filtro.getCheque())
				.closeParentheses();
			}
		}
		
		if(filtro.getAssociacao() != null){
			query
				.where("exists(select cli.cdpessoa from Cliente cli where cli.cdpessoa = pessoa.cdpessoa and cli.associacao = ?)", filtro.getAssociacao());
		}
		
		if(filtro.getPermitirFiltroDocumentoDeAte() != null && filtro.getPermitirFiltroDocumentoDeAte()){
			try {
				query.where("cast(COALESCE(documento.numero, '0') AS long) >= ?", Long.parseLong(filtro.getDocumentoDe()));
			} catch (Exception e) {}
			try {
				query.where("cast(COALESCE(documento.numero, '0') AS long) <= ?", Long.parseLong(filtro.getDocumentoAte()));
			} catch (Exception e) {}
		} else if(filtro.getDocumentoDe() != null && filtro.getDocumentoAte() != null &&  filtro.getDocumentoDe().equals(filtro.getDocumentoAte())){
			query.where("documento.numero = ?", filtro.getDocumentoDe());
		}
		
		if(filtro.getDtprimeirovencimento1() != null || filtro.getDtprimeirovencimento2() != null){
			query
				.where("exists(" +
							"select dh.cddocumentohistorico " +
							"from Documentohistorico dh " +
							"where dh.documento = documento " +
							"and dh.cddocumentohistorico = (select min(dh2.cddocumentohistorico) from Documentohistorico dh2 where dh2.documento = documento) " +
							(filtro.getDtprimeirovencimento1() != null ? "and dh.dtvencimento >= '" + SinedDateUtils.toStringPostgre(filtro.getDtprimeirovencimento1()) + "' " : "") + 
							(filtro.getDtprimeirovencimento2() != null ? "and dh.dtvencimento <= '" + SinedDateUtils.toStringPostgre(filtro.getDtprimeirovencimento2()) + "' " : "") + 
						")");
		}
		
		if(idContrato != null || idVenda != null || filtro.getContratotipo() != null || idPedidovenda != null){
			query
				.leftOuterJoinIfNotExists("documento.listaDocumentoOrigem documentoorigem");
			
			if(idContrato != null || filtro.getContratotipo() != null){
				query
					.leftOuterJoinIfNotExists("documentoorigem.contrato contrato")
					.leftOuterJoinIfNotExists("documento.listaNotaDocumento notadocumento")
					.leftOuterJoinIfNotExists("notadocumento.nota nota")
					.leftOuterJoinIfNotExists("nota.listaNotaContrato notacontrato")
					.leftOuterJoinIfNotExists("notacontrato.contrato contratocomnota");	
				
				ignoreJoins.add("documentoorigem");
				ignoreJoins.add("notadocumento");
				ignoreJoins.add("nota");
				ignoreJoins.add("notacontrato");
				ignoreJoins.add("contratocomnota");
				
				if(idContrato != null){
					query
						.openParentheses()
							.where("contrato.cdcontrato = ?", idContrato)
							.or()
							.where("contratocomnota.cdcontrato = ?", idContrato)
						.closeParentheses();
				}
				if(filtro.getContratotipo() != null){
					//query para o filtro por tipo de contrato
					query
						.openParentheses()
							.where("contrato.contratotipo = ?", filtro.getContratotipo())
							.or()
							.where("contratocomnota.contratotipo = ?", filtro.getContratotipo())
						.closeParentheses();
				}
			}
			
			if(idVenda != null){
				query.leftOuterJoinIfNotExists("documentoorigem.venda venda");
				query.leftOuterJoinIfNotExists("documentoorigem.pedidovenda pedidovenda");
				
				ignoreJoins.add("documentoorigem");
				ignoreJoins.add("venda");
				ignoreJoins.add("pedidovenda");
				
				query.openParentheses();
				query.where("venda.cdvenda = ?", idVenda).or();
				query.where("pedidovenda.cdpedidovenda = ?", idPedidovenda);
				query.closeParentheses();
			}
		}
		
		if(StringUtils.isNotBlank(filtro.getVendaDe()) || StringUtils.isNotBlank(filtro.getVendaAte())){
			query
				.leftOuterJoinIfNotExists("documento.listaDocumentoOrigem documentoorigem")
				.leftOuterJoinIfNotExists("documentoorigem.venda venda");
			
			if(StringUtils.isNotBlank(filtro.getVendaDe())){
				query.where("venda.cdvenda >= ?", Integer.parseInt(filtro.getVendaDe()));
			}
			if(StringUtils.isNotBlank(filtro.getVendaAte())){
				query.where("venda.cdvenda <= ?", Integer.parseInt(filtro.getVendaAte()));
			}
		}
		
		if (Documentoacao.REAGENDAMENTO.equals(filtro.getDocumentoacao())) {
			query.where("documento.dtvencimentooriginal IS NOT NULL");
		}

		query.where("documento.dtcartorio is not null", Documentoacao.PROTESTADA.equals(filtro.getDocumentoacao()));
		
		if (Documentoacao.ATRASADA.equals(filtro.getDocumentoacao())) {
			query
				.where("documento.dtvencimento < ?", SinedDateUtils.currentDate())
				.where("documento.documentoacao <> ?", Documentoacao.BAIXADA)
				.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
				.where("documento.documentoacao <> ?", Documentoacao.NEGOCIADA);
		} else if (Documentoacao.PAG_ATRASADO.equals(filtro.getDocumentoacao())) {
			query
				.where("exists (select m.cdmovimentacao "
						+ "from Movimentacao m "
						+ "join m.listaMovimentacaoorigem as listaMovimentacaoorigem "
						+ "join listaMovimentacaoorigem.documento as doc "
						+ "join m.movimentacaoacao as movimentacaoacao "
						+ "where doc.cddocumento = documento.cddocumento "
						+ "and movimentacaoacao.cdmovimentacaoacao <> 0" 
						+ "and documento.dtvencimento < m.dtmovimentacao)");
		} else if(Documentoacao.DEVOLVIDA.equals(filtro.getDocumentoacao())){
			query.where("documento.dtdevolucao is not null");
		} else if(Documentoacao.PENDENTE.equals(filtro.getDocumentoacao())){
			query
				.leftOuterJoinIfNotExists("documento.listaMovimentacaoOrigem movimentacaoorigem")
				.leftOuterJoinIfNotExists("movimentacaoorigem.movimentacao movimentacao")
				.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
				.openParentheses()
					.where("movimentacao.movimentacaoacao <> ?", Movimentacaoacao.CONCILIADA)
					.or()
					.where("movimentacao.id is null")
				.closeParentheses();
		} else if(Documentoacao.ATRASADA_CONTRATO_ATIVO.equals(filtro.getDocumentoacao())){
			String inativo = "(" + SituacaoContrato.CANCELADO.getValue().toString() + "," + SituacaoContrato.SUSPENSO.getValue().toString() + ")";
			query
				.where("documento.dtvencimento < ?", SinedDateUtils.currentDate())
				.where("documento.documentoacao <> ?", Documentoacao.BAIXADA)
				.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
				.where("documento.documentoacao <> ?", Documentoacao.NEGOCIADA)
				.openParentheses()
					.where(" documento.cddocumento in (select d2.cddocumento " +
							" from Documento d2 " +
							" join d2.listaNotaDocumento as lnd " +
							" join lnd.nota as nta " +
							" join nta.listaNotaContrato as lnc " +
							" join lnc.contrato as c2 " +
							" join c2.aux_contrato as aux2 " +
							" where aux2.situacao not in " +inativo+
							" and d2.cddocumento = documento.cddocumento)")
					.or()
					.where(" documento.cddocumento in (select d1.cddocumento " +
							" from Documento d1 " +
							" join d1.listaDocumentoOrigem ldo " +
							" join ldo.contrato c1 " +
							" join c1.aux_contrato aux1 " +
							" where aux1.situacao not in " +inativo+
							" and d1.cddocumento = documento.cddocumento) ")
				.closeParentheses();
		} else if (Documentoacao.ATRASADA_CONTRATO_SUSPENSO.equals(filtro.getDocumentoacao())) {
			query
				.where("documento.dtvencimento < ?", SinedDateUtils.currentDate())
				.where("documento.documentoacao <> ?", Documentoacao.BAIXADA)
				.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
				.where("documento.documentoacao <> ?", Documentoacao.NEGOCIADA)
				.openParentheses()
					.where(" documento.cddocumento in (select d2.cddocumento " +
							" from Documento d2 " +
							" join d2.listaNotaDocumento as lnd " +
							" join lnd.nota as nta " +
							" join nta.listaNotaContrato as lnc " +
							" join lnc.contrato as c2 " +
							" join c2.aux_contrato as aux2 " +
							" where aux2.situacao = " +SituacaoContrato.SUSPENSO.getValue().toString()+
							" and d2.cddocumento = documento.cddocumento)")
					.or()
					.where(" documento.cddocumento in (select d1.cddocumento " +
							" from Documento d1 " +
							" join d1.listaDocumentoOrigem ldo " +
							" join ldo.contrato c1 " +
							" join c1.aux_contrato aux1 " +
							" where aux1.situacao = " +SituacaoContrato.SUSPENSO.getValue().toString()+
							" and d1.cddocumento = documento.cddocumento) ")
				.closeParentheses();
		} else if (Documentoacao.ATRASADA_SEM_PROTESTO.equals(filtro.getDocumentoacao())) {
			query.where("documento.dtvencimento < ?", SinedDateUtils.currentDate())
				 .where("documento.documentoacao <> ?", Documentoacao.BAIXADA)
				 .where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
				 .where("documento.documentoacao <> ?", Documentoacao.NEGOCIADA)
				 .where("documento.dtcartorio is null");
		}
		
		if (Documentoacao.ENVIO_MANUAL_BOLETO.equals(filtro.getDocumentoacao())) {
			query.where("documento.cddocumento in (select d.cddocumento " +
					"from Documentohistorico do " +
					"join do.documento d " +
					"join do.documentoacao da " +
					"where da.cddocumentoacao = 110 " +
					")");
		}
		
		if (Documentoacao.BOLETO_GERADO.equals(filtro.getDocumentoacao())) {
			query.where("coalesce(documento.boletogerado,false)=?", Boolean.TRUE);
		}
		
		if (Documentoacao.BOLETO_NAO_GERADO.equals(filtro.getDocumentoacao())) {
			query.where("coalesce(documento.boletogerado,false)=?", Boolean.FALSE);
		}
		
		if (Documentoacao.ATRASADA_CONTRATO_CANCELADO.equals(filtro.getDocumentoacao())) {
			query
				.where("documento.dtvencimento < ?", SinedDateUtils.currentDate())
				.where("documento.documentoacao <> ?", Documentoacao.BAIXADA)
				.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
				.where("documento.documentoacao <> ?", Documentoacao.NEGOCIADA)
				.openParentheses()
					.where(" documento.cddocumento in (select d2.cddocumento " +
							" from Documento d2 " +
							" join d2.listaNotaDocumento as lnd " +
							" join lnd.nota as nta " +
							" join nta.listaNotaContrato as lnc " +
							" join lnc.contrato as c2 " +
							" join c2.aux_contrato as aux2 " +
							" where aux2.situacao = " +SituacaoContrato.CANCELADO.getValue().toString()+
							" and d2.cddocumento = documento.cddocumento)")
					.or()
					.where(" documento.cddocumento in (select d1.cddocumento " +
							" from Documento d1 " +
							" join d1.listaDocumentoOrigem ldo " +
							" join ldo.contrato c1 " +
							" join c1.aux_contrato aux1 " +
							" where aux1.situacao = " +SituacaoContrato.CANCELADO.getValue().toString()+
							" and d1.cddocumento = documento.cddocumento) ")
				.closeParentheses();
		}
		
		if(filtro.getContagerencial()!=null || filtro.getCentrocusto()!=null){
			query
				.leftOuterJoinIfNotExists("documento.rateio rateio")
				.leftOuterJoinIfNotExists("rateio.listaRateioitem item")
				.where("item.contagerencial = ?", filtro.getContagerencial())
				.where("item.centrocusto = ?", filtro.getCentrocusto());
			
			ignoreJoins.add("rateio");
			ignoreJoins.add("item");
		}
		
		if(filtro.getDtbaixa1() != null || filtro.getDtbaixa2() != null){
			query.openParentheses();
			query.where("documento.cddocumento in (select d.cddocumento " +
														"from Movimentacaoorigem mo " +
														"join mo.documento d " +
														"join mo.movimentacao m " +
														"where m.movimentacaoacao.cdmovimentacaoacao <> 0 " +
														(filtro.getDtbaixa1() != null ? "and coalesce(m.dtbanco, m.dtmovimentacao) >= '" + filtro.getDtbaixa1() + "' " : "") +
														(filtro.getDtbaixa2() != null ? "and coalesce(m.dtbanco, m.dtmovimentacao) <= '" + filtro.getDtbaixa2() + "' " : "") +
														"and d.cddocumento = documento.cddocumento " +
														")");
			query.or();
			query.where("documento.cddocumento in (select d.cddocumento " +
												"from Valecompraorigem vori " +
												"join vori.documento d " +
												"join vori.valecompra v " +
												"where v.tipooperacao.cdtipooperacao = " + Tipooperacao.TIPO_DEBITO.getCdtipooperacao() + " " +
												(filtro.getDtbaixa1() != null ? "and v.data >= '" + filtro.getDtbaixa1() + "' " : "") +
												(filtro.getDtbaixa2() != null ? "and v.data <= '" + filtro.getDtbaixa2() + "' " : "") +
												"and d.cddocumento = documento.cddocumento " +
												")");
			query.or();
			
			String parametro = parametrogeralService.getValorPorNome(Parametrogeral.DATA_PAGAMENTO_ANTECIPACAO);
			parametro = parametro != null ? br.com.linkcom.utils.StringUtils.tiraAcento(parametro).toUpperCase() : "";
			
			if ("ANTECIPACAO".equalsIgnoreCase(parametro)) {
				query.where("documento.cddocumento in (select d.cddocumento " +
														"from HistoricoAntecipacao ha " +
														"join ha.documentoReferencia d " +
														"join ha.documento d2 " +
														"where 1 = 1 " +
														"and d.cddocumento = documento.cddocumento " +
														"and d2.cddocumento in (select d3.cddocumento " +
																				"from Movimentacaoorigem mo " +
																				"join mo.documento d3 " +
																				"join mo.movimentacao m " +
																				"where m.movimentacaoacao.cdmovimentacaoacao <> 0 " +
																				(filtro.getDtbaixa1() != null ? "and coalesce(m.dtbanco, m.dtmovimentacao) >= '" + filtro.getDtbaixa1() + "' " : "") +
																				(filtro.getDtbaixa2() != null ? "and coalesce(m.dtbanco, m.dtmovimentacao) <= '" + filtro.getDtbaixa2() + "' " : "") +
																				"and d3.cddocumento = d2.cddocumento)" +
														")");
			} else if ("BAIXA".equalsIgnoreCase(parametro)) {
				query.where("documento.cddocumento in (select d.cddocumento " +
														"from HistoricoAntecipacao ha " +
														"join ha.documentoReferencia d " +
														"where 1 = 1 " +
														(filtro.getDtbaixa1() != null ? "and ha.dataHora >= '" + filtro.getDtbaixa1() + "' " : "") +
														(filtro.getDtbaixa2() != null ? "and ha.dataHora <= '" + filtro.getDtbaixa2() + "' " : "") +
														"and d.cddocumento = documento.cddocumento " +
														")");
			}
			
			query.closeParentheses();
		}
		
		if(filtro.getDiferencaDiasDe() != null || filtro.getDiferencaDiasAte() != null){
			String diferencaDiasDe = filtro.getDiferencaDiasDe();
			String diferencaDiasAte = filtro.getDiferencaDiasAte();
			if(filtro.getDiferencaDiasAte() != null && filtro.getDiferencaDiasDe() != null){
				if(Integer.parseInt(filtro.getDiferencaDiasDe()) > Integer.parseInt(filtro.getDiferencaDiasAte())){
					diferencaDiasDe = filtro.getDiferencaDiasAte();
					diferencaDiasAte = filtro.getDiferencaDiasDe();
				}
			}
											
			query
				.openParentheses()
					.where("exists(select d.cddocumento " +
														"from Movimentacaoorigem mo " +
														"join mo.documento d " +
														"join mo.movimentacao m " +
														"where m.movimentacaoacao.cdmovimentacaoacao <> 0 " +
														(diferencaDiasDe != null ? "and (coalesce(m.dtbanco, m.dtmovimentacao) - d.dtvencimento) >= " + diferencaDiasDe + " " : "") +
														(diferencaDiasAte != null ? "and (coalesce(m.dtbanco, m.dtmovimentacao) - d.dtvencimento) <= " + diferencaDiasAte + " " : "") +
														"and d.cddocumento = documento.cddocumento " +
														")")
					.or()
					.where("exists(select d.cddocumento " +
														"from Valecompraorigem vori " +
														"join vori.documento d " +
														"join vori.valecompra v " +
														"where v.tipooperacao.cdtipooperacao = " + Tipooperacao.TIPO_DEBITO.getCdtipooperacao() + " " +
														(diferencaDiasDe != null ? "and (v.data - d.dtvencimento) >= " + diferencaDiasDe + " " : "") +
														(diferencaDiasAte != null ? "and (v.data - d.dtvencimento) <= " + diferencaDiasAte + " " : "") +
														"and d.cddocumento = documento.cddocumento " +
														")")
				.closeParentheses();
		}

		if(filtro.getContatipo() != null && filtro.getConta() != null){
			query
				.where("documento.cddocumento in (select d.cddocumento " +
													"from Movimentacaoorigem mo " +
													"join mo.documento d " +
													"join mo.movimentacao m " +
													"join m.movimentacaoacao ma " +
													"join m.conta c " +
													"where c.id = " + filtro.getConta().getCdconta() + " " +
													"and ma.cdmovimentacaoacao <> 0)");
		} else if(filtro.getContatipo() != null){
			query
				.where("documento.cddocumento in (select d.cddocumento " +
													"from Movimentacaoorigem mo " +
													"join mo.documento d " +
													"join mo.movimentacao m " +
													"join m.movimentacaoacao ma " +
													"join m.conta c " +
													"join c.contatipo ct " +
													"where ct.id = " + filtro.getContatipo().getCdcontatipo() + " " +
													"and ma.cdmovimentacaoacao <> 0)");
		}
			
		query
			.where(" (select min(dh.dtaltera) from Documentohistorico dh where dh.documento.cddocumento = documento.cddocumento) >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getDtiniciolancamento()))
			.where(" (select min(dh.dtaltera) from Documentohistorico dh where dh.documento.cddocumento = documento.cddocumento) <= ?", SinedDateUtils.dateToTimestampFinalDia(filtro.getDtfimlancamento()));

		
		if (StringUtils.isBlank(filtro.getOrderBy())) {
			filtro.setAsc(false);
			filtro.setOrderBy("documento.cddocumento desc");
		}
		
		if(filtro.getVendedor() != null && filtro.getVendedor().getCdpessoa() != null){
			query
				.openParentheses()
					.where("exists(select v.cdvenda " +
									" from Venda v " +
									" join v.listadocumentoorigem docori " +
									" join docori.documento docVenda " +
									" join v.colaborador col " +
									" where docVenda.cddocumento = documento.cddocumento " +
									" and col.cdpessoa = " + filtro.getVendedor().getCdpessoa() + " " + 
									")")
					.or()
					.where("exists(select pv.cdpedidovenda " +
									" from Pedidovenda pv " +
									" join pv.listaDocumentoorigem docori " +
									" join docori.documento docPedidovenda " +
									" join pv.colaborador col " +
									" where docPedidovenda.cddocumento = documento.cddocumento " +
									" and col.cdpessoa = " + filtro.getVendedor().getCdpessoa() + " " + 
									")")
					.or()
					.where("exists(select c.cdcontrato " +
									" from Contrato c " +
									" join c.listaDocumentoOrigem docori " +
									" join docori.documento docVenda " +
									" join c.vendedor col " +
									" where docVenda.cddocumento = documento.cddocumento " +
									" and col.cdpessoa = " + filtro.getVendedor().getCdpessoa() + " " + 
									")")
					.or()
					.where("exists(select c.cdcontrato " +
									" from Contrato c " +
									" join c.listaNotacontrato notacontra " +
									" join notacontra.nota notaVenda " +
									" join notaVenda.listaNotaDocumento notadoc" +
									" join notadoc.documento docVenda " +
									" join c.vendedor col " +
									" where docVenda.cddocumento = documento.cddocumento " +
									" and col.cdpessoa = " + filtro.getVendedor().getCdpessoa() + " " + 
									")")
				.closeParentheses();
		}
		
		if(filtro.getMotivocancelamento() != null){
			query
				.where(" exists ( " +
							"select dh.cddocumentohistorico " +
							"from Documentohistorico dh " +
							"join dh.documento doc " +
							"join dh.motivocancelamento mc " +
							"where doc.cddocumento = documento.cddocumento " +
							"and mc.id = " + filtro.getMotivocancelamento().getCdmotivocancelamento() + " " + 
						" )");
		}
		
		if(filtro.getIsRestricaoClienteVendedor() != null && filtro.getIsRestricaoClienteVendedor()){
			Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
			if(colaborador != null){
				query.openParentheses()
					.openParentheses()
						.where("documento.tipopagamento = 0")
						.where("exists ( SELECT c.cdpessoa FROM Cliente c " +
								"LEFT OUTER JOIN c.listaClientevendedor cv " +
								"LEFT OUTER JOIN cv.colaborador col " +
								"where c.cdpessoa = pessoa.cdpessoa and " +
								"(cv is null or col.cdpessoa = " + colaborador.getCdpessoa() + ") )  ")
					.closeParentheses()
					.or()
					.where("documento.tipopagamento <> 0")
				.closeParentheses();
			}
		}
		
		if(filtro.getNumeroNotaFrom() != null || filtro.getNumeroNotaTo() != null || 
				filtro.getNumeroNfseTo() != null || filtro.getNumeroNfseFrom() != null ||
				filtro.getDtemissaonotaInicio()!=null || filtro.getDtemissaonotaFim()!=null) {
			query.leftOuterJoinIfNotExists("documento.listaDocumentoOrigem listaDocumentoOrigem");
			query.leftOuterJoinIfNotExists("listaDocumentoOrigem.venda venda");
			query.leftOuterJoinIfNotExists("venda.listaNotavenda listaNotavenda");
			query.leftOuterJoinIfNotExists("listaNotavenda.nota notavenda");
			query.leftOuterJoinIfNotExists("documento.listaNotaDocumento listaNotaDocumento");
			query.leftOuterJoinIfNotExists("listaNotaDocumento.nota nota");
			
			query.leftOuterJoinIfNotExists("listaDocumentoOrigem.pedidovenda pedidovenda");
			query.leftOuterJoinIfNotExists("pedidovenda.listaVenda vendaPedido");
			query.leftOuterJoinIfNotExists("vendaPedido.listaNotavenda listaNotavendaPedido");
			query.leftOuterJoinIfNotExists("listaNotavendaPedido.nota notavendaPedido");
			
			ignoreJoins.add("listaDocumentoOrigem");
			ignoreJoins.add("venda");
			ignoreJoins.add("listaNotavenda");
			ignoreJoins.add("notavenda");
			
			ignoreJoins.add("pedidovenda");
			ignoreJoins.add("vendaPedido");
			ignoreJoins.add("listaNotavendaPedido");
			ignoreJoins.add("notavendaPedido");
			
			query.openParentheses();
			query.openParentheses();
			query.where("cast(nota.numero AS long) >= ?", filtro.getNumeroNotaFrom());
			query.where("cast(nota.numero AS long) <= ?", filtro.getNumeroNotaTo());
			query.where("nota.dtEmissao>=?", filtro.getDtemissaonotaInicio());
			query.where("nota.dtEmissao<=?", filtro.getDtemissaonotaFim());
			query.closeParentheses();
			query.or();
			query.openParentheses();
			query.where("cast(notavenda.numero AS long) >= ?", filtro.getNumeroNotaFrom());
			query.where("cast(notavenda.numero AS long) <= ?", filtro.getNumeroNotaTo());
			query.where("notavenda.dtEmissao>=?", filtro.getDtemissaonotaInicio());
			query.where("notavenda.dtEmissao<=?", filtro.getDtemissaonotaFim());
			query.closeParentheses();
			query.or();
			query.openParentheses();
			query.where("cast(notavendaPedido.numero AS long) >= ?", filtro.getNumeroNotaFrom());
			query.where("cast(notavendaPedido.numero AS long) <= ?", filtro.getNumeroNotaTo());
			query.where("notavendaPedido.dtEmissao>=?", filtro.getDtemissaonotaInicio());
			query.where("notavendaPedido.dtEmissao<=?", filtro.getDtemissaonotaFim());
			query.closeParentheses();
			query.closeParentheses();
			
			if(filtro.getNumeroNfseTo() != null || filtro.getNumeroNfseFrom() != null){
				query.leftOuterJoin("nota.listaArquivonfnota arquivonfnota");
				
				
				query.where("arquivonfnota.numero_filtro >= ?", filtro.getNumeroNfseFrom());
				query.where("arquivonfnota.numero_filtro <= ?", filtro.getNumeroNfseTo());
			}
		}
		
		if(filtro.getNumeroFaturaFrom()!= null || filtro.getNumeroFaturaTo()!= null) {
			query.leftOuterJoinIfNotExists("documento.listaContratofaturalocacaodocumento listaContratofaturalocacaodocumento");
			query.leftOuterJoinIfNotExists("listaContratofaturalocacaodocumento.contratofaturalocacao contratofaturalocacao");
			query.where("contratofaturalocacao.numero >= ?", filtro.getNumeroFaturaFrom());
			query.where("contratofaturalocacao.numero <= ?", filtro.getNumeroFaturaTo());
		}
		
		if(filtro.getAntecipado() != null){
			query.where("coalesce(documento.antecipado, false) = ?", filtro.getAntecipado());
		}
		
		boolean otimizacao = true;
		try{
			String paramOtimizacao = parametrogeralService.getValorPorNome(Parametrogeral.OTIMIZACAO_LISTAGEM_DOCUMENTO);
			otimizacao = paramOtimizacao != null && paramOtimizacao.trim().equals("TRUE");
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		if(otimizacao){
			this.otimizacaoListagem(query, filtro, pessoaString, pessoaCpf, pessoaCnpj);
		} else {
			if(filtro.getEmpresa() != null){			
				query
					.leftOuterJoinIfNotExists("documento.empresa empresa")
					.where("documento.empresa = ?", filtro.getEmpresa());
				
				ignoreJoins.add("empresa");
			}
			
			query
				.where("documento.documentotipo = ?", filtro.getDocumentotipo())
				.where("documento.tipopagamento=?", filtro.getTipopagamento())
				.where("documento.dtemissao >= ?", filtro.getDtemissao1())
				.where("documento.dtemissao <= ?", filtro.getDtemissao2())
				.where("documento.dtvencimento >= ?", filtro.getDtvencimento1())
				.where("documento.dtvencimento <= ?", filtro.getDtvencimento2())
				.where("documento.valor >= ?", filtro.getValorminimo())
				.where("documento.valor <= ?", filtro.getValormaximo())
				.where("documento.conta = ?", filtro.getContaboleto())
				.where("documento.vinculoProvisionado = ?", filtro.getVinculoProvisionado())
				.where("documento.documentoacaoprotesto = ?", filtro.getDocumentoacaoprotesto())
				.where("documento.cddocumento = ?", filtro.getCddocumento())				
				.openParentheses()
				.whereLikeIgnoreAll("documento.numero", filtro.getDocumentoDescricao())
				.or()
				.whereLikeIgnoreAll("documento.descricao", filtro.getDocumentoDescricao())
				.closeParentheses()
				.where("documento.documentoclasse = ?", filtro.getDocumentoclasse());
				
				if(filtro.getDtcompetencia1() != null || filtro.getDtcompetencia2() != null){
					query
						.openParentheses()
							.openParentheses()
								.where("documento.dtcompetencia >= ?", filtro.getDtcompetencia1())
								.where("documento.dtcompetencia <= ?", filtro.getDtcompetencia2())
							.closeParentheses()
							.or()
							.openParentheses()
								.where("documento.cddocumento in (SELECT da.documento FROM DocumentoApropriacao da WHERE " +
										(filtro.getDtcompetencia1() != null ? "da.mesAno >= '" + SinedDateUtils.toStringPostgre(filtro.getDtcompetencia1()) + "'" : "") +
										(filtro.getDtcompetencia1() != null && filtro.getDtcompetencia2() != null ? " AND " : "") +
										(filtro.getDtcompetencia2() != null ? "da.mesAno <= '" + SinedDateUtils.toStringPostgre(filtro.getDtcompetencia2()) + "'" : "") +
										")")
							.closeParentheses()
						.closeParentheses();
				}else {
					query
						.where("documento.dtcompetencia >= ?", filtro.getDtcompetencia1())
						.where("documento.dtcompetencia <= ?", filtro.getDtcompetencia2());
				}
			
			if (StringUtils.isNotBlank(filtro.getPessoa())) {
				if (filtro.getPessoa().indexOf('?') > 0) {
					throw new IllegalArgumentException("A cl�usula where do QueryBuilder n�o pode ter o caracter '?'. Deve ser passada apenas a express�o que se deseja fazer o like. Veja javadoc!");
				}
				query.openParentheses();
				query.where("UPPER(" + funcaoTiraacento + "(pessoa.nome)) LIKE '%'||'"+pessoaString+"'||'%'");
				query.or();
				query.where("UPPER(" + funcaoTiraacento + "(documento.outrospagamento)) LIKE '%'||'"+pessoaString+"'||'%'");
				query.or();
				if(parametroArrayPessoa){
					query.where("retorna_array_pessoa('"+pessoaString+"', pessoa.cdpessoa )=true");
				}else{
					query.where("subquery_documento_listagem_fornecedor_cliente('"+pessoaString+"', pessoa.cdpessoa)=true"); 
				}
				query.closeParentheses();
			}
			
			List<Documentoacao> listaAcao = filtro.getListaDocumentoacao();
			if (listaAcao != null) {
				query.whereIn("documentoacao.cddocumentoacao", SinedUtil.listAndConcatenate(listaAcao, "cddocumentoacao", ","));
			}
			
			if (!administrador){
				query
					.leftOuterJoinIfNotExists("documento.documentotipo documentotipo")
					.leftOuterJoinIfNotExists("documentotipo.listaDocumentotipopapel listaDocumentotipopapel")
					.leftOuterJoinIfNotExists("documento.vinculoProvisionado vinculoP")
					.leftOuterJoinIfNotExists("vinculoP.listaContapapel listaContapapelVinculoP");
				
				if (whereInPapel != null && !whereInPapel.equals("")){
					query.openParentheses();
					query.whereIn("listaDocumentotipopapel.papel.cdpapel", whereInPapel)
					.or()
					.where("listaDocumentotipopapel.papel.cdpapel is null");
					query.closeParentheses();
					
					query.openParentheses();
					query.whereIn("listaContapapelVinculoP.papel.cdpapel", whereInPapel)
					.or()
					.where("listaContapapelVinculoP.papel.cdpapel is null");
					query.closeParentheses();
				} else {
					query.where("listaDocumentotipopapel.papel.cdpapel is null");
					query.where("listaContapapelVinculoP.papel.cdpapel is null");
				}
			}
		}
		
		addRestricaoClienteEmpresa(query);
		query.setIgnoreJoinPaths(SinedUtil.listToSet(ignoreJoins, String.class));
	}
	
	public void addRestricaoClienteEmpresa(QueryBuilder<Documento> query){
		String whereInEmpresa = new SinedUtil().getListaEmpresa();
		if(SinedUtil.isRestricaoEmpresaClienteUsuarioLogado() && StringUtils.isNotBlank(whereInEmpresa)){
			query.leftOuterJoinIfNotExists("documento.pessoa pessoa");
			query.openParentheses()
				.where("pessoa.cdpessoa is null")
				.or()
				.where("documento.tipopagamento <> ?", Tipopagamento.CLIENTE)
				.or()
				.where("pessoa.cdpessoa in (select c.cdpessoa from Cliente c left outer join c.listaClienteEmpresa ce left outer join ce.empresa e where e.cdpessoa is null or e.cdpessoa in (" + whereInEmpresa + "))")
			.closeParentheses();
		}
	}
	
	public void addRestricaoFornecedorEmpresa(QueryBuilder<Documento> query){
		String whereInEmpresa = new SinedUtil().getListaEmpresa();
		if(StringUtils.isNotBlank(whereInEmpresa) && SinedUtil.isRestricaoEmpresaFornecedorUsuarioLogado()){
			query.leftOuterJoinIfNotExists("documento.pessoa pessoa");
			query.openParentheses()
				.where("pessoa.cdpessoa is null")
				.or()
				.where("documento.tipopagamento <> ?", Tipopagamento.FORNECEDOR)
				.or()
				.where("pessoa.cdpessoa in (select f.cdpessoa from Fornecedor f left outer join f.listaFornecedorempresa fe left outer join fe.empresa e where e.cdpessoa is null or e.cdpessoa in (" + whereInEmpresa + "))")
			.closeParentheses();
		}
	}
	
	private void otimizacaoListagem(QueryBuilder<Documento> query, DocumentoFiltro filtro, String pessoaString, String pessoaCpf, String pessoaCnpj){
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();

		((QueryBuilderSined<Documento>) query).setUseWhereClienteEmpresa(Boolean.FALSE);
		((QueryBuilderSined<Documento>) query).setUseWhereFornecedorEmpresa(Boolean.FALSE);
		
		if (StringUtils.isBlank(filtro.getPessoa())) {
			pessoaString = "null";
		} else{
			pessoaString = "'" + pessoaString + "'";
		}
		
		if (StringUtils.isBlank(pessoaCpf)) {
			pessoaCpf = "null";
		} else{
			pessoaCpf = "'" + pessoaCpf + "'";
		}
		
		if (StringUtils.isBlank(pessoaCnpj)) {
			pessoaCnpj = "null";
		} else{
			pessoaCnpj = "'" + pessoaCnpj + "'";
		}
		
		String documentonumeroString = filtro.getDocumentoDescricao();
		if (StringUtils.isBlank(documentonumeroString)) {
			documentonumeroString = "null";
		} else {
			documentonumeroString = "'" +  documentonumeroString.replaceAll("'", "''") + "'";
		}
		
		String whereInAcao = "null";
		if (filtro.getListaDocumentoacao() != null) {
			whereInAcao = SinedUtil.listAndConcatenate(filtro.getListaDocumentoacao(), "cddocumentoacao", ",");
			if(StringUtils.isBlank(whereInAcao)){
				whereInAcao = "null";
			} else{
				whereInAcao = "'" + whereInAcao + "'";
			}
		}
		
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		if (administrador || StringUtils.isBlank(whereInPapel)){
			whereInPapel = "null";
		} else{
			whereInPapel = "'" + whereInPapel + "'";
		}
		
		String whereInEmpresaPermissao = new SinedUtil().getListaEmpresa();
		if (StringUtils.isBlank(whereInEmpresaPermissao)) {
			whereInEmpresaPermissao = "null";
		} else {
			whereInEmpresaPermissao = "'" + whereInEmpresaPermissao + "'";
		}
		
		String whereInEmpresa = "null";
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null){
			whereInEmpresa = "'" + filtro.getEmpresa().getCdpessoa() + "'";
		}
		
		Integer cddocumento = filtro.getCddocumento();
		Integer cddocumentoclasse = filtro.getDocumentoclasse().getCddocumentoclasse();
		Integer cddocumentotipo = filtro.getDocumentotipo() != null ? filtro.getDocumentotipo().getCddocumentotipo() : null;
		Integer tipopagamento = filtro.getTipopagamento() != null ? filtro.getTipopagamento().ordinal() : null;
		Integer cdconta = filtro.getContaboleto() != null ? filtro.getContaboleto().getCdconta() : null;
		Integer cdvinculoProvisionado = filtro.getVinculoProvisionado() != null ? filtro.getVinculoProvisionado().getCdconta() : null;
		Integer cddocumentoacaoprotesto = filtro.getDocumentoacaoprotesto() != null ? filtro.getDocumentoacaoprotesto().getCddocumentoacao() : null;
		Integer cdcontagerencial = filtro.getContagerencial() != null ? filtro.getContagerencial().getCdcontagerencial() : null;
		Integer cdcentrocusto = filtro.getCentrocusto() != null ? filtro.getCentrocusto().getCdcentrocusto() : null;
		
		String dtemissao1 = "null";
		if(filtro.getDtemissao1() != null) dtemissao1 = "'" + SinedDateUtils.toStringPostgre(filtro.getDtemissao1()) + "'";
		
		String dtemissao2 = "null";
		if(filtro.getDtemissao2() != null) dtemissao2 = "'" + SinedDateUtils.toStringPostgre(filtro.getDtemissao2()) + "'";
		
		String dtvencimento1 = "null";
		if(filtro.getDtvencimento1() != null) dtvencimento1 = "'" + SinedDateUtils.toStringPostgre(filtro.getDtvencimento1()) + "'";
		
		String dtvencimento2 = "null";
		if(filtro.getDtvencimento2() != null) dtvencimento2 = "'" + SinedDateUtils.toStringPostgre(filtro.getDtvencimento2()) + "'";
		
		String dtcompetencia1 = "null";
		if(filtro.getDtcompetencia1() != null) dtcompetencia1 = "'" + SinedDateUtils.toStringPostgre(filtro.getDtcompetencia1()) + "'";
		
		String dtcompetencia2 = "null";
		if(filtro.getDtcompetencia2() != null) dtcompetencia2 = "'" + SinedDateUtils.toStringPostgre(filtro.getDtcompetencia2()) + "'";
		
		String valorminimo = "null";
		if(filtro.getValorminimo() != null) valorminimo = SinedUtil.round(filtro.getValorminimo().getValue().doubleValue() * 100d, 2) + "";
		
		String valormaximo = "null";
		if(filtro.getValormaximo() != null) valormaximo = SinedUtil.round(filtro.getValormaximo().getValue().doubleValue() * 100d, 2) + "";
		
		Integer cdusuario = null;
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(usuario != null && usuario.getCdpessoa() != null){
			cdusuario = usuario.getCdpessoa();
		}
		
		query.where("otimizacao_documento_listagem(" +
				"documento.cddocumento, " +
				cddocumentoclasse + ", " +
				pessoaString + ", " +
				pessoaCpf + ", " +
				pessoaCnpj + ", " +
				whereInAcao + ", " +
				whereInPapel + ", " +
				whereInEmpresa + ", " +
				documentonumeroString + ", " + 
				cddocumento + ", " +
				cddocumentotipo + ", " +
				tipopagamento + ", " +
				dtemissao1 + ", " +
				dtemissao2 + ", " +
				dtvencimento1 + ", " +
				dtvencimento2 + ", " +
				dtcompetencia1 + ", " +
				dtcompetencia2 + ", " +
				valorminimo + ", " +
				valormaximo + ", " +
				cdconta + ", " +
				cdvinculoProvisionado + ", " +
				cddocumentoacaoprotesto + ", " +
				cdcontagerencial + ", " +
				cdcentrocusto + ", " +
				cdusuario + ", " +
				whereInEmpresaPermissao + "" +
				") = true");
	}
	
	public List<Documento> loadForListagem(String whereIn, DocumentoFiltro filtro, String orderBy, boolean asc) {
		QueryBuilder<Documento> query = querySined();
		
		//Ao alterar os campos desse select, favor conferir se o campo (select cli.razaosocial from Cliente cli where cli.cdpessoa = pessoa.cdpessoa)
		//continuar� na posi��o 5. 
		//Caso essa posi��o seja alterada, alterar o valor de 'order="5"' da property <t:property name="cliente.razaosocial" label="Recebimento de" order="5"/>
		//no arquivo contareceberListagem.jsp 
		//E alterar no m�todo updateListagemQuery desta mesma classe
		query
			.select(
					"distinct new br.com.linkcom.sined.geral.bean.Documento(" +
					"documento.cddocumento, " +
					"documento.descricao, " +
					"documento.referencia, " +
					"documento.dtcompetencia, " +
					"(select cli.razaosocial from Cliente cli where cli.cdpessoa = pessoa.cdpessoa), " +
					"documento.dtcartorio, " +
					"trim(coalesce(documento.numero,'')), " +
					"documento.dtdevolucao, " +
					"documento.dtemissao, " + 
					"documento.dtvencimento, " +
					"documento.valor, " +
					"classe.cddocumentoclasse, " +
					"classe.nome, " +
					"documento.outrospagamento, " +
					"documento.dtvencimentooriginal, " + 
					"pessoa.nome, " +
					"pessoa.cdpessoa, " +
					"documentotipo.cddocumentotipo, " +
					"documentotipo.nome, " +
					"documentotipo.antecipacao, " +
					"documentoacao.cddocumentoacao, " +
					"documentoacao.nome, " +
					"aux_documento.valoratual, " +
					"documento.codigobarras, " + 
					"parcela.cdparcela, " +
					"parcela.ordem, " +
					"parcelamento.cdparcelamento, " +
					"parcelamento.iteracoes, " +
					"coalesce(pessoa.nome,documento.outrospagamento), " +
					"(select vw.countv from Vwcountmovimentacaonormal vw where vw.documento = documento), " +
					"documento.rejeitadobanco, " +
					"documentoacaoprotesto.cddocumentoacao, " +
					"documentoacaoprotesto.nome, " +
					"pessoa.tipopessoa, " +
					"documento.dtvencimentoantiga," +
					"documento.cancelamentocobranca," +
					"documento.emissaoboletobloqueado," +
					"documento.tipopagamento,"+
					"(select forn.razaosocial from Fornecedor forn where forn.cdpessoa = pessoa.cdpessoa), " +
					"documento.antecipado," +
					"documento.dtantecipacao)")
					.setUseTranslator(false);
		
		query
			.join("documento.aux_documento aux_documento")
			.join("documento.documentoclasse classe")
			.leftOuterJoin("documento.documentotipo documentotipo")
			.join("documento.documentoacao documentoacao")
			.leftOuterJoin("documento.documentoacaoprotesto documentoacaoprotesto")
			.leftOuterJoin("documento.listaParcela parcela")
			.leftOuterJoin("parcela.parcelamento parcelamento")
			.leftOuterJoin("documento.pessoa pessoa");
		
		SinedUtil.quebraWhereIn("documento.cddocumento", whereIn, query);
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("cotacao.cdcotacao DESC");
		}
		
		return query.list();
	}
	
	/**
	 * Busca uma lista de contas atrasadas.
	 * @param empresa
	 * @param categoria
	 * @param documentoclasse
	 * @return
	 * @author Ramon Brazil
	 * @param contatotipo 
	 * @param filtro 
	 */
	public List<Documento> findByContasAtrasadas (Empresa empresa, Categoria categoria, Contatotipo contatotipo, Date dtvencimento1, Date dtvencimento2, Documentoclasse documentoclasse, MailingDevedorBean filtro){
		QueryBuilder<Documento> queryAtrasadasMailing = getQueryAtrasadasMailling(empresa, contatotipo, categoria, dtvencimento1, dtvencimento2, documentoclasse, filtro);
		SinedUtil.markAsReader();
		return queryAtrasadasMailing.list();
	}
	
	private QueryBuilder<Documento> getQueryAtrasadasMailling(Empresa empresa,
			Contatotipo contatotipo, Categoria categoria, Date dtvencimento1, Date dtvencimento2,
			Documentoclasse documentoclasse, MailingDevedorBean filtro) {
		
		QueryBuilder<Documento> queryAtrasadasMailing = querySined()
				.select("documento.cddocumento, documento.numero, documento.descricao, documento.dtemissao, documento.dtvencimento, documento.valor, documento.dtcartorio, " +
						"pessoa.cdpessoa, pessoa.nome, pessoa.email, conta.cdconta, telefone.cdtelefone, telefone.telefone, documento.dtultimacobranca," +
						"aux_documento.cddocumento, aux_documento.valoratual, banco.corPainelCobranca, listaDocumentohistorico.cddocumentohistorico," +
						"listaDocumentohistorico.dtvencimento, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, documentoacao.cddocumentoacao")
				.join("documento.documentoclasse documentoclasse")
				.join("documento.documentoacao documentoacao")
				.join("documento.pessoa pessoa")
				.join("documento.empresa empresa")
				.join("documento.documentotipo documentotipo")
				.join("documento.aux_documento aux_documento")
				.leftOuterJoin("documento.conta conta")
				.leftOuterJoin("conta.banco banco")
				.leftOuterJoin("pessoa.listaTelefone telefone")
				.leftOuterJoin("documento.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem rateioitem")
				.leftOuterJoin("documento.listaDocumentohistorico listaDocumentohistorico")
				.where("documentoclasse = ?",documentoclasse)
				.where("documento.dtvencimento >= ?", dtvencimento1)
				.where("empresa = ?", empresa)
				.where("documentotipo = ?", filtro.getDocumentotipo())
				.where("pessoa.cdpessoa in (select cliente.cdpessoa from Cliente cliente " +
												"join cliente.listaContato contato " +
												"join contato.contatotipo contatotipo " +
												"where contatotipo = ?)", contatotipo)
				.orderBy("pessoa.nome");
		
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		if (!administrador){
			queryAtrasadasMailing
				.leftOuterJoinIfNotExists("documento.documentotipo documentotipo")
				.leftOuterJoinIfNotExists("documentotipo.listaDocumentotipopapel listaDocumentotipopapel")
				.leftOuterJoinIfNotExists("documento.vinculoProvisionado vinculoP")
				.leftOuterJoinIfNotExists("vinculoP.listaContapapel listaContapapelVinculoP");
			
			if (whereInPapel != null && !whereInPapel.equals("")){
				queryAtrasadasMailing.openParentheses();
				queryAtrasadasMailing.whereIn("listaDocumentotipopapel.papel.cdpapel", whereInPapel)
				.or()
				.where("listaDocumentotipopapel.papel.cdpapel is null");
				queryAtrasadasMailing.closeParentheses();
				
				queryAtrasadasMailing.openParentheses();
				queryAtrasadasMailing.whereIn("listaContapapelVinculoP.papel.cdpapel", whereInPapel)
				.or()
				.where("listaContapapelVinculoP.papel.cdpapel is null");
				queryAtrasadasMailing.closeParentheses();
			} else {
				queryAtrasadasMailing.where("listaDocumentotipopapel.papel.cdpapel is null");
				queryAtrasadasMailing.where("listaContapapelVinculoP.papel.cdpapel is null");
			}
		}
		
		if(filtro.getCentrocusto() != null){
			queryAtrasadasMailing.where("rateioitem.centrocusto = ?", filtro.getCentrocusto());
		}
		
		if(dtvencimento2 == null || SinedDateUtils.beforeOrEqualIgnoreHour(SinedDateUtils.currentDate(), dtvencimento2)){
			queryAtrasadasMailing.where("documento.dtvencimento < ?", SinedDateUtils.currentDate());
		} else {
			queryAtrasadasMailing.where("documento.dtvencimento <= ?", dtvencimento2);
		}
		
		if(categoria != null && categoria.getVcategoria() != null && categoria.getVcategoria().getIdentificador() != null){
			queryAtrasadasMailing
				.where("pessoa.cdpessoa in (select cliente.cdpessoa " +
												"from Cliente cliente " +
												"join cliente.listaPessoacategoria pessoacategoria " +
												"join pessoacategoria.categoria categoria " +
												"join categoria.vcategoria vcategoria " +
												"where vcategoria.identificador like '" + categoria.getVcategoria().getIdentificador() + "%')");
				
			
		}
		
		if(filtro.getCliente() != null){
			queryAtrasadasMailing.where("pessoa = ?", filtro.getCliente());
		}
		
		if(filtro.getMostrar() != null){
			if(filtro.getMostrar().equals(MailingDevedorMostrar.SOMENTE_COBRADOS)){				
				if(filtro.getVisaoMostrar() != null){
					if(filtro.getVisaoMostrar().equals(MailingDevedorVisaoMostrar.DIARIA)){
						queryAtrasadasMailing
						.where("dtultimacobranca = ?", SinedDateUtils.currentDate());
						
					}else if(filtro.getVisaoMostrar().equals(MailingDevedorVisaoMostrar.SEMANAL)){
						queryAtrasadasMailing
						.where("dtultimacobranca >= ?", DateUtils.primeiroDiaSemana())
						.where("dtultimacobranca <= ?", DateUtils.ultimoDiaSemana());
						
					}else if(filtro.getVisaoMostrar().equals(MailingDevedorVisaoMostrar.MENSAL)){
						queryAtrasadasMailing
						.where("dtultimacobranca >= ?", SinedDateUtils.firstDateOfMonth())
						.where("dtultimacobranca <= ?", SinedDateUtils.lastDateOfMonth());
					}
				}else{
					queryAtrasadasMailing
					.where("dtultimacobranca is not null");
				}
			}else{				
				if(filtro.getVisaoMostrar() != null){
					if(filtro.getVisaoMostrar().equals(MailingDevedorVisaoMostrar.DIARIA)){
						queryAtrasadasMailing
						.openParentheses()
						.where("dtultimacobranca <> ?", SinedDateUtils.currentDate())
						.or()
						.where("dtultimacobranca is null")
						.closeParentheses();
						
					}else if(filtro.getVisaoMostrar().equals(MailingDevedorVisaoMostrar.SEMANAL)){
						queryAtrasadasMailing
						.openParentheses()
						.where("dtultimacobranca < ?", DateUtils.primeiroDiaSemana())
						.or()
						.where("dtultimacobranca > ?", DateUtils.ultimoDiaSemana())
						.or()
						.where("dtultimacobranca is null")
						.closeParentheses();
					}else if(filtro.getVisaoMostrar().equals(MailingDevedorVisaoMostrar.MENSAL)){
						queryAtrasadasMailing
						.openParentheses()
						.where("dtultimacobranca < ?", SinedDateUtils.firstDateOfMonth())
						.or()
						.where("dtultimacobranca > ?", SinedDateUtils.lastDateOfMonth())
						.or()
						.where("dtultimacobranca is null")
						.closeParentheses();
					}
				}else{
					queryAtrasadasMailing
					.where("dtultimacobranca is null");
				}
			}
		}
		
		if(filtro.getCampoOrdenacao() != null){
			if(filtro.getCampoOrdenacao().equals(OrdenacaoMailingDevedor.CLIENTE)){
				queryAtrasadasMailing.orderBy("pessoa.nome" + (filtro.getTipoOrdenacao() ? " asc " : " desc "));					
			}else{
				queryAtrasadasMailing.orderBy("documento." + filtro.getCampoOrdenacao().getCampo() + (filtro.getTipoOrdenacao() ? " asc " : " desc "));
			}
		}
		
		if(filtro.getContasProtestadas() != null){
			if(filtro.getContasProtestadas().equals(MailingDevedorContasProtestadas.NAO)){
				queryAtrasadasMailing.where("documento.dtcartorio is null");
			} else if(filtro.getContasProtestadas().equals(MailingDevedorContasProtestadas.SOMENTE)){
				queryAtrasadasMailing.where("documento.dtcartorio is not null");
			}
		}
		if(filtro.getContasBaixadasParcialmente() == null || SimNaoSomenteEnum.SIM.equals(filtro.getContasBaixadasParcialmente())){
			queryAtrasadasMailing
				.openParentheses()
					.where("documentoacao = ?", Documentoacao.DEFINITIVA)
					.or()
					.where("documentoacao = ?", Documentoacao.BAIXADA_PARCIAL)
				.closeParentheses();
		}else if(filtro.getContasBaixadasParcialmente().equals(SimNaoSomenteEnum.NAO)){
			queryAtrasadasMailing.where("documentoacao = ?", Documentoacao.DEFINITIVA);
		} else if(filtro.getContasBaixadasParcialmente().equals(SimNaoSomenteEnum.SOMENTE)){
			queryAtrasadasMailing.where("documentoacao = ?", Documentoacao.BAIXADA_PARCIAL);
		}
		
		if(filtro.getColaborador() != null){
			queryAtrasadasMailing
				.openParentheses()
					.where("exists(select v.cdvenda " +
									" from Venda v " +
									" join v.listadocumentoorigem docori " +
									" join docori.documento docVenda " +
									" join v.colaborador col " +
									" where docVenda.cddocumento = documento.cddocumento " +
									" and col.cdpessoa = " + filtro.getColaborador().getCdpessoa() + " " + 
									")")
					.or()
					.where("exists(select pv.cdpedidovenda " +
									" from Pedidovenda pv " +
									" join pv.listaDocumentoorigem docori " +
									" join docori.documento docPedidovenda " +
									" join pv.colaborador col " +
									" where docPedidovenda.cddocumento = documento.cddocumento " +
									" and col.cdpessoa = " + filtro.getColaborador().getCdpessoa() + " " + 
									")")
				.closeParentheses();
		}
		
		return queryAtrasadasMailing;
	}
	/**
	 * Busca uma lista de contas atrasadas atraves do id do documento
	 * @param empresa
	 * @param categoria
	 * @param documentoclasse
	 * @param wherein
	 * @return
	 * @author Ramon Brazil
	 * @param contatotipo 
	 */
	public List<Documento> findByIdForMaillingDevedor (Empresa empresa, Categoria categoria, Contatotipo contatotipo, Date dtvencimento1, Date dtvencimento2, Documentoclasse documentoclasse, String wherein, MailingDevedorBean filtro){
		QueryBuilder<Documento> query = getQueryAtrasadasMailling(empresa, contatotipo, categoria, dtvencimento1, dtvencimento2, documentoclasse, filtro);
		SinedUtil.quebraWhereIn("documento.cddocumento", wherein, query);
		return query.list();
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Documento> query) {
		query.leftOuterJoinFetch("documento.aux_documento aux_documento");
		query.leftOuterJoinFetch("documento.documentotipo documentotipo");
		query.leftOuterJoinFetch("documentotipo.contadestino contadestino");
		query.leftOuterJoinFetch("documento.documentoacao acao");
		query.leftOuterJoinFetch("documento.documentoacaoprotesto acaoprotesto");
		query.leftOuterJoinFetch("documento.rateio rateio");
		query.leftOuterJoinFetch("rateio.listaRateioitem item");
	}

	/**
	 * Obt�m uma lista de documentos de acordo com o filtro da listagem, sem
	 * restri��o de p�gina. A lista � usada para somar os valores dos documentos
	 * e exibir na listagem.
	 * 
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documento> findForSomaListagem(DocumentoFiltro filtro) {
		QueryBuilder<Documento> query = querySined();
		this.updateListagemQuery(query, filtro);
		boolean rateio = filtro.getContagerencial() != null || filtro.getCentrocusto() != null || filtro.getProjeto() != null; 
		query
			.select("documento.cddocumento, documento.dtemissao, documento.dtvencimento, " +
					"documento.descricao, documentoacao.cddocumentoacao, aux_documento.valoratual, documento.valor " +
					(rateio ? ", rateio.cdrateio, item.cdrateioitem, item.valor, contagerencial.cdcontagerencial, centrocusto.cdcentrocusto, projetoSoma.cdprojeto" : ""))
			.setUseTranslator(true);
		
		if(rateio){
			query
				.leftOuterJoinIfNotExists("documento.rateio rateio")
				.leftOuterJoinIfNotExists("rateio.listaRateioitem item")
				.leftOuterJoin("item.contagerencial contagerencial")
				.leftOuterJoin("item.centrocusto centrocusto")
				.leftOuterJoin("item.projeto projetoSoma");
		}
			
		query.setIgnoreJoinPaths(new HashSet<String>());
			
		return query.list();
	}

	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				Documento bean = (Documento) save.getEntity();
				Taxa taxas = bean.getTaxa();
				if (taxas != null) {
					taxaService.limpaReferenciasTaxa(taxas);
					taxaService.saveOrUpdate(taxas);
				}
				Rateio rateio = bean.getRateio();
				if (rateio != null) {
					rateioService.limpaReferenciaRateio(rateio);
					rateioService.saveOrUpdate(rateio);
				}
				save.saveOrUpdateManaged("listaDocumentohistorico");
				save.saveOrUpdateManaged("listaApropriacao");
				return null;
			}
		});
	}

	/**
	 * M�todo para obter lista de contas a pagar de acordo com o filtro.
	 * 
	 * @param filtro
	 * @return
	 * @author Flavio
	 */
	public List<Documento> findForGerarArquivobancario(
			ArquivobancarioProcessFiltro filtro) {
		return querySined().select("documento.descricao, documento.valor").join(
				"documento.listaDocumentorateio documentorateio").where(
				"documento.documentoclasse.cddocumentoclasse = ?",
				Documentoclasse.CD_PAGAR).where("documentorateio.projeto = ?",
				filtro.getProjeto()).where(
				"documentorateio.contagerencial = ?",
				filtro.getContagerencial()).where(
				"documentorateio.centrocusto = ?", filtro.getCentrocusto())
				.where("documento.dtvencimento>=?", filtro.getDtvenc1()).where(
						"documento.dtvencimento<=?", filtro.getDtvenc2())
				.list();
	}

	/**
	 * M�todo que carrega o documento.
	 * 
	 * @param documento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Documento carregaDocumento(Documento documento) {
		if (documento == null || documento.getCddocumento() == null) {
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		return querySined()
				.select(
						"documentotipo.cddocumentotipo, documentotipo.taxanoprocessamentoextratocartaocredito, documentotipo.percentual, documentotipo.percentual, documentotipo.taxavenda, "+
						"documento.dtemissao, documento.dtvencimento, documento.dtvencimentooriginal, documentotipo.antecipacao,  "
								+ "pessoa.cdpessoa, pessoa.nome, documento.numero, documento.valor, "
								+ "documento.descricao, documento.cddocumento, documento.dtcartorio, documentoacao.cddocumentoacao,"
								+ "classe.cddocumentoclasse, documento.tipopagamento, documento.outrospagamento,"
								+ "dh.cddocumentohistorico, dh.documento, dh.documentoacao, dh.cdusuarioaltera, dh.dtaltera, dh.observacao,"
								+ "dh.documentotipo,dh.descricao, dh.numero, dh.dtemissao, dh.dtvencimento, dh.valor,"
								+ "pessoahistorico.cdpessoa, dh.outrospagamento, dh.tipopagamento, documento.mensagem1, documento.mensagem2, conta.cdconta, "
								+ "tipotaxa.cdtipotaxa")

				.leftOuterJoin("documento.listaDocumentohistorico dh")
				.leftOuterJoin("dh.pessoa pessoahistorico")
				.leftOuterJoin("dh.documentoacao acaohistorico")
				.leftOuterJoin("dh.documentotipo tipohistorico")
				.leftOuterJoin("dh.documento dochistorico")
				.leftOuterJoin("documento.documentotipo documentotipo")
				.leftOuterJoin("documentotipo.tipotaxacontareceber tipotaxa")
				.join("documento.documentoacao documentoacao")
				.join("documento.documentoclasse classe")
				.leftOuterJoin("documento.pessoa pessoa")
				.leftOuterJoin("documento.conta conta")
				.where("documento = ?",
				documento)
				.unique();
	}

	/**
	 * Carrega documentos a partir de um whereIn.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Documento> carregaDocumentos(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Documentos n�o podem ser nulos.");
		}
		return querySined()
				.select(
						"documentotipo.cddocumentotipo, documentotipo.nome, documento.dtemissao, documento.dtvencimento, documento.descricao, "
								+ "pessoa.cdpessoa, pessoa.nome, documento.numero, documento.valor, documento.tipopagamento, documento.outrospagamento,"
								+ "aux_documento.valoratual, documento.dtvencimentooriginal, "
								+ "documento.descricao, documento.cddocumento, documento.dtcartorio, documentoacao.cddocumentoacao, documentoacao.nome, "
								+ "documentoclasse.cddocumentoclasse, documentoacaoprotesto.cddocumentoacao, "
								+ "rateio.cdrateio, item.cdrateioitem, item.valor, item.percentual,"
								+ "cc.cdcentrocusto, cg.cdcontagerencial, proj.cdprojeto, indicecorrecao.cdindicecorrecao, empresa.cdpessoa, "
								+ "endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, municipio.cdmunicipio, "
								+ "endereco.cep, municipio.nome, uf.cduf, uf.sigla, documentoacaoanteriornegociacao.cddocumentoacao,"
								+ "valecompra.valor, tipooperacaovalecompra.cdtipooperacao")

				.join("documento.aux_documento aux_documento")
				.join("documento.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem item")
				.leftOuterJoin("item.centrocusto cc")
				.leftOuterJoin("item.projeto proj")
				.leftOuterJoin("item.contagerencial cg")
				.leftOuterJoin("documento.indicecorrecao indicecorrecao")
				.leftOuterJoin("documento.empresa empresa")
				.leftOuterJoin("documento.documentotipo documentotipo")
				.join("documento.documentoacao documentoacao")
				.join("documento.documentoclasse documentoclasse")
				.leftOuterJoin("documento.pessoa pessoa")
				.leftOuterJoin("documento.documentoacaoprotesto documentoacaoprotesto")
				.leftOuterJoin("documento.endereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("documento.documentoacaoanteriornegociacao documentoacaoanteriornegociacao")
				.leftOuterJoin("documento.listaValecompraorigem listaValecompraorigem")
				.leftOuterJoin("listaValecompraorigem.valecompra valecompra")
				.leftOuterJoin("valecompra.tipooperacao tipooperacaovalecompra")
				.whereIn("documento.cddocumento", whereIn)
				.orderBy("documento.cddocumento")
				.list();
	}
	
	/**
	 * Carrega documento a partir de um whereIn
	 * @param whereIn
	 * @return
	 */
	public List<Documento> loadDocumentos(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Documentos n�o podem ser nulos.");
		}
		return querySined()
		.select(
				" pessoa.cdpessoa, pessoa.nome, documento.tipopagamento, documento.cddocumento, " +
				" documentoacao.cddocumentoacao, dh.cddocumentohistorico, dh.documento, dh.documentoacao, " +
				" dh.cdusuarioaltera, dh.dtaltera, dh.observacao, dh.documentotipo, dh.descricao, " +
				" dh.numero, dh.dtemissao, dh.dtvencimento, dh.valor, " +
				" documentotipo.cddocumentotipo, contadestino.cdconta, contadestino.nome")
				
				.join("documento.documentoacao documentoacao")
				.leftOuterJoin("documento.pessoa pessoa")
				.leftOuterJoin("documento.listaDocumentohistorico dh")
				.leftOuterJoin("documento.documentotipo documentotipo")
				.leftOuterJoin("documentotipo.contadestino contadestino")
				.whereIn("documento.cddocumento", whereIn)
				.list();
	}

	/**
	 * Carrega documentos a partir de um whereIn, para o relat�rio de c�pia de
	 * cheque.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Documento> findDocsCopiaCheque(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Documentos n�o podem ser nulos.");
		}
		return querySined()
				.select(
						"documentotipo.nome, pessoa.nome, documento.cddocumento, documento.numero, documento.dtemissao, "
								+ "documento.dtvencimento, documento.valor, rateio.cdrateio, documento.descricao,"
								+ "documento.tipopagamento, documento.outrospagamento,"
								+ "parcela.ordem, parcelamento.iteracoes, empresa.nome, empresa.razaosocial, empresa.nomefantasia, logomarca.cdarquivo, "
								+ "cheque.cdcheque, cheque.numero, cheque.agencia, cheque.banco, cheque.emitente, "
								+ "cheque.conta, cheque.dtbompara ")

				.leftOuterJoin("documento.pessoa pessoa")
				.leftOuterJoin("documento.empresa empresa")
				.leftOuterJoin("empresa.logomarca logomarca")
				.join("documento.documentotipo documentotipo")
				.leftOuterJoin("documento.rateio rateio")
				.leftOuterJoin("documento.listaParcela parcela")
				.leftOuterJoin("parcela.parcelamento parcelamento")
				.leftOuterJoin("documento.cheque cheque")
				.whereIn("documento.cddocumento", whereIn)
				.orderBy("documento.cddocumento")
				.list();
	}

	/**
	 * M�todo para obter os documentos associados aos outros atrav�s de
	 * parcelas.
	 * 
	 * @param documento
	 * @param parcelamento -
	 *            Par�metro em comum aos documentos associados.
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documento> findDocumentosAssociados(Documento documento,
			Parcelamento parcelamento) {
		if (documento == null)
			throw new SinedException("O par�metro documento n�o pode ser null.");
		return querySined()
				.select(
						"documento.cddocumento,documento.dtvencimento,documento.valor,documento.numero,documentoacao.cddocumentoacao,parcela.ordem, documento.codigobarras ")
				.join("documento.listaParcela parcela").join(
						"parcela.parcelamento parcelamento").join(
						"documento.documentoacao documentoacao").where(
						"parcelamento = ?", parcelamento).where(
						"documento <> ?", documento).orderBy(
						"documento.dtvencimento").list();
	}

	/**
	 * M�todo para atualizar o <code>Documentoacao</code> de um
	 * <code>Documento</code> com a acao que est� no pr�prio bean.
	 * 
	 * @param documento
	 * @author Fl�vio Tavares
	 */
	public void updateDocumentoacao(Documento documento) {
		if (documento == null || documento.getCddocumento() == null
				|| documento.getDocumentoacao() == null
				|| documento.getDocumentoacao().getCddocumentoacao() == null) {
			throw new SinedException(
					"Par�metros inv�lidos. "
							+ "Os campos documento,cddocumento,documentoacao ou cddocumentoacao n�o podem ser nulos.");
		}
		String sql = "update documento " + "set cddocumentoacao = ? "
				+ "where cddocumento = ?";
		Object[] fields = new Object[] {
				documento.getDocumentoacao().getCddocumentoacao(),
				documento.getCddocumento() };
		getJdbcTemplate().update(sql, fields);
	}

	/**
	 * M�todo para atualizar o valor de um documento.
	 * 
	 * @param documento
	 * @author Fl�vio Tavares
	 */
	public void updateValorDocumento(Documento documento) {
		if (documento == null || documento.getCddocumento() == null) {
			throw new SinedException("Par�metros inv�lidos.");
		}
		if (SinedUtil.isPropertyRequired(Documento.class, "valor")
				&& documento.getValor() == null) {
			throw new SinedException("O par�metro valor n�o pode ser null.");
		}

		String sql = null;
		Object[] objetos = null;
		if(documento.getDescricao() != null && !documento.getDescricao().equals("")){
			sql = "update Documento d set d.valor = ?, d.descricao = ? where d.id = ?";
			objetos = new Object[] { documento.getValor(), documento.getDescricao(), documento.getCddocumento() };
		}else{
			sql = "update Documento d set d.valor = ? where d.id = ?";
			objetos = new Object[] { documento.getValor(),	documento.getCddocumento() };
		}
		getHibernateTemplate().bulkUpdate(sql, objetos);
	}
	
	/**
	 * M�todo para atualizar o nosso n�mero de um documento.
	 * 
	 * @param documento
	 * @author Marden Silva
	 */
	public void updateNossoNumero(Documento documento) {
		if (documento == null || documento.getCddocumento() == null) {
			throw new SinedException("Par�metros inv�lidos.");
		}
		if (documento.getNossonumero() == null || documento.getNossonumero().equals("")) {
			throw new SinedException("O par�metro nosso n�mero n�o pode ser null.");
		}

		String sql = null;
		Object[] objetos = null;
		sql = "update Documento d set d.nossonumero = ? where d.id = ?";
		objetos = new Object[] { documento.getNossonumero(), documento.getCddocumento() };
		getHibernateTemplate().bulkUpdate(sql, objetos);
	}
	
	public void updateAssociadomanualmente(Documento documento) {
		if (documento != null && documento.getCddocumento() != null) {
			String sql = "update Documento d set d.associadomanualmente = " + (Boolean.TRUE.equals(documento.getAssociadomanualmente()) ? "true" : "null") + " where d.id = ?";
			getHibernateTemplate().bulkUpdate(sql, new Object[] {documento.getCddocumento() });
		}
	}

	/**
	 * M�todo para carregar o historico de um documento dentro do pr�prio bean.
	 * � utilizado para modificar e atualizar a lista de hitorico do documento.
	 * 
	 * @param documento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Documento carregaDocumentohistorico(Documento documento) {
		if (documento == null || documento.getCddocumento() == null) {
			throw new SinedException(
					"O par�metro documento ou cddocumento n�o pode ser null.");
		}
		return querySined().join("documento.documentoacao documentoacao")
				.leftOuterJoinFetch(
						"documento.listaDocumentohistorico historico").where(
						"documento = ?", documento).unique();
	}

	/**
	 * Carrega o documento com o juros preenchido.
	 * 
	 * @param documento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Documento carregaJurosDocumento(Documento documento) {
		if (documento == null || documento.getCddocumento() == null) {
			throw new SinedException(
					"O par�metro documento ou cddocumento n�o pode ser null.");
		}
		// return query()
		// .select("documento.cddocumento, documento.dtvencimento,
		// juros.cdjuros, juros.desconto, juros.multa, " +
		// "juros.juros, juros.desagio, juros.dtlimitedesconto, " +
		// "juros.dtlimitemulta, juros.dtlimitedesagio, juros.dtlimitejuros,
		// listaDocumentoautori.valor")
		// .leftOuterJoin("documento.juros juros")
		// .join("documento.listaDocumentoautori listaDocumentoautori")
		// .where("documento = ?", documento)
		// .unique();
		/**
		 * Alterei o sql pq mudou a organiza��o das tabelas referente �s taxas
		 * (antiga tabela Juros).
		 * 
		 * @author Fl�vio
		 */
		return querySined()
				.select(
						"documento.cddocumento, documento.dtvencimento, documento.valor, listaDocumentoautori.valor,"
								+ "taxa.cdtaxa, tipotaxa.cdtipotaxa, tipotaxa.nome,ti.percentual,ti.valor,ti.dtlimite, ti.motivo")
				.leftOuterJoin("documento.taxa taxa").leftOuterJoin(
						"taxa.listaTaxaitem ti").leftOuterJoin(
						"ti.tipotaxa tipotaxa").leftOuterJoin(
						"documento.listaDocumentoautori listaDocumentoautori")
				.where("documento = ?", documento).unique();
	}

	/**
	 * Faz o update do bean taxa de documento.
	 * 
	 * @param documento
	 * @author Rodrigo Freitas
	 */
	public void updateTaxa(Documento documento) {
		if (documento == null || documento.getCddocumento() == null) {
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Documento d set d.taxa = ? where d.id = ?", new Object[] { documento.getTaxa(), documento.getCddocumento()});
	}
	
	public void updateRateio(Documento documento) {
		if (documento == null || documento.getCddocumento() == null) {
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Documento d set d.rateio = ? where d.id = ?", new Object[] { documento.getRateio(), documento.getCddocumento()});
	}

	/**
	 * M�todo para obter lista de contas a pagar/receber para gera��o do
	 * relat�rio de fluxo de caixa.
	 * 
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documento> findForReportFluxocaixa(FluxocaixaFiltroReport filtro) {
		List<ItemDetalhe> listaCentrocusto = filtro.getListaCentrocusto();
		List<ItemDetalhe> listaProjetotipo = filtro.getListaProjetotipo();
		List<ItemDetalhe> listaEmpresa = filtro.getListaEmpresa();

//		boolean protPagar = false, protReceber = false;
		List<Documentoacao> listaPagar = filtro.getListaAcoesPagar();
//		if (SinedUtil.isListNotEmpty(listaPagar)
//				&& listaPagar.contains(Documentoacao.PROTESTADA)) {
//			listaPagar.remove(Documentoacao.PROTESTADA);
//			protPagar = true;
//		}
		List<Documentoacao> listaReceber = filtro.getListaAcoesReceber();
//		if (SinedUtil.isListNotEmpty(listaReceber)
//				&& listaReceber.contains(Documentoacao.PROTESTADA)) {
//			listaReceber.remove(Documentoacao.PROTESTADA);
//			protReceber = true;
//		}

		QueryBuilder<Documento> q = querySined()
				.select(
						"documento.cddocumento,documento.numero,documento.dtvencimento,documento.valor,documento.descricao,aux_documento.valoratual,"
								+ "documento.outrospagamento, documento.tipopagamento,"
								+ "acao.cddocumentoacao,acao.nome,classe.cddocumentoclasse,"
								+ "pessoa.cdpessoa,pessoa.nome, projeto.cdprojeto, projeto.nome, rateioitem.valor, "
								+ "contagerencial.cdcontagerencial, contagerencial.nome, centrocusto.cdcentrocusto, documentotipo.diasreceber, "
								+ "documentotipo.cartao, cheque.dtbompara ")

				.join("documento.aux_documento aux_documento")
				.join("documento.documentoacao acao")
				.join("documento.documentoclasse classe")
				.leftOuterJoin("documento.conta conta")
				.leftOuterJoin("conta.contatipo contatipo")
				.leftOuterJoin("documento.vinculoProvisionado vinculoProvisionado")
				.leftOuterJoin("vinculoProvisionado.contatipo vinculoProvisionadoContatipo")
				.leftOuterJoin("documento.pessoa pessoa")
				.leftOuterJoin("documento.empresa empresa")
				.leftOuterJoin("documento.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem rateioitem")
				.leftOuterJoin("rateioitem.contagerencial contagerencial")
				.leftOuterJoin("rateioitem.projeto projeto")
				.leftOuterJoin("projeto.projetotipo projetotipo")
				.leftOuterJoin("rateioitem.centrocusto centrocusto")
				.leftOuterJoin("documento.documentotipo documentotipo")
				.leftOuterJoin("documento.cheque cheque");

		/**
		 * Geral
		 */
		if(filtro.getMostrarContasInativadas() == null || !filtro.getMostrarContasInativadas()){
			q.openParentheses();
				q.where("vinculoProvisionado is null");
				q.or();
				q.openParentheses();
					q.openParentheses();
						q.where("vinculoProvisionadoContatipo = ?", Contatipo.TIPO_CONTA_BANCARIA);
						q.where("vinculoProvisionado.ativo = true");
					q.closeParentheses();
					q.or();
					q.where("vinculoProvisionadoContatipo is null");
					q.or();
					q.where("vinculoProvisionadoContatipo <> ?", Contatipo.TIPO_CONTA_BANCARIA);
				q.closeParentheses();
			q.closeParentheses();
		}
		
		q.openParentheses();
		q.where("acao <> ?", Documentoacao.BAIXADA);
		q.where("acao <> ?", Documentoacao.BAIXADA_PARCIAL);
		q.where("acao <> ?", Documentoacao.CANCELADA);
		q.where("acao <> ?", Documentoacao.NAO_AUTORIZADA);
		q.where("acao <> ?", Documentoacao.DEVOLVIDA);
		q.where("acao <> ?", Documentoacao.NEGOCIADA);
		q.closeParentheses();

		Boolean listaCaixa = filtro.getListaCaixa() != null && !filtro.getListaCaixa().isEmpty();
		Boolean listaCartao = filtro.getListaCartao() != null && !filtro.getListaCartao().isEmpty();
		Boolean listaConta = filtro.getListaContabancaria() != null && !filtro.getListaContabancaria().isEmpty();
		if(listaCaixa || listaCartao || listaConta) {
			q.openParentheses();
				if(filtro.getRadVinculoprovisionado() != null){
					if(listaCaixa) {
						q.whereIn("documento.vinculoProvisionado", CollectionsUtil.listAndConcatenate(filtro.getListaCaixa(), "conta.cdconta", ","));
						q.or();
					}else if(filtro.getRadVinculoprovisionado() != null && Boolean.TRUE.equals(filtro.getRadCaixa())){
						q.where("vinculoProvisionadoContatipo = ?", Contatipo.TIPO_CAIXA);
						q.or();
					}
					if(listaCartao) {
						q.whereIn("documento.vinculoProvisionado", CollectionsUtil.listAndConcatenate(filtro.getListaCartao(), "conta.cdconta", ","));
						q.or();
					}else if(filtro.getRadVinculoprovisionado() != null && Boolean.TRUE.equals(filtro.getRadCartao())){
						q.where("vinculoProvisionadoContatipo = ?", Contatipo.TIPO_CARTAO_CREDITO);
						q.or();
					}
					if(listaConta) {
						q.whereIn("documento.vinculoProvisionado", CollectionsUtil.listAndConcatenate(filtro.getListaContabancaria(), "conta.cdconta", ","));
						q.or();
					}else if(filtro.getRadVinculoprovisionado() != null && Boolean.TRUE.equals(filtro.getRadContbanc())){
						q.where("vinculoProvisionadoContatipo = ?", Contatipo.TIPO_CONTA_BANCARIA);
						q.or();
					}
				}
				if(filtro.getRadVinculoprovisionado() == null){
					q.where("documento.vinculoProvisionado is null");
				}
			q.closeParentheses();
		}else if(Boolean.FALSE.equals(filtro.getRadVinculoprovisionado())){
			q.where("documento.vinculoProvisionado is not null");
			if(filtro.getRadCaixa() == null){
				q.where("vinculoProvisionadoContatipo <> ?", Contatipo.TIPO_CAIXA);
			}
			if(filtro.getRadCartao() == null){
				q.where("vinculoProvisionadoContatipo <> ?", Contatipo.TIPO_CARTAO_CREDITO);
			}
			if(filtro.getRadContbanc() == null){
				q.where("vinculoProvisionadoContatipo <> ?", Contatipo.TIPO_CONTA_BANCARIA);
			}
		}else if(filtro.getRadVinculoprovisionado() == null){
			q.where("documento.vinculoProvisionado is null");
		}else if(Boolean.TRUE.equals(filtro.getRadVinculoprovisionado())){
			q.openParentheses();
				q.where("documento.vinculoProvisionado is null");
				q.or();
				q.openParentheses();
					if(filtro.getRadCaixa() == null){
						q.where("vinculoProvisionadoContatipo <> ?", Contatipo.TIPO_CAIXA);
					}
					if(filtro.getRadCartao() == null){
						q.where("vinculoProvisionadoContatipo <> ?", Contatipo.TIPO_CARTAO_CREDITO);
					}
					if(filtro.getRadContbanc() == null){
						q.where("vinculoProvisionadoContatipo <> ?", Contatipo.TIPO_CONTA_BANCARIA);
					}
				q.closeParentheses();
			q.closeParentheses();
		}

		/**
		 * Outros
		 */
		q.openParentheses();
		// WHEREIN LISTA DE CENTRO DE CUSTO
		if (listaCentrocusto != null && listaCentrocusto.size() > 0) {
			q.whereIn("centrocusto.cdcentrocusto", CollectionsUtil
					.listAndConcatenate(listaCentrocusto,
							"centrocusto.cdcentrocusto", ","));
		}
		
		// WHEREIN LISTA DE TIPO DE PROJETO
		if (listaProjetotipo != null && listaProjetotipo.size() > 0) {
			q.whereIn("projetotipo.cdprojetotipo", CollectionsUtil.listAndConcatenate(listaProjetotipo, "projetotipo.cdprojetotipo", ","));
		}

		// WHERE IN NA LISTA DE PROJETO
		if (filtro.getRadProjeto() != null) {
			String idProjeto = String.valueOf(filtro.getRadProjeto().getId());
			if (idProjeto.equals("escolherProjeto") && filtro.getListaProjeto() != null && !filtro.getListaProjeto().isEmpty()) {
				q.whereIn("projeto.cdprojeto", CollectionsUtil.listAndConcatenate(filtro.getListaProjeto(),"projeto.cdprojeto", ","));
			} else if (idProjeto.equals("comProjeto")) {
				q.where("projeto is not null");
			} else if (filtro.getRadProjeto().getId().equals("semProjeto")) {
				q.where("projeto is null");
			}
		}

		// WHEREIN LISTA DE EMPRESAS
		if (SinedUtil.isListNotEmpty(listaEmpresa)) {
			q.whereIn("empresa.cdpessoa", CollectionsUtil.listAndConcatenate(listaEmpresa, "empresa.cdpessoa", ","));
		}

		String whereVencimento = filtro.isFiltroOrigem() ? "coalesce(cheque.dtbompara, documento.dtvencimento) + coalesce(documentotipo.diasreceber, 0)" : "documento.dtvencimento + coalesce(documentotipo.diasreceber, 0)";
		
		q.openParentheses();
		/**
		 * Conta a PAGAR
		 */
		q.where("classe = ?", Documentoclasse.OBJ_PAGAR);
		q.where(whereVencimento + " >= ?", filtro.getPeriodoDe());
		q.where(whereVencimento + " <= ?", filtro.getPeriodoAte());

		// CONTAS PROTESTADAS
		/*if (protPagar)
			q.where("documento.dtcartorio is not null");
		// CONTAS N�O PROTESTADAS
		else
			q.where("documento.dtcartorio is null");*/

		// WHEREIN LISTA DE A��ES
		if (SinedUtil.isListNotEmpty(listaPagar)) {
			q.whereIn("acao.cddocumentoacao", CollectionsUtil
					.listAndConcatenate(listaPagar, "cddocumentoacao", ","));
		} else {
			q.where("acao.cddocumentoacao not in (0,1,2,3,4,5,6,7)");
		}
		// WHEREIN LISTA DE FORNECEDORES
		if (SinedUtil.isListNotEmpty(filtro.getListaFornecedor())) {
			q.whereIn("pessoa.cdpessoa", CollectionsUtil.listAndConcatenate(filtro.getListaFornecedor(), "fornecedor.cdpessoa", ","));
		}

		q.or();

		/**
		 * Conta a RECEBER
		 */
		q.where("classe = ?", Documentoclasse.OBJ_RECEBER);
		q.where(whereVencimento + " >= ?", SinedDateUtils.addDiasData(filtro.getPeriodoDe(), -3));
		q.where(whereVencimento + " <= ?", SinedDateUtils.addDiasData(filtro.getPeriodoAte(), 3));

		// CONTAS PROTESTADAS
		/*if (protReceber)
			q.where("documento.dtcartorio is not null");
		// CONTAS N�O PROTESTADAS
		else
			q.where("documento.dtcartorio is null");*/
		
		//De acordo com a an�lise:
		//Somente = somente contas protestadas
		//Sim = vai exibir 'todas' contas protestadas e sem protesto
		//N�o = n�o vai exibir as contas protestadas...somente as que est�o normais
		Boolean exibirContaProtestada = filtro.getExibirContaProtestada();
		if (Boolean.TRUE.equals(exibirContaProtestada)){ //Somente
			q.where("documento.dtcartorio is not null");
		}else if (exibirContaProtestada==null){ //N�o
			q.where("documento.dtcartorio is null");
		}
		
		// WHEREIN LISTA DE A��ES
		if (SinedUtil.isListNotEmpty(listaReceber)) {
			q.whereIn("acao.cddocumentoacao", CollectionsUtil
					.listAndConcatenate(listaReceber, "cddocumentoacao", ","));
		} else {
			q.where("acao.cddocumentoacao not in (0,1,2,3,4,5,6,7)");
		}
		// WHEREIN LISTA DE CLIENTES
		if (SinedUtil.isListNotEmpty(filtro.getListaCliente())) {
			q.whereIn("pessoa.cdpessoa", CollectionsUtil.listAndConcatenate(
					filtro.getListaCliente(), "cliente.cdpessoa", ","));
		}
		
		q.closeParentheses();

		q.closeParentheses();
		
//		WHEREIN PARA A LISTA DE NATUREZA DA CONTA GERENCIAL
		q.whereIn("contagerencial.natureza", NaturezaContagerencial.listAndConcatenate(filtro.getListanaturezaContaGerencial()));

		Empresa empresa = empresaService.loadPrincipal();
		List<Documento> listaDocumento = new ArrayList<Documento>();
		Iterator<Documento> iterator = q.list().iterator();
		while (iterator.hasNext()) {
			Documento documento = (Documento) iterator.next();
			if(Documentoclasse.OBJ_PAGAR.equals(documento.getDocumentoclasse())){
				listaDocumento.add(documento);
			}else{
				documento.alterarDtvencimentoFluxoCaixa(empresa, null, filtro.isFiltroOrigem());
				if(documento.getDtvencimentoFluxoCaixa(filtro.isFiltroOrigem()) != null && SinedDateUtils.afterOrEqualsIgnoreHour(documento.getDtvencimentoFluxoCaixa(filtro.isFiltroOrigem()), filtro.getPeriodoDe()) && 
						SinedDateUtils.beforeOrEqualIgnoreHour(documento.getDtvencimentoFluxoCaixa(filtro.isFiltroOrigem()), filtro.getPeriodoAte())){
					listaDocumento.add(documento);
				}
			}
			iterator.remove();
		}
		
		return listaDocumento;
	}

	/**
	 * M�todo para obter as contas a pagar/receber atrasadas e em aberto.
	 * 
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documento> findAnterioresForFluxocaixa(
			FluxocaixaFiltroReport filtro) {
		String whereVencimento = filtro.isFiltroOrigem() ? "coalesce(cheque.dtbompara, documento.dtvencimento) " : " documento.dtvencimento ";
		
		List<ItemDetalhe> listaEmpresa = filtro.getListaEmpresa();

		boolean emAberto, emAtraso;
		Date de = SinedDateUtils.addDiasData(filtro.getPeriodoDe(), 3);

		QueryBuilderSined<Documento> q = querySined();
				q
				.select(
						"documento.cddocumento, documento.dtvencimento, documento.valor, documento.descricao, documento.numero, aux_documento.valoratual,"
								+ "documento.outrospagamento, documento.tipopagamento,"
								+ "acao.cddocumentoacao, acao.nome, classe.cddocumentoclasse,"
								+ "pessoa.cdpessoa, pessoa.nome, projeto.cdprojeto, projeto.nome, rateioitem.valor, "
								+ "contagerencial.cdcontagerencial, contagerencial.nome, centrocusto.cdcentrocusto, "
								+ "conta.cdconta, contatipo.cdcontatipo, cheque.dtbompara, "
								+ "vinculoProvisionado.cdconta, vinculoProvisionadoContatipo.cdcontatipo, documentotipo.diasreceber, "
								+ "documentotipo.cartao, cheque.dtbompara ")
				
				.join("documento.aux_documento aux_documento")
				.join("documento.documentoacao acao")
				.join("documento.documentoclasse classe")
				.leftOuterJoin("documento.vinculoProvisionado vinculoProvisionado")
				.leftOuterJoin("vinculoProvisionado.contatipo vinculoProvisionadoContatipo")
				.leftOuterJoin("documento.conta conta")
				.leftOuterJoin("conta.contatipo contatipo")
				.leftOuterJoin("documento.pessoa pessoa")
				.leftOuterJoin("documento.empresa empresa")
				.leftOuterJoin("documento.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem rateioitem")
				.leftOuterJoin("rateioitem.contagerencial contagerencial")
				.leftOuterJoin("rateioitem.projeto projeto")
				.leftOuterJoin("projeto.projetotipo projetotipo")
				.leftOuterJoin("rateioitem.centrocusto centrocusto")
				.leftOuterJoin("documento.documentotipo documentotipo")
				.leftOuterJoin("documento.cheque cheque");

		/**
		 * Geral
		 */
		if(filtro.getMostrarContasInativadas() == null || !filtro.getMostrarContasInativadas()){
			q.openParentheses();
				q.where("vinculoProvisionado is null");
				q.or();
				q.openParentheses();
					q.openParentheses();
						q.where("vinculoProvisionadoContatipo = ?", Contatipo.TIPO_CONTA_BANCARIA);
						q.where("vinculoProvisionado.ativo = true");
					q.closeParentheses();
					q.or();
					q.where("vinculoProvisionadoContatipo is null");
					q.or();
					q.where("vinculoProvisionadoContatipo <> ?", Contatipo.TIPO_CONTA_BANCARIA);
				q.closeParentheses();
			q.closeParentheses();
		}
		
		q.openParentheses();
		q.where("acao <> ?", Documentoacao.BAIXADA);
		q.where("acao <> ?", Documentoacao.BAIXADA_PARCIAL);
		q.where("acao <> ?", Documentoacao.CANCELADA);
		q.where("acao <> ?", Documentoacao.NAO_AUTORIZADA);
		q.where("acao <> ?", Documentoacao.DEVOLVIDA);
		q.where("acao <> ?", Documentoacao.NEGOCIADA);
		q.closeParentheses();

		// WHEREIN LISTA DE EMPRESAS
		if (SinedUtil.isListNotEmpty(listaEmpresa)) {
			q.whereIn("empresa.cdpessoa", CollectionsUtil.listAndConcatenate(listaEmpresa, "empresa.cdpessoa", ","));
		}

		q.openParentheses();
		q.openParentheses();
			emAberto = Util.booleans.isTrue(filtro.getLancContareceberAberto());
			emAtraso = Util.booleans.isTrue(filtro.getLancContareceberAtraso());
	
			/**
			 * Contas a receber
			 */
			q.where("classe = ?", Documentoclasse.OBJ_RECEBER);
			// WHEREIN LISTA DE CLIENTES
			if (SinedUtil.isListNotEmpty(filtro.getListaCliente())) {
				q.whereIn("pessoa.cdpessoa", CollectionsUtil.listAndConcatenate(filtro.getListaCliente(), "cliente.cdpessoa", ","));
			}
			q.openParentheses();
				if (emAberto) {
					q.where(whereVencimento + " + coalesce(documentotipo.diasreceber, 0) >= ?", SinedDateUtils.addDiasData(SinedDateUtils.currentDate(), -3));
					q.where(whereVencimento + " + coalesce(documentotipo.diasreceber, 0) < ?", de);
				} else {
					q.where("0=1");
				}
				q.or();
				if (emAtraso) {
					q.where(whereVencimento + " + coalesce(documentotipo.diasreceber, 0) < ?", SinedDateUtils.addDiasData(SinedDateUtils.currentDate(), 3));
					q.where(whereVencimento + " + coalesce(documentotipo.diasreceber, 0) < ?", de);
				} else {
					q.where("0=1");
				}
			q.closeParentheses();
		
		q.closeParentheses();
		q.or();
		q.openParentheses();
			emAberto = Util.booleans.isTrue(filtro.getLancContapagarAberto());
			emAtraso = Util.booleans.isTrue(filtro.getLancContapagarAtraso());
	
			/**
			 * Contas a pagar
			 */
			q.where("classe = ?", Documentoclasse.OBJ_PAGAR);
			// WHEREIN LISTA DE FORNECEDORES
			if (SinedUtil.isListNotEmpty(filtro.getListaFornecedor())) {
				q.whereIn("pessoa.cdpessoa", CollectionsUtil.listAndConcatenate(filtro.getListaFornecedor(), "fornecedor.cdpessoa", ","));
			}
			q.openParentheses();
			if (emAberto) {
				q
						.where(whereVencimento + " + coalesce(documentotipo.diasreceber, 0) >= ?", SinedDateUtils.addDiasData(SinedDateUtils
								.currentDate(), -3));
				q.where(whereVencimento + " + coalesce(documentotipo.diasreceber, 0) < ?", de);
			} else {
				q.where("0=1");
			}
			q.or();
			if (emAtraso) {
				q.where(whereVencimento + " + coalesce(documentotipo.diasreceber, 0) < ?", SinedDateUtils.addDiasData(SinedDateUtils.currentDate(), 3));
				q.where(whereVencimento + " + coalesce(documentotipo.diasreceber, 0) < ?", de);
			} else {
				q.where("0=1");
			}
			q.closeParentheses();
		q.closeParentheses();
		q.closeParentheses();
		
		// WHEREIN LISTA DE CENTRO DE CUSTO
		List<ItemDetalhe> listaCentrocusto = filtro.getListaCentrocusto();
		if (listaCentrocusto != null && listaCentrocusto.size() > 0) {
			q.whereIn("centrocusto.cdcentrocusto", CollectionsUtil.listAndConcatenate(listaCentrocusto, "centrocusto.cdcentrocusto", ","));
		}
		
		// WHEREIN LISTA DE TIPO DE PROJETO
		List<ItemDetalhe> listaProjetotipo = filtro.getListaProjetotipo();
		if (listaProjetotipo != null && listaProjetotipo.size() > 0) {
			q.whereIn("projetotipo.cdprojetotipo", CollectionsUtil.listAndConcatenate(listaProjetotipo, "projetotipo.cdprojetotipo", ","));
		}

		// WHERE IN NA LISTA DE PROJETO
		if (filtro.getRadProjeto() != null) {
			String idProjeto = String.valueOf(filtro.getRadProjeto().getId());
			if (idProjeto.equals("escolherProjeto") && filtro.getListaProjeto() != null && !filtro.getListaProjeto().isEmpty()) {
				q.whereIn("projeto.cdprojeto", CollectionsUtil.listAndConcatenate(filtro.getListaProjeto(),"projeto.cdprojeto", ","));
			} else if (idProjeto.equals("comProjeto")) {
				q.where("projeto is not null");
			} else if (filtro.getRadProjeto().getId().equals("semProjeto")) {
				q.where("projeto is null");
			}
		}
		
		
		Boolean listaCaixa = filtro.getListaCaixa() != null && !filtro.getListaCaixa().isEmpty();
		Boolean listaCartao = filtro.getListaCartao() != null && !filtro.getListaCartao().isEmpty();
		Boolean listaConta = filtro.getListaContabancaria() != null && !filtro.getListaContabancaria().isEmpty();
		if( listaCaixa || listaCartao || listaConta) {
			q.openParentheses();
				if(listaCaixa) {
					q.whereIn("documento.vinculoProvisionado", CollectionsUtil.listAndConcatenate(filtro.getListaCaixa(), "conta.cdconta", ","));
					q.or();
				}
				if(listaCartao) {
					q.whereIn("documento.vinculoProvisionado", CollectionsUtil.listAndConcatenate(filtro.getListaCartao(), "conta.cdconta", ","));
					q.or();
				}
				if(listaConta) {
					q.whereIn("documento.vinculoProvisionado", CollectionsUtil.listAndConcatenate(filtro.getListaContabancaria(), "conta.cdconta", ","));
					q.or();
				}
				q.where("documento.vinculoProvisionado is null");
			q.closeParentheses();
		}
		q.whereIn("contagerencial.natureza", NaturezaContagerencial.listAndConcatenate(filtro.getListanaturezaContaGerencial()));
		
		Empresa empresa = empresaService.loadPrincipal();
		List<Documento> listaDocumento = new ArrayList<Documento>();
		Iterator<Documento> iterator = q.list().iterator();
		HashMap<String, List<Calendario>> mapCalendario = new HashMap<String, List<Calendario>>();
		while (iterator.hasNext()) {
			Documento documento = (Documento) iterator.next();
			documento.alterarDtvencimentoFluxoCaixa(empresa, mapCalendario, filtro.isFiltroOrigem());
			if(documento.getDtvencimentoFluxoCaixa(filtro.isFiltroOrigem()) != null) {
				if(Documentoclasse.OBJ_RECEBER.equals(documento.getDocumentoclasse())){
					if((Util.booleans.isTrue(filtro.getLancContareceberAberto()) && 
							SinedDateUtils.afterOrEqualsIgnoreHour(SinedDateUtils.currentDate(), documento.getDtvencimentoFluxoCaixa(filtro.isFiltroOrigem())) && 
							SinedDateUtils.beforeIgnoreHour(filtro.getPeriodoDe(), documento.getDtvencimentoFluxoCaixa(filtro.isFiltroOrigem()))) ||
						(Util.booleans.isTrue(filtro.getLancContareceberAtraso()) && 
									SinedDateUtils.afterIgnoreHour(SinedDateUtils.currentDate(), documento.getDtvencimentoFluxoCaixa(filtro.isFiltroOrigem())) && 
									SinedDateUtils.afterIgnoreHour(filtro.getPeriodoDe(), documento.getDtvencimentoFluxoCaixa(filtro.isFiltroOrigem())))){
						listaDocumento.add(documento);
					}
				}else if(Documentoclasse.OBJ_PAGAR.equals(documento.getDocumentoclasse())){
					if((Util.booleans.isTrue(filtro.getLancContapagarAberto()) && 
							SinedDateUtils.afterOrEqualsIgnoreHour(SinedDateUtils.currentDate(), documento.getDtvencimentoFluxoCaixa(filtro.isFiltroOrigem())) && 
							SinedDateUtils.beforeIgnoreHour(filtro.getPeriodoDe(), documento.getDtvencimentoFluxoCaixa(filtro.isFiltroOrigem()))) ||
						(Util.booleans.isTrue(filtro.getLancContapagarAtraso()) && 
									SinedDateUtils.afterIgnoreHour(SinedDateUtils.currentDate(), documento.getDtvencimentoFluxoCaixa(filtro.isFiltroOrigem())) && 
									SinedDateUtils.afterIgnoreHour(filtro.getPeriodoDe(), documento.getDtvencimentoFluxoCaixa(filtro.isFiltroOrigem())))){
						listaDocumento.add(documento);
					}
				}
			}
			iterator.remove();
		}
		
		return listaDocumento;
	}

	/**
	 * M�todo para obter uma lista de documentos com seus valores atuais e id's
	 * preenchidos.
	 * 
	 * @param whereIn
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documento> obterListaValorAtual(String whereIn) {
		if (StringUtils.isEmpty(whereIn)) {
			throw new SinedException(
					"O par�metro whereIn n�o pode estar vazio.");
		}
		return querySined()
				.select("documento.cddocumento, aux_documento.valoratual")
				.join("documento.aux_documento aux_documento").whereIn(
						"documento.cddocumento", whereIn).list();
	}

	/***************************************************************************
	 * *********************** Relat�rio de contas a pagar/receber
	 * ************************
	 **************************************************************************/

	/**
	 * M�todo para obter lista de Contas a receber para relat�rio, de acordo com
	 * o filtro.
	 * 
	 * @param filtro
	 * @return
	 * @author Flavio, Andre, Leandro
	 */
	public List<ContaReceberPagarBeanReport> findForReport(
			DocumentoFiltro filtro) {
		if (filtro == null) {
			throw new SinedException("O par�metro filtro n�o pode ser null.");
		}

		String sql = criaQueryContas(filtro);

		SinedUtil.markAsReader();
		@SuppressWarnings("unchecked")
		List<ContaReceberPagarBeanReport> lista = getJdbcTemplate().query(sql,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						
						Money taxa = new Money(rs.getDouble("taxa"), true);
						String parcelas = rs.getString("parcelas");

						if (taxa != null && taxa.getValue().doubleValue() == 0)
							taxa = null;
						
						if (parcelas != null && parcelas.equals("/"))
							parcelas = null;

						Money valoratual = new Money(rs.getLong("valorAtual"),true);
						
						String razaosocialPessoa = rs.getInt("tipopagamento") == Tipopagamento.CLIENTE.ordinal()? rs.getString("razaosocialCliente")
								:rs.getInt("tipopagamento") == Tipopagamento.FORNECEDOR.ordinal()? rs.getString("razaosocialFornecedor"):
									"";
						
						ContaReceberPagarBeanReport contaReceberPagarBeanReport = new ContaReceberPagarBeanReport(
								rs.getInt("cdconta"), 
								rs.getDate("dtVencimento"), 
								rs.getString("descricao"), 
								rs.getString("numero"),
								rs.getString("formaPagamento"), 
								parcelas,
								new Money(rs.getDouble("valor"), true), 
								taxa,
								valoratual, 
								new Money(rs.getDouble("valorPago"), true),
								rs.getString("centrocusto"), 
								rs.getInt("cdcentrocusto"), 
								rs.getString("contagerencial"),
								rs.getInt("cdcontagerencial"),
								rs.getString("identificador"), 
								rs.getString("projeto"),
								rs.getInt("cdprojeto") == 0 ? null : rs.getInt("cdprojeto"),
								new Money(rs.getDouble("valorRateio"), true),
								rs.getDouble("percentualRateio"),
								rs.getString("situacao"),
								rs.getDate("dtemissao"),
								rs.getInt("cdindicecorrecao") == 0 ? null : rs.getInt("cdindicecorrecao"),
								rs.getString("cheque"),
								rs.getString("datasPagto"), 
								rs.getString("codigobarras"),
								rs.getInt("cdtaxa"),
								rs.getDate("primeiro_vencimento"),
								new Money(rs.getDouble("valorMovimentacoes"), true),
								new Money(rs.getDouble("valorValecompra"), true),
								new Money(rs.getDouble("valorAntecipado"), true),
								new Documentotipo(rs.getInt("cddocumentotipo"), rs.getString("documentotipo_nome")),
								rs.getInt("cddocumentoacao"),
								rs.getString("nomePessoa"),
								razaosocialPessoa,
								rs.getString("outrospagamento"),
								rs.getInt("tipopagamento"),
								rs.getInt("tipopessoa"),
								new Money(rs.getLong("valorJurosMulta"),true),
								rs.getDate("dtcompetencia"));
						
						contaReceberPagarBeanReport.setClienteIdentificador(rs.getString("clienteIdentificador"));
						contaReceberPagarBeanReport.setObservacaoItemRateio(rs.getString("observacaoItemRateio"));
						contaReceberPagarBeanReport.setCdRateioItem(rs.getInt("cdrateioitem"));
						return contaReceberPagarBeanReport;
						
					}

				});

		return lista;
	}
	
	/**
	 * Monta a string SQL do relatorio de contas
	 * 
	 * @see #montaCondicoes(StringBuilder, DocumentoFiltro)
	 * @param filtro
	 * @return Uma String SQL
	 * @author Leandro Lima
	 * @author Fl�vio Tavares - Mudan�a do relacionamento de documento e pessoa
	 */
	private String criaQueryContas(DocumentoFiltro filtro) {
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		boolean parametroArrayPessoa = ParametrogeralService.getInstance().getBoolean("parametroArrayPessoa");
		
		StringBuilder query = new StringBuilder();

		query.append("select distinct documento.cddocumento as cdconta, documentotipo.cddocumentotipo, documentotipo.nome as documentotipo_nome,fp.nome as formaPagamento,");
		query.append("ri.cdrateio, documento.dtVencimento, documento.dtemissao, documento.dtcompetencia, documento.numero, documento.outrospagamento, documento.tipopagamento, coalesce(aux_documento.valorjurosmes, 0) + coalesce(aux_documento.valorjuros, 0) + coalesce(aux_documento.valormulta, 0) as valorJurosMulta,");
		query.append("cast(aux_documento.valoratual - documento.valor as numeric)  as taxa, documento.cdindicecorrecao, documentoacao.cddocumentoacao, ");
		query.append("coalesce(pessoa.nome,documento.outrospagamento) as descricaoPagamento, documento.codigobarras, cliente.identificador as clienteIdentificador, cliente.razaosocial as razaosocialCliente, ");
		query.append("pessoa.nome as nomePessoa, pessoa.tipopessoa, fornecedor.razaosocial as razaosocialFornecedor, ");
		query.append("documento.valor, documento.descricao, aux_documento.valorAtual as valorAtual, cc.nome as centrocusto, cc.cdcentrocusto, ");
		query.append("cg.cdcontagerencial, cg.nome as contagerencial, pj.nome as projeto, pj.cdprojeto, documentoacao.nome as situacao, vcg.identificador as identificador, ");
		query.append((parametrogeralService.getBoolean(Parametrogeral.OBS_RATEIO_CONTA) ? " ri.observacao " : "null") + " as observacaoItemRateio, ");
		query.append("cast(pa.ordem as varchar(5)) || '/' || cast( po.iteracoes as varchar(5)) as parcelas, ");
		query.append("(select count(*) as rateios from rateioitem where cdrateio = documento.cdrateio) as qtdRateios, ri.valor as valorRateio, ri.percentual as percentualRateio, ri.cdrateioitem, "); 
		query.append("coalesce(chequeDoc.numero, (array_to_string(array(Select ch1.numero From Movimentacao m1 JOIN Movimentacaoorigem mo1 ON mo1.cdmovimentacao = m1.cdmovimentacao JOIN Cheque ch1 ON ch1.cdcheque = m1.cdcheque Where documento.cddocumento = mo1.cddocumento and m1.cdmovimentacaoacao <> 0 and ch1.cdcheque = m1.cdcheque), ', '))) as cheque, ");
//		query.append("(Select max(coalesce(m.dtbanco, m.dtmovimentacao)) From Movimentacao m, Movimentacaoorigem mo Where mo.cdmovimentacao = m.cdmovimentacao and documento.cddocumento = mo.cddocumento and m.cdmovimentacaoacao <> 0) as dtbanco, ");
		query.append("array_to_string(array((Select distinct to_char(coalesce(m2.dtbanco, m2.dtmovimentacao), 'DD/MM/YYYY') From Movimentacao m2 JOIN Movimentacaoorigem mo2 ON mo2.cdmovimentacao = m2.cdmovimentacao Where documento.cddocumento = mo2.cddocumento and m2.cdmovimentacaoacao <> 0 UNION ALL SELECT distinct to_char(v.data, 'DD/MM/YYYY') FROM VALECOMPRA V JOIN VALECOMPRAORIGEM VO ON VO.CDVALECOMPRA = V.CDVALECOMPRA WHERE VO.CDDOCUMENTO = documento.cddocumento)), ', ') as datasPagto, ");
		query.append("(Select coalesce(sum(m3.valor), 0) From Movimentacao m3 JOIN Movimentacaoorigem mo3 ON mo3.cdmovimentacao = m3.cdmovimentacao Where documento.cddocumento = mo3.cddocumento and m3.cdmovimentacaoacao <> 0) as valorMovimentacoes, ");
		query.append("(Select coalesce(sum(vc1.valor), 0) From Valecompra vc1 JOIN Valecompraorigem vco1 ON vco1.cdvalecompra = vc1.cdvalecompra Where documento.cddocumento = vco1.cddocumento ) as valorValecompra, ");
		query.append("(select coalesce (sum (ha.valor), 0) from historicoantecipacao ha where ha.cddocumentoreferencia = documento.cddocumento) as valorAntecipado, ");
		query.append("aux_documento.valorAtual as valorPago, ");

		query.append("tax.cdtaxa as cdtaxa, ");
		query.append("(select dh.dtvencimento from Documentohistorico dh where dh.cddocumento = documento.cddocumento and dh.cddocumentohistorico = (select min(dh2.cddocumentohistorico) from Documentohistorico dh2 where dh2.cddocumento = documento.cddocumento)) as primeiro_vencimento ");
		
		query.append("from documento ");
		query.append("left outer join Rateio ra on documento.cdrateio = ra.cdrateio ");
		query.append("left outer join Rateioitem ri on ra.cdrateio = ri.cdrateio ");
		query.append("left outer join Projeto pj on ri.cdprojeto = pj.cdprojeto ");
		if(SinedUtil.getUsuarioLogado().getTodosprojetos() == null || !SinedUtil.getUsuarioLogado().getTodosprojetos()){
			query.append("left outer join Usuarioprojeto up on up.cdprojeto = pj.cdprojeto ");
		}
		query.append("left outer join Centrocusto cc on ri.cdcentrocusto = cc.cdcentrocusto ");
		query.append("left outer join Contagerencial cg on ri.cdcontagerencial = cg.cdcontagerencial ");
		query.append("left outer join Vcontagerencial vcg on cg.cdcontagerencial = vcg.cdcontagerencial ");
		
		String pessoaString = "";
		if(StringUtils.isNotBlank(filtro.getPessoa())){
			String cpfCnpjSoNumero = br.com.linkcom.neo.util.StringUtils.soNumero(filtro.getPessoa());
			if(StringUtils.isNotBlank(cpfCnpjSoNumero) && (Cpf.cpfValido(cpfCnpjSoNumero) || Cnpj.cnpjValido(cpfCnpjSoNumero))){
				query.append("left outer join Pessoa pessoa on documento.cdpessoa = pessoa.cdpessoa ");
			} else{
				pessoaString = Util.strings.tiraAcento(filtro.getPessoa().replaceAll("'", "''").toUpperCase());
				if(parametroArrayPessoa){
					query.append("left outer join Pessoa pessoa on documento.cdpessoa = pessoa.cdpessoa and ((pessoa.cdpessoa = any (retornaarraypessoa('"+pessoaString+"'))) = true) ");
				}else{
					query.append("left outer join Pessoa pessoa on documento.cdpessoa = pessoa.cdpessoa and (UPPER(" + funcaoTiraacento + "(pessoa.nome)) LIKE '%'||'"+pessoaString+"'||'%' or " +
							"(pessoa.cdpessoa in (SELECT f.cdpessoa FROM fornecedor f "+
				      		 "WHERE UPPER(" + funcaoTiraacento + "(f.razaosocial)) LIKE '%" + pessoaString + "%') " +
					 		 " or pessoa.cdpessoa in (SELECT c.cdpessoa FROM Cliente c " +
					 		 "WHERE UPPER(" + funcaoTiraacento + "(c.razaosocial)) LIKE '%" + pessoaString + "%')) = true)"
					);
				}
			}
		} else {
			query.append("left outer join Pessoa pessoa on documento.cdpessoa = pessoa.cdpessoa ");
		}
		
		query.append("left outer join documentoacao on documento.cddocumentoacao = documentoacao.cddocumentoacao ");
		query.append("left outer join aux_documento on documento.cddocumento = aux_documento.cddocumento ");
//		query.append("left outer join Documentotipo dt on documento.cddocumentotipo = dt.cddocumentotipo ");
//		query.append("left outer join Documentotipopapel dtp on dtp.cddocumentotipo = dt.cddocumentotipo ");
//		query.append("left outer join Papel pap on pap.cdpapel = dtp.cdpapel ");
		query.append("left outer join Cheque chequeDoc on chequeDoc.cdcheque = documento.cdcheque ");
		query.append("left outer join Empresa e on documento.cdempresa = e.cdpessoa ");
//		query.append("left outer join Pessoa ep on e.cdpessoa = ep.cdpessoa ");
		query.append("left outer join Parcela pa on documento.cddocumento = pa.cddocumento ");
		query.append("left outer join Parcelamento po on pa.cdparcelamento = po.cdparcelamento ");
		query.append("left outer join Movimentacaoorigem mo on documento.cddocumento = mo.cddocumento ");
		query.append("left outer join Movimentacao m on mo.cdmovimentacao = m.cdmovimentacao ");
		query.append("left outer join Formapagamento fp on fp.cdformapagamento = m.cdformapagamento ");
		query.append("left outer join Conta c on m.cdconta = c.cdconta ");
		if(filtro.getContatipo() != null){
			query.append("left outer join Contatipo ct on c.cdcontatipo = ct.cdcontatipo ");
		}
		query.append("left outer join Movimentacaoacao ma on m.cdmovimentacaoacao = ma.cdmovimentacaoacao ");
		query.append("left outer join taxa tax on tax.cdtaxa = documento.cdtaxa ");
		if ((filtro.getMotivo() != null && !filtro.getMotivo().equals("")) || filtro.getTipotaxa() != null){
			query.append("left outer join taxaitem ti on ti.cdtaxa = tax.cdtaxa ");
			query.append("left outer join tipotaxa tt on tt.cdtipotaxa = ti.cdtipotaxa ");
		}
//		query.append("left outer join Conta conta on conta.cdconta = documento.cdconta ");
		query.append("left outer join Documentotipo documentotipo on documentotipo.cddocumentotipo = documento.cddocumentotipo ");
		query.append("left outer join Cliente cliente on cliente.cdpessoa = pessoa.cdpessoa ");
		query.append("left outer join Fornecedor fornecedor on fornecedor.cdpessoa = pessoa.cdpessoa ");
		
		if(!SinedUtil.isUsuarioLogadoAdministrador()){
			query.append("left outer join Contapapel contapapel on contapapel.cdconta = c.cdconta ");
		}
		
		if(filtro.getCheque() != null && !"".equals(filtro.getCheque())){
			query.append("left outer join Cheque ch on ch.cdcheque = m.cdcheque ");
			query.append("left outer join Cheque ch2 on ch2.cdcheque = documento.cdcheque ");
		}
		
		if (filtro.getDtemissaonotaInicio()!=null || filtro.getDtemissaonotaFim()!=null || 
				filtro.getNumeroNotaFrom() != null || filtro.getNumeroNotaTo() != null || 
				filtro.getNumeroNfseTo() != null || filtro.getNumeroNfseFrom() != null){
			query.append("left outer join Notadocumento notadocumento on notadocumento.cddocumento = documento.cddocumento ");
			query.append("left outer join Documentoorigem docOrigem on docOrigem.cddocumento = documento.cddocumento ");
			query.append("left outer join NotaVenda notaVendaOrigem on notaVendaOrigem.cdvenda = docOrigem.cdvenda ");
			query.append("left outer join Nota nota on (nota.cdNota = notadocumento.cdNota or nota.cdNota = notaVendaOrigem.cdNota) ");
			
			if(filtro.getNumeroNfseTo() != null || filtro.getNumeroNfseFrom() != null){
				query.append("left outer join arquivonfnota arquivonfnota on arquivonfnota.cdNota = nota.cdNota ");
			}
		}

		if(filtro.getNumeroFaturaFrom()!= null || filtro.getNumeroFaturaTo()!= null) {
			query.append("left outer join Contratofaturalocacaodocumento contratofaturalocacaodocumento on contratofaturalocacaodocumento.cddocumento = documento.cddocumento ");
			query.append("left outer join Contratofaturalocacao contratofaturalocacao on contratofaturalocacao.cdcontratofaturalocacao = contratofaturalocacaodocumento.cdcontratofaturalocacao ");
		}

		if(filtro.getCategoria() != null && filtro.getCategoria().getCdcategoria() != null){
			query.append("left outer join Pessoacategoria pessoacategoria on pessoacategoria.cdpessoa = pessoa.cdpessoa ");
		}
		
		this.montaCondicoes(query, filtro);

		String orderBy = filtro.getOrderBy();
		String newOrderBy = "";

		if (orderBy == null || orderBy.equals(""))
			newOrderBy = "documento.cddocumento";
		else if (orderBy.matches("documento.aux_documento.valoratual.*")) {
			newOrderBy = "aux_documento.valoratual";
			if (orderBy.endsWith("asc") || orderBy.endsWith("desc")) {
				newOrderBy += orderBy.substring(orderBy.indexOf(" "), orderBy
						.length());
			}
		} else
			newOrderBy = orderBy;

		query.append(" ORDER BY ").append(newOrderBy);

		return query.toString();

	}

	/**
	 * Adiciona na String SQL as condi��es, de acordo com o filtro.
	 * 
	 * @param query
	 * @param filtro
	 * @author Leandro Lima
	 */
	private void montaCondicoes(StringBuilder query, DocumentoFiltro filtro) {

		query.append(" WHERE documento.cddocumentoclasse = ").append(
				filtro.getDocumentoclasse().getCddocumentoclasse());
		
//		if(filtro.getCheque() != null && !filtro.getCheque().equals("")){
//			String whereInCheque = this.findWhereInCheque(filtro.getCheque());
//			if(whereInCheque != null && !whereInCheque.equals("")){
//				query.append(" AND documento.cddocumento in (").append(whereInCheque).append(") ");
//			}
//		}

		this.adicionaConta(query, filtro.getCddocumento());
		this.adicionaDescricaoNumero(query, filtro.getDocumentoDescricao());
		this.adicionaVencimento(query, filtro.getDtvencimento1(), filtro.getDtvencimento2());
		this.adicionaEmissao(query, filtro.getDtemissao1(), filtro.getDtemissao2());
		this.adicionaCompetencia(query, filtro.getDtcompetencia1(), filtro.getDtcompetencia2());
		this.adicionaEmissaoNota(query, filtro.getDtemissaonotaInicio(), filtro.getDtemissaonotaFim());
		this.adicionaNumeroNota(query, filtro.getNumeroNotaFrom(), filtro.getNumeroNotaTo());
		this.adicionaNumeroNfseNota(query, filtro.getNumeroNfseFrom(), filtro.getNumeroNfseTo());
		this.adicionaNumeroFatura(query, filtro.getNumeroFaturaFrom(), filtro.getNumeroFaturaTo());
		this.adicionaBaixa(query, filtro.getDtbaixa1(), filtro.getDtbaixa2());
		this.adicionaValor(query, filtro.getValorminimo(), filtro.getValormaximo());
		this.adicionaAcao(query, filtro.getDocumentoacao());
		this.adicionaListaAcao(query, filtro.getListaDocumentoacao() != null ? CollectionsUtil.listAndConcatenate(filtro.getListaDocumentoacao(),"cddocumentoacao", ",") : null);
		this.adicionaStatusProjeto(query, filtro);
		this.adicionaListaDocumentoTipo(query, filtro);
		this.adicionaUsuarioLogado(query);
		this.adicionaPrimeiroVencimento(query, filtro.getDtprimeirovencimento1(), filtro.getDtprimeirovencimento2());
		
		if(filtro.getAntecipado() != null){
			query.append("AND coalesce(documento.antecipado, false) = "+filtro.getAntecipado().toString());
		}
		
		if(filtro.getTipoConta() != null && filtro.getTipoConta().equals("RECEBER")) {
			this.adicionaNumeroDocumento(query, filtro.getPermitirFiltroDocumentoDeAte(), filtro.getDocumentoDe(), filtro.getDocumentoAte());
		}	
		if(filtro.getAssociacao() != null){
			this.adicionaAssociacao(query, filtro.getAssociacao().getCdpessoa());
		}
		if(filtro.getDocumentotipo() != null){
			this.adicionaDocumentotipo(query, filtro.getDocumentotipo().getCddocumentotipo());
		}
		if (filtro.getContagerencial() != null)
			this.adicionaContagerencial(query, filtro.getContagerencial().getCdcontagerencial());
		
		this.adicionaListaContagerencial(query, filtro);

		if (filtro.getProjeto() != null)
			this.adicionaProjeto(query, filtro.getProjeto().getCdprojeto());

		if (filtro.getCentrocusto() != null)
			this.adicionaCentrocusto(query, filtro.getCentrocusto().getCdcentrocusto());
		
		this.adicionaListaCentrocusto(query, filtro);

		if (filtro.getTipopagamento() != null)
			this.adicionaTipopagamento(query, filtro.getTipopagamento());

		if (filtro.getCliente() != null)
			this.adicionaPessoa(query, filtro.getCliente().getCdpessoa());

		if (filtro.getFornecedor() != null)
			this.adicionaPessoa(query, filtro.getFornecedor().getCdpessoa());

		if (filtro.getColaborador() != null)
			this.adicionaPessoa(query, filtro.getColaborador().getCdpessoa());

		if (filtro.getOutrospagamento() != null)
			this.adicionaOutrospagamento(query, filtro.getOutrospagamento());
		
		if(filtro.getPessoa() != null)
			this.adicionaPessoaString(query, filtro.getPessoa());

		this.adicionaPessoasOutros(query, filtro);

		if (filtro.getEmpresa() != null)
			this.adicionaEmpresa(query, filtro.getEmpresa().getCdpessoa());
		else if(Util.booleans.isFalse(filtro.getAllEmpresa()) && filtro.getListaEmpresa() != null && filtro.getListaEmpresa().size() > 0)
			this.adicionaListaEmpresa(query, filtro);
		else
			this.adiconaEmpresaPermissao(query);

		this.adicionaSituacaoVenc(query, filtro);
		this.adicionaVinculoMovimentacao(query, filtro.getContatipo(), filtro.getConta(), filtro.getListaCaixa(), filtro.getListaCartao(), filtro.getListaContabancaria());
		
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		if(!administrador){
			if (whereInPapel != null && !whereInPapel.equals("")){
				query.append(" AND (contapapel.cdpapel is null or contapapel.cdpapel in ("+whereInPapel+")) ");
			} else {
				query.append("AND contapapel is null");
			}
		}
		
		if (filtro.getMotivo() != null && !filtro.getMotivo().equals("")){
			query.append("AND ti.motivo = '" + filtro.getMotivo() + "' ");
		}
		
		if (filtro.getTipotaxa() != null){
			query.append("AND tt.cdtipotaxa = " + filtro.getTipotaxa().getCdtipotaxa());
		}
		
		if(filtro.getCheque() != null && !filtro.getCheque().equals("")){
			query.append("AND (ch.numero = '").append(filtro.getCheque()).append("' OR ").append(" ch2.numero = '").append(filtro.getCheque()).append("') ");
		}
		
		if(filtro.getContaboleto()!= null){
			query.append("AND documento.cdconta = " + filtro.getContaboleto().getCdconta());
		}
		
		if(filtro.getVendedor() != null && filtro.getVendedor().getCdpessoa() != null){
			query
				.append(" AND ( ")
				.append(" exists(select v.cdvenda " +
								" from Venda v " +
								" join documentoorigem docorigemv on docorigemv.cdvenda = v.cdvenda " +
								" where docorigemv.cddocumento = documento.cddocumento " +
								" and v.cdcolaborador = " + filtro.getVendedor().getCdpessoa() + " " + 
								" ) ")
				.append(" OR ")
				.append(" exists(select pv.cdpedidovenda " +
								" from Pedidovenda pv " +
								" join documentoorigem docorigempv on docorigempv.cdpedidovenda = pv.cdpedidovenda " +
								" where docorigempv.cddocumento = documento.cddocumento " +
								" and pv.cdcolaborador = " + filtro.getVendedor().getCdpessoa() + " " + 
								" ) ")
				.append(" OR ")
				.append(" exists(select c.cdcontrato " +
								" from Contrato c " +
								" join documentoorigem docoric on docoric.cdcontrato = c.cdcontrato " +
								" where docoric.cddocumento = documento.cddocumento " +
								" and c.cdvendedor = " + filtro.getVendedor().getCdpessoa() + " " + 
								" ) ")
				.append(" OR ")
				.append(" exists(select c.cdcontrato " +
								" from Contrato c " +
								" join notacontrato ncc on ncc.cdcontrato = c.cdcontrato " +
								" join notadocumento ndc on ndc.cdnota = ncc.cdnota " +
								" where ndc.cddocumento = documento.cddocumento " +
								" and c.cdvendedor = " + filtro.getVendedor().getCdpessoa() + " " + 
								" ) ")
				.append(" ) ");
		}
		
		if(filtro.getMotivocancelamento() != null){
			query
				.append(" AND exists ( ")
				.append(" select dh.cddocumentohistorico ")
				.append(" from Documentohistorico dh ")
				.append(" where dh.cddocumento = documento.cddocumento ")
				.append(" and dh.cdmotivocancelamento = ").append(filtro.getMotivocancelamento().getCdmotivocancelamento()).append(" ")
				.append(" ) ") 
				;
		}
		
		if(filtro.getDtiniciolancamento() != null){
			query
			.append(" AND ( (select min(dh1.dtaltera) " +
							"from documentohistorico dh1 " +
							"where dh1.cddocumento = documento.cddocumento) >= '"+SinedDateUtils.dateToTimestampInicioDia(filtro.getDtiniciolancamento())+"') ");
		
		}
		if(filtro.getDtfimlancamento() != null){
			query
				.append(" AND ( (select min(dh1.dtaltera) " +
							"from documentohistorico dh1 " +
							"where dh1.cddocumento = documento.cddocumento) <= '"+SinedDateUtils.dateToTimestampFinalDia(filtro.getDtfimlancamento())+"') ");
		}
		
		if(filtro.getVinculoProvisionado() != null && filtro.getVinculoProvisionado().getCdconta() != null){
			query.append(" AND documento.cdvinculoprovisionado = " + filtro.getVinculoProvisionado().getCdconta());
		}
		
		if(filtro.getDocumentoacaoprotesto() != null && filtro.getDocumentoacaoprotesto().getCddocumentoacao() != null){
			query.append(" AND documento.cddocumentoacaoprotesto = " + filtro.getDocumentoacaoprotesto().getCddocumentoacao());
		}
		
		if(filtro.getCategoria() != null && filtro.getCategoria().getCdcategoria() != null){
			query.append(" AND pessoacategoria.cdcategoria =  " + filtro.getCategoria().getCdcategoria());
		}
		
		if(filtro.getContratotipo() != null && filtro.getContratotipo().getCdcontratotipo() != null){
			query.append(" AND ( ")
					.append(" exists(select c.cdcontrato " +
							" from Contrato c " +
							" join documentoorigem docoric on docoric.cdcontrato = c.cdcontrato " +
							" where docoric.cddocumento = documento.cddocumento " +
							" and c.cdcontratotipo = " + filtro.getContratotipo().getCdcontratotipo() + " " + 
							" ) ")
			.append(" OR ")
			.append(" exists(select c.cdcontrato " +
							" from Contrato c " +
							" join notacontrato ncc on ncc.cdcontrato = c.cdcontrato " +
							" join notadocumento ndc on ndc.cdnota = ncc.cdnota " +
							" where ndc.cddocumento = documento.cddocumento " +
							" and c.cdcontratotipo = " + filtro.getContratotipo().getCdcontratotipo() + " " + 
							" ) ")
			.append(" ) ");
		}
		
		query.append(" and (ma.cdmovimentacaoacao is null or (documento.cddocumentoacao in (6,7) and (ma.cdmovimentacaoacao not in (0)) or exists (select valecompraorigem.cddocumento from valecompraorigem where valecompraorigem.cddocumento = documento.cddocumento) ) or (documento.cddocumentoacao not in (6,7) and ma.cdmovimentacaoacao is not null and not exists (select valecompraorigem.cddocumento from valecompraorigem where valecompraorigem.cddocumento = documento.cddocumento))) ");
		
		addRestricaoClienteEmpresa(query);
		addRestricaoFornecedorEmpresa(query);
	}
	
	public void addRestricaoClienteEmpresa(StringBuilder sb){
		String whereInEmpresa = new SinedUtil().getListaEmpresa();
		if(SinedUtil.isRestricaoEmpresaClienteUsuarioLogado() && StringUtils.isNotBlank(whereInEmpresa)){
			sb.append(" AND (");
				sb.append(" pessoa.cdpessoa is null");
				sb.append(" or ");
				sb.append(" documento.tipopagamento <> " + Tipopagamento.CLIENTE.ordinal());
				sb.append(" or ");
				sb.append(new SinedUtil().getWhereClienteempresa("pessoa", false, null));
			sb.append(")");
		}
	}
	
	public void addRestricaoFornecedorEmpresa(StringBuilder sb){
		String whereInEmpresa = new SinedUtil().getListaEmpresa();
		if(StringUtils.isNotBlank(whereInEmpresa) && SinedUtil.isRestricaoEmpresaFornecedorUsuarioLogado()){
			sb.append(" AND (");
				sb.append(" pessoa.cdpessoa is null");
				sb.append(" or ");
				sb.append(" documento.tipopagamento <> " + Tipopagamento.FORNECEDOR.ordinal());
				sb.append(" or ");
				sb.append(new SinedUtil().getWhereFornecedorempresa("pessoa", false, null));
			sb.append(")");
		}
	}

	private void adicionaNumeroDocumento(StringBuilder query, Boolean permitirFiltroDocumentoDeAte, String documentoDe,	String documentoAte) {
		if(permitirFiltroDocumentoDeAte != null && permitirFiltroDocumentoDeAte && 
				(documentoDe != null && !"".equals(documentoDe)) ||
				(documentoAte != null && !"".equals(documentoAte))){
			if(documentoDe != null && !"".equals(documentoDe)){
				try {
					query.append(" AND CAST(COALESCE(documento.numero, '0') AS Numeric) >= " + Long.parseLong(documentoDe) + " "); 
				} catch (Exception e) {}
			}
			if(documentoAte != null && !"".equals(documentoAte)){
				try {
					query.append(" AND CAST(COALESCE(documento.numero, '0') AS Numeric) <= " + Long.parseLong(documentoAte) + " "); 
				} catch (Exception e) {}
			}
		}
	}
	
	private void adicionaVinculoMovimentacao(StringBuilder query, 
			Contatipo contatipo, 
			Conta conta, 
			List<ItemDetalhe> listaCaixa, 
			List<ItemDetalhe> listaCartao, 
			List<ItemDetalhe> listaContabancaria) {
		if(conta != null){
			query
				.append(" AND (c.cdconta = ")
				.append(conta.getCdconta())
				.append(" ) ");
		} else if(contatipo != null){
			query
				.append(" AND (ct.cdcontatipo = ")
				.append(contatipo.getCdcontatipo())
				.append(" ) ");
		}
		
		List<Integer> listaIdsContas = new ArrayList<Integer>();
		if(listaCaixa != null){
			for (ItemDetalhe it : listaCaixa) {
				if(it.getConta() != null)
					listaIdsContas.add(it.getConta().getCdconta());
			}
		}
		if(listaCartao != null){
			for (ItemDetalhe it : listaCartao) {
				if(it.getConta() != null)
					listaIdsContas.add(it.getConta().getCdconta());
			}
		}
		if(listaContabancaria != null){
			for (ItemDetalhe it : listaContabancaria) {
				if(it.getConta() != null)
					listaIdsContas.add(it.getConta().getCdconta());
			}
		}
		if(listaIdsContas.size() > 0){
			query
				.append(" AND (c.cdconta IN (")
				.append(CollectionsUtil.concatenate(listaIdsContas, ","))
				.append(" )) ");
		}
	}
	
	private void adiconaEmpresaPermissao(StringBuilder query) {
		String listaEmpresa = new SinedUtil().getListaEmpresa();
		if(listaEmpresa != null && !listaEmpresa.equals("")){
			query
				.append(" AND (documento.cdempresa in ( ")
				.append(listaEmpresa)
				.append(" ) OR documento.cdempresa is null) ");
		}
	}
	
	private void adicionaPessoaString(StringBuilder query, String pessoa) {
boolean parametroArrayPessoa = ParametrogeralService.getInstance().getBoolean("parametroArrayPessoa");
		
		if (StringUtils.isNotBlank(pessoa)) {
			String pessoaString = Util.strings.tiraAcento(pessoa.replaceAll("'", "''").toUpperCase());
			query.append(" AND (");
			
			String cpfCnpjSoNumero = br.com.linkcom.neo.util.StringUtils.soNumero(pessoa);
			if(StringUtils.isNotBlank(cpfCnpjSoNumero) && Cpf.cpfValido(cpfCnpjSoNumero)){
				query.append(" pessoa.cpf = '").append(cpfCnpjSoNumero).append("' OR ");
			} else if(StringUtils.isNotBlank(cpfCnpjSoNumero) && Cnpj.cnpjValido(cpfCnpjSoNumero)){
				query.append(" pessoa.cnpj = '").append(cpfCnpjSoNumero).append("' OR ");
			}
			
			query
				.append(" UPPER(retira_acento(documento.outrospagamento)) LIKE '%' || '" + pessoaString + "' || '%' ")
				.append(" OR ")
				.append(" UPPER(retira_acento(pessoa.nome)) LIKE '%' || '" + pessoaString + "' || '%' ");
			
			if(parametroArrayPessoa){
				query.append(" OR (pessoa.cdpessoa = any (retornaarraypessoa('" + pessoaString + "'))) = true) ");
			}else {
				query.append(" OR (" +
						" pessoa.cdpessoa in ( " +
						" SELECT f.cdpessoa " +
						" FROM fornecedor f " +
						" WHERE UPPER(retira_acento(f.razaosocial)) LIKE '%" + pessoaString + "%' ) " +
						" OR pessoa.cdpessoa in ( " +
						" SELECT c.cdpessoa " +
						" FROM Cliente c " +
						" WHERE UPPER(retira_acento(c.razaosocial)) LIKE '%" + pessoaString + "%')) = true) ");
			}
		}
		
	}
	/**
	 * Adiciona a cl�usula Mostrar do filtro de contas a pagar/receber
	 * 
	 * @param query
	 * @param documentoacao
	 * @author Fl�vio Tavares
	 */
	private void adicionaAcao(StringBuilder query, Documentoacao documentoacao) {
		if (documentoacao == null)
			return;
		
		if(Documentoacao.REAGENDAMENTO.equals(documentoacao)){
			query.append(" AND documento.dtvencimentooriginal IS NOT NULL ");
		} else if (Documentoacao.PROTESTADA.equals(documentoacao) || Documentoacao.ENVIADO_CARTORIO.equals(documentoacao) ||
				Documentoacao.ENVIADO_JURIDICO.equals(documentoacao) || Documentoacao.ARQUIVO_MORTO.equals(documentoacao)) {
			query.append(" AND (documento.dtcartorio is not null or documento.cddocumentoacaoprotesto = " + documentoacao.getCddocumentoacao() + ") ");
		} else if (Documentoacao.ATRASADA.equals(documentoacao)) {
			query.append(" AND documento.dtvencimento < '").append(SinedDateUtils.toStringPostgre(SinedDateUtils.currentDate())).append("' ");
			query.append(" AND documentoacao.cddocumentoacao <> ").append(Documentoacao.BAIXADA.getCddocumentoacao());
			query.append(" AND documentoacao.cddocumentoacao <> ").append(Documentoacao.CANCELADA.getCddocumentoacao());
			query.append(" AND documentoacao.cddocumentoacao <> ").append(Documentoacao.NEGOCIADA.getCddocumentoacao());
			query.append(" ");
		} else if (Documentoacao.PAG_ATRASADO.equals(documentoacao)) {
			query.append(" AND ");
			query
					.append("exists (select m.cdmovimentacao ")
					.append("from Movimentacao m ")
					.append("join movimentacaoorigem mo on mo.cdmovimentacao = m.cdmovimentacao ")
					.append("join documento d on d.cddocumento = mo.cddocumento ")
					.append("where d.cddocumento = documento.cddocumento ")
					.append("and m.cdmovimentacaoacao is not null and m.cdmovimentacaoacao <> 0 ") 
					.append("and documento.dtvencimento < coalesce(m.dtbanco, m.dtmovimentacao) ) ");
		} else if (Documentoacao.DEVOLVIDA.equals(documentoacao)) {
			query.append(" AND documento.dtdevolucao is not null ");
		} else if (Documentoacao.BOLETO_GERADO.equals(documentoacao)) {
			query.append(" AND documento.boletogerado=true ");
		} else if (Documentoacao.PENDENTE.equals(documentoacao)){
			query.append(" AND ");
			query.append("documentoacao.cddocumentoacao <> ").append(
					Documentoacao.CANCELADA.getCddocumentoacao());
			query
				.append(" AND ")
				.append("exists (select m.cdmovimentacao ")
				.append("from Movimentacao m ")
				.append("join Movimentacaoacao mac on m.cdmovimentacaoacao = mac.cdmovimentacaoacao ")
				.append("where mac.cdmovimentacaoacao <> " + Movimentacaoacao.CONCILIADA.getCdmovimentacaoacao())
				.append(" or m.cdmovimentacao IS NULL) ");
		} else if (Documentoacao.ATRASADA_CONTRATO_ATIVO.equals(documentoacao)){
			String inativo = "("+SituacaoContrato.CANCELADO.getValue().toString()+
			","+SituacaoContrato.SUSPENSO.getValue().toString()+")";
			
			query.append(" AND documento.dtvencimento < '").append(SinedDateUtils.toStringPostgre(SinedDateUtils.currentDate())).append("' ");
			query.append(" AND documentoacao.cddocumentoacao <> ").append(Documentoacao.BAIXADA.getCddocumentoacao());
			query.append(" AND documentoacao.cddocumentoacao <> ").append(Documentoacao.CANCELADA.getCddocumentoacao());
			query.append(" AND documentoacao.cddocumentoacao <> ").append(Documentoacao.NEGOCIADA.getCddocumentoacao());
			query.append(" ");
			
			query.append(" AND (")
			
			.append("exists (select nd.cddocumento ")
			.append("from NotaDocumento nd ")
			.append("join NotaContrato nc on nc.cdnota = nd.cdnota ")
			.append("join Aux_contrato aux on aux.cdcontrato = nc.cdcontrato ")
			.append("where aux.situacao not in " + inativo + " and nd.cddocumento = documento.cddocumento) ")

			.append(" OR ")

			.append("exists (select doco.cddocumento ")
			.append("from Documentoorigem doco ")
			.append("join Aux_contrato aux2 on aux2.cdcontrato = doco.cdcontrato ")
			.append("where aux2.situacao not in " + inativo + " and doco.cddocumento = documento.cddocumento) ")
			
			.append(") ");
		} else if (Documentoacao.ATRASADA_CONTRATO_SUSPENSO.equals(documentoacao)) {
			query.append(" AND documento.dtvencimento < '").append(SinedDateUtils.toStringPostgre(SinedDateUtils.currentDate())).append("' ");
			query.append(" AND documentoacao.cddocumentoacao <> ").append(Documentoacao.BAIXADA.getCddocumentoacao());
			query.append(" AND documentoacao.cddocumentoacao <> ").append(Documentoacao.CANCELADA.getCddocumentoacao());
			query.append(" AND documentoacao.cddocumentoacao <> ").append(Documentoacao.NEGOCIADA.getCddocumentoacao());
			query.append(" ");
			
			query.append(" AND (")
			
			.append("exists (select nd.cddocumento ")
			.append("from NotaDocumento nd ")
			.append("join NotaContrato nc on nc.cdnota = nd.cdnota ")
			.append("join Aux_contrato aux on aux.cdcontrato = nc.cdcontrato ")
			.append("where aux.situacao = " + SituacaoContrato.SUSPENSO.getValue().toString() + " and nd.cddocumento = documento.cddocumento) ")

			.append(" OR ")

			.append("exists (select doco.cddocumento ")
			.append("from Documentoorigem doco ")
			.append("join Aux_contrato aux2 on aux2.cdcontrato = doco.cdcontrato ")
			.append("where aux2.situacao = " + SituacaoContrato.SUSPENSO.getValue().toString() + " and doco.cddocumento = documento.cddocumento) ")
			
			.append(") ");
		} else if (Documentoacao.ATRASADA_SEM_PROTESTO.equals(documentoacao)) {
			query.append(" AND documento.dtvencimento < '").append(SinedDateUtils.toStringPostgre(SinedDateUtils.currentDate())).append("' ");
			query.append(" AND documentoacao.cddocumentoacao <> ").append(Documentoacao.BAIXADA.getCddocumentoacao());
			query.append(" AND documentoacao.cddocumentoacao <> ").append(Documentoacao.CANCELADA.getCddocumentoacao());
			query.append(" AND documentoacao.cddocumentoacao <> ").append(Documentoacao.NEGOCIADA.getCddocumentoacao());
			query.append(" AND documento.dtcartorio is null ");
		} else if (Documentoacao.ENVIO_MANUAL_BOLETO.equals(documentoacao)) {
			query.append(" AND ");
			query
				.append("documento.cddocumento in (select doc.cddocumento ")
				.append("from Documentohistorico dh ")
				.append("join Documento doc on doc.cddocumento = dh.cddocumento ")
				.append("join Documentoacao da on da.cddocumentoacao = doc.cddocumentoacao ")
				.append("where dh.cddocumentoacao = 110) ");
		} else if (Documentoacao.ATRASADA_CONTRATO_CANCELADO.equals(documentoacao)) {
			query.append(" AND documento.dtvencimento < '").append(SinedDateUtils.toStringPostgre(SinedDateUtils.currentDate())).append("' ");
			query.append(" AND documentoacao.cddocumentoacao <> ").append(Documentoacao.BAIXADA.getCddocumentoacao());
			query.append(" AND documentoacao.cddocumentoacao <> ").append(Documentoacao.CANCELADA.getCddocumentoacao());
			query.append(" AND documentoacao.cddocumentoacao <> ").append(Documentoacao.NEGOCIADA.getCddocumentoacao());
			
			query.append(" AND (")
			
			.append("exists (select nd.cddocumento ")
			.append("from NotaDocumento nd  ")
			.append("join NotaContrato nc on nc.cdnota = nd.cdnota ")
			.append("join Aux_contrato aux on aux.cdcontrato = nc.cdcontrato ")
			.append("where aux.situacao = " + SituacaoContrato.CANCELADO.getValue().toString() + " and nd.cddocumento = documento.cddocumento) ")

			.append(" OR ")

			.append("exists (select doco.cddocumento ")
			.append("from Documentoorigem doco ")
			.append("join Aux_contrato aux2 on aux2.cdcontrato = doco.cdcontrato ")
			.append("where aux2.situacao = " + SituacaoContrato.CANCELADO.getValue().toString() + " and doco.cddocumento = documento.cddocumento) ")
			
			.append(") ");
		}
	}

	/**
	 * Adiciona a cl�usula WHERE para o campo outrospagamento.
	 * 
	 * @param query
	 * @param outrospagamento
	 * @author Fl�vio Tavares
	 */
	private void adicionaOutrospagamento(StringBuilder query,
			String outrospagamento) {
		if (StringUtils.isNotBlank(outrospagamento)) {
			query
					.append(
							" AND UPPER(retira_acento(documento.outrospagamento)) LIKE '%")
					.append(
							Util.strings.tiraAcento(outrospagamento
									.toUpperCase())).append("%' ");
		}
	}

	/**
	 * Adiciona a cl�usula where para tipopagamento.
	 * 
	 * @param query
	 * @param tipopagamento
	 * @author Fl�vio Tavares
	 */
	private void adicionaTipopagamento(StringBuilder query,
			Tipopagamento tipopagamento) {
		if (tipopagamento != null) {
			query.append(" AND documento.tipopagamento = ").append(
					tipopagamento.ordinal()).append(" ");
			;
		}
	}

	/**
	 * Atribui a cl�usula referente � pessoa (fornecedor, colaborador ou
	 * cliente), se estiver presente no filtro.
	 * 
	 * @param query
	 * @param cdpessoa
	 * @author Fl�vio Tavares
	 */
	private void adicionaPessoa(StringBuilder query, Integer cdpessoa) {
		if (cdpessoa != null) {
			query.append(" AND documento.cdpessoa = ").append(
					cdpessoa.toString()).append(" ");
			;
		}
	}

	/**
	 * Atribui a cl�usula referente a situa��o do documento, se estiver presente
	 * no filtro.
	 * 
	 * @param query
	 * @param filtro
	 * @author Leandro Lima
	 */
	private void adicionaSituacaoVenc(StringBuilder query,
			DocumentoFiltro filtro) {
		boolean aVencer = Util.booleans.isTrue(filtro.getAvencer()), vencidas = Util.booleans
				.isTrue(filtro.getVencida()), quitadas = Util.booleans
				.isTrue(filtro.getQuitada());
		if (aVencer || vencidas || quitadas) {
			String cond = " AND ";
			query.append(cond).append(" ( ");

			if (aVencer) {
				query.append(" ( ");
				query.append(" documento.dtvencimento >= current_date");
				query.append(" AND documento.cddocumentoacao <> ").append(
						Documentoacao.BAIXADA.getCddocumentoacao());
				query.append(" ) ");
				cond = "OR";
			}
			if (vencidas) {
				if (cond.equals("OR"))
					query.append(cond).append(" ( ");
				query.append(" documento.dtvencimento < current_date");
				query.append(" AND documento.cddocumentoacao <> ").append(
						Documentoacao.BAIXADA.getCddocumentoacao());
				if (cond.equals("OR"))
					query.append(" ) ");
				cond = "OR";

			}
			if (quitadas) {
				if (cond.equals("OR"))
					query.append(cond).append(" ( ");
				query.append(" ma.cdmovimentacaoacao = ").append(
						Movimentacaoacao.CONCILIADA.getCdmovimentacaoacao());
				if (cond.equals("OR"))
					query.append(" ) ");
				cond = "OR";
			}
			query.append(" ) ");
		}
	}

	/**
	 * Atribui a cl�usula referente ao documento, se estiver presente no filtro.
	 * 
	 * @param query
	 * @param cddocumento
	 * @author Leandro Lima
	 */
	private void adicionaConta(StringBuilder query, Integer cdDocumento) {
		if (cdDocumento != null) {
			query.append(" AND documento.cddocumento = ").append(
					cdDocumento.toString()).append(" ");
		}
	}

	/**
	 * Atribui a cl�usula referente a descricao, se estiver presente no filtro.
	 * 
	 * @param query
	 * @param descricao
	 * @author Leandro Lima
	 */
	private void adicionaDescricaoNumero(StringBuilder query, String descricaoNumero) {
		if (descricaoNumero != null && !descricaoNumero.equals("")) {
			String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
			if (funcaoTiraacento != null) {
				query.append(" AND (UPPER(" + funcaoTiraacento + "(documento.descricao)) LIKE '%' || '" +  Util.strings.tiraAcento(descricaoNumero).toUpperCase()  + "' ||'%' ");
			}
			else {
				query.append(" AND (UPPER(documento.descricao) LIKE '%' || '" + Util.strings.tiraAcento(descricaoNumero).toUpperCase() + "' || '%' ");
			}
			query.append(" OR UPPER(documento.numero) like '%").append(descricaoNumero.toUpperCase()).append("%' )");
		}
		
		
	}

	/**
	 * Adiciona � consulta a clausula abaixo que, verifica se o usu�rio tem permiss�o 
	 * para acessar todos os projetos ao emitir o relat�rio "Conta a pagar/receber".
	 * 
	 * @param query
	 * @author Taidson Santos
	 * @since 07/04/2010
	 */
	private void adicionaUsuarioLogado(StringBuilder query){
//		query.append(" AND (up.cdusuario = " + SinedUtil.getUsuarioLogado().getCdpessoa() +
//				" or ri.cdprojeto is null " +
//				" or not exists(select usuarioprojeto.cdusuario " +
//				"from usuarioprojeto where cdusuario = " + 
//				SinedUtil.getUsuarioLogado().getCdpessoa()+"))");
		
		if(SinedUtil.getUsuarioLogado().getTodosprojetos() == null || !SinedUtil.getUsuarioLogado().getTodosprojetos()){
			query.append(" AND (up.cdusuario = " + SinedUtil.getUsuarioLogado().getCdpessoa() +
				" or ri.cdprojeto is null " + 
				"or ((select max(usuarioprojeto.cdusuario) from usuarioprojeto where cdusuario = " + SinedUtil.getUsuarioLogado().getCdpessoa() + ")=null))");
		}
		
	}
	/**
	 * Atribui a cl�usula referente ao centro de custo, se estiver presente no
	 * filtro.
	 * 
	 * @param query
	 * @param cdcentrocusto
	 * @author Leandro Lima
	 */
	private void adicionaCentrocusto(StringBuilder query, Integer cdCentrocusto) {
		if (cdCentrocusto != null) {
			query.append(" AND cc.cdcentrocusto = ").append(
					cdCentrocusto.toString()).append(" ");
			;
		}
	}

	/**
	 * Atribui a cl�usula referente a lista de centro de custos, se estiver
	 * presente no filtro.
	 * 
	 * @param query
	 * @param Filtro
	 * @author Leandro Lima
	 */
	private void adicionaListaCentrocusto(StringBuilder query,
			DocumentoFiltro filtro) {
		if (Util.booleans.isFalse(filtro.getAllCentrocusto())
				&& filtro.getListaCentrocusto() != null
				&& filtro.getListaCentrocusto().size() > 0)
			query.append(" AND cc.cdcentrocusto in ( ").append(
					CollectionsUtil.listAndConcatenate(filtro
							.getListaCentrocusto(),
							"centrocusto.cdcentrocusto", ",")).append(" ) ");

	}

	/**
	 * Adiciona a condi��o para lista de conta gerencial.
	 * 
	 * @param query
	 * @param filtro
	 * @author Fl�vio Tavares
	 */
	private void adicionaListaContagerencial(StringBuilder query,
			DocumentoFiltro filtro) {
		if (Util.booleans.isFalse(filtro.getAllContagerencial())
				&& SinedUtil.isListNotEmpty(filtro.getListaContagerencial()))
			query.append(" AND cg.cdcontagerencial in ( ").append(
					CollectionsUtil.listAndConcatenate(filtro
							.getListaContagerencial(),
							"contagerencial.cdcontagerencial", ",")).append(
					" ) ");
	}

	/**
	 * Atribui a cl�usula referente a conta gerencial, se estiver presente no
	 * filtro.
	 * 
	 * @param query
	 * @param cdcontagerencial
	 * @author Leandro Lima
	 */
	private void adicionaContagerencial(StringBuilder query,
			Integer cdContagerencial) {
		if (cdContagerencial != null) {
			query.append(" AND cg.cdcontagerencial = ").append(
					cdContagerencial.toString()).append(" ");
			;
		}
	}
	
	/**
	 * Atribui a cl�usula referente a tipo de documento, 
	 * se estiver presente no filtro.
	 * 
	 * @param query
	 * @param cddocumentotipo
	 * @author Taidson Santos
	 * @since 07/04/2010
	 */
	private void adicionaDocumentotipo(StringBuilder query, Integer cdDocumentotipo) {
		query.append(" AND documentotipo.cddocumentotipo = ").append(cdDocumentotipo.toString()).append(" ");
	}
	
	private void adicionaAssociacao(StringBuilder query, Integer cdAssociacao) {
		query.append(" AND cliente.cdassociacao = ").append(cdAssociacao.toString()).append(" ");
	}
	
	
	/**
	 * Atribui a cl�usula referente ao projeto, se estiver presente no filtro.
	 * 
	 * @param query
	 * @param cdProjeto
	 * @author Leandro Lima
	 */
	private void adicionaProjeto(StringBuilder query, Integer cdProjeto) {
		if (cdProjeto != null) {
			query.append(" AND pj.cdprojeto = ").append(cdProjeto.toString())
					.append(" ");
			;
		}
	}

	/**
	 * Atribui a cl�usula referente ao status do projeto, se estiver presente no
	 * filtro.
	 * 
	 * @param query
	 * @param filtro
	 * @author Leandro Lima
	 */
	private void adicionaStatusProjeto(StringBuilder query,
			DocumentoFiltro filtro) {
		if (filtro.getRadProjeto() != null) {
			String idProjeto = String.valueOf(filtro.getRadProjeto().getId());
			if (idProjeto.equals("escolherProjeto")
					&& filtro.getListaProjeto() != null
					&& !filtro.getListaProjeto().isEmpty()) {
				query.append(" AND pj.cdprojeto in (").append(
						CollectionsUtil.listAndConcatenate(filtro
								.getListaProjeto(), "projeto.cdprojeto", ","))
						.append(" ) ");
			} else if (idProjeto.equals("comProjeto")) {
				query.append(" AND pj.cdprojeto is not null");
			} else if (filtro.getRadProjeto().getId().equals("semProjeto")) {
				query.append(" AND pj.cdprojeto is null");
			}
		}
	}

	/**
	 * Adiciona a lista de clientes, fornecedor, colaborador e outros na query.
	 * 
	 * @param query
	 * @param filtro
	 * @author Fl�vio Tavares
	 */
	private void adicionaPessoasOutros(StringBuilder query,
			DocumentoFiltro filtro) {
		if (SinedUtil.isListNotEmpty(filtro.getListaCliente())
				|| SinedUtil.isListNotEmpty(filtro.getListaFornecedor())
				|| SinedUtil.isListNotEmpty(filtro.getListaOutrospagamento())
				|| SinedUtil.isListNotEmpty(filtro.getListaColaborador())) {
			query.append(" AND (");
			this.adicionaListaCliente(query, filtro);
			query.append(" OR ");
			this.adicionaListaFornecedor(query, filtro);
			query.append(" OR ");
			this.adicionaListaColaborador(query, filtro);
			query.append(" OR ");
			this.adicionaListaOutrospagamento(query, filtro);
			query.append(") ");
		}
	}

	/**
	 * Atribui a cl�usula referente a lista de clientes, se estiver presente no
	 * filtro.
	 * 
	 * @param query
	 * @param Filtro
	 * @author Leandro Lima
	 */
	private void adicionaListaCliente(StringBuilder query,
			DocumentoFiltro filtro) {
		if (SinedUtil.isListNotEmpty(filtro.getListaCliente())) {
			query.append(" documento.cdpessoa IN ( ").append(
					CollectionsUtil.listAndConcatenate(
							filtro.getListaCliente(), "cliente.cdpessoa", ","))
					.append(" ) ");
		} else
			query.append(" 0=1 ");
	}

	/**
	 * Atribui a cl�usula referente a lista de colaborador, se estiver presente
	 * no filtro.
	 * 
	 * @param query
	 * @param Filtro
	 * @author Fl�vio Tavares
	 */
	private void adicionaListaColaborador(StringBuilder query,
			DocumentoFiltro filtro) {
		if (SinedUtil.isListNotEmpty(filtro.getListaColaborador())) {
			query.append(" documento.cdpessoa IN ( ").append(
					CollectionsUtil
							.listAndConcatenate(filtro.getListaColaborador(),
									"colaborador.cdpessoa", ",")).append(" ) ");
		} else
			query.append(" 0=1 ");
	}

	/**
	 * Atribui a cl�usula referente a lista de outros pagamento, se estiver
	 * presente no filtro.
	 * 
	 * @param query
	 * @param Filtro
	 * @author Fl�vio Tavares
	 */
	private void adicionaListaOutrospagamento(StringBuilder query,
			DocumentoFiltro filtro) {
		List<ItemDetalhe> listaOutrospagamento = filtro
				.getListaOutrospagamento();
		if (SinedUtil.isListNotEmpty(listaOutrospagamento)) {
			for (Iterator<ItemDetalhe> it = listaOutrospagamento.iterator(); it
					.hasNext();) {
				ItemDetalhe id = it.next();
				String concat = id.getOutrospagamento();
				query
						.append(
								" UPPER(retira_acento(documento.outrospagamento)) LIKE '%")
						.append(Util.strings.tiraAcento(concat.toUpperCase()))
						.append("%' ");
				if (it.hasNext())
					query.append("OR ");
			}
		} else
			query.append(" 0=1 ");
	}

	/**
	 * Atribui a cl�usula referente a lista de fornecedor, se estiver presente
	 * no filtro.
	 * 
	 * @param query
	 * @param Filtro
	 * @author Fl�vio Tavares
	 */
	private void adicionaListaFornecedor(StringBuilder query,
			DocumentoFiltro filtro) {
		if (SinedUtil.isListNotEmpty(filtro.getListaFornecedor())) {
			query.append(" documento.cdpessoa IN ( ").append(
					CollectionsUtil.listAndConcatenate(filtro
							.getListaFornecedor(), "fornecedor.cdpessoa", ","))
					.append(" ) ");
		} else
			query.append(" 0=1 ");
	}

	/**
	 * Atribui a cl�usula referente a lista de tipo de documentos, se estiver
	 * presente no filtro.
	 * 
	 * @param query
	 * @param Filtro
	 * @author Leandro Lima
	 */
	private void adicionaListaDocumentoTipo(StringBuilder query,
			DocumentoFiltro filtro) {
		if (filtro.getListaDocumentotipo() != null
				&& filtro.getListaDocumentotipo().size() > 0) {
			query.append("AND documentotipo.cddocumentotipo in (").append(
					CollectionsUtil.listAndConcatenate(filtro
							.getListaDocumentotipo(), "cddocumentotipo", ","))
					.append(" ) ");
		}

	}

	/**
	 * Atribui a cl�usula referente a empresa, se estiver presente no filtro.
	 * 
	 * @param query
	 * @param cdEmpresa
	 * @author Leandro Lima
	 */
	private void adicionaEmpresa(StringBuilder query, Integer cdEmpresa) {
		if (cdEmpresa != null) {
			query
				.append(" AND (documento.cdempresa = ")
				.append(cdEmpresa.toString())
				.append(" OR documento.cdempresa is null)");
		}
	}

	/**
	 * Atribui a cl�usula referente a lista de empresas, se estiver presente no
	 * filtro.
	 * 
	 * @param query
	 * @param Filtro
	 * @author Leandro Lima
	 */
	private void adicionaListaEmpresa(StringBuilder query,
			DocumentoFiltro filtro) {
		if (Util.booleans.isFalse(filtro.getAllEmpresa()) && filtro.getListaEmpresa() != null && filtro.getListaEmpresa().size() > 0)
			query
				.append(" AND documento.cdempresa in ( ")
				.append(CollectionsUtil.listAndConcatenate(filtro.getListaEmpresa(), "empresa.cdpessoa", ","))
				.append(" ) ");

	}

	/**
	 * Atribui a cl�usula referente a data de vencimento, se estiver presente no
	 * filtro.
	 * 
	 * @param query
	 * @param dtInicio
	 * @param dtFim
	 * @author Leandro Lima
	 */
	private void adicionaVencimento(StringBuilder query, Date dtInicio, Date dtFim) {
		if (dtInicio != null) {
			query.append(" AND documento.dtvencimento >= '").append(
					SinedDateUtils.toStringPostgre(dtInicio)).append("' ");
		}
		
		if (dtFim != null) {
			query.append(" AND documento.dtvencimento <= '").append(
					SinedDateUtils.toStringPostgre(dtFim)).append("' ");
		}
	}
	
	private void adicionaPrimeiroVencimento(StringBuilder query, Date dtInicio, Date dtFim) {
		if(dtInicio != null || dtFim != null){
			query.append(" AND exists(select dhist.cddocumentohistorico " +
								"from Documentohistorico dhist " +
								"where dhist.cddocumento = documento.cddocumento " +
								"and dhist.cddocumentohistorico = (select min(dhist2.cddocumentohistorico) from Documentohistorico dhist2 where dhist2.cddocumento = documento.cddocumento) " +
								(dtInicio != null ? "and dhist.dtvencimento >= '" + SinedDateUtils.toStringPostgre(dtInicio) + "' " : "") + 
								(dtFim != null ? "and dhist.dtvencimento <= '" + SinedDateUtils.toStringPostgre(dtFim) + "' " : "") + 
							") ");
		}
	}

	private void adicionaBaixa(StringBuilder query, Date dtbaixa1, Date dtbaixa2) {
		if (dtbaixa1 != null || dtbaixa2 != null) {
			String parametro = parametrogeralService.getValorPorNome(Parametrogeral.DATA_PAGAMENTO_ANTECIPACAO);
			parametro = parametro != null ? br.com.linkcom.utils.StringUtils.tiraAcento(parametro).toUpperCase() : "";
			
			String whereAntecipacao = null;
			if ("ANTECIPACAO".equalsIgnoreCase(parametro)) {
				whereAntecipacao = " or documento.cddocumento in (select ha.cddocumentoreferencia " +
														"from HistoricoAntecipacao ha " +
														"where 1 = 1 " +
														"and ha.cddocumentoreferencia = documento.cddocumento " +
														"and ha.cddocumento in (select mo.cddocumento " +
																				"from Movimentacaoorigem mo " +
																				"join movimentacao m on m.cdmovimentacao = mo.cdmovimentacao " +
																				"where m.cdmovimentacaoacao <> 0 " +
																				(dtbaixa1 != null ? "and coalesce(m.dtbanco, m.dtmovimentacao) >= '" + dtbaixa1 + "' " : "") +
																				(dtbaixa2 != null ? "and coalesce(m.dtbanco, m.dtmovimentacao) <= '" + dtbaixa2 + "' " : "") +
																				"and mo.cddocumento = ha.cddocumento)" +
														") ";
			} else if ("BAIXA".equalsIgnoreCase(parametro)) {
				whereAntecipacao = " or documento.cddocumento in (select ha.cddocumento " +
														"from HistoricoAntecipacao ha " +
														"where 1 = 1 " +
														(dtbaixa1 != null ? "and ha.dataHora >= '" + dtbaixa1 + "' " : "") +
														(dtbaixa2 != null ? "and ha.dataHora <= '" + dtbaixa2 + "' " : "") +
														"and ha.cddocumentoreferencia = documento.cddocumento " +
														") ";
			}
			
			
			query
					.append(" AND (DOCUMENTO.CDDOCUMENTO IN ( "
							+ "SELECT D2.CDDOCUMENTO "
							+ "FROM DOCUMENTO D2 "
							+ "JOIN MOVIMENTACAOORIGEM MO ON MO.CDDOCUMENTO = D2.CDDOCUMENTO "
							+ "JOIN MOVIMENTACAO M ON M.CDMOVIMENTACAO = MO.CDMOVIMENTACAO "
							+ "WHERE 1 = 1 " 
							+ "AND M.CDMOVIMENTACAOACAO <> 0 "
							+ (dtbaixa1 != null ? "AND COALESCE(M.DTBANCO, M.DTMOVIMENTACAO) >= ' " + SinedDateUtils.toStringPostgre(dtbaixa1) + "'" : " ")
							+ (dtbaixa2 != null ? "AND COALESCE(M.DTBANCO, M.DTMOVIMENTACAO) <= ' " + SinedDateUtils.toStringPostgre(dtbaixa2) + "'" : " ") 
							+ " )"
							+ " OR "
							+ "DOCUMENTO.CDDOCUMENTO IN ( "
							+ "SELECT D2.CDDOCUMENTO "
							+ "FROM DOCUMENTO D2 "
							+ "JOIN VALECOMPRAORIGEM VCO ON VCO.CDDOCUMENTO = D2.CDDOCUMENTO "
							+ "JOIN VALECOMPRA VC ON VC.CDVALECOMPRA = VCO.CDVALECOMPRA "
							+ "AND VC.CDTIPOOPERACAO = " + Tipooperacao.TIPO_DEBITO.getCdtipooperacao() + " "
							+ (dtbaixa1 != null ? "AND VC.DATA >= ' " + SinedDateUtils.toStringPostgre(dtbaixa1) + "'" : " ")
							+ (dtbaixa2 != null ? "AND VC.DATA <= ' " + SinedDateUtils.toStringPostgre(dtbaixa2) + "'" : " ")
							+ " ) "
							+ whereAntecipacao
							+ " ) ");
		}
	}

	/**
	 * Atribui a cl�usula referente a data de emissao, se estiver presente no
	 * filtro.
	 * 
	 * @param query
	 * @param dtInicio
	 * @param dtFim
	 * @author Leandro Lima
	 */
	private void adicionaEmissao(StringBuilder query, Date dtInicio, Date dtFim) {
		if (dtInicio != null) {
			query.append(" AND documento.dtemissao >= '").append(
					SinedDateUtils.toStringPostgre(dtInicio)).append("' ");
		}
		if (dtFim != null) {
			query.append(" AND documento.dtemissao <= '").append(
					SinedDateUtils.toStringPostgre(dtFim)).append("' ");
		}
	}
	
	private void adicionaCompetencia(StringBuilder query, Date dtInicio, Date dtFim) {
		if (dtInicio != null) {
			query.append(" AND documento.dtcompetencia >= '").append(
					SinedDateUtils.toStringPostgre(dtInicio)).append("' ");
		}
		if (dtFim != null) {
			query.append(" AND documento.dtcompetencia <= '").append(
					SinedDateUtils.toStringPostgre(dtFim)).append("' ");
		}
	}
	
	private void adicionaEmissaoNota(StringBuilder query, Date dtInicio, Date dtFim) {
		if (dtInicio != null) {
			query.append(" AND nota.dtEmissao >= '").append(
					SinedDateUtils.toStringPostgre(dtInicio)).append("' ");
		}
		if (dtFim != null) {
			query.append(" AND nota.dtEmissao <= '").append(
					SinedDateUtils.toStringPostgre(dtFim)).append("' ");
		}
	}
	
	private void adicionaNumeroNota(StringBuilder query, Long numeroNotaFrom, Long numeroNotaTo) {
		if (numeroNotaFrom != null) {
			query.append(" AND cast(nota.numero AS Numeric) >= ").append(numeroNotaFrom).append(" ");
		}
		if (numeroNotaTo != null) {
			query.append(" AND cast(nota.numero AS Numeric) <= ").append(numeroNotaTo).append(" ");
		}
	}
	
	private void adicionaNumeroNfseNota(StringBuilder query, Integer numeroNfseFrom, Integer numeroNfseTo) {
		if (numeroNfseFrom != null) {
			query.append(" AND arquivonfnota.numero_filtro >= '").append(numeroNfseFrom).append("' ");
		}
		if (numeroNfseTo != null) {
			query.append(" AND arquivonfnota.numero_filtro <= '").append(numeroNfseTo).append("' ");
		}
	}
	
	private void adicionaNumeroFatura(StringBuilder query, Integer numeroFaturaFrom, Integer numeroFaturaTo) {
		if (numeroFaturaFrom != null) {
			query.append(" AND contratofaturalocacao.numero >= ").append(numeroFaturaFrom).append(" ");
		}
		if (numeroFaturaTo != null) {
			query.append(" AND contratofaturalocacao.numero <= ").append(numeroFaturaTo).append(" ");
		}
	}

	/**
	 * Atribui a cl�usula referente a valor do documento, se estiver presente no
	 * filtro.
	 * 
	 * @param query
	 * @param Valorminimo
	 * @param Valormaximo
	 * @author Leandro Lima
	 */
	private void adicionaValor(StringBuilder query, Money Valorminimo,
			Money Valormaximo) {
		if (Valorminimo != null)
			query.append(" AND documento.valor >= ").append(
					Valorminimo.toLong()).append(" ");

		if (Valormaximo != null)
			query.append(" AND documento.valor <= ").append(
					Valormaximo.toLong()).append(" ");
	}

	/**
	 * Atribui a cl�usula referente a acao, se estiver presente no filtro.
	 * 
	 * @param query
	 * @param cddocumentoacao
	 * @author Leandro Lima
	 */
	private void adicionaListaAcao(StringBuilder query, String cddocumentoacao) {
		if (cddocumentoacao != null && !cddocumentoacao.equals(""))
			query.append(" AND documento.cddocumentoacao in (").append(
					cddocumentoacao).append(") ");
	}

	/**
	 * Deleta as contas a pagar ao estornar uma despesa de viagem
	 * 
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void deleteDocumentoFromDespesa(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		getJdbcTemplate()
				.execute(
						"DELETE "
								+ "FROM DOCUMENTO  "
								+ "WHERE EXISTS( "
								+ "SELECT 1 "
								+ "FROM DOCUMENTOORIGEM "
								+ "WHERE DOCUMENTO.CDDOCUMENTO = DOCUMENTOORIGEM.CDDOCUMENTO "
								+ "AND (DOCUMENTOORIGEM.CDDESPESAVIAGEMADIANTAMENTO IN ("
								+ whereIn
								+ ")  "
								+ "OR DOCUMENTOORIGEM.CDDESPESAVIAGEMACERTO IN ("
								+ whereIn + ")) " + ")");
	}

	/**
	 * Faz a chamada da procedure para atualizar o valor atual do documento
	 * passado.
	 * 
	 * @param documento
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaDocumento(Documento documento) {
		if (documento == null || documento.getCddocumento() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		String context = "w3erp";
		if(Neo.isInRequestContext()){
			context = NeoWeb.getRequestContext().getServletRequest().getContextPath();
		}
		SinedUtil.desmarkAsReader();
		getJdbcTemplate().execute("SELECT ATUALIZA_DOCUMENTO(" + documento.getCddocumento() + ", '" + context + "')");
	}

	/**
	 * Chama a fun��o do BD para atualizar os aux_documento.
	 * 
	 * @author Rodrigo Freitas
	 */
	public void atualizaAux() {
		getJdbcTemplate().execute("SELECT atualiza_todos_documentos()");
	}

	/**
	 * M�todo para carregar um documento para a tela de Gerar Receita.
	 * 
	 * @param documento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Documento loadForReceita(Documento documento) {
		if (!SinedUtil.isObjectValid(documento)) {
			throw new SinedException("Documento inv�lido.");
		}

		return query()
				.select(
						"documento.cddocumento,documento.dtvencimento,documento.valor,documento.descricao,"
								+ "documento.outrospagamento,pessoa.nome")
				.leftOuterJoin("documento.pessoa pessoa").where(
						"documento = ?", documento).unique();
	}

	/**
	 * Busca uma lista de documentos pertencentes a uma nota.
	 * 
	 * @param nota
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documento> findByNota(Nota nota) {
		if (!SinedUtil.isObjectValid(nota)) {
			throw new SinedException("Par�metro nota inv�lido.");
		}

		return querySined()
				.select("documento.cddocumento,documento.dtvencimento,documento.valor," +
						"documento.descricao,documento.outrospagamento,pessoa.nome,documentoacao.cddocumentoacao, " +
						"documento.mensagem1, documento.mensagem2 ")
				.leftOuterJoin("documento.pessoa pessoa")
				.join("documento.documentoacao documentoacao")
				.join("documento.listaNotaDocumento notaDocumento")
				.join("notaDocumento.nota nota")
				.where("nota = ?", nota)
				.where("documentoacao <> ?", Documentoacao.CANCELADA)
				.orderBy("documento.dtvencimento, documento.cddocumento")
				.list();
	}
	
	public List<Documento> findNotaDiferenteCanceladaNegociada(Nota nota) {
		if (!SinedUtil.isObjectValid(nota)) {
			throw new SinedException("Par�metro nota inv�lido.");
		}
		
		return querySined()
				.select("documento.cddocumento,documento.dtvencimento,documento.valor," +
						"documento.descricao,documento.outrospagamento,pessoa.nome,documentoacao.cddocumentoacao, " +
						"documento.mensagem1, documento.mensagem2 ")
						.leftOuterJoin("documento.pessoa pessoa")
						.join("documento.documentoacao documentoacao")
						.join("documento.listaNotaDocumento notaDocumento")
						.join("notaDocumento.nota nota")
						.where("nota = ?", nota)
						.where("documentoacao <> ?", Documentoacao.CANCELADA)
						.where("documentoacao <> ?", Documentoacao.NEGOCIADA)
						.orderBy("documento.dtvencimento, documento.cddocumento")
						.list();
	}
	
	public List<Documento> findForAntecipacao(Pessoa pessoa, Documentotipo documentotipo, boolean consideraPedido, Pedidovenda pedidovenda, Documentoclasse documentoClasse) {
		return this.findForAntecipacao(pessoa, documentotipo, consideraPedido, pedidovenda, null, documentoClasse);
	}
	
	/**
	 * Busca uma lista de documentos de antecipa��o.
	 * 
	 * @param documentotipo
	 * @return
	 * @author Marden Silva
	 */
	@SuppressWarnings("unchecked")
	public List<Documento> findForAntecipacao(Pessoa pessoa, Documentotipo documentotipo, boolean consideraPedido, Pedidovenda pedidovenda, Documento documento, Documentoclasse documentoClasse) {
		if (!SinedUtil.isObjectValid(pessoa)) {
			throw new SinedException("Par�metro cliente inv�lido.");
		}

		String sql = createSqlDocumentoAntecipacao(pessoa.getCdpessoa().toString(), documentotipo, consideraPedido, pedidovenda, documento, documentoClasse);
		
		SinedUtil.markAsReader();
		List<Documento> antecipacoes = getJdbcTemplate().query(sql, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Documento documento = new Documento();

				documento.setCddocumento(rs.getInt("cddocumento"));
				documento.setNumero(rs.getString("numero"));
				documento.setValor(new Money(rs.getLong("pago"), true));
				documento.setDescricao(rs.getString("descricao"));
				
				return documento;
			}

		});
			
		List<Documento> listaDocumento = new ArrayList<Documento>();
		
		for (Documento antecipacao : antecipacoes){
			if (antecipacao != null && antecipacao.getValor() != null && antecipacao.getValor().getValue().doubleValue() > 0)
				listaDocumento.add(antecipacao);
		}

		return listaDocumento;
	}
	
	private String createSqlDocumentoAntecipacao(String whereIn, Documentotipo documentotipo, boolean consideraPedido, Pedidovenda pedidovenda, Documentoclasse documentoClasse){
		return this.createSqlDocumentoAntecipacao(whereIn, documentotipo, consideraPedido, pedidovenda, null, documentoClasse);
	}
	
	/**
	* M�todo que cria select de documento de antecipa��o
	*
	* @param whereIn
	* @param documentotipo
	* @param consideraPedido
	* @param pedidovenda
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	private String createSqlDocumentoAntecipacao(String whereIn, Documentotipo documentotipo, boolean consideraPedido, Pedidovenda pedidovenda, Documento documento, Documentoclasse documentoClasse){
		String sql = 
			"select d.cdpessoa, d.cddocumento, d.numero, d.descricao, (d.valor - " + 
			"	       coalesce((select sum(vp.valororiginal) from venda v " +
			"					   join vendapagamento vp on vp.cdvenda = v.cdvenda     " +
			"	                   where vp.cddocumentoantecipacao = d.cddocumento " +
			"					   and v.cdvendasituacao <> " + Vendasituacao.CANCELADA.getCdvendasituacao() +
			(consideraPedido ? " and (v.cdpedidovenda is null or not exists ( select pvp.cdpedidovenda " +
																			" from pedidovendapagamento pvp " +
																			" where pvp.cdpedidovenda = v.cdpedidovenda and " +
																			" pvp.cddocumentoantecipacao = d.cddocumento) )" : "") +
			"					), 0) - " +
			"	       coalesce((select sum(docDevolvido.valor) from Documento docDevolvido " +
			"	                   where docDevolvido.cddocumento = d.cddocumentorelacionado " +
			"					   and docDevolvido.cddocumentoclasse = " + Documentoclasse.CD_PAGAR +
			"					   and docDevolvido.cddocumentoacao = " + Documentoacao.BAIXADA.getCddocumentoacao() +"), 0) - " +
			"  		   coalesce((SELECT sum(ha.valor) " +
			"					FROM historicoantecipacao ha " +
			"					WHERE ha.cddocumento = d.cddocumento ), 0) " +
			"  		   - coalesce((SELECT sum(ep.valor) " +
			"					FROM entregapagamento ep " +
			"					join entregadocumento ed on ed.cdentregadocumento = ep.cdentregadocumento "	+
			"					WHERE ep.cddocumentoantecipacao = d.cddocumento " +
			"					and ed.entregadocumentosituacao in ( " + Entregadocumentosituacao.FATURADA.getValue() + ", " + Entregadocumentosituacao.REGISTRADA.getValue() + ")), 0) ";
			if (consideraPedido){
				sql +=
					"	     - coalesce((select sum(vp.valororiginal) from pedidovendapagamento vp " +
					"					   join pedidovenda pv on pv.cdpedidovenda = vp.cdpedidovenda " +
					"	                   where vp.cddocumentoantecipacao = d.cddocumento " +
											(pedidovenda != null && pedidovenda.getCdpedidovenda() != null ? 
													" and vp.cdpedidovenda <> " + pedidovenda.getCdpedidovenda() : "") +
					"					   and pv.pedidovendasituacao <> " + Pedidovendasituacao.CANCELADO.getValue() + "), 0) ";				
			}
			sql += 
			") as pago " + 		
			"	from documento d " +
			"   join documentotipo dt on d.cddocumentotipo = dt.cddocumentotipo and dt.antecipacao = true" +
			"	where d.cdpessoa in ( " + whereIn + ") ";
			
			if (documentotipo != null && documentotipo.getCddocumentotipo() != null){
				sql += " and dt.cddocumentotipo = " + documentotipo.getCddocumentotipo();
			}else {
				sql += " and coalesce(dt.antecipacao, false) = true ";
			}
			
			sql += " and d.cddocumentoacao = " + Documentoacao.BAIXADA.getCddocumentoacao();
			sql += " and d.cddocumentoclasse = " + (Documentoclasse.OBJ_PAGAR.equals(documentoClasse)? Documentoclasse.CD_PAGAR: Documentoclasse.CD_RECEBER);
			if(Util.objects.isPersistent(documento)){
				sql += " and d.cddocumento = " + documento.getCddocumento();
			}
			
		return sql;
	}
	
	@SuppressWarnings("unchecked")
	public List<GenericBean> findSaldoAntecipacao(String whereIn, Documentoclasse documentoClasse) {
		if (StringUtils.isBlank(whereIn)) {
			throw new SinedException("Par�metro inv�lido.");
		}

		String sql = " select query.cdpessoa as cdcliente, sum(coalesce(query.pago, 0)) as saldoantecipacao" +
					 " from (" +
					 createSqlDocumentoAntecipacao(whereIn, null, true, null, documentoClasse) + 
					 " ) query " +
					 " group by query.cdpessoa ";
		
		SinedUtil.markAsReader();
		 List<GenericBean> list = getJdbcTemplate().query(sql ,new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new GenericBean(rs.getInt("cdcliente"), rs.getDouble("saldoantecipacao")/100d);
				}
			});
			
		return list;
	}
	
	/**
	 * Busca uma lista de documentos de antecipa��o j� utilizados totalmente
	 *
	 * @param cliente
	 * @param documentotipo
	 * @param b
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<Documento> findForAntecipacaoConsideraVendaUtilizados(Cliente cliente, Documentotipo documentotipo, boolean b) {
		if (!SinedUtil.isObjectValid(cliente)) {
			throw new SinedException("Par�metro cliente inv�lido.");
		}

		String sql = 
		"select d.cddocumento, d.numero, (d.valor - " + 
		"	       coalesce((select sum(vp.valororiginal) from venda v " +
		"					   join vendapagamento vp on vp.cdvenda = v.cdvenda     " +
		"	                   where vp.cddocumentoantecipacao = d.cddocumento " +
		"					   and v.cdvendasituacao <> " + Vendasituacao.CANCELADA.getCdvendasituacao() +"), 0) - " +
		"	       coalesce((select sum(docDevolvido.valor) from Documento docDevolvido " +
		"	                   where docDevolvido.cddocumento = d.cddocumentorelacionado " +
		"					   and docDevolvido.cddocumentoclasse = " + Documentoclasse.CD_PAGAR +
		"					   and docDevolvido.cddocumentoacao = " + Documentoacao.BAIXADA.getCddocumentoacao() +"), 0) ";
		sql += 
		") as pago " + 		
		"	from documento d " +
		"   join documentotipo dt on d.cddocumentotipo = dt.cddocumentotipo and dt.antecipacao = true" +
		"	where d.cdpessoa = " + cliente.getCdpessoa();
		if (documentotipo != null && documentotipo.getCddocumentotipo() != null)
			sql += " and dt.cddocumentotipo = " + documentotipo.getCddocumentotipo();
		
		sql += " and d.cddocumentoacao = " + Documentoacao.BAIXADA.getCddocumentoacao();
		sql += " and d.cddocumentoclasse = " + Documentoclasse.CD_RECEBER;
		
		SinedUtil.markAsReader();
		List<Documento> antecipacoes = getJdbcTemplate().query(sql, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Documento documento = new Documento();

				documento.setCddocumento(rs.getInt("cddocumento"));
				documento.setNumero(rs.getString("numero"));
				documento.setValor(new Money(rs.getLong("pago"), true));
				
				return documento;
			}

		});
			
		List<Documento> listaDocumento = new ArrayList<Documento>();
		
		for (Documento antecipacao : antecipacoes){
			if (antecipacao != null && antecipacao.getValor() != null && antecipacao.getValor().getValue().doubleValue() == 0)
				listaDocumento.add(antecipacao);
		}

		return listaDocumento;
	}
	
	/**
	 * Busca uma lista de documentos de antecipa��o. (para a entrega)
	 *
	 * @param fornecedor
	 * @param documentotipo
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<Documento> findForAntecipacaoEntrega(Pessoa pessoa, Documentotipo documentotipo) {
		if (!SinedUtil.isObjectValid(pessoa)) {
			throw new SinedException("Par�metro pessoa inv�lido.");
		}

		String sql = 
		"select d.cddocumento, d.numero, d.descricao, (d.valor " + 
		"	    - coalesce((select sum(ep.valor) from entregadocumento ed " +
		"			   join entregapagamento ep on ep.cdentregadocumento = ed.cdentregadocumento " +
		"              where ep.cddocumentoantecipacao = d.cddocumento " +
		"			   and ed.entregadocumentosituacao <> " + Entregadocumentosituacao.CANCELADA.getValue() + "), 0)" +
		"  		- coalesce((SELECT sum(ha.valor) "+
		"					FROM historicoantecipacao ha "+
		"					WHERE ha.cddocumento = d.cddocumento ), 0) " +		
		") as pago " + 		
		"	from documento d " +
		"   join documentotipo dt on d.cddocumentotipo = dt.cddocumentotipo and dt.antecipacao = true" +
		"	where d.cdpessoa = " + pessoa.getCdpessoa();
		if (documentotipo != null && documentotipo.getCddocumentotipo() != null)
			sql += " and dt.cddocumentotipo = " + documentotipo.getCddocumentotipo();
		
		sql += " and d.cddocumentoacao = " + Documentoacao.BAIXADA.getCddocumentoacao();
		sql += " and d.cddocumentoclasse = " + Documentoclasse.CD_PAGAR;
		SinedUtil.markAsReader();
		List<Documento> antecipacoes = getJdbcTemplate().query(sql, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Documento documento = new Documento();
				
				documento.setDescricao(rs.getString("descricao"));
				documento.setCddocumento(rs.getInt("cddocumento"));
				documento.setNumero(rs.getString("numero"));
				documento.setValor(new Money(rs.getLong("pago"), true));
				
				return documento;
			}

		});
			
		List<Documento> listaDocumento = new ArrayList<Documento>();
		
		for (Documento antecipacao : antecipacoes){
			if (antecipacao != null && antecipacao.getValor() != null && antecipacao.getValor().getValue().doubleValue() > 0)
				listaDocumento.add(antecipacao);
		}

		return listaDocumento;
	}
	
	@SuppressWarnings("unchecked")
	public List<Documento> findForAntecipacaoEntregaContaReceber(Pessoa pessoa, Documentotipo documentotipo) {
		if (!SinedUtil.isObjectValid(pessoa)) {
			throw new SinedException("Par�metro pessoa inv�lido.");
		}

		String sql = 
		"select d.cddocumento, d.numero, d.descricao, (d.valor " + 
		"	    - coalesce((select sum(ep.valor) from entregadocumento ed " +
		"			   join entregapagamento ep on ep.cdentregadocumento = ed.cdentregadocumento " +
		"              where ep.cddocumentoantecipacao = d.cddocumento " +
		"			   and ed.entregadocumentosituacao <> " + Entregadocumentosituacao.CANCELADA.getValue() + "), 0)" +
		"  		- coalesce((SELECT sum(ha.valor) "+
		"					FROM historicoantecipacao ha "+
		"					WHERE ha.cddocumento = d.cddocumento ), 0) " +		
		") as pago " + 		
		"	from documento d " +
		"   join documentotipo dt on d.cddocumentotipo = dt.cddocumentotipo and dt.antecipacao = true" +
		"	where d.cdpessoa = " + pessoa.getCdpessoa();
		if (documentotipo != null && documentotipo.getCddocumentotipo() != null)
			sql += " and dt.cddocumentotipo = " + documentotipo.getCddocumentotipo();
		
		sql += " and d.cddocumentoacao = " + Documentoacao.BAIXADA.getCddocumentoacao();
		sql += " and d.cddocumentoclasse = " + Documentoclasse.CD_RECEBER;
		SinedUtil.markAsReader();
		List<Documento> antecipacoes = getJdbcTemplate().query(sql, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Documento documento = new Documento();
				
				documento.setDescricao(rs.getString("descricao"));
				documento.setCddocumento(rs.getInt("cddocumento"));
				documento.setNumero(rs.getString("numero"));
				documento.setValor(new Money(rs.getLong("pago"), true));
				
				return documento;
			}

		});
			
		List<Documento> listaDocumento = new ArrayList<Documento>();
		
		for (Documento antecipacao : antecipacoes){
			if (antecipacao != null && antecipacao.getValor() != null && antecipacao.getValor().getValue().doubleValue() > 0)
				listaDocumento.add(antecipacao);
		}

		return listaDocumento;
	}
	
	/**
	 * Busca uma lista de documentos de antecipa��o j� utilizados totalmente (relacionado a entrega)
	 *
	 * @param cliente
	 * @param documentotipo
	 * @param b
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<Documento> findForAntecipacaoConsideraEntregaUtilizados(Fornecedor fornecedor, Documentotipo documentotipo) {
		if (!SinedUtil.isObjectValid(fornecedor)) {
			throw new SinedException("Par�metro cliente inv�lido.");
		}

		String sql = 
		"select d.cddocumento, d.numero, d.descricao, (d.valor " + 
		"	    - coalesce((select sum(ep.valor) from entregadocumento ed " +
		"					   join entregapagamento ep on ep.cdentregadocumento = ed.cdentregadocumento     " +
		"	                   where ep.cddocumentoantecipacao = d.cddocumento " +
		"					   and ed.entregadocumentosituacao <> " + Entregadocumentosituacao.CANCELADA.getValue() +"), 0)" +
		"  		- coalesce((SELECT sum(ha.valor) "+
		"					FROM historicoantecipacao ha "+
		"					WHERE ha.cddocumento = d.cddocumento ), 0) " +
		") as pago " + 		
		"	from documento d " +
		"   join documentotipo dt on d.cddocumentotipo = dt.cddocumentotipo and dt.antecipacao = true" +
		"	where d.cdpessoa = " + fornecedor.getCdpessoa();
		if (documentotipo != null && documentotipo.getCddocumentotipo() != null)
			sql += " and dt.cddocumentotipo = " + documentotipo.getCddocumentotipo();
		
		sql += " and d.cddocumentoacao = " + Documentoacao.BAIXADA.getCddocumentoacao();
		sql += " and d.cddocumentoclasse = " + Documentoclasse.CD_PAGAR;
		SinedUtil.markAsReader();
		List<Documento> antecipacoes = getJdbcTemplate().query(sql, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Documento documento = new Documento();

				documento.setCddocumento(rs.getInt("cddocumento"));
				documento.setNumero(rs.getString("numero"));
				documento.setValor(new Money(rs.getLong("pago"), true));
				
				return documento;
			}

		});
			
		List<Documento> listaDocumento = new ArrayList<Documento>();
		
		for (Documento antecipacao : antecipacoes){
			if (antecipacao != null && antecipacao.getValor() != null && antecipacao.getValor().getValue().doubleValue() == 0)
				listaDocumento.add(antecipacao);
		}

		return listaDocumento;
	}

	/**
	 * Carrega informa��es dos documentos para impress�o de boletos.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Documento> findForBoleto(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return querySined()
				.select("conta.cdconta, conta.nome, conta.numero, conta.dvnumero, conta.agencia, conta.dvagencia, " +
				     	"conta.limite, conta.diavencimento, conta.diafechamento, conta.titular, conta.localpagamento, " +
						"conta.sequencial, conta.conciliacaoautomatica, conta.taxaboleto, conta.codigoempresasispag, " +
						"conta.nossonumerointervalo, conta.nossonumeroinicial, conta.nossonumerofinal, " +						
						"contacarteira.cdcontacarteira, contacarteira.convenio, contacarteira.conveniolider, contacarteira.carteira, contacarteira.carteiracarne, " +
						"contacarteira.codigocarteira, contacarteira.tipoarquivoremessa, contacarteira.fatorvencimentozerado, contacarteira.msgboleto1, " +
						"contacarteira.msgboleto2, contacarteira.bancogeranossonumero, contacarteira.naogerarboleto, contacarteira.variacaocarteira, " +
						"contacarteira.idemissaoboleto, contacarteira.idemissaoboletoimpresso, contacarteira.iddistribuicao, contacarteira.tipocobranca, " +
						"contacarteira.instrucao1, contacarteira.instrucao2, contacarteira.qtdedias, contacarteira.limiteposicoesintervalonossonumero, " +
						"contacarteira.especiedocumento, contacarteira.codigotransmissao, contacarteira.especiedocumentoremessa, contacarteira.aceito, " +					 
						"contacarteira.protestoautomaticoremessa, contacarteira.enviardescontoremessa, contacarteira.enviarmultaremessa, contacarteira.enviarmoraremessa, " +
						"contacarteira.padrao, contacarteira.boletocobranca, contacarteira.aprovadoproducao, contacarteira.operacao, contacarteira.escritural, " +				
						"banco.nome, banco.numero, documento.numero, " +						
					    "documento.dtemissao, documento.dtvencimento, aux_documento.valoratual, documento.cddocumento, documento.valor, " +
					    "documento.tipopagamento, documento.descricao, documento.observacao, documento.mensagem1, documento.mensagem2, documento.mensagem3, " +  
					    "documento.mensagem4, documento.mensagem5, documento.mensagem6, documento.nossonumero, documento.outrospagamento," +
					    "documento.boletogerado," +
					    "reportboleto.cdreporttemplate, reportboleto.nome, " +
					    "listaDocumentoOrigem.cddocumentoorigem, " +
					    "listaParcela.cdparcela, listaParcela.ordem, " +
					    "venda.cdvenda, " +
					    "taxa.cdtaxa, " +					    
					    "pessoa.cdpessoa, pessoa.email, pessoa.nome, " +
					    "contato.responsavelos, contato.nome, contato.emailcontato, " +
					    "endereco.cdendereco, " +					    
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.email, empresa.textoboleto, logomarca.cdarquivo, " +
					    "listaTelefone.telefone, telefonetipo.cdtelefonetipo, empresa.textoautenticidadeboleto, empresa.formaEnvioBoleto, " +
						"contaempresa.cdpessoa, contaempresa.nome, contaempresa.razaosocial, contaempresa.cnpj, contaempresa.cpf, listaEndereco.cdendereco, " +											
						"contrato.cdcontrato, contrato.dtcancelamento, documentotipo.boleto, documentotipo.cddocumentotipo, " +
						"documentoacao.cddocumentoacao, listaEnderecoEmpresaDoc.cdendereco, enderecotipoEmpresaDoc.cdenderecotipo, enderecotipo.cdenderecotipo, empresa.cnpj, empresa.cpf, empresa.emailfinanceiro, " +
						"listaEnderecoEmpresatitular.cdendereco, enderecotipoEmpresatitular.cdenderecotipo, empresatitular.cdpessoa, empresatitular.cnpj, empresatitular.cpf," +
						"empresatitular.nome, empresatitular.razaosocial, empresatitular.nomefantasia, empresatitular.email, empresa.textoboleto, logomarcaEmpresatitular.cdarquivo, " +
						"listaBoletodigital.cdboletodigital, listaBoletodigital.nossonumero, listaBoletodigital.codigodebarras, listaBoletodigital.linhadigitavel, listaBoletodigital.carteira")						
				.join("documento.documentoacao documentoacao")
				.leftOuterJoin("documento.conta conta")
				.leftOuterJoin("documento.contacarteira contacarteira")
				.leftOuterJoin("conta.banco banco")
				.leftOuterJoin("conta.listaContaempresa listaContaempresa")
				.leftOuterJoin("listaContaempresa.empresa contaempresa")
				.leftOuterJoin("contaempresa.listaEndereco listaEndereco")
				.leftOuterJoin("listaEndereco.enderecotipo enderecotipo")
				.join("documento.aux_documento aux_documento")
				.leftOuterJoin("documento.pessoa pessoa")
				.leftOuterJoin("pessoa.cliente cliente")
				.leftOuterJoin("cliente.listaContato listaContato")
				.leftOuterJoin("listaContato.contato contato")
				.leftOuterJoin("documento.endereco endereco")
				.leftOuterJoin("documento.empresa empresa")
				.leftOuterJoin("empresa.cdreporttemplateboleto reportboleto")
				.leftOuterJoin("empresa.listaEndereco listaEnderecoEmpresaDoc")
				.leftOuterJoin("empresa.listaTelefone listaTelefone")
				.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
				.leftOuterJoin("listaEnderecoEmpresaDoc.enderecotipo enderecotipoEmpresaDoc")
				.leftOuterJoin("empresa.logomarca logomarca")
				.leftOuterJoin("documento.taxa taxa")
				.leftOuterJoin("documento.listaDocumentoOrigem listaDocumentoOrigem")
				.leftOuterJoin("listaDocumentoOrigem.contrato contrato")
				.leftOuterJoin("listaDocumentoOrigem.venda venda")
				.leftOuterJoin("documento.listaParcela listaParcela")
				.leftOuterJoin("conta.empresatitular empresatitular")
				.leftOuterJoin("empresatitular.listaEndereco listaEnderecoEmpresatitular")
				.leftOuterJoin("listaEnderecoEmpresatitular.enderecotipo enderecotipoEmpresatitular")
				.leftOuterJoin("empresatitular.logomarca logomarcaEmpresatitular")
				.leftOuterJoin("documento.documentotipo documentotipo")
				.leftOuterJoin("documento.listaBoletodigital listaBoletodigital")
				.whereIn("documento.cddocumento", whereIn)
				.orderBy("documento.cddocumento")
				.list();
	}

	/**
	 * Verifica se o documento n�o est� na situa��o DEFINITIVA e PREVISTA.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean isDocumentoNotDefitivaNotPrevista(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return newQueryBuilderSined(Long.class).select(
				"count(*)").from(Documento.class).join(
				"documento.documentoacao documentoacao").where(
				"documentoacao.cddocumentoacao <> ?",
				Documentoacao.DEFINITIVA.getCddocumentoacao()).where(
				"documentoacao.cddocumentoacao <> ?",
				Documentoacao.PREVISTA.getCddocumentoacao()).whereIn(
				"documento.cddocumento", whereIn).unique() > 0;
	}
	
	public boolean isDocumentoNotBaixadaOrDefinitiva(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return newQueryBuilderSined(Long.class)
					
					.select("count(*)")
					.from(Documento.class)
					.join("documento.documentoacao documentoacao")
					.where(	"documentoacao.cddocumentoacao <> ?", Documentoacao.BAIXADA.getCddocumentoacao())
					.where(	"documentoacao.cddocumentoacao <> ?", Documentoacao.PREVISTA.getCddocumentoacao())
					.where(	"documentoacao.cddocumentoacao <> ?", Documentoacao.DEFINITIVA.getCddocumentoacao())
					.where(	"documentoacao.cddocumentoacao <> ?", Documentoacao.AUTORIZADA.getCddocumentoacao())
					.where(	"documentoacao.cddocumentoacao <> ?", Documentoacao.AUTORIZADA_PARCIAL.getCddocumentoacao())
					.whereIn("documento.cddocumento", whereIn)
					.unique() > 0;
	}
	
	/**
	 * Verifica se o documento possui conta marcada para n�o gerar boleto.
	 * 
	 * @param whereIn
	 * @return
	 * @author Marden Silva
	 */
	public boolean isDocumentoContaNotGeraBoleto(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return newQueryBuilderSined(Long.class).select(
				"count(*)").from(Documento.class).join(
				"documento.contacarteira contacarteira").where(
				"contacarteira.naogerarboleto = true").whereIn(
				"documento.cddocumento", whereIn).unique() > 0;
	}	
	
	/**
	 * Lista os documentos que possuem conta marcada para n�o gerar boleto.
	 * 
	 * @param whereIn
	 * @return
	 * @author Mairon Cezar
	 */
	public List<Documento> findDocumentoContaNotGeraBoleto(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return querySined().select("documento.cddocumento, documento.valor").join(
													"documento.contacarteira contacarteira").where(
													"contacarteira.naogerarboleto = true").whereIn(
													"documento.cddocumento", whereIn).list();
	}	
	
	/**
	 * M�todo que verifica se o documento n�o tem o nosso n�mero e a carteira tem o campo bancogeranossonumero marcado
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean existDocumentoContaNotNossonumero(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Documento.class)
			.join("documento.contacarteira contacarteira")
			.where("contacarteira.bancogeranossonumero = true")
			.openParentheses()
				.where("documento.nossonumero is null")
				.or()
				.where("documento.nossonumero = ''")
			.closeParentheses()
			.whereIn("documento.cddocumento", whereIn)
			.unique() > 0;
	}
	
	/**
	 * M�todo verifica se documento est� negociado
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean isDocumentoNegociado(Documento documento) {
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Documento n�o pode ser nulo");
		
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Documento.class)
					.join("documento.documentoacao documentoacao")
					.where("documentoacao.cddocumentoacao == ?", Documentoacao.NEGOCIADA.getCddocumentoacao())
					.where("documento = ?", documento)
					.unique() > 0;
	}

	public String findWhereInCheque(String cheque) {
		if (cheque != null && !cheque.equals("")) {
			List<Documento> listaDocumento = this.findByCheque(cheque);

			if (listaDocumento != null && listaDocumento.size() > 0) {
				return CollectionsUtil.listAndConcatenate(listaDocumento,"cddocumento", ",");
			} else {
				return "0"; // RETORNA ZERO PARA N�O APARECER NENHUM DOCUMENTO
			}
		} else {
			return null;
		}
	}

	private List<Documento> findByCheque(String cheque) {
		return query().select("documento.cddocumento, documento.valor").join(
				"documento.listaMovimentacaoOrigem origem").join(
				"origem.movimentacao movimentacao").where(
				"movimentacao.cheque like ?", cheque).list();
	}

	/**
	 * M�todo para obter lista de contas a receber para gera��o do arquivo de
	 * remessa
	 * 
	 * @param filtro
	 * @return List<Documento>
	 * @author Jo�o Paulo Zica
	 */
	public List<Documento> findDadosForListagemRemessa(
			GerarArquivoRemessaBean filtro) {
		return querySined()
				
				.select(
						"documento.cddocumento, documento.descricao, documento.dtvencimento, pessoa.cdpessoa,"
								+ "pessoa.nome, aux_documento.valoratual")

				.join("documento.aux_documento aux_documento")
				.join("documento.documentoclasse documentoclasse")
				.leftOuterJoin("documento.conta conta")
				.leftOuterJoin("documento.contacarteira contacarteira")
				.leftOuterJoin("documento.empresa empresa")
				.join("documento.documentoacao documentoacao")
				.join("documento.pessoa pessoa")
				.leftOuterJoin("documento.listaNotaDocumento listaNotaDocumento")
				.leftOuterJoin("listaNotaDocumento.nota nota")
				.leftOuterJoin("documento.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.leftOuterJoin("listaRateioitem.projeto projeto")
				.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
				.where("documentoacao = ?",	Documentoacao.DEFINITIVA)
				.where("documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
				.where("documento.dtvencimento >=?", filtro.getDtinicio())
				.where("documento.dtvencimento <=?", filtro.getDtfim())
				.where("documento.dtemissao >= ?", filtro.getDtemissao1())
				.where("documento.dtemissao <= ?", filtro.getDtemissao2())
				.where("nota.dtEmissao >= ?", filtro.getDtemissaonota1())
				.where("nota.dtEmissao <= ?", filtro.getDtemissaonota2())
				.where("conta=?", filtro.getConta())
				.where("contacarteira=?", filtro.getContacarteira())
				.where("pessoa = ?", filtro.getCliente())
				.where("projeto = ?", filtro.getProjeto())
				.where("centrocusto = ?", filtro.getCentrocusto())
				.openParentheses()
					.where("empresa = ?", filtro.getEmpresa())
					.or()
					.where("empresa is null")
				.closeParentheses()
				.where("not exists (select docconf.cddocumentoconfirmacao from Documentoconfirmacao docconf where docconf.documento = documento)")
				.where("documento.documentotipo=?", filtro.getDocumentotipo(), filtro.getDocumentotipo()!=null && Boolean.TRUE.equals(filtro.getIgualDiferenteDocumentotipo()))
				.where("documento.documentotipo<>?", filtro.getDocumentotipo(), filtro.getDocumentotipo()!=null && Boolean.FALSE.equals(filtro.getIgualDiferenteDocumentotipo()))
				.orderBy("retira_acento(upper(pessoa.nome)), documento.dtvencimento")
				.list();
	}
	
	
	
	
	public List<Documento> findDadosForListagemRemessaBoletoDigitalConfiguravel(BoletoDigitalRemessaBean filtro) {
		if(filtro.getCurrentPage() == null) filtro.setCurrentPage(0);
		if(filtro.getPageSize() == null) filtro.setPageSize(100);
		
		QueryBuilder<Documento> query = querySined();
		
		boolean faturalocacao = parametrogeralService.getBoolean(Parametrogeral.FATURA_LOCACAO);
		
		query
			.select("distinct documento.cddocumento, documento.descricao, documento.dtvencimento, pessoa.cdpessoa," +
					"pessoa.nome, aux_documento.valoratual, documento.dtemissao, boletodigital.cdboletodigital, boletodigital.statusintegracao, boletodigital.msgintegracao " +
					(faturalocacao ? ", contratofaturalocacao.numero " : ""))
			.join("documento.aux_documento aux_documento")
			.join("documento.pessoa pessoa")
			.leftOuterJoin("documento.listaBoletodigital boletodigital")
			.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
			.where("documento.dtemissao >=?", filtro.getDtinicio())
			.where("documento.dtemissao <=?", filtro.getDtfim())
			.where("documento.dtvencimento >=?", filtro.getDtvencimento1())
			.where("documento.dtvencimento <=?", filtro.getDtvencimento2())
			.where("documento.conta=?", filtro.getConta())
			.where("documento.contacarteira=?", filtro.getContacarteira())
			.where("pessoa=?", filtro.getCliente())
			.where("documento.empresa = ?", filtro.getEmpresa())
			.where("documento.documentoacao = ?", Documentoacao.DEFINITIVA)
			.where("not exists(select 1 from Boletodigital b where b.documento = documento and b.statusintegracao <> ?)", W3erpBankStatusIntegracao.ERRO_CRIACAO)
			.setPageNumberAndSize(filtro.getCurrentPage(), filtro.getPageSize())
			.orderBy("pessoa.nome, documento.dtemissao, documento.cddocumento")
			.ignoreJoin("listaNotaDocumento", "nota", "listaRateioitem", "projeto", "centrocusto");
		
		if(filtro.getProjeto() != null || filtro.getCentrocusto() != null){
			query
				.leftOuterJoin("documento.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.where("listaRateioitem.projeto = ?", filtro.getProjeto())
				.where("listaRateioitem.centrocusto = ?", filtro.getCentrocusto());
		}
		
		if(filtro.getDtemissaonota1() != null || filtro.getDtemissaonota2() != null){
			query
				.leftOuterJoin("documento.listaNotaDocumento listaNotaDocumento")
				.leftOuterJoin("listaNotaDocumento.nota nota")
				.where("nota.dtEmissao >=?", filtro.getDtemissaonota1())
				.where("nota.dtEmissao <=?", filtro.getDtemissaonota2());
		}
		
		if(filtro.getCriteriodocumentotipo() != null && filtro.getDocumentotipo() != null){
			query.where("documento.documentotipo " + (filtro.getCriteriodocumentotipo() ? " = " : " <> ") + "?", filtro.getDocumentotipo());
		}
				
		if(faturalocacao){
			query
				.leftOuterJoin("documento.listaContratofaturalocacaodocumento listaContratofaturalocacaodocumento")
				.leftOuterJoin("listaContratofaturalocacaodocumento.contratofaturalocacao contratofaturalocacao")
				.where("contratofaturalocacao.numero >= ?", filtro.getNumerofatura1())
				.where("contratofaturalocacao.numero <= ?", filtro.getNumerofatura2());
		}
		
		if(filtro.getWhereInCddocumento() != null){
			query.whereIn("documento.cddocumento", filtro.getWhereInCddocumento());
		}
		
		return query.list();
	}
	
	/**
	 * M�todo para obter lista de contas a receber para gera��o do arquivo de
	 * remessa configur�vel
	 * 
	 * @param filtro
	 * @return List<Documento>
	 * @author Marden Silva
	 */
	public List<Documento> findDadosForListagemRemessaConfiguravel(ArquivoConfiguravelRemessaBean filtro) {
		if(filtro.getCurrentPage() == null) filtro.setCurrentPage(0);
		if(filtro.getPageSize() == null) filtro.setPageSize(100);
		
		QueryBuilder<Documento> query = querySined();
		
		boolean faturalocacao = parametrogeralService.getBoolean(Parametrogeral.FATURA_LOCACAO);
		
		query
			.select("documento.cddocumento, documento.descricao, documento.dtvencimento, pessoa.cdpessoa," +
					"pessoa.nome, aux_documento.valoratual " +
					(faturalocacao ? ", contratofaturalocacao.numero " : ""))
			.join("documento.aux_documento aux_documento")
			.join("documento.pessoa pessoa")
//			.leftOuterJoin("documento.listaArqBancarioDoc listaArqBancarioDoc")
//			.leftOuterJoin("listaArqBancarioDoc.arquivobancario arquivobancario")
//			.leftOuterJoin("arquivobancario.arquivobancariosituacao arquivobancariosituacao")
			.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
			.where("documento.dtemissao >=?", filtro.getDtinicio())
			.where("documento.dtemissao <=?", filtro.getDtfim())
			.where("documento.dtvencimento >=?", filtro.getDtvencimento1())
			.where("documento.dtvencimento <=?", filtro.getDtvencimento2())
			.where("documento.conta=?", filtro.getConta())
			.where("documento.contacarteira=?", filtro.getContacarteira())
			.where("pessoa=?", filtro.getCliente())
			.where("documento.empresa = ?", filtro.getEmpresa())
			.setPageNumberAndSize(filtro.getCurrentPage(), filtro.getPageSize())
			.orderBy("pessoa.nome, documento.dtemissao, documento.cddocumento")
			.ignoreJoin("listaNotaDocumento", "nota", "listaRateioitem", "projeto", "centrocusto", "listaArqBancarioDoc", "arquivobancario", "arquivobancariosituacao", "listaNotaDocumento", "nota");
		
		if (filtro.getInstrucaoCobranca() != null){
			if(BancoConfiguracaoRemessaInstrucaoEnum.CANCELAR_PROTESTO.equals(filtro.getInstrucaoCobranca().getInstrucao())){
				query.where("documento.documentoacao = ?", Documentoacao.PROTESTADA);
			}else if(BancoConfiguracaoRemessaInstrucaoEnum.ALTERACAO_DADOS_COBRANCA.equals(filtro.getInstrucaoCobranca().getInstrucao()) ||
					BancoConfiguracaoRemessaInstrucaoEnum.PROTESTO.equals(filtro.getInstrucaoCobranca().getInstrucao()) ||
					BancoConfiguracaoRemessaInstrucaoEnum.BAIXAR.equals(filtro.getInstrucaoCobranca().getInstrucao()) ||
					BancoConfiguracaoRemessaInstrucaoEnum.CANCELAR_COBRANCA.equals(filtro.getInstrucaoCobranca().getInstrucao())){
				query.where("documento.cddocumento in (select dc.documento.cddocumento from Documentoconfirmacao dc)");
				query.where("documento.documentoacao = ?", Documentoacao.DEFINITIVA);
			}else if(BancoConfiguracaoRemessaInstrucaoEnum.PEDIDO_RESGISTRO.equals(filtro.getInstrucaoCobranca().getInstrucao())){
				query.where("documento.cddocumento not in (select dc.documento.cddocumento from Documentoconfirmacao dc)");
				query.where("documento.documentoacao = ?", Documentoacao.DEFINITIVA);
			}
		} else {
			query.where("documento.documentoacao = ?", Documentoacao.DEFINITIVA);
		}
		
		if(filtro.getProjeto() != null || filtro.getCentrocusto() != null){
			query
				.leftOuterJoin("documento.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.where("listaRateioitem.projeto = ?", filtro.getProjeto())
				.where("listaRateioitem.centrocusto = ?", filtro.getCentrocusto());
		}
		
		if(filtro.getDtemissaonota1() != null || filtro.getDtemissaonota2() != null){
			query
				.leftOuterJoin("documento.listaNotaDocumento listaNotaDocumento")
				.leftOuterJoin("listaNotaDocumento.nota nota")
				.where("nota.dtEmissao >=?", filtro.getDtemissaonota1())
				.where("nota.dtEmissao <=?", filtro.getDtemissaonota2());
		}
		
		if(filtro.getCriteriodocumentotipo() != null && filtro.getDocumentotipo() != null){
			query.where("documento.documentotipo " + (filtro.getCriteriodocumentotipo() ? " = " : " <> ") + "?", filtro.getDocumentotipo());
		}
				
		if(filtro.getSituacaoremessa() != null){
			if(SituacaoremessaEnum.NAO_ENVIADO.equals(filtro.getSituacaoremessa())){
				query.where("not exists (select d.cddocumento from Arquivobancariodocumento abd join abd.documento d where d.cddocumento = documento.cddocumento ) ");
			}else if(SituacaoremessaEnum.ENVIADO.equals(filtro.getSituacaoremessa())){
				query.where("exists (select d.cddocumento from Arquivobancariodocumento abd join abd.documento d where d.cddocumento = documento.cddocumento and abd.arquivobancario.arquivobancariosituacao <> ? )", Arquivobancariosituacao.CANCELADO);
//				query.where("listaArqBancarioDoc.arquivobancario.arquivobancariosituacao <> ?", Arquivobancariosituacao.CANCELADO);	
			}else if(SituacaoremessaEnum.CANCELADO.equals(filtro.getSituacaoremessa())){
				query.where("exists (select d.cddocumento from Arquivobancariodocumento abd join abd.documento d where d.cddocumento = documento.cddocumento and abd.arquivobancario.arquivobancariosituacao = ? )", Arquivobancariosituacao.CANCELADO);
//				query.where("listaArqBancarioDoc.arquivobancario.arquivobancariosituacao = ?", Arquivobancariosituacao.CANCELADO);
			}
		}
		
		if(faturalocacao){
			query
				.leftOuterJoin("documento.listaContratofaturalocacaodocumento listaContratofaturalocacaodocumento")
				.leftOuterJoin("listaContratofaturalocacaodocumento.contratofaturalocacao contratofaturalocacao")
				.where("contratofaturalocacao.numero >= ?", filtro.getNumerofatura1())
				.where("contratofaturalocacao.numero <= ?", filtro.getNumerofatura2());
		}
		
		if(filtro.getWhereInCddocumento() != null){
			query.whereIn("documento.cddocumento", filtro.getWhereInCddocumento());
		}
		
		return query.list();
	}
	
	public List<Documento> findDadosForListagemRemessaConfiguravel(String whereIn) {
		QueryBuilder<Documento> query = querySined();
		
		boolean faturalocacao = parametrogeralService.getBoolean(Parametrogeral.FATURA_LOCACAO);
		
		query
			.select("documento.cddocumento, documento.descricao, documento.dtvencimento, pessoa.cdpessoa," +
					"pessoa.nome, aux_documento.valoratual, " +
					"listaArqBancarioDoc.cdarquivobancariodocumento,  arquivobancario.cdarquivobancario, arquivobancariosituacao.cdarquivobancariosituacao " +
					(faturalocacao ? ", contratofaturalocacao.numero " : ""))
			.join("documento.aux_documento aux_documento")
			.join("documento.pessoa pessoa")
			.leftOuterJoin("documento.listaArqBancarioDoc listaArqBancarioDoc")
			.leftOuterJoin("listaArqBancarioDoc.arquivobancario arquivobancario")
			.leftOuterJoin("arquivobancario.arquivobancariosituacao arquivobancariosituacao")
			.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
			.orderBy("pessoa.nome, documento.dtemissao, documento.cddocumento");
		
		if(faturalocacao){
			query
				.leftOuterJoin("documento.listaContratofaturalocacaodocumento listaContratofaturalocacaodocumento")
				.leftOuterJoin("listaContratofaturalocacaodocumento.contratofaturalocacao contratofaturalocacao");
		}
		
		SinedUtil.quebraWhereIn("documento.cddocumento", whereIn, query);
		
		return query.list();
	}
	
	/**
	 * M�todo para obter lista de contas a receber para gera��o do arquivo de
	 * remessa configur�vel
	 * 
	 * @param filtro
	 * @return List<Documento>
	 * @author Rafael Salvio
	 */
	public List<Documento> findDadosForListagemRemessaPagamentoConfiguravel(ArquivoConfiguravelRemessaPagamentoBean filtro) {
		QueryBuilder<Documento> query = querySined();
		
		query
			.select("documento.cddocumento, documento.descricao, documento.dtvencimento, pessoa.cdpessoa," +
					"pessoa.nome, aux_documento.valoratual, documentotipo.cddocumentotipo, documentotipo.nome")
			.join("documento.aux_documento aux_documento")
			.join("documento.documentoclasse documentoclasse")
			.join("documento.documentotipo documentotipo")
			.leftOuterJoin("documento.conta conta")
			.leftOuterJoin("documento.contacarteira contacarteira")
			.join("documento.documentoacao documentoacao")
			.join("documento.pessoa pessoa")
			.where("documentoclasse = ?", Documentoclasse.OBJ_PAGAR)
			.where("documento.dtemissao >=?", filtro.getDtinicioEmissao())
			.where("documento.dtemissao <=?", filtro.getDtfimEmissao())
			.where("documento.dtvencimento >= ?", filtro.getDtinicioVencimento())
			.where("documento.dtvencimento <= ?", filtro.getDtfimVencimento())
			.where("documentotipo = ?", filtro.getDocumentotipo())
			.where("documento.empresa = ?", filtro.getEmpresafiltro())
			.where("documento.valor >= ?", filtro.getValorMinimo())
			.where("documento.valor <= ?", filtro.getValorMaximo())
			.orderBy("retira_acento(upper(pessoa.nome)), documento.dtemissao");
		if(filtro.getContaGerencial() != null || filtro.getCentroCusto()!=null || filtro.getProjeto()!=null){
			query.leftOuterJoin("documento.rateio rateio");
			query.leftOuterJoin("rateio.listaRateioitem listarateioitem");
			if(filtro.getContaGerencial() != null){
				query.where("listarateioitem.contagerencial = ?", filtro.getContaGerencial());
			}
			if(filtro.getCentroCusto() != null){
				query.where("listarateioitem.centrocusto = ?", filtro.getCentroCusto());
			}
			if(filtro.getProjeto() != null){
				query.where("listarateioitem.projeto = ?", filtro.getProjeto());
			}
		}
		if(filtro.getPessoa() != null){
			query.whereLikeIgnoreAll("documento.pessoa.nome", filtro.getPessoa());
		}
		
		if(Tipopagamento.CLIENTE.equals(filtro.getTipoPagamentoFiltro())){
			query.where("documento.tipopagamento =?",Tipopagamento.CLIENTE);
		}
		if(Tipopagamento.COLABORADOR.equals(filtro.getTipoPagamentoFiltro())){
			query.where("documento.tipopagamento =?",Tipopagamento.COLABORADOR);
		}
		if(Tipopagamento.FORNECEDOR.equals(filtro.getTipoPagamentoFiltro())){
			query.where("documento.tipopagamento =?",Tipopagamento.FORNECEDOR);
		}
		/*if(Tipopagamento.OUTROS.equals(filtro.getTipoPagamentoFiltro())){
			query.where("documento.tipopagamento =?",Tipopagamento.OUTROS);
		}*/

		
		if(SinedUtil.isListNotEmpty(filtro.getListaDocumentoacao())){
			query.whereIn("documentoacao.cddocumentoacao", CollectionsUtil.listAndConcatenate(filtro.getListaDocumentoacao(), "cddocumentoacao", ","));
		}else {
			query.where("documentoacao <> ?", Documentoacao.BAIXADA);
			query.where("documentoacao <> ?", Documentoacao.CANCELADA);
			query.where("documentoacao <> ?", Documentoacao.NEGOCIADA);
			query.where("documentoacao <> ?", Documentoacao.NAO_AUTORIZADA);
		}
		
		if (filtro.getInstrucaoPagamento() != null && BancoConfiguracaoRemessaInstrucaoEnum.ALTERACAO_DADOS_PAGAMENTO.equals(filtro.getInstrucaoPagamento())){
			query.where("documentoacao = ?", Documentoacao.AUTORIZADA);
			//TODO contas com data de agendamento confirmada pelo banco
		} else if(filtro.getInstrucaoPagamento() != null && BancoConfiguracaoRemessaInstrucaoEnum.PEDIDO_PAGAMENTO.equals(filtro.getInstrucaoPagamento())){
			query.where("documentoacao = ?", Documentoacao.AUTORIZADA);			
			//TODO contas com data de agendamento n�o confirmada pelo banco
		} else if(filtro.getInstrucaoPagamento() != null && BancoConfiguracaoRemessaInstrucaoEnum.CANCELAR_PAGAMENTO.equals(filtro.getInstrucaoPagamento())){
			query.where("documentoacao = ?", Documentoacao.AUTORIZADA);			
			//TODO contas com data de agendamento confirmada pelo banco
		} else if(filtro.getInstrucaoPagamento() != null && BancoConfiguracaoRemessaInstrucaoEnum.HOLERITE.equals(filtro.getInstrucaoPagamento())){
			query.where("documento.nossonumero is not null");			
		}
		return query.list();
	}

	/**
	 * M�todo para obter lista de contas a receber para gera��o do arquivo de
	 * remessa
	 * 
	 * @param bancoFiltro
	 * @param mes
	 * @param anoRemessa
	 * @return List<Documento>
	 * @author Jo�o Paulo Zica
	 */
	public List<Documento> findDadosArquivoRemessa(GerarArquivoRemessaBean filtro){
		return querySined()
					.select("documento.cddocumento, documento.documentoacao,documento.descricao, documento.numero, documento.nossonumero, conta.cdconta, " +				
							"documento.dtemissao, documento.dtvencimento, documento.valor, documento.observacao, documentoacao.cddocumentoacao," +	// 8
							"pessoa.cdpessoa, pessoa.nome, pessoa.cpf, pessoa.cnpj, aux_documento.valoratual, listaEndereco.cdendereco, " +		// 13
							"dadobancario.conta, dadobancario.dvconta, dadobancario.dvagencia, dadobancario.agencia, " +
							"listaEndereco.logradouro, listaEndereco.bairro, listaEndereco.numero, listaEndereco.complemento,"+		// 17
							"listaEndereco.cep, municipio.nome, uf.sigla, documento.mensagem1, documento.mensagem2, documento.mensagem3," +		   	// 23
							"documento.mensagem4, documento.mensagem5, taxa.cdtaxa, cheque.cdcheque, " + // 26
							"pessoaBanco.nome, pessoaBanco.numero, documentotipo.cddocumentotipo, documentotipo.taxavenda, " +
							"contacarteira.cdcontacarteira, contacarteira.especiedocumento, contacarteira.padrao, contacarteira.convenio, documento.tipopagamento," +
							"contacarteira.especiedocumentoremessa, contacarteira.limiteposicoesintervalonossonumero, documento.outrospagamento")						
					.join("documento.aux_documento aux_documento")
					.join("documento.conta conta")
					.leftOuterJoin("documento.contacarteira contacarteira")
					.leftOuterJoin("conta.banco banco")
					.join("documento.documentoclasse documentoclasse")
					.join("documento.documentoacao documentoacao")
					.leftOuterJoin("documento.documentotipo documentotipo")
					.join("documento.pessoa pessoa")
					.leftOuterJoin("documento.cheque cheque")
					.leftOuterJoin("pessoa.listaDadobancario dadobancario")
					.leftOuterJoin("dadobancario.banco pessoaBanco")
					.leftOuterJoin("documento.endereco listaEndereco") 
					.leftOuterJoin("listaEndereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("documento.taxa taxa")
					.where("documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.whereIn("documento.cddocumento", filtro.getContas())
					.list();
		
		
//		QueryBuilder<Object[]> query = newQueryBuilderWithFrom(Object[].class);
//		
//		List<Object[]> lista = query
//		.select("documento.cddocumento, " +			// 0
//				"documento.documentoacao," +		// 1
//				"documento.descricao," +			// 2
//				"documento.numero," +				// 3
//				"documento.dtemissao," +			// 4
//				"documento.dtvencimento," +			// 5
//				"documento.valor," +				// 6
//				"documento.observacao," +			// 7
//
//				"documentoacao.cddocumentoacao," +	// 8
//				
//				"pessoa.cdpessoa," +				// 9
//				"pessoa.nome," +					// 10
//				"pessoa.cpf," +						// 11
//				"pessoa.cnpj," +					// 12
//				
//				"aux_documento.valoratual," +		// 13
//				"listaEndereco.logradouro," +		// 14
//				"listaEndereco.bairro,"+			// 15
//				"listaEndereco.numero,"+			// 16
//				"listaEndereco.complemento,"+		// 17
//				"listaEndereco.cep,"+				// 18
//				"municipio.nome,"+					// 19
//				"uf.sigla," +						// 20
//				"documento.mensagem1," +		   	// 21
//				"documento.mensagem2," +		   	// 22
//				"documento.mensagem3," +		   	// 23
//				"documento.mensagem4," +		   	// 24
//				"documento.mensagem5," +			// 25
//				"taxa.cdtaxa")						// 26						
//
//		.join("documento.aux_documento aux_documento")
//		.join("documento.conta conta")
//		.leftOuterJoin("conta.banco banco")
//		.join("documento.documentoclasse documentoclasse")
//		.join("documento.documentoacao documentoacao")
//		.join("documento.pessoa pessoa")
//		.leftOuterJoin("pessoa.listaEndereco listaEndereco")
//		.leftOuterJoin("listaEndereco.municipio municipio")
//		.leftOuterJoin("municipio.uf uf")
//		.leftOuterJoin("documento.taxa taxa")
//		.where("documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
//		.whereIn("documento.cddocumento", filtro.getContas())
//		.setUseTranslator(Boolean.FALSE)
//		.list();
//		
//		// Beans
//		List<Documento> listaDocumento = new ArrayList<Documento>();
//		Documento documento;
//		Pessoa pessoa;
//		Aux_documento aux_documento;
//		Endereco endereco;
//		Set<Endereco> listaEndereco;
//		for (Object[] o : lista) {
//		
//			// Cliente
//			pessoa = new Pessoa();
//			pessoa.setCdpessoa((Integer)o[9]);
//			pessoa.setNome((String)o[10]);
//			if (!o[11].toString().equals("")) {				
//				pessoa.setCpf(new Cpf(o[11].toString()));
//			}else if (!o[12].toString().equals("")) {
//				pessoa.setCnpj(new Cnpj(o[12].toString()));				
//			}
//			
//			endereco = new Endereco();
//			if (o[14]!=null) {
//				endereco.setLogradouro(o[14].toString());				
//			}
//			if (o[15]!=null) {
//				endereco.setBairro(o[15].toString());
//			}	
//			if (o[16]!=null) {
//				endereco.setNumero(o[16].toString());
//			}	
//			if (o[17]!=null) {				
//				endereco.setComplemento(o[17].toString());
//			}		
//			endereco.setCep(new Cep(o[18].toString()));
//			endereco.setMunicipio(new Municipio());
//			endereco.getMunicipio().setNome(o[19].toString());
//			endereco.setUf(new Uf());
//			endereco.getUf().setSigla(o[20].toString());
//			listaEndereco = new ListSet<Endereco>(Endereco.class);
//			listaEndereco.add(endereco);
//			pessoa.setListaEndereco(listaEndereco);
//			
//			// Documento
//			documento = new Documento();
//			documento.setCddocumento((Integer)o[0]);
//			documento.setDocumentoacao((Documentoacao)o[1]);
//			documento.setDescricao((String)o[2]);
//			documento.setNumero((String)o[3]);
//			documento.setPessoa(pessoa);
//			documento.setDtemissao((Date)o[4]);
//			documento.setDtvencimento((Date)o[5]);
//			documento.setValor((Money)o[6]);
//			documento.setObservacao((String)o[7]);
//			documento.setMensagem1((String)o[21]);
//			documento.setMensagem2((String)o[22]);
//			documento.setMensagem3((String)o[23]);
//			documento.setMensagem4((String)o[24]);
//			documento.setMensagem5((String)o[25]);
//			if(o[26] != null){
//				documento.setTaxa(new Taxa());
//				documento.getTaxa().setCdtaxa((Integer)o[26]);
//			}
//			
//			// Aux_documento
//			aux_documento = new Aux_documento();
//			aux_documento.setValoratual((Money) o[13]);
//			
//			documento.setAux_documento(aux_documento);
//			
//			listaDocumento.add(documento);
//		}
//		return listaDocumento;
	}

	/**
	 * M�todo que salva um arquivo de remessa a partir de um resource Cria e
	 * salva arquivo banc�rio Cria e salva uma lista de Arquivobancariodocumento
	 * Atualiza uma lista de documentos de a��o Prevista para Definitiva
	 * 
	 * Todas a��es acontecem dentro de uma transa��o
	 * 
	 * @param resource
	 * @param listaDocumento
	 * 
	 * @author Bruno Eust�quio
	 */
	public void saveAndUpdateArquivoRemessa(Resource resource, List<Documento> listaDocumento, String descricao, String sequencial, List<Movimentacao> listaMovimentacao) {
		if (descricao == null) {
			throw new SinedException(
					"Existem dados nulos no m�todo \"saveAndUpdateArquivoRemessa\" da classe \"Documento DAO\".");
		}

		Money valorTotal = new Money(0.0);
		Date date = new Date(System.currentTimeMillis());
//				String nomeArq = "ACC" + data + ".REM";

		// Cria e salva arquivo
		Arquivo arq = new Arquivo();
		arq.setNome(descricao);
		arq.setTipoconteudo("text/plain");
		arq.setTamanho(new Long(resource.getContents().length));
		arq.setContent(resource.getContents());
		arquivoService.saveOrUpdate(arq);

		// Cria e salva arquivo banc�rio
		Arquivobancario ab = new Arquivobancario();
		ab.setNome(descricao);
		ab.setArquivo(arq);
		ab.setSequencial(sequencial);
		ab.setArquivobancariotipo(Arquivobancariotipo.REMESSA);
		ab.setArquivobancariosituacao(Arquivobancariosituacao.GERADO);
		ab.setDtgeracao(date);
		arquivobancarioService.saveOrUpdate(ab);
		arquivoDAO.saveFile(ab, "arquivo");

		// - Cria e salva uma lista de Arquivobancariodocumento que
		// vincula
		// os registros que foram incluidos em um determinado arquivo de
		// remessa.
		// - Atualiza o documento de Prevista para Definitiva
		if (listaDocumento!=null){
			Arquivobancariodocumento abd;
			for (Documento d : listaDocumento) {
				abd = new Arquivobancariodocumento();
				abd.setArquivobancario(ab);
				abd.setDocumento(d);
				arquivobancariodocumentoService.saveOrUpdate(abd);
				valorTotal.add(d.getValor());
			}
		}
		
		if (listaMovimentacao!=null){
			for (Movimentacao movimentacao: listaMovimentacao){
				arquivobancariomovimentacaoService.saveOrUpdate(new Arquivobancariomovimentacao(ab, movimentacao));
			}
		}
	}
	
	/**
	 * M�todo que retorna a data da �ltima conta a pagar cadastrada no sistema
	 * 
	 * @param documentoclasse
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Date getDtUltimoCadastro(Documentoclasse documentoclasse) {
		return newQueryBuilderSined(Date.class)
			.from(Documento.class)
			.setUseTranslator(false) 
			.select("max(documento.dtemissao)")
			.join("documento.documentoclasse documentoclasse")
			.join("documento.documentoacao documentoacao")
			.where("documentoclasse = ?", documentoclasse)
			.where("documentoacao <> ?", Documentoacao.CANCELADA)
			.unique();
	}
	
	/**
	 * M�todo que retorna a qtde. de contas (pagar/receber dependendo do parametro) atrasadas est�o cadastradas no sistema
	 *  
	 * @param documentoclasse
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getQtdeAtrasadas(Documentoclasse documentoclasse) {
		return newQueryBuilderSined(Long.class)
			.from(Documento.class)
			.setUseTranslator(false) 
			.select("count(*)")
			.join("documento.documentoclasse documentoclasse")
			.join("documento.documentoacao documentoacao")
			.where("documento.dtvencimento < ?", new Date(System.currentTimeMillis()))
			.where("documentoclasse = ?", documentoclasse)
			.where("documentoacao <> ?", Documentoacao.NEGOCIADA)
			.where("documentoacao <> ?", Documentoacao.CANCELADA)
			.where("documentoacao <> ?", Documentoacao.BAIXADA)
			.unique()
			.intValue();
	}
	
	/**
	 * Retorna a quantidade de contas abertas (PREVISTA, DEFINITIVA, AUTORIZADA, AUTORIZADA PARCIAL)
	 *
	 * @param documentoclasse
	 * @return
	 * @since 24/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdeAbertas(Documentoclasse documentoclasse) {
		return newQueryBuilderSined(Long.class)
		
		.from(Documento.class)
		.setUseTranslator(false) 
		.select("count(*)")
		.join("documento.documentoclasse documentoclasse")
		.join("documento.documentoacao documentoacao")
		.where("documentoclasse = ?", documentoclasse)
		.openParentheses()
		.where("documentoacao = ?", Documentoacao.PREVISTA).or()
		.where("documentoacao = ?", Documentoacao.DEFINITIVA).or()
		.where("documentoacao = ?", Documentoacao.AUTORIZADA).or()
		.where("documentoacao = ?", Documentoacao.AUTORIZADA_PARCIAL).or()
		.closeParentheses()
		.unique()
		.intValue();
	}
	
	/**
	 * M�toro que retorna a qtde. total de contas a pagar/receber cadastradas no sistema
	 * 
	 * @param documentoclasse
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Integer getQtdeTotalContas(Documentoclasse documentoclasse) {
		return newQueryBuilderSined(Long.class)
			
			.from(Documento.class)
			.setUseTranslator(false) 
			.select("count(*)")
			.join("documento.documentoclasse documentoclasse")
			.join("documento.documentoacao documentoacao")
			.where("documentoclasse = ?", documentoclasse)
			.where("documentoacao <> ?", Documentoacao.CANCELADA)
			.unique()
			.intValue();
	}
	
	public List<Documento> findForRecibo(String whereIn) {
		return querySined()
					.select("documento.cddocumento, documento.outrospagamento, documento.tipopagamento, documento.numero, documentoacao.cddocumentoacao, " +
							"documento.valor, pessoa.nome, pessoa.cdpessoa, documento.descricao, empresa.cdpessoa, empresa.cnpj, " +
							"aux_documento.valoratual, formapagamento.cdformapagamento, formapagamento.nome, " +
							"banco.cdbanco, banco.numero, rateio.cdrateio, item.cdrateioitem, cheque.numero, " +
							"projeto.cdprojeto, projeto.nome, projeto.cnpj, item.valor, pessoa.cpf, pessoa.cnpj, " +
							"movimentacao.dtmovimentacao, movimentacao.checknum, movimentacao.valor, pessoa.tipopessoa, " +
							"movimentacao.cdmovimentacao, movimentacaoacao.cdmovimentacaoacao, movimentacaoacao.nome, " +
							"endereco.cdendereco, endereco.cep, endereco.complemento, endereco.numero, endereco.logradouro," +
							"enderecotipo.cdenderecotipo, " +
							"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.nome, uf.sigla, endereco.bairro ")
					.leftOuterJoin("documento.documentoacao documentoacao")
					.leftOuterJoin("documento.rateio rateio")
					.leftOuterJoin("rateio.listaRateioitem item")
					.leftOuterJoin("item.projeto projeto")
					.leftOuterJoin("documento.pessoa pessoa")
					.leftOuterJoin("documento.empresa empresa")
					.leftOuterJoin("documento.listaMovimentacaoOrigem listaMovimentacaoOrigem")
					.leftOuterJoin("listaMovimentacaoOrigem.conta conta")
					.leftOuterJoin("conta.banco banco")
					.leftOuterJoin("listaMovimentacaoOrigem.movimentacao movimentacao")
					.leftOuterJoin("movimentacao.movimentacaoacao movimentacaoacao")
					.leftOuterJoin("movimentacao.cheque cheque")
					.leftOuterJoin("movimentacao.formapagamento formapagamento")
					.leftOuterJoin("documento.aux_documento aux_documento")
					.leftOuterJoin("documento.endereco endereco")
					.leftOuterJoin("endereco.enderecotipo enderecotipo")
					.leftOuterJoin("endereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.whereIn("documento.cddocumento", whereIn)
					.list();
	}
	
	@SuppressWarnings("unchecked")
	public Money getValorAtual(Integer cddocumento, Date dtcredito, Tipotaxa tipotaxaignorar) {
		
		String sql = "SELECT (SELECT VALORATUAL FROM VALORATUALCOMPOSTO(d.cddocumento, " +
				"'" + SinedDateUtils.toStringPostgre(dtcredito) + "',  " +
				"d.cdtaxa,  " +
				"d.valor, " +
				"d.dtvencimento,  " +
				"d.cddocumentoacao, " + 
				(tipotaxaignorar != null ? tipotaxaignorar.getCdtipotaxa().toString() : "null") + 
				") AS f(VALORATUAL numeric, VALORJUROS NUMERIC, VALORMULTA NUMERIC, VALORDESCONTO NUMERIC, VALORDESAGIO NUMERIC, VALORTAXABOLETO NUMERIC, VALORTERMO NUMERIC, VALORMOVIMENTO NUMERIC, VALORTAXABAIXA NUMERIC, VALORTAXABAIXA_ACRESCIMO NUMERIC, VALOR_JUROSMES NUMERIC)) " +
				"FROM documento d " +
				"WHERE d.cddocumento = " +cddocumento;
		 
		List<Long> listaUpdate = getJdbcTemplate().query(sql, 
			new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return rs.getLong("VALORATUAL");
				}
	
			});
		
		if(listaUpdate == null || listaUpdate.size() == 0){
			return new Money();
		} else {
			return new Money(listaUpdate.get(0), true);
		}
	}


	/**
	 * Seta dados de devolu��o na conta a receber;
	 * 
	 * @author Taidson
	 * @since 22/04/2010
	 * 
	 */
	public void salvaContaDevolvida(Integer cddocumento, String observacaoDevolucao, Integer cddocumentorelacionado){
		Date dataAtual = new Date(System.currentTimeMillis());
		String query = "update Documento set dtdevolucao = ? , observacao = ? , cddocumentorelacionado = ?" +
				" where cddocumento = ?" ;
		getHibernateTemplate().bulkUpdate(
				query, new Object[]{dataAtual, observacaoDevolucao, cddocumentorelacionado, cddocumento}
		);
	}
	
	
	/**
	 * Cancela devolucao a partir do cancelamento de uma conta a pagar;
	 * 
	 * @author Taidson
	 * @since 22/04/2010
	 * 
	 */
	public void cancelaContaDevolvida(Documento documento){
		String query = "update Documento set dtdevolucao = null where cddocumentorelacionado = ?" ;
		getHibernateTemplate().bulkUpdate(query, documento.getCddocumento());
	}
	
	
	/**
	 * Retorna uma lista de contas a receber de acordo com a lista de itens passada;
	 * 
	 * @param itens
	 * @return
	 * @author Taidson
	 * @since 22/04/2010
	 * 
	 */
	public List<Documento> contaReceberDevolucao(String itens) {
		if(itens == null){
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		return querySined()
		.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
							"documentotipo.cddocumentotipo, documentotipo.nome, documento.valor, " +
							"documento.cddocumento, documento.descricao, pessoa.cdpessoa, pessoa.nome")
					.leftOuterJoin("documento.pessoa pessoa")
					.join("documento.empresa empresa")
					.join("documento.documentotipo documentotipo")
					.whereIn("documento.cddocumento", itens)
					.list();
		
	}
	
	/**
	 * Retorna uma conta a receber de acordo com o cddocumento passado;
	 * 
	 * @param cddocumento
	 * @return
	 * @author Taidson
	 * @since 22/04/2010
	 * 
	 */
	public Documento contaReceberDevolucao(Integer cddocumento) {
		if(cddocumento == null){
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		return querySined()
		.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
							"documentotipo.cddocumentotipo, documentotipo.nome, documentotipo.antecipacao, documento.valor, " +
							"documento.cddocumento, documento.descricao, pessoa.cdpessoa, pessoa.nome, " +
							"documentoacao.cddocumentoacao, documentoacao.nome, documento.numero, " +
							"documento.dtemissao, documento.dtvencimento, documento.valor, " +
							" documento.outrospagamento, documento.dtdevolucao, documentoclasse.cddocumentoclasse ")
					.join("documento.documentoclasse documentoclasse")
					.leftOuterJoin("documento.pessoa pessoa")
					.join("documento.documentoacao documentoacao")
					.leftOuterJoin("documento.empresa empresa")
					.join("documento.documentotipo documentotipo")
					.where("documento.cddocumento = ?", cddocumento)
					.unique();
		
	}
	
	/**
	 * Verifica se os itens selecionados pertencem a uma �nica empresa
	 * 
	 * @param itens
	 * @return
	 * @author Taidson
	 * @since 22/04/2010
	 * 
	 */
	public List<Documento> verificaEmpresasEmComum(String itens) {
		if(itens == null){
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		return querySined()
		.select("distinct empresa.nome, empresa.razaosocial ")
					.leftOuterJoin("documento.pessoa pessoa")
					.join("documento.empresa empresa")
					.join("documento.documentotipo documentotipo")
					.whereIn("documento.cddocumento", itens)
					.list();
	}
	
	/**
	 * Verifica se os itens selecionados possuem o mesmo documento;
	 * 
	 * @author Taidson
	 * @since 22/04/2010
	 * 
	 */
	public List<Documento> verificaDocumentosEmComun(String itens) {
		if(itens == null){
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		return querySined()
		.select("distinct documento.numero ")
		.leftOuterJoin("documento.pessoa pessoa")
		.join("documento.empresa empresa")
		.join("documento.documentotipo documentotipo")
		.whereIn("documento.cddocumento", itens)
		.list();
	}
	
	/**
	 * Verifica se os itens selecionados possuem o mesmo tipo de documento;
	 * 
	 * @author Taidson
	 * @since 22/04/2010
	 * 
	 */
	public List<Documento> verificaTiposDocumentosEmComun(String itens) {
		if(itens == null){
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		return querySined()
		.select("distinct documentotipo.nome ")
		.leftOuterJoin("documento.pessoa pessoa")
		.join("documento.empresa empresa")
		.join("documento.documentotipo documentotipo")
		.whereIn("documento.cddocumento", itens)
		.list();
	}
	
	/**
	 * Retorna o documentoacao atual do documento
	 * @param documento
	 * @return
	 * @autor Taidson
	 * @since 10/06/2010
	 */
	
	public Documento verificaDocumentoacao(Documento documento){
		if (documento == null || documento.getCddocumento() == null) {
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		return querySined()
				.select("documentoacao.cddocumentoacao")
				.join("documento.documentoacao documentoacao")
				.where("documento = ?",	documento)
				.unique();
	}
	
	public List<Documento> findForCancelamentoAutomatico(Date data) {
		return querySined()
					.select("documento.cddocumento, documento.dtvencimento")
					.join("documento.documentoacao documentoacao")
					.whereIn("documentoacao.cddocumentoacao", "1,2") // PREVISTA E DEFINITIVA
					.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.where("documento.dtvencimento <= ?", data)
					.list();
	}
	
	/**
	 * Verifica se existe alguma conta � receber em atraso.
	 * @param cliente
	 * @return
	 * @author Taidson
	 * @since 30/06/2010
	 */
	public Date getPrimeiroVencimentoDocumentoAtrasado(Cliente cliente){
		return newQueryBuilderSined(Date.class)
					.from(Documento.class)
					.setUseTranslator(false)
					.select("min(documento.dtvencimento)")
					.join("documento.documentoclasse documentoclasse")
					.join("documento.aux_documento aux_documento")
					.join("documento.documentoacao documentoacao")
					.join("documento.pessoa pessoa")
					.where("documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.openParentheses()
					.where("documentoacao = ?", Documentoacao.DEFINITIVA).or()
					.where("documentoacao = ?", Documentoacao.PREVISTA).or()
					.where("documentoacao = ?", Documentoacao.BAIXADA_PARCIAL).or()
					.closeParentheses()
					.where("pessoa = ?", cliente)
					.where("documento.dtvencimento < ?", SinedDateUtils.currentDateToBeginOfDay())
					.unique();
	}
	
	public Date getPrimeiroVencimentoContasEmAbertoByCliente(Cliente cliente){
		return newQueryBuilderSined(Date.class)
					.from(Documento.class)
					.setUseTranslator(false)
					.select("min(documento.dtvencimento)")
					.join("documento.documentoclasse documentoclasse")
					.join("documento.aux_documento aux_documento")
					.join("documento.documentoacao documentoacao")
					.join("documento.pessoa pessoa")
					.where("documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.where("documentoacao <> ?", Documentoacao.BAIXADA)
					.where("documentoacao <> ?", Documentoacao.CANCELADA)
					.where("documentoacao <> ?", Documentoacao.NEGOCIADA)
					.where("pessoa = ?", cliente)
					.where("documento.dtvencimento < ?", SinedDateUtils.currentDateToBeginOfDay())
					.unique();
	}	
	
	/**
	 * Verifica se existe projeto vinculado ao rateio e se este primeiro possui CNPJ.
	 * @param request
	 * @author Taidson
	 * @since 19/07/2010
	 */
	public boolean haveProjetoCNPJ(String whereIn){
		return newQueryBuilderSined(Long.class)
		.from(Documento.class)
		.setUseTranslator(false)
		.select("count(*)")
		.join("documento.documentoclasse documentoclasse")
		.join("documento.documentoacao documentoacao")
		.leftOuterJoin("documento.rateio rateio")
		.leftOuterJoin("rateio.listaRateioitem item")
		.leftOuterJoin("item.projeto projeto")	
		.where("projeto is not null")
		.where("projeto.cnpj is not null")
		.where("documentoclasse = ?", Documentoclasse.OBJ_PAGAR)
		.where("documentoacao = ?", Documentoacao.BAIXADA)
		.whereIn("documento.cddocumento", whereIn)
		.unique() > 0;
	}
	
	/**
	 * Carrega dados de produtos vendidos para recibo de conta a receber,
	 * caso a conta for originada de uma venda.
	 * @param documento
	 * @return
	 * @author Taidson
	 * @since 27/08/2010
	 */
	public List<Documento> docOrigemVenda(Documento documento) {
		return querySined()
					.select("documento.cddocumento, venda.cdvenda, " +
							"material.cdmaterial, material.nome")
					.leftOuterJoin("documento.listaDocumentoOrigem origem")
					.join("origem.venda venda")
					.leftOuterJoin("venda.listavendamaterial vendamaterial")
					.join("vendamaterial.material material")
					.where("documento.cddocumento = ?", documento.getCddocumento())
					.where("material.produto = ?", Boolean.TRUE)
					.list();
	}
	
	public void atualizaDtvencimento(Documento documento, Date dtvencimento) {
		getHibernateTemplate().bulkUpdate("update Documento d set d.dtvencimento = ? where d.id = ?", 
				new Object[]{dtvencimento, documento.getCddocumento()});
	}
	
	/**
	 * Retorna todos os pagamentos de um determinado cliente.
	 * @param cliente
	 * @return
	 * @author Taidson
	 * @since 17/09/2010
	 */
	public List<Documento> findPagamentosByCliente(Cliente cliente, Empresa empresa) {
		String whereInEmpresas = empresa != null? empresa.getCdpessoa().toString():
			CollectionsUtil.listAndConcatenate(empresaService.findByUsuario(), "cdpessoa", ",");
		
		return querySined()
					.select("documento.cddocumento, documento.dtvencimento, documento.valor, " +
							"documento.descricao, documentoacao.cddocumentoacao, documentoacao.nome, documentoclasse.cddocumentoclasse," +
							"documentoclasse.nome")
					.join("documento.documentoclasse documentoclasse")
					.join("documento.documentoacao documentoacao")
					.join("documento.pessoa pessoa")
					.leftOuterJoin("documento.empresa empresa")
//					.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.where("documento.tipopagamento = ?", Tipopagamento.CLIENTE)
					.whereIn("documentoacao.cddocumentoacao", "1,2,7") // PREVISTA E DEFINITIVA
					.where("pessoa = ?", cliente)
					.openParentheses()
						.whereIn("empresa.cdpessoa", whereInEmpresas)
						.or()
						.where("empresa.cdpessoa is null")
					.closeParentheses()
					.orderBy("documentoclasse.cddocumentoclasse desc, documento.dtvencimento desc")
					.list();
	}
	
	
	/**
	 * Retorna contas atrasadas conforme documentos inforamdos como par�mentro
	 * @param whereIn
	 * @return
	 * @author Taidson
	 * @author 24/11/2010
	 */
	public List<Documento> contasAtrasadasByContrato(String whereIn){
		return querySined()
		.joinFetch("documento.documentoacao documentoacao")
		.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
		.where("documento.dtvencimento < ?", SinedDateUtils.currentDate())
		.where("documentoacao <> ?", Documentoacao.BAIXADA)
		.where("documentoacao <> ?", Documentoacao.CANCELADA)
		.where("documentoacao <> ?", Documentoacao.NEGOCIADA)
		.whereIn("documento.cddocumento", whereIn)
		.orderBy("documento.dtvencimento")
		.list();
	}
	
	public List<Documento> contasAtrasadasByCliente(Cliente cliente) {
		return querySined()
					.select("documento.cddocumento, documento.dtvencimento, aux_documento.valoratual")
					.join("documento.aux_documento aux_documento")
					.join("documento.documentoacao documentoacao")
					.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.where("documento.dtvencimento < ?", SinedDateUtils.currentDate())
					.where("documentoacao <> ?", Documentoacao.BAIXADA)
					.where("documentoacao <> ?", Documentoacao.CANCELADA)
					.where("documentoacao <> ?", Documentoacao.NEGOCIADA)
					.where("documento.pessoa = ?", cliente)
					.list();
	}
	
	
	/**
	 * Busca a lista de movimenta��es daquela contagerencial e de todas suas filhas
	 *
	 * @param identificador
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<Documento> findForAnaliseRecDespesaFlex(AnaliseReceitaDespesaReportFiltro filtro, String identificador, Boolean max, String campo, String ordenacao) {
		if (identificador == null || identificador.equals("")) {
			throw new SinedException("Identificador n�o pode ser nulo.");
		}
//		AnaliseReceitaDespesaReportFiltro filtro = (AnaliseReceitaDespesaReportFiltro)NeoWeb.getRequestContext().getSession().getAttribute(ContagerencialService.FILTRO_SESSION);
//		System.out.println("QUERY: " + sql);
		
		List<Documento> lista = null;
		
		if(filtro.getExibirMovimentacaoSemVinculoDocumento() != null 
				&& filtro.getExibirMovimentacaoSemVinculoDocumento()
				&& filtro.getCarregarMovimentacoes() != null 
				&& filtro.getCarregarMovimentacoes()) {
			String sql = movimentacaoDAO.criaQueryAnaliseRecDesp(filtro, identificador, max, campo, ordenacao);
			SinedUtil.markAsReader();
			lista = getJdbcTemplate().query(sql, new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					Documento documento = new Documento();
					
					documento.setCddocumento(rs.getInt("cdmovimentacao"));
					documento.setDescricao(rs.getString("descricao"));
					documento.setDtvencimento(rs.getDate("dtbanco"));
					documento.setValor(new Money(rs.getInt("valor"), true));
					documento.setTipoRegistro("Movimenta��o");
					documento.setIsMovimentacao(true);
					
					return documento;
				}
			});			
		} else {
			String sql = this.criaQueryAnaliseRecDesp(filtro, identificador, max, campo, ordenacao);
			SinedUtil.markAsReader();
			lista = getJdbcTemplate().query(sql, new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					Documento documento = new Documento();
					
					documento.setCddocumento(rs.getInt("cddocumento"));
					documento.setDescricao(rs.getString("descricao"));
					documento.setDtvencimento(rs.getDate("dtvencimento"));
					documento.setValor(new Money(rs.getInt("valor"), true));
					documento.setDocumentoclasse(new Documentoclasse(rs.getInt("documentoclasse")));
					documento.setTipoRegistro(documento.getDocumentoclasse().getCddocumentoclasse().equals(Documentoclasse.CD_PAGAR) ? "Conta a pagar" : "Conta a receber");
					
					StringBuilder s = new StringBuilder();
					if(rs.getString("numero") != null && !"".equals(rs.getString("numero"))){
						s.append(rs.getString("numero"));
					}
					if(rs.getString("pessoanome") != null && !"".equals(rs.getString("pessoanome"))){
						s.append(!"".equals(s.toString()) ? " - " : "").append(rs.getString("pessoanome"));
					}
					s.append(!"".equals(s.toString()) ? " - " : "").append(documento.getDescricao());
					documento.setNumeroPessoaDescricao(s.toString());
					
					return documento;
				}
			});
		}
		
		return lista;
	}
	
	/**
	 * Cria query para buscar as contas da contagerencial.
	 *
	 * @param filtro
	 * @param identificador
	 * @param max
	 * @return
	 * @author Rodrigo Freitas
	 */
	private String criaQueryAnaliseRecDesp(AnaliseReceitaDespesaReportFiltro filtro, String identificador, Boolean max, String campo, String ordenacao) {
		
		StringBuilder query = new StringBuilder();

		query.append(" SELECT * from ( ");
		query.append(" SELECT documento.cddocumento AS cddocumento, ");
		query.append(" documento.descricao AS descricao, ");
		query.append(" documento.dtvencimento AS dtvencimento, ");
		query.append(" documento.cddocumentoclasse AS documentoclasse, ");
		
		if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)){
			query.append(" documento.numero AS numero, ");
			query.append(" pessoa.nome AS pessoanome, ");
		}
		query.append(" SUM(rateioitem.valor) AS valor ");

		query.append(contagerencialDAO.criaFrom(filtro));
		
		query.append(" AND vcontagerencial.identificador like '" + identificador + "%'");
		
		if(filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER)){
			query.append(" GROUP BY documento.cddocumento, documento.descricao, documento.dtvencimento, documento.numero, pessoa.nome ");
		}else{
			query.append(" GROUP BY documento.cddocumento, documento.descricao, documento.dtvencimento ");
		}
		
		if(StringUtils.isEmpty(campo) 
				|| (filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER) 
						&& filtro.getExibirMovimentacaoSemVinculoDocumento() != null
						&& filtro.getExibirMovimentacaoSemVinculoDocumento())) {
			query.append(" ORDER BY documento.dtvencimento desc ");
			query.append(") as query ORDER BY query.valor desc ");
		} else {
			query.append(" ORDER BY documento.dtvencimento desc ");
			query.append(") as query ORDER BY query." + campo + " " + ordenacao);
		}

		return query.toString();
	}
	
	/**
	 * Soma os valores dos documentos que s�o do tipo "Conta a receber" que est�o associados a um determinado projeto.
	 * 
	 * @param filtro Filtro do relat�rio de acompanhamento econ�mico.
	 * @param documentoClasse Classe de documento a buscar
	 * @param contagerencial Uma conta gerencial espec�fica ou <code>null</code> para somar os valores de todas as contas.
	 * @return
	 * @author Giovane Freitas
	 */
	public Money getTotalProjeto(AcompanhamentoEconomicoReportFiltro filtro, Documentoclasse documentoClasse, Contagerencial contagerencial) {
		if (filtro == null || filtro.getProjeto() == null || filtro.getProjeto().getCdprojeto() == null || documentoClasse == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		QueryBuilder<Long> q = newQueryBuilderSined(Long.class)
				.from(Documento.class)
				.join("documento.documentoacao acao")
				.join("documento.documentoclasse classe")
				.leftOuterJoin("documento.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem rateioitem")
				.leftOuterJoin("rateioitem.projeto projeto");

		if (contagerencial == null){
			q.select("sum(documento.valor)");
		} else {
			q.select("sum(rateioitem.valor)");
		}
		
		q.where("projeto = ?", filtro.getProjeto());
		q.where("classe = ?", documentoClasse);
		q.where("documento.dtvencimento <= ?", filtro.getDtFim());
		q.where("rateioitem.contagerencial = ?", contagerencial);
		
		q.where("acao <> ?", Documentoacao.CANCELADA);
		q.where("acao <> ?", Documentoacao.NAO_AUTORIZADA);
		q.where("acao <> ?", Documentoacao.DEVOLVIDA);
		q.where("acao <> ?", Documentoacao.NEGOCIADA);

		Long total = q.setUseTranslator(false).unique();
		if (total != null)
			return new Money(total, true);
		else
			return new Money(0);
	}
	

	/**
	* M�todo que verifica se existe contas a receber que est�o vinculadas a um contrato
	*
	* @param contrato
	* @return
	* @since Jul 27, 2011
	* @author Luiz Fernando F Silva
	*/
	public boolean findForContasContrato(Contrato contrato) {
		if(contrato == null || contrato.getCdcontrato() == null){
			throw new SinedException("Par�metros inv�lidos.");
		}
		
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Documento.class)
					.leftOuterJoin("documento.listaDocumentoOrigem documentoorigem")
					.leftOuterJoin("documentoorigem.contrato contrato")
					.leftOuterJoin("documento.listaNotaDocumento notadocumento")
					.leftOuterJoin("notadocumento.nota nota")
					.leftOuterJoin("nota.listaNotaContrato notacontrato")
					.leftOuterJoin("notacontrato.contrato contratocomnota")
					.where("contrato = ?", contrato).or()
					.where("contratocomnota = ?", contrato)
					.unique() > 0;
		
	}
	
	public void updateTipoPagamentoCliente(Integer cddocumento, Integer cdcliente) {
		getHibernateTemplate().bulkUpdate("update Documento d set d.tipopagamento = ?, d.pessoa = ?, d.outrospagamento = null where d.id = ?", 
				new Object[]{Tipopagamento.CLIENTE, new Pessoa(cdcliente), cddocumento});
	}
	
	public void updateEndereco(Integer cddocumento, Endereco endereco) {
		getHibernateTemplate().bulkUpdate("update Documento d set d.endereco = ? where d.id = ?", 
				new Object[]{endereco, cddocumento});
	}
	
	public void updateMsg2Msg3(Integer cddocumento) {
		getJdbcTemplate().execute("UPDATE DOCUMENTO SET MENSAGEM2 = NULL, MENSAGEM3 = NULL WHERE CDDOCUMENTO = " + cddocumento);
	}
	
	
	/**
	* M�todo com refer�ncia ao DAO
	* busca os materiais quando a conta a pagar tem origem de entrega	* 
	* 
	* @see	
	*
	* @param cdconta
	* @return
	* @since Aug 17, 2011
	* @author Luiz Fernando F Silva
	*/
	@SuppressWarnings("unchecked")
	public String buscaMateriaisByDocumento(Integer cdconta) {
		if(cdconta == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		StringBuilder sql = new StringBuilder("");
		
		sql
		  .append("select distinct m.cdmaterial, m.nome ")
		  .append("from Documento d ")
          .append("join Documentoorigem doco on doco.cddocumento = d.cddocumento ")
		  .append("join Entrega e on e.cdentrega = doco.cdentrega ")
		  .append("join Entregamaterial em on e.cdentrega = em.cdentrega ")
		  .append("join Material m on m.cdmaterial = em.cdmaterial ")
		  .append("where d.cddocumento = " + cdconta);
		SinedUtil.markAsReader();
		 List<String> listaMateriais = getJdbcTemplate().query(sql.toString(), 
				new RowMapper(){
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new String(rs.getString("nome"));
				}
		 
		 });
		 
		 String materiaisConcatenados = CollectionsUtil.concatenate(listaMateriais, ", ");
		 
		 return materiaisConcatenados;
			
	}
	/**
	* M�todo que buscar os dados da conta a receber para criar a nota fiscal
	*
	* @param documento
	* @return
	* @since Oct 18, 2011
	* @author Luiz Fernando F Silva
	*/
	public Documento findForCriarnotafiscal(Documento documento) {
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				.select("documento.cddocumento, documento.tipopagamento, documento.dtvencimento, documentoacao.cddocumentoacao, " +
						"pessoa.cdpessoa, pessoa.nome, pessoa.cnpj, pessoa.cpf, empresa.cdpessoa, " +
						"empresa.nome, empresa.razaosocial, empresa.nomefantasia, endereco.cdendereco, aux_documento.valoratual, documento.descricao, documento.valor")
				.leftOuterJoin("documento.pessoa pessoa")
				.leftOuterJoin("documento.endereco endereco")
				.leftOuterJoin("documento.empresa empresa")
				.join("documento.aux_documento aux_documento")
				.join("documento.documentoacao documentoacao")
				.where("documento = ?", documento)
				.unique();
	}
	
	/**
	 * Busca a lista de documentos para serem consultados na popup da tela de consulta de conta a pagar e receber
	 *
	 * @param pessoa
	 * @param classe
	 * @param listagemTotal 
	 * @return
	 * @since 10/11/2011
	 * @author Rodrigo Freitas
	 */
	public List<Documento> findByPessoaDocumentoclasse(Pessoa pessoa, Documentoclasse classe, boolean listagemTotal) {
		if(pessoa == null || pessoa.getCdpessoa() == null){
			throw new SinedException("A pessoa n�o pode ser nula.");
		}
		QueryBuilder<Documento> query = querySined()
					.select("documento.cddocumento, pessoa.nome, documento.dtvencimento, parcela.ordem, " +
							"parcelamento.iteracoes, documento.numero, documento.descricao, documentoacao.cddocumentoacao, " +
							"documentoacao.nome, aux_documento.valoratual")
					.join("documento.documentoacao documentoacao")
					.join("documento.aux_documento aux_documento")
					.leftOuterJoin("documento.listaParcela parcela")
					.leftOuterJoin("parcela.parcelamento parcelamento")
					.leftOuterJoin("documento.pessoa pessoa")
					.where("documento.documentoclasse = ?", classe)
					.where("pessoa = ?", pessoa)
					.where("documentoacao <> ?", Documentoacao.NEGOCIADA)
					.where("documentoacao <> ?", Documentoacao.CANCELADA)
					.orderBy("documento.cddocumento desc");
		
		if(!listagemTotal){
			query.setMaxResults(20);
		}
		return query.list();
	}
	
	/**
	 * Busca todos os registros para a exporta��o de arquivos gerenciais.
	 * @author Giovane Freitas <giovane.freitas@linkcom.com.br>
	 * @param documentoclasse 
	 */
	public Iterator<Object[]> findForArquivoGerencial(Documentoclasse documentoclasse) {
		
		//C�digo do Centro de Custo;Data da emiss�o;Data do Vencimento;Data do Pagamento;Valor;C�digo da Conta Gerencial;Situa��o;C�digo do Projeto;C�digo da Pessoa
		return newQueryBuilder(Object[].class)
			.select("rateioitem.centrocusto.id, documento.dtemissao, documento.dtvencimento, MAX(coalesce(movimentacao.dtmovimentacao, movimentacao.dtbanco))," +
					"rateioitem.valor as valor, documento.descricao, rateioitem.contagerencial.id, documentoacao.nome, rateioitem.projeto.id, pessoa.cdpessoa, " +
					"documento.id ")
			.from(Documento.class)
			.join("documento.rateio rateio")
			.join("documento.documentoacao documentoacao")
			.join("rateio.listaRateioitem rateioitem")
			.leftOuterJoin("documento.pessoa pessoa")
			.leftOuterJoin("documento.listaMovimentacaoOrigem movimentacaoorigem")
			.leftOuterJoin("movimentacaoorigem.movimentacao movimentacao WITH movimentacao.movimentacaoacao.cdmovimentacaoacao <> " + Movimentacaoacao.CANCELADA.getCdmovimentacaoacao())
			.where("documento.documentoclasse = ?", documentoclasse)
			.groupBy("rateioitem.centrocusto.id, rateioitem.contagerencial.id, rateioitem.projeto.id, " +
					"documento.dtemissao, documento.dtvencimento, documento.valor, documento.descricao, " +
					"documentoacao.nome, documento.id, pessoa.cdpessoa, rateioitem.valor")
			.iterate();
	}
	
	/**M�todo que busca os documentos de Conta a Pagar
	 * @author Thiago Augusto
	 * @param filtro
	 * @return
	 */
	public List<Documento> findDocumentosRemessa(GerarArquivoRemessaBean filtro){
		return querySined()
		.select("documento.cddocumento, documento.documentoacao,documento.descricao, documento.numero, documento.nossonumero, " +				
				"documento.dtemissao, documento.dtvencimento, documento.valor, documento.observacao, documentoacao.cddocumentoacao," +	// 8
				"pessoa.cdpessoa, pessoa.nome, pessoa.cpf, pessoa.cnpj, aux_documento.valoratual, endereco.cdendereco, " +		// 13
				"endereco.logradouro, endereco.bairro, endereco.numero, endereco.complemento,"+		// 17
				"endereco.cep, municipio.nome, uf.sigla, documento.mensagem1, documento.mensagem2, documento.mensagem3," +		   	// 23
				"documento.mensagem4, documento.mensagem5, taxa.cdtaxa, listaDadobancario.cddadobancario, listaDadobancario.agencia, listaDadobancario.dvagencia, listaDadobancario.conta, listaDadobancario.dvconta, banco.numero")						// 26
		.join("documento.aux_documento aux_documento")
		.join("documento.documentoclasse documentoclasse")
		.join("documento.documentoacao documentoacao")
		.join("documento.pessoa pessoa")
		.leftOuterJoin("pessoa.listaDadobancario listaDadobancario")
		.leftOuterJoin("listaDadobancario.banco banco")
		.leftOuterJoin("documento.endereco endereco")
		.leftOuterJoin("endereco.municipio municipio")
		.leftOuterJoin("municipio.uf uf")
		.leftOuterJoin("documento.taxa taxa")
		.where("documentoclasse = ?", Documentoclasse.OBJ_PAGAR)
		.whereIn("documento.cddocumento", filtro.getContas())
		.list();
	}
	
	/**
	 * M�todo que atualiza o documento gerado a partir da venda com o n�mero da NF
	 *
	 * @param documento
	 * @param numeroNF
	 * @author Luiz Fernando
	 */
	public void updateNumeroNFDocumentoVenda(Documento documento, String numeroNF) {
		if(documento == null || documento.getCddocumento() == null || numeroNF == null || "".equals(numeroNF))
			throw new SinedException("Par�metros inv�lidos.");
		
		getHibernateTemplate().bulkUpdate("update Documento d set d.numero = ? where d.cddocumento = ?",
				new Object[]{numeroNF, documento.getCddocumento()});
			
	}
	
	public void updateCdRateio(Integer documento, Integer cdRateio) {
		if(documento == null || cdRateio == null ){
			throw new SinedException("Par�metros inv�lidos.");
		}
		getHibernateTemplate().bulkUpdate("update Documento d set d.rateio.cdrateio = ? where d.cddocumento = ?",
				new Object[]{cdRateio, documento});
	}
	
	
	/**
	 * M�todo que atualiza o documento gerado a partir da venda com a nova data de vencimento
	 *
	 * @param documento
	 * @param dtvencimento
	 * @author Luiz Fernando
	 * @since 22/05/2014
	 */
	public void updateDtvencimentoNFDocumentoVenda(Documento documento, Date dtvencimento) {
		if(documento == null || documento.getCddocumento() == null || dtvencimento == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		getHibernateTemplate().bulkUpdate("update Documento d set d.dtvencimento = ? where d.cddocumento = ?",
				new Object[]{dtvencimento, documento.getCddocumento()});
			
	}
	
	/**
	 * M�todo que atualiza o documento gerado a partir da venda com a mensagem 1 e 2
	 *
	 * @param documento
	 * @param msg1
	 * @param msg2
	 * @author Luiz Fernando
	 */
	public void updateMesangem1And2NFDocumentoVenda(Documento documento, String msg1, String msg2) {
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		if(msg2 != null){
			getHibernateTemplate().bulkUpdate("update Documento d set d.mensagem1 = ?, d.mensagem2 = ? where d.cddocumento = ?",
					new Object[]{(msg1 != null ? msg1 : ""), (msg2 != null ? msg2 : "" ), documento.getCddocumento()});
		} else {
			getHibernateTemplate().bulkUpdate("update Documento d set d.mensagem1 = ? where d.cddocumento = ?",
					new Object[]{(msg1 != null ? msg1 : ""), documento.getCddocumento()});
		}
	}
	
	/**
	 * Atualiza o n�mero do documento a partir do n�mero da NF.
	 * Caso j� existe algo no campo ele concatena.
	 *
	 * @param documento
	 * @param numero
	 * @since 21/09/2012
	 * @author Rodrigo Freitas
	 */
	public void updateNumerosNFDocumentoFaturamento(Documento documento, String numero) {
		if(documento == null || documento.getCddocumento() == null || numero == null || "".equals(numero))
			throw new SinedException("Par�metros inv�lidos.");
		
		getHibernateTemplate().bulkUpdate("update Documento d set d.numero = d.numero || ' | ' || ? where d.cddocumento = ? and d.numero is not null and d.numero <> ''", new Object[]{numero, documento.getCddocumento()});
		getHibernateTemplate().bulkUpdate("update Documento d set d.numero = ? where d.cddocumento = ? and (d.numero is null or d.numero = '')", new Object[]{numero, documento.getCddocumento()});
	}
	
	/**
	 * M�todo que faz a atualiza��o do campo numero.
	 *
	 * @param documentos
	 * @param numeroNF
	 * @since 15/06/2012
	 * @author Rodrigo Freitas
	 */
	public void updateNumerosNFDocumento(String documentos, String numeroNF) {
		if(documentos == null || "".equals(documentos) || numeroNF == null || "".equals(numeroNF))
			throw new SinedException("Par�metros inv�lidos.");
		
		getHibernateTemplate().bulkUpdate("update Documento d set d.numero = ? where d.cddocumento in (" + documentos + ")",
				new Object[]{numeroNF});
			
	}
	
	/**
	 *
	 * M�todo que retorna um documento com o arquivo para a tela de conta a pagar
	 *
	 * @author Thiago Augusto
	 * @date 02/04/2012
	 * @param bean
	 * @return
	 */
	public Documento getDocumentoComArquivo(Documento bean){
		return querySined()
					.select("documento.cddocumento, arquivo.cdarquivo, arquivo.nome")
					.leftOuterJoin("documento.imagem arquivo")
					.where("documento = ?", bean)
					.unique();
	}
	
	/**
	 * 
	 * @param documentoAcao
	 * @return
	 */
	public List<Documento> findDocumentosSemMovimentacao(List<Integer> documentoAcao,Tipooperacao tipooperacao){		
		return querySined()
			.select("documento.cddocumento,documento.descricao,documento.valor,documentoacao.nome")
			.join("documento.documentoacao documentoacao")
			.join("documento.documentoclasse documentoclasse")
			.whereIn("documentoacao.cddocumentoacao",SinedUtil.concatenate(documentoAcao, ","))
			.where("documentoclasse.cddocumentoclasse = ?",tipooperacao.getCdtipooperacao())
		.list();
	}
	
	/**
	 *  
	 * @param documento
	 */
	public void atualizaDocumentoacao(Documento documento) {		
		getHibernateTemplate().bulkUpdate("update Documento d set d.documentoacao = ? where d.cddocumento = ?",
				new Object[]{Documentoacao.BAIXADA, documento.getCddocumento()});		
	}
	/**
	 * 
	 * M�todo que retorna uma conta a receber proveniente de uma movimentacao
	 *
	 *@author Rafael Salvio
	 *@date 10/05/2012
	 * @param bean
	 * @return Documento
	 */
	 
	public List<Documento> findDocumentoByMovimentacao(Movimentacao bean){
		if(bean == null || bean.getCdmovimentacao() == null)
			throw new SinedException("Par�metro inv�lido.");
		return querySined()
					.select("documento.cddocumento, documento.cddocumentorelacionado, documento.descricao, documento.numero, " +
							"documento.dtemissao, documento.dtvencimento, documento.dtdevolucao, documento.tipopagamento, " +
							"documento.nossonumero, documento.codigobarras, documento.dtvencimentooriginal, documento.referencia, " +
							"documento.dtcartorio, documento.outrospagamento, indicecorrecao.cdindicecorrecao, documento.mensagem1, " +
							"documento.mensagem2, documento.mensagem3, documento.mensagem4, documento.mensagem5, documento.mensagem6," +
							"documento.valor, documentotipo.taxavenda, documentotipo.percentual, documentotipo.cddocumentotipo, documentotipo.taxanoprocessamentoextratocartaocredito, " +
							"pessoa.cdpessoa, rateio.cdrateio, taxa.cdtaxa, empresa.cdpessoa, " +
							"imagem.cdarquivo, endereco.cdendereco, conta.cdconta, cheque.cdcheque, " +
							"tipotaxacontareceber.cdtipotaxa, tipotaxacontareceber.nome, " +
							"centrocustoTaxa.cdcentrocusto, centrocustoTaxa.nome, " +
							"contagerencialTaxa.cdcontagerencial, contagerencialTipo.nome, " +
							"centrocustoTipo.cdcentrocusto, centrocustoTaxa.nome, " +
							"contagerencialTipo.cdcontagerencial, contagerencialTipo.nome, " +
							"aux_documento.valoratual")
					.from(Documento.class)
					.join("documento.documentoclasse documentoclasse")
					.leftOuterJoin("documento.documentotipo documentotipo")
					.leftOuterJoin("documentotipo.tipotaxacontareceber tipotaxacontareceber")
					.leftOuterJoin("documentotipo.centrocusto centrocustoTipo")
					.leftOuterJoin("documentotipo.contagerencialtaxa contagerencialTipo")
					.leftOuterJoin("tipotaxacontareceber.contadebito contagerencialTaxa")
					.leftOuterJoin("tipotaxacontareceber.centrocusto centrocustoTaxa")
					.leftOuterJoin("documento.pessoa pessoa")
					.leftOuterJoin("documento.rateio rateio")
					.leftOuterJoin("documento.taxa taxa")
					.leftOuterJoin("documento.empresa empresa")
					.leftOuterJoin("documento.imagem imagem")
					.leftOuterJoin("documento.conta conta")
					.leftOuterJoin("documento.endereco endereco")
					.leftOuterJoin("documento.cheque cheque")
					.leftOuterJoin("documento.indicecorrecao indicecorrecao")
					.leftOuterJoin("documento.aux_documento aux_documento")
					.where("documento.cddocumento in (select doc.cddocumento " +
										" from Movimentacaoorigem mo " +
										" join mo.movimentacao m " +
										" join mo.documento doc " +
										" where m.cdmovimentacao = "+ bean.getCdmovimentacao()+" )")
					.where("documentoclasse.cddocumentoclasse = ?",Documentoclasse.CD_RECEBER)
					.list();
	}
	
	/**
	 * M�todo que verifica a exist�ncia de contas iguais � do par�metro de acordo com os seguintes campos:
	 * valor, data de emiss�o, data de vencimento, pessoa-recebimento e empresa
	 * @param documento
	 * @return Boolean
	 */
	public Boolean verificaDuplicidade(Documento documento){
		if(documento == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class);
		
		query
			.select("count(*)")
			.from(Documento.class)
			.where("documento.documentoclasse = ?", documento.getDocumentoclasse())
			.where("documento.dtemissao = ?", documento.getDtemissao())
			.where("documento.dtvencimento = ?",documento.getDtvencimento())
			.where("documento.valor = ?", documento.getValor())
			.where("documento.tipopagamento = ?", documento.getTipopagamento())
			.where("documento.documentoacao <> ?", Documentoacao.CANCELADA);
		
		if(documento.getCddocumento()!=null){
			query.where("documento.cddocumento != ?", documento.getCddocumento());
		}
		
		if(documento.getEmpresa() != null){
			query.where("documento.empresa = ?", documento.getEmpresa());
		}
		else{
			query.where("documento.empresa is null");
		}
		
		if(documento.getTipopagamento() == Tipopagamento.CLIENTE){
			query.where("documento.pessoa = ?", documento.getCliente());
		} else if(documento.getTipopagamento() == Tipopagamento.COLABORADOR){
			query.where("documento.pessoa = ?", documento.getColaborador());
		} else if(documento.getTipopagamento() == Tipopagamento.FORNECEDOR){
			query.where("documento.pessoa = ?", documento.getFornecedor());
		} else if(documento.getTipopagamento() == Tipopagamento.OUTROS){
			query.where("documento.outrospagamento = ?", documento.getOutrospagamento());
		}
		return query.unique() > 0;
	}
	
	
	/**
	 * M�todo que retorna um booleano que indica se existem documentos com data de vencimento referente a um per�odo fechado.
	 * @param whereIn
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaListaFechamento(String whereIn){
		if(whereIn == null || "".equals((whereIn)) )
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
				.setUseTranslator(Boolean.FALSE)
				.from(Documento.class)
				.select("count(*)")
				.whereIn("documento.cddocumento", whereIn)
				.where("exists ( SELECT f.id " +
								" FROM Fechamentofinanceiro f " +
								" LEFT OUTER JOIN f.conta c " +
								" WHERE f.empresa = documento.empresa " +
								" AND c is null " +
								" AND f.datainicio <= documento.dtvencimento " +
								" AND f.datalimite >= documento.dtvencimento " +
								" AND f.ativo = ?)", new Object[]{Boolean.TRUE})
				.unique() > 0;
	}
	
	/**
	 * M�todo que retorna um booleano que indica se existem documentos com data de vencimento referente a um per�odo fechado.
	 * @param whereInConta 
	 * @param whereIn, dtpagamento
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaListaFechamento(String whereIn, Date dtpagamento, String whereInConta){
		if(StringUtils.isBlank(whereIn) || dtpagamento == null)
			throw new SinedException("Par�metro inv�lido.");
		
		String condicao = "1=1";
		if(StringUtils.isNotBlank(whereInConta)){
			condicao = "c.id in (" + whereInConta + ") OR c is null";
		}
		
		return newQueryBuilderSined(Long.class)
				.setUseTranslator(Boolean.FALSE)
				.from(Documento.class)
				.select("count(*)")
				.whereIn("documento.cddocumento", whereIn)
				.where("exists ( SELECT f.id " +
								" FROM Fechamentofinanceiro f " +
								" LEFT OUTER JOIN f.conta c " +
								" WHERE f.empresa = documento.empresa " +
								" AND (" + condicao + ") " +
								" AND f.datainicio <= ? " +
								" AND f.datalimite >= ? " +
								" AND f.ativo = ?)", new Object[]{dtpagamento, dtpagamento, Boolean.TRUE})
				.unique() > 0 ;
	}
	
	/**
	 * Faz o update da dtcartorio dos documentos.
	 * 
	 * @param whereIn
	 * @param documentoacao
	 * @author Rafael Salvio
	 */
	public void updateRetirarCartorio(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Documento n�o pode ser nulo nem string vazia.");
		}

		getHibernateTemplate().bulkUpdate("update Documento d set d.dtcartorio = null where d.id in ("+whereIn+") and d.dtcartorio is not null");
		
	}
	
	public void updateDocumentoNumeroByNota(String numeroNfse, Nota nota) {
		getJdbcTemplate()
			.execute("UPDATE DOCUMENTO SET NUMERO = '" + numeroNfse + "' " +
						"WHERE CDDOCUMENTO IN (SELECT ND.CDDOCUMENTO FROM NOTADOCUMENTO ND WHERE ND.CDNOTA = " + nota.getCdNota() + " " +
								"UNION ALL " +
								"SELECT DOCO.CDDOCUMENTO FROM NOTAVENDA NV JOIN DOCUMENTOORIGEM DOCO ON DOCO.CDVENDA = NV.CDVENDA WHERE NV.CDNOTA = " + nota.getCdNota() + " " +
								")");
	}
	
	/**
	 * M�todo que verifica se o cheque j� est� associado ao documento 
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isChequeAssociado(Documento documento) {
		if(documento == null || documento.getCddocumento() == null)
			return Boolean.FALSE;
		
		return newQueryBuilderSined(Long.class)
			.setUseTranslator(Boolean.FALSE)
			.from(Documento.class)
			.leftOuterJoin("documento.cheque cheque")
			.select("count(*)")
			.where("documento.cddocumento = ?", documento.getCddocumento())
			.where("cheque = ?", documento.getCheque())
			.unique() > 0;
	}
	
	/**
	 * M�todo que verifica se existe documento relacionado a venda passada por par�metro
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean findForContasVenda(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nula.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Documento.class)
			.leftOuterJoin("documento.listaDocumentoOrigem documentoorigem")
			.leftOuterJoin("documentoorigem.venda venda")
			.openParentheses()
			.where("documentoorigem.venda = ?", venda)
			.or()
			.where("documentoorigem.pedidovenda = ?", venda.getPedidovenda())
			.closeParentheses()
			.unique() > 0;
	}
	
	/**
	 * M�todo que retorna o total de documento de antecipa��o do cliente
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getTotalAntecipacaoByCliente(Cliente cliente) {		
		String sql = 
			"select sum(d.valor - " + 
			"	       coalesce((select sum(vp.valororiginal) from venda v " +
			"					   join vendapagamento vp on vp.cdvenda = v.cdvenda     " +
			"	                   where vp.cddocumentoantecipacao = d.cddocumento " +
			"					   and v.cdvendasituacao <> " + Vendasituacao.CANCELADA.getCdvendasituacao() +"), 0) " +
			") as pago " + 		
			"	from documento d " +
			"   join documentotipo dt on d.cddocumentotipo = dt.cddocumentotipo and dt.antecipacao = true" +
			"	where d.cdpessoa = " + cliente.getCdpessoa() +
			" and d.cddocumentoacao = " + Documentoacao.BAIXADA.getCddocumentoacao();
		SinedUtil.markAsReader();
		Long total = (Long)getJdbcTemplate().queryForObject(sql.toString(), 
		new RowMapper() {
			public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getLong("pago");
			}
		});
		
		if (total != null)
			return new Money(total, true);
		else
			return new Money(0);
	}
	
	/**
	 * Carrega documentos a partir de um whereIn.
	 * 
	 * @param whereIn
	 * @return
	 * @author Marden Silva
	 */
	public List<Documento> findDocumentosForRemessaConfiguravel(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Documentos n�o podem ser nulos.");
		}
		return querySined()
					.select("documento.cddocumento, documento.documentoacao, documento.descricao, documento.numero, documento.nossonumero, " +				
							"documento.dtemissao, documento.dtvencimento, documento.valor, documento.observacao, documentoacao.cddocumentoacao, " +
							"documento.codigobarras, documento.rejeitadobanco, " +
							"pessoa.cdpessoa, pessoa.nome, pessoa.cpf, pessoa.cnpj, pessoa.tipopessoa, pessoa.email, " +
							"enderecopessoa.logradouro, enderecopessoa.bairro, enderecopessoa.numero, enderecopessoa.complemento, " +
							"enderecopessoa.cep, municipioPessoa.nome, ufPessoa.sigla, " +
							"cliente.cdpessoa, cliente.razaosocial, " +
							"aux_documento.valoratual, endereco.cdendereco, " +		
							"endereco.logradouro, endereco.bairro, endereco.numero, endereco.complemento, "+		
							"endereco.cep, municipio.nome, uf.sigla, documento.mensagem1, documento.mensagem2, documento.mensagem3," +		   	
							"documento.mensagem4, documento.mensagem5, documento.mensagem6, taxa.cdtaxa, taxaitem.cdtaxaitem, " +
							"tipotaxa.cdtipotaxa,tipotaxa.nome, taxaitem.valor, taxaitem.percentual, taxaitem.dtlimite, taxaitem.aodia, " +
							"dadobancario.cddadobancario, dadobancario.agencia, dadobancario.dvagencia, dadobancario.conta, dadobancario.dvconta, " +
							"banco.numero, documentotipo.cddocumentotipo, documentotipo.nome, documentotipo.tipotributo, tipoconta.cdtipoconta, tipoconta.nome," +
							"empresa.cdpessoa, empresa.razaosocial, enderecoEmp.cdendereco, enderecoEmp.logradouro, enderecoEmp.bairro, enderecoEmp.numero," +
							"enderecoEmp.complemento, enderecoEmp.cep, municipioEmp.cdmunicipio, municipioEmp.nome, ufEmp.cduf, ufEmp.nome, ufEmp.sigla, " +
							"conta.cdconta, empresatitular.cdpessoa, " +
							"documento.codigoreceita, documento.mesano, documento.valoroutrasentidades, documento.valoratualizacaomonetaria, " +
							"documento.dtapuracao, documento.numeroreferencia, documento.valorreceitabrutaacumulada, documento.percentualreceitabrutaacumulada, " +
							"documento.identificacaotributo, documento.iecodmunnumdec, documento.dividaativanumetiqueta, documento.numparcnotificacao, " + 
							"documento.anobase, documento.mesano, documento.renavam, documento.placaveiculo, documento.opcaopagamento, " +
							"uftributo.cduf, uftributo.sigla, municipiotributo.cdmunicipio, municipiotributo.cdibge, contacarteira.cdcontacarteira, " +
							"contacarteira.limiteposicoesintervalonossonumero," +
							"documento.nfgts,documento.lacreconectividade,documento.digitolacre ")
					.join("documento.aux_documento aux_documento")
					.join("documento.documentoclasse documentoclasse")
					.join("documento.documentoacao documentoacao")
					.leftOuterJoin("documento.documentotipo documentotipo")
					.join("documento.pessoa pessoa")
					.leftOuterJoin("pessoa.listaEndereco enderecopessoa")
					.leftOuterJoin("enderecopessoa.municipio municipioPessoa")
					.leftOuterJoin("municipioPessoa.uf ufPessoa")
					.leftOuterJoin("documento.empresa empresa")
					.leftOuterJoin("empresa.listaEndereco enderecoEmp")
					.leftOuterJoin("enderecoEmp.municipio municipioEmp")
					.leftOuterJoin("municipioEmp.uf ufEmp")					
					.leftOuterJoin("pessoa.cliente cliente")
					.leftOuterJoin("pessoa.listaDadobancario dadobancario")
					.leftOuterJoin("dadobancario.tipoconta tipoconta")
					.leftOuterJoin("dadobancario.banco banco")
					.leftOuterJoin("documento.endereco endereco")
					.leftOuterJoin("endereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("documento.taxa taxa")
					.leftOuterJoin("taxa.listaTaxaitem taxaitem")
					.leftOuterJoin("taxaitem.tipotaxa tipotaxa")
					.leftOuterJoin("documento.conta conta")
					.leftOuterJoin("documento.contacarteira contacarteira")
					.leftOuterJoin("conta.empresatitular empresatitular")
					.leftOuterJoin("documento.uf uftributo")
					.leftOuterJoin("documento.municipio municipiotributo")
					.whereIn("documento.cddocumento", whereIn)
					.orderBy("retira_acento(upper(pessoa.nome)), documento.dtemissao")
					.list();
	}
	
	/**
	 * M�todo que carrega os documentos cancelados para desmarcar parcela cobrada no contrato
	 *
	 * @param ids
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findforDesmarcarParcelacobradaOrigemContrato(String ids) {
		if(ids == null || "".equals(ids))
			throw new SinedException("Venda n�o pode ser nula.");
		
		return querySined()
				.select("documento.cddocumento, documento.dtvencimento, documentoacao.cddocumentoacao, documentoorigem.cddocumentoorigem, " +
						"contrato.cdcontrato, contratoparcela.cdcontratoparcela, contratoparcela.dtvencimento ")
				.join("documento.documentoacao documentoacao")
				.join("documento.listaDocumentoOrigem documentoorigem")
				.join("documentoorigem.contrato contrato")
				.join("contrato.listaContratoparcela contratoparcela")
				.whereIn("documento.cddocumento", ids)
				.where("documentoacao = ?", Documentoacao.CANCELADA)
				.list();
	}
	
	/**
	 * M�todo que busca os documento para cria��o da movimenta��o cont�bil
	 *
	 * @param whereInDocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForMovimentacaocontabil(String whereInDocumento) {
		if(whereInDocumento == null || "".equals(whereInDocumento))
			throw new SinedException("Par�metro inv�lido."); 
		
		return querySined()
				.select("documento.cddocumento, documento.valor, aux_documento.valoratual, pessoa.cdpessoa, documento.tipopagamento," +
						"documento.numero, empresa.cdpessoa ")
				.join("documento.pessoa pessoa")
				.join("documento.aux_documento aux_documento")
				.leftOuterJoin("documento.empresa empresa")
				.whereIn("documento.cddocumento", whereInDocumento)
				.list();
	}
	
	/**
	 * M�todo que faz busca os dados para a atualiza��o do rateio de despesa de viagem.
	 *
	 * @param despesaviagem
	 * @param documentoclasse
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/10/2012
	 */
	public List<Documento> findByDespesaviagem(Despesaviagem despesaviagem, Documentoclasse documentoclasse) {
		if(despesaviagem == null || despesaviagem.getCddespesaviagem() == null){
			throw new SinedException("Despesa de viagem n�o pode ser nula."); 
		}
		return querySined()
					.select("documento.cddocumento, documento.valor, rateio.cdrateio, documentoacao.cddocumentoacao")
					.join("documento.documentoclasse documentoclasse")
					.join("documento.documentoacao documentoacao")
					.join("documento.listaDocumentoOrigem documentoorigem")
					.leftOuterJoin("documentoorigem.despesaviagemadiantamento despesaviagemadiantamento")
					.leftOuterJoin("documentoorigem.despesaviagemacerto despesaviagemacerto")
					.leftOuterJoin("documento.rateio rateio")
					.where("documentoclasse = ?", documentoclasse)
					.openParentheses()
					.where("despesaviagemadiantamento = ?", despesaviagem)
					.or()
					.where("despesaviagemacerto = ?", despesaviagem)
					.closeParentheses()
					.list();
	}
	
	/**
	 * Busca os documentos pela venda.
	 *
	 * @param venda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/12/2012
	 */
	public List<Documento> findByVenda(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nula.");
		
		return querySined()
				.select("documento.cddocumento, documento.numero, documento.dtvencimento")
				.join("documento.listaDocumentoOrigem documentoorigem")
				.join("documentoorigem.venda venda")
				.where("venda = ?", venda)
				.list();
	}
	
	public List<Documento> findVendaDiferenteCanceladaNegociada(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nula.");
		
		return querySined()
				.select("documento.cddocumento, documento.numero, documento.dtvencimento")
				.join("documento.listaDocumentoOrigem documentoorigem")
				.join("documentoorigem.venda venda")
				.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
				.where("documento.documentoacao <> ?", Documentoacao.NEGOCIADA)
				.where("venda = ?", venda)
				.list();
	}
	
	/**
	 * M�todo que verifica se existe documentos que tenham rela��o com comissionamento que considera diferen�a de valor pago 
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isComissionamentoValorbrutoConsiderandodiferencavalorpago(String whereIn) {
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Documento.class)
			.leftOuterJoin("documento.listaDocumentoOrigem listaDocumentoOrigem")
			.leftOuterJoin("listaDocumentoOrigem.contrato contratoDoc")
			.leftOuterJoin("contratoDoc.comissionamento comissionamentoDocVendedor")
			.leftOuterJoin("contratoDoc.listaContratocolaborador listaContratocolaboradorDoc")
			.leftOuterJoin("listaContratocolaboradorDoc.comissionamento comissionamentoDocColaborador")
			
			
			.leftOuterJoin("documento.listaNotaDocumento listaNotaDocumento")
			.leftOuterJoin("listaNotaDocumento.nota nota")
			.leftOuterJoin("nota.listaNotaContrato listaNotaContrato")
			.leftOuterJoin("listaNotaContrato.contrato contratoNota")
			.leftOuterJoin("contratoNota.comissionamento comissionamentoNotaVendedor")
			.leftOuterJoin("contratoNota.listaContratocolaborador listaContratocolaboradorNota")
			.leftOuterJoin("listaContratocolaboradorNota.comissionamento comissionamentoNotaColaborador")
			
			.openParentheses()
				.where("comissionamentoDocVendedor.considerardiferencapagamento = true")
				.or()
				.where("comissionamentoNotaVendedor.considerardiferencapagamento = true")
				.or()
				.where("comissionamentoDocColaborador.considerardiferencapagamento = true")
				.or()
				.where("comissionamentoNotaColaborador.considerardiferencapagamento = true")
			.closeParentheses()	
			.openParentheses()
				.whereIn("documento.cddocumento", whereIn)
				.or()
				.where("documento.cddocumento in (select v.cddocumento " +
									"from Vdocumentonegociado v " +
									"where v.cddocumentonegociado in ( " + whereIn + ") )")
			.closeParentheses()
			.unique() > 0;
	}
	
	/**
	 * M�todo que busca os documentos negocidados para calcular qtde de parcelas
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForQtdeParcelasComissionamento(Documento documento) {
		return querySined()
				.select("documento.cddocumento, documentoacao.cddocumentoacao")
				.join("documento.documentoacao documentoacao")
				.where("documento.cddocumento in (select v.cddocumento " +
									"from Vdocumentonegociado v " +
									"where v.cddocumentonegociado = " + documento.getCddocumento() + " )")
				.list();
	}
	
	/**
	 * M�todo que busca os documentos negocidados de acordo com o documento e contrato para obter a nota
	 *
	 * @param documento
	 * @param contrato
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documento> findForDocumentoNegociadoWithNota(Documento documento, Contrato contrato) {
		return querySined()
				.select("documento.cddocumento, documentoacao.cddocumentoacao, nota.cdNota, notaTipo.cdNotaTipo, contrato.cdcontrato")
				.join("documento.documentoacao documentoacao")
				.join("documento.listaNotaDocumento listaNotaDocumento")
				.join("listaNotaDocumento.nota nota")
				.join("nota.listaNotaContrato listaNotaContrato")
				.join("listaNotaContrato.contrato contrato")
				.join("nota.notaTipo notaTipo")
				.where("contrato = ?", contrato)
				.where("documento.cddocumento in (select v.cddocumento " +
									"from Vdocumentonegociado v " +
									"where v.cddocumentonegociado = " + documento.getCddocumento() + " )")
				.list();
	}
	
	public void updateBoletoGerado(Documento documento){
		getHibernateTemplate().bulkUpdate("update Documento d set boletogerado=? where d=?", new Object[]{Boolean.TRUE, documento});
	}
	
	/**
	 * M�todo que atualiza o n�mero do documento
	 *
	 * @param documento
	 * @param numero
	 * @author Luiz Fernando
	 */
	public void updateNumeroByEntradafsical(Documento documento, String numero) {
		if(documento != null && documento.getCddocumento() != null && numero != null && !"".equals(numero)){
			getHibernateTemplate().bulkUpdate("update Documento d set numero = ? where d = ?", new Object[]{numero, documento});
		}
	}
	
	/**
	 * Busca as contas a receber que tenha o cddocumento ou nossonumero e a conta do filtro.
	 *
	 * @param listaNossoNumero
	 * @param conta
	 * @return
	 * @author Marden Silva
	 */
	public List<Documento> findForRetornoConfiguravel(List<ArquivoConfiguravelRetornoDocumentoBean> listaDocumento, Conta conta, Documentoclasse documentoclasse) {		
		return querySined()
				.select("documento.cddocumento, pessoa.nome, documento.outrospagamento, documento.somentenossonumero, documento.dtvencimento, " +
						"aux_documento.valoratual, documentoacao.cddocumentoacao, documentoacao.nome, documento.nossonumero ")
				.leftOuterJoin("documento.pessoa pessoa")
				.join("documento.documentoacao documentoacao")
				.leftOuterJoin("documento.conta conta")
				.join("documento.aux_documento aux_documento")
				.where("documento.documentoclasse = ?", documentoclasse)
				.where("conta = ?", conta)
				.openParentheses()
					.openParentheses()
						.openParentheses()
							.where("documento.somentenossonumero is null")
							.or()
							.where("documento.somentenossonumero = ?", Boolean.FALSE)
						.closeParentheses()
						.whereIn("documento.cddocumento", CollectionsUtil.listAndConcatenate(listaDocumento, "usoempresaLong", ","))
					.closeParentheses()
					.or()
					.openParentheses()
						.openParentheses()
							.where("documento.somentenossonumero is null")
							.or()
							.where("documento.somentenossonumero = ?", Boolean.FALSE)
						.closeParentheses()
						.whereIn("documento.cddocumento", CollectionsUtil.listAndConcatenate(listaDocumento, "nossonumeroLong", ","))
					.closeParentheses()
					.or()
					.where("documento.nossonumero in ('" + CollectionsUtil.listAndConcatenate(listaDocumento, "nossonumero", "','") + "')", !listaDocumento.isEmpty())
				.closeParentheses()
				.list();
	}
	
	/**
	 * Carrega um documento
	 *
	 * @param documento
	 * @return
	 * @author Marden Silva
	 */
	public Documento loadForRetornoConfiguravel(Documento documento) {		
		if (documento == null || documento.getCddocumento() == null)
			return null;
		
		return querySined()
				.select("documento.cddocumento, pessoa.nome, documento.outrospagamento, documento.somentenossonumero, documento.dtvencimento, " +
						"aux_documento.valoratual, documentoacao.cddocumentoacao, documentoacao.nome, documento.nossonumero ")
				.leftOuterJoin("documento.pessoa pessoa")
				.join("documento.documentoacao documentoacao")
				.leftOuterJoin("documento.conta conta")
				.join("documento.aux_documento aux_documento")
				.where("documento = ?", documento)
				.unique();
	}
	
	/**
	 * Busca as contas a receber que tenha o cddocumento ou nossonumero e a conta do filtro.
	 *
	 * @param listaNossoNumero
	 * @param conta
	 * @return
	 * @author Marden Silva
	 */
	public List<Documento> findForSelecaoDocumentoListagem(SelecionarDocumentoBean filtro) {	
		QueryBuilder<Documento> qry = querySined();
		qry.select("documento.cddocumento, pessoa.nome, documento.outrospagamento, documento.somentenossonumero, documento.dtvencimento, " +
			       "aux_documento.valoratual, documentoacao.cddocumentoacao, documentoacao.nome, documento.nossonumero")
			.leftOuterJoin("documento.pessoa pessoa")
			.join("documento.documentoacao documentoacao")
			.leftOuterJoin("documento.conta conta")
			.join("documento.documentoclasse documentoclasse")
			.join("documento.aux_documento aux_documento")
			.where("documento.nossonumero is null")
			.where("documento.cddocumento not in (" + filtro.getCds() + ")", filtro.getCds() != null && !"".equals(filtro.getCds()))
			.where("documentoclasse = ?", filtro.getDocumentoclasse())
			.where("conta = ?", filtro.getConta())
			.where("documento.dtvencimento >= ?", filtro.getDtVencimentoIni())
			.where("documento.dtvencimento <= ?", filtro.getDtVencimentoFim())
			.whereLikeIgnoreAll("documento.descricao", filtro.getDescricao())
			.whereLikeIgnoreAll("documento.numero", filtro.getNumero())
			.where("documento.cddocumento = ?", filtro.getCddocumento())
			.whereLikeIgnoreAll("pessoa.nome", filtro.getPessoa());
			
		if (filtro.isLimitar())
			qry.setMaxResults(filtro.limiteListagem);
		
		return qry.list();
	}
	
	public void updateNossoNumeroDocumento(Documento documento){
		if (documento != null && documento.getCddocumento() != null && documento.getNossonumero() != null)
			getHibernateTemplate().bulkUpdate("update Documento d set nossonumero=? where d=?", new Object[]{documento.getNossonumero(), documento});
	}
	
	/**
	 *  Atualiza o campo de rejeitado pelo banco do documento
	 * 
	 * @param documento
	 * @author Marden Silva
	 */
	public void updateRejeitado(Documento documento) {
		if (documento == null || documento.getCddocumento() == null || documento.getRejeitadobanco() == null) 
			throw new SinedException("Par�metros inv�lidos.");
		
		String sql = null;
		Object[] objetos = null;
		sql = "update Documento d set d.rejeitadobanco = ? where d.id = ?";
		objetos = new Object[] { documento.getRejeitadobanco(), documento.getCddocumento() };
		getHibernateTemplate().bulkUpdate(sql, objetos);
	}
	
	/**
	 * 
	 * @param filtro
	 * @author Thiago Clemnente
	 * 
	 */
	public void alterarContaReceberLote(String selectedItens, DocumentoFiltro filtro){
		
		if (selectedItens==null || selectedItens.trim().equals("")){
			return;
		}
		
		Date dtvencimentoLote = filtro.getDtvencimentoLote();
		Conta contaLote = filtro.getContaLote();
		Contacarteira contacarteiraLote = filtro.getContacarteiraLote();
		Cliente clienteLote = filtro.getClienteLote();
		Indicecorrecao indicecorrecaoLote = filtro.getIndicecorrecaoLote();
		Conta vinculoProvisionadoLote = filtro.getVinculoProvisionadoLote();
		
		String set = "";
		
		if(filtro.getValorLote() != null && filtro.getValorLote().getValue().doubleValue() > 0){
			set += "valor='" + filtro.getValorLote().multiply(new Money(100d)).getValue().doubleValue() + "',";
		}
		
		List<String> listaAlterouVencimento = new ArrayList<String>();
		
		if (dtvencimentoLote!=null){
			set += "dtvencimento='" + new SimpleDateFormat("yyyy-MM-dd").format(dtvencimentoLote) + "',";
			//Atualiza��o Data vencimento antiga
			List<Documento> listadocumento = this.findDocumentoUpdateBoletoETaxa(selectedItens);
			if(SinedUtil.isListNotEmpty(listadocumento)){
				for (Documento documento : listadocumento) {
					if(documento != null && documento.getCddocumento() != null
										 && documento.getDtvencimento() != null){
						documento.setDtvencimentoantiga(documento.getDtvencimento());
						this.updateDtVencimentoAntiga(documento);
					}
					if(Boolean.TRUE.equals(filtro.getAtualizarDtlimiteTaxa()) &&
						documento.getTaxa() != null && SinedUtil.isListNotEmpty(documento.getTaxa().getListaTaxaitem())){
						//Atualizando dtlimite das taxas
						for(Taxaitem ti: documento.getTaxa().getListaTaxaitem()){
							Date dtlimite = null;
							if(Tipocalculodias.ANTES_VENCIMENTO.equals(ti.getTipocalculodias())){
								dtlimite = SinedDateUtils.addDiasData(dtvencimentoLote, -ti.getDias());
							}else {
								dtlimite = SinedDateUtils.addDiasData(dtvencimentoLote, ti.getDias());
							}
							taxaitemService.updateDtlimite(ti, dtlimite);
						}
					}
					
					if (SinedDateUtils.diferencaDias(documento.getDtvencimento(), dtvencimentoLote)!=0){
						String sqlAlterouVencimento = "update documentohistorico dh set observacao = 'Altera��o em lote. Altera��o da data de vencimento de " + NeoFormater.getInstance().format(documento.getDtvencimento()) + " para " + NeoFormater.getInstance().format(dtvencimentoLote) + ". ' where dh.cddocumentohistorico = (select MAX(dh2.cddocumentohistorico) from documentohistorico dh2 where dh2.cddocumentoacao = 101 and dh2.cddocumento = " + documento.getCddocumento() + ")";
						listaAlterouVencimento.add(sqlAlterouVencimento);
					}
				}
			}
		}
		
		if (contaLote!=null){
			set += "cdconta=" + contaLote.getCdconta() + ",";
			if (contacarteiraLote!=null){
				set += "cdcontacarteira=" + contacarteiraLote.getCdcontacarteira() + ",";
			}
		}
		
		
		if (clienteLote!=null){
			set += "cdpessoa=" + clienteLote.getCdpessoa() + ",tipopagamento=0,";
		}
		
		if (indicecorrecaoLote!=null){
			set += "cdindicecorrecao=" + indicecorrecaoLote.getCdindicecorrecao() + ",";
		}
		
		if(vinculoProvisionadoLote!=null){
			set += "cdvinculoprovisionado=" + vinculoProvisionadoLote.getCdconta() + ",";
		}
		
		set = set.substring(0, set.length()-1);
		
		String sql = "update documento set <SET> where cddocumento in (" + selectedItens + ")";
		sql = sql.replace("<SET>", set);
		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		Integer cdusuarioaltera = usuario != null ?  usuario.getCdpessoa() : null;
		Timestamp dtaltera = new Timestamp(System.currentTimeMillis());
		
		
		String insertDocumentohistorico = 
			" insert into documentohistorico (cddocumentohistorico, cddocumento, cddocumentotipo, dtemissao, cdpessoa, tipopagamento, outrospagamento, numero, valor, descricao, observacao, cdusuarioaltera, dtaltera, cddocumentoacao) " +
			" (select nextval('sq_documentohistorico'), d.cddocumento, d.cddocumentotipo, d.dtemissao, d.cdpessoa, d.tipopagamento, " +
			" d.outrospagamento, d.numero, d.valor, d.descricao, 'Altera��o em lote.', " + cdusuarioaltera + ", '" + dtaltera + "', " + Documentoacao.ALTERADA.getCddocumentoacao() +
			" from documento d " +
			" where d.cddocumento in (" + selectedItens + ") )";
 
		getJdbcTemplate().execute(insertDocumentohistorico);
		getJdbcTemplate().update(sql);
		
		for (String sqlAlterouVencimento: listaAlterouVencimento){
			getJdbcTemplate().update(sqlAlterouVencimento);
		}
	}
	
	public void updatePessoaEndereco(Documento documento, Pessoa pessoa, Endereco endereco) {
		getHibernateTemplate().bulkUpdate("update Documento d set d.pessoa = ?, d.endereco = ? where d = ?", new Object[]{
			pessoa, endereco, documento
		});
	}	
	
	/**
	 * N�O UTILIZAR ESTE M�TODO!
	 * @author Rafael Salvio
	 */
	@Deprecated
	public List<Documento> getDocumentosForCorrecaoRateio(){
		return querySined()
				.select("documento.cddocumento, documento.valor, documento.dtemissao, rateio.cdrateio, rateioitem.cdrateioitem, rateio2.cdrateio, " +
						"centrocusto.cdcentrocusto, projeto.cdprojeto, contagerencial.cdcontagerencial, rateioitem.percentual, rateioitem.valor")
				.join("documento.rateio rateio")
				.join("rateio.listaRateioitem rateioitem")
				.leftOuterJoin("rateioitem.rateio rateio2")
				.leftOuterJoin("rateioitem.centrocusto centrocusto")
				.leftOuterJoin("rateioitem.projeto projeto")
				.leftOuterJoin("rateioitem.contagerencial contagerencial")
				.list();
	}
	
	/**
	 * Carrega o documento pelo N�mero e Empresa.
	 *
	 * @param empresa
	 * @param numero
	 * @return
	 * @author Rodrigo Freitas
	 * @since 24/10/2013
	 */
	public Documento loadContareceberByNumeroEmpresa(Empresa empresa, String numero, String doc) {
		List<Documento> lista = querySined()
									.select("documento.cddocumento, documento.valor, documentoacao.cddocumentoacao, pessoa.nome, documento.outrospagamento, documento.numero, "+
											"documentotipo.cddocumentotipo, documentotipo.taxanoprocessamentoextratocartaocredito, documentotipo.percentual, "+
											"documentotipo.taxavenda, aux_documento.valoratual, "+
											"tipotaxa.cdtipotaxa")
									.join("documento.documentoacao documentoacao")
									.leftOuterJoin("documento.pessoa pessoa")
									.leftOuterJoin("documento.aux_documento aux_documento")
									.leftOuterJoin("documento.documentotipo documentotipo")
									.leftOuterJoin("documentotipo.tipotaxacontareceber tipotaxa")
									.where("documento.empresa = ?", empresa)
									.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
									.openParentheses()
										.where("documento.numero = ?", numero)
										.or()
										.where("documento.numero = ?", doc)
									.closeParentheses()
									.list();
		
		if(lista != null && lista.size() == 1)
			return lista.get(0);
		else
			return null;
	}
	
	/**
	 * M�todo que carrega o documento com o valor atual
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 * @since 29/10/2013
	 */
	public Documento loadWithValoratual(Documento documento) {
		if(documento == null || documento.getCddocumento() == null){
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		
		return querySined()
			.select("documento.cddocumento, documento.valor, aux_documento.cddocumento, aux_documento.valoratual")
			.join("documento.aux_documento aux_documento")
			.where("documento = ?", documento)
			.unique();
	}
	
	public Documento loadWithEmpresa(Documento documento) {
		if(documento == null || documento.getCddocumento() == null){
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		return querySined()
			.select("empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, documento.cddocumento")
			.leftOuterJoin("documento.empresa empresa")
			.where("documento = ?", documento)
			.unique();
		
	}
	
	/**
	 * M�todo que verifica se existe documento com o campo numero preenchido com letra
	 *
	 * @return
	 * @author Luiz Fernando
	 * @since 02/05/2014
	 */
	public Boolean permitirFiltroDocumentoDeAte() {
		String sql = "select count(d.cddocumento) as total from documento d where d.cddocumentoclasse = 2 and d.numero ~ '[^0-9]' limit 1";
		try {
			Long count = (Long) getJdbcTemplate().queryForObject(sql.toString(), new RowMapper() {
				public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Long(rs.getLong("total"));
				}
			});	
			return count == 0;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * Busca as contas a receber que est�o baixadas da empresa, data emiss�o e valor passadas por par�metro.
	 *
	 * @param empresa
	 * @param dtvencimento
	 * @param valor
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/06/2014
	 */
	public List<Documento> findBaixadasByEmpresaDtemissaoDtvencimentoValor(Empresa empresa, Date dtemissao, Date dtvencimento, Money valor, Money valorTaxa, String numero, String doc) {
		if(valor == null) valor = new Money();
		QueryBuilder<Documento> query = querySined()
					.select("documento.cddocumento, documento.valor, documentoacao.cddocumentoacao, pessoa.nome, documento.outrospagamento, documento.numero, "+
							"documentotipo.cddocumentotipo, documentotipo.taxavenda, documentotipo.percentual, documentotipo.taxanoprocessamentoextratocartaocredito, "+
							"aux_documento.valoratual, tipotaxa.cdtipotaxa")
					.join("documento.documentoacao documentoacao")
					.leftOuterJoin("documento.aux_documento aux_documento")
					.leftOuterJoin("documento.documentotipo documentotipo")
					.leftOuterJoin("documentotipo.tipotaxacontareceber tipotaxa")
					.leftOuterJoin("documento.pessoa pessoa")
					.where("documento.empresa = ?", empresa)
					.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.where("documentoacao = ?", Documentoacao.BAIXADA)
					.openParentheses()
						.openParentheses()
							.where("documento.dtemissao = ?", dtemissao)
							.where("documento.dtvencimento = ?", dtvencimento);
		
		if(valorTaxa == null || valorTaxa.getValue().doubleValue() == 0){
			query.where("documento.valor = (select " + valor.toLong() + " + sum(coalesce(m1.valor, 0)) from Movimentacaoorigem mo1 join mo1.documento d1 join mo1.movimentacaorelacionada m1 where d1.cddocumento=documento.cddocumento and m1.tipooperacao=? and m1.movimentacaoacao<>?)", new Object[]{Tipooperacao.TIPO_DEBITO, Movimentacaoacao.CANCELADA});
		}else {
			query.where("documento.valor = ?", valor.toLong());
		}
		
		return query
						.closeParentheses()
						.or()
						.where("documento.numero = ?", numero)
						.or()
						.where("documento.numero = ?", doc)
					.closeParentheses()
					.list();
	}
	
	/**
	* M�todo que busca as contas a receber que est�o baixadas da empresa para concilia��o de cart�o de cr�dito
	*
	* @param empresa
	* @param dtemissao
	* @param valor
	* @param numero
	* @param cdvenda
	* @return
	* @since 29/08/2016
	* @author Luiz Fernando
	*/
	public List<Documento> findBaixadasByEmpresaDtemissaoValorCdvenda(Empresa empresa, Date dtemissao, Money valor, Money valorTaxa, String numero, Integer cdvenda) {
		if(valor == null) valor = new Money();
		QueryBuilder<Documento> query = querySined()
					.select("documento.cddocumento, documento.valor, documentoacao.cddocumentoacao, pessoa.nome, documento.outrospagamento, documento.numero, "+
							"documentotipo.cddocumentotipo, documentotipo.taxavenda, documentotipo.percentual, documentotipo.taxanoprocessamentoextratocartaocredito, "+
							"aux_documento.valoratual, tipotaxa.cdtipotaxa")
					.join("documento.documentoacao documentoacao")
					.leftOuterJoin("documento.aux_documento aux_documento")
					.leftOuterJoin("documento.documentotipo documentotipo")
					.leftOuterJoin("documentotipo.tipotaxacontareceber tipotaxa")
					.leftOuterJoin("documento.listaDocumentoOrigem listaDocumentoOrigem")
					.leftOuterJoin("documento.pessoa pessoa")
					.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.where("documento.empresa = ?", empresa)
					.where("documentoacao = ?", Documentoacao.BAIXADA)
					.openParentheses()
						.openParentheses()
							.where("documento.dtemissao = ?", dtemissao);
		
		if(valorTaxa == null || valorTaxa.getValue().doubleValue() == 0){
			query.where("documento.valor = (select " + valor.toLong() + " + sum(coalesce(m1.valor, 0)) from Movimentacaoorigem mo1 join mo1.documento d1 join mo1.movimentacaorelacionada m1 where d1.cddocumento=documento.cddocumento and m1.tipooperacao=? and m1.movimentacaoacao<>?)", new Object[]{Tipooperacao.TIPO_DEBITO, Movimentacaoacao.CANCELADA});
		}else {
			query.where("documento.valor = ?", valor.toLong());
		}
		
		return query
							.where("listaDocumentoOrigem.venda.cdvenda =?", cdvenda)
						.closeParentheses()
						.or()
						.where("documento.numero = ?", numero)
					.closeParentheses()
					.list();
	}
	
	/**
	 * Busca as contas a receber que est�o baixadas da empresa, no per�odo da data de emiss�o
	 *
	 * @param empresa
	 * @param dtemissao
	 * @return
	 * @author Rodrigo Freitas
	 * @param dtemissao22 
	 * @since 20/08/2014
	 */
	public List<Documento> findBaixadasByEmpresaDtemissaoPeriodo(Empresa empresa, Date dtemissao1, Date dtemissao2) {
		return querySined()
					.select("documento.cddocumento, documentoacao.cddocumentoacao, pessoa.nome, documento.outrospagamento, documento.numero, " +
							"documento.dtvencimento, documento.dtemissao, documento.valor, documento.descricao, " +
							"movimentacaoorigem.cdmovimentacaoorigem, movimentacao.cdmovimentacao, movimentacao.valor, movimentacaoacao.cdmovimentacaoacao, " +
							"aux_documento.valoratual, tipotaxa.cdtipotaxa, "+
							"documentotipo.cddocumentotipo, documentotipo.taxavenda, documentotipo.percentual, documentotipo.taxanoprocessamentoextratocartaocredito")
					.join("documento.documentoacao documentoacao")
					.join("documento.aux_documento aux_documento")
					.leftOuterJoin("documento.pessoa pessoa")
					.leftOuterJoin("documento.documentotipo documentotipo")
					.leftOuterJoin("documentotipo.tipotaxacontareceber tipotaxa")
					.join("documento.listaMovimentacaoOrigem movimentacaoorigem")
					.join("movimentacaoorigem.movimentacao movimentacao")
					.join("movimentacao.movimentacaoacao movimentacaoacao")
					.where("documento.empresa = ?", empresa)
					.where("documentoacao = ?", Documentoacao.BAIXADA)
					.where("documento.dtemissao >= ?", dtemissao1)
					.where("documento.dtemissao <= ?", dtemissao2)
					.where("movimentacaoacao = ?", Movimentacaoacao.NORMAL)
					.orderBy("documento.cddocumento")
					.list();
	}
	
	/**
	* M�todo que relaciona o novo cheque ao documento
	*
	* @param whereIn
	* @param chequeSubstituto
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public void updateContaChequeSubstituto(String whereIn, Cheque chequeSubstituto) {
		if(StringUtils.isNotEmpty(whereIn) && chequeSubstituto != null && chequeSubstituto.getCdcheque() != null){
			getHibernateTemplate().bulkUpdate(
					"update Documento set cdcheque = ? where cddocumento in (" + whereIn + ")"
					, new Object[]{chequeSubstituto.getCdcheque()}
			);
		}
	}
	
	/**
	* M�todo remove o cheque do documento
	*
	* @param whereIn
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public void updateContaRemoveCheque(String whereIn) {
		if(StringUtils.isNotEmpty(whereIn)){
			getHibernateTemplate().bulkUpdate("update Documento set cdcheque = null where cddocumento in (" + whereIn + ")");
		}
	}
	
	/**
	* M�todo que carrega os documetos para substitui��o de cheque
	*
	* @param whereInCheque
	* @param documentoclasse
	* @param documentoacao
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public List<Documento> findContaForSubstituicaoByCheque(String whereInCheque, Documentoclasse documentoclasse, Documentoacao... documentoacao) {
		if(StringUtils.isEmpty(whereInCheque) || documentoclasse == null || documentoacao == null || documentoacao.length == 0)
			return new ArrayList<Documento>();
		
		String whereInSituacao = null;
		StringBuilder values = new StringBuilder();
		for (int i=0; i < documentoacao.length; i++) {
			values.append(documentoacao[i].getCddocumentoacao()).append(",");
		}
		whereInSituacao = values.toString().substring(0, values.toString().length()-1);
		
		return querySined()
				.select("documento.cddocumento, cheque.cdcheque")
				.join("documento.cheque cheque")
				.whereIn("cheque.cdcheque", whereInCheque)
				.where("documento.documentoclasse = ?", documentoclasse)
				.where("documento.documentoacao " + 
						(Documentoclasse.OBJ_PAGAR.equals(documentoclasse) ? " not " : "") + 
						" in (" + whereInSituacao + ")")
				.list();
	}
	
	/**
	 * Retorna o pr�ximo n�mero do sequence sq_documento
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/09/2014
	 */
	public Integer getNextSequenceDocumento() {
		return getJdbcTemplate().queryForInt("select nextval('sq_documento')");
	}
	
	/**
	 * M�todo que atualiza a data da �ltima cobran�a
	 * @param data
	 * @param documento
	 */
	public void atualizaDataUltimaCobranca(Date data, Documento documento) {
		
		if (documento == null || documento.getCddocumento() == null) 
			throw new SinedException("Par�metros inv�lidos.");
		
		String sql = null;
		Object[] objetos = null;
		sql = "update Documento d set d.dtultimacobranca = ? where d.cddocumento = ?";
		objetos = new Object[] { data, documento.getCddocumento() };
		getHibernateTemplate().bulkUpdate(sql, objetos);
	}
	
	/**
	 * Busca os documento para a a��o de enviar mailing passando os cddocumentos(whereIn)
	 * @param whereIn
	 * @return
	 */
	public List<Documento> findByWhereInForMaillingDevedor(String whereIn){
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Documentos n�o podem ser nulos.");
		}
		return querySined()
		
		.select("documento.cddocumento, documento.descricao, documento.dtvencimento, documento.valor, " +
				"pessoa.cdpessoa, pessoa.nome, pessoa.email, conta.cdconta, aux_documento.valoratual, " +
				"empresa.cdpessoa, empresa.email, empresa.emailfinanceiro, empresa.formaEnvioBoleto ")
		.join("documento.pessoa pessoa")
		.join("documento.aux_documento aux_documento")
		.leftOuterJoin("documento.empresa empresa")
		.leftOuterJoin("documento.conta conta")
		.whereIn("documento.cddocumento", whereIn)
		.list();		
	}
	
	/**
	 * Atualiza o tipo de documento de uma conta a recebr/pagar
	 *
	 * @param documento
	 * @param documentotipo
	 * @author Rodrigo Freitas
	 * @since 03/11/2014
	 */
	public void updateDocumentotipo(Documento documento, Documentotipo documentotipo) {
		if (documento == null || documento.getCddocumento() == null) 
			throw new SinedException("Par�metros inv�lidos.");
		
		getHibernateTemplate().bulkUpdate("update Documento d set documentotipo = ? where d = ?", new Object[]{
			documentotipo, documento
		});
	}
	
	/**
	 * Atualiza a data de vencimento
	 *
	 * @param documento
	 * @author Rodrigo Freitas
	 * @since 26/05/2015
	 */
	public void updateVencimento(Documento documento) {
		if (documento == null || documento.getCddocumento() == null) 
			throw new SinedException("Par�metros inv�lidos.");
		
		getHibernateTemplate().bulkUpdate("update Documento d set dtvencimento = ? where d = ?", new Object[]{
			documento.getDtvencimento(), documento
		});
	}
	
	/**
	* M�todo que carrega os documentos com a data da movimenta��o
	*
	* @param whereIn
	* @return
	* @since 03/06/2015
	* @author Luiz Fernando
	*/
	public List<Documento> findWithDatapagamento(String whereIn) {
		if (whereIn == null || whereIn.equals(""))
			throw new SinedException("Documentos n�o podem ser nulos.");
		
		String sql = " select distinct d.cddocumento, m.dtmovimentacao as datapagamento " +
					 " from documento d " +
					 " join movimentacaoorigem mo on mo.cddocumento = d.cddocumento " +
					 " join movimentacao m on m.cdmovimentacao = mo.cdmovimentacao " +
					 " where d.cddocumento in ("+ whereIn +")";
		
		SinedUtil.markAsReader();
		@SuppressWarnings("unchecked")
		List<Documento> lista = getJdbcTemplate().query(sql,
				new RowMapper() {
					public Documento mapRow(ResultSet rs, int rowNum) throws SQLException {
						Documento d = new Documento();
						d.setCddocumento(rs.getInt("cddocumento"));
						d.setDatapagamento(rs.getDate("datapagamento"));
						return d;
					}
				});
		
		return lista;
	}
	
	/**
	 * @param entrega
	 * @return
	 * @since 16/06/2015
	 * @author Andrey Leonardo
	 */
	public Boolean existeContaPagarByEntrega(Entrega entrega){
		return newQueryBuilderSined(Long.class)
		.from(Documento.class)
		.select("count(*)")
		.join("documento.listaDocumentoOrigem listaDocumentoOrigem")
		.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
		.where("listaDocumentoOrigem.entrega = ?", entrega)
		.unique() > 0;
	}
	
	/**
	* M�todo que valida as situa��es do documento
	*
	* @param whereIn
	* @param diferente
	* @param documentoacao
	* @return
	* @since 05/10/2015
	* @author Luiz Fernando
	*/
	public boolean validaDocumentoSituacao(String whereIn, boolean diferente, Documentoacao... documentoacao) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		
		String whereInSituacao = null;
		StringBuilder values = new StringBuilder();
		for (int i=0; i < documentoacao.length; i++) {
			values.append(documentoacao[i].getCddocumentoacao()).append(",");
		}
		whereInSituacao = values.toString().substring(0, values.toString().length()-1);
		
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Documento.class)
					.join("documento.documentoacao documentoacao")
					.whereIn("documentoacao.cddocumentoacao " + (diferente ? "not" : ""), whereInSituacao)
					.whereIn("documento.cddocumento", whereIn)
					.unique() > 0;
	}
	
	/**
	 * Busca informa��es para o webservice SOAP
	 *
	 * @param empresa
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/04/2016
	 */
	public List<Documento> findForWSSOAP(Empresa empresa, Cliente cliente) {
		return querySined()
					.select("documento.cddocumento, documento.dtvencimento, aux_documento.valoratual")
					.join("documento.aux_documento aux_documento")
					.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.openParentheses()
						.where("documento.documentoacao = ?", Documentoacao.PREVISTA)
						.or()
						.where("documento.documentoacao = ?", Documentoacao.DEFINITIVA)
					.closeParentheses()
					.where("documento.empresa.cdpessoa = ?", empresa.getCdpessoa())
					.where("documento.pessoa.cdpessoa = ?", cliente.getCdpessoa())
					.list();
	}
	
	/**
	* M�todo que busca as contas a receber dos clientes
	*
	* @param whereIn
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<Documento> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		QueryBuilder<Documento> query = query()
			.select("documento.cddocumento, documento.descricao, documento.numero, documento.dtvencimento, documento.dtemissao, " +
					"documento.valor, pessoa.cdpessoa, documentoacao.cddocumentoacao ")
			.join("documento.pessoa pessoa")
			.join("documento.documentoacao documentoacao")
			.where("documento.tipopagamento = ?", Tipopagamento.CLIENTE)
			.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
			.whereIn("documento.cddocumento", whereIn);
		
		if(sincronizacaoInicial){
			List<Documentoacao> listaDocumentoacao = new ArrayList<Documentoacao>();
			listaDocumentoacao.add(Documentoacao.PREVISTA);
			listaDocumentoacao.add(Documentoacao.DEFINITIVA);
			listaDocumentoacao.add(Documentoacao.AUTORIZADA);
			listaDocumentoacao.add(Documentoacao.BAIXADA_PARCIAL);
			query.whereIn("documentoacao.cddocumentoacao", SinedUtil.listAndConcatenate(listaDocumentoacao, "cddocumentoacao", ","));
		}
		
		return query.list();
	}
	
	/**
	* M�todo que busca os documentos para atualizar os itens do rateio
	*
	* @param whereIn
	* @return
	* @since 16/09/2016
	* @author Luiz Fernando
	*/
	public List<Documento> findForAtualizarRateio(String whereIn) {
		if (StringUtils.isBlank(whereIn)) {
			throw new SinedException("Documentos n�o podem ser nulos.");
		}
		return querySined()
				.select("documento.cddocumento, documento.valor, rateio.cdrateio, item.cdrateioitem, item.valor, item.percentual")
				.join("documento.rateio rateio")
				.join("rateio.listaRateioitem item")
				.whereIn("documento.cddocumento", whereIn)
				.list();
	}
	
	/**
	* M�todo que verirfica se o documento tem origem de contrato com nota ap�s recebimento
	*
	* @param contas
	* @return
	* @since 19/10/2016
	* @author Luiz Fernando
	*/
	public boolean isOrigemContratoNotaAposRecebimento(String contas) {
		if (StringUtils.isBlank(contas)) {
			throw new SinedException("Documentos n�o podem ser nulos.");
		}
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Documento.class)
			.openParentheses()
				.where("documento.cddocumento in (select d.cddocumento " +
						" from Documentoorigem doco " +
						" join doco.documento d " +
						" join doco.contrato c " +
						" where d.cddocumento in (" + contas + ") and c.formafaturamento = " + Formafaturamento.NOTA_APOS_RECEBIMENTO.getValue() + ")")
				.or()
				.where("documento.cddocumento in (select vdoc.cddocumento " +
						" from Vdocumentonegociado vdoc " +
						" join vdoc.documento d " +
						" join d.listaDocumentoOrigem doco " +
						" join doco.contrato c " +
						" where d.cddocumento in (" + contas + ") and c.formafaturamento = " + Formafaturamento.NOTA_APOS_RECEBIMENTO.getValue() + ")")
			.closeParentheses()
			.unique() > 0;
	}
	
	/**
	 * M�todo respons�vel por retornar dtvencimento por documento
	 * @param wherein
	 * @return
	 * @since 17/11/2016
	 * @author C�sar
	 */
	public List<Documento> findDtVencimentoByWhereIn (String wherein){
		if(StringUtils.isEmpty(wherein)){
			throw new SinedException("Par�metro inv�lido para consulta.");
		}
		return querySined()
					.select("documento.cddocumento, documento.dtvencimento")
					.whereIn("documento.cddocumento", wherein)
					.list();
	}
	
	/**
	 * M�todo respons�vel por atualizar dtvencimentoantiga por documento
	 * @param documento
	 * @since 17/11/2016
	 * @author C�sar
	 */
	public void updateDtVencimentoAntiga(Documento documento){
		if(documento == null || documento.getCddocumento() == null 
							 || documento.getDtvencimentoantiga() == null){
			throw new SinedException("Par�metros incorretos para atualiza��o do documento.");			
		}
		String sql = "update documento set dtvencimentoantiga = '" + SinedDateUtils.toStringPostgre(documento.getDtvencimentoantiga())
															+ "' where cddocumento = " + documento.getCddocumento();
		getJdbcTemplate().update(sql);
	}
	
	public List<Documento> findForGerarLancamentoContabil(GerarLancamentoContabilFiltro filtro, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento, Formapagamento formaPagamento){
		List<MovimentacaocontabilTipoLancamentoEnum> listaTipoLancamentoEnum = new ArrayList<MovimentacaocontabilTipoLancamentoEnum>();
		if(MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_CARTAO.equals(movimentacaocontabilTipoLancamento) || 
				MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_CHEQUE.equals(movimentacaocontabilTipoLancamento) ||
				MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_CREDITO_CONTA_CORRENTE.equals(movimentacaocontabilTipoLancamento) ||
				MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_DINHEIRO.equals(movimentacaocontabilTipoLancamento) ||
				MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_RETORNO_BANCARIO.equals(movimentacaocontabilTipoLancamento)){
			listaTipoLancamentoEnum.add(MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_CARTAO);
			listaTipoLancamentoEnum.add(MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_CHEQUE);
			listaTipoLancamentoEnum.add(MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_CREDITO_CONTA_CORRENTE);
			listaTipoLancamentoEnum.add(MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_DINHEIRO);
			listaTipoLancamentoEnum.add(MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_RETORNO_BANCARIO);
			
		}else {
			listaTipoLancamentoEnum.add(movimentacaocontabilTipoLancamento);
		}
		
		boolean baixadaParcial = MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_PARCIAL.equals(movimentacaocontabilTipoLancamento);
		boolean baixaPorFormaPagamento = MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_POR_FORMA_PAGAMENTO.equals(movimentacaocontabilTipoLancamento);
		List<Documentoacao> listaDocumentoacao = new ArrayList<Documentoacao>();
		listaDocumentoacao.add(Documentoacao.BAIXADA);
		if(baixadaParcial || baixaPorFormaPagamento){
			listaDocumentoacao.add(Documentoacao.BAIXADA_PARCIAL);	
		}
		
		return querySined()
				.select("documento.cddocumento, documento.numero, documento.tipopagamento, documento.outrospagamento, documento.descricao, documento.valor," +
						"aux_documento.valoratual, aux_documento.valorjuros, aux_documento.valorjurosmes, aux_documento.valormulta, aux_documento.valordesconto, aux_documento.valordesagio," +
						"aux_documento.valortaxaboleto, aux_documento.valortermo, aux_documento.valortaxamovimento, aux_documento.valortaxabaixa," +
						"documentoclasse.cddocumentoclasse, documentotipo.cddocumentotipo, documentotipo.cartao, " +
						"cliente.nome, cliente.razaosocial, fornecedor.nome, fornecedor.razaosocial, colaborador.nome, colaborador.cdpessoa, " +
						"contaContabilCliente.cdcontacontabil, contaContabilCliente.nome, contaContabilFornecedor.cdcontacontabil, contaContabilFornecedor.nome," +
						"contaContabilColaborador.cdcontacontabil, contaContabilColaborador.nome, " +
						"listaParcela.ordem," +
						"movimentacao.cdmovimentacao, movimentacao.dtmovimentacao, movimentacao.dtbanco, movimentacao.valor, movimentacaoacao.cdmovimentacaoacao, " +
						"contaContabilMovimentacao.cdcontacontabil, contaContabilMovimentacao.nome," +
						"contaContabilOrigem.cdcontacontabil, contaContabilOrigem.nome," +
						"contaContabilCredito.cdcontacontabil, contaContabilCredito.nome," +
						"listaRateioitem.percentual, listaRateioitem.valor, centrocusto.cdcentrocusto, centrocusto.nome," +
						"cheque.numero, cheque.dtbompara, cheque.banco, cheque.agencia, cheque.conta, cheque.emitente, cheque.valor," +
						"projeto.cdprojeto, projeto.nome, " +
						"formapagamento.cdformapagamento")
				.join("documento.aux_documento aux_documento")
				.join("documento.documentoclasse documentoclasse")
				.leftOuterJoin("documento.documentotipo documentotipo")
				.leftOuterJoin("documento.pessoa pessoa")
				.leftOuterJoin("pessoa.cliente cliente")
				.leftOuterJoin("cliente.contaContabil contaContabilCliente")
				.leftOuterJoin("pessoa.fornecedorBean fornecedor")
				.leftOuterJoin("fornecedor.contaContabil contaContabilFornecedor")
				.leftOuterJoin("documento.listaParcela listaParcela")
				.leftOuterJoin("pessoa.colaborador colaborador")
				.leftOuterJoin("colaborador.contaContabil contaContabilColaborador")
				.join("documento.listaMovimentacaoOrigem listaMovimentacaoOrigem")
				.join("listaMovimentacaoOrigem.movimentacao movimentacao")
				.leftOuterJoin("movimentacao.movimentacaoacao movimentacaoacao")
				.leftOuterJoin("listaMovimentacaoOrigem.conta contaorigem")
				.leftOuterJoin("contaorigem.contaContabil contaContabilOrigem")
				.join("movimentacao.formapagamento formapagamento")
				.join("movimentacao.rateio rateio")
				.join("rateio.listaRateioitem listaRateioitem")
				.join("listaRateioitem.centrocusto centrocusto")
				.leftOuterJoin("listaRateioitem.projeto projeto")
				.join("movimentacao.conta contamovimentacao")
				.leftOuterJoin("contamovimentacao.contaContabil contaContabilMovimentacao")
				.leftOuterJoin("movimentacao.cheque cheque")
				.leftOuterJoin("movimentacao.listaMovimentacaoorigemcartaocredito listaMovimentacaoorigemcartaocredito")
				.leftOuterJoin("listaMovimentacaoorigemcartaocredito.conta contacartaocredito")
				.leftOuterJoin("contacartaocredito.contaContabil contaContabilCredito")
				.where("documento.empresa=?", filtro.getEmpresa())
				.where("formapagamento=?", formaPagamento, MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_POR_FORMA_PAGAMENTO.equals(movimentacaocontabilTipoLancamento))
				.where("coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)>=?", filtro.getDtPeriodoInicio())
				.where("coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)<=?", filtro.getDtPeriodoFim())
				.where("listaRateioitem.projeto=?", filtro.getProjeto())
				.whereIn("documento.documentoacao.cddocumentoacao", CollectionsUtil.listAndConcatenate(listaDocumentoacao, "cddocumentoacao", ","))
				.where("documentoclasse=?", Documentoclasse.OBJ_PAGAR, MovimentacaocontabilTipoLancamentoEnum.CONTA_PAGAR_BAIXADA_FORNECEDOR.equals(movimentacaocontabilTipoLancamento) || MovimentacaocontabilTipoLancamentoEnum.JUROS_PAGOS.equals(movimentacaocontabilTipoLancamento) || MovimentacaocontabilTipoLancamentoEnum.DESCONTOS_OBTIDOS.equals(movimentacaocontabilTipoLancamento))
				.where("documentoclasse=?", Documentoclasse.OBJ_RECEBER, MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_DINHEIRO.equals(movimentacaocontabilTipoLancamento) || MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_CHEQUE.equals(movimentacaocontabilTipoLancamento) || MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_CARTAO.equals(movimentacaocontabilTipoLancamento) || MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_RETORNO_BANCARIO.equals(movimentacaocontabilTipoLancamento)
																	|| MovimentacaocontabilTipoLancamentoEnum.JUROS_RECEBIDOS.equals(movimentacaocontabilTipoLancamento) || MovimentacaocontabilTipoLancamentoEnum.DESCONTOS_CONCEDIDOS.equals(movimentacaocontabilTipoLancamento) || MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_CREDITO_CONTA_CORRENTE.equals(movimentacaocontabilTipoLancamento) || MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_PARCIAL.equals(movimentacaocontabilTipoLancamento)
																	|| MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_POR_FORMA_PAGAMENTO.equals(movimentacaocontabilTipoLancamento))
				.where("movimentacao.formapagamento=? and coalesce(documentotipo.cartao, false) = false ", Formapagamento.DINHEIRO, MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_DINHEIRO.equals(movimentacaocontabilTipoLancamento))
				.where("movimentacao.formapagamento=?", Formapagamento.CHEQUE, MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_CHEQUE.equals(movimentacaocontabilTipoLancamento))
				.where("coalesce(documentotipo.cartao, false) = true", MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_CARTAO.equals(movimentacaocontabilTipoLancamento))
				.where("movimentacao.formapagamento=?", Formapagamento.CREDITOCONTACORRENTE, MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_CREDITO_CONTA_CORRENTE.equals(movimentacaocontabilTipoLancamento))
				.openParentheses()
					.where("aux_documento.valorjuros>0", MovimentacaocontabilTipoLancamentoEnum.JUROS_PAGOS.equals(movimentacaocontabilTipoLancamento) || MovimentacaocontabilTipoLancamentoEnum.JUROS_RECEBIDOS.equals(movimentacaocontabilTipoLancamento))
					.or()
					.where("aux_documento.valorjurosmes>0", MovimentacaocontabilTipoLancamentoEnum.JUROS_PAGOS.equals(movimentacaocontabilTipoLancamento) || MovimentacaocontabilTipoLancamentoEnum.JUROS_RECEBIDOS.equals(movimentacaocontabilTipoLancamento))
				.closeParentheses()
				.where("aux_documento.valordesconto>0", MovimentacaocontabilTipoLancamentoEnum.DESCONTOS_OBTIDOS.equals(movimentacaocontabilTipoLancamento) || MovimentacaocontabilTipoLancamentoEnum.DESCONTOS_CONCEDIDOS.equals(movimentacaocontabilTipoLancamento))
				.where("exists (select 1 from Arquivobancariodocumento abd where abd.documento=documento)", MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_RETORNO_BANCARIO.equals(movimentacaocontabilTipoLancamento))
//				.where("listaMovimentacaoorigemcartaocredito is not null", MovimentacaocontabilTipoLancamentoEnum.CONCILIACAO_CARTAO.equals(movimentacaocontabilTipoLancamento))  //O sistema ir� buscar estes registros na consulta de transfer�ncia movimentacaoDAO.findForGerarLancamentoContabilTransferencia
				.where("not exists (select 1 from Movimentacaocontabilorigem mco where mco.documento=documento and mco.movimentacaocontabil.tipoLancamento in (" + MovimentacaocontabilTipoLancamentoEnum.listAndConcatenate(listaTipoLancamentoEnum) + "))", !baixadaParcial && !baixaPorFormaPagamento)
				.where("not exists (select 1 from Movimentacaocontabilorigem mco where mco.documento=documento and mco.movimentacao=movimentacao and mco.movimentacaocontabil.tipoLancamento in (" + MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_POR_FORMA_PAGAMENTO.getId() + "))", baixaPorFormaPagamento)
				.where("exists (select d.cddocumento from Documentohistorico dh join dh.documento d join dh.documentoacao docacao where d.cddocumento = documento.cddocumento and docacao = ?)", Documentoacao.BAIXADA_PARCIAL, baixadaParcial && !baixaPorFormaPagamento)
				.where("not exists (select d.cddocumento from Documentohistorico dh join dh.documento d join dh.documentoacao docacao where d.cddocumento = documento.cddocumento and docacao = ?)", Documentoacao.BAIXADA_PARCIAL, !baixadaParcial && !baixaPorFormaPagamento)
				.list();
	}
	
	public List<Documento> findForGerarLancamentoContabilAntecipacao(GerarLancamentoContabilFiltro filtro, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento){
		List<MovimentacaocontabilTipoLancamentoEnum> listaTipoLancamentoEnum = new ArrayList<MovimentacaocontabilTipoLancamentoEnum>();
		listaTipoLancamentoEnum.add(movimentacaocontabilTipoLancamento);
		List<Documentoacao> listaDocumentoacao = new ArrayList<Documentoacao>();
		listaDocumentoacao.add(Documentoacao.BAIXADA);
		listaDocumentoacao.add(Documentoacao.BAIXADA_PARCIAL);	
		QueryBuilder<Documento> query = querySined();
			query
				.select("documento.cddocumento, documento.numero, documento.tipopagamento, documento.outrospagamento, documento.descricao, documento.valor," +
						"aux_documento.valoratual, aux_documento.valorjuros, aux_documento.valormulta, aux_documento.valordesconto, aux_documento.valordesagio," +
						"aux_documento.valortaxaboleto, aux_documento.valortermo, aux_documento.valortaxamovimento, aux_documento.valortaxabaixa," +
						"documentoclasse.cddocumentoclasse, documentotipo.cddocumentotipo, documentotipo.cartao, " +
						"cliente.nome, cliente.razaosocial, fornecedor.nome, fornecedor.razaosocial, colaborador.nome, colaborador.cdpessoa, " +
						"contaContabilCliente.cdcontacontabil, contaContabilCliente.nome, contaContabilFornecedor.cdcontacontabil, contaContabilFornecedor.nome," +
						"contaContabilColaborador.cdcontacontabil, contaContabilColaborador.nome, " +
						"listaParcela.ordem," +
						"listaRateioitem.percentual, listaRateioitem.valor, centrocusto.cdcentrocusto, centrocusto.nome," +
						"listaHistoricoAntecipacaoDestino.cdHistoricoAntecipacao, listaHistoricoAntecipacaoDestino.descricao, listaHistoricoAntecipacaoDestino.dataHora, " +
						"listaHistoricoAntecipacaoDestino.valor, " +
						"projeto.cdprojeto, projeto.nome, " +
						"documento2.cddocumento")
				.join("documento.aux_documento aux_documento")
				.join("documento.documentoclasse documentoclasse")
				.leftOuterJoin("documento.documentotipo documentotipo")
				.leftOuterJoin("documento.pessoa pessoa")
				.leftOuterJoin("pessoa.cliente cliente")
				.leftOuterJoin("cliente.contaContabil contaContabilCliente")
				.leftOuterJoin("pessoa.fornecedorBean fornecedor")
				.leftOuterJoin("fornecedor.contaContabil contaContabilFornecedor")
				.leftOuterJoin("documento.listaParcela listaParcela")
				.leftOuterJoin("pessoa.colaborador colaborador")
				.leftOuterJoin("colaborador.contaContabil contaContabilColaborador")
				.join("documento.listaHistoricoAntecipacaoDestino listaHistoricoAntecipacaoDestino")
				.join("listaHistoricoAntecipacaoDestino.documento documento2")
				.join("documento.rateio rateio")
				.join("rateio.listaRateioitem listaRateioitem")
				.join("listaRateioitem.centrocusto centrocusto")
				.leftOuterJoin("listaRateioitem.projeto projeto")
				.where("documento.empresa=?", filtro.getEmpresa())
				.where("listaRateioitem.projeto=?", filtro.getProjeto())
				.whereIn("documento.documentoacao.cddocumentoacao", CollectionsUtil.listAndConcatenate(listaDocumentoacao, "cddocumentoacao", ","))
				.where("not exists (select 1 from Movimentacaocontabilorigem mco where mco.documento=documento and mco.movimentacaocontabil.tipoLancamento in (" + MovimentacaocontabilTipoLancamentoEnum.listAndConcatenate(listaTipoLancamentoEnum) + "))")
				.where("documentoclasse=?", Documentoclasse.OBJ_PAGAR, MovimentacaocontabilTipoLancamentoEnum.CONTA_PAGAR_BAIXADA_ADIANTAMENTO.equals(movimentacaocontabilTipoLancamento))
				.where("documentoclasse=?", Documentoclasse.OBJ_RECEBER, MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_ADIANTAMENTO.equals(movimentacaocontabilTipoLancamento));
		String parametro = parametrogeralService.getValorPorNome(Parametrogeral.DATA_PAGAMENTO_ANTECIPACAO);
		parametro = parametro != null ? br.com.linkcom.utils.StringUtils.tiraAcento(parametro).toUpperCase() : "";
		
		if ("ANTECIPACAO".equalsIgnoreCase(parametro)) {
			query.where("exists(select 1 from Movimentacaoorigem mo join mo.movimentacao "+
					" join mo.movimentacao.movimentacaoacao "+
					" join mo.documento where mo.documento = listaHistoricoAntecipacaoDestino.documento and mo.movimentacao.dtmovimentacao>='" +SinedDateUtils.toStringPostgre(filtro.getDtPeriodoInicio()) +
					"' and mo.movimentacao.dtmovimentacao<='" +SinedDateUtils.toStringPostgre(filtro.getDtPeriodoFim())+"' and mo.movimentacao.movimentacaoacao.cdmovimentacaoacao not in ("+Movimentacaoacao.CANCELADA.getCdmovimentacaoacao()+", "+Movimentacaoacao.CANCEL_MOVIMENTACAO.getCdmovimentacaoacao()+"))");
		}else{
			query.where("cast(listaHistoricoAntecipacaoDestino.dataHora as date)>=?", filtro.getDtPeriodoInicio())
				.where("cast(listaHistoricoAntecipacaoDestino.dataHora as date)<=?", filtro.getDtPeriodoFim());
		}
		return query.list();
	}
	
	public void updateCancelamentocobranca(String whereIn, boolean cancelamentocobranca){
		whereIn = whereIn.replace(" ", "");
		String[] ids = whereIn.split(",");
		List<String> listaIds = Arrays.asList(ids);
		
		String sql = "update documento d set cancelamentocobranca=" + cancelamentocobranca;
		sql += " where exists (select 1 from documentoconfirmacao dc where dc.cddocumento=d.cddocumento) and (1=2";
		
		for (int i=0; i<listaIds.size(); i+=1000){
			int fim = (i + 1000 > listaIds.size()) ? listaIds.size() : i + 1000;
			sql += " or d.cddocumento in (" + CollectionsUtil.concatenate(listaIds.subList(i, fim), ",") + ")";
		}
		
		sql += ")";
		
		getJdbcTemplate().update(sql);
	}
	
	public void updateEmissaoboletobloqueado(Documento documento){
		getHibernateTemplate().bulkUpdate("update Documento d set emissaoboletobloqueado=? where d=?", new Object[]{Boolean.TRUE, documento});
	}
	
	public boolean isPossuiEmissaoBoletoBloqueado(String whereIn){
		SinedUtil.markAsReader();
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.whereIn("documento.cddocumento", whereIn)
				.where("documento.emissaoboletobloqueado=true")
				.unique()>0;
	}
	
	public void updateCheque(Documento documento, Cheque cheque) {
		if(documento != null && documento.getCddocumento() != null && cheque != null && cheque.getCdcheque() != null){
			getHibernateTemplate().bulkUpdate("update Documento d set cheque = ? where d = ?", new Object[]{cheque, documento});
		}
	}
	
	/***
	 * Verifico se as outras contas geradas a partir de uma negocia��o est�o canceladas.
	 * 
	 * @param cddocumento
	 * @return
	 */
	public boolean isTodasContasCanceladasOrigemNegociacao(Integer cddocumento){
		String sql = " select count(*)";
		sql += " from documento d1";
		sql += " where d1.cddocumento in (select dni1.cddocumento from documentonegociadoitem dni1 where dni1.cddocumentonegociado in (select dni2.cddocumentonegociado from documentonegociadoitem dni2 where dni2.cddocumento = " + cddocumento + "))";
		sql += " and d1.cddocumento<>" + cddocumento;
		sql += " and d1.cddocumentoacao<>0 ";
		
		return getJdbcTemplate().queryForInt(sql)==0;
	}
	
	public void updateDocumentoacaoanteriornegociacao(Documento documento, Documentoacao documentoacaoanteriornegociacao){
		getHibernateTemplate().bulkUpdate("update Documento d set documentoacaoanteriornegociacao=? where d=?", new Object[]{documentoacaoanteriornegociacao, documento});
	}
	
	public void updateTaxa(Documento documento, Taxa taxa){
		getHibernateTemplate().bulkUpdate("update Documento d set taxa=? where d=?", new Object[]{taxa, documento});
	}
	
	private QueryBuilder<?> getQueryBuilderMovimentacaoTaxa(Documento documento){
		return querySined()
				.join("documento.aux_documento aux_documento")
				.join("documento.documentotipo documentotipo")
				.join("documentotipo.centrocusto centrocusto")
				.join("documentotipo.contagerencialtaxa contagerencialtaxa")
				.leftOuterJoin("documentotipo.formapagamentodebito formapagamentodebito")
				.where("documento=?", documento)
				.where("documentotipo.taxavenda>0")
				.where("documentotipo.gerarmovimentacaoseparadataxa=?", Boolean.TRUE);
	}
	
	@SuppressWarnings("unchecked")
	public boolean isMovimentacaoTaxa(Documento documento){
		QueryBuilder<Long> queryBuilder = (QueryBuilder<Long>) getQueryBuilderMovimentacaoTaxa(documento);
		return queryBuilder.select("count(*)").unique()>0;
	}
	
	@SuppressWarnings("unchecked")
	public Documento loadForMovimentacaoTaxa(Documento documento) {
		if (documento==null || documento.getCddocumento()==null){
			return null;
		}
		
		QueryBuilder<Documento> queryBuilder = (QueryBuilder<Documento>) getQueryBuilderMovimentacaoTaxa(documento);
		
		return queryBuilder
				.select("documento.cddocumento, documento.valor, aux_documento.valoratual, documentotipo.percentual, documentotipo.taxavenda, c" +
						"entrocusto.cdcentrocusto, contagerencialtaxa.cdcontagerencial, formapagamentodebito.cdformapagamento, " +
						"taxa.cdtaxa, tipotaxa.cdtipotaxa, tipotaxa.nome, ti.percentual,ti.valor,ti.dtlimite, documento.dtvencimento, "+
						"documento.descricao, documento.tipopagamento, conta.numero, conta.nome, pessoa.nome, documento.outrospagamento, documento.numero, "+
						"listaParcela.ordem, parcelamento.iteracoes")
				.leftOuterJoinIfNotExists("documento.taxa taxa")
				.leftOuterJoinIfNotExists("taxa.listaTaxaitem ti")
				.leftOuterJoinIfNotExists("ti.tipotaxa tipotaxa")
				.leftOuterJoinIfNotExists("documento.conta conta")
				.leftOuterJoinIfNotExists("documento.pessoa pessoa")
				.leftOuterJoinIfNotExists("documento.listaParcela listaParcela")
				.leftOuterJoinIfNotExists("listaParcela.parcelamento parcelamento")
				.unique();
	}
	
	public boolean isPossuiDocumentotipoAntecipacao(String whereIn){
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		SinedUtil.markAsReader();
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.whereIn("documento.cddocumento", whereIn)
				.where("documento.documentotipo.antecipacao = ?", Boolean.TRUE)
				.unique()>0;
	}
	
	public void updateDocumentotipo(String whereIn, Documentotipo documentotipo){
		getJdbcTemplate().update("update documento set cddocumentotipo = " + documentotipo.getCddocumentotipo() + " where cddocumento in (" + whereIn + ")");
	}
	
	public boolean isPodeAlterarValorConta(Documento documento){
		List<String> listaCampo = Arrays.asList(new String[]{"cdcontrato", "cdentrega", "cdentregadocumento", "cdvenda", "cdpedidovenda"});
		String sqlCountDocumentoorigem = "select count(*) from documentoorigem where cddocumento=? and (1=2";
		for (String campo: listaCampo){
			sqlCountDocumentoorigem += " or " + campo + " is not null ";
		}
		sqlCountDocumentoorigem += ")";
		
		int countDocumentoorigem = getJdbcTemplate().queryForInt(sqlCountDocumentoorigem, new Object[]{documento.getCddocumento()});
		if (countDocumentoorigem==0){
			int countNotadocumento = getJdbcTemplate().queryForInt("select count(*) from notadocumento where cddocumento=?", new Object[]{documento.getCddocumento()});
			if (countNotadocumento==0){
				return getJdbcTemplate().queryForInt("select count(*) from contratofaturalocacaodocumento where cddocumento=?", new Object[]{documento.getCddocumento()})==0;
			}else {
				return false;
			}
		}else {
			return false;
		}
	}
	
	public List<Documento> findDocsCopiaChequeForTemplate(String whereIn){
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Documentos n�o podem ser nulos.");
		}
		return querySined()
				.select(
						"documentotipo.nome, pessoa.nome, documento.cddocumento, documento.numero, documento.dtemissao, "
								+ "documento.dtvencimento, documento.valor, documento.descricao,"
								+ "documento.tipopagamento, documento.outrospagamento, conta.cdconta, "
								+ "empresa.nome, empresa.razaosocial, empresa.nomefantasia, logomarca.cdarquivo, "
								+ "cheque.valor, cheque.cdcheque, cheque.numero, cheque.agencia, cheque.banco, cheque.emitente, "
								+ "cheque.conta, cheque.dtbompara, cheque.linhanumerica, cheque.observacao, "
								+ "chequeMov.valor, chequeMov.cdcheque, chequeMov.numero, chequeMov.agencia, chequeMov.banco, chequeMov.emitente, "
								+ "chequeMov.conta, chequeMov.dtbompara, chequeMov.linhanumerica, chequeMov.observacao, "
								+ "empresaMov.nome, empresaMov.razaosocial, empresaMov.nomefantasia, "
								+ "empresaMovimentacaoCheque.nome, empresaMovimentacaoCheque.razaosocial, empresaMovimentacaoCheque.nomefantasia, "
								+ "empresaChequeMov.nome, empresaChequeMov.razaosocial, empresaChequeMov.nomefantasia, "
								+ "empresaCheque.nome, empresaCheque.razaosocial, empresaCheque.nomefantasia, "
								+ "bancoMov.nome, contaMov.agencia, contaMov.numero, contaMov.dvnumero, contaMov.dvagencia, "
								/*Movimenta��es do documento*/
								+ "movimentacao.dtmovimentacao, movimentacao.dtbanco, movimentacao.cdmovimentacao, "
								+ "movimentacao.observacaocopiacheque, movimentacao.valor, movimentacao.historico, "
								+ "tipooperacao.cdtipooperacao, documento.documentoclasse, documento.tipopagamento, "
								+ "fornecedor.identificador, cliente.identificador, "
								/*Movimenta��es do cheque ligado � movimenta��o que est� ligada ao documento*/
								+ "listaMovimentacaoCheque.dtmovimentacao, listaMovimentacaoCheque.dtbanco, listaMovimentacaoCheque.cdmovimentacao, "
								+ "listaMovimentacaoCheque.observacaocopiacheque, listaMovimentacaoCheque.valor, listaMovimentacaoCheque.historico, "
								+ "tipooperacaoMovimentacaoCheque.cdtipooperacao, tipooperacaoMovimentacaoCheque.nome, "
								/*Documentos*/
								+ "documentoMovimentacao.cddocumento, documentoMovimentacao.numero, documentoMovimentacao.dtemissao, "
								+ "documentoMovimentacao.dtvencimento, documentoMovimentacao.valor, documentoMovimentacao.descricao,"
								+ "documentoMovimentacao.tipopagamento, documentoMovimentacao.outrospagamento, docClasseMovimentacao.nome, docClasseMovimentacao.cddocumentoclasse, "
								+ "pessoaDocumentoMovimentacao.nome, fornecedorDocumentoMovimentacao.identificador, clienteDocumentoMovimentacao.identificador, "
								+ "contaDocumentoMovimentacao.cdconta, "
								/*Movimenta��es do cheque ligado ao documento*/
								+ "listaMovimentacaoChequeDoc.dtmovimentacao, listaMovimentacaoChequeDoc.dtbanco, listaMovimentacaoChequeDoc.cdmovimentacao, "
								+ "listaMovimentacaoChequeDoc.observacaocopiacheque, listaMovimentacaoChequeDoc.valor, listaMovimentacaoChequeDoc.historico, "
								/*Documentos*/
								+ "documentoChequeDoc.cddocumento, documentoChequeDoc.numero, documentoChequeDoc.dtemissao, "
								+ "documentoChequeDoc.dtvencimento, documentoChequeDoc.valor, documentoChequeDoc.descricao, "
								+ "documentoChequeDoc.tipopagamento, documentoChequeDoc.outrospagamento, docClasseChequeDoc.nome, docClasseChequeDoc.cddocumentoclasse, "
								+ "pessoaDocumentoChequeDoc.nome, fornecedorDocumentoChequeDoc.identificador, clienteDocumentoChequeDoc.identificador, "
								+ "contaDocumentoChequeDoc.cdconta, "
								
								+ "documentoCheque.cddocumento, documentoCheque.numero, documentoCheque.dtemissao, "
								+ "documentoCheque.dtvencimento, documentoCheque.valor, documentoCheque.descricao, "
								+ "documentoCheque.tipopagamento, documentoCheque.outrospagamento, documentoClasseCheque.nome, documentoClasseCheque.cddocumentoclasse, "
								+ "pessoaDocumentoCheque.nome, fornecedorDocumentoCheque.identificador, clienteDocumentoCheque.identificador, "
								+ "contaDocumentoCheque.cdconta")
								
				.leftOuterJoin("documento.pessoa pessoa")
				.leftOuterJoin("documento.conta conta")
				.leftOuterJoin("pessoa.cliente cliente")
				.leftOuterJoin("pessoa.fornecedorBean fornecedor")
				.leftOuterJoin("documento.empresa empresa")
				.leftOuterJoin("empresa.logomarca logomarca")
				.leftOuterJoin("documento.listaMovimentacaoOrigem listaMovimentacaoOrigem")
				/*Movimenta��es do documento*/
				.leftOuterJoin("listaMovimentacaoOrigem.movimentacao movimentacao")
				.leftOuterJoin("movimentacao.listaMovimentacaoorigem movimentacaoorigem")
				.leftOuterJoin("listaMovimentacaoOrigem.documento documentoMovimentacao")
				.leftOuterJoin("documentoMovimentacao.documentoclasse docClasseMovimentacao")
				.leftOuterJoin("documentoMovimentacao.pessoa pessoaDocumentoMovimentacao")
				.leftOuterJoin("documentoMovimentacao.conta contaDocumentoMovimentacao")
				.leftOuterJoin("pessoaDocumentoMovimentacao.cliente clienteDocumentoMovimentacao")
				.leftOuterJoin("pessoaDocumentoMovimentacao.fornecedorBean fornecedorDocumentoMovimentacao")
				.leftOuterJoin("movimentacao.tipooperacao tipooperacao")
				.leftOuterJoin("movimentacao.empresa empresaMov")
				.leftOuterJoin("movimentacao.conta contaMov")
				.leftOuterJoin("contaMov.banco bancoMov")
				
				/*Movimenta��es do cheque ligado � movimenta��o que est� ligada ao documento*/
				.leftOuterJoin("movimentacao.cheque chequeMov")
				.leftOuterJoin("chequeMov.listaMovimentacao listaMovimentacaoCheque")
				.leftOuterJoin("listaMovimentacaoCheque.empresa empresaMovimentacaoCheque")
				.leftOuterJoin("listaMovimentacaoCheque.tipooperacao tipooperacaoMovimentacaoCheque")
				.leftOuterJoin("listaMovimentacaoCheque.listaMovimentacaoorigem movimentacaoorigemCheque")
				.leftOuterJoin("movimentacaoorigemCheque.documento documentoCheque")
				.leftOuterJoin("documentoCheque.documentoclasse documentoClasseCheque")
				.leftOuterJoin("documentoCheque.conta contaDocumentoCheque")
				.leftOuterJoin("documentoCheque.pessoa pessoaDocumentoCheque")
				.leftOuterJoin("pessoaDocumentoCheque.cliente clienteDocumentoCheque")
				.leftOuterJoin("pessoaDocumentoCheque.fornecedorBean fornecedorDocumentoCheque")
				.leftOuterJoin("chequeMov.empresa empresaChequeMov")
				
				.join("documento.documentotipo documentotipo")
				
				/*Movimenta��es do cheque ligado ao documento*/				
				.leftOuterJoin("documento.cheque cheque")
				.leftOuterJoin("cheque.listaMovimentacao listaMovimentacaoChequeDoc")
				.leftOuterJoin("listaMovimentacaoChequeDoc.listaMovimentacaoorigem movimentacaoorigemChequeDoc")
				.leftOuterJoin("movimentacaoorigemChequeDoc.documento documentoChequeDoc")
				.leftOuterJoin("documentoChequeDoc.documentoclasse docClasseChequeDoc")
				.leftOuterJoin("documentoChequeDoc.conta contaDocumentoChequeDoc")
				.leftOuterJoin("documentoChequeDoc.pessoa pessoaDocumentoChequeDoc")
				.leftOuterJoin("pessoaDocumentoChequeDoc.cliente clienteDocumentoChequeDoc")
				.leftOuterJoin("pessoaDocumentoChequeDoc.fornecedorBean fornecedorDocumentoChequeDoc")
				
				.leftOuterJoin("cheque.empresa empresaCheque")
				.whereIn("documento.cddocumento", whereIn)
				.orderBy("documento.cddocumento")
				.list();
	}
	
	public List<Documento> findForFormapagamento(String whereIn, String whereInMovimentacoes){
		return querySined()
			.select("formapagamento.cdformapagamento, movimentacaoacao.cdmovimentacaoacao, movimentacaoacao.nome ")
			.join("documento.listaMovimentacaoOrigem listaMovimentacaoOrigem")
			.join("listaMovimentacaoOrigem.movimentacao movimentacao")
			.join("movimentacao.movimentacaoacao movimentacaoacao")
			.join("movimentacao.formapagamento formapagamento")
			.whereIn("documento.cddocumento", whereIn)
			.whereIn("movimentacao.cdmovimentacao", whereInMovimentacoes)
			.list();
	}
	
	public List<Documento> findForUpdateTaxaitem(String whereIn){
		return querySined()
			.select("documento.cddocumento, taxa.cdtaxa, listaTaxaitem.cdtaxaitem, tipotaxa.cdtipotaxa, empresa.cdpessoa")
			.join("documento.empresa empresa")
			.join("documento.taxa taxa")
			.join("taxa.listaTaxaitem listaTaxaitem")
			.join("listaTaxaitem.tipotaxa tipotaxa")
			.whereIn("documento.cddocumento", whereIn)
			.list();
	}
	
	public List<Documento> findForGerarLancamentoContabilContaGerencial(GerarLancamentoContabilFiltro filtro, Contagerencial contagerencial, String whereNotIn, String whereNotInRatioitem){
		return querySined()
				.select("documento.cddocumento, documento.numero, documento.dtcompetencia, documento.tipopagamento, documento.outrospagamento, documento.descricao, documento.dtemissao, documento.valor," +
						"aux_documento.valoratual, aux_documento.valorjuros, aux_documento.valormulta, aux_documento.valordesconto, aux_documento.valordesagio," +
						"aux_documento.valortaxaboleto, aux_documento.valortermo, aux_documento.valortaxamovimento, aux_documento.valortaxabaixa, documento.documentoclasse," +
						"cliente.nome, cliente.razaosocial, fornecedor.nome, fornecedor.razaosocial, listaRateioitem.cdrateioitem," +
						"contaContabilCliente.cdcontacontabil, contaContabilCliente.nome, contacontabilFornecedor.cdcontacontabil, contacontabilFornecedor.nome," +
						"listaParcela.ordem, listaRateioitem.percentual, listaRateioitem.valor, contagerencial.cdcontagerencial, contagerencial.nome," +
						"centrocusto.cdcentrocusto, centrocusto.nome, projeto.cdprojeto, projeto.nome")
				.join("documento.aux_documento aux_documento")
				.leftOuterJoin("documento.pessoa pessoa")
				.leftOuterJoin("pessoa.cliente cliente")
				.leftOuterJoin("cliente.contaContabil contaContabilCliente")
				.leftOuterJoin("pessoa.fornecedorBean fornecedor")
				.leftOuterJoin("fornecedor.contaContabil contacontabilFornecedor")
				.leftOuterJoin("documento.listaParcela listaParcela")
				.join("documento.rateio rateio")
				.join("rateio.listaRateioitem listaRateioitem")
				.join("listaRateioitem.contagerencial contagerencial")
				.join("listaRateioitem.centrocusto centrocusto")
				.leftOuterJoin("listaRateioitem.projeto projeto")
				.where("documento.empresa=?", filtro.getEmpresa())
				.where("coalesce(documento.dtcompetencia, documento.dtemissao)>=?", filtro.getDtPeriodoInicio())
				.where("coalesce(documento.dtcompetencia, documento.dtemissao)<=?", filtro.getDtPeriodoFim())
				.where("projeto=?", filtro.getProjeto())
				.where("documento.documentoacao not in (?,?,?)", new Object[]{Documentoacao.CANCELADA, Documentoacao.NEGOCIADA, Documentoacao.PREVISTA})
				.where("contagerencial=?", contagerencial)
				.where("documento.cddocumento not in (" + whereNotIn +  ")", !whereNotIn.trim().equals(""))
				.where("listaRateioitem.cdrateioitem not in (" + whereNotInRatioitem +  ")", !whereNotInRatioitem.trim().equals(""))
				.where("not exists (select 1 from Movimentacaocontabilorigem mco where mco.documento=documento and mco.movimentacaocontabil.tipoLancamento=?)", new Object[]{MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_CONTA_PAGAR_RECEBER})
				.where("not exists (select 1 from NotaDocumento nd join nd.documento d where d.cddocumento = documento.cddocumento) ")
				.where("not exists (select 1 from Documentoorigem doco join doco.entrega ed join doco.documento d where d.cddocumento = documento.cddocumento) ")
				.where("not exists (select 1 from Documentoorigem doco join doco.entregadocumento ed join doco.documento d where d.cddocumento = documento.cddocumento) ")
				.list();
	}
	
	public List<Documento> findForValidationDtbanco(String whereIn){
		return querySined()
			.select("documento.cddocumento, " +
					"documentotipo.cddocumentotipo, documentotipo.nome, documentotipo.preencherdatabanco, documentotipo.diasreceber, " +
					"empresa.cdpessoa, empresa.nome")
			.join("documento.documentotipo documentotipo")
			.leftOuterJoin("documento.empresa empresa")
			.whereIn("documento.cddocumento", whereIn)
			.list();
	}
	
	public List<Documento> findForValiationProcessamentoExtratocartao(String whereInMovimentacoes){
		return querySined()
			.select("documento.cddocumento, movimentacao.cdmovimentacao, "+
					"documentotipo.cddocumentotipo, tipotaxa.cdtipotaxa, tipotaxa.nome, "+
					"contadebito.cdcontagerencial, contadebito.nome, "+
					"centrocusto.cdcentrocusto, centrocusto.nome, "+
					"contagerencialTipodocumento.cdcontagerencial, contagerencialTipodocumento.nome, "+
					"centrocustoTipodocumento.cdcentrocusto, centrocustoTipodocumento.nome ")
			.join("documento.listaMovimentacaoOrigem movimentacaoorigem")
			.join("movimentacaoorigem.movimentacao movimentacao")
			.join("documento.documentotipo documentotipo")
			.leftOuterJoin("documentotipo.tipotaxacontareceber tipotaxa")
			.leftOuterJoin("tipotaxa.contadebito contadebito")
			.leftOuterJoin("tipotaxa.centrocusto centrocusto")
			.leftOuterJoin("documentotipo.centrocusto centrocustoTipodocumento")
			.leftOuterJoin("documentotipo.contagerencialtaxa contagerencialTipodocumento")
			.whereIn("movimentacao.cdmovimentacao", whereInMovimentacoes)
			.where("documentotipo.taxanoprocessamentoextratocartaocredito = ?", Boolean.TRUE)
			.list();
	}
	/**
	 * Atualizadas os atributos mensage2, mensagem3 e taxa do documento.
	 * @param documento - documento que deve ser atualizado.
	 * @param mapFields - Hashmap com as mensagem 2 e 3. Deve conter as chaves 'mensagem2' e 'mensagem3'.
	 * @param taxa - Nova taxa do documento.
	 */
	public void updateMsgBoleto(Documento documento, Map<String, String> mapFields, Taxa taxa) {
		if(taxa!=null && taxa.getCdtaxa() != null){
			mapFields.put("cdtaxa", taxa.getCdtaxa().toString());
		}else{
			mapFields.put("cdtaxa", "null");
		}
		
		updateDocumento(documento, mapFields);
	}
	
	/**
	 * Atualiza campos do documento.
	 * @param documento - documento a ser atualizado.
	 * @param mapFields - Map com campos a serem atualizados. o key � o nome do campo e value o valor.
	 */
	private void updateDocumento(Documento documento, Map<String, String> mapFields){
		if(documento.getCddocumento() == null)
			return;
		StringBuilder sql = new StringBuilder();
		sql.append("update documento set <SET> where cddocumento = " + documento.getCddocumento());
		String campos[] = mapFields.keySet().toArray(new String[mapFields.size()]);
		StringBuilder set = new StringBuilder();
		for(int i = 0; i < campos.length; i++){
			String fieldValue = "null".equals(mapFields.get(campos[i]))? "null": ("'"+mapFields.get(campos[i])+"'");
			set.append(campos[i] + " = " + fieldValue);
			if(i + 1 < campos.length)
				set.append(", ");
		}
		getJdbcTemplate().update(sql.toString().replace("<SET>", set.toString()));
	}
	
	/**
	 * Carrega os documentos para a atualiza��o das taxas e mensagens do boleto.
	 * @param documentosIDs - String com os ids dos documentos separados por ','.
	 * @return
	 */
	public List<Documento> findDocumentoUpdateBoletoETaxa(String whereIn) {
		if(StringUtils.isBlank(whereIn)) return new ArrayList<Documento>();
		
		return querySined()
				.select("documento.cddocumento,documento.dtvencimento, taxa.cdtaxa, " +
						"taxaitem.dias, taxaitem.tipocalculodias, taxaitem.cdtaxaitem, taxaitem.gerarmensagem, taxaitem.percentual, taxaitem.valor, taxaitem.dtlimite, "+
						"tipotaxa.cdtipotaxa, tipotaxa.nome")
				.leftOuterJoin("documento.taxa taxa")
				.leftOuterJoin("taxa.listaTaxaitem taxaitem")
				.leftOuterJoin("taxaitem.tipotaxa tipotaxa")
				.whereIn("documento.cddocumento", whereIn)
				.list();
	}
	
	public List<Documento> findDocumentoByMovimentacao(String whereIn, Documentoclasse documentoclasse, Tipopagamento tipopagamento){
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		return querySined()
					.select("documento.cddocumento, documentoclasse.cddocumentoclasse, pessoa.cdpessoa ")
					.join("documento.documentoclasse documentoclasse")
					.leftOuterJoin("documento.pessoa pessoa")
					.where("documento.cddocumento in (select doc.cddocumento " +
										" from Movimentacaoorigem mo " +
										" join mo.movimentacao m " +
										" join mo.documento doc " +
										" where m.cdmovimentacao in ("+ whereIn+") )")
					.where("documentoclasse = ?", documentoclasse)
					.where("documento.tipopagamento = ?", tipopagamento)
					.list();
	}
	
	public Documento findEmpresaByDocumento(Integer cddocumento) {
		return query()
				.select("empresa.cdpessoa, empresa.textoautenticidadeboleto")
				.leftOuterJoin("documento.empresa empresa")
				.where("cddocumento = ?", cddocumento)
				.unique();
	}
	
	public List<Documento> findByDocumentoacao(Documentoacao documentoacao, Cliente cliente, Boolean atrasadas) {
		QueryBuilder<Documento> query = querySined()
											.select("documento.valor, documento.cddocumento, documento.dtemissao,documento.documentoclasse")
											.where("documento.documentoacao = ?", documentoacao)
											.where("documento.pessoa = ?", cliente);
		if(atrasadas){
			query.where("(CURRENT_DATE - documento.dtvencimento) > 1");
		}
		return query.list();
	}
	public List<Documento> buscarPorCentroCusto(boolean csv, Centrocusto centroCusto,Date dtInicio, Date dtFim, Empresa empresa) {
		return querySined()
				.select("documento.cddocumento,rateio.cdrateio")
				.leftOuterJoin("documento.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem item")
				.openParentheses()
					.where("documento.empresa = ? ", empresa)
						.or()
					.where("documento.empresa is null")
				.closeParentheses()
				.where("item.projeto is null", !csv)
				.where("item.centrocusto = ?", centroCusto)
				.where("documento.dtemissao >= ?", dtInicio)
				.where("documento.dtemissao <= ?", dtFim)
				.where("documento.documentoacao <> ?", Documentoacao.CANCELADA)
				.list();
	}
	
	/**
	 * Verifica se o documento passado por par�metro � de origem de uma negocia��o.
	 *
	 * @param documento
	 * @return
	 * @since 16/08/2019
	 * @author Rodrigo Freitas
	 */
	public boolean isOrigemNegociacao(Documento documento) {
		SinedUtil.markAsReader();
		return newQueryBuilderWithFrom(Long.class)
					.select("count(*)")
					.where("documento = ?", documento)
					.where("exists(select 1 from Documentonegociadoitem di where di.documento = documento)")
					.unique() > 0;
	}
	
	public List<Documento> findForCompensarSaldo(String whereIn) {
		if(StringUtils.isBlank(whereIn)){
			return new ArrayList<Documento>();
		}
		
		return querySined()
				.select("documento.cddocumento, documento.outrospagamento, documento.valor, documento.tipopagamento, " +
						"documentoclasse.cddocumentoclasse, documentoclasse.nome, " +
						"documentotipo.cddocumentotipo, documentotipo.nome, documentotipo.antecipacao, " +
						"documentoacao.cddocumentoacao, documentoacao.nome, " +
						"pessoa.cdpessoa, pessoa.nome, empresa.cdpessoa, " +
						"historicoantecipacao.cdHistoricoAntecipacao, historicoantecipacao.valor, " +
						"docAntecipacaoOrigem.cddocumento, classeOrigem.cddocumentoclasse, classeOrigem.nome, " +
						"docAntecipacaoReferencia.cddocumento, classeReferencia.cddocumentoclasse, classeReferencia.nome")
				.leftOuterJoin("documento.documentoacao documentoacao")
				.leftOuterJoin("documento.documentotipo documentotipo")
				.leftOuterJoin("documento.empresa empresa")
				.leftOuterJoin("documento.pessoa pessoa")
				.leftOuterJoin("documento.documentoclasse documentoclasse")
				.leftOuterJoin("documento.listaHistoricoAntecipacao historicoantecipacao")
				.leftOuterJoin("historicoantecipacao.documento docAntecipacaoOrigem")
				.leftOuterJoin("historicoantecipacao.documentoReferencia docAntecipacaoReferencia")
				.leftOuterJoin("docAntecipacaoOrigem.documentoclasse classeOrigem")
				.leftOuterJoin("docAntecipacaoReferencia.documentoclasse classeReferencia")
				.whereIn("documento.cddocumento", whereIn)
				.list();
	}
	
	public List<Documento> buscarDocumentosNaoCompensadosPorPessoa(Documento documento) {
		QueryBuilder<Documento> query = querySined()
			.select("documento.cddocumento, documento.valor, documentotipo.antecipacao, " + 
					"historicoantecipacao.cdHistoricoAntecipacao, historicoantecipacao.valor")
			.leftOuterJoin("documento.documentotipo documentotipo")
			.leftOuterJoin("documento.listaHistoricoAntecipacao historicoantecipacao")
			.where("documento.tipopagamento = ?", documento.getTipopagamento())
			.where("documento.documentoacao = ?", Documentoacao.BAIXADA)
			.where("documentotipo.antecipacao = true");
		
		if(documento.getPessoa() != null && documento.getPessoa().getCdpessoa() != null) {
			query.where("documento.pessoa = ?", documento.getPessoa());
		} else {
			query.where("documento.outrospagamento = ?", documento.getOutrospagamento());
		}
		
		return query.list();
	}
	
	public List<Documento> findWithDespesaColaborador(String whereIn) {
		if(StringUtils.isBlank(whereIn)){
			return new ArrayList<Documento>();
		}
		
		return querySined()
				.select("documento.cddocumento, documento.valor, " + 
						"documentoOrigem.cddocumentoorigem, " +
						"colaboradorDespesa.cdcolaboradordespesa, " +
						"documentoClasse.cddocumentoclasse")
				.join("documento.listaDocumentoOrigem documentoOrigem")
				.join("documentoOrigem.colaboradordespesa colaboradorDespesa")
				.join("documento.documentoclasse documentoClasse")
				.whereIn("documento.cddocumento", whereIn)
				.list();
	}
	
	public boolean existeContaPagarEntradaFiscal(String whereIn) {
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Documento.class)
				.join("documento.listaDocumentoOrigem listaDocumentoOrigem")
				.join("listaDocumentoOrigem.entregadocumento entregadocumento")
				.whereIn("entregadocumento.cdentregadocumento", whereIn)
				.where("documento.documentoacao <> ?", Documentoacao.CANCELADA);
		
		return query.unique() > 0;
	}
	
	public List<Documento> findForNotaPromissoria(String whereIn){
		return querySined()
				.setUseReadOnly(true)
				.select("documento.cddocumento, documento.numero, documento.dtvencimento, documento.dtvencimento, aux_documento.valoratual, documentoacao.cddocumentoacao, " +
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, listaTelefoneEmpresa.telefone, " +
						"listaEnderecoEmpresa.logradouro, listaEnderecoEmpresa.numero, listaEnderecoEmpresa.bairro, municipioEmpresa.nome, ufEmpresa.sigla, " +
						"cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj, listaTelefoneCliente.telefone, listaEnderecoCliente.logradouro, " +
						"listaEnderecoCliente.numero, listaEnderecoCliente.bairro, municipioCliente.nome, ufCliente.sigla, " +
						"vendaOrigem.cdvenda, vendaOrigem.identificador, pedidovendaOrigem.cdpedidovenda, pedidovendaOrigem.identificador, " +
						"documentoOrigem.numero, listaDocumentohistorico.cddocumentohistorico, documentoacaoHistorico.cddocumentoacao, " +
						"enderecoConta.logradouro, enderecoConta.numero, enderecoConta.bairro, municipioEnderecoConta.nome, ufEnderecoConta.sigla")
				.leftOuterJoin("documento.documentoacao documentoacao")
				.leftOuterJoin("documento.empresa empresa")
				.leftOuterJoin("empresa.listaTelefone listaTelefoneEmpresa")
				.leftOuterJoin("empresa.listaEndereco listaEnderecoEmpresa")
				.leftOuterJoin("listaEnderecoEmpresa.municipio municipioEmpresa")
				.leftOuterJoin("municipioEmpresa.uf ufEmpresa")
				.leftOuterJoin("documento.pessoa cliente")
				.leftOuterJoin("cliente.listaTelefone listaTelefoneCliente")
				.leftOuterJoin("cliente.listaEndereco listaEnderecoCliente")
				.leftOuterJoin("listaEnderecoCliente.municipio municipioCliente")
				.leftOuterJoin("municipioCliente.uf ufCliente")
				.leftOuterJoin("documento.listaDocumentoOrigem listaDocumentoOrigem")
				.leftOuterJoin("listaDocumentoOrigem.venda vendaOrigem")
				.leftOuterJoin("listaDocumentoOrigem.pedidovenda pedidovendaOrigem")
				.leftOuterJoin("listaDocumentoOrigem.documento documentoOrigem")
				.leftOuterJoin("documento.listaDocumentohistorico listaDocumentohistorico")
				.leftOuterJoin("listaDocumentohistorico.documentoacao documentoacaoHistorico")
				.leftOuterJoin("documento.aux_documento aux_documento")
				.leftOuterJoin("documento.endereco enderecoConta")
				.leftOuterJoin("enderecoConta.municipio municipioEnderecoConta")
				.leftOuterJoin("municipioEnderecoConta.uf ufEnderecoConta")
				.whereIn("documento.cddocumento", whereIn)
				.list();
	}
	
}