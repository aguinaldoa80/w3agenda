package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Arquivognre;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ArquivognreDAO extends GenericDAO<Arquivognre> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Arquivognre> query, FiltroListagem _filtro) {
		query.leftOuterJoinFetch("arquivognre.listaArquivognreitem listaArquivognreitem");
		query.leftOuterJoinFetch("arquivognre.arquivoxml arquivoxml");
		query.orderBy("arquivognre.cdarquivognre desc");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Arquivognre> query) {
		query.leftOuterJoinFetch("arquivognre.listaArquivognreitem listaArquivognreitem");
		query.leftOuterJoinFetch("listaArquivognreitem.gnre gnre");
		query.leftOuterJoinFetch("gnre.uf uf");
		query.leftOuterJoinFetch("arquivognre.arquivoxml arquivoxml");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaArquivognreitem");
	}
	
}
