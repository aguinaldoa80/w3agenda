package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.ArquivoIntegracaoBridgestone;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ArquivoIntegracaoBridgestoneFiltro;

public class ArquivoIntegracaoBridgestoneDAO extends GenericDAO<br.com.linkcom.sined.geral.bean.ArquivoIntegracaoBridgestone> {

	public List<ArquivoIntegracaoBridgestone> findForArquivoIntegracaoBridgestoneJob(){
		return query()
					.select("arquivoIntegracaoBridgestone.cdarquivointegracaobridgestone, arquivoIntegracaoBridgestone.tipo, arquivoIntegracaoBridgestone.dtCriacao, " +
							"nota.cdNota, nota.dtEmissao, nota.numero, nota.telefoneCliente, " +
							"empresa.cdpessoa, empresa.integracaobts, " +
							"venda.cdvenda, campoExtraPVT.cdcampoextrapedidovendatipo, campoExtraPVT.nome, campoExtraPVT.ordem, arquivonfnota.cdarquivonfnota, arquivonfnota.chaveacesso, prazopagamento.nome, " +
							"requisicao.cdrequisicao, material.cdmaterial, material.nome, materialgrupo.cdmaterialgrupo, material.valorvenda, fornecedor.cdpessoa, fornecedor.nome, material.produto, " +
							"notaServico.cdNota, notaServico.indicadortipopagamento, " +
							"cliente.cdpessoa, cliente.cpf, cliente.cnpj, cliente.nome, cliente.email, municipio.nome, municipio.cdmunicipio, uf.sigla, uf.cduf, materialRequisicao.valorunitario")
					.leftOuterJoin("arquivoIntegracaoBridgestone.nota nota")
					.leftOuterJoin("nota.empresa empresa")
					.leftOuterJoin("arquivoIntegracaoBridgestone.notaFiscalServico notaServico")
					.leftOuterJoin("nota.cliente cliente")
					.leftOuterJoin("nota.enderecoCliente endereco")
					.leftOuterJoin("endereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("nota.listaArquivonfnota arquivonfnota")
					.leftOuterJoin("nota.listaNotavenda listaNotavenda")
					.leftOuterJoin("nota.listaRequisicaonota listaRequisicaonota")
					.leftOuterJoin("listaRequisicaonota.requisicao requisicao")
					.leftOuterJoin("requisicao.listaMateriaisrequisicao materialRequisicao")
					.leftOuterJoin("materialRequisicao.material material")
					.leftOuterJoin("material.materialgrupo materialgrupo")
					.leftOuterJoin("material.listaFornecedor materialfornecedor WITH materialfornecedor.fabricante = TRUE")
					.leftOuterJoin("materialfornecedor.fornecedor fornecedor")
					.leftOuterJoin("listaNotavenda.venda venda")
					.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
					.leftOuterJoin("venda.prazopagamento prazopagamento")
					.leftOuterJoin("pedidovendatipo.listaCampoextrapedidovenda campoExtraPVT")
					.where("arquivoIntegracaoBridgestone.processada = ?", Boolean.FALSE)
//					.where("materialfornecedor.fabricante = ?", Boolean.TRUE)
					.list();
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<ArquivoIntegracaoBridgestone> query, FiltroListagem _filtro) {
		
		ArquivoIntegracaoBridgestoneFiltro filtro = (ArquivoIntegracaoBridgestoneFiltro) _filtro;
		
		query
			.select("arquivoIntegracaoBridgestone.cdarquivointegracaobridgestone, arquivoIntegracaoBridgestone.tipo, " +
					"arquivoIntegracaoBridgestone.processada, arquivoIntegracaoBridgestone.dtCriacao, arquivo.cdarquivo, arquivo.nome")
			.leftOuterJoin("arquivoIntegracaoBridgestone.arquivo arquivo")
			.where("arquivoIntegracaoBridgestone.tipo = ?", filtro.getTipo())
			.where("date(arquivoIntegracaoBridgestone.dtCriacao) = ?", filtro.getDtCriacao())
			.where("arquivoIntegracaoBridgestone.processada = ?", filtro.getProcessada())
			.list();
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		ArquivoDAO.getInstance().saveFile(save.getEntity(), "arquivo");
		super.updateSaveOrUpdate(save);
	}
	
	
	public List<ArquivoIntegracaoBridgestone> findForDownloadZip(String whereIn){
		if(StringUtils.isBlank(whereIn))
			throw new RuntimeException("O par�metro whereIn n�o pode estar vazio.");
		
		return query()
				.select("arquivoIntegracaoBridgestone.cdarquivointegracaobridgestone, arquivo.cdarquivo, arquivo.nome")
				.leftOuterJoin("arquivoIntegracaoBridgestone.arquivo arquivo")
				.whereIn("arquivoIntegracaoBridgestone.cdarquivointegracaobridgestone", whereIn)
				.list();
	}
	
}
