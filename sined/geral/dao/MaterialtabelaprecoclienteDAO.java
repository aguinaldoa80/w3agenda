package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Materialtabelapreco;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecocliente;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialtabelaprecoclienteDAO extends GenericDAO<Materialtabelaprecocliente> {

	public List<Materialtabelaprecocliente> findForAndroid(String whereIn) {
		return query()
				.select("materialtabelaprecocliente.cdmaterialtabelaprecocliente, " +
						"cliente.cdpessoa, materialtabelapreco.cdmaterialtabelapreco")
				.join("materialtabelaprecocliente.materialtabelapreco materialtabelapreco")
				.join("materialtabelaprecocliente.cliente cliente")
				.whereIn("materialtabelaprecocliente.cdmaterialtabelaprecocliente", whereIn)
				.list();
	}
	

	public List<Materialtabelaprecocliente> findByMaterialtabelapreco(Materialtabelapreco materialtabelapreco){
		return query()
			.select("materialtabelaprecocliente.cdmaterialtabelaprecocliente, " +
					"cliente.cdpessoa, materialtabelapreco.cdmaterialtabelapreco")
			.join("materialtabelaprecocliente.materialtabelapreco materialtabelapreco")
			.join("materialtabelaprecocliente.cliente cliente")
			.where("materialtabelapreco = ?", materialtabelapreco)
			.list();
	}
}
