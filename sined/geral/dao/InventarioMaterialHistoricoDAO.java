package br.com.linkcom.sined.geral.dao;


import java.util.List;

import br.com.linkcom.sined.geral.bean.InventarioMaterialHistorico;
import br.com.linkcom.sined.geral.bean.Inventariomaterial;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class InventarioMaterialHistoricoDAO extends GenericDAO<InventarioMaterialHistorico>{
	
	
	public List< InventarioMaterialHistorico> findByInventarioMaterialHistorico (Inventariomaterial inventarioMaterial){
		return query()
				.select("inventarioMaterialHistorico.cdInventarioMaterialHistorico,inventarioMaterialHistorico.acao,inventarioMaterialHistorico.cdusuarioaltera,inventarioMaterialHistorico.dtaltera,inventarioMaterialHistorico.observacao")
				.leftOuterJoin("inventarioMaterialHistorico.inventarioMaterial inventariomaterial")
				.where("inventariomaterial=?",inventarioMaterial)
				.orderBy("inventarioMaterialHistorico.cdInventarioMaterialHistorico DESC")
				.list();
	}
}
