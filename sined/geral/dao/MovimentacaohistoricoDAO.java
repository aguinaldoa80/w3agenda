package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaohistorico;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MovimentacaohistoricoDAO extends GenericDAO<Movimentacaohistorico>{

	/**
	 * M�todo para obter o historico de uma movimentacao financeira.
	 * 
	 * @param movimentacao
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Movimentacaohistorico> findByMovimentacao(Movimentacao movimentacao){
		if(movimentacao == null || movimentacao.getCdmovimentacao() == null){
			throw new SinedException("Par�metros inv�lidos. Movimentacao ou cdmovimentacao n�o podem ser null.");
		}
		return
			query()
			.select("movimentacaohistorico.cdmovimentacaohistorico, movimentacaohistorico.cdusuarioaltera, movimentacaohistorico.dtaltera," +
					"movimentacaohistorico.observacao,movimentacaoacao.cdmovimentacaoacao,movimentacaoacao.nome,movimentacao.cdmovimentacao")
			.leftOuterJoin("movimentacaohistorico.movimentacao movimentacao")
			.leftOuterJoin("movimentacaohistorico.movimentacaoacao movimentacaoacao")
			.where("movimentacao = ?", movimentacao)
			.orderBy("movimentacaohistorico.dtaltera desc")
			.list();
	}
}
