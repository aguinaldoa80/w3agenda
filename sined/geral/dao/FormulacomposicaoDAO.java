package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Formulacomposicao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("formulacomposicao.nome")
public class FormulacomposicaoDAO extends GenericDAO<Formulacomposicao>{

}
