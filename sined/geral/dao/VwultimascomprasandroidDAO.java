package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.sined.geral.bean.view.Vwultimascomprasandroid;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VwultimascomprasandroidDAO extends GenericDAO<Vwultimascomprasandroid> {
	
	/**
	* M�todo que busca as ultimas compras para o android
	*
	* @param dataUltimaAtualizacao
	* @return
	* @since 28/10/2015
	* @author Luiz Fernando
	*/
	public List<Vwultimascomprasandroid> findForAndroid(Timestamp dataUltimaAtualizacao) {
		return query()
			.where("vwultimascomprasandroid.maxhistorico >= ?", dataUltimaAtualizacao)
			.list();
	}
}
