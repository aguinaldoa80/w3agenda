package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculomodelo;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVeiculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocontroleveiculo;
import br.com.linkcom.sined.geral.bean.view.Vwveiculoseminspecaovencido3;
import br.com.linkcom.sined.geral.bean.view.Vwveiculosseminspecao3;
import br.com.linkcom.sined.geral.bean.view.Vwveiculosvencidos;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.process.filter.AgendarVeiculoInspecaoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.process.filter.BuscarVeiculosSemInspecaoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.FormularioinspecaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;


public class VeiculoDAO extends GenericDAO<Veiculo> {

	@Override
	public void updateListagemQuery(QueryBuilder<Veiculo> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		VeiculoFiltro filtro = (VeiculoFiltro) _filtro;
		query
			.select("distinct veiculo.cdveiculo, veiculo.placa, veiculocor.descricao, veiculo.dtentrada, colaborador.nome, veiculomodelo.nome," +
					"patrimonioitem.plaqueta, bempatrimonio.nome, patrimonioitem.ativo, " +
					"aux_veiculo.situacao, empresa.nome, empresa.razaosocial")
			.join("veiculo.aux_veiculo aux_veiculo")
			.join("veiculo.patrimonioitem patrimonioitem")
			.join("patrimonioitem.bempatrimonio bempatrimonio")
			.leftOuterJoin("veiculo.colaborador colaborador")
			.leftOuterJoin("veiculo.veiculomodelo veiculomodelo")
			.leftOuterJoin("veiculo.veiculocor veiculocor")
			.leftOuterJoin("veiculo.empresa empresa")
			.leftOuterJoin("veiculo.seguradora seguradora")
			.leftOuterJoin("veiculo.listaVeiculouso listaVeiculouso")
			.leftOuterJoin("listaVeiculouso.projeto projeto")
			.where("veiculomodelo = ?", filtro.getVeiculomodelo())
			.where("empresa = ?", filtro.getEmpresa())
			.where("seguradora = ?", filtro.getFornecedor())
			.where("veiculo.dtvencimentoseguro >= ?", filtro.getDtvencimentoinicio())
			.where("veiculo.dtvencimentoseguro <= ?", filtro.getDtvencimentofim())
			.whereLikeIgnoreAll("veiculo.placa", filtro.getPlaca())
			.where("projeto=?", filtro.getProjeto());
		
		if(filtro.getSeguro() != null && !"".equals(filtro.getSeguro())){
			if("Vencidos".equals(filtro.getSeguro())){
				query.where("veiculo.dtvencimentoseguro < ?", new Date(System.currentTimeMillis()));
			}else if("A Vencer".equals(filtro.getSeguro())){
				query
					.where("(veiculo.dtvencimentoseguro - ?) < 60", new Date(System.currentTimeMillis()))
					.where("(veiculo.dtvencimentoseguro - ?) >= 0", new Date(System.currentTimeMillis()));;
			}
		}
		
		List<SituacaoVeiculo> listaSituacao = filtro.getListaveiculo();
		if (listaSituacao != null && !listaSituacao.isEmpty()) {
			String list = "";
			for (int i = 0; i < listaSituacao.size(); i++) {
				Object o = listaSituacao.get(i);
				SituacaoVeiculo situacaoContrato = null;
				if (o instanceof String) {
					situacaoContrato = SituacaoVeiculo.valueOf((String)o);
				}else{
					situacaoContrato = (SituacaoVeiculo)o;
				}
				
				list += situacaoContrato.getValue() + ",";
			}
			query.whereIn("aux_veiculo.situacao", list.substring(0, (list.length()-1) ));
		}
		
		query.ignoreJoin("listaVeiculouso", "projeto");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Veiculo> query) {
		query
			.select("veiculo.cdveiculo, veiculo.placa, veiculo.tipocontroleveiculo, veiculo.kminicial, veiculo.horimetroinicial, " +
					"veiculo.dtentrada, veiculo.anofabricacao, veiculo.anomodelo, " +
					"veiculo.chassi, veiculo.renavam, veiculo.anotacoes, veiculo.cdusuarioaltera, veiculo.dtaltera, " +
					"empresa.cdpessoa, veiculocor.cdveiculocor, veiculocombustivel.cdveiculocombustivel, " +
					"fornecedor.cdpessoa, patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, bempatrimonio.nome, listaveiculoacessorio.cdveiculoacessorio, " +
					"listaveiculoacessorio.quantidade, listaveiculoacessorio.acessorio, aux_veiculo.cdveiculo, aux_veiculo.situacao, veiculo.prefixo, veiculo.prazopreventiva, " +
					"colaborador.cdpessoa, colaborador.nome, veiculomodelo.cdveiculomodelo, escala.cdescala, seguradora.cdpessoa, " +
					"seguradora.nome, veiculo.dtvencimentoseguro, uf.cduf, municipio.cdmunicipio ")
			.join("veiculo.patrimonioitem patrimonioitem")
			.join("veiculo.aux_veiculo aux_veiculo")
			.join("patrimonioitem.bempatrimonio bempatrimonio")
			.leftOuterJoin("veiculo.colaborador colaborador")
			.leftOuterJoin("veiculo.veiculomodelo veiculomodelo")
			.leftOuterJoin("veiculo.veiculocor veiculocor")
			.leftOuterJoin("veiculo.veiculocombustivel veiculocombustivel")
			.leftOuterJoin("veiculo.empresa empresa")
			.leftOuterJoin("veiculo.fornecedor fornecedor")
			.leftOuterJoin("veiculo.seguradora seguradora")
			.leftOuterJoin("veiculo.escala escala")
			.leftOuterJoin("veiculo.uf uf")
			.leftOuterJoin("veiculo.municipio municipio")
			.leftOuterJoin("veiculo.listaveiculoacessorio listaveiculoacessorio");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaveiculoacessorio");
	}
	
	public void atualizaKmInicial(Veiculo veiculo){
		String sql = "update veiculo set veiculo.kminicial = ?, veiculo.cdusuarioaltera = ?, " +
				"veiculo.dtaltera = ? WHERE veiculo.cdveiculo = ?";
		getJdbcTemplate().update(sql, new Object[]{veiculo.getKminicial(), veiculo.getCdusuarioaltera(), new Timestamp(System.currentTimeMillis()), veiculo.getCdveiculo()});
	}
	
	@Override
	public List<Veiculo> findForCombo(String... extraFields) {
		QueryBuilder<Veiculo> query = query();
		
		query
			.select("veiculo.cdveiculo, veiculo.placa, bempatrimonio.nome")
			.from(Veiculo.class)
			.join("veiculo.patrimonioitem patrimonioitem")
			.join("patrimonioitem.bempatrimonio bempatrimonio")
			.orderBy("bempatrimonio.nome");
		
		return query.list();
	}
	
	public List<Veiculo> findForComboFlex(String... extraFields) {
		QueryBuilder<Veiculo> query = query();
		
		query
		.select("veiculo.cdveiculo, veiculo.placa, bempatrimonio.nome, empresa.cdpessoa")
		.from(Veiculo.class)
		.join("veiculo.patrimonioitem patrimonioitem")
		.join("veiculo.empresa empresa")
		.join("patrimonioitem.bempatrimonio bempatrimonio")
		.orderBy("bempatrimonio.nome");
		
		return query.list();
	}
	
	
	/**
	 * Procedure que atualiza a tabela auxiliar de veiculo. 
	 *
	 * @author Rodrigo Freitas
	 */
	public void updateAux() {
		getJdbcTemplate().execute("SELECT ATUALIZA_VEICULO()");
	}
	
	/**
	 * Carrega o ve�culo com os dados da view Vwveiculo
	 * 
	 * @param veiculo
	 * @return Veiculo
	 * @throws W3AutoException
	 * @author Rafael Odon
	 */
	public Veiculo loadWithVwveiculo(Veiculo veiculo){
		if(veiculo == null || veiculo.getCdveiculo() == null){
			throw new SinedException("Ve�culo n�o pode ser nulo.");
		}
		return query()
		.joinFetch("veiculo.vwveiculo vwveiculo")	
		.where("veiculo = ?", veiculo)			
		.unique();	
	}
	
	/**
	 * Procura ve�culo pela sua placa
	 * @author Rafael Odon
	 * @param veiculo
	 * @return Veiculo
	 */
	public Veiculo findByPlaca(Veiculo veiculo){
		if(veiculo == null){
			throw new SinedException("Par�metro incorreto. Veiculo n�o pode ser null.");
		}
		if(veiculo.getPlaca() == null){
			throw new SinedException("Placa n�o pode ser nula.");
		}
		return query()
			.select("veiculo.cdveiculo, veiculo.placa, veiculomodelo.cdveiculomodelo, categoriaveiculo.cdcategoriaveiculo")
			.leftOuterJoin("veiculo.veiculomodelo veiculomodelo")
			.leftOuterJoin("veiculomodelo.categoriaveiculo categoriaveiculo")
			.where("LOWER(veiculo.placa) = LOWER(?)",veiculo.getPlaca())
			.unique();
	}
	
	/**
	 * Procura ve�culo pela sua placa
	 * @author Rafael Odon
	 * @param veiculo
	 * @return Veiculo
	 */
	public Veiculo findVeiculo(Veiculo veiculo){
		if(veiculo == null || veiculo.getCdveiculo() == null){
			throw new SinedException("Par�metro incorreto. Veiculo n�o pode ser null.");
		}
		return query()
		.select("veiculo.cdveiculo, veiculo.placa, veiculo.tipocontroleveiculo, veiculomodelo.cdveiculomodelo, categoriaveiculo.cdcategoriaveiculo, " +
				"bempatrimonio.nome")
		.leftOuterJoin("veiculo.patrimonioitem patrimonioitem")
		.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
		.leftOuterJoin("veiculo.veiculomodelo veiculomodelo")
		.leftOuterJoin("veiculomodelo.categoriaveiculo categoriaveiculo")
		.where("veiculo.cdveiculo = ?",veiculo.getCdveiculo())
		.unique();
	}
	
	/**
	 * Carrega o ve�culo com os dados para o agendamento
	 * 
	 * @param veiculo
	 * @return Veiculo
	 * @author Rafael Odon
	 */
	public Veiculo loadForAgendamento(Veiculo veiculo){
		if(veiculo == null || veiculo.getCdveiculo() == null){
			throw new SinedException("Ve�culo n�o pode ser nulo.");
		}
		return query()
		.select("veiculo.cdveiculo, veiculo.placa, veiculo.tipocontroleveiculo, vwveiculo.kmatual, vwveiculo.horimetroatual, veiculomodelo.nome, " +
				"colaborador.nome, colaborador.cdpessoa, colaborador.email")
		.leftOuterJoin("veiculo.veiculomodelo veiculomodelo")
		.leftOuterJoin("veiculo.colaborador colaborador")
		.leftOuterJoin("veiculo.vwveiculo vwveiculo")	
		.where("veiculo = ?", veiculo)			
		.unique();	
	}
	
	/**
	 * <b>M�todo respons�vel por carregar um ou mais veiculos e ordenar os itens de acordo com filtro  <br>
	 * @author Ramon Barzil
	 * @param filtro, order
	 * @return List<Veiculo>
	 */
	public List<Veiculo> findForVeiculo(FormularioinspecaoFiltro filtro, String order){
		return query()
		.select("veiculo.cdveiculo,veiculo.placa,veiculo.prefixo,colaborador.nome,colaborador.cdpessoa,"+
				"veiculomodelo.nome, veiculomodelo.cdveiculomodelo,veiculotipo.descricao,vwveiculo.kmatual")
		.leftOuterJoin("veiculo.colaborador colaborador")
		.leftOuterJoin("veiculo.veiculomodelo veiculomodelo")		
		.leftOuterJoin("veiculomodelo.veiculotipo veiculotipo")
		.leftOuterJoin("veiculo.vwveiculo vwveiculo")	
		.where("colaborador =?",filtro.getCooperado())
		.where("veiculo =?",filtro.getVeiculo())
		.where("veiculomodelo=?",filtro.getModelo())
		.where("veiculotipo =?", filtro.getTipo())
		.orderBy(order)
		.list();
	}
	

	/**
	 * <b>Lista os ve�culos n�o possuem inspe��o no periodo estabelecido como parametro 
	 * DIA_SEM_INSPECAO (ver view no banco)</b>
	 * 
	 * @return List<Vwveiculosseminspecao2>
	 * @throws W3AutoException
	 * @param BuscarVeiculosSemInspecaoFiltro
	 * @author Jo�o Paulo Zica
	 */
	@SuppressWarnings("unchecked")
	public List<Vwveiculosseminspecao3> findVeiculosAgendados(BuscarVeiculosSemInspecaoFiltro filtro){
		if (filtro == null){
			throw new SinedException("Filtro n�o pode ser nulo.");
		}
		
//		return newQueryBuilder(Vwveiculosseminspecao3.class)	
//			.select("veiculo.cdveiculo, veiculo.prefixo, veiculo.placa, veiculomodelo.nome, colaborador.nome, " +
//			"vwveiculosseminspecao3.dtagendamento, vwveiculosseminspecao3.dtinspecao, vwveiculosseminspecao3.tipoinspecao")
//			.from(Vwveiculosseminspecao3.class)
//			.join("vwveiculosseminspecao3.veiculo veiculo")
//			.leftOuterJoin("veiculo.veiculomodelo veiculomodelo")
//			.leftOuterJoin("veiculo.colaborador colaborador")
//			.whereWhen("vwveiculosseminspecao3.dtagendamento is null", filtro.getStatus() != null && !filtro.getStatus())
//			.whereWhen("vwveiculosseminspecao3.dtagendamento is not null", filtro.getStatus() != null && filtro.getStatus())
//			.whereWhen("(vwveiculosseminspecao3.tipoinspecao is true or vwveiculosseminspecao3.tipoinspecao is null)", filtro.getInspecao() != null && filtro.getInspecao())
//			.whereWhen("(vwveiculosseminspecao3.tipoinspecao is false or vwveiculosseminspecao3.tipoinspecao is null)", filtro.getInspecao() != null && !filtro.getInspecao())
////			.whereWhen("vwveiculosseminspecao3.tipoinspecao is not null or vwveiculosseminspecao3.tipoinspecao is null", filtro.getInspecao() == null )
////			.whereWhen("vwveiculosseminspecao3.tipoinspecao is not null ", filtro.getInspecao() == null && filtro.getStatus() == null)
//			.orderBy(filtro.getOrdenacao())
//			.list();
		
		
		String sql = 	"select v.cdveiculo, v.prefixo, v.placa, vm.nome as modelo_nome, " +
						"p.nome as colaborador_nome, vw3.dtagendamento, " +
						"vw3.dtinspecao, vw3.tipoinspecao, vw3.cdveiculo, vm.cdveiculomodelo, c.cdpessoa " +
						
						"from Vwveiculosseminspecao3 vw3  " +
						
						"inner join Veiculo v on vw3.cdveiculo = v.cdveiculo  " +
						"left outer join Veiculomodelo vm on v.cdveiculomodelo = vm.cdveiculomodelo  " +
						"left outer join Colaborador c on v.cdcolaborador = c.cdpessoa  " +
						"left outer join Pessoa p on c.cdpessoa = p.cdpessoa " +
						"where true = true ";
		
		if(filtro.getStatus() != null && !filtro.getStatus()){
			sql += "and vw3.dtagendamento is null ";
		} else if(filtro.getStatus() != null && filtro.getStatus()){
			sql += "and vw3.dtagendamento is not null ";
		}
		
		if(filtro.getInspecao() != null && filtro.getInspecao()){
			sql += "and (vw3.tipoinspecao = TRUE or vw3.tipoinspecao is null) ";
		} else if(filtro.getInspecao() != null && !filtro.getInspecao()){
			sql += "and (vw3.tipoinspecao = FALSE or vw3.tipoinspecao is null) ";
		}
		
		String order = filtro.getOrdenacao();
		if(order != null && !order.trim().equals("")){
			sql += "order by " + order;
		}

		SinedUtil.markAsReader();
		List<Vwveiculosseminspecao3> listaUpdate = getJdbcTemplate().query(sql, 
			new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					
					Veiculomodelo veiculomodelo = new Veiculomodelo();
					veiculomodelo.setCdveiculomodelo(rs.getInt("cdveiculomodelo"));
					veiculomodelo.setNome(rs.getString("modelo_nome"));
					
					Colaborador colaborador = new Colaborador();
					colaborador.setNome(rs.getString("colaborador_nome"));
					colaborador.setCdpessoa(rs.getInt("cdpessoa"));
					
					Veiculo veiculo = new Veiculo();
					veiculo.setCdveiculo(rs.getInt("cdveiculo"));
					veiculo.setPrefixo(rs.getString("prefixo"));
					veiculo.setPlaca(rs.getString("placa"));
					veiculo.setVeiculomodelo(veiculomodelo);
					veiculo.setColaborador(colaborador);
					
					Boolean tipoinspecao = rs.getBoolean("tipoinspecao");
					
					Vwveiculosseminspecao3 vw3 = new Vwveiculosseminspecao3();
					vw3.setCdveiculo(veiculo.getCdveiculo());
					vw3.setVeiculo(veiculo);
					vw3.setDtagendamento(rs.getDate("dtagendamento"));
					vw3.setDtinspecao(rs.getDate("dtinspecao"));
					vw3.setTipoinspecao(tipoinspecao);
					
					return vw3;
				}
	
			});
		
		
		return listaUpdate;
	}
	
	/**
	 * <b>Lista os ve�culos n�o possuem inspe��o no periodo estabelecido por ve�culo</b>
	 * 
	 * @return List<Veiculo>
	 * @throws W3AutoException
	 * @author Rafael Odon, Jo�o Paulo Zica
	 */
	public List<Vwveiculoseminspecaovencido3> findForAgendamento(AgendarVeiculoInspecaoFiltro filtro){
		return newQueryBuilder(Vwveiculoseminspecaovencido3.class)
		.from(Vwveiculoseminspecaovencido3.class)
		.select("veiculo.cdveiculo, " +
			"veiculo.prefixo, veiculo.placa, veiculomodelo.nome, colaborador.nome, vwveiculoseminspecaovencido3.dtinspecao")
		.join("vwveiculoseminspecaovencido3.veiculo veiculo")
		.join("veiculo.veiculomodelo veiculomodelo")
		.join("veiculo.colaborador colaborador")
		.orderBy(filtro.getOrdenacao())
		.list();	
	}
	
	/**
	 * Lista os ve�culos que posseum items de inspe��o
	 * vencidos atrav�s da view Vwitemvencido
	 * 
	 * @return Veiculo
	 * @throws W3AutoException
	 * @author Rafael Odon
	 */
	public List<Vwveiculosvencidos> findVencidos(){
		return newQueryBuilder(Vwveiculosvencidos.class)
					.from(Vwveiculosvencidos.class)
					.select("veiculo.cdveiculo, vwveiculosvencidos.tipovencimento, " +
					"veiculo.prefixo, veiculo.placa, veiculomodelo.nome, colaborador.nome")
					.join("vwveiculosvencidos.veiculo veiculo")
					.join("veiculo.veiculomodelo veiculomodelo")
					.join("veiculo.colaborador colaborador")
					.list();									
	}	
	
	/**
	 * Lista os itens vencidos do ve�culo
	 * 
	 * @param veiculo
	 * @return Veiculo
	 * @throws W3AutoException
	 * @author Rafael Odon
	 */
	public Veiculo findComItensVencidos(Veiculo veiculo){
		if(veiculo == null || veiculo.getCdveiculo() == null){
			throw new SinedException("Ve�culo n�o pode ser nulo.");
		}
		return query()
		.select("veiculo.cdveiculo, veiculo.placa, veiculo.prefixo, veiculomodelo.nome, colaborador.nome," +
				"vwitemvencido.cditeminspecao, vwitemvencido.tipovencimento, inspecaoitem.nome")
		.leftOuterJoin("veiculo.veiculomodelo veiculomodelo")
		.leftOuterJoin("veiculo.colaborador colaborador")
		.leftOuterJoin("veiculo.vwitemvencido vwitemvencido")
		.leftOuterJoin("vwitemvencido.inspecaoitem inspecaoitem")
		//.where("vwitemvencido.tipovencimento <> 4")
		.where("veiculo = ?",veiculo)
		.unique();									
	}

	/**
	 * M�todo que retorna a escala do ve�culo caso haja
	 * 
	 * @param veiculo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Veiculo getEscalaVeiculo(Veiculo veiculo) {
		if(veiculo == null || veiculo.getCdveiculo() == null){
			throw new SinedException("Ve�culo n�o pode ser nulo.");
		}
		return query()
			.select("veiculo.cdveiculo, escala.nome")
			.join("veiculo.escala escala")
			.where("veiculo = ?", veiculo)
			.unique();
	}

	/**
	 * M�todo que busca os dados da listagem para o relat�rio
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Veiculo> findForCSV(VeiculoFiltro filtro) {
		QueryBuilder<Veiculo> query = querySined();
		this.updateListagemQuery(query, filtro);
		
		query
			.select("veiculo.cdveiculo, veiculo.placa, veiculocor.descricao, veiculo.dtentrada, colaborador.nome, veiculomodelo.nome, " +
					"patrimonioitem.plaqueta, bempatrimonio.nome, patrimonioitem.ativo, fornecedor.nome, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
					"aux_veiculo.situacao, veiculo.kminicial, veiculomodelo.nome, veiculocombustivel.descricao, veiculo.renavam, veiculo.prefixo, " +
					"veiculo.anofabricacao, veiculo.anomodelo, veiculo.chassi, veiculo.prazopreventiva, escala.nome, veiculo.anotacoes ")
			.leftOuterJoin("veiculo.fornecedor fornecedor")
			.leftOuterJoin("veiculo.escala escala")
			.leftOuterJoin("veiculo.veiculocombustivel veiculocombustivel");
		
		return query.list();
	}

	/**
	 * M�todo que busca os dados do ve�culo para autocomplete
	 *
	 * @param param
	 * @return List<Veiculo>
	 * @author Jo�o Vitor
	 */
	public List<Veiculo> findForAutocomplete(String param) {
		return query()
		.select("veiculo.cdveiculo, veiculo.placa, bempatrimonio.nome")
		.from(Veiculo.class)
		.join("veiculo.patrimonioitem patrimonioitem")
		.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
		.openParentheses()
			.whereLikeIgnoreAll("bempatrimonio.nome", param)
			.or()
			.whereLikeIgnoreAll("veiculo.placa", param)
		.closeParentheses()
		.orderBy("bempatrimonio.nome, veiculo.placa")
		.list();
	}
	/**
	 *  M�todo que retorna lista de ve�culos para valida��o de vencimento dos itens de categoria
	 *
	 * @return List<Veiculo>
	 * @author Jo�o Vitor
	 */
	public List<Veiculo> findVeiculoListForValidaItemCategoriaVencido(){
		return query()
		.select("veiculo.cdveiculo, veiculo.tipocontroleveiculo, veiculomodelo.categoriaveiculo, veiculomodelo.cdveiculomodelo, veiculo.prefixo, veiculo.placa, veiculomodelo.nome, " +
				"colaborador.nome, listaVeiculoKm.kmnovo, listaVeiculoKm.cdveiculokm, listaVeiculoKm.dtentrada, " +
				"listaVeiculohorimetro.horimetronovo, listaVeiculohorimetro.cdveiculohorimetro, listaVeiculohorimetro.dtentrada")
		.join("veiculo.veiculomodelo veiculomodelo")
		.join("veiculo.colaborador colaborador")
		.leftOuterJoin("veiculo.listaVeiculoKm listaVeiculoKm")
		.leftOuterJoin("veiculo.listaVeiculohorimetro listaVeiculohorimetro")
		.openParentheses()
			.openParentheses()
				.where("veiculo.tipocontroleveiculo = ?", Tipocontroleveiculo.HODOMETRO)
				.openParentheses()
					.where("listaVeiculoKm.cdveiculokm IS NULL")
					.or()
					.where("listaVeiculoKm.cdveiculokm = (SELECT MAX(v.cdveiculokm) FROM Veiculokm v WHERE v.veiculo = veiculo)")
				.closeParentheses()
			.closeParentheses()
			.or()
			.openParentheses()
				.where("veiculo.tipocontroleveiculo = ?", Tipocontroleveiculo.HORIMETRO)
				.openParentheses()
					.where("listaVeiculohorimetro.cdveiculohorimetro IS NULL")
					.or()
					.where("listaVeiculohorimetro.cdveiculohorimetro = (SELECT MAX(v.cdveiculohorimetro) FROM Veiculohorimetro v WHERE v.veiculo = veiculo)")
				.closeParentheses()
			.closeParentheses()
		.closeParentheses()
		.list();
	}
	
	/**
	 *  M�todo que retorna ve�culo para valida��o de vencimento dos itens de categoria
	 *
	 * @param veiculo
	 * @return Veiculo
	 * @author Jo�o Vitor
	 */
	public Veiculo findVeiculoForValidaItemCategoriaVencido(Veiculo veiculo){
		return query()
		.select("veiculo.cdveiculo, veiculomodelo.categoriaveiculo, veiculomodelo.cdveiculomodelo, veiculo.prefixo, veiculo.placa, veiculomodelo.nome, " +
				"colaborador.nome, listaVeiculoKm.kmnovo, listaVeiculoKm.cdveiculokm, listaVeiculoKm.dtentrada, " +
				"listaVeiculohorimetro.horimetronovo, listaVeiculohorimetro.cdveiculohorimetro, listaVeiculohorimetro.dtentrada ")
		.leftOuterJoin("veiculo.veiculomodelo veiculomodelo")
		.join("veiculo.colaborador colaborador")
		.leftOuterJoin("veiculo.listaVeiculoKm listaVeiculoKm")
		.leftOuterJoin("veiculo.listaVeiculohorimetro listaVeiculohorimetro")
		.openParentheses()
			.openParentheses()
				.where("veiculo.tipocontroleveiculo = ?", Tipocontroleveiculo.HODOMETRO)
				.openParentheses()
					.where("listaVeiculoKm.cdveiculokm IS NULL")
					.or()
					.where("listaVeiculoKm.cdveiculokm = (SELECT MAX(v.cdveiculokm) FROM Veiculokm v WHERE v.veiculo = veiculo)")
				.closeParentheses()
			.closeParentheses()
			.or()
			.openParentheses()
				.where("veiculo.tipocontroleveiculo = ?", Tipocontroleveiculo.HORIMETRO)
				.openParentheses()
					.where("listaVeiculohorimetro.cdveiculohorimetro IS NULL")
					.or()
					.where("listaVeiculohorimetro.cdveiculohorimetro = (SELECT MAX(v.cdveiculohorimetro) FROM Veiculohorimetro v WHERE v.veiculo = veiculo)")
				.closeParentheses()
			.closeParentheses()
		.closeParentheses()
		.where("veiculo = ?", veiculo)
		.unique();
	}
	
	/**
	 * M�todo que busca os implementos para o autocomplete no detalhe de veiculoimplemento na tela de entrada de veiculouso
	 * 
	 * @param param
	 * @return
	 * @since 30/06/2017
	 * @author Rafael Salvio
	 */
	public List<Veiculo> findImplementosForAutocomplete(String param){
		return query()
				.select("veiculo.cdveiculo, veiculo.placa, bempatrimonio.nome")
				.from(Veiculo.class)
				.join("veiculo.veiculomodelo veiculomodelo")
				.join("veiculomodelo.veiculotipo veiculotipo")
				.join("veiculo.patrimonioitem patrimonioitem")
				.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
				.where("veiculotipo.implemento is true")
				.openParentheses()
					.whereLikeIgnoreAll("bempatrimonio.nome", param)
					.or()
					.whereLikeIgnoreAll("veiculo.placa", param)
				.closeParentheses()
				.orderBy("bempatrimonio.nome, veiculo.placa")
				.list();
	}

	@Override
	public ListagemResult<Veiculo> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Veiculo> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("veiculo.cdveiculo");
		
		return new ListagemResult<Veiculo>(query, filtro);
	}
}
