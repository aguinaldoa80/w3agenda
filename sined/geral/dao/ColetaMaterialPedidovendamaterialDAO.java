package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.ColetaMaterialPedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColetaMaterialPedidovendamaterialDAO extends GenericDAO<ColetaMaterialPedidovendamaterial> {

	public List<ColetaMaterialPedidovendamaterial> findNaoRecusados(ColetaMaterial coletaMaterial){
		return querySined()
				.setUseReadOnly(false)
				.select("coletaMaterialPedidovendamaterial.cdColetaMaterialPedidovendamaterial, coletaMaterialPedidovendamaterial.producaoRecusada")
				.join("coletaMaterialPedidovendamaterial.pedidovendamaterial pedidovendamaterial")
				.join("coletaMaterialPedidovendamaterial.coletaMaterial coletaMaterial")
				.where("coletaMaterial = ?", coletaMaterial)
				.where("coalesce(coletaMaterialPedidovendamaterial.producaoRecusada, false) <> true")
				.list();
	}
	
	public void updateRecusado(ColetaMaterial coletaMaterial, Pedidovendamaterial pedidoVendaMaterial){
		getHibernateTemplate().bulkUpdate("update ColetaMaterialPedidovendamaterial cmpv set cmpv.producaoRecusada = true where cmpv.coletaMaterial = ? and cmpv.pedidovendamaterial = ?", new Object[]{coletaMaterial, pedidoVendaMaterial});
	}
}
