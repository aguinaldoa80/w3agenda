package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cfopescopo;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Naturezaoperacaocfop;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NaturezaoperacaocfopDAO extends GenericDAO<Naturezaoperacaocfop>{
	
	/**
	 * 
	 * @param naturezaoperacao
	 * @param cfopescopo
	 * @author Thiago Clemente
	 * 
	 */
	public List<Naturezaoperacaocfop> find(Naturezaoperacao naturezaoperacao, Cfopescopo cfopescopo){
		return query()
				.select("naturezaoperacaocfop.cdnaturezaoperacaocfop, naturezaoperacao.cdnaturezaoperacao, cfop.cdcfop, cfop.codigo," +
						"cfop.descricaoresumida, cfopescopo.cdcfopescopo, cfopescopo.descricao")
				.join("naturezaoperacaocfop.naturezaoperacao naturezaoperacao")
				.join("naturezaoperacaocfop.cfop cfop")
				.join("cfop.cfopescopo cfopescopo")
				.where("naturezaoperacao=?", naturezaoperacao)
				.where("cfopescopo=?", cfopescopo)
				.list();
	}
	
}
