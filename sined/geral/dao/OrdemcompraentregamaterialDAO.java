package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Ordemcompraentregamaterial;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OrdemcompraentregamaterialDAO extends GenericDAO<Ordemcompraentregamaterial>{

	
	/**
	 * Busca o relacionamento de entregamaterial com ordemcompramaterial a partir do item da ordem de compra.
	 *
	 * @param ordemcompramaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/12/2012
	 */
	public List<Ordemcompraentregamaterial> findByOrdemcompramaterial(Ordemcompramaterial ordemcompramaterial) {
		if(ordemcompramaterial == null || ordemcompramaterial.getCdordemcompramaterial() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("entregamaterial.cdentregamaterial, materialclasse.cdmaterialclasse, materialclasse.nome")
					.join("ordemcompraentregamaterial.ordemcompramaterial ordemcompramaterial")
					.join("ordemcompraentregamaterial.entregamaterial entregamaterial")
					.join("entregamaterial.materialclasse materialclasse")
					.where("ordemcompramaterial = ?", ordemcompramaterial)
					.list();
	}

}
