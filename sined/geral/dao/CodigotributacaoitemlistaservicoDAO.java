package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Codigotributacaoitemlistaservico;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CodigotributacaoitemlistaservicoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CodigotributacaoitemlistaservicoDAO extends GenericDAO<Codigotributacaoitemlistaservico> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Codigotributacaoitemlistaservico> query, FiltroListagem _filtro) {
		CodigotributacaoitemlistaservicoFiltro filtro = (CodigotributacaoitemlistaservicoFiltro) _filtro;
		
		query
			.select("codigotributacaoitemlistaservico.cdcodigotributacaoitemlistaservico, " +
					"codigotributacao.cdcodigotributacao, codigotributacao.codigo, codigotributacao.descricao, " +
					"itemlistaservico.cditemlistaservico, itemlistaservico.codigo, itemlistaservico.descricao, " +
					"municipio.nome, uf.sigla")
			.leftOuterJoin("codigotributacaoitemlistaservico.codigotributacao codigotributacao")		
			.leftOuterJoin("codigotributacao.municipio municipio")		
			.leftOuterJoin("municipio.uf uf")		
			.leftOuterJoin("codigotributacaoitemlistaservico.itemlistaservico itemlistaservico")
			.whereLikeIgnoreAll("codigotributacao.codigo", filtro.getCodigo())
			.whereLikeIgnoreAll("codigotributacao.descricao", filtro.getDescricao())
			.where("municipio = ?", filtro.getMunicipio())
			.where("uf = ?", filtro.getUf())
			.where("codigotributacao.ativo = ?", Boolean.TRUE);
	}
	
}
