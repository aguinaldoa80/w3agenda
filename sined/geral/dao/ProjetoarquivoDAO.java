package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Projetoarquivo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProjetoarquivoDAO extends GenericDAO<Projetoarquivo> {
	
	/* singleton */
	private static ProjetoarquivoDAO instance;
	public static ProjetoarquivoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ProjetoarquivoDAO.class);
		}
		return instance;
	}
		
}
