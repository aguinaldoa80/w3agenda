package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Materiallegenda;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MateriallegendaFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MateriallegendaDAO extends GenericDAO<Materiallegenda>{

	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Materiallegenda> query, FiltroListagem _filtro) {
		MateriallegendaFiltro filtro = (MateriallegendaFiltro) _filtro;
		
		query
			.select("distinct materiallegenda.cdmateriallegenda, materiallegenda.legenda, materiallegenda.processo")
			.leftOuterJoin("materiallegenda.listaMateriallegendamaterialtipo listaMateriallegendamaterialtipo")
			.where("listaMateriallegendamaterialtipo.materialtipo = ?", filtro.getMaterialtipo())
			.whereLikeIgnoreAll("materiallegenda.legenda", filtro.getLegenda())
			.ignoreJoin("listaMateriallegendamaterialtipo");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Materiallegenda> query) {
		query
			.leftOuterJoinFetch("materiallegenda.listaMateriallegendamaterialtipo listaMateriallegendamaterialtipo")
			.leftOuterJoinFetch("materiallegenda.simbolo simbolo");
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				
				Materiallegenda materiallegenda = (Materiallegenda) save.getEntity();
				if(materiallegenda.getSimbolo() != null){
					arquivoDAO.saveFile(materiallegenda, "simbolo");
				}
				
				save.saveOrUpdateManaged("listaMateriallegendamaterialtipo");
				return null;
			}
		});
	}

	/**
	 * Busca as legendas a partir do material.
	 *
	 * @param whereInMaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/02/2014
	 */
	public List<Materiallegenda> findByMaterial(String whereInMaterial) {
		return query()
					.select("materiallegenda.cdmateriallegenda, materiallegenda.legenda, simbolo.cdarquivo")
					.join("materiallegenda.simbolo simbolo")
					.join("materiallegenda.listaMateriallegendamaterialtipo listaMateriallegendamaterialtipo")
					.join("listaMateriallegendamaterialtipo.materialtipo materialtipo")
					.join("materialtipo.listaMaterial listaMaterial")
					.whereIn("listaMaterial.cdmaterial", whereInMaterial)
					.orderBy("materiallegenda.ordem")
					.list();
	}

	/**
	 * Verifica se existe a ordem cadastrada
	 *
	 * @param ordem
	 * @param cdmateriallegenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 16/04/2014
	 */
	public boolean haveOrdem(Integer ordem, Integer cdmateriallegenda) {
		return newQueryBuilderWithFrom(Long.class)
					.select("count(*)")
					.where("materiallegenda.cdmateriallegenda <> ?", cdmateriallegenda)
					.where("materiallegenda.ordem = ?", ordem)
					.unique() > 0;
	}
	
	/**
	 * M�todo que carrega a lista de legendas de material vinculados aos tipos de material do whereIn.
	 *
	 * @param whereInMaterial
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Materiallegenda> findByMaterialtipo(String whereInMaterialTipo){
		if(whereInMaterialTipo == null || whereInMaterialTipo.trim().isEmpty()){
			return null;
		}
		
		return query()
				.select("materiallegenda.cdmateriallegenda, listaMateriallegendamaterialtipo.cdmateriallegendamaterialtipo, " +
						"materialtipo.cdmaterialtipo, simbolo.cdarquivo, simbolo.nome, simbolo.tipoconteudo, simbolo.tamanho")
				.join("materiallegenda.simbolo simbolo")
				.join("materiallegenda.listaMateriallegendamaterialtipo listaMateriallegendamaterialtipo")
				.join("listaMateriallegendamaterialtipo.materialtipo materialtipo")
				.whereIn("materialtipo.cdmaterialtipo", whereInMaterialTipo)
				.orderBy("materialtipo.cdmaterialtipo, materiallegenda.ordem  ASC")
				.list();
	}
}
