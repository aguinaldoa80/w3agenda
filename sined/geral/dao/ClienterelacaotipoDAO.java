package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Clienterelacaotipo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("clienterelacaotipo.descricao")
public class ClienterelacaotipoDAO extends GenericDAO<Clienterelacaotipo>{

}
