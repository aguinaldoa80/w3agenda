package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoCampo;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoSegmentoDetalhe;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class BancoConfiguracaoRetornoCampoDAO extends GenericDAO<BancoConfiguracaoRetornoCampo> {

	
	
	public List<BancoConfiguracaoRetornoCampo> findBySegmento(BancoConfiguracaoRetornoSegmentoDetalhe bean){
		if(bean == null || bean.getCdBancoConfiguracaoRetornoSegmentoDetalhe() == null)
			throw new SinedException("Segmento n�o pode ser nulo");
		
		return query()
		.select("bancoConfiguracaoRetornoCampo.cdbancoconfiguracaoretornocampo, bancoConfiguracaoRetornoCampo.campo, " + 
				"bancoConfiguracaoRetornoCampo.posIni, bancoConfiguracaoRetornoCampo.tamanho, " +
				"bancoConfiguracaoRetornoCampo.qtdeDecimal, bancoConfiguracaoRetornoCampo.formatoData, bancoConfiguracaoRetornoCampo.calculado")
		.where("bancoConfiguracaoRetornoCampo.bancoConfiguracaoRetornoSegmentoDetalhe = ?", bean)
		.list();
		
		
	}
	
}
