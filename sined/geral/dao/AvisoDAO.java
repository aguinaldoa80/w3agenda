	package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Tipoaviso;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.AvisoFiltro;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.GerenciaavisoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("aviso.dtaviso desc")
public class AvisoDAO extends GenericDAO<Aviso> {

	@Override
	public void updateListagemQuery(QueryBuilder<Aviso> query,	FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		AvisoFiltro filtro = (AvisoFiltro) _filtro;
		
		query
			.select("distinct aviso.cdaviso, aviso.assunto, aviso.dtaviso, aviso.complemento, " +
					"tipoaviso.descricao, usuario.nome, papel.nome, categoria.cdcategoria, " +
					"cliente.cdpessoa, motivoaviso.cdmotivoaviso, motivoaviso.descricao, motivoaviso.motivo ")
			.join("aviso.tipoaviso tipoaviso")
			.leftOuterJoin("aviso.papel papel")
			.leftOuterJoin("aviso.usuario usuario")
			.leftOuterJoin("aviso.categoria categoria")
			.leftOuterJoin("aviso.cliente cliente")
			.leftOuterJoin("aviso.motivoaviso motivoaviso")
			.leftOuterJoin("aviso.listaAvisousuario listaAvisousuario")
			.leftOuterJoin("listaAvisousuario.usuario usuarioAviso")
			.whereLikeIgnoreAll("categoria.nome", filtro.getCategoria())
			.whereLikeIgnoreAll("cliente.nome", filtro.getCliente())
			.whereLikeIgnoreAll("papel.nome", filtro.getPapel())
				

			.whereLikeIgnoreAll("assunto", filtro.getAssunto())
			.whereLikeIgnoreAll("complemento", filtro.getComplemento())
			.where("aviso.dtaviso >= ?", filtro.getDtavisode())
			.where("aviso.dtaviso <= ?", filtro.getDtavisoate())
			.where("aviso.avisoOrigem = ?", filtro.getAvisoOrigem())
			.ignoreJoin("listaAvisousuario", "usuarioAviso");
		
		if(filtro.getTipoaviso() != null){
			if(filtro.getTipoaviso().getCdtipoaviso().equals(Tipoaviso.NIVEL.getCdtipoaviso())){
				query.where("tipoaviso = ?", Tipoaviso.NIVEL);
			} else if(filtro.getTipoaviso().getCdtipoaviso().equals(Tipoaviso.USUARIO.getCdtipoaviso())){
				query.where("tipoaviso = ?", Tipoaviso.USUARIO);
			} else if(filtro.getTipoaviso().getCdtipoaviso().equals(Tipoaviso.CLIENTE.getCdtipoaviso())){
				query.where("tipoaviso = ?", Tipoaviso.CLIENTE);
			} else if(filtro.getTipoaviso().getCdtipoaviso().equals(Tipoaviso.GERAL.getCdtipoaviso())){
				query.where("tipoaviso = ?", Tipoaviso.GERAL);
			}
		}
		
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		if (!administrador){
			if (whereInPapel != null && !whereInPapel.equals("")){
				query.openParentheses();
				query.whereIn("papel.cdpapel", whereInPapel)
				.or()
					.where("motivoaviso.motivo = ?",MotivoavisoEnum.ATRASO_AGENDA_INTERACAO)
				.or()
					.where("motivoaviso.motivo = ?",MotivoavisoEnum.ORDEM_SERVICO_ATRASADA)
				.or()
					.where("papel.cdpapel is null");
				query.closeParentheses();
			} else {
				query.where("papel.cdpapel is null");
			}
		}
		
		if(filtro.getLido() != null){
			if(StringUtils.isNotBlank(filtro.getUsuario())){
				query.whereLikeIgnoreAll("usuarioAviso.nome", filtro.getUsuario());
				query.where("listaAvisousuario.dtleitura " + (filtro.getLido() ? " is not null" : " is null"));
			}else {
				query.where("listaAvisousuario.dtleitura " + (filtro.getLido() ? " is not null" : " is null"));
			}
		}
		
		if(StringUtils.isNotBlank(filtro.getUsuario())){
			query.openParentheses()
				.whereLikeIgnoreAll("usuario.nome", filtro.getUsuario())
				.or()
				.whereLikeIgnoreAll("usuarioAviso.nome", filtro.getUsuario())
			.closeParentheses();
		}
		
		if(filtro.getMotivoaviso() != null){
			query.openParentheses()
				.where("motivoaviso.cdmotivoaviso = ?", filtro.getMotivoaviso().getCdmotivoaviso())
			.closeParentheses();
		}
		
	
		query.orderBy("aviso.dtaviso desc, aviso.cdaviso desc");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Aviso> query) {
		query
			.select("aviso.cdaviso, aviso.assunto, aviso.dtaviso, aviso.dtfim, aviso.complemento, aviso.cdusuarioaltera, aviso.dtaltera, " +
					"tipoaviso.cdtipoaviso, usuario.cdpessoa, papel.cdpapel, empresa.cdpessoa, " +
					"aviso.avisoOrigem, aviso.idOrigem, categoria.cdcategoria, categoria.nome, " +
					"cliente.cdpessoa, cliente.nome, motivoaviso.cdmotivoaviso, motivoaviso.descricao, " +
					"listaAvisousuario.cdavisousuario, usuarioAviso.cdpessoa, usuarioAviso.nome, listaAvisousuario.dtleitura, listaAvisousuario.dtnotificacao, " +
					"listaAvisousuario.dtEmail ")
			.join("aviso.tipoaviso tipoaviso")
			.leftOuterJoin("aviso.empresa empresa")
			.leftOuterJoin("aviso.papel papel")
			.leftOuterJoin("aviso.usuario usuario")
			.leftOuterJoin("aviso.categoria categoria")
			.leftOuterJoin("aviso.cliente cliente")
			.leftOuterJoin("aviso.motivoaviso motivoaviso")
			.leftOuterJoin("aviso.listaAvisousuario listaAvisousuario")
			.leftOuterJoin("listaAvisousuario.usuario usuarioAviso");
	}

	/**
	 * M�todo que faz listagem padr�o
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Aviso> findForListagemFlex(GerenciaavisoFiltro filtro) {
//		if(filtro.getLidos() == null){
//			filtro.setLidos("NAO_LIDOS");
//		}
//		
//		StringBuilder query = new StringBuilder("");
//		if(filtro.getLidos().toUpperCase().equals("TODOS") || filtro.getLidos().toUpperCase().equals("NAO_LIDOS")){
//			query.append( 
//				"SELECT A.CDAVISO AS CDAVISO, NULL AS CDAVISOUSUARIO, A.ASSUNTO AS ASSUNTO, A.COMPLEMENTO AS COMPLEMENTO, A.DTAVISO AS DTAVISO, 'N�o' AS STATUS, A.AVISOORIGEM AS AVISOORIGEM, A.IDORIGEM AS IDORIGEM "+
//				"FROM AVISO A " +
//				"JOIN TIPOAVISO TA ON TA.CDTIPOAVISO = A.CDTIPOAVISO "+
//				"LEFT JOIN USUARIO U ON U.CDPESSOA  = A.CDUSUARIO "+
//				"LEFT JOIN PAPEL P ON P.CDPAPEL  = A.CDPAPEL " +
//				"WHERE 1=1 ");
//			appendWhere(query, filtro);
//			
//			query.append("AND (");
//		
//			if(filtro.getUsuario().getListaUsuariopapel() != null && filtro.getUsuario().getListaUsuariopapel().size() > 0){
//				query.append("P.CDPAPEL IN ("+CollectionsUtil.listAndConcatenate(filtro.getUsuario().getListaUsuariopapel(), "papel.cdpapel",",")+") OR " +
//							 "A.CDUSUARIO = "+filtro.getUsuario().getCdpessoa()+" OR TA.CDTIPOAVISO = 1) ");
//			}
//		
//			query.append("AND A.CDAVISO NOT IN " +
//						 "(SELECT AU.CDAVISO "+
//						 " FROM AVISOUSUARIO AU " +
//						 " WHERE AU.CDUSUARIO = " + filtro.getUsuario().getCdpessoa()+
//				 		 " AND A.CDAVISO = AU.CDAVISO) ");
//		}		
//		
//		if(filtro.getLidos().toUpperCase().equals("TODOS")){
//			query.append("UNION ALL ");
//		}
//		
//		if(filtro.getLidos().toUpperCase().equals("TODOS") || filtro.getLidos().toUpperCase().equals("LIDOS")){
//			query.append(
//				 "SELECT NULL AS CDAVISO, AU.CDAVISOUSUARIO AS CDAVISOUSUARIO, A.ASSUNTO AS ASSUNTO, A.COMPLEMENTO AS COMPLEMENTO, A.DTAVISO AS DTAVISO, 'Sim' AS STATUS, A.AVISOORIGEM AS AVISOORIGEM, A.IDORIGEM AS IDORIGEM "+
//				 "FROM AVISOUSUARIO AU " +
//				 "JOIN AVISO A ON A.CDAVISO = AU.CDAVISO "+
//				 "WHERE AU.CDUSUARIO = " + filtro.getUsuario().getCdpessoa()+" ");
//				appendWhere(query, filtro);
//		}
//		List<Aviso> list = getJdbcTemplate().query(
//			query.toString(),
//			new RowMapper() {
//				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
//					Aviso aviso = new Aviso();
//					aviso.setCdaviso(rs.getInt("CDAVISO"));
//					aviso.setCdavisousuario(rs.getInt("CDAVISOUSUARIO"));
//					aviso.setAssunto(rs.getString("ASSUNTO"));
//					aviso.setComplemento(rs.getString("COMPLEMENTO"));
//					aviso.setDtaviso(rs.getDate("DTAVISO"));
//					aviso.setLido(rs.getString("STATUS"));
//					aviso.setAvisoOrigem(AvisoOrigem.values()[rs.getInt("AVISOORIGEM")]);
//					
//					int id = rs.getInt("IDORIGEM");
//					aviso.setIdOrigem(id == 0 ? null : id);
//					
//					return aviso;
//				}
//			});
		
		return new ArrayList<Aviso>();
	}

//	/**
//	 * M�todo que adiciona where � query
//	 * 
//	 * @param query
//	 * @param filtro
//	 * @author Tom�s Rabelo
//	 */
//	private void appendWhere(StringBuilder query, GerenciaavisoFiltro filtro) {
//		if(filtro.getAssunto() != null && !filtro.getAssunto().equals("")){
//			query.append(" AND UPPER(retira_acento(A.ASSUNTO)) LIKE '%");
//			query.append(Util.strings.tiraAcento(filtro.getAssunto()).toUpperCase());
//			query.append("%' ");
//		}
//		
//		if(filtro.getComplemento() != null && !filtro.getComplemento().equals("")){
//			query.append(" AND UPPER(retira_acento(A.COMPLEMENTO)) LIKE '%");
//			query.append(Util.strings.tiraAcento(filtro.getComplemento().toUpperCase()));
//			query.append("%' ");
//		}
//		
//		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");  
//		if(filtro.getDtavisode() != null)
//			query.append("AND A.DTAVISO >= to_date('"+format.format(filtro.getDtavisode())+"', 'DD/MM/YYYY') ");
//		
//		if(filtro.getDtavisoate() != null)
//			query.append("AND A.DTAVISO <= to_date('"+format.format(filtro.getDtavisoate())+"', 'DD/MM/YYYY') ");
//		
//		Usuario usuario = SinedUtil.getUsuarioLogado();
//		if(usuario != null && usuario.getCdpessoa() != null)
//			query.append("AND (A.CDPESSOA IS NULL OR A.CDPESSOA IN ")
//				 .append("(SELECT E.CDPESSOA FROM USUARIOEMPRESA UE ")
//				 .append("JOIN EMPRESA E ON E.CDPESSOA = UE.CDEMPRESA ")
//				 .append("JOIN USUARIO U ON U.CDPESSOA = UE.CDUSUARIO ")
//				 .append("WHERE U.CDPESSOA = "+usuario.getCdpessoa()).append(")) ");
//	}
	
	public void deleteAvisosVelhos(){
		getJdbcTemplate().execute("delete from aviso where current_date - dtaviso > 60");
	}
	
	
		
	/**
	 * Retorna os avisos a serem inclu�dos no boleto
	 * @param cliente
	 * @param categoria
	 * @return
	 * @author Taidson
	 * @since 16/11/2010
	 */
	@SuppressWarnings("unchecked")
	public List<Aviso> avisoBoleto(Cliente cliente, String whereIn){
		StringBuilder sql = new StringBuilder();
		
		sql.append("select a.cdaviso, a.assunto, a.complemento, a.dtaviso ");
		sql.append("from aviso a ");
		sql.append("left outer join categoria cat on(a.cdcategoria = cat.cdcategoria) ");
		sql.append("left outer join cliente cli on(a.cdcliente = cli.cdpessoa) ");
		sql.append("where a.cdtipoaviso = 4 and (a.dtfim is null or to_char(a.dtfim, 'yyyy-mm-dd') >= '"+SinedDateUtils.currentDate()+"') ");
		
		if(cliente != null && cliente.getCdpessoa() != null && whereIn  != null && !whereIn.equals("")){
			sql.append("and ((a.cdcliente ="+cliente.getCdpessoa()+" or (a.cdcategoria in("+whereIn+ ") and a.cdcliente is null)) or (a.cdcliente is null and a.cdcategoria is null))");
		}else if(whereIn  != null && !whereIn.equals("")){
			sql.append("and ((a.cdcategoria in("+whereIn+ ")) or (a.cdcliente is null and a.cdcategoria is null)) ");
		}else if(cliente != null && cliente.getCdpessoa() != null){
			sql.append("and ((a.cdcliente ="+cliente.getCdpessoa()+") or (a.cdcliente is null and a.cdcategoria is null)) ");
		}

		SinedUtil.markAsReader();
		return getJdbcTemplate().query(sql.toString(), new RowMapper(){
			public Object mapRow(ResultSet rs, int row) throws SQLException {
				Aviso aviso = new Aviso();
				aviso.setCdaviso(rs.getInt("cdaviso"));
				aviso.setAssunto(rs.getString("assunto"));
				aviso.setComplemento(rs.getString("complemento"));
				aviso.setDtaviso(rs.getDate("dtaviso"));
				return aviso;
			}
		});
	}

	/**
	 * Busca os avisos para exibir na agenda
	 *
	 * @param data
	 * @param usuario
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Aviso> findForAgenda(Date data, Usuario usuario) {
		if(data == null || usuario == null)
			throw new SinedException("Par�metros inv�lidos");
		
		return query()
				.select("aviso.cdaviso, aviso.assunto ")
				.openParentheses()
					.openParentheses()
						.where("aviso.tipoaviso = ?", Tipoaviso.NIVEL)
						.openParentheses()
							.where("aviso.papel is null")
							.or()
							.whereIn("aviso.papel.cdpapel", CollectionsUtil.listAndConcatenate(usuario.getListaUsuariopapel(),"papel.cdpapel", ","))
						.closeParentheses()
					.closeParentheses()
					.or()
					.openParentheses()
						.where("aviso.tipoaviso = ?", Tipoaviso.USUARIO)
						.openParentheses()
							.where("aviso.usuario is null")
							.or()
							.where("aviso.usuario = ?", usuario)
						.closeParentheses()
					.closeParentheses()
				.closeParentheses()
				.openParentheses()
					.openParentheses()
						.where("aviso.dtaviso = ?", data)
						.where("aviso.dtfim is null")
					.closeParentheses()
					.or()
					.openParentheses()				
						.where("? >= aviso.dtaviso", data)
						.where("? <= aviso.dtfim", data)
					.closeParentheses()
				.closeParentheses()
				.list();
	}

	public List<Aviso> findLastAvisoDateByMotivoIdOrigem(Motivoaviso motivoaviso, Integer cdagendamento) {
		return query()
				.select("aviso.dtaviso")
				.where("aviso.motivoaviso = ?", motivoaviso)
				.where("aviso.idOrigem = ?", cdagendamento)
				.where("aviso.papel = ?", motivoaviso != null ? motivoaviso.getPapel() : null)
				.orderBy("aviso.dtaviso desc")
				.list();
	}
}