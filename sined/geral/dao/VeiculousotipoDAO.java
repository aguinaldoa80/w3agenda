package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Veiculousotipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.VeiculousotipoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("veiculousotipo.descricao")
public class VeiculousotipoDAO extends GenericDAO<Veiculousotipo> {

	@Override
	public void updateListagemQuery(QueryBuilder<Veiculousotipo> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		VeiculousotipoFiltro filtro = (VeiculousotipoFiltro) _filtro;
		query
			.select("veiculousotipo.cdveiculousotipo, veiculousotipo.descricao, veiculousotipo.escolta, veiculousotipo.normal")
			.whereLikeIgnoreAll("veiculousotipo.descricao", filtro.getDescricao())
			.orderBy("veiculousotipo.cdveiculousotipo desc");
	}

	/**
	 * M�todo que verifica se o tipo de uso � escolta
	 * 
	 * @param veiculousotipo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Boolean isVeiculoUsoTipoEscolta(Veiculousotipo veiculousotipo) {
		if (veiculousotipo == null || veiculousotipo.getCdveiculousotipo() == null) {
			throw new SinedException("Par�metros inv�lidos na fun��o isVeiculoUsoTipoEscolta.");
		}
		
		return newQueryBuilderWithFrom(Long.class)
			.select("count (*)")
			.where("veiculousotipo = ?", veiculousotipo)
			.where("veiculousotipo.escolta = ?", Boolean.TRUE)
			.unique() > 0L;
	}

	/**
	 * M�todo que retorna o veiculo uso tipo definido como escolta
	 * @return
	 */
	public Veiculousotipo veiculoUsoTipoEscolta() {
		return query()
			.select("veiculousotipo.cdveiculousotipo, veiculousotipo.descricao")
			.where("veiculousotipo.escolta = ?", Boolean.TRUE)
			.unique();
	}

	/**
	 * M�todo que remove o tipo escolta do registro atual
	 * 
	 * @author Tom�s Rabelo
	 */
	public void removeEscoltaVeiculoUsoTipo() {
		getHibernateTemplate().bulkUpdate("update Veiculousotipo vut set vut.escolta = "+Boolean.FALSE+" where vut.escolta = "+Boolean.TRUE);
	}
	
	/**
	 * M�todo que remove o tipo normal do registro atual
	 * 
	 * @author Fernando Boldrini
	 */
	public void removeNormalVeiculoUsoTipo() {
		getHibernateTemplate().bulkUpdate("update Veiculousotipo vut set vut.normal = " + Boolean.FALSE + " where vut.normal = "+Boolean.TRUE);
	}

	/**
	 * M�todo que retorna o tipo de uso de ve�culo normal
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Veiculousotipo veiculoUsoTipoNormal() {
		return query()
			.select("veiculousotipo.cdveiculousotipo, veiculousotipo.descricao")
			.where("veiculousotipo.normal = ?", Boolean.TRUE)
			.unique();
	}	
}
