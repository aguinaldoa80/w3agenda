package br.com.linkcom.sined.geral.dao;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.CusteioProcessado;
import br.com.linkcom.sined.geral.bean.SituacaoCusteio;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.CusteioProcessadoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CusteioProcessadoDAO  extends GenericDAO<CusteioProcessado> {
	
	private ArquivoService arquivoService;
	private ArquivoDAO arquivoDAO;
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<CusteioProcessado> query) {
		query.select("custeioProcessado.cdCusteioProcessado,custeioProcessado.dtInicio,custeioProcessado.dtFim,custeioProcessado.situacaoCusteio," +
				"custeioProcessado.dtConclusao,custeioProcessado.dtaltera,custeioProcessado.cdusuarioaltera," +
				"empresa.cdpessoa,empresa.nome," +
				"usuario.cdpessoa,usuario.nome," +
				"custeioProcessado.arquivo")
				.leftOuterJoin("custeioProcessado.empresa empresa")
				.leftOuterJoin("custeioProcessado.arquivo arquivo")
				.leftOuterJoin("custeioProcessado.usuario usuario");
		super.updateEntradaQuery(query);
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<CusteioProcessado> query,FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		}
		CusteioProcessadoFiltro filtro = (CusteioProcessadoFiltro) _filtro;
 		query
			.select("distinct custeioProcessado.cdCusteioProcessado,custeioProcessado.dtInicio,custeioProcessado.dtFim,custeioProcessado.dtConclusao," +
					"custeioProcessado.situacaoCusteio," +
					"empresa.cdpessoa,empresa.nome," +
					"usuario.cdpessoa,usuario.nome")
			.leftOuterJoin("custeioProcessado.empresa empresa")
			.leftOuterJoin("custeioProcessado.usuario usuario")
			.where("empresa = ?", filtro.getEmpresa())
			.where("custeioProcessado.dtConclusao = ?", filtro.getDtProcessamento())
			.where("custeioProcessado.dtInicio >= ?", filtro.getDtInicio())
			.where("custeioProcessado.situacaoCusteio = ?", filtro.getSituacaoCusteio())
			.where("custeioProcessado.dtFim >= ?", filtro.getDtFim())
			.where("usuario = ?", filtro.getUsuario())
			.orderBy("custeioProcessado.cdCusteioProcessado desc")
			.ignoreJoin("listaCusteioTipoDocumento");
 			if(filtro.getTipoDocumentoCusteioEnum() != null || filtro.getCodObjetoOrigem() !=null){
 				query
 					.leftOuterJoin("custeioProcessado.listaCusteioTipoDocumento listaCusteioTipoDocumento")
 					.where("listaCusteioTipoDocumento.codObjetoOrigem = ?",filtro.getCodObjetoOrigem())
 					.where("listaCusteioTipoDocumento.tipoDocumentoCusteioEnum =?",filtro.getTipoDocumentoCusteioEnum());
 			}
 			
	}
	
	public void updateSituacao(CusteioProcessado custeio, SituacaoCusteio situacao) {
		getHibernateTemplate().bulkUpdate("update CusteioProcessado c set c.situacaoCusteio = ? where c.cdCusteioProcessado = "+custeio.getCdCusteioProcessado()+"", new Object[]{
			situacao
		});
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				CusteioProcessado custeio = (CusteioProcessado)save.getEntity();
				
				if(custeio.getArquivo() != null){
					arquivoService.saveOrUpdate(custeio.getArquivo());
					
					arquivoDAO.saveFile(custeio, "arquivo");
					
				}
				return null;
			}
		});
	}
	public CusteioProcessado buscarCusteioPorId(Integer id) {
		return query()
				.select("custeioProcessado.cdCusteioProcessado,custeioProcessado.situacaoCusteio," +
						"listaCusteioTipoDocumento.cdCusteioTipoDocumento,listaCusteioTipoDocumento.tipoDocumentoCusteioEnum," +
						"listaCusteioTipoDocumento.codObjetoOrigem,listaCusteioTipoDocumento.codRateioOrigem")
				.leftOuterJoin("custeioProcessado.listaCusteioTipoDocumento listaCusteioTipoDocumento")
				.where("custeioProcessado.cdCusteioProcessado = ?", id)
				.unique();
	}
	
}
