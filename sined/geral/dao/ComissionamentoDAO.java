package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Comissionamento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Comissionamentotipo;
import br.com.linkcom.sined.geral.bean.enumeration.Criteriocomissionamento;
import br.com.linkcom.sined.geral.bean.enumeration.Documentocomissaotipoperiodo;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaovendaFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.bean.ComissionamentovendaReportBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ComissionamentoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("comissionamento.nome")
public class ComissionamentoDAO extends GenericDAO<Comissionamento> {

	@Override
	public void updateEntradaQuery(QueryBuilder<Comissionamento> query) {				
		query
			.select("comissionamento.cdcomissionamento, comissionamento.nome, comissionamento.comissionamentotipo, " +
					"comissionamento.deduzirvalor, comissionamento.quantidade, comissionamento.percentual, comissionamento.valor, " +
					"comissionamento.percentualFaixaVendedor, comissionamento.percentualFaixaVendedorPrincipal, comissionamento.percentualFaixaTotal, " +
					"listaComissionamentopessoa.cdcomissionamentopessoa, colaborador.cdpessoa, colaborador.nome, " +
					"listaComissionamentopessoa.recebecomissao, comissionamento.ativo, comissionamento.considerardiferencaentremetas, " +
					"comissionamento.cdusuarioaltera, comissionamento.dtaltera, listaComissionamentometa.cdcomissionamentometa, " +
					"listaComissionamentometa.metavenda, listaComissionamentometa.comissao, comissionamento.tipobccomissionamento," +
					"comissionamento.considerardiferencapagamento, comissionamento.criteriocomissionamento, " +
					"comissionamento.percentualdivisao, comissionamento.valordivisao, comissionamento.desempenhocumulativo, " +
					"comissionamento.agencia, comissionamento.considerardiferencapreco, comissionamento.calcularComissaoPorFaixa, " +
					"listaComissionamentofaixadesconto.cdcomissionamentofaixadesconto, listaComissionamentofaixadesconto.percentualdescontoate, " +
					"listaComissionamentofaixadesconto.percentualcomissao, " +
					"listaComissionamentoFaixaMarkup.cdComissionamentoFaixaMarkup, listaComissionamentoFaixaMarkup.percentualVendedor, " +
					"listaComissionamentoFaixaMarkup.percentualVendedorPrincipal, listaComissionamentoFaixaMarkup.percentualTotal, " +
					"faixa.cdFaixaMarkupNome, faixa.nome")
			.leftOuterJoin("comissionamento.listaComissionamentopessoa listaComissionamentopessoa")
			.leftOuterJoin("comissionamento.listaComissionamentometa listaComissionamentometa")
			.leftOuterJoin("listaComissionamentopessoa.colaborador colaborador")
			.leftOuterJoin("comissionamento.listaComissionamentofaixadesconto listaComissionamentofaixadesconto")
			.leftOuterJoin("comissionamento.listaComissionamentoFaixaMarkup listaComissionamentoFaixaMarkup")
			.leftOuterJoin("listaComissionamentoFaixaMarkup.faixa faixa")
			.orderBy("listaComissionamentofaixadesconto.percentualdescontoate");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Comissionamento> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		ComissionamentoFiltro filtro = (ComissionamentoFiltro) _filtro;
		query
			.select("comissionamento.cdcomissionamento, comissionamento.nome")
			.whereLikeIgnoreAll("comissionamento.nome", filtro.getNome());
	}

	/**
	* M�todo que busca o comissionamento com o campo deduzirvalor
	*
	* @param cdcomissionamento
	* @return
	* @since Sep 6, 2011
	* @author Luiz Fernando F Silva
	*/
	public Comissionamento verificaDeducao(Integer cdcomissionamento) {
		if(cdcomissionamento == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
				.select("comissionamento.cdcomissionamento, comissionamento.deduzirvalor")
				.where("comissionamento.cdcomissionamento = ?", cdcomissionamento)
				.unique();
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaComissionamentopessoa");
		save.saveOrUpdateManaged("listaComissionamentometa");
		save.saveOrUpdateManaged("listaComissionamentofaixadesconto");
		save.saveOrUpdateManaged("listaComissionamentoFaixaMarkup");
	}
	
	/**
	 * M�todo que busca os dados para relat�rio de comissionamento de venda
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<ComissionamentovendaReportBean> findForReportComissionamentovenda(DocumentocomissaovendaFiltro filtro) {
		if(filtro == null)
			throw new SinedException("Par�metro inv�lido.");
			
		StringBuilder query = new StringBuilder();
		StringBuilder where = new StringBuilder();
		Boolean clausulaWhere = false; 

		query.append("SELECT d.dtvencimento as vencimento, pcli.nome as clientenome, pcol.nome as colaboradornome, " +
				"v.cdvenda as cdvenda, dc.valorcomissao as valorcomissao, " +
				"pv.cdpedidovenda as cdpedidovenda, pvcli.nome as pvclientenome ");
		query.append("FROM documentocomissao dc ");
		query.append("LEFT OUTER JOIN  documento d ON d.cddocumento = dc.cddocumento ");
		query.append("LEFT OUTER JOIN  documentoacao da ON da.cddocumentoacao = d.cddocumentoacao ");
		query.append("LEFT OUTER JOIN  venda v ON v.cdvenda = dc.cdvenda ");
		query.append("LEFT OUTER JOIN  pessoa pcli ON pcli.cdpessoa = v.cdcliente ");
		query.append("LEFT OUTER JOIN  pessoa pcol ON pcol.cdpessoa = dc.cdpessoa ");
		query.append("LEFT OUTER JOIN  colaboradorcomissao cc ON cc.cdcolaboradorcomissao = dc.cdcolaboradorcomissao ");
		query.append("LEFT OUTER JOIN  pedidovenda pv ON pv.cdpedidovenda = dc.cdpedidovenda ");
		query.append("LEFT OUTER JOIN  pessoa pvcli ON pvcli.cdpessoa = pv.cdcliente ");
		
		if (Boolean.FALSE.equals(filtro.getExibirComissao())) {
			query
				.append(" where (dc.cdpedidovenda is null and pv.pedidovendasituacao in (" + Pedidovendasituacao.CONFIRMADO.getValue() + ", " + Pedidovendasituacao.PREVISTA.getValue() + ") )")
				.append(" and v is null ");
		}else if(Boolean.TRUE.equals(filtro.getExibirComissao())){
			query.append(" where v is not null");
		}else {
			query.append(" where (v is not null or pv is not null) ");
		}
		
		clausulaWhere = Boolean.TRUE;
		
		if(filtro.getCliente() != null && filtro.getCliente().getCdpessoa() != null){			
			if(clausulaWhere)
				where.append(" AND (v.cdcliente = " + filtro.getCliente().getCdpessoa() + " OR pv.cdcliente = " + filtro.getCliente().getCdpessoa() + ")");
			else
				where.append(" (v.cdcliente = " + filtro.getCliente().getCdpessoa() + " OR pv.cdcliente = " + filtro.getCliente().getCdpessoa() + ")");
			clausulaWhere = Boolean.TRUE;
		}
		
		if(filtro.getColaboradorcomissao() != null && filtro.getColaboradorcomissao().getCdpessoa() != null){
			if(clausulaWhere)
				where.append(" AND dc.cdpessoa = " + filtro.getColaboradorcomissao().getCdpessoa());					
			else 
				where.append(" dc.cdpessoa = " + filtro.getColaboradorcomissao().getCdpessoa());			
			clausulaWhere = Boolean.TRUE;
		}
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		
		if(Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
			if(filtro.getDtinicio() != null && filtro.getDtfim() != null){
				where.append(" (");
					where.append("(");
					where.append(" AND d.dtvencimento >= to_date('"+ format.format(filtro.getDtinicio())+ "', 'DD/MM/YYYY')");
					where.append(" AND d.dtvencimento <= to_date('"+ format.format(filtro.getDtfim())+ "', 'DD/MM/YYYY')");
					where.append(")");
				where.append(" OR ");
				where.append("exists (select v.cddocumento " +
										"from Vdocumentonegociado v " +
										"where v.cddocumento = documento.cddocumento " +
										"and v.dtvencimentonegociado >= '" + filtro.getDtinicio() + "' " +
										"and v.dtvencimentonegociado <= '" + filtro.getDtfim() + "' )");
				where.append(") ");
			}else if(filtro.getDtinicio() != null){
				where.append("(");
				where.append(" AND d.dtvencimento >= to_date('"+ format.format(filtro.getDtinicio())+ "', 'DD/MM/YYYY')");
				where.append(" OR ");
				where.append("exists (select v.cddocumento " +
										"from Vdocumentonegociado v " +
										"where v.cddocumento = documento.cddocumento " +
										"and v.dtvencimentonegociado >= '" + filtro.getDtinicio() + "' )");
				
				where.append(") ");
			}else if(filtro.getDtfim() != null){
				where.append("(");
				where.append(" AND d.dtvencimento <= to_date('"+ format.format(filtro.getDtfim())+ "', 'DD/MM/YYYY')");
				where.append(" OR ");
				where.append("exists (select v.cddocumento " +
										"from Vdocumentonegociado v " +
										"where v.cddocumento = documento.cddocumento " +
										"and v.dtvencimentonegociado <= '" + filtro.getDtfim() + "' )");
				where.append(") ");
			}
		}else if(Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
			if(filtro.getDtinicio() != null && filtro.getDtfim() != null){
				where.append("(");
				where.append("exists (select d.cddocumento " +
								"from Movimentacaoorigem mo " +
								"join mo.documento d " +
								"join mo.movimentacao m " +
								"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
								"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
								"and d.cddocumento = documento.cddocumento " +
								")");
				where.append(" OR ");
				where.append("exists(select d.cddocumento " +
								"from Movimentacaoorigem mo " +
								"join mo.documento d " +
								"join mo.movimentacao m " +
								"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
								"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
								"and d.cddocumento in (select v.cddocumentonegociado from Vdocumentonegociado v where v.cddocumento = documento.cddocumento) " +
								")");
				where.append(") ");
			}else if(filtro.getDtinicio() != null){
				where.append("(");
				where.append("exists (select d.cddocumento " +
								"from Movimentacaoorigem mo " +
								"join mo.documento d " +
								"join mo.movimentacao m " +
								"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
								"and d.cddocumento = documento.cddocumento " +
								")");
				where.append(" OR ");
				where.append("exists(select d.cddocumento " +
								"from Movimentacaoorigem mo " +
								"join mo.documento d " +
								"join mo.movimentacao m " +
								"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
								"and d.cddocumento in (select v.cddocumentonegociado from Vdocumentonegociado v where v.cddocumento = documento.cddocumento) " +
								")");
				where.append(") ");
			}else if(filtro.getDtfim() != null){
				where.append("(");
				where.append("exists (select d.cddocumento " +
								"from Movimentacaoorigem mo " +
								"join mo.documento d " +
								"join mo.movimentacao m " +
								"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
								"and d.cddocumento = documento.cddocumento " +
								")");
				where.append(" OR ");
				where.append("exists(select d.cddocumento " +
								"from Movimentacaoorigem mo " +
								"join mo.documento d " +
								"join mo.movimentacao m " +
								"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
								"and d.cddocumento in (select v.cddocumentonegociado from Vdocumentonegociado v where v.cddocumento = documento.cddocumento) " +
								")");
				where.append(") ");
			}
		} else if(Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo())){
			if(filtro.getDtinicio() != null && filtro.getDtfim() != null){
				where.append("(");
				where.append("(");
				where.append("documento.dtemissao >= to_date('"+ format.format(filtro.getDtinicio())+ "', 'DD/MM/YYYY')");
				where.append("documento.dtemissao <= to_date('"+ format.format(filtro.getDtfim())+ "', 'DD/MM/YYYY')");
				where.append(") ");
				where.append(" OR ");
				where.append("exists (select v.cddocumento " +
										"from Vdocumentonegociado v " +
										"where v.cddocumento = documento.cddocumento " +
										"and v.dtemissaonegociado >= '" + filtro.getDtinicio() + "' " +
										"and v.dtemissaonegociado <= '" + filtro.getDtfim() + "' )");
				where.append(") ");
			}else if(filtro.getDtinicio() != null){
				where.append("(");
				where.append("documento.dtemissao >= to_date('"+ format.format(filtro.getDtinicio())+ "', 'DD/MM/YYYY')");
				where.append(" OR ");
				where.append("exists (select v.cddocumento " +
										"from Vdocumentonegociado v " +
										"where v.cddocumento = documento.cddocumento " +
										"and v.dtemissaonegociado >= '" + filtro.getDtinicio() + "' )");
				
				where.append(") ");
			}else if(filtro.getDtfim() != null){
				where.append("(");
				where.append("documento.dtemissao <= to_date('"+ format.format(filtro.getDtfim())+ "', 'DD/MM/YYYY')");
				where.append(" OR ");
				where.append("exists (select v.cddocumento " +
										"from Vdocumentonegociado v " +
										"where v.cddocumento = documento.cddocumento " +
										"and v.dtemissaonegociado <= '" + filtro.getDtfim() + "' )");
				where.append(") ");
			}
		}
		
		if(filtro.getMostrar() != null && filtro.getMostrar().equals("Pagos com pend�ncia de repasse")){
			if(clausulaWhere)
				where.append(" AND da.cddocumentoacao = " + Documentoacao.BAIXADA.getCddocumentoacao())
					 .append("OR da.cddocumentoacao = " + Documentoacao.NEGOCIADA.getCddocumentoacao())
					 .append(" AND cc.cdcolaboradorcomissao is null ");
			else
				where.append(" da.cddocumentoacao = " + Documentoacao.BAIXADA.getCddocumentoacao())
				.append("OR da.cddocumentoacao = " + Documentoacao.NEGOCIADA.getCddocumentoacao())
					 .append(" AND cc.cdcolaboradorcomissao is null ");
			clausulaWhere = Boolean.TRUE;			
		}
		
		if(clausulaWhere)			
			query.append("WHERE ").append(where);
		
//		System.out.println("QUERY: " + query);
		SinedUtil.markAsReader();
		List<ComissionamentovendaReportBean> lista = null;		
		lista = getJdbcTemplate().query(query.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new ComissionamentovendaReportBean(
						rs.getDate("vencimento"),
						rs.getString("clientenome"),
						rs.getString("pvclientenome"),
						rs.getString("colaboradornome"),
						rs.getInt("cdvenda"),
						rs.getInt("cdpedidovenda"),
						rs.getLong("valorcomissao"));
			}
		});
		
		return lista;
	}
	/**
	 * M�todo que busca os comissionamentos para valida��o de ativo e colaboradores
	 *
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Comissionamento> findForComissionamentodesempenho(Comissionamento bean) {		
		QueryBuilder<Comissionamento> query = query();
		
		query
			.select("comissionamento.cdcomissionamento, comissionamento.nome, comissionamentopessoa.cdcomissionamentopessoa, " +
					"colaborador.cdpessoa, colaborador.nome, comissionamento.ativo")
			.leftOuterJoin("comissionamento.listaComissionamentopessoa comissionamentopessoa")
			.leftOuterJoin("comissionamentopessoa.colaborador colaborador")
			.where("comissionamento.comissionamentotipo = ?", Comissionamentotipo.DESEMPENHO);
		
		if(bean != null && bean.getCdcomissionamento() != null)
			query.where("comissionamento.cdcomissionamento <> ? ", bean.getCdcomissionamento());
		
		return query.list();				
	}
	
	public Comissionamento findForCalcularmeta(Comissionamento comissionamento) {		
		return query()
			.select("comissionamento.cdcomissionamento, comissionamento.nome, comissionamentopessoa.cdcomissionamentopessoa, " +
					"colaborador.cdpessoa, colaborador.nome, comissionamento.ativo, comissionamentometa.cdcomissionamentometa, " +
					"comissionamentometa.metavenda, comissionamentometa.comissao, comissionamento.desempenhocumulativo, " +
					"comissionamento.considerardiferencaentremetas ")
			.leftOuterJoin("comissionamento.listaComissionamentopessoa comissionamentopessoa")
			.leftOuterJoin("comissionamento.listaComissionamentometa comissionamentometa")
			.leftOuterJoin("comissionamentopessoa.colaborador colaborador")
			.where("comissionamento.comissionamentotipo = ?", Comissionamentotipo.DESEMPENHO)
			.where("comissionamento.ativo = true")
			.where("comissionamento = ?", comissionamento)
			.orderBy("comissionamentometa.metavenda")
			.unique();				
	}
	/**
	 * M�todo que busca os comissionamentos para contrato. 
	 * @return
	 */
	public List<Comissionamento> findForContrato(){
		return query()
			.select("comissionamento.cdcomissionamento, comissionamento.nome")
			//.where("comissionamento.ativo = ?", Boolean.TRUE) //Aguardando informa��es sobre melhoria, pois existe problema em filtrar somente os comissionamentos ativos. 
			.where("comissionamento.comissionamentotipo = ?", Comissionamentotipo.CONTRATO)
			.orderBy("comissionamento.nome")
			.list();
	}
	
	public List<Comissionamento> findForTabelapreco(Comissionamento comissionamento){
		List<Comissionamentotipo> lista = new ArrayList<Comissionamentotipo>();
		lista.add(Comissionamentotipo.VENDA);
		
		return query()
			.select("comissionamento.cdcomissionamento, comissionamento.nome")
			.where("comissionamento.ativo = true")
			.openParentheses()
				.whereIn("comissionamento.comissionamentotipo", Comissionamentotipo.listAndConcatenate(lista))
				.or()
				.where("comissionamento = ?", comissionamento)
			.closeParentheses()
			.orderBy("comissionamento.nome")
			.list();
	}
	
	/**
	 * M�todo que busca o comissionamento com o colaborador
	 *
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Comissionamento> findComissionamentoByColaborador(Colaborador colaborador) {
		return query()
			.select("comissionamento.cdcomissionamento, comissionamento.nome ")
			.join("comissionamento.listaComissionamentopessoa comissionamentopessoa")
			.join("comissionamentopessoa.colaborador colaborador")
			.where("comissionamento.ativo = true")
			.where("colaborador = ?", colaborador)
			.orderBy("comissionamento.nome")
			.list();
	}
	
	public Comissionamento findByCriterio(Criteriocomissionamento criteriocomissionamento) {
		QueryBuilder<Comissionamento> query = query();
		
		updateEntradaQuery(query);
		return query
				.where("comissionamento.criteriocomissionamento = ?", criteriocomissionamento)
				.unique();
	}

	public Comissionamento findByNome(String nome) {
		return query().whereEqualIgnoreAll("comissionamento.nome", nome).unique();
	}
}