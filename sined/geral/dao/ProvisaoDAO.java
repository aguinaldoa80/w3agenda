package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Colaboradordespesa;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Provisao;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesaitemtipo;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ProvisaoFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.ConsultaSaldoEventoProvisionamentoReportFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProvisaoDAO extends GenericDAO<Provisao>{

	@Override
	public void updateEntradaQuery(QueryBuilder<Provisao> query) {
		query
			.select("provisao.cdProvisao, provisao.dtProvisao, provisao.valor, provisao.dtaltera, provisao.cdusuarioaltera, provisao.tipo, " +
					"empresa.cdpessoa, empresa.nome, empresa.nomefantasia, " +
					"colaborador.cdpessoa, colaborador.nome, " +
					"evento.cdEventoPagamento, evento.nome, evento.tipoEvento, " +
					"colaboradorDespesaOrigem.cdcolaboradordespesa")
			.leftOuterJoin("provisao.empresa empresa")
			.leftOuterJoin("provisao.colaborador colaborador")
			.leftOuterJoin("provisao.evento evento")
			.leftOuterJoin("provisao.colaboradorDespesaOrigem colaboradorDespesaOrigem");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Provisao> query,
			FiltroListagem _filtro) {
		ProvisaoFiltro filtro = (ProvisaoFiltro) _filtro;
		
		query
			.select("provisao.cdProvisao, provisao.dtProvisao, provisao.dtaltera, provisao.cdusuarioaltera, provisao.valor, " +
					"empresa.cdpessoa, empresa.nome, empresa.nomefantasia, " +
					"colaborador.cdpessoa, colaborador.nome, " +
					"evento.cdEventoPagamento, evento.nome, evento.tipoEvento")
			.leftOuterJoin("provisao.empresa empresa")
			.leftOuterJoin("provisao.colaborador colaborador")
			.leftOuterJoin("provisao.evento evento")
			.where("colaborador = ?", filtro.getColaborador())
			.where("empresa = ?", filtro.getEmpresa())
			.where("evento = ?", filtro.getEvento());

		if(filtro.getDtInicio() != null || filtro.getDtFim() != null) {
			query.where("provisao.dtProvisao >= ?", filtro.getDtInicio())
				 .where("provisao.dtProvisao <= ?", filtro.getDtFim());
		}
	}

	public List<Provisao> findByColaboradorDespesaOrigem(Colaboradordespesa colaboradorDespesaOrigem){
		if(colaboradorDespesaOrigem == null || colaboradorDespesaOrigem.getCdcolaboradordespesa() == null){
			return new ArrayList<Provisao>();
		}
		return query()
				.select("provisao.cdProvisao, colaboradorDespesaOrigem.cdcolaboradordespesa")
				.join("provisao.colaboradorDespesaOrigem colaboradorDespesaOrigem")
				.where("colaboradorDespesaOrigem = ?", colaboradorDespesaOrigem)
				.list();
	}
	
	public void deleteProvisoes(String whereIn){
		if(StringUtils.isNotBlank(whereIn)){
			getHibernateTemplate().bulkUpdate("delete from Provisao p where p.cdProvisao in("+whereIn+")");
		}
	}

	public List<Provisao> findForSaldoEventoProvisao(ConsultaSaldoEventoProvisionamentoReportFiltro filtro) {
		return querySined()
				.select("provisao.cdProvisao, provisao.dtProvisao, provisao.valor, " +
						"empresa.cdpessoa, empresa.nome, empresa.nomefantasia, " +
						"colaborador.cdpessoa, colaborador.nome, " +
						"evento.cdEventoPagamento, evento.nome, evento.tipoEvento")
				.leftOuterJoin("provisao.empresa empresa")
				.leftOuterJoin("provisao.colaborador colaborador")
				.leftOuterJoin("provisao.evento evento")
				.where("empresa = ?", filtro.getEmpresa())
				.where("colaborador = ?", filtro.getColaborador())
				.where("provisao.dtProvisao >= ?", filtro.getDtinicio())
				.where("provisao.dtProvisao <= ?", filtro.getDtfim())
				.where("evento = ?", filtro.getEventoPagamento())
				.orderBy("empresa.cdpessoa, colaborador.nome, provisao.dtProvisao")
				.list();
	}

	public Money calcularValorTotalFechamentoFolha(Empresa empresa, String whereInEventoProvisionamento, Date dtInicio, Date dtFim) {
		Long valorTotalCredito = newQueryBuilder(Long.class)
				.select("sum(provisao.valor)")
				.setUseTranslator(false)
				.from(Provisao.class)
				.leftOuterJoin("provisao.evento evento")
				.where("provisao.empresa = ?", empresa)
				.where("provisao.dtProvisao >= ?", dtInicio)
				.where("provisao.dtProvisao <= ?", dtFim)
				.whereIn("evento.cdEventoPagamento", whereInEventoProvisionamento)
				.where("evento.tipoEvento = ?", Colaboradordespesaitemtipo.CREDITO)
				.unique();
		
		Long valorTotalDebito = newQueryBuilder(Long.class)
				.select("sum(provisao.valor)")
				.setUseTranslator(false)
				.from(Provisao.class)
				.leftOuterJoin("provisao.evento evento")
				.where("provisao.empresa = ?", empresa)
				.where("provisao.dtProvisao >= ?", dtInicio)
				.where("provisao.dtProvisao <= ?", dtFim)
				.whereIn("evento.cdEventoPagamento", whereInEventoProvisionamento)
				.where("evento.tipoEvento = ?", Colaboradordespesaitemtipo.DEBITO)
				.unique();
		
		if(valorTotalCredito == null) valorTotalCredito = 0l;
		if(valorTotalDebito == null) valorTotalDebito = 0l;
		
		return new Money((valorTotalCredito - valorTotalDebito) / 100d);
	}
		
	public List<Provisao> findForGerarLancamentoContabil(GerarLancamentoContabilFiltro filtro){
		return query()
				.select("provisao.cdProvisao, provisao.dtProvisao, provisao.valor, provisao.dtaltera, provisao.cdusuarioaltera, provisao.tipo, " +
						"empresa.cdpessoa, empresa.nome, empresa.nomefantasia, " +
						"colaborador.cdpessoa, colaborador.nome, " +
						"evento.cdEventoPagamento, evento.nome, evento.tipoEvento, " +
						"contaContabil.cdcontacontabil, contaContabil.nome")
				.leftOuterJoin("provisao.empresa empresa")
				.leftOuterJoin("provisao.colaborador colaborador")
				.leftOuterJoin("colaborador.contaContabil contaContabil")
				.leftOuterJoin("provisao.evento evento")
				.where("provisao.dtProvisao>=?", filtro.getDtPeriodoInicio())
				.where("provisao.dtProvisao<=?", filtro.getDtPeriodoFim())
				.where("provisao.empresa=?", filtro.getEmpresa())
				.where("provisao.tipo=?", Colaboradordespesaitemtipo.CREDITO)
				.where("not exists(select mco.cdmovimentacaocontabilorigem from Movimentacaocontabilorigem mco where mco.provisao = provisao)")
				.list();
	}

	public List<Provisao> findWithOrigem(String whereIn) {
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("provisao.cdProvisao, colaboradorDespesaOrigem.cdcolaboradordespesa")
				.join("provisao.colaboradorDespesaOrigem colaboradorDespesaOrigem")
				.whereIn("provisao.cdProvisao", whereIn)
				.list();
		
	}

	public List<Provisao> findByIds(String whereIn) {
		return querySined()
				
				.whereIn("provisao.cdProvisao", whereIn)
				.list();	
	}

}
