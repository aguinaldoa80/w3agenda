package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materiallocalarmazenagem;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MateriallocalarmazenagemDAO extends GenericDAO<Materiallocalarmazenagem>{

	/**
	* M�todo que busca os locais com a quantidade min�ma do material
	*
	* @param material
	* @return
	* @since 22/08/2016
	* @author Luiz Fernando
	*/
	public List<Materiallocalarmazenagem> findByMaterial(Material material){
		if(material == null || material.getCdmaterial() == null){
			throw new SinedException("Material n�o pode ser nulo.");
		}
		
		return query()
				.select("materiallocalarmazenagem.cdmateriallocalarmazenagem, materiallocalarmazenagem.quantidademinima, " +
						"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome ")
				.join("materiallocalarmazenagem.localarmazenagem localarmazenagem")
				.where("materiallocalarmazenagem.material = ?", material)
				.list();
	}
}
