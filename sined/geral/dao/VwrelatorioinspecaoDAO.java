package br.com.linkcom.sined.geral.dao;


import java.util.List;

import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.view.Vwrelatorioinspecao;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.FormularioinspecaoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VwrelatorioinspecaoDAO extends GenericDAO<Vwrelatorioinspecao> {
	/**
	 * <b>M�todo respons�vel por carregar uma lista de itensinspecao atrav�s de uma view
	 * @author Ramon Barzil
	 * @param veiculo,filtro
	 * @return List<Vwrelatorioinspecao>
	 */
	public List<Vwrelatorioinspecao> findForRelatorio(Veiculo veiculo,FormularioinspecaoFiltro filtro ){
		
			return query()
			.select("veiculo.cdveiculo, veiculo.tipocontroleveiculo, vwrelatorioinspecao.placa, vwrelatorioinspecao.prefixo,"+
					"vwrelatorioinspecao.cditeminspecao, vwrelatorioinspecao.nomepessoa, vwrelatorioinspecao.nomeiteminspecao,"+
					"vwrelatorioinspecao.visual, vwrelatorioinspecao.nometipoiteminspecao, vwrelatorioinspecao.tipovencimento,"+
					"vwrelatorioinspecao.kminspecao,vwrelatorioinspecao.dtmanutencao,vwrelatorioinspecao.calculakmatual,"+
					"vwrelatorioinspecao.nometipo, vwrelatorioinspecao.nomemodelo")
			.leftOuterJoin("vwrelatorioinspecao.veiculo veiculo")
			.where("veiculo.cdveiculo =?",veiculo.getCdveiculo())
			.where("vwrelatorioinspecao.visual=?",filtro.getVisual())
			.orderBy("vwrelatorioinspecao.nometipoiteminspecao, vwrelatorioinspecao.nomeiteminspecao")
			.list();
		}
	/**
	 * <b>M�todo respons�vel por carregar uma lista de itensinspecao atrav�s de uma view
	 * @author Fernando Boldrini
	 * @param filtro, order
	 * @return List<Vwrelatorioinspecao>
	 */
	public List<Vwrelatorioinspecao> findForVeiculoParaInspecao(
			FormularioinspecaoFiltro filtro, String order) {
		
		return querySined()
				
		.select("vwrelatorioinspecao.cdrelatorioinspecao, veiculo.cdveiculo, veiculo.tipocontroleveiculo, vwrelatorioinspecao.placa, vwrelatorioinspecao.prefixo,"+
					"vwrelatorioinspecao.nomepessoa, vwrelatorioinspecao.nomeiteminspecao,"+
					"vwrelatorioinspecao.visual, vwrelatorioinspecao.nometipoiteminspecao,"+
					"vwrelatorioinspecao.kminspecao, vwrelatorioinspecao.horimetroinspecao, vwrelatorioinspecao.dtmanutencao,vwrelatorioinspecao.calculakmatual,vwrelatorioinspecao.calculahorimetroatual,"+
					"vwrelatorioinspecao.nometipo, vwrelatorioinspecao.nomemodelo, vwrelatorioinspecao.nomeultimoprojetousoveiculo," +
					"vwrelatorioinspecao.nomeultimocondutorusoveiculo")
		.leftOuterJoin("vwrelatorioinspecao.veiculo veiculo")
		.where("nomepessoa = ?", filtro.getCooperado() == null ? null : filtro.getCooperado().getNome())
		.where("veiculo.cdveiculo = ?", filtro.getVeiculo() == null ? null : filtro.getVeiculo().getCdveiculo())
		.where("veiculo.veiculomodelo = ?", filtro.getModelo())
		.where("cdveiculotipo = ?", filtro.getTipo() == null ? null : filtro.getTipo().getCdveiculotipo())
		.where("visual = ?", filtro.getVisual())
		.whereIn("veiculo.cdveiculo", filtro.getWhereInVeiculo() != null && !"".equals(filtro.getWhereInVeiculo()) ? filtro.getWhereInVeiculo() : "")
		.orderBy(order)
		.list();
	}
}





