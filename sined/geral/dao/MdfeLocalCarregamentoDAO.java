package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.MdfeLocalCarregamento;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MdfeLocalCarregamentoDAO extends GenericDAO<MdfeLocalCarregamento>{

	public List<MdfeLocalCarregamento> findByMdfe(Mdfe mdfe){
		return query()
			.select("mdfeLocalCarregamento.cdMdfeLocalCarregamento, municipio.nome, municipio.cdmunicipio, municipio.cdibge, "+
					"uf.cduf, uf.sigla, uf.nome")
			.join("mdfeLocalCarregamento.municipio municipio")
			.join("municipio.uf uf")
			.where("mdfeLocalCarregamento.mdfe = ?", mdfe)
			.list();
	}
}
