package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaAgrupamento;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRetornoDocumentoBean;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class BancoConfiguracaoRemessaAgrupamentoDAO extends GenericDAO<BancoConfiguracaoRemessaAgrupamento> {

	
	/**
	 * Busca os agrupamentos para o processamento de arquivo de retorno configurável
	 *
	 * @param listaDocumento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/10/2014
	 */
	public List<BancoConfiguracaoRemessaAgrupamento> findForRetornoConfiguravel(List<ArquivoConfiguravelRetornoDocumentoBean> listaDocumento) {
		return query()
					.select("bancoConfiguracaoRemessaAgrupamento.identificador, bancoConfiguracaoRemessaAgrupamento.cdbancoconfiguracaoremessaagrupamento, " +
							"documento.cddocumento, pessoa.nome, documento.outrospagamento, documento.somentenossonumero, documento.dtvencimento, " +
							"aux_documento.valoratual, documentoacao.cddocumentoacao, documentoacao.nome, documento.nossonumero")
					.join("bancoConfiguracaoRemessaAgrupamento.documento documento")
					.leftOuterJoin("documento.pessoa pessoa")
					.leftOuterJoin("documento.documentoacao documentoacao")
					.leftOuterJoin("documento.conta conta")
					.join("documento.aux_documento aux_documento")
					.openParentheses()
					.whereIn("bancoConfiguracaoRemessaAgrupamento.identificador", CollectionsUtil.listAndConcatenate(listaDocumento, "usoempresaLong", ",")).or()
					.whereIn("bancoConfiguracaoRemessaAgrupamento.identificador", CollectionsUtil.listAndConcatenate(listaDocumento, "nossonumeroLong", ","))
					.closeParentheses()
					.list();
	}
	
}