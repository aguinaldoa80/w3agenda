package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Agendainteracaocontratomodelo;
import br.com.linkcom.sined.geral.bean.Contratotipo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AgendainteracaocontratomodeloDAO extends GenericDAO<Agendainteracaocontratomodelo>{

	public List<Agendainteracaocontratomodelo> findForCreationAgendainteracao(Contratotipo contratotipo){
		return query().select("agendainteracaocontratomodelo.referencia, agendainteracaocontratomodelo.diasparacomecar, agendainteracaocontratomodelo.descricao, " +
							"agendainteracaocontratomodelo.frequencia, agendainteracaocontratomodelo.pessoatipo, " +
							"atividadetipo.nome, atividadetipo.cdatividadetipo, periodicidade.nome, periodicidade.cdfrequencia")
					.leftOuterJoin("agendainteracaocontratomodelo.periodicidade periodicidade")
					.leftOuterJoin("agendainteracaocontratomodelo.atividadetipo atividadetipo")
					.where("agendainteracaocontratomodelo.contratotipo = ?", contratotipo)
					.list();
		
	}
}
