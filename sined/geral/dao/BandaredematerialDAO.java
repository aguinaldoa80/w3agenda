package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Bandaredematerial;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("bandaredematerial.material")
public class BandaredematerialDAO extends GenericDAO<Bandaredematerial>{
	
}
