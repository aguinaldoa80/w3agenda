package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.view.Vcontratoqtdelocacao;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VcontratoqtdelocacaoDAO extends GenericDAO<Vcontratoqtdelocacao> {

	/**
	 * Busca a lista para a atualiza��o autom�tica do per�odo de loca��o
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/05/2014
	 */
	public List<Vcontratoqtdelocacao> findForAtualizacao() {
		return query()
					.where("vcontratoqtdelocacao.dtfimlocacao <= ?", SinedDateUtils.currentDate())
					.where("vcontratoqtdelocacao.qtde_romaneio - vcontratoqtdelocacao.qtde_fechamento > 0")
					.where("exists (select 1 from Contrato c " +
							"join c.contratotipo ct " +
							"where c.cdcontrato = vcontratoqtdelocacao.cdcontrato " +
							"and (ct.naorenovarperiodolocacaoautomatico is null or ct.naorenovarperiodolocacaoautomatico = false) )")
					.orderBy("vcontratoqtdelocacao.cdcontrato")
					.list();
	}

	public boolean isHabilitadoParaRenovacao(Contrato contrato) {
		Long countNaoFechado = newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
//				.where("vcontratoqtdelocacao.dtfimlocacao <= ?", SinedDateUtils.currentDate())
				.where("vcontratoqtdelocacao.qtde_romaneio - vcontratoqtdelocacao.qtde_fechamento > 0")
				.where("vcontratoqtdelocacao.cdcontrato = ?", contrato.getCdcontrato())
				.unique();
		
		Long countExists = newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.where("vcontratoqtdelocacao.cdcontrato = ?", contrato.getCdcontrato())
				.unique();
		
		return (countExists > 0 && countNaoFechado > 0) || countExists.equals(0l);
	}
	
}
