package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaTipoPagamento;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class BancoConfiguracaoRemessaTipoPagamentoDAO extends GenericDAO<BancoConfiguracaoRemessaTipoPagamento> {

}
