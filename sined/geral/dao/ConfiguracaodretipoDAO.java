package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Configuracaodretipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ConfiguracaodretipoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("configuracaodretipo.descricao")
public class ConfiguracaodretipoDAO extends GenericDAO<Configuracaodretipo> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Configuracaodretipo> query, FiltroListagem _filtro) {
		ConfiguracaodretipoFiltro filtro = (ConfiguracaodretipoFiltro) _filtro;
		
		query
			.select("configuracaodretipo.cdconfiguracaodretipo, configuracaodretipo.descricao, " +
					"configuracaodretipo.configuracaodreseparacao, configuracaodretipo.configuracaodreoperacao")
			.whereLikeIgnoreAll("configuracaodretipo.descricao", filtro.getDescricao());
	}
	
}
