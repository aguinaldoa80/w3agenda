package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Arquivoemporium;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Emporiumvenda;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.service.EmporiumvendafinalizadoraService;
import br.com.linkcom.sined.geral.service.EmporiumvendaitemService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.EmporiumvendaBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.EmporiumvendaFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EmporiumvendaDAO extends GenericDAO<Emporiumvenda>{
	
	private EmporiumvendaitemService emporiumvendaitemService;
	private EmporiumvendafinalizadoraService emporiumvendafinalizadoraService;
	
	public void setEmporiumvendaitemService(
			EmporiumvendaitemService emporiumvendaitemService) {
		this.emporiumvendaitemService = emporiumvendaitemService;
	}

	public void setEmporiumvendafinalizadoraService(
			EmporiumvendafinalizadoraService emporiumvendafinalizadoraService) {
		this.emporiumvendafinalizadoraService = emporiumvendafinalizadoraService;
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Emporiumvenda> query, FiltroListagem _filtro) {
		EmporiumvendaFiltro filtro = (EmporiumvendaFiltro) _filtro;
		
		query
		.select("emporiumvenda.cdemporiumvenda, emporiumvenda.loja, emporiumvenda.pdv, emporiumvenda.numero_ticket, " +
				"emporiumvenda.data_hora, emporiumvenda.valor, venda.cdvenda")
			.leftOuterJoin("emporiumvenda.venda venda")
			.where("venda.cdvenda = ?", filtro.getVenda())
			.whereIn("venda.pedidovenda.cdpedidovenda", filtro.getPedidovenda())
			.where("emporiumvenda.pdv = ?", filtro.getPdv())
			.where("emporiumvenda.loja = ?", filtro.getLoja())
			.where("emporiumvenda.numero_ticket = ?", filtro.getNumero_ticket())
			.where("emporiumvenda.data_hora >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getData1()))
			.where("emporiumvenda.data_hora <= ?", SinedDateUtils.dateToTimestampFinalDia(filtro.getData2()))
			.where("emporiumvenda.valor >= ?", filtro.getValor1())
			.where("emporiumvenda.valor <= ?", filtro.getValor2())
			.orderBy("emporiumvenda.data_hora desc")
			;
		
		if(filtro.getVendacriada() != null && filtro.getVendacriada()){
			query.where("venda.cdvenda is not null");
		} else if(filtro.getVendacriada() != null && !filtro.getVendacriada()){
			query.where("venda.cdvenda is null");
		}
	}
	
	@Override
	public Emporiumvenda loadForEntrada(Emporiumvenda bean) {
		Emporiumvenda form = super.loadForEntrada(bean);
		
		form.setListaEmporiumvendaitem(emporiumvendaitemService.findByEmporiumvenda(bean));
		form.setListaEmporiumvendafinalizadora(emporiumvendafinalizadoraService.findByEmporiumvenda(bean));
		
		return form;
	}
	
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaEmporiumvendaitem");
		save.saveOrUpdateManaged("listaEmporiumvendafinalizadora");
	}
	
	public boolean haveTicketDia(Emporiumpdv emporiumpdv, Date dataInicio) {
		Integer codigoemporium = null;
		try{
			codigoemporium =Integer.parseInt(emporiumpdv.getCodigoemporium());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newQueryBuilderSined(Long.class)
					.from(Emporiumvenda.class)
					.select("count(*)")
					.setUseTranslator(false)
					.where("emporiumvenda.pdv = ?", codigoemporium != null ? codigoemporium.toString() : null)
					.where("emporiumvenda.data_hora >= ?", SinedDateUtils.dateToTimestampInicioDia(dataInicio))
					.where("emporiumvenda.data_hora <= ?", SinedDateUtils.dateToTimestampFinalDia(dataInicio))
					.unique() > 0;
	}
	
	/**
	 * Verifica se existe algum ticket cadastrado.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @param numeroTicket 
	 * @param pdv 
	 * @param loja
	 * @since 15/04/2013
	 */
	public Emporiumvenda loadTicketCadastrado(String loja, String pdv, String numeroTicket) {
		return query()
				.select("emporiumvenda.cdemporiumvenda, venda.cdvenda")
				.leftOuterJoin("emporiumvenda.venda venda")
				.where("emporiumvenda.loja = ?", loja)
				.where("emporiumvenda.pdv = ?", pdv)
				.where("emporiumvenda.numero_ticket = ?", numeroTicket)
				.unique();
	}

	public void updateVenda(Emporiumvenda emporiumvenda, Venda venda) {
		getHibernateTemplate().bulkUpdate("update Emporiumvenda e set e.venda = ? where e.cdemporiumvenda = ?", new Object[]{
				venda, emporiumvenda.getCdemporiumvenda()
		});
	}

	public List<Emporiumvenda> findPendentes(Arquivoemporium arquivoemporium, String loja, String whereInEmporiumvenda) {
		QueryBuilder<Emporiumvenda> query = query()
					.leftOuterJoinFetch("emporiumvenda.listaEmporiumvendafinalizadora listaEmporiumvendafinalizadora")
					.where("emporiumvenda.venda is null")
					.where("emporiumvenda.loja = ?", loja)
					.whereIn("emporiumvenda.cdemporiumvenda", whereInEmporiumvenda);
		
		if(arquivoemporium != null){
			query
				.leftOuterJoin("emporiumvenda.listaEmporiumvendaarquivo listaEmporiumvendaarquivo")
				.where("listaEmporiumvendaarquivo.arquivoemporium = ?", arquivoemporium);
		}
		return query.list();
	}
	
	public List<Emporiumvenda> findForValidacao(String whereIn) {
		return query()
				.select("emporiumvenda.cdemporiumvenda, emporiumvenda.numero_ticket, venda.cdvenda ")
				.leftOuterJoin("emporiumvenda.venda venda")
				.whereIn("emporiumvenda.cdemporiumvenda", whereIn)
				.list();
	}
	
	public List<Emporiumvenda> findForSped(Emporiumpdv emporiumpdv, Date dtinicio, Date dtfim) {
		Integer codigoemporium = null;
		try{
			if(emporiumpdv != null){
				codigoemporium = Integer.parseInt(emporiumpdv.getCodigoemporium());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return query()
					.select("emporiumvendaitem.cdemporiumvendaitem, emporiumvendaitem.valortotal, emporiumvendaitem.quantidade, emporiumvendaitem.sequencial, emporiumvendaoutro.loja, " +
							"emporiumvendaitem.produto_id, emporiumvenda.cdemporiumvenda, emporiumvenda.data_hora, emporiumvenda.valor_desconto, emporiumvenda.valor, emporiumvenda.numero_ticket, emporiumvenda.loja, " +
							"material.cdmaterial, material.nome, material.ncmcompleto, tributacaoecf.cdtributacaoecf, unidademedida.cdunidademedida, unidademedida.simbolo, tributacaoecf.cstpis, emporiumvendaoutro.data_hora, " +
							"tributacaoecf.cstcofins, tributacaoecf.aliqpis, tributacaoecf.aliqcofins, emporiumvendaitem.legenda, emporiumvendaitem.valorunitario, emporiumvendaitem.desconto, vcontacontabil.identificador")
					.join("emporiumvenda.listaEmporiumvendaitem emporiumvendaitem")
					.join("emporiumvendaitem.emporiumvenda emporiumvendaoutro")
					.leftOuterJoin("emporiumvendaitem.material material")
					.leftOuterJoin("material.contaContabil contacontabil")
					.leftOuterJoin("contacontabil.vcontacontabil vcontacontabil")
					.leftOuterJoin("material.unidademedida unidademedida")
					.leftOuterJoin("material.tributacaoecf tributacaoecf")
					.where("emporiumvenda.pdv = ?", codigoemporium != null ? codigoemporium.toString() : null)
					.where("emporiumvenda.data_hora >= ?", SinedDateUtils.dateToTimestampInicioDia(dtinicio))
					.where("emporiumvenda.data_hora <= ?", SinedDateUtils.dateToTimestampFinalDia(dtfim))
					.orderBy("emporiumvenda.data_hora, emporiumvendaitem.sequencial")
					.list();
	}
	
	public List<Emporiumvenda> findForAcerto(Date dtinicio, Date dtfim) {
		return query()
					.select("emporiumvendaitem.cdemporiumvendaitem, emporiumvendaitem.valortotal, emporiumvendaitem.quantidade, emporiumvendaoutro.data_hora, emporiumvendaoutro.numero_ticket, emporiumvendaoutro.cdemporiumvenda, " +
							"emporiumvendaitem.produto_id, emporiumvendaitem.produto_desc, emporiumvenda.cdemporiumvenda, emporiumvenda.data_hora, emporiumvenda.valor_desconto, emporiumvendaoutro.pdv, " +
							"emporiumvenda.valor, emporiumvendaitem.legenda, emporiumvendaitem.valorunitario, emporiumvendaitem.desconto, emporiumvendaoutro.pdv, emporiumvendaitem.legenda, emporiumvendaoutro.loja")
					.join("emporiumvenda.listaEmporiumvendaitem emporiumvendaitem")
					.join("emporiumvendaitem.emporiumvenda emporiumvendaoutro")
					.where("emporiumvenda.data_hora >= ?", SinedDateUtils.dateToTimestampInicioDia(dtinicio))
					.where("emporiumvenda.data_hora <= ?", SinedDateUtils.dateToTimestampFinalDia(dtfim))
					.orderBy("emporiumvenda.pdv, emporiumvenda.data_hora, emporiumvendaitem.sequencial")
					.list();
	}

	public boolean haveVenda(Venda venda) {
		return newQueryBuilderSined(Long.class)
					.from(Emporiumvenda.class)
					.select("count(*)")
					.setUseTranslator(false)
					.where("emporiumvenda.venda = ?", venda)
					.unique() > 0;
	}

	public boolean fromVendaemporium(Nota nota) {
		return newQueryBuilderSined(Long.class)
				.from(Emporiumvenda.class)
				.select("count(*)")
				.setUseTranslator(false)
				.join("emporiumvenda.venda venda")
				.join("venda.listaNotavenda notavenda")
				.where("notavenda.nota = ?", nota)
				.unique() > 0;
	}

	@SuppressWarnings("unchecked")
	public Nota getNotaFromAcertoByTicket(String numeroTicket, String pdv, String loja) {
		try{
			String sql = "select cdnota from emporiumnotavenda where numero_ticket = '" + numeroTicket + "' and pdv = '" + pdv + "' and loja = '" + loja + "'" ;
			
			SinedUtil.markAsReader();
			List<Nota> listaNota = getJdbcTemplate().query(sql, 
				new RowMapper() {
					public Nota mapRow(ResultSet rs, int rowNum) throws SQLException {
						return new Nota(rs.getInt("CDNOTA"));
					}
				}
			);
			
			if(listaNota != null && listaNota.size() > 0) 
				return listaNota.get(0);
			
		} catch (Exception e) {}
		
		return null;
	}

	@SuppressWarnings("unchecked")
	public boolean existeVendaCupom(String numeroTicket, String loja, String pdv, Empresa empresavenda, Double valor) {
		if(numeroTicket == null || loja == null || pdv == null || empresavenda == null || valor == null)
			return false;
		
		String sql = "SELECT v.numero_ticket, v.loja, v.pdv, v.cdvenda " +
						"FROM emporiumvenda v " +
//						"JOIN venda ve ON (ve.cdvenda = v.cdvenda AND ve.cdvendasituacao <> " + Vendasituacao.CANCELADA.getCdvendasituacao() + ") " +
						"WHERE v.numero_ticket = '" + numeroTicket + "'  " +
						"AND v.loja = '" + loja + "'  " +
						"AND v.pdv = '" + pdv + "'  " +
						"AND v.valor = " + valor + " " +
						"AND v.cdvenda is not null " +
						"ORDER BY v.numero_ticket;";

		SinedUtil.markAsReader();
		List<EmporiumvendaBean> lista = getJdbcTemplate().query(sql, 
				new RowMapper() {
					public EmporiumvendaBean mapRow(ResultSet rs, int rowNum) throws SQLException {
						return new EmporiumvendaBean(rs.getInt("cdvenda"), rs.getString("numero_ticket"));
					}
				}
			);
		
		if(SinedUtil.isListNotEmpty(lista)){
			StringBuilder whereInVenda = new StringBuilder();
			for(EmporiumvendaBean genericBean : lista){
				if(genericBean.getCdvenda() != null && genericBean.getCdvenda() > 0){
					whereInVenda.append(genericBean.getCdvenda().toString()).append(",");
				}
			}
			
			if(whereInVenda.length() > 0){
				sql = "SELECT count(*) as qtde_venda " +
							 "FROM venda v " +
							 "WHERE v.cdvenda in ( " + whereInVenda.substring(0, whereInVenda.length()-1) + " ) " +
							 "AND v.cdvendasituacao <> " + Vendasituacao.CANCELADA.getCdvendasituacao();

				SinedUtil.markAsReader();
				Integer qtde_venda = (Integer) getJdbcTemplate().queryForObject(sql.toString(), 
						new RowMapper() {
							public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
								return rs.getInt("qtde_venda");
							}
						});
				
				return qtde_venda != null && qtde_venda > 0;
			}
		}
		return false;
	}

	public List<Emporiumvenda> isExisteECF(String ids, String origem) {
		
		if(origem.equals("venda")){
			return query().select("emporiumvenda.cdemporiumvenda, emporiumvenda.numero_ticket,venda.cdvenda")
					.leftOuterJoin("emporiumvenda.venda venda")
					.whereIn("venda.cdvenda", ids)
					.list();
		}
		if(origem.equals("pedidovenda")){
			return query().select("emporiumvenda.cdemporiumvenda, emporiumvenda.numero_ticket")
					.whereIn("emporiumvenda.pedidovenda", ids)
					.list();
		}
		
		return null;
	}

	public List<Emporiumvenda> findForCriarNotas(){
		return query()
				.join("emporiumvenda.venda venda")
				.where("emporiumvenda.gerar_nota = true")
				.where("not exists(select 1 from Notafiscalproduto nota where nota.emporiumVenda = emporiumvenda)")
				.list();
	}
	
	public void updateNotaGerada(Emporiumvenda emporiumVenda){
		if(Util.objects.isPersistent(emporiumVenda)){
			getHibernateTemplate().bulkUpdate("update Emporiumvenda ev set ev.gerar_nota = false where ev = ?", emporiumVenda);
		}
	}
}
