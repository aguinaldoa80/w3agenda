package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Acao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.AcaoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AcaoDAO extends GenericDAO<Acao> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Acao> query,FiltroListagem _filtro) {
		AcaoFiltro filtro = (AcaoFiltro) _filtro;
		
		query
			.whereLikeIgnoreAll("acao.descricao", filtro.getDescricao())
			.whereLikeIgnoreAll("acao.key", filtro.getKey())
			.orderBy("acao.descricao")
		;
		
	}
	
}
