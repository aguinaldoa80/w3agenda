package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Versao;
import br.com.linkcom.sined.geral.bean.Versaonovidade;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VersaonovidadeDAO extends GenericDAO<Versaonovidade> {
	
	/* singleton */
	private static VersaonovidadeDAO instance;
	public static VersaonovidadeDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(VersaonovidadeDAO.class);
		}
		return instance;
	}
	
	/**
	 * M�todo que carrega as novidades da vers�o
	 * 
	 * @param versao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Versaonovidade> getNovidadesVersao(Versao versao) {
		if(versao == null || versao.getCdversao() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("versaonovidade.cdversaonovidade, versaonovidade.descricao")
			.join("versaonovidade.versao versao")
			.where("versao = ?", versao)
			.list();
	}
}
