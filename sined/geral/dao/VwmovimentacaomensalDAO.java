package br.com.linkcom.sined.geral.dao;


import java.util.Iterator;
import java.util.List;

import br.com.linkcom.sined.geral.bean.view.Vwmovimentacaomensal;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VwmovimentacaomensalDAO extends GenericDAO<Vwmovimentacaomensal> {

	/**
	 * Retorna os totais das movimentação de receitas e despesas CONCILIADAS
	 * nos meses/ano informados no parâmetro.
	 * 
	 * @param listaMesAno
	 * @return
	 */
	public List<Vwmovimentacaomensal> findByMesAno(List<String> listaMesAno) {
		
		if (listaMesAno == null || listaMesAno.isEmpty()) {
			throw new SinedException("Parâmetros inválidos!");
		}
		
		StringBuilder sbMesAno = new StringBuilder();
		for (Iterator<String> iter = listaMesAno.iterator(); iter.hasNext();) {
			String mesAno = iter.next();
			sbMesAno.append("'").append(mesAno).append("'");
			if(iter.hasNext()){
				sbMesAno.append(",");
			}
		}
		
		return query()
			.select("vwmovimentacaomensal.mesano, vwmovimentacaomensal.valor," +
					"tipooperacao.cdtipooperacao, vwmovimentacaomensal.id")
			.join("vwmovimentacaomensal.tipooperacao tipooperacao")
			.whereIn("vwmovimentacaomensal.mesano", sbMesAno.toString())
			.orderBy("vwmovimentacaomensal.id")
			.list();
	}

	
}
