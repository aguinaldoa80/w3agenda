package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Calendario;
import br.com.linkcom.sined.geral.bean.Calendarioprojeto;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CalendarioprojetoDAO extends GenericDAO<Calendarioprojeto> {

	/**
	 * Verifica se existe outro calendario com aquele projeto.
	 *
	 * @param projeto
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean verificaOutroCalendario(Projeto projeto, Calendario bean) {
		if(projeto == null || projeto.getCdprojeto() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
				.from(Calendarioprojeto.class)
				.select("count(*)")
				.join("calendarioprojeto.projeto projeto")
				.join("calendarioprojeto.calendario calendario")
				.where("projeto = ?", projeto);
		
		if(bean != null && bean.getCdcalendario() != null){
			query.where("calendario.cdcalendario <> ?", bean.getCdcalendario());
		}
		
		return query.unique() > 0;
	}
	

}
