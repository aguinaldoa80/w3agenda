package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Projetotipo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("projetotipo.nome")
public class ProjetotipoDAO extends GenericDAO<Projetotipo>{

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaProjetotipoitem");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Projetotipo> query) {
		query.leftOuterJoinFetch("projetotipo.listaProjetotipoitem listaProjetotipoitem");
	}

	/**
	* M�todo que busca os tipos de projeto para o flex
	*
	* @return
	* @since 11/08/2016
	* @author Luiz Fernando
	*/
	public List<Projetotipo> findForComboFlex() {
		return query()
				.select("projetotipo.cdprojetotipo, projetotipo.nome")
				.list();
	}
	
}
