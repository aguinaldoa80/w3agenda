package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Ordemcompraentregamaterial;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.report.filtro.LivroregistroentradaFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EntregamaterialDAO extends GenericDAO<Entregamaterial> {

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaOrdemcompraentregamaterial");
		save.saveOrUpdateManaged("listaEntregamateriallote");
	}
	
	/**
	 * M�todo que retorna quantidade de material entregue referente a 
	 * ordem de compra
	 * @param ordemcompramaterial
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Double getQtdEntregueDoMaterialNaOrdemCompra(Ordemcompramaterial ordemcompramaterial) {
		return newQueryBuilderSined(Double.class)
					.select("sum(qtde_unidadeprincipal(material.cdmaterial, unidademedidacomercial.cdunidademedida, ordemcompraentregamaterial.qtde))")
					.from(Ordemcompraentregamaterial.class)
					.setUseTranslator(false)
					.join("ordemcompraentregamaterial.ordemcompramaterial ordemcompramaterial")
					.join("ordemcompraentregamaterial.entregamaterial entregamaterial")
					.join("entregamaterial.entregadocumento entregadocumento")
					.join("entregadocumento.entrega entrega")
					.join("entregamaterial.material material")
					.join("entregamaterial.unidademedidacomercial unidademedidacomercial")
					.where("ordemcompramaterial = ?", ordemcompramaterial)
					.where("entrega.dtcancelamento is null")
					.unique();
	}

	/**
	 * Busca a lista de material da entrega a partir da entrega passada por par�metro.
	 *
	 * @param e
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Entregamaterial> findByEntrega(Entrega e) {
		if (e == null || e.getCdentrega() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return 
			query()
				.select("entregamaterial.qtde, entregamaterial.valorunitario")
				.where("entregamaterial.entrega = ?",e)
				.list();
	}
	
	
	
	@SuppressWarnings("unchecked")
	public Double getQtdeByEntregaMaterial(Entregamaterial entregamaterial){
		
		if(entregamaterial == null || entregamaterial.getOcm() == null || entregamaterial.getOcm().getCdordemcompramaterial() == null){
//			throw new SinedException("O bean ordemcompramaterial n�o pode ser nulo.");
			return 0.0;
		}
		
		String sql = 	"SELECT (qtde_unidadeprincipal(OM.cdmaterial, OM.cdunidademedida, OM.QTDPEDIDA)*OM.QTDEFREQUENCIA)-SUM(qtde_unidadeprincipal(em.cdmaterial, em.cdunidademedidacomercial, OCEM.QTDE)) AS RESTO " +
						"FROM ORDEMCOMPRAENTREGAMATERIAL OCEM " +
						"JOIN ORDEMCOMPRAMATERIAL OM ON OM.CDORDEMCOMPRAMATERIAL = OCEM.CDORDEMCOMPRAMATERIAL " +
						"JOIN ENTREGAMATERIAL EM ON EM.CDENTREGAMATERIAL = OCEM.CDENTREGAMATERIAL " +
						"JOIN ENTREGADOCUMENTO ED ON ED.CDENTREGADOCUMENTO = EM.CDENTREGADOCUMENTO " +
						"JOIN AUX_ENTREGA E ON E.CDENTREGA = ED.CDENTREGA " +
						"WHERE E.SITUACAO <> 9 " +
						"AND OM.CDORDEMCOMPRAMATERIAL = " + entregamaterial.getOcm().getCdordemcompramaterial() + " " +
						(entregamaterial.getCdentregamaterial() != null ? "AND EM.CDENTREGAMATERIAL <>" + entregamaterial.getCdentregamaterial() + " " : "") +
						"GROUP BY  OM.CDORDEMCOMPRAMATERIAL, OM.QTDPEDIDA, OM.QTDEFREQUENCIA";

		SinedUtil.markAsReader();
		List<Double> listaUpdate = getJdbcTemplate().query(sql, 
			new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return rs.getDouble("RESTO");
				}
	
			});
		
		if(listaUpdate != null && listaUpdate.size() > 0){
			return listaUpdate.get(0);
		} else {
			String sql2 = 	"SELECT qtde_unidadeprincipal(OM.cdmaterial, OM.cdunidademedida, OM.QTDPEDIDA) * OM.QTDEFREQUENCIA AS RESTO " +
							"FROM ORDEMCOMPRAMATERIAL OM " +
							"WHERE OM.CDORDEMCOMPRAMATERIAL = " + entregamaterial.getOcm().getCdordemcompramaterial();

			SinedUtil.markAsReader();
			List<Double> listaUpdate2 = getJdbcTemplate().query(sql2, 
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						return rs.getDouble("RESTO");
					}
				
				});
			
			if(listaUpdate2 != null && listaUpdate2.size() > 0){
				return listaUpdate2.get(0);
			} else {
				return 0.0;
			}
			
		}
	}

	/**
	 * M�todo que busca as informa��es de unidade de medida dos materiais e unidademedidacomercial da entrega
	 *
	 * @param entrega
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregamaterial> findForLancarEstoqueConverterUnidademedida(Entrega entrega, Material material) {
		if(entrega == null || entrega.getCdentrega() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("entregamaterial.cdentregamaterial, material.cdmaterial, unidademedida.cdunidademedida, entregamaterial.qtde," +
						"unidademedidacomercial.cdunidademedida, loteestoque.cdloteestoque," +
						"entregamaterial.valoripi,entregamaterial.valorfrete,entregamaterial.valoricmsst,entregamaterial.valorseguro,"+
						"entregamaterial.valoroutrasdespesas,entregamaterial.valordesconto, cfop.cdcfop, cfop.codigo," +
						"listaEntregamateriallote.cdentregamateriallote")
				.join("entregamaterial.entregadocumento entregadocumento")
				.join("entregadocumento.entrega entrega")
				.join("entregamaterial.material material")
				.join("material.unidademedida unidademedida")
				.join("entregamaterial.unidademedidacomercial unidademedidacomercial")
				.leftOuterJoin("entregamaterial.loteestoque loteestoque")
				.leftOuterJoin("entregamaterial.cfop cfop")
				.leftOuterJoin("entregamaterial.listaEntregamateriallote listaEntregamateriallote")
				.where("entrega = ?", entrega)
				.where("material = ?", material)
				.list();
	}

	/**
	 * M�todo que atualiza a qtde utilizada do entregamaterial
	 *
	 * @param cdentregamaterial
	 * @param qtdeutilizada
	 * @param valorsaldoretido
	 * @author Luiz Fernando
	 */
	public void atualizaQtdeutilizada(Integer cdentregamaterial, Double qtdeutilizada) {
		if(cdentregamaterial != null && qtdeutilizada != null){
			getHibernateTemplate().bulkUpdate("update Entregamaterial set qtdeutilizada = ? where cdentregamaterial = ?",
					new Object[]{qtdeutilizada, cdentregamaterial});			
		}		
	}

	/**
	 * M�todo que busca a qtdeutilizada da entregamaterial
	 *
	 * @param cdentregamaterial
	 * @return
	 * @author Luiz Fernando
	 */
	public Entregamaterial findQtdeutilizada(Integer cdentregamaterial) {
		if(cdentregamaterial == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("entregamaterial.cdentregamaterial, entregamaterial.qtdeutilizada")			
			.where("entregamaterial.cdentregamaterial = ?", cdentregamaterial)
			.unique();
	}

	/**
	 * M�todo que busca �ltimas compras 
	 *
	 * @param cdmaterial
	 * @param cdempresa
	 * @param limite
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregamaterial> findUltimascomprasByMaterial(Integer cdmaterial, Integer cdempresa, Integer limite) {
		if(cdmaterial == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("entregamaterial.qtde, entregamaterial.valorunitario, entregadocumento.dtentrada, fornecedor.cdpessoa, fornecedor.nome, entrega.dtentrega," +
						"listaOrdemcompraentrega.cdordemcompraentrega, ordemcompra.cdordemcompra ")
				.join("entregamaterial.material material")
				.join("entregamaterial.entregadocumento entregadocumento")
				.join("entregadocumento.fornecedor fornecedor")
				.leftOuterJoin("entregadocumento.empresa empresa")
				.join("entregadocumento.entrega entrega")
				.leftOuterJoin("entrega.empresa empresaentrega")
				.join("entrega.aux_entrega aux_entrega")
				.leftOuterJoin("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")
				.leftOuterJoin("listaOrdemcompraentrega.ordemcompra ordemcompra")
				.where("material.cdmaterial = ?", cdmaterial)
				.where("entrega.dtentrega is not null")
				.openParentheses()
					.where("empresa.cdpessoa = ?", cdempresa)
					.or()
					.where("empresaentrega.cdpessoa = ?", cdempresa)
				.closeParentheses()
				.setMaxResults(limite)
				.orderBy("entrega.dtentrega desc, entregadocumento.dtentrada")
				.where("aux_entrega.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
				.list();
	}
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Entregamaterial> findForPdfListagem(String whereIn){
		return query()
				.select("entregamaterial.cdentregamaterial, entregamaterial.qtde, entregamaterial.valorunitario, entregamaterial.valorbcicms, " +
						"entregamaterial.icms, entregamaterial.valoricms, entregamaterial.valorbcipi, entregamaterial.ipi, entregamaterial.valoripi, entregamaterial.valorfrete," +
						"entregamaterial.valorFiscalIcms, entregamaterial.valorFiscalIpi, entregadocumento.cdentregadocumento, entregadocumento.dtentrada, " +
						"entregadocumento.serie, entregadocumento.numero, entregadocumento.dtemissao, " +
						"fornecedor.nome, fornecedor.cnpj, fornecedor.inscricaoestadual, fornecedor.identificador, " +
						"uf.sigla, cfop.codigo, entregamaterial.valoricmsst, entregamaterial.valoroutrasdespesas, entregamaterial.valordesconto, " +
						"entregamaterial.valorseguro, modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.nome")
				.join("entregamaterial.entregadocumento entregadocumento")
				.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
				.join("entregadocumento.fornecedor fornecedor")
				.leftOuterJoin("fornecedor.listaEndereco listaEndereco")
				.leftOuterJoin("listaEndereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("entregamaterial.cfop cfop")
				.whereIn("entregadocumento.cdentregadocumento", whereIn)
				.list();
	}
	
	/**
	 * M�todo que remove os elementos de entregamaterial que n�o mais perten�am a uma entrega.
	 * 
	 * @param cdentrega
	 * @param whereNotIn
	 * @author Rafael Salvio
	 */
	public void deleteAllFromEntregaNotInWhereIn(Integer cdentrega, String whereNotIn){
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM entregamaterial WHERE cdentrega = "+cdentrega);
		if(whereNotIn != null){
			sql.append(" AND cdentregamaterial NOT IN ("+whereNotIn+")");
		}
		
		getJdbcTemplate().execute(sql.toString());
	}
	
	/**
	 * M�todo que carregaos dados de entregamaterial, material e loteestoque para integra��o com WMS.
	 * 
	 * @param entregamaterial
	 * @return
	 * @author Rafael Salvio
	 */
	public Entregamaterial carregaEntregamaterialWMS(Entregamaterial entregamaterial){
		if(entregamaterial == null || entregamaterial.getCdentregamaterial() == null){
			return null;
		}
		
		return query()
				.select("entregamaterial.cdentregamaterial, entregamaterial.qtde, entregamaterial.valorunitario, loteestoque.cdloteestoque, " +
						"loteestoque.numero, material.cdmaterial, material.nome, material.qtdeunidade, material.codigofabricante, " +
						"materialgrupo.cdmaterialgrupo, materialgrupo.nome, produto.pesobruto, produto.cdmaterial, produto.altura, " +
						"produto.largura, produto.comprimento, materialcor.cdmaterialcor, materialmestregrade.cdmaterial, " +
						"unidademedida.cdunidademedida, unidademedida.simbolo, " +
						"listaEntregamateriallote.qtde, loteestoqueEML.cdloteestoque, loteestoqueEML.numero ")
				.leftOuterJoin("entregamaterial.loteestoque loteestoque")
				.leftOuterJoin("entregamaterial.material material")
				.leftOuterJoin("material.materialcor materialcor")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("material.materialproduto produto")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("entregamaterial.listaEntregamateriallote listaEntregamateriallote")
				.leftOuterJoin("listaEntregamateriallote.loteestoque loteestoqueEML")
				.where("entregamaterial = ?", entregamaterial)
				.unique();
	}
	
	/**
	 * Busca todas as entradas para o relat�rio de livro de registro de entrada.
	 * 
	 * @return
	 * @author Giovane Freitas
	 */
	public List<Entregamaterial> findForLivroEntrada(LivroregistroentradaFiltro filtro){
		return querySined()
				.select("entregamaterial.cdentregamaterial, entregamaterial.qtde, entregamaterial.valorunitario, entregamaterial.valorbcicms, " +
						"entregamaterial.icms, entregamaterial.valoricms, entregamaterial.valorbcipi, entregamaterial.ipi, entregamaterial.valoripi, entregamaterial.valorfrete," +
						"entregamaterial.valorFiscalIcms, entregamaterial.valorFiscalIpi, entregadocumento.cdentregadocumento, entregadocumento.dtentrada, " +
						"entregadocumento.serie, entregadocumento.numero, entregadocumento.dtemissao, " +
						"fornecedor.nome, fornecedor.cnpj, fornecedor.inscricaoestadual, fornecedor.identificador, entregamaterial.valorFiscalIcms, entregamaterial.valorFiscalIpi, " +
						"uf.sigla, cfop.codigo, entregamaterial.valoricmsst, entregamaterial.icmsst, entregamaterial.valorbcicmsst, entregamaterial.valoroutrasdespesas, entregamaterial.valordesconto, " +
						"entregamaterial.valorseguro, modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.nome, modelodocumentofiscal.codigo")
				.join("entregamaterial.entregadocumento entregadocumento")
				.join("entregadocumento.fornecedor fornecedor")
				.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
				.leftOuterJoin("entregadocumento.entrega entrega")
				.leftOuterJoin("entrega.aux_entrega aux_entrega")
				.leftOuterJoin("fornecedor.listaEndereco listaEndereco")
				.leftOuterJoin("listaEndereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("entregamaterial.cfop cfop")
				.where("modelodocumentofiscal.livroentrada = ?", Boolean.TRUE)
				.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
				.where("entregadocumento.empresa = ?",filtro.getEmpresa())
				.where("entregadocumento.dtentrada >= ?",filtro.getDtinicio())
				.where("entregadocumento.dtentrada <= ?",filtro.getDtfim())
				.list();
	}
	
	public List<Entregamaterial> findForIndustrializacaoRetorno(String whereIn, String whereInVenda){
		return query()
					.select("entregamaterial.cdentregamaterial, entregamaterial.valorunitario, entregadocumento.cdentregadocumento, entregadocumento.numero," +
							"entregadocumento.dtentrada, entregadocumento.dtemissao, material.cdmaterial, material.nome, entregamaterial.qtde")
					.join("entregamaterial.entregadocumento entregadocumento")
					.join("entregamaterial.material material")
					.join("entregamaterial.pedidovendamaterial pedidovendamaterial")
					.join("pedidovendamaterial.pedidovenda pedidovenda")
					.join("pedidovenda.pedidovendatipo pedidovendatipo")
					.where("pedidovendatipo.referenciarnotaentradananotasaida = ?", Boolean.TRUE)
					.whereIn("entregamaterial.cdentregamaterial", whereIn)
					.where("pedidovenda.cdpedidovenda in (select pv.cdpedidovenda from Venda v join v.pedidovenda pv where v.cdvenda in (" + whereInVenda + "))", (whereInVenda!=null && !whereInVenda.equals("")))
					.where("material.cdmaterial in (select m.cdmaterial from Vendamaterial vm join vm.material m where vm.venda.cdvenda in (" + whereInVenda + "))", (whereInVenda!=null && !whereInVenda.equals("")))
					.orderBy("coalesce(entregadocumento.dtentrada, entregadocumento.dtemissao), material.nome")
					.list();
	}	
	
	public List<Entregamaterial> findForCTRC(String whereInCdentregadocumento, String whereNotInCdentregamaterial){
		return query()
				.select("entregamaterial.cdentregamaterial, entregadocumento.cdentregadocumento, material.cdmaterial, material.identificacao," +
						"material.nome, unidademedida.simbolo, entregamaterial.qtde, entregamaterial.valorunitario")
				.join("entregamaterial.entregadocumento entregadocumento")
				.join("entregamaterial.material material")
				.join("material.unidademedida unidademedida")
				.whereIn("entregadocumento.cdentregadocumento", whereInCdentregadocumento)
				.where("entregamaterial.cdentregamaterial not in (" + whereNotInCdentregamaterial + ")", (whereNotInCdentregamaterial!=null && !whereNotInCdentregamaterial.equals("")))
				.list();
	}
	
	/**
	* M�todo que busca o �ltimo valor de compra do material
	*
	* @param material
	* @param fornecedor
	* @return
	* @since 08/01/2015
	* @author Luiz Fernando
	*/
	public List<Entregamaterial> findByFornecedorMaterial(Material material, Fornecedor fornecedor) {
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		if (fornecedor == null || fornecedor.getCdpessoa() == null) {
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		}
		return query()
					.select("entregamaterial.valorunitario, entregadocumento.dtemissao, entregadocumento.dtentrada ")
					.setMaxResults(1)
					.join("entregamaterial.entregadocumento entregadocumento")
					.join("entregadocumento.entrega entrega")
					.join("entregamaterial.material material")
					.join("entregadocumento.fornecedor fornecedor")
					.join("entrega.aux_entrega aux_entrega")
					.leftOuterJoin("entregamaterial.cfop cfop")
					.where("material = ?", material)
					.where("fornecedor = ?", fornecedor)
					.openParentheses()
						.where("cfop is null")
						.or()
						.openParentheses()
							.where("cfop.codigo <> '1910'")
							.where("cfop.codigo <> '2910'")
						.closeParentheses()
					.closeParentheses()
					.where("entregamaterial.valorunitario is not null")
					.where("aux_entrega.situacaosuprimentos <> ? ", Situacaosuprimentos.CANCELADA)
					.orderBy("entregadocumento.dtemissao desc, entregadocumento.dtentrada desc, entregamaterial.cdentregamaterial desc")
					.list();
	}
	
	/**
	* Busca os registros de invent�rio para o SPED
	*
	* @param filtro
	* @return
	* @since 19/01/2015
	* @author Jo�o Vitor
	*/
	public List<Entregamaterial> findForSpedRegistroK250(SpedarquivoFiltro filtro) {
		if(filtro == null)
			throw new SinedException("Par�metro inv�lido");
		
		return query()
				.select("entregamaterial.cdentregamaterial, entregadocumento.cdentregadocumento, entregadocumento.dtemissao, material.cdmaterial, material.identificacao," +
						"entregamaterial.qtde, listaProducaoMaterial.cdmaterial")
				.join("entregamaterial.entregadocumento entregadocumento")
				.join("entregadocumento.naturezaoperacao naturezaoperacao")
				.join("entregamaterial.material material")
				.join("material.listaProducao listaProducao")
				.join("listaProducao.material listaProducaoMaterial")
				.where("naturezaoperacao.retornoindustrializacao = ?", Boolean.TRUE)
				.where("entregadocumento.dtemissao <= ?", filtro.getDtinicio())
				.where("entregadocumento.dtemissao >= ?", filtro.getDtfim())
				.list();
	}
	
	/**
	 * @param numero
	 * @return
	 * @since 28/08/2015
	 * @author Andrey Leonardo
	 */
	public List<Entregamaterial> findForVincularNotaFiscalProdutoItem(Notafiscalprodutoitem notafiscalprodutoitem){
		return query()
		.select("entregamaterial.cdentregamaterial, entregamaterial.qtde, material.cdmaterial, " +
				"material.ncmcompleto, material.nome, entregadocumento.numero, entregadocumento.dtemissao," +
				"fornecedor.nome, listaNotafiscalprodutoitementregamaterial.saldo")
				.join("entregamaterial.material material")
				.join("entregamaterial.entregadocumento entregadocumento")
				.leftOuterJoin("entregadocumento.fornecedor fornecedor")
				.leftOuterJoin("entregamaterial.listaNotafiscalprodutoitementregamaterial listaNotafiscalprodutoitementregamaterial")
				.where("entregadocumento.numero = ?", notafiscalprodutoitem.getNumeroNFEntrada())
				.where("entregadocumento.dtemissao = ?", notafiscalprodutoitem.getDtemissao())
				.where("fornecedor = ?", notafiscalprodutoitem.getFornecedor())
				.orderBy("material.cdmaterial")
				.list();
	}

	/**
	* M�todo que retorna o pre�o m�ximo de compra. 
	* 'Pre�o m�ximo de compra': m�dia aritm�tica das compras dos �ltimos N meses ou o valor da �ltima compra que n�o estiver neste per�odo
	*
	* @param cdmaterial
	* @param cdempresa
	* @param qtdeMeses
	* @return
	* @since 16/03/2016
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public Double getPrecoMaximoCompra(String cdmaterial, String cdempresa,	Integer qtdeMeses) {
		Date dtinicio = SinedDateUtils.addMesData(new Date(System.currentTimeMillis()), -qtdeMeses); 
		
		String sql =	" select em.valorunitario as valorunitario " +
						" from entregamaterial em " +
						" join entregadocumento ed on ed.cdentregadocumento = em.cdentregadocumento " +
						" join entrega e on e.cdentrega = ed.cdentrega " +
						" join aux_entrega aux_e on aux_e.cdentrega = e.cdentrega " +
						" where em.cdmaterial = " + cdmaterial +
						" and aux_e.situacao <> " + Situacaosuprimentos.CANCELADA.getCodigo() + " " +
						(StringUtils.isNotBlank(cdempresa) ? " and (ed.cdempresa = " + cdempresa + " or e.cdempresa = " + cdempresa + ") " : "");

		String whereData = " and ed.dtentrada >= '" + SinedDateUtils.toStringPostgre(dtinicio) + "'";

		SinedUtil.markAsReader();
		List<Double> lista = getJdbcTemplate().query(sql + whereData, 
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						return rs.getDouble("valorunitario");
					}
				});
		
		if(SinedUtil.isListEmpty(lista)){
			sql += " order by e.dtentrega desc, ed.dtentrada " +
				   " limit 1";
			SinedUtil.markAsReader();
			lista = getJdbcTemplate().query(sql, 
					new RowMapper() {
						public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
							return rs.getDouble("valorunitario");
						}
					});
		}
		
		Double precoMaximoCompra = null;
		if(SinedUtil.isListNotEmpty(lista)){
			Double total = 0d;
			for(Double valorunitario : lista) total += valorunitario;
			
			precoMaximoCompra = total / lista.size();
		}
		
		return precoMaximoCompra;
	}
	
	
	public Entregamaterial findByMaterialEntradaFiscal (Entregadocumento entrada, Material material){
		return query()
				.select("Entregamaterial.valorbcpis,Entregamaterial.pis,Entregamaterial.valorpis, Entregamaterial.qtde")
				.where("Entregamaterial.entregadocumento=?",entrada)
				.where("Entregamaterial.material = ?", material)
				.unique();
	}
}