package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Arquivobancario;
import br.com.linkcom.sined.geral.bean.Arquivobancariosituacao;
import br.com.linkcom.sined.geral.bean.Arquivobancariotipo;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.ArquivobancarioFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("arquivobancario.nome")
public class ArquivobancarioDAO extends GenericDAO<Arquivobancario> {

	
	@Override
	public void updateListagemQuery(QueryBuilder<Arquivobancario> query,FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O par�metro filtro n�o pode ser null.");
		}
		ArquivobancarioFiltro filtro = (ArquivobancarioFiltro) _filtro;
		
		query
			.select("arquivobancario.cdarquivobancario,arquivobancario.nome,arquivobancario.arquivobancariocorrespondente, arquivobancario.sequencial, " +
					"arquivobancariotipo.cdarquivobancariotipo,arquivobancariotipo.nome,arquivobancario.dtgeracao,arquivobancario.dtprocessa," +
					"arquivobancario.arquivo,arquivobancario.arquivobancariocorrespondente, situacao.cdarquivobancariosituacao, " +
					"situacao.nome")
			.leftOuterJoin("arquivobancario.arquivo arquivo")
			.leftOuterJoin("arquivobancario.arquivobancariocorrespondente arquivobancariocorrespondente")
			.leftOuterJoin("arquivobancario.arquivobancariotipo arquivobancariotipo")
			.leftOuterJoin("arquivobancario.arquivobancariosituacao situacao")
			.whereLikeIgnoreAll("arquivobancariocorrespondente.nome", filtro.getArquivoretorno())
			.whereLikeIgnoreAll("arquivobancario.nome", filtro.getArquivoremessa())
			.whereLikeIgnoreAll("arquivobancario.sequencial", filtro.getSequencial())
			.where("arquivobancariotipo = ?",filtro.getArquivobancariotipo())
			.whereIn("situacao.cdarquivobancariosituacao", filtro.getListaSituacao() != null && filtro.getListaSituacao().size() > 0  ? CollectionsUtil.listAndConcatenate(filtro.getListaSituacao(), "cdarquivobancariosituacao", ",") : null)
			.orderBy("coalesce(arquivobancario.dtprocessa,arquivobancario.dtgeracao) DESC")
			;
		if (filtro.getArquivobancariotipo() != null && filtro.getArquivobancariotipo().equals(Arquivobancariotipo.REMESSA)) {
			query
				.where("arquivobancario.dtgeracao >= ?", filtro.getDtgeracao1())
				.where("arquivobancario.dtgeracao <= ?",filtro.getDtgeracao2())
				;
		} else if (filtro.getArquivobancariotipo() != null && filtro.getArquivobancariotipo().equals(Arquivobancariotipo.RETORNO)) {
			query
				.where("arquivobancario.dtprocessa >= ?",filtro.getDtprocessa1())
				.where("arquivobancario.dtprocessa <= ?",filtro.getDtprocessa2())
				;
		}
	}

	/**
	 * Encontra uma lista de arquivos banc�rios para que seja feita a valida��o no cancelamento.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Arquivobancario> findArquivosBancarios(String whereIn) {
		return query()
					.select("arquivobancario.cdarquivobancario, cor.cdarquivobancario, sit.cdarquivobancariosituacao ")
					.leftOuterJoin("arquivobancario.arquivobancariocorrespondente cor")
					.join("arquivobancario.arquivobancariosituacao sit")
					.whereIn("arquivobancario.cdarquivobancario", whereIn)
					.list();
	}

	/**
	 * Faz update na situa��o do arquivo banc�rio.
	 * 
	 * @param situacao
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void updateSituacao(Arquivobancariosituacao situacao, String whereIn) {
		getHibernateTemplate().bulkUpdate("update Arquivobancario a set a.arquivobancariosituacao = ? where a.id in ("+whereIn+")", new Object[]{situacao});
	}

	/**
	 * Carrega a lista de arquivos banc�rios a partir de uma lista de documentos.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Arquivobancario> findArquivoDocumentos(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Documentos n�o podem ser nulos.");
		}
		return query()
					.select("arquivobancario.cdarquivobancario, documento.cddocumento")
					.join("arquivobancario.listaArqBancarioDoc arqdoc")
					.join("arqdoc.documento documento")
					.where("arquivobancario.arquivobancariotipo = ?",Arquivobancariotipo.REMESSA)
					.where("arquivobancario.arquivobancariosituacao = ?",Arquivobancariosituacao.GERADO)
					.whereIn("documento.cddocumento", whereIn)
					.list();
	}

	/**
	 * Faz um update no arquivo banc�rio atualizando o arquivo banc�rio correspondente e a situa��o do arquivo banc�rio.
	 * 
	 * @param arquivobancario
	 * @param arquivobancariocorrespondente
	 * @param situacao
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoCorrespondenteSituacao(Arquivobancario arquivobancario,
			Arquivobancario arquivobancariocorrespondente, Arquivobancariosituacao situacao) {
		if (arquivobancario == null || arquivobancario.getCdarquivobancario() == null) {
			throw new SinedException("Arquivo Banc�rio n�o pode ser nulo.");
		}
		
		getHibernateTemplate().bulkUpdate("update Arquivobancario ab set ab.arquivobancariocorrespondente = ?, ab.arquivobancariosituacao = ? where ab.id = ?", 
				new Object[]{arquivobancariocorrespondente,situacao,arquivobancario.getCdarquivobancario()});
		
	}
	
	
	public List<Arquivobancario> findByNomeArquivo(String nome){
		return query()
					.select("arquivobancario.cdarquivobancario")
					.join("arquivobancario.arquivo arquivo")
					.where("arquivo.nome = ?", nome)
					.list();
	}
	
	@Override
	public ListagemResult<Arquivobancario> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Arquivobancario> query = query();
		updateListagemQuery(query, filtro);
		return new ListagemResult<Arquivobancario>(query, filtro);
	}

}
