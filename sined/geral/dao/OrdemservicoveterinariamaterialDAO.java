package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Ordemservicoveterinariamaterial;
import br.com.linkcom.sined.util.IteracaoVeterinariaRetorno;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OrdemservicoveterinariamaterialDAO extends GenericDAO<Ordemservicoveterinariamaterial> {

	/**
	 * M�todo que persiste uma OrdemServicoVeterinariaMaterial com o Status de Faturado 
	 *
	 * @param OrdemServicoVeterinariaMaterial
	 * @author Marcos Lisboa
	 */
	public void updateFaturado(Ordemservicoveterinariamaterial ordemservicoveterinariamaterial) {
		getHibernateTemplate().bulkUpdate(
						"update Ordemservicoveterinariamaterial o set o.faturado = ? where o.cdordemservicoveterinariamaterial = ?",
						new Object[] {
								new Boolean(true),
								ordemservicoveterinariamaterial.getCdordemservicoveterinariamaterial() });
		
	}
	
	/**
	 * 
	 * @author Thiago Clemente
	 * 
	 */
	public List<Ordemservicoveterinariamaterial> findForIteracao(){
		return query()
				.select("ordemservicoveterinariamaterial.cdordemservicoveterinariamaterial," +
						"ordemservicoveterinariamaterial.dtinicio, ordemservicoveterinariamaterial.hrinicio," +
						"ordemservicoveterinariamaterial.prazotermino, ordemservicoveterinariamaterial.quantidadeiteracao," +
						"ordemservicoveterinariamaterial.dtproximaiteracao, ordemservicoveterinariamaterial.hrproximaiteracao," +
						"ordemservicoveterinariamaterial.dtfimprevisaoiteracao, ordemservicoveterinariamaterial.hrfimprevisaoiteracao," +
						"ordemservicoveterinariamaterial.dtfim, ordemservicoveterinariamaterial.frequencia," +
						"periodicidade.cdfrequencia, listaAplicacao.dtprevisto, listaAplicacao.hrprevisto")
				.leftOuterJoin("ordemservicoveterinariamaterial.periodicidade periodicidade")
				.leftOuterJoin("ordemservicoveterinariamaterial.listaAplicacao listaAplicacao")
				.where("ordemservicoveterinariamaterial.dtfim is null")
				.list();
	}
	
	//Este m�todo � igual nos projetos do W3erp e do W3erpVeterinaria. Modifica��es devem ser feitas nos dois projetos.
	public void updateIteracao(Ordemservicoveterinariamaterial ordemservicoveterinariamaterial, IteracaoVeterinariaRetorno retorno){
		Timestamp hrproximaiteracao = null;
		if (retorno.getHrproximaiteracao()!=null){
			hrproximaiteracao = new Timestamp(retorno.getHrproximaiteracao().getTime());
		}
		
		Timestamp hrfimprevisaoiteracao = null;
		if (retorno.getHrfimprevisaoiteracao()!=null){
			hrfimprevisaoiteracao = new Timestamp(retorno.getHrfimprevisaoiteracao().getTime());
		}
		
		getJdbcTemplate().update("update ordemservicoveterinariamaterial o set dtproximaiteracao=?, hrproximaiteracao=?, dtfimprevisaoiteracao=?, hrfimprevisaoiteracao=? where o.cdordemservicoveterinariamaterial=?", new Object[]{retorno.getDtproximaiteracao(), hrproximaiteracao, retorno.getDtfimprevisaoiteracao(), hrfimprevisaoiteracao, ordemservicoveterinariamaterial.getCdordemservicoveterinariamaterial()});
		
		if (retorno.getDtfim()==null){
			//S� atualiza o dtfim para nulo quando estiver preenchido. Isto acontece em casos quando se altera a itera��o no cadastro de OS
			getJdbcTemplate().update("update ordemservicoveterinariamaterial o set dtfim=null where o.cdordemservicoveterinariamaterial=? and dtfim is not null", new Object[]{ordemservicoveterinariamaterial.getCdordemservicoveterinariamaterial()});
		}else {
			//S� atualizo o dtfim quando estiver nulo. Caso j� terminou uma itera��o, n�o � mais necess�rio atualizar
			getJdbcTemplate().update("update ordemservicoveterinariamaterial o set dtfim=? where o.cdordemservicoveterinariamaterial=? and dtfim is null", new Object[]{retorno.getDtfim(), ordemservicoveterinariamaterial.getCdordemservicoveterinariamaterial()});
		}		
	}
	
	public List<Ordemservicoveterinariamaterial> findWithReserva(String whereInOSVM){
		if(StringUtils.isBlank(whereInOSVM))
			return new ArrayList<Ordemservicoveterinariamaterial>();
		
		return query()
				.select("ordemservicoveterinariamaterial.cdordemservicoveterinariamaterial," +
						"listaReserva.cdreserva, materialReserva.cdmaterial ")
				.join("ordemservicoveterinariamaterial.listaReserva listaReserva")
				.leftOuterJoin("listaReserva.material materialReserva")
				.whereIn("ordemservicoveterinariamaterial.cdordemservicoveterinariamaterial", whereInOSVM)
				.list();
	}
}