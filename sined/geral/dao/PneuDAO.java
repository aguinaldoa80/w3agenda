package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Pneuqualificacao;
import br.com.linkcom.sined.geral.bean.Producaoetapa;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoagendasituacao;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericPneuSearchBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PneuFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PneuDAO extends GenericDAO<Pneu> {

//	private static String MONTAGEM_NOME_PNEU = "concat('PNEU ', coalesce(pneumarca.nome, ''), ' ', coalesce(pneumodelo.nome, ''), ' ', coalesce(pneu.serie, ''))";
	
	@Override
	public void updateListagemQuery(QueryBuilder<Pneu> query, FiltroListagem _filtro) {
		PneuFiltro filtro = (PneuFiltro) _filtro;
		
		query
			.select("pneu.cdpneu, pneu.serie, pneu.dot, pneu.numeroreforma, pneu.acompanhaRoda, pneu.descricao, pneu.lonas, " +
					"pneumarca.cdpneumarca, pneumarca.nome, " +
					"pneumodelo.cdpneumodelo, pneumodelo.nome, " +
					"pneumedida.cdpneumedida, pneumedida.nome, " +
					"materialbanda.cdmaterial, materialbanda.identificacao, materialbanda.nome, pneuqualificacao.nome , otrPneuTipo.cdOtrPneuTipo, otrPneuTipo.nome, pneuSegmento.cdPneuSegmento, pneuSegmento.nome")
			.leftOuterJoin("pneu.pneumarca pneumarca")
			.leftOuterJoin("pneu.pneumodelo pneumodelo")
			.leftOuterJoin("pneu.pneumedida pneumedida")
			.leftOuterJoin("pneu.materialbanda materialbanda")
			.leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
			.leftOuterJoin("pneu.otrPneuTipo otrPneuTipo")
			.leftOuterJoin("pneu.pneuSegmento pneuSegmento")
			.where("pneumarca = ?", filtro.getPneumarca())
			.where("pneumodelo = ?", filtro.getPneumodelo())
			.where("pneumedida = ?", filtro.getPneumedida())
			.where("otrPneuTipo = ?", filtro.getTipoPneuOtr())
			.where("pneuSegmento = ?", filtro.getPneuSegmento())
			.whereLikeIgnoreAll("pneu.serie", filtro.getSerie())
			.whereLikeIgnoreAll("pneu.dot", filtro.getDot());
		
		if(Boolean.TRUE.equals(filtro.getSomenteclientepedido()) && 
				filtro.getClientepedidovenda() != null){
			query.where("exists (  select 1 " +
								 " from Pedidovenda pv " +
								 " join pv.cliente c " +
								 " join pv.listaPedidovendamaterial pvm "+
								 " where pvm.pneu.cdpneu = pneu.cdpneu and " +
								 " pv.cliente.cdpessoa = " + filtro.getClientepedidovenda().getCdpessoa() + " )");
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Pneu> query) {
		query
			.select("pneu.cdpneu, pneu.serie, pneu.dot, pneu.numeroreforma, pneu.cdusuarioaltera, pneu.dtaltera, pneu.acompanhaRoda, " +
					"pneu.descricao, pneu.lonas, " +
					"pneuSegmento.cdPneuSegmento, pneuSegmento.nome, pneuSegmento.marca, pneuSegmento.descricao, pneuSegmento.lonas, " +
					"pneuSegmento.modelo, pneuSegmento.medida, pneuSegmento.qualificacaoPneu, pneuSegmento.serieFogo, " +
					"pneuSegmento.dot, pneuSegmento.numeroReformas, pneuSegmento.bandaCamelbak, pneuSegmento.tipoPneuOtr, " +
					"pneumarca.cdpneumarca, pneumarca.nome, " +
					"pneumodelo.cdpneumodelo, pneumodelo.nome, " +
					"pneumedida.cdpneumedida, pneumedida.nome, " +
					"materialbanda.cdmaterial, materialbanda.identificacao, materialbanda.nome, materialbanda.profundidadesulco, " +
					"pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome, otrPneuTipo.cdOtrPneuTipo, otrPneuTipo.nome")
			.leftOuterJoin("pneu.pneumarca pneumarca")
			.leftOuterJoin("pneu.pneumodelo pneumodelo")
			.leftOuterJoin("pneu.pneumedida pneumedida")
			.leftOuterJoin("pneu.materialbanda materialbanda")
			.leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
			.leftOuterJoin("pneu.otrPneuTipo otrPneuTipo")
			.leftOuterJoin("pneu.pneuSegmento pneuSegmento");
	}
	
	/**
	 * Busca os pneus para serem enviados ao W3Producao
	 * @param whereIn
	 * @return
	 */
	public List<Pneu> findForW3Producao(String whereIn){
		return query()
					.select("pneu.cdpneu, pneu.serie, pneu.dot, pneu.numeroreforma, " +
							"pneumarca.cdpneumarca, pneumedida.cdpneumedida, pneumodelo.cdpneumodelo, materialbanda.cdmaterial," +
							"pneuqualificacao.cdpneuqualificacao")
					.leftOuterJoin("pneu.pneumarca pneumarca")
					.leftOuterJoin("pneu.pneumodelo pneumodelo")
					.leftOuterJoin("pneu.pneumedida pneumedida")
					.leftOuterJoin("pneu.materialbanda materialbanda")
					.leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
					.whereIn("pneu.cdpneu", whereIn)
					.list();
	}

	/**
	 * Insere o pneu com o id informado.
	 *
	 * @param cdpneu
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/04/2016
	 */
	public Pneu insertWithId(Integer cdpneu) {
		getJdbcTemplate().execute("INSERT INTO PNEU(CDPNEU) VALUES (" + cdpneu + ")");
		return new Pneu(cdpneu);
	}
	
	/**
	* M�todo que busca os pneus
	*
	* @param whereIn
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<Pneu> findForAndroid(String whereIn) {
		return query()
			.select("pneu.cdpneu, pneu.serie, pneu.dot, pneu.numeroreforma, " +
					"pneumarca.cdpneumarca, pneumedida.cdpneumedida, pneumodelo.cdpneumodelo, materialbanda.cdmaterial")
			.leftOuterJoin("pneu.pneumarca pneumarca")
			.leftOuterJoin("pneu.pneumodelo pneumodelo")
			.leftOuterJoin("pneu.pneumedida pneumedida")
			.leftOuterJoin("pneu.materialbanda materialbanda")
			.whereIn("pneu.cdpneu", whereIn)
			.list();
	}

	/**
	 * Atualiza a banda do pneu
	 *
	 * @param pneu
	 * @param materialbanda
	 * @author Rodrigo Freitas
	 * @since 08/07/2016
	 */
	public void updateBanda(Pneu pneu, Material materialbanda) {
		getHibernateTemplate().bulkUpdate("update Pneu p set p.materialbanda = ? where p = ?", new Object[]{materialbanda, pneu});
	}

	/**
	 * Atualiza registro para o ch�o de f�brica para for�ar a sincroniza��o
	 *
	 * @param cdpneu
	 * @author Rodrigo Freitas
	 * @since 27/09/2016
	 */
	public void atualizaW3producao(Integer cdpneu) {
		getJdbcTemplate().execute("update Pneu set cdpneu = cdpneu where cdpneu = " + cdpneu);
	}

	public List<Pneu> findPneuComProducaoagenda(String whereInPneu, Producaoetapa producaoEtapa, Producaoagendasituacao... situacoes) {
		if(StringUtils.isBlank(whereInPneu)){
//			throw new SinedException("Par�metro inv�lido.");
			return null;
		}
		
		QueryBuilder<Pneu> query = query()
				.select("pneu.cdpneu, pneu.dot")
				.whereIn("pneu.cdpneu", whereInPneu);
		
		if(situacoes != null && situacoes.length > 0){
			query.where("exists (select p.cdpneu " +
								"from Producaoagendamaterial pam " +
								"join pam.pneu p " +
								"left join pam.producaoetapa producaoetapa " +
								"join pam.producaoagenda pa " +
								"where p.cdpneu in (" + whereInPneu + ") " +
								"  and p = pneu " +
								(Util.objects.isPersistent(producaoEtapa)? " and producaoetapa.cdproducaoetapa = "+producaoEtapa.getCdproducaoetapa(): "") +
								" and pa.producaoagendasituacao in (" + Producaoagendasituacao.listAndConcatenate(Arrays.asList(situacoes)) + "))");
		}
		
		return query.list();
	}
	
	public void updatePneuqualificacao(Pneu pneu, Integer cdpneuqualificacao) {
		getHibernateTemplate().bulkUpdate("update Pneu p set p.pneuqualificacao = ? where p = ?", new Object[]{new Pneuqualificacao(cdpneuqualificacao), pneu});
	}

	@Override
	public ListagemResult<Pneu> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Pneu> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("pneu.cdpneu");
		
		return new ListagemResult<Pneu>(query, filtro);
	}
	
	public List<Pneu> findAutocompletePneu(String value) {
		if(StringUtils.isBlank(value)){
			return new ArrayList<Pneu>();
		}
		
		return query()
				.select("pneu.cdpneu, pneu.dot, pneu.serie, pneumarca.cdpneumarca, pneumarca.nome, pneumodelo.cdpneumodelo, pneumodelo.nome, pneumedida.cdpneumedida, pneumedida.nome, " +
						"pneuSegmento.cdPneuSegmento, pneuSegmento.nome, pneuSegmento.tipoPneuOtr, pneuSegmento.marca, pneuSegmento.descricao, pneuSegmento.lonas, pneuSegmento.modelo, " +
						"pneuSegmento.medida, pneuSegmento.qualificacaoPneu, pneuSegmento.serieFogo, pneuSegmento.dot, pneuSegmento.numeroReformas, pneuSegmento.bandaCamelbak, " +
						"materialbanda.cdmaterial, materialbanda.nome, materialbanda.profundidadesulco, pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome, pneu.numeroreforma, " +
						"otrPneuTipo.cdOtrPneuTipo, otrPneuTipo.nome, pneu.lonas")
				.leftOuterJoin("pneu.pneumarca pneumarca")
				.leftOuterJoin("pneu.pneumodelo pneumodelo")
				.leftOuterJoin("pneu.pneumedida pneumedida")
				.leftOuterJoin("pneu.materialbanda materialbanda")
				.leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
				.leftOuterJoin("pneu.pneuSegmento pneuSegmento")
				.leftOuterJoin("pneu.otrPneuTipo otrPneuTipo")
				.openParentheses()
					.whereLikeIgnoreAll("pneumarca.nome", value)
					.or()
					.whereLikeIgnoreAll("pneumodelo.nome", value)
					.or()
					.whereLikeIgnoreAll("pneu.serie", value)
					.or()
					.whereLikeIgnoreAll("pneu.cdpneu", value)
					.or()
					.whereLikeIgnoreAll("pneumedida.nome", value)
				.closeParentheses()
				//.whereLikeIgnoreAll("concat('PNEU ', coalesce(pneumarca.nome, ''), ' ', coalesce(pneumodelo.nome, ''), ' ', coalesce(pneu.serie, ''))", value)
				.setMaxResults(30)
				.list();
	}
	
	public Pneu loadForWS(Pneu pneu) {
		return query()
				.select("pneu.cdpneu, pneu.dot, pneu.serie, pneumarca.nome, pneumodelo.nome")
				.leftOuterJoin("pneu.pneumarca pneumarca")
				.leftOuterJoin("pneu.pneumodelo pneumodelo")
				.where("pneu = ?", pneu)
				.setMaxResults(1)
				.unique();
	}

	@SuppressWarnings("unchecked")
	public List<Pneu> findForPesquisaPneu(GenericPneuSearchBean bean) {
		return (List<Pneu>) getQueryForPesquisaPneu(query(), bean)
				.setPageNumberAndSize(bean.getCurrentPage(), 30)
				.orderBy("pneu.cdpneu")
				.list();
	}

	@SuppressWarnings("rawtypes")
	private QueryBuilder getQueryForPesquisaPneu(QueryBuilder query, GenericPneuSearchBean bean) {
		return query
				.select("pneu.cdpneu, pneu.dot, pneu.serie, pneumarca.cdpneumarca, pneumarca.nome, pneumodelo.cdpneumodelo, pneumodelo.nome, pneumedida.cdpneumedida, pneumedida.nome, " +
						"pneuSegmento.cdPneuSegmento, pneuSegmento.nome, pneuSegmento.tipoPneuOtr, pneuSegmento.marca, pneuSegmento.descricao, pneuSegmento.lonas, pneuSegmento.modelo, " +
						"pneuSegmento.medida, pneuSegmento.qualificacaoPneu, pneuSegmento.serieFogo, pneuSegmento.dot, pneuSegmento.numeroReformas, pneuSegmento.bandaCamelbak, " +
						"materialbanda.cdmaterial, materialbanda.nome, materialbanda.profundidadesulco, pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome, pneu.numeroreforma, " +
						"otrPneuTipo.cdOtrPneuTipo, otrPneuTipo.nome, pneu.lonas")
				.leftOuterJoin("pneu.pneumarca pneumarca")
				.leftOuterJoin("pneu.pneumodelo pneumodelo")
				.leftOuterJoin("pneu.pneumedida pneumedida")
				.leftOuterJoin("pneu.materialbanda materialbanda")
				.leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
				.leftOuterJoin("pneu.pneuSegmento pneuSegmento")
				.leftOuterJoin("pneu.otrPneuTipo otrPneuTipo")
				.where("pneu.pneumarca = ?", bean.getPneuMarca())
				.where("pneu.pneumodelo = ?", bean.getPneuModelo())
				.where("pneu.pneumedida = ?", bean.getPneuMedida())
				.where("pneu.otrPneuTipo = ?", bean.getPneuTipo())
				.where("pneu.pneuSegmento = ?", bean.getPneuSegmento())
				.where("pneu.dot = ?", bean.getDot())
				.where("pneu.serie = ?", bean.getSerie());
	}

	public Long countForPesquisaPneu(GenericPneuSearchBean bean) {
		return (Long) getQueryForPesquisaPneu(newQueryBuilderWithFrom(Long.class), bean)
					.select("count(*)")
					.unique();
	}
	
	
	public Pneu loadPneu(Pneu pneu){
		if(Util.objects.isNotPersistent(pneu)){
			return null;
		}
		return querySined()
			.setUseReadOnly(false)
			.select("pneu.cdpneu, pneu.serie, pneu.dot, pneu.numeroreforma, pneu.cdusuarioaltera, pneu.dtaltera, pneu.acompanhaRoda, " +
					"pneu.descricao, pneu.lonas, " +
					"pneuSegmento.cdPneuSegmento, pneuSegmento.nome, pneuSegmento.marca, pneuSegmento.descricao, pneuSegmento.lonas, " +
					"pneuSegmento.modelo, pneuSegmento.medida, pneuSegmento.qualificacaoPneu, pneuSegmento.serieFogo, " +
					"pneuSegmento.dot, pneuSegmento.numeroReformas, pneuSegmento.bandaCamelbak, pneuSegmento.tipoPneuOtr, " +
					"pneumarca.cdpneumarca, pneumarca.nome, " +
					"pneumodelo.cdpneumodelo, pneumodelo.nome, " +
					"pneumedida.cdpneumedida, pneumedida.nome, " +
					"materialbanda.cdmaterial, materialbanda.identificacao, materialbanda.nome, materialbanda.profundidadesulco, " +
					"pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome, otrPneuTipo.cdOtrPneuTipo, otrPneuTipo.nome")
			.leftOuterJoin("pneu.pneumarca pneumarca")
			.leftOuterJoin("pneu.pneumodelo pneumodelo")
			.leftOuterJoin("pneu.pneumedida pneumedida")
			.leftOuterJoin("pneu.materialbanda materialbanda")
			.leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
			.leftOuterJoin("pneu.otrPneuTipo otrPneuTipo")
			.leftOuterJoin("pneu.pneuSegmento pneuSegmento")
			.where("pneu = ?", pneu)
			.setMaxResults(1)
			.unique();
	}
}
