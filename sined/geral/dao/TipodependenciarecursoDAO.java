package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Tipodependenciarecurso;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("tipodependenciarecurso.nome")
public class TipodependenciarecursoDAO extends GenericDAO<Tipodependenciarecurso>{

	/**
	 * Carrega a lista de tipo de depend�ncia entre recursos para ser exibida no m�dulo de or�amentos em Flex.
	 *
	 * @return List<Tipodependenciarecurso>
	 * @author Rodrigo Alvarenga
	 */
	public List<Tipodependenciarecurso> findAllForFlex(){
		return query()
			.select("tipodependenciarecurso.cdtipodependenciarecurso, tipodependenciarecurso.nome")
			.orderBy("tipodependenciarecurso.nome")
			.list();
	}
}
