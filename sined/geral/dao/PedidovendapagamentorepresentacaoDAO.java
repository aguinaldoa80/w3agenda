package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendapagamentorepresentacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PedidovendapagamentorepresentacaoDAO extends GenericDAO<Pedidovendapagamentorepresentacao> {
	
	/**
	* M�todo que carrega os pagamentos de representa��o
	*
	* @param pedidovenda
	* @return
	* @since 13/02/2015
	* @author Luiz Fernando
	*/
	public List<Pedidovendapagamentorepresentacao> findPedidovendaPagamentoRepresentacaoByPedidovenda(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Pedido de Venda n�o pode ser nula!");
		
		return query()
			.select("pedidovendapagamentorepresentacao.cdpedidovendapagamentorepresentacao, pedidovendapagamentorepresentacao.valororiginal, " +
					"pedidovendapagamentorepresentacao.dataparcela, " +
					"documentotipo.cddocumentotipo, documentotipo.nome, " +
					"prazopagamentorepresentacao.cdprazopagamento, prazopagamentorepresentacao.nome, " +
					"pedidovenda.cdpedidovenda, " + 
					"aux_documento.cddocumento, aux_documento.valoratual, " +
					"documento.cddocumento, documento.dtvencimento, documento.valor," +
					"fornecedor.cdpessoa, fornecedor.nome ")
			.join("pedidovendapagamentorepresentacao.pedidovenda pedidovenda")
			.leftOuterJoin("pedidovendapagamentorepresentacao.documento documento")
			.leftOuterJoin("documento.aux_documento aux_documento")
			.leftOuterJoin("pedidovendapagamentorepresentacao.documentotipo documentotipo")
			.leftOuterJoin("pedidovenda.prazopagamentorepresentacao prazopagamentorepresentacao")
			.leftOuterJoin("pedidovendapagamentorepresentacao.fornecedor fornecedor")
			.where("pedidovenda = ?", pedidovenda)
			.orderBy("pedidovendapagamentorepresentacao.dataparcela, documento.cddocumento")
			.list();
	}
}
