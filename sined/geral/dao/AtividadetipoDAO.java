package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.AtividadetipoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("atividadetipo.nome")
public class AtividadetipoDAO extends GenericDAO<Atividadetipo>{

	@Override
	public void updateListagemQuery(QueryBuilder<Atividadetipo> query,FiltroListagem _filtro) {
		
		AtividadetipoFiltro filtro = (AtividadetipoFiltro) _filtro;
		
		query
			.leftOuterJoinFetch("atividadetipo.colaborador colaborador")
			.leftOuterJoinFetch("atividadetipo.listaAtividadetipoitem listaAtividadetipoitem")
			.whereLikeIgnoreAll("atividadetipo.nome", filtro.getNome())
			.where("atividadetipo.ativo = ?",filtro.getAtivo())
		;
	}
	
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Atividadetipo> query) {
		 query
		 /*	.select("atividadetipo.cdatividadetipo,atividadetipo.nome,atividadetipo.ativo, " +
		 			"colaborador.cdpessoa, colaborador.nome, listaAtividadetipoitem.cdatividadetipoitem, " +
		 			"listaAtividadetipoitem.nome, listaAtividadetipoitem.obrigatorio")*/
		 	.leftOuterJoinFetch("atividadetipo.colaborador colaborador")
			.leftOuterJoinFetch("atividadetipo.listaAtividadetipoitem listaAtividadetipoitem");
	}
	
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				save.saveOrUpdateManaged("listaAtividadetipoitem");
				save.saveOrUpdateManaged("listaAtividadetipomodelo");
				save.saveOrUpdateManaged("listaAtividadetiporesponsavel");
				return null;
			}
		});
	}
	
	/**
	 * M�todo que busca a atividade padr�o
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public Atividadetipo findAtividadePadrao(){
		return query()
				.where("atividadetipo.atividadepadrao = true")
				.unique();
	}

	/**
	 * M�todo que busca os tipos de atividades ativos
	 *
	 * @return
	 * @author Luiz Fernando
	 * @since 26/11/2013
	 */
	public List<Atividadetipo> findAtividadetipoAtivo() {
		return query()
			.select("atividadetipo.cdatividadetipo, atividadetipo.nome")
			.where("atividadetipo.ativo = true")
			.orderBy("atividadetipo.nome")
			.list();
	}
	
	public Atividadetipo findPrazoConclusao (Atividadetipo atividadetipo) {
		return query()
			.select("atividadetipo.prazoconclusao")
			.where("atividadetipo.cdatividadetipo = ?", atividadetipo.getCdatividadetipo())
			.unique();
	}

	/**
	* M�todo que busca os tipos de atividade
	*
	* @param whereIn
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<Atividadetipo> findForAndroid(String whereIn) {
		return query()
			.select("atividadetipo.cdatividadetipo, atividadetipo.nome ")
			.whereIn("atividadetipo.cdatividadetipo", whereIn)
			.list();
	}
	
	/**
	 * M�todo que busca os tipos de atividades ativos com o flag aplicacaoservico igual a true
	 *
	 * @return
	 * @author Mairon Cezar
	 * @since 03/01/2017
	 */
	public List<Atividadetipo> findForServicos() {
		return query()
			.select("atividadetipo.cdatividadetipo, atividadetipo.nome")
			.where("coalesce(atividadetipo.ativo, false) = true")
			.where("coalesce(atividadetipo.aplicacaoservico, false) = true")
			.orderBy("atividadetipo.nome")
			.list();
	}
	
	/**
	 * M�todo que busca os tipos de atividades ativos com o flag aplicacaocrm igual a true
	 *
	 * @return
	 * @author Mairon Cezar
	 * @since 03/01/2017
	 */
	public List<Atividadetipo> findForCrm() {
		return query()
			.select("atividadetipo.cdatividadetipo, atividadetipo.nome")
			.where("atividadetipo.ativo = true")
			.where("atividadetipo.aplicacaocrm = true")
			.orderBy("atividadetipo.nome")
			.list();
	}
}
