package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("emporiumpdv.descricao")
public class EmporiumpdvDAO extends GenericDAO<Emporiumpdv>{

	@Override
	public void updateEntradaQuery(QueryBuilder<Emporiumpdv> query) {
		query
			.leftOuterJoinFetch("emporiumpdv.localarmazenagem localarmazenagem");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Emporiumpdv> query, FiltroListagem filtro) {
		query
			.select("emporiumpdv.cdemporiumpdv, emporiumpdv.codigoemporium, emporiumpdv.descricao, empresa.nome, empresa.razaosocial, empresa.nomefantasia")
			.leftOuterJoin("emporiumpdv.empresa empresa");
	}

	public Emporiumpdv loadForIntegracaoEmporium(Emporiumpdv emporiumpdv) {
		return query()
				.select("emporiumpdv.cdemporiumpdv, emporiumpdv.codigoemporium, emporiumpdv.descricao, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.integracaopdv")
				.leftOuterJoin("emporiumpdv.empresa empresa")
				.entity(emporiumpdv)
				.unique();
	}

	public List<Emporiumpdv> findByEmpresa(Empresa empresa) {
		return query()
					.leftOuterJoinFetch("emporiumpdv.empresa empresa")
					.where("empresa = ?", empresa)
					.orderBy("emporiumpdv.codigoemporium")
					.list();
	}
	
	public List<Emporiumpdv> findForIntegracao() {
		return query()
				.select("emporiumpdv.cdemporiumpdv, emporiumpdv.codigoemporium, empresa.cdpessoa, empresa.integracaopdv, conta.cdconta, localarmazenagem.cdlocalarmazenagem")
				.leftOuterJoin("emporiumpdv.conta conta")
				.leftOuterJoin("emporiumpdv.empresa empresa")
				.leftOuterJoin("emporiumpdv.localarmazenagem localarmazenagem")
				.list();
	}

	public List<Emporiumpdv> findForAcerto() {
		return query()
				.leftOuterJoinFetch("emporiumpdv.empresa empresa")
				.orderBy("emporiumpdv.codigoemporium")
				.list();
	}
	
}
