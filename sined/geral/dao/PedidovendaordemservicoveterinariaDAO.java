package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Pedidovendaordemservicoveterinaria;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PedidovendaordemservicoveterinariaDAO extends GenericDAO<Pedidovendaordemservicoveterinaria> {

	public List<Pedidovendaordemservicoveterinaria> findForReservarAposCancelamentoPedido(String whereInPedidovenda) {
		if(StringUtils.isBlank(whereInPedidovenda))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("ordemservicoveterinaria.cdordemservicoveterinaria, empresa.cdpessoa, requisicaoestado.cdrequisicaoestado, " +
						"atividadetipoveterinaria.cdatividadetipoveterinaria, atividadetipoveterinaria.reserva, " +
						"localarmazenagem.cdlocalarmazenagem, listaOrdemservicoveterinariamaterial.qtdeusada, " +
						"listaOrdemservicoveterinariamaterial.cdordemservicoveterinariamaterial, " +
						"material.cdmaterial, material.servico, material.vendapromocional, loteestoque.cdloteestoque," +
						"listaReserva.cdreserva, materialReserva.cdmaterial, " +
						"pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao")
				.join("pedidovendaordemservicoveterinaria.ordemservicoveterinaria ordemservicoveterinaria")
				.join("ordemservicoveterinaria.requisicaoestado requisicaoestado")
				.join("ordemservicoveterinaria.atividadetipoveterinaria atividadetipoveterinaria")
				.join("atividadetipoveterinaria.localarmazenagem localarmazenagem")
				.join("ordemservicoveterinaria.listaOrdemservicoveterinariamaterial listaOrdemservicoveterinariamaterial")
				.join("listaOrdemservicoveterinariamaterial.material material")
				.join("pedidovendaordemservicoveterinaria.pedidovenda pedidovenda")
				.leftOuterJoin("listaOrdemservicoveterinariamaterial.loteestoque loteestoque")
				.leftOuterJoin("ordemservicoveterinaria.empresa empresa")
				.leftOuterJoin("listaOrdemservicoveterinariamaterial.listaReserva listaReserva")
				.leftOuterJoin("listaReserva.material materialReserva")
				.whereIn("pedidovenda.cdpedidovenda", whereInPedidovenda)
				.list();
	}

}
