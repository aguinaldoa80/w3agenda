package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Ecom_PedidoVenda;
import br.com.linkcom.sined.geral.bean.Ecom_PedidoVendaSituacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class Ecom_PedidoVendaSituacaoDAO extends GenericDAO<Ecom_PedidoVendaSituacao>{

	public List<Ecom_PedidoVendaSituacao> findByEcomPedidoVenda(Ecom_PedidoVenda bean){
		return querySined()
				.select("ecom_PedidoVendaSituacao.cdEcom_PedidoVendaSituacao, ecom_PedidoVendaSituacao.id_situacao, "+
	                    "ecom_PedidoVendaSituacao.situacao, ecom_PedidoVendaSituacao.texto")
				.join("ecom_PedidoVendaSituacao.ecomPedidoVenda ecomPedidoVenda")
				.where("ecomPedidoVenda = ?", bean)
				.list();
	}
}
