package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.ConfiguracaoFolha;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ConfiguracaoFolhaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ConfiguracaoFolhaDAO extends GenericDAO<ConfiguracaoFolha>{

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaEvento");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<ConfiguracaoFolha> query) {
		query
			.leftOuterJoinFetch("configuracaoFolha.agendamento agendamento")
			.leftOuterJoinFetch("configuracaoFolha.listaEvento listaEvento");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<ConfiguracaoFolha> query,
			FiltroListagem _filtro) {
		ConfiguracaoFolhaFiltro filtro = (ConfiguracaoFolhaFiltro)_filtro;
		query
			.select("configuracaoFolha.cdConfiguracaoFolha, configuracaoFolha.descricao, " +
					"empresa.cdpessoa, empresa.nome, empresa.nomefantasia, empresa.razaosocial, " +
					"agendamento.cdagendamento, agendamento.descricao ")
			.leftOuterJoin("configuracaoFolha.agendamento agendamento")
			.leftOuterJoin("configuracaoFolha.empresa empresa")
			.leftOuterJoin("configuracaoFolha.listaEvento listaEvento")
			.where("agendamento = ?", filtro.getAgendamento())
			.where("empresa = ?", filtro.getEmpresa())
			.where("listaEvento.evento = ?", filtro.getEvento());
	}
	
	

	public ConfiguracaoFolha loadForFechamentoFolha(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("configuracaoFolha.cdConfiguracaoFolha, agendamento.cdagendamento, agendamento.dtproximo, " +
						"listaEvento.cdConfiguracaoFolhaEvento, evento.cdEventoPagamento, evento.nome, evento.provisionamento")
				.join("configuracaoFolha.agendamento agendamento")
				.join("configuracaoFolha.listaEvento listaEvento")
				.join("listaEvento.evento evento")
				.where("configuracaoFolha.empresa = ?", empresa)
				.where("agendamento.empresa = ?", empresa)
				.unique();
	}
	
	public boolean existeConfiguracaoFolha(Agendamento agendamento){
		return newQueryBuilderSined(Long.class)
				.select("count (*)")
				.from(ConfiguracaoFolha.class)
				.where("cdagendamento = ?", agendamento.getCdagendamento())
				.unique() > 0;
	}
}
