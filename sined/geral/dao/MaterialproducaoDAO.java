package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoitemsped;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialproducaoDAO extends GenericDAO<Materialproducao> {

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaMaterialproducaoformula");
	}
	
	public List<Materialproducao> findForAtualizar(String whereIn){
		return query()
					.select("materialmestre.cdmaterial, materialproducao.cdmaterialproducao, material.cdmaterial, materialproducao.preco, materialproducao.consumo, materialmestre.valorcusto, materialproducao.valorconsumo")
					.join("materialproducao.materialmestre materialmestre")
					.join("materialproducao.material material")
					.whereIn("material.cdmaterial", whereIn)					
					.list();
	}
	
	public void upadateValor(Materialproducao materialproducao){
		if(materialproducao == null || materialproducao.getCdmaterialproducao() == null || materialproducao.getPreco() == null){
			throw new SinedException("Par�metros inv�lidos");
		}
		
		getJdbcTemplate().execute("update materialproducao set preco = '" + materialproducao.getPreco() + "', valorconsumo = '" + 
				materialproducao.getValorconsumo() + "' where cdmaterialproducao = " + materialproducao.getCdmaterialproducao());
	}
	
	public List<Materialproducao> findListaProducao(Integer cdmaterial) {
		return this.findListaProducao(cdmaterial, Boolean.TRUE);
	}
	
	public List<Materialproducao> findListaProducao(Integer cdmaterial, Boolean somenteCarcaca) {
		QueryBuilder<Materialproducao> qry = query()
			.select("materialproducao.cdmaterialproducao, materialproducao.consumo, materialproducao.valorconsumo, materialproducao.exibirvenda, " +
					"material.cdmaterial, material.nome, material.identificacao, material.servico, material.produto, " +
					"material.epi, material.patrimonio, material.producao, material.profundidadesulco, " +
					"producaoetapanome.cdproducaoetapanome, producaoetapanome.nome, " +
					"materialmestre.cdmaterial, materialmestre.nome")
			.join("materialproducao.materialmestre materialmestre")
			.join("materialproducao.material material")
			.leftOuterJoin("materialproducao.producaoetapanome producaoetapanome")
			.where("materialmestre.cdmaterial = ?", cdmaterial);
		if(Boolean.TRUE.equals(somenteCarcaca)){
			qry.where("materialproducao.carcaca = ?", somenteCarcaca);
		}
			
		return qry.list();		
	}

	public void deleteWhereNotInMaterial(Material material, String whereIn) {
		if(whereIn != null && !whereIn.equals("")){
			getHibernateTemplate().bulkUpdate("delete from Materialproducao mp where mp.materialmestre = ? and mp.cdmaterialproducao not in (" + whereIn + ")", new Object[]{material});
		} else {
			getHibernateTemplate().bulkUpdate("delete from Materialproducao mp where mp.materialmestre = ?", new Object[]{material});
		}
	}

	/**
	* M�todo que atualiza o consumo e valor de consumo do item da produ��o
	*
	* @param materialproducao
	* @param preco
	* @param valorconsumo
	* @since 16/10/2014
	* @author Luiz Fernando
	*/
	public void atualizaPrecoEValorConsumo(Materialproducao materialproducao, Double preco, Double valorconsumo) {
		if(materialproducao != null && materialproducao.getCdmaterialproducao() != null && preco != null && valorconsumo != null){
			getJdbcTemplate().update("update materialproducao set preco = ?, valorconsumo = ? " +
					" where cdmaterialproducao = ? ", new Object[]{preco, valorconsumo, materialproducao.getCdmaterialproducao()});
		}
	}
	
	/**
	 * Busca as inst�ncias de Materialproducao a serem enviadas para o W3Producao
	 * @param whereIn
	 * @return
	 */
	public List<Materialproducao> findForW3Producao(String whereIn){
		return query()
				.select("materialproducao.cdmaterialproducao, materialproducao.ordemexibicao, materialproducao.consumo, materialproducao.exibirvenda," +
						"materialproducao.acompanhabanda, material.cdmaterial, materialmestre.cdmaterial, producaoetapanome.cdproducaoetapanome")
				.leftOuterJoin("materialproducao.material material")
				.leftOuterJoin("materialproducao.materialmestre materialmestre")
				.leftOuterJoin("materialproducao.producaoetapanome producaoetapanome")
				.whereIn("materialproducao.cdmaterialproducao", whereIn)
				.list();
	}

	/**
	* M�todo que busca mat�ria-prima
	*
	* @param whereIn
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<Materialproducao> findForAndroid(String whereIn) {
		return query()
			.select("materialproducao.cdmaterialproducao, materialproducao.exibirvenda, " +
					"material.cdmaterial, materialmestre.cdmaterial ")
			.leftOuterJoin("materialproducao.material material")
			.leftOuterJoin("materialproducao.materialmestre materialmestre")
			.whereIn("materialproducao.cdmaterialproducao", whereIn)
			.list();
	}

	/**
	* M�todo que carrega os insumos para o sped
	*
	* @param whereIn
	* @return
	* @since 09/12/2016
	* @author Luiz Fernando
	*/
	public List<Materialproducao> findForSped(String whereIn) {
		List<Tipoitemsped> listaTipoitemsped = new ArrayList<Tipoitemsped>();
		listaTipoitemsped.add(Tipoitemsped.MERCADORIA_REVENDA);
		listaTipoitemsped.add(Tipoitemsped.MATERIA_PRIMA);
		listaTipoitemsped.add(Tipoitemsped.EMBALAGEM);
		listaTipoitemsped.add(Tipoitemsped.PRODUTO_PROCESSO);
		listaTipoitemsped.add(Tipoitemsped.PRODUTO_ACABADO);
		listaTipoitemsped.add(Tipoitemsped.SUBPRODUTO);
		listaTipoitemsped.add(Tipoitemsped.PRODUTO_INTERMEDIARIO);
		listaTipoitemsped.add(Tipoitemsped.OUTROS_INSUMOS); 
		
		return query()
			.select("materialproducao.cdmaterialproducao, materialproducao.consumo, material.cdmaterial, material.identificacao, material.tipoitemsped  ")
			.join("materialproducao.material material")
			.whereIn("materialproducao.materialmestre.cdmaterial", whereIn)
			.whereIn("material.tipoitemsped", CollectionsUtil.listAndConcatenate(listaTipoitemsped, "id", ","))
			.list();
	}

	public Materialproducao findAcompanhabanda(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("materialproducao.cdmaterialproducao, materialproducao.consumo, material.cdmaterial, unidademedida.cdunidademedida")
				.join("materialproducao.material material")
				.join("material.unidademedida unidademedida")
				.where("materialproducao.materialmestre = ?", material)
				.where("materialproducao.acompanhabanda = true")
				.setMaxResults(1)
				.unique();
	}
}
