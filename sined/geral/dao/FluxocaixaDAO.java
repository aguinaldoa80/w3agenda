package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Fluxocaixa;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContagerencial;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.FluxocaixaFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.FluxocaixaFiltroReport;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

import com.ibm.icu.text.SimpleDateFormat;

@DefaultOrderBy("fluxocaixa.descricao")
public class FluxocaixaDAO extends GenericDAO<Fluxocaixa>{

	private RateioService rateioService;
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Fluxocaixa> query,FiltroListagem _filtro) {
		FluxocaixaFiltro filtro = (FluxocaixaFiltro) _filtro;
		
		query
			.select("fluxocaixa.cdfluxocaixa,fluxocaixa.descricao,fluxocaixa.valor,fluxocaixa.ativo,tipooperacao.nome,fluxocaixa.dtinicio, fluxocaixa.dtfim")
			.leftOuterJoin("fluxocaixa.tipooperacao tipooperacao")
			.leftOuterJoin("fluxocaixa.fluxocaixaTipo fluxocaixaTipo")
			
			.whereLikeIgnoreAll("fluxocaixa.descricao", filtro.getDescricao())
			.where("fluxocaixaTipo = ?",filtro.getFluxocaixaTipo())
			.where("tipooperacao = ?",filtro.getTipooperacao())
			.where("fluxocaixa.valor >= ?",filtro.getValorDe())
			.where("fluxocaixa.valor <= ?",filtro.getValorAte())
		;
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Fluxocaixa bean = (Fluxocaixa) save.getEntity();
		Rateio rateio = bean.getRateio();
		if(rateio != null){
			rateioService.saveOrUpdateNoUseTransaction(rateio);
		}
		save.saveOrUpdateManaged("listaFluxocaixaorigem");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Fluxocaixa> query) {
//		query.select("fluxocaixa.cdfluxocaixa,fluxocaixa.descricao,fluxocaixa.valor,fluxocaixa.dtinicio,fluxocaixa.dtfim,fluxocaixa.ativo," +
//				"fluxocaixatipo.cdfluxocaixatipo,tipooperacao.cdtipooperacao,prazopagamento.cdprazopagamento," +
//				"rateio.cdrateio,rateioitem.valor,rateioitem.percentual,centrocusto.cdcentrocusto,projeto.cdprojeto,contagerencial.cdcontagerencial");
		
		query.leftOuterJoinFetch("fluxocaixa.fluxocaixaTipo fluxocaixatipo");
		query.joinFetch("fluxocaixa.tipooperacao tipooperacao");
		query.joinFetch("fluxocaixa.prazopagamento prazopagamento");
		
		query.leftOuterJoinFetch("fluxocaixa.rateio rateio");
		query.leftOuterJoinFetch("rateio.listaRateioitem rateioitem");
		query.leftOuterJoinFetch("rateioitem.contagerencial contagerencial");
		query.leftOuterJoinFetch("rateioitem.projeto projeto");
		query.leftOuterJoinFetch("rateioitem.centrocusto centrocusto");
	}
	
	/**
	 * M�todo para buscar os fluxos de caixa cadastrados para exibir no relat�rio de fluxo de caixa.
	 * 
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	@SuppressWarnings("unchecked")
	public List<Fluxocaixa> findForReportFluxocaixa(FluxocaixaFiltroReport filtro){
		List<ItemDetalhe> listaProjeto = filtro.getListaProjeto();
		List<ItemDetalhe> listaCentrocusto = filtro.getListaCentrocusto();
		List<ItemDetalhe> listaProjetotipo = filtro.getListaProjetotipo();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		StringBuilder sb = new StringBuilder("")
		.append("SELECT DISTINCT F.CDFLUXOCAIXA, F.DESCRICAO, F.DTINICIO, F.DTFIM, TIPO.CDTIPOOPERACAO, TIPO.NOME, ")
		.append("PP.CDPRAZOPAGAMENTO, PP.NOME AS NOME1, FT.CDFLUXOCAIXATIPO, FT.NOME AS NOME2, FT.SIGLA, P.CDPESSOA, P.NOME AS NOME3, OC.CDORDEMCOMPRA, OC.OBSERVACAO, ")
		.append("(SELECT SUM(RI2.VALOR) FROM RATEIOITEM RI2 LEFT JOIN PROJETO PR2 ON PR2.CDPROJETO = RI2.CDPROJETO WHERE RI2.CDRATEIO = F.CDRATEIO ");
		
		
		if(SinedUtil.isListNotEmpty(listaCentrocusto)){
			sb.append("AND RI2.CDCENTROCUSTO IN (").append(CollectionsUtil.listAndConcatenate(listaCentrocusto, "centrocusto.cdcentrocusto", ",")).append(") ");
		}
		
		if (listaProjetotipo != null && listaProjetotipo.size() > 0) {
			sb.append("AND PR2.CDPROJETOTIPO IN (").append(CollectionsUtil.listAndConcatenate(listaProjetotipo, "projetotipo.cdprojetotipo", ",")).append(") ");
		}
		
		if(listaProjeto != null){
			String idProjeto = String.valueOf(filtro.getRadProjeto().getId());
			if(idProjeto.equals("escolherProjeto") && SinedUtil.isListNotEmpty(listaProjeto)){
				sb.append("AND RI2.CDPROJETO IN (").append(CollectionsUtil.listAndConcatenate(listaProjeto, "projeto.cdprojeto", ",")).append(") ");
			}else if(idProjeto.equals("comProjeto")){
				sb.append("AND RI2.CDPROJETO IS NOT NULL ");
			}else if(filtro.getRadProjeto().getId().equals("semProjeto")){
				sb.append("AND RI2.CDPROJETO IS NULL ");
			}
		}
		
		sb
		.append(") AS VALOR, ")
		.append("array_to_string(array(SELECT DISTINCT proj.nome from rateioitem ri2 join projeto proj on proj.cdprojeto = ri2.cdprojeto where ri2.cdrateio = F.cdrateio), ', ') AS PROJETO ")
		.append("FROM FLUXOCAIXA F ")
		.append("JOIN TIPOOPERACAO TIPO ON TIPO.CDTIPOOPERACAO = F.CDTIPOOPERACAO ")
		.append("JOIN PRAZOPAGAMENTO PP ON PP.CDPRAZOPAGAMENTO = F.CDPRAZOPAGAMENTO ")
		.append("LEFT JOIN FLUXOCAIXATIPO FT ON FT.CDFLUXOCAIXATIPO = F.CDFLUXOCAIXATIPO ")
		.append("LEFT JOIN RATEIO R ON R.CDRATEIO = F.CDRATEIO ")
		.append("LEFT JOIN RATEIOITEM RI ON R.CDRATEIO = RI.CDRATEIO ")
		.append("LEFT JOIN PROJETO PR ON RI.CDPROJETO = PR.CDPROJETO ")
		.append("LEFT JOIN FLUXOCAIXAORIGEM FCO ON FCO.CDFLUXOCAIXA = F.CDFLUXOCAIXA ")
		.append("LEFT JOIN ORDEMCOMPRA OC ON OC.CDORDEMCOMPRA = FCO.CDORDEMCOMPRA ")
		.append("LEFT JOIN AUX_ORDEMCOMPRA AOC ON AOC.CDORDEMCOMPRA = OC.CDORDEMCOMPRA ")
		.append("LEFT JOIN PESSOA P ON OC.CDFORNECEDOR = P.CDPESSOA ")
		.append("WHERE F.ATIVO = TRUE AND F.DTINICIO <= to_date('")
		.append(sdf.format(filtro.getPeriodoAte())).append("', 'dd/mm/yyyy') AND (F.DTFIM IS NOT NULL AND F.DTFIM >= to_date('")
		.append(sdf.format(filtro.getPeriodoDe())).append("', 'dd/mm/yyyy') OR F.DTFIM IS NULL) ")
		
		// SOMENTE AS ORDENS DE COMPRA N�O CANCELADAS
		.append("AND AOC.SITUACAO NOT IN (9) ")
		
		// SOMENTE AS ORDENS DE COMPRA N�O FATURADAS
		.append("AND NOT EXISTS (SELECT E1.CDENTREGA FROM ENTREGA E1 JOIN AUX_ENTREGA AE ON AE.CDENTREGA = E1.CDENTREGA WHERE E1.CDORDEMCOMPRA = OC.CDORDEMCOMPRA AND AE.situacao NOT IN (0,9)) ");
		
		if(SinedUtil.isListNotEmpty(filtro.getListaFluxocaixaTipo())){
			sb.append("AND FT.CDFLUXOCAIXATIPO IN (").append(CollectionsUtil.listAndConcatenate(filtro.getListaFluxocaixaTipo(), "cdfluxocaixatipo", ",")).append(") ");
		} else {
			return new ArrayList<Fluxocaixa>();
		}
		
		if(SinedUtil.isListNotEmpty(listaCentrocusto)){
			sb.append("AND RI.CDCENTROCUSTO IN (").append(CollectionsUtil.listAndConcatenate(listaCentrocusto, "centrocusto.cdcentrocusto", ",")).append(") ");
		}
		
		if (listaProjetotipo != null && listaProjetotipo.size() > 0) {
			sb.append("AND PR.CDPROJETOTIPO IN (").append(CollectionsUtil.listAndConcatenate(listaProjetotipo, "projetotipo.cdprojetotipo", ",")).append(") ");
		}
		
		if(listaProjeto != null){
			String idProjeto = String.valueOf(filtro.getRadProjeto().getId());
			if(idProjeto.equals("escolherProjeto") && SinedUtil.isListNotEmpty(listaProjeto)){
				sb.append("AND RI.CDPROJETO IN (").append(CollectionsUtil.listAndConcatenate(listaProjeto, "projeto.cdprojeto", ",")).append(") ");
			}else if(idProjeto.equals("comProjeto")){
				sb.append("AND RI.CDPROJETO IS NOT NULL ");
			}else if(filtro.getRadProjeto().getId().equals("semProjeto")){
				sb.append("AND RI.CDPROJETO IS NULL ");
			}
		}

		SinedUtil.markAsReader();
		List<Fluxocaixa> list = getJdbcTemplate().query(sb.toString(),
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						return new Fluxocaixa(rs.getInt("CDFLUXOCAIXA"), rs.getString("DESCRICAO"), rs.getLong("VALOR"), rs.getDate("DTINICIO"), rs.getDate("DTFIM"),
								rs.getInt("CDTIPOOPERACAO"), rs.getString("NOME"), rs.getInt("CDPRAZOPAGAMENTO"), rs.getString("NOME1"), rs.getInt("CDFLUXOCAIXATIPO"), 
								rs.getString("NOME2"), rs.getString("SIGLA"), rs.getInt("CDPESSOA"), rs.getString("NOME3"), rs.getInt("CDORDEMCOMPRA"), rs.getString("OBSERVACAO"), rs.getString("PROJETO"));
					}
				});
		return list;
		/*
		QueryBuilder<Fluxocaixa> q = query()
			.select("fluxocaixa.cdfluxocaixa,fluxocaixa.descricao,fluxocaixa.valor,fluxocaixa.dtinicio,fluxocaixa.dtfim," +
					"tipooperacao.cdtipooperacao,tipooperacao.nome," +
					"prazopagamento.cdprazopagamento,prazopagamento.nome," +
					"fluxocaixaTipo.cdfluxocaixatipo,fluxocaixaTipo.nome,fluxocaixaTipo.sigla")
					
			.join("fluxocaixa.tipooperacao tipooperacao")
			.join("fluxocaixa.prazopagamento prazopagamento")
			.leftOuterJoin("fluxocaixa.fluxocaixaTipo fluxocaixaTipo")
			
			.join("fluxocaixa.rateio rateio")
			.leftOuterJoin("rateio.listaRateioitem rateioitem")
			.leftOuterJoin("rateioitem.projeto projeto")
			.leftOuterJoin("rateioitem.centrocusto centrocusto")
			
			.where("fluxocaixa.ativo = true")
			.where("fluxocaixa.dtinicio <= ?",filtro.getPeriodoAte())
			.openParentheses()
				.where("fluxocaixa.dtfim is not null and fluxocaixa.dtfim >= ?",filtro.getPeriodoDe())
				.or()
				.where("fluxocaixa.dtfim is null")
			.closeParentheses()
			;
			
		if(SinedUtil.isListNotEmpty(filtro.getListaFluxocaixaTipo())){
			q.whereIn("fluxocaixaTipo.cdfluxocaixatipo",CollectionsUtil.listAndConcatenate(filtro.getListaFluxocaixaTipo(), "cdfluxocaixatipo", ","));
		}else{
			q.where("0=1");
		}
		
		if(SinedUtil.isListNotEmpty(listaCentrocusto)){
			q.whereIn("centrocusto.cdcentrocusto",CollectionsUtil.listAndConcatenate(listaCentrocusto, "centrocusto.cdcentrocusto", ","));
		}
		
		if(listaProjeto != null){
			String idProjeto = String.valueOf(filtro.getRadProjeto().getId());
			if(idProjeto.equals("escolherProjeto") && SinedUtil.isListNotEmpty(listaProjeto)){
				q.whereIn("projeto.cdprojeto", CollectionsUtil.listAndConcatenate(listaProjeto, "projeto.cdprojeto", ","));
			}else if(idProjeto.equals("comProjeto")){
				q.where("projeto is not null");
			}else if(filtro.getRadProjeto().getId().equals("semProjeto")){
				q.where("projeto is null");
			}
		}
		return q.list();
		*/
	}
	
	/**
	 * M�todo para buscar os fluxos de caixa cadastrados para exibir no relat�rio de fluxo de caixa.
	 * A diferen�a para o m�todo acima, � que este n�o agrupa os dados, o agrupamento � feito no processamento
	 * do fluxo de caixa.
	 * @param filtro
	 * @return
	 * @author Marden Silva
	 */
	public List<Fluxocaixa> findForReportFluxocaixaWithoutGroup(FluxocaixaFiltroReport filtro){
		List<ItemDetalhe> listaProjeto = filtro.getListaProjeto();
		List<ItemDetalhe> listaCentrocusto = filtro.getListaCentrocusto();
		List<ItemDetalhe> listaProjetotipo = filtro.getListaProjetotipo();
		
		QueryBuilderSined<Fluxocaixa> q = querySined();
				q
				.select(
						"fluxocaixa.cdfluxocaixa,fluxocaixa.descricao,fluxocaixa.valor,fluxocaixa.dtinicio,fluxocaixa.dtfim,"
						+ "tipooperacao.cdtipooperacao,tipooperacao.nome,"
						+ "prazopagamento.cdprazopagamento,prazopagamento.nome,"
						+ "fluxocaixaTipo.cdfluxocaixatipo,fluxocaixaTipo.nome,fluxocaixaTipo.sigla,"
						+ "fornecedor.cdpessoa, fornecedor.nome, ordemcompra.cdordemcompra, ordemcompra.observacao,"
						+ "rateioitem.valor, contagerencial.cdcontagerencial, contagerencial.nome, centrocusto.cdcentrocusto")
								
				.join("fluxocaixa.tipooperacao tipooperacao")
				.join("fluxocaixa.prazopagamento prazopagamento")
				.leftOuterJoin("fluxocaixa.fluxocaixaTipo fluxocaixaTipo")

				.join("fluxocaixa.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem rateioitem")
				.leftOuterJoin("rateioitem.projeto projeto")
				.leftOuterJoin("projeto.projetotipo projetotipo")
				.leftOuterJoin("rateioitem.centrocusto centrocusto")
				.leftOuterJoin("rateioitem.contagerencial contagerencial")
				
				.leftOuterJoin("fluxocaixa.listaFluxocaixaorigem fluxocaixaorigem")
				.leftOuterJoin("fluxocaixaorigem.ordemcompra ordemcompra")
				.leftOuterJoin("ordemcompra.aux_ordemcompra aux_ordemcompra")
				.leftOuterJoin("ordemcompra.fornecedor fornecedor")
				.leftOuterJoin("ordemcompra.empresa empresa")

				.where("fluxocaixa.ativo = true")
				.where("empresa = ?", filtro.getEmpresa())
//				.openParentheses().openParentheses()
//				.where("fluxocaixa.dtinicio <= ?", filtro.getPeriodoAte()).where("fluxocaixa.dtfim is not null").where("fluxocaixa.dtfim >= ?", filtro.getPeriodoDe())
//				.closeParentheses().or().where("fluxocaixa.dtfim is null").closeParentheses()
				;
		
		
		if(filtro.getValoresAnteriores() != null && filtro.getValoresAnteriores()){
				q.openParentheses()
					.where("fluxocaixa.dtinicio <= ?", filtro.getPeriodoAte())
					.openParentheses()
						.where("fluxocaixa.dtfim is null").or()
						.where("fluxocaixa.dtfim >= ?", filtro.getPeriodoDe())
					.closeParentheses()
				.closeParentheses();
		}else {
			q.openParentheses()
				.where("fluxocaixa.dtinicio >= ?", filtro.getPeriodoDe())
				.openParentheses()
					.where("fluxocaixa.dtfim is null").or()
					.where("fluxocaixa.dtfim <= ?", filtro.getPeriodoAte())
				.closeParentheses()
			.closeParentheses();
		}
				
				// SOMENTE AS ORDENS DE COMPRA N�O CANCELADAS
			q
				.openParentheses()
					.where("aux_ordemcompra.situacaosuprimentos NOT IN (9)").or()
					.where("aux_ordemcompra.situacaosuprimentos is null")
				.closeParentheses()
				
				// SOMENTE AS ORDENS DE COMPRA N�O FATURADAS
				.where("NOT EXISTS (" +
						"SELECT entrega.cdentrega " +
						"FROM br.com.linkcom.sined.geral.bean.Entrega entrega " +
						"JOIN entrega.listaOrdemcompraentrega listaOrdemcompraentrega2 " +
						"JOIN listaOrdemcompraentrega2.ordemcompra ordemcompra2 " +
						"JOIN entrega.aux_entrega aux_entrega " +
						"WHERE ordemcompra2.cdordemcompra = ordemcompra.cdordemcompra " +
						"AND aux_entrega.situacaosuprimentos NOT IN (0,9)" +
						") ");

		if (SinedUtil.isListNotEmpty(filtro.getListaFluxocaixaTipo())) {
			q.whereIn("fluxocaixaTipo.cdfluxocaixatipo", CollectionsUtil
					.listAndConcatenate(filtro.getListaFluxocaixaTipo(),
							"cdfluxocaixatipo", ","));
		} else {
			return new ArrayList<Fluxocaixa>();
		}

		if (SinedUtil.isListNotEmpty(listaCentrocusto)) {
			q.whereIn("centrocusto.cdcentrocusto", CollectionsUtil
					.listAndConcatenate(listaCentrocusto,
							"centrocusto.cdcentrocusto", ","));
		}
		
		if (listaProjetotipo != null && listaProjetotipo.size() > 0) {
			q.whereIn("projetotipo.cdprojetotipo", CollectionsUtil.listAndConcatenate(listaProjetotipo, "projetotipo.cdprojetotipo", ","));
		}

		if (listaProjeto != null) {
			String idProjeto = String.valueOf(filtro.getRadProjeto().getId());
			if (idProjeto.equals("escolherProjeto")
					&& SinedUtil.isListNotEmpty(listaProjeto)) {
				q.whereIn("projeto.cdprojeto", CollectionsUtil
						.listAndConcatenate(listaProjeto, "projeto.cdprojeto",
								","));
			} else if (idProjeto.equals("comProjeto")) {
				q.where("projeto is not null");
			} else if (filtro.getRadProjeto().getId().equals("semProjeto")) {
				q.where("projeto is null");
			}
		}
		
//		WHEREIN PARA A LISTA DE NATUREZA DA CONTA GERENCIAL
		q.whereIn("contagerencial.natureza", NaturezaContagerencial.listAndConcatenate(filtro.getListanaturezaContaGerencial()));
		
		return q.list();
	}
	
	/**
	* M�todo que verifica se existe um registro de fluxo de caixa referente a ordem de compra
	*
	* @param ordemcompra
	* @return
	* @since 13/01/2015
	* @author Luiz Fernando
	*/
	public boolean existeFluxocaixa(Ordemcompra ordemcompra) {
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Fluxocaixa.class)
			.join("fluxocaixa.listaFluxocaixaorigem listaFluxocaixaorigem")
			.join("listaFluxocaixaorigem.ordemcompra ordemcompra")
			.where("ordemcompra = ?", ordemcompra)
			.unique() > 0;
	}
}
