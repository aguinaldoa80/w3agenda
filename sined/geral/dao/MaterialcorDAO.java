package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Materialcor;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("materialcor.nome")
public class MaterialcorDAO extends GenericDAO<Materialcor> {

	/**
	 * Busca a cor para a importa��o de dados
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/01/2015
	 */
	public Materialcor findByNomeForImportacao(String nome) {
		if(nome == null || nome.equals("")){
			throw new SinedException("Nome da cor do material n�o pode ser nulo.");
		}
		
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		List<Materialcor> lista = query()
			.select("materialcor.cdmaterialcor, materialcor.nome")
			.where("UPPER(" + funcaoTiraacento + "(materialcor.nome)) LIKE ?", Util.strings.tiraAcento(nome).toUpperCase())
			.list();
		
		if(lista != null && lista.size() > 1){
			throw new SinedException("Mais de uma cor de material encontrado.");
		} else if(lista != null && lista.size() == 1){
			return lista.get(0);
		} else return null;
	}
	
}
