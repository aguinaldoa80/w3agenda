package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RateioDAO extends GenericDAO<Rateio>{

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaRateioitem");			
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Rateio> query) {
		query.leftOuterJoinFetch("rateio.listaRateioitem");
	}
	
	/**
	 * M�todo para obter o Rateio e os itens do rateio de um documento.
	 * 
	 * @param documento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Rateio findByDocumento(Documento documento){
		return 
			query()
			.select("rateio.cdrateio, ri.percentual, ri.valor, ri.observacao, cc.cdcentrocusto, cc.nome, cg.cdcontagerencial, " +
					"cg.nome, pj.cdprojeto, pj.nome, vcg.identificador, vcg.arvorepai")
			.leftOuterJoin("rateio.listaRateioitem ri")
			.leftOuterJoin("ri.centrocusto cc")
			.leftOuterJoin("ri.contagerencial cg")
			.leftOuterJoin("cg.vcontagerencial vcg")
			.leftOuterJoin("ri.projeto pj")
			.join("rateio.listaDocumento documento")
			.where("documento = ?",documento)
			.unique();
	}
	
	public List<Rateio> findByOrdemcompra(String whereIn) {
		return query()
					.select("rateio.cdrateio, listaRateioitem.cdrateioitem, listaRateioitem.percentual, listaRateioitem.valor, " +
							"centrocusto.cdcentrocusto, projeto.cdprojeto, contagerencial.cdcontagerencial, contagerencial.nome, " +
							"vcontagerencial.identificador, vcontagerencial.arvorepai")
					.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
					.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
					.leftOuterJoin("listaRateioitem.projeto projeto")
					.leftOuterJoin("listaRateioitem.contagerencial contagerencial")
					.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
					.join("rateio.listaOrdemcompra ordemcompra")
					.whereIn("ordemcompra.cdordemcompra", whereIn)
					.list();
	}

	/**
	 * M�todo que busca o rateio e seus determinados itens
	 * 
	 * @param rateio
	 * @return
	 * @author Tom�s T. Rabelo
	 */
	public Rateio findRateio(Rateio rateio) {
		return query()
				.select("rateio.cdrateio, listaRateioitem.cdrateioitem, listaRateioitem.percentual, listaRateioitem.valor, " +
						"centrocusto.cdcentrocusto, projeto.cdprojeto, contagerencial.cdcontagerencial, contagerencial.nome, " +
						"vcontagerencial.identificador, vcontagerencial.arvorepai")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
				.leftOuterJoin("listaRateioitem.projeto projeto")
				.leftOuterJoin("listaRateioitem.contagerencial contagerencial")
				.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
				.where("rateio = ?", rateio)
				.unique();
	}
	
	

	/**
	 * Busca os rateios para a atualiza��o dos dados de um contrato
	 *
	 * @param whereIn
	 * @return
	 * @since 05/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Rateio> findForAtualizadadosContrato(String whereIn) {
		return query()
				.select("rateio.cdrateio, listaRateioitem.cdrateioitem, listaRateioitem.percentual, listaRateioitem.valor")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.whereIn("rateio.cdrateio", whereIn)
				.list();
	}

	/**
	 * M�todo que faza atualiza��o do rateio da movimenta��o gerada por um ou v�rios documentos.
	 *
	 * @param movimentacao
	 * @author Rodrigo Freitas
	 * @since 17/10/2012
	 */
	public void atualizaRateioMovimentacao(Movimentacao movimentacao) {
		String delete = "delete from rateioitem Where cdrateio in (select distinct m.cdrateio from movimentacao m where m.cdmovimentacao = " + movimentacao.getCdmovimentacao() + ")";

		String insert = "insert into rateioitem(cdrateioitem,cdrateio,cdprojeto, cdcentrocusto, cdcontagerencial, valor, percentual) " +
						"Select nextval('sq_rateioitem'),m.cdrateio, ri2.cdprojeto, ri2.cdcentrocusto, ri2.cdcontagerencial, sum(ri2.valor), (sum(ri2.valor)/m.valor)*100 " +
						"From rateioitem ri2, movimentacao m, movimentacaoorigem mo, documento d " +
						"Where mo.cddocumento = d.cddocumento and mo.cdmovimentacao = m.cdmovimentacao and d.cdrateio = ri2.cdrateio and m.cdmovimentacao = " + movimentacao.getCdmovimentacao() + " " +
						"group by m.cdrateio, ri2.cdprojeto, ri2.cdcentrocusto, ri2.cdcontagerencial, m.valor ";
		
		getJdbcTemplate().execute(delete);
		getJdbcTemplate().execute(insert);
	}

	/**
	* M�todo que busca os rateios dos documentos
	*
	* @param whereIn
	* @return
	* @since 26/10/2015
	* @author Luiz Fernando
	*/
	public List<Rateio> findByRateio(String whereIn){
		if (StringUtils.isBlank(whereIn)) {
			throw new SinedException("Par�metro inv�lido.");
		}
		QueryBuilder<Rateio> query = query()
					.select("rateio.cdrateio, rateioitem.cdrateioitem, rateioitem.valor, rateioitem.percentual, " +
							"contagerencial.nome, contagerencial.cdcontagerencial, projeto.nome, " +
							"projeto.cdprojeto, centrocusto.nome, centrocusto.cdcentrocusto, " +
							"vcontagerencial.identificador, vcontagerencial.arvorepai")
					.join("rateio.listaRateioitem rateioitem")
					.join("rateioitem.contagerencial contagerencial")
					.join("contagerencial.vcontagerencial vcontagerencial")
					.join("rateioitem.centrocusto centrocusto")
					.leftOuterJoin("rateioitem.projeto projeto");
					
		SinedUtil.quebraWhereIn("rateio.cdrateio", whereIn, query);
		
		return query.list();
	}
}
