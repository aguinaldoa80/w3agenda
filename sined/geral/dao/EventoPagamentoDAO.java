package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.EventoPagamento;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.EventoPagamentoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EventoPagamentoDAO extends GenericDAO<EventoPagamento>{


	@Override
	public void updateEntradaQuery(QueryBuilder<EventoPagamento> query) {
		query
			.select("eventoPagamento.cdEventoPagamento, eventoPagamento.nome, eventoPagamento.provisionamento, " +
					"eventoPagamento.dtaltera, eventoPagamento.cdusuarioaltera, eventoPagamento.numeroColuna, " +
					"eventoPagamento.tipoEvento, eventoPagamento.colaboradorDespesaItemTipoImportacao, eventoPagamento.colaboradorDespesaItemTipoProcessar, " +
					"contaContabil.cdcontacontabil, contaContabil.nome, " +
					"vContaContabil.nivel, vContaContabil.arvorepai, vContaContabil.identificador, " +
					"listaEmpresa.cdEventoPagamentoEmpresa, listaEmpresa.codigoIntegracao, " +
					"empresaItem.cdpessoa, empresaItem.nome, " +
					"contaContabilItem.cdcontacontabil, contaContabilItem.nome, " +
					"vContaContabilItem.nivel, vContaContabilItem.arvorepai, vContaContabilItem.identificador")
			.leftOuterJoin("eventoPagamento.contaContabil contaContabil")
			.leftOuterJoin("contaContabil.vcontacontabil vContaContabil")
			.leftOuterJoin("eventoPagamento.listaEmpresa listaEmpresa")
			.leftOuterJoin("listaEmpresa.contaContabil contaContabilItem")
			.leftOuterJoin("contaContabilItem.vcontacontabil vContaContabilItem")
			.leftOuterJoin("listaEmpresa.empresa empresaItem");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaEmpresa");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<EventoPagamento> query,
			FiltroListagem _filtro) {
		EventoPagamentoFiltro filtro = (EventoPagamentoFiltro)_filtro;
		query
			.select("eventoPagamento.nome, eventoPagamento.cdEventoPagamento, eventoPagamento.provisionamento, " +
					"contaContabil.cdcontacontabil, contaContabil.nome, vcontacontabil.nivel, vcontacontabil.identificador, vcontacontabil.arvorepai, " +
					"eventoPagamento.tipoEvento")
			.leftOuterJoin("eventoPagamento.contaContabil contaContabil")
			.leftOuterJoin("contaContabil.vcontacontabil vcontacontabil")
			.whereLikeIgnoreAll("eventoPagamento.nome", filtro.getNome())
			.where("eventoPagamento.contaContabil = ?", filtro.getContaContabil())
			.where("eventoPagamento.tipoEvento = ?", filtro.getTipoEvento())
			.where("eventoPagamento.provisionamento = ?", filtro.getProvisionamento());
	}
	
	public List<EventoPagamento> findByEmpresa(Empresa empresa) {
		QueryBuilder<EventoPagamento> query = query();
		query.select("eventoPagamento.nome, eventoPagamento.cdEventoPagamento, eventoPagamento.provisionamento, " +
					"eventoPagamento.tipoEvento, " +
					"listaEmpresa.cdEventoPagamentoEmpresa, " +
					"empresa.cdpessoa, empresa.nome")
			.leftOuterJoin("eventoPagamento.listaEmpresa listaEmpresa")
			.leftOuterJoin("listaEmpresa.empresa empresa");
		if(empresa != null && empresa.getCdpessoa() != null){
			query.openParentheses()
				.where("empresa is null")
				.or()
				.where("empresa = ?", empresa)
				.closeParentheses();
		}
		
		query.orderBy("eventoPagamento.nome");
			
		return query.list();
	}
	
	public List<EventoPagamento> findByEmpresa(EventoPagamento evento, Empresa empresa) {
		QueryBuilder<EventoPagamento> query = query(); 
		query.select("eventoPagamento.nome, eventoPagamento.cdEventoPagamento, eventoPagamento.provisionamento, " +
					"eventoPagamento.tipoEvento, " +
					"listaEmpresa.cdEventoPagamentoEmpresa, " +
					"empresa.cdpessoa, empresa.nome")
			.leftOuterJoin("eventoPagamento.listaEmpresa listaEmpresa")
			.leftOuterJoin("listaEmpresa.empresa empresa")
			.where("eventoPagamento = ?", evento);
		if(empresa != null && empresa.getCdpessoa() != null){
			query.openParentheses()
				.where("empresa is null")
				.or()
				.where("empresa = ?", empresa)
				.closeParentheses();
		}
			
		return query.list();
	}
	
	public EventoPagamento loadWithTipo(EventoPagamento eventoPagamento) {
		if(eventoPagamento == null || eventoPagamento.getCdEventoPagamento() == null){
			return null;
		}
		return query()
				.select("eventoPagamento.nome, eventoPagamento.cdEventoPagamento, eventoPagamento.provisionamento, " +
						"eventoPagamento.tipoEvento")
				.where("eventoPagamento = ?", eventoPagamento)
				.setMaxResults(1)
				.unique();
	}
	
	public List<EventoPagamento> findForImportacaoProvisionamento() {
		QueryBuilder<EventoPagamento> query = query();
		query.select("eventoPagamento.nome, eventoPagamento.cdEventoPagamento, eventoPagamento.provisionamento, eventoPagamento.colaboradorDespesaItemTipoImportacao, " +
					"eventoPagamento.colaboradorDespesaItemTipoProcessar, eventoPagamento.tipoEvento, eventoPagamento.numeroColuna")
			.where("eventoPagamento.provisionamento = ?", Boolean.TRUE)
			.where("eventoPagamento.numeroColuna is not null");
			
		return query.list();
	}
	
	public EventoPagamento loadForImportarFolhaPagamento(String codigoIntegracao, Empresa empresa) {
		return query().select("eventoPagamento.nome, eventoPagamento.cdEventoPagamento, eventoPagamento.provisionamento, " +
					"eventoPagamento.tipoEvento, eventoPagamento.numeroColuna")
				.join("eventoPagamento.listaEmpresa eventoPagamentoEmpresa")
				.where("eventoPagamentoEmpresa.codigoIntegracao = ?", codigoIntegracao)
				.where("eventoPagamentoEmpresa.empresa = ?", empresa)
				.setMaxResults(1)
				.unique();
	}
}
