package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotaDocumentoDAO extends GenericDAO<NotaDocumento> {

	public List<NotaDocumento> getListaNotaDocumentoWebservice(String whereIn) {
		return query()
					.select("notaDocumento.cdNotaDocumento, nota.cdNota, empresa.cdpessoa")
					.join("notaDocumento.documento documento")
					.join("notaDocumento.nota nota")
					.leftOuterJoin("nota.empresa empresa")
					.whereIn("documento.cddocumento", whereIn)
					.where("notaDocumento.fromwebservice = ?", Boolean.TRUE)
					.list();
	}
	
	public List<NotaDocumento> getListaNotaDocumentoWebserviceEmEspera(String whereIn) {
		return query()
					.select("notaDocumento.cdNotaDocumento, nota.cdNota, empresa.cdpessoa, notaTipo.cdNotaTipo, documento.cddocumento")
					.join("notaDocumento.documento documento")
					.join("notaDocumento.nota nota")
					.join("nota.notaTipo notaTipo")
					.leftOuterJoin("nota.empresa empresa")
					.whereIn("documento.cddocumento", whereIn)
					.where("notaDocumento.fromwebservice = ?", Boolean.TRUE)
					.where("nota.notaStatus = ?", NotaStatus.EM_ESPERA)
					.list();
	}

	public boolean isNotaWebservice(Nota nota) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(NotaDocumento.class)
					.where("notaDocumento.nota = ?", nota)
					.where("notaDocumento.fromwebservice = ?", Boolean.TRUE)
					.unique() > 0;
	}

	public boolean isDocumentoWebservice(Documento documento) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(NotaDocumento.class)
					.where("notaDocumento.documento = ?", documento)
					.where("notaDocumento.fromwebservice = ?", Boolean.TRUE)
					.unique() > 0;
	}

	/**
	 * M�todo que retorna as notas de um determinado documento
	 * 
	 * @param documento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<NotaDocumento> findByDocumento(Documento documento) {
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("notaDocumento.cdNotaDocumento, notaDocumento.ordem, notaDocumento.fromwebservice, documento.cddocumento, nota.cdNota")
			.join("notaDocumento.documento documento")
			.join("notaDocumento.nota nota")
			.where("documento = ? ", documento)
			.list();
	}
	
	/**
	 * Buscar as NotaDocumento a partir do whereIN de documento
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 21/09/2015
	 */
	public List<NotaDocumento> findByDocumento(String whereIn) {
		if(whereIn == null || whereIn.isEmpty())
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
				.select("notaDocumento.cdNotaDocumento, notaDocumento.ordem, notaDocumento.fromwebservice, documento.cddocumento, nota.cdNota, nota.numero, notaTipo.cdNotaTipo, notaStatus.cdNotaStatus")
				.join("notaDocumento.documento documento")
				.join("notaDocumento.nota nota")
				.join("nota.notaTipo notaTipo")
				.join("nota.notaStatus notaStatus")
				.whereIn("documento.cddocumento", whereIn)
				.list();
	}
	
	/**
	 * Busca os registros de NotaDocumento a partir do Documento e as notas n�o canceladas
	 *
	 * @param documento
	 * @return
	 * @since 13/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<NotaDocumento> findByDocumentoNotCancelada(Documento documento) {
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("notaDocumento.cdNotaDocumento, notaDocumento.ordem, notaDocumento.fromwebservice, documento.cddocumento, nota.cdNota")
			.join("notaDocumento.documento documento")
			.join("notaDocumento.nota nota")
			.join("nota.notaStatus notaStatus")
			.where("documento = ? ", documento)
			.where("notaStatus <> ?", NotaStatus.CANCELADA)
			.list();
	}
	
	/**
	 * Verifica se uma determinada conta a receber � proveniente de uma nota.
	 * 
	 * @param cdconta
	 * @return
	 * @author Rafael Salvio
	 * @param notaTipo 
	 * @since 16/04/2012
	 */
	public boolean verificaContaNota(Integer cdconta, NotaTipo notaTipo) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(NotaDocumento.class,"notadocumento")
					.join("notadocumento.documento documento")
					.join("documento.documentoclasse documentoclasse")
					.join("notadocumento.nota nota")
					.join("nota.notaStatus notaStatus")
					.where("documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.where("nota.notaTipo = ?", notaTipo)
					.where("documento.cddocumento = ?", cdconta)
					.where("notaStatus <> ?", NotaStatus.CANCELADA)
					.unique() > 0;
	}
	
	/**
	 * M�todo para centralizar a query das valida��es do faturamento.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/10/2012
	 */
	private QueryBuilder<NotaDocumento> getQueryValidacaoFaturamento() {
		return query()
					.select("notaDocumento.cdNotaDocumento, nota.cdNota, notaTipo.cdNotaTipo, documento.cddocumento, " +
							"documentoacao.cddocumentoacao, nota.numero, notaDocumento.fromwebservice, notaStatus.cdNotaStatus")
					.join("notaDocumento.nota nota")
					.join("nota.notaTipo notaTipo")
					.join("nota.notaStatus notaStatus")
					.join("notaDocumento.documento documento")
					.join("documento.documentoacao documentoacao");
	}

	/**
	 * Busca as informa��es de notas e documentos para valida��o.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/10/2012
	 */
	public List<NotaDocumento> findByNotasForValidacao(String whereIn) {
		return getQueryValidacaoFaturamento()
					.whereIn("nota.cdNota", whereIn)
					.list();
	}

	/**
	 * Busca as informa��es de notas e documentos para valida��o.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/10/2012
	 */
	public List<NotaDocumento> findByDocumentoForValidacao(String whereIn) {
		return getQueryValidacaoFaturamento()
					.whereIn("documento.cddocumento", whereIn)
					.list();
	}

	/**
	 * Verifica se existe algum documento n�o cancelado associado a nota.
	 *
	 * @param nota
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/10/2012
	 */
	public boolean haveDocumentoNaoCanceladoByNota(Nota nota) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(NotaDocumento.class, "notadocumento")
					.join("notadocumento.documento documento")
					.join("documento.documentoacao documentoacao")
					.join("notadocumento.nota nota")
					.join("nota.notaStatus notaStatus")
					.where("documentoacao <> ?", Documentoacao.CANCELADA)
					.where("nota = ?", nota)
					.unique() > 0;
	}
	
	/**
	 * M�todo que retorna as notadocumento que tenham vendas
	 *
	 * @param itensSelecionados
	 * @return
	 * @author Luiz Fernando
	 */
	public List<NotaDocumento> findForAlterarSituacaoVenda(String whereInNota) {
		if(whereInNota == null || "".equals(whereInNota))
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				.setUseReadOnly(false)
				.select("notaDocumento.cdNotaDocumento, nota.cdNota, documento.cddocumento, " +
						"listaDocumentoOrigem.cddocumentoorigem, venda.cdvenda, listaNotavenda.cdnotavenda")
				.join("notaDocumento.nota nota")
				.join("nota.notaStatus notaStatus")
				.join("notaDocumento.documento documento")
				.join("documento.listaDocumentoOrigem listaDocumentoOrigem")
				.join("listaDocumentoOrigem.venda venda")
				.join("venda.vendasituacao vendasituacao")
				.leftOuterJoin("venda.listaNotavenda listaNotavenda")
				.whereIn("nota.cdNota", whereInNota)
				.where("vendasituacao = ?", Vendasituacao.FATURADA)
				.where("notaStatus = ?", NotaStatus.CANCELADA)
				.where("listaNotavenda is null")
				.list();
	}
	
	/**
	 * M�todo que busca os documentos da nota
	 *
	 * @param nota
	 * @return
	 * @author Luiz Fernando
	 * @since 08/04/2014
	 */
	public List<NotaDocumento> findByNota(Nota nota) {
		if(nota == null || nota.getCdNota() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("notaDocumento.cdNotaDocumento, documento.numero, documento.cddocumento, nota.cdNota")
			.join("notaDocumento.documento documento")
			.join("notaDocumento.nota nota")
			.where("nota = ? ", nota)
			.list();
	}
	
	public boolean existeContaAssociadaNota(String whereIn) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(NotaDocumento.class,"notadocumento")
					.join("notadocumento.documento documento")
					.join("documento.documentoclasse documentoclasse")
					.join("notadocumento.nota nota")
					.join("nota.notaStatus notaStatus")
					.where("documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.whereIn("documento.cddocumento", whereIn)
					.where("notaStatus <> ?", NotaStatus.CANCELADA)
					.unique() > 0;
	}
}
