package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoautorizacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotafiscalprodutoautorizacaoDAO extends GenericDAO<Notafiscalprodutoautorizacao> {

	public List<Notafiscalprodutoautorizacao> findByNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		return query()
					.select("notafiscalprodutoautorizacao.cdnotafiscalprodutoautorizacao, notafiscalprodutoautorizacao.tipopessoa, " +
							"notafiscalprodutoautorizacao.cpf, notafiscalprodutoautorizacao.cnpj")
					.where("notafiscalprodutoautorizacao.notafiscalproduto = ?", notafiscalproduto)
					.orderBy("notafiscalprodutoautorizacao.tipopessoa")
					.list();
	}
	
}