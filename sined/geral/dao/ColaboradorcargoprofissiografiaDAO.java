package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Colaboradorcargoprofissiografia;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColaboradorcargoprofissiografiaDAO extends GenericDAO<Colaboradorcargoprofissiografia> {

	/* singleton */
	private static ColaboradorcargoprofissiografiaDAO instance;
	public static ColaboradorcargoprofissiografiaDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradorcargoprofissiografiaDAO.class);
		}
		return instance;
	}
}