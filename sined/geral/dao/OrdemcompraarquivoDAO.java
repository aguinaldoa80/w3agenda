package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.Ordemcompraarquivo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OrdemcompraarquivoDAO extends GenericDAO<Ordemcompraarquivo> {

	public Ordemcompraarquivo loadForEnvioPedidoCompra (Ordemcompraarquivo ordemcompraarquivo){
		
		
		return query()
			.select("ordemcompraarquivo.cdordemcompraarquivo, arquivo.cdarquivo, arquivo.nome")
			.leftOuterJoin("ordemcompraarquivo.arquivo arquivo")
			.where("ordemcompraarquivo = ?", ordemcompraarquivo)
			.unique();
		
	}
	
}
