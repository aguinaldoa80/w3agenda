package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Empresaconfiguracaonfproduto;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.EmpresaconfiguracaonfprodutoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EmpresaconfiguracaonfprodutoDAO extends GenericDAO<Empresaconfiguracaonfproduto>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Empresaconfiguracaonfproduto> query, FiltroListagem _filtro) {
		EmpresaconfiguracaonfprodutoFiltro filtro = (EmpresaconfiguracaonfprodutoFiltro) _filtro;
		
		query
			.select("empresaconfiguracaonfproduto.cdempresaconfiguracaonfproduto, empresaconfiguracaonfproduto.descricao")
			.whereLikeIgnoreAll("empresaconfiguracaonfproduto.descricao", filtro.getDescricao());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Empresaconfiguracaonfproduto> query) {
		query.leftOuterJoinFetch("empresaconfiguracaonfproduto.listaCampo listaCampo").orderBy("listaCampo.linha, listaCampo.coluna");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaCampo");
	}
	
	public boolean verificaConfiguracao() {
		return newQueryBuilderSined(Long.class)
		.select("count(*)")
		.from(Empresaconfiguracaonfproduto.class)
		.unique() > 0;
	}
	
	@Override
	public ListagemResult<Empresaconfiguracaonfproduto> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Empresaconfiguracaonfproduto> query = query();
		updateListagemQuery(query, filtro);
		
		return new ListagemResult<Empresaconfiguracaonfproduto>(query, filtro);
	}
}
