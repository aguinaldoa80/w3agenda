package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.Configuracaoconciliacaocartao;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConciliacaoCartaoCreditoBean.TipoOperadoraCartaoCredito;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ConfiguracaoconciliacaocartaoDAO extends GenericDAO<Configuracaoconciliacaocartao>{

	public Configuracaoconciliacaocartao loadByTipooperadora(TipoOperadoraCartaoCredito tipooperadora){
		return query()
				.where("configuracaoconciliacaocartao.tipooperadora = ?", tipooperadora)
				.unique();
	}
}
