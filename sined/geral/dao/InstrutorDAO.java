package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Instrutor;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("upper(retira_acento(instrutor.nome))")
public class InstrutorDAO extends GenericDAO<Instrutor> {

	/* singleton */
	private static InstrutorDAO instance;
	public static InstrutorDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(InstrutorDAO.class);
		}
		return instance;
	}
	
}
