package br.com.linkcom.sined.geral.dao;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Faturamentocontratohistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FaturamentocontratohistoricoDAO extends GenericDAO<Faturamentocontratohistorico>{

	/**
	 * Atualiza a flag de finalizado do hist�rico de contrato.
	 *
	 * @param whereInFaturamentohitorico
	 * @author Rodrigo Freitas
	 * @since 30/06/2014
	 */
	public void updateFinalizado(String whereInFaturamentohitorico) {
		getHibernateTemplate().bulkUpdate("update Faturamentocontratohistorico f set f.finalizado = true where f.id in (" + whereInFaturamentohitorico + ")");
	}

	/**
	 * Verifica se existe algum faturamento n�o finalizado.
	 *
	 * @param whereInContrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/06/2014
	 */
	public boolean haveFaturamentoNaoFinalizadoByContrato(String whereInContrato) {
		return newQueryBuilderWithFrom(Long.class)
					.select("count(*)")
					.join("faturamentocontratohistorico.contrato contrato")
					.whereIn("contrato.cdcontrato", whereInContrato)
					.openParentheses()
					.where("faturamentocontratohistorico.finalizado is null").or()
					.where("faturamentocontratohistorico.finalizado = ?", Boolean.FALSE)
					.closeParentheses()
					.unique() > 0;
	}
	
	/**
	 * Verifica se existe algum faturamento do contrato com a mesma data de vencimento. 
	 *
	 * @param contrato
	 * @param dtvencimento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/06/2014
	 */
	public boolean haveFaturamentoByContratoDtvencimento(Contrato contrato, Date dtvencimento){
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.join("faturamentocontratohistorico.contrato contrato")
				.where("faturamentocontratohistorico.contrato = ?", contrato)
				.where("faturamentocontratohistorico.dtvencimento = ?", dtvencimento)
				.unique() > 0;
	}
	
}