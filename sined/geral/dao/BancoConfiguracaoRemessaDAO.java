package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessa;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRemessaTipoEnum;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRemessaPagamentoBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.BancoConfiguracaoRemessaFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class BancoConfiguracaoRemessaDAO extends GenericDAO<BancoConfiguracaoRemessa> {

	@Override
	public void updateListagemQuery(QueryBuilder<BancoConfiguracaoRemessa> query, FiltroListagem _filtro) {
		BancoConfiguracaoRemessaFiltro filtro = (BancoConfiguracaoRemessaFiltro) _filtro;
		query
			.joinFetch("bancoConfiguracaoRemessa.banco banco")
			.whereLikeIgnoreAll("bancoConfiguracaoRemessa.nome", filtro.getNome())
			.where("bancoConfiguracaoRemessa.banco = ?", filtro.getBanco())
			.where("bancoConfiguracaoRemessa.tipo = ?", filtro.getBancoConfiguracaoRemessaTipo());
		super.updateListagemQuery(query, filtro);
	}
		
	@Override
	public void updateEntradaQuery(QueryBuilder<BancoConfiguracaoRemessa> query) {
		query
			.select("bancoConfiguracaoRemessa.cdbancoconfiguracaoremessa, bancoConfiguracaoRemessa.nome, bancoConfiguracaoRemessa.tipo, " +
					"bancoConfiguracaoRemessa.tamanho, bancoConfiguracaoRemessa.cdusuarioaltera, bancoConfiguracaoRemessa.dtaltera, " +
					"bancoConfiguracaoRemessa.utilizaTabelaAuxiliar1, bancoConfiguracaoRemessa.utilizaTabelaAuxiliar2, " +
					"bancoConfiguracaoRemessa.nomeTabelaAuxiliar1, bancoConfiguracaoRemessa.nomeTabelaAuxiliar2, " +
					"bancoConfiguracaoRemessa.empresaobrigatoria, " +
					"banco.cdbanco, banco.nome, banco.numero, " +
					"bancoConfiguracaoRemessaSegmento.cdbancoconfiguracaoremessasegmento, bancoConfiguracaoRemessaSegmento.ordem, " +
					"bancoConfiguracaoRemessaSegmento.condicao, " +
					"bancoConfiguracaoSegmento.cdbancoconfiguracaosegmento, bancoConfiguracaoSegmento.nome, " +
					"bancoConfiguracaoSegmento.tipoSegmento, " + 
					"bancoConfiguracaoRemessaInstrucao.cdbancoconfiguracaoremessainstrucao, bancoConfiguracaoRemessaInstrucao.codigo, " +
					"bancoConfiguracaoRemessaInstrucao.nome, bancoConfiguracaoRemessaInstrucao.instrucao, " +
					"bancoConfiguracaoRemessaTipoPagamento.cdbancoconfiguracaoremessatipopagamento, " +
					"bancoTipoPagamento.cdbancotipopagamento, bancoTipoPagamento.descricao, " +
					"bancoConfiguracaoRemessaFormaPagamento.cdbancoconfiguracaoremessaformapagamento, " +
					"bancoFormapagamento.cdbancoformapagamento, bancoFormapagamento.descricao, " +					
					"bancoConfiguracaoRemessaAuxiliar1.cdbancoconfiguracaoremessaauxiliar, bancoConfiguracaoRemessaAuxiliar1.codigo, " +
					"bancoConfiguracaoRemessaAuxiliar1.nome," +
					"bancoConfiguracaoRemessaAuxiliar2.cdbancoconfiguracaoremessaauxiliar, bancoConfiguracaoRemessaAuxiliar2.codigo, " +
					"bancoConfiguracaoRemessaAuxiliar2.nome, " +
					"bancoConfiguracaoRemessa.considerarcontasituacao, " +
					"listaBancoConfiguracaoRemessaDocumentoacao.cdBancoConfiguracaoRemessaDocumentoacao," +
					"documentoacao.cddocumentoacao, documentoacao.nome," +
					"bancoConfiguracaoRemessaFormaPagamento.agrupamentofornecedor ") 
			.join("bancoConfiguracaoRemessa.banco banco")
			.join("bancoConfiguracaoRemessa.listaBancoConfiguracaoRemessaSegmento bancoConfiguracaoRemessaSegmento")
			.join("bancoConfiguracaoRemessaSegmento.bancoConfiguracaoSegmento bancoConfiguracaoSegmento")
			.leftOuterJoin("bancoConfiguracaoRemessa.listaBancoConfiguracaoRemessaInstrucao bancoConfiguracaoRemessaInstrucao")
			.leftOuterJoin("bancoConfiguracaoRemessa.listaBancoConfiguracaoRemessaTipoPagamento bancoConfiguracaoRemessaTipoPagamento")
			.leftOuterJoin("bancoConfiguracaoRemessaTipoPagamento.bancoTipoPagamento bancoTipoPagamento")
			.leftOuterJoin("bancoConfiguracaoRemessa.listaBancoConfiguracaoRemessaFormaPagamento bancoConfiguracaoRemessaFormaPagamento")
			.leftOuterJoin("bancoConfiguracaoRemessaFormaPagamento.bancoFormapagamento bancoFormapagamento")
			.leftOuterJoin("bancoConfiguracaoRemessa.listaBancoConfiguracaoRemessaAuxiliar1 bancoConfiguracaoRemessaAuxiliar1")
			.leftOuterJoin("bancoConfiguracaoRemessa.listaBancoConfiguracaoRemessaAuxiliar2 bancoConfiguracaoRemessaAuxiliar2")
			.leftOuterJoin("bancoConfiguracaoRemessa.listaBancoConfiguracaoRemessaDocumentoacao listaBancoConfiguracaoRemessaDocumentoacao")
			.leftOuterJoin("listaBancoConfiguracaoRemessaDocumentoacao.documentoacao documentoacao")
			.orderBy("bancoConfiguracaoRemessaSegmento.ordem, bancoConfiguracaoRemessaInstrucao.codigo, bancoTipoPagamento.descricao, bancoFormapagamento.descricao");
		super.updateEntradaQuery(query);
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaBancoConfiguracaoRemessaSegmento");
		save.saveOrUpdateManaged("listaBancoConfiguracaoRemessaInstrucao");
		save.saveOrUpdateManaged("listaBancoConfiguracaoRemessaTipoPagamento");
		save.saveOrUpdateManaged("listaBancoConfiguracaoRemessaFormaPagamento");
		save.saveOrUpdateManaged("listaBancoConfiguracaoRemessaAuxiliar1", "bancoConfiguracaoRemessa1");
		save.saveOrUpdateManaged("listaBancoConfiguracaoRemessaAuxiliar2", "bancoConfiguracaoRemessa2");
		save.saveOrUpdateManaged("listaBancoConfiguracaoRemessaDocumentoacao");
		super.updateSaveOrUpdate(save);
	}
	
	public BancoConfiguracaoRemessa findByBancoAndTipo(Banco banco, BancoConfiguracaoRemessaTipoEnum tipo){
		return query()
				.join("bancoConfiguracaoRemessa.banco banco")
				.where("bancoConfiguracaoRemessa.tipo = ?", tipo)
				.where("banco = ?", banco)
				.unique();
	}
	
	public List<BancoConfiguracaoRemessa> loadRemessasForPagamento(ArquivoConfiguravelRemessaPagamentoBean filtro){		
		return query()
				.join("bancoConfiguracaoRemessa.banco banco")
				.join("bancoConfiguracaoRemessa.listaBancoConfiguracaoRemessaTipoPagamento listaBancoConfiguracaoRemessaTipoPagamento")
				.join("bancoConfiguracaoRemessa.listaBancoConfiguracaoRemessaFormaPagamento listaBancoConfiguracaoRemessaFormaPagamento")
				.leftOuterJoin("bancoConfiguracaoRemessa.listaBancoConfiguracaoRemessaInstrucao listaBancoConfiguracaoRemessaInstrucao")
				.where("banco = ?", filtro.getConta().getBanco())
				.where("listaBancoConfiguracaoRemessaTipoPagamento.bancoTipoPagamento = ?", filtro.getTipoPagamento())
				.where("listaBancoConfiguracaoRemessaFormaPagamento.bancoFormapagamento = ?", filtro.getFormaPagamento())
				.where("listaBancoConfiguracaoRemessaInstrucao.instrucao = ?", (filtro.getInstrucaoPagamento() != null ? filtro.getInstrucaoPagamento().getInstrucao() : null))
				.where("bancoConfiguracaoRemessa.tipo = ?", BancoConfiguracaoRemessaTipoEnum.PAGAMENTO)
				.list();
	}

	/**
	* M�todo que carrega o bean com a lista de documentoacao
	*
	* @param bancoConfiguracaoRemessa
	* @return
	* @since 31/07/2014
	* @author Luiz Fernando
	*/
	public BancoConfiguracaoRemessa loadWithBancoConfiguracaoRemessaDocumentoacao(BancoConfiguracaoRemessa bancoConfiguracaoRemessa) {
		return query()
			.select("bancoConfiguracaoRemessa.cdbancoconfiguracaoremessa, bancoConfiguracaoRemessa.nome, bancoConfiguracaoRemessa.tipo, " +
					"bancoConfiguracaoRemessa.tamanho, bancoConfiguracaoRemessa.considerarcontasituacao, " +
					"listaBancoConfiguracaoRemessaDocumentoacao.cdBancoConfiguracaoRemessaDocumentoacao," +
					"documentoacao.cddocumentoacao, documentoacao.nome ") 
			.leftOuterJoin("bancoConfiguracaoRemessa.listaBancoConfiguracaoRemessaDocumentoacao listaBancoConfiguracaoRemessaDocumentoacao")
			.leftOuterJoin("listaBancoConfiguracaoRemessaDocumentoacao.documentoacao documentoacao")
			.where("bancoConfiguracaoRemessa = ?", bancoConfiguracaoRemessa)
			.unique();
	}
	
	/**
	 * M�todo que carrega um bancoconfiguracaoremessa de acordo com o banco da conta informada.
	 * Caso n�o seja informada conta, retorna null.
	 * 
	 * @param conta
	 * @return
	 * @author Rafael Salvio
	 */
	public List<BancoConfiguracaoRemessa> findByContabancaria(Conta conta, BancoConfiguracaoRemessaTipoEnum tipo){
		if(conta == null || conta.getCdconta() == null){
			return null;
		}
		
		return query()
				.join("bancoConfiguracaoRemessa.banco banco")
				.join("banco.listaContas listaContas")
				.where("listaContas = ?", conta)
				.where("bancoConfiguracaoRemessa.tipo=?", tipo)
				.list();
	}
	
	public boolean isPossuiBancoConfiguracaoRemessa(Conta conta){
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.join("bancoConfiguracaoRemessa.banco banco")
				.join("banco.listaContas listaContas")
				.where("listaContas=?", conta)
				.unique()>0;
	}

	@Override
	public ListagemResult<BancoConfiguracaoRemessa> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<BancoConfiguracaoRemessa> query = query();
		updateListagemQuery(query, filtro);
		
		return new ListagemResult<BancoConfiguracaoRemessa>(query, filtro);
	}
}