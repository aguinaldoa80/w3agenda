package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Motivodevolucao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MotivodevolucaoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MotivodevolucaoDAO extends GenericDAO<Motivodevolucao>{

	@Override
	public void updateListagemQuery(QueryBuilder<Motivodevolucao> query, FiltroListagem _filtro) {
		MotivodevolucaoFiltro filtro = (MotivodevolucaoFiltro) _filtro;
		
		query
			.select("motivodevolucao.cdmotivodevolucao, motivodevolucao.descricao, motivodevolucao.constatacao, motivodevolucao.motivodevolucao")
			.whereLikeIgnoreAll("motivodevolucao.descricao", filtro.getDescricao())
			.where("motivodevolucao.constatacao = ?", filtro.getConstatacao())
			.where("motivodevolucao.motivodevolucao = ?", filtro.getMotivodevolucao());
	}

	/**
	* M�todo que retorna os motivos de devolu��o de acordo com o whereIn
	*
	* @param whereIn
	* @return
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public List<Motivodevolucao> findByMotivodevolucao(String whereIn) {
		return query()
				.whereIn("motivodevolucao.cdmotivodevolucao", whereIn)
				.list();
	}
	
	/**
	 * Busca a lista de motivodevolucao para enviar ao W3Producao
	 * @param whereIn
	 * @return
	 */
	public List<Motivodevolucao> findForW3Producao(String whereIn){
		return query()
					.select("motivodevolucao.cdmotivodevolucao, motivodevolucao.descricao, motivodevolucao.constatacao, motivodevolucao.motivodevolucao")
					.whereIn("motivodevolucao.cdmotivodevolucao", whereIn)
					.list();
	}
	
	public List<Motivodevolucao> findForConstacacao(){
		return query()
			.select("motivodevolucao.cdmotivodevolucao, motivodevolucao.descricao")
			.where("motivodevolucao.constatacao = ?", Boolean.TRUE)
			//.orderBy("upper(motivodevolucao.nome)")
			.orderBy("motivodevolucao.descricao")
			.list();
	}
	
	public List<Motivodevolucao> findForMotivodevolucao(){
		return query()
			.select("motivodevolucao.cdmotivodevolucao, motivodevolucao.descricao")
			.where("motivodevolucao.motivodevolucao = ?", Boolean.TRUE)
			.orderBy("motivodevolucao.descricao")
			.list();
	}
}
