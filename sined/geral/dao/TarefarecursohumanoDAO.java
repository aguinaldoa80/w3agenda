package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Tarefarecursohumano;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TarefarecursohumanoDAO extends GenericDAO<Tarefarecursohumano>{

	/**
	 * Carrega a lista de recursos humanos da tarefa de um planejamento passado por par�metro.
	 *
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefarecursohumano> findByPlanejamento(Planejamento planejamento) {
		if (planejamento == null || planejamento.getCdplanejamento() == null) {
			throw new SinedException("Planejamento n�o pode ser nulo.");
		}
		return query()
					.select("tarefarecursohumano.qtde, cargo.cdcargo, cargo.custohora")
					.join("tarefarecursohumano.cargo cargo")
					.join("tarefarecursohumano.tarefa tarefa")
					.join("tarefa.planejamento planejamento")
					.where("planejamento = ?",planejamento)
					.list();
	}
	
	/**
	 * Carrega a lista de recursos humano de tarefa para o relat�rio de or�amento.
	 *
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefarecursohumano> findForOrcamento(Planejamento planejamento) {
		if (planejamento == null || planejamento.getCdplanejamento() == null) {
			throw new SinedException("Planejamento n�o pode ser nulo.");
		}
		return query()
					.select("vcontagerencial.identificador, contagerencial.nome, tipooperacao.nome, " +
							"cargo.custohora, cargo.nome, cargo.cdcargo, tarefarecursohumano.qtde")
					.join("tarefarecursohumano.cargo cargo")
					.join("tarefarecursohumano.tarefa tarefa")
					.leftOuterJoin("cargo.contagerencial contagerencial")
					.leftOuterJoin("contagerencial.tipooperacao tipooperacao")
					.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
					.join("tarefa.planejamento planejamento")
					.where("planejamento = ?",planejamento)
					.list();

	}

	/**
	* M�todo que carrega lista de tarefarecursohumano com tarefa e cargo
	*
	* @param whereIn
	* @return
	* @since Aug 11, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Tarefarecursohumano> carregaCargoTarefa(String whereIn) {
		if(whereIn == null){
			throw new SinedException("Par�metros inv�lidos.");
		}
		
		return query()
					.select("tarefarecursohumano.cdtarefarecursohumano, tarefa.cdtarefa, tarefa.descricao, tarefa.dtinicio, tarefa.dtfim, " +
							"tarefa.duracao, cargo.cdcargo")
					.leftOuterJoin("tarefarecursohumano.tarefa tarefa")
					.leftOuterJoin("tarefarecursohumano.cargo cargo")
					.whereIn("tarefa.cdtarefa", whereIn)
					.list();
	}

}
