package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.MargemContribuicao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MargemContribuicaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

public class MargemContribuicaoDAO extends GenericDAO<MargemContribuicao> {

	@Override
	public void updateEntradaQuery(QueryBuilder<MargemContribuicao> query) {
		query
			.select("margemContribuicao.cdMargemContribuicao, margemContribuicao.nome, margemContribuicao.gastosEmpresa, margemContribuicao.valorGastosEmpresa, " +
					"margemContribuicao.calcularPontoEquilibrio, margemContribuicao.valorPontoEquilibrioAmarelo, margemContribuicao.valorPontoEquilibrio, " +
					"margemContribuicao.mediaComissaoMaterial, margemContribuicao.padrao, " +
					"empresa.cdpessoa, empresa.nome, empresa.razaosocial ")
			.leftOuterJoin("margemContribuicao.empresa empresa");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<MargemContribuicao> query, FiltroListagem _filtro) {
		MargemContribuicaoFiltro margemContribuicaoFiltro = (MargemContribuicaoFiltro) _filtro;
		
		query
			.select("margemContribuicao.nome, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia ")
			.leftOuterJoin("margemContribuicao.empresa empresa")
			.where("empresa = ?", margemContribuicaoFiltro.getEmpresa())
			.whereLikeIgnoreAll("margemContribuicao.nome", margemContribuicaoFiltro.getNome())
			.list();
	}

	public Boolean validaPadrao(MargemContribuicao bean) {
		if (bean == null) {
			return false;
		}
		
		return new QueryBuilderSined<Long>(getHibernateTemplate())
				.select("count(*)")
				.from(MargemContribuicao.class)
				.where("margemContribuicao.padrao = true")
				.where("margemContribuicao.cdMargemContribuicao <> ?", bean.getCdMargemContribuicao())
				.unique() > 0;
	}

	public List<MargemContribuicao> findByEmpresa(Empresa empresa) {
		if (empresa == null || empresa.getCdpessoa() == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
					.select("margemContribuicao.cdMargemContribuicao, margemContribuicao.nome, margemContribuicao.padrao")
					.openParentheses()
						.where("margemContribuicao.empresa = ?", empresa)
						.or()
						.where("margemContribuicao.empresa is null")
					.closeParentheses()
					.list();
	}
	
	public MargemContribuicao loadByMargemContribuicao(MargemContribuicao margemContribuicao) {
		if (margemContribuicao == null || margemContribuicao.getCdMargemContribuicao() == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("margemContribuicao.cdMargemContribuicao, margemContribuicao.nome, margemContribuicao.gastosEmpresa, margemContribuicao.valorGastosEmpresa, " +
						"margemContribuicao.calcularPontoEquilibrio, margemContribuicao.valorPontoEquilibrioAmarelo, " +
						"margemContribuicao.valorPontoEquilibrio, margemContribuicao.mediaComissaoMaterial, margemContribuicao.padrao ")
				.where("margemContribuicao = ?", margemContribuicao)
				.unique();
	}

	public MargemContribuicao findPrincipal() {
		return query()
				.select("margemContribuicao.cdMargemContribuicao, margemContribuicao.nome, margemContribuicao.padrao")
				.where("margemContribuicao.padrao = true")
				.unique();
	}
}
