package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculohorimetro;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculohorimetroFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.AnaliticoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VeiculohorimetroDAO extends GenericDAO<Veiculohorimetro> {

	@Override
	public void updateListagemQuery(QueryBuilder<Veiculohorimetro> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		VeiculohorimetroFiltro filtro = (VeiculohorimetroFiltro) _filtro;
		query
			.select("veiculohorimetro.cdveiculohorimetro, veiculo.cdveiculo, veiculo.placa, veiculohorimetro.dtentrada, veiculohorimetro.horimetronovo, bempatrimonio.nome")
			.join("veiculohorimetro.veiculo veiculo")
			.join("veiculo.patrimonioitem patrimonioitem")
			.join("patrimonioitem.bempatrimonio bempatrimonio")
			.whereLikeIgnoreAll("veiculo.placa", filtro.getPlaca())
			.whereLikeIgnoreAll("veiculo.prefixo", filtro.getPrefixo())
			.where("veiculo = ?", filtro.getVeiculo())
			.where("veiculohorimetro.horimetrotipoajuste = ?", filtro.getHorimetrotipoajuste())
			.orderBy("veiculohorimetro.cdveiculohorimetro desc");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Veiculohorimetro> query) {
		query
			.select("veiculohorimetro.cdveiculohorimetro, veiculohorimetro.motivo, veiculo.cdveiculo, veiculohorimetro.dtentrada, veiculohorimetro.horimetronovo, " +
					"veiculohorimetro.horimetrotipoajuste ")
			.join("veiculohorimetro.veiculo veiculo");
	}

	/**
	 * M�todo que retorna �ltimo registro gravado de KM do ve�culo
	 * 
	 * @param veiculo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Veiculohorimetro getHorimetroAtualDoVeiculo(Veiculo veiculo) {
		if(veiculo == null || veiculo.getCdveiculo() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
					.select("veiculohorimetro.horimetronovo, veiculohorimetro.cdveiculohorimetro, veiculohorimetro.dtentrada")
					.where("veiculohorimetro.cdveiculohorimetro = (select max(v.cdveiculohorimetro) " +
													" from Veiculohorimetro v " +
													" where v.veiculo.cdveiculo =  " + veiculo.getCdveiculo() +
													" and v.dtentrada IN (" +
													"	select max(v2.dtentrada) " +
	                                                "	from Veiculohorimetro v2 " +
	                                                "	where v2.veiculo.cdveiculo = " + veiculo.getCdveiculo() + " ))")
					.where("veiculohorimetro.veiculo = ?", veiculo)
					.unique();
		
	}
	
	/**
	 * M�todo encontrar a quilometragem de um ve�culo
	 * 
	 * @author Rafael Odon
	 * @param veiculo
	 * @return Horimetro
	 * @throws W3AutoException
	 */
	public Veiculohorimetro findByVeiculo(Veiculo veiculo){
		if (veiculo == null || veiculo.getCdveiculo() == null){
			throw new SinedException("Ve�culo n�o pode ser nulo.");
		}
		return query()
			.select("veiculohorimetro.cdveiculohorimetro, veiculohorimetro.dtentrada, veiculohorimetro.horimetronovo, veiculo.cdveiculo, vwveiculo.kmatual")
			.join("veiculohorimetro.veiculo veiculo")
			.leftOuterJoin("veiculo.vwveiculo vwveiculo")
			.where("veiculo = ?",veiculo)
			.orderBy("veiculohorimetro.cdveiculohorimetro DESC, veiculohorimetro.dtentrada DESC")
			.unique();
	}
	
	/**
	 * Buscar todos os ajustes dentro de um determinado periodo
	 * 
	 * @param AnaliticoFiltro
	 * @author Ramon Brazil
	 * @see br.com.linkcom.w3auto.geral.dao.AjusteDAO#findAnalitico(filtro)
	 * @return List<Ajuste>
	 */
	public List<Veiculohorimetro> findAnalitico(AnaliticoFiltro filtro){
		if(filtro == null){
			throw new SinedException("O filtro n�o pode ser nulo");			
		}
		return query()
			.select("veiculohorimetro.dtentrada, veiculohorimetro.horimetronovo, veiculohorimetro.cdveiculohorimetro, horimetrotipoajuste.descricao,"+
					"veiculo.cdveiculo, veiculo.placa, veiculo.prefixo, veiculo.kminicial, colaborador.nome,"+
					"veiculomodelo.cdveiculomodelo, veiculomodelo.nome, vwveiculo.kmatual, vwveiculo.kmreal, vwveiculo.kmrodado")
			.leftOuterJoin("veiculohorimetro.horimetrotipoajuste horimetrotipoajuste")
			.leftOuterJoin("veiculohorimetro.veiculo veiculo")
			.leftOuterJoin("veiculo.vwveiculo vwveiculo")
			.leftOuterJoin("veiculo.veiculomodelo veiculomodelo")
			.leftOuterJoin("veiculo.colaborador colaborador")
			.where("veiculohorimetro.dtentrada >=?",filtro.getDtinicio())
			.where("veiculohorimetro.dtentrada <=?",filtro.getDtfim())
			.where("veiculomodelo =?",filtro.getModelo())
			.where("colaborador =?",filtro.getColaborador())
			.where("veiculo =? ",filtro.getVeiculo())
			.orderBy("veiculomodelo.nome, veiculo.placa, colaborador.nome, veiculohorimetro.cdveiculohorimetro, veiculohorimetro.dtentrada")
			.list();
	}

	

	@SuppressWarnings("unchecked")
	public Double getHorimetroanterior(Veiculohorimetro ajuste) {
		SinedUtil.markAsReader();
		List<Double> listaUpdate = getJdbcTemplate().query("SELECT VHORIMETRO.HORIMETRONOVO AS HORIMETROANTIGO " +
															"FROM VEICULOHORIMETRO VHORIMETRO " +
															"WHERE VHORIMETRO.CDVEICULO = " + ajuste.getVeiculo().getCdveiculo() + " " +
															"AND VHORIMETRO.CDVEICULOHORIMETRO = (SELECT MAX(VEICULOHORIMETRO.CDVEICULOHORIMETRO) " +
															                         " FROM VEICULOHORIMETRO " +
															                         " WHERE VEICULOHORIMETRO.CDVEICULO = VHORIMETRO.CDVEICULO " +
															                         " AND VEICULOHORIMETRO.CDVEICULOHORIMETRO < " + ajuste.getCdveiculohorimetro() + ")", 
															new RowMapper() {
																public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
																	return rs.getInt("HORIMETROANTIGO");
																}
													
															});
		
		if(listaUpdate != null && listaUpdate.size() > 0){
			return listaUpdate.get(0);
		} else {
			return 0d;
		}
		
	}
}

