package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaContrato;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotaContratoDAO extends GenericDAO<NotaContrato> {

	/**
	 * Busca a lista de NotaContrato a partir de uma nota.
	 *
	 * @param nota
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<NotaContrato> findByNota(Nota nota) {
		return query()
					.select("notaContrato.cdNotaContrato, nota.cdNota, contrato.cdcontrato, comissionamento.cdcomissionamento")
					.join("notaContrato.nota nota")
					.join("notaContrato.contrato contrato")
					.leftOuterJoin("contrato.comissionamento comissionamento")
					.where("nota = ?", nota)
					.list();
	}

	/**
	 * M�todo que carrega a lista de notacontrato do contrato
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 07/04/2014
	 */
	public List<NotaContrato> findForCobranca(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		QueryBuilder<NotaContrato> query = query()
				.select("notaContrato.cdNotaContrato, faturamentocontrato.cdfaturamentocontrato, contrato.cdcontrato")
				.join("notaContrato.contrato contrato")
				.leftOuterJoin("notaContrato.faturamentocontrato faturamentocontrato");
		
		SinedUtil.quebraWhereIn("contrato.cdcontrato", whereIn, query);
		return query
				.orderBy("contrato.cdcontrato")
				.list();
	}
	
	public List<NotaContrato> findForRateioCobranca(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		QueryBuilder<NotaContrato> query = query()
				.select("notaContrato.cdNotaContrato, contrato.cdcontrato, rateio.cdrateio, contrato.reajuste, contrato.percentualreajuste")
					.leftOuterJoin("notaContrato.contrato contrato")
					.leftOuterJoin("contrato.rateio rateio");
		
		SinedUtil.quebraWhereIn("notaContrato.nota.cdNota", whereIn, query);
		return query
				.list();
	}

}
