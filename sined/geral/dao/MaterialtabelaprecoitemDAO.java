package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;
import java.util.Set;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialtabelapreco;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecoitem;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterial;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialtabelaprecoitemDAO extends GenericDAO<Materialtabelaprecoitem> {

	/**
	 * Retorna o registro atual da tabela de pre�o de um material.
	 *
	 * @param material
	 * @param cliente
	 * @param prazopagamento
	 * @param pedidovendatipo
	 * @return
	 * @since 23/07/2012
	 * @author Rodrigo Freitas
	 * @param listaCategoriaCliente 
	 * @param empresa 
	 */
	public Materialtabelaprecoitem getItemTabelaPrecoAtual(Material material, Cliente cliente, List<Categoria> listaCategoria, Prazopagamento prazopagamento, Pedidovendatipo pedidovendatipo, Empresa empresa, Unidademedida unidademedida) {
		QueryBuilder<Materialtabelaprecoitem> query = query();
		Date current = SinedDateUtils.currentDate(); 
		
		query
			.select("materialtabelaprecoitem.cdmaterialtabelaprecoitem, materialtabelaprecoitem.valor, materialtabelapreco.cdmaterialtabelapreco, " +
					"materialtabelaprecoitem.valorvendaminimo, materialtabelaprecoitem.valorvendamaximo, materialtabelapreco.taxaacrescimo," +
					"unidademedida.cdunidademedida, unidademedidatabela.cdunidademedida, material.cdmaterial, prazopagamento.cdprazopagamento," +
					"materialtabelaprecocliente.cdmaterialtabelaprecocliente, cliente.cdpessoa, materialtabelapreco.acrescimopadrao, materialtabelapreco.descontopadrao," +
					"materialtabelapreco.tabelaprecotipo, materialtabelapreco.discriminardescontovenda, " +
					"materialtabelaprecoitem.identificadorfabricante")
			.leftOuterJoin("materialtabelaprecoitem.material material")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("materialtabelaprecoitem.unidademedida unidademedidatabela")
			.leftOuterJoin("materialtabelaprecoitem.materialtabelapreco materialtabelapreco")
			.leftOuterJoin("materialtabelapreco.prazopagamento prazopagamento")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecocliente materialtabelaprecocliente")
			.leftOuterJoin("materialtabelaprecocliente.cliente cliente")
			.leftOuterJoin("materialtabelapreco.categoria categoria")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoempresa materialtabelaprecoempresa")
			.leftOuterJoin("materialtabelaprecoempresa.empresa empresa")
			.leftOuterJoin("materialtabelapreco.pedidovendatipo pedidovendatipo")
			.where("material = ?", material)
			.where("materialtabelapreco.dtinicio <= ?", current)
			.openParentheses()
				.where("materialtabelapreco.dtfim >= ?", current)
				.or()
				.where("materialtabelapreco.dtfim is null")
			.closeParentheses();
		
		if(listaCategoria != null && !listaCategoria.isEmpty()){
			query.whereIn("categoria.cdcategoria", CollectionsUtil.listAndConcatenate(listaCategoria, "cdcategoria", ","));
			query.where("cliente is null");			
		}else {
			if(cliente != null && cliente.getCdpessoa() != null){
				query.where("cliente = ?", cliente);
				query.where("categoria is null");
			} else {
				query.where("cliente is null");
				query.where("categoria is null");
			}
		}
		
		if(prazopagamento != null && prazopagamento.getCdprazopagamento() != null){
			query.where("prazopagamento = ?", prazopagamento);
		} else {
			query.where("prazopagamento is null");
		}
		
		if(pedidovendatipo != null && pedidovendatipo.getCdpedidovendatipo() != null){
			query.where("pedidovendatipo = ?", pedidovendatipo);
		} else {
			query.where("pedidovendatipo is null");
		}
		
		if(empresa != null && empresa.getCdpessoa() != null){
			query.where("empresa = ?", empresa);
		} else {
			query.where("empresa is null");
		}
		
		if(unidademedida != null && unidademedida.getCdunidademedida() != null){
			query.where("unidademedidatabela = ?", unidademedida);
		} else {
			query.openParentheses()
				.where("unidademedidatabela is null").or()
				.where("unidademedida.cdunidademedida = unidademedidatabela.cdunidademedida")
				.closeParentheses();
		}
		
		List<Materialtabelaprecoitem> lista = query.list();
		if(lista != null && lista.size() > 0){
			Materialtabelaprecoitem materialtabelaprecoitem = lista.get(0);
			materialtabelaprecoitem.setQtdeTabelaEncontrada(lista.size());
			return materialtabelaprecoitem;
		} else return null;
	}
	
	public List<Materialtabelaprecoitem> findTabelaPrecoAtualOtimizado(Material material) {
		Date current = SinedDateUtils.currentDate(); 
		QueryBuilder<Materialtabelaprecoitem> query = query()
				.select("materialtabelaprecoitem.cdmaterialtabelaprecoitem, materialtabelaprecoitem.valor, materialtabelapreco.cdmaterialtabelapreco, " +
						"materialtabelaprecoitem.valorvendaminimo, materialtabelaprecoitem.valorvendamaximo, materialtabelaprecoitem.percentualdesconto, " +
						"materialtabelapreco.discriminardescontovenda, materialtabelapreco.tabelaprecotipo, materialtabelapreco.taxaacrescimo, " +
						"unidademedida.cdunidademedida, unidademedidatabela.cdunidademedida, material.cdmaterial, prazopagamento.cdprazopagamento," +
						"materialtabelaprecocliente.cdmaterialtabelaprecocliente, cliente.cdpessoa, materialtabelapreco.acrescimopadrao, materialtabelapreco.descontopadrao," +
						"categoria.cdcategoria, pedidovendatipo.cdpedidovendatipo, empresa.cdpessoa, comissionamento.cdcomissionamento, comissionamento.comissionamentotipo, " +
						"materialtabelaprecoitem.identificadorfabricante, comissionamentoItem.cdcomissionamento, comissionamentoItem.nome, comissionamentoItem.comissionamentotipo  ")
				.leftOuterJoin("materialtabelaprecoitem.material material")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("materialtabelaprecoitem.unidademedida unidademedidatabela")
				.leftOuterJoin("materialtabelaprecoitem.materialtabelapreco materialtabelapreco")
				.leftOuterJoin("materialtabelapreco.prazopagamento prazopagamento")
				.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecocliente materialtabelaprecocliente")
				.leftOuterJoin("materialtabelaprecocliente.cliente cliente")
				.leftOuterJoin("materialtabelapreco.categoria categoria")
				.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoempresa materialtabelaprecoempresa")
				.leftOuterJoin("materialtabelaprecoempresa.empresa empresa")
				.leftOuterJoin("materialtabelapreco.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("materialtabelapreco.comissionamento comissionamento")
				.leftOuterJoin("materialtabelaprecoitem.comissionamento comissionamentoItem")
				.where("materialtabelapreco.dtinicio <= ?", current)
				.openParentheses()
					.where("materialtabelapreco.dtfim >= ?", current)
					.or()
					.where("materialtabelapreco.dtfim is null")
				.closeParentheses();
		
		Parametrogeral param = ParametrogeralService.getInstance().findByNome("MATERIAL_MESTRE_TABELAPRECO");
		if(param != null && param.getValor().equalsIgnoreCase("TRUE")) {
			query.where("material = ?", material.getMaterialmestregrade() != null ? material.getMaterialmestregrade() : material);
		} else {
			query.where("material = ?", material);
		}
		
		return query.list();
	}
	
	/**
	 * M�todo que busca o item da tabela de pre�o de acordo com o material e materialtabelapreco 
	 *
	 * @param material
	 * @param materialtabelapreco
	 * @return
	 * @author Luiz Fernando
	 */
	public Materialtabelaprecoitem getItemTabelaPrecoAtual(Material material, Materialtabelapreco materialtabelapreco) {
		List<Materialtabelaprecoitem> lista = null;
		if(material != null && material.getCdmaterial() != null && materialtabelapreco != null && materialtabelapreco.getCdmaterialtabelapreco() != null){
			QueryBuilder<Materialtabelaprecoitem> query = query()
				.select("materialtabelaprecoitem.cdmaterialtabelaprecoitem, materialtabelaprecoitem.valor, " +
						"materialtabelaprecoitem.valorvendaminimo, materialtabelaprecoitem.valorvendamaximo," +
						"materialtabelapreco.taxaacrescimo, unidademedidatabela.cdunidademedida, unidademedida.cdunidademedida," +
						"material.cdmaterial, materialtabelapreco.acrescimopadrao, materialtabelapreco.descontopadrao, materialtabelapreco.cdmaterialtabelapreco, " +
						"materialtabelapreco.tabelaprecotipo, materialtabelapreco.discriminardescontovenda, comissionamento.cdcomissionamento, comissionamento.comissionamentotipo, " +
						"materialtabelaprecoitem.identificadorfabricante, comissionamentoItem.cdcomissionamento, comissionamentoItem.comissionamentotipo  ")
				.join("materialtabelaprecoitem.material material")
				.join("material.unidademedida unidademedida")
				.join("materialtabelaprecoitem.materialtabelapreco materialtabelapreco")
				.leftOuterJoin("materialtabelaprecoitem.unidademedida unidademedidatabela")
				.leftOuterJoin("materialtabelapreco.comissionamento comissionamento")
				.leftOuterJoin("materialtabelaprecoitem.comissionamento comissionamentoItem")
				.where("material = ?", material.getMaterialmestregrade())
				.where("materialtabelapreco = ?", materialtabelapreco);
			
			lista = query.list();
		}
		 
		if(lista != null && lista.size() > 0){
			Materialtabelaprecoitem materialtabelaprecoitem = lista.get(0);
			materialtabelaprecoitem.setQtdeTabelaEncontrada(lista.size());
			return materialtabelaprecoitem;
		} else return null;
	}
	
	/**
	 * M�todo que busca tabela de pre�o do material
	 *
	 * @param material
	 * @param unidademedidaEscolhida
	 * @param materialtabelapreco
	 * @return
	 * @author Luiz Fernando
	 * @since 11/10/2013
	 */
	public List<Materialtabelaprecoitem> findTabelaPrecoAtual(Material material, Unidademedida unidademedidaEscolhida, Materialtabelapreco materialtabelapreco) {
		
		
		List<Materialtabelaprecoitem> lista = null;
		if(material != null && material.getCdmaterial() != null && materialtabelapreco != null && materialtabelapreco.getCdmaterialtabelapreco() != null){
			QueryBuilder<Materialtabelaprecoitem> query = query()
				.select("materialtabelaprecoitem.cdmaterialtabelaprecoitem, materialtabelaprecoitem.valor, " +
						"materialtabelaprecoitem.valorvendaminimo, materialtabelaprecoitem.valorvendamaximo, materialtabelaprecoitem.percentualdesconto, " +
						"materialtabelapreco.taxaacrescimo, materialtabelapreco.cdmaterialtabelapreco, materialtabelapreco.tabelaprecotipo, materialtabelapreco.discriminardescontovenda, " +
						"unidademedidatabela.cdunidademedida, unidademedida.cdunidademedida," +
						"material.cdmaterial, materialtabelapreco.acrescimopadrao, materialtabelapreco.descontopadrao, comissionamento.cdcomissionamento, comissionamento.comissionamentotipo, " +
						"materialtabelaprecoitem.identificadorfabricante, comissionamentoItem.cdcomissionamento, comissionamentoItem.nome, comissionamentoItem.comissionamentotipo ")
				.join("materialtabelaprecoitem.material material")
				.join("material.unidademedida unidademedida")
				.join("materialtabelaprecoitem.materialtabelapreco materialtabelapreco")
				.leftOuterJoin("materialtabelaprecoitem.unidademedida unidademedidatabela")
				.leftOuterJoin("materialtabelapreco.comissionamento comissionamento")
				.leftOuterJoin("materialtabelaprecoitem.comissionamento comissionamentoItem")
				.where("material = ?", material)
				.where("unidademedidatabela = ?", unidademedidaEscolhida)
				.where("materialtabelapreco = ?", materialtabelapreco);
			
			lista = query.list();
		}
		 
		return lista;
	}
		
	/**
	 * M�todo que realiza as opera��es de ajuste de pre�o no banco.
	 * 
	 * @param whereIn
	 * @param reajuste
	 * @author Rafael Salvio
	 */
	public void ajustarPrecos(String whereIn, Double reajuste){
		if (whereIn == null || whereIn.trim() == "" || reajuste == null || reajuste <= 1.0) {
			throw new SinedException("Par�metros inv�lidos");
		}
		
		getJdbcTemplate().execute("update Materialtabelaprecoitem set valor = valor * " + reajuste + 
				", valorvendaminimo = valorvendaminimo * " + reajuste + ",  valorvendamaximo = valorvendamaximo * " + reajuste +
				" where cdmaterialtabelaprecoitem in (" + whereIn + ")");
	
	}

	public Materialtabelaprecoitem getItemTabelaPrecoAtualForContrato(Material material, Cliente cliente, List<Categoria> listaCategoria, Frequencia frequencia, Empresa empresa) {
		QueryBuilder<Materialtabelaprecoitem> query = query();
		Date current = SinedDateUtils.currentDate(); 
		
		query
			.select("materialtabelaprecoitem.cdmaterialtabelaprecoitem, materialtabelaprecoitem.valor, materialtabelapreco.cdmaterialtabelapreco, " +
					"materialtabelaprecoitem.valorvendaminimo, materialtabelaprecoitem.valorvendamaximo, materialtabelapreco.taxaacrescimo," +
					"unidademedida.cdunidademedida, unidademedidatabela.cdunidademedida, material.cdmaterial, " +
					"materialtabelaprecocliente.cdmaterialtabelaprecocliente, cliente.cdpessoa, materialtabelapreco.acrescimopadrao, materialtabelapreco.descontopadrao," +
					"comissionamento.cdcomissionamento, comissionamento.comissionamentotipo, materialtabelapreco.tabelaprecotipo, materialtabelapreco.discriminardescontovenda, materialtabelaprecoitem.identificadorfabricante," +
					"comissionamentoItem.cdcomissionamento, comissionamentoItem.nome, comissionamentoItem.comissionamentotipo ")
			.leftOuterJoin("materialtabelaprecoitem.material material")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("materialtabelaprecoitem.unidademedida unidademedidatabela")
			.leftOuterJoin("materialtabelaprecoitem.materialtabelapreco materialtabelapreco")
			.leftOuterJoin("materialtabelapreco.frequencia frequencia")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecocliente materialtabelaprecocliente")
			.leftOuterJoin("materialtabelaprecocliente.cliente cliente")
			.leftOuterJoin("materialtabelapreco.categoria categoria")
			.leftOuterJoin("materialtabelapreco.listaMaterialtabelaprecoempresa materialtabelaprecoempresa")
			.leftOuterJoin("materialtabelaprecoempresa.empresa empresa")
			.leftOuterJoin("materialtabelapreco.comissionamento comissionamento")
			.leftOuterJoin("materialtabelaprecoitem.comissionamento comissionamentoItem")
			.where("material = ?", material)
			.where("materialtabelapreco.dtinicio <= ?", current)
			.openParentheses()
				.where("materialtabelapreco.dtfim >= ?", current)
				.or()
				.where("materialtabelapreco.dtfim is null")
			.closeParentheses();
		
		if(listaCategoria != null && !listaCategoria.isEmpty()){
			query.whereIn("categoria.cdcategoria", CollectionsUtil.listAndConcatenate(listaCategoria, "cdcategoria", ","));
			query.where("cliente is null");			
		}else {
			if(cliente != null && cliente.getCdpessoa() != null){
				query.where("cliente = ?", cliente);
				query.where("categoria is null");
			} else {
				query.where("cliente is null");
				query.where("categoria is null");
			}
		}
		
		if(frequencia != null && frequencia.getCdfrequencia() != null){
			query.where("frequencia = ?", frequencia);
		} else {
			query.where("frequencia is null");
		}
		
		if(empresa != null && empresa.getCdpessoa() != null){
			query.where("empresa = ?", empresa);
		} else {
			query.where("empresa is null");
		}
		
		List<Materialtabelaprecoitem> lista = query.list();
		if(lista != null && lista.size() > 0){
			Materialtabelaprecoitem materialtabelaprecoitem = lista.get(0);
			materialtabelaprecoitem.setQtdeTabelaEncontrada(lista.size());
			return materialtabelaprecoitem;
		} else return null;
	}
	
	/**
	 * Busca os itens de uma tabela de pre�o
	 *
	 * @param materialtabelapreco
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/07/2014
	 */
	public List<Materialtabelaprecoitem> findByMaterialtabelapreco(Materialtabelapreco materialtabelapreco) {
		return query()
					.select("materialtabelaprecoitem.cdmaterialtabelaprecoitem, materialtabelaprecoitem.valor, materialtabelaprecoitem.valorvendamaximo, " +
							"materialtabelaprecoitem.valorvendaminimo, materialtabelaprecoitem.identificadorespecifico, materialtabelaprecoitem.identificadorfabricante, " +
							"unidademedida.cdunidademedida, material.cdmaterial, material.nome, material.identificacao, " +
							"materialtabelapreco.cdmaterialtabelapreco, materialtabelapreco.taxaacrescimo, materialtabelapreco.acrescimopadrao, materialtabelapreco.descontopadrao, " +
							"materialtabelapreco.tabelaprecotipo, materialtabelapreco.discriminardescontovenda, comissionamento.cdcomissionamento, comissionamento.comissionamentotipo, materialtabelaprecoitem.identificadorfabricante," +
							"comissionamentoItem.cdcomissionamento, comissionamentoItem.nome, comissionamentoItem.comissionamentotipo ")
					.join("materialtabelaprecoitem.material material")
					.leftOuterJoin("materialtabelaprecoitem.unidademedida unidademedida")
					.leftOuterJoin("materialtabelaprecoitem.materialtabelapreco materialtabelapreco")
					.leftOuterJoin("materialtabelapreco.comissionamento comissionamento")
					.leftOuterJoin("materialtabelaprecoitem.comissionamento comissionamentoItem")
					.where("materialtabelaprecoitem.materialtabelapreco = ?", materialtabelapreco)
					.list();
	}
	
	public List<Materialtabelaprecoitem> findForAndroid(String whereIn) {
		return query()
			.select("materialtabelaprecoitem.cdmaterialtabelaprecoitem, materialtabelaprecoitem.valor, materialtabelaprecoitem.valorvendamaximo, " +
					"materialtabelaprecoitem.valorvendaminimo, materialtabelaprecoitem.identificadorespecifico, materialtabelaprecoitem.identificadorfabricante," +
					"materialtabelaprecoitem.percentualdesconto, " +
					"unidademedida.cdunidademedida, material.cdmaterial, material.nome, material.identificacao," +
					"materialtabelapreco.cdmaterialtabelapreco")
			.join("materialtabelaprecoitem.material material")
			.join("materialtabelaprecoitem.materialtabelapreco materialtabelapreco")
			.leftOuterJoin("materialtabelaprecoitem.unidademedida unidademedida")
			.whereIn("materialtabelaprecoitem.cdmaterialtabelaprecoitem", whereIn)
			.list();
	}
	
	public Set<Material> findAtivosByWhereInMaterial(String whereIn){
		Set<Material> resultSet = new ListSet<Material>(Material.class);
		List<Materialtabelaprecoitem> itens = query()
										.select("materialtabelaprecoitem.cdmaterialtabelaprecoitem, material.cdmaterial")
										.join("materialtabelaprecoitem.material material")
										.leftOuterJoin("materialtabelaprecoitem.materialtabelapreco materialtabelapreco")
										.whereIn("material.cdmaterial", whereIn)
										.openParentheses()
											.where("materialtabelapreco.cdmaterialtabelapreco is null")
											.or()
											.openParentheses()
												.where("materialtabelapreco.dtinicio <= ?", SinedDateUtils.currentDate())
												.openParentheses()
													.where("materialtabelapreco.dtfim >= ?", SinedDateUtils.currentDate())
													.or()
													.where("materialtabelapreco.dtfim is null")
												.closeParentheses()
											.closeParentheses()
										.closeParentheses()
										.list();
		if(itens != null && !itens.isEmpty()){
			for(Materialtabelaprecoitem item : itens){
				resultSet.add(new Material(item.getMaterial().getCdmaterial()));
			}
		}
	
		return resultSet;
	}

	public void updatePreco(Materialtabelaprecoitem materialtabelaprecoitem, Double valor) {
		if(materialtabelaprecoitem != null && materialtabelaprecoitem.getCdmaterialtabelaprecoitem() != null && valor != null){
			String updRelacaoVenda = "";
			if(materialtabelaprecoitem.getPedidoVendaMaterial() != null){
				updRelacaoVenda = ", cdvendamaterial = null, cdvendaorcamentomaterial = null, cdpedidovendamaterial = "+materialtabelaprecoitem.getPedidoVendaMaterial().getCdpedidovendamaterial();
			}else if(materialtabelaprecoitem.getVendaMaterial() != null){
				updRelacaoVenda = ", cdpedidovendamaterial = null, cdvendaorcamentomaterial = null, cdvendamaterial = "+materialtabelaprecoitem.getVendaMaterial().getCdvendamaterial();
			}else if(materialtabelaprecoitem.getVendaOrcamentoMaterial() != null){
				updRelacaoVenda = ", cdvendamaterial = null, cdpedidovendamaterial = null, cdvendaorcamentomaterial = "+materialtabelaprecoitem.getVendaOrcamentoMaterial().getCdvendaorcamentomaterial();
			}
			getJdbcTemplate().execute("update Materialtabelaprecoitem set valor = " + valor + updRelacaoVenda + " where cdmaterialtabelaprecoitem = " + materialtabelaprecoitem.getCdmaterialtabelaprecoitem());
			System.out.println("update Materialtabelaprecoitem set valor = " + valor + updRelacaoVenda + " where cdmaterialtabelaprecoitem = " + materialtabelaprecoitem.getCdmaterialtabelaprecoitem());
		}
	}
	
	public void updatePercentualDesconto(Materialtabelaprecoitem materialtabelaprecoitem, Double percentualDesconto) {
		if(materialtabelaprecoitem != null && materialtabelaprecoitem.getCdmaterialtabelaprecoitem() != null && percentualDesconto != null){
			String updRelacaoVenda = "";
			if(materialtabelaprecoitem.getPedidoVendaMaterial() != null){
				updRelacaoVenda = ", cdvendamaterial = null, cdvendaorcamentomaterial = null, cdpedidovendamaterial = "+materialtabelaprecoitem.getPedidoVendaMaterial().getCdpedidovendamaterial();
			}else if(materialtabelaprecoitem.getVendaMaterial() != null){
				updRelacaoVenda = ", cdpedidovendamaterial = null, cdvendaorcamentomaterial = null, cdvendamaterial = "+materialtabelaprecoitem.getVendaMaterial().getCdvendamaterial();
			}else if(materialtabelaprecoitem.getVendaOrcamentoMaterial() != null){
				updRelacaoVenda = ", cdvendamaterial = null, cdpedidovendamaterial = null, cdvendaorcamentomaterial = "+materialtabelaprecoitem.getVendaOrcamentoMaterial().getCdvendaorcamentomaterial();
			}
			getJdbcTemplate().execute("update Materialtabelaprecoitem set percentualdesconto = " + percentualDesconto + updRelacaoVenda + " where cdmaterialtabelaprecoitem = " + materialtabelaprecoitem.getCdmaterialtabelaprecoitem());
			System.out.println("update Materialtabelaprecoitem set percentualdesconto = " + percentualDesconto + updRelacaoVenda + " where cdmaterialtabelaprecoitem = " + materialtabelaprecoitem.getCdmaterialtabelaprecoitem());
		}
	}
	
	public Boolean existeMaterialtabelaprecoitem(Materialtabelapreco materialtabelapreco){
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.from(Materialtabelaprecoitem.class)
				.where("materialtabelaprecoitem.materialtabelapreco = ?", materialtabelapreco)
				.unique() > 0;
	}
	
	public Materialtabelaprecoitem loadByVendaMaterial(Vendamaterial vendaMaterial){
		return query()
				.select("materialtabelaprecoitem.cdmaterialtabelaprecoitem, materialtabelaprecoitem.valor, materialtabelaprecoitem.percentualdesconto, "+
						"materialtabelapreco.tabelaprecotipo, vendaMaterial.cdvendamaterial")
				.join("materialtabelaprecoitem.materialtabelapreco materialtabelapreco")
				.join("materialtabelaprecoitem.vendaMaterial vendaMaterial")
				.where("vendaMaterial = ?", vendaMaterial)
				.setMaxResults(1)
				.unique();
	}
	
	public Materialtabelaprecoitem loadByPedidoVendaMaterial(Pedidovendamaterial pedidoVendaMaterial){
		return query()
				.select("materialtabelaprecoitem.cdmaterialtabelaprecoitem, materialtabelaprecoitem.valor, materialtabelaprecoitem.percentualdesconto, "+
						"materialtabelapreco.tabelaprecotipo, pedidoVendaMaterial.cdpedidovendamaterial")
				.join("materialtabelaprecoitem.materialtabelapreco materialtabelapreco")
				.join("materialtabelaprecoitem.pedidoVendaMaterial pedidoVendaMaterial")
				.where("pedidoVendaMaterial = ?", pedidoVendaMaterial)
				.setMaxResults(1)
				.unique();
	}
	
	public Materialtabelaprecoitem loadByVendaOrcamentoMaterial(Vendaorcamentomaterial vendaOrcamentoMaterial){
		return query()
				.select("materialtabelaprecoitem.cdmaterialtabelaprecoitem, materialtabelaprecoitem.valor, materialtabelaprecoitem.percentualdesconto, "+
						"materialtabelapreco.tabelaprecotipo, vendaOrcamentoMaterial.cdvendaorcamentomaterial")
				.join("materialtabelaprecoitem.materialtabelapreco materialtabelapreco")
				.join("materialtabelaprecoitem.vendaOrcamentoMaterial vendaOrcamentoMaterial")
				.where("vendaOrcamentoMaterial = ?", vendaOrcamentoMaterial)
				.setMaxResults(1)
				.unique();
	}
}
