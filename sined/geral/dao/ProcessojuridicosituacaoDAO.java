package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Processojuridicosituacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("processojuridicosituacao.nome")
public class ProcessojuridicosituacaoDAO extends GenericDAO<Processojuridicosituacao>{

}
