package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.AnimalVacina;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AnimalVacinaDAO extends GenericDAO<AnimalVacina> {

	
	/**
	 * M�todo respons�vel por buscar Animais que necessitam de vacina��o
	 * @param proximaVacina
	 * @return
	 * @since 28/04/2016
	 * @author C�sar
	 */
	public List<AnimalVacina> findForDataVacina(Date proximaVacina){
		if(proximaVacina == null){
			throw new SinedException("Par�metro incorreto para consulta");
		}
		
		return query()
					.select("animalVacina.cdanimalvacina, animalVacina.proximaaplicacao, animal.cdanimal, animal.nome," +
							"cliente.cdpessoa, cliente.email, cliente.nome, produto.cdmaterial,produto.nome, " +
							"animalSituacao.cdanimalsituacao, animalSituacao.obito")
					.join("animalVacina.animal animal")
					.join("animal.cliente cliente")
					.join("animalVacina.produto produto")
					.join("animal.animalSituacao animalSituacao")
					.leftOuterJoin("animalVacina.animalVacinaProxima animalVacinaProxima")
					.where("coalesce(animalSituacao.obito, false)=false")
					.where("animalVacina.proximaaplicacao is not null")
					.where("animalVacinaProxima is null")
					.where("cliente.email is not null")
					.where("animalVacina.proximaaplicacao = ?", proximaVacina)
					.orderBy("animalVacina.proximaaplicacao, produto.nome")
					.list();
	}
}
