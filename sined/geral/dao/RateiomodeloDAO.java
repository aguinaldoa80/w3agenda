package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Rateiomodelo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.RateiomodeloFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RateiomodeloDAO extends GenericDAO<Rateiomodelo>{

	@Override
	public void updateListagemQuery(QueryBuilder<Rateiomodelo> query,
			FiltroListagem _filtro) {
		RateiomodeloFiltro filtro = (RateiomodeloFiltro)_filtro;
		query.select("rateiomodelo.nome, rateiomodelo.cdrateiomodelo, tipooperacao.nome")
			.leftOuterJoin("rateiomodelo.tipooperacao tipooperacao")
			.where("rateiomodelo.tipooperacao = ?", filtro.getTipooperacao());
		super.updateListagemQuery(query, filtro);
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Rateiomodelo> query) {
		query.select("rateiomodelo.cdrateiomodelo, rateiomodelo.nome, tipooperacao.nome, tipooperacao.cdtipooperacao, " +
					"centrocusto.cdcentrocusto, centrocusto.nome, projeto.cdprojeto, projeto.nome, contagerencial.nome, " +
					"contagerencial.cdcontagerencial, rateiomodeloitem.percentual")
		.leftOuterJoin("rateiomodelo.listaRateiomodeloitem rateiomodeloitem")
		.leftOuterJoin("rateiomodelo.tipooperacao tipooperacao")
		.leftOuterJoin("rateiomodeloitem.projeto projeto")
		.leftOuterJoin("rateiomodeloitem.contagerencial contagerencial")
		.leftOuterJoin("rateiomodeloitem.centrocusto centrocusto");
		super.updateEntradaQuery(query);
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
//		Rateiomodelo rateiomodelo = (Rateiomodelo)save.getEntity();
		save.saveOrUpdateManaged("listaRateiomodeloitem");
	}
	
	public Rateiomodelo loadForRateio(Rateiomodelo rateiomodelo){
		return query().select("rateiomodelo.cdrateiomodelo, rateiomodelo.nome, tipooperacao.nome, tipooperacao.cdtipooperacao, " +
					"centrocusto.cdcentrocusto, centrocusto.nome, projeto.cdprojeto, projeto.nome, contagerencial.nome, " +
					"contagerencial.cdcontagerencial, rateiomodeloitem.percentual")
		.leftOuterJoin("rateiomodelo.listaRateiomodeloitem rateiomodeloitem")
		.leftOuterJoin("rateiomodelo.tipooperacao tipooperacao")
		.leftOuterJoin("rateiomodeloitem.projeto projeto")
		.leftOuterJoin("rateiomodeloitem.contagerencial contagerencial")
		.leftOuterJoin("rateiomodeloitem.centrocusto centrocusto")
		.where("rateiomodelo = ?", rateiomodelo)
		.unique();
	}
	
	public Rateiomodelo findByNome(String nome){
		return query().select("rateiomodelo.cdrateiomodelo, rateiomodelo.nome")
						.where("retira_acento(upper(rateiomodelo.nome)) = ?", Util.strings.tiraAcento(Util.strings.emptyIfNull(nome)).toUpperCase())
						.unique();
	}

	@Override
	public ListagemResult<Rateiomodelo> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Rateiomodelo> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("rateiomodelo.cdrateiomodelo");
		
		return new ListagemResult<Rateiomodelo>(query, filtro);
	}
}
