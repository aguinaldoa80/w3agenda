package br.com.linkcom.sined.geral.dao;


import java.util.List;

import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaocontabil;
import br.com.linkcom.sined.geral.bean.Movimentacaocontabilorigem;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoLancamentoEnum;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MovimentacaocontabilorigemDAO extends GenericDAO<Movimentacaocontabilorigem>{
	
	public List<Movimentacaocontabilorigem> findByMovimentacaocontabil(Movimentacaocontabil movimentacaocontabil){
		return query()
				.select("nota.cdNota, notaTipo.cdNotaTipo, entregadocumento.cdentregadocumento," +
						"documento.cddocumento, documentoclasse.cddocumentoclasse, movimentacao.cdmovimentacao, venda.cdvenda")
				.leftOuterJoin("movimentacaocontabilorigem.nota nota")
				.leftOuterJoin("nota.notaTipo notaTipo")
				.leftOuterJoin("movimentacaocontabilorigem.entregadocumento entregadocumento")
				.leftOuterJoin("movimentacaocontabilorigem.documento documento")
				.leftOuterJoin("documento.documentoclasse documentoclasse")
				.leftOuterJoin("movimentacaocontabilorigem.movimentacao movimentacao")
				.leftOuterJoin("movimentacaocontabilorigem.venda venda")
				.where("movimentacaocontabilorigem.movimentacaocontabil=?", movimentacaocontabil)
				.list();
	}
	
	public Boolean existeLancamentoContabilMovimentacao(Movimentacao movimentacao, MovimentacaocontabilTipoLancamentoEnum tipoLancamentoEnum){
		if(movimentacao == null || movimentacao.getCdmovimentacao() == null || tipoLancamentoEnum == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Movimentacaocontabilorigem.class)
			.where("movimentacaocontabilorigem.movimentacao = ?", movimentacao)
			.where("movimentacaocontabilorigem.movimentacaocontabil.tipoLancamento = ?", tipoLancamentoEnum)
			.unique() > 0;
	}
}
