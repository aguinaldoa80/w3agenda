package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialpneumodelo;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialpneumodeloDAO extends GenericDAO<Materialpneumodelo>{

	/**
	* M�todo que busca os modelos vinculados ao material
	*
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/12/2016
	 */
	public List<Materialpneumodelo> findByMaterial(Material material){
		if(material == null || material.getCdmaterial() == null){
			throw new SinedException("Material n�o pode ser nulo.");
		}
		
		return query()
				.select("materialpneumodelo.cdmaterialpneumodelo, pneumodelo.cdpneumodelo, pneumarca.cdpneumarca, pneumedida.cdpneumedida")
				.leftOuterJoin("materialpneumodelo.pneumedida pneumedida")
				.leftOuterJoin("materialpneumodelo.pneumodelo pneumodelo")
				.leftOuterJoin("pneumodelo.pneumarca pneumarca")
				.where("materialpneumodelo.material = ?", material)
				.orderBy("pneumarca.nome, pneumodelo.nome")
				.list();
	}
	
	/**
	 * Busca os registros para enviar para o W3Produ��o
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/12/2016
	 */
	public List<Materialpneumodelo> findForW3Producao(String whereIn){
		return query()
					.select("materialpneumodelo.cdmaterialpneumodelo, material.cdmaterial, pneumodelo.cdpneumodelo, pneumedida.cdpneumedida")
					.join("materialpneumodelo.material material")
					.join("materialpneumodelo.pneumodelo pneumodelo")
					.leftOuterJoin("materialpneumodelo.pneumedida pneumedida")
					.whereIn("materialpneumodelo.cdmaterialpneumodelo", whereIn)
					.list();
	}
}
