package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotafiscalprodutoProducaoordem;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotafiscalprodutoProducaoordemDAO extends GenericDAO<NotafiscalprodutoProducaoordem> {

	/**
	 * Verifica se existe alguma nota de devolu��o vinculada �s Producaoordems que n�o est�o canceladas.
	 *
	 * @param whereInProducaoordem
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/10/2015
	 */
	public boolean haveNotaDevolucaoNaoCanceladaByProducaoordem(String whereInProducaoordem, String whereInPedidovendamaterial) {
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(NotafiscalprodutoProducaoordem.class)
				.join("notafiscalprodutoProducaoordem.notafiscalproduto notafiscalproduto")
				.join("notafiscalproduto.listaItens listaItens")
				.where("notafiscalprodutoProducaoordem.devolucao = ?", Boolean.TRUE)
				.whereIn("notafiscalprodutoProducaoordem.producaoordem.cdproducaoordem", whereInProducaoordem)
				.whereIn("listaItens.pedidovendamaterial.cdpedidovendamaterial", whereInPedidovendamaterial)
				.where("notafiscalproduto.notaStatus <> ?", NotaStatus.CANCELADA)
				.unique() > 0;
	}

}
