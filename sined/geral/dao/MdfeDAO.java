package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.enumeration.MdfeSituacao;
import br.com.linkcom.sined.geral.service.MdfeService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.MdfeFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MdfeDAO extends GenericDAO<Mdfe>{

	private MdfeService mdfeService;
	
	public void setMdfeService(MdfeService mdfeService) {
		this.mdfeService = mdfeService;
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Mdfe> query, FiltroListagem _filtro) {
		MdfeFiltro filtro = (MdfeFiltro) _filtro;
		
		query
			.select("mdfe.cdmdfe, mdfe.mdfeSituacao, mdfe.serie, mdfe.numero, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, mdfe.dataHoraEmissao, mdfe.dataHoraInicioViagem, mdfe.tipoEmitenteMdfe, " +
					"mdfe.tipoTransportador, mdfe.modalidadeFrete, mdfe.formaEmissao ")
			.leftOuterJoin("mdfe.empresa empresa")
			.where("mdfe.serie = ?", filtro.getSerie())
			.where("mdfe.numero = ?", filtro.getNumero())
			.where("empresa = ?", filtro.getEmpresa())
			.where("mdfe.dataHoraEmissao >= ?", filtro.getDataHoraEmissaoInicio())
			.where("mdfe.dataHoraEmissao <= ?", filtro.getDataHoraEmissaoFim())
			.where("mdfe.dataHoraInicioViagem >= ?", filtro.getDataHoraInicioViagemInicio())
			.where("mdfe.dataHoraInicioViagem <= ?", filtro.getDataHoraInicioViagemFim())
			.where("mdfe.tipoEmitenteMdfe = ?", filtro.getTipoEmitenteMdfe())
			.where("mdfe.tipoTransportador = ?", filtro.getTipoTransportador())
			.where("mdfe.modalidadeFrete = ?", filtro.getModalidadeFrete())
			.where("mdfe.formaEmissao = ?", filtro.getFormaEmissao())
			.orderBy("mdfe.cdmdfe desc")
			;
			
		if(SinedUtil.isListNotEmpty(filtro.getListaMdfeSituacao())){
			query.whereIn("mdfe.mdfeSituacao", MdfeSituacao.listAndConcatenate(filtro.getListaMdfeSituacao()));
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Mdfe> query) {
		query.select(
				"mdfe.cdmdfe, mdfe.mdfeSituacao, mdfe.serie, " +
				"mdfe.numero, " + 
				"empresa.cdpessoa, empresa.nome, empresa.cnpj, empresa.inscricaoestadual, empresa.email, " +
				"mdfe.dataHoraEmissao, mdfe.dataHoraInicioViagem, " +
				"uf.cduf, uf.sigla, uf.cdibge, " + 
				"mdfe.tipoEmitenteMdfe, mdfe.tipoTransportador,	mdfe.modalidadeFrete, mdfe.formaEmissao, " + 
				"ufLocalDescarregamento.cduf, ufLocalDescarregamento.sigla, ufLocalDescarregamento.cdibge, " + 
				"ufLocalCarregamento.cduf, ufLocalCarregamento.sigla, ufLocalCarregamento.cdibge, " +
				"mdfe.valorTotalCargaTransportada," +
				"mdfe.unidadeMedidaPesoBrutoCarga, " + 
				"mdfe.pesoBrutoTotalCarga, " + 
				"mdfe.cdusuarioaltera, mdfe.dtaltera, mdfe.infoAdicionaisFisco,	mdfe.infoAdicionaisContribuinte, mdfe.rntrc, " +
				"mdfe.codigoAgendamentoPorto, mdfe.codigoInternoVeiculo, mdfe.tipoCarroceria, mdfe.placa, mdfe.tara,"  +
				"mdfe.renavam, mdfe.carregamentoNoExterior, mdfe.descarregamentoNoExterior, " +
				"ufFreteRodoviario.cduf, ufFreteRodoviario.sigla, ufFreteRodoviario.cdibge, " +
				"mdfe.tipoRodado, mdfe.capacidadeKgRodoviario, mdfe.capacidadeM3Rodoviario,	mdfe.pertenceEmpresaEmitente, " +
				"mdfe.tipoPessoaProprietarioRodoviario,	mdfe.nomeProprietarioRodoviario, mdfe.cpfProprietarioRodoviario, " +
				"mdfe.cnpjProprietarioRodoviario, mdfe.inscEstProprietarioRodoviario, mdfe.isentoRodoviario, mdfe.rntrcProprietario, " +
				"ufProprietarioRodoviario.cduf, ufProprietarioRodoviario.sigla, ufProprietarioRodoviario.cdibge, " + 
				"mdfe.tipoProprietarioVeiculoRodoviario, mdfe.marcaNacionalidadeAeronave, mdfe.marcaMatriculaAeronave, " +
				"mdfe.numeroVoo, mdfe.aerodromoEmbarque, mdfe.aerodromoDestino, mdfe.dataVoo, mdfe.irinNavio, " +
				"mdfe.codigoTipoEmbarcacao,	mdfe.codigoEmbarcacao, mdfe.numeroViagemEmbarcacao, mdfe.nomeEmbarcacao, " + 
				"mdfe.codigoPortoEmbarque, mdfe.codigoPortoDestino,	mdfe.portoTransbordo, mdfe.tipoNavegacao, mdfe.qrCode, " +
				"mdfe.prefixoTrem, mdfe.dataHoraLiberacaoTremOrigem, mdfe.origemTrem, mdfe.destinoTrem,	mdfe.qtdeVagoesCarregados, mdfe.projetoCanalVerde ")
			.leftOuterJoin("mdfe.empresa empresa")
			.leftOuterJoin("mdfe.uf uf")
			.leftOuterJoin("mdfe.ufLocalDescarregamento ufLocalDescarregamento")
			.leftOuterJoin("mdfe.ufLocalCarregamento ufLocalCarregamento")
			.leftOuterJoin("mdfe.ufFreteRodoviario ufFreteRodoviario")
			.leftOuterJoin("mdfe.ufProprietarioRodoviario ufProprietarioRodoviario");
	}
	
	public List<Mdfe> findForDamdfe(String itensSelecionados) {
		QueryBuilder<Mdfe> query = querySined();
		updateEntradaQuery(query);

		query
		.select(query.getSelect().getValue() + ", endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, ufEmpresa.sigla, " +
				"municipioEmpresa.nome, endereco.cep, enderecotipo.cdenderecotipo, logomarca.cdarquivo ")
		.leftOuterJoin("empresa.logomarca logomarca")
		.leftOuterJoin("empresa.listaEndereco endereco")
		.leftOuterJoin("endereco.municipio municipioEmpresa")
		.leftOuterJoin("municipioEmpresa.uf ufEmpresa")
		.leftOuterJoin("endereco.enderecotipo enderecotipo");
		
		return query
				.whereIn("mdfe.cdmdfe", itensSelecionados)
				.list();
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaLocalCarregamento");
		save.saveOrUpdateManaged("listaLocalDescarregamento");
		save.saveOrUpdateManaged("listaUfPercurso");
		save.saveOrUpdateManaged("listaLacre");
		save.saveOrUpdateManaged("listaAutorizacaoDownload");
		save.saveOrUpdateManaged("listaCondutor");
		save.saveOrUpdateManaged("listaReboque");
		save.saveOrUpdateManaged("listaCiot");
		save.saveOrUpdateManaged("listaContratante");
		save.saveOrUpdateManaged("listaValePedagio");
		save.saveOrUpdateManaged("listaAquaviarioTerminalCarregamento");
		save.saveOrUpdateManaged("listaAquaviarioTerminalDescarregamento");
		save.saveOrUpdateManaged("listaAquaviarioUnidadeCargaVazia");
		save.saveOrUpdateManaged("listaAquaviarioUnidadeTransporteVazia");
		save.saveOrUpdateManaged("listaAquaviarioEmbarcacaoComboio");
		save.saveOrUpdateManaged("listaVagao");
		save.saveOrUpdateManaged("listaMdfeCte");
		save.saveOrUpdateManaged("listaInformacaoUnidadeTransporteCte");
		save.saveOrUpdateManaged("listaInformacaoUnidadeCargaCte");
		save.saveOrUpdateManaged("listaLacreUnidadeTransporteCte");
		save.saveOrUpdateManaged("listaLacreUnidadeCargaCte");
		save.saveOrUpdateManaged("listaProdutoPerigosoCte");
		save.saveOrUpdateManaged("listaNfe");
		save.saveOrUpdateManaged("listaInformacaoUnidadeTransporteNfe");
		save.saveOrUpdateManaged("listaInformacaoUnidadeCargaNfe");
		save.saveOrUpdateManaged("listaLacreUnidadeTransporteNfe");
		save.saveOrUpdateManaged("listaLacreUnidadeCargaNfe");
		save.saveOrUpdateManaged("listaProdutoPerigosoNfe");
		save.saveOrUpdateManaged("listaMdfeReferenciado");
		save.saveOrUpdateManaged("listaInformacaoUnidadeTransporteMdfeReferenciado");
		save.saveOrUpdateManaged("listaInformacaoUnidadeCargaMdfeReferenciado");
		save.saveOrUpdateManaged("listaLacreUnidadeTransporteMdfeReferenciado");
		save.saveOrUpdateManaged("listaLacreUnidadeCargaMdfeReferenciado");
		save.saveOrUpdateManaged("listaProdutoPerigosoMdfeReferenciado");
		save.saveOrUpdateManaged("listaSeguroCarga");
		save.saveOrUpdateManaged("listaSeguroCargaNumeroAverbacao");

		super.updateSaveOrUpdate(save);
	}
	
	public Boolean alterarStatusAcao(final Mdfe mdfe, final String observacao) {
		Object sucesso = getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(final TransactionStatus status) {
				return mdfeService.alterarStatusAcaoSemTransacao(mdfe, observacao);
			}
		});
		
		return (Boolean) sucesso;
	}
	
	public void atualizaMdfeSituacao(Mdfe mdfe, MdfeSituacao mdfeSituacao) {
		if (mdfe == null || mdfe.getCdmdfe() == null) {
			throw new SinedException("A MDF-e n�o pode ser nula.");
		}
		if (mdfeSituacao == null) {
			throw new SinedException("A situa��o da MDF-e n�o pode ser nula.");
		}

		getJdbcTemplate().execute("UPDATE MDFE SET MDFESITUACAO = " + mdfeSituacao.ordinal() + " WHERE CDMDFE = " + mdfe.getCdmdfe());
	}
	
	public List<Mdfe> findByNotInSituacoes(String whereIn, MdfeSituacao... situacoes){
		if(StringUtils.isEmpty(whereIn) || situacoes.length == 0)
			return new ArrayList<Mdfe>();
		
		String whereInSituacao = null;
		StringBuilder values = new StringBuilder();
		for (int i=0; i < situacoes.length; i++) {
			values.append(situacoes[i].getValue()).append(",");
		}
		whereInSituacao = values.toString().substring(0, values.toString().length()-1);
		return query()
			.select("mdfe.cdmdfe, mdfe.mdfeSituacao")
			.where("mdfe.mdfeSituacao not in (" + whereInSituacao + ")")
			.list();
	}
	
	public boolean haveMDFeDiferenteStatus(String whereIn, MdfeSituacao[] status) {
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
									.select("count(*)")
									.from(Mdfe.class)
									.setUseTranslator(false)									
									.whereIn("mdfe.cdmdfe", whereIn);
		
		for (int i = 0; i < status.length; i++) {
			query.where("mdfe.mdfeSituacao <> ?", status[i]);
		}
		
		return query.unique() > 0;
	}
	
	public void updateNumeroMdfe(Mdfe mdfe, Integer proxNum) {
		if(mdfe == null || mdfe.getCdmdfe() == null) {
			throw new SinedException("Nota n�o pode ser nula.");
		}
		
		if(proxNum == null) {
			throw new SinedException("O n�mero da nota n�o pode ser nulo.");
		}
		
		getJdbcTemplate().execute("UPDATE MDFE SET NUMERO = '" + proxNum + "' WHERE CDMDFE = " + mdfe.getCdmdfe());
	}

	public void alterarQrCode(Mdfe mdfe) {
		if (mdfe == null || StringUtils.isEmpty(mdfe.getQrCode())) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		getHibernateTemplate().bulkUpdate("update Mdfe m set m.qrCode = ? where m.id = ?", 
				new Object[] {mdfe.getQrCode(), mdfe.getCdmdfe()});
	}
}
