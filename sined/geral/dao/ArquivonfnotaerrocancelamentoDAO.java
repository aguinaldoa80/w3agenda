package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Arquivonfnotaerrocancelamento;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ArquivonfnotaerrocancelamentoDAO extends GenericDAO<Arquivonfnotaerrocancelamento> {

	/**
	 * Busca os registros de Arquivonfnotaerrocancelamento a partir de um Arquivonfnota.
	 *
	 * @param arquivonfnota
	 * @return
	 * @since 05/03/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonfnotaerrocancelamento> findByArquivonfnota(Arquivonfnota arquivonfnota) {
		if(arquivonfnota == null || arquivonfnota.getCdarquivonfnota() == null){
			throw new SinedException("Arquivonfnota n�o pode ser nulo.");
		}
		return query()
					.select("arquivonfnotaerrocancelamento.cdarquivonfnotaerrocancelamento, arquivonfnotaerrocancelamento.mensagem")
					.join("arquivonfnotaerrocancelamento.arquivonfnota arquivonfnota")
					.where("arquivonfnota = ?", arquivonfnota)
					.orderBy("arquivonfnotaerrocancelamento.cdarquivonfnotaerrocancelamento")
					.list();
	}

}
