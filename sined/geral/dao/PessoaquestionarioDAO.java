package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Agendainteracao;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Tipopessoaquestionario;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.FornecedorFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.Pessoaquestionario;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PessoaquestionarioDAO extends GenericDAO<Pessoaquestionario>{
	
	/**
	 * 
	 * M�todo para carregar a lista com todos os questionarios de um fornecedor
	 *
	 *@author Thiago Augusto
	 *@date 06/03/2012
	 * @param bean
	 * @return
	 */
	public List<Pessoaquestionario> carregarPessoaQuestinario(Fornecedor bean){
		return query()
					.select("pessoaquestionario.cdpessoaquestionario, pessoaquestionario.situacao, pessoaquestionario.pontuacao, pessoaquestionario.dtaltera, pessoaquestionario.cdusuarioaltera, " +
							"pessoa.cdpessoa, listaPessoaQuestionarioQuestao.cdpessoaquestionarioquestao, listaPessoaQuestionarioQuestao.resposta, listaPessoaQuestionarioQuestao.sim, " +
							"questionarioquestao.cdquestionarioquestao, questionarioquestao.questao, questionarioquestao.tiporesposta, " +
							"questionarioquestao.pontuacaosim, questionarioquestao.pontuacaonao, questionarioquestao.pontuacaoquestao")
					.leftOuterJoin("pessoaquestionario.pessoa pessoa")
					.leftOuterJoin("pessoaquestionario.listaPessoaQuestionarioQuestao listaPessoaQuestionarioQuestao")
					.leftOuterJoin("listaPessoaQuestionarioQuestao.questionarioquestao questionarioquestao")
					.where("pessoa = ? ", bean)
					.orderBy("pessoaquestionario.dtaltera desc, pessoaquestionario.cdpessoaquestionario desc, listaPessoaQuestionarioQuestao.cdpessoaquestionarioquestao asc")
					.list();
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				
				save.saveOrUpdateManaged("listaPessoaQuestionarioQuestao");
				
				return null;
			}
		});
	}

	/**
	 * 
	 * M�todo que faz a exclus�o a partir da tela de Fornecedor.
	 *
	 *@author Thiago Augusto
	 *@date 07/03/2012
	 * @param whereIn
	 * @param fornecedor
	 */
	public void excluirDaListaPessoa(String whereIn, Fornecedor fornecedor){
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM Pessoaquestionario");
		sql.append(" WHERE cdpessoa = ");
		sql.append(fornecedor.getCdpessoa());
		sql.append(" AND cdpessoaquestionario NOT IN (");
		sql.append(whereIn);
		sql.append(")");
		
		getJdbcTemplate().execute(sql.toString());
	}
	
	/**
	 * 
	 * M�todo que faz a exclus�o da lista inteira a partir de um fornecedor
	 *
	 *@author Thiago Augusto
	 *@date 09/03/2012
	 * @param whereIn
	 * @param fornecedor
	 */
	public void excluirListaCompletaPessoa(Fornecedor fornecedor){
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM Pessoaquestionario");
		sql.append(" WHERE cdpessoa = ");
		sql.append(fornecedor.getCdpessoa());
		
		getJdbcTemplate().execute(sql.toString());
	}
	
	/**
	 * 
	 * @param whereIn
	 * @param filtro
	 * @author Thiago Clemente
	 * 
	 */
	public List<Pessoaquestionario> findForFornecedorCSVListagem(String whereIn, FornecedorFiltro filtro){
		return query()
				.select("pessoaquestionario.pontuacao, pessoaquestionario.dtaltera, pessoa.cdpessoa, "+
						"questionario.cdquestionario, questionario.quantidadeNotasMedia")
				.join("pessoaquestionario.pessoa pessoa")
				.join("pessoaquestionario.questionario questionario")
				.where("pessoaquestionario.pontuacao is not null")
				.whereIn("pessoa.cdpessoa", whereIn)
				.where("pessoaquestionario.dtaltera>=?", filtro.getDtAvaliacaoInicio())
				.where("pessoaquestionario.dtaltera<=?", filtro.getDtAvaliacaoFim())
				.orderBy("pessoaquestionario.dtaltera desc")
				.list();
	}
	
	/**
	 * 
	 * M�todo para carregar um questionario a partir de um pessoaquestionario
	 *
	 *@author Jo�o Vitor
	 *@date 13/10/2014
	 * @param bean
	 * @return
	 */
	public Pessoaquestionario findPessoaQuestionario(Pessoaquestionario bean){
		return query().select("pessoaquestionario.cdpessoaquestionario, pessoaquestionario.situacao, pessoaquestionario.pontuacao, pessoaquestionario.dtaltera, pessoaquestionario.cdusuarioaltera, " +
				"pessoa.cdpessoa, listaPessoaQuestionarioQuestao.cdpessoaquestionarioquestao, listaPessoaQuestionarioQuestao.resposta, listaPessoaQuestionarioQuestao.sim, " +
				"questionarioquestao.cdquestionarioquestao, questionarioquestao.questao, questionarioquestao.tiporesposta, " +
		"questionarioquestao.pontuacaosim, questionarioquestao.pontuacaonao, questionarioquestao.pontuacaoquestao")
		.leftOuterJoin("pessoaquestionario.pessoa pessoa")
		.leftOuterJoin("pessoaquestionario.listaPessoaQuestionarioQuestao listaPessoaQuestionarioQuestao")
		.leftOuterJoin("listaPessoaQuestionarioQuestao.questionarioquestao questionarioquestao")
		.where("pessoaquestionario = ? ", bean)
		.orderBy("listaPessoaQuestionarioQuestao.cdpessoaquestionarioquestao")
		.unique();
	}
	
	/**
	 * 
	 * M�todo para carregar a lista com todos os questionarios de uma pessoa
	 *
	 *@author Mairon Cezar
	 *@date 18/08/2017
	 * @param bean
	 * @return
	 */
	public List<Pessoaquestionario> carregarPessoaQuestionario(Pessoa bean){
		return query()
					.select("pessoaquestionario.cdpessoaquestionario, pessoaquestionario.situacao, pessoaquestionario.pontuacao, pessoaquestionario.dtaltera, pessoaquestionario.cdusuarioaltera, " +
							"pessoa.cdpessoa, listaPessoaQuestionarioQuestao.cdpessoaquestionarioquestao, listaPessoaQuestionarioQuestao.resposta, listaPessoaQuestionarioQuestao.sim, listaPessoaQuestionarioQuestao.pontuacao, " +
							"questionarioquestao.cdquestionarioquestao, questionarioquestao.questao, questionarioquestao.tiporesposta, " +
							"questionarioquestao.pontuacaosim, questionarioquestao.pontuacaonao, questionarioquestao.pontuacaoquestao")
					.join("pessoaquestionario.pessoa pessoa")
					.leftOuterJoin("pessoaquestionario.listaPessoaQuestionarioQuestao listaPessoaQuestionarioQuestao")
					.leftOuterJoin("listaPessoaQuestionarioQuestao.questionarioquestao questionarioquestao")
					.join("pessoaquestionario.questionario questionario")
					.where("pessoa = ? ", bean)
					.where("questionario.tipopessoaquestionario <> ?", Tipopessoaquestionario.SERVICO)
					.orderBy("pessoaquestionario.dtaltera desc, pessoaquestionario.cdpessoaquestionario desc, listaPessoaQuestionarioQuestao.cdpessoaquestionarioquestao asc")
					.list();
	}
	
	/**
	 * 
	 * M�todo para carregar a lista com todos os questionarios de uma pessoa do tipo SERVICO
	 *
	 *@author Mairon Cezar
	 *@date 26/12/2017
	 * @param bean
	 * @return
	 */
	public List<Pessoaquestionario> carregarPessoaquestionarioByInteracao(Pessoa bean){
		return query()
					.select("pessoaquestionario.cdpessoaquestionario, pessoaquestionario.situacao, pessoaquestionario.pontuacao, pessoaquestionario.dtaltera, pessoaquestionario.cdusuarioaltera, " +
							"pessoa.cdpessoa, listaPessoaQuestionarioQuestao.cdpessoaquestionarioquestao, listaPessoaQuestionarioQuestao.resposta, listaPessoaQuestionarioQuestao.sim, listaPessoaQuestionarioQuestao.pontuacao, " +
							"questionarioquestao.cdquestionarioquestao, questionarioquestao.questao, questionarioquestao.tiporesposta, " +
							"questionarioquestao.pontuacaosim, questionarioquestao.pontuacaonao, questionarioquestao.pontuacaoquestao")
					.join("pessoaquestionario.pessoa pessoa")
					.join("pessoaquestionario.listaPessoaQuestionarioQuestao listaPessoaQuestionarioQuestao")
					.join("listaPessoaQuestionarioQuestao.questionarioquestao questionarioquestao")
					.join("pessoaquestionario.questionario questionario")
					.where("pessoa = ? ", bean)
					.where("questionario.tipopessoaquestionario = ?", Tipopessoaquestionario.SERVICO)
					.orderBy("pessoaquestionario.dtaltera desc, pessoaquestionario.cdpessoaquestionario desc, listaPessoaQuestionarioQuestao.cdpessoaquestionarioquestao asc")
					.list();
	}
	
	public List<Pessoaquestionario> carregarPessoaquestionarioByAgendaInteracao(Agendainteracao agendainteracao){
		return query()
					.select("pessoaquestionario.cdpessoaquestionario, pessoaquestionario.situacao, pessoaquestionario.pontuacao, pessoaquestionario.dtaltera, pessoaquestionario.cdusuarioaltera, " +
							"pessoa.cdpessoa, listaPessoaQuestionarioQuestao.cdpessoaquestionarioquestao, listaPessoaQuestionarioQuestao.resposta, listaPessoaQuestionarioQuestao.sim, listaPessoaQuestionarioQuestao.pontuacao, " +
							"questionarioquestao.cdquestionarioquestao, questionarioquestao.questao, questionarioquestao.tiporesposta, " +
							"questionarioquestao.pontuacaosim, questionarioquestao.pontuacaonao, questionarioquestao.pontuacaoquestao")
					.join("pessoaquestionario.pessoa pessoa")
					.join("pessoaquestionario.listaPessoaQuestionarioQuestao listaPessoaQuestionarioQuestao")
					.join("listaPessoaQuestionarioQuestao.questionarioquestao questionarioquestao")
					.join("pessoaquestionario.questionario questionario")
					.where("pessoaquestionario.agendainteracao = ? ", agendainteracao)
					.where("questionario.tipopessoaquestionario = ?", Tipopessoaquestionario.SERVICO)
					.orderBy("pessoaquestionario.dtaltera desc, pessoaquestionario.cdpessoaquestionario desc, listaPessoaQuestionarioQuestao.cdpessoaquestionarioquestao asc")
					.list();
	}
}