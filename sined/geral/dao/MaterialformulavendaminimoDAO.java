package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialformulavendaminimo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("materialformulavendaminimo.ordem")
public class MaterialformulavendaminimoDAO extends GenericDAO<Materialformulavendaminimo>{

	/**
	* M�todo que busca a lista de f�rmula para c�lculo do vendaminimo
	*
	* @param material
	* @return
	* @since 18/05/2015
	* @author Luiz Fernando
	*/
	public List<Materialformulavendaminimo> findByMaterial(Material material) {
		return query()
					.select("materialformulavendaminimo.cdmaterialformulavendaminimo, materialformulavendaminimo.ordem, " +
							"materialformulavendaminimo.identificador, materialformulavendaminimo.formula")
					.where("materialformulavendaminimo.material = ?", material)
					.orderBy("materialformulavendaminimo.ordem, materialformulavendaminimo.identificador")
					.list();
	}

	public void deleteFormulavendaminimoByMaterial(Material material) {
		if(material != null && material.getCdmaterial() != null){
			getJdbcTemplate().execute("delete from materialformulavendaminimo where cdmaterial = " + material.getCdmaterial());
		}
	}

}
