package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Plataforma;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("plataforma.nome")
public class PlataformaDAO extends GenericDAO<Plataforma>{
	
	/**
	 * Retorna um objeto de acordo com o nome informado
	 * @param nome
	 * @return
	 * @author Thiago Gon�alves
	 */
	public Plataforma findByNome(String nome){
		return query().whereLikeIgnoreAll("nome", nome)
					  .setMaxResults(1)
					  .unique();
	}
	
}
