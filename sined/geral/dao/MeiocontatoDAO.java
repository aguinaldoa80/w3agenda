package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Meiocontato;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MeiocontatoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MeiocontatoDAO extends GenericDAO<Meiocontato> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Meiocontato> query,FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		}
		
		MeiocontatoFiltro filtro = (MeiocontatoFiltro) _filtro;
		
		query
		.select("meiocontato.cdmeiocontato, meiocontato.nome, meiocontato.padrao, meiocontato.ativo")
		.whereLikeIgnoreAll("meiocontato.nome", filtro.getNome());
		
		
		if(filtro.getAtivo() != null && filtro.getAtivo()){
			query
			.where("meiocontato.ativo = ?", Boolean.TRUE);
		}else{
			query.where("meiocontato.ativo = ?", Boolean.FALSE)
			.or()
			.where("meiocontato.ativo is null");
		}
	}

	/**
	 * Verifica se j� existe algum meiocontato marcado
	 * como padr�o
	 * @return
	 */
	public boolean existeMeiocontatoPadrao() {
		return newQueryBuilderSined(Long.class)
		.select("count(*)")
		.from(Meiocontato.class)
		.where("meiocontato.padrao = ?", Boolean.TRUE)
		.openParentheses()
			.where("meiocontato.ativo = ?", Boolean.TRUE)
			.or()
			.where("meiocontato.ativo is null")
		.closeParentheses()
		.unique() > 0;
		
	}
	
	/**
	 * Verifica se o meiocontato passado � padr�o
	 * @param cdmeiocontato
	 * @return
	 */
	public boolean isMeiocontatoPadrao(Integer cdmeiocontato){
		if(cdmeiocontato == null){
			throw new SinedException("O par�metro cdmeiocontato n�o pode ser nulo.");
		}
		return newQueryBuilderSined(Long.class)
		.select("count(*)")
		.from(Meiocontato.class)
		.where("meiocontato.padrao = ?", Boolean.TRUE)
		.openParentheses()
			.where("meiocontato.ativo = ?", Boolean.TRUE)
			.or()
			.where("meiocontato.ativo is null")
		.closeParentheses()
		.where("meiocontato.cdmeiocontato = ?", cdmeiocontato)
		.unique() > 0;
	}
	
	/**
	 * Busca o meiocontato padr�o
	 * @return
	 */
	public Meiocontato findMeioContatoPadrao(){
		return query()
				.select("meiocontato.cdmeiocontato, meiocontato.nome, meiocontato.ativo, meiocontato.padrao")
				.where("meiocontato.padrao = ?", Boolean.TRUE)
				.openParentheses()
					.where("meiocontato.ativo = ?", Boolean.TRUE)
					.or()
					.where("meiocontato.ativo is null")
				.closeParentheses()
				.unique();
	}

	/**
	 * Busca apenas meiocontatos ativos
	 * @return
	 */
	public List<Meiocontato> findAtivos() {
		return query()
				.select("meiocontato.cdmeiocontato, meiocontato.nome")
				.where("meiocontato.ativo = ?", Boolean.TRUE)
				.list();
	}
	
	/**
	* M�todo que busca os meios de contato
	*
	* @param whereIn
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<Meiocontato> findForAndroid(String whereIn) {
		return query()
			.select("meiocontato.cdmeiocontato, meiocontato.nome")
			.whereIn("meiocontato.cdmeiocontato", whereIn)
			.list();
	}

	@Override
	public ListagemResult<Meiocontato> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Meiocontato> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("meiocontato.cdmeiocontato");
		
		return new ListagemResult<Meiocontato>(query, filtro);
	}
}
