package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Contacrmcontatofone;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContacrmcontatofoneDAO extends GenericDAO<Contacrmcontatofone>{

	public List<Contacrmcontatofone> findByContacrmcontato(Contacrmcontato contacrmcontato) {
		if(contacrmcontato == null || contacrmcontato.getCdcontacrmcontato() == null){
			throw new SinedException("Contato n�o pode ser nulo.");
		}
		return query()
					.select("contacrmcontatofone.telefone, contacrmcontatofone.telefonetipo")
					.where("contacrmcontatofone.contacrmcontato = ?", contacrmcontato)
					.list();
	}

}
