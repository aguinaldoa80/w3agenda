package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EnderecoDAO extends GenericDAO<Endereco> {

	@Override
	public void updateListagemQuery(QueryBuilder<Endereco> query, FiltroListagem _filtro) {
//		EtniaFiltro filtro = (EtniaFiltro) _filtro;
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Endereco> query) {
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
	}
	
	@Override
	public List<Endereco> findForCombo(String... extraFields) {
		return querySined()
			.select("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento," +
					"endereco.bairro, municipio.nome, uf.sigla, endereco.cep")
			.join("endereco.municipio municipio")
			.join("municipio.uf uf")
			.list();
	}
		

	/* singleton */
	private static EnderecoDAO instance;
	public static EnderecoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(EnderecoDAO.class);
		}
		return instance;
	}

	/**
	 * Retorna endere�o da empresa
	 * @return
	 * @author Tom�s T. Rabelo
	 */
	public Endereco carregaEnderecoEmpresa(Empresa empresa, boolean considerarSomenteAtivo) {
		if(empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo");
		}
		return querySined()
			.leftOuterJoin("endereco.pessoa pessoa")
			.leftOuterJoinFetch("endereco.municipio municipio")
			.leftOuterJoinFetch("municipio.uf")
			.leftOuterJoinFetch("endereco.pais pais")
			.where("pessoa = ?", empresa)
			.where("endereco.enderecotipo <> ?", Enderecotipo.INATIVO, considerarSomenteAtivo)
			.setMaxResults(1)
			.unique();
	}

	/**
	 * M�todo busca endere�o principal do fornecedor
	 * @param fornecedor
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Endereco getEnderecoPrincipalFornecedor(Fornecedor fornecedor) {
		if(fornecedor == null || fornecedor.getCdpessoa() == null){
			throw new SinedException("Empresa n�o pode ser nulo");
		}
		return querySined()
			.select("endereco.logradouro, endereco.bairro, endereco.cep, municipio.nome, uf.nome, endereco.numero, endereco.bairro, uf.sigla ")
			.join("endereco.pessoa pessoa")
			.join("endereco.enderecotipo enderecotipo")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.where("pessoa = ?", fornecedor)
			.orderBy("enderecotipo.cdenderecotipo")
			.setMaxResults(1)
			.unique();
	}

	/**
	 * Carrega todos os endere�os de uma pessoa.
	 * 
	 * @param pessoa
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Endereco> carregarListaEndereco(Pessoa pessoa) {
		if (pessoa == null || pessoa.getCdpessoa() == null) {
			throw new SinedException("O par�metro Pessoa n�o pode ser nulo.");
		}
		
		return querySined()
			.select("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro," +
					"municipio.nome, municipio.cdibge, uf.sigla, uf.nome, endereco.cep, enderecotipo.cdenderecotipo, endereco.pontoreferencia")
			.leftOuterJoin("endereco.enderecotipo enderecotipo")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("endereco.pessoa pessoa")
			.where("pessoa = ?", pessoa)
			.orderBy("endereco.cdendereco desc")
			.list();
	}

	/**
	 * Carrega a lista de endereco dos clientes para confec��o das etiquetas.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Endereco> findForEtiqueta(String whereIn) {
		QueryBuilder<Endereco> query = querySined()
					.select("endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro," +
							"municipio.nome, uf.sigla, endereco.cep, pessoa.nome, pessoa.cdpessoa, endereco.caixapostal, " +
							"endereco.pontoreferencia")
					.join("endereco.pessoa pessoa")
					.leftOuterJoin("pessoa.listaPessoacategoria listaPessoacategoria")
					.leftOuterJoin("listaPessoacategoria.categoria categoria")
					.leftOuterJoin("endereco.enderecotipo enderecotipo")
					.leftOuterJoin("endereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.whereIn("endereco.cdendereco", whereIn)
					.orderBy("pessoa.nome");
		
		return query.list();
	}
	
	public List<Endereco> findByCliente(Cliente cliente) {
		return querySined()
				.select("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, pessoa.cdpessoa, " +
							  "endereco.bairro, municipio.cdmunicipio, municipio.nome, municipio.cdibge, uf.sigla, endereco.cep, enderecotipo.cdenderecotipo")
					  .join("endereco.pessoa pessoa")
					  .leftOuterJoin("endereco.municipio municipio")
					  .leftOuterJoin("endereco.enderecotipo enderecotipo")
					  .leftOuterJoin("municipio.uf uf")
					  .where("pessoa.cdpessoa = ?", cliente.getCdpessoa())
					  .orderBy("endereco.logradouro, endereco.numero, enderecotipo.cdenderecotipo desc")
					  .list();
	}
	
	/**
	* M�todo que carrega a lista de endere�o de um fornecedor
	*
	* @param fornecedor
	* @return
	* @since 23/09/2014
	* @author Luiz Fernando
	*/
	public List<Endereco> findByFornecedor(Fornecedor fornecedor) {
		return querySined()
				.select("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento," +
							  "endereco.bairro, municipio.nome, municipio.cdibge, uf.sigla, endereco.cep, enderecotipo.cdenderecotipo")
					  .join("endereco.pessoa pessoa")
					  .leftOuterJoin("endereco.municipio municipio")
					  .leftOuterJoin("endereco.enderecotipo enderecotipo")
					  .leftOuterJoin("municipio.uf uf")
					  .where("pessoa.cdpessoa = ?", fornecedor.getCdpessoa())
					  .orderBy("endereco.logradouro, endereco.numero, enderecotipo.cdenderecotipo desc")
					  .list();
	}
	
	public List<Endereco> findAtivosByCliente(Cliente cliente) {
		return querySined()
				.select("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento," +
							  "endereco.bairro, municipio.nome, municipio.cdibge, uf.sigla, endereco.cep, enderecotipo.cdenderecotipo, " +
							  "endereco.descricao, endereco.substituirlogradouro")
					  .join("endereco.pessoa pessoa")
					  .leftOuterJoin("endereco.municipio municipio")
					  .leftOuterJoin("endereco.enderecotipo enderecotipo")
					  .leftOuterJoin("municipio.uf uf")
					  .where("pessoa.cdpessoa = ?", cliente.getCdpessoa())
					  .where("enderecotipo <> ? ", Enderecotipo.INATIVO)
					  .orderBy("endereco.logradouro, endereco.numero, enderecotipo.cdenderecotipo desc")
					  .list();
	}

	public Endereco loadEndereco(Endereco enderecoCliente) {
		if(enderecoCliente == null || enderecoCliente.getCdendereco() == null){
			throw new SinedException("Endere�o n�o pode ser nulo.");
		}
		return querySined()
					.select("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.inscricaoestadual, " +
							  "endereco.bairro,municipio.cdmunicipio, municipio.nome, municipio.cdibge, municipio.cdsiafi, municipio.codigogoiania, " +
							  "uf.cduf, uf.sigla, endereco.cep, enderecotipo.cdenderecotipo, enderecotipo.nome, " +
							  "endereco.pontoreferencia, pais.cdpais, pais.nome, pais.cdbacen")
				  .leftOuterJoin("endereco.pessoa pessoa")
				  .leftOuterJoin("endereco.pais pais")
				  .leftOuterJoin("endereco.municipio municipio")
				  .leftOuterJoin("endereco.enderecotipo enderecotipo")
				  .leftOuterJoin("municipio.uf uf")
				  .where("endereco = ?", enderecoCliente)
				  .unique();
	}
	
	public Endereco loadEnderecoNota(Endereco enderecoCliente) {
		if(enderecoCliente == null || enderecoCliente.getCdendereco() == null){
			throw new SinedException("Endere�o n�o pode ser nulo.");
		}
		return querySined()
					.select("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento," +
							  "endereco.bairro,municipio.cdmunicipio, municipio.nome, municipio.cdibge, municipio.cdsiafi, " +
							  "uf.cduf, uf.sigla, endereco.cep, enderecotipo.cdenderecotipo, enderecotipo.nome, " +
							  "endereco.pontoreferencia, pais.cdpais, pais.nome, pais.cdbacen")
				  .leftOuterJoin("endereco.nota nota")
				  .leftOuterJoin("endereco.pais pais")
				  .leftOuterJoin("endereco.municipio municipio")
				  .leftOuterJoin("endereco.enderecotipo enderecotipo")
				  .leftOuterJoin("municipio.uf uf")
				  .where("endereco = ?", enderecoCliente)
				  .unique();
	}
	
	/**
	 * Retorna lista de endere�os para confec��o de etiquetas de colaboradores.
	 * @param whereIn
	 * @return
	 * @author Taidson
	 * @since 04/08/2010
	 */
	public List<Endereco> findForEtiquetaColaborador(String whereIn) {
		QueryBuilder<Endereco> query = querySined()
					.select("endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro," +
							"municipio.nome, uf.sigla, endereco.cep, pessoa.nome, endereco.caixapostal")
					.join("endereco.pessoa pessoa")
					.leftOuterJoin("endereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.whereIn("endereco.cdendereco", whereIn)
					.orderBy("pessoa.nome");
		
		return query.list();
	}
	
	/**
	 * Seta o cdpessoa de colaborador em endere�o.
	 * @param endereco
	 * @param pessoa
	 * @author Taidson
	 * @since 05/08/2010
	 */
	public void setPessoaEndereco(Endereco endereco, Colaborador colaborador) {
		String sql = "UPDATE Endereco" +
					 " SET cdpessoa = " + colaborador.getCdpessoa()  +
					 " WHERE cdendereco = (" + endereco.getCdendereco() + ")";
		getJdbcTemplate().execute(sql);
	}
	/***
	 * Retorna o endere�o buscando pelo cep, n�mero e cdpessoa.
	 * @param cep
	 * @param numero
	 * @param cdpessoa
	 * @return {@link Endereco}
	 */
	public Endereco loadEndereco(String cep, String numero, Integer cdpessoa) {

		return querySined()
					.select("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento," +
							  "endereco.bairro,municipio.cdmunicipio, municipio.nome, municipio.cdibge, uf.sigla, " +
							  "endereco.cep, enderecotipo.cdenderecotipo, enderecotipo.nome, endereco.pontoreferencia")
				  .join("endereco.pessoa pessoa")
				  .leftOuterJoin("endereco.municipio municipio")
				  .leftOuterJoin("endereco.enderecotipo enderecotipo")
				  .leftOuterJoin("municipio.uf uf")
				  .where("endereco.cep = ?", new Cep(cep))
				  .where("endereco.numero = ?", numero)
				  .where("pessoa.cdpessoa = ?", cdpessoa)
				  .unique();
	}
	
	/**
	 * M�todo que busca o endre�o de acrodo com o logradouro, numero e cdpessoa
	 *
	 * @param logradouro
	 * @param numero
	 * @param cdpessoa
	 * @return
	 * @author Luiz Fernando
	 * @since 16/10/2013
	 */
	public Endereco loadEnderecoLogradouro(String logradouro, String numero, Integer cdpessoa) {

		return querySined()
					.select("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento," +
							  "endereco.bairro,municipio.cdmunicipio, municipio.nome, municipio.cdibge, uf.sigla, " +
							  "endereco.cep, enderecotipo.cdenderecotipo, enderecotipo.nome, endereco.pontoreferencia")
				  .join("endereco.pessoa pessoa")
				  .leftOuterJoin("endereco.municipio municipio")
				  .leftOuterJoin("endereco.enderecotipo enderecotipo")
				  .leftOuterJoin("municipio.uf uf")
				  .where("endereco.logradouro = ?", logradouro)
				  .where("endereco.numero = ?", numero)
				  .where("pessoa.cdpessoa = ?", cdpessoa)
				  .unique();
	}

	/**
	 * M�todo busca o endereco
	 *
	 * @param endereco
	 * @return
	 * @author Luiz Fernando
	 */
	public Endereco carregaEnderecoComUfPais(Endereco endereco) {
		if(endereco == null || endereco.getCdendereco() == null)
			throw new SinedException("Endereco n�o pode ser nulo.");
		
		return querySined()
				.select("endereco.cdendereco, municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla, pais.cdpais ")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("endereco.pais pais")
				.where("endereco.cdendereco = ?", endereco.getCdendereco())
				.unique();
	}

	public void updatePessoa(Endereco endereco, Pessoa pessoa) {
		getHibernateTemplate().bulkUpdate("update Endereco e set e.pessoa = ? where e.id = ?", new Object[]{
				pessoa, endereco.getCdendereco()
		});
	}
	
	public void updateNota(Endereco endereco, Nota nota) {
		getHibernateTemplate().bulkUpdate("update Endereco e set e.nota = ? where e.id = ?", new Object[]{
				nota, endereco.getCdendereco()
		});
	}
	
	/**
	 * 
	 * @param pessoa
	 * @author Thiago Clemente
	 * @param endereco 
	 * 
	 */
	public List<Endereco> findByPessoaWithoutInativo(Pessoa pessoa, Endereco endereco) {
		return querySined()
				.select("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, endereco.caixapostal," +
						"municipio.cdmunicipio, municipio.nome, municipio.cdibge, uf.cduf, uf.sigla, uf.nome, endereco.cep," +
						"endereco.pontoreferencia, endereco.distancia, enderecotipo.cdenderecotipo, enderecotipo.nome, pessoa.cdpessoa, " +
						"endereco.substituirlogradouro, endereco.descricao")
				.join("endereco.pessoa pessoa")
				.join("endereco.enderecotipo enderecotipo")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.where("pessoa=?", pessoa)
				.openParentheses()
					.where("enderecotipo not in (?)", Enderecotipo.INATIVO)
					.or()
					.where("endereco = ?", endereco)
				.closeParentheses()
				.list();
	}

	/**
	 * Busca os endere�os ativos do fornecedor 
	 * @param fornecedor
	 * @return
	 */
	public List<Endereco> findAtivosByForncedor(Fornecedor fornecedor) {
		return querySined()
		.select("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento," +
					  "endereco.bairro, municipio.nome, municipio.cdibge, uf.sigla, endereco.cep, enderecotipo.cdenderecotipo")
			  .join("endereco.pessoa pessoa")
			  .leftOuterJoin("endereco.municipio municipio")
			  .leftOuterJoin("endereco.enderecotipo enderecotipo")
			  .leftOuterJoin("municipio.uf uf")
			  .where("pessoa.cdpessoa = ?", fornecedor.getCdpessoa())
			  .where("enderecotipo <> ? ", Enderecotipo.INATIVO)
			  .orderBy("endereco.logradouro, endereco.numero, enderecotipo.cdenderecotipo desc")
			  .list();
	}

	public List<Endereco> findForAndroid(String whereIn) {
		QueryBuilder<Endereco> query = querySined()
			.select("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, uf.cduf, pessoa.cdpessoa, " +
					"municipio.cdmunicipio, municipio.nome, municipio.cdibge, uf.sigla, uf.nome, endereco.cep, enderecotipo.cdenderecotipo, endereco.pontoreferencia")
			.leftOuterJoin("endereco.enderecotipo enderecotipo")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.join("endereco.pessoa pessoa")
			.openParentheses()
				.where("exists (select 1 from Cliente c where c.cdpessoa = pessoa.cdpessoa)")
				.or()
				.where("exists (select 1 from Empresa e where e.cdpessoa = pessoa.cdpessoa)")
			.closeParentheses();
		
		if(StringUtils.isNotBlank(whereIn)){
			SinedUtil.quebraWhereIn("endereco.cdendereco", whereIn, query);
		}
		
		return query
			.orderBy("endereco.cdendereco desc")
			.list();
	}
	
	public Endereco getEnderecoPrincipalPessoa(Pessoa pessoa) {
		if(pessoa == null || pessoa.getCdpessoa() == null){
			throw new SinedException("Pessoa n�o pode ser nulo");
		}
		return querySined()
			.select("endereco.logradouro, endereco.bairro, endereco.cep, municipio.cdmunicipio, municipio.nome, uf.cduf, uf.nome, endereco.numero, endereco.bairro, uf.sigla ")
			.join("endereco.pessoa pessoa")
			.join("endereco.enderecotipo enderecotipo")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.where("pessoa = ?", pessoa)
			.orderBy("enderecotipo.cdenderecotipo")
			.setMaxResults(1)
			.unique();
	}

	public List<Endereco> findEnderecosDoClienteDoTipo(Cliente cliente, List<Enderecotipo> list) {
		String whereIn = "";
		for(int i = 0; i < list.size(); i++){
			whereIn += list.get(i).getCdenderecotipo();
			if(i + 1 < list.size())
				whereIn += ",";
		}
		return querySined()
				.select("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento," +
						"endereco.bairro, municipio.nome, municipio.cdibge, uf.sigla, endereco.cep, enderecotipo.cdenderecotipo, " +
						"endereco.descricao, endereco.substituirlogradouro")
				.join("endereco.pessoa pessoa")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("endereco.enderecotipo enderecotipo")
				.leftOuterJoin("municipio.uf uf")
				.where("pessoa.cdpessoa = ?", cliente.getCdpessoa())
				.whereIn("enderecotipo", whereIn)
				.orderBy("endereco.logradouro, endereco.numero, enderecotipo.cdenderecotipo desc")
				.list();
	}

	public List<Endereco> findByPessoaWhereNotIn(Integer cdpessoa, String whereNotIn) {
		QueryBuilder<Endereco> query = querySined()
				.select("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento," +
						"endereco.bairro, municipio.cdmunicipio, municipio.nome, municipio.cdibge, uf.sigla, endereco.cep, enderecotipo.cdenderecotipo, " +
						"endereco.descricao, endereco.substituirlogradouro")
				.join("endereco.pessoa pessoa")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("endereco.enderecotipo enderecotipo")
				.leftOuterJoin("municipio.uf uf")
				.where("endereco.pessoa.cdpessoa = ?", cdpessoa);
				
		if(StringUtils.isNotBlank(whereNotIn)){
			query.whereIn("endereco.cdendereco not", whereNotIn);
		}
				
		return query.list();
	}
	
	public Endereco getEnderecoFaturamento(Pessoa pessoa){
		return query()
				.select("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento")
				.join("endereco.enderecotipo enderecotipo")
				.where("enderecotipo = ?", Enderecotipo.FATURAMENTO)
				.where("endereco.pessoa = ?", pessoa)
				.setMaxResults(1)
				.unique();
	}
}