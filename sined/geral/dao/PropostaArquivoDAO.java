package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.geral.bean.PropostaArquivo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PropostaArquivoDAO extends GenericDAO<PropostaArquivo>{

	/**
	 * 
	 * M�todo para buscar a lista de Arquivos para a proposta
	 *
	 *@author Thiago Augusto
	 *@date 01/02/2012
	 * @param proposta
	 * @return
	 */
	public List<PropostaArquivo> findListByProposta(Proposta proposta){
		return query()
			.select("propostaArquivo.cdpropostaarquivo, propostaArquivo.descricao, " +
			"proposta.cdproposta, arquivo.cdarquivo, arquivo.tamanho, " + 
			"arquivo.tipoconteudo, arquivo.nome ")
			.join("propostaArquivo.proposta proposta")
			.join("propostaArquivo.arquivo arquivo")
			.where("proposta = ?", proposta)
			.list();
			
	}
}
