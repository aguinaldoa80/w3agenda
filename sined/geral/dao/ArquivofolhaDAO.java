package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Arquivofolha;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivofolhasituacao;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ArquivofolhaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ArquivofolhaDAO extends GenericDAO<Arquivofolha>{
	
	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Arquivofolha> query, FiltroListagem _filtro) {
		ArquivofolhaFiltro filtro = (ArquivofolhaFiltro) _filtro;
		
		query
			.select("arquivofolha.cdarquivofolha, arquivofolha.arquivofolhaorigem, arquivofolha.arquivofolhamotivo, " +
					"arquivofolha.arquivo, arquivofolha.dtinsercao, arquivofolha.arquivofolhasituacao")
			.where("arquivofolha.arquivofolhaorigem = ?", filtro.getArquivofolhaorigem())
			.where("arquivofolha.arquivofolhatipo = ?", filtro.getArquivofolhatipo())
			.where("arquivofolha.arquivofolhamotivo = ?", filtro.getArquivofolhamotivo())
			.where("arquivofolha.dtinsercao <= ?", filtro.getDtinsercao2())
			.where("arquivofolha.dtinsercao >= ?", filtro.getDtinsercao1())
			;
		
		if(filtro.getListaSituacao() != null){
			query.whereIn("arquivofolha.arquivofolhasituacao", Arquivofolhasituacao.listAndConcatenate(filtro.getListaSituacao()));
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Arquivofolha> query) {
		query
			.leftOuterJoinFetch("arquivofolha.arquivofolhamotivo arquivofolhamotivo")
			.leftOuterJoinFetch("arquivofolha.arquivo arquivo")
			.leftOuterJoinFetch("arquivofolha.listaArquivofolhahistorico arquivofolhahistorico")
			.orderBy("arquivofolhahistorico.dtaltera desc")
			;
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {		
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				Arquivofolha arquivofolha = (Arquivofolha)save.getEntity();
				
				if(arquivofolha.getArquivo() != null)
					arquivoDAO.saveFile(save.getEntity(), "arquivo");
				
				return null;
			}			
		});		
	}

	/**
	 * Busca a lista de Arquivofolha para o processo de autoriza��o.
	 *
	 * @param whereIn
	 * @return
	 * @since Jun 8, 2011
	 * @author Rodrigo Freitas
	 */
	public List<Arquivofolha> findForAutorizarEstornarCancelar(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
					.select("arquivofolha.cdarquivofolha, arquivofolha.arquivofolhasituacao")
					.whereIn("arquivofolha.cdarquivofolha", whereIn)
					.list();
	}

	/**
	 * Atualiza a situa��o do Arquivofolha de acordo com o par�metro passado.
	 *
	 * @param arquivofolha
	 * @param arquivofolhasituacao
	 * @since Jun 8, 2011
	 * @author Rodrigo Freitas
	 */
	public void updateSituacao(Arquivofolha arquivofolha, Arquivofolhasituacao arquivofolhasituacao) {
		if(arquivofolha == null || arquivofolha.getCdarquivofolha() == null || arquivofolhasituacao == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		getHibernateTemplate().bulkUpdate("update Arquivofolha a set a.arquivofolhasituacao = ? where a.id = ?", 
				new Object[]{arquivofolhasituacao, arquivofolha.getCdarquivofolha()});
	}

	/**
	 * Busca informa��es necess�rias para o processamento do arquivo de folha.
	 *
	 * @param whereIn
	 * @return
	 * @since Jun 16, 2011
	 * @author Rodrigo Freitas
	 */
	public List<Arquivofolha> findForProcessamento(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Par�metros inv�lidos.");
		}
		return query()
					.select("arquivofolha.cdarquivofolha, arquivofolha.arquivofolhasituacao, " +
							"arquivofolha.arquivofolhaorigem, arquivo.cdarquivo, arquivofolha.arquivofolhatipo")
					.join("arquivofolha.arquivo arquivo")
					.whereIn("arquivofolha.cdarquivofolha", whereIn)
					.list();
	}	
}
