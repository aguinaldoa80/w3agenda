package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Fornecedorhistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FornecedorhistoricoDAO extends GenericDAO<Fornecedorhistorico> {

	/**
	 * Busca o histórico a partir do fornecedor
	 *
	 * @param fornecedor
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/07/2014
	 */
	public List<Fornecedorhistorico> findByFornecedor(Fornecedor fornecedor) {
		return query()
					.select("fornecedorhistorico.cdfornecedorhistorico, fornecedorhistorico.dtaltera, " +
							"fornecedorhistorico.observacao, responsavel.nome")
					.leftOuterJoin("fornecedorhistorico.responsavel responsavel")
					.where("fornecedorhistorico.fornecedor = ?", fornecedor)
					.orderBy("fornecedorhistorico.dtaltera DESC")
					.list();
	}

}
