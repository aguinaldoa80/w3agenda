package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoLancamentoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.TipoVendedorEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.enumeration.NotafiscalservicomostrarFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiscalServicoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.Faixaimpostoapuracao;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.ApuracaoimpostoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SintegraFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedpiscofinsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaopiscofinsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ArquivoIssFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.SagefiscalFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotaFiscalServicoDAO extends GenericDAO<NotaFiscalServico> {

	private UsuarioService usuarioService;
	
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	@Override
	public void updateListagemQuery(QueryBuilder<NotaFiscalServico> query, FiltroListagem _filtro) {
		NotaFiscalServicoFiltro filtro = (NotaFiscalServicoFiltro) _filtro;
		/*
		if(filtro.getNumNota() != null && !filtro.getNumNota().equals("")){
			while(filtro.getNumNota().indexOf(",,") != -1){
				filtro.setNumNota(filtro.getNumNota().replaceAll(",,", ","));
			}
			
			if(filtro.getNumNota().substring(0, 1).equals(",")){
				filtro.setNumNota(filtro.getNumNota().substring(1, filtro.getNumNota().length()));
			}
			
			if(filtro.getNumNota().substring(filtro.getNumNota().length() - 1, filtro.getNumNota().length()).equals(",")){
				filtro.setNumNota(filtro.getNumNota().substring(0, filtro.getNumNota().length()-1));
			}
		}
		*/
		Integer idArquivonf = null;
		String cdarquivonf = NeoWeb.getRequestContext().getParameter("cdarquivonf");
		if(cdarquivonf != null && !cdarquivonf.equals("")){
			idArquivonf = Integer.parseInt(cdarquivonf);
		}
		
		query
			.select("distinct notaFiscalServico.cdNota, notaFiscalServico.numero, notaFiscalServico.dtEmissao, notaFiscalServico.numeronota, " +
					"projeto.nome, cliente.nome, notaStatus.cdNotaStatus, notaFiscalServico.diferencaduplicata, notaFiscalServico.cadastrarcobranca, naturezaoperacao.simplesremessa")
	
			.leftOuterJoin("notaFiscalServico.notaTipo notaTipo")
			.leftOuterJoin("notaFiscalServico.projeto projeto")
			.leftOuterJoin("notaFiscalServico.empresa empresa")
			.leftOuterJoin("notaFiscalServico.cliente cliente")
			.leftOuterJoin("notaFiscalServico.notaStatus notaStatus")
			.leftOuterJoin("notaFiscalServico.naturezaoperacao naturezaoperacao");
		
		if (filtro.getColaborador()!= null){
		    if (filtro.getTipoVendedor() != null && TipoVendedorEnum.PRINCIPAL.equals(filtro.getTipoVendedor())) {
				query
				    .leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
				    .openParentheses()
					.where("listaClientevendedor.colaborador = ?", filtro.getColaborador())
					.where("listaClientevendedor.principal = ?", Boolean.TRUE)
				.closeParentheses();
		    } else {
		    	query
			         .leftOuterJoin("notaFiscalServico.listaNotavenda listaNotavenda")
			         .leftOuterJoin("listaNotavenda.venda venda")
			         .where("venda.colaborador = ?", filtro.getColaborador());
		    }
		    query.ignoreJoin("listaClienteVendedor","colaborador", "fornecedor","principal","listaNotavenda","venda","expedicao");
		}
			
		
		boolean addJoinArquivonf = filtro.getNumeroNfseDe() != null || filtro.getNumeroNfseAte() != null || idArquivonf != null || filtro.getDtEmissaoInicioNFSe() != null || filtro.getDtEmissaoFimNFSe() != null ||
									filtro.getDtEmissaoInicioNFSe() != null || filtro.getDtEmissaoFimNFSe() != null;
		if(addJoinArquivonf){
			query
				.leftOuterJoin("notaFiscalServico.listaArquivonfnota arquivonfnota")
				.leftOuterJoin("arquivonfnota.arquivonf arquivonf");
		}
		
		if(filtro.getCentrocusto() != null && filtro.getCentrocusto().getCdcentrocusto() != null){
			query		
			.leftOuterJoin("notaFiscalServico.listaNotaContrato listaNotaContrato")			
			.leftOuterJoin("listaNotaContrato.contrato contrato")
			.leftOuterJoin("contrato.rateio rateio")
			.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
			.leftOuterJoin("listaRateioitem.centrocusto centrocusto");
		}
		
		query
			.where("empresa = ?", filtro.getEmpresa())
			//.whereIn("notaFiscalServico.numero", filtro.getNumNota() != null && !filtro.getNumNota().equals("") ? "'" + filtro.getNumNota().replaceAll(",", "','") + "'" : null)
			.where("cast(notaFiscalServico.numero AS long) >= ?", filtro.getNumNotaDe())
			.where("cast(notaFiscalServico.numero AS long) <= ?", filtro.getNumNotaAte())
			.where("notaFiscalServico.dtEmissao >= ?", filtro.getDtEmissaoInicio())
			.where("notaFiscalServico.dtEmissao <= ?", filtro.getDtEmissaoFim())
			.where("arquivonfnota.dtemissao >= ?", SinedDateUtils.dateToBeginOfDay(filtro.getDtEmissaoInicioNFSe()))
			.where("arquivonfnota.dtemissao <= ?", SinedDateUtils.dataToEndOfDay(filtro.getDtEmissaoFimNFSe()))
			.where("cliente = ?", filtro.getCliente())
			.where("projeto = ?", filtro.getProjeto())
			.where("arquivonf.cdarquivonf = ?", idArquivonf)
			.whereIn("notaStatus.cdNotaStatus", filtro.getListaNotaStatus() != null ? CollectionsUtil.listAndConcatenate(filtro.getListaNotaStatus(), "cdNotaStatus", ",") : null)
			.where("notaTipo = ?", NotaTipo.NOTA_FISCAL_SERVICO)
			
			.where("notaFiscalServico.cdNota in (select nf.cdNota from notaFiscalServico nf "
					+ "left join nf.listaNotaHistorico listaNotaHistorico "
					+ "where listaNotaHistorico.dtaltera >= ?"
					+ "and listaNotaHistorico.nota = notaFiscalServico "
					+ "and listaNotaHistorico.notaStatus = 13)", filtro.getDtCancelamentoInicio())
			.where("notaFiscalServico.cdNota in (select nf.cdNota from notaFiscalServico nf "
					+ "left join nf.listaNotaHistorico listaNotaHistorico "
					+ "where listaNotaHistorico.dtaltera <= ?"
					+ "and listaNotaHistorico.nota = notaFiscalServico "
					+ "and listaNotaHistorico.notaStatus = 13)", filtro.getDtCancelamentoFim())		
			
			.ignoreJoin("arquivonfnota", "arquivonf");
		
		NotafiscalservicomostrarFiltro notafiscalservicomostrarFiltro = filtro.getNotafiscalservicomostrarFiltro();
				
		if(notafiscalservicomostrarFiltro != null && 
				(notafiscalservicomostrarFiltro.equals(NotafiscalservicomostrarFiltro.CANCELADAS_ELETRONICO) || notafiscalservicomostrarFiltro.equals(NotafiscalservicomostrarFiltro.CANCELADAS_INTERNO)) && 
				filtro.getListaNotaStatus() != null &&
				filtro.getListaNotaStatus().contains(NotaStatus.CANCELADA)){
			query.where("notaFiscalServico.cdNota " 
					+ (notafiscalservicomostrarFiltro.equals(NotafiscalservicomostrarFiltro.CANCELADAS_INTERNO) ? " not " : "" ) 
					+ " in (select nf.cdNota from notaFiscalServico nf "
					+ "left join nf.listaNotaHistorico listaNotaHistorico "
					+ "where listaNotaHistorico.nota = notaFiscalServico "
					+ "and listaNotaHistorico.notaStatus.cdNotaStatus = " + NotaStatus.NFSE_EMITIDA.getCdNotaStatus() + ")");
		}
		
		if(notafiscalservicomostrarFiltro != null){
			if(notafiscalservicomostrarFiltro.equals(NotafiscalservicomostrarFiltro.RPS_GERADO)){
				query.where("exists (select 1 from NotaFiscalServicoRPS rps where rps.notaFiscalServico = notaFiscalServico)");
			} else if(notafiscalservicomostrarFiltro.equals(NotafiscalservicomostrarFiltro.RPS_NAOGERADO)){
				query.where("not exists (select 1 from NotaFiscalServicoRPS rps where rps.notaFiscalServico = notaFiscalServico)");
			} else if(notafiscalservicomostrarFiltro.equals(NotafiscalservicomostrarFiltro.LOTERPS_GERADO)){
				query.where("exists (select 1 from NotaFiscalServicoRPS rps where rps.notaFiscalServico = notaFiscalServico and rps.notaFiscalServicoLoteRPS is not null)");
			} else if(notafiscalservicomostrarFiltro.equals(NotafiscalservicomostrarFiltro.LOTERPS_NAOGERADO)){
				query.where("exists (select 1 from NotaFiscalServicoRPS rps where rps.notaFiscalServico = notaFiscalServico and rps.notaFiscalServicoLoteRPS is null)");
			}
		}
		Movimentacaoacao cancelada = Movimentacaoacao.CANCELADA;
		if (filtro.getDtPagamentoInicio() != null && filtro.getDtPagamentoFim() != null)
			query.where("notaFiscalServico.cdNota in (" +
					"select n.cdNota from Nota n " +
						"join n.listaNotaDocumento lnd " +
						"join lnd.documento d " +
						"join d.listaMovimentacaoOrigem lmo " +
						"join lmo.movimentacao m " +
							"where m.movimentacaoacao <> ?" +
							"and coalesce(m.dtbanco, m.dtmovimentacao) >= ? " +
							"and coalesce(m.dtbanco, m.dtmovimentacao) <= ?)",
							new Object[]{cancelada,filtro.getDtPagamentoInicio(), filtro.getDtPagamentoFim()});
		else if (filtro.getDtPagamentoInicio() != null)
			query.where("notaFiscalServico.cdNota in (" +
					"select n.cdNota from Nota n " +
					"join n.listaNotaDocumento lnd " +
					"join lnd.documento d " +
					"join d.listaMovimentacaoOrigem lmo " +
					"join lmo.movimentacao m " +
						"where m.movimentacaoacao <> ? " +
						"and coalesce(m.dtbanco, m.dtmovimentacao) >= ?)", new Object[]{cancelada,filtro.getDtPagamentoInicio()});
		else if (filtro.getDtPagamentoFim() != null)
			query.where("notaFiscalServico.cdNota in (" +
					"select n.cdNota from Nota n " +
					"join n.listaNotaDocumento lnd " +
					"join lnd.documento d " +
					"join d.listaMovimentacaoOrigem lmo " +
					"join lmo.movimentacao m " +
						"where m.movimentacaoacao <> ? " +
						"and coalesce(m.dtbanco, m.dtmovimentacao) <= ?)", new Object[]{cancelada,filtro.getDtPagamentoFim()});
		
		if(filtro.getCentrocusto() != null && filtro.getCentrocusto().getCdcentrocusto() != null){
			query.where("centrocusto = ?", filtro.getCentrocusto());
		}
		if(filtro.getOrderBy() == null || filtro.getOrderBy().equals("")){
			query.orderBy("notaFiscalServico.dtEmissao desc, notaFiscalServico.numero desc");
		}
		
		if(filtro.getMostraComNumero() != null){
			if(filtro.getMostraComNumero()){
				query
					.where("notaFiscalServico.numero is not null")
					.where("notaFiscalServico.numero <> ?", "");
			} else {
				query
					.openParentheses()
					.where("notaFiscalServico.numero is null")
					.or()
					.where("notaFiscalServico.numero = ?", "")
					.closeParentheses();
			}
		}	
		
		if(filtro.getIncideiss() != null && filtro.getIncideiss()){
			query.where("notaFiscalServico.incideiss = ?", filtro.getIncideiss());
		}
		if(filtro.getIncideir() != null && filtro.getIncideir()){
			query.where("notaFiscalServico.incideir = ?", filtro.getIncideir());
		}
		if(filtro.getIncideinss() != null && filtro.getIncideinss()){
			query.where("notaFiscalServico.incideinss = ?", filtro.getIncideinss());
		}
		if(filtro.getIncidepis() != null && filtro.getIncidepis()){
			query.where("notaFiscalServico.incidepis = ?", filtro.getIncidepis());
		}
		if(filtro.getIncidecofins() != null && filtro.getIncidecofins()){
			query.where("notaFiscalServico.incidecofins = ?", filtro.getIncidecofins());
		}
		if(filtro.getIncidecsll() != null && filtro.getIncidecsll()){
			query.where("notaFiscalServico.incidecsll = ?", filtro.getIncidecsll());
		}
		if(filtro.getIncideicms() != null && filtro.getIncideicms()){
			query.where("notaFiscalServico.incideicms = ?", filtro.getIncideicms());
		}
		if(filtro.getDtEmissaoInicioNFSe() != null && filtro.getDtEmissaoFimNFSe() != null){
			query.where("arquivonfnota.dtemissao >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getDtEmissaoInicioNFSe()));
			query.where("arquivonfnota.dtemissao <= ?", SinedDateUtils.dateToTimestampFinalDia(filtro.getDtEmissaoFimNFSe()));
		} else if(filtro.getDtEmissaoInicioNFSe() != null){
			query.where("arquivonfnota.dtemissao >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getDtEmissaoInicioNFSe()));
		} else if(filtro.getDtEmissaoFimNFSe() != null){
			query.where("arquivonfnota.dtemissao <= ?", SinedDateUtils.dateToTimestampFinalDia(filtro.getDtEmissaoFimNFSe()));
		} 
		
		if(filtro.getNumeroNfseDe() != null || filtro.getNumeroNfseAte() != null){
			String numeroNfseDeStr = filtro.getNumeroNfseDe() != null ? filtro.getNumeroNfseDe().toString() : null;
			String numeroNfseAteStr = filtro.getNumeroNfseAte() != null ? filtro.getNumeroNfseAte().toString() : null;
			
			String prefixNumeroNfseDe = null;
			String prefixNumeroNfseAte = null;
			
			if(numeroNfseDeStr != null && numeroNfseDeStr.length() == 15){
				prefixNumeroNfseDe = numeroNfseDeStr.substring(0, 4);
				Integer numeroNfseDe = Integer.parseInt(numeroNfseDeStr.substring(4, 15));
				
				query.where("arquivonfnota.numero_filtro >= ?", numeroNfseDe);
			} else {
				query.where("arquivonfnota.numero_filtro >= ?", filtro.getNumeroNfseDe() != null ? filtro.getNumeroNfseDe().intValue() : null);
			}
			
			if(numeroNfseAteStr != null && numeroNfseAteStr.length() == 15){
				prefixNumeroNfseAte = numeroNfseAteStr.substring(0, 4);
				Integer numeroNfseAte = Integer.parseInt(numeroNfseAteStr.substring(4, 15));
				
				query.where("arquivonfnota.numero_filtro <= ?", numeroNfseAte);
			} else {
				query.where("arquivonfnota.numero_filtro <= ?", filtro.getNumeroNfseAte() != null ? filtro.getNumeroNfseAte().intValue() : null);
			}
			
			if(prefixNumeroNfseDe != null || prefixNumeroNfseAte != null){
				query.openParentheses();
				query.where("arquivonfnota.numeronfse like '" + prefixNumeroNfseDe + "%'", prefixNumeroNfseDe != null);
				query.or();
				query.where("arquivonfnota.numeronfse like '" + prefixNumeroNfseAte + "%'", prefixNumeroNfseAte != null);
				query.closeParentheses();
			}
		}
		
		addWhereRestricaoClienteVendedor(query);
	}
	
	/**
	* M�todo  que adiciona o where de restri��o cliente/vendedor
	*
	* @param query
	* @since 22/10/2015
	* @author Luiz Fernando
	*/
	public void addWhereRestricaoClienteVendedor(QueryBuilder<NotaFiscalServico> query){
		boolean restricaoClienteVendedor = usuarioService.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado());
		Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
		if(restricaoClienteVendedor && colaborador != null){
			query.leftOuterJoinIfNotExists("notaFiscalServico.cliente cliente");
			query.where("cliente.cdpessoa in (select c.cdpessoa " +
											" from Cliente c " +
											" left outer join c.listaClientevendedor clientevendedor " +
											" left outer join clientevendedor.colaborador colaborador " +
											" where colaborador.cdpessoa = " + colaborador.getCdpessoa() + " " +
											" or colaborador is null ) ");
		}
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		Nota bean = (Nota)save.getEntity();
		bean.setNotaTipo(NotaTipo.NOTA_FISCAL_SERVICO);
		
		save.saveOrUpdateManaged("listaItens");
		save.saveOrUpdateManaged("listaDuplicata");
		
		//Somente salvar a lista de hist�rico se houver itens nela.
		if (bean.getListaNotaHistorico() != null && !bean.getListaNotaHistorico().isEmpty()) {
			save.saveOrUpdateManaged("listaNotaHistorico");
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<NotaFiscalServico> query) {
		query
			.select("notaFiscalServico.cdNota, notaFiscalServico.retornocanhoto, notaStatus.cdNotaStatus, notaStatus.nome, notaFiscalServico.numero, notaFiscalServico.notaimportadaxml, " +
					"notaFiscalServico.dtEmissao, notaFiscalServico.hremissao, notaFiscalServico.fatura, notaFiscalServico.duplicata, notaFiscalServico.infocomplementar, " +
					"notaFiscalServico.dtVencimento, projeto.cdprojeto, notaFiscalServico.dadosAdicionais, notaFiscalServico.ir, notaFiscalServico.diferencaduplicata, " +
					"notaFiscalServico.numerofatura, notaFiscalServico.valororiginalfatura, notaFiscalServico.valordescontofatura, notaFiscalServico.valorliquidofatura, " +
					"notaFiscalServico.incentivoFiscalIss, " +
					"prazopagamentofatura.cdprazopagamento, contaboleto.cdconta, contacarteira.cdcontacarteira, " +
					"cliente.cdpessoa, cliente.nome, cliente.razaosocial, cliente.cpf, cliente.cnpj, cliente.inscricaoestadual, cliente.tipopessoa, cliente.incidiriss, " +
					"notaFiscalServico.pracaPagamento, empresa.nome, empresa.razaosocial, empresa.nomefantasia, notaFiscalServico.telefoneCliente, notaFiscalServico.nomefantasiaCliente, " +
					"itemServico.cdNotaFiscalServicoItem, itemServico.qtde, itemServico.desconto, itemServico.descricao," +
					"itemServico.precoUnitario, notaFiscalServico.iss, notaFiscalServico.inss, notaFiscalServico.csll, notaFiscalServico.cofins, " +
					"notaFiscalServico.pis, notaFiscalServico.icms, notaFiscalServico.impostocumulativoinss, " +
					"notaFiscalServico.impostocumulativoir, notaFiscalServico.impostocumulativoiss, " +
					"notaFiscalServico.impostocumulativoicms, notaFiscalServico.impostocumulativopis," +
					"notaFiscalServico.impostocumulativocofins, notaFiscalServico.impostocumulativocsll, " +
					"notaFiscalServico.incideiss, notaFiscalServico.incideinss, notaFiscalServico.incidecsll, notaFiscalServico.incidecofins, " +
					"notaFiscalServico.incidepis, notaFiscalServico.incideicms, notaFiscalServico.incideir, " +
					"documentotipo.cddocumentotipo, documentotipo.nome, " +
					"empresa.cdpessoa, listaNotaContrato.cdNotaContrato, contrato.cdcontrato, contrato.inscricaoestadual, " +
					"grupotributacaoContrato.cdgrupotributacao, grupotributacaoContrato.nome, notaFiscalServico.pisretido, notaFiscalServico.cofinsretido, " +
					"notaFiscalServico.basecalculo, notaFiscalServico.descontocondicionado, notaFiscalServico.descontoincondicionado, " +
					"notaFiscalServico.deducao, notaFiscalServico.outrasretencoes, municipioissqn.cdmunicipio, municipioissqn.nome, uf.cduf, uf.sigla," +
					"enderecoCliente.cdendereco, regimetributacao.cdregimetributacao, naturezaoperacao.cdnaturezaoperacao, regimetributacao.codigonfse, naturezaoperacao.codigonfse, " +
					"tipovencimento.cdtipovencimento, tipovencimento.nome, notaFiscalServico.inscricaomunicipal," +
					"notaFiscalServico.basecalculoir, notaFiscalServico.basecalculoiss, notaFiscalServico.basecalculopis, notaFiscalServico.basecalculocofins, " +
					"notaFiscalServico.basecalculocsll, notaFiscalServico.basecalculoinss, notaFiscalServico.basecalculoicms," +
					"notaFiscalServico.cstpis, notaFiscalServico.cstcofins, notaFiscalServico.indicadortipopagamento, " +
					"itemlistaservicoBean.cditemlistaservico, itemlistaservicoBean.codigo, itemlistaservicoBean.descricao, " +
					"codigocnaeBean.cdcodigocnae, codigocnaeBean.cnae, codigocnaeBean.descricaocnae, " +
					"codigotributacao.cdcodigotributacao, codigotributacao.codigo, codigotributacao.descricao, " +
					"grupotributacao.cdgrupotributacao, grupotributacao.nome, notaFiscalServico.situacaoissparacatu, notaFiscalServico.situacaoissbrumadinho, " +
					"enderecoCliente.inscricaoestadual, " +
					"vendamaterial.cdvendamaterial, vendamaterial.ordem, venda.cdvenda, venda.dtvenda, notaFiscalServico.cadastrarcobranca, cliente.crt, " +
					"material.nome, material.cdmaterial, material.nomenf ")
			.join("notaFiscalServico.notaStatus notaStatus")
			.join("notaFiscalServico.cliente cliente")
			.leftOuterJoin("notaFiscalServico.documentotipo documentotipo")
			.leftOuterJoin("notaFiscalServico.codigocnaeBean codigocnaeBean")
			.leftOuterJoin("notaFiscalServico.codigotributacao codigotributacao")
			.leftOuterJoin("notaFiscalServico.itemlistaservicoBean itemlistaservicoBean")
			.leftOuterJoin("notaFiscalServico.enderecoCliente enderecoCliente")
			.leftOuterJoin("notaFiscalServico.listaItens itemServico")
			.leftOuterJoin("itemServico.vendamaterial vendamaterial")
			.leftOuterJoin("itemServico.material material")
			.leftOuterJoin("vendamaterial.venda venda")
			.leftOuterJoin("notaFiscalServico.projeto projeto")
			.leftOuterJoin("notaFiscalServico.empresa empresa")
			.leftOuterJoin("notaFiscalServico.listaNotaContrato listaNotaContrato")
			.leftOuterJoin("notaFiscalServico.regimetributacao regimetributacao")
			.leftOuterJoin("notaFiscalServico.naturezaoperacao naturezaoperacao")
			.leftOuterJoin("notaFiscalServico.municipioissqn municipioissqn")
			.leftOuterJoin("municipioissqn.uf uf")
			.leftOuterJoin("listaNotaContrato.contrato contrato")
			.leftOuterJoin("contrato.grupotributacao grupotributacaoContrato")
			.leftOuterJoin("notaFiscalServico.tipovencimento tipovencimento")
			.leftOuterJoin("notaFiscalServico.grupotributacao grupotributacao")
			.leftOuterJoin("notaFiscalServico.prazopagamentofatura prazopagamentofatura")
			.leftOuterJoin("notaFiscalServico.contaboleto contaboleto")
			.leftOuterJoin("notaFiscalServico.contacarteira contacarteira")
			.orderBy("itemServico.descricao");
	}
	
	/**
	 * Carrega as informa��es necess�rias para
	 * calcular o total de cada nota fiscal de Produto.
	 * 
	 * @param listaNfServico
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Nota> carregarInfoParaCalcularTotal(List<Nota> listaNfServico) {
		if (listaNfServico == null || listaNfServico.isEmpty()) {
			return new ArrayList<Nota>();
		}
		
		List<NotaFiscalServico> listaNfs = this.carregarInfoParaCalcularTotalServico(CollectionsUtil.listAndConcatenate(listaNfServico, "cdNota", ","), null, null);
		if(SinedUtil.isListNotEmpty(listaNfs)){
			for(NotaFiscalServico nfs : listaNfs){
				nfs.setValorNotaTransient(nfs.getValorNota());
			}
		}
		
		
		return new ArrayList<Nota>(listaNfs);
	}
	
	public List<NotaFiscalServico> carregarInfoCalcularTotal(List<NotaFiscalServico> listaNfServico, String orderBy, Boolean asc) {
		if (listaNfServico == null || listaNfServico.isEmpty()) {
			return new ArrayList<NotaFiscalServico>();
		}
		return new ArrayList<NotaFiscalServico>(this.carregarInfoParaCalcularTotalServico(CollectionsUtil.listAndConcatenate(listaNfServico, "cdNota", ","), orderBy, asc));
	}

	public List<NotaFiscalServico> carregarInfoParaCalcularTotalServico(String whereIn, String orderBy, Boolean asc) {
		QueryBuilder<NotaFiscalServico> query = query();
		
		query
			.select("notaFiscalServico.cdNota, notaFiscalServico.notaTipo, servicoItem.qtde, servicoItem.desconto, servicoItem.precoUnitario, notaFiscalServico.ir, " +
					"notaFiscalServico.icms, notaFiscalServico.pis, notaFiscalServico.cofins, notaFiscalServico.csll, notaFiscalServico.inss, " +
					"notaFiscalServico.iss, notaFiscalServico.dtEmissao, notaFiscalServico.pisretido,  notaFiscalServico.cofinsretido, " +
					"notaFiscalServico.incideiss, notaFiscalServico.incideinss, notaFiscalServico.incidecsll, notaFiscalServico.incidecofins, " +
					"notaFiscalServico.incidepis, notaFiscalServico.incideicms, notaFiscalServico.incideir, notaFiscalServico.numero, notaFiscalServico.dtEmissao, " +
					"notaStatus.cdNotaStatus, notaStatus.nome, cliente.nome, projeto.nome, cliente.cpf, cliente.cnpj," +
					"notaFiscalServico.basecalculo, notaFiscalServico.descontocondicionado, notaFiscalServico.descontoincondicionado, " +
					"notaFiscalServico.deducao, notaFiscalServico.outrasretencoes, " +
					"notadocumento.cdNotaDocumento, notadocumento.ordem, documento.cddocumento, documento.numero, documento.dtvencimento, documentoacao.cddocumentoacao, " +
					"notaFiscalServico.basecalculoir, notaFiscalServico.basecalculoiss, notaFiscalServico.basecalculopis, " + 
					"notaFiscalServico.basecalculocofins, notaFiscalServico.basecalculocsll, notaFiscalServico.basecalculoinss, notaFiscalServico.basecalculoicms, " +
					"contrato.cdcontrato, notaFiscalServico.inscricaomunicipal, cliente.razaosocial, cliente.tipopessoa, prazopagamentofatura.cdprazopagamento, notaFiscalServico.diferencaduplicata, notaFiscalServico.cadastrarcobranca, " +
					"naturezaoperacao.simplesremessa, contaboleto.cdconta, documentotipo.cddocumentotipo")
					
			.leftOuterJoin("notaFiscalServico.listaItens servicoItem")
			.leftOuterJoin("notaFiscalServico.projeto projeto")
			.leftOuterJoin("notaFiscalServico.cliente cliente")
			.leftOuterJoin("notaFiscalServico.notaStatus notaStatus")
			.leftOuterJoin("notaFiscalServico.listaNotaContrato notacontrato")
			.leftOuterJoin("notaFiscalServico.naturezaoperacao naturezaoperacao")
			.leftOuterJoin("notacontrato.contrato contrato")
			.leftOuterJoin("notaFiscalServico.prazopagamentofatura prazopagamentofatura")
			.leftOuterJoin("notaFiscalServico.listaNotaDocumento notadocumento")
			.leftOuterJoin("notadocumento.documento documento")
			.leftOuterJoin("documento.documentoacao documentoacao")
			.leftOuterJoin("notaFiscalServico.contaboleto contaboleto")
			.leftOuterJoin("notaFiscalServico.documentotipo documentotipo")
			
			;
			
		SinedUtil.quebraWhereIn("notaFiscalServico.cdNota", whereIn, query);
	
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("notaFiscalServico.dtEmissao desc, notaFiscalServico.numero desc");
		}
		
		return query.list();
	}

	/**
	 * M�todo para obter Notas fiscais de servico para impress�o de Notas.
	 * 
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<NotaFiscalServico> findForReport(NotaFiltro filtro){
		String whereIn = filtro.getWhereIn();
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Par�metro whereIn inv�lido.");
		}
		
		return 
			query()
				.select("notaFiscalServico.cdNota, notaFiscalServico.dtEmissao, notaFiscalServico.numero, notaFiscalServico.fatura, " +
						"notaFiscalServico.duplicata, notaFiscalServico.dtVencimento, notaFiscalServico.pracaPagamento, " +
						"notaFiscalServico.iss, notaFiscalServico.ir, notaFiscalServico.inss," +
						"itens.cdNotaFiscalServicoItem, itens.qtde, itens.desconto, itens.descricao, itens.precoUnitario, " +
						"cliente.nome, cliente.razaosocial, cliente.inscricaoestadual, cliente.inscricaomunicipal, cliente.cpf, cliente.cnpj, " +
						"enderecoCliente.logradouro, enderecoCliente.numero, enderecoCliente.complemento, enderecoCliente.bairro," +
						"enderecoCliente.cep, municipio.nome, uf.sigla, notaFiscalServico.csll, notaFiscalServico.cofins, " +
						"notaFiscalServico.pis, notaFiscalServico.icms, " +
						"notaFiscalServico.incideiss, notaFiscalServico.incideinss, notaFiscalServico.incidecsll, notaFiscalServico.incidecofins, " +
						"notaFiscalServico.incidepis, notaFiscalServico.incideicms, notaFiscalServico.incideir," +
						"notaFiscalServico.basecalculo, notaFiscalServico.basecalculoir, notaFiscalServico.basecalculoiss, " +
						"notaFiscalServico.basecalculopis, notaFiscalServico.basecalculocofins, notaFiscalServico.basecalculocsll, " +
						"notaFiscalServico.basecalculoinss, notaFiscalServico.basecalculoicms, " +
						"notaFiscalServico.descontocondicionado, notaFiscalServico.descontoincondicionado, " +
						"notaFiscalServico.deducao, notaFiscalServico.outrasretencoes, contrato.cdcontrato, " +
						"tipovencimento.cdtipovencimento, tipovencimento.nome, notaFiscalServico.dadosAdicionais, " +
						"notaFiscalServico.inscricaomunicipal, documento.cddocumento, " +
						"regimetributacao.cdregimetributacao, naturezaoperacao.cdnaturezaoperacao, regimetributacao.codigonfse, naturezaoperacao.codigonfse, " +
						"material.cdmaterial, material.nome ")
				.leftOuterJoin("notaFiscalServico.listaNotaDocumento listaNotaDocumento")
				.leftOuterJoin("listaNotaDocumento.documento documento")
				.leftOuterJoin("notaFiscalServico.listaNotaContrato listaNotaContrato")
				.leftOuterJoin("listaNotaContrato.contrato contrato")
				.leftOuterJoin("notaFiscalServico.listaItens itens")
				.leftOuterJoin("itens.material material")
				.leftOuterJoin("notaFiscalServico.cliente cliente")
				.leftOuterJoin("notaFiscalServico.naturezaoperacao naturezaoperacao")
				.leftOuterJoin("notaFiscalServico.regimetributacao regimetributacao")
				.leftOuterJoin("notaFiscalServico.enderecoCliente enderecoCliente")
				.leftOuterJoin("enderecoCliente.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("notaFiscalServico.tipovencimento tipovencimento")								
				
				.where("notaFiscalServico.notaTipo = ?", NotaTipo.NOTA_FISCAL_SERVICO)
				.whereIn("notaFiscalServico.cdNota",whereIn)
				.orderBy("itens.cdNotaFiscalServicoItem")
				.list();
	}
	
	/**
	 * Busca uma lista de notas de servi�o para sele��o ao faturar medi��es.
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<NotaFiscalServico> findForSelect(String projetos) {
		return 
			query()
			.select("notaFiscalServico.cdNota, notaFiscalServico.dtEmissao, notaFiscalServico.numero")
			.join("notaFiscalServico.projeto projeto")
			.join("notaFiscalServico.notaTipo notaTipo")
			.join("notaFiscalServico.notaStatus notaStatus")
			.where("notaTipo = ?", NotaTipo.NOTA_FISCAL_SERVICO)
			.where("notaStatus = ?", NotaStatus.EMITIDA)
			.whereIn("projeto.cdprojeto", projetos)
			.orderBy("notaFiscalServico.dtEmissao desc")
			.list();
	}

	/**
	 * Carrega todas as informa��es das notas e seus contratos 
	 * para a gera��o de receita autom�tica.
	 * 
	 * @param cdNota
	 * @return
	 * @author Rodrigo Freitas
	 */
	public NotaFiscalServico loadForReceita(Integer cdNota) {
		if(cdNota == null){
			throw new SinedException("C�digo da nota n�o pode ser nulo.");
		}
		return query()
					.select("notaFiscalServico.cdNota, cliente.cdpessoa, cliente.nome, notaFiscalServico.dtEmissao, notaFiscalServico.valorliquidofatura, " +
							"notaFiscalServico.dtVencimento, notaFiscalServico.numero, notaFiscalServico.inss, " +
							"notaFiscalServico.pis, notaFiscalServico.icms, notaFiscalServico.csll, notaFiscalServico.cofins, " +
							"notaFiscalServico.iss, notaFiscalServico.ir, item.precoUnitario, item.qtde, item.desconto, " +
							"documentotipo.cddocumentotipo, documentotipo.nome, documentotipoDup.cddocumentotipo, documentotipoDup.nome, " + 
							"empresa.cdpessoa, conta.cdconta, conta.nossonumerointervalo, item.descricao, rateio.cdrateio, taxa.cdtaxa, " +
							"notaFiscalServico.incideiss, notaFiscalServico.incideinss, notaFiscalServico.incidecsll, " +
							"notaFiscalServico.incidecofins, notaFiscalServico.incidepis, notaFiscalServico.incideicms, " +
							"notaFiscalServico.incideir, notaStatus.cdNotaStatus, enderecoCliente.cdendereco," +
							"notaFiscalServico.basecalculo, notaFiscalServico.basecalculoir, notaFiscalServico.basecalculoiss, " +
							"notaFiscalServico.basecalculopis, notaFiscalServico.basecalculocofins, notaFiscalServico.basecalculoinss, " +
							"notaFiscalServico.basecalculoicms, notaFiscalServico.basecalculocsll, " +
							"notaFiscalServico.descontocondicionado, notaFiscalServico.descontoincondicionado, " +
							"notaFiscalServico.deducao, notaFiscalServico.outrasretencoes, comissionamento.cdcomissionamento, " +
							"comissionamento.nome, comissionamento.quantidade, comissao.cdcomissionamento, comissao.nome, comissao.quantidade, " +
							"listaContratocolaborador.cdcontratocolaborador, colaborador.cdpessoa, vendedor.cdpessoa, " +
							"contrato.cdcontrato, banco.cdbanco, banco.numero," +
							"listaEndereco.cdendereco, enderecotipo.cdenderecotipo," +
							"prazopagamentofatura.cdprazopagamento, prazopagamentofatura.dataparceladiautil, " +
							"listaDuplicata.numero, listaDuplicata.dtvencimento, listaDuplicata.valor, " +
							"contaboleto.cdconta, contacarteira.cdcontacarteira, contacarteira.msgboleto2, contrato.reajuste, contrato.percentualreajuste, " +
							"naturezaoperacao.simplesremessa, documentotipocontrato.cddocumentotipo, documentotipocontrato.nome, " +
							"material.cdmaterial, material.nome, nota.cdNota ")
					.leftOuterJoin("notaFiscalServico.listaItens item")
					.leftOuterJoin("item.material material")
					.leftOuterJoin("notaFiscalServico.listaNotaContrato listaNotaContrato")
					.leftOuterJoin("listaNotaContrato.nota nota")
					.leftOuterJoin("listaNotaContrato.contrato contrato")
					.leftOuterJoin("notaFiscalServico.cliente cliente")
					.leftOuterJoin("cliente.listaEndereco listaEndereco")
					.leftOuterJoin("listaEndereco.enderecotipo enderecotipo")
					.leftOuterJoin("notaFiscalServico.documentotipo documentotipo")
					.leftOuterJoin("notaFiscalServico.empresa empresa")
					.leftOuterJoin("notaFiscalServico.enderecoCliente enderecoCliente")
					.leftOuterJoin("notaFiscalServico.notaStatus notaStatus")
					.leftOuterJoin("contrato.conta conta")
					.leftOuterJoin("conta.banco banco")
					.leftOuterJoin("contrato.rateio rateio")
					.leftOuterJoin("contrato.taxa taxa")
					.leftOuterJoin("contrato.documentotipo documentotipocontrato")
					.leftOuterJoin("contrato.vendedor vendedor")
					.leftOuterJoin("contrato.comissionamento comissionamento")
					.leftOuterJoin("contrato.listaContratocolaborador listaContratocolaborador")
					.leftOuterJoin("listaContratocolaborador.colaborador colaborador")
					.leftOuterJoin("listaContratocolaborador.comissionamento comissao")
					.leftOuterJoin("notaFiscalServico.prazopagamentofatura prazopagamentofatura")
					.leftOuterJoin("notaFiscalServico.listaDuplicata listaDuplicata")
					.leftOuterJoin("listaDuplicata.documentotipo documentotipoDup")
					.leftOuterJoin("notaFiscalServico.contaboleto contaboleto")
					.leftOuterJoin("notaFiscalServico.contacarteira contacarteira")
					.leftOuterJoin("notaFiscalServico.naturezaoperacao naturezaoperacao")
					.where("notaFiscalServico.cdNota = ?", cdNota)
					.orderBy("notaFiscalServico.cdNota, listaDuplicata.dtvencimento")
					.unique();
	}
	
	/**
	 * M�todo que busca as notas fiscais de serivi�o para gerar receita
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<NotaFiscalServico> loadForReceita(String whereIn) {
		if(whereIn == null || "".equals(whereIn)){
			throw new SinedException("C�digo da nota n�o pode ser nulo.");
		}
		return query()
					.select("notaFiscalServico.cdNota, cliente.cdpessoa, cliente.nome, notaFiscalServico.dtEmissao, " +
							"notaFiscalServico.dtVencimento, notaFiscalServico.numero, notaFiscalServico.inss, " +
							"notaFiscalServico.pis, notaFiscalServico.icms, notaFiscalServico.csll, notaFiscalServico.cofins, " +
							"notaFiscalServico.iss, notaFiscalServico.ir, item.precoUnitario, item.qtde, item.desconto," +
							"empresa.cdpessoa, conta.cdconta, conta.nossonumerointervalo, item.descricao, rateio.cdrateio, taxa.cdtaxa, " +
							"notaFiscalServico.incideiss, notaFiscalServico.incideinss, notaFiscalServico.incidecsll, " +
							"notaFiscalServico.incidecofins, notaFiscalServico.incidepis, notaFiscalServico.incideicms, " +
							"notaFiscalServico.incideir, notaStatus.cdNotaStatus, enderecoCliente.cdendereco," +
							"notaFiscalServico.basecalculo, notaFiscalServico.descontocondicionado, notaFiscalServico.descontoincondicionado, " +
							"notaFiscalServico.deducao, notaFiscalServico.outrasretencoes, comissionamento.cdcomissionamento, " +
							"comissionamento.nome, comissionamento.quantidade, comissao.cdcomissionamento, comissao.nome, comissao.quantidade, " +
							"listaContratocolaborador.cdcontratocolaborador, colaborador.cdpessoa, vendedor.cdpessoa, " +
							"contrato.cdcontrato, contrato.dtproximovencimento, banco.cdbanco, banco.numero," +
							"notaFiscalServico.basecalculoir, notaFiscalServico.basecalculoiss, notaFiscalServico.basecalculopis, notaFiscalServico.basecalculocofins, " +
							"notaFiscalServico.basecalculocsll, notaFiscalServico.basecalculoinss, notaFiscalServico.basecalculoicms," +
							"documentotipoDup.cddocumentotipo, documentotipoDup.nome, documentotipoDup.boleto, " +
							"prazopagamentofatura.cdprazopagamento,  documentotipo.cddocumentotipo, " +
							"listaDuplicata.numero, listaDuplicata.dtvencimento, listaDuplicata.valor, " +
							"contaboleto.cdconta, contacarteira.cdcontacarteira, contacarteira.msgboleto2," +
							"venda.cdvenda, contrato.reajuste, contrato.percentualreajuste, " +
							"material.cdmaterial, material.nome ")
					.leftOuterJoin("notaFiscalServico.listaItens item")
					.leftOuterJoin("item.material material ")
					.leftOuterJoin("notaFiscalServico.listaNotaContrato listaNotaContrato")
					.leftOuterJoin("listaNotaContrato.contrato contrato")
					.leftOuterJoin("notaFiscalServico.cliente cliente")
					.leftOuterJoin("notaFiscalServico.empresa empresa")
					.leftOuterJoin("notaFiscalServico.enderecoCliente enderecoCliente")
					.leftOuterJoin("notaFiscalServico.notaStatus notaStatus")
					.leftOuterJoin("contrato.conta conta")
					.leftOuterJoin("conta.banco banco")
					.leftOuterJoin("contrato.rateio rateio")
					.leftOuterJoin("contrato.taxa taxa")
					.leftOuterJoin("contrato.vendedor vendedor")
					.leftOuterJoin("contrato.comissionamento comissionamento")
					.leftOuterJoin("contrato.endereco enderecoFaturamento")
					.leftOuterJoin("contrato.enderecoentrega enderecoentrega")
					.leftOuterJoin("contrato.enderecocobranca enderecocobranca")
					.leftOuterJoin("contrato.listaContratocolaborador listaContratocolaborador")
					.leftOuterJoin("listaContratocolaborador.colaborador colaborador")
					.leftOuterJoin("listaContratocolaborador.comissionamento comissao")
					
					.leftOuterJoin("notaFiscalServico.prazopagamentofatura prazopagamentofatura")
					.leftOuterJoin("notaFiscalServico.listaDuplicata listaDuplicata")
					.leftOuterJoin("listaDuplicata.documentotipo documentotipoDup")
					.leftOuterJoin("notaFiscalServico.contaboleto contaboleto")
					.leftOuterJoin("notaFiscalServico.contacarteira contacarteira")
					.leftOuterJoin("notaFiscalServico.documentotipo documentotipo")
					
					.leftOuterJoin("notaFiscalServico.listaNotavenda listaNotavenda")
					.leftOuterJoin("listaNotavenda.venda venda")

					.whereIn("notaFiscalServico.cdNota", whereIn)
					.list();
	}

	/**
	 * Carrega a lista de notas fiscais para a gera��o da GPS.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<NotaFiscalServico> findForGPS(String whereIn) {
		return query()
					.select("notaFiscalServico.cdNota, cliente.nome, cliente.cnpj, notaFiscalServico.dtEmissao, " +
							"notaFiscalServico.numero, notaFiscalServico.inss, notaFiscalServico.basecalculoinss, item.precoUnitario, item.qtde, item.desconto, " +
							"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, contrato.codigogps")
					.leftOuterJoin("notaFiscalServico.listaItens item")
					.leftOuterJoin("notaFiscalServico.listaNotaContrato listaNotaContrato")
					.leftOuterJoin("listaNotaContrato.contrato contrato")
					.leftOuterJoin("notaFiscalServico.empresa empresa")
					.leftOuterJoin("notaFiscalServico.cliente cliente")
					.whereIn("notaFiscalServico.cdNota", whereIn)
					.list();
	}

	public List<NotaFiscalServico> findForArquivoIss(ArquivoIssFiltro filtro) {
		return query()
					.select("notaFiscalServico.cdNota, notaFiscalServico.numero, notaFiscalServico.dtEmissao, " +
							"item.precoUnitario, item.qtde, item.desconto, notaFiscalServico.incideiss, notaFiscalServico.iss, " +
							"notaFiscalServico.inscricaomunicipal, cliente.inscricaomunicipal, cliente.cnpj, cliente.cpf, cliente.razaosocial, cliente.nome, " +
							"enderecoCliente.logradouro, enderecoCliente.numero, enderecoCliente.complemento, enderecoCliente.bairro, " +
							"municipio.nome, uf.sigla, enderecoCliente.cep")
					.leftOuterJoin("notaFiscalServico.listaItens item")
					.leftOuterJoin("notaFiscalServico.empresa empresa")
					.leftOuterJoin("notaFiscalServico.cliente cliente")
					.leftOuterJoin("notaFiscalServico.enderecoCliente enderecoCliente")
					.leftOuterJoin("enderecoCliente.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.where("empresa = ?", filtro.getEmpresa())
					.where("notaFiscalServico.dtEmissao >= ?", filtro.getData1())
					.where("notaFiscalServico.dtEmissao <= ?", filtro.getData2())
					//BUSCAR SOMENTE AS NOTAS QUE TEM N�MERO CADASTRADO
					.where("notaFiscalServico.numero is not null")
					.where("notaFiscalServico.numero <> ?", "")
					.list();
	}

	public List<NotaFiscalServico> findForNFe(String whereIn) {
		return query()
					.select("notaFiscalServico.cdNota, notaFiscalServico.dtEmissao, notaFiscalServico.numero, listaItens.qtde, listaItens.precoUnitario, listaItens.desconto, " +
							"listaItens.descricao, notaFiscalServico.iss, notaFiscalServico.incideiss, notaFiscalServico.pis, notaFiscalServico.incidepis, " +
							"notaFiscalServico.cofins, notaFiscalServico.incidecofins, notaFiscalServico.inss, notaFiscalServico.incideinss, notaFiscalServico.incentivoFiscalIss, " +
							"notaFiscalServico.ir, notaFiscalServico.incideir, notaFiscalServico.csll, notaFiscalServico.incidecsll, notaFiscalServico.infocomplementar, " +
							"notaFiscalServico.iss, notaFiscalServico.incideiss, cliente.razaosocial, cliente.tipopessoa, cliente.cpf, cliente.cnpj, " +
							"notaFiscalServico.inscricaomunicipal, cliente.inscricaomunicipal, cliente.inscricaoestadual, cliente.cdpessoa, cliente.nome, cliente.email, " +
							"enderecoCliente.cdendereco, enderecoCliente.logradouro, enderecoCliente.complemento, enderecoCliente.numero, enderecoCliente.bairro, " +
							"enderecoCliente.cep, municipio.nome, municipio.cdibge, municipio.codigogoiania, uf.sigla, " +
							"empresa.cdpessoa, empresa.cnpj, notaFiscalServico.basecalculo, notaFiscalServico.descontocondicionado, " +
							"notaFiscalServico.descontoincondicionado, notaFiscalServico.deducao, notaFiscalServico.outrasretencoes, " +
							"regimetributacao.cdregimetributacao, regimetributacao.codigonfse, regimetributacao.descricao, " +
							"naturezaoperacao.codigonfse, naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.descricao, codigotributacao.cdcodigotributacao, " +
							"municipioissqn.cdmunicipio, municipioissqn.cdibge, municipioissqn.nome, municipioissqn.codigogoiania, ufissqn.sigla, grupotributacao.cdgrupotributacao, " +
							"codigocnaeBean.cnae, codigocnaeBean.descricaocnae, codigotributacao.codigo, codigotributacao.descricao, itemlistaservicoBean.codigo, " +
							"itemlistaservicoBean.descricao, notaFiscalServico.basecalculoir, notaFiscalServico.basecalculoiss, notaFiscalServico.basecalculopis, " +
							"notaFiscalServico.basecalculocofins, notaFiscalServico.basecalculocsll, notaFiscalServico.basecalculoinss, notaFiscalServico.basecalculoicms, " +
							"listaTelefone.telefone, notaFiscalServico.telefoneCliente, notaFiscalServico.nomefantasiaCliente, municipioissqn.cdsiafi, " +
							"notaFiscalServico.situacaoissparacatu, notaFiscalServico.situacaoissbrumadinho, projeto.cdprojeto, projeto.cei, projeto.art," +
							"notaFiscalServico.dadosAdicionais, notaFiscalServico.pisretido, notaFiscalServico.cofinsretido, notaStatus.cdNotaStatus, notaFiscalServico.dtVencimento, " +
							"notaFiscalServico.incideicms, notaFiscalServico.icms, " +
							"listaDuplicata.numero, listaDuplicata.dtvencimento, listaDuplicata.valor," +
							"vendamaterial.cdvendamaterial, vendamaterial.ordem, venda.cdvenda, venda.dtvenda, notaFiscalServico.hremissao, " +
							"material.cdmaterial, material.nome ") 
					.join("notaFiscalServico.listaItens listaItens")
					.join("notaFiscalServico.cliente cliente")
					.join("notaFiscalServico.notaStatus notaStatus")
					.leftOuterJoin("notaFiscalServico.projeto projeto")
					.leftOuterJoin("notaFiscalServico.enderecoCliente enderecoCliente")
					.leftOuterJoin("enderecoCliente.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("notaFiscalServico.grupotributacao grupotributacao")
					.leftOuterJoin("notaFiscalServico.empresa empresa")
					.leftOuterJoin("notaFiscalServico.codigocnaeBean codigocnaeBean")
					.leftOuterJoin("notaFiscalServico.codigotributacao codigotributacao")
					.leftOuterJoin("notaFiscalServico.itemlistaservicoBean itemlistaservicoBean")
					.leftOuterJoin("notaFiscalServico.municipioissqn municipioissqn")
					.leftOuterJoin("municipioissqn.uf ufissqn")
					.leftOuterJoin("notaFiscalServico.regimetributacao regimetributacao")
					.leftOuterJoin("notaFiscalServico.naturezaoperacao naturezaoperacao")
					.leftOuterJoin("cliente.listaTelefone listaTelefone")
					.leftOuterJoin("notaFiscalServico.listaDuplicata listaDuplicata")
					.leftOuterJoin("listaItens.vendamaterial vendamaterial")
					.leftOuterJoin("listaItens.material material")
					.leftOuterJoin("vendamaterial.venda venda")
					.whereIn("notaFiscalServico.cdNota", whereIn)
					.orderBy("notaFiscalServico.numero, listaDuplicata.dtvencimento")
					.list();
	}

	/**
	 * Busca a nota a aprtir do n�mero da NFS-e.
	 *
	 * @param num
	 * @param cnpj
	 * @return
	 * @since 12/03/2012
	 * @author Rodrigo Freitas
	 */
	public NotaFiscalServico findByNumNfseCNPJProcessoNovo(String num, Cnpj cnpj) {
		return query()
					.select("notaFiscalServico.cdNota, notaFiscalServico.numero")
					.join("notaFiscalServico.empresa empresa")
					.join("notaFiscalServico.listaArquivonfnota arquivonfnota")
					.where("arquivonfnota.numeronfse = ?", num)
					.where("empresa.cnpj = ?", cnpj)
					.orderBy("notaFiscalServico.cdNota desc")
					.unique();
	}
	
	/**
	 * Buscar nota a partir do n�mero e cnpj
	 *
	 * @param num
	 * @param cnpj
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/05/2015
	 */
	public List<NotaFiscalServico> findByNumCNPJ(String num, Cnpj cnpj) {
		return query()
					.select("notaFiscalServico.cdNota, notaFiscalServico.numero")
					.join("notaFiscalServico.empresa empresa")
					.where("notaFiscalServico.notaTipo = ?", NotaTipo.NOTA_FISCAL_SERVICO)
					.openParentheses()
						.where("notaFiscalServico.notaStatus = ?", NotaStatus.NFSE_EMITIDA)
						.or()
						.where("notaFiscalServico.notaStatus = ?", NotaStatus.NFSE_LIQUIDADA)
					.closeParentheses()
					.where("notaFiscalServico.numero = ?", num)
					.where("empresa.cnpj = ?", cnpj)
					.orderBy("notaFiscalServico.cdNota desc")
					.list();
	}

	public List<NotaFiscalServico> findEmitidasSubstituicao(Empresa empresa) {
		return query()
					.select("notaFiscalServico.cdNota, notaFiscalServico.numero")
					.join("notaFiscalServico.empresa empresa")
					.join("notaFiscalServico.notaStatus notaStatus")
					.join("notaFiscalServico.notaTipo notaTipo")
					.where("empresa = ?", empresa)
					.where("notaTipo = ?", NotaTipo.NOTA_FISCAL_SERVICO)
					.where("notaStatus = ?", NotaStatus.EMITIDA)
					.list();
	}

	
	public String getNextNumero() {
		return query()
					.select("notaFiscalServico.cdNota, notaFiscalServico.numero")
					.where("notaFiscalServico.numero is not null")
					.where("notaFiscalServico.numero <> ''")
					.setMaxResults(1)
					.orderBy("notaFiscalServico.cdNota desc")
					.unique().getNumero();
	}
//	/**
//	 * Verifica se o cd passado por par�metro � de uma nota fiscal de servi�o.
//	 *
//	 * @param cdNota
//	 * @return
//	 * @author Rodrigo Freitas
//	 */
//	public boolean isNotaFiscalServico(int cdNota) {
//		return newQueryBuilderSined(Long.class)
//					.select("count(*)")
//					.setUseTranslator(false)
//					.from(NotaFiscalServico.class, "nf")
//					.where("nf.cdNota = ?", cdNota)
//					.unique() > 0;
//	}
	
	/**
	 * Carrega notas fiscais de servi��es que est�o na situa��o Emitida ou NFS-e Emitida
	 * para realizar o processo de associar uma conta a receber a uma nota fiscal de servi�o.
	 * @author Taidson
	 * @since 15/12/2010
	 */
	public List<NotaFiscalServico> loadNotasServico() {
		return 
			query()
			.select("notaFiscalServico.cdNota, notaFiscalServico.dtEmissao, " +
					"notaFiscalServico.numero, notaStatus.nome")
			.join("notaFiscalServico.notaTipo notaTipo")
			.join("notaFiscalServico.notaStatus notaStatus")
			.where("notaTipo = ?", NotaTipo.NOTA_FISCAL_SERVICO)
			.openParentheses()
			.where("notaStatus = ?", NotaStatus.EMITIDA)
			.or()
			.where("notaStatus = ?", NotaStatus.NFSE_EMITIDA)
			.closeParentheses()
			.orderBy("notaFiscalServico.dtEmissao desc")
			.list();
	}

	/**
	 * M�todo que busca as notas pelo wherein
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<NotaFiscalServico> findByWhereIn(String whereIn) {
		return query()
			.select("notaFiscalServico.cdNota, notaFiscalServico.dtEmissao, notaFiscalServico.dtVencimento, notaFiscalServico.numero, notaFiscalServico.basecalculo, notaFiscalServico.inss, notaFiscalServico.ir, notaFiscalServico.iss, " +
					"notaFiscalServico.icms, notaFiscalServico.cofins, notaFiscalServico.pis, notaFiscalServico.csll, notaFiscalServico.outrasretencoes, " +
					"notaFiscalServico.descontoincondicionado, notaFiscalServico.descontocondicionado, listaItens.cdNotaFiscalServicoItem, listaItens.precoUnitario, listaItens.qtde, listaItens.desconto," +
					"listaNotaDocumento.cdNotaDocumento, documento.cddocumento, documentoacao.cddocumentoacao, notaStatus.cdNotaStatus, notaFiscalServico.cadastrarcobranca, prazopagamentofatura.cdprazopagamento, notaFiscalServico.valorliquidofatura")
			.join("notaFiscalServico.listaItens listaItens")
			.leftOuterJoin("notaFiscalServico.notaStatus notaStatus")
			.leftOuterJoin("notaFiscalServico.prazopagamentofatura prazopagamentofatura")
			.leftOuterJoin("notaFiscalServico.listaNotaDocumento listaNotaDocumento")
			.leftOuterJoin("listaNotaDocumento.documento documento")
			.leftOuterJoin("documento.documentoacao documentoacao")
			.whereIn("notaFiscalServico.id", whereIn)
			.list();
	}

	/**
	 * Criado c�digo novo pelo fato do whereIn ficar muito grande e travar a query
	 * O c�digo anterior foi mantido com o mesmo nome do m�todo sem o "NEW"
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<NotaFiscalServico> carregarInfoParaCalcularTotalNew(NotaFiltro filtro) {
		QueryBuilder<NotaFiscalServico> query = query();
		
		query
			.select("notaFiscalServico.cdNota, notaFiscalServico.notaTipo, servicoItem.qtde,  servicoItem.precoUnitario, " +
					"notaFiscalServico.basecalculoir, notaFiscalServico.basecalculoiss, notaFiscalServico.basecalculopis," +
					"notaFiscalServico.basecalculocofins, notaFiscalServico.basecalculocsll, notaFiscalServico.basecalculoinss," +
					"notaFiscalServico.basecalculoicms,notaFiscalServico.ir, " +
					"notaFiscalServico.icms, notaFiscalServico.pis, notaFiscalServico.cofins, notaFiscalServico.csll, notaFiscalServico.inss, " +
					"notaFiscalServico.iss, notaFiscalServico.dtEmissao, " +
					"notaFiscalServico.incideiss, notaFiscalServico.incideinss, notaFiscalServico.incidecsll, notaFiscalServico.incidecofins, " +
					"notaFiscalServico.incidepis, notaFiscalServico.incideicms, notaFiscalServico.incideir, notaFiscalServico.numero, notaFiscalServico.dtEmissao, " +
					"notaStatus.cdNotaStatus, notaStatus.nome, cliente.nome, projeto.nome, cliente.cpf, cliente.cnpj," +
					"notaFiscalServico.basecalculo, notaFiscalServico.descontocondicionado, notaFiscalServico.descontoincondicionado, " +
					"notaFiscalServico.deducao, notaFiscalServico.outrasretencoes, " +
					"notadocumento.cdNotaDocumento, notadocumento.ordem, documento.cddocumento, documento.numero, documentoacao.cddocumentoacao, servicoItem.desconto")
			.leftOuterJoin("notaFiscalServico.listaItens servicoItem")
			.leftOuterJoin("notaFiscalServico.projeto projeto")
			.leftOuterJoin("notaFiscalServico.empresa empresa")
			.leftOuterJoin("notaFiscalServico.cliente cliente")
			.leftOuterJoin("notaFiscalServico.notaStatus notaStatus")
			.leftOuterJoin("notaFiscalServico.notaTipo notaTipo")
			.leftOuterJoin("notaFiscalServico.listaNotaDocumento notadocumento")
			.leftOuterJoin("notadocumento.documento documento")
			.leftOuterJoin("documento.documentoacao documentoacao");
		
		if(filtro.getNumeroNfseDe() != null || filtro.getNumeroNfseAte() != null){
			query.leftOuterJoin("notaFiscalServico.listaArquivonfnota arquivonfnota")
			.where("arquivonfnota.numero_filtro >= ?", filtro.getNumeroNfseDe())
			.where("arquivonfnota.numero_filtro <= ?", filtro.getNumeroNfseAte());
		}
		
		if(filtro.getCentrocusto() != null && filtro.getCentrocusto().getCdcentrocusto() != null){
			query		
			.leftOuterJoin("notaFiscalServico.listaNotaContrato listaNotaContrato")			
			.leftOuterJoin("listaNotaContrato.contrato contrato")
			.leftOuterJoin("contrato.rateio rateio")
			.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
			.leftOuterJoin("listaRateioitem.centrocusto centrocusto");
		}
		
		query
		.where("empresa = ?", filtro.getEmpresa())
		.whereIn("notaFiscalServico.numero", filtro.getNumNota() != null && !filtro.getNumNota().equals("") ? "'" + filtro.getNumNota().replaceAll(",", "','") + "'" : null)
		.where("notaFiscalServico.dtEmissao >= ?", filtro.getDtEmissaoInicio())
		.where("notaFiscalServico.dtEmissao <= ?", filtro.getDtEmissaoFim())
		.where("cliente = ?", filtro.getCliente())
		.where("projeto = ?", filtro.getProjeto())
		.whereIn("notaStatus.cdNotaStatus", filtro.getListaNotaStatus() != null ? CollectionsUtil.listAndConcatenate(filtro.getListaNotaStatus(), "cdNotaStatus", ",") : null)
		.whereIn("notaTipo.cdNotaTipo", filtro.getListaNotaTipo() != null ? CollectionsUtil.listAndConcatenate(filtro.getListaNotaTipo(), "cdNotaTipo", ",") : null);
	
		if (filtro.getDtPagamentoInicio() != null && filtro.getDtPagamentoFim() != null)
			query.where("notaFiscalServico.cdNota in (select n.cdNota from Nota n join n.listaNotaDocumento lnd join lnd.documento d join d.listaMovimentacaoOrigem lmo join lmo.movimentacao m where m.dtmovimentacao >= ? and m.dtmovimentacao <= ?)", new Object[]{filtro.getDtPagamentoInicio(), filtro.getDtPagamentoFim()});
		else if (filtro.getDtPagamentoInicio() != null)
			query.where("notaFiscalServico.cdNota in (select n.cdNota from Nota n join n.listaNotaDocumento lnd join lnd.documento d join d.listaMovimentacaoOrigem lmo join lmo.movimentacao m where m.dtmovimentacao >= ?)", filtro.getDtPagamentoInicio());
		else if (filtro.getDtPagamentoFim() != null)
			query.where("notaFiscalServico.cdNota in (select n.cdNota from Nota n join n.listaNotaDocumento lnd join lnd.documento d join d.listaMovimentacaoOrigem lmo join lmo.movimentacao m where m.dtmovimentacao <= ?)", filtro.getDtPagamentoFim());
		
		if(filtro.getCentrocusto()!=null)
			query.where("centrocusto = ?", filtro.getCentrocusto());
		
		if(filtro.getNumNotaDe() != null)
			query.where("cast(notaFiscalServico.numero AS long) >= ?", filtro.getNumNotaDe());
		
		if(filtro.getNumNotaAte() != null)
			query.where("cast(notaFiscalServico.numero AS long) <= ?", filtro.getNumNotaAte());
		
		if(filtro.getMostraComNumero()!= null){
			if(filtro.getMostraComNumero()){
				query
					.where("notaFiscalServico.numero not null")
					.where("notaFiscalServico.numero <> ?", "");
			}
			else {
				query
					.openParentheses()
					.where("notaFiscalServico.numero is null")
					.or()
					.where("notaFiscalServico.numero = ?", "")
					.closeParentheses();
			}
		}
		
		if(filtro.getDtCancelamentoInicio() != null || filtro.getDtCancelamentoFim() != null){
			StringBuilder whereCancelamento = new StringBuilder(
												"notaFiscalServico.cdNota in (select nf.cdNota from notaFiscalServico nf "
												+ "left join nf.listaNotaHistorico listaNotaHistorico "
												+ "where listaNotaHistorico.nota = notaFiscalServico "
												+ "and listaNotaHistorico.notaStatus = 13 "); 
			if(filtro.getDtCancelamentoInicio() != null){
				whereCancelamento.append(" and listaNotaHistorico.dtaltera >=  '" + SinedDateUtils.dateToTimestampInicioDia(filtro.getDtCancelamentoInicio()) + "' ");
			}
			if(filtro.getDtCancelamentoFim() != null){
				whereCancelamento.append(" and listaNotaHistorico.dtaltera <= '" + SinedDateUtils.dateToTimestampInicioDia(filtro.getDtCancelamentoFim()) + "' ");
			}
			whereCancelamento.append(" )");
			query.where(whereCancelamento.toString());
		}
		
		NotafiscalservicomostrarFiltro notafiscalservicomostrarFiltro = filtro.getNotafiscalservicomostrarFiltro();
		if(notafiscalservicomostrarFiltro != null && 
				(notafiscalservicomostrarFiltro.equals(NotafiscalservicomostrarFiltro.CANCELADAS_ELETRONICO) || notafiscalservicomostrarFiltro.equals(NotafiscalservicomostrarFiltro.CANCELADAS_INTERNO)) && 
				filtro.getListaNotaStatus() != null &&
				filtro.getListaNotaStatus().contains(NotaStatus.CANCELADA)){
			query.where("notaFiscalServico.cdNota " 
					+ (notafiscalservicomostrarFiltro.equals(NotafiscalservicomostrarFiltro.CANCELADAS_INTERNO) ? " not " : "" ) 
					+ " in (select nf.cdNota from notaFiscalServico nf "
					+ "left join nf.listaNotaHistorico listaNotaHistorico "
					+ "where listaNotaHistorico.nota = notaFiscalServico "
					+ "and listaNotaHistorico.notaStatus.cdNotaStatus = " + NotaStatus.NFSE_EMITIDA.getCdNotaStatus() + ")");
		}
		
		if(notafiscalservicomostrarFiltro != null){
			if(notafiscalservicomostrarFiltro.equals(NotafiscalservicomostrarFiltro.RPS_GERADO)){
				query.where("exists (select 1 from NotaFiscalServicoRPS rps where rps.notaFiscalServico = notaFiscalServico)");
			} else if(notafiscalservicomostrarFiltro.equals(NotafiscalservicomostrarFiltro.RPS_NAOGERADO)){
				query.where("not exists (select 1 from NotaFiscalServicoRPS rps where rps.notaFiscalServico = notaFiscalServico)");
			} else if(notafiscalservicomostrarFiltro.equals(NotafiscalservicomostrarFiltro.LOTERPS_GERADO)){
				query.where("exists (select 1 from NotaFiscalServicoRPS rps where rps.notaFiscalServico = notaFiscalServico and rps.notaFiscalServicoLoteRPS is not null)");
			} else if(notafiscalservicomostrarFiltro.equals(NotafiscalservicomostrarFiltro.LOTERPS_NAOGERADO)){
				query.where("exists (select 1 from NotaFiscalServicoRPS rps where rps.notaFiscalServico = notaFiscalServico and rps.notaFiscalServicoLoteRPS is null)");
			}
		}
		
		query.orderBy("notaFiscalServico.dtEmissao desc, notaFiscalServico.numero desc");
		
		addWhereRestricaoClienteVendedor(query);
		return query.list();
	}
	
	/**
	 * M�todo para obter lista de notas para o relat�rio de resumo de faturamento.
	 * 
	 * @param filtro
	 * @return
	 * @author Leandro Lima
	 * @author Hugo Ferreira
	 */
	public List<NotaFiscalServico> buscarParaRelatorioResumoFaturamento(NotaFiscalServicoFiltro filtro){
		if(filtro == null){
			throw new SinedException("O par�metro filtro n�o pode ser null.");
		}
		
		//Por algum motivo o isAsc tem que ser invertido. 
		//O Neo deve interpret�-lo antes de sua mudan�a de valor.
		String orderBy = filtro.getOrderBy() + " " + (!filtro.isAsc() ? "ASC" : "DESC");
		
		QueryBuilder<NotaFiscalServico> query = newQueryBuilderWithFrom(NotaFiscalServico.class);
		this.updateListagemQuery(query, filtro);
		
		if (!StringUtils.isEmpty(filtro.getOrderBy())) {
			query.orderBy(orderBy);
		}
		
		return query.list();
	}

	/**
	 * M�todo que busca os dados na Nota Fiscal de Servi�o para Integra��o com Dom�nio
	 *
	 * @param empresa
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Luiz Fernando
	 */
	public List<NotaFiscalServico> findForDominiointegracaoSaida(Empresa empresa, Date dtinicio, Date dtfim) {
		return querySined()
				
				.select("notaFiscalServico.cdNota, notaFiscalServico.numero, notaFiscalServico.dtEmissao, notaFiscalServico.inscricaomunicipal, " +
						"notaFiscalServico.basecalculoir, notaFiscalServico.basecalculoiss, notaFiscalServico.basecalculopis," +
						"notaFiscalServico.basecalculocofins, notaFiscalServico.basecalculocsll, notaFiscalServico.basecalculoinss," +
						"notaFiscalServico.basecalculoicms, notaFiscalServico.ir, notaFiscalServico.iss, " +
						"notaFiscalServico.pis, notaFiscalServico.cofins, notaFiscalServico.csll, notaFiscalServico.inss, " +
						"notaFiscalServico.icms, notaFiscalServico.incideir, notaFiscalServico.incideiss, notaFiscalServico.incidepis, " +
						"notaFiscalServico.incidecofins, notaFiscalServico.incidecsll, notaFiscalServico.incideinss, " +
						"notaFiscalServico.incideicms, notaFiscalServico.indicadortipopagamento, " +
						"cliente.cpf, cliente.cnpj, cliente.inscricaomunicipal, cliente.inscricaoestadual, uf.sigla, " +						
						"endereco.cdendereco, municipio.cdmunicipio, municipio.cdibge, uf.cduf, uf.sigla, uf.cdibge," +
						"listaItens.qtde, listaItens.descricao, listaItens.precoUnitario, listaItens.desconto," +
						"arquivonf.cdarquivonf, arquivonfnota.numeronfse, arquivonfnota.cdarquivonfnota," +
						"naturezaoperacao.cdnaturezaoperacao, " +
						"operacaocontabilavista.cdoperacaocontabil, operacaocontabilavista.codigointegracao," +
						"operacaocontabilaprazo.cdoperacaocontabil, operacaocontabilaprazo.codigointegracao," +
						"enderecoNF.cdendereco, municipioNF.cdmunicipio, municipioNF.cdibge, ufNF.cduf, ufNF.sigla, ufNF.cdibge ")
				.leftOuterJoin("notaFiscalServico.empresa empresa")
				.leftOuterJoin("notaFiscalServico.cliente cliente")
				.leftOuterJoin("cliente.listaEndereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("notaFiscalServico.enderecoCliente enderecoNF")
				.leftOuterJoin("enderecoNF.municipio municipioNF")
				.leftOuterJoin("municipioNF.uf ufNF")
				.leftOuterJoin("notaFiscalServico.listaItens listaItens")
				.leftOuterJoin("notaFiscalServico.listaArquivonfnota arquivonfnota")
				.leftOuterJoin("arquivonfnota.arquivonf arquivonf")
				.leftOuterJoin("arquivonf.configuracaonfe configuracaonfe")
				.leftOuterJoin("notaFiscalServico.naturezaoperacao naturezaoperacao")
				.leftOuterJoin("naturezaoperacao.operacaocontabilavista operacaocontabilavista")
				.leftOuterJoin("naturezaoperacao.operacaocontabilaprazo operacaocontabilaprazo")
				.where("empresa = ?", empresa)
				.where("notaFiscalServico.dtEmissao >= ?", dtinicio)
				.where("notaFiscalServico.dtEmissao <= ?", dtfim)
				.list();
	}

	/**
	 * M�todo que busca as informa��es para o RegistroA010 do SPED PIS/COFINS
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<NotaFiscalServico> findForSpedPiscofinsRegA0100(SpedpiscofinsFiltro filtro, Empresa empresa, boolean haveNfse) {
		if(filtro != null) {
			QueryBuilder<NotaFiscalServico> query = querySined()
					
					.select("cliente.cdpessoa, cliente.cnpj, cliente.cpf, " +
							"notaFiscalServico.cdNota, notaFiscalServico.numero, notaFiscalServico.dtEmissao, empresa.cnpj, empresa.cdpessoa, " +
							"notaFiscalServico.basecalculoir, notaFiscalServico.basecalculoiss, notaFiscalServico.basecalculopis," +
							"notaFiscalServico.basecalculocofins, notaFiscalServico.basecalculocsll, notaFiscalServico.basecalculoinss," +
							"notaFiscalServico.basecalculoicms, notaFiscalServico.ir, notaFiscalServico.iss, " +
							"notaFiscalServico.pis, notaFiscalServico.cofins, notaFiscalServico.csll, notaFiscalServico.inss, " +
							"notaFiscalServico.icms, notaFiscalServico.incideir, notaFiscalServico.incideiss, notaFiscalServico.incidepis, " +
							"notaFiscalServico.incidecofins, notaFiscalServico.incidecsll, notaFiscalServico.incideinss, " +
							"notaFiscalServico.incideicms, notaFiscalServico.pisretido, notaFiscalServico.cofinsretido, " +
							"notaFiscalServico.dadosAdicionais, " +
							"regimetributacao.cdregimetributacao, regimetributacao.descricao, regimetributacao.codigonfse, " +
							"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.descricao, naturezaoperacao.codigonfse," +
							"item.qtde, item.descricao, item.precoUnitario, item.desconto, notaFiscalServico.indicadortipopagamento," +
							"notaFiscalServico.cstpis, notaFiscalServico.cstcofins, " +
							"listaGrupotributacaoimposto.controle, listaGrupotributacaoimposto.codigonaturezareceita, " +
							"vcontacontabilmaterial.identificador, vcontacontabilservicocontrato.identificador, " +
							"vcontacontabilcontrato.identificador, " +
//							"vcontagerencialcontabilservicocontratoDoc.identificador, " +
//							"vcontagerencialcontabilcontratoDoc.identificador, " +
							"materialNota.cdmaterial, materialNota.nome, materialNota.identificacao, material.cdmaterial, material.identificacao, material.nome ")
					.join("notaFiscalServico.notaStatus notaStatus")
					.join("notaFiscalServico.empresa empresa")
					.join("notaFiscalServico.cliente cliente")
					.leftOuterJoin("notaFiscalServico.regimetributacao regimetributacao")
					.leftOuterJoin("notaFiscalServico.naturezaoperacao naturezaoperacao")
					.join("notaFiscalServico.listaItens item")
					.leftOuterJoin("item.material materialNota")
					.leftOuterJoin("notaFiscalServico.grupotributacao grupotributacao")
					.leftOuterJoin("grupotributacao.listaGrupotributacaoimposto listaGrupotributacaoimposto")
					.leftOuterJoin("item.vendamaterial vendamaterial")
					.leftOuterJoin("vendamaterial.material material")
					.leftOuterJoin("material.contaContabil contacontabilmaterial")
					.leftOuterJoin("contacontabilmaterial.vcontacontabil vcontacontabilmaterial")
					.leftOuterJoin("notaFiscalServico.listaNotaContrato listaNotaContrato")
					.leftOuterJoin("listaNotaContrato.contrato contrato")
					.leftOuterJoin("contrato.listaContratomaterial listaContratomaterial")
					.leftOuterJoin("listaContratomaterial.servico servico")
					.leftOuterJoin("servico.contaContabil contacontabilcontrato")
					.leftOuterJoin("contacontabilcontrato.vcontacontabil vcontacontabilservicocontrato")
					.leftOuterJoin("contrato.contaContabil contacontabil")
					.leftOuterJoin("contacontabil.vcontacontabil vcontacontabilcontrato")
//					.leftOuterJoin("notaFiscalServico.listaNotaDocumento listaNotaDocumento")
//					.leftOuterJoin("listaNotaDocumento.documento documento")
//					.leftOuterJoin("documento.listaDocumentoOrigem listaDocumentoOrigem")
//					.leftOuterJoin("listaDocumentoOrigem.contrato contratoDoc")
//					.leftOuterJoin("contratoDoc.listaContratomaterial listaContratomaterialDoc")
//					.leftOuterJoin("listaContratomaterialDoc.servico servicoDoc")
//					.leftOuterJoin("servicoDoc.contagerencialcontabil contagerencialcontabilcontratoDoc")
//					.leftOuterJoin("contagerencialcontabilcontratoDoc.vcontagerencial vcontagerencialcontabilservicocontratoDoc")
//					.leftOuterJoin("contratoDoc.contagerencialcontabil contagerencialcontabilDoc")
//					.leftOuterJoin("contagerencialcontabilDoc.vcontagerencial vcontagerencialcontabilcontratoDoc")
					
					.where("empresa = ?", empresa);
			
			if(haveNfse){
				query.openParentheses()
					.where("notaStatus = ?", NotaStatus.NFSE_EMITIDA)
					.or()
					.where("notaStatus = ?", NotaStatus.NFSE_LIQUIDADA)
				.closeParentheses();
			} else {
				query.openParentheses()
					.where("notaStatus = ?", NotaStatus.EMITIDA)
					.or()
					.where("notaStatus = ?", NotaStatus.LIQUIDADA)
				.closeParentheses();
			}
			
			
				query
					.where("notaFiscalServico.dtEmissao >= ?", filtro.getDtinicio())
					.where("notaFiscalServico.dtEmissao <= ?", filtro.getDtfim())
				;			
			
			return query.list();
		} else
			return null;
	}
	
	/**
	* M�todo que busca as notas para a integra��o SAGE
	*
	* @param filtro
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public List<NotaFiscalServico> findForSagefiscal(SagefiscalFiltro filtro) {
		QueryBuilder<NotaFiscalServico> query = querySined()
				
				.select("cliente.cdpessoa, cliente.cpf, cliente.cnpj, " +
						"notaFiscalServico.cdNota, notaFiscalServico.numero, notaFiscalServico.dtEmissao, empresa.cnpj, " +
						"notaFiscalServico.descontocondicionado, notaFiscalServico.descontoincondicionado," +
						"notaFiscalServico.basecalculoir, notaFiscalServico.basecalculoiss, notaFiscalServico.basecalculopis," +
						"notaFiscalServico.basecalculocofins, notaFiscalServico.basecalculocsll, notaFiscalServico.basecalculoinss," +
						"notaFiscalServico.basecalculoicms, notaFiscalServico.ir, notaFiscalServico.iss, " +
						"notaFiscalServico.pis, notaFiscalServico.cofins, notaFiscalServico.csll, notaFiscalServico.inss, " +
						"notaFiscalServico.icms, notaFiscalServico.incideir, notaFiscalServico.incideiss, notaFiscalServico.incidepis, " +
						"notaFiscalServico.incidecofins, notaFiscalServico.incidecsll, notaFiscalServico.incideinss, " +
						"notaFiscalServico.incideicms, notaFiscalServico.pisretido, notaFiscalServico.cofinsretido, " +
						"notaFiscalServico.dadosAdicionais, " +
						"regimetributacao.cdregimetributacao, regimetributacao.descricao, regimetributacao.codigonfse, " +
						"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.descricao, naturezaoperacao.codigonfse," +
						"item.qtde, item.descricao, item.precoUnitario, item.desconto, notaFiscalServico.indicadortipopagamento," +
						"notaFiscalServico.cstpis, notaFiscalServico.cstcofins, " +
						"listaGrupotributacaoimposto.controle, listaGrupotributacaoimposto.codigonaturezareceita, " +
						"vendamaterial.cdvendamaterial, material.cdmaterial," +
						"enderecoCliente.cdendereco, enderecotipoCliente.cdenderecotipo, municipioCliente.cdmunicipio, municipioCliente.cdibge, ufCliente.cduf, ufCliente.sigla, paisCliente.cdpais, " +
						"enderecoEmpresa.cdendereco, enderecotipoEmpresa.cdenderecotipo, municipioEmpresa.cdmunicipio, municipioEmpresa.cdibge," +
						"contacontabil.cdcontacontabil, contacontabil.nome, vcontacontabil.identificador, empresa.crt," +
						"itemlistaservicoBean.codigo, notaStatus.cdNotaStatus, materialitem.cdmaterial, materialitem.nome, " +
						"contacontabilitem.cdcontacontabil, contacontabilitem.nome, vcontacontabilitem.identificador")
				.join("notaFiscalServico.notaStatus notaStatus")
				.join("notaFiscalServico.empresa empresa")
				.leftOuterJoin("empresa.listaEndereco enderecoEmpresa")
				.leftOuterJoin("enderecoEmpresa.enderecotipo enderecotipoEmpresa")
				.leftOuterJoin("enderecoEmpresa.municipio municipioEmpresa")
				.join("notaFiscalServico.cliente cliente")
				.leftOuterJoin("cliente.listaEndereco enderecoCliente")
				.leftOuterJoin("enderecoCliente.enderecotipo enderecotipoCliente")
				.leftOuterJoin("enderecoCliente.municipio municipioCliente")
				.leftOuterJoin("municipioCliente.uf ufCliente")
				.leftOuterJoin("enderecoCliente.pais paisCliente")
				.leftOuterJoin("notaFiscalServico.regimetributacao regimetributacao")
				.leftOuterJoin("notaFiscalServico.naturezaoperacao naturezaoperacao")
				.join("notaFiscalServico.listaItens item")
				.leftOuterJoin("item.vendamaterial vendamaterial")
				.leftOuterJoin("item.material materialitem")
				.leftOuterJoin("vendamaterial.material material")
				.leftOuterJoin("material.contaContabil contacontabil")
				.leftOuterJoin("contacontabil.vcontacontabil vcontacontabil")
				.leftOuterJoin("materialitem.contacontabil contacontabilitem")
				.leftOuterJoin("contacontabilitem.vcontacontabil vcontacontabilitem")
				.leftOuterJoin("notaFiscalServico.grupotributacao grupotributacao")
				.leftOuterJoin("grupotributacao.listaGrupotributacaoimposto listaGrupotributacaoimposto")
				.leftOuterJoin("notaFiscalServico.itemlistaservicoBean itemlistaservicoBean")
				.where("empresa = ?", filtro.getEmpresa())
//				.where("notaFiscalServico.cdNota = ?", 35896)
				.where("notaFiscalServico.dtEmissao >= ?", filtro.getDtinicio())
				.where("notaFiscalServico.dtEmissao <= ?", filtro.getDtfim())
				.openParentheses()
					.where("notaStatus = ?", NotaStatus.NFSE_EMITIDA)
					.or()
					.where("notaStatus = ?", NotaStatus.NFSE_LIQUIDADA)
					.or()
					.openParentheses()
						.where("notaStatus = ?", NotaStatus.CANCELADA)
						.openParentheses()
							.where("exists(select nh.cdNotaHistorico from NotaHistorico nh where nh.nota = notaFiscalServico and nh.notaStatus.cdNotaStatus in (" + NotaStatus.NFSE_EMITIDA.getCdNotaStatus() + ", " + NotaStatus.NFSE_LIQUIDADA.getCdNotaStatus() + "))")
							.or()
							.where("exists (select 1 from NotaFiscalServicoRPS rps where rps.notaFiscalServico = notaFiscalServico and rps.notaFiscalServicoLoteRPS is not null)")
						.closeParentheses()
					.closeParentheses()
				.closeParentheses();
		
		
		return query.list();
	}

	/**
	 * Verifica se existe uma nota da empresa com o n�mero passado por parametro
	 *
	 * @param empresa
	 * @param numeroNota
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/12/2013
	 */
	public boolean isExisteNota(Empresa empresa, String numeroNota) {
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(beanClass)
					.where("empresa = ?", empresa)			
					.where("notaFiscalServico.numero = ?", numeroNota)
					.join("notaFiscalServico.empresa empresa")
					.where("notaFiscalServico.notaStatus <> ?", NotaStatus.CANCELADA)
					.unique() > 0;
	}

	/**
	 * Busca os registros de nota para o cancelamento da NFS-e.
	 *
	 * @param whereIn
	 * @return
	 * @since 28/02/2012
	 * @author Rodrigo Freitas
	 */
	public List<NotaFiscalServico> findForCancelamentoNfse(String whereIn) {
		return query()
					.select("notaFiscalServico.cdNota, notaFiscalServico.numero, empresa.cdpessoa, arquivonf.cdarquivonf, arquivonfnota.codigoverificacao, " +
							"empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.inscricaomunicipal, arquivonfnota.numeronfse, " +
							"arquivonfnota.cdarquivonfnota, documento.cddocumento, configuracaonfe.prefixowebservice, configuracaonfe.inscricaomunicipal, " +
							"arquivonf.numeroLote, arquivonf.numerorecibo, configuracaonfe.senhaparacatu, configuracaonfe.token, configuracaonfe.loginparacatu ")
					.leftOuterJoin("notaFiscalServico.empresa empresa")
					.leftOuterJoin("notaFiscalServico.listaNotaDocumento notadocumento")
					.leftOuterJoin("notadocumento.documento documento")
					.leftOuterJoin("notaFiscalServico.listaArquivonfnota arquivonfnota")
					.leftOuterJoin("arquivonfnota.arquivonf arquivonf")
					.leftOuterJoin("arquivonf.configuracaonfe configuracaonfe")
					.where("arquivonfnota.dtcancelamento is null")
					.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_SUCESSO)
					.whereIn("notaFiscalServico.cdNota", whereIn)
					.list();
	}

	/**
	 * M�todo que busca as informa��es para o registro75 do SIntegra
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<NotaFiscalServico> findForSIntegraRegistro75(SintegraFiltro filtro) {
		if(filtro == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				
				.select("cliente.cdpessoa, " +
							"notaFiscalServico.cdNota, notaFiscalServico.numero, notaFiscalServico.dtEmissao, " +
							"notaFiscalServico.basecalculoir, notaFiscalServico.basecalculoiss, notaFiscalServico.basecalculopis," +
							"notaFiscalServico.basecalculocofins, notaFiscalServico.basecalculocsll, notaFiscalServico.basecalculoinss," +
							"notaFiscalServico.basecalculoicms, notaFiscalServico.ir, notaFiscalServico.iss, " +
							"notaFiscalServico.pis, notaFiscalServico.cofins, notaFiscalServico.csll, notaFiscalServico.inss, " +
							"notaFiscalServico.icms, notaFiscalServico.incideir, notaFiscalServico.incideiss, notaFiscalServico.incidepis, " +
							"notaFiscalServico.incidecofins, notaFiscalServico.incidecsll, notaFiscalServico.incideinss, " +
							"notaFiscalServico.incideicms, notaFiscalServico.dadosAdicionais, " +
							"regimetributacao.cdregimetributacao, regimetributacao.descricao, regimetributacao.codigonfse, " +
							"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.descricao, naturezaoperacao.codigonfse," +
							"item.qtde, item.descricao, item.precoUnitario, item.desconto, notaFiscalServico.indicadortipopagamento," +
							"notaFiscalServico.cstpis, notaFiscalServico.cstcofins ")
					.join("notaFiscalServico.notaStatus notaStatus")
					.join("notaFiscalServico.empresa empresa")
					.join("notaFiscalServico.cliente cliente")
					.join("notaFiscalServico.listaItens item")
					.leftOuterJoin("notaFiscalServico.regimetributacao regimetributacao")
					.leftOuterJoin("notaFiscalServico.naturezaoperacao naturezaoperacao")
					.where("empresa = ?", filtro.getEmpresa())					
					.openParentheses()
						.where("notaStatus = ?", NotaStatus.NFSE_EMITIDA)
						.or()
						.where("notaStatus = ?", NotaStatus.NFSE_LIQUIDADA)
					.closeParentheses()
					.where("notaFiscalServico.dtEmissao >= ?", filtro.getDtinicio())
					.where("notaFiscalServico.dtEmissao <= ?", filtro.getDtfim())
					.list();
	}

	/**
	 * M�todo que busca as informa��es para o registro50 do SIntegra
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<NotaFiscalServico> findForSIntegraRegistro50(SintegraFiltro filtro) {
		if(filtro == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				
				.select("cliente.cdpessoa, cliente.cnpj, cliente.cpf, cliente.inscricaoestadual, notaFiscalServico.cdNota, " +
						"notaFiscalServico.dtEmissao, ufinscricaoestadual.sigla, notaFiscalServico.numero, " +
						"notaFiscalServico.basecalculoicms, notaFiscalServico.icms, listaItens.precoUnitario, listaItens.qtde, " +
						"listaItens.desconto, listaItens.descricao, notaStatus.cdNotaStatus, notaFiscalServico.cstpis, " +
						"notaFiscalServico.basecalculoir, notaFiscalServico.basecalculoiss, notaFiscalServico.basecalculopis," +
						"notaFiscalServico.basecalculocofins, notaFiscalServico.basecalculocsll, notaFiscalServico.basecalculoinss," +
						"notaFiscalServico.ir, notaFiscalServico.iss, notaFiscalServico.pis, notaFiscalServico.cofins, " +
						"notaFiscalServico.csll, notaFiscalServico.inss, notaFiscalServico.incideir, notaFiscalServico.incideiss, notaFiscalServico.incidepis, " +
						"notaFiscalServico.incidecofins, notaFiscalServico.incidecsll, notaFiscalServico.incideinss, " +
						"notaFiscalServico.incideicms, endereco.cdendereco, municipio.cdmunicipio, uf.sigla, pais.cdpais, " +
						"regimetributacao.cdregimetributacao, regimetributacao.descricao, regimetributacao.codigonfse, " +
						"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.descricao, naturezaoperacao.codigonfse " )
				.join("notaFiscalServico.cliente cliente")
				.leftOuterJoin("cliente.ufinscricaoestadual ufinscricaoestadual")
				.leftOuterJoin("cliente.listaEndereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("endereco.pais pais")
				.join("notaFiscalServico.notaStatus notaStatus")
				.leftOuterJoin("notaFiscalServico.empresa empresa")
				.leftOuterJoin("notaFiscalServico.regimetributacao regimetributacao")
				.leftOuterJoin("notaFiscalServico.naturezaoperacao naturezaoperacao")
				.join("notaFiscalServico.listaItens listaItens")
				.leftOuterJoin("notaFiscalServico.listaNotaHistorico listaNotaHistorico")
				.where("empresa = ?", filtro.getEmpresa())
				.where("notaFiscalServico.dtEmissao >= ?", filtro.getDtinicio())
				.where("notaFiscalServico.dtEmissao <= ?", filtro.getDtfim())
				.openParentheses()
					.where("notaStatus = ?", NotaStatus.NFSE_EMITIDA)
					.or()
					.where("notaStatus = ?", NotaStatus.NFSE_LIQUIDADA)
					.or()
					.openParentheses()
						.where("notaStatus = ?", NotaStatus.CANCELADA)
						.openParentheses()
							.where("listaNotaHistorico.notaStatus = ?", NotaStatus.NFSE_EMITIDA).or()
							.where("listaNotaHistorico.notaStatus = ?", NotaStatus.NFSE_LIQUIDADA)
						.closeParentheses()
					.closeParentheses()
				.closeParentheses()
				.list();
	}

	/**
	 * 
	 * M�todo que carrega os valores dos descontos na nota para a gera��o de receita das notas de servi�o.
	 *
	 *@author Thiago Augusto
	 *@date 28/03/2012
	 * @param nota
	 * @return
	 */
	public NotaFiscalServico findDescontosByNota(Nota nota){
		return query()
					.select("notaFiscalServico.cdNota, notaFiscalServico.descontocondicionado, notaFiscalServico.descontoincondicionado, notaFiscalServico.deducao, notaFiscalServico.outrasretencoes")
					.where("notaFiscalServico = ?", nota)
					.unique();
	}

//	/**
//	 * M�todo para buscar as notas com o xml
//	 *
//	 * @param whereIn
//	 * @return
//	 * @author Luiz Fernando
//	 */
//	public List<NotaFiscalServico> findForEnviarXmlParaCliente(String whereIn) {
//		if(whereIn == null || "".equals(whereIn))
//			throw new SinedException("Par�metro inv�lido.");		
//		
//		return query()
//					.select("notaFiscalServico.cdNota, cliente.cdpessoa, cliente.nome, cliente.email, " +
//							"listaArquivonfnota.cdarquivonfnota, arquivoxml.cdarquivo, arquivoxml.nome, arquivoxml.tipoconteudo, " +
//							"arquivoxml.tamanho, arquivonf.cdarquivonf, arquivonf.arquivonfsituacao, listaArquivonfnota.numeronfse, " +
//							"notaStatus.cdNotaStatus, notaStatus.nome ")
//					.leftOuterJoin("notaFiscalServico.cliente cliente")
//					.leftOuterJoin("notaFiscalServico.listaArquivonfnota listaArquivonfnota")
//					.leftOuterJoin("listaArquivonfnota.arquivonf arquivonf")
//					.leftOuterJoin("arquivonf.arquivoxml arquivoxml")
//					.leftOuterJoin("notaFiscalServico.notaStatus notaStatus")
//					.whereIn("notaFiscalServico.cdNota", whereIn)
//					.list();
//	}

	/**
	 * M�todo que registra no hist�rico da nota o envio do xml
	 *
	 * @param nfs
	 * @param historico
	 * @author Luiz Fernando
	 */
	public void registrarHistoricoEnvioEmailXmlCliente(Nota nfs, String historico) {
		if(nfs != null && nfs.getCdNota() != null && historico != null && !"".equals(historico)){
			
			String insert = "INSERT INTO NOTAHISTORICO (CDNOTAHISTORICO, CDNOTA, CDNOTASTATUS, CDUSUARIOALTERA, DTALTERA, OBSERVACAO) " +
				" VALUES (nextval('sq_notahistorico'), " + 
				nfs.getCdNota() + ", " + 
				nfs.getNotaStatus().getCdNotaStatus() + ", " +
				SinedUtil.getUsuarioLogado().getCdpessoa() + ", '" + 
				new Timestamp(System.currentTimeMillis()) + "', '" + 
				historico + "');";
			
			getJdbcTemplate().execute(insert);
		}
	}

	/**
	 * Busca as notas de servi�o para rec�lculo do comissinamento.
	 *
	 * @param whereIn
	 * @param dtrecalculoDe
	 * @param dtrecalculoAte
	 * @return
	 * @since 18/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<NotaFiscalServico> findForRecalculoComissao(String whereIn, Date dtrecalculoDe, Date dtrecalculoAte) {
		return query()
					.select("notaFiscalServico.cdNota, contrato.cdcontrato, comissionamento.cdcomissionamento, comissionamento.tipobccomissionamento")
					.join("notaFiscalServico.listaNotaContrato notaContrato")
					.join("notaContrato.contrato contrato")
					.leftOuterJoin("notaFiscalServico.listaNotaDocumento notadocumento")
					.leftOuterJoin("contrato.comissionamento comissionamento")
					.whereIn("contrato.cdcontrato", whereIn)
					.where("notaFiscalServico.dtEmissao >= ?", dtrecalculoDe)
					.where("notaFiscalServico.dtEmissao <= ?", dtrecalculoAte)
					.where("notadocumento is null")
					.orderBy("notaFiscalServico.dtEmissao, notaFiscalServico.cdNota")
					.list();
	}
	
	/**
	 * M�todo que busca os dados da nota para apura��o de pis/cofins
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<NotaFiscalServico> findforApuracaopiscofins(ApuracaopiscofinsFiltro filtro){
		if(filtro == null)
			throw new SinedException("Filtro n�o pode ser nulo");
		
		return querySined()
				
				.select("cliente.cdpessoa, cliente.razaosocial, cliente.nome, cliente.cnpj, cliente.cpf, " +
						"notaFiscalServico.cdNota, notaFiscalServico.numero, notaFiscalServico.dtEmissao, " +
						"notaFiscalServico.basecalculoir, notaFiscalServico.basecalculoiss, notaFiscalServico.basecalculopis," +
						"notaFiscalServico.basecalculocofins, notaFiscalServico.basecalculocsll, notaFiscalServico.basecalculoinss," +
						"notaFiscalServico.basecalculoicms, notaFiscalServico.ir, notaFiscalServico.iss, " +
						"notaFiscalServico.pis, notaFiscalServico.cofins, notaFiscalServico.csll, notaFiscalServico.inss, " +
						"notaFiscalServico.icms, notaFiscalServico.incideir, notaFiscalServico.incideiss, notaFiscalServico.incidepis, " +
						"notaFiscalServico.incidecofins, notaFiscalServico.incidecsll, notaFiscalServico.incideinss, " +
						"notaFiscalServico.incideicms, notaFiscalServico.dadosAdicionais, " +
						"item.qtde, item.descricao, item.precoUnitario, item.desconto, notaFiscalServico.indicadortipopagamento," +
						"notaFiscalServico.cstpis, notaFiscalServico.cstcofins, " +
						"regimetributacao.cdregimetributacao, regimetributacao.descricao, regimetributacao.codigonfse, " +
						"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.descricao, naturezaoperacao.codigonfse, " +
						"notaFiscalServico.cstpis, notaFiscalServico.cstcofins")
				.join("notaFiscalServico.notaStatus notaStatus")
				.join("notaFiscalServico.empresa empresa")
				.join("notaFiscalServico.cliente cliente")
				.join("notaFiscalServico.listaItens item")
				.leftOuterJoin("notaFiscalServico.regimetributacao regimetributacao")
				.leftOuterJoin("notaFiscalServico.naturezaoperacao naturezaoperacao")
				.where("empresa = ?", filtro.getEmpresa())					
				.openParentheses()
					.where("notaStatus = ?", NotaStatus.NFSE_EMITIDA)
					.or()
					.where("notaStatus = ?", NotaStatus.NFSE_LIQUIDADA)
					.or()
					.where("notaStatus = ?", NotaStatus.EMITIDA)
					.or()
					.where("notaStatus = ?", NotaStatus.LIQUIDADA)
				.closeParentheses()
				.where("notaFiscalServico.dtEmissao >= ?", filtro.getDtinicio())
				.where("notaFiscalServico.dtEmissao <= ?", filtro.getDtfim())
//				.openParentheses()
//					.where("notaFiscalServico.pis is not null")
//					.or()
//					.where("notaFiscalServico.cofins is not null")
//				.closeParentheses()
				.orderBy("notaFiscalServico.dtEmissao")
				.list();
	}
	
	/**
	 * M�todo que retorna um booleano que indica se existem notas com data de vencimento/emissao referente a um per�odo fechado.
	 * @param whereIn
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaListaFechamento(String whereIn){
		if(whereIn == null || "".equals((whereIn)) )
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
				.setUseTranslator(Boolean.FALSE)
				.from(NotaFiscalServico.class)
				.select("count(*)")
				.whereIn("notaFiscalServico.cdNota", whereIn)
				.where("exists ( SELECT f.id " +
								" FROM Fechamentofinanceiro f " +
								" WHERE f.empresa = notaFiscalServico.empresa " +
								" AND coalesce(f.ativo, false) = true " + 
								" AND f.datainicio <= coalesce(notaFiscalServico.dtVencimento, notaFiscalServico.dtEmissao) " +
								" AND f.datalimite >= coalesce(notaFiscalServico.dtVencimento, notaFiscalServico.dtEmissao) )")
																		
				.unique() > 0;
	}

	/**
	 * M�todo que retorna um booleano que indica se existem notas com o numeronfse j� cadastrada.
	 * @param empresa
	 * @return
	 * @autor Thiers Euler
	 */
	public boolean isExisteNotaByNumeronfse(Empresa empresa, String numeronfse, Configuracaonfe configuracaonfe) {
		return newQueryBuilder(Long.class)
		.select("count(*)")
		.setUseTranslator(false)
		.from(beanClass)
		.where("empresa = ?", empresa)			
		.join("notaFiscalServico.empresa empresa")
		.join("notaFiscalServico.listaArquivonfnota listaArquivonfnota")
		.join("listaArquivonfnota.arquivonf arquivonf")
		.where("listaArquivonfnota.numeronfse = ?", numeronfse)
		.where("arquivonf.configuracaonfe = ?", configuracaonfe)
		.where("notaFiscalServico.notaStatus <> ?", NotaStatus.CANCELADA)
		.unique() > 0;
	}

	/**
	 * M�todo que carrega as notas para serem associadas. Obs: apenas as notas referente a pessoa do documento
	 *
	 * @param whereInDocumentos
	 * @return
	 * @author Luiz Fernando
	 * @param listaSituacao 
	 * @param dtEmissao2 
	 * @param dtEmissao1 
	 */
	public List<NotaFiscalServico> loadNotasServicoAssociar(String whereInDocumentos, Date dtEmissao1, Date dtEmissao2, List<NotaStatus> listaSituacao) {
		if(SinedUtil.isListEmpty(listaSituacao)){
			listaSituacao = new ArrayList<NotaStatus>();
			listaSituacao.add(NotaStatus.EMITIDA);
			listaSituacao.add(NotaStatus.NFSE_EMITIDA);
			listaSituacao.add(NotaStatus.LIQUIDADA);
			listaSituacao.add(NotaStatus.NFE_LIQUIDADA);
		}
		return query()
				.select("notaFiscalServico.cdNota, notaFiscalServico.dtEmissao, notaFiscalServico.numero, notaStatus.nome")
				.join("notaFiscalServico.notaTipo notaTipo")
				.join("notaFiscalServico.notaStatus notaStatus")
				.leftOuterJoin("notaFiscalServico.cliente cliente")
				.where("notaTipo = ?", NotaTipo.NOTA_FISCAL_SERVICO)
				.where("notaFiscalServico.dtEmissao >= ?", dtEmissao1)
				.where("notaFiscalServico.dtEmissao <= ?", dtEmissao2)
				.whereIn("notaStatus.cdNotaStatus", CollectionsUtil.listAndConcatenate(listaSituacao, "cdNotaStatus", ","))
				.orderBy("notaFiscalServico.dtEmissao desc")
				.where("cliente.cdpessoa in (SELECT d.pessoa.cdpessoa FROM Documento d where d.cddocumento in (" + whereInDocumentos + ") )")
				.list();
	}

	public List<NotaFiscalServico> findForConferenciaNfse() {
		return query()
					.select("notaFiscalServico.cdNota, notaFiscalServico.numero, cliente.nome, cliente.cnpj, cliente.cpf, " +
							"item.precoUnitario, item.qtde, item.desconto, item.descricao")
					.join("notaFiscalServico.notaTipo notaTipo")
					.join("notaFiscalServico.notaStatus notaStatus")
					.leftOuterJoin("notaFiscalServico.cliente cliente")
					.join("notaFiscalServico.listaItens item")
					.where("notaTipo = ?", NotaTipo.NOTA_FISCAL_SERVICO)
					.where("notaStatus = ?", NotaStatus.NFSE_SOLICITADA)
					.orderBy("notaFiscalServico.numero")
					.list();
	}
	
	/**
	 * M�todo que busca as notas canceladas para atualizar a OS
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<NotaFiscalServico> findForAtualizarRequisicao(String whereIn) {
		if(whereIn == null || "".equals((whereIn)) )
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("notaFiscalServico.cdNota, item.cdNotaFiscalServicoItem")
				.join("notaFiscalServico.listaItens item")
				.join("notaFiscalServico.notaStatus notaStatus")
				.whereIn("notaFiscalServico.cdNota", whereIn)
				.where("notaStatus = ?", NotaStatus.CANCELADA)
				.list();
	}

	/**
	 * M�todo que busca as notas para apura��o de outros impostos
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<NotaFiscalServico> findForApuracaoimposto(ApuracaoimpostoFiltro filtro) {
		if(filtro == null)
			throw new SinedException("Par�metro inv�lido.");
		
		if(filtro.getFaixaimpostoapuracao() == null || Faixaimpostoapuracao.IMPORTACAO.equals(filtro.getFaixaimpostoapuracao()))
			return null;
		
		QueryBuilder<NotaFiscalServico> query = querySined()
			.select("cliente.cdpessoa, cliente.cpf, cliente.cnpj, cliente.nome, " +
					"notaFiscalServico.cdNota, notaFiscalServico.numero, notaFiscalServico.dtEmissao, " +
					"notaFiscalServico.basecalculoir, notaFiscalServico.basecalculoiss, notaFiscalServico.basecalculopis," +
					"notaFiscalServico.basecalculocofins, notaFiscalServico.basecalculocsll, notaFiscalServico.basecalculoinss," +
					"notaFiscalServico.basecalculoicms, notaFiscalServico.ir, notaFiscalServico.iss, " +
					"notaFiscalServico.pis, notaFiscalServico.cofins, notaFiscalServico.csll, notaFiscalServico.inss, " +
					"notaFiscalServico.icms, notaFiscalServico.incideir, notaFiscalServico.incideiss, notaFiscalServico.incidepis, " +
					"notaFiscalServico.incidecofins, notaFiscalServico.incidecsll, notaFiscalServico.incideinss, " +
					"notaFiscalServico.incideicms, " +
					"notaFiscalServico.dadosAdicionais, notaFiscalServico.naturezaoperacao, " +
					"item.qtde, item.descricao, item.precoUnitario, item.desconto, notaFiscalServico.indicadortipopagamento," +
					"notaFiscalServico.cstpis, notaFiscalServico.cstcofins ")
			.join("notaFiscalServico.notaStatus notaStatus")
			.join("notaFiscalServico.empresa empresa")
			.join("notaFiscalServico.cliente cliente")
			.join("notaFiscalServico.listaItens item")
			.where("empresa = ?", filtro.getEmpresa())					
			.openParentheses()
				.where("notaStatus = ?", NotaStatus.NFSE_EMITIDA)
				.or()
				.where("notaStatus = ?", NotaStatus.NFSE_LIQUIDADA)
				.or()
				.where("notaStatus = ?", NotaStatus.EMITIDA)
				.or()
				.where("notaStatus = ?", NotaStatus.LIQUIDADA)
			.closeParentheses()
			.where("notaFiscalServico.dtEmissao >= ?", filtro.getDtinicio())
			.where("notaFiscalServico.dtEmissao <= ?", filtro.getDtfim());
		
		if(filtro.getFaixaimpostoapuracao() != null && !Faixaimpostoapuracao.IMPORTACAO.equals(filtro.getFaixaimpostoapuracao())){
			if(Faixaimpostoapuracao.ISS.equals(filtro.getFaixaimpostoapuracao())){
				query.where("notaFiscalServico.iss is not null and notaFiscalServico.iss > 0");
				if(filtro.getImpostoretido()){
					query.where("notaFiscalServico.incideiss = true");
				}
			}else if(Faixaimpostoapuracao.CSLL.equals(filtro.getFaixaimpostoapuracao())){
				query.where("notaFiscalServico.csll is not null and notaFiscalServico.csll > 0");
				if(filtro.getImpostoretido()){
					query.where("notaFiscalServico.incidecsll = true");
				}
			}else if(Faixaimpostoapuracao.IR.equals(filtro.getFaixaimpostoapuracao())){
				query.where("notaFiscalServico.ir is not null and notaFiscalServico.ir > 0");
				if(filtro.getImpostoretido()){
					query.where("notaFiscalServico.incideir = true");
				}
			}else if(Faixaimpostoapuracao.INSS.equals(filtro.getFaixaimpostoapuracao())){
				query.where("notaFiscalServico.inss is not null and notaFiscalServico.inss > 0");
				if(filtro.getImpostoretido()){
					query.where("notaFiscalServico.incideinss = true");
				}
			}
		}
		
		return query
				.orderBy("notaFiscalServico.dtEmissao, notaFiscalServico.numero")
				.list();
			
	}
	
	/**
	 * 
	 * @param whereInCddocumento
	 * @param notaStatus
	 * @author Thiago Clemente
	 * 
	 */
	public boolean isPossuiNotaFiscalServico(String whereInCddocumento, NotaStatus... notaStatus){
		
		QueryBuilder<Long> queryBuilder = newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.join("notaFiscalServico.listaNotaDocumento listaNotaDocumento")
				.join("listaNotaDocumento.documento documento")
				.whereIn("documento.cddocumento", whereInCddocumento);

		if (notaStatus!=null && notaStatus.length>0){
			String whereInNotaStatus = "";
			for (int i=0; i<notaStatus.length; i++) {
				whereInNotaStatus += notaStatus[i].getCdNotaStatus() + ",";
			}
			whereInNotaStatus = whereInNotaStatus.substring(0, whereInNotaStatus.length()-1);
			queryBuilder.join("notaFiscalServico.notaStatus notaStatus");
			queryBuilder.whereIn("notaStatus.cdNotaStatus", whereInNotaStatus);
		}
		
		return queryBuilder.unique()>0;
	}

	/**
	 * Busca os dados da nota para a atualiza��o da data de emiss�o na cria��o da NFS-e
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/07/2013
	 */
	public List<NotaFiscalServico> findForAtualizacaoData(String whereIn) {
		return query()
					.select("notaFiscalServico.cdNota, notaFiscalServico.dtEmissao, notaFiscalServico.dtVencimento")
					.whereIn("notaFiscalServico.cdNota", whereIn)
					.list();
	}

	/**
	 * Atualiza a data de emiss�o com a data atual.
	 *
	 * @param nfs
	 * @author Rodrigo Freitas
	 * @since 11/07/2013
	 */
	public void updateDataemissaoAtual(NotaFiscalServico nfs) {
		getJdbcTemplate().update("UPDATE NOTA SET DTEMISSAO = ? WHERE CDNOTA = ?", new Object[]{
			SinedDateUtils.currentDate(), nfs.getCdNota()
		});
	}

	/**
	 * Atualiza a data de vencimento com a data atual.
	 *
	 * @param nfs
	 * @author Rodrigo Freitas
	 * @since 11/07/2013
	 */
	public void updateDatavencimentoAtual(NotaFiscalServico nfs) {
		getJdbcTemplate().update("UPDATE NOTAFISCALSERVICO SET DTVENCIMENTO = ? WHERE CDNOTA = ?", new Object[]{
			SinedDateUtils.currentDate(), nfs.getCdNota()
		});
	}

	/**
	 * 
	 * @param idnota
	 */
	public List<NotaFiscalServico> validaNotaStatus(String idnota) {
		return query()
			.select("notaFiscalServico.notaStatus")
			.whereIn("notaFiscalServico.cdNota",idnota)
			.list();
	}

	/**
	 * 
	 * @param idnota
	 * @param cstcofins
	 * @param cstpis
	 */
	public void atualizaDados(String idnota, Tipocobrancacofins cstcofins, Tipocobrancapis cstpis) {
		if (idnota==null || idnota.trim().equals("")){
			return;
		}
		String set = "";
		
		if (cstcofins!=null){
			set += " CSTCOFINS = " + cstcofins.getValue();
		}
		
		if (cstpis!=null){
			set += !set.equals("") ? ", " : "";
			set += " CSTPIS = " + cstpis.getValue();
		}
		
		if (!set.equals("")){
			getJdbcTemplate().update("UPDATE NOTAFISCALSERVICO SET " + set + " WHERE CDNOTA IN ("+idnota+")");
		}
		
//		getJdbcTemplate().update("UPDATE NOTAFISCALSERVICO SET CSTCOFINS = "+cstcofins.getValue()+", CSTPIS = "+cstpis.getValue()+" WHERE CDNOTA IN ("+idnota+")");
	}

	/**
	* M�todo que busca as notas para recalcular os impostos
	*
	* @param whereIn
	* @return
	* @since 18/11/2014
	* @author Luiz Fernando
	*/
	public List<NotaFiscalServico> buscarNotaRecalcularImposto(String whereIn) {
		return query()
			.select("notaFiscalServico.cdNota, notaFiscalServico.dtEmissao")
			.where("notaFiscalServico.notaTipo = ?", NotaTipo.NOTA_FISCAL_SERVICO)
			.whereIn("notaFiscalServico.cdNota", whereIn)
			.list();
	}

	public List<NotaFiscalServico> buscarNotasValidacaoEdicaoNumeroPosterior(Cliente cliente, String numero) {
		if(StringUtils.isEmpty(numero) || cliente == null || cliente.getCdpessoa() == null)
			return null;
		
		String cnpj = cliente.getCnpj() != null ? cliente.getCnpj().getValue() : null;
		if (cnpj != null && cnpj.length() > 7)
			cnpj = cnpj.substring(0,8);
		
		//Se for contabilidade centralizada, carrega as notas das filiais tamb�m
		boolean contabilidadecentralizada = cliente.getContabilidadecentralizada() != null && cnpj != null && cnpj.length() == 8 ? cliente.getContabilidadecentralizada() : false;
		
		QueryBuilder<NotaFiscalServico> query = query()
				.select("notaFiscalServico.cdNota, notaFiscalServico.numero")
				.join("notaFiscalServico.notaStatus notaStatus")
				.join("notaFiscalServico.notaTipo notaTipo")
				.join("notaFiscalServico.cliente cliente")
				.where("notaStatus <> ? ", NotaStatus.CANCELADA)
				.openParentheses()
					.where("notaStatus = ?", NotaStatus.EM_ESPERA)
					.or()
					.where("notaStatus = ?", NotaStatus.EMITIDA)
				.closeParentheses()
				.where("notaFiscalServico.numero is not null")
				.where("notaFiscalServico.numero <> ''")
				.where("cast(notaFiscalServico.numero AS long) < ?", Long.parseLong(numero));
		
		if (contabilidadecentralizada)
			query.where("cliente.cnpj LIKE '" + cnpj + "%' ");
		else	
			query.where("cliente = ?", cliente);
		
		return query.list();
	}
	
	public List<NotaFiscalServico> findForGerarLancamentoContabil(GerarLancamentoContabilFiltro filtro, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento, String whereNotIn, String whereInNaturezaOperacao){
		if(StringUtils.isBlank(whereNotIn)){
			whereNotIn = "-1";
		}
		
		return query()
				.select("notaFiscalServico.cdNota, notaFiscalServico.dtEmissao, notaFiscalServico.numero," +
						"notaFiscalServico.ir, notaFiscalServico.iss, notaFiscalServico.inss, notaFiscalServico.csll, notaFiscalServico.cofins, " +
						"notaFiscalServico.pis, notaFiscalServico.icms, notaFiscalServico.impostocumulativoinss, " +
						"notaFiscalServico.impostocumulativoir, notaFiscalServico.impostocumulativoiss, " +
						"notaFiscalServico.impostocumulativoicms, notaFiscalServico.impostocumulativopis," +
						"notaFiscalServico.impostocumulativocofins, notaFiscalServico.impostocumulativocsll, " +
						"notaFiscalServico.incideiss, notaFiscalServico.incideinss, notaFiscalServico.incidecsll, notaFiscalServico.incidecofins, " +
						"notaFiscalServico.incidepis, notaFiscalServico.incideicms, notaFiscalServico.incideir, " +
						"notaFiscalServico.pisretido, notaFiscalServico.cofinsretido, " +
						"notaFiscalServico.basecalculo, notaFiscalServico.descontocondicionado, notaFiscalServico.descontoincondicionado, " +
						"notaFiscalServico.deducao, notaFiscalServico.outrasretencoes, notaFiscalServico.basecalculoir, notaFiscalServico.basecalculoiss," +
						"notaFiscalServico.basecalculopis, notaFiscalServico.basecalculocofins, " +
						"notaFiscalServico.basecalculocsll, notaFiscalServico.basecalculoinss, notaFiscalServico.basecalculoicms," +
						"notaFiscalServico.cstpis, notaFiscalServico.cstcofins, notaFiscalServico.indicadortipopagamento," +
						"notaTipo.cdNotaTipo," +
						"cliente.nome, cliente.razaosocial," +
						"contaContabil.cdcontacontabil, contaContabil.nome," +
						"notaFiscalServico.situacaoissparacatu, notaFiscalServico.situacaoissbrumadinho," +
						"listaItens.cdNotaFiscalServicoItem, listaItens.qtde, listaItens.desconto, listaItens.descricao, listaItens.precoUnitario," +
						"operacaocontabilpagamento.cdoperacaocontabil, operacaocontabilpagamento.nome, operacaocontabilpagamento.tipo," +
						"operacaocontabilpagamento.historico, operacaocontabilpagamento.codigointegracao, operacaocontabilpagamento.agruparvalormesmaconta," +
						"tipoLancamento.cdOperacaoContabilTipoLancamento, tipoLancamento.nome, tipoLancamento.ordem, tipoLancamento.movimentacaocontabilTipoLancamento, " +
						"listaDebito.cdoperacaocontabildebitocredito, listaDebito.tipo, listaDebito.controle, listaDebito.centrocusto, listaDebito.projeto, listaDebito.historico, contacontabildebito.cdcontacontabil, contacontabildebito.nome, listaDebito.formula, listaDebito.codigointegracao, " +
						"listaCredito.cdoperacaocontabildebitocredito, listaCredito.tipo, listaCredito.controle, listaCredito.centrocusto, listaCredito.projeto, listaCredito.historico, contacontabilcredito.cdcontacontabil, contacontabilcredito.nome, listaCredito.formula, listaCredito.codigointegracao ")
				.join("notaFiscalServico.notaTipo notaTipo")
				.join("notaFiscalServico.cliente cliente")
				.leftOuterJoin("cliente.contaContabil contaContabil")
				.join("notaFiscalServico.listaItens listaItens")
				.leftOuterJoin("notaFiscalServico.naturezaoperacao naturezaoperacao")
				.leftOuterJoin("naturezaoperacao.operacaocontabilpagamento operacaocontabilpagamento")
				.leftOuterJoin("operacaocontabilpagamento.listaDebito listaDebito")
				.leftOuterJoin("listaDebito.contaContabil contacontabildebito")
				.leftOuterJoin("operacaocontabilpagamento.listaCredito listaCredito")
				.leftOuterJoin("listaCredito.contaContabil contacontabilcredito")
				.leftOuterJoin("operacaocontabilpagamento.tipoLancamento tipoLancamento")
				.where("notaFiscalServico.empresa=?", filtro.getEmpresa())
				.where("notaFiscalServico.dtEmissao>=?", filtro.getDtPeriodoInicio())
				.where("notaFiscalServico.dtEmissao<=?", filtro.getDtPeriodoFim())
				.where("notaFiscalServico.projeto=?", filtro.getProjeto())
				.whereIn("naturezaoperacao.cdnaturezaoperacao", whereInNaturezaOperacao)
				.where("notaFiscalServico.notaStatus not in (?, ?, ?, ?, ?)", new Object[]{NotaStatus.CANCELADA, NotaStatus.NFSE_CANCELANDO, NotaStatus.NFE_CANCELANDO, NotaStatus.NFE_DENEGADA, NotaStatus.EM_ESPERA})
				.where("coalesce(cliente.incidiriss, false)=?", Boolean.TRUE, MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamento))
				.where("not exists (select 1 from Movimentacaocontabilorigem mco where mco.nota=notaFiscalServico and mco.movimentacaocontabil.tipoLancamento=?)", new Object[]{movimentacaocontabilTipoLancamento})
				.whereIn("notaFiscalServico.cdNota not ", whereNotIn)
				.list();
	}
	
	public List<NotaFiscalServico> findNotaSemContaBoleto(String whereIn) {
		return query()
				.select("notaFiscalServico.cdNota, notaFiscalServico.numero ")
				.from(NotaFiscalServico.class)
				.leftOuterJoin("notaFiscalServico.listaDuplicata listaDuplicata")
				.leftOuterJoin("listaDuplicata.documentotipo documentotipo")
				.leftOuterJoin("notaFiscalServico.contaboleto contaboleto")
				.whereIn("notaFiscalServico.cdNota", whereIn)
				.where("coalesce(documentotipo.boleto, false) = true")
				.where("contaboleto is null")
				.list();
	}
	
	/**
	 * M�todo que busca as notas pelo wherein, com os atributos de imposto para poss�vel c�lculo do total
	 * 
	 * @param whereIn
	 * @return
	 * @author Mairon Cezar
	 */
	public List<NotaFiscalServico> findByWhereInForCalculototal(String whereIn) {
		return query()
			.select("notaFiscalServico.cdNota, notaFiscalServico.dtEmissao, notaFiscalServico.dtVencimento, notaFiscalServico.numero, notaFiscalServico.basecalculo, notaFiscalServico.inss, notaFiscalServico.ir, notaFiscalServico.iss, " +
					"notaFiscalServico.icms, notaFiscalServico.cofins, notaFiscalServico.pis, notaFiscalServico.csll, notaFiscalServico.outrasretencoes, " +
					"notaFiscalServico.descontoincondicionado, notaFiscalServico.descontocondicionado, listaItens.cdNotaFiscalServicoItem, listaItens.precoUnitario, listaItens.qtde, listaItens.desconto," +
					"notaFiscalServico.incideiss, notaFiscalServico.incideinss, notaFiscalServico.incidecsll, notaFiscalServico.incidecofins, " +
					"notaFiscalServico.incidepis, notaFiscalServico.incideicms, notaFiscalServico.incideir, " +
					"notaFiscalServico.impostocumulativoinss, " +
					"notaFiscalServico.impostocumulativoir, notaFiscalServico.impostocumulativoiss, " +
					"notaFiscalServico.impostocumulativoicms, notaFiscalServico.impostocumulativopis," +
					"notaFiscalServico.impostocumulativocofins, notaFiscalServico.impostocumulativocsll, " +
					"notaFiscalServico.basecalculoir, notaFiscalServico.basecalculoiss, notaFiscalServico.basecalculopis, notaFiscalServico.basecalculocofins, " +
					"notaFiscalServico.basecalculocsll, notaFiscalServico.basecalculoinss, notaFiscalServico.basecalculoicms," +
					"listaNotaDocumento.cdNotaDocumento, documento.cddocumento, documentoacao.cddocumentoacao, notaStatus.cdNotaStatus, notaFiscalServico.cadastrarcobranca, prazopagamentofatura.cdprazopagamento, notaFiscalServico.valorliquidofatura")
			.join("notaFiscalServico.listaItens listaItens")
			.leftOuterJoin("notaFiscalServico.notaStatus notaStatus")
			.leftOuterJoin("notaFiscalServico.prazopagamentofatura prazopagamentofatura")
			.leftOuterJoin("notaFiscalServico.listaNotaDocumento listaNotaDocumento")
			.leftOuterJoin("listaNotaDocumento.documento documento")
			.leftOuterJoin("documento.documentoacao documentoacao")
			.whereIn("notaFiscalServico.id", whereIn)
			.list();
	}

	/**
	 * Busca a nota pelo n�mero de NFS-e e Empresa
	 * 	 
	 * @param empresa
	 * @param numeronota
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/10/2017
	 */
	public NotaFiscalServico findNotaByNumeroEmpresa(Empresa empresa, String numeronota) {
		List<NotaFiscalServico> list = query()
					.select("notaFiscalServico.cdNota, notaFiscalServico.dtEmissao")
					.join("notaFiscalServico.listaArquivonfnota arquivonfnota")
					.join("arquivonfnota.arquivonf arquivonf")
					.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_SUCESSO)
					.where("arquivonfnota.numeronfse is not null")
					.where("arquivonfnota.codigoverificacao is not null")
					.where("arquivonfnota.numero_filtro = ?", Integer.parseInt(numeronota))
					.where("notaFiscalServico.empresa = ?", empresa)
					.list();
		
		
		if(list == null || list.size() == 0) return null;
		if(list.size() > 1) throw new SinedException("Mais de uma nota encontrada na busca solicitada.");
		return list.get(0);
	}
	
	public List<NotaFiscalServico> findNotaWithNaturezaoperacao(String whereIn){
		return query()
			.select("notaFiscalServico.cdNota, notaFiscalServico.dtEmissao, notaFiscalServico.numero, naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.simplesremessa")
			.leftOuterJoin("notaFiscalServico.naturezaoperacao naturezaoperacao")
			.whereIn("notaFiscalServico.id", whereIn)
			.list();
	}

	@SuppressWarnings("unchecked")
	public List<NotaFiscalServico> findByNotaSemContaReceber(Date dateToSearch) {
		List<NotaStatus> lista = new ArrayList<NotaStatus>();
		lista.add(NotaStatus.LIQUIDADA);
		lista.add(NotaStatus.NFSE_LIQUIDADA);
		lista.add(NotaStatus.NFE_LIQUIDADA);
		lista.add(NotaStatus.CANCELADA);
		lista.add(NotaStatus.NFSE_CANCELANDO);
		lista.add(NotaStatus.NFE_CANCELANDO);
		lista.add(NotaStatus.NFE_DENEGADA);
		
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("select p.cdnota, n.cdempresa, n.numero ");
		sql.append("from notafiscalservico p ");
		sql.append("join nota n on n.cdnota = p.cdnota ");
		sql.append("join naturezaoperacao no on no.cdnaturezaoperacao = p.cdnaturezaoperacao ");
		sql.append("join notadocumento nd on nd.cdnota = p.cdnota ");
		sql.append("join documento d on d.cddocumento = nd.cddocumento ");
		sql.append("where n.cdnotastatus not in (" + CollectionsUtil.listAndConcatenate(lista, "cdNotaStatus", ",") + ") ");
		sql.append("	and no.simplesremessa <> true and d.cddocumentoclasse = 2");
		sql.append("    and n.dtemissao between '" + dateToSearch + "' and current_date ");

		SinedUtil.markAsReader();
		List<NotaFiscalServico> notaFiscalList = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			
			@Override
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				NotaFiscalServico n = new NotaFiscalServico(rs.getInt("cdnota"));
				n.setNumero(rs.getString("numero"));
				if(rs.getInt("cdempresa") > 0) n.setEmpresa(new Empresa(rs.getInt("cdempresa")));
				return n;
			}
		});
		
		return notaFiscalList;
	}
	
	public List<NotaFiscalServico> findByCdNota(String whereIn) {
		return query()
				.leftOuterJoinFetch("notaFiscalServico.enderecoCliente enderecoCliente")
				.whereIn("notaFiscalServico.cdNota", whereIn)
				.list();
	}

	public List<NotaFiscalServico> findForConferenciaNfseSigBancos(Empresa empresa) {
		if (empresa == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("notaFiscalServico.cdNota, notaFiscalServico.numero, cliente.nome, cliente.cnpj, cliente.cpf, " +
						"item.precoUnitario, item.qtde, item.desconto, item.descricao")
				.join("notaFiscalServico.notaTipo notaTipo")
				.join("notaFiscalServico.notaStatus notaStatus")
				.leftOuterJoin("notaFiscalServico.cliente cliente")
				.leftOuterJoin("notaFiscalServico.empresa empresa")
				.join("notaFiscalServico.listaItens item")
				.where("notaTipo = ?", NotaTipo.NOTA_FISCAL_SERVICO)
				.openParentheses()
					.where("notaStatus = ?", NotaStatus.NFSE_SOLICITADA)
					.or()
					.where("notaStatus = ?", NotaStatus.EMITIDA)
				.closeParentheses()
				.where("empresa = ?", empresa)
				.orderBy("notaFiscalServico.numero")
				.list();
	}

	public NotaFiscalServico buscarNumeroNota(Integer cdNota) {
		return query()
				.select("notaFiscalServico.numero")
				.where("notaFiscalServico.cdNota = ?", cdNota)
				.unique();
	}
}