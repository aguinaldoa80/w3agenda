package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Arquivotef;
import br.com.linkcom.sined.geral.bean.Arquivotefhistorico;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ArquivotefhistoricoDAO extends GenericDAO<Arquivotefhistorico> {

	public List<Arquivotefhistorico> findByArquivotef(Arquivotef arquivotef) {
		if(arquivotef == null || arquivotef.getCdarquivotef() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.where("arquivotefhistorico.arquivotef = ?", arquivotef)
					.orderBy("arquivotefhistorico.dtaltera desc")
					.list();
	}

	
}
