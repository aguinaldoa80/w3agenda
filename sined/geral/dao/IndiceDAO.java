package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Indice;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.IndiceFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class IndiceDAO extends GenericDAO<Indice> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Indice> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		IndiceFiltro filtro = (IndiceFiltro) _filtro;
		query
			.select("indice.cdindice, indice.descricao, indice.qtde, indice.identificador, unidademedida.nome")
			.join("indice.unidademedida unidademedida")
			.whereLikeIgnoreAll("indice.descricao", filtro.getDescricao())
			.whereLikeIgnoreAll("indice.identificador", filtro.getIdentificador())
			.orderBy("indice.descricao");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Indice> query) {
		query
			.select("indice.cdindice, indice.descricao, indice.identificador, unidademedida.cdunidademedida, indice.qtde, " +
					"listaIndicerecursogeral.cdindicerecursogeral, listaIndicerecursogeral.outro, listaIndicerecursogeral.qtde, material.cdmaterial, " +
					"material.nome, listaIndicerecursohumano.cdindicerecursohumano, listaIndicerecursohumano.qtde, cargo.cdcargo, " +
					"cargo.nome, indice.cdusuarioaltera, indice.dtaltera, unidademedidarecurso.cdunidademedida, unidademedidarecurso.nome")
			.leftOuterJoin("indice.listaIndicerecursogeral listaIndicerecursogeral")
			.leftOuterJoin("indice.listaIndicerecursohumano listaIndicerecursohumano")
			.leftOuterJoin("indice.unidademedida unidademedida")
			.leftOuterJoin("listaIndicerecursogeral.material material")
			.leftOuterJoin("listaIndicerecursogeral.unidademedida unidademedidarecurso")
			.leftOuterJoin("listaIndicerecursohumano.cargo cargo");
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {	
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				
				save.saveOrUpdateManaged("listaIndicerecursogeral");
				save.saveOrUpdateManaged("listaIndicerecursohumano");
				return null;
			}			
		});	
		
	}
	
	/**
	 * M�todo para carregar as listas de Indicerecursogeral e Indicerecursohumano de um Indice
	 * 
	 * @param indice
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Indice loadRecursos(Indice indice){
		if(indice == null || indice.getCdindice() == null){
			throw new SinedException("Os par�metros indice ou cdindice n�o podem ser null.");
		}
		return 
			query()
			.select("indice.cdindice, rgi.cdindicerecursogeral, rgi.quantidade, material.cdmaterial,material.nome," +
					"materialtipo.cdmaterialtipo,materialtipo.nome," +
					"rhi.cdindicerecursohumano,cargo.cdcargo,cargo.nome")
			.leftOuterJoin("indice.listaIndicerecursogeral rgi")
			.leftOuterJoin("rgi.material material")
			.leftOuterJoin("material.materialtipo materialtipo")
			.leftOuterJoin("indice.listaIndicerecursohumano rhi")
			.leftOuterJoin("rhi.cargo cargo")
			.unique();
	}

	/**
	 * Carrega o composi��o com a unidade de medida preenchido.
	 *
	 * @param indice
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Indice loadWithUnidade(Indice indice) {
		if (indice == null || indice.getCdindice() == null) {
			throw new SinedException("Indice n�o pode ser nulo.");
		}
		return query()
					.select("indice.cdindice, unidademedida.cdunidademedida")
					.join("indice.unidademedida unidademedida")
					.where("indice = ?",indice)
					.unique();
	}
	
	/**
	 * Carrega a lista de composi��o para exibi��o na parte em flex.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Indice> findAllForFlex() {
		return query()
					.select("indice.cdindice, indice.descricao, indice.identificador")
					.orderBy("indice.identificador, indice.descricao")
					.list();
	}
	
	/**
	 * Carrega o indice com as lista de recursos preenchidas.
	 *
	 * @param indice
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Indice findWithListasFlex(Indice indice) {
		if(indice == null || indice.getCdindice() == null){
			throw new SinedException("Indice n�o pode ser nulo.");			
		}
		return query()
					.select("indice.cdindice, indice.identificador, indice.qtde, unidademedida.cdunidademedida, unidademedida.nome, material.nome, " +
							"material.cdmaterial, material.identificacao, cargo.nome, cargo.cdcargo, material.valorcusto, cargo.custohora,  " +
							"listaIndicerecursogeral.cdindicerecursogeral, listaIndicerecursogeral.qtde, " +
							"listaIndicerecursogeral.outro, unidademedidarg.cdunidademedida, " +
							"listaIndicerecursohumano.cdindicerecursohumano, listaIndicerecursohumano.qtde")
					.leftOuterJoin("indice.listaIndicerecursogeral listaIndicerecursogeral")
					.leftOuterJoin("listaIndicerecursogeral.material material")
					.leftOuterJoin("listaIndicerecursogeral.unidademedida unidademedidarg")
					.leftOuterJoin("indice.listaIndicerecursohumano listaIndicerecursohumano")
					.leftOuterJoin("listaIndicerecursohumano.cargo cargo")
					.leftOuterJoin("indice.unidademedida unidademedida")
					.where("indice = ?", indice)
					.orderBy("material.nome, cargo.nome")
					.unique();
	}
	
	/**
	 * Busca o indice pelo identificador. Para a importa��o das tarefas.
	 *
	 * @param identificador
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Indice findForImportacaoTarefa(String identificador) {
		if(identificador == null || identificador.equals("")){
			throw new SinedException("Identificador n�o pode ser nulo.");			
		}
		return query()
		.select("indice.cdindice, indice.identificador, indice.qtde, unidademedida.cdunidademedida, unidademedida.nome, material.nome, " +
				"material.cdmaterial, cargo.nome, cargo.cdcargo, material.valorcusto, cargo.custohora,  " +
				"listaIndicerecursogeral.cdindicerecursogeral, listaIndicerecursogeral.qtde, " +
				"listaIndicerecursohumano.cdindicerecursohumano, listaIndicerecursohumano.qtde")
		.leftOuterJoin("indice.listaIndicerecursogeral listaIndicerecursogeral")
		.leftOuterJoin("listaIndicerecursogeral.material material")
		.leftOuterJoin("indice.listaIndicerecursohumano listaIndicerecursohumano")
		.leftOuterJoin("listaIndicerecursohumano.cargo cargo")
		.leftOuterJoin("indice.unidademedida unidademedida")
		.where("upper(indice.identificador) like ?", identificador.toUpperCase())
		.orderBy("material.nome, cargo.nome")
		.unique();
	}

}
