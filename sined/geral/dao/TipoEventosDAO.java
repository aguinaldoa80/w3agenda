package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.EventosCorreios;
import br.com.linkcom.sined.geral.bean.EventosFinaisCorreios;
import br.com.linkcom.sined.geral.bean.TipoEventos;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TipoEventosDAO extends GenericDAO<TipoEventos>{

	public TipoEventos loadEventoFinal(EventosCorreios eventoCorreios){
		return querySined()
				.select("tipoEventos.cdtiposeventos, eventosFinaisCorreios.cdeventosfinaiscorreios, eventosFinaisCorreios.corLetra, eventosFinaisCorreios.corFundo, eventosFinaisCorreios.letra, "+
						"eventosFinaisCorreios.finalizadoSucesso, eventosFinaisCorreios.insucesso, eventosFinaisCorreios.enviaemailinsucesso, eventosFinaisCorreios.email, "+
						"eventosFinaisCorreios.nome, "+
						"tipoEventoCorreios.cdTipoEventoCorreios, tipoEventoCorreios.descricao, "+
						"eventosCorreios.cdeventoscorreios, eventosCorreios.descricao, eventosCorreios.eventoFinal")
				.join("tipoEventos.eventosCorreios eventosCorreios")
				.join("tipoEventos.eventosFinaisCorreios eventosFinaisCorreios")
				.join("tipoEventos.tipoEventoCorreios tipoEventoCorreios")
				.where("eventosCorreios = ?", eventoCorreios)
				.setMaxResults(1)
				.unique();
	}
	
	public TipoEventos loadWithEventosCorreios(TipoEventos tipoEventos){
		return querySined()
				.select("tipoEventos.cdtiposeventos, eventosFinaisCorreios.cdeventosfinaiscorreios, eventosFinaisCorreios.corLetra, eventosFinaisCorreios.corFundo, eventosFinaisCorreios.letra, "+
						"eventosFinaisCorreios.finalizadoSucesso, eventosFinaisCorreios.insucesso, eventosFinaisCorreios.enviaemailinsucesso, eventosFinaisCorreios.email, "+
						"eventosFinaisCorreios.nome, "+
						"tipoEventoCorreios.cdTipoEventoCorreios, tipoEventoCorreios.descricao, "+
						"eventosCorreios.cdeventoscorreios, eventosCorreios.descricao, eventosCorreios.eventoFinal")
				.join("tipoEventos.eventosCorreios eventosCorreios")
				.join("tipoEventos.eventosFinaisCorreios eventosFinaisCorreios")
				.join("tipoEventos.tipoEventoCorreios tipoEventoCorreios")
				.where("tipoEventos = ?", tipoEventos)
				.setMaxResults(1)
				.unique();
	}
	
	public List<TipoEventos> loadByEventosFinaisCorreios(EventosFinaisCorreios eventosFinaisCorreios){
		return querySined()
				.select("tipoEventos.cdtiposeventos, eventosFinaisCorreios.cdeventosfinaiscorreios, eventosFinaisCorreios.corLetra, eventosFinaisCorreios.corFundo, eventosFinaisCorreios.letra, "+
						"eventosFinaisCorreios.finalizadoSucesso, eventosFinaisCorreios.insucesso, eventosFinaisCorreios.enviaemailinsucesso, eventosFinaisCorreios.email, "+
						"eventosFinaisCorreios.nome, "+
						"tipoEventoCorreios.cdTipoEventoCorreios, tipoEventoCorreios.descricao, "+
						"eventosCorreios.cdeventoscorreios, eventosCorreios.descricao, eventosCorreios.eventoFinal, eventosCorreios.codigo, eventosCorreios.detalhe")
				.join("tipoEventos.eventosCorreios eventosCorreios")
				.join("tipoEventos.eventosFinaisCorreios eventosFinaisCorreios")
				.join("tipoEventos.tipoEventoCorreios tipoEventoCorreios")
				.where("eventosFinaisCorreios = ?", eventosFinaisCorreios)
				.list();
	}
}
