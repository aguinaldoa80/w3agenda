package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Projetotipo;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaoprojeto;
import br.com.linkcom.sined.geral.bean.enumeration.TipoRegistroDetalheAcompanhamentoEconomico;
import br.com.linkcom.sined.geral.service.ProjetoarquivoService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.ProjetoFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.AcompanhamentoEconomicoListagemDetalheBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("projeto.nome")
public class ProjetoDAO extends GenericDAO<Projeto> {
	
	protected ProjetoarquivoService projetoarquivoService;
	
	public void setProjetoarquivoService(ProjetoarquivoService projetoarquivoService) {
		this.projetoarquivoService = projetoarquivoService;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Projeto> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		ProjetoFiltro filtro = (ProjetoFiltro) _filtro;
		query
			.select("distinct projeto.cdprojeto, projeto.nome, projeto.dtprojeto, projeto.sigla, cliente.nome, empresa.nome, empresa.razaosocial, empresa.nomefantasia, municipio.nome, " +
					"uf.sigla, colaborador.nome, projeto.situacao")
			.leftOuterJoin("projeto.projetotipo projetotipo")
			.leftOuterJoin("projeto.listaProjetoitem projetoitem")
			.leftOuterJoin("projetoitem.projetotipoitem projetotipoitem")
			.leftOuterJoin("projeto.cliente cliente")
			.leftOuterJoin("projeto.empresa empresa")
			.leftOuterJoin("projeto.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("projeto.colaborador colaborador")
			.whereLikeIgnoreAll("projeto.sigla",filtro.getSigla())
			.where("cliente = ?",filtro.getCliente())
			.where("empresa = ?",filtro.getEmpresa())
			.whereLikeIgnoreAll("projeto.nome", filtro.getNome())
			.where("uf = ?", filtro.getUf())
			.where("municipio = ?", filtro.getMunicipio())
			.where("projetotipo = ?", filtro.getProjetotipo())
			.where("projetotipoitem = ?", filtro.getProjetotipoitem())
			.where("projetoitem.valor = ?", filtro.getValorprojetoitem())
			.whereLikeIgnoreAll("colaborador.nome", filtro.getColaborador())
			.where("projeto.cdprojeto = ?", filtro.getCdprojeto())
			.orderBy("projeto.nome")
			.ignoreJoin("projetoitem", "projetotipoitem");
		
		
		
		if(filtro.getListaSituacaoprojeto() != null && !filtro.getListaSituacaoprojeto().isEmpty())
			query.whereIn("projeto.situacao", Situacaoprojeto.listAndConcatenate(filtro.getListaSituacaoprojeto()));
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Projeto> query) {
		query
			.select("projeto.cdprojeto, projeto.nome, projeto.area, projeto.dtprojeto, projeto.dtfimprojeto, colaborador.cdpessoa, colaborador.nome," +
					"listaProjetoarquivo.cdprojetoarquivo, arquivo.cdarquivo, arquivo.nome, projeto.cdusuarioaltera, projeto.endereco, " +
					"projeto.dtaltera, listaProjetoarquivo.descricao, listaProjetoarquivo.dtarquivo, cliente.cdpessoa, cliente.nome, " +
					"municipio.cdmunicipio, uf.cduf, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, projeto.cnpj, projeto.cei, projeto.art, " +
					"listaProjetohistorico.dthistorico, usuario.cdpessoa, usuario.nome, centrocusto.cdcentrocusto, centrocusto.nome, " +
					"listaProjetohistorico.observacao, listaProjetohistorico.cdprojetohistorico, proposta.cdproposta, proposta.numero, " +
					"listaProjetodespesa.cdprojetodespesa, listaProjetodespesa.mesano, contagerencial.cdcontagerencial, vcontagerencial.identificador, " +
					"contagerencial.nome, vcontagerencial.arvorepai, listaProjetodespesa.valortotal, listaProjetodespesa.valorhora, " +
					"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, projeto.situacao, projetotipo.cdprojetotipo, " +
					"projetoitem.cdprojetoitem, projetoitem.valor, projetotipoitem.cdprojetotipoitem, projetotipoitem.nome, projetotipoitem.obrigatorio, " +
					"modelodiarioobra.cdreporttemplate, oportunidade.cdoportunidade, oportunidade.nome")
			.join("projeto.colaborador colaborador")
			.join("projeto.municipio municipio")
			.join("municipio.uf uf")
			.leftOuterJoin("projeto.modelodiarioobra modelodiarioobra")
			.leftOuterJoin("projeto.projetotipo projetotipo")
			.leftOuterJoin("projeto.listaProjetoitem projetoitem")
			.leftOuterJoin("projetoitem.projetotipoitem projetotipoitem")
			.leftOuterJoin("projeto.listaProjetoarquivo listaProjetoarquivo")
			.leftOuterJoin("projeto.listaProjetohistorico listaProjetohistorico")
			.leftOuterJoin("listaProjetohistorico.usuarioresponsavel usuario")
			.leftOuterJoin("listaProjetoarquivo.arquivo arquivo")
			.leftOuterJoin("projeto.listaProjetodespesa listaProjetodespesa")
			.leftOuterJoin("listaProjetodespesa.contagerencial contagerencial")
			.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			.leftOuterJoin("projeto.cliente cliente")
			.leftOuterJoin("projeto.empresa empresa")
			.leftOuterJoin("projeto.centrocusto centrocusto")
			.leftOuterJoin("projeto.proposta proposta")
			.leftOuterJoin("projeto.localarmazenagem localarmazenagem")
			.leftOuterJoin("projeto.oportunidade oportunidade")
			.orderBy("listaProjetohistorico.dthistorico, projetotipoitem.nome");
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {		
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);

				Projeto projeto = (Projeto) save.getEntity();
				projetoarquivoService.saveArquivos(projeto);
				save.saveOrUpdateManaged("listaProjetoarquivo");
				save.saveOrUpdateManaged("listaProjetohistorico");
				save.saveOrUpdateManaged("listaProjetodespesa");
				save.saveOrUpdateManaged("listaProjetoitem");
				return null;
			}			
		});		
	}	
	
	/**
	 * M�todo que atualiza a situa��o do projeto
	 *
	 * @author Luiz Fernando
	 */
	public void atualizaSituacaoProjeto() {
		getJdbcTemplate().update("UPDATE Projeto SET situacao = " + Situacaoprojeto.CONCLUIDO.getValue() +
				 " WHERE cdprojeto in (SELECT p.cdprojeto FROM Projeto p " +
				 " WHERE p.dtfimprojeto is not null " +
				 " AND (p.situacao IS NULL or (p.situacao <> " + Situacaoprojeto.CANCELADO.getValue() +  
				 " AND p.situacao <> " + Situacaoprojeto.CONCLUIDO.getValue() + " ) ) "+
				 " AND p.dtfimprojeto < now()) ");
		
	}

	/**
	 * Retorna uma lista de projeto contendo apenas o campo Nome
	 * 
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Projeto> findDescricao(String whereIn) {
		return querySined()
			
			.select("projeto.cdprojeto, projeto.nome")
			.whereIn("projeto.cdprojeto", whereIn)
			.list();
	}
	
	/**
	 * Retorna o c�digo e o nome dos projetos para serem utilizados no Flex
	 *  
	 * @return lista de projetos
	 * @author Rodrigo Alvarenga
	 */		
	public List<Projeto> findAllForFlex(){
		return querySined()
			
			.select("projeto.cdprojeto, projeto.nome")
			.orderBy("projeto.nome")
			.list();
	}
	
	/* singleton */
	private static ProjetoDAO instance;
	public static ProjetoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ProjetoDAO.class);
		}
		return instance;
	}

	/**
	 * Carrega o projeto de um planejamento.
	 *
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Projeto findByPlanejamento(Planejamento planejamento) {
		if(planejamento == null || planejamento.getCdplanejamento() == null){
			throw new SinedException("Erro na passagem de parametro.");
		}
		return querySined()
					
					.select("projeto.cdprojeto, projeto.dtprojeto")
					.leftOuterJoin("projeto.listaPlanejamento listaPlanejamento")
					.where("listaPlanejamento = ?", planejamento)
					.unique();
	}
	
	/**
	 * Carrega dados dos arquivos de um projeto
	 * @param projeto
	 * @return
	 * @author Taidson
	 * @since 14/07/2010
	 */
	public Projeto arquivosEHistoricosPorProjeto(Projeto projeto) {
		if(projeto == null || projeto.getCdprojeto() == null){
			throw new SinedException("Erro na passagem de parametro.");
		}
		return querySined()
				
		.select("projeto.cdprojeto, listaProjetoarquivo.cdprojetoarquivo, listaProjetoarquivo.descricao, " +
				"listaProjetohistorico.cdprojetohistorico, listaProjetohistorico.dthistorico, " +
				"listaProjetohistorico.observacao, usuario.cdpessoa, usuario.nome")
		.leftOuterJoin("projeto.listaProjetoarquivo listaProjetoarquivo")
		.leftOuterJoin("projeto.listaProjetohistorico listaProjetohistorico")
		.leftOuterJoin("listaProjetohistorico.usuarioresponsavel usuario")
		.where("projeto = ?", projeto)
		.unique();
	}

	/**
	 * Carrega informa��es para o combo de projeto no filtro.
	 *
	 * @param projetoAberto
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Projeto> findCombo(Boolean projetoAberto) {
		if(projetoAberto == null){
			return findAll();
		} else if(projetoAberto){
			return querySined()
					
						.select("projeto.cdprojeto, projeto.nome")
						.openParentheses()
						.where("projeto.dtfimprojeto is null")
						.or()
						.where("projeto.dtfimprojeto > ?", SinedDateUtils.currentDate())
						.closeParentheses()
						.orderBy("projeto.nome")
						.list();
		} else {
			return querySined()
				
				.select("projeto.cdprojeto, projeto.nome")
				.where("projeto.dtfimprojeto is not null")
				.where("projeto.dtfimprojeto <= ?", SinedDateUtils.currentDate())
				.orderBy("projeto.nome")
				.list();
		}
	}

	/**
	 * M�todo que retorna os projetos em aberto + os projetos que possam estar finalizados
	 * 
	 * @param projetos
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Projeto> findProjetosAbertos(List<Projeto> projetos, boolean retirarCancelados, Boolean permissaoProjeto) {
		QueryBuilder<Projeto> query = querySined()
			
			.select("projeto.cdprojeto, projeto.nome")
			.openParentheses();
		
		if(projetos != null && projetos.size() > 0){
			query.whereIn("projeto", CollectionsUtil.listAndConcatenate(projetos, "cdprojeto",",")).or();
		}
		
		query.openParentheses();
		if(permissaoProjeto != null && permissaoProjeto){
			String whereInProjetosUsuario =  SinedUtil.getListaProjeto();
			if(whereInProjetosUsuario != null && !"".equals(whereInProjetosUsuario)){
				query.whereIn("projeto", whereInProjetosUsuario);
			}
		}
		query
			.openParentheses()
				.where("projeto.dtfimprojeto is null")
				.or()
				.where("projeto.dtfimprojeto > ?", SinedDateUtils.currentDate())
			.closeParentheses()
		.closeParentheses()
		.orderBy("projeto.nome");
		
		
		
		if (retirarCancelados){
			query.where("projeto.situacao not in (?)", new Object[]{Situacaoprojeto.CANCELADO});
		}
		
		return query.closeParentheses().list();
	}
	
	/**
	 * M�todo que retorna os projetos em aberto + os projetos que possam estar finalizados
	 * 
	 * @param projetos
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Projeto> findProjetosAbertosSemPermissao(List<Projeto> projetos, boolean retirarCancelados, Boolean permissaoProjeto) {
		QueryBuilder<Projeto> query = querySined()
			
			.setUseWhereProjeto(false)
			.select("projeto.cdprojeto, projeto.nome")
			.openParentheses();
		
		if(projetos != null && projetos.size() > 0){
			query.whereIn("projeto", CollectionsUtil.listAndConcatenate(projetos, "cdprojeto",",")).or();
		}
		
		query.openParentheses();
		if(permissaoProjeto != null && permissaoProjeto){
			String whereInProjetosUsuario =  SinedUtil.getListaProjeto();
			if(whereInProjetosUsuario != null && !"".equals(whereInProjetosUsuario)){
				query.whereIn("projeto", whereInProjetosUsuario);
			}
		}
		
		query
			.openParentheses()
				.where("projeto.dtfimprojeto is null")
				.or()
				.where("projeto.dtfimprojeto > ?", SinedDateUtils.currentDate())
			.closeParentheses()
		.closeParentheses()
		.orderBy("projeto.nome");
		
		if (retirarCancelados){
			query.where("projeto.situacao not in (?)", new Object[]{Situacaoprojeto.CANCELADO});
		}
		
		return query.closeParentheses().list();
	}

	/**
	 * Busca os projetos cadastrados no rateio do contrato.
	 *
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Projeto> findByContrato(Contrato contrato) {
		QueryBuilder<Projeto> query = querySined()
					
					.select("distinct projeto.cdprojeto, projeto.nome")
					.orderBy("projeto.nome");
		
		if(contrato != null && contrato.getCdcontrato() != null){
			query
				.leftOuterJoin("projeto.listaRateioitem rateioitem", true)
				.leftOuterJoin("rateioitem.rateio rateio", true)
				.leftOuterJoin("rateio.listaContrato contrato", true)
				.where("contrato = ?", contrato);
		}
		return query.list();
	}
	
	/**
	 * M�todo que busca os projetos da tarefa ou do rateio do contrato
	 *
	 * @param contrato
	 * @param cdtarefa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Projeto> findByContrato(Contrato contrato, String cdstarefa, Integer cdrequisicao) {
		QueryBuilder<Projeto> query = querySined()
				
				.select("distinct projeto.cdprojeto, projeto.nome, cliente.cdpessoa, cliente.nome")
				.leftOuterJoin("projeto.cliente cliente")
				.openParentheses()
					.where("projeto.cdprojeto = (select r.projeto.cdprojeto from Requisicao r where r.cdrequisicao = ?)", cdrequisicao)
					.or()
					.openParentheses()
						.where("projeto.situacao = ?", Situacaoprojeto.EM_ESPERA)
						.or()
						.where("projeto.situacao = ?", Situacaoprojeto.EM_ANDAMENTO)
					.closeParentheses()
				.closeParentheses()
				.orderBy("projeto.nome");
		
		if(cdstarefa != null && !cdstarefa.trim().equals("")){
			query
				.join("projeto.listaPlanejamento listaPlanejamento")
				.join("listaPlanejamento.listaTarefa listaTarefa")				
				.whereIn("listaTarefa.cdtarefa", cdstarefa);
		} else if(contrato != null && contrato.getCdcontrato() != null){
			query
				.leftOuterJoin("projeto.listaRateioitem rateioitem")
				.leftOuterJoin("rateioitem.rateio rateio")
				.leftOuterJoin("rateio.listaContrato contrato")
				.where("contrato = ?", contrato);
		}
		
		return query.list();
	}

	public Projeto loadForEmail(Projeto projeto) {
		return querySined()
					
					.select("projeto.cdprojeto, colaborador.email")
					.join("projeto.colaborador colaborador")
					.where("projeto = ?", projeto)
					.unique();
	}

	public Projeto loadForDiarioObra(Projeto projeto) {
		if(projeto == null || projeto.getCdprojeto() == null){
			throw new SinedException("O projeto n�o pode ser nulo.");
		}
		return querySined()
					
					.select("projeto.cdprojeto, projeto.nome, municipio.nome, uf.sigla, logocliente.cdarquivo, " +
							"modelodiarioobra.cdreporttemplate")
					.leftOuterJoin("projeto.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("projeto.cliente cliente")
					.leftOuterJoin("cliente.logocliente logocliente")
					.leftOuterJoin("projeto.modelodiarioobra modelodiarioobra")
					.where("projeto = ?", projeto)
					.unique();
	}

	/**
	 * Carrega um projeto para o relat�rio de acompanhamento econ�mico.
	 * 
	 * @param projeto
	 * @return
	 * 
	 * @author Giovane Freitas
	 */
	public Projeto loadForAcompanhamentoEconomico(Projeto projeto) {
		if(projeto == null || projeto.getCdprojeto() == null){
			throw new SinedException("O projeto n�o pode ser nulo.");
		}
		
		return querySined()
					
					.select("projeto.cdprojeto, projeto.nome, projeto.dtaltera, projeto.dtprojeto, projeto.dtfimprojeto, " +
							"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla, logocliente.cdarquivo, cliente.cdpessoa, " +
							"cliente.nome, proposta.cdproposta, rateioitem.cdrateioitem, " +
							"rateioitem.valor")
					.leftOuterJoin("projeto.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("projeto.cliente cliente")
					.leftOuterJoin("cliente.logocliente logocliente")
					.leftOuterJoin("projeto.proposta proposta")
					.leftOuterJoin("projeto.listaRateioitem rateioitem")
					.where("projeto = ?", projeto)
					.unique();
	}

	/**
	 * Busca o projeto pela sigla.
	 *
	 * @param sigla
	 * @return
	 * @since Jul 5, 2011
	 * @author Rodrigo Freitas
	 */
	public Projeto findBySigla(String sigla) {
		if(sigla == null || sigla.equals("")){
			throw new SinedException("Sigla n�o pode ser nula.");
		}
		List<Projeto> lista = querySined()
									
									.select("projeto.cdprojeto, projeto.nome, centrocusto.cdcentrocusto")
									.leftOuterJoin("projeto.centrocusto centrocusto")
									.where("projeto.sigla like ?", sigla)
									.list();
		if(lista == null || lista.size() == 0){
			return null;
		} else {
			return lista.get(0);
		}
	}

	/**
	* M�todo que busca o local de armzazenagem e o centro de custo do projeto
	*
	* @param projeto
	* @return
	* @since Sep 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public Projeto carregaLocalarmazenagemCentroCusto(Projeto projeto) {
		if(projeto == null || projeto.getCdprojeto() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		return querySined()
				
				.select("projeto.cdprojeto, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, centrocusto.cdcentrocusto, " +
						"centrocusto.nome")
				.leftOuterJoin("projeto.localarmazenagem localarmazenagem")
				.leftOuterJoin("projeto.centrocusto centrocusto")
				.where("projeto = ?", projeto)
				.unique();
	}

	/**
	 * Busca a listagem do detalhe das despesas de um projeto de uma certa contagerencial at� a dtFim.
	 *
	 * @param projeto
	 * @param dtfim
	 * @param contagerencial
	 * @return
	 * @since 13/10/2011
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<AcompanhamentoEconomicoListagemDetalheBean> buscaDetalheAcompanhamentoEconomico(Projeto projeto, Date dtfim, Contagerencial contagerencial) {
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("select tipo, id, descricao, data, valor ");
		sql.append("from ( ");
		
		sql.append("select 0 as tipo, f.cdfornecimentocontrato as id, f.descricao as descricao, f.dtinicio as data, f.valor as valor ");

		sql.append("from Fornecimentocontrato f ");
		sql.append("left outer join Contagerencial c on f.cdcontagerencial = c.cdcontagerencial ");
		sql.append("left outer join Vcontagerencial v on c.cdcontagerencial = v.cdcontagerencial ");

		sql.append("where (v.identificador like '").append(contagerencial.getVcontagerencial().getIdentificador()).append("%' ");
		sql.append("or f.cdcontagerencial = ").append(contagerencial.getCdcontagerencial()).append(")  ");
		sql.append("and f.cdprojeto = ").append(projeto.getCdprojeto()).append(" ");
		sql.append("and f.dtinicio <= '").append(SinedDateUtils.toStringPostgre(dtfim)).append("' ");

		sql.append("union all ");

		sql.append("select 1 as tipo, o.cdordemcompra as id, 'OC ' || o.cdordemcompra as descricao, o.dtautorizacao as data, sum(ri.valor) as valor ");

		sql.append("from Ordemcompra o ");

		sql.append("left outer join Rateio r on o.cdrateio = r.cdrateio ");
		sql.append("left outer join Rateioitem ri on r.cdrateio = ri.cdrateio ");
		sql.append("left outer join Contagerencial c on ri.cdcontagerencial = c.cdcontagerencial ");
		sql.append("left outer join Vcontagerencial v on c.cdcontagerencial = v.cdcontagerencial ");

		sql.append("where (v.identificador like '").append(contagerencial.getVcontagerencial().getIdentificador()).append("%' ");
		sql.append("or ri.cdcontagerencial = ").append(contagerencial.getCdcontagerencial()).append(")  ");
		sql.append("and ri.cdprojeto = ").append(projeto.getCdprojeto()).append(" ");
		sql.append("and o.dtcancelamento is null  ");
		sql.append("and o.dtautorizacao is not null  ");
		sql.append("and o.dtautorizacao <= '").append(SinedDateUtils.toStringPostgre(dtfim)).append("' ");

		sql.append("group by o.cdordemcompra, o.dtautorizacao ");

		sql.append("union all ");

		sql.append("select 2 as tipo, d.cddocumento as id, d.descricao as descricao, d.dtvencimento as data, sum(ri.valor) as valor ");

		sql.append("from Documento d ");
		     
		sql.append("left outer join Documentoacao da on d.cddocumentoacao = da.cddocumentoacao ");
		sql.append("left outer join Documentoorigem doc on d.cddocumento = doc.cddocumento ");
		sql.append("left outer join Rateio r on d.cdrateio = r.cdrateio ");
		sql.append("left outer join Rateioitem ri on r.cdrateio = ri.cdrateio ");
		sql.append("left outer join Contagerencial c on ri.cdcontagerencial = c.cdcontagerencial ");
		sql.append("left outer join Vcontagerencial v on c.cdcontagerencial = v.cdcontagerencial ");

		sql.append("where (v.identificador like '").append(contagerencial.getVcontagerencial().getIdentificador()).append("%'  ");
		sql.append("or ri.cdcontagerencial = ").append(contagerencial.getCdcontagerencial()).append(")  ");
		sql.append("and ri.cdprojeto = ").append(projeto.getCdprojeto()).append(" ");
		sql.append("and d.cddocumentoclasse = 1  ");
		sql.append("and doc.cdentrega is null  ");
		sql.append("and da.cddocumentoacao in (1, 2, 3, 4, 6)  ");
		sql.append("and d.dtvencimento <= '").append(SinedDateUtils.toStringPostgre(dtfim)).append("' ");

		sql.append("group by d.cddocumento, d.descricao, d.dtvencimento ");
		
		sql.append(") QUERY ");
		sql.append("order by data desc, tipo ");

		SinedUtil.markAsReader();
		List<AcompanhamentoEconomicoListagemDetalheBean> lista = getJdbcTemplate().query(sql.toString(), 
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						return new AcompanhamentoEconomicoListagemDetalheBean(
								TipoRegistroDetalheAcompanhamentoEconomico.values()[rs.getInt("tipo")], 
								rs.getInt("id"),
								rs.getString("descricao"), 
								rs.getDate("data"), 
								new Money(rs.getInt("valor"), true));
					}
				});
		
		return lista;
	}

	/**
	 * Busca todos os registros para a exporta��o de arquivos gerenciais.
	 * @author Giovane Freitas <giovane.freitas@linkcom.com.br>
	 */
	public Iterator<Projeto> findForArquivoGerencial() {
		return query()
			.iterate();
	}
	
	/**
	 * M�todo que busca os projetos que est�o relacionados com a tarefa
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Projeto> findForGerarOrdemServico(String whereIn) {
		if(whereIn == null || "".equals(whereIn)) throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				
				.select("projeto.cdprojeto, projeto.nome, cliente.cdpessoa, cliente.nome")
				.join("projeto.listaPlanejamento listaPlanejamento")
				.leftOuterJoin("projeto.cliente cliente")
				.join("listaPlanejamento.listaTarefa listaTarefa")				
				.whereIn("listaTarefa.cdtarefa", whereIn)				
				.list();
		
	}
	
	/**
	 * 
	 * M�todo que busca a lista de Projetos para a gera��o do CSV.
	 *
	 * @name findListaProjetoForCSV
	 * @param _filtro
	 * @return
	 * @return List<Projeto>
	 * @author Thiago Augusto
	 * @date 28/06/2012
	 *
	 */
	public List<Projeto> findListaProjetoForCSV(ProjetoFiltro filtro){
		QueryBuilder<Projeto> query = querySined();
		updateListagemQuery(query, filtro);
		query.setIgnoreJoinPaths(new HashSet<String>());
		
		query
			.leftOuterJoinIfNotExists("projeto.localarmazenagem localarmazenagem")
			.leftOuterJoinIfNotExists("projeto.centrocusto centrocusto")
			.leftOuterJoinIfNotExists("projeto.proposta proposta");
		
		return query
				.select("projeto.nome, projeto.cnpj, projeto.cei, projeto.dtprojeto, projeto.dtfimprojeto, projeto.sigla, " +
						"localarmazenagem.nome, municipio.nome, uf.sigla, colaborador.nome, cliente.nome, empresa.nome, empresa.razaosocial, empresa.nomefantasia, centrocusto.nome, proposta.numero, proposta.sufixo")
				.orderBy("projeto.nome")
				.list();
	}
	
	public List<Projeto> findForAutocompleteByUsuariologado(String q) {
		Usuario usuario = (Usuario)NeoWeb.getUser();
		
		QueryBuilder<Projeto> query = querySined()
				;
		query
			.select("projeto.cdprojeto, projeto.nome")
			.whereLikeIgnoreAll("projeto.nome", q)
			.orderBy("projeto.nome");
		
		if(!usuario.getTodosprojetos()){
			query
				.join("projeto.listaUsuarioprojeto usuarioprojeto")
				.join("usuarioprojeto.usuario usuario")
				.where("usuario.cdpessoa = ?", usuario.getCdpessoa());
		}
		
		return query.list();
	}
	
	/**
	 * M�todo que busca os projetos n�o cancelados para o autocomplete
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Projeto> findForComboByUsuariologadoNotCancelado(String q) {
		Usuario usuario = (Usuario)NeoWeb.getUser();
		
		QueryBuilder<Projeto> query = querySined()
				;
		query
			.select("projeto.cdprojeto, projeto.nome")
			.whereLikeIgnoreAll("projeto.nome", q)
			.where("projeto.situacao <> ?", Situacaoprojeto.CANCELADO)
			.orderBy("projeto.nome");
		
		if(!usuario.getTodosprojetos()){
			query
				.join("projeto.listaUsuarioprojeto usuarioprojeto")
				.join("usuarioprojeto.usuario usuario")
				.where("usuario.cdpessoa = ?", usuario.getCdpessoa());
		}
		
		return query.list();
	}
	
	/**
	 * M�todo que busca os projetos para o filtro de acordo com a restri��o do usu�rio
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Projeto> findForComboByUsuariologado() {
		Usuario usuario = (Usuario)NeoWeb.getUser();
		
		QueryBuilder<Projeto> query = querySined()
				;
		query
			.select("projeto.cdprojeto, projeto.nome")
			.orderBy("projeto.nome");
		
		if(!usuario.getTodosprojetos()){
			query
				.join("projeto.listaUsuarioprojeto usuarioprojeto")
				.join("usuarioprojeto.usuario usuario")
				.where("usuario.cdpessoa = ?", usuario.getCdpessoa());
		}
		
		return query.list();
	}
	
	/**
	 * M�todo que busca os projetos, diferente de cancelado, do usu�rio 
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Projeto> findForComboByUsuariologadoNotCancelado() {
		Usuario usuario = (Usuario)NeoWeb.getUser();
		
		QueryBuilder<Projeto> query = querySined()
				;
		query
			.select("projeto.cdprojeto, projeto.nome")
			.where("projeto.situacao <> ?", Situacaoprojeto.CANCELADO)
			.orderBy("projeto.nome");
		
		if(!usuario.getTodosprojetos()){
			query
				.join("projeto.listaUsuarioprojeto usuarioprojeto")
				.join("usuarioprojeto.usuario usuario")
				.where("usuario.cdpessoa = ?", usuario.getCdpessoa());
		}
		
		return query.list();
	}
	
	/**
	 * M�todo que busca os projetos de acordo com os par�metros e permiss�o de usu�rio
	 *
	 * @param projetos
	 * @param retirarCancelados
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Projeto> findForComboByUsuariologado(List<Projeto> projetos, boolean retirarCancelados) {
		Usuario usuario = (Usuario)NeoWeb.getUser();
		
		QueryBuilder<Projeto> query = querySined()
				;
		query
			.select("projeto.cdprojeto, projeto.nome")
			.orderBy("projeto.nome");
		
		if(!usuario.getTodosprojetos()){
			query
			.join("projeto.listaUsuarioprojeto usuarioprojeto")
			.join("usuarioprojeto.usuario usuario");
		}
		
		if(projetos != null && projetos.size() > 0){
			query.whereIn("projeto", CollectionsUtil.listAndConcatenate(projetos, "cdprojeto",",")).or();
		}
		
		query.openParentheses();
		
		if(!usuario.getTodosprojetos()){
			query.where("usuario.cdpessoa = ?", usuario.getCdpessoa());
		}
		
		if(retirarCancelados)
			query.where("projeto.situacao <> ?", Situacaoprojeto.CANCELADO);
		
		return query.closeParentheses().list();
	}
	
	public List<Projeto> findForSolicitacaoCompra(List<Projeto> projetos, boolean retirarFinalizados) {
		Usuario usuario = (Usuario)NeoWeb.getUser();
		
		QueryBuilder<Projeto> query = querySined()
				;
		query
			.select("projeto.cdprojeto, projeto.nome")
			.orderBy("projeto.nome");
		
		if(!usuario.getTodosprojetos()){
			query
			.join("projeto.listaUsuarioprojeto usuarioprojeto")
			.join("usuarioprojeto.usuario usuario");
		}
		
		if(projetos != null && projetos.size() > 0){
			query.whereIn("projeto", CollectionsUtil.listAndConcatenate(projetos, "cdprojeto",",")).or();
		}
		
		query.openParentheses();
		
		if(!usuario.getTodosprojetos()){
			query.where("usuario.cdpessoa = ?", usuario.getCdpessoa());
		}
		
		if(retirarFinalizados) {
			query.where("projeto.situacao <> ?", Situacaoprojeto.CANCELADO);
			query.where("projeto.situacao <> ?", Situacaoprojeto.CONCLUIDO);
		}
		
		return query.closeParentheses().list();
	}

	/**
	 * Busca os dados para o cache da tela de pedido de venda offline
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Projeto> findForPVOffline() {
		return query()
				.select("projeto.cdprojeto, projeto.nome, usuarioprojeto.cdusuarioprojeto, usuario.cdpessoa")
				.leftOuterJoin("projeto.listaUsuarioprojeto usuarioprojeto")
				.leftOuterJoin("usuarioprojeto.usuario usuario")
				.list();
	}
	
	public List<Projeto> findForAndroid(String whereIn) {
		return query()
				.select("projeto.cdprojeto, projeto.nome, projeto.situacao, usuarioprojeto.cdusuarioprojeto, usuario.cdpessoa")
				.leftOuterJoin("projeto.listaUsuarioprojeto usuarioprojeto")
				.leftOuterJoin("usuarioprojeto.usuario usuario")
				.whereIn("projeto.cdprojeto", whereIn)
				.list();
	}

	/**
	 * Retorna os c�digo e o nome dos projetos n�o cancelados
	 *  
	 * @return lista de projetos
	 * @author Lucas Costa
	 */		
	public List<Projeto> findNaoCancelados(){
		return querySined()
			
			.select("projeto.cdprojeto, projeto.nome")
			.where("projeto.situacao <> ?", Situacaoprojeto.CANCELADO)
			.orderBy("projeto.nome")
			.list();
	}
	
	public List<Projeto> findByCentrocusto(Centrocusto centrocusto){
		QueryBuilder<Projeto> query = querySined()
				
				.select("projeto.cdprojeto, projeto.nome")
				.openParentheses()
					.where("projeto.dtfimprojeto is null")
					.or()
					.where("projeto.dtfimprojeto > ?", SinedDateUtils.currentDate())
				.closeParentheses()
				.where("projeto.situacao not in (?)", new Object[]{Situacaoprojeto.CANCELADO})
				.orderBy("projeto.nome");
		
		if(centrocusto != null){
			query
				.openParentheses()
				.where("projeto.centrocusto=?", centrocusto)
				.or()
				.where("projeto.centrocusto is null")
				.closeParentheses();
		}
		
		return query.list();
	}
	
	public Projeto loadForRequisicao(Projeto projeto) {
		return querySined()
				
					.select("projeto.cdprojeto, colaborador.cdcolaborador")
					.join("projeto.colaborador colaborador")
					.where("projeto = ?", projeto)
					.unique();
	}
	
	/**
	 * M�todo que updata a data de t�rminio do projeto
	 *
	 * @param date
	 * @param whereIn
	 * @return
	 * @author Lucas Costa
	 */
	public void updateDataTerminio(String whereIn, Date date){
		if (whereIn == null || date == null) {
			throw new SinedException("Par�metros inv�lidos");
		}
		
		getHibernateTemplate().bulkUpdate("update Projeto p set p.dtfimprojeto = ?, p.situacao = ? where p.cdprojeto in (" + whereIn + ")", new Object[]{
			date, Situacaoprojeto.CONCLUIDO
		});
	}

	/**
	 * M�tdo que busca o projeto para o combo 
	 * reload com o tipo de projeto e com a 
	 * valida��o aberto ou fechado
	 * @param projetoAberto
	 * @param projetotipo
	 * @return
	 */
	public List<Projeto> findComboProjetoTipo(Boolean projetoAberto, Projetotipo projetotipo) {
		if(projetoAberto == null){
			if(projetotipo == null){
				return findAll();				
			}else{
				return querySined()
						
							.select("projeto.cdprojeto, projeto.nome")
							.where("projeto.projetotipo = ?",projetotipo)
							.orderBy("projeto.nome")
							.list();
			}
		} else if(projetoAberto){
			return querySined()
					
						.select("projeto.cdprojeto, projeto.nome")
						.openParentheses()
						.where("projeto.dtfimprojeto is null")
						.or()
						.where("projeto.dtfimprojeto > ?", SinedDateUtils.currentDate())
						.closeParentheses()
						.where("projeto.projetotipo = ?",projetotipo)
						.orderBy("projeto.nome")
						.list();
		} else {
			return querySined()
					
				.select("projeto.cdprojeto, projeto.nome")
				.where("projeto.dtfimprojeto is not null")
				.where("projeto.dtfimprojeto <= ?", SinedDateUtils.currentDate())
				.where("projeto.projetotipo = ?",projetotipo)
				.orderBy("projeto.nome")
				.list();
		}
	}
	
	/**
	 * M�tdo que busca o projeto para o combo 
	 * reload com o tipo de projeto
	 * @param projetotipo
	 * @return
	 */
	public List<Projeto> findComboProjetoTipo(Projetotipo projetotipo) {
		if(projetotipo == null){
			return findAll();
		}else{
			return querySined()
			
			.select("projeto.cdprojeto, projeto.nome")
			.where("projeto.projetotipo = ?",projetotipo)
			.orderBy("projeto.nome")
			.list();
		}
	}
	
	/**
	* M�todo que busca os projetos considerados ativos (diferente de CANCELADO e CONCLUIDO)
	*
	* @param projetos
	* @param permissaoProjeto
	* @return
	* @since 22/07/2016
	* @author Luiz Fernando
	*/
	public List<Projeto> findProjetosAtivos(List<Projeto> projetos, Boolean permissaoProjeto) {
		QueryBuilder<Projeto> query = querySined()
				
			.select("projeto.cdprojeto, projeto.nome")
			.openParentheses();
		
		if(projetos != null && projetos.size() > 0){
			query.whereIn("projeto", CollectionsUtil.listAndConcatenate(projetos, "cdprojeto",",")).or();
		}
		
		query.openParentheses();
		if(permissaoProjeto != null && permissaoProjeto){
			String whereInProjetosUsuario =  SinedUtil.getListaProjeto();
			if(whereInProjetosUsuario != null && !"".equals(whereInProjetosUsuario)){
				query.whereIn("projeto", whereInProjetosUsuario);
			}
		}
		
		query.where("projeto.situacao <> ?", Situacaoprojeto.CANCELADO);
		query.where("projeto.situacao <> ?", Situacaoprojeto.CONCLUIDO);
		query.closeParentheses();
		
		
		return query.closeParentheses().orderBy("projeto.nome").list();
	}
	
	public List<Projeto> findByCdsprojetotipo(String cdsprojetotipo) {
		return querySined()
				
				.select("projeto.cdprojeto, projeto.nome")
				.join("projeto.projetotipo projetotipo")
				.whereIn("projetotipo.cdprojetotipo", cdsprojetotipo)
				.orderBy("projeto.nome")
				.list();
				
	}

	public List<Projeto> buscarParaCusteio(Date dtInicio, Date dtFim) {
		return querySined()
				
				.select("projeto.cdprojeto,projeto.nome")
					.openParentheses()
						.where("projeto.dtprojeto is null")
						.or()
						.where("projeto.dtprojeto <= ? ",dtFim )
					.closeParentheses()
					.openParentheses()
						.where("projeto.dtfimprojeto is null")
						.or()
						.where("projeto.dtfimprojeto >= ? ",dtInicio )
					.closeParentheses()
					.where("projeto.situacao <> ?", Situacaoprojeto.CANCELADO)
					.where("projeto.situacao <> ?", Situacaoprojeto.CONCLUIDO)
				.list();
	}

	public Projeto buscarParaProcessarCusteio(Integer id) {
		return querySined()
				
				.select("projeto.cdprojeto, projeto.nome")
				.where("projeto.cdprojeto = ?",id)
				.unique();
	}
}



