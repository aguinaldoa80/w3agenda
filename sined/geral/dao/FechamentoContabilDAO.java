package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.FechamentoContabil;
import br.com.linkcom.sined.modulo.contabil.controller.crud.filter.FechamentoContabilFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FechamentoContabilDAO extends GenericDAO<FechamentoContabil> {
	@Override
	public void updateListagemQuery(QueryBuilder<FechamentoContabil> query, FiltroListagem _filtro) {
		FechamentoContabilFiltro filtro = (FechamentoContabilFiltro) _filtro;
		
		query.select("fechamentoContabil.cdFechamentoContabil, fechamentoContabil.dtfechamento, fechamentoContabil.motivo, " +
					 "empresa.cdpessoa, empresa.nomefantasia")
			 .whereLikeIgnoreAll("fechamentoContabil.motivo", filtro.getMotivo());
		
		if(filtro.getDataInicio() != null || filtro.getDataFim() != null) {
			query.where("fechamentoContabil.dtfechamento >= ?", filtro.getDataInicio())
				 .where("fechamentoContabil.dtfechamento <= ?", filtro.getDataFim());
		}
		
		if(filtro.getEmpresa() != null) {
			query.where("empresa = ?", filtro.getEmpresa());
		} else {
			query.whereIn("empresa", new SinedUtil().getListaEmpresa());
		}
		
		query.leftOuterJoin("fechamentoContabil.empresa empresa");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<FechamentoContabil> query) {
		query
			.select("fechamentoContabil.cdFechamentoContabil, fechamentoContabil.dtfechamento, fechamentoContabil.motivo, " +
					"fechamentoContabil.historico, fechamentoContabil.cdusuarioaltera, fechamentoContabil.dtaltera, " +
					"empresa.cdpessoa, empresa.nomefantasia, " +
					"listaFechamentoContabilHistorico.cdFechamentoContabilHistorico, listaFechamentoContabilHistorico.dtaltera, " +
					"listaFechamentoContabilHistorico.acao, listaFechamentoContabilHistorico.observacao, " +
					"usuarioAltera.cdpessoa, usuarioAltera.nome")
			.leftOuterJoin("fechamentoContabil.empresa empresa")
			.leftOuterJoin("fechamentoContabil.listaFechamentoContabilHistorico listaFechamentoContabilHistorico")
			.leftOuterJoin("listaFechamentoContabilHistorico.usuarioAltera usuarioAltera")
			.orderBy("listaFechamentoContabilHistorico.dtaltera desc");
	}
	
	public boolean existeFechamentoCadastradoMesmosDados(FechamentoContabil bean) {
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(FechamentoContabil.class)
			.where("fechamentoContabil.dtfechamento = ?", bean.getDtfechamento())
			.where("fechamentoContabil.empresa = ?", bean.getEmpresa())
			.where("fechamentoContabil.cdFechamentoContabil not in (?)", bean.getCdFechamentoContabil(), bean.getCdFechamentoContabil() != null)
			.unique() > 0;
	}
	
	public Date carregarUltimaDataFechamento(Empresa empresa) {
		return newQueryBuilderSined(Date.class)
				.select("max(dtfechamento)")
				.from(FechamentoContabil.class)
				.where("fechamentoContabil.empresa = ?", empresa)
				.unique();
	}
	
	@SuppressWarnings("unchecked")
	public List<FechamentoContabil> carregarUltimasDatasFechamentoPorEmpresa(String whereInEmpresa) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT f.cdempresa AS empresa, max(dtfechamento) AS dtfechamento ")
		   .append("FROM fechamentocontabil AS f ")
		   .append("WHERE f.cdempresa in (" + whereInEmpresa + ") ")
		   .append("GROUP BY cdempresa");

		SinedUtil.markAsReader();
		List<FechamentoContabil> listaFechamentoContabil = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				FechamentoContabil fechamentoContabil = new FechamentoContabil();
				
				fechamentoContabil.setEmpresa(new Empresa(rs.getInt("empresa")));
				fechamentoContabil.setDtfechamento(rs.getDate("dtfechamento"));
				
				return fechamentoContabil;
			}
		});
		
		return listaFechamentoContabil;
	}
	
	public List<FechamentoContabil> carregarFechamentosContabeisWhereIn(String whereIn) {
		return query()
				.select("fechamentoContabil.cdFechamentoContabil, fechamentoContabil.dtfechamento")
				.whereIn("fechamentoContabil.cdFechamentoContabil", whereIn)
				.list();
	}
}
