package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemhistorico;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoordemhistoricoDAO extends GenericDAO<Producaoordemhistorico> {

	/**
	 * Busca a lista de hist�rico de acordo com a Producaoordem.
	 *
	 * @param producaoordem
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Producaoordemhistorico> findByProducaoordem(Producaoordem producaoordem) {
		if(producaoordem == null || producaoordem.getCdproducaoordem() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("producaoordemhistorico.cdproducaoordemhistorico, producaoordemhistorico.dtaltera, " +
							"producaoordemhistorico.cdusuarioaltera, producaoordemhistorico.observacao")
					.join("producaoordemhistorico.producaoordem producaoordem")
					.where("producaoordem = ?", producaoordem)
					.orderBy("producaoordemhistorico.dtaltera desc")
					.list();
	}
	
}
