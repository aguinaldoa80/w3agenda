package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.ConferenciaMaterialExpedicao;
import br.com.linkcom.sined.geral.bean.Expedicaohistorico;
import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ConferenciaMaterialExpedicaoDAO extends GenericDAO<ConferenciaMaterialExpedicao>{

	public List<ConferenciaMaterialExpedicao> findByHistorico(Expedicaohistorico historico){
		return querySined()
				.select("conferenciaMaterialExpedicao.cdConferenciaMaterialExpedicao, conferenciaMaterialExpedicao.qtdeConferida, conferenciaMaterialExpedicao.qtdeVenda, "+
						"material.cdmaterial, material.nome, vendaMaterial.cdvendamaterial, venda.cdvenda, " +
						"unidadeMedida.cdunidademedida, unidadeMedida.nome, unidadeMedida.simbolo")
				.join("conferenciaMaterialExpedicao.expedicaoHistorico expedicaoHistorico")
				.join("conferenciaMaterialExpedicao.material material")
				.leftOuterJoin("conferenciaMaterialExpedicao.unidadeMedida unidadeMedida")
				.leftOuterJoin("conferenciaMaterialExpedicao.vendaMaterial vendaMaterial")
				.leftOuterJoin("vendaMaterial.venda venda")
				.where("expedicaoHistorico = ?", historico)
				.list();
	}

	public ConferenciaMaterialExpedicao loadConferenciaByHistoricoAndItem(Expedicaohistorico expedicaohistorico, Expedicaoitem expedicaoitem) {
		return querySined()
				.select("conferenciaMaterialExpedicao.cdConferenciaMaterialExpedicao, conferenciaMaterialExpedicao.qtdeConferida, conferenciaMaterialExpedicao.qtdeVenda, " +
						"material.cdmaterial, material.nome")
				.join("conferenciaMaterialExpedicao.material material")
				.where("conferenciaMaterialExpedicao.expedicaoHistorico = ?", expedicaohistorico)
				.where("conferenciaMaterialExpedicao.expedicaoitem = ?", expedicaoitem)
				.unique();
	}

}
