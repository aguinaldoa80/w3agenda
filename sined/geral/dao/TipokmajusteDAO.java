package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Tipokmajuste;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("tipokmajuste.descricao")
public class TipokmajusteDAO extends GenericDAO<Tipokmajuste> {

}
