package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.DocumentoApropriacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentoApropriacaoDAO extends GenericDAO<DocumentoApropriacao>{

	public List<DocumentoApropriacao> findByDocumento(Documento documento){
		return query()
				.where("documentoApropriacao.documento = ?", documento)
				.list();
	}
	
}
