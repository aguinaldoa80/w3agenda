package br.com.linkcom.sined.geral.dao;

import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.AUTORIZADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.BAIXADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.CANCELADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.ESTORNAR;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.PEDIDO_ENVIADO;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Fluxocaixa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompraacao;
import br.com.linkcom.sined.geral.bean.Ordemcompraarquivo;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Solicitacaocompraordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Tipopessoaquestionario;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.view.Vanalisecompra;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.geral.service.SolicitacaocompraordemcompramaterialService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.AcompanhamentoEconomicoReportFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.AnaliseCompraBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.enumeration.OrigemOrdemCompra;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.AnalisecompraFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.OrdemcompraFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OrdemcompraDAO extends GenericDAO<Ordemcompra> {

	private ArquivoDAO arquivoDAO;
	private UsuarioService usuarioService;
	private SolicitacaocompraordemcompramaterialService solicitacaocompraordemcompramaterialService;
	private MaterialcategoriaService materialcategoriaService;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setSolicitacaocompraordemcompramaterialService(SolicitacaocompraordemcompramaterialService solicitacaocompraordemcompramaterialService) {
		this.solicitacaocompraordemcompramaterialService = solicitacaocompraordemcompramaterialService;
	}
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {
		this.materialcategoriaService = materialcategoriaService;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Ordemcompra> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		OrdemcompraFiltro filtro = (OrdemcompraFiltro) _filtro;
		
		if(filtro.getWhereInCdcompra() != null && !filtro.getWhereInCdcompra().equals("")){
			while(filtro.getWhereInCdcompra().indexOf(",,") != -1){
				filtro.setWhereInCdcompra(filtro.getWhereInCdcompra().replaceAll(",,", ","));
			}
			
			if(filtro.getWhereInCdcompra().substring(0, 1).equals(",")){
				filtro.setWhereInCdcompra(filtro.getWhereInCdcompra().substring(1, filtro.getWhereInCdcompra().length()));
			}
			
			if(filtro.getWhereInCdcompra().substring(filtro.getWhereInCdcompra().length() - 1, filtro.getWhereInCdcompra().length()).equals(",")){
				filtro.setWhereInCdcompra(filtro.getWhereInCdcompra().substring(0, filtro.getWhereInCdcompra().length()-1));
			}
		}
		Set<String> ignoreJoins = new HashSet<String>();
		
		query
			.select("distinct ordemcompra.cdordemcompra, ordemcompra.dtproximaentrega, ordemcompra.dtaprova, ordemcompra.dtcriacao, ordemcompra.rateioverificado, ordemcompra.dtpedido, " +
					"ordemcompra.frete, ordemcompra.valoricmsst, ordemcompra.desconto, ordemcompra.cdusuarioaltera, fornecedor.nome, fornecedor.cdpessoa, localarmazenagem.nome, " +
					"aux_ordemcompra.situacaosuprimentos, aux_ordemcompra.entregapercentual, prazopagamento.cdprazopagamento, ordemcompra.observacao, ordemcompra.observacoesinternas, ordemcompra.ordemcompraantecipada, " +
					"pessoaquestionario.cdpessoaquestionario, pessoaquestionariofornecedor.cdpessoa, questionario.cdquestionario, questionario.tipopessoaquestionario, " +
					"cotacao.cdcotacao")
			.join("ordemcompra.fornecedor fornecedor")
			.join("ordemcompra.prazopagamento prazopagamento")
			.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
			.leftOuterJoin("ordemcompra.localarmazenagem localarmazenagem")
			.leftOuterJoin("ordemcompra.pessoaquestionario pessoaquestionario")
			.leftOuterJoin("pessoaquestionario.questionario questionario")
			.leftOuterJoin("pessoaquestionario.pessoa pessoaquestionariofornecedor")
			.openParentheses()
				.where("ordemcompra.empresa is null")
				.or()
				.where("ordemcompra.empresa in ( select ue.empresa " +
									" from Usuarioempresa ue " +
									" where ue.usuario = ? ) ", SinedUtil.getUsuarioLogado())
			.closeParentheses()
			.whereIn("ordemcompra.cdordemcompra", SinedUtil.montaWhereInInteger(filtro.getWhereInCdcompra()))
			.where("fornecedor = ?", filtro.getFornecedor())
			.where("localarmazenagem = ?", filtro.getLocalarmazenagem())
			.where("ordemcompra.dtproximaentrega >= ?", filtro.getProximaentregade())
			.where("ordemcompra.dtproximaentrega <= ?", filtro.getProximaentregaate())
			.where("ordemcompra.empresa = ?", filtro.getEmpresa())
			.where("ordemcompra.rateioverificado = ?",filtro.getRateioverificado())
			.where("aux_ordemcompra.valor >= ?", filtro.getValor1())
			.where("aux_ordemcompra.valor <= ?", filtro.getValor2())
			.where("ordemcompra.dtcriacao >= ?", filtro.getDtcriacao1())
			.where("ordemcompra.dtcriacao <= ?", filtro.getDtcriacao2());
		
		
		if(filtro.getMaterial() != null || filtro.getMaterialgrupo() != null || filtro.getMaterialtipo() != null 
				|| StringUtils.isNotBlank(filtro.getObservacao()) || filtro.getProjeto() != null 
				|| (filtro.getMaterialcategoria() != null)){
			
			query
				.join("ordemcompra.listaMaterial listmaterial")
				.join("listmaterial.material material");
				
				if(filtro.getMaterial() != null){
					query.where("material = ?", filtro.getMaterial());
				}
				
				if(filtro.getMaterialgrupo() != null){
					query.where("material.materialgrupo = ?",filtro.getMaterialgrupo());
				}
				
				if(filtro.getMaterialtipo() != null){
					query.where("material.materialtipo = ?",filtro.getMaterialtipo());
				}
				
				if(StringUtils.isNotBlank(filtro.getObservacao())){
					query.whereLikeIgnoreAll("listmaterial.observacao", filtro.getObservacao());
				}
				
				if(filtro.getMaterialcategoria() != null){
					materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
					query
						.leftOuterJoin("material.materialcategoria materialcategoria")
						.leftOuterJoin("materialcategoria.vmaterialcategoria vmaterialcategoria")
						.openParentheses()
							.where("vmaterialcategoria.identificador like '" + filtro.getMaterialcategoria().getIdentificador() + ".%'")
						.or()
							.where("vmaterialcategoria.identificador like ? ", filtro.getMaterialcategoria().getIdentificador())
						.closeParentheses();
				}			
			
			if(filtro.getProjeto()!=null){
				query
				.leftOuterJoin("ordemcompra.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.leftOuterJoin("listaRateioitem.projeto projeto")
				.leftOuterJoin("listmaterial.projeto projetoItem")
				.openParentheses()
					.where("projeto = ?", filtro.getProjeto())
					.or()
					.where("projetoItem = ?", filtro.getProjeto())
				.closeParentheses();
			}
			
			ignoreJoins.add("listmaterial");
			ignoreJoins.add("material");
			ignoreJoins.add("materialcategoria");
			ignoreJoins.add("vmaterialcategoria");
			ignoreJoins.add("projetoItem");
		}
		
		if(filtro.getOrigemOrdemCompra() != null){
			if(OrigemOrdemCompra.COTACAO.equals(filtro.getOrigemOrdemCompra())){
				query
				.join("ordemcompra.cotacao cotacao");
				if(filtro.getIdOrigem() != null){
					query
					.where("cotacao.cdcotacao = ?", filtro.getIdOrigem());
				}
			}else if(OrigemOrdemCompra.SOLICITACAO_COMPRA.equals(filtro.getOrigemOrdemCompra())){
				query
				.join("ordemcompra.listaOrdemcompraorigem listaOrdemcompraorigem")
				.join("listaOrdemcompraorigem.solicitacaocompra solicitacaocompra");
				if(filtro.getIdOrigem() != null){
					query
					.where("solicitacaocompra.cdsolicitacaocompra = ?", filtro.getIdOrigem());
				}
			}else{
				if(filtro.getIdOrigem() != null){
					query
					.leftOuterJoin("ordemcompra.listaOrdemcomprahistorico listaOrdemcomprahistorico")
					.where("listaOrdemcomprahistorico.ordemcompraacao = ?", Ordemcompraacao.CRIADA)
					.where("listaOrdemcomprahistorico.cdusuarioaltera = ?", filtro.getIdOrigem());
				}else{
					query
					.join("ordemcompra.cotacao cotacao")
					.join("ordemcompra.listaOrdemcompraorigem listaOrdemcompraorigem")
					.join("listaOrdemcompraorigem.solicitacaocompra solicitacaocompra");
				}
			}
		}
		query.leftOuterJoinIfNotExists("ordemcompra.cotacao cotacao");
		
		if(filtro.getListasituacoes() != null) {
			query.whereIn("aux_ordemcompra.situacaosuprimentos", Situacaosuprimentos.listAndConcatenate(filtro.getListasituacoes()));
		}

		if(filtro.getUsuario() != null) {
			query.where("ordemcompra.cdordemcompra in (select oc.cdordemcompra " +
							" from Ordemcomprahistorico och " +
							" join och.ordemcompraacao ordemcompraacao " +
							" join och.ordemcompra oc " +
							" where ordemcompraacao.cdordemcompraacao =  " + 
				(filtro.getCriadorordemcompra() != null && filtro.getCriadorordemcompra() ? 
						Ordemcompraacao.CRIADA.getCdordemcompraacao() : Ordemcompraacao.AUTORIZADA.getCdordemcompraacao()) +
							" and och.cdusuarioaltera = "+filtro.getUsuario().getCdpessoa()+")");
		}
		
		if (filtro.getFromentregacriar() != null && filtro.getFromentregacriar()) {
			List<Situacaosuprimentos> list = new ArrayList<Situacaosuprimentos>();
			list.add(Situacaosuprimentos.ENTREGA_PARCIAL);
			list.add(Situacaosuprimentos.PEDIDO_ENVIADO);
			query.whereIn("aux_ordemcompra.situacaosuprimentos", Situacaosuprimentos.listAndConcatenate(list));
		}
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioService.isRestricaoFornecedorColaborador(usuarioLogado)){
			query.where("("+
							"exists("+
								"select 1 from ColaboradorFornecedor cf "+
								"where "+
								"cf.fornecedor =  fornecedor.cdpessoa and "+
								"cf.colaborador = "+usuarioLogado.getCdpessoa()+
							") "+
							"or "+
							"not exists("+
								"select 1 from ColaboradorFornecedor cf "+
								"where "+
								"cf.fornecedor =  fornecedor.cdpessoa"+
							")"+
						")");
		}
		
		query.setIgnoreJoinPaths(ignoreJoins);
		query.orderBy("ordemcompra.cdordemcompra desc");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Ordemcompra> query) {
		query
			.select("ordemcompra.cdordemcompra, ordemcompra.dtautorizacao, ordemcompra.dtcriacao, ordemcompra.dtpedido, ordemcompra.dtcancelamento, ordemcompra.dtproximaentrega, ordemcompra.observacao, ordemcompra.rateioverificado, " +
					"ordemcompra.dtproximaentrega, ordemcompra.desconto, ordemcompra.frete, fornecedor.cdpessoa, fornecedor.nome, listaMaterial.cdordemcompramaterial, listaMaterial.qtdpedida, listaMaterial.valor, " +
					"listaMaterial.icmsiss, listaMaterial.ipi, listaMaterial.icmsissincluso, listaMaterial.ipiincluso, material.cdmaterial, material.nome, material.identificacao, cotacao.cdcotacao, " +
					"listaOrdemcomprahistorico.cdordemcomprahistorico, listaOrdemcomprahistorico.observacao, listaOrdemcomprahistorico.dtaltera, listaOrdemcomprahistorico.cdusuarioaltera, ordemcompraacao.cdordemcompraacao, ordemcompraacao.nome, " +
					"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, empresa.cdpessoa, prazopagamento.cdprazopagamento, contato.cdpessoa, aux_ordemcompra.cdordemcompra, aux_ordemcompra.situacaosuprimentos, " +
					"listaArquivosordemcompra.cdordemcompraarquivo, listaArquivosordemcompra.enviarFornecedor, arquivo.cdarquivo, arquivo.nome, arquivo.tipoconteudo, arquivo.tamanho, rateio.cdrateio, ordemcompra.cdusuarioaltera, ordemcompra.dtaltera, " +
					"ordemcompra.faturamentocliente, ordemcompra.tipofrete, garantia.cdgarantia, frequencia.cdfrequencia, frequencia.nome, listaMaterial.qtdefrequencia, material.servico, " +
					"contagerencialMat.cdcontagerencial, contagerencialMat.nome, vcontagerencialMat.identificador, vcontagerencialMat.arvorepai, documentotipo.cddocumentotipo, " +
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, ordemcompra.valoricmsst, ordemcompra.observacoesinternas, transportador.cdpessoa, transportador.nome, " +
					"listaMaterial.dtentrega, listaMaterial.valoroutrasdespesas, listaMaterial.valorfrete, " +
					"listaOrdemcompraorigem.cdordemcompraorigem, solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.identificador, " +
					"centrocusto.cdcentrocusto, centrocusto.nome, projeto.cdprojeto, projeto.nome, localarmazenagemMat.cdlocalarmazenagem, localarmazenagemMat.nome, listaMaterial.observacao," +
					"ordemcompra.inspecao, listaMaterial.altura, listaMaterial.comprimento, listaMaterial.largura, listaMaterial.pesototal, listaMaterial.qtdvolume, " +
					"materialmestregrade.nome, materialmestregrade.identificacao, ordemcompra.ordemcompraantecipada, " +
					"fornecedoroptriangular.cdpessoa, fornecedoroptriangular.nome, fornecedoroptriangular.cpf, fornecedoroptriangular.cnpj, " +
					"enderecoentrega.cdendereco, enderecoentrega.logradouro, enderecoentrega.numero, enderecoentrega.complemento, enderecoentrega.bairro," +
					"municipio.nome, municipio.cdibge, uf.sigla, uf.nome, enderecoentrega.cep, enderecotipo.cdenderecotipo, enderecoentrega.pontoreferencia," +
					"unidademedidaMaterial.cdunidademedida, pessoaquestionario.cdpessoaquestionario, " +
					"planejamento.cdplanejamento, planejamento.descricao, planejamentorecursogeral.cdplanejamentorecursogeral")
			.join("ordemcompra.fornecedor fornecedor")
			.join("ordemcompra.prazopagamento prazopagamento")
			.join("ordemcompra.listaMaterial listaMaterial")
			.join("listaMaterial.material material")			
			.leftOuterJoin("material.unidademedida unidademedidaMaterial")
			.leftOuterJoin("material.materialmestregrade materialmestregrade")
			.join("listaMaterial.contagerencial contagerencialMat")
			.join("contagerencialMat.vcontagerencial vcontagerencialMat")
			.join("listaMaterial.frequencia frequencia")
			.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
			.leftOuterJoin("listaMaterial.localarmazenagem localarmazenagemMat")
			.leftOuterJoin("listaMaterial.unidademedida unidademedida")
			.leftOuterJoin("ordemcompra.garantia garantia")
			.leftOuterJoin("ordemcompra.rateio rateio")
			.leftOuterJoin("ordemcompra.contato contato")
			.leftOuterJoin("ordemcompra.documentotipo documentotipo")
			.leftOuterJoin("ordemcompra.localarmazenagem localarmazenagem")
			.leftOuterJoin("ordemcompra.empresa empresa")
			.leftOuterJoin("ordemcompra.cotacao cotacao")
			.leftOuterJoin("ordemcompra.transportador transportador")
			.leftOuterJoin("ordemcompra.listaOrdemcomprahistorico listaOrdemcomprahistorico")
			.leftOuterJoin("ordemcompra.listaOrdemcompraorigem listaOrdemcompraorigem")
			.leftOuterJoin("listaOrdemcompraorigem.solicitacaocompra solicitacaocompra")
			.leftOuterJoin("listaOrdemcompraorigem.planejamento planejamento")
			.leftOuterJoin("listaOrdemcomprahistorico.ordemcompraacao ordemcompraacao")
			.leftOuterJoin("ordemcompra.listaArquivosordemcompra listaArquivosordemcompra")
			.leftOuterJoin("listaArquivosordemcompra.arquivo arquivo")
			.leftOuterJoin("listaMaterial.centrocusto centrocusto")
			.leftOuterJoin("listaMaterial.projeto projeto")
			.leftOuterJoin("listaMaterial.planejamentorecursogeral planejamentorecursogeral")
			.leftOuterJoin("ordemcompra.fornecedoroptriangular fornecedoroptriangular")
			.leftOuterJoin("ordemcompra.enderecoentrega enderecoentrega")
			.leftOuterJoin("enderecoentrega.enderecotipo enderecotipo")
			.leftOuterJoin("enderecoentrega.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("ordemcompra.pessoaquestionario pessoaquestionario")		
			.orderBy("listaMaterial.cdordemcompramaterial, listaOrdemcomprahistorico.dtaltera desc");
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		save.useTransaction(false);
		
		Ordemcompra ordemcompra = (Ordemcompra) save.getEntity();
		
		if(ordemcompra.getListaMaterial() != null){
			for (Ordemcompramaterial ocm : ordemcompra.getListaMaterial()) {
				if(ocm.getCdordemcompramaterial() != null){
					Set<Solicitacaocompraordemcompramaterial> listaSCOCM = ocm.getListaSolicitacaocompraordemcompramaterial();
					if(SinedUtil.isListNotEmpty(listaSCOCM)){
						for (Solicitacaocompraordemcompramaterial scocm : listaSCOCM) {
							scocm.setOrdemcompramaterial(ocm);
							solicitacaocompraordemcompramaterialService.saveOrUpdateNoUseTransaction(scocm);
						}
					}
				}
			}
		}
		
		if(ordemcompra.getListaArquivosordemcompra() != null && ordemcompra.getListaArquivosordemcompra().size() > 0){
			for (Ordemcompraarquivo ordemcompraarquivo : ordemcompra.getListaArquivosordemcompra()) {
				arquivoDAO.saveFile(ordemcompraarquivo, "arquivo");
			}
		}
		save.saveOrUpdateManaged("listaArquivosordemcompra");
		save.saveOrUpdateManaged("listaMaterial");
		
	}

	/**
	 * Retorna todas as ordens de compra onde for igual aos PK's 
	 * concatenadas na String whereIn
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Ordemcompra> findOrdens(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		return query()
			.select("ordemcompra.cdordemcompra, ordemcompra.dtbaixa, ordemcompra.rateioverificado, aux_ordemcompra.situacaosuprimentos, " +
					"fornecedor.cdpessoa, ordemcompra.frete, empresa.cdpessoa, ordemcompra.frete, ordemcompra.desconto, ordemcompra.valoricmsst, " +
					"material.cdmaterial, material.nome, listaMaterial.cdordemcompramaterial, projeto.cdprojeto, listaMaterial.qtdpedida, listaMaterial.qtdefrequencia, " +
					"unidademedida.cdunidademedida, unidademedidaMaterial.cdunidademedida ")
			.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
			.leftOuterJoin("ordemcompra.fornecedor fornecedor")
			.leftOuterJoin("ordemcompra.empresa empresa")
			.join("ordemcompra.listaMaterial listaMaterial")
			.join("listaMaterial.material material")
			.leftOuterJoin("material.unidademedida unidademedidaMaterial")
			.leftOuterJoin("listaMaterial.projeto projeto")
			.leftOuterJoin("listaMaterial.unidademedida unidademedida")
			.whereIn("ordemcompra.cdordemcompra", whereIn)
			.list();
	}

	/**
	 * 
	 * @param whereIn
	 * @return
	 */
	public List<Ordemcompra> findOrdensForEntrada(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		return query()
		.select("ordemcompra.cdordemcompra, ordemcompra.dtautorizacao, ordemcompra.dtcriacao, ordemcompra.dtpedido, ordemcompra.dtcancelamento, ordemcompra.dtproximaentrega, ordemcompra.observacao, ordemcompra.rateioverificado, " +
				"ordemcompra.dtproximaentrega, ordemcompra.desconto, ordemcompra.frete, fornecedor.cdpessoa, fornecedor.nome, listaMaterial.cdordemcompramaterial, listaMaterial.qtdpedida, listaMaterial.valor, " +
				"listaMaterial.icmsiss, listaMaterial.ipi, listaMaterial.icmsissincluso, listaMaterial.ipiincluso, material.cdmaterial, material.nome, material.identificacao, cotacao.cdcotacao, " +
				"listaOrdemcomprahistorico.cdordemcomprahistorico, listaOrdemcomprahistorico.observacao, listaOrdemcomprahistorico.dtaltera, listaOrdemcomprahistorico.cdusuarioaltera, ordemcompraacao.cdordemcompraacao, ordemcompraacao.nome, " +
				"localarmazenagem.cdlocalarmazenagem, empresa.cdpessoa, prazopagamento.cdprazopagamento, contato.cdpessoa, aux_ordemcompra.cdordemcompra, aux_ordemcompra.situacaosuprimentos, " +
				"listaArquivosordemcompra.cdordemcompraarquivo, arquivo.cdarquivo, arquivo.nome, arquivo.tipoconteudo, arquivo.tamanho, rateio.cdrateio, ordemcompra.cdusuarioaltera, ordemcompra.dtaltera, " +
				"ordemcompra.faturamentocliente, ordemcompra.tipofrete, garantia.cdgarantia, frequencia.cdfrequencia, frequencia.nome, listaMaterial.qtdefrequencia, material.servico, " +
				"contagerencialMat.cdcontagerencial, contagerencialMat.nome, vcontagerencialMat.identificador, vcontagerencialMat.arvorepai, documentotipo.cddocumentotipo, " +
				"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, ordemcompra.valoricmsst, ordemcompra.observacoesinternas, transportador.cdpessoa, transportador.nome, listaMaterial.dtentrega, " +
				"listaOrdemcompraorigem.cdordemcompraorigem, solicitacaocompra.cdsolicitacaocompra, listaMaterial.valoroutrasdespesas")
		.join("ordemcompra.fornecedor fornecedor")
		.join("ordemcompra.prazopagamento prazopagamento")
		.join("ordemcompra.listaMaterial listaMaterial")
		.join("listaMaterial.material material")
		.join("listaMaterial.contagerencial contagerencialMat")
		.join("contagerencialMat.vcontagerencial vcontagerencialMat")
		.join("listaMaterial.frequencia frequencia")
		.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
		.leftOuterJoin("listaMaterial.unidademedida unidademedida")
		.leftOuterJoin("ordemcompra.garantia garantia")
		.leftOuterJoin("ordemcompra.rateio rateio")
		.leftOuterJoin("ordemcompra.contato contato")
		.leftOuterJoin("ordemcompra.documentotipo documentotipo")
		.leftOuterJoin("ordemcompra.localarmazenagem localarmazenagem")
		.leftOuterJoin("ordemcompra.empresa empresa")
		.leftOuterJoin("ordemcompra.cotacao cotacao")
		.leftOuterJoin("ordemcompra.transportador transportador")
		.leftOuterJoin("ordemcompra.listaOrdemcomprahistorico listaOrdemcomprahistorico")
		.leftOuterJoin("ordemcompra.listaOrdemcompraorigem listaOrdemcompraorigem")
		.leftOuterJoin("listaOrdemcompraorigem.solicitacaocompra solicitacaocompra")
		.leftOuterJoin("listaOrdemcomprahistorico.ordemcompraacao ordemcompraacao")
		.leftOuterJoin("ordemcompra.listaArquivosordemcompra listaArquivosordemcompra")
		.leftOuterJoin("listaArquivosordemcompra.arquivo arquivo")
		.whereIn("ordemcompra.cdordemcompra", whereIn)
		.orderBy("listaOrdemcomprahistorico.dtaltera desc")
		.list();
	}
	
	/**
	 * Da update na(s) ordens de acordo com a situa��o da ordem
	 * 
	 * @param whereIn
	 * @param situacao
	 * @author Tom�s Rabelo
	 */
	public void doUpdateSituacaoOrdens(String whereIn, Situacaosuprimentos situacao) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		if (situacao == null) {
			throw new SinedException("Situa��o da ordem n�o pode ser nulo.");
		}
		
		Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		
		StringBuilder sql = new StringBuilder()
			.append("update Ordemcompra o set o.cdusuarioaltera = ?, o.dtaltera = ?, ");
		
		if(situacao.equals(AUTORIZADA)){
			sql.append("o.dtautorizacao = ?");
		} else if(situacao.equals(PEDIDO_ENVIADO)) {
			sql.append("o.dtpedido = ?");
		} else if(situacao.equals(BAIXADA)){
			sql.append("o.dtbaixa = ?");
		} else if(situacao.equals(CANCELADA)){
			sql.append("o.dtcancelamento = ?");
		} else {
			sql.append("o.dtautorizacao = null, o.dtcancelamento = null, o.dtbaixa = null, o.dtpedido = null, o.dtaprova = null");
		}
		
		sql.append(" where o.cdordemcompra in ("+whereIn+")");
		
		getHibernateTemplate().bulkUpdate(sql.toString(), situacao.equals(ESTORNAR) ? new Object[]{cdusuarioaltera,hoje} : new Object[]{cdusuarioaltera,hoje, hoje});
	}

	/**
	 * Busca ordens de compra
	 * 
	 * A query est� sendo feita utilizando fetch pois, se for feita buscando apenas os campos necess�rios
	 * algumas listas como listaEndereco do fornecedor, listaCaracteristica do material est�o sendo carregadas 
	 * com apenas 1 item. Este problema parece ser do hibernate.
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Ordemcompra> findOrdensCompra(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		return query()
//			.leftOuterJoinFetch("ordemcompra.empresa empresa")
//			.leftOuterJoinFetch("empresa.logomarca logomarca")
//			.leftOuterJoinFetch("empresa.listaTelefone listaTelefone")
			.joinFetch("ordemcompra.listaMaterial listaMaterial")
			.joinFetch("listaMaterial.material material")
			.leftOuterJoinFetch("listaMaterial.unidademedida unidademedida")
			.joinFetch("listaMaterial.contagerencial contagerencialMat")
			.leftOuterJoinFetch("material.contagerencial contagerencial")
			.joinFetch("listaMaterial.frequencia frequencia")
			.joinFetch("material.unidademedida unidademedida2")
//			.joinFetch("ordemcompra.fornecedor fornecedor")
			.leftOuterJoinFetch("ordemcompra.garantia garantia")
			.leftOuterJoinFetch("ordemcompra.localarmazenagem")
			.leftOuterJoinFetch("ordemcompra.documentotipo")
			.leftOuterJoinFetch("ordemcompra.rateio")
			.leftOuterJoinFetch("ordemcompra.contato contato")
			.leftOuterJoinFetch("ordemcompra.prazopagamento")
//			.leftOuterJoinFetch("material.listaCaracteristica")
//			.leftOuterJoinFetch("ordemcompra.fornecedoroptriangular")
//			.leftOuterJoinFetch("ordemcompra.enderecoentrega")
//			.leftOuterJoinFetch("fornecedor.listaEndereco listaEndereco")
//			.leftOuterJoinFetch("listaEndereco.municipio municipio")
//			.leftOuterJoinFetch("municipio.uf")
//			.leftOuterJoinFetch("ordemcompra.transportador transportador")
//			.leftOuterJoinFetch("transportador.listaTelefone listaTelefoneTransportador")
			.whereIn("ordemcompra.cdordemcompra", whereIn)
			.list();
		/*
		 * Obs: Esta query especificando somente os campos necess�rios quando busca 2 listas caracteristicas de materiais e telefones do contato ela vem so com 1 item,
		 * parece ser problema do hibernate. Para resolver tal problema quando houver contato ele carrega a lista separa o mesmo para caracteristicas.
		 * Query deixada como backup caso a de cima apresente problemas de performance
		 
		return query()
			.select("ordemcompra.cdordemcompra, ordemcompra.observacao, listaMaterial.qtdpedida, listaMaterial.valor, listaMaterial.icmsiss, listaMaterial.ipi, "+
					"listaMaterial.icmsissincluso, listaMaterial.ipiincluso, material.cdmaterial, material.nome, unidademedida.simbolo, fornecedor.cdpessoa, fornecedor.nome, fornecedor.cnpj, fornecedor.cpf, "+
					"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, contato.cdpessoa, contato.nome")
			.join("ordemcompra.listaMaterial listaMaterial")
			.join("listaMaterial.material material")
			.join("material.unidademedida unidademedida")
			.join("ordemcompra.fornecedor fornecedor")
			.leftOuterJoin("ordemcompra.localarmazenagem localarmazenagem")
			.leftOuterJoin("ordemcompra.contato contato")
			.whereIn("ordemcompra.cdordemcompra", whereIn)
			.list();*/ 
	}

	/**
	 * Carrega a ordem de compra da entrega e s� preenchido com alista de material do material passado por par�metro.
	 * OBS: Material pode ser nulo, quando isso acontecer ser� carregado todos os materiais.
	 *
	 * @param entrega
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Ordemcompra loadForBaixaEntrega(Entrega entrega, Material material) {
		if (entrega == null || entrega.getCdentrega() == null) {
			throw new SinedException("Entrega n�o pode ser nulo.");
		}
		return query()
					.select("ordemcompra.cdordemcompra, cotacao.cdcotacao, ordemcompramaterial.qtdpedida, material.cdmaterial, material.nome, material.identificacao")
					.join("ordemcompra.listaOrdemcompraentrega listaOrdemcompraentrega")
					.join("listaOrdemcompraentrega.entrega entrega")
					.leftOuterJoin("ordemcompra.cotacao cotacao")
					.leftOuterJoin("ordemcompra.listaMaterial ordemcompramaterial")
					.leftOuterJoin("ordemcompramaterial.material material")
					.where("material = ?",material)					
					.where("entrega = ?",entrega)
					.unique();
	}
	
	/**
	 * Carrega a ordem de compra com a lista de material.
	 *
	 * @param ordemcompra
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Ordemcompra loadWithLista(Ordemcompra ordemcompra){
		if (ordemcompra == null || ordemcompra.getCdordemcompra() == null){
			throw new SinedException("Ordem de compra n�o pode ser nulo.");
		}
		return query()
			.select("ordemcompra.cdordemcompra, ordemcompramaterial.qtdpedida, material.cdmaterial, material.nome, material.identificacao")
			.leftOuterJoin("ordemcompra.listaMaterial ordemcompramaterial")
			.leftOuterJoin("ordemcompramaterial.material material")
			.where("ordemcompra = ?",ordemcompra)
			.unique();
	}

	/**
	 *  M�todo que busca as ordens de compra para serem autorizadas.
	 *  
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Ordemcompra> findOrdensParaAutorizar(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		return query()
			.select("ordemcompra.cdordemcompra, ordemcompra.dtbaixa, ordemcompra.rateioverificado, aux_ordemcompra.situacaosuprimentos, " +
					"ordemcompra.frete, ordemcompra.valoricmsst, ordemcompra.desconto, rateio.cdrateio, listaMaterial.valor, empresa.cdpessoa, " +
					"prazopagamento.cdprazopagamento, ordemcompra.dtproximaentrega")
			.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
			.join("ordemcompra.listaMaterial listaMaterial")
			.join("ordemcompra.prazopagamento prazopagamento")
			.leftOuterJoin("ordemcompra.rateio rateio")
			.leftOuterJoin("ordemcompra.empresa empresa")
			.whereIn("ordemcompra.cdordemcompra", whereIn)
			.list();
	}

	/**
	 * M�todo que da um update na ordem de compra mudando a situa��o para autorizada e colocando o fluxo de caixa gerado
	 * 
	 * @param ordemcompra
	 * @author Tom�s T. Rabelo
	 */
	public void doUpdateOrdemCompraAutorizar(Ordemcompra ordemcompra) {
		Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		
		StringBuilder sql = new StringBuilder()
			.append("update Ordemcompra o set o.cdusuarioaltera = ?, o.dtaltera = ?, o.dtautorizacao = ? ")
			.append("where o.cdordemcompra = "+ordemcompra.getCdordemcompra());
		
		getHibernateTemplate().bulkUpdate(sql.toString(), new Object[]{cdusuarioaltera, hoje, hoje});
	}
	
	public void doUpdateOrdemCompraAprovar(Ordemcompra ordemcompra) {
		Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		
		StringBuilder sql = new StringBuilder()
		.append("update Ordemcompra o set o.cdusuarioaltera = ?, o.dtaltera = ?, o.dtaprova = ? ")
		.append("where o.cdordemcompra = "+ordemcompra.getCdordemcompra());
		
		getHibernateTemplate().bulkUpdate(sql.toString(), new Object[]{cdusuarioaltera, hoje, hoje});
	}

	/**
	 * Carrega a lista de ordens de compra para que o fluxo de caixa seja exclu�do.
	 *
	 * @param ids
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Ordemcompra> findWithFluxocaixa(String ids) {
		if (ids == null || ids.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		return query()
					.select("ordemcompra.cdordemcompra, aux_ordemcompra.situacaosuprimentos, fluxocaixa.cdfluxocaixa, rateio.cdrateio")
					.leftOuterJoin("ordemcompra.listaFluxocaixaorigem listaFluxocaixaorigem")
					.leftOuterJoin("listaFluxocaixaorigem.fluxocaixa fluxocaixa")
					.leftOuterJoin("fluxocaixa.rateio rateio")
					.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
					.whereIn("ordemcompra.cdordemcompra", ids)
					.list();
	}

	/**
	 * Seta o fluxo de caixa igual a nulo.
	 *
	 * @param cdordemcompra
	 * @author Rodrigo Freitas
	 
	public void setNullFluxocaixa(Integer cdordemcompra) {
		getHibernateTemplate().bulkUpdate("update Ordemcompra o set o.fluxocaixa = null where o.id = ?", new Object[]{cdordemcompra});		
	}*/

	/**
	 * M�todo que busca ordem de compra para enviar pedido
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Ordemcompra findOrdemCompraParaEnviarPedido(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		if(whereIn.split(",").length > 1){
			throw new SinedException("S� � poss�vel enviar 1 pedido por vez.");
		}
		return query()
			.select("ordemcompra.cdordemcompra, aux_ordemcompra.situacaosuprimentos, contato.cdpessoa, " +
					"contato.emailcontato, contato.nome, fornecedor.cdpessoa, empresa.cdpessoa, ordemcompra.rateioverificado, "+
					"listaArquivosordemcompra.cdordemcompraarquivo, listaArquivosordemcompra.enviarFornecedor, arquivo.cdarquivo, arquivo.nome, arquivo.tipoconteudo, arquivo.tamanho")
			.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
			.join("ordemcompra.fornecedor fornecedor")
			.leftOuterJoin("ordemcompra.empresa empresa")
			.leftOuterJoin("ordemcompra.contato contato")
			.leftOuterJoin("ordemcompra.listaArquivosordemcompra listaArquivosordemcompra")
			.leftOuterJoin("listaArquivosordemcompra.arquivo arquivo")
			.where("ordemcompra.cdordemcompra = ?", new Integer(whereIn))
			.unique();
	}
	
	/**
	 * Chama a procedure de atualiza��o da tabela auxiliar de ordem de compra
	 *
	 * @param cotacao
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaOrdemcompra(Ordemcompra ordemcompra){
		if (ordemcompra == null || ordemcompra.getCdordemcompra() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		getJdbcTemplate().execute("SELECT ATUALIZA_ORDEMCOMPRA(" + ordemcompra.getCdordemcompra()+ ", '" + NeoWeb.getRequestContext().getServletRequest().getContextPath() + "')");
	}

	/**
	 * M�todo que busca a outra parte necess�ria para buscar as solicita��es de compra da ordem de compra.
	 * 
	 * @param cotacao
	 * @return
	 * @author Tom�s Rabelo
	 * @param ordemcompra 
	 */
	public List<Ordemcompra> getOrdensCompraDaCotacao(Cotacao cotacao, Ordemcompra ordemcompra) {
		if (cotacao == null || cotacao.getCdcotacao() == null){
			throw new SinedException("Cota��o n�o pode ser nulo.");
		}
		return query()
			.select("ordemcompra.cdordemcompra, ordemcompramaterial.qtdpedida, ordemmaterial.cdmaterial, fornecedorordem.cdpessoa, " +
					"localarmazenagem.cdlocalarmazenagem")
			.join("ordemcompra.listaMaterial ordemcompramaterial")
			.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
			.join("ordemcompra.cotacao cotacao")
			.join("ordemcompra.fornecedor fornecedorordem")
			.join("ordemcompra.localarmazenagem localarmazenagem")
			.join("ordemcompramaterial.material ordemmaterial")
			.where("cotacao = ?",cotacao)
			.whereIn("aux_ordemcompra.situacaosuprimentos", Situacaosuprimentos.PEDIDO_ENVIADO.getCodigo()+","+Situacaosuprimentos.ENTREGA_PARCIAL.getCodigo()+","+Situacaosuprimentos.BAIXADA.getCodigo()).or()
			.where("ordemcompra = ?", ordemcompra)
			.list();
	}
	
	/**
	 * M�todo que busca ordem de compra da cota��o
	 *
	 * @param cotacao
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Ordemcompra> findForEdicaoCotacao(Cotacao cotacao) {
		if (cotacao == null || cotacao.getCdcotacao() == null){
			throw new SinedException("Cota��o n�o pode ser nulo.");
		}
		return query()
			.select("ordemcompra.cdordemcompra, ordemcompramaterial.qtdpedida, ordemmaterial.cdmaterial, fornecedorordem.cdpessoa")
			.join("ordemcompra.listaMaterial ordemcompramaterial")
			.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
			.join("ordemcompra.cotacao cotacao")
			.join("ordemcompra.fornecedor fornecedorordem")
			.join("ordemcompramaterial.material ordemmaterial")
			.where("cotacao = ?",cotacao)
			.where("aux_ordemcompra.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
			.list();
	}

	/**
	 * M�todo que busca ordens de compra para estorno. Ele � diferente pois caso as ordens passem pela valida��o agiliza o trabalho de buscar as
	 * solicita��es de compra da ordem de compra caso haja.
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Ordemcompra> findOrdensParaEstorno(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		return query()
			.select("ordemcompra.cdordemcompra, ordemcompra.dtbaixa, ordemcompra.rateioverificado, aux_ordemcompra.situacaosuprimentos, cotacao.cdcotacao, " +
					"material.cdmaterial, localarmazenagem.cdlocalarmazenagem, fornecedor.cdpessoa, listaMaterial.qtdpedida")
			.join("ordemcompra.listaMaterial listaMaterial")
			.join("ordemcompra.fornecedor fornecedor")
			.join("ordemcompra.localarmazenagem localarmazenagem")
			.join("listaMaterial.material material")
			.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
			.leftOuterJoin("ordemcompra.cotacao cotacao")
			.whereIn("ordemcompra.cdordemcompra", whereIn)
			.list();
	}

	/**
	 * M�todo que verifica se a entrega que esta sendo gerada ir� precisar de romaneio
	 * 
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	@SuppressWarnings("unchecked")
	public boolean verificaSeEntregaPrecisaRomaneio(Entregadocumento bean, Ordemcompra ordemcompra) {
		SinedUtil.markAsReader();
		List<Material> list = getJdbcTemplate().query(
				"SELECT M.CDMATERIAL " +
				"FROM ORDEMCOMPRA O "+
				"JOIN COTACAO C ON C.CDCOTACAO = O.CDCOTACAO "+ 
				"JOIN COTACAOFORNECEDOR CF ON CF.CDCOTACAO = C.CDCOTACAO "+ 
				"JOIN FORNECEDOR F ON F.CDPESSOA = CF.CDFORNECEDOR "+
				"JOIN COTACAOFORNECEDORITEM CFI ON CFI.CDCOTACAOFORNECEDOR = CF.CDCOTACAOFORNECEDOR "+ 
				"JOIN MATERIAL M ON M.CDMATERIAL = CFI.CDMATERIAL "+
				"WHERE O.CDORDEMCOMPRA = "+ordemcompra.getCdordemcompra()+" AND "+
				"M.CDMATERIAL IN ("+CollectionsUtil.listAndConcatenate(bean.getListaEntregamaterial(), "material.cdmaterial",",")+") AND " +
				"F.CDPESSOA = "+bean.getFornecedor().getCdpessoa()+" AND "+
				"CFI.CDLOCALARMAZENAGEM <> CFI.CDLOCALARMAZENAGEMSOLICITACAO"
				,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						Material material = new Material(rs.getInt("CDMATERIAL"));
						
						return material;
					}
				});
		if(list != null && list.size() == 0)
			return false;
		
		return true;
	}

	/**
	 * M�todo que busca dados para o relat�rio de analise de compra. Busca todos as informa��es de ordens de compra que possuem entrega essas 
	 * entregas possuem conta a pagar e foram baixadas.
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	@SuppressWarnings("unchecked")
	public List<AnaliseCompraBean> getDadosForAnaliseCompra(AnalisecompraFiltro filtro) {
		String inicio = new SimpleDateFormat("yyyy-MM-dd").format(filtro.getDtinicio());
		String fim = null;
		if (filtro.getDtfim() != null)
			fim = new SimpleDateFormat("yyyy-MM-dd").format(filtro.getDtfim());
		
		String sql =
					"SELECT M.CDMATERIAL AS CDMATERIAL, M.NOME AS NOMEMATERIAL, P.CDPESSOA AS CDPESSOA, P.NOME AS NOMEFORNECEDOR, O.DTCRIACAO AS DATACOMPRA, O.DTPROXIMAENTREGA AS DATAENTREGA,   " +
					"(OCM.VALOR / (OCM.QTDPEDIDA * OCM.QTDEFREQUENCIA)) AS VALORUNITARIO, 													  " +
					"OCM.QTDPEDIDA AS QTDE, SUM(PPI.DIAS) AS QTDEDIAS," +
					"(O.FRETE / 100) AS FRETE,										  														  " +
					"(O.DESCONTO / 100) AS DESCONTO,				     																	  " +
					"OCM.VALOR AS TOTAL " +
					
					"FROM ORDEMCOMPRA O 																									  " +
					"JOIN PESSOA P ON P.CDPESSOA = O.CDFORNECEDOR       																	  " +
					"JOIN RATEIO r ON R.CDRATEIO = O.CDRATEIO 																				  " +
					"JOIN RATEIOITEM RI ON RI.CDRATEIO = R.CDRATEIO 																		  " +
					"JOIN ORDEMCOMPRAMATERIAL OCM ON OCM.CDORDEMCOMPRA = O.CDORDEMCOMPRA 													  " +
					"JOIN MATERIAL M ON M.CDMATERIAL = OCM.CDMATERIAL 																	      " +
					"JOIN PRAZOPAGAMENTO PP ON PP.CDPRAZOPAGAMENTO = O.CDPRAZOPAGAMENTO 													  " +
					"LEFT OUTER JOIN PRAZOPAGAMENTOITEM PPI ON PPI.CDPRAZOPAGAMENTO = PP.CDPRAZOPAGAMENTO	 								  " +
					"JOIN AUX_ORDEMCOMPRA AOC ON AOC.CDORDEMCOMPRA = O.CDORDEMCOMPRA 														  " +
					"LEFT OUTER JOIN MATERIALGRUPO MG ON MG.CDMATERIALGRUPO = M.CDMATERIALGRUPO				 								  " ;
		
		if(filtro.getMaterialcategoria() != null 
				&& filtro.getMaterialcategoria().getIdentificador() != null){
			
			sql +=" LEFT OUTER JOIN MATERIALCATEGORIA MC ON MC.CDMATERIALCATEGORIA = M.CDMATERIALCATEGORIA										" + 
				  "LEFT OUTER JOIN VMATERIALCATEGORIA VC ON VC.CDMATERIALCATEGORIA = MC.CDMATERIALCATEGORIA										";
		}
		

		sql +=	  "WHERE AOC.SITUACAO IN (1, 12, 6, 7, 8) 																					  " +
				  "AND O.DTCRIACAO >= '" + inicio +"' ";
		if (fim != null){
			sql += "AND O.DTCRIACAO <= '" + fim + "' ";
		}
		
		//PERMISS�O DE PROJETOS
		if(NeoWeb.getUser() != null){
			Usuario usuario = (Usuario)NeoWeb.getUser();
			if(!usuario.getTodosprojetos()){
				sql += " AND O.CDORDEMCOMPRA IN (" +
												"SELECT OSUBQUERY.CDORDEMCOMPRA " +
												"FROM ORDEMCOMPRA OSUBQUERY " +
												"JOIN RATEIOITEM RISUBQUERY ON RISUBQUERY.CDRATEIO = OSUBQUERY.CDRATEIO " +
												"WHERE RISUBQUERY.CDPROJETO IN (" + SinedUtil.getListaProjeto() + ")" +
												")";
			}
		}
		
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null)
			sql += " AND O.CDEMPRESA = "+filtro.getEmpresa().getCdpessoa();
		if(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null)
			sql += " AND OCM.CDMATERIAL = "+filtro.getMaterial().getCdmaterial();
		if(filtro.getContagerencial() != null && filtro.getContagerencial().getCdcontagerencial() != null)
			sql += " AND RI.CDCONTAGERENCIAL = "+filtro.getContagerencial().getCdcontagerencial();
		if(filtro.getProjeto() != null && filtro.getProjeto().getCdprojeto() != null)
			sql += " AND OCM.CDPROJETO = "+filtro.getProjeto().getCdprojeto();
		if(filtro.getFornecedor() != null && filtro.getFornecedor().getCdpessoa() != null)
			sql += " AND O.CDFORNECEDOR = "+filtro.getFornecedor().getCdpessoa();
		if(filtro.getMaterialgrupo() != null && filtro.getMaterialgrupo().getCdmaterialgrupo() != null)
			sql += " AND MG.CDMATERIALGRUPO = "+ filtro.getMaterialgrupo().getCdmaterialgrupo();
		
		if(filtro.getMaterialcategoria() != null 
				&& filtro.getMaterialcategoria().getIdentificador() != null){
			
			sql += " AND (VC.IDENTIFICADOR LIKE '"+ filtro.getMaterialcategoria().getIdentificador() +".%'";
			sql += " OR VC.IDENTIFICADOR LIKE '"+ filtro.getMaterialcategoria().getIdentificador() +"')";
		}
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioService.isRestricaoFornecedorColaborador(usuarioLogado)){
			sql += (" AND ("+
							"EXISTS("+
								"SELECT 1 FROM COLABORADORFORNECEDOR CF "+
								"WHERE "+
								"CF.CDFORNECEDOR =  O.CDFORNECEDOR AND "+
								"CF.CDCOLABORADOR = "+usuarioLogado.getCdpessoa()+
							") "+
							"OR "+
							"NOT EXISTS("+
								"SELECT 1 FROM COLABORADORFORNECEDOR CF "+
								"WHERE "+
								"CF.CDFORNECEDOR = O.CDFORNECEDOR"+
							")"+
						")");
		}
		
		if(SinedUtil.isRestricaoEmpresaFornecedorUsuarioLogado()){
			String whereInEmpresa = new SinedUtil().getListaEmpresa();
			sql += " AND O.CDFORNECEDOR in ( select fRestricao.cdpessoa " +
	    							"from Fornecedor fRestricao " +
	    							"left outer join fornecedorempresa feRestricao on feRestricao.cdfornecedor= fRestricao.cdpessoa " +
	    							"where feRestricao.cdempresa is null or feRestricao.cdempresa in (" + whereInEmpresa + ")) ";
		}
		
		sql += " GROUP BY  M.CDMATERIAL, M.NOME, P.CDPESSOA, P.NOME, O.DTCRIACAO, O.DTPROXIMAENTREGA, OCM.VALOR, OCM.QTDPEDIDA, OCM.QTDEFREQUENCIA, O.FRETE, O.DESCONTO " +
		"ORDER BY M.NOME";
		
		SinedUtil.markAsReader();
		List<AnaliseCompraBean> list = getJdbcTemplate().query(
				sql,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						AnaliseCompraBean bean = new AnaliseCompraBean(rs.getInt("CDMATERIAL"), rs.getString("NOMEMATERIAL"), 
												rs.getInt("CDPESSOA"), rs.getString("NOMEFORNECEDOR"), rs.getDate("DATACOMPRA"), 
												 rs.getDate("DATAENTREGA"), rs.getDouble("VALORUNITARIO"), 
												 rs.getDouble("QTDE"), rs.getInt("QTDEDIAS"), rs.getDouble("TOTAL"), 
												 rs.getDouble("FRETE"), rs.getDouble("DESCONTO"));
						return bean;
					}
				});
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Vanalisecompra> findForAnaliseCompra(AnalisecompraFiltro filtro, String whereInMaterial) {
		if(whereInMaterial == null || "".equals(whereInMaterial))
			throw new SinedException("Par�metro inv�lido.");
		
		String sql = 
			"SELECT VAC.CDMATERIAL AS CDMATERIAL, VAC.CDFORNECEDOR AS CDFORNECEDOR, " +
			"		VAC.DTENTREGA AS DATAENTREGA, VAC.DTENTRADA AS DATAENTRADA, " +
			"		VAC.VALORUNITARIO AS VALORUNITARIO, VAC.DTCRIACAOORDEMCOMPRA AS DATACRIACAOORDEMCOMPRA  " +
			"FROM VANALISECOMPRA VAC " +
			"WHERE VAC.CDMATERIAL IN (" + whereInMaterial + ") " +
			(filtro != null && filtro.getEmpresa() != null ? (" AND  VAC.CDEMPRESA = " + filtro.getEmpresa().getCdpessoa() + " " ) : " ") +
			(filtro != null && filtro.getFornecedor() != null ? (" AND  VAC.CDFORNECEDOR = " + filtro.getFornecedor().getCdpessoa() + " " ) : " ") + 
			"GROUP BY VAC.CDMATERIAL, VAC.CDFORNECEDOR, " +
			"		VAC.DTENTREGA, VAC.DTENTRADA, " +
			"		VAC.VALORUNITARIO, VAC.DTCRIACAOORDEMCOMPRA";

		SinedUtil.markAsReader();
		List<Vanalisecompra> list = getJdbcTemplate().query(
				sql,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						Vanalisecompra bean = new Vanalisecompra(rs.getInt("CDMATERIAL"),  
																 rs.getInt("CDFORNECEDOR"), 
																 rs.getDate("DATAENTREGA"),
																 rs.getDate("DATAENTRADA"),
																 rs.getDouble("VALORUNITARIO"), 
																 rs.getDate("DATACRIACAOORDEMCOMPRA"));
						return bean;
					}
				});
		return list;
	}
	
	/**
	 * M�todo que busca os dados para relat�rio an�lise de compra sint�tico
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<AnaliseCompraBean> getDadosForAnaliseCompraSintetico(AnalisecompraFiltro filtro) {
		String inicio = new SimpleDateFormat("yyyy-MM-dd").format(filtro.getDtinicio());
		String fim = null;
		if (filtro.getDtfim() != null)
			fim = new SimpleDateFormat("yyyy-MM-dd").format(filtro.getDtfim());
		
		String sql =
					"SELECT M.CDMATERIAL AS CDMATERIAL, M.NOME AS NOMEMATERIAL, O.CDORDEMCOMPRA, O.DTCRIACAO AS DATACOMPRA,	" +
					"(OCM.VALOR / (OCM.QTDPEDIDA * OCM.QTDEFREQUENCIA)) AS VALORUNITARIO, 					 				" +
					
					"(SELECT EM.VALORUNITARIO " +
					"	FROM ENTREGAMATERIAL EM " +
					"	JOIN ENTREGADOCUMENTO ED ON ED.CDENTREGADOCUMENTO = EM.CDENTREGADOCUMENTO " +
					"	JOIN ENTREGA E ON E.CDENTREGA = ED.CDENTREGA " +
					"	JOIN AUX_ENTREGA AUX_E ON AUX_E.CDENTREGA = E.CDENTREGA " +
					"	WHERE EM.CDMATERIAL = M.CDMATERIAL AND " +
					"	(E.DTENTREGA IS NOT NULL OR ED.DTENTRADA IS NOT NULL) AND " +
					(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null ? 
							(" (	E.CDEMPRESA = " + filtro.getEmpresa().getCdpessoa() + 
							 "  OR ED.CDEMPRESA = " + filtro.getEmpresa().getCdpessoa() +
							 "  ) AND "): " ") +
					"	AUX_E.SITUACAO <> " + Situacaosuprimentos.CANCELADA.getCodigo() + " " +
					"	ORDER BY E.DTENTREGA DESC, ED.DTENTRADA DESC, E.CDENTREGA DESC " +
					"	LIMIT 1) as VALORULTIMACOMPRA  	" +
					   
					"FROM ORDEMCOMPRA O 																	" +
					"JOIN PESSOA P ON P.CDPESSOA = O.CDFORNECEDOR       									" +
					"JOIN RATEIO r ON R.CDRATEIO = O.CDRATEIO 												" +
					"JOIN RATEIOITEM RI ON RI.CDRATEIO = R.CDRATEIO 										" +
					"JOIN ORDEMCOMPRAMATERIAL OCM ON OCM.CDORDEMCOMPRA = O.CDORDEMCOMPRA 					" +
					"JOIN MATERIAL M ON M.CDMATERIAL = OCM.CDMATERIAL 										" +
					"JOIN PRAZOPAGAMENTO PP ON PP.CDPRAZOPAGAMENTO = O.CDPRAZOPAGAMENTO 					" +
					"LEFT OUTER JOIN PRAZOPAGAMENTOITEM PPI ON PPI.CDPRAZOPAGAMENTO = PP.CDPRAZOPAGAMENTO	" +
					"JOIN AUX_ORDEMCOMPRA AOC ON AOC.CDORDEMCOMPRA = O.CDORDEMCOMPRA 						" +
					"LEFT OUTER JOIN MATERIALGRUPO MG ON MG.CDMATERIALGRUPO = M.CDMATERIALGRUPO				" ;
		
		if(filtro.getMaterialcategoria() != null 
				&& filtro.getMaterialcategoria().getIdentificador() != null){
			
			sql +=" LEFT OUTER JOIN MATERIALCATEGORIA MC ON MC.CDMATERIALCATEGORIA = M.CDMATERIALCATEGORIA										" + 
				  " LEFT OUTER JOIN VMATERIALCATEGORIA VC ON VC.CDMATERIALCATEGORIA = MC.CDMATERIALCATEGORIA										";
		}

		    sql +="WHERE AOC.SITUACAO IN (1, 12, 6, 7, 8) 													" +
				  "AND O.DTCRIACAO >= '" + inicio +"' ";
		if (fim != null){
			sql += "AND O.DTCRIACAO <= '" + fim + "' ";
		}
		
		//PERMISS�O DE PROJETOS
		if(NeoWeb.getUser() != null){
			Usuario usuario = (Usuario)NeoWeb.getUser();
			if(!usuario.getTodosprojetos()){
				sql += " AND O.CDORDEMCOMPRA IN (" +
												"SELECT OSUBQUERY.CDORDEMCOMPRA " +
												"FROM ORDEMCOMPRA OSUBQUERY " +
												"JOIN RATEIOITEM RISUBQUERY ON RISUBQUERY.CDRATEIO = OSUBQUERY.CDRATEIO " +
												"WHERE RISUBQUERY.CDPROJETO IN (" + SinedUtil.getListaProjeto() + ")" +
												")";
			}
		}
		
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null)
			sql += " AND O.CDEMPRESA = "+filtro.getEmpresa().getCdpessoa();
		if(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null)
			sql += " AND OCM.CDMATERIAL = " + filtro.getMaterial().getCdmaterial();
		if(filtro.getContagerencial() != null && filtro.getContagerencial().getCdcontagerencial() != null)
			sql += " AND RI.CDCONTAGERENCIAL = "+filtro.getContagerencial().getCdcontagerencial();
		if(filtro.getProjeto() != null && filtro.getProjeto().getCdprojeto() != null)
			sql += " AND OCM.CDPROJETO = "+filtro.getProjeto().getCdprojeto();
		if(filtro.getFornecedor() != null && filtro.getFornecedor().getCdpessoa() != null)
			sql += " AND O.CDFORNECEDOR = "+filtro.getFornecedor().getCdpessoa();
		if(filtro.getMaterialgrupo() != null && filtro.getMaterialgrupo().getCdmaterialgrupo() != null)
			sql += " AND MG.CDMATERIALGRUPO = "+ filtro.getMaterialgrupo().getCdmaterialgrupo();
		
		if(filtro.getMaterialcategoria() != null 
				&& filtro.getMaterialcategoria().getIdentificador() != null){
			
			sql += " AND (VC.IDENTIFICADOR LIKE '"+ filtro.getMaterialcategoria().getIdentificador() +".%'";
			sql += " OR VC.IDENTIFICADOR LIKE '"+ filtro.getMaterialcategoria().getIdentificador() +"')";
		}
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioService.isRestricaoFornecedorColaborador(usuarioLogado)){
			sql += (" AND ("+
							"EXISTS("+
								"SELECT 1 FROM COLABORADORFORNECEDOR CF "+
								"WHERE "+
								"CF.CDFORNECEDOR =  O.CDFORNECEDOR AND "+
								"CF.CDCOLABORADOR = "+usuarioLogado.getCdpessoa()+
							") "+
							"OR "+
							"NOT EXISTS("+
								"SELECT 1 FROM COLABORADORFORNECEDOR CF "+
								"WHERE "+
								"CF.CDFORNECEDOR = O.CDFORNECEDOR"+
							")"+
						")");
		}
		
		if(SinedUtil.isRestricaoEmpresaFornecedorUsuarioLogado()){
			String whereInEmpresa = new SinedUtil().getListaEmpresa();
			sql += " AND O.CDFORNECEDOR in ( select fRestricao.cdpessoa " +
	    							"from Fornecedor fRestricao " +
	    							"left outer join fornecedorempresa feRestricao on feRestricao.cdfornecedor= fRestricao.cdpessoa " +
	    							"where feRestricao.cdempresa is null or feRestricao.cdempresa in (" + whereInEmpresa + ")) ";
		}
		
		sql += " GROUP BY  M.CDMATERIAL, M.NOME, OCM.VALOR, OCM.QTDPEDIDA, OCM.QTDEFREQUENCIA, O.CDORDEMCOMPRA, O.DTCRIACAO " +
		"ORDER BY M.NOME";
		
		SinedUtil.markAsReader();
		List<AnaliseCompraBean> list = getJdbcTemplate().query(
				sql,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						AnaliseCompraBean bean = new AnaliseCompraBean(
									rs.getInt("CDMATERIAL"), 
									rs.getString("NOMEMATERIAL"),
									rs.getInt("CDORDEMCOMPRA"),
									rs.getDate("DATACOMPRA"),
									rs.getDouble("VALORUNITARIO"), 
									rs.getDouble("VALORULTIMACOMPRA"));
						return bean;
					}
				});
		return list;
	}

	/**
	 * M�todo que retorna a data da �ltima ordem de compra cadastrada no sistema.
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Date getDtUltimoCadastro() {
		return newQueryBuilderSined(Date.class)
			
			.from(Ordemcompra.class)
			.setUseTranslator(false) 
			.select("max(ordemcompra.dtautorizacao)")
			.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
			.where("aux_ordemcompra.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
			.unique();
	}

	/**
	 * M�todo que retorna a qtde. total de ordens de compra que est�o na situa��o 'Em aberto'
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Integer getQtdeEmAberto() {
		return newQueryBuilderSined(Long.class)
			
			.from(Ordemcompra.class)
			.setUseTranslator(false) 
			.select("count(*)")
			.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
			.where("aux_ordemcompra.situacaosuprimentos = ?", Situacaosuprimentos.EM_ABERTO)
			.unique()
			.intValue();
	}
	
	/**
	 * Retorna a quantidade de Ordens de Compra Autorizadas
	 *
	 * @return
	 * @since 23/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdeAutorizadas() {
		return newQueryBuilderSined(Long.class)
			
			.from(Ordemcompra.class)
			.setUseTranslator(false) 
			.select("count(*)")
			.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
			.where("aux_ordemcompra.situacaosuprimentos = ?", Situacaosuprimentos.AUTORIZADA)
			.unique()
			.intValue();
	}
	
	/**
	 * Retorna a quantidade de Ordens de Compra na situa��o Pedido Enviado
	 *
	 * @return
	 * @since 23/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdePedidoEnviado() {
		return newQueryBuilderSined(Long.class)
		
		.from(Ordemcompra.class)
		.setUseTranslator(false) 
		.select("count(*)")
		.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
		.where("aux_ordemcompra.situacaosuprimentos = ?", Situacaosuprimentos.PEDIDO_ENVIADO)
		.unique()
		.intValue();
	}

	/**
	 * M�todo que retorna a qtde. total de ordens de compra que est�o na situa��o 'Baixada'
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Integer getQtdeBaixadas() {
		return newQueryBuilderSined(Long.class)
			
			.from(Ordemcompra.class)
			.setUseTranslator(false) 
			.select("count(*)")
			.where("ordemcompra.dtbaixa is not null")
			.unique()
			.intValue();
	}

	/**
	 * M�todo que retorna a qtde. total de ordens de compra cadastradas no sistema.
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Integer getQtdeTotalOrdensCompra() {
		return newQueryBuilderSined(Long.class)
			
			.from(Ordemcompra.class)
			.setUseTranslator(false) 
			.select("count(*)")
			.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
			.where("aux_ordemcompra.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
			.unique()
			.intValue();
	}
	
	/**
	 * M�todo que calcula o valor total de todas as ordens de compra a partir de AUTORIZADAS.
	 * 
	 * @return
	 * @author Giovane Freitas
	 */
	public Money getValorTotalProjeto(AcompanhamentoEconomicoReportFiltro filtro, Contagerencial contagerencial) {
		if(filtro == null || filtro.getProjeto() == null || filtro.getProjeto().getCdprojeto() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		QueryBuilder<Long> query = newQueryBuilder(Long.class)
		.from(Ordemcompra.class)
		.select("SUM(rateioitem.valor)")
		.leftOuterJoin("ordemcompra.rateio rateio")
		.leftOuterJoin("rateio.listaRateioitem rateioitem")
		.leftOuterJoin("rateioitem.contagerencial contagerencial")
		
		//listando TODAS as contas gerenciais filhas
		.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
		.openParentheses()
			.where("vcontagerencial.identificador like '" + contagerencial.getVcontagerencial().getIdentificador() + "%'")
			.or()
			.where("rateioitem.contagerencial = ?", contagerencial)
		.closeParentheses()

		//lista apenas as contas gerenciais iguais
		//.where("contagerencial = ?", contagerencial)

		.where("rateioitem.projeto = ?", filtro.getProjeto())
		.where("ordemcompra.dtcancelamento is null")
		.where("ordemcompra.dtautorizacao is not null")
		.where("ordemcompra.dtautorizacao <= ?", filtro.getDtFim());
	
			
		Long totalMaterial = query.setUseTranslator(false).unique();
		
		if (totalMaterial != null)
			return new Money(totalMaterial, true);
		else 
			return new Money();
		
	}
	/**
	 * M�todo que calcula o valor total de todas as ordens de compra a partir de AUTORIZADAS.
	 * 
	 * @return
	 * @author Giovane Freitas
	 */
	public Money getValorTotalProjetoFluxocaixa(AcompanhamentoEconomicoReportFiltro filtro, Contagerencial contagerencial) {
		if(filtro == null || filtro.getProjeto() == null || filtro.getProjeto().getCdprojeto() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		QueryBuilder<Long> query = newQueryBuilder(Long.class)
		
		.from(Fluxocaixa.class)
		
		.select("SUM(rateioitem.valor)")
		.leftOuterJoin("fluxocaixa.rateio rateio")
		.leftOuterJoin("rateio.listaRateioitem rateioitem")
		.leftOuterJoin("rateioitem.contagerencial contagerencial")
		.join("fluxocaixa.listaFluxocaixaorigem listaFluxocaixaorigem")
		.join("listaFluxocaixaorigem.ordemcompra ordemcompra")
		
		//listando TODAS as contas gerenciais filhas
		.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
		.openParentheses()
		.where("vcontagerencial.identificador like '" + contagerencial.getVcontagerencial().getIdentificador() + "%'")
		.or()
		.where("rateioitem.contagerencial = ?", contagerencial)
		.closeParentheses()
		
		//lista apenas as contas gerenciais iguais
		//.where("contagerencial = ?", contagerencial)
		
		.where("ordemcompra is not null")
		.where("rateioitem.projeto = ?", filtro.getProjeto())
		.where("ordemcompra.dtcancelamento is null")
		.where("ordemcompra.dtautorizacao is not null")
		
		.where("fluxocaixa.dtinicio <= ?", filtro.getDtFim());
		
		
		Long totalMaterial = query.setUseTranslator(false).unique();
		
		if (totalMaterial != null)
			return new Money(totalMaterial, true);
		else 
			return new Money();
		
	}

	/**
	 * M�todo que busca ordem de compra para enviar email
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Ordemcompra> findForEnvioemailcompra(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro n�o podem ser nulo.");
		
		return query()
				.select("ordemcompra.cdordemcompra, empresa.cdpessoa, empresa.email, projeto.cdprojeto, " +
						"listaRateioitem.cdrateioitem, rateio.cdrateio, listaMaterial.cdordemcompramaterial, " +
						"material.cdmaterial, materialgrupo.cdmaterialgrupo, listaMaterialgrupousuario.cdmaterialgrupousuario, " +
						"usuario.cdpessoa ")
				.leftOuterJoin("ordemcompra.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.leftOuterJoin("listaRateioitem.projeto projeto")
				.leftOuterJoin("ordemcompra.empresa empresa")
				.join("ordemcompra.listaMaterial listaMaterial")
				.join("listaMaterial.material material")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("materialgrupo.listaMaterialgrupousuario listaMaterialgrupousuario")
				.leftOuterJoin("listaMaterialgrupousuario.usuario usuario")
				.whereIn("ordemcompra.cdordemcompra", whereIn)
				.list();
	}
	
	/**
	 * M�todo que carrega a ordem de compra com a cota��o
	 *
	 * @param ordemcompra
	 * @return
	 * @author Luiz Fernando
	 */
	public Ordemcompra loadWithCotacao(Ordemcompra ordemcompra) {
		if(ordemcompra == null || ordemcompra.getCdordemcompra() == null)
			throw new SinedException("Par�metro n�o podem ser nulo.");
		
		return query()
				.select("ordemcompra.cdordemcompra, cotacao.cdcotacao, listaOrigem.cdcotacaoorigem, " +
						"solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.identificador, centrocusto.cdcentrocusto, centrocusto.nome, " +
						"projeto.cdprojeto, projeto.nome, colaborador.cdpessoa, colaborador.nome, " +
						"material.cdmaterial, material.nome, material.identificacao")
				.join("ordemcompra.cotacao cotacao")
				.join("cotacao.listaOrigem listaOrigem")
				.join("listaOrigem.solicitacaocompra solicitacaocompra")
				.join("solicitacaocompra.centrocusto centrocusto")
				.join("solicitacaocompra.colaborador colaborador")
				.join("solicitacaocompra.material material")
				.leftOuterJoin("solicitacaocompra.projeto projeto")
				.where("cotacao.dtcancelamento is null")
				.where("solicitacaocompra.dtcancelamento is null")
				.where("ordemcompra = ?", ordemcompra)
				.unique();
	}

	public List<Ordemcompra> findByItens(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido. (Id's ordem de compra material n�o pode ser nulo.)");
		
		return query()
					.select("ordemcompra.cdordemcompra, aux_ordemcompra.situacaosuprimentos")
					.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
					.join("ordemcompra.listaMaterial ordemcompramaterial")
					.whereIn("ordemcompramaterial.cdordemcompramaterial", whereIn)
					.list();
	}
	
	/**
	 * M�todo que verifica se a ordem de compra pode ser editada (situa��o Em Aberto)
	 *
	 * @param ordemcompra
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean podeEditar(Ordemcompra ordemcompra){
		if(ordemcompra == null || ordemcompra.getCdordemcompra() == null) return Boolean.TRUE;
		
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Ordemcompra.class)
				.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
				.where("ordemcompra = ?", ordemcompra)
				.where("aux_ordemcompra.situacaosuprimentos = ?", Situacaosuprimentos.EM_ABERTO)
				.unique() > 0;
	}

	/**
	 * M�todo que busca as ordens de compra de acordo com o documento
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Ordemcompra> findByDocumento(Documento documento) {
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Documento n�o pode ser nulo.");
		
		return query()
				.select("ordemcompra.cdordemcompra, listaOrdemcompraentrega.cdordemcompraentrega, entrega.cdentrega, " +
						"listaDocumentoorigem.cddocumentoorigem, documento.cddocumento, " +
						"listaMaterial.valor, listaMaterial.icmsissincluso, listaMaterial.icmsiss, listaMaterial.ipiincluso, " +
						"listaMaterial.ipi, ordemcompra.valoricmsst, ordemcompra.frete, ordemcompra.desconto ")
				.join("ordemcompra.listaMaterial listaMaterial")
				.join("ordemcompra.listaOrdemcompraentrega listaOrdemcompraentrega")
				.join("listaOrdemcompraentrega.entrega entrega")
				.join("entrega.listaDocumentoorigem listaDocumentoorigem")
				.join("listaDocumentoorigem.documento documento")
				.where("documento = ?", documento)
				.list();
	}

	/**
	 * M�todo que busca as ordem de compra para cria��o de contrato de fornecimento
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Ordemcompra> findForGerarfornecimento(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("ordemcompra.cdordemcompra, fornecedor.cdpessoa, fornecedor.nome, empresa.cdpessoa, " +
						"aux_ordemcompra.situacaosuprimentos, prazopagamento.nome, " +
						"listaMaterial.cdordemcompramaterial, listaMaterial.qtdpedida, listaMaterial.valor, listaMaterial.qtdefrequencia, " +
						"material.cdmaterial, material.nome, prazopagamento.cdprazopagamento, " +
						"rateio.cdrateio, listaRateioitem.cdrateioitem, listaRateioitem.percentual, listaRateioitem.valor, " +
						"contagerencial.cdcontagerencial, centrocusto.cdcentrocusto, projeto.cdprojeto")
				.leftOuterJoin("ordemcompra.aux_ordemcompra aux_ordemcompra")
				.leftOuterJoin("ordemcompra.fornecedor fornecedor")
				.leftOuterJoin("ordemcompra.empresa empresa")
				.leftOuterJoin("ordemcompra.prazopagamento prazopagamento")
				.leftOuterJoin("ordemcompra.listaMaterial listaMaterial")
				.leftOuterJoin("listaMaterial.material material")
				.leftOuterJoin("ordemcompra.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.leftOuterJoin("listaRateioitem.contagerencial contagerencial")
				.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
				.leftOuterJoin("listaRateioitem.projeto projeto")
				.whereIn("ordemcompra.cdordemcompra", whereIn)
				.list();
	}

	/**
	 * M�todo que atualiza o campo dtbaixa da ordem de compra
	 *
	 * @param whereIn
	 * @param dtbaixa
	 * @author Luiz Fernando
	 */
	public void updateDtbaixa(String whereIn, Date dtbaixa) {
		if(whereIn != null && !"".equals(whereIn)){
			getHibernateTemplate().bulkUpdate("update Ordemcompra oc set oc.dtbaixa = ? where oc.cdordemcompra in (" + whereIn + ") ",
					new Object[] { dtbaixa });
		}
		
	}

	/**
	 * M�todo que busca a ordem de compra com as origens
	 * @param whereInOrdemcompra
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Ordemcompra> findWithOrigem(String whereInOrdemcompra) {
		if(whereInOrdemcompra == null || "".equals(whereInOrdemcompra))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("ordemcompra.cdordemcompra, listaMaterial.cdordemcompramaterial, " +
						"listaOrdemcompraorigem.cdordemcompraorigem, solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.identificador, " +
						"cotacao.cdcotacao, listaOrigem.cdcotacaoorigem, material.cdmaterial, solicitacaocompra.faturamentocliente, " +
						"solicitacaocompraCotacao.cdsolicitacaocompra, solicitacaocompraCotacao.identificador, " +
						"materialCotacao.cdmaterial, solicitacaocompraCotacao.faturamentocliente ")
				.join("ordemcompra.listaMaterial listaMaterial")
				.leftOuterJoin("ordemcompra.listaOrdemcompraorigem listaOrdemcompraorigem")
				.leftOuterJoin("listaOrdemcompraorigem.solicitacaocompra solicitacaocompra")
				.leftOuterJoin("solicitacaocompra.material material")
				.leftOuterJoin("ordemcompra.cotacao cotacao")
				.leftOuterJoin("cotacao.listaOrigem listaOrigem")
				.leftOuterJoin("listaOrigem.solicitacaocompra solicitacaocompraCotacao")
				.leftOuterJoin("solicitacaocompraCotacao.material materialCotacao")
				.whereIn("ordemcompra.cdordemcompra", whereInOrdemcompra)
				.list();
	}

	
	/**
	 * M�todo que busca campos para nova ordem compra com base na origen do pedido
	 * @param whereIn
	 * @return List<Ordemcompra>
	 * @author Yago Silvestre
	 */
	public List<Ordemcompra> findOrigem(String whereIn ) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("ordemcompra.cdordemcompra,ordemcompra.observacoesinternas,ordemcompra.observacao")	
				.whereIn("ordemcompra.cdordemcompra", whereIn)
				.list();
	}
	
	
	
	
	/**
	 * M�todo que carrega a ordem de compra para gerar antecipa��o
	 *
	 * @param ordemcompra
	 * @return
	 * @author Luiz Fernando
	 * @since 25/10/2013
	 */
	public Ordemcompra loadForGerarAntecipacao(Ordemcompra ordemcompra) {
		if(ordemcompra == null || ordemcompra.getCdordemcompra() == null)
			throw new SinedException("Ordem de compra n�o pode ser nula.");
		
		QueryBuilder<Ordemcompra> query = query();
		this.updateEntradaQuery(query);
		query.where("ordemcompra = ?", ordemcompra);
		
		return query.unique();
	}

	public Double getQteOrdemBaixadaByPedidovenda(Pedidovenda pedidovenda) {
		SinedUtil.markAsReader();
		Long qtde = newQueryBuilder(Long.class)
			.select("count(*)")
			.from(Ordemcompra.class)
			.join("ordemcompra.listaMaterial listaMaterial")
			.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
			.where("listaMaterial.pedidovenda = ?", pedidovenda)
			.where("aux_ordemcompra.situacaosuprimentos = ?", Situacaosuprimentos.BAIXADA)
			.unique();
		return qtde != null ? qtde.doubleValue() : 0D;
	}
	
	public Double getQteOrdemByPedidovenda(Pedidovenda pedidovenda) {
		SinedUtil.markAsReader();
		Long qtde = newQueryBuilder(Long.class)
			.select("count(*)")
			.from(Ordemcompra.class)
			.join("ordemcompra.listaMaterial listaMaterial")
			.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
			.where("listaMaterial.pedidovenda = ?", pedidovenda)
			.where("aux_ordemcompra.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
			.unique();
		return qtde != null ? qtde.doubleValue() : 0D;
	}

	public Double getQteOrdemBaixadaByOrcamento(Vendaorcamento vendaorcamento) {
		SinedUtil.markAsReader();
		Long qtde = newQueryBuilder(Long.class)
			.select("count(*)")
			.from(Ordemcompra.class)
			.join("ordemcompra.listaMaterial listaMaterial")
			.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
			.where("listaMaterial.vendaorcamento = ?", vendaorcamento)
			.where("aux_ordemcompra.situacaosuprimentos = ?", Situacaosuprimentos.BAIXADA)
			.unique();
		return qtde != null ? qtde.doubleValue() : 0D;
	}

	public Double getQteOrdemByOrcamento(Vendaorcamento vendaorcamento) {
		SinedUtil.markAsReader();
		Long qtde = newQueryBuilder(Long.class)
				.select("count(*)")
				.from(Ordemcompra.class)
				.join("ordemcompra.listaMaterial listaMaterial")
				.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
				.join("listaMaterial.vendaorcamento vendaorcamento")
				.where("listaMaterial.vendaorcamento = ?", vendaorcamento)
				.where("aux_ordemcompra.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
				.unique();
		return qtde != null ? qtde.doubleValue() : 0D;
	}

	public void salvarCompraantecipada(Ordemcompra ordemcompra) {
		getJdbcTemplate().update("update Ordemcompra ordemcompra set ordemcompraantecipada = ? where cdordemcompra = ?" , new Object[]{ordemcompra.getOrdemcompraantecipada(), ordemcompra.getCdordemcompra()});
		
	}

	/**
	 * M�todo que retorna true caso a ordem de compra tenha um material que fa�a parte de uma grade
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 27/05/2014
	 */
	public Boolean existItemGrade(String whereIn) {
		if(StringUtils.isBlank(whereIn)) 
			return false;
		
		String sql = " select count(*) " +
					 " from Ordemcompramaterial ocm " +
					 " join Material m on m.cdmaterial = ocm.cdmaterial " +
					 " where ocm.cdordemcompra in (" + whereIn + ") " +
					 " and m.cdmaterialmestregrade is not null ";

		SinedUtil.markAsReader();
		Long qtde = getJdbcTemplate().queryForLong(sql);
		return qtde != null && qtde > 0;
	}
	
	/**
	 * M�todo que vincula PessoaQuestionario a Ordemcompra
	 * 
	 * @param ordemcompra
	 * @return
	 * @author Jo�o Vitor
	 * @since 13/10/2014
	 */
	public void vinculaPessoaQuestionarioAOrdemCompra(Ordemcompra ordemcompra){
		getJdbcTemplate().update("update Ordemcompra ordemcompra set cdpessoaquestionario = ? where cdordemcompra = ?" , new Object[]{ordemcompra.getPessoaquestionario().getCdpessoaquestionario(), ordemcompra.getCdordemcompra()});
	}
	
	/**
	 * M�todo que apaga o vinculo entre PessoaQuestionario a Ordemcompra
	 * 
	 * @param ordemcompra
	 * @return
	 * @author Jo�o Vitor
	 * @since 13/10/2014
	 */
	public void deleteVinculoPessoaQuestionarioAOrdemCompra(Ordemcompra ordemcompra){
		if (ordemcompra == null || ordemcompra.getCdordemcompra() == null) {
			throw new SinedException("Ordem de compra n�o pode ser nula.");
		}
		getJdbcTemplate().update("update Ordemcompra ordemcompra set cdpessoaquestionario = NULL where cdordemcompra = ?" , new Object[]{ordemcompra.getCdordemcompra()});
	}
	
	public List<Ordemcompra> buscarOrdemcompraCriarFluxocaixa(Date dtInicio, Date dtFim, String whereIn) {
		if((dtInicio != null && dtFim != null && SinedDateUtils.afterOrEqualsIgnoreHour(dtFim, dtInicio)) ||
				StringUtils.isNotEmpty(whereIn))
			return new ArrayList<Ordemcompra>();
		
		return query()
				.select("ordemcompra.cdordemcompra")
				.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
				.where("ordemcompra.dtcriacao >= ?", dtInicio)
				.where("ordemcompra.dtcriacao <= ?", dtFim)
				.where("aux_ordemcompra.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
				.where("aux_ordemcompra.situacaosuprimentos <> ?", Situacaosuprimentos.EM_ABERTO)
				.where("not exists ( select oc.cdordemcompra " +
									"from Fluxocaixaorigem fco " +
									"join fco.ordemcompra oc " +
									"where oc.cdordemcompra = ordemcompra.cdordemcompra) ")
				.whereIn("ordemcompra.cdordemcompra", whereIn)
				.setMaxResults(30)		
				.list()
				;
	}
	
	
	public Ordemcompra findOrdemcompraAvaliacao(Ordemcompra ordemcompra) {
		if(ordemcompra == null || ordemcompra.getCdordemcompra() == null)
			return null;
		
		return query()
				.select("ordemcompra.cdordemcompra, pessoaquestionario.cdpessoaquestionario, fornecedor.cdpessoa")
				.join("ordemcompra.pessoaquestionario pessoaquestionario")
				.join("pessoaquestionario.questionario questionario")
				.join("pessoaquestionario.pessoa fornecedor")
				.where("ordemcompra = ?", ordemcompra)
				.where("questionario.tipopessoaquestionario = ?", Tipopessoaquestionario.FORNECEDOR)
				.unique();
	}
	
	public Ordemcompra findFornecedorForOrdemCompra(Ordemcompra ordemcompra) {
		if(ordemcompra == null || ordemcompra.getCdordemcompra() == null) 
			return null;
		
		return query()
				.select("ordemcompra.cdordemcompra, fornecedor.cdpessoa")
				.join("ordemcompra.fornecedor fornecedor")
				.where("ordemcompra = ?", ordemcompra)
				.unique();
	}
	
	/**
	* M�todo que carrega a ordem de compra com os itens do recebimento
	*
	* @param whereIn
	* @return
	* @since 01/09/2015
	* @author Luiz Fernando
	*/
	public List<Ordemcompra> findForBaixar(String whereIn){
		return query()
			.select("ordemcompra.cdordemcompra, " +
					"listaMaterial.cdordemcompramaterial, listaMaterial.qtdpedida, unidademedidaOCM.cdunidademedida, " +
					"material.cdmaterial, unidademedida.cdunidademedida, " +
					"listaOrdemcompraentregamaterial.cdordemcompraentregamaterial, listaOrdemcompraentregamaterial.qtde, " +
					"entregamaterial.cdentregamaterial, unidademedidacomercial.cdunidademedida, " +
					"entrega.cdentrega, aux_entrega.situacaosuprimentos")
			.join("ordemcompra.listaMaterial listaMaterial")
			.join("listaMaterial.material material")
			.join("material.unidademedida unidademedida")
			.join("listaMaterial.unidademedida unidademedidaOCM")
			.leftOuterJoin("listaMaterial.listaOrdemcompraentregamaterial listaOrdemcompraentregamaterial")
			.leftOuterJoin("listaOrdemcompraentregamaterial.entregamaterial entregamaterial")
			.leftOuterJoin("entregamaterial.unidademedidacomercial unidademedidacomercial")
			.leftOuterJoin("entregamaterial.entregadocumento entregadocumento")
			.leftOuterJoin("entregadocumento.entrega entrega")
			.leftOuterJoin("entrega.aux_entrega aux_entrega")
			.whereIn("ordemcompra.cdordemcompra", whereIn)
			.list();
				
	}
	
	/**
	* M�todo que verifica se existe ordem de compra diferente da situa��o passada por par�metro
	*
	* @param whereIn
	* @param situacaosuprimentos
	* @return
	* @since 01/09/2015
	* @author Luiz Fernando
	*/
	public Boolean existeSituacaoDiferente(String whereIn, Situacaosuprimentos situacaosuprimentos) {
		if(StringUtils.isEmpty(whereIn)){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Ordemcompra.class)
			.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
			.whereIn("ordemcompra.cdordemcompra", whereIn)
			.where("aux_ordemcompra.situacaosuprimentos <> ?", situacaosuprimentos)
			.unique() > 0;
	}
	
	/**
	* M�todo que retorna as ordens de compra para solicitar quantidade restante
	*
	* @param whereIn
	* @return
	* @since 01/09/2015
	* @author Luiz Fernando
	*/
	public List<Ordemcompra> findForSolicitarrestante(String whereIn) {
		if(StringUtils.isEmpty(whereIn)){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("ordemcompra.cdordemcompra, ordemcompra.dtcriacao, ordemcompra.tipofrete, " +
						"listaMaterial.cdordemcompramaterial, listaMaterial.qtdpedida, listaMaterial.valor, listaMaterial.qtdefrequencia, " +
						"material.cdmaterial, material.nome, material.produto, material.epi, material.patrimonio, material.servico, " +
						"unidademedidaM.cdunidademedida, unidademedidaM.nome, unidademedidaM.simbolo, " +
						"contagerencial.cdcontagerencial, contagerencial.nome, " +
						"vcontagerencial.identificador, vcontagerencial.arvorepai, " +
						"centrocusto.cdcentrocusto, centrocusto.nome, " +
						"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo," +
						"localarmazenagemItem.cdlocalarmazenagem, localarmazenagemItem.nome, " +
						
						"cotacao.cdcotacao, listaCotacaofornecedor.cdcotacaofornecedor, fornecedorC.cdpessoa, " +
						"listaCotacaofornecedoritem.cdcotacaofornecedoritem, " +
						"materialCFI.cdmaterial, " +
						"listaCotacaofornecedoritemsolicitacaocompra.cdcotacaofornecedoritemsolicitacaocompra, " +
						"solicitacaocompraCFISC.cdsolicitacaocompra, solicitacaocompraCFISC.descricao, " +
						
						"listaOrdemcompraorigem.cdordemcompraorigem," +
						"solicitacaocompraOCO.cdsolicitacaocompra, solicitacaocompraOCO.descricao," +
						"materialOCO.cdmaterial," +
						"projeto.cdprojeto,listaMaterial.observacao,"+
						"fornecedor.cdpessoa, fornecedor.nome, " +
						"contato.cdpessoa, contato.nome," +
						"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
						"empresa.cdpessoa, empresa.nome, " +
						"documentotipo.cddocumentotipo, documentotipo.nome, " +
						"transportador.cdpessoa, transportador.nome, " +
						"garantia.cdgarantia, garantia.descricao, " +
						"prazopagamento.cdprazopagamento, prazopagamento.nome ")
				.join("ordemcompra.listaMaterial listaMaterial")
				.join("listaMaterial.material material")
				.join("material.unidademedida unidademedidaM")
				.leftOuterJoin("listaMaterial.contagerencial contagerencial")
				.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
				.leftOuterJoin("listaMaterial.centrocusto centrocusto")
				.leftOuterJoin("listaMaterial.unidademedida unidademedida")
				.leftOuterJoin("listaMaterial.localarmazenagem localarmazenagemItem")
				.leftOuterJoin("listaMaterial.projeto projeto")
				.leftOuterJoin("ordemcompra.cotacao cotacao")
				.leftOuterJoin("cotacao.listaCotacaofornecedor listaCotacaofornecedor")
				.leftOuterJoin("listaCotacaofornecedor.fornecedor fornecedorC")
				.leftOuterJoin("listaCotacaofornecedor.listaCotacaofornecedoritem listaCotacaofornecedoritem")
				.leftOuterJoin("listaCotacaofornecedoritem.material materialCFI")
				.leftOuterJoin("listaCotacaofornecedoritem.listaCotacaofornecedoritemsolicitacaocompra listaCotacaofornecedoritemsolicitacaocompra")
				.leftOuterJoin("listaCotacaofornecedoritemsolicitacaocompra.solicitacaocompra solicitacaocompraCFISC")
				
				.leftOuterJoin("ordemcompra.listaOrdemcompraorigem listaOrdemcompraorigem")
				.leftOuterJoin("listaOrdemcompraorigem.solicitacaocompra solicitacaocompraOCO")
				.leftOuterJoin("solicitacaocompraOCO.material materialOCO")
				
				.leftOuterJoin("ordemcompra.fornecedor fornecedor")
				.leftOuterJoin("ordemcompra.contato contato")
				.leftOuterJoin("ordemcompra.localarmazenagem localarmazenagem")
				.leftOuterJoin("ordemcompra.empresa empresa")
				.leftOuterJoin("ordemcompra.documentotipo documentotipo")
				.leftOuterJoin("ordemcompra.transportador transportador")
				.leftOuterJoin("ordemcompra.garantia garantia")
				.leftOuterJoin("ordemcompra.prazopagamento prazopagamento")
				
				.whereIn("ordemcompra.cdordemcompra", whereIn)
				.list();
	}
	
	/**
	* M�todo que faz o update da cota��o na ordem de compra
	*
	* @param ordemcompra
	* @param cotacao
	* @since 02/09/2015
	* @author Luiz Fernando
	*/
	public void updateVinculoCotacao(Ordemcompra ordemcompra, Cotacao cotacao) {
		if(ordemcompra != null && ordemcompra.getCdordemcompra() != null && cotacao != null){
			String sql = "update Ordemcompra o set o.cotacao = ? where o.cdordemcompra = ? ";
			getHibernateTemplate().bulkUpdate(sql, new Object[]{cotacao, ordemcompra.getCdordemcompra()});
		}
	}
	
	/**
	* M�todo que busca a ordem de compra que n�o est�o canceladas
	*
	* @param ids
	* @param situacaosuprimentos
	* @return
	* @since 14/09/2015
	* @author Luiz Fernando
	*/
	public List<Ordemcompra> findOrdemcompraEmAberto(String ids, Material material) {
		if (ids == null || ids.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		return query()
					.select("ordemcompra.cdordemcompra, aux_ordemcompra.situacaosuprimentos")
					.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
					.join("ordemcompra.listaMaterial listaMaterial")
					.whereIn("ordemcompra.cdordemcompra", ids)
					.where("aux_ordemcompra.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
					.where("aux_ordemcompra.situacaosuprimentos <> ?", Situacaosuprimentos.BAIXADA)
					.where("listaMaterial.material = ?", material)
					.list();
	}
	
	public List<Ordemcompra> findOrdemCompraWithEmpresa(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		return query()
			.select("ordemcompra.cdordemcompra, empresa.cdpessoa, empresa.nome ")
			.leftOuterJoin("ordemcompra.empresa empresa")
			.whereIn("ordemcompra.cdordemcompra", whereIn)
			.list();
	}
	
	public List<Ordemcompra> findByAtrasado(Date data, Date dateToSearch) {
		return querySined()
				.setUseWhereClienteEmpresa(false)
				.setUseWhereEmpresa(false)
				.setUseWhereFornecedorEmpresa(false)
				.setUseWhereProjeto(false)
				.select("ordemcompra.cdordemcompra, empresa.cdpessoa")
				.leftOuterJoin("ordemcompra.aux_ordemcompra aux_ordemcompra")
				.leftOuterJoin("ordemcompra.empresa empresa")
				.where("aux_ordemcompra.situacaosuprimentos = ?", Situacaosuprimentos.EM_ABERTO)
				.where("ordemcompra.dtproximaentrega < ?", data)
				.wherePeriodo("ordemcompra.dtproximaentrega", dateToSearch, SinedDateUtils.currentDate())
				.list();
	}
	
	public List<Ordemcompra> findByEnviarPedidoCompra(Date data, Date dateToSearch) {
		return querySined()
				.setUseWhereClienteEmpresa(false)
				.setUseWhereEmpresa(false)
				.setUseWhereFornecedorEmpresa(false)
				.setUseWhereProjeto(false)
				.select("ordemcompra.cdordemcompra, empresa.cdpessoa")
				.leftOuterJoin("ordemcompra.aux_ordemcompra aux_ordemcompra")
				.leftOuterJoin("ordemcompra.empresa empresa")
				.where("aux_ordemcompra.situacaosuprimentos = ?", Situacaosuprimentos.AUTORIZADA)
				.wherePeriodo("ordemcompra.dtcriacao", dateToSearch, SinedDateUtils.currentDate())
				.list();
	}
	
	/**
	 * Busca todas as ordems de compras relacionadas a uma entrega.
	 * @param entrega
	 * @return
	 */
	public List<Ordemcompra> getOrdemcompraPorEntrega(Entrega entrega) {
		if(entrega == null || entrega.getCdentrega() == null)
			throw new IllegalArgumentException("O par�metro entrega n�o pode ser nulo");
		return query()
				.select("ordemcompra.cdordemcompra")
				.leftOuterJoin("ordemcompra.listaOrdemcompraentrega ordemcompraentrega")
				.leftOuterJoin("ordemcompraentrega.entrega entrega")
				.where("entrega = ?", entrega)
				.list();
	}
}
