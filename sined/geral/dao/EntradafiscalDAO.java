package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Ordemcompraentregamaterial;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.SIntegraNaturezaoperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoiss;
import br.com.linkcom.sined.geral.service.OrdemcompraentregamaterialService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.Faixaimpostoapuracao;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.ApuracaoimpostoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.EntradafiscalFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SintegraFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedpiscofinsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaoicmsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaoipiFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaopiscofinsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.SagefiscalFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EntradafiscalDAO extends GenericDAO<Entregadocumento> {
	
	private RateioService rateioService;
	private OrdemcompraentregamaterialService ordemcompraentregamaterialService;
	private UsuarioService usuarioService;
	private ParametrogeralService parametrogeralService;
	
	public void setOrdemcompraentregamaterialService(OrdemcompraentregamaterialService ordemcompraentregamaterialService) {
		this.ordemcompraentregamaterialService = ordemcompraentregamaterialService;
	}
	public void setRateioService(RateioService rateioService) {	
		this.rateioService = rateioService;	
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Entregadocumento> query, FiltroListagem _filtro) {
		
		EntradafiscalFiltro filtro = (EntradafiscalFiltro) _filtro;
		
		filtro.setPopupCTRC("true".equalsIgnoreCase(NeoWeb.getRequestContext().getParameter("popupCTRC")));
		
		query
			.select("distinct entregadocumento.cdentregadocumento, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, fornecedor.cdpessoa, fornecedor.nome, " +
					"entregadocumento.dtentrada, entregadocumento.dtemissao, entregadocumento.numero, entregadocumento.valor," +
					"entregadocumento.situacaodocumento, modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.nome, entregadocumento.diferencaduplicata")
			.join("entregadocumento.fornecedor fornecedor")
			.leftOuterJoin("entregadocumento.empresa empresa")
			.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
			.where("entregadocumento.dtentrada >= ?",filtro.getDtentrada1())
			.where("entregadocumento.dtentrada <= ?",filtro.getDtentrada2())
			.where("entregadocumento.dtemissao >= ?",filtro.getDtemissao1())
			.where("entregadocumento.dtemissao <= ?",filtro.getDtemissao2())
			.where("entregadocumento.valor >= ?", filtro.getValordocumento1())
			.where("entregadocumento.valor <= ?", filtro.getValordocumento2())
			.where("entregadocumento.situacaodocumento = ?", filtro.getSituacaodocumento())
			.whereLikeIgnoreAll("entregadocumento.numero", filtro.getNumero())
			.where("entregadocumento.empresa = ?",filtro.getEmpresa())
			.where("entregadocumento.fornecedor = ?", filtro.getFornecedor())
			.where("entregadocumento.modelodocumentofiscal = ?", filtro.getModelodocumentofiscal())
			.orderBy("entregadocumento.cdentregadocumento desc")
			.ignoreJoin("rateio", "listaRateioitem", "projeto", "listaEntregamaterial", "loteestoque");
		
		if(filtro.getDocumentotipo() != null){
			query.where("entregadocumento.documentotipo = ?", filtro.getDocumentotipo());
		}
		
		if(filtro.getProjeto() != null ){
			query
				.leftOuterJoin("entregadocumento.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.where("listaRateioitem.projeto = ?", filtro.getProjeto());
		}
		
		if(filtro.getLoteestoque() != null || filtro.getCfop() != null || filtro.getMaterial() != null){
			query
				.leftOuterJoin("entregadocumento.listaEntregamaterial listaEntregamaterial")
				.where("listaEntregamaterial.loteestoque = ?",filtro.getLoteestoque())
				.where("listaEntregamaterial.material = ?",filtro.getMaterial())
				.where("listaEntregamaterial.cfop = ?", filtro.getCfop());
		}
		
		if (filtro.getListaSituacao() != null) {
			query.whereIn("entregadocumento.entregadocumentosituacao", Entregadocumentosituacao.listAndConcatenate(filtro.getListaSituacao()));
		}
		
		if (filtro.isPopupCTRC()){
			query.openParentheses();
			query.where("modelodocumentofiscal.ctrc is null").or();
			query.where("modelodocumentofiscal.ctrc = ?", Boolean.FALSE);
			query.closeParentheses();
		}
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioService.isRestricaoFornecedorColaborador(usuarioLogado)){
			query.where("("+
							"exists("+
								"select 1 from ColaboradorFornecedor cf "+
								"where "+
								"cf.fornecedor =  fornecedor.cdpessoa and "+
								"cf.colaborador = "+usuarioLogado.getCdpessoa()+
							") "+
							"or "+
							"not exists("+
								"select 1 from ColaboradorFornecedor cf "+
								"where "+
								"cf.fornecedor =  fornecedor.cdpessoa"+
							")"+
						")");
		}
		
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Entregadocumento> query) {
		query		
			.select("entregadocumento.cdentregadocumento, entregadocumento.dtentrada, entregadocumento.dtemissao, " +
					"entregadocumento.numero, entregadocumento.especie, entregadocumento.serie, entregadocumento.situacaodocumento, " +
					"entregadocumento.natop, naturezaoperacao.cdnaturezaoperacao, entregadocumento.indpag, " +
					"entregadocumento.valoroutrasdespesas, entregadocumento.valordesconto, entregadocumento.valorfrete, " +
					"entregadocumento.valoricmsst, entregadocumento.valoripi, entregadocumento.valorfretepeso, " +
					"entregadocumento.valortaxacoleta, entregadocumento.valortaxaentrega, entregadocumento.valorseccat, " +
					"entregadocumento.valorpedagio, entregadocumento.emitente, entregadocumento.vincularOutrosDocumentos, " +
					"entregadocumento.infoadicionalfisco, entregadocumento.infoadicionalcontrib, entregadocumento.transportadora, " +
					"entregadocumento.placaveiculo, entregadocumento.valorseguro, entregadocumento.valor, entregadocumento.chaveacesso, " +
					"entregadocumento.entregadocumentosituacao, entregadocumento.tipotitulo, entregadocumento.numerotitulo, " +
					"entregadocumento.descricaotitulo, entregadocumento.simplesremessa, entregadocumento.retornowms, " +
					"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.nome, modelodocumentofiscal.codigo, " +
					"modelodocumentofiscal.enviospedfiscal, modelodocumentofiscal.enviospedpiscofins, modelodocumentofiscal.ctrc, " +
					"modelodocumentofiscal.obrigartitulo, modelodocumentofiscal.obrigarchaveacesso, " +
					"municipioorigem.cdmunicipio, municipioorigem.nome, uforigem.cduf, uforigem.sigla, entregadocumento.municipioorigemexterior, " +
					"municipiodestino.cdmunicipio, municipiodestino.nome, ufdestino.cduf, ufdestino.sigla, entregadocumento.municipiodestinoexterior, " +
					"entrega.cdentrega, entrega.faturamentocliente, " +
					"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.geracaospedpiscofins, empresa.geracaospedicmsipi, empresa.presencacompradornfe, " +
					"fornecedor.cdpessoa,fornecedor.nome, " +
					"responsavelFrete.cdResponsavelFrete, responsavelFrete.nome, responsavelFrete.cdnfe, " +
					"rateio.cdrateio, listaRateioitem.cdrateioitem, listaRateioitem.percentual, listaRateioitem.valor, " +
					"contagerencial.cdcontagerencial, contagerencial.nome, centrocusto.cdcentrocusto, centrocusto.nome, " +
					"projeto.cdprojeto, projeto.nome, " +
					"documentotipo.cddocumentotipo, documentotipo.nome, " +
					"listaEntregamaterial.cdentregamaterial, listaEntregamaterial.identificadorinterno, listaEntregamaterial.qtde, listaEntregamaterial.comprimento, " +
					"listaEntregamaterial.valorunitario, listaEntregamaterial.cdusuarioaltera," +
					"listaEntregamaterial.dtaltera,listaEntregamaterial.cprod,listaEntregamaterial.xprod," +
					"listaEntregamaterial.valorproduto,listaEntregamaterial.valordesconto, " +
					"listaEntregamaterial.valorfrete,listaEntregamaterial.valorseguro, " +
					"listaEntregamaterial.valoroutrasdespesas,listaEntregamaterial.csticms, " +
					"listaEntregamaterial.valorbcicms,listaEntregamaterial.icms,listaEntregamaterial.valoricms, " +
					"listaEntregamaterial.valorbcicmsst,listaEntregamaterial.icmsst,listaEntregamaterial.valoricmsst," +
					"listaEntregamaterial.valorbcfcp,listaEntregamaterial.fcp,listaEntregamaterial.valorfcp, " +
					"listaEntregamaterial.valorbcfcpst,listaEntregamaterial.fcpst,listaEntregamaterial.valorfcpst," +
					"listaEntregamaterial.valormva,listaEntregamaterial.naoconsideraricmsst,listaEntregamaterial.cstipi," +
					"listaEntregamaterial.valorbcipi,listaEntregamaterial.ipi,listaEntregamaterial.valoripi," +
					"listaEntregamaterial.cstpis,listaEntregamaterial.valorbcpis,listaEntregamaterial.pis," +
					"listaEntregamaterial.pisretido,listaEntregamaterial.qtdebcpis,listaEntregamaterial.valorunidbcpis," +
					"listaEntregamaterial.valorpis,listaEntregamaterial.valorpisretido,listaEntregamaterial.cstcofins," +
					"listaEntregamaterial.valorbccofins,listaEntregamaterial.cofins,listaEntregamaterial.cofinsretido," +
					"listaEntregamaterial.qtdebccofins,listaEntregamaterial.valorunidbccofins,listaEntregamaterial.valorcofins," +
					"listaEntregamaterial.valorcofinsretido,listaEntregamaterial.valorcsll,listaEntregamaterial.valorir," +
					"listaEntregamaterial.valorinss,listaEntregamaterial.valorimpostoimportacao,listaEntregamaterial.valorbciss," +
					"listaEntregamaterial.iss,listaEntregamaterial.valoriss,listaEntregamaterial.tipotributacaoiss," +
					"listaEntregamaterial.clistservico,listaEntregamaterial.naturezabcc,listaEntregamaterial.valorFiscalIcms," +
					"listaEntregamaterial.valorFiscalIpi, listaEntregamaterial.faturamentocliente, listaEntregamaterial.sequenciaitem, "+
					"listaEntregamaterial.valorbcii, listaEntregamaterial.valoriiItem, listaEntregamaterial.pesomedio, "+
					"listaEntregamaterial.valorDespesasAduaneiras, listaEntregamaterial.iof, listaEntregamaterial.tipolote, " +
					"listaEntregamaterial.valorIcmsDesonerado, listaEntregamaterial.motivoDesoneracaoIcms, listaEntregamaterial.origemProduto, " +
					"listaAjustefiscal.cdajustefiscal, " +
					"listaAjustefiscal.codigoajuste," +
					"listaAjustefiscal.descricaocomplementar, listaAjustefiscal.aliquotaicms, " +
					"listaAjustefiscal.basecalculoicms, listaAjustefiscal.valoricms, " +
					"listaAjustefiscal.outrosvalores," +
					"obslancamentofiscal.cdobslancamentofiscal, obslancamentofiscal.descricao, " +
					"materialajuste.cdmaterial, materialajuste.nome, materialajuste.identificacao, "+
					"material.cdmaterial, material.nome, material.identificacao, material.nomenf, material.valorcusto, " +
					"materialclasse.cdmaterialclasse, materialclasse.nome, " +
					"unidademedida.cdunidademedida, unidademedida.nome, " +
					"cfop.cdcfop, cfop.codigo, cfop.descricaoresumida, cfop.descricaocompleta, cfop.naoconsiderarreceita, " +
					"unidademedidacomercial.cdunidademedida, unidademedidacomercial.nome, unidademedidacomercial.simbolo," +
					"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
					"loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade," +
					"projetoentregamaterial.cdprojeto, projetoentregamaterial.nome, " +
					"centrocustoEntregamaterial.cdcentrocusto, centrocustoEntregamaterial.nome, " +
					"municipioiss.cdmunicipio, municipioiss.nome, uf.cduf, uf.nome, " +
					"listadocumento.cdentregapagamento, listadocumento.numero, listadocumento.dtemissao, " +
					"listadocumento.dtvencimento, listadocumento.valor, listadocumento.cdusuarioaltera, " +
					"listadocumento.dtaltera, listadocumento.parcela, " +
					"documentoantecipacao.cddocumento, documentoantecipacao.descricao, " +
					"documentoorigem.cddocumento, documentoorigem.descricao, " +
					"listaEntregadocumentohistorico.cdentregadocumentohistorico, " +
					"listaEntregadocumentohistorico.observacao, listaEntregadocumentohistorico.entregadocumentosituacao, " +
					"prazopagamento.cdprazopagamento, prazopagamento.nome," +
					"listaOrdemcompraentregamaterial.cdordemcompraentregamaterial, " +
					"listaOrdemcompraentregamaterial.qtde, " +
					"listaInspecao.cdentregamaterialinspecao, listaInspecao.observacao," +
					"ordemcompramaterial.cdordemcompramaterial, ordemcompra.cdordemcompra, listaEntregamaterial.whereInMaterialItenGrade," +
					"grupotributacao.cdgrupotributacao, grupotributacao.nome, grupotributacao.formulabcst, grupotributacao.formulavalorst, " +
					"uficmsst.cduf, uficmsst.sigla, " +
					"pedidovendamaterial.cdpedidovendamaterial, " +
					"entregadocumentoreferenciado.cdentregadocumentoreferenciado, entregadocumentoreferenciado.modelodocumento, entregadocumentoreferenciado.imposto, " +
					"entregadocumentoreferenciado.identificadorinternoentregamaterial, " +
					"entregadocumentoreferenciado.numero, entregadocumentoreferenciado.autenticacaobancaria, entregadocumentoreferenciado.valor, " +
					"entregadocumentoreferenciado.dtvencimento, entregadocumentoreferenciado.dtpagamento, edruf.cduf, edruf.sigla, " +
					"material.ncmcompleto, ncmcapitulo.cdncmcapitulo, fornecedor.cnpj, materialgrupo.cdmaterialgrupo, materialgrupo.nome, materialgrupo.registrarpesomedio, entregadocumento.diferencaduplicata")
			.leftOuterJoin("entregadocumento.empresa empresa")
			.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
			.leftOuterJoin("entregadocumento.municipioorigem municipioorigem")
			.leftOuterJoin("municipioorigem.uf uforigem")
			.leftOuterJoin("entregadocumento.municipiodestino municipiodestino")
			.leftOuterJoin("municipiodestino.uf ufdestino")
			.leftOuterJoin("entregadocumento.fornecedor fornecedor")
			.leftOuterJoin("entregadocumento.responsavelfrete responsavelFrete")
			.leftOuterJoin("entregadocumento.rateio rateio")
			.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
			.leftOuterJoin("listaRateioitem.contagerencial contagerencial")
			.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
			.leftOuterJoin("listaRateioitem.projeto projeto")
			.leftOuterJoin("entregadocumento.entrega entrega")
			.leftOuterJoin("entregadocumento.naturezaoperacao naturezaoperacao")
			.leftOuterJoin("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")			
			.leftOuterJoin("entregadocumento.listadocumento listadocumento")
			.leftOuterJoin("listadocumento.documentoantecipacao documentoantecipacao")
			.leftOuterJoin("listadocumento.documentoorigem documentoorigem")
			.leftOuterJoin("entregadocumento.listaEntregamaterial listaEntregamaterial")
			.leftOuterJoin("listaEntregamaterial.material material")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("entregadocumento.prazopagamento prazopagamento")
			.leftOuterJoin("listaEntregamaterial.materialclasse materialclasse")
			.leftOuterJoin("listaEntregamaterial.listaInspecao listaInspecao")
			.leftOuterJoin("listaEntregamaterial.listaOrdemcompraentregamaterial listaOrdemcompraentregamaterial")
			.leftOuterJoin("listaOrdemcompraentregamaterial.ordemcompramaterial ordemcompramaterial")
			.leftOuterJoin("ordemcompramaterial.ordemcompra ordemcompra")
			.leftOuterJoin("listaEntregamaterial.projeto projetoentregamaterial")
			.leftOuterJoin("listaEntregamaterial.centrocusto centrocustoEntregamaterial")
			.leftOuterJoin("listaEntregamaterial.cfop cfop")
			.leftOuterJoin("listaEntregamaterial.municipioiss municipioiss")
			.leftOuterJoin("municipioiss.uf uf")
			.leftOuterJoin("listaEntregamaterial.loteestoque loteestoque")
			.leftOuterJoin("entregadocumento.listaEntregadocumentohistorico listaEntregadocumentohistorico")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
			.leftOuterJoin("entregadocumento.documentotipo documentotipo")
			.leftOuterJoin("listaEntregamaterial.unidademedidacomercial unidademedidacomercial")
			.leftOuterJoin("listaEntregamaterial.localarmazenagem localarmazenagem")
			.leftOuterJoin("listaEntregamaterial.grupotributacao grupotributacao")
			.leftOuterJoin("grupotributacao.uficmsst uficmsst")
			.leftOuterJoin("listaEntregamaterial.pedidovendamaterial pedidovendamaterial")
			.leftOuterJoin("entregadocumento.listaEntregadocumentoreferenciado entregadocumentoreferenciado")
			.leftOuterJoin("entregadocumentoreferenciado.uf edruf")
			.leftOuterJoin("entregadocumento.listaAjustefiscal listaAjustefiscal")
			.leftOuterJoin("listaAjustefiscal.obslancamentofiscal obslancamentofiscal")
			.leftOuterJoin("listaAjustefiscal.material materialajuste")
			.orderBy("listaEntregamaterial.sequenciaitem asc, listadocumento.parcela")
			;
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Entregadocumento entregadocumento = (Entregadocumento) save.getEntity();
		
		if(entregadocumento.getListaEntregamaterial() != null){
			for (Entregamaterial em : entregadocumento.getListaEntregamaterial()) {
				if(em.getCdentregamaterial() != null){
					Set<Ordemcompraentregamaterial> listaOrdemcompraentregamaterial = em.getListaOrdemcompraentregamaterial();
					if(listaOrdemcompraentregamaterial != null && listaOrdemcompraentregamaterial.size() > 0){
						for (Ordemcompraentregamaterial ordemcompraentregamaterial : listaOrdemcompraentregamaterial) {
							ordemcompraentregamaterial.setEntregamaterial(em);
							ordemcompraentregamaterialService.saveOrUpdateNoUseTransaction(ordemcompraentregamaterial);
						}
					}
				}
			}
		}
		
		save
			.saveOrUpdateManaged("listaEntregamaterial")
			.saveOrUpdateManaged("listaAjustefiscal")
			.saveOrUpdateManaged("listadocumento")
			.saveOrUpdateManaged("listaEntregadocumentofreterateio")
			.saveOrUpdateManaged("listaEntregadocumentoreferenciado")
			.saveOrUpdateManaged("listaApropriacao");
		
		if(entregadocumento.getRateio() != null){
			rateioService.saveOrUpdateNoUseTransaction(entregadocumento.getRateio());
		}
		if(Hibernate.isInitialized(entregadocumento.getListaEntregadocumentohistorico()) && SinedUtil.isListNotEmpty(entregadocumento.getListaEntregadocumentohistorico())){
			save.saveOrUpdateManaged("listaEntregadocumentohistorico");
		}
		
//		Entregadocumento entregadocumento = (Entregadocumento) save.getEntity();
//		
//		if(entregadocumento.getListaEntregamaterial() != null){
//			if(entregadocumento.getCdentregadocumento() != null){
//				String whereNotIn = SinedUtil.listAndConcatenateIDs(entregadocumento.getListaEntregamaterial());
//				if(whereNotIn != null && !whereNotIn.trim().equals("")){
//					entregamaterialService.deleteAllFromEntregaNotInWhereIn(entregadocumento.getCdentregadocumento(), whereNotIn);
//				}
//			}
//			
//			for (Entregamaterial em : entregadocumento.getListaEntregamaterial()) {
//				entregamaterialService.saveOrUpdateNoUseTransaction(em);
//				if(em.getCdentregamaterial() != null){
//					Set<Ordemcompraentregamaterial> listaOrdemcompraentregamaterial = em.getListaOrdemcompraentregamaterial();
//					if(listaOrdemcompraentregamaterial != null && listaOrdemcompraentregamaterial.size() > 0){
//						for (Ordemcompraentregamaterial ordemcompraentregamaterial : listaOrdemcompraentregamaterial) {
//							ordemcompraentregamaterial.setEntregamaterial(em);
//							ordemcompraentregamaterialService.saveOrUpdateNoUseTransaction(ordemcompraentregamaterial);
//						}
//					}
//				}
//			}
//		}
//		
//		if(entregadocumento.getListadocumento() != null){
//			if(entregadocumento.getCdentregadocumento() != null){
//				String whereNotIn = SinedUtil.listAndConcatenateIDs(entregadocumento.getListadocumento());
//				if(whereNotIn != null && !whereNotIn.trim().equals("")){
//					entregapagamentoService.deleteAllFromEntregaNotInWhereIn(entregadocumento.getCdentregadocumento(), whereNotIn);
//				}
//			}
//			
//			for(Entregapagamento ep : entregadocumento.getListadocumento()){
//				entregapagamentoService.saveOrUpdateNoUseTransaction(ep);
//			}
//		}
//		
//		if(entregadocumento.getListaEntregadocumentohistorico() != null){
//			if(entregadocumento.getCdentregadocumento() != null){
//				String whereNotIn = SinedUtil.listAndConcatenateIDs(entregadocumento.getListaEntregadocumentohistorico());
//				if(whereNotIn != null && !whereNotIn.trim().equals("")){
//					entregadocumentohistoricoService.deleteAllFromEntregaNotInWhereIn(entregadocumento.getCdentregadocumento(), whereNotIn);
//				}
//			}
//			
//			for(Entregadocumentohistorico edh : entregadocumento.getListaEntregadocumentohistorico()){
//				entregadocumentohistoricoService.saveOrUpdateNoUseTransaction(edh);
//			}
//		}
//		
//		if(entregadocumento.getRateio() != null){
//			rateioService.saveOrUpdateNoUseTransaction(entregadocumento.getRateio());
//		}
	}

	public List<Entregadocumento> findEntregadocumentoByEntrega(Entrega entrega) {
		if(entrega == null || entrega.getCdentrega() == null)
			throw new SinedException("Entrega n�o pode ser nula.");
		
		QueryBuilder<Entregadocumento> query = query()				
			.select("entregadocumento.cdentregadocumento, entregadocumento.dtentrada, entregadocumento.dtemissao, " +
				"entregadocumento.numero, entregadocumento.especie, entregadocumento.serie, entregadocumento.situacaodocumento, " +
				"entregadocumento.natop, entregadocumento.indpag, entregadocumento.fornecedorAvaliado, " +
				"entregadocumento.valoroutrasdespesas, entregadocumento.valordesconto, entregadocumento.valorfrete, " +
				"entregadocumento.valoricmsst, entregadocumento.valoripi," +
				"entregadocumento.infoadicionalfisco, entregadocumento.infoadicionalcontrib, entregadocumento.transportadora, " +
				"entregadocumento.placaveiculo, entregadocumento.valorseguro, entregadocumento.valor, entregadocumento.chaveacesso, " +
				"entregadocumento.entregadocumentosituacao, entregadocumento.tipotitulo, entregadocumento.numerotitulo, " +
				"entregadocumento.descricaotitulo, entregadocumento.simplesremessa, entregadocumento.retornowms, " +
				"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.nome, " +
				"entrega.cdentrega, " +
				"empresa.cdpessoa, empresa.nome, empresa.razaosocial, " +
				"fornecedor.cdpessoa,fornecedor.nome, " +
				"responsavelFrete.cdResponsavelFrete, responsavelFrete.nome, responsavelFrete.cdnfe, " +
				"rateio.cdrateio, listaRateioitem.cdrateioitem, listaRateioitem.percentual, listaRateioitem.valor, " +
				"contagerencial.cdcontagerencial, contagerencial.nome, centrocusto.cdcentrocusto, centrocusto.nome, " +
				"projeto.cdprojeto, projeto.nome, " +
				"documentotipo.cddocumentotipo, documentotipo.nome, " +
				"listaEntregamaterial.cdentregamaterial, listaEntregamaterial.qtde, listaEntregamaterial.comprimento, listaEntregamaterial.tipolote, " +
				"listaEntregamaterial.valorunitario, listaEntregamaterial.cdusuarioaltera," +
				"listaEntregamaterial.dtaltera,listaEntregamaterial.cprod,listaEntregamaterial.xprod," +
				"listaEntregamaterial.valorproduto,listaEntregamaterial.valordesconto, " +
				"listaEntregamaterial.valorfrete,listaEntregamaterial.valorseguro, " +
				"listaEntregamaterial.valoroutrasdespesas,listaEntregamaterial.csticms, " +
				"listaEntregamaterial.valorbcicms,listaEntregamaterial.icms,listaEntregamaterial.valoricms, " +
				"listaEntregamaterial.valorbcicmsst,listaEntregamaterial.icmsst,listaEntregamaterial.valoricmsst," +
				"listaEntregamaterial.valormva,listaEntregamaterial.naoconsideraricmsst,listaEntregamaterial.cstipi," +
				"listaEntregamaterial.valorbcipi,listaEntregamaterial.ipi,listaEntregamaterial.valoripi," +
				"listaEntregamaterial.cstpis,listaEntregamaterial.valorbcpis,listaEntregamaterial.pis," +
				"listaEntregamaterial.pisretido,listaEntregamaterial.qtdebcpis,listaEntregamaterial.valorunidbcpis," +
				"listaEntregamaterial.valorpis,listaEntregamaterial.valorpisretido,listaEntregamaterial.cstcofins," +
				"listaEntregamaterial.valorbccofins,listaEntregamaterial.cofins,listaEntregamaterial.cofinsretido," +
				"listaEntregamaterial.qtdebccofins,listaEntregamaterial.valorunidbccofins,listaEntregamaterial.valorcofins," +
				"listaEntregamaterial.valorcofinsretido,listaEntregamaterial.valorcsll,listaEntregamaterial.valorir," +
				"listaEntregamaterial.valorinss,listaEntregamaterial.valorimpostoimportacao,listaEntregamaterial.valorbciss," +
				"listaEntregamaterial.iss,listaEntregamaterial.valoriss,listaEntregamaterial.tipotributacaoiss," +
				"listaEntregamaterial.clistservico,listaEntregamaterial.naturezabcc,listaEntregamaterial.valorFiscalIcms," +
				"listaEntregamaterial.valorFiscalIpi, listaEntregamaterial.pesomedio, " +
				"listaAjustefiscal.cdajustefiscal, "+
				"listaAjustefiscal.codigoajuste, " +
				"listaAjustefiscal.descricaocomplementar, listaAjustefiscal.aliquotaicms, " +
				"listaAjustefiscal.basecalculoicms, listaAjustefiscal.valoricms, " +
				"obslancamentofiscal.cdobslancamentofiscal, obslancamentofiscal.descricao, " +
				"listaAjustefiscal.outrosvalores, " +
				"materialajuste.cdmaterial, materialajuste.nome, materialajuste.identificacao, "+
				"material.cdmaterial, material.nome, material.identificacao, material.nomenf, " +
				"materialclasse.cdmaterialclasse, materialclasse.nome, " +
				"unidademedida.cdunidademedida, unidademedida.nome, " +
				"cfop.cdcfop, cfop.codigo, cfop.descricaoresumida, cfop.descricaocompleta, cfop.naoconsiderarreceita, " +
				"unidademedidacomercial.cdunidademedida, unidademedidacomercial.nome, unidademedidacomercial.simbolo," +
				"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
				"loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade," +
				"projetoentregamaterial.cdprojeto, projetoentregamaterial.nome, " +
				"centrocustoEntregamaterial.cdcentrocusto, centrocustoEntregamaterial.nome, " +
				"municipioiss.cdmunicipio, municipioiss.nome, uf.cduf, uf.nome, " +
				"listadocumento.cdentregapagamento, listadocumento.numero, listadocumento.dtemissao, " +
				"listadocumento.dtvencimento, listadocumento.valor, listadocumento.cdusuarioaltera, " +
				"listadocumento.dtaltera, listadocumento.parcela, " +
				"documentoantecipacao.cddocumento, documentoantecipacao.descricao, " +
				"documentoorigem.cddocumento, documentoorigem.descricao, " +
				"listaEntregadocumentohistorico.cdentregadocumentohistorico, " +
				"listaEntregadocumentohistorico.observacao, listaEntregadocumentohistorico.entregadocumentosituacao, " +
				"prazopagamento.cdprazopagamento, prazopagamento.nome," +
				"listaOrdemcompraentregamaterial.cdordemcompraentregamaterial, " +
				"listaOrdemcompraentregamaterial.qtde, ordemcompra.cdordemcompra, " +
				"listaInspecao.cdentregamaterialinspecao, listaInspecao.observacao," +
				"ordemcompramaterial.cdordemcompramaterial, listaEntregamaterial.whereInMaterialItenGrade," +
				"grupotributacao.cdgrupotributacao, " +
				"entregadocumentoreferenciado.cdentregadocumentoreferenciado, entregadocumentoreferenciado.modelodocumento, " +
				"entregadocumentoreferenciado.numero, entregadocumentoreferenciado.autenticacaobancaria, entregadocumentoreferenciado.valor, " +
				"entregadocumentoreferenciado.dtvencimento, entregadocumentoreferenciado.dtpagamento, edruf.cduf, edruf.sigla, "+
				"materialgrupo.cdmaterialgrupo, materialgrupo.registrarpesomedio, naturezaoperacao.cdnaturezaoperacao ")
		.leftOuterJoin("entregadocumento.empresa empresa")
		.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
		.leftOuterJoin("entregadocumento.fornecedor fornecedor")
		.leftOuterJoin("entregadocumento.responsavelfrete responsavelFrete")
		.leftOuterJoin("entregadocumento.rateio rateio")
		.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
		.leftOuterJoin("listaRateioitem.contagerencial contagerencial")
		.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
		.leftOuterJoin("listaRateioitem.projeto projeto")
		.leftOuterJoin("entregadocumento.entrega entrega")			
		.leftOuterJoin("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")			
		.leftOuterJoin("entregadocumento.listadocumento listadocumento")
		.leftOuterJoin("listadocumento.documentoantecipacao documentoantecipacao")
		.leftOuterJoin("listadocumento.documentoorigem documentoorigem")
		.join("entregadocumento.listaEntregamaterial listaEntregamaterial")
		.join("listaEntregamaterial.material material")
		.leftOuterJoin("material.materialgrupo materialgrupo")
		.leftOuterJoin("entregadocumento.prazopagamento prazopagamento")
		.leftOuterJoin("listaEntregamaterial.materialclasse materialclasse")
		.leftOuterJoin("listaEntregamaterial.listaInspecao listaInspecao")
		.leftOuterJoin("listaEntregamaterial.listaOrdemcompraentregamaterial listaOrdemcompraentregamaterial")
		.leftOuterJoin("listaOrdemcompraentregamaterial.ordemcompramaterial ordemcompramaterial")
		.leftOuterJoin("ordemcompramaterial.ordemcompra ordemcompra")
		.leftOuterJoin("listaEntregamaterial.projeto projetoentregamaterial")
		.leftOuterJoin("listaEntregamaterial.centrocusto centrocustoEntregamaterial")
		.leftOuterJoin("listaEntregamaterial.cfop cfop")
		.leftOuterJoin("listaEntregamaterial.municipioiss municipioiss")
		.leftOuterJoin("municipioiss.uf uf")
		.leftOuterJoin("listaEntregamaterial.loteestoque loteestoque")
		.leftOuterJoin("entregadocumento.listaEntregadocumentohistorico listaEntregadocumentohistorico")
		.leftOuterJoin("material.unidademedida unidademedida")
		.leftOuterJoin("entregadocumento.documentotipo documentotipo")
		.leftOuterJoin("listaEntregamaterial.unidademedidacomercial unidademedidacomercial")
		.leftOuterJoin("listaEntregamaterial.localarmazenagem localarmazenagem")
		.leftOuterJoin("listaEntregamaterial.grupotributacao grupotributacao")
		.leftOuterJoin("entregadocumento.listaEntregadocumentoreferenciado entregadocumentoreferenciado")
		.leftOuterJoin("entregadocumentoreferenciado.uf edruf")
		.leftOuterJoin("entregadocumento.listaAjustefiscal listaAjustefiscal")
		.leftOuterJoin("listaAjustefiscal.obslancamentofiscal obslancamentofiscal")
		.leftOuterJoin("listaAjustefiscal.material materialajuste")
		.leftOuterJoin("entregadocumento.naturezaoperacao naturezaoperacao")
		.where("entrega = ?", entrega);
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioService.isRestricaoFornecedorColaborador(usuarioLogado)){
			query.where("("+
					"exists("+
						"select 1 from ColaboradorFornecedor cf "+
						"where "+
						"cf.fornecedor =  fornecedor and "+
						"cf.colaborador = "+usuarioLogado.getCdpessoa()+
					") "+
					"or "+
					"not exists("+
						"select 1 from ColaboradorFornecedor cf "+
						"where "+
						"cf.fornecedor =  fornecedor"+
					")"+
				")");
		}
		
		return query.list();
	}

	public Entregadocumento findEntregadocumentoComSaldo(Integer cdentregadocumento) {
		if(cdentregadocumento == null){
			return null;
		}
		
		return query()
				.select("entregadocumento.cdentregadocumento, listaEntregamaterial.cdentregamaterial, listaEntregamaterial.qtde, listaEntregamaterial.valoricmsst, " +
						"listaEntregamaterial.valorbcicmsst, material.cdmaterial, listaEntregamaterial.icmsst, listaEntregamaterial.qtdeutilizada ")
				.join("entregadocumento.listaEntregamaterial listaEntregamaterial")
				.join("listaEntregamaterial.material material")
				.where("entregadocumento.cdentregadocumento = ?", cdentregadocumento)
				.unique();
	}
	
	public List<Entregadocumento> findForDominiointegracaoEntrada(Empresa empresa, Date dtinicio, Date dtfim) {
		if(empresa == null || empresa.getCdpessoa() == null || dtinicio == null || dtfim == null)
			throw new SinedException("Par�metros inv�lidos");
		
		return query()
					.select("entregadocumento.cdentregadocumento, entregadocumento.valor, entregadocumento.valorfrete, entregadocumento.valorseguro, entregadocumento.valoroutrasdespesas, " +
							"entregadocumento.chaveacesso, entregadocumento.dtemissao, entregadocumento.especie, " +
							"fornecedor.cpf, fornecedor.cnpj, fornecedor.inscricaomunicipal, fornecedor.inscricaoestadual, " +
							"entregadocumento.numero, entregadocumento.serie, entregadocumento.dtentrada, " +
							"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.nome, modelodocumentofiscal.codigo, " +
							"entrega.cdentrega, entregadocumento.indpag, " +
							"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.codigodominio, " +
							"modelodocumentofiscal.notafiscalprodutoeletronica, modelodocumentofiscal.notafiscalservicoeletronica, " +
							"ordemcompra.tipofrete, entregadocumento.situacaodocumento, " +
							"material.cdmaterial, entregamaterial.cdentregamaterial, " +
							"entregamaterial.csticms, entregamaterial.cstpis, entregamaterial.cstcofins, entregamaterial.cstipi, " +
							"entregamaterial.valorbcipi, entregamaterial.valoripi, entregamaterial.valorbcipi, entregamaterial.csticms, " +
							"entregamaterial.valorunitario, entregamaterial.qtde, entregamaterial.valoricms, entregamaterial.valoricmsst, " +
							"entregamaterial.icms, entregamaterial.valorfrete, entregamaterial.valorseguro, entregamaterial.valoroutrasdespesas, " +
							"entregamaterial.valorproduto, entregamaterial.cstipi, entregamaterial.ipi, entregamaterial.valorbcicmsst, " +
							"entregamaterial.pis, entregamaterial.valorpis, entregamaterial.cofins, entregamaterial.valorcofins, " +
							"entregamaterial.valorbcpis, entregamaterial.valorbccofins, entregamaterial.valorproduto, " +
							"cfopEm.codigo, cfopEm.cdcfop, endereco.cdendereco, municipio.cdmunicipio, municipio.cdibge, uf.cduf, uf.sigla, uf.cdibge," +
							"listadocumento.cdentregapagamento, listadocumento.dtemissao, " +
							"naturezaoperacao.cdnaturezaoperacao, " +
							"operacaocontabilavista.cdoperacaocontabil, operacaocontabilavista.codigointegracao," +
							"operacaocontabilaprazo.cdoperacaocontabil, operacaocontabilaprazo.codigointegracao")
					.leftOuterJoin("entregadocumento.empresa empresa")
					.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
					.leftOuterJoin("entregadocumento.fornecedor fornecedor")
					.leftOuterJoin("fornecedor.listaEndereco endereco")
					.leftOuterJoin("endereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("entregadocumento.entrega entrega")
					.leftOuterJoin("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")
					.leftOuterJoin("listaOrdemcompraentrega.ordemcompra ordemcompra")
					.leftOuterJoin("ordemcompra.empresa empresaoc")
					.join("entregadocumento.listaEntregamaterial entregamaterial")
					.leftOuterJoin("entregamaterial.material material")
					.leftOuterJoin("entregamaterial.cfop cfopEm")
					.leftOuterJoin("entregadocumento.listadocumento listadocumento")
					.leftOuterJoin("entregadocumento.naturezaoperacao naturezaoperacao")
					.leftOuterJoin("naturezaoperacao.operacaocontabilavista operacaocontabilavista")
					.leftOuterJoin("naturezaoperacao.operacaocontabilaprazo operacaocontabilaprazo")
					.openParentheses()
					.where("empresa = ?", empresa).or().where("empresaoc = ?", empresa).or().where("empresa is null")
					.closeParentheses()
					.where("entregadocumento.dtentrada >= ?", dtinicio)
					.where("entregadocumento.dtentrada <= ?", dtfim)
					.list();
					
	}
	
	/**
	 * M�todo que busca as informa��es para o registro50 do SIntegra
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForSIntegraRegistro50(SintegraFiltro filtro) {
		if(filtro == null || filtro.getEmpresa() == null || filtro.getEmpresa().getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Entregadocumento> query = querySined()
			
			.select("item.cdentregamaterial, item.qtde, item.valorunitario, item.valoricms, item.valoroutrasdespesas, item.icms, item.csticms, " +
					"item.valorbcicmsst, item.valorbcicms, item.icmsst, item.valoricmsst, item.valorproduto, item.valorfrete, item.valordesconto, " +
					"item.valorseguro, item.valoripi, " +
					
					"entregadocumento.cdentregadocumento, entregadocumento.dtentrada, entregadocumento.serie, entregadocumento.numero, " +
					"entregadocumento.valorfrete, entregadocumento.valordesconto, entregadocumento.valor, entregadocumento.dtemissao," +
					"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.nome, modelodocumentofiscal.codigo, " +
					"modelodocumentofiscal.sintegra_50, modelodocumentofiscal.sintegra_51, " +
					"modelodocumentofiscal.sintegra_53, modelodocumentofiscal.sintegra_54, " +
					
					"fornecedor.cnpj, fornecedor.cpf, fornecedor.inscricaoestadual, empresa.cdpessoa, empresa.crt, " +
					"cfop.cdcfop, cfop.codigo, " +
					"material.ncmcompleto, material.cdmaterial, material.nome, material.identificacao, material.origemproduto, unidademedida.simbolo, " +
					"unidademedidacomercial.simbolo, ufinscricaoestadual.sigla, uf.sigla, pais.cdpais ")
			.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
			.leftOuterJoin("entregadocumento.fornecedor fornecedor")
			.leftOuterJoin("fornecedor.ufinscricaoestadual ufinscricaoestadual")
			.leftOuterJoin("fornecedor.listaEndereco endereco")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("endereco.pais pais")
			.leftOuterJoin("entregadocumento.empresa empresa")
			.join("entregadocumento.listaEntregamaterial item")
			.join("item.material material")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("item.unidademedidacomercial unidademedidacomercial")
			.leftOuterJoin("item.cfop cfop")
			.where("modelodocumentofiscal.sintegra_50 = ?", Boolean.TRUE)
			.where("entregadocumento.dtentrada >= ?", filtro.getDtinicio())
			.where("entregadocumento.dtentrada <= ?", filtro.getDtfim())
			.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
			.where("empresa = ?", filtro.getEmpresa());
		
		if(filtro.getNaturezaoperacao() != null && 
				!SIntegraNaturezaoperacao.TOTALIDADE_OPERACOES_INFORMANTE.equals(filtro.getNaturezaoperacao())){
			if(SIntegraNaturezaoperacao.INTERESTADUAIS_COM_OU_SEM_SUBSTITUICAO_TRIBUTARIA.equals(filtro.getNaturezaoperacao())){
				query.where("exists " + 
						"(select 1 " +
						"from empresa e " +
						"where e.cdpessoa = " + filtro.getEmpresa().getCdpessoa() +
						" and e.ufsped <> uf.cduf )");
			}else if(SIntegraNaturezaoperacao.INTERESTADUAIS_SOMENTE_OPERACOES_REGIME_SUBSTITUICAO_TRIBUTARIA.equals(filtro.getNaturezaoperacao())){
				query.where("exists " + 
						"(select 1 " +
						"from empresa e " +
						"where e.cdpessoa = " + filtro.getEmpresa().getCdpessoa() +
						" and e.ufsped <> uf.cduf )");
				query
					.where("item.valoricmsst is not null ")
					.where("item.valoricmsst > 0 ");
			}
		}
	
		return query.list();		
	}

	/**
	 * M�todo que busca as informa��es para o registro70 do SIntegra
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForSIntegraRegistro70(SintegraFiltro filtro) {
		if(filtro == null || filtro.getEmpresa() == null || filtro.getEmpresa().getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Entregadocumento> query = querySined()
			
			.select("entregamaterial.cdentregamaterial, entregamaterial.qtde, entregamaterial.valorunitario, entregamaterial.valoricms, entregamaterial.valoroutrasdespesas, entregamaterial.icms, entregamaterial.csticms, " +
					"entregamaterial.valorbcicmsst, entregamaterial.valorbcicms, entregamaterial.icmsst, entregamaterial.valoricmsst, entregamaterial.valorproduto, entregamaterial.valorfrete, entregamaterial.valordesconto, " +
					"entregamaterial.valorseguro, entregamaterial.valoripi, " +
					
					"entregadocumento.cdentregadocumento, entregadocumento.dtentrada, entregadocumento.serie, entregadocumento.numero, " +
					"entregadocumento.valorfrete, entregadocumento.valordesconto, entregadocumento.valor, entregadocumento.dtemissao, " +
					"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.sintegra_71, " +
					
					"fornecedor.cnpj, fornecedor.cpf, fornecedor.inscricaoestadual, empresa.cdpessoa, empresa.crt, " +
					"cfop.cdcfop, cfop.codigo, " +
					"material.ncmcompleto, material.cdmaterial, material.nome, material.identificacao, material.origemproduto, unidademedida.simbolo, " +
					"unidademedidacomercial.simbolo, ufinscricaoestadual.sigla ")
			.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
			.leftOuterJoin("entregadocumento.fornecedor fornecedor")
			.leftOuterJoin("fornecedor.ufinscricaoestadual ufinscricaoestadual")
			.leftOuterJoin("fornecedor.listaEndereco endereco")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("entregadocumento.empresa empresa")
			.join("entregadocumento.listaEntregamaterial entregamaterial")
			.join("entregamaterial.material material")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("entregamaterial.unidademedidacomercial unidademedidacomercial")
			.leftOuterJoin("entregamaterial.cfop cfop")
			.where("modelodocumentofiscal.sintegra_70 = ?", Boolean.TRUE)
			.where("entregadocumento.dtentrada >= ?", filtro.getDtinicio())
			.where("entregadocumento.dtentrada <= ?", filtro.getDtfim())
			.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
			.where("empresa = ?", filtro.getEmpresa());
	
		if(filtro.getNaturezaoperacao() != null && 
				!SIntegraNaturezaoperacao.TOTALIDADE_OPERACOES_INFORMANTE.equals(filtro.getNaturezaoperacao())){
			if(SIntegraNaturezaoperacao.INTERESTADUAIS_COM_OU_SEM_SUBSTITUICAO_TRIBUTARIA.equals(filtro.getNaturezaoperacao())){
				query.where("exists " + 
						"(select 1 " +
						"from empresa e " +
						"where e.cdpessoa = " + filtro.getEmpresa().getCdpessoa() +
						" and e.ufsped <> uf.cduf )");
			}else if(SIntegraNaturezaoperacao.INTERESTADUAIS_SOMENTE_OPERACOES_REGIME_SUBSTITUICAO_TRIBUTARIA.equals(filtro.getNaturezaoperacao())){
				query.where("exists " + 
						"(select 1 " +
						"from empresa e " +
						"where e.cdpessoa = " + filtro.getEmpresa().getCdpessoa() +
						" and e.ufsped <> uf.cduf )");
				query
					.where("entregamaterial.valoricmsst is not null ")
					.where("entregamaterial.valoricmsst > 0 ");
			}
		}
		
		return query.list();
	}

	/**
	 * M�todo que busca as informa��es para o registro76 do SIntegra
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForSIntegraRegistro76(SintegraFiltro filtro) {
		if(filtro == null || filtro.getEmpresa() == null || filtro.getEmpresa().getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Entregadocumento> query = querySined()
			
			.select("entregamaterial.cdentregamaterial, fornecedor.cnpj, fornecedor.cpf, fornecedor.inscricaoestadual, entregadocumento.dtentrada, ufinscricaoestadual.sigla, " +
					"entregadocumento.serie, entregadocumento.numero, cfop.cdcfop, cfop.codigo, " +
					"entregamaterial.qtde, entregamaterial.valorunitario, entregadocumento.valorfrete, entregadocumento.valordesconto, " +
					"entregamaterial.valoricms, entregamaterial.valoroutrasdespesas, entregamaterial.valordesconto, " +
					"entregamaterial.icms, entregamaterial.csticms, material.cdmaterial, material.nome, material.identificacao, unidademedidacomercial.simbolo," +
					"endereco.cdendereco, municipio.cdmunicipio, uf.sigla, modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, " +
					"modelodocumentofiscal.nome")
			.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
			.leftOuterJoin("entregadocumento.fornecedor fornecedor")
			.leftOuterJoin("fornecedor.ufinscricaoestadual ufinscricaoestadual")
			.leftOuterJoin("fornecedor.listaEndereco endereco")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("entregadocumento.empresa empresa")
			.join("entregadocumento.listaEntregamaterial entregamaterial")
			.join("entregamaterial.material material")
			.leftOuterJoin("entregamaterial.unidademedidacomercial unidademedidacomercial")
			.leftOuterJoin("entregamaterial.cfop cfop")
			.where("modelodocumentofiscal.sintegra_76_77 = ?", Boolean.TRUE)
			.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
			.where("entregadocumento.dtentrada >= ?", filtro.getDtinicio())
			.where("entregadocumento.dtentrada <= ?", filtro.getDtfim())
			.where("empresa = ?", filtro.getEmpresa());
		
		if(filtro.getNaturezaoperacao() != null && 
				!SIntegraNaturezaoperacao.TOTALIDADE_OPERACOES_INFORMANTE.equals(filtro.getNaturezaoperacao())){
			if(SIntegraNaturezaoperacao.INTERESTADUAIS_COM_OU_SEM_SUBSTITUICAO_TRIBUTARIA.equals(filtro.getNaturezaoperacao())){
				query.where("exists " + 
						"(select 1 " +
						"from empresa e " +
						"where e.cdpessoa = " + filtro.getEmpresa().getCdpessoa() +
						" and e.ufsped <> uf.cduf )");
			}else if(SIntegraNaturezaoperacao.INTERESTADUAIS_SOMENTE_OPERACOES_REGIME_SUBSTITUICAO_TRIBUTARIA.equals(filtro.getNaturezaoperacao())){
				query.where("exists " + 
						"(select 1 " +
						"from empresa e " +
						"where e.cdpessoa = " + filtro.getEmpresa().getCdpessoa() +
						" and e.ufsped <> uf.cduf )");
				query
					.where("entregamaterial.valoricmsst is not null ")
					.where("entregamaterial.valoricmsst > 0 ");
			}
		}
	
		return query.list();
	}
	
	/**
	 * Busca os registro de entrega para o registro D100 do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Entregadocumento> findForSpedRegD100(SpedarquivoFiltro filtro) {
		QueryBuilder<Entregadocumento> query = querySined()
										
										.select("entregadocumento.numero, entregadocumento.cdentregadocumento, entregadocumento.chaveacesso, " +
												"entregadocumento.dtentrada, entregadocumento.valor, entregadocumento.indpag, entregadocumento.serie, " +
												"entregadocumento.infoadicionalfisco, entregadocumento.natop, responsavelfrete.cdnfe, fornecedor.cdpessoa, " +
												"entrega.cdentrega, entregadocumento.dtemissao, entregadocumento.valordesconto, entregadocumento.valor, " +
												"entregadocumento.valorfrete, entregadocumento.valorseguro, entregadocumento.valoroutrasdespesas, " +
												"entregadocumento.tipotitulo, item.valoriss, item.valorbciss, item.iss, item.tipotributacaoiss, " +
												"entregadocumento.municipioorigemexterior, entregadocumento.municipiodestinoexterior, " +
												"municipioorigem.cdmunicipio, municipioorigem.cdibge, municipiodestino.cdmunicipio, municipiodestino.cdibge, " +
												"entregadocumento.descricaotitulo, entregadocumento.numerotitulo, material.cdmaterial, material.identificacao, material.origemproduto, unidademedidacomercial.cdunidademedida, item.qtde, item.valorunitario, " +
												"item.csticms, cfop.cdcfop, cfop.codigo, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, item.icmsst, " +
												"item.valoricmsst, item.cstipi, item.valorbcipi, item.ipi, item.valoripi, item.cstpis, item.valorbcpis, item.pis, item.qtdebcpis, " +
												"item.valorunidbcpis, item.valorpis, item.cstcofins, item.valorbccofins, item.cofins, item.qtdebccofins, item.valorunidbccofins, " +
												"item.valorcofins, entregadocumento.situacaodocumento, item.valorir, item.valorcsll, item.valorinss, " +
												"item.valorimpostoimportacao, item.valorpisretido, item.valorcofinsretido, item.valordesconto, " +
												"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.nome, " +
												"listaAjustefiscal.cdajustefiscal, " +
												"listaAjustefiscal.codigoajuste, " +
												"listaAjustefiscal.descricaocomplementar, listaAjustefiscal.aliquotaicms, " +
												"listaAjustefiscal.basecalculoicms, listaAjustefiscal.valoricms, " +
												"listaAjustefiscal.outrosvalores, " +
												"obslancamentofiscal.cdobslancamentofiscal, obslancamentofiscal.descricao, " +
												"materialajuste.cdmaterial, materialajuste.nome, materialajuste.identificacao") 
										.leftOuterJoin("entregadocumento.empresa empresa")
										.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
										.leftOuterJoin("entregadocumento.entrega entrega")
										.leftOuterJoin("entrega.aux_entrega aux_entrega")
										.leftOuterJoin("entregadocumento.responsavelfrete responsavelfrete")
										.leftOuterJoin("entregadocumento.fornecedor fornecedor")
										.leftOuterJoin("entregadocumento.municipioorigem municipioorigem")
										.leftOuterJoin("entregadocumento.municipiodestino municipiodestino")
										.join("entregadocumento.listaEntregamaterial item")
										.leftOuterJoin("item.cfop cfop")
										.join("item.material material")
										.join("item.unidademedidacomercial unidademedidacomercial")
										.leftOuterJoin("entregadocumento.listaAjustefiscal listaAjustefiscal")
										.leftOuterJoin("listaAjustefiscal.obslancamentofiscal obslancamentofiscal")
										.leftOuterJoin("listaAjustefiscal.material materialajuste")
										.openParentheses()
											.where("empresa = ?", filtro.getEmpresa()).or()
											.where("empresa is null")
										.closeParentheses()
										.where("modelodocumentofiscal.spedfiscal_d100 = ?", Boolean.TRUE)
										.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
										.where(getSqlDtemissaoDtentrada() + ">= ?", filtro.getDtinicio())
										.where(getSqlDtemissaoDtentrada() + " <= ?", filtro.getDtfim());
		
		return query.list();
	}
	
	private String getSqlDtemissaoDtentrada() {
		return parametrogeralService.getBoolean(Parametrogeral.SPED_DATA_EMISSAO) ? "entregadocumento.dtemissao" : "entregadocumento.dtentrada";
	}
	/**
	 * Busca os registro de entrega para o registro C100 do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Entregadocumento> findForSpedRegC100(SpedarquivoFiltro filtro) {
		QueryBuilder<Entregadocumento> query = querySined()
			
			.select("entregadocumento.numero, entregadocumento.cdentregadocumento, entregadocumento.situacaodocumento," +
					"entregadocumento.dtentrada, entregadocumento.valor, entregadocumento.indpag, entregadocumento.serie, " +
					"entregadocumento.infoadicionalfisco, entregadocumento.natop, naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome, responsavelfrete.cdnfe, fornecedor.cdpessoa, " +
					"entrega.cdentrega, entregadocumento.dtemissao, entregadocumento.valordesconto, entregadocumento.valor, " +
					"entregadocumento.valorfrete, entregadocumento.valorseguro, entregadocumento.valoroutrasdespesas, " +
					"entregadocumento.tipotitulo, item.valoriss, item.valorbciss, item.iss, item.tipotributacaoiss, " +
					"entregadocumento.descricaotitulo, entregadocumento.numerotitulo, material.cdmaterial, material.identificacao, material.origemproduto, material.tipoitemsped, material.vendapromocional, unidademedidacomercial.cdunidademedida, " +
					"item.cdentregamaterial, item.qtde, item.valorunitario, item.csticms, cfop.cdcfop, cfop.codigo, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, item.icmsst, " +
					"item.valoricmsst, item.cstipi, item.valorbcipi, item.ipi, item.valoripi, item.cstpis, item.valorbcpis, item.pis, item.qtdebcpis, " +
					"item.valorunidbcpis, item.valorpis, item.cstcofins, item.valorbccofins, item.cofins, item.qtdebccofins, item.valorunidbccofins, " +
					"item.valorcofins, entregadocumento.simplesremessa, entregadocumento.chaveacesso, item.valorir, item.valorcsll, item.valorinss, " +
					"item.valorimpostoimportacao, item.valorpisretido, item.valorcofinsretido, item.valordesconto, " +
					"uf.cduf, uf.sigla, item.valorfrete, item.valoroutrasdespesas, " +
					"listaEntregadocumentoreferenciado.modelodocumento, listaEntregadocumentoreferenciado.numero, " +
					"listaEntregadocumentoreferenciado.autenticacaobancaria, listaEntregadocumentoreferenciado.valor, " +
					"listaEntregadocumentoreferenciado.dtvencimento, listaEntregadocumentoreferenciado.dtpagamento," +
					"uf.cduf, ufEdr.sigla, modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, " +
					"modelodocumentofiscal.nome, modelodocumentofiscal.sintegra_71, " +
					"listaAjustefiscal.cdajustefiscal, " +
					"listaAjustefiscal.codigoajuste, " +
					"listaAjustefiscal.descricaocomplementar, listaAjustefiscal.aliquotaicms, " +
					"listaAjustefiscal.basecalculoicms, listaAjustefiscal.valoricms, " +
					"listaAjustefiscal.outrosvalores, " +
					"obslancamentofiscal.cdobslancamentofiscal, obslancamentofiscal.descricao, " +
					"materialajuste.cdmaterial, materialajuste.nome, materialajuste.identificacao, empresa.cdpessoa ")
			.leftOuterJoin("entregadocumento.empresa empresa")
			.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
			.leftOuterJoin("entregadocumento.entrega entrega")
			.leftOuterJoin("entrega.aux_entrega aux_entrega")
			.leftOuterJoin("entregadocumento.responsavelfrete responsavelfrete")
			.leftOuterJoin("entregadocumento.fornecedor fornecedor")
			.join("entregadocumento.listaEntregamaterial item")
			.leftOuterJoin("item.cfop cfop")
			.join("item.material material")
			.join("item.unidademedidacomercial unidademedidacomercial")
			.leftOuterJoin("fornecedor.listaEndereco listaEndereco")
			.leftOuterJoin("listaEndereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("entregadocumento.listaEntregadocumentoreferenciado listaEntregadocumentoreferenciado")
			.leftOuterJoin("listaEntregadocumentoreferenciado.uf ufEdr")
			.leftOuterJoin("entregadocumento.listaAjustefiscal listaAjustefiscal")
			.leftOuterJoin("listaAjustefiscal.obslancamentofiscal obslancamentofiscal")
			.leftOuterJoin("listaAjustefiscal.material materialajuste")
			.leftOuterJoin("entregadocumento.naturezaoperacao naturezaoperacao")
			.openParentheses()
				.where("empresa = ?", filtro.getEmpresa()).or()
				.where("empresa is null")
			.closeParentheses()	
			.where("coalesce(modelodocumentofiscal.spedfiscal_c100, false) = ?", Boolean.TRUE)
			.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
			.where(getSqlDtemissaoDtentrada() + " >= ?", filtro.getDtinicio())
			.where(getSqlDtemissaoDtentrada() + " <= ?", filtro.getDtfim());
		
		return query.list();
	}
	
	public List<Entregadocumento> findForSpedRegD500(SpedarquivoFiltro filtro) {
		QueryBuilder<Entregadocumento> query = querySined()
				
				.select("entregadocumento.numero, entregadocumento.cdentregadocumento, entregadocumento.situacaodocumento," +
						"entregadocumento.dtentrada, entregadocumento.valor, entregadocumento.indpag, entregadocumento.serie, " +
						"entregadocumento.infoadicionalfisco, entregadocumento.natop, responsavelfrete.cdnfe, fornecedor.cdpessoa, " +
						"entrega.cdentrega, entregadocumento.dtemissao, entregadocumento.valordesconto, entregadocumento.valor, " +
						"entregadocumento.valorfrete, entregadocumento.valorseguro, entregadocumento.valoroutrasdespesas, " +
						"entregadocumento.tipotitulo, item.valoriss, item.valorbciss, item.iss, item.tipotributacaoiss, " +
						"entregadocumento.descricaotitulo, entregadocumento.numerotitulo, material.cdmaterial, material.identificacao, material.origemproduto, unidademedidacomercial.cdunidademedida, item.qtde, item.valorunitario, " +
						"item.csticms, cfop.cdcfop, cfop.codigo, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, item.icmsst, " +
						"item.valoricmsst, item.cstipi, item.valorbcipi, item.ipi, item.valoripi, item.cstpis, item.valorbcpis, item.pis, item.qtdebcpis, " +
						"item.valorunidbcpis, item.valorpis, item.cstcofins, item.valorbccofins, item.cofins, item.qtdebccofins, item.valorunidbccofins, " +
						"item.valorcofins, entregadocumento.simplesremessa, entregadocumento.chaveacesso, item.valorir, item.valorcsll, item.valorinss, " +
						"item.valorimpostoimportacao, item.valorpisretido, item.valorcofinsretido, item.naturezabcc, item.valordesconto, " +
						"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.nome")
				.leftOuterJoin("entregadocumento.empresa empresa")
				.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
				.leftOuterJoin("entregadocumento.entrega entrega")
				.leftOuterJoin("entrega.aux_entrega aux_entrega")
				.leftOuterJoin("entregadocumento.responsavelfrete responsavelfrete")
				.leftOuterJoin("entregadocumento.fornecedor fornecedor")
				.join("entregadocumento.listaEntregamaterial item")
				.leftOuterJoin("item.cfop cfop")
				.join("item.material material")
				.join("item.unidademedidacomercial unidademedidacomercial")
				.openParentheses()
					.where("empresa = ?", filtro.getEmpresa()).or()
					.where("empresa is null")
				.closeParentheses()
				.where("modelodocumentofiscal.spedfiscal_d500 = ?", Boolean.TRUE)
				.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
				.where(getSqlDtemissaoDtentrada() + " >= ?", filtro.getDtinicio())
				.where(getSqlDtemissaoDtentrada() + " <= ?", filtro.getDtfim());
		
		return query.list();
	}
	
	public List<Entregadocumento> findForSpedRegC500(SpedarquivoFiltro filtro) {
		QueryBuilder<Entregadocumento> query = querySined()
					
					.select("entregadocumento.numero, entregadocumento.cdentregadocumento, entregadocumento.situacaodocumento," +
							"entregadocumento.dtentrada, entregadocumento.valor, entregadocumento.indpag, entregadocumento.serie, " +
							"entregadocumento.infoadicionalfisco, entregadocumento.natop, responsavelfrete.cdnfe, fornecedor.cdpessoa, " +
							"entrega.cdentrega, entregadocumento.dtemissao, entregadocumento.valordesconto, entregadocumento.valor, " +
							"entregadocumento.valorfrete, entregadocumento.valorseguro, entregadocumento.valoroutrasdespesas, " +
							"entregadocumento.tipotitulo, item.valoriss, item.valorbciss, item.iss, item.tipotributacaoiss, " +
							"entregadocumento.descricaotitulo, entregadocumento.numerotitulo, material.cdmaterial, material.identificacao, material.origemproduto, unidademedidacomercial.cdunidademedida, item.qtde, item.valorunitario, " +
							"item.csticms, cfop.cdcfop, cfop.codigo, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, item.icmsst, " +
							"item.valoricmsst, item.cstipi, item.valorbcipi, item.ipi, item.valoripi, item.cstpis, item.valorbcpis, item.pis, item.qtdebcpis, " +
							"item.valorunidbcpis, item.valorpis, item.cstcofins, item.valorbccofins, item.cofins, item.qtdebccofins, item.valorunidbccofins, " +
							"item.valorcofins, entregadocumento.simplesremessa, entregadocumento.chaveacesso, item.valorir, item.valorcsll, item.valorinss, " +
							"item.valorimpostoimportacao, item.valorpisretido, item.valorcofinsretido, item.naturezabcc, item.valordesconto, entrega.cdentrega, " +
							"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.nome, modelodocumentofiscal.codigo")
					.leftOuterJoin("entregadocumento.empresa empresa")
					.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
					.leftOuterJoin("entregadocumento.entrega entrega")
					.leftOuterJoin("entrega.aux_entrega aux_entrega")
					.leftOuterJoin("entregadocumento.responsavelfrete responsavelfrete")
					.leftOuterJoin("entregadocumento.fornecedor fornecedor")
					.join("entregadocumento.listaEntregamaterial item")
					.leftOuterJoin("item.cfop cfop")
					.join("item.material material")
					.join("item.unidademedidacomercial unidademedidacomercial")
					.openParentheses()
						.where("empresa = ?", filtro.getEmpresa()).or()
						.where("empresa is null")
					.closeParentheses()
					.where("modelodocumentofiscal.spedfiscal_c500 = ?", Boolean.TRUE)
					.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
					.where(getSqlDtemissaoDtentrada() + " >= ?", filtro.getDtinicio())
					.where(getSqlDtemissaoDtentrada() + " <= ?", filtro.getDtfim());
		
		return query.list();
	}
	
	/**
	 * M�todo que busca as informa��es para o RegistroC100 do SPED PIS/COFINS
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForSpedPiscofinsRegC100(SpedpiscofinsFiltro filtro, boolean servico, Empresa empresa) {
		QueryBuilder<Entregadocumento> query = querySined()
				
				.select("entregadocumento.numero, empresa.cnpj, empresa.cdpessoa, entregadocumento.situacaodocumento," +
						"entregadocumento.dtentrada, entregadocumento.valor, entregadocumento.indpag, entregadocumento.serie, " +
						"entregadocumento.infoadicionalfisco, entregadocumento.natop, responsavelfrete.cdnfe, fornecedor.cdpessoa, fornecedor.cnpj, fornecedor.cpf, " +
						"entrega.cdentrega, entregadocumento.dtemissao, entregadocumento.valordesconto, entregadocumento.valor, " +
						"entregadocumento.valorfrete, entregadocumento.valorseguro, entregadocumento.valoroutrasdespesas, item.valoroutrasdespesas, " +
						"entregadocumento.tipotitulo, item.valoriss, item.valorbciss, item.iss, item.tipotributacaoiss, " +
						"entregadocumento.descricaotitulo, entregadocumento.numerotitulo, material.cdmaterial, material.identificacao, material.origemproduto, unidademedidacomercial.cdunidademedida, item.qtde, item.valorunitario, " +
						"item.csticms, cfop.cdcfop, cfop.codigo, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, item.icmsst, " +
						"item.valoricmsst, item.cstipi, item.valorbcipi, item.ipi, item.valoripi, item.cstpis, item.valorbcpis, item.pis, item.qtdebcpis, " +
						"item.valorunidbcpis, item.valorpis, item.cstcofins, item.valorbccofins, item.cofins, item.qtdebccofins, item.valorunidbccofins, " +
						"item.valorcofins, entregadocumento.simplesremessa, entregadocumento.chaveacesso, item.naturezabcc, item.valorir, item.valorcsll, item.valorinss, " +
						"item.valorimpostoimportacao, item.valorpisretido, item.valorcofinsretido, item.valordesconto, naturezaoperacao.cdnaturezaoperacao, " +
						"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.nome, vcontacontabil.identificador")
				.leftOuterJoin("entregadocumento.empresa empresa")
				.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
				.leftOuterJoin("entregadocumento.entrega entrega")
				.leftOuterJoin("entrega.aux_entrega aux_entrega")
				.leftOuterJoin("entregadocumento.responsavelfrete responsavelfrete")
				.leftOuterJoin("entregadocumento.fornecedor fornecedor")
				.leftOuterJoin("entregadocumento.naturezaoperacao naturezaoperacao")
				.join("entregadocumento.listaEntregamaterial item")
				.leftOuterJoin("item.cfop cfop")
				.join("item.material material")
				.join("item.unidademedidacomercial unidademedidacomercial")
				.leftOuterJoin("material.contaContabil contacontabil")
				.leftOuterJoin("contacontabil.vcontacontabil vcontacontabil")
				.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
				.where("empresa = ?", empresa)
				.where(getSqlDtemissaoDtentrada() + " >= ?", filtro.getDtinicio())
				.where(getSqlDtemissaoDtentrada() + " <= ?", filtro.getDtfim())
				.openParentheses()
					.where("naturezaoperacao is null")
					.or()
					.where("naturezaoperacao.simplesremessa is null")
					.or()
					.where("naturezaoperacao.simplesremessa = false")
				.closeParentheses();
		
		if(filtro.getListaNaturezabccredito() != null){
			query
				.openParentheses()
				.where("item.naturezabcc not in (" + CollectionsUtil.listAndConcatenate(filtro.getListaNaturezabccredito(),"value", ",") + ")")
				.or()
				.where("item.naturezabcc is null")
				.closeParentheses();
		}
		
		if(servico){
			query.where("modelodocumentofiscal.spedpiscofins_a100 = ?", Boolean.TRUE);
		} else {
			query.where("modelodocumentofiscal.spedpiscofins_c100 = ?", Boolean.TRUE);
		}
		
		query.where("cfop.codigo <> '5908' and cfop.codigo <> '1909' ");
		
		return query.list();
	}
	
	/**
	 * M�todo que busca as informa��es para o RegistroF100 do SPED PIS/COFINS
	 *
	 * @param filtro
	 * @param servico
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForSpedPiscofinsRegF100(SpedpiscofinsFiltro filtro, boolean servico, Empresa empresa) {
		QueryBuilder<Entregadocumento> query = querySined()
				
				.select("entregadocumento.numero, empresa.cnpj, entregadocumento.situacaodocumento, " +
						"entregadocumento.dtentrada, entregadocumento.valor, entregadocumento.indpag, entregadocumento.serie, " +
						"entregadocumento.infoadicionalfisco, entregadocumento.natop, responsavelfrete.cdnfe, fornecedor.cdpessoa, fornecedor.cnpj, fornecedor.cpf, " +
						"entrega.cdentrega, entregadocumento.dtemissao, entregadocumento.valordesconto, entregadocumento.valor, " +
						"entregadocumento.valorfrete, entregadocumento.valorseguro, entregadocumento.valoroutrasdespesas, " +
						"entregadocumento.tipotitulo, item.valoriss, item.valorbciss, item.iss, item.tipotributacaoiss, " +
						"entregadocumento.descricaotitulo, entregadocumento.numerotitulo, material.cdmaterial, material.identificacao, material.origemproduto, unidademedidacomercial.cdunidademedida, item.qtde, item.valorunitario, " +
						"item.csticms, cfop.cdcfop, cfop.codigo, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, item.icmsst, " +
						"item.valoricmsst, item.cstipi, item.valorbcipi, item.ipi, item.valoripi, item.cstpis, item.valorbcpis, item.pis, item.qtdebcpis, " +
						"item.valorunidbcpis, item.valorpis, item.cstcofins, item.valorbccofins, item.cofins, item.qtdebccofins, item.valorunidbccofins, " +
						"item.valorcofins, entregadocumento.simplesremessa, entregadocumento.chaveacesso, item.naturezabcc, item.valorir, item.valorcsll, item.valorinss, " +
						"item.valorimpostoimportacao, item.valorpisretido, item.valorcofinsretido, material.nome, item.valordesconto, " +
						"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.nome, vcontacontabil.identificador")
				.leftOuterJoin("entregadocumento.empresa empresa")
				.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
				.leftOuterJoin("entregadocumento.entrega entrega")
				.leftOuterJoin("entrega.aux_entrega aux_entrega")
				.leftOuterJoin("entregadocumento.responsavelfrete responsavelfrete")
				.leftOuterJoin("entregadocumento.fornecedor fornecedor")
				.join("entregadocumento.listaEntregamaterial item")
				.leftOuterJoin("item.cfop cfop")
				.join("item.material material")
				.join("item.unidademedidacomercial unidademedidacomercial")
				.leftOuterJoin("material.contaContabil contacontabil")
				.leftOuterJoin("contacontabil.vcontacontabil vcontacontabil")
				.where("modelodocumentofiscal.spedpiscofins_f100 = ?", Boolean.TRUE)
				.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
				.where("empresa = ?", empresa)
				.where(getSqlDtemissaoDtentrada() + " >= ?", filtro.getDtinicio())
				.where(getSqlDtemissaoDtentrada() + " <= ?", filtro.getDtfim());
		
		if(filtro.getListaNaturezabccredito() != null)
			query.where("item.naturezabcc in (" + CollectionsUtil.listAndConcatenate(filtro.getListaNaturezabccredito(),"value", ",") + ")");
		
		query.where("cfop.codigo <> '5908' and cfop.codigo <> '1909' ");
		
		return query.list();
	}

	/**
	 * M�todo que busca as informa��es para o RegistroD100 do SPED PIS/COFINS
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForSpedPiscofinsRegD100(SpedpiscofinsFiltro filtro, Empresa empresa) {
		QueryBuilder<Entregadocumento> query = querySined()
										
										.select("entregadocumento.numero, empresa.cnpj, entregadocumento.situacaodocumento, entregadocumento.chaveacesso, " +
												"entregadocumento.dtentrada, entregadocumento.valor, entregadocumento.indpag, entregadocumento.serie, " +
												"entregadocumento.infoadicionalfisco, entregadocumento.natop, responsavelfrete.cdnfe, fornecedor.cdpessoa, fornecedor.cnpj, fornecedor.cpf, " +
												"entrega.cdentrega, entregadocumento.dtemissao, entregadocumento.valordesconto, entregadocumento.valor, " +
												"entregadocumento.valorfrete, entregadocumento.valorseguro, entregadocumento.valoroutrasdespesas, " +
												"entregadocumento.tipotitulo, item.valoriss, item.valorbciss, item.iss, item.tipotributacaoiss, " +
												"entregadocumento.descricaotitulo, entregadocumento.numerotitulo, material.cdmaterial, material.identificacao, material.origemproduto, unidademedidacomercial.cdunidademedida, item.qtde, item.valorunitario, " +
												"item.csticms, cfop.cdcfop, cfop.codigo, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, item.icmsst, " +
												"item.valoricmsst, item.cstipi, item.valorbcipi, item.ipi, item.valoripi, item.cstpis, item.valorbcpis, item.pis, item.qtdebcpis, " +
												"item.valorunidbcpis, item.valorpis, item.cstcofins, item.valorbccofins, item.cofins, item.qtdebccofins, item.valorunidbccofins, " +
												"item.valorcofins, item.valorir, item.valorcsll, item.valorinss, " +
												"item.valorimpostoimportacao, item.valorpisretido, item.valorcofinsretido, item.valordesconto, " +
												"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.nome, vcontacontabil.identificador")
										.leftOuterJoin("entregadocumento.empresa empresa")
										.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
										.leftOuterJoin("entregadocumento.entrega entrega")
										.leftOuterJoin("entrega.aux_entrega aux_entrega")
										.leftOuterJoin("entregadocumento.responsavelfrete responsavelfrete")
										.leftOuterJoin("entregadocumento.fornecedor fornecedor")
										.join("entregadocumento.listaEntregamaterial item")
										.leftOuterJoin("item.cfop cfop")
										.join("item.material material")
										.join("item.unidademedidacomercial unidademedidacomercial")
										.leftOuterJoin("material.contaContabil contacontabil")
										.leftOuterJoin("contacontabil.vcontacontabil vcontacontabil")
										.where("modelodocumentofiscal.spedpiscofins_d100 = ?", Boolean.TRUE)
										.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
										.where("empresa = ?", empresa)
										.where(getSqlDtemissaoDtentrada() + " >= ?", filtro.getDtinicio())
										.where(getSqlDtemissaoDtentrada() + " <= ?", filtro.getDtfim());
		
		if(filtro.getListaNaturezabccredito() != null)
			query.where("item.naturezabcc not in (" + CollectionsUtil.listAndConcatenate(filtro.getListaNaturezabccredito(),"value", ",") + ")");
		
		query.where("cfop.codigo <> '5908' and cfop.codigo <> '1909' ");
		
		return query.list();
	}
	
	/**
	 * M�todo que busca informa��es de tributos para o RegistroC500 do SPED PIS/COFINS
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForSpedPiscofinsRegC500(SpedpiscofinsFiltro filtro, Empresa empresa) {
		QueryBuilder<Entregadocumento> query = querySined()
				
				.select("entregadocumento.numero, entregadocumento.cdentregadocumento, empresa.cnpj, entregadocumento.situacaodocumento," +
						"entregadocumento.dtentrada, entregadocumento.valor, entregadocumento.indpag, entregadocumento.serie, " +
						"entregadocumento.infoadicionalfisco, entregadocumento.natop, responsavelfrete.cdnfe, fornecedor.cdpessoa, fornecedor.cnpj, fornecedor.cpf, " +
						"entrega.cdentrega, entregadocumento.dtemissao, entregadocumento.valordesconto, entregadocumento.valor, " +
						"entregadocumento.valorfrete, entregadocumento.valorseguro, entregadocumento.valoroutrasdespesas, " +
						"entregadocumento.tipotitulo, item.valoriss, item.valorbciss, item.iss, item.tipotributacaoiss, " +
						"entregadocumento.descricaotitulo, entregadocumento.numerotitulo, material.cdmaterial, material.identificacao, material.origemproduto, unidademedidacomercial.cdunidademedida, item.qtde, item.valorunitario, " +
						"item.csticms, cfop.cdcfop, cfop.codigo, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, item.icmsst, " +
						"item.valoricmsst, item.cstipi, item.valorbcipi, item.ipi, item.valoripi, item.cstpis, item.valorbcpis, item.pis, item.qtdebcpis, " +
						"item.valorunidbcpis, item.valorpis, item.cstcofins, item.valorbccofins, item.cofins, item.qtdebccofins, item.valorunidbccofins, " +
						"item.valorcofins, entregadocumento.simplesremessa, entregadocumento.chaveacesso, item.valorir, item.valorcsll, item.valorinss, " +
						"item.valorimpostoimportacao, item.valorpisretido, item.valorcofinsretido, item.naturezabcc, item.valordesconto, " +
						"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.nome, vcontacontabil.identificador")
				.leftOuterJoin("entregadocumento.empresa empresa")
				.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
				.leftOuterJoin("entregadocumento.entrega entrega")
				.leftOuterJoin("entrega.aux_entrega aux_entrega")
				.leftOuterJoin("entregadocumento.responsavelfrete responsavelfrete")
				.leftOuterJoin("entregadocumento.fornecedor fornecedor")
				.join("entregadocumento.listaEntregamaterial item")
				.leftOuterJoin("item.cfop cfop")
				.join("item.material material")
				.join("item.unidademedidacomercial unidademedidacomercial")
				.leftOuterJoin("material.contaContabil contacontabil")
				.leftOuterJoin("contacontabil.vcontacontabil vcontacontabil")
				.where("modelodocumentofiscal.spedpiscofins_c500 = ?", Boolean.TRUE)
				.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
				.where("empresa = ?", empresa)
				.where(getSqlDtemissaoDtentrada() + " >= ?", filtro.getDtinicio())
				.where(getSqlDtemissaoDtentrada() + " <= ?", filtro.getDtfim());
		
		query.where("cfop.codigo <> '5908' and cfop.codigo <> '1909' ");
		
		return query.list();
	}
	
	/**
	 * M�todo que busca informa��es para o RegistroD500 do SPED PIS/COFINS 
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForSpedPiscofinsRegD500(SpedpiscofinsFiltro filtro, Empresa empresa) {
		QueryBuilder<Entregadocumento> query = querySined()
				
				.select("entregadocumento.numero, entregadocumento.cdentregadocumento, empresa.cnpj, entregadocumento.situacaodocumento," +
						"entregadocumento.dtentrada, entregadocumento.valor, entregadocumento.indpag, entregadocumento.serie, " +
						"entregadocumento.infoadicionalfisco, entregadocumento.natop, responsavelfrete.cdnfe, fornecedor.cdpessoa, fornecedor.cnpj, fornecedor.cpf, " +
						"entrega.cdentrega, entregadocumento.dtemissao, entregadocumento.valordesconto, entregadocumento.valor, " +
						"entregadocumento.valorfrete, entregadocumento.valorseguro, entregadocumento.valoroutrasdespesas, " +
						"entregadocumento.tipotitulo, item.valoriss, item.valorbciss, item.iss, item.tipotributacaoiss, " +
						"entregadocumento.descricaotitulo, entregadocumento.numerotitulo, material.cdmaterial, material.identificacao, material.origemproduto, unidademedidacomercial.cdunidademedida, item.qtde, item.valorunitario, " +
						"item.csticms, cfop.cdcfop, cfop.codigo, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, item.icmsst, " +
						"item.valoricmsst, item.cstipi, item.valorbcipi, item.ipi, item.valoripi, item.cstpis, item.valorbcpis, item.pis, item.qtdebcpis, " +
						"item.valorunidbcpis, item.valorpis, item.cstcofins, item.valorbccofins, item.cofins, item.qtdebccofins, item.valorunidbccofins, " +
						"item.valorcofins, entregadocumento.simplesremessa, entregadocumento.chaveacesso, item.valorir, item.valorcsll, item.valorinss, " +
						"item.valorimpostoimportacao, item.valorpisretido, item.valorcofinsretido, item.naturezabcc, item.valordesconto, " +
						"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.nome")
				.leftOuterJoin("entregadocumento.empresa empresa")
				.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
				.leftOuterJoin("entregadocumento.entrega entrega")
				.leftOuterJoin("entrega.aux_entrega aux_entrega")
				.leftOuterJoin("entregadocumento.responsavelfrete responsavelfrete")
				.leftOuterJoin("entregadocumento.fornecedor fornecedor")
				.join("entregadocumento.listaEntregamaterial item")
				.leftOuterJoin("item.cfop cfop")
				.join("item.material material")
				.join("item.unidademedidacomercial unidademedidacomercial")
				.where("modelodocumentofiscal.spedpiscofins_d500 = ?", Boolean.TRUE)
				.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
				.where("empresa = ?", empresa)
				.where(getSqlDtemissaoDtentrada() + " >= ?", filtro.getDtinicio())
				.where(getSqlDtemissaoDtentrada() + " <= ?", filtro.getDtfim());
		
		query.where("cfop.codigo <> '5908' and cfop.codigo <> '1909' ");
		
		return query.list();
	}
	
	/**
	* M�todo que busca as entradas fiscais para a integra��o SAGE
	*
	* @param filtro
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public List<Entregadocumento> findForSagefiscal(SagefiscalFiltro filtro) {
		QueryBuilder<Entregadocumento> query = querySined()
			
			.select("entregadocumento.numero, entregadocumento.cdentregadocumento, entregadocumento.situacaodocumento," +
					"entregadocumento.dtentrada, entregadocumento.valor, entregadocumento.indpag, entregadocumento.serie, " +
					"entregadocumento.infoadicionalfisco, entregadocumento.natop, responsavelfrete.cdResponsavelFrete, responsavelfrete.cdnfe, fornecedor.cdpessoa, fornecedor.cpf, fornecedor.cnpj, " +
					"entrega.cdentrega, entregadocumento.dtemissao, entregadocumento.valordesconto, entregadocumento.valor, " +
					"entregadocumento.valorfrete, entregadocumento.valorseguro, entregadocumento.valoroutrasdespesas, " +
					"entregadocumento.tipotitulo, entregadocumento.placaveiculo, item.valoriss, item.valorbciss, item.iss, item.tipotributacaoiss, " +
					"entregadocumento.descricaotitulo, entregadocumento.numerotitulo, " +
					"material.cdmaterial, material.identificacao, material.origemproduto, material.peso, material.pesobruto, " +
					"unidademedidacomercial.cdunidademedida, unidademedidacomercial.simbolo, item.qtde, item.valorunitario, " +
					"item.csticms, cfop.cdcfop, cfop.codigo, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, item.icmsst, " +
					"item.valoricmsst, item.cstipi, item.valorbcipi, item.ipi, item.valoripi, item.cstpis, item.valorbcpis, item.pis, item.qtdebcpis, " +
					"item.valorunidbcpis, item.valorpis, item.cstcofins, item.valorbccofins, item.cofins, item.qtdebccofins, item.valorunidbccofins, " +
					"item.valorcofins, entregadocumento.simplesremessa, entregadocumento.chaveacesso, item.valorir, item.valorcsll, item.valorinss, " +
					"item.valorimpostoimportacao, item.valorpisretido, item.valorcofinsretido, item.valordesconto, item.valorFiscalIcms, item.valorFiscalIpi, " +
					"uf.cduf, uf.sigla, item.valorfrete, item.valoroutrasdespesas, item.naturezabcc, " +
					"listaEntregadocumentoreferenciado.modelodocumento, listaEntregadocumentoreferenciado.numero, " +
					"listaEntregadocumentoreferenciado.autenticacaobancaria, listaEntregadocumentoreferenciado.valor, " +
					"listaEntregadocumentoreferenciado.dtvencimento, listaEntregadocumentoreferenciado.dtpagamento," +
					"uf.cduf, ufEdr.sigla, modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.spedpiscofins_f100, " +
					"pais.cdpais, naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome, " +
					"modelodocumentofiscal.nome, modelodocumentofiscal.sintegra_71, " +
					"listaAjustefiscal.cdajustefiscal, " +
					"listaAjustefiscal.codigoajuste, " +
					"listaAjustefiscal.descricaocomplementar, listaAjustefiscal.aliquotaicms, " +
					"listaAjustefiscal.basecalculoicms, listaAjustefiscal.valoricms, " +
					"listaAjustefiscal.outrosvalores, " +
					"obslancamentofiscal.cdobslancamentofiscal, obslancamentofiscal.descricao, " +
					"materialajuste.cdmaterial, materialajuste.nome, materialajuste.identificacao, " +
					"entregadocumentovinculo.cdentregadocumento, entregadocumentovinculo.serie, entregadocumentovinculo.subserie, entregadocumentovinculo.numero, " +
					"entregadocumentovinculo.dtemissao, fornecedorVinculo.cdpessoa, fornecedorVinculo.cpf, fornecedorVinculo.cnpj, modelodocumentofiscalVinculo.cdmodelodocumentofiscal, modelodocumentofiscalVinculo.codigo," +
					"unidademedidaMaterial.cdunidademedida, unidademedidaMaterial.simbolo, " +
					"grupotributacao.cdgrupotributacao, grupotributacao.tipocobrancaipi, grupotributacao.valorFiscalIcms, grupotributacao.valorFiscalIpi, grupotributacao.tipocobrancaicms ")
			.leftOuterJoin("entregadocumento.empresa empresa")
			.leftOuterJoin("entregadocumento.naturezaoperacao naturezaoperacao")
			.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
			.leftOuterJoin("entregadocumento.entrega entrega")
			.leftOuterJoin("entrega.aux_entrega aux_entrega")
			.leftOuterJoin("entregadocumento.responsavelfrete responsavelfrete")
			.leftOuterJoin("entregadocumento.fornecedor fornecedor")
			.join("entregadocumento.listaEntregamaterial item")
			.leftOuterJoin("item.cfop cfop")
			.leftOuterJoin("item.grupotributacao grupotributacao")
			.join("item.material material")
			.join("item.unidademedidacomercial unidademedidacomercial")
			.join("material.unidademedida unidademedidaMaterial")
			.leftOuterJoin("fornecedor.listaEndereco listaEndereco")
			.leftOuterJoin("listaEndereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("listaEndereco.pais pais")
			.leftOuterJoin("entregadocumento.listaEntregadocumentoreferenciado listaEntregadocumentoreferenciado")
			.leftOuterJoin("listaEntregadocumentoreferenciado.uf ufEdr")
			.leftOuterJoin("entregadocumento.listaAjustefiscal listaAjustefiscal")
			.leftOuterJoin("listaAjustefiscal.obslancamentofiscal obslancamentofiscal")
			.leftOuterJoin("listaAjustefiscal.material materialajuste")
			.leftOuterJoin("entregadocumento.listaEntregadocumentofrete listaEntregadocumentofrete")
			.leftOuterJoin("listaEntregadocumentofrete.entregadocumentovinculo entregadocumentovinculo")
			.leftOuterJoin("entregadocumentovinculo.fornecedor fornecedorVinculo")
			.leftOuterJoin("entregadocumentovinculo.modelodocumentofiscal modelodocumentofiscalVinculo")
			.openParentheses()
				.where("coalesce(modelodocumentofiscal.enviospedfiscal, false) = true")
				.or()
				.where("coalesce(modelodocumentofiscal.enviospedpiscofins, false) = true")
			.closeParentheses()
			.openParentheses()
				.where("empresa = ?", filtro.getEmpresa()).or()
				.where("empresa is null")
			.closeParentheses()	
			.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
			.where(getSqlDtemissaoDtentrada() + " >= ?", filtro.getDtinicio())
			.where(getSqlDtemissaoDtentrada() + " <= ?", filtro.getDtfim());
		
		return query.list();
	}
	
	/**
	 * 
	 * M�todo para fazer o update na entrega quando o retorno � feito do WMS
	 *
	 *@author Thiago Augusto
	 *@date 19/01/2012
	 * @param bean
	 */
	public void updateRetornoWMS(Entregadocumento bean){
		StringBuilder sql = new StringBuilder()
		.append("update Entregadocumento ed set ed.retornowms = ? " +
				"where ed.cdentregadocumento = "+bean.getCdentregadocumento());
	
		getHibernateTemplate().bulkUpdate(sql.toString(), new Object[]{bean.getRetornowms()});
	}

	/**
	 * M�todo que carrega a entregadocumento com o rateio
	 *
	 * @param entregadocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public Entregadocumento findRateioByEntregadocumento(Entregadocumento entregadocumento) {
		if(entregadocumento == null || entregadocumento.getCdentregadocumento() == null)
			throw new SinedException("Entrada fiscal n�o pode ser nula.");
		
		return query()
				.select("entregadocumento.cdentregadocumento, entrega.cdentrega, ordemcompra.cdordemcompra, " +
						"rateioordemcompra.cdrateio, " +
						"rateio.cdrateio, listaRateioitem.cdrateioitem, listaRateioitem.percentual, listaRateioitem.valor, " +
						"centrocusto.cdcentrocusto, projeto.cdprojeto, contagerencial.cdcontagerencial, contagerencial.nome, " +
						"vcontagerencial.identificador, vcontagerencial.arvorepai")
				.join("entregadocumento.rateio rateio")
				.join("rateio.listaRateioitem listaRateioitem")
				.leftOuterJoin("entregadocumento.entrega entrega")
				.leftOuterJoin("entrega.listaOrdemcompraentrega listaOrdemcompraentrega")
				.leftOuterJoin("listaOrdemcompraentrega.ordemcompra ordemcompra")
				.leftOuterJoin("ordemcompra.rateio rateioordemcompra")
				.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
				.leftOuterJoin("listaRateioitem.projeto projeto")
				.leftOuterJoin("listaRateioitem.contagerencial contagerencial")
				.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
				.where("entregadocumento = ?", entregadocumento)
				.unique();
	}

	/**
	 * M�todo que verifica se existe fornecedor e n�mero j� cadastrado
	 *
	 * @param ed
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean existFornecedorNumero(Entregadocumento ed) {
		if(ed == null)
			return false;
		
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(Entregadocumento.class)
					.join("entregadocumento.fornecedor fornecedor")
					.leftOuterJoin("entregadocumento.empresa empresa")
					.leftOuterJoin("entregadocumento.entrega entrega")
					.leftOuterJoin("entrega.aux_entrega aux_entrega")
					.openParentheses()
						.where("empresa = ?", ed.getEmpresa()).or()
						.where("empresa is null")
					.closeParentheses()
					.where("fornecedor = ?", ed.getFornecedor())
					.where("entregadocumento.numero = ?", ed.getNumero())
					.where("entregadocumento.serie = ?", ed.getSerie())
					.where("entregadocumento.modelodocumentofiscal.codigo = ?", ed.getModelodocumentofiscal()!=null ? ed.getModelodocumentofiscal().getCodigo() : null)
					.where("entregadocumento.cdentregadocumento <> ?", ed.getCdentregadocumento())
					.openParentheses()
						.where("aux_entrega.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
						.or()
						.openParentheses()
							.where("aux_entrega is null")
							.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
						.closeParentheses()
					.closeParentheses()
					.unique() > 0;
	}

	/**
	 * M�todo que busca a entregadocumento para ser exclu�da. O notWhereInEntregadocumento passado por par�metro, s�o as entregadocumento que n�o ser�o exclu�das
	 * Obs: utilizado quando a entregadocumento � removida no recebimento
	 *
	 * @param entrega
	 * @param notWhereInEntregadocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForDeleteManual(Entrega entrega, String notWhereInEntregadocumento) {
		if(entrega == null || entrega.getCdentrega() == null)
			throw new SinedException("Entrega n�o pode ser nula.");
		
		if(notWhereInEntregadocumento != null && !"".equals(notWhereInEntregadocumento)){
			return query()
			.select("entregadocumento.cdentregadocumento, entrega.cdentrega, listadocumento.cdentregapagamento, " +
					"listaEntregamaterial.cdentregamaterial ")
			.join("entregadocumento.entrega entrega")
			.join("entrega.aux_entrega aux_entrega")
			.leftOuterJoin("entregadocumento.listadocumento listadocumento")
			.leftOuterJoin("entregadocumento.listaEntregamaterial listaEntregamaterial")
			.where("entrega = ?", entrega)
			.where("aux_entrega.situacaosuprimentos = ?", Situacaosuprimentos.EM_ABERTO)
			.where("entregadocumento.cdentregadocumento not in (" + notWhereInEntregadocumento + ")")
			.list();
		}else {
			return query()
				.select("entregadocumento.cdentregadocumento, entrega.cdentrega, listadocumento.cdentregapagamento, " +
					"listaEntregamaterial.cdentregamaterial ")
				.join("entregadocumento.entrega entrega")
				.join("entrega.aux_entrega aux_entrega")
				.leftOuterJoin("entregadocumento.listadocumento listadocumento")
				.leftOuterJoin("entregadocumento.listaEntregamaterial listaEntregamaterial")
				.where("entrega = ?", entrega)
				.where("aux_entrega.situacaosuprimentos = ?", Situacaosuprimentos.EM_ABERTO)
				.list();
		}
	}
	
	/**
	 * M�todo que busca as entradas fiscais para o total da listagem
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 * @since 23/01/2014
	 */
	public List<Entregadocumento> findForTotalListagem(EntradafiscalFiltro filtro) {
		QueryBuilder<Entregadocumento> query = query();
		updateListagemQuery(query, filtro);
		query.select("distinct entregadocumento.cdentregadocumento, entregadocumento.valor");
		return query.list();
	}

	/**
	 * M�todo que busca os dados da entregadocumento para gerar csv
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForCsv(EntradafiscalFiltro filtro) {
		QueryBuilder<Entregadocumento> query = querySined();
		updateListagemQuery(query, filtro);
		return query.list();
	}

	public List<Entregadocumento> loadWithLista(String whereIn, String orderBy, boolean asc) {
		QueryBuilder<Entregadocumento> query = query()
			.select("entregadocumento.cdentregadocumento, entregadocumento.entregadocumentosituacao, entregadocumento.serie, " +
					"entregadocumento.valordesconto, entregadocumento.valorfrete, entregadocumento.dtentrada, entregadocumento.numero, " +
					"entregadocumento.dtemissao, entregadocumento.numerotitulo, entregadocumento.retornowms, entregadocumento.tipotitulo, " +
					"entregadocumento.descricaotitulo, entregadocumento.valor, entregadocumento.dtentrada, entregadocumento.dtemissao," +
					"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.nome, "	+		
					"listaEntregamaterial.cdentregamaterial, listaEntregamaterial.valorunitario, listaEntregamaterial.csticms, " +
					"listaEntregamaterial.valorbcicms, listaEntregamaterial.valoricms, listaEntregamaterial.valoripi, listaEntregamaterial.qtde, " +
					"listaEntregamaterial.valorbcicmsst, listaEntregamaterial.valoricmsst, listaEntregamaterial.valoroutrasdespesas, " +
					"listaEntregamaterial.valorseguro, listaEntregamaterial.faturamentocliente,   " +
					"listaEntregamaterial.valordesconto, listaEntregamaterial.valorpis, listaEntregamaterial.valorcofins, " +
					"material.cdmaterial, material.nome, material.identificacao, unidademedida.cdunidademedida, unidademedida.simbolo, " +
					"cfop.codigo, fornecedor.nome, responsavelfrete.nome, contagerencial.nome, empresa.nomefantasia, entregadocumento.situacaodocumento, entregadocumento.valorseguro," +
					"listaEntregamaterial.valorbcipi, listaEntregamaterial.valorbciss, listaEntregamaterial.valoriss, listaEntregamaterial.valorbcpis," +
					"listaEntregamaterial.valorbccofins")
			.leftOuterJoin("entregadocumento.fornecedor fornecedor")
			.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
			.leftOuterJoin("entregadocumento.responsavelfrete responsavelfrete")
			.join("entregadocumento.listaEntregamaterial listaEntregamaterial")
			.leftOuterJoin("listaEntregamaterial.cfop cfop")
			.leftOuterJoin("listaEntregamaterial.material material")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("entregadocumento.rateio rateio")
			.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
			.leftOuterJoin("listaRateioitem.contagerencial contagerencial")
			.leftOuterJoin("entregadocumento.empresa empresa")
			.whereIn("entregadocumento.cdentregadocumento", whereIn);
		
		
	//	Material, CFOP, CST, quantidade e valor
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		}else{
			query.orderBy("entregadocumento.cdentregadocumento desc");
		}
		
		return query.list();
	}

	/**
	 * M�todo que busca os dados da entregadocumento para apura��o de pis/cofins
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findforApuracaopiscofins(ApuracaopiscofinsFiltro filtro) {
		if(filtro == null)
			throw new SinedException("Filtro n�o pode ser nulo");
		
		QueryBuilder<Entregadocumento> query = querySined()
				.select("entregadocumento.numero, modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, " +
						"entregadocumento.dtentrada, entregadocumento.valor, entregadocumento.indpag, entregadocumento.serie, " +
						"entregadocumento.infoadicionalfisco, entregadocumento.natop, responsavelfrete.cdnfe, " +
						"entrega.cdentrega, entregadocumento.dtemissao, entregadocumento.valordesconto, entregadocumento.valor, " +
						"entregadocumento.valorfrete, entregadocumento.valorseguro, entregadocumento.valoroutrasdespesas, " +
						"entregadocumento.tipotitulo, " +
						"entregadocumento.descricaotitulo, entregadocumento.numerotitulo, material.cdmaterial, unidademedidacomercial.cdunidademedida, item.qtde, item.valorunitario, " +
						"item.csticms, cfop.cdcfop, cfop.codigo, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, item.icmsst, " +
						"item.valoricmsst, item.cstipi, item.valorbcipi, item.ipi, item.valoripi, item.cstpis, item.valorbcpis, item.pis, item.qtdebcpis, " +
						"item.valorunidbcpis, item.valorpis, item.cstcofins, item.valorbccofins, item.cofins, item.qtdebccofins, item.valorunidbccofins, " +
						"item.valorcofins, entregadocumento.simplesremessa, entregadocumento.chaveacesso, item.naturezabcc, item.valorir, item.valorcsll, item.valorinss, " +
						"item.valorimpostoimportacao, item.valordesconto, " +
						"fornecedor.cdpessoa, fornecedor.nome, fornecedor.cnpj, fornecedor.cpf, fornecedor.razaosocial ")
				.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
				.leftOuterJoin("entregadocumento.empresa empresa")
				.leftOuterJoin("entregadocumento.entrega entrega")
				.leftOuterJoin("entrega.aux_entrega aux_entrega")
				.leftOuterJoin("entregadocumento.responsavelfrete responsavelfrete")
				.leftOuterJoin("entregadocumento.fornecedor fornecedor")
				.join("entregadocumento.listaEntregamaterial item")
				.leftOuterJoin("item.cfop cfop")
				.join("item.material material")
				.join("item.unidademedidacomercial unidademedidacomercial")	
				.where("modelodocumentofiscal.apuracaopiscofins = ?", Boolean.TRUE)
				.openParentheses()
					.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
					.or()
					.where("entregadocumento.entregadocumentosituacao is null")
				.closeParentheses()
				.openParentheses()
					.where("aux_entrega.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
					.or()
					.where("aux_entrega is null")
				.closeParentheses()
				.where("entregadocumento.empresa = ?", filtro.getEmpresa());
		
		addWhereDtemissaoDtentrada(query, filtro.getDtinicio(), filtro.getDtfim());
				
		return query
				.orderBy(getSqlDtemissaoDtentrada())
				.list();
	
	}

	private void addWhereDtemissaoDtentrada(QueryBuilder<Entregadocumento> query, Date dtinicio, Date dtfim) {
		if(parametrogeralService.getBoolean(Parametrogeral.SPED_DATA_EMISSAO)){
			query
				.where("entregadocumento.dtemissao >= ?", dtinicio)
				.where("entregadocumento.dtemissao <= ?", dtfim);
		}else {
			query
			.where("entregadocumento.dtentrada >= ?", dtinicio)
			.where("entregadocumento.dtentrada <= ?", dtfim);
		}
	}
	
	/**
	 * M�todo que busca os dados da entrada fiscal para apuracao de ipi
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForApuracaoipi(ApuracaoipiFiltro filtro) {
		if(filtro == null)
			throw new SinedException("Filtro n�o pode ser nulo");
		
		QueryBuilder<Entregadocumento> query = query()
				.select("entregadocumento.numero, " +
						"entregadocumento.dtentrada, entregadocumento.valor, entregadocumento.indpag, entregadocumento.serie, " +
						"entregadocumento.infoadicionalfisco, entregadocumento.natop, responsavelfrete.cdnfe, fornecedor.cdpessoa, " +
						"entrega.cdentrega, entregadocumento.dtemissao, entregadocumento.valordesconto, entregadocumento.valor, " +
						"entregadocumento.valorfrete, entregadocumento.valorseguro, entregadocumento.valoroutrasdespesas, " +
						"entregadocumento.tipotitulo, " +
						"entregadocumento.descricaotitulo, entregadocumento.numerotitulo, material.cdmaterial, unidademedidacomercial.cdunidademedida, item.qtde, item.valorunitario, " +
						"item.csticms, cfop.cdcfop, cfop.codigo, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, item.icmsst, " +
						"item.valoricmsst, item.cstipi, item.valorbcipi, item.ipi, item.valoripi, item.cstpis, item.valorbcpis, item.pis, item.qtdebcpis, " +
						"item.valorunidbcpis, item.valorpis, item.cstcofins, item.valorbccofins, item.cofins, item.qtdebccofins, item.valorunidbccofins, " +
						"item.valorcofins, entregadocumento.simplesremessa, entregadocumento.chaveacesso, item.naturezabcc, item.valorir, item.valorcsll, item.valorinss, " +
						"item.valorimpostoimportacao, item.valoroutrasdespesas, cfopescopo.cdcfopescopo, cfopescopo.descricao, item.valorfrete, item.valordesconto, item.valorseguro, " +
						"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.nome, item.valorFiscalIpi, item.valorFiscalIcms")
				.leftOuterJoin("entregadocumento.empresa empresa")
				.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
				.leftOuterJoin("entregadocumento.entrega entrega")
				.leftOuterJoin("entrega.aux_entrega aux_entrega")
				.leftOuterJoin("entregadocumento.responsavelfrete responsavelfrete")
				.leftOuterJoin("entregadocumento.fornecedor fornecedor")
				.join("entregadocumento.listaEntregamaterial item")
				.join("item.cfop cfop")
				.join("cfop.cfopescopo cfopescopo")
				.join("item.material material")
				.join("item.unidademedidacomercial unidademedidacomercial")	
				.where("modelodocumentofiscal.apuracaoipi = ?", Boolean.TRUE)
				.openParentheses()
					.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
					.or()
					.where("entregadocumento.entregadocumentosituacao is null")
				.closeParentheses()
				.openParentheses()
					.where("aux_entrega.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
					.or()
					.where("aux_entrega is null")
				.closeParentheses()
				.where("entregadocumento.empresa = ?", filtro.getEmpresa());
				
			addWhereDtemissaoDtentrada(query, filtro.getDtinicio(), filtro.getDtfim());
			
			return query
				.orderBy(getSqlDtemissaoDtentrada())
				.list();
	}

	/**
	 * M�todo que busca os dados da entrada fiscal para apuracao de icms
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForApuracaoicms(ApuracaoicmsFiltro filtro) {
		if(filtro == null)
			throw new SinedException("Filtro n�o pode ser nulo");
		
		QueryBuilder<Entregadocumento> query = query()
				.select("entregadocumento.numero, " +
						"entregadocumento.dtentrada, entregadocumento.valor, entregadocumento.indpag, entregadocumento.serie, " +
						"entregadocumento.infoadicionalfisco, entregadocumento.natop, responsavelfrete.cdnfe, fornecedor.cdpessoa, " +
						"entrega.cdentrega, entregadocumento.dtemissao, entregadocumento.valordesconto, entregadocumento.valor, " +
						"entregadocumento.valorfrete, entregadocumento.valorseguro, entregadocumento.valoroutrasdespesas, " +
						"entregadocumento.tipotitulo, " +
						"entregadocumento.descricaotitulo, entregadocumento.numerotitulo, material.cdmaterial, unidademedidacomercial.cdunidademedida, item.qtde, item.valorunitario, " +
						"item.csticms, cfop.cdcfop, cfop.codigo, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, item.icmsst, " +
						"item.valoricmsst, item.cstipi, item.valorbcipi, item.ipi, item.valoripi, item.cstpis, item.valorbcpis, item.pis, item.qtdebcpis, " +
						"item.valorunidbcpis, item.valorpis, item.cstcofins, item.valorbccofins, item.cofins, item.qtdebccofins, item.valorunidbccofins, item.valorIcmsDesonerado, " +
						"item.valorcofins, entregadocumento.simplesremessa, entregadocumento.chaveacesso, item.naturezabcc, item.valorir, item.valorcsll, item.valorinss, " +
						"item.valorimpostoimportacao, item.valoroutrasdespesas, cfopescopo.cdcfopescopo, cfopescopo.descricao, item.valorfrete, item.valordesconto, item.valorFiscalIcms, item.valorseguro, " +
						"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.nome, item.valorFiscalIcms, item.valorFiscalIpi")
				.leftOuterJoin("entregadocumento.empresa empresa")
				.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
				.leftOuterJoin("entregadocumento.entrega entrega")
				.leftOuterJoin("entrega.aux_entrega aux_entrega")
				.leftOuterJoin("entregadocumento.responsavelfrete responsavelfrete")
				.leftOuterJoin("entregadocumento.fornecedor fornecedor")
				.leftOuterJoin("entregadocumento.listaEntregamaterial item")
				.leftOuterJoin("item.cfop cfop")
				.leftOuterJoin("cfop.cfopescopo cfopescopo")
				.leftOuterJoin("item.material material")
				.leftOuterJoin("item.unidademedidacomercial unidademedidacomercial")	
				.where("modelodocumentofiscal.apuracaoicms = ?", Boolean.TRUE)
				.openParentheses()
					.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
					.or()
					.where("entregadocumento.entregadocumentosituacao is null")
				.closeParentheses()
				.openParentheses()
					.where("aux_entrega.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
					.or()
					.where("aux_entrega is null")
				.closeParentheses()
				.where("entregadocumento.empresa = ?", filtro.getEmpresa());
		
		addWhereDtemissaoDtentrada(query, filtro.getDtinicio(), filtro.getDtfim());
		
		return query
			.orderBy(getSqlDtemissaoDtentrada())
			.list();
	}

	/**
	 * M�todo que verifica se a entrada fiscal est� com situa��o diferente de cancelada
	 * 
	 * @param fornecedor
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existEntradafiscalNotCancelada(Integer cdentregadocumento) {
		if(cdentregadocumento == null)
			throw new SinedException("Id da Entrada Fiscal n�o pode ser nulo");
		
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.setUseTranslator(false)
				.from(Entregadocumento.class)
				.where("entregadocumento.cdentregadocumento = ?", cdentregadocumento)
				.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
				.unique() > 0;
	}
	
	/**
	 * M�todo que verifica se o documento j� foi registrado na entrada fiscal
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existEntradafiscalByDocumento(Documento documento) {
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Documento n�o pode ser nulo.");
		
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.setUseTranslator(false)
				.from(Entregadocumento.class)
				.join("entregadocumento.listadocumento listadocumento")
				.join("listadocumento.documentoorigem documentoorigem")
				.where("documentoorigem = ?", documento)
				.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
				.unique() > 0;
	}
	
	/**
	 * M�todo que verifica se a entrada fiscal tem origem da entrega ou da conta a pagar
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isOrigemDocumentoOrEntrega(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.setUseTranslator(false)
				.from(Entregadocumento.class)
				.leftOuterJoin("entregadocumento.listadocumento listadocumento")
				.leftOuterJoin("listadocumento.documentoorigem documentoorigem")
				.leftOuterJoin("entregadocumento.entrega entrega")
				.whereIn("entregadocumento.cdentregadocumento", whereIn)
				.openParentheses()
					.where("documentoorigem is not null")
					.or()
					.where("entrega is not null")
				.closeParentheses()
				.unique() > 0;
	}
	
	/**
	 * M�todo que busca os dados da entrada fiscal para faturar 
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForFaturar(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()		
			.select("entregadocumento.cdentregadocumento, entregadocumento.dtentrada, entregadocumento.dtemissao, entregadocumento.simplesremessa, " +
					"entregadocumento.numero, entregadocumento.valor, entregadocumento.entregadocumentosituacao, entregadocumento.especie, entregadocumento.serie, " +
					"empresa.cdpessoa, fornecedor.cdpessoa,  fornecedor.nome, rateio.cdrateio, listaRateioitem.cdrateioitem, " +
					"listaRateioitem.percentual, listaRateioitem.valor, centrocusto.cdcentrocusto, projeto.cdprojeto, " +
					"contagerencial.cdcontagerencial, listadocumento.cdentregapagamento, listadocumento.valor, " +
					"listadocumento.dtvencimento, listadocumento.numero, prazopagamento.cdprazopagamento, documentotipo.cddocumentotipo, " +
					"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.nome, " +
					"documentoantecipacao.cddocumento")
			.leftOuterJoin("entregadocumento.documentotipo documentotipo")
			.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
			.leftOuterJoin("entregadocumento.empresa empresa")
			.leftOuterJoin("entregadocumento.fornecedor fornecedor")
			.leftOuterJoin("entregadocumento.rateio rateio")
			.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
			.leftOuterJoin("listaRateioitem.contagerencial contagerencial")
			.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
			.leftOuterJoin("listaRateioitem.projeto projeto")
			.leftOuterJoin("entregadocumento.listadocumento listadocumento")
			.leftOuterJoin("listadocumento.documentoantecipacao documentoantecipacao")
			.leftOuterJoin("entregadocumento.listaEntregamaterial listaEntregamaterial")
			.leftOuterJoin("listaEntregamaterial.material material")
			.leftOuterJoin("entregadocumento.prazopagamento prazopagamento")
			.leftOuterJoin("listaEntregamaterial.materialclasse materialclasse")
			.leftOuterJoin("listaEntregamaterial.loteestoque loteestoque")
			.leftOuterJoin("material.unidademedida unidademedida")
			.whereIn("entregadocumento.cdentregadocumento", whereIn)
			.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.CANCELADA)
			.where("entregadocumento.entregadocumentosituacao <> ?", Entregadocumentosituacao.FATURADA)
			.orderBy("entregadocumento.cdentregadocumento, listadocumento.cdentregapagamento")
			.list();
	}
	
	/**
	 * M�todo que atualiza a situa��o da entrada fiscal
	 *
	 * @param entregadocumento
	 * @param situacao
	 * @author Luiz Fernando
	 */
	public void updateSituacaoEntradafiscal(String whereIn, Entregadocumentosituacao situacao) {
		if(whereIn != null && !whereIn.equals("") && situacao != null){
			getHibernateTemplate().bulkUpdate("update Entregadocumento ed set entregadocumentosituacao = ? where ed.cdentregadocumento in ( " + whereIn + ")", 
				situacao);
		}
	}
	
	/**
	 * M�todo que verifica se existe fornecedor diferente das entradas fiscais
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public Boolean isFornecedorDiferente(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		String sql = "select ed.cdfornecedor as cdpessoa " +
					 "from entregadocumento ed " +
					 "where ed.cdentregadocumento in (" + whereIn + ") " +
					 "group by ed.cdfornecedor";
		
		List<Integer> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getInt("cdpessoa");
			}
		});
		
		if(list != null && list.size() > 1)
			return true;
		
		return false;
	}
	
	/**
	 * M�todo que busca as entradas fiscais para gerar devolu��o
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForDevolucaoEntradafiscal(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Entregadocumento> query = query();
		this.updateEntradaQuery(query);
		
		return query
			.whereIn("entregadocumento.cdentregadocumento", whereIn)
			.list();
	}
	
	/**
	 * M�todo que busca as entradas fiscais para apura��o de outros impostos
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForApuracaoimposto(ApuracaoimpostoFiltro filtro) {
		if(filtro == null)
			throw new SinedException("Par�metro inv�lido");
		
		QueryBuilder<Entregadocumento> query = querySined()
			.select("fornecedor.nome, fornecedor.cpf, fornecedor.cnpj, entregadocumento.entregadocumentosituacao, " +
					"entregadocumento.valordesconto, entregadocumento.valorfrete, entregadocumento.dtentrada, " +
					"entregadocumento.dtemissao, entregadocumento.cdentregadocumento, " +
					"listaEntregamaterial.qtde, listaEntregamaterial.valorunitario, material.nome, " +
					"listaEntregamaterial.csticms, cfop.codigo,entregadocumento.valor, " +
					"listaEntregamaterial.valorbcicms, listaEntregamaterial.valoricms, listaEntregamaterial.valordesconto, " +
					"listaEntregamaterial.valorbcicmsst, listaEntregamaterial.valoricmsst, listaEntregamaterial.valoroutrasdespesas, " +
					"listaEntregamaterial.valorseguro, entregadocumento.tipotitulo, entregadocumento.descricaotitulo, " +
					"entregadocumento.numerotitulo, listaEntregamaterial.valoripi, listaEntregamaterial.valorpis, listaEntregamaterial.valorcofins, " +
					"entregadocumento.serie, entregadocumento.numero, " +
					"entregadocumento.dtentrada, responsavelfrete.nome, listaEntregamaterial.valorimpostoimportacao," +
					"listaEntregamaterial.valorir, listaEntregamaterial.valorcsll, listaEntregamaterial.iss," +
					"listaEntregamaterial.valorbciss, listaEntregamaterial.valoriss, listaEntregamaterial.valorinss, listaEntregamaterial.tipotributacaoiss, " +
					"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.nome")			
			.leftOuterJoin("entregadocumento.fornecedor fornecedor")
			.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
			.join("entregadocumento.empresa empresa")
			.leftOuterJoin("entregadocumento.responsavelfrete responsavelfrete")
			.join("entregadocumento.listaEntregamaterial listaEntregamaterial")
			.leftOuterJoin("listaEntregamaterial.cfop cfop")
			.leftOuterJoin("listaEntregamaterial.material material")
			.where("empresa = ?", filtro.getEmpresa());
		
		addWhereDtemissaoDtentrada(query, filtro.getDtinicio(), filtro.getDtfim());
		
		if(filtro.getFaixaimpostoapuracao() != null){
			if(Faixaimpostoapuracao.ISS.equals(filtro.getFaixaimpostoapuracao())){
				query
					.where("listaEntregamaterial.tipotributacaoiss = ?", Tipotributacaoiss.RETIDA);
			}else if(Faixaimpostoapuracao.CSLL.equals(filtro.getFaixaimpostoapuracao())){
				query.where("listaEntregamaterial.valorcsll is not null and listaEntregamaterial.valorcsll > 0");
			}else if(Faixaimpostoapuracao.INSS.equals(filtro.getFaixaimpostoapuracao())){
				query.where("listaEntregamaterial.valorinss is not null and listaEntregamaterial.valorinss > 0");
			}else if(Faixaimpostoapuracao.IR.equals(filtro.getFaixaimpostoapuracao())){
				query.where("listaEntregamaterial.valorir is not null and listaEntregamaterial.valorir > 0");
			}else if(Faixaimpostoapuracao.IMPORTACAO.equals(filtro.getFaixaimpostoapuracao())){
				query.where("listaEntregamaterial.valorimpostoimportacao is not null and listaEntregamaterial.valorimpostoimportacao > 0");
			}
		}
		
		return	query
					.orderBy(getSqlDtemissaoDtentrada())
					.list();
	}
	
	public List<Entregadocumento> findForCTRC(String whereIn, String whereNotIn) {
		return query()
				.select("entregadocumento.cdentregadocumento, entregadocumento.numero, entregadocumento.dtentrada, " +
						"entregadocumento.dtemissao, entregadocumento.situacaodocumento, entregadocumento.valor, " +
						"modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo, modelodocumentofiscal.nome")
				.leftOuterJoin("entregadocumento.modelodocumentofiscal modelodocumentofiscal")
				.whereIn("entregadocumento.cdentregadocumento", whereIn)
				.where("entregadocumento.cdentregadocumento not in (" + whereNotIn + ")", (whereNotIn!=null && !whereNotIn.trim().equals("")))
				.orderBy("entregadocumento.cdentregadocumento desc")
				.list();
	}
	
	/**
	* M�todo que carrega a entrada fiscal para calcular o valor da entrada de cada item
	*
	* @param entrega
	* @return
	* @since 27/08/2014
	* @author Luiz Fernando
	 * @param entregadocumento 
	*/
	public List<Entregadocumento> findForCalcularEntradaMaterial(Entrega entrega, String whereInEntregadocumento) {
		if((entrega == null || entrega.getCdentrega() == null) && StringUtils.isBlank(whereInEntregadocumento))
			throw new SinedException("Par�metro inv�lido");
		
		QueryBuilder<Entregadocumento> query = query();
		updateEntradaQuery(query);
		
		query.where("entrega = ?", entrega);
		if(StringUtils.isNotBlank(whereInEntregadocumento)){
			query.whereIn("entregadocumento.cdentregadocumento", whereInEntregadocumento);
		}
		
		return query.list();
	}
	
	/**
	* M�todo que carrega a entrada fiscal com a entrega
	*
	* @param entregadocumento
	* @return
	* @since 29/08/2014
	* @author Luiz Fernando
	*/
	public Entregadocumento loadWithEntrega(Entregadocumento entregadocumento){
		if(entregadocumento == null || entregadocumento.getCdentregadocumento() == null)
			throw new SinedException("Par�metro inv�lido");
		
		return query()
				.select("entregadocumento.cdentregadocumento, entrega.cdentrega")
				.leftOuterJoin("entregadocumento.entrega entrega")
				.where("entregadocumento = ?", entregadocumento)
				.unique();
	}
	
	/**
	 * M�todo que busca uma lista de entradas fiscais de acordo com a chave de acesso informada.
	 * 
	 * @param chaveacesso
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Entregadocumento> findByChaveacesso(String chaveacesso){
		return query()
				.select("entregadocumento.cdentregadocumento, entregadocumento.numero, entregadocumento.serie, entregadocumento.subserie, " +
						"entregadocumento.modelodocumentofiscal, entregadocumento.dtemissao, " +
						"fornecedor.cdpessoa, fornecedor.cpf, fornecedor.cnpj")
				.join("entregadocumento.fornecedor fornecedor")
				.where("entregadocumento.chaveacesso = ?", chaveacesso)
				.list();
	}
	
	/**
	 * M�todo que carrega as entregadocumentos selecionados na tela de confer�ncia de CTE
	 * 
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Entregadocumento> findAllForConferenciaCte(String whereIn){
		if(whereIn == null || whereIn.trim().isEmpty()){
			return null;
		}
		
		return query()
				.select("entregadocumento.cdentregadocumento, entregadocumento.numero")
				.whereIn("entregadocumento.cdentregadocumento", whereIn)
				.orderBy("entregadocumento.numero ASC")
				.list();
	}
	
	/**
	* M�todo que carrega a entrada fiscal para registrar ajuste fiscal
	*
	* @param entregadocumento
	* @return
	* @since 06/01/2015
	* @author Luiz Fernando
	*/
	public Entregadocumento carregarDocumentoParaAjusteFiscal(Entregadocumento entregadocumento) {
		if(entregadocumento == null || entregadocumento.getCdentregadocumento() == null)
			throw new SinedException("Par�metro inv�lido");
		
		return query()
				.select("entregadocumento.cdentregadocumento, listaEntregamaterial.valorbcicms, listaEntregamaterial.icms, " +
						"material.cdmaterial, material.nome, material.identificacao ")
				.join("entregadocumento.listaEntregamaterial listaEntregamaterial")
				.join("listaEntregamaterial.material material")
				.where("entregadocumento = ?", entregadocumento)
				.unique();
	}
	
	/**
	* M�todo que carrega a entrada fiscal para para o registro K200
	*
	* @param material
	* @param filtro	
	* @return 
	* @since 06/01/2015
	* @author Jo�o Vitor
	*/
	public List<Entregadocumento> findForSpedRegK200(Material material, SpedarquivoFiltro filtro) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido");
		
		return querySined()//FIXME ADICIONAR MAIS UM PARAMETRO PARA EVITAR FORNECEDORES DUPLICADOS PARA O MESMO PRODUTO
				
				.select("entregadocumento.cdentregadocumento, fornecedor.cdpessoa, material.cdmaterial, material.identificacao")
				.join("entregadocumento.listaEntregamaterial listaEntregamaterial")
				.join("listaEntregamaterial.material material")
				.join("entregadocumento.fornecedor fornecedor")
				.where("material = ?", material)
				.where("entregadocumento.dtemissao >= ?", filtro.getDtinicio())
				.where("entregadocumento.dtemissao <= ?", filtro.getDtfim())
				.list();
	}
	
	/**
	* M�todo que busca as entradas fiscais de acordo com o documento
	*
	* @param whereInDocumento
	* @return
	* @since 01/11/2016
	* @author Luiz Fernando
	*/
	public List<Entregadocumento> findByDocumento(String whereInDocumento) {
		if(StringUtils.isBlank(whereInDocumento))
			throw new SinedException("Par�metro inv�lido");
		
		return query()
				.select("entregadocumento.cdentregadocumento, entregadocumento.entregadocumentosituacao")
				.leftOuterJoin("entregadocumento.entrega entrega")
				.openParentheses()
					.where("entrega.cdentrega in (   select e.cdentrega " +
															" from Documentoorigem doco " +
															" join doco.entrega e " +
															" where doco.documento.cddocumento in (" + whereInDocumento + ") " +
															" )")
					.or()
					.where("entregadocumento.cdentregadocumento in ( select ed.cdentregadocumento " +
																	" from Documentoorigem doco " +
																	" join doco.entregadocumento ed " +
																	" where doco.documento.cddocumento in (" + whereInDocumento + ") " +
																	" )")
				.closeParentheses()
				.list();
	}
	
	public boolean haveEntradafiscalSituacao(String whereIn, boolean not, Entregadocumentosituacao[] situacoes) {
		List<Entregadocumentosituacao> lista = new ArrayList<Entregadocumentosituacao>();
		for (int i = 0; i < situacoes.length; i++) {
			lista.add(situacoes[i]);
		}
		
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Entregadocumento.class)
					.whereIn("entregadocumento.cdentregadocumento", whereIn)
					.where("entregadocumento.entregadocumentosituacao " + (not ? " not " : "") + " in (" + Entregadocumentosituacao.listAndConcatenate(lista) + ")");
		
		return query.unique() > 0;
	}

	public boolean existeNotaDevolucao(Entregadocumento entregadocumento) {
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Entregadocumento.class)
			.join("entregadocumento.listaEntregadocumentohistorico listaEntregadocumentohistorico")
			.join("listaEntregadocumentohistorico.notafiscalproduto notafiscalproduto")
			.join("notafiscalproduto.notaStatus notaStatus")
			.where("entregadocumento = ?", entregadocumento)
			.where("notaStatus <> ?", NotaStatus.CANCELADA)
			.unique() > 0;
	}
	
	public Entregadocumento loadForCancelamento(Entregadocumento entregadocumento){
		return query()
				.select("entregadocumento.cdentregadocumento, entregadocumento.numero, entregadocumento.entregadocumentosituacao, " +
						"entrega.cdentrega, entrega.cdentrega, documentoacaoantecipacao.cddocumentoacao, " +
						"documentoacaoorigem.cddocumentoacao")
				.leftOuterJoin("entregadocumento.entrega entrega")
				.leftOuterJoin("entregadocumento.listadocumento listadocumento")
				.leftOuterJoin("listadocumento.documentoorigem documentoorigem")
				.leftOuterJoin("documentoorigem.documentoacao documentoacaoorigem")
				.leftOuterJoin("listadocumento.documentoantecipacao documentoantecipacao")
				.leftOuterJoin("documentoantecipacao.documentoacao documentoacaoantecipacao")
				.where("entregadocumento = ?", entregadocumento).unique();
	}


	
	@SuppressWarnings("unchecked")
	public List<Entregadocumento> findByEntradaFiscalSemVinculoFinanceiro(Date dateToSearch) {
		StringBuilder sql = new StringBuilder();
		sql.append("select e.cdentregadocumento, e.cdempresa ");
		sql.append("from entregadocumento e ");
		sql.append("join entregamaterial em on em.cdentregadocumento = e.cdentregadocumento ");
		sql.append("where not(e.simplesremessa != true or em.faturamentocliente != true) ");
		sql.append("	and e.dtentrada between '" + dateToSearch + "' and current_date ");		
		
		List<Entregadocumento> entregaDocumentoLista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			
			@Override
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Entregadocumento e = new Entregadocumento(rs.getInt("cdentregadocumento"));
				if(rs.getInt("cdempresa") > 0) e.setEmpresa(new Empresa(rs.getInt("cdempresa")));
				return e;
			}
		});
		
		return entregaDocumentoLista;
	}
	
	public Entregadocumento findEntradaFiscalByNumeroSerie(String numero, String serie) {
		return query()
				.select("entregadocumento.chaveacesso, cfop.codigo, item.valoricms")
				.join("entregadocumento.listaEntregamaterial item")
				.join("item.cfop cfop")
				.where("entregadocumento.numero = ?", numero)
				.where("entregadocumento.serie = ?", serie)
				.unique();
	}
}