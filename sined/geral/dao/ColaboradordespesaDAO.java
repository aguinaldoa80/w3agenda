package br.com.linkcom.sined.geral.dao;


import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradordespesa;
import br.com.linkcom.sined.geral.bean.Colaboradordespesamotivo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesaitemtipo;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesasituacao;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.FluxocaixaFiltroReport;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ColaboradordespesaFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.bean.ColaboradorPagamentoBean;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.ColaboradorPagamentoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

@DefaultOrderBy("colaboradordespesa.dtinsercao desc")
public class ColaboradordespesaDAO extends GenericDAO<Colaboradordespesa> {

	private RateioService rateioService;
	
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Colaboradordespesa> query) {
		query
			.select("colaboradordespesa.cdcolaboradordespesa, colaborador.cdpessoa, colaborador.nome, colaboradordespesamotivo.cdcolaboradordespesamotivo," +
					"colaboradordespesamotivo.descricao, colaboradordespesa.dtdeposito, colaboradordespesa.valor, colaboradordespesa.dtinsercao, " +
					"colaboradordespesaitem.cdcolaboradordespesaitem, colaboradordespesaitem.motivo, colaboradordespesaitem.valor, " +
					"colaboradordespesaitem.tipo, colaboradordespesa.situacao, colaboradordespesahistorico.cdcolaboradordespesahistorico, " +
					"colaboradordespesahistorico.dtaltera, colaboradordespesahistorico.acao, colaboradordespesahistorico.observacao, " +
					"colaboradordespesa.total, colaboradordespesahistorico.cdusuarioaltera, colaboradordespesa.bloqueardebitocredito, " +
					"projeto.cdprojeto, colaboradordespesa.dtholeriteinicio, colaboradordespesa.dtholeritefim, empresa.cdpessoa, " +
					"colaboradordespesa.tipovalorreceber,  " +
					"evento.cdEventoPagamento, evento.nome, evento.tipoEvento, " +
					"rateio.cdrateio")
			.leftOuterJoin("colaboradordespesa.colaborador colaborador")
			.leftOuterJoin("colaboradordespesa.projeto projeto")
			.leftOuterJoin("colaboradordespesa.empresa empresa")
			.leftOuterJoin("colaboradordespesa.listaColaboradordespesaitem colaboradordespesaitem")
			.leftOuterJoin("colaboradordespesaitem.evento evento")
			.leftOuterJoin("colaboradordespesa.colaboradordespesamotivo colaboradordespesamotivo")
			.leftOuterJoin("colaboradordespesa.listaColaboradordespesahistorico colaboradordespesahistorico")
			.leftOuterJoin("colaboradordespesa.rateio rateio")
			.orderBy("colaboradordespesahistorico.dtaltera desc");
			
	}	
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				Colaboradordespesa bean = (Colaboradordespesa) save.getEntity();
				Rateio rateio = bean.getRateio();
				if (rateio != null) {
					//rateioService.limpaReferenciaRateio(rateio);
					rateioService.saveOrUpdate(rateio);
				}
				if (Hibernate.isInitialized(((Colaboradordespesa)save.getEntity()).getListaColaboradordespesaitem()))
					save.saveOrUpdateManaged("listaColaboradordespesaitem");
				return null;
			}
		});
	}
	
	
	/**
	* Atualiza a situa��o do Colaboradordespesa de acordo com o par�metro passado
	*
	* @param colaboradordespesasituacao
	* @param colaboradordespesa
	* @since Jun 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public void updateSituacao(Colaboradordespesasituacao colaboradordespesasituacao, Colaboradordespesa colaboradordespesa){
		if(colaboradordespesa == null || colaboradordespesa.getCdcolaboradordespesa() == null || colaboradordespesasituacao == null ||
				colaboradordespesasituacao.getValue() == null){
			throw new SinedException("Par�metros inv�lidos");
		}		
		
		getHibernateTemplate().bulkUpdate("update Colaboradordespesa c set c.situacao = ? where c.cdcolaboradordespesa = ?",
										new Object[]{colaboradordespesasituacao, colaboradordespesa.getCdcolaboradordespesa()});
		
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Colaboradordespesa> query, FiltroListagem _filtro) {
		
		ColaboradordespesaFiltro filtro = (ColaboradordespesaFiltro) _filtro;
		
		query
			.select("colaboradordespesamotivo.cdcolaboradordespesamotivo, colaboradordespesamotivo.descricao, colaboradordespesa.cdcolaboradordespesa, " +
					"colaborador.nome, colaboradordespesa.valor, colaboradordespesa.dtinsercao, colaboradordespesa.dtdeposito, colaboradordespesa.situacao , colaboradordespesa.total, " +
					"colaboradordespesa.dtholeriteinicio, colaboradordespesa.dtholeritefim, " +
					"empresa.cdpessoa, empresa.nomefantasia")
			.join("colaboradordespesa.colaborador colaborador")
			.join("colaboradordespesa.colaboradordespesamotivo colaboradordespesamotivo")
			.leftOuterJoin("colaboradordespesa.empresa empresa")
			.where("colaborador = ?", filtro.getColaborador())
			.where("colaboradordespesamotivo = ?", filtro.getMotivo())			
			.where("colaboradordespesa.dtdeposito = ?", filtro.getDtdeposito())
			.where("colaboradordespesa.total = ?", filtro.getTotal())
			.where("colaboradordespesa.dtinsercao = ?", filtro.getDtinsercao());
		
		if(filtro.getDtreferenciaHolerite1() != null && filtro.getDtreferenciaHolerite2() != null){
			query.where("colaboradordespesa.dtholeriteinicio <= ?", filtro.getDtreferenciaHolerite2());
			query.where("colaboradordespesa.dtholeritefim >= ?", filtro.getDtreferenciaHolerite1());
		}
			
		if(filtro.getListaSituacao() != null){
			query.whereIn("colaboradordespesa.situacao", Colaboradordespesasituacao.listAndConcatenate(filtro.getListaSituacao()));
		}
		
	}	
	
	/**
	* Busca a lista de Colaboradordespesa para o processo de autoriza��o, estorno e cancelamento.
	*
	* @param whereIn
	* @return
	* @since Jun 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Colaboradordespesa> findForAutorizarEstornarCancelar(String whereIn){
		
		return query()
					.select("colaboradordespesa.cdcolaboradordespesa, colaboradordespesa.situacao, colaboradordespesa.valor," +
							"colaborador.cdpessoa, colaborador.nome, colaboradordespesa.situacao, colaboradordespesa.dtdeposito, colaboradordespesa.dtinsercao, " +
							"colaboradordespesamotivo.cdcolaboradordespesamotivo, colaboradordespesamotivo.descricao," +
							"item.cdcolaboradordespesaitem, item.valor, item.tipo, projeto.cdprojeto, centrocusto.cdcentrocusto, " +
							"empresa.cdpessoa, colaboradordespesa.dtholeriteinicio, colaboradordespesa.dtholeritefim, " +
							"colaboradordespesa.total, colaboradordespesa.tipovalorreceber, " +
							"evento.cdEventoPagamento, evento.nome, evento.tipoEvento, evento.provisionamento, evento.colaboradorDespesaItemTipoImportacao, " +
							"evento.colaboradorDespesaItemTipoProcessar, rateio.cdrateio")
					.join("colaboradordespesa.colaborador colaborador")
					.join("colaboradordespesa.colaboradordespesamotivo colaboradordespesamotivo")
					.leftOuterJoin("colaboradordespesa.listaColaboradordespesaitem item")
					.leftOuterJoin("colaboradordespesa.rateio rateio")
					.leftOuterJoin("item.evento evento")
					.leftOuterJoin("colaboradordespesa.empresa empresa")
					.leftOuterJoin("colaboradordespesa.projeto projeto")
					.leftOuterJoin("projeto.centrocusto centrocusto")
					.whereIn("colaboradordespesa.cdcolaboradordespesa", whereIn)
					.list();
	}
	
	/**
	* Busca a lista de Colaboradordespesa para o processo de processar
	*
	* @param whereIn
	* @return
	* @since Jul 5, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Colaboradordespesa> findForProcessar(String whereIn){
		return query()
					.select("colaboradordespesa.cdcolaboradordespesa, colaboradordespesa.situacao, colaboradordespesa.total, " +
							"colaboradordespesa.situacao, colaborador.cdpessoa, colaboradordespesa.tipovalorreceber, colaboradordespesa.dtinsercao ")
					.join("colaboradordespesa.colaborador colaborador")
					.whereIn("colaboradordespesa.cdcolaboradordespesa", whereIn)
					.list();
	}

	/**
	 * Busca as despesa do colaborador
	 * 
	 * @param cdcolaborador
	 * @param referencia
	 * @return
	 * @author Luiz Fernando F Silva
	 */
	public List<Colaboradordespesa> findForConsultarColaboradordespesa(Integer cdcolaborador, Date dtreferencia1, Date dtreferencia2) {
		return query()	
			.select("colaboradordespesa.cdcolaboradordespesa, colaboradordespesa.dtinsercao")
			.join("colaboradordespesa.colaborador colaborador")
			.where("colaborador.cdpessoa = ?", cdcolaborador)
			.where("colaboradordespesa.dtholeriteinicio <= ?", dtreferencia2)
			.where("colaboradordespesa.dtholeritefim >= ?", dtreferencia1)
			.list();
	}
	
	/**
	 * 
	 * M�todo que busca a lista de ColaboradorDespesa para a gera��o do CVS.
	 *
	 * @name findForCSV
	 * @param filtro
	 * @return
	 * @return List<Colaboradordespesa>
	 * @author Thiago Augusto
	 * @date 04/05/2012
	 *
	 */
	public List<Colaboradordespesa> findForCSV(ColaboradordespesaFiltro filtro){
		return querySined()
					.select("colaboradordespesa.cdcolaboradordespesa, colaboradordespesa.total, colaborador.nome, " +
							"colaboradordespesaitem.motivo, colaboradordespesaitem.valor, colaboradordespesa.dtinsercao, " +
							"colaboradordespesa.dtdeposito, listaColaboradorcargo.cdcolaboradorcargo, " +
							"listaColaboradorcargo.matricula, listaColaboradorcargo.dtfim ")
					.join("colaboradordespesa.colaborador colaborador")
					.leftOuterJoin("colaborador.listaColaboradorcargo listaColaboradorcargo")
					.join("colaboradordespesa.colaboradordespesamotivo colaboradordespesamotivo")
					.leftOuterJoin("colaboradordespesa.listaColaboradordespesaitem colaboradordespesaitem")
					.where("colaborador = ?", filtro.getColaborador())
					.where("colaboradordespesamotivo = ?", filtro.getMotivo())			
					.where("colaboradordespesa.dtdeposito = ?", filtro.getDtdeposito())
					.where("colaboradordespesa.total = ?", filtro.getTotal())
					.where("colaboradordespesa.dtinsercao = ?", filtro.getDtinsercao())
					.whereIn("colaboradordespesa.situacao", Colaboradordespesasituacao.listAndConcatenate(filtro.getListaSituacao()))
					.orderBy("colaboradordespesa.dtdeposito")
					.list();
	}

	/**
	 * M�todo que busca as despesas com colaboradores com situa��o Em Aberto ou Autorizada (despesas que n�o geraram contas a pagar)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Colaboradordespesa> findForReportFluxocaixa(FluxocaixaFiltroReport filtro, Boolean considerarApenasAnteriores) {
		if(!Util.booleans.isTrue(filtro.getConsDespesacolaborador())){
			return new ArrayList<Colaboradordespesa>();
		}
		List<Colaboradordespesasituacao> listaSituacao = new ArrayList<Colaboradordespesasituacao>();
		listaSituacao.add(Colaboradordespesasituacao.AUTORIZADO);
		listaSituacao.add(Colaboradordespesasituacao.EM_ABERTO);
		
		QueryBuilderSined<Colaboradordespesa> query = querySined();
				query
				.select("colaboradordespesa.cdcolaboradordespesa, colaboradordespesa.total, colaborador.nome, " +
						"colaboradordespesaitem.motivo, colaboradordespesaitem.valor, colaboradordespesa.dtinsercao, " +
						"colaboradordespesa.dtdeposito, colaboradordespesamotivo.descricao ")
				.join("colaboradordespesa.colaborador colaborador")
				.join("colaboradordespesa.colaboradordespesamotivo colaboradordespesamotivo")
				.leftOuterJoin("colaboradordespesa.listaColaboradordespesaitem colaboradordespesaitem")
				.where("colaboradordespesa.dtdeposito is not null");
		
		if(considerarApenasAnteriores != null && considerarApenasAnteriores){
			query.where("colaboradordespesa.dtdeposito < ?", filtro.getPeriodoDe());
		}else {
			query.where("colaboradordespesa.dtdeposito >= ?", filtro.getPeriodoDe())
				 .where("colaboradordespesa.dtdeposito <= ?", filtro.getPeriodoAte());
		}
				
		query.whereIn("colaboradordespesa.situacao", Colaboradordespesasituacao.listAndConcatenate(listaSituacao))
			 .orderBy("colaboradordespesa.dtdeposito");
		
		return query.list();
	}

	public List<Colaboradordespesa> findForArquivoDIRF(Empresa empresa, Date dtinicio, Date dtfim, Colaboradordespesamotivo colaboradordespesamotivo) {
		List<Colaboradordespesasituacao> listaSituacao = new ArrayList<Colaboradordespesasituacao>();
		listaSituacao.add(Colaboradordespesasituacao.AUTORIZADO);
		listaSituacao.add(Colaboradordespesasituacao.EM_ABERTO);
		
		return querySined()
				
				.select("colaboradordespesa.cdcolaboradordespesa, colaborador.nome, colaborador.cpf, " +
						"colaboradordespesaitem.motivo, colaboradordespesaitem.valor, colaboradordespesa.dtinsercao")
				.join("colaboradordespesa.colaborador colaborador")
				.leftOuterJoin("colaboradordespesa.colaboradordespesamotivo colaboradordespesamotivo")
				.leftOuterJoin("colaboradordespesa.listaColaboradordespesaitem colaboradordespesaitem")
				.where("colaboradordespesa.dtinsercao >= ?", dtinicio)
				.where("colaboradordespesa.dtinsercao <= ?", dtfim)
				.whereIn("colaboradordespesa.situacao", Colaboradordespesasituacao.listAndConcatenate(listaSituacao))
				.where("colaboradordespesamotivo = ?", colaboradordespesamotivo)
				.openParentheses()
					.openParentheses()
					.where("colaboradordespesaitem.motivo = ?", "CORRIDA")
					.where("colaboradordespesaitem.valor > 0")
					.closeParentheses().or()
					
					.openParentheses()
					.where("colaboradordespesaitem.motivo = ?", "IR")
					.where("colaboradordespesaitem.valor > 0")
					.closeParentheses().or()
					
					.openParentheses()
					.where("colaboradordespesaitem.motivo = ?", "INSS")
					.where("colaboradordespesaitem.valor > 0")
					.closeParentheses().or()
				.closeParentheses()
				.list();
	}

	/**
	 * Busca para verifica��o na listagem de holerite
	 *
	 * @param dtreferencia1
	 * @param dtreferencia2
	 * @param empresa
	 * @param projeto
	 * @param colaborador
	 * @param holeritePorProjeto 
	 * @return
	 * @author Rodrigo Freitas
	 * @since 16/12/2015
	 */
	public List<Colaboradordespesa> findForListagemHolerite(Date dtreferencia1, Date dtreferencia2, Empresa empresa, Projeto projeto, Colaborador colaborador, Boolean holeritePorProjeto) {
		QueryBuilder<Colaboradordespesa> query = query()
					.select("colaboradordespesa.cdcolaboradordespesa, colaboradordespesa.dtholeriteinicio, colaboradordespesa.dtholeritefim")
					.where("colaboradordespesa.empresa = ?", empresa)
					.where("colaboradordespesa.colaborador = ?", colaborador)
					.where("colaboradordespesa.dtholeriteinicio <= ?", dtreferencia2)
					.where("colaboradordespesa.dtholeritefim >= ?", dtreferencia1);
		
		if(projeto != null){
			query.where("colaboradordespesa.projeto = ?", projeto);
		} else {
			if(holeritePorProjeto){
				query.where("colaboradordespesa.projeto is not null");
			} else {
				query.where("colaboradordespesa.projeto is null");
			}
		}
		return query.list();
	}

	public Colaboradordespesa loadWithRateio(Colaboradordespesa bean) {
		return query()	
			.select("colaboradordespesa.cdcolaboradordespesa, colaboradordespesa.dtinsercao, colaboradordespesa.total, " +
					"rateio.cdrateio")
			.join("colaboradordespesa.rateio rateio")
			.setMaxResults(1)
			.unique();
	}
	
	public List<Colaboradordespesa> findForGerarLancamentoContabil(GerarLancamentoContabilFiltro filtro, boolean considerarEvento, String whereInEventos){
		return query()
					.select("colaboradordespesa.cdcolaboradordespesa, colaboradordespesa.total, colaboradordespesa.valor, " +
							"colaboradordespesa.situacao, colaborador.cdpessoa, colaborador.nome, colaboradordespesa.tipovalorreceber, colaboradordespesa.dtinsercao, " +
							"contaContabil.cdcontacontabil, contaContabil.nome, " +
							"colaboradordespesaitem.cdcolaboradordespesaitem, colaboradordespesaitem.valor, " +
							"evento.cdEventoPagamento, evento.nome, " +
							"contaContabilItem.cdcontacontabil, contaContabilItem.nome")
					.join("colaboradordespesa.colaborador colaborador")
					.join("colaboradordespesa.listaColaboradordespesaitem colaboradordespesaitem")
					.leftOuterJoin("colaboradordespesaitem.evento evento")
					.leftOuterJoin("colaborador.contaContabil contaContabil")
					.leftOuterJoin("evento.contaContabil contaContabilItem")
					.where("colaboradordespesa.dtinsercao>=?", filtro.getDtPeriodoInicio())
					.where("colaboradordespesa.dtinsercao<=?", filtro.getDtPeriodoFim())
					.where("colaboradordespesa.empresa=?", filtro.getEmpresa())
					.whereIn("evento.cdEventoPagamento", whereInEventos)
					.where("not exists(select mco.cdmovimentacaocontabilorigem from Movimentacaocontabilorigem mco where mco.colaboradorDespesa = colaboradordespesa)", !considerarEvento)
					.where("not exists(select mco.cdmovimentacaocontabilorigem from Movimentacaocontabilorigem mco where mco.colaboradorDespesaItem = colaboradordespesaitem and mco.eventoPagamento = evento)", considerarEvento)
					.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<ColaboradorPagamentoBean> findForReportPagamentoColaborador(ColaboradorPagamentoFiltro filtro){
		StringBuilder query = new StringBuilder();
		query.append("select sum(colaboradordespesa.total) as valor, pessoa.nome, pessoa.cdpessoa, colaborador.rg");
		query.append("  from colaboradordespesa");
		query.append("  join pessoa on pessoa.cdpessoa = colaboradordespesa.cdcolaborador");
		query.append("  join colaborador on pessoa.cdpessoa = colaborador.cdpessoa");
		query.append(" where colaboradordespesa.dtdeposito is not null ");
		if(filtro.getDtPagamentoIni() != null){ 
			query.append(" and colaboradordespesa.dtdeposito >= '").append(SinedDateUtils.toStringPostgre(filtro.getDtPagamentoIni())).append("' ");
		}
		if(filtro.getDtPagamentoFim() != null){
			query.append(" and colaboradordespesa.dtdeposito <= '").append(SinedDateUtils.toStringPostgre(filtro.getDtPagamentoFim())).append("' ");
		}
		if(filtro.getColaborador() != null && filtro.getColaborador().getCdpessoa() != null){
			query.append(" and colaborador.cdpessoa = ").append(filtro.getColaborador().getCdpessoa());
		}
		if(SinedUtil.isListNotEmpty(filtro.getListaSituacao())){
			query.append("and colaboradordespesa.situacao in ("+Colaboradordespesasituacao.listAndConcatenate(filtro.getListaSituacao())+")");
		}
		if(filtro.getTipoRemuneracao() != null){
			query.append("and colaboradordespesa.cdcolaboradordespesamotivo = "+filtro.getTipoRemuneracao().getCdcolaboradordespesamotivo());
		}
		if((filtro.getCargo() != null && filtro.getCargo().getCdcargo() != null) 
				|| (filtro.getDepartamento() != null && filtro.getDepartamento().getCddepartamento() != null) 
				|| (filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null)){
			
			if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null){
				query.append("and (colaboradordespesa.cdempresa = ").append(filtro.getEmpresa().getCdpessoa()).append(" or colaboradordespesa.cdempresa is null)");
			}
			
			query.append(" and exists(select 1 from colaboradorcargo cc");
			query.append(" 			   where cc.cdpessoa = colaborador.cdpessoa");
			query.append(" 			     and cc.dtfim is null");
			
			if(filtro.getCargo() != null && filtro.getCargo().getCdcargo() != null){
				query.append(" 			 and cc.cdcargo = ").append(filtro.getCargo().getCdcargo());
			}
			if(filtro.getDepartamento() != null && filtro.getDepartamento().getCddepartamento() != null){
				query.append(" 			 and cc.cddepartamento = ").append(filtro.getDepartamento().getCddepartamento());
			}
			if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null){
				query.append(" 			 and (cc.cdempresa = ").append(filtro.getEmpresa().getCdpessoa()).append(" or cc.cdempresa is null)");
			}
			query.append(")");
		}
		query.append(" group by pessoa.nome, pessoa.cdpessoa, colaborador.rg");
		query.append(" order by pessoa.nome");
		SinedUtil.markAsReader();
		List<ColaboradorPagamentoBean> lista = getJdbcTemplate().query(query.toString(), new RowMapper() {
			
			@Override
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				ColaboradorPagamentoBean bean = new ColaboradorPagamentoBean();
				bean.setColaborador(new Colaborador(rs.getInt("cdpessoa"), rs.getString("nome")));
				bean.getColaborador().setRg(rs.getString("rg"));
				bean.setValor(new Money(rs.getLong("valor"), true));
				
				return bean;
			}
		});
		
		return lista;
	}
	
	public Money calcularValorTotalDespesaColaborador(Empresa empresa, String whereInEventoDespesaColaborador, Date dtInicio, Date dtFim) {
		Long valorTotalCredito = newQueryBuilder(Long.class)
				.select("sum(colaboradordespesa.valor)")
				.setUseTranslator(false)
				.from(Colaboradordespesa.class)
				.leftOuterJoin("colaboradordespesa.listaColaboradordespesaitem listaColaboradordespesaitem")
				.leftOuterJoin("listaColaboradordespesaitem.evento evento")
				.where("colaboradordespesa.empresa = ?", empresa)
				.where("colaboradordespesa.dtinsercao >= ?", dtInicio)
				.where("colaboradordespesa.dtinsercao <= ?", dtFim)
				.whereIn("evento.cdEventoPagamento", whereInEventoDespesaColaborador)
				.where("evento.tipoEvento = ?", Colaboradordespesaitemtipo.CREDITO)
				.unique();
		
		Long valorTotalDebito = newQueryBuilder(Long.class)
				.select("sum(colaboradordespesa.valor)")
				.setUseTranslator(false)
				.from(Colaboradordespesa.class)
				.leftOuterJoin("colaboradordespesa.listaColaboradordespesaitem listaColaboradordespesaitem")
				.leftOuterJoin("listaColaboradordespesaitem.evento evento")
				.where("colaboradordespesa.empresa = ?", empresa)
				.where("colaboradordespesa.dtinsercao >= ?", dtInicio)
				.where("colaboradordespesa.dtinsercao <= ?", dtFim)
				.whereIn("evento.cdEventoPagamento", whereInEventoDespesaColaborador)
				.where("evento.tipoEvento = ?", Colaboradordespesaitemtipo.DEBITO)
				.unique();
		
		if(valorTotalCredito == null) valorTotalCredito = 0l;
		if(valorTotalDebito == null) valorTotalDebito = 0l;
		
		return new Money((valorTotalCredito - valorTotalDebito) / 100d);
	}

	public List<Colaboradordespesa> findByIds(String whereIn) {
		return querySined()
				
				.whereIn("colaboradordespesa.cdcolaboradordespesa", whereIn)
				.list();
	}
}
