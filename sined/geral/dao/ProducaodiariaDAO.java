package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Producaodiaria;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.ProducaodiariaFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaodiariaDAO extends GenericDAO<Producaodiaria>{

	@Override
	public void updateListagemQuery(QueryBuilder<Producaodiaria> query,FiltroListagem _filtro) {
		ProducaodiariaFiltro filtro = (ProducaodiariaFiltro)_filtro;
		
		query
			.select("producaodiaria.cdproducaodiaria,tarefa.descricao,planejamento.descricao,producaodiaria.faturar,producaodiaria.dtfatura," +
					"projeto.nome")
			.join("producaodiaria.tarefa tarefa")
			.join("tarefa.planejamento planejamento")
			.join("planejamento.projeto projeto")
			
			.where("projeto = ?",filtro.getProjeto())
			.where("planejamento = ?",filtro.getPlanejamento())
			.where("tarefa = ?",filtro.getTarefa())
			.where("producaodiaria.faturar = ?",filtro.getFaturar())
		;
		
		query.openParentheses();
			if(Util.booleans.isTrue(filtro.getFaturada())){
				query.where("producaodiaria.dtfatura is not null");
			}
			query.or();
			if(Util.booleans.isTrue(filtro.getEmaberto())){
				query.where("producaodiaria.dtfatura is null");
			}
		query.closeParentheses();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Producaodiaria> query) {
		query.select("producaodiaria.cdproducaodiaria,tarefa.cdtarefa, planejamento.cdplanejamento, projeto.cdprojeto,producaodiaria.dtproducao," +
				"producaodiaria.qtde,producaodiaria.dtfatura,producaodiaria.faturar, producaodiaria.cdusuarioaltera,producaodiaria.dtaltera," +
				"producaodiaria.observacao, unidademedida.cdunidademedida, unidademedida.nome, " +
				"nota.cdNota, notaStatus.cdNotaStatus,producaodiaria.percentual, tarefa.qtde");
		
		query.leftOuterJoin("producaodiaria.tarefa tarefa");
		query.leftOuterJoin("tarefa.planejamento planejamento");
		query.leftOuterJoin("tarefa.unidademedida unidademedida");
		query.leftOuterJoin("planejamento.projeto projeto");
		
		query.leftOuterJoin("producaodiaria.nota nota");
		query.leftOuterJoin("nota.notaStatus notaStatus");
	}
	
	/**
	 * Retorna uma lista de producaodiaria de um determinado planejamento.
	 *
	 * @param planejamento
	 * @return lista de Producaodiaria
	 * @throws SinedException - quando o par�metro planejamento for nulo.
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public List<Producaodiaria> findByPlanejamento(Planejamento planejamento) {
		if (planejamento == null || planejamento.getCdplanejamento() == null) {
			throw new SinedException("Par�metros inv�lidos. O campo planejamento n�o pode ser nulo.");
		}
		return query()
			.select("producaodiaria.cdproducaodiaria, producaodiaria.dtproducao, producaodiaria.qtde, " +
					"tarefa.cdtarefa")
			.join("producaodiaria.tarefa tarefa")
			.join("tarefa.planejamento planejamento")
			.where("planejamento = ?",planejamento)
			.orderBy("producaodiaria.dtproducao")
			.list();		
	}
	
	/**
	 * Retorna uma lista de producaodiaria
	 * 
	 * @param whereIn
	 * @return lista de producaodiaria
	 * @throws SinedException - quando o par�metro whereIn for nulo ou vazio.
	 * 
	 * @author Fl�vio Tavares
	 * 
	 */
	public List<Producaodiaria> findByWhereIn(String whereIn){
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Par�metro whereIn inv�lido.");
		}
		
		return 
			query()
			.select("producaodiaria.cdproducaodiaria, producaodiaria.faturar, producaodiaria.dtfatura, " +
					"projeto.cdprojeto, tarefa.cdtarefa, tarefa.descricao, nota.cdNota")
			.join("producaodiaria.tarefa tarefa")
			.join("tarefa.planejamento planejamento")
			.join("planejamento.projeto projeto")
			.leftOuterJoin("producaodiaria.nota nota")
			.whereIn("producaodiaria.cdproducaodiaria", whereIn)
			.list();
	}
	
	/**
	 * Associa � uma lista de produ��es di�rias uma nota.
	 * 
	 * @param nota
	 * @param listaProducaodiaria
	 * @author Fl�vio Tavares
	 */
	public void associaNotaProducoesdiarias(Nota nota, List<Producaodiaria> listaProducaodiaria){
		if(!SinedUtil.isObjectValid(nota) || SinedUtil.isListEmpty(listaProducaodiaria)){
			throw new SinedException("Nota ou lista de Produ��o di�ria inv�lidos.");
		}
		
		String whereIn = CollectionsUtil.listAndConcatenate(listaProducaodiaria, "cdproducaodiaria", ",");
		String sql = "update Producaodiaria pd " +
				"set pd.nota = ?, " +
				"pd.dtfatura = ?, " +
				"pd.cdusuarioaltera = ?, " +
				"pd.dtaltera = ? " +
				"where pd.id in (" + whereIn + ") " +
						"and pd.faturar = true " + //Somente as medi��es que podem ser faturadas
						"and pd.dtfatura is null"; //Somente as medi��es que n�o foram faturadas
		
		Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
		Date current_date = SinedDateUtils.currentDate();
		
		Object[] fields = new Object[]{nota, current_date, cdusuarioaltera, current_date};
		
		getHibernateTemplate().bulkUpdate(sql, fields);
	}
	
	/**
	 * Estorna uma produ��o di�ria.
	 * Atualiza as propriedades <code>dtfatura</code> de cada uma pra <code>null</code>.
	 *  
	 * @param listaProducaodiaria
	 * @author Fl�vio Tavares
	 */
	public void estornarProducaoDiaria(Producaodiaria producaodiaria){
		if(!SinedUtil.isObjectValid(producaodiaria)){
			throw new SinedException("Medi��o inv�lida.");
		}
				
		String sql = "update Producaodiaria pd " +
				"set pd.dtfatura = null, " +
				"pd.cdusuarioaltera = ?, " +
				"pd.dtaltera = ? " +
				"where pd.id = ? "+
						"and pd.dtfatura is not null"; //Somente as medi��es que n�o foram faturadas
		
		Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
		Date current_date = SinedDateUtils.currentDate();
		
		Object[] fields = new Object[]{cdusuarioaltera, current_date, producaodiaria.getCdproducaodiaria()};
		
		getHibernateTemplate().bulkUpdate(sql, fields);
		
	}

	/**
	 * Carrega uma string com todos os ids dos projetos das produ��es
	 * di�rias passadas por par�metro.
	 *
	 * @param itensSelecionados
	 * @return
	 * @author Rodrigo Freitas
	 */
	public String loadWithProjeto(String itensSelecionados) {
		if(itensSelecionados == null || itensSelecionados.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		List<Producaodiaria> list = query()
					.select("producaodiaria.cdproducaodiaria, projeto.cdprojeto")
					.join("producaodiaria.tarefa tarefa")
					.join("tarefa.planejamento planejamento")
					.join("planejamento.projeto projeto")
					.whereIn("producaodiaria.cdproducaodiaria", itensSelecionados)
					.list();
		return SinedUtil.listAndConcatenate(list, "tarefa.planejamento.projeto.cdprojeto", ",");
	}

	/**
	 * M�todo que retorna a menor data da produ��o di�ria de um projeto e planejamento
	 * 
	 * @param projeto
	 * @param planejamento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Date getMenorDataProducaoDiariaProjetoPlanejamento(Projeto projeto,	Planejamento planejamento) {
		if(projeto == null || projeto.getCdprojeto() == null || planejamento == null || planejamento.getCdplanejamento() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return newQueryBuilderSined(Date.class)
			.select("min(producaodiaria.dtproducao)")
			.from(Producaodiaria.class)
			.join("producaodiaria.tarefa tarefa")
			.join("tarefa.planejamento planejamento")
			.join("planejamento.projeto projeto")
			.where("projeto = ?", projeto)
			.where("planejamento = ?", planejamento)
			.where("tarefa.tarefapai is not null")
			.setUseTranslator(false)
			.unique();
	}

	/**
	 * M�todo que retorna a maior data da produ��o di�ria de um projeto e planejamento
	 * 
	 * @param projeto
	 * @param planejamento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Date getMaiorDataProducaoDiariaProjetoPlanejamento(Projeto projeto, Planejamento planejamento) {
		if(projeto == null || projeto.getCdprojeto() == null || planejamento == null || planejamento.getCdplanejamento() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return newQueryBuilderSined(Date.class)
			.select("max(producaodiaria.dtproducao)")
			.from(Producaodiaria.class)
			.join("producaodiaria.tarefa tarefa")
			.join("tarefa.planejamento planejamento")
			.join("planejamento.projeto projeto")
			.where("projeto = ?", projeto)
			.where("planejamento = ?", planejamento)
			.where("tarefa.tarefapai is not null")
			.setUseTranslator(false)
			.unique();
	}

	/**
	 * M�todo que retorna a m�dia do percentual da produ��o di�ria de um dado projeto
	 * 
	 * @param projeto
	 * @return
	 * @author Giovane Freitas
	 */
	public double getMediaPercentualProducaoDiaria(Projeto projeto) {
		if(projeto == null || projeto.getCdprojeto() == null )
			throw new SinedException("Par�metros inv�lidos.");
		
		Double media = newQueryBuilderSined(Double.class)
			.select("avg(producaodiaria.percentual)")
			.from(Producaodiaria.class)
			.join("producaodiaria.tarefa tarefa")
			.join("tarefa.planejamento planejamento")
			.join("planejamento.projeto projeto")
			.where("projeto = ?", projeto)
			.where("tarefa.tarefapai is not null")
			.setUseTranslator(false)
			.unique();
		
		if (media != null)
			return media.doubleValue();
		else
			return 0;
	}

	/**
	 * Busca a soma das quantidades j� realizadas.
	 *
	 * @param tarefa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/11/2012
	 */
	public Double getQtdeRealizadaTarefa(Tarefa tarefa) {
		Double soma = newQueryBuilderSined(Double.class)
					.select("sum(producaodiaria.qtde)")
					.from(Producaodiaria.class)
					.join("producaodiaria.tarefa tarefa")
					.where("tarefa = ?", tarefa)
					.setUseTranslator(false)
					.unique();
		
		if (soma != null)
			return soma;
		else
			return 0d;
	}
	
}
