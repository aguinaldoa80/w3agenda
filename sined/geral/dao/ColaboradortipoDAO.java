package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Colaboradortipo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("colaboradortipo.nome")
public class ColaboradortipoDAO extends GenericDAO<Colaboradortipo> {

}
