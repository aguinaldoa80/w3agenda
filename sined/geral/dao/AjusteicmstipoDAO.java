package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Ajusteicmstipo;
import br.com.linkcom.sined.geral.bean.enumeration.TipoAjuste;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.AjusteicmstipoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("ajusteicmstipo.descricao")
public class AjusteicmstipoDAO extends GenericDAO<Ajusteicmstipo>{

	@Override
	public void updateListagemQuery(QueryBuilder<Ajusteicmstipo> query, FiltroListagem _filtro) {
		AjusteicmstipoFiltro filtro = (AjusteicmstipoFiltro) _filtro;
		
		query
			.select("ajusteicmstipo.cdajusteicmstipo, ajusteicmstipo.descricao, ajusteicmstipo.apuracao, ajusteicmstipo.tipoajuste, " +
					"ajusteicmstipo.codigo, uf.sigla")
			.leftOuterJoin("ajusteicmstipo.uf uf")
			.where("uf = ?", filtro.getUf())
			.whereLikeIgnoreAll("ajusteicmstipo.descricao", filtro.getDescricao())
			.where("ajusteicmstipo.apuracao = ?", filtro.getApuracao())
			.where("ajusteicmstipo.tipoajuste = ?", filtro.getTipoajuste())
			.where("ajusteicmstipo.codigo = ?", filtro.getCodigo());
	}

	/**
	 * Busca os tipo de ajuste de ICMS a partir do tipo ajuste
	 *
	 * @param tipoajuste
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/02/2014
	 */
	public List<Ajusteicmstipo> findByTipoajuste(TipoAjuste tipoajuste) {
		return query()
					.select("ajusteicmstipo.cdajusteicmstipo, ajusteicmstipo.descricao")
					.where("ajusteicmstipo.tipoajuste = ?", tipoajuste)
					.orderBy("ajusteicmstipo.descricao")
					.list();
	}
	
	@Override
	public ListagemResult<Ajusteicmstipo> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Ajusteicmstipo> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("ajusteicmstipo.cdajusteicmstipo");
		
		return new ListagemResult<Ajusteicmstipo>(query, filtro);
	}
}
