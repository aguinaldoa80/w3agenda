package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialformulavendamaximo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("materialformulavendamaximo.ordem")
public class MaterialformulavendamaximoDAO extends GenericDAO<Materialformulavendamaximo>{

	/**
	* M�todo que busca a lista de f�rmula para c�lculo do vendamaximo
	*
	* @param material
	* @return
	* @since 18/05/2015
	* @author Luiz Fernando
	*/
	public List<Materialformulavendamaximo> findByMaterial(Material material) {
		return query()
					.select("materialformulavendamaximo.cdmaterialformulavendamaximo, materialformulavendamaximo.ordem, " +
							"materialformulavendamaximo.identificador, materialformulavendamaximo.formula")
					.where("materialformulavendamaximo.material = ?", material)
					.orderBy("materialformulavendamaximo.ordem, materialformulavendamaximo.identificador")
					.list();
	}

	public void deleteFormulavendamaximoByMaterial(Material material) {
		if(material != null && material.getCdmaterial() != null){
			getJdbcTemplate().execute("delete from materialformulavendamaximo where cdmaterial = " + material.getCdmaterial());
		}
	}

}
