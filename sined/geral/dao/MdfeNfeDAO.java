package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.MdfeNfe;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MdfeNfeDAO extends GenericDAO<MdfeNfe>{

	public List<MdfeNfe> findByMdfe(Mdfe mdfe){
		if(mdfe==null){
			return new ArrayList<MdfeNfe>();
		}
		return query()
			.select("mdfeNfe.cdMdfeNfe, mdfeNfe.idNfe, mdfeNfe.chaveAcesso, mdfeNfe.indicadorReentrega, "+
					"municipio.cdmunicipio, municipio.nome, municipio.cdibge, uf.cduf, uf.sigla, uf.nome")
			.leftOuterJoin("mdfeNfe.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.where("mdfeNfe.mdfe = ?", mdfe)
			.list();
	}
}
