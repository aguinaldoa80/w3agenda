package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentoclasseDAO extends GenericDAO<Documentoclasse> {

	/* singleton */
	private static DocumentoclasseDAO instance;
	public static DocumentoclasseDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(DocumentoclasseDAO.class);
		}
		return instance;
	}
}