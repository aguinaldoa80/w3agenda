package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.enumeration.DocumentocomissaoOrigem;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaovendedorFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentocomissaovendedorDAO extends GenericDAO<Documentocomissao>{

	@Override
	public void updateListagemQuery(QueryBuilder<Documentocomissao> query, FiltroListagem _filtro) {
		DocumentocomissaovendedorFiltro filtro = (DocumentocomissaovendedorFiltro) _filtro;
		
		query
		.select("documentocomissao.cddocumentocomissao, documentocomissao.valorcomissao, documentocomissao.percentualcomissao, " +
				"pessoa.cdpessoa, pessoa.nome, venda.cdvenda, venda.identificador, clienteVenda.cdpessoa, clienteVenda.nome, "+
				"pedidovenda.cdpedidovenda, pedidovenda.identificador, clientePedidoVenda.cdpessoa, clientePedidoVenda.nome, "+
				"contrato.cdcontrato, contrato.identificador, clienteContrato.cdpessoa, clienteContrato.nome ")
		.leftOuterJoin("documentocomissao.documento documento")
		.leftOuterJoin("documentocomissao.pessoa pessoa")
		.leftOuterJoin("documentocomissao.venda venda")
		.leftOuterJoin("venda.cliente clienteVenda")
		.leftOuterJoin("documentocomissao.pedidovenda pedidovenda")
		.leftOuterJoin("pedidovenda.cliente clientePedidoVenda")
		.leftOuterJoin("documentocomissao.contrato contrato")
		.leftOuterJoin("contrato.cliente clienteContrato");
		
		if (filtro.getColaborador() != null)
			query.where("pessoa = ?", filtro.getColaborador());
		else if (filtro.getFornecedor() != null)
			query.where("pessoa = ?", filtro.getFornecedor());
		query
		.where("documento.dtemissao >= ? ", filtro.getDataDe())
		.where("documento.dtemissao <= ? ", filtro.getDataAte());
		
		if(filtro.getDocumentocomissaoOrigem() != null && filtro.getDocumentocomissaoOrigem().equals(DocumentocomissaoOrigem.VENDA)){
			query.where("venda.cdvenda is not null");
			query.where("pedidovenda.cdpedidovenda is null");
		}else if(filtro.getDocumentocomissaoOrigem() != null && filtro.getDocumentocomissaoOrigem().equals(DocumentocomissaoOrigem.CONTRATO)){
			query.where("contrato.cdcontrato is not null");
		}else if(filtro.getDocumentocomissaoOrigem() != null && filtro.getDocumentocomissaoOrigem().equals(DocumentocomissaoOrigem.PEDIDOVENDA)){
			query.where("pedidovenda.cdpedidovenda is not null");
		}
		
		query
		.where("venda.cdvenda = ?", filtro.getCdvenda())
		.where("contrato.cdcontrato = ?", filtro.getCdcontrato())
		.where("pedidovenda.cdpedidovenda = ?", filtro.getCdpedidovenda())
		
		.orderBy("pessoa.nome, coalesce(clienteVenda.nome, clientePedidoVenda.nome, clienteContrato.nome)");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Documentocomissao> query) {
		query
		.select("documentocomissao.cddocumentocomissao, documentocomissao.valorcomissao, documentocomissao.percentualcomissao, " +
				"pessoa.cdpessoa, pessoa.nome, documentocomissao.vendedortipo, "+
				"venda.cdvenda, venda.identificador, clienteVenda.cdpessoa, clienteVenda.nome, "+
				"pedidovenda.cdpedidovenda, pedidovenda.identificador, clientePedidoVenda.cdpessoa, clientePedidoVenda.nome, "+
				"contrato.cdcontrato, contrato.identificador, clienteContrato.cdpessoa, clienteContrato.nome, "+
				"documento.cddocumento, documento.descricao, documento.numero, colaboradorcomissao.cdcolaboradorcomissao")
		.leftOuterJoin("documentocomissao.documento documento")
		.leftOuterJoin("documentocomissao.pessoa pessoa")
		.leftOuterJoin("documentocomissao.venda venda")
		.leftOuterJoin("venda.cliente clienteVenda")
		.leftOuterJoin("documentocomissao.pedidovenda pedidovenda")
		.leftOuterJoin("pedidovenda.cliente clientePedidoVenda")
		.leftOuterJoin("documentocomissao.contrato contrato")
		.leftOuterJoin("contrato.cliente clienteContrato")
		.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao");		
	}
	
	public void alterarDocumentoComissaoVendedor (Documentocomissao documentocomissao){
		getHibernateTemplate().bulkUpdate("update Documentocomissao set cdpessoa = ?, valorcomissao = ?, percentualcomissao = ?, "+
				"cdvenda = "+(documentocomissao.getVenda()!=null ? documentocomissao.getVenda().getCdvenda(): "null")+
				", cdcontrato = "+(documentocomissao.getContrato()!=null ? documentocomissao.getContrato().getCdcontrato(): "null")+
				", cdpedidovenda = "+(documentocomissao.getPedidovenda()!=null ? documentocomissao.getPedidovenda().getCdpedidovenda(): "null")+
				", cddocumento =  "+ (documentocomissao.getDocumento()!=null ? documentocomissao.getDocumento().getCddocumento(): "null")+
				" where cddocumentocomissao = ?", new Object[]{
					documentocomissao.getPessoa().getCdpessoa(), 
					documentocomissao.getValorcomissao(), 
					documentocomissao.getPercentualcomissao(),
					documentocomissao.getCddocumentocomissao()
				});
	}

	/**
	* M�todo que carrega o documentocomissao para atualizar o valor ou percentual
	*
	* @param whereIn
	* @return
	* @since 01/09/2014
	* @author Luiz Fernando
	*/
	public List<Documentocomissao> findForAtualizarValorPercentual(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("documentocomissao.cddocumentocomissao, venda.cdvenda, pedidovenda.cdpedidovenda, " +
					"contrato.cdcontrato, documento.cddocumento, pessoa.cdpessoa ")
			.join("documentocomissao.pessoa pessoa")
			.leftOuterJoin("documentocomissao.documento documento")
			.leftOuterJoin("documentocomissao.venda venda")
			.leftOuterJoin("documentocomissao.pedidovenda pedidovenda")
			.leftOuterJoin("documentocomissao.contrato contrato")
			.whereIn("documentocomissao.cddocumentocomissao", whereIn)
			.list();
	}
	
}
