package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Orcamentosituacao;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projetodespesa;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.OrcamentoFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.AcompanhamentoFinanceiroBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("orcamento.nome")
public class OrcamentoDAO extends GenericDAO<Orcamento>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Orcamento> query, FiltroListagem _filtro) {
		OrcamentoFiltro filtro = (OrcamentoFiltro) _filtro;
		
		query
			.select("orcamento.cdorcamento, orcamento.nome, cliente.nome, orcamento.prazosemana, orcamento.calcularmaterialseguranca, " +
					"orcamentosituacao.nome, bdicalculotipo.cdbdicalculotipo ")
			.leftOuterJoin("orcamento.cliente cliente")
			.leftOuterJoin("orcamento.orcamentosituacao orcamentosituacao")
			.leftOuterJoin("orcamento.bdicalculotipo bdicalculotipo")
			.whereLikeIgnoreAll("orcamento.nome", filtro.getNome())
			.where("cliente = ?", filtro.getCliente())
			.orderBy("orcamento.nome");			
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Orcamento> query) {
		query
			.select("orcamento.cdorcamento, orcamento.nome, orcamento.data, orcamento.calcularhistograma, orcamento.endereco, " +
					"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla, orcamento.prazosemana, orcamento.calcularmaterialseguranca, " +
					"cliente.cdpessoa, cliente.nome, orcamento.observacao, orcamento.cdusuarioaltera, " +
					"orcamento.dtaltera, contagerencialmod.cdcontagerencial, contagerencialmod.nome, " +
					"contagerencialmoi.cdcontagerencial, contagerencialmoi.nome, oportunidade.cdoportunidade, " +
					"orcamentosituacao.cdorcamentosituacao, orcamentosituacao.nome, bdicalculotipo.cdbdicalculotipo," +
					"projetotipo.cdprojetotipo, projetotipo.nome ")
			.leftOuterJoin("orcamento.contagerencialmod contagerencialmod")
			.leftOuterJoin("orcamento.contagerencialmoi contagerencialmoi")
			.leftOuterJoin("orcamento.cliente cliente")
			.leftOuterJoin("orcamento.bdicalculotipo bdicalculotipo")
			.leftOuterJoin("orcamento.municipio municipio")
			.leftOuterJoin("orcamento.oportunidade oportunidade")
			.leftOuterJoin("orcamento.orcamentosituacao orcamentosituacao")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("orcamento.projetotipo projetotipo");
	}
	
	
	/**
	 * Retorna uma lista de or�amentos de um determinado cliente
	 *
	 * @param cliente
	 * @return lista de Orcamento
	 * @throws SinedException - caso o par�metro seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Orcamento> findByClienteForFlex(Cliente cliente) {
		if (cliente == null || cliente.getCdpessoa() == null) {
			throw new SinedException("O par�metro cliente n�o pode ser nulo.");
		}
		return 
			query()
				.select("orcamento.cdorcamento, orcamento.nome")
				.join("orcamento.cliente cliente")
				.where("cliente = ?", cliente)
				.list();
	}

	/**
	 * Busca os or�amentos para a listagem do crud de or�amento em flex.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @param filtro 
	 */
	public List<Orcamento> findForListagemFlex(OrcamentoFiltro filtro) {
		
		QueryBuilder<Orcamento> query = query()
					.select("orcamento.cdorcamento, orcamento.nome, cliente.cdpessoa, cliente.nome, orcamento.prazosemana, orcamento.calcularmaterialseguranca, orcamento.endereco, " +
							"orcamento.calcularhistograma, orcamento.data, orcamento.observacao, municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla, oportunidade.cdoportunidade, " +
							"orcamentosituacao.cdorcamentosituacao, orcamentosituacao.nome, bdicalculotipo.cdbdicalculotipo," +
							"projetotipo.cdprojetotipo, projetotipo.nome ")
					.leftOuterJoin("orcamento.cliente cliente")
					.leftOuterJoin("orcamento.oportunidade oportunidade")
					.leftOuterJoin("orcamento.municipio municipio")
					.leftOuterJoin("orcamento.bdicalculotipo bdicalculotipo")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("orcamento.orcamentosituacao orcamentosituacao")
					.leftOuterJoin("orcamento.projetotipo projetotipo")
					.whereLikeIgnoreAll("orcamento.nome", filtro.getNome())
					.where("cliente = ?", filtro.getCliente())
					.orderBy("orcamento.nome");
		
		if(filtro.getListaSituacao() != null && filtro.getListaSituacao().size() > 0){
			query.whereIn("orcamentosituacao.cdorcamentosituacao", SinedUtil.listAndConcatenateIDs(filtro.getListaSituacao()));
		}
		
		return query.list();
	}

	/**
	 * Busca a listagem do relat�rio de acompanhamento financeiro.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<AcompanhamentoFinanceiroBean> findForAcompanhamentoFinanceiro(Planejamento planejamento) {	
		String sql = 	"SELECT RI.CDCONTAGERENCIAL, CG.NOME, 0 AS ORCADO, SUM(RI.VALOR/100) AS REALIZADO " +
						"FROM RATEIOITEM RI " +
						"JOIN MOVIMENTACAO M ON M.CDRATEIO = RI.CDRATEIO " +
						"JOIN PLANEJAMENTO PL ON PL.CDPROJETO = RI.CDPROJETO " +
						"JOIN CONTAGERENCIAL CG ON CG.CDCONTAGERENCIAL = RI.CDCONTAGERENCIAL " +
						"WHERE M.CDMOVIMENTACAOACAO = 2 " +
						"AND PL.CDPLANEJAMENTO = " + planejamento.getCdplanejamento() + " " +
						"GROUP BY RI.CDCONTAGERENCIAL, CG.NOME " +
						"ORDER BY ORCADO DESC";
		
		

		SinedUtil.markAsReader();
		List<AcompanhamentoFinanceiroBean> lista = getJdbcTemplate().query(sql, 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new AcompanhamentoFinanceiroBean(
						new Contagerencial(rs.getInt("CDCONTAGERENCIAL"), rs.getString("NOME")), 
						rs.getDouble("ORCADO"), 
						rs.getDouble("REALIZADO")
						);
			}
		});
		
		return lista;
	}

	public Orcamento loadWithContagerencial(Orcamento orcamento) {
		if(orcamento == null || orcamento.getCdorcamento() == null){
			throw new SinedException("Or�amento n�o pode ser nulo.");
		}
		return query()
					.select("orcamento.cdorcamento, contagerencialmod.cdcontagerencial, contagerencialmoi.cdcontagerencial, " +
							"vcontagerencialmod.identificador, vcontagerencialmoi.identificador")
					.leftOuterJoin("orcamento.contagerencialmod contagerencialmod")
					.leftOuterJoin("contagerencialmod.vcontagerencial vcontagerencialmod")
					.leftOuterJoin("orcamento.contagerencialmoi contagerencialmoi")
					.leftOuterJoin("contagerencialmoi.vcontagerencial vcontagerencialmoi")
					.where("orcamento = ?", orcamento)
					.unique();
	}

	/**
	 * M�todo que salva a situa��o do or�amento como conclu�do
	 *
	 * @param orcamento
	 * @author Luiz Fernando
	 */
	public void salvaSituacaoorcamento(Orcamento orcamento) {
		if(orcamento == null || orcamento.getCdorcamento() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		getHibernateTemplate().bulkUpdate("update Orcamento o set o.orcamentosituacao = ? where o.cdorcamento = ?",
				new Object[]{Orcamentosituacao.CONCLUIDO, orcamento.getCdorcamento()});
	}

	/**
	 * M�todo que verifica se no orcamento o campo calcularhistograma est� marcado 
	 *
	 * @param orcamento
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isCalcularhistogramaOrcamento(Orcamento orcamento) {		
		if(orcamento == null || orcamento.getCdorcamento() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.from(Orcamento.class)
			.where("orcamento = ?", orcamento)
			.where("orcamento.calcularhistograma = true")			
			.unique() > 0;
	}

	/**
	 * Busca as depesas do or�amento para importar para o projeto.
	 *
	 * @param orcamento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/01/2013
	 */
	@SuppressWarnings("unchecked")
	public List<Projetodespesa> buscarDespesasMateriais(Orcamento orcamento) {
		String sql = "SELECT M.CDCONTAGERENCIAL AS CDCONTAGERENCIAL, " +
						"SUM(TRG.QUANTIDADE * COALESCE(RC.CUSTOUNITARIO, M.VALORCUSTO*100, 0)) AS VALORTOTAL, " +
						"SUM(TRG.QUANTIDADE) AS QTDETOTAL " +
						
						"FROM TAREFAORCAMENTORG TRG " +
						"JOIN TAREFAORCAMENTO T ON T.CDTAREFAORCAMENTO = TRG.CDTAREFAORCAMENTO " +
						"LEFT OUTER JOIN MATERIAL M ON M.CDMATERIAL = TRG.CDMATERIAL " +
						"LEFT OUTER JOIN RECURSOCOMPOSICAO RC ON (RC.CDMATERIAL = M.CDMATERIAL OR RC.NOME = TRG.OUTRO) " +
						
						"WHERE T.CDORCAMENTO = " + orcamento.getCdorcamento() + " " +
						"AND RC.CDORCAMENTO = " + orcamento.getCdorcamento() + " " +
						
						"GROUP BY M.CDCONTAGERENCIAL"; 

		SinedUtil.markAsReader();
		return getJdbcTemplate().query(sql, 
				new RowMapper() {
					public Projetodespesa mapRow(ResultSet rs, int rowNum) throws SQLException {
						Money valortotal = new Money(rs.getLong("VALORTOTAL"), true);
						Double qtdetotal = rs.getDouble("QTDETOTAL");
						
						Money valorhora = qtdetotal > 0 ? valortotal.divide(new Money(qtdetotal)) : new Money();
						
						if(rs.getInt("CDCONTAGERENCIAL") == 0) 
							return new Projetodespesa();
						
						return new Projetodespesa(new Contagerencial(rs.getInt("CDCONTAGERENCIAL")), valortotal, valorhora);
					}
				}
			);
	}
	
	/**
	* M�todo que carrega o Or�amento com o cliente
	*
	* @param orcamento
	* @return
	* @since 05/08/2014
	* @author Luiz Fernando
	*/
	public Orcamento loadWithCliente(Orcamento orcamento){
		if(orcamento == null || orcamento.getCdorcamento() == null)
			throw new SinedException("Or�amento n�o pode ser nulo.");
		
		return query()
					.select("orcamento.cdorcamento, orcamento.nome, cliente.cdpessoa, cliente.nome")
					.leftOuterJoin("orcamento.cliente cliente")
					.where("orcamento = ?", orcamento)
					.unique();
	}

	/**
	* M�todo que verifica se existe item de custo e/ou compisi��o de m�o de obra
	*
	* @param orcamento
	* @return
	* @since 22/06/2015
	* @author Luiz Fernando
	*/
	public boolean existeItemCustoEComposicaoMO(Orcamento orcamento) {
		if(orcamento == null || orcamento.getCdorcamento() == null)
			throw new SinedException("Orcamento n�o pode ser nulo");
		
		String sql = "select sum(query.qtde) as qtde " +
				"FROM (" + 
				
				"SELECT count(*) as qtde " +
				"FROM Composicaomaoobra cmo " +
				"WHERE cmo.cdorcamento = " + orcamento.getCdorcamento() + " " +
				
				"UNION " +
				
				"SELECT count(*) as qtde " +
				"FROM Customaoobraitem cmoi " +
				"WHERE cmoi.cdorcamento = " + orcamento.getCdorcamento() + " " + 
				
				") query ";

		SinedUtil.markAsReader();
		Long count = (Long) getJdbcTemplate().queryForObject(sql.toString(), 
				new RowMapper() {
					public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
						return rs.getLong("qtde");
					}
				});
		
		return count != null ? count > 0 : false;
	}

}
