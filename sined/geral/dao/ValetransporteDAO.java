package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Valetransporte;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("valetransporte.descricao")
public class ValetransporteDAO extends GenericDAO<Valetransporte> {
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Valetransporte> query) {
		query.select("valetransporte.cdvaletransporte, valetransporte.descricao");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Valetransporte> query,
			FiltroListagem filtro) {
		query.select("valetransporte.cdvaletransporte, valetransporte.descricao");
	}
	
}
