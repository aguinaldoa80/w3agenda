package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Chequesituacao;
import br.com.linkcom.sined.geral.bean.enumeration.ExibirChequesEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.ProtocoloRetiradaChequeFiltro;
import br.com.linkcom.sined.modulo.sistema.controller.crud.bean.ChequeBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ChequeFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ChequeDAO extends GenericDAO<Cheque>{
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Cheque> query) {
		query
		.select("cheque.cdcheque, cheque.banco, cheque.agencia, cheque.conta, cheque.numero, cheque.emitente, cheque.dtbompara, cheque.proprio, " +
				"cheque.linhanumerica, cheque.chequesituacao, cheque.valor, cheque.resgatado, cheque.cpfcnpj, " +
				"empresa.cdpessoa, empresa.nome, empresa.razaosocial, " +
				"contabancaria.cdconta, contabancaria.nome, " +
				"listaChequehistorico.cdchequehistorico, listaChequehistorico.cdusuarioaltera, listaChequehistorico.dtaltera, " +
				"listaChequehistorico.observacao, listaChequehistorico.chequesituacao ")
		.leftOuterJoin("cheque.listaChequehistorico listaChequehistorico")
		.leftOuterJoin("cheque.empresa empresa")
		.leftOuterJoin("cheque.contabancaria contabancaria");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Cheque> query, FiltroListagem _filtro) {
		
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		ChequeFiltro filtro = (ChequeFiltro) _filtro;
		
		Set<String> ignoreJoin = new HashSet<String>();
		ignoreJoin.add("chequehistorico");
		ignoreJoin.add("chequedevolucaomotivo");
		
		query
			.select("distinct cheque.cdcheque, cheque.banco, cheque.agencia, cheque.conta, " +
					"cheque.numero, cheque.emitente, cheque.dtbompara, cheque.valor, cheque.chequesituacao, cheque.protocoloretiradachequeemitida")
			.leftOuterJoin("cheque.listaChequehistorico chequehistorico")
			.where("cheque.banco = ?",filtro.getBanco())
			.where("cheque.agencia = ?",filtro.getAgencia())
			.where("cheque.conta = ?",filtro.getConta())
			.where("cheque.numero = ?",filtro.getNumero())
			.where("cheque.valor >= ?", filtro.getMinValor())
			.where("cheque.valor <= ?", filtro.getMaxValor())
			.whereLikeIgnoreAll("cheque.emitente", filtro.getEmitente()!=null ? filtro.getEmitente():"")
			.whereLikeIgnoreAll("chequehistorico.observacao", filtro.getObservacao())
			.where("cheque.dtbompara >= ?", filtro.getDtbomparade())
			.where("cheque.dtbompara <= ?", filtro.getDtbomparaate())
			.where("cheque.empresa = ?", filtro.getEmpresa())
			.where("chequehistorico.chequedevolucaomotivo = ?", filtro.getChequedevolucaomotivo())
			.where("chequehistorico.chequesituacao = ?", filtro.getChequesituacao())
			.orderBy("cheque.numero ASC, cheque.emitente")
			.ignoreJoin("listaMovimentacao", "listaMovimentacaoorigem", "documento", "pessoa", "chequehistorico", "listaDocumento", "documentoacaoDoc", "pessoaDoc");
		
		if(!SinedUtil.isUsuarioLogadoAdministrador()){
			String whereInEmpresa = new SinedUtil().getListaEmpresa();
			query
				.openParentheses()
					.whereIn("cheque.empresa.cdpessoa", whereInEmpresa)
					.or()
					.where("cheque.empresa is null")
				.closeParentheses();
		}
		
		query
			.where("(select min(ch.dtaltera) from Chequehistorico ch where ch.cheque = cheque) >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getDtentradade()))
			.where("(select min(ch.dtaltera) from Chequehistorico ch where ch.cheque = cheque) <= ?", SinedDateUtils.dateToTimestampFinalDia(filtro.getDtentradaate()));
		
		if(filtro.getChequeusados() != null && filtro.getChequeusados()){
			List<Chequesituacao> lista = new ArrayList<Chequesituacao>();
			lista.add(Chequesituacao.PREVISTO);
			lista.add(Chequesituacao.DEVOLVIDO);
			lista.add(Chequesituacao.BAIXADO);
			query.whereIn("cheque.chequesituacao", Chequesituacao.listAndConcatenate(lista));
		}
		
		List<Chequesituacao> listaChequesituacao = filtro.getListaChequesituacao();
		if (SinedUtil.isListNotEmpty(listaChequesituacao)){
			query.whereIn("cheque.chequesituacao", Chequesituacao.listAndConcatenate(listaChequesituacao));
		}
		
		if(StringUtils.isNotEmpty(filtro.getVendaNota())){
			if(StringUtils.isNotEmpty(filtro.getWhereInChequeClienteVendaNota())){
				SinedUtil.quebraWhereIn("cheque.cdcheque", filtro.getWhereInChequeClienteVendaNota(), query);
			}else {
				query.where("1=0");
			}
		}
		
		if(StringUtils.isNotBlank(filtro.getPagoa()) || StringUtils.isNotBlank(filtro.getRecebidode()) || 
				ExibirChequesEnum.EMITIDOS.equals(filtro.getExibirCheques()) || ExibirChequesEnum.RECEBIDOS.equals(filtro.getExibirCheques()) ||
				(filtro.getContatipomovimentacao() != null && filtro.getContamovimentacao() != null)){
			query.leftOuterJoin("cheque.listaMovimentacao listaMovimentacao");
			query.where("listaMovimentacao.conta = ? ", filtro.getContamovimentacao());
			query.where("listaMovimentacao.conta.contatipo = ? ", filtro.getContatipomovimentacao());
			
			if(StringUtils.isNotBlank(filtro.getPagoa()) || StringUtils.isNotBlank(filtro.getRecebidode()) 
					|| ExibirChequesEnum.EMITIDOS.equals(filtro.getExibirCheques()) || ExibirChequesEnum.RECEBIDOS.equals(filtro.getExibirCheques())){
				query.leftOuterJoin("listaMovimentacao.listaMovimentacaoorigem listaMovimentacaoorigem");
				query.leftOuterJoin("listaMovimentacaoorigem.documento documento");
				query.leftOuterJoin("documento.pessoa pessoa");
				query.leftOuterJoin("cheque.listaDocumento listaDocumento");
				query.leftOuterJoin("listaDocumento.documentoacao documentoacaoDoc");
				query.leftOuterJoin("listaDocumento.pessoa pessoaDoc");
				
				List<Documentoacao> listaDocumentoacao = new ArrayList<Documentoacao>();
				listaDocumentoacao.add(Documentoacao.PREVISTA);
				listaDocumentoacao.add(Documentoacao.DEFINITIVA);
				
				query.openParentheses();
					query.openParentheses();
						if(StringUtils.isNotBlank(filtro.getPagoa()) || ExibirChequesEnum.EMITIDOS.equals(filtro.getExibirCheques())){
							query.openParentheses();
								query.where("documento.documentoclasse = ?", Documentoclasse.OBJ_PAGAR);
								if(StringUtils.isNotBlank(filtro.getPagoa())){
									query.whereLikeIgnoreAll("coalesce(pessoa.nome, documento.outrospagamento)", filtro.getPagoa());								
								}
							query.closeParentheses();
						}
						if(StringUtils.isNotBlank(filtro.getPagoa()) && StringUtils.isNotBlank(filtro.getRecebidode())){
							query.or();
						}
						if(StringUtils.isNotBlank(filtro.getRecebidode()) || ExibirChequesEnum.RECEBIDOS.equals(filtro.getExibirCheques())){
							query.openParentheses();
								query.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER);
								if(StringUtils.isNotBlank(filtro.getRecebidode())){
									query.whereLikeIgnoreAll("coalesce(pessoa.nome, documento.outrospagamento)", filtro.getRecebidode());								
								}
							query.closeParentheses();
						}
					query.closeParentheses();
					query.or();
					query.openParentheses();
						if(StringUtils.isNotBlank(filtro.getPagoa()) || ExibirChequesEnum.EMITIDOS.equals(filtro.getExibirCheques())){
							query.openParentheses();
								query.where("listaDocumento.documentoclasse = ?", Documentoclasse.OBJ_PAGAR);
								if(StringUtils.isNotBlank(filtro.getPagoa())){
									query.whereIn("documentoacaoDoc.cddocumentoacao", CollectionsUtil.listAndConcatenate(listaDocumentoacao, "cddocumentoacao", ","));
									query.whereLikeIgnoreAll("coalesce(pessoaDoc.nome, listaDocumento.outrospagamento)", filtro.getPagoa());									
								}
							query.closeParentheses();
						}
						if(StringUtils.isNotBlank(filtro.getPagoa()) && StringUtils.isNotBlank(filtro.getRecebidode())){
							query.or();
						}
						if(StringUtils.isNotBlank(filtro.getRecebidode()) || ExibirChequesEnum.RECEBIDOS.equals(filtro.getExibirCheques())){
							query.openParentheses();
								query.where("listaDocumento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER);
								if(StringUtils.isNotBlank(filtro.getRecebidode())){
									query.whereIn("documentoacaoDoc.cddocumentoacao", CollectionsUtil.listAndConcatenate(listaDocumentoacao, "cddocumentoacao", ","));
									query.whereLikeIgnoreAll("coalesce(pessoaDoc.nome, listaDocumento.outrospagamento)", filtro.getRecebidode());									
								}
							query.closeParentheses();
						}
					query.closeParentheses();
				query.closeParentheses();
			}
			if(ExibirChequesEnum.EMITIDOS.equals(filtro.getExibirCheques())){
				query.where("coalesce(cheque.proprio, false) = ?", filtro.getChequeProprio());
			}
		}
	}
	
	/**
	 * M�todo que busca os dados do cheque e hist�rico para o relat�rio
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Cheque> findForReport(ChequeFiltro filtro) {
		QueryBuilder<Cheque> query = querySined();
		this.updateListagemQuery(query, filtro);
		
		query.setIgnoreJoinPaths(new ListSet<String>(String.class));
		query.ignoreJoin("movimentacao");
		
		return query
			.select(query.getSelect().getValue() +
					", chequehistorico.cdchequehistorico, chequehistorico.cdusuarioaltera, chequehistorico.dtaltera, chequehistorico.observacaorelatorio")
			.orderBy("cheque.numero ASC, cheque.emitente")
			.list();
	}
	
	/**
	 * M�todo que verifica duplicidade de cheque
	 *
	 * @param cheque
	 * @return
	 * @author Jo�o Vitor
	 */
	public Boolean verificarDuplicidadeCheque(Cheque cheque) {
		if (cheque == null) {
			return false;
		}
		
		try {
			String sql = "select count(*) as qtde " +
			" from cheque c " +
			" where c.banco = " + (cheque.getBanco() != null ? "'" + cheque.getBanco() + "'" : " is null") + 
			" and c.agencia = " + (cheque.getAgencia() != null ? "'" + cheque.getAgencia() + "'" : " is null") +
			" and c.conta = " + (cheque.getConta() != null ? "'" + cheque.getConta() + "'" : " is null") +
			" and cast(regexp_replace(c.numero, '[^0-9]', '', 'g') as NUMERIC) = " + (cheque.getNumero() != null ? "cast('" + cheque.getNumero().replaceAll("[^0-9\\ ]", "") + "' as NUMERIC)" : " is null") +
			" and c.cdempresa = " + (cheque.getEmpresa() != null && cheque.getEmpresa().getCdpessoa() != null ? cheque.getEmpresa().getCdpessoa().toString() : " is null") +
			(cheque.getCdcheque() != null ? " and c.cdcheque <> " + cheque.getCdcheque() : "");
			
			SinedUtil.markAsReader();
			Integer qtde = (Integer) getJdbcTemplate().queryForObject(sql.toString(), 
					new RowMapper() {
				public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Integer(rs.getInt("qtde"));
				}
			});
			
			return qtde != null && qtde > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public List<Cheque> findForAutocompleteCompleto(String q) {
		return querySined()
					.select("cheque.cdcheque, cheque.banco, cheque.agencia, cheque.conta, cheque.numero, cheque.dtbompara, cheque.emitente, cheque.valor")
					.whereLikeIgnoreAll("cheque.numero", q)
					.where("cheque.chequesituacao <> ?", Chequesituacao.CANCELADO)
					.orderBy("cheque.numero")
					.autocomplete()
					.list();
	}
	
	/**
	 * M�todo que busca os dados do cheque para gerar transfer�ncia 
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 * @param contatipo 
	 * @param conta 
	 */
	public List<Cheque> findForAutocompleteCompletoGerarMovimentacao(String q, Conta conta, Contatipo contatipo) {
		QueryBuilder<Cheque> query = querySined();
		
		query
			.select("cheque.cdcheque, cheque.banco, cheque.agencia, cheque.conta, cheque.numero, cheque.dtbompara, cheque.emitente, cheque.valor");
		
		if(conta != null){
			query.where("cheque.cdcheque not in (SELECT m.cheque.cdcheque FROM Movimentacao m where m.cheque.cdcheque is not null and m.movimentacaoacao.cdmovimentacaoacao <> " + Movimentacaoacao.NORMAL.getCdmovimentacaoacao() + " and m.conta.cdconta <> " + conta.getCdconta() + ") ");
		}else if(contatipo != null){
			query.where("cheque.cdcheque not in (SELECT m.cheque.cdcheque FROM Movimentacao m where m.cheque.cdcheque is not null and m.movimentacaoacao.cdmovimentacaoacao <> " + Movimentacaoacao.NORMAL.getCdmovimentacaoacao() + " and m.conta.contatipo.cdcontatipo <> " + contatipo.getCdcontatipo() + ") ");
		}else {
			query.where("cheque.cdcheque not in (SELECT m.cheque.cdcheque FROM Movimentacao m where m.cheque.cdcheque is not null and m.movimentacaoacao.cdmovimentacaoacao <> " + Movimentacaoacao.NORMAL.getCdmovimentacaoacao() + ") ");
		}
		
		return query
				.whereLikeIgnoreAll("cheque.numero", q)
				.where("cheque.chequesituacao <> ?", Chequesituacao.CANCELADO)
				.orderBy("cheque.numero")
				.autocomplete()
				.list();
	}

	/**
	 * M�todo que busca os cheque que n�o s�o devolvidos e que n�o est�o relacionados a movimenta��o conciliada.
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Cheque> findForAutocompleteCompletoContapagar(String q, String whereInEmpresa) {
		return querySined()
					.select("cheque.cdcheque, cheque.banco, cheque.agencia, cheque.conta, cheque.numero, cheque.dtbompara, cheque.emitente, cheque.valor, cheque.linhanumerica, cheque.chequesituacao")
//					.where("cheque.cdcheque not in (SELECT m.cheque.cdcheque FROM Movimentacao m where m.cheque.cdcheque is not null and m.movimentacaoacao.cdmovimentacaoacao = " + Movimentacaoacao.CONCILIADA.getCdmovimentacaoacao() + ") ")
					.whereLikeIgnoreAll("cheque.numero", q)
//					.where("cheque.devolvido <> true")
					.where("cheque.chequesituacao = ?", Chequesituacao.PREVISTO)
					.openParentheses()
						.whereIn("cheque.empresa.cdpessoa", StringUtils.isNotBlank(whereInEmpresa) ? whereInEmpresa : null)
						.or()
						.where("cheque.empresa.cdpessoa is null", StringUtils.isBlank(whereInEmpresa))
					.closeParentheses()
					.orderBy("cheque.numero")
					.autocomplete()
					.list();
	}
	
	public List<Cheque> findForAutocompleteCompletoContareceber(String q, String whereInEmpresa, String whereInDocumento) {
		QueryBuilder<Cheque> query = querySined()
					.select("cheque.cdcheque, cheque.banco, cheque.agencia, cheque.conta, cheque.numero, cheque.dtbompara, cheque.emitente, cheque.valor, cheque.linhanumerica, cheque.chequesituacao")
					.leftOuterJoin("cheque.listaDocumento listaDocumento")
					.whereLikeIgnoreAll("cheque.numero", q)
					.where("cheque.chequesituacao = ?", Chequesituacao.PREVISTO)
					.openParentheses()
						.where("listaDocumento is null")
						.or()
						.where("listaDocumento.documentoacao = ?", Documentoacao.CANCELADA)
						.or()
						.whereIn("listaDocumento.cddocumento", whereInDocumento)
					.closeParentheses();
		
		if(StringUtils.isNotBlank(whereInEmpresa)){
			query.openParentheses()
				.whereIn("cheque.empresa.cdpessoa", StringUtils.isNotBlank(whereInEmpresa) ? whereInEmpresa : null)
				.or()
				.where("cheque.empresa.cdpessoa is null")
			.closeParentheses();
		}
		
		return query
					.orderBy("cheque.numero")
					.autocomplete()
					.list();
	}
	/**
	 * M�todo que retorna o valor do cheque
	 *
	 * @param cheque
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getValor(Cheque cheque) {
		Money valor = null;
		if(cheque != null && cheque.getCdcheque() != null)
			valor = newQueryBuilderSined(Money.class).removeUseWhere()
			.select("cheque.valor")
				.from(Cheque.class)
				.where("cheque.cdcheque = ?", cheque.getCdcheque())
				.setUseTranslator(false)
				.unique();
		
		return valor != null ? new Money(valor) : new Money();
	}

	/**
	 * M�todo que verifica se o cheque foi resgatado
	 *
	 * @param cheque
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isResgatado(Cheque cheque) {
		if(cheque == null || cheque.getCdcheque() == null)
			throw new SinedException("Cheque n�o pode ser nulo.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Cheque.class)
			.where("cheque = ?", cheque)
			.where("cheque.resgatado = true")
			.unique() > 0;
	}

	/**
	 * M�todo que busca os cheques
	 *
	 * @param idsCheques
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Cheque> findCheques(String idsCheques) {
		if(idsCheques == null || "".equals(idsCheques))
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				.select("cheque.cdcheque, cheque.valor, cheque.dtbompara, cheque.emitente, cheque.numero, " +
						"cheque.conta, cheque.agencia, cheque.banco, cheque.linhanumerica, cheque.chequesituacao")
				.whereIn("cheque.cdcheque", idsCheques)
				.list();
	}
	
	/**
	* M�todo que carrega o cheque com a empresa
	*
	* @param whereIn
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public List<Cheque> findChequeWithEmpresa(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				.select("cheque.cdcheque, cheque.valor, cheque.dtbompara, cheque.emitente, cheque.numero, " +
						"cheque.conta, cheque.agencia, cheque.banco, cheque.linhanumerica, cheque.chequesituacao," +
						"empresa.cdpessoa")
				.leftOuterJoin("cheque.empresa empresa")
				.whereIn("cheque.cdcheque", whereIn)
				.list();
	}
	
	/**
	 * 
	 * @param q
	 * @author Thiago Clemente
	 * 
	 */
	public List<Cheque> findForAutocompleteMovimentacao(String q) {
		return querySined()
				.select("cheque.cdcheque, cheque.banco, cheque.agencia, cheque.conta, cheque.numero, cheque.dtbompara, cheque.emitente, cheque.valor")
				.whereLikeIgnoreAll("cheque.numero", q)
				.openParentheses()
					.where("cheque.chequesituacao = ?", Chequesituacao.DEVOLVIDO)
					.or()
					.where("cheque.chequesituacao = ?", Chequesituacao.PREVISTO)
				.closeParentheses()
//				.openParentheses()
//					.where("cheque.devolvido = ?", Boolean.TRUE)
//					.or()
//					.where("not exists (" +
//							"select m.cdmovimentacao " +
//							"from Movimentacao m " +
//							"join m.tipooperacao to " +
//							"join m.cheque c " +
//							"join m.movimentacaoacao macao " +
//							"where c.cdcheque = cheque.cdcheque " +
//							"and (macao.id=2 or (macao.id=1 and to.id=1)) " +
//							")")
//				.closeParentheses()
				.orderBy("cheque.numero")
				.autocomplete()
				.list();
	}

	public Cheque carregaCheque(Cheque cheque) {
		if(cheque == null || cheque.getCdcheque() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				.setUseWhereEmpresa(false)
				.select("cheque.cdcheque, cheque.valor, cheque.dtbompara, cheque.emitente, cheque.numero, " +
						"cheque.conta, cheque.agencia, cheque.banco, empresa.cdpessoa, empresa.nome, empresa.razaosocial, " +
						"cheque.linhanumerica, cheque.chequesituacao")
				.leftOuterJoin("cheque.empresa empresa")
				.where("cheque = ?", cheque)
				.unique();
	}

	/**
	* M�todo que verifica se existe cheque diferente da situa��o passada como par�metro
	*
	* @param whereIn
	* @param chequesituacao
	* @return
	* @since 22/07/2014
	* @author Luiz Fernando
	*/
	public boolean isNotSituacao(String whereIn, Chequesituacao... chequesituacao) {
		if (chequesituacao == null || chequesituacao.length == 0)
			throw new SinedException("Par�metro inv�lido.");
			
		String whereInSituacao = null;
		StringBuilder values = new StringBuilder();
		for (int i=0; i < chequesituacao.length; i++) {
			values.append(chequesituacao[i].getValue()).append(",");
		}
		whereInSituacao = values.toString().substring(0, values.toString().length()-1);
		
		Long count = newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Cheque.class)
			.whereIn("cheque.cdcheque", whereIn)
			.where("cheque.chequesituacao not in (" + whereInSituacao + ")")
			.unique();
		return count != null && count > 0;
	}

	/**
	* M�todo que verifica se o cheque est� vinculado a uma movimenta��o n�o cancelada de credito
	*
	* @param whereIn
	* @return
	* @since 23/07/2014
	* @author Luiz Fernando
	*/
	public boolean isVinculoCredito(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		Long count = newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Cheque.class)
			.whereIn("cheque.cdcheque", whereIn)
			.where("exists (" +
				"select m.cdmovimentacao " +
				"from Movimentacao m " +
				"join m.tipooperacao to " +
				"join m.cheque c " +
				"join m.movimentacaoacao macao " +
				"where c.cdcheque = cheque.cdcheque " +
				"and (macao.id <> " + Movimentacaoacao.CANCELADA.getCdmovimentacaoacao() + 
					" and to.id=" + Tipooperacao.TIPO_CREDITO.getCdtipooperacao() + ") " +
				")")				
			.unique();
		return count != null && count > 0;
	}
	
	/**
	* M�todo que verifica se o cheque est� vinculado a uma movimenta��o/conta a pagar n�o cancelada de debito
	*
	* @param whereIn
	* @return
	* @since 25/07/2014
	* @author Luiz Fernando
	*/
	public boolean isVinculoDebito(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		Long count = newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Cheque.class)
			.whereIn("cheque.cdcheque", whereIn)
			.openParentheses()
				.where("exists (" +
					"select 1 " +
					"from Movimentacao m " +
					"join m.tipooperacao to " +
					"join m.cheque c " +
					"join m.movimentacaoacao macao " +
					"where c.cdcheque = cheque.cdcheque " +
					"and macao.id <> " + Movimentacaoacao.CANCELADA.getCdmovimentacaoacao() + 
					" and to.id=" + Tipooperacao.TIPO_DEBITO.getCdtipooperacao() +
					")")
				.or()
				.where("exists (" +
					"select 1 " +
					"from Documento d " +
					"join d.documentoclasse dclasse " +
					"join d.cheque c " +
					"join d.documentoacao dacao " +
					"where c.cdcheque = cheque.cdcheque " +
					"and dacao.id <> " + Documentoacao.CANCELADA.getCddocumentoacao() + 
					" and dclasse.id=" + Documentoclasse.CD_PAGAR +
					")")
			.closeParentheses()
			.unique();
		return count != null && count > 0;
	}

	/**
	* M�todo qua atualiza a situa��o do cheque de acordo com o par�metro
	*
	* @param whereIn
	* @param chequesituacao
	* @since 23/07/2014
	* @author Luiz Fernando
	*/
	public void updateSituacao(String whereIn, Chequesituacao chequesituacao) {
		if(StringUtils.isNotEmpty(whereIn) && chequesituacao != null){
			getHibernateTemplate().bulkUpdate("update Cheque c set c.chequesituacao = ? where c.cdcheque in (" + whereIn + ")",
					new Object[]{chequesituacao});
		}
	}

	/**
	* M�todo que verifica se existe cheque de clientes diferentes 
	* 
	* @param whereIn
	* @return
	* @since 29/07/2014
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public boolean isClienteDiferente(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		StringBuilder sql = new StringBuilder();
		sql.append("select d.cdpessoa as cdpessoa "); 
		sql.append("from movimentacao m ");
		sql.append("left outer join movimentacaoorigem mo on mo.cdmovimentacao = m.cdmovimentacao ");
		sql.append("left outer join documento d on d.cddocumento = mo.cddocumento ");
		sql.append("where m.cdcheque in ("+whereIn+") ");
		sql.append("group by d.cdpessoa ");
		SinedUtil.markAsReader();
		List<Integer> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getInt("cdpessoa");
			}
		});
		
		if(SinedUtil.isListNotEmpty(lista)){
			Integer id = null;
			for(Integer cdpessoa : lista){
				if(cdpessoa != null && !cdpessoa.equals(0)){
					if(id != null){
						if(!id.equals(cdpessoa)){
							return true;
						}
					}else {
						id = cdpessoa;
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInMovimentacao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/02/2015
	 */
	public List<Cheque> findByMovimentacao(String whereInMovimentacao) {
		return querySined()
					.select("cheque.cdcheque, cheque.chequesituacao, movimentacao.cdmovimentacao")
					.join("cheque.listaMovimentacao movimentacao")
					.whereIn("movimentacao.cdmovimentacao", whereInMovimentacao)
					.list();
	}
	
	/**
	* M�todo que retorna os cheques de acordo com banco, agencia, conta e numero
	*
	* @param banco
	* @param agencia
	* @param conta
	* @param numero
	* @return
	* @since 13/03/2015
	* @author Luiz Fernando
	*/
	public List<Cheque> findChequeForVenda(Integer banco, Integer agencia, Integer conta, String numero) {
		if(numero == null || "".equals(numero))
			return new ArrayList<Cheque>();
		
		return querySined()
				.select("cheque.cdcheque, cheque.banco, cheque.agencia, cheque.conta, cheque.numero")
				.where("cheque.numero = ?", numero)
				.openParentheses()
					.where("cheque.banco = ?", banco)
					.or()
					.where("cheque.banco is null ", banco == null)
				.closeParentheses()
				.openParentheses()
					.where("cheque.agencia = ?", agencia)
					.or()
					.where("cheque.agencia is null", agencia == null)
				.closeParentheses()
				.openParentheses()
					.where("cheque.conta = ?", conta)
					.or()
					.where("cheque.conta is null", conta == null)
				.closeParentheses()
				.list();
	}

	/**
	* M�todo que retorna o whereIn de cheque de acordo com o campo (clienteVendaNota)
	*
	* @param filtro
	* @return
	* @since 01/06/2015
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public String getWhereInChequeClienteVendaNota(ChequeFiltro filtro) {
		if(filtro == null || StringUtils.isBlank(filtro.getVendaNota()))
			return null;
		
		Integer cdvendaCdnota = null;
		try {
			cdvendaCdnota = Integer.parseInt(filtro.getVendaNota());
		} catch (Exception e) { 
			return null;
		}
		
		StringBuilder sql = new StringBuilder(); 
		sql.append(" select m.cdcheque ");
		sql.append(" from movimentacao m ");
		sql.append(" join movimentacaoorigem mo on mo.cdmovimentacao = m.cdmovimentacao ");
		sql.append(" join documentoorigem doc on doc.cddocumento = mo.cddocumento ");
		sql.append(" left outer join venda v on v.cdvenda = doc.cdvenda " + (cdvendaCdnota != null ? " and v.cdvenda = " + cdvendaCdnota + " ": " "));
		sql.append(" left outer join notavenda nv on nv.cdvenda = v.cdvenda ");
		sql.append(" left outer join nota n on n.cdnota = nv.cdnota " + (cdvendaCdnota != null ? " and n.numero = '" + cdvendaCdnota + "' " : " "));
		sql.append("  ");
		sql.append(" where m.cdcheque is not null ");
		sql.append(" and (n.numero = '").append(cdvendaCdnota).append("' or v.cdvenda = ").append(cdvendaCdnota).append(")");
		SinedUtil.markAsReader();
		List<Integer> list = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getInt("cdcheque");
			}
		});
		
		String whereInCheque = null;
		if(SinedUtil.isListNotEmpty(list)){
			StringBuilder whereIn = new StringBuilder();
			for(Integer cdcheque : list){
				whereIn.append(cdcheque).append(",");
			}
			whereInCheque = whereIn.substring(0, whereIn.length()-1);
		}
		return whereInCheque;
	}

	/**
	* M�todo que retorna o valor total de cheques de acordo com o cliente e situa��o
	*
	* @param cliente
	* @param situacao
	* @return
	* @since 22/08/2016
	* @author Luiz Fernando
	*/
	public Money getValorTotalCheques(Cliente cliente, Chequesituacao[] situacao) {
		if(situacao == null || situacao.length == 0 || cliente == null || cliente.getCdpessoa() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		Long total = newQueryBuilderSined(Long.class)
			.select("SUM(cheque.valor)")
			.setUseTranslator(false)
			.from(Cheque.class)
			.whereIn("cheque.chequesituacao", SinedUtil.listAndConcatenate(Arrays.asList(situacao), "value", ","))
			.openParentheses()
				.where("exists (" +
						" select m.cdmovimentacao " +
						" from Movimentacao m " +
						" join m.cheque c " +
						" join m.listaMovimentacaoorigem mo " +
						" join mo.documento d " +
						" where d.pessoa.cdpessoa = " + cliente.getCdpessoa() +
						" and c.cdcheque = cheque.cdcheque " + 
						" and d.documentoclasse.cddocumentoclasse = " + Documentoclasse.CD_RECEBER +
						" and d.tipopagamento = " + Tipopagamento.CLIENTE.ordinal() +
						" )")
			.closeParentheses()
			.unique();
		
		return total != null ? new Money(total, true) : new Money();
	}

	/**
	* M�todo que retorna os cheques com as informa��es de 'recebido de' e 'pago a'
	*
	* @param whereInCheque
	* @return
	* @since 12/09/2016
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public List<ChequeBean> findWithInformacoesRecebidoPago(String whereInCheque) {
		String sql = 
			
		" select query.cdcheque, array_agg(coalesce(query.pagoa, '')) as pagoa, array_agg(coalesce(query.recebidode, '')) as recebidode " + 
		" from (" +
		
		" (select mo.cdcheque, coalesce(p.nome, d.outrospagamento) as pagoa, NULL as recebidode " +
		" from movimentacao mo " +
		" join movimentacaoorigem moorigem on moorigem.cdmovimentacao = mo.cdmovimentacao " +
		" join documento d on d.cddocumento = moorigem.cddocumento and d.cddocumentoclasse = 1 and d.cddocumentoacao <> 0 " +
		" left outer join pessoa p on p.cdpessoa = d.cdpessoa " +
		" where mo.cdcheque in (" + whereInCheque + ") " +
		" group by mo.cdcheque, p.nome, d.outrospagamento)" +
		
		" UNION ALL " +

		" (select mo.cdcheque, NULL as pagoa, coalesce(p.nome, d.outrospagamento) as recebidode " +
		" from movimentacao mo " +
		" join movimentacaoorigem moorigem on moorigem.cdmovimentacao = mo.cdmovimentacao " +
		" join documento d on d.cddocumento = moorigem.cddocumento and d.cddocumentoclasse = 2 and d.cddocumentoacao <> 0 " +
		" left outer join pessoa p on p.cdpessoa = d.cdpessoa " +
		" where mo.cdcheque in (" + whereInCheque + ") " +
		" group by mo.cdcheque, p.nome, d.outrospagamento)" +
		
		") query " +
		" group by query.cdcheque ";
		SinedUtil.markAsReader();
		List<ChequeBean> lista = getJdbcTemplate().query(sql, new RowMapper() {
			public ChequeBean mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new ChequeBean(rs.getInt("cdcheque"), rs.getArray("pagoa"), rs.getArray("recebidode"));
			}
		});
		
		return lista;
	}
	
	/**
	 * @param whereInCheques
	 * @param whereInMovimentacoes
	 * @return
	 * @author Mairon Cezar
	 * @since 19/06/2017
	 */
	public List<Cheque> findForCopiaChequeByMovimentacoes(String whereInCheques, String whereInMovimentacoes) {
		return querySined()
				.select("banco.nome, cheque.numero, cheque.valor, cheque.dtbompara, cheque.observacao, cheque.emitente, cheque.banco, cheque.agencia, cheque.conta, "+
						"cheque.linhanumerica, movimentacao.dtmovimentacao, movimentacao.dtbanco, pessoa.nome, documentoclasse.cddocumentoclasse, documentoclasse.nome, " +
						"documento.numero, documento.cddocumento, documento.dtemissao, documento.dtvencimento, documento.valor, " +
						"documento.descricao, documento.tipopagamento, documento.outrospagamento, documento.documentoclasse, contaDoc.cdconta, " +
						"conta.agencia, conta.numero, conta.dvnumero, conta.dvagencia, " +
						"movimentacao.cdmovimentacao, movimentacao.valor, movimentacao.observacaocopiacheque, documento.outrospagamento, documento.tipopagamento, " +
						"empresa.nome, empresa.razaosocial, empresa.nomefantasia, "+
						"empresaCheque.cdpessoa, empresaCheque.nome, empresaCheque.razaosocial, "+
						"empresaMov.nome, empresaMov.razaosocial, empresaMov.nomefantasia, "+
						"fornecedorDoc.nome, fornecedorDoc.identificador, clienteDoc.nome, clienteDoc.identificador, "+
						"movimentacao.observacaocopiacheque, movimentacao.valor, movimentacao.historico, movimentacao.tipooperacao ")

				.join("cheque.empresa empresaCheque")
				.join("cheque.listaMovimentacao movimentacao")
				.leftOuterJoin("movimentacao.conta conta")
				.leftOuterJoin("conta.banco banco")				
				.leftOuterJoin("conta.listaContaempresa listaContaempresa")
				.leftOuterJoin("listaContaempresa.empresa empresa")
				.leftOuterJoin("movimentacao.listaMovimentacaoorigem origem")
				.leftOuterJoin("movimentacao.movimentacaoacao movimentacaoacao")
				.leftOuterJoin("movimentacao.empresa empresaMov")
				.leftOuterJoin("origem.documento documento")
				.leftOuterJoin("documento.pessoa pessoa")
				.leftOuterJoin("documento.conta contaDoc")
				.leftOuterJoin("pessoa.cliente clienteDoc")
				.leftOuterJoin("pessoa.fornecedorBean fornecedorDoc")
				.leftOuterJoin("documento.documentoclasse documentoclasse")
				
				.whereIn("cheque.cdcheque", whereInCheques)
				.whereIn("movimentacao.cdmovimentacao", whereInMovimentacoes)
				.where("movimentacaoacao.cdmovimentacaoacao not in (0, 104)")
				.list();			
	}

	public Cheque findForEmitenteCheque(Integer banco, Integer agencia,	Integer conta) {
		if(banco == null || agencia == null || conta == null)
			return null;
		
		return querySined()
				.setUseWhereEmpresa(false)
				.select("cheque.cdcheque, cheque.emitente, cheque.cpfcnpj")
				.where("cheque.banco = ?", banco)
				.where("cheque.agencia = ?", agencia)
				.where("cheque.conta = ?", conta)
				.openParentheses()
					.where("cheque.emitente is not null")
					.or()
					.where("cheque.cpfcnpj is not null")
				.closeParentheses()
				.setMaxResults(1)
				.orderBy("cheque.cdcheque desc, cheque.emitente desc, cheque.cpfcnpj desc")
				.unique();
	}
	
	public List<Cheque> findForProtocoloRetiradaCheque(ProtocoloRetiradaChequeFiltro filtro ) {
		if ((filtro.getWhereInCdcheque()==null || filtro.getWhereInCdcheque().equals("")) 
				&& (filtro.getWhereInCdmovimentacao()==null || filtro.getWhereInCdmovimentacao().equals(""))){
			return new ArrayList<Cheque>();
		}
		
		return querySined()
				.select("cheque.cdcheque, cheque.banco, cheque.observacao, cheque.numero, cheque.valor," +
						"empresa.cdpessoa, empresa.razaosocial, logomarca.cdarquivo, municipio.nome," +
						"chequedevolucaomotivo.descricao, listaChequehistorico.dtaltera," +
						"listaMovimentacao.cdmovimentacao, cliente.nome, cliente.identificador, municipioCliente.nome")
				.leftOuterJoin("cheque.empresa empresa")
				.leftOuterJoin("empresa.logomarca logomarca")
				.leftOuterJoin("empresa.listaEndereco listaEndereco")
				.leftOuterJoin("listaEndereco.municipio municipio")
				.leftOuterJoin("cheque.listaChequehistorico listaChequehistorico")
				.leftOuterJoin("listaChequehistorico.chequedevolucaomotivo chequedevolucaomotivo")
				.join("cheque.listaMovimentacao listaMovimentacao")
				.join("listaMovimentacao.listaMovimentacaoorigem listaMovimentacaoorigem")
				.join("listaMovimentacaoorigem.documento documento")
				.join("documento.pessoa pessoa")
				.join("pessoa.cliente cliente")
				.leftOuterJoin("cliente.listaEndereco listaEnderecoCliente")
				.leftOuterJoin("listaEnderecoCliente.municipio municipioCliente")
				.whereIn("cheque.cdcheque", filtro.getWhereInCdcheque())
				.whereIn("listaMovimentacao.cdmovimentacao", filtro.getWhereInCdmovimentacao())
				.where("listaMovimentacao.movimentacaoacao in (?, ?)", new Movimentacaoacao[]{Movimentacaoacao.NORMAL, Movimentacaoacao.CONCILIADA})
				.orderBy("cheque.numero")
				.list();
	}
	
	public void updateProtocoloretiradachequeemitida(String whereIn, Boolean protocoloretiradachequeemitida) {
		if (StringUtils.isNotEmpty(whereIn) && protocoloretiradachequeemitida!=null){
			getHibernateTemplate().bulkUpdate("update Cheque c set c.protocoloretiradachequeemitida = ? where c.cdcheque in (" + whereIn + ")", new Object[]{protocoloretiradachequeemitida});
		}
	}
	
	
	public void updateEmpresaCheque(Movimentacao moDestino) {
		if(moDestino == null || moDestino.getCheque() == null || moDestino.getEmpresa() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		getHibernateTemplate().bulkUpdate("update Cheque c set c.empresa = ? where c.cdcheque =?", new Object[]{moDestino.getEmpresa() ,moDestino.getCheque().getCdcheque()});
	}
	
}