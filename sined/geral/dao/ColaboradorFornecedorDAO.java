package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.ColaboradorFornecedor;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColaboradorFornecedorDAO extends GenericDAO<ColaboradorFornecedor> {

	/**
	* M�todo que carrega os colaboradores do fornecedor 
	*
	* @param fornecedor
	* @return
	* @since 07/03/2016
	* @author Luiz Fernando
	*/
	public List<ColaboradorFornecedor> findByFornecedor(Fornecedor fornecedor){
		if(fornecedor==null || fornecedor.getCdpessoa() == null){
			throw new SinedException("O par�metro fornecedor n�o pode ser null.");
		}
		
		return query()
			.select("colaboradorFornecedor.cdcolaboradorfornecedor, fornecedor.cdpessoa, fornecedor.nome, colaborador.cdpessoa, colaborador.nome")
			.join("colaboradorFornecedor.colaborador colaborador")
			.join("colaboradorFornecedor.fornecedor fornecedor")
			.where("fornecedor = ? ", fornecedor)
			.list();
	}
	
	/**
	 * M�todo que valida se o usuario logado est� vinculado ao fornecedor
	 * @param fornecedor, colaborador
	 * @return
	 * @author Lucas Costa
	 */
	public Boolean isFornecedorVinculadoColaborador (Fornecedor fornecedor, Colaborador colaborador){
		if(fornecedor==null || fornecedor.getCdpessoa() == null){
			throw new SinedException("O par�metro fornecedor n�o pode ser null.");
		}
		
		if(colaborador==null || colaborador.getCdpessoa() == null){
			throw new SinedException("O par�metro colaborador n�o pode ser null.");
		}
		
		Boolean vinculo = newQueryBuilder(Long.class)
						.select("count(*)")
						.from(ColaboradorFornecedor.class)
						.where("colaboradorFornecedor.colaborador = ?", colaborador)
						.where("colaboradorFornecedor.fornecedor = ?", fornecedor)
						.unique() > 0;
						
		Boolean semVinculo = newQueryBuilder(Long.class)
							.select("count(*)")
							.from(ColaboradorFornecedor.class)
							.where("colaboradorFornecedor.fornecedor = ?", fornecedor)
							.unique() == 0;
						
		return vinculo || semVinculo;
	}
	
	/**
	 * M�todo que retorna os fornecedores vinculados ao usuario logado
	 * @param colaborador
	 * @return
	 * @author Lucas Costa
	 */
	public List<ColaboradorFornecedor> findByUsuarioLogado(Colaborador colaborador){
		if(colaborador==null || colaborador.getCdpessoa() == null){
			throw new SinedException("O par�metro colaborador n�o pode ser null.");
		}
		
		return query()
			.select("colaboradorFornecedor.cdcolaboradorfornecedor, fornecedor.cdpessoa, fornecedor.nome")
			.join("colaboradorFornecedor.fornecedor fornecedor")
			.where("colaboradorFornecedor.colaborador = ? ", colaborador)
			.list();
	}

	/**
	 * M�todo que retorna os fornecedores vinculados ao usuario logado
	 * @param colaborador
	 * @return
	 * @author Lucas Costa
	 */
	public List<ColaboradorFornecedor> findNaoVinculadosByUsuarioLogado(Colaborador colaborador){
		if(colaborador==null || colaborador.getCdpessoa() == null){
			throw new SinedException("O par�metro colaborador n�o pode ser null.");
		}
		
		return query()
			.select("distinct fornecedor.nome")
			.join("colaboradorFornecedor.fornecedor fornecedor")
			.where("colaboradorFornecedor.colaborador <> ? ", colaborador)
			.where("fornecedor not in (select cf.fornecedor from colaboradorFornecedor cf where cf.colaborador = ?)", colaborador)
			.list();
	}
	
	/**
	 * M�todo que retorna os fornecedores vinculados ao usuario logado
	 * @param colaborador
	 * @return
	 * @author Lucas Costa
	 */
	@SuppressWarnings("unchecked")
	public List<String> findNomeFornecedorNaoVinculados(Colaborador colaborador){
		if(colaborador==null || colaborador.getCdpessoa() == null){
			throw new SinedException("O par�metro colaborador n�o pode ser null.");
		}
		
		String query =  "select distinct f.nome "+
						"from colaboradorFornecedor cf, pessoa f "+
						"where "+
						"cf.cdfornecedor = f.cdpessoa and "+
						"cf.cdcolaborador <> "+colaborador.getCdpessoa()+" and "+
						"f.cdpessoa not in (select cf.cdfornecedor from colaboradorFornecedor cf where cf.cdcolaborador = "+colaborador.getCdpessoa()+")";

		SinedUtil.markAsReader();
		return getJdbcTemplate().queryForList(query, String.class);
	}
}
