package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Arquivonf;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Arquivonfnotaerro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ArquivonfnotaerroDAO extends GenericDAO<Arquivonfnotaerro> {

	/**
	 * Busca a listagem de Arquivonfnotaerro a partir de um Arquivonf.
	 *
	 * @param arquivonf
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonfnotaerro> findByArquivonf(Arquivonf arquivonf) {
		if(arquivonf == null || arquivonf.getCdarquivonf() == null){
			throw new SinedException("Id de arquivonf n�o pode ser nulo.");
		}
		return query()
					.select("arquivonfnotaerro.cdarquivonfnotaerro, arquivonfnotaerro.mensagem, arquivonfnotaerro.correcao, " +
							"nota.cdNota, nota.numero, arquivonfnotaerro.data, arquivonfnotaerro.codigo ")
					.leftOuterJoin("arquivonfnotaerro.arquivonf nf")
					.leftOuterJoin("arquivonfnotaerro.arquivonfnota arquivonfnota")
					.leftOuterJoin("arquivonfnota.arquivonf arquivonf")
					.leftOuterJoin("arquivonfnota.nota nota")
					.openParentheses()
					.where("arquivonf = ?", arquivonf)
					.or()
					.where("nf = ?", arquivonf)
					.closeParentheses()
					.orderBy("arquivonfnotaerro.data desc, nota.numero, arquivonfnotaerro.mensagem")
					.list();
	}

	public List<Arquivonfnotaerro> findByArquivonfnota(Arquivonfnota arquivonfnotaFound) {
		return query()
				.select("arquivonfnotaerro.codigo, arquivonf.arquivonfsituacao ")
				.leftOuterJoin("arquivonfnotaerro.arquivonfnota arquivonfnota")
				.leftOuterJoin("arquivonfnota.arquivonf arquivonf")
				.where("arquivonfnota.cdarquivonfnota = ?", arquivonfnotaFound.getCdarquivonfnota())
				.list();
	}

	public void deleteByArquivonf(Arquivonf arquivonf) {
		getJdbcTemplate().execute("delete from Arquivonfnotaerro where cdarquivonf = " + arquivonf.getCdarquivonf());
	}
}
