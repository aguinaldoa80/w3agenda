package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Faturamentocontrato;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.FaturamentocontratoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FaturamentocontratoDAO extends GenericDAO<Faturamentocontrato> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Faturamentocontrato> query, FiltroListagem _filtro) {
		FaturamentocontratoFiltro filtro = (FaturamentocontratoFiltro)_filtro;
		
		query
			.select("distinct new Faturamentocontrato(faturamentocontrato.cdfaturamentocontrato, cliente.nome)")
			.setUseTranslator(false)
			.join("faturamentocontrato.listaNotascontrato listaNotascontrato")
			.join("listaNotascontrato.nota nota")
			.join("nota.notaStatus notaStatus")
			.join("nota.empresa empresa")
			.join("nota.cliente cliente")
			.where("empresa = ?", filtro.getEmpresa())
			.where("nota.dtEmissao >= ?", filtro.getDtinicio())
			.where("nota.dtEmissao <= ?", filtro.getDtfim())
			.whereIn("notaStatus.id", NotaStatus.EMITIDA.getCdNotaStatus()+","+NotaStatus.NFSE_EMITIDA.getCdNotaStatus()+","+NotaStatus.NFE_EMITIDA.getCdNotaStatus());
	}

	/**
	 * M�todo que retorna os faturamentos para a listagem
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Faturamentocontrato> findFaturamentosForListagem(String whereIn) {
		if(whereIn == null || whereIn.equals(""))
			return new ArrayList<Faturamentocontrato>();
		
		QueryBuilder<Faturamentocontrato> query = query()
			.select("faturamentocontrato.cdfaturamentocontrato, listaNotascontrato.cdNotaContrato, contrato.cdcontrato, contrato.descricao, contrato.dataparceladiautil, contrato.dataparcelaultimodiautilmes, frequencia.cdfrequencia, " +
					"cliente.cdpessoa, cliente.nome, nota.cdNota")
			.join("faturamentocontrato.listaNotascontrato listaNotascontrato")
			.join("listaNotascontrato.contrato contrato")
			.join("contrato.cliente cliente")
			.join("listaNotascontrato.nota nota")
			.join("nota.notaStatus notaStatus")
			.leftOuterJoin("contrato.frequencia frequencia")
			.whereIn("faturamentocontrato.cdfaturamentocontrato", whereIn)
			.whereIn("notaStatus.id", NotaStatus.EMITIDA.getCdNotaStatus()+","+NotaStatus.NFSE_EMITIDA.getCdNotaStatus()+","+NotaStatus.NFE_EMITIDA.getCdNotaStatus());
		
//		if(filtro.getOrdemalfabetica() != null && filtro.getOrdemalfabetica())
//			query.orderBy("cliente.nome asc");

		return query.list();
	}

	/**
	 * M�todo que busca os faturamentos para gerar receita em lot
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Faturamentocontrato> findFaturamentos(String whereIn) {
		if(whereIn == null || whereIn.equals(""))
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("faturamentocontrato.cdfaturamentocontrato, listaNotascontrato.cdNotaContrato, contrato.cdcontrato, contrato.descricao, contrato.dataparceladiautil, contrato.dataparcelaultimodiautilmes, frequencia.cdfrequencia, " +
					"contrato.frequencia, contrato.mes, contrato.ano, " +
					"cliente.cdpessoa, cliente.nome, nota.cdNota, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, endereco.cdendereco, rateio.cdrateio, " +
					"listaRateioitem.cdrateioitem, listaRateioitem.percentual, listaRateioitem.valor, centrocusto.cdcentrocusto, contagerencial.cdcontagerencial, " +
					"projeto.cdprojeto, taxa.cdtaxa, conta.cdconta, notaStatus.cdNotaStatus")
			.leftOuterJoin("faturamentocontrato.listaNotascontrato listaNotascontrato")
			.leftOuterJoin("listaNotascontrato.contrato contrato")
			.leftOuterJoin("contrato.cliente cliente")
			.leftOuterJoin("contrato.empresa empresa")
			.leftOuterJoin("contrato.endereco endereco")
			.leftOuterJoin("contrato.rateio rateio")
			.leftOuterJoin("contrato.conta conta")
			.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
			.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
			.leftOuterJoin("listaRateioitem.contagerencial contagerencial")
			.leftOuterJoin("listaRateioitem.projeto projeto")
			.leftOuterJoin("contrato.taxa taxa")
			.leftOuterJoin("listaNotascontrato.nota nota")
			.leftOuterJoin("nota.notaStatus notaStatus")
			.leftOuterJoin("contrato.frequencia frequencia")
			.whereIn("faturamentocontrato.id", whereIn)
			.whereIn("notaStatus.id", NotaStatus.EMITIDA.getCdNotaStatus()+","+NotaStatus.NFSE_EMITIDA.getCdNotaStatus()+","+NotaStatus.NFE_EMITIDA.getCdNotaStatus())
			.list();
	}

	/**
	 * M�todo que retorna o faturamento do contrato
	 * 
	 * @param contrato
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Faturamentocontrato findByContrato(Contrato contrato) {
		if(contrato == null || contrato.getCdcontrato() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("faturamentocontrato.cdfaturamentocontrato")
			.join("faturamentocontrato.listaNotascontrato listaNotascontrato")
			.join("listaNotascontrato.contrato contrato")
			.where("contrato = ?", contrato)
			.unique();
	}

}
