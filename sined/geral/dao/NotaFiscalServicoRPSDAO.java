package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoLoteRPS;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoRPS;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiscalServicoRPSFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotaFiscalServicoRPSDAO extends GenericDAO<NotaFiscalServicoRPS> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<NotaFiscalServicoRPS> query, FiltroListagem _filtro) {
		NotaFiscalServicoRPSFiltro filtro = (NotaFiscalServicoRPSFiltro) _filtro;
		
		query
			.select("notaFiscalServicoRPS.cdNotaFiscalServicoRPS, notaFiscalServicoRPS.numero, notaFiscalServicoRPS.dataemissao, " +
					"cliente.nome, empresa.nomefantasia")
			.join("notaFiscalServicoRPS.notaFiscalServico notaFiscalServico")
			.join("notaFiscalServico.empresa empresa")
			.join("notaFiscalServico.cliente cliente")
			.where("notaFiscalServico.empresa = ?", filtro.getEmpresa())
			.where("notaFiscalServicoRPS.notaFiscalServicoLoteRPS is null")
			.orderBy("notaFiscalServicoRPS.numero desc");
		
		if(filtro.getEmpresa() == null){
			query.where("1 = 0");
		}
	}

	/**
	 * Verifica se existe o RPS para a nota passada por par�metro.
	 * 
	 * @param notaFiscalServico
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/04/2016
	 */
	public boolean haveRPS(NotaFiscalServico notaFiscalServico) {
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.from(NotaFiscalServicoRPS.class)
				.where("notaFiscalServicoRPS.notaFiscalServico = ?", notaFiscalServico)
				.unique() > 0;
	}

	/**
	 * Busca o RPS pela nota passada por par�metro.
	 * 
	 * @param notaFiscalServico
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/04/2016
	 */
	public NotaFiscalServicoRPS findByNota(NotaFiscalServico notaFiscalServico) {
		return query()
				.where("notaFiscalServicoRPS.notaFiscalServico = ?", notaFiscalServico)
				.unique();
	}
	
	/**
	 * Busca so RPS pelas notas passadas por par�metro.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/05/2016
	 */
	public List<NotaFiscalServicoRPS> findByNota(String whereIn) {
		return query()
				.select("notaFiscalServicoRPS.cdNotaFiscalServicoRPS, notaFiscalServicoLoteRPS.cdNotaFiscalServicoLoteRPS, notaFiscalServico.cdNota")
				.join("notaFiscalServicoRPS.notaFiscalServico notaFiscalServico")
				.join("notaFiscalServicoRPS.notaFiscalServicoLoteRPS notaFiscalServicoLoteRPS")
				.whereIn("notaFiscalServico.cdNota", whereIn)
				.list();
	}

	/**
	 * Busca as notas pendentes para emiss�o de NFS-e
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/04/2016
	 */
	public List<NotaFiscalServicoRPS> findForNfe(NotaFiscalServicoRPSFiltro filtro) {
		return query()
				.select("notaFiscalServicoRPS.cdNotaFiscalServicoRPS, notaFiscalServicoRPS.numero, notaFiscalServicoRPS.dataemissao, " +
						"empresa.cnpj, codigotributacao.codigo, naturezaoperacao.codigonfse, " +
						"notaFiscalServico.dtEmissao, notaFiscalServico.basecalculo, notaFiscalServico.cdNota, " +
						"notaFiscalServico.incidepis, notaFiscalServico.pis, notaFiscalServico.basecalculopis, " +
						"notaFiscalServico.incidecofins, notaFiscalServico.cofins, notaFiscalServico.basecalculocofins, " +
						"notaFiscalServico.incidecsll, notaFiscalServico.csll, notaFiscalServico.basecalculocsll, " +
						"notaFiscalServico.incideir, notaFiscalServico.ir, notaFiscalServico.basecalculoir, " +
						"notaFiscalServico.incideinss, notaFiscalServico.inss, notaFiscalServico.basecalculoinss, " +
						"listaItens.qtde, listaItens.descricao, listaItens.precoUnitario, listaItens.desconto, " +
						"municipioissqn.nome, notaStatus.cdNotaStatus, " +
						"cliente.cdpessoa, cliente.cnpj, cliente.cpf, cliente.nome, cliente.razaosocial, cliente.email, cliente.tipopessoa, " +
						"endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.bairro, endereco.cep, endereco.complemento, " +
						"enderecotipo.cdenderecotipo, " +
						"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla, " +
						"telefone.cdtelefone, telefone.telefone, telefonetipo.cdtelefonetipo, " +
						"material.cdmaterial, material.nome ")
				.join("notaFiscalServicoRPS.notaFiscalServico notaFiscalServico")
				.join("notaFiscalServico.empresa empresa")
				.join("notaFiscalServico.cliente cliente")
				.join("notaFiscalServico.notaStatus notaStatus")
				.leftOuterJoin("notaFiscalServico.municipioissqn municipioissqn")
				.leftOuterJoin("notaFiscalServico.codigotributacao codigotributacao")
				.leftOuterJoin("notaFiscalServico.naturezaoperacao naturezaoperacao")
				.leftOuterJoin("notaFiscalServico.listaItens listaItens")
				.leftOuterJoin("listaItens.material material")
				.leftOuterJoin("cliente.listaEndereco endereco")
				.leftOuterJoin("endereco.enderecotipo enderecotipo")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("cliente.listaTelefone telefone")
				.leftOuterJoin("telefone.telefonetipo telefonetipo")
				.where("notaFiscalServico.empresa = ?", filtro.getEmpresa())
				.where("notaFiscalServicoRPS.notaFiscalServicoLoteRPS is null")
				.orderBy("notaFiscalServicoRPS.numero")
				.list();
	}

	/**
	 * Atualiza o lote do RPS passado por par�metro.
	 *
	 * @param rps
	 * @param loteRPS
	 * @author Rodrigo Freitas
	 * @since 05/04/2016
	 */
	public void updateLoteRPS(NotaFiscalServicoRPS rps, NotaFiscalServicoLoteRPS loteRPS) {
		getHibernateTemplate().bulkUpdate("update NotaFiscalServicoRPS n set n.notaFiscalServicoLoteRPS = ? where n = ?", new Object[]{
			loteRPS, rps
		});
	}

	/**
	 * Busca os RPS's para confer�ncia e processamento de NFS-e do GeisWeb
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/11/2016
	 */
	public List<NotaFiscalServicoRPS> findForConferenciaNfseGeisweb(Empresa empresa) {
		return query()
				.select("notaFiscalServicoRPS.cdNotaFiscalServicoRPS, notaFiscalServicoRPS.numero, notaFiscalServico.cdNota, cliente.nome, cliente.cnpj, cliente.cpf, " +
						"item.precoUnitario, item.qtde, item.desconto, item.descricao")
				.join("notaFiscalServicoRPS.notaFiscalServico notaFiscalServico")
				.join("notaFiscalServico.notaStatus notaStatus")
				.leftOuterJoin("notaFiscalServico.cliente cliente")
				.join("notaFiscalServico.listaItens item")
				.where("notaFiscalServico.empresa = ?", empresa)
				.openParentheses()
					.where("notaFiscalServico.notaStatus = ?", NotaStatus.EMITIDA)
					.or()
					.where("notaFiscalServico.notaStatus = ?", NotaStatus.LIQUIDADA)
				.closeParentheses()
				.orderBy("notaFiscalServicoRPS.numero")
				.list();
	}

	/**
	 * Busca o RPS por n�mero e empresa.
	 * 	 
	 * @param empresa
	 * @param numero
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/11/2017
	 */
	public NotaFiscalServicoRPS findByNumeroEmpresa(Empresa empresa, Integer numero) {
		return query()
				.select("notaFiscalServicoRPS.cdNotaFiscalServicoRPS, notaFiscalServicoRPS.numero, notaFiscalServico.cdNota")
				.join("notaFiscalServicoRPS.notaFiscalServico notaFiscalServico")
				.where("notaFiscalServico.empresa = ?", empresa)
				.where("notaFiscalServicoRPS.numero = ?", numero)
				.orderBy("notaFiscalServicoRPS.numero")
				.unique();
	}


}
