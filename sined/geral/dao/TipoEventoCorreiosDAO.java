package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.TipoEventoCorreios;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TipoEventoCorreiosDAO extends GenericDAO<TipoEventoCorreios>{

	public TipoEventoCorreios findByNome(String nome){
		return querySined()
				.select("tipoEventoCorreios.cdtipoEventoCorreios, tipoEventoCorreios.descricao")
				.whereEqualIgnoreAll("tipoEventoCorreios.descricao = ?", nome)
				.setMaxResults(1)
				.unique();
	}
}
