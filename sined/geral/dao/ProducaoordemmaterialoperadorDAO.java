package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialoperador;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoordemmaterialoperadorDAO extends GenericDAO<Producaoordemmaterialoperador> {

	public List<Producaoordemmaterialoperador> findByProducaoordemmaterial(Producaoordemmaterial producaoordemmaterial) {
		return query()
				.select("producaoordemmaterialoperador.cdproducaoordemmaterialoperador, producaoordemmaterial.cdproducaoordemmaterial," +
						"operador.cdpessoa, operador.nome, producaoordemmaterialoperador.qtdeproduzido, producaoordemmaterialoperador.qtdeperdadescarte, " +
						"producaoordemmaterialperdadescarte.cdproducaoordemmaterialperdadescarte, producaoordemmaterialperdadescarte.qtdeperdadescarte, " +
						"motivodevolucaoperda.cdmotivodevolucao, motivodevolucaoperda.descricao")
				.join("producaoordemmaterialoperador.producaoordemmaterial producaoordemmaterial")
				.join("producaoordemmaterialoperador.operador operador")
				.leftOuterJoin("producaoordemmaterialoperador.listaProducaoordemmaterialperdadescarte producaoordemmaterialperdadescarte")
				.leftOuterJoin("producaoordemmaterialperdadescarte.motivodevolucao motivodevolucaoperda")
				.where("producaoordemmaterial=?", producaoordemmaterial)
				.orderBy("operador.nome")
				.list();
	}
	
	public void delete(Producaoordemmaterial producaoordemmaterial) {
		getHibernateTemplate().bulkUpdate("delete from Producaoordemmaterialoperador p where p.producaoordemmaterial=?", producaoordemmaterial);
	}
	
	public void delete(Producaoordem producaoordem) {
		if(producaoordem != null && producaoordem.getCdproducaoordem() != null){
			getHibernateTemplate().bulkUpdate("delete from Producaoordemmaterialoperador p where p.producaoordemmaterial.cdproducaoordemmaterial in (" +
				"select pom.cdproducaoordemmaterial from Producaoordemmaterial pom join pom.producaoordem po where po.cdproducaoordem = ?) ", producaoordem.getCdproducaoordem());
		}
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Producaoordemmaterialoperador producaoordemmaterialoperador = (Producaoordemmaterialoperador) save.getEntity();
		if(Hibernate.isInitialized(producaoordemmaterialoperador.getListaProducaoordemmaterialperdadescarte()))
			save.saveOrUpdateManaged("listaProducaoordemmaterialperdadescarte");
	}
}
