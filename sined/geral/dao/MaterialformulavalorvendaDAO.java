package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialformulavalorvenda;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialformulavalorvendaDAO extends GenericDAO<Materialformulavalorvenda>{

	/**
	 * M�todo que busca as formulas do valor de venda de acordo com o material
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 20/05/2014
	 */
	public List<Materialformulavalorvenda> findByMaterial(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Materia n�o pode ser nulo.");
			
		return query()
					.select("materialformulavalorvenda.cdmaterialformulavalorvenda, materialformulavalorvenda.ordem, " +
							"materialformulavalorvenda.identificador, materialformulavalorvenda.formula")
					.where("materialformulavalorvenda.material = ?", material)
					.orderBy("materialformulavalorvenda.ordem, materialformulavalorvenda.identificador")
					.list();
	}

	public void deleteFormulavalorvendaByMaterial(Material material) {
		if(material != null && material.getCdmaterial() != null){
			getJdbcTemplate().execute("delete from materialformulavalorvenda where cdmaterial = " + material.getCdmaterial());
		}
	}
	
	/**
	* M�todo que retorna as formulas de venda dos materiais para sincronizar com o app
	*
	* @param whereIn
	* @return
	* @since 29/07/2016
	* @author Luiz Fernando
	*/
	public List<Materialformulavalorvenda> findForAndroid(String whereIn) {
		List<Materialformulavalorvenda> lista = query()
		.select("materialformulavalorvenda.cdmaterialformulavalorvenda, materialformulavalorvenda.ordem, " +
				"materialformulavalorvenda.identificador, materialformulavalorvenda.formula, material.cdmaterial")
		.leftOuterJoin("materialformulavalorvenda.material material")
		.whereIn("materialformulavalorvenda.cdmaterialformulavalorvenda", whereIn)
		.where("material.ativo = ?", Boolean.TRUE, StringUtils.isBlank(whereIn))
		.list();
		return lista;
	}
}
