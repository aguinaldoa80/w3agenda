package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Exameclinica;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ExameclinicaFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("exameclinica.nome")
public class ExameclinicaDAO extends GenericDAO<Exameclinica> {
		
	
	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	/* singleton */
	private static ExameclinicaDAO instance;
	public static ExameclinicaDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ExameclinicaDAO.class);
		}
		return instance;
	}
	
	/**
	 * M�todo que busca todos os registros de exameclinica
	 * Permite selecionar campos e escolher a ordem
	 * @author Orestes
	 * @param String, String
	 * @return List<Exameclinica>
	*/
	public List<Exameclinica> findAll(String campos, String orderBy) {
		QueryBuilder<Exameclinica> queryBuilder = query();
		if (campos != null && !campos.trim().equals("")) {
			queryBuilder.select(campos);
		}
		if (orderBy != null && !orderBy.trim().equals("")) {
			queryBuilder.orderBy(orderBy);
		}
		
		return queryBuilder.list();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Exameclinica> query) {
		query
			.leftOuterJoinFetch("exameclinica.logotipo logotipo")
			.leftOuterJoinFetch("exameclinica.listaExameconvenioclinica exameconvenioclinica");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Exameclinica> query,
			FiltroListagem _filtro) {
		
		ExameclinicaFiltro filtro = (ExameclinicaFiltro)_filtro;
		
		query
		.leftOuterJoinFetch("exameclinica.logotipo logotipo")
		.leftOuterJoinFetch("exameclinica.listaExameconvenioclinica exameconvenioclinica")
		.whereLikeIgnoreAll("exameclinica.nome", filtro.getNome());
		
		super.updateListagemQuery(query, _filtro);
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {		
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				Exameclinica exameclinica = (Exameclinica)save.getEntity();
				if(exameclinica.getLogotipo()!= null)
					arquivoDAO.saveFile(save.getEntity(), "logotipo");
				save.saveOrUpdateManaged("listaExameconvenioclinica");
				return null;
			}			
		});		
	}	
	
}