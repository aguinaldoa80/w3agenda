package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Materialtabelaprecoempresa;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialtabelaprecoempresaDAO extends GenericDAO<Materialtabelaprecoempresa> {

	public List<Materialtabelaprecoempresa> findForAndroid(String whereIn) {
		return query()
				.select("materialtabelaprecoempresa.cdmaterialtabelaprecoempresa, " +
						"empresa.cdpessoa, materialtabelapreco.cdmaterialtabelapreco")
				.join("materialtabelaprecoempresa.materialtabelapreco materialtabelapreco")
				.join("materialtabelaprecoempresa.empresa empresa")
				.whereIn("materialtabelaprecoempresa.cdmaterialtabelaprecoempresa", whereIn)
				.list();
	}
	
	
}
