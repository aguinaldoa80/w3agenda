package br.com.linkcom.sined.geral.dao;


import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.view.Vwrelatoriomargemprojeto;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VwrelatoriomargemprojetoDAO extends GenericDAO<Vwrelatoriomargemprojeto> {

	/**
	 * Busca os registros para o preenchimento do relatório de margem de projeto.
	 *
	 * @param projeto
	 * @param dtref1
	 * @param dtref2
	 * @param tipo
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Vwrelatoriomargemprojeto> findByProjeto(Projeto projeto, Date dtref1, Date dtref2, Tipooperacao tipo) {
		return query()
					.select("vwrelatoriomargemprojeto.id, vwrelatoriomargemprojeto.valor, vwrelatoriomargemprojeto.dtref, " +
							"projeto.cdprojeto, projeto.nome, tipooperacao.cdtipooperacao")
					.leftOuterJoin("vwrelatoriomargemprojeto.projeto projeto")
					.join("vwrelatoriomargemprojeto.tipooperacao tipooperacao")
					.where("projeto = ?", projeto)
					.where("tipooperacao = ?", tipo)
					.where("vwrelatoriomargemprojeto.dtref >= ?", dtref1)
					.where("vwrelatoriomargemprojeto.dtref <= ?", dtref2)
					.list();
	}


}





