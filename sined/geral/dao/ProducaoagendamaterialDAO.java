package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.modulo.producao.controller.process.filter.IntegracaoVipalFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoagendamaterialDAO extends GenericDAO<Producaoagendamaterial>{

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Producaoagendamaterial producaoagendamaterial = (Producaoagendamaterial) save.getEntity();
		
		if(Hibernate.isInitialized(producaoagendamaterial.getListaProducaoagendamaterialmateriaprima()))
			save.saveOrUpdateManaged("listaProducaoagendamaterialmateriaprima");
	}
	
	/**
	 * Busca os itens de agenda de produ��o de acordo com o WhereIn passado por par�metro.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Producaoagendamaterial> findByProducaoagenda(String whereInProducaoagenda, String whereInProducaoagendamaterial) {
		if(whereInProducaoagenda == null || whereInProducaoagenda.equals("")){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("producaoagendamaterial.cdproducaoagendamaterial, producaoagendamaterial.qtde, " +
							"material.cdmaterial, material.nome, material.identificacao, material.qtdereferencia, material.producao, " +
							"producaoagenda.cdproducaoagenda, producaoagendamaterial.largura, producaoagendamaterial.altura, " +
							"producaoagendamaterial.comprimento, producaoagendatipo.cdproducaoagendatipo, " +
							"producaoagendatipo.nome, producaoetapa.cdproducaoetapa, producaoetapa.corteproducao, producaoetapa.baixarmateriaprimacascata, " +
							"listaProducaoagendamaterialmateriaprima.cdproducaoagendamaterialmateriaprima, " +
							"listaProducaoagendamaterialmateriaprima.qtdeprevista, " +
							"materialMateriaprima.cdmaterial, materialMateriaprima.nome, materialMateriaprima.identificacao, " +
							"materialMateriaprima.servico, materialMateriaprima.produto, " +
							"materialMateriaprima.patrimonio, materialMateriaprima.epi," +
							"unidademedidaMateriaprima.cdunidademedida, unidademedidaMateriaprima.nome, unidademedidaMateriaprima.casasdecimaisestoque, empresa.cdpessoa," +
							"unidademedidaPAMMP.cdunidademedida, unidademedidaPAMMP.nome, unidademedidaPAMMP.casasdecimaisestoque, " +
							"listaProducaoagendamaterialmateriaprima.fracaounidademedida, listaProducaoagendamaterialmateriaprima.qtdereferenciaunidademedida, " +
							"materialproduto.altura, materialproduto.largura, materialproduto.comprimento, materialproduto.fatorconversaoproducao, " +
							"pneumaterialbanda.cdmaterial, pneumaterialbanda.nome, pneumaterialbanda.identificacao," +
							"producaoetapanomePAMMP.cdproducaoetapanome, producaoetapanomePAMMP.nome ")
					.join("producaoagendamaterial.producaoagenda producaoagenda")
					.join("producaoagendamaterial.material material")
					.leftOuterJoin("material.materialproduto materialproduto")
					.leftOuterJoin("producaoagendamaterial.pneu pneu")
					.leftOuterJoin("pneu.materialbanda pneumaterialbanda")
					.leftOuterJoin("producaoagendamaterial.producaoetapa producaoetapa")
					.leftOuterJoin("producaoagenda.empresa empresa")
					.leftOuterJoin("producaoagenda.producaoagendatipo producaoagendatipo")
					.leftOuterJoin("producaoagendamaterial.listaProducaoagendamaterialmateriaprima listaProducaoagendamaterialmateriaprima")
					.leftOuterJoin("listaProducaoagendamaterialmateriaprima.material materialMateriaprima")
					.leftOuterJoin("materialMateriaprima.unidademedida unidademedidaMateriaprima")
					.leftOuterJoin("listaProducaoagendamaterialmateriaprima.unidademedida unidademedidaPAMMP")
					.leftOuterJoin("listaProducaoagendamaterialmateriaprima.producaoetapanome producaoetapanomePAMMP")
					.whereIn("producaoagenda.cdproducaoagenda", whereInProducaoagenda)
					.whereIn("producaoagendamaterial.cdproducaoagendamaterial", whereInProducaoagendamaterial)
					.list();
	}
	
	/**
	 * M�todo que busca os materias da agenda de produ��o
	 *
	 * @param producaoagenda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Producaoagendamaterial> findByProducaoagenda(Producaoagenda producaoagenda) {
		if(producaoagenda == null || producaoagenda.getCdproducaoagenda() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("producaoagendamaterial.cdproducaoagendamaterial, producaoagendamaterial.qtde, " +
							"material.cdmaterial, material.nome, material.identificacao, material.qtdereferencia, " +
							"producaoagenda.cdproducaoagenda")
					.join("producaoagendamaterial.producaoagenda producaoagenda")
					.join("producaoagendamaterial.material material")
					.where("producaoagenda = ?", producaoagenda)
					.list();
	}
	
	/**
	* M�todo que busca os itens que ser�o exclu�dos na agenda de produ��o
	*
	* @param producaoagenda
	* @param whereIn
	* @return
	* @since 10/03/2015
	* @author Luiz Fernando
	*/
	public List<Producaoagendamaterial> findForDelete(Producaoagenda producaoagenda, String whereIn) {
		if(whereIn == null || whereIn.equals("") || producaoagenda == null || producaoagenda.getCdproducaoagenda() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("producaoagendamaterial.cdproducaoagendamaterial, producaoagenda.cdproducaoagenda")
					.join("producaoagendamaterial.producaoagenda producaoagenda")
					.where("producaoagendamaterial.cdproducaoagendamaterial not in (" +whereIn+")")
					.where("producaoagenda = ?", producaoagenda)
					.list();
	}
	
	/**
	* M�todo que carrega os itens da agenda de produ��o de acordo com os itens do pedido de venda
	*
	* @param whereInPedidovendamaterial
	* @return
	* @since 17/06/2015
	* @author Luiz Fernando
	*/
	public List<Producaoagendamaterial> findByPedidovendamaterial(String whereInPedidovendamaterial) {
		if(StringUtils.isEmpty(whereInPedidovendamaterial))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("producaoagendamaterial.cdproducaoagendamaterial, pedidovendamaterial.cdpedidovendamaterial")
			.join("producaoagendamaterial.pedidovendamaterial pedidovendamaterial")
			.whereIn("pedidovendamaterial.cdpedidovendamaterial", whereInPedidovendamaterial)
			.list();
	}

	/**
	 * Busca as agendas de produ��o para a integra��o com o RQG da Vipal
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/08/2015
	 */
	public List<Producaoagendamaterial> findForIntegracaoVipal(IntegracaoVipalFiltro filtro) {
		return query()
					.select("producaoagendamaterial.cdproducaoagendamaterial, producaoagendamaterial.observacao, " +
							"producaoagenda.cdproducaoagenda, " +
							"empresa.cdpessoa, empresa.nome, empresa.nomefantasia, empresa.cnpj, " +
							"cliente.cdpessoa, cliente.tipopessoa, cliente.cpf, cliente.cnpj, cliente.nome, " +
							"cliente.razaosocial, cliente.inscricaoestadual, cliente.rg, cliente.email, " +
							"municipio.cdmunicipio, municipio.nome, " +
							"uf.cduf, uf.sigla, " +
							"endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.bairro, endereco.complemento, endereco.cep, " +
							"contato.cdpessoa, contato.nome, contato.responsavelos, " +
							"telefone.cdtelefone, telefone.telefone, " +
							"telefonetipo.cdtelefonetipo, " +
							"pedidovendamaterial.cdpedidovendamaterial, " +
							"pedidovenda.cdpedidovenda, pedidovenda.dtpedidovenda, " +
							"pneu.cdpneu, pneu.serie, pneu.dot, pneu.numeroreforma, " +
							"pneumarca.cdpneumarca, pneumarca.nome, pneumarca.codigointegracao, " +
							"pneumodelo.cdpneumodelo, pneumodelo.nome, pneumodelo.codigointegracao, " +
							"pneumedida.cdpneumedida, pneumedida.nome, pneumedida.codigointegracao, " +
							"materialbanda.cdmaterial, materialbanda.nome, pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome," +
							"listaClienteSegmento.principal, segmento.cdsegmento")
					.join("producaoagendamaterial.producaoagenda producaoagenda")
					.join("producaoagenda.empresa empresa")
					.leftOuterJoin("producaoagendamaterial.pneu pneu")
					.leftOuterJoin("pneu.pneumarca pneumarca")
					.leftOuterJoin("pneu.pneumedida pneumedida")
					.leftOuterJoin("pneu.pneumodelo pneumodelo")
					.leftOuterJoin("pneu.materialbanda materialbanda")
					.leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
					.leftOuterJoin("producaoagendamaterial.pedidovendamaterial pedidovendamaterial")
					.leftOuterJoin("producaoagenda.pedidovenda pedidovenda")
					.leftOuterJoin("producaoagenda.cliente cliente")
					.leftOuterJoin("cliente.listaEndereco endereco")
					.leftOuterJoin("endereco.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("cliente.listaContato contato")
					.leftOuterJoin("cliente.listaTelefone telefone")
					.leftOuterJoin("telefone.telefonetipo telefonetipo")
					.leftOuterJoin("cliente.listaClienteSegmento listaClienteSegmento")
					.leftOuterJoin("listaClienteSegmento.segmento segmento")
					.where("producaoagenda.dtentrega >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getDtinicio()))
					.where("producaoagenda.dtentrega <= ?", SinedDateUtils.dateToTimestampFinalDia(filtro.getDtfim()))
					.where("producaoagenda.empresa = ?", filtro.getEmpresa())
					.list();
	}

	/**
	* M�todo que carrega o item da agenda de produ��o com as dimens�es
	*
	* @param producaoagendamaterial
	* @return
	* @since 17/08/2016
	* @author Luiz Fernando
	*/
	public Producaoagendamaterial loadForRegistroproducao(Producaoagendamaterial producaoagendamaterial) {
		if(producaoagendamaterial == null || producaoagendamaterial.getCdproducaoagendamaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("producaoagendamaterial.cdproducaoagendamaterial, producaoagendamaterial.altura, producaoagendamaterial.largura, producaoagendamaterial.comprimento, " +
						"material.cdmaterial, materialproduto.fatorconversaoproducao, materialproduto.altura, materialproduto.largura, materialproduto.comprimento ")
				.leftOuterJoin("producaoagendamaterial.material material")
				.leftOuterJoin("material.materialproduto materialproduto")
				.where("producaoagendamaterial = ?", producaoagendamaterial)
				.unique();
	}

	/**
	 * Reliza o update do item da agenda de produ��o
	 *
	 * @param producaoagendamaterial
	 * @param material
	 * @author Rodrigo Freitas
	 * @since 07/04/2017
	 */
	public void updateMaterial(Producaoagendamaterial producaoagendamaterial, Material material) {
		if(producaoagendamaterial == null || producaoagendamaterial.getCdproducaoagendamaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		getHibernateTemplate().bulkUpdate("update Producaoagendamaterial p set p.material = ? where p = ?", new Object[]{material, producaoagendamaterial});
	}
	
	public void updateBandaenviada(Producaoagendamaterial producaoagendamaterial, Boolean bandaenviada) {
		if(producaoagendamaterial == null || producaoagendamaterial.getCdproducaoagendamaterial() == null || bandaenviada == null)
			throw new SinedException("Par�metro inv�lido.");
		
			getHibernateTemplate().bulkUpdate("update Producaoagendamaterial p set p.bandaenviada = ? where p = ?", new Object[]{bandaenviada, producaoagendamaterial});
	}

	public List<Producaoagendamaterial> findByAgendaAndPneu(String whereInAgenda, String whereInPneu) {
		return query()
				.select("producaoagendamaterial.cdproducaoagendamaterial,producaoagendamaterial.qtde, producaoagenda.producaoagendasituacao, " +
						"pneu.cdpneu, pneu.serie, pneu.dot, pneu.numeroreforma, " +
						"pneumarca.cdpneumarca, pneumarca.nome, pneumarca.codigointegracao, " +
						"pneumodelo.cdpneumodelo, pneumodelo.nome, pneumodelo.codigointegracao," +
						"materialbanda.cdmaterial, materialbanda.nome," +
						"pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome," +
						"pneumedida.cdpneumedida, pneumedida.nome, pneumedida.codigointegracao, " +
						"cliente.cdpessoa,cliente.identificador,cliente.nome,cliente.razaosocial,cliente.cpf,cliente.cnpj," +
						"cliente.inscricaoestadual,cliente.email," +
						"ufinscricaoestadual.cduf,ufinscricaoestadual.nome,ufinscricaoestadual.sigla," +
						"pedidovenda.cdpedidovenda,pedidovenda.identificacaoexterna,pedidovenda.dtpedidovenda," +
						"listaPedidovendavalorcampoextra.cdpedidovendavalorcampoextra,listaPedidovendavalorcampoextra.valor," +
						"producaoetapa.cdproducaoetapa,producaoetapa.nome," +
						"material.cdmaterial,material.nome,material.identificacao," +
						"pedidovendamaterial.ordem,pedidovendamaterial.cdpedidovendamaterial,pedidovendamaterial.quantidade,pedidovendamaterial.dtprazoentrega," +
						"clienteEndereco.cdendereco,clienteEndereco.numero,clienteEndereco.complemento,clienteEndereco.enderecotipo,clienteEndereco.cep," +
						"clienteEndereco.logradouro,clienteEndereco.bairro," +
						"ufCliente.cduf,ufCliente.nome,ufCliente.sigla," +
						"municipio.cdmunicipio,municipio.nome," +
						"enderecoPedido.cdendereco,enderecoPedido.numero,enderecoPedido.complemento,enderecoPedido.enderecotipo,enderecoPedido.cep," +
						"enderecoPedido.logradouro,enderecoPedido.bairro," +
						"ufPedido.cduf,ufPedido.nome,ufPedido.sigla," +
						"municipioPedido.cdmunicipio,municipioPedido.nome," +
						"empresa.cdpessoa,empresa.razaosocial,empresa.logomarca,empresa.nomefantasia," +
						"colaborador.cdpessoa, colaborador.nome, campoextrapedidovendatipo.ordem, " +
						"pedidoVendaByItem.cdpedidovenda")
				.join("producaoagendamaterial.producaoagenda producaoagenda")
				.join("producaoagendamaterial.pneu pneu")
				.leftOuterJoin("pneu.pneumarca pneumarca")
				.leftOuterJoin("pneu.pneumedida pneumedida")
				.leftOuterJoin("pneu.pneumodelo pneumodelo")
				.leftOuterJoin("pneu.materialbanda materialbanda")
				.leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
				.leftOuterJoin("producaoagenda.venda venda")
				.leftOuterJoin("producaoagenda.pedidovenda pedidovenda")
				.leftOuterJoin("producaoagenda.empresa empresa")
				.leftOuterJoin("producaoagenda.cliente cliente")
				.leftOuterJoin("pedidovenda.listaPedidovendavalorcampoextra listaPedidovendavalorcampoextra")
				.leftOuterJoin("listaPedidovendavalorcampoextra.campoextrapedidovendatipo campoextrapedidovendatipo")
				.leftOuterJoin("pedidovenda.colaborador colaborador")
				.leftOuterJoin("pedidovenda.endereco enderecoPedido")
				.leftOuterJoin("enderecoPedido.municipio municipioPedido")
				.leftOuterJoin("municipioPedido.uf ufPedido")
				.leftOuterJoin("producaoagendamaterial.pedidovendamaterial pedidovendamaterial")
				.leftOuterJoin("pedidovendamaterial.pedidovenda pedidoVendaByItem")
				.leftOuterJoin("pedidovendamaterial.material material")
				.leftOuterJoin("producaoagendamaterial.producaoetapa producaoetapa")
				.leftOuterJoin("cliente.listaEndereco clienteEndereco")
				.leftOuterJoin("clienteEndereco.municipio municipio")
				.leftOuterJoin("municipio.uf ufCliente")
				.leftOuterJoin("cliente.ufinscricaoestadual ufinscricaoestadual")
				.whereIn("pneu.cdpneu", whereInPneu)
				.whereIn("producaoagenda.cdproducaoagenda", whereInAgenda)
				.list();
				
	}
}








