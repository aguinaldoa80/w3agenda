package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Veiculocor;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculotipomarcacorcombustivelFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("veiculocor.descricao")
public class VeiculocorDAO extends GenericDAO<Veiculocor> {

	@Override
	public void updateListagemQuery(QueryBuilder<Veiculocor> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inválidos");
		}
		VeiculotipomarcacorcombustivelFiltro filtro = (VeiculotipomarcacorcombustivelFiltro) _filtro;
		query
			.select("veiculocor.cdveiculocor, veiculocor.descricao")
			.whereLikeIgnoreAll("veiculocor.descricao", filtro.getDescricao());
	}
	
	
}
