package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.ContaCarteiraNossoNumero;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("contacarteiranossonumero.cdcontacarteiranossonumero")
public class ContaCarteiraNossoNumeroDAO extends GenericDAO<ContaCarteiraNossoNumero>{
	
	public ContaCarteiraNossoNumero findByContacarteira(Contacarteira contacarteira){
		if(contacarteira == null){
			throw new SinedException("O campo cdcontacarteira n�o pode ser nulo.");
		}
		
		return querySined()
				 
				.select("contaCarteiraNossoNumero.cdcontacarteiranossonumero, contaCarteiraNossoNumero.contacarteira, contaCarteiraNossoNumero.nossonumero")
				.where("contaCarteiraNossoNumero.contacarteira = ?", contacarteira)
				.unique();
	}

	public void updateContaCarteiraNossoNumero(ContaCarteiraNossoNumero contaCarteiraNossoNumero, Integer nossoNumeroUltimo) {
		getHibernateTemplate().bulkUpdate("update ContaCarteiraNossoNumero c set c.nossonumero = " + nossoNumeroUltimo + "where c.contacarteira = " + contaCarteiraNossoNumero.getContacarteira().getCdcontacarteira()); ;
	}
}
