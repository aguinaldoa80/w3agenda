package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.MdfeReferenciado;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MdfeReferenciadoDAO extends GenericDAO<MdfeReferenciado>{

	public List<MdfeReferenciado> findByMdfe(Mdfe mdfe){
		if(mdfe==null){
			return new ArrayList<MdfeReferenciado>();
		}
		return query()
			.select("mdfeReferenciado.cdMdfeReferenciado, mdfeReferenciado.idMdfeReferenciado, mdfeReferenciado.chaveAcesso, mdfeReferenciado.indicadorReentrega, "+
					"municipio.cdmunicipio, municipio.nome, municipio.cdibge, uf.cduf, uf.sigla, uf.nome")
			.leftOuterJoin("mdfeReferenciado.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.where("mdfeReferenciado.mdfe = ?", mdfe)
			.list();
	}
}
