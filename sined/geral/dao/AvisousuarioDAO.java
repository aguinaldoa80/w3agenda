package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Notificacao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AvisousuarioDAO extends GenericDAO<Avisousuario> {

	public List<Avisousuario> findAvisousuario(Usuario usuario, String whereInEmpresa, boolean notificado, Integer limite, Date dtInicio, Boolean notifica) {
		if(usuario == null || usuario.getCdpessoa() == null)
			return new ArrayList<Avisousuario>();
		
		QueryBuilder<Avisousuario> query = query();
		query		
				.select("avisousuario.cdavisousuario, aviso.cdaviso, aviso.dtaviso, aviso.assunto, " +
						"aviso.complemento, aviso.avisoOrigem, aviso.idOrigem, avisousuario.dtleitura ")
				.join("avisousuario.aviso aviso")
				.where("aviso.dtaviso >= ?", dtInicio)
				.where("avisousuario.dtnotificacao is null", notificado)
				.where("avisousuario.dtleitura is null")
				.openParentheses()
					.where("aviso.empresa is null")
					.or()
					.whereIn("aviso.empresa.cdpessoa", whereInEmpresa)
				.closeParentheses()
				.where("avisousuario.usuario = ?", usuario);
		
		if (notifica != null) {
			query
				.openParentheses()
				.where("aviso.notifica is null ")
				.or()
				.where("aviso.notifica <> ?", notifica)
				.closeParentheses();
		}
		
		if(limite != null){
			query.setMaxResults(limite);
		}
		
		return query
			.orderBy("aviso.dtaviso desc, aviso.cdaviso desc")
			.list();
	}

	public void updateNotificado(Avisousuario avisousuario) {
		if(avisousuario != null && avisousuario.getCdavisousuario() != null){
			getHibernateTemplate().bulkUpdate("update Avisousuario a set dtnotificacao = ? where a = ?", new Object[]{
				new Date(System.currentTimeMillis()), avisousuario
			});
		}
	}
	
	public void updateLido(Avisousuario avisousuario) {
		if(avisousuario != null && avisousuario.getCdavisousuario() != null){
			getHibernateTemplate().bulkUpdate("update Avisousuario a set dtleitura = ? where a = ?", new Object[]{
				new Date(System.currentTimeMillis()), avisousuario
			});
		}
	}
	
	public void updateLidoTodos(Usuario usuario, String whereInAviso) {
		if(usuario != null && usuario.getCdpessoa() != null){
			getHibernateTemplate().bulkUpdate("update Avisousuario a set dtleitura = ? where a.usuario.cdpessoa = ?" + 
					(StringUtils.isNotBlank(whereInAviso) ? " and a.aviso.cdaviso in (" + whereInAviso + ") " : "")
				, new Object[]{new Date(System.currentTimeMillis()), usuario.getCdpessoa()}
			);
		}
	}

	public Integer getQtdeNaoLidos(Usuario usuario, String whereInEmpresa) {
		if(usuario == null || usuario.getCdpessoa() == null)
			return 0;
		
		return newQueryBuilderSined(Long.class)
			.from(Avisousuario.class)
			.setUseTranslator(false) 
			.select("count(*)")
			.join("avisousuario.aviso aviso")
			.where("avisousuario.dtleitura is null")
			.openParentheses()
				.where("aviso.empresa is null")
				.or()
				.whereIn("aviso.empresa.cdpessoa", whereInEmpresa)
			.closeParentheses()
			.where("avisousuario.usuario = ?", usuario)
			.unique()
			.intValue();
	}
	
	public void deleteAllFromAviso(Aviso aviso){
		if(aviso != null && aviso.getCdaviso() != null){
			getJdbcTemplate().execute("DELETE FROM avisousuario WHERE cdaviso = " + aviso.getCdaviso());
		}
	}

	public boolean existeUsuarioNotificado(Aviso aviso) {
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Avisousuario.class)
			.join("avisousuario.aviso aviso")
			.where("aviso = ?", aviso)
			.where("avisousuario.dtnotificacao is not null")
			.unique() > 0;
	}
	
	public void atualizarAvisoComoLido(String selectedItem, String cdusuario) {
		this.getJdbcTemplate().update("update avisousuario set dtleitura = current_date where cdavisousuario in " +
									  "(select distinct u.cdavisousuario " +
									  "from aviso a " +
									  "left join avisousuario u on u.cdaviso = a.cdaviso " +
									  "where u.dtleitura is null and a.cdmotivoaviso = ? " + 
									  " and u.cdusuario = ?)", new Object[]{Integer.parseInt(selectedItem), Integer.parseInt(cdusuario)});
	}

	@SuppressWarnings("unchecked")
	public List<Notificacao> findAllNaoLidasNotificacao() {
		StringBuilder sql = new StringBuilder();
		
		sql.append("select a.cdmotivoaviso, m.descricao, count(u.*) as quantidade, u.cdusuario, max(a.dtaviso) as dtaviso ");
		sql.append("from avisousuario u ");
		sql.append("left join aviso a on a.cdaviso = u.cdaviso ");
		sql.append("left join papel p on p.cdpapel = a.cdpapel ");
		sql.append("left join motivoaviso m on m.cdmotivoaviso = a.cdmotivoaviso ");
		sql.append("where u.dtleitura is null and u.cdusuario = " + SinedUtil.getUsuarioLogado().getCdpessoa() + " and m.notificacao = true and a.cdmotivoaviso is not null ");
		
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		if (!administrador){
			if (whereInPapel != null && !whereInPapel.equals("")){
				sql.append(" and (((p.cdpapel in (" + whereInPapel + ")) or (p.cdpapel is null)) ");
				sql = adicionarSqlAppendPapeisComExecao(sql);
			} else {
				sql.append(" and p.cdpapel is null ");
			}
		}
		
		sql.append("group by a.cdmotivoaviso, m.descricao, u.cdusuario ");
		sql.append("order by m.descricao ");

		SinedUtil.markAsReader();
		return getJdbcTemplate().query(sql.toString(), new RowMapper() {
			@Override
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Notificacao(rs.getString("descricao"), SinedDateUtils.toString(rs.getDate("dtaviso")), rs.getInt("quantidade"), rs.getInt("cdmotivoaviso"), rs.getInt("cdusuario"));
			}
		});
	}
	
	private StringBuilder adicionarSqlAppendPapeisComExecao(StringBuilder sql) {
		sql.append("or m.motivo = "+MotivoavisoEnum.ATRASO_AGENDA_INTERACAO.getId()+" or m.motivo ="+MotivoavisoEnum.ORDEM_SERVICO_ATRASADA.getId()+" ");
		sql.append("and u.dtleitura is null and u.cdusuario = " + SinedUtil.getUsuarioLogado().getCdpessoa() +") ");
		return sql;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Notificacao> findAllNaoLidasPopup() {
		StringBuilder sql = new StringBuilder();
		
		sql.append("select a.cdmotivoaviso, m.descricao, count(u.*) as quantidade, u.cdusuario, max(a.dtaviso) as dtaviso ");
		sql.append("from avisousuario u ");
		sql.append("left join aviso a on a.cdaviso = u.cdaviso ");
		sql.append("left join papel p on p.cdpapel = a.cdpapel ");
		sql.append("left join motivoaviso m on m.cdmotivoaviso = a.cdmotivoaviso ");
		sql.append("where u.dtleitura is null and u.cdusuario = " + SinedUtil.getUsuarioLogado().getCdpessoa() + " and m.popup = true and a.cdmotivoaviso is not null ");
		
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		if (!administrador){
			if (whereInPapel != null && !whereInPapel.equals("")){
				sql.append(" and (((p.cdpapel in (" + whereInPapel + ")) or (p.cdpapel is null)) ");
				sql = adicionarSqlAppendPapeisComExecao(sql);
			} else {
				sql.append(" and p.cdpapel is null ");
			}
		}
		
		sql.append("group by a.cdmotivoaviso, m.descricao, u.cdusuario ");
		sql.append("order by m.descricao ");

		SinedUtil.markAsReader();
		return getJdbcTemplate().query(sql.toString(), new RowMapper() {
			@Override
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Notificacao(rs.getString("descricao"), SinedDateUtils.toString(rs.getDate("dtaviso")), rs.getInt("quantidade"), rs.getInt("cdmotivoaviso"), rs.getInt("cdusuario"));
			}
		});
	}
	
	@SuppressWarnings("unchecked")
	public List<Notificacao> findAllEnvioEmailDiario(Usuario u) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("select a.cdmotivoaviso, m.descricao, count(au.*) as quantidade, au.cdusuario, max(a.dtaviso) as dtaviso ");
		sql.append("from avisousuario au ");
		sql.append("left join aviso a on a.cdaviso = au.cdaviso ");
		sql.append("left join papel e on e.cdpapel = a.cdpapel ");
		sql.append("left join motivoaviso m on m.cdmotivoaviso = a.cdmotivoaviso ");
		sql.append("left join pessoa p on p.cdpessoa = au.cdusuario ");
		sql.append("where au.dtleitura is null and au.cdusuario = " + u.getCdpessoa() + " and m.emaildiario = true and a.cdmotivoaviso is not null ");
		
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		if (!administrador){
			if (whereInPapel != null && !whereInPapel.equals("")){
				sql.append(" and (((e.cdpapel in (" + whereInPapel + ")) or (e.cdpapel is null)) ");
				sql.append("or m.motivo = "+MotivoavisoEnum.ATRASO_AGENDA_INTERACAO.getId()+" or m.motivo ="+MotivoavisoEnum.ORDEM_SERVICO_ATRASADA.getId()+") ");
			} else {
				sql.append(" and e.cdpapel is null ");
			}
		}
		
		sql.append("group by a.cdmotivoaviso, m.descricao, au.cdusuario ");
		sql.append("order by m.descricao ");

		SinedUtil.markAsReader();
		return getJdbcTemplate().query(sql.toString(), new RowMapper() {
			@Override
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Notificacao(rs.getString("descricao"), SinedDateUtils.toString(rs.getDate("dtaviso")), rs.getInt("quantidade"), rs.getInt("cdmotivoaviso"), rs.getInt("cdusuario"));
			}
		});
	}
	
	public Avisousuario findAvisoNaoLidasEmail() {
		QueryBuilder<Avisousuario> query = query();
		
		query
			.select("avisousuario.cdavisousuario, avisousuario.dtnotificacao, avisousuario.dtleitura, avisousuario.dtEmail, " +
					"aviso.cdaviso, aviso.assunto, usuario.cdpessoa, avisousuario.preparadoEnvioEmail, aviso.complemento, " +
					"aviso.dtaviso, usuario.cdpessoa, usuario.email, empresa.razaosocial, aviso.motivoaviso, aviso.idOrigem," +
					"usuario.bloqueado,motivoaviso.cdmotivoaviso ")
			.leftOuterJoin("avisousuario.aviso aviso")
			.leftOuterJoin("avisousuario.usuario usuario")
			.leftOuterJoin("aviso.motivoaviso motivoaviso")
			.leftOuterJoin("aviso.empresa empresa")
			.leftOuterJoin("aviso.papel papel");
		
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		if (!administrador){
			if (whereInPapel != null && !whereInPapel.equals("")){
				query.openParentheses();
				query.whereIn("papel.cdpapel", whereInPapel)
				.or()
					.where("motivoaviso.motivo = ?",MotivoavisoEnum.ATRASO_AGENDA_INTERACAO)
				.or()
					.where("motivoaviso.motivo = ?",MotivoavisoEnum.ORDEM_SERVICO_ATRASADA)
				.or()
				.where("papel.cdpapel is null");
				query.closeParentheses();
			} else {
				query.where("papel.cdpapel is null");
			}
		}

		
		query
			.where("avisousuario.dtEmail is null")
			.where("avisousuario.dtleitura is null")
			.where("(avisousuario.preparadoEnvioEmail is null or avisousuario.preparadoEnvioEmail = false)")
			.setMaxResults(1)
			.orderBy("avisousuario.cdavisousuario");
				
		
		return query.unique();
	}

	public void updatePreparadoEnvioEmail(Avisousuario avisousuario, Boolean preparadoEnvioEmail) {
		this.getJdbcTemplate().update("update avisousuario set preparadoenvioemail = " + preparadoEnvioEmail + " where cdavisousuario = " + avisousuario.getCdavisousuario());
	}
	
	
	public void updateAvisoComoEnviadoEmail(Avisousuario avisousuario) {
		this.getJdbcTemplate().update("update avisousuario set dtEmail = current_date where cdavisousuario = ? " +
									  " and cdusuario = ?", new Object[]{avisousuario.getCdavisousuario(), avisousuario.getUsuario().getCdpessoa()});
	}

	public List<Avisousuario> findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(Motivoaviso motivoaviso, Integer cdagendamento, Date lastAvisoDate) {
		return query()
				.select("usuario.cdpessoa")
				.leftOuterJoin("avisousuario.aviso aviso")
				.leftOuterJoin("avisousuario.usuario usuario")
				.leftOuterJoin("aviso.motivoaviso motivoaviso")
				.where("aviso.motivoaviso = ?", motivoaviso)
				.where("aviso.idOrigem = ?", cdagendamento)
				.openParentheses()
				.where("aviso.papel = ?", motivoaviso != null ? motivoaviso.getPapel() : null)
				.or()
					.where("motivoaviso.movivo = ?",MotivoavisoEnum.ATRASO_AGENDA_INTERACAO)
				.or()
					.where("motivoaviso.motivo = ?",MotivoavisoEnum.ORDEM_SERVICO_ATRASADA)
				.closeParentheses()
				.where("avisousuario.dtleitura is null")
				.where("aviso.dtaviso = ?", lastAvisoDate)
				.list();
	}
}