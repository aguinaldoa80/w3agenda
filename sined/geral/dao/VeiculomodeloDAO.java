package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Veiculomodelo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocontroleveiculo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.VeiculomodeloFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("veiculomodelo.nome")
public class VeiculomodeloDAO extends GenericDAO<Veiculomodelo> {

	@Override
	public void updateListagemQuery(QueryBuilder<Veiculomodelo> query, FiltroListagem _filtro) {
		VeiculomodeloFiltro filtro = (VeiculomodeloFiltro) _filtro;
		
		query
			.select("veiculomodelo.cdveiculomodelo, veiculomodelo.nome, veiculomodelo.descricao, " +
					"veiculomodelo.limiteodometro, fornecedor.nome")
			.leftOuterJoin("veiculomodelo.categoriaveiculo categoriaveiculo")
			.leftOuterJoin("veiculomodelo.veiculotipo veiculotipo")
			.leftOuterJoin("veiculomodelo.fornecedor fornecedor")
			.where("categoriaveiculo = ?", filtro.getCategoriaveiculo())
			.where("veiculotipo = ?", filtro.getVeiculotipo())
			.where("fornecedor = ?", filtro.getFornecedor())
			.whereLikeIgnoreAll("veiculomodelo.nome", filtro.getNome());
	}

	public List<Veiculomodelo> findByTipocontrole(Tipocontroleveiculo tipocontroleveiculo) {
		return query()
				.select("veiculomodelo.cdveiculomodelo, veiculomodelo.nome, veiculomodelo.descricao ")
				.leftOuterJoin("veiculomodelo.categoriaveiculo categoriaveiculo")
				.where("categoriaveiculo.tipocontroleveiculo = ?", tipocontroleveiculo)
				.list();
	}
	
	public Veiculomodelo loadWithCategoria(Veiculomodelo veiculomodelo) {
		return query()
				.select("veiculomodelo.cdveiculomodelo, veiculomodelo.nome, veiculomodelo.descricao, categoriaveiculo.tipocontroleveiculo ")
				.leftOuterJoin("veiculomodelo.categoriaveiculo categoriaveiculo")
				.where("veiculomodelo = ?", veiculomodelo)
				.unique();
	}
}
