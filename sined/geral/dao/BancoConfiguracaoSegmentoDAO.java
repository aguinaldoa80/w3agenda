package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoSegmento;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.BancoConfiguracaoSegmentoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class BancoConfiguracaoSegmentoDAO extends GenericDAO<BancoConfiguracaoSegmento> {

	@Override
	public void updateListagemQuery(QueryBuilder<BancoConfiguracaoSegmento> query, FiltroListagem _filtro) {
		BancoConfiguracaoSegmentoFiltro filtro = (BancoConfiguracaoSegmentoFiltro) _filtro;
		query
			.leftOuterJoinFetch("bancoConfiguracaoSegmento.banco banco")
			.whereLikeIgnoreAll("bancoConfiguracaoSegmento.nome", filtro.getNome())
			.where("bancoConfiguracaoSegmento.banco = ?", filtro.getBanco())
			.where("bancoConfiguracaoSegmento.tipoSegmento = ?", filtro.getTipoSegmento());
		super.updateListagemQuery(query, filtro);
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<BancoConfiguracaoSegmento> query) {
		query
			.leftOuterJoinFetch("bancoConfiguracaoSegmento.banco banco")
			.leftOuterJoinFetch("bancoConfiguracaoSegmento.listaBancoConfiguracaoSegmentoCampo bancoConfiguracaoSegmentoCampo")
			.orderBy("bancoConfiguracaoSegmentoCampo.posInicial");
		super.updateEntradaQuery(query);
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaBancoConfiguracaoSegmentoCampo");
		super.updateSaveOrUpdate(save);
	}
	
	/**
	 * M�todo respons�vel por retornar dados para exporta��o
	 * @param whereIn
	 * @return
	 * @since 07/06/2016
	 * @author C�sar
	 */
	public List<BancoConfiguracaoSegmento> findforWhereIn (String whereIn){
		if(whereIn == null || whereIn.isEmpty()){
			throw new SinedException("Par�metro incorreto para pesquisa!");
		}
		return query().select("bancoConfiguracaoSegmento.cdbancoconfiguracaosegmento, bancoConfiguracaoSegmento.nome," +
				"bancoConfiguracaoSegmento.tamanho,bancoConfiguracaoSegmento.tipoSegmento, banco.cdbanco, banco.nome, banco.numero," +
				"campos.cdbancoconfiguracaosegmentocampo,campos.posInicial, campos.tamanho, campos.descricao, campos.completarZero, campos.completarDireita," +
				"campos.preencherCom, campos.obrigatorio, campos.formatoData, campos.campo")
				.leftOuterJoin("bancoConfiguracaoSegmento.banco banco")
				.leftOuterJoin("bancoConfiguracaoSegmento.listaBancoConfiguracaoSegmentoCampo campos")
				.whereIn("bancoConfiguracaoSegmento.cdbancoconfiguracaosegmento", whereIn)
				.list();
	}
	/**
	 * M�todo respons�vel por validar se j� existe BancoConfiguracaoSegmento salvo com o segmento enviado no bean
	 * @param bancoConfiguracaoSegmento
	 * @return
	 * @since 08/07/2016
	 * @author C�sar
	 */
	public Boolean validaBancoConfiguracaSeg (BancoConfiguracaoSegmento bancoConfiguracaoSegmento){
		if(bancoConfiguracaoSegmento == null || bancoConfiguracaoSegmento.getNome() == null 
				|| bancoConfiguracaoSegmento.getNome().isEmpty() 
				|| bancoConfiguracaoSegmento.getTamanho() == null
				|| bancoConfiguracaoSegmento.getTipoSegmento() == null
				|| bancoConfiguracaoSegmento.getBanco() == null){
			throw new SinedException("Par�metro incorreto para pesquisa!");
		}
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.setUseTranslator(false)
				.from(BancoConfiguracaoSegmento.class)
				.where("bancoConfiguracaoSegmento <> ?", bancoConfiguracaoSegmento, bancoConfiguracaoSegmento.getCdbancoconfiguracaosegmento() != null)
				.where("bancoConfiguracaoSegmento.nome = ?", bancoConfiguracaoSegmento.getNome())
				.where("bancoConfiguracaoSegmento.tamanho = ?", bancoConfiguracaoSegmento.getTamanho())
				.where("bancoConfiguracaoSegmento.tipoSegmento = ?", bancoConfiguracaoSegmento.getTipoSegmento())
				.where("bancoConfiguracaoSegmento.banco = ?", bancoConfiguracaoSegmento.getBanco())
				.unique() > 0;
	}
	
	/**
	 * M�todo respons�vel por retornar todos o bancoseg iguais aos importados
	 * @param bancoConfiguracaoSegmento
	 * @return
	 * @since 18/07/2016
	 * @author C�sar
	 */
	public List<BancoConfiguracaoSegmento> validateForImportacao (BancoConfiguracaoSegmento bancoConfiguracaoSegmento){
		if(bancoConfiguracaoSegmento == null || bancoConfiguracaoSegmento.getNome() == null 
				|| bancoConfiguracaoSegmento.getNome().isEmpty() 
				|| bancoConfiguracaoSegmento.getTamanho() == null
				|| bancoConfiguracaoSegmento.getTipoSegmento() == null
				|| bancoConfiguracaoSegmento.getBanco() == null){
			throw new SinedException("Par�metro incorreto para pesquisa!");
		}
		return query()
				.select("bancoConfiguracaoSegmento.cdbancoconfiguracaosegmento, bancoConfiguracaoSegmento.nome")
				.where("bancoConfiguracaoSegmento.nome like ? || '%'", bancoConfiguracaoSegmento.getNome())
				.where("bancoConfiguracaoSegmento.tamanho = ?", bancoConfiguracaoSegmento.getTamanho())
				.where("bancoConfiguracaoSegmento.tipoSegmento = ?", bancoConfiguracaoSegmento.getTipoSegmento())
				.where("banco = ?", bancoConfiguracaoSegmento.getBanco())
				.list();
	}

	/**
	* M�todo que busca as configura��es de segmentos de acordo com o banco 
	*
	* @param banco
	* @return
	* @since 30/01/2017
	* @author Luiz Fernando
	*/
	public List<BancoConfiguracaoSegmento> findByBanco(Banco banco) {
		return query()
				.select("bancoConfiguracaoSegmento.cdbancoconfiguracaosegmento, bancoConfiguracaoSegmento.nome ")
				.where("bancoConfiguracaoSegmento.banco = ?", banco)
				.list();
	}

	@Override
	public ListagemResult<BancoConfiguracaoSegmento> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<BancoConfiguracaoSegmento> query = query();
		updateListagemQuery(query, filtro);
		
		return new ListagemResult<BancoConfiguracaoSegmento>(query, filtro);
	}
}
