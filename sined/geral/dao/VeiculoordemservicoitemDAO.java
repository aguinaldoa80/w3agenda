package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Categoriaiteminspecao;
import br.com.linkcom.sined.geral.bean.Ordemservicotipo;
import br.com.linkcom.sined.geral.bean.Veiculoordemservico;
import br.com.linkcom.sined.geral.bean.Veiculoordemservicoitem;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculomanutencaoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.BaixamanutencaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("tipoiteminspecao.nome, iteminspecao.nome")
public class VeiculoordemservicoitemDAO extends GenericDAO<Veiculoordemservicoitem>{

	@Override
	public void updateEntradaQuery(QueryBuilder<Veiculoordemservicoitem> query) {
		query()
		.leftOuterJoinFetch("veiculoordemservicoitem.inspecaoitem inspecaoitem")
		.leftOuterJoinFetch("veiculoordemservicoitem.veiculoordemservico veiculoordemservico")
		;
	}

//	/**
//	 * <b>M�todo respons�vel em apagar todas ordens de servi�o a partir de um cdordemservicoitem<br>
//	 * as ordens de servi�o podem ser Inpe��o, manuten��o e preventiva</b>
//	 * @author Biharck
//	 * @param id
//	 */
//	public void deleteForIdOrdemServico(Ordemservico ordemservico){
//		getHibernateTemplate().delete(ordemservico);	
//	}
	
	/**
	 * M�todo para buscar os dados para gerar o relatorio Baixa de Manute��o 
	 * @author Ramon Brazil
	 * @param BaixamanutencaoFiltro 
	 * @return Ordemservicoitem
	 */
	public List<Veiculoordemservicoitem> findByItensForStatus(BaixamanutencaoFiltro filtro){
		if (filtro == null) {
			throw new SinedException("O filtro n�o pode ser nulo.");
		}
		QueryBuilder<Veiculoordemservicoitem> query = querySined();
		query
			.select("veiculo.placa, veiculo.prefixo ,colaborador.nome,veiculoordemservicoitem.localmanut,ordemservico.dtprevista," +
					"veiculoordemservicoitem.cdveiculoordemservicoitem, veiculoordemservicoitem.status,veiculoordemservicoitem.observacao, " +
			"inspecaoitemtipo.nome ,inspecaoitem.nome,ordemservicotipo.cdordemservicotipo")
			.leftOuterJoin("veiculoordemservicoitem.ordemservico ordemservico")
			.leftOuterJoin("ordemservico.veiculo veiculo")
			.leftOuterJoin("veiculo.colaborador colaborador")
			.leftOuterJoin("ordemservico.ordemservicotipo ordemservicotipo")
			.leftOuterJoin("veiculoordemservicoitem.inspecaoitem inspecaoitem")
			.leftOuterJoin("inspecaoitem.inspecaoitemtipo inspecaoitemtipo");
		
		if(filtro.getStatus()){
			query.where("ordemservico.dtprevista >=?",filtro.getDtinicio());
			query.where("ordemservico.dtprevista <=?",filtro.getDtfim());
		}else{
			query.where("ordemservico.dtrealizada >=?",filtro.getDtinicio());
			query.where("ordemservico.dtrealizada <=?",filtro.getDtfim());
		}
		
		return query
			.whereLikeIgnoreAll("veiculo.placa", filtro.getPlaca())
			.where("veiculoordemservicoitem.status = ?",filtro.getStatus())
			.where("ordemservicotipo = ?", Ordemservicotipo.MANUTENCAO) 
			.orderBy("colaborador.nome,inspecaoitemtipo.nome ,inspecaoitem.nome")
			.list();
	}
//
//	/**
//	 * <b>M�todo que retorna o ordemservicoitem a partir de uma ordem de servico</b>
//	 * @param id
//	 * @author Biharck
//	 * @return
//	 */
//	public Ordemservicoitem findOrdemServicoItemByCodigoOrdemServico(Integer id) {
//		if (id == null) {
//			throw new W3AutoException("Par�metro C�digo n�o pode ser null.");
//		}
//		return query()
//		.select("ordemservicoitem.cdordemservicoitem, ordemservicoitem.ordemservico")
//		.where("ordemservicoitem.ordemservico.cdordemservico = ?", id)
//		.setMaxResults(1)
//		.unique();
//	}
//	
//	/**
//	 * @author Ramon Brazil
//	 * @param veiculo
//	 * @param filtro
//	 * @return
//	 */
//	public List<Ordemservicoitem> findByItensForData(Veiculo veiculo,FormularioinspecaoFiltro filtro){
//		if(filtro.getVisual()){
//			return query()
//				
//		 		.leftOuterJoinFetch("ordemservicoitem.ordemservico ordemservico")
//		 		.leftOuterJoinFetch("ordemservico.veiculo veiculo")
//		 		.leftOuterJoinFetch("ordemservico.tiposervico tiposervico")
//		 		.leftOuterJoinFetch("ordemservicoitem.iteminspecao iteminspecao")
//		 		.leftOuterJoinFetch("iteminspecao.tipoiteminspecao tipoiteminspecao")
//		 		.where("veiculo= ?",veiculo)
//		 		.where("iteminspecao.visual = ?",filtro.getVisual())
//		 		.where("ordemservico.dtrealizada =?",filtro.getDtultima())
//		 		.orderBy("tipoiteminspecao.nome, iteminspecao.nome ")
//		 		.list();
//		}else{
//			return query()
//		 		.leftOuterJoinFetch("ordemservicoitem.ordemservico ordemservico")
//		 		.leftOuterJoinFetch("ordemservico.veiculo veiculo")
//		 		.leftOuterJoinFetch("ordemservico.tiposervico tiposervico")
//		 		.leftOuterJoinFetch("ordemservicoitem.iteminspecao iteminspecao")
//		 		.leftOuterJoinFetch("iteminspecao.tipoiteminspecao tipoiteminspecao")
//		 		.where("veiculo= ?",veiculo)
//		 		.where("ordemservico.dtprevista =?",filtro.getDtagendamento())
//		 		.orderBy("tipoiteminspecao.nome, iteminspecao.nome")
//		 		.list();
//		}
//	}
//	
//	
//	
//	
//	/**
//	 * <b>M�todo respons�vel em carregar uma lista de ordem de servi�o item a partir de uma categoria<br>
//	 * contida em modelo</b>
//	 * @author Biharck
//	 * @param modelo
//	 * @return uma lista de ordem de servi�o item
//	 */
//	public List<Ordemservicoitem> loadListaOrdemServicoItem(Modelo modelo){
//		if(modelo == null || modelo.getCategoria()==null){
//			throw new W3AutoException("A propriedade n�o pode ser nula");
//		}else{
//			return query()
//				.select("iteminspecao.cditeminspecao, iteminspecao.nome, listaCategoriaiteminspecao.categoria")
//				.join("ordemservicoitem.iteminspecao iteminspecao")
//				.join("iteminspecao.listaCategoriaiteminspecao listaCategoriaiteminspecao")
//				.where("listaCategoriaiteminspecao.categoria=?",modelo.getCategoria())
//				.list()
//				;
//		}
//	}
//	
//	
//	/**
//	 * <b>M�todo respons�vel em localizar uma lista de ordem de servico item a partir de uma ordem de servico</b>
//	 * @author Biharck
//	 * @param ordemservico
//	 * @return uma lista de ordem de servico item
//	 */
//	public List<Ordemservicoitem> loadListaOrdemServicoItemByOrdemServico(Ordemservico ordemservico, Boolean tipo){
//		if(ordemservico == null){
//			throw new W3AutoException("A propriedade n�o pode ser nula");
//		}
//		if(ordemservico.getCdordemservico()== null){
//			throw new W3AutoException("o cd n�o pode ser nulo");
//		}
//		if(tipo){
//			return query()
//					.select("ordemservicoitem.cdordemservicoitem, ordemservicoitem.iteminspecao, ordemservicoitem.ordemservico," +
//							"ordemservicoitem.status,ordemservicoitem.ordemservicoiteminspecao, ordemservicoitem.status," +
//							"ordemservicoitem.ativo")
//					.where("ordemservicoitem.ordemservico =?",ordemservico)
//					.list()
//					;
//		}else{
//			return query()
//				.select("ordemservicoitem.cdordemservicoitem, ordemservicoitem.iteminspecao, ordemservicoitem.ordemservico," +
//						"ordemservicoitem.status")
//				.where("ordemservicoitem.ordemservico.cdordemservico =?",ordemservico.getCdordemservico())
//				.list()
//				;
//		}
//	}
//	
	/**
	 * Carrega todas as OSI da ordem de servi�o que est�o marcadas como true no status.
	 * @author Pedro Gon�alves
	 * @param ordemservico
	 * @return uma lista de ordem de servico item
	 */
	public List<Veiculoordemservicoitem> carregarOSIMarcados(Veiculoordemservico ordemservico){
		if(ordemservico == null){
			throw new SinedException("A propriedade n�o pode ser nula");
		}
		if(ordemservico.getCdveiculoordemservico()== null){
			throw new SinedException("o cd n�o pode ser nulo");
		}
		
		return query()
				.select("veiculoordemservicoitem.cdveiculoordemservicoitem, inspecaoitem.cdinspecaoitem, inspecaoitem.nome, veiculoordemservicoitem.status, veiculoordemservicoitem.obsevacao")
				.join("veiculoordemservicoitem.inspecaoitem inspecaoitem")
				.where("veiculoordemservicoitem.ordemservico =?", ordemservico)
				.where("veiculoordemservicoitem.status = ?",Boolean.TRUE)
				.list();
	}
//	
//	/**
//	 * <b>M�todo respons�vel em localizar um auto relacionamento</b>
//	 * @author Biharck
//	 * @param ordemservico
//	 * @return
//	 */
//	public Ordemservicoitem findForId(Ordemservico ordemservico){
//		if(ordemservico == null){
//			throw new W3AutoException("A propriedade n�o pode ser nula");
//		}
//		if(ordemservico.getCdordemservico()== null){
//			throw new W3AutoException("o cd n�o pode ser nulo");
//		}
//		return query()
//				.select("ordemservicoitem.cdordemservicoitem, ordemservicoitem.ordemservicoiteminspecao, ordemservicoitem.ordemservico")
//				.where("ordemservicoitem.ordemservico =?",ordemservico)
//				.unique()
//				;
//	}
	
	/**
	 * <B> Carrega uma ordemservicoitem completa</B>
	 * 
	 * @author Jo�o Paulo Zica
	 * @param filtro
	 * @return
	 */
	public Veiculoordemservicoitem carregaOrdemServicoItem(Veiculoordemservicoitem ordemservicoitem) {
		if (ordemservicoitem == null || ordemservicoitem.getCdveiculoordemservicoitem() == null) {
			throw new SinedException("Par�metro Filtro n�o pode ser null.");
		}
		return query()
			.leftOuterJoinFetch("veiculoordemservicoitem.ordemservico ordemservico")
			.leftOuterJoinFetch("veiculoordemservicoitem.inspecaoitem inspecaoitem")
			.leftOuterJoinFetch("veiculoordemservicoitem.ordemservicoiteminspecao ordemservicoiteminspecao")
			.leftOuterJoinFetch("ordemservico.veiculo veiculo")
			.leftOuterJoinFetch("veiculo.colaborador colaborador")
			.where("veiculoordemservicoitem = ?", ordemservicoitem)
			.unique();
	}
	
	/**
	 * <b>M�todo respons�vel em carregar uma lista de ordem de servi�o item a partir de uma categoria<br>
	 * contida em modelo</b>
	 * @author Jo�o Paulo Zica
	 * @param modelo
	 * @return uma lista de ordem de servi�o item
	 */
	public List<Veiculoordemservicoitem> findOrdemServicoItemByOrdemServico(Veiculoordemservico ordemservico) {
		if (ordemservico == null || ordemservico.getCdveiculoordemservico() == null) {
			throw new SinedException("Par�metro C�digo n�o pode ser null.");
		}
		return query()
			.leftOuterJoinFetch("veiculoordemservicoitem.ordemservico ordemservico")
			.leftOuterJoinFetch("veiculoordemservicoitem.inspecaoitem inspecaoitem")
			.leftOuterJoinFetch("veiculoordemservicoitem.ordemservicoiteminspecao ordemservicoiteminspecao")
			.where("ordemservico = ?", ordemservico)
			.list();
	}

	/**
	 * <b>M�todo respons�vel em carregar uma lista de ordem de servi�o item a partir de um filtro Manuten��o e o 
	 * status desejado de manuten��es</b>
	 * @author Jo�o Paulo Zica
	 * @param modelo
	 * @return uma lista de ordem de servi�o item
	 */
	public List<Veiculoordemservicoitem> findOrdemServicoItemByManutencaoFilter(VeiculomanutencaoFiltro filtro) {
		if (filtro == null) {
			throw new SinedException("Par�metro Filtro n�o pode ser null.");
		}
		String orderBy = "";
		if(filtro.getOrderBy()!=null && !filtro.getOrderBy().equals("")){
			orderBy = filtro.getOrderBy() + (filtro.isAsc()?" asc":" desc");
		}
		return query()
			.select("veiculoordemservico.dtrealizada, colaborador.nome, inspecaoitem.nome, veiculo.cdveiculo, veiculoordemservico.dtprevista, " +
					"veiculo.prefixo, veiculo.placa, inspecaoitem.nome, veiculoordemservicoitem.cdveiculoordemservicoitem, " +
					"ordemservicoiteminspecao.cdveiculoordemservicoitem")
			.leftOuterJoin("veiculoordemservicoitem.ordemservico veiculoordemservico")
			.leftOuterJoin("veiculoordemservicoitem.ordemservicoiteminspecao ordemservicoiteminspecao")
			.leftOuterJoin("veiculoordemservicoitem.inspecaoitem inspecaoitem")
			.leftOuterJoin("veiculoordemservico.veiculo veiculo")
			.leftOuterJoin("veiculoordemservico.ordemservicotipo ordemservicotipo")
			.leftOuterJoin("veiculo.colaborador colaborador")
			.where("veiculoordemservicoitem.status = false")
			.where("veiculoordemservicoitem.ativo = true")
//			.where("ordemservicoiteminspecao is not null")
			.where("ordemservicotipo = ?",Ordemservicotipo.MANUTENCAO)
			.whereLikeIgnoreAll("veiculo.placa",filtro.getPlaca())
			.whereLikeIgnoreAll("veiculo.prefixo",filtro.getPrefixo())
			.orderBy(orderBy)
			.list();
	}
	
	/**
	 * <b>Atualiza uma ordem de servi�o item como realizada</b>
	
	 * @author Jo�o Paulo Zica
	 * @param cdordemservicoitem
	 * @return
	 */
	public void saveOrdemServicoItem(Integer cdordemservicoitem){
		if (cdordemservicoitem == null) {
			throw new SinedException("Par�metros incorretos. Cdordemservicoitem n�o pode ser null.");
		}
		getHibernateTemplate().bulkUpdate("update Veiculoordemservicoitem ordemservicoitem set ordemservicoitem.status = ? where ordemservicoitem.cdveiculoordemservicoitem = ?",
				new Object[]{Boolean.FALSE, cdordemservicoitem});
	}
	
	/**
	 * <b>M�todo para carregar ordemservicoitem, atrav�s de uma ordemservicoiteminspecao.</b>
	 * 
	 * @param ordemservicoitem
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public Veiculoordemservicoitem findOrdemServicoItemInspecao(Veiculoordemservicoitem ordemservicoitem){
		if (ordemservicoitem == null || ordemservicoitem.getCdveiculoordemservicoitem() == null) {
			throw new SinedException("O par�metro ordemservicoitem n�o pode ser null.");
		}
		return query()
			.leftOuterJoinFetch("veiculoordemservicoitem.ordemservicoiteminspecao ordemservicoiteminspecao")
			.where("ordemservicoiteminspecao = ?", ordemservicoitem)
			.unique();
		
	}
	
//	/**
//	 * <b>M�todo respons�vel em carregar uma lista de status por ordem de servi�o</b>
//	 * @author Jo�o Paulo Zica
//	 * @param ordemservico
//	 * @return List<Boolean>
//	 */
//	public List<Ordemservicoitem> findOrdemServicoItemStatusByOrdemServico(Ordemservico ordemservico) {
//		if (ordemservico == null || ordemservico.getCdordemservico() == null) {
//			throw new W3AutoException("Par�metro Ordemservico n�o pode ser null.");
//		}
//		return query()		
//			.select("ordemservicoitem.status")
//			.leftOuterJoin("ordemservicoitem.ordemservico ordemservico")
//			.where("ordemservicoitem.ordemservico = ?", ordemservico)
//			.list();
//	}
	
	// Ramon Brazil
	/* singleton */
	private static VeiculoordemservicoitemDAO instance;
	public static VeiculoordemservicoitemDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(VeiculoordemservicoitemDAO.class);
		}
		return instance;
	}

	/**
	 * Obt�m os ids dos itens que ser�o exclu�dos.
	 *
	 * @param whereIn
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<Integer> getIdWhereNotIn(String whereIn, Veiculoordemservico bean) {
		if(bean == null || bean.getCdveiculoordemservico() == null){
			throw new SinedException("Erro na passagem de par�metros.");
		}

		SinedUtil.markAsReader();
		List<Integer> listaUpdate = getJdbcTemplate().query("SELECT OSI.CDVEICULOORDEMSERVICOITEM AS COD " +
															"FROM VEICULOORDEMSERVICOITEM OSI " +
															"WHERE OSI.CDVEICULOORDEMSERVICOITEM NOT IN ("+ whereIn +") " +
															"AND OSI.CDVEICULOORDEMSERVICO = " + bean.getCdveiculoordemservico(), 
															new RowMapper() {
																public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
																	return rs.getInt("COD");
																}
													
															});
		
		return listaUpdate;
	}

	
	/**
	 * <B> Carrega todas os veiculoordemservicoitem de uma veiculoordemservico</B>
	 * 
	 * @author Jo�o Vitor
	 * @param veiculoordemservico
	 * @return
	 */
	public List<Veiculoordemservicoitem> carregaListaOrdemServicoItem(Veiculoordemservico veiculoordemservico) {
		if (veiculoordemservico == null || veiculoordemservico.getCdveiculoordemservico() == null) {
			throw new SinedException("Par�metro Filtro n�o pode ser null.");
		}
		return query()
			.select("veiculoordemservicoitem.status")
			.where("veiculoordemservicoitem.ativo = ?" , Boolean.TRUE)
			.where("veiculoordemservicoitem.ordemservico = ?", veiculoordemservico)
			.list();
	}
	
	
	/**
	 * <B> Carrega veiculoordemservicoitem de uma Ordem de Servi�o</B>
	 * 
	 * @author Jo�o Vitor
	 * @param veiculoordemservico
	 * @return
	 */
	public Veiculoordemservicoitem carregaOrdemServicoItem(Veiculoordemservico veiculoordemservico, Categoriaiteminspecao categoriaiteminspecao) {
		if (veiculoordemservico == null || veiculoordemservico.getCdveiculoordemservico() == null) {
			throw new SinedException("Par�metro Filtro n�o pode ser null.");
		}
		return query()
			.select("veiculoordemservicoitem.status")
			.where("veiculoordemservicoitem.ativo = ?" , Boolean.TRUE)
			.where("veiculoordemservicoitem.ordemservico = ?", veiculoordemservico)
			.where("veiculoordemservicoitem.inspecaoitem = ?", categoriaiteminspecao.getInspecaoitem())
			.unique();
	}
	
}
