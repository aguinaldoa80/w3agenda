package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Servidor;
import br.com.linkcom.sined.geral.bean.Servidorinterface;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ServidorinterfaceDAO extends GenericDAO<Servidorinterface>{

	/**
	 * Retorna lista de servidorinterface de acordo com o servidor
	 * @param servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servidorinterface> findByServidor(Servidor servidor){
		return query().where("servidorinterface.servidor = ?", servidor)
					  .list();
	}
	
}
