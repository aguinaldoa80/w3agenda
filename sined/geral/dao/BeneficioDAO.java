package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Beneficio;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.BeneficioFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("beneficio.nome")
public class BeneficioDAO extends GenericDAO<Beneficio>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Beneficio> query, FiltroListagem _filtro) {
		BeneficioFiltro filtro = (BeneficioFiltro) _filtro;
		
		query
			.select("beneficio.cdbeneficio, beneficio.nome, beneficio.ativo, beneficiotipo.nome")
			.join("beneficio.beneficiotipo beneficiotipo")
			.whereLikeIgnoreAll("beneficio.nome", filtro.getNome())
			.where("beneficiotipo=?", filtro.getBeneficiotipo())
			.where("coalesce(beneficio.ativo, false)=?", filtro.getAtivo())
			.orderBy("beneficiotipo.nome");			
		
		super.updateListagemQuery(query, filtro);
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Beneficio> query) {
		query.joinFetch("beneficio.beneficiotipo beneficiotipo");
		query.leftOuterJoinFetch("beneficio.listaBeneficiovalor listaBeneficiovalor");
		query.orderBy("listaBeneficiovalor.dtinicio");
		super.updateEntradaQuery(query);
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaBeneficiovalor");
		super.updateSaveOrUpdate(save);
	}
}
