package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendahistorico;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VendahistoricoDAO extends GenericDAO<Vendahistorico>{
	
	private EmpresaService empresaService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}

	/**
	 * Busca a lista de hist�rico da venda.
	 * 
	 * @param venda
	 * @return
	 * @since 16/03/2012
	 * @author Rodrigo Freitas
	 */
	public List<Vendahistorico> findByVenda(Venda venda) {
		if(venda == null || venda.getCdvenda() == null){
			throw new SinedException("Venda n�o pode ser nula.");
		}
		return query()
					.select("vendahistorico.cdvendahistorico, vendahistorico.acao, " +
							"vendahistorico.observacao, vendahistorico.cdusuarioaltera, " +
							"vendahistorico.dtaltera")
					.where("vendahistorico.venda = ?", venda)
					.orderBy("vendahistorico.dtaltera desc")
					.list();
	}
	
	
	/**
	 * Busca lista de hist�ricos de venda a partir do cpf/cnpj de um cliente.
	 * 
	 * @param cpfcnpj, tipopessoa
	 * @since 21/05/2012
	 * @author Rafael Salvio
	 */
	public List<Vendahistorico> findByCpfCnpjCliente(String cpfcnpj, Tipopessoa tipopessoa){
		if(cpfcnpj == null || cpfcnpj.equals("")){
			throw new SinedException("Erro no par�metro.");
		}
		
		QueryBuilder<Vendahistorico> query = query();
		query
			.select("vendahistorico.dtaltera, vendahistorico.acao, vendahistorico.observacao, usuarioaltera.nome")
			.join("vendahistorico.venda venda")
			.join("venda.cliente cliente")
			.join("vendahistorico.usuarioaltera usuarioaltera");
		if(tipopessoa != null && tipopessoa == Tipopessoa.PESSOA_FISICA){
			Cpf cpf = new Cpf(cpfcnpj);
			query
				.where("cliente.cpf = ?",cpf);
		}
		else if(tipopessoa != null && tipopessoa == Tipopessoa.PESSOA_JURIDICA){
			Cnpj cnpj = new Cnpj(cpfcnpj);
			query
				.where("cliente.cnpj = ?",cnpj);
		}
		return query
					.orderBy("vendahistorico.dtaltera")
					.list();
	}
	
	/**
	 * Busca lista de hist�ricos de venda de acordo com cpf/cnpj e per�odo.
	 *
	 * @param cpfcnpj
	 * @param tipopessoa
	 * @param dtinicio
	 * @param dtfim
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Vendahistorico> findByCpfCnpjCliente(String cpfcnpj, Tipopessoa tipopessoa, Date dtinicio, Date dtfim, Empresa empresa){
		if(cpfcnpj == null || cpfcnpj.equals("")){
			throw new SinedException("Erro no par�metro.");
		}
		
		String whereInEmpresas = empresa != null? empresa.getCdpessoa().toString():
			CollectionsUtil.listAndConcatenate(empresaService.findByUsuario(), "cdpessoa", ",");
		
		QueryBuilder<Vendahistorico> query = query()
			.select("vendahistorico.dtaltera, vendahistorico.acao, vendahistorico.observacao, usuarioaltera.nome, empresa.nome")
			.join("vendahistorico.venda venda")
			.join("venda.cliente cliente")
			.join("venda.empresa empresa")
			.join("vendahistorico.usuarioaltera usuarioaltera")
			.where("vendahistorico.dtaltera >= ?", SinedDateUtils.dateToTimestampInicioDia(dtinicio))
			.where("vendahistorico.dtaltera <= ?", SinedDateUtils.dateToTimestampFinalDia(dtfim))
			.openParentheses()
				.where("empresa.cdpessoa is null")
				.or()
				.whereIn("empresa.cdpessoa", whereInEmpresas)
			.closeParentheses();
		
		if(tipopessoa != null && tipopessoa == Tipopessoa.PESSOA_FISICA){
			query.where("cliente.cpf = ?", new Cpf(cpfcnpj));
		}else if(tipopessoa != null && tipopessoa == Tipopessoa.PESSOA_JURIDICA){
			query.where("cliente.cnpj = ?", new Cnpj(cpfcnpj));
		}
		
		return query
				.orderBy("vendahistorico.dtaltera")
				.list();
	}	
}
