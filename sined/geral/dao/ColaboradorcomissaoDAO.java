package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcomissao;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColaboradorcomissaoDAO extends GenericDAO<Colaboradorcomissao>{

	/**
	 * Busca a soma das comiss�es geradas para o colaborador passado por par�metro no m�s da data passada tamb�m por par�metro.
	 *
	 * @param colaborador
	 * @param dataGerada
	 * @return
	 * @since 10/02/2012
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public Money getTotalComissaoColaboradorMes(Colaborador colaborador, Date dataGerada) {

		Date dataInicio = SinedDateUtils.firstDateOfMonth(dataGerada);
		Date dataFim = SinedDateUtils.lastDateOfMonth(dataGerada);
		
		String sql = "select sum(dc.valorcomissao) " +
					"from documentocomissao dc " +
					"join documento doc on doc.cddocumento = dc.cddocumento " +
					"join aux_documento ax on ax.cddocumento = doc.cddocumento " +
					"where dc.cdpessoa = " + colaborador.getCdpessoa() + " " +
					"and doc.dtemissao >= '" + SinedDateUtils.toStringPostgre(dataInicio) + "' " +
					"and doc.dtemissao <= '" + SinedDateUtils.toStringPostgre(dataFim) + "' ";

		SinedUtil.markAsReader();
		List<Money> listaUpdate = getJdbcTemplate().query(sql, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Money(rs.getInt(1), true);
			}
		});
		
		if(listaUpdate != null && listaUpdate.size() > 0) return listaUpdate.get(0);		
		return new Money();
	}
	
	/**
	 * Busca a soma das comiss�es geradas para o colaborador
	 * 
	 * @param colaborador
	 * @param dataGerada
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public Money buscaTotalComissaoColaboradorMes(Colaborador colaborador, Date dataGerada) {

		Date dataInicio = SinedDateUtils.firstDateOfMonth(dataGerada);
		Date dataFim = SinedDateUtils.lastDateOfMonth(dataGerada);
		
		String sql = "select sum(dc.valorcomissao) " +
					"from documentocomissao dc " +					 
					"join colaboradorcomissao cc on cc.cdcolaborador = dc.cdpessoa " +
					"join documento doc on doc.cddocumento = cc.cddocumento " +
					"where dc.cdpessoa = " + colaborador.getCdpessoa() + " " +
					"and doc.dtemissao >= '" + SinedDateUtils.toStringPostgre(dataInicio) + "' " +
					"and doc.dtemissao <= '" + SinedDateUtils.toStringPostgre(dataFim) + "' ";

		SinedUtil.markAsReader();
		List<Money> listaUpdate = getJdbcTemplate().query(sql, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Money(rs.getInt(1), true);
			}
		});
		
		if(listaUpdate != null && listaUpdate.size() > 0) return listaUpdate.get(0);		
		return new Money();
	}
}
