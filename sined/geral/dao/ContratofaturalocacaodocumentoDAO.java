package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contratofaturalocacaodocumento;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContratofaturalocacaodocumentoDAO extends GenericDAO<Contratofaturalocacaodocumento> {

	/**
	 * Busca os dados para valida��o do cancelamento de fatura de loca��o
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/09/2013
	 */
	public List<Contratofaturalocacaodocumento> findForValidacao(String whereIn) {
		return query()
					.select("contratofaturalocacaodocumento.cdcontratofaturalocacaodocumento, contratofaturalocacao.numero, " +
							"documento.cddocumento, documentoacao.cddocumentoacao, documento.dtvencimento, conta.cdconta, documentotipo.cddocumentotipo, documentotipo.boleto ")
					.join("contratofaturalocacaodocumento.contratofaturalocacao contratofaturalocacao")
					.join("contratofaturalocacaodocumento.documento documento")
					.leftOuterJoin("documento.conta conta")
					.leftOuterJoin("documento.documentotipo documentotipo")
					.join("documento.documentoacao documentoacao")
					.whereIn("contratofaturalocacao.cdcontratofaturalocacao", whereIn)
					.list();
	}

}
