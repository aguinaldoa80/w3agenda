package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Configuracaoecom;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ConfiguracaoecomDAO extends GenericDAO<Configuracaoecom>{

	@Override
	public void updateListagemQuery(QueryBuilder<Configuracaoecom> query, FiltroListagem filtro) {
		
		query
			.select("configuracaoecom.cdconfiguracaoecom, configuracaoecom.url, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
					"localarmazenagem.nome, materialtabelapreco.nome, conta.nome, configuracaoecom.principal")
			.leftOuterJoin("configuracaoecom.empresa empresa")
			.leftOuterJoin("configuracaoecom.conta conta")
			.leftOuterJoin("configuracaoecom.localarmazenagem localarmazenagem")
			.leftOuterJoin("configuracaoecom.materialtabelapreco materialtabelapreco");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Configuracaoecom> query) {
		query
			.leftOuterJoinFetch("configuracaoecom.listaConfiguracaoecomconta listaConfiguracaoecomconta")
			.leftOuterJoinFetch("configuracaoecom.localarmazenagem localarmazenagem")
			.leftOuterJoinFetch("configuracaoecom.materialtabelapreco materialtabelapreco");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaConfiguracaoecomconta");
	}

	/**
	 * Verifica se j� existe alguma configura��o principal para a empresa.
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @param cdconfiguracaoecom 
	 * @since 23/01/2014
	 */
	public boolean haveConfiguracaoPrincipalEmpresa(Empresa empresa, Integer cdconfiguracaoecom) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Configuracaoecom.class)
					.where("configuracaoecom.empresa = ?", empresa)
					.where("configuracaoecom.principal = ?", Boolean.TRUE)
					.where("configuracaoecom.cdconfiguracaoecom <> ?", cdconfiguracaoecom)
					.unique() > 0;
	}

	/**
	 * Busca a configura��o da empresa
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/01/2014
	 */
	public Configuracaoecom findPrincipalByEmpresa(Empresa empresa) {
		return query()
					.select("configuracaoecom.cdconfiguracaoecom, configuracaoecom.url, empresa.cdpessoa, empresa.nome, " +
							"empresa.razaosocial, empresa.nomefantasia, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
							"materialtabelapreco.cdmaterialtabelapreco, materialtabelapreco.nome, conta.cdconta, conta.nome, " +
							"conta2.cdconta, conta2.nome, documentotipo2.cddocumentotipo")
					.leftOuterJoin("configuracaoecom.empresa empresa")
					.leftOuterJoin("configuracaoecom.conta conta")
					.leftOuterJoin("configuracaoecom.localarmazenagem localarmazenagem")
					.leftOuterJoin("configuracaoecom.materialtabelapreco materialtabelapreco")
					.leftOuterJoin("configuracaoecom.listaConfiguracaoecomconta listaConfiguracaoecomconta")
					.leftOuterJoin("listaConfiguracaoecomconta.conta conta2")
					.leftOuterJoin("listaConfiguracaoecomconta.documentotipo documentotipo2")
					.where("empresa = ?", empresa)
					.where("configuracaoecom.principal = ?", Boolean.TRUE)
					.unique();
	}
	
	@Override
	public ListagemResult<Configuracaoecom> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Configuracaoecom> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("configuracaoecom.cdconfiguracaoecom");
		
		return new ListagemResult<Configuracaoecom>(query, filtro);
	}
}
