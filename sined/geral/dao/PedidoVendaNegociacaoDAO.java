package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.PedidoVendaNegociacao;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PedidoVendaNegociacaoDAO extends GenericDAO<PedidoVendaNegociacao>{

	/**
	 * Busca a lista de pagamentos de um pedido de venda.
	 *
	 * @param pedidovenda
	 * @return
	 * @since 16/11/2011
	 * @author Rodrigo Freitas
	 */
	public List<PedidoVendaNegociacao> findByPedidovenda(Pedidovenda pedidovenda,boolean isBuscarCancelados){
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null){
			throw new SinedException("Pedido de venda n�o pode ser nulo.");
		}
		QueryBuilder<PedidoVendaNegociacao> query = querySined()
			.select("pedidoVendaNegociacao.cdPedidoVendaNegociacao, pedidoVendaNegociacao.banco, pedidoVendaNegociacao.emitente, pedidoVendaNegociacao.cpfcnpj, " +
					"pedidoVendaNegociacao.agencia, pedidoVendaNegociacao.conta, pedidoVendaNegociacao.numero, " +
					"pedidoVendaNegociacao.valorOriginal, pedidoVendaNegociacao.dataParcela, documentoTipo.cddocumentotipo, documentoTipo.cartao, " +
					"documentoTipo.antecipacao, documentoTipo.nome, documento.cddocumento, documento.numero, documento.valor, " +
					"documentoAntecipacao.cddocumento, documentoAntecipacao.numero, documentoAntecipacao.valor, prazopagamento.cdprazopagamento, " +
					"prazopagamento.nome, prazopagamento.juros, pedidovenda.cdpedidovenda, pedidoVendaNegociacao.valorJuros, cheque.cdcheque," +
					"documentoacao.cddocumentoacao, documentoacaoAntecipacao.cddocumentoacao ")
				.join("pedidoVendaNegociacao.pedidovenda pedidovenda")
				.join("pedidoVendaNegociacao.documentoTipo documentoTipo")
				.leftOuterJoin("pedidovenda.prazopagamento prazopagamento")
				.leftOuterJoin("pedidoVendaNegociacao.documento documento")
				.leftOuterJoin("documento.documentoacao documentoacao")
				.leftOuterJoin("pedidoVendaNegociacao.documentoAntecipacao documentoAntecipacao")
				.leftOuterJoin("documentoAntecipacao.documentoacao documentoacaoAntecipacao")
				.leftOuterJoin("pedidoVendaNegociacao.cheque cheque")
				.where("pedidovenda = ?", pedidovenda)
				.orderBy("pedidoVendaNegociacao.dataParcela");
		if(!isBuscarCancelados){
			query.where("documentoacao <> ?", Documentoacao.CANCELADA);
		}
		
		
		return query.list();	
	}

	
	
	
}
