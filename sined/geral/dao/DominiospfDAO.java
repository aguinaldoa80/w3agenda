package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Dominiospf;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.DominiospfFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DominiospfDAO extends GenericDAO<Dominiospf>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Dominiospf> query, FiltroListagem _filtro) {
		DominiospfFiltro filtro = (DominiospfFiltro)_filtro;
		query.leftOuterJoinFetch("dominiospf.dominio dominio")
			 .leftOuterJoinFetch("dominiospf.dominioredirecionar dominioredirecionar")
		 	 .leftOuterJoinFetch("dominiospf.contratomaterial contratomaterial")
		 	 .leftOuterJoinFetch("contratomaterial.contrato contrato")		 	 
		 	 .where("dominiospf.dominio = ?", filtro.getDominio())
			 .where("dominiospf.contratomaterial = ?", filtro.getContratomaterial())
			 .where("contrato.cliente = ?", filtro.getCliente());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Dominiospf> query) {
		query.leftOuterJoinFetch("dominiospf.dominio dominio")
			 .leftOuterJoinFetch("dominiospf.dominioredirecionar dominioredirecionar")
		 	 .leftOuterJoinFetch("dominiospf.contratomaterial contratomaterial")
		 	 .leftOuterJoinFetch("contratomaterial.contrato contrato")
		 	 .leftOuterJoinFetch("dominiospf.listaDominiospfip listaDominiospfip");
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaDominiospfip");		
	}

}
