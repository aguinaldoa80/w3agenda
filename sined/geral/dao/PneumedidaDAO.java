package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Pneumedida;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PneumedidaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

@DefaultOrderBy("pneumedida.nome")
public class PneumedidaDAO extends GenericDAO<Pneumedida>{

	/**
	* M�todo autocomplete para buscar medida de pneu
	*
	* @param q
	* @return
	* @since 23/10/2015
	* @author Luiz Fernando
	*/
	public List<Pneumedida> findAutocomplete(String q, boolean otr) {
		QueryBuilder<Pneumedida> query = query()
								.select("pneumedida.cdpneumedida, pneumedida.nome")
								.whereLikeIgnoreAll("pneumedida.nome", q)
								.orderBy("pneumedida.nome")
								.autocomplete();
		if(otr){
			query.where("coalesce(pneumedida.otr, false) = true");
		}else{
			query.where("coalesce(pneumedida.otr, false) = false");
		}
		return query.list();
	}
	
	/**
	 * Busca a lista de pneumedida para enviar ao W3Producao
	 * @param whereIn
	 * @return
	 */
	public List<Pneumedida> findForW3Producao(String whereIn){
		return query()
					.select("pneumedida.cdpneumedida, pneumedida.nome")
					.whereIn("pneumedida.cdpneumedida", whereIn)
					.list();
	}
	
	/**
	 * Busca a medida de pneu pelo nome.
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/04/2016
	 */
	public Pneumedida loadByNome(String nome) {
		if(nome == null || nome.trim().equals("")){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
				.select("pneumedida.cdpneumedida, pneumedida.nome")
				.where("upper(pneumedida.nome) like ?", nome.toUpperCase())
				.unique();
	}
	
	/**
	* M�todo que busca as medidas dos pneus
	*
	* @param whereIn
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<Pneumedida> findForAndroid(String whereIn) {
		return query()
		.select("pneumedida.cdpneumedida, pneumedida.nome")
		.whereIn("pneumedida.cdpneumedida", whereIn)
		.list();
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Pneumedida> query,
			FiltroListagem _filtro) { 
		PneumedidaFiltro pneuMedidaFiltro = (PneumedidaFiltro) _filtro;
			query
				.select("pneumedida.cdpneumedida, pneumedida.nome, pneumedida.codigointegracao, pneumedida.circunferencia, pneumedida.otr")
				.where("pneumedida.otr=?",pneuMedidaFiltro.getOtr());
	}
	
	
	public List<Pneumedida> loadByOtr(){
		return query()
				.select("pneumedida.cdpneumedida, pneumedida.nome")
				.where("pneumedida.otr = true")
				.list();
			
	}

	public List<Pneumedida> findByPneuSegmento(Boolean otr) {
		QueryBuilderSined<Pneumedida> query = querySined();
		query
			.select("pneumedida.cdpneumedida, pneumedida.nome, pneumedida.codigointegracao, pneumedida.circunferencia, pneumedida.otr");
			if(Boolean.TRUE.equals(otr)){
				query.where("pneumedida.otr = true");
			}else {
				query.where("pneumedida.otr = false");
			}
			return query
					.orderBy("pneumedida.nome")
					.list();
	}
}
