package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Exameclinica;
import br.com.linkcom.sined.geral.bean.Exameconvenio;
import br.com.linkcom.sined.geral.bean.Exameresponsavel;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ExameconvenioFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ExameconvenioDAO extends GenericDAO<Exameconvenio>{
	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}

	
	@Override
	public void updateEntradaQuery(QueryBuilder<Exameconvenio> query) {
		query
			.leftOuterJoinFetch("exameconvenio.logotipo logotipo");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Exameconvenio> query, FiltroListagem _filtro) {
		
		ExameconvenioFiltro filtro = (ExameconvenioFiltro)_filtro;
		
		query
			.select("exameconvenio.cdexameconvenio, exameconvenio.nome")
			.whereLikeIgnoreAll("exameconvenio.nome", filtro.getNome());
		
		super.updateListagemQuery(query, _filtro);
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {		
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				Exameconvenio exameconvenio = (Exameconvenio)save.getEntity();
				if(exameconvenio.getLogotipo()!= null)
					arquivoDAO.saveFile(save.getEntity(), "logotipo");
				return null;
			}			
		});		
	}	
	

	/**
	 * Retorna todos os registros de Cl�nica.
	 * Permite tamb�m a especifica��o dos campos do select e defini��o de ordena��o.
	 * @param campos
	 * @param orderBy
	 * @return
	 * @author Taidson
	 * @since 22/09/2010
	 */
	public List<Exameconvenio> findAll(String campos, String orderBy) {
		QueryBuilder<Exameconvenio> queryBuilder = query();
		if (campos != null && !campos.trim().equals("")) {
			queryBuilder.select(campos);
		}
		if (orderBy != null && !orderBy.trim().equals("")) {
			queryBuilder.orderBy(orderBy);
		}
		
		return queryBuilder.list();
	}
	
	/**
	 * Retorna lista de conv�nios para popular a combo de conv�ncio em Emitir autoriza��o exame,
	 * conforme a Cl�nica e/ou o Profissional selecionado(s).
	 * @param clinica
	 * @param responsavel
	 * @return
	 * @author Taidson
	 * @since 22/09/2010
	 */
	public List<Exameconvenio> findByClinicaResponsavel(Exameclinica clinica, Exameresponsavel responsavel){
		QueryBuilder<Exameconvenio> query = query()
		.select("exameconvenio.cdexameconvenio, exameconvenio.nome")
		.leftOuterJoin("exameconvenio.listaExameconvenioclinica convenioclinica")
		.leftOuterJoin("exameconvenio.listaExameconvenioresponsavel convenioresponsavel");
		if((clinica != null && clinica.getCdexameclinica() != null)&&(responsavel != null && responsavel.getCdexameresponsavel() != null) ){
			query.where("convenioclinica.exameclinica = ?", clinica);
			query.where("convenioresponsavel.exameresponsavel = ?", responsavel);
		}
		if(clinica != null && clinica.getCdexameclinica() != null)
			query.where("convenioclinica.exameclinica = ?", clinica);
		if(responsavel != null && responsavel.getCdexameresponsavel() != null)
			query.where("convenioresponsavel.exameresponsavel = ?", responsavel);
		return query.list();
	}
}
