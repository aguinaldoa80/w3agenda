package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.sined.geral.bean.Itemlistaservico;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ItemlistaservicoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ItemlistaservicoDAO extends GenericDAO<Itemlistaservico> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Itemlistaservico> query, FiltroListagem _filtro) {
		ItemlistaservicoFiltro filtro = (ItemlistaservicoFiltro) _filtro;
		
		query
			.select("itemlistaservico.cditemlistaservico, itemlistaservico.codigo, itemlistaservico.descricao")
			.whereLikeIgnoreAll("itemlistaservico.codigo", filtro.getCodigo())
			.whereLikeIgnoreAll("itemlistaservico.descricao", filtro.getDescricao())
			;
	}

	/**
	 * Carrega o item da lista de servi�o a partir do c�digo.
	 *
	 * @param codigo
	 * @return
	 * @since 30/03/2012
	 * @author Rodrigo Freitas
	 */
	public Itemlistaservico loadByCodigo(String codigo) {
		return query()
					.select("itemlistaservico.cditemlistaservico, itemlistaservico.codigo, itemlistaservico.descricao")
					.where("translate(itemlistaservico.codigo, '.', '') like ?", StringUtils.soNumero(codigo))
					.unique();
	}
	
}
