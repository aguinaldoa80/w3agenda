package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.ImportacaoProvisaoColuna;
import br.com.linkcom.sined.geral.bean.enumeration.ImportacaoProvisaoCampo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ImportacaoProvisaoColunaFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ImportacaoProvisaoColunaDAO extends GenericDAO<ImportacaoProvisaoColuna> {
	@Override
	public void updateListagemQuery(QueryBuilder<ImportacaoProvisaoColuna> query, FiltroListagem _filtro) {
		ImportacaoProvisaoColunaFiltro filtro = (ImportacaoProvisaoColunaFiltro) _filtro;
		
		query.select("importacaoProvisaoColuna.cdImportacaoProvisaoColuna, importacaoProvisaoColuna.coluna, importacaoProvisaoColuna.tipo")
			 .where("importacaoProvisaoColuna.coluna = ?", filtro.getColuna())
			 .where("importacaoProvisaoColuna.tipo = ?", filtro.getTipo());
	}
	
	public boolean existeColunaCadastrada(ImportacaoProvisaoColuna bean) {
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.from(ImportacaoProvisaoColuna.class)
				.where("importacaoProvisaoColuna.coluna = ?", bean.getColuna())
				.where("importacaoProvisaoColuna.cdImportacaoProvisaoColuna not in (?)", 
							bean.getCdImportacaoProvisaoColuna(), 
							bean.getCdImportacaoProvisaoColuna() != null)
				.where("importacaoProvisaoColuna.tipo <> ?", ImportacaoProvisaoCampo.MATRICULA)
				.unique() > 0;
	}

	public List<ImportacaoProvisaoColuna> findByTipo(ImportacaoProvisaoCampo importacaoProvisaoCampo) {
		return query()
				.select("importacaoProvisaoColuna.cdImportacaoProvisaoColuna, importacaoProvisaoColuna.coluna, importacaoProvisaoColuna.tipo")
				.where("importacaoProvisaoColuna.tipo = ?", importacaoProvisaoCampo)
				.list();
	}
}
