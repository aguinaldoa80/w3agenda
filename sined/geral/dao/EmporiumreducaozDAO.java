package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Emporiumreducaoz;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.EmporiumreducaozFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EmporiumreducaozDAO extends GenericDAO<Emporiumreducaoz>{

	@Override
	public void updateListagemQuery(QueryBuilder<Emporiumreducaoz> query, FiltroListagem _filtro) {
		EmporiumreducaozFiltro filtro = (EmporiumreducaozFiltro) _filtro;
		
		query
			.select("emporiumreducaoz.cdemporiumreducaoz, emporiumreducaoz.data, emporiumreducaoz.gtfinal, " +
					"emporiumreducaoz.crz, emporiumreducaoz.valorvendabruta, emporiumpdv.descricao, " +
					"emporiumreducaoz.cro, emporiumreducaoz.valorcancelados, emporiumreducaoz.valordesconto, " +
					"emporiumreducaoz.valoracrescimo")
			.leftOuterJoin("emporiumreducaoz.emporiumpdv emporiumpdv")
			.leftOuterJoin("emporiumpdv.empresa empresa")
			.where("emporiumpdv = ?", filtro.getEmporiumpdv())
			.where("empresa = ?", filtro.getEmpresa())
			.where("emporiumreducaoz.data >= ?", filtro.getData1())
			.where("emporiumreducaoz.data <= ?", filtro.getData2())
			.orderBy("emporiumreducaoz.data desc");
	}

	public List<Emporiumreducaoz> findForSped(Emporiumpdv emporiumpdv, Date dtinicio, Date dtfim) {
		return query()
					.where("emporiumreducaoz.emporiumpdv = ?", emporiumpdv)
					.where("emporiumreducaoz.data >= ?", dtinicio)
					.where("emporiumreducaoz.data <= ?", dtfim)
					.orderBy("emporiumreducaoz.data")
					.list();
	}

	public boolean haveReducaozEmpresa(Empresa empresa, Date dataInicio, Date dataFim) {
		return newQueryBuilderSined(Long.class)
					.from(Emporiumreducaoz.class)
					.select("count(*)")
					.join("emporiumreducaoz.emporiumpdv emporiumpdv")
					.where("emporiumpdv.empresa = ?", empresa)
					.where("emporiumreducaoz.data >= ?", dataInicio)
					.where("emporiumreducaoz.data <= ?", dataFim)
					.unique() > 0;
	}

	public Emporiumreducaoz loadReducaoz(Integer crz, Date data, Emporiumpdv emporiumpdv) {
		return query()
					.where("emporiumreducaoz.emporiumpdv = ?", emporiumpdv)
					.where("emporiumreducaoz.data = ?", data)
					.where("emporiumreducaoz.crz = ?", crz)
					.unique();
	}

	public List<Emporiumreducaoz> findForAcerto(Date dtinicio, Date dtfim) {
		return query()
					.joinFetch("emporiumreducaoz.emporiumpdv emporiumpdv")
					.where("emporiumreducaoz.data >= ?", dtinicio)
					.where("emporiumreducaoz.data <= ?", dtfim)
					.orderBy("emporiumreducaoz.data")
					.list();
	}

	public Emporiumreducaoz findLastReducaoz(Date data, Emporiumpdv emporiumpdv) {
		return query()
				.where("emporiumreducaoz.emporiumpdv = ?", emporiumpdv)
				.where("emporiumreducaoz.data < ?", data)
				.orderBy("emporiumreducaoz.data desc")
				.setMaxResults(1)
				.unique();
	}
	
}
