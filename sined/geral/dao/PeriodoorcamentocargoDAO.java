package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Periodoorcamentocargo;
import br.com.linkcom.sined.geral.bean.Tarefaorcamentorh;
import br.com.linkcom.sined.geral.bean.Tipocargo;
import br.com.linkcom.sined.geral.service.OrcamentorecursohumanoService;
import br.com.linkcom.sined.geral.service.RecursocomposicaoService;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PeriodoorcamentocargoDAO extends GenericDAO<Periodoorcamentocargo> {

	private OrcamentorecursohumanoService orcamentorecursohumanoService;
	private RecursocomposicaoService recursocomposicaoService;
	
	public void setOrcamentorecursohumanoService(OrcamentorecursohumanoService orcamentorecursohumanoService) {
		this.orcamentorecursohumanoService = orcamentorecursohumanoService;
	}
	
	public void setRecursocomposicaoService(RecursocomposicaoService recursocomposicaoService) {
		this.recursocomposicaoService = recursocomposicaoService;
	}
	
	/***
	 * Verifica se j� existe o histograma calculado para um determinado or�amento.
	 * 
	 * @param orcamento
	 * @return verdadeiro ou falso
	 * @throws SinedException - caso o or�amento seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public Boolean existeHistograma(Orcamento orcamento) {
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		Long resultado =
			newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Periodoorcamentocargo.class)
				.setUseTranslator(false)
				.where("periodoorcamentocargo.orcamento=?",orcamento)
				.unique();
		
		return resultado != null && resultado > 0;
	}	
	
	/***
	 * Retorna os cargos e suas quantidades associados �s tarefas de um determinado or�amento
	 * 
	 * @param orcamento
	 * @return List<Periodoorcamentocargo>
	 * @throws SinedException - caso o or�amento seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Periodoorcamentocargo> findByOrcamento(Orcamento orcamento) {
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		return
			query()
				.select("periodoorcamentocargo.cdperiodoorcamentocargo, periodoorcamentocargo.cargohorasemanal, periodoorcamentocargo.totalhoracalculada, " +
						"periodoorcamento.cdperiodoorcamento, periodoorcamento.numero, periodoorcamento.qtde, " +
						"cargo.cdcargo, cargo.nome, cargo.custohora, tipocargo.cdtipocargo, tipocargo.nome")
				.join("periodoorcamentocargo.orcamento orcamento")
				.join("periodoorcamentocargo.cargo cargo")
				.join("cargo.tipocargo tipocargo")
				.leftOuterJoin("periodoorcamentocargo.listaPeriodoorcamento periodoorcamento")
				.where("orcamento = ?", orcamento)
				.orderBy("cargo.nome, periodoorcamento.numero")
				.list();				
	}
	
//	/***
//	 * Retorna os cargos e suas quantidades associados �s tarefas de um determinado or�amento
//	 * 
//	 * @param orcamento
//	 * @return List<Periodoorcamentocargo>
//	 * @throws SinedException - caso o or�amento seja nulo
//	 * 
//	 * @author Rodrigo Alvarenga
//	
//	OBS: A princ�pio, este m�todo retornaria os mesmos resultados do m�todo logo abaixo deste. Por�m, ao utilizar o getJdbcTemplate,
//		 ele ignora a transa��o corrente e executa a consulta em outra, podendo retornar resultados inv�lidos dependendo da situa��o.
	
//	 */
//	@SuppressWarnings("unchecked")
//	public List<Periodoorcamentocargo> carregaListaCargoQuantidade(Orcamento orcamento) {
//		if (orcamento == null || orcamento.getCdorcamento() == null) {
//			throw new SinedException("O or�amento n�o pode ser nulo.");
//		}
//		
//		List<Periodoorcamentocargo> listaPeriodoorcamentocargo = 
//			getJdbcTemplate()
//				.query(
//						"SELECT C.CDCARGO, C.NOME, C.TOTALHORASEMANA, C.CUSTOHORA, TC.CDTIPOCARGO, SUM(TORH.QUANTIDADE) AS SOMA " +
//						"FROM TAREFAORCAMENTORH TORH " +
//						"JOIN CARGO C ON TORH.CDCARGO = C.CDCARGO " +
//						"JOIN TIPOCARGO TC ON C.CDTIPOCARGO = TC.CDTIPOCARGO " +
//						"JOIN TAREFAORCAMENTO T ON TORH.CDTAREFAORCAMENTO = T.CDTAREFAORCAMENTO " +
//						"JOIN ORCAMENTO O ON T.CDORCAMENTO = O.CDORCAMENTO " +
//						"WHERE O.CDORCAMENTO = " + orcamento.getCdorcamento() + " " +
//						"GROUP BY C.CDCARGO,C.NOME,C.TOTALHORASEMANA,C.CUSTOHORA,TC.CDTIPOCARGO", 
//		new RowMapper() {
//			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
//				Periodoorcamentocargo bean = new Periodoorcamentocargo();
//				bean.setTotalhoracalculada(rs.getInt("SOMA"));
//				bean.setCargohorasemanal(rs.getInt("TOTALHORASEMANA"));
//				
//				Cargo cargo = new Cargo();
//				cargo.setCdcargo(rs.getInt("CDCARGO"));				
//				cargo.setNome(rs.getString("NOME"));
//				cargo.setTotalhorasemana(rs.getInt("TOTALHORASEMANA"));
//				cargo.setCustohora(rs.getObject("CUSTOHORA") != null ? new Money(rs.getDouble("CUSTOHORA")) : null);
//				
//				Tipocargo tipoCargo = new Tipocargo();
//				tipoCargo.setCdtipocargo(rs.getInt("CDTIPOCARGO"));
//				cargo.setTipocargo(tipoCargo);
//				
//				bean.setCargo(cargo);
//				
//				return bean;
//			}
//		});
//		
//		return listaPeriodoorcamentocargo;
//	}
	
	/***
	 * Retorna os cargos (com suas quantidades) associados �s tarefas de um determinado or�amento.
	 * Quando n�o � calculado o Histograma.
	 * 
	 * @param orcamento
	 * @return List<Orcamentorecursohumano>
	 * @throws SinedException - caso o or�amento seja nulo
	 * 
	 * @author Rodrigo Freitas
	 */
	public Map<Cargo, Double> carregaListaCargoQuantidadeSemHistograma(Orcamento orcamento){
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		
		
		Map<Cargo, Double> map = new HashMap<Cargo, Double>();
		Cargo cargo;
		Tipocargo tipoCargo; 
		Double qtde;
		
		QueryBuilder<Object[]> query = newQueryBuilder(Object[].class);
		
		List<Object[]> lista =
			query
				.select("cargo.cdcargo, cargo.nome, cargo.totalhorasemana, cargo.custohora, tipocargo.cdtipocargo, sum(tarefaorcamentorh.quantidade) as totalhoracalculada")
				.setUseTranslator(false)
				.from(Tarefaorcamentorh.class)
				.join("tarefaorcamentorh.cargo cargo")
				.join("cargo.tipocargo tipocargo")
				.join("tarefaorcamentorh.tarefaorcamento tarefaorcamento")
				.join("tarefaorcamento.orcamento orcamento")
				.where("orcamento = ?", orcamento)
				.groupBy("cargo.cdcargo, cargo.nome, cargo.totalhorasemana, cargo.custohora, tipocargo.cdtipocargo")
				.list();
		
		if (lista != null) {
			for (Object[] objeto : lista) {
				cargo = new Cargo();
				cargo.setCdcargo((Integer) objeto[0]);		
				cargo.setNome((String) objeto[1]);
				
				if(objeto[2] != null){
					cargo.setTotalhorasemana((Double) objeto[2]);
				} else {
					cargo.setTotalhorasemana(44d);
				}
				cargo.setCustohora((Money) objeto[3]);
				
				tipoCargo = new Tipocargo();
				tipoCargo.setCdtipocargo((Integer) objeto[4]);
				cargo.setTipocargo(tipoCargo);	
				
				if(map.containsKey(cargo)){
					qtde = map.get(cargo);
					qtde += ((Double) objeto[5]);
				} else {
					qtde = ((Double) objeto[5]);
				}
				map.put(cargo, qtde);
			}
		}
		return map;
	}
	
	/***
	 * Retorna os cargos (com suas quantidades) associados �s tarefas de um determinado or�amento
	 * 
	 * @param orcamento
	 * @return List<Periodoorcamentocargo>
	 * @throws SinedException - caso o or�amento seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Periodoorcamentocargo> carregaListaCargoQuantidade(Orcamento orcamento) {
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		
		List<Periodoorcamentocargo> listaPeriodoOrcamentoCargo = new ArrayList<Periodoorcamentocargo>();
		Periodoorcamentocargo periodoOrcamentoCargo;
		Cargo cargo;
		Tipocargo tipoCargo;
		
		QueryBuilder<Object[]> query = newQueryBuilder(Object[].class);
		
		List<Object[]> lista =
			query
				.select("cargo.cdcargo, cargo.nome, cargo.totalhorasemana, cargo.custohora, tipocargo.cdtipocargo, sum(tarefaorcamentorh.quantidade) as totalhoracalculada")
				.setUseTranslator(false)
				.from(Tarefaorcamentorh.class)
				.join("tarefaorcamentorh.cargo cargo")
				.join("cargo.tipocargo tipocargo")
				.join("tarefaorcamentorh.tarefaorcamento tarefaorcamento")
				.join("tarefaorcamento.orcamento orcamento")
				.where("orcamento = ?", orcamento)
				.groupBy("cargo.cdcargo, cargo.nome, cargo.totalhorasemana, cargo.custohora, tipocargo.cdtipocargo")
				.list();
		
		if (lista != null) {
			for (Object[] objeto : lista) {
				periodoOrcamentoCargo = new Periodoorcamentocargo();

				cargo = new Cargo();
				cargo.setCdcargo((Integer) objeto[0]);				
				cargo.setNome((String) objeto[1]);
				cargo.setTotalhorasemana((Double) objeto[2]);
				cargo.setCustohora((Money) objeto[3]);				

				tipoCargo = new Tipocargo();
				tipoCargo.setCdtipocargo((Integer) objeto[4]);
				cargo.setTipocargo(tipoCargo);				
				
				periodoOrcamentoCargo.setTotalhoracalculada((Double) objeto[5]);
				periodoOrcamentoCargo.setCargohorasemanal((Double) objeto[2]);
				periodoOrcamentoCargo.setCargo(cargo);
				
				listaPeriodoOrcamentoCargo.add(periodoOrcamentoCargo);
			}
		}
		return listaPeriodoOrcamentoCargo;
	}	
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaPeriodoorcamento");
	}
	
	/***
	 * Insere/atualiza os registros presentes em listaPeriodoOrcamentoCargoForUpdate.
	 * Apaga os registros presentes em listaPeriodoOrcamentoCargoForDelete.
	 * Atualiza os registros na tabela Orcamentorecursohumano
	 * Atualiza os registros na tabela Recursocomposicao
	 *
	 * @param orcamento
	 * @param listaPeriodoOrcamentoCargoForUpdate
	 * @param listaPeriodoOrcamentoCargoForDelete 
	 * @return 
	 * @throws SinedException - caso um dos par�metros seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public void saveHistograma(final Orcamento orcamento, final List<Periodoorcamentocargo> listaPeriodoOrcamentoCargoForUpdate, final List<Periodoorcamentocargo> listaPeriodoOrcamentoCargoForDelete) {
		if (listaPeriodoOrcamentoCargoForUpdate == null || listaPeriodoOrcamentoCargoForDelete == null) {
			throw new SinedException("O par�metro n�o pode ser nulo.");
		}		
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {

				//Apaga os registros
				for (Periodoorcamentocargo periodoOrcamentoCargoForDelete : listaPeriodoOrcamentoCargoForDelete) {
					periodoOrcamentoCargoForDelete.setOrcamento(null);
					delete(periodoOrcamentoCargoForDelete);
				}

				//Insere/atualiza os registros				
				for (Periodoorcamentocargo periodoOrcamentoCargoForUpdatde : listaPeriodoOrcamentoCargoForUpdate) {
					saveOrUpdate(periodoOrcamentoCargoForUpdatde);
				}
				
				//Atualiza os dados da tabela Orcamentorecursohumano
				orcamentorecursohumanoService.atualizaOrcamentoRecursoHumanoFlex(orcamento, listaPeriodoOrcamentoCargoForUpdate);
				
				//Atualiza os dados da tabela Recursocomposicao
				recursocomposicaoService.atualizaOrcamentoRecursoGeralFlex(orcamento, listaPeriodoOrcamentoCargoForUpdate);
				
				return null;
			}
		});
	}

	/**
	 * Deleta o histograma pelo or�amento.
	 *
	 * @param orcamento
	 * @author Rodrigo Freitas
	 */
	public void deleteByOrcamento(Orcamento orcamento) {
		if(orcamento == null || orcamento.getCdorcamento() == null){
			throw new SinedException("Erro na passagem d epar�metro.");
		}
		getJdbcTemplate().execute("DELETE FROM PERIODOORCAMENTOCARGO WHERE CDORCAMENTO = " + orcamento.getCdorcamento());
	}
}
