package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratodesconto;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContratodescontoDAO extends GenericDAO<Contratodesconto>{
	
	/**
	 * Busca os descontos de um contrato
	 *
	 * @param contrato
	 * @return
	 * @since 06/08/2012
	 * @author Rodrigo Freitas
	 */
	public List<Contratodesconto> findByContrato(Contrato contrato) {
		if(contrato == null || contrato.getCdcontrato() == null){
			throw new SinedException("Contrato n�o pode ser nulo.");
		}
		return queryEntrada()
					.where("contrato = ?", contrato)
					.orderBy("contratodesconto.dtinicio, contratodesconto.cdcontratodesconto")
					.list();
	}

	/**
	 * Busca os descontos para todos os contratos passados por par�metro.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 21/12/2012
	 */
	public List<Contratodesconto> findByContratoComissinamento(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Par�metro inv�lido.");
		}
		return queryEntrada()
					.whereIn("contrato.cdcontrato", whereIn)
					.list();
	}

	/**
	 * Query centralizada da entrada.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 21/12/2012
	 */
	private QueryBuilder<Contratodesconto> queryEntrada() {
		return query()
					.select("contratodesconto.cdcontratodesconto, contratodesconto.dtinicio, contratodesconto.dtfim, " +
							"contratodesconto.motivo, contratodesconto.tiponota, contratodesconto.valor, " +
							"contratodesconto.tipodesconto, contrato.cdcontrato")
					.leftOuterJoin("contratodesconto.contrato contrato");
	}

	/**
	 * Verifica se existe o desconto para aquele contrato no dia
	 *
	 * @param contrato
	 * @param data
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/02/2014
	 */
	public boolean haveDescontoContratoData(Contrato contrato, Date data) {
		return newQueryBuilderWithFrom(Long.class)
					.select("count(*)")
					.where("contratodesconto.contrato = ?", contrato)
					.where("contratodesconto.dtinicio = ?", data)
					.where("contratodesconto.dtfim = ?", data)
					.unique() > 0;
	}

	/**
	 * M�todo que carrega a lista de desconto do contrato
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 07/04/2014
	 */
	public List<Contratodesconto> findForCobranca(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		QueryBuilder<Contratodesconto> query = query()
				.select("contratodesconto.cdcontratodesconto, contratodesconto.valor, contratodesconto.dtinicio, contratodesconto.dtfim, " +
						 "contratodesconto.tipodesconto, contratodesconto.tiponota, contrato.cdcontrato")
				 .join("contratodesconto.contrato contrato");
		
		
		SinedUtil.quebraWhereIn("contrato.cdcontrato", whereIn, query);
		return query
				.orderBy("contrato.cdcontrato")
				.list();
	}

}
