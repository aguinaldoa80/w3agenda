package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratoparcela;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContratoparcelaDAO extends GenericDAO<Contratoparcela> {

	/**
	 * M�todo que carrega a lista de contrato parcela de um contrato
	 * 
	 * @param contrato
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Contratoparcela> findByContrato(Contrato contrato) {
		if(contrato == null || contrato.getCdcontrato() == null)
			throw new SinedException("Par�metros inv�lidos.");
		return query()
			.select("contratoparcela.cdcontratoparcela, contratoparcela.valor, contratoparcela.dtvencimento, " +
					"contratoparcela.aplicarindice, contratoparcela.cobrada, " +
					"contratoparcela.desconto, contratoparcela.valornfservico, contratoparcela.valornfproduto")
			.join("contratoparcela.contrato contrato") 
			.where("contrato = ?", contrato)
			.orderBy("contratoparcela.dtvencimento")
			.list();
	}

	/**
	 * M�todo que atualiza a parcela do contrato para cobrada
	 * 
	 * @param contratoparcela
	 * @Author Tom�s Rabelo
	 */
	public void updateParcelaCobrada(Contrato contrato) {
		if(contrato == null || contrato.getCdcontrato() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		getHibernateTemplate().bulkUpdate("update Contratoparcela cp set cp.cobrada = ? where cp.id = " +
										  "(select min(cp1.cdcontratoparcela) from Contrato c " +
										  "join c.listaContratoparcela cp1 where c.id = "+contrato.getCdcontrato()+
										  " and (cp1.cobrada = false or cp1.cobrada is null))", 
				new Object[]{Boolean.TRUE});
	}

	/**
	 * Atualiza o valor da parcela.
	 *
	 * @param contratoparcela
	 * @author Rodrigo Freitas
	 */
	public void updateValorParcela(Contratoparcela contratoparcela) {
		getHibernateTemplate().bulkUpdate("update Contratoparcela cp set cp.valor = ? where cp.id = ?", 
				  								new Object[]{contratoparcela.getValor(), contratoparcela.getCdcontratoparcela()});
	}
	
	/**
	 * M�todo que atualiza a parcela do contrato para cobrada
	 * 
	 * @param contratoparcela
	 * @Author Rodrigo Freitas
	 */
	public void updateParcelaCobrada(Contratoparcela contratoparcela){
		getHibernateTemplate().bulkUpdate("update Contratoparcela cp set cp.cobrada = true where cp.id = ?", 
					new Object[]{contratoparcela.getCdcontratoparcela()});
	}
	
	/**
	* M�todo que atualiza a parcela do contrato para n�o cobrada
	*
	* @param contratoparcela
	* @since 07/11/2014
	* @author Luiz Fernando
	*/
	public void updateParcelaNaoCobrada(Contratoparcela contratoparcela){
		getHibernateTemplate().bulkUpdate("update Contratoparcela cp set cp.cobrada = false where cp.id = ?", 
					new Object[]{contratoparcela.getCdcontratoparcela()});
	}
	
	/**
	 * M�todo que atualiza a parcela do contrato para cobrada
	 * 
	 * @param contratoparcela
	 * @Author Rodrigo Freitas
	 */
	public void updateDtVencimento(Contratoparcela contratoparcela){
		getHibernateTemplate().bulkUpdate("update Contratoparcela cp set cp.dtvencimento = ? where cp.id = ?", 
				new Object[]{contratoparcela.getDtvencimento(), contratoparcela.getCdcontratoparcela()});
	}

	/**
	 * Atualiza a parcela renovando o contrato.
	 *
	 * @param contratoparcela
	 * @author Rodrigo Freitas
	 * @since 05/10/2012
	 */
	public void updateRenovacao(Contratoparcela contratoparcela) {
		getHibernateTemplate().bulkUpdate("update Contratoparcela cp set cp.dtvencimento = ?, cp.cobrada = false where cp.id = ?", 
				new Object[]{contratoparcela.getDtvencimento(), contratoparcela.getCdcontratoparcela()});
	}
	
	/**
	 * M�todo que desmarca parcela cobrada do contrato ap�s cancelamento do documento

	 * @param contratoparcela
	 * @param cobrada
	 * @author Luiz Fernando
	 */
	public void updateDesmarcarParcelacobrada(Contratoparcela contratoparcela, Boolean cobrada){
		if(contratoparcela != null && contratoparcela.getCdcontratoparcela() != null && cobrada != null){
			getHibernateTemplate().bulkUpdate("update Contratoparcela cp set cp.cobrada = ? where cp.id = ?", 
					new Object[]{cobrada, contratoparcela.getCdcontratoparcela()});
		}
	}

}
