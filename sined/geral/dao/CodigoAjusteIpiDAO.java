package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.CodigoAjusteIpi;
import br.com.linkcom.sined.geral.bean.enumeration.CreditoDebitoEnum;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CodigoAjusteIpiDAO extends GenericDAO<CodigoAjusteIpi> {

	public List<CodigoAjusteIpi> findByTipoAjuste(CreditoDebitoEnum tipoAjuste) {
		return query()
				.where("natureza = ?", tipoAjuste)
				.list();
	}

}
