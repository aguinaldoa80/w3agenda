package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cargoexameobrigatorio;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CargoexameobrigatorioDAO extends GenericDAO<Cargoexameobrigatorio>{
	
	/* singleton */
	private static CargoexameobrigatorioDAO instance;
	public static CargoexameobrigatorioDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(CargoexameobrigatorioDAO.class);
		}
		return instance;
	}
	
	public List<Cargoexameobrigatorio> findExameobrigatorioByCargo(Cargo cargo){
		if(cargo == null || cargo.getCdcargo() == null){
			throw new SinedException("Cargo n�o pode ser nulo.");
		}
		
		return query()
				.select("cargoexameobrigatorio.cdcargoexameobrigatorio, cargoexameobrigatorio.exametipo, " +
						"cargoexameobrigatorio.examenatureza, cargoexameobrigatorio.frequencia")
				.leftOuterJoin("cargoexameobrigatorio.exametipo exametipo")
				.leftOuterJoin("cargoexameobrigatorio.examenatureza examenatureza")
				.leftOuterJoin("cargoexameobrigatorio.frequencia frequencia")
				.where("cargoexameobrigatorio.cargo = ?", cargo)
				.list();
	}

}
