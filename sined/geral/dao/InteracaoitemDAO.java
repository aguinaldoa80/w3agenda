package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Interacao;
import br.com.linkcom.sined.geral.bean.Interacaoitem;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class InteracaoitemDAO extends GenericDAO<Interacaoitem>{

	/**
	 * Carrega a lista de itens de intera��o de uma espec�fica intera��o.
	 * 
	 * @param interacao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Interacaoitem> findByInteracao(Interacao interacao){
		if (interacao == null || interacao.getCdinteracao() == null) {
			throw new SinedException("Intera��o n�o pode ser nula.");
		}
		return query()
					.select("interacaoitem.cdinteracaoitem, interacaoitem.data, interacaoTipo.cdinteracaotipo, " +
							"interacaoitem.resumo, interacaoitem.de, " +
							"interacaoitem.para, interacaoitem.assunto, arquivo.nome, arquivo.cdarquivo")
					.leftOuterJoin("interacaoitem.arquivo arquivo")
					.join("interacaoitem.interacaoTipo interacaoTipo")
					.where("interacaoitem.interacao = ?",interacao)
					.orderBy("interacaoitem.data")
					.list();
	}
	
}
