package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Servico;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ServicoDAO extends GenericDAO<Servico> {
	
	/**
	 * Carrega um servi�o com todas as informa��es do servi�o.
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Servico carregaServico(Material material) {
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		return query()
		.select("servico.nomereduzido, servico.qtdeminima, servico.ativo, servico.observacao")
		.where("servico.cdmaterial = ?",material.getCdmaterial())
		.unique();
	}
	
	/**
	 * Deleta somente o registro da tabela Servi�o.
	 * 
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void deleteServico(Material material) {
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		String delete = "DELETE FROM SERVICO WHERE CDMATERIAL = ?";
		getJdbcTemplate().update(delete, new Object[]{material.getCdmaterial()});		
	}

	/**
	 * Insere um registro na tabela de Servi�o.
	 * 
	 * @param produto
	 * @author Rodrigo Freitas
	 */
	public void insertServico(Servico servico) {
		if (servico == null || servico.getCdmaterial() == null) {
			throw new SinedException("Servi�o n�o pode ser nulo.");
		}
		
		String insert = "INSERT INTO SERVICO (CDMATERIAL, NOMEREDUZIDO, QTDEMINIMA, OBSERVACAO, CDUSUARIOALTERA, DTALTERA)" +
				" VALUES (?,?,?,?,?,?)";
		getJdbcTemplate().update(insert, new Object[]{servico.getCdmaterial(),servico.getNomereduzido(),servico.getQtdeminima(),
													servico.getObservacao(), SinedUtil.getUsuarioLogado().getCdpessoa()
													,new Timestamp(System.currentTimeMillis())});
		
	}
	
	/**
	 * Faz update no servi�o.
	 * 
	 * @param produto
	 * @author Rodrigo Freitas
	 */
	public void updateServico(Servico servico){
		if (servico == null || servico.getCdmaterial() == null) {
			throw new SinedException("Servi�o n�o pode ser nulo.");
		}
		
		String update = "UPDATE SERVICO SET NOMEREDUZIDO = ?, QTDEMINIMA = ?, OBSERVACAO = ?, " +
						" CDUSUARIOALTERA = ?, DTALTERA = ? WHERE SERVICO.CDMATERIAL = ?";
		getJdbcTemplate().update(update, new Object[]{servico.getNomereduzido(),servico.getQtdeminima(),
				servico.getObservacao(), SinedUtil.getUsuarioLogado().getCdpessoa()
				,new Timestamp(System.currentTimeMillis()),servico.getCdmaterial()});
	}
	
	/**
	 * Retorna o n�mero de registros da tabela Servi�o do cdmaterial passado.
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Long countServico(Material material){
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Servico.class)
					.where("servico.cdmaterial = ?",material.getCdmaterial())
					.unique();
	}
	
	/**
	 * Retorna o n�mero de registros que tem na tabela servico com o 
	 * mesmo nome do servico cadastrado.
	 * 
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Long verificaServico(Material bean) {
		if (bean == null || bean.getServico_nomereduzido() == null) {
			throw new SinedException("Nome reduzido do servico n�o pode ser nulo.");
		}
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(Servico.class)
					.where("upper(servico.nomereduzido) = ?",bean.getServico_nomereduzido().toUpperCase())
					.where("servico.cdmaterial <> ?",bean.getCdmaterial())
					.unique();
	}
	
}
