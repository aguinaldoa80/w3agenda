package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.EntregadocumentoColeta;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EntregadocumentoColetaDAO extends GenericDAO<EntregadocumentoColeta> {

	@SuppressWarnings("unchecked")
	public String getWhereInNotaColeta(String whereInColeta) {
		if(StringUtils.isBlank(whereInColeta))
			return null;
		
		String sql = " select distinct ed.numero " +
					 " from entregadocumentocoleta edc " +
					 " join entregadocumento ed on ed.cdentregadocumento = edc.cdentregadocumento " +
					 " where edc.cdcoleta in (" + whereInColeta + ") "; 

		SinedUtil.markAsReader();
		List<String> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString("numero");
			}
		});
		
		return list != null ? CollectionsUtil.concatenate(list, ",") : null;
	}

	public List<EntregadocumentoColeta> findForDevolucaoByEntradafiscal(Entregadocumento entregadocumento) {
		return query()
				.select("entregadocumentoColeta.cdentregadocumentocoleta, coleta.cdcoleta, " +
						"listaColetaMaterial.cdcoletamaterial, listaColetaMaterial.quantidadedevolvida, " +
						"listaColetaMaterial.quantidade, listaColetaMaterial.quantidadedevolvidanota, " +
						"listaColetaMaterial.observacao, material.cdmaterial, " +
						"pneu.cdpneu, pneu.serie, pneu.dot, pneu.numeroreforma, " +
						"pneumarca.cdpneumarca, pneumarca.nome, " +
						"pneumedida.cdpneumedida, pneumedida.nome, " +
						"pneumodelo.cdpneumodelo, pneumodelo.nome, " +
						"pneumaterialbanda.cdmaterial, pneumaterialbanda.identificacao, pneumaterialbanda.nome, " +
						"pedidovendamaterial.cdpedidovendamaterial, pedidovenda.cdpedidovenda, " +
						"pedidovendamaterialServicos.cdpedidovendamaterial")
				.join("entregadocumentoColeta.coleta coleta")
				.join("coleta.listaColetaMaterial listaColetaMaterial")
				.join("listaColetaMaterial.pneu pneu")
				.join("listaColetaMaterial.material material")
				.leftOuterJoin("listaColetaMaterial.pedidovendamaterial pedidovendamaterial")
				.leftOuterJoin("listaColetaMaterial.listaColetaMaterialPedidovendamaterial listaColetaMaterialPedidovendamaterial")
				.leftOuterJoin("listaColetaMaterialPedidovendamaterial.pedidovendamaterial pedidovendamaterialServicos")
				.leftOuterJoin("pedidovendamaterial.pedidovenda pedidovenda")
				.leftOuterJoin("pneu.pneumarca pneumarca")
			    .leftOuterJoin("pneu.pneumodelo pneumodelo")
			    .leftOuterJoin("pneu.pneumedida pneumedida")
			    .leftOuterJoin("pneu.materialbanda pneumaterialbanda")
				.where("entregadocumentoColeta.entregadocumento = ?",entregadocumento)
				.list();
	}
}
