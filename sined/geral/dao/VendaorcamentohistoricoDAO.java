package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentohistorico;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VendaorcamentohistoricoDAO extends GenericDAO<Vendaorcamentohistorico>{

	/**
	 * Busca a lista de hist�rico da vendaorcamento.
	 * 
	 * @param vendaorcamento
	 * @return
	 * @since 16/03/2012
	 * @author Rodrigo Freitas
	 */
	public List<Vendaorcamentohistorico> findByVendaorcamento(Vendaorcamento vendaorcamento) {
		if(vendaorcamento == null || vendaorcamento.getCdvendaorcamento() == null){
			throw new SinedException("Vendaorcamento n�o pode ser nula.");
		}
		return query()
					.select("vendaorcamentohistorico.cdvendaorcamentohistorico, vendaorcamentohistorico.acao, " +
							"vendaorcamentohistorico.observacao, vendaorcamentohistorico.cdusuarioaltera, " +
							"vendaorcamentohistorico.dtaltera")
					.where("vendaorcamentohistorico.vendaorcamento = ?", vendaorcamento)
					.orderBy("vendaorcamentohistorico.dtaltera desc")
					.list();
	}
	
	
	/**
	 * Busca lista de hist�ricos de vendaorcamento a partir do cpf/cnpj de um cliente.
	 * 
	 * @param cpfcnpj, tipopessoa
	 * @since 21/05/2012
	 * @author Rafael Salvio
	 */
	public List<Vendaorcamentohistorico> findByCpfCnpjCliente(String cpfcnpj, Tipopessoa tipopessoa){
		if(cpfcnpj == null || cpfcnpj.equals("")){
			throw new SinedException("Erro no par�metro.");
		}
		
		QueryBuilder<Vendaorcamentohistorico> query = query();
		query
			.select("vendaorcamentohistorico.dtaltera, vendaorcamentohistorico.acao, vendaorcamentohistorico.observacao, usuarioaltera.nome")
			.join("vendaorcamentohistorico.vendaorcamento vendaorcamento")
			.join("vendaorcamento.cliente cliente")
			.join("vendaorcamentohistorico.usuarioaltera usuarioaltera");
		if(tipopessoa != null && tipopessoa == Tipopessoa.PESSOA_FISICA){
			Cpf cpf = new Cpf(cpfcnpj);
			query
				.where("cliente.cpf = ?",cpf);
		}
		else if(tipopessoa != null && tipopessoa == Tipopessoa.PESSOA_JURIDICA){
			Cnpj cnpj = new Cnpj(cpfcnpj);
			query
				.where("cliente.cnpj = ?",cnpj);
		}
		return query
					.orderBy("vendaorcamentohistorico.dtaltera")
					.list();
	}
}
