package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ExpedicaoitemDAO extends GenericDAO<Expedicaoitem> {

	/**
	 * Busca os itens da expedi��o de uma venda para o faturamento.
	 *
	 * @param venda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/02/2013
	 */
	public List<Expedicaoitem> findByVendaNotFaturada(Venda venda) {
		return query()
					.select("expedicaoitem.cdexpedicaoitem, expedicaoitem.dtexpedicaoitem, expedicaoitem.qtdeexpedicao, " +
							"vendamaterial.quantidade, material.cdmaterial, material.nome, material.identificacao")
					.join("expedicaoitem.expedicao expedicao")
					.join("expedicaoitem.material material")
					.join("expedicaoitem.vendamaterial vendamaterial")
					.join("vendamaterial.venda venda")
					.where("expedicao.expedicaosituacao = ?", Expedicaosituacao.CONFIRMADA)
					.where("venda = ?", venda)
					.openParentheses()
					.where("expedicaoitem.faturada is null")
					.or()
					.where("expedicaoitem.faturada = ?", Boolean.FALSE)
					.closeParentheses()
					.list();
	}

	/**
	 * Busca os itens de expedi��o para o faturamento.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/02/2013
	 */
	public List<Expedicaoitem> findForFaturamento(String whereIn) {
		return query()
					.select("expedicaoitem.cdexpedicaoitem, expedicaoitem.qtdeexpedicao, vendamaterial.cdvendamaterial, " +
							"expedicao.cdexpedicao")
					.join("expedicaoitem.vendamaterial vendamaterial")
					.join("expedicaoitem.expedicao expedicao")
					.whereIn("expedicaoitem.cdexpedicaoitem", whereIn)
					.list();
	}

	/**
	 * Atualiza o campo faturada da expedi��o
	 *
	 * @param expedicaoitem
	 * @param faturada
	 * @author Rodrigo Freitas
	 * @since 19/02/2013
	 */
	public void updateFaturada(Expedicaoitem expedicaoitem, Boolean faturada) {
		getHibernateTemplate().bulkUpdate("update Expedicaoitem ei set ei.faturada = ? where ei.id = ?", new Object[]{
				faturada, expedicaoitem.getCdexpedicaoitem()
		});
	}

	/**
	 * Verifica se existe algum item de expedi��o n�o faturada.
	 *
	 * @param expedicao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/02/2013
	 */
	public boolean haveNotFaturada(Expedicao expedicao) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Expedicaoitem.class)
					.where("expedicaoitem.expedicao = ?", expedicao)
					.openParentheses()
					.where("expedicaoitem.faturada is null")
					.or()
					.where("expedicaoitem.faturada = ?", Boolean.FALSE)
					.closeParentheses()
					.unique() > 0;
	}

	/**
	 * Verifica se existe algum item de expedi��o j� faturada para a venda.
	 *
	 * @param whereInVenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/02/2013
	 */
	public boolean haveFaturada(String whereInVenda) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Expedicaoitem.class)
					.join("expedicaoitem.vendamaterial vendamaterial")
					.join("vendamaterial.venda venda")
					.whereIn("venda.cdvenda", whereInVenda)
					.where("expedicaoitem.faturada = ?", Boolean.TRUE)
					.unique() > 0;
	}
	
	/**
	 * 
	 * @param listaExpedicaoitem
	 */
	public void confirmarAdicaoVenda(final List<Expedicaoitem> listaExpedicaoitem) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(final TransactionStatus status) {
				for (Expedicaoitem expedicaoitem : listaExpedicaoitem)
					saveOrUpdateNoUseTransaction(expedicaoitem);	
				
				return null;				
			}
		});	
	}
	
	public List<Expedicaoitem> carregaDadosEmitirExpedicao(Expedicao expedicao){
		return query()
					.select("expedicaoitem.cdexpedicaoitem, venda.cdvenda, venda.dtvenda, material.cdmaterial, material.nome, " +
							"municipio.nome, uf.sigla, endereco.logradouro, endereco.bairro, endereco.numero, endereco.complemento, " +
							"vendamaterial.quantidade, expedicaoitem.qtdeexpedicao, cliente.cdpessoa, " +
							"municipiocliente.nome, ufcliente.sigla, enderecocliente.logradouro, enderecocliente.bairro, enderecocliente.numero, " +
							"enderecocliente.complemento, listaTelefone.telefone, telefonetipo.cdtelefonetipo, contato.nome")
					.join("expedicaoitem.expedicao expedicao")
					.join("expedicaoitem.vendamaterial vendamaterial")
					.join("vendamaterial.venda venda")
					.join("vendamaterial.material material")
					.join("expedicaoitem.enderecocliente endereco")
					.join("endereco.municipio municipio")
					.join("municipio.uf uf")
					.join("expedicaoitem.cliente cliente")
					.join("cliente.listaEndereco enderecocliente")
					.join("enderecocliente.municipio municipiocliente")
					.join("municipiocliente.uf ufcliente")
					.leftOuterJoin("expedicaoitem.contatocliente contato")
					.leftOuterJoin("contato.listaTelefone listaTelefone")
					.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
					.where("expedicao=?", expedicao)
					.orderBy("expedicao.cdexpedicao desc")
					.list();
	}

	/**
	 * 
	 * @param expedicao
	 * @param listaExpedicaoitem 
	 * @return
	 */
	public List<Expedicaoitem> findByExpedicao(Expedicao expedicao, List<Expedicaoitem> listaExpedicaoitem) {

		QueryBuilder<Expedicaoitem> query = query().where("expedicaoitem.expedicao = ?", expedicao);

		String whereIn = "";
		if(SinedUtil.isListNotEmpty(listaExpedicaoitem)){
			whereIn = SinedUtil.listAndConcatenate(listaExpedicaoitem, "cdexpedicaoitem", ",");
		}
		if(whereIn != null && !"".equals(whereIn)){
			query.where("expedicaoitem.cdexpedicaoitem not in ("+whereIn+")");
		}

		return query.list();
	}
	
	/**
	 * 
	 * @param whereInNota //String de id notafiscalproduto
	 * @return List<Expedicaoitem>
	 * 
	 */
	public List<Expedicaoitem> findByWhereInNota(String whereInNota,boolean isBuscaCanceladas){
		Expedicaosituacao cancelada = null;
		if(!isBuscaCanceladas){
			cancelada = Expedicaosituacao.CANCELADA;
		}
		return query()
				.select("expedicaoitem.cdexpedicaoitem," +
						"expedicao.cdexpedicao," +
						"coletaMaterial.cdcoletamaterial," +
						"coleta.cdcoleta," +
						"notaFiscalProdutoItem.cdnotafiscalprodutoitem," +
						"notafiscalproduto.numero,notafiscalproduto.cdNota," +
						"cliente.nome,cliente.cdpessoa")
				.join("expedicaoitem.expedicao expedicao")
				.leftOuterJoin("expedicaoitem.coletaMaterial coletaMaterial")
				.leftOuterJoin("coletaMaterial.coleta coleta")
				.leftOuterJoin("expedicaoitem.notaFiscalProdutoItem notaFiscalProdutoItem")
				.leftOuterJoin("notaFiscalProdutoItem.notafiscalproduto notafiscalproduto")
				.leftOuterJoin("notafiscalproduto.cliente cliente")
				.where("expedicaoitem.notaFiscalProdutoItem is not null")
				.where("expedicao.expedicaosituacao <> ?", cancelada)
				.whereIn("notafiscalproduto.cdNota", whereInNota)
				.list();
	}
	
	public List<Expedicaoitem> findByCdExpedicao(Integer cdExpedicao){
		return query()
				.select("expedicaoitem.cdexpedicaoitem, expedicaoitem.dtexpedicaoitem, " +
						"expedicao.cdexpedicao," +
						"coletaMaterial.cdcoletamaterial," +
						"coleta.cdcoleta," +
						"notaFiscalProdutoItem.cdnotafiscalprodutoitem," +
						"notafiscalproduto.numero,notafiscalproduto.cdNota," +
						"cliente.nome,cliente.cdpessoa, " +
						"localarmazenagem.cdlocalarmazenagem, " +
						"empresa.cdpessoa ")
				.join("expedicaoitem.expedicao expedicao")
				.leftOuterJoin("expedicaoitem.coletaMaterial coletaMaterial")
				.leftOuterJoin("expedicaoitem.localarmazenagem localarmazenagem")
				.leftOuterJoin("coletaMaterial.coleta coleta")
				.leftOuterJoin("expedicaoitem.notaFiscalProdutoItem notaFiscalProdutoItem")
				.leftOuterJoin("notaFiscalProdutoItem.notafiscalproduto notafiscalproduto")
				.leftOuterJoin("notafiscalproduto.cliente cliente")
				.leftOuterJoin("expedicao.empresa empresa")
				.where("expedicao.cdexpedicao =", cdExpedicao)
				.list();
	}
	
	public List<Expedicaoitem> findByWhereInExpedicao(String whereInExpedicao){
		return query()
				.select("expedicaoitem.cdexpedicaoitem, expedicaoitem.dtexpedicaoitem, " +
						"expedicao.cdexpedicao," +
						"coletaMaterial.cdcoletamaterial," +
						"coleta.cdcoleta," +
						"notaFiscalProdutoItem.cdnotafiscalprodutoitem," +
						"notafiscalproduto.numero,notafiscalproduto.cdNota," +
						"cliente.nome,cliente.cdpessoa, " +
						"localarmazenagem.cdlocalarmazenagem, " +
						"empresa.cdpessoa ")
				.join("expedicaoitem.expedicao expedicao")
				.leftOuterJoin("expedicaoitem.coletaMaterial coletaMaterial")
				.leftOuterJoin("expedicaoitem.localarmazenagem localarmazenagem")
				.leftOuterJoin("coletaMaterial.coleta coleta")
				.leftOuterJoin("expedicaoitem.notaFiscalProdutoItem notaFiscalProdutoItem")
				.leftOuterJoin("notaFiscalProdutoItem.notafiscalproduto notafiscalproduto")
				.leftOuterJoin("notafiscalproduto.cliente cliente")
				.leftOuterJoin("expedicao.empresa empresa")
				.whereIn("expedicao.cdexpedicao", whereInExpedicao)
				.list();
	}
	
	/**
	* M�todo que carrega os itens da expedi��o para confer�ncia de materiais
	*
	* @param whereIn
	* @return
	* @since 17/10/2016
	* @author Luiz Fernando
	*/
	public List<Expedicaoitem> findForConferenciaMateriaisFromExpedicao(String whereIn) {
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("expedicaoitem.cdexpedicaoitem, expedicaoitem.qtdeexpedicao, " +
					"expedicao.cdexpedicao, expedicao.expedicaosituacao, " +
					"material.cdmaterial, material.nome, material.codigobarras, " +
					"material.desconsiderarcodigobarras, " +
					"vendamaterial.cdvendamaterial, vendamaterial.quantidade, " +
					"venda.cdvenda, unidademedida.cdunidademedida, unidademedida.nome, " +
					"listaEmbalagem.cdembalagem, listaEmbalagem.codigoBarras, " +
					"unidadeMedidaEmbalagem.cdunidademedida, unidadeMedidaEmbalagem.nome, " +
					"material.desconsiderarcodigobarras, loteEstoque.cdloteestoque, loteEstoque.numero")
			.leftOuterJoin("expedicaoitem.vendamaterial vendamaterial")
			.leftOuterJoin("vendamaterial.venda venda")
			.leftOuterJoin("expedicaoitem.material material")
			.leftOuterJoin("expedicaoitem.unidademedida unidademedida")
			.leftOuterJoin("expedicaoitem.loteEstoque loteEstoque")
			.leftOuterJoin("expedicaoitem.expedicao expedicao")
			.leftOuterJoin("material.listaEmbalagem listaEmbalagem")
			.leftOuterJoin("listaEmbalagem.unidadeMedida unidadeMedidaEmbalagem")
			.whereIn("expedicaoitem.expedicao.cdexpedicao", whereIn)
			.orderBy("venda.cdvenda")
			.list();
	}
	
	public List<Expedicaoitem> findForConferenciaPneuFromExpedicao(String whereIn) {
		QueryBuilder<Expedicaoitem> query = query()
				.select("expedicaoitem.cdexpedicaoitem," +
						"expedicao.cdexpedicao,expedicao.expedicaosituacao," +
						"cliente.cdpessoa,cliente.nome," +
						"notaFiscalProdutoItem.cdnotafiscalprodutoitem," +
						"notafiscalproduto.cdNota,notafiscalproduto.numero")
				.join("expedicaoitem.expedicao expedicao")
				.leftOuterJoin("expedicaoitem.cliente cliente")
				.leftOuterJoin("expedicaoitem.pneu pneu")
				.leftOuterJoin("expedicaoitem.notaFiscalProdutoItem notaFiscalProdutoItem")
				.leftOuterJoin("notaFiscalProdutoItem.notafiscalproduto notafiscalproduto")
				.whereIn("expedicaoitem.expedicao.cdexpedicao", whereIn)
				.orderBy("notafiscalproduto.cdNota");
		query = SinedUtil.setJoinsByComponentePneu(query);
		return query.list();
	}
	
	public List<Expedicaoitem> findForFinalizaExpedicao(String whereIn) {
		QueryBuilder<Expedicaoitem> query = query()
				.select("expedicaoitem.cdexpedicaoitem," +
						"expedicao.cdexpedicao,expedicao.expedicaosituacao," +
						"cliente.cdpessoa,cliente.nome," +
						"material.cdmaterial, material.nome, " +
						"vendamaterial.cdvendamaterial, venda.cdvenda")
				.join("expedicaoitem.expedicao expedicao")
				.join("expedicaoitem.vendamaterial vendamaterial")
				.join("vendamaterial.venda venda")
				.leftOuterJoin("expedicaoitem.cliente cliente")
				.leftOuterJoin("expedicaoitem.pneu pneu")
				.leftOuterJoin("expedicaoitem.material material")
				.whereIn("expedicaoitem.expedicao.cdexpedicao", whereIn)
				.orderBy("venda.cdvenda");
		query = SinedUtil.setJoinsByComponentePneu(query);
		return query.list();
	}
	
	public boolean existeVendasDistintas(String whereIn) {
		if(StringUtils.isEmpty(whereIn)) return false;
		
		String sql = "select distinct vm.cdvenda " +
					 "from expedicaoitem ei " +
					 "join vendamaterial vm on vm.cdvendamaterial = ei.cdvendamaterial " +
				 	 "where ei.cdexpedicao in (" + whereIn + ")";		
		
		@SuppressWarnings("unchecked")
		List<Integer> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getInt("cdvenda");
			}
		});
		
		return list != null && list.size() > 1;
	}
	
	public List<Expedicaoitem> findByColetaMaterial(Integer cdColetaMaterail, Material material, Integer cdExpeticaoItem, String whereInExcludeExpedicao) {
		return query()
				.select("expedicaoitem.cdexpedicaoitem,expedicaoitem.qtdeexpedicao," +
						"expedicao.cdexpedicao,expedicao.expedicaosituacao," +
						"coletaMaterial.quantidade,coletaMaterial.cdcoletamaterial," +
						"coleta.cdcoleta")
				.join("expedicaoitem.expedicao expedicao")
				.leftOuterJoin("expedicaoitem.coletaMaterial coletaMaterial")
				.leftOuterJoin("coletaMaterial.coleta coleta")
				.where("expedicaoitem.cdexpedicaoitem <> ?", cdExpeticaoItem)
				.where("expedicao.cdexpedicao not in (" + whereInExcludeExpedicao + ")", StringUtils.isNotBlank(whereInExcludeExpedicao))
				.where("expedicao.expedicaosituacao <> ?", Expedicaosituacao.CANCELADA)
				.where("listaColetaMaterial.material = ?", material)
				.where("coletaMaterial.cdcoletamaterial =?", cdColetaMaterail)
				.list();
	}

	public List<Expedicaoitem> findByColetaMaterial(Integer cdColeta, Integer cdExpedicao) {
		return query().select("expedicaoitem.cdexpedicaoitem,expedicaoitem.qtdeexpedicao," +
				"expedicao.cdexpedicao,expedicao.expedicaosituacao," +
				"coletaMaterial.quantidade,coletaMaterial.cdcoletamaterial," +
				"coleta.cdcoleta")
				.join("expedicaoitem.expedicao expedicao")
				.leftOuterJoin("expedicaoitem.coletaMaterial coletaMaterial")
				.leftOuterJoin("coletaMaterial.coleta coleta")
				.where("expedicao.cdexpedicao <> ?",cdExpedicao)
				.where("expedicao.expedicaosituacao <> ?", Expedicaosituacao.CANCELADA)
				.where("coleta.cdcoleta =?", cdColeta)
				.list();
	}
}
