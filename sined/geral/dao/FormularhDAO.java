package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Formularh;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.FormularhFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("formularh.nome")
public class FormularhDAO extends GenericDAO<Formularh> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Formularh> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inválidos");
		}
		FormularhFiltro filtro = (FormularhFiltro) _filtro;
		
		if(filtro != null && (filtro.getDescricao() != null || filtro.getAtivo() != null)){
			query				
				.where("formularh.nome = ?",filtro.getDescricao())
				.where("formularh.ativa = ?",filtro.getAtivo());				
		}
		
		if (filtro != null && filtro.getCargo() != null){
			query
				.join("formularh.formularhcargo formularhcargo")				
				.join("formularhcargo.cargo cargo")
				.where("cargo = ?",filtro.getCargo());
		}
	}	
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Formularh> query) {
		query
			.leftOuterJoinFetch("formularh.formularhitem formularhitem")
//			.leftOuterJoinFetch("formularh.formularhcargo formularhcargo")
//			.leftOuterJoinFetch("formularhcargo.cargo cargo")
			.orderBy("formularhitem.ordem")
			.setUseTranslator(true);		
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save){
		save			
			.saveOrUpdateManaged("formularhitem")
			.saveOrUpdateManaged("formularhcargo");		
	} 
	
	public Formularh findByCargo(Cargo cargo){
		 QueryBuilder<Formularh> query = query()
		 	.joinFetch("formularh.formularhcargo formularhcargo")
		 	.joinFetch("formularh.formularhitem formularhitem")
		 	.joinFetch("formularhcargo.cargo cargo")
		 	.where("formularhcargo.cargo = ?", cargo)
		 	.where("formularh.ativa = true")
		 	.orderBy("formularhitem.ordem");
		 return query.unique();
	}
}