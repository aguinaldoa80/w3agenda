package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Arquivobancario;
import br.com.linkcom.sined.geral.bean.Arquivobancariodocumento;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;


public class ArquivobancariodocumentoDAO extends GenericDAO<Arquivobancariodocumento> {

	/**
	 * 
	 * M�todo que busca a lista de ArquivobancarioDocumento � partir de um ArquivoBancario.
	 *
	 * @name findListaForArquivoBancario
	 * @param arquivobancario
	 * @return
	 * @return List<Arquivobancariodocumento>
	 * @author Thiago Augusto
	 * @date 07/05/2012
	 *
	 */
	public List<Arquivobancariodocumento> findListaForArquivoBancario(Arquivobancario arquivobancario){
		if (arquivobancario == null){
			throw new SinedException("O arquivo banc�rio n�o pode ser nulo!");
		}
		return query()
					.select("arquivobancariodocumento.cdarquivobancariodocumento, pessoa.cdpessoa, pessoa.nome")
					.leftOuterJoin("arquivobancariodocumento.documento documento")
					.leftOuterJoin("documento.pessoa pessoa")
					.leftOuterJoin("arquivobancariodocumento.arquivobancario arquivobancario")
					.where("arquivobancario = ? ", arquivobancario)
					.list();
	}
	
	/**
	 * 
	 * M�todo que busca documentos que ainda n�o foram baixados
	 *
	 * @name findListaForArquivoBancario
	 * @param arquivobancario
	 * @return
	 * @return List<Arquivobancariodocumento>
	 * @author Marden Silva
	 * @date 30/07/2103
	 *
	 */
	public List<Arquivobancariodocumento> findDocumentosNaoBaixado(Arquivobancario arquivobancario, String whereIn){
		if (arquivobancario == null){
			throw new SinedException("O arquivo banc�rio n�o pode ser nulo!");
		}
		return query()
					.select("arquivobancariodocumento.cdarquivobancariodocumento, documento.cddocumento, documentoacao.cddocumentoacao")
					.join("arquivobancariodocumento.documento documento")
					.join("documento.documentoacao documentoacao")
					.join("arquivobancariodocumento.arquivobancario arquivobancario")
					.where("arquivobancario = ? ", arquivobancario)
					.where("documentoacao <> ?", Documentoacao.BAIXADA)
					.where("documento.cddocumento not in (" + whereIn + ")", (whereIn != null && !"".equals(whereIn)))
					.list();
	}
	
	public Boolean existeProcessamentoArquivoRetorno(Documento documento){
		if (documento == null || documento.getCddocumento() == null){
			throw new SinedException("Documento n�o pode ser nulo!");
		}
		return newQueryBuilderSined(Long.class)
					
					.select("count(*)")
					.from(Arquivobancariodocumento.class)
					.join("arquivobancariodocumento.documento documento")
					.where("documento.documentoacao = ?", Documentoacao.BAIXADA)
					.where("documento = ?", documento)
					.unique() > 0;
	}
	
}
