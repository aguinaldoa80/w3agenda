package br.com.linkcom.sined.geral.dao;

import java.util.HashMap;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Tela;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocoluna;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.TelaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TelaDAO extends GenericDAO<Tela> {
	protected HashMap<String, HashMap<String, String>> mapTelas = new HashMap<String, HashMap<String,String>>();
	protected HashMap<String, Boolean> mapMenuTituloTela;
	
	@Override
	public void updateListagemQuery(QueryBuilder<Tela> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		TelaFiltro filtro = (TelaFiltro)_filtro;
		query.whereLikeIgnoreAll("descricao", filtro.getDescricao());		 
		
		if(filtro.getModulo() != null && !filtro.getModulo().equals("")){
			query.where("path like '/" + filtro.getModulo() + "/%'");
		}
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Tela> query) {
//		query.leftOuterJoinFetch("funcao.listaPermissao listaPermissao");
//		query			
//			.leftOuterJoinFetch("tela.listaCampolistagem listaCampolistagem").orderBy("listaCampolistagem.ordemexibicao");
		query
			.select("tela.cdtela, tela.path, tela.descricao, " +
					"listaCampolistagem.cdcampolistagem, listaCampolistagem.ordemjsp, listaCampolistagem.nome, " +
					"listaCampolistagem.exibir, listaCampolistagem.ordemexibicao, listaCampolistagem.tipocoluna, " +
					"listaCampoentrada.cdcampoentrada, listaCampoentrada.campo, listaCampoentrada.exibir ")
			.leftOuterJoin("tela.listaCampolistagem listaCampolistagem ")
			.leftOuterJoin("tela.listaCampoentrada listaCampoentrada ")			
			.orderBy("listaCampolistagem.ordemexibicao, listaCampoentrada.exibir desc");
	}	

	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		//save.saveOrUpdateManaged("listaPermissao");
		save
			.saveOrUpdateManaged("listaCampolistagem")
			.saveOrUpdateManaged("listaCampoentrada");
	}
	
	/**
	 * Captura a descri��o cadastrada no banco a partir de uma url
	 * A url deve estar no formado /modulo/tela ( a mesma cadastrada no @controller na propriedade path)
	 * @param url
	 * @return Descri��o da tela cadastrada no banco
	 * @author Pedro Gon�alves
	 */
	public String getTelaDescriptionByUrl(String url) {
		String cliente = SinedUtil.getSubdominioCliente();
		if(mapTelas == null)
			mapTelas = new HashMap<String, HashMap<String,String>>();		
		if(mapTelas.get(cliente) == null){
			List<Tela> telas = findAll();
			HashMap<String, String> mapa = new HashMap<String, String>();
			for (Tela tela : telas) {
				mapa.put(tela.getPath(), tela.getDescricao());
			}
			mapTelas.put(cliente, mapa);
		}
		return mapTelas.get(cliente).get(url);
	}
	
	/**
	 * M�todo que busca a descri��o da tela
	 *
	 * @param url
	 * @return
	 * @author Luiz Fernando
	 */
	public String getTituloMenuByUrl(String url) {
		String cliente = SinedUtil.getSubdominioCliente();
		if(mapTelas == null)
			mapTelas = new HashMap<String, HashMap<String,String>>();
		if(mapTelas.get(cliente) == null){
			List<Tela> telas = findAll();
			HashMap<String, String> mapa = new HashMap<String, String>();
			for (Tela tela : telas) {
				mapa.put(tela.getPath(), tela.getDescricao());
			}
			mapTelas.put(cliente, mapa);
			if(mapMenuTituloTela == null){
				mapMenuTituloTela = new HashMap<String, Boolean>();
				List<Tela> telasTituloMenu = findForTituloMenu();
				HashMap<String, Boolean> mapaMenu = new HashMap<String, Boolean>();
				for (Tela tela : telasTituloMenu) {
					mapaMenu.put(tela.getPath(), Boolean.TRUE);
				}
				mapMenuTituloTela.putAll(mapaMenu);
			}
		}else if(mapMenuTituloTela == null){
			mapMenuTituloTela = new HashMap<String, Boolean>();
			List<Tela> telasTituloMenu = findForTituloMenu();
			HashMap<String, Boolean> mapaMenu = new HashMap<String, Boolean>();
			for (Tela tela : telasTituloMenu) {
				mapaMenu.put(tela.getPath(), Boolean.TRUE);
			}
			mapMenuTituloTela.putAll(mapaMenu);
							
		}
		
		if(mapMenuTituloTela.get(url) != null && mapMenuTituloTela.get(url))
			return mapTelas.get(cliente).get(url);
		
		return null;
	}
	
	/**
	 * Carrega uma lista de tela.
	 * 
	 * @param whereIn
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Tela> loadAll(String whereIn){
		if(whereIn == null || "".equals(whereIn)){
			throw new SinedException("Par�metro whereIn inv�lido.");
		}
		return query()
		.select("tela.cdtela,tela.path")
		.whereIn("tela.cdtela", whereIn)
		.list();
	}
	
	/**
	 * Limpa o cache de telas
	 * @author Pedro Gon�alves
	 */
	public void clearTelaCache(){
		mapTelas = null;
	}
	
	/**
	 * Retorna uma lista com os atalhos dispon�veis para a pessoa
	 * passada como par�metro.
	 * 
	 * @param pesssoa
	 * @return
	 * @since 30/03/2011
	 */	
	public List<Tela> findAtalhoByPessoa(Pessoa pessoa) {
		return querySined()
			
			.select("tela.cdtela, tela.descricao, tela.path")
			.join("tela.listaPessoaatalho pessoaatalho")
			.join("pessoaatalho.pessoa pessoa")
			.where("pessoa = ?", pessoa)
			.orderBy("tela.descricao")
			.list();		
	}
	
	/**
	 * Retorna uma lista de telas cujo path seja igual ao par�metro passado.
	 * 
	 * @param path
	 * @return
	 * @since 30/03/2011
	 */	
	public List<Tela> findByPath(String path) {
		return querySined()
			
			.where("path = ?", path)
			.orderBy("tela.cdtela")
			.list();		
	}
		
	/**
	* M�todo para buscar os campos da listagem de acordo com o par�metro
	*
	* @param path
	* @param tipocoluna
	* @return
	* @since Oct 28, 2011
	* @author Luiz Fernando F Silva
	*/
	public Tela findForCampolistagemByPath(String path, Tipocoluna tipocoluna) {
		return query()
			.select("tela.cdtela, tela.path, tela.descricao, listaCampolistagem.cdcampolistagem, listaCampolistagem.ordemjsp, " +
					"listaCampolistagem.nome, listaCampolistagem.exibir, listaCampolistagem.ordemexibicao")
			.leftOuterJoin("tela.listaCampolistagem listaCampolistagem")
			.where("path = ?", path)
			.where("listaCampolistagem.tipocoluna = ?", tipocoluna)
			.orderBy("tela.cdtela, listaCampolistagem.ordemexibicao")
			.unique();		
	}
	
	/**
	* M�todo para buscar os campos da listagem da tela
	*
	* @param path
	* @return
	* @since Oct 28, 2011
	* @author Luiz Fernando F Silva
	*/
	public Tela findForCampolistagemByPath(String path) {
		return query()
			.select("tela.cdtela, tela.path, tela.descricao, listaCampolistagem.cdcampolistagem, listaCampolistagem.ordemjsp, " +
					"listaCampolistagem.nome, listaCampolistagem.exibir, listaCampolistagem.ordemexibicao")
			.leftOuterJoin("tela.listaCampolistagem listaCampolistagem")
			.where("path = ?", path)			
			.orderBy("listaCampolistagem.ordemexibicao")
			.unique();		
	}

	/* singleton */
	private static TelaDAO instance;
	public static TelaDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(TelaDAO.class);
		}
		return instance;
	}

	/**
	* M�todo que busca as colunas fixas da tela na listagem
	*
	* @param path
	* @return
	* @since Oct 28, 2011
	* @author Luiz Fernando F Silva
	*/
	public Tela findColunaFixa(String path) {
		return query()
				.select("tela.cdtela, listaCampolistagem.cdcampolistagem, listaCampolistagem.ordemjsp, " +
						"listaCampolistagem.ordemexibicao, listaCampolistagem.tipocoluna")
				.leftOuterJoin("tela.listaCampolistagem listaCampolistagem")
				.where("path = ?", path)
				.where("listaCampolistagem.tipocoluna = ?", Tipocoluna.COLUNA_FIXA)
				.orderBy("listaCampolistagem.ordemexibicao")
				.unique();
	}
	
	/**
	* M�todo para buscar a tela
	*
	* @param path
	* @return
	* @since Oct 28, 2011
	* @author Luiz Fernando F Silva
	*/
	public Tela findTelaByPath(String path){
		return query()
			.select("tela.cdtela, tela.path")
			.where("path = ?", path)
			.unique();
	}
	
	/**
	 * M�todo para buscar os campos da entrada de acordo com o par�metro ( url da tela )
	 *
	 * @param path
	 * @return
	 * @author Luiz Fernando
	 */
	public Tela findForCampoentradaByPath(String path){
		return query()
				.select("tela.cdtela, tela.path, listaCampoentrada.cdcampoentrada, listaCampoentrada.campo, listaCampoentrada.exibir")
				.leftOuterJoin("tela.listaCampoentrada listaCampoentrada ")
				.where("path = ?", path)
				.openParentheses()
				.where("listaCampoentrada.exibir is null").or()
				.where("listaCampoentrada.exibir = false")
				.closeParentheses()
				.unique();
	}
	
	/**
	 * M�todo que verifica se existe campos da entrada cadastrado para a tela (url da tela)
	 *
	 * @param path
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isCampoentrada(String path){
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.from(Tela.class)
				.join("tela.listaCampoentrada listaCampoentrada")
				.where("path = ?", path)
				.unique() > 0;
				
	}
	
	/**
	 * M�todo que busca as telas que utilizam a descri��o no menu
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Tela> findForTituloMenu(){
		return query()
				.select("tela.cdtela, tela.path, tela.descricaomenu")
				.where("descricaomenu = true")
				.list();
	}
}