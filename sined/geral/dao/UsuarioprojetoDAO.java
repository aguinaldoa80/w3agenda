package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Usuarioprojeto;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class UsuarioprojetoDAO extends GenericDAO<Usuarioprojeto> {
	
	public List<Usuarioprojeto> findForAutocompleteByUsuariologado(String q) {
		return query().select(
				"usuarioprojeto.cdusuarioprojeto, projeto.cdprojeto, projeto.nome")
				.leftOuterJoin("usuarioprojeto.projeto projeto")
				.leftOuterJoin("usuarioprojeto.usuario usuario")
				.where("usuario.cdpessoa = ?", SinedUtil.getUsuarioLogado().getCdpessoa())
				.whereLikeIgnoreAll("projeto.nome", q)
				.orderBy("projeto.nome")
				.list();
	}
}