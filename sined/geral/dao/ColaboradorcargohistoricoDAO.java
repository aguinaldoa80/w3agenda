package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Colaboradorcargohistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColaboradorcargohistoricoDAO extends GenericDAO<Colaboradorcargohistorico>{

	public List<Colaboradorcargohistorico> findByColaboradorcargo(Colaboradorcargo colaboradorcargo) {
		return 
			query()
				.select("colaboradorcargohistorico.cdcolaboradorcargohistorico, colaboradorcargo.cdcolaboradorcargo, " +
						"colaboradorcargohistorico.dtaltera, colaboradorcargohistorico.cdusuarioaltera, colaboradorcargohistorico.acao")
				.leftOuterJoin("colaboradorcargohistorico.colaboradorcargo colaboradorcargo")
				.where("colaboradorcargo=?", colaboradorcargo)
				.list();
		
	}
	

}
