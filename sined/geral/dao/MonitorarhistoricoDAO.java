package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Radiushistorico;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.MonitorarhistoricoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

public class MonitorarhistoricoDAO extends GenericDAO<Radiushistorico>{

	@Override
	public void updateListagemQuery(QueryBuilder<Radiushistorico> query, FiltroListagem _filtro) {
		if (_filtro ==  null)
			throw new SinedException("Dados inv�lidos");
		
		MonitorarhistoricoFiltro filtro = (MonitorarhistoricoFiltro) _filtro;
		
		query
			.where("radiushistorico.nasip = ?", filtro.getNasip())
			.where("radiushistorico.framedipaddress = ?", filtro.getFramedipaddress())
			.where("radiushistorico.dtinicio >= ?", SinedDateUtils.dateToBeginOfDay(filtro.getDtinicio()))
			.where("radiushistorico.dtfim <= ?", SinedDateUtils.dataToEndOfDay(filtro.getDtfim()))
			.whereLikeIgnoreAll("radiushistorico.estacaoservidor", filtro.getEstacaoservidor())
			.whereLikeIgnoreAll("radiushistorico.estacaocliente", filtro.getEstacaocliente())
			.where("radiushistorico.usuario in(select cmr.usuario from Contratomaterialrede cmr where " +
					"UPPER(retira_acento(cmr.contratomaterial.contrato.cliente.nome)) " +
					"LIKE '%'||?||'%')", filtro.getCliente() != null ?  Util.strings.tiraAcento(filtro.getCliente().toUpperCase()) : null);
		
		if(filtro.getUsuario() != null){
			if(filtro.getPesquisacompleta())
				query.where("upper(retira_acento(radiushistorico.usuario)) like ?||'%'", Util.strings.tiraAcento(filtro.getUsuario().toUpperCase()));
			else
				query.whereLikeIgnoreAll("radiushistorico.usuario", filtro.getUsuario());
		}
	}

	/**
	 * Retorna o total de uploads para exibi��o na tela de listagem.
	 * @param filtro
	 * @author Taidson
	 * @since 09/07/2010
	 */
	@SuppressWarnings("unchecked")
	public Long somaUploads(MonitorarhistoricoFiltro filtro) {
		if (filtro ==  null) 
			throw new SinedException("Dados inv�lidos");
		
		QueryBuilderSined<Radiushistorico> listagemQuery = (QueryBuilderSined<Radiushistorico>)query();
		updateListagemQuery(listagemQuery, filtro);
		
		QueryBuilder<Long> query = newQueryBuilder(Long.class);
		query.select("sum(radiushistorico.dadoentrada)");
		query.setUseTranslator(false);

		QueryBuilder.From from = listagemQuery.getFrom();
		query.from(from);

		for (QueryBuilder.Join join : listagemQuery.getJoins()) {
		//quando estiver contando nao precisa de fazer fetch do join
		query.join(join.getJoinMode(), false, join.getPath());
		}
		QueryBuilder.Where where = listagemQuery.getWhere();
		query.where(where); 
		
		Long unique = query.unique();
		if(unique != null)
			return unique;
		else
			return 0L;
	}
	
	/**
	 * Retorna o total de downloads para exibi��o na tela de listagem.
	 * @param filtro
	 * @author Taidson
	 * @since 09/07/2010
	 */
	@SuppressWarnings("unchecked")
	public Long somaDownloads(MonitorarhistoricoFiltro filtro) {
		
		if (filtro ==  null) 
			throw new SinedException("Dados inv�lidos");
		
		QueryBuilderSined<Radiushistorico> listagemQuery = (QueryBuilderSined<Radiushistorico>)query();
		updateListagemQuery(listagemQuery, filtro);
		
		QueryBuilder<Long> query = newQueryBuilder(Long.class);
		query.select("sum(radiushistorico.dadosaida)");
		query.setUseTranslator(false);

		QueryBuilder.From from = listagemQuery.getFrom();
		query.from(from);

		for (QueryBuilder.Join join : listagemQuery.getJoins()) {
		//quando estiver contando nao precisa de fazer fetch do join
		query.join(join.getJoinMode(), false, join.getPath());
		}
		QueryBuilder.Where where = listagemQuery.getWhere();
		query.where(where); 
	
		Long unique = query.unique();
		if(unique != null)
			return unique;
		else
			return 0L;
	}
	
	
	/**
	 * Retorna o total de tempo sess�o para exibi��o na tela de listagem.
	 * @param filtro
	 * @author Taidson
	 * @since 09/07/2010
	 */
	@SuppressWarnings("unchecked")
	public Long somaTemposessao(MonitorarhistoricoFiltro filtro) {
		
		QueryBuilderSined<Radiushistorico> listagemQuery = (QueryBuilderSined<Radiushistorico>)query();
		updateListagemQuery(listagemQuery, filtro);
		
		QueryBuilder<Long> query = newQueryBuilder(Long.class);
		query.select("sum(radiushistorico.temposessao)");
		query.setUseTranslator(false);

		QueryBuilder.From from = listagemQuery.getFrom();
		query.from(from);

		for (QueryBuilder.Join join : listagemQuery.getJoins()) {
		//quando estiver contando nao precisa de fazer fetch do join
		query.join(join.getJoinMode(), false, join.getPath());
		}
		QueryBuilder.Where where = listagemQuery.getWhere();
		query.where(where); 
	
		Long unique = query.unique();
		if(unique != null)
			return unique;
		else
			return 0L;
	}
	
	/**
	 * Carrega uma lista de hist�ricos para preencher relat�rio.
	 * @param filtro
	 * @return
	 * @author Taidson
	 * @since 12/07/2010
	 */
	public List<Radiushistorico> gerarMonitoramentoHistorico(MonitorarhistoricoFiltro filtro) {
		if (filtro ==  null)
			throw new SinedException("Dados inv�lidos");
		
		return query()
			.whereLikeIgnoreAll("radiushistorico.usuario", filtro.getUsuario())
			.where("radiushistorico.nasip = ?", filtro.getNasip())
			.where("radiushistorico.framedipaddress = ?", filtro.getFramedipaddress())
			.where("radiushistorico.dtinicio >= ?", SinedDateUtils.dateToBeginOfDay(filtro.getDtinicio()))
			.where("radiushistorico.dtfim <= ?", SinedDateUtils.dataToEndOfDay(filtro.getDtfim()))
			.whereLikeIgnoreAll("radiushistorico.estacaoservidor", filtro.getEstacaoservidor())
			.whereLikeIgnoreAll("radiushistorico.estacaocliente", filtro.getEstacaocliente())
			.where("radiushistorico.usuario in(select cmr.usuario from Contratomaterialrede cmr where " +
					"UPPER(retira_acento(cmr.contratomaterial.contrato.cliente.nome)) " +
					"LIKE '%'||?||'%')", filtro.getCliente() != null ?  Util.strings.tiraAcento(filtro.getCliente().toUpperCase()) : null)
			.list();
	}


}
