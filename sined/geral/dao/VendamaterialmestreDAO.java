package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterialmestre;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VendamaterialmestreDAO extends GenericDAO<Vendamaterialmestre>{

	/**
	 * 
	 * @param venda
	 * @return
	 */
	public List<Vendamaterialmestre> findByVenda(Venda venda) {
		return query()
			.select("vendamaterialmestre.cdvendamaterialmestre, vendamaterialmestre.altura, vendamaterialmestre.qtde, " +
					 "vendamaterialmestre.nomealternativo, vendamaterialmestre.exibiritenskitflexivel, " +
					 "vendamaterialmestre.largura, vendamaterialmestre.comprimento, venda.cdvenda, material.cdmaterial, " +
					 "vendamaterialmestre.preco, vendamaterialmestre.dtprazoentrega, vendamaterialmestre.desconto, " +
					 "vendamaterialmestre.valoripi, vendamaterialmestre.ipi, vendamaterialmestre.tipocobrancaipi, " +
					 "vendamaterialmestre.tipoCalculoIpi, vendamaterialmestre.aliquotaReaisIpi, " +
					 "vendamaterialmestre.tipocobrancaicms, vendamaterialmestre.valorbcicms, vendamaterialmestre.icms, " +
					 "vendamaterialmestre.valoricms, vendamaterialmestre.valorbcicmsst, vendamaterialmestre.icmsst, " +
					 "vendamaterialmestre.valoricmsst, vendamaterialmestre.reducaobcicmsst, vendamaterialmestre.margemvaloradicionalicmsst, " +
					 "vendamaterialmestre.valorbcfcp, vendamaterialmestre.fcp, vendamaterialmestre.valorfcp, " +
					 "vendamaterialmestre.valorbcfcpst, vendamaterialmestre.fcpst, vendamaterialmestre.valorfcpst, " +
					 "vendamaterialmestre.valorbcdestinatario, vendamaterialmestre.valorbcfcpdestinatario, vendamaterialmestre.fcpdestinatario, " +
					 "vendamaterialmestre.icmsdestinatario, vendamaterialmestre.icmsinterestadual, vendamaterialmestre.icmsinterestadualpartilha, " +
					 "vendamaterialmestre.valorfcpdestinatario, vendamaterialmestre.valoricmsdestinatario, vendamaterialmestre.valoricmsremetente, " +
					 "vendamaterialmestre.difal, vendamaterialmestre.valordifal, ncmcapituloMestre.cdncmcapitulo, cfop.cdcfop, " +
					 "vendamaterialmestre.ncmcompleto, vendamaterialmestre.modalidadebcicms, vendamaterialmestre.modalidadebcicmsst, " +
					 "vendamaterialmestre.valorSeguro, vendamaterialmestre.outrasdespesas, " +
					 "vendamaterialmestre.percentualdesoneracaoicms, vendamaterialmestre.valordesoneracaoicms, vendamaterialmestre.abaterdesoneracaoicms, " +
					 "grupotributacao.cdgrupotributacao, " +
					 "material.nome, material.producao, material.identificacao, unidademedida.cdunidademedida, unidademedida.nome, " +
					 "material.peso, material.pesobruto, material.produto, material.servico, material.epi, material.patrimonio, " +
					 "material.kitflexivel, material.vendapromocional, materialgrupo.cdmaterialgrupo, vendamaterialmestre.identificadorinterno, " +
					 "ncmcapitulo.cdncmcapitulo, ncmcapitulo.descricao, material.ncmcompleto," +
					 "materialtipo.cdmaterialtipo, materialcategoria.cdmaterialcategoria")
			.join("vendamaterialmestre.material material")
			.join("vendamaterialmestre.venda venda")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
			.leftOuterJoin("vendamaterialmestre.grupotributacao grupotributacao")
			.leftOuterJoin("material.materialtipo materialtipo")
			.leftOuterJoin("material.materialcategoria materialcategoria")
			.leftOuterJoin("vendamaterialmestre.cfop cfop")
			.leftOuterJoin("vendamaterialmestre.ncmcapitulo ncmcapituloMestre")
			.where("venda = ?",venda)
			.list();		
	}
	
	public void updateNomeAlternativo(Vendamaterialmestre vmm) {
		if(vmm != null && vmm.getCdvendamaterialmestre() != null){
			getHibernateTemplate().bulkUpdate("update Vendamaterialmestre vmm set vmm.nomealternativo = ? where vmm.id = ?", new Object[]{vmm.getNomealternativo(), vmm.getCdvendamaterialmestre()});
		}
	}

}
