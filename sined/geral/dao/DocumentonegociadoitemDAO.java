package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Documentonegociadoitem;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentonegociadoitemDAO extends GenericDAO<Documentonegociadoitem> {

	/* singleton */
	private static DocumentonegociadoitemDAO instance;
	public static DocumentonegociadoitemDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(DocumentonegociadoitemDAO.class);
		}
		return instance;
	}
}