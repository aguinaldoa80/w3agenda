package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Leadsituacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("leadsituacao.nome")
public class LeadsituacaoDAO extends GenericDAO<Leadsituacao>{

	/**
	 * M�todo que busca as situa��es do lead para exibir na listagem
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Leadsituacao> findForListagem() {
		return query()
				.select("leadsituacao.cdleadsituacao, leadsituacao.nome")
				.where("leadsituacao.defaultListagem = true")
				.orderBy("leadsituacao.nome")
				.list();
	}

	/**
	 * Busca a situa��o do lead pelo nome.
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/02/2016
	 */
	public Leadsituacao findByNome(String nome) {
		if(nome == null || nome.equals("")){
			throw new SinedException("Nome da situa��o n�o pode ser nulo.");
		}
		
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		List<Leadsituacao> lista = query()
			.select("leadsituacao.cdleadsituacao, leadsituacao.nome")
			.where("UPPER(" + funcaoTiraacento + "(leadsituacao.nome)) LIKE ?", Util.strings.tiraAcento(nome).toUpperCase())
			.list();
		
		if(lista != null && lista.size() > 1){
			throw new SinedException("Mais de uma situa��o encontrada.");
		} else if(lista != null && lista.size() == 1){
			return lista.get(0);
		} else return null;
	}
	
	@Override
	public ListagemResult<Leadsituacao> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Leadsituacao> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("leadsituacao.cdleadsituacao");
		
		return new ListagemResult<Leadsituacao>(query, filtro);
	}
}
