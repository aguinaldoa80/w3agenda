package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Tipovencimento;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.TipovencimentoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TipovencimentoDAO extends GenericDAO<Tipovencimento>{

	@Override
	public void updateListagemQuery(QueryBuilder<Tipovencimento> query,	FiltroListagem _filtro) {
		
		TipovencimentoFiltro filtro = (TipovencimentoFiltro) _filtro;
		
		query
			.whereLikeIgnoreAll("tipovencimento.nome", filtro.getNome())
			.orderBy("tipovencimento.nome");
	}
}
