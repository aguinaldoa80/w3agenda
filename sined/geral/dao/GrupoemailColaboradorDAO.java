package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Grupoemail;
import br.com.linkcom.sined.geral.bean.GrupoemailColaborador;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class GrupoemailColaboradorDAO extends GenericDAO<GrupoemailColaborador>{
	
	/**
	 * Retorna todos os colaboradores
	 * do grupoemail
	 * @param grupoemail
	 * @return	
	 * @author C�ntia Nogueira
	 */
	public List<GrupoemailColaborador> find(List<Grupoemail> listagrupoemail){
		if(listagrupoemail==null){
			throw new SinedException("O objeto n�o pode ser nulo em GrupoemailcolaboradorDAO");
		}
		return query().
				select("grupoemailColaborador.cdgrupoemailcolaborador, colaborador.nome, colaborador.cdpessoa," +
				"colaborador.email")
				.join("grupoemailColaborador.colaborador colaborador")
				.whereIn("grupoemailColaborador.grupoemail", CollectionsUtil.listAndConcatenate(listagrupoemail, "cdgrupoemail", ",") )
				.list();
	}

}
