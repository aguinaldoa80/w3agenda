package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.MdfeHistorico;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MdfeHistoricoDAO extends GenericDAO<MdfeHistorico>{

	public List<MdfeHistorico> findByMdfe(Mdfe mdfe) {
		if(mdfe == null || mdfe.getCdmdfe() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("mdfeHistorico.cdMdfeHistorico, mdfeHistorico.mdfeSituacao, mdfeHistorico.dtaltera, mdfeHistorico.observacao, usuarioAltera.cdpessoa, usuarioAltera.nome")
				.leftOuterJoin("mdfeHistorico.usuarioAltera usuarioAltera")
				.where("mdfeHistorico.mdfe = ?", mdfe)
				.orderBy("mdfeHistorico.dtaltera desc, mdfeHistorico.cdMdfeHistorico desc")
				.list();
	}

}
