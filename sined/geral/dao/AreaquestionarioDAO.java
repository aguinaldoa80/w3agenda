package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Area;
import br.com.linkcom.sined.geral.bean.Areaquestionario;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AreaquestionarioDAO extends GenericDAO<Areaquestionario> {

	public List<Areaquestionario> findByArea(Area area) {
		if(area == null || area.getCdarea() == null){
			throw new SinedException("�rea n�o pode ser nulo.");
		}
		return query()
					.select("areaquestionario.cdareaquestionario, areaquestionario.ordem, " +
							"areaquestionario.pergunta, areaquestionario.tiporesposta")
					.where("areaquestionario.area = ?", area)
					.orderBy("areaquestionario.ordem")
					.list();
	}
	
}
