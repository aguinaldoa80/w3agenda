package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Contadigital;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContadigitalDAO extends GenericDAO<Contadigital> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Contadigital> query, FiltroListagem _filtro) {
		query.leftOuterJoinFetch("contadigital.conta conta");
	}
	
}
