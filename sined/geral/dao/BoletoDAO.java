package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Boleto;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class BoletoDAO extends GenericDAO<Boleto> {

	/**
	 * M�todo que verifica se h� algum boleto cadastrado
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeBoletos() {
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.from(Boleto.class)
			.unique() > 0;
	}
	
	@Override
	public ListagemResult<Boleto> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Boleto> query = query();
		query.select("boleto.cdboleto, boleto.nome");
		
		return new ListagemResult<Boleto>(query, filtro);
	}
}
