package br.com.linkcom.sined.geral.dao;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Calendario;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CalendarioFiltro;
import br.com.linkcom.sined.util.DateUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CalendarioDAO extends GenericDAO<Calendario> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Calendario> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		CalendarioFiltro filtro = (CalendarioFiltro) _filtro;
		query
			.select("distinct calendario.cdcalendario, calendario.nome")
			.leftOuterJoin("calendario.listaCalendarioorcamento listaCalendarioorcamento")
			.leftOuterJoin("listaCalendarioorcamento.orcamento orcamento")
			.leftOuterJoin("calendario.listaCalendarioprojeto listaCalendarioprojeto")
			.leftOuterJoin("listaCalendarioprojeto.projeto projeto")
			.ignoreAllJoinsPath(true)
			.whereLikeIgnoreAll("calendario.nome", filtro.getDescricao())
			.where("orcamento = ?", filtro.getOrcamento())
			.where("projeto = ?", filtro.getProjeto());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Calendario> query) {
		query
			.select("calendario.cdcalendario, calendario.nome, projeto.cdprojeto, orcamento.cdorcamento, listaCalendarioitem.cdcalendarioitem, " +
					"listaCalendarioitem.descricao, listaCalendarioitem.dtferiado, calendario.sabadoutil, calendario.domingoutil, " +
					"calendario.cdusuarioaltera, calendario.dtaltera, uf.cduf, municipio.cdmunicipio")
			.leftOuterJoin("calendario.uf uf")
			.leftOuterJoin("calendario.municipio municipio")
			.leftOuterJoin("calendario.listaCalendarioorcamento listaCalendarioorcamento")
			.leftOuterJoin("listaCalendarioorcamento.orcamento orcamento")
			.leftOuterJoin("calendario.listaCalendarioprojeto listaCalendarioprojeto")
			.leftOuterJoin("listaCalendarioprojeto.projeto projeto")
			.leftOuterJoin("calendario.listaCalendarioitem listaCalendarioitem")
			.orderBy("listaCalendarioitem.descricao");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaCalendarioitem");
		save.saveOrUpdateManaged("listaCalendarioorcamento");
		save.saveOrUpdateManaged("listaCalendarioprojeto");
	}
	
	/**
	 * Retorna uma lista de calend�rios, cada qual com sua lista de feriados de um determinado projeto
	 *
	 * @param projeto
	 * @throws SinedException - quando o par�metro for nulo.
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public List<Calendario> findByProjeto(Projeto projeto) {
		if (projeto == null || projeto.getCdprojeto() == null) {
			throw new SinedException("Par�metros inv�lidos. O campo projeto n�o pode ser nulo.");
		}
		return 
			query()
				.select("calendario.cdcalendario, calendario.sabadoutil, calendario.domingoutil, " +
						"listaCalendarioitem.cdcalendarioitem, listaCalendarioitem.descricao, listaCalendarioitem.dtferiado")
				.leftOuterJoin("calendario.listaCalendarioitem listaCalendarioitem")
				.leftOuterJoin("calendario.listaCalendarioprojeto listaCalendarioprojeto")
				.leftOuterJoin("listaCalendarioprojeto.projeto projeto")
				.where("projeto = ?", projeto)
				.list();		
	}

	/**
	 * Carrega a lista do calendario sem o c�dido dos itens.
	 *
	 * ### N�O PODE COLOCAR O CDCALENDARIOITEM NO SELECT ###
	 *
	 * @param calendario
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Calendario findWithLista(Projeto projeto) {
		if(projeto == null || projeto.getCdprojeto() == null){
			throw new SinedException("Calend�rio n�o pode ser nulo.");
		}
		return query()
					.select("listaCalendarioitem.descricao, listaCalendarioitem.dtferiado")
					.leftOuterJoin("calendario.listaCalendarioitem listaCalendarioitem")
					.leftOuterJoin("calendario.listaCalendarioprojeto listaCalendarioprojeto")
					.leftOuterJoin("listaCalendarioprojeto.projeto projeto")
					.where("projeto = ?", projeto)
					.unique();
	}

	/**
	 * Carrega a listagem de calendario com todas as listas preenchidas.
	 *
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Calendario> loadWithLista(String whereIn, String orderBy, boolean asc) {
		QueryBuilder<Calendario> query = query()
					.select("calendario.cdcalendario, calendario.nome, projeto.nome, orcamento.nome")
					.leftOuterJoin("calendario.listaCalendarioorcamento listaCalendarioorcamento")
					.leftOuterJoin("listaCalendarioorcamento.orcamento orcamento")
					.leftOuterJoin("calendario.listaCalendarioprojeto listaCalendarioprojeto")
					.leftOuterJoin("listaCalendarioprojeto.projeto projeto")
					.whereIn("calendario.cdcalendario", whereIn);
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("calendario.nome");
		}
		
		return query.list();
	}

	/**
	 * Retorna uma lista de calend�rios, cada qual com sua lista de feriados de um determinado projeto
	 *
	 * @param projeto
	 * @throws SinedException - quando o par�metro for nulo.
	 * 
	 * @author Rodrigo Freitas
	 */	
	public List<Calendario> findByOrcamento(Orcamento orcamento) {
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("Par�metros inv�lidos. O campo or�amento n�o pode ser nulo.");
		}
		return 
			query()
				.select("calendario.cdcalendario, calendario.sabadoutil, calendario.domingoutil, " +
						"listaCalendarioitem.cdcalendarioitem, listaCalendarioitem.descricao, listaCalendarioitem.dtferiado")
				.leftOuterJoin("calendario.listaCalendarioitem listaCalendarioitem")
				.leftOuterJoin("calendario.listaCalendarioorcamento listaCalendarioorcamento")
				.leftOuterJoin("listaCalendarioorcamento.orcamento orcamento")
				.where("orcamento = ?", orcamento)
				.list();		
	}
	
	/**
	 * Retorna verdadeiro se for considerado feriado.
	 * @param data
	 * @return
	 * @author Taidson
	 * @param municipio 
	 * @param uf 
	 * @since 08/11/2010
	 */
	public Boolean isFeriado(Date data, Uf uf, Municipio municipio, boolean consideraFimSemana){
		String sql = "SELECT COUNT(*) " +
						"FROM calendario c " +
						"JOIN calendarioitem ci ON ci.cdcalendario = c.cdcalendario " +
						"WHERE NOT EXISTS (SELECT cp.cdcalendarioprojeto FROM calendarioprojeto cp WHERE cp.cdcalendario = c.cdcalendario) " +
						"AND NOT EXISTS (SELECT co.cdcalendarioorcamento FROM calendarioorcamento co WHERE co.cdcalendario = c.cdcalendario) ";
		
		if(uf != null && uf.getCduf() != null){
			sql += "AND (c.cduf is null OR c.cduf = " + uf.getCduf() + ") ";
			
			if(municipio != null && municipio.getCdmunicipio() != null){
				sql += "AND (c.cdmunicipio is null OR c.cdmunicipio = " + municipio.getCdmunicipio() + ") ";
			} else {
				sql += "AND c.cdmunicipio is null ";
			}
		} else sql += "AND c.cduf is null AND c.cdmunicipio is null ";
		SinedUtil.markAsReader();
		boolean isFeriado = getJdbcTemplate().queryForLong(sql + "AND ci.dtferiado = '" + SinedDateUtils.toStringPostgre(data) + "' ") > 0;
		if(!isFeriado && consideraFimSemana && DateUtils.fimDeSemana(data)){

			
			sql = "SELECT COUNT(1) " +
			"FROM calendario c " +
			"JOIN calendarioitem ci ON ci.cdcalendario = c.cdcalendario " +
			"WHERE NOT EXISTS (SELECT cp.cdcalendarioprojeto FROM calendarioprojeto cp WHERE cp.cdcalendario = c.cdcalendario) " +
			"AND NOT EXISTS (SELECT co.cdcalendarioorcamento FROM calendarioorcamento co WHERE co.cdcalendario = c.cdcalendario) ";

			if(uf != null && uf.getCduf() != null){
				sql += "AND (c.cduf is null OR c.cduf = " + uf.getCduf() + ") ";
				
				if(municipio != null && municipio.getCdmunicipio() != null){
					sql += "AND (c.cdmunicipio is null OR c.cdmunicipio = " + municipio.getCdmunicipio() + ") ";
				} else {
					sql += "AND c.cdmunicipio is null ";
				}
			} else sql += "AND c.cduf is null AND c.cdmunicipio is null ";
			if(DateUtils.isSabado(data)){
				sql += " AND c.sabadoutil = true";
			}else if(DateUtils.isDomingo(data)){
				sql += " AND c.domingoutil = true";
			}
			SinedUtil.markAsReader();
			isFeriado = getJdbcTemplate().queryForLong(sql) == 0;
		}
		return isFeriado;
	}

	/**
	* M�todo que busca calendario geral 
	*
	* @return
	* @since Aug 17, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Calendario> findCalendarioGeral(Uf uf, Municipio municipio) {
		QueryBuilder<Calendario> query = query()
				.select("calendario.cdcalendario, calendario.sabadoutil, calendario.domingoutil, listaCalendarioitem.cdcalendarioitem, listaCalendarioitem.dtferiado")
				.leftOuterJoin("calendario.listaCalendarioitem listaCalendarioitem")
				.where("not exists (select co.cdcalendarioorcamento from Calendarioorcamento co where co.calendario = calendario)")
				.where("not exists (select cp.cdcalendarioprojeto from Calendarioprojeto cp where cp.calendario = calendario)");
		
		this.montaWhereMunicipioUf(uf, municipio, query);
		return query.list();
	}

	/**
	 * Monta o where de municipio e uf no calend�rio
	 *
	 * @param uf
	 * @param municipio
	 * @param query
	 * @author Rodrigo Freitas
	 * @since 04/02/2015
	 */
	private void montaWhereMunicipioUf(Uf uf, Municipio municipio,QueryBuilder<Calendario> query) {
		if(uf != null && uf.getCduf() != null){
			query
				.openParentheses()
					.where("calendario.uf is null")
					.or()
					.where("calendario.uf = ?", uf)
				.closeParentheses();
			
			if(municipio != null && municipio.getCdmunicipio() != null){
				query
					.openParentheses()
						.where("calendario.municipio is null")
						.or()
						.where("calendario.municipio = ?", municipio)
					.closeParentheses();
			} else {
				query.where("calendario.municipio is null");
			}
		} else {
			query
				.where("calendario.uf is null")
				.where("calendario.municipio is null");
		}
	}
	
	/**
	 * M�todo que busca calendario geral sem permiss�o de projeto e empresa
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/02/2015
	 */
	public List<Calendario> findCalendarioGeralWithoutWhereEmpresaProjeto(Uf uf, Municipio municipio) {
		QueryBuilder<Calendario> query = querySined()
				.setUseWhereEmpresa(false)
				.setUseWhereProjeto(false)
				.select("calendario.cdcalendario, calendario.sabadoutil, calendario.domingoutil, listaCalendarioitem.cdcalendarioitem, listaCalendarioitem.dtferiado")
				.leftOuterJoin("calendario.listaCalendarioitem listaCalendarioitem")
				.where("not exists (select co.cdcalendarioorcamento from Calendarioorcamento co where co.calendario = calendario)")
				.where("not exists (select cp.cdcalendarioprojeto from Calendarioprojeto cp where cp.calendario = calendario)");
		
		this.montaWhereMunicipioUf(uf, municipio, query);
		return query.list();
	}
	
}
