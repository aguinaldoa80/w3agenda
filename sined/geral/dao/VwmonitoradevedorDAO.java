package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaorestricao;
import br.com.linkcom.sined.geral.bean.view.Vwmonitoradevedor;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.VwmonitoradevedorFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VwmonitoradevedorDAO extends GenericDAO<Vwmonitoradevedor>{

	@Override
	public void updateListagemQuery(QueryBuilder<Vwmonitoradevedor> query,FiltroListagem _filtro) {
		
		if(_filtro == null){
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		}
		VwmonitoradevedorFiltro filtro = (VwmonitoradevedorFiltro)_filtro;
		
		
		query
			.where("qtdecontas >= ?", filtro.getQtdecontas1())
			.where("qtdecontas <= ?", filtro.getQtdecontas2())
			.orderBy("nome_cliente");
			if(filtro.getCategoria() != null){
				query.where("cdcategoria = ?", filtro.getCategoria().getCdcategoria());
			}
			if(filtro.getEmpresa() != null){
				query.where("cdempresa = ?", filtro.getEmpresa().getCdpessoa());
			}
			if(filtro.getCliente() != null){
				query.where("cdcliente = ?", filtro.getCliente().getCdpessoa());
			}
			if(filtro.getSituacaorestricao() != null){
				if(filtro.getSituacaorestricao().equals(Situacaorestricao.NAO_BLOQUEADO)){
					query.where("(cdsituacao is null or cdsituacao = ?)", filtro.getSituacaorestricao().ordinal());
				}else{
					query.where("cdsituacao = ?", filtro.getSituacaorestricao().ordinal());
				}
			}
		
	}

	
	
	
}
