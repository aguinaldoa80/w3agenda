package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Garantiareforma;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.Garantiasituacao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.GarantiareformaFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class GarantiareformaDAO extends GenericDAO<Garantiareforma>{

	@Override
	public void updateEntradaQuery(QueryBuilder<Garantiareforma> query) {
		query.select("garantiareforma.cdgarantiareforma, garantiareforma.dtgarantia, garantiareforma.pedidoexterno, "+
					 "garantiareforma.dtaltera, garantiareforma.identificadorexternopedidovenda, garantiareforma.garantiasituacao, "+
					 "cliente.nome, cliente.razaosocial, cliente.cdpessoa, "+
					 "empresa.nome, empresa.razaosocial, empresa.cdpessoa, "+
					 "vendedor.nome, vendedor.cdpessoa, "+
					 "pedidovenda.cdpedidovenda, pedidovenda.identificador, clientepedidovenda.cdpessoa, clientepedidovenda.nome, clientepedidovenda.razaosocial, "+
					 "pedidovendaorigem.cdpedidovenda, pedidovendaorigem.identificador, clientepedidovendaorigem.cdpessoa, clientepedidovendaorigem.nome, clientepedidovendaorigem.razaosocial, "+
					 "listaGarantiareformaitem.cdgarantiareformaitem, "+
					 "material.cdmaterial, listaGarantiareformaitem.residuobanda, "+
					 "material.nome, material.identificacao, "+
					 "garantiatipo.cdgarantiatipo, garantiatipo.descricao, garantiatipo.geragarantia, "+
					 "garantiatipopercentual.cdgarantiatipopercentual, garantiatipopercentual.percentual, "+
					 "listaGarantiareformahistorico.cdgarantiareformahistorico, listaGarantiareformahistorico.acao, listaGarantiareformahistorico.observacao, listaGarantiareformahistorico.dtaltera, listaGarantiareformahistorico.cdusuarioaltera," +
					 "garantiareformaresultado.nome, garantiareformaresultado.cdgarantiareformaresultado," +
					 "motivodevolucao.cdmotivodevolucao, motivodevolucao.descricao")
				.leftOuterJoin("garantiareforma.cliente cliente")
				.leftOuterJoin("garantiareforma.empresa empresa")
				.leftOuterJoin("garantiareforma.vendedor vendedor")
				.leftOuterJoin("garantiareforma.garantiareformaresultado garantiareformaresultado")
				.leftOuterJoin("garantiareforma.listaGarantiareformaitem listaGarantiareformaitem")
				.leftOuterJoin("garantiareforma.listaGarantiareformahistorico listaGarantiareformahistorico")
				.leftOuterJoin("garantiareforma.pedidovenda pedidovenda")
				.leftOuterJoin("pedidovenda.cliente clientepedidovenda")
				.leftOuterJoin("garantiareforma.pedidovendaorigem pedidovendaorigem")
				.leftOuterJoin("pedidovendaorigem.cliente clientepedidovendaorigem")
				.leftOuterJoin("listaGarantiareformaitem.garantiatipo garantiatipo")
				.leftOuterJoin("listaGarantiareformaitem.pneu pneu")
				.leftOuterJoin("listaGarantiareformaitem.material material")
//				.leftOuterJoin("listaGarantiareformaitem.constatacao constatacao")
				.leftOuterJoin("listaGarantiareformaitem.garantiatipopercentual garantiatipopercentual")
				.leftOuterJoin("listaGarantiareformaitem.listaGarantiareformaitemconstatacao listaGarantiareformaitemconstatacao")
				.leftOuterJoin("listaGarantiareformaitemconstatacao.motivodevolucao motivodevolucao");
		query = SinedUtil.setJoinsByComponentePneu(query);
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaGarantiareformaitem");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Garantiareforma> query,
			FiltroListagem filtro) {
		GarantiareformaFiltro _filtro = (GarantiareformaFiltro)filtro;
		query
			.select("garantiareforma.cdgarantiareforma, garantiareforma.dtgarantia, garantiareforma.pedidoexterno, "+
					 "garantiareforma.dtaltera, garantiareforma.identificadorexternopedidovenda, garantiareforma.garantiasituacao, "+
					 "cliente.nome, cliente.razaosocial, cliente.cdpessoa, "+
					 "empresa.nome, empresa.razaosocial, empresa.cdpessoa, "+
					 "vendedor.nome, vendedor.cdpessoa, "+
					 "pedidovenda.cdpedidovenda,pedidovenda.identificador, "+
					 "pneu.cdpneu, "+
					 "material.cdmaterial, material.nome, pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome")
			.join("garantiareforma.listaGarantiareformaitem listaGarantiareformaitem")
			.leftOuterJoin("garantiareforma.cliente cliente")
			.leftOuterJoin("garantiareforma.empresa empresa")
			.leftOuterJoin("garantiareforma.vendedor vendedor")
			.leftOuterJoin("garantiareforma.pedidovenda pedidovenda")
			.leftOuterJoin("garantiareforma.pedidovendaorigem pedidovendaorigem")
			.leftOuterJoin("garantiareforma.garantiareformaresultado garantiareformaresultado")
			.leftOuterJoin("listaGarantiareformaitem.material material")
			.leftOuterJoin("listaGarantiareformaitem.pneu pneu")
			.leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
			.where("empresa = ?", _filtro.getEmpresa())
			.where("cliente = ?", _filtro.getCliente())
			.where("vendedor = ?", _filtro.getVendedor())
			.where("garantiareforma.garantiasituacao = ?", _filtro.getGarantiasituacao())
			.where("pedidovenda = ?", _filtro.getPedidovenda())
			.where("pedidovendaorigem = ?", _filtro.getPedidovendaorigem())
			.where("garantiareforma.identificadorexternopedidovenda = ?", _filtro.getIdentificadorexternopedidovenda())
			.where("material = ?", _filtro.getServicogarantido())
			.where("garantiareformaresultado = ?", _filtro.getGarantiareformaresultado())
			.where("pneu.cdpneu = ?", _filtro.getPneu())
			.orderBy("garantiareforma.cdgarantiareforma");
	}
	
	public void updateSituacaoGarantia(String whereIn, Garantiasituacao garantiasituacao){
		String sql = "update garantiareforma set garantiasituacao = " + garantiasituacao.ordinal() + 
					 " where cdgarantiareforma in (" + whereIn+")";
		getJdbcTemplate().execute(sql);
	}
	
	public List<Garantiareforma> findForCalculargarantia(String whereIn, Cliente cliente){
		return this.findForCalculargarantia(whereIn, cliente, null);
	}
	
	public List<Garantiareforma> findForCalculargarantia(String whereIn, Cliente cliente, String whereNotInGarantiareformaitem){
		return query()
			.select("garantiareforma.cdgarantiareforma, garantiareforma.dtgarantia, garantiareforma.pedidoexterno, empresa.cdpessoa, "+
					 "garantiareforma.dtaltera, garantiareforma.identificadorexternopedidovenda, garantiareforma.garantiasituacao, "+
					 "cliente.nome, cliente.razaosocial, cliente.cdpessoa, "+
					 "pedidovendaorigem.cdpedidovenda, pneu.cdpneu, material.cdmaterial, material.nome, material.valorvenda, listaGarantiareformaitem.cdgarantiareformaitem, "+
					 "garantiatipopercentual.cdgarantiatipopercentual, garantiatipopercentual.percentual, "+
					 "garantiatipo.cdgarantiatipo, garantiatipo.descricao, "+
					 "pedidovendatipo.cdpedidovendatipo, "+
					 "prazopagamento.cdprazopagamento, pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome")
			.join("garantiareforma.cliente cliente")
			.join("garantiareforma.empresa empresa")
			.leftOuterJoin("garantiareforma.pedidovendaorigem pedidovendaorigem")
			.leftOuterJoin("pedidovendaorigem.pedidovendatipo pedidovendatipo")
			.leftOuterJoin("pedidovendaorigem.prazopagamento prazopagamento")
			.join("garantiareforma.listaGarantiareformaitem listaGarantiareformaitem")
			.leftOuterJoin("listaGarantiareformaitem.garantiatipo garantiatipo")
			.leftOuterJoin("listaGarantiareformaitem.garantiatipopercentual garantiatipopercentual")
			.join("listaGarantiareformaitem.material material")
			.join("listaGarantiareformaitem.pneu pneu")
			.leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
			.where("garantiareforma.cliente = ?", cliente)
			.where("garantiareforma.garantiasituacao = ?", Garantiasituacao.DISPONIVEL)
			.whereIn("garantiareforma.cdgarantiareforma", whereIn)
			.whereIn("listaGarantiareformaitem.cdgarantiareformaitem not", whereNotInGarantiareformaitem)
			.list();

	}
	
	public List<Garantiareforma> findGarantiautilizadaByPedido(Pedidovenda pedidovendadestino, String whereNotInGarantiareformaitem){
		return query()
			.select("garantiareforma.cdgarantiareforma, garantiareforma.dtgarantia, garantiareforma.pedidoexterno, empresa.cdpessoa, "+
					 "garantiareforma.dtaltera, garantiareforma.identificadorexternopedidovenda, garantiareforma.garantiasituacao, "+
					 "cliente.nome, cliente.razaosocial, cliente.cdpessoa, "+
					 "pedidovendaorigem.cdpedidovenda, pneu.cdpneu, material.cdmaterial, material.nome, material.valorvenda, listaGarantiareformaitem.cdgarantiareformaitem, "+
					 "garantiatipopercentual.cdgarantiatipopercentual, garantiatipopercentual.percentual, "+
					 "garantiatipo.cdgarantiatipo, garantiatipo.descricao, "+
					 "pedidovendatipo.cdpedidovendatipo, "+
					 "prazopagamento.cdprazopagamento, pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome")
			.join("garantiareforma.cliente cliente")
			.join("garantiareforma.empresa empresa")
			.leftOuterJoin("garantiareforma.pedidovendaorigem pedidovendaorigem")
			.leftOuterJoin("pedidovendaorigem.pedidovendatipo pedidovendatipo")
			.leftOuterJoin("pedidovendaorigem.prazopagamento prazopagamento")
			.join("garantiareforma.listaGarantiareformaitem listaGarantiareformaitem")
			.leftOuterJoin("listaGarantiareformaitem.garantiatipo garantiatipo")
			.leftOuterJoin("listaGarantiareformaitem.garantiatipopercentual garantiatipopercentual")
			.join("listaGarantiareformaitem.material material")
			.join("listaGarantiareformaitem.pneu pneu")
			.join("listaGarantiareformaitem.pedidovendamaterialdestino pedidovendamaterialdestino")
			.join("pedidovendamaterialdestino.pedidovenda pedidovendadestino")
			.leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
			.where("pedidovendadestino = ?", pedidovendadestino)
			.where("garantiareforma.garantiasituacao = ?", Garantiasituacao.UTILIZADA)
			.whereIn("listaGarantiareformaitem.cdgarantiareformaitem not", whereNotInGarantiareformaitem)
			.list();
	}
	
	public List<Garantiareforma> findGarantiautilizadaByVenda(Venda vendadestino, String whereNotInGarantiareformaitem){
		return query()
			.select("garantiareforma.cdgarantiareforma, garantiareforma.dtgarantia, garantiareforma.pedidoexterno, empresa.cdpessoa, "+
					 "garantiareforma.dtaltera, garantiareforma.identificadorexternopedidovenda, garantiareforma.garantiasituacao, "+
					 "cliente.nome, cliente.razaosocial, cliente.cdpessoa, "+
					 "pedidovendaorigem.cdpedidovenda, pneu.cdpneu, material.cdmaterial, material.nome, material.valorvenda, listaGarantiareformaitem.cdgarantiareformaitem, "+
					 "garantiatipopercentual.cdgarantiatipopercentual, garantiatipopercentual.percentual, "+
					 "garantiatipo.cdgarantiatipo, garantiatipo.descricao, "+
					 "pedidovendatipo.cdpedidovendatipo, "+
					 "prazopagamento.cdprazopagamento, pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome")
			.join("garantiareforma.cliente cliente")
			.join("garantiareforma.empresa empresa")
			.leftOuterJoin("garantiareforma.pedidovendaorigem pedidovendaorigem")
			.leftOuterJoin("pedidovendaorigem.pedidovendatipo pedidovendatipo")
			.leftOuterJoin("pedidovendaorigem.prazopagamento prazopagamento")
			.join("garantiareforma.listaGarantiareformaitem listaGarantiareformaitem")
			.leftOuterJoin("listaGarantiareformaitem.garantiatipo garantiatipo")
			.leftOuterJoin("listaGarantiareformaitem.garantiatipopercentual garantiatipopercentual")
			.join("listaGarantiareformaitem.material material")
			.join("listaGarantiareformaitem.pneu pneu")
			.join("listaGarantiareformaitem.vendamaterialdestino vendamaterialdestino")
			.join("vendamaterialdestino.venda vendadestino")
			.leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
			.where("vendadestino = ?", vendadestino)
			.where("garantiareforma.garantiasituacao = ?", Garantiasituacao.UTILIZADA)
			.whereIn("listaGarantiareformaitem.cdgarantiareformaitem not", whereNotInGarantiareformaitem)
			.list();
		
		
	}
	
	public List<Garantiareforma> findByWherein(String whereIn){
		return query()
			.whereIn("garantiareforma.cdgarantiareforma", whereIn)
			.list();
	}
	
	public List<Garantiareforma> findForComprovanteGarantia(String whereIn){
		return query()
			.select("garantiareforma.cdgarantiareforma, garantiareforma.dtgarantia, garantiareforma.pedidoexterno, garantiareforma.impresso, "+
					 "garantiareforma.dtaltera, garantiareforma.identificadorexternopedidovenda, garantiareforma.garantiasituacao, "+
					 "cliente.nome, cliente.razaosocial, cliente.cdpessoa, "+
					 "pedidovendaorigem.cdpedidovenda, pedidovendaorigem.identificador, listaGarantiareformaitem.cdgarantiareformaitem, "+
					 "garantiatipopercentual.cdgarantiatipopercentual, garantiatipopercentual.percentual, "+
					 "garantiatipo.descricao, garantiatipo.cdgarantiatipo, "+
					 "material.cdmaterial, material.nome, material.identificacao, pedidovenda.pedidovendasituacao, "+
					 "materialbanda.cdmaterial, materialbanda.nome, materialbanda.profundidadesulco, "+
					 "pneu.cdpneu, pneu.dot, pneu.serie, "+
					 "pneumedida.nome, pneumedida.cdpneumedida, "+
					 "pneumodelo.nome, pneumodelo.cdpneumodelo, "+
					 "pneumarca.cdpneumarca, pneumarca.nome, "+
					 "motivodevolucao.cdmotivodevolucao, motivodevolucao.descricao, "+
					 "pedidovenda.cdpedidovenda, pedidovenda.identificador, "+
					 "vendedor.cdpessoa, vendedor.nome, "+
					 "empresa.cdpessoa, empresa.nome, "+
					 "logomarca.cdarquivo, "+
					 "garantiareformaresultado.nome, garantiareformaresultado.mensagem1, garantiareformaresultado.mensagem2, "+
					 "endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento," +
					 "endereco.bairro, municipio.nome, uf.sigla, endereco.cep, listaGarantiareformaitem.residuobanda, "+
					 "enderecoClienteAvulso.cdendereco, enderecoClienteAvulso.logradouro, enderecoClienteAvulso.numero, enderecoClienteAvulso.complemento," +
					 "enderecoClienteAvulso.bairro, municipioClienteAvulso.nome, ufClienteAvulso.sigla, enderecoClienteAvulso.cep, pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome")
			.join("garantiareforma.cliente cliente")
			.join("garantiareforma.listaGarantiareformaitem listaGarantiareformaitem")
			.join("garantiareforma.vendedor vendedor")
			.leftOuterJoin("garantiareforma.garantiareformaresultado garantiareformaresultado")
			.leftOuterJoin("garantiareforma.empresa empresa")
			.leftOuterJoin("empresa.logomarca logomarca")
			.leftOuterJoin("listaGarantiareformaitem.garantiatipopercentual garantiatipopercentual")
			.join("listaGarantiareformaitem.garantiatipo garantiatipo")
			.join("listaGarantiareformaitem.material material")
			.join("listaGarantiareformaitem.pneu pneu")
//			.leftOuterJoin("listaGarantiareformaitem.constatacao constatacao")
			.leftOuterJoin("garantiareforma.pedidovendaorigem pedidovendaorigem")
			.leftOuterJoin("pneu.pneumedida pneumedida")
			.leftOuterJoin("pneu.pneumarca pneumarca")
			.leftOuterJoin("pneu.pneumodelo pneumodelo")
			.leftOuterJoin("pneu.materialbanda materialbanda")
			.leftOuterJoin("garantiareforma.pedidovenda pedidovenda")
			.leftOuterJoin("pedidovenda.colaborador colaborador")
			.leftOuterJoin("pedidovenda.endereco endereco")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
			.leftOuterJoin("listaGarantiareformaitem.listaGarantiareformaitemconstatacao listaGarantiareformaitemconstatacao")
			.leftOuterJoin("listaGarantiareformaitemconstatacao.motivodevolucao motivodevolucao")
			.join("cliente.listaEndereco enderecoClienteAvulso")
			.join("enderecoClienteAvulso.municipio municipioClienteAvulso")
			.join("municipio.uf ufClienteAvulso")
			.whereIn("garantiareforma.cdgarantiareforma", whereIn)
			.list();
	}
	
	public List<Garantiareforma> findByNotInGarantiasituacao(String whereIn, Garantiasituacao... situacoes){
		String whereInSituacao = this.montaWhereInSituacoes(situacoes);
		return query()
			.whereIn("garantiareforma.cdgarantiareforma", whereIn)
			.whereIn("garantiareforma.garantiasituacao not", whereInSituacao)
			.list();
	}
	
	private String montaWhereInSituacoes(Garantiasituacao... situacoes){
		String whereInSituacao = null;
		StringBuilder values = new StringBuilder();
		for (int i=0; i < situacoes.length; i++) {
			values.append(situacoes[i].ordinal()).append(",");
		}
		whereInSituacao = values.toString().substring(0, values.toString().length()-1);
		return whereInSituacao;
	}
	
	public List<Garantiareforma> findDisponiveis(Cliente cliente, String whereNotInGarantiareformaitem){
		return query()
			.select("garantiareforma.cdgarantiareforma")
			.join("garantiareforma.listaGarantiareformaitem garantiareformaitem")
			.whereIn("garantiareformaitem.cdgarantiareformaitem not", whereNotInGarantiareformaitem)
			.where("garantiareforma.garantiasituacao = ?", Garantiasituacao.DISPONIVEL)
			.where("garantiareforma.cliente = ?", cliente)
			.list();
	}
	
	public List<Garantiareforma> findBySelecaoPedidovenda(Cliente cliente, Pedidovenda pedidovendadestino, String whereNotInGarantiareformaitem){
		List<Garantiareforma> lista = this.findDisponiveis(cliente, whereNotInGarantiareformaitem);
		lista.addAll(query()
			.select("garantiareforma.cdgarantiareforma")
			.join("garantiareforma.listaGarantiareformaitem garantiareformaitem")
			.join("garantiareformaitem.pedidovendamaterialdestino pedidovendamaterialdestino")
			.join("pedidovendamaterialdestino.pedidovenda pedidovenda")
			.whereIn("garantiareformaitem.cdgarantiareformaitem not", whereNotInGarantiareformaitem)
			.where("pedidovenda = ?", pedidovendadestino)
			.list());
		return lista;
	}
	
	public List<Garantiareforma> findBySelecaoVenda(Cliente cliente, Venda vendadestino, String whereNotInGarantiareformaitem){
		List<Garantiareforma> lista = this.findDisponiveis(cliente, whereNotInGarantiareformaitem);
		lista.addAll(query()
			.select("garantiareforma.cdgarantiareforma")
			.join("garantiareforma.listaGarantiareformaitem garantiareformaitem")
			.join("garantiareformaitem.vendamaterialdestino vendamaterialdestino")
			.join("vendamaterialdestino.venda venda")
			.whereIn("garantiareformaitem.cdgarantiareformaitem not", whereNotInGarantiareformaitem)
			.where("venda = ?", vendadestino)
			.list());
		return lista;
	}
	
	public void sinalizaGarantiareformaComoImpressa(String whereIn){
		String sql = "update garantiareforma set impresso = true "+ 
					" where cdgarantiareforma in (" + whereIn+") "+
					"   and coalesce(impresso, false) = false";
		getJdbcTemplate().execute(sql);
	}

	public Garantiareforma findGarantiaReformaById(String id) {
		return query()
				.select("garantiareforma.cdgarantiareforma, garantiareforma.dtgarantia, garantiareforma.pedidoexterno, "+
				"garantiareforma.dtaltera, garantiareforma.identificadorexternopedidovenda, garantiareforma.garantiasituacao, "+
						
				"cliente.nome, cliente.razaosocial, cliente.cdpessoa, cliente.cpf, cliente.cnpj, cliente.inscricaoestadual, cliente.inscricaomunicipal, listaTelefoneCliente.telefone, "+
				"listaEnderecoCliente.logradouro, listaEnderecoCliente.bairro, listaEnderecoCliente.numero, listaEnderecoCliente.complemento, listaEnderecoCliente.cep, " +
				"municipioCliente.nome, ufCliente.sigla, "+
				
				"empresa.logomarca, empresa.nome, empresa.razaosocial, empresa.cdpessoa, empresa.cpf, empresa.cnpj, empresa.inscricaoestadual, empresa.inscricaomunicipal, listaTelefoneEmpresa.telefone, "+
				"listaEnderecoEmpresa.logradouro, listaEnderecoEmpresa.bairro, listaEnderecoEmpresa.numero, listaEnderecoEmpresa.complemento, listaEnderecoEmpresa.cep, " +
				"municipioEmpresa.nome, ufEmpresa.sigla, "+
				
				"vendedor.nome, vendedor.cdpessoa, "+
				"pedidovenda.cdpedidovenda, pedidovenda.identificador, clientepedidovenda.cdpessoa, clientepedidovenda.nome, clientepedidovenda.razaosocial, "+
				"pedidovendaorigem.cdpedidovenda, pedidovendaorigem.identificador, clientepedidovendaorigem.cdpessoa, clientepedidovendaorigem.nome, clientepedidovendaorigem.razaosocial, "+
				"listaGarantiareformaitem.cdgarantiareformaitem, pneumarca.cdpneumarca, pneumarca.nome, pneumodelo.cdpneumodelo, pneumodelo.nome, pneumedida.cdpneumedida, pneumedida.nome, "+
				"pneu.descricao, pneu.acompanhaRoda, pneu.lonas, otrPneuTipo.cdOtrPneuTipo, " +
				"pneu.serie, pneu.cdpneu, pneu.dot, pneu.numeroreforma, materialbanda.cdmaterial, materialbanda.nome, materialbanda.profundidadesulco, material.cdmaterial, listaGarantiareformaitem.residuobanda, "+
				"material.nome, material.identificacao, "+
				"garantiatipo.cdgarantiatipo, garantiatipo.descricao, garantiatipo.geragarantia, "+
				"garantiatipopercentual.cdgarantiatipopercentual, garantiatipopercentual.percentual, "+
				"listaGarantiareformahistorico.cdgarantiareformahistorico, listaGarantiareformahistorico.acao, listaGarantiareformahistorico.observacao, listaGarantiareformahistorico.dtaltera, listaGarantiareformahistorico.cdusuarioaltera," +
				"pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome, "+
				"garantiareformaresultado.nome, garantiareformaresultado.mensagem1, garantiareformaresultado.mensagem2, garantiareformaresultado.cdgarantiareformaresultado," +
				"motivodevolucao.cdmotivodevolucao, motivodevolucao.descricao")
			.leftOuterJoin("garantiareforma.cliente cliente")
			.leftOuterJoin("cliente.listaEndereco listaEnderecoCliente")
			.leftOuterJoin("listaEnderecoCliente.municipio municipioCliente")
			.leftOuterJoin("municipioCliente.uf ufCliente")
			.leftOuterJoin("cliente.listaTelefone listaTelefoneCliente")
			.leftOuterJoin("garantiareforma.empresa empresa")
			.leftOuterJoin("empresa.listaEndereco listaEnderecoEmpresa")
			.leftOuterJoin("listaEnderecoEmpresa.municipio municipioEmpresa")
			.leftOuterJoin("municipioEmpresa.uf ufEmpresa")
			.leftOuterJoin("empresa.listaTelefone listaTelefoneEmpresa")
			.leftOuterJoin("garantiareforma.vendedor vendedor")
			.leftOuterJoin("garantiareforma.garantiareformaresultado garantiareformaresultado")
			.leftOuterJoin("garantiareforma.listaGarantiareformaitem listaGarantiareformaitem")
			.leftOuterJoin("garantiareforma.listaGarantiareformahistorico listaGarantiareformahistorico")
			.leftOuterJoin("garantiareforma.pedidovenda pedidovenda")
			.leftOuterJoin("pedidovenda.cliente clientepedidovenda")
			.leftOuterJoin("garantiareforma.pedidovendaorigem pedidovendaorigem")
			.leftOuterJoin("pedidovendaorigem.cliente clientepedidovendaorigem")
			.leftOuterJoin("listaGarantiareformaitem.garantiatipo garantiatipo")
			.leftOuterJoin("listaGarantiareformaitem.pneu pneu")
			.leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
			.leftOuterJoin("pneu.pneumarca pneumarca")
			.leftOuterJoin("pneu.pneumodelo pneumodelo")
			.leftOuterJoin("pneu.pneumedida pneumedida")
			.leftOuterJoin("pneu.materialbanda materialbanda")
			.leftOuterJoin("pneu.otrPneuTipo otrPneuTipo")
			.leftOuterJoin("listaGarantiareformaitem.material material")
			.leftOuterJoin("listaGarantiareformaitem.garantiatipopercentual garantiatipopercentual")
			.leftOuterJoin("listaGarantiareformaitem.listaGarantiareformaitemconstatacao listaGarantiareformaitemconstatacao")
			.leftOuterJoin("listaGarantiareformaitemconstatacao.motivodevolucao motivodevolucao")
			.where("garantiareforma.cdgarantiareforma = ?", Integer.parseInt(id))
			.unique();
	}
}
