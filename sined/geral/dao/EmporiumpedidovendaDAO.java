package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Emporiumpedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.EmporiumpedidovendaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EmporiumpedidovendaDAO extends GenericDAO<Emporiumpedidovenda>{

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaEmporiumpedidovendaitem");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Emporiumpedidovenda> query, FiltroListagem _filtro) {
		EmporiumpedidovendaFiltro filtro = (EmporiumpedidovendaFiltro) _filtro;
		query.whereIn("emporiumpedidovenda.pedidovenda.cdpedidovenda", filtro.getPedidovenda());
		query.orderBy("emporiumpedidovenda.datahorafiscal desc");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Emporiumpedidovenda> query) {
		query
			.leftOuterJoinFetch("emporiumpedidovenda.listaEmporiumpedidovendaitem listaEmporiumpedidovendaitem")
			.leftOuterJoinFetch("emporiumpedidovenda.arquivo arquivo");
	}

	/**
	 * Atualiza a flag do pedido de venda
	 *
	 * @param emporiumpedidovenda
	 * @author Rodrigo Freitas
	 * @since 23/04/2013
	 */
	public void updateProcessado(Emporiumpedidovenda emporiumpedidovenda) {
		getHibernateTemplate().bulkUpdate("update Emporiumpedidovenda e set e.processado = ? where e.id = ?", new Object[]{
			Boolean.TRUE, emporiumpedidovenda.getCdemporiumpedidovenda()
		});
	}

	/**
	 * Atualiza o arquivo do pedido de venda
	 *
	 * @param emporiumpedidovenda
	 * @author Rodrigo Freitas
	 * @since 23/04/2013
	 */
	public void updateArquivo(Emporiumpedidovenda emporiumpedidovenda) {
		getHibernateTemplate().bulkUpdate("update Emporiumpedidovenda e set e.arquivo = ? where e.id = ?", new Object[]{
			emporiumpedidovenda.getArquivo(), emporiumpedidovenda.getCdemporiumpedidovenda()
		});
	}

	/**
	 * Verifica se existe o pedido de venda na tabela do ECF
	 *
	 * @param pedidovenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/04/2013
	 */
	public boolean havePedidovendaEnviado(String whereIn) {
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Emporiumpedidovenda.class)
				.join("emporiumpedidovenda.pedidovenda pedidovenda")
				.whereIn("pedidovenda.cdpedidovenda", whereIn)
				.unique() > 0;
	}

	public List<Emporiumpedidovenda> findForWebserviceGET() {
		return query()
					.select("emporiumpedidovenda.cdemporiumpedidovenda, arquivo.cdarquivo, arquivo.nome")
					.join("emporiumpedidovenda.arquivo arquivo")
					.where("emporiumpedidovenda.enviadoemporium = ?", Boolean.FALSE)
					.list();
	}

	public void updateEnviadoemporium(Integer id) {
		getHibernateTemplate().bulkUpdate("update Emporiumpedidovenda e set e.enviadoemporium = ? where e.id = ?", new Object[]{
				Boolean.TRUE, id
		});
	}
	
	public Emporiumpedidovenda findByPedidovenda(Pedidovenda pedidovenda){
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido. Pedido de venda n�o pode ser nulo.");
			
		QueryBuilder<Emporiumpedidovenda> query = query();
		updateEntradaQuery(query);
		query.where("emporiumpedidovenda.pedidovenda = ?", pedidovenda);
		query.setMaxResults(1);
		
		return query.unique(); 
	}
	
	public Money loadValorTotalValeCompraEmporiumByPedidovenda(Pedidovenda pedidovenda){
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null){
			throw new SinedException("Pedido de venda n�o pode ser nulo.");
		}
		
		StringBuilder sql = new StringBuilder();
		
		sql
			.append(" select sum(coalesce(epvi.valorvalecompra, 0)) as valor_total_vale_compra")
			.append(" from emporiumpedidovendaitem epvi ")
			.append(" where epvi.cdemporiumpedidovenda = (select max(epv.cdemporiumpedidovenda) from emporiumpedidovenda epv where epv.cdpedidovenda = " + pedidovenda.getCdpedidovenda() + ") "); 

		SinedUtil.markAsReader();
		Money valorTotalValeCompraEmporium = (Money)getJdbcTemplate().queryForObject(sql.toString(), 
		new RowMapper() {
			public Money mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Money(new Money(rs.getDouble("valor_total_vale_compra"), false));
			}
		});
		
		return valorTotalValeCompraEmporium != null ? valorTotalValeCompraEmporium : new Money();
	}
	
}