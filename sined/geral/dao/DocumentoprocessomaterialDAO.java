package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Documentoprocesso;
import br.com.linkcom.sined.geral.bean.Documentoprocessomaterial;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentoprocessomaterialDAO extends GenericDAO<Documentoprocessomaterial>{

	/**
	 * M�todo que busca os materiais do documento/processo
	 *
	 * @param documentoprocesso
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentoprocessomaterial> findByDocumentoprocesso(Documentoprocesso documentoprocesso) {
		if(documentoprocesso == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
					.select("documentoprocessomaterial.cddocumentoprocessomaterial, documentoprocesso.cddocumentoprocesso")
					.join("documentoprocessomaterial.documentoprocesso documentoprocesso")
					.where("documentoprocesso = ?", documentoprocesso)
					.list();
	}
}
