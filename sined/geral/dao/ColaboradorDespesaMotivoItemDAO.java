package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.ColaboradorDespesaMotivoItem;
import br.com.linkcom.sined.geral.bean.Colaboradordespesamotivo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColaboradorDespesaMotivoItemDAO extends GenericDAO<ColaboradorDespesaMotivoItem>{

	public List<ColaboradorDespesaMotivoItem> findByMotivo(Colaboradordespesamotivo motivo, Empresa empresa){
		return query()
				.select("colaboradorDespesaMotivoItem.cdColaboradorDespesaMotivoItem, " +
						"empresa.cdpessoa, empresa.nome, empresa.nomefantasia, " +
						"contaGerencial.cdcontagerencial")
				.leftOuterJoin("colaboradorDespesaMotivoItem.contaGerencial contaGerencial")
				.leftOuterJoin("colaboradorDespesaMotivoItem.empresa empresa")
				.where("empresa = ?", empresa)
				.where("colaboradorDespesaMotivoItem.colaboradorDespesaMotivo = ?", motivo)
				.list();
	}
}
