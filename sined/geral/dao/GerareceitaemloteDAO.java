package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.FaturamentocontratoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class GerareceitaemloteDAO extends GenericDAO<Contrato> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Contrato> query, FiltroListagem _filtro) {
	
		FaturamentocontratoFiltro filtro = (FaturamentocontratoFiltro) _filtro;		
		query
		.select("contrato.cdcontrato")
		.join("notacontrato.listaNotacontrato listaNotacontrato")
		.join("listaNotacontrato.nota nota")
		.join("nota.notaStatus notaStatus")
		.where("contrato.dtinicio <= ?", filtro.getDtinicio())
		.where("contrato.dtfim >= ?", filtro.getDtinicio())
		.where("contrato.dtinicio <= ?", filtro.getDtfim())
		.where("contrato.dtfim <= ?", filtro.getDtfim())
		.whereIn("notaStatus", NotaStatus.EMITIDA.getCdNotaStatus()+","+NotaStatus.NFSE_EMITIDA.getCdNotaStatus()+","+NotaStatus.NFE_EMITIDA.getCdNotaStatus())
		;
	}
	
}