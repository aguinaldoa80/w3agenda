package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Usuariopapel;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("usuariopapel.papel.nome")
public class UsuariopapelDAO extends GenericDAO<Usuariopapel> {

	@Override
	public void updateListagemQuery(QueryBuilder<Usuariopapel> query, FiltroListagem filtro) {
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Usuariopapel> query) {
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
	}


	/* singleton */
	private static UsuariopapelDAO instance;
	public static UsuariopapelDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(UsuariopapelDAO.class);
		}
		return instance;
	}

	public List<Usuariopapel> findByUsuario(Usuario usuario) {
		return query()
		.where("usuariopapel.pessoa = ?", usuario)
		.list();
	}

	public List<Usuariopapel> findByPapeis(List<Papel> listaPapel) {
		return query()
		.whereIn("usuariopapel.papel", CollectionsUtil.listAndConcatenate(listaPapel, "cdpapel", ",") )
		.where("usuariopapel.pessoa = ?", SinedUtil.getUsuarioLogado())
		.list();
	}
	
	public List<Usuariopapel> findByPapeisTodos(List<Papel> listaPapel) {
		return query()
			.select("usuariopapel.cdusuariopapel, papel.cdpapel, pessoa.cdpessoa, pessoa.todosprojetos, projeto.cdprojeto, empresa.cdpessoa ")
			.join("usuariopapel.papel papel")
			.join("usuariopapel.pessoa pessoa")
			.leftOuterJoin("pessoa.listaUsuarioprojeto listaUsuarioprojeto")
			.leftOuterJoin("listaUsuarioprojeto.projeto projeto")
			.leftOuterJoin("pessoa.listaUsuarioempresa listaUsuarioempresa")
			.leftOuterJoin("listaUsuarioempresa.empresa empresa")
			.whereIn("papel", CollectionsUtil.listAndConcatenate(listaPapel, "cdpapel", ","))
			.list();
	}
}