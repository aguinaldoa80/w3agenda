package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Servicoweb;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.ServicowebFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ServicowebDAO extends GenericDAO<Servicoweb>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Servicoweb> query, FiltroListagem _filtro) {
		ServicowebFiltro filtro = (ServicowebFiltro)_filtro;
		query.select("servicoweb.cdservicoweb, contrato.descricao, contrato.cliente")
			 .leftOuterJoin("servicoweb.contratomaterial contratomaterial")
		 	 .leftOuterJoin("contratomaterial.contrato contrato")
		 	 .leftOuterJoin("servicoweb.servicoftp servicoftp")		 	 
		 	 .where("contrato.cliente = ?", filtro.getCliente())
			 .where("servicoweb.contratomaterial = ?", filtro.getContratomaterial())
			 .where("servicoweb.servicoftp = ?", filtro.getServicoftp());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Servicoweb> query) {
		query.leftOuterJoinFetch("servicoweb.contratomaterial contratomaterial")
		 	 .leftOuterJoinFetch("contratomaterial.contrato contrato")
		 	 .leftOuterJoinFetch("servicoweb.servicoftp servicoftp")
		 	 .leftOuterJoinFetch("servicoweb.listaServicowebalias listaServicowebalias")
			 .leftOuterJoinFetch("listaServicowebalias.dominiozona dominiozona")
			 .leftOuterJoinFetch("dominiozona.dominio dominio");
	}
		
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaServicowebalias");
	}

	public int countContratomaterial(Integer cdservicoweb, Contratomaterial contratomaterial) {
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.from(Servicoweb.class)
					.where("servicoweb.cdservicoweb <> ?", cdservicoweb)
					.where("servicoweb.contratomaterial = ?", contratomaterial)
					.unique()
					.intValue();
	}
}
