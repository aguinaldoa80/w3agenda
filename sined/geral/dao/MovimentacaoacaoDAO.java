package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("movimentacaoacao.nome")
public class MovimentacaoacaoDAO extends GenericDAO<Movimentacaoacao>{

	@Override
	public List<Movimentacaoacao> findForCombo(String... extraFields) {
		return 
			query()
			.select("movimentacaoacao.cdmovimentacaoacao, movimentacaoacao.nome")
			.where("movimentacaoacao.mostramovimentacao = true")
			.orderBy("movimentacaoacao.nome")
			.list();
	}
}
