package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Configuracaognre;
import br.com.linkcom.sined.geral.bean.Configuracaognreuf;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ConfiguracaognreufDAO extends GenericDAO<Configuracaognreuf> {

	public List<Configuracaognreuf> findByConfiguracaognre(Configuracaognre configuracaognre) {
		if(configuracaognre == null || configuracaognre.getCdconfiguracaognre() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
				.select("configuracaognreuf.cdconfiguracaognreuf, configuracaognreuf.versao, " +
						"configuracaognreuf.cdconfiguracaognreuf, configuracaognreuf.chaveacesso, configuracaognreuf.codigochaveacesso, " +
						"configuracaognreuf.dtsaida, configuracaognreuf.codigodtsaida, configuracaognreuf.dtemissao, configuracaognreuf.codigodtemissao, " +
						"configuracaognreuf.infocomplementar, configuracaognreuf.codigoinfocomplementar, configuracaognreuf.codigodetalhamento, " +
						"configuracaognreuf.considerardtvencimento, configuracaognreuf.diasdtvencimento, configuracaognreuf.considerardtpagamento, " +
						"configuracaognreuf.diasdtpagamento, configuracaognreuf.codigoperiodoreferencia, configuracaognreuf.documentoorigem, " +
						"configuracaognreuf.codigodocumentoorigem, configuracaognreuf.documentoorigemtipo, configuracaognreuf.destinatario, " +
						"configuracaognreuf.iedestinatario, configuracaognreuf.ieremetente, configuracaognreuf.infocomplementar2, " +
						"configuracaognreuf.codigoinfocomplementar2, configuracaognreuf.maximoinfocomplementar, configuracaognreuf.maximoinfocomplementar2, " +
						"configuracaognreuf.referencia, configuracaognreuf.produto, configuracaognreuf.convenio, " +
						"uf.cduf, uf.nome, uf.sigla")
				.leftOuterJoin("configuracaognreuf.uf uf")
				.where("configuracaognreuf.configuracaognre = ?", configuracaognre)
				.orderBy("uf.sigla")
				.list();
	}

}
