package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.MdfeCte;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MdfeCteDAO extends GenericDAO<MdfeCte>{

	
	public List<MdfeCte> findByMdfe(Mdfe mdfe){
		if(mdfe==null){
			return new ArrayList<MdfeCte>();
		}
		return query()
			.select("mdfeCte.cdMdfeCte, mdfeCte.identificadorCte, mdfeCte.chaveAcesso, mdfeCte.codigoBarra, mdfeCte.indicadorReentrega, "+
					"municipio.cdmunicipio, municipio.nome, municipio.cdibge, uf.cduf, uf.sigla, uf.nome")
			.leftOuterJoin("mdfeCte.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.where("mdfeCte.mdfe = ?", mdfe)
			.list();
	}
}
