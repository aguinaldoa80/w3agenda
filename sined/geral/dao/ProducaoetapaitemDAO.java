package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Producaoetapa;
import br.com.linkcom.sined.geral.bean.Producaoetapaitem;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoetapaitemDAO extends GenericDAO<Producaoetapaitem> {

	/**
	 * Busca pelo Producaoetapa
	 *
	 * @param producaoetapa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/03/2014
	 */
	public List<Producaoetapaitem> findByProducaoetapa(Producaoetapa producaoetapa) {
		return query()
					.select("producaoetapaitem.cdproducaoetapaitem, producaoetapanome.cdproducaoetapanome, producaoetapanome.nome, producaoetapaitem.ordem, " +
							"producaoetapaitem.qtdediasprevisto, producaoetapaitem.percentualComissao, producaoetapa.cdproducaoetapa, " +
							"departamento.cddepartamento, departamento.nome")
					.leftOuterJoin("producaoetapaitem.departamento departamento")
					.leftOuterJoin("producaoetapaitem.producaoetapanome producaoetapanome")
					.join("producaoetapaitem.producaoetapa producaoetapa")
					.where("producaoetapa = ?", producaoetapa)
					.list();
	}
	
	/**
	 * Busca a lista de producaoetapaitem para enviar ao W3Producao
	 * @param whereIn
	 * @return
	 */
	public List<Producaoetapaitem> findForW3Producao(String whereIn){
		return query()
					.select("producaoetapaitem.cdproducaoetapaitem, producaoetapanome.cdproducaoetapanome, producaoetapanome.nome, producaoetapaitem.ordem, producaoetapa.cdproducaoetapa")
					.leftOuterJoin("producaoetapaitem.producaoetapa producaoetapa")
					.leftOuterJoin("producaoetapaitem.producaoetapanome producaoetapanome")
					.whereIn("producaoetapaitem.cdproducaoetapaitem", whereIn)
					.list();
				
	}

	public Producaoetapaitem loadForProducao(Producaoetapaitem producaoetapaitem) {
		if(producaoetapaitem == null || producaoetapaitem.getCdproducaoetapaitem() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("producaoetapaitem.cdproducaoetapaitem, producaoetapanome.cdproducaoetapanome, producaoetapanome.nome, producaoetapaitem.ordem, " +
						"producaoetapaitem.qtdediasprevisto, producaoetapaitem.percentualComissao, producaoetapa.cdproducaoetapa ")
				.join("producaoetapaitem.producaoetapa producaoetapa")
				.leftOuterJoin("producaoetapaitem.producaoetapanome producaoetapanome")
				.where("producaoetapaitem = ?", producaoetapaitem)
				.unique();
	}
}
