package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.QueryBuilder.Where;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialfornecedor;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialfornecedorDAO extends GenericDAO<Materialfornecedor> {

	public List<Materialfornecedor> findByMaterial(Material form) {
		if (form == null || form.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		return query()
					.select("materialfornecedor.cdmaterialfornecedor, materialfornecedor.codigo, materialfornecedor.tempoentrega, " +
							"materialfornecedor.fabricante, materialfornecedor.principal, fornecedor.cdpessoa, fornecedor.nome, fornecedor.valorTicketMedio ")
					.leftOuterJoin("materialfornecedor.fornecedor fornecedor")
					.where("materialfornecedor.material = ?", form)
					.list();
	}
	
	public boolean haveMaterialFornecedor(Material material, Fornecedor fornecedor, String cprod) {
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.from(Materialfornecedor.class)
					.setUseTranslator(false)
					.where("materialfornecedor.material = ?", material)
					.where("materialfornecedor.fornecedor = ?", fornecedor)
					.where("materialfornecedor.codigo = ?", cprod)
					.unique() > 0;
	}
	
	
	
	public List<Materialfornecedor> findCodigoAlternativo(Material material, Fornecedor fornecedor) {
		if (material == null || material.getCdmaterial() == null || fornecedor == null || fornecedor.getCdpessoa() == null) {
			throw new SinedException("Material ou Forncedor n�o podem ser nulos.");
		}	
		return query()
				.select("materialfornecedor.cdmaterialfornecedor, materialfornecedor.codigo")				
				.where("materialfornecedor.material = ?", material)
				.where("materialfornecedor.fornecedor = ?", fornecedor)
				.list();
				
		
	}

	
	
	
	
	
}
