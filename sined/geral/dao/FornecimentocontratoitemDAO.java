package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Fornecimentocontratoitem;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FornecimentocontratoitemDAO extends GenericDAO<Fornecimentocontratoitem> {

	/**
	 * Busca as informações para a listagem do CSV.
	 *
	 * @param filtro
	 * @return
	 * @author Thiers Euller
	 */
	public List<Fornecimentocontratoitem> findForCSV(String whereIn){
		return query()
		.select("fornecimentocontratoitem.cdfornecimentocontratoitem, fornecimentocontratoitem.descricao")
		.leftOuterJoin("fornecimentocontratoitem.fornecimentocontrato fornecimentocontrato")
		.whereIn("fornecimentocontrato.cdfornecimentocontrato", whereIn)
		.list();
	}
}
