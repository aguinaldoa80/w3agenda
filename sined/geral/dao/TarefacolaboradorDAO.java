package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Tarefacolaborador;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TarefacolaboradorDAO extends GenericDAO<Tarefacolaborador>{

	/**
	* M�todo que retorna os colaboradores cadastrados na tarefa 
	*
	* @param whereIn
	* @param whereInColaborador
	* @return
	* @since Aug 8, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Tarefacolaborador> findColaboradorNaTarefa(String whereIn, String whereInColaborador) {
		if(whereIn == null || whereIn.equals("") || whereInColaborador == null || whereInColaborador.equals(""))
			throw new SinedException("Par�metros inv�lidos.");

		return query()
			.select("tarefacolaborador.cdtarefacolaborador, tarefa.cdtarefa, colaborador.cdpessoa, colaborador.nome")
			.join("tarefacolaborador.tarefa tarefa")
			.join("tarefacolaborador.colaborador colaborador")
			.whereIn("colaborador.cdpessoa", whereInColaborador)
			.whereIn("tarefa.cdtarefa", whereIn)
			.list();
	}

	/**
	* M�todo que verifica se existe colaborador exclusivo no mesmo per�odo
	*
	* @param tarefa
	* @param dtinicio
	* @param colaborador
	* @param exclusivo
	* @return
	* @since 12/03/2015
	* @author Luiz Fernando
	*/
	public boolean existeColaboradorExclusivoAlocadoPeriodo(Tarefa tarefa, Date dtinicio, Colaborador colaborador, boolean exclusivo) {
		if(colaborador == null || colaborador.getCdpessoa() == null || dtinicio == null)
			throw new SinedException("Colaborador n�o pode ser nulo");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Tarefacolaborador.class)
			.join("tarefacolaborador.tarefa tarefa")
			.join("tarefacolaborador.colaborador colaborador")
			.where("tarefa <> ?", tarefa, tarefa != null && tarefa.getCdtarefa() != null)
			.where("colaborador = ?", colaborador)
			.where("coalesce(tarefacolaborador.exclusivo, false) = true", !exclusivo)
			.where("tarefa.dtfim >= ?", dtinicio)
			.unique() > 0;
	}
}
