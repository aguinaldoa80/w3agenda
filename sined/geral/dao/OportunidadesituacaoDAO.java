package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Oportunidadesituacao;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.OportunidadesituacaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("oportunidadesituacao.nome")
public class OportunidadesituacaoDAO extends GenericDAO<Oportunidadesituacao>{
	
	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}

	/**
	 * Carrega a combo incluindo a situa��o atual e as situa��es futuras.
	 * Elimina a possibilidade de selecionar situa��es passadas.
	 * @author Taidson Santos
	 * @since 30/03/2010
	 * @param filtro
	 */
	public List<Oportunidadesituacao> populaComboSituacao(Oportunidade oportunidade){
		return query()
		.select("oportunidadesituacao.cdoportunidadesituacao, oportunidadesituacao.nome ")
		//.where("oportunidadesituacao.cdoportunidadesituacao >= ?", oportunidade.getOportunidadesituacao().getCdoportunidadesituacao())
		.orderBy("oportunidadesituacao.nome")
		.list();
		
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				Oportunidadesituacao oportunidadesituacao = (Oportunidadesituacao) save.getEntity();
				
				if(oportunidadesituacao.getImagem() != null)
					arquivoDAO.saveFile(oportunidadesituacao, "imagem");
				
				return null;
			}			
		});		
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Oportunidadesituacao> query) {
		query.leftOuterJoinFetch("oportunidadesituacao.imagem imagem");
		super.updateEntradaQuery(query);
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Oportunidadesituacao> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		
		OportunidadesituacaoFiltro filtro = (OportunidadesituacaoFiltro)_filtro;
		
		query.leftOuterJoinFetch("oportunidadesituacao.imagem imagem")
			 .whereLikeIgnoreAll("oportunidadesituacao.nome", filtro.getNome());
		query.orderBy("oportunidadesituacao.nome");
		super.updateListagemQuery(query, _filtro);
	}

	/**
	* Carrega o �cone da situa��o
	*
	* @param oportunidadesituacao
	* @return
	* @since Sep 23, 2011
	* @author Luiz Fernando F Silva
	*/
	public Oportunidadesituacao carregaImagemarquivo(Oportunidadesituacao oportunidadesituacao) {
		if(oportunidadesituacao == null || oportunidadesituacao.getCdoportunidadesituacao() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.leftOuterJoinFetch("oportunidadesituacao.imagem imagem")
				.where("oportunidadesituacao = ?", oportunidadesituacao)
				.unique();
				
	}

	/**
	* Busca as situa��es marcadas para aparecer na listagem
	*
	* @return
	* @since Sep 23, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Oportunidadesituacao> findDefaultlistagem() {
		return query()
					.select("oportunidadesituacao.cdoportunidadesituacao, oportunidadesituacao.defaultlistagem")
					.where("oportunidadesituacao.defaultlistagem = ?", Boolean.TRUE)
					.list();
	}

	/**
	* Busca a situa��o marcada como final
	*
	* @return
	* @since Sep 23, 2011
	* @author Luiz Fernando F Silva
	*/
	public Oportunidadesituacao findSituacaofinal() {
		return query()
				.where("oportunidadesituacao.situacaofinal = ?", Boolean.TRUE)
				.unique();
	}

	/**
	* Busca a situa��o marcada como antes da final
	*
	* @return
	* @since Sep 23, 2011
	* @author Luiz Fernando F Silva
	*/
	public Oportunidadesituacao findSituacaoantesfinal() {
		return query()
				.where("oportunidadesituacao.situacaoantesfinal = ?", Boolean.TRUE)
				.unique();
	}
	
	/**
	* Busca a situa��o marcada como inicial
	*
	* @return
	* @since Sep 23, 2011
	* @author Luiz Fernando F Silva
	*/
	public Oportunidadesituacao findSituacaoinicial() {
		return query()
				.where("oportunidadesituacao.situacaoinicial = ?", Boolean.TRUE)
				.unique();
	}

	/**
	* Busca a situa��o marcada como canelada
	*
	* @return
	* @since Sep 23, 2011
	* @author Luiz Fernando F Silva
	*/
	public Oportunidadesituacao findSituacaocancelada() {
		return query()
				.where("oportunidadesituacao.situacaocancelada = ?", Boolean.TRUE)
				.unique();
	}
	
	/**
	 * M�todo que verifica se existe uma oportunidadesituacao j� cadastrada de acordo com a situa��o passada como par�metro.
	 *
	 * @see
	 *
	 * @param cdoportunidadesituacao
	 * @param situacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existeOportunidadesituacao(Integer cdoportunidadesituacao, String situacao){
		if(situacao == null || "".equals(situacao))
			throw new SinedException("Par�metro inv�lido.");
		
		if("situacaoinicial".equalsIgnoreCase(situacao)){
			return query()
					.where("oportunidadesituacao.cdoportunidadesituacao <> ?", cdoportunidadesituacao)
					.where("oportunidadesituacao.situacaoinicial = true")
					.list().size() > 0;
		}else if("situacaocancelada".equalsIgnoreCase(situacao)){
			return query()
			.where("oportunidadesituacao.cdoportunidadesituacao <> ?", cdoportunidadesituacao)
			.where("oportunidadesituacao.situacaocancelada = true")
			.list().size() > 0;
		}else if("situacaoantesfinal".equalsIgnoreCase(situacao)){
			return query()
			.where("oportunidadesituacao.cdoportunidadesituacao <> ?", cdoportunidadesituacao)
			.where("oportunidadesituacao.situacaoantesfinal = true")
			.list().size() > 0;
		}else if("situacaofinal".equalsIgnoreCase(situacao)){
			return query()
			.where("oportunidadesituacao.cdoportunidadesituacao <> ?", cdoportunidadesituacao)
			.where("oportunidadesituacao.situacaofinal = true")
			.list().size() > 0;
		}else {
			throw new SinedException("Par�metro inv�lido.");
		}
	}	
}
