package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Licenca;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class LicencaDAO extends GenericDAO<Licenca> {
	
	@Override
	public List<Licenca> findAll() {
		return query().orderBy("cdlicenca")
					  .list();
	}
	
	/**
	 * Limpa a tabela
	 * @author Thiago Gon�alves
	 */
	public void deleteAll(){
		getHibernateTemplate().bulkUpdate("delete from Licenca");
	}
	
}
