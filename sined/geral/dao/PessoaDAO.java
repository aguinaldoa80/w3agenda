package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;


import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ParticipanteSpedBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PessoaDAO extends GenericDAO<Pessoa> {
		
	/* singleton */
	private static PessoaDAO instance;
	public static PessoaDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(PessoaDAO.class);
		}
		return instance;
	}
	
	/**
	 * M�todo para carregar dados de pessoa.
	 * 
	 * @param bean
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Pessoa carregaPessoa(Pessoa bean){
		if(!SinedUtil.isObjectValid(bean)){
			throw new SinedException("Par�metro pessoa inv�lido.");
		}
		
		return
			query()
			.select("pessoa.cdpessoa, pessoa.nome, pessoa.cnpj, pessoa.cpf, " +
					"dadobancario.cddadobancario, dadobancario.agencia, dadobancario.conta, dadobancario.dvagencia, dadobancario.dvconta," +
					"banco.cdbanco, banco.nome, banco.numero," +
					"endereco.cdendereco, endereco.cep, endereco.complemento, endereco.numero, endereco.logradouro," +
					"enderecotipo.cdenderecotipo, " +
					"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.nome, uf.sigla, endereco.bairro, pessoa.tipopessoa")
					
			.leftOuterJoin("pessoa.listaDadobancario dadobancario")
			.leftOuterJoin("dadobancario.banco banco")
			
			.leftOuterJoin("pessoa.listaEndereco endereco")
			.leftOuterJoin("endereco.enderecotipo enderecotipo")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			
			.where("pessoa = ?", bean)
			.unique();
		
	}
	
	/**
	 * M�todo para carregar os dados de pessoa.
	 * 
	 * @param pessoa
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Pessoa carregaDadosPessoa(Pessoa pessoa){
		if(pessoa == null || pessoa.getCdpessoa() == null){
			throw new SinedException("Os par�metros pessoa ou cdpessoa n�o podem ser null.");
		}
		return 
			query()
			.select("pessoa.cdpessoa, pessoa.email")
			.where("pessoa = ?",pessoa)
			.unique();		
	}
	
	/**
	 * M�todo para carregar o email de pessoa.
	 * 
	 * @param pessoa
	 * @return
	 * @author Rafael Salvio
	 */
	public Pessoa carregaEmailPessoa(Pessoa pessoa){
		if(pessoa == null || pessoa.getCdpessoa() == null){
			throw new SinedException("Os par�metros pessoa ou cdpessoa n�o podem ser null.");
		}
		return 
			query()
			.select("pessoa.email")
			.where("pessoa.cdpessoa = ?",pessoa.getCdpessoa())
			.unique();		
	}
	
	/**
	 * M�todo para localiza��o de cadastro pr�vio de pessoa.
	 * 
	 * @throws SinedException - Se o par�metro informado for null
	 * @param cpf
	 * @return null se n�o encontrar
	 * @author Jo�o Paulo Zica
	 */
	public Pessoa findPessoaByCpf(Cpf cpf) {
		if (cpf == null) {
			throw new SinedException("Par�metro CPF n�o pode ser null.");
		}
		return
			query()
				.select("pessoa.cdpessoa, pessoa.nome, pessoa.cpf, pessoa.email")
				.where("pessoa.cpf = ?", cpf)
				.setMaxResults(1)
				.unique();
	}
	
	/**
	 * M�todo para obter uma pessoa pelo CNPJ.
	 * 
	 * @throws SinedException - Se o par�metro informado for null.
	 * @param cnpj
	 * @return 
	 * @author Flavio
	 */
	public Pessoa findPessoaByCnpj(Cnpj cnpj){
		if(cnpj==null){
			throw new SinedException("O par�metro cnpj n�o pode ser null.");
		}
		return
			query()
				.select("pessoa.cdpessoa, pessoa.nome, pessoa.cnpj, pessoa.email")
				.where("pessoa.cnpj = ?",cnpj)
				.setMaxResults(1)
				.unique();
	}
	
	/**
	 * M�todo para localiza��o de pessao atrav�s de um c�digo v�lido
	 * 
	 * @param cod
	 * @return Pessoa
	 * @author Jo�o Paulo Zica
	 */
	public Pessoa findPessoaByCodigo(Integer cod) {
		if (cod == null) {
			throw new SinedException("Par�metro C�digo n�o pode ser null.");
		}
		return
			query()
				.select("pessoa.cdpessoa, pessoa.nome")
				.where("pessoa.cdpessoa = ?", cod)
				.setMaxResults(1)
				.unique();
	}

	@Override
	public void saveOrUpdate(Pessoa bean) {
		if (bean instanceof Cliente) {
			super.saveOrUpdate((Cliente)bean);
		} else if(bean instanceof Colaborador){
			super.saveOrUpdate((Colaborador)bean);
		} else if(bean instanceof Fornecedor){
			super.saveOrUpdate((Fornecedor)bean);
		} else if(bean instanceof Usuario){
			super.saveOrUpdate((Usuario)bean);
		} else {
			throw new SinedException("Dados incorretos.");
		}
		
		super.saveOrUpdate(bean);
	}

	/**
	 * Informa se o CPF j� est� cadastrado.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PessoaDAO#cpfJaCadastrado(Cpf)
	 * @param cpf
	 * @return <i>true<i> se o CPF j� estiver cadastrado
	 * @return <i>false</i> se o CPF ainda n�o estiver cadastrado
	 * @author Hugo Ferreira
	 */
	public Boolean cpfJaCadastrado(Cpf cpf) {
		return newQueryBuilderWithFrom(Long.class)
			.select("cont(*)")
			.where("pessoa.cpf = ", cpf.getValue())
			.setUseTranslator(false)
			.unique() > 0;
	}
	
	
	/**
	 * Retorna as pessoas
	 * que tem e-mail
	 * @param nome
	 * @return
	 * @author C�ntia Nogueira
	 */
	public List<Pessoa> findWithEmail(String nome){
		
		return query()
			  .select("pessoa.cdpessoa, pessoa.nome, pessoa.email")
			  .whereLikeIgnoreAll("pessoa.nome", nome)
			  .where("pessoa.email is not null")
			  .orderBy("pessoa.nome asc")
			  .list();
	}

	/**
	 * Atualiza o email da pessoa passada por par�metro.
	 *
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void updateEmail(Pessoa bean) {
		if(bean.getCdpessoa() == null || bean.getEmail() == null){
			throw new SinedException("Usu�rio e seu e-mail n�o podem ser nulos.");
		}
//		getHibernateTemplate().bulkUpdate("update Pessoa p set p.email = ? where p.id = ?", new Object[]{bean.getEmail(), bean.getCdpessoa()});
		getJdbcTemplate().execute("UPDATE PESSOA SET EMAIL = '"+bean.getEmail()+"' WHERE CDPESSOA = " + bean.getCdpessoa());
	}

	/**
	 * Busca os participantes para o registro 0150 do SPED.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<ParticipanteSpedBean> findForSpedReg0150(String whereIn) {
		String sql = 	"SELECT p.cdpessoa AS cdpessoa, " +
						"COALESCE(f.razaosocial, c.razaosocial, p.nome) AS nome, " +
						"p.cnpj AS cnpj,  " +
						"p.cpf AS cpf,  " +
						"COALESCE(f.inscricaoestadual, c.inscricaoestadual) AS ie,  " +
						"m.cdibge AS cod_municipio,  " +
						"null AS cdendereco, " +
						"e.logradouro AS ender,  " +
						"e.numero AS num,  " +
						"e.complemento AS compl,  " +
						"e.bairro AS bairro, " +
						"pa.nome as nomepais, " +
						"pa.cdbacen as cdbacenpais " +
						
						"FROM pessoa p " +
						
						"LEFT OUTER JOIN fornecedor f ON f.cdpessoa = p.cdpessoa " +
						"LEFT OUTER JOIN cliente c ON c.cdpessoa = p.cdpessoa " +
						"LEFT OUTER JOIN endereco e ON e.cdpessoa = p.cdpessoa " +
						"LEFT OUTER JOIN municipio m ON m.cdmunicipio = e.cdmunicipio " +
						"LEFT OUTER JOIN pais pa ON pa.cdpais = e.cdpais " +
						
						"WHERE p.cdpessoa IN (" + whereIn + ") " +
						"AND (e.cdendereco = (SELECT e2.cdendereco " +
											"FROM endereco e2 " +
						                    "WHERE e2.cdpessoa = p.cdpessoa " +
						                    "ORDER BY e2.cdenderecotipo " +
						                    "LIMIT 1) OR e.cdendereco IS NULL) ";
		
		String sql2 = 	"SELECT p.cdpessoa AS cdpessoa, " +
						"COALESCE(f.razaosocial, c.razaosocial, p.nome) AS nome, " +
						"p.cnpj AS cnpj,  " +
						"p.cpf AS cpf,  " +
						"e.inscricaoestadual AS ie,  " +
						"m.cdibge AS cod_municipio,  " +
						"e.cdendereco AS cdendereco, " +
						"e.logradouro AS ender,  " +
						"e.numero AS num,  " +
						"e.complemento AS compl,  " +
						"e.bairro AS bairro, " +
						"pa.nome as nomepais, " +
						"pa.cdbacen as cdbacenpais " +
						
						"FROM pessoa p " +
						
						"LEFT OUTER JOIN fornecedor f ON f.cdpessoa = p.cdpessoa " +
						"LEFT OUTER JOIN cliente c ON c.cdpessoa = p.cdpessoa " +
						"LEFT OUTER JOIN endereco e ON e.cdpessoa = p.cdpessoa " +
						"LEFT OUTER JOIN municipio m ON m.cdmunicipio = e.cdmunicipio " +
						"LEFT OUTER JOIN pais pa ON pa.cdpais = e.cdpais " +
						
						"WHERE p.cdpessoa IN (" + whereIn + ") " +
						"AND e.inscricaoestadual is not null AND e.inscricaoestadual <> '' ";
		
		SinedUtil.markAsReader();
		List<ParticipanteSpedBean> lista = getJdbcTemplate().query(sql + " UNION ALL " + sql2, 
			new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					ParticipanteSpedBean bean = new ParticipanteSpedBean();
					
					bean.setCdpessoa(rs.getInt("cdpessoa"));
					if(rs.getObject("cdendereco") != null){
						bean.setCdendereco(rs.getInt("cdendereco"));
					}
					bean.setNome(rs.getString("nome"));
					bean.setCnpj(rs.getString("cnpj"));
					bean.setCpf(rs.getString("cpf"));
					bean.setIe(rs.getString("ie"));
					bean.setCod_municipio(rs.getString("cod_municipio"));
					bean.setEnd(rs.getString("ender"));
					bean.setNum(rs.getString("num"));
					bean.setCompl(rs.getString("compl"));
					bean.setBairro(rs.getString("bairro"));
					bean.setCod_pais(rs.getString("cdbacenpais"));
					
					String nomePais = rs.getString("nomepais");
					if(nomePais != null && !"BRASIL".equals(nomePais.toUpperCase())){
						bean.setCod_municipio("9999999");
					}
					return bean;
				}
			});
		
		return lista;
	}

	/**
	 * Busca a pessoa pelo nome.
	 * 
	 * @param nome
	 * @return
	 * @since 05/03/2012
	 * @author Rodrigo Freitas
	 */
	public Pessoa findPessoaByNome(String nome) {
		if(nome == null || nome.equals("")){
			throw new SinedException("Nome n�o pode ser nulo.");
		}
		List<Pessoa> lista = query()
					.select("pessoa.cdpessoa, pessoa.nome")
					.where("pessoa.nome = ?", nome)
					.list();
		
		if(lista == null || lista.size() == 0){
			return null;
		} else if(lista.size() > 1){
			throw new SinedException("Existem mais de uma pessoa com o nome: " + nome);
		} else return lista.get(0);
	}

	/**
	 * M�todo que carrega os dados da pessoa para o contato
	 * @param pessoa
	 * @return
	 */
	public Pessoa carregaPessoaForContato(Pessoa pessoa) {
		if(!SinedUtil.isObjectValid(pessoa)){
			throw new SinedException("Par�metro pessoa inv�lido.");
		}
		
		return
			query()
			.select("pessoa.cdpessoa, pessoa.nome, pessoa.cpf, pessoa.dtnascimento, pessoa.email," +
					"telefone.cdtelefone,telefone.telefone," +
					"telefonetipo.cdtelefonetipo, telefonetipo.nome," +
					"endereco.cdendereco, endereco.cep, endereco.complemento, endereco.numero, endereco.logradouro," +
					"enderecotipo.cdenderecotipo, enderecotipo.nome," +
					"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.nome, uf.sigla, endereco.bairro, pessoa.tipopessoa")
					
			.leftOuterJoin("pessoa.listaTelefone telefone")
			.leftOuterJoin("telefone.telefonetipo telefonetipo")
			
			.leftOuterJoin("pessoa.listaEndereco endereco")
			.leftOuterJoin("endereco.enderecotipo enderecotipo")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			
			.where("pessoa = ?", pessoa)
			.unique();
	}

	public String findNomeByCdpessoa(Integer cdpessoa) {
		return query()
		.select("pessoa.nome")
		.where("pessoa.cdpessoa = ?", cdpessoa)
		.unique().getNome();
	}
	
	/**
	* M�todo que carrega a pessoa com a lista de telefones
	*
	* @param pessoa
	* @return
	* @since 03/10/2016
	* @author Luiz Fernando
	*/
	public Pessoa loadWithTelefone(Pessoa pessoa) {
		if(!SinedUtil.isObjectValid(pessoa)){
			throw new SinedException("Par�metro pessoa inv�lido.");
		}
		
		return
			query()
			.select("pessoa.cdpessoa, pessoa.nome, " +
					"telefone.cdtelefone,telefone.telefone," +
					"telefonetipo.cdtelefonetipo, telefonetipo.nome")
			.leftOuterJoin("pessoa.listaTelefone telefone")
			.leftOuterJoin("telefone.telefonetipo telefonetipo")
			.where("pessoa = ?", pessoa)
			.unique();
	}
	
	public Pessoa loadWithEndereco(Pessoa pessoa) {
		return query().select("pessoa.cdpessoa, endereco.cdendereco, enderecotipo.cdenderecotipo")
				.leftOuterJoin("pessoa.listaEndereco endereco")
				.leftOuterJoin("endereco.enderecotipo enderecotipo")
				.where("pessoa = ?", pessoa).unique();
	}
	
	public List<Pessoa> findAutocompleteProcessoReferenciado(String nome, Integer nota, Integer entrada){
		if(nota == null && entrada == null)
			throw new SinedException("Codigo da nota ou entrada n�o pode ser nulo");
		QueryBuilder<Pessoa> query = query()
		.select("pessoa.cdpessoa, pessoa.nome")
		.whereLikeIgnoreAll("pessoa.nome", nome);
		if(nota !=null){
			query
			.where("pessoa.cdpessoa in (select c.cdpessoa from Notafiscalproduto nfp join nfp.cliente c where nfp.cdNota = ?)",nota);
		}
		if(entrada != null){
			query
			.where("pessoa.cdpessoa in (select f.cdpessoa  from Entregadocumento ed join ed.fornecedor f where ed.cdentregadocumento = ?)",entrada);
		}
		
return	query.list();
		
	}
}
