package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;

import br.com.linkcom.sined.geral.bean.Configuracaoecom;
import br.com.linkcom.sined.geral.bean.Ecommaterialestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EcommaterialestoqueDAO extends GenericDAO<Ecommaterialestoque>{

	/**
	 * Busca o primeiro registro da fila para sincroniza��o
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @param currentTimestamp 
	 * @since 18/07/2013
	 */
	public Ecommaterialestoque primeiroFilaSincronizacao(Timestamp currentTimestamp) {
		return query()
					.setMaxResults(1)
					.leftOuterJoinFetch("ecommaterialestoque.configuracaoecom configuracaoecom")
					.where("ecommaterialestoque.sincronizado = ?", Boolean.FALSE)
					.where("ecommaterialestoque.data <= ?", currentTimestamp)
					.where("ecommaterialestoque.identificador is not null")
					.orderBy("ecommaterialestoque.data")
					.unique();
	}
	
	/**
	* M�todo que carrega os dados do bean ecommaterialestoque
	*
	* @param ecommaterialestoque
	* @return
	* @since 24/04/2015
	* @author Luiz Fernando
	*/
	public Ecommaterialestoque loadEcommaterialestoque(Ecommaterialestoque ecommaterialestoque) {
		return query()
					.leftOuterJoinFetch("ecommaterialestoque.configuracaoecom configuracaoecom")
					.where("ecommaterialestoque = ?", ecommaterialestoque)
					.orderBy("ecommaterialestoque.data")
					.unique();
	}

	/**
	 * Atualiza a flag de sincronizado para TRUE
	 *
	 * @param ecommaterialestoque
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public void updateSincronizado(Ecommaterialestoque ecommaterialestoque) {
		getHibernateTemplate().bulkUpdate("update Ecommaterialestoque e set e.sincronizado = ? where e.id = ?", new Object[]{
				Boolean.TRUE, ecommaterialestoque.getCdecommaterialestoque()
		});
	}

	/**
	 * Atualiza a flag de sincronizado para FALSE e a data para a atual.
	 *
	 * @param ecommaterialestoque
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public void updateFinalFila(Ecommaterialestoque ecommaterialestoque) {
		getHibernateTemplate().bulkUpdate("update Ecommaterialestoque e set e.sincronizado = ?, e.data = ? where e.id = ?", new Object[]{
				Boolean.FALSE, new Timestamp(System.currentTimeMillis()), ecommaterialestoque.getCdecommaterialestoque()
		});
	}
	
	/**
	 * Atualiza o identificador do material na tabela auxiliar
	 *
	 * @param material
	 * @param identificador
	 * @author Rodrigo Freitas
	 * @since 30/07/2013
	 */
	public void updateIdentificador(Material material, String identificador, Configuracaoecom configuracaoecom) {
		getHibernateTemplate().bulkUpdate("update Ecommaterialestoque e set e.identificador = ? where e.material = ? and e.configuracaoecom = ?", new Object[]{
				identificador, material, configuracaoecom
		});
	}


}
