package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;

import br.com.linkcom.sined.geral.bean.Configuracaoecom;
import br.com.linkcom.sined.geral.bean.Ecommaterial;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EcommaterialDAO extends GenericDAO<Ecommaterial>{
	
	/**
	 * Verifica se existe algum dado a ser sincronizado.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public boolean haveDadosSincronizacao() {
		String sql = "SELECT SUM(contador) " +
						"FROM (" +
						"SELECT COUNT(e.cdecommaterial) AS contador FROM ecommaterial e WHERE e.sincronizado = FALSE" +
						" UNION ALL " +
						"SELECT COUNT(e2.cdecommaterialestoque) AS contador FROM ecommaterialestoque e2 WHERE e2.sincronizado = FALSE" +
						") QUERY";

		SinedUtil.markAsReader();
		Long count = getJdbcTemplate().queryForLong(sql);
		return count > 0;
	}

	/**
	 * Busca o primeiro registro de sincroniza��o.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @param currentTimestamp 
	 * @since 18/07/2013
	 */
	public Ecommaterial primeiroFilaSincronizacao(Timestamp currentTimestamp) {
		return query()
					.setMaxResults(1)
					.leftOuterJoinFetch("ecommaterial.configuracaoecom configuracaoecom")
					.leftOuterJoin("ecommaterial.ecommaterialestoque ecommaterialestoque")
					.where("ecommaterial.sincronizado = ?", Boolean.FALSE)
					.where("ecommaterial.data <= ?", currentTimestamp)
					.orderBy("ecommaterial.data")
					.unique();
	}
	
	/**
	* M�todo que carrega os dados do bean ecommaterial
	*
	* @param ecommaterial
	* @return
	* @since 23/04/2015
	* @author Luiz Fernando
	*/
	public Ecommaterial loadEcommaterial(Ecommaterial ecommaterial) {
		return query()
					.leftOuterJoinFetch("ecommaterial.configuracaoecom configuracaoecom")
					.leftOuterJoin("ecommaterial.ecommaterialestoque ecommaterialestoque")
					.where("ecommaterial = ?", ecommaterial)
					.orderBy("ecommaterial.data")
					.unique();
	}

	/**
	 * Atualiza a flag de sincronizado para TRUE
	 *
	 * @param ecommaterialestoque
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public void updateSincronizado(Ecommaterial ecommaterial) {
		getHibernateTemplate().bulkUpdate("update Ecommaterial e set e.sincronizado = ? where e.id = ?", new Object[]{
				Boolean.TRUE, ecommaterial.getCdecommaterial()
		});
	}

	/**
	 * Atualiza a flag de sincronizado para FALSE e a data para a atual.
	 *
	 * @param ecommaterialestoque
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public void updateFinalFila(Ecommaterial ecommaterial) {
		getHibernateTemplate().bulkUpdate("update Ecommaterial e set e.sincronizado = ?, e.data = ? where e.id = ?", new Object[]{
				Boolean.FALSE, new Timestamp(System.currentTimeMillis()), ecommaterial.getCdecommaterial()
		});
	}

	/**
	 * Atualiza o identificador
	 *
	 * @param ecommaterial
	 * @param identificador
	 * @author Rodrigo Freitas
	 * @param configuracaoecom 
	 * @since 18/07/2013
	 */
	public void updateIdentificador(Ecommaterial ecommaterial, String identificador, Configuracaoecom configuracaoecom) {
		getHibernateTemplate().bulkUpdate("update Ecommaterial e set e.identificador = ? where e.id = ? and e.configuracaoecom = ?", new Object[]{
				identificador, ecommaterial.getCdecommaterial(), configuracaoecom
		});
	}

}
