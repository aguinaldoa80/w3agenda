package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialempresa;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialempresaDAO extends GenericDAO<Materialempresa> {

	/**
	 * Carrega a lista de materialempresa a partir de um material.
	 * 
	 * @param form
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Materialempresa> findByMaterial(Material form) {
		if (form == null || form.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		return query()
					.select("materialempresa.cdmaterialempresa, empresa.cdpessoa, empresa.nomefantasia, " +
							"grupotributacao.cdgrupotributacao, grupotributacao.nome ")
					.join("materialempresa.empresa empresa")
					.leftOuterJoin("materialempresa.grupotributacao grupotributacao")
					.where("materialempresa.material = ?",form)
					.list();
	}

	public List<Materialempresa>findForAndroid(String whereIn){	
		return query()
				.select("materialempresa.cdmaterialempresa,empresa.cdpessoa,material.cdmaterial")
				.leftOuterJoin("materialempresa.empresa empresa")
				.leftOuterJoin("materialempresa.material material")
				.whereIn("materialempresa.cdmaterialempresa", whereIn)
				.list();
	}
	
}
