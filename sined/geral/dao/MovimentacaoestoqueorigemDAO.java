package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MovimentacaoestoqueorigemDAO extends GenericDAO<Movimentacaoestoqueorigem> {

	/**
	 * M�todo que verifica se j� existe alguma movimenta��o com a entrega de origem,
	 * pois se houver � porque esta sendo duplicado o registro.
	 * 
	 * @param entrega
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeMovEstoqueComOrigemEntrega(Entrega entrega) {
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Movimentacaoestoqueorigem.class)
			.join("movimentacaoestoqueorigem.entrega entrega")
			.setUseTranslator(false)
			.where("entrega = ?",entrega)
			.where("exists (select meo.cdmovimentacaoestoqueorigem from br.com.linkcom.sined.geral.bean.Movimentacaoestoque me " +
								" join me.movimentacaoestoqueorigem meo " +
								"where me.dtcancelamento is null and meo.cdmovimentacaoestoqueorigem = movimentacaoestoqueorigem.cdmovimentacaoestoqueorigem )")
			.unique() > 0;
	}
	
	/**
	* M�todo que verifica se existe movimenta��o de estoque com origem de requisi��o de material
	*
	* @param whereIn
	* @param somenteOrigemRequisicao
	* @return
	* @since 04/09/2015
	* @author Luiz Fernando
	*/
	public boolean existeMovEstoqueComOrigemRequisicaomaterial(String whereIn, Boolean somenteOrigemRequisicao) {
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Movimentacaoestoqueorigem.class)
			.setUseTranslator(false)
			.whereIn("movimentacaoestoqueorigem.requisicaomaterial.cdrequisicaomaterial", whereIn);
			
		if(somenteOrigemRequisicao == null || !somenteOrigemRequisicao){
			query
				.leftOuterJoin("movimentacaoestoqueorigem.romaneio romaneio")
				.where("romaneio is null");
		}
		
		return query.unique() > 0;
	}
}
