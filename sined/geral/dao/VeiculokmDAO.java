package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculokm;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculokmFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.AnaliticoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VeiculokmDAO extends GenericDAO<Veiculokm> {

	@Override
	public void updateListagemQuery(QueryBuilder<Veiculokm> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		VeiculokmFiltro filtro = (VeiculokmFiltro) _filtro;
		query
			.select("veiculokm.cdveiculokm, veiculo.cdveiculo, veiculo.placa, veiculokm.dtentrada, veiculokm.kmnovo, bempatrimonio.nome")
			.join("veiculokm.veiculo veiculo")
			.join("veiculo.patrimonioitem patrimonioitem")
			.join("patrimonioitem.bempatrimonio bempatrimonio")
			.whereLikeIgnoreAll("veiculo.placa", filtro.getPlaca())
			.whereLikeIgnoreAll("veiculo.prefixo", filtro.getPrefixo())
			.where("veiculo.veiculomodelo = ?", filtro.getVeiculomodelo())
			.where("veiculo = ?", filtro.getVeiculo())
			.where("veiculokm.tipokmajuste = ?", filtro.getTipokmajuste())
			.orderBy("veiculokm.cdveiculokm desc");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Veiculokm> query) {
		query
			.select("veiculokm.cdveiculokm, veiculokm.motivo, veiculo.cdveiculo, veiculokm.dtentrada, veiculokm.kmnovo, " +
					"tipokmajuste.cdtipokmajuste")
			.join("veiculokm.veiculo veiculo")
			.leftOuterJoin("veiculokm.tipokmajuste tipokmajuste");
	}

	/**
	 * M�todo que retorna �ltimo registro gravado de KM do ve�culo
	 * 
	 * @param veiculo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Veiculokm getKmAtualDoVeiculo(Veiculo veiculo) {
		return query()
					.select("veiculokm.kmnovo, veiculokm.cdveiculokm, veiculokm.dtentrada")
					.where("veiculokm.cdveiculokm = (select max(v.cdveiculokm) " +
													"from Veiculokm v " +
													"where v.veiculo = ? )", veiculo)
					.where("veiculokm.veiculo = ?", veiculo)
					.unique();
		
	}
	
	/**
	 * M�todo encontrar a quilometragem de um ve�culo
	 * 
	 * @author Rafael Odon
	 * @param veiculo
	 * @return Km
	 * @throws W3AutoException
	 */
	public Veiculokm findByVeiculo(Veiculo veiculo){
		if (veiculo == null || veiculo.getCdveiculo() == null){
			throw new SinedException("Ve�culo n�o pode ser nulo.");
		}
		return query()
			.select("veiculokm.cdveiculokm, veiculokm.dtentrada, veiculokm.kmnovo, veiculo.cdveiculo, vwveiculo.kmatual")
			.join("veiculokm.veiculo veiculo")
			.leftOuterJoin("veiculo.vwveiculo vwveiculo")
			.where("veiculo = ?",veiculo)
			.orderBy("veiculokm.cdveiculokm DESC, veiculokm.dtentrada DESC")
			.unique();
	}
	
	/**
	 * Buscar todos os ajustes dentro de um determinado periodo
	 * 
	 * @param AnaliticoFiltro
	 * @author Ramon Brazil
	 * @see br.com.linkcom.w3auto.geral.dao.AjusteDAO#findAnalitico(filtro)
	 * @return List<Ajuste>
	 */
	public List<Veiculokm> findAnalitico(AnaliticoFiltro filtro){
		if(filtro == null){
			throw new SinedException("O filtro n�o pode ser nulo");			
		}
		return querySined()
				
			.select("veiculokm.dtentrada, veiculokm.kmnovo, veiculokm.cdveiculokm, tipokmajuste.descricao,"+
					"veiculo.cdveiculo, veiculo.placa, veiculo.prefixo, veiculo.kminicial, colaborador.nome,"+
					"veiculomodelo.cdveiculomodelo, veiculomodelo.nome, vwveiculo.kmatual, vwveiculo.kmreal, vwveiculo.kmrodado")
			.leftOuterJoin("veiculokm.tipokmajuste tipokmajuste")
			.leftOuterJoin("veiculokm.veiculo veiculo")
			.leftOuterJoin("veiculo.vwveiculo vwveiculo")
			.leftOuterJoin("veiculo.veiculomodelo veiculomodelo")
			.leftOuterJoin("veiculo.colaborador colaborador")
			.where("veiculokm.dtentrada >=?",filtro.getDtinicio())
			.where("veiculokm.dtentrada <=?",filtro.getDtfim())
			.where("veiculomodelo =?",filtro.getModelo())
			.where("colaborador =?",filtro.getColaborador())
			.where("veiculo =? ",filtro.getVeiculo())
			.orderBy("veiculomodelo.nome, veiculo.placa, colaborador.nome, veiculokm.cdveiculokm, veiculokm.dtentrada")
			.list();
	}

	

	@SuppressWarnings("unchecked")
	public Integer getKmanterior(Veiculokm ajuste) {
		SinedUtil.markAsReader();
		List<Integer> listaUpdate = getJdbcTemplate().query("SELECT VKM.KMNOVO AS KMANTIGO " +
															"FROM VEICULOKM VKM " +
															"WHERE VKM.CDVEICULO = " + ajuste.getVeiculo().getCdveiculo() + " " +
															"AND VKM.CDVEICULOKM = (SELECT MAX(VEICULOKM.CDVEICULOKM) " +
															                         " FROM VEICULOKM " +
															                         " WHERE VEICULOKM.CDVEICULO = VKM.CDVEICULO " +
															                         " AND VEICULOKM.CDVEICULOKM < " + ajuste.getCdveiculokm() + ")", 
															new RowMapper() {
																public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
																	return rs.getInt("KMANTIGO");
																}
													
															});
		
		if(listaUpdate != null && listaUpdate.size() > 0){
			return listaUpdate.get(0);
		} else {
			return 0;
		}
		
	}
}
