package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Colaboradordeficiencia;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("colaboradordeficiencia.nome")
public class ColaboradordeficienciaDAO extends GenericDAO<Colaboradordeficiencia> {
	
	/* singleton */
	private static ColaboradordeficienciaDAO instance;
	public static ColaboradordeficienciaDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradordeficienciaDAO.class);
		}
		return instance;
	}
		
}
