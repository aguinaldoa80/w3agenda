package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CartaocreditoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CartaocreditoDAO extends GenericDAO<Conta> {

	@Override
	public void updateEntradaQuery(QueryBuilder<Conta> query) {
		query.leftOuterJoinFetch("conta.listaContaempresa listaContaempresa");
		query.leftOuterJoinFetch("listaContaempresa.empresa empresa");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaContaempresa");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Conta> query,FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		}
		CartaocreditoFiltro filtro = (CartaocreditoFiltro) _filtro;
		query
			.select("conta.cdconta, conta.nome, conta.numero, conta.limite, conta.ativo, view.saldoatual")
			.join("conta.vconta view")
			.leftOuterJoin("conta.listaContaempresa listaContaempresa")
			.leftOuterJoin("listaContaempresa.empresa empresa")
			.where("empresa = ?",filtro.getEmpresa())
			.where("conta.contatipo.cdcontatipo = ?",Contatipo.CARTAO_DE_CREDITO)
			.whereLikeIgnoreAll("conta.nome", filtro.getNome())
			.whereLikeIgnoreAll("conta.numero", filtro.getNumero())
			.where("conta.ativo = ?",filtro.getAtivo())
			.orderBy("conta.nome")
			.ignoreAllJoinsPath(true)
			;
	}

	/**
	 * M�todo para obter a quantidade de Cartaocredito pelo n�mero.
	 * 
	 * @param bean
	 * @return 
	 * @author Flavio Tavares
	 */
	public Long findCartaocreditoByNumero(Conta bean){
		if(bean==null || bean.getNumero() == null){
			throw new SinedException("Os par�metros bean e n�mero n�o podem ser null.");
		}
		return newQueryBuilderWithFrom(Long.class)
			.select("count(*)")
			.where("conta.contatipo.cdcontatipo = ?",Contatipo.CARTAO_DE_CREDITO)
			.where("conta.numero=?", bean.getNumero())
			.where("conta.cdconta <> ?",bean.getCdconta())
			.unique();
	}
	
	/**
	 * M�todo para obter a quantidade de Cartaocredito pelo nome.
	 * 
	 * @param bean
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Long findCartaocreditoByNome(Conta bean){
		if(bean==null || bean.getNome() == null){
			throw new SinedException("Os par�metros bean e nome n�o podem ser null.");
		}
		return newQueryBuilderWithFrom(Long.class)
			.select("count(*)")
			.where("conta.contatipo.cdcontatipo = ?",Contatipo.CARTAO_DE_CREDITO)
			.where("upper(conta.nome)=?", bean.getNome().toUpperCase().trim())
			.where("conta.cdconta <> ?",bean.getCdconta())
			.unique();
	}	
}
