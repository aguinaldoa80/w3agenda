package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.authorization.AuthorizationManager;
import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Arquivobancariotipo;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.Operacaocontabil;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_movimentacao;
import br.com.linkcom.sined.geral.bean.enumeration.AnaliseReceitaDespesaOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.Chequesituacao;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoFinanceiraVinculoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoLancamentoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContagerencial;
import br.com.linkcom.sined.geral.bean.enumeration.OrdenacaoExtratoContaReport;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaodespesaviagem;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.DespesaviagemService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.RelatorioMargemContribuicaoFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.MovimentacaoFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.process.filter.GerarArquivoRemessaConfiguravelChequeFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.AnaliseReceitaDespesaReportFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.CopiaChequeReportFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.ExtratoContaReportFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.FluxocaixaFiltroReport;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.LcdprArquivoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.CustooperacionalFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

public class MovimentacaoDAO extends GenericDAO<Movimentacao>{
	
	private RateioService rateioService;
	private DespesaviagemService despesaviagemService;
	private ContagerencialDAO contagerencialDAO;
	private ArquivoDAO arquivoDAO;
	
	public void setContagerencialDAO(ContagerencialDAO contagerencialDAO) {
		this.contagerencialDAO = contagerencialDAO;
	}
	public void setDespesaviagemService(
			DespesaviagemService despesaviagemService) {
		this.despesaviagemService = despesaviagemService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Movimentacao> query,FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O parametro _filtro n�o pode ser null.");
		}
		MovimentacaoFiltro filtro = (MovimentacaoFiltro) _filtro;
		
		query
			
		.select("distinct new br.com.linkcom.sined.geral.bean.Movimentacao(" +
					"movimentacao.cdmovimentacao, conta.nome, movimentacao.dtmovimentacao, movimentacao.historico, movimentacao.valor, " +
					"tipooperacao.cdtipooperacao, tipooperacao.nome, movimentacao.checknum, movimentacao.dtbanco,movimentacaoacao.cdmovimentacaoacao," +
					"movimentacaoacao.nome,cheque.numero, coalesce(movimentacao.dtbanco,movimentacao.dtmovimentacao), movimentacao.observacaocopiacheque, " +
					"coalesce(movimentacao.historico, movimentacao.observacaocopiacheque)" +
				")")
			.setUseTranslator(false)
			.join("movimentacao.conta conta")
			.leftOuterJoin("movimentacao.empresa empresaMov")
			.leftOuterJoin("conta.listaContaempresa listaContaempresa")
			.leftOuterJoin("listaContaempresa.empresa empresa")
			.leftOuterJoin("conta.contatipo contatipo")
			.leftOuterJoin("conta.banco banco")
			.join("movimentacao.movimentacaoacao movimentacaoacao")
			.leftOuterJoin("movimentacao.tipooperacao tipooperacao")
			.leftOuterJoin("movimentacao.formapagamento formapagamento")
			.leftOuterJoin("movimentacao.cheque cheque")
			
			.leftOuterJoin("movimentacao.rateio rateio")
			.leftOuterJoin("rateio.listaRateioitem item")
			.leftOuterJoin("item.projeto projeto")
			.leftOuterJoin("item.contagerencial contagerencial")
			.leftOuterJoin("item.centrocusto centrocusto")
			.leftOuterJoin("movimentacao.listaMovimentacaoorigem listaMovimentacaoorigem")
			.leftOuterJoin("listaMovimentacaoorigem.documento documento")
			.leftOuterJoin("documento.documentotipo documentotipo")
			.leftOuterJoin("documentotipo.listaDocumentotipopapel listaDocumentotipopapel")
			.leftOuterJoin("listaDocumentotipopapel.papel papelTipoDocumento")
			
			.where("conta.contatipo = ?",filtro.getContatipo())
			.where("conta = ?",filtro.getConta())
			
			.where("movimentacao.valor >= ?",filtro.getValorDe())
			.where("movimentacao.valor <= ?",filtro.getValorAte())
			
			.where("formapagamento = ?", filtro.getFormapagamento())
			
			.where("movimentacao.dtaltera >= ?",SinedDateUtils.dateToBeginOfDay(filtro.getDtalteracaoDe()))
			.where("movimentacao.dtaltera <= ?",SinedDateUtils.dataToEndOfDay(filtro.getDtalteracaoAte()));
		
			if(filtro.getDtperiodomovimentacaoDe() != null || filtro.getDtperiodomovimentacaoAte() != null){
				query
				.where("movimentacao.dtmovimentacao >= ? ", SinedDateUtils.dateToBeginOfDay(filtro.getDtperiodomovimentacaoDe()))
				.where("movimentacao.dtmovimentacao <= ? ", SinedDateUtils.dataToEndOfDay(filtro.getDtperiodomovimentacaoAte()));
			}else{
				query
				.where("movimentacao.dtbanco >= ?",filtro.getDtbancoDe())
				.where("movimentacao.dtbanco <= ?",filtro.getDtbancoAte())
				
				.where("movimentacao.dtmovimentacao >= ? ", SinedDateUtils.dateToBeginOfDay(filtro.getDtperiodomovimentacaoDe()))
				
				.where("coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao) >= ?",filtro.getDtperiodoDe())
				.where("coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao) <= ?",filtro.getDtperiodoAte());
			}
			query.openParentheses()
			.whereLikeIgnoreAll("movimentacao.historico", filtro.getChequeHistoricoChecknum()).or()
			.whereLikeIgnoreAll("movimentacao.checknum", filtro.getChequeHistoricoChecknum()).or()
			.whereLikeIgnoreAll("cheque.numero", filtro.getChequeHistoricoChecknum())
			.closeParentheses()
			
			.where("tipooperacao = ?",filtro.getTipooperacao())
			.whereIn("movimentacaoacao.cdmovimentacaoacao", 
					filtro.getListaAcao() != null ? CollectionsUtil.listAndConcatenate(filtro.getListaAcao(), "cdmovimentacaoacao", ",") : null)
			
			.where("contagerencial = ?",filtro.getContagerencial())
			.where("contagerencial.natureza = ?",filtro.getNaturezaContagerencial())
			
			.where("cheque.dtbompara >= ?", filtro.getDtbomparade())
			.where("cheque.dtbompara <= ?", filtro.getDtbomparaate())
			
			.where("projeto = ?",filtro.getProjeto())
			.where("centrocusto = ?",filtro.getCentrocusto())
//			.orderBy("movimentacao.dtmovimentacao desc")
			.ignoreJoin("rateio","item","projeto","contagerencial","centrocusto", "listaContapapel", "documentotipo", "listaDocumentotipopapel", "papelTipoDocumento");
		;
		
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		if(!administrador){
			query.leftOuterJoinIfNotExists("conta.listaContapapel listaContapapel");
		
			if (whereInPapel != null && !whereInPapel.equals("")){
				query.openParentheses();
					query.whereIn("listaContapapel.papel.cdpapel", whereInPapel)
					.or()
					.where("listaContapapel.papel.cdpapel is null");
				query.closeParentheses();
			} else {
				query.where("listaContapapel.papel.cdpapel is null");
			}
			
			query
				.openParentheses()
					.where("listaDocumentotipopapel is null")
					.or()
					.whereIn("papelTipoDocumento.cdpapel", whereInPapel)
				.closeParentheses();
		}
		
		if(filtro.getAntecipada() != null && filtro.getAntecipada()){
			query.where("movimentacao.antecipada = ?", filtro.getAntecipada());
		}else if(filtro.getAntecipada() != null && !filtro.getAntecipada()){
			query
				.where("listaMovimentacaoorigem is null ");
			
		}
		
		if(filtro.getDocumentotipo() != null){
			query
				.where("exists(" +
						"select mo.id " +
						"from Movimentacaoorigem mo " +
						"join mo.movimentacao mv " +
						"join mo.documento doc " +
						"join doc.documentotipo doctipo " +
						"where mv.cdmovimentacao = movimentacao.cdmovimentacao " +
						"and doctipo.cddocumentotipo = " + filtro.getDocumentotipo().getCddocumentotipo() + " " +
						")");
		}
		
		if(filtro.getCdvenda() != null){
			query
				.where("exists(" +
						"select v.cdvenda " +
						"from Venda v " +
						"join v.listadocumentoorigem docorigem " +
						"join docorigem.documento doc " +
						"join doc.listaMovimentacaoOrigem movorigem " +
						"join movorigem.movimentacao mov " +
						"where mov.cdmovimentacao = movimentacao.cdmovimentacao " +
						"and v.cdvenda = " + filtro.getCdvenda() + " " +
						")");
		}
		AuthorizationManager authorizationManager = Neo.getApplicationContext().getAuthorizationManager();
	
		if (!authorizationManager.isAuthorized("/financeiro/crud/Contapagar", null, SinedUtil.getUsuarioLogado())){
			query
		.where ("movimentacao.tipooperacao <> ?", Tipooperacao.TIPO_DEBITO);
		}
		
		if (!authorizationManager.isAuthorized("/financeiro/crud/Contareceber", null, SinedUtil.getUsuarioLogado())){
			query
			.where("movimentacao.tipooperacao <>?", Tipooperacao.TIPO_CREDITO);
		}
		if(filtro.getOrdenacaoReport() != null){
			query.orderBy("movimentacao." + filtro.getOrdenacaoReport().getCampo() + (filtro.getAscReport() != null && filtro.getAscReport() ? " asc " : " desc "));
		}
		
		query.leftOuterJoin("documento.pessoa pessoaDocumento");
		if (StringUtils.isNotBlank(filtro.getPessoa()) || filtro.getEmpresa() != null || SinedUtil.isRestricaoEmpresaClienteUsuarioLogado() || SinedUtil.isRestricaoEmpresaFornecedorUsuarioLogado() ||
			filtro.getDtcompravendaDe() != null || filtro.getDtcompravendaAte() != null) {
			
			if(filtro.getDtcompravendaDe() != null || filtro.getDtcompravendaAte() != null){
				query.leftOuterJoin("documento.listaDocumentoOrigem documentoorigem");
				String whereDtIniOrdemcompra = filtro.getDtcompravendaDe() != null? ("and _DTINI >= '" + SinedDateUtils.toStringPostgre(filtro.getDtcompravendaDe()) + "' ") : "";
				String whereDtFimOrdemcompra = filtro.getDtcompravendaAte() != null? ("and _DTFIM <= '" + SinedDateUtils.toStringPostgre(filtro.getDtcompravendaAte()) + "' ") : "";

				query.openParentheses()
						.openParentheses()
							.where("documentoorigem.venda is not null")
							.where("exists(" +
										"select v.cdvenda " +
										"from Venda v " +
										"join v.listadocumentoorigem docorigem " +
										"where documentoorigem.cddocumentoorigem = docorigem.cddocumentoorigem " +
										whereDtIniOrdemcompra.replace("_DTINI", "v.dtvenda") +
										whereDtFimOrdemcompra.replace("_DTFIM", "v.dtvenda") +
										")")
						.closeParentheses()
						.or()
						.openParentheses()
							.where("documentoorigem.pedidovenda is not null")
							.where("exists(" +
										"select pv.cdpedidovenda " +
										"from Pedidovenda pv " +
										"join pv.listaDocumentoorigem docorigem " +
										"where documentoorigem.cddocumentoorigem = docorigem.cddocumentoorigem " +
										whereDtIniOrdemcompra.replace("_DTINI", "pv.dtpedidovenda") +
										whereDtFimOrdemcompra.replace("_DTFIM", "pv.dtpedidovenda") +
										")")
						.closeParentheses()
						.or()
						.openParentheses()
							.where("documentoorigem.entrega is not null")
							.where("exists(" +
										"select e.cdentrega " +
										"from Entrega e " +
										"join e.listaEntregadocumento ed " +
										"where e = documentoorigem.entrega " +
										whereDtIniOrdemcompra.replace("_DTINI", "ed.dtemissao") +
										whereDtFimOrdemcompra.replace("_DTFIM", "ed.dtemissao") +
										")")
						.closeParentheses()
						.or()
						.openParentheses()
							.where("documentoorigem.entregadocumento is not null")
							.where("exists(" +
										"select ed.cdentregadocumento " +
										"from Entregadocumento ed " +
										"where ed = documentoorigem.entregadocumento " +
										whereDtIniOrdemcompra.replace("_DTINI", "ed.dtemissao") +
										whereDtFimOrdemcompra.replace("_DTFIM", "ed.dtemissao") +
										")")
						.closeParentheses()
					.closeParentheses();
				}
			
			if(filtro.getEmpresa() != null){
				query.openParentheses()
					.openParentheses()
						.where("empresaMov is not null")
						.where("empresaMov = ?", filtro.getEmpresa())
					.closeParentheses();
					query.or();
					query.openParentheses()
						.where("empresaMov is null")
						.openParentheses()
							.openParentheses()
								.where("documento is not null")
								.where("documento.empresa = ?", filtro.getEmpresa())
							.closeParentheses()
							.or()
							.openParentheses()
								.where("documento is null")
								.where("empresa = ?", filtro.getEmpresa())
							.closeParentheses()
						.closeParentheses()
					.closeParentheses();
				query.closeParentheses();
				
			}
			
			if(StringUtils.isNotBlank(filtro.getPessoa())){
				query
					.leftOuterJoin("listaMovimentacaoorigem.agendamento agendamento")
					.leftOuterJoin("agendamento.pessoa pessoaAgendamento");
				
				Cpf cpf = null;
				Cnpj cnpj = null;
				try {	
					cpf = new Cpf(filtro.getPessoa());												
				} catch (Exception e) {	}
				try {					
					cnpj = new Cnpj(filtro.getPessoa());						
				} catch (Exception e) {	}
				
				if (filtro.getPessoa().indexOf('?') > 0) {
					throw new IllegalArgumentException("A cl�usula where do QueryBuilder n�o pode ter o caracter '?'. Deve ser passada apenas a express�o que se deseja fazer o like. Veja javadoc!");
				}
				
				if(filtro.getTipopagamento() != null){
					query.openParentheses()
						.where("documento.tipopagamento = ?", filtro.getTipopagamento())
						.or()
						.where("agendamento.tipopagamento = ?", filtro.getTipopagamento())
					.closeParentheses();
				}
				
				query
				.openParentheses()
					.openParentheses()
						.whereLikeIgnoreAll("pessoaDocumento.nome", filtro.getPessoa()).or()
						.whereLikeIgnoreAll("pessoaAgendamento.nome", filtro.getPessoa())
					.closeParentheses();
				
					if(cpf != null || cnpj != null){
						query.or()
						.openParentheses()
							.where("pessoaDocumento.cpf = ?", cpf).or()
							.where("pessoaAgendamento.cpf = ?", cpf).or()
							.where("pessoaDocumento.cnpj = ?", cnpj).or()
							.where("pessoaAgendamento.cnpj = ?", cnpj).or()
						.closeParentheses();	
					}
						
					query.or()
					.openParentheses()
						.whereLikeIgnoreAll("documento.outrospagamento", filtro.getPessoa()).or()
						.whereLikeIgnoreAll("agendamento.outrospagamento", filtro.getPessoa())
					.closeParentheses();
					
				String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
				if (funcaoTiraacento != null){
					if(filtro.getTipopagamento() != null && Tipopagamento.FORNECEDOR.equals(filtro.getTipopagamento())){
						query.or()
						.openParentheses()
							.where("pessoaDocumento.cdpessoa in (SELECT f.cdpessoa FROM Fornecedor f "+
								"WHERE UPPER(" + funcaoTiraacento + "(f.razaosocial)) LIKE '%'||?||'%')", Util.strings.tiraAcento(filtro.getPessoa().replaceAll("'", "\'").toUpperCase()))
							.or()
							.where("pessoaAgendamento.cdpessoa in (SELECT f.cdpessoa FROM Fornecedor f "+
								"WHERE UPPER(" + funcaoTiraacento + "(f.razaosocial)) LIKE '%'||?||'%')", Util.strings.tiraAcento(filtro.getPessoa().replaceAll("'", "\'").toUpperCase()))
						.closeParentheses();
						
					}
					if(filtro.getTipopagamento() == null || Tipopagamento.CLIENTE.equals(filtro.getTipopagamento())){
						query.or()
						.openParentheses()
							.where("pessoaDocumento.cdpessoa in (SELECT c.cdpessoa FROM Cliente c "+
								"WHERE UPPER(" + funcaoTiraacento + "(c.identificador)) LIKE '%'||?||'%')", Util.strings.tiraAcento(filtro.getPessoa().replaceAll("'", "\'").toUpperCase()))
							.or()
							.where("pessoaAgendamento.cdpessoa in (SELECT c.cdpessoa FROM Cliente c "+
									"WHERE UPPER(" + funcaoTiraacento + "(c.identificador)) LIKE '%'||?||'%')", Util.strings.tiraAcento(filtro.getPessoa().replaceAll("'", "\'").toUpperCase()))
						.closeParentheses();
					}
				}
				query.closeParentheses();
			}
			
			if(SinedUtil.isRestricaoEmpresaClienteUsuarioLogado() || SinedUtil.isRestricaoEmpresaFornecedorUsuarioLogado()){
				String whereInEmpresa = new SinedUtil().getListaEmpresa();
				if(SinedUtil.isRestricaoEmpresaClienteUsuarioLogado() && StringUtils.isNotBlank(whereInEmpresa)){
					String tipoPagamento = "";
					if(SinedUtil.isRestricaoEmpresaClienteUsuarioLogado()){
						tipoPagamento += Tipopagamento.CLIENTE.ordinal();
					}
					if(SinedUtil.isRestricaoEmpresaFornecedorUsuarioLogado()){
						if(tipoPagamento.length() > 0) tipoPagamento += ",";
						tipoPagamento += Tipopagamento.FORNECEDOR.ordinal();
					}
					
					query.openParentheses()
						.where("pessoaDocumento.cdpessoa is null")
						.or()
						.where("documento.tipopagamento not in (" + tipoPagamento + ")");
					
					if(SinedUtil.isRestricaoEmpresaFornecedorUsuarioLogado()){
						query.or()
						.openParentheses()
							.where("documento.tipopagamento = ?", Tipopagamento.FORNECEDOR)
							.where("pessoaDocumento.cdpessoa in (select f.cdpessoa from Fornecedor f left outer join f.listaFornecedorempresa fe left outer join fe.empresa e where e.cdpessoa is null or e.cdpessoa in (" + whereInEmpresa + "))")
						.closeParentheses();
					}
					
					if(SinedUtil.isRestricaoEmpresaClienteUsuarioLogado()){
						query.or()
						.openParentheses()
							.where("documento.tipopagamento = ?", Tipopagamento.CLIENTE)
							.where("pessoaDocumento.cdpessoa in (select c.cdpessoa from Cliente c left outer join c.listaClienteEmpresa ce left outer join ce.empresa e where e.cdpessoa is null or e.cdpessoa in (" + whereInEmpresa + "))")
						.closeParentheses();
					
					}
					query.closeParentheses();
				}
			}
		}
		query.whereLikeIgnoreAll("movimentacao.bordero", filtro.getBordero());
	}
	
	public void addRestricaoClienteEmpresa(QueryBuilder<Movimentacao> query){
		String whereInEmpresa = new SinedUtil().getListaEmpresa();
		if(SinedUtil.isRestricaoEmpresaClienteUsuarioLogado() && StringUtils.isNotBlank(whereInEmpresa)){
			query.leftOuterJoinIfNotExists("documento.pessoa pessoa");
			query.openParentheses()
				.where("pessoa.cdpessoa is null")
				.or()
				.where("documento.tipopagamento <> ?", Tipopagamento.CLIENTE)
				.or()
				.where("pessoa.cdpessoa in (select c.cdpessoa from Cliente c left outer join c.listaClienteEmpresa ce left outer join ce.empresa e where e.cdpessoa is null or e.cdpessoa in (" + whereInEmpresa + "))")
			.closeParentheses();
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Movimentacao> query) {
		query.leftOuterJoinFetch("movimentacao.movimentacaoacao movimentacaoacao");
		query.leftOuterJoinFetch("movimentacao.cheque cheque");
		query.leftOuterJoinFetch("cheque.empresa empresaCheque");
		query.leftOuterJoinFetch("movimentacao.conta conta");
		query.leftOuterJoinFetch("conta.contatipo contatipo");
		query.leftOuterJoinFetch("movimentacao.formapagamento formapagamento");
		query.leftOuterJoinFetch("movimentacao.tipooperacao tipooperacao");
//		query.leftOuterJoinFetch("movimentacao.empresa empresa");
		
		query.leftOuterJoinFetch("movimentacao.rateio rateio");
		query.leftOuterJoinFetch("rateio.listaRateioitem rateioitem");
		query.leftOuterJoinFetch("rateioitem.centrocusto centrocusto");
		query.leftOuterJoinFetch("rateioitem.projeto projeto");
		query.leftOuterJoinFetch("rateioitem.contagerencial contagerencial");
		
		query.leftOuterJoinFetch("movimentacao.comprovante comprovante");
		query.leftOuterJoinFetch("movimentacao.operacaocontabil operacaocontabil");
//		query.leftOuterJoinFetch("movimentacao.empresa empresa");
		
//		query.leftOuterJoinFetch("movimentacao.listaMovimentacaohistorico movimentacaohistorico");
//		query.leftOuterJoinFetch("movimentacaohistorico.movimentacaoacao movimentacaoacao");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Movimentacao bean = (Movimentacao) save.getEntity();
		
		if (bean.getListaMovimentacaoorigem() != null && bean.getListaMovimentacaoorigem().size() > 0) {
			String whereIn = "";
			for (Movimentacaoorigem movimentacaoorigem : bean.getListaMovimentacaoorigem()) {
				if (movimentacaoorigem.getDespesaviagemacerto() != null && 
						movimentacaoorigem.getDespesaviagemacerto().getCddespesaviagem() != null) {
					whereIn += movimentacaoorigem.getDespesaviagemacerto().getCddespesaviagem() + ",";
				}
			}
			if (!whereIn.equals("")) {
				whereIn = whereIn.substring(0, whereIn.length()-1);
				despesaviagemService.updateSituacao(whereIn, Situacaodespesaviagem.BAIXADA);
			}
		}
		
		save.saveOrUpdateManaged("listaMovimentacaohistorico");
		save.saveOrUpdateManaged("listaMovimentacaoorigem", "movimentacao");
		
		if (bean.getRateio() != null) {
			rateioService.saveOrUpdateNoUseTransaction(bean.getRateio());
		}
		if(bean.getComprovante() != null)
			arquivoDAO.saveFile(save.getEntity(), "comprovante");

	}
	
	/**
	 * Obt�m uma lista de movimenta��es de acordo com o filtro da listagem, sem restri��o de p�gina.
	 * A lista � usada para somar os valores das movimenta��es e exibir na listagem.
	 * 
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Movimentacao> findForSomaListagem(MovimentacaoFiltro filtro){
		QueryBuilder<Movimentacao> query = query();
		this.updateListagemQuery(query, filtro);
		query.select("movimentacao.cdmovimentacao, conta.nome, movimentacao.dtmovimentacao, movimentacao.historico, movimentacao.valor, " +
					"tipooperacao.cdtipooperacao, tipooperacao.nome, movimentacao.checknum, movimentacao.dtbanco,movimentacaoacao.cdmovimentacaoacao," +
					"movimentacaoacao.nome,cheque.numero, movimentacao.observacaocopiacheque, rateio.cdrateio, projeto.cdprojeto, contagerencial.cdcontagerencial," +
					"centrocusto.cdcentrocusto, item.valor ")
				.setUseTranslator(true)
				.setIgnoreJoinPaths(new HashSet<String>());
		return query.list();
	}
	
	/**
	 * M�todo para atualizar a a��o de uma movimentacao.
	 * O bean ser� atualizado com o objeto que estiver na propriedade <code>movimentacaoacao</code>.
	 * 
	 * @param movimentacao
	 * @author Fl�vio Tavares
	 * 		   Rodrigo Freitas - Mudan�a para o uso do hibernate para fazer o update.
	 */
	public void updateAcao(Movimentacao movimentacao){
		if(movimentacao == null || 
			movimentacao.getCdmovimentacao() == null ||
			movimentacao.getMovimentacaoacao() == null ||
			movimentacao.getMovimentacaoacao().getCdmovimentacaoacao() == null){
			throw new SinedException("Par�metros inv�lidos. " +
					"Os campos movimentacao,cdmovimentacao,movimentacaoacao ou cdmovimentacaoacao n�o podem ser nulos.");
		}
		
		Integer cdusuario = null;
		
		if (SinedUtil.getUsuarioLogado() != null) {
			cdusuario = SinedUtil.getUsuarioLogado().getCdpessoa();
		}
		
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		
		String sql = "update Movimentacao m " +
				"set movimentacaoacao = ?, ";
				
				if(cdusuario != null){
					sql +=	"cdusuarioaltera = ?, ";
				}
				
				sql += "dtaltera = ? " +
				"where m.id = ?";
				
				Object[] fields;
				
				if (cdusuario != null) {
					 fields = new Object[]{movimentacao.getMovimentacaoacao(),cdusuario,hoje,movimentacao.getCdmovimentacao()};
				} else {
					 fields = new Object[]{movimentacao.getMovimentacaoacao(),hoje,movimentacao.getCdmovimentacao()};
				}
				
		getHibernateTemplate().bulkUpdate(sql, fields);
	}
	
	/**
	 * M�todo para estornar uma movimenta��o.
	 * 
	 * @param movimentacao - Movimenta��o a ser estornada.
	 * @param acaoEstorno - A��o da movimenta��o ao ser estornada. 
	 * 						Poss�veis valores: Constantes de {@link br.com.linkcom.sined.geral.bean.Movimentacaoacao}.
	 * 						Se for null ser� utilizado a a��o padr�o: Normal {@link br.com.linkcom.sined.geral.bean.Movimentacaoacao#NORMAL}.
	 * @author Fl�vio Tavares
	 */
	public void estornarMovimentacao(Movimentacao movimentacao, Movimentacaoacao acaoEstorno){
		if(movimentacao == null || movimentacao.getCdmovimentacao() == null){
			throw new SinedException("Par�metros inv�lidos. " +
					"Os campos movimentacao e cdmovimentacao n�o podem ser nulos.");
		}
		
		String sql = "update Movimentacao m " +
					"set movimentacaoacao = ?, " +
					"cdusuarioaltera = ?, " +
					"dtaltera = ?, " +
					"dtbanco = null, " +
					"checknum = null, " +
					"fitid = null " +
					"where m.id = ?";
		
		Integer cdusuario = SinedUtil.getUsuarioLogado().getCdpessoa();
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		
		Object[] fields = new Object[]{
				acaoEstorno != null ? acaoEstorno.getCdmovimentacaoacao() : Movimentacaoacao.NORMAL,
				cdusuario, hoje, movimentacao.getCdmovimentacao()};
		
		getHibernateTemplate().bulkUpdate(sql, fields);
	}
	
	/**
	 * Carrega a listagem das movimenta��es de forma de pagamento 'cheque' para que seja listado,
	 * na tela de relat�rio de c�pia de cheque.
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Movimentacao> findForListagemRelatorio(CopiaChequeReportFiltro filtro) {
		return query()
					.select("cheque.numero, movimentacao.valor, movimentacao.dtmovimentacao, movimentacao.cdmovimentacao, " +
							"documento.descricao, pessoa.nome, conta.nome, conta.agencia, conta.numero, empresa.nome, empresa.razaosocial, empresa.nomefantasia")
					.join("movimentacao.conta conta")
					.join("conta.banco banco")
					.leftOuterJoin("conta.listaContaempresa listaContaempresa")
					.leftOuterJoin("listaContaempresa.empresa empresa")
					.leftOuterJoin("movimentacao.cheque cheque")
					.join("movimentacao.listaMovimentacaoorigem origem")
					.join("origem.documento documento")
					.join("documento.pessoa pessoa")
					.where("documento.tipopagamento = ?", Tipopagamento.FORNECEDOR)
					.where("movimentacao.dtmovimentacao >= ?",filtro.getDtInicio())
					.where("movimentacao.dtmovimentacao <= ?",filtro.getDtFinal())
					.where("empresa = ?",filtro.getEmpresa())
					.where("conta = ?",filtro.getConta())
					.whereLikeIgnoreAll("cheque.numero", filtro.getNumCheque())
					.where("movimentacao.valor >= ?",filtro.getValorminimo())
					.where("movimentacao.valor <= ?",filtro.getValormaximo())
					.where("movimentacao.formapagamento = ?",Formapagamento.CHEQUE)
					.where("movimentacao.tipooperacao = ?", Tipooperacao.TIPO_DEBITO)
					.list();
	}

	
	/**
	 * Carrega a lista de movimenta��es para que seja preenchido o relat�rio.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Movimentacao> findForCopiaCheque(String whereIn) {
		return query()
				.select("banco.nome, cheque.numero, movimentacao.dtmovimentacao, movimentacao.dtbanco, pessoa.nome, pessoaag.nome," +
						"documento.numero, documento.cddocumento, documento.dtemissao, documento.dtvencimento, documento.valor, parcela.ordem, parcelamento.iteracoes," +
						"documento.descricao, documento.tipopagamento, documento.outrospagamento, projeto.nome, rateioitem.valor, contagerencial.nome, " +
						"conta.agencia, conta.numero, documentotipoag.nome, documentotipo.nome, rateio.cdrateio, conta.dvnumero, conta.dvagencia, " +
						"movimentacao.valor, movimentacao.observacaocopiacheque, documento.outrospagamento, documento.tipopagamento, agendamento.tipopagamento, " +
						"empresa.nome, empresa.razaosocial, empresa.nomefantasia, logomarca.cdarquivo")
				
				.join("movimentacao.conta conta")
				.leftOuterJoin("conta.banco banco")				
				.leftOuterJoin("conta.listaContaempresa listaContaempresa")
				.leftOuterJoin("listaContaempresa.empresa empresa")
				.leftOuterJoin("empresa.logomarca logomarca")
				
				.leftOuterJoin("movimentacao.listaMovimentacaoorigem origem")
				.leftOuterJoin("origem.documento documento")
				.leftOuterJoin("origem.agendamento agendamento")
				
				.leftOuterJoin("documento.listaParcela parcela")
				.leftOuterJoin("parcela.parcelamento parcelamento")
				
				
				.leftOuterJoin("agendamento.documentotipo documentotipoag")
				.leftOuterJoin("documento.documentotipo documentotipo")
				
				.leftOuterJoin("agendamento.pessoa pessoaag")
				.leftOuterJoin("documento.pessoa pessoa")
				
				.leftOuterJoin("movimentacao.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem rateioitem")
				.leftOuterJoin("rateioitem.projeto projeto")
				.leftOuterJoin("rateioitem.contagerencial contagerencial")
				
				.leftOuterJoin("movimentacao.cheque cheque")
//				.where("documento.tipopagamento = ?", Tipopagamento.FORNECEDOR)
				.whereIn("movimentacao.cdmovimentacao", whereIn)
				.list();
	}

	/**
	 * Pega o pr�ximo valor do sequence SQ_COPIACHEQUE.
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Integer nextvalCopiacheque() {
		String sql = "select sq_copiacheque.nextval from dual";
		return getJdbcTemplate().queryForInt(sql);
	}

	/**
	 * Verifica se tem alguma movimenta��o do documento selecionado com o status diferente de 'cancelada'.
	 * 
	 * @param documento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Boolean isMovimentacaoCancelada(Documento documento) {
		Long count = newQueryBuilder(Long.class)
								.select("count(*)")
								.from(Movimentacao.class)
								.setUseTranslator(false)
								.join("movimentacao.listaMovimentacaoorigem origem")
								.where("origem.documento = ?",documento)
								.where("movimentacao.movimentacaoacao <> ?",Movimentacaoacao.CANCELADA)
								.unique();
		return count != 0;
	}
	
	/**
	 * Retorna lista de movimenta��es n�o canceladas.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Long listaMovimentcaoNaoCancelada(String whereIn, String whereInMovDelete){
		QueryBuilderSined<Long> query = newQueryBuilderSined(Long.class);
		query.setUseWhereEmpresa(false);
		
		query
			.select("count(*)")
			.from(Movimentacao.class)
			.join("movimentacao.listaMovimentacaoorigem origem")
			.whereIn("origem.documento", whereIn)
			.where("movimentacao.movimentacaoacao <> ?",Movimentacaoacao.CANCELADA)
			.setUseTranslator(false);
		
		if(whereInMovDelete != null && !whereInMovDelete.equals("")){
			query.where("movimentacao.cdmovimentacao not in (" + whereInMovDelete + ")");
		}
		
		return query
					.unique();
	}
	
	/**
	 * M�todo para carregar campos de uma movimentacao.
	 * 
	 * @param bean
	 * @param campos
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Movimentacao load(Movimentacao bean, String campos){
		if(bean==null){
			throw new SinedException("O par�metro bean n�o pode ser null.");
		}
		QueryBuilder<Movimentacao> query = query();
		
		if(campos!=null && !campos.equals("")){
			query.select(campos);
		}
		
		return query
				.entity(bean)
				.unique();
	}
	
	/**
	 * <p>M�todo para carregar lista de Movimenta��o com base nos ID's.</p>
	 * <pre>Faz um WhereIn do campo cdmovimenta��o no campo passado <code>cds</code>.</pre>
	 * 
	 * @param whereIn - ID's das movimenta��es separados por v�rgula. <b>Ex.: 1,2,25,3654,4589</b>
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Movimentacao> carregaLista(String whereIn){
		if(whereIn == null || "".equals(whereIn)){
			throw new SinedException("O par�metro cds n�o pode ser null ou vazio.");
		}
		return
			query()
			 .select("movimentacao.cdmovimentacao, movimentacao.movimentacaoacao, contatipo.cdcontatipo, contatipo.nome, " +
			 		"conta.cdconta, conta.nome, banco.nome, conta.agencia, conta.dvagencia, conta.numero, conta.dvnumero, " +
			 		"movimentacao.dtmovimentacao, tipooperacao.cdtipooperacao, tipooperacao.nome, movimentacao.valor, movimentacao.valortaxa, " +
			 		"aux_movimentacao.movimentacaodescricao, acao.cdmovimentacaoacao, acao.nome," +
			 		"documentoacao.cddocumentoacao, movimentacao.dtaltera," +
			 		"documento.cddocumento, classe.cddocumentoclasse, cheque.cdcheque, cheque.numero, cheque.chequesituacao," +
			 		"formapagamento.cdformapagamento, rateioitem.cdrateioitem, rateioitem.percentual, rateioitem.valor")
			 
			 .join("movimentacao.conta conta")
			 .leftOuterJoin("conta.banco banco")
			 .join("conta.contatipo contatipo")
			 .leftOuterJoin("movimentacao.cheque cheque")
			 .leftOuterJoin("movimentacao.tipooperacao tipooperacao")
			 .join("movimentacao.aux_movimentacao aux_movimentacao")
			 .join("movimentacao.movimentacaoacao acao")
			 
			 .leftOuterJoin("movimentacao.listaMovimentacaoorigem origem")
			 .leftOuterJoin("origem.documento documento")
			 .leftOuterJoin("documento.documentoacao documentoacao")
			 .leftOuterJoin("documento.documentoclasse classe")
			 
			 .leftOuterJoin("movimentacao.formapagamento formapagamento")
			 .join("movimentacao.rateio rateio")
			 .join("rateio.listaRateioitem rateioitem")
			 
			 .whereIn("movimentacao.cdmovimentacao",whereIn)
			 .orderBy("movimentacao.dtmovimentacao")
			 .list();
	}
	
	/**
	 * M�todo para obter a movimenta��o que deu origem � uma movimenta��o.
	 * 
	 * @param mv
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Movimentacao findMovimentacaoDestino(Movimentacao mv){
		return 
			query()
			.join("movimentacao.conta contamovimentacao")
			.leftOuterJoin("movimentacao.listaMovimentacaoorigem mo")
			.leftOuterJoin("mo.conta contamo")
			
			.where("mo.dtaltera = ?",mv.getDtaltera())
			.where("movimentacao <> ?",mv)
			.setMaxResults(1)
			.unique();
	}
	
	/**
	 * <p>Fornece os dados de origem para relat�rio de Extrato de Conta.
	 * Pega uma lista de movimenta��es que corresponda aos crit�rios do par�metro filtro.</p>
	 * 
	 * @param filtro
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Movimentacao> findForExtratoContaReport(ExtratoContaReportFiltro filtro) {
		if (filtro == null) {
			throw new SinedException("O filtro n�o pode ser nulo.");
		}
		if (filtro.getDtInicio() == null || filtro.getDtFim() == null) {
			throw new SinedException("O per�odo deve ser preenchido.");
		} else if (filtro.getDtInicio().after(filtro.getDtFim())) {
			throw new SinedException("A data inicial do per�odo n�o pode ser maior que a data final.");
		}
		if (filtro.getDescricaoVinculo() == null) {
			throw new SinedException("O campo 'Descri��o do v�nculo' � obrigat�rio");
		}
		
		QueryBuilder<Movimentacao> q = query()
			.select("movimentacao.dtbanco, movimentacao.dtmovimentacao, movimentacao.cdmovimentacao, movimentacao.checknum, movimentacao.fitid, " +
					"movimentacao.valor, movimentacao.historico, tipooperacao.cdtipooperacao, tipooperacao.nome, aux_movimentacao.movimentacaodescricao, " +
					"origem.cdmovimentacaoorigem, documento.cddocumento, documento.descricao, pessoa.cdpessoa, pessoa.nome, documento.tipopagamento, documento.outrospagamento, " +
					"documentotipo.cddocumentotipo, documentotipo.nome, documento.valor, aux_documento.valoratual, rateio.cdrateio, rateioitem.cdrateioitem," +
					"rateioitem.valor, centrocusto.cdcentrocusto ,centrocusto.nome, contagerencial.nome, contagerencial.cdcontagerencial, projeto.nome, projeto.cdprojeto " +
					(filtro.getCentrocusto() != null ? ", rateioDoc.cdrateio, rateioitemDoc.valor, centrocustoDoc.cdcentrocusto, centrocustoDoc.nome " : ""))
			.join("movimentacao.tipooperacao tipooperacao")
			.join("movimentacao.movimentacaoacao movimentacaoacao")
			.join("movimentacao.aux_movimentacao aux_movimentacao")
			.leftOuterJoin("movimentacao.listaMovimentacaoorigem origem")
			.leftOuterJoin("origem.documento documento")
			.leftOuterJoin("documento.aux_documento aux_documento")
			.leftOuterJoin("documento.documentotipo documentotipo")
			.leftOuterJoin("documento.pessoa pessoa")
			.leftOuterJoin("movimentacao.rateio rateio")
			.leftOuterJoin("rateio.listaRateioitem rateioitem")
			.leftOuterJoin("rateioitem.centrocusto centrocusto")
			.leftOuterJoin("rateioitem.contagerencial contagerencial")
			.leftOuterJoin("rateioitem.projeto projeto");
		
		if(filtro.getCentrocusto() != null){
			q.leftOuterJoin("documento.rateio rateioDoc")
			 .leftOuterJoin("rateioDoc.listaRateioitem rateioitemDoc")
			 .leftOuterJoin("rateioitem.centrocusto centrocustoDoc");
		}
		
		q
			.where("movimentacaoacao <> ?", Movimentacaoacao.CANCELADA)
			.where("movimentacao.conta = ?", filtro.getDescricaoVinculo())
			.where("movimentacao.conta.contatipo = ?", filtro.getContaTipo())
			.where("coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao) >= ?", filtro.getDtInicio())
			.where("coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao) <= ?", filtro.getDtFim())
			.where("documentotipo = ?", filtro.getDocumentotipo())
			.orderBy("coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao) ASC, movimentacao.cdmovimentacao ASC , tipooperacao.nome ASC");
		
		if(filtro.getEmpresa() != null){
			q.leftOuterJoin("movimentacao.empresa empresa");
			q.leftOuterJoin("movimentacao.conta conta");
			q.leftOuterJoin("conta.listaContaempresa listaContaempresa");
			
			q.openParentheses()
				.where("movimentacao.empresa = ?", filtro.getEmpresa())
				.or()
				.openParentheses()
					.where("movimentacao.empresa is null")
					.where("(listaContaempresa is null or listaContaempresa.empresa=?)", filtro.getEmpresa())
				.closeParentheses()
			.closeParentheses();
		}
		
		if(filtro.getCentrocusto() != null){
			q.where("rateioitem.centrocusto = ?", filtro.getCentrocusto());
		}
		if(SinedUtil.isListNotEmpty(filtro.getListaMovimentacaoacao())){
			q.whereIn("movimentacaoacao.cdmovimentacaoacao", CollectionsUtil.listAndConcatenate(filtro.getListaMovimentacaoacao(), "cdmovimentacaoacao", ","));
		}
		
		if(filtro.getOrdenacaoExtratoContaReport() != null){
			if(filtro.getOrdenacaoExtratoContaReport().equals(OrdenacaoExtratoContaReport.DATA)){
				q.orderBy("coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)" + (filtro.getAscReport() ? " asc " : " desc "));
			}else {
				q.orderBy("movimentacao." + filtro.getOrdenacaoExtratoContaReport().getCampo() + (filtro.getAscReport() ? " asc " : " desc "));
			}
		}
		
		List<Movimentacao> list = q.list();
		
		if (list != null){
			for (Movimentacao movimentacao : list){
				
				//C�digo para manter compatibilidade com a consulta antia que fazia o seguinte select:
				//	.select("new Movimentacao(coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)," +
				//		"CAST(UPPER(MOVIMENTACAODESCRICAO(movimentacao.cdmovimentacao)) as string)," +
				//		" movimentacao.checknum, movimentacao.valor, UPPER(SUBSTRING(tipooperacao.nome, 1, 1))" +
				//	")")
				
				if (movimentacao.getDtbanco() != null){
					movimentacao.setDtmovimentacao(movimentacao.getDtbanco());
				}
				
				if (StringUtils.isEmpty(movimentacao.getHistorico()) && movimentacao.getAux_movimentacao() != null && movimentacao.getAux_movimentacao().getMovimentacaodescricao() != null){
					movimentacao.setHistorico(movimentacao.getAux_movimentacao().getMovimentacaodescricao().toUpperCase());
				}
				
				movimentacao.getTipooperacao().setNome(movimentacao.getTipooperacao().getNome().substring(0, 1));
				
			}
		}
		
		return list;
	}
	
	/**
	 * Encontra as movimenta��es a partir de uma lista de arquivo banc�rio.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Movimentacao> findMovimentacaoArquivo(String whereIn) {
		return query()
					.select("movimentacao.cdmovimentacao")
					.leftOuterJoin("movimentacao.listaMovimentacaoorigem listaMovimentacaoorigem")
					.leftOuterJoin("listaMovimentacaoorigem.documento documento")
					.leftOuterJoin("documento.listaArqBancarioDoc listaArqBancarioDoc")
					.leftOuterJoin("listaArqBancarioDoc.arquivobancario ab")
					.whereIn("ab.cdarquivobancario", whereIn)
					.where("movimentacao.movimentacaoacao = ?",Movimentacaoacao.NORMAL)
					.list();
	}
	
	/**
	 * Faz o update nas movimenta��es na concilia��o manual.
	 * 
	 * @param movimentacao
	 * @author Rodrigo Freitas
	 */
	public void updateConciliacao(Movimentacao movimentacao) {
		if (movimentacao == null || movimentacao.getCdmovimentacao() == null) {
			throw new SinedException("Movimenta��o n�o pode ser nula.");
		}
		
		if(movimentacao.getFitid() == null){
			getHibernateTemplate().bulkUpdate("update Movimentacao m set m.observacaocopiacheque = ?, m.checknum = ?, m.dtbanco = ? where m.id = ?", 
				new Object[]{movimentacao.getObservacaocopiacheque(),movimentacao.getChecknum(),movimentacao.getDtbanco(),movimentacao.getCdmovimentacao()});
		} else {
			getHibernateTemplate().bulkUpdate("update Movimentacao m set m.fitid = ?, m.observacaocopiacheque = ?, m.checknum = ?, m.dtbanco = ? where m.id = ?", 
					new Object[]{movimentacao.getFitid(), movimentacao.getObservacaocopiacheque(),movimentacao.getChecknum(),movimentacao.getDtbanco(),movimentacao.getCdmovimentacao()});
		}
		
	}
	
	public void updateCdRateio(Integer cdMov, Rateio rateio) {
		if(cdMov == null || rateio == null ){
			throw new SinedException("Par�metros inv�lidos.");
		}
		getHibernateTemplate().bulkUpdate("update Movimentacao m set m.rateio = ? where m.id = ?",
				new Object[]{rateio, cdMov});
	}
	
	public void updateHistorico(Movimentacao movimentacao) {
		if (movimentacao == null || movimentacao.getCdmovimentacao() == null) {
			throw new SinedException("Movimenta��o n�o pode ser nula.");
		}
		
		getHibernateTemplate().bulkUpdate("update Movimentacao m set m.historico = ? where m.id = ?", 
				new Object[]{movimentacao.getHistorico(),movimentacao.getCdmovimentacao()});
	}
	
	/**
	 * M�todo para criar o select e joins para obten��o das movimenta��es do relat�rio de fluxo de caixa.
	 * 
	 * @param q
	 * @author Fl�vio Tavares
	 */
	private void createQueryFluxocaixa(QueryBuilder<Movimentacao> q){
		q.select("movimentacao.cdmovimentacao,movimentacao.dtmovimentacao,movimentacao.dtbanco,movimentacao.valor,movimentacao.checknum,cheque.numero,formapagamento.cdformapagamento, " +
				"conta.cdconta,conta.nome,contatipo.cdcontatipo,conta.diavencimento,conta.diafechamento," +
				"top.cdtipooperacao,top.nome,movimentacaoacao.cdmovimentacaoacao,movimentacaoacao.nome," +
				"aux_movimentacao.movimentacaodescricao, pessoaDocOrigem.nome, documentoOrigem.outrospagamento, documentoOrigem.tipopagamento, " +
				"projeto.cdprojeto, projeto.nome, rateioitem.valor, contagerencial.cdcontagerencial, contagerencial.nome, centrocusto.cdcentrocusto, " +
				"documentotipoOrigem.cddocumentotipo ")
				
		.join("movimentacao.aux_movimentacao aux_movimentacao")
		.join("movimentacao.tipooperacao top")
		.join("movimentacao.conta conta")
		.leftOuterJoin("movimentacao.empresa empresaMov")
		.leftOuterJoin("conta.listaContaempresa listaContaempresa")
		.leftOuterJoin("listaContaempresa.empresa empresa")
		.join("conta.contatipo contatipo")
		.join("movimentacao.movimentacaoacao movimentacaoacao")
		.leftOuterJoin("movimentacao.cheque cheque")
		.leftOuterJoin("movimentacao.formapagamento formapagamento")
		
		.leftOuterJoin("movimentacao.listaMovimentacaoorigem origem")
		.leftOuterJoin("origem.documento documentoOrigem")
		.leftOuterJoin("documentoOrigem.documentoacao documentoacaoOrigem")
		.leftOuterJoin("documentoOrigem.documentotipo documentotipoOrigem")
		.leftOuterJoin("documentoOrigem.pessoa pessoaDocOrigem")
		.leftOuterJoin("origem.agendamento agendamentoOrigem")
		.leftOuterJoin("agendamentoOrigem.pessoa pessoaAgOrigem")
		.leftOuterJoin("origem.conta contaOrigem")
		
		.join("movimentacao.rateio rateio")
		.join("rateio.listaRateioitem rateioitem")
		.join("rateioitem.contagerencial contagerencial")
		.leftOuterJoin("rateioitem.projeto projeto")
		.leftOuterJoin("projeto.projetotipo projetotipo")
		.join("rateioitem.centrocusto centrocusto")
		;
	}
	
	/**
	 * M�todo para obter lista de movimenta��es para gera��o do relat�rio de fluxo de caixa.
	 * 
	 * @see #createQueryFluxocaixa(QueryBuilder)
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Movimentacao> findForReportFluxocaixa(FluxocaixaFiltroReport filtro, Boolean considerarApenasAnteriores, boolean buscarMovComOrigem){
		QueryBuilderSined<Movimentacao> q = querySined();
		/**
		 * In�cio da query: Select + joins
		 */
		this.createQueryFluxocaixa(q);
				
		/**
		 * Geral
		 */
		if(filtro.getMostrarContasInativadas() == null || !filtro.getMostrarContasInativadas()){
			q.openParentheses();
				q.openParentheses();
					q.where("contatipo = ?", Contatipo.TIPO_CONTA_BANCARIA);
					q.where("conta.ativo = true");
				q.closeParentheses();
				q.or();
				q.where("contatipo <> ?", Contatipo.TIPO_CONTA_BANCARIA);
			q.closeParentheses();
		}
		
		q.openParentheses();
			if(considerarApenasAnteriores != null && considerarApenasAnteriores){
				q.where("coalesce(movimentacao.dtbanco,movimentacao.dtmovimentacao) < ?",filtro.getPeriodoDe());
				q.openParentheses()
					.where("conta.dtsaldo is null")
					.or()
					.openParentheses()
						.where("conta.dtsaldo is not null")
						.where("coalesce(movimentacao.dtbanco,movimentacao.dtmovimentacao) >= conta.dtsaldo")
					.closeParentheses()
				.closeParentheses();
				
				if(buscarMovComOrigem){
					q.openParentheses();
						q.where("agendamentoOrigem is not null");
						q.or();
						q.where("documentoacaoOrigem is not null");
						q.or();
						q.where("contaOrigem is not null");
					q.closeParentheses();
				}else {
					q.where("origem is null");
					q.where("conta.dtsaldo is not null");
				}
			}else {
				q.where("coalesce(movimentacao.dtbanco,movimentacao.dtmovimentacao) >= ?",filtro.getPeriodoDe());
				q.where("coalesce(movimentacao.dtbanco,movimentacao.dtmovimentacao) <= ?",filtro.getPeriodoAte());
			}
			q.where("movimentacaoacao <> ?",Movimentacaoacao.CANCELADA);
			q.where("conta.contatipo.cdcontatipo <> " + Contatipo.CARTAO_DE_CREDITO);
			if(Util.booleans.isFalse(filtro.getShowMovimentacoes())){
				q.where("1=0");
			}
			
			if(filtro.getRadCaixa() == null){
				q.where("conta.contatipo.cdcontatipo <> " + Contatipo.CAIXA);
			}
			
			if(filtro.getRadContbanc() == null){
				q.where("conta.contatipo.cdcontatipo <> " + Contatipo.CONTA_BANCARIA);
			}
		q.closeParentheses();
		
		
		/**
		 * Outros
		 */
		q.openParentheses();
			
			q.openParentheses();
				List<ItemDetalhe> listaFornecedor = filtro.getListaFornecedor();
				if(listaFornecedor != null && listaFornecedor.size() > 0){
						q.where("pessoaDocOrigem.cdpessoa is not null");
						q.whereIn("pessoaDocOrigem.cdpessoa", CollectionsUtil.listAndConcatenate(listaFornecedor, "fornecedor.cdpessoa", ","));
						q.or();
						q.where("pessoaAgOrigem.cdpessoa is not null");
						q.whereIn("pessoaAgOrigem.cdpessoa", CollectionsUtil.listAndConcatenate(listaFornecedor, "fornecedor.cdpessoa", ","));
				}
			
				q.or();
				
				List<ItemDetalhe> listaCliente = filtro.getListaCliente();
				if(listaCliente != null && listaCliente.size() > 0){
					q.where("pessoaDocOrigem.cdpessoa is not null");
					q.whereIn("pessoaDocOrigem.cdpessoa", CollectionsUtil.listAndConcatenate(listaCliente, "cliente.cdpessoa", ","));
					q.or();
					q.where("pessoaAgOrigem.cdpessoa is not null");
					q.whereIn("pessoaAgOrigem.cdpessoa", CollectionsUtil.listAndConcatenate(listaCliente, "cliente.cdpessoa", ","));
				}
			q.closeParentheses();
			
			// WHERE IN NA LISTA DE PROJETO
			if(filtro.getRadProjeto() != null){
				String idProjeto = String.valueOf(filtro.getRadProjeto().getId());
				if(idProjeto.equals("escolherProjeto") && filtro.getListaProjeto() != null && !filtro.getListaProjeto().isEmpty()){
					q.whereIn("projeto.cdprojeto", CollectionsUtil.listAndConcatenate(filtro.getListaProjeto(), "projeto.cdprojeto", ","));
				}else if(idProjeto.equals("comProjeto")){
					q.where("projeto is not null");
				}else if(filtro.getRadProjeto().getId().equals("semProjeto")){
					q.where("projeto is null");
				}
			}
			
			// WHEREIN LISTA DE CENTRO DE CUSTO
			List<ItemDetalhe> listaCentrocusto = filtro.getListaCentrocusto();
			if(listaCentrocusto != null && listaCentrocusto.size() > 0){
				q.whereIn("centrocusto.cdcentrocusto", CollectionsUtil.listAndConcatenate(listaCentrocusto, "centrocusto.cdcentrocusto", ","));
			}
			
			// WHEREIN LISTA DE TIPO DE PROJETO
			List<ItemDetalhe> listaProjetotipo = filtro.getListaProjetotipo();
			if (listaProjetotipo != null && listaProjetotipo.size() > 0) {
				q.whereIn("projetotipo.cdprojetotipo", CollectionsUtil.listAndConcatenate(listaProjetotipo, "projetotipo.cdprojetotipo", ","));
			}
			
			// WHEREIN LISTA DE EMPRESA
			List<ItemDetalhe> listaEmpresa = filtro.getListaEmpresa();
			if(SinedUtil.isListNotEmpty(listaEmpresa)){
				q.openParentheses();
					q.whereIn("empresaMov.cdpessoa", CollectionsUtil.listAndConcatenate(listaEmpresa, "empresa.cdpessoa", ","));
					q.or();
					q.openParentheses();
						q.where("empresaMov is null");
						q.whereIn("empresa.cdpessoa", CollectionsUtil.listAndConcatenate(listaEmpresa, "empresa.cdpessoa", ","));
					q.closeParentheses();
				q.closeParentheses();
			}
			
		q.closeParentheses();
		
		
		
		/**
		 * V�nculo
		 */
		q.openParentheses();
						
			//WHEREIN LISTA DE CAIXA
			List<ItemDetalhe> listaCaixa = filtro.getListaCaixa();
			if(filtro.getRadCaixa() != null){
				q.where("conta.contatipo.cdcontatipo = " + Contatipo.CAIXA);
				if(Util.booleans.isFalse(filtro.getRadCaixa()) && SinedUtil.isListNotEmpty(listaCaixa)){
					// Op��o 'Escolher'
					q.whereIn("conta.cdconta", CollectionsUtil.listAndConcatenate(listaCaixa, "conta.cdconta", ","));
				}
			}
			
			q.or();
				
			// WHEREIN LISTA DE CONTA BANC�RIA
			List<ItemDetalhe> listaContabancaria = filtro.getListaContabancaria();
			if(filtro.getRadContbanc() != null){
				q.where("conta.contatipo.cdcontatipo = " + Contatipo.CONTA_BANCARIA);
				if(Util.booleans.isFalse(filtro.getRadContbanc()) && SinedUtil.isListNotEmpty(listaContabancaria)){
					// Op��o 'Escolher'
					q.whereIn("conta.cdconta", CollectionsUtil.listAndConcatenate(listaContabancaria, "conta.cdconta", ","));
				}
			}
		q.closeParentheses();	
		
//		WHEREIN PARA A LISTA DE NATUREZA DA CONTA GERENCIAL
		q.whereIn("contagerencial.natureza", NaturezaContagerencial.listAndConcatenate(filtro.getListanaturezaContaGerencial()));
		
		return q.list();
	}
	
	/**
	 * M�todo para obter as movimenta��es relacionadas a cart�es de cr�dito para o relat�rio de fluxo de caixa.
	 * 
	 * @see #createQueryFluxocaixa(QueryBuilder)
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 * @param considerarApenasAnteriores 
	 */
	public List<Movimentacao> findMovCartoesForFluxocaixa(FluxocaixaFiltroReport filtro, Boolean considerarApenasAnteriores){
		QueryBuilder<Movimentacao> q = query();
		
		/**
		 * Inicio da query: Select e joins
		 */
		this.createQueryFluxocaixa(q);
				
		/**
		 * Geral
		 */
		if(filtro.getMostrarContasInativadas() == null || !filtro.getMostrarContasInativadas()){
			q.openParentheses();
				q.openParentheses();
					q.where("contatipo = ?", Contatipo.TIPO_CONTA_BANCARIA);
					q.where("conta.ativo = true");
				q.closeParentheses();
				q.or();
				q.where("contatipo <> ?", Contatipo.TIPO_CONTA_BANCARIA);
			q.closeParentheses();
		}
		
		q.openParentheses();
			if(considerarApenasAnteriores != null && considerarApenasAnteriores){
				q.where("coalesce(movimentacao.dtbanco,movimentacao.dtmovimentacao) < ?", SinedDateUtils.incrementDate(filtro.getPeriodoDe(),2,Calendar.MONTH));
				q.where("origem is null");
			}else {
				q.where("coalesce(movimentacao.dtbanco,movimentacao.dtmovimentacao) >= ?", SinedDateUtils.incrementDate(filtro.getPeriodoDe(),-2,Calendar.MONTH));
				q.where("coalesce(movimentacao.dtbanco,movimentacao.dtmovimentacao) <= ?", SinedDateUtils.incrementDate(filtro.getPeriodoAte(),2,Calendar.MONTH));
			}
			q.where("movimentacaoacao <> ?",Movimentacaoacao.CANCELADA);
			q.where("conta.contatipo.cdcontatipo = " + Contatipo.CARTAO_DE_CREDITO);
			if(Util.booleans.isFalse(filtro.getShowMovimentacoes()) || filtro.getRadCartao() == null){
				q.where("1=0");
			}

		q.closeParentheses();
		
		
		/**
		 * Outros
		 */
		q.openParentheses();
			
			q.openParentheses();
				List<ItemDetalhe> listaFornecedor = filtro.getListaFornecedor();
				if(listaFornecedor != null && listaFornecedor.size() > 0){
						q.where("pessoaDocOrigem.cdpessoa is not null");
						q.whereIn("pessoaDocOrigem.cdpessoa", CollectionsUtil.listAndConcatenate(listaFornecedor, "fornecedor.cdpessoa", ","));
						q.or();
						q.where("pessoaAgOrigem.cdpessoa is not null");
						q.whereIn("pessoaAgOrigem.cdpessoa", CollectionsUtil.listAndConcatenate(listaFornecedor, "fornecedor.cdpessoa", ","));
				}
			
				q.or();
				
				List<ItemDetalhe> listaCliente = filtro.getListaCliente();
				if(listaCliente != null && listaCliente.size() > 0){
					q.where("pessoaDocOrigem.cdpessoa is not null");
					q.whereIn("pessoaDocOrigem.cdpessoa", CollectionsUtil.listAndConcatenate(listaCliente, "cliente.cdpessoa", ","));
					q.or();
					q.where("pessoaAgOrigem.cdpessoa is not null");
					q.whereIn("pessoaAgOrigem.cdpessoa", CollectionsUtil.listAndConcatenate(listaCliente, "cliente.cdpessoa", ","));
				}
			q.closeParentheses();
			
			// WHERE IN NA LISTA DE PROJETO
			if(filtro.getRadProjeto() != null){
				String idProjeto = String.valueOf(filtro.getRadProjeto().getId());
				if(idProjeto.equals("escolherProjeto") && filtro.getListaProjeto() != null && !filtro.getListaProjeto().isEmpty()){
					q.whereIn("projeto.cdprojeto", CollectionsUtil.listAndConcatenate(filtro.getListaProjeto(), "projeto.cdprojeto", ","));
				}else if(idProjeto.equals("comProjeto")){
					q.where("projeto is not null");
				}else if(filtro.getRadProjeto().getId().equals("semProjeto")){
					q.where("projeto is null");
				}
			}
						
			// WHEREIN LISTA DE CENTRO DE CUSTO
			List<ItemDetalhe> listaCentrocusto = filtro.getListaCentrocusto();
			if(listaCentrocusto != null && listaCentrocusto.size() > 0){
				q.whereIn("centrocusto.cdcentrocusto", CollectionsUtil.listAndConcatenate(listaCentrocusto, "centrocusto.cdcentrocusto", ","));
			}
			
			// WHEREIN LISTA DE TIPO DE PROJETO
			List<ItemDetalhe> listaProjetotipo = filtro.getListaProjetotipo();
			if (listaProjetotipo != null && listaProjetotipo.size() > 0) {
				q.whereIn("projetotipo.cdprojetotipo", CollectionsUtil.listAndConcatenate(listaProjetotipo, "projetotipo.cdprojetotipo", ","));
			}
			
			// WHEREIN LISTA DE EMPRESA
			List<ItemDetalhe> listaEmpresa = filtro.getListaEmpresa();
			if(SinedUtil.isListNotEmpty(listaEmpresa)){
				q.whereIn("empresa.cdpessoa", CollectionsUtil.listAndConcatenate(listaEmpresa, "empresa.cdpessoa", ","));
			}
			
		q.closeParentheses();
		
		
		
		/**
		 * V�nculo
		 */
		q.openParentheses();
			
			// WHEREIN LISTA DE CART�O DE CR�DITO
			List<ItemDetalhe> listaCartao = filtro.getListaCartao();
			if(filtro.getRadCartao() != null){
				if(Util.booleans.isFalse(filtro.getRadCartao()) && SinedUtil.isListNotEmpty(listaCartao)){
					// Op��o 'Escolher'
					q.whereIn("conta.cdconta", CollectionsUtil.listAndConcatenate(listaCartao, "conta.cdconta", ","));
				}
			}

		q.closeParentheses();
		
//		WHEREIN PARA A LISTA DE NATUREZA DA CONTA GERENCIAL
		q.whereIn("contagerencial.natureza", NaturezaContagerencial.listAndConcatenate(filtro.getListanaturezaContaGerencial()));
		
		return q.list();
	}
	
	/**
	 * Carrega movimenta��o para concilia��o manual.
	 * 
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 * @author Fl�vio Tavares - Inclus�o do rateio
	 */
	public Movimentacao carregaMovimentacao(Movimentacao bean) {
		if (bean == null || bean.getCdmovimentacao() == null) {
			throw new SinedException("Movimenta��o n�o pode ser nulo.");
		}
		return query()
					.select("movimentacao.cdmovimentacao, aux_movimentacao.movimentacaodescricao, conta.dtsaldo, rateio.cdrateio, movimentacao.observacaocopiacheque, " +
							"contatipo.cdcontatipo, conta.cdconta, movimentacao.dtmovimentacao, movimentacao.checknum, movimentacao.historico, movimentacao.dtbanco")
					.leftOuterJoin("movimentacao.rateio rateio")
					.join("movimentacao.aux_movimentacao aux_movimentacao")
					.join("movimentacao.conta conta")
					.leftOuterJoin("conta.contatipo contatipo")
					.where("movimentacao = ?", bean)
					.unique();
	}
	
	public List<Movimentacao> carregaMovimentacao(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Movimenta��o n�o pode ser nulo.");
		}
		return query()
.select("movimentacao.cdmovimentacao, aux_movimentacao.movimentacaodescricao, conta.cdconta, conta.dtsaldo, rateio.cdrateio, " +
				"movimentacaoacao.cdmovimentacaoacao, movimentacao.valor, movimentacao.tipooperacao, movimentacao.checknum, movimentacao.observacaocopiacheque, " +
				"contatipo.cdcontatipo")
		.leftOuterJoin("movimentacao.rateio rateio")
		.join("movimentacao.aux_movimentacao aux_movimentacao")
		.join("movimentacao.conta conta")
		.leftOuterJoin("conta.contatipo contatipo")
		.join("movimentacao.movimentacaoacao movimentacaoacao")
		.whereIn("movimentacao.cdmovimentacao", whereIn)
		.list();
	}
	
	/**
	 * Carrega todas as movimenta��es de uma conta com situa��o 'NORMAL'. 
	 * 
	 * @param conta
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Movimentacao> findByConta(Conta conta) {
		if (conta == null || conta.getCdconta() == null) {
			throw new SinedException("Conta n�o pode ser nula.");
		}
		return query()
					.select("movimentacao.dtmovimentacao, movimentacaoacao.cdmovimentacaoacao, movimentacao.cdmovimentacao, " +
							"movimentacao.checknum, cheque.cdcheque, cheque.numero, movimentacao.valor, tipooperacao.cdtipooperacao, " +
							"movimentacao.dtbanco, movimentacao.fitid, formapagamento.cdformapagamento")
					.join("movimentacao.tipooperacao tipooperacao")
					.join("movimentacao.movimentacaoacao movimentacaoacao")
					.leftOuterJoin("movimentacao.cheque cheque")
					.join("movimentacao.conta conta")
					.leftOuterJoin("conta.listaContaempresa listaContaempresa")
					.leftOuterJoin("listaContaempresa.empresa empresa")
					.leftOuterJoin("movimentacao.formapagamento formapagamento")
					.where("conta = ?",conta)
					.where("not exists ( SELECT f.id " +
								" FROM Fechamentofinanceiro f " +
								" LEFT OUTER JOIN f.conta c " +
								" WHERE f.empresa = empresa " +
								" AND f.datainicio <= movimentacao.dtmovimentacao " +
								" AND f.datalimite >= movimentacao.dtmovimentacao " +
								" AND coalesce(f.ativo, false) = true " +
								" AND (c.cdconta = " + conta.getCdconta() + " OR c is null) ) ")
					.list();
	}
	/**
	 * Carrega a lista de movimenta��es a ser associada na tela de concilia��o.
	 * 
	 * @param conta
	 * @param tipo
	 * @return
	 * @author Rodrigo Freitas
	 * @param whereNotIn 
	 */
	public List<Movimentacao> findByContaTipoOperacao(Conta conta, Tipooperacao tipo, String whereNotIn) {
		QueryBuilder<Movimentacao> query = query()
					.select("movimentacao.cdmovimentacao, aux_movimentacao.movimentacaodescricao, movimentacao.dtmovimentacao, cheque.numero, movimentacao.valor, op.nome")
					.join("movimentacao.tipooperacao op")
					.join("movimentacao.aux_movimentacao aux_movimentacao")
					.leftOuterJoin("movimentacao.cheque cheque")
					.join("movimentacao.conta conta")
					.leftOuterJoin("conta.listaContaempresa listaContaempresa")
					.leftOuterJoin("listaContaempresa.empresa empresa")
					.where("conta = ?",conta)
					.where("not exists ( SELECT f.id " +
								" FROM Fechamentofinanceiro f " +
								" WHERE f.empresa = empresa " +
								" AND f.ativo = true " +
								" AND f.conta = conta " +
								" AND f.datainicio <= movimentacao.dtmovimentacao " +
								" AND f.datalimite >= movimentacao.dtmovimentacao )")
					.where("movimentacao.tipooperacao = ?",tipo)
					.where("movimentacao.movimentacaoacao = ?",Movimentacaoacao.NORMAL)
					.orderBy("movimentacao.dtmovimentacao, movimentacao.valor, aux_movimentacao.movimentacaodescricao");
		
		if (whereNotIn != null && !whereNotIn.equals("")) {
			query.where("movimentacao.cdmovimentacao not in ("+whereNotIn+")");
		}
		
		return query.list();
	}
	
	/**
	 * Carrega a movimenta��o do documento passado por par�metro.
	 *
	 * @param form
	 * @return
	 */
	public Movimentacao findByDocumento(Documento form, String whereInMovimentacao) {
		if (form == null || form.getCddocumento() == null) {
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		return querySined()
					
					.select("movimentacao.cdmovimentacao, movimentacao.dtbanco, movimentacao.dtmovimentacao, movimentacao.valor, movimentacaoacao.cdmovimentacaoacao, movimentacaorelacionada.valor, movimentacaoacaorelacionada.cdmovimentacaoacao, movimentacaorelacionada.taxageradaprocessamentocartao, tipooperacaorelacionada.cdtipooperacao")
					.join("movimentacao.listaMovimentacaoorigem movimentacaoorigem")
					.leftOuterJoin("movimentacaoorigem.movimentacaorelacionada movimentacaorelacionada")
					.leftOuterJoin("movimentacaorelacionada.tipooperacao tipooperacaorelacionada")
					.leftOuterJoin("movimentacaorelacionada.movimentacaoacao movimentacaoacaorelacionada")
					.join("movimentacaoorigem.documento documento")
					.join("movimentacao.movimentacaoacao movimentacaoacao")
					.where("documento = ?",form)
					.where("movimentacaoacao <> ?",Movimentacaoacao.CANCELADA)
					.whereIn("movimentacao.cdmovimentacao", StringUtils.isNotBlank(whereInMovimentacao) ? whereInMovimentacao : null)
					.setMaxResults(1)
					.unique();
	}
	
	/**
	 * M�todo usado para obter as movimenta��es geradas com a baixa do documento.
	 * Tamb�m obtem propriedades do documento e do rateio da movimentacao.
	 * 
	 * @param documento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Movimentacao> findMovimentacaoGerada(Documento documento){
		if(!SinedUtil.isObjectValid(documento)){
			throw new SinedException("Parametros inv�lidos em documento.");
		}

//		String subselect = query()
//			.select("max(movimentacao.cdmovimentacao)")
//			.join("movimentacao.listaMovimentacaoorigem mvoigem")
//			.join("mvoigem.documento doc")
//			.where("doc = ?")
//			.getQuery();
		
		
//		String subselect = "select max(movSUB.cdmovimentacao) " +
//							"from Movimentacao movSUB " +
//							"join movSUB.listaMovimentacaoorigem listaMovimentacaoorigemSUB " +
//							"join listaMovimentacaoorigemSUB.documento documentoSUB " +
//							"where documentoSUB.cddocumento = " + documento.getCddocumento();
		
		
		
		return query()
			.select("movimentacao.cdmovimentacao,movimentacao.valor,movimentacaoacao.cdmovimentacaoacao,origem.cdmovimentacaoorigem," +
					"documento.cddocumento,documento.numero,documento.descricao,documento.dtemissao,documento.dtvencimento," +
					"documentoacao.cddocumentoacao,aux_documento.valoratual,documento.tipopagamento, pessoa.cdpessoa")
			
			.join("movimentacao.movimentacaoacao movimentacaoacao")
			
			.join("movimentacao.listaMovimentacaoorigem origem")
			.join("origem.movimentacao movorigem")
			.join("origem.documento documento")
			.join("documento.documentoacao documentoacao")
			.join("documento.aux_documento aux_documento")
			.leftOuterJoin("documento.pessoa pessoa")
			.leftOuterJoin("documento.documentoclasse documentoclasse")
			.where("otimizacao_movimentacao_gerada(movimentacao.cdmovimentacao, " + documento.getCddocumento() + ") = true ")
			.where("movimentacaoacao <> ?", Movimentacaoacao.CANCELADA)
			.list();

		
	}
	
	/**
	 * Retorna as movimenta��es e seus itens de rateio relacionadas a um determinado projeto.
	 * 
	 * @param projeto
	 * @return lista de Object
	 * @throws SinedException - Caso o par�metro seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Object> findByProjeto(Projeto projeto) {
		if (projeto == null || projeto.getCdprojeto() == null) {
			throw new SinedException("Parametros inv�lidos. O campo projeto n�o pode ser nulo.");
		}
		
//		Essa query s� retornar� os resultados corretamente quando o QueryResultTranslator estiver corrigido.
//		return
//			query()			
//				.select("movimentacao.cdmovimentacao,movimentacao.dtmovimentacao, movimentacao.dtbanco," +
//						"rateio.cdrateio," +
//						"item.cdrateioitem,item.valor")
//				.leftOuterJoin("movimentacao.rateio rateio")
//				.leftOuterJoin("rateio.listaRateioitem item")
//				.leftOuterJoin("item.projeto projeto")
//				.join("movimentacao.movimentacaoacao movimentacaoacao")
//				.where("movimentacaoacao <> ?",Movimentacaoacao.CANCELADA)				
//				.where("projeto = ?", projeto)
//				.list();		
		
		List<Object> listaObjeto = newQueryBuilderSined(Object.class)			
			.select("movimentacao.cdmovimentacao,movimentacao.dtmovimentacao, movimentacao.dtbanco," +
					"rateio.cdrateio," +
					"item.cdrateioitem,item.valor")
			.from(Movimentacao.class)
			.setUseTranslator(false)					
			.leftOuterJoin("movimentacao.rateio rateio")
			.leftOuterJoin("rateio.listaRateioitem item")
			.leftOuterJoin("item.projeto projeto")
			.join("movimentacao.movimentacaoacao movimentacaoacao")
			.where("movimentacaoacao <> ?",Movimentacaoacao.CANCELADA)				
			.where("projeto = ?", projeto)
			.list();
			
		return listaObjeto;			
	}
	
	public String criaQueryAnaliseRecDesp(AnaliseReceitaDespesaReportFiltro filtro, String identificador, Boolean max, String campo, String ordenacao) {
		
		StringBuilder query = new StringBuilder();

		query.append(" SELECT * from ( ");
		query.append(" SELECT movimentacao.cdmovimentacao AS cdmovimentacao, ");
		query.append(" aux_movimentacao.movimentacaodescricao AS descricao, ");
		query.append(" movimentacao.dtbanco AS dtbanco, ");
		query.append(" movimentacao.dtmovimentacao AS dtmovimentacao, ");
		query.append(" SUM(rateioitem.valor) AS valor ");
		query.append(contagerencialDAO.criaFrom(filtro));
		query.append(" AND vcontagerencial.identificador like '" + identificador + "%'");
		query.append(" GROUP BY movimentacao.cdmovimentacao, aux_movimentacao.movimentacaodescricao, movimentacao.dtbanco ");
		query.append(" ORDER BY movimentacao.dtbanco desc ");
		
		if(StringUtils.isEmpty(campo)
				|| (filtro.getOrigem().equals(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER) 
						&& filtro.getExibirMovimentacaoSemVinculoDocumento() != null
						&& filtro.getExibirMovimentacaoSemVinculoDocumento())) {
			query.append(") as query ORDER BY query.valor desc ");			
		}else{
			query.append(") as query ORDER BY query." + campo + " " + ordenacao);
		}
		
		return query.toString();
	}
	
	/**
	 * Busca a lista de movimenta��es daquela contagerencial e de todas suas filhas
	 *
	 * @param identificador
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<Movimentacao> findForAnaliseRecDespesaFlex(AnaliseReceitaDespesaReportFiltro filtro, String identificador, Boolean max, String campo, String ordenacao) {
		if (identificador == null || identificador.equals("")) {
			throw new SinedException("Identificador n�o pode ser nulo.");
		}
//		AnaliseReceitaDespesaReportFiltro filtro = (AnaliseReceitaDespesaReportFiltro)NeoWeb.getRequestContext().getSession().getAttribute(ContagerencialService.FILTRO_SESSION);
		
		String sql = this.criaQueryAnaliseRecDesp(filtro, identificador, max, campo, ordenacao);
//		System.out.println("QUERY: " + sql);
		
		SinedUtil.markAsReader();
		List<Movimentacao> lista = getJdbcTemplate().query(sql, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Movimentacao movimentacao = new Movimentacao();
				
				movimentacao.setCdmovimentacao(rs.getInt("CDMOVIMENTACAO"));
				movimentacao.setValor(new Money(rs.getDouble("VALOR"), true));
//				try {
//					movimentacao.setDtbanco(SinedDateUtils.stringToDate(rs.getString("DTBANCO"), "yyyy-MM-dd"));
//				} catch (ParseException e) {
//					e.printStackTrace();
//				}
				movimentacao.setDtbanco(rs.getDate("DTBANCO"));
				movimentacao.setDtmovimentacao(rs.getDate("DTMOVIMENTACAO"));

				if(movimentacao.getDtbanco()!=null){
					movimentacao.setDtbanco(SinedDateUtils.setDateProperty(movimentacao.getDtbanco(), 12, Calendar.HOUR_OF_DAY));					
				}else{
					movimentacao.setDtbanco(SinedDateUtils.setDateProperty(movimentacao.getDtmovimentacao(), 12, Calendar.HOUR_OF_DAY));					
				}
				
				Aux_movimentacao aux_movimentacao = new Aux_movimentacao();
				aux_movimentacao.setMovimentacaodescricao(rs.getString("DESCRICAO"));
				movimentacao.setAux_movimentacao(aux_movimentacao);
				
				return movimentacao;
			}

		});
		
		return lista;
//		
//		QueryBuilder<Movimentacao> query = query()
//					.select("movimentacao.cdmovimentacao, movimentacao.valor, movimentacao.dtbanco, aux_movimentacao.movimentacaodescricao, listaRateioitem.valor")
//					.join("movimentacao.aux_movimentacao aux_movimentacao")
//					.join("movimentacao.conta conta")
//					.leftOuterJoin("conta.empresa empresa")
//					.leftOuterJoin("movimentacao.rateio rateio")
//					.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
//					.leftOuterJoin("listaRateioitem.projeto projeto")
//					.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
//					.leftOuterJoin("listaRateioitem.contagerencial contagerencial")
//					.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
//					.join("movimentacao.movimentacaoacao movimentacaoacao")
//					.join("movimentacao.tipooperacao tipooperacao")
//					.where("vcontagerencial.identificador like ?", identificador + "%")
//					.where("movimentacaoacao = ?",Movimentacaoacao.CONCILIADA)
//					.openParentheses()
//					.where("contagerencial.desconsideraAnalise = false")
//					.or()
//					.where("contagerencial.desconsideraAnalise is null")
//					.closeParentheses()
//					.where("movimentacao.dtbanco >= ?", filtro.getDtInicio())
//					.where("movimentacao.dtbanco <= ?", filtro.getDtFim())
//					.orderBy("movimentacao.dtmovimentacao desc");
//		
//		if (max) {
//			query.setMaxResults(10);
//		}
//		
//		//FILTRO DE EMPRESA
//		if(filtro.getListaEmpresa() != null && filtro.getListaEmpresa().size() > 0){
//			query.openParentheses();
//			for (ItemDetalhe id : filtro.getListaEmpresa()) {
//				query.where("empresa = ?", id.getEmpresa()).or();
//			}
//			query.closeParentheses();
//		}
//		
//		//FILTRO DE PROJETO
//		if(filtro.getListaProjeto() != null && filtro.getListaProjeto().size() > 0){
//			query.openParentheses();
//			for (ItemDetalhe id : filtro.getListaProjeto()) {
//				query.where("projeto = ?", id.getProjeto()).or();
//			}
//			query.closeParentheses();
//		} else if (filtro.getRadProjeto().getId().equals("comProjeto")) {
//			query.where("projeto is not null");
//		} else if (filtro.getRadProjeto().getId().equals("semProjeto")) {
//			query.where("projeto is null");
//		}
//		
//		//FILTRO DE CENTRO DE CUSTO
//		if(filtro.getListaCentrocusto() != null && filtro.getListaCentrocusto().size() > 0){
//			query.openParentheses();
//			for (ItemDetalhe id : filtro.getListaCentrocusto()) {
//				query.where("centrocusto = ?", id.getCentrocusto()).or();
//			}
//			query.closeParentheses();
//		}
//		
////		String cdsColaborador = listaColaborador != null && listaColaborador.size() > 0 ? CollectionsUtil.listAndConcatenate(listaColaborador, "colaborador.cdpessoa", ",") : "";
////		String cdsFornecedor = listaFornecedor != null && listaFornecedor.size() > 0 ? CollectionsUtil.listAndConcatenate(listaFornecedor, "fornecedor.cdpessoa", ",") : "";
////		String cdsOutros = CollectionsUtil.listAndConcatenate(listaOutrospagamento, "outrospagamento", "','");
//		
//		String wherePessoa = "";
//		boolean comeco = false;
//		
//		//FILTRO CLIENTE
//		if(filtro.getListaCliente() != null && filtro.getListaCliente().size() > 0){
//			
//			if(comeco) wherePessoa += " or ";
//			else comeco = true;
//			
//			String cdsCliente = CollectionsUtil.listAndConcatenate(filtro.getListaCliente(), "cliente.cdpessoa", ",");
//			wherePessoa += " exists (select m.id " +
//									"from Movimentacao m " +
//									"join m.listaMovimentacaoorigem listaMovimentacaoorigem " +
//									"join listaMovimentacaoorigem.documento doc " +
//									"join doc.pessoa pes " +
//									"where m.id = movimentacao.id " +
//									"and doc.cdpessoa IN (" + cdsCliente + ") " +
//									"and doc.tipopagamento";
//		}
//		
//		//FILTRO FORNECEDOR
//		if(filtro.getListaFornecedor() != null && filtro.getListaFornecedor().size() > 0){
//			
//			if(comeco) wherePessoa += " or ";
//			else comeco = true;
//			
//			whereIn = CollectionsUtil.listAndConcatenate(filtro.getListaFornecedor(), "fornecedor.cdpessoa", ",");
//			wherePessoa += " pessoa.id in (" + whereIn + ") ";
//		}
//		
//		//FILTRO COLABORADOR
//		if(filtro.getListaColaborador() != null && filtro.getListaColaborador().size() > 0){
//			
//			if(comeco) wherePessoa += " or ";
//			else comeco = true;
//			
//			whereIn = CollectionsUtil.listAndConcatenate(filtro.getListaColaborador(), "colaborador.cdpessoa", ",");
//			wherePessoa += " pessoa.id in (" + whereIn + ") ";
//		}
//		
//		//FILTRO OUTROS PAGAMENTOS
//		if(filtro.getListaOutrospagamento() != null && filtro.getListaOutrospagamento().size() > 0){
//			
//			if(comeco) wherePessoa += " or ";
//			else comeco = true;
//			
//			whereIn = CollectionsUtil.listAndConcatenate(filtro.getListaColaborador(), "outrospagamento", "','");
//			wherePessoa += " documento.outrospagamento in ('" + whereIn + "') ";
//		}
//
//		
//		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Movimentacao> findForAnaliseRecDespesaFlexMovimentacaoAvulsa(String identificador, Boolean max, String campo, String ordenacao, String whereNotInMovimentacao) {
		if (identificador == null || identificador.equals("")) { throw new SinedException("Identificador n�o pode ser nulo."); }
		AnaliseReceitaDespesaReportFiltro filtro = (AnaliseReceitaDespesaReportFiltro)NeoWeb.getRequestContext().getSession().getAttribute(ContagerencialService.FILTRO_SESSION);
		
		String sql = this.criaQueryAnaliseRecDesp(filtro, identificador, max, campo, ordenacao);
//		System.out.println("QUERY: " + sql);

		SinedUtil.markAsReader();
		List<Movimentacao> lista = getJdbcTemplate().query(sql, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Movimentacao movimentacao = new Movimentacao();
				
				movimentacao.setCdmovimentacao(rs.getInt("CDMOVIMENTACAO"));
				movimentacao.setValor(new Money(rs.getDouble("VALOR"), true));
				movimentacao.setDtbanco(rs.getDate("DTBANCO"));
				movimentacao.setDtmovimentacao(rs.getDate("DTMOVIMENTACAO"));

				if(movimentacao.getDtbanco()!=null){
					movimentacao.setDtbanco(SinedDateUtils.setDateProperty(movimentacao.getDtbanco(), 12, Calendar.HOUR_OF_DAY));					
				}else{
					movimentacao.setDtbanco(SinedDateUtils.setDateProperty(movimentacao.getDtmovimentacao(), 12, Calendar.HOUR_OF_DAY));					
				}
				
				Aux_movimentacao aux_movimentacao = new Aux_movimentacao();
				aux_movimentacao.setMovimentacaodescricao(rs.getString("DESCRICAO"));
				movimentacao.setAux_movimentacao(aux_movimentacao);
				
				return movimentacao;
			}

		});
		
		return lista;
	}
	
	/**
	 * Deleta as movimenta��es ao estornar uma depsesa de viagem.
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void deleteFromDespesa(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		getJdbcTemplate().execute("DELETE " +
									"FROM MOVIMENTACAO  " +
									"WHERE EXISTS( " +
									"SELECT 1 " +
									"FROM MOVIMENTACAOORIGEM " +
									"WHERE MOVIMENTACAO.CDMOVIMENTACAO = MOVIMENTACAOORIGEM.CDMOVIMENTACAO " +
									"AND (MOVIMENTACAOORIGEM.CDDESPESAVIAGEMADIANTAMENTO IN ("+whereIn+")  " +
									"OR MOVIMENTACAOORIGEM.CDDESPESAVIAGEMACERTO IN ("+whereIn+")) " +
									")");
	}
	
	/**
	 * Deleta a movimenta��o financeira.
	 *
	 * @param movimentacao
	 * @author Rodrigo Freitas
	 */
	public void deleteMovimentacao(Movimentacao movimentacao) {
		if (movimentacao == null || movimentacao.getCdmovimentacao() == null) {
			throw new SinedException("Movimenta��o n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("DELETE " +
				"FROM MOVIMENTACAO M " +
				"WHERE M.CDMOVIMENTACAO = " + movimentacao.getCdmovimentacao());
	}
	
	/**
	 * Faz a chamada da procedure para atualizar a descri��o da movimenta��o
	 *
	 * @param documento
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaMovimentacao(Movimentacao movimentacao){
		if (movimentacao == null || movimentacao.getCdmovimentacao() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		getJdbcTemplate().execute("SELECT ATUALIZA_MOVIMENTACAO("+movimentacao.getCdmovimentacao()+")");
	}
	
	public void callProcedureAtualizaMovimentacaoNecessidade(){
		getJdbcTemplate().execute("SELECT ATUALIZA_MOVIMENTACAO(M.CDMOVIMENTACAO) FROM MOVIMENTACAO M WHERE NOT EXISTS (SELECT * FROM AUX_MOVIMENTACAO AM WHERE AM.CDMOVIMENTACAO = M.CDMOVIMENTACAO)");
	}
	
	/**
	 * Carrego a lista de movimenta��o financeira para o relat�rio de movimenta��o de cheques
	 *
	 * @param whereIn
	 * @author Thiago Clemente
	 */
	@SuppressWarnings("unchecked")
	public List<Movimentacao> findForChequeMovimentacao(String whereIn, String whereInCheque){
		
		String whereInMovimentacaoCheque = null;
		if(StringUtils.isNotBlank(whereInCheque)){
			String sql = "select (select m.cdmovimentacao " +
					" from Movimentacao m  " +
					" where m.cdcheque = c.cdcheque " + 
					" and m.cdformapagamento = " + Formapagamento.CHEQUE.getCdformapagamento() +
					" and m.cdmovimentacaoacao <> " + Movimentacaoacao.CANCELADA.getCdmovimentacaoacao() +
					" order by m.dtmovimentacao desc " +
					" limit 1) " +
					" from Cheque c " +
					" where c.cdcheque in ("+whereInCheque+")";

			SinedUtil.markAsReader();
			List<Movimentacao> lista = getJdbcTemplate().query(sql, 
					new RowMapper() {
						public Movimentacao mapRow(ResultSet rs, int rowNum) throws SQLException {
							return new Movimentacao(rs.getInt("cdmovimentacao"));
						}
					});
			
			if(SinedUtil.isListNotEmpty(lista)){
				whereInMovimentacaoCheque = CollectionsUtil.listAndConcatenate(lista, "cdmovimentacao", ",");
			}
		}
		
		if ((whereIn==null || whereIn.trim().equals("")) && (whereInMovimentacaoCheque==null || whereInMovimentacaoCheque.trim().equals(""))) {
			return new ArrayList<Movimentacao>(); 
		}
		
		QueryBuilder<Movimentacao> query = query()
				.select("movimentacao.cdmovimentacao, cheque.numero, movimentacao.valor, movimentacao.historico," +
						"movimentacao.dtmovimentacao, documento.tipopagamento, pessoa.cdpessoa, pessoa.nome")
				.join("movimentacao.formapagamento formapagamento")
				.leftOuterJoin("movimentacao.listaMovimentacaoorigem listaMovimentacaoorigem")
				.leftOuterJoin("listaMovimentacaoorigem.documento documento")
				.leftOuterJoin("documento.pessoa pessoa")
				.leftOuterJoin("movimentacao.cheque cheque")
				.where("formapagamento=?", Formapagamento.CHEQUE)
				.whereIn("movimentacao.cdmovimentacao", whereIn)
				.whereIn("movimentacao.cdmovimentacao", whereInMovimentacaoCheque);
		
			
		return query.list();
	}
	
	/**
	 * Retorna o cheque da movimenta��o do documento.
	 *
	 * @param cdconta
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Movimentacao getChequeDocumento(Integer cdconta) {
//		String subselect = query()
//		.select("max(movimentacao.cdmovimentacao)")
//		.join("movimentacao.listaMovimentacaoorigem mvoigem")
//		.join("mvoigem.documento doc")
//		.where("doc = ?")
//		.getQuery();
		
		String subselect = "select max(movSUB.cdmovimentacao) " +
							"from Movimentacao movSUB " +
							"join movSUB.listaMovimentacaoorigem listaMovimentacaoorigemSUB " +
							"join listaMovimentacaoorigemSUB.documento documentoSUB " +
							"where documentoSUB.cddocumento = " + cdconta;
	
	return query()
			.select("movimentacao.cdmovimentacao, cheque.numero, movimentacao.dtmovimentacao, movimentacao.dtbanco")
			.join("movimentacao.listaMovimentacaoorigem listaMovimentacaoorigem")
			.join("listaMovimentacaoorigem.documento documento")
			.leftOuterJoin("movimentacao.cheque cheque")
			.where("movimentacao.cdmovimentacao in (" + subselect + ")")
			.where("documento.cddocumento = ?", cdconta)
			.unique();
	}
	
	/**
	 * Atualiza o historico da conta com a descri��o que � formada do BD.
	 *
	 * @param movimentacao
	 * @author Rodrigo Freitas
	 */
	public void updateHistoricoWithDescricao(Movimentacao movimentacao) {
		if (movimentacao == null || movimentacao.getCdmovimentacao() == null) {
			throw new SinedException("Movimenta��o n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE MOVIMENTACAO M SET HISTORICO =  " +
									"(SELECT A.MOVIMENTACAODESCRICAO FROM AUX_MOVIMENTACAO A WHERE A.CDMOVIMENTACAO = M.CDMOVIMENTACAO) " +  
									"WHERE CDMOVIMENTACAO = " + movimentacao.getCdmovimentacao());
	}
	
	/**
	 * M�todo que retorna a data da �ltima conta a pagar ou conta a receber baixada no sistema
	 * @param documentoclasse 
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Date getDtUltimaContaPagarReceberBaixada(Documentoclasse documentoclasse) {
		return newQueryBuilderSined(Date.class)
			.from(Movimentacao.class)
			.setUseTranslator(false) 
			.select("max(movimentacao.dtmovimentacao)")
			.join("movimentacao.listaMovimentacaoorigem listaMovimentacaoorigem")
			.join("listaMovimentacaoorigem.documento documento")
			.join("documento.documentoclasse documentoclasse")
			.join("movimentacao.movimentacaoacao movimentacaoacao")
			.where("movimentacaoacao <> ?", Movimentacaoacao.CANCELADA)
			.where("documentoclasse = ?", documentoclasse)
			.unique();
	}
	
	/**
	 * M�todo que retorna a data da �ltima movimenta��o financeira
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Date getDtUltimoCadastro() {
		return newQueryBuilderSined(Date.class)
			
			.from(Movimentacao.class)
			.setUseTranslator(false) 
			.select("max(movimentacao.dtmovimentacao)")
			.join("movimentacao.movimentacaoacao movimentacaoacao")
			.where("movimentacaoacao <> ?", Movimentacaoacao.CANCELADA)
			.unique();
	}
	
	/**
	 * M�todo que retorna quantidade total de movimenta��es financeiras do sistema
	 * 
	 * @return
	 * @authro Tom�s Rabelo
	 */
	public Integer getQtdeTotalMovimentacoes() {
		return newQueryBuilderSined(Long.class)
			
			.from(Movimentacao.class)
			.setUseTranslator(false) 
			.select("count(*)")
			.join("movimentacao.movimentacaoacao movimentacaoacao")
			.where("movimentacaoacao <> ?", Movimentacaoacao.CANCELADA)
			.unique()
			.intValue();
	}
	
	/**
	 * M�todo que retorna a data da �ltima concilia��o banc�ria
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Date getDtUltimaConciliacao() {
		return newQueryBuilderSined(Date.class)
			
			.from(Movimentacao.class)
			.setUseTranslator(false) 
			.select("max(movimentacao.dtbanco)")
			.join("movimentacao.movimentacaoacao movimentacaoacao")
			.where("movimentacaoacao <> ?", Movimentacaoacao.CANCELADA)
			.unique();
	}
	
	/**
	 * M�todo que retorna a quantidade de movimenta��es n�o conciliadas
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getQtdeTotalNaoConciliados() {
		return newQueryBuilderSined(Long.class)
			
			.from(Movimentacao.class)
			.setUseTranslator(false) 
			.select("count(distinct cdmovimentacao)")
			.join("movimentacao.movimentacaoacao movimentacaoacao")
			.where("movimentacaoacao = ?", Movimentacaoacao.NORMAL)
			.unique()
			.intValue();
	}
	
	/**
	 * M�todo que realiza busca geral nas movimenta��es financeiras de acordo com o par�metro
	 * 
	 * @param busca
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Movimentacao> findMovimentacoesForBuscaGeral(String busca) {
		return query()
			.select("movimentacao.cdmovimentacao, movimentacao.historico")
			.whereLikeIgnoreAll("movimentacao.historico", busca)
			.orderBy("movimentacao.historico")
			.list();
	}
	
	/**
	 * M�todo que retorna valores para PDF
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Movimentacao> findforReportListagem(MovimentacaoFiltro filtro) {
		QueryBuilder<Movimentacao> query = query();
		this.updateListagemQuery(query, filtro);
		
		query
			.setUseTranslator(true)
			.setIgnoreJoinPaths(new HashSet<String>());
		
		query
			.select("movimentacao.cdmovimentacao, conta.nome, movimentacao.dtmovimentacao, movimentacao.historico, movimentacao.valor," +
					"tipooperacao.cdtipooperacao, tipooperacao.nome, movimentacao.checknum, movimentacao.dtbanco,movimentacaoacao.cdmovimentacaoacao," +
					"movimentacaoacao.nome,cheque.numero, movimentacao.dtbanco, movimentacao.dtmovimentacao, rateio.cdrateio, item.cdrateioitem, item.valor, " +
					"movimentacao2.cdmovimentacao, " +
					"projeto.cdprojeto, projeto.nome, centrocusto.cdcentrocusto, centrocusto.nome, contagerencial.cdcontagerencial, contagerencial.nome");
		
		if(Boolean.TRUE.equals(filtro.getExibirPessoa())){
			query
			.select(query.getSelect().getValue()+", pessoaDocumento.nome, pessoaDocumento.cdpessoa, documento.outrospagamento, documento.tipopagamento, " +
					"cliente.nome, cliente.razaosocial, " +
					"fornecedorBean.nome, fornecedorBean.razaosocial, " +
					"pessoaDespesaAdiantamento.cdpessoa, pessoaDespesaAdiantamento.nome, pessoaDespesaAcerto.cdpessoa, pessoaDespesaAcerto.nome, " +
					"agendamento.tipopagamento, agendamento.outrospagamento, pessoaAgendamento.cdpessoa, pessoaAgendamento.nome, "+
					"clienteAgendamento.nome, clienteAgendamento.razaosocial, fornecedorAgendamento.nome, fornecedorAgendamento.razaosocial");
			query
				.leftOuterJoin("listaMovimentacaoorigem.despesaviagemadiantamento despesaadiantamento")
				.leftOuterJoin("despesaadiantamento.colaborador pessoaDespesaAdiantamento")
				.leftOuterJoin("listaMovimentacaoorigem.despesaviagemacerto despesaacerto")
				.leftOuterJoin("despesaacerto.colaborador pessoaDespesaAcerto")
				.leftOuterJoin("pessoaDocumento.cliente cliente")
				.leftOuterJoin("pessoaDocumento.fornecedorBean fornecedorBean")
				.leftOuterJoin("listaMovimentacaoorigem.agendamento agendamento")
				.leftOuterJoin("agendamento.pessoa pessoaAgendamento")
				.leftOuterJoin("pessoaAgendamento.cliente clienteAgendamento")
				.leftOuterJoin("pessoaAgendamento.fornecedorBean fornecedorAgendamento");
		}
		query
		.leftOuterJoin("movimentacao.movimentacaoorigemTaxa movimentacaoorigemTaxa")
		.leftOuterJoin("movimentacaoorigemTaxa.movimentacao movimentacao2")
		.where("movimentacao.cdmovimentacao = ?", StringUtils.isNotBlank(filtro.getCdmovimentacao()) ? Integer.parseInt(filtro.getCdmovimentacao()): null);
		
		
		if(filtro.getOrdenacaoReport() != null){
			query.orderBy("movimentacao." + filtro.getOrdenacaoReport().getCampo() + (filtro.getAscReport() != null && filtro.getAscReport() ? " asc " : " desc "));
		}else {
			query.orderBy("movimentacao.tipooperacao asc, movimentacao.dtmovimentacao asc");
		}
		
//		QueryBuilder<Movimentacao> query = newQueryBuilderSined(Movimentacao.class)
//			.select("movimentacao.cdmovimentacao, conta.nome, movimentacao.dtmovimentacao, movimentacao.historico, movimentacao.valor," +
//					"tipooperacao.cdtipooperacao, tipooperacao.nome, movimentacao.checknum, movimentacao.dtbanco,movimentacaoacao.cdmovimentacaoacao," +
//					"movimentacaoacao.nome,cheque.numero, movimentacao.dtbanco, movimentacao.dtmovimentacao, rateio.cdrateio, listaRateioitem.cdrateioitem, listaRateioitem.valor, " +
//					"projeto.cdprojeto, projeto.nome, centrocusto.cdcentrocusto, centrocusto.nome, contagerencial.cdcontagerencial, contagerencial.nome")
//			.from(beanClass)
//			.join("movimentacao.conta conta")
//			.leftOuterJoin("conta.empresa empresa")
//			.leftOuterJoin("conta.contatipo contatipo")
//			.leftOuterJoin("conta.banco banco")
//			.join("movimentacao.movimentacaoacao movimentacaoacao")
//			.leftOuterJoin("movimentacao.tipooperacao tipooperacao")
//			.leftOuterJoin("movimentacao.formapagamento formapagamento")
//			.leftOuterJoin("movimentacao.cheque cheque")
//			
//			.leftOuterJoin("movimentacao.rateio rateio")
//			.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
//			.leftOuterJoin("listaRateioitem.projeto projeto")
//			.leftOuterJoin("listaRateioitem.contagerencial contagerencial")
//			.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
//			
//			.where("empresa = ?",filtro.getEmpresa())
//			.where("conta.contatipo = ?",filtro.getContatipo())
//			.where("conta = ?",filtro.getConta())
//			
//			.where("movimentacao.valor >= ?",filtro.getValorDe())
//			.where("movimentacao.valor <= ?",filtro.getValorAte())
//			
//			.where("formapagamento = ?", filtro.getFormapagamento())
//			
//			.where("movimentacao.dtaltera >= ?",filtro.getDtalteracaoDe())
//			.where("movimentacao.dtaltera <= ?",filtro.getDtalteracaoAte())
//	
//			.where("movimentacao.dtbanco >= ?",filtro.getDtbancoDe())
//			.where("movimentacao.dtbanco <= ?",filtro.getDtbancoAte())
//			
//			.where("cheque.dtbompara >= ?", filtro.getDtbomparade())
//			.where("cheque.dtbompara <= ?", filtro.getDtbomparaate())
//			
//			.where("coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao) >= ?",filtro.getDtmovimentacaoDe())
//			.where("coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao) <= ?",filtro.getDtmovimentacaoAte())
//			
//			.whereLikeIgnoreAll("movimentacao.historico", filtro.getHistorico())
//			.whereLikeIgnoreAll("movimentacao.checknum", filtro.getChecknum())
//			.whereLikeIgnoreAll("cheque.numero", filtro.getNumerocheque())
//			.where("tipooperacao = ?",filtro.getTipooperacao())
//			.whereIn("movimentacaoacao.cdmovimentacaoacao", 
//					filtro.getListaAcao() != null ? CollectionsUtil.listAndConcatenate(filtro.getListaAcao(), "cdmovimentacaoacao", ",") : null)
//			
//			.where("contagerencial = ?",filtro.getContagerencial())
//			.where("contagerencial.natureza = ?",filtro.getNaturezaContagerencial())		
//			.where("projeto = ?",filtro.getProjeto())
//			.where("centrocusto = ?",filtro.getCentrocusto())
//			.orderBy("movimentacao.dtmovimentacao desc");
//		
//		if(filtro.getCdvenda() != null){
//			query
//				.where("exists(" +
//						"select v.cdvenda " +
//						"from Venda v " +
//						"join v.listadocumentoorigem docorigem " +
//						"join docorigem.documento doc " +
//						"join doc.listaMovimentacaoOrigem movorigem " +
//						"join movorigem.movimentacao mov " +
//						"where mov.cdmovimentacao = movimentacao.cdmovimentacao " +
//						"and v.cdvenda = " + filtro.getCdvenda() + " " +
//						")");
//		}
		
		return query.list();
	}
	
	/**
	 * M�todo que verifica se existe movimenta��o passada por par�metro com situa��o diferente de Normal e Tipo Cr�dito 
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isMovimentacaoDiferenteCreditoNormal(String whereIn) {
		if(whereIn == null || "".equals(whereIn) )
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.from(Movimentacao.class)
				.join("movimentacao.movimentacaoacao movimentacaoacao")
				.leftOuterJoin("movimentacao.tipooperacao tipooperacao")	
				.whereIn("movimentacao.cdmovimentacao", whereIn)
				.openParentheses()
				.where("movimentacaoacao.cdmovimentacaoacao <> ? ", Movimentacaoacao.NORMAL.getCdmovimentacaoacao())
				.or()
				.where("tipooperacao.cdtipooperacao = ?", Tipooperacao.TIPO_DEBITO.getCdtipooperacao())
				.closeParentheses()
				.unique() > 0;
				
	}
	
	/**
	 * M�todo para buscar o valor total das movimenta��es
	 *
	 * @see
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getValorTotalMovimentacoes(String whereIn, Conta conta) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		Long valor =  newQueryBuilder(Long.class)
				.select("sum(movimentacao.valor)")
				.setUseTranslator(false)
				.from(Movimentacao.class)
				.join("movimentacao.conta conta")
				.whereIn("movimentacao.cdmovimentacao", whereIn)
				.where("conta = ?", conta)
				.unique();
		
		if(valor != null && valor != 0)
			return new Money(valor.doubleValue() /100);
		else
			return new Money();
	}

	/**
	 * M�todo que verifica se existe alguma movimenta��o diferente de conciliada passada pelo whereIn  
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isMovimentacaoDiferenteConciliadaOuCredito(String whereIn) {
		if(whereIn == null || "".equals((whereIn)) )
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.from(Movimentacao.class)
				.join("movimentacao.movimentacaoacao movimentacaoacao")
				.leftOuterJoin("movimentacao.tipooperacao tipooperacao")
				.whereIn("movimentacao.cdmovimentacao", whereIn)
				.openParentheses()
				.where("movimentacaoacao.cdmovimentacaoacao <> ? ", Movimentacaoacao.CONCILIADA.getCdmovimentacaoacao())
				.or()
				.where("tipooperacao.cdtipooperacao <> ?", Tipooperacao.TIPO_CREDITO.getCdtipooperacao())
				.closeParentheses()
				.unique() > 0;
	}
	
	/**
	 * M�todo para buscar as informa��es para registrar devolu��o
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Movimentacao> findForRegistrarDevolucao(String whereIn) {
		if(whereIn == null || "".equals((whereIn)) )
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("movimentacao.cdmovimentacao, movimentacao.dtbanco, movimentacao.valor, movimentacao.checknum, " +
						"tipooperacao.cdtipooperacao, conta.cdconta, contatipo.cdcontatipo, contagerencial.cdcontagerencial, " +
						"centrocusto.cdcentrocusto, projeto.cdprojeto, rateio.cdrateio, rateioitem.valor, rateioitem.percentual, " +
						"movimentacaohistorico.cdmovimentacaohistorico, movimentacaohistorico.observacao, mh_movimentacaoacao.cdmovimentacaoacao, " +
						"cheque.cdcheque ")
				.join("movimentacao.movimentacaoacao movimentacaoacao")
				.join("movimentacao.conta conta")
				.leftOuterJoin("conta.contatipo contatipo")
				.leftOuterJoin("movimentacao.tipooperacao tipooperacao")				
				.leftOuterJoin("movimentacao.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem rateioitem")
				.leftOuterJoin("rateioitem.centrocusto centrocusto")
				.leftOuterJoin("rateioitem.projeto projeto")
				.leftOuterJoin("rateioitem.contagerencial contagerencial")				
				.leftOuterJoin("movimentacao.comprovante comprovante")
				.leftOuterJoin("movimentacao.cheque cheque")
				.leftOuterJoin("movimentacao.listaMovimentacaohistorico movimentacaohistorico")
				.leftOuterJoin("movimentacaohistorico.movimentacaoacao mh_movimentacaoacao")
				.whereIn("movimentacao.cdmovimentacao", whereIn)
				.list();
	}
	
	/**
	 * M�todo que busca as movimenta��es da conta a pagar/receber
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Movimentacao> findByDocumentoVariasMovimentacoes(Documento documento) {
		return query()
			.select("movimentacao.cdmovimentacao, movimentacao.valor, movimentacao.historico, movimentacao.observacaocopiacheque, movimentacao.dtmovimentacao")
			.join("movimentacao.listaMovimentacaoorigem movimentacaoorigem")
			.join("movimentacaoorigem.documento documento")
			.join("movimentacao.movimentacaoacao movimentacaoacao")
			.where("documento = ?", documento)
			.where("movimentacaoacao <> ?",Movimentacaoacao.CANCELADA)
			.list();
	}	
	
	/**
	 * M�todo que busca a movimenta��o com a sua respectiva conta.
	 * 
	 * @param movimentacao
	 * @return
	 */
	public Movimentacao loadWithConta(Movimentacao movimentacao){
		return query()
			.joinFetch("movimentacao.conta conta")
			.joinFetch("movimentacao.movimentacaoacao movimentacaoacao")
			.joinFetch("movimentacao.tipooperacao tipooperacao")
			.where("movimentacao = ?",movimentacao)
		.unique();
	}
	
	/**
	 * M�todo que busca as movimenta��es pelos cdcheque.
	 * @param whereIn
	 * @return
	 * @author Thiers Euller
	 */
	public List<Movimentacao> findByCheque(String whereIn) {
		if(whereIn == null || "".equals((whereIn)) )
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("movimentacao.cdmovimentacao, tipooperacao.cdtipooperacao, cheque.cdcheque, cheque.valor, conta.cdconta, cheque.numero, movimentacao.valor, " +
						"documento.cddocumento, documentoclasse.cddocumentoclasse, movimentacaoacao.cdmovimentacaoacao, empresa.cdpessoa, " +
						"contagerencialdevolucaocredito.cdcontagerencial, contagerencialdevolucaodebito.cdcontagerencial, " +
						"contagerencialdevolucaocreditoCheque.cdcontagerencial, contagerencialdevolucaodebitoCheque.cdcontagerencial, " +
						"listaDocumentoOrigem.cddocumentoorigem, pedidovenda.cdpedidovenda, pedidovenda.identificacaoexterna, venda.cdvenda, venda.identificadorexterno, aux_documento.valoratual, " +
						"empresaCheque.cdpessoa, empresaCheque.nome, contadevolucaoCheque.cdconta, contadevolucaoCheque.nome")
				.join("movimentacao.cheque cheque")
				.join("movimentacao.movimentacaoacao movimentacaoacao")
				.join("movimentacao.tipooperacao tipooperacao")
				.join("movimentacao.conta conta")
				.leftOuterJoin("movimentacao.empresa empresa")
				.leftOuterJoin("empresa.contagerencialdevolucaocredito contagerencialdevolucaocredito")
				.leftOuterJoin("empresa.contagerencialdevolucaodebito contagerencialdevolucaodebito")
				.leftOuterJoin("movimentacao.listaMovimentacaoorigem movimentacaoorigem")
				.leftOuterJoin("movimentacaoorigem.documento documento")
				.leftOuterJoin("documento.aux_documento aux_documento")
				.leftOuterJoin("documento.documentoclasse documentoclasse")
				.leftOuterJoin("documento.listaDocumentoOrigem listaDocumentoOrigem")
				.leftOuterJoin("listaDocumentoOrigem.pedidovenda pedidovenda")
				.leftOuterJoin("listaDocumentoOrigem.venda venda")
				.leftOuterJoin("cheque.empresa empresaCheque")
				.leftOuterJoin("empresaCheque.contadevolucao contadevolucaoCheque")
				.leftOuterJoin("empresaCheque.contagerencialdevolucaocredito contagerencialdevolucaocreditoCheque")
				.leftOuterJoin("empresaCheque.contagerencialdevolucaodebito contagerencialdevolucaodebitoCheque")
				.where("movimentacao.movimentacaoacao <> ?",Movimentacaoacao.CANCELADA)
				.whereIn("cheque.cdcheque", whereIn)
				.orderBy("movimentacao.dtmovimentacao, movimentacao.cdmovimentacao, cheque.cdcheque")
				.list();
	}
	
	/**
	 * M�todo que busca a movimenta��o com o cheque
	 *
	 * @param movimentacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Movimentacao loadMovimentacaoWithCheque(Movimentacao movimentacao) {
		if(movimentacao == null || movimentacao.getCdmovimentacao() == null )
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("movimentacao.cdmovimentacao, cheque.cdcheque, cheque.numero, movimentacao.valor, cheque.chequesituacao")
				.leftOuterJoin("movimentacao.cheque cheque")
				.where("movimentacao = ?", movimentacao)
				.unique();
	}
	
	/**
	* M�todo que carrega a �ltima movimenta��o vinculada ao cheque
	*
	* @param cheque
	* @return
	* @since 24/03/2017
	* @author Luiz Fernando
	*/
	public Movimentacao loadUltimaMovimentacaoWithCheque(Cheque cheque) {
		if(cheque == null || cheque.getCdcheque() == null )
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("movimentacao.cdmovimentacao, cheque.cdcheque, cheque.numero, movimentacao.valor, cheque.chequesituacao")
				.join("movimentacao.cheque cheque")
				.where("movimentacao.movimentacaoacao <> ?", Movimentacaoacao.CANCELADA)
				.where("cheque = ?", cheque)
				.setMaxResults(1)
				.orderBy("movimentacao.dtmovimentacao desc, movimentacao.cdmovimentacao desc")
				.unique();
	}
	
	/**
	* M�todo que carrega a movimenta��o com o cheque
	*
	* @param whereIn
	* @return
	* @since 04/08/2014
	* @author Luiz Fernando
	*/
	public List<Movimentacao> findMovimentacaoWithCheque(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("movimentacao.cdmovimentacao, cheque.cdcheque, cheque.numero, movimentacao.valor")
				.leftOuterJoin("movimentacao.cheque cheque")
				.whereIn("movimentacao.cdmovimentacao", whereIn)
				.list();
	}
	
	/**
	 * M�todo que busca as movimenta��es de origem pelos cdcheque
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Movimentacao findMovimentacaoByCheque(String whereIn) {
		if(whereIn == null || "".equals((whereIn)) )
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("movimentacao.cdmovimentacao, movimentacao.dtmovimentacao, cheque.cdcheque, cheque.numero, movimentacao.valor, movimentacao.checknum")
				.leftOuterJoin("movimentacao.cheque cheque")
				.join("movimentacao.movimentacaoacao movimentacaoacao")
				.join("movimentacao.tipooperacao tipooperacao")
				.where("movimentacaoacao = ?", Movimentacaoacao.NORMAL)
				.whereIn("cheque.cdcheque", whereIn)
				.orderBy("movimentacao.dtmovimentacao desc")
				.setMaxResults(1)
				.unique();
	}
	
	/**
	 * M�todo que retorna um booleano que indica se existem movimenta��es com data referente a um per�odo fechado.
	 * @param whereIn
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaListaFechamento(String whereIn){
		if(whereIn == null || "".equals((whereIn)) )
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
				.setUseTranslator(Boolean.FALSE)
				.from(Movimentacao.class)
				.select("count(*)")
				.join("movimentacao.conta conta")
				.join("conta.listaContaempresa listaContaempresa")
				.join("listaContaempresa.empresa empresa")
				.whereIn("movimentacao.cdmovimentacao", whereIn)
				.where("exists ( SELECT f.id " +
								" FROM Fechamentofinanceiro f " +
								" WHERE f.empresa = empresa " +
								" AND (f.conta = conta OR f.conta is null) " +
								" AND f.datainicio <= movimentacao.dtmovimentacao " +
								" AND f.datalimite >= movimentacao.dtmovimentacao " +
								" AND f.ativo = ?)", new Object[]{Boolean.TRUE})
				.unique() > 0;
	}
	
	/**
	 * Busca as movimenta��es do cheque
	 *
	 * @param cheque
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Movimentacao> findByCheque(Cheque cheque) {
		if(cheque == null || cheque.getCdcheque() == null)
			throw new SinedException("Cheque n�o pode ser nulo.");
		
		return query()
				.select("movimentacao.cdmovimentacao, movimentacao.dtmovimentacao, conta.cdconta, cheque.cdcheque, cheque.numero")
				.join("movimentacao.cheque cheque")
				.join("movimentacao.conta conta")
				.where("cheque = ?", cheque)
				.list();
	}
	
	/**
	 * M�todo que busca a movimenta��o para valida��o do local do cheque
	 *
	 * @param cheque
	 * @return
	 * @author Luiz Fernando
	 * @since 17/09/2013
	 */
	public List<Movimentacao> findForGerarmovimentacao(Cheque cheque) {
		if(cheque == null || cheque.getCdcheque() == null)
			throw new SinedException("Cheque n�o pode ser nulo.");
		
		return query()
				.select("movimentacao.cdmovimentacao, movimentacao.dtmovimentacao, conta.cdconta, cheque.cdcheque, cheque.numero")
				.join("movimentacao.cheque cheque")
				.join("movimentacao.movimentacaoacao movimentacaoacao")
				.join("movimentacao.conta conta")
				.where("cheque = ?", cheque)
				.where("movimentacaoacao <> ?", Movimentacaoacao.CANCELADA)
				.list();
	}
	
	/**
	 * Busca as informa��es das movimenta��es de acerto ou adiamentamento relacionados a despesa de viagem vinculada.
	 *
	 * @param despesaviagem
	 * @param tipooperacao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/10/2012
	 */
	public List<Movimentacao> findByDespesaviagem(Despesaviagem despesaviagem, Tipooperacao tipooperacao) {
		if(despesaviagem == null || despesaviagem.getCddespesaviagem() == null){
			throw new SinedException("Despesa de viagem n�o pode ser nula");
		}
		return query()
				.select("movimentacao.cdmovimentacao, movimentacao.valor, rateio.cdrateio")
				.join("movimentacao.tipooperacao tipooperacao")
				.join("movimentacao.listaMovimentacaoorigem listaMovimentacaoorigem")
				.leftOuterJoin("listaMovimentacaoorigem.despesaviagemadiantamento despesaviagemadiantamento")
				.leftOuterJoin("listaMovimentacaoorigem.despesaviagemacerto despesaviagemacerto")
				.leftOuterJoin("movimentacao.rateio rateio")
				.where("tipooperacao = ?", tipooperacao)
				.openParentheses()
				.where("despesaviagemadiantamento = ?", despesaviagem)
				.or()
				.where("despesaviagemacerto = ?", despesaviagem)
				.closeParentheses()
				.list();
	}
	
	/**
	 * M�todo que verifica se existe movimenta��o relacionado a tarifa. (compara��o: data, valor, hist�rico)
	 *
	 * @param movimentacao
	 * @return
	 * @author Luiz Fernando
	 * @param valorTotalTarifa 
	 */
	public Boolean existMovimentacaoDataValorHistorico(Movimentacao movimentacao, Money valorTotalTarifa) {
		if(movimentacao == null || movimentacao.getDtmovimentacao() == null || movimentacao.getValor() == null || 
				movimentacao.getHistorico() == null || "".equals(movimentacao.getHistorico()) || 
				valorTotalTarifa == null){
			return false;
		}
		
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.from(Movimentacao.class)
				.setUseTranslator(false)
				.where("movimentacao.dtmovimentacao = ?", movimentacao.getDtmovimentacao())
				.where("movimentacao.valor = ?", valorTotalTarifa)
				.whereLikeIgnoreAll("movimentacao.historico", movimentacao.getHistorico())
				.where("movimentacao.movimentacaoacao <> ?", Movimentacaoacao.CANCELADA)
				.unique() > 0;
	}
	
	/**
	 * M�todo que busca todos os valores das movimenta��es j� vinculadas ao documento e calcula o que falta ser pago.
	 * 
	 * @param documento
	 * @return
	 */
	public Long calcularMovimentacoesByWhereInDocumento(String whereInDocumento) {
		String sql = 
			" select sum(m.valor) as VALOR " +
			" from Movimentacao m " +
			" join movimentacaoorigem mo on mo.cdmovimentacao = m.cdmovimentacao " +
			" join documento d on d.cddocumento = mo.cddocumento " +
			" join movimentacaoacao ma on ma.cdmovimentacaoacao = m.cdmovimentacaoacao " +
			" where ma.cdmovimentacaoacao <> " + Movimentacaoacao.CANCELADA.getCdmovimentacaoacao() +
			" and d.cddocumento in (" + whereInDocumento + ") ";
		
		SinedUtil.markAsReader();
		Long total = (Long)getJdbcTemplate().queryForObject(sql.toString(), 
		new RowMapper() {
			public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getLong("VALOR");
			}
		});
		
		return total != null ? total : 0;
	}
	
	/**
	* M�todo que carrega a movimenta��o ordenada por data, id e cheque, 
	* para verificar a o tipo de opera��o da primeira movimenta��o
	*
	* @param whereInCheque
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public List<Movimentacao> findForVerificarTipooperacaoByCheque(String whereInCheque) {
		if(StringUtils.isEmpty(whereInCheque))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("movimentacao.cdmovimentacao, tipooperacao.cdtipooperacao, cheque.cdcheque, conta.cdconta")
			.join("movimentacao.cheque cheque")
			.join("movimentacao.tipooperacao tipooperacao")
			.join("movimentacao.conta conta")
			.where("movimentacao.movimentacaoacao <> ?",Movimentacaoacao.CANCELADA)
			.whereIn("cheque.cdcheque", whereInCheque)
			.orderBy("movimentacao.dtmovimentacao, movimentacao.cdmovimentacao, cheque.cdcheque")
			.list();
	}
	
	/**
	* M�todo que atualiza o cheque susbtituto da movimenta��o
	*
	* @param whereIn
	* @param chequeSubstituto
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public void updateMovimentacaoChequeSubstituto(String whereIn, Cheque chequeSubstituto) {
		if(StringUtils.isNotEmpty(whereIn) && chequeSubstituto != null && chequeSubstituto.getCdcheque() != null){
			getHibernateTemplate().bulkUpdate(
					"update Movimentacao set cdcheque = ? where cdmovimentacao in (" + whereIn + ")"
					, new Object[]{chequeSubstituto.getCdcheque()}
			);
		}
	}
	
	/**
	* M�todo que carrega a movimenta��o para substitui��o de cheque
	*
	* @param whereIn
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public List<Movimentacao> findForSubistituirByCheque(String whereIn) {
		if(StringUtils.isEmpty(whereIn) )
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Movimentacao> query = query();
		updateEntradaQuery(query);
		
		return query
			.where("movimentacaoacao <> ?",Movimentacaoacao.CANCELADA)
			.whereIn("cheque.cdcheque", whereIn)
			.orderBy("movimentacao.cdmovimentacao, movimentacao.dtmovimentacao")
			.list();
	}
	
	/**
	* M�todo que altera a situa��o da movimenta��o para cancelada
	*
	* @param whereIn
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public void updateSituacaoCanceladaBySubstituicaoCheque(String whereIn) {
		if(StringUtils.isNotEmpty(whereIn)){
			getHibernateTemplate().bulkUpdate(
					"update Movimentacao set cdmovimentacaoacao = ? where cdmovimentacao in (" + whereIn + ")"
					, new Object[]{Movimentacaoacao.CANCELADA.getCdmovimentacaoacao()}
			);
		}
		
	}
	
	/**
	* M�todo que verifica se existe movimenta��o de acordo com a situa��o
	*
	* @param whereIn
	* @param notIn
	* @param movimentacaoacao
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public Boolean existMovimentacaoSituacao(String whereIn, boolean notIn, Movimentacaoacao... movimentacaoacao) {
		if(whereIn == null || "".equals((whereIn)) )
			throw new SinedException("Par�metro inv�lido.");
		
		String whereInSituacao = null;
		StringBuilder values = new StringBuilder();
		for (int i=0; i < movimentacaoacao.length; i++) {
			values.append(movimentacaoacao[i].getCdmovimentacaoacao()).append(",");
		}
		whereInSituacao = values.toString().substring(0, values.toString().length()-1);
		
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.from(Movimentacao.class)
				.join("movimentacao.movimentacaoacao movimentacaoacao")
				.whereIn("movimentacao.cdmovimentacao", whereIn)
				.where("movimentacaoacao.cdmovimentacaoacao " + (notIn ? "not" : "") + " in (" + whereInSituacao + ") ")
				.unique() > 0;
	}
	
	/**
	* M�todo que verifica se existe movimenta��o de acordo com o tipo de opera��o
	*
	* @param whereIn
	* @param tipooperacao
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public boolean isVinculoTipooperacao(String whereIn, Tipooperacao tipooperacao) {
		if(StringUtils.isEmpty(whereIn) || tipooperacao == null)
			throw new SinedException("Par�metro inv�lido.");
		
		Long count = newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Movimentacao.class)
			.whereIn("movimentacao.cdmovimentacao", whereIn)
			.where("movimentacao.tipooperacao = ?", tipooperacao)				
			.unique();
		return count != null && count > 0;
	}
	
	/**
	 * Busca as movimenta��es com a conta e o valor preenchidos a partir do whereIn 
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/09/2014
	 */
	public List<Movimentacao> findWithConta(String whereIn) {
		return query()
					.select("movimentacao.cdmovimentacao, movimentacao.valor, conta.cdconta, movimentacaoacao.cdmovimentacaoacao ")
					.join("movimentacao.movimentacaoacao movimentacaoacao")
					.join("movimentacao.conta conta")
					.whereIn("movimentacao.cdmovimentacao", whereIn)
					.list();
	}
	
	/**
	 * Busca as movimenta��es com o cheque preenchido a partir do whereIn 
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/08/2015
	 */
	public List<Movimentacao> findWithCheque(String whereIn) {
		return query()
				.select("movimentacao.cdmovimentacao, cheque.cdcheque, empresa.cdpessoa, empresaCheque.cdpessoa, " +
						"contagerencialdevolucaocredito.cdcontagerencial, " +
						"contagerencialdevolucaodebito.cdcontagerencial," +
						"contagerencialdevolucaocreditoEmpresaCheque.cdcontagerencial, " +
						"contagerencialdevolucaodebitoEmpresaCheque.cdcontagerencial ")
				.leftOuterJoin("movimentacao.cheque cheque")
				.leftOuterJoin("movimentacao.empresa empresa")
				.leftOuterJoin("empresa.contagerencialdevolucaocredito contagerencialdevolucaocredito")
				.leftOuterJoin("empresa.contagerencialdevolucaodebito contagerencialdevolucaodebito")
				.leftOuterJoin("cheque.empresa empresaCheque")
				.leftOuterJoin("empresaCheque.contagerencialdevolucaocredito contagerencialdevolucaocreditoEmpresaCheque")
				.leftOuterJoin("empresaCheque.contagerencialdevolucaodebito contagerencialdevolucaodebitoEmpresaCheque")
				.whereIn("movimentacao.cdmovimentacao", whereIn)
				.list();
	}
	
	/**
	 * Busca as movimenta��es para a tranfer�ncia em lote de movimenta��es
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/09/2014
	 */
	public List<Movimentacao> findForTransferenciaLote(String whereIn) {
		return query()
					.select("movimentacao.cdmovimentacao, movimentacaoacao.cdmovimentacaoacao, conta.cdconta, " +
							"tipooperacao.cdtipooperacao, movimentacao.valor, contatipo.cdcontatipo, " +
							"cheque.cdcheque, cheque.numero, empresa.cdpessoa, empresaCheque.cdpessoa ")
					.join("movimentacao.movimentacaoacao movimentacaoacao")
					.leftOuterJoin("movimentacao.cheque cheque")
					.leftOuterJoin("cheque.empresa empresaCheque")
					.join("movimentacao.conta conta")
					.leftOuterJoin("conta.contatipo contatipo")
					.leftOuterJoin("movimentacao.tipooperacao tipooperacao")
					.leftOuterJoin("movimentacao.empresa empresa")
					.whereIn("movimentacao.cdmovimentacao", whereIn)
					.list();
	}
	

	
	/**
	* M�todo que carrega a movimenta��o com o cheque
	*
	* @param movimentacao
	* @return
	* @since 27/11/2014
	* @author Luiz Fernando
	*/
	public Movimentacao loadWithCheque(Movimentacao movimentacao){
		if(movimentacao == null || movimentacao.getCdmovimentacao() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("movimentacao.cdmovimentacao, cheque.cdcheque")
			.join("movimentacao.cheque cheque")
			.where("movimentacao = ?",movimentacao)
			.unique();
	}
	
	/**
	* M�todo que busca as movimenta��es relacionada ao cheque para fazer update na situa��o do cheque
	*
	* @param cheque
	* @param movimentacao
	* @return
	* @since 27/11/2014
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public List<Movimentacao> buscarMovimentacaoParaExecutarUpdateSituacaoCheque(Cheque cheque) {
		if(cheque == null || cheque.getCdcheque() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		String sql = " select m.cdmovimentacao, m.cdmovimentacaoacao " + 
					 " from movimentacao m " +
					 " join cheque c on c.cdcheque = m.cdcheque " +
					 " where m.cdcheque =  " + cheque.getCdcheque() +
					 " and c.chequesituacao =  " + Chequesituacao.DEVOLVIDO.ordinal() + 
					 " order by m.dtmovimentacao desc, m.cdmovimentacao desc ";

		SinedUtil.markAsReader();
		List<Movimentacao> lista = getJdbcTemplate().query(sql.toString(), 
				new RowMapper() {
					public Movimentacao mapRow(ResultSet rs, int rowNum) throws SQLException {
						Movimentacao movimentacao = new Movimentacao();
						movimentacao.setCdmovimentacao(rs.getInt("cdmovimentacao"));
						movimentacao.setMovimentacaoacao(new Movimentacaoacao(rs.getInt("cdmovimentacaoacao")));
						return movimentacao;
					}
				});
		
		return lista;
	}
	
	/**
	 * M�todo que verifica a exist�ncia de movimenta��es iguais �quelas que ser�o criadas a partir da baixa de contas.
	 * 
	 * @param documento
	 * @return Boolean
	 * 
 	 * @author Rafael Salvio
	 */
	public Boolean verificaDuplicidade(String whereInValorMovimentacao, Date dtmovimentacao){
		if(whereInValorMovimentacao == null || whereInValorMovimentacao.trim().isEmpty() || dtmovimentacao == null){
			throw new SinedException("Par�metros inv�lidos.");
		}
		
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class);
		
		return query
			.select("count(*)")
			.from(Movimentacao.class)
			.where("movimentacao.dtmovimentacao = ?", dtmovimentacao)
			.whereIn("movimentacao.valor", whereInValorMovimentacao)
			.unique() > 0;
	}
	
	/**
	* M�todo que retorna o valor total de movimenta��es de acordo com o filtro
	*
	* @param filtro
	* @return
	* @since 26/05/2015
	* @author Luiz Fernando
	*/
	public Money getValorTotalMovimentacoes(CustooperacionalFiltro filtro) {
		if(filtro == null || filtro.getEmpresa() == null || filtro.getDtinicio() == null || filtro.getDtfim() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		String whereInContagerencial = SinedUtil.isListNotEmpty(filtro.getListaContagerencial()) ? 
				SinedUtil.listAndConcatenate(filtro.getListaContagerencial(), "cdcontagerencial", ",") : null;
		
		Long valor = newQueryBuilder(Long.class)
				.select("sum(listaRateioitem.valor)")
				.setUseTranslator(false)
				.from(Movimentacao.class)
				.join("movimentacao.conta conta")
				.leftOuterJoin("conta.listaContaempresa listaContaempresa")
				.leftOuterJoin("listaContaempresa.empresa empresa")
				.join("movimentacao.rateio rateio")
				.join("rateio.listaRateioitem listaRateioitem")
				.where("coalesce(movimentacao.dtbanco,movimentacao.dtmovimentacao) >= ?", filtro.getDtinicio())
				.where("coalesce(movimentacao.dtbanco,movimentacao.dtmovimentacao) <= ?", filtro.getDtfim())
				.where("movimentacao.tipooperacao = ?", Tipooperacao.TIPO_DEBITO)
				.where("movimentacao.movimentacaoacao = ?", Movimentacaoacao.CONCILIADA)
				.whereIn("listaRateioitem.contagerencial.cdcontagerencial", whereInContagerencial)
				.openParentheses()
					.where("empresa = ?", filtro.getEmpresa())
					.or()
					.where("empresa is null")
				.closeParentheses()
				.unique();
		
		if(valor != null && valor != 0)
			return new Money(valor.doubleValue() /100);
		else
			return new Money();
	}
	
	/**
	* M�todo que retorna as movimenta��es que deram origem as outras movimenta��es
	*
	* @param movimentacao
	* @return
	* @since 02/10/2015
	* @author Luiz Fernando
	*/
	public List<Movimentacao> findOrigemDevolucao(Movimentacao movimentacao, Boolean desconsiderarOrigemDocumento) {
		if(movimentacao == null || movimentacao.getCdmovimentacao() == null)
			return null;
		
		return query()
				.select("movimentacao.cdmovimentacao, movimentacao.dtmovimentacao, movimentacao.dtbanco," +
						"movimentacaoacao.cdmovimentacaoacao ")
				.join("movimentacao.movimentacaoacao movimentacaoacao")
				.where("movimentacao.cdmovimentacao in (  select mdevolucao.cdmovimentacao " +
														" from Movimentacaoorigem mo " +
														" join mo.movimentacao m " +
														" join mo.movimentacaodevolucao mdevolucao " +
														" where mdevolucao is not null " +
														" and m.cdmovimentacao = " + movimentacao.getCdmovimentacao() + " ) ")
				.where("not exists (" +
						" select m.cdmovimentacao " +
						" from Movimentacaoorigem mo " +
						" join mo.movimentacao m " +
						" join mo.documento d " +
						" join d.documentoacao dacao " +
						" where m.cdmovimentacao = movimentacao.cdmovimentacao " +
						" and d.cddocumento is not null " +
						" and dacao.cddocumentoacao in ( " + Documentoacao.BAIXADA.getCddocumentoacao() + ", " + Documentoacao.BAIXADA_PARCIAL.getCddocumentoacao() + ") " +
						")", Boolean.TRUE.equals(desconsiderarOrigemDocumento))
				.list();
	}
	
	/**
	* M�todo que retorna as movimenta��es criadas a partir da movimenta��o passada por par�metro
	*
	* @param movimentacao
	* @return
	* @since 02/10/2015
	* @author Luiz Fernando
	*/
	public List<Movimentacao> findMovimentacaoVinculoDevolucao(Movimentacao movimentacao) {
		if(movimentacao == null || movimentacao.getCdmovimentacao() == null)
			return null;
		
		return query()
				.select("movimentacao.cdmovimentacao, movimentacao.dtmovimentacao ")
				.where("movimentacao.movimentacaoacao <> ?", Movimentacaoacao.CANCELADA)
				.where("movimentacao.cdmovimentacao in (  select m.cdmovimentacao " +
														" from Movimentacaoorigem mo " +
														" join mo.movimentacao m " +
														" join mo.movimentacaodevolucao mdevolucao " +
														" where mdevolucao.cdmovimentacao = " + movimentacao.getCdmovimentacao() + " ) ")
				.list();
	}
	
	/**
	* M�todo que busca as movimenta��es referente ao adiantamento de despesa de viagem
	*
	* @param despesaviagem
	* @return
	* @since 16/02/2016
	* @author Luiz Fernando
	*/
	public List<Movimentacao> findAdiantamentoByDespesaviagem(String whereIn) {
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("movimentacao.cdmovimentacao, despesaviagemadiantamento.cddespesaviagem ")
				.join("movimentacao.listaMovimentacaoorigem listaMovimentacaoorigem")
				.join("listaMovimentacaoorigem.despesaviagemadiantamento despesaviagemadiantamento")
				.where("movimentacao.movimentacaoacao <> ?", Movimentacaoacao.CANCELADA)
				.where("exists ( select mo.cdmovimentacaoorigem " +
								"from Movimentacaoorigem mo " +
								"join mo.movimentacao m " +
								"join mo.despesaviagemadiantamento dva " +
								"where dva.cddespesaviagem in (" + whereIn + ") and m.cdmovimentacao = movimentacao.cdmovimentacao) ")
				.list();
	}
	
	/**
	* M�todo que faz update no valor da movimenta��o
	*
	* @param movimentacao
	* @param valor
	* @since 31/10/2016
	* @author Luiz Fernando
	*/
	public void updateValorMovimentacao(Movimentacao movimentacao, Money valor) {
		if (movimentacao == null || movimentacao.getCdmovimentacao() == null || valor == null) {
			throw new SinedException("Movimenta��o n�o pode ser nula.");
		}
		
		getHibernateTemplate().bulkUpdate("update Movimentacao m set m.valor = ? where m.id = ?", 
			new Object[]{valor,movimentacao.getCdmovimentacao()});
	}
	
	public List<Movimentacao> findForGerarLancamentoContabilTransferencia(GerarLancamentoContabilFiltro filtro, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento){
		Contatipo contatipoOrigem = null;
		Contatipo contatipoDestino = null;
		Boolean cheque = null;
		
		if (MovimentacaocontabilTipoLancamentoEnum.DEPOSITO.equals(movimentacaocontabilTipoLancamento)){
			contatipoOrigem = Contatipo.TIPO_CAIXA; 
			contatipoDestino = Contatipo.TIPO_CONTA_BANCARIA; 
			cheque = Boolean.FALSE;
		}else if (MovimentacaocontabilTipoLancamentoEnum.DEPOSITO_CHEQUE.equals(movimentacaocontabilTipoLancamento)){
			contatipoOrigem = Contatipo.TIPO_CAIXA; 
			contatipoDestino = Contatipo.TIPO_CONTA_BANCARIA; 
			cheque = Boolean.TRUE;
		}else if (MovimentacaocontabilTipoLancamentoEnum.SAQUE.equals(movimentacaocontabilTipoLancamento)){
			contatipoOrigem = Contatipo.TIPO_CONTA_BANCARIA; 
			contatipoDestino = Contatipo.TIPO_CAIXA;
			cheque = Boolean.FALSE;
		}else if (MovimentacaocontabilTipoLancamentoEnum.SAQUE_CHEQUE.equals(movimentacaocontabilTipoLancamento)){
			contatipoOrigem = Contatipo.TIPO_CONTA_BANCARIA; 
			contatipoDestino = Contatipo.TIPO_CAIXA;
			cheque = Boolean.TRUE;
		}else if (MovimentacaocontabilTipoLancamentoEnum.TRANSFERENCIA_ENTRE_CONTAS.equals(movimentacaocontabilTipoLancamento)){
			contatipoOrigem = Contatipo.TIPO_CONTA_BANCARIA; 
			contatipoDestino = Contatipo.TIPO_CONTA_BANCARIA; 
		}else if (MovimentacaocontabilTipoLancamentoEnum.TRANSFERENCIA_ENTRE_CAIXAS.equals(movimentacaocontabilTipoLancamento)){
			contatipoOrigem = Contatipo.TIPO_CAIXA; 
			contatipoDestino = Contatipo.TIPO_CAIXA; 
		}
		
		QueryBuilder<Movimentacao> queryBuilder = query();
		queryBuilder.select("movimentacao.cdmovimentacao, movimentacao.valor, movimentacao.dtmovimentacao, movimentacao.dtbanco, movimentacao.checknum, movimentacao.historico," +
							"contaContabilMovimentacao.cdcontacontabil, contaContabilMovimentacao.nome," +
							"contaContabilOrigem.cdcontacontabil, contaContabilOrigem.nome," +
							"listaRateioitem.percentual, listaRateioitem.valor, centrocusto.cdcentrocusto, centrocusto.nome," +
							"cheque.cdcheque, cheque.numero, cheque.dtbompara, cheque.banco, cheque.agencia, cheque.conta, cheque.emitente, cheque.valor," +
							"movimentacaodebito.cdmovimentacao, movimentacaodebito.valor, movimentacaodebito.dtmovimentacao, movimentacaodebito.dtbanco, movimentacaodebito.checknum," +
							"contaContabilMovimentacaoDebito.cdcontacontabil, contaContabilMovimentacaoDebito.nome," +
							"contaContabilOrigemdebito.cdcontacontabil, contaContabilOrigemdebito.nome," +
							"listaRateioitemdebito.percentual, listaRateioitemdebito.valor, centrocustodebito.cdcentrocusto, centrocustodebito.nome," +
							"chequedebito.cdcheque, chequedebito.numero, chequedebito.dtbompara, chequedebito.banco, chequedebito.agencia, chequedebito.conta," +
							"chequedebito.emitente, chequedebito.valor, projeto.cdprojeto, projeto.nome, projetodebito.cdprojeto, projetodebito.nome");
		
		//Conversado e definido pelo Christian no dia 11/09/2017
		//A opera��o cont�bil de Dep�sitos deve partir da movimenta��o financeira de cr�dito.
		
		//Cr�dito
		queryBuilder
			.join("movimentacao.tipooperacao tipooperacao")
			.join("movimentacao.movimentacaoacao movimentacaoacao")
			.join("movimentacao.rateio rateio")
			.join("rateio.listaRateioitem listaRateioitem")
			.join("listaRateioitem.centrocusto centrocusto")
			.leftOuterJoin("listaRateioitem.projeto projeto")
			.join("movimentacao.conta contamovimentacao")
			.join("contamovimentacao.contatipo contatipo")
			.leftOuterJoin("movimentacao.empresa empresa")
			.leftOuterJoin("contamovimentacao.listaContaempresa listaContaempresa")
			.leftOuterJoin("contamovimentacao.contaContabil contaContabilMovimentacao")
			.leftOuterJoin("movimentacao.cheque cheque")
			.join("movimentacao.listaMovimentacaoorigem listaMovimentacaoorigem")
			.leftOuterJoin("listaMovimentacaoorigem.conta contaorigem")
			.leftOuterJoin("contaorigem.contaContabil contaContabilOrigem")
			.join("movimentacao.listaMovimentacaohistorico listaMovimentacaohistorico")
			.leftOuterJoin("listaMovimentacaoorigem.movimentacaorelacionadacartaocredito movimentacaorelacionadacartaocreditoCredito");
		
		//D�bito
		queryBuilder
			.join("listaMovimentacaoorigem.movimentacaorelacionada movimentacaodebito")
			.join("movimentacaodebito.tipooperacao tipooperacaodebito")
			.join("movimentacaodebito.movimentacaoacao movimentacaoacaodebito")
			.join("movimentacaodebito.rateio rateiodebito")
			.join("rateiodebito.listaRateioitem listaRateioitemdebito")
			.join("listaRateioitemdebito.centrocusto centrocustodebito")
			.leftOuterJoin("listaRateioitemdebito.projeto projetodebito")
			.join("movimentacaodebito.conta contamovimentacaodebito")
			.join("contamovimentacaodebito.contatipo contatipodebito")
			.leftOuterJoin("contamovimentacaodebito.contaContabil contaContabilMovimentacaoDebito")
			.leftOuterJoin("movimentacaodebito.cheque chequedebito")
			.join("movimentacaodebito.listaMovimentacaoorigem listaMovimentacaoorigemdebito")
			.leftOuterJoin("listaMovimentacaoorigemdebito.conta contaorigemdebito")
			.leftOuterJoin("contaorigemdebito.contaContabil contaContabilOrigemdebito")
			.join("movimentacaodebito.listaMovimentacaohistorico listaMovimentacaohistoricodebito");
			
		queryBuilder
			.openParentheses()
				.where("empresa = ?", filtro.getEmpresa())
				.or()
				.openParentheses()
					.where("empresa is null")
					.openParentheses()
						.where("listaContaempresa is null")
						.or()
						.where("listaContaempresa.empresa = ?", filtro.getEmpresa())
					.closeParentheses()
				.closeParentheses()
			.closeParentheses()
			.where("coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)>=?", filtro.getDtPeriodoInicio())
			.where("coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)<=?", filtro.getDtPeriodoFim())
			.openParentheses()
				.where("projeto=?", filtro.getProjeto())
				.or()
				.where("projetodebito=?", filtro.getProjeto())
			.closeParentheses()
			.where("tipooperacao=?", Tipooperacao.TIPO_CREDITO)
			.where("tipooperacaodebito=?", Tipooperacao.TIPO_DEBITO)
			.where("movimentacaoacao in (?, ?)", new Object[]{Movimentacaoacao.NORMAL, Movimentacaoacao.CONCILIADA})
			.where("movimentacaoacaodebito in (?, ?)", new Object[]{Movimentacaoacao.NORMAL, Movimentacaoacao.CONCILIADA})
			.where("listaMovimentacaohistorico.movimentacaoacao=?", Movimentacaoacao.TRANSFERENCIA)
			.where("listaMovimentacaohistoricodebito.movimentacaoacao=?", Movimentacaoacao.TRANSFERENCIA)
			.where("contatipodebito=?", contatipoOrigem) //A transfer�ncia � feita de d�bito para cr�dito
			.where("contatipo=?", contatipoDestino) //A transfer�ncia � feita de d�bito para cr�dito
			.where("cheque is not null", Boolean.TRUE.equals(cheque)) //Vari�vel "cheque" pode ser null
			.where("cheque is null", Boolean.FALSE.equals(cheque)) //Vari�vel "cheque" pode ser null
			.where("movimentacaorelacionadacartaocreditoCredito is not null", MovimentacaocontabilTipoLancamentoEnum.CONCILIACAO_CARTAO.equals(movimentacaocontabilTipoLancamento))
			.where("movimentacaorelacionadacartaocreditoCredito is null", !MovimentacaocontabilTipoLancamentoEnum.CONCILIACAO_CARTAO.equals(movimentacaocontabilTipoLancamento))
			.where("not exists (select 1 from Movimentacaocontabilorigem mco where mco.movimentacao=movimentacao and mco.movimentacaocontabil.tipoLancamento=?)", new Object[]{movimentacaocontabilTipoLancamento});
		
		return queryBuilder.list();
	}
	
	public List<Movimentacao> findForGerarLancamentoContabilRetornoBancario(GerarLancamentoContabilFiltro filtro, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento){
		return query()
				.select("movimentacao.cdmovimentacao, movimentacao.valor, movimentacao.dtmovimentacao, movimentacao.dtbanco, movimentacao.checknum, movimentacao.historico," +
						"contaContabilMovimentacao.cdcontacontabil, contaContabilMovimentacao.nome," +
						"listaRateioitem.percentual, listaRateioitem.valor, centrocusto.cdcentrocusto, centrocusto.nome, projeto.cdprojeto, projeto.nome")
				.join("movimentacao.tipooperacao tipooperacao")
				.join("movimentacao.movimentacaoacao movimentacaoacao")
				.join("movimentacao.rateio rateio")
				.join("rateio.listaRateioitem listaRateioitem")
				.join("listaRateioitem.centrocusto centrocusto")
				.leftOuterJoin("listaRateioitem.projeto projeto")
				.join("movimentacao.conta contamovimentacao")
				.leftOuterJoin("movimentacao.empresa empresa")
				.leftOuterJoin("contamovimentacao.listaContaempresa listaContaempresa")
				.leftOuterJoin("contamovimentacao.contaContabil contaContabilMovimentacao")
				.join("movimentacao.listaMovimentacaoorigem listaMovimentacaoorigem")
				.join("listaMovimentacaoorigem.documento documento")
				.join("documento.listaArqBancarioDoc listaArqBancarioDoc")
				.join("listaArqBancarioDoc.arquivobancario arquivobancario")
				.openParentheses()
					.where("empresa = ?", filtro.getEmpresa())
					.or()
					.openParentheses()
						.where("empresa is null")
						.openParentheses()
							.where("listaContaempresa is null")
							.or()
							.where("listaContaempresa.empresa = ?", filtro.getEmpresa())
						.closeParentheses()
					.closeParentheses()
				.closeParentheses()
				.where("coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)>=?", filtro.getDtPeriodoInicio())
				.where("coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)<=?", filtro.getDtPeriodoFim())
				.where("listaRateioitem.projeto=?", filtro.getProjeto())
				.where("movimentacaoacao in (?, ?)", new Object[]{Movimentacaoacao.NORMAL, Movimentacaoacao.CONCILIADA})
				.where("arquivobancario.arquivobancariotipo=?", Arquivobancariotipo.RETORNO)
				.where("not exists (select 1 from Movimentacaocontabilorigem mco where mco.movimentacao=movimentacao and mco.movimentacaocontabil.tipoLancamento=?)", new Object[]{movimentacaocontabilTipoLancamento})
				.list();
	}
	
	public List<Movimentacao> findForGerarLancamentoContabilContaGerencial(GerarLancamentoContabilFiltro filtro, Operacaocontabil operacaoContabil, String whereNotIn, String whereNotInRateioitem, boolean considerarRateio){
		QueryBuilder<Movimentacao> query = query();
				query
				.select("movimentacao.cdmovimentacao, movimentacao.valor, movimentacao.dtmovimentacao, movimentacao.dtbanco, movimentacao.checknum, movimentacao.historico," +
						"contaContabilMovimentacao.cdcontacontabil, contaContabilMovimentacao.nome, listaRateioitem.cdrateioitem," +
						"listaRateioitem.percentual, listaRateioitem.valor, contagerencial.cdcontagerencial, contagerencial.nome, centrocusto.cdcentrocusto," +
						"centrocusto.nome, projeto.cdprojeto, projeto.nome, empresa.nomefantasia, empresa.nome, " +
						"documentoOrigem.cddocumento, documentoOrigem.valor, " +
						"contaContabilCliente.cdcontacontabil, contaContabilCliente.nome, " +
						"contaContabilFornecedor.cdcontacontabil, contaContabilFornecedor.nome, " +
						"contaContabilColaborador.cdcontacontabil, contaContabilColaborador.nome, " +
						"cliente.cdpessoa, fornecedor.cdpessoa, colaborador.cdpessoa, " +
						"aux_documento.cddocumento, aux_documento.valoratual, " +
						"docOrigemRateioitem.cdrateioitem, docOrigemRateioitem.percentual, docOrigemRateioitem.valor, " +
						"docOrigemRateioItemProjeto.cdprojeto, docOrigemRateioItemProjeto.nome, " +
						"docOrigemRateioItemCG.cdcontagerencial, docOrigemRateioItemCG.nome, " +
						"docOrigemRateioItemCentroCusto.cdcentrocusto, docOrigemRateioItemCentroCusto.nome")
				.join("movimentacao.tipooperacao tipooperacao")
				.join("movimentacao.movimentacaoacao movimentacaoacao")
				.join("movimentacao.rateio rateio")
				.leftOuterJoin("movimentacao.empresa empresa")
				.leftOuterJoin("movimentacao.listaMovimentacaoorigem listaMovimentacaoorigem")
				.leftOuterJoin("listaMovimentacaoorigem.documento documentoOrigem")
				.leftOuterJoin("documentoOrigem.aux_documento aux_documento")
				.leftOuterJoin("documentoOrigem.pessoa pessoa")
				.leftOuterJoin("documentoOrigem.rateio docOrigemRateio")
				.leftOuterJoin("docOrigemRateio.listaRateioitem docOrigemRateioitem")
				.leftOuterJoin("docOrigemRateioitem.projeto docOrigemRateioItemProjeto")
				.leftOuterJoin("docOrigemRateioitem.contagerencial docOrigemRateioItemCG")
				.leftOuterJoin("docOrigemRateioitem.centrocusto docOrigemRateioItemCentroCusto")	
				.leftOuterJoin("pessoa.cliente cliente")
				.leftOuterJoin("pessoa.fornecedorBean fornecedor")
				.leftOuterJoin("pessoa.colaborador colaborador")
				.leftOuterJoin("colaborador.contaContabil contaContabilColaborador")
				.leftOuterJoin("cliente.contaContabil contaContabilCliente")
				.leftOuterJoin("fornecedor.contaContabil contaContabilFornecedor")
				.leftOuterJoin("movimentacao.conta contamovimentacao")
				.join("rateio.listaRateioitem listaRateioitem")
				.join("listaRateioitem.contagerencial contagerencial")
				.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
				.leftOuterJoin("listaRateioitem.projeto projeto")
				.leftOuterJoin("contamovimentacao.listaContaempresa listaContaempresa")
				.leftOuterJoin("contamovimentacao.contaContabil contaContabilMovimentacao")
				.openParentheses()
					.where("empresa = ?", filtro.getEmpresa())
					.or()
					.openParentheses()
						.where("empresa is null")
						.openParentheses()
							.where("listaContaempresa is null")
							.or()
							.where("listaContaempresa.empresa = ?", filtro.getEmpresa())
						.closeParentheses()
					.closeParentheses()
				.closeParentheses()
				.where("coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)>=?", filtro.getDtPeriodoInicio())
				.where("coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)<=?", filtro.getDtPeriodoFim())
				.where("listaRateioitem.projeto=?", filtro.getProjeto())
				.where("movimentacaoacao in (?, ?)", new Object[]{Movimentacaoacao.NORMAL, Movimentacaoacao.CONCILIADA})
				.where("contagerencial=?", operacaoContabil != null ? operacaoContabil.getContagerencial() : null)
				.where("movimentacao.cdmovimentacao not in (" + whereNotIn +  ")", !whereNotIn.trim().equals(""))
				.where("listaRateioitem.cdrateioitem not in (" + whereNotInRateioitem +  ")", !whereNotInRateioitem.trim().equals(""));
				
				if(considerarRateio){
					query.where("listaRateioitem.cdrateioitem not in (select ri.cdrateioitem from Movimentacaocontabilorigem mco join mco.rateioitem ri where mco.movimentacaocontabil.tipoLancamento=?)", new Object[]{MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_MOVIMENTACAO});
				} else {
					query.where("not exists (select 1 from Movimentacaocontabilorigem mco where mco.movimentacao=movimentacao and mco.movimentacaocontabil.tipoLancamento=?)", new Object[]{MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_MOVIMENTACAO});
				}
				
				if(operacaoContabil.getMovimentacaoFinanceiraVinculo() != null && operacaoContabil.getMovimentacaoFinanceiraVinculo().equals(MovimentacaoFinanceiraVinculoEnum.COM_ORIGEM_DOCUMENTO)) {
					query.where("listaMovimentacaoorigem.documento is not null");
				} else if(operacaoContabil.getMovimentacaoFinanceiraVinculo() != null && operacaoContabil.getMovimentacaoFinanceiraVinculo().equals(MovimentacaoFinanceiraVinculoEnum.SEM_ORIGEM_DOCUMENTO)) {
					query.where("listaMovimentacaoorigem.documento is null");
				}
								
		return query.list();		
	}
	
	public List<Movimentacao> findForContaListagem(String whereInCddocumento){
		return querySined()
				
				.select("movimentacao.cdmovimentacao, movimentacao.dtmovimentacao, movimentacao.dtbanco, documento.cddocumento")
				.join("movimentacao.listaMovimentacaoorigem listaMovimentacaoorigem")
				.join("listaMovimentacaoorigem.documento documento")
				.where("movimentacao.movimentacaoacao in (?, ?)", new Object[]{Movimentacaoacao.NORMAL, Movimentacaoacao.CONCILIADA})
				.where("documento.documentoacao in (?, ?)", new Object[]{Documentoacao.BAIXADA_PARCIAL, Documentoacao.BAIXADA})
				.whereIn("documento.cddocumento", whereInCddocumento)
				.list();
	}
	
	public List<Movimentacao> findForGerarArquivoRemessaConfiguravelCheque(GerarArquivoRemessaConfiguravelChequeFiltro filtro){
		return querySined()
				
				.select("movimentacao.cdmovimentacao, movimentacao.dtmovimentacao, movimentacao.valor, movimentacao.bordero," +
						"movimentacao.conta, movimentacao.cheque")
				.join("movimentacao.listaMovimentacaohistorico listaMovimentacaohistorico")
				.where("movimentacao.movimentacaoacao in (?, ?)", new Object[]{Movimentacaoacao.NORMAL, Movimentacaoacao.CONCILIADA})
				.where("listaMovimentacaohistorico.movimentacaoacao=?", Movimentacaoacao.TRANSFERENCIA)
				.where("movimentacao.tipooperacao=?", Tipooperacao.TIPO_CREDITO)
				.where("movimentacao.conta=?", filtro.getConta())
				.where("movimentacao.dtmovimentacao>=?", filtro.getDtmovimentacaoInicio())
				.where("movimentacao.dtmovimentacao<=?", filtro.getDtmovimentacaoFim())
				.orderBy("movimentacao.dtmovimentacao")
				.list();
	}
	
	/**
	* M�todo que verifica se existe movimenta��o em aberto para o cheque
	*
	* @param whereIn
	* @return
	* @since 20/03/2017
	* @author Luiz Fernando
	*/
	public boolean existeMovimentacaoEmAbertoByCheque(String whereIn) {
		if(whereIn == null || "".equals((whereIn)) )
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.from(Movimentacao.class)
				.setUseTranslator(false)
				.join("movimentacao.cheque cheque")
				.join("movimentacao.movimentacaoacao movimentacaoacao")
				.where("movimentacao.movimentacaoacao <> ?",Movimentacaoacao.CANCELADA)
				.whereIn("cheque.cdcheque", whereIn)
				.unique() > 0;
	}
	
	public List<Movimentacao> findMovimenacaoTaxa(Movimentacao movimentacao) {
		if(movimentacao == null || movimentacao.getCdmovimentacao() == null)
			return null;
		
		return query()
				.select("movimentacao.cdmovimentacao, movimentacao.dtmovimentacao, movimentacao.dtbanco, movimentacao.valor, " +
						"movimentacaoacao.cdmovimentacaoacao, movimentacao.taxageradaprocessamentocartao ")
				.join("movimentacao.movimentacaoacao movimentacaoacao")
				.where("movimentacao.cdmovimentacao in (  select mtaxa.cdmovimentacao " +
														" from Movimentacaoorigem mo " +
														" join mo.movimentacao m " +
														" join mo.movimentacaorelacionada mtaxa " +
														" where coalesce(mtaxa.taxa, false) = true " +
														" and m.cdmovimentacao = " + movimentacao.getCdmovimentacao() + " ) ")
				.list();
	}
	
	public boolean isTransferencia(Movimentacao movimentacao){
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.join("movimentacao.listaMovimentacaohistorico listaMovimentacaohistorico")
				.join("movimentacao.listaMovimentacaoorigem listaMovimentacaoorigem")
				.join("listaMovimentacaoorigem.movimentacaorelacionada movimentacaorelacionada")
				.join("movimentacaorelacionada.listaMovimentacaohistorico listaMovimentacaohistoricorelacionada")
				.where("movimentacao=?", movimentacao)
				.where("listaMovimentacaohistorico.movimentacaoacao=?", Movimentacaoacao.TRANSFERENCIA)
				.where("listaMovimentacaohistoricorelacionada.movimentacaoacao=?", Movimentacaoacao.TRANSFERENCIA)
				.unique()>0;
	}
	
	@SuppressWarnings("unchecked")
	public List<Movimentacao> findMovimentacaoRelacionadaTransferencia(Movimentacao movimentacao, boolean todas, boolean cheque){
		/*Movimentacao movimentacaoRelacionada = query()
												.select("movimentacao.cdmovimentacao, movimentacaorelacionada.cdmovimentacao," +
														"movimentacaorelacionada.dtmovimentacao, movimentacaorelacionada.valor," +
														"movimentacaorelacionada.historico")
												.join("movimentacao.listaMovimentacaohistorico listaMovimentacaohistorico")
												.join("movimentacao.listaMovimentacaoorigem listaMovimentacaoorigem")
												.join("listaMovimentacaoorigem.movimentacaorelacionada movimentacaorelacionada")
												.join("movimentacaorelacionada.listaMovimentacaohistorico listaMovimentacaohistoricorelacionada")
												.where("movimentacao=?", movimentacao)
												.where("listaMovimentacaohistorico.movimentacaoacao=?", Movimentacaoacao.TRANSFERENCIA)
												.where("listaMovimentacaohistoricorelacionada.movimentacaoacao=?", Movimentacaoacao.TRANSFERENCIA)
												.unique();
		
		return movimentacaoRelacionada.getListaMovimentacaoorigem().get(0).getMovimentacaorelacionada();*/
		
		String sql = "select cdmovimentacaorelacionada, cdcheque " + 
					 "from ( " +
					 "select mo.cdmovimentacaorelacionada, m.cdcheque " +
					 "from movimentacaoorigem mo " +
					 "join movimentacao m on m.cdmovimentacao=mo.cdmovimentacaorelacionada " +
					 "where mo.cdmovimentacao in (" + movimentacao.getCdmovimentacao() + ") " +
					 "union " +
					 "select mo.cdmovimentacaorelacionada, m.cdcheque " +
					 "from movimentacaoorigem mo " +
					 "join movimentacao m on m.cdmovimentacao=mo.cdmovimentacaorelacionada " +
					 "where mo.cdmovimentacao in " + 
					 "(select cdmovimentacaorelacionada " +
					 "from movimentacaoorigem mo " +
					 "where mo.cdmovimentacao in (" + movimentacao.getCdmovimentacao() + "))) as t " +
					 "where 1=1 ";
		
		if (!todas){
			sql += "and cdmovimentacaorelacionada not in (" + movimentacao.getCdmovimentacao() + ") ";
		}
		
		if (cheque){
			sql += "and cdcheque is not null ";
		}

		SinedUtil.markAsReader();
		List<Integer> lista = getJdbcTemplate().query(sql, 
														new RowMapper() {
															public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
																return rs.getInt("cdmovimentacaorelacionada");
															}
														});
		
		return carregaLista(CollectionsUtil.concatenate(lista, ", "));
	}
	
	public void updateDtmovimentacao(Movimentacao movimentacao, Date dtmovimentacao) {
		if (movimentacao == null || movimentacao.getCdmovimentacao() == null || dtmovimentacao == null) {
			throw new SinedException("Movimenta��o n�o pode ser nula.");
		}
		
		getHibernateTemplate().bulkUpdate("update Movimentacao m set m.dtmovimentacao = ? where m.id = ?", 
			new Object[]{dtmovimentacao, movimentacao.getCdmovimentacao()});
	}
	
	public void updateHistorico(Movimentacao movimentacao, String historico) {
		if (movimentacao == null || movimentacao.getCdmovimentacao() == null || historico==null) {
			throw new SinedException("Movimenta��o n�o pode ser nula.");
		}
		
		getHibernateTemplate().bulkUpdate("update Movimentacao m set m.historico = ? where m.id = ?", 
				new Object[]{historico, movimentacao.getCdmovimentacao()});
	}
	
	public List<Movimentacao> findByMovimentacaorelacionada(Movimentacao movimentacao) {
		if(movimentacao == null || movimentacao.getCdmovimentacao() == null)
			return null;
		
		return query()
				.select("movimentacao.cdmovimentacao, movimentacao.dtmovimentacao, movimentacao.dtbanco, movimentacao.valor, conta.cdconta, conta.nome ")
				.join("movimentacao.listaMovimentacaoorigem movimentacaoorigem")
				.join("movimentacao.conta conta")
				.where("movimentacaoorigem.movimentacaorelacionada = ?", movimentacao)
				.list();
	}
	
	public boolean existeMovimentacaotaxaGeradaNoProcessamentoExtratocartao(Movimentacao movimentacao){
		return !query()
				.select("movimentacao.cdmovimentacao")
				.join("movimentacao.listaMovimentacaoorigem movimentacaoorigem")
				.leftOuterJoin("movimentacao.conta conta")
				.join("movimentacaoorigem.movimentacaorelacionada movimentacaorelacionada")
				.where("movimentacao = ?", movimentacao)
				.where("movimentacaorelacionada.taxa = ?", Boolean.TRUE)
				.where("movimentacaorelacionada.taxageradaprocessamentocartao = ?", Boolean.TRUE)
				.where("movimentacaorelacionada.tipooperacao = ?", Tipooperacao.TIPO_DEBITO)
				.where("movimentacaorelacionada.movimentacaoacao <> ?", Movimentacaoacao.CANCELADA)
				.list()
				.isEmpty();
	}
	
	public Movimentacao loadForCriaMovimentacaoTaxaProcessamentoextratocartao(Movimentacao movimentacao){
		return query()
			.select("movimentacao.cdmovimentacao, movimentacao.dtmovimentacao, movimentacao.dtbanco, "+
					"movimentacao.checknum, movimentacao.valor, "+
					"empresa.cdpessoa, empresa.nome, "+
					"conta.cdconta, conta.nome, conta.agencia, conta.dvagencia, conta.numero, conta.dvnumero, "+
					"banco.cdbanco, banco.numero")
			.leftOuterJoin("movimentacao.conta conta")
			.leftOuterJoin("movimentacao.empresa empresa")
			.leftOuterJoin("conta.banco banco")
			.where("movimentacao = ?", movimentacao)
			.unique();
	}
	public List<Movimentacao> findByAtrasado(Date data, Date dateToSearch) {
		Date dtmovimentacao = SinedDateUtils.incrementDate(data, -3, Calendar.DAY_OF_MONTH);
		Integer acao = Movimentacaoacao.NORMAL.getCdmovimentacaoacao();
		return querySined()
				.setUseWhereClienteEmpresa(false)
				.setUseWhereEmpresa(false)
				.setUseWhereFornecedorEmpresa(false)
				.setUseWhereProjeto(false)
				.select("movimentacao.cdmovimentacao, empresa.cdpessoa")
				.leftOuterJoin("movimentacao.empresa empresa")
				.join("movimentacao.movimentacaoacao acao")
				.where("acao.cdmovimentacaoacao = ?", acao )
				.where("movimentacao.dtmovimentacao <= ?", dtmovimentacao)
				.where("movimentacao.dtmovimentacao >= ?", dateToSearch)
				.list();
	}
	
	public List<Movimentacao> findNotNormal(String whereIn){
		return query()
			.where("movimentacao.movimentacaoacao <> ?", Movimentacaoacao.NORMAL)
			.whereIn("movimentacao.cdmovimentacao", whereIn)
			.list();
	}
	
	public List<Movimentacao> verificaTipoDocumentoAntecipacaoContaVinculadaBaixada(String ids) {
		return query()
				.select("distinct documento2.cddocumento")
				.leftOuterJoin("movimentacao.listaMovimentacaoorigem listaMovimentacaoorigem")
				.leftOuterJoin("listaMovimentacaoorigem.documento documento")
				.leftOuterJoin("documento.documentotipo documentotipo")
				.leftOuterJoin("documento.listaHistoricoAntecipacao listaHistoricoAntecipacao")
				.leftOuterJoin("listaHistoricoAntecipacao.documento documento2")
				.whereIn("movimentacao.cdmovimentacao", ids)
				.where("documento <> null")
				.where("documentotipo.antecipacao = true")
				.where("documento2.documentoacao = ?", Documentoacao.BAIXADA)
				.list();
	}
	public List<Movimentacao> buscarPorCentroCusto(boolean csv, Centrocusto centroCusto,Date dtInicio, Date dtFim, Empresa empresa) {
		return query()
				.select("movimentacao.cdmovimentacao,rateio.cdrateio")
				.leftOuterJoin("movimentacao.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.leftOuterJoin("movimentacao.conta conta")
				.leftOuterJoin("conta.listaContaempresa listaContaempresa")
				.where("listaRateioitem.projeto is null", !csv)
				.openParentheses()
					.where("movimentacao.empresa = ?",empresa)
						.or()
					.openParentheses()
						.where("movimentacao.empresa is null")
						.openParentheses()
							.where("listaContaempresa.empresa = ?", empresa)
							.or()
							.where("listaContaempresa is null")
						.closeParentheses()
					.closeParentheses()
				.closeParentheses()
				.where("movimentacao.dtmovimentacao >= ?",dtInicio)
				.where("movimentacao.dtmovimentacao <= ?",dtFim)
				.where("listaRateioitem.centrocusto = ?", centroCusto)
				.where("movimentacao.movimentacaoacao <> ?", Movimentacaoacao.CANCELADA)
				.list();
	}
	
	public List<Movimentacao> loadForLcdpr(LcdprArquivoFiltro filtro) {
		return query()
				.select("movimentacao.cdmovimentacao, movimentacao.historico, movimentacao.dtmovimentacao, movimentacao.checknum, movimentacao.valor, " +
						"tipooperacao.cdtipooperacao, documento.cddocumento, documento.numero, documentotipo.tipoDocumentoLCDPR, documentotipo.antecipacao, " +
						"conta.cdconta, empresa.cdpessoa, aux_documento.valoratual, formapagamento.cdformapagamento, pessoa.cpf, pessoa.cnpj, " +
						"colaboradorviagemadiantamento.cpf, colaboradorviagemadiantamento.cnpj, colaboradorviagemacerto.cpf, colaboradorviagemacerto.cnpj," +
						"pessoaagendamento.cpf, pessoaagendamento.cnpj")
				.leftOuterJoin("movimentacao.tipooperacao tipooperacao")
				.leftOuterJoin("movimentacao.conta conta")
				.leftOuterJoin("movimentacao.empresa empresa")
				.leftOuterJoin("movimentacao.formapagamento formapagamento")
				.leftOuterJoin("movimentacao.listaMovimentacaoorigem listaMovimentacaoorigem")
				.leftOuterJoin("listaMovimentacaoorigem.documento documento")
				.leftOuterJoin("documento.pessoa pessoa")
				.leftOuterJoin("documento.documentotipo documentotipo")
				.leftOuterJoin("documento.aux_documento aux_documento")
				.leftOuterJoin("listaMovimentacaoorigem.agendamento agendamento")
				.leftOuterJoin("agendamento.pessoa pessoaagendamento")
				.leftOuterJoin("listaMovimentacaoorigem.despesaviagemadiantamento despesaviagemadiantamento")
				.leftOuterJoin("despesaviagemadiantamento.colaborador colaboradorviagemadiantamento")
				.leftOuterJoin("listaMovimentacaoorigem.despesaviagemacerto despesaviagemacerto")
				.leftOuterJoin("despesaviagemacerto.colaborador colaboradorviagemacerto")
				.where("empresa.cdpessoa in (select e.cdpessoa FROM Empresaproprietario ep " +
												"JOIN ep.empresa e " +
												"WHERE ep.colaborador.cdpessoa = " +filtro.getColaborador().getCdpessoa() + ")")
				.where("movimentacao.dtmovimentacao >= ?", filtro.getDtinicio())
				.where("movimentacao.dtmovimentacao <= ?", filtro.getDtfim())
				.where("movimentacao.movimentacaoacao <> ?", Movimentacaoacao.CANCELADA)
				.orderBy("movimentacao.dtmovimentacao")	
				.list();
	}

	/**
	 * M�todo que busca a quantidade de movimenta��es vinculadas ao documento.
	 * 
	 * @param documento
	 * @return
	 */
	public Long countQtdeMovimentacao(Documento documento) {
		String sql = 
			" select count(1) as qtde " +
			" from Movimentacao m " +
			" join movimentacaoorigem mo on mo.cdmovimentacao = m.cdmovimentacao " +
			" join documento d on d.cddocumento = mo.cddocumento " +
			" join movimentacaoacao ma on ma.cdmovimentacaoacao = m.cdmovimentacaoacao " +
			" where ma.cdmovimentacaoacao <> " + Movimentacaoacao.CANCELADA.getCdmovimentacaoacao() +
			" and d.cddocumento = " + documento.getCddocumento() + " ";
		
		SinedUtil.markAsReader();
		Long total = (Long)getJdbcTemplate().queryForObject(sql.toString(), 
		new RowMapper() {
			public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getLong("qtde");
			}
		});
		
		return total != null ? total : 0;
	}

	/**
	 * M�todo que retorna a quantidade de documentos(diferentes do passado como par�metro) vinculados � movimenta��o passada no whereIn.
	 * 
	 * @param documento
	 * @return
	 */
	public Long countOutrosDocumentosVinculados(String whereInMovimentacao, Documento documento) {
		String sql = 
			" select count(1) as qtde " +
			" from Movimentacao m " +
			" join movimentacaoorigem mo on mo.cdmovimentacao = m.cdmovimentacao " +
			" join documento d on d.cddocumento = mo.cddocumento " +
			" join movimentacaoacao ma on ma.cdmovimentacaoacao = m.cdmovimentacaoacao " +
			" where ma.cdmovimentacaoacao <> " + Movimentacaoacao.CANCELADA.getCdmovimentacaoacao() +
			" and d.cddocumento <> " + documento.getCddocumento() +
			" and m.cdmovimentacao in("+whereInMovimentacao+")";
		
		SinedUtil.markAsReader();
		Long total = (Long)getJdbcTemplate().queryForObject(sql.toString(), 
		new RowMapper() {
			public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getLong("qtde");
			}
		});
		
		return total != null ? total : 0;
	}
	
	public Money buscarGastoEmpresa(RelatorioMargemContribuicaoFiltro filtro) {
		Money gastosEmpresa = new Money();
		
		Long valor = new QueryBuilder<Long>(getHibernateTemplate())
				.select("sum(listaRateioitem.valor)")
				.setUseTranslator(false)
				.from(Movimentacao.class)
				.leftOuterJoin("movimentacao.empresa empresa")
				.leftOuterJoin("movimentacao.rateio rateio")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.leftOuterJoin("listaRateioitem.contagerencial contagerencial")
				.where("movimentacao.movimentacaoacao <> ?", Movimentacaoacao.CANCELADA)
				.openParentheses()
					.where("empresa = ?", filtro.getEmpresa())
					.or()
					.where("empresa is null")
				.closeParentheses()
				.wherePeriodo("coalesce(movimentacao.dtbanco, movimentacao.dtmovimentacao)", filtro.getDtInicio(), filtro.getDtFim())
				.openParentheses()
					.where("contagerencial.usadocalculocustooperacionalproducao = true")
					.or()
					.where("contagerencial.usadocalculocustocomercialproducao = true")
					.closeParentheses()
				.unique();
		
		if (valor != null && valor != 0) {
			gastosEmpresa = new Money(valor.doubleValue() / 100);
		}
		
		return gastosEmpresa;
	}
}