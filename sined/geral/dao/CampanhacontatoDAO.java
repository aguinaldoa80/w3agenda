package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Campanha;
import br.com.linkcom.sined.geral.bean.Campanhacontato;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CampanhacontatoDAO extends GenericDAO<Campanhacontato>{
	
	public List<Campanhacontato> findByCampanha(Campanha campanha){
		
		if(campanha == null || campanha.getCdcampanha() == null){
			throw new SinedException("O par�metro contacrm e cdcontacrm n�o podem ser null.");
		}
		return 
			query()
				.select("campanhacontato.cdcampanhacontato, campanha.cdcampanha, " +
						"contacrmcontato.cdcontacrmcontato, contacrmcontato.nome, " +
						"contacrm.cdcontacrm, contacrm.nome, " +
						"responsavel.cdpessoa, responsavel.nome,  " +
						"listcontacrmcontatoemail.cdcontacrmcontatoemail, listcontacrmcontatoemail.email ")
				.leftOuterJoin("campanhacontato.campanha campanha")
				.leftOuterJoin("campanhacontato.contacrmcontato contacrmcontato")
				.leftOuterJoin("contacrmcontato.listcontacrmcontatoemail listcontacrmcontatoemail")
				.leftOuterJoin("contacrmcontato.contacrm contacrm")
				.leftOuterJoin("contacrm.responsavel responsavel")
				.where("campanha = ?",campanha)
				.orderBy("campanha.cdcampanha")
				.list();
		
	}

	
	public List<Campanhacontato> loadByContacrm(Contacrm contacrm){
		
		if(contacrm == null || contacrm.getCdcontacrm() == null){
			throw new SinedException("O par�metro contacrm e cdcontacrm n�o podem ser null.");
		}
		return 
			query()
				.select("campanhacontato.cdcampanhacontato, campanha.cdcampanha, " +
						"contacrmcontato.cdcontacrmcontato, contacrmcontato.nome, " +
						"contacrm.cdcontacrm, contacrm.responsavel, contacrm.nome, " +
						"listcontacrmcontatoemail.cdcontacrmcontatoemail, listcontacrmcontatoemail.email ")
				.leftOuterJoin("campanhacontato.campanha campanha")
				.leftOuterJoin("campanhacontato.contacrm contacrm")
				.leftOuterJoin("campanhacontato.contacrmcontato contacrmcontato")
				.leftOuterJoin("campanhacontato.listcontacrmcontatoemail listcontacrmcontatoemail")
				.where("contacrm = ?",contacrm)
				.orderBy("contacrm.cdcontacrm")
				.list();
		
	}


}
