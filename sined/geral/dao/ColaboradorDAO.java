package br.com.linkcom.sined.geral.dao;

import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Area;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.ColaboradorArquivo;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Colaboradordependente;
import br.com.linkcom.sined.geral.bean.Colaboradorsituacao;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Sindicato;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.enumeration.ColaboradorFiltroMostrar;
import br.com.linkcom.sined.geral.bean.enumeration.EmitirautorizacaoReportFiltroMostrar;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ColaboradorFiltro;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.Colaboradorholerite;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.AlterarsituacaocolaboradorFilter;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.ExportarcolaboradorFiltro;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.HoleriteFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.EmitirautorizacaoReportFiltro;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.bean.ColaboradoragendaservicoReportBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.filter.ColaboradoragendaservicoReportFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

@DefaultOrderBy("colaborador.nome")
public class ColaboradorDAO extends GenericDAO<Colaborador> {
	
	private ArquivoDAO arquivoDAO;
	private EnderecoService enderecoService;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	
	
	@Override
	public void updateListagemQuery(QueryBuilder<Colaborador> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		ColaboradorFiltro filtro = (ColaboradorFiltro) _filtro;
		query
			.select("distinct colaborador.cdpessoa, colaborador.nome, colaborador.email, colaborador.cpf, colaborador.dtnascimento")
			.leftOuterJoin("colaborador.listaColaboradorcargo colaboradorcargo")
			.leftOuterJoin("colaboradorcargo.sindicato sindicato")
			.leftOuterJoin("colaboradorcargo.departamento departamento")
			.leftOuterJoin("colaboradorcargo.empresa empresa")
			.leftOuterJoin("colaboradorcargo.cargo cargo")
			.leftOuterJoin("colaboradorcargo.projeto projeto")
			.whereLikeIgnoreAll("colaborador.nome", filtro.getNome())
			.whereLikeIgnoreAll("colaborador.cpf", filtro.getCpf())
			.whereLikeIgnoreAll("colaborador.rg", filtro.getRg())
			.where("colaborador.dtadmissao >= ?",filtro.getDtAdmissaoDe())
			.where("colaborador.dtadmissao <= ?",filtro.getDtAdmissaoAte())
			.where("exists (select cc2.cdcolaboradorcargo from Colaboradorcargo cc2 where cc2.regimecontratacao = ? AND cc2.colaborador = colaborador AND cc2.dtinicio = (select max(cc.dtinicio) from Colaboradorcargo cc where cc.colaborador = colaborador))", filtro.getRegimecontratacao())
			.where("date_part('month', colaborador.dtnascimento) >= ?",filtro.getNascimentoDe() != null ? filtro.getNascimentoDe().ordinal()+1 : null)
			.where("date_part('month', colaborador.dtnascimento) <= ?",filtro.getNascimentoAte() != null ? filtro.getNascimentoAte().ordinal()+1 : null)
			.orderBy("colaborador.nome")
			.ignoreJoin("colaboradorcargo","sindicato","departamento","cargo","empresa","projeto")
			;
		
		
		if(filtro.getSindicato() != null || 
				filtro.getDepartamento() != null || 
				filtro.getCargo() != null || 
				filtro.getMatricula() != null || 
				filtro.getEmpresa() != null ||
				filtro.getProjeto() != null){
			
			String joinSindicato = "left outer join cc.sindicato s ";
			String joinDepartamento = "left outer join cc.departamento d ";
			String joinCargo = "left outer join cc.cargo c ";
			String joinProjeto = "left outer join cc.projeto p ";
			String joinEmpresa = "left outer join cc.empresa e  ";
			
			String whereSindicato = " and s.cdsindicato = ";
			String whereDepartamento = " and d.cddepartamento = ";
			String whereCargo = " and c.cdcargo = ";
			String whereProjeto = " and p.cdprojeto = ";
			String whereEmpresa = " and e.cdpessoa = ";
			String whereMatricula = " and cc.matricula = ";
			
			query.where(" exists (select cc.colaborador.cdpessoa from Colaboradorcargo cc " +
					(filtro.getSindicato() != null ? joinSindicato : "") +
					(filtro.getDepartamento() != null ? joinDepartamento : "") +
					(filtro.getCargo() != null ? joinCargo : "") +
					(filtro.getProjeto() != null ? joinProjeto : "") +
					(filtro.getEmpresa() != null ? joinEmpresa : "") +
					
					" where cc.colaborador.cdpessoa = colaborador.cdpessoa " + 
					
					(filtro.getSindicato() != null ? whereSindicato + filtro.getSindicato().getCdsindicato(): "") +
					(filtro.getDepartamento() != null ? whereDepartamento + filtro.getDepartamento().getCddepartamento() : "") +
					(filtro.getCargo() != null ? whereCargo + filtro.getCargo().getCdcargo() : "") +
					(filtro.getProjeto() != null ? whereProjeto + filtro.getProjeto().getCdprojeto() : "") +
					(filtro.getEmpresa() != null ? whereEmpresa + filtro.getEmpresa().getCdpessoa() : "") +
					(filtro.getMatricula() != null ? whereMatricula + filtro.getMatricula() : "") +
					
					" and cc.cdcolaboradorcargo = (ultimocargo_colaborador(colaborador.cdpessoa)) ) "
			);
		}
		
		if(filtro.getMostrar() != null){
			if(filtro.getMostrar().equals(ColaboradorFiltroMostrar.EPI_VENCIDOS)){
				query
					.leftOuterJoin("colaborador.listaColaboradorequipamento listaColaboradorequipamento")
					.where("listaColaboradorequipamento.dtentrega <= ?", SinedDateUtils.currentDate())
					.ignoreJoin("listaColaboradorequipamento");
			}
			else if(filtro.getMostrar().equals(ColaboradorFiltroMostrar.EXAMES_A_REALIZAR)){
				query
					.leftOuterJoin("colaborador.listaColaboradorexame listaColaboradorexame")
					.where("listaColaboradorexame.dtproximoexame >= ?", SinedDateUtils.currentDate())
					.where("listaColaboradorexame.dtproximoexame <= ?", SinedDateUtils.addDiasData(SinedDateUtils.currentDate(), 7))
					.ignoreJoin("listaColaboradorexame");
			}
			else if(filtro.getMostrar().equals(ColaboradorFiltroMostrar.EXAMES_VENCIDOS)){
				query
					.leftOuterJoin("colaborador.listaColaboradorexame listaColaboradorexame")
					.where("listaColaboradorexame.cdcolaboradorexame in ( select ce.cdcolaboradorexame " +
																		" from Colaboradorexame ce " +
																		" where ce.colaborador = colaborador " +
																		" and ce.dtproximoexame < ? " +
																		" and not exists ( select ce2.cdcolaboradorexame " +
																						" from Colaboradorexame ce2 " +
																						" where ce2.colaborador = ce.colaborador " +
																						" and ce2.exametipo = ce.exametipo " +
																						" and ce2.cdcolaboradorexame <> ce.cdcolaboradorexame " +
																						" and ce2.dtproximoexame >= ? ) " +
																		" ) ", new Object[]{SinedDateUtils.currentDate(),SinedDateUtils.currentDate()})
					.ignoreJoin("listaColaboradorexame");
			}
		}
		
		if(SinedUtil.isListNotEmpty(filtro.getListaColaboradorsituacao())){
			query.whereIn("colaborador.colaboradorsituacao",CollectionsUtil.listAndConcatenate(filtro.getListaColaboradorsituacao(),"cdcolaboradorsituacao",","));
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Colaborador> query) {
		query
			.leftOuterJoinFetch("colaborador.estadocivil estadocivil")
			.leftOuterJoinFetch("colaborador.grauinstrucao grauinstrucao")
			.leftOuterJoinFetch("colaborador.colaboradorformapagamento colaboradorformapagamento")
			.leftOuterJoinFetch("colaborador.etnia etnia")
			.leftOuterJoinFetch("colaborador.tipocarteiratrabalho tipocarteiratrabalho")
			.leftOuterJoinFetch("colaborador.sexo sexo")
			.leftOuterJoinFetch("colaborador.colaboradortipo colaboradortipo")
			
			.leftOuterJoinFetch("colaborador.ufctps ufctps")
			.leftOuterJoinFetch("colaborador.tipoinscricao tipoinscricao")
			.leftOuterJoinFetch("colaborador.endereco endereco")
			.leftOuterJoinFetch("endereco.pessoa pessoa")
			.leftOuterJoinFetch("endereco.municipio municipio")
			.leftOuterJoinFetch("municipio.uf uf")
			
//			.leftOuterJoinFetch("colaborador.listaColaboradorequipamento listaColaboradorequipamento")
//			.leftOuterJoinFetch("listaColaboradorequipamento.epi epi")

			.leftOuterJoinFetch("colaborador.listaColaboradorarea listaColaboradorarea")
			.leftOuterJoinFetch("listaColaboradorarea.area area")
//           .leftOuterJoinFetch("colaborador.listaColaboradorocorrencia listaColaboradorocorrencia")
			.leftOuterJoinFetch("colaborador.listaTelefone listaTelefone")

//			.leftOuterJoinFetch("colaborador.listaEscalacolaborador listaEscalacolaborador")
//			.leftOuterJoinFetch("listaEscalacolaborador.escala escala")
			
//			.leftOuterJoinFetch("colaborador.listaContratocolaborador listaContratocolaborador")
//			.leftOuterJoinFetch("listaContratocolaborador.contrato contrato")
//			.leftOuterJoinFetch("contrato.cliente cliente")
//			.leftOuterJoinFetch("contrato.empresa empresa")
			
			.leftOuterJoinFetch("colaborador.listaColaboradordependente listaColaboradordependente")
			.leftOuterJoinFetch("listaColaboradordependente.tipodependente tipodependente")
			
			.leftOuterJoinFetch("colaborador.listaColaboradorcargo listaColaboradorcargo")
//			.leftOuterJoinFetch("listaColaboradorcargo.empresa empresa")
			.leftOuterJoinFetch("listaColaboradorcargo.departamento departamento")
//			.leftOuterJoinFetch("listaColaboradorcargo.empresa empresa")
			.leftOuterJoinFetch("listaColaboradorcargo.cargo cargo")
			.leftOuterJoinFetch("listaColaboradorcargo.regimecontratacao regimecontratacao")
			.leftOuterJoinFetch("listaColaboradorcargo.sindicato sindicato")
			
			.leftOuterJoinFetch("colaborador.listaColaboradorcategoriacnh listaColaboradorcategoriacnh")
			
			.leftOuterJoinFetch("colaborador.foto foto")
			.leftOuterJoinFetch("colaborador.colaboradordeficiente colaboradordeficiente")
			.leftOuterJoinFetch("colaborador.colaboradorsituacao colaboradorsituacao")
			.leftOuterJoinFetch("colaborador.dirf dirf")
			.leftOuterJoinFetch("colaborador.ufcnh ufcnh")
			.leftOuterJoinFetch("colaborador.tipodocumentomilitar")
			
			.leftOuterJoinFetch("colaborador.municipionaturalidade municipionaturalidade")
			.leftOuterJoinFetch("municipionaturalidade.uf ufnaturalidade")
			
			.leftOuterJoinFetch("colaborador.listaDadobancario dadobancario")
			
			.leftOuterJoinFetch("dadobancario.tipoconta tipoconta")
			.leftOuterJoinFetch("dadobancario.banco banco")

//			.leftOuterJoinFetch("colaborador.escala escala")

			.leftOuterJoinFetch("colaborador.listaColaboradoracidente listaColaboradoracidente")
			.leftOuterJoinFetch("colaborador.listaColaboradorexame listaColaboradorexame")

			.leftOuterJoinFetch("listaColaboradorexame.exameresponsavel exameresponsavel")
			.leftOuterJoinFetch("listaColaboradorexame.exametipo exametipo")
			.leftOuterJoinFetch("listaColaboradorexame.examenatureza examenatureza")
//			.leftOuterJoinFetch("colaborador.valetransporte valetransporte")
			.leftOuterJoinFetch("colaborador.contaContabil")
			
			.leftOuterJoinFetch("colaborador.listaColaboradorArquivo listaColaboradorArquivo")
			.leftOuterJoinFetch("listaColaboradorArquivo.arquivo arquivo")
			;
			
			
	}
		
	/**
	 *  Verifica se a pessoa � colaborador e usu�rio do sistema
	 * @param pessoa
	 * @throws SinedException - Se o par�metro informado for null
	 * @return null se n�o encontrar
	 * @author Jo�o Paulo Zica
	 */
	public Colaborador findColaboradorusuario(Integer cdpessoa){
		if (cdpessoa == null) {
			throw new SinedException("Par�metro cdpessoa n�o pode ser null.");
		}
		return
			querySined()
				.setUseWhereEmpresa(false)
				.setUseWhereProjeto(false)
				.setUseWhereFornecedorEmpresa(false)
				.setUseWhereClienteEmpresa(false)
				.where("colaborador.cdpessoa = ?", cdpessoa)
				.unique();
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {		
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				Colaborador colaborador = (Colaborador) save.getEntity();
				colaborador.setTipopessoa(Tipopessoa.PESSOA_FISICA);
				colaborador.getEndereco().setEnderecotipo(Enderecotipo.UNICO);
				
				if (colaborador.getEndereco() != null){
					Endereco endereco = colaborador.getEndereco();
					enderecoService.saveOrUpdateNoUseTransaction(endereco);
				}
				
				if(colaborador.getFoto() != null)
					arquivoDAO.saveFile(colaborador, "foto");
				
				save.saveOrUpdateManaged("listaTelefone");
				save.saveOrUpdateManaged("listaDadobancario");
				save.saveOrUpdateManaged("listaColaboradorcategoriacnh");
				if(colaborador.getCdpessoa() == null || Boolean.TRUE.equals(colaborador.getCriarColaboradorCargo())){
					save.saveOrUpdateManaged("listaColaboradorcargo");
				}
				save.saveOrUpdateManaged("listaColaboradordependente");
				save.saveOrUpdateManaged("listaColaboradorarea");
//				save.saveOrUpdateManaged("listaContratocolaborador");
				save.saveOrUpdateManaged("listaColaboradorequipamento");
				save.saveOrUpdateManaged("listaColaboradoracidente");
				save.saveOrUpdateManaged("listaColaboradorexame");
				save.saveOrUpdateManaged("listaEscalacolaborador");
				save.saveOrUpdateManaged("listaColaboradorocorrencia");
				save.saveOrUpdateManaged("listaColaboradorbeneficio");
				
				for(ColaboradorArquivo colaboradorArquivo : colaborador.getListaColaboradorArquivo()){
					if(colaboradorArquivo.getArquivo() != null)
						arquivoDAO.saveFile(colaboradorArquivo, "arquivo");
				}
				save.saveOrUpdateManaged("listaColaboradorArquivo");
				
				return null;
			}			
		});		
	}	
	
	public void updateColaboradorsituacaoByColaborador(Colaborador colaborador, Colaboradorsituacao colaboradorsituacao){
		if(colaborador != null && colaborador.getCdpessoa() != null && colaboradorsituacao != null && colaboradorsituacao.getCdcolaboradorsituacao() != null){
			getJdbcTemplate().update("update colaborador set cdcolaboradorsituacao = ? where cdpessoa = ?", 
					new Object[]{colaboradorsituacao.getCdcolaboradorsituacao(), colaborador.getCdpessoa()});
		}
	}
	
	/**
	 * M�todo para salvar o colaborador j� cadastrado no sistema como usu�rio.
	 * @param colaborador
	 * @author Jo�o Paulo Zica
	 * @author Flavio Tavares
	 */
	public void salvaColaborador(Colaborador colaborador){
		if (colaborador ==  null){
			throw new SinedException("O par�metro colaborador n�o pode ser null");
		}
		Integer cdpessoa = colaborador.getCdpessoaaux();
		getJdbcTemplate().update("insert into Colaborador "+ 
				  		  "(CDPESSOA, CDSEXO, CDESTADOCIVIL, CDGRAUINSTRUCAO, MAE,CDCOLABORADORFORMAPAGAMENTO,DTADMISSAO, " +
				  		  "CDETNIA, CDTIPOCARTEIRATRABALHO, CDCOLABORADORTIPO, RECONTRATAR) "+
						  "values (?,?,?,?,?,?,?,?,?,?,?)",
				new Object[]{cdpessoa, colaborador.getSexo().getCdsexo(), colaborador.getEstadocivil().getCdestadocivil(),
				colaborador.getGrauinstrucao().getCdgrauinstrucao(), colaborador.getMae(),colaborador.getColaboradorformapagamento().getCdcolaboradorformapagamento(), 
				colaborador.getDtadmissao(), colaborador.getEtnia().getCdetnia(), colaborador.getTipocarteiratrabalho().getCdtipocarteiratrabalho(),
				colaborador.getColaboradortipo().getCdcolaboradortipo(),colaborador.getRecontratar()}
		);
		
		if(colaborador.getDtnascimento() != null){
			getJdbcTemplate().update("UPDATE Pessoa SET DTNASCIMENTO = ?" +
								" WHERE CDPESSOA = ?", new Object[]{colaborador.getDtnascimento(), cdpessoa}
								);
		}
		updateCollections(colaborador);
	}
	
	/**
	 * M�todo para atualizar as refer�ncias de colaborador nas listas do bean, no caso de o colaborador ser um usuario.
	 * 
	 * @param bean
	 * @author Flavio
	 */
	private void updateCollections(Colaborador bean){
		Integer cdpessoa = bean.getCdpessoaaux(); 
		bean.setCdpessoa(cdpessoa);
		
		Set<Colaboradorcargo> historicoCargo = bean.getListaColaboradorcargo(); 
		if(historicoCargo!=null){
			for (Colaboradorcargo cargo : historicoCargo) {
				cargo.setColaborador(new Colaborador(cdpessoa));
			}
		}
		
		Set<Colaboradordependente> dependentes = bean.getListaColaboradordependente();
		if(dependentes != null){
			for (Colaboradordependente dependente : dependentes) {
				dependente.setColaborador(new Colaborador(cdpessoa));
			}
		}
		
	}
	
	/**
	 * M�todo para atualizar alterados do colaborador
	 * @param colaborador
	 * @author Jo�o Paulo Zica
	 * @author Fl�vio Tavares
	 */
	public void updateColaborador(Colaborador colaborador){
		if (colaborador ==  null){
			throw new SinedException("O par�metro colaborador n�o pode ser null");
		}
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		Integer user = SinedUtil.getUsuarioLogado().getCdpessoa(); 
		
		String sql = "update Colaborador set cdcolaboradorsituacao = ?, dtaltera = ?, cdusuarioaltera = ? where cdpessoa = ?";
		Object[] fields = new Object[]{colaborador.getColaboradorsituacao().getCdcolaboradorsituacao(),				
				hoje, user, colaborador.getCdpessoa()};
		
		getJdbcTemplate().update(sql,fields);
		
		if(colaborador.getDtinicio() != null){
			sql = "update colaborador set dtadmissao = ? where cdpessoa = ?";
			fields = new Object[]{colaborador.getDtinicio(), colaborador.getCdpessoa()};
			getJdbcTemplate().update(sql, fields);
		}
		
		//apaga o vencimento se alterar a situacao do colaborador
		if(colaborador.getApagaVencimento() != null && colaborador.getApagaVencimento()) {
			sql = "update Colaboradorcargo " +
					"set cdcolaboradorsituacao = ?, " +
					"cdusuarioaltera = ?, " +
					"dtaltera = ?, " +
					"vencimentosituacao = ?" +
					"where cdpessoa = ? and dtfim is null";
			fields = new Object[]{colaborador.getColaboradorsituacao().getCdcolaboradorsituacao(),
					user, hoje, null, colaborador.getCdpessoa()};
		} else {
			if(colaborador.getVencimentosituacao() != null){
				sql = "update Colaboradorcargo " +
						"set cdcolaboradorsituacao = ?, " +
						"cdusuarioaltera = ?, " +
						"dtaltera = ?, " +
						"vencimentosituacao = ?" +
						"where cdpessoa = ? and dtfim is null";
				fields = new Object[]{colaborador.getColaboradorsituacao().getCdcolaboradorsituacao(),
						user, hoje, colaborador.getVencimentosituacao(), colaborador.getCdpessoa()};
			} else {
				sql = "update Colaboradorcargo " +
						"set cdcolaboradorsituacao = ?, " +
						"cdusuarioaltera = ?, " +
						"dtaltera = ? " +
						"where cdpessoa = ? and dtfim is null";
				fields = new Object[]{colaborador.getColaboradorsituacao().getCdcolaboradorsituacao(),
						user, hoje,	colaborador.getCdpessoa()};
			}
		}
		
		getJdbcTemplate().update(sql, fields);
		
		if(colaborador.getRegimecontratacao() != null){
			sql = "update Colaboradorcargo " +
					"set cdregimecontratacao = ?, " +
					"cdusuarioaltera = ?, " +
					"dtaltera = ? " +
					"where cdpessoa = ? and dtfim is null";
			fields = new Object[]{colaborador.getRegimecontratacao().getCdregimecontratacao(),
					user, hoje,	colaborador.getCdpessoa()};
			
			getJdbcTemplate().update(sql, fields);
		}
	}
	
	/**
	 * M�todo para deletar o colaborador cadastrado no sistema como usu�rio.
	 * @param colaborador
	 * @author Jo�o Paulo Zica
	 */
	public void deleteColaborador(Colaborador colaborador){
		if (colaborador ==  null){
			throw new SinedException("O par�metro colaborador n�o pode ser null");
		}
		getJdbcTemplate().update("delete from Colaborador where cdpessoa = ?", new Object[]{colaborador.getCdpessoa()});
	}

	/**
	 * M�todo para retornar os dados de preenchimento do callback do ajax para altera��o dos dados
	 * 
	 * @param cdpessoa
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public Colaborador findColaboradorByPk(Integer cdpessoa){
		if (cdpessoa == null) {
			throw new SinedException("Par�metro cdpessoa n�o pode ser null.");
		}
		return
			query()
				.select("colaborador.cdpessoa, colaboradorcargo.dtinicio, colaboradorcargo.dtfim, colaboradorcargo.regimecontratacao, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cdpessoa, colaboradorcargo.observacao, colaboradorsituacao.cdcolaboradorsituacao, colaboradorsituacao.nome, colaborador.nome, colaborador.cpf, departamento.nome,departamento.cddepartamento, cargo.nome, cargo.cdcargo")
				.leftOuterJoin("colaborador.listaColaboradorcargo colaboradorcargo")
				.leftOuterJoin("colaboradorcargo.departamento departamento")
				.leftOuterJoin("colaboradorcargo.empresa empresa")
				.leftOuterJoin("colaboradorcargo.cargo cargo")
				.leftOuterJoin("colaboradorcargo.regimecontratacao regimecontratacao")
				.leftOuterJoin("colaborador.colaboradorsituacao colaboradorsituacao")
				.where("colaborador.cdpessoa = ?", cdpessoa)
				.unique();
	}
	
	
	
	/**
	 * Gera uma lista de colaboradores para a emiss�o de relat�rio
	 * 
	 * @throws SinedException - Caso o par�metro filtro seja null.
	 * @param filtro
	 * @return Lista de Colaborador
	 * @author Jo�o Paulo Zica
	 * @author Fl�vio Tavares
	 */
	public List<Colaborador> findForAutorizacaoExame(EmitirautorizacaoReportFiltro filtro){
		if(filtro == null)
			throw new SinedException("O par�metro filtro n�o deve ser null.");
		QueryBuilder<Colaborador> query = querySined();
		query
			.select("colaborador.cdpessoa, cbo.nome, colaborador.cpf, colaborador.nome, colaborador.dtnascimento, colaborador.dtadmissao, sexo.nome," +
					" listaColaboradorexame.cdcolaboradorexame, listaColaboradorexame.dtproximoexame, exametipo.nome, examenatureza.nome, " +
					"listaColaboradorexame.dtexame, exameresponsavel.nome, listaColaboradorexame.examers, " +
					"listaColaboradorexame.resultadonormal, listaColaboradorexame.resultadoalterado, " +
					"listaColaboradorexame.resultadoestavel, listaColaboradorexame.resultadoagravamento, " +
					"listaColaboradorexame.resultadoocupacional, listaColaboradorexame.resultadonaoocupacional ")
			.leftOuterJoin("colaborador.listaColaboradorcargo colaboradorcargo")
			.leftOuterJoin("colaboradorcargo.empresa empresa")
			.leftOuterJoin("colaboradorcargo.cargo cargo")
			.leftOuterJoin("colaboradorcargo.departamento departamento")
			.leftOuterJoin("colaborador.sexo sexo")
			.leftOuterJoin("cargo.cbo cbo")
			.join("colaborador.listaColaboradorexame listaColaboradorexame")
			.leftOuterJoin("listaColaboradorexame.exametipo exametipo")
			.leftOuterJoin("listaColaboradorexame.examenatureza examenatureza")
			.leftOuterJoin("listaColaboradorexame.exameresponsavel exameresponsavel")			
	//		.leftOuterJoin("colaboradorcargo.projeto projeto")
			.whereLikeIgnoreAll("colaborador.nome", filtro.getNome())
			.where("colaboradorcargo.matricula = ?", filtro.getMatricula())
			.where("colaborador.cpf = ?", filtro.getCpf())
			.where("departamento = ?", filtro.getDepartamento())
			.where("empresa = ?", filtro.getEmpresa())
			.where("cargo = ?", filtro.getCargo())
	//		.where("projeto = ?",filtro.getProjeto())
			.where("colaborador.dtadmissao >= ?", filtro.getDtinicio())
			.where("colaborador.dtadmissao <= ?", filtro.getDtfim())
			.where("examenatureza = ?", filtro.getExameNatureza())
			.where("colaboradorcargo.dtfim is null");
		if(filtro.getMostrar() != null){
			if(filtro.getMostrar().equals(EmitirautorizacaoReportFiltroMostrar.EXAMES_A_REALIZAR)){
				query
					.where("listaColaboradorexame.dtproximoexame >= ?", SinedDateUtils.currentDate())
					.where("listaColaboradorexame.dtproximoexame <= ?", SinedDateUtils.addDiasData(SinedDateUtils.currentDate(), 7));
					
			}
			else if(filtro.getMostrar().equals(EmitirautorizacaoReportFiltroMostrar.EXAMES_VENCIDOS)){
				query
					.where("listaColaboradorexame.cdcolaboradorexame in ( select ce.cdcolaboradorexame " +
																		" from Colaboradorexame ce " +
																		" where ce.colaborador = colaborador " +
																		" and ce.dtproximoexame < ? " +
																		" and not exists ( select ce2.cdcolaboradorexame " +
																						" from Colaboradorexame ce2 " +
																						" where ce2.colaborador = ce.colaborador " +
																						" and ce2.exametipo = ce.exametipo " +
																						" and ce2.cdcolaboradorexame <> ce.cdcolaboradorexame " +
																						" and ce2.dtproximoexame >= ? ) " +
																		" ) ", new Object[]{SinedDateUtils.currentDate(),SinedDateUtils.currentDate()});
					
			}
		}else {
			query
				.where("listaColaboradorexame.dtproximoexame >= ?", filtro.getDtproximoexameInicio())
				.where("listaColaboradorexame.dtproximoexame <= ?", filtro.getDtproximoexameFim());
		}
		return query
			.orderBy(filtro.getOrderBy())
			.list();
	}
	
	/**
	 * Gera uma lista de colaboradores para a emiss�o de relat�rio
	 * 
	 * @throws SinedException - Caso o par�metro filtro seja null.
	 * @param filtro
	 * @return Lista de Colaborador
	 * @author Jo�o Paulo Zica
	 */
	public Colaborador findForAutorizacaoExameByPk(Integer cdpessoa){
		if(cdpessoa == null)
			throw new SinedException("O par�metro cdpessoa n�o deve ser null.");
		
		StringBuilder campos = new StringBuilder();
		campos.append("colaborador.cdpessoa, cargo.cbo, colaborador.cpf, colaborador.nome, ");
		campos.append("colaborador.dtnascimento, colaborador.dtadmissao, sexo.nome, colaborador.rg");
		return query()
			.select(campos.toString())
			.leftOuterJoin("colaborador.listaColaboradorcargo colaboradorcargo")
			.leftOuterJoin("colaboradorcargo.cargo cargo")
			.leftOuterJoin("colaborador.sexo sexo")
			.leftOuterJoin("cargo.cbo cbo")
			.where("colaborador.cdpessoa = ?", cdpessoa)
			.where("colaboradorcargo.dtfim is null")
			.unique();
	}
	
	/**
	 * M�todo utilizado na listagem de colaborador para exporta��o.
	 * 
	 * @param filtro
	 * @author Jo�o Paulo Zica
	 * @author Hugo Ferreira
	 */
	public List<Colaborador> findForListagemExportarColaborador(ExportarcolaboradorFiltro filtro){
		if (filtro ==  null){
			throw new SinedException("O par�metro filtro n�o pode ser null");
		}
		return query()
			.select("colaborador.cdpessoa, colaborador.nome," +
					"colaborador.cpf, colaborador.dtadmissao," +
					"colaboradorstatusfolha.cdcolaboradorstatusfolha, colaboradorstatusfolha.nome," +
					"colaboradorsituacao.cdcolaboradorsituacao")
			.join("colaborador.colaboradorsituacao colaboradorsituacao")
			.join("colaborador.colaboradorstatusfolha colaboradorstatusfolha")
			.join("colaborador.listaColaboradorcargo colaboradorcargo")
			.join("colaboradorcargo.cargo cargo")
			.join("colaboradorcargo.departamento departamento")
			.where("colaboradorsituacao.cdcolaboradorsituacao <> ? ", Colaboradorsituacao.EM_CONTRATACAO) //n�o buscar colaboradores cuja situa��o � "Em contrata��o"
			.whereLikeIgnoreAll("colaborador.nome", filtro.getNome())
			.where("departamento = ?", filtro.getDepartamento())
			.where("colaboradorcargo.matricula = ?", filtro.getMatricula())
			.where("cargo = ?", filtro.getCargo())
			.where("colaborador.dtadmissao >= ?", filtro.getDtinicio())
			.where("colaborador.dtadmissao <= ?", filtro.getDtfim())
			.where("colaboradorstatusfolha = ?", filtro.getColaboradorstatusfolha())
			.orderBy("colaborador.nome")
			.list();		
	}
	

	/**
	 * M�todo utilizado na listagem de colaborador para cria��o do relat�rio CSV.
	 * 
	 * @param filtro
	 * @author Rafael Salvio Martins
	 */
	public ListagemResult<Colaborador> findForListagemGerarCSVColaborador(ColaboradorFiltro filtro){
		QueryBuilder<Colaborador> query = querySined();
	
		this.updateListagemQuery(query, filtro);
		
		query
			.select("colaborador.cdpessoa, colaborador.nome, colaborador.email, colaborador.cpf, colaborador.dtnascimento, " +
					" colaborador.dtadmissao, colaboradorcargo.cdcolaboradorcargo, colaborador.carteiratrabalho, colaboradorcargo.salario, cargo.nome, departamento.nome, " +
					" colaboradorcargo.dtinicio, colaboradorcargo.dtfim, colaborador.numeroinscricao, " +
					"endereco.logradouro, endereco.numero, endereco.bairro, endereco.complemento, municipio.nome, uf.sigla, endereco.cep, " +
					"endereco.caixapostal, colaborador.rg, tipoinscricao.cdtipoinscricao, tipoinscricao.nome ")
			.leftOuterJoin("colaborador.tipoinscricao tipoinscricao")
			.leftOuterJoin("colaborador.endereco endereco")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf");
		
		query.setIgnoreJoinPaths(new HashSet<String>());
		QueryBuilder<Colaborador> queryBuilder = query;
		return new ListagemResult<Colaborador>(queryBuilder, filtro);
	}
		
		
	
	/**
	 * Gera uma lista com todos os dados do colaborador para criar o arquivo de exporta��o
	 * para o NG Folha.
	 * 
	 * @param whereIn
	 * @author Hugo Ferreira
	 */
	public List<Colaborador> gerarListaArquivoExportacao(String whereIn) {
		if (StringUtils.isEmpty(whereIn)){
			throw new SinedException("O par�metro colaboradores n�o pode ser nulo");
		}
		return query()
			.leftOuterJoinFetch("colaborador.listaColaboradorcargo colaboradorcargo")
			.leftOuterJoinFetch("colaborador.ufctps uf")
			.leftOuterJoinFetch("colaborador.endereco endereco")
			.leftOuterJoinFetch("colaborador.colaboradorstatusfolha colaboradorstatusfolha")
			.leftOuterJoinFetch("colaborador.dirf dirf")
			.leftOuterJoinFetch("endereco.municipio municipio")
			.leftOuterJoinFetch("municipio.uf uf")
			.leftOuterJoinFetch("colaborador.colaboradorsituacao colaboradorsituacao")
			.leftOuterJoinFetch("colaborador.listaColaboradordependente colaboradordependente")
			.leftOuterJoinFetch("colaboradordependente.sexo sexo")
			.leftOuterJoinFetch("colaborador.municipionaturalidade municipionaturalidade")
			.leftOuterJoinFetch("municipionaturalidade.uf ufnaturalidade")
			.leftOuterJoinFetch("colaborador.listaDadobancario dadobancario")
			.leftOuterJoinFetch("dadobancario.banco banco")

			.leftOuterJoinFetch("colaborador.sexo sexo")
			.leftOuterJoinFetch("colaborador.estadocivil estadocivil")
			.leftOuterJoinFetch("colaborador.grauinstrucao grauinstrucao")
			.leftOuterJoinFetch("colaborador.etnia etnia")
			.leftOuterJoinFetch("colaborador.tipodocumentomilitar tipodocumentomilitar")
			.leftOuterJoinFetch("colaborador.ufctps ufctps")
			.leftOuterJoinFetch("colaborador.tipoinscricao tipoinscricao")
			.leftOuterJoinFetch("colaborador.listaTelefone listaTelefone")
			.leftOuterJoinFetch("colaboradorcargo.regimecontratacao regimecontratacao")
			.leftOuterJoinFetch("colaboradorcargo.cargo cargo")
			.leftOuterJoinFetch("colaboradorcargo.departamento departamento")
			.leftOuterJoinFetch("colaboradorcargo.projeto projeto")
			.leftOuterJoinFetch("colaboradorcargo.sindicato sindicato")
			.leftOuterJoinFetch("cargo.cbo cbo")
			
			.whereIn("colaborador.cdpessoa", whereIn)
			.list();		
	}
	
	/**
	 * M�todo para localiza��o de colaborador para altera��o de situa��o
	 * 
	 * @param filtro
	 * @author Jo�o Paulo Zica
	 */
	public List<Colaborador> findColaboradorAltera(AlterarsituacaocolaboradorFilter filtro){
		if (filtro ==  null){
			throw new SinedException("O par�metro filtro n�o pode ser null");
		}
		return
		query()
			.select("colaborador.cdpessoa, colaborador.nome, colaborador.cpf, colaboradorsituacao.nome, colaboradorcargo.dtinicio")
			.from(Colaborador.class, "colaborador")
			.leftOuterJoin("colaborador.listaColaboradorcargo colaboradorcargo")
			.leftOuterJoin("colaborador.colaboradorsituacao colaboradorsituacao")
			.whereLikeIgnoreAll("colaborador.nome", filtro.getNome())
			.where("departamento = ?", filtro.getDepartamento())
			.where("cargo = ?", filtro.getCargo())
			.where("dtinicio >=", filtro.getDtadminicio())
			.where("dtinicio <=", filtro.getDtadmfim())
			.where("dtfim is not null")
			.list();		
	}
	
	
	/**
	 * M�todo para obter o colaborador para alterar cargo.
	 * 
	 * @param colaboradorcargo
	 * @return
	 * @author Flavio
	 */
	public Colaborador findColaboradorAlteraCargo(Colaboradorcargo colaboradorcargo){
		if(colaboradorcargo==null || colaboradorcargo.getCdcolaboradorcargo()==null){
			throw new SinedException("O par�metro cdcolaboradorcargo n�o pode ser null.");
		}
		return
			query()
			.select("colaborador.cdpessoa, colaboradorcargo.dtinicio, colaboradorcargo.dtfim," +
					" colaboradorcargo.regimecontratacao, colaboradorcargo.observacao," +
					" colaboradorsituacao.cdcolaboradorsituacao, colaboradorsituacao.nome, colaborador.nome," +
					" colaborador.cpf, departamento.nome,departamento.cddepartamento, cargo.nome, cargo.cdcargo," +
					" sindicato.cdsindicato, sindicato.nome")
			.join("colaborador.listaColaboradorcargo colaboradorcargo")
			.join("colaboradorcargo.departamento departamento")
			.join("colaboradorcargo.cargo cargo")
			.leftOuterJoin("colaboradorcargo.regimecontratacao regimecontratacao")
			.leftOuterJoin("colaborador.colaboradorsituacao colaboradorsituacao")
			.leftOuterJoin("colaboradorcargo.sindicato sindicato")
			.where("colaboradorcargo = ?", colaboradorcargo)
			.unique();
	}
	
	/**
	 * Atualiza o status de exporta��o para o NG Folha de um ou mais colaboradores.
	 * 
	 * @param colaboradores String de c�digos de colaboradores separados por v�rgula
	 * @param situacao Um status que o colaborador pode assumir (cdstatusfolha)
	 * @author Hugo Ferreira
	 */
	public void alterarStatusFolhaPorCodigo(String colaboradores, Integer situacao) {
		if (colaboradores ==  null || situacao == null){
			throw new SinedException("Par�metros inv�lidos na fun��o alterarStatusFolha: n�o pode ser null.");
		}
		String sql = "UPDATE Colaborador SET cdcolaboradorstatusfolha = " + situacao + " WHERE (cdpessoa) IN (" + colaboradores + ")";
		getJdbcTemplate().execute(sql);
	}
	
	/**
	 * Altera o status NG Folha de um colaborador
	 * 
	 * @param whereIn
	 * @param cdColaboradorStatusFolha
	 * @author Hugo Ferreira
	 */
	public void atualizaColaboradorStatusFolha(String whereIn, Integer cdColaboradorStatusFolha) {
		if (StringUtils.isEmpty(whereIn) || cdColaboradorStatusFolha == null){
			throw new SinedException("Par�metros inv�lidos na fun��o atualizaColaboradorStatusFolha: n�o pode ser null.");
		}
		String sql = "UPDATE Colaborador " +
					 "SET cdcolaboradorstatusfolha = " + cdColaboradorStatusFolha + " " +
					 "WHERE (cdpessoa) IN (" + whereIn + ")";
		getJdbcTemplate().execute(sql);
	}
	
	/**
	 * Altera o status NG Folha de um colaborador
	 * 
	 * @param whereIn
	 * @param alterado
	 * @author Hugo Ferreira
	 */
	public void atualizaAlterado(String whereIn, Boolean alterado) {
		if (StringUtils.isEmpty(whereIn) || alterado == null){
			throw new SinedException("Par�metros inv�lidos na fun��o atualizaAlterado: n�o pode ser null.");
		}
		String sql = "UPDATE Colaborador " +
					 "SET alterado = " + (alterado ? "1" : "0") + " " +
					 "WHERE (cdpessoa) IN (" + whereIn + ")";
		getJdbcTemplate().execute(sql);
	}
	
	
	/**
	 * M�todo para carregar determinados campos de um colaborador informados pelo pr�metros.
	 * 
	 * @param bean
	 * @param campos
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Colaborador load(Colaborador bean, String campos) {
		if(bean==null || bean.getCdpessoa()==null){
			throw new SinedException("Os par�metros bean e cdpessoa n�o podem ser null.");
		}
		QueryBuilder<Colaborador> query = query();
		
		if(campos!=null && !campos.equals("")){
			query.select(campos);
		}
		
		return query
				.where("colaborador.cdpessoa=?",bean.getCdpessoa())
				.unique();
	}
	
	public Colaborador loadWithoutPermissao(Colaborador bean, String campos) {
		if(bean==null || bean.getCdpessoa()==null){
			throw new SinedException("Os par�metros bean e cdpessoa n�o podem ser null.");
		}
		QueryBuilderSined<Colaborador> query = querySined();
		
		if(campos!=null && !campos.equals("")){
			query.select(campos);
		}
		
		return query
		.setUseWhereEmpresa(false)
		.setUseWhereProjeto(false)
		.where("colaborador.cdpessoa=?",bean.getCdpessoa())
		.unique();
	}
	
	/**
	 * M�todo para carregar determinados campos de um colaborador informados pelo pr�metros, utilizando
	 * WHERE IN nos ID's.
	 * 
	 * @param bean
	 * @param campos
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Colaborador> load(String whereIn, String campos) {
		if(whereIn==null || campos==null){
			throw new SinedException("Os par�metros whereIn e campos n�o podem ser null.");
		}
		QueryBuilder<Colaborador> query = query();
		
		if(campos!=null && !campos.equals("")){
			query.select(campos);
		}
		
		return query
		.whereIn("colaborador.cdpessoa",whereIn)
		.list();
	}
	
	/**
	 * M�todo para obter o CPF de um colaborador
	 * 
	 * @param colaborador
	 * @return Cpf
	 * @author Flavio
	 */
	public Cpf getCpfColaborador(Colaborador colaborador){
		 Colaborador bean =	query()
			.select("colaborador.cpf")
			.where("colaborador = ?", colaborador)
			.unique();
		 return bean != null ? bean.getCpf() : null;
	}
	
	/**
	 * Lista de colaboradores por departamento.
	 * 
	 * @param departamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Colaborador> findByDepartamento(Departamento departamento) {
		if (departamento == null || departamento.getCddepartamento() == null) {
			throw new SinedException("Departamento n�o pode ser nulo.");
		}
		return query()
					.select("colaborador.cdpessoa, colaborador.nome")
					.leftOuterJoin("colaborador.listaColaboradorcargo listaColaboradorcargo")
					.leftOuterJoin("listaColaboradorcargo.departamento departamento")
					.where("departamento = ?",departamento)
					.where("listaColaboradorcargo.dtfim is null")
					.orderBy("colaborador.nome")
					.list();
	}

	/**
	 * Recupera uma lista de colaboradores a partir de CPFs separados por v�rgula.
	 * 
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Colaborador> findByCpf(String whereIn) {
		return query()
			.select("colaborador.cdpessoa, colaborador.cpf, colaboradorsituacao.cdcolaboradorsituacao")
			.join("colaborador.colaboradorsituacao colaboradorsituacao")
			.whereIn("colaborador.cpf", whereIn)
			.orderBy("colaborador.cpf")
			.list();
	}
	
	/**
	 * Busca o colaborador pelo CPF.
	 *
	 * @param cpf
	 * @return
	 * @since Jul 5, 2011
	 * @author Rodrigo Freitas
	 */
	public Colaborador findByCpf(Cpf cpf) {
		if(cpf == null){
			throw new SinedException("CPF n�o pode ser nulo.");
		}
		return query()
					.select("colaborador.cdpessoa, colaborador.nome")
					.where("colaborador.cpf = ?", cpf)
					.unique();
	}
	
	/**
	 * Busca o colaborador pelo Nome.
	 *
	 * @param nome
	 * @return
	 * @since Jul 5, 2011
	 * @author Rodrigo Freitas
	 */
	public Colaborador findByNome(String nome) {
		if(nome == null || nome.equals("")){
			throw new SinedException("Nome n�o pode ser nulo.");
		}
		return query()
					.select("colaborador.cdpessoa, colaborador.nome")
					.where("colaborador.nome = ?", nome)
					.unique();
	}

	/**
	 * Recupera uma lista de colaboradores a partir de CNPJs separados por v�rgula.
	 * 
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Colaborador> findByCnpj(String whereIn) {
		return query()
			.select("colaborador.cdpessoa, colaborador.cnpj, colaboradorsituacao.cdcolaboradorsituacao")
			.join("colaborador.colaboradorsituacao colaboradorsituacao")
			.whereIn("colaborador.cnpj", whereIn)
			.orderBy("colaborador.cnpj")
			.list();
	}

	/**
	 * M�todo para obter colaboradores de determinado cargo que est�o ligados a um projeto, ou 
	 * o pr�prio colaborador informado por par�metro.
	 * 
	 * @param cargo
	 * @param projeto
	 * @param colaborador
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Colaborador> findByCargoProjeto(Cargo cargo, Projeto projeto, Colaborador colaborador){
		return 
			query()
			.select("colaborador.cdpessoa,colaborador.nome")
			.leftOuterJoin("colaborador.listaContratocolaborador listaContratocolaborador")
			.leftOuterJoin("listaContratocolaborador.contrato contrato")
			.leftOuterJoin("contrato.rateio rateio")
			.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
			.leftOuterJoin("listaRateioitem.projeto projeto")
			.leftOuterJoin("colaborador.listaColaboradorcargo colaboradorcargo")
			
			.where("colaboradorcargo.dtfim is null")
			.where("projeto = ?", projeto)
			.where("colaboradorcargo.cargo = ?", cargo)
			.or()
			.where("colaborador = ?", colaborador)
			.orderBy("colaborador.nome")
			.list();
	}
	
	public List<Colaborador> findByCargo(Cargo cargo){
		return 
		query()
		.select("colaborador.cdpessoa,colaborador.nome")
		.leftOuterJoin("colaborador.listaColaboradorcargo colaboradorcargo")
		.where("colaboradorcargo.dtfim is null")
		.where("colaboradorcargo.cargo = ?", cargo)
		.orderBy("colaborador.nome")
		.list();
	}

	/**
	 * Carrega a listagem de colaborador para exibi��o no flex.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Colaborador> findAllForFlex() {
		return query()
					.select("colaborador.cdpessoa, colaborador.nome")
					.orderBy("colaborador.nome")
					.list();
	}

	public Colaborador loadForBoleto(Integer cdpessoa) {
		return query()
			.select("colaborador.nome, colaborador.cpf, colaborador.cnpj, endereco.logradouro, endereco.numero, endereco.bairro, endereco.complemento, " +
					"municipio.nome, uf.sigla, endereco.cep, endereco.caixapostal, colaborador.tipopessoa")
			.leftOuterJoin("colaborador.endereco endereco")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.where("colaborador.cdpessoa = ?", cdpessoa)
			.unique();
	}
	
	/**
	 * M�todo que carrega email do colaborador
	 * 
	 * @param colaborador
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Colaborador carregaEmailColaborador(Colaborador colaborador) {
		return query()
			.select("colaborador.cdpessoa, colaborador.email")
			.where("colaborador = ?", colaborador)
			.unique();
	}
	
	/**
	 * Carrega todos os <code>Colaboradores</code> de um 
	 * <code>Contrato</code> passado por par�metro.
	 *
	 * @param contrato
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public List<Colaborador> findByContrato(Contrato contrato) {
		return query()
					.select("colaborador.cdpessoa, colaborador.nome")
					.leftOuterJoin("colaborador.listaContratocolaborador listaContratocolaborador")
					.leftOuterJoin("listaContratocolaborador.contrato contrato")
					.where("contrato = ?", contrato)
					.orderBy("colaborador.nome")
					.list();
	}
	
	/**
	 * Carrega colaboradores, conforme contrato e que n�o estejam nas situa��es:
	 * AFASTADO ou DEMITIDO;
	 * @param contrato
	 * @return
	 * @author Taidson
	 * @since 01/12/2010
	 */
	public List<Colaborador> loadColaboradores(Contrato contrato) {
		QueryBuilder<Colaborador> query = query()
					.select("colaborador.cdpessoa, colaborador.nome, colaboradorsituacao.cdcolaboradorsituacao")
					.join("colaborador.colaboradorsituacao colaboradorsituacao")
					.openParentheses()
					.where("colaboradorsituacao.cdcolaboradorsituacao = ?", Colaboradorsituacao.NORMAL)
					.or()
					.where("colaboradorsituacao.cdcolaboradorsituacao = ?", Colaboradorsituacao.CONTRATO_EXPERIENCIA)
					.or()
					.where("colaboradorsituacao.cdcolaboradorsituacao = ?", Colaboradorsituacao.AVISO_PREVIO)
					.or()
					.where("colaboradorsituacao.cdcolaboradorsituacao = ?", Colaboradorsituacao.EM_CONTRATACAO)
					.or()
					.where("colaboradorsituacao.cdcolaboradorsituacao = ?", Colaboradorsituacao.FERIAS)
					.closeParentheses()
					.orderBy("colaborador.nome");
		
		if(contrato != null && contrato.getCdcontrato() != null){
			query
				.leftOuterJoin("colaborador.listaContratocolaborador listaContratocolaborador", true)
				.leftOuterJoin("listaContratocolaborador.contrato contrato", true)
				.where("contrato = ?", contrato);
		}
		return query.list();
	}
	/**
	 * 
	 * @param contrato
	 * @return
	 */
	public List<Colaborador> loadColaboradoresAtivos() {
		return query()
					.select("colaborador.cdpessoa, colaborador.nome, colaboradorsituacao.cdcolaboradorsituacao")
					.join("colaborador.colaboradorsituacao colaboradorsituacao")
					.where("colaboradorsituacao.cdcolaboradorsituacao <> ?", Colaboradorsituacao.DEMITIDO)
					.where("colaboradorsituacao.cdcolaboradorsituacao <> ?", Colaboradorsituacao.AFASTADO)
					.orderBy("colaborador.nome")
					.list();
		
	}
	/**
	 * M�todo que retorna o respons�vel pelo veiculo
	 * 
	 * @param veiculo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Colaborador findColaboradorResponsavelVeiculo(Veiculo veiculo) {
		return query()
			.select("colaborador.cdpessoa, colaborador.nome")
			.where("colaborador.id = (select v.colaborador.id from Veiculo v where v.id = "+veiculo.getCdveiculo()+")")
			.unique();
	}
	
	/** M�todo que retorna o respons�vel pela conta no crm
	 * 
	 * @param contacrm
	 * @return
	 * @author Mario Caixeta
	 */
	public List<Colaborador> findColaboradorResponsavelContacrm(Contacrm contacrm){
		return query()
			.select("colaborador.cdpessoa, colaborador.nome")
			.where("colaborador.id = (select c.responsavel.id from Contacrm c where c.id = "+contacrm.getCdcontacrm()+")")
			.list();
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Colaborador> findDesbloqueado(){
		SinedUtil.markAsReader();
		List<Colaborador> listaUpdate = getJdbcTemplate().query(
				
				"SELECT P.CDPESSOA AS CODIGO, P.NOME AS NOMEPESSOA, P.EMAIL AS EMAILPESSOA " +
				"FROM PESSOA P " +
				"JOIN COLABORADOR C ON C.CDPESSOA = P.CDPESSOA " +
				"JOIN USUARIO U ON U.CDPESSOA = P.CDPESSOA " +
				"WHERE U.BLOQUEADO = FALSE " +
				"ORDER BY P.NOME", 
				
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Colaborador bean = new Colaborador();
				
				bean.setNome(rs.getString("NOMEPESSOA"));
				bean.setEmail(rs.getString("EMAILPESSOA"));
				bean.setCdpessoa(rs.getInt("CODIGO"));				
				return bean;
			}
		});
		
		return listaUpdate;
	}

	/**
	 * M�todo que realiza busca geral em colaboradores de acordo com o par�metro informado na busca
	 * 
	 * @param busca
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Colaborador> findColaboradoresForBuscaGeral(String busca) {
		return query()
			.select("colaborador.cdpessoa, colaborador.nome")
			.openParentheses()
				.whereLikeIgnoreAll("colaborador.mae", busca).or()
				.whereLikeIgnoreAll("colaborador.pai", busca).or()
				.whereLikeIgnoreAll("colaborador.nome", busca).or()
				.whereLikeIgnoreAll("colaborador.email", busca)
			.closeParentheses()
			.orderBy("colaborador.nome")
			.list();
	}
	
	/**
	 * M�todo que retorna os colaboradores que est�o fazendo anivers�rio no determinado m�s
	 * 
	 * @param mes
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Colaborador> findColaboradoresAniversariantesDoMesFlex(Integer mes) {
		return query()
			.select("colaborador.cdpessoa, colaborador.nome, colaborador.dtnascimento")
			.leftOuterJoin("colaborador.colaboradorsituacao colaboradorsituacao")
			.where("extract(month from colaborador.dtnascimento) = ?", mes)
			.whereIn("colaboradorsituacao.cdcolaboradorsituacao", Colaboradorsituacao.NORMAL + "," +
																	Colaboradorsituacao.CONTRATO_EXPERIENCIA  + "," +
																	Colaboradorsituacao.AVISO_PREVIO  + "," +
																	Colaboradorsituacao.FERIAS  + "," +
																	Colaboradorsituacao.EM_CONTRATACAO) 
			.orderBy("extract(day from colaborador.dtnascimento)")
			.list();
	}
	
	public List<Colaborador> sugestaoColaboradorMenosAtendimentoPeriodo(Material material, String dtReferencia, String dtreferenciaAte, Empresa empresa) {
		return sugestaoColaboradorMenosAtendimentoPeriodo(material, dtReferencia, dtreferenciaAte, empresa, null);
	}

	/**
	 * M�todo que retorna os colaboradores ativos referentes ao material e em ordem de colaboradores com menos agendamento de servi�o
	 * 
	 * @param material
	 * @param dtReferencia
	 * @param dtreferenciaAte
	 * @return
	 * @author Tom�s Rabelo
	 * @param empresa 
	 */
	@SuppressWarnings("unchecked")
	public List<Colaborador> sugestaoColaboradorMenosAtendimentoPeriodo(Material material, String dtReferencia, String dtreferenciaAte, Empresa empresa, String q) {

		if(material == null || material.getCdmaterial() == null || dtReferencia == null || dtreferenciaAte == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		String whereDatas = "";
		if(dtReferencia != null && !dtReferencia.equals(""))
			whereDatas += " AND AGS.DATA >= TO_DATE('"+dtReferencia+"', 'DD/MM/YYYY')";
		if(dtreferenciaAte != null && !dtreferenciaAte.equals(""))
			whereDatas += " AND AGS.DATA <= TO_DATE('"+dtreferenciaAte+"', 'DD/MM/YYYY')";
		
		String whereUsuarioLogado = "";
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioLogado != null && usuarioLogado.getRestricaoclientevendedor() != null && usuarioLogado.getRestricaoclientevendedor()){
			whereUsuarioLogado += " AND P.CDPESSOA ="+SinedUtil.getUsuarioLogado().getCdpessoa()+" ";
		}

		SinedUtil.markAsReader();
		List<Colaborador> list = getJdbcTemplate().query(
				"SELECT P.CDPESSOA AS CDPESSOA, P.NOME AS NOME, " +
				"(SELECT COUNT(*) FROM AGENDAMENTOSERVICO AGS " +
				" JOIN COLABORADOR CO ON CO.CDPESSOA = AGS.CDCOLABORADOR " +
				" WHERE CO.CDPESSOA = C.CDPESSOA " +whereDatas+
				") AS QTDEAGENDAMENTOS "+ 
				"FROM COLABORADOR C "+
				"JOIN PESSOA P ON P.CDPESSOA = C.CDPESSOA " +
				"WHERE P.CDPESSOA IN (SELECT C1.CDPESSOA FROM MATERIAL M " +
									 "JOIN MATERIALCOLABORADOR MC ON MC.CDMATERIAL = M.CDMATERIAL " +
									 "JOIN COLABORADOR C1 ON C1.CDPESSOA = MC.CDCOLABORADOR " +
									 "WHERE M.CDMATERIAL = "+material.getCdmaterial()+" AND MC.ATIVO = TRUE AND C1.CDPESSOA = P.CDPESSOA) " +
				whereUsuarioLogado +
				"AND EXISTS (SELECT CC.CDCOLABORADORCARGO " +
							"FROM COLABORADORCARGO CC " +
							"WHERE CC.DTFIM IS NULL " +
							"AND CC.CDPESSOA = P.CDPESSOA " +
							"AND (CC.CDEMPRESA = " + empresa.getCdpessoa() + " OR CC.CDEMPRESA IS NULL) " +
							") " +
							
				( q != null ? "AND upper(p.nome) like upper('% " + Util.strings.tiraAcento(q) + "%')" : "") +
				"ORDER BY QTDEAGENDAMENTOS ASC, P.NOME DESC"
				,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						Colaborador colaborador = new Colaborador();
						colaborador.setCdpessoa(rs.getInt("CDPESSOA"));
						colaborador.setNome(rs.getString("NOME"));
						
						return colaborador;
					}
				});
		
		return list;
	}

	/**
	 * M�todo que retorna a escala de hor�rio do Colaborador
	 * 
	 * @param colaborador
	 * @return
	 * @author Tom�s Rabelo
	 * @param dateate 
	 * @param datede 
	 */
	public Colaborador getEscalaColaborador(Colaborador colaborador, Date datede, Date dateate) {
		if(colaborador == null || colaborador.getCdpessoa() == null){
			throw new SinedException("Colaborador n�o pode ser nulo.");
		}
		return query()
			.select("colaborador.cdpessoa, escala.cdescala, escala.nome")
			.join("colaborador.listaEscalacolaborador listaEscalacolaborador")
			.join("listaEscalacolaborador.escala escala")
			.where("colaborador = ?", colaborador)
			.openParentheses()
				.openParentheses()
					.where("listaEscalacolaborador.dtinicio <= ?", datede).or()
					.where("listaEscalacolaborador.dtfim >= ?", datede)
				.closeParentheses().or()
				.openParentheses()
					.where("listaEscalacolaborador.dtinicio <= ?", datede).or()
					.where("listaEscalacolaborador.dtfim is null")
				.closeParentheses().or()
				.openParentheses()
					.where("listaEscalacolaborador.dtinicio <= ?", dateate).or()
					.where("listaEscalacolaborador.dtfim >= ?", dateate)
				.closeParentheses().or()
				.openParentheses()
					.where("listaEscalacolaborador.dtinicio <= ?", dateate).or()
					.where("listaEscalacolaborador.dtfim is null")
				.closeParentheses()
			.closeParentheses()
			.unique();
	}
	
	/**M�todo para autocomplete de Colaborador
	 * 
	 * @param q
	 * @return
	 * @author M�rio Caixeta
	 */
	public List<Colaborador> findAutocomplete(String q) {
		return querySined()
					
					.select("colaborador.nome, colaborador.cpf, colaborador.cdpessoa")
//					.leftOuterJoin("colaborador.listaColaboradorcargo listaColaboradorcargo")
					.leftOuterJoin("colaborador.colaboradorsituacao colaboradorsituacao")
					.whereLikeIgnoreAll("colaborador.nome", q)
					.where("colaboradorsituacao.cdcolaboradorsituacao <> ?", Colaboradorsituacao.DEMITIDO)
//					.openParentheses()
//					.where("exists (select c.cdpessoa from Colaboradorcargo cc " +
//							"left outer join cc.colaborador c " +
//							"left outer join cc.cargo cg " +
//							"where cc.dtfim is null " +
//							"and c.cdpessoa = colaborador.cdpessoa ) ")
//					.or()
//					.where("listaColaboradorcargo is null")
//					.closeParentheses()
					.orderBy("colaborador.nome")
					.list();
	}
	
	/**M�todo autocomplete de Colaborador para tela de emiss�o de Funil de vendas. 
	 * 
	 * @param q
	 * @return
	 * @author Jo�o Vitor
	 */
	public List<Colaborador> findAutocompleteForFunilVendas(String q) {
		return querySined()
					
					.select("colaborador.nome, colaborador.cpf, colaborador.cdpessoa")
					.leftOuterJoin("colaborador.colaboradorsituacao colaboradorsituacao")
					.whereLikeIgnoreAll("colaborador.nome", q)
					.where("colaboradorsituacao.cdcolaboradorsituacao <> ?", Colaboradorsituacao.DEMITIDO)
					.where("exists(select 1 from Oportunidade op where op.responsavel.cdpessoa = colaborador.cdpessoa " +
							"and op.tiporesponsavel = ?)", Tiporesponsavel.COLABORADOR)
					.orderBy("colaborador.nome")
					.list();
	}
	
	/**M�todo que carrega um Colaborador para o processo em Contrato
	 * 
	 * @param whereIn
	 * @return
	 * @author M�rio Caixeta
	 */
	public Colaborador carregaColaborador(String whereIn) {
		return query()
				.select("colaborador.cdpessoa, colaborador.nome")
					.whereIn("colaborador.cdpessoa", whereIn)
					.orderBy("colaborador.nome")
				.unique();
	}
	public List<Colaborador> colaboradorArea(Area area){
		return query()
		.select("colaborador.nome, colaborador.cdpessoa")
		.join("colaborador.listaColaboradorarea listaColaboradorarea")
		.join("listaColaboradorarea.area area")
		.where("area =?", area)
		.orderBy("colaborador.nome")
		.list();
	}

	/**
	 * M�todo que carrega Colaborador para formul�rio PPP
	 * 
	 * @param whereIn
	 * @return
	 */
	public List<Colaborador> carregaColaboradorFormularioPPP(String whereIn, Integer matricula) {
		if(whereIn == null || whereIn.equals(""))
			throw new SinedException("Par�metros inv�lidos!");
		
		return query()
			.select("colaborador.cdpessoa, colaborador.nome, colaborador.cdpessoa, colaborador.numeroinscricao, colaborador.ctps, " +
					"colaborador.seriectps, colaborador.dtnascimento, colaborador.brpdh, colaborador.dtadmissao, ufctps.cduf, ufctps.sigla, " +
					"sexo.cdsexo, sexo.nome, listaEscalacolaborador.cdescalacolaborador, escala.cdescala, escala.nome, listaColaboradorcargo.cdcolaboradorcargo, listaColaboradorexame.resultadonormal, " +
					"listaColaboradoracidente.cdcolaboradoracidente, listaColaboradoracidente.registro, listaColaboradoracidente.dtregistro, " +
					"listaColaboradorexame.cdcolaboradorexame, listaColaboradorexame.dtexame, listaColaboradorexame.examers, listaColaboradorexame.resultadoalterado, " +
					"listaColaboradorexame.resultadoestavel, listaColaboradorexame.resultadoagravamento, listaColaboradorexame.resultadoocupacional, " +
					"listaColaboradorexame.resultadonaoocupacional, exameresponsavel.cdexameresponsavel, exameresponsavel.nome, exameresponsavel.nit, " +
					"exameresponsavel.registroconselho, exametipo.cdexametipo, exametipo.nome, examenatureza.nome")
			.join("colaborador.sexo sexo")
			.leftOuterJoin("colaborador.listaEscalacolaborador listaEscalacolaborador")
			.leftOuterJoin("listaEscalacolaborador.escala escala")
			.leftOuterJoin("colaborador.ufctps ufctps")
			.leftOuterJoin("colaborador.listaColaboradorcargo listaColaboradorcargo")
			.leftOuterJoin("colaborador.listaColaboradorexame listaColaboradorexame")
			.leftOuterJoin("colaborador.listaColaboradoracidente listaColaboradoracidente")
			.leftOuterJoin("listaColaboradorexame.exameresponsavel exameresponsavel")
			.leftOuterJoin("listaColaboradorexame.exametipo exametipo")
			.leftOuterJoin("listaColaboradorexame.examenatureza examenatureza")
			.whereIn("colaborador.cdpessoa", whereIn)
			.where("listaColaboradorcargo.matricula = ?", matricula)
			.orderBy("listaColaboradoracidente.dtregistro desc, listaColaboradorexame.dtexame desc")
			.list();
	}
	
	
	/**M�todo que carrega uma lista de colaboradores para emiss�o de ficha de colaborador
	 * 
	 * @author Taidson
	 * @since 30/04/2010
	 */
	public List<Colaborador> findForRelatorioFichaColaborador(String colaboradoresSelecionadas) {
		if (StringUtils.isEmpty(colaboradoresSelecionadas)) throw new SinedException("Deve haver pelo menos um colaborador selecionado");

		return querySined()
				.select("colaborador.cdpessoa, colaborador.nome, colaborador.nomeconjuge, colaborador.cdpessoa, colaborador.numeroinscricao, colaborador.ctps, listaColaboradorcargo.matricula, " +
					"colaborador.seriectps, colaborador.dtnascimento, colaborador.brpdh, colaborador.dtadmissao, ufctps.cduf, ufctps.sigla, " +
					"sexo.cdsexo, sexo.nome, listaColaboradorcargo.cdcolaboradorcargo, " +
					"listaColaboradoracidente.cdcolaboradoracidente, listaColaboradoracidente.registro, listaColaboradoracidente.dtregistro, " +
					"listaColaboradorexame.cdcolaboradorexame, listaColaboradorexame.dtexame, listaColaboradorexame.examers, listaColaboradorexame.resultadoalterado, " +
					"listaColaboradorexame.resultadoestavel, listaColaboradorexame.resultadoagravamento, listaColaboradorexame.resultadoocupacional, " +
					"listaColaboradorexame.resultadonaoocupacional, exameresponsavel.cdexameresponsavel, exameresponsavel.nome, exameresponsavel.nit, " +
					"exameresponsavel.registroconselho, exametipo.cdexametipo, exametipo.nome, examenatureza.nome, colaborador.dtnascimento," +
					"municipionaturalidade.nome, ufnaturalidade.nome, grauinstrucao.nome, colaborador.email, colaborador.pai, colaborador.mae," +
					"etnia.nome, colaborador.tiposanguineo, colaborador.nacionalidade, colaboradortipo.nome, estadocivil.nome," +
					"colaboradordeficiente.nome, listaTelefone.telefone, telefonetipo.cdtelefonetipo, telefonetipo.nome, endereco.logradouro, endereco.numero, " +
					"endereco.complemento, endereco.bairro, endereco.cep, endereco.caixapostal, municipio.nome, uf.nome, colaborador.rg, " +
					"colaborador.orgaoemissorrg, colaborador.dtemissaorg, colaborador.cpf, colaborador.tituloeleitoral, colaborador.zonaeleitoral, " +
					"colaborador.secaoeleitoral, colaborador.ctps, colaborador.dtEmissaoCtps, colaborador.documentomilitar, colaborador.numeroinscricao, " +
					"colaborador.cnh, colaborador.dtemissaocnh, colaborador.dtvalidadecnh, dirf.nome, ufcnh.sigla, tipodocumentomilitar.nome, " +
					"tipoinscricao.nome, tipocarteiratrabalho.nome, categoriacnh.cdcategoriacnh, colaborador.fgtsopcao, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
					"colaboradorformapagamento.nome, dadobancario.conta, banco.nome, tipoconta.nome, dadobancario.operacao, dadobancario.agencia, " +
					"dadobancario.dvagencia, dadobancario.dvconta, colaborador.dtadmissao, departamento.nome, cargo.nome, sindicato.nome, listaColaboradorcargo.salario, " +
					"regimecontratacao.nome, colaboradorsituacao.nome, foto.cdarquivo, " +
					"projeto.cdprojeto, projeto.nome, listaColaboradorcargo.dtfim, logomarca.cdarquivo, listaEscalacolaborador.cdescalacolaborador, escala.cdescala, escala.nome ")
			.join("colaborador.sexo sexo")
			.join("colaborador.grauinstrucao grauinstrucao")
			.join("colaborador.etnia etnia")
			.join("colaborador.colaboradortipo colaboradortipo")
			.join("colaborador.estadocivil estadocivil")
			.join("colaborador.colaboradordeficiente colaboradordeficiente")
			.join("colaborador.endereco endereco")
			.join("endereco.municipio municipio")
			.join("municipio.uf uf")
			.join("colaborador.colaboradorformapagamento colaboradorformapagamento")
			.leftOuterJoin("colaborador.listaEscalacolaborador listaEscalacolaborador")
			.leftOuterJoin("listaEscalacolaborador.escala escala")
			.leftOuterJoin("colaborador.foto foto")
			.leftOuterJoin("colaborador.dirf dirf")
			.leftOuterJoin("colaborador.ufcnh ufcnh")
			.leftOuterJoin("colaborador.tipodocumentomilitar tipodocumentomilitar")
			.leftOuterJoin("colaborador.listaTelefone listaTelefone")
			.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
			.leftOuterJoin("colaborador.tipoinscricao tipoinscricao")
			.leftOuterJoin("colaborador.tipocarteiratrabalho tipocarteiratrabalho")
			.leftOuterJoin("colaborador.ufctps ufctps")
			.leftOuterJoin("colaborador.listaColaboradorcargo listaColaboradorcargo")
			.leftOuterJoin("listaColaboradorcargo.cargo cargo")
			.leftOuterJoin("listaColaboradorcargo.empresa empresa")
			.leftOuterJoin("listaColaboradorcargo.departamento departamento")
			.leftOuterJoin("listaColaboradorcargo.sindicato sindicato")
			.leftOuterJoin("listaColaboradorcargo.projeto projeto")
			.leftOuterJoin("listaColaboradorcargo.regimecontratacao regimecontratacao")
			.leftOuterJoin("colaborador.listaColaboradorexame listaColaboradorexame")
			.leftOuterJoin("colaborador.listaColaboradoracidente listaColaboradoracidente")
			.leftOuterJoin("colaborador.municipionaturalidade municipionaturalidade")
			.leftOuterJoin("municipionaturalidade.uf ufnaturalidade")
			.leftOuterJoin("listaColaboradorexame.exameresponsavel exameresponsavel")
			.leftOuterJoin("listaColaboradorexame.exametipo exametipo")
			.leftOuterJoin("listaColaboradorexame.examenatureza examenatureza")
			.leftOuterJoin("colaborador.listaColaboradorcategoriacnh listaColaboradorcategoriacnh")
			.leftOuterJoin("listaColaboradorcategoriacnh.categoriacnh categoriacnh")	
			.leftOuterJoin("colaborador.listaDadobancario dadobancario")
			.leftOuterJoin("dadobancario.tipoconta tipoconta")
			.leftOuterJoin("dadobancario.banco banco")
			.leftOuterJoin("colaborador.colaboradorsituacao colaboradorsituacao")
//			.leftOuterJoin("colaborador.valetransporte valetransporte")
			.leftOuterJoin("empresa.logomarca logomarca")
			.whereIn("colaborador.cdpessoa", colaboradoresSelecionadas)
			.orderBy("colaborador.nome")
			.list();
	}
	/**M�todo que carrega uma lista de colaboradores para emiss�o de ficha de colaborador
	 * 
	 * @author Taidson
	 * @since 30/04/2010
	 */
	public List<Colaborador> findForRelatorioAgendaServico(ColaboradoragendaservicoReportFiltro filtro) {
		if (filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		
		return query()
		.select("colaborador.cdpessoa, colaborador.nome, colaborador.cdpessoa")
		.where("colaborador", filtro.getColaborador())
		.orderBy("colaborador.nome")
		.list();
	}
	
	/**Preenche combo de colaboradores de acordo com o servi�o selecionado.
	 * 
	 * @author Taidson
	 * @since 07/05/2010
	 */
	@SuppressWarnings("unchecked")
	public List<Colaborador> comboColaboradorAgendamento(Material material) {

		SinedUtil.markAsReader();
		List<Colaborador> list = getJdbcTemplate().query(
				"SELECT P.CDPESSOA AS CDPESSOA, P.NOME AS NOME " +
				"FROM COLABORADOR C "+
				"JOIN PESSOA P ON P.CDPESSOA = C.CDPESSOA "+
				"WHERE P.CDPESSOA IN (SELECT C1.CDPESSOA FROM MATERIAL M " +
									 "JOIN MATERIALCOLABORADOR MC ON MC.CDMATERIAL = M.CDMATERIAL " +
									 "JOIN COLABORADOR C1 ON C1.CDPESSOA = MC.CDCOLABORADOR " +
									 "WHERE M.CDMATERIAL = "+material.getCdmaterial()+
				") ORDER BY P.NOME"
				,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						Colaborador colaborador = new Colaborador();
						colaborador.setCdpessoa(rs.getInt("CDPESSOA"));
						colaborador.setNome(rs.getString("NOME"));
						
						return colaborador;
					}
				});
		
		return list;
	}

	/**
	 * Busca os colaboradores que tem usu�rios desbloquedos no sistema.
	 * Se passar o departamento ele busca somente daquele departamento.
	 *
	 * @param departamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Colaborador> findForSolicitacaoservico(Departamento departamento) {
		QueryBuilder<Colaborador> query = query()
											.select("colaborador.cdpessoa, colaborador.nome, colaborador.email")
											.where("colaborador.cdpessoa in (select u.cdpessoa from Usuario u where u.bloqueado = false or u.bloqueado is null)")
											.orderBy("colaborador.nome");
		
		if(departamento != null){
			query
				.leftOuterJoin("colaborador.listaColaboradorcargo listaColaboradorcargo")
				.leftOuterJoin("listaColaboradorcargo.departamento departamento")
				.where("departamento = ?",departamento)
				.where("listaColaboradorcargo.dtfim is null");
		}
		
		return query.list();
	}

	/**
	 * Verifica se h� colaborador para o cdpessoa informado.
	 * @param cdpessoa
	 * @return
	 * @author Taidson
	 * @since 19/07/2010
	 */
	public Colaborador carregaColaboradorresponsavel(Integer cdpessoa) {
		return query()
				.select("colaborador.cdpessoa, colaborador.nome")
					.where("colaborador.cdpessoa = ?", cdpessoa)
				.unique();
	}

	@SuppressWarnings("unchecked")
	public List<ColaboradoragendaservicoReportBean> buscarListagemColaboradorAgendamento(final ColaboradoragendaservicoReportFiltro filtro, boolean fromReport) {
		String sql = "SELECT * " +
						"FROM vwcolaboradoragendamento vw " +
						"WHERE 1 = 1 " +
						(filtro.getColaborador() != null ? ("AND vw.colaborador_cdpessoa = " + filtro.getColaborador().getCdpessoa() + " ") : "") +
						(filtro.getMaterial() != null ? ("AND vw.servico_cdmaterial = " + filtro.getMaterial().getCdmaterial() + " ") : "") +
						(filtro.getDtinicio() != null ? ("AND vw.dtagendamento >= '" + SinedDateUtils.toStringPostgre(filtro.getDtinicio()) + "' ") : "") +
						(filtro.getDtfim() != null ? ("AND vw.dtagendamento <= '" + SinedDateUtils.toStringPostgre(filtro.getDtfim()) + "' ") : "") +
						(filtro.getEmpresa() != null ? ("AND vw.empresa_cdempresa = " + filtro.getEmpresa().getCdpessoa() + " ") : "") +
						(filtro.getCliente() != null ? ("AND vw.cliente_cdpessoa = " + filtro.getCliente().getCdpessoa() + " ") : "") +
						(filtro.getCategoria() != null ? ("AND vw.cliente_cdpessoa in (SELECT ps.cdpessoa FROM Pessoacategoria ps WHERE ps.cdcategoria = " + filtro.getCategoria().getCdcategoria() + ") ") : "");
		
		if(fromReport){
			sql += "ORDER BY vw.colaborador_nome" + (filtro.getOrderby() != null && filtro.getOrderby().getQuery() == 0 ? (", " + filtro.getOrderby().getCampo() + (filtro.getAsc() != null && !filtro.getAsc() ? " DESC " : " ")) : "");
		} else {
			sql += (filtro.getOrderby() != null && filtro.getOrderby().getQuery() == 0 ? ("ORDER BY " + filtro.getOrderby().getCampo() + (filtro.getAsc() != null && !filtro.getAsc() ? " DESC " : " ")) : "");
		}
		
		String sql2 = "SELECT * " +
						"FROM vwcolaboradoragendamentovalores vw " +
						"WHERE 1 = 1 " +
						(filtro.getDtinicio() != null ? ("AND vw.dtagendamento >= '" + SinedDateUtils.toStringPostgre(filtro.getDtinicio()) + "' ") : "") +
						(filtro.getDtfim() != null ? ("AND vw.dtagendamento <= '" + SinedDateUtils.toStringPostgre(filtro.getDtfim()) + "' ") : "");
		
		SinedUtil.markAsReader();
		List<ColaboradoragendaservicoReportBean> lista = getJdbcTemplate().query(sql,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						ColaboradoragendaservicoReportBean bean = new ColaboradoragendaservicoReportBean();
						
						bean.setCdagendamentoservicodocumento(rs.getInt("cdagendamentoservicodocumento"));
						bean.setDtagendamento(rs.getDate("dtagendamento"));
						bean.setColaborador(new Colaborador(rs.getInt("colaborador_cdpessoa"), rs.getString("colaborador_nome")));
						
						Cliente cliente;
						if(rs.getObject("cliente_identificador") != null){
							cliente = new Cliente(rs.getInt("cliente_cdpessoa"), rs.getString("cliente_nome"), rs.getString("cliente_identificador"));
						} else {
							cliente = new Cliente(rs.getInt("cliente_cdpessoa"), rs.getString("cliente_nome"));
						}
						
						bean.setCliente(cliente);
						bean.setServico(new Material(rs.getInt("servico_cdmaterial"), rs.getString("servico_nome")));
						bean.setQuantidade(rs.getInt("quantidade"));
						bean.setCancelados(rs.getInt("cancelados"));
						bean.setPrevisto(new Money(rs.getInt("previsto"), true));
						
						return bean;
					}
				});

		SinedUtil.markAsReader();
		List<ColaboradoragendaservicoReportBean> listavalores = getJdbcTemplate().query(sql2,
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						ColaboradoragendaservicoReportBean bean = new ColaboradoragendaservicoReportBean();
						int devido = rs.getInt("devido");
						int recebido = rs.getInt("recebido");
						
						bean.setCdagendamentoservicodocumento(rs.getInt("cdagendamentoservicodocumento"));
						bean.setDevido(new Money(devido, true));
						bean.setRecebido(new Money(recebido, true));
						bean.setSaldo(new Money(recebido-devido, true));
						
						return bean;
					}
				});
		
		
		if(lista!= null && lista.size() > 0 && listavalores != null && listavalores.size() > 0){
			int idx;
			for (ColaboradoragendaservicoReportBean b2 : lista) {
				idx = listavalores.indexOf(b2);
				
				if(idx != -1){
					ColaboradoragendaservicoReportBean b1 = listavalores.get(idx);
					
					b2.setDevido(b1.getDevido());
					b2.setRecebido(b1.getRecebido());
					b2.setSaldo(b1.getSaldo());
				}
			}
			
			if(filtro.getOrderby() != null && filtro.getOrderby().getQuery() == 1){
				switch (filtro.getOrderby()) {
					case DEVIDO:
						Collections.sort(lista,new Comparator<ColaboradoragendaservicoReportBean>(){
							public int compare(ColaboradoragendaservicoReportBean o1, ColaboradoragendaservicoReportBean o2) {
								if (filtro.getAsc() == null || filtro.getAsc()) 
									return o1.getDevido().compareTo(o2.getDevido());
								else 
									return o2.getDevido().compareTo(o1.getDevido());
							}
						});
						break;
					case RECEBIDO:
						Collections.sort(lista,new Comparator<ColaboradoragendaservicoReportBean>(){
							public int compare(ColaboradoragendaservicoReportBean o1, ColaboradoragendaservicoReportBean o2) {
								if (filtro.getAsc() == null || filtro.getAsc()) 
									return o1.getRecebido().compareTo(o2.getRecebido());
								else 
									return o2.getRecebido().compareTo(o1.getRecebido());
							}
						});
						break;
					case SALDO:
						Collections.sort(lista,new Comparator<ColaboradoragendaservicoReportBean>(){
							public int compare(ColaboradoragendaservicoReportBean o1, ColaboradoragendaservicoReportBean o2) {
								if (filtro.getAsc() == null || filtro.getAsc()) 
									return o1.getSaldo().compareTo(o2.getSaldo());
								else 
									return o2.getSaldo().compareTo(o1.getSaldo());
							}
						});
						break;
					default:
						break;
				}
			}
		}
		
		return lista;
	}
	
	/**
	 * Busca o Colaborador pela Matr�cula. Caso seja informada a empresa, a buca ser� feita por matr�cula e empresa.
	 *
	 * @param matricula
	 * @param empresa
	 * @return
	 * @since Jun 10, 2011
	 * @author Rodrigo Freitas
	 */
	public Colaborador findByMatricula(Long matricula, Empresa empresa) {
		 QueryBuilder<Colaborador> query = query()
			.select("colaborador.cdpessoa, colaborador.nome, " +
					"centroCusto.cdcentrocusto, centroCusto.nome, " +
					"projeto.cdprojeto, projeto.nome")
			.leftOuterJoin("colaborador.listaColaboradorcargo colaboradorcargo")
			.leftOuterJoin("colaboradorcargo.centroCusto centroCusto")
			.leftOuterJoin("colaboradorcargo.projeto projeto")
			.where("colaboradorcargo.matricula = ?", matricula);
		
		if(empresa != null && empresa.getCdpessoa() != null){
			query.openParentheses()
				.where("colaboradorcargo.empresa = ?", empresa)
				.or()
				.where("colaboradorcargo.empresa is null")
			.closeParentheses();
		}
					
				
		List<Colaborador> lista = query
				.orderBy("colaboradorcargo.dtinicio desc")
				.list();
		
		if(lista == null || lista.size() == 0){
			return null;
		} else {
			return lista.get(0);
		}
	}
	
	public List<Colaborador> findAllByMatricula(Long matricula) {
		return query()
			.select("colaborador.cdpessoa, colaborador.nome, " +
					"centroCusto.cdcentrocusto, centroCusto.nome, " +
					"projeto.cdprojeto, projeto.nome, " +
					"empresa.cdpessoa")
			.leftOuterJoin("colaborador.listaColaboradorcargo colaboradorcargo")
			.leftOuterJoin("colaboradorcargo.centroCusto centroCusto")
			.leftOuterJoin("colaboradorcargo.projeto projeto")
			.leftOuterJoin("colaboradorcargo.empresa empresa")
			.where("colaboradorcargo.matricula = ?", matricula)
			.orderBy("colaboradorcargo.dtinicio desc")
			.list();
	}
	
	/**
	 * Busca o Colaborador pela Numero de Inscri��o.
	 *
	 * @param matricula
	 * @return
	 * @since 
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public Colaborador findByNumeroinscricao(String numeroInscricao) {
		StringBuilder sql = new StringBuilder();
		
		sql.append(" select p.cdpessoa, p.nome ");
		sql.append(" from colaborador c ");
		sql.append(" join pessoa p on p.cdpessoa = c.cdpessoa ");
		sql.append(" left outer join colaboradorcargo cg on cg.cdpessoa = c.cdpessoa ");
		sql.append(" where upper(so_numero(c.numeroinscricao)) like upper(so_numero( '").append(numeroInscricao).append("')) ");
		sql.append(" order by cg.dtinicio ");

		SinedUtil.markAsReader();
		List<Colaborador> lista = getJdbcTemplate().query(sql.toString(), new RowMapper(){
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Colaborador item = new Colaborador();			
				item.setCdpessoa(rs.getInt("cdpessoa"));
				item.setNome(rs.getString("nome"));
				return item;
			}
		});
		
		if(lista!=null && lista.size()>0)
			return lista.get(0);
		
		return null;
	}
	
	/**
	 * Executa o update no registro de colaborador.
	 *
	 * @param pessoa (Boolean para indicar se � na tabela PESSOA ou COLABORADOR)
	 * @param campo (Nome do campo para ser atualizado)
	 * @param valor (Valor do campo atualizado)
	 * @param colaborador (Colaborador a ser atualizado)
	 * @since Jul 6, 2011
	 * @author Rodrigo Freitas
	 * @throws UnsupportedEncodingException 
	 */
	public void updateCampoColaborador(boolean pessoa, String campo, Object valor, Colaborador colaborador) throws UnsupportedEncodingException {
		if(campo == null || campo.equals("") || valor == null || colaborador == null || colaborador.getCdpessoa() == null){
			throw new SinedException("Par�metros inv�lidos.");
		}
		
		if(valor instanceof String){
			valor = new String(valor.toString().getBytes(), "LATIN1");
		}
		
		if(pessoa){
			getJdbcTemplate().update("UPDATE PESSOA SET " + campo + " = ? WHERE CDPESSOA = ?", new Object[]{valor, colaborador.getCdpessoa()});
		} else {
			getJdbcTemplate().update("UPDATE COLABORADOR SET " + campo + " = ? WHERE CDPESSOA = ?", new Object[]{valor, colaborador.getCdpessoa()});
		}
	}

	/**
	* M�todo para buscar lista de colaborador alocado na tarefa
	*
	* @param tarefa
	* @return
	* @since Aug 12, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Colaborador> findByColaboradorTarefa(Tarefa tarefa) {
		if(tarefa == null || tarefa.getCdtarefa() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		QueryBuilder<Colaborador> query = query()
				.select("colaborador.cdpessoa, colaborador.nome")
				.where("colaborador.cdpessoa in (select c.cdpessoa from Tarefacolaborador tc join tc.colaborador c join tc.tarefa t where t.cdtarefa = " + tarefa.getCdtarefa() + ")")
				.orderBy("colaborador.nome");
		
		return query.list();
	}	
	
	/**
	 * M�todo respons�vel por retornar a listagem dos colaboradores com seus respectivos salarios atuais. 
	 * 
	 * @author Filipe Santos
	 * @param filtro
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Colaboradorholerite> findByHolerite(HoleriteFiltro filtro){
		if (filtro == null || filtro.getDtreferencia1() == null || filtro.getDtreferencia2() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		StringBuilder sql = new StringBuilder();
		
		sql.append(" select c.cdpessoa as codigo, p.nome as nome , cc.salario as salario, c.dtadmissao ");
		sql.append(" from colaborador c ");
		sql.append(" join colaboradorcargo cc on cc.cdpessoa = c.cdpessoa ");
		sql.append(" join pessoa p on p.cdpessoa = c.cdpessoa ");
		if (filtro.getEmpresa()!=null){
			sql.append(" join empresa e on e.cdpessoa = cc.cdempresa ");
		}
		if (filtro.getCargo()!=null){
			sql.append(" join cargo cg on cg.cdcargo = cc.cdcargo ");						
		}
		sql.append(" where cc.dtfim is null ");
		sql.append(" and cc.salario is not null ");
		sql.append(" and cc.salario > 0 ");
		if(filtro.getCargo()!=null){
			sql.append(" and cg.cdcargo = "+filtro.getCargo().getCdcargo()+" ");
		}
		if (filtro.getEmpresa()!=null){
			sql.append(" and e.cdpessoa = "+filtro.getEmpresa().getCdpessoa()+" ");
		}		
		if (filtro.getColaborador() != null) {
			sql.append(" and c.cdpessoa = " + filtro.getColaborador().getCdpessoa() + " ");
		}
		if(filtro.getProjeto() != null){
			sql.append(" and exists(select * from apontamento ap join planejamento pl on pl.cdplanejamento = ap.cdplanejamento where ap.cdcolaborador = c.cdpessoa "); 
			sql.append(" and pl.cdprojeto = " + filtro.getProjeto().getCdprojeto() + " "); 
			sql.append(" and ap.dtapontamento >= '" + SinedDateUtils.toStringPostgre(filtro.getDtreferencia1())+"' "); 
			sql.append(" and ap.dtapontamento <= '" + SinedDateUtils.toStringPostgre(filtro.getDtreferencia2())+"' "); 
			sql.append(" ) "); 
		}
		sql.append(" order by p.nome ");

		SinedUtil.markAsReader();
		return getJdbcTemplate().query(sql.toString(), new RowMapper(){
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Colaboradorholerite item = new Colaboradorholerite();			
				item.setNome(rs.getString("nome"));
				item.setDtadmissaoColaborador(rs.getDate("dtadmissao"));
				item.setSalarioColaborador(rs.getDouble("salario"));
				item.setCdpessoa(rs.getInt("codigo"));
				return item;
			}
		});
	}
	
	/**
	 * 
	 * M�todo para buscar todos os colaboradores com refer�ncia na tabela oportunidade
	 *
	 *@author Thiago Augusto
	 *@date 02/02/2012
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Colaborador> findListColaboradorOportunidade(){
		StringBuilder sql = new StringBuilder();
		sql.append("select DISTINCT(c.cdpessoa) as CDPESSOA, p.nome as NOME from colaborador c join oportunidade o ");
		sql.append("	on c.cdpessoa = o.cdresponsavel");
		sql.append("	join pessoa p on c.cdpessoa = p.cdpessoa");
		SinedUtil.markAsReader();
		List<Colaborador> lista = getJdbcTemplate().query(sql.toString(),
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						Colaborador colaborador = new Colaborador(rs
								.getInt("CDPESSOA"), rs.getString("NOME"));
						return colaborador;
					}
				});
		return lista;
	}

	/**
	 * M�todo que verifica se existe colaborador em alguma tarefa
	 * 
	 * @param tarefa
	 * @return
	 */
	public Boolean isColaboradorTarefa(Tarefa tarefa, Colaborador colaborador) {
		if(tarefa == null || tarefa.getCdtarefa() == null || colaborador == null || colaborador.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Colaborador> query = query()
				.select("colaborador.cdpessoa, colaborador.nome")
				.where("colaborador.cdpessoa in (select c.cdpessoa from Tarefacolaborador tc join tc.colaborador c join tc.tarefa t where t.cdtarefa = " + tarefa.getCdtarefa() + " and c.cdpessoa = " + colaborador.getCdpessoa() + ")");
				
		return query.list().size() > 0;
	}

	/**
	 * M�todo que verifica se existe colaborador tem o cargo
	 * 
	 * @param cargo
	 * @param colaborador
	 * @return
	 */
	public Boolean isColaboradorCargo(Cargo cargo, Colaborador colaborador) {
		if(cargo == null || cargo.getCdcargo() == null || colaborador == null || colaborador.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		return query()
				.select("colaborador.cdpessoa,colaborador.nome")
				.where("colaborador.cdpessoa in (select c.cdpessoa from Colaboradorcargo cc join cc.colaborador c join cc.cargo cg where cg.cdcargo = " + cargo.getCdcargo() + "and cc.dtfim is null and c.cdpessoa = " + colaborador.getCdpessoa() + ")")
				.list().size() > 0;
	}

	/**
     * Busca os dados para o cache da tela de pedido de venda offline
     */
	public List<Colaborador> findForPVOffline() {
		return query()
		.select("colaborador.cdpessoa, colaborador.nome")
		.list();
	}
	
	public List<Colaborador> findForAndroid(String whereIn) {
		return query()
		.select("colaborador.cdpessoa, colaborador.nome, listaTelefone.telefone, telefonetipo.cdtelefonetipo, telefonetipo.nome ")
		.leftOuterJoin("colaborador.listaTelefone listaTelefone")
		.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
		.whereIn("colaborador.cdpessoa", whereIn)
		.list();
	}
	
	/**
	 * Atualiza as informa��es do cargo do colaborador
	 *
	 * @param colaboradorcargo
	 * @param salario
	 * @param sindicato
	 * @param empresa
	 * @since 25/04/2012
	 * @author Rodrigo Freitas
	 */
	public void updateSindicatoEmpresaSalario(Colaboradorcargo colaboradorcargo, Money salario, Sindicato sindicato, Empresa empresa) {
		
		if(salario != null | sindicato != null || empresa != null){
			List<Object> listaParam = new ArrayList<Object>();
			
			StringBuilder sb = new StringBuilder();
			sb.append("update Colaboradorcargo c set ");
			
			if(salario != null){
				sb.append(" c.salario = ? ");
				listaParam.add(salario);
			} else sb.append(" c.salario = null ");
			
			sb.append(" , ");
			
			if(sindicato != null){
				sb.append(" c.sindicato = ? ");
				listaParam.add(sindicato);
			} else sb.append(" c.sindicato = null ");
			
			sb.append(" , ");
			
			if(empresa != null){
				sb.append(" c.empresa = ? ");
				listaParam.add(empresa);
			} else sb.append(" c.empresa = null ");
			
			sb.append(" where c.id = ? ");
			listaParam.add(colaboradorcargo.getCdcolaboradorcargo());
			
			getHibernateTemplate().bulkUpdate(sb.toString(), listaParam.toArray());
		}
		
	}
	
	/**
	 * M�todo que retorna lista de colaboradores que realizaram vendas
	 * @return
	 * @author Rafael Salvio
	 * @param colaborador 
	 */
	public List<Colaborador> findColaboradoresComVenda(Empresa empresa, Colaborador colaborador, boolean ativo){
		QueryBuilder<Colaborador> query = query()
				.select("colaborador.cdpessoa, colaborador.nome")
				.where("colaborador = ?", colaborador)
				.where("colaborador.colaboradorsituacao <> ?", new Colaboradorsituacao(Colaboradorsituacao.DEMITIDO), ativo)
				.orderBy("colaborador.nome");
		
		if(empresa != null && empresa.getCdpessoa() != null){
			query.where("exists ( SELECT v.cdvenda " +
					" FROM Venda v " +
					" JOIN v.empresa e" +
					" WHERE v.colaborador = colaborador " +
					" AND e.cdpessoa = ? ) ",empresa.getCdpessoa());
		} else {
			query.where("exists ( SELECT v.cdvenda " +
					" FROM Venda v " +
					" JOIN v.empresa e" +
					" WHERE v.colaborador = colaborador ) ");
		}
		
		return query.list();
	}
	
	public List<Colaborador> findWithVenda(Empresa empresa){
		
		return query()
				.select("colaborador.cdpessoa, colaborador.nome")
				.where("exists ( SELECT v.cdvenda " +
								" FROM Venda v " +
								(empresa != null && empresa.getCdpessoa() != null ? " JOIN v.empresa e" : "") + 
								" WHERE v.colaborador = colaborador " +
								(empresa != null && empresa.getCdpessoa() != null ? " AND e.cdpessoa = " + empresa.getCdpessoa() : "") +
								" )" )
				.orderBy("colaborador.nome")
				.list();
	}

	/**
	 * M�todo que busca os colaboradores, caso o usu�rio tenha restri��o, exibe apenas o pr�prio usu�rio 
	 *
	 * @param s
	 * @param isRestricaoVendaVendedor
	 * @return
	 * @author Luiz Fernando
	 * @param isRestricaoClienteVendedor 
	 */
	public List<Colaborador> findColaboradoresAutocompleteVenda(String s, Boolean isRestricaoVendaVendedor, Boolean isRestricaoClienteVendedor) {
		Colaborador colaborador = null;
		if(isRestricaoVendaVendedor != null && isRestricaoVendaVendedor){
			if(isRestricaoClienteVendedor == null || !isRestricaoClienteVendedor)
				colaborador = SinedUtil.getUsuarioComoColaborador();
		}
		
		return query()
			.select("colaborador.cdpessoa, colaborador.nome")
			.whereLikeIgnoreAll("colaborador.nome", s)
			.where("colaborador = ?", colaborador)
			.orderBy("colaborador.nome")
			.list();
	}
	
	public List<Colaborador> findColaboradoresVenda(String s, Boolean isRestricaoVendaVendedor, Boolean isRestricaoClienteVendedor) {
		Colaborador colaborador = null;
		if(isRestricaoVendaVendedor != null && isRestricaoVendaVendedor){
			if(isRestricaoClienteVendedor == null || !isRestricaoClienteVendedor)
				colaborador = SinedUtil.getUsuarioComoColaborador();
		}
		
		return query()
			.select("colaborador.cdpessoa, colaborador.nome")
			.leftOuterJoin("colaborador.colaboradorsituacao colaboradorsituacao")
			.whereLikeIgnoreAll("colaborador.nome", s)
			.where("colaborador = ?", colaborador)
			.where("colaboradorsituacao.cdcolaboradorsituacao <> ?", Colaboradorsituacao.DEMITIDO)
			.orderBy("colaborador.nome")
			.list();
	}

	/**
	 * Carrega o Colaborador com a conta gerencial
	 *
	 * @param pessoa
	 * @return
	 * @author Luiz Fernando
	 */
	public Colaborador loadForMovimentacaocontabil(Pessoa pessoa) {
		if(pessoa == null || pessoa.getCdpessoa() == null)
			throw new SinedException("Pessoa n�o pode ser nula.");
		
		return query()
				.select("colaborador.cdpessoa, contacontabil.cdcontacontabil")
				.leftOuterJoin("colaborador.contaContabil contacontabil")
				.where("colaborador.cdpessoa = ?", pessoa.getCdpessoa())
				.unique();
	}

	public List<Colaborador> findForIntegracaoEmporium() {
		return querySined()
					.setUseWhereEmpresa(false)
					.select("colaborador.cdpessoa, colaborador.nome, colaborador.codigoemporium")
					.where("colaborador.codigoemporium is not null")
					.where("colaborador.codigoemporium <> ''")
					.list();
	}

	/**
	 * M�todo que carrega o colaborador com a lista de cargo
	 *
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 * @since 23/01/2014
	 */
	public Colaborador loadWithListaCargo(Colaborador colaborador) {
		if(colaborador == null || colaborador.getCdpessoa() == null)
			throw new SinedException("Colaborador n�o pode ser nulo.");
		
		return query()
			.select("colaborador.cdpessoa, colaborador.nome, listaColaboradorcargo.cdcolaboradorcargo, listaColaboradorcargo.dtfim, cargo.cdcargo")
			.leftOuterJoin("colaborador.listaColaboradorcargo listaColaboradorcargo")
			.leftOuterJoin("listaColaboradorcargo.cargo cargo")
			.where("colaborador = ?", colaborador)
			.unique();
	}

	/**
	 * 
	 * @return
	 */
	public List<Colaborador> findAllDestinatarios() {
		return query()
			.select("colaborador.cdpessoa, colaborador.nome")
			.join("colaborador.listaComunicadodestinatario listaComunicadodestinatario")
			.leftOuterJoin("colaborador.colaboradorsituacao colaboradorsituacao")
			.where("colaboradorsituacao.cdcolaboradorsituacao <> ?", Colaboradorsituacao.DEMITIDO)
			.list();
	}
	
	public List<Colaborador> findForAndroid() {
		return query()
		.select("colaborador.cdpessoa, colaborador.nome")
		.list();
	}

	public boolean isColaborador(Integer cdpessoa) {
		return newQueryBuilderWithFrom(Long.class)
		.select("count(*)")
		.where("colaborador.cdpessoa=?", cdpessoa)
		.unique()>0;
	}
	
	public List<Colaborador> carregaColaboradorAtivo() {
		return query()
			.select("colaborador.cdpessoa, colaborador.nome")
			.leftOuterJoin("colaborador.colaboradorsituacao colaboradorsituacao")
			.where("colaboradorsituacao.cdcolaboradorsituacao <> ?", Colaboradorsituacao.DEMITIDO)
			.orderBy("colaborador.nome")
			.list();
	}

	/**
	 * Busca infos para o WS SOAP
	 *
	 * @return
	 * @param cargo 
	 * @param dataReferencia 
	 * @since 26/04/2016
	 */
	public List<Colaborador> findForWSSOAP(Timestamp dataReferencia, Integer cargo) {
		QueryBuilder<Colaborador> query = query()
					.select("colaborador.cdpessoa, colaborador.nome")
					.where("colaborador.dtaltera >= ?", dataReferencia)
					.orderBy("colaborador.nome");
		
		if(cargo != null){
			query.where("colaborador.cdpessoa in (select c.cdpessoa from Colaboradorcargo cc join cc.colaborador c join cc.cargo cg where cg.cdcargo = " + cargo + "and cc.dtfim is null)");
		}
		
		return query.list();
	}
	
	public Colaborador findByTipoAtividade(Atividadetipo tipo){
		QueryBuilder<Colaborador> query = query()
		.select("colaborador.cdpessoa, colaborador.nome")
		.from(Colaborador.class, "colaborador")
		.where("colaborador.cdpessoa in (select c.cdpessoa from Atividadetipo ativ join ativ.colaborador c where ativ.cdatividadetipo = " + tipo.getCdatividadetipo() + ")");

		return query.list().iterator().next();
	}
	
	public List<Colaborador> findForImportacaoFolhaPagamento(List<Cpf> listaCpf){
		QueryBuilderSined<Colaborador> query = querySined().setUseWhereEmpresa(false);
		query.select("colaborador.cdpessoa, colaborador.cpf, listaColaboradorcargo.dtinicio, contagerencial.cdcontagerencial");
		query.join("colaborador.listaColaboradorcargo listaColaboradorcargo");
		query.join("listaColaboradorcargo.cargo cargo");
		query.leftOuterJoin("cargo.contagerencial contagerencial");
		query.where("listaColaboradorcargo.dtfim is null");
		
		query.openParentheses();
		for (Cpf cpf: listaCpf){
			query.where("colaborador.cpf=?", cpf);
			query.or();
		}
		query.closeParentheses();
		
		query.orderBy("colaborador.cdpessoa, listaColaboradorcargo.dtinicio desc");
		
		return query.list();
	}

	public List<Colaborador> findColaboradorAutocompleteByEmpresaCargo(String query, Empresa empresa, Cargo cargo) {
		return query()
				.select("colaborador.cdpessoa, colaborador.nome")
				.join("colaborador.listaColaboradorcargo cc")
				.join("cc.empresa empresa")
				.where("cc.cargo = ?", cargo)
				.where("empresa = ?", empresa)
				.whereLikeIgnoreAll("colaborador.nome", query)
				.list();
	}

	public List<Colaborador> findColaboradorAutoCompleteByMaterialEmpresa(String query, Empresa empresa, Material material) {
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		QueryBuilder<Colaborador> q = query();
		q.select("colaborador.cdpessoa, colaborador.nome")
		.leftOuterJoin("colaborador.listaColaboradorcargo cc")
		.leftOuterJoin("colaborador.listaMaterialColaborador mc");
		if(usuarioLogado != null && usuarioLogado.getRestricaoclientevendedor() != null && usuarioLogado.getRestricaoclientevendedor())
			q.where("colaborador.cdpessoa = ?", usuarioLogado.getCdpessoa());
		return q.where("cc.dtfim is null")
		.openParentheses()
			.where("cc is null").or()
			.where("cc.empresa = ?", empresa)
		.closeParentheses()
		.openParentheses()
			.where("mc is null").or()
			.where("mc.material = ?", material)
		.closeParentheses()
		.whereLikeIgnoreAll("colaborador.nome", query).orderBy("colaborador.nome").list();
	}

	public Colaborador getUniqueColaborador(Colaborador colaborador, Material material, Empresa empresa) {
		return query().select("colaborador.cdpessoa")
		.leftOuterJoin("colaborador.listaColaboradorcargo cc")
		.leftOuterJoin("colaborador.listaMaterialColaborador mc")
		.where("colaborador = ?", colaborador)
		.where("cc.dtfim is null")
		.openParentheses()
			.where("cc is null").or()
			.where("cc.empresa = ?", empresa)
		.closeParentheses()
		.openParentheses()
			.where("mc is null").or()
			.where("mc.material = ?", material)
		.closeParentheses()
		.unique();
	}
}