package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.FechamentoFinanceiroHistorico;
import br.com.linkcom.sined.geral.bean.Fechamentofinanceiro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FechamentoFinanceiroHistoricoDAO extends GenericDAO<FechamentoFinanceiroHistorico>{

	/**
	 * M�todo que busca o historico para a tela de fechamento financeiro
	 * @param fechamentoFinanceiro
	 * @return
	 */
	public List<FechamentoFinanceiroHistorico> findHistoricoByFechamentoFinanceiro(Fechamentofinanceiro fechamentoFinanceiro) {
		return query()
		.select("fechamentoFinanceiroHistorico.cdfechamentofinanceirohistorico, fechamentoFinanceiroHistorico.observacao, fechamentoFinanceiroHistorico.cdusuarioaltera," +
				"fechamentoFinanceiroHistorico.dtaltera")
		.join("fechamentoFinanceiroHistorico.fechamentofinanceiro fechamentofinanceiro")
		.where("fechamentofinanceiro = ?", fechamentoFinanceiro)
		.orderBy("fechamentoFinanceiroHistorico.cdfechamentofinanceirohistorico desc")
		.list();
	}

}
