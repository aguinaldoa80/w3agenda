package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Codigoanp;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("codigoanp.codigo")
public class CodigoanpDAO extends GenericDAO<Codigoanp>{
	
}
