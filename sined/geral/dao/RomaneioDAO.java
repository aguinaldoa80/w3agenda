package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.bean.Romaneiosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Romaneiotipo;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.RomaneioFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RomaneioDAO extends GenericDAO<Romaneio> {
	
	private MaterialcategoriaService materialcategoriaService;
	
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {this.materialcategoriaService = materialcategoriaService;}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Romaneio> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		RomaneioFiltro filtro = (RomaneioFiltro) _filtro;
		
		query
			.select("distinct new br.com.linkcom.sined.geral.bean.Romaneio(" +
					"romaneio.cdromaneio, " +
					"romaneio.descricao, " +
					"localarmazenagemorigem.nome, " +
					"localarmazenagemdestino.nome, " +
					"romaneiosituacao.descricao, " +
					"romaneiosituacao.cdromaneiosituacao, " +
					"empresa.nome, " +
					"empresa.razaosocial, " +
					"empresa.nomefantasia, " +
					"romaneio.dtromaneio, " +
					"(select count(nr.cdnotaromaneio) from Notaromaneio nr join nr.nota n where nr.romaneio = romaneio and n.notaStatus.cdNotaStatus <> 3)" +
					")")
			.setUseTranslator(false)
			.leftOuterJoin("romaneio.localarmazenagemorigem localarmazenagemorigem")
			.leftOuterJoin("romaneio.localarmazenagemdestino localarmazenagemdestino")
			.leftOuterJoin("romaneio.romaneiosituacao romaneiosituacao")
			.leftOuterJoin("romaneio.empresa empresa")
			.where("localarmazenagemorigem = ?", filtro.getLocalorigem())
			.where("localarmazenagemdestino = ?", filtro.getLocaldestino())
			.whereLikeIgnoreAll("romaneio.descricao", filtro.getDescricao())
			.where("empresa = ?", filtro.getEmpresa())
			.where("romaneio.placa = ?", filtro.getPlaca())
			.wherePeriodo("romaneio.dtromaneio", filtro.getDtRomaneioInicio(), filtro.getDtRomaneioFim())
			.ignoreJoin("listaRomaneioitem", "material", "materialcategoria", "vmaterialcategoria", "listaRomaneioorigem", "contrato", "cliente", "contratofechamento", "clientecontratofechamento");
		
		if(filtro.getMaterial() != null || filtro.getMaterialcategoria() != null || Util.strings.isNotEmpty(filtro.getPlaquetaSerie())){
			
			query.leftOuterJoin("romaneio.listaRomaneioitem listaRomaneioitem");
			
			if(filtro.getMaterial() != null){
				query.where("romaneio.cdromaneio in (SELECT romaneio2.cdromaneio " +
						"FROM Romaneio romaneio2 join romaneio2.listaRomaneioitem item2 " +
						"join item2.material material2 " +
						"WHERE material2.cdmaterial = ?)", filtro.getMaterial().getCdmaterial());
			}
			
			if(filtro.getMaterialcategoria() != null){
				materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
				query
					.leftOuterJoin("listaRomaneioitem.material material")
					.leftOuterJoin("material.materialcategoria materialcategoria")
					.leftOuterJoin("materialcategoria.vmaterialcategoria vmaterialcategoria")
					.openParentheses()
						.where("vmaterialcategoria.identificador like ? ||'.%'", filtro.getMaterialcategoria().getIdentificador())
						.or()
						.where("vmaterialcategoria.identificador like ? ", filtro.getMaterialcategoria().getIdentificador())
					.closeParentheses();
			}
			
			if(Util.strings.isNotEmpty(filtro.getPlaquetaSerie())){
				query.join("listaRomaneioitem.patrimonioitem patrimonioitem");
				query.leftOuterJoin("patrimonioitem.materialnumeroserie materialnumeroserie");
				query
					.openParentheses()
						.whereLikeIgnoreAll("patrimonioitem.plaqueta", filtro.getPlaquetaSerie())
						.or()
						.whereLikeIgnoreAll("materialnumeroserie.numero", filtro.getPlaquetaSerie())
					.closeParentheses();
			}
		}
	
		if(filtro.getContrato() != null || filtro.getCliente() != null){
			query.leftOuterJoin("romaneio.listaRomaneioorigem listaRomaneioorigem");

			if(filtro.getContrato() != null){
				query
				.openParentheses()
					.where("listaRomaneioorigem.contrato = ?", filtro.getContrato())
					.or()
					.where("listaRomaneioorigem.contratofechamento = ?", filtro.getContrato())
				.closeParentheses();
			}
			
			if(filtro.getCliente() != null){
				query
					.leftOuterJoin("listaRomaneioorigem.contratofechamento contratofechamento")
					.leftOuterJoin("listaRomaneioorigem.contrato contrato");
				
				query
				.openParentheses()
					.where("contrato.cliente = ?", filtro.getCliente())
						.or()
					.where("contratofechamento.cliente = ?", filtro.getCliente())
						.or()
					.where("romaneio.cliente = ?", filtro.getCliente())
				.closeParentheses();
			}
		}
	
		if(filtro.getMostrar() != null){
			if("Remessa de Contrato".equals(filtro.getMostrar())){
				query.where("romaneio.romaneiotipo = ?", Romaneiotipo.SAIDA);
			}else if("Devolu��o de Contrato".equals(filtro.getMostrar())){
				query.where("romaneio.romaneiotipo = ?", Romaneiotipo.ENTRADA);
			}else if("Outros".equals(filtro.getMostrar())){
				query.where("romaneio.romaneiotipo is null");
			} else if("Simples Remessa Gerada".equals(filtro.getMostrar())){
				query.where("exists(select 1 from Notaromaneio nr join nr.nota n where nr.romaneio = romaneio and n.notaStatus.cdNotaStatus <> 3)");
			} else if("Sem Simples Remessa".equals(filtro.getMostrar())){
				query.where("not exists(select 1 from Notaromaneio nr join nr.nota n where nr.romaneio = romaneio and n.notaStatus.cdNotaStatus <> 3)");
			}
		}
		
		if(filtro.getListasituacoes() != null){
			query.whereIn("romaneiosituacao", CollectionsUtil.listAndConcatenate(filtro.getListasituacoes(), "cdromaneiosituacao",","));
		}
		
		if(!StringUtils.isEmpty(filtro.getCodigo())){
			String codigo = SinedUtil.montaWhereInInteger(filtro.getCodigo());
			if(codigo != null){
				query.whereIn("romaneio.cdromaneio", codigo);		
			}else{
				query.where("romaneio.cdromaneio is null");
			}
		}

		query.orderBy("romaneio.cdromaneio desc");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Romaneio> query) {
		query
			.select("romaneio.cdromaneio, romaneio.descricao, romaneio.dtaltera, romaneio.cdusuarioaltera, romaneio.romaneiotipo, centroCusto.cdcentrocusto, " +
					"localarmazenagemorigem.cdlocalarmazenagem, localarmazenagemorigem.nome, " +
					"localarmazenagemdestino.cdlocalarmazenagem, localarmazenagemdestino.nome, " +
					"romaneioitem.cdromaneioitem, romaneioitem.qtde, romaneio.dtromaneio,unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
					"materialclasse.cdmaterialclasse, materialclasse.nome, " +
					"material.cdmaterial, material.nome, material.codigobarras, material.extipi, material.ncmcompleto, material.valorcusto, " +
					"material.identificacao, material.valorindenizacao, " +
					"ncmcapitulo.cdncmcapitulo, " +
					"romaneiosituacao.cdromaneiosituacao, romaneiosituacao.descricao, " +
					"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.inscricaoestadual, " +
					"logomarca.cdarquivo, " +
					"loteestoque.cdloteestoque, loteestoque.numero, " +
					"romaneioorigem.estoque, entrega.cdentrega, contrato.cdcontrato, contrato.identificador, " +
					"contratofechamento.cdcontrato, contratofechamento.identificador, " +
					"cliente.cdpessoa, cliente.nome, romaneio.placa, romaneio.motorista, " +
					"patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, clienteRomaneio.cdpessoa, clienteRomaneio.identificador, clienteRomaneio.nome, clienteRomaneio.cpf, clienteRomaneio.cnpj, " +
					"endereco.cdendereco, projeto.cdprojeto, tarefa.cdtarefa, requisicaomaterial.cdrequisicaomaterial, requisicaomaterial.identificador, " +
					"romaneio.solicitante, romaneio.conferente, romaneio.qtdeajudantes, " +
					"romaneiosubstituido.cdromaneio, romaneioitemsubstituido.cdromaneioitem, listaMaterialnumeroserie.numero")
			.join("romaneio.localarmazenagemorigem localarmazenagemorigem")
			.join("romaneio.localarmazenagemdestino localarmazenagemdestino")
			.join("romaneio.romaneiosituacao romaneiosituacao")
			.join("romaneio.listaRomaneioitem romaneioitem")
			.join("romaneioitem.materialclasse materialclasse")
			.join("romaneioitem.material material")
			.join("material.unidademedida unidademedida")
			.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
			.leftOuterJoin("material.listaMaterialnumeroserie listaMaterialnumeroserie")
			.leftOuterJoin("romaneioitem.patrimonioitem patrimonioitem")
			.leftOuterJoin("romaneio.listaRomaneioorigem romaneioorigem")
			.leftOuterJoin("romaneioorigem.requisicaomaterial requisicaomaterial")
			.leftOuterJoin("romaneioorigem.tarefa tarefa")
			.leftOuterJoin("romaneioorigem.entrega entrega")
			.leftOuterJoin("romaneioorigem.contrato contrato")
			.leftOuterJoin("contrato.cliente cliente")
			.leftOuterJoin("romaneioorigem.contratofechamento contratofechamento")
			.leftOuterJoin("romaneio.empresa empresa")
			.leftOuterJoin("empresa.logomarca logomarca")
			.leftOuterJoin("romaneioitem.loteestoque loteestoque")
			.leftOuterJoin("romaneio.cliente clienteRomaneio")
			.leftOuterJoin("romaneio.endereco endereco")
			.leftOuterJoin("romaneio.projeto projeto")
			.leftOuterJoin("romaneio.romaneiosubstituido romaneiosubstituido")
			.leftOuterJoin("romaneioitem.romaneioitemsubstituido romaneioitemsubstituido")
			.leftOuterJoin("romaneio.centroCusto centroCusto");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaRomaneioitem");
	}

	/**
	 * M�todo que busca os possiveis romaneios para as entregas. Leva em considera��o o local de oridem e os destinos (local da solicita��o de compra)
	 * 
	 * @param localarmazenagemorigem
	 * @param whereInDestino
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Romaneio> getRomaneiosPossiveisParaEntregas(Localarmazenagem localarmazenagemorigem, String whereInDestino) {
		return query()
		.select("romaneio.cdromaneio, romaneio.descricao")
		.join("romaneio.localarmazenagemorigem localarmazenagemorigem")
		.join("romaneio.localarmazenagemdestino localarmazenagemdestino")
		.join("romaneio.romaneiosituacao romaneiosituacao")
		.where("localarmazenagemorigem = ?", localarmazenagemorigem)
		.whereIn("localarmazenagemdestino.cdlocalarmazenagem", whereInDestino)
		.where("romaneiosituacao = ?", Romaneiosituacao.EM_ABERTO)
		.list();
	}

	/**
	 * M�todo que atualiza a situa��o dos romaneios
	 * 
	 * @param whereInRomaneios
	 * @param romaneioSituacao
	 * @author Tom�s Rabelo
	 */
	public void doUpdateSituacaoRomaneio(String whereInRomaneios, Romaneiosituacao romaneioSituacao) {
		if(whereInRomaneios == null || whereInRomaneios.equals("")){
			throw new SinedException("Nenhum romaneio selecionado.");
		}
		
		Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		
		getHibernateTemplate().bulkUpdate(
				"update Romaneio r set r.cdusuarioaltera = ?, r.dtaltera = ?, r.romaneiosituacao = ? where r.cdromaneio in ("+whereInRomaneios+")"
				, new Object[]{cdusuarioaltera,hoje,romaneioSituacao});
	}
	
	/**
	 * M�todo que busca a lista de romaneios da entrega
	 * 
	 * @param form
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Romaneio> getListaRomaneioDaEntrega(Entrega entrega) {
		return query() 
			.select("romaneio.cdromaneio")
			.join("romaneio.listaRomaneioorigem romaneioorigem")
			.join("romaneioorigem.entrega entrega")
			.where("entrega = ?", entrega)
			.where("romaneio.romaneiosituacao <> ?", Romaneiosituacao.CANCELADA)
			.list();
	}

	

	/**
	 * M�todo que busca os romaneios para a baixa.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/12/2012
	 */
	public List<Romaneio> findForBaixar(String whereIn) {
		return query()
					.select("romaneio.cdromaneio, romaneiosituacao.cdromaneiosituacao, " +
							"empresa.cdpessoa, localarmazenagemorigem.cdlocalarmazenagem, " +
							"localarmazenagemdestino.cdlocalarmazenagem, " +
							"listaRomaneioitem.cdromaneioitem, listaRomaneioitem.qtde, " +
							"material.cdmaterial, material.nome, material.valorcusto, " +
							"patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, " +
							"materialclasse.cdmaterialclasse, " +
							"loteestoque.cdloteestoque, projeto.cdprojeto,projeto.nome," +
							"cliente.cdpessoa," +
							"materialRateioEstoque.cdMaterialRateioEstoque," +
							"contaGerencialRomaneioEntrada.cdcontagerencial,contaGerencialRomaneioEntrada.nome," +
							"contaGerencialRomaneioSaida.cdcontagerencial,contaGerencialRomaneioSaida.nome," +
							"centroCusto.cdcentrocusto,centroCusto.nome")
					.leftOuterJoin("romaneio.romaneiosituacao romaneiosituacao")	
					.leftOuterJoin("romaneio.empresa empresa")	
					.leftOuterJoin("romaneio.cliente cliente")
					.leftOuterJoin("romaneio.localarmazenagemorigem localarmazenagemorigem")
					.leftOuterJoin("romaneio.centroCusto centroCusto")
					.leftOuterJoin("romaneio.localarmazenagemdestino localarmazenagemdestino")	
					.leftOuterJoin("romaneio.listaRomaneioitem listaRomaneioitem")
					.leftOuterJoin("romaneio.projeto projeto")
					.leftOuterJoin("listaRomaneioitem.material material")
					.leftOuterJoin("material.materialRateioEstoque materialRateioEstoque")
					.leftOuterJoin("materialRateioEstoque.contaGerencialRomaneioEntrada contaGerencialRomaneioEntrada")
					.leftOuterJoin("materialRateioEstoque.contaGerencialRomaneioSaida contaGerencialRomaneioSaida")
					.leftOuterJoin("listaRomaneioitem.loteestoque loteestoque")	
					.leftOuterJoin("listaRomaneioitem.patrimonioitem patrimonioitem")	
					.leftOuterJoin("listaRomaneioitem.materialclasse materialclasse")	
					.whereIn("romaneio.cdromaneio", whereIn)
					.list();
	}
	
	/**
	 * M�todo que busca os romaneios para gera��o de nota
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/12/2012
	 */
	public List<Romaneio> findForGerarNota(String whereIn) {
		return query()
					.select("romaneio.cdromaneio, empresa.cdpessoa, endereco.cdendereco, cliente.cdpessoa, contrato.cdcontrato, contratofechamento.cdcontrato, " +
							"clientecontrato.cdpessoa, listaRomaneioitem.cdromaneioitem, listaRomaneioitem.qtde, material.cdmaterial, material.nome, materialclasse.cdmaterialclasse, " +
							"material.identificacao, unidademedida.cdunidademedida, material.valorindenizacao, material.valorcusto, ncmcapitulo.cdncmcapitulo, " +
							"material.ncmcompleto, material.extipi, material.codigobarras, material.peso, material.pesobruto, materialgrupo.cdmaterialgrupo, material.nve, " +
							"materialgrupo.cdmaterialgrupo, material.servico, material.produto, material.tributacaomunicipal, " +
							"loteestoque.cdloteestoque, loteestoque.validade, loteestoque.numero, " +
							"enderecotipo.cdenderecotipo,  patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta")
					.leftOuterJoin("romaneio.empresa empresa")	
					.leftOuterJoin("romaneio.cliente cliente")	
					.leftOuterJoin("romaneio.endereco endereco")
					.leftOuterJoin("endereco.enderecotipo enderecotipo")
					.leftOuterJoin("romaneio.listaRomaneioorigem listaRomaneioorigem")	
					.leftOuterJoin("listaRomaneioorigem.contrato contrato")
					.leftOuterJoin("listaRomaneioorigem.contratofechamento contratofechamento")
					.leftOuterJoin("contrato.cliente clientecontrato")
					.leftOuterJoin("romaneio.listaRomaneioitem listaRomaneioitem")	
					.leftOuterJoin("listaRomaneioitem.material material")
					.leftOuterJoin("material.unidademedida unidademedida")	
					.leftOuterJoin("material.ncmcapitulo ncmcapitulo")	
					.leftOuterJoin("material.materialgrupo materialgrupo")	
					.leftOuterJoin("listaRomaneioitem.materialclasse materialclasse")
					.leftOuterJoin("listaRomaneioitem.patrimonioitem patrimonioitem")
					.leftOuterJoin("listaRomaneioitem.loteestoque loteestoque")
					.whereIn("romaneio.cdromaneio", whereIn)
					.list();
	}

	/**
	 * Busca os romaneios para a valida��o antes de criar nota.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/12/2012
	 */
	public List<Romaneio> findForEmissaoNotaValidacao(String whereIn) {
		return query()
					.select("romaneio.cdromaneio, romaneio.descricao, localarmazenagemdestino.cdlocalarmazenagem")
					.join("romaneio.localarmazenagemdestino localarmazenagemdestino")
					.whereIn("romaneio.cdromaneio", whereIn)
					.list();
	}

	/**
	 * M�todo que busca os romaneios de origem o contrato.
	 *
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/12/2012
	 */
	public List<Romaneio> findByContrato(Contrato contrato) {
		return findByContratoQuery()
					.where("contrato = ?", contrato)
					.list();
	}
	
	/**
	 * M�todo que busca os romaneios de origem o contrato utilizando whereIn.
	 *
	 * @param whereIn
	 * @return
	 * @author Jo�o Vitor
	 * @since 25/03/2015
	 */
	public List<Romaneio> findForCancelamentoByWhereInContrato(String whereIn) {
		if (whereIn == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
				.select("romaneio.cdromaneio, romaneio.indenizacao, romaneiosituacao.cdromaneiosituacao, romaneio.romaneiotipo, romaneio.dtromaneio, material.cdmaterial, " +
						"contrato.cdcontrato,contrato.dtfim, servico.cdmaterial, listaRomaneioitem.qtde, " +
						"listaContratomaterial.cdcontratomaterial, listaContratomateriallocacao.cdcontratomateriallocacao, " +
						"listaContratomateriallocacao.qtde, servico.cdmaterial, listaContratomateriallocacao.cdromaneio, " +
						"listaContratomateriallocacao.contratomateriallocacaotipo, " +
						"contratofechamento.cdcontrato, contratofechamento.dtfim, servicofechamento.cdmaterial, " +
						"listaContratomaterialfechamento.cdcontratomaterial, listaContratomateriallocacaofechamento.cdcontratomateriallocacao, " +
						"listaContratomateriallocacaofechamento.qtde, listaContratomateriallocacaofechamento.cdromaneio, " +
						"listaContratomateriallocacaofechamento.contratomateriallocacaotipo ")
				.join("romaneio.romaneiosituacao romaneiosituacao")
				.leftOuterJoin("romaneio.listaRomaneioitem listaRomaneioitem")
				.leftOuterJoin("listaRomaneioitem.material material")
				.leftOuterJoin("romaneio.listaRomaneioorigem listaRomaneioorigem")
				.leftOuterJoin("listaRomaneioorigem.contrato contrato")
				.leftOuterJoin("contrato.listaContratomaterial listaContratomaterial")
				.leftOuterJoin("listaContratomaterial.servico servico")
				.leftOuterJoin("listaContratomaterial.listaContratomateriallocacao listaContratomateriallocacao")
				.leftOuterJoin("listaRomaneioorigem.contratofechamento contratofechamento")
				.leftOuterJoin("contratofechamento.listaContratomaterial listaContratomaterialfechamento")
				.leftOuterJoin("listaContratomaterialfechamento.servico servicofechamento")
				.leftOuterJoin("listaContratomaterialfechamento.listaContratomateriallocacao listaContratomateriallocacaofechamento")
				.where("romaneiosituacao.cdromaneiosituacao <> ?", Romaneiosituacao.CANCELADA.getCdromaneiosituacao())
				.whereIn("contrato.cdcontrato", whereIn)
				.list();
	}
	
	/**
	 * M�todo que busca os romaneios de origem o fechamento de contrato.
	 *
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/12/2012
	 */
	public List<Romaneio> findByContratoFechamento(Contrato contrato) {
		return findByContratoQuery()
					.where("contratofechamento = ?", contrato)
					.list();
	}

	/**
	 * Query para findBycontratoCentralizada
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/03/2014
	 */
	private QueryBuilder<Romaneio> findByContratoQuery() {
		return query()
					.select("romaneio.cdromaneio, romaneio.descricao, empresa.cdpessoa, " +
							"localarmazenagemorigem.cdlocalarmazenagem, localarmazenagemorigem.nome, " +
							"localarmazenagemdestino.cdlocalarmazenagem, localarmazenagemdestino.nome, " +
							"listaRomaneioitem.qtde, material.cdmaterial, material.nome, " +
							"contrato.cdcontrato, contratofechamento.cdcontrato, " +
							"materialclasse.cdmaterialclasse, patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta," +
							"romaneiosituacao.cdromaneiosituacao, localarmazenagemorigem.indenizacao, " +
							"localarmazenagemorigem.clientelocacao, localarmazenagemdestino.indenizacao, " +
							"localarmazenagemdestino.clientelocacao")
					.leftOuterJoin("romaneio.empresa empresa")
					.leftOuterJoin("romaneio.localarmazenagemorigem localarmazenagemorigem")
					.leftOuterJoin("romaneio.localarmazenagemdestino localarmazenagemdestino")
					.leftOuterJoin("romaneio.listaRomaneioorigem listaRomaneioorigem")	
					.leftOuterJoin("listaRomaneioorigem.contrato contrato")
					.leftOuterJoin("listaRomaneioorigem.contratofechamento contratofechamento")
					.leftOuterJoin("romaneio.listaRomaneioitem listaRomaneioitem")	
					.leftOuterJoin("listaRomaneioitem.material material")	
					.leftOuterJoin("listaRomaneioitem.materialclasse materialclasse")	
					.leftOuterJoin("listaRomaneioitem.patrimonioitem patrimonioitem")	
					.leftOuterJoin("romaneio.romaneiosituacao romaneiosituacao")	
					.where("romaneio.romaneiosituacao <> ?", Romaneiosituacao.CANCELADA);
	}

	/**
	 * M�todo que busca os romaneios da listagem
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Romaneio> findForReport(RomaneioFiltro filtro) {
		QueryBuilder<Romaneio> query = querySined();
		this.updateListagemQuery(query, filtro);
		
		query.select("romaneio.cdromaneio, romaneio.descricao, romaneio.dtaltera, romaneio.cdusuarioaltera, romaneio.dtromaneio, " +
				"localarmazenagemorigem.cdlocalarmazenagem, localarmazenagemorigem.nome, " +
				"localarmazenagemdestino.cdlocalarmazenagem, localarmazenagemdestino.nome, " +
				"listaRomaneioitem.qtde, unidademedida.simbolo, " +
				"materialclasse.cdmaterialclasse, materialclasse.nome, " +
				"material.cdmaterial, material.nome, material.codigobarras, material.extipi, material.ncmcompleto, " +
				"material.valorcusto, material.identificacao, material.peso, material.pesobruto, " +
				"ncmcapitulo.cdncmcapitulo, " +
				"romaneiosituacao.cdromaneiosituacao, romaneiosituacao.descricao, " +
				"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.inscricaoestadual, " +
				"logomarca.cdarquivo, " +
				"loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, " +
				"listaRomaneioorigem.estoque, entrega.cdentrega, contrato.cdcontrato, contratofechamento.cdcontrato ,patrimonioitem.plaqueta")
			.setUseTranslator(true)
			.leftOuterJoinIfNotExists("romaneio.listaRomaneioorigem listaRomaneioorigem")
			.leftOuterJoinIfNotExists("romaneio.cliente clienteRomaneio")
			.leftOuterJoinIfNotExists("listaRomaneioorigem.contrato contrato")
			.leftOuterJoinIfNotExists("listaRomaneioorigem.contratofechamento contratofechamento")
			.leftOuterJoinIfNotExists("contrato.cliente cliente")
			.leftOuterJoinIfNotExists("contratofechamento.cliente clientecontratofechamento")
			.leftOuterJoinIfNotExists("romaneio.listaRomaneioitem listaRomaneioitem")	
			.joinIfNotExists("listaRomaneioitem.materialclasse materialclasse")
			.joinIfNotExists("listaRomaneioitem.material material")
			.joinIfNotExists("material.unidademedida unidademedida")
			.leftOuterJoinIfNotExists("material.ncmcapitulo ncmcapitulo")
			.leftOuterJoinIfNotExists("listaRomaneioorigem.entrega entrega")
			.leftOuterJoinIfNotExists("empresa.logomarca logomarca")
			.leftOuterJoinIfNotExists("listaRomaneioitem.loteestoque loteestoque")
			.leftOuterJoinIfNotExists("listaRomaneioitem.patrimonioitem patrimonioitem")
			.orderBy("romaneio.dtaltera")
			.setIgnoreJoinPaths(new HashSet<String>());
//			.ignoreJoin("materialclasse", "unidademedida", "ncmcapitulo", "loteestoque", "patrimonioitem");
		
		return query.list();
		
		
	}
	
	/**
	 * M�todo que busca os romaneios para cancelamento
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Romaneio> findForCancelar(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Nenhum romaneio selecionado.");

		return query()
				.select("romaneio.cdromaneio, romaneiosituacao.cdromaneiosituacao, romaneio.romaneiotipo, romaneio.dtromaneio, romaneio.indenizacao, material.cdmaterial, " +
						"listaRomaneioitem.cdromaneioitem, listaRomaneioitem.qtde, " +
						"contrato.cdcontrato, contratofechamento.cdcontrato, " +
						"patrimonioitem.cdpatrimonioitem, requisicaomaterial.cdrequisicaomaterial, " +
						"romaneiosubstituido.cdromaneio, romaneiosituacaosubstituido.cdromaneiosituacao," +
						"romaneioitemsubstituido.cdromaneioitem, materialsubstituido.cdmaterial, patrimonioitemsubstituido.cdpatrimonioitem ")
				.join("romaneio.romaneiosituacao romaneiosituacao")
				.leftOuterJoin("romaneio.listaRomaneioitem listaRomaneioitem")
				.leftOuterJoin("listaRomaneioitem.material material")
				.leftOuterJoin("listaRomaneioitem.patrimonioitem patrimonioitem")
				.leftOuterJoin("romaneio.listaRomaneioorigem listaRomaneioorigem")
				.leftOuterJoin("listaRomaneioorigem.contrato contrato")
				.leftOuterJoin("listaRomaneioorigem.contratofechamento contratofechamento")
				.leftOuterJoin("romaneio.romaneiosubstituido romaneiosubstituido")
				.leftOuterJoin("romaneiosubstituido.romaneiosituacao romaneiosituacaosubstituido")
				.leftOuterJoin("listaRomaneioitem.romaneioitemsubstituido romaneioitemsubstituido")
				.leftOuterJoin("romaneioitemsubstituido.material materialsubstituido")
				.leftOuterJoin("romaneioitemsubstituido.patrimonioitem patrimonioitemsubstituido")
				.leftOuterJoin("listaRomaneioorigem.requisicaomaterial requisicaomaterial")
				.whereIn("romaneio.cdromaneio", whereIn)
				.where("romaneiosituacao.cdromaneiosituacao <> ?", Romaneiosituacao.CANCELADA.getCdromaneiosituacao())
				.list();
	}
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Romaneio> findForImprimirRomaneio(String whereIn){
		return querySined()
				
				.select("romaneio.cdromaneio, romaneio.placa, romaneio.motorista, romaneio.descricao, romaneio.dtromaneio, empresa.cdpessoa, logomarca.cdarquivo, " +
						"romaneio.solicitante, romaneio.conferente, romaneio.qtdeajudantes, " +
						
						"enderecoorigem.cdendereco, enderecoorigem.logradouro, enderecoorigem.numero, enderecoorigem.complemento," +
						"enderecoorigem.bairro, municipioorigem.nome, uforigem.sigla, enderecoorigem.cep, enderecoorigem.pontoreferencia," +
						
						"enderecodestino.cdendereco, enderecodestino.logradouro, enderecodestino.numero, enderecodestino.complemento," +
						"enderecodestino.bairro, municipiodestino.nome, ufdestino.sigla, enderecodestino.cep, enderecodestino.pontoreferencia," +
						
						"enderecodestinoRomaneio.cdendereco, enderecodestinoRomaneio.logradouro, enderecodestinoRomaneio.numero, enderecodestinoRomaneio.complemento," +
						"enderecodestinoRomaneio.bairro, municipiodestinoRomaneio.nome, ufdestinoRomaneio.sigla, enderecodestinoRomaneio.cep, enderecodestinoRomaneio.pontoreferencia," +
						
						"enderecocontrato.cdendereco, enderecocontrato.logradouro, enderecocontrato.numero, enderecocontrato.complemento," +
						"enderecocontrato.bairro, municipiocontrato.nome, ufcontrato.sigla, enderecocontrato.cep, enderecocontrato.pontoreferencia," +
						
						"enderecocontratofechamento.cdendereco, enderecocontratofechamento.logradouro, enderecocontratofechamento.numero, " +
						"enderecocontratofechamento.complemento, enderecocontratofechamento.bairro, municipiocontratofechamento.nome, " +
						"ufcontratofechamento.sigla, enderecocontratofechamento.cep, enderecocontratofechamento.pontoreferencia, " +
						
						"enderecoentregacontrato.cdendereco, enderecoentregacontrato.logradouro, enderecoentregacontrato.numero, enderecoentregacontrato.complemento," +
						"enderecoentregacontrato.bairro, municipioentregacontrato.nome, ufentregacontrato.sigla, enderecoentregacontrato.cep, enderecoentregacontrato.pontoreferencia," +
						
						"enderecoentregacontratofechamento.cdendereco, enderecoentregacontratofechamento.logradouro, enderecoentregacontratofechamento.numero, " +
						"enderecoentregacontratofechamento.complemento, enderecoentregacontratofechamento.bairro, municipioentregacontratofechamento.nome, " +
						"ufentregacontratofechamento.sigla, enderecoentregacontratofechamento.cep, enderecoentregacontratofechamento.pontoreferencia, " +
						
						"contrato.cdcontrato, contrato.descricao, contratofechamento.cdcontrato, contratofechamento.descricao, " +
						"material.identificacao, material.nome, unidademedida.simbolo, loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, " +
						"romaneioitem.qtde, material.peso, material.pesobruto, localarmazenagemorigem.clientelocacao, localarmazenagemorigem.nome, " +
						"localarmazenagemdestino.nome, localarmazenagemdestino.clientelocacao, empresa.nome, empresa.razaosocial, empresa.cnpj, empresa.cpf, empresa.inscricaoestadual, " +
						"patrimonioitem.plaqueta, cliente.cdpessoa, cliente.nome, clientefechamento.cdpessoa, clientefechamento.nome, " +
						"clienteromaneio.cdpessoa, clienteromaneio.nome, listaMaterialnumeroserie.cdmaterialnumeroserie, listaMaterialnumeroserie.numero, " +
						"materialnumeroserie.cdmaterialnumeroserie, materialnumeroserie.numero ")
				.join("romaneio.localarmazenagemorigem localarmazenagemorigem")
				.join("romaneio.localarmazenagemdestino localarmazenagemdestino")
				.join("romaneio.listaRomaneioitem romaneioitem")
				.join("romaneioitem.material material")
				.join("material.unidademedida unidademedida")
				.leftOuterJoin("material.listaMaterialnumeroserie listaMaterialnumeroserie")
				.leftOuterJoin("romaneio.cliente clienteromaneio")
				.leftOuterJoin("romaneioitem.loteestoque loteestoque")
				.leftOuterJoin("romaneioitem.patrimonioitem patrimonioitem")
				.leftOuterJoin("patrimonioitem.materialnumeroserie materialnumeroserie")
				.leftOuterJoin("romaneio.empresa empresa")
				.leftOuterJoin("empresa.logomarca logomarca")
				.leftOuterJoin("localarmazenagemorigem.endereco enderecoorigem")
				.leftOuterJoin("enderecoorigem.municipio municipioorigem")
				.leftOuterJoin("municipioorigem.uf uforigem")
				.leftOuterJoin("localarmazenagemdestino.endereco enderecodestino")
				.leftOuterJoin("enderecodestino.municipio municipiodestino")
				.leftOuterJoin("municipiodestino.uf ufdestino")
				.leftOuterJoin("romaneio.endereco enderecodestinoRomaneio")
				.leftOuterJoin("enderecodestinoRomaneio.municipio municipiodestinoRomaneio")
				.leftOuterJoin("municipiodestinoRomaneio.uf ufdestinoRomaneio")
				.leftOuterJoin("romaneio.listaRomaneioorigem listaRomaneioorigem")
				.leftOuterJoin("listaRomaneioorigem.contrato contrato")
				.leftOuterJoin("contrato.cliente cliente")
				.leftOuterJoin("contrato.enderecoentrega enderecoentregacontrato")
				.leftOuterJoin("enderecoentregacontrato.municipio municipioentregacontrato")
				.leftOuterJoin("municipioentregacontrato.uf ufentregacontrato")
				.leftOuterJoin("contrato.endereco enderecocontrato")
				.leftOuterJoin("enderecocontrato.municipio municipiocontrato")
				.leftOuterJoin("municipiocontrato.uf ufcontrato")
				
				.leftOuterJoin("listaRomaneioorigem.contratofechamento contratofechamento")
				.leftOuterJoin("contratofechamento.cliente clientefechamento")
				.leftOuterJoin("contratofechamento.enderecoentrega enderecoentregacontratofechamento")
				.leftOuterJoin("enderecoentregacontratofechamento.municipio municipioentregacontratofechamento")
				.leftOuterJoin("municipioentregacontratofechamento.uf ufentregacontratofechamento")
				
				.leftOuterJoin("contratofechamento.endereco enderecocontratofechamento")
				.leftOuterJoin("enderecocontratofechamento.municipio municipiocontratofechamento")
				.leftOuterJoin("municipiocontratofechamento.uf ufcontratofechamento")
				.whereIn("romaneio.cdromaneio", whereIn)
				.orderBy("romaneio.cdromaneio desc, listaRomaneioorigem.cdromaneioorigem asc")
				.list();
	}

	/**
	 * Verifica se exite algum romaneio que tenha origem os contratos passados por par�metro.
	 *
	 * @param whereInContrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/10/2013
	 */
	public Boolean haveRomaneioOrigemContrato(String whereInContrato) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Romaneio.class)
					.leftOuterJoin("romaneio.listaRomaneioorigem romaneioorigem")
					.leftOuterJoin("romaneioorigem.contrato contrato")
					.whereIn("contrato.cdcontrato", whereInContrato)
					.unique() > 0;
	}
	
	/**
	 * M�todo que verifica se existe romaneio de devolu��o associado ao contrato
	 *
	 * @param whereInContrato
	 * @return
	 * @author Luiz Fernando
	 * @since 19/05/2014
	 */
	public Boolean haveRomaneioEntradaOrigemContrato(String whereInContrato) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Romaneio.class)
					.leftOuterJoin("romaneio.listaRomaneioorigem romaneioorigem")
					.leftOuterJoin("romaneioorigem.contrato contrato")
					.leftOuterJoin("romaneioorigem.contratofechamento contratofechamento")
					.leftOuterJoin("romaneio.romaneiosituacao romaneiosituacao")
					.openParentheses()
						.whereIn("contratofechamento.cdcontrato", whereInContrato)
						.or()
						.whereIn("contratofechamento.cdcontrato", whereInContrato)
					.closeParentheses()
					.where("romaneio.romaneiotipo = ?", Romaneiotipo.ENTRADA)
					.where("romaneiosituacao <> ?", Romaneiosituacao.CANCELADA)
					.unique() > 0;
	}

	/**
	 * Busca os romaneios que tem origem a requisi��o de material
	 *
	 * @param requisicaomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 22/04/2014
	 */
	public List<Romaneio> findByRequisicaomaterial(Requisicaomaterial requisicaomaterial) {
		return query()
				.select("romaneio.cdromaneio, romaneio.descricao")
				.leftOuterJoin("romaneio.listaRomaneioorigem romaneioorigem")
				.where("romaneio.romaneiosituacao <> ?", Romaneiosituacao.CANCELADA)
				.where("romaneioorigem.requisicaomaterial = ?", requisicaomaterial)
				.list();
	}
	
	/**
	* M�todo que verifica se o romaneio � o �ltimo romaneio gerado
	*
	* @param romaneio
	* @param whereInContrato
	* @return
	* @since 09/07/2014
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public boolean isUltimoRomaneioContrato(Romaneio romaneio, String whereInContrato){
		if(StringUtils.isEmpty(whereInContrato)) return true;
		if(romaneio == null || romaneio.getCdromaneio() == null) return false;
		
		StringBuilder sql = new StringBuilder();
		sql.append(" select r.dtromaneio as dtromaneio");
		sql.append(" from romaneio r ");
		sql.append(" join romaneioorigem ro on ro.cdromaneio = r.cdromaneio ");
		sql.append(" where (ro.cdcontrato in (" + whereInContrato + ") or ro.cdcontratofechamento in (" + whereInContrato + ") ) ");
		sql.append(" and r.dtromaneio is not null ");
		sql.append(" and r.cdromaneiosituacao <> " + Romaneiosituacao.CANCELADA.getCdromaneiosituacao());
		sql.append(" and r.cdromaneio <> " + romaneio.getCdromaneio());
		sql.append(" order by r.dtromaneio desc limit 1 ");

		SinedUtil.markAsReader();
		List<Date> list = getJdbcTemplate().query(sql.toString() ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getDate("dtromaneio");
			}
		});
		Date dataUltimoRomaneio = SinedUtil.isListNotEmpty(list) ? list.get(0) : null;
		
		
		return dataUltimoRomaneio == null || SinedDateUtils.afterIgnoreHour(romaneio.getDtromaneio(), dataUltimoRomaneio);
	}
	
	/**
	* M�todo que verifica se o �ltimo romaneio � de devolu��o
	*
	* @param contrato
	* @return
	* @since 18/08/2014
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public boolean isUltimoRomaneioDevolucaoNotCanceladoByContrato(Contrato contrato){
		if(contrato == null || contrato.getCdcontrato() == null) return false;
		
		StringBuilder sql = new StringBuilder();
		sql.append(" select r.romaneiotipo as romaneiotipo, r.cdromaneiosituacao as cdromaneiosituacao ");
		sql.append(" from romaneio r ");
		sql.append(" join romaneioorigem ro on ro.cdromaneio = r.cdromaneio ");
		sql.append(" where (ro.cdcontrato = " + contrato.getCdcontrato() + " or ro.cdcontratofechamento = " + contrato.getCdcontrato() + " ) ");
		sql.append(" and r.dtromaneio is not null ");
//		sql.append(" and r.cdromaneiosituacao <> " + Romaneiosituacao.CANCELADA.getCdromaneiosituacao());
		sql.append(" order by r.dtromaneio desc, r.cdromaneio desc limit 1 ");

		SinedUtil.markAsReader();
		List<Romaneio> list = getJdbcTemplate().query(sql.toString() ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Romaneio romaneio = new Romaneio();
				Integer valueRomaneiotipo = rs.getInt("romaneiotipo");
				Integer valueRomaneiosituacao = rs.getInt("cdromaneiosituacao");
				romaneio.setRomaneiotipo(Romaneiotipo.SAIDA.getValue().equals(valueRomaneiotipo) ?  Romaneiotipo.SAIDA : Romaneiotipo.ENTRADA);
				romaneio.setRomaneiosituacao(new Romaneiosituacao(valueRomaneiosituacao, ""));
				return romaneio;
			}
		});
		
		Romaneio romaneio = null;
		if(SinedUtil.isListNotEmpty(list)){
			romaneio = list.get(0);
		}
		
		return romaneio != null && romaneio.getRomaneiotipo() != null && Romaneiotipo.ENTRADA.equals(romaneio.getRomaneiotipo()) && 
				romaneio.getRomaneiosituacao() != null && !Romaneiosituacao.CANCELADA.equals(romaneio.getRomaneiosituacao());
	}
	
	/**
	* M�todo que carrega o romaneio com a situa��o
	*
	* @param romaneio
	* @return
	* @since 09/07/2014
	* @author Luiz Fernando
	*/
	public Romaneio loadWithSituacao(Romaneio romaneio) {
		if(romaneio == null || romaneio.getCdromaneio() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("romaneio.cdromaneio, romaneio.dtromaneio, romaneiosituacao.cdromaneiosituacao")
				.join("romaneio.romaneiosituacao romaneiosituacao")
				.where("romaneio = ?", romaneio)
				.unique();
	}
	
	/**
	 * Realiza busca geral em Romaneio conforme par�metro da busca.
	 * 
	 * @param busca
	 * @return
	 * @author Mairon
	 * @sice 22/12/2017
	 */
	public List<Romaneio> findRomaneioForBuscaGeral(String busca) {
		return query().select("romaneio.cdromaneio, romaneio.descricao, localarmazenagemdestino.nome, cliente.nome")
				.leftOuterJoin("romaneio.cliente cliente")
				.leftOuterJoin("romaneio.localarmazenagemdestino localarmazenagemdestino")
				.openParentheses()
				.whereLikeIgnoreAll("cliente.nome", busca)
				.or()
				.whereLikeIgnoreAll("romaneio.descricao", busca)
				.closeParentheses()
				.where("romaneio.romaneiosituacao = ?", Romaneiosituacao.EM_ABERTO)
				.orderBy("romaneio.descricao").list();
	}
	
	public Romaneio findRomaneioById(Integer id) {
		return query()
				.select("romaneio.cdromaneio, romaneio.romaneiotipo, localarmazenagemorigem.nome, localarmazenagemdestino.nome, empresa.cdpessoa, " +
						"romaneio.descricao, romaneio.placa, romaneio.motorista, romaneio.conferente, romaneio.qtdeajudantes, cliente.cdpessoa, " +
						"empresa.nome, cliente.nome, material.cdmaterial, patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, " +
						"listaMaterialnumeroserie.numero, patrimonioitem.materialnumeroserie, romaneio.dtromaneio, material.nome, " +
						"material.codigobarras, material.identificacao, loteestoque.numero, cliente.cpf, cliente.cnpj, cliente.inscricaoestadual, " +
						"cliente.inscricaomunicipal, listaEndereco.logradouro, listaEndereco.cep, municipio.nome, uf.sigla, listaTelefone.telefone, " +
						"empresa.cpf, empresa.cnpj, empresa.inscricaoestadual, empresa.inscricaomunicipal, listaEnderecoEmp.logradouro, " +
						"listaEnderecoEmp.cep, municipioEmp.nome, ufEmp.sigla, listaTelefoneEmp.telefone, romaneio.romaneiotipo, romaneio.solicitante, " +
						"cliente.razaosocial, empresa.razaosocial, listaRomaneioitem.qtde ")
				.leftOuterJoin("romaneio.localarmazenagemorigem localarmazenagemorigem")
				.leftOuterJoin("romaneio.localarmazenagemdestino localarmazenagemdestino")
				.leftOuterJoin("romaneio.empresa empresa")
				.leftOuterJoin("empresa.listaEndereco listaEnderecoEmp")
				.leftOuterJoin("listaEnderecoEmp.municipio municipioEmp")
				.leftOuterJoin("municipioEmp.uf ufEmp")
				.leftOuterJoin("empresa.listaTelefone listaTelefoneEmp")
				.leftOuterJoin("romaneio.cliente cliente")
				.leftOuterJoin("cliente.listaEndereco listaEndereco")
				.leftOuterJoin("listaEndereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("cliente.listaTelefone listaTelefone")
				.leftOuterJoin("romaneio.listaRomaneioitem listaRomaneioitem")
				.leftOuterJoin("listaRomaneioitem.material material")
				.leftOuterJoin("material.listaMaterialnumeroserie listaMaterialnumeroserie")
				.leftOuterJoin("listaRomaneioitem.patrimonioitem patrimonioitem")
				.leftOuterJoin("patrimonioitem.materialnumeroserie materialnumeroserie")
				.leftOuterJoin("listaRomaneioitem.loteestoque loteestoque")
				.where("romaneio.cdromaneio = ?", id)
				.unique();
	}

	public List<Romaneio> findForCsvReport(RomaneioFiltro filtro) {
		QueryBuilder<Romaneio> query = querySined();
		updateListagemQuery(query, filtro);
		return query.list();
	}

	
}