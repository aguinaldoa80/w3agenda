package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Fornecimentotipo;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Periodo;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Planejamentosituacao;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.HistogramaRGFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.HistogramaRHFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.PlanejamentoFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.Histograma;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.RecursosReportBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.OrcamentoAnaliticoReportFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("planejamento.descricao")
public class PlanejamentoDAO extends GenericDAO<Planejamento>{

	@Override
	public void updateListagemQuery(QueryBuilder<Planejamento> query,FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		}
		PlanejamentoFiltro filtro = (PlanejamentoFiltro) _filtro;
		
		query
			.select("planejamento.cdplanejamento, planejamento.descricao, planejamento.versao, planejamento.dtinicio, " +
					"planejamento.dtfim, planejamentosituacao.cdplanejamentosituacao,  planejamentosituacao.nome, projeto.nome")
			.join("planejamento.projeto projeto")
			.join("planejamento.planejamentosituacao planejamentosituacao")
			.where("projeto = ?",filtro.getProjeto())
			.whereLikeIgnoreAll("planejamento.descricao", filtro.getDescricao());
		
		List<Planejamentosituacao> listaSituacao = filtro.getListaSituacao();
		if (listaSituacao != null && listaSituacao.size() > 0) {
			query.whereIn("planejamentosituacao.cdplanejamentosituacao", CollectionsUtil.listAndConcatenate(listaSituacao, "cdplanejamentosituacao", ","));
		}
		
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Planejamento> query) {
		query
			.select("planejamento.cdplanejamento, planejamento.descricao, projeto.cdprojeto, projeto.dtprojeto, planejamento.dtinicio, " +
					"planejamento.dtfim, planejamento.custo, planejamento.versao, planejamento.divisaotempo, planejamento.tamanhotempo, " +
					"planejamentosituacao.cdplanejamentosituacao, planejamentosituacao.nome, listaPlanejamentorecursogeral.cdplanejamentorecursogeral, " +
					"listaPlanejamentorecursogeral.qtde, listaPlanejamentorecursogeral.dtinicio, listaPlanejamentorecursogeral.dtfim, " +
					"listaPlanejamentorecursohumano.cdplanejamentorecursohumano, listaPlanejamentorecursohumano.qtde, listaPlanejamentorecursohumano.dtinicio, " +
					"listaPlanejamentorecursohumano.dtfim, material.cdmaterial, material.nome, cargo.cdcargo, cargo.nome, " +
					"listaPlanejamentodespesa.cdplanejamentodespesa, listaPlanejamentodespesa.qtde, listaPlanejamentodespesa.unitario, " +
					"fornecimentotipo.cdfornecimentotipo, contagerencial.cdcontagerencial, listaPlanejamentodespesa.observacao, orcamento.cdorcamento")
			.leftOuterJoin("planejamento.listaPlanejamentorecursogeral listaPlanejamentorecursogeral")
			.leftOuterJoin("listaPlanejamentorecursogeral.material material")		
			.leftOuterJoin("planejamento.listaPlanejamentorecursohumano listaPlanejamentorecursohumano")
			.leftOuterJoin("planejamento.listaPlanejamentodespesa listaPlanejamentodespesa")
			.leftOuterJoin("listaPlanejamentodespesa.contagerencial contagerencial")
			.leftOuterJoin("listaPlanejamentodespesa.fornecimentotipo fornecimentotipo")
			.leftOuterJoin("listaPlanejamentorecursohumano.cargo cargo")
			.join("planejamento.planejamentosituacao planejamentosituacao")
			.join("planejamento.projeto projeto")
			.leftOuterJoin("planejamento.orcamento orcamento")
			.orderBy("material.nome, cargo.nome");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaPlanejamentorecursogeral");
		save.saveOrUpdateManaged("listaPlanejamentorecursohumano");
		save.saveOrUpdateManaged("listaPlanejamentodespesa");
		
		Planejamento planejamento = (Planejamento)save.getEntity();
		if(SinedUtil.isListNotEmpty(planejamento.getListaTarefa())){
			save.saveOrUpdateManaged("listaTarefa");
		}
		if(SinedUtil.isListNotEmpty(planejamento.getListaApontamento())){
			save.saveOrUpdateManaged("listaApontamento");
		}
	}
	
	/**
	 * Retorna um boleano que representa se tem outro planejamento daquele projeto nas situa��es
	 * que foram passadas por par�metros.
	 *
	 * @param planejamento
	 * @param lista
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean isOutroProjetoComSituacao(Planejamento planejamento, List<Planejamentosituacao> lista){
		if (planejamento == null || planejamento.getProjeto() == null || planejamento.getProjeto().getCdprojeto() == null) {
			throw new SinedException("Projeto n�o pode ser nulo.");
		}		
		if (lista == null || lista.size() <= 0) {
			throw new SinedException("Lista de situa��es n�o pode ser nula.");
		}
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.setUseTranslator(false)
				.from(Planejamento.class)
				.join("planejamento.projeto projeto")
				.join("planejamento.planejamentosituacao planejamentosituacao")
				.where("planejamento.cdplanejamento <> ?",planejamento.getCdplanejamento())
				.where("projeto = ?",planejamento.getProjeto())
				.whereIn("planejamentosituacao.cdplanejamentosituacao", CollectionsUtil.listAndConcatenate(lista, "cdplanejamentosituacao", ","))
				.unique() > 0;
	}

	/**
	 * Carrega a lista de planejamentos com a situa��o preenchida.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Planejamento> loadWithSituacao(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("PLanjamentos n�o podem ser nulos.");
		}
		return query()
					.select("planejamento.cdplanejamento, planejamentosituacao.cdplanejamentosituacao, projeto.cdprojeto, projeto.nome, empresa.cdpessoa ")
					.join("planejamento.planejamentosituacao planejamentosituacao")
					.join("planejamento.projeto projeto")
					.leftOuterJoin("projeto.empresa empresa")
					.whereIn("planejamento.cdplanejamento", whereIn)
					.list();
	}

	/**
	 * Executa o update nos planejamentos passados por par�metro da situa��o tamb�m passada por par�metro.
	 *
	 * @param whereIn
	 * @param situacao
	 * @author Rodrigo Freitas
	 */
	public void updateSituacao(String whereIn, Planejamentosituacao situacao) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("PLanjamentos n�o podem ser nulos.");
		}
		getHibernateTemplate().bulkUpdate("update Planejamento p set p.planejamentosituacao = ? where p.id in ("+whereIn+")",situacao);		
	}
	
	/**
	 * Retorna o c�digo e a descri��o dos planejamentos associados ao projeto para serem utilizados no Flex
	 * Se o projeto for nulo, n�o ser� retornado nenhum planejamento.
	 *  
	 * @param projeto
	 * @return lista de planejamentos
	 * @author Rodrigo Alvarenga
	 */			
	public List<Planejamento> findByProjetoForFlex(Projeto projeto){
		if (projeto == null || projeto.getCdprojeto() == null) {
			return new ArrayList<Planejamento>();
		}
		return query()
			.select("planejamento.cdplanejamento, planejamento.descricao")
			.join("planejamento.projeto projeto")
			.where("projeto = ?", projeto)
			.orderBy("planejamento.descricao")
			.list();
	}

	/**
	 * Carrega a lista de periodo com as quantidades j� somadas dos cargos, para o c�lculo
	 * de recursos do projeto.
	 *
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<Periodo> carregaListaCargoQtde(Planejamento planejamento) {		
		SinedUtil.markAsReader();
		List<Periodo> listaUpdate = getJdbcTemplate().query("SELECT CDCARGO, NOME, SUM(SOMA) AS SOMA " +
				"FROM " +
				"(SELECT C.CDCARGO, C.NOME, SUM(TH.QTDE) AS SOMA " +
				"FROM TAREFA T " +
				"JOIN TAREFARECURSOHUMANO TH ON T.CDTAREFA = TH.CDTAREFA " +
				"JOIN CARGO C ON TH.CDCARGO = C.CDCARGO " +
				"WHERE T.CDPLANEJAMENTO = " + planejamento.getCdplanejamento() + " " +
				"GROUP BY C.CDCARGO, C.NOME " +
				"UNION ALL " +
				"SELECT CC.CDCARGO, CC.NOME, SUM(PH.QTDE) AS SOMA " +
				"FROM PLANEJAMENTO P " +
				"JOIN PLANEJAMENTORECURSOHUMANO PH ON P.CDPLANEJAMENTO = PH.CDPLANEJAMENTO " +
				"JOIN CARGO CC ON CC.CDCARGO = PH.CDCARGO " +
				"WHERE P.CDPLANEJAMENTO = " + planejamento.getCdplanejamento() + " " +
				"GROUP BY CC.CDCARGO, CC.NOME) QUERY " +
				"GROUP BY CDCARGO, NOME " +
				"ORDER BY NOME", 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Periodo filtro = new Periodo();
				filtro.setTotal(rs.getInt("SOMA"));
				
				Cargo cargo = new Cargo();
				cargo.setNome(rs.getString("NOME"));
				cargo.setCdcargo(rs.getInt("CDCARGO"));
				filtro.setCargo(cargo);
				
				return filtro;
			}
		});
		
		return listaUpdate;
	}	
	
	/**
	 * Retorna a lista de recursos (Geral, Humano, Despesa) de um planejamento.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<RecursosReportBean> getListaRecursosReport(OrcamentoAnaliticoReportFiltro filtro){
		SinedUtil.markAsReader();
		List<RecursosReportBean> listaUpdate = getJdbcTemplate().query(
				"SELECT TIPO, COD, ITEM, UNIDADE, SUM(SOMA) AS QTDE, UNITARIO, SUM(SOMA)*UNITARIO AS TOTAL " +
				"FROM ( " +
				"SELECT 2 AS IMPORTANCIA, 'RECURSOS HUMANOS' AS TIPO, C1.CDCARGO AS COD, C1.NOME AS ITEM, SUM(PH.QTDE) AS SOMA, 'HH' AS UNIDADE, (SELECT VR.VALOR FROM VALORREFERENCIA VR WHERE VR.CDPLANEJAMENTO = P1.CDPLANEJAMENTO AND VR.CDCARGO = C1.CDCARGO) AS UNITARIO " +
				"FROM PLANEJAMENTO P1 " +
				"JOIN PLANEJAMENTORECURSOHUMANO PH ON PH.CDPLANEJAMENTO = P1.CDPLANEJAMENTO " +
				"JOIN CARGO C1 ON C1.CDCARGO = PH.CDCARGO " +
				"WHERE P1.CDPLANEJAMENTO = " + filtro.getPlanejamento().getCdplanejamento() + " " +
				(filtro.getFuncao() != null && !filtro.getFuncao() && filtro.getListaCargo() != null && filtro.getListaCargo().size() > 0 ? "AND C1.CDCARGO IN (" + CollectionsUtil.listAndConcatenate(filtro.getListaCargo(), "cdcargo", ",") + ") " : "") +
				(filtro.getFuncao() == null ? "AND 1 = 0 " : "") +
				
				"GROUP BY C1.CDCARGO, C1.NOME, P1.CDPLANEJAMENTO " +
			
				"UNION ALL " +
			
				"SELECT 1 AS IMPORTANCIA,'RECURSOS GERAIS' AS TIPO, M1.CDMATERIAL AS COD, M1.NOME AS ITEM, SUM(PG.QTDE) AS SOMA, UN.SIMBOLO AS UNIDADE, (SELECT VR.VALOR FROM VALORREFERENCIA VR WHERE VR.CDPLANEJAMENTO = P2.CDPLANEJAMENTO AND VR.CDMATERIAL = M1.CDMATERIAL) AS UNITARIO " +
				"FROM PLANEJAMENTO P2 " +
				"JOIN PLANEJAMENTORECURSOGERAL PG ON PG.CDPLANEJAMENTO = P2.CDPLANEJAMENTO " +
				"JOIN MATERIAL M1 ON M1.CDMATERIAL = PG.CDMATERIAL " +
				"JOIN UNIDADEMEDIDA UN ON UN.CDUNIDADEMEDIDA = M1.CDUNIDADEMEDIDA " +
				"WHERE P2.CDPLANEJAMENTO = " + filtro.getPlanejamento().getCdplanejamento() + " " +
				(filtro.getMaterial() != null && !filtro.getMaterial() && filtro.getListaMaterial() != null && filtro.getListaMaterial().size() > 0 ? "AND M1.CDMATERIAL IN (" + CollectionsUtil.listAndConcatenate(filtro.getListaMaterial(), "cdmaterial", ",") + ") " : "") +
				(filtro.getMaterial() == null ? "AND 1 = 0 " : "") +
				
				"GROUP BY M1.CDMATERIAL, M1.NOME, UN.SIMBOLO, P2.CDPLANEJAMENTO " +
			
				"UNION ALL " +
			
				"SELECT 2 AS IMPORTANCIA, 'RECURSOS HUMANOS' AS TIPO, C1.CDCARGO AS COD, C1.NOME AS ITEM, SUM(TH.QTDE) AS SOMA, 'HH' AS UNIDADE, (SELECT VR.VALOR FROM VALORREFERENCIA VR WHERE VR.CDPLANEJAMENTO = PP1.CDPLANEJAMENTO AND VR.CDCARGO = C1.CDCARGO) AS UNITARIO " +
				"FROM TAREFA T1 " +
				"JOIN PLANEJAMENTO PP1 ON PP1.CDPLANEJAMENTO = T1.CDPLANEJAMENTO " +
				"JOIN TAREFARECURSOHUMANO TH ON TH.CDTAREFA = T1.CDTAREFA " +
				"JOIN CARGO C1 ON C1.CDCARGO = TH.CDCARGO " +
				"WHERE PP1.CDPLANEJAMENTO = " + filtro.getPlanejamento().getCdplanejamento() + " " +
				(filtro.getFuncao() != null && !filtro.getFuncao() && filtro.getListaCargo() != null && filtro.getListaCargo().size() > 0 ? "AND C1.CDCARGO IN (" + CollectionsUtil.listAndConcatenate(filtro.getListaCargo(), "cdcargo", ",") + ") " : "") +
				(filtro.getFuncao() == null ? "AND 1 = 0 " : "") +
				
				"GROUP BY C1.CDCARGO, C1.NOME, PP1.CDPLANEJAMENTO " +
			
				"UNION ALL " +
			
				"SELECT 1 AS IMPORTANCIA, 'RECURSOS GERAIS' AS TIPO, M2.CDMATERIAL AS COD, M2.NOME AS ITEM, SUM(TG.QTDE) AS SOMA, UN.SIMBOLO AS UNIDADE, (SELECT VR.VALOR FROM VALORREFERENCIA VR WHERE VR.CDPLANEJAMENTO = PP2.CDPLANEJAMENTO AND VR.CDMATERIAL = M2.CDMATERIAL) AS UNITARIO " +
				"FROM TAREFA T2 " +
				"JOIN PLANEJAMENTO PP2 ON PP2.CDPLANEJAMENTO = T2.CDPLANEJAMENTO " +
				"JOIN TAREFARECURSOGERAL TG ON TG.CDTAREFA = T2.CDTAREFA " +
				"JOIN MATERIAL M2 ON M2.CDMATERIAL = TG.CDMATERIAL " +
				"JOIN UNIDADEMEDIDA UN ON UN.CDUNIDADEMEDIDA = M2.CDUNIDADEMEDIDA " +
				"WHERE PP2.CDPLANEJAMENTO = " + filtro.getPlanejamento().getCdplanejamento() + " " +
				(filtro.getMaterial() != null && !filtro.getMaterial() && filtro.getListaMaterial() != null && filtro.getListaMaterial().size() > 0 ? "AND M2.CDMATERIAL IN (" + CollectionsUtil.listAndConcatenate(filtro.getListaMaterial(), "cdmaterial", ",") + ") " : "") +
				(filtro.getMaterial() == null ? "AND 1 = 0 " : "") +
				
				"GROUP BY M2.CDMATERIAL, M2.NOME, UN.SIMBOLO, PP2.CDPLANEJAMENTO " +
				
				"UNION ALL " +
				
				"SELECT 3 AS IMPORTANCIA, 'DESPESAS' AS TIPO, FT.CDFORNECIMENTOTIPO AS COD, FT.NOME AS ITEM, PD.QTDE AS SOMA, 'Un' AS UNIDADE, PD.VALORUNITARIO AS UNITARIO " +
				"FROM PLANEJAMENTO P " +
				"JOIN PLANEJAMENTODESPESA PD ON PD.CDPLANEJAMENTO = P.CDPLANEJAMENTO " +
				"JOIN FORNECIMENTOTIPO FT ON FT.CDFORNECIMENTOTIPO = PD.CDFORNECIMENTOTIPO " +
				"WHERE P.CDPLANEJAMENTO = " + filtro.getPlanejamento().getCdplanejamento() + " " +
				(filtro.getDespesa() != null && !filtro.getDespesa() && filtro.getListaDespesa() != null && filtro.getListaDespesa().size() > 0 ? "AND FT.CDFORNECIMENTOTIPO IN ("+CollectionsUtil.listAndConcatenate(filtro.getListaDespesa(), "cdfornecimentotipo", ",")+") " : "")+
				(filtro.getDespesa() == null ? "AND 1 = 0 " : "") +
				") QUERY " +
				"GROUP BY IMPORTANCIA, TIPO, COD, ITEM, UNIDADE, UNITARIO " +
				"ORDER BY IMPORTANCIA, ITEM", 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new RecursosReportBean(
						rs.getString("TIPO"), 
						rs.getString("ITEM"), 
						rs.getString("UNIDADE"), 
						rs.getInt("QTDE"), 
						new Money(rs.getInt("UNITARIO"),true), 
						new Money(rs.getInt("TOTAL"), true));
			}
		});
		
		return listaUpdate;
	}

	/**
	 * Retorna a listagem de todos os cargos do planejamento.
	 *
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<Cargo> findCargos(Planejamento planejamento) {
		SinedUtil.markAsReader();
		List<Cargo> listaUpdate = getJdbcTemplate().query(
				"SELECT * " +
				"FROM( " +
				"SELECT C1.CDCARGO, C1.NOME " +
				"FROM PLANEJAMENTO P1 " +
				"JOIN PLANEJAMENTORECURSOHUMANO PH ON PH.CDPLANEJAMENTO = P1.CDPLANEJAMENTO " +
				"JOIN CARGO C1 ON C1.CDCARGO = PH.CDCARGO " +
				"WHERE P1.CDPLANEJAMENTO = " + planejamento.getCdplanejamento() +
				"GROUP BY C1.CDCARGO, C1.NOME " +
				"UNION " +
				"SELECT C1.CDCARGO, C1.NOME " +
				"FROM TAREFA T1 " +
				"JOIN TAREFARECURSOHUMANO TH ON TH.CDTAREFA = T1.CDTAREFA " +
				"JOIN CARGO C1 ON C1.CDCARGO = TH.CDCARGO " +
				"WHERE T1.CDPLANEJAMENTO = " + planejamento.getCdplanejamento() +
				"GROUP BY C1.CDCARGO, C1.NOME " +
				") QUERY " +
				"ORDER BY NOME", 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Cargo(
						rs.getInt("CDCARGO"), 
						rs.getString("NOME"));
			}
		});
		
		return listaUpdate;
	}

	/**
	 * Carrega a lista de materiais de um planejamento.
	 *
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<Material> findMateriais(Planejamento planejamento) {
		SinedUtil.markAsReader();
		List<Material> listaUpdate = getJdbcTemplate().query(
				"SELECT * " +
				"FROM( " +
				"SELECT C1.CDMATERIAL, C1.NOME " +
				"FROM PLANEJAMENTO P1 " +
				"JOIN PLANEJAMENTORECURSOGERAL PH ON PH.CDPLANEJAMENTO = P1.CDPLANEJAMENTO " +
				"JOIN MATERIAL C1 ON C1.CDMATERIAL = PH.CDMATERIAL " +
				"WHERE P1.CDPLANEJAMENTO = " + planejamento.getCdplanejamento() +
				"GROUP BY C1.CDMATERIAL, C1.NOME " +
				"UNION " +
				"SELECT C1.CDMATERIAL, C1.NOME " +
				"FROM TAREFA T1 " +
				"JOIN TAREFARECURSOGERAL TH ON TH.CDTAREFA = T1.CDTAREFA " +
				"JOIN MATERIAL C1 ON C1.CDMATERIAL = TH.CDMATERIAL " +
				"WHERE T1.CDPLANEJAMENTO = " + planejamento.getCdplanejamento() +
				"GROUP BY C1.CDMATERIAL, C1.NOME " +
				") QUERY " +
				"ORDER BY NOME", 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Material(
						rs.getInt("CDMATERIAL"), 
						rs.getString("NOME"));
			}
		});
		
		return listaUpdate;
	}

	/**
	 * Carrega a lista de despesa de um planejamento.
	 *
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<Fornecimentotipo> findDespesa(Planejamento planejamento) {

		SinedUtil.markAsReader();
		List<Fornecimentotipo> lista = getJdbcTemplate().query(
				"SELECT DISTINCT FT.CDFORNECIMENTOTIPO, FT.NOME " +
				"FROM PLANEJAMENTODESPESA PD " +
				"JOIN FORNECIMENTOTIPO FT ON FT.CDFORNECIMENTOTIPO = PD.CDFORNECIMENTOTIPO " +
				"WHERE PD.CDPLANEJAMENTO = " + planejamento.getCdplanejamento(), 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Fornecimentotipo(
						rs.getInt("CDFORNECIMENTOTIPO"), 
						rs.getString("NOME"));
			}
		});
		
		return lista;
	}

	/**
	 * Retorna uma lista de planejamentos de um determinado projeto que est�o em uma determinada situa��o.
	 *
	 * @param projeto
	 * @param planejamentoSituacao
	 * @return lista de Planejamento
	 * @throws SinedException - quando um dos par�metros for nulo.
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Planejamento> findByProjetoSituacao(Projeto projeto, Planejamentosituacao planejamentoSituacao) {
		if (projeto == null || projeto.getCdprojeto() == null || planejamentoSituacao == null || planejamentoSituacao.getCdplanejamentosituacao() == null) {
			throw new SinedException("Par�metros inv�lidos. Os campos projeto e planejamentoSituacao n�o podem ser nulos.");
		}
		return 
			query()
				.select("planejamento.cdplanejamento, planejamento.descricao, planejamento.dtinicio, planejamento.dtfim")
				.join("planejamento.projeto projeto")				
				.join("planejamento.planejamentosituacao planejamentosituacao")
				.where("projeto = ?", projeto)
				.where("planejamentosituacao = ?", planejamentoSituacao)
				.list();
	}

	/**
	 * Carrega a lista de <code>Histograma</code> para o preenchimento do relat�rio de histograma de RG 
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<Histograma> findForHistogramaRG(HistogramaRGFiltro filtro) {
		
		if(filtro == null){
			throw new SinedException("Erro na passagem de par�mentro");
		}
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		SinedUtil.markAsReader();
		List<Histograma> lista = getJdbcTemplate().query(
				"SELECT DATA, SUM(QTDEPREVISTA) AS QTDEPREVISTA, SUM(QTDEREALIZADA) AS QTDEREALIZADA " +
				"FROM( " +
				"SELECT T.DTINICIO AS DATA, SUM(TG.QTDE) AS QTDEPREVISTA, 0 AS QTDEREALIZADA " +
				"FROM TAREFA T " +
				"JOIN TAREFARECURSOGERAL TG ON TG.CDTAREFA = T.CDTAREFA " +
				"WHERE T.CDPLANEJAMENTO = " + filtro.getPlanejamento().getCdplanejamento() + " " +
				"AND TG.CDMATERIAL = " + filtro.getMaterial().getCdmaterial() + " " +
				(filtro.getDtinicio() != null ? "AND T.DTINICIO >= TO_DATE('"+format.format(filtro.getDtinicio())+"', 'dd/mm/yyyy')" : "") +
				(filtro.getDtfim() != null ? "AND T.DTINICIO <= TO_DATE('"+format.format(filtro.getDtfim())+"', 'dd/mm/yyyy')" : "") +
				"GROUP BY T.DTINICIO " +
				
				"UNION ALL " +
				
				"SELECT P.DTINICIO AS DATA, SUM(PG.QTDE) AS QTDEPREVISTA, 0 AS QTDEREALIZADA " +
				"FROM PLANEJAMENTO P  " +
				"JOIN PLANEJAMENTORECURSOGERAL PG ON PG.CDPLANEJAMENTO = P.CDPLANEJAMENTO  " +
				"WHERE P.CDPLANEJAMENTO = " + filtro.getPlanejamento().getCdplanejamento() + " " +
				"AND PG.CDMATERIAL = " + filtro.getMaterial().getCdmaterial() + " " +
				(filtro.getDtinicio() != null ? "AND P.DTINICIO >= TO_DATE('"+format.format(filtro.getDtinicio())+"', 'dd/mm/yyyy')" : "") +
				(filtro.getDtfim() != null ? "AND P.DTINICIO <= TO_DATE('"+format.format(filtro.getDtfim())+"', 'dd/mm/yyyy')" : "") +
				"GROUP BY P.DTINICIO " +
				
				"UNION ALL " +
				
				"SELECT A.DTAPONTAMENTO AS DATA, 0 AS QTDEPREVISTA, A.QTDEHORAS AS QTDEREALIZADA " +
				"FROM APONTAMENTO A " +
				"WHERE A.CDPLANEJAMENTO = " + filtro.getPlanejamento().getCdplanejamento() + " " +
				"AND A.CDMATERIAL = " + filtro.getMaterial().getCdmaterial() + " " +
				(filtro.getDtinicio() != null ? "AND A.DTAPONTAMENTO >= TO_DATE('"+format.format(filtro.getDtinicio())+"', 'dd/mm/yyyy')" : "") +
				(filtro.getDtfim() != null ? "AND A.DTAPONTAMENTO <= TO_DATE('"+format.format(filtro.getDtfim())+"', 'dd/mm/yyyy')" : "") +
				") QUERY " +
				"GROUP BY DATA " +
				"ORDER BY DATA",
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Histograma h = new Histograma();
				h.setDia(SinedDateUtils.toString(rs.getDate("DATA")));
				h.setPlanejado(rs.getDouble("QTDEPREVISTA"));
				h.setRealizado(rs.getDouble("QTDEREALIZADA"));
				return h;
			}
		});
		
		return lista;
	}

	/**
	 * Carrega a lista de <code>Histograma</code> para o preenchimento do relat�rio de histograma de RH 
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public List<Histograma> findForHistogramaRH(HistogramaRHFiltro filtro) {
		
		if(filtro == null){
			throw new SinedException("Erro na passagem de par�mentro");
		}
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		SinedUtil.markAsReader();
		List<Histograma> lista = getJdbcTemplate().query(
				"SELECT DATA, SUM(QTDEPREVISTA) AS QTDEPREVISTA, SUM(QTDEREALIZADA) AS QTDEREALIZADA " +
				"FROM( " +
				"SELECT T.DTINICIO AS DATA, SUM(TH.QTDE) AS QTDEPREVISTA, 0 AS QTDEREALIZADA " +
				"FROM TAREFA T " +
				"JOIN TAREFARECURSOHUMANO TH ON TH.CDTAREFA = T.CDTAREFA " +
				"WHERE T.CDPLANEJAMENTO = " + filtro.getPlanejamento().getCdplanejamento() + " " +
				(filtro.getFuncao() == null && filtro.getListacargo() != null && filtro.getListacargo().size() > 0 ? "AND TH.CDCARGO IN (" + CollectionsUtil.listAndConcatenate(filtro.getListacargo(), "cdcargo", ",")+ ") " : "" ) +
				(filtro.getFuncao() != null && !filtro.getFuncao() && filtro.getCargo() != null ? "AND TH.CDCARGO = " + filtro.getCargo().getCdcargo() + " " : "") +
				(filtro.getDtinicio() != null ? "AND T.DTINICIO >= TO_DATE('"+format.format(filtro.getDtinicio())+"', 'dd/mm/yyyy')" : "") +
				(filtro.getDtfim() != null ? "AND T.DTINICIO <= TO_DATE('"+format.format(filtro.getDtfim())+"', 'dd/mm/yyyy')" : "") +
				"GROUP BY T.DTINICIO " +
				
				"UNION ALL " +
				
				"SELECT P.DTINICIO AS DATA, SUM(PH.QTDE) AS QTDEPREVISTA, 0 AS QTDEREALIZADA " +
				"FROM PLANEJAMENTO P " +
				"JOIN PLANEJAMENTORECURSOHUMANO PH ON PH.CDPLANEJAMENTO = P.CDPLANEJAMENTO  " +
				"WHERE P.CDPLANEJAMENTO = " + filtro.getPlanejamento().getCdplanejamento() + " " +
				(filtro.getFuncao() == null && filtro.getListacargo() != null && filtro.getListacargo().size() > 0 ? "AND PH.CDCARGO IN (" + CollectionsUtil.listAndConcatenate(filtro.getListacargo(), "cdcargo", ",")+ ") " : "" ) +
				(filtro.getFuncao() != null && !filtro.getFuncao() && filtro.getCargo() != null ? "AND PH.CDCARGO = " + filtro.getCargo().getCdcargo() + " " : "") + 
				(filtro.getDtinicio() != null ? "AND P.DTINICIO >= TO_DATE('"+format.format(filtro.getDtinicio())+"', 'dd/mm/yyyy')" : "") +
				(filtro.getDtfim() != null ? "AND P.DTINICIO <= TO_DATE('"+format.format(filtro.getDtfim())+"', 'dd/mm/yyyy')" : "") +
				"GROUP BY P.DTINICIO " +
				
				"UNION ALL " +
				
				"SELECT A.DTAPONTAMENTO AS DATA, 0 AS QTDEPREVISTA, A.QTDEHORAS AS QTDEREALIZADA " +
				"FROM APONTAMENTO A " +
				"WHERE A.CDPLANEJAMENTO = " + filtro.getPlanejamento().getCdplanejamento() + " " +
				(filtro.getFuncao() == null && filtro.getListacargo() != null && filtro.getListacargo().size() > 0 ? "AND A.CDCARGO IN (" + CollectionsUtil.listAndConcatenate(filtro.getListacargo(), "cdcargo", ",")+ ") " : "" ) +
				(filtro.getFuncao() != null && !filtro.getFuncao() && filtro.getCargo() != null ? "AND A.CDCARGO = " + filtro.getCargo().getCdcargo() + " " : "") + 
				(filtro.getDtinicio() != null ? "AND A.DTAPONTAMENTO >= TO_DATE('"+format.format(filtro.getDtinicio())+"', 'dd/mm/yyyy')" : "") +
				(filtro.getDtfim() != null ? "AND A.DTAPONTAMENTO <= TO_DATE('"+format.format(filtro.getDtfim())+"', 'dd/mm/yyyy')" : "") +
				") QUERY " +
				"GROUP BY DATA " +
				"ORDER BY DATA",
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Histograma h = new Histograma();
				h.setDia(rs.getDate("DATA")!=null ? SinedDateUtils.toString(rs.getDate("DATA")) : "");
				h.setPlanejado(rs.getDouble("QTDEPREVISTA"));
				h.setRealizado(rs.getDouble("QTDEREALIZADA"));
				return h;
			}
		});
		
		return lista;
	}

	/**
	 * Retorna true se o planjamento tiver alguma solicitacao de compra vinculada a alguma de suas tarefas.
	 *
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public boolean haveSolicitacaocompra(Planejamento planejamento) {
		if (planejamento == null || planejamento.getCdplanejamento() == null) {
			throw new SinedException("Planejamento n�o pode ser nulo.");
		}	
		SinedUtil.markAsReader();
		List<Integer> lista = getJdbcTemplate().query(
				"SELECT COUNT(*) AS QTDE " +
				"FROM PLANEJAMENTO P " +
				"JOIN TAREFA T ON T.CDPLANEJAMENTO = P.CDPLANEJAMENTO " +
				"JOIN TAREFARECURSOGERAL TRG ON TRG.CDTAREFA = T.CDTAREFA " +
				"JOIN SOLICITACAOCOMPRAORIGEM O ON O.CDTAREFARECURSOGERAL = TRG.CDTAREFARECURSOGERAL " +
				"JOIN SOLICITACAOCOMPRA SC ON SC.CDSOLICITACAOCOMPRAORIGEM = O.CDSOLICITACAOCOMPRAORIGEM " +
				"JOIN AUX_SOLICITACAOCOMPRA A ON A.CDSOLICITACAOCOMPRA = SC.CDSOLICITACAOCOMPRA " +
				"WHERE P.CDPLANEJAMENTO = " + planejamento.getCdplanejamento() + " " +
				"AND A.SITUACAO IN ("
				+Situacaosuprimentos.EM_ABERTO.getCodigo()+","
				+Situacaosuprimentos.AUTORIZADA.getCodigo()+","
				+Situacaosuprimentos.EM_PROCESSOCOMPRA.getCodigo()
				+")",
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getInt("QTDE");
			}
		});
		
		return lista.get(0) > 0;
	}		
	
	/**
	 * M�todo para carregar um planejamento e todas as listas da �rvore para c�pia de projeto.
	 * Os Fetch's s�o utilizados para facilitar o carregamento, j� que s�o muitos campos e se forem inclu�dos
	 * ou retirados, os joins j� buscam todos automaticamente.
	 * 
	 * @param planejamento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Planejamento loadForCopiaProjeto(Planejamento planejamento){
		if(!SinedUtil.isObjectValid(planejamento)){
			throw new SinedException("O par�metro planejamento � inv�lido.");
		}
		
		QueryBuilder<Planejamento> query = query();
		query
			.joinFetch("planejamento.projeto projeto")
			.leftOuterJoinFetch("planejamento.listaTarefa listaTarefa")
			.leftOuterJoinFetch("listaTarefa.listaProducaodiaria listaProducaodiaria")
			.leftOuterJoinFetch("listaTarefa.listaAtividade listaAtividade")
			.leftOuterJoinFetch("listaTarefa.listaTarefarecursogeral listaTarefarecursogeral")
			.leftOuterJoinFetch("listaTarefa.listaTarefarecursohumano listaTarefarecursohumano")
			.leftOuterJoinFetch("planejamento.listaApontamento listaApontamento")
			.leftOuterJoinFetch("planejamento.listaPlanejamentorecursogeral listaPrg")
			.leftOuterJoinFetch("planejamento.listaPlanejamentorecursohumano listaPrh")
			.leftOuterJoinFetch("planejamento.listaPlanejamentodespesa listaPlanejamentodespesa")
		;
		query.where("planejamento = ?", planejamento);
		return query.unique();
	}

	/**
	 * Verifica se o material pertence ao planejamento.
	 *
	 * @param material
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public boolean materialProjeto(Material material, Planejamento planejamento) {
		if (material == null || material.getCdmaterial() == null || planejamento == null || planejamento.getCdplanejamento() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		SinedUtil.markAsReader();
		List<Integer> lista = getJdbcTemplate().query(
				"SELECT COUNT(*) AS QTDE FROM ( " + 
				"SELECT M.CDMATERIAL, M.NOME " +
				"FROM PLANEJAMENTO PL " +
				"JOIN PLANEJAMENTORECURSOGERAL PRG ON PRG.CDPLANEJAMENTO = PL.CDPLANEJAMENTO " +
				"JOIN MATERIAL M ON M.CDMATERIAL = PRG.CDMATERIAL " +
				"WHERE M.CDMATERIAL = " + material.getCdmaterial() + " " +
				"AND PL.CDPLANEJAMENTO = " + planejamento.getCdplanejamento() + " " +
		
				"UNION " +
		
				"SELECT M.CDMATERIAL, M.NOME " +
				"FROM PLANEJAMENTO PL " +
				"JOIN TAREFA T ON T.CDPLANEJAMENTO = PL.CDPLANEJAMENTO " +
				"JOIN TAREFARECURSOGERAL TRG ON TRG.CDTAREFA = TRG.CDTAREFA " +
				"JOIN MATERIAL M ON M.CDMATERIAL = TRG.CDMATERIAL " +
				"WHERE M.CDMATERIAL = " + material.getCdmaterial() + " " +
				"AND PL.CDPLANEJAMENTO = " + planejamento.getCdplanejamento() + " " +
				") QUERY ",
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getInt("QTDE");
			}
		});
		
		return lista.get(0) > 0;
	}

	public Planejamento loadWithOrcamento(Planejamento planejamento) {
		if (planejamento == null || planejamento.getCdplanejamento() == null) {
			throw new SinedException("Planejamento n�o pode ser nulo.");
		}	
		return query()
					.select("planejamento.cdplanejamento, projeto.cdprojeto, orcamento.cdorcamento, " +
							"contagerencialmoi.cdcontagerencial, contagerencialmoi.nome," +
							" contagerencialmod.cdcontagerencial, contagerencialmod.nome, " +
							"vcontagerencialmod.identificador, vcontagerencialmoi.identificador")
					.join("planejamento.projeto projeto")
					.leftOuterJoin("planejamento.orcamento orcamento")
					.leftOuterJoin("orcamento.contagerencialmod contagerencialmod")
					.leftOuterJoin("contagerencialmod.vcontagerencial vcontagerencialmod")
					.leftOuterJoin("orcamento.contagerencialmoi contagerencialmoi")
					.leftOuterJoin("contagerencialmoi.vcontagerencial vcontagerencialmoi")
					.where("planejamento = ?", planejamento)
					.unique();
	}

	public Planejamento loadWithProjto(Planejamento planejamento) {
		return query()
					.select("planejamento.cdplanejamento, projeto.cdprojeto")
					.join("planejamento.projeto projeto")
					.where("planejamento = ?", planejamento)
					.unique();
	}
	
	public List<Planejamento> findByProjeto(Projeto projeto){
		if (projeto == null || projeto.getCdprojeto() == null)
			return new ArrayList<Planejamento>();
		
		return query()
			.select("planejamento.cdplanejamento, planejamento.descricao, planejamento.dtinicio, planejamento.dtfim")
			.join("planejamento.projeto projeto")
			.where("projeto = ?", projeto)
			.orderBy("planejamento.dtinicio desc")
			.list();
	}
}
