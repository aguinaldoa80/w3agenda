package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Prontuario;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.ProntuarioFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProntuarioDAO extends GenericDAO<Prontuario> {

	private ColaboradorService colaboradorService;
	
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Prontuario> query,	FiltroListagem _filtro) {
		ProntuarioFiltro filtro = (ProntuarioFiltro) _filtro;
		
		query
			.select("prontuario.cdprontuario, terapeuta.nome, paciente.nome, prontuario.dtinicio, prontuario.dtfim")
			.join("prontuario.terapeuta terapeuta")
			.join("prontuario.paciente paciente")
			.where("terapeuta=?", filtro.getTerapeuta())
			.where("paciente=?", filtro.getPaciente())
			.where("prontuario.dtinicio=?", filtro.getDtinicio())
			.where("prontuario.dtfim=?", filtro.getDtfim())
			.orderBy("prontuario.dtinicio, terapeuta.nome, paciente.nome");
		
		if (!Boolean.TRUE.equals(SinedUtil.isUsuarioLogadoAdministrador())){
			Colaborador colaborador = colaboradorService.load(new Colaborador(SinedUtil.getUsuarioLogado().getCdpessoa()));
			if (colaborador!=null){
				query.where("terapeuta=?", colaborador);
			}else {
				query.where("1=2");				
			}
		}
	}
		
	@Override
	public void updateEntradaQuery(QueryBuilder<Prontuario> query) {
		query
			.select("prontuario.cdprontuario, terapeuta.cdpessoa, terapeuta.nome, paciente.cdpessoa, paciente.nome, prontuario.numero," +
					"prontuario.dtinicio, prontuario.hrinicio, prontuario.dtfim, prontuario.hrfim, prontuario.observacao," +
					"listaProntuariohistorico.cdprontuariohistorico, listaProntuariohistorico.data, usuario.nome, listaProntuariohistorico.observacao")
			.join("prontuario.terapeuta terapeuta")
			.join("prontuario.paciente paciente")
			.join("prontuario.listaProntuariohistorico listaProntuariohistorico")
			.join("listaProntuariohistorico.usuario usuario")
			.orderBy("listaProntuariohistorico.data desc");
	}
}
