package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.EventosFinaisCorreios;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.EventosFinaisCorreiosFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EventosFinaisCorreiosDAO extends GenericDAO<EventosFinaisCorreios>{

	@Override
	public void updateListagemQuery(QueryBuilder<EventosFinaisCorreios> query, FiltroListagem _filtro) {	
		EventosFinaisCorreiosFiltro filtro = (EventosFinaisCorreiosFiltro) _filtro;
		
		query
			.select("distinct eventosFinaisCorreios.cdeventosfinaiscorreios, eventosFinaisCorreios.nome, eventosFinaisCorreios.finalizadoSucesso, eventosFinaisCorreios.insucesso, eventosFinaisCorreios.letra")
			.leftOuterJoin("eventosFinaisCorreios.listaTipoEventos tipoEventos", true)
			.leftOuterJoin("tipoEventos.eventosCorreios eventosCorreios", true)
			.leftOuterJoin("tipoEventos.tipoEventoCorreios tipoEventoCorreios", true)
			.whereLikeIgnoreAll("eventosFinaisCorreios.nome", filtro.getNome())
			.whereLikeIgnoreAll("eventosFinaisCorreios.letra", filtro.getLetra())
			.where("tipoEventoCorreios = ?", filtro.getTipoEventos());
	}
	
	
	@Override
	public void updateEntradaQuery(QueryBuilder<EventosFinaisCorreios> query) {
		query
			.select("eventosFinaisCorreios.cdeventosfinaiscorreios, eventosFinaisCorreios.nome, eventosFinaisCorreios.finalizadoSucesso, eventosFinaisCorreios.insucesso, " +
					"eventosFinaisCorreios.corLetra,eventosFinaisCorreios.corFundo, eventosFinaisCorreios.letra, " +
					"eventosFinaisCorreios.enviaemailinsucesso, eventosFinaisCorreios.email, eventosFinaisCorreios.cdusuarioaltera, eventosFinaisCorreios.dtaltera, " +
					"eventosCorreios.cdeventoscorreios,eventosCorreios.descricao, eventosCorreios.detalhe, eventosCorreios.codigo, tipoEventoCorreios.descricao, " +
					"tipoEventoCorreios.cdTipoEventoCorreios")
			.leftOuterJoin("eventosFinaisCorreios.listaTipoEventos tipoEventos")
			.leftOuterJoin("tipoEventos.eventosCorreios eventosCorreios")
			.leftOuterJoin("tipoEventos.tipoEventoCorreios tipoEventoCorreios");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaTipoEventos");
	}
}
