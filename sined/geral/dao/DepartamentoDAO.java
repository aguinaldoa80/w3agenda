package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.DepartamentoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("departamento.nome")
public class DepartamentoDAO extends GenericDAO<Departamento> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Departamento> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		DepartamentoFiltro filtro = (DepartamentoFiltro) _filtro;
		query
			.select("distinct departamento.cddepartamento, departamento.nome, departamento.codigofolha")
			.whereLikeIgnoreAll("departamento.nome", filtro.getNome())
			.whereLikeIgnoreAll("departamento.codigofolha", filtro.getCodigofolha())
			.orderBy("departamento.nome")
			;
		
		if(filtro.getCargo() != null){
			query
				.leftOuterJoin("departamento.listaCargodepartamento cargodepartamento")
				.leftOuterJoin("cargodepartamento.cargo cargo")
				.where("cargo = ?", filtro.getCargo())
				.ignoreJoin("cargodepartamento","cargo");
		}
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaCargodepartamento");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Departamento> query) {
		StringBuilder campos  = new StringBuilder();
		campos.append("departamento.cddepartamento, departamento.nome, departamento.descricao, ");
		campos.append("departamento.descricao, departamento.codigofolha, departamento.cdusuarioaltera,");
		campos.append("departamento.dtaltera,");
		campos.append("responsavel.nome, responsavel.cdpessoa, ");
		campos.append("cargo.nome, cargo.cdcargo, cargodepartamento.cdcargodepartamento");
		query
			.select(campos.toString())
			.leftOuterJoin("departamento.listaCargodepartamento cargodepartamento")
			.leftOuterJoin("cargodepartamento.cargo cargo")
			.leftOuterJoin("departamento.responsavel responsavel")
			.orderBy("cargo.nome");
	}

	public List<Departamento> findAllDepartamentos(Cargo cargo){
		return 
			query()
				.select("departamento.nome")
				.join("departamento.listaCargodepartamento departamentocargo")
				.where("departamentocargo.cargo = ?", cargo)
				.orderBy("departamento.nome")
				.list();
	}
	

	/* singleton */
	private static DepartamentoDAO instance;
	public static DepartamentoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(DepartamentoDAO.class);
		}
		return instance;
	}

	/**
	 * Carrega a lista de departamentos para exibi��o no flex
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Departamento> findAllForFlex() {
		return query()
					.select("departamento.cddepartamento, departamento.nome")
					.orderBy("departamento.nome")
					.list();
	}

	/**
	 * Busca o departamento pelo seu c�digo folha.
	 *
	 * @param codigofolha
	 * @return
	 * @since Jul 5, 2011
	 * @author Rodrigo Freitas
	 */
	public Departamento findByCodigofolha(String codigofolha) {
		if(codigofolha == null || codigofolha.equals("")){
			throw new SinedException("O c�digo folha n�o pode ser nulo.");
		}
		return query()
					.select("departamento.cddepartamento, departamento.nome")
					.where("departamento.codigofolha like ?", codigofolha)
					.unique();
	}
	
	/**
	 * M�todo autocomplete do departamento
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Departamento> findDepartamentoAutocomplete(String q) {
		return query().select(
				"departamento.cddepartamento, departamento.nome")
				.whereLikeIgnoreAll("departamento.nome", q)
				.list();
	}

	public Departamento retornarResponsavel(Integer dep) {
		return query()
				.select("departamento.cddepartamento,responsavel.cdpessoa, responsavel.nome")
				.leftOuterJoin("departamento.responsavel responsavel")
				.where("departamento.cddepartamento = ? ", dep)
				.unique();
		
	}

	public Departamento findByNome(String nome_departamento) {
		if(nome_departamento == null || nome_departamento.equals("")){
			throw new SinedException("O c�digo folha n�o pode ser nulo.");
		}
		return query()
					.select("departamento.cddepartamento, departamento.nome")
					.whereLikeIgnoreAll("departamento.nome", nome_departamento)
					.unique();
	}
	
}
