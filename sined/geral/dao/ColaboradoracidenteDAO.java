package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Colaboradoracidente;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColaboradoracidenteDAO extends GenericDAO<Colaboradoracidente> {

	/* singleton */
	private static ColaboradoracidenteDAO instance;
	public static ColaboradoracidenteDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradoracidenteDAO.class);
		}
		return instance;
	}
}