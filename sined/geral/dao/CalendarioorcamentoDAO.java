package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Calendario;
import br.com.linkcom.sined.geral.bean.Calendarioorcamento;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CalendarioorcamentoDAO extends GenericDAO<Calendarioorcamento> {

	/**
	 * Verifica se o or�amento est� presente em outro calend�rio.
	 *
	 * @param orcamento
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean verificaOutroCalendario(Orcamento orcamento, Calendario bean) {
		if(orcamento == null || orcamento.getCdorcamento() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
				.from(Calendarioorcamento.class)
				.select("count(*)")
				.join("calendarioorcamento.orcamento orcamento")
				.join("calendarioorcamento.calendario calendario")
				.where("orcamento = ?", orcamento);
		
		if(bean != null && bean.getCdcalendario() != null){
			query.where("calendario.cdcalendario <> ?", bean.getCdcalendario());
		}
		
		return query.unique() > 0;
	}
	

}
