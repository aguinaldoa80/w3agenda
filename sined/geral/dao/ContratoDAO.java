
package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Arquivoretornocontrato;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratohistorico;
import br.com.linkcom.sined.geral.bean.Contratotipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fatorajuste;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Indicecorrecao;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Romaneiosituacao;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Contratoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Formafaturamento;
import br.com.linkcom.sined.geral.bean.enumeration.Mes;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContagerencial;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.geral.service.ArquivoretornocontratoService;
import br.com.linkcom.sined.geral.service.ContratoarquivoService;
import br.com.linkcom.sined.geral.service.ContratohistoricoService;
import br.com.linkcom.sined.geral.service.ContratomaterialService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.TaxaService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ContratoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ArquivoRetornoContratoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ArquivoRetornoContratoItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.filter.ArquivoRetornoContratoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EmitirIndenizacaoContratoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EmitiracompanhamentometaFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.FluxocaixaFiltroReport;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PagamentoClienteServicoBean;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

@DefaultOrderBy("contrato.descricao")
public class ContratoDAO extends GenericDAO<Contrato> {

	private RateioService rateioService;
	private TaxaService taxaService;
	private ContratoarquivoService contratoarquivoService;
	private ContratohistoricoService contratohistoricoService;
	private ContratomaterialService contratomaterialService;
	private ArquivoretornocontratoService arquivoretornocontratoService;
	
	public void setContratoarquivoService(ContratoarquivoService contratoarquivoService) {this.contratoarquivoService = contratoarquivoService;}
	public void setTaxaService(TaxaService taxaService) {this.taxaService = taxaService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setContratohistoricoService(ContratohistoricoService contratohistoricoService) {this.contratohistoricoService = contratohistoricoService;}
	public void setContratomaterialService(ContratomaterialService contratomaterialService) {this.contratomaterialService = contratomaterialService;}
	public void setArquivoretornocontratoService(ArquivoretornocontratoService arquivoretornocontratoService) {this.arquivoretornocontratoService = arquivoretornocontratoService;}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Contrato> query, FiltroListagem _filtro) {
		ContratoFiltro filtro = (ContratoFiltro) _filtro;
		if(filtro.getContratos() != null && !filtro.getContratos().equals("")){
			while(filtro.getContratos().indexOf(",,") != -1){
				filtro.setContratos(filtro.getContratos().replaceAll(",,", ","));
			}
			
			if(filtro.getContratos().substring(0, 1).equals(",")){
				filtro.setContratos(filtro.getContratos().substring(1, filtro.getContratos().length()));
			}
			
			if(filtro.getContratos().substring(filtro.getContratos().length() - 1, filtro.getContratos().length()).equals(",")){
				filtro.setContratos(filtro.getContratos().substring(0, filtro.getContratos().length()-1));
			}
		}
		if(filtro.getIdentificador() != null && !filtro.getIdentificador().equals("")){
			while(filtro.getIdentificador().indexOf(",,") != -1){
				filtro.setIdentificador(filtro.getIdentificador().replaceAll(",,", ","));
			}
			
			if(filtro.getIdentificador().substring(0, 1).equals(",")){
				filtro.setIdentificador(filtro.getIdentificador().substring(1, filtro.getIdentificador().length()));
			}
			
			if(filtro.getIdentificador().substring(filtro.getIdentificador().length() - 1, filtro.getIdentificador().length()).equals(",")){
				filtro.setIdentificador(filtro.getIdentificador().substring(0, filtro.getIdentificador().length()-1));
			}
		}
		
		query
			.select("distinct new br.com.linkcom.sined.geral.bean.Contrato(contrato.cdcontrato, contrato.descricao, contrato.dtproximovencimento, " +
					"contrato.dtinicio, contrato.dtfim, cliente.nome, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
					"aux_contrato.situacao, contrato.identificador, coalesce(aux_contrato.identificadornumerico,contrato.cdcontrato))")
			.setUseTranslator(false)
			.join("contrato.cliente cliente")
			.join("contrato.empresa empresa")
			.join("contrato.aux_contrato aux_contrato")
			.where("cliente = ?", filtro.getClientenome())
			.where("cliente = ?", filtro.getClienterazaosocial())
			.where("cliente.cpf = ?", filtro.getCpf())
			.where("cliente.cnpj = ?", filtro.getCnpj())
			.where("cliente.tipopessoa = ?", filtro.getTipopessoa())
			.where("cliente.associacao = ?", filtro.getAssociacao())
			.where("empresa = ?", filtro.getEmpresa())
			.whereLikeIgnoreAll("contrato.descricao", filtro.getDescricao())
			.whereIn("contrato.cdcontrato", SinedUtil.montaWhereInInteger(filtro.getContratos()))
			.whereIn("contrato.identificador", SinedUtil.montaWhereInString(filtro.getIdentificador()))
			.where("contrato.dtinicio >= ?", filtro.getDtiniciode())
			.where("contrato.dtinicio <= ?", filtro.getDtinicioate())
			.where("contrato.dtfim >= ?", filtro.getDtfimde())
			.where("contrato.dtfim <= ?", filtro.getDtfimate())
			.where("contrato.dtproximovencimento >= ?", filtro.getDtproximovencimentode())
			.where("contrato.dtproximovencimento <= ?", filtro.getDtproximovencimentoate())
			.where("contrato.dtrenovacao >= ?", filtro.getDtrenovacaode())
			.where("contrato.dtrenovacao <= ?", filtro.getDtrenovacaoate())
			.where("contrato.dtsuspensao >= ?", filtro.getDtsuspensaode())
			.where("contrato.dtsuspensao <= ?", filtro.getDtsuspensaoate())
			.where("contrato.valor >= ?", filtro.getValorde())
			.where("contrato.valor <= ?", filtro.getValorate())
			.where("contrato.dtassinatura >= ?", filtro.getDtassinaturade())
			.where("contrato.dtassinatura <= ?", filtro.getDtassinaturaate())
			.where("contrato.formafaturamento = ?", filtro.getFormafaturamento())
			.where("contrato.frequencia = ?", filtro.getFrequencia())
			.where("contrato.fatorajuste = ?", filtro.getFatorajuste())
			.where("contrato.contratotipo = ?", filtro.getContratotipo())
			.where("contrato.vendedor = ?", filtro.getColaborador())
			.where("contrato.vendedor = ?", filtro.getFornecedor())
			.where("contrato.dtcancelamento >= ?", filtro.getDtcancelamentode())
			.where("contrato.dtcancelamento <= ?", filtro.getDtcancelamentoate());
			
		if(filtro.getCentrocusto() != null || filtro.getProjeto() != null || filtro.getContagerencial() != null){
			query
			.leftOuterJoin("contrato.rateio rateio")
			.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.where("listaRateioitem.centrocusto = ?", filtro.getCentrocusto())
				.where("listaRateioitem.projeto = ?", filtro.getProjeto())
				.where("listaRateioitem.contagerencial = ?", filtro.getContagerencial())
				.ignoreJoin("rateio", "listaRateioitem");
		}
		
		if(filtro.getMaterialgrupo() != null){
			query
				.leftOuterJoin("contrato.listaContratomaterial listaContratomaterial")
				.leftOuterJoin("listaContratomaterial.servico servico")
				.leftOuterJoin("listaContratomaterial.materialpromocional materialpromocional")
				.openParentheses()
					.where("servico.materialgrupo = ?", filtro.getMaterialgrupo())
					.or()
					.where("materialpromocional.materialgrupo =?", filtro.getMaterialgrupo())
				.closeParentheses();
		}
		
		if(filtro.getMaterial() != null){
			query
				.leftOuterJoin("contrato.listaContratomaterial listaContratomaterial")
				.where("listaContratomaterial.servico = ?", filtro.getMaterial())
				.ignoreJoin("listaContratomaterial");
		}
		
		
		if (filtro.getEnderecoStr()!=null && !filtro.getEnderecoStr().trim().equals("")){
			query
				.leftOuterJoin("contrato.endereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.whereLikeIgnoreAll("(coalesce(endereco.logradouro,'') || coalesce(endereco.numero,'') || coalesce(endereco.complemento,'') || " +
									 " coalesce(endereco.bairro,'') || coalesce(endereco.cep,'') || coalesce(endereco.pontoreferencia,'') || " +
									 " coalesce(municipio.nome,'') || coalesce(uf.sigla,''))", filtro.getEnderecoStr());
		}
		if (filtro.getEnderecoentregaStr()!=null && !filtro.getEnderecoentregaStr().trim().equals("")){
			query
				.leftOuterJoin("contrato.enderecoentrega enderecoentrega")
				.leftOuterJoin("enderecoentrega.municipio municipioentrega")
				.leftOuterJoin("municipioentrega.uf ufentrega")
				.whereLikeIgnoreAll("(coalesce(enderecoentrega.logradouro,'') || coalesce(enderecoentrega.numero,'') || coalesce(enderecoentrega.complemento,'') || " +
									 " coalesce(enderecoentrega.bairro,'') || coalesce(enderecoentrega.cep,'') || coalesce(enderecoentrega.pontoreferencia,'') || " +
									 " coalesce(municipioentrega.nome,'') || coalesce(ufentrega.sigla,''))", filtro.getEnderecoentregaStr());
		}
		
		List<SituacaoContrato> listaSituacao = filtro.getListaSituacao();
		if (listaSituacao != null && !listaSituacao.isEmpty()) {
			String list = "";
			for (int i = 0; i < listaSituacao.size(); i++) {
				Object o = listaSituacao.get(i);
				SituacaoContrato situacaoContrato = null;
				if (o instanceof String) {
					situacaoContrato = SituacaoContrato.valueOf((String)o);
				}else{
					situacaoContrato = (SituacaoContrato)o;
				}
				
				list += situacaoContrato.getValue() + ",";
			}
			query.whereIn("aux_contrato.situacao", list.substring(0, (list.length()-1) ));
		}
		
		query.orderBy("contrato.cdcontrato desc, contrato.identificador desc");
	}
	
	/**
	 * Carrega a listagem de contrato com todas as listas preenchidas.
	 *
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contrato> loadWithLista(String whereIn, String orderBy, boolean asc) {
		if(whereIn == null || whereIn.equals("")) return new ArrayList<Contrato>();
		
        QueryBuilder<Contrato> query = query()
                .select("contrato.cdcontrato, contrato.identificador, cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj, contrato.descricao, " +
                        "contrato.dtproximovencimento, contrato.dtinicio, contrato.dtfim, empresa.nome, empresa.razaosocial, empresa.nomefantasia, aux_contrato.situacao, contrato.valor, " +
                        "contrato.inss, contrato.incideinss, contrato.incideir, contrato.ir, contrato.incideiss, contrato.iss, " +
                        "contrato.incideicms, contrato.icms, contrato.incidepis, contrato.pis, contrato.incidecofins, contrato.cofins, " +
                        "contrato.incidecsll, contrato.csll, contratodesconto.valor, contratodesconto.dtinicio, contratodesconto.dtfim, " +
                        "contratoparcela.dtvencimento, contratoparcela.valor, responsavel.nome, contrato.historico, contrato.anotacao, " +
                        "conta.nome, contrato.qtde, fatorajuste.cdfatorajuste, fatorajustevalor.cdfatorajustevalor, fatorajustevalor.valor, " +
                        "fatorajustevalor.mesano, contrato.formafaturamento, contrato.ignoravalormaterial, contrato.valoracrescimo," +
                        "contrato.dtrenovacao")
                .leftOuterJoin("contrato.cliente cliente")
                .leftOuterJoin("contrato.empresa empresa")
                .leftOuterJoin("contrato.aux_contrato aux_contrato")
                .leftOuterJoin("contrato.listaContratoparcela contratoparcela")
                .leftOuterJoin("contrato.listaContratodesconto contratodesconto")
                .leftOuterJoin("contrato.responsavel responsavel")
                .leftOuterJoin("contrato.conta conta")
                .leftOuterJoin("contrato.fatorajuste fatorajuste")
                .leftOuterJoin("fatorajuste.listaFatorajustevalor fatorajustevalor");
		
		SinedUtil.quebraWhereIn("contrato.cdcontrato", whereIn, query);
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("contrato.cdcontrato DESC");
		}
		
		return query.list();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Contrato> query) {
		// N�O COLOCAR AQUI AS LISTAS DE DESCONTO E HIST�RICO PARA CARREGAR AQUI.
		// CASO PRECISE DELAS CARREGAR MANUALMENTE DEPOIS DESSA QUERY
		
		query
			.select("contrato.cdcontrato, contrato.identificador, contrato.descricao, contrato.dtinicio, contrato.dtfim, contrato.dtsuspensao, contrato.historico, " +
					"contrato.formafaturamento, contrato.anotacao, contrato.dtproximovencimento, contrato.mes, contrato.ano, contrato.valor, " +
					"contrato.anotacaocorpo, contrato.inscricaoestadual, contrato.ir, contrato.incideir, contrato.iss, contrato.incideiss, " +
					"contrato.pis, contrato.incidepis, contrato.cofins, contrato.incidecofins, contrato.csll, contrato.incidecsll, " +
					"contrato.inss, contrato.incideinss, contrato.icms, contrato.incideicms, contrato.codigogps, contrato.cdusuarioaltera, " +
					"contrato.dtaltera, contrato.nomefantasia, contrato.dtconfirma, contrato.dtconclusao, contrato.faturamentolote, contrato.qtde, contrato.urlsistema, " +
					"contrato.vendedortipo, contrato.valordedutivocomissionamento, contrato.valoracrescimo, contrato.acrescimoproximofaturamento, " +
					"contratotipo.cdcontratotipo, contratotipo.nome, contratotipo.locacao, contratotipo.faturarhoratrabalhada, contratotipo.atualizarProximoVencimentoConfirmar, contratotipo.faturamentoporitensdevolvidos, " +
					"cliente.cdpessoa, cliente.nome, cliente.tipopessoa, cliente.crt, cliente.incidiriss, cliente.inscricaomunicipal, contrato.dtcancelamento, " +
					"contrato.dtassinatura, contrato.dtrenovacao, contrato.isento, contrato.ignoravalormaterial, contrato.dtiniciolocacao, contrato.dtfimlocacao, contrato.finalizarcontratofinalizarpagamentos, " +
					"empresa.cdpessoa, empresa.tributacaomunicipiocliente, " +
					"documentotipo.cddocumentotipo, documentotipo.nome, " +
					"regimetributacao.cdregimetributacao, naturezaoperacao.cdnaturezaoperacao, regimetributacao.codigonfse, naturezaoperacao.codigonfse, " +
					"municipioempresa.cdmunicipio, municipioempresa.cdibge, " +
					"conta.cdconta, contacarteira.cdcontacarteira, contatipo.cdcontatipo, " + //conta.msgboleto1, conta.msgboleto2, " +
					"vendedor.cdpessoa, vendedor.nome, " +
					"responsavel.cdpessoa, responsavel.nome, " +
					"frequencia.cdfrequencia, frequenciarenovacao.cdfrequencia, contrato.dataparceladiautil, contrato.dataparcelaultimodiautilmes, " +
					"listaContratoparcela.desconto, " +
//					"listaContratocolaborador.cdcontratocolaborador, listaContratocolaborador.dtinicio, listaContratocolaborador.dtfim, listaContratocolaborador.ordemcomissao, " +
//					"listaContratocolaborador.valorhora, colaborador.cdpessoa, colaborador.nome, " +
//					"escala.cdescala, " +
					"rateio.cdrateio, " +
					"taxa.cdtaxa, " +
//					"listaContratocolaborador.horainicio, listaContratocolaborador.horafim, " +
					"aux_contrato.situacao, " +
					"listaContratomaterial.cdcontratomaterial, listaContratomaterial.dtinicio, listaContratomaterial.dtfim, listaContratomaterial.valorunitario, " +
					"listaContratomaterial.qtde, listaContratomaterial.periodocobranca, listaContratomaterial.valordesconto, listaContratomaterial.hrinicio, listaContratomaterial.hrfim, " +
					"listaContratomaterial.detalhamento, listaContratomaterial.valorfechado, listaContratomaterial.observacao," +
					"indice.cdindice, " +
					"materialpromocional.cdmaterial, materialpromocional.nome, " +
					"servico.cdmaterial, servico.nome, servico.servico, servico.materialtipo, servico.tributacaoestadual, servico.produto, servico.tipoitemsped, " +
					"servico.codigobarras, servico.ncmcompleto, servico.extipi, servico.patrimonio, servico.nve, servico.tributacaomunicipal, materialgrupo.cdmaterialgrupo, " +
					"patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, " +
					"unidademedidaservico.simbolo, unidademedidaservico.cdunidademedida, " +
					"ncmcapitulo.cdncmcapitulo, ncmcapitulo.descricao, " +
					"prazopagamento.cdprazopagamento, " +
					"indicecorrecao.cdindicecorrecao, " +
					"listaContratoparcela.cdcontratoparcela, listaContratoparcela.dtvencimento, listaContratoparcela.valor, listaContratoparcela.aplicarindice, " +
					"listaContratoparcela.cobrada, listaContratoparcela.valornfservico, listaContratoparcela.valornfproduto, " +
					"endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro,  endereco.cep, " +
					"municipio.cdmunicipio, municipio.nome, municipio.cdibge, " +
					"uf.cduf, uf.sigla," +
					"enderecotipo.cdenderecotipo, " +
					"enderecocliente.cdendereco, municipiocliente.cdmunicipio, ufcliente.cduf, " +
					
					"enderecoentrega.cdendereco, enderecoentrega.logradouro, enderecoentrega.numero, enderecoentrega.complemento, " +
					"enderecoentrega.bairro,  enderecoentrega.cep, " +
					"municipioentrega.cdmunicipio, municipioentrega.nome, municipioentrega.cdibge, " +
					"ufentrega.cduf, ufentrega.sigla," +
					"enderecotipoentrega.cdenderecotipo, " +
					
					"enderecocobranca.cdendereco, enderecocobranca.logradouro, enderecocobranca.numero, enderecocobranca.complemento, " +
					"enderecocobranca.bairro,  enderecocobranca.cep, " +
					"municipiocobranca.cdmunicipio, municipiocobranca.nome, municipiocobranca.cdibge, " +
					"ufcobranca.cduf, ufcobranca.sigla," +
					"enderecotipocobranca.cdenderecotipo, " +
					
					"contacontabil.cdcontacontabil, contacontabil.nome, " +
					"grupotributacao.cdgrupotributacao, grupotributacao.nome, grupotributacao.tributacaomunicipiocliente, grupotributacao.tributacaomunicipioprojeto, grupotributacao.infoadicionalfisco, grupotributacao.infoadicionalcontrib, " +
					"codigotributacao.cdcodigotributacao, codigotributacao.codigo, codigotributacao.descricao, " +
					"itemlistaservicoBean.cditemlistaservico, itemlistaservicoBean.codigo, itemlistaservicoBean.descricao, " +
					"codigocnaeBean.cdcodigocnae, codigocnaeBean.cnae, codigocnaeBean.descricaocnae, " +
					
					"listaContratoservico.cdcontratoservico, listaContratoservico.identificador, listaContratoservico.dtvalidade,contratoservicostatusContratoServico.cdcontratoservicostatus,materialContratoServico.cdmaterial," +
					
					"enderecotipocliente.cdenderecotipo, " +
					"comissionamento.cdcomissionamento, comissionamento.nome, comissionamento.valor, comissionamento.percentual, comissionamento.quantidade, comissionamento.deduzirvalor, " +
//					"comissao.cdcomissionamento, comissao.nome, comissao.valor, comissao.percentual, comissao.quantidade, " +
					"listaContratoarquivo.cdcontratoarquivo, listaContratoarquivo.descricao, listaContratoarquivo.dtarquivo, " +
					"arquivo.cdarquivo, arquivo.nome, " +
					"contagerencialvenda.cdcontagerencial, contagerencialvenda.nome, vcontagerencialvenda.identificador, vcontagerencialvenda.arvorepai, " +
					"centrocustovenda.cdcentrocusto," +
					"fatorajuste.cdfatorajuste, fatorajuste.sigla, contato.cdpessoa, contato.nome, " +
					"contrato.valortotalfechado, contrato.aliquotavalortotalfechado, contrato.valorhoraextra," +
					"materialContratoTipo.cdmaterial, materialContratoTipo.nome, " +
					
					"codigotributacao_grupotributacao.cdcodigotributacao, codigotributacao_grupotributacao.codigo," +
					"itemlistaservico_grupotributacao.cditemlistaservico, itemlistaservico_grupotributacao.codigo," +
					"codigocnae_grupotributacao.cdcodigocnae, codigocnae_grupotributacao.cnae, contrato.reajuste, contrato.percentualreajuste, " +
					"vendaorcamento.cdvendaorcamento "
					)
					
			.leftOuterJoin("contrato.rateio rateio")
			.leftOuterJoin("contrato.cliente cliente")
			.leftOuterJoin("contrato.contato contato")
			
			.leftOuterJoin("contrato.vendaorcamento vendaorcamento")
			
			.leftOuterJoin("contrato.taxa taxa")
			.leftOuterJoin("contrato.contratotipo contratotipo")			
			.leftOuterJoin("contrato.documentotipo documentotipo")			
			.leftOuterJoin("contrato.aux_contrato aux_contrato")
			.leftOuterJoin("contrato.conta conta")
			.leftOuterJoin("contrato.contacarteira contacarteira")
			.leftOuterJoin("contrato.vendedor vendedor")
			.leftOuterJoin("contrato.comissionamento comissionamento")
			.leftOuterJoin("contrato.responsavel responsavel")
			.leftOuterJoin("contrato.frequencia frequencia")
			.leftOuterJoin("contrato.frequenciarenovacao frequenciarenovacao")
			
			.leftOuterJoin("contrato.empresa empresa")
			.leftOuterJoin("empresa.naturezaoperacao naturezaoperacao")
			.leftOuterJoin("empresa.regimetributacao regimetributacao")
			.leftOuterJoin("empresa.listaEndereco enderecoempresa")
			.leftOuterJoin("enderecoempresa.municipio municipioempresa")
			
			.leftOuterJoin("cliente.listaEndereco enderecocliente")
			.leftOuterJoin("enderecocliente.municipio municipiocliente")
			.leftOuterJoin("municipiocliente.uf ufcliente")
			.leftOuterJoin("enderecocliente.enderecotipo enderecotipocliente")
			
			.leftOuterJoin("contrato.endereco endereco")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("endereco.enderecotipo enderecotipo")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("contrato.enderecoentrega enderecoentrega")
			.leftOuterJoin("enderecoentrega.municipio municipioentrega")
			.leftOuterJoin("enderecoentrega.enderecotipo enderecotipoentrega")
			.leftOuterJoin("municipioentrega.uf ufentrega")
			.leftOuterJoin("contrato.enderecocobranca enderecocobranca")
			.leftOuterJoin("enderecocobranca.municipio municipiocobranca")
			.leftOuterJoin("enderecocobranca.enderecotipo enderecotipocobranca")
			.leftOuterJoin("municipiocobranca.uf ufcobranca")
			
			.leftOuterJoin("contrato.listaContratoarquivo listaContratoarquivo")
			.leftOuterJoin("listaContratoarquivo.arquivo arquivo")
			
			.leftOuterJoin("contrato.listaContratoservico listaContratoservico")
			.leftOuterJoin("listaContratoservico.material materialContratoServico")
			.leftOuterJoin("listaContratoservico.contratoservicostatus contratoservicostatusContratoServico")
			
//			.leftOuterJoin("contrato.listaContratocolaborador listaContratocolaborador")
//			.leftOuterJoin("listaContratocolaborador.colaborador colaborador")
//			.leftOuterJoin("listaContratocolaborador.escala escala")
//			.leftOuterJoin("listaContratocolaborador.comissionamento comissao")
			
			.leftOuterJoin("contrato.listaContratomaterial listaContratomaterial")
			.leftOuterJoin("listaContratomaterial.patrimonioitem patrimonioitem ")
			.leftOuterJoin("listaContratomaterial.indice indice ")
			.leftOuterJoin("listaContratomaterial.materialpromocional materialpromocional ")
			.leftOuterJoin("listaContratomaterial.servico servico ")
			.leftOuterJoin("servico.materialtipo materialtipo")
			.leftOuterJoin("servico.materialgrupo materialgrupo")
			.leftOuterJoin("servico.contagerencialvenda contagerencialvenda")
			.leftOuterJoin("contagerencialvenda.vcontagerencial vcontagerencialvenda")
			.leftOuterJoin("servico.centrocustovenda centrocustovenda")
			.leftOuterJoin("servico.unidademedida unidademedidaservico")
			.leftOuterJoin("servico.ncmcapitulo ncmcapitulo")
			
			.leftOuterJoin("contrato.grupotributacao grupotributacao")
			.leftOuterJoin("contrato.codigotributacao codigotributacao")
			.leftOuterJoin("contrato.itemlistaservicoBean itemlistaservicoBean")
			.leftOuterJoin("contrato.codigocnaeBean codigocnaeBean")
			
			.leftOuterJoin("grupotributacao.codigotributacao codigotributacao_grupotributacao")
			.leftOuterJoin("grupotributacao.itemlistaservico itemlistaservico_grupotributacao")
			.leftOuterJoin("grupotributacao.codigocnae codigocnae_grupotributacao")
			.leftOuterJoin("contrato.contaContabil contacontabil")
			
			.leftOuterJoin("contrato.prazopagamento prazopagamento")
			.leftOuterJoin("contrato.indicecorrecao indicecorrecao")
			.leftOuterJoin("contrato.listaContratoparcela listaContratoparcela")
			.leftOuterJoin("contrato.fatorajuste fatorajuste")
			
			.leftOuterJoin("contratotipo.material materialContratoTipo")
			.leftOuterJoin("conta.contatipo contatipo")
			
			.orderBy("listaContratoparcela.dtvencimento, listaContratomaterial.cdcontratomaterial");
		
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Contrato contrato = (Contrato)save.getEntity();
		
		contratoarquivoService.saveArquivos(contrato);
		save
			.saveOrUpdateManaged("listaContratocolaborador")
			.saveOrUpdateManaged("listaContratohistorico")
			.saveOrUpdateManaged("listaContratomaterial")
			.saveOrUpdateManaged("listaContratoparcela")
			.saveOrUpdateManaged("listaContratodesconto")
			.saveOrUpdateManaged("listaContratoservico")
			.saveOrUpdateManaged("listaContratoarquivo");
		
		if(contrato.getRateio() != null){
			rateioService.saveOrUpdateNoUseTransaction(contrato.getRateio());
		}
		
		if(contrato.getTaxa() != null){
			taxaService.saveOrUpdateNoUseTransaction(contrato.getTaxa());
		}
	}
	
	/**
	 * Procedure que atualiza a tabela auxiliar de contrato. 
	 *
	 * @author Rodrigo Freitas
	 */
	public void updateAux() {
		getJdbcTemplate().execute("SELECT ATUALIZA_CONTRATO()");
	}
	
	/**
	 * Procedure que atualiza a tabela auxiliar de contrato. 
	 * @author Taidson
	 * @since 07/06/2010
	 */
	public void updateAux(Integer cdcontrato) {
		getJdbcTemplate().execute("SELECT ATUALIZA_CONTRATO("+cdcontrato+")");
	}
	
	
	/**
	 * Carrega informa��es para a gera��o de cobran�as.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contrato> findForCobranca(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		
		QueryBuilderSined<Contrato> query = querySined();
		this.updateEntradaQuery(query);
		return query
//				.select("contrato.cdcontrato, contrato.formafaturamento, contrato.dtproximovencimento, contrato.dataparceladiautil, contrato.dtinicio, " +
//				"contrato.dataparcelaultimodiautilmes, contrato.qtde, contratotipo.faturarhoratrabalhada, contrato.valorhoraextra, contrato.valor, " +
//				"contrato.nomefantasia, contrato.mes, contrato.ano, contrato.descricao, " +
//							"rateio.cdrateio, rateioitem.cdrateioitem, taxa.cdtaxa, grupotributacao.cdgrupotributacao, frequencia.cdfrequencia, " +
//							"fatorajuste.cdfatorajuste,  cliente.nome, indicecorrecao.cdindicecorrecao" +
//							"empresa.cdpessoa, conta.cdconta, contacarteira.cdcontacarteira")
//					.select(query.getSelect().getValue() + ", " +
//  							 "listaContratodesconto.cdcontratodesconto, listaContratodesconto.valor, " +
//							 "listaContratodesconto.dtinicio, listaContratodesconto.dtfim, " +
//							 "listaContratodesconto.tipodesconto, listaContratodesconto.tiponota," +
//							 "faturamentocontrato.cdfaturamentocontrato, contrato.identificador")
//					.leftOuterJoin("contrato.listaContratodesconto listaContratodesconto")
//					.leftOuterJoin("contrato.listaNotacontrato listaNotacontrato")
//					.leftOuterJoin("listaNotacontrato.faturamentocontrato faturamentocontrato")
//					.leftOuterJoin("contrato.empresa empresa")
//					.leftOuterJoin("contrato.frequencia frequencia")
//					.leftOuterJoin("contrato.grupotributacao grupotributacao")
//					.leftOuterJoin("contrato.taxa taxa")
//					.leftOuterJoin("contrato.rateio rateio")
//					.leftOuterJoin("rateio.listaRateioitem rateioitem")
//					.leftOuterJoin("contrato.fatorajuste fatorajuste")
//					.leftOuterJoin("contrato.contratotipo contratotipo")
//					.leftOuterJoin("contrato.endereco enderecofaturamento")
//					.leftOuterJoin("contrato.enderecoentrega enderecoentrega")
//					.leftOuterJoin("contrato.enderecocobranca enderecocobranca")
//					.leftOuterJoin("contrato.cliente cliente")
//					.leftOuterJoin("contrato.empresa empresa")
//					.leftOuterJoin("contrato.conta conta")
//					.leftOuterJoin("contrato.indicecorrecao indicecorrecao")
//					.leftOuterJoin("contrato.contacarteira contacarteira")
//					.leftOuterJoin("cliente.listaEndereco enderecoCliente")
					.whereIn("contrato.cdcontrato", whereIn)
					.orderBy("upper(retira_acento(cliente.nome))")
					.list();
	}
	
	/**
	 * Carrega informa��es para a gera��o de cobran�as atrav�s do n�mero do conv�nio fornecido.
	 *
	 * @param identificador
	 * @return
	 * @author Rafael Salvio
	 */
	public Contrato findForCobrancaByIdentificador(String identificador) {
		if(identificador == null || identificador.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		
		QueryBuilderSined<Contrato> query = querySined();
		this.updateEntradaQuery(query);
		return query
					.select(query.getSelect().getValue())
					.whereIn("contrato.identificador", SinedUtil.montaWhereInString(identificador))
					.orderBy("upper(retira_acento(cliente.nome))")
					.unique();
	}
	
	/**
	 * Realiza o update nos valores do contrato
	**/
	public void updateValoreContrato(Contrato contrato) {
		
		if(contrato == null || contrato.getCdcontrato() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		
		if(contrato.getValor() != null){
			getHibernateTemplate().bulkUpdate("update Contrato c set c.valor = ? where c.id = ?",new Object[]{contrato.getValor(), contrato.getCdcontrato()});
		}
	}
	
	/**
	 * Verifica se tem algum contrato nas situa��es passadas por par�metro.
	 *
	 * @param situacoes
	 * @return
	 * @author Rodrigo Freitas
	 * @param whereIn 
	 */
	public boolean haveContratoSituacao(String whereIn, SituacaoContrato[] situacoes) {
		
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < situacoes.length; i++) {
			sb.append(situacoes[i].getValue());
			if((i + 1) < situacoes.length){
				sb.append(",");
			}
		}
		
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Contrato.class)
					.join("contrato.aux_contrato aux_contrato")
					.whereIn("aux_contrato.situacao", sb.toString())
					.whereIn("contrato.cdcontrato", whereIn)
					.unique() > 0;
	}
	
	/**
	 * Carrega informa��es para o relat�rio de fluxo de caixa.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contrato> findForReportFluxocaixa(FluxocaixaFiltroReport filtro) {
		List<ItemDetalhe> listaProjeto = filtro.getListaProjeto();
		List<ItemDetalhe> listaCentrocusto = filtro.getListaCentrocusto();
		List<ItemDetalhe> listaProjetotipo = filtro.getListaProjetotipo();
		List<ItemDetalhe> listaEmpresa = filtro.getListaEmpresa();
		List<ItemDetalhe> listaCliente = filtro.getListaCliente();
		
		if(!Util.booleans.isTrue(filtro.getContratos()) || filtro.getRadContbanc() == null){
			return new ArrayList<Contrato>();
		}
		
		QueryBuilderSined<Contrato> q = querySined();
			q
			.select("contrato.cdcontrato,contrato.identificador,contrato.descricao,contrato.valor, contrato.valoracrescimo,contrato.dtfim," +
					"contrato.dtproximovencimento, cliente.cdpessoa, cliente.nome, contratotipo.cdcontratotipo, contratotipo.medicao, " +
					"frequencia.cdfrequencia, conta.cdconta, contatipo.cdcontatipo, contrato.qtde, " +
					"contrato.ir, contrato.incideir, contrato.incideiss, contrato.iss, " +
					"contrato.inss, contrato.incideinss, contrato.icms, contrato.incideicms, " +
					"contrato.pis, contrato.incidepis, contrato.csll, contrato.incidecsll, " +
					"contrato.cofins, contrato.incidecofins, listaContratoparcela.cdcontratoparcela, listaContratoparcela.cobrada, " +
					"listaContratoparcela.valor, listaContratoparcela.dtvencimento, rateioitem.valor, rateioitem.percentual, " +
					"contagerencial.cdcontagerencial, contagerencial.nome, " +
					"listaContratodesconto.valor, listaContratodesconto.dtinicio, listaContratodesconto.dtfim," +
					"contagerencial.nome, centrocusto.cdcentrocusto, centrocusto.nome")
			
			.leftOuterJoin("contrato.empresa empresa")
			.leftOuterJoin("contrato.cliente cliente")
			.leftOuterJoin("contrato.conta conta")
			.leftOuterJoin("conta.contatipo contatipo")
			.leftOuterJoin("contrato.frequencia frequencia")
			.leftOuterJoin("contrato.contratotipo contratotipo")
			.join("contrato.aux_contrato aux_contrato")
			
			.leftOuterJoin("contrato.rateio rateio")
			.leftOuterJoin("rateio.listaRateioitem rateioitem")
			.leftOuterJoin("rateioitem.contagerencial contagerencial")
			.leftOuterJoin("rateioitem.projeto projeto")
			.leftOuterJoin("projeto.projetotipo projetotipo")
			.leftOuterJoin("rateioitem.centrocusto centrocusto")
			
			.leftOuterJoin("contrato.listaContratoparcela listaContratoparcela")
			.leftOuterJoin("contrato.listaContratodesconto listaContratodesconto")
			;
		
		if(filtro.getMostrarContasInativadas() == null || !filtro.getMostrarContasInativadas()){
			q.openParentheses();
				q.where("conta is null");
				q.or();
				q.openParentheses();
					q.openParentheses();
						q.where("contatipo = ?", Contatipo.TIPO_CONTA_BANCARIA);
						q.where("conta.ativo = true");
					q.closeParentheses();
					q.or();
					q.where("contatipo is null");
					q.or();
					q.where("contatipo <> ?", Contatipo.TIPO_CONTA_BANCARIA);
				q.closeParentheses();
			q.closeParentheses();
		}
		
		q.openParentheses();
			q.where("contrato.dtfim is not null and contrato.dtfim >= ?",filtro.getPeriodoDe());
			q.or();
			q.where("contrato.dtfim is null");
		q.closeParentheses();
		q.where("contrato.dtproximovencimento <= ?",filtro.getPeriodoAte());
		
		// Os agendamentos consolidados ou finalizados n�o entram no fluxo de caixa
		q.where("aux_contrato.situacao <> ?", SituacaoContrato.FINALIZADO);
		q.where("aux_contrato.situacao <> ?", SituacaoContrato.CANCELADO);
		q.where("aux_contrato.situacao <> ?", SituacaoContrato.EM_ESPERA);
		q.where("aux_contrato.situacao <> ?", SituacaoContrato.ISENTO);
		q.where("aux_contrato.situacao <> ?", SituacaoContrato.SUSPENSO);
			
		
		// WHEREIN LISTA DE CENTRO DE CUSTO
		if(SinedUtil.isListNotEmpty(listaCentrocusto)){
			q.whereIn("centrocusto.cdcentrocusto",CollectionsUtil.listAndConcatenate(listaCentrocusto, "centrocusto.cdcentrocusto", ","));
		}
		
		// WHEREIN LISTA DE TIPO DE PROJETO
		if (listaProjetotipo != null && listaProjetotipo.size() > 0) {
			q.whereIn("projetotipo.cdprojetotipo", CollectionsUtil.listAndConcatenate(listaProjetotipo, "projetotipo.cdprojetotipo", ","));
		}
		
		// WHERE DE PROJETO
		String idProjeto = String.valueOf(filtro.getRadProjeto().getId());
		if(idProjeto.equals("escolherProjeto") && SinedUtil.isListNotEmpty(listaProjeto)){
			q.whereIn("projeto.cdprojeto", CollectionsUtil.listAndConcatenate(listaProjeto, "projeto.cdprojeto", ","));
		}else if(idProjeto.equals("comProjeto")){
			q.where("projeto is not null");
		}else if(filtro.getRadProjeto().getId().equals("semProjeto")){
			q.where("projeto is null");
		}
		
		// WHEREIN LISTA DE EMPRESAS
		if(listaEmpresa != null && !listaEmpresa.isEmpty()){
			q.whereIn("empresa.cdpessoa",CollectionsUtil.listAndConcatenate(listaEmpresa, "empresa.cdpessoa", ","));
		} else {
			q.openParentheses();
			q.whereIn("empresa.cdpessoa",new SinedUtil().getListaEmpresa());
			q.or();
			q.where("empresa is null");
			q.closeParentheses();
		}
					
		
		// WHEREIN NA LISTA DE CLIENTE			
		if(listaCliente != null && listaCliente.size() > 0){
			q.whereIn("cliente.cdpessoa",CollectionsUtil.listAndConcatenate(listaCliente, "cliente.cdpessoa", ","));
		}
			
		// WHEREIN LISTA DE CONTA BANC�RIA
		List<ItemDetalhe> listaContabancaria = filtro.getListaContabancaria();
		if(Util.booleans.isFalse(filtro.getRadContbanc()) && listaContabancaria != null && listaContabancaria.size() > 0){
			// Op��o 'Escolher'
			q.whereIn("conta.cdconta", CollectionsUtil.listAndConcatenate(listaContabancaria, "conta.cdconta", ","));
		}else{
			// Op��o 'Todas'
			q.where("conta.contatipo.cdcontatipo = " + Contatipo.CONTA_BANCARIA);
		}
		
//		WHEREIN PARA A LISTA DE NATUREZA DA CONTA GERENCIAL
		q.whereIn("contagerencial.natureza", NaturezaContagerencial.listAndConcatenate(filtro.getListanaturezaContaGerencial()));
		
		q.orderBy("contrato.dtproximovencimento");
		
		return q.list();
	}
	
	public List<Contrato> findByClienteAbertosApontamento(Cliente cliente, Integer cdapontamento) {
		if(cliente == null || cliente.getCdpessoa() == null){
			throw new SinedException("Cliente n�o pode ser nulo.");
		}
		QueryBuilder<Contrato> query = query()
					.select("contrato.cdcontrato, contrato.descricao")
					.join("contrato.cliente cliente")
					.join("contrato.aux_contrato aux_contrato")
					.where("cliente = ?", cliente)
					.openParentheses()
					.openParentheses()
					.where("aux_contrato.situacao <> ?", SituacaoContrato.FINALIZADO)
					.where("aux_contrato.situacao <> ?", SituacaoContrato.CANCELADO)
					.closeParentheses();
		
		
		if(cdapontamento != null){
			query
				.or()
				.where("contrato.cdcontrato in (select c.cdcontrato from Apontamento a join a.contrato as c where a.cdapontamento = " + cdapontamento + ")");
		}
		
		query.closeParentheses()
				.orderBy("contrato.descricao");
		
		return query.list();
	}
	
	public List<Contrato> findByClienteEmpresaAbertosApontamento(Cliente cliente, Empresa empresa) {
		if((cliente == null || cliente.getCdpessoa() == null) && (empresa == null || empresa.getCdpessoa() == null)){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
					.select("contrato.cdcontrato, contrato.descricao")
					.join("contrato.cliente cliente")
					.leftOuterJoin("contrato.empresa empresa")
					.join("contrato.aux_contrato aux_contrato")
					.where("cliente = ?", cliente)
					.where("empresa = ?", empresa)
					.openParentheses()
					.where("aux_contrato.situacao <> ?", SituacaoContrato.FINALIZADO)
					.where("aux_contrato.situacao <> ?", SituacaoContrato.CANCELADO)
					.closeParentheses()
					.orderBy("contrato.descricao")
					.list();
	}
	
	/**
	 * M�todo que retorna a data do �ltimo contrato cadastrado no sistema.
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Date getDtUltimoCadastro() {
		return newQueryBuilderSined(Date.class)
			.from(Contrato.class)
			.setUseTranslator(false) 
			.select("max(contrato.dtinicio)")
			.unique();
	}
	
	/**
	 * M�todo que retorna a qtde. total de contratos atrasados no sistema.
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Integer getQtdeAtrasada() {
		return newQueryBuilderSined(Long.class)
			
			.from(Contrato.class)
			.setUseTranslator(false) 
			.select("count(*)")
			.join("contrato.aux_contrato aux_contrato")
			.where("aux_contrato.situacao = ?", SituacaoContrato.ATRASADO)
			.unique()
			.intValue();
	}
	
	/**
	 * Retorna a quantidade de contratos 'A Consolidar'
	 *
	 * @return
	 * @since 24/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdeAFaturar() {
		return newQueryBuilderSined(Long.class)
		
		.from(Contrato.class)
		.setUseTranslator(false) 
		.select("count(*)")
		.join("contrato.aux_contrato aux_contrato")
		.where("aux_contrato.situacao = ?", SituacaoContrato.A_CONSOLIDAR)
		.unique()
		.intValue();
	}
	
	/**
	 * M�todo que retorna a qtde. total de contratos cadastrados no sistema.
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Integer getQtdeTotalContratos() {
		return newQueryBuilderSined(Long.class)
			
			.from(Contrato.class)
			.setUseTranslator(false) 
			.select("count(*)")
			.unique()
			.intValue();
	}
	
	/**
	 * M�todo que finaliza os contratos de frequ�ncia �nica cobrados.
	 * Obs: este m�todo � chamado toda vez que � logado um usu�rio
	 * 
	 * @author Tom�s Rabelo
	 */
	public void atualizaDataFimContratos() {
		getJdbcTemplate().update("update Aux_contrato set situacao = "+SituacaoContrato.FINALIZADO.getValue()+
								 " where cdcontrato in (select c.cdcontrato from Contrato c " +
								 "where c.dtfim < now() and c.cdfrequencia = "+Frequencia.UNICA+" and " +
								 "c.cdcontrato in (select ch.cdcontrato from Contratohistorico ch " +
								 "where ch.cdcontrato = c.cdcontrato) and " +
								 "c.cdcontrato in (select a.cdcontrato from Aux_contrato a where a.situacao = "+SituacaoContrato.FINALIZADO.getValue()+"))");
	}
	
	/**
	 * M�todo que busca todos os contratos para busca geral
	 * 
	 * @param busca
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Contrato> findContratosForBuscaGeral(String busca) {
		return query()
			.select("contrato.cdcontrato, contrato.descricao")
			.openParentheses()
				.whereLikeIgnoreAll("contrato.descricao", busca)
			.closeParentheses()
			.orderBy("contrato.descricao")
			.list();
	}	
	
	public Contrato findForEmissaoContratoRTF(Contrato contrato) {
		/*Parece pode existir alguma implementa��o err�nea e/ou mapeamento errado que 
		 *est� fazendo com que as cole��es do cliente sejam carregadas de forma incompleta. 
		 *Por este motivo e pelo fato de que somente ser� solicitado um contrato por vez, os joins da query 
		 *foram alterados para Fecth */
		
		return query()
//				.leftOuterJoinFetch("contrato.empresa empresa")
//				.leftOuterJoinFetch("empresa.logomarca logomarca")
				.leftOuterJoinFetch("contrato.frequencia frequencia")
				.leftOuterJoinFetch("contrato.frequenciarenovacao frequenciarenovacao")
//				.leftOuterJoinFetch("contrato.responsavel responsavel")
				.leftOuterJoinFetch("contrato.prazopagamento prazopagamento")
				
//				.leftOuterJoinFetch("contrato.cliente cliente")
				.leftOuterJoinFetch("contrato.contato contato")
				.leftOuterJoinFetch("contrato.listaContratomaterial listaContratomaterial")
				.leftOuterJoinFetch("listaContratomaterial.servico servico")
				
//				.leftOuterJoinFetch("cliente.estadocivil estadocivil")
//				.leftOuterJoinFetch("cliente.clienteprofissao clienteprofissao")
//				.leftOuterJoinFetch("cliente.listaEndereco listaEndereco")
//				.leftOuterJoinFetch("listaEndereco.enderecotipo enderecotipo")
//				.leftOuterJoinFetch("listaEndereco.municipio municipio")
//				.leftOuterJoinFetch("municipio.uf uf")
//				
//				.leftOuterJoinFetch("cliente.listaTelefone telefone")
//				.leftOuterJoinFetch("telefone.telefonetipo telefonetipo")
				
				.leftOuterJoinFetch("contrato.codigocnaeBean codigocnaeBean")
				.leftOuterJoinFetch("contrato.indicecorrecao indicecorrecao")
				
				.leftOuterJoinFetch("contrato.endereco enderecocontrato")
				.leftOuterJoinFetch("enderecocontrato.enderecotipo enderecotipocontrato")
				.leftOuterJoinFetch("enderecocontrato.municipio municipiocontrato")
				.leftOuterJoinFetch("municipiocontrato.uf ufcontrato")
				.leftOuterJoinFetch("contrato.enderecoentrega enderecoentrega")
				.leftOuterJoinFetch("enderecoentrega.enderecotipo enderecotipoentrega")
				.leftOuterJoinFetch("enderecoentrega.municipio municipioentrega")
				.leftOuterJoinFetch("municipioentrega.uf ufentrega")
				
				.where("contrato = ?", contrato)
//				.orderBy("upper(retira_acento(cliente.nome))")
				.unique();
	}
	
	public Contrato carregaTodosCampos(String whereIn){
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		
		return
			query()
				.select("contrato.cdcontrato, contrato.identificador, cliente.cdpessoa, contrato.descricao, contrato.dtinicio, contrato.dtfim, " +
						"empresa.cdpessoa, conta.cdconta, vendedor.cdpessoa, responsavel.cdpessoa, contrato.historico, contrato.formafaturamento, " +
						"contrato.anotacao, contrato.dtproximovencimento, frequencia.cdfrequencia, " +
						"contrato.mes, contrato.ano, contrato.valor, contrato.valoracrescimo, contrato.anotacaocorpo, " +
						"contrato.ir, contrato.incideir, contrato.iss, contrato.incideiss, contrato.pis, contrato.incidepis, " +
						"contrato.cofins, contrato.incidecofins, contrato.csll, contrato.incidecsll, contrato.inss, contrato.incideinss, " +
						"contrato.icms, contrato.incideicms, contrato.codigogps, listaContratocolaborador.cdcontratocolaborador, " +
						"listaContratocolaborador.dtinicio, listaContratocolaborador.dtfim, colaborador.cdpessoa, escala.cdescala, " +
						"rateio.cdrateio, taxa.cdtaxa, contrato.cdusuarioaltera, contrato.dtaltera, " +
						"listaContratohistorico.dthistorico, listaContratohistorico.acao, usuario.nome, listaContratohistorico.observacao, " +
						"listaContratocolaborador.horainicio, listaContratocolaborador.horafim, contrato.faturamentolote, aux_contrato.situacao, " +
						"listaContratomaterial.cdcontratomaterial, listaContratomaterial.dtinicio, listaContratomaterial.dtfim, listaContratomaterial.valorunitario, " +
						"listaContratomaterial.qtde, materialpromocional.cdmaterial, materialpromocional.nome, servico.cdmaterial, servico.nome, " +
						"servico.materialtipo, prazopagamento.cdprazopagamento, indicecorrecao.cdindicecorrecao, listaContratoparcela.cdcontratoparcela, " +
						"listaContratoparcela.dtvencimento, listaContratoparcela.valor, listaContratoparcela.aplicarindice, listaContratoparcela.cobrada, " +
						"codigotributacao.cdcodigotributacao, codigotributacao.codigo, codigotributacao.descricao, " +
						"itemlistaservicoBean.cditemlistaservico, itemlistaservicoBean.codigo, itemlistaservicoBean.descricao, " +
						"codigocnaeBean.cdcodigocnae, codigocnaeBean.cnae, codigocnaeBean.descricaocnae")
				.join("contrato.cliente cliente")
				.join("contrato.aux_contrato aux_contrato")
				.join("contrato.empresa empresa")
				.join("contrato.conta conta")
				.leftOuterJoin("contrato.vendedor vendedor")
				.join("contrato.responsavel responsavel")
				.leftOuterJoin("contrato.frequencia frequencia")
				.leftOuterJoin("contrato.rateio rateio")
				.leftOuterJoin("contrato.taxa taxa")
				
				.leftOuterJoin("contrato.listaContratocolaborador listaContratocolaborador")
				.leftOuterJoin("listaContratocolaborador.colaborador colaborador")
				.leftOuterJoin("listaContratocolaborador.escala escala")
				
				.leftOuterJoin("contrato.listaContratomaterial listaContratomaterial")
				.leftOuterJoin("listaContratomaterial.materialpromocional materialpromocional ")
				.leftOuterJoin("listaContratomaterial.servico servico ")
				.leftOuterJoin("servico.materialtipo materialtipo")
				
				.leftOuterJoin("contrato.listaContratohistorico listaContratohistorico")
				.leftOuterJoin("listaContratohistorico.usuario usuario")
				
				.leftOuterJoin("contrato.codigotributacao codigotributacao")
				.leftOuterJoin("contrato.itemlistaservicoBean itemlistaservicoBean")
				.leftOuterJoin("contrato.codigocnaeBean codigocnaeBean")
				
				.leftOuterJoin("contrato.prazopagamento prazopagamento")
				.leftOuterJoin("contrato.indicecorrecao indicecorrecao")
				.leftOuterJoin("contrato.listaContratoparcela listaContratoparcela")
			.whereIn("contrato.cdcontrato", whereIn)
			
				.unique();
		
	}
	
	/**
	 * Busca os contratos do clinete e tipo de contrato passado por parametro.
	 * OBS: SE OS DOIS PAR�METROS ESTIVEREM NULOS RETORNAR� NULO.
	 *
	 * @param cliente
	 * @param contratotipo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/12/2012
	 */
	public List<Contrato> findByClienteContratotipo(Cliente cliente, Contratotipo contratotipo) {
		if(cliente == null && contratotipo == null) 
			return new ArrayList<Contrato>(); // N�O MUDAR!!
		
		return query()
			.select("contrato.cdcontrato, contrato.descricao")
			.leftOuterJoin("contrato.contratotipo contratotipo")
			.join("contrato.cliente cliente")
			.where("cliente = ?", cliente)
			.where("contratotipo = ?", contratotipo)
			.orderBy("contrato.descricao")
			.list();
	}
	
	public List<Contrato> findByContrato(Cliente cliente) {
		if(cliente == null || cliente.getCdpessoa() == null){
			throw new SinedException("O par�metro cliente e Cdpessoa n�o podem ser null.");
		}
		return query()
			.select("contrato.cdcontrato, contrato.descricao")
			.join("contrato.cliente cliente")
			.where("cliente = ?", cliente)
			.orderBy("contrato.descricao")
			.list();
	}
	
	/**
	 * M�todo que busca os contratos com o cliente
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 12/05/2014
	 */
	public List<Contrato> findByContratoWithCliente(String whereIn) {
		if(whereIn == null || "".equals(whereIn)){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
			.select("contrato.cdcontrato, contrato.descricao, cliente.cdpessoa, cliente.nome, contratotipo.atualizarProximoVencimentoConfirmar")
			.join("contrato.cliente cliente")
			.leftOuterJoin("contrato.contratotipo contratotipo")
			.whereIn("contrato.cdcontrato", whereIn)
			.list();
	}

	/**
	 * M�todo que atualiza e confirma os contratos
	 * 
	 * @param whereIn
	 * @param dtproximovencimento
	 * @author Tom�s Rabelo
	 * @param ano 
	 * @param mes 
	 */
	public void confirmaContratos(String whereIn, Date dtproximovencimento, Mes mes, Integer ano) {
		if(mes != null){
			getHibernateTemplate().bulkUpdate("update Contrato c set c.dtproximovencimento = ?, c.dtconfirma = ?, c.dtsuspensao = null, c.mes = ?, c.ano = " + (ano != null ? ano : "null") + " where c.id in ("+whereIn+")", 
				new Object[]{dtproximovencimento, new Date(System.currentTimeMillis()), mes});
		} else {
			getHibernateTemplate().bulkUpdate("update Contrato c set c.dtproximovencimento = ?, c.dtconfirma = ?, c.dtsuspensao = null, c.mes = null, c.ano = " + (ano != null ? ano : "null") + " where c.id in ("+whereIn+")", 
					new Object[]{dtproximovencimento, new Date(System.currentTimeMillis())});
		}
	}
	
	/**
	 * M�todo que seta a data da suspen��o do contrato.
	 * @param cdcontrato
	 * @author Taidson
	 * @since 17/12/2010
	 */
	public void setDataSuspensao(Integer cdcontrato) {
		getHibernateTemplate().bulkUpdate("update Contrato c set c.dtsuspensao = ? where c.cdcontrato = ?",  
		new Object[]{new Date (System.currentTimeMillis()), cdcontrato});
	}

	/**
	 * M�todo que retorna o contrato da NFS
	 * 
	 * @param nfs
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Contrato findInscricaoEstadualContratoByNota(NotaFiscalServico nfs) {
		return query()
			.select("contrato.cdcontrato, contrato.inscricaoestadual, contrato.nomefantasia")
			.join("contrato.listaNotacontrato listaNotacontrato")
			.join("listaNotacontrato.nota nota")
			.where("nota = ?", nfs)
			.unique();
	}

	/**
	 * M�todo que verifica contratos para gerar cobran�as futuras
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean haveContratoGerarNotaAntes(String whereIn) {
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Contrato.class)
			.where("contrato.formafaturamento = ?", Formafaturamento.NOTA_ANTES_RECEBIMENTO)
			.whereIn("contrato.cdcontrato", whereIn)
			.unique() > 0;
	}	
	
	/**
	 * Verifica se o contrato possui taxa do tipo Des�gio ou Desconto
	 * @param whereIn
	 * @return
	 * @author Taidson
	 * @since 03/08/2010
	 */
	public boolean haveDesagioDesconto(String whereIn){
		return newQueryBuilder(Long.class)
		.from(Contrato.class)
		.setUseTranslator(false)
		.select("count(*)")
		.leftOuterJoin("contrato.taxa taxa")
		.leftOuterJoin("taxa.listaTaxaitem item")
		.leftOuterJoin("item.tipotaxa tipotaxa")
		.openParentheses()
		.where("tipotaxa = ?", Tipotaxa.DESAGIO)
		.or()
		.where("tipotaxa = ?", Tipotaxa.DESCONTO)
		.closeParentheses()
		.whereIn("contrato.cdcontrato", whereIn)
		.unique() > 0;
	}

	/**
	 * Atualiza a data de cancelamento dos itens passados por par�metro.
	 *
	 * @param itensSelecionados
	 * @author Rodrigo Freitas
	 */
	public void updateDataCancelamento(String itensSelecionados) {
		getHibernateTemplate().bulkUpdate("update Contrato c set c.dtcancelamento = ? where c.id in (" + itensSelecionados + ")", 
				new Date(System.currentTimeMillis()));
	}
	
	/**
	 * Atualiza o isento para true.
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 * @since 04/12/2012
	 */
	public void updateIsento(String whereIn) {
		getHibernateTemplate().bulkUpdate("update Contrato c set c.isento = ? where c.id in (" + whereIn + ")", 
				Boolean.TRUE);
	}
	
	/**
	 * Coloca a data de suspens�o nula ao cancelar um contrato.
	 *
	 * @param itensSelecionados
	 * @author Rodrigo Freitas
	 */
	public void updateDtsupensaoCancelamento(String itensSelecionados) {
		getHibernateTemplate().bulkUpdate("update Contrato c set c.dtsuspensao = null where c.id in (" + itensSelecionados + ")");
	}
	
	public void updateEstornoCancelamento(String itensSelecionados) {
		getHibernateTemplate().bulkUpdate("update Contrato c set c.dtcancelamento = null where c.id in (" + itensSelecionados + ")");
	}

	public Contrato findLastByProjeto(Projeto projeto) {
		return query()
					.select("contrato.cdcontrato, contrato.identificador, contrato.dtinicio, contrato.dtfim")
					.setMaxResults(1)
					.join("contrato.rateio rateio")
					.join("rateio.listaRateioitem listaRateioitem")
					.join("listaRateioitem.projeto projeto")
					.where("projeto = ?", projeto)
					.orderBy("contrato.cdcontrato desc")
					.unique();
	}
	
	public Contrato clienteByContrato(Integer cdcontrato) {
		return query()
			.select("contrato.cdcontrato, contrato.identificador, contrato.descricao, cliente.cdpessoa, cliente.nome")
			.join("contrato.cliente cliente")
			.where("contrato.id = ?", cdcontrato)
			.orderBy("cliente.nome")
			.unique();
	}
	
	
	
	public Contrato servicosByContrato(Contrato contrato) {
		return query()
			.select("contrato.cdcontrato, contrato.identificador, cliente.cdpessoa, " +
					"servico.cdmaterial, servico.nome, listaContratomaterial.cdcontratomaterial")
			.join("contrato.cliente cliente")
			.leftOuterJoin("contrato.listaContratomaterial listaContratomaterial")
			.leftOuterJoin("listaContratomaterial.servico servico ")
			.where("contrato = ?", contrato)
			.unique();
	}
	
	/**
	 * M�todo que retorna os contratos que os respons�veis possuem email
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Contrato> findContratosComResponsaveis(String whereIn) {
		return query()
			.select("contrato.cdcontrato, contrato.identificador, contrato.descricao, empresa.cdpessoa, cliente.cdpessoa, cliente.nome, responsavel.cdpessoa, responsavel.nome, responsavel.email")
			.join("contrato.responsavel responsavel")
			.join("contrato.cliente cliente")
			.leftOuterJoin("contrato.empresa empresa")
			.whereIn("contrato.id", whereIn)
			.where("responsavel.email is not null")
			.list();
	}
	
	/**
	 * Busca a listagem de contrato sem pagina��o.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contrato> findTodosContrato(ContratoFiltro filtro) {
		QueryBuilderSined<Contrato> query = querySined();
		updateListagemQuery(query, filtro);
		return query.list();
	}
	
	/**
	 * Atualiza a data fim do contrato.
	 *
	 * @param c
	 * @author Rodrigo Freitas
	 */
	public void atualizaDataFimContrato(Contrato c) {
		getHibernateTemplate().bulkUpdate("update Contrato c set c.dtfim = ? where c.id = ?", 
				new Object[]{c.getDtfim(), c.getCdcontrato()});
	}
	
	/**
	* M�todo que seta nulo a data fim do contrato
	*
	* @param c
	* @since 07/11/2014
	* @author Luiz Fernando
	*/
	public void setNullDataFimContrato(Contrato c) {
		getHibernateTemplate().bulkUpdate("update Contrato c set c.dtfim = null where c.id = ?", 
				new Object[]{c.getCdcontrato()});
	}
	
	/**
	 * Atualiza a data de proximo vencimento do contrato.
	 *
	 * @param c
	 * @author Rodrigo Freitas
	 */
	public void atualizaDtProximoVencimentoContrato(Contrato c) {
		getHibernateTemplate().bulkUpdate("update Contrato c set c.dtproximovencimento = ? where c.id = ?", 
				new Object[]{c.getDtproximovencimento(), c.getCdcontrato()});
	}
	
	/**
	 * Atualiza a data de proximo vencimento do contrato.
	 *
	 * @param c
	 * @author Rodrigo Freitas
	 */
	public void atualizaDtConfirmaContrato(Contrato c) {
		getHibernateTemplate().bulkUpdate("update Contrato c set c.dtconfirma = ?, c.dtsuspensao = null where c.id = ?", 
				new Object[]{SinedDateUtils.currentDate(), c.getCdcontrato()});
	}
	
	/**
	 * Atualiza a data de conclus�o do contrato
	 *
	 * @param c
	 * @author Rodrigo Freitas
	 * @since 09/07/2014
	 */
	public void atualizaDataConclusaoContrato(Contrato c) {
		getHibernateTemplate().bulkUpdate("update Contrato c set c.dtconclusao = ? where c.id = ?", 
				new Object[]{SinedDateUtils.currentDate(), c.getCdcontrato()});
	}
	
	/**
	 * Atualiza a data de proximo vencimento do contrato.
	 *
	 * @param c
	 * @author Rodrigo Freitas
	 */
	public void atualizaValorContrato(Contrato c) {
		getHibernateTemplate().bulkUpdate("update Contrato c set c.valor = ? where c.id = ?", 
				new Object[]{c.getValor(), c.getCdcontrato()});
	}
	
	/**
	 * Atualiza o m�s e ano do contrato.
	 *
	 * @param c
	 * @author Rodrigo Freitas
	 */
	public void atualizaMesAnoContrato(Contrato c) {
		getHibernateTemplate().bulkUpdate("update Contrato c set c.mes = ?, c.ano = ? where c.id = ?", 
				new Object[]{c.getMes(), c.getAno(), c.getCdcontrato()});
	}
	
	@SuppressWarnings("unchecked")
	public List<PagamentoClienteServicoBean> buscaListaPagamentoWebservice(Cliente cliente, Material material, Date dtinicio, Date dtfim) {
		
		String query = 
			
		"SELECT DISTINCT 'CONTRATO' AS tipo, c.cdcontrato AS cdcontrato,  0 AS cdvenda, c.cdcliente AS cdcliente,  c.cdfrequencia AS cdfrequencia,  " +
		"cm.cdservico AS cdmaterial, cm.qtde AS quantidade, d.cddocumento AS cddocumento, d.dtvencimento AS dtvencimento, " +
		"d.cddocumentoacao AS cddocumentoacao, ad.valoratual AS valordocumento, COALESCE(m.dtbanco, m.dtmovimentacao) AS dtpagamento, m.valor AS valormovimentacao " +
		"FROM contrato c " +
		"JOIN contratomaterial cm ON cm.cdcontrato = c.cdcontrato " +
		"JOIN notacontrato nc ON nc.cdcontrato = c.cdcontrato " +
		"JOIN notadocumento nd ON nd.cdnota = nc.cdnota " +
		"JOIN documento d ON d.cddocumento = nd.cddocumento " +
		"JOIN aux_documento ad ON ad.cddocumento = d.cddocumento " +
		"LEFT OUTER JOIN movimentacaoorigem mo ON mo.cddocumento = d.cddocumento " +
		"LEFT OUTER JOIN movimentacao m ON m.cdmovimentacao = mo.cdmovimentacao " +

		"WHERE d.cddocumentoacao NOT IN (0,107) " +
		"AND cm.cdservico = " + material.getCdmaterial() + " " +
		"AND COALESCE(m.dtbanco, m.dtmovimentacao, d.dtvencimento) >= '" + SinedDateUtils.toStringPostgre(dtinicio) + "' " +
		"AND COALESCE(m.dtbanco, m.dtmovimentacao, d.dtvencimento) <= '" + SinedDateUtils.toStringPostgre(dtfim) + "' " +
		(cliente != null && cliente.getCdpessoa() != null ? ("AND c.cdcliente = " + cliente.getCdpessoa() + " ") : "") +

		"UNION ALL " +

		"SELECT DISTINCT  'CONTRATO' AS tipo, c.cdcontrato AS cdcontrato, 0 AS cdvenda, c.cdcliente AS cdcliente, c.cdfrequencia AS cdfrequencia,  " +
		"cm.cdservico AS cdmaterial, cm.qtde AS quantidade, d.cddocumento AS cddocumento, d.dtvencimento AS dtvencimento, " +
		"d.cddocumentoacao AS cddocumentoacao, ad.valoratual AS valordocumento, COALESCE(m.dtbanco, m.dtmovimentacao) AS dtpagamento, m.valor AS valormovimentacao " +
		"FROM contrato c " +
		"JOIN contratomaterial cm ON cm.cdcontrato = c.cdcontrato " +
		"JOIN documentoorigem doc ON doc.cdcontrato = c.cdcontrato " +
		"JOIN documento d ON d.cddocumento = doc.cddocumento " +
		"JOIN aux_documento ad ON ad.cddocumento = d.cddocumento " +
		"LEFT OUTER JOIN movimentacaoorigem mo ON mo.cddocumento = d.cddocumento " +
		"LEFT OUTER JOIN movimentacao m ON m.cdmovimentacao = mo.cdmovimentacao " +

		"WHERE d.cddocumentoacao NOT IN (0,107) " +
		"AND cm.cdservico = " + material.getCdmaterial() + " " +
		"AND COALESCE(m.dtbanco, m.dtmovimentacao, d.dtvencimento) >= '" + SinedDateUtils.toStringPostgre(dtinicio) + "' " +
		"AND COALESCE(m.dtbanco, m.dtmovimentacao, d.dtvencimento) <= '" + SinedDateUtils.toStringPostgre(dtfim) + "' " +
		(cliente != null && cliente.getCdpessoa() != null ? ("AND c.cdcliente = " + cliente.getCdpessoa() + " ") : "") +

		"UNION ALL  " +

		"SELECT DISTINCT  'VENDA' AS tipo, 0 AS cdcontrato, v.cdvenda AS cdvenda, v.cdcliente AS cdcliente, 0 AS cdfrequencia,  " +
		"vm.cdmaterial AS cdmaterial, vm.quantidade AS quantidade, d.cddocumento AS cddocumento, d.dtvencimento AS dtvencimento, " +
		"d.cddocumentoacao AS cddocumentoacao, ad.valoratual AS valordocumento, COALESCE(m.dtbanco, m.dtmovimentacao) AS dtpagamento, m.valor AS valormovimentacao " +
		"FROM venda v " +
		"JOIN vendamaterial vm ON vm.cdvenda = v.cdvenda " +
		"JOIN documentoorigem doc ON doc.cdvenda = v.cdvenda " +
		"JOIN documento d ON d.cddocumento = doc.cddocumento " +
		"JOIN aux_documento ad ON ad.cddocumento = d.cddocumento " +
		"LEFT OUTER JOIN movimentacaoorigem mo ON mo.cddocumento = d.cddocumento " +
		"LEFT OUTER JOIN movimentacao m ON m.cdmovimentacao = mo.cdmovimentacao " +

		"WHERE d.cddocumentoacao NOT IN (0,107) " +
		"AND vm.cdmaterial = " + material.getCdmaterial() + " " +
		"AND COALESCE(m.dtbanco, m.dtmovimentacao, d.dtvencimento) >= '" + SinedDateUtils.toStringPostgre(dtinicio) + "' " +
		"AND COALESCE(m.dtbanco, m.dtmovimentacao, d.dtvencimento) <= '" + SinedDateUtils.toStringPostgre(dtfim) + "' " +
		(cliente != null && cliente.getCdpessoa() != null ? ("AND v.cdcliente = " + cliente.getCdpessoa() + " ") : "");

		SinedUtil.markAsReader();
		List<PagamentoClienteServicoBean> lista = getJdbcTemplate().query(query, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				
				int valormovimentacao = rs.getInt("valormovimentacao");
				int valordocumento = rs.getInt("valordocumento");
				int cdcontrato = rs.getInt("cdcontrato");
				int cdvenda = rs.getInt("cdvenda");
				int cdfrequencia = rs.getInt("cdfrequencia");
				
				return new PagamentoClienteServicoBean(
								rs.getString("tipo"), 
								(cdcontrato != 0 ? cdcontrato : null), 
								(cdvenda != 0 ? cdvenda : null), 
								rs.getInt("cdcliente"), 
								(cdfrequencia != 0 ? cdfrequencia : null), 
								rs.getInt("cdmaterial"), 
								rs.getInt("quantidade"), 
								rs.getInt("cddocumento"), 
								rs.getDate("dtvencimento"),
								rs.getInt("cddocumentoacao"), 
								(valordocumento != 0 ? new Money(valordocumento, true) : null), 
								rs.getDate("dtpagamento"),
								(valormovimentacao != 0 ? new Money(valormovimentacao, true) : null)
								);
			}
		});
		
		return lista;
	}
	
	/**
	* M�todo que atualiza os dados de vencimento, valor, reponsavel, quantidade e fator de ajuste do contrato
	*
	* @param c
	* @param dtVencimento
	* @param valor
	* @param responsavel
	* @param qtde
	* @param fatorajuste
	* @since Sep 6, 2011
	* @author Luiz Fernando F Silva
	*/
	public void atualizaDados(Contrato c, Date dtVencimento, Money valor, Colaborador responsavel, Double qtde, Fatorajuste fatorajuste, Indicecorrecao indicecorrecao, Date dtRenovacao) {
		if(c == null || c.getCdcontrato() == null){
			throw new SinedException("Par�metros inv�lidos.");
		}
				
		StringBuilder  sql = new StringBuilder("");		
		
		if(dtVencimento != null)
			sql.append("dtproximovencimento = '" + dtVencimento + "'");		
		if(valor != null && !sql.toString().equals(""))
			sql.append(", valor = " + valor.getValue().doubleValue() * 100);
		else if(valor != null)
			sql.append("valor = " + valor.getValue().doubleValue() * 100);		
		if(responsavel != null && responsavel.getCdpessoa() != null && !sql.toString().equals(""))
			sql.append(", cdresponsavel = " + responsavel.getCdpessoa());
		else if(responsavel != null && responsavel.getCdpessoa() != null)
			sql.append("cdresponsavel = " + responsavel.getCdpessoa());
		if(qtde != null && !sql.toString().equals(""))
			sql.append(", qtde = " + qtde);
		else if(qtde != null)
			sql.append("qtde = " + qtde);
		if(fatorajuste != null && fatorajuste.getCdfatorajuste() != null && !sql.toString().equals(""))
			sql.append(", cdfatorajuste = " + fatorajuste.getCdfatorajuste());
		else if(fatorajuste != null && fatorajuste.getCdfatorajuste() != null)
			sql.append("cdfatorajuste = " + fatorajuste.getCdfatorajuste());
		if(indicecorrecao != null && indicecorrecao.getCdindicecorrecao() != null && !sql.toString().equals(""))
			sql.append(", cdindicecorrecao = " + indicecorrecao.getCdindicecorrecao());
		else if(indicecorrecao != null && indicecorrecao.getCdindicecorrecao() != null)
			sql.append("cdindicecorrecao = " + indicecorrecao.getCdindicecorrecao());
		if(dtRenovacao != null && !sql.toString().equals(""))
			sql.append(", dtrenovacao = '" + dtRenovacao + "'");
		else if(dtRenovacao != null)
			sql.append("dtrenovacao = '" + dtRenovacao + "'");
				
		if(!sql.toString().trim().equals(""))
			getJdbcTemplate().execute("update Contrato set " + sql.toString() + " where cdcontrato = " + c.getCdcontrato());
		
	}
	
	/**
	* M�todo que busca os contrato para atualizar dados
	*
	* @param whereIn
	* @return
	* @since Sep 6, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Contrato> findContratoAtualizarDados(String whereIn) {
		if(whereIn == null){
			throw new SinedException("Par�metros inv�lidos.");
		}
		return query()
				.select("contrato.cdcontrato, contrato.valor, contrato.valoracrescimo, frequencia.cdfrequencia, contrato.dtproximovencimento, " +
						"contrato.identificador, contrato.dtinicio, contrato.formafaturamento, contrato.historico, contrato.anotacao, " +
						"contrato.dtrenovacao, contrato.qtde, responsavel.cdpessoa, responsavel.nome, " +
						"rateio.cdrateio, indicecorrecao.cdindicecorrecao, fatorajuste.cdfatorajuste, prazopagamento.cdprazopagamento, " +
						"empresa.cdpessoa, contrato.ignoravalormaterial, cliente.cdpessoa, cliente.nome, " +
						"listaContratomaterial.cdcontratomaterial, listaContratomaterial.qtde, listaContratomaterial.valorunitario, listaContratomaterial.valorfechado, " +
						"listaContratomaterial.periodocobranca, listaContratomaterial.valordesconto, listaContratomaterial.dtinicio, listaContratomaterial.dtfim, " +
						"listaRateioitem.cdrateioitem, listaRateioitem.percentual, listaRateioitem.valor, " +
						"contagerencial.cdcontagerencial, centrocusto.cdcentrocusto, projeto.cdprojeto, " +
						"listaContratoparcela.dtvencimento, listaContratoparcela.valornfservico, listaContratoparcela.valornfproduto, " +
						"listaContratoparcela.desconto, listaContratoparcela.valor, listaContratoparcela.aplicarindice, listaContratoparcela.cobrada")
				.leftOuterJoin("contrato.listaContratomaterial listaContratomaterial")
				.leftOuterJoin("contrato.cliente cliente")
				.leftOuterJoin("contrato.empresa empresa")
				.leftOuterJoin("contrato.frequencia frequencia")
				.leftOuterJoin("contrato.rateio rateio")
				.leftOuterJoin("contrato.indicecorrecao indicecorrecao")
				.leftOuterJoin("contrato.prazopagamento prazopagamento")
				.leftOuterJoin("contrato.responsavel responsavel")
				.leftOuterJoin("contrato.fatorajuste fatorajuste")
				.leftOuterJoin("contrato.listaContratoparcela listaContratoparcela")
				.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
				.leftOuterJoin("listaRateioitem.contagerencial contagerencial")
				.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
				.leftOuterJoin("listaRateioitem.projeto projeto")
				.whereIn("contrato.cdcontrato", whereIn)
				.orderBy("contrato.cdcontrato")
				.list();
	}
	
	/**
	* M�todo que busca os contrato para calcular o valor do contrato
	*
	* @param whereIn
	* @return
	* @since Oct 17, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Contrato> findContratoParaCalculoValorcontrato(String whereIn){
		
		if(whereIn == null || whereIn.equals(""))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("contrato.cdcontrato, contrato.inss, contrato.incideinss, contrato.ir, contrato.incideir, contrato.valor, contrato.valoracrescimo, " +
						"contrato.iss, contrato.incideiss, contrato.icms, contrato.incideicms, contrato.dtproximovencimento, contrato.qtde, " +
						"contrato.pis, contrato.incidepis, contrato.cofins, contrato.incidecofins, contrato.csll, contrato.incidecsll, " +
						"listaContratodesconto.cdcontratodesconto, listaContratodesconto.valor, listaContratodesconto.dtinicio, listaContratodesconto.dtfim, " +
						"listaContratoparcela.cdcontratoparcela, listaContratoparcela.valor, listaContratoparcela.dtvencimento, " +
						"listaContratomaterial.cdcontratomaterial, listaContratomaterial.qtde, listaContratomaterial.valorunitario, " +
						"listaContratomaterial.dtinicio, listaContratomaterial.dtfim, listaContratomaterial.valordesconto, " +
						"contrato.valordedutivocomissionamento, listaContratocolaborador.cdcontratocolaborador, listaContratocolaborador.ordemcomissao, " +
						"comissionamento.cdcomissionamento, comissionamento.deduzirvalor, comissionamento.percentual, comissionamento.valor, comissionamento.quantidade, " +
						"comissionamentoColaborador.cdcomissionamento, comissionamentoColaborador.deduzirvalor, comissionamentoColaborador.percentual, " +
						"comissionamentoColaborador.valor, comissionamentoColaborador.quantidade, " +
						"cliente.cdpessoa, cliente.nome, " +
						"pessoa.cdpessoa, colaborador.cdpessoa")
				.leftOuterJoin("contrato.listaContratodesconto listaContratodesconto")
				.leftOuterJoin("contrato.listaContratoparcela listaContratoparcela")
				.leftOuterJoin("contrato.listaContratomaterial listaContratomaterial")
				.leftOuterJoin("contrato.listaContratocolaborador listaContratocolaborador")
				.leftOuterJoin("listaContratocolaborador.colaborador colaborador")
				.leftOuterJoin("contrato.comissionamento comissionamento")
				.leftOuterJoin("listaContratocolaborador.comissionamento comissionamentoColaborador")
				.leftOuterJoin("contrato.vendedor pessoa")
				.leftOuterJoin("contrato.cliente cliente")
				.whereIn("contrato.cdcontrato", whereIn)
				.orderBy("listaContratocolaborador.ordemcomissao")
				.list();
		
	}
	/**
	* M�todo que retorna a lista de contrato do cliente passado como par�metro
	*
	* @param cliente
	* @return
	* @since Nov 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Contrato> findByCliente(Cliente cliente) {
		return query()
				.select("contrato.cdcontrato, contrato.descricao, contrato.identificador, contratotipo.nome")
				.leftOuterJoin("contrato.cliente cliente")
				.leftOuterJoin("contrato.contratotipo contratotipo")
				.where("cliente= ?", cliente)
				.orderBy("contrato.descricao")
				.list();
	}
	
	public List<Contrato> findByClienteForProducaoAgenda(Cliente cliente) {
		return query()
			.select("contrato.cdcontrato, contrato.descricao, contrato.identificador, contratotipo.nome")
			.leftOuterJoin("contrato.cliente cliente")
			.leftOuterJoin("contrato.contratotipo contratotipo")
			.where("cliente= ?", cliente)
			.orderBy("contrato.identificador, contratotipo.nome, contrato.descricao")
			.list();
	}
	
	/**
	* M�todo que busca os dados para verificar a cria��o do comissionamento
	*
	* @param contrato
	* @return
	* @since Nov 10, 2011
	* @author Luiz Fernando F Silva
	*/
	public Contrato findForComissaoByContrato(Contrato contrato) {
		if(contrato == null || contrato.getCdcontrato() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("contrato.cdcontrato, contrato.valordedutivocomissionamento, " +
						"contratocolaborador.cdcontratocolaborador, contratocolaborador.ordemcomissao, " +
						"vendedor.cdpessoa, colaborador.cdpessoa, comissionamentovendedor.tipobccomissionamento, comissionamento.tipobccomissionamento, " +
						"comissionamentovendedor.cdcomissionamento, comissionamentovendedor.deduzirvalor, comissionamentovendedor.quantidade, " +
						"comissionamento.cdcomissionamento, comissionamento.deduzirvalor, comissionamento.quantidade, " +
						"comissionamentovendedor.considerardiferencapagamento, comissionamento.considerardiferencapagamento")
				.leftOuterJoin("contrato.vendedor vendedor")
				.leftOuterJoin("contrato.comissionamento comissionamentovendedor")
				.leftOuterJoin("contrato.listaContratocolaborador contratocolaborador")
				.leftOuterJoin("contratocolaborador.comissionamento comissionamento")
				.leftOuterJoin("contratocolaborador.colaborador colaborador")
				.where("contrato = ?", contrato)
				.orderBy("contratocolaborador.ordemcomissao")
				.unique();
	}
	
	/**
	* M�todo que busca os dados para calculo do valor do contrato e inscri��o municipal do cliente
	*
	* @param whereIn
	* @return
	* @since Nov 10, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Contrato> findForFaturarPorMedicao(String whereIn) {
		if(whereIn == null || whereIn.equals(""))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("contrato.cdcontrato, contrato.inss, contrato.incideinss, contrato.ir, contrato.incideir, contrato.valor, contrato.valoracrescimo, " +
					"contrato.iss, contrato.incideiss, contrato.icms, contrato.incideicms, contrato.qtde, contrato.valoracrescimo, " +
					"contrato.pis, contrato.incidepis, contrato.cofins, contrato.incidecofins, contrato.csll, contrato.incidecsll, " +
					"listaContratodesconto.cdcontratodesconto, listaContratodesconto.valor, listaContratodesconto.dtinicio, listaContratodesconto.dtfim, " +
					"listaContratoparcela.cdcontratoparcela, listaContratoparcela.valor, listaContratoparcela.dtvencimento, " +
					"listaContratomaterial.cdcontratomaterial, listaContratomaterial.qtde, listaContratomaterial.valorunitario, " +
					"listaContratomaterial.dtinicio, listaContratomaterial.dtfim, listaContratomaterial.valordesconto, " +					
					"cliente.cdpessoa, cliente.inscricaomunicipal, fatorajuste.cdfatorajuste, fatorajustevalor.cdfatorajustevalor, " + 
					"fatorajustevalor.mesano, fatorajustevalor.valor, contrato.ignoravalormaterial ")
			.leftOuterJoin("contrato.listaContratodesconto listaContratodesconto")
			.leftOuterJoin("contrato.listaContratoparcela listaContratoparcela")
			.leftOuterJoin("contrato.listaContratomaterial listaContratomaterial")
			.leftOuterJoin("contrato.cliente cliente")
			.leftOuterJoin("contrato.fatorajuste fatorajuste")
			.leftOuterJoin("fatorajuste.listaFatorajustevalor fatorajustevalor")
			.whereIn("contrato.cdcontrato", whereIn)
			.list();
	}
	
	
	/**
	 * M�todo que busca os contrato que tenham fator de ajuste
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contrato> findForAtualizarValorFatorajuste() {
		return query()
				.select("contrato.cdcontrato, contrato.qtde, fatorajuste.cdfatorajuste")
				.join("contrato.fatorajuste fatorajuste")
				.list();				
	}
	
	/**
	 * M�todo que atualiza o valor do contrato
	 *
	 * @param cdcontrato
	 * @param valor
	 * @author Luiz Fernando
	 */
	public void updateValorFatorajusteContrato(Integer cdcontrato, Money valor) {		
		if(cdcontrato != null && valor != null)
			getHibernateTemplate().bulkUpdate("update Contrato c set c.valor = ? where c.id = ?", 
					new Object[]{valor, cdcontrato});
	}
	
	/**
	 * M�todo que retorna o total faturado do contrato no m�s
	 *
	 * @param contrato
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public Money findForTotalFaturado(Contrato contrato){
		if(contrato == null || contrato.getCdcontrato() == null)
			throw new SinedException("Par�metro inv�lido");
		
		StringBuilder sql = new StringBuilder();
		
		sql
			.append("select sum(nfsi.qtde * nfsi.precounitario) as total ")
			.append("from notafiscalservicoitem nfsi ")
			.append("join nota n on n.cdnota = nfsi.cdnotafiscalservico ")
			.append("join notacontrato nc on nc.cdnota = nfsi.cdnotafiscalservico ")
			.append("where nc.cdcontrato = ").append(contrato.getCdcontrato())
			.append("and n.cdnotastatus <> 3 ")
			.append("group by cdcontrato");
		
//		System.out.println("QUERY: " + sql.toString());
//		List<Long> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
//				public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
//					return rs.getLong("total");
//					}
//			});
//		
//		return lista != null && lista.size() > 0 && lista.get(0) > 0 ? new Money(lista.get(0)/100d) : new Money(0.0);

		SinedUtil.markAsReader();
		List<Double> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
				public Double mapRow(ResultSet rs, int rowNum) throws SQLException {
					return rs.getDouble("total");
					}
		});
		
		return lista != null && lista.size() > 0 && lista.get(0) > 0 ? new Money(lista.get(0)) : new Money(0.0);
	}
	
	/**
	 * 
	 * M�todo para buscar a Url de redirecionamento do sistema para o Atendimento
	 *
	 *@author Thiago Augusto
	 *@date 03/02/2012
	 * @return
	 */
	public String carregaUrlSistema(Contrato contrato){
		Contrato bean = querySined()
			.select("contrato.cdcontrato, contrato.urlsistema")
			.where("contrato = ?", contrato)
			.unique();
		
		return bean != null ? bean.getUrlsistema() : "";
	}
	
	/**
	 * 
	 * M�todo que carrega o contrato do documento com o documento de origem para a verifica��o na hora de baixar uma conta a receber
	 *
	 *@author Thiago Augusto
	 *@date 16/03/2012
	 * @param bean
	 * @return
	 */
	public Contrato carregarContratoComDocumentoOrigem(Documento bean){
		return query()
					.select("contrato.cdcontrato, contrato.formafaturamento, contrato.dtproximovencimento, contratotipo.cdcontratotipo, " +
							"documento.cddocumento, listacontratotipodesconto.cdcontratotipodesconto, listacontratotipodesconto.diasantesvencimento, " +
							"listacontratotipodesconto.forma, listacontratotipodesconto.valor, listacontratotipodesconto.limite, prazopagamento.cdprazopagamento")
					.leftOuterJoin("contrato.prazopagamento prazopagamento")
					.leftOuterJoin("contrato.contratotipo contratotipo")
					.leftOuterJoin("contratotipo.listacontratotipodesconto listacontratotipodesconto")
					.leftOuterJoin("contrato.listaDocumentoOrigem listaDocumentoOrigem")
					.leftOuterJoin("listaDocumentoOrigem.documento documento")
					.where("documento = ? ", bean)
					.unique();
	}
	
	/**
	 * 
	 * M�todo M�todo que carrega o contrato do documento pela nota fiscal para a verifica��o na hora de baixar uma conta a receber
	 *
	 *@author Thiago Augusto
	 *@date 16/03/2012
	 * @param bean
	 * @return
	 */
	public Contrato carregarContratoComNotaFiscal(Documento bean){
		return query()
					.select("contrato.cdcontrato, contrato.formafaturamento, contrato.dtproximovencimento, contratotipo.cdcontratotipo, " +
							"documento.cddocumento, listacontratotipodesconto.cdcontratotipodesconto, listacontratotipodesconto.diasantesvencimento, " +
							"listacontratotipodesconto.forma, listacontratotipodesconto.valor, listacontratotipodesconto.limite, prazopagamento.cdprazopagamento")
					.leftOuterJoin("contrato.prazopagamento prazopagamento")
					.leftOuterJoin("contrato.contratotipo contratotipo")
					.leftOuterJoin("contratotipo.listacontratotipodesconto listacontratotipodesconto")
					.leftOuterJoin("contrato.listaNotacontrato listaNotacontrato")
					.leftOuterJoin("listaNotacontrato.nota nota")
					.leftOuterJoin("nota.listaNotaDocumento listaNotaDocumento")
					.leftOuterJoin("listaNotaDocumento.documento documento")
					.where("documento = ? ", bean)
					.unique();
	}
	
	/**
	 * 
	 * M�todo que carrega o bean de contrato com o vendedor para a compara��o na hora de montar o report de comissionamento.
	 *
	 * @name LoadContratoWithColaborador
	 * @param contrato
	 * @return
	 * @return Contrato
	 * @author Thiago Augusto
	 * @date 18/06/2012
	 *
	 */
	public Contrato loadContratoWithColaborador(Contrato contrato){
		return query()
					.select("contrato.cdcontrato, vendedor.cdpessoa, vendedor.nome")
					.leftOuterJoin("contrato.vendedor vendedor")
					.where("contrato = ? ", contrato)
					.unique();
	}
	
	/**
	 * Carrega o contrato com a empresa preenchida
	 *
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/03/2015
	 */
	public Contrato loadContratoWithEmpresa(Contrato contrato){
		return query()
		.select("contrato.cdcontrato, empresa.cdpessoa, empresa.nome")
		.leftOuterJoin("contrato.empresa empresa")
		.where("contrato = ? ", contrato)
		.unique();
	}
	
	/**
	 * M�todo que retorna um booleano que indica se existem contrato com data de pr�ximo vencimento referente a um per�odo fechado.
	 * @param whereIn
	 * @return
	 * @autor Rafael Salvio
	 */
	public Boolean verificaListaFechamento(String whereIn){
		if(whereIn == null || "".equals((whereIn)) )
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
				.setUseTranslator(Boolean.FALSE)
				.from(Contrato.class)
				.select("count(*)")
				.whereIn("contrato.cdcontrato", whereIn)
				.where("exists ( SELECT f.id " +
								" FROM Fechamentofinanceiro f " +
								" WHERE f.empresa = contrato.empresa " +
								" AND (f.conta = contrato.conta OR f.conta is null) " +
								" AND f.datainicio <= contrato.dtproximovencimento " +
								" AND f.datalimite >= contrato.dtproximovencimento " +
								" AND f.ativo = ?)", new Object[]{Boolean.TRUE})
				.unique() > 0;
	}
	
	/**
	 * Busca os contratos para verifica��o do n�mero de parcelas m�xima no faturamento futuro.
	 *
	 * @param whereIn
	 * @return
	 * @since 14/09/2012
	 * @author Rodrigo Freitas
	 */
	public List<Contrato> findSomenteComParcelasEmAberto(String whereIn) {
		if(whereIn == null || "".equals((whereIn)) )
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
					.select("contrato.cdcontrato, contratoparcela.cobrada, contratoparcela.dtvencimento")
					.join("contrato.listaContratoparcela contratoparcela")
					.whereIn("contrato.cdcontrato", whereIn)
					.openParentheses()
					.where("contratoparcela.cobrada is null")
					.or()
					.where("contratoparcela.cobrada = ?", Boolean.FALSE)
					.closeParentheses()
					.list();
	}
	
	/**
	 * M�todo que busca os contratos relacionados ao colaborador e per�odo de assinatura
	 *
	 * @param colaborador
	 * @param dtassinatura
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contrato> findForComissaoByColaboradorDtassinatura(Colaborador colaborador, Date dtreferencia1, Date dtreferencia2) {
		if(colaborador == null || colaborador.getCdpessoa() == null || dtreferencia1 == null || dtreferencia2 == null)
			throw new SinedException("Par�metro inv�lido.");
		
		
		return query()
				.select("contrato.cdcontrato, contrato.valor, contrato.valoracrescimo, " +
						"contratocolaborador.cdcontratocolaborador, contratocolaborador.ordemcomissao, " +
						"vendedor.cdpessoa, colaborador.cdpessoa, comissionamentovendedor.tipobccomissionamento, comissionamento.tipobccomissionamento, " +
						"comissionamentovendedor.cdcomissionamento, comissionamentovendedor.deduzirvalor, comissionamentovendedor.quantidade, " +
						"comissionamento.cdcomissionamento, comissionamento.deduzirvalor, comissionamento.quantidade")
				.leftOuterJoin("contrato.vendedor vendedor")
				.leftOuterJoin("contrato.comissionamento comissionamentovendedor")
				.leftOuterJoin("contrato.listaContratocolaborador contratocolaborador")
				.leftOuterJoin("contratocolaborador.comissionamento comissionamento")
				.leftOuterJoin("contratocolaborador.colaborador colaborador")
				.openParentheses()
					.where("colaborador = ?", colaborador)
					.or()
					.where("vendedor = ?", colaborador)
				.closeParentheses()
				.where("contrato.dtassinatura >= ?", dtreferencia1)
				.where("contrato.dtassinatura <= ?", dtreferencia2)
				.orderBy("contratocolaborador.ordemcomissao")
				.list();
	}
	
	/**
	 * Carrega o contrato para a gera��o de romaneio.
	 *
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/12/2012
	 */
	public Contrato loadForRomaneio(Contrato contrato) {
		if(contrato == null || contrato.getCdcontrato() == null)
			throw new SinedException("Par�metro inv�lido");
		
		return query()
					.select("contrato.cdcontrato, contrato.identificador, empresa.cdpessoa, projeto.cdprojeto, contratomaterial.cdcontratomaterial, " +
							"contratomaterial.valorunitario, contratomaterial.qtde, servico.cdmaterial, " +
							"contratomaterialitem.qtde, materialDetalhe.cdmaterial, patrimonioitem.cdpatrimonioitem, contratotipo.locacao," +
							"contrato.dtproximovencimento, cliente.cdpessoa, enderecoentrega.cdendereco, " +
							"contratotipo.confirmarautomaticoprimeiroromaneio")
					.leftOuterJoin("contrato.cliente cliente")
					.leftOuterJoin("contrato.enderecoentrega enderecoentrega")
					.leftOuterJoin("contrato.contratotipo contratotipo")
					.leftOuterJoin("contrato.listaContratomaterial contratomaterial")
					.leftOuterJoin("contratomaterial.servico servico")
					.leftOuterJoin("contratomaterial.patrimonioitem patrimonioitem")
					.leftOuterJoin("contratomaterial.listaContratomaterialitem contratomaterialitem")
					.leftOuterJoin("contratomaterialitem.material materialDetalhe")
					.leftOuterJoin("contrato.empresa empresa")
					.leftOuterJoin("contrato.rateio rateio")
					.leftOuterJoin("rateio.listaRateioitem rateioitem")
					.leftOuterJoin("rateioitem.projeto projeto")
					.entity(contrato)
					.unique();
	}
	
	/**
	 * Carrega o contrato para a gera��o de romaneio.
	 *
	 * @param contrato
	 * @return
	 * @author Filipe Santos
	 * @since 21/03/2014
	 */
	public List<Contrato> loadListForRomaneio(String whereIn) {
		if(whereIn == null || whereIn.isEmpty())
			throw new SinedException("Par�metro inv�lido");
		
		return query()
					.select("contrato.cdcontrato, contrato.identificador, empresa.cdpessoa, projeto.cdprojeto, contratomaterial.cdcontratomaterial, " +
							"contratomaterial.valorunitario, contratomaterial.qtde, servico.cdmaterial, " +
							"contratomaterialitem.qtde, materialDetalhe.cdmaterial, patrimonioitem.cdpatrimonioitem, contratotipo.locacao," +
							"contrato.dtproximovencimento, cliente.cdpessoa")
					.leftOuterJoin("contrato.contratotipo contratotipo")
					.leftOuterJoin("contrato.listaContratomaterial contratomaterial")
					.leftOuterJoin("contratomaterial.servico servico")
					.leftOuterJoin("contratomaterial.patrimonioitem patrimonioitem")
					.leftOuterJoin("contratomaterial.listaContratomaterialitem contratomaterialitem")
					.leftOuterJoin("contratomaterialitem.material materialDetalhe")
					.leftOuterJoin("contrato.empresa empresa")
					.leftOuterJoin("contrato.rateio rateio")
					.leftOuterJoin("rateio.listaRateioitem rateioitem")
					.leftOuterJoin("rateioitem.projeto projeto")
					.leftOuterJoin("contrato.cliente cliente")
					.whereIn("contrato.cdcontrato", whereIn)
					.orderBy("contrato.cdcontrato")
					.list();
	}
	
	public List<Contrato> findForRelatorioComissionamentoContrato(DocumentocomissaoFiltro filtro) {
		QueryBuilder<Contrato> query = querySined();
		this.updateEntradaQuery(query);
		query.leftOuterJoinIfNotExists(new String[]{"contrato.listaContratocolaborador listaContratocolaborador"});
		
		query
			.where("contrato = ? ", filtro.getContrato())
			.where("contratotipo = ?", filtro.getContratotipo())
			.where("cliente = ?", filtro.getCliente())
			.where("contrato.dtproximovencimento >= ?", filtro.getDtinicio())
			.where("contrato.dtproximovencimento <= ?", filtro.getDtfim())
			
			.openParentheses()
			.where("comissionamento is not null")
			.or()
			.where("listaContratocolaborador is not null")
			.closeParentheses();
		
		query.ignoreJoin("listaContratocolaborador");
		return query.list();
	}
	
	/**
	 * M�todo que atualiza o campo isento do contrato
	 *
	 * @param contrato
	 * @param isento
	 * @author Luiz Fernando
	 */
	public void atualizaCampoIsentoContrato(Contrato contrato, boolean isento) {
		getHibernateTemplate().bulkUpdate("update Contrato c set c.isento = ? where c.id = ?", 
				new Object[]{isento, contrato.getCdcontrato()});		
	}
	
	/**
	 * M�todo que busca o contrato para faturar loca��o
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contrato> loadForFaturarlocacao(String whereIn) {
		if(whereIn == null || whereIn.equals(""))
			throw new SinedException("Contrato n�o pode ser nulo.");
		
		return query()
			.select("contrato.cdcontrato, contrato.identificador, contrato.descricao, contrato.dtinicio, contrato.dtfim, contrato.dtsuspensao, contrato.dtrenovacao, contrato.dataparceladiautil, contrato.dataparcelaultimodiautilmes, " +
					"contrato.formafaturamento, contrato.dtproximovencimento, contrato.mes, contrato.ano, contrato.valor, contrato.valoracrescimo, contrato.acrescimoproximofaturamento, " +
					"contrato.ir, contrato.incideir, contrato.iss, contrato.incideiss, contrato.anotacao, " +
					"contrato.pis, contrato.incidepis, contrato.cofins, contrato.incidecofins, contrato.csll, contrato.incidecsll, " +
					"contrato.inss, contrato.incideinss, contrato.icms, contrato.incideicms, contrato.dtiniciolocacao, contrato.dtfimlocacao, " +
					"contrato.dtconfirma, contrato.dtconclusao, contrato.faturamentolote, contrato.qtde, " +
					"contratotipo.cdcontratotipo, contratotipo.faturamentoporitensdevolvidos, " +
					"contrato.dtcancelamento, cliente.cdpessoa, " +
					"contrato.dtassinatura, contrato.dtrenovacao, contrato.isento, contrato.ignoravalormaterial, " +
					"empresa.cdpessoa, " +
					"frequencia.cdfrequencia, frequenciarenovacao.cdfrequencia, " +
					"listaContratoparcela.desconto, " +
					"taxa.cdtaxa, " +
					"aux_contrato.situacao, " +
					"listaContratomaterial.cdcontratomaterial, listaContratomaterial.dtinicio, listaContratomaterial.dtfim, listaContratomaterial.valorunitario, " +
					"listaContratomaterial.qtde, listaContratomaterial.periodocobranca, listaContratomaterial.valordesconto, listaContratomaterial.hrinicio, listaContratomaterial.hrfim, " +
					"listaContratomaterial.detalhamento, listaContratomaterial.valorfechado, " +
					"indice.cdindice, " +
					"materialpromocional.cdmaterial, materialpromocional.nome, " +
					"servico.cdmaterial, servico.nome, servico.servico, servico.materialtipo, servico.tributacaoestadual, servico.produto, servico.tipoitemsped, " +
					"servico.codigobarras, servico.ncmcompleto, servico.extipi, " +
					"unidademedidaservico.simbolo, unidademedidaservico.cdunidademedida, " +
					"prazopagamento.cdprazopagamento, " +
					"indicecorrecao.cdindicecorrecao, " +
					"listaContratoparcela.cdcontratoparcela, listaContratoparcela.dtvencimento, listaContratoparcela.valor, listaContratoparcela.aplicarindice, " +
					"listaContratoparcela.cobrada, listaContratoparcela.valornfservico, listaContratoparcela.valornfproduto, " +
					"itemlistaservicoBean.cditemlistaservico, itemlistaservicoBean.codigo, itemlistaservicoBean.descricao, " +
					"listaContratoservico.cdcontratoservico, listaContratoservico.dtvalidade,contratoservicostatusContratoServico.cdcontratoservicostatus,materialContratoServico.cdmaterial," +
					"fatorajuste.cdfatorajuste, " +
					"contrato.valortotalfechado, contrato.aliquotavalortotalfechado, " +
					
					"listaContratomateriallocacao.cdcontratomateriallocacao, listaContratomateriallocacao.dtmovimentacao, " +
					"listaContratomateriallocacao.qtde, listaContratomateriallocacao.contratomateriallocacaotipo, " +
					"listaContratomateriallocacao.substituicao, " +
					
					"listaContratodesconto.cdcontratodesconto, listaContratodesconto.dtinicio, listaContratodesconto.dtfim, " +
					"listaContratodesconto.valor, listaContratodesconto.tipodesconto, listaContratodesconto.tiponota, " +
					"rateio.cdrateio, listaRateioitem.valor, listaRateioitem.percentual, contagerencial.cdcontagerencial, " +
					"centrocusto.cdcentrocusto, projeto.cdprojeto," +
					"endereco.cdendereco, enderecoentrega.cdendereco, enderecocobranca.cdendereco, " +
					"contacarteira.cdcontacarteira,  conta.cdconta, patrimonioitem.cdpatrimonioitem, " +
					"romaneio.cdromaneio, romaneio.qtdeajudantes, romaneio.dtromaneio, " +
					"romaneiosituacao.cdromaneiosituacao, romaneiosituacao.descricao")
					
			.leftOuterJoin("contrato.cliente cliente")		
			.leftOuterJoin("contrato.endereco endereco")			
			.leftOuterJoin("contrato.enderecoentrega enderecoentrega")
			.leftOuterJoin("contrato.enderecocobranca enderecocobranca")
			.leftOuterJoin("contrato.conta conta")			
			.leftOuterJoin("contrato.contacarteira contacarteira")			
			.leftOuterJoin("contrato.contratotipo contratotipo")			
			.leftOuterJoin("contrato.aux_contrato aux_contrato")
			.leftOuterJoin("contrato.frequencia frequencia")
			.leftOuterJoin("contrato.frequenciarenovacao frequenciarenovacao")
			.leftOuterJoin("contrato.taxa taxa")
			.leftOuterJoin("contrato.empresa empresa")
			.leftOuterJoin("contrato.listaContratodesconto listaContratodesconto")
			.leftOuterJoin("contrato.listaContratoservico listaContratoservico")
			.leftOuterJoin("listaContratoservico.material materialContratoServico")
			.leftOuterJoin("listaContratoservico.contratoservicostatus contratoservicostatusContratoServico")
			.leftOuterJoin("contrato.listaContratomaterial listaContratomaterial")
			.leftOuterJoin("listaContratomaterial.indice indice ")
			.leftOuterJoin("listaContratomaterial.materialpromocional materialpromocional ")
			.leftOuterJoin("listaContratomaterial.servico servico ")
			.leftOuterJoin("listaContratomaterial.patrimonioitem patrimonioitem")
			.leftOuterJoin("servico.materialtipo materialtipo")
			.leftOuterJoin("servico.unidademedida unidademedidaservico")
			.leftOuterJoin("contrato.itemlistaservicoBean itemlistaservicoBean")
			.leftOuterJoin("contrato.prazopagamento prazopagamento")
			.leftOuterJoin("contrato.indicecorrecao indicecorrecao")
			.leftOuterJoin("contrato.listaContratoparcela listaContratoparcela")
			.leftOuterJoin("contrato.fatorajuste fatorajuste")
			.leftOuterJoin("listaContratomaterial.listaContratomateriallocacao listaContratomateriallocacao")
			.leftOuterJoin("contrato.rateio rateio")
			.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
			.leftOuterJoin("listaRateioitem.contagerencial contagerencial")
			.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
			.leftOuterJoin("listaRateioitem.projeto projeto")
			.leftOuterJoin("contrato.listaRomaneioorigem listaRomaneioorigem")
			.leftOuterJoin("listaRomaneioorigem.romaneio romaneio")
			.leftOuterJoin("romaneio.romaneiosituacao romaneiosituacao")
			.whereIn("contrato.cdcontrato", whereIn)
			.list();
	}
	
	public void updateZeraAcrescimo(Contrato c) {
		getHibernateTemplate().bulkUpdate("update Contrato c set c.valoracrescimo = null where c.id = ?", c.getCdcontrato());
	}
	
	/**
	 * M�todo que busca os contrato com projeto no rateio
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contrato> findContratoWithRateioProjeto(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("contrato.cdcontrato, rateio.cdrateio, listaRateioitem.cdrateioitem, " +
						"contagerencial.cdcontagerencial, centrocusto.cdcentrocusto, projeto.cdprojeto," +
						"listaRateioitem.percentual, listaRateioitem.valor ")
				.join("contrato.rateio rateio")
				.join("rateio.listaRateioitem listaRateioitem")
				.join("listaRateioitem.projeto projeto")
				.leftOuterJoin("listaRateioitem.contagerencial contagerencial")
				.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
				.whereIn("contrato.cdcontrato", whereIn)
				.list();
	}
	
	/**
	 * M�todo que busca o contrat com os dados da qtde de loca��o
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contrato> findForRomaneioreport(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("contrato.cdcontrato, contrato.dtiniciolocacao, contrato.ignoravalormaterial, contratotipo.faturamentoporitensdevolvidos, listaContratomaterial.cdcontratomaterial, listaContratomateriallocacao.qtde, listaContratomateriallocacao.dtmovimentacao, " +
				"listaContratomateriallocacao.contratomateriallocacaotipo, servico.cdmaterial, servico.nome, listaContratomateriallocacao.substituicao, " +
				"listaContratomaterial.valorunitario,  listaContratomaterial.qtde, patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta," +
				"listaContratomaterial.valorfechado, frequencia.cdfrequencia")
			.leftOuterJoin("contrato.contratotipo contratotipo")
			.leftOuterJoin("contrato.listaContratomaterial listaContratomaterial")
			.leftOuterJoin("listaContratomaterial.servico servico")
			.leftOuterJoin("listaContratomaterial.patrimonioitem patrimonioitem")
			.leftOuterJoin("listaContratomaterial.listaContratomateriallocacao listaContratomateriallocacao")
			.leftOuterJoin("contrato.frequencia frequencia")
			.whereIn("contrato.cdcontrato", whereIn)
			.list();
	}
	
	/**
	 * 
	 * @author Thiago Clemente
	 * 
	 */
	public List<Contrato> findForEnviarEmailResponsavelContrato() {
		return query()
				.select("contrato.cdcontrato, contrato.identificador, contrato.dtrenovacao, empresa.nome, empresa.razaosocial, empresa.email, cliente.nome," +
						"responsavel.email")
				.join("contrato.aux_contrato aux_contrato")
				.join("contrato.empresa empresa")
				.join("contrato.cliente cliente")
				.join("contrato.responsavel responsavel")
				.where("aux_contrato.situacao not in (?, ?, ?)", new Object[]{SituacaoContrato.FINALIZADO, SituacaoContrato.SUSPENSO, SituacaoContrato.CANCELADO})
				.where("contrato.dtrenovacao-current_date=1") 
				//.where("empresa.email is not null")
				.where("responsavel.email is not null")
				.list();
	}
	
	/**
	 * M�todo que busca os contrato para lan�ar o desconto
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contrato> findForLancardesconto(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("contrato.cdcontrato, contrato.descricao")
			.whereIn("contrato.cdcontrato", whereIn)
			.where("contrato.prazopagamento is null")
			.list();
	}
	
	/**
	 * Busca o contrato com o cliente para valida��o do agrupamento.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/07/2013
	 */
	public List<Contrato> findForValidacaoAgrupamentoCliente(String whereIn) {
		return query()
				.select("contrato.cdcontrato, contrato.identificador, cliente.cdpessoa, endereco.cdendereco, " +
						"contrato.dtiniciolocacao, contrato.dtfimlocacao, frequencia.cdfrequencia")
				.join("contrato.cliente cliente")
				.leftOuterJoin("contrato.endereco endereco")
				.leftOuterJoin("contrato.frequencia frequencia")
				.whereIn("contrato.cdcontrato", whereIn)
				.list();
	}
	
	/**
	 * Busca a lista de contrato com o identificador.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 09/10/2013
	 */
	public List<Contrato> findWithIdentificador(String whereIn) {
		return query()
				.select("contrato.cdcontrato, contrato.descricao, contrato.identificador, " +
						"cliente.cdpessoa, enderecoentrega.cdendereco, contratotipo.cdcontratotipo, " +
						"contratotipo.locacao, contratotipo.confirmarautomaticoprimeiroromaneio, " +
						"aux_contrato.situacao, contrato.dtproximovencimento, contrato.mes, contrato.ano, " +
						"frequenciarenovacao.cdfrequencia ")
				.leftOuterJoin("contrato.contratotipo contratotipo")
				.leftOuterJoin("contrato.enderecoentrega enderecoentrega")
				.leftOuterJoin("contrato.cliente cliente")
				.leftOuterJoin("contrato.aux_contrato aux_contrato")
				.leftOuterJoin("contrato.frequenciarenovacao frequenciarenovacao")
				.whereIn("contrato.cdcontrato", whereIn)
				.orderBy("contrato.cdcontrato")
				.list();
	}
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public boolean verificarConfirmarContratoLocacao(String whereIn){
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.join("contrato.contratotipo contratotipo")
				.whereIn("contrato.cdcontrato", whereIn)
				.where("contratotipo.locacao=?", Boolean.TRUE)
				.where("not exists (select 1 " +
					   "			from Contrato c" +
					   "			join c.listaRomaneioorigem listaRomaneioorigem" +
					   "			join listaRomaneioorigem.romaneio romaneio" +
					   "			join romaneio.romaneiosituacao romaneiosituacao" +
					   "			where c.cdcontrato=contrato.cdcontrato" +
					   "			and romaneiosituacao in (?, ?))", new Object[]{Romaneiosituacao.EM_ABERTO, Romaneiosituacao.BAIXADA})
				.unique()>0;
	}
	
	/**
	 * 
	 * @param contrato
	 * @author Thiago Clemente
	 * 
	 */
	public boolean isLocacao(Contrato contrato){
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.join("contrato.contratotipo contratotipo")
				.where("contrato=?", contrato)
				.where("contratotipo.locacao=?", Boolean.TRUE)
				.unique().longValue()>0;
	}
	
	/**
	 * M�todo que verifica se o contrato tem empresa com o municipio igual a brasilia
	 *
	 * @param whereInContrato
	 * @return
	 * @author Luiz Fernando
	 * @since 27/11/2013
	 */
	public Boolean isMunicipioBrasiliaAndNotContratomaterial(String whereInContrato) {
		return newQueryBuilderWithFrom(Long.class)
		.select("count(*)")
		.leftOuterJoin("contrato.empresa empresa")
		.leftOuterJoin("empresa.municipiosped municipiosped")
		.leftOuterJoin("empresa.ufsped ufsped")
		.leftOuterJoin("contrato.listaContratomaterial listaContratomaterial")
		.whereIn("contrato.cdcontrato", whereInContrato)
		.where("upper(retira_acento(municipiosped.nome)) = 'BRASILIA'")
		.where("listaContratomaterial is null")
		.unique()>0;
	}
	
	/**
	 * M�todo que busca os contrato para acompanhamento de meta
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 * @since 23/01/2014
	 */
	public List<Contrato> findForAcompanhamentometa(EmitiracompanhamentometaFiltro filtro) {
		return query()
			.select("contrato.cdcontrato, contrato.inss, contrato.incideinss, contrato.ir, contrato.incideir, contrato.valor, contrato.valoracrescimo, " +
					"contrato.iss, contrato.incideiss, contrato.icms, contrato.incideicms, contrato.dtproximovencimento, contrato.qtde, " +
					"contrato.pis, contrato.incidepis, contrato.cofins, contrato.incidecofins, contrato.csll, contrato.incidecsll, " +
					"listaContratodesconto.cdcontratodesconto, listaContratodesconto.valor, listaContratodesconto.dtinicio, listaContratodesconto.dtfim, " +
					"listaContratoparcela.cdcontratoparcela, listaContratoparcela.valor, listaContratoparcela.dtvencimento, " +
					"listaContratomaterial.cdcontratomaterial, listaContratomaterial.qtde, listaContratomaterial.valorunitario, " +
					"listaContratomaterial.dtinicio, listaContratomaterial.dtfim, listaContratomaterial.valordesconto, contrato.vendedortipo," +
					"contrato.valordedutivocomissionamento, vendedor.cdpessoa ")
			.join("contrato.aux_contrato aux_contrato")
			.leftOuterJoin("contrato.empresa empresa")
			.leftOuterJoin("contrato.listaContratodesconto listaContratodesconto")
			.leftOuterJoin("contrato.listaContratoparcela listaContratoparcela")
			.leftOuterJoin("contrato.listaContratomaterial listaContratomaterial")
			.leftOuterJoin("contrato.vendedor vendedor")
			.where("aux_contrato.situacao <> ?", SituacaoContrato.CANCELADO)
			.where("vendedor = ?", filtro.getColaborador())
			.where("empresa = ?", filtro.getEmpresa())
			.where("contrato.dtinicio >= ?", filtro.getDtreferenciaInicio())
			.where("contrato.dtinicio <= ?", filtro.getDtreferenciaFim() != null ? filtro.getDtreferenciaFim() : filtro.getFirstDateMonth())
			.list();
	}
	
	/**
	 * M�todo que carrega o contrato com o tipo de contrato
	 *
	 * @param contrato
	 * @return
	 * @author Luiz Fernando
	 * @since 13/12/2013
	 */
	public Contrato loadWithContratotipo(Contrato contrato){
		if(contrato == null || contrato.getCdcontrato() == null)
			throw new SinedException("Contrato n�o pode ser nulo.");
		
		return query()
			.select("contrato.cdcontrato, contratotipo.cdcontratotipo, contratotipo.nome, " +
					"contratotipo.locacao, contratotipo.faturarhoratrabalhada ")
			.leftOuterJoin("contrato.contratotipo contratotipo")
			.where("contrato = ?", contrato)
			.unique();
	}
	
	/**
	 * 
	 * @param param
	 * @return
	 * @author Lucas Costa
	 */
	public  List<Contrato> findContratoForAutocomplete(String q) {
		return query()
			.autocomplete()
			.select("contrato.cdcontrato, contrato.identificador, cliente.nome")
			.join("contrato.cliente cliente")
			.openParentheses()
				.whereLikeIgnoreAll("contrato.cdcontrato",q)
				.or()
				.whereLikeIgnoreAll("cliente.nome",q)
			.closeParentheses()
			.list();
	}
	
	/**
	 * M�todo que verifica se existe identificador cadastrado de acordo com a empresa
	 *
	 * @param contrato
	 * @param identificador
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 * @since 19/05/2014
	 */
	public boolean existsIdentificadorEmpresa(Contrato contrato, String identificador, Empresa empresa) {
		if(identificador == null || "".equals(identificador))
			return false;
		
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Contrato.class)
//				.join("contrato.aux_contrato aux_contrato")
				.leftOuterJoin("contrato.empresa empresa")
//				.where("aux_contrato.situacao <> ?", SituacaoContrato.CANCELADO)
				.where("contrato <> ?", contrato)
				.where("empresa = ?", empresa)
				.where("contrato.identificador = ?", identificador)
				.unique() > 0;
	}
	
	/**
	 * Atualiza o per�odo de loca��o do contrato
	 *
	 * @param contrato
	 * @param dtiniciolocacao
	 * @param dtfimlocacao
	 * @author Rodrigo Freitas
	 * @since 19/05/2014
	 */
	public void updatePeriodoLocacao(Contrato contrato, Date dtiniciolocacao, Date dtfimlocacao) {
		getHibernateTemplate().bulkUpdate("update Contrato c set c.dtiniciolocacao = ?, c.dtfimlocacao = ? where c = ?", new Object[]{
				dtiniciolocacao, dtfimlocacao, contrato
		});
	}
	
	/**
	 * Busca os contrato para a atualiza��o autom�tica do per�odo de loca��o
	 *
	 * @param whereInContrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/05/2014
	 */
	public List<Contrato> findForAtualizacaoPeriodoLocacao(String whereInContrato) {
		return query()
					.select("contrato.cdcontrato, contrato.dtiniciolocacao, contrato.dtfimlocacao, frequenciarenovacao.cdfrequencia")
					.join("contrato.frequenciarenovacao frequenciarenovacao")
					.whereIn("contrato.cdcontrato", whereInContrato)
					.list();
	}
	
	public boolean validaEstornoIdentificadorContrato(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			return false;
		
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Contrato.class)
				.leftOuterJoin("contrato.empresa empresa")
				.whereIn("contrato.cdcontrato", whereIn)
				.where("contrato.identificador is not null ")
				.where("contrato.identificador <> '' ")
				.where("exists (select 1 from Contrato c " +
						"join c.aux_contrato aux_c  " +
						"left outer join c.empresa e " +
						"where c.identificador is not null and c.identificador <> '' " +
						"and c.identificador = contrato.identificador " +
						"and aux_c.situacao <> " + SituacaoContrato.CANCELADO.getValue() +
						"and c.cdcontrato <> contrato.cdcontrato " +
						"and ((e.cdpessoa is null and empresa.cdpessoa is null) or (e.cdpessoa = empresa.cdpessoa)) " +
						")")
				.unique() > 0;
	}
	
	/**
	* M�todo que busca os contratos com o hist�rico
	*
	* @param whereInContrato
	* @return
	* @since 09/07/2014
	* @author Luiz Fernando
	*/
	public List<Contrato> findForVerificarUltimoRomaneio(String whereInContrato) {
		if (StringUtils.isEmpty(orderBy)) 
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("contrato.cdcontrato, listaContratohistorico.dthistorico, listaContratohistorico.observacao," +
						"listaContratohistorico.acao")
				.join("contrato.listaContratohistorico listaContratohistorico")
				.whereIn("contrato.cdcontrato", whereInContrato)
				.orderBy("contrato.cdcontrato, listaContratohistorico.dthistorico desc")
				.list();
	}
	
	/**
	 * M�todo que verifica se existe algum contrato finalizado para o Romaneio
	 * @param whereIn
	 * @return
	 */
	public boolean existeContratoFinalizado(String whereIn){
		if(whereIn == null || "".equals(whereIn))
			return false;
		
		return newQueryBuilderSined(Long.class)
		.select("count(*)")
		.from(Contrato.class)
		.whereIn("contrato.cdcontrato",whereIn)
		.openParentheses()
		.where("contrato.dtfim is not null")
		.or()
		.where("contrato.dtconclusao is not null")
		.closeParentheses()
		.unique() > 0;
	}
	
	/**
	 * Busca os contratos finalizados
	 * @param whereIn
	 * @return
	 */
	public List<Contrato> findContratosFinalizados(String whereIn) {
		return query()
				.select("contrato.cdcontrato, contrato.dtfim")
				.whereIn("contrato.cdcontrato", whereIn)
				.openParentheses()
		        .where("contrato.dtfim is not null")
		        .or()
		        .where("contrato.dtconclusao is not null")
		        .closeParentheses()
				.list();
	}
	
	/**
	 * Reabre um contrato finalizado, atualizando sua data fim
	 * @param contrato
	 */
	public void updateDtfimContrato(Contrato contrato) {
		getHibernateTemplate().bulkUpdate("update Contrato c set c.dtconclusao = null, c.dtfim = null where c.id = ?", new Object[]{contrato.getCdcontrato()});
	}
	
	/**
	* M�todo que carrega o contrato com a lista de parcela
	*
	* @param whereIn
	* @return
	* @since 14/07/2014
	* @author Luiz Fernando
	*/
	public List<Contrato> findWithParcela(String whereIn){
		if (StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("contrato.cdcontrato, contrato.identificador, listaContratoparcela.cdcontratoparcela, listaContratoparcela.valor, " +
					"listaContratoparcela.dtvencimento, listaContratoparcela.aplicarindice, " +
					"listaContratoparcela.cobrada, listaContratoparcela.desconto")
			.join("contrato.listaContratoparcela listaContratoparcela") 
			.whereIn("contrato.cdcontrato", whereIn)
			.orderBy("contrato.cdcontrato, listaContratoparcela.dtvencimento")
			.list();
	}
	
	/**
	 * M�todo verifica se existe contrato com o flag de "Faturamento em lote" marcado.
	 * @param whereIn
	 * @author Danilo Guimar�es
	 */
	public boolean existeFaturamentolote(String whereIn){
		if(StringUtils.isBlank(whereIn))
			return false;
		
		return newQueryBuilderSined(Long.class)
		.select("count(*)")
		.from(Contrato.class)
		.whereIn("contrato.cdcontrato",whereIn)
		.where("contrato.faturamentolote = true")
		.unique() > 0;
	}
	
	/**
	* M�todo que busca os contratos que a �ltima nota est� cancelada
	*
	* @param whereInNota
	* @return
	* @since 07/11/2014
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public List<Contrato> buscarContratoAposUltimaNotaCancelada(String whereInNota) {
		if (StringUtils.isEmpty(whereInNota))
			throw new SinedException("Par�metros inv�lidos.");
			
		String sql = "select distinct query.cdcontrato " +
					"from ( " +
					"select nc.cdcontrato, max(nc.cdnotacontrato) as cdnotacontrato " +
					"from notacontrato nc " +
					"join nota n on n.cdnota = nc.cdnota " +
					"where exists (select nc3.cdnotacontrato from notacontrato nc3 " +
					"				where nc.cdcontrato = nc3.cdcontrato " +
					"				and nc3.cdnota in (" + whereInNota + ") ) " +
					"group by nc.cdcontrato " +
					") query " +
					"where exists ( " +
					"select n2.cdnotastatus " +
					"from notacontrato nc2  " +
					"join nota n2 on n2.cdnota = nc2.cdnota " +
					"where nc2.cdnotacontrato = query.cdnotacontrato " +
					"and n2.cdnotastatus = " + NotaStatus.CANCELADA.getCdNotaStatus() + " " +
					"and n2.cdnotatipo = " + NotaTipo.NOTA_FISCAL_SERVICO.getCdNotaTipo() + " " +
					") ";

		SinedUtil.markAsReader();
		List<Contrato> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Contrato mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Contrato(rs.getInt("cdcontrato"));
				}
		});
		
		return lista;
	}
	public boolean existeContratoAtivo(Cliente cliente, Contagerencial contagerencialMensalidade, Projeto projetoW3erp, Projeto projetoSindis, Projeto projetoSupergov) {
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.join("contrato.aux_contrato aux_contrato")
				.join("contrato.rateio rateio")
				.join("rateio.listaRateioitem rateioitem")
				.from(Contrato.class)
				.where("contrato.cliente = ?",cliente)
				.openParentheses()
					.where("rateioitem.projeto = ?", projetoSindis)
					.or()
					.where("rateioitem.projeto = ?", projetoW3erp)
					.or()
					.where("rateioitem.projeto = ?", projetoSupergov)
				.closeParentheses()
				.where("rateioitem.contagerencial = ?", contagerencialMensalidade)
				.where("aux_contrato.situacao <> ?", SituacaoContrato.SUSPENSO)
				.where("aux_contrato.situacao <> ?", SituacaoContrato.CANCELADO)
				.where("aux_contrato.situacao <> ?", SituacaoContrato.FINALIZADO)
				.unique() > 0;
	}
	
	/**
	 * Verifica se o contrato � do tipo de medi��o
	 * @param contrato
	 * @return
	 */
	public boolean isContratoMedicao(Contrato contrato) {
		if(contrato == null || contrato.getCdcontrato() == null){
			throw new SinedException("O par�metro contrato n�o pode ser nulo.");
		}
		
		return newQueryBuilderSined(Long.class)
		.select("count(*)")
		.leftOuterJoin("contrato.contratotipo contratotipo")
		.from(Contrato.class)
		.where("contratotipo.medicao = ?", Boolean.TRUE)
		.where("contrato = ?", contrato)
		.unique() > 0;
	}
	
		
	/**
	 * M�todo verifica se o tipo de contrato utilizado estiver marcado com a op��o de 'Faturamento via webservice'
	 * @author Danilo Guimar�es
	 */
	public boolean existeFaturamentoWebService(String whereIn){
		if(StringUtils.isBlank(whereIn))
			return false;
		
		return newQueryBuilderSined(Long.class)
		.select("count(*)")
		.from(Contrato.class)
		.leftOuterJoin("contrato.contratotipo contratotipo")
		.whereIn("contrato.cdcontrato", whereIn)
		.where("contratotipo.faturamentowebservice = true")
		.unique() > 0;
	}
	
	public Contrato loadForWS(Contrato contrato) {
		if(contrato==null || contrato.getCdcontrato()==null)
			throw new RuntimeException("Contrato n�o pode ser nulo.");
		
		return query()
		.entity(contrato)
		.leftOuterJoinFetch("contrato.listaContratomaterial listaContratomaterial")
		.leftOuterJoinFetch("listaContratomaterial.servico servico")
		.unique();
	}
	
	/**
	 * M�todo respons�vel por encontrar os contratos v�nculados com centro custo especifico
	 * para valida��o
	 * @param centrocusto
	 * @return
	 * @since 17/03/2016
	 * @author C�sar
	 */
	public List<Contrato> findByCentroCustoValidacao (Centrocusto centrocusto){
		
		if(centrocusto == null)
			throw new SinedException("Par�metro incorreto para consulta");
		
		return query()
				.select("contrato.cdcontrato,contrato.identificador, contrato.dtfim, aux_contrato.cdcontrato,aux_contrato.situacao ")
				.join("contrato.rateio rateio")
				.join("rateio.listaRateioitem rateioitem")
				.join("contrato.aux_contrato aux_contrato")
				.where("rateioitem.centrocusto = ?", centrocusto)
				.list();
	}
	/**
	 * M�todo respons�vel por encontrar os contratos v�nculados com conta gerencial especifica
	 * para valida��o
	 * @param centrocusto
	 * @return
	 * @since 17/03/2016
	 * @author C�sar
	 */
	public List<Contrato> findByContaGerencialValidacao (Contagerencial contagerencial){
		
		if(contagerencial == null)
			throw new SinedException("Par�metro incorreto para consulta");
		
		return query()
				.select("contrato.cdcontrato,contrato.identificador, contrato.dtfim, aux_contrato.cdcontrato,aux_contrato.situacao ")
				.join("contrato.rateio rateio")
				.join("rateio.listaRateioitem rateioitem")
				.join("contrato.aux_contrato aux_contrato")
				.where("rateioitem.contagerencial = ?", contagerencial)
				.list();
	}
	
	/**
	 * M�todo respons�vel por encontrar os contratos v�nculados com conta gerencial especifica
	 * para valida��o
	 * @param conta contabil
	 * @return
	 * @since 29/10/2019	
	 * @author Arthur Gomes
	 */
	public List<Contrato> findByContaGerencialValidacao (ContaContabil contacontabil){
		
		if(contacontabil == null)
			throw new SinedException("Par�metro incorreto para consulta");
		
		return query()
				.select("contrato.cdcontrato,contrato.identificador, contrato.dtfim, aux_contrato.cdcontrato,aux_contrato.situacao ")
				.join("contrato.rateio rateio")
				.join("rateio.listaRateioitem rateioitem")
				.join("contrato.aux_contrato aux_contrato")
				.where("rateioitem.contacontabil = ?", contacontabil)
				.list();
	}
	
	/**
	* M�todo que verifica se existe algum contrato n�o finalizado e com todas as parcelas cobradas
	*
	* @param whereIn
	* @return
	* @since 24/03/2016
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public Boolean existeContratoComTodasParcelasCobradas(String whereIn) {
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		String sql = " select cp.cdcontrato as cdcontrato, count(cp.cdcontratoparcela), sum(CASE WHEN cp.cobrada = true THEN 1 ELSE 0 END) " +
			 		 " from Contratoparcela cp " +
					 " where cp.cdcontrato in (" + whereIn + ") " +
					 " group by cp.cdcontrato " +
					 " having count(cp.cdcontratoparcela) = sum(CASE WHEN cp.cobrada = true THEN 1 ELSE 0 END) ";

		SinedUtil.markAsReader();
		List<Contrato> lista = getJdbcTemplate().query(sql, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Contrato(rs.getInt("cdcontrato"));
			}
		});
		
		return SinedUtil.isListNotEmpty(lista);
	}
	
	/**
	 * M�todo respons�vel por retornar dados a serem apresentado no dashboard
	 * @param dataAtual
	 * @param dataLimite
	 * @return
	 * @since 27/06/2016
	 * @author C�sar
	 */
	public List<Contrato> findForPainelContratos(Date dataAtual, Date dataLimite){
		if(dataAtual == null || dataLimite == null){
			throw new SinedException("Par�metro Inv�lido para consulta");
		}
		return querySined()
					
					.select("contrato.cdcontrato, contrato.identificador, contrato.dtproximovencimento, contrato.descricao," +
							"cliente.cdpessoa, cliente.nome")
					.leftOuterJoin("contrato.cliente cliente")
					.leftOuterJoin("contrato.aux_contrato aux_contrato")
					.where("contrato.dtproximovencimento >= ?", dataAtual)
					.where("contrato.dtproximovencimento <= ?", dataLimite)
					.where("aux_contrato.situacao in (?, ?, ?)", new Object[]{SituacaoContrato.A_CONSOLIDAR, 
																				SituacaoContrato.ATENCAO, SituacaoContrato.ATRASADO})
					.list();
	}
	
	/**
	 * Verifica se existe produto e servi�o no contrato
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/09/2016
	 */
	public boolean haveContratoProdutoServico(String whereIn) {
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Contrato.class)
				.whereIn("contrato.cdcontrato", whereIn)
				.openParentheses()
					.where("contrato.ignoravalormaterial = false")
					.or()
					.where("contrato.ignoravalormaterial is null")
				.closeParentheses()
				.where("exists(select 1 from Contratomaterial cm where cm.contrato = contrato)")
				.unique() > 0;
	}
	
	public boolean haveContratoProduto(String whereIn) {
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Contrato.class)
				.whereIn("contrato.cdcontrato", whereIn)
				.openParentheses()
					.where("contrato.ignoravalormaterial = false")
					.or()
					.where("contrato.ignoravalormaterial is null")
				.closeParentheses()
				.where("exists(select 1 from Contratomaterial cm join cm.servico m where cm.contrato = contrato and (coalesce(m.produto, false) = true or coalesce(m.tributacaoestadual, false) = true))")
				.unique() > 0;
	}

	/**
	 * Atualiza o identificador de um contrato
	 *
	 * @param contrato
	 * @param identificador
	 * @author Rodrigo Freitas
	 * @since 09/02/2017
	 */
	public void updateIdentificador(Contrato contrato, String identificador) {
		getHibernateTemplate().bulkUpdate("update Contrato c set c.identificador = ? where c.id = ?",new Object[]{identificador, contrato.getCdcontrato()});
	}

	/**
	 * Retorna os contratos que encontram-se cancelados, conforme whereIn passado como par�metro.
	 *
	 * @param whereIn
	 * @author Mairon Cezar
	 * @since 13/03/2017
	 */
	public List<Contrato> findContratosCancelados(String whereIn){
		return query()
				.select("contrato.cdcontrato, contrato.descricao, contrato.identificador, contrato.dtcancelamento ")
				.whereIn("contrato.cdcontrato", whereIn)
				.where("contrato.dtcancelamento is not null")
				.orderBy("contrato.cdcontrato")
				.list();
	}
	
	public void updateReajuste(Contrato c, Boolean reajuste, Double percentualreajuste) {
		if (percentualreajuste==null){
			getHibernateTemplate().bulkUpdate("update Contrato c set c.reajuste = ?, c.percentualreajuste=null where c.id = ?", new Object[]{reajuste, c.getCdcontrato()});
		}else {
			getHibernateTemplate().bulkUpdate("update Contrato c set c.reajuste = ?, c.percentualreajuste=? where c.id = ?", new Object[]{reajuste, percentualreajuste, c.getCdcontrato()});
		}
	}
	
	public List<Contrato> findForArquivoRetornoContrato(ArquivoRetornoContratoFiltro filtro){
		return query()
					.select("contrato.cdcontrato, contrato.identificador, contrato.descricao, contrato.dtproximovencimento, contrato.valor," +
							"cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj, aux_contrato.situacao")
					.join("contrato.cliente cliente")
					.join("contrato.aux_contrato aux_contrato")
					.where("contrato.contratotipo=?", filtro.getContratotipo())
					.where("aux_contrato.situacao != ?", SituacaoContrato.CANCELADO)
					.where("aux_contrato.situacao != ?", SituacaoContrato.FINALIZADO)
					.where("not exists (select 1 from Contratoparcela cp where cp.contrato=contrato)")
					.list();
	}
	
	public void transactionArquivoRetornoContrato(final ArquivoRetornoContratoFiltro filtro){
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				for (ArquivoRetornoContratoBean bean: filtro.getListaBean()){
					if (Boolean.TRUE.equals(bean.getSelecao())){
						if(ArquivoRetornoContratoFiltro.TIPO_SCPC.equals(filtro.getTipo())){
							if(SinedUtil.isListNotEmpty(bean.getListaItens())){
								Contrato contrato = bean.getContrato();
								for(ArquivoRetornoContratoItemBean item : bean.getListaItens()){
									if(item.getContratomaterial() != null){
										if(item.getContratomaterial().getCdcontratomaterial() != null){
											contratomaterialService.updateCamposRetornoContrato(item.getContratomaterial());
										}else {
											contratomaterialService.saveOrUpdateNoUseTransaction(item.getContratomaterial());
										}
									}
								}
								
								if(bean.getRateio() != null){
									rateioService.saveOrUpdateNoUseTransaction(bean.getRateio());
								}
								if(bean.getValorContrato() != null){
									getHibernateTemplate().bulkUpdate("update Contrato c set c.valor=? where c=?", new Object[]{bean.getValorContrato(), contrato});
								}
								
								String historico = "Processamento de arquivo de retorno da remessa " + filtro.getNumeroremessa() + ".";
								Contratohistorico contratohistorico = new Contratohistorico(contrato, new Timestamp(System.currentTimeMillis()), Contratoacao.ALTERADO, SinedUtil.getUsuarioLogado(), historico);
								contratohistoricoService.saveOrUpdateNoUseTransaction(contratohistorico);
							}
						}else {
							Contrato contrato = bean.getContrato();
							
							getHibernateTemplate().bulkUpdate("update Contrato c set c.valor=? where c=?", new Object[]{bean.getValorArquivo(), contrato});
							
							String historico = "Valor atualizado de R$" + contrato.getValor().toString() + " para R$" + bean.getValorArquivo().toString();
							Contratohistorico contratohistorico = new Contratohistorico(contrato, new Timestamp(System.currentTimeMillis()), Contratoacao.ARQUIVO_RETORNO_CONTRATO, SinedUtil.getUsuarioLogado(), historico);
							contratohistoricoService.saveOrUpdateNoUseTransaction(contratohistorico);
						}
					}
				}
				
				if(filtro.getArquivo() != null){
					Usuario usuario = SinedUtil.getUsuarioLogado();
					Arquivoretornocontrato arquivoretornocontrato = new Arquivoretornocontrato();
					arquivoretornocontrato.setNumeroremessa(filtro.getNumeroremessa());
					arquivoretornocontrato.setArquivo(filtro.getArquivo());
					arquivoretornocontrato.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
					arquivoretornocontrato.setDtaltera(new Timestamp(System.currentTimeMillis()));
					arquivoretornocontratoService.saveOrUpdate(arquivoretornocontrato);
				}
				
				return null;
			}
		});
	}
	
	public boolean existeContratotipo(Contrato contrato) {
		if(contrato == null || contrato.getContratotipo() == null)
			throw new SinedException("Par�metro inv�lido.");
			
		List<SituacaoContrato> lista = new ArrayList<SituacaoContrato>();
		lista.add(SituacaoContrato.EM_ESPERA);
		lista.add(SituacaoContrato.A_CONSOLIDAR);
		lista.add(SituacaoContrato.NORMAL);
		lista.add(SituacaoContrato.ATENCAO);
		lista.add(SituacaoContrato.ISENTO);
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Contrato.class)
			.join("contrato.aux_contrato aux_contrato")
			.whereIn("aux_contrato.situacao", SituacaoContrato.listAndConcatenateValue(lista))
			.where("contrato.contratotipo = ?", contrato.getContratotipo())
			.where("contrato.empresa = ?", contrato.getEmpresa())
			.where("contrato.cliente = ?", contrato.getCliente())
			.where("contrato.cdcontrato <> ?", contrato.getCdcontrato())
			.unique() > 0;
	}
	
	public List<Contrato> findForIndenizacao(Cliente cliente){
		return query()
			.select("contrato.cdcontrato, contrato.descricao, contrato.identificador")
			.join("contrato.cliente cliente")
			.where("cliente = ?", cliente)
			.where("exists (select 1 from Contratofaturalocacao cfl where cfl.contrato=contrato and cfl.indenizacao=true)")
			.orderBy("contrato.cdcontrato")
			.list();
	}
	
	public List<Contrato> findByEmitirindenizacaofiltro(EmitirIndenizacaoContratoFiltro filtro){
		return query()
			.select("contrato.cdcontrato, contrato.descricao, contrato.identificador, " +
					"cliente.nome, cliente.razaosocial")
			.join("contrato.cliente cliente")
			.where("contrato = ?", filtro.getContrato())
			.where("cliente = ?", filtro.getCliente())
			.where("contrato.empresa = ?", filtro.getEmpresa())
			.where("contrato.dtinicio >= ?", filtro.getDtinicio())
			.where("contrato.dtinicio <= ?", filtro.getDtfim())
			.orderBy("contrato.cdcontrato")
			.list();
	}
	
	public Contrato loadContratoByCriarRequisicao(Contrato contrato){
		return query()
			.select("contrato.cdcontrato, contrato.descricao, contrato.identificador, " +
					"cliente.cdpessoa, cliente.nome, cliente.razaosocial, " +
					"empresa.cdpessoa, empresa.nome, empresa.razaosocial, " +
					"responsavel.cdpessoa, responsavel.nome, " +
					"endereco.cdendereco, " +
					"contato.cdpessoa, contato.nome, contato.responsavelos, " +
					"contratomaterial.cdcontratomaterial, contratomaterial.qtde, contratomaterial.valorunitario, " +
					"contratomaterial.periodocobranca, contratomaterial.valordesconto, contratomaterial.observacao, " +
					"material.cdmaterial, material.nome, " +
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo")
			.join("contrato.cliente cliente")
			.leftOuterJoin("contrato.empresa empresa")
			.leftOuterJoin("contrato.responsavel responsavel")
			.leftOuterJoin("contrato.endereco endereco")
			.leftOuterJoin("cliente.listaContato contatoPessoa")
			.leftOuterJoin("contatoPessoa.contato contato")
			.leftOuterJoin("contrato.listaContratomaterial contratomaterial")
			.leftOuterJoin("contratomaterial.servico material")
			.leftOuterJoin("material.unidademedida unidademedida")
			.where("contrato = ?", contrato)
			.unique();
	}
	
	public Contrato findForCancelarRomaneio(Contrato contrato) {
		return query()
			.select("contrato.cdcontrato,contrato.dtfim, servico.cdmaterial, " +
					"listaContratomaterial.cdcontratomaterial, listaContratomateriallocacao.cdcontratomateriallocacao, " +
					"listaContratomateriallocacao.qtde, servico.cdmaterial, patrimonioitem.cdpatrimonioitem, " +
					"listaContratomateriallocacao.contratomateriallocacaotipo, listaContratomateriallocacao.cdromaneio ")
			.leftOuterJoin("contrato.listaContratomaterial listaContratomaterial")
			.leftOuterJoin("listaContratomaterial.servico servico")
			.leftOuterJoin("listaContratomaterial.listaContratomateriallocacao listaContratomateriallocacao")
			.leftOuterJoin("listaContratomaterial.patrimonioitem patrimonioitem")
			.where("contrato = ?", contrato)
			.unique();
	}
	/**
	 * Carrega um contrato com as informa��es para determinar o endere�o priorit�rio do contrato.
	 * @param contrato
	 * @author Murilo
	 * @return contrato com seus endere�os (endereco, enderecoentrega, enderecocobranca e cliente.listaEndereco)
	 */
	public Contrato loadContratoWithEndereco(Contrato contrato) {
		if(contrato == null || contrato.getCdcontrato() == null)
			return contrato;
		return query()
				.select("contrato.cdcontrato, contrato.formafaturamento, cliente.cdpessoa, endereco.cdendereco, enderecotipo.cdenderecotipo, enderecofaturamento.cdendereco, " +
						"enderecotipofaturamento.cdenderecotipo, enderecoentrega.cdendereco, enderecotipoentrega.cdenderecotipo, enderecocobranca.cdendereco," +
						" enderecotipocobranca.cdenderecotipo")
				.leftOuterJoin("contrato.endereco enderecofaturamento")
				.leftOuterJoin("enderecofaturamento.enderecotipo enderecotipofaturamento")
				.leftOuterJoin("contrato.enderecoentrega enderecoentrega")
				.leftOuterJoin("enderecoentrega.enderecotipo enderecotipoentrega")
				.leftOuterJoin("contrato.enderecocobranca enderecocobranca")
				.leftOuterJoin("enderecocobranca.enderecotipo enderecotipocobranca")
				.leftOuterJoin("contrato.cliente cliente")
				.leftOuterJoin("cliente.listaEndereco endereco")
				.leftOuterJoin("endereco.enderecotipo enderecotipo")
				.where("contrato = ?", contrato)
				.unique();
	}
	public boolean isOrigemOrcamento(Integer cdvendaorcamento) {
		return newQueryBuilderSined(Long.class)
				
				.select("count(*)")
				.from(Contrato.class)
				.join("contrato.vendaorcamento vendaorcamento")
				.join("contrato.aux_contrato aux_contrato")
				.where("vendaorcamento.cdvendaorcamento = ?", cdvendaorcamento)
				.where("aux_contrato.situacao <> ?", SituacaoContrato.CANCELADO)
				.unique() > 0;
	}
}