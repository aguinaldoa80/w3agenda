package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Arquivotef;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivotefsituacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ArquivotefDAO extends GenericDAO<Arquivotef> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Arquivotef> query, FiltroListagem _filtro) {
		query.leftOuterJoinFetch("arquivotef.configuracaotef configuracaotef");
		query.leftOuterJoinFetch("configuracaotef.empresa empresa");
		query.leftOuterJoinFetch("arquivotef.arquivoenvio arquivoenvio");
		query.leftOuterJoinFetch("arquivotef.arquivoretorno arquivoretorno");
		query.leftOuterJoinFetch("arquivotef.arquivoretornopagamento arquivoretornopagamento");
		query.orderBy("arquivotef.cdarquivotef desc");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Arquivotef> query) {
		query
			.select("arquivotef.cdarquivotef, arquivotef.dtenvio, arquivotef.situacao, arquivotef.dtaltera, arquivotef.cdusuarioaltera, " +
					"configuracaotef.cdconfiguracaotef, configuracaotef.chaveintegracao, configuracaotef.impressaoautomatica, " +
					"empresa.cdpessoa, empresa.nome, " +
					"arquivoenvio.cdarquivo, arquivoenvio.nome, arquivoenvio.tipoconteudo, arquivoenvio.tamanho, arquivoenvio.dtmodificacao, " +
					"arquivoretorno.cdarquivo, arquivoretorno.nome, arquivoretorno.tipoconteudo, arquivoretorno.tamanho, arquivoretorno.dtmodificacao, " +
					"arquivoretornopagamento.cdarquivo, arquivoretornopagamento.nome, arquivoretornopagamento.tipoconteudo, arquivoretornopagamento.tamanho, arquivoretornopagamento.dtmodificacao, " +
					"listaArquivotefhistorico.cdarquivotefhistorico, listaArquivotefhistorico.acao, listaArquivotefhistorico.observacao, listaArquivotefhistorico.cdusuarioaltera, listaArquivotefhistorico.dtaltera, " +
					"listaArquivotefitem.cdarquivotefitem, " +
					"notafiscalprodutoduplicata.cdnotafiscalprodutoduplicata, notafiscalprodutoduplicata.numero, notafiscalprodutoduplicata.valor, " +
					"documentotipo.cddocumentotipo, documentotipo.meiopagamentonfe, documentotipo.nome, documentotipo.tipointegracaonfe, documentotipo.cnpjcredenciadoranfe, documentotipo.bandeiracartaonfe, " +
					"notafiscalproduto.cdNota, notafiscalproduto.modeloDocumentoFiscalEnum, notafiscalproduto.dtEmissao, " +
					"cliente.cdpessoa, cliente.nome, " +
					"vendapagamento.cdvendapagamento, vendapagamento.dataparcela, vendapagamento.valororiginal, " +
					"documentotipoVenda.cddocumentotipo, documentotipoVenda.meiopagamentonfe, documentotipoVenda.tipointegracaonfe, documentotipoVenda.nome, documentotipoVenda.cnpjcredenciadoranfe, documentotipoVenda.bandeiracartaonfe, " +
					"venda.cdvenda, venda.dtvenda, " +
					"clienteVenda.cdpessoa, clienteVenda.nome")
			.leftOuterJoin("arquivotef.arquivoenvio arquivoenvio")
			.leftOuterJoin("arquivotef.arquivoretorno arquivoretorno")
			.leftOuterJoin("arquivotef.arquivoretornopagamento arquivoretornopagamento")
			.leftOuterJoin("arquivotef.configuracaotef configuracaotef")
			.leftOuterJoin("configuracaotef.empresa empresa")
			.leftOuterJoin("arquivotef.listaArquivotefhistorico listaArquivotefhistorico")
			.leftOuterJoin("arquivotef.listaArquivotefitem listaArquivotefitem")
			.leftOuterJoin("listaArquivotefitem.notafiscalprodutoduplicata notafiscalprodutoduplicata")
			.leftOuterJoin("notafiscalprodutoduplicata.documentotipo documentotipo")
			.leftOuterJoin("notafiscalprodutoduplicata.notafiscalproduto notafiscalproduto")
			.leftOuterJoin("notafiscalproduto.cliente cliente")
			.leftOuterJoin("listaArquivotefitem.vendapagamento vendapagamento")
			.leftOuterJoin("vendapagamento.documentotipo documentotipoVenda")
			.leftOuterJoin("vendapagamento.venda venda")
			.leftOuterJoin("venda.cliente clienteVenda");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaArquivotefitem");
	}

	public void updateArquivoretorno(Arquivotef arquivotef) {
		getHibernateTemplate().bulkUpdate("update Arquivotef a set a.arquivoretorno = ? where a.id = ?", 
				new Object[]{arquivotef.getArquivoretorno(), arquivotef.getCdarquivotef()});
	}

	public void updateArquivoenvio(Arquivotef arquivotef) {
		getHibernateTemplate().bulkUpdate("update Arquivotef a set a.arquivoenvio = ? where a.id = ?", 
				new Object[]{arquivotef.getArquivoenvio(), arquivotef.getCdarquivotef()});
	}

	public void updateSituacao(Arquivotef arquivotef, Arquivotefsituacao situacao) {
		getHibernateTemplate().bulkUpdate("update Arquivotef a set a.situacao = ? where a.id = ?", 
				new Object[]{situacao, arquivotef.getCdarquivotef()});
	}

	public void updateArquivoretornopagamento(Arquivotef arquivotef) {
		getHibernateTemplate().bulkUpdate("update Arquivotef a set a.arquivoretornopagamento = ? where a.id = ?", 
				new Object[]{arquivotef.getArquivoretornopagamento(), arquivotef.getCdarquivotef()});
	}

	public List<Arquivotef> findByVenda(String whereInVenda) {
		if(StringUtils.isBlank(whereInVenda)) return null;
		return query()
					.select("arquivotef.cdarquivotef, arquivotef.situacao, vendapagamento.cdvendapagamento, venda.cdvenda, arquivoretornopagamento.cdarquivo")
					.leftOuterJoin("arquivotef.listaArquivotefitem listaArquivotefitem")
					.leftOuterJoin("listaArquivotefitem.vendapagamento vendapagamento")
					.leftOuterJoin("vendapagamento.venda venda")
					.leftOuterJoin("arquivotef.arquivoretornopagamento arquivoretornopagamento")
					.whereIn("venda.cdvenda", whereInVenda)
					.list();
	}

	public List<Arquivotef> findByNota(String whereInNota) {
		if(StringUtils.isBlank(whereInNota)) return null;
		return query()
					.select("arquivotef.cdarquivotef, arquivotef.situacao, notafiscalprodutoduplicata.cdnotafiscalprodutoduplicata, notafiscalproduto.cdNota, arquivoretornopagamento.cdarquivo")
					.leftOuterJoin("arquivotef.listaArquivotefitem listaArquivotefitem")
					.leftOuterJoin("listaArquivotefitem.notafiscalprodutoduplicata notafiscalprodutoduplicata")
					.leftOuterJoin("notafiscalprodutoduplicata.notafiscalproduto notafiscalproduto")
					.leftOuterJoin("arquivotef.arquivoretornopagamento arquivoretornopagamento")
					.whereIn("notafiscalproduto.cdNota", whereInNota)
					.list();
	}
	
}
