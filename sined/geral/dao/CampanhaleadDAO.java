package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Campanha;
import br.com.linkcom.sined.geral.bean.Campanhalead;
import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CampanhaleadDAO extends GenericDAO<Campanhalead>{
	
	public List<Campanhalead> findByCampanha(Campanha campanha){
		
		if(campanha == null || campanha.getCdcampanha() == null){
			throw new SinedException("O par�metro campanha e cdcampanha n�o podem ser null.");
		}
		return 
			query()
				.select("campanhalead.cdcampanhalead, campanha.cdcampanha, " +
						"lead.cdlead, lead.nome, lead.empresa, responsavel.cdpessoa, " +
						"responsavel.nome, listleademail.cdleademail, listleademail.email ")
				.leftOuterJoin("campanhalead.campanha campanha")
				.leftOuterJoin("campanhalead.lead lead")
				.leftOuterJoin("lead.responsavel responsavel")
				.leftOuterJoin("lead.listleademail listleademail")
				.where("campanha = ?",campanha)
				.orderBy("campanha.cdcampanha")
				.list();
		
	}
	
	public List<Campanhalead> loadByLead(Lead lead){
		
		if(lead == null || lead.getCdlead() == null){
			throw new SinedException("O par�metro lead e cdlead n�o podem ser null.");
		}
		return 
			query()
				.select("campanhalead.cdcampanhalead, lead.cdlead, " +
						"lead.nome, lead.empresa, lead.responsavel, " +
						"listleademail.cdleademail, listleademail.email ")
				.leftOuterJoin("campanhalead.lead lead")
				.leftOuterJoin("lead.listleademail listleademail")
				.where("lead = ?",lead)
				.orderBy("lead.cdlead")
				.list();
		
	}

}
