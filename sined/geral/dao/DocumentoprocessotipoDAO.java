package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Documentoprocessotipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.DocumentoprocessotipoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentoprocessotipoDAO extends GenericDAO<Documentoprocessotipo>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Documentoprocessotipo> query,FiltroListagem _filtro) {
		DocumentoprocessotipoFiltro filtro = (DocumentoprocessotipoFiltro) _filtro;
		query
			.select("documentoprocessotipo.cddocumentoprocessotipo, documentoprocessotipo.nome," +
					"documentoprocessotipo.ativo")
					.where("documentoprocessotipo.ativo=?", filtro.getAtivo())
					.whereLikeIgnoreAll("documentoprocessotipo.nome", filtro.getNome());
		
	}
	
	/**
	 * Retorna os documentos ativos na ordena��o passada
	 * @param orderBy
	 * @return
	 * @author C�ntia Nogueira
	 */
	public List<Documentoprocessotipo> findAtivos(String orderBy){
		return query()
		.select("documentoprocessotipo.cddocumentoprocessotipo, documentoprocessotipo.nome")
		.where("documentoprocessotipo.ativo = ?", Boolean.TRUE)
		.orderBy((orderBy!=null && !orderBy.equals(""))?orderBy:"documentoprocessotipo.cddocumentoprocessotipo")
		.list();
	}

}
