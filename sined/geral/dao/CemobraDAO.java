package br.com.linkcom.sined.geral.dao;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Cemobra;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CemobraDAO extends GenericDAO<Cemobra>{
	
	private ArquivoDAO arquivoDAO;

	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				
				Cemobra cemobra = (Cemobra) save.getEntity();
				if(cemobra.getArquivo() != null){
					arquivoDAO.saveFile(cemobra, "arquivo");
				}
				save.saveOrUpdateManaged("listaCemcomponenteacumulado");
				save.saveOrUpdateManaged("listaCemperfilacumulado");
				return null;
			}
		});
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Cemobra> query) {
		query
			.leftOuterJoinFetch("cemobra.arquivo arquivo")
			.leftOuterJoinFetch("cemobra.listaCemcomponenteacumulado listaCemcomponenteacumulado")
			.leftOuterJoinFetch("cemobra.listaCemperfilacumulado listaCemperfilacumulado")
//			.leftOuterJoinFetch("cemobra.listaCemtipologia listaCemtipologia")
//			.leftOuterJoinFetch("listaCemtipologia.listaCemcomponente listaCemcomponente")
//			.leftOuterJoinFetch("listaCemtipologia.listaCemperfil listaCemperfil")
//			.leftOuterJoinFetch("listaCemtipologia.listaCemvidro listaCemvidro")
//			.leftOuterJoinFetch("listaCemtipologia.listaCempainel listaCempainel")
			;
		
	}

}
