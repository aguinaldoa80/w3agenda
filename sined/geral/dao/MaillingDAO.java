package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clientehistorico;
import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.geral.bean.view.Vwemailcontatocliente;
import br.com.linkcom.sined.geral.service.ClientehistoricoService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.MaillingFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaillingDAO extends GenericDAO<Vwemailcontatocliente> {

	private ClientehistoricoService clienteHistoricoService;

	public void setClienteHistoricoService(
			ClientehistoricoService clienteHistoricoService) {
		this.clienteHistoricoService = clienteHistoricoService;
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Vwemailcontatocliente> query,
			FiltroListagem _filtro) {

		MaillingFiltro filtro = (MaillingFiltro) _filtro;
		query
				.select(
						"vwemailcontatocliente.id, cliente.cdpessoa, cliente.nome, vwemailcontatocliente.emailcontatocliente, " +
						"vwemailcontatocliente.cdcontato, vwemailcontatocliente.contatonome")
				.join("vwemailcontatocliente.cliente cliente")
				.whereLikeIgnoreAll("cliente.nome", filtro.getCliente())
				.where("cliente.ativo = ?", filtro.getAtivo());
		
		if(filtro.getCategoria() != null){
			query
				.leftOuterJoin("cliente.listaPessoacategoria listaPessoacategoria")
				.leftOuterJoin("listaPessoacategoria.categoria categoria")
				.where("categoria = ?", filtro.getCategoria())
				.ignoreJoin("listaPessoacategoria", "categoria");
		}
		
		if(filtro.getAniversariantesMes() != null || filtro.getAniversariantesDe() != null || filtro.getAniversariantesAte() != null){
			query.openParentheses()
				.openParentheses()
					.where("DATE_PART('MONTH', cliente.dtnascimento) = ?", filtro.getAniversariantesMes()!= null ? (filtro.getAniversariantesMes().ordinal()+1): null)
					.where("DATE_PART('DAY', cliente.dtnascimento) >= ?", filtro.getAniversariantesDe())
					.where("DATE_PART('DAY', cliente.dtnascimento) <= ?", filtro.getAniversariantesAte())
				.closeParentheses()
				.or()
				.openParentheses()
					.where("exists (select p.cdpessoa " +
							" from Pessoa p " +
							"where p.cdpessoa = vwemailcontatocliente.cdcontato " +
							(filtro.getAniversariantesMes() != null ? " and DATE_PART('MONTH', p.dtnascimento) = " + (filtro.getAniversariantesMes().ordinal()+1) : "") +
							(filtro.getAniversariantesDe() != null ? " and DATE_PART('DAY', p.dtnascimento) >= " + filtro.getAniversariantesDe() : "") +
							(filtro.getAniversariantesAte() != null ? " and DATE_PART('DAY', p.dtnascimento) <= " + filtro.getAniversariantesAte() : "") +
							") ")
				.closeParentheses()
			.closeParentheses();
		}
		
		if(filtro.getContatotipo()!=null){
			query
			.where("vwemailcontatocliente.cdcontato in (select contato.cdpessoa from Contato contato join contato.contatotipo contatotipo where contatotipo.cdcontatotipo = " + filtro.getContatotipo().getCdcontatotipo() + " )" );
		}
		
		if(filtro.getColaborador() != null || filtro.getColaboradorPrincipal() != null) {
			query
				.where("exists ( select 1 " +
								"from Clientevendedor cv " +
								"join cv.cliente cli " +
								"join cv.colaborador c " +
								"where cli.cdpessoa = cliente.cdpessoa " +
								"and c = ? " +
								"and coalesce(cv.principal, false) <> true)", filtro.getColaborador())
				.where("exists ( select 1 " +
								"from Clientevendedor cv " +
								"join cv.cliente cli " +
								"join cv.colaborador c " +
								"where cli.cdpessoa = cliente.cdpessoa " +
								"and c = ? " +
								"and coalesce(cv.principal, false) = true)", filtro.getColaboradorPrincipal());
		}
		
		if(filtro.getListaSegmento() != null) {
			if(filtro.getCombinarSegmentos()){
				Iterator<Segmento> iterator = filtro.getListaSegmento().iterator();
				StringBuilder segmentos = new StringBuilder();
				while (iterator.hasNext()){
					segmentos.append(iterator.next().getCdsegmento().toString());		
					if(iterator.hasNext()){
						segmentos.append("-");
					}
				}
				query.where("cliente.cdpessoa in (select vcs.cdcliente from Vclientesegmento vcs where array_to_string(vcs.segmentos, '-') ='" + segmentos.toString() + "')");
			}else{
				query
				.leftOuterJoin("cliente.listaClienteSegmento listaClienteSegmento")
				.leftOuterJoin("listaClienteSegmento.segmento segmento")
				.whereIn("segmento.cdsegmento", CollectionsUtil.listAndConcatenate(filtro.getListaSegmento(), "cdsegmento", ","));				
			}
		}
	}

	/**
	 * M�todo que retorna os clientes e contatos selecionados na tela de
	 * mailling para envio de email
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Vwemailcontatocliente> findClientesContatosByWhereIn(
			String whereIn) {
		if (whereIn == null || whereIn.equals(""))
			throw new SinedException("Par�metros inv�lidos!");

		return query()
				.select(
						"vwemailcontatocliente.id, cliente.cdpessoa, cliente.nome, cliente.login, cliente.senha, "
								+ "vwemailcontatocliente.emailcontatocliente, vwemailcontatocliente.cdcontato, "
								+ "vwemailcontatocliente.contatonome").join(
						"vwemailcontatocliente.cliente cliente").whereIn(
						"vwemailcontatocliente.id", whereIn).list();
	}

	/**
	 * M�todo que salva os hist�ricos de clientes apartir dos contatos
	 * selecionados na tela de envio de mailling
	 * 
	 * @param listaClientesContatos
	 * @return
	 * @author Igor Silv�rio Costa
	 * @param assunto 
	 */
	public void salvarHistoricosCliente(
			List<Vwemailcontatocliente> listaClientesContatos, String assunto) {
		// ArrayList<Clientehistorico> clientesHistorico = new
		// ArrayList<Clientehistorico>();
		Cliente cliente;
		List<Integer> listaIdCliente = new ArrayList<Integer>();
		for (Vwemailcontatocliente vwcliente : listaClientesContatos) {
			cliente = vwcliente.getCliente();
			if(cliente != null && cliente.getCdpessoa() != null && !listaIdCliente.contains(cliente.getCdpessoa())){
				Clientehistorico clientehistorico = new Clientehistorico();
				clientehistorico.setCliente(cliente);
				clientehistorico.setObservacao(cliente.getHistorico());
				clientehistorico.setDtaltera(cliente.getDtaltera());
				clientehistorico.setCdusuarioaltera(cliente.getCdusuarioaltera());
				clientehistorico.setObservacao("Email Enviado: " + assunto);
				clienteHistoricoService.saveOrUpdate(clientehistorico);
				listaIdCliente.add(cliente.getCdpessoa());
			}
		}
	}

	@Override
	public ListagemResult<Vwemailcontatocliente> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Vwemailcontatocliente> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("vwemailcontatocliente.cdcontato");
		
		return new ListagemResult<Vwemailcontatocliente>(query, filtro);
	}
}
