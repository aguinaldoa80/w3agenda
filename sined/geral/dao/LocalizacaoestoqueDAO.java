package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Localizacaoestoque;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("localizacaoestoque.descricao")
public class LocalizacaoestoqueDAO extends GenericDAO<Localizacaoestoque> {
	
}
