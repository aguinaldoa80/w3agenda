package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailitem;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EnvioemailitemDAO extends GenericDAO<Envioemailitem> {
	
	/**
	 * M�todo que retorna uma lista de item de emaisl enviados, de acordo com a tabela pai Envioemail
	 * 
	 * @param envioemail
	 * @author Thiago Clemente
	 * 
	 */
	public List<Envioemailitem> find(Envioemail envioemail){
		return query()
			.select("envioemailitem.cdenvioemailitem, envioemailitem.email, envioemailitem.dtconfirmacao, envioemail.assunto, envioemail.mensagem, pessoa.nome, pessoa.cdpessoa," +
					"envioemailitem.nomecontato, envioemailitem.situacaoemail, envioemailitem.ultimaverificacao")
			.leftOuterJoin("envioemailitem.envioemail envioemail")
			.leftOuterJoin("envioemailitem.pessoa pessoa")
			.where("envioemail=?", envioemail)
			.list();
	}
	
	/**
	 * M�todo que atualiza os emails lidos
	 * 
	 * @param cdenvioemail
	 * @param email
	 * @author Thiago Clemente
	 * 
	 */
	public void confirmacaoEmail(Integer cdenvioemail, String email){
		
		StringBuilder sql = new StringBuilder();
		sql.append(" update envioemailitem set dtconfirmacao=current_timestamp");
		sql.append(" where cdenvioemail=" + cdenvioemail);
		sql.append(" and TRIM(UPPER(email))='" + email.toUpperCase().trim() + "'");
		
		getJdbcTemplate().update(sql.toString());
	}
}