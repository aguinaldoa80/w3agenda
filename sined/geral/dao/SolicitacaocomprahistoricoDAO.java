package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Solicitacaocompraacao;
import br.com.linkcom.sined.geral.bean.Solicitacaocomprahistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class SolicitacaocomprahistoricoDAO extends GenericDAO<Solicitacaocomprahistorico>{

	public Solicitacaocomprahistorico getUsuarioLastAction(Solicitacaocompra solicitacaocompra, Solicitacaocompraacao acao) {
		return query()
					.select("solicitacaocomprahistorico.cdsolicitacaocomprahistorico, solicitacaocomprahistorico.cdusuarioaltera, solicitacaocomprahistorico.dtaltera")
					.where("solicitacaocomprahistorico.cdsolicitacaocomprahistorico = (select max(s.cdsolicitacaocomprahistorico) " +
																			"from Solicitacaocomprahistorico s " +
																			"join s.solicitacaocompraacao as acao " +
																			"join s.solicitacaocompra as os " +
																			"where acao.cdsolicitacaocompraacao = " + acao.getCdsolicitacaocompraacao() + " " +
																			"and os.cdsolicitacaocompra = " + solicitacaocompra.getCdsolicitacaocompra() + ")")
					.unique();
	}
	
	
}
