package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialformulapeso;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("materialformulapeso.ordem")
public class MaterialformulapesoDAO extends GenericDAO<Materialformulapeso>{

	/**
	 * Busca a lista de f�rmula para c�lculo do peso
	 *
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/09/2013
	 */
	public List<Materialformulapeso> findByMaterial(Material material) {
		return query()
					.select("materialformulapeso.cdmaterialformulapeso, materialformulapeso.ordem, " +
							"materialformulapeso.identificador, materialformulapeso.formula")
					.where("materialformulapeso.material = ?", material)
					.orderBy("materialformulapeso.ordem, materialformulapeso.identificador")
					.list();
	}

}
