package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Solicitacaocompraordemcompramaterial;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class SolicitacaocompraordemcompramaterialDAO extends GenericDAO<Solicitacaocompraordemcompramaterial>{

	
	/**
	* Busca o relacionamento de ordemcompramaterial com solicitacaocompra a partir do item da ordem de compra.
	*
	* @param whereIn
	* @return
	* @since 21/10/2016
	* @author Luiz Fernando
	*/
	public List<Solicitacaocompraordemcompramaterial> findByOrdemcompramaterial(String whereIn) {
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
					.select("solicitacaocompraordemcompramaterial.cdsolicitacaocompraordemcompramaterial, solicitacaocompraordemcompramaterial.qtde, " +
							"solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.identificador, ordemcompramaterial.cdordemcompramaterial ")
					.join("solicitacaocompraordemcompramaterial.solicitacaocompra solicitacaocompra")
					.join("solicitacaocompraordemcompramaterial.ordemcompramaterial ordemcompramaterial")
					.whereIn("ordemcompramaterial.cdordemcompramaterial", whereIn)
					.list();
	}

}
