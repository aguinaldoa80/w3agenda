package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedor;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.CotacaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CotacaoDAO extends GenericDAO<Cotacao> {
	
	private UsuarioService usuarioService;
	private MaterialcategoriaService materialcategoriaService;
	
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {this.materialcategoriaService = materialcategoriaService;}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Cotacao> query) {
		query
			.select("cotacao.cdcotacao, cotacao.cdusuarioaltera, cotacao.dtaltera, cotacao.dtcancelamento, cotacao.dtbaixa, cotacao.considerarlocalunicoentrega, " +
					"aux_cotacao.dtlimite, aux_cotacao.situacaosuprimentos, listaCotacaofornecedor.cdcotacaofornecedor, " +
					"fornecedor.cdpessoa, fornecedor.nome, contato.cdpessoa, listaCotacaofornecedor.conviteenviado, " +
					"empresa.cdpessoa, empresa.nome, listaCotacaofornecedor.desconto, prazopagamento.cdprazopagamento, listaCotacaofornecedor.observacao, " +
					"listaCotacaofornecedoritem.dtgarantia, listaCotacaofornecedoritem.cdcotacaofornecedoritem, material.nome, material.cdmaterial, material.identificacao, material.servico, " +
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, materialmestregrade.nome, materialmestregrade.identificacao, " +
					"listaCotacaofornecedoritem.qtdesol, listaCotacaofornecedoritem.qtdecot, listaCotacaofornecedoritem.valor, " +
					"localarmazenagem.cdlocalarmazenagem, localarmazenagemsolicitacao.cdlocalarmazenagem, listaCotacaofornecedoritem.frete, listaCotacaofornecedoritem.icmsissincluso, " +
					"listaCotacaofornecedoritem.icmsiss, listaCotacaofornecedoritem.ipiincluso, listaCotacaofornecedoritem.ipi, cotacaofornecedor.cdcotacaofornecedor, " +
					"listaCotacaofornecedoritem.dtentrega, materialclasse.cdmaterialclasse, listaOrdemcompra.cdordemcompra, listaOrdemcompra.dtcancelamento, " +
					"frequencia.cdfrequencia, listaCotacaofornecedoritem.qtdefrequencia, unidademedidaCFI.cdunidademedida, unidademedidaCFI.nome, " +
					"listaCotacaofornecedoritem.desconto, listaCotacaofornecedoritem.observacao, listaCotacaofornecedoritem.naocotar, " +
					"solicitacaocompraIT.cdsolicitacaocompra, listaCotacaofornecedoritemsolicitacaocompra.cdcotacaofornecedoritemsolicitacaocompra")
			.leftOuterJoin("cotacao.listaOrdemcompra listaOrdemcompra")
			.leftOuterJoin("cotacao.empresa empresa")
			.leftOuterJoin("cotacao.listaCotacaofornecedor listaCotacaofornecedor")
			.leftOuterJoin("listaCotacaofornecedor.listaCotacaofornecedoritem listaCotacaofornecedoritem")
			.leftOuterJoin("listaCotacaofornecedoritem.cotacaofornecedor cotacaofornecedor")
			.leftOuterJoin("listaCotacaofornecedor.fornecedor fornecedor")
			.leftOuterJoin("listaCotacaofornecedor.contato contato")
			.leftOuterJoin("listaCotacaofornecedor.prazopagamento prazopagamento")
			.leftOuterJoin("listaCotacaofornecedoritem.material material")
			.leftOuterJoin("material.materialmestregrade materialmestregrade")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("listaCotacaofornecedoritem.localarmazenagem localarmazenagem")
			.leftOuterJoin("listaCotacaofornecedoritem.localarmazenagemsolicitacao localarmazenagemsolicitacao")
			.leftOuterJoin("listaCotacaofornecedoritem.materialclasse materialclasse")
			.leftOuterJoin("listaCotacaofornecedoritem.frequencia frequencia")
			.leftOuterJoin("listaCotacaofornecedoritem.unidademedida unidademedidaCFI")
			.leftOuterJoin("listaCotacaofornecedoritem.listaCotacaofornecedoritemsolicitacaocompra listaCotacaofornecedoritemsolicitacaocompra")
			.leftOuterJoin("listaCotacaofornecedoritemsolicitacaocompra.solicitacaocompra solicitacaocompraIT")
			.join("cotacao.aux_cotacao aux_cotacao")
			.orderBy("fornecedor.nome, localarmazenagem.nome, material.nome");
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioService.isRestricaoFornecedorColaborador(usuarioLogado)){
			query.where("("+
					"exists("+
						"select 1 from ColaboradorFornecedor cf "+
						"where "+
						"cf.fornecedor =  listaCotacaofornecedor.fornecedor and "+
						"cf.colaborador = "+usuarioLogado.getCdpessoa()+
					") "+
					"or "+
					"not exists("+
						"select 1 from ColaboradorFornecedor cf "+
						"where "+
						"cf.fornecedor =  listaCotacaofornecedor.fornecedor"+
					")"+
				")");
		}
		
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Cotacao> query, FiltroListagem _filtro) {
		CotacaoFiltro filtro = (CotacaoFiltro) _filtro;		
		query
			.select("distinct cotacao.cdcotacao, empresa.nome, empresa.nomefantasia, aux_cotacao.dtlimite, aux_cotacao.situacaosuprimentos, listaCotacaofornecedor.cdcotacaofornecedor")
			.leftOuterJoin("cotacao.empresa empresa")
			.leftOuterJoin("cotacao.listaCotacaofornecedor listaCotacaofornecedor")
			.leftOuterJoin("listaCotacaofornecedor.listaCotacaofornecedoritem listaCotacaofornecedoritem")
			.leftOuterJoin("listaCotacaofornecedoritem.material material")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.join("cotacao.aux_cotacao aux_cotacao")
			.where("empresa = ?", filtro.getEmpresa())
			.where("cotacao.cdcotacao = ?",filtro.getIdcotacao())
			.where("listaCotacaofornecedor.fornecedor = ?", filtro.getFornecedor())
			.where("material = ?",filtro.getMaterial())
			.where("materialgrupo = ?",filtro.getMaterialgrupo())
			.where("material.materialtipo = ?",filtro.getMaterialtipo())
			.where("aux_cotacao.dtlimite >= ?",filtro.getDtinicio())
			.where("aux_cotacao.dtlimite <= ?",filtro.getDtfim())
			.ignoreJoin("listaCotacaofornecedoritem","material","materialgrupo","materialcategoria","vmaterialcategoria");
		
		if (filtro.getListaSituacao() != null) {
			query.whereIn("aux_cotacao.situacaosuprimentos", Situacaosuprimentos.listAndConcatenate(filtro.getListaSituacao()));
		}
		
		if(!SinedUtil.isUsuarioLogadoAdministrador() && SinedUtil.haveGrupoMaterialUsuario()){
			query.whereIn("materialgrupo.cdmaterialgrupo", SinedUtil.getWhereInGrupoMaterialUsuario());
		}
		
		if(filtro.getMaterialcategoria() != null){
			materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
			query
				.leftOuterJoin("material.materialcategoria materialcategoria")
				.leftOuterJoin("materialcategoria.vmaterialcategoria vmaterialcategoria")
				.openParentheses()
					.where("vmaterialcategoria.identificador like ? ||'.%'", filtro.getMaterialcategoria().getIdentificador())
				.or()
					.where("vmaterialcategoria.identificador like ? ", filtro.getMaterialcategoria().getIdentificador())
				.closeParentheses();
		}
		
		if (filtro.getFromsolicitacao() != null && filtro.getFromsolicitacao()) {
			List<Situacaosuprimentos> list = new ArrayList<Situacaosuprimentos>();
			list.add(Situacaosuprimentos.EM_ABERTO);
			query.whereIn("aux_cotacao.situacaosuprimentos", Situacaosuprimentos.listAndConcatenate(list));
		}
		
		if(filtro.getProjeto() != null && filtro.getProjeto().getCdprojeto() != null){
			query
				.where("exists (select co.cdcotacaoorigem " +
								"from Cotacaoorigem co " +
								"join co.solicitacaocompra so " +
								"join so.projeto po " +
								"join co.cotacao c " +
								"where c.id = cotacao.cdcotacao " +
								"and po.id = " + filtro.getProjeto().getCdprojeto() + ")");
		}
		
		boolean usuarioComRestricao = SinedUtil.getUsuarioLogado().getRestricaoFornecedorColaborador();
		Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
		if(usuarioComRestricao){
			query.where("not exists (select cf.cdcotacaofornecedor" +
									" from Cotacaofornecedor cf" +
									" join cf.cotacao co" +
									" join cf.fornecedor f" +
									" join f.listaColaboradorFornecedor lcf " +
									" where co.cdcotacao = cotacao.cdcotacao " +
									" and not exists (select cf2.cdcolaboradorfornecedor" +
														" from ColaboradorFornecedor cf2 " +
														" where cf2.cdcolaboradorfornecedor = lcf.cdcolaboradorfornecedor" +
														" and cf2.colaborador = ?))", colaborador);
		}
		
		query.orderBy("cotacao.cdcotacao desc");
	}

	/**
	 * Carrega a lista de cota��es com as respectivas listas.
	 * 
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cotacao> loadWithLista(String whereIn, String orderBy, boolean asc) {
		QueryBuilder<Cotacao> query = query()
					.select("cotacao.cdcotacao, empresa.cdpessoa, empresa.nomefantasia, aux_cotacao.fornecedores, aux_cotacao.dtlimite, material.cdmaterial, " +
							"material.nome, material.identificacao, materialmestregrade.nome, materialmestregrade.identificacao, " +
							"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, aux_cotacao.situacaosuprimentos, listaCotacaofornecedor.cdcotacaofornecedor")
					.join("cotacao.aux_cotacao aux_cotacao")
					.leftOuterJoin("cotacao.empresa empresa")
					.leftOuterJoin("cotacao.listaCotacaofornecedor listaCotacaofornecedor")
					.leftOuterJoin("listaCotacaofornecedor.listaCotacaofornecedoritem listaCotacaofornecedoritem")
					.leftOuterJoin("listaCotacaofornecedoritem.material material")
					.leftOuterJoin("material.materialmestregrade materialmestregrade")
					.leftOuterJoin("listaCotacaofornecedoritem.localarmazenagem localarmazenagem")
					.whereIn("cotacao.cdcotacao", whereIn);
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("cotacao.cdcotacao DESC");
		}
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioService.isRestricaoFornecedorColaborador(usuarioLogado)){
			query.where("("+
					"exists("+
						"select 1 from ColaboradorFornecedor cf "+
						"where "+
						"cf.fornecedor =  listaCotacaofornecedor.fornecedor and "+
						"cf.colaborador = "+usuarioLogado.getCdpessoa()+
					") "+
					"or "+
					"not exists("+
						"select 1 from ColaboradorFornecedor cf "+
						"where "+
						"cf.fornecedor =  listaCotacaofornecedor.fornecedor"+
					")"+
				")");
		}
		
		return query.list();
	}
	
	/**
	 * Carrega a lista de cota��es com as respectivas listas.
	 * 
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Cotacao loadWithLista(Cotacao cotacao, Fornecedor fornecedor, Localarmazenagem localarmazenagem, Material material) {
		if (cotacao == null || cotacao.getCdcotacao() == null) {
			throw new SinedException("Cota��o n�o pode ser nulo.");
		}
		QueryBuilder<Cotacao> query = query()
					.select("cotacao.cdcotacao, fornecedor.cdpessoa, material.cdmaterial, localarmazenagem.cdlocalarmazenagem, " +
							"listaCotacaofornecedoritem.qtdecot, listaCotacaofornecedoritem.qtdesol, listaCotacaofornecedoritem.valor, " +
							"listaCotacaofornecedoritem.qtdefrequencia, listaCotacaofornecedoritem.desconto")
					.leftOuterJoin("cotacao.listaCotacaofornecedor listaCotacaofornecedor")
					.leftOuterJoin("listaCotacaofornecedor.fornecedor fornecedor")
					.leftOuterJoin("listaCotacaofornecedor.listaCotacaofornecedoritem listaCotacaofornecedoritem")
					.leftOuterJoin("listaCotacaofornecedoritem.material material")
					.leftOuterJoin("listaCotacaofornecedoritem.localarmazenagem localarmazenagem")
					.where("cotacao = ?", cotacao)
					.where("localarmazenagem = ?", localarmazenagem)
					.where("material = ?", material)
					.where("fornecedor = ?",fornecedor);
		return query.unique();
	}

	/**
	 * Atualiza a cota��o dependendo da situa��o passada por par�metro.
	 * 
	 * @param whereIn
	 * @param situacao
	 * @author Rodrigo Freitas
	 */
	public void updateStatus(String whereIn, Situacaosuprimentos situacao) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Where in n�o pode ser nulo.");
		}
		
		StringBuilder builder = new StringBuilder();
		builder.append("update Cotacao c set ");
		
		if (situacao.equals(Situacaosuprimentos.CANCELADA)) {
			builder.append("c.dtcancelamento = ?");
		} else if (situacao.equals(Situacaosuprimentos.BAIXADA)) {
			builder.append("c.dtbaixa = ?");
		} else {
			throw new SinedException("Situa��o n�o suportada.");
		}
		
		builder.append(" where c.id in (" + whereIn + ")");
		
		getHibernateTemplate().bulkUpdate(builder.toString(),new Date(System.currentTimeMillis()));		
	}

	
	/**
	 * Estorna a Cota��o.
	 * 
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void updateEstornada(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Where In n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Cotacao c set dtcancelamento = null, dtbaixa = null where c.id in ("+whereIn+")");
	}

	/**
	 * Carrega a lista de Cota��es para verifica��o.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cotacao> findForVerificacao(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Where In n�o pode ser nulo.");
		}
		return query()
					.select("cotacao.cdcotacao, aux_cotacao.situacaosuprimentos, cotacao.dtbaixa")
					.join("cotacao.aux_cotacao aux_cotacao")
					.whereIn("cotacao.cdcotacao", whereIn)
					.list();
	}

	/**
	 * M�todo carrega cota��o e suas respectivas listas
	 * 
	 * @param cotacao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Cotacao carregaCotacao(Cotacao cotacao) {
		if (cotacao == null || cotacao.getCdcotacao() == null) {
			throw new SinedException("C�digo da cota��o n�o pode ser nula.");
		}
		
		return query()
			.select("cotacao.cdcotacao, cotacao.considerarlocalunicoentrega, listaCotacaofornecedor.cdcotacaofornecedor, listaCotacaofornecedor.conviteenviado, listaCotacaofornecedor.desconto, listaCotacaofornecedor.observacao, " +
					"contato.cdpessoa, contato.nome, fornecedor.cdpessoa, fornecedor.nome, prazopagamento.cdprazopagamento, prazopagamento.nome, " +
					"aux_cotacao.cdcotacao, aux_cotacao.situacaosuprimentos")
			.join("cotacao.aux_cotacao aux_cotacao")
			.join("cotacao.listaCotacaofornecedor listaCotacaofornecedor")
			.join("listaCotacaofornecedor.fornecedor fornecedor")
			.leftOuterJoin("listaCotacaofornecedor.prazopagamento prazopagamento")
			.leftOuterJoin("listaCotacaofornecedor.contato contato")
			.where("cotacao = ?", cotacao)
			.unique();
	}

	/**
	 * M�todo que busca informa��es da cota��o refer�nte a uma determinada ordem de compra para fazer o rateio
	 * 
	 * @param ordemcompra
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Cotacao getCotacaoDaOrdemCompraParaRateio(Ordemcompra ordemcompra) {
		return querySined()
			.setUseWhereProjeto(false)
			.select(
					"cotacao.cdcotacao, listaOrigem.cdcotacaoorigem, solicitacaocompra.cdsolicitacaocompra, " +
					"solicitacaocompra.qtde, projetoSolicitacao.cdprojeto, centrocustoSolicitacao.cdcentrocusto, " +
					"projetoRequisicao.cdprojeto, centrocustoRequisicao.cdcentrocusto, material.cdmaterial, " +
					"contagerencial.cdcontagerencial, contagerencial.nome, vcontagerencial.arvorepai, vcontagerencial.identificador, " +
					"contagerencialSolicitacao.cdcontagerencial, contagerencialSolicitacao.nome, vcontagerencialsolicitacao.arvorepai, " +
					"vcontagerencialsolicitacao.identificador")
			.leftOuterJoin("cotacao.listaOrdemcompra listaOrdemcompra")
			.leftOuterJoin("cotacao.listaOrigem listaOrigem")
			.leftOuterJoin("listaOrigem.solicitacaocompra solicitacaocompra")
			.leftOuterJoin("solicitacaocompra.material material")
			.leftOuterJoin("material.contagerencial contagerencial")
			.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			.leftOuterJoin("solicitacaocompra.solicitacaocompraorigem solicitacaocompraorigem")
			.leftOuterJoin("solicitacaocompraorigem.requisicaomaterial requisicaomaterial")
			.leftOuterJoin("solicitacaocompra.projeto projetoSolicitacao")
			.leftOuterJoin("solicitacaocompra.centrocusto centrocustoSolicitacao")			
			.leftOuterJoin("solicitacaocompra.contagerencial contagerencialSolicitacao")
			.leftOuterJoin("contagerencialSolicitacao.vcontagerencial vcontagerencialsolicitacao")
			.leftOuterJoin("requisicaomaterial.projeto projetoRequisicao")
			.leftOuterJoin("requisicaomaterial.centrocusto centrocustoRequisicao")			
			.where("listaOrdemcompra = ?", ordemcompra)
			.unique();
	}
	
	/**
	 * M�todo que carrega a cotacao para calcular o rateio ao gerar a ordem de compra
	 *
	 * @param cotacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Cotacao getCotacaoDaOrdemCompraParaRateio(Cotacao cotacao) {
		if(cotacao == null || cotacao.getCdcotacao() == null)
			throw new SinedException("Cota��o n�o pode ser nula.");
		
		return query()	
			.select(
					"cotacao.cdcotacao, listaOrigem.cdcotacaoorigem, solicitacaocompra.cdsolicitacaocompra, " +
					"solicitacaocompra.qtde, projetoSolicitacao.cdprojeto, centrocustoSolicitacao.cdcentrocusto, " +
					"projetoRequisicao.cdprojeto, centrocustoRequisicao.cdcentrocusto, material.cdmaterial, " +
					"contagerencial.cdcontagerencial, contagerencial.nome, vcontagerencial.arvorepai, vcontagerencial.identificador, " +
					"contagerencialSolicitacao.cdcontagerencial, contagerencialSolicitacao.nome, vcontagerencialsolicitacao.arvorepai, " +
					"vcontagerencialsolicitacao.identificador")
			.leftOuterJoin("cotacao.listaOrigem listaOrigem")
			.leftOuterJoin("listaOrigem.solicitacaocompra solicitacaocompra")
			.leftOuterJoin("solicitacaocompra.material material")
			.leftOuterJoin("material.contagerencial contagerencial")
			.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			.leftOuterJoin("solicitacaocompra.solicitacaocompraorigem solicitacaocompraorigem")
			.leftOuterJoin("solicitacaocompraorigem.requisicaomaterial requisicaomaterial")
			.leftOuterJoin("solicitacaocompra.projeto projetoSolicitacao")
			.leftOuterJoin("solicitacaocompra.centrocusto centrocustoSolicitacao")			
			.leftOuterJoin("solicitacaocompra.contagerencial contagerencialSolicitacao")
			.leftOuterJoin("contagerencialSolicitacao.vcontagerencial vcontagerencialsolicitacao")
			.leftOuterJoin("requisicaomaterial.projeto projetoRequisicao")
			.leftOuterJoin("requisicaomaterial.centrocusto centrocustoRequisicao")			
			.where("cotacao = ?", cotacao)
			.unique();
	}
	
	/**
	 * Chama a procedure de atualiza��o da tabela auxiliar de cota��o
	 *
	 * @param cotacao
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaCotacao(Cotacao cotacao){
		if (cotacao == null || cotacao.getCdcotacao() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		getJdbcTemplate().execute("SELECT ATUALIZA_COTACAO(" + cotacao.getCdcotacao() + ", '" + NeoWeb.getRequestContext().getServletRequest().getContextPath() + "')");
	}

	/**
	 * M�todo que busca informa��es da cota��o para enviar email
	 * 
	 * @param where
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Cotacao findCotacaoParaEnviarEmail(String where) {
		if (where == null || where.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		if(where.split(",").length > 1){
			throw new SinedException("S� � poss�vel enviar 1 pedido por vez.");
		}
		
		return query()
			.select("cotacao.cdcotacao, aux_cotacao.situacaosuprimentos, contato.cdpessoa, listaCotacaofornecedor.cdcotacaofornecedor, " +
					"contato.emailcontato, contato.nome, fornecedor.cdpessoa, fornecedor.nome, empresa.nome, empresa.razaosocial")
			.join("cotacao.aux_cotacao aux_cotacao")
			.join("cotacao.listaCotacaofornecedor listaCotacaofornecedor")
			.leftOuterJoin("listaCotacaofornecedor.contato contato")
			.leftOuterJoin("listaCotacaofornecedor.fornecedor fornecedor")
			.leftOuterJoin("cotacao.listaOrigem listaOrigem")
			.leftOuterJoin("listaOrigem.solicitacaocompra solicitacaocompra")
			.leftOuterJoin("solicitacaocompra.empresa empresa")
			.where("cotacao.cdcotacao = ?", new Integer(where))
			.unique();
	}

	/**
	 * M�todo que carrega v�rios dados da cota��o referente a uma ordem de compra. Este m�todo carrega parte das coisas necess�rias
	 * para descobrir quais s�o as solicita��es de compra de uma ordem de compra
	 * 
	 * @param ordemcompra
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Cotacao loadCotacaoParaAjustarPedidoSolicitacaoCompra(Ordemcompra ordemcompra) {
		return query()
			.select("cotacao.cdcotacao, solicitacaocompra.cdsolicitacaocompra, localarmazenagemsolicitacao.cdlocalarmazenagem, cotacaofornecedor.cdpessoa, " +
					"materialsolicitacao.cdmaterial, cotacaofornecedor.cdpessoa, itemmaterial.cdmaterial, itemlocalarmazenagem.cdlocalarmazenagem, " +
					"itemlocalarmazenagemsolicitacao.cdlocalarmazenagem, listaCotacaofornecedoritem.qtdesol, solicitacaocompra.entrega, solicitacaocompra.pedido, " +
					"listaCotacaofornecedoritem.desconto")
			.join("cotacao.listaOrdemcompra listaOrdemcompra")
			.join("cotacao.listaCotacaofornecedor listaCotacaofornecedor")
			.join("cotacao.listaOrigem listaOrigem")
			.join("listaOrigem.solicitacaocompra solicitacaocompra")
			.join("solicitacaocompra.localarmazenagem localarmazenagemsolicitacao")
			.join("solicitacaocompra.material materialsolicitacao")
			.join("listaCotacaofornecedor.listaCotacaofornecedoritem listaCotacaofornecedoritem")
			.join("listaCotacaofornecedor.fornecedor cotacaofornecedor")
			.join("listaCotacaofornecedoritem.material itemmaterial")
			.join("listaCotacaofornecedoritem.localarmazenagem itemlocalarmazenagem")
			.join("listaCotacaofornecedoritem.localarmazenagemsolicitacao itemlocalarmazenagemsolicitacao")
			.where("listaOrdemcompra = ?", ordemcompra)
			.unique();
	}

	/**
	 * M�todo que carrega v�rios dados da cota��o referente a uma ordem de compra. Este m�todo carrega parte das coisas necess�rias
	 * para descobrir quais s�o as solicita��es de compra de uma ordem de compra
	 * 
	 * @param whereInOrdemCompra
	 * @return
	 */
	public List<Cotacao> loadCotacoesParaAjustarPedidoSolicitacaoCompra(String whereInOrdemCompra) {
		return query()
			.select("cotacao.cdcotacao, solicitacaocompra.cdsolicitacaocompra, localarmazenagemsolicitacao.cdlocalarmazenagem, cotacaofornecedor.cdpessoa, " +
					"materialsolicitacao.cdmaterial, cotacaofornecedor.cdpessoa, itemmaterial.cdmaterial, itemlocalarmazenagem.cdlocalarmazenagem, " +
					"itemlocalarmazenagemsolicitacao.cdlocalarmazenagem, listaCotacaofornecedoritem.qtdesol, solicitacaocompra.pedido, solicitacaocompra.entrega, " +
					"listaCotacaofornecedoritem.desconto")
			.join("cotacao.listaOrdemcompra listaOrdemcompra")
			.join("cotacao.listaCotacaofornecedor listaCotacaofornecedor")
			.join("cotacao.listaOrigem listaOrigem")
			.join("listaOrigem.solicitacaocompra solicitacaocompra")
			.join("solicitacaocompra.localarmazenagem localarmazenagemsolicitacao")
			.join("solicitacaocompra.material materialsolicitacao")
			.join("listaCotacaofornecedor.listaCotacaofornecedoritem listaCotacaofornecedoritem")
			.join("listaCotacaofornecedor.fornecedor cotacaofornecedor")
			.join("listaCotacaofornecedoritem.material itemmaterial")
			.join("listaCotacaofornecedoritem.localarmazenagem itemlocalarmazenagem")
			.join("listaCotacaofornecedoritem.localarmazenagemsolicitacao itemlocalarmazenagemsolicitacao")
			.whereIn("listaOrdemcompra", whereInOrdemCompra)
			.list();
	}

	/**
	 * Busca as informa��es para a listagem do CSV.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cotacao> findForCsv(CotacaoFiltro filtro) {
		QueryBuilder<Cotacao> query = querySined();
		updateListagemQuery(query, filtro);
		return query.list();
	}

	/**
	 * M�todo que carrega as informa��es da cota��o e solicita��o de compra da cota��o de uma ordem de compra
	 * 
	 * @param ordemcompra
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Cotacao findByOrdemcompra(Ordemcompra ordemcompra) {
		return query()
			.select("cotacao.cdcotacao, listaOrigem.cdcotacaoorigem, solicitacaocompra.cdsolicitacaocompra, centrocusto.cdcentrocusto, centrocusto.nome, " +
					"projeto.cdprojeto, projeto.nome, colaborador.cdpessoa, colaborador.nome, material.cdmaterial, material.nome, material.identificacao")
			.join("cotacao.listaOrigem listaOrigem")
			.join("listaOrigem.solicitacaocompra solicitacaocompra")
			.join("solicitacaocompra.centrocusto centrocusto")
			.join("solicitacaocompra.colaborador colaborador")
			.join("solicitacaocompra.material material")
			.leftOuterJoin("solicitacaocompra.projeto projeto")
			.where("cotacao.dtcancelamento is null")
			.where("solicitacaocompra.dtcancelamento is null")
			.where("cotacao = ?", ordemcompra.getCotacao())
			.unique();
	}
	
	/**
	 * M�todo que faz o update do Mapa de Cotacao
	 * 
	 * @param Cotacao
	 * @return
	 * @author Thiago Augusto
	 */
	public void updateMapaCotacao(Cotacao cotacao){
		StringBuilder sql;
		for (Cotacaofornecedor cotacaofornecedor : cotacao.getListaCotacaofornecedor()) {
			for (Cotacaofornecedoritem cotacaofornecedoritem : cotacaofornecedor.getListaCotacaofornecedoritem()) {
				
//				Faz o update da lista de fornecedorItem
				sql = new StringBuilder("UPDATE Cotacaofornecedoritem SET ");
				sql.append("qtdecot = ?").append(", ");
				sql.append("ipi = ?").append(", ");
				sql.append("icmsiss = ?").append(", ");
				sql.append("ipiincluso = ?").append(", ");
				sql.append("icmsissincluso = ?").append(", ");
				sql.append("valor = ?").append(", ");
				sql.append("dtgarantia = ?").append(", ");
				sql.append("dtentrega = ?").append(", ");
				sql.append("frete = ?");
				sql.append(" WHERE cdcotacaofornecedoritem = ? ");
				
				getJdbcTemplate().update(sql.toString(), new Object[]{
							cotacaofornecedoritem.getQtdecot(),
							cotacaofornecedoritem.getIpi(), 
							cotacaofornecedoritem.getIcmsiss(),
							cotacaofornecedoritem.getIpiincluso(),
							cotacaofornecedoritem.getIcmsissincluso(),
							cotacaofornecedoritem.getValor(), 
							cotacaofornecedoritem.getDtgarantia(), 
							cotacaofornecedoritem.getDtentrega(), 
							cotacaofornecedoritem.getFrete(),
							cotacaofornecedoritem.getCdcotacaofornecedoritem()
							});
			}	
			
//			Faz o update da lista de fornecedor
			sql = new StringBuilder("UPDATE Cotacaofornecedor SET cdprazopagamento = ?");
						  sql.append(" WHERE cdcotacaofornecedor = ?");
						  Object cdPrazoPagamento = null;
						  if (cotacaofornecedor.getPrazopagamento() != null)
							  	cdPrazoPagamento = cotacaofornecedor.getPrazopagamento().getCdprazopagamento();
						  getJdbcTemplate().update(sql.toString(), new Object[]{cdPrazoPagamento, cotacaofornecedor.getCdcotacaofornecedor()});
		}
	}

	/**
	 * M�todo que carrega a cota��o com a situa��o
	 *
	 * @param cotacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Cotacao loadWithSituacao(Cotacao cotacao) {
		if(cotacao == null || cotacao.getCdcotacao() == null)
			throw new SinedException("Cota��o n�o pode ser nula.");
		
		return query()
				.select("cotacao.cdcotacao, aux_cotacao.cdcotacao, aux_cotacao.situacaosuprimentos")
				.join("cotacao.aux_cotacao aux_cotacao")
				.where("cotacao = ?", cotacao)
				.unique();
	}

	/**
	 * M�todo que busca as cota��es para gera��o do excel
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Cotacao> findForExcel(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		QueryBuilder<Cotacao> query = querySined(); 
		return query
					.select("cotacao.cdcotacao, aux_cotacao.fornecedores, aux_cotacao.dtlimite, material.cdmaterial, " +
							"material.nome, material.identificacao, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
							"aux_cotacao.situacaosuprimentos, listaCotacaofornecedor.cdcotacaofornecedor, " +
							"fornecedor.nome, listaTelefone.telefone, telefonetipo.cdtelefonetipo, prazopagamento.cdprazopagamento," +
							"prazopagamento.nome, " +
							"listaOrigem.cdcotacaoorigem, solicitacaocompra.cdsolicitacaocompra, projeto.cdprojeto, projeto.nome," +
							"unidademedida.cdunidademedida, unidademedida.nome, listaCotacaofornecedoritem.dtentrega, " +
							"listaCotacaofornecedoritem.qtdecot, listaCotacaofornecedoritem.qtdefrequencia, listaCotacaofornecedoritem.valor, " +
							"listaCotacaofornecedoritem.desconto, listaCotacaofornecedoritem.frete, listaCotacaofornecedoritem.icmsiss, listaCotacaofornecedoritem.ipi ")
					.leftOuterJoin("cotacao.aux_cotacao aux_cotacao")
					.leftOuterJoin("cotacao.listaCotacaofornecedor listaCotacaofornecedor")
					.leftOuterJoin("listaCotacaofornecedor.fornecedor fornecedor")
					.leftOuterJoin("fornecedor.listaTelefone listaTelefone")
					.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
					.leftOuterJoin("listaCotacaofornecedor.listaCotacaofornecedoritem listaCotacaofornecedoritem")
					.leftOuterJoin("listaCotacaofornecedoritem.material material")
					.leftOuterJoin("listaCotacaofornecedoritem.localarmazenagem localarmazenagem")
					.leftOuterJoin("cotacao.listaOrigem listaOrigem")
					.leftOuterJoin("listaOrigem.solicitacaocompra solicitacaocompra")
					.leftOuterJoin("solicitacaocompra.projeto projeto")
					.leftOuterJoin("listaCotacaofornecedoritem.unidademedida unidademedida")
					.leftOuterJoin("listaCotacaofornecedor.prazopagamento prazopagamento")
					.whereIn("cotacao.cdcotacao", whereIn)
					.orderBy("cotacao.cdcotacao DESC, fornecedor.nome, localarmazenagem.nome, material.nome")
					.list();
	}
	
	/**
	 * M�todo que carrega a cota��o com a solicita��o de compra
	 *
	 * @param cotacaofornecedor
	 * @return
	 * @author Luiz Fernando
	 * @since 10/10/2013
	 */
	public Cotacao carregaCotacaoWithSolicitacaocompra(Cotacaofornecedor cotacaofornecedor){
		if(cotacaofornecedor == null || cotacaofornecedor.getCdcotacaofornecedor() == null)
			throw new SinedException("Cotacaofornecedor n�o pode ser nulo");
		
		return query()
				.select("cotacao.cdcotacao, fornecedor.nome, " +
						"listaOrigem.cdcotacaoorigem, solicitacaocompra.cdsolicitacaocompra, empresa.cdpessoa, empresa.nome, empresa.razaosocial, " +
						"empresa.cnpj, empresa.inscricaoestadual")
				.leftOuterJoin("cotacao.listaCotacaofornecedor listaCotacaofornecedor")
				.leftOuterJoin("listaCotacaofornecedor.fornecedor fornecedor")
				.leftOuterJoin("cotacao.listaOrigem listaOrigem")
				.leftOuterJoin("listaOrigem.solicitacaocompra solicitacaocompra")
				.leftOuterJoin("solicitacaocompra.empresa empresa")
				.where("listaCotacaofornecedor = ?", cotacaofornecedor)
				.unique();
	}

	public List<Integer> findCotacoesNaoCanceladas(String whereInCdcotacao) {
		if(whereInCdcotacao == null || whereInCdcotacao.isEmpty()) {
			throw new SinedException("WhereInCdcotacao n�o pode ser nulo");
		}
		List<Cotacao> lista = query()
					.select("cotacao.cdcotacao, cotacao.dtbaixa")
					.whereIn("cotacao.cdcotacao", whereInCdcotacao)
					.where("cotacao.aux_cotacao.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
					.list();
		
		List<Integer> listaCdcotacao = new ArrayList<Integer>();
		if(SinedUtil.isListNotEmpty(lista)){
			for(Cotacao cotacao : lista){
				listaCdcotacao.add(cotacao.getCdcotacao());
			}
		}
		return listaCdcotacao;
	}
	
	/**
	 * M�todo que retorna uma lista de cota��es com suas listas internas carregadas.
	 * @param cotacao
	 * @return
	 */
	public Cotacao loadWithListaCotacaoFornecedor(Cotacao cotacao) {
		if (cotacao == null || cotacao.getCdcotacao() == null) {
			throw new SinedException("Cota��o n�o pode ser nulo.");
		}
		QueryBuilder<Cotacao> query = query()
					.select("cotacao.cdcotacao, listaCotacaofornecedoritem.cdcotacaofornecedoritem, material.cdmaterial, material.nome, listaCotacaofornecedor.cotacao, " +
							"listaCotacaofornecedor.fornecedor, listaCotacaofornecedor.cdcotacaofornecedor, listaCotacaofornecedor.conviteenviado, " +
							"listaCotacaofornecedoritem.localarmazenagem, material.identificacao")
					.leftOuterJoin("cotacao.listaCotacaofornecedor listaCotacaofornecedor")
					.leftOuterJoin("listaCotacaofornecedor.listaCotacaofornecedoritem listaCotacaofornecedoritem")
					.leftOuterJoin("listaCotacaofornecedoritem.material material")
					.where("cotacao = ?", cotacao);
		return query.unique();
	}
	
	/**
	 * M�todo que apaga materiais de uma cota��o
	 * @param cdcotacaofornecedoritem
	 */
	public void deleteListaMaterialNaoCotado(String cdcotacaofornecedoritem){
		String sql = "DELETE FROM COTACAOFORNECEDORITEM WHERE cdcotacaofornecedoritem = (" + cdcotacaofornecedoritem + ");";
		getJdbcTemplate().execute(sql);
	}
	
	/**
	 * M�todo que atualiza a situa��o de uma solicita��o de compra, ap�s a exclus�o de um material na cota��o.
	 * @param whereIn
	 */
	public void updateSituacaoSolicitacaoCompra(String whereIn){
		if(StringUtils.isNotEmpty(whereIn)){
			String sql = "UPDATE AUX_SOLICITACAOCOMPRA SET SITUACAO = " + Situacaosuprimentos.AUTORIZADA.getCodigo() + 
							", ORDEMCOMPRA = null, COTACAO=null WHERE cdsolicitacaocompra IN (" + whereIn + ");";
			getJdbcTemplate().execute(sql);
		}
	}
	
	/**
	* M�todo que verifica se existe o mesmo material na cota��o
	*
	* @param whereInSolicitacaocompra
	* @param cotacao
	* @return
	* @since 01/12/2014
	* @author Luiz Fernando
	*/
	public boolean existeMaterial(String whereInSolicitacaocompra, Cotacao cotacao) {
		if(StringUtils.isEmpty(whereInSolicitacaocompra) || cotacao == null || cotacao.getCdcotacao() == null)
			return false;
		
		String sql = " select count(*) " +
					" from cotacaofornecedoritem cfi " +
					" join cotacaofornecedor cf on cf.cdcotacaofornecedor = cfi.cdcotacaofornecedor " +
					" join solicitacaocompra sc on sc.cdmaterial = cfi.cdmaterial " +
					" where cf.cdcotacao =  " + cotacao.getCdcotacao() + 
					" and sc.cdsolicitacaocompra in ( " +  whereInSolicitacaocompra + ") ";

		SinedUtil.markAsReader();
		long contador = getJdbcTemplate().queryForLong(sql);
		return contador > 0;
	}
	
	/**
	* M�todo que carrega a ordem de compra com a empresa
	*
	* @param cotacao
	* @return
	* @since 23/01/2015
	* @author Luiz Fernando
	*/
	public Cotacao loadWithEmpresa(Cotacao cotacao){
		if(cotacao == null || cotacao.getCdcotacao() == null)
			throw new SinedException("Cota��o n�o pode ser nula.");
		
		return query()
				.select("cotacao.cdcotacao, empresa.cdpessoa, empresa.nome")
				.leftOuterJoin("cotacao.empresa empresa")
				.where("cotacao = ?", cotacao)
				.unique();
	}

	/**
	* M�todo que atualiza a empresa da cota��o
	*
	* @param cotacao
	* @param empresa
	* @since 10/03/2015
	* @author Luiz Fernando
	*/
	public void updateEmpresaUnicaentrega(Cotacao cotacao, Empresa empresa) {
		if(cotacao != null && cotacao.getCdcotacao() != null && empresa != null && empresa.getCdpessoa() != null){
			String sql = "update cotacao set cdempresa = ? where cdcotacao = ?" ;
			getJdbcTemplate().update(sql, new Object[]{empresa.getCdpessoa(), cotacao.getCdcotacao()});
		}
	}
	
	/**
	* M�todo que busca as cota��es em aberto
	*
	* @param whereInCdcotacao
	* @return
	* @since 28/09/2015
	* @author Luiz Fernando
	*/
	public List<Cotacao> findCotacoesEmAberto(String whereInCdcotacao) {
		if(whereInCdcotacao == null || whereInCdcotacao.isEmpty()) {
			throw new SinedException("WhereInCdcotacao n�o pode ser nulo");
		}
		
		List<Situacaosuprimentos> listaSituacao = new ArrayList<Situacaosuprimentos>();
		listaSituacao.add(Situacaosuprimentos.EM_ABERTO);
		listaSituacao.add(Situacaosuprimentos.EM_COTACAO);
		
		return query()
					.select("cotacao.cdcotacao, cotacao.dtbaixa")
					.whereIn("cotacao.cdcotacao", whereInCdcotacao)
					.whereIn("cotacao.aux_cotacao.situacaosuprimentos", Situacaosuprimentos.listAndConcatenate(listaSituacao))
					.list();
	}
	public List<Cotacao> findByAtrasado(Date data, Date dateToSearch) {
		return querySined()
				.setUseWhereClienteEmpresa(false)
				.setUseWhereEmpresa(false)
				.setUseWhereFornecedorEmpresa(false)
				.setUseWhereProjeto(false)
				.select("cotacao.cdcotacao, empresa.cdpessoa")
				.leftOuterJoin("cotacao.aux_cotacao aux_cotacao")
				.leftOuterJoin("cotacao.empresa empresa")
				.where("aux_cotacao.situacaosuprimentos = ?", Situacaosuprimentos.EM_ABERTO)
				.wherePeriodo("aux_cotacao.dtlimite", dateToSearch, SinedDateUtils.currentDate())
				.where("aux_cotacao.dtlimite < ?", data)
				.list();
	}
}
