package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Composicaomaoobra;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.ComposicaomaoobraFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ComposicaomaoobraDAO extends GenericDAO<Composicaomaoobra> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Composicaomaoobra> query, FiltroListagem _filtro) {
		ComposicaomaoobraFiltro filtro = (ComposicaomaoobraFiltro) _filtro;

		query
			.select("composicaomaoobra.cdcomposicaomaoobra, composicaomaoobra.salario, cargo.nome")
			.leftOuterJoin("composicaomaoobra.cargo cargo")
			.where("composicaomaoobra.orcamento = ?", filtro.getOrcamento())
			.where("composicaomaoobra.cargo = ?", filtro.getCargo());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Composicaomaoobra> query) {
		query
			.select("composicaomaoobra.cdcomposicaomaoobra, composicaomaoobra.salario," +
					"composicaomaoobra.cdusuarioaltera, composicaomaoobra.dtaltera,  " +
					"orcamento.cdorcamento, orcamento.nome, " +
					"cargo.cdcargo, cargo.nome, " +
					"listaComposicaomaoobraformula.cdcomposicaomaoobraformula, listaComposicaomaoobraformula.ordem, " +
					"listaComposicaomaoobraformula.identificador, listaComposicaomaoobraformula.formula")
			.leftOuterJoin("composicaomaoobra.orcamento orcamento")
			.leftOuterJoin("composicaomaoobra.cargo cargo")
			.leftOuterJoin("composicaomaoobra.listaComposicaomaoobraformula listaComposicaomaoobraformula");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaComposicaomaoobraformula");
	}

	/**
	 * Busca todas as composi��es para o flex
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/01/2015
	 */
	public List<Composicaomaoobra> findAllForFlex() {
		return query()
					.select("composicaomaoobra.cdcomposicaomaoobra, composicaomaoobra.salario, " +
							"listaComposicaomaoobraformula.ordem, listaComposicaomaoobraformula.identificador, listaComposicaomaoobraformula.formula, " +
							"listaComposicaomaoobraformula.cdcomposicaomaoobraformula, cargo.cdcargo, cargo.nome")
					.join("composicaomaoobra.cargo cargo")
					.join("composicaomaoobra.listaComposicaomaoobraformula listaComposicaomaoobraformula")
					.list();
	}

	/**
	 * Busca as composi��es a partir do cargo
	 *
	 * @param cargo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/01/2015
	 */
	public List<Composicaomaoobra> findByCargo(Cargo cargo) {
		return query()
					.select("composicaomaoobra.cdcomposicaomaoobra, composicaomaoobra.salario, " +
							"listaComposicaomaoobraformula.ordem, listaComposicaomaoobraformula.identificador, listaComposicaomaoobraformula.formula, " +
							"listaComposicaomaoobraformula.cdcomposicaomaoobraformula, cargo.cdcargo, cargo.nome")
					.join("composicaomaoobra.cargo cargo")
					.join("composicaomaoobra.listaComposicaomaoobraformula listaComposicaomaoobraformula")
					.where("cargo = ?", cargo)
					.list();
	}

	/**
	* M�todo que busca as composi��es de m�o de obra para copia de acordo com o or�amento
	*
	* @param orcamento
	* @return
	* @since 22/04/2015
	* @author Luiz Fernando
	*/
	public List<Composicaomaoobra> buscarComposicaomaoobraParaCopia(Orcamento orcamento) {
		if(orcamento == null || orcamento.getCdorcamento() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Composicaomaoobra> query = query();
		updateEntradaQuery(query);
		
		return query
				.where("orcamento = ?", orcamento)
				.list();
	}
}