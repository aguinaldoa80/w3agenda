package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Situacaohistorico;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.OrdenacaoControleInteracao;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ContacrmcontatoService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.LeadService;
import br.com.linkcom.sined.geral.service.LeadhistoricoService;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ContacrmFiltro;
import br.com.linkcom.sined.modulo.crm.controller.process.bean.ControleInteracaoBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContacrmDAO extends GenericDAO<Contacrm>{
	
	protected ContacrmcontatoService contacrmcontatoService;
	protected OportunidadeService oportunidadeService;
	protected LeadService leadService;
	protected LeadhistoricoService leadhistoricoService;
	protected UsuarioService usuarioService;
	
	
	public void setContacrmcontatoService(ContacrmcontatoService contacrmcontatoService) {
		this.contacrmcontatoService = contacrmcontatoService;
	}
	public void setOportunidadeService(OportunidadeService oportunidadeService) {
		this.oportunidadeService = oportunidadeService;
	}
	public void setLeadService(LeadService leadService) {
		this.leadService = leadService;
	}
	public void setLeadhistoricoService(LeadhistoricoService leadhistoricoService) {
		this.leadhistoricoService = leadhistoricoService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	
	
	@Override
	public void updateListagemQuery(QueryBuilder<Contacrm> query, FiltroListagem _filtro) {
		
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		
		ContacrmFiltro filtro = (ContacrmFiltro)_filtro;
		
		query
			.select("distinct contacrm.cdcontacrm, contacrm.nome, contacrm.tipo, responsavel.nome, agencia.nome, contacrm.tiporesponsavel")
					.leftOuterJoin("contacrm.listcontacrmcontato listcontacrmcontato")
					.leftOuterJoin("listcontacrmcontato.sexo sexo")
					.leftOuterJoin("listcontacrmcontato.municipio municipio")
					.leftOuterJoin("listcontacrmcontato.listcampanhacontato listcampanhacontato")
					.leftOuterJoin("listcampanhacontato.campanha campanha")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("contacrm.listcontacrmsegmento listcontacrmsegmento")
					.leftOuterJoin("contacrm.responsavel responsavel")
					.leftOuterJoin("contacrm.agencia agencia")
					.leftOuterJoin("contacrm.lead lead")
					.leftOuterJoin("contacrm.concorrente concorrente")
					.leftOuterJoin("lead.cliente cliente")
				.where("campanha = ?", filtro.getCampanha())
				.whereLikeIgnoreAll("contacrm.nome", filtro.getNome())
				.where("listcontacrmsegmento.segmento = ?", filtro.getSegmento())
				.where("uf = ?", filtro.getUf())
				.where("municipio = ?", filtro.getMunicipio())
				.where("concorrente = ?", filtro.getConcorrente())
				.ignoreJoin("listcontacrmsegmento", "listcontacrmcontato", "municipio", "uf", "sexo", "listcampanhacontato", "campanha");
		
		if(SinedUtil.isRestricaoClienteVendedor(usuarioLogado)){
			if(!usuarioService.isVisualizaOutrasOportunidades(usuarioLogado)){
				if(ColaboradorService.getInstance().isColaborador(usuarioLogado.getCdpessoa())){
					query.where("responsavel.cdpessoa = ?", usuarioLogado.getCdpessoa());
				} else if(FornecedorService.getInstance().isFornecedor(usuarioLogado.getCdpessoa())){
					query.where("agencia.cdpessoa = ?", usuarioLogado.getCdpessoa());	
				}
			}else{
				query.where("responsavel = ?", filtro.getResponsavel());
				query.where("agencia = ?", filtro.getAgencia());				
			}
		}else{
			query.where("responsavel = ?", filtro.getResponsavel());
			query.where("agencia = ?", filtro.getAgencia());
		}
		
		
		super.updateListagemQuery(query, _filtro);
	}
	
	public List<Contacrm> loadWithLista(String whereIn, String orderBy, boolean asc) {
		
		QueryBuilder<Contacrm> query = query()
					.select("contacrm.cdcontacrm, contacrm.nome, contacrm.tipo, " +
							"listcontacrmcontato.cdcontacrmcontato, responsavel.nome, " +
							"listcontacrmcontatofone.cdcontacrmcontatofone, listcontacrmcontatofone.telefone, " +
							"telefonetipo.cdtelefonetipo, telefonetipo.nome, listcontacrmcontatoemail.email")
					.leftOuterJoin("contacrm.listcontacrmcontato listcontacrmcontato")
					.leftOuterJoin("listcontacrmcontato.listcontacrmcontatoemail listcontacrmcontatoemail")
					.leftOuterJoin("listcontacrmcontato.listcontacrmcontatofone listcontacrmcontatofone")
					.leftOuterJoin("listcontacrmcontatofone.telefonetipo telefonetipo")
					.leftOuterJoin("contacrm.responsavel responsavel")
					.whereIn("contacrm.cdcontacrm", whereIn);
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("contacrm.nome");
		}
		
		return query.list();
	}
	
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Contacrm> query) {
		query
		.leftOuterJoinFetch("contacrm.lead lead")
		.leftOuterJoinFetch("contacrm.responsavel responsavel")
		.leftOuterJoinFetch("contacrm.listcontacrmhistorico listcontacrmhistorico")
		.leftOuterJoin("contacrm.concorrente concorrente")
		.orderBy("listcontacrmhistorico.dtaltera DESC");
	}
	
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				
				save.saveOrUpdateManaged("listcontacrmsegmento");
				return null;
			}
		});
	}
	
	@Override
	public void saveOrUpdate(final Contacrm bean) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
		
				boolean isCriar = bean.getCdcontacrm() == null;
				
				saveOrUpdateNoUseTransaction(bean);
				List<Contacrmcontato> listaContato = bean.getListcontacrmcontato();
				if(isCriar){
					leadhistoricoService.saveOrUpdateHistoricoFromConta(bean);
				}
				
				try{
					contacrmcontatoService.saveOrUpdateListContacrmcontato(bean, listaContato);
				} catch (DataAccessException da) {
					throw new SinedException("Contato n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
				}
				
				return null;
			}
		});
	}
	
	
	public Contacrm loadForOportunidade(Integer cdcontacrm){
		return query()
		.select("contacrm.cdcontacrm, contacrm.nome, listoportunidade.cdoportunidade, oportunidadesituacao.cdoportunidadesituacao, " +
				"oportunidadesituacao.nome, campanha.cdcampanha ")
		.leftOuterJoin("contacrm.listoportunidade listoportunidade")
		.leftOuterJoin("listoportunidade.oportunidadesituacao oportunidadesituacao")
		.leftOuterJoin("listoportunidade.campanha campanha")
		.where("contacrm.cdcontacrm =?", cdcontacrm)
		.unique();
	}
	
	
	public List<Contacrm> findForReport(ContacrmFiltro contacrmFiltro){
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		
		QueryBuilder<Contacrm> query = querySined()
			.select("contacrm.cdcontacrm, contacrm.nome, contacrm.tipo, " +
					"listcontacrmcontato.cdcontacrmcontato, listcontacrmcontato.nome, " +
					"responsavel.cdpessoa, responsavel.nome, " +
					"listcontacrmcontatoemail.email, listcontacrmcontatoemail.cdcontacrmcontatoemail, " +
					"listcontacrmcontatofone.cdcontacrmcontatofone, listcontacrmcontatofone.telefone, " +
					"telefonetipo.cdtelefonetipo, telefonetipo.nome ")
				.leftOuterJoin("contacrm.listcontacrmcontato listcontacrmcontato")
				.leftOuterJoin("listcontacrmcontato.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("listcontacrmcontato.listcontacrmcontatofone listcontacrmcontatofone")
				.leftOuterJoin("listcontacrmcontatofone.telefonetipo telefonetipo")
				.leftOuterJoin("listcontacrmcontato.listcontacrmcontatoemail listcontacrmcontatoemail")
				.leftOuterJoin("contacrm.listcontacrmsegmento listcontacrmsegmento")
				.leftOuterJoin("contacrm.listcontacrmhistorico listcontacrmhistorico")
				.leftOuterJoin("contacrm.responsavel responsavel")
				.leftOuterJoin("contacrm.lead lead")
				.leftOuterJoin("listcontacrmcontato.sexo sexo")
			.whereLikeIgnoreAll("lead.nome", contacrmFiltro.getNome())
			.where("responsavel = ?", contacrmFiltro.getResponsavel())
			.where("listcontacrmsegmento.segmento = ?", contacrmFiltro.getSegmento())
			.where("municipio.uf = ?", contacrmFiltro.getUf())
			.where("municipio = ?", contacrmFiltro.getMunicipio())
			.orderBy("contacrm.nome");
				
			if(SinedUtil.isRestricaoClienteVendedor(usuarioLogado)){
				query.where("responsavel.cdpessoa = ?", usuarioLogado.getCdpessoa());
			}
			
		return query.list();
	}
	
	/**
	 * M�todo que carrega dados da conta e reponsavel
	 * 
	 * @param contacrm
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Contacrm loadContaWithResponsavel(Contacrm contacrm){
		if(contacrm == null || contacrm.getCdcontacrm() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("contacrm.cdcontacrm, contacrm.nome, contacrm.tipo, responsavel.cdpessoa, responsavel.nome")
			.join("contacrm.responsavel responsavel")
			.where("contacrm = ?", contacrm)
			.unique();
	}
	
	/**M�todo que carrega dados da conta de acordo com o autocomplete
	 * 
	 * @param q
	 * @return
	 * @author M�rio Caixeta
	 */
	public List<Contacrm> findContacrmAutocomplete(String q) {
		return query()
					.select("contacrm.nome, contacrm.cdcontacrm, listcontacrmcontato.cdcontacrmcontato")
					.leftOuterJoin("contacrm.listcontacrmcontato listcontacrmcontato")
					.whereLikeIgnoreAll("contacrm.nome", q)
					.orderBy("contacrm.nome")
					.list();
	}
	
	/**M�todo que carrega dados da conta utilizando o cd passado como String
	 * 
	 * @param whereIn
	 * @return
	 * @author M�rio Caixeta
	 */
	public Contacrm carregaContacrmcontato(String whereIn) {
		return query()
				.select("contacrm.cdcontacrm, contacrm.nome, " +
						"listcontacrmcontato.cdcontacrmcontato, listcontacrmcontato.nome, " +
						"responsavel.nome, responsavel.cdpessoa, listcontacrmcontatoemail.cdcontacrmcontatoemail, " +
						"listcontacrmcontatoemail.email, listcontacrmcontatofone.telefone ")
					.leftOuterJoin("contacrm.responsavel responsavel")
					.leftOuterJoin("contacrm.listcontacrmcontato listcontacrmcontato")
					.leftOuterJoin("listcontacrmcontato.listcontacrmcontatoemail listcontacrmcontatoemail")
					.leftOuterJoin("listcontacrmcontato.listcontacrmcontatofone listcontacrmcontatofone")
					.whereIn("contacrm.cdcontacrm", whereIn)
				.unique();
	}
	
	public List<Contacrm> findContacrmcontato(String whereIn) {
		return query()
		.select("contacrm.cdcontacrm, contacrm.nome, " +
				"listcontacrmcontato.cdcontacrmcontato, listcontacrmcontato.nome, " +
				"responsavel.nome, responsavel.cdpessoa, listcontacrmcontatoemail.cdcontacrmcontatoemail, " +
				"listcontacrmcontatoemail.email ")
		.leftOuterJoin("contacrm.responsavel responsavel")
		.leftOuterJoin("contacrm.listcontacrmcontato listcontacrmcontato")
		.leftOuterJoin("listcontacrmcontato.listcontacrmcontatoemail listcontacrmcontatoemail")
		.whereIn("contacrm.cdcontacrm", whereIn)
		.list();
	}
	
	/**
	 * Realiza busca geral em Contacrm conforme par�metro da busca.
	 * @param busca
	 * @return
	 * @author Taidson
	 * @since 04/09/2010
	 */
	public List<Contacrm> findContacrmForBuscaGeral(String busca) {
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		
		QueryBuilder<Contacrm> query = query()
		.select("contacrm.cdcontacrm, contacrm.nome, " +
				"listcontacrmcontato.cdcontacrmcontato, listcontacrmcontato.nome")
		.leftOuterJoin("contacrm.listcontacrmcontato listcontacrmcontato")
		.leftOuterJoin("contacrm.responsavel responsavel")
		.leftOuterJoin("listcontacrmcontato.listcontacrmcontatoemail listcontacrmcontatoemail");

		if(SinedUtil.isRestricaoClienteVendedor(usuarioLogado)){
			if(ColaboradorService.getInstance().isColaborador(usuarioLogado.getCdpessoa())){
				query.where("responsavel.cdpessoa = ?", usuarioLogado.getCdpessoa());
			} else if(FornecedorService.getInstance().isFornecedor(usuarioLogado.getCdpessoa())){
				query.where("agencia.cdpessoa = ?", usuarioLogado.getCdpessoa());	
			}
		}
		
		return query.openParentheses()
		.whereLikeIgnoreAll("contacrm.nome", busca)
		.or()
		.whereLikeIgnoreAll("listcontacrmcontato.nome", busca)
		.or()
		.whereLikeIgnoreAll("listcontacrmcontatoemail.email", busca)
		.closeParentheses()
		.orderBy("contacrm.nome")
		.list();	
	}
	
	public List<Contacrm> findByEmail(String email) {
		return query()
					.select("contacrm.cdcontacrm, contacrm.nome")
					.leftOuterJoin("contacrm.listcontacrmcontato listcontacrmcontato")
					.leftOuterJoin("listcontacrmcontato.listcontacrmcontatoemail listcontacrmcontatoemail")
					.where("listcontacrmcontatoemail.email like ?", email)
					.list();
	}
	
	/**
	 * Busca informa��es para o webservice.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contacrm> findForWebservice(String nome) {
		return query()
					.select("contacrm.cdcontacrm, contacrm.nome, contacrm.tipo, contacrm.cpf, contacrm.cnpj, " +
							"contato.nome, contato.logradouro, contato.numero, contato.complemento, " +
							"contato.bairro, municipio.nome, uf.sigla")
					.leftOuterJoin("contacrm.listcontacrmcontato contato")
					.leftOuterJoin("contato.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.whereLikeIgnoreAll("contacrm.nome", nome)
					.list();
	}
	
	/**
	* Carrega os dados da contacrm com a lista de oportunidade e situacao 
	* 
	* @param whereIn
	* @return
	* @since Aug 3, 2011
	* @author Luiz Fernando F Silva
	*/
	public Contacrm carregaContacrmcontatoOportunidadeSituacao(String whereIn) {
		return query()
				.select("contacrm.cdcontacrm, contacrm.nome, campanha.cdcampanha, " +
						"listcontacrmcontato.cdcontacrmcontato, listcontacrmcontato.nome, " +
						"responsavel.nome, responsavel.cdpessoa, listoportunidade.cdoportunidade, " +
						"oportunidadesituacao.cdoportunidadesituacao, oportunidadesituacao.nome, listcontacrmhistorico.cdcontacrmhistorico, " +
						"listcontacrmhistorico.observacao, listcontacrmhistorico.dtaltera, listcontacrmhistorico.cdusuarioaltera")
					.leftOuterJoin("contacrm.responsavel responsavel")
					.leftOuterJoin("contacrm.listoportunidade listoportunidade")
					.leftOuterJoin("listoportunidade.oportunidadesituacao oportunidadesituacao")
					.leftOuterJoin("listoportunidade.campanha campanha")
					.leftOuterJoin("contacrm.listcontacrmcontato listcontacrmcontato")
					.leftOuterJoin("contacrm.listcontacrmhistorico listcontacrmhistorico")
					.whereIn("contacrm.cdcontacrm", whereIn)
				.unique();
	}
	
	/**
	* M�todo que busca UF/MUNICIPIO do colaborador cadastrado como responsavel na conta
	*
	* @param colaborador
	* @param uf
	* @return
	* @since Oct 6, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Contacrm> buscaEnderecoColaborador(Colaborador colaborador, Uf uf) {
		if(colaborador == null || colaborador.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("contacrm.cdcontacrm, listcontacrmcontato.cdcontacrmcontato, municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla ")
				.leftOuterJoin("contacrm.responsavel responsavel")
				.leftOuterJoin("contacrm.listcontacrmcontato listcontacrmcontato")
				.leftOuterJoin("listcontacrmcontato.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.where("responsavel = ?", colaborador)
				.where("uf = ?", uf)
				.list();
				
	}
	
	public List<Contacrm> findContacrmByLeads(String whereIn) {
		return query()
				.select("contacrm.cdcontacrm, lead.cdlead ")
					.leftOuterJoin("contacrm.lead lead")
					.whereIn("lead.cdlead", whereIn)
				.list();
	}
	
	/**
	 * M�todo que carrega as contacrm's selecionados na tela de listagem para desassocia��o de uma campanha.
	 * 
	 * @param whereIn
	 * @return
	 * @throws Exception
	 * @author Rafael Salvio
	 */
	public List<Contacrm> findAllForDesassociarCampanha(String whereIn) throws Exception{
		if(whereIn == null || whereIn.trim().isEmpty())
			throw new Exception("Par�metro inv�lido");
		
		return query()
					.select("contacrm.cdcontacrm, listcontacrmcontato.cdcontacrmcontato, listcampanhacontato.cdcampanhacontato, campanha.cdcampanha, campanha.nome")
					.leftOuterJoin("contacrm.listcontacrmcontato listcontacrmcontato")
					.leftOuterJoin("listcontacrmcontato.listcampanhacontato listcampanhacontato")
					.leftOuterJoin("listcampanhacontato.campanha campanha")
					.whereIn("contacrm.cdcontacrm", whereIn)
					.list();
	}
	
	public List<Contacrm> findForControleInteracao(ControleInteracaoBean bean){
		if(bean == null)
			throw new SinedException("Os dados da intera��o n�o podem ser nulos.");
		
		QueryBuilder<Contacrm> query = querySined();
		
		query
			.select("contacrm.cdcontacrm, contacrm.nome, listcontacrmcontatofone.cdcontacrmcontatofone, listcontacrmcontatofone.telefone, telefonetipo.cdtelefonetipo, " +
					"telefonetipo.nome, listcontacrmcontato.cdcontacrmcontato, listcontacrmcontato.nome, municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla, " +
					"segmento.cdsegmento, segmento.nome, listcontacrmhistorico.cdcontacrmhistorico, listcontacrmhistorico.dtaltera, " +
					"situacaohistorico.cdsituacaohistorico, situacaohistorico.descricao, atividadetipo.cdatividadetipo, atividadetipo.naoexibirnopainel")
			.leftOuterJoin("contacrm.listcontacrmcontato listcontacrmcontato")
			.leftOuterJoin("listcontacrmcontato.listcontacrmcontatofone listcontacrmcontatofone")
			.leftOuterJoin("listcontacrmcontatofone.telefonetipo telefonetipo")
			.leftOuterJoin("contacrm.listcontacrmsegmento listcontacrmsegmento")
			.leftOuterJoin("listcontacrmsegmento.segmento segmento")
			.leftOuterJoin("contacrm.listcontacrmhistorico listcontacrmhistorico")
			.leftOuterJoin("listcontacrmhistorico.atividadetipo atividadetipo")
			.leftOuterJoin("listcontacrmhistorico.situacaohistorico situacaohistorico")
			;
		
		if(bean.getContacrm() != null && bean.getContacrm().getCdcontacrm() != null){
			query.where("contacrm = ?", bean.getContacrm());
		}
		
		if(bean.getColaborador() != null){
			query.where("contacrm.responsavel = ?", bean.getColaborador());
		}
		
		if(bean.getSegmento() != null){
			query.where("segmento = ?", bean.getSegmento());
		}
		
		if(bean.getListaSituacaohistorico() != null && !bean.getListaSituacaohistorico().isEmpty()){
			String whereIn = "";
			for(Situacaohistorico item : bean.getListaSituacaohistorico()){
				whereIn += item.getCdsituacaohistorico().toString() + ",";
			}
			
			query.whereIn("situacaohistorico.cdsituacaohistorico", whereIn.substring(0, whereIn.length() - 1));
		}
		
		if(bean.getUf() != null){
			query.join("listcontacrmcontato.municipio municipio")
				.join("municipio.uf uf")
				.where("uf = ?", bean.getUf());
			if(bean.getMunicipio() != null){
				query.where("municipio = ?", bean.getMunicipio());
			} 
		} else{
			query.leftOuterJoin("listcontacrmcontato.municipio municipio")
				.leftOuterJoin("municipio.uf uf");
		}
		
		if(bean.getOrdenacao() != null && !bean.getOrdenacao().equals(OrdenacaoControleInteracao.CATEGORIA)){
			query.orderBy(bean.getOrdenacao().getCampo());					
		} else{
			query.orderBy("contacrm.nome, listcontacrmhistorico.dtaltera asc");
		}
		return query.list();
	}
	
	/**
	 * M�todo que carrega os dados de uma Contacrm para preenchimento de um OportuindadeRTF.
	 * @param conta
	 * @return
	 * @author Matheus Santos
	 */
	public Contacrm loadForMakeOportunidadeRTF(Contacrm conta){
		if(conta==null || conta.getCdcontacrm()==null)
			throw new RuntimeException("O par�metro conta n�o pode ser nulo.");
		
		return query()
					.select("contacrm.cdcontacrm, contacrm.cpf, contacrm.cnpj, " +
							"lead.bairro, lead.logradouro, lead.numero, municipio.nome, lead.cep, uf.sigla, lead.complemento")
					.leftOuterJoin("contacrm.lead lead")
					.leftOuterJoin("lead.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.where("contacrm = ?", conta)
					.unique();
	}
	
	public Contacrm findForContato(Contacrm conta){
		return query().select("contato.nome, telefone.telefone")
					.leftOuterJoin("contacrm.listcontacrmcontato contato")
					.leftOuterJoin("contato.listcontacrmcontatofone telefone")
					.where("contacrm = ?", conta)
					.unique();
	}
	
	public List<Contacrm> findWithResponsavel(String whereIn){
		return query()
					.select("contacrm.cdcontacrm, contacrm.nome, responsavel.cdpessoa, responsavel.nome")
					.leftOuterJoin("contacrm.responsavel responsavel")
					.whereIn("contacrm.cdcontacrm", whereIn)
					.list();
	}

	@Override
	public ListagemResult<Contacrm> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Contacrm> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("contacrm.nome");
		
		return new ListagemResult<Contacrm>(query, filtro);
	}
}
