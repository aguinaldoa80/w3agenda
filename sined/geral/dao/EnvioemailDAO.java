package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.enumeration.EmailStatusEnum;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.MailingEnviadosFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EnvioemailDAO extends GenericDAO<Envioemail> {
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaEnvioemailitem");		
	}
	
	/**
	 * M�todo que retorna a lista dos emails enviados pela aplica��o
	 *  
	 * @param filtro
	 * @author Thiago Clemente
	 */
	public List<Envioemail> findForMailingEnviados(MailingEnviadosFiltro filtro){
		String whereIn = filtro.getSituacaoemail()!=null? EmailStatusEnum.listAndConcatenate(filtro.getSituacaoemail()): null;
		return query()
				.select("envioemail.cdenvioemail, envioemail.dtenvio, envioemail.remetente, envioemail.assunto, envioemail.situacaoemail, envioemail.ultimaverificacao," +
						"envioemailtipo.nome, envioemailitem.cdenvioemailitem, envioemailitem.email, "+
						"pessoa.cdpessoa")
				.join("envioemail.envioemailtipo envioemailtipo")
				.join("envioemail.listaEnvioemailitem envioemailitem")
				.leftOuterJoin("envioemailitem.pessoa pessoa")
				.where("envioemailtipo=?", filtro.getEnvioemailtipo())
				.where("envioemail=?", filtro.getEnvioemail())
				
				.whereLikeIgnoreAll("pessoa.nome", filtro.getNomeDestinatario())
				.whereLikeIgnoreAll("envioemailitem.nomecontato", filtro.getNomeContato())
				.whereLikeIgnoreAll("envioemailitem.email", filtro.getEmailDestinatario())
				.where("envioemail.dtenvio >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getDataEnvioDe()))
				.where("envioemail.dtenvio <= ?", SinedDateUtils.dateToTimestampFinalDia(filtro.getDataEnvioAte()))
				.whereIn("envioemail.situacaoemail", whereIn)
				.orderBy("envioemail.dtenvio desc, envioemailtipo.nome, envioemail.assunto")
				.list();
	}
	
	public void updateStatusemail(Envioemail envioemail, EmailStatusEnum emailstatusenum){
		getJdbcTemplate().execute("update envioemail set situacaoemail = "+emailstatusenum.ordinal()+" where cdenvioemail = "+envioemail.getCdenvioemail());
	}
	
	public void updateUltimaverificacao(Envioemail envioemail, Timestamp ultimaverificacao){
		String sql = "update envioemail set ultimaverificacao = ? where cdenvioemail = "+envioemail.getCdenvioemail();
		getJdbcTemplate().update(sql, new Object[]{ultimaverificacao});
	}
}