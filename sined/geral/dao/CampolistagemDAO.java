package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.Campolistagem;
import br.com.linkcom.sined.geral.bean.Tela;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CampolistagemDAO extends GenericDAO<Campolistagem>{
	
	/**
	 * M�todo para verificar se existe campos da listagem cadastrados para a tela
	 *
	 * @param tela
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existeCamposCadastrados(Tela tela){
		if(tela == null || tela.getCdtela() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilder(Long.class)
		.select("count(*)")
		.from(Campolistagem.class)
		.join("campolistagem.tela tela")
		.where("tela.cdtela = ?", tela.getCdtela())
		.unique() > 0;
	}
}
