package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.SaldoIpi;
import br.com.linkcom.sined.geral.bean.enumeration.CreditoDebitoEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SaldoIpiFiltro;

public class SaldoIpiDAO extends GenericDAO<SaldoIpi> {

	@Override
	public void updateListagemQuery(QueryBuilder<SaldoIpi> query, FiltroListagem _filtro) {
		SaldoIpiFiltro filtro = (SaldoIpiFiltro) _filtro;

		query
			.select("saldoIpi.mesAno, saldoIpi.valor, empresa.cdpessoa, empresa.nomefantasia, saldoIpi.tipoUtilizacao ")
			.leftOuterJoin("saldoIpi.empresa empresa");

		if (filtro.getEmpresa() != null) {
			query.where("empresa = ?", filtro.getEmpresa());
		}

		if (filtro.getValor() != null) {
			query.where("saldoIpi.valor = ?", filtro.getValor());
		}
		
		if (filtro.getMesAno() != null) {
			query.where("saldoIpi.mesAno = ?", filtro.getMesAno());
		}
		
		if (filtro.getTipoUtilizacao() != null) {
			query.where("saldoIpi.tipoUtilizacao = ?", filtro.getTipoUtilizacao());
		}

		query.orderBy("saldoIpi.mesAno desc");
		query.list();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<SaldoIpi> query) {
		query
			.select("saldoIpi.cdSaldoIpi, saldoIpi.mesAno, saldoIpi.valor, empresa.cdpessoa, empresa.nomefantasia, saldoIpi.tipoUtilizacao ")
			.leftOuterJoin("saldoIpi.empresa empresa");
	}

	public SaldoIpi getLastMesAnoSaldoIpiByEmpresa(Empresa empresa, CreditoDebitoEnum debitoCredito) {
		return query()
				.select("saldoIpi.cdSaldoIpi, saldoIpi.mesAno, saldoIpi.valor, empresa.cdpessoa, empresa.nomefantasia, saldoIpi.tipoUtilizacao ")
				.leftOuterJoin("saldoIpi.empresa empresa")
				.where("empresa = ?", empresa)
				.where("saldoIpi.tipoUtilizacao = ?", debitoCredito)
				.orderBy("saldoIpi.mesAno desc")
				.setMaxResults(1)
				.unique();
	}

	public SaldoIpi getSaldoIpiByMesAnoEmpresaTipoUtilizacao(SaldoIpi bean) {
		return query()
				.select("saldoIpi.cdSaldoIpi, saldoIpi.mesAno, saldoIpi.valor, empresa.cdpessoa, empresa.nomefantasia, saldoIpi.tipoUtilizacao ")
				.leftOuterJoin("saldoIpi.empresa empresa")
				.where("empresa = ?", bean.getEmpresa())
				.where("saldoIpi.mesAno = ?", bean.getMesAno())
				.where("saldoIpi.tipoUtilizacao = ?", bean.getTipoUtilizacao())
				.setMaxResults(1)
				.unique();
	}
}
