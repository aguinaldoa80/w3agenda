package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.ClienteBancoDados;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.ClienteBancoDadosFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ClienteBancoDadosDAO extends GenericDAO<ClienteBancoDados>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<ClienteBancoDados> query,FiltroListagem _filtro) {
		ClienteBancoDadosFiltro filtro = (ClienteBancoDadosFiltro) _filtro;
		query.select("cliente.nome, contrato.descricao, clienteBancoDados.nomebanco, clienteBancoDados.cdclientebancodados");
		query.leftOuterJoin("clienteBancoDados.contrato contrato");
		query.leftOuterJoin("clienteBancoDados.cliente cliente");
		query.where("cliente = ?", filtro.getCliente());
		query.where("contrato = ?", filtro.getContrato());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<ClienteBancoDados> query) {
		query.leftOuterJoinFetch("clienteBancoDados.listaModulos listaModulos");
		query.leftOuterJoinFetch("clienteBancoDados.listaTelas listaTelas");
		query.leftOuterJoinFetch("listaTelas.tela tela");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaModulos");
		save.saveOrUpdateManaged("listaTelas");
	}
	
	/** M�todo que busca os clientes Banco Dados por Categoria e Projeto
	 * @author Thiago.Augusto
	 */
	public List<ClienteBancoDados> findClienteBancoDados(Categoria categoria, Projeto projeto){
		QueryBuilder<ClienteBancoDados> query = query();
		query.select("contrato.cdcontrato, contrato.descricao, cliente.nome, cliente.cnpj, cliente.cpf")
		.leftOuterJoin("clienteBancoDados.cliente cliente")
		.leftOuterJoin("clienteBancoDados.contrato contrato")
		.leftOuterJoin("cliente.listaProjeto projeto")
		.leftOuterJoin("cliente.listaPessoacategoria listaPessoacategoria")
		.leftOuterJoin("listaPessoacategoria.categoria categoria")
		.where("categoria = ?", categoria)
		.where("projeto = ?", projeto)
		.orderBy("cliente.nome, contrato.descricao");
		return query.list();
	}

	public List<ClienteBancoDados> findForAcessos() {
		return query()
					.select("clienteBancoDados.cdclientebancodados, clienteBancoDados.nomebanco")
					.where("clienteBancoDados.nomebanco is not null")
					.where("clienteBancoDados.nomebanco <> ''")
					.list();
	}

	public ClienteBancoDados findByContrato(Contrato contrato) {
		List<ClienteBancoDados> list = query()
					.select("clienteBancoDados.cdclientebancodados, clienteBancoDados.nomebanco")
					.leftOuterJoin("clienteBancoDados.contrato contrato")
					.where("contrato = ?", contrato)
					.where("clienteBancoDados.nomebanco is not null")
					.where("clienteBancoDados.nomebanco <> ''")
					.list();
		
		if(list != null && list.size() > 0) return list.get(0);
		else return null;
	}

	public List<ClienteBancoDados> findForLicenca(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		return query()
					.select("clienteBancoDados.cdclientebancodados, clienteBancoDados.nomebanco, cliente.cpf, cliente.cnpj, " +
							"contrato.cdcontrato, tela.path, listaModulos.modulo")
					.join("clienteBancoDados.contrato contrato")
					.join("clienteBancoDados.cliente cliente")
					.leftOuterJoin("clienteBancoDados.listaModulos listaModulos")
					.leftOuterJoin("clienteBancoDados.listaTelas listaTelas")
					.leftOuterJoin("listaTelas.tela tela")
					.whereIn("clienteBancoDados.cdclientebancodados", whereIn)
					.list();
	}

	public ClienteBancoDados findForVersaoAtual(Integer cdpessoa, Integer cdcontrato) {
		List<ClienteBancoDados> list = query()
					.select("clienteBancoDados.cdclientebancodados, clienteBancoDados.nomebanco, clienteBancoDados.versaoatual ")
					.leftOuterJoin("clienteBancoDados.contrato contrato")
					.leftOuterJoin("clienteBancoDados.cliente cliente")
					.where("contrato.cdcontrato = ?", cdcontrato)
					.where("cliente.cdpessoa = ?", cdpessoa)
					.where("clienteBancoDados.nomebanco is not null")
					.where("clienteBancoDados.nomebanco <> ''")
					.list();
		
		if(list != null && list.size() > 0) return list.get(0);
		else return null;
	}
	
	
}
