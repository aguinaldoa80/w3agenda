package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.EventosCorreios;
import br.com.linkcom.sined.geral.bean.TipoEventoCorreios;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EventosCorreiosDAO extends GenericDAO<EventosCorreios>{

	public EventosCorreios loadByCodigoAndTipoEvento(TipoEventoCorreios tipoEventoCorreios, Integer codigo){
		return querySined()
				.join("eventosCorreios.tipoEventoCorreios tipoEventoCorreios")
				.where("tipoEventoCorreios = ?", tipoEventoCorreios)
				.where("eventosCorreios.codigo = ?", codigo)
				.setMaxResults(1)
				.unique();
	}
	
	public List<EventosCorreios>findDescricaoByTipo(TipoEventoCorreios tipoEvento){
		return query()
					.select("eventosCorreios.cdeventoscorreios, eventosCorreios.descricao, eventosCorreios.detalhe, eventosCorreios.codigo")
					.leftOuterJoin("eventosCorreios.tipoEventoCorreios tipoeventoscorreios")
					.where("tipoeventoscorreios = ?",tipoEvento)
					.orderBy("eventosCorreios.descricao")
					.list();
	}
	
	public EventosCorreios findCodigoByEvento (EventosCorreios evento){
		return query()
					.select("eventosCorreios.cdeventoscorreios, eventosCorreios.codigo")
					.where("eventosCorreios = ?", evento)
					.unique();
	}
	
	public List<EventosCorreios> findAllWithTipoEvento(){
		return querySined()
				.select("eventosCorreios.cdeventoscorreios, eventosCorreios.codigo, eventosCorreios.descricao, eventosCorreios.detalhe, "+
						"eventosCorreios.eventoFinal, "+
						"tipoEventoCorreios.cdTipoEventoCorreios, tipoEventoCorreios.descricao")
				.join("eventosCorreios.tipoEventoCorreios tipoEventoCorreios")
				.list();
	}
}
