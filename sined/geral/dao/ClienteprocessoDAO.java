package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Clienteprocesso;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("clienteprocesso.dtvencimento")
public class ClienteprocessoDAO extends GenericDAO<Clienteprocesso>{

}
