package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Processojuridico;
import br.com.linkcom.sined.geral.bean.Processojuridicoarquivo;
import br.com.linkcom.sined.geral.bean.Processojuridicosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Processojuridicoclienteenvolvido;
import br.com.linkcom.sined.modulo.juridico.controller.crud.filter.ProcessojuridicoFiltro;
import br.com.linkcom.sined.modulo.juridico.controller.report.bean.QuantitativoProcessojuridicoBean;
import br.com.linkcom.sined.modulo.juridico.controller.report.bean.QuantitativoProcessojuridicoSituacaoBean;
import br.com.linkcom.sined.modulo.juridico.controller.report.filter.QuantitativoProcessojuridicoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProcessojuridicoDAO extends GenericDAO<Processojuridico>{
	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Processojuridico> query, FiltroListagem _filtro) {
		
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		
		ProcessojuridicoFiltro filtro = (ProcessojuridicoFiltro)_filtro;
		
		query
			.select("distinct processojuridico.cdprocessojuridico, processojuridico.data, " +
					"area.descricao, advogado.nome, processojuridicosituacao.nome")
			
			.leftOuterJoin("processojuridico.processojuridicosituacao processojuridicosituacao")
			.leftOuterJoin("processojuridico.area area")
			.leftOuterJoin("processojuridico.advogado advogado")
			
			.leftOuterJoin("processojuridico.areatipo areatipo", true)
			.leftOuterJoin("processojuridico.listaProcessojuridicoinstancia processojuridicoinstancia", true)
			.leftOuterJoin("processojuridicoinstancia.processojuridicoinstanciatipo processojuridicoinstanciatipo", true)
			.leftOuterJoin("processojuridico.listaProcessojuridicocliente processojuridicocliente", true)
			.leftOuterJoin("processojuridicocliente.cliente cliente", true)
			.leftOuterJoin("processojuridicocliente.partecontraria partecontraria", true)
			
			.where("processojuridico.data >= ?", filtro.getDatainicio())
			.where("processojuridico.data <= ?", filtro.getDatafim())
			.where("area = ?", filtro.getArea())
			.where("areatipo = ?", filtro.getAreatipo())
			.where("advogado = ?", filtro.getAdvogado())
			.where("cliente = ?", filtro.getCliente())
			.where("partecontraria = ?", filtro.getPartecontraria())
			.where("processojuridicoinstanciatipo = ?", filtro.getProcessojuridicoinstanciatipo())
			.where("processojuridicoinstancia.numero = ?", filtro.getNumeroinstancia())
			.where("processojuridico.projeto = ?", filtro.getProjeto());
		
		if(filtro.getListaProcessojuridicosituacao() != null && filtro.getListaProcessojuridicosituacao().size() > 0){
			query.whereIn("processojuridicosituacao.cdprocessojuridicosituacao", CollectionsUtil.listAndConcatenate(filtro.getListaProcessojuridicosituacao(), "cdprocessojuridicosituacao", ","));
		}
	}
	
	public List<Processojuridico> loadWithLista(String whereIn, String orderBy, boolean asc) {
		QueryBuilder<Processojuridico> query = query()
				.select("processojuridico.cdprocessojuridico, processojuridico.data, area.descricao, " +
						"advogado.nome, processojuridicosituacao.nome, processojuridicoinstanciatipo.cdprocessojuridicoinstanciatipo, " +
						"cliente.nome, processojuridicocliente.envolvido, vprocessojuridico.descricao, processojuridicoinstancia.numero")
				
				.leftOuterJoin("processojuridico.processojuridicosituacao processojuridicosituacao")
				.leftOuterJoin("processojuridico.area area")
				.leftOuterJoin("processojuridico.advogado advogado")
				.leftOuterJoin("processojuridico.listaProcessojuridicoinstancia processojuridicoinstancia")
				.leftOuterJoin("processojuridicoinstancia.processojuridicoinstanciatipo processojuridicoinstanciatipo")
				.leftOuterJoin("processojuridico.listaProcessojuridicocliente processojuridicocliente")
				.leftOuterJoin("processojuridicocliente.cliente cliente")
				.leftOuterJoin("processojuridico.vprocessojuridico vprocessojuridico")
				.whereIn("processojuridico.cdprocessojuridico", whereIn);
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		}
		
		return query.list();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Processojuridico> query) {
		query
		.select(	"processojuridico.cdprocessojuridico, processojuridico.data, processojuridico.tipohonorario, processojuridico.honorario, " +
					"processojuridico.valorcontrato, processojuridico.sintese, processojuridico.valorcausa, processojuridico.cdusuarioaltera, processojuridico.dtaltera, " +
					"processorelacionado.cdprocessojuridico, " +
					"prazopagamento.cdprazopagamento, " +
					"listaprocessorelacionado.cdprocessojuridico, " +
					"advogadorelacionado.nome, listaprocessorelacionado.valorcausa, arearelacionada.descricao, " +
					"processojuridicosituacao.cdprocessojuridicosituacao, processojuridicosituacao.nome, " +
					"processojuridicocliente.cdprocessojuridicocliente, processojuridicocliente.envolvido, " +
					"processojuridicoinstancia.cdprocessojuridicoinstancia, processojuridicoinstancia.processojuridicoinstanciatipo," +
					"processojuridicoinstancia.cdprocessojuridicoinstancia, processojuridicoinstancia.numero, processojuridicoinstancia.vara, " +
					"processojuridicoinstancia.data, area.cdarea, area.descricao, " +
					"advogado.cdpessoa, advogado.nome, " +
					"processojuridicocliente.cdprocessojuridicocliente, processojuridicocliente.processojuridicoclientetipo, processojuridicocliente.encabecador, " +
					"cliente.cdpessoa, cliente.nome, " +
					"partecontraria.cdpartecontraria, partecontraria.nome, " +
					"processojuridicoclientetipo.cdprocessojuridicoclientetipo, processojuridicoclientetipo.nome, " +
					"processojuridicoinstanciatipo.cdprocessojuridicoinstanciatipo, processojuridicoinstanciatipo.nome, " +
					"processojuridicoarquivo.cdprocessojuridicoarquivo, processojuridicoarquivo.descricao, " +
					"arquivo.cdarquivo, arquivo.nome, arquivo.tipoconteudo, arquivo.tamanho," +
					"vara.cdvara, vara.nome, areatipo.cdareatipo, areatipo.descricao, " +
					"processojuridicoquestionario.cdprocessojuridicoquestionario, processojuridicoquestionario.pergunta, processojuridicoquestionario.resposta, " +
					"processojuridicoquestionario.tiporesposta, processojuridicoquestionario.ordem, processojuridico.projeto ")
				
				.leftOuterJoin("processojuridico.prazopagamento prazopagamento")
				.leftOuterJoin("processojuridico.advogado advogado")
				.leftOuterJoin("processojuridico.area area")
				.leftOuterJoin("processojuridico.areatipo areatipo")
				.leftOuterJoin("processojuridico.processorelacionado processorelacionado")
				.leftOuterJoin("processojuridico.processojuridicosituacao processojuridicosituacao")
				
				.leftOuterJoin("processojuridico.listaProcessojuridicocliente processojuridicocliente")
				.leftOuterJoin("processojuridicocliente.cliente cliente")
				.leftOuterJoin("processojuridicocliente.partecontraria partecontraria")
				.leftOuterJoin("processojuridicocliente.processojuridicoclientetipo processojuridicoclientetipo")
				
				.leftOuterJoin("processojuridico.listaProcessojuridicoquestionario processojuridicoquestionario")
				
				.leftOuterJoin("processojuridico.listaProcessojuridicoinstancia processojuridicoinstancia")
				.leftOuterJoin("processojuridicoinstancia.vara vara")
				
				.leftOuterJoin("processojuridicoinstancia.processojuridicoinstanciatipo processojuridicoinstanciatipo")
				
				.leftOuterJoin("processojuridico.listaProcessojuridicoarquivo processojuridicoarquivo")
				.leftOuterJoin("processojuridicoarquivo.arquivo arquivo")
				
				.leftOuterJoin("processojuridico.listaprocessorelacionado listaprocessorelacionado")
				.leftOuterJoin("listaprocessorelacionado.advogado advogadorelacionado")
				.leftOuterJoin("listaprocessorelacionado.area arearelacionada")
				
				.leftOuterJoin("processojuridico.projeto projeto")
				
				.orderBy("processojuridicoinstanciatipo.nome, processojuridicoquestionario.ordem")
			;
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback(){
			
			Processojuridico processojuridico = (Processojuridico) save.getEntity();
			
			public Object doInTransaction(TransactionStatus status){
				
				if(processojuridico.getListaProcessojuridicoarquivo() != null && processojuridico.getListaProcessojuridicoarquivo().size() > 0){
					for (Processojuridicoarquivo processojuridicoarquivo : processojuridico.getListaProcessojuridicoarquivo()) {
						arquivoDAO.saveFile(processojuridicoarquivo, "arquivo");
					}
				}
				save.saveOrUpdateManaged("listaProcessojuridicocliente");
				save.saveOrUpdateManaged("listaProcessojuridicoarquivo");
				save.saveOrUpdateManaged("listaProcessojuridicoinstancia");
				save.saveOrUpdateManaged("listaProcessojuridicoquestionario");
				
				return null;
			}
		});
	}
	
	/**
	 * Popula a combo de processos relacionados.
	 * @author Taidson Santos
	 * @since 16/03/2010
	 * @param request
	 */
	public List<Processojuridico> populaCombo(Processojuridico processojuridico) {
		return query()
				.select("processojuridico.cdprocessojuridico, vprocessojuridico.descricao")
				.join("processojuridico.vprocessojuridico vprocessojuridico")
				.where("processojuridico.cdprocessojuridico <> ?", processojuridico.getCdprocessojuridico())
				.where("coalesce (processojuridico.processorelacionado.id, 0) <> ?", processojuridico.getCdprocessojuridico())
				.orderBy("processojuridico.sintese")
				.list();
	}

	/**
	 * Busca os processos juridicos de um determinado cliente.
	 *
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Processojuridico> findByCliente(Cliente cliente) {
		if(cliente == null || cliente.getCdpessoa() == null){
			throw new SinedException("O cliente n�o pode ser nulo.");
		}
		return query()
					.select("processojuridico.cdprocessojuridico, vprocessojuridico.descricao")
					.join("processojuridico.vprocessojuridico vprocessojuridico")
					.join("processojuridico.listaProcessojuridicocliente listaProcessojuridicocliente")
					.join("listaProcessojuridicocliente.cliente cliente")
					.where("cliente = ?", cliente)
					.list();
	}
	
	/**
	 * 
	 * @param filtro
	 * @author Thiago Clemente
	 * 
	 */
	public List<QuantitativoProcessojuridicoBean> findForQuantitativoProcessoJuridicoCliente(QuantitativoProcessojuridicoFiltro filtro){
		
		QueryBuilder<Object[]> queryBuilder = getQueryBuilderQuantitativoProcessoJuridico(filtro);
		queryBuilder.select("count(distinct processojuridico.cdprocessojuridico), cliente.nome, processojuridicosituacao.cdprocessojuridicosituacao, processojuridicosituacao.nome")
					.join("processojuridico.listaProcessojuridicocliente listaProcessojuridicocliente")
					.join("listaProcessojuridicocliente.cliente cliente")
					.leftOuterJoin("cliente.listaPessoacategoria listaPessoacategoria")
					.leftOuterJoin("listaPessoacategoria.categoria categoria")
					.where("listaProcessojuridicocliente.envolvido=?", Processojuridicoclienteenvolvido.CLIENTE)
					.where("categoria=?", filtro.getCategoria())
					.groupBy("cliente.nome, processojuridicosituacao.cdprocessojuridicosituacao, processojuridicosituacao.nome");
		
		List<Object[]> listaObjeto = queryBuilder.list();
		List<QuantitativoProcessojuridicoBean> listaBean = getListaQuantitativoProcessojuridicoBean(listaObjeto);
		
		return listaBean;
	}
	
	/**
	 * 
	 * @param filtro
	 * @author Thiago Clemente
	 * 
	 */
	public List<QuantitativoProcessojuridicoBean> findForQuantitativoProcessoJuridicoParteContraria(QuantitativoProcessojuridicoFiltro filtro){
		
		QueryBuilder<Object[]> queryBuilder = getQueryBuilderQuantitativoProcessoJuridico(filtro);
		queryBuilder.select("count(distinct processojuridico.cdprocessojuridico), partecontraria.nome, processojuridicosituacao.cdprocessojuridicosituacao, processojuridicosituacao.nome")
					.join("processojuridico.listaProcessojuridicocliente listaProcessojuridicocliente")
					.join("listaProcessojuridicocliente.partecontraria partecontraria")
					.where("listaProcessojuridicocliente.envolvido=?", Processojuridicoclienteenvolvido.PARTE_CONTRARIA)
					.groupBy("partecontraria.nome, processojuridicosituacao.cdprocessojuridicosituacao, processojuridicosituacao.nome");
		
		List<Object[]> listaObjeto = queryBuilder.list();
		List<QuantitativoProcessojuridicoBean> listaBean = getListaQuantitativoProcessojuridicoBean(listaObjeto);
		
		return listaBean;
	}
	
	/**
	 * 
	 * @param filtro
	 * @author Thiago Clemente
	 * 
	 */
	public List<QuantitativoProcessojuridicoBean> findForQuantitativoProcessoJuridicoArea(QuantitativoProcessojuridicoFiltro filtro){
		
		QueryBuilder<Object[]> queryBuilder = getQueryBuilderQuantitativoProcessoJuridico(filtro);
		queryBuilder.select("count(distinct processojuridico.cdprocessojuridico), area.descricao, processojuridicosituacao.cdprocessojuridicosituacao, processojuridicosituacao.nome")
					.join("processojuridico.area area")
					.groupBy("area.descricao, processojuridicosituacao.cdprocessojuridicosituacao, processojuridicosituacao.nome");
		
		List<Object[]> listaObjeto = queryBuilder.list();
		List<QuantitativoProcessojuridicoBean> listaBean = getListaQuantitativoProcessojuridicoBean(listaObjeto);
		
		return listaBean;
	}
	
	/**
	 * 
	 * @param filtro
	 * @author Thiago Clemente
	 * 
	 */
	public List<QuantitativoProcessojuridicoBean> findForQuantitativoProcessoJuridicoAdvogado(QuantitativoProcessojuridicoFiltro filtro){
		
		QueryBuilder<Object[]> queryBuilder = getQueryBuilderQuantitativoProcessoJuridico(filtro);
		queryBuilder.select("count(distinct processojuridico.cdprocessojuridico), advogado.nome, processojuridicosituacao.cdprocessojuridicosituacao, processojuridicosituacao.nome")
					.join("processojuridico.advogado advogado")
					.groupBy("advogado.nome, processojuridicosituacao.cdprocessojuridicosituacao, processojuridicosituacao.nome");
		
		List<Object[]> listaObjeto = queryBuilder.list();
		List<QuantitativoProcessojuridicoBean> listaBean = getListaQuantitativoProcessojuridicoBean(listaObjeto);
		
		return listaBean;
	}
	
	/**
	 * 
	 * @param filtro
	 * @author Thiago Clemente
	 * 
	 */
	private QueryBuilder<Object[]> getQueryBuilderQuantitativoProcessoJuridico(QuantitativoProcessojuridicoFiltro filtro){
		
		QueryBuilder<Object[]> queryBuilder = newQueryBuilder(Object[].class);
		queryBuilder.from(Processojuridico.class)
					.join("processojuridico.advogado advogado")
					.join("processojuridico.processojuridicosituacao processojuridicosituacao")
					.leftOuterJoin("processojuridico.area area")
					.leftOuterJoin("processojuridico.areatipo areatipo")							
					.where("processojuridico.data>=?", filtro.getDtinicio())
					.where("processojuridico.data<=?", filtro.getDtfim())
					.where("advogado=?", filtro.getAdvogado())
					.where("area=?", filtro.getArea())
					.where("areatipo=?", filtro.getAreatipo())
					.setUseTranslator(false);		
		
		return queryBuilder;
	}
	
	/**
	 * 
	 * @param listaObjeto
	 * @author Thiago Clemente
	 * 
	 */
	private List<QuantitativoProcessojuridicoBean> getListaQuantitativoProcessojuridicoBean(List<Object[]> listaObjeto){
		
		List<QuantitativoProcessojuridicoBean> listaBean = new ArrayList<QuantitativoProcessojuridicoBean>();
		QuantitativoProcessojuridicoBean bean = null;
		Map<String, List<QuantitativoProcessojuridicoSituacaoBean>> mapa = new HashMap<String, List<QuantitativoProcessojuridicoSituacaoBean>>();
		
		for (Object[] objeto: listaObjeto){
			List<QuantitativoProcessojuridicoSituacaoBean> listaSituacaoBeanAux = mapa.get((String) objeto[1]);
			QuantitativoProcessojuridicoSituacaoBean situacaoBean = new QuantitativoProcessojuridicoSituacaoBean();
			situacaoBean.setProcessojuridicosituacao(new Processojuridicosituacao((Integer) objeto[2], (String) objeto[3]) );
			situacaoBean.setTotal(((Long) objeto[0]).intValue());
			
			if (listaSituacaoBeanAux==null){
				listaSituacaoBeanAux = new ArrayList<QuantitativoProcessojuridicoSituacaoBean>();
			}
			listaSituacaoBeanAux.add(situacaoBean);
			
			mapa.put((String) objeto[1], listaSituacaoBeanAux);
		}
		
		for (String key: mapa.keySet()){
			bean = new QuantitativoProcessojuridicoBean();
			bean.setNome(key);
			bean.setListaSituacaoBean(mapa.get(key));
			listaBean.add(bean);
		}
		
		return listaBean;
	}

	public List<Processojuridico> findAutocomplete(String q) {
		return query()
					.select("processojuridico.cdprocessojuridico, vprocessojuridico.descricao")
					.join("processojuridico.vprocessojuridico vprocessojuridico")
					.whereLikeIgnoreAll("vprocessojuridico.descricao", q)
					.orderBy("vprocessojuridico.descricao, processojuridico.cdprocessojuridico")
					.list();
	}

	public List<Processojuridico> findForGeracaoContrato(String whereIn) {
		return query()
					.select("processojuridico.cdprocessojuridico, processojuridico.data, processojuridico.valorcontrato, " +
							"processojuridico.valorcausa, processojuridico.tipohonorario, processojuridico.honorario, " +
							"cliente.cdpessoa, processojuridicocliente.envolvido, area.descricao, areatipo.descricao, " +
							"centrocusto.cdcentrocusto, contagerencial.cdcontagerencial, projeto.cdprojeto, prazopagamentoitem.dias, " +
							"prazopagamentoitem.meses, prazopagamento.cdprazopagamento")
					.leftOuterJoin("processojuridico.listaProcessojuridicocliente processojuridicocliente")
					.leftOuterJoin("processojuridicocliente.cliente cliente")
					.leftOuterJoin("processojuridico.prazopagamento prazopagamento")
					.leftOuterJoin("prazopagamento.listaPagamentoItem prazopagamentoitem")
					.leftOuterJoin("processojuridico.areatipo areatipo")
					.leftOuterJoin("processojuridico.area area")
					.leftOuterJoin("area.centrocusto centrocusto")
					.leftOuterJoin("area.contagerencial contagerencial")
					.leftOuterJoin("area.projeto projeto")
					.whereIn("processojuridico.cdprocessojuridico", whereIn)
					.list();
	}
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Processojuridico> findForFichaProcessoJuridico(String whereIn) {
		return querySined()
				
			.select("processojuridico.cdprocessojuridico, area.descricao, areatipo.descricao, advogado.nome," +
					"processojuridico.sintese")
			.leftOuterJoin("processojuridico.area area")
			.leftOuterJoin("processojuridico.areatipo areatipo")
			.leftOuterJoin("processojuridico.advogado advogado")
			.whereIn("processojuridico.cdprocessojuridico", whereIn)
			.list();
	}
}