package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Naturezaoperacaocst;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NaturezaoperacaocstDAO extends GenericDAO<Naturezaoperacaocst>{
	
	/**
	 * 
	 * @param naturezaoperacao
	 * @author Thiago Clemente
	 * 
	 */
	public List<Naturezaoperacaocst> findByNaturezaoperacao(Naturezaoperacao naturezaoperacao){
		return query()
				.join("naturezaoperacaocst.naturezaoperacao naturezaoperacao")
				.where("naturezaoperacao=?", naturezaoperacao)
				.list();
	}
}
