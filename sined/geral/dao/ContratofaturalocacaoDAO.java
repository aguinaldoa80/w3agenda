package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.HashSet;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.Contratofaturalocacaosituacao;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ContratofaturalocacaoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EmitirIndenizacaoContratoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EvolucaoFaturamentoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContratofaturalocacaoDAO extends GenericDAO<Contratofaturalocacao>{

	@Override
	public void updateListagemQuery(QueryBuilder<Contratofaturalocacao> query, FiltroListagem _filtro) {
		ContratofaturalocacaoFiltro filtro = (ContratofaturalocacaoFiltro)_filtro;
		
		query.select("distinct contratofaturalocacao.cdcontratofaturalocacao, contratofaturalocacao.numero, contratofaturalocacao.valortotal, contratofaturalocacao.dtemissao, " +
				"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, cliente.cdpessoa, cliente.nome, " +
				"contratofaturalocacao.situacao")
			.leftOuterJoin("contratofaturalocacao.empresa empresa")
			.leftOuterJoin("contratofaturalocacao.cliente cliente");
		
		if(filtro.getContrato() != null){
			query.leftOuterJoin("contratofaturalocacao.listaContratofaturalocacaoorigem contratofaturalocacaoorigem");
			query.leftOuterJoin("contratofaturalocacao.contrato contrato");
			query.leftOuterJoin("contratofaturalocacaoorigem.contrato contratoorigem");
			query
				.openParentheses()
					.where("contrato = ?", filtro.getContrato())
					.or()
					.where("contratoorigem = ?", filtro.getContrato())
				.closeParentheses();			
		}
		
		if(filtro.getMaterial() != null){
			query.leftOuterJoin("contratofaturalocacao.listaContratofaturalocacaomaterial listaContratofaturalocacaomaterial");
			query.where("listaContratofaturalocacaomaterial.material = ?", filtro.getMaterial());
		}
		
		query
			.where("contratofaturalocacao.empresa = ?", filtro.getEmpresa())
			.where("contratofaturalocacao.cliente = ?", filtro.getCliente())
			.where("contratofaturalocacao.numero >= ?", filtro.getNumero1())
			.where("contratofaturalocacao.numero <= ?", filtro.getNumero2())
			.where("contratofaturalocacao.dtemissao >= ? ", filtro.getPeriodoEmissaoInicio())
			.where("contratofaturalocacao.dtemissao <= ?", filtro.getPeriodoEmissaoFim())
			.where("contratofaturalocacao.dtvencimento >= ? ", filtro.getPeriodoVencimentoInicio())
			.where("contratofaturalocacao.dtvencimento <= ?", filtro.getPeriodoVencimentoFim())
			.where("contratofaturalocacao.cdcontratofaturalocacao in (select contrato.cdcontratofaturalocacao "
					+ "from contratofaturalocacao contrato " 
					+ "left join contrato.listaContratofaturalocacaohistorico listaContratofaturalocacaohistorico " 
					+ "where listaContratofaturalocacaohistorico.acao = 1 " 
					+ "and listaContratofaturalocacaohistorico.contratofaturalocacao = contratofaturalocacao " 
					+ "and listaContratofaturalocacaohistorico.dtaltera >= ? )", filtro.getPeriodoCancelamentoInicio())
			.where("contratofaturalocacao.cdcontratofaturalocacao in (select contrato.cdcontratofaturalocacao "
					+ "from contratofaturalocacao contrato " 
					+ "left join contrato.listaContratofaturalocacaohistorico listaContratofaturalocacaohistorico " 
					+ "where listaContratofaturalocacaohistorico.acao = 1 " 
					+ "and listaContratofaturalocacaohistorico.contratofaturalocacao = contratofaturalocacao " 
					+ "and listaContratofaturalocacaohistorico.dtaltera <= ? )", filtro.getPeriodoCancelamentoFim())
			
			.orderBy("contratofaturalocacao.numero desc")
			.ignoreJoin("listaContratofaturalocacaomaterial", "material", "contratofaturalocacaoorigem", "contratoorigem", "contrato");
		
		List<Contratofaturalocacaosituacao> listaSituacao = filtro.getListaSituacao();
		if(listaSituacao != null){
			query.whereIn("contratofaturalocacao.situacao", Contratofaturalocacaosituacao.listAndConcatenate(listaSituacao));
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Contratofaturalocacao> query) {
		query
			.leftOuterJoinFetch("contratofaturalocacao.cfop cfop")
			.leftOuterJoinFetch("contratofaturalocacao.cliente cliente")
			.leftOuterJoinFetch("contratofaturalocacao.empresa empresa")
			.leftOuterJoinFetch("empresa.cdreporttemplateboleto cdreporttemplateboleto")
			.leftOuterJoinFetch("contratofaturalocacao.listaContratofaturalocacaomaterial listaContratofaturalocacaomaterial")
			.leftOuterJoinFetch("listaContratofaturalocacaomaterial.material material")
			.leftOuterJoinFetch("listaContratofaturalocacaomaterial.patrimonioitem patrimonioitem")
			;
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaContratofaturalocacaomaterial");
	}

	public List<Contratofaturalocacao> findForFaturalocacao(String whereIn) {
		QueryBuilder<Contratofaturalocacao> query = query();
		return query
					.leftOuterJoinFetch("contratofaturalocacao.cfop cfop")
					.leftOuterJoinFetch("contratofaturalocacao.empresa empresa")
					.leftOuterJoinFetch("empresa.logomarca logomarca")
					.leftOuterJoinFetch("empresa.listaTelefone listaTelefoneEmpresa")
					.leftOuterJoinFetch("empresa.listaEndereco listaEnderecoEmpresa")
					.leftOuterJoinFetch("listaEnderecoEmpresa.municipio municipioEmpresa")
					.leftOuterJoinFetch("municipioEmpresa.uf ufEmpresa")
					.leftOuterJoinFetch("contratofaturalocacao.contrato contrato")
					.leftOuterJoin("contratofaturalocacao.cliente cliente")
//					.leftOuterJoinFetch("cliente.listaTelefone listaTelefoneCliente")
//					.leftOuterJoinFetch("cliente.listaEndereco listaEnderecoCliente")
//					.leftOuterJoinFetch("listaEnderecoCliente.enderecotipo enderecotipoCliente")
//					.leftOuterJoinFetch("listaEnderecoCliente.municipio municipioCliente")
//					.leftOuterJoinFetch("municipioCliente.uf ufCliente")
					.leftOuterJoinFetch("contratofaturalocacao.endereco endereco")
					.leftOuterJoinFetch("endereco.municipio municipio")
					.leftOuterJoinFetch("municipio.uf uf")
					.leftOuterJoinFetch("contratofaturalocacao.enderecofatura enderecofatura")
					.leftOuterJoinFetch("enderecofatura.municipio municipiofatura")
					.leftOuterJoinFetch("municipiofatura.uf uffatura")
					.whereIn("contratofaturalocacao.cdcontratofaturalocacao", whereIn)
					.orderBy("contratofaturalocacao.cdcontratofaturalocacao")
					.list();
	}

	/**
	 * Atualiza a situa��o das faturas passadas por par�metro.
	 *
	 * @param whereIn
	 * @param situacao
	 * @author Rodrigo Freitas
	 * @since 05/08/2013
	 */
	public void updateSituacao(String whereIn, Contratofaturalocacaosituacao situacao) {
		getHibernateTemplate().bulkUpdate("update Contratofaturalocacao c set c.situacao = ? where c.id in (" + whereIn + ")", situacao);
	}

	/**
     * M�todo que calcula o total de fatura da listagem
	 * @param filtro
	 * @return
     * @author Luiz Fernando
	 */
	public Money findForCalculoTotalGeral(ContratofaturalocacaoFiltro filtro) {
		Money totalfaturalocacao = new Money(0.);
		
		if(filtro != null){
			QueryBuilder<Contratofaturalocacao> query = query();
			this.updateListagemQuery(query, filtro);
			query.select("distinct contratofaturalocacao.cdcontratofaturalocacao, contratofaturalocacao.valortotal")
				.ignoreAllJoinsPath(true)
				.orderBy("contratofaturalocacao.valortotal desc");
			List<Contratofaturalocacao> lista = query.list();
			
			if(lista != null && !lista.isEmpty()){
				for(Contratofaturalocacao cfl : lista){
					if(cfl.getValortotal() != null){						
						totalfaturalocacao = totalfaturalocacao.add(cfl.getValortotal());
					}
				}
			}
		}	
		
		return totalfaturalocacao;
	}

	/**
	 * Busca a listagem para o CSV e PDF.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/09/2013
	 */
	public List<Contratofaturalocacao> findForListagemCsvPdf(ContratofaturalocacaoFiltro filtro) {
		QueryBuilder<Contratofaturalocacao> query = query();
		this.updateListagemQuery(query, filtro);
		query.setIgnoreJoinPaths(new HashSet<String>());
		
		return query
				.select("contratofaturalocacao.cdcontratofaturalocacao, contratofaturalocacao.numero, contratofaturalocacao.valortotal, contratofaturalocacao.descricao, " +
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, cliente.cdpessoa, cliente.nome, contratofaturalocacao.situacao, " +
						"listaContratofaturalocacaomaterial.valor, material.nome, patrimonioitem.plaqueta, contratofaturalocacao.dtemissao, contratofaturalocacao.dtvencimento, " +
						"contrato.cdcontrato, contrato.identificador, documento.cddocumento, documento.dtvencimento, rateio.cdrateio, " +
						"listaContratofaturalocacaohistorico.observacao, listaContratofaturalocacaohistorico.dtaltera")
				.leftOuterJoinIfNotExists("contratofaturalocacao.listaContratofaturalocacaomaterial listaContratofaturalocacaomaterial")
				.leftOuterJoinIfNotExists("listaContratofaturalocacaomaterial.material material")
				.leftOuterJoinIfNotExists("listaContratofaturalocacaomaterial.patrimonioitem patrimonioitem")
				.leftOuterJoinIfNotExists("contratofaturalocacao.listaContratofaturalocacaodocumento listaContratofaturalocacaodocumento")
				.leftOuterJoinIfNotExists("listaContratofaturalocacaodocumento.documento documento")
				.leftOuterJoinIfNotExists("documento.rateio rateio")
				.leftOuterJoinIfNotExists("contratofaturalocacao.listaContratofaturalocacaohistorico listaContratofaturalocacaohistorico")
				.leftOuterJoinIfNotExists("contratofaturalocacao.listaContratofaturalocacaoorigem contratofaturalocacaoorigem")
				.leftOuterJoinIfNotExists("contratofaturalocacao.contrato contrato")
				.leftOuterJoinIfNotExists("contratofaturalocacaoorigem.contrato contratoorigem")
				.orderBy("contratofaturalocacao.numero")
				.list();
	}

	/**
	 * Busca os dados para a gera��o de receita.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/09/2013
	 */
	public List<Contratofaturalocacao> findForGerarReceita(String whereIn) {
		QueryBuilder<Contratofaturalocacao> query = query();
		this.updateEntradaQuery(query);
		return query
				.leftOuterJoin("contratofaturalocacao.empresa empresa")
				.leftOuterJoinFetch("contratofaturalocacao.contrato contrato")
				.leftOuterJoinFetch("contratofaturalocacao.endereco endereco")
				.leftOuterJoinFetch("contratofaturalocacao.enderecofatura enderecofatura")
				.whereIn("contratofaturalocacao.cdcontratofaturalocacao", whereIn)
				.list();
	}

	/**
	 * Busca as faturas de loca��o para valida��o
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/10/2013
	 */
	public List<Contratofaturalocacao> findForValidacao(String whereIn) {
		return query()
					.select("contratofaturalocacao.cdcontratofaturalocacao, cliente.cdpessoa, contratofaturalocacaomaterial.cdcontratofaturalocacaomaterial, " +
							"contratofaturalocacao.numero, contratofaturalocacao.situacao, empresa.cdpessoa")
					.leftOuterJoin("contratofaturalocacao.empresa empresa")
					.leftOuterJoin("contratofaturalocacao.cliente cliente")
					.leftOuterJoin("contratofaturalocacao.listaContratofaturalocacaomaterial contratofaturalocacaomaterial")
					.whereIn("contratofaturalocacao.cdcontratofaturalocacao", whereIn)
					.list();
	}
	
	/**
	 * 
	 * @param contrato
	 * @author Thiago Clemente
	 * 
	 */
	public Date getDtultimaFaturalocacao(Contrato contrato){
		return newQueryBuilderWithFrom(Date.class)
				.select("max(coalesce(contratofaturalocacao.dtvencimento, contratofaturalocacao.dtemissao))")
				.leftOuterJoin("contratofaturalocacao.contrato contrato")
				.leftOuterJoin("contratofaturalocacao.listaContratofaturalocacaoorigem contratofaturalocacaoorigem")
				.leftOuterJoin("contratofaturalocacaoorigem.contrato contratoorigem")
				.where("contratofaturalocacao.situacao in (?, ?)", new Object[]{Contratofaturalocacaosituacao.EMITIDA, Contratofaturalocacaosituacao.FATURADA})
				.openParentheses()
					.where("contrato = ?", contrato)
					.or()
					.where("contratoorigem = ?", contrato)
				.closeParentheses()
				.setUseTranslator(false)
				.unique();
	}
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public boolean isFaturaAvulsaSemContrato(String whereIn){
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.leftOuterJoin("contratofaturalocacao.contrato contrato")
				.leftOuterJoin("contratofaturalocacao.listaContratofaturalocacaoorigem contratofaturalocacaoorigem")
				.leftOuterJoin("contratofaturalocacaoorigem.contrato contratoorigem")
				.where("contrato is null")
				.where("contratoorigem is null")
				.whereIn("contratofaturalocacao.cdcontratofaturalocacao", whereIn)
				.setUseTranslator(false)
				.unique().longValue() == 1;
	}
	
	public boolean isFaturaIndenizacaoWithContrato(String whereIn){
		Long qtde = newQueryBuilderWithFrom(Long.class)
				.select("count(distinct contratofaturalocacao.cdcontratofaturalocacao)")
				.leftOuterJoin("contratofaturalocacao.contrato contrato")
				.leftOuterJoin("contratofaturalocacao.listaContratofaturalocacaoorigem contratofaturalocacaoorigem")
				.leftOuterJoin("contratofaturalocacaoorigem.contrato contratoorigem")
				.where("contrato is null")
				.where("contratoorigem is not null")
				.where("contratofaturalocacao.indenizacao = true ")
				.whereIn("contratofaturalocacao.cdcontratofaturalocacao", whereIn)
				.groupBy("contratofaturalocacao.cdcontratofaturalocacao")
				.setUseTranslator(false)
				.unique();

		return qtde != null && qtde == 1;
	}
	
	public boolean existFaturaDiferenteEmitida(String whereIn){
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.where("contratofaturalocacao.situacao <> ?", Contratofaturalocacaosituacao.EMITIDA) 
				.whereIn("contratofaturalocacao.cdcontratofaturalocacao", whereIn)
				.setUseTranslator(false)
				.unique().longValue() > 0;
	}


	/**
	 * Busca as faturas de loca��o para o relat�rio evolu��o do faturamento 
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 17/05/2014
	 */
	public List<Contratofaturalocacao> findForEvolucao(EvolucaoFaturamentoFiltro filtro) {
		if(filtro == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("contratofaturalocacao.cdcontratofaturalocacao, contratofaturalocacao.numero, " +
					"contratofaturalocacao.valortotal, contratofaturalocacao.dtemissao ")
			.where("contratofaturalocacao.situacao <> ?", Contratofaturalocacaosituacao.CANCELADA)
			.where("contratofaturalocacao.dtemissao >= ?", filtro.getDtinicio())
			.where("contratofaturalocacao.dtemissao <= ?", filtro.getDtfim())
			.where("contratofaturalocacao.empresa = ?", filtro.getEmpresa())
			.list();
	}

	/**
	* M�todo que busca as faturas de loca��o sem origem
	*
	* @param cdcontratofaturalocacao
	* @return
	* @since 09/07/2014
	* @author Luiz Fernando
	*/
	public List<Contratofaturalocacao> findForSemOrigem(Integer cdcontratofaturalocacao) {
		return query()
			.select("contratofaturalocacao.cdcontratofaturalocacao, listaContratofaturalocacaohistorico.observacao ")
			.join("contratofaturalocacao.listaContratofaturalocacaohistorico listaContratofaturalocacaohistorico")
			.leftOuterJoin("contratofaturalocacao.contrato contrato")
			.leftOuterJoin("contratofaturalocacao.listaContratofaturalocacaoorigem listaContratofaturalocacaoorigem")
			.where("contratofaturalocacao.cdcontratofaturalocacao = ?", cdcontratofaturalocacao)
			.where("listaContratofaturalocacaoorigem is null")
			.where("listaContratofaturalocacaohistorico.observacao is not null")
			.orderBy("contratofaturalocacao.cdcontratofaturalocacao, listaContratofaturalocacaohistorico.cdcontratofaturalocacaohistorico")
			.list();
	}

	/**
	 * M�todo que verifica a exist�ncia do n�mero cadastrado para a empresa
	 *
	 * @param empresa
	 * @param numero
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/08/2014
	 */
	public boolean existNumeroEmpresa(Empresa empresa, Integer numero) {
		return newQueryBuilderWithFrom(Long.class)
					.select("count(*)")
					.where("contratofaturalocacao.empresa = ?", empresa)
					.where("contratofaturalocacao.numero = ?", numero)
					.where("coalesce(contratofaturalocacao.ignorarnumeracao, false) = false")
					.unique() > 0;
	}

	/**
	* M�todo que altera o campo ignorarnumeracao para TRUE, as faturas com o n�mero maior ou igual ao n�mero passado por par�metro
	* de acordo com a empresa 
	*
	* @param empresa
	* @param numfaturalocacaoReiniciar
	* @since 10/09/2014
	* @author Luiz Fernando
	*/
	public void ignorarNumeracaoByEmpresa(Empresa empresa, Integer numfaturalocacaoReiniciar) {
		if(empresa != null && empresa.getCdpessoa() != null && numfaturalocacaoReiniciar != null){
			String sql = "update contratofaturalocacao set ignorarnumeracao = true where cdempresa = " + empresa.getCdpessoa() +
				" and numero >= " + numfaturalocacaoReiniciar;
			getJdbcTemplate().update(sql);
		}
		
	}
	
	public List<Contratofaturalocacao> findForEnviarFaturaLocacao(String whereIn) {
		return query()
				.select("contratofaturalocacao.cdcontratofaturalocacao, contratofaturalocacao.descricao, contratofaturalocacao.numero," +
						"empresa.nome, cliente.cdpessoa, cliente.nome, cliente.email, empresa.formaEnvioBoleto, cdreporttemplateboleto.cdreporttemplate, " +
						"contato.nome, contato.emailcontato, contato.recebernf, contato.receberboleto," +
						"documento.cddocumento, documento.numero, conta.cdconta, documentotipo.boleto, documentoacao.cddocumentoacao")
				.join("contratofaturalocacao.empresa empresa")
				.join("contratofaturalocacao.cliente cliente")
				.leftOuterJoin("cliente.listaContato listaContato")
				.leftOuterJoin("listaContato.contato contato")
				.leftOuterJoin("contratofaturalocacao.listaContratofaturalocacaodocumento listaContratofaturalocacaodocumento")
				.leftOuterJoin("listaContratofaturalocacaodocumento.documento documento")
				.leftOuterJoin("documento.conta conta")
				.leftOuterJoin("documento.documentotipo documentotipo")
				.leftOuterJoin("documento.documentoacao documentoacao")
				.leftOuterJoin("empresa.cdreporttemplateboleto cdreporttemplateboleto")
				.whereIn("contratofaturalocacao.cdcontratofaturalocacao", whereIn, false)
				.list();
	}
	
	public List<Contratofaturalocacao> findByEmitirindenizacaofiltro(EmitirIndenizacaoContratoFiltro filtro){
		return query()
			.select("contrato.cdcontrato, contrato.descricao, contrato.identificador, " +
					"cliente.nome, cliente.razaosocial")
			.join("contratofaturalocacao.contrato contrato")
			.join("contrato.cliente cliente")
			.where("contrato = ?", filtro.getContrato())
			.where("cliente = ?", filtro.getCliente())
			.where("empresa = ?", filtro.getEmpresa())
			.where("contrato.dtinicio >= ?", filtro.getDtinicio())
			.where("contrato.dtinicio <= ?", filtro.getDtfim())
			.where("contratofaturalocacao.indenizacao = ?", Boolean.TRUE)
			.orderBy("contrato.cdcontrato")
			.list();
	}
}