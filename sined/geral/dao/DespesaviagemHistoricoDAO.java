package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.DespesaviagemHistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DespesaviagemHistoricoDAO extends GenericDAO<DespesaviagemHistorico>{

	/**
	 * Busca o histórico para a tela de entrada do despesa viagem
	 * @param form
	 * @return
	 */
	public List<DespesaviagemHistorico> findForEntradaDespesaViagem(Despesaviagem form) {
		return query()
				.select("despesaviagemHistorico.cddespesaviagemhistorico, despesaviagemHistorico.observacao, despesaviagemHistorico.dtaltera, " +
						"despesaviagemHistorico.acao, usuario.cdpessoa, usuario.nome")
				.leftOuterJoin("despesaviagemHistorico.usuario usuario")
				.where("despesaviagemHistorico.despesaviagem = ?", form)
				.orderBy("despesaviagemHistorico.dtaltera desc")
				.list();
	}

}
