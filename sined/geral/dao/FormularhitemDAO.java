package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Formularh;
import br.com.linkcom.sined.geral.bean.Formularhitem;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FormularhitemDAO extends GenericDAO<Formularhitem>  {
	
	public List<Formularhitem> findByHolerite(Formularh formularh){
		QueryBuilder<Formularhitem> query = query()
			.where("formularhitem.formularh = ?",formularh)
			.orderBy("formularhitem.ordem");
		return query.list();
	}
	
	public Formularhitem loadForGerarArquivoSEFIP(Formularh formularh, String identificador){
		return query()
				.where("formularhitem.formularh=?", formularh)
				.where("upper(formularhitem.identificador)=?", identificador)
				.setMaxResults(1)
				.unique();
	}	
}
