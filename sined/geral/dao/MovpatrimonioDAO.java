package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialnumeroserie;
import br.com.linkcom.sined.geral.bean.Movpatrimonio;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MovpatrimonioFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

public class MovpatrimonioDAO extends GenericDAO<Movpatrimonio> {
	
	private MaterialcategoriaService materialcategoriaService;
	
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {this.materialcategoriaService = materialcategoriaService;}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Movpatrimonio> query) {
		((QueryBuilderSined<Movpatrimonio>) query).setUseWhereClienteEmpresa(true);
		((QueryBuilderSined<Movpatrimonio>) query).setUseWhereFornecedorEmpresa(true);
		
		query
			.select("movpatrimonio.cdmovpatrimonio, movpatrimonio.dtmovimentacao, patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, bempatrimonio.cdmaterial, bempatrimonio.nome, bempatrimonio.identificacao, requisicaomaterial.cdrequisicaomaterial, " +
					"entrega.cdentrega, usuario.cdpessoa, usuario.nome, localarmazenagem.cdlocalarmazenagem, departamento.cddepartamento, departamento.nome, empresa.cdpessoa, fornecedor.cdpessoa, fornecedor.nome, fornecedororigem.cdpessoa, fornecedororigem.nome, movpatrimonioorigem.cdmovpatrimonioorigem, " +
					"movpatrimonio.cdusuarioaltera, movpatrimonio.dtaltera, movpatrimonio.valormovimentacao, colaborador.cdpessoa, colaborador.nome, " +
					"projeto.cdprojeto, projeto.nome, cliente.cdpessoa, cliente.nome, materialnumeroserie.numero, romaneio.cdromaneio, romaneio.descricao ")
			.join("movpatrimonio.patrimonioitem patrimonioitem")
			.join("patrimonioitem.bempatrimonio bempatrimonio")
			.leftOuterJoin("patrimonioitem.materialnumeroserie materialnumeroserie")
			.leftOuterJoin("movpatrimonio.movpatrimonioorigem movpatrimonioorigem")
			.leftOuterJoin("movpatrimonioorigem.romaneio romaneio")
			.leftOuterJoin("movpatrimonioorigem.requisicaomaterial requisicaomaterial")
			.leftOuterJoin("movpatrimonioorigem.entrega entrega")
			.leftOuterJoin("movpatrimonioorigem.usuario usuario")
			.leftOuterJoin("movpatrimonio.localarmazenagem localarmazenagem")
			.leftOuterJoin("movpatrimonio.departamento departamento")
			.leftOuterJoin("movpatrimonio.colaborador colaborador")
			.leftOuterJoin("movpatrimonio.empresa empresa")
			.leftOuterJoin("movpatrimonio.fornecedor fornecedor")
			.leftOuterJoin("movpatrimonio.fornecedororigem fornecedororigem")
			.leftOuterJoin("movpatrimonio.projeto projeto")
			.leftOuterJoin("movpatrimonio.cliente cliente");
		
		/*query.leftOuterJoinFetch("movpatrimonio.patrimonioitem patrimonioitem")
			.leftOuterJoinFetch("patrimonioitem.bempatrimonio bempatrimonio")
			.leftOuterJoinFetch("movpatrimonio.movpatrimonioorigem movpatrimonioorigem")
			.leftOuterJoinFetch("movpatrimonioorigem.requisicaomaterial")
			.leftOuterJoinFetch("movpatrimonioorigem.entrega")
			.leftOuterJoinFetch("movpatrimonioorigem.usuario");*/
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Movpatrimonio> query, FiltroListagem _filtro) {
		MovpatrimonioFiltro filtro = (MovpatrimonioFiltro) _filtro;
		
		((QueryBuilderSined<Movpatrimonio>) query).setUseWhereClienteEmpresa(true);
		((QueryBuilderSined<Movpatrimonio>) query).setUseWhereFornecedorEmpresa(true);
		
		query
			.select("movpatrimonio.cdmovpatrimonio, movpatrimonio.dtcancelamento, patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, bempatrimonio.nome, vfornecedor.nome, " +
					"vlocalarmazenagem.nome, fornecedororigem.nome, fornecedor.nome, vfornecedor.nome, localarmazenagem.nome, " +
					"movpatrimonio.dtmovimentacao, materialgrupo.nome, materialtipo.nome, departamento.nome, vdepartamento.nome, " +
					"colaborador.nome, vcolaborador.nome, empresa.nome, empresa.razaosocial, empresa.nomefantasia, vempresa.nome, fornecedorcontrato.nome," +
					"bempatrimonio.cdmaterial, materialnumeroserie.numero")
			.leftOuterJoin("movpatrimonio.patrimonioitem patrimonioitem")
			.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
			.leftOuterJoin("bempatrimonio.materialgrupo materialgrupo")
			.leftOuterJoin("bempatrimonio.materialtipo materialtipo")
			.join("movpatrimonio.vmovpatrimonio vmovpatrimonio")
			.leftOuterJoin("vmovpatrimonio.fornecedor vfornecedor")
			.leftOuterJoin("vmovpatrimonio.localarmazenagem vlocalarmazenagem")
			.leftOuterJoin("vmovpatrimonio.departamento vdepartamento")
			.leftOuterJoin("vmovpatrimonio.empresa vempresa")
			.leftOuterJoin("vmovpatrimonio.colaborador vcolaborador")
			.leftOuterJoin("movpatrimonio.fornecedororigem fornecedororigem")
			.leftOuterJoin("movpatrimonio.fornecedor fornecedor")
			.leftOuterJoin("movpatrimonio.localarmazenagem localarmazenagem")
			.leftOuterJoin("movpatrimonio.departamento departamento")
			.leftOuterJoin("movpatrimonio.colaborador colaborador")
			.leftOuterJoin("movpatrimonio.empresa empresa")
			.leftOuterJoin("movpatrimonio.cliente cliente")
			.leftOuterJoin("patrimonioitem.fornecimentocontrato fornecimentocontrato")
			.leftOuterJoin("fornecimentocontrato.fornecedor fornecedorcontrato")
			.leftOuterJoin("patrimonioitem.materialnumeroserie materialnumeroserie")
//			.whereLikeIgnoreAll("patrimonioitem.plaqueta", filtro.getPlaqueta())
			.whereLikeIgnoreAll("coalesce(vfornecedor.nome, vlocalarmazenagem.nome, fornecedororigem.nome)", filtro.getOrigem())
			.whereLikeIgnoreAll("localarmazenagem.nome", filtro.getDestino())
			.where("movpatrimonio.dtmovimentacao >= ?",filtro.getData1())
			.where("movpatrimonio.dtmovimentacao <= ?",filtro.getData2())
			.where("materialtipo = ?",filtro.getMaterialtipo())
			.where("materialgrupo = ?",filtro.getMaterialgrupo())
			.where("cliente = ?", filtro.getCliente())
			.where("patrimonioitem = ?", filtro.getPatrimonioitem());
			if(filtro.getPatrimonioitem() != null && filtro.getPatrimonioitem().getBempatrimonio() != null)
				query.whereLikeIgnoreAll("bempatrimonio.nome", filtro.getPatrimonioitem().getBempatrimonio().getNome());
		
		if (filtro.getUltimos() != null && filtro.getUltimos()) {
			query.where("movpatrimonio.cdmovpatrimonio = (select max(m2.cdmovpatrimonio) from Movpatrimonio m2 where m2.patrimonioitem = movpatrimonio.patrimonioitem and m2.dtcancelamento is null group by movpatrimonio.patrimonioitem)");
		}
		
		if(filtro.getCanceladas() == null || !filtro.getCanceladas()){
			query.where("movpatrimonio.dtcancelamento is null");
		}
		
		if(filtro.getMaterialcategoria() != null){
			materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
			query
				.leftOuterJoin("bempatrimonio.materialcategoria materialcategoria")
				.leftOuterJoin("materialcategoria.vmaterialcategoria vmaterialcategoria")
				.openParentheses()
					.where("vmaterialcategoria.identificador like ? ||'.%'", filtro.getMaterialcategoria().getIdentificador())
				.or()
					.where("vmaterialcategoria.identificador like ? ", filtro.getMaterialcategoria().getIdentificador())
				.closeParentheses();
		}
		
//		if ("TRUE".equalsIgnoreCase(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.RECAPAGEM))){
//			query.leftOuterJoin("bempatrimonio.listaMaterialnumeroserie listaMaterialnumeroserie");
//			query.ignoreJoin("listaMaterialnumeroserie");
//			query.whereLikeIgnoreAll("patrimonioitem.plaqueta", filtro.getPlaqueta());
//			query.or();			
//			query.whereLikeIgnoreAll("listaMaterialnumeroserie.numero", filtro.getPlaqueta());
//		}else {
//			query.whereLikeIgnoreAll("patrimonioitem.plaqueta", filtro.getPlaqueta());
//		}
		
		query.openParentheses();
		query.whereLikeIgnoreAll("patrimonioitem.plaqueta", filtro.getPlaqueta());
		query.or();			
		query.whereLikeIgnoreAll("materialnumeroserie.numero", filtro.getPlaqueta());
		query.closeParentheses();
		
		query.orderBy("movpatrimonio.cdmovpatrimonio desc");
	}
	
	/**
	 * Carrega a �ltima movimentacao do item de patrim�nio.
	 * 
	 * @param patrimonioitem
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Movpatrimonio findLastMov(Movpatrimonio movpatrimonio) {
		if (movpatrimonio == null || movpatrimonio.getPatrimonioitem() == null || movpatrimonio.getPatrimonioitem().getCdpatrimonioitem() == null) {
			throw new SinedException("Patrimonioitem n�o pode ser nulo.");
		}
		return query()
					.select("fornecedor.cdpessoa, fornecedor.nome, localarmazenagem.cdlocalarmazenagem, departamento.cddepartamento, departamento.nome, " +
							"empresa.cdpessoa, movpatrimonio.dtmovimentacao, movpatrimonio.valormovimentacao, bempatrimonio.cdmaterial, bempatrimonio.nome, " +
							"colaborador.cdpessoa, colaborador.nome, projeto.cdprojeto, projeto.nome, " +
							"cliente.cdpessoa, cliente.nome, localarmazenagem.nome ")
					.leftOuterJoin("movpatrimonio.colaborador colaborador")
					.leftOuterJoin("movpatrimonio.fornecedor fornecedor")
					.leftOuterJoin("movpatrimonio.localarmazenagem localarmazenagem")
					.leftOuterJoin("localarmazenagem.endereco endereco")
					.leftOuterJoin("movpatrimonio.empresa empresa")
					.leftOuterJoin("movpatrimonio.departamento departamento")
					.leftOuterJoin("movpatrimonio.patrimonioitem patrimonioitem")
					.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
					.leftOuterJoin("movpatrimonio.projeto projeto")
					.leftOuterJoin("movpatrimonio.cliente cliente")
					.setMaxResults(1)
					.where("movpatrimonio.dtcancelamento is null")
					.where("movpatrimonio.patrimonioitem = ?",movpatrimonio.getPatrimonioitem())
					.where("movpatrimonio.cdmovpatrimonio <> ?",movpatrimonio.getCdmovpatrimonio())
					.where("movpatrimonio.cdmovpatrimonio <= ?",movpatrimonio.getCdmovpatrimonio())
					.orderBy("movpatrimonio.cdmovpatrimonio DESC")
					.unique();
	}
	
	
	/**
	 * Verifica se a movimenta��o passada � a �ltima do item de patrim�nio.
	 * 
	 * @param movpatrimonio
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Boolean isLastMov(Movpatrimonio movpatrimonio){
		if (movpatrimonio == null || movpatrimonio.getCdmovpatrimonio() == null) {
			throw new SinedException("Movimenta��o de patrim�nio n�o pode ser nulo.");
		}
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Movpatrimonio.class)
					.where("movpatrimonio = ?",movpatrimonio)
					.where("movpatrimonio.cdmovpatrimonio = (select max(m2.cdmovpatrimonio) from Movpatrimonio m2 where m2.patrimonioitem = movpatrimonio.patrimonioitem and m2.dtcancelamento is null group by movpatrimonio.patrimonioitem)")
					.unique() > 0;
	}

	/**
	 * Faz o update na data cancelada com a data atual.
	 * 
	 * @param movpatrimonio
	 * @author Rodrigo Freitas
	 */
	public void updateCancelada(Movpatrimonio movpatrimonio) {
		if (movpatrimonio == null || movpatrimonio.getCdmovpatrimonio() == null) {
			throw new SinedException("Movimenta��o de patrim�nio n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Movpatrimonio m set dtcancelamento = ? where m.id = ?", 
				new Object[]{new Date(System.currentTimeMillis()),movpatrimonio.getCdmovpatrimonio()});
	}
	
	public void updateCancelada(String whereIn) {
		getHibernateTemplate().bulkUpdate("update Movpatrimonio m set dtcancelamento = ? where m.cdmovpatrimonio in (" + whereIn + ")", new Object[]{
			new Date(System.currentTimeMillis())
		});
	}

	/**
	 * M�todo que retorna quantas movimenta��es de patrimonio ja ocorreram para aquela
	 * requisi��o de material
	 * 
	 * @param requisicaomaterial
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Long getQuantidadeMovimentadaDaRequisicaoMaterial(Requisicaomaterial requisicaomaterial) {
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(beanClass)
			.join("movpatrimonio.movpatrimonioorigem movpatrimonioorigem")
			.join("movpatrimonioorigem.requisicaomaterial requisicaomaterial")
			.where("requisicaomaterial = ?", requisicaomaterial)
			.where("movpatrimonio.dtcancelamento is null")
			.unique();
	}

	/**
	 * M�todo que busca todas as movimenta��es que a entrega gerou
	 * 
	 * @param entrega
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Movpatrimonio> getListaMovPatrimonioDaEntrega(Entrega entrega) {
		return query()
			.select("movpatrimonio.cdmovpatrimonio")
			.join("movpatrimonio.movpatrimonioorigem movpatrimonioorigem")
			.join("movpatrimonioorigem.entrega entrega")
			.where("entrega = ?", entrega)
			.where("movpatrimonio.dtcancelamento is null")
			.list();
	}
	
	/**
	 * M�todo que verifica se existe moviemnta��o de patrim�nio relacionado a entrega
	 *
	 * @param entrega
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existMovPatrimonioDaEntrega(Entrega entrega) {
		return newQueryBuilderSined(Long.class)
			.from(Movpatrimonio.class)
			.select("count(*)")
			.join("movpatrimonio.movpatrimonioorigem movpatrimonioorigem")
			.join("movpatrimonioorigem.entrega entrega")
			.where("entrega = ?", entrega)
			.where("movpatrimonio.dtcancelamento is null")
			.unique() > 0;
	}

	/**
	 * Verifica se a plaqueta digitada esta no local de armazenagem correto e pertence ao material em especifico
	 * 
	 * @param movpatrimonio
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Boolean isPlaquetaValida(Movpatrimonio movpatrimonio) {
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Movpatrimonio.class)
			.join("movpatrimonio.localarmazenagem localarmazenagem")
			.join("movpatrimonio.patrimonioitem patrimonioitem")
			.join("patrimonioitem.bempatrimonio bempatrimonio")
			.where("localarmazenagem = ?",movpatrimonio.getLocalarmazenagem())
			.where("bempatrimonio = ?",movpatrimonio.getPatrimonioitem().getBempatrimonio())
			.where("patrimonioitem.plaqueta = ?",movpatrimonio.getPlaqueta())
			.where("movpatrimonio.cdmovpatrimonio = (select max(m2.cdmovpatrimonio) from Movpatrimonio m2 where m2.patrimonioitem = movpatrimonio.patrimonioitem and m2.dtcancelamento is null group by movpatrimonio.patrimonioitem)")
			.unique() > 0;
	}

	/**
	 * M�todo que busca os dados da listagem para relat�rio
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Movpatrimonio> findForReport(MovpatrimonioFiltro filtro) {
		QueryBuilder<Movpatrimonio> query = querySined();
		updateListagemQuery(query, filtro);
		return query.list();
	}

	public Double qtdeDisponivelPatrimonio(Material material, Patrimonioitem patrimonioitem, Localarmazenagem localarmazenagemorigem, Empresa empresa, Projeto projeto) {
		QueryBuilderSined<Long> query = (QueryBuilderSined<Long>) newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Movpatrimonio.class)
				.leftOuterJoin("movpatrimonio.patrimonioitem patrimonioitem")
				.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
				.where("bempatrimonio.cdmaterial = ?", material != null ? material.getCdmaterial() : null)
				.where("movpatrimonio.patrimonioitem = ?", patrimonioitem)
				.where("movpatrimonio.localarmazenagem = ?", localarmazenagemorigem);
		
		if(projeto != null){
			query.openParentheses()
				.where("movpatrimonio.projeto = ?", projeto)
				.or()
				.where("movpatrimonio.projeto is null")
				.closeParentheses();
		}
		
		if(empresa != null){
			query.openParentheses()
			.where("movpatrimonio.empresa = ?", empresa)
			.or()
			.where("movpatrimonio.empresa is null")
			.closeParentheses();
		}
		
		return new Double(query
				.where("movpatrimonio.cdmovpatrimonio = (select max(m2.cdmovpatrimonio) from Movpatrimonio m2 where m2.patrimonioitem = movpatrimonio.patrimonioitem and m2.dtcancelamento is null group by movpatrimonio.patrimonioitem)")
				.unique());
	}

	public boolean existMovpatrimonioByNotas(String whereIn) {
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Movpatrimonio.class)
				.join("movpatrimonio.movpatrimonioorigem movpatrimonioorigem")
				.join("movpatrimonioorigem.notafiscalproduto notafiscalproduto")
				.whereIn("notafiscalproduto.cdNota", whereIn)
				.unique() > 0;
	}

	public List<Movpatrimonio> findByRomaneio(Romaneio romaneio) {
		return query()
					.join("movpatrimonio.movpatrimonioorigem movpatrimonioorigem")
					.where("movpatrimonioorigem.romaneio = ?", romaneio)
					.where("movpatrimonio.dtcancelamento is null")
					.list();
	}

	public List<Movpatrimonio> findForIndenizacaoContrato(String whereIn){
		return query()
			.select("movpatrimonio.dtmovimentacao, patrimonioitem.plaqueta, movpatrimonio.valormovimentacao, movpatrimonio.cdmovpatrimonio, contratofechamento.cdcontrato, contratofechamento.descricao, contratofechamento.identificador, " +
					"bempatrimonio.nome, bempatrimonio.nomereduzido, bempatrimonio.valorindenizacao, bempatrimonio.valorcusto, bempatrimonio.cdmaterial, bempatrimonio.identificacao, cliente.nome, cliente.razaosocial")
			.join("movpatrimonio.movpatrimonioorigem movpatrimonioorigem")
			.join("movpatrimonioorigem.romaneio romaneio")
			.join("romaneio.listaRomaneioorigem romaneioorigem")
			.join("romaneioorigem.contratofechamento contratofechamento")
			.join("contratofechamento.cliente cliente")
			.join("movpatrimonio.localarmazenagem localarmazenagem")
			.join("movpatrimonio.patrimonioitem patrimonioitem")
			.join("patrimonioitem.bempatrimonio bempatrimonio")
			.whereIn("contratofechamento.cdcontrato", whereIn)
			.where("localarmazenagem.indenizacao = ?", Boolean.TRUE)
			.where("movpatrimonio.dtcancelamento is null")
			.where("romaneio.indenizacao = ?", Boolean.TRUE)
			.list();
	}
	
	public Boolean isMovPatrimonioRomaneio(Movpatrimonio movpatrimonio) {
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Movpatrimonio.class)
				.join("movpatrimonio.movpatrimonioorigem movpatrimonioorigem")
				.join("movpatrimonioorigem.romaneio romaneio")
				.where("movpatrimonio = ?",movpatrimonio)
				.unique() > 0;
	}

	public List<Movpatrimonio> findBySerie(Materialnumeroserie materialNumeroSerie) {
		return query()
				.select("movpatrimonio.cdmovpatrimonio, movpatrimonio.dtmovimentacao")
				.join("movpatrimonio.patrimonioitem patrimonioitem")
				.join("patrimonioitem.materialnumeroserie materialnumeroserie")
				.where("materialnumeroserie = ?", materialNumeroSerie)
				.list();
	}
}
