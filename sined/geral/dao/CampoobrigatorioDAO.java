package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Campoobrigatorio;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CampoobrigatorioFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CampoobrigatorioDAO extends GenericDAO<Campoobrigatorio> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Campoobrigatorio> query, FiltroListagem _filtro) {
		CampoobrigatorioFiltro filtro = (CampoobrigatorioFiltro) _filtro;
		
		query
			.select("campoobrigatorio.cdcampoobrigatorio, campoobrigatorio.path")
			.whereLikeIgnoreAll("campoobrigatorio.path", filtro.getPath());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Campoobrigatorio> query) {
		query.leftOuterJoinFetch("campoobrigatorio.listaCampoobrigatorioitem listaCampoobrigatorioitem");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaCampoobrigatorioitem");
	}

	public Campoobrigatorio loadByPath(String path) {
		return querySined()
					
					.leftOuterJoinFetch("campoobrigatorio.listaCampoobrigatorioitem listaCampoobrigatorioitem")
					.where("campoobrigatorio.path = ?", path)
					.unique();
	}
	
}
