package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Customaoobraitem;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.CustomaoobraitemFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CustomaoobraitemDAO extends GenericDAO<Customaoobraitem> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Customaoobraitem> query, FiltroListagem _filtro) {
		CustomaoobraitemFiltro filtro = (CustomaoobraitemFiltro) _filtro;
		
		query
			.select("distinct customaoobraitem.cdcustomaoobraitem, customaoobraitem.identificador, customaoobraitem.descricao, " +
					"customaoobraitemgrupo.descricao, unidademedida.nome, customaoobraitem.custounitario")
			.leftOuterJoin("customaoobraitem.unidademedida unidademedida")
			.leftOuterJoin("customaoobraitem.customaoobraitemgrupo customaoobraitemgrupo")
			.leftOuterJoin("customaoobraitem.listaCustomaoobraitemcargo listaCustomaoobraitemcargo")
			.where("upper(customaoobraitem.identificador) = ?", filtro.getIdentificador() != null ? filtro.getIdentificador().toUpperCase() : null)
			.whereLikeIgnoreAll("customaoobraitem.descricao", filtro.getDescricao())
			.where("customaoobraitemgrupo = ?", filtro.getCustomaoobraitemgrupo())
			.where("listaCustomaoobraitemcargo.cargo = ?", filtro.getCargo())
			.where("customaoobraitem.orcamento = ?", filtro.getOrcamento())
			.ignoreJoin("listaCustomaoobraitemcargo");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Customaoobraitem> query) {
		query
			.select("customaoobraitem.cdcustomaoobraitem, customaoobraitem.identificador, customaoobraitem.descricao, " +
					"customaoobraitem.custounitario, customaoobraitem.quantidade, customaoobraitem.percentualvenda, " +
					"customaoobraitem.custovenda, customaoobraitem.todoscargos, " +
					"customaoobraitem.cdusuarioaltera, customaoobraitem.dtaltera, " +
					"customaoobraitemgrupo.cdcustomaoobraitemgrupo, customaoobraitemgrupo.descricao, " +
					"unidademedida.cdunidademedida, unidademedida.nome, " +
					"contagerencial.cdcontagerencial, contagerencial.nome, " +
					"listaCustomaoobraitemcargo.cdcustomaoobraitemcargo, " +
					"cargo.cdcargo, cargo.nome, " +
					"orcamento.cdorcamento, orcamento.nome ")
			.leftOuterJoin("customaoobraitem.orcamento orcamento")
			.leftOuterJoin("customaoobraitem.customaoobraitemgrupo customaoobraitemgrupo")
			.leftOuterJoin("customaoobraitem.unidademedida unidademedida")
			.leftOuterJoin("customaoobraitem.contagerencial contagerencial")
			.leftOuterJoin("customaoobraitem.listaCustomaoobraitemcargo listaCustomaoobraitemcargo")
			.leftOuterJoin("listaCustomaoobraitemcargo.cargo cargo");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaCustomaoobraitemcargo");
	}

	/**
	 * Busca os itens de custo de m�o de obra para a altera��o do percentual
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/01/2015
	 */
	public List<Customaoobraitem> findForAlteracaoPercentual(String whereIn) {
		if(whereIn == null || whereIn.trim().equals("")){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("customaoobraitem.cdcustomaoobraitem, customaoobraitem.custounitario, customaoobraitem.quantidade")
					.whereIn("customaoobraitem.cdcustomaoobraitem", whereIn)
					.list();
	}

	/**
	 * Atualiza o campo percentualvenda
	 *
	 * @param customaoobraitem
	 * @param percentualvenda
	 * @author Rodrigo Freitas
	 * @since 12/01/2015
	 */
	public void updatePercentualVenda(Customaoobraitem customaoobraitem, Double percentualvenda) {
		getHibernateTemplate().bulkUpdate("update Customaoobraitem c set c.percentualvenda = ?, c.cdusuarioaltera = ?, c.dtaltera = ? where c.id = ?", new Object[]{
			percentualvenda, 
			SinedUtil.getUsuarioLogado().getCdpessoa(), 
			SinedDateUtils.currentTimestamp(),
			customaoobraitem.getCdcustomaoobraitem()
		});
	}

	/**
	 * Atualiza o campo custovenda
	 *
	 * @param customaoobraitem
	 * @param custovenda
	 * @author Rodrigo Freitas
	 * @since 12/01/2015
	 */
	public void updateCustoVenda(Customaoobraitem customaoobraitem, Money custovenda) {
		getHibernateTemplate().bulkUpdate("update Customaoobraitem c set c.custovenda = ?, c.cdusuarioaltera = ?, c.dtaltera = ? where c.id = ?", new Object[]{
				custovenda, 
			SinedUtil.getUsuarioLogado().getCdpessoa(), 
			SinedDateUtils.currentTimestamp(),
			customaoobraitem.getCdcustomaoobraitem()
		});
	}

	/**
	 * Busca as informa��es para o c�lculo da composi��o de m�o de obra
	 *
	 * @param cargo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/01/2015
	 */
	public List<Customaoobraitem> findByCargo(Cargo cargo) {
		if(cargo == null || cargo.getCdcargo() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("customaoobraitem.cdcustomaoobraitem, customaoobraitem.identificador, customaoobraitem.custovenda")
					.leftOuterJoin("customaoobraitem.listaCustomaoobraitemcargo listaCustomaoobraitemcargo")
					.leftOuterJoin("listaCustomaoobraitemcargo.cargo cargo")
					.openParentheses()
						.where("customaoobraitem.todoscargos = true")
						.or()
						.where("cargo = ?", cargo)
					.closeParentheses()
					.list();
	}

	/**
	* M�todo que busca os custos de m�o de obra do or�amento
	*
	* @param orcamento
	* @return
	* @since 05/03/2015
	* @author Luiz Fernando
	*/
	public List<Customaoobraitem> findForImportarOrcamento(Orcamento orcamento) {
		if(orcamento == null || orcamento.getCdorcamento() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("customaoobraitem.cdcustomaoobraitem, customaoobraitem.custounitario, customaoobraitem.quantidade, " +
						"customaoobraitem.percentualvenda, customaoobraitem.custovenda, contagerencial.cdcontagerencial ")
				.join("customaoobraitem.contagerencial contagerencial")
				.where("customaoobraitem.orcamento = ?", orcamento)
				.list();
	}

	/**
	* M�todo que busca os itens de custo de m�o de obra para copia de acordo com o or�amento
	*
	* @param orcamento
	* @return
	* @since 22/04/2015
	* @author Luiz Fernando
	*/
	public List<Customaoobraitem> buscarCustomaoobraitemParaCopia(Orcamento orcamento) {
		if(orcamento == null || orcamento.getCdorcamento() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Customaoobraitem> query = query();
		updateEntradaQuery(query);
		
		return query
				.where("orcamento = ?", orcamento)
				.list();
	}

	/**
	* M�todo que calcula o total de custo por item de custo de mao de obra
	*
	* @param orcamento
	* @return
	* @since 06/07/2015
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public Double getTotalcustovenda(Orcamento orcamento) {
		String sql = 
			"SELECT SUM(COALESCE(CMOI.CUSTOVENDA,0)/100) AS TOTAL " +
			"FROM CUSTOMAOOBRAITEM CMOI " +
			"WHERE CMOI.CDORCAMENTO = " + orcamento.getCdorcamento();

		SinedUtil.markAsReader();
		List<Double> listaUpdate = getJdbcTemplate().query(sql, 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getDouble("TOTAL");
			}
		});
		
		return listaUpdate != null && listaUpdate.size() > 0 ? listaUpdate.get(0) : 0.0;
	}
	
}
