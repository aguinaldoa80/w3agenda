package br.com.linkcom.sined.geral.dao;


import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialkitflexivel;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialkitflexivelDAO extends GenericDAO<Materialkitflexivel> {

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaMaterialkitflexivelformula");
	}
	
	public void deleteWhereNotInMaterial(Material material, String whereIn) {
		if(whereIn != null && !whereIn.equals("")){
			getHibernateTemplate().bulkUpdate("delete from Materialkitflexivel mkf where mkf.material = ? and mkf.cdmaterialkitflexivel not in (" + whereIn + ")", new Object[]{material});
		} else {
			getHibernateTemplate().bulkUpdate("delete from Materialkitflexivel mkf where mkf.material = ?", new Object[]{material});
		}
	}
}
