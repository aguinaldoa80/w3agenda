package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.LancamentosOperacaoCartao;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.LancamentosOperacaoCartaoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class LancamentosOperacaoCartaoDAO extends GenericDAO<LancamentosOperacaoCartao>{
	@Override
	public void updateListagemQuery(QueryBuilder<LancamentosOperacaoCartao> query, FiltroListagem _filtro) {
		LancamentosOperacaoCartaoFiltro filtro = (LancamentosOperacaoCartaoFiltro) _filtro;

		Date dtFim = null;
		Date dtInicio = null;
		if(filtro.getDtLancamentoFim()!= null){
			dtFim = SinedDateUtils.lastDateOfMonth(filtro.getDtLancamentoFim());
		}
		if(filtro.getDtLancamentoInicio()!= null){
			dtInicio = SinedDateUtils.firstDateOfMonth(filtro.getDtLancamentoInicio());
		}
		
		query
		.select("lancamentosOperacaoCartao.cdLancamentosOperacaoCartao, lancamentosOperacaoCartao.mesAno, " +
				"lancamentosOperacaoCartao.valorCredito, lancamentosOperacaoCartao.valorDebito, " +
				"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
				"credenciadora.cdpessoa, credenciadora.nome")
		.leftOuterJoin("lancamentosOperacaoCartao.empresa empresa")
		.leftOuterJoin("lancamentosOperacaoCartao.credenciadora credenciadora")
		.where("lancamentosOperacaoCartao.mesAno >= ?", dtInicio)
		.where("lancamentosOperacaoCartao.mesAno <= ?", dtFim)
		.where("lancamentosOperacaoCartao.valorCredito >=? ", filtro.getValorCreditoDe())
		.where("lancamentosOperacaoCartao.valorCredito <= ?", filtro.getValorCreditoA())
		.where("lancamentosOperacaoCartao.valorDebito >= ?", filtro.getValorDebitoDe())
		.where("lancamentosOperacaoCartao.valorDebito <= ?", filtro.getValorDebitoA())
		.where("empresa = ?", filtro.getEmpresa())
		.where("credenciadora = ?", filtro.getCredenciadora());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<LancamentosOperacaoCartao> query) {
		query
		.select("lancamentosOperacaoCartao.cdLancamentosOperacaoCartao, lancamentosOperacaoCartao.mesAno, " +
				"lancamentosOperacaoCartao.valorCredito, lancamentosOperacaoCartao.valorDebito, " +
				"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
				"credenciadora.cdpessoa, credenciadora.nome")
		.leftOuterJoin("lancamentosOperacaoCartao.empresa empresa")
		.leftOuterJoin("lancamentosOperacaoCartao.credenciadora credenciadora");
	}

	public List<LancamentosOperacaoCartao> findForSpedFiscal1600(SpedarquivoFiltro filtro) {		
		return querySined()
				.setUseReadOnly(true)
				.select("lancamentosOperacaoCartao.cdLancamentosOperacaoCartao, lancamentosOperacaoCartao.valorCredito," +
						"lancamentosOperacaoCartao.valorDebito, credenciadora.cdpessoa")
				.leftOuterJoin("lancamentosOperacaoCartao.empresa empresa")
				.leftOuterJoin("lancamentosOperacaoCartao.credenciadora credenciadora")
				.where("empresa = ?", filtro.getEmpresa())
				.where("lancamentosOperacaoCartao.mesAno >= ?", filtro.getDtinicio())
				.list();
	}

	public boolean existeOperacaoPeriodoCredenciadora(LancamentosOperacaoCartao bean) {
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(LancamentosOperacaoCartao.class)
				.leftOuterJoin("lancamentosOperacaoCartao.empresa empresa")
				.join("lancamentosOperacaoCartao.credenciadora credenciadora")
				.where("credenciadora = ?", bean.getCredenciadora())
				.where("empresa = ?", bean.getEmpresa())
				.where("lancamentosOperacaoCartao.mesAno = ?", bean.getMesAno())
				.where("lancamentosOperacaoCartao.cdLancamentosOperacaoCartao <> ?", bean.getCdLancamentosOperacaoCartao())
				.unique() > 0;
	}

	public boolean existeRegistroPeríodo(SpedarquivoFiltro filtro) {
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(LancamentosOperacaoCartao.class)
				.leftOuterJoin("lancamentosOperacaoCartao.empresa empresa")
				.where("empresa = ?", filtro.getEmpresa())
				.where("lancamentosOperacaoCartao.mesAno = ?", filtro.getDtinicio())
				.unique() > 0;
	}

}
