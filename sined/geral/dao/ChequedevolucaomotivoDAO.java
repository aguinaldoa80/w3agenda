package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Chequedevolucaomotivo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("chequedevolucaomotivo.codigo")
public class ChequedevolucaomotivoDAO extends GenericDAO<Chequedevolucaomotivo> {

}
