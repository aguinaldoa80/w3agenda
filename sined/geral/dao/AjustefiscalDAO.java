package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Ajustefiscal;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AjustefiscalDAO extends GenericDAO<Ajustefiscal>{

	public List<Ajustefiscal> findByEntregadocumento(Entregadocumento entregadocumento) {
		if(entregadocumento == null || entregadocumento.getCdentregadocumento() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("ajustefiscal.codigoajuste, obslancamentofiscal.cdobslancamentofiscal, obslancamentofiscal.descricao," +
						"ajustefiscal.descricaocomplementar, ajustefiscal.aliquotaicms," +
						"ajustefiscal.basecalculoicms,ajustefiscal.valoricms, " +
						"ajustefiscal.outrosvalores, entregadocumento.cdentregadocumento," +
						"material.cdmaterial, material.nome")
				.join("ajustefiscal.entregadocumento entregadocumento")
				.leftOuterJoin("ajustefiscal.obslancamentofiscal obslancamentofiscal")
				.leftOuterJoin("ajustefiscal.material material")
				.where("entregadocumento = ?", entregadocumento)
				.list();
	}
	
	public List<Ajustefiscal> buscarItemComAjusteFiscal(String whereInEntregadocumento) {
		if(StringUtils.isEmpty(whereInEntregadocumento))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("ajustefiscal.cdajustefiscal, " +
						"ajustefiscal.descricaocomplementar, ajustefiscal.aliquotaicms," +
						"ajustefiscal.basecalculoicms,ajustefiscal.valoricms, " +
						"ajustefiscal.outrosvalores, entregadocumento.cdentregadocumento")
				.join("ajustefiscal.entregadocumento entregadocumento")
				.whereIn("entregadocumento.cdentregadocumento", whereInEntregadocumento)
				.list();
	}
	
	public List<Ajustefiscal> buscarItemComAjusteFiscalNota(String whereInNota) {
		if(StringUtils.isEmpty(whereInNota))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("ajustefiscal.cdajustefiscal, " +
						"ajustefiscal.descricaocomplementar, ajustefiscal.aliquotaicms," +
						"ajustefiscal.basecalculoicms,ajustefiscal.valoricms, " +
						"ajustefiscal.outrosvalores, notafiscalproduto.cdNota")
				.join("ajustefiscal.notafiscalproduto notafiscalproduto")
				.list();
	}
}
