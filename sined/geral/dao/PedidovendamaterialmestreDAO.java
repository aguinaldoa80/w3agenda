package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterialmestre;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PedidovendamaterialmestreDAO extends GenericDAO<Pedidovendamaterialmestre>{

	
	/**
	 * M�todo que busca os itens mestre do pedido de venda
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 * @since 27/03/2014
	 */
	public List<Pedidovendamaterialmestre> findByPedidovenda(Pedidovenda pedidovenda) {
		return query()
			.select("pedidovendamaterialmestre.cdpedidovendamaterialmestre, pedidovendamaterialmestre.altura, pedidovendamaterialmestre.qtde, " +
					 "pedidovendamaterialmestre.identificadorespecifico, pedidovendamaterialmestre.nomealternativo, pedidovendamaterialmestre.exibiritenskitflexivel, " +
					 "pedidovendamaterialmestre.largura, pedidovendamaterialmestre.comprimento, pedidovendamaterialmestre.identificadorinterno, " +
					 "pedidovendamaterialmestre.preco, pedidovendamaterialmestre.dtprazoentrega, pedidovendamaterialmestre.desconto, " +
					 "pedidovendamaterialmestre.valoripi, pedidovendamaterialmestre.ipi, pedidovendamaterialmestre.tipocobrancaipi, " +
					 "pedidovendamaterialmestre.tipoCalculoIpi, pedidovendamaterialmestre.aliquotaReaisIpi, " +
					 "pedidovendamaterialmestre.tipotributacaoicms, pedidovendamaterialmestre.outrasdespesas, pedidovendamaterialmestre.valorSeguro, " +
					 "pedidovendamaterialmestre.tipocobrancaicms, pedidovendamaterialmestre.valorbcicms, pedidovendamaterialmestre.icms, " +
					 "pedidovendamaterialmestre.valoricms, pedidovendamaterialmestre.valorbcicmsst, pedidovendamaterialmestre.icmsst, " +
					 "pedidovendamaterialmestre.valoricmsst, pedidovendamaterialmestre.reducaobcicmsst, pedidovendamaterialmestre.margemvaloradicionalicmsst, " +
					 "pedidovendamaterialmestre.valorbcfcp, pedidovendamaterialmestre.fcp, pedidovendamaterialmestre.valorfcp, " +
					 "pedidovendamaterialmestre.valorbcfcpst, pedidovendamaterialmestre.fcpst, pedidovendamaterialmestre.valorfcpst, " +
					 "pedidovendamaterialmestre.valorbcdestinatario, pedidovendamaterialmestre.valorbcfcpdestinatario, pedidovendamaterialmestre.fcpdestinatario, " +
					 "pedidovendamaterialmestre.icmsdestinatario, pedidovendamaterialmestre.icmsinterestadual, pedidovendamaterialmestre.icmsinterestadualpartilha, " +
					 "pedidovendamaterialmestre.valorfcpdestinatario, pedidovendamaterialmestre.valoricmsdestinatario, pedidovendamaterialmestre.valoricmsremetente, " +
					 "pedidovendamaterialmestre.difal, pedidovendamaterialmestre.valordifal, ncmcapitulo.cdncmcapitulo, cfop.cdcfop, " +
					 "pedidovendamaterialmestre.ncmcompleto, pedidovendamaterialmestre.modalidadebcicms, pedidovendamaterialmestre.modalidadebcicmsst, " +
					 "pedidovendamaterialmestre.percentualdesoneracaoicms, pedidovendamaterialmestre.valordesoneracaoicms, pedidovendamaterialmestre.abaterdesoneracaoicms, " +
					 "grupotributacao.cdgrupotributacao, " +
					 "pedidovenda.cdpedidovenda, material.cdmaterial, material.identificacao, unidademedida.cdunidademedida, unidademedida.nome, " +
					 "material.nome, material.producao, material.peso, material.pesobruto, material.kitflexivel, materialgrupo.cdmaterialgrupo, material.ncmcompleto," +
					 "materialtipo.cdmaterialtipo, materialcategoria.cdmaterialcategoria")
			.join("pedidovendamaterialmestre.material material")
			.join("pedidovendamaterialmestre.pedidovenda pedidovenda")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("pedidovendamaterialmestre.grupotributacao grupotributacao")
			.leftOuterJoin("material.materialtipo materialtipo")
			.leftOuterJoin("material.materialcategoria materialcategoria")
			.leftOuterJoin("pedidovendamaterialmestre.cfop cfop")
			.leftOuterJoin("pedidovendamaterialmestre.ncmcapitulo ncmcapitulo")
			.where("pedidovenda = ?",pedidovenda)
			.list();		
	}

}
