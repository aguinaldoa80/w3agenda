package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.FaixaMarkupNome;
import br.com.linkcom.sined.geral.bean.Garantiareformaitem;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoagendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoordemsituacao;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.PedidoItenBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;


public class PedidovendamaterialDAO extends GenericDAO<Pedidovendamaterial>{
	
	/**
	 * Busca os materiais de uma determinada venda.
	 *
	 * @param pedidovenda
	 * @return
	 * @author Thiago Augusto
	 */
	public List<Pedidovendamaterial> findByPedidoVenda(Pedidovenda pedidovenda, String orderBy, Boolean fromCopiar){
		if (pedidovenda == null || pedidovenda.getCdpedidovenda() == null) {
			throw new SinedException("Venda n�o pode ser nulo.");
		}
		QueryBuilder<Pedidovendamaterial> query = querySined();
		
		query
				.select("pedidovendamaterial.cdpedidovendamaterial, pedidovendamaterial.ordem, material.identificacao, material.nome, pedidovendamaterial.preco, pedidovendamaterial.desconto, pedidovendamaterial.percentualdesconto, material.cdmaterial, material.produto, material.servico, " +
						"material.extipi, material.ncmcompleto, material.codigobarras, ncmcapitulo.cdncmcapitulo, ncmcapitulo.descricao, unidademedida.simbolo, material.nomenf, " +
						"material.patrimonio, material.epi, materialmestregrade.cdmaterial, materialmestregrade.nome, materialmestregrade.identificacao, pedidovendamaterial.quantidade, unidademedida.cdunidademedida, unidademedida.nome, " +
						"material.producao, material.pesoliquidovalorvenda, material.peso, material.pesobruto, material.valorcusto, material.vendapromocional, material.vendaecf, " + 
						"pedidovendamaterial.saldo, pedidovendamaterial.qtdereferencia, pedidovendamaterial.peso, pedidovendamaterial.bandaalterada, pedidovendamaterial.servicoalterado, pedidovendamaterial.valorcoleta, " +
						"pedidovendamaterial.valoripi, pedidovendamaterial.ipi, pedidovendamaterial.tipocobrancaipi, pedidovendamaterial.aliquotareaisipi, pedidovendamaterial.tipocalculoipi, grupotributacao.cdgrupotributacao, " +
						"material.obrigarlote, materialgrupo.cdmaterialgrupo, materialgrupo.nome, " + 
						"unidademedidamaterial.cdunidademedida, " +
						"empresa.cdpessoa, empresa.integracaowms, pedidovendamaterial.dtprazoentrega, " +
						"loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, pedidovendamaterial.observacao, pedidovendamaterial.valorimposto, pedidovendamaterial.valorMinimo, " +
						"material.valorvenda, material.valorcusto, material.valorvendaminimo, material.valorvendamaximo, pedidovendamaterial.fatorconversao, " +
						"pedidovendamaterial.valorcustomaterial, pedidovendamaterial.valorvendamaterial, pedidovendamaterial.comprimento, pedidovendamaterial.custooperacional, " +
						"pedidovendamaterial.altura, pedidovendamaterial.largura, pedidovendamaterial.comprimentooriginal, pedidovendamaterial.multiplicador," +
						"arquivo.cdarquivo, arquivoMestre.cdarquivo, fornecedor.cdpessoa, fornecedor.nome, listaCaracteristica.cdmaterialcaracteristica, listaCaracteristica.nome, fornecedor.inscricaoestadual," +
						"fornecedor.tipopessoa, fornecedor.cnpj, fornecedor.cpf, fornecedor.razaosocial, " +
						"materialcoleta.cdmaterial, materialcoleta.nome, materialcoleta.identificacao," +
						"listaCaracteristicaColeta.cdmaterialcaracteristica, listaCaracteristicaColeta.nome, " +
						"listaCaracteristicaMestre.cdmaterialcaracteristica, listaCaracteristicaMestre.nome, " +
						"localizacaoestoque.cdlocalizacaoestoque, localizacaoestoque.descricao," +
						"materialmestre.cdmaterial, materialmestre.nome, materialmestre.producao, materialmestre.vendapromocional, materialmestre.kitflexivel, " +
						"materialmestregrupo.cdmaterialgrupo, materialmestregrupo.nome, pedidovendamaterial.identificadorespecifico, pedidovendamaterial.identificadorintegracao, pedidovendamaterial.identificadorinterno," +
						"pedidovendamaterial.percentualrepresentacao, " +
						"pedidovendamaterial.percentualcomissaoagencia, materialproduto.largura, materialproduto.comprimento, materialproduto.altura, " +
						"pedidovendamaterial.qtdevolumeswms, pedidovendamaterial.valorSeguro, pedidovendamaterial.outrasdespesas, " +
						"pedidovendamaterial.tipotributacaoicms, " +
						"pedidovendamaterial.tipocobrancaicms, pedidovendamaterial.valorbcicms, pedidovendamaterial.icms, " +
						"pedidovendamaterial.valoricms, pedidovendamaterial.valorbcicmsst, pedidovendamaterial.icmsst, " +
						"pedidovendamaterial.valoricmsst, pedidovendamaterial.reducaobcicmsst, pedidovendamaterial.margemvaloradicionalicmsst, " +
						"pedidovendamaterial.valorbcfcp, pedidovendamaterial.fcp, pedidovendamaterial.valorfcp, " +
						"pedidovendamaterial.valorbcfcpst, pedidovendamaterial.fcpst, pedidovendamaterial.valorfcpst, " +
						"pedidovendamaterial.valorbcdestinatario, pedidovendamaterial.valorbcfcpdestinatario, pedidovendamaterial.fcpdestinatario, " +
						"pedidovendamaterial.icmsdestinatario, pedidovendamaterial.icmsinterestadual, pedidovendamaterial.icmsinterestadualpartilha, " +
						"pedidovendamaterial.valorfcpdestinatario, pedidovendamaterial.valoricmsdestinatario, pedidovendamaterial.valoricmsremetente, " +
						"pedidovendamaterial.difal, pedidovendamaterial.valordifal, ncmcapituloItem.cdncmcapitulo, cfop.cdcfop, " +
						"pedidovendamaterial.ncmcompleto, pedidovendamaterial.modalidadebcicms, pedidovendamaterial.modalidadebcicmsst, " +
						"pedidovendamaterial.percentualdesoneracaoicms, pedidovendamaterial.valordesoneracaoicms, pedidovendamaterial.abaterdesoneracaoicms, " +
						"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.sincronizarComWMS, pedidovendamaterial.pesomedio, " +
						"materialFaixaMarkup.cdMaterialFaixaMarkup, " +
						"faixaMarkupNome.nome, faixaMarkupNome.cor, faixaMarkupNome.cdFaixaMarkupNome," +
						"materialtipo.cdmaterialtipo, materialcategoria.cdmaterialcategoria, pneu.acompanhaRoda, pneu.lonas, otrpneutipo.cdOtrPneuTipo")
				.join("pedidovendamaterial.pedidovenda pedidovenda")
				.join("pedidovendamaterial.material material")
				.leftOuterJoin("material.materialtipo materialtipo")
				.leftOuterJoin("material.materialcategoria materialcategoria")
				.leftOuterJoin("pedidovendamaterial.materialmestre materialmestre")
				.leftOuterJoin("pedidovendamaterial.materialcoleta materialcoleta")
				.leftOuterJoin("pedidovendamaterial.pneu pneu")
				.leftOuterJoin("pedidovendamaterial.cfop cfop")
				.leftOuterJoin("pedidovendamaterial.ncmcapitulo ncmcapituloItem")
				.leftOuterJoin("pneu.otrPneuTipo otrpneutipo")
				.leftOuterJoin("material.unidademedida unidademedidamaterial")
				.leftOuterJoin("pedidovendamaterial.loteestoque loteestoque")
				.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("materialmestre.materialgrupo materialmestregrupo")
				.leftOuterJoin("pedidovendamaterial.unidademedida unidademedida")
				.leftOuterJoin("pedidovenda.empresa empresa")
				.leftOuterJoin("material.arquivo arquivo")
				.leftOuterJoin("materialmestre.arquivo arquivoMestre")
				.leftOuterJoin("pedidovendamaterial.fornecedor fornecedor")
				.leftOuterJoin("material.listaCaracteristica listaCaracteristica")
				.leftOuterJoin("materialcoleta.listaCaracteristica listaCaracteristicaColeta")
				.leftOuterJoin("materialmestre.listaCaracteristica listaCaracteristicaMestre")
				.leftOuterJoin("material.localizacaoestoque localizacaoestoque")
				.leftOuterJoin("material.materialproduto materialproduto")
				.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("pedidovendamaterial.grupotributacao grupotributacao")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.leftOuterJoin("pedidovendamaterial.materialFaixaMarkup materialFaixaMarkup")
				.leftOuterJoin("pedidovendamaterial.faixaMarkupNome faixaMarkupNome")
				.where("pedidovenda = ?",pedidovenda);

		query = SinedUtil.setJoinsByComponentePneu(query);
		if(fromCopiar != null && fromCopiar){
			query.leftOuterJoin("material.listaMaterialempresa listaMaterialempresa");
			query.leftOuterJoin("listaMaterialempresa.empresa empresaMaterial");
			query.openParentheses()
				.where("empresaMaterial is null").or()
				.whereIn("empresaMaterial.cdpessoa", new SinedUtil().getListaEmpresa())
			.closeParentheses();
		}
		
		if(orderBy != null && !"".equals(orderBy)){
			query.orderBy(orderBy);
		}else {
			query.orderBy("pedidovendamaterial.ordem, material.nome");
		}
		
		return query.list();
	}

	/**
	 * Busca a quantidade restante do pedido de venda material.
	 *
	 * @param pedidovendamaterial
	 * @return
	 * @since 09/11/2011
	 * @author Rodrigo Freitas
	 * @param considerarDevolucaoProducaoOtr 
	 */
	@SuppressWarnings("unchecked")
	public Double getQtdeRestante(Pedidovendamaterial pedidovendamaterial, boolean considerarDevolucaoProducaoOtr) {
		boolean isSegmentoOtr = SinedUtil.isSegmentoOtr();
		String queryInterna = "SELECT PVM.CDPEDIDOVENDAMATERIAL,  " +
							"PVM.QUANTIDADE AS QTDE_PEDIDOVENDA,  " +
					        "0 AS QTDE_VENDA,  " +
					        "0 AS QTDE_DEVOLUCAOCOLETA," +
					        "0 AS QTDE_COMPRADA " +
							"FROM PEDIDOVENDAMATERIAL PVM " +
							"WHERE PVM.CDPEDIDOVENDAMATERIAL =  " + pedidovendamaterial.getCdpedidovendamaterial() + " " +
							"GROUP BY PVM.CDPEDIDOVENDAMATERIAL, PVM.QUANTIDADE " +
					
							"UNION ALL " +
							
							"SELECT PVM.CDPEDIDOVENDAMATERIAL,  " +
						    		"PVM.QUANTIDADE AS QTDE_PEDIDOVENDA,  " +
						            "SUM(VM.QUANTIDADE) AS QTDE_VENDA,  " +
						            "0 AS QTDE_DEVOLUCAOCOLETA," +
						            "0 AS QTDE_COMPRADA " +
							"FROM PEDIDOVENDAMATERIAL PVM " +
							"JOIN VENDAMATERIAL VM ON VM.CDPEDIDOVENDAMATERIAL = PVM.CDPEDIDOVENDAMATERIAL  " +
							"JOIN VENDA V ON V.CDVENDA = VM.CDVENDA  " +
							"WHERE PVM.CDPEDIDOVENDAMATERIAL =  " + pedidovendamaterial.getCdpedidovendamaterial() + " " +
							"AND V.CDVENDASITUACAO <> 4 " +
							"GROUP BY PVM.CDPEDIDOVENDAMATERIAL, PVM.QUANTIDADE " +
				
							"UNION ALL " +
						    
						    "SELECT PVM.CDPEDIDOVENDAMATERIAL,  " +
						    		"PVM.QUANTIDADE AS QTDE_PEDIDOVENDA,  " +
						            "0 AS QTDE_VENDA,  " +
						            "SUM(COALESCE(CM.QUANTIDADEDEVOLVIDA, 0)) AS QTDE_DEVOLUCAOCOLETA, " +
						            "SUM(COALESCE(CM.QUANTIDADECOMPRADA, 0)) AS QTDE_COMPRADA " +
							"FROM PEDIDOVENDAMATERIAL PVM " +
							"JOIN COLETAMATERIAL CM ON CM.CDPEDIDOVENDAMATERIAL = PVM.CDPEDIDOVENDAMATERIAL   " +
							"WHERE PVM.CDPEDIDOVENDAMATERIAL =  " + pedidovendamaterial.getCdpedidovendamaterial() + " " +
							(isSegmentoOtr? ("and not EXISTS(select 1 "+
											"  				from PRODUCAOORDEMMATERIAL "+
											" 				where PRODUCAOORDEMMATERIAL.cdpedidovendamaterial = pvm.cdpedidovendamaterial) "): "")+
							"GROUP BY PVM.CDPEDIDOVENDAMATERIAL, PVM.QUANTIDADE ";
		if(considerarDevolucaoProducaoOtr && SinedUtil.isSegmentoOtr()){
			queryInterna += " UNION ALL "+
							"SELECT PVM.CDPEDIDOVENDAMATERIAL,   " +
							"PVM.QUANTIDADE AS QTDE_PEDIDOVENDA,  " +
							"0 AS QTDE_VENDA,  " +
							"SUM(COALESCE(PRODUCAOORDEMMATERIAL.qtdeperdadescarte, 0)) AS QTDE_DEVOLUCAOCOLETA," + 
							"SUM(COALESCE(CM.QUANTIDADECOMPRADA, 0)) AS QTDE_COMPRADA " +
							"FROM PEDIDOVENDAMATERIAL PVM " +
							"JOIN coletamaterialpedidovendamaterial CMPVM ON CMPVM.CDPEDIDOVENDAMATERIAL = PVM.CDPEDIDOVENDAMATERIAL " +
							"JOIN coletamaterial CM ON CM.cdcoletamaterial = CMPVM.cdcoletamaterial " +
							"join PRODUCAOORDEMMATERIAL on PRODUCAOORDEMMATERIAL.cdpedidovendamaterial = PVM.CDPEDIDOVENDAMATERIAL " +
							"join producaoordem on producaoordem.cdproducaoordem = PRODUCAOORDEMMATERIAL.cdproducaoordem " +
							"WHERE PVM.CDPEDIDOVENDAMATERIAL =   " + pedidovendamaterial.getCdpedidovendamaterial() + 
							" and producaoordem.producaoordemsituacao = " + Producaoordemsituacao.CANCELADA.ordinal() + " " +
							" and not exists (select 1 from producaoordemhistorico poh where poh.cdproducaoordem = producaoordem.cdproducaoordem and upper(poh.observacao) like '%REPROCESSADA%') " +
							" GROUP BY PVM.CDPEDIDOVENDAMATERIAL, PVM.QUANTIDADE"; 
		}
		String sql = "SELECT CDPEDIDOVENDAMATERIAL, QTDE_PEDIDOVENDA - SUM(QTDE_VENDA) - SUM(QTDE_DEVOLUCAOCOLETA) - SUM(QTDE_COMPRADA) AS RESTANTE " +
				"FROM ( "+queryInterna+") AS QUERY " +

		"GROUP BY CDPEDIDOVENDAMATERIAL, QTDE_PEDIDOVENDA";
		
		List<Double> listaUpdate = getJdbcTemplate().query(sql, 
			new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return rs.getDouble("RESTANTE");
				}
			});
		
		if(listaUpdate != null && listaUpdate.size() > 0){
			return listaUpdate.get(0);
		} else {
			return null;
		}
	}

	/**
	 * Busca os itens de v�rios pedidos de venda para o cancelamento.
	 *
	 * @param whereIn
	 * @return
	 * @since 14/11/2011
	 * @author Rodrigo Freitas
	 */
	public List<Pedidovendamaterial> findByPedidoVenda(String whereIn) {
		return query()
					.select("pedidovendamaterial.cdpedidovendamaterial, pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao, empresa.cdpessoa, empresa.integracaowms, " +
							"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.sincronizarComWMS")
					.join("pedidovendamaterial.pedidovenda pedidovenda")
					.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
					.leftOuterJoin("pedidovenda.empresa empresa")
					.whereIn("pedidovenda.cdpedidovenda", whereIn)
					.list();
	}
	
	public List<Pedidovendamaterial> findByPedidoVendaWithMaterial(String whereIn) {
		return query()
					.select("pedidovendamaterial.cdpedidovendamaterial, pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao, empresa.cdpessoa, empresa.integracaowms, " +
							"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.sincronizarComWMS, material.cdmaterial, material.nome, pneu.cdpneu, pneu.serie, pneu.dot")
					.join("pedidovendamaterial.pedidovenda pedidovenda")
					.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
					.leftOuterJoin("pedidovenda.empresa empresa")
					.leftOuterJoin("pedidovendamaterial.material material")
					.leftOuterJoin("pedidovendamaterial.pneu pneu")
					.whereIn("pedidovenda.cdpedidovenda", whereIn)
					.list();
	}
	
	/**
	 * M�todo que busca os itens do pedido de venda
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 16/01/2014
	 */
	public List<Pedidovendamaterial> findForMontarGrade(String whereIn) {
		return query()
					.select("pedidovendamaterial.cdpedidovendamaterial, pedidovendamaterial.quantidade, pedidovendamaterial.preco, " +
							"material.cdmaterial, material.nome, material.identificacao, loteestoque.cdloteestoque," +
							"materialmestregrade.cdmaterial, pedidovendamaterial.altura, pedidovendamaterial.largura," +
							"pedidovendamaterial.comprimento, pedidovendamaterial.comprimentooriginal," +
							"pedidovendamaterial.multiplicador ")
					.join("pedidovendamaterial.material material")
					.leftOuterJoin("material.materialmestregrade materialmestregrade")
					.leftOuterJoin("pedidovendamaterial.loteestoque loteestoque")
					.whereIn("pedidovendamaterial.cdpedidovendamaterial", whereIn)
					.list();
	}
	
	/**
	 * M�todo que busca a pedidovendamaterial com os dados de fornecedores do material
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovendamaterial> loadComMaterialfornecedor(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Pedido de Venda n�o pode ser nula.");
		
		return query()
				.select("pedidovendamaterial.cdpedidovendamaterial, material.cdmaterial, listaFornecedor.cdmaterialfornecedor, " +
						"fornecedor.cdpessoa, pedidovendamaterial.quantidade, pedidovendamaterial.preco, pedidovendamaterial.desconto, pedidovenda.cdpedidovenda, empresa.cdpessoa, pedidovendamaterial.fornecedor ")
				.join("pedidovendamaterial.pedidovenda pedidovenda")
				.join("pedidovendamaterial.material material")
				.join("material.listaFornecedor listaFornecedor")
				.join("listaFornecedor.fornecedor fornecedor")
				.join("pedidovenda.empresa empresa")
				.where("pedidovenda  = ?", pedidovenda)
				.list();
	}
	
	/**
	 * M�todo que retorna a quantidade reservada do material
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @param empresa 
	 */
	@SuppressWarnings("unchecked")
	public Double getQtdeReservada(Material material, Empresa empresa, Localarmazenagem localarmazenagem, Loteestoque loteestoque){
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo");
		
		String sql = "SELECT (SUM(qtde_unidadeprincipal(PVM.CDMATERIAL, PVM.CDUNIDADEMEDIDA, PVM.QUANTIDADE)) - SUM(CASE WHEN V.CDVENDASITUACAO IS NOT NULL AND V.CDVENDASITUACAO <> 4 THEN qtde_unidadeprincipal(VM.CDMATERIAL, VM.CDUNIDADEMEDIDA, COALESCE(VM.QUANTIDADE, 0)) ELSE 0 END )) AS RESERVADO " + 
		"FROM PEDIDOVENDAMATERIAL PVM " +
		"JOIN PEDIDOVENDA PV ON PV.CDPEDIDOVENDA = PVM.CDPEDIDOVENDA " +
		"JOIN EMPRESA E ON E.CDPESSOA = PV.CDEMPRESA " +
		"JOIN PEDIDOVENDATIPO PVT ON PVT.CDPEDIDOVENDATIPO = PV.CDPEDIDOVENDATIPO " +
		"LEFT OUTER JOIN VENDAMATERIAL VM ON VM.CDPEDIDOVENDAMATERIAL = PVM.CDPEDIDOVENDAMATERIAL " +
		"LEFT OUTER JOIN VENDA V ON V.CDVENDA = VM.CDVENDA " +
		"LEFT OUTER JOIN MATERIALRELACIONADO MR ON MR.CDMATERIAL = PVM.CDMATERIAL " +
		"WHERE (PVM.CDMATERIAL = " + material.getCdmaterial() + " OR MR.CDMATERIALPROMOCAO = " + material.getCdmaterial() + ") " +
		"AND PVT.RESERVA = TRUE and PV.PEDIDOVENDASITUACAO not in (" + 
										Pedidovendasituacao.CANCELADO.getValue() + "," +
										Pedidovendasituacao.CONFIRMADO.getValue() + 
										") " +
		"AND (V IS NULL OR V.CDVENDASITUACAO <> 4) " + 
		(empresa != null && empresa.getCdpessoa() != null ? ("AND E.CDPESSOA = " + empresa.getCdpessoa()) + " ": "") +
		(localarmazenagem != null && localarmazenagem.getCdlocalarmazenagem() != null ? ("AND PV.CDLOCALARMAZENAGEM = " + localarmazenagem.getCdlocalarmazenagem()) + " ": "") +
		(loteestoque != null && loteestoque.getCdloteestoque() != null ? ("AND PVM.CDLOTEESTOQUE = " + loteestoque.getCdloteestoque()) + " ": "") +
		" ";
		//sql = "SELECT SUM(quantidade) from reserva"

		SinedUtil.markAsReader();
		List<Double> lista = getJdbcTemplate().query(sql, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getDouble("RESERVADO");
			}
		});
		
		if(lista != null && lista.size() > 0){
			return lista.get(0);
		} else {
			return null;
		}
	}
	
	/**
	 * M�todo que retorna a qtde reservada do material
	 *
	 * @param material
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public Double getQtdeReservada(Material material, Pedidovenda pedidovenda, Loteestoque loteestoque){
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo");
		
		Empresa empresa = null;
		if(pedidovenda != null){
			empresa = pedidovenda.getEmpresa();
		}
		Localarmazenagem localarmazenagem = null;
		if(pedidovenda != null && pedidovenda.getLocalarmazenagem() != null){
			localarmazenagem = pedidovenda.getLocalarmazenagem();
		}
		
		String sql = "SELECT (SUM(qtde_unidadeprincipal(PVM.CDMATERIAL, PVM.CDUNIDADEMEDIDA, PVM.QUANTIDADE) * COALESCE(MR.QUANTIDADE, 1)) - SUM(qtde_unidadeprincipal(VM.CDMATERIAL, VM.CDUNIDADEMEDIDA, COALESCE(VM.QUANTIDADE, 0)))) AS RESERVADO " + 
		"FROM PEDIDOVENDAMATERIAL PVM " +
		"JOIN PEDIDOVENDA PV ON PV.CDPEDIDOVENDA = PVM.CDPEDIDOVENDA " +
		"LEFT OUTER JOIN EMPRESA E ON E.CDPESSOA = PV.CDEMPRESA " +
		"JOIN PEDIDOVENDATIPO PVT ON PVT.CDPEDIDOVENDATIPO = PV.CDPEDIDOVENDATIPO " +
		"LEFT OUTER JOIN VENDAMATERIAL VM ON VM.CDPEDIDOVENDAMATERIAL = PVM.CDPEDIDOVENDAMATERIAL " +
		"LEFT OUTER JOIN VENDA V ON V.CDVENDA = VM.CDVENDA " +
		"LEFT OUTER JOIN MATERIALRELACIONADO MR ON MR.CDMATERIAL = PVM.CDMATERIAL " +
		"WHERE (PVM.CDMATERIAL = " + material.getCdmaterial() + " OR MR.CDMATERIALPROMOCAO = " + material.getCdmaterial() + ") " +
		"AND PVT.RESERVA = TRUE " +
		"AND PV.PEDIDOVENDASITUACAO not in (" + 
										Pedidovendasituacao.CANCELADO.getValue() + "," +
										Pedidovendasituacao.CONFIRMADO.getValue() + 
										") " +
		"AND (V IS NULL OR V.CDVENDASITUACAO <> 4) " +
		(loteestoque != null && loteestoque.getCdloteestoque() != null ? (" AND PVM.CDLOTEESTOQUE = " + loteestoque.getCdloteestoque()) + " ": "") +
		(empresa != null && empresa.getCdpessoa() != null ? (" AND E.CDPESSOA = " + empresa.getCdpessoa()) + " ": "") +
		(localarmazenagem != null && localarmazenagem.getCdlocalarmazenagem() != null ? (" AND PV.CDLOCALARMAZENAGEM = " + localarmazenagem.getCdlocalarmazenagem()) + " ": "") +
		(pedidovenda != null && pedidovenda.getCdpedidovenda() != null ? (" AND PV.CDPEDIDOVENDA <> " + pedidovenda.getCdpedidovenda()) + " ": "") +
		" ";

		SinedUtil.markAsReader();
		List<Double> lista = getJdbcTemplate().query(sql, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getDouble("RESERVADO");
			}
		});
		
		if(lista != null && lista.size() > 0){
			return lista.get(0);
		} else {
			return null;
		}
	}
	
	public List<Pedidovendamaterial> findByVendaProducao(String whereIn, String whereInPedidovendamaterial) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		if("".equals(whereInPedidovendamaterial)){
			whereInPedidovendamaterial = null;
		}
		
		QueryBuilder<Pedidovendamaterial> query = query()
			.select("pedidovendamaterial.cdpedidovendamaterial, material.nome, pedidovendamaterial.quantidade, pedidovendamaterial.dtprazoentrega, unidademedida.simbolo, unidademedida.nome, unidademedida.cdunidademedida, " +
					"material.cdmaterial, material.servico, material.patrimonio, " +
					"material.produto, material.epi, pedidovenda.cdpedidovenda, cliente.cdpessoa," +
					"producaoetapa.cdproducaoetapa, pedidovendamaterial.altura, pedidovendamaterial.largura, pedidovendamaterial.comprimento, " +
					"pedidovendatipo.cdpedidovendatipo, producaoagendatipo.cdproducaoagendatipo, " +
					"producaoetapaPedido.cdproducaoetapa, pedidovendamaterial.observacao, empresa.cdpessoa, pneu.cdpneu")
			.join("pedidovendamaterial.pedidovenda pedidovenda")
			.join("pedidovenda.cliente cliente")
			.join("pedidovendamaterial.material material")
			.leftOuterJoin("material.producaoetapa producaoetapa")
			.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
			.leftOuterJoin("pedidovenda.empresa empresa")
			.leftOuterJoin("pedidovendatipo.producaoagendatipo producaoagendatipo")
			.leftOuterJoin("producaoagendatipo.producaoetapa producaoetapaPedido")
			.leftOuterJoin("pedidovendamaterial.unidademedida unidademedida")
			.leftOuterJoin("pedidovendamaterial.pneu pneu")
			.where("material.producao = ?", Boolean.TRUE)
			.whereIn("pedidovenda.cdpedidovenda", whereIn)
			.whereIn("pedidovendamaterial.cdpedidovendamaterial", whereInPedidovendamaterial)
			.orderBy("pedidovenda.cdpedidovenda, pedidovendamaterial.ordem");
		query = SinedUtil.setJoinsByComponentePneu(query);
		return query.list();
	}
	
	public List<Pedidovendamaterial> findForEntradafiscal(String whereIn, String whereInColeta) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		return query()
				.select("pedidovendamaterial.cdpedidovendamaterial, pedidovendamaterial.quantidade, pedidovendamaterial.preco, pedidovendamaterial.valorcoleta, " +
						"material.cdmaterial, material.nome, material.servico, material.patrimonio, material.produto, material.epi, " +
						"unidademedida.cdunidademedida, material.valorcusto, " +
						"materialcoleta.cdmaterial, materialcoleta.nome, materialcoleta.servico, materialcoleta.patrimonio, materialcoleta.produto, materialcoleta.epi, " +
						"unidademedidacoleta.cdunidademedida, materialcoleta.valorcusto ")
				.join("pedidovendamaterial.material material")
				.join("pedidovendamaterial.pedidovenda pedidovenda")
				.leftOuterJoin("pedidovendamaterial.unidademedida unidademedida")
				.leftOuterJoin("pedidovendamaterial.materialcoleta materialcoleta")
				.leftOuterJoin("materialcoleta.unidademedida unidademedidacoleta")
				.whereIn("pedidovenda.cdpedidovenda", whereIn)
				.where("pedidovendamaterial.cdpedidovendamaterial in (" +
						" select pvm.cdpedidovendamaterial " +
						" from ColetaMaterial cm " +
						" join cm.coleta c " +
						" join cm.pedidovendamaterial pvm " +
						" where c.cdcoleta in ("+whereInColeta+"))", StringUtils.isNotBlank(whereInColeta))
				.where("pedidovendamaterial.cdpedidovendamaterial in (" +
						"select pvm.cdpedidovendamaterial " +
						"from ColetaMaterial cm " +
						"join cm.coleta c " +
						"join cm.pedidovendamaterial pvm " +
						"join pvm.pedidovenda pv " +
						"where pv.cdpedidovenda in ("+whereIn+"))", StringUtils.isBlank(whereInColeta))
				.list();
	}

	public void updateSaldo(Pedidovendamaterial pvm, Money saldo) {
		getHibernateTemplate().bulkUpdate("update Pedidovendamaterial pvm set pvm.saldo = ? where pvm.id = ?", new Object[]{
				saldo, pvm.getCdpedidovendamaterial()
		});
	}
	
	/**
	 * Verifica se existe o vinculo com coleta ou produ��o
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/07/2014
	 */
	public List<Pedidovendamaterial> findForExistenciaColetaProducao(String whereIn) {
		return query()
					.select("pedidovendamaterial.cdpedidovendamaterial, pedidovendamaterial.quantidade")
					.whereIn("pedidovendamaterial.cdpedidovendamaterial", whereIn)
					.openParentheses()
					.where("exists (select cm.cdcoletamaterial from ColetaMaterial cm where cm.pedidovendamaterial = pedidovendamaterial)")
					.or()
					.where("exists (select pa.cdproducaoagendamaterial from Producaoagendamaterial pa where pa.pedidovendamaterial = pedidovendamaterial)")
					.closeParentheses()
					.list();
	}
	
	/**
	 * Retorna os itens do pedido de venda que est�o relacionados a Cota��o
	 *
	 * @param cotacao
	 * @return
	 * @author Lucas Costa
	 * @since 31/10/2014
	 */
	@SuppressWarnings("unchecked")
	public List<Pedidovendamaterial> findByCotacao (Cotacao cotacao){
		if(cotacao == null || cotacao.getCdcotacao() == null)
			throw new SinedException("Par�metro inv�lido");
		
		String sql= "select pm.cdmaterial, max(pm.preco) as preco "+ 
					"from cotacao c, cotacaoorigem co, solicitacaocompra s, Solicitacaocompraorigem so, "+ 
					"requisicaomaterial r, requisicaoorigem ro, pedidovenda p, pedidovendamaterial pm  "+
					"where "+ 
					"c.cdcotacao = co.cdcotacao and "+
					"co.cdsolicitacaocompra = s.cdsolicitacaocompra and "+
					"s.cdsolicitacaocompraorigem = so.cdsolicitacaocompraorigem and "+
					"so.cdrequisicaomaterial = r.cdrequisicaomaterial and "+
					"r.cdrequisicaoorigem = ro.cdrequisicaoorigem and "+
					"ro.cdpedidovenda = p.cdpedidovenda and "+
					"p.cdpedidovenda = pm.cdpedidovenda and "+
				 	"c.cdcotacao = "+cotacao.getCdcotacao() +" "+
				 	"group by pm.cdmaterial ";

		SinedUtil.markAsReader();
		return getJdbcTemplate().query(sql, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Pedidovendamaterial pedidovendamaterial = new Pedidovendamaterial();
				pedidovendamaterial.setMaterial(new Material(rs.getInt("cdmaterial")));
				pedidovendamaterial.setPreco(rs.getDouble("preco"));
				return pedidovendamaterial;
			}
		});
	}

	public List<Pedidovendamaterial> findForConferenciaByPedidovenda(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		return query()
			.select("pedidovendamaterial.cdpedidovendamaterial, pedidovendamaterial.quantidade, pedidovendamaterial.preco, " +
					"pedidovendamaterial.multiplicador, " +
					"material.cdmaterial, material.nome, material.valorcusto, material.identificacao, " +
					"pedidovenda.cdpedidovenda," +
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo ")
			.join("pedidovendamaterial.pedidovenda pedidovenda")
			.join("pedidovendamaterial.material material")
			.join("pedidovendamaterial.unidademedida unidademedida")
			.whereIn("pedidovenda.cdpedidovenda", whereIn)
			.list();
	}
	
	public List<Pedidovendamaterial> findForConferenciaByColeta(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		return query()
			.select("pedidovendamaterial.cdpedidovendamaterial, pedidovendamaterial.quantidade, pedidovendamaterial.preco, " +
					"pedidovendamaterial.multiplicador, " +
					"material.cdmaterial, material.nome, material.valorcusto, material.identificacao, " +
					"pedidovenda.cdpedidovenda," +
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo ")
			.join("pedidovendamaterial.pedidovenda pedidovenda")
			.join("pedidovendamaterial.material material")
			.join("pedidovendamaterial.unidademedida unidademedida")
			.where("exists (select pv.cdpedidovenda from ColetaMaterial cm " +
					"join cm.coleta c " +
					"join c.pedidovenda pv " +
					"where c.cdcoleta in (" + whereIn + ") " +
					"and pv.cdpedidovenda = pedidovenda.cdpedidovenda )")
			.list();
	}
	
	/**
	 * M�todo que atualiza a quantidade de um pedidovendamaterial de acordo com a separa��o realizada no wms;
	 * 
	 * @param pedidovendamaterial
	 * @throws Exception
	 * @author Rafael Salvio
	 */
	public void updateQuantidadeWms(Pedidovendamaterial pedidovendamaterial) throws Exception{
		if(pedidovendamaterial == null || pedidovendamaterial.getCdpedidovendamaterial() == null){
			throw new Exception("Pedidovendamaterial n�o pode ser nulo.");
		} else if(pedidovendamaterial.getQuantidade() == null){
			throw new Exception("A quantidade do pedidovendamaterial n�o pode ser nula.");
		}
		
		getJdbcTemplate().update("UPDATE pedidovendamaterial SET quantidade = ? WHERE cdpedidovendamaterial = ?;", 
				new Object[]{pedidovendamaterial.getQuantidade(), pedidovendamaterial.getCdpedidovendamaterial()});
	}
	
	public void updateValorcustomaterial(Pedidovendamaterial pedidovendamaterial, Double valorcustomaterial) {
		if(pedidovendamaterial != null && pedidovendamaterial.getCdpedidovendamaterial() != null && valorcustomaterial != null){
			getJdbcTemplate().update("UPDATE pedidovendamaterial SET valorcustomaterial = ? WHERE cdpedidovendamaterial = ?;", 
					new Object[]{valorcustomaterial, pedidovendamaterial.getCdpedidovendamaterial()});
		}
	}
	
	/**
	* M�todo que atualiza a quantidade volume de um pedidovendamaterial de acordo com a separa��o realizada no wms
	*
	* @param pedidovendamaterial
	* @throws Exception
	* @since 19/10/2015
	* @author Luiz Fernando
	*/
	public void updateQtdeVolumeWms(Pedidovendamaterial pedidovendamaterial) throws Exception{
		if(pedidovendamaterial == null || pedidovendamaterial.getCdpedidovendamaterial() == null){
			throw new Exception("Pedidovendamaterial n�o pode ser nulo.");
		} else if(pedidovendamaterial.getQuantidade() != null){
			getJdbcTemplate().update("UPDATE pedidovendamaterial SET qtdevolumeswms = ? WHERE cdpedidovendamaterial = ?;", 
				new Object[]{pedidovendamaterial.getQtdevolumeswms(), pedidovendamaterial.getCdpedidovendamaterial()});
		}
	}

	/**
	 * Carrega o pedidovendamaterial com o pedidovenda preenchido.
	 *
	 * @param pedidovendamaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/11/2015
	 */
	public Pedidovendamaterial loadWithPedidovenda(Pedidovendamaterial pedidovendamaterial) {
		return query()
					.select("pedidovendamaterial.cdpedidovendamaterial, pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao")
					.join("pedidovendamaterial.pedidovenda pedidovenda")
					.where("pedidovendamaterial = ?", pedidovendamaterial)
					.unique();
	}

	/**
	* M�todo que busca os itens interrompidos do pedido de venda
	*
	* @param pedidovenda
	* @return
	* @since 08/01/2016
	* @author Luiz Fernando
	*/
	public List<Pedidovendamaterial> findMaterialproducaoInterrompido(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("pedidovendamaterial.cdpedidovendamaterial, " +
						"material.identificacao, material.nome , material.nomenf, material.cdmaterial, material.producao, " +
						"pedidovendamaterial.preco, pedidovendamaterial.desconto, pedidovendamaterial.quantidade, " +
						"materialbanda.cdmaterial, materialbanda.nome, pneumarca.cdpneumarca, pneumarca.nome, pneu.serie, pneu.dot, pneu.cdpneu, " +
						"pneumedida.cdpneumedida, pneumedida.nome, pedidovenda.cdpedidovenda, unidademedida.cdunidademedida, unidademedida.nome, " +
						"unidademedida.simbolo, pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome, " +
						"pneumodelo.cdpneumodelo, pneumodelo.nome ")
				.join("pedidovendamaterial.pedidovenda pedidovenda")		
				.join("pedidovendamaterial.material material")
				.join("pedidovendamaterial.unidademedida unidademedida")
				.join("pedidovendamaterial.materialcoleta materialcoleta")
				.leftOuterJoin("pedidovendamaterial.pneu pneu")
				.leftOuterJoin("pneu.materialbanda materialbanda")
				.leftOuterJoin("pneu.pneumarca pneumarca")
				.leftOuterJoin("pneu.pneumedida pneumedida")
				.leftOuterJoin("pneu.pneumodelo pneumodelo")
				.leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
				.where("pedidovenda = ?", pedidovenda)
				.where("material.producao = true")
				.where("material.servico = true")
				.where("pedidovendamaterial.cdpedidovendamaterial in (select cm.pedidovendamaterial.cdpedidovendamaterial " +
								" from ColetaMaterial cm" +
								" join cm.pedidovendamaterial pd " +
								" join cm.material m " +
								" where m.cdmaterial = materialcoleta.cdmaterial " +
								" and cm.quantidadedevolvida is NOT NULL )")
//				.where("exists (select pdm.cdpedidovendamaterial from Producaoordemmaterial pdc" +
// 								" join pdc.pedidovendamaterial pdm " +
// 								"where pdm.cdpedidovendamaterial = pedidovendamaterial.cdpedidovendamaterial)")
				.list();
	}

	/**
	* M�todo que faz o update dos impostos do pedido de venda
	*
	* @param pedidovendamaterial
	* @since 22/01/2016
	* @author Luiz Fernando
	*/
	public void updateInformacoesImposto(Pedidovendamaterial pedidovendamaterial) {
		if(pedidovendamaterial != null && pedidovendamaterial.getCdpedidovendamaterial() != null){
			String sqlUpdate = "UPDATE Pedidovendamaterial SET " +
			" ipi = " + pedidovendamaterial.getIpi() +
			" ,valoripi = " + (pedidovendamaterial.getValoripi() != null ? SinedUtil.round(pedidovendamaterial.getValoripi().getValue().doubleValue(),2)*100 : "null") +
			" ,tipocobrancaipi = " + (pedidovendamaterial.getTipocobrancaipi() != null ? pedidovendamaterial.getTipocobrancaipi().getValue() : "null") +
			" ,tipocalculoipi = " + (pedidovendamaterial.getTipocalculoipi() != null ? pedidovendamaterial.getTipocalculoipi().getValue() : "null") +
			" ,aliquotareaisipi = " + (pedidovendamaterial.getAliquotareaisipi() != null ?  SinedUtil.round(pedidovendamaterial.getAliquotareaisipi().getValue().doubleValue(),2)*100 : "null") +
			" ,cdgrupotributacao = " + (pedidovendamaterial.getGrupotributacao() != null ? pedidovendamaterial.getGrupotributacao().getCdgrupotributacao() : "null") +
			" where cdpedidovendamaterial = " + pedidovendamaterial.getCdpedidovendamaterial();
			
			getJdbcTemplate().update(sqlUpdate);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Pedidovendamaterial> findForGerarProducao(String whereIn) {
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		
		String sql = 
				
				" select pvm.cdpedidovendamaterial, null as cdproducaoagenda, null as situacao, pvm.cdpneu" +
				" from pedidovendamaterial pvm " +
				" join material m on m.cdmaterial = pvm.cdmaterial " +
			    "  where pvm.cdpedidovenda in (" + whereIn +") " +
				" and m.producao = true " +
				" and not exists (select 1 from producaoordemmaterial pom where pom.cdpedidovendamaterial = pvm.cdpedidovendamaterial) " +
				" and not exists (select 1 from producaoagendamaterial pam join producaoagenda pa on pa.cdproducaoagenda = pam.cdproducaoagenda " +
				"	                   where pam.cdpedidovendamaterial = pvm.cdpedidovendamaterial and  " +
				"	                   pa.producaoagendasituacao <> " + Producaoagendasituacao.CANCELADA.ordinal() + ") " +
				
				" UNION ALL  " +
				
				" select distinct pom.cdpedidovendamaterial, pomo.cdproducaoagenda as cdproducaoagenda, po.producaoordemsituacao as situacao, pom.cdpneu " +
				" from producaoordemmaterial pom " +
				" join pedidovendamaterial pvm on pvm.cdpedidovendamaterial = pom.cdpedidovendamaterial and pvm.cdpedidovenda in (" + whereIn +") " +
				" join producaoordem po on po.cdproducaoordem = pom.cdproducaoordem " +
				" join producaoordemmaterialorigem pomo on pomo.cdproducaoordemmaterial = pom.cdproducaoordemmaterial " +
				" group by pom.cdpedidovendamaterial, pomo.cdproducaoagenda, po.producaoordemsituacao, pom.cdpneu " +
				
				" UNION ALL  " +
				
				" select distinct pam.cdpedidovendamaterial, pomo.cdproducaoagenda as cdproducaoagenda, po.producaoordemsituacao as situacao, pom.cdpneu " +
				" from producaoordemmaterial pom " +
				" join producaoordemmaterialorigem pomo on pomo.cdproducaoordemmaterial = pom.cdproducaoordemmaterial " +
			    " join producaoagendamaterial pam on pam.cdproducaoagendamaterial = pomo.cdproducaoagendamaterial " +
			    " join pedidovendamaterial pvm on pvm.cdpedidovendamaterial = pam.cdpedidovendamaterial and pvm.cdpedidovenda in (" + whereIn +") " +
			    " join producaoordem po on po.cdproducaoordem = pom.cdproducaoordem " +
			    " where pom.cdpedidovendamaterial is null " +
			    " and not exists (select 1 from producaoagendamaterial pam2 join producaoagenda pa2 on pa2.cdproducaoagenda = pam2.cdproducaoagenda " +
				"	                   where pam2.cdpedidovendamaterial = pam.cdpedidovendamaterial and  " +
				"	                   pa2.producaoagendasituacao = " + Producaoagendasituacao.EM_ESPERA.ordinal() + ") " +
			    " group by pam.cdpedidovendamaterial, pomo.cdproducaoagenda, po.producaoordemsituacao, pom.cdpneu ";
		

		SinedUtil.markAsReader();		
		List<PedidoItenBean> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new PedidoItenBean(rs.getInt("cdpedidovendamaterial"), 
										  rs.getObject("cdproducaoagenda") != null ? rs.getInt("cdproducaoagenda") : null, 
										  rs.getObject("situacao") != null ? rs.getInt("situacao") : null,
										  rs.getObject("cdpneu") != null ? rs.getInt("cdpneu") : null);
			}
		});
		
		List<Pedidovendamaterial> listaPedidovendamaterial = new ArrayList<Pedidovendamaterial>();
		if(SinedUtil.isListNotEmpty(list)){
			HashMap<Integer, Integer> mapAgenda;
			List<Pedidovendamaterial> listaNotAdd = new ArrayList<Pedidovendamaterial>();
			
			for(PedidoItenBean bean : list){
				if(bean.getCdproducaoagenda() == null){
					listaPedidovendamaterial.add(bean.getPedidovendamaterial());
				}else if(!listaPedidovendamaterial.contains(bean.getPedidovendamaterial()) && !listaNotAdd.contains(bean.getPedidovendamaterial())){
					mapAgenda = new HashMap<Integer, Integer>();
					
					boolean adicionar = true;
					for(PedidoItenBean bean2 : list){
						if(bean.getPedidovendamaterial().equals(bean2.getPedidovendamaterial()) && bean2.getCdproducaoagenda() != null){
							if(Producaoordemsituacao.EM_ESPERA.getValue().equals(bean2.getSituacao()) || Producaoordemsituacao.EM_ANDAMENTO.getValue().equals(bean2.getSituacao())){
								adicionar = false;
								break;
							}else {
								if(mapAgenda.get(bean2.getSituacao()) == null || !Producaoordemsituacao.CANCELADA.getValue().equals(mapAgenda.get(bean2.getCdproducaoagenda()))){
									mapAgenda.put(bean2.getCdproducaoagenda(), bean2.getSituacao());
								}
							}
						}
					}
					
					if(adicionar){
						for(Integer cdproducaoagenda : mapAgenda.keySet()){
							if(!Producaoordemsituacao.CANCELADA.getValue().equals(mapAgenda.get(cdproducaoagenda))){
								adicionar = false;
							}
						}
					}
					
					if(adicionar){
						listaPedidovendamaterial.add(bean.getPedidovendamaterial());
					}else {
						listaNotAdd.add(bean.getPedidovendamaterial());
					}
				}
			}
		}
		
		
		return listaPedidovendamaterial;
	}

	/**
	* M�todo que executa update no item do pedido de venda com altera��o de banda
	*
	* @param whereIn
	* @since 21/06/2016
	* @author Luiz Fernando
	*/
	public void updateBandaAlterada(String whereIn) {
		if(StringUtils.isNotBlank(whereIn)){
			getHibernateTemplate().bulkUpdate("update Pedidovendamaterial p set p.bandaalterada = true where p.id in (" + whereIn + ")"); 
		}
	}
	
	/**
	 * M�todo que executa update no item do pedido de venda com altera��o de servi�o
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 * @since 10/04/2017
	 */
	public void updateServicoAlterado(String whereIn) {
		if(StringUtils.isNotBlank(whereIn)){
			getHibernateTemplate().bulkUpdate("update Pedidovendamaterial p set p.servicoalterado = true where p.id in (" + whereIn + ")"); 
		}
	}

	/**
	 * Faz o update do material no item do pedido de venda
	 *
	 * @param pedidovendamaterial
	 * @param material
	 * @author Rodrigo Freitas
	 * @since 07/04/2017
	 */
	public void updateMaterial(Pedidovendamaterial pedidovendamaterial, Material material) {
		if(pedidovendamaterial == null || pedidovendamaterial.getCdpedidovendamaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		getHibernateTemplate().bulkUpdate("update Pedidovendamaterial p set p.material = ? where p = ?", new Object[]{material, pedidovendamaterial});
	}
	
	public Pedidovendamaterial findByReformapneu(Pedidovenda pedidovenda, Material servico, Pneu pneu, Pedidovendamaterial pedidovendamaterial){
		return query()
			.select("pedidovendamaterial.cdpedidovendamaterial, pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao, pedidovendamaterial.valorvendamaterial, pneu.cdpneu, pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome")
			.join("pedidovendamaterial.pedidovenda pedidovenda")
			.join("pedidovendamaterial.material material")
			.join("pedidovendamaterial.pneu pneu")
			.leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
			.where("pedidovenda = ?", pedidovenda)
			.where("material = ?", servico)
			.where("pneu = ?", pneu)
			.where("pedidovendamaterial = ?", pedidovendamaterial)
			.unique();
	}
	
	public void limpaGarantiareformaitem(Pedidovendamaterial pedidovendamaterial){
		String sql = "update pedidovendamaterial set cdgarantiareformaitem = null " + 
					 " where cdpedidovendamaterial = " + pedidovendamaterial.getCdpedidovendamaterial();
		getJdbcTemplate().execute(sql);		
	}
	
	public void updateDesconto(Pedidovendamaterial pedidovendamaterial, Money desconto, Double percentualdesconto) {
		getHibernateTemplate().bulkUpdate("update Pedidovendamaterial pvm set pvm.desconto = ?, pvm.percentualdesconto = ? where pvm = ?", new Object[]{desconto, percentualdesconto, pedidovendamaterial});
	}

	public void updateGarantiareformaitemAndDescontogarantiareforma(Pedidovendamaterial pedidovendamaterial, Garantiareformaitem garantiareformaitem, Money descontogarantiareforma) {
		getHibernateTemplate().bulkUpdate("update Pedidovendamaterial pvm set pvm.garantiareformaitem = ?, pvm.descontogarantiareforma = ? where pvm = ?", new Object[]{garantiareformaitem, descontogarantiareforma, pedidovendamaterial});
	}
	
	@SuppressWarnings("unchecked")
	public Double getQtdeVendasrelacionadas(Pedidovendamaterial pedidovendamaterial, Vendamaterial vendamaterialIgnorar) {
		String sql = "SELECT CDPEDIDOVENDAMATERIAL, SUM(QTDE_VENDA) + SUM(QTDE_DEVOLUCAOCOLETA) + SUM(QTDE_COMPRADA) AS QTDE " +
		"FROM ( " +
			"SELECT PVM.CDPEDIDOVENDAMATERIAL,  " +
		    		"qtde_unidadeprincipal(PVM.CDMATERIAL, PVM.CDUNIDADEMEDIDA, PVM.QUANTIDADE) AS QTDE_PEDIDOVENDA,  " +
		            "SUM(qtde_unidadeprincipal(VM.CDMATERIAL, VM.CDUNIDADEMEDIDA, VM.QUANTIDADE))  AS QTDE_VENDA,  " +
		            "0 AS QTDE_DEVOLUCAOCOLETA," +
		            "0 AS QTDE_COMPRADA " +
			"FROM PEDIDOVENDAMATERIAL PVM " +
			"JOIN VENDAMATERIAL VM ON VM.CDPEDIDOVENDAMATERIAL = PVM.CDPEDIDOVENDAMATERIAL  " +
			"JOIN VENDA V ON V.CDVENDA = VM.CDVENDA  " +
			"WHERE PVM.CDPEDIDOVENDAMATERIAL =  " + pedidovendamaterial.getCdpedidovendamaterial() + " " +
			"AND V.CDVENDASITUACAO <> 4 " +
			(vendamaterialIgnorar != null? " AND VM.CDVENDAMATERIAL <> "+vendamaterialIgnorar.getCdvendamaterial(): "")+
			"GROUP BY PVM.CDPEDIDOVENDAMATERIAL, PVM.QUANTIDADE " +

			"UNION ALL " +
		    
		    "SELECT PVM.CDPEDIDOVENDAMATERIAL,  " +
		    		"PVM.QUANTIDADE AS QTDE_PEDIDOVENDA,  " +
		            "0 AS QTDE_VENDA,  " +
		            "SUM(COALESCE(CM.QUANTIDADEDEVOLVIDA, 0)) AS QTDE_DEVOLUCAOCOLETA, " +
		            "SUM(COALESCE(CM.QUANTIDADECOMPRADA, 0)) AS QTDE_COMPRADA " +
			"FROM PEDIDOVENDAMATERIAL PVM " +
			"JOIN COLETAMATERIAL CM ON CM.CDPEDIDOVENDAMATERIAL = PVM.CDPEDIDOVENDAMATERIAL   " +
			"WHERE PVM.CDPEDIDOVENDAMATERIAL =  " + pedidovendamaterial.getCdpedidovendamaterial() + " " +
			"GROUP BY PVM.CDPEDIDOVENDAMATERIAL, PVM.QUANTIDADE " +
		") AS QUERY " +

		"GROUP BY CDPEDIDOVENDAMATERIAL";
		

		SinedUtil.markAsReader();
		List<Double> listaUpdate = getJdbcTemplate().query(sql, 
			new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return rs.getDouble("QTDE");
				}
			});
		
		if(listaUpdate != null && listaUpdate.size() > 0){
			return listaUpdate.get(0);
		} else {
			return null;
		}
	}
	
	public Pedidovendamaterial loadWithMaterialMestre(Pedidovendamaterial pedidovendamaterial) {
		if(pedidovendamaterial == null || pedidovendamaterial.getCdpedidovendamaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
					.removeUseWhere()
					.select("pedidovendamaterial.cdpedidovendamaterial, material.cdmaterial, materialmestregrade.cdmaterial, materialmestre.cdmaterial")
					.leftOuterJoin("pedidovendamaterial.material material")
					.leftOuterJoin("material.materialmestregrade materialmestregrade")
					.leftOuterJoin("pedidovendamaterial.materialmestre materialmestre")
					.where("pedidovendamaterial = ?", pedidovendamaterial)
					.unique();
	}
	
	public void updateFaixaMarkup(Pedidovendamaterial vm, FaixaMarkupNome faixaMarkup) {
		getHibernateTemplate().bulkUpdate("update Pedidovendamaterial vm set vm.faixaMarkupNome = ? where vm = ?", new Object[]{faixaMarkup, vm});
	}

	public Date findMaiorDataPrazoEntrega(Pedidovenda pedidovenda, Pneu pneu) {
		return newQueryBuilderSined(Date.class)
			.from(Pedidovendamaterial.class)
			.setUseTranslator(false) 
			.select("max(pedidovendamaterial.dtprazoentrega)")
			.where("pedidovendamaterial.pedidovenda = ?", pedidovenda)
			.where("pedidovendamaterial.pneu = ?", pneu)
			.unique();
	}

}