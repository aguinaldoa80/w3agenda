package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Inspecaoitem;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.InspecaoitemFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("inspecaoitem.nome")
public class InspecaoitemDAO extends GenericDAO<Inspecaoitem> {

	@Override
	public void updateListagemQuery(QueryBuilder<Inspecaoitem> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		InspecaoitemFiltro filtro = (InspecaoitemFiltro) _filtro;
		query
			.select("inspecaoitem.cdinspecaoitem, inspecaoitem.nome, inspecaoitem.ativo, inspecaoitemtipo.nome, " +
					"inspecaoitem.visual, inspecaoitem.descricao")
			.leftOuterJoin("inspecaoitem.inspecaoitemtipo inspecaoitemtipo")
			.where("inspecaoitemtipo = ?", filtro.getInspecaoitemtipo())
			.whereLikeIgnoreAll("inspecaoitem.nome", filtro.getNome())
			.where("inspecaoitem.ativo = ?", filtro.getAtivo());
	}

	/**
	 * M�todo que busca os itens utilizando whereIn
	 * 
	 * @param itens
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Inspecaoitem> findItens(List<Inspecaoitem> itens) {
		return query()
			.select("inspecaoitem.cdinspecaoitem, inspecaoitem.nome")
			.whereIn("inspecaoitem.cdinspecaoitem", CollectionsUtil.listAndConcatenate(itens, "cdinspecaoitem", ","))
			.list();
	}
	
	@Override
	public ListagemResult<Inspecaoitem> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Inspecaoitem> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("inspecaoitem.cdinspecaoitem");
		
		return new ListagemResult<Inspecaoitem>(query, filtro);
	}
}
