package br.com.linkcom.sined.geral.dao;

import java.util.Collections;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendavalorcampoextra;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VendavalorcampoextraDAO extends GenericDAO<Vendavalorcampoextra>{

	/**
	 * Busca todos os campos extras de um determinado {@link Venda}
	 * @param pedidovendatipo
	 * @return
	 */
	public List<Vendavalorcampoextra> findByVenda(Venda bean) {

		if(bean == null  || bean.getCdvenda() == null){
			return Collections.emptyList();
		}
		
		return query()
	   		 .select("vendavalorcampoextra.cdvendavalorcampoextra, vendavalorcampoextra.valor," +
	   		 		"campoextrapedidovendatipo.cdcampoextrapedidovendatipo, campoextrapedidovendatipo.nome, " +
	   		 		"campoextrapedidovendatipo.ordem, campoextrapedidovendatipo.obrigatorio")
	   		 		.join("vendavalorcampoextra.campoextrapedidovendatipo campoextrapedidovendatipo")
	   		 		.where("vendavalorcampoextra.venda = ?", bean)
	   		 		.orderBy("campoextrapedidovendatipo.ordem")
	   		 		.list();
	}
	
}
