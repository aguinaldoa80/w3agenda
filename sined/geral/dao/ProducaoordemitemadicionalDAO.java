package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemitemadicional;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoordemitemadicionalDAO extends GenericDAO<Producaoordemitemadicional> {
	
	/**
	* M�todo que deleta os campos adicionais
	*
	* @param producaoordem
	* @param whereIn
	* @return
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordemitemadicional> findForDelete(Producaoordem producaoordem, String whereIn) {
		if(whereIn == null || whereIn.equals("") || producaoordem == null || producaoordem.getCdproducaoordem() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("producaoordemitemadicional.cdproducaoordemitemadicional, producaoordem.cdproducaoordem")
					.join("producaoordemitemadicional.producaoordem producaoordem")
					.where("producaoordemitemadicional.cdproducaoordemitemadicional not in (" +whereIn+")")
					.where("producaoordem = ?", producaoordem)
					.list();
	}

	/**
	 * Busca os itens adicionais da ordem de produ��o
	 *
	 * @param producaoordem
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/01/2017
	 */
	public List<Producaoordemitemadicional> findByProducaoordem(Producaoordem producaoordem) {
		if(producaoordem == null || producaoordem.getCdproducaoordem() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("producaoordemitemadicional.cdproducaoordemitemadicional, producaoordem.cdproducaoordem, " +
							"material.cdmaterial, localarmazenagem.cdlocalarmazenagem, producaoordemitemadicional.quantidade, " +
							"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo")
					.join("producaoordemitemadicional.producaoordem producaoordem")
					.join("producaoordemitemadicional.material material")
					.leftOuterJoin("material.unidademedida unidademedida")
					.join("producaoordemitemadicional.localarmazenagem localarmazenagem")
					.where("producaoordem = ?", producaoordem)
					.list();
	}
	
	public List<Producaoordemitemadicional> findForProducaoordemEntrada(Producaoordem producaoordem) {
		if(producaoordem == null || producaoordem.getCdproducaoordem() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		QueryBuilder<Producaoordemitemadicional> query = query()
					.select("producaoordemitemadicional.cdproducaoordemitemadicional, producaoordem.cdproducaoordem, " +
							"material.nome, material.identificacao, material.cdmaterial, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, producaoordemitemadicional.quantidade, " +
							"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo")
					.join("producaoordemitemadicional.producaoordem producaoordem")
					.join("producaoordemitemadicional.material material")
					.leftOuterJoin("material.unidademedida unidademedida")
					.leftOuterJoin("producaoordemitemadicional.localarmazenagem localarmazenagem")
					.leftOuterJoin("producaoordemitemadicional.pneu pneu")
					.where("producaoordem = ?", producaoordem);
		query = SinedUtil.setJoinsByComponentePneu(query);
		return query.list();
	}
}
