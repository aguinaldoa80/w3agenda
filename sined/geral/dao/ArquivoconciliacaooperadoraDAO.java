package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Arquivoconciliacaooperadora;
import br.com.linkcom.sined.geral.bean.enumeration.EnumArquivoConciliacaoOperadoraStatusPagamento;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.ArquivoconciliacaooperadoraFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;


public class ArquivoconciliacaooperadoraDAO extends GenericDAO<Arquivoconciliacaooperadora> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Arquivoconciliacaooperadora> query,FiltroListagem _filtro) {
		ArquivoconciliacaooperadoraFiltro filtro = (ArquivoconciliacaooperadoraFiltro)_filtro;
		
		query.select("arquivoconciliacaooperadora.cdarquivoconciliacaooperadora, arquivoconciliacaooperadora.dataapresentacao, arquivoconciliacaooperadora.tipotransacao, arquivoconciliacaooperadora.numeroro, arquivoconciliacaooperadora.nsudoc, " +
				"arquivoconciliacaooperadora.tid, arquivoconciliacaooperadora.datavenda, arquivoconciliacaooperadora.valorvenda, arquivoconciliacaooperadora.totalparcela, arquivoconciliacaooperadora.dataprevistapagamento, arquivoconciliacaooperadora.empresaestabelecimento, " +
				"arquivoconciliacaooperadora.statuspagamento, arquivoconciliacaooperadora.valorliquido, arquivoconciliacaooperadora.valorbruto, taxaparcela.numeroparcelas, taxaparcela.taxa, documento.cddocumento, documento.valor, " +
				"documentoacao.cddocumentoacao, documentoacao.nome")
		.leftOuterJoin("arquivoconciliacaooperadora.taxaparcela taxaparcela")
		.leftOuterJoin("arquivoconciliacaooperadora.documento documento")
		.leftOuterJoin("documento.documentoacao documentoacao")
		.leftOuterJoin("arquivoconciliacaooperadora.empresaestabelecimento empresaestabelecimento");
		if (filtro.getEstabelecimento() != null) {
			query
			.where("empresaestabelecimento = ?", filtro.getEstabelecimento())
			.where("empresaestabelecimento.estabelecimento = arquivoconciliacaooperadora.estabelecimento");
		}
		query
		.where("arquivoconciliacaooperadora.dataapresentacao >= ?", filtro.getDataapresentacaoincio())
		.where("arquivoconciliacaooperadora.dataapresentacao <= ?", filtro.getDataapresentacaofim())
		.where("arquivoconciliacaooperadora.numeroro = ?", filtro.getNumeroro())
		.where("arquivoconciliacaooperadora.nsudoc = ?", filtro.getNsudoc())
		.where("arquivoconciliacaooperadora.tid = ?", filtro.getTid())
		.where("arquivoconciliacaooperadora.valorvenda = ?", filtro.getValorvenda())
		.where("arquivoconciliacaooperadora.datavenda >= ?", filtro.getDatavendaincio())
		.where("arquivoconciliacaooperadora.datavenda <= ?", filtro.getDatavendafim())
		.where("arquivoconciliacaooperadora.totalparcela = ?", filtro.getTotalparcela())
		.where("arquivoconciliacaooperadora.dataprevistapagamento >= ?", filtro.getDataprevistapagamentoinicio())
		.where("arquivoconciliacaooperadora.dataprevistapagamento <= ?", filtro.getDataprevistapagamentofim())
		.where("arquivoconciliacaooperadora.statuspagamento = ?", filtro.getStatuspagamento())
		.where("arquivoconciliacaooperadora.cdarquivoconciliacaooperadora = ?", filtro.getCdarquivoconciliacaooperadora())
		.where("arquivoconciliacaooperadora.tipotransacao = ?", filtro.getTipotransacao());
	}
	
	public Arquivoconciliacaooperadora findForAtulizaStatusPagamento(Arquivoconciliacaooperadora arquivoconciliacaooperadora){
		QueryBuilder<Arquivoconciliacaooperadora> query = query();
		query
		.openParentheses()
			.where("arquivoconciliacaooperadora.datavenda = ?",arquivoconciliacaooperadora.getDatavenda());
		if (StringUtils.isNotBlank(arquivoconciliacaooperadora.getTid())) {
			query
			 .where("arquivoconciliacaooperadora.tid = ?", arquivoconciliacaooperadora.getTid());
		}
		query
		.closeParentheses()
		.or()
		.openParentheses();
		query
			.where("arquivoconciliacaooperadora.nsudoc = ?", arquivoconciliacaooperadora.getNsudoc())
			.where("arquivoconciliacaooperadora.numeroro = ?", arquivoconciliacaooperadora.getNumeroro())
		.closeParentheses();
		return query.unique();
	}
	
	public void associaArquivoConciliacaoAutomatica(Arquivoconciliacaooperadora arquivoconciliacaooperadora){
		String sql = "update arquivoconciliacaooperadora " 
			+ "set cddocumento = ? "
			+ "where cdarquivoconciliacaooperadora = ?";
		Object[] fields = new Object[] {
				arquivoconciliacaooperadora.getDocumento().getCddocumento(),
				arquivoconciliacaooperadora.getCdarquivoconciliacaooperadora()};
		getJdbcTemplate().update(sql, fields);
	}
	
	public void atualizaStatusPagamento(Arquivoconciliacaooperadora arquivoconciliacaooperadoraVenda, Arquivoconciliacaooperadora arquivoconciliacaooperadoraPagamenmto){
		String sql = "update arquivoconciliacaooperadora " 
			+ "set statuspagamento = ? "
			+ "where cdarquivoconciliacaooperadora = ?";
		Object[] fields = new Object[] {
				arquivoconciliacaooperadoraPagamenmto.getStatuspagamento().getCodigo(),
				arquivoconciliacaooperadoraVenda.getCdarquivoconciliacaooperadora()};
		getJdbcTemplate().update(sql, fields);
	}
	
	public List<Arquivoconciliacaooperadora> findForCalculoValorTotalLiquidoVenda(ArquivoconciliacaooperadoraFiltro filtro) {
		QueryBuilder<Arquivoconciliacaooperadora> query = query();
		this.updateListagemQuery(query, filtro);
		return query.list();
	}
	
	public Arquivoconciliacaooperadora findArquivoForAssociacaoArquivoOperadora(Integer cdarquivoconciliacaooperadora){
		if (cdarquivoconciliacaooperadora == null) {
			throw new SinedException("Par�metro Inv�lido");
		}
		return query()
		.select("arquivoconciliacaooperadora.cdarquivoconciliacaooperadora, arquivoconciliacaooperadora.dataprevistapagamento, " +
				"arquivoconciliacaooperadora.datavenda, arquivoconciliacaooperadora.dataenviobanco ")
		.where("arquivoconciliacaooperadora.cdarquivoconciliacaooperadora = ?", cdarquivoconciliacaooperadora)
		.unique();
	}
	
	public Arquivoconciliacaooperadora findArquivoForConciliacaoArquivoOperadora(Integer cdarquivoconciliacaooperadora){
		if (cdarquivoconciliacaooperadora == null) {
			throw new SinedException("Par�metro Inv�lido");
		}
		return query()
		.select("arquivoconciliacaooperadora.cdarquivoconciliacaooperadora, arquivoconciliacaooperadora.dataapresentacao, " +
				"arquivoconciliacaooperadora.valorbruto, " +
				"arquivoconciliacaooperadora.datavenda, arquivoconciliacaooperadora.dataprevistapagamento, arquivoconciliacaooperadora.dataenviobanco, " +
				"documento.cddocumento, documentoacao.cddocumentoacao, documentoacao.nome ")
		.leftOuterJoin("arquivoconciliacaooperadora.documento documento ")
		.leftOuterJoin("documento.documentoacao documentoacao ")
		.where("arquivoconciliacaooperadora.cdarquivoconciliacaooperadora = ?", cdarquivoconciliacaooperadora)
		.unique();
	}
	
	public boolean isArquivoConciliacaoOperadoraPago(Integer cdarquivoconciliacaooperadora){
		if (cdarquivoconciliacaooperadora == null) {
			throw new SinedException("Par�metro Inv�lido");
		}
		
		return newQueryBuilderSined(Long.class)
		.select("count(*)")
		.from(Arquivoconciliacaooperadora.class)
		.where("arquivoconciliacaooperadora.statuspagamento = ?", EnumArquivoConciliacaoOperadoraStatusPagamento.PAGO)
		.where("arquivoconciliacaooperadora.cdarquivoconciliacaooperadora = ?", cdarquivoconciliacaooperadora)
		.unique() > 0;
		
	}

	public boolean haveArquivoconciliacaooperadoraByNsudoc(Arquivoconciliacaooperadora arquivoconciliacaooperadora) {
		return  newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Arquivoconciliacaooperadora.class)
					.where("arquivoconciliacaooperadora.nsudoc = ?", arquivoconciliacaooperadora.getNsudoc())
					.unique() > 0;
	}
}
