package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Producaoetapa;
import br.com.linkcom.sined.geral.bean.Producaoetapaitem;
import br.com.linkcom.sined.modulo.producao.controller.crud.filter.ProducaoetapaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoetapaDAO extends GenericDAO<Producaoetapa> {

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaProducaoetapaitem");
		super.updateSaveOrUpdate(save);
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Producaoetapa> query) {
		query
			.select("producaoetapa.cdproducaoetapa, producaoetapa.nome, producaoetapa.corteproducao, producaoetapa.baixarEstoqueMateriaprima, producaoetapa.baixarmateriaprimacascata,producaoetapa.ordemprioridade," +
					"listaProducaoetapaitem.cdproducaoetapaitem, producaoetapanome.cdproducaoetapanome, producaoetapanome.nome, listaProducaoetapaitem.ordem, " +
					"producaoetapa.cdusuarioaltera, producaoetapa.dtaltera, producaoetapa.naoagruparmaterial, producaoetapa.naogerarentradaconclusao, " +
					"departamento.cddepartamento,  listaProducaoetapaitem.qtdediasprevisto, listaProducaoetapaitem.percentualComissao ")
			.leftOuterJoin("producaoetapa.listaProducaoetapaitem listaProducaoetapaitem")
			.leftOuterJoin("listaProducaoetapaitem.departamento departamento")
			.leftOuterJoin("listaProducaoetapaitem.producaoetapanome producaoetapanome")
			.orderBy("listaProducaoetapaitem.ordem");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Producaoetapa> query,	FiltroListagem _filtro) {
		ProducaoetapaFiltro filtro = (ProducaoetapaFiltro) _filtro;
		
		query
			.select("producaoetapa.cdproducaoetapa, producaoetapa.nome")
			.whereLikeIgnoreAll("producaoetapa.nome", filtro.getNome())
			.orderBy("producaoetapa.nome");
			
	}
	
	/**
	 * M�todo que carrega a etapa de produ��o
	 *
	 * @param producaoetapa
	 * @return
	 * @author Luiz Fernando
	 */
	public Producaoetapa loadWithList(Producaoetapa producaoetapa){
		if(producaoetapa == null || producaoetapa.getCdproducaoetapa() == null)
			throw new SinedException("Par�metro n�o pode ser nulo.");
		
		return query()
				.select("producaoetapa.cdproducaoetapa, producaoetapa.nome, listaProducaoetapaitem.cdproducaoetapaitem, " +
						"producaoetapanome.cdproducaoetapanome, producaoetapanome.nome, listaProducaoetapaitem.ordem")
				.leftOuterJoin("producaoetapa.listaProducaoetapaitem listaProducaoetapaitem")
				.leftOuterJoin("listaProducaoetapaitem.producaoetapanome producaoetapanome")
				.orderBy("listaProducaoetapaitem.ordem")
				.unique();
	}

	/**
	 * Verifica se � a �ltima etapa.
	 *
	 * @param producaoetapaitem
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/03/2014
	 */
	public boolean isUltimaEtapa(Producaoetapaitem producaoetapaitem) {
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.from(Producaoetapaitem.class)
					.where("producaoetapaitem.producaoetapa = ?", producaoetapaitem.getProducaoetapa())
					.where("producaoetapaitem.ordem > ?", producaoetapaitem.getOrdem())
					.where("producaoetapaitem <> ?", producaoetapaitem)
					.unique() == 0;
	}
	
	public Producaoetapa loadBaixarEstoqueMateriaprima(Producaoetapa producaoetapa){
		return query()
				.select("producaoetapa.cdproducaoetapa, producaoetapa.baixarEstoqueMateriaprima, producaoetapa.baixarmateriaprimacascata")
				.where("producaoetapa = ?", producaoetapa)
				.unique();
	}
	
	/**
	 * M�todo que busca a lista para a sincroniza��o do W3produ��o
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/09/2016
	 */
	public List<Producaoetapa> findForW3Producao(String whereIn){
		return query()
					.select("producaoetapa.cdproducaoetapa, producaoetapa.nome, producaoetapa.baixarEstoqueMateriaprima, producaoetapa.baixarmateriaprimacascata, producaoetapa.ordemprioridade")
					.whereIn("producaoetapa.cdproducaoetapa", whereIn)
					.list();
	}

	/**
	 * For�a a sincroniza��o das tabelas do w3produ��o
	 *
	 * @param cdproducaoetapa
	 * @author Rodrigo Freitas
	 * @since 28/09/2016
	 */
	public void atualizaW3producao(Integer cdproducaoetapa) {
		SinedUtil.desmarkAsReader();
		getJdbcTemplate().execute("update producaoetapa set nome = nome where cdproducaoetapa = " + cdproducaoetapa);
	}
	
	public Boolean validaOrdemPrioridade(Producaoetapa bean){
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.from(Producaoetapa.class)
				.where("ordemprioridade = ?", bean.getOrdemprioridade())
				.where("producaoetapa <> ? ", bean, bean.getCdproducaoetapa() != null)
				.unique() > 0;
	}
}
