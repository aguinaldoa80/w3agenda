package br.com.linkcom.sined.geral.dao;


import java.util.List;

import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.view.Vwrelatoriooverhead;
import br.com.linkcom.sined.modulo.faturamento.controller.process.filter.RelacaoOverheadFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VwrelatoriooverheadDAO extends GenericDAO<Vwrelatoriooverhead> {

	/**
	 * Busca a listagem do relat�rio da rela��o overhead/faturamento.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Vwrelatoriooverhead> findForRelatorio(RelacaoOverheadFiltro filtro) {
		if(filtro == null){
			throw new SinedException("Filtro n�o pode ser nulo.");
		}
		return query()	
					.select("centrocusto.cdcentrocusto, vwrelatoriooverhead.valor, vwrelatoriooverhead.dtref")
					.join("vwrelatoriooverhead.centrocusto centrocusto")
					.join("vwrelatoriooverhead.empresa empresa")
					.join("vwrelatoriooverhead.tipooperacao tipooperacao")
					.where("empresa = ?", filtro.getEmpresa())
					.where("vwrelatoriooverhead.dtref >= ?", filtro.getDtref1())
					.where("vwrelatoriooverhead.dtref <= ?", filtro.getDtref2())
					.where("tipooperacao = ?", Tipooperacao.TIPO_DEBITO)
					.orderBy("vwrelatoriooverhead.dtref")
					.list();
	}

	/**
	 * Busca a listagem do faturamento l�quido.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Vwrelatoriooverhead> findFaturamentoLiquido(RelacaoOverheadFiltro filtro) {
		if(filtro == null){
			throw new SinedException("Filtro n�o pode ser nulo.");
		}
		return query()	
					.select("centrocusto.cdcentrocusto, vwrelatoriooverhead.valor, vwrelatoriooverhead.dtref")
					.join("vwrelatoriooverhead.centrocusto centrocusto")
					.join("vwrelatoriooverhead.empresa empresa")
					.join("vwrelatoriooverhead.tipooperacao tipooperacao")
					.where("empresa = ?", filtro.getEmpresa())
					.where("vwrelatoriooverhead.dtref >= ?", filtro.getDtref1())
					.where("vwrelatoriooverhead.dtref <= ?", filtro.getDtref2())
					.where("tipooperacao = ?", Tipooperacao.TIPO_CREDITO)
					.orderBy("vwrelatoriooverhead.dtref")
					.list();
	}

	

}





