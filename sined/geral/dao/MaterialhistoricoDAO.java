package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialhistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialhistoricoDAO extends GenericDAO<Materialhistorico> {
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Materialhistorico> query) {
		query.select("materialhistorico.nome, materialhistorico.ncmcompleto, materialhistorico.valorvenda, " +
					 "materialhistorico.valorVendaMinimo, materialhistorico.codigobarras, materialhistorico.vendapromocional, " +
					 "materialhistorico.ativo, materialhistorico.vendaecf, unidademedida.cdunidademedida, unidademedida.nome, " +
					 "materialgrupo.cdmaterialgrupo, materialgrupo.nome")
			 .leftOuterJoin("materialhistorico.unidademedida unidademedida")
			 .leftOuterJoin("materialhistorico.materialgrupo materialgrupo");
	}
	
	/**
	 * Busca os históricos pelo material.
	 *
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/04/2013
	 */
	public List<Materialhistorico> findByMaterial(Material material) {
		return query()
					.where("materialhistorico.material = ?", material)
					.orderBy("materialhistorico.dtaltera desc")
					.list();
	}

}
