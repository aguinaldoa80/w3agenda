package br.com.linkcom.sined.geral.dao;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Propostarevisao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PropostarevisaoDAO extends GenericDAO<Propostarevisao>{

	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				arquivoDAO.saveFile(save.getEntity(), "arquivoPTM");
				arquivoDAO.saveFile(save.getEntity(), "arquivoPCM");
				return null;
			}
		});
	}

}
