package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.Notaerrocorrecao;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Notaerrocorrecaotipo;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.VerificaNotaErroCorrecaoBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.NotaErroCorrecaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotaerrocorrecaoDAO extends GenericDAO<Notaerrocorrecao> {

	@Override
	public void updateEntradaQuery(QueryBuilder<Notaerrocorrecao> query) {
		query
		.select("notaerrocorrecao.cdnotaerrocorrecao, notaerrocorrecao.tipo, notaerrocorrecao.prefixo, notaerrocorrecao.codigo,  " +
				"notaerrocorrecao.codigo, notaerrocorrecao.url, notaerrocorrecao.mensagem, notaerrocorrecao.correcao," +
				"listaNotaErroCorrecaoHistorico.cdnotaerrocorrecaoHistorico, listaNotaErroCorrecaoHistorico.cdnotaerrocorrecaoHistorico, " +
				"listaNotaErroCorrecaoHistorico.acaoexecutada, listaNotaErroCorrecaoHistorico.responsavel, listaNotaErroCorrecaoHistorico.observacao, listaNotaErroCorrecaoHistorico.dataaltera")
		.leftOuterJoin("notaerrocorrecao.listaNotaErroCorrecaoHistorico listaNotaErroCorrecaoHistorico")
		.orderBy("listaNotaErroCorrecaoHistorico.dataaltera desc");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Notaerrocorrecao> query, FiltroListagem _filtro) {
		NotaErroCorrecaoFiltro filtro = (NotaErroCorrecaoFiltro) _filtro;
		
		query.
			select("notaerrocorrecao.cdnotaerrocorrecao, notaerrocorrecao.tipo, notaerrocorrecao.prefixo, " +
					"notaerrocorrecao.codigo, notaerrocorrecao.url, notaerrocorrecao.mensagem, notaerrocorrecao.correcao")
			.whereLikeIgnoreAll("notaerrocorrecao.codigo", filtro.getCodigo())
			.whereLikeIgnoreAll("notaerrocorrecao.prefixo", filtro.getPrefixo())
			.where("notaerrocorrecao.tipo = ?", filtro.getTipo())
			.orderBy("notaerrocorrecao.codigo asc");
	}

	@SuppressWarnings("unchecked")
	public List<Notaerrocorrecao> findByNota(Nota nota, Notaerrocorrecaotipo notaerrocorrecaotipo, String prefixo) {
		String sql = " SELECT NEC.CORRECAO AS CORRECAO " +
					 " FROM ARQUIVONFNOTA ANF LEFT JOIN ARQUIVONFNOTAERRO ENF ON (ANF.CDARQUIVONFNOTA = ENF.CDARQUIVONFNOTA) " +
					 " JOIN NOTAERROCORRECAO NEC ON (NEC.CODIGO = ENF.CODIGO) " +
					 " WHERE ANF.CDNOTA = " + nota.getCdNota() +
					 " AND NEC.CORRECAO IS NOT NULL " +
					 " AND NEC.TIPO = " + notaerrocorrecaotipo.ordinal() +
					 (StringUtils.isNotBlank(prefixo) ? " AND NEC.PREFIXO = '" + prefixo + "' " : " AND COALESCE(NEC.PREFIXO, '') = '' ") +
					 " AND ANF.CDARQUIVONF = (" +
					 "		SELECT MAX(ARQUIVONFNOTA.CDARQUIVONF) " +
					 "		FROM ARQUIVONFNOTA " +
			         "		JOIN ARQUIVONF ON ARQUIVONF.CDARQUIVONF = ARQUIVONFNOTA.CDARQUIVONF " +
			         "		WHERE CDNOTA = ANF.CDNOTA " +
			         "		AND ARQUIVONF.ARQUIVONFSITUACAO = " + Arquivonfsituacao.PROCESSADO_COM_ERRO.ordinal() +
			         ") ";

		SinedUtil.markAsReader();
		List<Notaerrocorrecao> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Notaerrocorrecao(rs.getString("CORRECAO"));
			}
		});
		return list;
	}
	
	public boolean existeCodigoPrefixoNotaerrocorrecao (Notaerrocorrecao notaerrocorrecao) {
		return new QueryBuilder<Long>(getHibernateTemplate()).from(Notaerrocorrecao.class)
				.select("count(*)")
				.where("notaerrocorrecao.codigo = ?", notaerrocorrecao.getCodigo())
				.where("notaerrocorrecao.prefixo = ?", notaerrocorrecao.getPrefixo())
				.unique() > 0;
	}
	
	public Notaerrocorrecao loadNotaErroCorrecao(VerificaNotaErroCorrecaoBean bean) {
		if(bean.getCodigo() == null || "".equals(bean.getCodigo())){
			throw new SinedException("Parāmetros incorretos!");
		}
		return querySined()
				.setUseReadOnly(true)
				.select("notaerrocorrecao.mensagem, notaerrocorrecao.correcao, notaerrocorrecao.url")
				.where("notaerrocorrecao.codigo = ?", bean.getCodigo())
				.where("notaerrocorrecao.tipo = 0")
				.unique();	
	}
	
}

