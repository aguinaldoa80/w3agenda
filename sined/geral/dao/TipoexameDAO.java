package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Tipoexame;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TipoexameDAO extends GenericDAO<Tipoexame> {

	@Override
	public void updateListagemQuery(QueryBuilder<Tipoexame> query, FiltroListagem _filtro) {
//		EtniaFiltro filtro = (EtniaFiltro) _filtro;
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Tipoexame> query) {
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
	}
	

	/* singleton */
	private static TipoexameDAO instance;
	public static TipoexameDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(TipoexameDAO.class);
		}
		return instance;
	}
}