package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Grupoquestionario;
import br.com.linkcom.sined.geral.bean.Questionario;
import br.com.linkcom.sined.geral.bean.Questionarioquestao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class QuestionarioquestaoDAO extends GenericDAO<Questionarioquestao>{
	

	/**
	 * 
	 * M�todo que carrega o Bean de Questionarioquestao
	 *
	 *@author Thiago Augusto
	 *@date 06/03/2012
	 * @param bean
	 * @return
	 */
	public Questionarioquestao carregarQuestionarioQuestao(Questionarioquestao bean){
		return query()
					.select("questionarioquestao.cdquestionarioquestao, questionarioquestao.questao, questionarioquestao.tiporesposta, " +
							"questionarioquestao.pontuacaosim, questionarioquestao.pontuacaonao, questionario.cdquestionario, " +
							"questionarioquestao.pontuacaoquestao ")
					.leftOuterJoin("questionarioquestao.questionario questionario")
					.where("questionarioquestao = ?", bean)
					.unique();
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaquestionarioresposta");
	}

	public List<Questionarioquestao> loadQuestoes(Questionario questionario){
		return query()
					.where("questionarioquestao.questionario = ?", questionario)
					.list();
	}
	
	public List<Questionarioquestao> carregarQuestionarioQuestaoForGrupoquestionario(Grupoquestionario grupoquestionario){
		return query()
					.select("questionarioquestao.cdquestionarioquestao, questionarioquestao.questao, questionarioquestao.tiporesposta, " +
							"questionarioquestao.pontuacaosim, questionarioquestao.pontuacaonao, grupoquestionario.cdgrupoquestionario, " +
							"questionarioquestao.pontuacaoquestao ")
					.leftOuterJoin("questionarioquestao.grupoquestionario grupoquestionario")
					.where("grupoquestionario = ?", grupoquestionario)
					.list();
	}
	
	public void findForDelete(String whereIn, Grupoquestionario grupoquestionario){
		if (grupoquestionario == null || grupoquestionario.getCdgrupoquestionario() == null) {
			throw new SinedException("Grupo questionario n�o pode ser nulo.");
		}
		
		StringBuilder query = new StringBuilder();
		
		query.append("delete Questionarioquestao qq ");
		query.append("where qq.grupoquestionario.id = " + grupoquestionario.getCdgrupoquestionario() + " ");
		if (StringUtils.isNotBlank(whereIn)) {
			query.append("and qq.id not in ("+whereIn+") ");
		}
		getHibernateTemplate().bulkUpdate(query.toString());
	}
	
	public List<Questionarioquestao> carregarListaQuestionarioQuestao(String whereIn){
		return query()
					.select("questionarioquestao.cdquestionarioquestao, questionarioquestao.questao, questionarioquestao.tiporesposta, " +
							"questionarioquestao.pontuacaosim, questionarioquestao.pontuacaonao, questionario.cdquestionario, " +
							"questionarioquestao.pontuacaoquestao, listaquestionarioresposta.cdquestionarioresposta, listaquestionarioresposta.resposta, " +
							"listaquestionarioresposta.pontuacao")
					.leftOuterJoin("questionarioquestao.questionario questionario")
					.leftOuterJoin("questionarioquestao.listaquestionarioresposta listaquestionarioresposta")
					.whereIn("questionarioquestao.cdquestionarioquestao", whereIn)
					.list();
	}
}
