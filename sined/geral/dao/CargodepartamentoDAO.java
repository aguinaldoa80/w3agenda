package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cargodepartamento;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CargodepartamentoDAO extends GenericDAO<Cargodepartamento> {

	/* singleton */
	private static CargodepartamentoDAO instance;
	public static CargodepartamentoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(CargodepartamentoDAO.class);
		}
		return instance;
	}

	public boolean haveCargoDepartamento(Cargo cargo, Departamento departamento) {
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.from(Cargodepartamento.class)
					.where("cargodepartamento.cargo = ?", cargo)
					.where("cargodepartamento.departamento = ?", departamento)
					.unique() > 0;
	}
}