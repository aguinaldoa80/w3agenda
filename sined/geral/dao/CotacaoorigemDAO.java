package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Cotacaoorigem;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CotacaoorigemDAO extends GenericDAO<Cotacaoorigem> {

	
	/**
	 * Carrega a lista de COtacaoorigem a partir de uma cotacao.
	 * 
	 * @param cotacao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cotacaoorigem> findByCotacao(Cotacao cotacao){
		if (cotacao == null || cotacao.getCdcotacao() == null) {
			throw new SinedException("Cotacao n�o pode ser nulo.");
		}
		return query()
					.select("cotacaoorigem.cdcotacaoorigem, solicitacaocompra.cdsolicitacaocompra, " +
							"projeto.cdprojeto, centrocusto.cdcentrocusto")
					.leftOuterJoin("cotacaoorigem.solicitacaocompra solicitacaocompra")
					.leftOuterJoin("solicitacaocompra.projeto projeto")
					.leftOuterJoin("solicitacaocompra.centrocusto centrocusto")
					.where("cotacaoorigem.cotacao = ?",cotacao)
					.list();
	}

	/**
	 * Busca todas as solicita��es (origens) da cota��o
	 * 
	 * @param cotacao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Cotacaoorigem> findSolicitacoesByCotacao(Cotacao cotacao) {
		if (cotacao == null || cotacao.getCdcotacao() == null) {
			throw new SinedException("Cotacao n�o pode ser nula.");
		}
		return query()
			.select("cotacaoorigem.cdcotacaoorigem, solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.qtde, " +
					"solicitacaocompra.faturamentocliente, material.cdmaterial, localarmazenagem.cdlocalarmazenagem, " +
					"frequencia.cdfrequencia, solicitacaocompra.qtdefrequencia, unidademedida.cdunidademedida, solicitacaocompra.observacao, " +
					"materialgrupo.cdmaterialgrupo, materialgrupo.nome, contagerencial.cdcontagerencial, contagerencial.nome, contagerencialMaterial.cdcontagerencial, contagerencialMaterial.nome, " +
					"unidademedidamaterial.cdunidademedida, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, centrocusto.cdcentrocusto, projeto.cdprojeto, " +
					"solicitacaocompra.altura, solicitacaocompra.comprimento, solicitacaocompra.largura, solicitacaocompra.pesototal, solicitacaocompra.qtdvolume")
			.leftOuterJoin("cotacaoorigem.solicitacaocompra solicitacaocompra")
			.leftOuterJoin("solicitacaocompra.localarmazenagem localarmazenagem")
			.leftOuterJoin("solicitacaocompra.material material")
			.leftOuterJoin("material.unidademedida unidademedidamaterial")
			.leftOuterJoin("solicitacaocompra.frequencia frequencia")
			.leftOuterJoin("solicitacaocompra.materialgrupo materialgrupo")
			.leftOuterJoin("solicitacaocompra.contagerencial contagerencial")
			.leftOuterJoin("solicitacaocompra.unidademedida unidademedida")
			.leftOuterJoin("solicitacaocompra.projeto projeto")
			.leftOuterJoin("solicitacaocompra.centrocusto centrocusto")
			.leftOuterJoin("solicitacaocompra.empresa empresa")
			.leftOuterJoin("material.contagerencial contagerencialMaterial")
			.where("cotacaoorigem.cotacao = ?",cotacao)
			.list();
	}
	
	/**
	 * Carrega a lista de COtacaoorigem a partir de cotac�es.
	 * 
	 * @param whereIn
	 * @return
	 * @author Thiago Clemente
	 * 
	 */
	public List<Cotacaoorigem> findByCotacao(String whereIn){
		return query()
					.select("cotacao.cdcotacao, " +
							"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.cnpj, empresa.inscricaoestadual, logomarca.cdarquivo, logomarca.tamanho," +
							"empresaCotacao.cdpessoa, empresaCotacao.nome, empresaCotacao.razaosocial, empresaCotacao.cnpj, empresaCotacao.inscricaoestadual, " +
							"logomarcaCotacao.cdarquivo, logomarcaCotacao.tamanho")
					.join("cotacaoorigem.cotacao cotacao")
					.join("cotacaoorigem.solicitacaocompra solicitacaocompra")
					.leftOuterJoin("solicitacaocompra.empresa empresa")
					.leftOuterJoin("empresa.logomarca logomarca")
					.leftOuterJoin("cotacao.empresa empresaCotacao")
					.leftOuterJoin("empresaCotacao.logomarca logomarcaCotacao")
					.whereIn("cotacao.cdcotacao", whereIn)
					.list();
	}

	/**
	 * Deleta as cota��es origem das solicita�oes de compra n�o cotadas
	 * @param whereInCdSolicitacao
	 * @param cdcotacao
	 */
	public void deleteCotacaoOrigem(String whereInCdSolicitacao,Integer cdcotacao) {
		if(StringUtils.isNotEmpty(whereInCdSolicitacao) && cdcotacao != null){
			String sql = "DELETE from COTACAOORIGEM WHERE cdsolicitacaocompra IN (" + whereInCdSolicitacao +") AND cdcotacao = "+cdcotacao+ ";"; 
			getJdbcTemplate().execute(sql);
		}
	}
}
