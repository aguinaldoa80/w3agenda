package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Codigocnae;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CodigocnaeFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CodigocnaeDAO extends GenericDAO<Codigocnae> {

	@Override
	public void updateListagemQuery(QueryBuilder<Codigocnae> query, FiltroListagem _filtro) {	
		CodigocnaeFiltro filtro = (CodigocnaeFiltro) _filtro;
		
		query
			.whereLikeIgnoreAll("codigocnae.cnae", filtro.getCnae())
			.whereLikeIgnoreAll("codigocnae.descricaocnae", filtro.getDescricaocnae());
	}

	/**
	 * M�todo que busca o CNAE pelo c�digo
	 *
	 * @param cnae
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Codigocnae> findByCodigocnae(String cnae) {
		if(cnae == null || "".equals(cnae))
			throw new SinedException("C�digo cnae inv�lido.");
		
		return query()
				.select("codigocnae.cdcodigocnae, codigocnae.cnae")
				.where("translate(codigocnae.cnae, '-/', '') like ?", cnae)
				.list();
	}

	public List<Codigocnae> findByCnae(String cnae) {
		return query()
				.whereLikeIgnoreAll("cnae", cnae).or().whereLikeIgnoreAll("descricaocnae", cnae)
				.list();
	}
	
}
