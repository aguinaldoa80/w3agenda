package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Planejamentorecursogeral;
import br.com.linkcom.sined.geral.bean.Planejamentosituacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PlanejamentorecursogeralDAO extends GenericDAO<Planejamentorecursogeral>{

	/**
	 * Carrega a lista de recurso gerais de uma planejamento;
	 *
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Planejamentorecursogeral> findByPlanejamento(Planejamento planejamento) {
		if (planejamento == null || planejamento.getCdplanejamento() == null) {
			throw new SinedException("Planejamento n�o pode ser nulo.");
		}
		return query()
					.select("planejamentorecursogeral.qtde, material.cdmaterial, material.valorvenda")
					.join("planejamentorecursogeral.material material")
					.join("planejamentorecursogeral.planejamento planejamento")
					.where("planejamento = ?",planejamento)
					.list();
	}

	/**
	 * Carrega a lista de recursos gerais de planejamento para o relat�rio de or�amento.
	 *
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Planejamentorecursogeral> findForOrcamento(Planejamento planejamento) {
		if (planejamento == null || planejamento.getCdplanejamento() == null) {
			throw new SinedException("Planejamento n�o pode ser nulo.");
		}
		return query()
					.select("vcontagerencial.identificador, contagerencial.nome, tipooperacao.nome, " +
							"material.valorvenda, material.nome, material.cdmaterial, planejamentorecursogeral.qtde")
					.join("planejamentorecursogeral.material material")
					.leftOuterJoin("material.contagerencial contagerencial")
					.leftOuterJoin("contagerencial.tipooperacao tipooperacao")
					.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
					.join("planejamentorecursogeral.planejamento planejamento")
					.where("planejamento = ?",planejamento)
					.list();
	}

	/**
	 * Carrega a lista de <code>Planejamentorecursogeral</code> para a gera��o das solicita��es de compra para um planejamento.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Planejamentorecursogeral> findByPlanejamento(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metro.");			
		}
		return query()
					.select("planejamentorecursogeral.cdplanejamentorecursogeral, planejamentorecursogeral.qtde, material.cdmaterial, material.nome, material.valorcusto, " +
							"projeto.cdprojeto, projeto.nome, centrocusto.cdcentrocusto, planejamento.cdplanejamento, planejamento.descricao, planejamento.dtinicio, unidademedida.cdunidademedida, unidademedida.simbolo, " +
							"listaSolicitacaocompraorigem.cdsolicitacaocompraorigem," +
							"listaOrdemcompramaterial.cdordemcompramaterial," +
							"listaRequisicaoorigem.cdrequisicaoorigem ")
					.join("planejamentorecursogeral.material material")
					.join("material.unidademedida unidademedida")
					.join("planejamentorecursogeral.planejamento planejamento")
					.join("planejamento.projeto projeto")
					.leftOuterJoin("projeto.centrocusto centrocusto")
					.leftOuterJoin("planejamentorecursogeral.listaSolicitacaocompraorigem listaSolicitacaocompraorigem")
					.leftOuterJoin("planejamentorecursogeral.listaOrdemcompramaterial listaOrdemcompramaterial")
					.leftOuterJoin("planejamentorecursogeral.listaRequisicaoorigem listaRequisicaoorigem")
					.whereIn("planejamento.cdplanejamento", whereIn)
					.list();
	}

	/**
	* M�todo que busca os recursos gerais do planejamento
	*
	* @param whereIn
	* @return
	* @since 15/03/2016
	* @author Luiz Fernando
	*/
	public List<Planejamentorecursogeral> findForBloquearQtdeCompra(String whereInProjeto, String whereInMaterial) {
		if(StringUtils.isBlank(whereInProjeto)){
			throw new SinedException("Par�metro inv�lido.");			
		}
		
		return query()
			.select("planejamentorecursogeral.cdplanejamentorecursogeral, planejamentorecursogeral.qtde, material.cdmaterial, material.nome, projeto.cdprojeto ")
			.join("planejamentorecursogeral.material material")
			.join("planejamentorecursogeral.planejamento planejamento")
			.join("planejamento.projeto projeto")
			.whereIn("projeto.cdprojeto", whereInProjeto)
			.where("planejamento.planejamentosituacao <> ?", Planejamentosituacao.CANCELADA)
			.whereIn("material.cdmaterial", whereInMaterial)
			.list();
	}
	

	
}
