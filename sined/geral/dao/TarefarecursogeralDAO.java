package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Tarefarecursogeral;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TarefarecursogeralDAO extends GenericDAO<Tarefarecursogeral>{

	/**
	 * Carrega a lista de recursos geral da tarefa de um planejamento passado por par�metro.
	 *
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefarecursogeral> findByPlanejamento(Planejamento planejamento) {
		if (planejamento == null || planejamento.getCdplanejamento() == null) {
			throw new SinedException("Planejamento n�o pode ser nulo.");
		}
		return query()
					.select("tarefarecursogeral.qtde, material.cdmaterial, material.valorvenda")
					.join("tarefarecursogeral.material material")
					.join("tarefarecursogeral.tarefa tarefa")
					.join("tarefa.planejamento planejamento")
					.where("planejamento = ?",planejamento)
					.list();
	}

	/**
	 * Carrega a lista de recursos gerais de tarefa para o relat�rio de or�amento.
	 *
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefarecursogeral> findForOrcamento(Planejamento planejamento) {
		if (planejamento == null || planejamento.getCdplanejamento() == null) {
			throw new SinedException("Planejamento n�o pode ser nulo.");
		}
		return query()
					.select("vcontagerencial.identificador, contagerencial.nome, tipooperacao.nome, " +
							"material.valorvenda, material.nome, material.cdmaterial, tarefarecursogeral.qtde")				
					.join("tarefarecursogeral.material material")
					.leftOuterJoin("material.contagerencial contagerencial")
					.leftOuterJoin("contagerencial.tipooperacao tipooperacao")
					.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
					.join("tarefarecursogeral.tarefa tarefa")
					.join("tarefa.planejamento planejamento")
					.where("planejamento = ?",planejamento)
					.list();
	}

	/**
	 * Carrega a lista de recursos geral das tarefas do whereIn passado.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefarecursogeral> findByTarefa(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("tarefarecursogeral.cdtarefarecursogeral, tarefarecursogeral.qtde, material.cdmaterial, material.nome, " +
							"tarefa.cdtarefa, tarefa.dtinicio, tarefa.dtfim, tarefa.descricao, unidademedida.cdunidademedida, unidademedida.nome, " +
							"unidademedida.simbolo, listaSolicitacaocompraorigem.cdsolicitacaocompraorigem, projeto.cdprojeto")
					.join("tarefarecursogeral.material material")
					.join("material.unidademedida unidademedida")
					.join("tarefarecursogeral.tarefa tarefa")
					.join("tarefa.planejamento planejamento")
					.join("planejamento.projeto projeto")
					.leftOuterJoin("tarefarecursogeral.listaSolicitacaocompraorigem listaSolicitacaocompraorigem")
					.whereIn("tarefa.cdtarefa", whereIn)
					.orderBy("tarefa.descricao, material.nome")
					.list();
	}

	

}
