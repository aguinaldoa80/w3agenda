package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Leadhistorico;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class LeadhistoricoDAO extends GenericDAO<Leadhistorico>{

	@Override
	public void updateEntradaQuery(QueryBuilder<Leadhistorico> query) {
		query
			.select("leadhistorico.cdleadhistorico, leadhistorico.cdusuarioaltera, " +
					"leadhistorico.dtaltera, leadhistorico.observacao, lead.cdlead")
			.join("leadhistorico.lead lead");
	}
	
	public List<Leadhistorico> findByContacrm(Contacrm contacrm, Atividadetipo atividadetipo) {
		if(contacrm == null || contacrm.getCdcontacrm() == null){
			throw new SinedException("Conta n�o pode ser nula.");
		}
		return query()
					.select("leadhistorico.cdleadhistorico, leadhistorico.cdusuarioaltera, " +
							"leadhistorico.dtaltera, leadhistorico.observacao")
					.join("leadhistorico.lead lead")
					.join("lead.listaContacrm contacrm")
					.leftOuterJoin("leadhistorico.atividadetipo atividadetipo")
					.where("contacrm = ?", contacrm)
					.where("atividadetipo = ?", atividadetipo)
					.list();
	}

	public boolean haveHistorico(Lead l, Timestamp dtenvio, String obs) {
		if(l == null || l.getCdlead() == null){
			throw new SinedException("Lead n�o pode ser nula.");
		}
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(Leadhistorico.class)
					.join("leadhistorico.lead lead")
					.where("lead = ?", l)
					.where("leadhistorico.dtaltera = ?", dtenvio)
					.where("leadhistorico.observacao like ?", obs)
					.unique() > 0;
	}

	/**
	* M�todo que carrega a lista de hist�rico do Lead ordenada por data
	*
	* @param lead
	* @return
	* @since Aug 3, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Leadhistorico> carregaListaleadhistorico(Lead lead) {
		if(lead == null || lead.getCdlead() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
					.leftOuterJoin("leadhistorico.lead lead")
					.where("lead = ?", lead)
					.orderBy("leadhistorico.dtaltera")
					.list();
	}

	/**
	 * M�todo que carrega o hist�rico do lead para consulta
	 *
	 * @param lead
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Leadhistorico> findByLead(Lead lead) {
		if(lead == null || lead.getCdlead() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
			.select("leadhistorico.cdleadhistorico, leadhistorico.cdusuarioaltera, " +
					"leadhistorico.dtaltera, leadhistorico.observacao, atividadetipo.cdatividadetipo, " +
					"atividadetipo.nome")
			.join("leadhistorico.lead lead")
			.leftOuterJoin("leadhistorico.atividadetipo atividadetipo")
			.where("lead = ?", lead)
			.orderBy("leadhistorico.dtaltera DESC")
			.list();
	}
	
	/**
	 * 
	 * @param lead
	 * @param dthistoricoInicio
	 * @param dthistoricoFim
	 * @author Thiago Clemente
	 * 
	 */
	public Long getQtdeInteracoes(Lead lead, Date dthistoricoInicio, Date dthistoricoFim){
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.join("leadhistorico.lead lead")
				.where("lead=?", lead)
				.where("date(leadhistorico.dtaltera)>=?", dthistoricoInicio)
				.where("date(leadhistorico.dtaltera)<=?", dthistoricoFim)
				.setUseTranslator(false)
				.unique();
	}	
}
