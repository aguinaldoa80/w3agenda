package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialnumeroserie;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialnumeroserieDAO extends GenericDAO<Materialnumeroserie> {

	/**
	 * M�todo que busca a lista materialnumeroserie do material
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Materialnumeroserie> findListaMaterialnumeroserieByMaterial(Material material) {
		return query()
				.select("materialnumeroserie.cdmaterialnumeroserie, entrega.cdentrega, notafiscalproduto.cdNota, materialnumeroserie.numero, " +
						"material.cdmaterial")
				.leftOuterJoin("materialnumeroserie.entrega entrega")
				.leftOuterJoin("materialnumeroserie.notafiscalproduto notafiscalproduto")
				.join("materialnumeroserie.material material")
				.where("material = ?", material)
				.list();
	}
	
	/**
	 * M�todo que busca n�meros de s�rie dispon�veis do material
	 *
	 * @param codigo
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Materialnumeroserie> loadWithNumeroserie(Integer codigo) {
		return query()
			.select("materialnumeroserie.cdmaterialnumeroserie, materialnumeroserie.numero")
			.leftOuterJoin("materialnumeroserie.notafiscalproduto notafiscalproduto")
			.join("materialnumeroserie.material material")
			.where("material.cdmaterial = ?", codigo)
			.where("notafiscalproduto is null")
			.list();
	}

	public void salvaEscolhaNumeroserie(Integer cdnota, Integer cdmaterial, String whereIn) {
		if(cdnota != null && !"".equals(cdnota) && cdmaterial != null && !"".equals(cdmaterial) && whereIn != null && !"".equals(whereIn)){
			getHibernateTemplate().bulkUpdate("update Materialnumeroserie set cdnota = ? where cdmaterial = ? and cdmaterialnumeroserie in (" + whereIn + ") " ,
					new Object[]{cdnota, cdmaterial});
		}
	}

	/**
	 * M�todo que retorna a qtde j� registrada de n�mero de s�rie para o material da nota
	 *
	 * @param cdmaterial
	 * @param cdnota
	 * @return
	 * @author Luiz Fernando
	 */
	public Integer getQtdeJaRegistrada(Integer cdmaterial, Integer cdnota) {
		Integer qtdeJaRegistrada = 0;
		
		if(cdmaterial != null && cdnota != null){
			Long qtde = newQueryBuilder(Long.class)
				.select("count(*)")
				.setUseTranslator(false)
				.from(Materialnumeroserie.class)
				.join("materialnumeroserie.notafiscalproduto notafiscalproduto")
				.join("materialnumeroserie.material material")
				.where("material.cdmaterial = ?", cdmaterial)
				.where("notafiscalproduto.cdNota = ?", cdnota)
				.unique();
			if(qtde != null){
				qtdeJaRegistrada = qtde.intValue();
			}
		}
		
		return qtdeJaRegistrada;
	}
	
	public List<Materialnumeroserie> findByCdsmaterial(String whereIn) {
		return query()
				.select("materialnumeroserie.cdmaterialnumeroserie, entrega.cdentrega, notafiscalproduto.cdNota, materialnumeroserie.numero, " +
						"material.cdmaterial")
				.join("materialnumeroserie.material material")
				.leftOuterJoin("materialnumeroserie.entrega entrega")
				.leftOuterJoin("materialnumeroserie.notafiscalproduto notafiscalproduto")
				.whereIn("material.cdmaterial", whereIn)
				.list();
	}
}