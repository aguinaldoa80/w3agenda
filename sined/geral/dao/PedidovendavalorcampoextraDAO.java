package br.com.linkcom.sined.geral.dao;

import java.util.Collections;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendavalorcampoextra;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PedidovendavalorcampoextraDAO extends GenericDAO<Pedidovendavalorcampoextra>{

	/**
	 * Busca todos os campos extras de um determinado {@link Pedidovenda}
	 * @param pedidovendatipo
	 * @return
	 */
	public List<Pedidovendavalorcampoextra> findByPedidoVenda(Pedidovenda bean) {

		if(bean == null  || bean.getCdpedidovenda() == null){
			return Collections.emptyList();
		}
		
		return query()
	   		 .select("pedidovendavalorcampoextra.cdpedidovendavalorcampoextra, pedidovendavalorcampoextra.valor," +
	   		 		"campoextrapedidovendatipo.cdcampoextrapedidovendatipo, campoextrapedidovendatipo.nome, " +
	   		 		"campoextrapedidovendatipo.ordem, campoextrapedidovendatipo.obrigatorio")
	   		 		.join("pedidovendavalorcampoextra.campoextrapedidovendatipo campoextrapedidovendatipo")
	   		 		.where("pedidovendavalorcampoextra.pedidovenda = ?", bean)
	   		 		.orderBy("campoextrapedidovendatipo.ordem")
	   		 		.list();
	}

}
