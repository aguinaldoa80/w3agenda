package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculoordemservico;
import br.com.linkcom.sined.geral.bean.Veiculouso;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVeiculouso;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.AnaliseveiculousoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.FichaalocacaoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculousoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.report.bean.FichaUsoVeiculoResult;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.FichausoveiculoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VeiculousoDAO extends GenericDAO<Veiculouso> {

	@Override
	public void updateListagemQuery(QueryBuilder<Veiculouso> query,	FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		VeiculousoFiltro filtro = (VeiculousoFiltro) _filtro;
		query
			.select("veiculouso.cdveiculouso, veiculouso.kminicio, veiculouso.kmfim, veiculouso.dtfimuso, veiculouso.dtiniciouso, " +
					"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, localarmazenagem.nome, cliente.nome, veiculo.placa, bempatrimonio.nome, " +
					"aux_veiculouso.situacao, veiculousotipo.descricao, escalahorario.cdescalahorario, escalahorario.horainicio, " +
					"escalahorario.horafim")
			.join("veiculouso.veiculo veiculo")
			.join("veiculouso.veiculousotipo veiculousotipo")
			.join("veiculo.patrimonioitem patrimonioitem")
			.join("patrimonioitem.bempatrimonio bempatrimonio")
			.join("veiculouso.empresa empresa")
			.leftOuterJoin("veiculouso.localarmazenagem localarmazenagem")
			.leftOuterJoin("veiculouso.cliente cliente")
			.leftOuterJoin("veiculouso.escalahorario escalahorario")
			.join("veiculouso.aux_veiculouso aux_veiculouso")
			.leftOuterJoin("veiculouso.projeto projeto")
			.where("veiculo = ?", filtro.getVeiculo())
			.where("veiculousotipo = ?", filtro.getVeiculousotipo())
			.where("cliente = ?", filtro.getCliente())
			.where("localarmazenagem = ?", filtro.getLocalarmazenagem())
			.where("empresa = ?", filtro.getEmpresa())
			.whereIn("aux_veiculouso.situacao", filtro.getListaveiculo() != null ? SituacaoVeiculouso.listAndConcatenate(filtro.getListaveiculo()) : null)
//			.where("veiculouso.dtiniciouso >= ?", filtro.getDatade())
//			.where("veiculouso.dtfimuso <= ?", filtro.getDatate());
			.where("projeto = ?", filtro.getProjeto())
			.whereLikeIgnoreAll("veiculo.placa", filtro.getPlaca())
			.where("veiculouso.dtiniciouso >= ?", filtro.getDtiniciouso())
			.where("veiculouso.dtfimuso <= ?", filtro.getDtfimuso())
			.orderBy("veiculouso.cdveiculouso desc");
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Veiculouso> query) {
		query
			.select("veiculouso.cdveiculouso, veiculouso.dtiniciouso, veiculouso.dtfimuso, veiculouso.kminicio, veiculouso.kmfim, " +
					"veiculouso.horimetroinicio, veiculouso.horimetrofim, " +
					"veiculouso.cdusuarioaltera, veiculouso.dtaltera, veiculo.cdveiculo, veiculo.tipocontroleveiculo, contato.cdpessoa, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
					"empresa.cdpessoa, centrocusto.cdcentrocusto, aux_veiculouso.situacao, cliente.cdpessoa, cliente.nome, cliente.identificador, projeto.cdprojeto, " +
					"listaveiculousoitem.cdveiculousoitem, patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, listaveiculousoparada.cdveiculousoparada, " +
					"listaveiculousoparada.local, listaveiculousoparada.inicioparada, listaveiculousoparada.fimparada, listaveiculousoparada.km, listaveiculousoparada.horimetro, " +
					"listaescolta.cdveiculousoescolta, " +
					"listaescolta.origem, listaescolta.destino, listaescolta.celular1, listaescolta.celular2, listaescolta.checoutelefone, " +
					"listaescolta.checouradio, listaescolta.placa, veiculomodelo.cdveiculomodelo, listaescolta.checoudocumentos, listaescolta.checouarmasmunicao, " +
					"listaescolta.checoubateria, " +
					"listaescolta.checourastreador, listaescolta.motorista, listaescolta.rgmotorista, listaescolta.anotacoes, condutor.cdpessoa, " +
					"vigilante1.cdpessoa, vigilante2.cdpessoa, vigilante3.cdpessoa, veiculousoescoltatipo.cdveiculousoescoltatipo, condutor.nome, " +
					"veiculocor.cdveiculocor, uf.cduf, municipio.cdmunicipio, municipio.nome, contagerencial.cdcontagerencial, veiculousotipo.cdveiculousotipo, " +
					"escalahorario.cdescalahorario, escalahorario.horainicio, escalahorario.horafim, projetolistaveiculousoparada.cdprojeto, categoriaveiculo.cdcategoriaveiculo, " +
					"bempatrimonio.nome, veiculo.placa, veiculo.dtentrada, listaVeiculoImplemento.cdveiculoimplemento, listaVeiculoImplemento.horimetroinicio, " +
					"listaVeiculoImplemento.horimetrofim, listaVeiculoImplemento.kminicio, listaVeiculoImplemento.kmfim, implemento.cdveiculo, " +
					"implemento.placa, implemento.tipocontroleveiculo, implementopatrimonioitem.cdpatrimonioitem, implementobempatrimonio.nome")
			.join("veiculouso.veiculo veiculo")
			.join("veiculo.patrimonioitem veiculopatrimonioitem")
			.leftOuterJoin("veiculopatrimonioitem.bempatrimonio bempatrimonio")
			.leftOuterJoin("veiculouso.contato contato")
			.leftOuterJoin("veiculouso.cliente cliente")
			.leftOuterJoin("veiculouso.localarmazenagem localarmazenagem")
			.leftOuterJoin("veiculouso.escalahorario escalahorario")
			.leftOuterJoin("veiculouso.empresa empresa")
			.leftOuterJoin("veiculouso.centrocusto centrocusto")
			.leftOuterJoin("veiculouso.aux_veiculouso aux_veiculouso")
			.leftOuterJoin("veiculouso.veiculousotipo veiculousotipo")
			.leftOuterJoin("veiculouso.condutor condutor")
			.leftOuterJoin("veiculouso.contagerencial contagerencial")
			.leftOuterJoin("veiculouso.projeto projeto")
			.leftOuterJoin("veiculouso.listaescolta listaescolta")
//			.leftOuterJoin("listaescolta.condutor condutor")
			.leftOuterJoin("listaescolta.vigilante1 vigilante1")
			.leftOuterJoin("listaescolta.vigilante2 vigilante2")
			.leftOuterJoin("listaescolta.vigilante3 vigilante3")
			.leftOuterJoin("listaescolta.veiculousoescoltatipo veiculousoescoltatipo")
			.leftOuterJoin("listaescolta.veiculocor veiculocor")
			.leftOuterJoin("listaescolta.veiculomodelo veiculomodelo")
			.leftOuterJoin("listaescolta.uf uf")
			.leftOuterJoin("listaescolta.municipio municipio")
			.leftOuterJoin("veiculouso.listaveiculousoparada listaveiculousoparada")
			.leftOuterJoin("veiculouso.listaveiculousoitem listaveiculousoitem")
			.leftOuterJoin("listaveiculousoparada.projeto projetolistaveiculousoparada")
			.leftOuterJoin("veiculo.veiculomodelo modelo")
			.leftOuterJoin("modelo.categoriaveiculo categoriaveiculo")
			.leftOuterJoin("listaveiculousoitem.patrimonioitem patrimonioitem")
			.leftOuterJoin("veiculouso.listaVeiculoImplemento listaVeiculoImplemento")
			.leftOuterJoin("listaVeiculoImplemento.implemento implemento")
			.leftOuterJoin("implemento.patrimonioitem implementopatrimonioitem")
			.leftOuterJoin("implementopatrimonioitem.bempatrimonio implementobempatrimonio");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaescolta");
		save.saveOrUpdateManaged("listaveiculousoparada");
		save.saveOrUpdateManaged("listaveiculousoitem");
		save.saveOrUpdateManaged("listaVeiculoImplemento");
	}
	
	/**
	 * M�todo que retorna todos os registros de veiculo uso de acordo com o filtro da tela de aloca��o veiculo flex
	 * 
	 * @param dtReferencia
	 * @param dtReferenciaLimite
	 * @param veiculo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Veiculouso> findAlocacaoVeiculoData(Date dtReferencia, Date dtReferenciaLimite, Veiculo veiculo) {
		return query()
			.select("veiculouso.cdveiculouso, veiculouso.dtiniciouso, veiculouso.dtfimuso, cliente.cdpessoa, cliente.nome, " +
					"veiculousotipo.cdveiculousotipo, veiculousotipo.descricao, escalahorario.cdescalahorario, escalahorario.horainicio, " +
					"escalahorario.horafim, condutor.cdpessoa, veiculo.cdveiculo")
			.leftOuterJoin("veiculouso.cliente cliente")
			.leftOuterJoin("veiculouso.escalahorario escalahorario")
			.join("veiculouso.condutor condutor")
			.join("veiculouso.veiculo veiculo")
			.join("veiculouso.veiculousotipo veiculousotipo")
			.join("veiculouso.aux_veiculouso aux_veiculouso")
//			.where("cliente = ?", filtro.getCliente())
//			.where("condutor = ?", filtro.getColaborador())
			.where("veiculo = ?", veiculo)
			.where("veiculouso.dtfimuso >= ?", dtReferencia)
			.where("veiculouso.dtiniciouso <= ?", dtReferenciaLimite)
//			.where("veiculouso.dtiniciouso >= ?", dtReferencia)
//			.where("veiculouso.dtfimuso <= ?", dtReferenciaLimite)
			.where("aux_veiculouso.situacao <> ?", SituacaoVeiculouso.CANCELADO)
			.list();
	}
	
	
	/**
	 * Verifica se tem algum uso de ve�culo passado por par�metro que j� foi cancelado.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean haveVeiculousoCancelado(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Where in n�o pode ser nulo.");
		}
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(Veiculouso.class)
					.join("veiculouso.aux_veiculouso aux_veiculouso")
					.where("aux_veiculouso.situacao = " + SituacaoVeiculouso.CANCELADO.ordinal())
					.whereIn("veiculouso.cdveiculouso", whereIn)
					.unique() > 0;
	}

	/**
	 * Cancela o uso do ve�culo.
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void updateSituacaoCancelado(String whereIn) {
		getHibernateTemplate().bulkUpdate("update Veiculouso v set v.dtcancela = ? where v.id in (" + whereIn + ")", SinedDateUtils.currentDate());
	}
	
	/**
	 * Verifica se tem algum uso de ve�culo passado por par�metro que j� foi finalizado.
	 *
	 * @param whereIn
	 * @return
	 * @author Fernando Boldrini
	 */
	public boolean haveVeiculousoFinalizado(String whereIn) {
		if(whereIn == null || whereIn.equals(""))
			throw new SinedException("Where in n�o pode ser nulo.");
		
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(Veiculouso.class)
					.join("veiculouso.aux_veiculouso aux_veiculouso")
					.where("aux_veiculouso.situacao = " + SituacaoVeiculouso.FINALIZADO.ordinal())
					.whereIn("veiculouso.cdveiculouso", whereIn)
					.unique() > 0;
	}
	
	/**
	 * Verifica se um determinado ve�culo ja foi alocado naquele hor�rio dentro da data inicio e fim
	 * 
	 * @param veiculouso
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeVeiculoAlocadoMesmoHorarioData(Veiculouso veiculouso) {
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(beanClass)
			.join("veiculouso.veiculo veiculo")
			.leftOuterJoin("veiculouso.escalahorario escalahorario")
			.join("veiculouso.aux_veiculouso aux_veiculouso")
			.where("veiculo = ?", veiculouso.getVeiculo())			
			.where("veiculouso <> ?", veiculouso.getCdveiculouso() != null ? veiculouso : null)
			.where("aux_veiculouso.situacao <> ?",SituacaoVeiculouso.CANCELADO)
			.where("veiculouso.dtfimuso >= ?", veiculouso.getDtiniciouso())
			.where("veiculouso.dtiniciouso <= ?", veiculouso.getDtfimuso())
			.openParentheses()
			.where("escalahorario is null")
			.or()
			.where("escalahorario is not null")
			.where("escalahorario = ?", veiculouso.getEscalahorario() != null ? veiculouso.getEscalahorario() : null)
			.closeParentheses()
			.unique() > 0;
	}
	
//		/**
//	 * Retorna os dados para o relat�rio de ficha de uso
//	 * @param filtro
//	 * @return
//	 * @author Thiago Gon�alves
//	 * @deprecated Fernando Boldrini
//	 */	
//	public List<Veiculouso> findForFichaUso(FichausoveiculoFiltro filtro){
//		return query().joinFetch("veiculouso.empresa empresa")
//				      .joinFetch("veiculouso.condutor condutor")
//				      .joinFetch("veiculouso.veiculo veiculo")
//				      .joinFetch("veiculo.patrimonioitem patrimonioitem")
//				      .joinFetch("patrimonioitem.bempatrimonio bempatrimonio")
//				      .leftOuterJoinFetch("veiculouso.cliente cliente")
//				      .where("veiculo = ?", filtro.getVeiculo())
//				      .where("empresa = ?", filtro.getEmpresa())
//				      .where("veiculouso.dtiniciouso = ?", filtro.getData())
//				      .orderBy("veiculouso.cdveiculouso,veiculouso.condutor")
//				      .list();
//	}
	
	/**
	 * Retorna os dados para o relat�rio de ficha de uso
	 * @param filtro
	 * @return
	 * @author Fernando Boldrini
	 */	
	
	public List<FichaUsoVeiculoResult> findForFichaUsoVeiculo(FichausoveiculoFiltro filtro) {
		if(filtro == null || filtro.getData() == null || filtro.getEmpresa() == null)
			throw new SinedException("Dados do filtro n�o informados");
		
		String data = new SimpleDateFormat("yyyy-MM-dd").format(filtro.getData());
		
		String sql = "SELECT V.CDVEICULO, EH.CDESCALAHORARIO, EMPRESA.NOMEFANTASIA AS EMPRESA, " +
				"MAT.NOME || ' (' || V.PLACA || ')' AS VEICULO, " +
				"RESP.NOME AS RESPONSAVEL, TO_CHAR(EH.HORAINICIO, 'hh24:mi') AS INICIO, " +
				"TO_CHAR(EH.HORAFIM, 'hh24:mi') AS FIM, CLIENTE.NOME AS CLIENTE " +
				"FROM VEICULOUSO VU " +
				"JOIN VEICULO V ON vu.cdveiculo = V.CDVEICULO " +
				"LEFT JOIN ESCALAHORARIO EH ON EH.CDESCALAHORARIO = VU.CDESCALAHORARIO " +
				"LEFT JOIN PESSOA RESP ON RESP.CDPESSOA = V.cdcolaborador " +
				"LEFT JOIN PESSOA CLIENTE ON CLIENTE.CDPESSOA = VU.CDCLIENTE " +
				"JOIN EMPRESA EMPRESA ON EMPRESA.CDPESSOA = VU.CDEMPRESA " +
				"JOIN PATRIMONIOITEM PI ON PI.CDPATRIMONIOITEM = V.CDPATRIMONIOITEM " +
				"JOIN MATERIAL MAT ON MAT.CDMATERIAL = PI.CDBEMPATRIMONIO " +
				"WHERE VU.DTCANCELA IS NULL " +
				"AND VU.CDEMPRESA = " + filtro.getEmpresa().getCdpessoa() + 
				" AND VU.DTINICIOUSO <= '" + data + "' " +
				"AND ((VU.DTFIMUSO IS NOT NULL AND VU.DTFIMUSO >= '" + data + "') OR (VU.DTFIMUSO IS NULL))";
		
		if(filtro.getVeiculo() != null) 
			sql += " AND V.CDVEICULO = " + filtro.getVeiculo().getCdveiculo();
		
		sql += " ORDER BY MAT.NOME || ' (' || V.PLACA || ')'";
		
		SinedUtil.markAsReader();
		@SuppressWarnings("unchecked")
		List<FichaUsoVeiculoResult> listFicha = getJdbcTemplate().query(sql, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				FichaUsoVeiculoResult bean = new FichaUsoVeiculoResult();
				
				bean.setCdescalahorario(rs.getInt("cdescalahorario"));
				bean.setCdveiculo(rs.getInt("cdveiculo"));
				bean.setCliente(rs.getString("cliente"));
				bean.setEmpresa(rs.getString("empresa"));
			
				bean.setHoraFinal(rs.getString("fim"));
				bean.setHoraInicial(rs.getString("inicio"));
				
				bean.setResponsavel(rs.getString("responsavel"));
				bean.setVeiculo(rs.getString("veiculo"));
				
				return bean;
			}
		});
		
		return listFicha;
		
		/*return query()
				.select("veiculouso.cdveiculo, escala.cdescalahorario,empresa.nome, material.nome, " +
				"responsavel.nome, escala.horainicio, escala.horafim, cliente.nome")
					.join("veiculouso.empresa empresa")
					.join("veiculouso.condutor condutor")
					.join("veiculosuo.escolahorario escala")
					.list();*/
	}

	/**
	 * M�todo que busca os dados de acordo com o filtro para montar o relat�rio de Fichas de aloca��o
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Veiculouso> findVeiculoUsoForFichaAlocacao(FichaalocacaoFiltro filtro) {
		return querySined()
				
			.select("veiculouso.cdveiculouso, veiculouso.dtiniciouso, veiculouso.dtfimuso, veiculo.placa, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
					"condutor.cdpessoa, condutor.nome, cliente.cdpessoa, cliente.nome, escalahorario.cdescalahorario, escalahorario.horainicio, " +
					"escalahorario.horafim, patrimonioitem.cdpatrimonioitem, bempatrimonio.nome, escala.cdescala, escala.naotrabalhadomingo, " +
					"escala.naotrabalhaferiado")
			.join("veiculouso.empresa empresa")
			.join("veiculouso.veiculo veiculo")
			.join("veiculo.patrimonioitem patrimonioitem")
			.join("veiculo.escala escala")
			.join("patrimonioitem.bempatrimonio bempatrimonio")
			.join("veiculouso.condutor condutor")
			.join("veiculouso.aux_veiculouso aux_veiculouso")
			.leftOuterJoin("veiculouso.cliente cliente")
			.leftOuterJoin("veiculouso.escalahorario escalahorario")
			.where("empresa = ?", filtro.getEmpresa())
			.where("veiculo = ?", filtro.getVeiculo())
			.where("cliente = ?", filtro.getCliente())
			.where("veiculouso.dtfimuso >= ?", filtro.getDtinicio())
			.where("veiculouso.dtiniciouso <= ?", filtro.getDtfim())
			.where("aux_veiculouso.situacao <> ?", SituacaoVeiculouso.CANCELADO)
			.orderBy("cliente.nome, veiculouso.dtiniciouso, escalahorario.horainicio")
			.list();
	}

	/**
	 * M�todo que busca os usos do veiculo em um prazo de 1 ano. Para gerar relatorio de analise
	 * 
	 * @param filtro
	 * @return
	 * @throws ParseException
	 * @author Tom�s Rabelo, Fernando Boldrini
	 */
	public List<Veiculouso> findVeiculoUsoForAnalise(AnaliseveiculousoFiltro filtro) throws ParseException{
		return querySined()
				
				.select("veiculouso.cdveiculouso, veiculouso.dtiniciouso, veiculouso.dtfimuso, veiculo.placa, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
						"condutor.cdpessoa, condutor.nome, patrimonioitem.cdpatrimonioitem, bempatrimonio.nome, escala.cdescala, escala.naotrabalhadomingo, " +
						"escala.naotrabalhaferiado, aux_veiculouso.situacao, veiculo.cdveiculo, veiculousotipo.cdveiculousotipo, veiculousotipo.descricao")
				.join("veiculouso.empresa empresa")
				.join("veiculouso.veiculousotipo veiculousotipo")
				.join("veiculouso.veiculo veiculo")
				.join("veiculo.patrimonioitem patrimonioitem")
				.leftOuterJoin("veiculo.escala escala")
				.join("patrimonioitem.bempatrimonio bempatrimonio")
				.join("veiculouso.condutor condutor")
				.join("veiculouso.aux_veiculouso aux_veiculouso")
				.where("empresa = ?", filtro.getEmpresa())
				.where("veiculo = ?", filtro.getVeiculo())
				.where("veiculouso.dtiniciouso <= ?", new SimpleDateFormat("dd/MM/yyyy").parse("31/12/"+filtro.getAno()))
				.where("veiculouso.dtfimuso >= ?", new SimpleDateFormat("dd/MM/yyyy").parse("01/01/"+(filtro.getAno())))
				.orderBy("veiculo.cdveiculo, condutor.cdpessoa, aux_veiculouso.situacao, veiculouso.dtiniciouso")
				.list();
	}
	/**
	 * Busca veiculo que est� em uso na data escolhida para a ordem de servi�o.
	 * @param veiculoordemservico
	 * @return
	 */

	public boolean existeVeiculoAlocadoData(Veiculoordemservico veiculoordemservico) {
		return newQueryBuilder(Long.class)
		.select("count(*)")
		.setUseTranslator(false)
		.from(beanClass)
		.join("veiculouso.veiculo veiculo")
		.where("veiculo = ?", veiculoordemservico.getVeiculo())
		.where("veiculouso.dtiniciouso >= ?", veiculoordemservico.getDtprevista())
		.where("veiculouso.dtfimuso <= ?", veiculoordemservico.getDtprevista())
		.unique() > 0;
	}

	/**
	 * M�todo que chama procedure que atualiza a situa��o dos veiculos uso, toda vez que usu�rio acessa a 
	 * listagem.
	 * @author Tom�s Rabelo
	 */
	public void updateAux() {
		getJdbcTemplate().execute("SELECT ATUALIZA_VEICULOUSO()");
	}

	/**
	 * M�todo que busca os dados do uso do ve�culo para gera��o do relat�rio
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Veiculouso> findForReport(VeiculousoFiltro filtro) {
		QueryBuilder<Veiculouso> query = querySined();
		this.updateListagemQuery(query, filtro);
		
		query
			.select("veiculouso.cdveiculouso, veiculouso.kminicio, veiculouso.kmfim, veiculouso.dtfimuso, veiculouso.dtiniciouso, " +
					"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, localarmazenagem.nome, cliente.nome, veiculo.placa, bempatrimonio.nome, " +
					"aux_veiculouso.situacao, veiculousotipo.descricao, escalahorario.cdescalahorario, escalahorario.horainicio, " +
					"escalahorario.horafim, listaveiculousoparada.cdveiculousoparada, listaveiculousoparada.local, " +
					"listaveiculousoparada.inicioparada, listaveiculousoparada.fimparada, listaveiculousoparada.km ")
			.leftOuterJoin("veiculouso.listaveiculousoparada listaveiculousoparada");
		
		return query.list();
	}
	
	/**
	 * M�todo que busca os dados do uso do ve�culo para gera��o do relat�rio csv
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Veiculouso> findForReportCSV(VeiculousoFiltro filtro) {
		QueryBuilder<Veiculouso> query = querySined();
		this.updateListagemQuery(query, filtro);
		
		query
			.select("veiculouso.cdveiculouso, veiculouso.kminicio, veiculouso.kmfim, veiculouso.dtfimuso, veiculouso.dtiniciouso, " +
					"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, localarmazenagem.nome, cliente.nome, veiculo.placa, bempatrimonio.nome, " +
					"aux_veiculouso.situacao, veiculousotipo.descricao, escalahorario.cdescalahorario, escalahorario.horainicio, " +
					"escalahorario.horafim, listaveiculousoparada.cdveiculousoparada, listaveiculousoparada.local, " +
					"listaveiculousoparada.inicioparada, listaveiculousoparada.fimparada, listaveiculousoparada.km, " +
					"condutor.nome, projeto.nome, centrocusto.nome, contagerencial.nome, contato.nome ")
			.leftOuterJoin("veiculouso.listaveiculousoparada listaveiculousoparada")
			.leftOuterJoin("veiculouso.centrocusto centrocusto")
			.leftOuterJoin("veiculouso.condutor condutor")
			.leftOuterJoin("veiculouso.contagerencial contagerencial")
			.leftOuterJoin("veiculouso.contato contato");
		
		return query.list();
	}

	/**
	 * M�todo que busca uma lista de uso de veiculo
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Veiculouso> findForInspecao(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				
				.select("veiculouso.cdveiculouso, veiculo.cdveiculo")
				.join("veiculouso.veiculo veiculo")
				.whereIn("veiculouso.cdveiculouso", whereIn)
				.list();
	}
	
	/**
	 * M�todo que carrega para o veiculouso para inspe��o
	 *
	 * @param veiculouso
	 * @return
	 * @author Luiz Fernando
	 */
	public Veiculouso loadForInspecao(Veiculouso veiculouso){
		return query()
				.select("veiculouso.cdveiculouso, veiculo.cdveiculo, veiculo.placa, veiculo.kminicial, veiculo.dtentrada, veiculo.anofabricacao, veiculo.anomodelo, " +
					"veiculo.chassi, veiculo.renavam, veiculo.anotacoes, veiculo.tipocontroleveiculo, patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, bempatrimonio.nome, " +
					"aux_veiculo.situacao, veiculo.prefixo, veiculomodelo.cdveiculomodelo ")
				.leftOuterJoin("veiculouso.veiculo veiculo")
				.leftOuterJoin("veiculo.patrimonioitem patrimonioitem")
				.leftOuterJoin("veiculo.aux_veiculo aux_veiculo")
				.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
				.leftOuterJoin("veiculo.veiculomodelo veiculomodelo")
				.where("veiculouso = ?", veiculouso)
				.unique();
	}
	
	/**
	 * 
	 * @param veiculo
	 * @author Thiago Clemente
	 * 
	 */
	public Veiculouso getUltimoUso(Veiculo veiculo){
		return query()
				.select("veiculouso.cdveiculouso, veiculo.cdveiculo, projeto.cdprojeto, projeto.nome")
				.join("veiculouso.veiculo veiculo")
				.join("veiculouso.projeto projeto")
				.where("veiculo=?", veiculo)
				.orderBy("veiculouso.dtiniciouso desc")
				.setMaxResults(1)
				.unique();
	}
}
