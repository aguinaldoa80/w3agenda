package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;

import br.com.linkcom.sined.geral.bean.Pedidovendasincronizacao;
import br.com.linkcom.sined.geral.bean.Pedidovendasincronizacaohistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PedidovendasincronizacaohistoricoDAO extends GenericDAO<Pedidovendasincronizacaohistorico> {
	
	/**
	 * M�todo que faz o insert do hist�rico de sincroniza��o
	 *
	 * @param pedidovendasincronizacao
	 * @author Luiz Fernando
	 */
	public void insertPedidovendasincronizacaohistorico(Pedidovendasincronizacao pedidovendasincronizacao, String obs, String log) {
		if(obs != null && obs.length() >= 100){
			obs = obs.substring(0, 99);
		}
		if(pedidovendasincronizacao != null && pedidovendasincronizacao.getCdpedidovendasincronizacao() != null){
			String insert = " INSERT INTO PEDIDOVENDASINCRONIZACAOHISTORICO (CDPEDIDOVENDASINCRONIZACAOHISTORICO, CDPEDIDOVENDASINCRONIZACAO, OBSERVACAO, MSGLOG, DTALTERA) " +
			" VALUES (nextval('sq_pedidovendasincronizacaohistorico'), " + pedidovendasincronizacao.getCdpedidovendasincronizacao() + ", '" + (obs != null ? obs : "") +"', '" + (log != null ? log : "") +"', '" + new Timestamp(System.currentTimeMillis()) + "') ;";
	
			getJdbcTemplate().execute(insert);
		}
	}
}
