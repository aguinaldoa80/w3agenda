package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.Notafiscalservicoduplicata;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotafiscalservicoduplicataDAO extends GenericDAO<Notafiscalservicoduplicata> {

	/**
	* M�todo que busca as duplicatas da nota de servi�o
	*
	* @param notaFiscalServico
	* @return
	* @since 16/01/2017
	* @author Luiz Fernando
	*/
	public List<Notafiscalservicoduplicata> findByNotafiscalservico(NotaFiscalServico notaFiscalServico) {
		if(notaFiscalServico == null || notaFiscalServico.getCdNota() == null){
			throw new SinedException("Nota fiscal de servi�o n�o pode ser nula.");
		}
		return query()
					.select("notafiscalservicoduplicata.cdnotafiscalservicoduplicata, notafiscalservicoduplicata.numero, " +
							"notafiscalservicoduplicata.dtvencimento, notafiscalservicoduplicata.valor, " +
							"documentotipo.cddocumentotipo, documentotipo.nome ")
					.leftOuterJoin("notafiscalservicoduplicata.documentotipo documentotipo")
					.where("notafiscalservicoduplicata.notafiscalservico = ?", notaFiscalServico)
					.orderBy("notafiscalservicoduplicata.dtvencimento")
					.list();
	}
	
	public Boolean existeDuplicata(NotaFiscalServico notaFiscalServico) {
		if(notaFiscalServico == null || notaFiscalServico.getCdNota() == null){
			throw new SinedException("Nota fiscal de servi�o n�o pode ser nula.");
		}
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Notafiscalservicoduplicata.class)
			.where("notafiscalservicoduplicata.notafiscalservico = ?", notaFiscalServico)
			.unique() > 0;
	}

	public void updateNumeroDuplicata(Notafiscalservicoduplicata dup, String numero) {
		if(dup != null && dup.getCdnotafiscalservicoduplicata() != null && numero != null && !"".equals(numero)){
			getJdbcTemplate().execute("UPDATE NOTAFISCALSERVICODUPLICATA SET NUMERO = '" + numero + "' WHERE CDNOTAFISCALSERVICODUPLICATA = " + dup.getCdnotafiscalservicoduplicata());
		}
	}
}
