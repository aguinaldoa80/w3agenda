package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemequipamento;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoordemequipamentoDAO extends GenericDAO<Producaoordemequipamento> {

	
	/**
	* M�todo que busca os equipamentos da ordem de rodu��o
	*
	* @param producaoordem
	* @return
	* @since 24/02/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordemequipamento> findByProducaoordem(String whereIn, Boolean patrimonioitemNulo) {
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		QueryBuilder<Producaoordemequipamento> query = query()
				.select("producaoordemequipamento.cdproducaoordemequipamento, producaoordemequipamento.identificadorinterno, producaoordem.cdproducaoordem, " +
						"producaoordemmaterial.cdproducaoordemmaterial, material.cdmaterial, material.nome, material.identificacao, " +
						"patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta ")
				.leftOuterJoin("producaoordemequipamento.producaoordem producaoordem")
				.leftOuterJoin("producaoordemequipamento.producaoordemmaterial producaoordemmaterial")
				.leftOuterJoin("producaoordemequipamento.material material")
				.leftOuterJoin("producaoordemequipamento.patrimonioitem patrimonioitem")
				.whereIn("producaoordem.cdproducaoordem", whereIn);
		
		if(patrimonioitemNulo != null && patrimonioitemNulo){
			query.where("patrimonioitem is null");
		}
		
		return query.list();
	}

	/**
	* M�todo que faz o update do vinculo entre o equipamento e o item da ordem de produ��o
	*
	* @param producaoordemequipamento
	* @param producaoordemmaterial
	* @since 26/02/2016
	* @author Luiz Fernando
	*/
	public void updateVinculoProducaoordemmaterial(Producaoordemequipamento producaoordemequipamento, Producaoordemmaterial producaoordemmaterial) {
		if(producaoordemequipamento == null || producaoordemequipamento.getCdproducaoordemequipamento() == null || producaoordemmaterial == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		getHibernateTemplate().bulkUpdate("update Producaoordemequipamento poe set poe.producaoordemmaterial = ? where poe.id = ?", 
				new Object[]{producaoordemmaterial, producaoordemequipamento.getCdproducaoordemequipamento()});
	}

	/**
	* M�todo que faz o update do item de patrimonio do equipamento da ordem de produ��o
	*
	* @param producaoordemequipamento
	* @param patrimonioitem
	* @since 26/02/2016
	* @author Luiz Fernando
	*/
	public void updatePatrimonioitem(Producaoordemequipamento producaoordemequipamento,	Patrimonioitem patrimonioitem) {
		if(producaoordemequipamento == null || producaoordemequipamento.getCdproducaoordemequipamento() == null || patrimonioitem == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		getHibernateTemplate().bulkUpdate("update Producaoordemequipamento poe set poe.patrimonioitem = ? where poe.id = ?", 
				new Object[]{patrimonioitem, producaoordemequipamento.getCdproducaoordemequipamento()});
	}
	
	/**
	* M�todo que delata os equipamentos
	*
	* @param producaoordem
	* @param whereIn
	* @return
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordemequipamento> findForDelete(Producaoordem producaoordem, String whereIn) {
		if(whereIn == null || whereIn.equals("") || producaoordem == null || producaoordem.getCdproducaoordem() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("producaoordemequipamento.cdproducaoordemequipamento, producaoordem.cdproducaoordem")
					.join("producaoordemequipamento.producaoordem producaoordem")
					.where("producaoordemequipamento.cdproducaoordemequipamento not in (" +whereIn+")")
					.where("producaoordem = ?", producaoordem)
					.list();
	}
}
