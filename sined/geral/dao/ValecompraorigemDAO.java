package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Valecompraorigem;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ValecompraorigemDAO extends GenericDAO<Valecompraorigem> {

	/**
	 * Busca as origens de vale compra a partir do documento
	 *
	 * @param documento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/09/2014
	 */
	public List<Valecompraorigem> findByWhereInCddocumento(String whereInCddocumento, Tipooperacao tipooperacao) {
		return querySined()
					
					.select("valecompraorigem.cdvalecompraorigem, valecompra.data, valecompra.valor, cliente.cdpessoa, valecompra.cdvalecompra, " +
							"tipooperacao.cdtipooperacao, documento.cddocumento, tipooperacao.nome")
					.join("valecompraorigem.valecompra valecompra")
					.join("valecompra.tipooperacao tipooperacao")
					.join("valecompra.cliente cliente")
					.join("valecompraorigem.documento documento")
					.where("valecompra.tipooperacao = ?", tipooperacao)
					.whereIn("documento.cddocumento", whereInCddocumento, false)
					.list();
	}
	
	/**
	* M�todo que verifica se existe vale compra para o documento
	*
	* @param whereIn
	* @return
	* @since 18/12/2015
	* @author Luiz Fernando
	*/
	public Boolean existeValecompra(String whereIn) {
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.from(Valecompraorigem.class)
					.join("valecompraorigem.documento documento")
					.whereIn("documento.cddocumento", whereIn)
					.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.unique() > 0;
	}
	
	/**
	* M�todo que calcula o total de vale compra utilizado na baixa
	*
	* @param whereIn
	* @return
	* @since 18/12/2015
	* @author Luiz Fernando
	*/
	public Money calcularValecompraByWhereInDocumento(String whereIn) {
		Long total = newQueryBuilderSined(Long.class)
					
					.select("sum(valecompra.valor)")
					.from(Valecompraorigem.class)
					.setUseTranslator(false)
					.join("valecompraorigem.documento documento")
					.join("valecompraorigem.valecompra valecompra")
					.whereIn("documento.cddocumento", whereIn)
					.where("documento.documentoclasse = ?", Documentoclasse.OBJ_RECEBER)
					.unique();
		
		return total != null ? new Money(total, true) : new Money();
	}

	/**
	 * Busca as origens pelo vale compra
	 *
	 * @param whereInValecompra
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/09/2014
	 */
	public List<Valecompraorigem> findByValecompra(String whereInValecompra) {
		return query()
					.select("valecompraorigem.cdvalecompraorigem, valecompra.cdvalecompra, " +
							"documento.cddocumento, documento.numero, documento.descricao, documento.dtemissao, documento.dtvencimento, documentoclasse.cddocumentoclasse, " +
							"documentoacao.cddocumentoacao, aux_documento.valoratual, documento.tipopagamento, pessoa.cdpessoa")
					.join("valecompraorigem.valecompra valecompra")
					.join("valecompraorigem.documento documento")
					.join("documento.documentoacao documentoacao")
					.join("documento.aux_documento aux_documento")
					.leftOuterJoin("documento.pessoa pessoa")
					.leftOuterJoin("documento.documentoclasse documentoclasse")
					.whereIn("valecompra.cdvalecompra", whereInValecompra)
					.list();
	}
	
}