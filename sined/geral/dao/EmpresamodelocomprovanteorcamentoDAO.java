package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.Empresamodelocomprovanteorcamento;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EmpresamodelocomprovanteorcamentoDAO extends GenericDAO<Empresamodelocomprovanteorcamento>{

	public Empresamodelocomprovanteorcamento loadWithArquivo(Empresamodelocomprovanteorcamento bean){
		return query()
				.select("arquivo.cdarquivo, arquivo.nome, arquivo.tipoconteudo, "+
						"empresamodelocomprovanteorcamento.cdempresamodelocomprovanteorcamento, empresamodelocomprovanteorcamento.nome")
				.join("empresamodelocomprovanteorcamento.arquivo arquivo")
				.where("empresamodelocomprovanteorcamento = ?", bean)
				.unique();
	}
}
