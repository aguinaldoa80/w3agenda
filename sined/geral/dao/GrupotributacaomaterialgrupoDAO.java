package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Grupotributacaomaterialgrupo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class GrupotributacaomaterialgrupoDAO extends GenericDAO<Grupotributacaomaterialgrupo>{

	public List<Grupotributacaomaterialgrupo> findByGrupotributacao(Grupotributacao grupotributacao){
		return query()
				.where("grupotributacaomaterialgrupo.grupotributacao = ?", grupotributacao)
				.list();
	}
	
	
}
