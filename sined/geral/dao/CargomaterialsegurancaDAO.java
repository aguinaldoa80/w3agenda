package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cargomaterialseguranca;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CargomaterialsegurancaDAO extends GenericDAO<Cargomaterialseguranca> {

	/***
	 * Busca a lista de material de seguran�a dos cargos presentes em listaCargo
	 * 
	 * @param listaCargo
	 * @return List<Cargomaterialseguranca>
	 * @throws SinedException - caso o cargo seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Cargomaterialseguranca> findByCargo(List<Cargo> listaCargo) {
		if (listaCargo == null) {
			throw new SinedException("A lista de cargos n�o pode ser nula.");
		}
		
		return 
			query()
				.select("cargomaterialseguranca.cdcargomaterialseguranca, cargomaterialseguranca.quantidade, " +
						"cargo.cdcargo, " +
						"epi.cdmaterial, epi.nome, epi.duracao, epi.valorvenda, " +
						"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo")
				.join("cargomaterialseguranca.cargo cargo")
				.join("cargomaterialseguranca.epi epi")
				.join("epi.unidademedida unidademedida")
				.whereIn("cargo.cdcargo", CollectionsUtil.listAndConcatenate(listaCargo, "cdcargo",","))
				.list();
	}
}