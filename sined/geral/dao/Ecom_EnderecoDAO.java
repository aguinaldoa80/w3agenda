package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Ecom_Cliente;
import br.com.linkcom.sined.geral.bean.Ecom_Endereco;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class Ecom_EnderecoDAO extends GenericDAO<Ecom_Endereco>{

	public List<Ecom_Endereco> findByCliente(Ecom_Cliente cliente){
		return querySined()
				.where("ecom_Endereco.ecomCliente = ?", cliente)
				.list();
	}
}
