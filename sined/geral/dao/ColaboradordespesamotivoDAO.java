package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Colaboradordespesamotivo;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ColaboradordespesamotivoFilter;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColaboradordespesamotivoDAO extends GenericDAO<Colaboradordespesamotivo>{	
	
	@Override
	public void updateListagemQuery(
			QueryBuilder<Colaboradordespesamotivo> query, FiltroListagem _filtro) {
		
		ColaboradordespesamotivoFilter filtro = (ColaboradordespesamotivoFilter) _filtro; 
		
		query
			.select("colaboradordespesamotivo.cdcolaboradordespesamotivo, colaboradordespesamotivo.descricao")
			.whereLikeIgnoreAll("descricao", filtro.getDescricao());
	}
	
	public Colaboradordespesamotivo motivoHolerite(){
		QueryBuilder<Colaboradordespesamotivo> query = query()
			.where("colaboradordespesamotivo.descricao = 'holerite'");
		return query.unique();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Colaboradordespesamotivo> query) {
		query
			.select("colaboradordespesamotivo.cdcolaboradordespesamotivo, colaboradordespesamotivo.descricao, " +
					"colaboradordespesamotivo.dtaltera, colaboradordespesamotivo.cdusuarioaltera, " +
					"empresa.cdpessoa, empresa.nome, " +
					"contaGerencial.cdcontagerencial, contaGerencial.nome, " +
					"vcontagerencial.nivel, vcontagerencial.identificador, vcontagerencial.nome, " +
					"listaMotivoItem.cdColaboradorDespesaMotivoItem, listaMotivoItem.codigoIntegracao")
			.leftOuterJoin("colaboradordespesamotivo.listaMotivoItem listaMotivoItem")
			.leftOuterJoin("listaMotivoItem.empresa empresa")
			.leftOuterJoin("listaMotivoItem.contaGerencial contaGerencial")
			.leftOuterJoin("contaGerencial.vcontagerencial vcontagerencial");
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaMotivoItem");
	}
}
