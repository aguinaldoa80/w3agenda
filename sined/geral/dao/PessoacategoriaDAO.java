package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Pessoacategoria;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PessoacategoriaDAO extends GenericDAO<Pessoacategoria> {
	
	
	/**
	 * Retorna uma lista de Pessoacategoria conforme Cliente informado como par�metro.
	 * @param cliente
	 * @return
	 * @author Taidson
	 * @since 16/11/2010
	 */
	public List<Pessoacategoria> findByCliente(Cliente cliente){
		return query()
		.select("pessoacategoria.cdpessoacategoria, pessoa.cdpessoa, pessoa.nome," +
				"categoria.cdcategoria, categoria.nome")
		.join("pessoacategoria.categoria categoria")
		.join("pessoacategoria.pessoa pessoa")
		.where("pessoa = ?", cliente)
		.list();
	}

	/**
	 * Verifica se existe a categoria no cliente.
	 *
	 * @param cliente
	 * @param categoria
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/03/2014
	 */
	public boolean haveCategoriaCliente(Cliente cliente, Categoria categoria) {
		return newQueryBuilderWithFrom(Long.class)
					.select("count(*)")
					.where("pessoacategoria.categoria = ?", categoria)
					.where("pessoacategoria.pessoa = ?", cliente)
					.unique() > 0;
	}

	/**
	 * Delete as categorias de um cliente.
	 *
	 * @param cliente
	 * @author Rodrigo Freitas
	 * @since 07/04/2014
	 */
	public void deleteByCliente(Cliente cliente) {
		getJdbcTemplate().execute("DELETE FROM PESSOACATEGORIA WHERE CDPESSOA = " + cliente.getCdpessoa());
	}
	
	public List<Pessoacategoria> findByFornecedor(Fornecedor fornecedor){
		return query()
		.select("pessoacategoria.cdpessoacategoria, pessoa.cdpessoa, pessoa.nome," +
				"categoria.cdcategoria, categoria.nome")
		.join("pessoacategoria.categoria categoria")
		.join("pessoacategoria.pessoa pessoa")
		.where("pessoa = ?", fornecedor)
		.list();
	}
	
	public List<Pessoacategoria> findClienteForAndroid(String whereIn) {
		return query()
				.select("pessoacategoria.cdpessoacategoria, pessoa.cdpessoa, pessoa.nome," +
						"categoria.cdcategoria, categoria.nome")
				.join("pessoacategoria.categoria categoria")
				.join("pessoacategoria.pessoa pessoa")
				.where("exists (select c.cdpessoa from Cliente c where c.cdpessoa = pessoa.cdpessoa)")
				.whereIn("pessoacategoria.cdpessoacategoria", whereIn)
				.list();
	}
	
}
