package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Inspecaoitemtipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.InspecaoitemtipoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("inspecaoitemtipo.nome")
public class InspecaoitemtipoDAO extends GenericDAO<Inspecaoitemtipo> {

	@Override
	public void updateListagemQuery(QueryBuilder<Inspecaoitemtipo> query, FiltroListagem _filtro) {
		InspecaoitemtipoFiltro filtro = (InspecaoitemtipoFiltro) _filtro;
		
		query
			.select("inspecaoitemtipo.cdinspecaoitemtipo, inspecaoitemtipo.nome")
			.whereLikeIgnoreAll("inspecaoitemtipo.nome", filtro.getNome());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Inspecaoitemtipo> query) {
		query
			.select("inspecaoitemtipo.cdinspecaoitemtipo, inspecaoitemtipo.nome, inspecaoitemtipo.dtaltera, " +
					"inspecaoitemtipo.cdusuarioaltera");
	}
	
}
