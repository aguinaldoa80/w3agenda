package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Servicodominiorede;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.ServicodominioredeFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ServicodominioredeDAO extends GenericDAO<Servicodominiorede>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Servicodominiorede> query, FiltroListagem _filtro) {
		ServicodominioredeFiltro filtro = (ServicodominioredeFiltro)_filtro;
		query.leftOuterJoinFetch("servicodominiorede.servicoservidortipo servicoservidortipo")
		 	 .leftOuterJoinFetch("servicodominiorede.redemascara redemascara")
		 	 .where("servicodominiorede.endereco = ?", filtro.getEndereco());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Servicodominiorede> query) {
		query.leftOuterJoinFetch("servicodominiorede.servicoservidortipo servicoservidortipo")
	 	 	 .leftOuterJoinFetch("servicodominiorede.redemascara redemascara");
	}
		
}
