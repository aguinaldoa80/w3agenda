package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoItem;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotaFiscalServicoItemDAO extends GenericDAO<NotaFiscalServicoItem> {

	public List<NotaFiscalServicoItem> findForRateioFaturamento(NotaFiscalServico nfs) {
		if(nfs == null || nfs.getCdNota() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("notaFiscalServicoItem.cdNotaFiscalServicoItem, notaFiscalServicoItem.qtde, notaFiscalServicoItem.precoUnitario, notaFiscalServicoItem.desconto, " +
						"notaFiscalServico.cdNota, empresa.cdpessoa, contagerencial.cdcontagerencial, " +
						"vendamaterial.cdvendamaterial, venda.cdvenda, centrocusto.cdcentrocusto, centrocustoEmpresa.cdcentrocusto, " +
						"material.cdmaterial, contagerencialvenda.cdcontagerencial, centrocustovenda.cdcentrocusto, " +
						"materialmestre.cdmaterial, contagerencialvenda2.cdcontagerencial, centrocustovenda2.cdcentrocusto ")
				.join("notaFiscalServicoItem.vendamaterial vendamaterial")
				.join("notaFiscalServicoItem.notaFiscalServico notaFiscalServico")
				.join("notaFiscalServico.empresa empresa")
				.join("vendamaterial.venda venda")
				.join("vendamaterial.material material")
				.leftOuterJoin("empresa.contagerencial contagerencial")
				.leftOuterJoin("vendamaterial.materialmestre materialmestre")
				.leftOuterJoin("material.contagerencialvenda contagerencialvenda")
				.leftOuterJoin("materialmestre.contagerencialvenda contagerencialvenda2")
				.leftOuterJoin("venda.centrocusto centrocusto")
				.leftOuterJoin("material.centrocustovenda centrocustovenda")
				.leftOuterJoin("materialmestre.centrocustovenda centrocustovenda2")
				.leftOuterJoin("empresa.centrocusto centrocustoEmpresa")
				.where("notaFiscalServico = ?", nfs)
				.list();
	}

	public List<NotaFiscalServicoItem> findForPdfListagem(String whereIn) {
		QueryBuilder<NotaFiscalServicoItem> query = querySined()
				
				.select("notaFiscalServico.cdNota, notaFiscalServico.dtEmissao, " +
						"empresa.cdpessoa, " +
						"uf.sigla, " +
						"notaFiscalServico.basecalculoicms, notaFiscalServico.icms, " +
						"notaFiscalServicoItem.precoUnitario, notaFiscalServicoItem.qtde, notaFiscalServicoItem.desconto ")
				.join("notaFiscalServicoItem.notaFiscalServico notaFiscalServico")
				.join("notaFiscalServico.cliente cliente")
				.leftOuterJoin("notaFiscalServico.empresa empresa")
				.leftOuterJoin("cliente.listaEndereco listaEndereco")
				.leftOuterJoin("listaEndereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf");
		
		SinedUtil.quebraWhereIn("notaFiscalServico.cdNota", whereIn, query);
		
		return query.list();
	}

	public Boolean existePedidoVendaMaterial(Pedidovendamaterial pedidovendamaterial) {
		if (pedidovendamaterial == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(NotaFiscalServicoItem.class)
				.join("notaFiscalServicoItem.notaFiscalServico notaFiscalServico")
				.where("notaFiscalServicoItem.pedidovendamaterial = ?", pedidovendamaterial)
				.where("notaFiscalServico.notaStatus <> ?", NotaStatus.CANCELADA)
				.unique() > 0;
	}

}
