package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.ArquivoMdfe;
import br.com.linkcom.sined.geral.bean.Arquivonf;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ConfiguracaonfeFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("configuracaonfe.descricao")
public class ConfiguracaonfeDAO extends GenericDAO<Configuracaonfe> {
	
	private EnderecoService enderecoService;
	private ArquivoService arquivoService;
	private ArquivoDAO arquivoDAO;
	
	
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Configuracaonfe> query, FiltroListagem _filtro) {
		ConfiguracaonfeFiltro filtro = (ConfiguracaonfeFiltro) _filtro;
		
		query
			.select("configuracaonfe.cdconfiguracaonfe, configuracaonfe.descricao, empresa.nome, empresa.razaosocial, empresa.nomefantasia, configuracaonfe.tipoconfiguracaonfe, " +
					"configuracaonfe.ativo, configuracaonfe.padrao, configuracaonfe.prefixowebservice ")
			.leftOuterJoin("configuracaonfe.empresa empresa")
			.whereLikeIgnoreAll("configuracaonfe.descricao", filtro.getDescricao())
			.where("configuracaonfe.tipoconfiguracaonfe = ?", filtro.getTipoconfiguracaonfe())
			.where("empresa = ?", filtro.getEmpresa())
			.where("configuracaonfe.ativo = ?", filtro.getAtivo())
			;
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Configuracaonfe> query) {
		query
			.leftOuterJoinFetch("configuracaonfe.empresa empresa")
			.leftOuterJoinFetch("configuracaonfe.logoNfse logoNfse")
			.leftOuterJoinFetch("configuracaonfe.uf ufemissao")
			.leftOuterJoinFetch("configuracaonfe.endereco endereco")
			.leftOuterJoinFetch("endereco.municipio municipio")
			.leftOuterJoinFetch("municipio.uf uf")
			.leftOuterJoinFetch("configuracaonfe.configuracaoNfeManifestoAutomatico configuracaoNfeManifestoAutomatico")
			.leftOuterJoinFetch("configuracaoNfeManifestoAutomatico.uf ufufAutomatico");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Configuracaonfe configuracaonfe = (Configuracaonfe)save.getEntity();
		
		if(configuracaonfe.getLogoNfse() != null){
			arquivoService.saveOrUpdate(configuracaonfe.getLogoNfse());
			arquivoDAO.saveFile(configuracaonfe , "logoNfse");
		}
				
		if(configuracaonfe.getEndereco() != null){
			configuracaonfe.getEndereco().setEnderecotipo(Enderecotipo.UNICO);
			enderecoService.saveOrUpdate(configuracaonfe.getEndereco());
		}
	}
	
	/**
	 * Atualiza o contador de lote de evento.
	 *
	 * @param configuracaonfe
	 * @param contador
	 * @since 17/09/2012
	 * @author Rodrigo Freitas
	 */
	public void updateContadorloteevento(Configuracaonfe configuracaonfe, Integer contador){
		getHibernateTemplate().bulkUpdate("update Configuracaonfe c set c.contadorloteevento = ? where c.id = ?", new Object[]{
				contador, configuracaonfe.getCdconfiguracaonfe()
		});
	}
	
	/**
	 * Busca as configura��es de NFe a partir do tipo.
	 *
	 * @param tipoconfiguracaonfe
	 * @return
	 * @since 25/01/2012
	 * @author Rodrigo Freitas
	 */
	public List<Configuracaonfe> findByTipo(Tipoconfiguracaonfe tipoconfiguracaonfe){
		return query()
					.select("configuracaonfe.cdconfiguracaonfe, configuracaonfe.descricao")
					.where("configuracaonfe.tipoconfiguracaonfe = ?", tipoconfiguracaonfe)
					.orderBy("configuracaonfe.descricao")
					.list();
	}

	/**
	 * Busca a configura��o de NF-e do Arquivonf passado por par�metro.
	 *
	 * @param arquivonf
	 * @return
	 * @since 13/02/2012
	 * @author Rodrigo Freitas
	 */
	public Configuracaonfe findByArquivonf(Arquivonf arquivonf) {
		if(arquivonf == null || arquivonf.getCdarquivonf() == null){
			throw new SinedException("Arquivonf n�o pode ser nulo.");
		}
		return query()
					.join("configuracaonfe.listaArquivonf arquivonf")
					.where("arquivonf = ?", arquivonf)
					.unique();
	}

	/**
	 * Busca a configura��o de NF-e do Arquivonfnota passado por par�metro.
	 *
	 * @param arquivonfnota
	 * @return
	 * @since 29/02/2012
	 * @author Rodrigo Freitas
	 */
	public Configuracaonfe findByArquivonfnota(Arquivonfnota arquivonfnota) {
		if(arquivonfnota == null || arquivonfnota.getCdarquivonfnota() == null){
			throw new SinedException("Arquivonfnota n�o pode ser nulo.");
		}
		return query()
					.join("configuracaonfe.listaArquivonf arquivonf")
					.join("arquivonf.listaArquivonfnota arquivonfnota")
					.where("arquivonfnota = ?", arquivonfnota)
					.unique();
	}

	/**
	 * Verifica se existe alguma configura��o padr�o do tipo passado por par�metro.
	 *
	 * @param cdconfiguracaonfe
	 * @param tipoconfiguracaonfe
	 * @return
	 * @since 27/08/2012
	 * @author Rodrigo Freitas
	 * @param empresa 
	 */
	public boolean havePadrao(Integer cdconfiguracaonfe, Tipoconfiguracaonfe tipoconfiguracaonfe, Empresa empresa) {
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.from(Configuracaonfe.class)
					.where("configuracaonfe.padrao = ?", Boolean.TRUE)
					.where("configuracaonfe.cdconfiguracaonfe <> ?", cdconfiguracaonfe)
					.where("configuracaonfe.tipoconfiguracaonfe = ?", tipoconfiguracaonfe)
					.where("configuracaonfe.empresa = ?", empresa)
					.unique() > 0;
	}
	
	public boolean havePrefixoativo(Empresa empresa, Prefixowebservice[] prefixowebserviceArray) {
		QueryBuilder<Long> query = newQueryBuilder(Long.class)
					.select("count(*)")
					.from(Configuracaonfe.class)
					.where("configuracaonfe.empresa = ?", empresa)
					.where("configuracaonfe.ativo = ?", Boolean.TRUE);
		
		query.openParentheses();
		for (Prefixowebservice prefixowebservice : prefixowebserviceArray) {
			query.where("configuracaonfe.prefixowebservice = ?", prefixowebservice).or();
		}
		query.closeParentheses();
		
		
		return query.unique() > 0;
	}
	
	/**
	 * Busca a configura��o padr�o da empreas e do tipo.
	 *
	 * @param tipoconfiguracaonfe
	 * @param empresa
	 * @return
	 * @since 27/08/2012
	 * @author Rodrigo Freitas
	 */
	public Configuracaonfe getConfiguracaoPadrao(Tipoconfiguracaonfe tipoconfiguracaonfe, Empresa empresa) {
		return query()
					.select("configuracaonfe.cdconfiguracaonfe, configuracaonfe.descricao, configuracaonfe.atualizacaodataemissao")
					.where("configuracaonfe.padrao = ?", Boolean.TRUE)
					.where("configuracaonfe.tipoconfiguracaonfe = ?", tipoconfiguracaonfe)
					.where("configuracaonfe.empresa = ?", empresa)
					.unique();
	}
	
	/**
	 * Busca as configura��es de NFe a partir do tipo e empresa.
	 *
	 * @param tipoconfiguracaonfe
	 * @param empresa
	 * @return
	 * @since 30/08/2012
	 * @author Rodrigo Freitas
	 */
	public List<Configuracaonfe> findAtivosByTipoEmpresa(Tipoconfiguracaonfe tipoconfiguracaonfe, Empresa empresa){
		String whereInEmpresas = null;
		try {
			whereInEmpresas = new SinedUtil().getListaEmpresa();
		} catch (Exception e) {}
		
		return query()
			.select("configuracaonfe.cdconfiguracaonfe, configuracaonfe.descricao, configuracaonfe.atualizacaodataentradasaida")
			.where("configuracaonfe.empresa = ?", empresa)
			.where("configuracaonfe.tipoconfiguracaonfe = ?", tipoconfiguracaonfe)
			.where("configuracaonfe.ativo = ?", Boolean.TRUE)
			.orderBy("configuracaonfe.descricao")
			.whereIn("empresa.cdpessoa", whereInEmpresas)
			.list();
	}
	
	/**
	 * Carrega o pr�ximo n�mero de lote da NF-e da empresa
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Integer carregaProxNumLoteNFe(Configuracaonfe configuracaonfe) {
		if(configuracaonfe == null || configuracaonfe.getCdconfiguracaonfe() == null){
			throw new SinedException("Configura��o n�o pode ser nulo.");
		}
		return query()
		.select("configuracaonfe.cdconfiguracaonfe, configuracaonfe.contadorlotenfe")
		.where("configuracaonfe = ?", configuracaonfe)
		.unique().getContadorlotenfe();
	}
	
	/**
	 * Atualiza o pr�ximo numero de lote de NF-e da empresa.
	 *
	 * @param empresa
	 * @param proximoNumero
	 * @author Rodrigo Freitas
	 */
	public void updateProximoNumLoteNFe(Configuracaonfe configuracaonfe, Integer proximoNumero) {
		if(configuracaonfe == null || configuracaonfe.getCdconfiguracaonfe() == null){
			throw new SinedException("Configura��o n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE CONFIGURACAONFE SET CONTADORLOTENFE = " + proximoNumero + " WHERE CDCONFIGURACAONFE = " + configuracaonfe.getCdconfiguracaonfe());
	}
	
	/**
	 * Carrega o pr�ximo n�mero da NF-e da empresa
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Integer carregaProxNumNFe(Configuracaonfe configuracaonfe) {
		if(configuracaonfe == null || configuracaonfe.getCdconfiguracaonfe() == null){
			throw new SinedException("Configura��o n�o pode ser nulo.");
		}
		return query()
		.select("configuracaonfe.cdconfiguracaonfe, configuracaonfe.contadornfe")
		.where("configuracaonfe = ?", configuracaonfe)
		.unique().getContadornfe();
	}
	
	public Integer carregaProxNumMdfe(Configuracaonfe configuracaonfe) {
		if(configuracaonfe == null || configuracaonfe.getCdconfiguracaonfe() == null){
			throw new SinedException("Configura��o n�o pode ser nulo.");
		}
		return query()
		.select("configuracaonfe.cdconfiguracaonfe, configuracaonfe.contadormdfe")
		.where("configuracaonfe = ?", configuracaonfe)
		.unique().getContadormdfe();
	}
	
	/**
	 * Atualiza o pr�ximo numero da NF-e da empresa.
	 *
	 * @param empresa
	 * @param proximoNumero
	 * @author Rodrigo Freitas
	 */
	public void updateProximoNumNFe(Configuracaonfe configuracaonfe, Integer proximoNumero) {
		if(configuracaonfe == null || configuracaonfe.getCdconfiguracaonfe() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE CONFIGURACAONFE SET CONTADORNFE = " + proximoNumero + " WHERE CDCONFIGURACAONFE = " + configuracaonfe.getCdconfiguracaonfe());
	}
	
	public void updateProximoNumMdfe(Configuracaonfe configuracaonfe, Integer proximoNumero) {
		if(configuracaonfe == null || configuracaonfe.getCdconfiguracaonfe() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE CONFIGURACAONFE SET CONTADORMDFE = " + proximoNumero + " WHERE CDCONFIGURACAONFE = " + configuracaonfe.getCdconfiguracaonfe());
	}
	
	/**
	 * Carrega o pr�ximo n�mero de lote da NFS-e da empresa
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Integer carregaProxNumLoteNFSe(Configuracaonfe configuracaonfe) {
		if(configuracaonfe == null || configuracaonfe.getCdconfiguracaonfe() == null){
			throw new SinedException("Configura��o n�o pode ser nulo.");
		}
		return query()
				.select("configuracaonfe.cdconfiguracaonfe, configuracaonfe.contadorlotenfse")
				.where("configuracaonfe = ?", configuracaonfe)
				.unique().getContadorlotenfse();
	}
	
	/**
	 * Atualiza o pr�ximo numero de lote de NFS-e da empresa.
	 *
	 * @param empresa
	 * @param proximoNumero
	 * @author Rodrigo Freitas
	 */
	public void updateProximoNumLoteNFSe(Configuracaonfe configuracaonfe, Integer proximoNumero) {
		if(configuracaonfe == null || configuracaonfe.getCdconfiguracaonfe() == null){
			throw new SinedException("Empresa n�o pode ser nulo.");
		}
		getJdbcTemplate().execute("UPDATE CONFIGURACAONFE SET CONTADORLOTENFSE = " + proximoNumero + " WHERE CDCONFIGURACAONFE = " + configuracaonfe.getCdconfiguracaonfe());
	}

	/**
	 * Busca todos os dados das configura��es de NF-e
	 *
	 * @return
	 * @since 28/08/2012
	 * @author Rodrigo Freitas
	 */
	public List<Configuracaonfe> findAllForWebservice() {
		QueryBuilder<Configuracaonfe> query = query();
		this.updateEntradaQuery(query);
		return query.list();
	}

	/**
	 * M�todo que verifica as configura��es de notas com n�meros maiores ou iguais ao campo "Pr�ximo n�mero de nota fiscal de produto", na tela empresa, \n
	 * para considera��o do ambiente de nota fiscal.
	 * @param empresa
	 * @author Danilo Guimar�es
	 */
	public List<Configuracaonfe> listaConfiguracaoNfproduto(Empresa empresa){
		return query()
				.select("configuracaonfe.cdconfiguracaonfe, configuracaonfe.prefixowebservice")
				.leftOuterJoin("configuracaonfe.listaArquivonf listaArquivonf")
				.leftOuterJoin("listaArquivonf.listaArquivonfnota listaArquivonfnota")
				.leftOuterJoin("listaArquivonfnota.nota nota")
				.where("nota.notaTipo = ?", NotaTipo.NOTA_FISCAL_PRODUTO)
				.where("nota.empresa = ?", empresa)
				.where("nota.numeronota >= ?", (empresa.getProximonumnfproduto() != null ? empresa.getProximonumnfproduto().doubleValue() : null))
				.where("nota.serienfe = ?", empresa.getSerienfe())
				.where("nota.cdNota in (select nfp.cdNota from Notafiscalproduto nfp where nfp.modeloDocumentoFiscalEnum = ?)", ModeloDocumentoFiscalEnum.NFE)
//				.where("nota.notaStatus != ?", NotaStatus.CANCELADA)
				.list();
	}
	/**
	 * Busca as configura��es de NFe a partir do tipo e empresa retornando os prefixos web
	 *
	 * @param tipoconfiguracaonfe
	 * @param empresa
	 * @return
	 * @since 30/10/2015
	 * @author Rodrigo Freitas - Cria��o
	 * @author C�sar - Altera��o - Inser��o do prefixowebservice	
	 */
	public List<Configuracaonfe> findAtivosByTipoEmpresaPrefixowebservice(Tipoconfiguracaonfe tipoconfiguracaonfe, Empresa empresa){
		String whereInEmpresas = null;
		try {
			whereInEmpresas = new SinedUtil().getListaEmpresa();
		} catch (Exception e) {}
		
		return query()
			.select("configuracaonfe.cdconfiguracaonfe, configuracaonfe.descricao, configuracaonfe.atualizacaodataentradasaida,configuracaonfe.prefixowebservice")
			.where("configuracaonfe.empresa = ?", empresa)
			.where("configuracaonfe.tipoconfiguracaonfe = ?", tipoconfiguracaonfe)
			.where("configuracaonfe.ativo = ?", Boolean.TRUE)
			.orderBy("configuracaonfe.descricao")
			.whereIn("empresa.cdpessoa", whereInEmpresas)
			.list();
	}

	public Configuracaonfe findByArquivoMdfe(ArquivoMdfe arquivoMdfe) {
		if(arquivoMdfe == null || arquivoMdfe.getCdArquivoMdfe() == null) {
			throw new SinedException("ArquivoMdfe n�o pode ser nulo.");
		}
		return query()
					.join("configuracaonfe.listaArquivoMdfe arquivoMdfe")
					.where("arquivoMdfe = ?", arquivoMdfe)
					.unique();
	}

	@Override
	public ListagemResult<Configuracaonfe> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Configuracaonfe> query = query();
		updateListagemQuery(query, filtro);
		
		return new ListagemResult<Configuracaonfe>(query, filtro);
	}

	public void updateUltNsu(Configuracaonfe configuracaoNfe) {
		if (configuracaoNfe == null || configuracaoNfe.getCdconfiguracaonfe() == null || configuracaoNfe.getUltNsu() == null) {
			throw new SinedException("Configura��o NF-e n�o pode ser nulo.");
		}
		
		getHibernateTemplate().bulkUpdate("update Configuracaonfe c set c.ultNsu = ? where c.id = ?", new Object[] {
				configuracaoNfe.getUltNsu(), configuracaoNfe.getCdconfiguracaonfe()
		});
	}

	public Configuracaonfe carregaConfiguracao(Configuracaonfe configuracaonfe) {
		return query()
				.select("configuracaonfe.cdconfiguracaonfe, configuracaonfe.ultNsu, configuracaonfe.prefixowebservice, uf.cdibge ")
				.leftOuterJoin("configuracaonfe.uf uf")
				.where("configuracaonfe = ?", configuracaonfe)
				.unique();
	}

	public List<Configuracaonfe> procurarDde() {
		return query()
				.select("configuracaonfe.cdconfiguracaonfe, configuracaonfe.ultNsu, " +
						"uf.cdibge, " +
						"empresa.cdpessoa ")
				.leftOuterJoin("configuracaonfe.uf uf")
				.leftOuterJoin("configuracaonfe.empresa empresa")
				.where("configuracaonfe.tipoconfiguracaonfe = ?", Tipoconfiguracaonfe.DDFE)
				.where("empresa is not null")
				.list();
	}
	
	public List<Configuracaonfe> procurarMd(Empresa empresa, Configuracaonfe configuracaonfe, boolean producao) {
		if(empresa == null) return new ArrayList<Configuracaonfe>();
		
		return query()
				.select("configuracaonfe.cdconfiguracaonfe, configuracaonfe.ultNsu, configuracaonfe.descricao, " +
						"uf.cdibge, " +
						"empresa.cdpessoa ")
				.leftOuterJoin("configuracaonfe.uf uf")
				.leftOuterJoin("configuracaonfe.empresa empresa")
				.where("configuracaonfe.tipoconfiguracaonfe = ?", Tipoconfiguracaonfe.MD)
				.where("configuracaonfe.prefixowebservice = ?", producao ? Prefixowebservice.AN_EVENTO_PROD : Prefixowebservice.AN_EVENTO_HOM)
				.where("empresa = ?", empresa)
				.openParentheses()
					.where("coalesce(configuracaonfe.ativo, false) = true")
					.or()
					.where("configuracaonfe = ?", configuracaonfe)
				.closeParentheses()
				.list();
	}

	public List<Configuracaonfe> buscaPorModeloEmpresa(ModeloDocumentoFiscalEnum modelo, Empresa empresa) {
		if (empresa == null) {
			return new ArrayList<Configuracaonfe>();
		}
		
		return query()
				.select("configuracaonfe.cdconfiguracaonfe, configuracaonfe.descricao ")
				.leftOuterJoin("configuracaonfe.empresa empresa")
				.where("configuracaonfe.tipoconfiguracaonfe = ?", ModeloDocumentoFiscalEnum.NFCE.equals(modelo) ? Tipoconfiguracaonfe.NOTA_FISCAL_CONSUMIDOR : Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO)
				.where("empresa = ?", empresa)
				.list();
	}
	
	public Configuracaonfe findForNfce(Empresa empresa){
		return query()
				.select("configuracaonfe.cdconfiguracaonfe, configuracaonfe.descricao ")
				.unique();
	}
}
