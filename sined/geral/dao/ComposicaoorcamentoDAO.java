package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Composicaoorcamento;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Recursocomposicao;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.ComposicaoOrcamentoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ComposicaoorcamentoDAO extends GenericDAO<Composicaoorcamento>{
	
	private RecursocomposicaoDAO recursocomposicaoDAO;
	
	public void setRecursocomposicaoDAO(RecursocomposicaoDAO recursocomposicaoDAO) {
		this.recursocomposicaoDAO = recursocomposicaoDAO;
	}
	
	/**
	 * Retorna uma lista de composi��o (somente c�digo e nome) de um determinado or�amento.
	 *
	 * @param orcamento
	 * @return lista de Composicaoorcamento
	 * @throws SinedException - caso o or�amento seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Composicaoorcamento> findByOrcamento(Orcamento orcamento) {
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("O par�metro or�amento n�o pode ser nulo.");
		}
		return 
			query()
				.select("composicaoorcamento.cdcomposicaoorcamento, composicaoorcamento.nome, contagerencial.cdcontagerencial, contagerencial.nome, vcontagerencial.identificador")
				.join("composicaoorcamento.orcamento orcamento")
				.leftOuterJoin("composicaoorcamento.contagerencial contagerencial")
				.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
				.where("orcamento = ?", orcamento)
				.orderBy("composicaoorcamento.nome")
				.list();
	}	
	
	/**
	 * Retorna uma lista de composi��o de um determinado or�amento.
	 * Caso o par�metro materialSeguranca seja verdadeiro, ser�o retornadas somente as composi��es que se referem a EPI
	 * Caso o par�metro materialSeguranca seja falso, ser�o retornadas somente as composi��es que n�o se referem a EPI
	 * Caso o par�metro materialSeguranca seja nulo, ser�o retornadas todas as composi��es
	 *
	 * @param orcamento
	 * @param materialSeguranca
	 * @return lista de Composicaoorcamento
	 * @throws SinedException - caso o or�amento seja nulo
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Composicaoorcamento> findByOrcamentoForFlex(Orcamento orcamento, Boolean materialSeguranca) {
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("O par�metro or�amento n�o pode ser nulo.");
		}
		return 
			query()
				.select(
					"composicaoorcamento.cdcomposicaoorcamento, composicaoorcamento.nome, composicaoorcamento.materialseguranca, " +
					"orcamento.cdorcamento, orcamento.nome, orcamento.prazosemana, orcamento.calcularmaterialseguranca, " +
					"recursocomposicao.cdrecursocomposicao, recursocomposicao.nome, recursocomposicao.quantidade, recursocomposicao.quantidadecalculada, recursocomposicao.custounitario, recursocomposicao.numocorrencia, " +
					"tipoitemcomposicao.cdtipoitemcomposicao, tipoitemcomposicao.nome, " +
					"formulacomposicao.cdformulacomposicao, formulacomposicao.nome, " +						
					"tiporelacaorecurso.cdtiporelacaorecurso, tiporelacaorecurso.nome, " +
					"tipodependenciarecurso.cdtipodependenciarecurso, tipodependenciarecurso.nome, " +
					"tipoocorrencia.cdtipoocorrencia, tipoocorrencia.nome, " +
					"material.cdmaterial, material.nome, materialgrupo.cdmaterialgrupo, materialgrupo.nome, " +
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, referenciacalculo.cdreferenciacalculo, referenciacalculo.nome")
				.leftOuterJoin("composicaoorcamento.orcamento orcamento")
				.leftOuterJoin("composicaoorcamento.listaRecursocomposicao recursocomposicao")
				.leftOuterJoin("recursocomposicao.tipoitemcomposicao tipoitemcomposicao")
				.leftOuterJoin("recursocomposicao.referenciacalculo referenciacalculo")
				.leftOuterJoin("recursocomposicao.formulacomposicao formulacomposicao")
				.leftOuterJoin("recursocomposicao.tiporelacaorecurso tiporelacaorecurso")
				.leftOuterJoin("recursocomposicao.tipodependenciarecurso tipodependenciarecurso")
				.leftOuterJoin("recursocomposicao.tipoocorrencia tipoocorrencia")
				.leftOuterJoin("recursocomposicao.material material")
				.leftOuterJoin("recursocomposicao.materialgrupo materialgrupo")
				.leftOuterJoin("recursocomposicao.unidademedida unidademedida")
				.where("orcamento = ?", orcamento)
				.where("composicaoorcamento.materialseguranca = ?", materialSeguranca)
				.orderBy("composicaoorcamento.nome")
				.list();
	}	
	
	/**
	 * Insere/atualiza a composi��o do or�amento
	 * 
	 * Tamb�m insere/atualiza os recursos de cada composi��o
	 * @see br.com.linkcom.sined.geral.dao.RecursocomposicaoDAO#saveListaRecursoComposicaoForFlex(List, List)
	 *
	 * @param bean
	 * @param listaRecursoComposicaoForDelete
	 * @return
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public void saveListaComposicaoOrcamentoForFlex(final Composicaoorcamento bean, final List<Recursocomposicao> listaRecursoComposicaoForDelete) {		
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {

				saveOrUpdateNoUseTransaction(bean);					
				recursocomposicaoDAO.saveListaRecursoComposicao(bean.getListaRecursocomposicao(), listaRecursoComposicaoForDelete);					
				
				return null;
			}
		});		
	}
	
	/**
	 * Busca as composi��es de or�amento a partir de um filtro.
	 *
	 * @param filtro
 	 * @return List<Composicaoorcamento>
 	 * 
 	 * @throws SinedException - caso o or�amento seja nulo
	 * 
 	 * @author Rodrigo Alvarenga
	 */
	public List<Composicaoorcamento> findForListagemFlex(ComposicaoOrcamentoFiltro filtro) {
		if (filtro.getOrcamento() == null || filtro.getOrcamento().getCdorcamento() == null) {
			throw new SinedException("O or�amento n�o pode ser nulo.");
		}
		return 
			query()
				.select("composicaoorcamento.cdcomposicaoorcamento, composicaoorcamento.nome, " +
						"orcamento.cdorcamento, orcamento.nome")
				.join("composicaoorcamento.orcamento orcamento")
				.where("orcamento = ?", filtro.getOrcamento())
				.whereLikeIgnoreAll("composicaoorcamento.nome", filtro.getNome())
				.openParentheses()
					.where("composicaoorcamento.materialseguranca is null")
					.or()
					.where("composicaoorcamento.materialseguranca = ?", Boolean.FALSE)
				.closeParentheses()
				.orderBy("composicaoorcamento.nome")
				.list();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Composicaoorcamento> query) {
		query
			.select(
				"composicaoorcamento.cdcomposicaoorcamento, composicaoorcamento.nome, " +
				"orcamento.cdorcamento, orcamento.nome, " +
				"recursocomposicao.cdrecursocomposicao, recursocomposicao.nome, recursocomposicao.quantidade, " +
				"recursocomposicao.quantidadecalculada, recursocomposicao.custounitario, recursocomposicao.numocorrencia, " +
				"tipoitemcomposicao.cdtipoitemcomposicao, tipoitemcomposicao.nome, " +
				"formulacomposicao.cdformulacomposicao, formulacomposicao.nome, " +						
				"tiporelacaorecurso.cdtiporelacaorecurso, tiporelacaorecurso.nome, " +
				"tipodependenciarecurso.cdtipodependenciarecurso, tipodependenciarecurso.nome, " +
				"tipoocorrencia.cdtipoocorrencia, tipoocorrencia.nome, " +
				"material.cdmaterial, material.nome, materialgrupo.cdmaterialgrupo, materialgrupo.nome, " +
				"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
				"dependenciacargocomposicao.cddependenciacargocomposicao, " +
				"cargoOrigem.cdcargo, cargoOrigem.nome, " +
				"dependenciafaixacomposicao.cddependenciafaixacomposicao, dependenciafaixacomposicao.faixade, " +
				"dependenciafaixacomposicao.faixaate, dependenciafaixacomposicao.quantidade, " +
				"contagerencial.cdcontagerencial, contagerencial.nome, referenciacalculo.cdreferenciacalculo, referenciacalculo.nome")
				
			.join("composicaoorcamento.orcamento orcamento")
			.join("composicaoorcamento.listaRecursocomposicao recursocomposicao")
			.join("recursocomposicao.tipoitemcomposicao tipoitemcomposicao")
			.leftOuterJoin("recursocomposicao.referenciacalculo referenciacalculo")
			.leftOuterJoin("recursocomposicao.formulacomposicao formulacomposicao")
			.leftOuterJoin("composicaoorcamento.contagerencial contagerencial")
			.join("recursocomposicao.tiporelacaorecurso tiporelacaorecurso")
			.join("recursocomposicao.tipodependenciarecurso tipodependenciarecurso")
			.join("recursocomposicao.tipoocorrencia tipoocorrencia")
			.leftOuterJoin("recursocomposicao.material material")
			.leftOuterJoin("recursocomposicao.materialgrupo materialgrupo")
			.leftOuterJoin("recursocomposicao.unidademedida unidademedida")
			.leftOuterJoin("recursocomposicao.listaDependenciacargocomposicao dependenciacargocomposicao")
			.leftOuterJoin("dependenciacargocomposicao.cargo cargoOrigem")
			.leftOuterJoin("recursocomposicao.listaDependenciafaixacomposicao dependenciafaixacomposicao")
			.orderBy("composicaoorcamento.nome, recursocomposicao.nome, cargoOrigem.nome, dependenciafaixacomposicao.faixade, dependenciafaixacomposicao.faixaate");
	}
	
	public List<Composicaoorcamento> findForCopiaOrcamento(String whereIn){
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
			.select(
				"composicaoorcamento.cdcomposicaoorcamento, composicaoorcamento.nome, " +
				"orcamento.cdorcamento, orcamento.nome, " +
				"recursocomposicao.cdrecursocomposicao, recursocomposicao.nome, recursocomposicao.quantidade, recursocomposicao.quantidadecalculada, recursocomposicao.custounitario, recursocomposicao.numocorrencia, " +
				"tipoitemcomposicao.cdtipoitemcomposicao, tipoitemcomposicao.nome, " +
				"formulacomposicao.cdformulacomposicao, formulacomposicao.nome, " +						
				"tiporelacaorecurso.cdtiporelacaorecurso, tiporelacaorecurso.nome, " +
				"tipodependenciarecurso.cdtipodependenciarecurso, tipodependenciarecurso.nome, " +
				"tipoocorrencia.cdtipoocorrencia, tipoocorrencia.nome, " +
				"material.cdmaterial, material.nome, " +
				"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
				"dependenciacargocomposicao.cddependenciacargocomposicao, " +
				"cargoOrigem.cdcargo, cargoOrigem.nome, " +
				"dependenciafaixacomposicao.cddependenciafaixacomposicao, dependenciafaixacomposicao.faixade, dependenciafaixacomposicao.faixaate, dependenciafaixacomposicao.quantidade")
				
			.join("composicaoorcamento.orcamento orcamento")
			.join("composicaoorcamento.listaRecursocomposicao recursocomposicao")
			.join("recursocomposicao.tipoitemcomposicao tipoitemcomposicao")
			.leftOuterJoin("recursocomposicao.formulacomposicao formulacomposicao")
			.join("recursocomposicao.tiporelacaorecurso tiporelacaorecurso")
			.join("recursocomposicao.tipodependenciarecurso tipodependenciarecurso")
			.join("recursocomposicao.tipoocorrencia tipoocorrencia")
			.leftOuterJoin("recursocomposicao.material material")
			.leftOuterJoin("recursocomposicao.unidademedida unidademedida")
			.leftOuterJoin("recursocomposicao.listaDependenciacargocomposicao dependenciacargocomposicao")
			.leftOuterJoin("dependenciacargocomposicao.cargo cargoOrigem")
			.leftOuterJoin("recursocomposicao.listaDependenciafaixacomposicao dependenciafaixacomposicao")
			.whereIn("composicaoorcamento.cdcomposicaoorcamento", whereIn)
			.list();
	}
	
	@Override
	public void saveOrUpdateNoUseTransaction(Composicaoorcamento bean) {
		try {
			super.saveOrUpdateNoUseTransaction(bean);
		}
		catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "UK_COMPOSICAOORCAMENTO_1")) {
				throw new SinedException("J� existe uma composi��o cadastrada com o nome " + bean.getNome() + ".");
			}			
			throw new SinedException(e.getMessage());
		}
	}
}
