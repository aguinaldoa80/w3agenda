package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Documentoenvioboleto;
import br.com.linkcom.sined.geral.bean.Documentoenvioboletocliente;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentoenvioboletoclienteDAO extends GenericDAO<Documentoenvioboletocliente> {

	/**
	 * Busca a lista de documentoenvioboletocliente
	 * 	 
	 * @param documentoenvioboleto
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/07/2018
	 */
	public List<Documentoenvioboletocliente> findByDocumentoenvioboleto(Documentoenvioboleto documentoenvioboleto) {
		if(documentoenvioboleto == null || documentoenvioboleto.getCddocumentoenvioboleto() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("documentoenvioboletocliente.cddocumentoenvioboletocliente, cliente.cdpessoa, cliente.nome")
					.join("documentoenvioboletocliente.cliente cliente")
					.where("documentoenvioboletocliente.documentoenvioboleto = ?", documentoenvioboleto)
					.orderBy("cliente.nome")
					.list();
	}
	
}
