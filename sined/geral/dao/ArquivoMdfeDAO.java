package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.ArquivoMdfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.enumeration.ArquivoMdfeSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.MdfeSituacao;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.ArquivoMdfeFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ArquivoMdfeDAO extends GenericDAO<ArquivoMdfe> {

	@Override
	public void updateListagemQuery(QueryBuilder<ArquivoMdfe> query, FiltroListagem _filtro) {
		ArquivoMdfeFiltro filtro = (ArquivoMdfeFiltro) _filtro;
		
		Integer cdMdfe = null;
		String cdArquivoMdfe = NeoWeb.getRequestContext().getParameter("cdMdfe");
		if(cdArquivoMdfe != null && !cdArquivoMdfe.equals("")){
			cdMdfe = Integer.parseInt(cdArquivoMdfe);
		}
		
		if(filtro.getIds() != null && !filtro.getIds().equals("")){
			while(filtro.getIds().indexOf(",,") != -1){
				filtro.setIds(filtro.getIds().replaceAll(",,", ","));
			}
			
			if(filtro.getIds().substring(0, 1).equals(",")){
				filtro.setIds(filtro.getIds().substring(1, filtro.getIds().length()));
			}
			
			if(filtro.getIds().substring(filtro.getIds().length() - 1, filtro.getIds().length()).equals(",")){
				filtro.setIds(filtro.getIds().substring(0, filtro.getIds().length()-1));
			}
		}
		
		query
			.select("distinct arquivoMdfe.cdArquivoMdfe, arquivoMdfe.dtenvio, arquivoMdfe.arquivoMdfeSituacao, " +
					"arquivoxml.cdarquivo, arquivoxml.nome, " +
					"arquivoxmlassinado.cdarquivo, arquivoxmlassinado.nome, " +
					"arquivoretornoenvio.cdarquivo, arquivoretornoenvio.nome, " +
					"arquivoretornoconsulta.cdarquivo, arquivoretornoconsulta.nome, " +
					"configuracaonfe.tipoconfiguracaonfe, configuracaonfe.descricao, configuracaonfe.prefixowebservice")
			.leftOuterJoin("arquivoMdfe.arquivoxml arquivoxml")
			.leftOuterJoin("arquivoMdfe.arquivoxmlassinado arquivoxmlassinado")
			.leftOuterJoin("arquivoMdfe.arquivoretornoenvio arquivoretornoenvio")
			.leftOuterJoin("arquivoMdfe.arquivoretornoconsulta arquivoretornoconsulta")
			.leftOuterJoin("arquivoMdfe.configuracaonfe configuracaonfe")
			.leftOuterJoin("arquivoMdfe.mdfe mdfe")
			.leftOuterJoin("arquivoMdfe.empresa empresa")
			.where("configuracaonfe = ?", filtro.getConfiguracaonfe())
			.wherePeriodo("arquivoMdfe.dtenvio", filtro.getDtenvio1(), filtro.getDtenvio2())
			.where("mdfe.numero = ?", filtro.getNumeroNota())
			.where("mdfe.cdmdfe = ?", cdMdfe)
			.where("empresa = ?", filtro.getEmpresa())
			.whereIn("arquivoMdfe.cdArquivoMdfe", SinedUtil.montaWhereInInteger(filtro.getIds()))
			.orderBy("arquivoMdfe.cdArquivoMdfe desc");
		
		if(filtro.getListaSituacao() != null) {
			query.whereIn("arquivoMdfe.arquivoMdfeSituacao", ArquivoMdfeSituacao.listAndConcatenate(filtro.getListaSituacao()));
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<ArquivoMdfe> query) {
		query
			.leftOuterJoinFetch("arquivoMdfe.listaArquivoMdfeErro listaArquivoMdfeErro")
			.leftOuterJoinFetch("arquivoMdfe.mdfe mdfe")
			.leftOuterJoinFetch("arquivoMdfe.arquivoxml arquivoxml")
			.leftOuterJoinFetch("arquivoMdfe.arquivoxmlconsultasituacao arquivoxmlconsultasituacao")
			.leftOuterJoinFetch("arquivoMdfe.arquivoxmlconsultalote arquivoxmlconsultalote")
			.leftOuterJoinFetch("arquivoMdfe.arquivoxmlassinado arquivoxmlassinado")
			.leftOuterJoinFetch("arquivoMdfe.arquivoretornoenvio arquivoretornoenvio")
			.leftOuterJoinFetch("arquivoMdfe.arquivoretornoconsulta arquivoretornoconsulta")
			.leftOuterJoinFetch("arquivoMdfe.arquivoretornoconsultasituacao arquivoretornoconsultasituacao")
			.leftOuterJoinFetch("arquivoMdfe.configuracaonfe configuracaonfe")
			.leftOuterJoinFetch("arquivoMdfe.arquivoxmlencerramento arquivoxmlencerramento");
	}
	
	public ArquivoMdfe loadByMdfe(Mdfe mdfe, ArquivoMdfeSituacao situacao) {
		if(mdfe == null || mdfe.getCdmdfe() == null){
			throw new SinedException("O id do arquivo de MDFe n�o pode ser nulo.");
		}
		return query()
				.select("arquivoMdfe.cdArquivoMdfe, mdfe.cdmdfe, arquivoMdfe.arquivoMdfeSituacao, arquivoMdfe.chaveacesso, " +
						"arquivoxmlassinado.cdarquivo, arquivoretornoconsulta.cdarquivo")
				.join("arquivoMdfe.mdfe mdfe")
				.join("arquivoMdfe.arquivoxmlassinado arquivoxmlassinado")
				.join("arquivoMdfe.arquivoretornoconsulta arquivoretornoconsulta")
				.where("arquivoMdfe.arquivoMdfeSituacao = ?", situacao)
				.where("arquivoMdfe.mdfe = ?", mdfe)
				.unique();
	}
	
	public List<ArquivoMdfe> findGeradosByMdfe(Mdfe mdfe) {
		if(mdfe == null || mdfe.getCdmdfe() == null){
			throw new SinedException("O id do arquivo de MDFe n�o pode ser nulo.");
		}
		return query()
					.select("arquivoMdfe.cdArquivoMdfe, mdfe.cdmdfe, arquivoMdfe.arquivoMdfeSituacao ")
					.join("arquivoMdfe.mdfe mdfe")
					.where("arquivoMdfe.arquivoMdfeSituacao = ?", ArquivoMdfeSituacao.GERADO)
					.list();
	}
	
	public List<ArquivoMdfe> findByMdfe(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			return new ArrayList<ArquivoMdfe>();
		}
		return query()
					.select("arquivoMdfe.cdArquivoMdfe, arquivoMdfe.numerorecibo, arquivoMdfe.dtrecebimento, arquivoMdfe.dtprocessamento, " +
							"arquivoMdfe.protocolomdfe, arquivoMdfe.arquivoMdfeSituacao, mdfe.cdmdfe, " +
							"arquivoxml.cdarquivo, arquivoxmlassinado.cdarquivo, arquivoretornoconsulta.cdarquivo, " +
							"arquivoMdfe.protocolomdfe, arquivoMdfe.chaveacesso, arquivoMdfe.emitindo, " +
							"configuracaonfe.prefixowebservice ")
					.join("arquivoMdfe.mdfe mdfe")
					.leftOuterJoin("arquivoMdfe.arquivoxmlassinado arquivoxmlassinado")
					.leftOuterJoin("arquivoMdfe.arquivoretornoconsulta arquivoretornoconsulta")
					.leftOuterJoin("arquivoMdfe.arquivoxml arquivoxml")
					.leftOuterJoin("arquivoMdfe.configuracaonfe configuracaonfe")
					.whereIn("mdfe.cdmdfe", whereIn)
					.list();
	}
	
	public ArquivoMdfe loadForEncerramento(Mdfe mdfe) {
		if(mdfe == null || mdfe.getCdmdfe() == null){
			throw new SinedException("Mdfe n�o pode ser nula.");
		}
		
		return query()
			.select("arquivoMdfe.cdArquivoMdfe, arquivoMdfe.numerorecibo, arquivoMdfe.dtrecebimento, arquivoMdfe.dtprocessamento, " +
					"arquivoMdfe.protocolomdfe, arquivoMdfe.arquivoMdfeSituacao, mdfe.cdmdfe, " +
					"arquivoxml.cdarquivo, arquivoxmlassinado.cdarquivo, arquivoretornoconsulta.cdarquivo, " +
					"arquivoMdfe.protocolomdfe, arquivoMdfe.chaveacesso, arquivoMdfe.emitindo, " +
					"configuracaonfe.cdconfiguracaonfe ")
			.join("arquivoMdfe.mdfe mdfe")
			.leftOuterJoin("arquivoMdfe.arquivoxmlassinado arquivoxmlassinado")
			.leftOuterJoin("arquivoMdfe.arquivoretornoconsulta arquivoretornoconsulta")
			.leftOuterJoin("arquivoMdfe.arquivoxml arquivoxml")
			.leftOuterJoin("arquivoMdfe.configuracaonfe configuracaonfe")
			.where("arquivoMdfe.dtencerramento is null")
			.where("arquivoMdfe.chaveacesso is not null")
			.where("arquivoMdfe.protocolomdfe is not null")
			.where("arquivoMdfe.dtprocessamento is not null")
			.where("arquivoMdfe.arquivoMdfeSituacao = ?", ArquivoMdfeSituacao.PROCESSADO_COM_SUCESSO)
			.where("mdfe = ?", mdfe)
			.unique();
	}
	
	public int marcarArquivoMdfe(String whereIn, String flag) {
		return getHibernateTemplate()
				.bulkUpdate("update ArquivoMdfe a set a." + flag + " = ? where a.cdArquivoMdfe in (" + whereIn + ") and (a." + flag + " = ? or a." + flag + " is null)", 
							new Object[]{Boolean.TRUE, Boolean.FALSE});
	}

	public List<ArquivoMdfe> findArquivoMdfeNovosForEmissao(Empresa empresa) {
		QueryBuilder<ArquivoMdfe> query = querySined();
		
		query
				.select("arquivoMdfe.cdArquivoMdfe, arquivoxml.cdarquivo, configuracaonfe.cdconfiguracaonfe")
				.leftOuterJoin("arquivoMdfe.empresa empresa")
				.leftOuterJoin("arquivoMdfe.arquivoxml arquivoxml")
				.leftOuterJoin("arquivoMdfe.configuracaonfe configuracaonfe")
				.where("empresa = ?", empresa)
				.where("arquivoxml is not null")
				.where("arquivoMdfe.arquivoMdfeSituacao = ?", ArquivoMdfeSituacao.GERADO)
				.openParentheses()
					.where("arquivoMdfe.emitindo = ?", Boolean.FALSE)
					.or()
					.where("arquivoMdfe.emitindo is null")
				.closeParentheses();
		
		return query.list();
	}
	
	public List<ArquivoMdfe> findArquivoMdfeNovosForConsulta(Empresa empresa) {
		QueryBuilder<ArquivoMdfe> query = querySined();
		
		query
				.select("arquivoMdfe.cdArquivoMdfe, arquivoxmlconsultalote.cdarquivo, configuracaonfe.cdconfiguracaonfe")
				.leftOuterJoin("arquivoMdfe.empresa empresa")
				.leftOuterJoin("arquivoMdfe.arquivoxmlconsultalote arquivoxmlconsultalote")
				.leftOuterJoin("arquivoMdfe.configuracaonfe configuracaonfe")
				.where("empresa = ?", empresa)
				.where("arquivoxmlconsultalote is not null")
				.where("arquivoMdfe.arquivoMdfeSituacao = ?", ArquivoMdfeSituacao.ENVIADO)
				.openParentheses()
					.where("arquivoMdfe.consultando = ?", Boolean.FALSE)
					.or()
					.where("arquivoMdfe.consultando is null")
				.closeParentheses();
		
		return query.list();
	}
	
	public void updateEncerramentoPorEvento(ArquivoMdfe arquivoMdfe, Boolean encerramentoPorEvento) {
		if(arquivoMdfe == null || arquivoMdfe.getCdArquivoMdfe() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update ArquivoMdfe a set a.encerramentoporevento = ? where a.id = ?", 
										new Object[]{encerramentoPorEvento, arquivoMdfe.getCdArquivoMdfe()});
		
	}
	
	public void updateCancelamentoPorEvento(ArquivoMdfe arquivoMdfe, Boolean cancelamentoPorEvento) {
		if(arquivoMdfe == null || arquivoMdfe.getCdArquivoMdfe() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update ArquivoMdfe a set a.cancelamentoPorEvento = ? where a.id = ?", 
										new Object[]{cancelamentoPorEvento, arquivoMdfe.getCdArquivoMdfe()});
		
	}
	
	public void updateInclusaoCondutorPorEvento(ArquivoMdfe arquivoMdfe, Boolean inclusaoCondutorPorEvento) {
		if(arquivoMdfe == null || arquivoMdfe.getCdArquivoMdfe() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update ArquivoMdfe a set a.inclusaoCondutorPorEvento = ? where a.id = ?", 
										new Object[]{inclusaoCondutorPorEvento, arquivoMdfe.getCdArquivoMdfe()});
		
	}
	
	public void updateArquivoxmlEncerrando(ArquivoMdfe arquivoMdfe) {
		if(arquivoMdfe == null || arquivoMdfe.getCdArquivoMdfe() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update ArquivoMdfe a set a.arquivoxmlencerramento = ?, a.encerrando = ? where a.id = ?", 
										new Object[]{arquivoMdfe.getArquivoxmlencerramento(), Boolean.FALSE, arquivoMdfe.getCdArquivoMdfe()});
		
	}
	
	public void updateArquivoXmlCancelando(ArquivoMdfe arquivoMdfe) {
		if(arquivoMdfe == null || arquivoMdfe.getCdArquivoMdfe() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update ArquivoMdfe a set a.arquivoXmlCancelamento = ?, a.cancelando = ? where a.id = ?", 
										new Object[]{arquivoMdfe.getArquivoXmlCancelamento(), Boolean.FALSE, arquivoMdfe.getCdArquivoMdfe()});
		
	}
	
	public void updateArquivoXmlInclusaoCondutor(ArquivoMdfe arquivoMdfe) {
		if(arquivoMdfe == null || arquivoMdfe.getCdArquivoMdfe() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update ArquivoMdfe a set a.arquivoXmlInclusaoCondutor = ?, a.incluindoCondutor = ? where a.id = ?", 
										new Object[]{arquivoMdfe.getArquivoXmlInclusaoCondutor(), Boolean.FALSE, arquivoMdfe.getCdArquivoMdfe()});
		
	}
	
	public void updateArquivoxmlRetornoEncerramento(ArquivoMdfe arquivoMdfe) {
		if(arquivoMdfe == null || arquivoMdfe.getCdArquivoMdfe() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update ArquivoMdfe a set a.arquivoxmlretornoencerramento = ?, a.encerrando = ? where a.id = ?", 
										new Object[]{arquivoMdfe.getArquivoxmlretornoencerramento(), Boolean.FALSE, arquivoMdfe.getCdArquivoMdfe()});
		
	}

	public void updateArquivoxmlassinado(ArquivoMdfe arquivoMdfe) {
		getHibernateTemplate().bulkUpdate("update ArquivoMdfe a set a.arquivoxmlassinado = ? where a.id = ?", 
				new Object[]{arquivoMdfe.getArquivoxmlassinado(), arquivoMdfe.getCdArquivoMdfe()});
	}

	public void updateArquivoretornoenvio(ArquivoMdfe arquivoMdfe) {
		getHibernateTemplate().bulkUpdate("update ArquivoMdfe a set a.arquivoretornoenvio = ?, dtenvio = ? where a.id = ?", 
				new Object[]{arquivoMdfe.getArquivoretornoenvio(), SinedDateUtils.currentDate(), arquivoMdfe.getCdArquivoMdfe()});
	}

	public void updateSituacao(ArquivoMdfe arquivoMdfe,	ArquivoMdfeSituacao situacao) {
		getHibernateTemplate().bulkUpdate("update ArquivoMdfe a set a.arquivoMdfeSituacao = ? where a.id = ?", 
				new Object[]{situacao, arquivoMdfe.getCdArquivoMdfe()});
	}
	
	public void updateConsultando(ArquivoMdfe arquivoMdfe, Boolean consultando) {
		getHibernateTemplate().bulkUpdate("update ArquivoMdfe a set a.consultando = ? where a.id = ?", 
				new Object[]{consultando, arquivoMdfe.getCdArquivoMdfe()});
	}

	public void updateReciboDatarecebimento(ArquivoMdfe arquivoMdfe, String numerorecibo, Timestamp dtrecebimento) {
		getHibernateTemplate().bulkUpdate("update ArquivoMdfe a set a.numerorecibo = ?, a.dtrecebimento = ? where a.id = ?", 
				new Object[]{numerorecibo, dtrecebimento, arquivoMdfe.getCdArquivoMdfe()});
	}

	public void updateArquivoxmlconsultalote(ArquivoMdfe arquivoMdfe) {
		getHibernateTemplate().bulkUpdate("update ArquivoMdfe a set a.arquivoxmlconsultalote = ? where a.id = ?", 
				new Object[]{arquivoMdfe.getArquivoxmlconsultalote(), arquivoMdfe.getCdArquivoMdfe()});
	}

	public void updateArquivoretornoconsulta(ArquivoMdfe arquivoMdfe) {
		getHibernateTemplate().bulkUpdate("update ArquivoMdfe a set a.arquivoretornoconsulta = ? where a.id = ?", 
				new Object[]{arquivoMdfe.getArquivoretornoconsulta(), arquivoMdfe.getCdArquivoMdfe()});
	}

	public void updateArquivoMdfenota(ArquivoMdfe arquivoMdfe, Timestamp dataRec, String nProtStr, String chMDFeStr) {
		getHibernateTemplate().bulkUpdate("update ArquivoMdfe a set a.dtprocessamento = ?, a.protocolomdfe = ?, chaveacesso = ? where a.id = ?",
				new Object[]{dataRec, nProtStr, chMDFeStr, arquivoMdfe.getCdArquivoMdfe()});
	}
	
	public List<ArquivoMdfe> findNovosEncerramentos(Empresa empresa) {
		return querySined()
					
					.select("arquivoMdfe.cdArquivoMdfe, arquivoxmlencerramento.cdarquivo, configuracaonfe.cdconfiguracaonfe ")
					.join("arquivoMdfe.arquivoxmlencerramento arquivoxmlencerramento")
					.join("arquivoMdfe.mdfe mdfe")
					.join("arquivoMdfe.empresa empresa")
					.join("arquivoMdfe.configuracaonfe configuracaonfe")
					.where("arquivoxmlencerramento is not null")
					.where("arquivoMdfe.dtencerramento is null")
					.where("empresa = ?", empresa)
					.where("mdfe.mdfeSituacao = ?", MdfeSituacao.EM_ENCERRAMENTO)
					.where("arquivoMdfe.arquivoMdfeSituacao = ?", ArquivoMdfeSituacao.PROCESSADO_COM_SUCESSO)
					.openParentheses()
						.where("arquivoMdfe.encerrando = ?", Boolean.FALSE)
						.or()
						.where("arquivoMdfe.encerrando is null")
					.closeParentheses()
					.where("arquivoMdfe.encerramentoporevento = true")
					.list();
	}
	
	public List<ArquivoMdfe> findNovosCancelamentos(Empresa empresa) {
		return querySined()
					
					.select("arquivoMdfe.cdArquivoMdfe, arquivoXmlCancelamento.cdarquivo, configuracaonfe.cdconfiguracaonfe ")
					.join("arquivoMdfe.arquivoXmlCancelamento arquivoXmlCancelamento")
					.join("arquivoMdfe.mdfe mdfe")
					.join("arquivoMdfe.empresa empresa")
					.join("arquivoMdfe.configuracaonfe configuracaonfe")
					.where("arquivoXmlCancelamento is not null")
					.where("arquivoMdfe.dtcancelamento is null")
					.where("empresa = ?", empresa)
					.where("mdfe.mdfeSituacao = ?", MdfeSituacao.EM_CANCELAMENTO)
					.where("arquivoMdfe.arquivoMdfeSituacao = ?", ArquivoMdfeSituacao.PROCESSADO_COM_SUCESSO)
					.openParentheses()
						.where("arquivoMdfe.cancelando = ?", Boolean.FALSE)
						.or()
						.where("arquivoMdfe.cancelando is null")
					.closeParentheses()
					.where("arquivoMdfe.cancelamentoPorEvento = true")
					.list();
	}
	
	public List<ArquivoMdfe> findNovosInclusaoCondutor(Empresa empresa) {
		return querySined()
					
					.select("arquivoMdfe.cdArquivoMdfe, arquivoXmlInclusaoCondutor.cdarquivo, configuracaonfe.cdconfiguracaonfe ")
					.join("arquivoMdfe.arquivoXmlInclusaoCondutor arquivoXmlInclusaoCondutor")
					.join("arquivoMdfe.mdfe mdfe")
					.join("arquivoMdfe.empresa empresa")
					.join("arquivoMdfe.configuracaonfe configuracaonfe")
					.where("arquivoXmlInclusaoCondutor is not null")
					.where("arquivoMdfe.dtInclusaoCondutor is null")
					.where("empresa = ?", empresa)
					.where("mdfe.mdfeSituacao = ?", MdfeSituacao.EM_INCLUSAO_CONDUTOR)
					.where("arquivoMdfe.arquivoMdfeSituacao = ?", ArquivoMdfeSituacao.PROCESSADO_COM_SUCESSO)
					.openParentheses()
						.where("arquivoMdfe.incluindoCondutor = ?", Boolean.FALSE)
						.or()
						.where("arquivoMdfe.incluindoCondutor is null")
					.closeParentheses()
					.where("arquivoMdfe.inclusaoCondutorPorEvento = true")
					.list();
	}

	public List<ArquivoMdfe> findByMdfeAutorizado(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			return new ArrayList<ArquivoMdfe>();
		}
		return query()
					.select("arquivoMdfe.cdArquivoMdfe, arquivoMdfe.numerorecibo, arquivoMdfe.dtrecebimento, arquivoMdfe.dtprocessamento, " +
							"arquivoMdfe.protocolomdfe, arquivoMdfe.arquivoMdfeSituacao, mdfe.cdmdfe, " +
							"arquivoxml.cdarquivo, arquivoxmlassinado.cdarquivo, arquivoretornoconsulta.cdarquivo, " +
							"arquivoMdfe.protocolomdfe, arquivoMdfe.chaveacesso, arquivoMdfe.emitindo, " +
							"configuracaonfe.prefixowebservice ")
					.join("arquivoMdfe.mdfe mdfe")
					.leftOuterJoin("arquivoMdfe.arquivoxmlassinado arquivoxmlassinado")
					.leftOuterJoin("arquivoMdfe.arquivoretornoconsulta arquivoretornoconsulta")
					.leftOuterJoin("arquivoMdfe.arquivoxml arquivoxml")
					.leftOuterJoin("arquivoMdfe.configuracaonfe configuracaonfe")
					.whereIn("mdfe.cdmdfe", whereIn)
					.where("arquivoMdfe.arquivoMdfeSituacao = ?", ArquivoMdfeSituacao.PROCESSADO_COM_SUCESSO)
					.list();
	}

	public void updateArquivoxmlRetornoCancelamento(ArquivoMdfe arquivoMdfe) {
		if(arquivoMdfe == null || arquivoMdfe.getCdArquivoMdfe() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		
		getHibernateTemplate().bulkUpdate("update ArquivoMdfe a set a.arquivoXmlRetornoCancelamento = ?, a.cancelando = ? where a.id = ?", 
										new Object[]{arquivoMdfe.getArquivoXmlRetornoCancelamento(), Boolean.FALSE, arquivoMdfe.getCdArquivoMdfe()});
	}
	
	public void updateArquivoxmlRetornoInclusaoCondutor(ArquivoMdfe arquivoMdfe) {
		if(arquivoMdfe == null || arquivoMdfe.getCdArquivoMdfe() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		
		getHibernateTemplate().bulkUpdate("update ArquivoMdfe a set a.arquivoXmlRetornoInclusaoCondutor = ?, a.incluindoCondutor = ? where a.id = ?", 
										new Object[]{arquivoMdfe.getArquivoXmlRetornoInclusaoCondutor(), Boolean.FALSE, arquivoMdfe.getCdArquivoMdfe()});
	}

	public void desmarcarArquivoMdfe(Integer cdArquivoMdfe, String flag) {
		getHibernateTemplate().bulkUpdate("update ArquivoMdfe a set a." + flag + " = ? where a.cdArquivoMdfe in (" + cdArquivoMdfe + ")", 
				new Object[]{Boolean.FALSE});
	}
}
