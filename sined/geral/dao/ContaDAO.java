package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.LcdprArquivoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("conta.nome")
public class ContaDAO extends GenericDAO<Conta>{
	
	/**
	 * M�todo para obter o saldo inicial de uma conta.
	 * 
	 * @param conta
	 * @return
	 * @author Flavio Tavares
	 */
	public Money obterSaldoInicial(Conta conta){
		if(conta == null || conta.getCdconta() == null){
			throw new SinedException("O par�metros conta ou cdconta n�o podem ser null.");
		}
		SinedUtil.markAsReader();
		return newQueryBuilderWithFrom(Money.class)
			.select("saldo")
			.where("conta = ?",conta)
			.unique();
	}
	
	/**
	 * M�todo para obter lista de contas por tipo.
	 * 
	 * @param empresa 
	 * @param contatipo
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Conta> findByTipo(Contatipo contatipo, Empresa empresa, boolean hasContaDigital){
		if(contatipo == null || contatipo.getCdcontatipo() == null){
			throw new SinedException("Os par�metros contatipo ou cdcontatipo n�o podem ser null.");
		}
		QueryBuilder<Conta> query = querySined();
		
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		query
			.select("conta.cdconta,conta.nome,conta.agencia,conta.numero,banco.nome, banco.cdbanco")
			.leftOuterJoin("conta.banco banco")
			.leftOuterJoin("conta.listaContaempresa listaContaempresa")
			.leftOuterJoin("listaContaempresa.empresa empresa")
			.leftOuterJoin("conta.listaContapapel listaContapapel")
			.leftOuterJoin("listaContapapel.papel papel")
			.where("conta.contatipo = ?",contatipo)
			.where("conta.ativo = ?",Boolean.TRUE)
			.orderBy("conta.nome");
		
			if(empresa != null && empresa.getCdpessoa() != null){
				query
				.openParentheses()
				.where("empresa = ?",empresa)
				.or()
				.where("empresa is null")
				.closeParentheses();
			}
			
			if (!administrador){
				if (whereInPapel != null && !whereInPapel.equals("")){
					query.openParentheses();
					query.whereIn("papel.cdpapel", whereInPapel)
					.or()
					.where("papel.cdpapel is null");
					query.closeParentheses();
				} else {
					query.where("papel.cdpapel is null");
				}
			}
			
			if(hasContaDigital){
				query.where("exists(select 1 from Contadigital c where c.conta = conta)");
			}
		
		return query.list();
	}
	
	/**
	 * M�todo para obter o saldo de <b>todas as contas</b> tendo como refer�ncia uma data. Utiliza a fun��o SALDOATUAL no banco.
	 * PS.: A query n�o foi feita utilizando o Hibernate pois este d� problema quando se usa fun��o no banco com mais de um par�metro.
	 * 
	 * @see #queryForMoney(String, Object[])
	 * @param date
	 * @param empresas
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Money calculaSaldoAtual(Date dataReferencia, Boolean mostrarContasInativadas, String whereInNatureza, Empresa... empresas){
		return calculaSaldoAtual(dataReferencia, mostrarContasInativadas, whereInNatureza, false, empresas);
	}
	
	public Money calculaSaldoAtual(Date dataReferencia, Boolean mostrarContasInativadas, String whereInNatureza, Boolean byFluxoCaixa, Empresa... empresas){
		if(dataReferencia == null){
			throw new SinedException("O par�metro dataReferencia n�o pode ser null.");
		}
		
		SinedUtil.markAsReader();
		String cdUsuarioLogado = SinedUtil.getUsuarioLogado() != null ? SinedUtil.getUsuarioLogado().getCdpessoa().toString() : "NULL";
		
		String sql = "select sum(SALDOATUAL_NATUREZA(c.cdconta, ?, " + cdUsuarioLogado + ", '" + whereInNatureza + "')) as soma " +
						"from conta c " +
						(empresas.length > 0 || mostrarContasInativadas == null || !mostrarContasInativadas ? " where 1 = 1 " : "") + 
						(mostrarContasInativadas == null || !mostrarContasInativadas ? " and c.ativo = true " : "") +
						(empresas.length > 0 ? " and cdempresa in (" + SinedUtil.listAndConcatenateIDs(Arrays.asList(empresas)) + ")" : "");
		if(Boolean.TRUE.equals(byFluxoCaixa)){
			sql += " and coalesce(c.desconsiderarsaldoinicialfluxocaixa, false) <> true ";
		}
		return this.queryForMoney(sql, new Object[]{dataReferencia});
	}
	
	/**
	 * M�todo para obter o saldo de <b>uma conta</b> tendo como refer�ncia uma data. Utiliza a fun��o SALDOATUAL no banco.
	 * PS.: A query n�o foi feita utilizando o Hibernate pois este d� problema quando se usa fun��o no banco com mais de um par�metro.
	 * 
	 * @see #queryForMoney(String, Object[])
	 * @param conta
	 * @param date
	 * @param empresas
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Money calculaSaldoAtual(Conta conta, Date date, Empresa... empresas) {	
		if(conta==null || conta.getCdconta()==null)
			throw new SinedException("N�o foi informado o c�digo da conta");
		
		String cdUsuarioLogado = SinedUtil.getUsuarioLogado() != null ? SinedUtil.getUsuarioLogado().getCdpessoa().toString() : "NULL";
		
		SinedUtil.markAsReader();
		String sql = "select SALDOATUAL(CDCONTA, ?, " + cdUsuarioLogado + ") " +
						"from conta where cdconta = "+conta.getCdconta() + " " +
						(empresas.length > 0 ? "and cdempresa in (" + SinedUtil.listAndConcatenateIDs(Arrays.asList(empresas)) + ")" : "");
		
		return this.queryForMoney(sql, new Object[]{date});
	}
	
	public Money calculaSaldoAtualConciliada(Conta conta, Date date, Empresa... empresas) {	
		if(conta==null || conta.getCdconta()==null)
			throw new SinedException("N�o foi informado o c�digo da conta");
		
		SinedUtil.markAsReader();
		String sql = "select SALDOATUAL_CONCILIADAS(CDCONTA, ?, " + SinedUtil.getUsuarioLogado().getCdpessoa() + ") " +
		"from conta where cdconta = "+conta.getCdconta() + " " +
		(empresas.length > 0 ? "and cdempresa in (" + SinedUtil.listAndConcatenateIDs(Arrays.asList(empresas)) + ")" : "");
		
		return this.queryForMoney(sql, new Object[]{date});
	}
	
	public Money calculaSaldoAtualNormal(Conta conta, Date date, Empresa... empresas) {	
		if(conta==null || conta.getCdconta()==null)
			throw new SinedException("N�o foi informado o c�digo da conta");
		
		SinedUtil.markAsReader();
		String sql = "select SALDOATUAL_NORMAL(CDCONTA, ?, " + SinedUtil.getUsuarioLogado().getCdpessoa() + ") " +
		"from conta where cdconta = "+conta.getCdconta() + " " +
		(empresas.length > 0 ? "and cdempresa in (" + SinedUtil.listAndConcatenateIDs(Arrays.asList(empresas)) + ")" : "");
		
		return this.queryForMoney(sql, new Object[]{date});
	}
	
	/**
	 * M�todo para obter o saldo de uma <b>lista de contas</b> tendo como refer�ncia uma data. Utiliza a fun��o SALDOATUAL no banco.
	 * Os ID's das contas devem estar concatenados e separados por v�rgula. � utilizado WHERE IN no sql. 
	 * PS.: A query n�o foi feita utilizando o Hibernate pois este d� problema quando se usa fun��o no banco com mais de um par�metro.
	 * 
	 * @see #queryForMoney(String, Object[])
	 * @param whereIn
	 * @param date
	 * @param empresas
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Money calculaSaldoAtual(String whereIn, Date dataReferencia, Boolean mostrarContasInativadas, String whereInNatureza, Empresa... empresas){
		return calculaSaldoAtual(whereIn, dataReferencia, mostrarContasInativadas, whereInNatureza, false, empresas);
	}
	
	public Money calculaSaldoAtual(String whereIn, Date dataReferencia, Boolean mostrarContasInativadas, String whereInNatureza, Boolean byFluxoCaixa, Empresa... empresas){
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Condi��o do Where In n�o informado.");
		}
		
		String cdUsuarioLogado = SinedUtil.getUsuarioLogado() != null ? SinedUtil.getUsuarioLogado().getCdpessoa().toString() : "NULL";
		
		SinedUtil.markAsReader();
		String sql = "select sum(SALDOATUAL_NATUREZA(c.cdconta, ?, " + cdUsuarioLogado + ", '" + whereInNatureza + "')) as soma " +
						"from conta c " +
						"where c.cdconta in ("+whereIn+") " +
						(mostrarContasInativadas == null || !mostrarContasInativadas ? " and c.ativo = true " : "") +
						(empresas.length > 0 ? "and exists (select ce.cdconta from contaempresa ce where ce.cdconta = c.cdconta and ce.cdempresa in (" + SinedUtil.listAndConcatenateIDs(Arrays.asList(empresas)) + ") )" : "");
		if(Boolean.TRUE.equals(byFluxoCaixa)){
			sql += " and coalesce(c.desconsiderarsaldoinicialfluxocaixa, false) <> true ";
		}
		return this.queryForMoney(sql, new Object[]{dataReferencia});
	}
	
	/**
	 * M�todo para obter o saldo de uma <b>lista de contas</b> tendo como refer�ncia uma data. Utiliza a fun��o SALDOATUAL no banco.
	 * Os ID's dos tipos de contas devem estar concatenados e separados por v�rgula. � utilizado WHERE IN no sql. 
	 * PS.: A query n�o foi feita utilizando o Hibernate pois este d� problema quando se usa fun��o no banco com mais de um par�metro.
	 * 
	 * @see #queryForMoney(String, Object[])
	 * @param whereIn
	 * @param date
	 * @param empresas
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Money calculaSaldoAtualByContatipo(String whereIn, Date dtReferencia, Empresa... empresas){
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Condi��o do Where In n�o informado.");
		}
		
		String cdUsuarioLogado = SinedUtil.getUsuarioLogado() != null ? SinedUtil.getUsuarioLogado().getCdpessoa().toString() : "NULL";
		
		SinedUtil.markAsReader();
		String sql = "select sum(SALDOATUAL(c.cdconta, ?, " + cdUsuarioLogado + ")) as soma " +
						"from conta c " +
						"where c.cdcontatipo in ("+whereIn+") " +
						(empresas.length > 0 ? "and exists (select ce.cdconta from contaempresa ce where ce.cdconta = c.cdconta and ce.cdempresa in (" + SinedUtil.listAndConcatenateIDs(Arrays.asList(empresas)) + ") )" : "");
				
		return this.queryForMoney(sql, new Object[]{dtReferencia});
	}
	
	/**
	 * M�todo para obter o saldo de <b>todas as contas que s�o de um determinado tipo</b> tendo como refer�ncia uma data. 
	 * Utiliza a fun��o SALDOATUAL no banco.
	 * PS.: A query n�o foi feita utilizando o Hibernate pois este d� problema quando se usa fun��o no banco com mais de um par�metro.
	 * 
	 * @see #queryForMoney(String, Object[])
	 * @param contatipo
	 * @param date
	 * @param empresas
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Money calculaSaldoAtual(Contatipo contatipo, Date dataReferencia, Boolean mostrarContasInativadas, String whereInNatureza, Empresa... empresas){
		
		if(contatipo == null || contatipo.getCdcontatipo() == null){
			throw new SinedException("Os par�metros contatipo ou cdcontatipo n�o podem ser null.");
		}
		
		String cdUsuarioLogado = SinedUtil.getUsuarioLogado() != null ? SinedUtil.getUsuarioLogado().getCdpessoa().toString() : "NULL";
		
		SinedUtil.markAsReader();
		String sql = "select sum(SALDOATUAL_NATUREZA(c.cdconta, ?, " + cdUsuarioLogado + ", '" + whereInNatureza + "')) as soma  " +
						"from conta c " +
						"where c.cdcontatipo = ? " +
						(mostrarContasInativadas == null || !mostrarContasInativadas ? " and c.ativo = true " : "") +
						(empresas.length > 0 ? "and exists (select ce.cdconta from contaempresa ce where ce.cdconta = c.cdconta and ce.cdempresa in (" + SinedUtil.listAndConcatenateIDs(Arrays.asList(empresas)) + ") )" : "");
		Object[] campos = new Object[]{dataReferencia, contatipo.getCdcontatipo()};

		return this.queryForMoney(sql, campos);
	}
	
	public Money calculaSaldoAtual(Contatipo contatipo, Date dataReferencia, Boolean mostrarContasInativadas, String whereInNatureza, Boolean byFluxoCaixa, Empresa... empresas){
		
		if(contatipo == null || contatipo.getCdcontatipo() == null){
			throw new SinedException("Os par�metros contatipo ou cdcontatipo n�o podem ser null.");
		}
		
		String cdUsuarioLogado = SinedUtil.getUsuarioLogado() != null ? SinedUtil.getUsuarioLogado().getCdpessoa().toString() : "NULL";
		
		SinedUtil.markAsReader();
		String sql = "select sum(SALDOATUAL_NATUREZA(c.cdconta, ?, " + cdUsuarioLogado + ", '" + whereInNatureza + "')) as soma  " +
						"from conta c " +
						"where c.cdcontatipo = ? " +
						(mostrarContasInativadas == null || !mostrarContasInativadas ? " and c.ativo = true " : "") +
						(empresas.length > 0 ? "and exists (select ce.cdconta from contaempresa ce where ce.cdconta = c.cdconta and ce.cdempresa in (" + SinedUtil.listAndConcatenateIDs(Arrays.asList(empresas)) + ") )" : "");
		if(Boolean.TRUE.equals(byFluxoCaixa)){
			sql += " and coalesce(c.desconsiderarsaldoinicialfluxocaixa, false) <> true ";
		}
		Object[] campos = new Object[]{dataReferencia, contatipo.getCdcontatipo()};

		return this.queryForMoney(sql, campos);
	}
	
	/**
	 * Consulta SQL que resulta em um valor Money.
	 * � esperado que a consulta resulte em um valor long, o mesmo ser� transformado em Money, considerando duas casas decimais.
	 * 
	 * @param sql
	 * @param fields
	 * @return 
	 * @author Fl�vio Tavares
	 */
	private Money queryForMoney(String sql, Object[] fields){

		SinedUtil.markAsReader();
		Long result = getJdbcTemplate().queryForLong(sql, fields);
		
		return new Money(result, true);
	}
	
	/**
	 * M�todo para obter o limite de uma conta.
	 * 
	 * @see #queryForMoney(String, Object[])
	 * @param conta
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Money findLimiteConta(Conta conta){
		if(conta == null || conta.getCdconta() == null){
			throw new SinedException("Os par�metros conta ou cdconta n�o podem ser null.");
		}
		
		SinedUtil.markAsReader();
		String sql = "select limite from conta c " +
				"where c.cdconta = ?";
		Object[] fields = new Object[]{conta.getCdconta()};
		
		return this.queryForMoney(sql, fields);
	}

	/**
	 * M�todo para obter a soma dos limites de varias contas.
	 * 
	 * @see #queryForMoney(String, Object[])
	 * @param whereIn
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Money findLimiteContas(String whereIn, Empresa... empresas){
		if(whereIn == null || "".equals(whereIn)){
			throw new SinedException("O par�metro whereIn n�o pode ser null ou vazio.");
		}
		/*String sql = "select sum(limite) " +
				"from conta " +
				"where cdconta in (?)";
		Object[] fields = new Object[]{whereIn};*/
		SinedUtil.markAsReader();
		String sql = " select sum(limite) " +
					 " from conta " +
					 " where cdconta in (" + whereIn + ")" +
					 (empresas.length > 0 ? " and exists (select c1.cdconta from conta c1 " +
								"left outer join contaempresa ce on ce.cdconta = c1.cdconta " +
								"where c1.cdconta = c.cdconta " +
								"and (ce is null or ce.cdempresa in (" + SinedUtil.listAndConcatenateIDs(Arrays.asList(empresas)) + ") ) ) " : "");
		return this.queryForMoney(sql, new Object[]{});
	}
	
	/**
	 * M�todo para obter a soma dos limites de um tipo de conta
	 * 
	 * @param contatipo
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Money findLimiteContatipo(Contatipo contatipo, Empresa... empresas){
		if(contatipo == null || contatipo.getCdcontatipo() == null){
			throw new SinedException("Os par�metros contatipo ou cdcontatipo n�o podem ser null.");
		}
		SinedUtil.markAsReader();
		String sql = "select sum(c.limite) from conta c " +
						"where c.cdcontatipo = ? " +
						(empresas.length > 0 ? " and exists (select c1.cdconta from conta c1 " +
								"left outer join contaempresa ce on ce.cdconta = c1.cdconta " +
								"where c1.cdconta = c.cdconta " +
								"and (ce is null or ce.cdempresa in (" + SinedUtil.listAndConcatenateIDs(Arrays.asList(empresas)) + ") ) ) " : "");
		Object[] fields = new Object[]{contatipo.getCdcontatipo()};
		
		return this.queryForMoney(sql, fields);
	}
	
	/**
	 * Carrega a conta banc�ria informada.
	 * 
	 * @param vinculo
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Conta carregaConta(Conta vinculo) {
		if (vinculo == null || vinculo.getCdconta() == null) {
			throw new SinedException("Conta n�o pode ser nula.");
		}
		return carregaConta(vinculo, null);
	}
	
	/**
	 * Carrega a conta banc�ria informada.
	 * 
	 * @param conta
	 * @param contacarteira
	 * @return
	 * @author Marden Silva
	 */
	public Conta carregaConta(Conta conta, Contacarteira contacarteira) {
		if (conta == null || conta.getCdconta() == null) {
			throw new SinedException("Conta n�o pode ser nula.");
		}
		return querySined()
				.select("conta.cdconta, conta.nome, conta.numero, conta.dvnumero, conta.agencia, conta.dvagencia, conta.dtsaldo, conta.saldo, " +
					     	  "conta.ativo, conta.limite, conta.diavencimento, conta.diafechamento, conta.titular, conta.localpagamento, conta.codigoempresasispag, " +
					     	  "conta.nossonumerointervalo, conta.nossonumeroinicial, conta.nossonumerofinal, conta.considerarnumerodocumentoremessa, " +
							  "conta.sequencial, conta.sequencialpagamento, conta.sequencialcheque, conta.conciliacaoautomatica, conta.taxaboleto, conta.codigoempresasispag, " +				
							  "contacarteira.cdcontacarteira, contacarteira.convenio, contacarteira.conveniolider, contacarteira.carteira, contacarteira.carteiracarne, " +
							  "contacarteira.codigocarteira, contacarteira.tipoarquivoremessa, contacarteira.fatorvencimentozerado, contacarteira.msgboleto1, " +
							  "contacarteira.msgboleto2, contacarteira.bancogeranossonumero, contacarteira.naogerarboleto, " +
							  "contacarteira.idemissaoboleto, contacarteira.iddistribuicao, contacarteira.tipocobranca, " +
							  "contacarteira.instrucao1, contacarteira.instrucao2, contacarteira.qtdedias, contacarteira.limiteposicoesintervalonossonumero, " +
							  "contacarteira.especiedocumento, contacarteira.codigotransmissao, contacarteira.especiedocumentoremessa, contacarteira.aceito, " +					 
							  "contacarteira.protestoautomaticoremessa, contacarteira.enviardescontoremessa, contacarteira.enviarmultaremessa, contacarteira.enviarmoraremessa, " +
				  			  "contacarteira.padrao, contacarteira.qtdediasdevolucao, contacarteira.qtdediasdistribuicao, contacarteira.variacaocarteira, " +				
							  "contatipo.cdcontatipo, contatipo.nome, " +
							  "banco.cdbanco, banco.nome, banco.numero, banco.gerarsispag, " +
							  "empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.cpf, " +
							  "bancoConfiguracaoRemessa.cdbancoconfiguracaoremessa, bancoConfiguracaoRetorno.cdbancoconfiguracaoretorno, " +
							  "bancoConfiguracaoRemessaPagamento.cdbancoconfiguracaoremessa, bancoConfiguracaoRetornoPagamento.cdbancoconfiguracaoretorno," +
							  "empresatitular.cdpessoa, empresatitular.nome, empresatitular.razaosocial, empresatitular.nomefantasia ")
					.leftOuterJoin("conta.listaContacarteira contacarteira")
					.leftOuterJoin("contacarteira.bancoConfiguracaoRemessa bancoConfiguracaoRemessa")
					.leftOuterJoin("contacarteira.bancoConfiguracaoRetorno bancoConfiguracaoRetorno")
					.leftOuterJoin("contacarteira.bancoConfiguracaoRemessaPagamento bancoConfiguracaoRemessaPagamento")
					.leftOuterJoin("contacarteira.bancoConfiguracaoRetornoPagamento bancoConfiguracaoRetornoPagamento")
					.leftOuterJoin("conta.contatipo contatipo")
					.leftOuterJoin("conta.banco banco")
					.leftOuterJoin("conta.listaContaempresa listaContaempresa")
					.leftOuterJoin("listaContaempresa.empresa empresa")
					.leftOuterJoin("conta.empresatitular empresatitular")
					.where("conta = ?", conta)
					.where("contacarteira = ?", contacarteira)
					.unique();
	}	
	
	/**
	 * Retorna uma lista de contas do tipo conta banc�ria a partir de um banco.
	 * 
	 * @param banco
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Conta> findByBanco(Banco banco){
		if (banco == null || banco.getCdbanco() == null) {
			throw new SinedException("Banco n�o pode ser nulo.");
		}
		
		return querySined()
					.select("conta.cdconta, conta.nome")
					.where("conta.banco = ?",banco)
					.where("conta.contatipo = ?",new Contatipo(Contatipo.CONTA_BANCARIA))
					.where("conta.ativo = ?",Boolean.TRUE)
					.list();
	}

	/**
	 * Carrega as contas para confer�ncia na concilia��o banc�ria.
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 * @param codigobanco 
	 */
	public List<Conta> findAllContas(Integer codigobanco) {
		if (codigobanco == null) {
			throw new SinedException("C�digo do banco n�o pode ser nulo.");
		}
		return querySined()
					.select("conta.numero, conta.dvnumero, conta.agencia, conta.dvagencia, conta.cdconta, conta.nome")
					.join("conta.banco banco")
					.where("banco.numero = ?",codigobanco)
					.where("conta.contatipo = ?",new Contatipo(Contatipo.CONTA_BANCARIA))
					.where("conta.ativo = ?",Boolean.TRUE)
					.list();
	}
	
	/**
	 * M�todo para obter dados de uma conta para ser gerado uma movimenta��o para ela.
	 * 
	 * @param conta
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Conta findForGerarMovimentacao(Conta conta){
		if(conta == null || conta.getCdconta() == null){
			throw new SinedException("Conta ou cdconta n�o podem ser null.");
		}
		return querySined()
			.select("conta.cdconta,conta.nome,contatipo.cdcontatipo,contatipo.nome,view.saldoatual")
			.join("conta.contatipo contatipo")
			.join("conta.vconta view")
			.where("conta = ?",conta)
			.unique();
	}
	
	/**
	 * Renorna uma lista de Conta contendo apenas o campo Nome
	 * 
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Conta> findDescricao(String whereIn) {
		return querySined()
			.select("conta.nome")
			.whereIn("conta.cdconta", whereIn)
			.list();
	}
	
	/**
	 * 
	 *<p>M�todo que atualiza o sequencial da conta </p>
	 * 
	 * @param conta
	 * @author Jo�o Paulo Zica	 
	 */
	public synchronized void setProximoSequencial(Conta conta) {
		getJdbcTemplate().update("update conta set sequencial = coalesce(sequencial,0)+1 where cdconta = " + conta.getCdconta());
	}
	
	/**
	 * M�todo que atualiza o sequencialpagamento da conta
	 * 
	 * @param conta
	 * @since 06/07/2017
	 * @author Rafael Salvio
	 */
	public synchronized void setProximoSequencialpagamento(Conta conta) {
		getJdbcTemplate().update("update conta set sequencialpagamento = coalesce(sequencialpagamento,0)+1 where cdconta = " + conta.getCdconta());
	}
	
	/**
	 * M�todo que atualiza o sequencialcheque da conta
	 *
	 * @param conta
	 * @author Rodrigo Freitas
	 * @since 18/07/2017
	 */
	public synchronized void setProximoSequencialcheque(Conta conta) {
		getJdbcTemplate().update("update conta set sequencialcheque = coalesce(sequencialcheque,0)+1 where cdconta = " + conta.getCdconta());
	}

	public boolean isContaBradesco(Conta vinculo) {
		if(vinculo == null || vinculo.getCdconta() == null){
			throw new SinedException("Conta banc�ria n�o pode ser nula.");
		}
		return querySined()
					.select("conta.cdconta, conta.nome")
					.join("conta.banco banco")
					.where("conta = ?", vinculo)
					.where("banco.numero = ?", 237)
					.list().size() > 0;
	}

	public List<Conta> findByTipoAndEmpresa(String whereInEmpresas, Contatipo... contatipo) {
		
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		QueryBuilder<Conta> query = querySined()
				.select("conta.cdconta,conta.nome,conta.agencia,conta.numero,banco.nome")
				.leftOuterJoin("conta.banco banco")
				.leftOuterJoin("conta.listaContaempresa listaContaempresa")
				.leftOuterJoin("listaContaempresa.empresa empresa")
				.leftOuterJoin("conta.listaContapapel listaContapapel")
				.leftOuterJoin("listaContapapel.papel papel")
				.where("conta.ativo = ?",Boolean.TRUE);
		
				if(whereInEmpresas != null && !"".equals(whereInEmpresas)){
					query.openParentheses()
					.whereIn("empresa.cdpessoa",whereInEmpresas)
					.or()
					.where("empresa is null")
					.closeParentheses();
				}
				
		
				if(contatipo != null && contatipo.length > 0){
					query.openParentheses();
					for (int i = 0; i < contatipo.length; i++) {
						query.where("conta.contatipo = ?", contatipo[i]).or();
					}
					query.closeParentheses();
				}
				
				if (!administrador){
					if (whereInPapel != null && !whereInPapel.equals("")){
						query.openParentheses();
						query.whereIn("papel.cdpapel", whereInPapel)
						.or()
						.where("papel.cdpapel is null");
						query.closeParentheses();
					} else {
						query.where("papel.cdpapel is null");
					}
				}
				
				query.orderBy("conta.nome");
				return query.list();
	}
	
	public Conta verificaBanco(Conta conta, Contacarteira contacarteira){
		if(conta == null || conta.getCdconta() == null){
			throw new SinedException("Conta ou cdconta n�o podem ser null.");
		}
		return querySined()
			.select("conta.cdconta, conta.nome, " +
					"contacarteira.cdcontacarteira, contacarteira.tipoarquivoremessa, contacarteira.padrao, " +
					"banco.cdbanco, banco.numero")
			.join("conta.banco banco")
			.leftOuterJoin("conta.listaContacarteira contacarteira")
			.where("conta = ?",conta)
			.where("contacarteira = ?",contacarteira)
			.unique();
	}
	
	/**
	 * Carrega dados de conta
	 * @param conta
	 * @return
	 * @author Taidson
	 * @since 10/08/2010 
	 */
	public Conta loadConta(Conta conta){
		if(conta == null || conta.getCdconta() == null){
			throw new SinedException("Os par�metros conta ou cdconta n�o podem ser null.");
		}
		
		return querySined()
			.select("conta.cdconta,conta.nome,conta.agencia,conta.numero, conta.dvagencia, conta.dvnumero, " +
					"contatipo.cdcontatipo, contatipo.nome, conta.conciliacaoautomatica," +
					"contacarteira.cdcontacarteira, contacarteira.padrao, " +
					"bancoConfiguracaoRemessa.cdbancoconfiguracaoremessa, " +
					"bancoConfiguracaoRetorno.cdbancoconfiguracaoretorno, banco.cdbanco, banco.numero, " +
					"empresa.cdpessoa, empresa.nomefantasia, conta.baixaautomaticafaturamento")
			.leftOuterJoin("conta.listaContaempresa listaContaempresa")
			.leftOuterJoin("listaContaempresa.empresa empresa")
			.leftOuterJoin("conta.banco banco")
			.leftOuterJoin("conta.contatipo contatipo")
			.leftOuterJoin("conta.listaContacarteira contacarteira")
			.leftOuterJoin("contacarteira.bancoConfiguracaoRemessa bancoConfiguracaoRemessa")
			.leftOuterJoin("contacarteira.bancoConfiguracaoRetorno bancoConfiguracaoRetorno")
			.where("conta = ?", conta)
			.orderBy("conta.nome")
			.unique();
		
	}
	
	/**M�todo para buscar o sequencial do registro da conta.
	 * @author Thiago Augusto
	 * @param bean
	 * @return
	 */
	public Integer findSequencialConta(Conta bean){
		Conta conta = querySined().where("cdconta = ?", bean.getCdconta()).unique();
		return conta.getSequencial();
	}
	
	/**
	 * M�todo que busca o c�digo sispag da empresa
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Conta> findForCodigoEmpresaSispagByEmpresa(Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
					.select("conta.cdconta, conta.codigoempresasispag, empresa.cdpessoa, empresa.cpf, empresa.cnpj ")
					.join("conta.listaContaempresa listaContaempresa")
					.join("listaContaempresa.empresa empresa")
					.where("empresa = ?", empresa)
					.where("conta.codigoempresasispag is not null")
					.list();
	}

	/**
     * Busca os dados para o cache da tela de pedido de venda offline
     */
	public List<Conta> findForPVOffline() {
		return query()
		.select("conta.cdconta, conta.nome, listaContaempresa.cdcontaempresa, empresa.cdpessoa, empresa.nome, empresa.razaosocial")
		.leftOuterJoin("conta.listaContaempresa listaContaempresa")
		.leftOuterJoin("listaContaempresa.empresa empresa")
		.list();
	}
	
	public List<Conta> findForAndroid(String whereIn) {
		return query()
		.select("conta.cdconta, conta.nome, conta.agencia, conta.dvagencia, conta.dvnumero, conta.numero, contacarteira.naogerarboleto, contatipo.cdcontatipo")
		.leftOuterJoin("conta.listaContacarteira contacarteira")
		.join("conta.contatipo contatipo")
		.whereIn("conta.cdconta", whereIn)
		.list();
	}

	/**
	 * M�todo que busca as contas ativas e de acordo com a permiss�o
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Conta> findContasAtivasComPermissao() {
		return querySined()
				.select("conta.cdconta, conta.nome")
				.leftOuterJoin("conta.listaContapapel listaContapapel")
				.leftOuterJoin("listaContapapel.papel papel")
				.where("conta.ativo = true")
				.openParentheses()
				.where("listaContapapel is null")
				.or()
				.where("papel.cdpapel in (SELECT p.cdpapel FROM Usuario u " +
												"join u.listaUsuariopapel as up " +
												"join up.papel as p " +
												"where u.cdpessoa = " + SinedUtil.getUsuarioLogado().getCdpessoa() + " )")
				.or()
				.where("(SELECT COUNT(*) FROM Usuario u " +
						" join u.listaUsuariopapel as up " +
						" join up.papel as p " +
						" where u.cdpessoa = " + SinedUtil.getUsuarioLogado().getCdpessoa() + 
						" and p.administrador = true) > 0 ")
				.closeParentheses()
				.list();
	}

	public Conta carregaContaComMensagem(Conta conta, Contacarteira contacarteira) {
		if (conta == null || conta.getCdconta() == null){
			throw new SinedException("A Conta n�o pode ser nula.");
		}
		return querySined()
					.select("conta.cdconta, conta.taxaboleto, " +
							"contacarteira.cdcontacarteira, contacarteira.padrao, contacarteira.msgboleto1, contacarteira.msgboleto2")
					.leftOuterJoin("conta.listaContacarteira contacarteira")
					.where("conta = ?", conta)
					.where("contacarteira = ?", contacarteira)
					.unique();
	}

	/**
	 * M�todo que busca as contas ativas de acordo com a permiss�o e a empresa passada por par�metro
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Conta> findContasAtivasComPermissao(Empresa empresa, Boolean desconsideraContaQueNaoassociacontareceber) {
		QueryBuilder<Conta> query = querySined()
			.select("conta.cdconta, conta.nome")
			.leftOuterJoin("conta.listaContaempresa listaContaempresa")
			.leftOuterJoin("listaContaempresa.empresa empresa")
			.leftOuterJoin("conta.listaContapapel listaContapapel")
			.leftOuterJoin("listaContapapel.papel papel")
			.leftOuterJoin("conta.contatipo contatipo")
			.where("conta.ativo = true")
			.where("contatipo.cdcontatipo = ?", Contatipo.CONTA_BANCARIA);
			if(Boolean.TRUE.equals(desconsideraContaQueNaoassociacontareceber)){
				query
				.openParentheses()
				.where("exists(select cc.cdcontacarteira from Contacarteira cc where cc.conta.cdconta = conta.cdconta and coalesce(cc.naoassociarcontareceber, false) = false)")
				.or()
				.where("not exists(select cc.cdcontacarteira from Contacarteira cc where cc.conta.cdconta = conta.cdconta)")
				.closeParentheses();
			}
		query.openParentheses()
			.where("listaContapapel is null")
			.or()
			.where("papel.cdpapel in (SELECT p.cdpapel FROM Usuario u " +
											"join u.listaUsuariopapel as up " +
											"join up.papel as p " +
											"where u.cdpessoa = " + SinedUtil.getUsuarioLogado().getCdpessoa() + " )")
			.or()
			.where("(SELECT COUNT(*) FROM Usuario u " +
					" join u.listaUsuariopapel as up " +
					" join up.papel as p " +
					" where u.cdpessoa = " + SinedUtil.getUsuarioLogado().getCdpessoa() + 
					" and p.administrador = true) > 0 ")
			.closeParentheses()
			.openParentheses()
			.where("empresa = ?", empresa)
			.or()
			.where("empresa is null")
			.closeParentheses();
		
		
		
		return query.list();
	}
	
	public List<Conta> findContasAtivasComPermissao(Empresa empresa) {
		return findContasAtivasComPermissao(empresa, false);
	}
	
	/**
	 * M�todo que busca as contas ativas de acordo com a permiss�o e a empresa passada por par�metro
	 * Obs: n�o busca as contas que o campo empresa estiver nulo, caso a empresa passada por par�metro n�o seja nula 
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Conta> findContasAtivasComPermissaoByEmpresa(Empresa empresa) {
		return querySined()
			.select("conta.cdconta, conta.nome")
			.leftOuterJoin("conta.listaContaempresa listaContaempresa")
			.leftOuterJoin("listaContaempresa.empresa empresa")
			.leftOuterJoin("conta.listaContapapel listaContapapel")
			.leftOuterJoin("listaContapapel.papel papel")
			.leftOuterJoin("conta.contatipo contatipo")
			.where("conta.ativo = true")
			.where("contatipo.cdcontatipo = ?", Contatipo.CONTA_BANCARIA)
			.openParentheses()
			.where("listaContapapel is null")
			.or()
			.where("papel.cdpapel in (SELECT p.cdpapel FROM Usuario u " +
											"join u.listaUsuariopapel as up " +
											"join up.papel as p " +
											"where u.cdpessoa = " + SinedUtil.getUsuarioLogado().getCdpessoa() + " )")
			.or()
			.where("(SELECT COUNT(*) FROM Usuario u " +
					" join u.listaUsuariopapel as up " +
					" join up.papel as p " +
					" where u.cdpessoa = " + SinedUtil.getUsuarioLogado().getCdpessoa() + 
					" and p.administrador = true) > 0 ")
			.closeParentheses()
			.where("empresa = ?", empresa)
			.list();
	}
	
	/**
	 * M�todo que busca as contas ativas e que geram boletos de acordo com a permiss�o e a empresa passada por par�metro
	 *
	 * @param empresa
	 * @return
	 * @author Marden Silva
	 */
	public List<Conta> findContasBoletoAtivasComPermissao(Empresa empresa) {
		return querySined()
			.select("conta.cdconta, conta.nome")
			.leftOuterJoin("conta.listaContaempresa listaContaempresa")
			.leftOuterJoin("listaContaempresa.empresa empresa")
			.leftOuterJoin("conta.listaContapapel listaContapapel")
			.leftOuterJoin("listaContapapel.papel papel")
			.leftOuterJoin("conta.contatipo contatipo")
			.where("conta.ativo = true")
			.where("contatipo.cdcontatipo = ?", Contatipo.CONTA_BANCARIA)
			.openParentheses()
			.where("listaContapapel is null")
			.or()
			.where("papel.cdpapel in (SELECT p.cdpapel FROM Usuario u " +
											"join u.listaUsuariopapel as up " +
											"join up.papel as p " +
											"where u.cdpessoa = " + SinedUtil.getUsuarioLogado().getCdpessoa() + " )")
			.or()
			.where("(SELECT COUNT(*) FROM Usuario u " +
					" join u.listaUsuariopapel as up " +
					" join up.papel as p " +
					" where u.cdpessoa = " + SinedUtil.getUsuarioLogado().getCdpessoa() + 
					" and p.administrador = true) > 0 ")
			.closeParentheses()
			.openParentheses()
			.where("empresa = ?", empresa)
			.or()
			.where("empresa is null")
			.closeParentheses()
			.list();
	}

	/**
	 * Carrega informa��es da conta para a exibi��o do saldo na tela de baixa de Conta a receber/pagar.
	 *
	 * @param conta
	 * @return
	 * @since 01/08/2012
	 * @author Rodrigo Freitas
	 */
	public Conta carregaContaForSaldoBaixar(Conta conta) {
		if(conta == null || conta.getCdconta() == null){
			throw new SinedException("A conta n�o pode ser nula.");
		}
		return querySined()
					.select("conta.cdconta, contatipo.cdcontatipo")
					.leftOuterJoin("conta.contatipo contatipo")
					.where("conta = ?", conta)
					.unique();
	}

	/**
	 * Verifica se o usu�rio logado tem permiss�o para a conta passada por par�metro.
	 *
	 * @param conta
	 * @return
	 * @since 01/08/2012
	 * @author Rodrigo Freitas
	 */
	public boolean isUsuarioPermissaoConta(Conta conta) {
		SinedUtil.markAsReader();
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Conta.class)
					.leftOuterJoin("conta.listaContapapel listaContapapel")
					.leftOuterJoin("listaContapapel.papel papel")
					.where("conta = ?", conta)
					.openParentheses()
					.where("listaContapapel is null")
					.or()
					.where("papel.cdpapel in (SELECT p.cdpapel FROM Usuario u " +
													"join u.listaUsuariopapel as up " +
													"join up.papel as p " +
													"where u.cdpessoa = " + SinedUtil.getUsuarioLogado().getCdpessoa() + " )")
					.or()
					.where("(SELECT COUNT(*) FROM Usuario u " +
							" join u.listaUsuariopapel as up " +
							" join up.papel as p " +
							" where u.cdpessoa = " + SinedUtil.getUsuarioLogado().getCdpessoa() + 
							" and p.administrador = true) > 0 ")
					.closeParentheses()
					.unique() > 0;
	}
	
	/**
	 * 
	 * M�todo que carrega a conta com a taxa de boleto a partir da empresa.
	 *
	 * @name loadTaxaBoletoByEmpresa
	 * @param empresa
	 * @return
	 * @return Conta
	 * @author Thiago Augusto
	 * @date 03/08/2012
	 *
	 */
	public Conta loadTaxaBoletoByEmpresa(Empresa empresa){
		if (empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("A Empresa n�o pode ser nula.");
		}
		return querySined()
					.select("conta.cdconta, conta.taxaboleto")
					.leftOuterJoin("conta.listaContaempresa listaContaempresa")
					.leftOuterJoin("listaContaempresa.empresa empresa")
					.where("empresa = ?", empresa)
					.unique();
	}
	
	/**
	 * 
	 * M�todo que carrega a Conta com a TaxaBoleto a partir da Conta.
	 *
	 * @name loadTaxaBoletoByConta
	 * @param empresa
	 * @return
	 * @return Conta
	 * @author Thiago Augusto
	 * @date 03/08/2012
	 *
	 */
	public Conta loadTaxaBoletoByConta(Conta conta){
		if (conta == null || conta.getCdconta() == null){
			throw new SinedException("A Empresa n�o pode ser nula.");
		}
		return querySined()
		.select("conta.cdconta, conta.taxaboleto")
		.where("conta = ?", conta)
		.unique();
	}
	
	/**
	 * 
	 * M�todo que carrega uma lista de contas de uma determinada empresa.
	 *
	 * @name findByEmpresaForRemessa
	 * @param empresa
	 * @return
	 * @return List<Conta>
	 * @author Marden Silva
	 * @date 05/11/2012
	 *
	 */
	public List<Conta> findContaBoletoByEmpresa(Empresa empresa, Boolean listagem){
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		QueryBuilder<Conta> query =	querySined()
			.select("conta.cdconta, conta.nome, contacarteira.cdcontacarteira, contacarteira.carteira, "+
					"contacarteira.naoassociarcontareceber, contacarteira.naogerarboleto, contacarteira.variacaocarteira ")
			.join("conta.contatipo contatipo")
			.leftOuterJoin("conta.listaContacarteira contacarteira")
			.leftOuterJoin("conta.listaContaempresa listaContaempresa")
			.leftOuterJoin("listaContaempresa.empresa empresa");
		
		
		if(listagem == null || !listagem){
			query
			.openParentheses()
				.where("empresa = ?", empresa, empresa != null && empresa.getCdpessoa() != null)
				.or()
				.where("listaContaempresa is null")
			.closeParentheses()
			.where("conta.ativo = true")
			.where("contatipo.cdcontatipo = ?", Contatipo.CONTA_BANCARIA)
			.openParentheses()
				.where("contacarteira is null")
				.or()
				.where("exists (select c.cdconta from Contacarteira cc join cc.conta c where c.cdconta = conta.cdconta and coalesce(cc.naogerarboleto, false) = false) ")
			.closeParentheses();
		
		}
	
		if (!administrador){
			query.leftOuterJoin("conta.listaContapapel listaContapapel")
				 .leftOuterJoin("listaContapapel.papel papel");
			if (whereInPapel != null && !whereInPapel.equals("")){
				query.openParentheses();
				query.whereIn("papel.cdpapel", whereInPapel)
				.or()
				.where("papel.cdpapel is null");
				query.closeParentheses();
			} else {
				query.where("papel.cdpapel is null");
			}
		}
		
		return query
				.orderBy("conta.nome")
				.list();
	}
	
	/**
	 * Metodo que retorna os vinculos a partir de uma empresa
	 * 
	 * @param empresa
	 * @return
	 * @author Luiz Romario Filho
	 * @since 21/01/2014
	 */
	public List<Conta> findVinculosByEmpresa(Empresa empresa, Boolean ativos){
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		QueryBuilder<Conta> query =	querySined()
			
			.select("conta.cdconta, conta.nome, contacarteira.cdcontacarteira")
			.leftOuterJoin("conta.listaContacarteira contacarteira");
		
		if(empresa != null){
			query
			.leftOuterJoin("conta.listaContaempresa listaContaempresa")
			.leftOuterJoin("listaContaempresa.empresa empresa")
			.openParentheses()
				.where("empresa = ?", empresa)
				.or()
				.where("listaContaempresa is null")
			.closeParentheses();
		}
		
		if(ativos != null && ativos){
			query.where("conta.ativo = true");
		}
			
		if (!administrador){
			query.leftOuterJoin("conta.listaContapapel listaContapapel")
				 .leftOuterJoin("listaContapapel.papel papel");
			if (whereInPapel != null && !whereInPapel.equals("")){
				query.openParentheses();
				query.whereIn("papel.cdpapel", whereInPapel)
				.or()
				.where("papel.cdpapel is null");
				query.closeParentheses();
			} else {
				query.where("papel.cdpapel is null");
			}
		}
			
		return query
			.orderBy("conta.nome")
			.list();
	}
	
	/**
	 * 
	 * M�todo que carrega uma lista de contas de uma determinada empresa.
	 *
	 * @name findByEmpresaForRemessaPagamento
	 * @param empresa
	 * @return
	 * @return List<Conta>
	 * @author Rafael Salvio
	 *
	 */
	public List<Conta> findByEmpresaForRemessaPagamento(Empresa empresa){
		return querySined()
				.select("conta.cdconta, conta.nome")
				.join("conta.contatipo contatipo")
				.leftOuterJoin("conta.listaContaempresa listaContaempresa")
				.leftOuterJoin("listaContaempresa.empresa empresa")
				.openParentheses()
					.where("empresa = ?", empresa)
					.or()
					.where("listaContaempresa is null", empresa!=null)
				.closeParentheses()
				.where("conta.ativo = true")
				.where("contatipo.cdcontatipo = ?", Contatipo.CONTA_BANCARIA)
				.orderBy("conta.nome")
				.list();
	}
	
	/**
	 * M�todo que carrega uma lista de contas de acordo com a empresa.
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Conta> findContaBoletoByEmpresaContrato(Empresa empresa){
		return querySined()
				.select("conta.cdconta, conta.nome, contacarteira.cdcontacarteira")
				.join("conta.contatipo contatipo")
				.leftOuterJoin("conta.listaContacarteira contacarteira")
				.leftOuterJoin("conta.listaContaempresa listaContaempresa")
				.leftOuterJoin("listaContaempresa.empresa empresa")
				.leftOuterJoin("conta.listaContapapel listaContapapel")
				.leftOuterJoin("listaContapapel.papel papel")
				.openParentheses()
					.where("empresa = ?", empresa)
					.or()
					.where("listaContaempresa is null")
				.closeParentheses()
				.openParentheses()
					.whereIn("papel.cdpapel", SinedUtil.getWhereInCdPapel())
					.or()
					.where("listaContapapel is null")
				.closeParentheses()
				.where("conta.ativo = true")
				.where("contatipo.cdcontatipo = ?", Contatipo.CONTA_BANCARIA)
				.orderBy("conta.nome")
				.list();
	}

	/**
	 * Carrega a conta com a conta gerencial
	 *
	 * @param conta
	 * @return
	 * @author Luiz Fernando
	 */
	public Conta loadWithContagerencial(Conta conta) {
		if (conta == null || conta.getCdconta() == null)
			throw new SinedException("A conta n�o pode ser nula.");
		
		return querySined()
			.select("conta.cdconta, contacontabil.cdcontacontabil")
			.leftOuterJoin("conta.contaContabil contacontabil")
			.where("conta = ?", conta)
			.unique();
	}	
	
	/**
	 * M�todo que carrega a conta com as taxas
	 *
	 * @param conta
	 * @return
	 * @author Luiz Fernando
	 */
	public Conta loadWithTaxa(Conta conta) {
		if(conta == null || conta.getCdconta() == null)
			throw new SinedException("Conta n�o pode ser nula.");
		
		return querySined()
			 .select("conta.cdconta, conta.taxaboleto, taxa.cdtaxa, listaTaxaitem.valor, listaTaxaitem.percentual, listaTaxaitem.dias, listaTaxaitem.aodia, " +
					 "listaTaxaitem.gerarmensagem, listaTaxaitem.motivo, tipotaxa.cdtipotaxa, tipotaxa.nome, listaTaxaitem.tipocalculodias," +
					 "listaTaxaitem.gerarmensagem ")
			 .leftOuterJoin("conta.taxa taxa")
			 .leftOuterJoin("taxa.listaTaxaitem listaTaxaitem")
			 .leftOuterJoin("listaTaxaitem.tipotaxa tipotaxa")
			 .where("conta = ?", conta)
			 .unique();	
	}
	
	/**
	 * M�todo que carrega a conta com a empresa
	 *
	 * @param conta
	 * @return
	 * @author Luiz Fernando
	 */
	public Conta loadContaForRemessa(Conta conta){
		if(conta == null || conta.getCdconta() == null){
			throw new SinedException("Os par�metros conta ou cdconta n�o podem ser null.");
		}
		
		return querySined()
			.select("conta.cdconta, conta.nome, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia")
			.leftOuterJoin("conta.listaContaempresa listaContaempresa")
			.leftOuterJoin("listaContaempresa.empresa empresa")
			.where("conta = ?", conta)
			.unique();
		
	}
	
	/**
	* M�todo que busca as contas das empresas que o usu�rio tem permiss�o
	*
	* @return
	* @since 17/02/2016
	* @author Luiz Fernando
	*/
	public List<Conta> findContaBoletoByEmpresaUsuario(){
		String whereInEmpresa = new SinedUtil().getListaEmpresa();
		return querySined()
				.select("conta.cdconta, conta.nome")
				.leftOuterJoin("conta.listaContaempresa listaContaempresa")
				.openParentheses()
					.whereIn("listaContaempresa.empresa.cdpessoa", whereInEmpresa, StringUtils.isNotBlank(whereInEmpresa))
					.or()
					.where("listaContaempresa is null")
				.closeParentheses()
				.where("conta.ativo = true")
				.where("conta.contatipo.cdcontatipo = ?", Contatipo.CONTA_BANCARIA)
				.orderBy("conta.nome")
				.list();
	}
	
	public List<Conta> findContaBoletoDigitalByEmpresaUsuario(){
		String whereInEmpresa = new SinedUtil().getListaEmpresa();
		return querySined()
				.select("conta.cdconta, conta.nome")
				.leftOuterJoin("conta.listaContaempresa listaContaempresa")
				.openParentheses()
				.whereIn("listaContaempresa.empresa.cdpessoa", whereInEmpresa, StringUtils.isNotBlank(whereInEmpresa))
				.or()
				.where("listaContaempresa is null")
				.closeParentheses()
				.where("conta.ativo = true")
				.where("conta.contatipo.cdcontatipo = ?", Contatipo.CONTA_BANCARIA)
				.where("exists(select 1 from Contadigital c where c.conta = conta)")
				.orderBy("conta.nome")
				.list();
	}

	/**
	* M�todo que busca a conta de acordo com a conta carteira
	*
	* @return
	* @since 14/11/2016
	* @author Mairon Cezar
	*/
	public Conta findContaByContacarteira(Contacarteira contacarteira){
		return querySined()
			.select("conta.cdconta, conta.nome, banco.cdbanco, banco.nome")
			.join("conta.banco banco")
			.leftOuterJoin("conta.listaContacarteira carteira")
			.where("carteira = ?", contacarteira)
			.unique();
	}
	
	/**
	* M�todo que carrega a conta com a lista de empresa
	*
	* @param conta
	* @return
	* @since 05/05/2017
	* @author Luiz Fernando
	*/
	public Conta loadContaWithEmpresa(Conta conta){
		if(conta == null || conta.getCdconta() == null){
			throw new SinedException("Os par�metros conta ou cdconta n�o podem ser null.");
		}
		
		return querySined()
			.select("conta.cdconta, conta.nome, contatipo.cdcontatipo, listaContaempresa.cdcontaempresa, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia")
			.leftOuterJoin("conta.contatipo contatipo")
			.leftOuterJoin("conta.listaContaempresa listaContaempresa")
			.leftOuterJoin("listaContaempresa.empresa empresa")
			.where("conta = ?", conta)
			.unique();
		
	}
	
	/**
	* M�todo que verifica se a empresa em permiss�o na conta
	*
	* @param conta
	* @param empresa
	* @return
	* @since 05/05/2017
	* @author Luiz Fernando
	*/
	public boolean isPermissaoConta(Conta conta, Empresa empresa){
		if(conta == null || conta.getCdconta() == null || empresa == null || empresa.getCdpessoa() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		SinedUtil.markAsReader();
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Conta.class)
			.leftOuterJoin("conta.listaContaempresa listaContaempresa")
			.leftOuterJoin("listaContaempresa.empresa empresa")
			.where("conta = ?", conta)
			.openParentheses()
				.where("listaContaempresa is null")
				.or()
				.where("empresa = ?", empresa)
			.closeParentheses()
			.unique() > 0;
		
	}
	
	public Conta loadForTransferenciaInterna(Conta conta){
		if(conta == null || conta.getCdconta() == null){
			throw new SinedException("Os par�metros conta ou cdconta n�o podem ser null.");
		}
		
		return querySined()
			.select("conta.cdconta, conta.nome, conta.numero, conta.agencia, conta.dvagencia, banco.nome, banco.numero, banco.cdbanco ")
			.leftOuterJoin("conta.banco banco")
			.where("conta = ?", conta)
			.unique();
	}

	public List<Conta> findByContatipo(Contatipo contatipo, Empresa empresa) {
		QueryBuilder<Conta> query = querySined();
		
		query
			.select("conta.cdconta,conta.nome,conta.agencia,conta.numero,banco.nome, banco.cdbanco")
			.leftOuterJoin("conta.banco banco")
			.leftOuterJoin("conta.listaContaempresa listaContaempresa")
			.leftOuterJoin("listaContaempresa.empresa empresa")
			.leftOuterJoin("conta.listaContapapel listaContapapel")
			.leftOuterJoin("listaContapapel.papel papel")
			.where("conta.contatipo = ?", contatipo)
			.where("conta.ativo = ?",Boolean.TRUE)
			.orderBy("conta.nome");
		
		if(empresa != null && empresa.getCdpessoa() != null){
			query.where("empresa = ?", empresa);
		} else {
			query.where("empresa is null");
		}
		
		return query.list();
	}

	public List<Conta> loadForLcdpr(LcdprArquivoFiltro filtro) {
		return querySined()
				.select("conta.cdconta, conta.numero, conta.dvnumero, conta.agencia, banco.cdbanco, banco.nome, banco.numero")
				.leftOuterJoin("conta.banco banco")
				.leftOuterJoin("conta.listaContaempresa listaContaempresa")
				.leftOuterJoin("listaContaempresa.empresa empresa")
				.where("conta.contatipo = 1")
				.where("conta.ativo = ?",Boolean.TRUE)
				.openParentheses()
					.where("listaContaempresa is null")
					.or()
					.where("empresa.cdpessoa in (select e.cdpessoa FROM Empresaproprietario ep " +
												"JOIN ep.empresa e " +
												"WHERE ep.colaborador.cdpessoa = " + filtro.getColaborador().getCdpessoa() + ")")
				.closeParentheses()
				.list();
	}
}