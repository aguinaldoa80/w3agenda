package br.com.linkcom.sined.geral.dao;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Ecom_PedidoVenda;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendahistorico;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Regiao;
import br.com.linkcom.sined.geral.bean.Regiaolocal;
import br.com.linkcom.sined.geral.bean.Regiaovendedor;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Comprovantestatus;
import br.com.linkcom.sined.geral.bean.enumeration.ContasFinanceiroSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoagendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.ProducaoagendasituacaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoPendencia;
import br.com.linkcom.sined.geral.bean.enumeration.TipoVendedorEnum;
import br.com.linkcom.sined.geral.service.ContapagarService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentohistoricoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendahistoricoService;
import br.com.linkcom.sined.geral.service.ReservaService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.PedidovendaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EmitircustovendaFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.FluxocaixaFiltroReport;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

public class PedidovendaDAO extends GenericDAO<Pedidovenda>{
	
	protected EmpresaService empresaService;
	protected MaterialcategoriaService materialcategoriaService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {
		this.materialcategoriaService = materialcategoriaService;
	}
	  
	@Override
	public void updateEntradaQuery(QueryBuilder<Pedidovenda> query) {
		
		query	
			 .select("pedidovenda.cdpedidovenda, pedidovenda.identificador, pedidovenda.identificacaoexterna, pedidovenda.dtpedidovenda, pedidovenda.observacao, pedidovenda.protocolooffline, pedidovenda.emitirnotacoleta, " +
			 		"pedidovenda.observacaointerna, pedidovenda.pedidovendasituacao, pedidovenda.valorfrete, pedidovenda.ordemcompra, pedidovenda.desconto, pedidovenda.limitecreditoexcedido, " +
			 		"pedidovenda.percentualdesconto, pedidovenda.restricaocliente, pedidovenda.taxapedidovenda, pedidovenda.dtaltera, pedidovenda.origemwebservice, pedidovenda.valorFreteCIF, " +
			 		"pedidovenda.origemOtr, pedidovenda.cdusuarioaltera, pedidovenda.prazomedio, pedidovenda.qtdeParcelas, pedidovenda.saldofinal, pedidovenda.integrar, pedidovenda.presencacompradornfe, pedidovenda.observacaoPedidoVendaTipo, " +
			 		"pedidovenda.identificadorcarregamento, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, " +
			 		"pedidovenda.pedidoEcommerceVisualizado, " +
			 		"cliente.cdpessoa, cliente.nome, cliente.cnpj, cliente.cpf, cliente.identificador, contato.cdpessoa, contato.nome, clienteindicacao.cdpessoa, clienteindicacao.nome, " +
			 		"colaborador.cdpessoa, colaborador.nome, colaborador.cpf, " +
			 		"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.descricao, pedidovendatipo.sigla, pedidovendatipo.gerarnotaconfirmacaopedido, pedidovendatipo.baixaestoqueEnum, pedidovendatipo.geracaocontareceberEnum, " +
			 		"pedidovenda.situacaoPendencia,pedidovendatipo.bonificacao, pedidovendatipo.reserva, pedidovendatipo.comodato, pedidovendatipo.representacao, pedidovendatipo.confirmacaoManualWMS, pedidovendatipo.permitirvendasemestoque, " +
			 		"pedidovendatipo.obrigarLoteOrcamento, pedidovendatipo.obrigarLotePedidovenda, pedidovendatipo.obrigarLoteConfirmarpedido, pedidovendatipo.obrigarLoteVenda, " +
			 		"pedidovendatipo.sincronizarComWMS, pedidovendatipo.confirmacaoManualWMS, pedidovendatipo.consideraripiconta, pedidovendatipo.consideraricmsstconta, pedidovendatipo.considerardesoneracaoconta, " +
			 		"pedidovendatipo.observacaoParaEnvioCancelamento, pedidovendatipo.observacaoParaEnvioConfirmacao, pedidovendatipo.atualizarStatusEcommerceAoCancelar, pedidovendatipo.atualizarStatusEcommerceAoConfirmar,"+
			 		"documentotipo.cddocumentotipo, documentotipo.nome, documentotipo.antecipacao, documentotipo.boleto, documentotipo.taxavenda, " +
			 		"endereco.cdendereco, endereco.logradouro, endereco.bairro, endereco.cep, endereco.caixapostal, endereco.numero, endereco.complemento, endereco.pontoreferencia, " +
			 		"endereco.descricao, endereco.substituirlogradouro, " +
			 		"municipio.cdmunicipio, municipio.nome, municipio.cdibge, " +
			 		"uf.cduf, uf.sigla, uf.nome, uf.cdibge, " +
			 		"enderecoFaturamento.cdendereco, enderecoFaturamento.logradouro, enderecoFaturamento.bairro, enderecoFaturamento.cep, enderecoFaturamento.caixapostal, enderecoFaturamento.numero, enderecoFaturamento.complemento, enderecoFaturamento.pontoreferencia, " +
			 		"enderecoFaturamento.descricao, enderecoFaturamento.substituirlogradouro, " +
			 		"municipioFaturamento.cdmunicipio, municipioFaturamento.nome, municipioFaturamento.cdibge, " +
			 		"ufFaturamento.cduf, ufFaturamento.sigla, ufFaturamento.nome, ufFaturamento.cdibge, " +
			 		"listaPedidovendamaterial.cdpedidovendamaterial, listaPedidovendamaterial.ordem, listaPedidovendamaterial.quantidade, listaPedidovendamaterial.preco, listaPedidovendamaterial.desconto, listaPedidovendamaterial.percentualdesconto, " +
			 		"listaPedidovendamaterial.dtprazoentrega, listaPedidovendamaterial.observacao, listaPedidovendamaterial.valorcustomaterial, listaPedidovendamaterial.valorvendamaterial, listaPedidovendamaterial.peso, listaPedidovendamaterial.valorMinimo, " +
			 		"listaPedidovendamaterial.valorcoleta, listaPedidovendamaterial.descontogarantiareforma, garantiareformaitem.cdgarantiareformaitem, garantiareforma.cdgarantiareforma, pedidovendamaterialgarantido.cdpedidovendamaterial, " +
			 		"material.cdmaterial, material.nome, material.identificacao, material.producao, material.valorvendaminimo, material.valorvendamaximo, material.valorvenda, material.pesoliquidovalorvenda, material.peso, material.pesobruto, material.tributacaoipi, " +
			 		"material.obrigarlote, unidademedidaM.cdunidademedida, unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
			 		"loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, " +
			 		"terceiro.cdpessoa, terceiro.nome, terceiro.cnpj, terceiro.cpf, " +
			 		"frete.cdResponsavelFrete, frete.nome, listaPedidovendamaterial.saldo, listaPedidovendamaterial.qtdereferencia, listaPedidovendamaterial.bandaalterada, listaPedidovendamaterial.servicoalterado," +
			 		"projeto.cdprojeto, projeto.nome, " +
			 		"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
			 		"prazopagamento.cdprazopagamento, prazopagamento.nome, prazopagamento.avista, " +
			 		"conta.cdconta, conta.nome, materialcoleta.cdmaterial, materialcoleta.nome, " +
			 		"pedidovenda.valorusadovalecompra, pedidovenda.valoraproximadoimposto, " +
			 		"indicecorrecao.cdindicecorrecao, indicecorrecao.descricao, " +
			 		"vendaorcamento.cdvendaorcamento, listaPedidovendamaterial.fatorconversao, material.qtdeunidade, material.considerarvendamultiplos," +
			 		"pedidovendaorigem.cdpedidovenda, clienteorigem.cdpessoa, clienteorigem.nome, localarmazenagemcoleta.cdlocalarmazenagem, " +
			 		"listaPedidovendamaterial.altura, listaPedidovendamaterial.largura, listaPedidovendamaterial.comprimento, listaPedidovendamaterial.comprimentooriginal, listaPedidovendamaterial.multiplicador, fornecedor.cdpessoa, fornecedor.nome, " +
					"materialunidademedida.cdmaterialunidademedida, materialunidademedida.fracao, materialunidademedida.qtdereferencia, unidademedidaconversao.cdunidademedida, unidademedidaconversao.nome, unidademedidaconversao.simbolo, " +
					"pedidovendamaterialseparacao.cdpedidovendamaterialseparacao, pedidovendamaterialseparacao.quantidade, unidademedidaseparacao.cdunidademedida, unidademedidaseparacao.nome, unidademedidaseparacao.simbolo," +
					"unidademedidamaterial.cdunidademedida, unidademedidamaterial.nome, materialmestre.cdmaterial, listaPedidovendamaterial.identificadorespecifico, listaPedidovendamaterial.identificadorintegracao, listaPedidovendamaterial.identificadorinterno," +
					"listaPedidovendamaterial.percentualrepresentacao, listaPedidovendamaterial.custooperacional, " +
					"listaPedidovendamaterial.percentualcomissaoagencia, listaPedidovendamaterial.valorSeguro, listaPedidovendamaterial.outrasdespesas, " +
					"listaPedidovendamaterial.valoripi, listaPedidovendamaterial.ipi, listaPedidovendamaterial.tipocobrancaipi, listaPedidovendamaterial.aliquotareaisipi, listaPedidovendamaterial.tipocalculoipi, grupotributacao.cdgrupotributacao, " +
					"listaPedidovendamaterial.tipotributacaoicms, " +
					"listaPedidovendamaterial.tipocobrancaicms, listaPedidovendamaterial.valorbcicms, listaPedidovendamaterial.icms, " +
					"listaPedidovendamaterial.valoricms, listaPedidovendamaterial.valorbcicmsst, listaPedidovendamaterial.icmsst, " +
					"listaPedidovendamaterial.valoricmsst, listaPedidovendamaterial.reducaobcicmsst, listaPedidovendamaterial.margemvaloradicionalicmsst, " +
					"listaPedidovendamaterial.valorbcfcp, listaPedidovendamaterial.fcp, listaPedidovendamaterial.valorfcp, " +
					"listaPedidovendamaterial.valorbcfcpst, listaPedidovendamaterial.fcpst, listaPedidovendamaterial.valorfcpst, " +
					"listaPedidovendamaterial.valorbcdestinatario, listaPedidovendamaterial.valorbcfcpdestinatario, listaPedidovendamaterial.fcpdestinatario, " +
					"listaPedidovendamaterial.icmsdestinatario, listaPedidovendamaterial.icmsinterestadual, listaPedidovendamaterial.icmsinterestadualpartilha, " +
					"listaPedidovendamaterial.valorfcpdestinatario, listaPedidovendamaterial.valoricmsdestinatario, listaPedidovendamaterial.valoricmsremetente, " +
					"listaPedidovendamaterial.difal, listaPedidovendamaterial.valordifal, ncmcapitulo.cdncmcapitulo, cfop.cdcfop, " +
					"listaPedidovendamaterial.ncmcompleto, listaPedidovendamaterial.modalidadebcicms, listaPedidovendamaterial.modalidadebcicmsst, " +
					"listaPedidovendamaterial.percentualdesoneracaoicms, listaPedidovendamaterial.valordesoneracaoicms, listaPedidovendamaterial.abaterdesoneracaoicms, " +
					"pedidovenda.valordescontorepresentacao, pedidovenda.valorbrutorepresentacao," +
					"prazopagamentorepresentacao.cdprazopagamento, " +
					"documentotiporepresentacao.cddocumentotipo, " +
					"contaboletorepresentacao.cdconta, " +
					"agencia.cdpessoa, agencia.nome, agencia.cpf, agencia.cnpj, " +
					"parceiro.cdpessoa, parceiro.nome, parceiro.razaosocial, parceiro.cnpj, " +
			 		"listaPedidovendamaterial.qtdevolumeswms, listaPedidovendamaterial.pesomedio, "+
			 		"centrocusto.cdcentrocusto, centrocusto.nome, comissionamento.cdcomissionamento, " +
			 		"materialFaixaMarkup.cdMaterialFaixaMarkup, " +
					"faixaMarkupNome.nome, faixaMarkupNome.cor, faixaMarkupNome.cdFaixaMarkupNome")
	 		 .leftOuterJoin("pedidovenda.vendaorcamento vendaorcamento")
	 		 .leftOuterJoin("pedidovenda.projeto projeto")
	 		 .leftOuterJoin("pedidovenda.prazopagamento prazopagamento")
	 		 .leftOuterJoin("pedidovenda.indicecorrecao indicecorrecao")
	 		 .leftOuterJoin("pedidovenda.conta conta")
	 		 .leftOuterJoin("pedidovenda.localarmazenagem localarmazenagem")
			 .leftOuterJoin("pedidovenda.empresa empresa")
			 .leftOuterJoin("pedidovenda.cliente cliente")
			 .leftOuterJoin("pedidovenda.clienteindicacao clienteindicacao")
			 .leftOuterJoin("pedidovenda.contato contato")
			 .leftOuterJoin("pedidovenda.colaborador colaborador")
			 .leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
			 .leftOuterJoin("pedidovendatipo.localarmazenagemcoleta localarmazenagemcoleta")
			 .leftOuterJoin("pedidovenda.documentotipo documentotipo")
			 .leftOuterJoin("pedidovenda.frete frete")
		     .leftOuterJoin("pedidovenda.endereco endereco")
		     .leftOuterJoin("pedidovenda.agencia agencia")
		     .leftOuterJoin("pedidovenda.centrocusto centrocusto")
		     .leftOuterJoin("endereco.municipio municipio")
		     .leftOuterJoin("municipio.uf uf")
		     .leftOuterJoin("pedidovenda.enderecoFaturamento enderecoFaturamento")
		     .leftOuterJoin("enderecoFaturamento.municipio municipioFaturamento")
		     .leftOuterJoin("municipioFaturamento.uf ufFaturamento")
		     .leftOuterJoin("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
		     .leftOuterJoin("listaPedidovendamaterial.ncmcapitulo ncmcapitulo")
		     .leftOuterJoin("listaPedidovendamaterial.cfop cfop")
		     .leftOuterJoin("listaPedidovendamaterial.pneu pneu")
		     .leftOuterJoin("listaPedidovendamaterial.fornecedor fornecedor")
		     .leftOuterJoin("listaPedidovendamaterial.material material")
		     .leftOuterJoin("listaPedidovendamaterial.materialmestre materialmestre")
		     .leftOuterJoin("material.unidademedida unidademedidaM")
		     .leftOuterJoin("listaPedidovendamaterial.materialcoleta materialcoleta")
		     .leftOuterJoin("listaPedidovendamaterial.loteestoque loteestoque")
		     .leftOuterJoin("listaPedidovendamaterial.unidademedida unidademedida")
		     .leftOuterJoin("pedidovenda.terceiro terceiro")
		     .leftOuterJoin("pedidovenda.pedidovendaorigem pedidovendaorigem")
		     .leftOuterJoin("pedidovendaorigem.cliente clienteorigem")
		     .leftOuterJoin("material.unidademedida unidademedidamaterial")
		     .leftOuterJoin("material.listaMaterialunidademedida materialunidademedida")
			 .leftOuterJoin("materialunidademedida.unidademedida unidademedidaconversao")
			 .leftOuterJoin("listaPedidovendamaterial.listaPedidovendamaterialseparacao pedidovendamaterialseparacao")
			 .leftOuterJoin("pedidovendamaterialseparacao.unidademedida unidademedidaseparacao")
			 .leftOuterJoin("pedidovenda.prazopagamentorepresentacao prazopagamentorepresentacao")
			 .leftOuterJoin("pedidovenda.documentotiporepresentacao documentotiporepresentacao")
			 .leftOuterJoin("pedidovenda.contaboletorepresentacao contaboletorepresentacao")
			 .leftOuterJoin("listaPedidovendamaterial.grupotributacao grupotributacao")
			 .leftOuterJoin("listaPedidovendamaterial.comissionamento comissionamento")
			 .leftOuterJoin("listaPedidovendamaterial.pedidovendamaterialgarantido pedidovendamaterialgarantido")
			 .leftOuterJoin("listaPedidovendamaterial.garantiareformaitem garantiareformaitem")
			 .leftOuterJoin("garantiareformaitem.garantiareforma garantiareforma")
			 .leftOuterJoin("listaPedidovendamaterial.materialFaixaMarkup materialFaixaMarkup")
			 .leftOuterJoin("listaPedidovendamaterial.faixaMarkupNome faixaMarkupNome")
			 .leftOuterJoin("pedidovenda.parceiro parceiro")
			 .orderBy("listaPedidovendamaterial.ordem, material.nome");
		query = SinedUtil.setJoinsByComponentePneu(query);
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Pedidovenda> query, FiltroListagem _filtro) {
		PedidovendaFiltro filtro = (PedidovendaFiltro)_filtro;
		 
		if (filtro.getDtinicio() == null)
			query.where("1=0");
		
		query
		.select("distinct pedidovenda.cdpedidovenda, pedidovenda.dtpedidovenda, pedidovenda.pedidovendasituacao, pedidovenda.identificacaoexterna, pedidovenda.identificador, " +
				"pedidovenda.ordemcompra, pedidovenda.desconto, pedidovenda.valorfrete, pedidovenda.taxapedidovenda, pedidovenda.saldofinal, pedidovenda.valorusadovalecompra, " +
				"pedidovenda.situacaoPendencia,pedidovenda.identificadorcarregamento, pedidovenda.limitecreditoexcedido, pedidovenda.origemOtr, pedidovenda.pedidoEcommerceVisualizado, " +
				"empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.exibiripivenda, " +
				"cliente.nome, cliente.identificador, cliente.creditolimitecompra, " +
				"endereco.cdendereco, endereco.cep, endereco.complemento, endereco.numero, endereco.logradouro, endereco.descricao, endereco.substituirlogradouro, " +
				"municipio.cdmunicipio, municipio.nome, " +
				"uf.cduf, uf.nome, uf.sigla, pedidovenda.dtprazoentregamax," +
				"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.confirmacaoManualWMS")
	     .leftOuterJoin("pedidovenda.empresa empresa")
	     .leftOuterJoin("pedidovenda.cliente cliente")
	     .leftOuterJoin("pedidovenda.endereco endereco")
	     .leftOuterJoin("endereco.municipio municipio")
	     .leftOuterJoin("municipio.uf uf")
		 .leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
		 .leftOuterJoinIfNotExists("cliente.listaClientevendedor listaClientevendedor");
		
		if(Boolean.TRUE.equals(filtro.getOrigemOtr())){
			//query.where("coalesce(pedidovenda.origemOtr, false) = true");
		}else{
			//query.where("coalesce(pedidovenda.origemOtr, false) = false");
		}
		if(filtro.getUsuario() != null) {
			query.where("pedidovenda.cdpedidovenda in (select pv.cdpedidovenda " +
							" from Pedidovendahistorico pvh " +
							" join pvh.pedidovenda pv " +
							" where upper(substring(pvh.acao, 1, 4)) =  " + "'CRIA'"  +
							" and pvh.cdusuarioaltera = "+filtro.getUsuario().getCdpessoa()+")");
		}
		
		if(filtro.getCampoextrapedidovendatipo() != null 
				&& filtro.getCampoextrapedidovendatipo().getCdcampoextrapedidovendatipo() != null 
				&& filtro.getValorCampoExtra() != null && !filtro.getValorCampoExtra().trim().isEmpty()){
			query
				.leftOuterJoin("pedidovenda.listaPedidovendavalorcampoextra listaPedidovendavalorcampoextra")
				.where("listaPedidovendavalorcampoextra.campoextrapedidovendatipo = ?", filtro.getCampoextrapedidovendatipo())
				.whereLikeIgnoreAll("listaPedidovendavalorcampoextra.valor", filtro.getValorCampoExtra());
		}

		
		this.preencheWhereQueryPedidovenda(query, filtro);
	}
	
	protected void preencheWhereQueryPedidovenda(QueryBuilder<? extends Object> query, PedidovendaFiltro filtro){
		query 
		 .whereLikeIgnoreAll("pedidovenda.identificador", filtro.getIdentificador())
		 .whereIn("pedidovenda.cdpedidovenda", filtro.getWhereIncdpedidovenda())
	     .where("pedidovenda.vendaorcamento.cdvendaorcamento = ?", filtro.getCdvendaorcamento())
	     .whereLikeIgnoreAll("pedidovenda.identificacaoexterna", filtro.getIdentificacaoexterna())
	     .where("pedidovenda.cliente = ?",filtro.getCliente())
	     .where("pedidovenda.empresa = ?",filtro.getEmpresa())
	     .where("pedidovenda.dtpedidovenda >= ?", filtro.getDtinicio())
	     .where("pedidovenda.dtpedidovenda <= ?", filtro.getDtfim())
	     .where("pedidovendatipo = ?", filtro.getPedidovendatipo())
	     .where("pedidovendatipo.reserva = ?", filtro.getTipopedidoReserva())
	     .where("pedidovenda.prazopagamento = ?", filtro.getPrazopagamento())
	     .where("pedidovenda.projeto = ?", filtro.getProjeto())
	     
	     .orderBy("pedidovenda.dtpedidovenda desc, pedidovenda.cdpedidovenda desc") 
	     .ignoreJoin("listaPedidovendamaterial", "material", "listaPedidovendapagamento", "documentotipoparcelas", 
	    		 "listaClientevendedor", "colaboradorcliente", "listaPedidovendavalorcampoextra", "materialmestregrade",
	    		 "listaFornecedor", "fornecedorRepresentacao");
		
		if(filtro.getCategoria() != null){
			query
				.leftOuterJoin("cliente.listaPessoacategoria listaPessoacategoria")
				.where("listaPessoacategoria.categoria = ?", filtro.getCategoria());
		}
		
		if(filtro.getDocumentotipo() != null){
			query
				.leftOuterJoinIfNotExists("pedidovenda.listaPedidovendapagamento listaPedidovendapagamento")
				.openParentheses()
				    .where("pedidovenda.documentotipo = ?", filtro.getDocumentotipo()).or()
				    .where("listaPedidovendapagamento.documentotipo = ?", filtro.getDocumentotipo())
			    .closeParentheses();
		}
		if(filtro.getNaoexibirresultado() != null && filtro.getNaoexibirresultado()){
			query.where("1=0");
		}
		
		if(filtro.getListaPedidovendasituacaoecommerce() != null && filtro.getListaPedidovendasituacaoecommerce().size() > 0){
			String whereInIdSituacao = CollectionsUtil.listAndConcatenate(filtro.getListaPedidovendasituacaoecommerce(), "idsecommerce", ",");
			query
				.where("exists(select ecp.id " +
							"from Ecom_PedidoVenda ecp " +
							"where ecp.pedidoVenda = pedidovenda " +
							"and ecp.id_situacao in (" + whereInIdSituacao + "))");
		}
		
		if(filtro.getPrazoentregainicio() != null || filtro.getPrazoentregafim() != null || 
				filtro.getFornecedorRepresentacao() != null || filtro.getMaterial() != null ||
				filtro.getMaterialgrupo() != null || filtro.getMaterialcategoria() != null){
			query.leftOuterJoinIfNotExists("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial");
			query.where("listaPedidovendamaterial.dtprazoentrega >= ?",filtro.getPrazoentregainicio());
		    query.where("listaPedidovendamaterial.dtprazoentrega <= ?", filtro.getPrazoentregafim());
		    
			if(filtro.getFornecedorRepresentacao() != null || filtro.getMaterial() != null ||
				filtro.getMaterialgrupo() != null || filtro.getMaterialcategoria() != null){
				query
					.leftOuterJoin("listaPedidovendamaterial.material material")
				 	.leftOuterJoin("material.materialmestregrade materialmestregrade");
				if(filtro.getFornecedorRepresentacao() != null){
					query.leftOuterJoinIfNotExists("listaPedidovendamaterial.fornecedor fornecedorRepresentacao");
					query.where("fornecedorRepresentacao = ?", filtro.getFornecedorRepresentacao());
				}
				if(filtro.getMaterial() != null){
					query.openParentheses()
						.where("material = ?", filtro.getMaterial())
						.or()
						.where("listaPedidovendamaterial.materialmestre = ?", filtro.getMaterial())
						.or()
						.where("materialmestregrade = ?", filtro.getMaterial());
					if(filtro.getMaterial().getVendapromocional() != null && filtro.getMaterial().getVendapromocional()){
						query.leftOuterJoin("listaPedidovendamaterial.materialmestre materialmestre");
						query.ignoreJoin("materialmestre");
						query.or().where("materialmestre = ?", filtro.getMaterial());
					}
					query.closeParentheses();
				}
				if(filtro.getMaterialgrupo() != null){
					query.where("material.materialgrupo = ?", filtro.getMaterialgrupo());
				}
				if(filtro.getMaterialcategoria() != null){
//					query.where("material.materialcategoria = ?", filtro.getMaterialcategoria());
					materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());					
					query.whereIn("material.materialcategoria.cdmaterialcategoria", CollectionsUtil.listAndConcatenate(materialcategoriaService.findForFitlro(filtro.getMaterialcategoria().getIdentificador()), "cdmaterialcategoria", ","));
				}
			}
		}
			
		if(filtro.getRegiao() != null){
			Regiao regiao = filtro.getRegiao();
			if(regiao.getListaRegiaolocal() != null && regiao.getListaRegiaolocal().size() > 0){
				query.openParentheses();
				for (Regiaolocal regiaolocal : regiao.getListaRegiaolocal()) {
					query
						.openParentheses()
						.where("endereco.cep >= ?", regiaolocal.getCepinicio())
						.where("endereco.cep <= ?", regiaolocal.getCepfinal())
						.closeParentheses()
						.or();
				}
				query.closeParentheses();
			}
			if(regiao.getListaRegiaovendedor() != null && regiao.getListaRegiaovendedor().size() > 0){
				query.openParentheses();
				for (Regiaovendedor regiaovendedor : regiao.getListaRegiaovendedor()) {
					query.where("pedidovenda.colaborador = ?", regiaovendedor.getColaborador()).or();
				}
				query.closeParentheses();
			}
		}
		
		if (filtro.getListaPedidoVendasituacao() != null && filtro.getListaPedidoVendasituacao().size() > 0 && !filtro.getListaPedidoVendasituacao().isEmpty()) {
			query.whereIn("pedidovenda.pedidovendasituacao", Pedidovendasituacao.listAndConcatenate(filtro.getListaPedidoVendasituacao()));
		}
		
		
		if (filtro.getListaSituacaoPendencia() != null && SinedUtil.isListNotEmpty(filtro.getListaSituacaoPendencia()) ) {
			query.openParentheses()
				.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.CONFIRMADO)
					.or()
				.whereIn("pedidovenda.situacaoPendencia", SituacaoPendencia.listAndConcatenate(filtro.getListaSituacaoPendencia()))
			.closeParentheses();
		}
		
		
		if (filtro.getListaProducaoagendasituacao() != null && filtro.getListaProducaoagendasituacao().size() > 0 && !filtro.getListaProducaoagendasituacao().isEmpty()) {
			Boolean isProducao = Boolean.FALSE, isCancelada = Boolean.FALSE, isProduzido = Boolean.FALSE, temVarios = Boolean.FALSE;;
			
			for (int i = 0 ; i < filtro.getListaProducaoagendasituacao().size() ; i++) {
				for (ProducaoagendasituacaoEnum situacoes : ProducaoagendasituacaoEnum.values()) {
					if (filtro.getListaProducaoagendasituacao().get(i) instanceof ProducaoagendasituacaoEnum) {
						if ((filtro.getListaProducaoagendasituacao().get(i)).equals(situacoes)) {
							if (situacoes.equals(ProducaoagendasituacaoEnum.EM_PRODUCAO)) {
								isProducao = Boolean.TRUE;
							} else if (situacoes.equals(ProducaoagendasituacaoEnum.CANCELADA)) {
								isCancelada = Boolean.TRUE;
							} else if (situacoes.equals(ProducaoagendasituacaoEnum.PRODUZIDO)) {
								isProduzido = Boolean.TRUE;
							}
						}
					} else {
						if (((Object) filtro.getListaProducaoagendasituacao().get(i)).equals(situacoes.name())) {
							if (situacoes.name().equals("EM_PRODUCAO")) {
								isProducao = Boolean.TRUE;
							} else if (situacoes.name().equals("CANCELADA")) {
								isCancelada = Boolean.TRUE;
							} else if (situacoes.name().equals("PRODUZIDO")) {
								isProduzido = Boolean.TRUE;
							}
						}
					}
				}
			}
			
			query
				.leftOuterJoin("pedidovenda.listaProduocaoagenda listaProduocaoagenda");
			
			if (isProducao && isCancelada && isProduzido) {
				query
					.where("listaProduocaoagenda is not null");
			} else if (isProducao || isCancelada || isProduzido) {
				query.openParentheses();
				
				if (isProducao) {
					if (temVarios) {
						query.or();
					} else {
						temVarios = Boolean.TRUE;
					}
					
					query
						.openParentheses()
						.where("exists (select pv " +
								"from Pedidovenda pv " +
								"join pv.listaProduocaoagenda producaoagenda " +
								"where pv.cdpedidovenda = pedidovenda.cdpedidovenda and  " +
								"producaoagenda.producaoagendasituacao in (" + Producaoagendasituacao.EM_ESPERA.getValue() + ", " +
										Producaoagendasituacao.EM_ANDAMENTO.getValue() + ", " + Producaoagendasituacao.CHAO_DE_FABRICA.getValue() + "))")
						.closeParentheses();
				}
				
				if (isCancelada) {
					if (temVarios) {
						query.or();
					} else {
						temVarios = Boolean.TRUE;
					}
	
					query
						.openParentheses() 
						.where("listaProduocaoagenda is not null")
						.where("not exists (select pv " +
							"from Pedidovenda pv " +
							"join pv.listaProduocaoagenda producaoagenda " +
							"where pv.cdpedidovenda = pedidovenda.cdpedidovenda and " +
							"producaoagenda.producaoagendasituacao not in (" + Producaoagendasituacao.CANCELADA.getValue() + "))")
						.closeParentheses();
				}
				
				if (isProduzido) {
					if (temVarios) {
						query.or();
					} else {
						temVarios = Boolean.TRUE;
					}
					
					query
						.openParentheses()
						.where("exists (select pv " +
							"from Pedidovenda pv " +
							"left join pv.listaProduocaoagenda producaoagenda " +
							"where pv.cdpedidovenda = pedidovenda.cdpedidovenda and " +
							"producaoagenda.producaoagendasituacao not in (" + Producaoagendasituacao.EM_ESPERA.getValue() + "" +
									", " + Producaoagendasituacao.EM_ANDAMENTO.getValue() + ", " + Producaoagendasituacao.CHAO_DE_FABRICA.getValue() + ") " +
									"and producaoagenda.producaoagendasituacao in (" + Producaoagendasituacao.CONCLUIDA.getValue() + "))")
						.closeParentheses();
				}
				
				query.closeParentheses();
			}
		}
		
		if(Boolean.TRUE.equals(filtro.getIsRestricaoVendaVendedor())){ 
			Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
			if(colaborador == null) {
				filtro.setNaoexibirresultado(true);
			}else {
				if(!Boolean.TRUE.equals(filtro.getIsRestricaoClienteVendedor())){
					filtro.setColaborador(colaborador);
					query.where("pedidovenda.colaborador = ?", colaborador);					
				}else {
					if(filtro.getColaborador() == null || !filtro.getColaborador().equals(colaborador)){
						if(filtro.getColaborador() == null){
							query.openParentheses()
								.where("listaClientevendedor.colaborador = ?", colaborador)
								.or()
								.where("pedidovenda.colaborador = ?", colaborador)
							.closeParentheses();
						}else {
							query.openParentheses()
								.where("listaClientevendedor.colaborador = ?", colaborador)
								.where("pedidovenda.colaborador = ?", filtro.getColaborador())
							.closeParentheses();
						}
					}else {
						query.where("pedidovenda.colaborador = ?", colaborador);
					}
					
				}
			}
		}else {
			if (filtro.getColaborador() != null && filtro.getTipoVendedor() != null && TipoVendedorEnum.PRINCIPAL.equals(filtro.getTipoVendedor())) {
				query
					.openParentheses()
						.where("listaClientevendedor.colaborador = ?", filtro.getColaborador())
						.where("listaClientevendedor.principal = ?", Boolean.TRUE)
					.closeParentheses();
			} else {
				query.where("pedidovenda.colaborador = ?",filtro.getColaborador());
			}
		}
		
		String solicitarCompraVenda = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.SOLICITARCOMPRAVENDA);
		if(solicitarCompraVenda != null){
			if("SOLICITACAOCOMPRA".equalsIgnoreCase(solicitarCompraVenda)) {
				//TRUE = Pedidos em aberto comprados | FALSE = Pedidos em processo de compra | NULL = Todos
				if(filtro.getPedidoprocessocompra() != null){
					if(filtro.getPedidoprocessocompra()){
						query
							.where("(select count(*) From Solicitacaocompra sc " +
										"join sc.solicitacaocompraorigem sco " +
										"join sco.pedidovenda scopv " +
										"where scopv.cdpedidovenda = pedidovenda.cdpedidovenda ) > 0 and" +
									"(select count(*) From Solicitacaocompra sc " +
										"join sc.solicitacaocompraorigem sco " +
										"join sco.pedidovenda scopv " +
										"where scopv.cdpedidovenda = pedidovenda.cdpedidovenda ) = " +
									"(select count(*) From Solicitacaocompra sc " +
										"join sc.solicitacaocompraorigem sco " +
										"join sco.pedidovenda scopv " +
										"join sc.aux_solicitacaocompra aux_sc " +
										"where scopv.cdpedidovenda = pedidovenda.cdpedidovenda and aux_sc.situacaosuprimentos = ?)", Situacaosuprimentos.BAIXADA);
					}else {
						query
							.where("(select count(*) From Solicitacaocompra sc " +
										"join sc.solicitacaocompraorigem sco " +
										"join sco.pedidovenda scopv " +
										"join sc.aux_solicitacaocompra aux_sc " +
										"where scopv.cdpedidovenda = pedidovenda.cdpedidovenda and aux_sc.situacaosuprimentos <> ?) > 0 ", Situacaosuprimentos.BAIXADA);
					}
				}
			}else if("ORDEMCOMPRA".equalsIgnoreCase(solicitarCompraVenda)) {
				//TRUE = Pedidos em aberto comprados | FALSE = Pedidos em processo de compra | NULL = Todos
				if(filtro.getPedidoprocessocompra() != null){
					if(filtro.getPedidoprocessocompra()){
						query
							.where("(select count(*) From Ordemcompra oc " +
										" join oc.aux_ordemcompra aux_ordemcompra" + 
										" join oc.listaMaterial listaMaterial " +
										" join listaMaterial.pedidovenda p " +
										" where p.cdpedidovenda = pedidovenda.cdpedidovenda ) > 0 and " +
								" (select count(*) From Ordemcompra oc " +
										" join oc.listaMaterial listaMaterial " +
										" join oc.aux_ordemcompra aux_ordemcompra " +
										" join listaMaterial.pedidovenda p " +
										" where p.cdpedidovenda = pedidovenda.cdpedidovenda ) = " +
								" (select count(*) From Ordemcompra oc " +
										" join oc.listaMaterial listaMaterial " +
										" join oc.aux_ordemcompra aux_ordemcompra " +
										" join listaMaterial.pedidovenda p " +
										" where p.cdpedidovenda = pedidovenda.cdpedidovenda and aux_ordemcompra.situacaosuprimentos = ?)", Situacaosuprimentos.BAIXADA);
					}else {
						query
							.where("(select count(*) From Ordemcompra oc " +
										" join oc.aux_ordemcompra aux_ordemcompra " + 
										" join oc.listaMaterial listaMaterial " +
										" join listaMaterial.pedidovenda p " +
										" where p.cdpedidovenda = pedidovenda.cdpedidovenda and aux_ordemcompra.situacaosuprimentos <> ?) > 0 ", Situacaosuprimentos.BAIXADA);
					}
				}	
			}
		}
		
		if(filtro.getVisualizarListagemreservas() != null && filtro.getVisualizarListagemreservas()){
			query
				.where("pedidovenda.pedidovendatipo.reserva = true")
				.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.CONFIRMADO)
				.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.CANCELADO);			
		}
		
		if (filtro.getEnderecoStr()!=null && !filtro.getEnderecoStr().trim().equals("")){
			query.whereLikeIgnoreAll("(coalesce(endereco.logradouro,'') || coalesce(endereco.numero,'') || coalesce(endereco.complemento,'') || " +
									 " coalesce(endereco.bairro,'') || coalesce(endereco.cep,'') || coalesce(endereco.pontoreferencia,'') || " +
									 " coalesce(municipio.nome,'') || coalesce(uf.sigla,''))", filtro.getEnderecoStr());
		}
		
		if(filtro.getPedidoProcessoLiberacaoWMS() != null){
			query
				.where("pedidovendatipo.confirmacaoManualWMS = TRUE")
				.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.PREVISTA);
			
			if(filtro.getPedidoProcessoLiberacaoWMS()){
				query.where("pedidovenda.identificadorcarregamento is NOT NULL");
			}else{
				query.where("pedidovenda.identificadorcarregamento is NULL");
			}
		}
		List<String> whereInNotSstatusComprovante = new ArrayList<String>();
		if(Boolean.TRUE.equals(filtro.getComprovanteEmitido())){
			query.where("exists(select pvh.acao from Pedidovendahistorico pvh where pvh.pedidovenda.cdpedidovenda = pedidovenda.cdpedidovenda and pvh.acao = ?)", Comprovantestatus.EMITIDO.toString());
		}else if(Boolean.FALSE.equals(filtro.getComprovanteEmitido())){
			whereInNotSstatusComprovante.add("'"+Comprovantestatus.EMITIDO.toString()+"'");
		}
		if(Boolean.TRUE.equals(filtro.getComprovanteEnviado())){
			query.where("exists(select pvh.acao from Pedidovendahistorico pvh where pvh.pedidovenda.cdpedidovenda = pedidovenda.cdpedidovenda and pvh.acao = ?)", Comprovantestatus.ENVIADO.toString());
		}else if(Boolean.FALSE.equals(filtro.getComprovanteEnviado())){
			whereInNotSstatusComprovante.add("'"+Comprovantestatus.ENVIADO.toString()+"'");
		}

		if(!whereInNotSstatusComprovante.isEmpty()){
			query.where("not exists(select pvh.acao from Pedidovendahistorico pvh where pvh.pedidovenda.cdpedidovenda = pedidovenda.cdpedidovenda and pvh.acao in ("+
					CollectionsUtil.concatenate(whereInNotSstatusComprovante, ",")+"))");
		}
		
		if(filtro.getLimitecreditoexcedido() != null){
			query.where("COALESCE(pedidovenda.limitecreditoexcedido, false) = " + (filtro.getLimitecreditoexcedido() ? "true" : "false"));
		}
		
		if(filtro.getPedidovendatipo() != null){
			if(filtro.getContaFinanceiroSituacao() != null && ContasFinanceiroSituacao.CONTA_EM_ABERTO.equals(filtro.getContaFinanceiroSituacao())){
				query.openParentheses()
				.where("coalesce(pedidovendatipo.permitirConfirmarPedidoBaixado, false) = false")
				.or()
				.openParentheses()
					.where("pedidovendatipo.permitirConfirmarPedidoBaixado = true")
					.where("exists(select 1 from Pedidovendapagamento pvp "+
							"		 join pvp.documento doc "+
							"		 join doc.documentoacao docAcao "+
							" 		where pvp.pedidovenda = pedidovenda" +
							"		  and (docAcao.cddocumentoacao = "+Documentoacao.DEFINITIVA.getCddocumentoacao()+" or docAcao.cddocumentoacao = "+Documentoacao.PREVISTA.getCddocumentoacao()+"))")
				.closeParentheses()
				.closeParentheses();
				 
			}
			
			if(filtro.getContaFinanceiroSituacao() != null && ContasFinanceiroSituacao.CONTA_BAIXADA.equals(filtro.getContaFinanceiroSituacao())){
				query.openParentheses()
				.where("coalesce(pedidovendatipo.permitirConfirmarPedidoBaixado, false) = false")
				.or()
				.openParentheses()
					.where("pedidovendatipo.permitirConfirmarPedidoBaixado = true")
					.where("exists(select 1 from Pedidovendapagamento pvp "+
							"		 join pvp.documento doc "+
							"		 join doc.documentoacao docAcao "+
							" 		where pvp.pedidovenda = pedidovenda" +
							"		  and docAcao.cddocumentoacao <> "+Documentoacao.DEFINITIVA.getCddocumentoacao()+" and docAcao.cddocumentoacao <> "+Documentoacao.PREVISTA.getCddocumentoacao()+")")
				.closeParentheses()
				.closeParentheses();
			}
		}
		
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {		
		
		Pedidovenda pedidovenda = (Pedidovenda) save.getEntity();
		
		save.saveOrUpdateManaged("listaPedidovendamaterial");
		save.saveOrUpdateManaged("listaPedidovendapagamento");
		save.saveOrUpdateManaged("listaPedidovendapagamentorepresentacao");
		save.saveOrUpdateManaged("listaPedidovendamaterialmestre");
		save.saveOrUpdateManaged("listaPedidoVendaFornecedorTicketMedio");
		
		if (pedidovenda.getListaPedidovendavalorcampoextra() != null && !pedidovenda.getListaPedidovendavalorcampoextra().isEmpty())
			save.saveOrUpdateManaged("listaPedidovendavalorcampoextra");
	}
	
	/**
	 * Carrega informa��es do pedido de venda para fazer a confirma��o.
	 *
	 * @param pedidovenda
	 * @return
	 * @since 09/11/2011
	 * @author Rodrigo Freitas
	 */
	public List<Pedidovenda> loadForConfirmacao(String whereIn){
		if(StringUtils.isEmpty(whereIn)){
			throw new SinedException("O pedido de venda n�o pode ser nulo.");
		}
		
		QueryBuilder<Pedidovenda> qry = query();
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("pedidovenda.cdpedidovenda, pedidovenda.dtpedidovenda, pedidovenda.observacao, pedidovenda.observacaointerna, pedidovenda.pedidovendasituacao, pedidovenda.presencacompradornfe, pedidovenda.identificadorcarregamento, pedidovenda.limitecreditoexcedido, ");
		sb.append("cliente.cdpessoa, cliente.nome, cliente.tipopessoa, cliente.cpf, cliente.cnpj, cliente.creditolimitecompra, contato.cdpessoa, contato.nome, pedidovenda.saldofinal, pedidovenda.identificador, pedidovenda.identificacaoexterna, ");
		sb.append("listaPedidovendamaterial.cdpedidovendamaterial, listaPedidovendamaterial.ordem, listaPedidovendamaterial.quantidade, listaPedidovendamaterial.preco, listaPedidovendamaterial.desconto, listaPedidovendamaterial.percentualdesconto, listaPedidovendamaterial.unidademedida, listaPedidovendamaterial.dtprazoentrega, ");
		sb.append("listaPedidovendamaterial.peso, pedidovendamaterialgarantido.cdpedidovendamaterial, listaPedidovendamaterial.descontogarantiareforma, garantiareformaitem.cdgarantiareformaitem, material.cdmaterial, material.nome, material.nome, material.codigofabricante, material.qtdeunidade, material.considerarvendamultiplos, material.pesoliquidovalorvenda, material.peso, material.pesobruto, material.obrigarlote, material.vendaecf, material.tributacaoipi, ");
		sb.append("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, ");
		sb.append("endereco.cep, endereco.pontoreferencia, municipio.nome, uf.sigla, listaPedidovendamaterial.valorcustomaterial, material.valorcusto, material.valorvenda, listaPedidovendamaterial.valorvendamaterial, ");
		sb.append("materialgrupo.cdmaterialgrupo, materialgrupo.nome, material.produto, material.patrimonio, material.epi, material.servico, materialmestre.cdmaterial, ");
		sb.append("material.identificacao, material.valorvendaminimo, material.valorvendamaximo, material.vendapromocional, material.kitflexivel, material.producao, listaPedidovendamaterial.fatorconversao, listaPedidovendamaterial.qtdereferencia, ");
		sb.append("empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.cdpessoa, pedidovenda.valorusadovalecompra, ");
		sb.append("colaborador.cdpessoa, colaborador.nome, loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, ");
		sb.append("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, listaPedidovendamaterial.observacao, listaPedidovendamaterial.valoripi, listaPedidovendamaterial.ipi, listaPedidovendamaterial.tipocobrancaipi, listaPedidovendamaterial.aliquotareaisipi, listaPedidovendamaterial.tipocalculoipi, grupotributacao.cdgrupotributacao, ");
		sb.append("prazopagamento.cdprazopagamento, prazopagamento.nome, prazopagamento.avista, prazopagamento.juros, indicecorrecao.cdindicecorrecao, conta.cdconta, documentotipo.cddocumentotipo, documentotipo.antecipacao, projeto.cdprojeto, ");
		sb.append("contagerencial.cdcontagerencial, contagerencial.nome, unidademedida.cdunidademedida, unidademedida.simbolo, vcontagerencial.identificador, ");
		sb.append("pedidovendatipo.cdpedidovendatipo, pedidovendatipo.reserva,  pedidovendatipo.bonificacao, pedidovendatipo.comodato, pedidovendatipo.baixaestoqueEnum, pedidovendatipo.geracaocontareceberEnum, pedidovendatipo.permitirvendasemestoque, ");
		sb.append("pedidovendatipo.obrigarLoteConfirmarpedido, pedidovendatipo.representacao, pedidovendatipo.confirmacaoManualWMS, pedidovendatipo.atualizaPedidoVendaWMS, pedidovendatipo.bloqAlteracaoPreco, ");
		sb.append("pedidovendatipo.considerarvalorcusto, pedidovendatipo.consideraripiconta, pedidovendatipo.consideraricmsstconta, pedidovendatipo.considerarfcpstconta, pedidovendatipo.considerardesoneracaoconta, ");
		sb.append("pedidovenda.frete, terceiro.cdpessoa, terceiro.nome, pedidovenda.valorfrete, pedidovenda.desconto, pedidovenda.taxapedidovenda, pedidovenda.prazomedio, pedidovenda.valorFreteCIF, ");
		sb.append("listaPedidovendamaterial.altura, listaPedidovendamaterial.largura, listaPedidovendamaterial.comprimento, listaPedidovendamaterial.comprimentooriginal, listaPedidovendamaterial.multiplicador, listaPedidovendamaterial.bandaalterada, listaPedidovendamaterial.servicoalterado, ");
		sb.append("materialcoleta.cdmaterial, materialcoleta.nome, materialcoleta.identificacao, fornecedor.cdpessoa, fornecedor.nome, ");
		sb.append("unidademedidaM.cdunidademedida, listaPedidovendamaterial.identificadorespecifico, listaPedidovendamaterial.identificadorintegracao, listaPedidovendamaterial.identificadorinterno, localarmazenagemcoleta.cdlocalarmazenagem, unidademedida.casasdecimaisestoque, ");
		sb.append("clienteindicacao.cdpessoa, clienteindicacao.nome, ");
        sb.append("enderecoFaturamento.cdendereco, ");
		sb.append("listaPedidovendamaterial.percentualrepresentacao, pedidovenda.valordescontorepresentacao, pedidovenda.valorbrutorepresentacao, ");
		sb.append("listaPedidovendamaterial.custooperacional, listaPedidovendamaterial.valorMinimo, ");
		sb.append("listaPedidovendamaterial.percentualrepresentacao, listaPedidovendamaterial.custooperacional, ");
		sb.append("listaPedidovendamaterial.percentualcomissaoagencia, listaPedidovendamaterial.valorSeguro, listaPedidovendamaterial.outrasdespesas, ");
		sb.append("listaPedidovendamaterial.valoripi, listaPedidovendamaterial.ipi, listaPedidovendamaterial.tipocobrancaipi, listaPedidovendamaterial.aliquotareaisipi, listaPedidovendamaterial.tipocalculoipi, grupotributacao.cdgrupotributacao, ");
		sb.append("listaPedidovendamaterial.tipotributacaoicms, ");
		sb.append("listaPedidovendamaterial.tipocobrancaicms, listaPedidovendamaterial.valorbcicms, listaPedidovendamaterial.icms, ");
		sb.append("listaPedidovendamaterial.valoricms, listaPedidovendamaterial.valorbcicmsst, listaPedidovendamaterial.icmsst, ");
		sb.append("listaPedidovendamaterial.valoricmsst, listaPedidovendamaterial.reducaobcicmsst, listaPedidovendamaterial.margemvaloradicionalicmsst, ");
		sb.append("listaPedidovendamaterial.valorbcfcp, listaPedidovendamaterial.fcp, listaPedidovendamaterial.valorfcp, ");
		sb.append("listaPedidovendamaterial.valorbcfcpst, listaPedidovendamaterial.fcpst, listaPedidovendamaterial.valorfcpst, ");
		sb.append("listaPedidovendamaterial.valorbcdestinatario, listaPedidovendamaterial.valorbcfcpdestinatario, listaPedidovendamaterial.fcpdestinatario, ");
		sb.append("listaPedidovendamaterial.icmsdestinatario, listaPedidovendamaterial.icmsinterestadual, listaPedidovendamaterial.icmsinterestadualpartilha, ");
		sb.append("listaPedidovendamaterial.valorfcpdestinatario, listaPedidovendamaterial.valoricmsdestinatario, listaPedidovendamaterial.valoricmsremetente, ");
		sb.append("listaPedidovendamaterial.difal, listaPedidovendamaterial.valordifal, ncmcapitulo.cdncmcapitulo, cfop.cdcfop, ");
		sb.append("listaPedidovendamaterial.ncmcompleto, listaPedidovendamaterial.modalidadebcicms, listaPedidovendamaterial.modalidadebcicmsst, ");
		sb.append("listaPedidovendamaterial.percentualdesoneracaoicms, listaPedidovendamaterial.valordesoneracaoicms, listaPedidovendamaterial.abaterdesoneracaoicms, ");
		sb.append("prazopagamentorepresentacao.cdprazopagamento, ");
		sb.append("documentotiporepresentacao.cddocumentotipo, ");
		sb.append("contaboletorepresentacao.cdconta, ");
		sb.append("agencia.cdpessoa, agencia.nome, agencia.cpf, agencia.cnpj, listaPedidovendamaterial.percentualcomissaoagencia, ");
		sb.append("listaPedidovendamaterial.pesomedio, ");
		sb.append("centrocusto.cdcentrocusto, centrocusto.nome, comissionamento.cdcomissionamento ");
		
		qry.select(sb.toString())
			 .leftOuterJoin("pedidovenda.cliente cliente")
			 .leftOuterJoin("pedidovenda.clienteindicacao clienteindicacao")
			 .leftOuterJoin("pedidovenda.contato contato")
			 .leftOuterJoin("pedidovenda.prazopagamento prazopagamento")
			 .leftOuterJoin("pedidovenda.indicecorrecao indicecorrecao")
			 .leftOuterJoin("pedidovenda.projeto projeto")
			 .leftOuterJoin("pedidovenda.conta conta")
			 .leftOuterJoin("pedidovenda.documentotipo documentotipo")
			 .leftOuterJoin("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
			 .leftOuterJoin("listaPedidovendamaterial.pneu pneu")
			 .leftOuterJoin("listaPedidovendamaterial.material material")
			 .leftOuterJoin("listaPedidovendamaterial.cfop cfop")
			 .leftOuterJoin("listaPedidovendamaterial.ncmcapitulo ncmcapitulo")
			 .leftOuterJoin("material.unidademedida unidademedidaM")
			 .leftOuterJoin("listaPedidovendamaterial.materialmestre materialmestre")
			 .leftOuterJoin("listaPedidovendamaterial.loteestoque loteestoque")
			 .leftOuterJoin("listaPedidovendamaterial.fornecedor fornecedor")
			 .leftOuterJoin("pedidovenda.empresa empresa")
			 .leftOuterJoin("pedidovenda.endereco endereco")
             .leftOuterJoin("pedidovenda.enderecoFaturamento enderecoFaturamento")
			 .leftOuterJoin("pedidovenda.colaborador colaborador")
			 .leftOuterJoin("endereco.municipio municipio")
			 .leftOuterJoin("municipio.uf uf")
			 .leftOuterJoin("material.materialgrupo materialgrupo")
			 .leftOuterJoin("material.contagerencial contagerencial")
			 .leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			 .leftOuterJoin("listaPedidovendamaterial.unidademedida unidademedida")
			 .leftOuterJoin("pedidovenda.localarmazenagem localarmazenagem")
			 .leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
			 .leftOuterJoin("pedidovenda.terceiro terceiro")
			 .leftOuterJoin("pedidovenda.frete frete")
			 .leftOuterJoin("listaPedidovendamaterial.materialcoleta materialcoleta")
			 .leftOuterJoin("pedidovendatipo.localarmazenagemcoleta localarmazenagemcoleta")
			 .leftOuterJoin("pedidovenda.contaboletorepresentacao contaboletorepresentacao")
			 .leftOuterJoin("pedidovenda.documentotiporepresentacao documentotiporepresentacao")
			 .leftOuterJoin("pedidovenda.prazopagamentorepresentacao prazopagamentorepresentacao")
			 .leftOuterJoin("pedidovenda.agencia agencia")
			 .leftOuterJoin("listaPedidovendamaterial.grupotributacao grupotributacao")
			 .leftOuterJoin("listaPedidovendamaterial.comissionamento comissionamento")
			 .leftOuterJoin("pedidovenda.centrocusto centrocusto")
			 .leftOuterJoin("listaPedidovendamaterial.pedidovendamaterialgarantido pedidovendamaterialgarantido")
			 .leftOuterJoin("listaPedidovendamaterial.garantiareformaitem garantiareformaitem")
			 .whereIn("pedidovenda.cdpedidovenda", whereIn)
			 .orderBy("pedidovenda.cdpedidovenda, listaPedidovendamaterial.ordem, material.nome");
		qry = SinedUtil.setJoinsByComponentePneu(qry);
		return qry.list();
	}
	
	public List<Pedidovenda> findForSolicitacaocompra(String whereIn){
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Pedidovenda> qry = query();
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("pedidovenda.cdpedidovenda, pedidovenda.dtpedidovenda, pedidovenda.observacao, pedidovenda.observacaointerna, pedidovenda.pedidovendasituacao,  ");
		sb.append("cliente.cdpessoa, cliente.nome, cliente.tipopessoa, cliente.cpf, cliente.cnpj, pedidovenda.saldofinal, ");
		sb.append("listaPedidovendamaterial.cdpedidovendamaterial, listaPedidovendamaterial.quantidade, listaPedidovendamaterial.preco, listaPedidovendamaterial.desconto, listaPedidovendamaterial.unidademedida, listaPedidovendamaterial.dtprazoentrega, ");
		sb.append("material.cdmaterial, material.nome, material.nome, material.codigofabricante, material.qtdeunidade, material.considerarvendamultiplos, ");
		sb.append("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, ");
		sb.append("endereco.cep, endereco.pontoreferencia, municipio.nome, uf.sigla, listaPedidovendamaterial.valorcustomaterial, material.valorcusto, material.valorvenda, listaPedidovendamaterial.valorvendamaterial, ");
		sb.append("materialgrupo.cdmaterialgrupo, materialgrupo.nome, material.produto, material.patrimonio, material.epi, material.servico, ");
		sb.append("material.valorvendaminimo, material.valorvendamaximo, listaPedidovendamaterial.fatorconversao, listaPedidovendamaterial.qtdereferencia, ");
		sb.append("empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.cdpessoa, pedidovenda.valorusadovalecompra, ");
		sb.append("colaborador.cdpessoa, colaborador.nome, loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, ");
		sb.append("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, listaPedidovendamaterial.observacao, ");
		sb.append("prazopagamento.cdprazopagamento, prazopagamento.juros, indicecorrecao.cdindicecorrecao, conta.cdconta, documentotipo.cddocumentotipo, documentotipo.antecipacao, projeto.cdprojeto, ");
		sb.append("contagerencial.cdcontagerencial, contagerencial.nome, unidademedida.cdunidademedida, unidademedida.simbolo, vcontagerencial.identificador, ");
		sb.append("pedidovendatipo.cdpedidovendatipo, pedidovendatipo.reserva,  pedidovendatipo.bonificacao, pedidovendatipo.comodato, ");
		sb.append("pedidovenda.frete, terceiro.cdpessoa, terceiro.nome, pedidovenda.valorfrete, pedidovenda.desconto, pedidovenda.taxapedidovenda, pedidovenda.prazomedio, ");
		sb.append("listaPedidovendamaterial.altura, listaPedidovendamaterial.largura, listaPedidovendamaterial.comprimento, listaPedidovendamaterial.comprimentooriginal, listaPedidovendamaterial.multiplicador ");
		
		qry.select(sb.toString())
			 .leftOuterJoin("pedidovenda.cliente cliente")
			 .leftOuterJoin("pedidovenda.prazopagamento prazopagamento")
			 .leftOuterJoin("pedidovenda.indicecorrecao indicecorrecao")
			 .leftOuterJoin("pedidovenda.projeto projeto")
			 .leftOuterJoin("pedidovenda.conta conta")
			 .leftOuterJoin("pedidovenda.documentotipo documentotipo")
			 .leftOuterJoin("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
			 .leftOuterJoin("listaPedidovendamaterial.material material")
			 .leftOuterJoin("listaPedidovendamaterial.loteestoque loteestoque")
			 .leftOuterJoin("pedidovenda.empresa empresa")
			 .leftOuterJoin("pedidovenda.endereco endereco")
			 .leftOuterJoin("pedidovenda.colaborador colaborador")
			 .leftOuterJoin("endereco.municipio municipio")
			 .leftOuterJoin("municipio.uf uf")
			 .leftOuterJoin("material.materialgrupo materialgrupo")
			 .leftOuterJoin("material.contagerencial contagerencial")
			 .leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			 .leftOuterJoin("listaPedidovendamaterial.unidademedida unidademedida")
			 .leftOuterJoin("pedidovenda.localarmazenagem localarmazenagem")
			 .leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
			 .leftOuterJoin("pedidovenda.terceiro terceiro")
			 .leftOuterJoin("pedidovenda.frete frete")
			 .whereIn("pedidovenda.cdpedidovenda", whereIn)
			 .orderBy("pedidovenda.cdpedidovenda, listaPedidovendamaterial.ordem");
		
		return qry.list();
	}
	
	/**
	 * Carrega informa��es para a gera��o do comprovante de pedido de venda.
	 *
	 * @param cdpedidovenda
	 * @return
	 * @since 09/11/2011
	 * @author Rodrigo Freitas
	 */
	public Pedidovenda loadForComprovante(Integer cdpedidovenda){
		return query()
		.select("pedidovenda.cdpedidovenda, pedidovenda.dtpedidovenda, pedidovendamaterial.ordem, pedidovendamaterial.quantidade, pedidovendamaterial.preco, pedidovendamaterial.desconto, pedidovenda.identificacaoexterna, " +
				"cliente.cdpessoa, cliente.nome, cliente.email, contato.cdpessoa, contato.nome, colaborador.cdpessoa, colaborador.nome, colaborador.email, pedidovenda.pedidovendasituacao, " +
				"pedidovenda.observacao, pedidovenda.observacaoPedidoVendaTipo, pedidovendamaterial.dtprazoentrega, pedidovenda.taxapedidovenda, endereco.descricao, endereco.substituirlogradouro, " +
				"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.email, empresa.site, empresa.cnpj, empresa.inscricaoestadual, logomarca.cdarquivo, " +
				"frete.nome, terceiro.nome, frete.cdResponsavelFrete, pedidovenda.valorfrete,  pedidovenda.desconto, documentotipo.nome, " +
				"endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, pedidovendamaterial.observacao, pedidovendamaterial.peso, " + 
				"municipio.nome, municipio.cdibge, uf.sigla, uf.nome, endereco.cep, endereco.pontoreferencia, empresa.observacaovenda," +
				"prazopagamento.cdprazopagamento, prazopagamento.nome, pedidovenda.identificador, vendaorcamento.cdvendaorcamento, " +
				"projeto.cdprojeto, projeto.nome, pedidovendamaterial.multiplicador, pedidovendatipo.descricao, pedidovenda.observacaointerna, pedidovenda.dtaltera, pedidovendamaterial.identificadorespecifico, pedidovendamaterial.identificadorintegracao, " +
				"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, localarmazenagem.qtdeacimaestoque, localarmazenagem.msgacimaestoque, pedidovenda.valorusadovalecompra")
		.leftOuterJoin("pedidovenda.cliente cliente")
		.leftOuterJoin("pedidovenda.contato contato")
		.leftOuterJoin("pedidovenda.listaPedidovendamaterial pedidovendamaterial")
		.leftOuterJoin("pedidovenda.colaborador colaborador")
		.leftOuterJoin("pedidovenda.empresa empresa")
		.leftOuterJoin("empresa.logomarca logomarca")
		.leftOuterJoin("pedidovenda.terceiro terceiro")
		.leftOuterJoin("pedidovenda.frete frete")
		.leftOuterJoin("pedidovenda.documentotipo documentotipo")
		.leftOuterJoin("pedidovenda.endereco endereco")
		.leftOuterJoin("endereco.municipio municipio")
		.leftOuterJoin("municipio.uf uf")
		.leftOuterJoin("pedidovenda.prazopagamento prazopagamento")
		.leftOuterJoin("pedidovenda.vendaorcamento vendaorcamento")
		.leftOuterJoin("pedidovenda.projeto projeto")
		.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
		.leftOuterJoin("pedidovenda.localarmazenagem localarmazenagem")
		.where("pedidovenda.cdpedidovenda = ?", cdpedidovenda)
		.orderBy("pedidovendamaterial.ordem")
		.unique();
	}
	
	/**
	 * M�todo que busca as vendas para gerar comprovantes de uma s� vez
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovenda> findForComprovante(String whereIn){
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()	
			.select("pedidovenda.cdpedidovenda, pedidovenda.dtpedidovenda, pedidovendamaterial.ordem, pedidovendamaterial.quantidade, pedidovendamaterial.preco, pedidovendamaterial.desconto, pedidovenda.identificacaoexterna, " +
					"cliente.cdpessoa, cliente.nome, cliente.email, contato.cdpessoa, contato.nome, colaborador.cdpessoa, colaborador.nome, colaborador.email, pedidovenda.pedidovendasituacao, pedidovenda.observacao, pedidovendamaterial.dtprazoentrega, " +
					"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.email, empresa.site, empresa.cnpj, empresa.inscricaoestadual, logomarca.cdarquivo, pedidovenda.valorfrete, " +
					"frete.cdResponsavelFrete, frete.nome, terceiro.nome, pedidovenda.desconto, documentotipo.nome, pedidovendamaterial.observacao, " +
					"endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, projeto.cdprojeto, projeto.nome, pedidovenda.valorusadovalecompra, pedidovenda.observacaoPedidoVendaTipo, " + 
					"municipio.nome, municipio.cdibge, uf.sigla, uf.nome, endereco.cep, endereco.pontoreferencia, empresa.observacaovenda, pedidovenda.observacaointerna,pedidovendatipo.descricao, pedidovenda.dtaltera, localarmazenagem.nome ")
			.leftOuterJoin("pedidovenda.cliente cliente")
			.leftOuterJoin("pedidovenda.contato contato")
			.leftOuterJoin("pedidovenda.listaPedidovendamaterial pedidovendamaterial")
			.leftOuterJoin("pedidovenda.colaborador colaborador")
			.leftOuterJoin("pedidovenda.empresa empresa")
			.leftOuterJoin("empresa.logomarca logomarca")
			.leftOuterJoin("pedidovenda.terceiro terceiro")
			.leftOuterJoin("pedidovenda.frete frete")
			.leftOuterJoin("pedidovenda.documentotipo documentotipo")
			.leftOuterJoin("pedidovenda.endereco endereco")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("pedidovenda.projeto projeto")
			.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
			.leftOuterJoin("pedidovenda.localarmazenagem localarmazenagem")
			.whereIn("pedidovenda.cdpedidovenda", whereIn)
			.orderBy("pedidovendamaterial.ordem")
			.list();
	}

	/**
	 * Busca informa��es do pedido de venda para o cancelamento.
	 * 
	 * @param whereIn
	 * @return
	 * @since 07/11/2011
	 * @author Rodrigo Freitas
	 */
	public List<Pedidovenda> findForCancelamento(String whereIn) {
		return query()
		
		.select("pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao, pedidovenda.identificadorcarregamento, empresa.cdpessoa, empresa.integracaowms, " +
				"cliente.cdpessoa, pedidovenda.valorusadovalecompra, localarmazenagem.cdlocalarmazenagem, " +
				"listaPedidovendamaterial.cdpedidovendamaterial, listaPedidovendamaterial.quantidade, material.cdmaterial, garantiareformaitem.cdgarantiareformaitem, " +
				"unidademedida.cdunidademedida, loteestoque.cdloteestoque ")
		.leftOuterJoin("pedidovenda.empresa empresa")
		.leftOuterJoin("pedidovenda.localarmazenagem localarmazenagem")
		.leftOuterJoin("pedidovenda.cliente cliente")
		.leftOuterJoin("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
		.leftOuterJoin("listaPedidovendamaterial.material material")
		.leftOuterJoin("listaPedidovendamaterial.unidademedida unidademedida")
		.leftOuterJoin("listaPedidovendamaterial.garantiareformaitem garantiareformaitem")
		.leftOuterJoin("listaPedidovendamaterial.loteestoque loteestoque")
		.whereIn("pedidovenda.cdpedidovenda", whereIn)
		.list();
	}

	/**
	 * Atualiza o pedido de venda para a situa��o cancelada.
	 *
	 * @param whereIn
	 * @since 07/11/2011
	 * @author Rodrigo Freitas
	 */
	public void updateCancelamento(String whereIn) {
		getHibernateTemplate()
		.bulkUpdate("update Pedidovenda p set p.pedidovendasituacao = ? where p.id in (" + whereIn + ")", 
				new Object[]{Pedidovendasituacao.CANCELADO});
	}
	
	/**
	 * Atualiza o pedido de venda para a situa��o confirmado.
	 *
	 * @param whereIn
	 * @since 08/11/2011
	 * @author Rodrigo Freitas
	 */
	public void updateConfirmacao(String whereIn) {
		getHibernateTemplate()
		.bulkUpdate("update Pedidovenda p set p.pedidovendasituacao = ? where p.id in (" + whereIn + ")", 
				new Object[]{Pedidovendasituacao.CONFIRMADO});
	}
	
	/**
	 * Atualiza o pedido de venda para a situa��o prevista.
	 *
	 * @param whereIn
	 * @since 25/04/2012
	 * @author Rodrigo Freitas
	 */
	public void updatePrevista(String whereIn) {
		getHibernateTemplate()
		.bulkUpdate("update Pedidovenda p set p.pedidovendasituacao = ? where p.id in (" + whereIn + ")", 
				new Object[]{Pedidovendasituacao.PREVISTA});
	}
	public void updateDepencia(String whereIn) {
		getHibernateTemplate()
		.bulkUpdate("update Pedidovenda p set p.situacaoPendencia = ? where p.id in (" + whereIn + ")", 
				new Object[]{SituacaoPendencia.PEDIDO_PREVISTO});
	}
	
	
	/**
	 * Atualiza o pedido de venda para a situa��o confirmado.
	 *
	 * @param whereIn
	 * @since 08/11/2011
	 * @author Rodrigo Freitas
	 */
	public void updateConfirmacaoParcial(String whereIn) {
		getHibernateTemplate()
		.bulkUpdate("update Pedidovenda p set p.pedidovendasituacao = ? where p.id in (" + whereIn + ")", 
				new Object[]{Pedidovendasituacao.CONFIRMADO_PARCIALMENTE});
	}
	

	
	
		

	/**
	 * Verifica se tem alguma venda nas situa��es passadas por par�metro.
	 *
	 * @param whereIn
	 * @param situacoes
	 * @return
	 * @since 09/11/2011
	 * @author Rodrigo Freitas
	 */
	public boolean havePedidovendaSituacao(String whereIn, Pedidovendasituacao[] situacoes) {
		List<Pedidovendasituacao> lista = new ArrayList<Pedidovendasituacao>();
		for (int i = 0; i < situacoes.length; i++) {
			lista.add(situacoes[i]);
		}
		
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Pedidovenda.class)
					.whereIn("pedidovenda.cdpedidovenda", whereIn)
					.whereIn("pedidovenda.pedidovendasituacao", Pedidovendasituacao.listAndConcatenate(lista));
		
		return query.unique() > 0;
	}
	
	/**
	 * M�todo que retorna true caso o pedido tenha material de produ��o
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 03/02/2014
	 */
	public boolean haveMaterialproducao(String whereIn) {
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Pedidovenda.class)
					.join("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
					.join("listaPedidovendamaterial.material material")
					.whereIn("pedidovenda.cdpedidovenda", whereIn)
					.where("material.producao = true");
		
		return query.unique() > 0;
	}
	
	/**
	 * Verifica se tem alguma venda diferente das situa��es passadas por par�metro.
	 *
	 * @param whereIn
	 * @param situacoes
	 * @return
	 * @since 12/04/2012
	 * @author Rodrigo Freitas
	 */
	public boolean havePedidovendaDiferenteSituacao(String whereIn, Pedidovendasituacao[] situacoes) {
		List<Pedidovendasituacao> lista = new ArrayList<Pedidovendasituacao>();
		for (int i = 0; i < situacoes.length; i++) {
			lista.add(situacoes[i]);
		}
		
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Pedidovenda.class)
					.whereIn("pedidovenda.cdpedidovenda", whereIn)
					.whereIn("pedidovenda.pedidovendasituacao not", Pedidovendasituacao.listAndConcatenate(lista));
		
		return query.unique() > 0;
	}
	
	/**
	 * Verifica se tem algum pedido de venda com diferente de reserva.
	 *
	 * @param whereIn
	 * @param situacoes
	 * @return
	 * @since 12/04/2012
	 * @author Rodrigo Freitas
	 */
	public boolean havePedidovendaDiferenteReserva(String whereIn) {		
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Pedidovenda.class)
					.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
					.whereIn("pedidovenda.cdpedidovenda", whereIn)
					.where("(pedidovendatipo.cdpedidovendatipo is null or pedidovendatipo.reserva = false)");
		return query.unique() > 0;
	}
	
	/**
	* M�todo que verifica se o pedido de venda est� marcado para sincronizar com o wms
	*
	* @param whereIn
	* @return
	* @since 04/03/2015
	* @author Luiz Fernando
	*/
	public boolean havePedidovendaIntegracaoWms(String whereIn) {		
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Pedidovenda.class)
					.join("pedidovenda.pedidovendatipo pedidovendatipo")
					.whereIn("pedidovenda.cdpedidovenda", whereIn)
					.where("coalesce(pedidovendatipo.sincronizarComWMS, false) = true")
					.where("coalesce(pedidovendatipo.confirmacaoManualWMS, false) = false");
		return query.unique() > 0;
	}
	
	/**
	* M�todo que verifica se dentre os itens do wherein existem pedidos de venda sincronizados com o wms 
	* mas pendentes de libera��o para confirma��o manual
	*
	* @param whereIn
	* @return
	* @author Rafael Salvio
	*/
	public boolean havePedidovendaIntegracaoManualPendenteWms(String whereIn) {		
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Pedidovenda.class)
					.join("pedidovenda.pedidovendatipo pedidovendatipo")
					.whereIn("pedidovenda.cdpedidovenda", whereIn)
					.where("coalesce(pedidovendatipo.sincronizarComWMS, false) = true")
					.where("coalesce(pedidovendatipo.confirmacaoManualWMS, false) = true")
					.where("pedidovenda.identificadorcarregamento is NULL");
		return query.unique() > 0;
	}
	
	/**
	 * Verifica se tem algum pedido de venda diferente de bonifica��o.
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean havePedidovendaDiferenteBonificacao(String whereIn) {		
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Pedidovenda.class)
					.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
					.whereIn("pedidovenda.cdpedidovenda", whereIn)
					.where("(pedidovendatipo.cdpedidovendatipo is null or pedidovendatipo.bonificacao = false)");
		return query.unique() > 0;
	}

	/**
	 * Busca os pedidos de venda a partir dos itens passados por par�metros.
	 *
	 * @param whereIn
	 * @return
	 * @since 16/11/2011
	 * @author Rodrigo Freitas
	 */
	public List<Pedidovenda> findByItens(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Itens n�o podem ser nulo.");
		}
		return query()
					.select("pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao")
					.join("pedidovenda.listaPedidovendamaterial pedidovendamaterial")
					.whereIn("pedidovendamaterial.cdpedidovendamaterial", whereIn)
					.list();
	}

	/**
	 * Atualiza o campo ordemcompra da tabela Pedidovenda.
	 *
	 * @param pedidovenda
	 * @since 12/04/2012
	 * @author Rodrigo Freitas
	 */
	public void updateOrdemcompra(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null){
			throw new SinedException("Pedido de venda n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Pedidovenda p set p.ordemcompra = true where p = ?", pedidovenda);
	}


	/**
	 * Atualiza a mensagem de erro de um pedido de venda.
	 *
	 * @since 12/03/2012
	 * @author Igor Silv�rio Costa
	 */
	public void updateMsgErro(Pedidovenda form, String message) {
		if(form == null || form.getCdpedidovenda()==null || form.getCdpedidovenda()==0)
			throw new SinedException("Pedido de venda n�o pode ser nulo ao atualizar a mensagem de erro.");
		
		getJdbcTemplate().update("update pedidovenda set msgerro=? where cdpedidovenda=?", new Object[]{form.getCdpedidovenda(), message});
	}

	/**
	 * Retorna a quantidade de Pedidovenda 'PREVISTO'
	 *
	 * @return
	 * @since 24/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdePrevistos() {
		return newQueryBuilderSined(Long.class)
					
					.from(Pedidovenda.class)
					.setUseTranslator(false) 
					.select("count(*)")
					.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.PREVISTA)
					.unique()
					.intValue();
	}

	/**
	 * M�todo que atualiza o pedido de venda que estava aguardando aprova��o por causa da restri��o do cliente
	 *
	 * @param whereIn
	 * @author Luiz Fernando
	 */
	public void updatePedidoAguardandoAprovacaoRestricaocliente(String whereIn) {
		if(whereIn != null && !"".equals(whereIn)){
			getJdbcTemplate().update("update pedidovenda set pedidovendasituacao = 0, integrar = true where pedidovendasituacao = ? and cdpedidovenda in (" + whereIn + ")", new Object[]{Pedidovendasituacao.AGUARDANDO_APROVACAO.getValue()});
		}
		
	}

	/**
	 * M�todo que busca os cdpedidovenda que estejam aguardando aprova��o por causa da restri��o de libera��o do cliente
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public String findForRestricaocliente(Cliente cliente) {
		StringBuilder s = new StringBuilder();
		
		List<Integer> lista = new ArrayList<Integer>();
		if(cliente != null && cliente.getCdpessoa() != null){
			lista = newQueryBuilderSined(Integer.class)
					.from(Pedidovenda.class)
					.setUseTranslator(false) 
					.select("pedidovenda.cdpedidovenda")
					.join("pedidovenda.cliente cliente")
					.join("pedidovenda.pedidovendatipo pedidovendatipo")
					.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.AGUARDANDO_APROVACAO)
					.where("cliente = ?", cliente)
					.where("pedidovenda.restricaocliente = true")
					.where("pedidovendatipo.sincronizarComWMS = true")
					.list();
		}
		
		if(lista != null && !lista.isEmpty()){
			for(Integer cdpedidovenda : lista){
				if(!"".equals(s.toString()))
					s.append(",");
				s.append(cdpedidovenda);
			}
		}
		return s.toString();
	}

	/**
	 * M�todo que carrega os pedidos de venda para serem sincronizados
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovenda> findForSincronizacaowms(String whereIn) {	
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("pedidovendamaterial.cdpedidovendamaterial, pedidovendamaterial.observacao, material.cdmaterial, " +
					"pedidovendamaterial.preco, pedidovendamaterial.desconto, pedidovendamaterial.quantidade, transportador.cdpessoa, " +
					"empresa.cdpessoa, empresa.integracaowms, pedidovenda.cdpedidovenda, pedidovenda.observacaointerna, " +
					"pedidovendapagamento.cdpedidovendapagamento, pedidovendapagamento.banco, " +
					"pedidovendapagamento.agencia, pedidovendapagamento.conta, pedidovendapagamento.numero, " +
					"pedidovendapagamento.valororiginal, pedidovendapagamento.dataparcela, documentotipo.cddocumentotipo, " +
					"documentotipo.antecipacao, documento.cddocumento, documento.numero, documento.valor, " +
					"documentoantecipacao.cddocumento, documentoantecipacao.numero, documentoantecipacao.valor, " +
					"cliente.cdpessoa, empresa.cdpessoa, endereco.cdendereco, materialmestregrade.cdmaterial, " +
					"listaMaterialunidademedida.cdmaterialunidademedida, listaMaterialunidademedida.fracao, listaMaterialunidademedida.valorunitario, " +
					"listaMaterialunidademedida.qtdereferencia, unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo")
			.join("pedidovenda.listaPedidovendamaterial pedidovendamaterial")
			.join("pedidovenda.endereco endereco")
			.join("pedidovenda.listaPedidovendapagamento pedidovendapagamento") 
			.join("pedidovenda.cliente cliente")
			.join("pedidovenda.empresa empresa")
			.join("pedidovendamaterial.material material")
			.leftOuterJoin("material.materialmestregrade materialmestregrade")
			.leftOuterJoin("material.listaMaterialunidademedida listaMaterialunidademedida")
			.leftOuterJoin("listaMaterialunidademedida.unidademedida unidademedida")
			.leftOuterJoin("pedidovendapagamento.documentotipo documentotipo")
			.leftOuterJoin("pedidovendapagamento.documento documento")
			.leftOuterJoin("pedidovendapagamento.documentoantecipacao documentoantecipacao")
			.leftOuterJoin("pedidovenda.transportador transportador")
			.whereIn("pedidovenda.cdpedidovenda", whereIn)
			.where("empresa.integracaowms = true")
			.list();
	}
	
	/**
	 * Busca os pedido de venda para enviar ao ECF
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/08/2014
	 */
	public List<Pedidovenda> findForEnvioECF(String whereIn) {	
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("pedidovendamaterial.cdpedidovendamaterial, pedidovendamaterial.preco, pedidovendamaterial.desconto, " +
					"pedidovendamaterial.quantidade, " +
					"pedidovenda.cdpedidovenda, pedidovenda.valorfrete, pedidovenda.taxapedidovenda, pedidovenda.desconto, pedidovenda.valorusadovalecompra, " +
					"material.cdmaterial, material.nome, material.vendaecf, " +
					"empresa.cdpessoa, empresa.integracaopdv, " +
					"unidademedida.simbolo, " +
					"cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj, " +
					"colaborador.cdpessoa, colaborador.nome, colaborador.codigoemporium, " +
					"tributacaoecf.cdtributacaoecf, tributacaoecf.codigoecf")
			.join("pedidovenda.cliente cliente")
			.join("pedidovenda.colaborador colaborador")
			.join("pedidovenda.empresa empresa")
			.join("pedidovenda.listaPedidovendamaterial pedidovendamaterial")
			.join("pedidovendamaterial.material material")
			.leftOuterJoin("material.tributacaoecf tributacaoecf")
			.leftOuterJoin("pedidovendamaterial.unidademedida unidademedida")
			.whereIn("pedidovenda.cdpedidovenda", whereIn)
			.list();
	}
	
	/**
	 * M�todo que busca todos os pedidos de venda n�o confirmados de determinado cliente
	 * @param cliente
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Pedidovenda> findAllNaoConfirmadosByClienteOrderbyData(Cliente cliente, Boolean isRestricaoClienteVendedor, Boolean isRestricaoVendaVendedor, Empresa empresa){
		if(cliente == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		String whereInEmpresas = empresa != null? empresa.getCdpessoa().toString():
												CollectionsUtil.listAndConcatenate(empresaService.findByUsuario(), "cdpessoa", ",");
		
		QueryBuilder<Pedidovenda> query = query()
				.select("pedidovenda.cdpedidovenda, pedidovenda.dtpedidovenda, pedidovendatipo.cdpedidovendatipo, pedidovendatipo.descricao, " +
						"material.nome, listaPedidovendamaterial.preco, listaPedidovendamaterial.desconto, listaPedidovendamaterial.quantidade")
				.join("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
				.join("pedidovenda.pedidovendatipo pedidovendatipo")
				.join("listaPedidovendamaterial.material material")
				.join("pedidovenda.cliente cliente")
				.join("pedidovenda.empresa empresa")
				.leftOuterJoin("pedidovenda.colaborador colaborador")
				.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
				.leftOuterJoin("listaClientevendedor.colaborador colaboradorcliente")
				.where("cliente = ?", cliente)
				.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.CONFIRMADO)
				.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.CANCELADO)
				.whereIn("empresa.cdpessoa", whereInEmpresas)
				;

		if((isRestricaoClienteVendedor != null && isRestricaoClienteVendedor) || 
				(isRestricaoVendaVendedor != null && isRestricaoVendaVendedor) ){ 
			Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
			if(colaborador == null) {
				query.where("1=0");
			}else {
				if(!isRestricaoClienteVendedor){
					query.where("colaborador = ?", colaborador);					
				}else {
					query.openParentheses()
						.where("colaboradorcliente = ?", colaborador)
						.or()
						.where("colaborador = ?", colaborador)
					.closeParentheses();
				}
			}
		}
		
		return query
				.orderBy("pedidovenda.dtpedidovenda DESC")
				.list();
	}

	/**
	 * M�todo que carrega a venda com o cliente
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Pedidovenda loadWithCliente(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Venda n�o pode ser nulo.");
		
		return query()
				.select("pedidovenda.cdpedidovenda, cliente.cdpessoa, cliente.nome")
				.join("pedidovenda.cliente cliente")
				.where("pedidovenda  = ?", pedidovenda)
				.unique();
	}
	
	public Pedidovenda loadWithPedidovendatipo(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Venda n�o pode ser nulo.");
		
		return query()
				.select("pedidovenda.cdpedidovenda, pedidovenda.identificadorcarregamento "
						+ ", pedidovenda.pedidoEcommerceVisualizado"
						+ ", pedidovendatipo.cdpedidovendatipo, pedidovendatipo.reserva " 
						+ ", pedidovendatipo.baixaestoqueEnum, pedidovendatipo.geracaocontareceberEnum "
						+ ", pedidovendatipo.sincronizarComWMS, pedidovendatipo.confirmacaoManualWMS "
						+ ", pedidovendatipo.naopermitirenviarecf , pedidovendatipo.validarEstoquePedidoEnviarECF "
						+ ", pedidovendatipo.validarestoquepedidonaaprovacao, pedidovendatipo.permitirvendasemestoque "
						+ ", empresa.cdpessoa, localarmazenagem.cdlocalarmazenagem, projeto.cdprojeto"
						)
				.leftOuterJoin("pedidovenda.empresa empresa")
				.leftOuterJoin("pedidovenda.localarmazenagem localarmazenagem")
				.leftOuterJoin("pedidovenda.projeto projeto")
				.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
				.where("pedidovenda = ?", pedidovenda)
				.unique();
	}
	
	/**
	 * M�todo que retorna o �ltimo identificador do pedido de venda
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public String getUltimoidentificador(Empresa empresa) {
//		return newQueryBuilderSined(String.class)
//					.from(Pedidovenda.class)
//					.setUseTranslator(false) 
//					.select("max(CAST(pedidovenda.identificador) AS integer)")
//					.join("pedidovenda.empresa empresa")
//					.where("empresa = ?", empresa)
//					.where("pedidovenda.identificador is not null")
//					.where("pedidovenda.identificador <> ''")
//					.unique(); 
		
		StringBuilder sql = new StringBuilder();
		sql.append(" select max(CAST(p.identificador AS INTEGER)) as ultimoidentificador ");
		sql.append(" from Pedidovenda p ");
		sql.append(" where p.identificador is not null and p.identificador <> '' ");
		sql.append(" and p.pedidovendasituacao <> " + Pedidovendasituacao.CANCELADO.getValue());
		if(empresa != null && empresa.getCdpessoa() != null){
			sql.append(" and p.cdempresa = " + empresa.getCdpessoa() );
		}
		
		Integer ultimoidentificador = (Integer)getJdbcTemplate().queryForObject(sql.toString(),new RowMapper() {
			public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Integer(rs.getInt("ultimoidentificador"));
			}
		});
		
		return ultimoidentificador != null ? ultimoidentificador.toString() : null;
	}

	/**
	 * M�todo que busca informa��es dos colaboradores/comissao para o comissionamento do pedido de venda (tipo reserva)
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Pedidovenda findForComissiaoPedidovendaTipoReserva(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("pedidovenda.cdpedidovenda, pedidovenda.valorusadovalecompra, listaPedidovendamaterial.cdpedidovendamaterial, material.cdmaterial, cliente.cdpessoa, pedidovenda.percentualdesconto, pedidovenda.desconto, empresa.cdpessoa, " +
						"colaborador.cdpessoa, colaborador.nome, listaPedidovendamaterial.preco, listaPedidovendamaterial.desconto, listaPedidovendamaterial.quantidade, prazopagamento.cdprazopagamento, prazopagamento.juros, " +
						"listaPedidovendamaterial.valorvendamaterial, pedidovenda.desconto, listaPedidovendamaterial.multiplicador, listaPedidovendamaterial.percentualdesconto, pedidovendatipo.cdpedidovendatipo, pedidovendatipo.representacao, " +
						"fornecedor.cdpessoa, comissionamento.cdcomissionamento, comissionamento.considerardiferencapreco, clienteindicacao.cdpessoa, " +
						"agencia.cdpessoa, listaPedidovendamaterial.percentualcomissaoagencia, comissionamento.cdcomissionamento, comissionamento.considerardiferencapreco ")
				.leftOuterJoin("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
				.leftOuterJoin("pedidovenda.prazopagamento prazopagamento")
				.leftOuterJoin("pedidovenda.colaborador colaborador")
				.leftOuterJoin("pedidovenda.cliente cliente")
				.leftOuterJoin("pedidovenda.empresa empresa")
				.join("listaPedidovendamaterial.material material")	
				.join("pedidovenda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("listaPedidovendamaterial.fornecedor fornecedor")
				.leftOuterJoin("listaPedidovendamaterial.comissionamento comissionamento")
				.leftOuterJoin("pedidovenda.agencia agencia")
				.leftOuterJoin("pedidovenda.clienteindicacao clienteindicacao")
				.where("pedidovenda  = ?", pedidovenda)	
//				.where("pedidovendatipo.reserva = true")
				.unique();
	}

	public List<Pedidovenda> findByCliente(Cliente cliente) {
		if(cliente == null || cliente.getCdpessoa() == null){
			throw new SinedException("Par�metro inv�lido!");
		}
		return query()
					.select("pedidovenda.cdpedidovenda, pedidovenda.dtpedidovenda, pedidovenda.pedidovendasituacao")
					.join("pedidovenda.cliente cliente")
					.where("cliente = ?", cliente)
					.orderBy("pedidovenda.cdpedidovenda")
					.list();
	}

	/**
	 * M�todo que busca os dados do pedido de venda para o relat�rio de Custo x Venda
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovenda> findForEmitircustopedidovenda(EmitircustovendaFiltro filtro) {
		if(filtro == null)
			throw new SinedException("Par�metro inv�lido!");
		
		QueryBuilderSined<Pedidovenda> query = querySined();
		query
		.select("pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao, pedidovenda.dtpedidovenda, pedidovenda.desconto, pedidovenda.valorusadovalecompra, colaborador.nome, cliente.cdpessoa, cliente.nome, cliente.cnpj, cliente.cpf, " +
				"listaPedidovendamaterial.cdpedidovendamaterial, listaPedidovendamaterial.quantidade, listaPedidovendamaterial.preco, listaPedidovendamaterial.desconto, " +
				"unidademedida.cdunidademedida, unidademedida.simbolo, unidademedida.nome, unidademedida.casasdecimaisestoque, material.cdmaterial, material.nome, material.referencia, " +
				"material.metrocubicovalorvenda, material.pesoliquidovalorvenda, " +
				"materialgrupo.cdmaterialgrupo, materialgrupo.nome, material.valorcusto, material.valorvenda, " +
				"listaPedidovendamaterial.desconto, listaPedidovendamaterial.observacao," +
				"unidademedidavenda.cdunidademedida, unidademedidavenda.nome, unidademedidavenda.simbolo, unidademedidavenda.casasdecimaisestoque, " +
				"listaPedidovendamaterial.fatorconversao, listaPedidovendamaterial.qtdereferencia, " +
				"listaPedidovendamaterial.valorcustomaterial, listaPedidovendamaterial.valorvendamaterial, listaPedidovendamaterial.multiplicador, " +
					"pedidovendatipo.bonificacao ")
		.join("pedidovenda.empresa empresa")
		.join("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
		.join("listaPedidovendamaterial.material material")
		.join("material.unidademedida unidademedida")
		.leftOuterJoin("material.materialgrupo materialgrupo")
		.leftOuterJoin("pedidovenda.cliente cliente")
		.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
		.leftOuterJoin("pedidovenda.colaborador colaborador")
		.leftOuterJoin("listaPedidovendamaterial.unidademedida unidademedidavenda")
		.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo");
		
		if (StringUtils.isNotBlank(filtro.getWhereInPedidovenda())) {
			query
			.whereIn("pedidovenda.cdpedidovenda", filtro.getWhereInPedidovenda());
		}else {
			if (SinedUtil.isListNotEmpty(filtro.getListapedidovendatipo())) {
				query
				.whereIn("pedidovenda.pedidovendatipo", SinedUtil.listAndConcatenate(filtro.getListapedidovendatipo(), "cdpedidovendatipo", ","));
			} else {
				query
				.openParentheses()
		 			.where("pedidovendatipo.bonificacao <> true")
		 			.or()
					.where("pedidovendatipo is null")
		 		.closeParentheses();
			}
			if (SinedUtil.isListNotEmpty(filtro.getListapedidovendasituacao())) {
				query
				.whereIn("pedidovenda.pedidovendasituacao", Pedidovendasituacao.listAndConcatenate(filtro.getListapedidovendasituacao()));
			} 
			query
			.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.CANCELADO)
			.where("pedidovenda.dtpedidovenda >= ?", filtro.getDtinicio())
			.where("pedidovenda.dtpedidovenda <= ?", filtro.getDtfim())
			.where("cliente = ?", filtro.getCliente())
			.where("pedidovenda.empresa = ?", filtro.getEmpresa());
			
			if (filtro.getColaborador() != null && filtro.getIsVendedorPrincipal() != null && filtro.getIsVendedorPrincipal()) {
				query
					.openParentheses()
						.where("listaClientevendedor.colaborador = ?", filtro.getColaborador())
						.where("listaClientevendedor.principal = ?", Boolean.TRUE)
					.closeParentheses();
			} else {
				query.where("pedidovenda.colaborador = ?", filtro.getColaborador());
			}
		}
		
		adicionaJoinWhereForMaterial(filtro, query);
		
		return query.list();
	}
	
	protected void adicionaJoinWhereForMaterial(EmitircustovendaFiltro filtro, QueryBuilder<Pedidovenda> query) {
		if(filtro.getMaterial() != null || 
				(filtro.getProduto() != null && filtro.getProduto()) ||
				(filtro.getServico() != null && filtro.getServico()) ||
				(filtro.getEpi() != null && filtro.getEpi()) ||
				(filtro.getPatrimonio() != null && filtro.getPatrimonio()) ||
				filtro.getMaterialcategoria() != null){
			
			StringBuilder selectForMaterial = new StringBuilder();
			String whereMaterialgrupo = "";
			String whereMaterialtipo = "";
			String whereMaterial = "";
			String whereFornecedor = "";
			String whereClasseMaterial = "";
			String whereMaterialcategoria = "";
			
			if(filtro.getMaterialcategoria()!=null){
				selectForMaterial.append(",materialcategoria.cdmaterialcategoria ");
				query.leftOuterJoinIfNotExists("material.materialcategoria materialcategoria");
				whereMaterialcategoria += " AND m.materialcategoria.cdmaterialcategoria = " + filtro.getMaterialcategoria().getCdmaterialcategoria() + " ";
			}
			if(filtro.getMaterial()!=null){
				whereMaterial += " AND m.cdmaterial = " + filtro.getMaterial().getCdmaterial() + " ";
			}
			if (filtro.getProduto() || filtro.getServico() || filtro.getEpi() || filtro.getPatrimonio()) {
				selectForMaterial.append(",material.produto, material.servico, material.epi, material.patrimonio ");
				whereClasseMaterial = " AND ";
				whereClasseMaterial += " ( ";
				if (filtro.getProduto()) {
					whereClasseMaterial += " m.produto = " + filtro.getProduto() + " ";
				}
				if (filtro.getServico()) {
					if (filtro.getProduto()) {
						whereClasseMaterial += " OR ";
					}
					whereClasseMaterial += " m.servico = " + filtro.getServico() + " ";
				}
				if (filtro.getEpi()) {
					if (filtro.getProduto() || filtro.getServico()) {
						whereClasseMaterial += " OR ";
					}
					whereClasseMaterial += " m.epi = " + filtro.getEpi() + " ";
				}
				if (filtro.getPatrimonio()) {
					if (filtro.getProduto() || filtro.getServico() || filtro.getEpi()) {
						whereClasseMaterial += " OR ";
					}
					whereClasseMaterial += " m.patrimonio = " + filtro.getPatrimonio() + " ";
				}
				whereClasseMaterial += " ) ";
			}
			
			query.select(query.getSelect().getValue() + selectForMaterial.toString());
			query.where(" exists (	  select pvm.pedidovenda.cdpedidovenda " +
								 	" from Pedidovendamaterial pvm " +
								 	" join pvm.material m " +
								 	" where pvm.pedidovenda.cdpedidovenda = pedidovenda.cdpedidovenda " +
								 	whereMaterialgrupo + 
								 	whereMaterialtipo +
								 	whereMaterialcategoria +
								 	whereMaterial + 
								 	whereFornecedor +
								 	whereClasseMaterial + " ) ");
		}
	}

	/**
	 * M�todo que busca os pedidos com o cliente e os contatos
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovenda> findForEnviocomprovanteemail(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("pedidovenda.cdpedidovenda, cliente.cdpessoa, cliente.nome, cliente.email, contato.cdpessoa, contato.nome, contato.emailcontato, "+
						"empresa.cdpessoa, empresa.razaosocial, empresa.nomefantasia, colaborador.cdpessoa, colaborador.nome, colaborador.email")
				.join("pedidovenda.cliente cliente")
				.join("pedidovenda.empresa empresa")
				.join("pedidovenda.colaborador colaborador")
				.leftOuterJoin("cliente.listaContato listaContato")
				.leftOuterJoin("listaContato.contato contato")
				.whereIn("pedidovenda.cdpedidovenda", whereIn)
				.list();
	}

	/**
	 * M�todo que busca os pedidos de venda para atualizar o saldo de produtos 
	 * 
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovenda> findForAtualizarsaldo(String whereIn) {
		return query()
				.select("pedidovenda.cdpedidovenda, pedidovendamaterial.cdpedidovendamaterial, pedidovendamaterial.preco, pedidovendamaterial.multiplicador, " +
						"pedidovendamaterial.desconto, pedidovendamaterial.quantidade, material.cdmaterial, " +
						"material.valorvendaminimo, material.valorvendamaximo, material.valorvenda, cliente.cdpessoa, prazopagamento.cdprazopagamento," +
						"pedidovendatipo.cdpedidovendatipo, unidademedida.cdunidademedida ")
				.join("pedidovenda.listaPedidovendamaterial pedidovendamaterial")
				.join("pedidovendamaterial.material material")
				.leftOuterJoin("pedidovendamaterial.unidademedida unidademedida")
				.leftOuterJoin("pedidovenda.cliente cliente")
				.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("pedidovenda.prazopagamento prazopagamento")
				.whereIn("pedidovenda.cdpedidovenda", whereIn)
				.list();
	}

	/**
	 * M�todo que faz o update do saldo final do pedido de venda
	 *
	 * @param pedidovenda
	 * @param saldofinal
	 * @author Luiz Fernando
	 */
	public void updateSaldofinalByPedidovenda(Pedidovenda pedidovenda, Money saldofinal) {
		if(pedidovenda != null && pedidovenda.getCdpedidovenda() != null && saldofinal != null){
			getHibernateTemplate()
			.bulkUpdate("update Pedidovenda p set p.saldofinal = ? where p.id = " + pedidovenda.getCdpedidovenda() + " ", 
					new Object[]{saldofinal});
		}		 
	}
	
	public void updateSituacaoPendenciaNegociacao(Pedidovenda pedidovenda, SituacaoPendencia situacaoPendencia) {
		getHibernateTemplate().bulkUpdate("update Pedidovenda p set p.situacaoPendencia = ? where p.id = " + pedidovenda.getCdpedidovenda() + " ", new Object[]{situacaoPendencia});
	}
	
	/**
	 * M�todo que verifica se existe uma venda para o cliente e o grupo de material
	 *
	 * @param cliente
	 * @param materialgrupo
	 * @param empresa
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existPedidovendaByClienteMaterialgrupo(Cliente cliente, Materialgrupo materialgrupo, Empresa empresa, Pedidovenda pedidovenda) {
		if(cliente == null)
			throw new SinedException("Cliente n�o pode ser nulo");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Pedidovenda.class)
			.join("pedidovenda.cliente cliente")
			.leftOuterJoin("pedidovenda.empresa empresa")
			.leftOuterJoin("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
			.leftOuterJoin("listaPedidovendamaterial.material material")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
			.where("pedidovenda <> ?", (pedidovenda != null && pedidovenda.getCdpedidovenda() != null ? pedidovenda : null))
			.where("cliente = ?", cliente)
			.where("materialgrupo = ?", materialgrupo)
			.where("empresa = ?", empresa)
			.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.CANCELADO)
			.where("pedidovendatipo.reserva = true")
			.unique() > 0;
	}
	
	/**
	 * M�todo que carrega os pedido de venda para combo de pedido de venda origem
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovenda> findForAutocompletePedidovendacomodato(String q, Integer cdpedidovenda){
		return query()
			.select("pedidovenda.cdpedidovenda, cliente.cdpessoa, cliente.nome")
			.join("pedidovenda.cliente cliente")
			.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
			.where("pedidovenda.cdpedidovenda <> ?", cdpedidovenda)
			.openParentheses()
				.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.PREVISTA)
				.or()
				.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.AGUARDANDO_APROVACAO)
				.or()
				.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.CONFIRMADO)
				.or()
				.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.CONFIRMADO_PARCIALMENTE)
			.closeParentheses()
			.openParentheses()
				.whereLikeIgnoreAll("cliente.nome", q)
				.or()
				.whereLikeIgnoreAll("cliente.razaosocial", q)
				.or()
				.whereLikeIgnoreAll("pedidovenda.cdpedidovenda", q)
			.closeParentheses()

			.orderBy("pedidovenda.cdpedidovenda desc")
			.list();
	}
	
	/**
	 * M�todo que verifica se os materiais do pedido tem reservas
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isMaterialReservadoOutrosPedidos(Pedidovenda pedidovenda, Boolean verificarQtdeDisponivel){
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Pedidovenda.class)
			.join("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
			.join("listaPedidovendamaterial.material material")
			.join("pedidovenda.pedidovendatipo pedidovendatipo")
			.join("pedidovenda.empresa empresa")
			.leftOuterJoin("pedidovenda.localarmazenagem localarmazenagem")
			.leftOuterJoin("listaPedidovendamaterial.loteestoque loteestoque")
			.where("pedidovenda <> ?", pedidovenda)
			.where("exists  (Select pv.cdpedidovenda " +
					"From Pedidovendamaterial pvm " +
					"join pvm.material m " +
					"join pvm.pedidovenda pv " +
					"join pv.empresa e " +
					"left outer join pvm.loteestoque le " +
					"left outer join pv.localarmazenagem la " +
					"where m.cdmaterial = material.cdmaterial and e.cdpessoa = empresa.cdpessoa " +
					"and ((localarmazenagem.cdlocalarmazenagem is null and la.cdlocalarmazenagem is null) or (localarmazenagem.cdlocalarmazenagem = la.cdlocalarmazenagem) ) " +
					"and ((loteestoque.cdloteestoque is null and le.cdloteestoque is null) or (loteestoque.cdloteestoque = le.cdloteestoque) ) " +
					"and pv.cdpedidovenda = " + pedidovenda.getCdpedidovenda() + ")")
			.openParentheses()
				.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.PREVISTA)
				.or()
				.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.AGUARDANDO_APROVACAO)
			.closeParentheses()
			.where("pedidovendatipo.reserva = true")
			.openParentheses()
				.where("coalesce(material.produto, false) = true")
				.or()
				.where("coalesce(material.epi, false) = true")
			.closeParentheses();
		
//		if(verificarQtdeDisponivel != null && verificarQtdeDisponivel){
//			QUERY EST� DEMORANDO MUITO TEMPO PARA EXECUTAR
//			query
//				.where("coalesce(qtdereservada(material.cdmaterial, localarmazenagem.cdlocalarmazenagem, empresa.cdpessoa, loteestoque.cdloteestoque), 0) > " +
//						"coalesce(material_disponivel_local(material.cdmaterial, " +
//															"CASE WHEN coalesce(material.epi, false) = true THEN 'EP' ELSE 'PO' END, " +
//															"localarmazenagem.cdlocalarmazenagem, empresa.cdpessoa, null, false, loteestoque.cdloteestoque), 0) ");
//		}
		
		return query.unique() > 0;
	}
	
	/**
	 * M�todo que retorna os pedidos que tenham material em reserva
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovenda> findReservasMateriais(String whereIn){
		return query()
				.select("pedidovenda.cdpedidovenda, listaPedidovendamaterial.cdpedidovendamaterial, material.nome, cliente.nome, " +
						"listaPedidovendamaterial.quantidade, listaPedidovendamaterial.preco ")
				.join("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
				.join("listaPedidovendamaterial.material material")
				.join("pedidovenda.pedidovendatipo pedidovendatipo")
				.join("pedidovenda.cliente cliente")
				.join("pedidovenda.empresa empresa")
				.leftOuterJoin("pedidovenda.localarmazenagem localarmazenagem")
				.leftOuterJoin("listaPedidovendamaterial.loteestoque loteestoque")
				.where("pedidovenda.cdpedidovenda not in (" + whereIn + ")")
				.where("exists  (Select pv.cdpedidovenda " +
						"From Pedidovendamaterial pvm " +
						"join pvm.material m " +
						"join pvm.pedidovenda pv " +
						"join pv.empresa e " +
						"left outer join pvm.loteestoque le " +
						"left outer join pv.localarmazenagem la " +
						"where m.cdmaterial = material.cdmaterial and e.cdpessoa = empresa.cdpessoa " +
						"and ((localarmazenagem.cdlocalarmazenagem is null and la.cdlocalarmazenagem is null) or (localarmazenagem.cdlocalarmazenagem = la.cdlocalarmazenagem) ) " +
						"and ((loteestoque.cdloteestoque is null and le.cdloteestoque is null) or (loteestoque.cdloteestoque = le.cdloteestoque) ) " +
						"and pvm.pedidovenda.cdpedidovenda in (" + whereIn + ") ) ")
				.openParentheses()
					.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.PREVISTA)
					.or()
					.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.AGUARDANDO_APROVACAO)
				.closeParentheses()
				.where("pedidovendatipo.reserva = true")
				.openParentheses()
					.where("coalesce(material.produto, false) = true")
					.or()
					.where("coalesce(material.epi, false) = true")
				.closeParentheses()
//				QUERY EST� DEMORANDO MUITO TEMPO PARA EXECUTAR
//				.where("coalesce(qtdereservada(material.cdmaterial, localarmazenagem.cdlocalarmazenagem, empresa.cdpessoa, loteestoque.cdloteestoque), 0) > " +
//						"coalesce(material_disponivel_local(material.cdmaterial, " +
//															"CASE WHEN coalesce(material.epi, false) = true THEN 'PO' ELSE 'PO' END, " +
//															"localarmazenagem.cdlocalarmazenagem, empresa.cdpessoa, null, false, loteestoque.cdloteestoque), 0) ")
				.orderBy("pedidovenda.dtpedidovenda")
				.list();
	}

	public List<Pedidovenda> findByIdentificador(String identificador) {
		if(identificador == null || identificador.equals("")){
			return null;
		}
		return query()
				.select("pedidovenda.cdpedidovenda, pedidovenda.dtpedidovenda")
				.where("pedidovenda.identificador = ?", identificador)
				.list();
	}
	
	/**
	* M�todo que retorna os pedidos de venda n�o cancelados de acordo com o identificador e cnpj da empresa
	*
	* @param identificador
	* @param cnpj
	* @return
	* @since 01/06/2015
	* @author Luiz Fernando
	*/
	public List<Pedidovenda> findByIdentificadorCnpj(String identificador, Cnpj cnpj) {
		if(identificador == null || identificador.equals("")){
			return null;
		}
		return query()
				.select("pedidovenda.cdpedidovenda, pedidovenda.dtpedidovenda, pedidovenda.pedidovendasituacao")
				.leftOuterJoin("pedidovenda.empresa empresa")
				.where("pedidovenda.identificador = ?", identificador)
				.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.CANCELADO)
				.where("empresa.cnpj = ?", cnpj)
				.list();
	}
	
	/**
	 * 
	 * @author Thiago Clemente
	 * 
	 */
	public void cancelamentoDiario(){
		final List<Pedidovenda> listapedidovendacancelamento = this.findForCancelamentoDiario();
		List<Documento> listaContaReceber = null;
		
		if(listapedidovendacancelamento != null && !listapedidovendacancelamento.isEmpty()){
			String whereIn = CollectionsUtil.listAndConcatenate(listapedidovendacancelamento, "cdpedidovenda", ",");
			
			getTransactionTemplate().execute(new TransactionCallback(){
				public Object doInTransaction(TransactionStatus status) {
					for (Pedidovenda pedido : listapedidovendacancelamento) {
						Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
						pedidovendahistorico.setPedidovenda(pedido);
						pedidovendahistorico.setAcao("Cancelado");
						pedidovendahistorico.setObservacao("CANCELAMENTO AUTOM�TICO CONFORME PAR�METRO DE CONFIGURA��O");
						PedidovendahistoricoService.getInstance().saveOrUpdate(pedidovendahistorico);
					}	
				return null;
				}	
			});
			
			if(whereIn != null && !whereIn.equals("")){
				listaContaReceber = ContareceberService.getInstance().findByPedidovenda(whereIn);
					getJdbcTemplate().update("update pedidovenda pv set pedidovendasituacao = ? " +
										"where pv.cdpedidovenda in (" + whereIn + ") ", 
										new Object[]{Pedidovendasituacao.CANCELADO.ordinal()});
					
					ReservaService.getInstance().deleteReservaByPedidovenda(whereIn);
					PedidovendaService.getInstance().executaCancelamentoReservaOSV(whereIn);
					
					if(SinedUtil.isListNotEmpty(listaContaReceber)){			
						ContareceberService.getInstance().cancelaDocumentosVinculadoPedidoVendaCancelado(listaContaReceber);
					}
				
			}
		}
	}
	
	public List<Pedidovenda> findForCancelamentoDiario(){
		String whereInDocumentoacaoQueImpedeCancelamento = Documentoacao.BAIXADA.getCddocumentoacao()+", "+Documentoacao.BAIXADA_PARCIAL.getCddocumentoacao()+", "+Documentoacao.NEGOCIADA.getCddocumentoacao();
		return query()
				.select("pedidovenda.cdpedidovenda")
				.join("pedidovenda.pedidovendatipo pedidovendatipo")
				.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.PREVISTA)
				.where("pedidovendatipo.diascancelamento is not null")
				.where("pedidovenda.dtpedidovenda + pedidovendatipo.diascancelamento <= ?", SinedDateUtils.currentDate())
				.where("pedidovendatipo.reserva = ?", Boolean.TRUE)
				.where("exists(select 1 from Documentoorigem do join do.documento doc join doc.documentoacao docAcao where do.pedidovenda = pedidovenda and docAcao.cddocumentoacao not in ("+whereInDocumentoacaoQueImpedeCancelamento+"))")
				.list();
	}

	public List<Pedidovenda> findForReportFluxocaixa(FluxocaixaFiltroReport filtro, Boolean considerarApenasAnteriores) {
		// FILTRO DE CONSIDERAR PEDIDO DE VENDA
		if(filtro.getConsPedidovenda() == null || !filtro.getConsPedidovenda()){
			return new ArrayList<Pedidovenda>();
		}
		
		List<ItemDetalhe> listaProjeto = filtro.getListaProjeto();
		List<ItemDetalhe> listaEmpresa = filtro.getListaEmpresa();
		List<ItemDetalhe> listaCliente = filtro.getListaCliente();
		
		QueryBuilderSined<Pedidovenda> query = querySined();
					query
					.select("pedidovenda.cdpedidovenda, cliente.cdpessoa, cliente.nome, pedidovenda.dtpedidovenda, " +
							"pedidovendamaterial.quantidade, pedidovendamaterial.multiplicador, pedidovendamaterial.preco, pedidovendamaterial.desconto, " +
							"pedidovenda.valorfrete, pedidovenda.taxapedidovenda, pedidovenda.desconto," +
							"listaPedidovendapagamento.valororiginal, listaPedidovendapagamento.dataparcela")
					.join("pedidovenda.listaPedidovendamaterial pedidovendamaterial")
					.join("pedidovenda.cliente cliente")
					.join("pedidovenda.empresa empresa")
					.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
					.leftOuterJoin("pedidovenda.projeto projeto")
					.join("pedidovenda.listaPedidovendapagamento listaPedidovendapagamento")
					.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.PREVISTA)
					.openParentheses()
						.where("pedidovendatipo is null")
						.or()
						.where("pedidovendatipo.bonificacao is null")
						.or()
						.where("pedidovendatipo.bonificacao <> true")
					.closeParentheses()
					;

		if(filtro.getMostrarContasInativadas() == null || !filtro.getMostrarContasInativadas()){
			query.leftOuterJoin("pedidovenda.conta conta");
			query.leftOuterJoin("conta.contatipo contatipo");
			query.openParentheses();
				query.where("conta is null");
				query.or();
				query.openParentheses();
					query.openParentheses();
						query.where("contatipo = ?", Contatipo.TIPO_CONTA_BANCARIA);
						query.where("conta.ativo = true");
					query.closeParentheses();
					query.or();
					query.openParentheses();
						query.where("contatipo is null");
						query.or();
						query.where("contatipo <> ?", Contatipo.TIPO_CONTA_BANCARIA);
					query.closeParentheses();
				query.closeParentheses();
			query.closeParentheses();
		}
		
		// WHERE DE PER�ODO
		if(considerarApenasAnteriores != null && considerarApenasAnteriores){
			query.where("listaPedidovendapagamento.dataparcela < ?", filtro.getPeriodoDe());
		}else {
			query
				.where("listaPedidovendapagamento.dataparcela >= ?", filtro.getPeriodoDe())
				.where("listaPedidovendapagamento.dataparcela <= ?", filtro.getPeriodoAte());
		}
		
		// WHERE DE PROJETO
		String idProjeto = String.valueOf(filtro.getRadProjeto().getId());
		if(idProjeto.equals("escolherProjeto") && SinedUtil.isListNotEmpty(listaProjeto)){
			query.whereIn("projeto.cdprojeto", CollectionsUtil.listAndConcatenate(listaProjeto, "projeto.cdprojeto", ","));
		}else if(idProjeto.equals("comProjeto")){
			query.where("projeto is not null");
		}else if(filtro.getRadProjeto().getId().equals("semProjeto")){
			query.where("projeto is null");
		}
		
		// WHEREIN LISTA DE EMPRESAS
		if(listaEmpresa != null && !listaEmpresa.isEmpty()){
			query.whereIn("empresa.cdpessoa",CollectionsUtil.listAndConcatenate(listaEmpresa, "empresa.cdpessoa", ","));
		} else {
			query.openParentheses();
			query.whereIn("empresa.cdpessoa",new SinedUtil().getListaEmpresa());
			query.or();
			query.where("empresa is null");
			query.closeParentheses();
		}
					
		
		// WHEREIN NA LISTA DE CLIENTE			
		if(listaCliente != null && listaCliente.size() > 0){
			query.whereIn("cliente.cdpessoa",CollectionsUtil.listAndConcatenate(listaCliente, "cliente.cdpessoa", ","));
		}
		
		return query.list();
	}
	
	/**
	 * M�todo que busca os pedidos do or�amento
	 *
	 * @param vendaorcamento
	 * @return
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public List<Pedidovenda> findByOrcamento(Vendaorcamento vendaorcamento) {
		if(vendaorcamento == null || vendaorcamento.getCdvendaorcamento() == null)
			throw new SinedException("Or�amento n�o pode ser nulo.");
		
		return query()
				.select("pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao, vendaorcamento.cdvendaorcamento ")
				.join("pedidovenda.vendaorcamento vendaorcamento")
				.where("vendaorcamento = ?", vendaorcamento)
				.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.CANCELADO)
				.list();
	}

	/**
	 * M�todo que carrega o pedido para troca de vendedor
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public Pedidovenda loadForTrocarvendedor(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Pedido de venda n�o pode ser nulo");
		
		return query()
				.select("pedidovenda.cdpedidovenda, colaborador.cdpessoa, colaborador.nome, cliente.cdpessoa, cliente.nome, " +
						"colaboradorcliente.cdpessoa, colaboradorcliente.nome ")
				.leftOuterJoin("pedidovenda.colaborador colaborador")
				.leftOuterJoin("pedidovenda.cliente cliente")
				.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
				.leftOuterJoin("listaClientevendedor.colaborador colaboradorcliente")
				.where("pedidovenda = ?", pedidovenda)
				.unique();
	}

	/**
	 * M�todo que atualiza o responsavel da pedido de venda
	 *
	 * @param pedidovenda
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public void updateColaboradorByPedidovenda(Pedidovenda pedidovenda) {
		if(pedidovenda != null && pedidovenda.getCdpedidovenda() != null && pedidovenda.getColaborador() != null && 
				pedidovenda.getColaborador().getCdpessoa() != null){
			getHibernateTemplate().bulkUpdate("update Pedidovenda p set p.colaborador = ? where p.id in (" + pedidovenda.getCdpedidovenda() + ")", 
					new Object[]{pedidovenda.getColaborador()});
		}		
	}

	/**
	 * Faz a busca para o relat�rio da listagem de pedido de venda.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 09/12/2013
	 */
	public List<Pedidovenda> findForRelatorioListagem(PedidovendaFiltro filtro) {
		QueryBuilder<Pedidovenda> query = query();
		this.updateListagemQuery(query, filtro);
		return query
					.orderBy("pedidovenda.cdpedidovenda desc")
					.list();
	}
	
	public List<Pedidovenda> findForRelatorioListagem(String whereIn) {
		QueryBuilder<Pedidovenda> query = query()
			.select("pedidovenda.cdpedidovenda, pedidovenda.dtpedidovenda, pedidovenda.observacao, pedidovenda.desconto, " +
					"pedidovenda.valorfrete, pedidovenda.taxapedidovenda, pedidovenda.saldofinal, pedidovenda.valorusadovalecompra, " +
					"listaPedidovendamaterial.valoripi, " +
					"empresa.cdpessoa, empresa.nome, empresa.razaosocial, cliente.cdpessoa, cliente.nome, " +
					"endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, endereco.cep," +
					"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla, " +
					"listaPedidovendamaterial.cdpedidovendamaterial, listaPedidovendamaterial.quantidade, listaPedidovendamaterial.preco, " +
					"listaPedidovendamaterial.observacao, listaPedidovendamaterial.dtprazoentrega, listaPedidovendamaterial.desconto, " +
					"listaPedidovendamaterial.multiplicador, material.cdmaterial, material.nome, "+
					"listaPedidovendamaterial.tipotributacaoicms, " +
					"listaPedidovendamaterial.tipocobrancaicms, listaPedidovendamaterial.valorbcicms, listaPedidovendamaterial.icms, " +
					"listaPedidovendamaterial.valoricms, listaPedidovendamaterial.valorbcicmsst, listaPedidovendamaterial.icmsst, " +
					"listaPedidovendamaterial.valoricmsst, listaPedidovendamaterial.reducaobcicmsst, listaPedidovendamaterial.margemvaloradicionalicmsst, " +
					"listaPedidovendamaterial.valorbcfcp, listaPedidovendamaterial.fcp, listaPedidovendamaterial.valorfcp, " +
					"listaPedidovendamaterial.valorbcfcpst, listaPedidovendamaterial.fcpst, listaPedidovendamaterial.valorfcpst, " +
					"listaPedidovendamaterial.valorbcdestinatario, listaPedidovendamaterial.valorbcfcpdestinatario, listaPedidovendamaterial.fcpdestinatario, " +
					"listaPedidovendamaterial.icmsdestinatario, listaPedidovendamaterial.icmsinterestadual, listaPedidovendamaterial.icmsinterestadualpartilha, " +
					"listaPedidovendamaterial.valorfcpdestinatario, listaPedidovendamaterial.valoricmsdestinatario, listaPedidovendamaterial.valoricmsremetente, " +
					"listaPedidovendamaterial.difal, listaPedidovendamaterial.valordifal, listaPedidovendamaterial.outrasdespesas, ncmcapitulo.cdncmcapitulo, cfop.cdcfop, " +
					"listaPedidovendamaterial.percentualdesoneracaoicms, listaPedidovendamaterial.valordesoneracaoicms, listaPedidovendamaterial.abaterdesoneracaoicms, " +
					"listaPedidovendamaterial.ncmcompleto, listaPedidovendamaterial.modalidadebcicms, listaPedidovendamaterial.modalidadebcicmsst, listaPedidovendamaterial.valorSeguro")
			.leftOuterJoin("pedidovenda.empresa empresa")
			.leftOuterJoin("pedidovenda.cliente cliente")
			.leftOuterJoin("pedidovenda.endereco endereco")
			.leftOuterJoin("endereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
			.leftOuterJoin("listaPedidovendamaterial.material material")
			.leftOuterJoin("listaPedidovendamaterial.cfop cfop")
			.leftOuterJoin("listaPedidovendamaterial.ncmcapitulo ncmcapitulo");
		SinedUtil.quebraWhereIn("pedidovenda.cdpedidovenda", whereIn, query);
		return query
			.orderBy("pedidovenda.cdpedidovenda desc")
			.list();
	}

	public List<Pedidovenda> findByClienteAndSituacao(Cliente cliente, Pedidovendasituacao situacao) {
		QueryBuilder<Pedidovenda> query = query();
		query
		  .select("pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao")
		  .where("pedidovenda.pedidovendasituacao = ?", situacao)
		  .where("pedidovenda.cliente = ?", cliente);
		return query.list();
	}
	/**
	 * Verifica se existe o pedido de venda da empresa com o identificador
	 *
	 * @param identificador
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/02/2014
	 */
	public boolean havePedidovendaByIdentificadorEmpresa(String identificador, Empresa empresa, boolean verificarCancelado) {
		return newQueryBuilderSinedWithFrom(Long.class)
				.setUseReadOnly(false)
				.select("count(*)")
				.where("pedidovenda.identificador = ?", identificador)
				.where("pedidovenda.empresa = ?", empresa)
				.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.CANCELADO, !verificarCancelado)
				.unique() > 0;
	}


	public Pedidovenda findPedidovendabyTipo(String cdpedidovenda){
		return query()
				.select("pedidovendatipo.reserva, producaoagendatipo.cdproducaoagendatipo, localarmazenagemcoleta.cdlocalarmazenagem")
				.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("pedidovendatipo.producaoagendatipo producaoagendatipo")
				.leftOuterJoin("pedidovendatipo.localarmazenagemcoleta localarmazenagemcoleta")
				.where("pedidovenda.cdpedidovenda = ?", Integer.parseInt(cdpedidovenda))
				.unique();
	}

	public List<Pedidovenda> findForOrdemcompra(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Pedidovenda> qry = query();
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("pedidovenda.cdpedidovenda, pedidovenda.dtpedidovenda, pedidovenda.observacao, pedidovenda.observacaointerna, pedidovenda.pedidovendasituacao,  ");
		sb.append("cliente.cdpessoa, cliente.nome, cliente.tipopessoa, cliente.cpf, cliente.cnpj, pedidovenda.saldofinal, ");
		sb.append("listaPedidovendamaterial.cdpedidovendamaterial, listaPedidovendamaterial.quantidade, listaPedidovendamaterial.preco, listaPedidovendamaterial.desconto, listaPedidovendamaterial.unidademedida, listaPedidovendamaterial.dtprazoentrega, ");
		sb.append("material.cdmaterial, material.nome, material.nome, material.codigofabricante, material.qtdeunidade, material.considerarvendamultiplos, ");
		sb.append("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, ");
		sb.append("endereco.cep, endereco.pontoreferencia, municipio.nome, uf.sigla, listaPedidovendamaterial.valorcustomaterial, material.valorcusto, material.valorvenda, listaPedidovendamaterial.valorvendamaterial, ");
		sb.append("materialgrupo.cdmaterialgrupo, materialgrupo.nome, material.produto, material.patrimonio, material.epi, material.servico, ");
		sb.append("material.valorvendaminimo, material.valorvendamaximo, listaPedidovendamaterial.fatorconversao, listaPedidovendamaterial.qtdereferencia, ");
		sb.append("empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.cdpessoa, pedidovenda.valorusadovalecompra, ");
		sb.append("colaborador.cdpessoa, colaborador.nome, loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, ");
		sb.append("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, listaPedidovendamaterial.observacao, ");
		sb.append("prazopagamento.cdprazopagamento, prazopagamento.juros, indicecorrecao.cdindicecorrecao, conta.cdconta, documentotipo.cddocumentotipo, documentotipo.antecipacao, projeto.cdprojeto, ");
		sb.append("contagerencial.cdcontagerencial, contagerencial.nome, unidademedida.cdunidademedida, unidademedida.simbolo, vcontagerencial.identificador, ");
		sb.append("pedidovendatipo.cdpedidovendatipo, pedidovendatipo.reserva,  pedidovendatipo.bonificacao, pedidovendatipo.comodato, ");
		sb.append("pedidovenda.frete, terceiro.cdpessoa, terceiro.nome, pedidovenda.valorfrete, pedidovenda.desconto, pedidovenda.taxapedidovenda, pedidovenda.prazomedio, ");
		sb.append("listaPedidovendamaterial.altura, listaPedidovendamaterial.largura, listaPedidovendamaterial.comprimento, listaPedidovendamaterial.comprimentooriginal, listaPedidovendamaterial.multiplicador ");
		
		qry.select(sb.toString())
			 .leftOuterJoin("pedidovenda.cliente cliente")
			 .leftOuterJoin("pedidovenda.prazopagamento prazopagamento")
			 .leftOuterJoin("pedidovenda.indicecorrecao indicecorrecao")
			 .leftOuterJoin("pedidovenda.projeto projeto")
			 .leftOuterJoin("pedidovenda.conta conta")
			 .leftOuterJoin("pedidovenda.documentotipo documentotipo")
			 .leftOuterJoin("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
			 .leftOuterJoin("listaPedidovendamaterial.material material")
			 .leftOuterJoin("listaPedidovendamaterial.loteestoque loteestoque")
			 .leftOuterJoin("pedidovenda.empresa empresa")
			 .leftOuterJoin("pedidovenda.endereco endereco")
			 .leftOuterJoin("pedidovenda.colaborador colaborador")
			 .leftOuterJoin("endereco.municipio municipio")
			 .leftOuterJoin("municipio.uf uf")
			 .leftOuterJoin("material.materialgrupo materialgrupo")
			 .leftOuterJoin("material.contagerencial contagerencial")
			 .leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			 .leftOuterJoin("listaPedidovendamaterial.unidademedida unidademedida")
			 .leftOuterJoin("pedidovenda.localarmazenagem localarmazenagem")
			 .leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
			 .leftOuterJoin("pedidovenda.terceiro terceiro")
			 .leftOuterJoin("pedidovenda.frete frete")
			 .whereIn("pedidovenda.cdpedidovenda", whereIn)
			 .orderBy("pedidovenda.cdpedidovenda, listaPedidovendamaterial.ordem");
		
		return qry.list();
	}
	
	public List<Pedidovenda> findForColetarProducao(String whereIn) {
		if(whereIn == null || whereIn.isEmpty()) 
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Pedidovenda> query = query()
				.select("pedidovenda.cdpedidovenda, pedidovenda.dtpedidovenda, " +
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
						"listaPedidovendamaterial.cdpedidovendamaterial, listaPedidovendamaterial.quantidade, listaPedidovendamaterial.identificadorintegracao, " +
						"listaPedidovendamaterial.observacao, material.cdmaterial, material.nome, material.identificacao, " +
						"materialcoleta.cdmaterial, materialcoleta.nome, materialcoleta.identificacao, materialcoleta.valorcusto, " +
						"unidademedidaPVM.cdunidademedida, unidademedidaPVM.nome, unidademedida.cdunidademedida, " +
						"unidademedida.nome, listaPedidovendamaterial.fatorconversao, listaPedidovendamaterial.qtdereferencia," +
						"unidademedidaColeta.cdunidademedida, unidademedidaColeta.nome, colaborador.cdpessoa, cliente.cdpessoa," +
						"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.obrigarinformarnotafiscalcoleta, " +
						"localarmazenagemcoleta.cdlocalarmazenagem, localarmazenagemcoleta.nome")
				.leftOuterJoin("pedidovenda.empresa empresa")
				.leftOuterJoin("pedidovenda.cliente cliente")
				.leftOuterJoin("pedidovenda.colaborador colaborador")
				.leftOuterJoin("pedidovenda.localarmazenagem localarmazenagem")
				.join("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
				.join("listaPedidovendamaterial.material material")
				.join("material.unidademedida unidademedida")
				.join("listaPedidovendamaterial.materialcoleta materialcoleta")
				.join("materialcoleta.unidademedida unidademedidaColeta")
				.leftOuterJoin("listaPedidovendamaterial.unidademedida unidademedidaPVM")
				.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("pedidovendatipo.localarmazenagemcoleta localarmazenagemcoleta")
			    .leftOuterJoin("listaPedidovendamaterial.pneu pneu")
				.whereIn("pedidovenda.cdpedidovenda", whereIn)
				.orderBy("pedidovenda.cdpedidovenda, listaPedidovendamaterial.ordem");
			boolean isOtr = ParametrogeralService.getInstance().getBoolean(Parametrogeral.HABILITAR_CONFIGURACAO_OTR);
			if(isOtr){
				query = query.openParentheses()
							.where("coalesce(pedidovenda.origemOtr, false) = false")
							.or()
								.openParentheses()
								.where("coalesce(pedidovenda.origemOtr, false) = true")
								.where("material.producao = true")
								.closeParentheses()
							.closeParentheses();
			}
			query = SinedUtil.setJoinsByComponentePneu(query);
			return query.list();
	}
	
	/**
	 * 
	 * @param cdpedidovenda
	 * @return
	 */
	public Pedidovenda findPedidovendaByCdpedidovenda(Integer cdpedidovenda){
		if (cdpedidovenda == null){
			throw new SinedException("Par�metros Inv�lidos.");
		}
		
		return query()
					.select("pedidovenda.cdpedidovenda, cliente.cdpessoa, cliente.nome, cliente.cnpj, cliente.cpf, pedidovenda.desconto, " +
							"listapedidovendamaterial.cdpedidovendamaterial, listapedidovendamaterial.quantidade, listapedidovendamaterial.preco, listapedidovendamaterial.desconto, " +
							"material.cdmaterial, material.nome, material.nome, material.codigofabricante, listapedidovendamaterial.qtdereferencia, listapedidovendamaterial.fatorconversao, " +
							"unidademedida.cdunidademedida, unidademedida.simbolo, unidademedida.nome, listapedidovendamaterial.observacao, listapedidovendamaterial.multiplicador, " +
							"unidademedidavenda.cdunidademedida, unidademedidavenda.simbolo, unidademedidavenda.nome," +
							"colaborador.cdpessoa, colaborador.nome, empresa.cdpessoa, projeto.cdprojeto ")
						.leftOuterJoin("pedidovenda.empresa empresa")
						.leftOuterJoin("pedidovenda.projeto projeto")
						.leftOuterJoin("pedidovenda.cliente cliente")
						.leftOuterJoin("pedidovenda.colaborador colaborador")
						.leftOuterJoin("pedidovenda.listaPedidovendamaterial listapedidovendamaterial")
						.leftOuterJoin("listapedidovendamaterial.material material")
						.leftOuterJoin("material.unidademedida unidademedida")
						.leftOuterJoin("listapedidovendamaterial.unidademedida unidademedidavenda")
						.where("pedidovenda.cdpedidovenda = ? ", cdpedidovenda)
						.orderBy("listapedidovendamaterial.ordem")
						.unique();
	}	
	
	/**
	 * 
	 * @param param
	 * @return
	 * @author Lucas Costa
	 */
	public  List<Pedidovenda> findPedidoVendaForAutocomplete(String param) {
		return query()
			.select("pedidovenda.cdpedidovenda, pedidovenda.identificador, cliente.nome")
			.join("pedidovenda.cliente cliente")
			.openParentheses()
				.whereLikeIgnoreAll("pedidovenda.cdpedidovenda",param)
				.or()
				.whereLikeIgnoreAll("cliente.nome",param)
			.closeParentheses()
			.list();
	}
	
	public List<Pedidovenda> findForGerarNotaByWhereIn(String whereIn) throws Exception{
		if(whereIn == null || whereIn.isEmpty()){
			throw new Exception("WhereIn n�o pode ser nulo.");
		}
		
		return query()
				.select("pedidovenda.cdpedidovenda, localarmazenagem.cdlocalarmazenagem, pedidovendamaterial.cdpedidovendamaterial, " +
						"pedidovendamaterial.quantidade, pedidovendamaterial.observacao, material.cdmaterial, material.valorcusto, material.nome, " +
						"unidademedida.cdunidademedida, cliente.cdpessoa, cliente.nome, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia," +
						"terceiro.cdpessoa, terceiro.nome, ncmcapitulo.cdncmcapitulo, material.ncmcompleto, ncmcapituloColeta.cdncmcapitulo, materialcoleta.ncmcompleto, " +
						"materialcoleta.cdmaterial, materialcoleta.nome, materialcoleta.identificacao, materialcoleta.valorcusto, unidademedidacoleta.cdunidademedida," +
						"pedidovendatipo.presencacompradornfecoleta, naturezaoperacaosaidafiscal.cdnaturezaoperacao, naturezaoperacaonotafiscalentrada.cdnaturezaoperacao," +
						"pneu.cdpneu")
				.leftOuterJoin("pedidovenda.cliente cliente")
				.leftOuterJoin("pedidovenda.empresa empresa")
				.leftOuterJoin("pedidovenda.terceiro terceiro")
				.leftOuterJoin("pedidovenda.localarmazenagem localarmazenagem")
				.leftOuterJoin("pedidovenda.listaPedidovendamaterial pedidovendamaterial")
				.leftOuterJoin("pedidovendamaterial.pneu pneu")
				.leftOuterJoin("pedidovendamaterial.material material")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("pedidovendamaterial.materialcoleta materialcoleta")
				.leftOuterJoin("materialcoleta.unidademedida unidademedidacoleta")
				.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
				.leftOuterJoin("materialcoleta.ncmcapitulo ncmcapituloColeta")
				.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("pedidovendatipo.naturezaoperacaosaidafiscal naturezaoperacaosaidafiscal")
				.leftOuterJoin("pedidovendatipo.naturezaoperacaonotafiscalentrada naturezaoperacaonotafiscalentrada")
				.whereIn("pedidovenda.cdpedidovenda", whereIn)
				.list();
	}

	public boolean existeIdentificador(Empresa empresa, String identificador, Integer cdpedidovenda) {
		if(identificador == null || "".equals(identificador))
			return false;
		
		Long qtde = newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Pedidovenda.class)
				.join("pedidovenda.empresa empresa")
				.where("pedidovenda.cdpedidovenda <> ?", cdpedidovenda)
				.where("empresa = ?", empresa)
				.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.CANCELADO)
				.where("pedidovenda.identificador = ?", identificador)
				.unique();
		
		return qtde != null && qtde > 0;
	}
	
	/**
	* M�todo que verifica se existe material com controle de grade
	*
	* @param whereInPedidovenda
	* @return
	* @since 30/07/2014
	* @author Luiz Fernando
	*/
	public boolean existeGrade(String whereInPedidovenda){
		StringBuilder sql = new StringBuilder();
		sql.append("select count(*) as qtde ");
		sql.append("from Material m ");	
		sql.append("join materialgrupo mg on mg.cdmaterialgrupo = m.cdmaterialgrupo ");
		sql.append("where exists (select pvm.cdmaterial from pedidovendamaterial pvm where pvm.cdmaterial = m.cdmaterial and pvm.cdpedidovenda in (" + whereInPedidovenda + ") ) ");
		sql.append("and mg.gradeestoquetipo is not null ");
		
		Long qtde = (Long)getJdbcTemplate().queryForObject(sql.toString(), 
				new RowMapper() {
					public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
						return rs.getLong("qtde");
					}
				});
		
		return qtde != null && qtde > 0;
	}

	/**
	 * Busca os pedidos relacionados para a atualiza��o da situa��o
	 *
	 * @param whereInColeta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 22/08/2014
	 */
	public List<Pedidovenda> findByColeta(String whereInColeta) {
		return query()
					.select("pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao, pedidovendamaterial.cdpedidovendamaterial")
					.join("pedidovenda.listaColeta coleta")
					.join("pedidovenda.listaPedidovendamaterial pedidovendamaterial")
					.whereIn("coleta.cdcoleta", whereInColeta)
					.list();
	}
	

	/**
	 * M�todo que atualiza o campo 'integrar' de um pedido venda
	 * 
	 * @param pedidovenda
	 * @param integrar
	 * @throws Exception 
	 * @author Rafael Salvio
	 */
	public void updateIntegrar(Pedidovenda pedidovenda, boolean integrar) throws Exception{
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null){
			throw new Exception("Par�metro inv�lido para a opera��o.");
		}
		
		getJdbcTemplate().update("update pedidovenda set integrar = ? where cdpedidovenda = ?", new Object[]{integrar, pedidovenda.getCdpedidovenda()});
	}
	
	/**
	 * Busca os pedidos de venda com o identificador e a empresa passada por par�metro.
	 *
	 * @param empresa_cnpj
	 * @param identificador
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/11/2014
	 */
	public List<Pedidovenda> findByEmpresaIdentificador(String empresa_cnpj, String identificador) {
		if(identificador == null || identificador.equals("")){
			return null;
		}
		return query()
				.select("pedidovenda.cdpedidovenda, pedidovenda.dtpedidovenda")
				.join("pedidovenda.empresa empresa")
				.where("empresa.cnpj = ?", empresa_cnpj != null ? new Cnpj(empresa_cnpj) : null)
				.where("pedidovenda.identificador = ?", identificador)
				.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.CANCELADO)
				.list();
	}
	
	/**
	* M�todo que verifica se o identificador j� est� cadastrado
	*
	* @param identificador
	* @return boolean
	* @author Jo�o Vitor
	* @since 01/12/2014
	*/
	public Boolean isIdentificadorCadastrado(Pedidovenda pedidovenda) {
		if(pedidovenda == null){
			return false;
		}
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
		String dataPedidoVendaAtual = null;
		
		if (pedidovenda.getDtpedidovenda() != null) {
			dataPedidoVendaAtual = simpleDateFormat.format(pedidovenda.getDtpedidovenda());
		}
		
		return query()
		.select("pedidovenda.identificador")
		.leftOuterJoin("pedidovenda.empresa empresa")
		.where("pedidovenda.identificador = ?", pedidovenda.getIdentificador())
		.where("pedidovenda.identificador <> ''")
		.where("pedidovenda.identificador IS NOT NULL")
		.where("pedidovenda.cdpedidovenda <> ?", pedidovenda.getCdpedidovenda())
		.where("EXTRACT(YEAR FROM date(pedidovenda.dtpedidovenda)) = ?", Integer.parseInt(dataPedidoVendaAtual))
		.where("empresa = ?", pedidovenda.getEmpresa())
		.unique() != null;
	}
	
	/**
	 * M�todo que verifica se existem pedidos de venda cuja confirma��o dever� ser realizada pelo WMS.
	 * Caso existam, retorna true, caso contr�rio, false;
	 * 
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public boolean verificaSincronizacaoComWmsForConfirmacao(String whereIn){
		if(whereIn == null || whereIn.trim().isEmpty()){
			return false;
		}
		
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(Pedidovenda.class)
					.join("pedidovenda.pedidovendatipo pedidovendatipo")
					.join("pedidovenda.empresa empresa")
					.whereIn("pedidovenda.cdpedidovenda", whereIn)
					.where("coalesce(pedidovendatipo.sincronizarComWMS, false) = true")
					.openParentheses()
						.openParentheses()
							.where("coalesce(pedidovendatipo.confirmacaoManualWMS, false) = true")
							.where("pedidovenda.identificadorcarregamento is NULL")
						.closeParentheses()
						.or()
						.where("coalesce(pedidovendatipo.confirmacaoManualWMS, false) = false")
					.closeParentheses()
					.where("empresa.integracaowms = true ")
					.unique() > 0;
	}

	public Pedidovenda loadByProtocolooffline(String protocolooffline) {
		return querySined()
				.setUseWhereEmpresa(false)
				.setUseWhereProjeto(false)
				.select("pedidovenda.cdpedidovenda, pedidovenda.protocolooffline")
				.where("pedidovenda.protocolooffline = ?", protocolooffline)
				.unique();
	}
	
	/**
	* M�todo que verifica se existe pedido de venda de representa��o
	*
	* @param whereIn
	* @return
	* @since 13/02/2015
	* @author Luiz Fernando
	*/
	public boolean existePedidoVendaRepresentacao(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Pedidovenda.class)
			.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
			.whereIn("pedidovenda.cdpedidovenda", whereIn)
			.where("pedidovendatipo.representacao = true")
			.unique() > 0;
	}
	
	/**
	 * M�todo que atualiza a refer�ncia ao carregamento no pedidovenda para permitir ligar a futura venda ao carregamento separado no wms.
	 * 
	 * @param pedidovenda
	 * @throws Exception
	 * @author Rafael Salvio
	 */
	public void updateCarregamentoWms(Pedidovenda pedidovenda) throws Exception{
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null){
			throw new Exception("Pedidovenda n�o pode ser nulo.");
		} else if(pedidovenda.getIdentificadorcarregamento() == null){
			throw new Exception("Identificador do carregamento n�o pode ser nulo.");
		}
		
		getJdbcTemplate().update("UPDATE pedidovenda SET identificadorcarregamento = ? WHERE cdpedidovenda = ?;", 
			new Object[]{pedidovenda.getIdentificadorcarregamento(), pedidovenda.getCdpedidovenda()});
	}

	/**
	* M�todo que busca os pedidos de venda com a empresa
	*
	* @param whereInPedidovenda
	* @return
	* @since 28/05/2015
	* @author Luiz Fernando
	*/
	public List<Pedidovenda> findForEntradafiscal(String whereInPedidovenda) {
		if(StringUtils.isEmpty(whereInPedidovenda))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("pedidovenda.cdpedidovenda, empresa.cdpessoa ")
				.leftOuterJoin("pedidovenda.empresa empresa")
				.whereIn("pedidovenda.cdpedidovenda", whereInPedidovenda)
				.list();
	}
	
	/**
	 * @param material
	 * @return
	 * @since 13/07/2015
	 * @author Andrey Leonardo
	 */
	public Date getMinDataPedidovendaReserva(Material material){
		return newQueryBuilderSined(Date.class)
					.setUseTranslator(false)
					.select("min(pedidovenda.dtpedidovenda)")
					.from(Pedidovenda.class)
					.join("pedidovenda.pedidovendatipo pedidovendatipo")
					.join("pedidovenda.listaPedidovendamaterial pedidovendamaterial")
					.where("pedidovendamaterial.material = ?", material)
					.where("pedidovendatipo.reserva = ?", Boolean.TRUE)
					.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.CANCELADO)
					.unique();
		
	}

	/**
	 * Busca os pedidos vinculados ao pneu.
	 *
	 * @param pneu
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/07/2015
	 */
	public List<Pedidovenda> findByPneu(Pneu pneu) {
		return query()
				.select("pedidovenda.cdpedidovenda, pedidovenda.dtpedidovenda")
				.join("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
				.where("listaPedidovendamaterial.pneu = ?", pneu)
				.list();
	}
	
	/**
	 * M�todo que atualiza em massa as observa��es dos pedidos de venda contidos no par�metro.
	 * 
	 * @param listaPedidovenda
	 * @author Rafael Salvio
	 */
	public void atualizarObservacaoPedidovenda(final List<Pedidovenda> listaPedidovenda){
		if(listaPedidovenda != null && !listaPedidovenda.isEmpty()){
			getJdbcTemplate().batchUpdate("update pedidovenda set observacaointerna = ? where cdpedidovenda = ?", new BatchPreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					Pedidovenda pv = listaPedidovenda.get(i);
					ps.setString(1, pv.getObservacaointerna());
					ps.setInt(2, pv.getCdpedidovenda());
				}
				@Override
				public int getBatchSize() {
					return listaPedidovenda.size();
				}
			});
		}
	}
	
	/**
	* M�todo que verifica se existe produtos e servi�os no pedido de venda
	*
	* @param whereIn
	* @return
	* @since 25/01/2016
	* @author Luiz Fernando
	*/
	public boolean existePedidoComServicoEProduto(String whereIn) {
		boolean servico = newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Pedidovenda.class)
			.join("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
			.join("listaPedidovendamaterial.material material")
			.whereIn("pedidovenda.cdpedidovenda", whereIn)
			.where("material.servico = true")
			.unique() > 0;
			
		boolean produto = newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Pedidovenda.class)
			.join("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
			.join("listaPedidovendamaterial.material material")
			.whereIn("pedidovenda.cdpedidovenda", whereIn)
			.where("material.produto = true")
			.unique() > 0;
			
		return servico && produto;
	}
	
	/**
	* M�todo que carrega o pedido de venda para registrar devolu��o
	*
	* @param whereIn
	* @return
	* @since 04/02/2016
	* @author Luiz Fernando
	*/
	public Pedidovenda loadForRegistrarDevolucao(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("pedidovenda.cdpedidovenda, pedidovenda.desconto, pedidovenda.valorusadovalecompra, pedidovenda.pedidovendasituacao, " +
						"empresa.cdpessoa, empresa.nome, empresa.integracaowms, " +
						"cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj, empresa.cpf, empresa.cnpj, " +
						"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
						"listaPedidovendamaterial.cdpedidovendamaterial, listaPedidovendamaterial.quantidade, " +
						"listaPedidovendamaterial.preco, listaPedidovendamaterial.desconto, listaPedidovendamaterial.altura, listaPedidovendamaterial.largura, " +
						"listaPedidovendamaterial.comprimento, listaPedidovendamaterial.comprimentooriginal, listaPedidovendamaterial.fatorconversao, listaPedidovendamaterial.qtdereferencia, " +
						"loteestoque.cdloteestoque, loteestoque.numero, " +
						"material.cdmaterial, material.nome, projeto.cdprojeto, centrocusto.cdcentrocusto, materialRateioEstoque.cdMaterialRateioEstoque, contaGerencialDevolucaoEntrada.cdcontagerencial, " +
						"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
						"unidademedidapedidovenda.cdunidademedida, unidademedidapedidovenda.nome, unidademedidapedidovenda.simbolo ")
				.join("pedidovenda.cliente cliente")
				.join("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
				.join("listaPedidovendamaterial.material material")
				.join("listaPedidovendamaterial.unidademedida unidademedidapedidovenda")
				.leftOuterJoin("listaPedidovendamaterial.loteestoque loteestoque")
				.join("material.unidademedida unidademedida")
				.join("pedidovenda.empresa empresa")
				.leftOuterJoin("pedidovenda.localarmazenagem localarmazenagem")
				.leftOuterJoin("pedidovenda.projeto projeto")
				.leftOuterJoin("pedidovenda.centrocusto centrocusto")
				.leftOuterJoin("material.materialRateioEstoque materialRateioEstoque")
				.leftOuterJoin("materialRateioEstoque.contaGerencialDevolucaoEntrada contaGerencialDevolucaoEntrada")
				.where("pedidovenda = ?", pedidovenda)
				.unique();
	}

	/**
	* M�todo que carrega o pedido de venda com o local de armazenagem
	*
	* @param pedidovenda
	* @return
	* @since 12/02/2016
	* @author Luiz Fernando
	*/
	public Pedidovenda loadWithLocalAndProjeto(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, projeto.cdprojeto ")
				.leftOuterJoin("pedidovenda.localarmazenagem localarmazenagem")
				.leftOuterJoin("pedidovenda.projeto projeto")
				.where("pedidovenda = ?", pedidovenda)
				.unique();
	}

	/**
	* M�todo que verifica se existe somente produtos que integram com o ecf
	*
	* @param whereIn
	* @return
	* @since 17/03/2016
	* @author Luiz Fernando
	*/
	public boolean existeSomenteProdutoVendaECF(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
				.from(Pedidovenda.class)
				.setUseTranslator(false) 
				.select("count(*)")
				.join("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
				.join("listaPedidovendamaterial.material material")
				.whereIn("pedidovenda.cdpedidovenda", whereIn)
				.where("COALESCE(material.vendaecf, false) = false")
				.unique()
				.intValue() == 0;
	}

	/**
	 * Busca o pedido de venda a partir do identificador de integra��o do item
	 *
	 * @param identificadorintegracao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 09/05/2016
	 */
	public Pedidovenda findByIdentificadorIntegracao(Integer identificadorintegracao) {
		return query()
					.select("pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao")
					.join("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
					.where("listaPedidovendamaterial.identificadorintegracao = ?", identificadorintegracao)
					.unique();
	}

	/**
	* M�todo que verifica se no pedido existe pelo menos um material com garantia
	*
	* @param cdpedidovenda
	* @return
	* @since 22/06/2016
	* @author Luiz Fernando
	*/
	public boolean existeItemComGarantia(Integer cdpedidovenda) {
		if(cdpedidovenda == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
				.from(Pedidovenda.class)
				.setUseTranslator(false) 
				.select("count(*)")
				.join("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
				.join("listaPedidovendamaterial.material material")
				.where("pedidovenda.cdpedidovenda = ?", cdpedidovenda)
				.where("material.prazogarantia is not null")
				.unique()
				.intValue() > 0;
	}

	/**
	* M�todo que carrega o pedido de venda para valida��o da garantia
	*
	* @param pedidovenda
	* @return
	* @since 23/06/2016
	* @author Luiz Fernando
	*/
	public Pedidovenda loadForValidarGarantia(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()	
				 .select("pedidovenda.cdpedidovenda, pedidovenda.dtpedidovenda,  " +
				 		"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.garantia, " +
				 		"listaPedidovendamaterial.cdpedidovendamaterial, listaPedidovendamaterial.quantidade, listaPedidovendamaterial.dtprazoentrega, " +
				 		"material.cdmaterial, material.nome, material.identificacao, material.servico, material.produto, material.epi, material.patrimonio, material.prazogarantia, " +
				 		"listaMaterialsimilar.cdmaterialsimilar, materialsimilaritem.cdmaterial, materialsimilaritem.nome, " +
						"pneu.cdpneu, pneu.serie, pneu.dot, pneu.numeroreforma, " +
						"pneumarca.cdpneumarca, pneumarca.nome, " +
						"pneumedida.cdpneumedida, pneumedida.nome, " +
						"pneumodelo.cdpneumodelo, pneumodelo.nome, " +
				 		"pneumaterialbanda.cdmaterial, pneumaterialbanda.identificacao, pneumaterialbanda.nome, pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome ")
				 .leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
			     .leftOuterJoin("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
			     .leftOuterJoin("listaPedidovendamaterial.pneu pneu")
			     .leftOuterJoin("pneu.pneumarca pneumarca")
			     .leftOuterJoin("pneu.pneumodelo pneumodelo")
			     .leftOuterJoin("pneu.pneumedida pneumedida")
			     .leftOuterJoin("pneu.materialbanda pneumaterialbanda")
			     .leftOuterJoin("listaPedidovendamaterial.material material")
				 .leftOuterJoin("material.listaMaterialsimilar listaMaterialsimilar")
				 .leftOuterJoin("listaMaterialsimilar.materialsimilaritem materialsimilaritem")
				 .leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
				 .where("pedidovenda = ?", pedidovenda)
				 .orderBy("listaPedidovendamaterial.ordem, material.nome")
				 .unique();
	}

	/**
	 * Query para sincroniza��o com o W3produ��o
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/09/2016
	 */
	public List<Pedidovenda> findForW3Producao(String whereIn) {
		return query()
					.select("pedidovenda.cdpedidovenda, pedidovenda.identificador, pedidovenda.dtpedidovenda, pedidovenda.identificacaoexterna," +
							"colaborador.cdpessoa,colaborador.nome")
					.leftOuterJoin("pedidovenda.colaborador colaborador")
					.whereIn("pedidovenda.cdpedidovenda", whereIn)
					.orderBy("pedidovenda.cdpedidovenda")
					.list();
	}
	
	/**
	* M�todo que carrega o pedido de venda para validar os valores com a venda
	*
	* @param pedidovenda
	* @return
	* @since 20/01/2017
	* @author Luiz Fernando
	*/
	public Pedidovenda loadForValidacaoPedidoDiferenteVenda(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()	
				 .select("pedidovenda.cdpedidovenda, pedidovenda.valorfrete, pedidovenda.desconto, " +
				 		"listaPedidovendamaterial.cdpedidovendamaterial, listaPedidovendamaterial.preco, listaPedidovendamaterial.desconto ")
			     .leftOuterJoin("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
				 .where("pedidovenda = ?", pedidovenda)
				 .unique();
	}
	
	public List<Pedidovenda> findForNotaRetorno(String whereIn) {
		return query()
					.select("pedidovenda.cdpedidovenda, pedidovenda.identificador, listaColeta.cdcoleta ")
					.leftOuterJoin("pedidovenda.listaColeta listaColeta")
					.whereIn("pedidovenda.cdpedidovenda", whereIn)
					.list();
	}
	
	/**
	 * 
	 * @param param
	 * @return
	 * @author Mairon Cezar
	 */
	public  List<Pedidovenda> findPedidoVendaGarantiaForAutocomplete(String param) {
		return query()
			.select("pedidovenda.cdpedidovenda, pedidovenda.identificador, cliente.nome")
			.join("pedidovenda.cliente cliente")
			.join("pedidovenda.pedidovendatipo pedidovendatipo")
			.where("pedidovendatipo.garantia = ?", Boolean.TRUE)
			.openParentheses()
				.whereLikeIgnoreAll("pedidovenda.cdpedidovenda",param)
				.or()
				.whereLikeIgnoreAll("cliente.nome",param)
			.closeParentheses()
			.list();
	}
	
	public Pedidovenda findForSelecaoservicogarantido(Pedidovenda pedidovenda, String whereInPneusJaSelecionados){
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Pedidovenda> query = query()	
				 .select("pedidovenda.cdpedidovenda, pedidovenda.valorfrete, pedidovenda.desconto, " +
				 		"listaPedidovendamaterial.cdpedidovendamaterial, listaPedidovendamaterial.preco, listaPedidovendamaterial.desconto, "+
				 		"material.cdmaterial, material.nome, material.identificacao")
			     .join("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
			     .join("listaPedidovendamaterial.material material")
			     .join("listaPedidovendamaterial.pneu pneu")
				 .where("pedidovenda = ?", pedidovenda)
				 .whereIn("pneu.cdpneu not", whereInPneusJaSelecionados);
		query = SinedUtil.setJoinsByComponentePneu(query);
		return query.unique();
	}
	
	public Pedidovenda findForGarantiareforma(Pedidovenda pedidovenda){
		return query()
		.select("pedidovenda.cdpedidovenda, pedidovenda.identificador, cliente.nome, pedidovendaorigem.cdpedidovenda, pedidovendaorigem.identificador, "+
				"cliente.nome, cliente.cdpessoa, cliente.razaosocial, "+
				"colaborador.cdpessoa, colaborador.nome, "+
				"empresa.cdpessoa, empresa.nome")
		.join("pedidovenda.cliente cliente")
		.join("pedidovenda.pedidovendatipo pedidovendatipo")
		.join("pedidovenda.pedidovendaorigem pedidovendaorigem")
		.leftOuterJoin("pedidovenda.colaborador colaborador")
		.leftOuterJoin("pedidovendaorigem.empresa empresa")
		.where("pedidovendatipo.garantia = ?", Boolean.TRUE)
		.where("pedidovenda.cdpedidovenda = ?", pedidovenda.getCdpedidovenda())
		.unique();
	}
	
	public Pedidovenda findByPedidovendamaterial(Pedidovendamaterial pedidovendamaterial){
		return query()
		.select("pedidovenda.cdpedidovenda")
		.join("pedidovenda.listaPedidovendamaterial pedidovendamaterial")
		.where("pedidovendamaterial.cdpedidovendamaterial = ?", pedidovendamaterial.getCdpedidovendamaterial())
		.unique();
	}

	public Pedidovenda loadWithAllDocumentotipo(String cdpedidovenda){
		if(cdpedidovenda == null || "".equals(cdpedidovenda))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("pedidovenda.cdpedidovenda, " +
					"documentotipo.cddocumentotipo, documentotipo.nome, documentotipo.exibirnavenda, " +
					"vendapagamentodocumentotipo.cddocumentotipo, vendapagamentodocumentotipo.nome, vendapagamentodocumentotipo.exibirnavenda ")
			.join("pedidovenda.listaPedidovendamaterial pedidovendamaterial")
			.leftOuterJoin("pedidovenda.documentotipo documentotipo")
			.leftOuterJoin("pedidovenda.listaPedidovendapagamento pedidovendapagamento") 
			.leftOuterJoin("pedidovendapagamento.documentotipo vendapagamentodocumentotipo")
			.where("pedidovenda.cdpedidovenda = ?", Integer.parseInt(cdpedidovenda))
			.unique();		
	}
	
	public boolean existeVariasVendas(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Pedidovenda.class)
			.where("pedidovenda = ?", pedidovenda)
			.where("(select count(v.cdvenda) " +
							"from Venda v " +
							"join v.vendasituacao vs " +
							"where v.pedidovenda.cdpedidovenda = pedidovenda.cdpedidovenda and vs.cdvendasituacao <> ?) > 1", Vendasituacao.CANCELADA.getCdvendasituacao())
			.unique() > 0;
	}
	public List<Pedidovenda> findByCodMaterial(Integer codMaterial) {
		return query().select("pedidovenda.cdpedidovenda,pedidovenda.dtpedidovenda,material.cdmaterial," +
				"material.nome,listaMaterial.quantidade")
			.leftOuterJoin("pedidovenda.listaPedidovendamaterial listaMaterial")
			.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
			.leftOuterJoin("listaMaterial.material material")
			.where("material.cdmaterial = ?",+codMaterial)
			.where("pedidovenda.pedidovendasituacao <> 1")
			.where("pedidovenda.pedidovendasituacao <> 2")
			.where("pedidovendatipo.reserva = true").list();
	}
	public List<Pedidovenda> identificadorExternoRepetido(Integer cdpedidovenda,
			String externo) {
		return query()
				.select("pedidovenda.cdpedidovenda,pedidovenda.dtpedidovenda")
				.where("pedidovenda.cdpedidovenda <> ?",cdpedidovenda)
				.where("pedidovenda.identificacaoexterna  like ?", externo)
				.list();
	}
	public boolean isOrigemOrcamento(Integer cdvendaorcamento) {
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Pedidovenda.class)
				.leftOuterJoin("pedidovenda.vendaorcamento vendaorcamento")
				.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.CANCELADO)
				.where("vendaorcamento.cdvendaorcamento = ?",cdvendaorcamento)
				.unique() > 0;
	}
	
	public Pedidovenda loadForRecalcularcomissao(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Pedido de Venda n�o pode ser nula.");
		
		return query()
				.select("pedidovenda.cdpedidovenda, pedidovenda.cdpedidovenda, pedidovendatipo.cdpedidovendatipo, pedidovenda.pedidovendasituacao, " +
						"pedidovendatipo.bonificacao, pedidovendatipo.reserva, pedidovendatipo.geracaocontareceberEnum ")
				.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
				.where("pedidovenda = ?", pedidovenda)
				.unique();
	}
	public Pedidovenda carregarListaMaterial(Pedidovenda pedidovenda) {
		return query()
				.select("pedidovenda.cdpedidovenda,material.cdmaterial,listaPedidovendamaterial.quantidade," +
						"pedidovendatipo.cdpedidovendatipo,pedidovendatipo.geracaocontareceberEnum")
				.leftOuterJoin("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
				.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("listaPedidovendamaterial.material material")
				.where("pedidovenda.cdpedidovenda = ?", pedidovenda.getCdpedidovenda())
				.unique();
	}
	public Pedidovenda loadPedidovenda(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Pedido de Venda n�o pode ser nula.");
		
		return query()
				.select("pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao, " +
						"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.bonificacao, pedidovendatipo.reserva, pedidovendatipo.gerarexpedicaovenda, " +
						"pedidovendatipo.comodato, pedidovendatipo.baixaestoqueEnum, pedidovendatipo.gerarNotaIndustrializacaoRetorno, pedidovendatipo.gerarnotaretornovenda, " +
						"pedidovendatipo.referenciarnotaentradananotasaida, pedidovendatipo.consideraripiconta, pedidovendatipo.geracaocontareceberEnum, pedidovendatipo.naoatualizarvencimento, " +
						"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome, naturezaoperacao.operacaonfe, naturezaoperacao.finalidadenfe, naturezaoperacao.razaosocial, " +
						"naturezaoperacaoservico.cdnaturezaoperacao, naturezaoperacaoservico.nome, naturezaoperacaoservico.operacaonfe, naturezaoperacaoservico.finalidadenfe, " +
						"notaTipo.cdNotaTipo, notaTipoServico.cdNotaTipo, " + 
						"cfopindustrializacao.cdcfop, cfopindustrializacao.codigo, " +
						"cfopindustrializacao.descricaoresumida, cfopindustrializacao.descricaocompleta, " +
						"cfopindustrializacao.naoconsiderarreceita, " +
						"cfopretorno.cdcfop, cfopretorno.codigo," +
						"cfopretorno.descricaoresumida, cfopretorno.descricaocompleta, cfopretorno.naoconsiderarreceita," +
						"templatenotaservico.cdreporttemplate, templatenotaservico.leiaute ")
				.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("pedidovendatipo.naturezaoperacao naturezaoperacao")
				.leftOuterJoin("naturezaoperacao.notaTipo notaTipo")
				.leftOuterJoin("pedidovendatipo.naturezaoperacaoservico naturezaoperacaoservico")
				.leftOuterJoin("naturezaoperacaoservico.notaTipo notaTipoServico")
				.leftOuterJoin("pedidovendatipo.cfopindustrializacao cfopindustrializacao")
				.leftOuterJoin("pedidovendatipo.cfopretorno cfopretorno")
				.leftOuterJoin("pedidovendatipo.templatenotaservico templatenotaservico")
				.where("pedidovenda = ?", pedidovenda)
				.unique();
	}
	
	public Pedidovenda findCdMaterialById(String id) {
		if(id == null) {
			throw new SinedException("Pedido de Venda n�o pode ser nula.");
		}
		
		return query()
				.select("material.cdmaterial")
				.leftOuterJoin("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
				.leftOuterJoin("listaPedidovendamaterial.material material")
				.where("pedidovenda = ?", new Pedidovenda(Integer.parseInt(id)))
				.unique();
	}
	public Pedidovenda carregarInfoPedidoSituacao(Pedidovenda pedidovenda) {
		return query()
				.select("pedidovenda.situacaoPendencia,pedidovenda.cdpedidovenda,pedidovenda.pedidovendasituacao," +
						"pedidovendatipo.cdpedidovendatipo,pedidovendatipo.baixaestoqueEnum,pedidovendatipo.geracaocontareceberEnum")
				.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
				.where("pedidovenda = ?", pedidovenda)
				.unique();
	}
	public Pedidovenda loadForNegociacao(Pedidovenda pv) {
		return query()
				.select("pedidovenda.situacaoPendencia,pedidovenda.cdpedidovenda,empresa.cdpessoa,empresa.nome," +
						"pedidovenda.colaborador,cliente.cdpessoa,cliente.nome,pedidovenda.dtpedidovenda,pedidovenda.identificador," +
						"pedidovenda.valorfrete,pedidovenda.taxapedidovenda,pedidovenda.desconto,pedidovenda.valorusadovalecompra," +
						"listaPedidovendamaterial.cdpedidovendamaterial,listaPedidovendamaterial.quantidade,listaPedidovendamaterial.multiplicador," +
						"listaPedidovendamaterial.preco,listaPedidovendamaterial.desconto,listaPedidovendamaterial.percentualdesconto," +
						"unidademedida.cdunidademedida,unidademedida.nome,unidademedida.simbolo," +
						"prazopagamento.cdprazopagamento,centrocusto.cdcentrocusto,contagerencial.cdcontagerencial," +
						"documento.cddocumento,material.cdmaterial," +
						"documentoclasse.cddocumentoclasse,documentoacao.cddocumentoacao")	
				.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
				.leftOuterJoin("listaPedidovendamaterial.material material")
				.leftOuterJoin("pedidovenda.cliente cliente")
				.leftOuterJoin("pedidovenda.empresa empresa")
				.leftOuterJoin("empresa.centrocusto centrocusto")
				.leftOuterJoin("pedidovenda.listaPedidoVendaNegociacao listaPedidoVendaNegociacao")
				.leftOuterJoin("listaPedidoVendaNegociacao.documento documento")
				.leftOuterJoin("documento.documentoclasse documentoclasse")
				.leftOuterJoin("documento.documentoacao documentoacao")
				.leftOuterJoin("empresa.contagerencial contagerencial")
				.leftOuterJoin("pedidovenda.prazopagamento prazopagamento")
				.leftOuterJoin("listaPedidovendamaterial.unidademedida unidademedida")
				.where("pedidovenda = ?", pv)
				.unique();
	}
	
	public Pedidovenda loadForCalculoMarkupPedido(Pedidovenda pedidovenda) {
		return query()
				.select("pedidovenda.cdpedidovenda, pedidovenda.desconto, " +
						"listaPedidovendamaterial.cdpedidovendamaterial, listaPedidovendamaterial.preco, listaPedidovendamaterial.desconto, " +
						"unidademedida.cdunidademedida, " +
						"listaPedidovendamaterial.quantidade, material.cdmaterial, material.valorcusto")
				.leftOuterJoin("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
				.leftOuterJoin("listaPedidovendamaterial.material material")
				.leftOuterJoin("listaPedidovendamaterial.unidademedida unidademedida")
				.where("pedidovenda = ?", pedidovenda)
				.unique();
	}
	
	public List<Pedidovenda> findForRecalculoFaixaMarkup(String whereIn){
		return query()
				.select("pedidovenda.valorusadovalecompra, pedidovenda.desconto, " +
						"vendamaterial.percentualdesconto, vendamaterial.desconto, vendamaterial.cdpedidovendamaterial, vendamaterial.preco, " +
						"vendamaterial.quantidade, vendamaterial.valorcustomaterial, vendamaterial.multiplicador, pedidovenda.taxapedidovenda, pedidovenda.valorfrete, " +
						"empresa.cdpessoa, " +
						"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.bonificacao, " +
						"material.cdmaterial, material.cadastrarFaixaMarkup")
				.join("pedidovenda.listaPedidovendamaterial vendamaterial")
				.leftOuterJoin("pedidovenda.empresa empresa")
				.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
				.join("vendamaterial.material material")
				.whereIn("pedidovenda.cdpedidovenda", whereIn)
				.list();
	}
	
	public Pedidovenda findForListaProducaoAgenda(Pedidovenda pedidoVenda){
		return query()
				.select("listaproducaoagenda.cdproducaoagenda, listaproducaoagenda.producaoagendasituacao, pneu.cdpneu, material.cdmaterial, material.servico")
				.leftOuterJoin("pedidovenda.listaProduocaoagenda listaproducaoagenda")
				.leftOuterJoin("pedidovenda.listaPedidovendamaterial pedidovendamaterial ")
				.leftOuterJoin("pedidovendamaterial.pneu pneu ")
				.leftOuterJoin("pedidovendamaterial.material material")
				.where("pedidovenda = ?", pedidoVenda)
				.unique();
		
		
	}
	
	public Pedidovenda findForComprovanteRTF(Pedidovenda pedidoVenda){
		if(pedidoVenda == null || pedidoVenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				.setUseReadOnly(true)
				.select("pedidovenda.cdpedidovenda,pedidovenda.qtdeParcelas,pedidovenda.observacaoPedidoVendaTipo, pedidovenda.pedidovendasituacao, " +
						"pedidovenda.identificacaoexterna, pedidovenda.dtpedidovenda, pedidovenda.observacao,cliente.cdpessoa, cliente.nome, cliente.email, " +
						"pedidovenda.observacaointerna,pedidovenda.valorfrete,pedidovenda.desconto, pedidovenda.taxapedidovenda, pedidovenda.valorusadovalecompra," +
						" vendaorcamento.cdvendaorcamento,frete.nome,documentotipo.nome,prazopagamento.nome, empresa.cdpessoa, empresa.nome,empresa.nomefantasia, empresa.cnpj, " +
						"empresa.inscricaoestadual,empresa.cdpessoa, empresa.email,empresa.site, projeto.nome,pedidovendatipo.descricao," +
						"terceiro.nome, listaPedidovendavalorcampoextra.valor, campoextrapedidovendatipo.nome, localarmazenagem.nome," +
						"colaborador.cdpessoa, colaborador.nome, cliente.cdpessoa, cliente.nome")
				.leftOuterJoin("pedidovenda.vendaorcamento vendaorcamento")
				.leftOuterJoin("pedidovenda.colaborador colaborador")
				.leftOuterJoin("pedidovenda.cliente cliente")
				.leftOuterJoin("pedidovenda.frete frete")
				.leftOuterJoin("pedidovenda.documentotipo documentotipo")
				.leftOuterJoin("pedidovenda.prazopagamento prazopagamento")
				.leftOuterJoin("pedidovenda.empresa empresa")
				.leftOuterJoin("pedidovenda.projeto projeto")
				.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("pedidovenda.terceiro terceiro")
				.leftOuterJoin("pedidovenda.localarmazenagem localarmazenagem")
				.leftOuterJoin("pedidovenda.listaPedidovendavalorcampoextra listaPedidovendavalorcampoextra")
				.leftOuterJoin("listaPedidovendavalorcampoextra.campoextrapedidovendatipo campoextrapedidovendatipo")
				.where("pedidovenda = ?", pedidoVenda)
				.setMaxResults(1)
				.unique();
		
	}
	public Pedidovenda loadForVendaNaoNegociada(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				.setUseReadOnly(true)
				.select("pedidovenda.cdpedidovenda, pedidovenda.identificacaoexterna, pedidovenda.identificador, " +
						"colaborador.cdpessoa, colaborador.nome, cliente.cdpessoa, cliente.nome, " +
						"empresa.cdpessoa, empresa.nome,empresa.nomefantasia, empresa.cnpj, " +
						"empresa.inscricaoestadual, empresa.cdpessoa, empresa.email, empresa.site, " +
						"projeto.cdprojeto, projeto.nome, endereco.cdendereco, " +
						"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
						"pedidovendamaterial.cdpedidovendamaterial, pedidovendamaterial.quantidade, pedidovendamaterial.preco, pedidovendamaterial.desconto, " +
						"pedidovendamaterial.multiplicador, pedidovendamaterial.valorSeguro, pedidovendamaterial.outrasdespesas, material.cdmaterial, unidademedida.cdunidademedida, unidademedida.nome")
				.leftOuterJoin("pedidovenda.colaborador colaborador")
				.leftOuterJoin("pedidovenda.cliente cliente")
				.leftOuterJoin("pedidovenda.empresa empresa")
				.leftOuterJoin("pedidovenda.projeto projeto")
				.leftOuterJoin("pedidovenda.localarmazenagem localarmazenagem")
				.leftOuterJoin("pedidovenda.endereco endereco")
				.leftOuterJoin("pedidovenda.listaPedidovendamaterial pedidovendamaterial")
				.leftOuterJoin("pedidovendamaterial.material material")
				.leftOuterJoin("pedidovendamaterial.unidademedida unidademedida")
				.where("pedidovenda = ?", pedidovenda)
				.unique();
	}
	
	public Integer getPedidosAguardandoAprovacaoForAndroid(Integer cdusuario) {
		return newQueryBuilderSinedWithFrom(Long.class)
					.select("count(*)")
					.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.AGUARDANDO_APROVACAO)
					.where("pedidovenda.colaborador = ?", new Colaborador(cdusuario))
					.unique().intValue();
	}
	
	public Integer getPedidosPrevistosForAndroid(Integer cdusuario) {
		return newQueryBuilderSinedWithFrom(Long.class)
				.select("count(*)")
				.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.PREVISTA)
				.where("pedidovenda.colaborador = ?", new Colaborador(cdusuario))
				.unique().intValue();
	}
	
	public Integer getPedidosConfirmadorForAndroid(Integer cdusuario) {
		return newQueryBuilderSinedWithFrom(Long.class)
				.select("count(*)")
				.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.CONFIRMADO)
				.where("exists(select 1 from Pedidovendahistorico ph where ph.pedidovenda = pedidovenda and ph.dtaltera >= ? and upper(ph.acao) = 'CONFIRMADO')", new Timestamp(SinedDateUtils.firstDateOfMonth().getTime()))
				.where("pedidovenda.colaborador = ?", new Colaborador(cdusuario))
				.unique().intValue();
	}	

	public void insertPedidoAprovadoTabelaSincronizacaoEcommerce(Ecom_PedidoVenda ecom_pedidoVenda) {
		getJdbcTemplate().execute("select sincronizacaotabelaecommerce_insert_update(8,false," + ecom_pedidoVenda.getCdEcom_pedidovenda() + ");");
	}
	
	public void updateCampo(Pedidovenda venda, String nomeCampo, Object valor) {
		if (Util.objects.isNotPersistent(venda)) {
			throw new SinedException("Par�metros inv�lidos.");
		}
		
		if(valor != null){
			getHibernateTemplate().bulkUpdate("update Pedidovenda venda set "+nomeCampo+" = ? where venda = ?", new Object[] {valor, venda});
		}else {
			getHibernateTemplate().bulkUpdate("update Pedidovenda venda set "+nomeCampo+" = null where venda = ?", new Object[] {venda});
		}
	}

	public void insertPedidoCanceladoTabelaSincronizacaoEcommerce(Ecom_PedidoVenda ecom_pedidoVenda) {
		getJdbcTemplate().execute("select sincronizacaotabelaecommerce_insert_update(10,false," + ecom_pedidoVenda.getCdEcom_pedidovenda() + ");");
	}
	
	public void insertPedidoConfirmadoTabelaSincronizacaoEcommerce(Ecom_PedidoVenda ecom_pedidoVenda) {
		getJdbcTemplate().execute("select sincronizacaotabelaecommerce_insert_update(11,false," + ecom_pedidoVenda.getCdEcom_pedidovenda() + ");");
	}	

}