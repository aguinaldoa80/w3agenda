package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Inventario;
import br.com.linkcom.sined.geral.bean.InventarioHistorico;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class InventarioHistoricoDAO extends GenericDAO<InventarioHistorico>{

	/**
	 * Busca o hist�rico a partir de um invent�rio 
	 *
	 * @param inventario
	 * @return
	 * @since 03/10/2019
	 * @author Rodrigo Freitas
	 */
	public List<InventarioHistorico> findByInventario(Inventario inventario) {
		if(inventario == null || inventario.getCdinventario() == null){
			throw new SinedException("Invent�rio n�o pode ser nulo.");
		}
		return query()
					.select("inventarioHistorico.cdInventarioHistorico, inventarioHistorico.inventarioAcao, inventarioHistorico.observacao, " +
							"inventarioHistorico.cdusuarioaltera, inventarioHistorico.dtaltera")
					.where("inventarioHistorico.inventario = ?", inventario)
					.orderBy("inventarioHistorico.dtaltera desc")
					.list();
	}

}
