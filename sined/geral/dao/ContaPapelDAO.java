package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contapapel;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContaPapelDAO extends GenericDAO<Contapapel>{
	
	/**
	 * 
	 * M�todo que apaga da lista de ContaPapel quem n�o estiver dentro do whereIn.
	 *
	 *@author Thiago Augusto
	 *@date 21/03/2012
	 * @param whereIn
	 * @param conta
	 */
	public void deleteListContaPapel(String whereIn, Conta conta){
		
//		APAGA DA LISTA DE CONTAPAPEL DE UMA CONTA QUE N�O ESTIVER DENTRO DO WHEREIN.
		if (whereIn != null && !whereIn.equals("")){
			getJdbcTemplate().update("DELETE FROM CONTAPAPEL WHERE CDCONTA = "+conta.getCdconta()+" AND CDCONTAPAPEL NOT IN ("+whereIn+")");
		} 
//		APAGA A LISTA INTEIRA DE CONTAPAPEL DE UMA CONTA.
		else {
			getJdbcTemplate().update("DELETE FROM CONTAPAPEL WHERE CDCONTA = "+conta.getCdconta());
		}
	}

	public List<Contapapel> findByConta(Conta conta) {
		if(conta == null || conta.getCdconta() == null) throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("contapapel.cdcontapapel, conta.cdconta, papel.cdpapel")
				.leftOuterJoin("contapapel.conta conta")
				.leftOuterJoin("contapapel.papel papel")
				.where("conta = ?", conta)
				.list();
	}

}
