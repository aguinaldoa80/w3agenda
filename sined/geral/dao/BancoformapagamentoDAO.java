package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.BancoFormapagamento;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class BancoformapagamentoDAO extends GenericDAO<BancoFormapagamento>{
	
	public List<BancoFormapagamento> findByBanco(Banco banco){
		return query().where("banco = ?", banco).orderBy("descricao").list();
	}
	
	public BancoFormapagamento findByFormaPagamento(BancoFormapagamento bean){
		if (bean == null){
			throw new SinedException("A forma de pagamento deve ser escolhida.");
		}
		return query()
					.where("bancoFormapagamento = ?", bean)
					.unique();
	}
	

}
