package br.com.linkcom.sined.geral.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Oportunidadehistorico;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.modulo.crm.controller.report.filter.IndicadoroportunidadeFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OportunidadehistoricoDAO extends GenericDAO<Oportunidadehistorico>{
	
	private ArquivoDAO arquivoDAO;
	private EmpresaService empresaService;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				
				Oportunidadehistorico oportunidadehistorico = (Oportunidadehistorico) save.getEntity();
				if(oportunidadehistorico.getArquivo() != null && oportunidadehistorico.getArquivo().getCdarquivo() == null){
					arquivoDAO.saveFile(oportunidadehistorico, "arquivo");
				}
				
				return null;
			}
		});
	}
	
	public List<Oportunidadehistorico> findByOportunidade(Oportunidade oportunidade, Atividadetipo atividadetipo, Empresa empresa){
		String whereInEmpresas = empresa != null? empresa.getCdpessoa().toString():
			CollectionsUtil.listAndConcatenate(empresaService.findByUsuario(), "cdpessoa", ",");
		return 
			query()
				.select("oportunidadehistorico.cdoportunidadehistorico, oportunidadehistorico.observacao, " +
						"oportunidadehistorico.dtaltera, oportunidadehistorico.cdusuarioaltera, oportunidade.cdoportunidade," +
						"oportunidadehistorico.probabilidade, oportunidadesituacao.cdoportunidadesituacao, oportunidadesituacao.nome, " +
						"contacrmcontato.nome, contacrmcontato.cdcontacrmcontato, meiocontato.cdmeiocontato, meiocontato.nome, " +
						"atividadetipo.cdatividadetipo, atividadetipo.nome, empresa.nome")
				.leftOuterJoin("oportunidadehistorico.oportunidade oportunidade")
				.leftOuterJoin("oportunidadehistorico.oportunidadesituacao oportunidadesituacao")
				.leftOuterJoin("oportunidadehistorico.contacrmcontato contacrmcontato")
				.leftOuterJoin("oportunidadehistorico.meiocontato meiocontato")
				.leftOuterJoin("oportunidadehistorico.atividadetipo atividadetipo")
				.leftOuterJoin("oportunidadehistorico.empresahistorico empresa")
				.where("oportunidade=?", oportunidade)
				.where("atividadetipo=?", atividadetipo)
				.openParentheses()
					.where("empresa.cdpessoa is null")
					.or()
					.whereIn("empresa.cdpessoa", whereInEmpresas)
				.closeParentheses()
				.list();
	}
	
	/**
	 * Retornar os dados para o relat�rio de acordo com o filtro
	 * @author Taidson Santos
	 * @since 30/03/2010
	 * @param filtro
	 */
	public List<Oportunidadehistorico> historicos(IndicadoroportunidadeFiltro filtro) throws Exception{
		String dtInicio;
		String dtFim;
		Date dataDe;
		Date dataAte;
		if(filtro.getDtPeriodoDe() != null && filtro.getDtPeriodoAte() != null){
			DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			dtInicio = format.format(filtro.getDtref1());
			dtFim = format.format(filtro.getDtref2());
				
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");  
			     
			dataDe = sdf.parse(dtInicio);  
			dataAte = sdf.parse(dtFim);  
			dataAte = SinedDateUtils.dataToEndOfDay(dataAte);
		}else{
			throw new SinedException("Erro na passagem de par�metro.");
		}

		return 
		query()
		.select("oportunidadehistorico.cdoportunidadehistorico, oportunidadehistorico.probabilidade, " +
				"oportunidadehistorico.dtaltera, oportunidade.cdoportunidade, situacao.cdoportunidadesituacao, " +
				"situacao.nome")
		.join("oportunidadehistorico.oportunidade oportunidade")
		.join("oportunidadehistorico.oportunidadesituacao situacao")
		.where("oportunidadehistorico.dtaltera >= ? ", dataDe)
		.where("oportunidadehistorico.dtaltera <= ? ", dataAte)
		.orderBy("oportunidadehistorico.dtaltera, oportunidade.cdoportunidade")
		.list();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Oportunidadehistorico> query) {
		query()
			.select("oportunidadehistorico.cdusuarioaltera, oportunidadehistorico.dtaltera, oportunidadehistorico.cdoportunidadehistorico, " +
					"oportunidadehistorico.observacao, oportunidade.cdoportunidade, oportunidadesituacao.cdoportunidadesituacao, oportunidadesituacao.nome, " +
					"oportunidadehistorico.probabilidade, contacrmcontato.cdcontacrmcontato, meiocontato.cdmeiocontato, atividadetipo.cdatividadetipo, " +
					"arquivo.cdarquivo, arquivo.nome")
			.leftOuterJoin("oportunidadehistorico.oportunidadesituacao oportunidadesituacao")
			.leftOuterJoin("oportunidadehistorico.arquivo arquivo")
			.leftOuterJoin("oportunidadehistorico.oportunidade oportunidade")
			.leftOuterJoin("oportunidadehistorico.contacrmcontato contacrmcontato")
			.leftOuterJoin("oportunidadehistorico.meiocontato meiocontato")
			.leftOuterJoin("oportunidadehistorico.atividadetipo atividadetipo")
			;
	}
	
	/**
	 * Insere historico quando da cria��o de um agendamento originado de uma oportunidade.
	 * @param oportunidadehistorico
	 * @author Taidson
	 * @since 10/09/2010
	 */
	public void insertHistoricoForAgendamento(Oportunidadehistorico oportunidadehistorico) {
		String sql = "INSERT INTO Oportunidadehistorico (cdoportunidadehistorico, cdoportunidade, " +
				"cdusuarioaltera, dtaltera, observacao, cdoportunidadesituacao) " +
					 "VALUES (nextval('sq_oportunidadehistorico'),"+oportunidadehistorico.getOportunidade().getCdoportunidade()+", "+
					 oportunidadehistorico.getCdusuarioaltera()+", '"+oportunidadehistorico.getDtaltera()+"', '"+
					 oportunidadehistorico.getObservacao()+"', "+oportunidadehistorico.getOportunidadesituacao().getCdoportunidadesituacao()+");";
		getJdbcTemplate().execute(sql);
	}
}
