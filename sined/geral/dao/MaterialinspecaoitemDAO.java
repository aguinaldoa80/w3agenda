package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialinspecaoitem;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialinspecaoitemDAO extends GenericDAO<Materialinspecaoitem> {

	public List<Materialinspecaoitem> findItensDosMateriais(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("O whereIn de materiais n�o pode ser nulo.");
		}
		return query()
			.select("materialinspecaoitem.cdmaterialinspecaoitem, material.cdmaterial, inspecaoitem.cdinspecaoitem")
			.join("materialinspecaoitem.material material")
			.join("materialinspecaoitem.inspecaoitem inspecaoitem")
			.whereIn("material.cdmaterial", whereIn)
			.list();
	}
	
	/**
	 * 
	 * M�todo que busca uma lista de Materialnspecao por Material.
	 *
	 * @name findListaMaterialInspecaoByMaterial
	 * @param material
	 * @return
	 * @return List<Materialinspecaoitem>
	 * @author Thiago Augusto
	 * @date 04/07/2012
	 *
	 */
	public List<Materialinspecaoitem> findListaMaterialInspecaoByMaterial(Material material) {
		if(material == null || material.getCdmaterial().equals("")){
			throw new SinedException("O material n�o pode ser nulo.");
		}
		return query()
		.select("materialinspecaoitem.cdmaterialinspecaoitem, material.cdmaterial, inspecaoitem.cdinspecaoitem, inspecaoitem.nome, inspecaoitem.descricao, inspecaoitem.visual ")
		.join("materialinspecaoitem.material material")
		.join("materialinspecaoitem.inspecaoitem inspecaoitem")
		.where("material = ? ", material)
		.list();
	}

}
