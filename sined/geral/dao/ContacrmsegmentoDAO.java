package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmsegmento;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContacrmsegmentoDAO extends GenericDAO<Contacrmsegmento>{
	
public List<Contacrmsegmento> findByContacrm(Contacrm contacrm){
		
		if(contacrm == null || contacrm.getCdcontacrm() == null){
			throw new SinedException("O par�metro contacrm e cdcontacrm n�o podem ser null.");
		}
		return 
			query()
				.select("segmento.nome, segmento.cdsegmento, contacrm.cdcontacrm")
				.join("contacrmsegmento.segmento segmento")
				.join("contacrmsegmento.contacrm contacrm")
				.where("contacrm = ?",contacrm)
				.orderBy("contacrm.cdcontacrm")
				.list();
		
	}

}
