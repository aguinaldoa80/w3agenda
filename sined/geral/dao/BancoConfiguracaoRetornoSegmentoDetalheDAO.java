package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetorno;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoSegmentoDetalhe;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class BancoConfiguracaoRetornoSegmentoDetalheDAO extends GenericDAO<BancoConfiguracaoRetornoSegmentoDetalhe>  {

	public List<BancoConfiguracaoRetornoSegmentoDetalhe> findByBancoConfiguracaoRetorno(BancoConfiguracaoRetorno bcr){
		
		return query()
					.select("bancoConfiguracaoRetornoSegmentoDetalhe.cdBancoConfiguracaoRetornoSegmentoDetalhe, bancoConfiguracaoRetornoSegmentoDetalhe.dtaltera, bancoConfiguracaoRetorno.cdbancoconfiguracaoretorno")
					.leftOuterJoin("bancoConfiguracaoRetornoSegmentoDetalhe.bancoConfiguracaoRetorno bancoConfiguracaoRetorno")
					.where("bancoConfiguracaoRetorno = ?", bcr)
					.list();
	}
}
