package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Formularh;
import br.com.linkcom.sined.geral.bean.Formularhcargo;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FormularhcargoDAO extends GenericDAO<Formularhcargo>{
	
	public List<Formularhcargo> validaCargo(String whereIn, String cargos){
		 QueryBuilder<Formularhcargo> query = query()
		 	.join("formularhcargo.cargo cargo")
		 	.whereIn("formularhcargo.cargo", cargos);
		 if(!whereIn.equals("0") && !"".equals(whereIn))
			 query.where("formularhcargo.cdformularhcargo not in ("+whereIn+")");
		 return query.list();
	}
	
	/**
	 * 
	 * M�todo que carrega a lista de FormulaRhCargo a partir de uma FormulaRh.
	 *
	 *@author Thiago Augusto
	 *@date 20/03/2012
	 * @param formularh
	 * @return
	 */
	public List<Formularhcargo> getListaFormulaRhCargoByFormulaRh(Formularh formularh){
		if(formularh == null || formularh.getCdformularh() == null){
			throw new SinedException("F�rmula RH n�o pode ser nula.");
		}
		return query()
					.select("formularhcargo.cdformularhcargo, cargo.cdcargo, cargo.nome")
					.leftOuterJoin("formularhcargo.formularh formularh")
					.leftOuterJoin("formularhcargo.cargo cargo")
					.where("formularh = ?", formularh)
					.list();
	}
}
