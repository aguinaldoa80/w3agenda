package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Rateioapuracao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.RateioapuracaoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RateioapuracaoDAO extends GenericDAO<Rateioapuracao>{

	@Override
	public void updateListagemQuery(QueryBuilder<Rateioapuracao> query, FiltroListagem _filtro) {
		RateioapuracaoFiltro filtro = (RateioapuracaoFiltro)_filtro;
		
		query
			.select("distinct rateioapuracao.cdrateioapuracao, rateioapuracao.nome, centrocusto.nome, contagerencial.nome, vcontagerencial.identificador, vcontagerencial.arvorepai")
			.leftOuterJoin("rateioapuracao.centrocusto centrocusto")
			.leftOuterJoin("rateioapuracao.contagerencial contagerencial")
			.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			.leftOuterJoin("rateioapuracao.listaRateioapuracaoempresa rateioapuracaoempresa")
			.where("rateioapuracao.centrocusto = ?", filtro.getCentrocusto())
			.where("rateioapuracao.contagerencial = ?", filtro.getContagerencial())
			.where("rateioapuracaoempresa.empresa = ?", filtro.getEmpresa())
			.whereLikeIgnoreAll("rateioapuracao.nome", filtro.getNome())
			.ignoreJoin("rateioapuracaoempresa");
		
		super.updateListagemQuery(query, filtro);
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Rateioapuracao> query) {
		query
			.select("rateioapuracao.cdrateioapuracao, rateioapuracao.nome, " +
					"centrocusto.cdcentrocusto, centrocusto.nome, " +
					"contagerencial.cdcontagerencial, contagerencial.nome, " +
					"vcontagerencial.identificador, vcontagerencial.arvorepai, " +
					"rateioapuracaoempresa.cdrateioapuracaoempresa, rateioapuracaoempresa.percentual, " +
					"empresa.cdpessoa, empresa.nome")
			.leftOuterJoin("rateioapuracao.centrocusto centrocusto")
			.leftOuterJoin("rateioapuracao.contagerencial contagerencial")
			.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			.leftOuterJoin("rateioapuracao.listaRateioapuracaoempresa rateioapuracaoempresa")
			.leftOuterJoin("rateioapuracaoempresa.empresa empresa");
		
		super.updateEntradaQuery(query);
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaRateioapuracaoempresa");
	}
	
	/**
	 * Busca pelo nome da apura��o de rateio.
	 * 	 
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/05/2018
	 */
	public Rateioapuracao findByNome(String nome){
		return query().select("rateioapuracao.cdrateioapuracao, rateioapuracao.nome")
						.where("retira_acento(upper(rateioapuracao.nome)) = ?", Util.strings.tiraAcento(Util.strings.emptyIfNull(nome)).toUpperCase())
						.unique();
	}
}
