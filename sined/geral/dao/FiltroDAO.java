package br.com.linkcom.sined.geral.dao;


import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Filtro;
import br.com.linkcom.sined.geral.bean.enumeration.FiltroTipoEnum;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FiltroDAO extends GenericDAO<Filtro> {

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaFiltropapel", "filtro");
	}

	public List<Filtro> findByTelaPath(String path) {
		QueryBuilder<Filtro> query = querySined()
			
			.select("filtro.cdfiltro, filtro.nome, usuario.cdpessoa")
			.join("filtro.tela tela")
			.leftOuterJoin("filtro.usuario usuario")
			.where("tela.path=?", path)
			.orderBy("upper(filtro.nome) asc");
		
		if(!SinedUtil.isUsuarioLogadoAdministrador()){
			query
			.leftOuterJoin("filtro.listaFiltropapel listaFiltropapel")
			.leftOuterJoin("listaFiltropapel.papel papel")
			.openParentheses()
				.openParentheses()
					.where("filtro.tipo = ?", FiltroTipoEnum.INDIVIDUAL)
					.where("filtro.usuario = ?", SinedUtil.getUsuarioLogado())
				.closeParentheses()
				.or()
				.openParentheses()
					.where("filtro.tipo = ?", FiltroTipoEnum.COMPARTILHADO)
					.whereIn("papel.cdpapel", SinedUtil.getWhereInCdPapel())
				.closeParentheses()
			.closeParentheses();
		}
		
		return query.list();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Filtro> query) {
		query.leftOuterJoinFetch("filtro.listaFiltropapel listaFiltropapel");
	}
}
