package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.CusteioProcessado;
import br.com.linkcom.sined.geral.bean.CusteioTipoDocumento;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;


public class CusteioTipoDocumentoDAO  extends GenericDAO<CusteioTipoDocumento> {

	public List<CusteioTipoDocumento> findByCusteioProcessado(CusteioProcessado bean) {
		return query()
				.select("custeioTipoDocumento.cdCusteioTipoDocumento,custeioTipoDocumento.tipoDocumentoCusteioEnum," +
					"custeioTipoDocumento.codObjetoOrigem,custeioTipoDocumento.codRateioOrigem,custeioTipoDocumento.codRateioProcessado," +
					"custeioProcessado.cdCusteioProcessado")
				.leftOuterJoin("custeioTipoDocumento.custeioProcessado custeioProcessado")
				.where("custeioTipoDocumento.custeioProcessado = ?", bean)
				.list();
	}
	
	
}
