package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.ImagensEcommerce;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ImagensEcommerceDAO extends GenericDAO<ImagensEcommerce>{

	public Long loadCdFileByHash(String hash) {
		if(hash ==  null)
			throw new SinedException("Par�metro pessoa inv�lido.");
		
		return query()
				.select("arquivo.cdarquivo")
				.join("imagensEcommerce.arquivo arquivo")
				.whereLike("imagensEcommerce.hash", hash)
				.unique().getArquivo().getCdarquivo();
	}



}
