package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Inventario;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioTipo;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SintegraFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.SagefiscalFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.InventarioFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("inventario.cdinventario")
public class InventarioDAO extends GenericDAO<Inventario>{
	
	private MaterialcategoriaService materialcategoriaService;
	
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {this.materialcategoriaService = materialcategoriaService;}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Inventario> query) {
		query
			.select("inventario.cdinventario,inventario.correcao, inventario.dtinventario, inventario.motivoinventario, " +
					"inventario.codigocontaanalitica, inventario.inventarioTipo, inventario.inventarioSituacao, " +
					"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, empresa.cdpessoa, empresa.nome, empresa.nomefantasia, " +
					"origem.cdinventario, projeto.cdprojeto, projeto.nome")
			.leftOuterJoin("inventario.localarmazenagem localarmazenagem")
			.leftOuterJoin("inventario.projeto projeto")
			.leftOuterJoin("inventario.empresa empresa")
			.leftOuterJoin("inventario.inventarioOrigem origem");			
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Inventario> query,	FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		InventarioFiltro filtro = (InventarioFiltro)_filtro;
		
		query
			.select("distinct inventario.cdinventario, inventario.dtinventario, empresa.cdpessoa, empresa.razaosocial, empresa.nomefantasia, inventario.inventarioTipo, inventario.inventarioSituacao")
			.leftOuterJoin("inventario.empresa empresa")
			.where("inventario.inventarioTipo = ?", filtro.getInventarioTipo())
			.where("inventario.empresa = ?", filtro.getEmpresa())
			.orderBy("inventario.cdinventario desc");
		
		if (filtro.getDtinventario() != null) {
			query
				.where("inventario.dtinventario >= ?", filtro.getDtinventario())
				.where("inventario.dtinventario <= ?", SinedDateUtils.lastDateOfMonth(filtro.getDtinventario()));
		}
		
		if(filtro.getMaterial() != null){
			query
				.joinIfNotExists("inventario.listaInventariomaterial listaInventariomaterial")
				.where("listaInventariomaterial.material = ?", filtro.getMaterial())
				.ignoreJoin("listaInventariomaterial", "material");
		}
		
		if(filtro.getMaterialcategoria() != null){
			materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
			query
				.joinIfNotExists("inventario.listaInventariomaterial listaInventariomaterial")
				.leftOuterJoinIfNotExists("material.materialcategoria materialcategoria")
				.leftOuterJoinIfNotExists("materialcategoria.vmaterialcategoria vmaterialcategoria")
				.openParentheses()
					.where("vmaterialcategoria.identificador like ? ||'.%'", filtro.getMaterialcategoria().getIdentificador())
				.or()
					.where("vmaterialcategoria.identificador like ? ", filtro.getMaterialcategoria().getIdentificador())
				.closeParentheses()
				.ignoreJoin("materialcategoria", "vmaterialcategoria");
		}
		
		if(filtro.getListaInventarioSituacao() != null) {
			query.whereIn("inventario.inventarioSituacao", InventarioSituacao.listAndConcatenate(filtro.getListaInventarioSituacao()));
		}
	}

	public List<Inventario> findForSpedRegistroH010(SpedarquivoFiltro filtro) {
		if(filtro == null || filtro.getInventario() == null || filtro.getInventario().size() == 0)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
					.select("inventario.cdinventario, listaInventariomaterial.indicadorpropriedade, inventario.dtinventario, listaInventariomaterial.qtde, " +
							"material.cdmaterial, material.identificacao, material.tipoitemsped, unidademedida.cdunidademedida, unidademedida.simbolo, material.valorcusto, material.valorvenda," +
							"pessoa.cdpessoa, inventario.motivoinventario, inventario.codigocontaanalitica, " +
							"listaInventariomaterial.valorUnitario, listaInventariomaterial.contaanaliticacontabil, listaInventariomaterial.gerarBlocoK,listaInventariomaterial.gerarBlocoH, " +
							"empresa.cdpessoa, empresa.nome ")
					.leftOuterJoin("inventario.listaInventariomaterial listaInventariomaterial")
					.leftOuterJoin("listaInventariomaterial.material material")
					.leftOuterJoin("material.unidademedida unidademedida")
					.leftOuterJoin("listaInventariomaterial.pessoa pessoa")
					.leftOuterJoin("inventario.empresa empresa")
					.whereIn("inventario.cdinventario", CollectionsUtil.listAndConcatenate(filtro.getInventario(), "cdinventario", ","))
					.list();
	}

	
	/**
	 * Busca os registros de invent�rio para o SPED
	 *
	 * @param filtro
	 * @return
	 */
	public List<Inventario> findForSpedRegistroK200(SpedarquivoFiltro filtro) {
		if(filtro == null || filtro.getInventarioEstoque() == null || filtro.getInventarioEstoque().size() == 0)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
					.select("inventario.cdinventario, listaInventariomaterial.indicadorpropriedade, inventario.dtinventario, listaInventariomaterial.qtde, material.cdmaterial, material.identificacao, material.tipoitemsped," +
							"pessoa.cdpessoa,listaInventariomaterial.tipopessoainventario ")
					.leftOuterJoin("inventario.listaInventariomaterial listaInventariomaterial")
					.leftOuterJoin("listaInventariomaterial.material material")
					.leftOuterJoin("listaInventariomaterial.pessoa pessoa")
					.whereIn("inventario.cdinventario", CollectionsUtil.listAndConcatenate(filtro.getInventarioEstoque(), "cdinventario", ","))
					.list();
	}
	
	/**
	 * M�todo que busca os dados do invent�rio para o registro74 do SIntegra
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Inventario findForSIntegraRegistro74(SintegraFiltro filtro) {
		if(filtro == null || filtro.getInventario() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		
		return query()
				.select("inventario.dtinventario, inventariomaterial.indicadorpropriedade, inventariomaterial.qtde, " +
						"material.cdmaterial, material.nome, material.valorcusto, unidademedida.cdunidademedida, " +
						"unidademedida.simbolo, inventariomaterial.valorUnitario, " +
						"inventariomaterial.tipopessoainventario, pessoa.cdpessoa, pessoa.cpf, pessoa.cnpj ")
				.join("inventario.listaInventariomaterial inventariomaterial")
				.join("inventariomaterial.material material")
				.join("material.unidademedida unidademedida")
				.leftOuterJoin("inventariomaterial.pessoa pessoa")
				.where("inventario = ?", filtro.getInventario())
				.unique();
	}
	
	public Inventario findForSagefiscal(SagefiscalFiltro filtro) {
		if(filtro == null || filtro.getInventario() == null || filtro.getInventario().getCdinventario() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
		.select("inventario.cdinventario, listaInventariomaterial.indicadorpropriedade, inventario.dtinventario, listaInventariomaterial.qtde, " +
							"material.cdmaterial, material.tipoitemsped, material.valorcusto, material.valorvenda," +
							"inventario.motivoinventario, inventario.codigocontaanalitica,inventario.dtinventariofim " +
							"listaInventariomaterial.valorUnitario, listaInventariomaterial.contaanaliticacontabil")
					.leftOuterJoin("inventario.listaInventariomaterial listaInventariomaterial")
					.leftOuterJoin("listaInventariomaterial.material material")
					.where("inventario = ?", filtro.getInventario())
					.unique();
	}
	
	/**
	 * M�todo que busca os dados do invent�rio para determinada empresa
	 *
	 * @param empresa
	 * @return
	 * @author Lucas Costa
	 */
	public List<Inventario> findByEmpresa(Empresa empresa, Date dtinventario,InventarioTipo inventarioTipo) {

		return query()
				.select("inventario.cdinventario, inventario.dtinventario,inventario.inventarioTipo")
				.openParentheses()
					.where("empresa = ?", empresa)
					.or()
					.where("inventario.empresa is null")
				.closeParentheses()
				.where("inventario.dtinventario = ?", dtinventario)
				.where("inventario.inventarioTipo = ? ", inventarioTipo)
				.where("inventario.inventarioSituacao = ?", InventarioSituacao.AUTORIZADO)
				.orderBy("inventario.dtinventario desc,inventario.cdinventario desc")
				.list();
	}
	
	public List<Inventario> findForSpedRegistroK280(SpedarquivoFiltro filtro) {
		if(filtro == null || filtro.getInventarioCorrecao() == null || filtro.getInventarioCorrecao().size() == 0){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		
		return query()
					.select("inventario.cdinventario, inventario.dtinventario,inventario.dtinventario,inventario.dtinventariofim, " +
							"listaInventariomaterial.qtde,listaInventariomaterial.qtdeOrigem,listaInventariomaterial.cdinventariomaterial,listaInventariomaterial.indicadorpropriedade," +
							" material.cdmaterial, material.identificacao, material.tipoitemsped,pessoa.cdpessoa,listaInventariomaterial.tipopessoainventario ")
					.leftOuterJoin("inventario.listaInventariomaterial listaInventariomaterial")
					.leftOuterJoin("listaInventariomaterial.material material")
					.leftOuterJoin("listaInventariomaterial.pessoa pessoa")
					.whereIn("inventario.cdinventario", CollectionsUtil.listAndConcatenate(filtro.getInventarioCorrecao(), "cdinventario", ","))
					.list();
	}
	
	/**
	 * Verifica se existe algum estoque escriturado no m�s e posterior a data informada para o bloqueio das opera��es.
	 *
	 * @param data
	 * @param empresa
	 * @param localarmazenagem
	 * @param projeto
	 * @return
	 * @since 29/10/2019
	 * @author Rodrigo Freitas
	 */
	public boolean hasEstoqueEscrituradoNotCanceladoPosteriorDataByEmpresaLocalProjeto(Date data, Empresa empresa, Localarmazenagem localarmazenagem, Projeto projeto){
		return newQueryBuilderWithFrom(Long.class)
					.select("count(*)")
					.openParentheses()
						.where("inventario.empresa = ?", empresa)
						.or()
						.where("inventario.empresa is null")
					.closeParentheses()
					.openParentheses()
						.where("inventario.localarmazenagem = ?", localarmazenagem)
						.or()
						.where("inventario.localarmazenagem is null")
					.closeParentheses()
					.openParentheses()
						.where("inventario.projeto = ?", projeto)
						.or()
						.where("inventario.projeto is null")
					.closeParentheses()
					.where("inventario.dtinventario >= ?", SinedDateUtils.firstDateOfMonth(data))
					.where("inventario.inventarioSituacao <> ?", InventarioSituacao.CANCELADO)
					.where("inventario.inventarioSituacao = ?", InventarioTipo.ESTOQUEESTRUTURADO)
					.unique() > 0;
	}
	
	public boolean hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(Date data, Empresa empresa, Localarmazenagem localarmazenagem, Projeto projeto, InventarioTipo  inventarioTipo){
		return newQueryBuilderWithFrom(Long.class)
					.select("count(*)")
					.openParentheses()
						.where("inventario.empresa = ?", empresa)
						.or()
						.where("inventario.empresa is null")
					.closeParentheses()
					.openParentheses()
						.where("inventario.localarmazenagem = ?", localarmazenagem)
						.or()
						.where("inventario.localarmazenagem is null")
					.closeParentheses()
					.openParentheses()
						.where("inventario.projeto = ?", projeto)
						.or()
						.where("inventario.projeto is null")
					.closeParentheses()
					.where("inventario.inventarioTipo = ?", inventarioTipo)
					.where("inventario.dtinventario >= ?", SinedDateUtils.firstDateOfMonth(data))
					.where("inventario.inventarioSituacao <> ?", InventarioSituacao.CANCELADO)
					.unique() > 0;
	}

	public Inventario buscarInventarioOrigem(Inventario inventarioAux) {
		return query()
				.select("inventario.cdinventario,inventarioOrigem.cdinventario ")
				.leftOuterJoin("inventario.inventarioOrigem inventarioOrigem")
				.where("inventario = ?", inventarioAux)
				.unique();
	}
	
	public List<Inventario> findForListagemReport(InventarioFiltro filtro) {
		QueryBuilder<Inventario> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("inventario.cdinventario");
		return query.list();
	}

	/**
	 * Verifica se h� algum invent�rio que n�o esteja nas situa��es passadas por par�metro.
	 *
	 * @param whereIn
	 * @param situacoes
	 * @return
	 * @since 04/10/2019
	 * @author Rodrigo Freitas
	 */
	public boolean hasNotAtSituacao(String whereIn, InventarioSituacao[] situacoes) {
		QueryBuilder<Long> query = newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.whereIn("inventario.cdinventario", whereIn);
		
		if(situacoes != null && situacoes.length > 0){
			for (InventarioSituacao inventarioSituacao : situacoes) {
				query.where("inventario.inventarioSituacao <> ?", inventarioSituacao);
			}
		}
	
		return query.unique() > 0;
	}

	/**
	 * Atualiza a situa��o dos invent�rios.
	 *
	 * @param whereIn
	 * @param inventarioSituacao
	 * @since 04/10/2019
	 * @author Rodrigo Freitas
	 */
	public void atualizaSituacao(String whereIn, InventarioSituacao inventarioSituacao) {
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Par�metro inv�lido.");
		}
		getHibernateTemplate().bulkUpdate("update Inventario i set i.inventarioSituacao = ? where i.cdinventario in (" + whereIn + ")", inventarioSituacao);
	}

	/**
	 * Busca os invent�rios de ajuste que foram criados a partir de uma origem.
	 *
	 * @param inventarioOrigem
	 * @return
	 * @since 14/10/2019
	 * @author Rodrigo Freitas
	 */
	public List<Inventario> findAjustesByOrigem(Inventario inventarioOrigem) {
		if(inventarioOrigem == null || inventarioOrigem.getCdinventario() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
					.select("inventario.cdinventario, inventariomaterial.cdinventariomaterial, " +
							"inventariomaterial.qtde, material.cdmaterial, loteestoque.cdloteestoque")
					.join("inventario.listaInventariomaterial inventariomaterial")
					.join("inventariomaterial.material material")
					.join("inventariomaterial.loteestoque loteestoque")
					.where("inventario.inventarioOrigem = ?", inventarioOrigem)
					.where("inventario.inventarioSituacao <> ?", InventarioSituacao.CANCELADO)
					.orderBy("inventario.cdinventario desc")
					.list();
	}

	/**
	 * Verifica se existe algum invent�rio posterior ao invent�rio passado por par�metro.
	 * @param inventario
	 * @return
	 * @since 18/10/2019
	 * @author Rodrigo Freitas
	 */
	public boolean hasInventarioPosterior(Inventario inventario) {
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.openParentheses()
					.where("inventario.localarmazenagem = ?", inventario.getLocalarmazenagem())
					.or()
					.where("inventario.localarmazenagem is null")
				.closeParentheses()
				.openParentheses()
					.where("inventario.empresa = ?", inventario.getEmpresa())
					.or()
					.where("inventario.empresa is null")
				.closeParentheses()
				.openParentheses()
					.where("inventario.projeto = ?", inventario.getProjeto())
					.or()
					.where("inventario.projeto is null")
				.closeParentheses()
				.where("inventario.dtinventario > ?", inventario.getDtinventario())
				.where("inventario.inventarioSituacao <> ?", InventarioSituacao.CANCELADO)
				.unique() > 0;
	}

	public List<Inventario> carregarInventario(String whereIn) {
		if (whereIn == null || StringUtils.isEmpty(whereIn)) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("inventario.cdinventario, inventario.dtinventario, inventario.inventarioTipo, inventario.inventarioSituacao, " +
						"localarmazenagem.cdlocalarmazenagem, projeto.cdprojeto, empresa.cdpessoa ")
				.leftOuterJoin("inventario.localarmazenagem localarmazenagem")
				.leftOuterJoin("inventario.projeto projeto")
				.leftOuterJoin("inventario.empresa empresa")
				.whereIn("inventario.cdinventario", whereIn)
				.list();
	}

	public Boolean existeInveitarioSubsequente(Inventario inventario) {
		if (inventario == null || inventario.getDtinventario() == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		Date firstDate = SinedDateUtils.firstDateOfMonth(SinedDateUtils.addMesData(inventario.getDtinventario(), 1));
		Date lastDate = SinedDateUtils.lastDateOfMonth(SinedDateUtils.addMesData(inventario.getDtinventario(), 1));
		
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.openParentheses()
					.where("inventario.localarmazenagem = ?", inventario.getLocalarmazenagem())
					.or()
					.where("inventario.localarmazenagem is null")
				.closeParentheses()
				.openParentheses()
					.where("inventario.empresa = ?", inventario.getEmpresa())
					.or()
					.where("inventario.empresa is null")
				.closeParentheses()
				.openParentheses()
					.where("inventario.projeto = ?", inventario.getProjeto())
					.or()
					.where("inventario.projeto is null")
				.closeParentheses()
				.where("inventario.inventarioTipo <> ?", InventarioTipo.CORRECAO)
				.where("inventario.inventarioTipo = ?", inventario.getInventarioTipo())
				.where("inventario.inventarioSituacao <> ?", InventarioSituacao.CANCELADO)
				.wherePeriodo("inventario.dtinventario", firstDate, lastDate)
				.unique() > 0;
	}
}