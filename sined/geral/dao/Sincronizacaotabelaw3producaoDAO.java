package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.sined.geral.bean.Sincronizacaotabelaw3producao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.rest.w3producao.downloadw3pneubean.DownloadBeanW3producaoRESTWSBean;

public class Sincronizacaotabelaw3producaoDAO extends GenericDAO<Sincronizacaotabelaw3producao>{

	public List<Sincronizacaotabelaw3producao> findForW3producao(DownloadBeanW3producaoRESTWSBean command, Boolean excluido) {
		if(command == null || command.getDataUltimaAtualizacao() == null){
			throw new SinedException("A data da ultima atualizacao n�o pode ser nula");
		}
		return query()
				.where("sincronizacaotabelaw3producao.dtalteracao >= ?", new Timestamp(command.getDataUltimaAtualizacao()))
				.where("coalesce(sincronizacaotabelaw3producao.excluido, false) = ?", excluido)
				.list();
	}
	
}
