package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.SaldoIcms;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SaldoIcmsFiltro;

public class SaldoIcmsDAO extends GenericDAO<SaldoIcms> {

	@Override
	public void updateListagemQuery(QueryBuilder<SaldoIcms> query, FiltroListagem _filtro) {
		SaldoIcmsFiltro filtro = (SaldoIcmsFiltro) _filtro;

		query
			.select("saldoIcms.mesAno, saldoIcms.valor, empresa.cdpessoa, empresa.nomefantasia, saldoIcms.tipoUtilizacaoCredito ")
			.leftOuterJoin("saldoIcms.empresa empresa");

		if (filtro.getEmpresa() != null) {
			query.where("empresa = ?", filtro.getEmpresa());
		}

		if (filtro.getValor() != null) {
			query.where("saldoIcms.valor = ?", filtro.getValor());
		}
		
		if (filtro.getMesAno() != null) {
			query.where("saldoIcms.mesAno = ?", filtro.getMesAno());
		}
		
		if (filtro.getTipoUtilizacaoCredito() != null) {
			query.where("saldoIcms.tipoUtilizacaoCredito = ?", filtro.getTipoUtilizacaoCredito());
		}

		query.orderBy("saldoIcms.mesAno desc");
		query.list();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<SaldoIcms> query) {
		query
			.select("saldoIcms.cdSaldoIcms, saldoIcms.mesAno, saldoIcms.valor, empresa.cdpessoa, empresa.nomefantasia, saldoIcms.tipoUtilizacaoCredito ")
			.leftOuterJoin("saldoIcms.empresa empresa");
	}

	public SaldoIcms getLastMesAnoSaldoIcmsByEmpresa(Empresa empresa, String codAjApur) {
		return query()
				.select("saldoIcms.cdSaldoIcms, saldoIcms.mesAno, saldoIcms.valor, empresa.cdpessoa, empresa.nomefantasia, saldoIcms.tipoUtilizacaoCredito ")
				.leftOuterJoin("saldoIcms.empresa empresa")
				.where("empresa = ?", empresa)
				.where("saldoIcms.tipoUtilizacaoCredito = ?", codAjApur)
				.orderBy("saldoIcms.mesAno desc")
				.setMaxResults(1)
				.unique();
	}

	public SaldoIcms getSaldoIcmsByMesAnoEmpresaTipoUtilizacaoCredito(SaldoIcms bean) {
		return query()
				.select("saldoIcms.cdSaldoIcms, saldoIcms.mesAno, saldoIcms.valor, empresa.cdpessoa, empresa.nomefantasia, saldoIcms.tipoUtilizacaoCredito ")
				.leftOuterJoin("saldoIcms.empresa empresa")
				.where("empresa = ?", bean.getEmpresa())
				.where("saldoIcms.mesAno = ?", bean.getMesAno())
				.where("saldoIcms.tipoUtilizacaoCredito = ?", bean.getTipoUtilizacaoCredito())
				.setMaxResults(1)
				.unique();
	}
}
