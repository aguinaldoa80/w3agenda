package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Periodo;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PeriodoDAO extends GenericDAO<Periodo> {

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaPeriodocargo");
	}

	public void deleteWherePlanejamento(Planejamento planejamento) {
		if (planejamento == null || planejamento.getCdplanejamento() == null) {
			throw new SinedException("Planejamento n�o pode ser nulo.");
		}
		getJdbcTemplate().update("DELETE FROM PERIODO WHERE PERIODO.CDPLANEJAMENTO = ?", new Object[]{planejamento.getCdplanejamento()});		
	}

	public List<Periodo> findByPlanejamento(Planejamento planejamento) {
		if (planejamento == null || planejamento.getCdplanejamento() == null) {
			throw new SinedException("Planejamnto n�o pode ser nulo.");
		}
		return query()
					.select("periodocargo.numero, periodocargo.qtde, cargo.cdcargo, cargo.nome")
					.join("periodo.listaPeriodocargo periodocargo")
					.join("periodo.cargo cargo")
					.where("periodo.planejamento = ?",planejamento)
					.orderBy("cargo.nome, periodocargo.numero")
					.list();
	}
	
}
