package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.JoinMode;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.AtributosMaterial;
import br.com.linkcom.sined.geral.bean.Bempatrimonio;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresamodelocontrato;
import br.com.linkcom.sined.geral.bean.Epi;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.ImagensEcommerce;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Localizacaoestoque;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialCustoEmpresa;
import br.com.linkcom.sined.geral.bean.MaterialRateioEstoque;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialcor;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Ncmcapitulo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Pneumedida;
import br.com.linkcom.sined.geral.bean.Pneumodelo;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Servico;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Tabelavalor;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Gradeestoquetipo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoitemsped;
import br.com.linkcom.sined.geral.bean.view.Vgerenciarmaterial;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.geral.service.VendamaterialService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ConferenciaMateriaisVendaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeAtualizacaoMaterialBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConsultarEstoqueBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.AtualizacaoEstoqueMaterialBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.TransferenciaCategoriaMaterialBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.AtualizacaoEstoqueFiltro;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.TransferenciaCategoriaFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.MaterialLoteBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.Materialajustarpreco;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MaterialFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MovimentacaoestoqueFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.SugestaocompraFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.MaterialHistoricoPrecoBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.filter.MaterialHistoricoPrecoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;


@DefaultOrderBy("material.nome")
public class MaterialDAO extends GenericDAO<Material> {

	private MaterialService materialService;
	private ArquivoDAO arquivoDAO;
	private UsuarioService usuarioService;
	private MaterialcategoriaService materialcategoriaService;
	private ParametrogeralService parametroGeralService;
	
	
	public void setParametroGeralService(ParametrogeralService parametroGeralService) {
		this.parametroGeralService = parametroGeralService;
	}

	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}

	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {this.materialcategoriaService = materialcategoriaService;}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Material> query,
			FiltroListagem _filtro) {
		MaterialFiltro filtro = (MaterialFiltro) _filtro;
		
//		Empresa empresaVenda = null;
//		try{
//			String paramEmpresa = NeoWeb.getRequestContext().getParameter("cdempresaFiltroVenda");
//			if(!StringUtils.isEmpty(paramEmpresa)){
//				empresaVenda = new Empresa(Integer.parseInt(paramEmpresa));
//			}
//		}catch (Exception e) {
//			e.printStackTrace();
//		}

		query
				.select(
						"distinct " +
						"material.cdmaterial, material.produto, material.epi, material.patrimonio, material.servico, material.valorcusto," + 
						"material.valorvenda, material.qtdeunidade, material.nome, material.identificacao, material.referencia, " +
						"contagerencial.nome, " +
						"unidademedida.cdunidademedida, unidademedida.simbolo, " +
						"materialtipo.nome, " + 
						"materialgrupo.cdmaterialgrupo, materialgrupo.nome, " +
						"materialcategoria.descricao,"+
						"materialmestregrade.cdmaterial, materialmestregrade.nome");		
		
		// COLOCADO ESSA PARTE DO C�DIGO NA FRENTE PARA OTIMIZA��O
		if(filtro.getQtdeate() != null || filtro.getQtdeate() != null || (filtro.getAbaixominimo() != null && filtro.getAbaixominimo())){
			query
				.join("material.vgerenciarmaterialsoma vgerenciarmaterialsoma")
				.where("vgerenciarmaterialsoma.qtdedisponivel >= ?", filtro.getQtdede())
				.where("vgerenciarmaterialsoma.qtdedisponivel <= ?", filtro.getQtdeate())
				.where("material.qtdeminima is not null", filtro.getAbaixominimo() != null && filtro.getAbaixominimo())
				.where("vgerenciarmaterialsoma.qtdedisponivel < material.qtdeminima", filtro.getAbaixominimo() != null && filtro.getAbaixominimo());
		}
		
		query				
				.join("material.unidademedida unidademedida")
				.join("material.materialgrupo materialgrupo")
				.leftOuterJoin("material.contagerencial contagerencial")
				.leftOuterJoin("material.materialtipo materialtipo")
				.leftOuterJoin("material.materialcategoria materialcategoria")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				
				.whereLikeIgnoreAll("material.nome", filtro.getNome())
				.where("contagerencial = ?", filtro.getContagerencial())
				.where("materialgrupo = ?", filtro.getMaterialgrupo())
				.where("materialtipo = ?", filtro.getMaterialtipo())
				.where("unidademedida = ?", filtro.getUnidademedida())
				.where("material.valorcusto >= ?", filtro.getValorcustode())
				.where("material.valorcusto <= ?", filtro.getValorcustoate())
				.where("material.valorvenda >= ?", filtro.getValorvendade())
				.where("material.valorvenda <= ?", filtro.getValorvendaate())
				.where("material.ativo = ?", filtro.getAtivo())
				.where("material.producao = ?", filtro.getProducao())
				.where("material.vendapromocional = ?", filtro.getKit())
				.whereLike("material.ncmcompleto", filtro.getNcmcompleto())
				.whereLike("material.codigonbs", filtro.getCodigonbs())
				.whereLike("material.codlistaservico", filtro.getCodlistaservico());
				
		if(filtro.getMaterialcategoria() != null){
			materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
			query
				.leftOuterJoin("materialcategoria.vmaterialcategoria vmaterialcategoria")
				.openParentheses()
					.where("vmaterialcategoria.identificador like ? ||'.%'", filtro.getMaterialcategoria().getIdentificador())
				.or()
					.where("vmaterialcategoria.identificador like ? ", filtro.getMaterialcategoria().getIdentificador())
				.closeParentheses();
		}
		
		if(filtro.getMaterialcor() != null){
			query
				.leftOuterJoin("material.materialcor materialcor", true)
				.where("materialcor = ?", filtro.getMaterialcor());
		}
		
		if(filtro.getEmpresa() != null ){
			query
				.leftOuterJoin("material.listaMaterialempresa listaMaterialempresa", true)
				.where("listaMaterialempresa.empresa = ?", filtro.getEmpresa());
//				.where("listaMaterialempresa.empresa = ?", empresaVenda);
		}
		
		if(filtro.getInspecaoitem() != null){
			query
				.leftOuterJoin("material.listaMaterialinspecaoitem listaMaterialinspecaoitem", true)
				.leftOuterJoin("listaMaterialinspecaoitem.inspecaoitem inspecaoitem", true)
				.where("inspecaoitem = ?", filtro.getInspecaoitem());
		}
		
		if(filtro.getCaracteristica() != null && !filtro.getCaracteristica().trim().equals("")){
			query
				.leftOuterJoin("material.listaCaracteristica listaCaracteristica", true)
				.whereLikeIgnoreAll("listaCaracteristica.nome", filtro.getCaracteristica());
		}
		
		if(filtro.getFornecedor() != null && !filtro.getFornecedor().trim().equals("")){
			query
				.leftOuterJoin("material.listaFornecedor listaFornecedor", true)
				.leftOuterJoin("listaFornecedor.fornecedor fornecedor", true)
				.whereLikeIgnoreAll("fornecedor.nome", filtro.getFornecedor());
		}
		
		if(filtro.getGrade() != null){			
			if (filtro.getGrade()) {
				query					
					.where("materialmestregrade is null and " +
							"exists ( select m.cdmaterial " +
							"from Material m " +
							"join m.materialmestregrade mmg " +
							"where mmg.cdmaterial = material.cdmaterial )");
			}else {
				query
					.where("materialmestregrade is not null")
					.where("materialmestregrade = ?", filtro.getMaterialmestregrade());														
			}			
		}
		
		if (filtro.getProduto() || filtro.getServico() || filtro.getEpi() || filtro.getBempatrimonio()) {
			query.openParentheses();
			if (filtro.getProduto()) {
				query.where("material.produto = ?", filtro.getProduto());
			}
			if (filtro.getServico()) {
				if (filtro.getProduto()) {
					query.or();
				}
				query.where("material.servico = ?", filtro.getServico());
			}
			if (filtro.getEpi()) {
				if (filtro.getProduto() || filtro.getServico()) {
					query.or();
				}
				query.where("material.epi = ?", filtro.getEpi());
			}
			if (filtro.getBempatrimonio()) {
				if (filtro.getProduto() || filtro.getServico()
						|| filtro.getEpi()) {
					query.or();
				}
				query.where("material.patrimonio = ?", filtro
						.getBempatrimonio());
			}
			query.closeParentheses();
		}

		if(filtro.getTipo() != null && !filtro.getTipo().equals("") && filtro.getCodigo() != null && !filtro.getCodigo().equals("")){
			if(filtro.getTipo().equals(MaterialFiltro.TODOS)){
				query.leftOuterJoinIfNotExists(new String[]{"material.listaFornecedor listaFornecedor"});
				query.openParentheses();
				query.whereLikeIgnoreAll("material.identificacao", filtro.getCodigo()).or();
				if(verificaDigito(filtro.getCodigo())){
					try{
						query.where("material.cdmaterial = ?", Integer.valueOf(filtro.getCodigo())).or();
					} catch (Exception e) {
					}
				}
				query.whereLikeIgnoreAll("listaFornecedor.codigo", filtro.getCodigo()).or();
				query.whereLikeIgnoreAll("material.referencia", filtro.getCodigo()).or();
				query.whereLikeIgnoreAll("material.codigofabricante", filtro.getCodigo()).or();
				query.whereLikeIgnoreAll("material.codigobarras", filtro.getCodigo()).or();
				query.closeParentheses();
			} else if (filtro.getTipo().equals(MaterialFiltro.IDENTIFICACAO))
				query.whereLikeIgnoreAll("material.identificacao", filtro.getCodigo());
			else if (filtro.getTipo().equals(MaterialFiltro.CODIGO) && verificaDigito(filtro.getCodigo()))
				query.where("material.cdmaterial = ?", Integer.valueOf(filtro.getCodigo()));
			else if (filtro.getTipo().equals(MaterialFiltro.CODIGO_ALTERNATIVO)){
				query.leftOuterJoinIfNotExists(new String[]{"material.listaFornecedor listaFornecedor"});
				query.whereLikeIgnoreAll("listaFornecedor.codigo", filtro.getCodigo());
			} else if (filtro.getTipo().equals(MaterialFiltro.REFERENCIA))
				query.whereLikeIgnoreAll("material.referencia", filtro.getCodigo());
			else if(filtro.getTipo().equals(MaterialFiltro.CODIGO_BARRAS))
				query.whereLikeIgnoreAll("material.codigobarras", filtro.getCodigo());
			else if (filtro.getTipo().equals(MaterialFiltro.CODIGO_FABRICANTE))
				query.whereLikeIgnoreAll("material.codigofabricante", filtro.getCodigo());
		}
		
		if(filtro.getItemGradePadrao() != null){
			if(filtro.getItemGradePadrao()){
				query.where("material.itemGradePadraoMaterialMestre is true");
			}else {
				query.where("material.itemGradePadraoMaterialMestre is not true");
			}
		}
		
		if(NeoWeb.getRequestContext().getParameter("cdempresaFiltroVenda") != null){
			query.openParentheses()
				.where("material.tipoitemsped is null")
				.or()
				.where("material.tipoitemsped <> ?", Tipoitemsped.USO_CONSUMO)
			.closeParentheses();
		}
		
		if(SinedUtil.isRestricaoFornecedorProdutoUsuarioLogado()){
			query.leftOuterJoin("material.listaFornecedor listaFornecedorRestricao");
			query.leftOuterJoin("listaFornecedorRestricao.fornecedor fornecedorRestricao");
			query.leftOuterJoin("fornecedorRestricao.listaColaboradorFornecedor listaColaboradorFornecedor");
			query.leftOuterJoin("listaColaboradorFornecedor.colaborador colaborador");
			query.openParentheses()
				.where("listaFornecedorRestricao is null")
				.or()
				.where("listaColaboradorFornecedor is null")
				.or()
				.where("colaborador = ?", SinedUtil.getUsuarioComoColaborador())
				.closeParentheses();
		}
		
		query.orderBy("material.cdmaterial desc");
	}

	/**
	 * M�todo que busca a listagem de materiais para relat�rio
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	@SuppressWarnings("unchecked")
	public List<Material> findForMaterialReport(MaterialFiltro filtro) {
		String funcaoTiraacento = Neo.getApplicationContext().getConfig()
				.getProperties().getProperty("funcaoTiraacento");

		StringBuilder sql = new StringBuilder();
		sql
				.append("SELECT distinct mg.nome as GRUPONOME, mt.nome as TIPONOME, mcat.descricao as CATEGORIADESCRICAO, m.cdmaterial as CDMATERIAL, m.produto as PRODUTO, m.pesobruto as PESOBRUTO, "
						+ "m.epi as EPI, m.patrimonio as PATRIMONIO, m.servico as SERVICO, m.nome as NOME, cg.nome as CONTANOME, "
						+ "COALESCE(vw.valor, 0) as VALORORDEM, m.valorcusto as VALORCUSTO, m.valorvenda as VALORVENDA, " 
						+ "m.valorvendamaximo as VALORVENDAMAXIMO, m.valorvendaminimo as VALORVENDAMINIMO, vw.nomefornecedor as NOMEFORNECEDOR,"
						+ " vw.dtcriacao as DTORDEMCOMPRA, m.codigofabricante as CODFABRICANTE, m.nomenf as NOMENF,"
						+ "m.identificacao as IDENTIFICADOR, m.referencia as REFERENCIA, m.codigobarras as CODBARRAS, um.nome as UNMEDIDA, "
						+ "mtc.nome as MATCOR, m.qtdeunidade as QTDUNIDADE, le.descricao as LOCESTOQUE, emc.nome as MODCONTRATO, "
						+ "m.qtdereferencia as QTDREFERENCIA,"
						+ "m.tempoentrega as TEMPOENTREGA, m.frete as FRETE, "
						+ "gt.nome as GRUPOTRIBUTACAO, cg2.nome as CONTAVENDA, cc.nome as CUSTOVENDA, "
						+ "m.tipoitemsped as TIPOITEMSPED, nc.descricao as NCMCAP, m.ncmcompleto as NCMCOMPLETO, m.cest as CEST, m.extipi as EXTIPI, "
						+ " m.valorfrete as VALORFRETE, m.codlistaservico as CODLISTASERVICO, mum.fracao as FRACAO, mum.valorunitario as VALORUNITARIO, mum.qtdereferencia as QTDEREFERENCIA"
						+ " FROM Material m " 
						+ "JOIN Materialgrupo mg ON mg.cdmaterialgrupo = m.cdmaterialgrupo ");
						
						
						sql.append("LEFT JOIN Contagerencial cg ON cg.cdcontagerencial = m.cdcontagerencial "
						+ "LEFT JOIN Contagerencial cg2 ON cg2.cdcontagerencial = m.cdcontagerencialvenda "
						+ "LEFT JOIN vwrelatoriomaterial vw ON vw.cdmaterial = m.cdmaterial "
						+ "LEFT JOIN Materialtipo mt ON mt.cdmaterialtipo = m.cdmaterialtipo "
						+ "LEFT JOIN Materialcategoria mcat ON mcat.cdmaterialcategoria = m.cdmaterialcategoria ");
						
						if (StringUtils.isNotEmpty(filtro.getTipo()) && StringUtils.isNotEmpty(filtro.getCodigo()) && (filtro.getTipo().equals(MaterialFiltro.CODIGO_ALTERNATIVO)) || filtro.getTipo().equals(MaterialFiltro.TODOS)){
								sql.append("LEFT JOIN Materialfornecedor mf ON mf.cdmaterial = m.cdmaterial ");
						}
						
						if (StringUtils.isNotEmpty(filtro.getCaracteristica())){
							sql.append("LEFT JOIN Materialcaracteristica mc ON mc.cdmaterial = m.cdmaterial ");
						}

				sql.append("LEFT JOIN Materialinspecaoitem mii ON mii.cdmaterial = m.cdmaterial ");
					if (filtro.getInspecaoitem() != null && filtro.getInspecaoitem().getCdinspecaoitem() != null){
						sql.append("LEFT JOIN Inspecaoitem ii ON ii.cdinspecaoitem = mii.cdinspecaoitem ");
					}
					
				if (filtro.getEmpresa() != null) {		
					sql.append("LEFT JOIN Materialempresa me ON me.cdmaterial = m.cdmaterial ");
				}
				
				if(filtro.getFornecedor() != null && !filtro.getFornecedor().trim().equals("")){
					sql.append("LEFT JOIN Materialfornecedor mfo ON mfo.cdmaterial = m.cdmaterial ");
					sql.append("LEFT JOIN Pessoa fo ON mfo.cdpessoa = fo.cdpessoa ");
				}
				
				sql.append("LEFT JOIN Unidademedida um ON um.cdunidademedida = m.cdunidademedida " 
						+ "LEFT JOIN Materialcor mtc ON mtc.cdmaterialcor = m.cdmaterialcor " 
						+ "LEFT JOIN Localizacaoestoque le ON le.cdlocalizacaoestoque = m.cdlocalizacaoestoque " 
						+ "LEFT JOIN Empresamodelocontrato emc ON emc.cdempresamodelocontrato = m.cdempresamodelocontrato " 
						+ "LEFT JOIN Grupotributacao gt ON gt.cdgrupotributacao = m.cdgrupotributacao " 
						+ "LEFT JOIN Centrocusto cc ON cc.cdcentrocusto = m.cdcentrocustovenda "
						+ "LEFT JOIN Ncmcapitulo nc ON nc.cdncmcapitulo = m.cdncmcapitulo "
						+ "LEFT JOIN Materialunidademedida mum ON mum.cdmaterial = m.cdmaterial AND mum.prioridadevenda = 'TRUE' "
						+ (filtro.getMaterialcategoria() != null ? " LEFT JOIN Vmaterialcategoria vmcat ON vmcat.cdmaterialcategoria = m.cdmaterialcategoria " : "")
						+ "WHERE ");

		if (filtro.getNome() != null && !filtro.getNome().equals(""))
			sql.append("UPPER(" + funcaoTiraacento + "(m.nome)) LIKE '%"
					+ Util.strings.tiraAcento(filtro.getNome()).toUpperCase()
					+ "%' AND ");
		if(filtro.getCodigofabricante() != null && !filtro.getCodigofabricante().equals(""))
			sql.append("UPPER(" + funcaoTiraacento + "(m.codigofabricante)) LIKE '%"
				+Util.strings.tiraAcento(filtro.getCodigofabricante()).toUpperCase()
				+"%'AND ");
		
		if(SinedUtil.isRestricaoEmpresaClienteUsuarioLogado()){
			sql.append(" ((NOT EXISTS(select 1 ");
			sql.append(" 			    from materialempresa ");
			sql.append(" 			   where materialempresa.cdmaterial = m.cdmaterial)) OR ");
			sql.append("       EXISTS(select 1 ");
			sql.append(" 				from materialempresa join usuarioempresa on usuarioempresa.cdempresa = materialempresa.cdpessoa ");
			sql.append(" 														and usuarioempresa.cdusuario = "+SinedUtil.getCdUsuarioLogado());
			sql.append("			   where materialempresa.cdmaterial = m.cdmaterial)) AND ");
		}
		if (StringUtils.isNotEmpty(filtro.getCaracteristica()))
			sql.append("UPPER("
					+ funcaoTiraacento
					+ "(mc.nome)) LIKE '%"
					+ Util.strings.tiraAcento(filtro.getCaracteristica())
							.toUpperCase() + "%' AND ");
		if (filtro.getContagerencial() != null
				&& filtro.getContagerencial().getCdcontagerencial() != null)
			sql.append("m.cdcontagerencial = "
					+ filtro.getContagerencial().getCdcontagerencial()
					+ " AND ");
		if (filtro.getMaterialgrupo() != null
				&& filtro.getMaterialgrupo().getCdmaterialgrupo() != null)
			sql.append("mg.cdmaterialgrupo = "
					+ filtro.getMaterialgrupo().getCdmaterialgrupo() + " AND ");
		if(filtro.getMaterialcategoria() != null
				&& filtro.getMaterialcategoria().getCdmaterialcategoria() != null){
			materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
			sql.append("(vmcat.identificador like '" + filtro.getMaterialcategoria().getIdentificador() + "' ||'.%' OR " +
					"vmcat.identificador like '" + filtro.getMaterialcategoria().getIdentificador() + "' ) AND ");
		}
		if (filtro.getMaterialtipo() != null
				&& filtro.getMaterialtipo().getCdmaterialtipo() != null)
			sql.append("m.cdmaterialtipo = "
					+ filtro.getMaterialtipo().getCdmaterialtipo() + " AND ");
		if (filtro.getInspecaoitem() != null
				&& filtro.getInspecaoitem().getCdinspecaoitem() != null)
			sql.append("ii.cdinspecaoitem = "
					+ filtro.getInspecaoitem().getCdinspecaoitem() + " AND ");
		// if(filtro.getCodigo() != null && filtro.getCodigo() != null)
		// sql.append("m.cdmaterial = "+filtro.getCodigo()+" AND ");
		
		if(filtro.getGrade() != null){
			sql.append(filtro.getGrade() ? "m.cdmaterialmestregrade is null and exists ( " +
																					"select mGrade.cdmaterial " +
																					"from material mGrade " +
																					"where mGrade.cdmaterialmestregrade = m.cdmaterial ) " 
											: "m.cdmaterialmestregrade is not null");
			sql.append(" AND ");
		}
		
		if (StringUtils.isNotEmpty(filtro.getTipo()) && StringUtils.isNotEmpty(filtro.getCodigo())) {
			if (filtro.getTipo().equals(MaterialFiltro.TODOS)){
				Integer codigo = null; 
				try {codigo = Integer.parseInt(filtro.getCodigo());} catch (Exception e){}
				
				sql.append("UPPER("
						+ funcaoTiraacento
						+ "(m.identificacao)) LIKE '%"
						+ Util.strings.tiraAcento(filtro.getCodigo())
								.toUpperCase() + "%' ");
				if(codigo != null) {
					sql.append(" AND m.cdmaterial = " + filtro.getCodigo() + " OR ");
				}else {
					sql.append(" OR ");
				}
				sql.append("UPPER("
						+ funcaoTiraacento
						+ "(mf.codigo)) LIKE '%"
						+ Util.strings.tiraAcento(filtro.getCodigo())
								.toUpperCase() + "%' OR ");
				sql.append("UPPER("
						+ funcaoTiraacento
						+ "(m.referencia)) LIKE '%"
						+ Util.strings.tiraAcento(filtro.getCodigo())
								.toUpperCase() + "%' OR ");
				sql.append("UPPER("
						+ funcaoTiraacento
						+ "(m.codigobarras)) LIKE '%"
						+ Util.strings.tiraAcento(filtro.getCodigo())
						.toUpperCase() + "%' OR ");
				sql.append("UPPER("
						+ funcaoTiraacento
						+ "(m.codigofabricante)) LIKE '%"
						+ Util.strings.tiraAcento(filtro.getCodigo())
								.toUpperCase() + "%' AND ");
			}
			else if (filtro.getTipo().equals(MaterialFiltro.IDENTIFICACAO))
				sql.append("UPPER("
						+ funcaoTiraacento
						+ "(m.identificacao)) LIKE '%"
						+ Util.strings.tiraAcento(filtro.getCodigo())
								.toUpperCase() + "%' AND ");
			else if (filtro.getTipo().equals(MaterialFiltro.CODIGO))
				sql.append("m.cdmaterial = " + filtro.getCodigo() + " AND ");
			else if (filtro.getTipo().equals(MaterialFiltro.CODIGO_ALTERNATIVO))
				sql.append("UPPER("
						+ funcaoTiraacento
						+ "(mf.codigo)) LIKE '%" ///FIXME sdds 
						+ Util.strings.tiraAcento(filtro.getCodigo())
								.toUpperCase() + "%' AND ");
			else if (filtro.getTipo().equals(MaterialFiltro.REFERENCIA))
				sql.append("UPPER("
						+ funcaoTiraacento
						+ "(m.referencia)) LIKE '%"
						+ Util.strings.tiraAcento(filtro.getCodigo())
								.toUpperCase() + "%' AND ");
			else if (filtro.getTipo().equals(MaterialFiltro.CODIGO_BARRAS))
				sql.append("UPPER("
						+ funcaoTiraacento
						+ "(m.codigobarras)) LIKE '%"
						+ Util.strings.tiraAcento(filtro.getCodigo())
						.toUpperCase() + "%' AND ");
			else if (filtro.getTipo().equals(MaterialFiltro.CODIGO_FABRICANTE))
				sql.append("UPPER("
						+ funcaoTiraacento
						+ "(m.codigofabricante)) LIKE '%"
						+ Util.strings.tiraAcento(filtro.getCodigo())
								.toUpperCase() + "%' AND ");
		}
		
		if (filtro.getEmpresa() != null) {
			sql.append("me.cdpessoa = "
					+ filtro.getEmpresa().getCdpessoa() + " AND ");
		}
		if(filtro.getFornecedor() != null && !filtro.getFornecedor().trim().equals("")){
			sql.append("UPPER("
					+ funcaoTiraacento
					+ "(fo.nome)) LIKE '%"
					+ Util.strings.tiraAcento(filtro.getFornecedor())
							.toUpperCase() + "%' AND ");

		}
		if (filtro.getMaterialcor() != null) {
			sql.append("m.cdmaterialcor = "
					+ filtro.getMaterialcor().getCdmaterialcor() + " AND ");
		}
		if (filtro.getUnidademedida() != null) {
			sql.append("m.cdunidademedida = "
					+ filtro.getUnidademedida().getCdunidademedida() + " AND ");
		}
		if (filtro.getValorcustode() != null) {
			sql.append("m.valorcusto >= "
					+ filtro.getValorcustode() + " AND ");
		}
		if (filtro.getValorcustoate() != null) {
			sql.append("m.valorcusto <= "
					+ filtro.getValorcustoate() + " AND ");
		}
		if (filtro.getValorvendade() != null) {
			sql.append("m.valorvenda >= "
					+ filtro.getValorvendade() + " AND ");
		}
		if (filtro.getValorvendaate() != null) {
			sql.append("m.valorvenda <= "
					+ filtro.getValorvendaate() + " AND ");
		}
		if(StringUtils.isNotBlank(filtro.getNcmcompleto())){
			sql.append("UPPER("
					+ funcaoTiraacento
					+ "(m.ncmcompleto)) LIKE '%"  
					+ Util.strings.tiraAcento(filtro.getNcmcompleto())
							.toUpperCase() + "%' AND ");
		}
		if (filtro.getQtdede() != null) {
			sql.append("(select sum(qtdedisponivel) " + 
					" from Vgerenciarmaterial " +
					" where cdmaterial = m.cdmaterial ) >= " + filtro.getQtdede() + " AND ");
		}
		if (filtro.getQtdeate() != null) {
			sql.append("(select sum(qtdedisponivel) " + 
					" from Vgerenciarmaterial " +
					" where cdmaterial = m.cdmaterial ) <= " + filtro.getQtdeate() + " AND ");
		}
		if(filtro.getContagerencial() != null){
			sql.append("m.cdcontagerencial = "
					+ filtro.getContagerencial().getCdcontagerencial() + " AND ");
		}
		if(filtro.getAtivo() != null){
			sql.append("m.ativo = "
					+ filtro.getAtivo() + " AND ");
		}

		if (filtro.getProduto() || filtro.getServico() || filtro.getEpi()
				|| filtro.getBempatrimonio()) {
			sql.append("(");
			if (filtro.getProduto()) {
				sql.append("m.produto = "
						+ (filtro.getProduto() ? "TRUE" : "FALSE"));
			}
			if (filtro.getServico()) {
				if (filtro.getProduto()) {
					sql.append(" OR ");
				}
				sql.append("m.servico = "
						+ (filtro.getServico() ? "TRUE" : "FALSE"));
			}
			if (filtro.getEpi()) {
				if (filtro.getProduto() || filtro.getServico()) {
					sql.append(" OR ");
				}
				sql.append("m.epi = " + (filtro.getEpi() ? "TRUE" : "FALSE"));
			}
			if (filtro.getBempatrimonio()) {
				if (filtro.getProduto() || filtro.getServico()
						|| filtro.getEpi()) {
					sql.append(" OR ");
				}
				sql.append("m.patrimonio = "
						+ (filtro.getBempatrimonio() ? "TRUE" : "FALSE"));
			}
			sql.append(") ");
		}

		if (sql.substring(sql.length() - 7, sql.length() - 1).contains("WHERE"))
			sql.delete(sql.length() - 7, sql.length() - 1);

		sql.append("ORDER BY m.cdmaterial desc");
		SinedUtil.markAsReader();
		List<Material> lista = getJdbcTemplate().query(sql.toString(),
				
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						Unidademedida unidademedida = new Unidademedida();
						unidademedida.setNome(rs.getString("UNMEDIDA"));
						Materialcor materialcor = new Materialcor();
						materialcor.setNome(rs.getString("MATCOR"));
						Localizacaoestoque localizacaoestoque = new Localizacaoestoque();
						localizacaoestoque.setDescricao(rs.getString("LOCESTOQUE"));
						Empresamodelocontrato empresamodelocontrato = new Empresamodelocontrato();
						empresamodelocontrato.setNome(rs.getString("MODCONTRATO"));
						Grupotributacao grupotributacao = new Grupotributacao();
						grupotributacao.setNome(rs.getString("GRUPOTRIBUTACAO"));
						Ncmcapitulo ncmcapitulo = new Ncmcapitulo();
						ncmcapitulo.setDescricao(rs.getString("NCMCAP"));
						Set<Materialunidademedida> listaMaterialUnidadeMedida= new ListSet<Materialunidademedida>(Materialunidademedida.class);	
						Materialunidademedida materialUnidademedida = new Materialunidademedida();
						materialUnidademedida.setFracao(rs.getDouble("FRACAO"));
						materialUnidademedida.setQtdereferencia(rs.getDouble("QTDEREFERENCIA"));
						listaMaterialUnidadeMedida.add(materialUnidademedida);
						
						Tipoitemsped tipoitemsped = null;
						if(rs.getObject("TIPOITEMSPED") != null){
							tipoitemsped = Tipoitemsped.values()[rs.getInt("TIPOITEMSPED")];
						}
						
						Material material = new Material(rs
								.getInt("CDMATERIAL"), rs.getString("NOME"), rs
								.getBoolean("PRODUTO"), rs.getBoolean("EPI"),
								rs.getBoolean("PATRIMONIO"), rs
										.getBoolean("SERVICO"), rs
										.getString("GRUPONOME"), rs
										.getString("TIPONOME"), rs
										.getString("CATEGORIADESCRICAO"), rs
										.getString("CONTANOME"), rs
										.getDouble("VALORORDEM"), rs
										.getDouble("VALORCUSTO"), rs
										.getDouble("VALORVENDA"), rs
										.getDouble("VALORVENDAMAXIMO"), rs
										.getDouble("VALORVENDAMINIMO"), rs
										.getDate("DTORDEMCOMPRA"), rs
										.getString("IDENTIFICADOR"), rs
										.getString("REFERENCIA"), rs
										.getString("CODBARRAS"),rs
										.getString("CODFABRICANTE"), rs
										.getString("NOMENF"),
										unidademedida,
										materialcor, rs
										.getInt("QTDUNIDADE"),
										localizacaoestoque,
										empresamodelocontrato,rs
										.getDouble("QTDREFERENCIA"),rs
										.getInt("TEMPOENTREGA"),  
										//new Money( rs .getDouble("ICMS")),
										//new Money( rs .getDouble("IPI")),
										//new Money( rs .getDouble("LUCRO")),
										//new Money (rs.getDouble("VALORLUCRO")),
										new Money( rs .getDouble("FRETE")),
										new Money( rs .getDouble("VALORFRETE")),
										grupotributacao, rs
										.getString("CONTAVENDA"),rs
										.getString("CUSTOVENDA") ,
										tipoitemsped,
										ncmcapitulo,rs
										.getString("NCMCOMPLETO"),rs
										.getString("CEST"),rs
										.getString("EXTIPI"),rs
										.getString("CODLISTASERVICO"),rs
										.getDouble("FRACAO"),rs
										.getDouble("VALORUNITARIO"),rs
										.getDouble("PESOBRUTO"),
										listaMaterialUnidadeMedida
										);
										
						
						return material;
					}
				});
		return lista;
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Material> query) {
		((QueryBuilderSined<Material>) query).setUseWhereFornecedorEmpresa(true);
		
		query
				.select(
						"material.cdmaterial, material.nome, material.nomenf, material.codigofabricante, material.radcaracteristica, material.radfornecedor, material.identificacao, material.referencia, "
								+ "material.radempresa, material.radinspecao, material.radimposto, material.tempoentrega, material.valorvenda, material.percentualimpostoecf, material.origemproduto, material.nve, "
								+ "material.produto, material.patrimonio, material.epi, material.servico, material.arquivo, unidademedida.cdunidademedida, material.materialcte, material.obrigarlote,  "
								+ "material.dtcustoinicial, material.qtdecustoinicial, material.valorcustoinicial, material.produtodiferenciado, material.codigonbs, material.definirqtdeminimaporlocal, material.tributacaomunicipal, "
								+ "material.codigoAnvisa, material.isento, material.codigoIsencao, material.formaTributacao, material.itemGradePadraoMaterialMestre, material.ecommerce, "
								+ "material.cadastrarCaracteristica, material.cadastrarItemInspecao, material.cadastrarColaboradoresPermissao, material.qtdeEstoqueMinimoEcommerce, "
								+ "material.cadastrarConversaoUnidades, material.cadastrarMateriaisSimilares, material.cadastrarSugestaoVenda, material.cadastrarNumeroSerie, material.embalagem, "
								+ "material.idEcommerce, material.maisInformacoes, material.detalhesTecnicos, material.itensInclusos, material.conexoes, material.precoVendaFantasia, "
								+ "material.qtdeCarrinho, material.precoSobConsulta, material.permiteVendaSite, material.ecommerce_largura, material.ecommerce_comprimento, material.ecommerce_altura, "
								+ "material.qtdeMaximaCarrinho, material.exibirMatrizAtributos, material.contraProposta, material.freteGratis,"
								+ "material.statusUso, material.desconsiderarcodigobarras, material.descontoAVista, modeloDisponibilidade.cdmodelodisponibilidade, modeloDisponibilidade.descricao, "
								+ "material.marketplace, material.somenteParceiros, material.urlVideo, material.idPaiExterno, material.enviarInformacoesEcommerce, "
								+ "contaContabil.cdcontacontabil, contaContabil.nome, "
								+ "contagerencial.cdcontagerencial, contagerencial.nome, contagerencialvenda.cdcontagerencial, contagerencialvenda.nome, "
								+ "materialgrupo.cdmaterialgrupo, materialgrupo.gradeestoquetipo, material.qtdereferencia, vcontagerencial.identificador, vcontagerencialvenda.identificador, "
								+ "materialtipo.cdmaterialtipo, listaCaracteristica.nome, listaCaracteristica.valor, listaCaracteristica.cdmaterialcaracteristica, "
								+ "listaFornecedor.cdmaterialfornecedor, listaFornecedor.codigo, listaFornecedor.tempoentrega, listaFornecedor.fabricante, fornecedor.cdpessoa, fornecedor.nome, listaFornecedor.principal, "
								+ "empresa.cdpessoa, listaMaterialempresa.cdmaterialempresa, grupotributacaolista.cdgrupotributacao, grupotributacaolista.nome, inspecaoitem.cdinspecaoitem, material.cdusuarioaltera, material.dtaltera, material.valorcusto, "
								+ "material.valorvendaminimo, material.valorvendamaximo, material.naoexibirminmaxvenda, material.qtdeunidade, material.vendapromocional, material.exibiritenskitvenda, material.producao, "
								+ "listaMaterialrelacionado.cdmaterialrelacionado, listaMaterialrelacionado.valorvenda, listaMaterialrelacionado.quantidade, listaMaterialrelacionado.desmontarKitRecebimento, "
								+ "listaMaterialrelacionado.exibirKitObservacaoItemNota, listaMaterialrelacionado.porcentagemCustoGeral, listaMaterialrelacionado.porcentagemCustoUnitario, "
								+ "materialpromocao.cdmaterial, materialpromocao.nome, materialpromocao.valorcusto, material.vendaecf, material.kitflexivel, material.tributacaoipi, "
								+ "centrocustovenda.cdcentrocusto, materialcor.cdmaterialcor, material.frete, material.valorcustomateriaprima, material.prazogarantia, "
								+ "listaProducao.ordemexibicao, listaProducao.largura, listaProducao.altura, listaProducao.consumo, listaProducao.preco, " 
								+ "listaProducao.valorconsumo, listaProducao.opcional, listaProducao.discriminarnota, listaProducao.exibirvenda, listaProducao.acompanhabanda, listaProducao.carcaca, " 
//								+ "listaProducao.comprimento,  listaProducao.partespeca,listaProducao.multiplo, listaProducao.larguraparte,"
//								+ "listaProducao.alturaparte,listaProducao.areaparte,listaProducao.mesa, listaProducao.corte,listaProducao.sobra,"
//								+ "listaProducao.partestira,listaProducao.tiras,listaProducao.totalpartes, listaProducao.sobraplaca,listaProducao.pecas,"
//								+ "listaProducao.sobrapartes, listaProducao.aproveitamento, listaProducao.observacao, listaProducao.melhor," 
								+ "listaProducao.cdmaterialproducao,  materialrelacionado.cdmaterial, materialrelacionado.nome, materialrelacionado.identificacao, " 
								+ "materialproducaoformula.cdmaterialproducaoformula, materialproducaoformula.ordem, materialproducaoformula.identificador, materialproducaoformula.formula, "
								+ "materialrelacionado.servico, listaMaterialrelacionado.ordemexibicao, listaMaterialcolaborador.cdmaterialcolaborador, listaMaterialcolaborador.percentual, listaMaterialcolaborador.valor, "
								+ "listaMaterialcolaborador.ativo, colaborador.cdpessoa, colaborador.nome, material.radcolaborador, "
								+ "material.tipoitemsped, material.codigobarras, material.ncmcompleto, material.cest, material.extipi, material.codlistaservico, ncmcapitulo.cdncmcapitulo, material.basecalculost, material.valorst, "
								+ "material.tributacaoestadual, localizacaoestoque.cdlocalizacaoestoque, localizacaoestoque.descricao, empresamodelocontrato.cdempresamodelocontrato, " 
								+ "empresamodelocontrato.nome, material.peso, material.pesobruto, material.idVinculoExterno, " 
								+ "documentotipo.cddocumentotipo, documentotipo.nome, material.valorfrete, material.freteseparadonota, material.ativo, "
								+ "grupotributacao.cdgrupotributacao, grupotributacao.nome, listaMaterialunidademedida.cdmaterialunidademedida, " 
								+ "unidademedidamaterial.cdunidademedida, unidademedidamaterial.nome, listaMaterialunidademedida.fracao, listaMaterialunidademedida.qtdereferencia, listaMaterialunidademedida.valorunitario, " 
								+ "listaMaterialnumeroserie.cdmaterialnumeroserie, listaMaterialnumeroserie.numero, listaMaterialunidademedida.mostrarconversao, listaMaterialunidademedida.valormaximo, listaMaterialunidademedida.valorminimo, "
								+ "listaMaterialunidademedida.considerardimensoesvendaconversao, listaMaterialunidademedida.pesoliquidocalculadopelaformula, " 
								+ "clientenumeroserie.cdpessoa, clientenumeroserie.nome, notafiscalproduto.cdNota, notafiscalproduto.dtEmissao, entrega.dtentrega, entrega.cdentrega, "
								+ "listaMaterialsimilar.cdmaterialsimilar, materialsimilaritem.cdmaterial, materialsimilaritem.nome, " 
								+ "listaMaterialvenda.cdmaterialvenda, materialvendaitem.cdmaterial, materialvendaitem.nome," 
								+ "listaMaterialunidademedida.prioridadevenda, listaMaterialunidademedida.prioridadeproducao, listaMaterialunidademedida.prioridadecompra, material.metaprazomediocompra, material.toleranciaprazocompra, " 
								+ "codigoanp.cdcodigoanp, codigoanp.codigo, material.considerarvendamultiplos, unidademedidaproducao.simbolo, " 
								+ "material.pesoliquidovalorvenda, material.metrocubicovalorvenda, material.valorindenizacao, material.cadastrarFaixaMarkup, "
								+ "materialmestregrade.cdmaterial, materialmestregrade.nome, materialmestregrade.identificacao, "
								+ "materialcoleta.cdmaterial, materialcoleta.nome, materialcoleta.identificacao, producaoetapa.cdproducaoetapa, "
								+ "materialrelacionadoformula.cdmaterialrelacionadoformula, materialrelacionadoformula.ordem, materialrelacionadoformula.identificador, materialrelacionadoformula.formula, "
								+ "materialcategoria.cdmaterialcategoria, materialcategoria.descricao, vmaterialcategoria.identificador, "
								+ "material.exibiritenskitflexivelvenda, "
								+ "listaMaterialkitflexivel.cdmaterialkitflexivel, listaMaterialkitflexivel.valorvenda, listaMaterialkitflexivel.quantidade, "
								+ "materialkit.cdmaterial, materialkit.nome, "
								+ "materialkitflexivelformula.cdmaterialkitflexivelformula, materialkitflexivelformula.ordem, materialkitflexivelformula.identificador, materialkitflexivelformula.formula, " 
								+ "empresaMaterialimposto.cdpessoa, empresaMaterialimposto.nome, empresaMaterialimposto.nomefantasia, " 
								+ "listaMaterialimposto.cdmaterialimposto, listaMaterialimposto.nacionalfederal, listaMaterialimposto.importadofederal, listaMaterialimposto.estadual, listaMaterialimposto.municipal, "
								+ "producaoetapanome.cdproducaoetapanome, producaoetapanome.nome, tributacaoecf.cdtributacaoecf, pneumedida.cdpneumedida, "
								+ "materialacompanhabanda.cdmaterial, materialacompanhabanda.nome, materialacompanhabanda.identificacao, material.profundidadesulco, material.faturamentoVeterinaria, material.qtdeunidadeveterinaria, "
								+ "materialrelacionadomaterialgrupo.cdmaterialgrupo, materialrelacionadomaterialgrupo.coleta,"
								+ "materialRateioEstoque.cdMaterialRateioEstoque," 
								+ "contaGerencialConsumo.cdcontagerencial,contaGerencialConsumo.nome," 
								+ "contaGerencialRequisicao.cdcontagerencial,contaGerencialRequisicao.nome,"
								+ "contaGerencialEntrada.cdcontagerencial,contaGerencialEntrada.nome,"
								+ "contaGerencialVenda.cdcontagerencial,contaGerencialVenda.nome,"
								+ "contaGerencialPerda.cdcontagerencial,contaGerencialPerda.nome,"
								+ "contaGerencialAjusteEntrada.cdcontagerencial,contaGerencialAjusteEntrada.nome,"
								+ "contaGerencialAjusteSaida.cdcontagerencial,contaGerencialAjusteSaida.nome,"
								+ "contaGerencialRomaneioEntrada.cdcontagerencial,contaGerencialRomaneioEntrada.nome,"
								+ "contaGerencialRomaneioSaida.cdcontagerencial,contaGerencialRomaneioSaida.nome,"
								+ "contaGerencialDevolucaoEntrada.cdcontagerencial,contaGerencialDevolucaoEntrada.nome,"
								+ "contaGerencialDevolucaoSaida.cdcontagerencial,contaGerencialDevolucaoSaida.nome,"
								+ "projetoRateio.cdprojeto,projetoRateio.nome, "
								+ "listaMaterialCustoEmpresa.cdMaterialCustoEmpresa, listaMaterialCustoEmpresa.quantidadeReferencia, listaMaterialCustoEmpresa.quantidadeReferencia, "
								+ "listaMaterialCustoEmpresa.dtInicio, listaMaterialCustoEmpresa.custoInicial, listaMaterialCustoEmpresa.custoMedio, listaMaterialCustoEmpresa.valorUltimaCompra, "
								+ "contaContabilMaterialCustoEmpresa.nome, contaContabilMaterialCustoEmpresa.cdcontacontabil, vcontaContabilMaterialCustoEmpresa.identificador, "
								+ "empresaCustoEmpresa.cdpessoa, empresaCustoEmpresa.nomefantasia, "
								+ "listaImagensEcommerce.cdimagensecommerce, listaImagensEcommerce.hash, imagemEcommerce.cdarquivo, imagemEcommerce.nome, listaImagensEcommerce.descricao, "
								+ "listaEmbalagem.cdembalagem, listaEmbalagem.codigoBarras, unidademedidaEmbalagem.cdunidademedida, unidademedidaEmbalagem.nome, listaMaterialAtributosMaterial.cdmaterialatributosmaterial," 
								+"listaMaterialAtributosMaterial.valor, listaMaterialAtributosMaterial.exibir, atributosMaterial.cdatributosmaterial, atributosMaterial.nome")
				.join("material.unidademedida unidademedida")
				.leftOuterJoin("material.contaContabil contaContabil")
				.leftOuterJoin("material.contagerencial contagerencial")
				.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
				.leftOuterJoin("material.localizacaoestoque localizacaoestoque")
				.leftOuterJoin("material.pneumedida pneumedida")
				.leftOuterJoin("material.codigoanp codigoanp")
				.leftOuterJoin("material.modeloDisponibilidade modeloDisponibilidade")
				.leftOuterJoin("material.materialRateioEstoque materialRateioEstoque")
				.leftOuterJoin("materialRateioEstoque.contaGerencialConsumo contaGerencialConsumo")	
				.leftOuterJoin("materialRateioEstoque.contaGerencialRequisicao contaGerencialRequisicao")	
				.leftOuterJoin("materialRateioEstoque.contaGerencialEntrada contaGerencialEntrada")
				.leftOuterJoin("materialRateioEstoque.contaGerencialVenda contaGerencialVenda")
				.leftOuterJoin("materialRateioEstoque.contaGerencialPerda contaGerencialPerda")
				.leftOuterJoin("materialRateioEstoque.contaGerencialAjusteEntrada contaGerencialAjusteEntrada")
				.leftOuterJoin("materialRateioEstoque.contaGerencialAjusteSaida contaGerencialAjusteSaida")
				.leftOuterJoin("materialRateioEstoque.contaGerencialRomaneioEntrada contaGerencialRomaneioEntrada")
				.leftOuterJoin("materialRateioEstoque.contaGerencialRomaneioSaida contaGerencialRomaneioSaida")				
				.leftOuterJoin("materialRateioEstoque.contaGerencialDevolucaoEntrada contaGerencialDevolucaoEntrada")
				.leftOuterJoin("materialRateioEstoque.contaGerencialDevolucaoSaida contaGerencialDevolucaoSaida")
				.leftOuterJoin("materialRateioEstoque.projeto projetoRateio")
				.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
				.leftOuterJoin("material.empresamodelocontrato empresamodelocontrato")
				.leftOuterJoin("material.centrocustovenda centrocustovenda")
				.leftOuterJoin("material.contagerencialvenda contagerencialvenda")
				.leftOuterJoin("contagerencialvenda.vcontagerencial vcontagerencialvenda")
				.leftOuterJoin("material.arquivo arquivo")
				.join("material.materialgrupo materialgrupo")
				.leftOuterJoin("material.materialtipo materialtipo")
				.leftOuterJoin("material.materialcor materialcor")
				.leftOuterJoin("material.listaCaracteristica listaCaracteristica")
				.leftOuterJoin("material.listaMaterialrelacionado listaMaterialrelacionado")
				.leftOuterJoin("listaMaterialrelacionado.materialpromocao materialpromocao")
				.leftOuterJoin("listaMaterialrelacionado.listaMaterialrelacionadoformula materialrelacionadoformula")
				.leftOuterJoin("material.listaFornecedor listaFornecedor")
				.leftOuterJoin("listaFornecedor.fornecedor fornecedor")
				.leftOuterJoin("material.listaProducao listaProducao")
				.leftOuterJoin("listaProducao.listaMaterialproducaoformula materialproducaoformula")
				.leftOuterJoin("listaProducao.material materialrelacionado")
				.leftOuterJoin("listaProducao.producaoetapanome producaoetapanome")
				.leftOuterJoin("materialrelacionado.unidademedida unidademedidaproducao")
				.leftOuterJoin("materialrelacionado.materialgrupo materialrelacionadomaterialgrupo")
				.leftOuterJoin("material.listaMaterialempresa listaMaterialempresa")
				.leftOuterJoin("listaMaterialempresa.empresa empresa")
				.leftOuterJoin("listaMaterialempresa.grupotributacao grupotributacaolista")
				.leftOuterJoin("material.listaMaterialinspecaoitem listaMaterialinspecaoitem")
				.leftOuterJoin("listaMaterialinspecaoitem.inspecaoitem inspecaoitem")
				.leftOuterJoin("material.listaMaterialcolaborador listaMaterialcolaborador")
				.leftOuterJoin("listaMaterialcolaborador.colaborador colaborador")
				.leftOuterJoin("material.listaMaterialnumeroserie listaMaterialnumeroserie")
				.leftOuterJoin("listaMaterialnumeroserie.entrega entrega")
				.leftOuterJoin("listaMaterialnumeroserie.notafiscalproduto notafiscalproduto")
				.leftOuterJoin("notafiscalproduto.cliente clientenumeroserie")
				.leftOuterJoin("listaMaterialcolaborador.documentotipo documentotipo")
				.leftOuterJoin("material.grupotributacao grupotributacao")
				.leftOuterJoin("material.tributacaoecf tributacaoecf")
				.leftOuterJoin("material.listaMaterialunidademedida listaMaterialunidademedida")
				.leftOuterJoin("listaMaterialunidademedida.unidademedida unidademedidamaterial")
				.leftOuterJoin("material.listaMaterialsimilar listaMaterialsimilar")
				.leftOuterJoin("listaMaterialsimilar.materialsimilaritem materialsimilaritem")
				.leftOuterJoin("material.listaMaterialvenda listaMaterialvenda")
				.leftOuterJoin("listaMaterialvenda.materialvendaitem materialvendaitem")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.leftOuterJoin("material.materialcoleta materialcoleta")
				.leftOuterJoin("material.producaoetapa producaoetapa")
				.leftOuterJoin("material.materialcategoria materialcategoria")
				.leftOuterJoin("materialcategoria.vmaterialcategoria vmaterialcategoria")
				.leftOuterJoin("material.listaMaterialimposto listaMaterialimposto")
				.leftOuterJoin("listaMaterialimposto.empresa empresaMaterialimposto")
				.leftOuterJoin("material.listaMaterialkitflexivel listaMaterialkitflexivel")
				.leftOuterJoin("listaMaterialkitflexivel.materialkit materialkit")
				.leftOuterJoin("listaMaterialkitflexivel.listaMaterialkitflexivelformula materialkitflexivelformula")
				.leftOuterJoin("material.materialacompanhabanda materialacompanhabanda")
				.leftOuterJoin("material.listaMaterialCustoEmpresa listaMaterialCustoEmpresa")
				.leftOuterJoin("listaMaterialCustoEmpresa.contaContabil contaContabilMaterialCustoEmpresa")
				.leftOuterJoin("contaContabilMaterialCustoEmpresa.vcontacontabil vcontaContabilMaterialCustoEmpresa")
				.leftOuterJoin("listaMaterialCustoEmpresa.empresa empresaCustoEmpresa")
				.leftOuterJoin("material.listaImagensEcommerce listaImagensEcommerce")
				.leftOuterJoin("listaImagensEcommerce.arquivo imagemEcommerce")
				.leftOuterJoin("material.listaEmbalagem listaEmbalagem")
				.leftOuterJoin("listaEmbalagem.unidadeMedida unidademedidaEmbalagem")		
				.leftOuterJoin("material.listaMaterialAtributosMaterial listaMaterialAtributosMaterial")		
				.leftOuterJoin("listaMaterialAtributosMaterial.atributosMaterial atributosMaterial");		
	}

	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);

				Material material = (Material) save.getEntity();
				if (material.getArquivo() != null) {
					arquivoDAO.saveFile(material, "arquivo");
				}
				
				if(SinedUtil.isListNotEmpty(material.getListaImagensEcommerce())){
					for (ImagensEcommerce imagem : material.getListaImagensEcommerce()) {
						arquivoDAO.saveFile(imagem, "arquivo");
					}
				}
				
//				save.saveOrUpdateManaged("listaMaterialrelacionado", "material");
				save.saveOrUpdateManaged("listaCaracteristica");
				save.saveOrUpdateManaged("listaFornecedor");
				save.saveOrUpdateManaged("listaMaterialformulapeso");
				save.saveOrUpdateManaged("listaMaterialformulacusto");
				save.saveOrUpdateManaged("listaMaterialformulavalorvenda");
				save.saveOrUpdateManaged("listaMaterialformulavendaminimo");
				save.saveOrUpdateManaged("listaMaterialformulavendamaximo");
				save.saveOrUpdateManaged("listaMaterialformulamargemcontribuicao");
//				save.saveOrUpdateManaged("listaProducao", "materialmestre");
				save.saveOrUpdateManaged("listaMaterialempresa");
				save.saveOrUpdateManaged("listaMaterialinspecaoitem");
				save.saveOrUpdateManaged("listaMaterialcolaborador");
				save.saveOrUpdateManaged("listaMaterialunidademedida");
				save.saveOrUpdateManaged("listaMaterialnumeroserie");
				save.saveOrUpdateManaged("listaMaterialsimilar", "material");
				save.saveOrUpdateManaged("listaMaterialvenda", "material");
				save.saveOrUpdateManaged("listaMateriallocalarmazenagem", "material");
				save.saveOrUpdateManaged("listaMaterialpneumodelo");
				save.saveOrUpdateManaged("listaMaterialCustoEmpresa");
				save.saveOrUpdateManaged("listaMaterialFaixaMarkup");
				save.saveOrUpdateManaged("listaImagensEcommerce");
				save.saveOrUpdateManaged("listaEmbalagem");
				save.saveOrUpdateManaged("listaMaterialAtributosMaterial");
				return null;
			}
		});
	}

	@Override
	public void saveOrUpdate(Material bean) {

		Boolean update = bean.getCdmaterial() != null;
		
		Integer idPaiExterno = bean.getIdPaiExterno();
		if(!update){
			if(Util.objects.isNotPersistent(bean.getMaterialmestregrade())){//Se for mestre ou se for item que n�o controla grade
				bean.setIdPaiExterno(materialService.getNextIdPaiExterno());
			}
		}

		Produto produto = materialService.preencheProduto(bean);
		Bempatrimonio bempatrimonio = materialService.preenchePatrimonio(bean);
		Epi epi = materialService.preencheEpi(bean);
		Servico servico = materialService.preencheServico(bean);

		super.saveOrUpdate(bean);

		materialService.insereProduto(bean, update, produto);
		materialService.inserePatrimonio(bean, update, bempatrimonio);
		materialService.insereEpi(bean, update, epi);
		materialService.insereServico(bean, update, servico);
	}
	
	/**
	* M�todo que busca os materiais para o autocomplete considerando o grupo e o tipo de mateiral
	*
	* @param q
	* @param materialgrupo
	* @param materialtipo
	* @return
	* @since 15/09/2015
	* @author Luiz Fernando
	*/
	public List<Material> findAutocompleteByTipoGrupo(String q, Materialgrupo materialgrupo, Materialtipo materialtipo) {
		return querySined().select(
				"material.cdmaterial, material.nome, material.identificacao, materialmestregrade.nome, materialmestregrade.identificacao ")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.where("material.materialgrupo = ?", materialgrupo)
				.where("material.materialtipo = ?", materialtipo)
				.openParentheses()
					.whereLikeIgnoreAll("material.nome", q)
					.or()
					.whereLikeIgnoreAll("material.identificacao", q)
				.closeParentheses()
				.orderBy("material.identificacaoordenacao, material.nome")
				.autocomplete()
				.list();
	}
	
	public List<Material> findAutocompleteByMontarGrade(String q) {
		return querySined().select(
					"material.cdmaterial, material.nome, material.identificacao, materialmestregrade.nome, materialmestregrade.identificacao ")
					.leftOuterJoin("material.materialmestregrade materialmestregrade")
					.where("materialmestregrade is null")
					.openParentheses()
						.whereLikeIgnoreAll("material.nome", q)
						.or()
						.whereLikeIgnoreAll("material.identificacao", q)
					.closeParentheses()
					.list();
	}

	/**
	 * Busca material pelo tipo e grupo
	 * 
	 * @param materialtipo
	 * @param materialgrupo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Material> findByTipoGrupo(Materialgrupo materialgrupo, Materialtipo materialtipo) {
		return querySined().select(
				"material.cdmaterial, material.nome, material.identificacao, materialmestregrade.nome, materialmestregrade.identificacao ")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.where("material.materialgrupo = ?", materialgrupo)
				.where("material.materialtipo = ?", materialtipo)
				.orderBy("material.nome")
				.list();
	}
	
	/**
	 * M�todo que busca os materiais de acordo com tipo, grupo e empresa
	 *
	 * @param materialgrupo
	 * @param materialtipo
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findByTipoGrupo(Materialgrupo materialgrupo, Materialtipo materialtipo, Empresa empresa) {
		QueryBuilder<Material> query = querySined()
				.select("material.cdmaterial, material.nome, material.identificacao, materialmestregrade.nome, materialmestregrade.identificacao")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.leftOuterJoin("material.listaMaterialempresa listaMaterialempresa")
				.leftOuterJoin("listaMaterialempresa.empresa empresa")
				.where("material.materialgrupo = ?", materialgrupo)
				.where("material.materialtipo = ?", materialtipo);
		
		if(empresa != null){
			query.openParentheses()
					.where("empresa = ?", empresa)
					.or()
					.where("listaMaterialempresa is null")
				.closeParentheses();
		}
		
		query.orderBy("material.nome");
		return query.list();
	}

	/**
	 * Busca material pelo grupo
	 * 
	 * @param materialgrupo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Material> findByGrupo(Materialgrupo materialgrupo) {
		return querySined()
				.select(
						"material.cdmaterial, material.nome, materialmestregrade.nome, materialmestregrade.identificacao, "
								+ "unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, material.valorcusto, material.valorvenda")
				.join("material.unidademedida unidademedida")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.where(
						"material.materialgrupo = ?", materialgrupo).orderBy(
						"material.nome").list();

	}

	/**
	 * Busca material (produto/epi/servi�o) pelo tipo e grupo
	 * 
	 * @param materialtipo
	 * @param materialgrupo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Material> findByTipoGrupoProdutoEpiServico(
			Materialgrupo materialgrupo, Materialtipo materialtipo) {
		return querySined().select("material.cdmaterial, material.nome").join(
				"material.materialgrupo materialgrupo").leftOuterJoin(
				"material.materialtipo materialtipo").where(
				"materialgrupo = ?", materialgrupo).where("materialtipo = ?",
				materialtipo).where("material.patrimonio = ?", Boolean.FALSE)
				.orderBy("material.nome").list();
	}

	/**
	 * Fun��o que retorna o s�mbolo da unidade de medida do material e o tempo
	 * de entrega
	 * 
	 * @param material
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Material getSimboloUnidadeMedidaETempoEntrega(Material material) {
		return querySined()
				.select(
						"material.tempoentrega, unidademedida.simbolo, unidademedida.nome, material.valorcusto, material.valorvenda, " +
						"materialgrupo.cdmaterialgrupo, materialgrupo.coleta," +
						"materialgrupo.acompanhaBanda,materialgrupo.banda")
				.join("material.unidademedida unidademedida")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.where("material = ?", material)
				.unique();
	}

	/**
	 * M�todo que retorna todos materiais pertencentes as classes produto, epi
	 * servi�o ou ent�o da classe em especifico, menos patrimonio
	 * 
	 * @param materialclasse
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Material> getListaMateriaisProdutoEpiServicoByClasse(
			Materialclasse materialclasse) {
		QueryBuilder<Material> query = querySined().select(
				"material.cdmaterial, material.nome, material.identificacao");
		if (materialclasse != null) {
			query.openParentheses();
			if (materialclasse.equals(Materialclasse.PRODUTO)) {
				query.where("material.produto = ?", Boolean.TRUE).or();
			} else if (materialclasse.equals(Materialclasse.EPI)) {
				query.where("material.epi = ?", Boolean.TRUE).or();
			} else if (materialclasse.equals(Materialclasse.SERVICO)) {
				query.where("material.servico = ?", Boolean.TRUE).or();
			}
			query.closeParentheses();
		} else {
			query.where("material.produto = ?", Boolean.TRUE).or().where(
					"material.epi = ?", Boolean.TRUE).or().where(
					"material.servico = ?", Boolean.TRUE);
		}
		return query.orderBy("material.nome").list();
	}

	/**
	 * M�todo para obter materiais por tarefa. Utiliza os registros da tabela
	 * TAREFARECURSOGERAL para buscar os cargos associados a determinada tarefa.
	 * 
	 * @param tarefa
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Material> findByTarefa(Tarefa tarefa) {
		if (tarefa == null || tarefa.getCdtarefa() == null) {
			throw new SinedException(
					"Os par�metros tarefa ou cdtarefa n�o podem ser null.");
		}

		return querySined().select("distinct material.cdmaterial,material.nome")
				.join("material.listaTarefarecursogeral trg").join(
						"trg.tarefa tarefa").where("tarefa = ?", tarefa).list();
	}

	/**
	 * M�todo para obter materiais por planejamento, tarefa, grupo ou tipo.
	 * 
	 * @param planejamento
	 * @param tarefa
	 * @param grupo
	 * @param tipo
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Material> findByForApontamento(Planejamento planejamento,
			Tarefa tarefa, Materialgrupo grupo, Materialtipo tipo) {
		QueryBuilder<Material> query = querySined().select(
				"distinct material.cdmaterial,material.nome").leftOuterJoin(
				"material.listaPlanejamentorecursogeral prg").leftOuterJoin(
				"material.listaTarefarecursogeral trg").leftOuterJoin(
				"trg.tarefa tarefa").leftOuterJoin(
				"prg.planejamento planejamento").where(
				"material.materialgrupo = ?", grupo).where(
				"material.materialtipo = ?", tipo).orderBy("material.nome");

		if (tarefa != null) {
			query.where("tarefa = ?", tarefa);
		} else {
			query.where("planejamento = ?", planejamento);
		}

		return query.list();
	}

	/**
	 * M�todo para obter materiais por planejamento.
	 * 
	 * @param planejamento
	 * @return
	 * @author Ramon Brazil
	 */
	public List<Material> findByMaterial(Planejamento planejamento) {
		if (planejamento == null || planejamento.getCdplanejamento() == null) {
			throw new SinedException(
					"Os par�metros planejamento ou cdplanejamento n�o podem ser null.");
		}

		return querySined().select("distinct material.cdmaterial,material.nome")
				.join("material.listaTarefarecursogeral trg").join(
						"trg.tarefa tarefa").join(
						"tarefa.planejamento planejamento").where(
						"planejamento = ?", planejamento).orderBy(
						"material.nome").list();
	}

	/**
	 * Carrega a lista de material a ser exibida na parte em flex.
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Material> findAllForFlex() {
		return querySined()
				.select(
						"material.cdmaterial, material.nome, "
								+ "unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, "
								+ "material.valorcusto, material.valorvenda").join(
						"material.unidademedida unidademedida").orderBy(
						"material.nome").list();
	}

	/**
	 * M�todo que retorna simbolo da unidade de medida
	 * 
	 * @param material
	 * @return
	 * @author Tom�sr Rabelo
	 */
	public String getSimboloUnidadeMedida(Material material) {
		Material material2 = querySined().select("unidademedida.simbolo").join(
				"material.unidademedida unidademedida").where("material = ?",
				material).unique();
		return material2.getUnidademedida().getSimbolo();
	}

	/**
	 * Retorna o ID do da unidade de medida.
	 * 
	 * @param material
	 * @return
	 * @author Taidson
	 * @since 28/12/2010
	 */
	public Integer getIdUnidadeMedida(Material material) {
		Material material2 = querySined().select("unidademedida.cdunidademedida")
				.join("material.unidademedida unidademedida").where(
						"material = ?", material).unique();
		return material2.getUnidademedida().getCdunidademedida();
	}

	/**
	 * M�todo que busca o c�digo e nome do material pelo wherIn
	 * 
	 * @param listaMateriais
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Material> findMateriais(List<Material> listaMateriais) {
		return querySined().select("material.nome, material.cdmaterial").whereIn(
				"material.cdmaterial",
				CollectionsUtil.listAndConcatenate(listaMateriais,
						"cdmaterial", ",")).list();
	}

	/**
	 * Carrega o material pelo c�digo dele.
	 * 
	 * @param cdmaterial
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Material loadWithCodFlex(Integer cdmaterial) {
		if (cdmaterial == null) {
			throw new SinedException("Cdmaterial inv�lido.");
		}
		return querySined()
				.select(
						"material.cdmaterial, material.nome, unidademedida.cdunidademedida, unidademedida.nome, material.qtdereferencia, material.producao, material.valorcusto, "
								+ "material.valorvendaminimo, material.valorvendamaximo,material.valorvenda,material.servico,material.exibiritenskitvenda, "
								+ "material.naoexibirminmaxvenda, material.vendapromocional, material.kitflexivel, material.considerarvendamultiplos, material.qtdeunidade, "
								+ "material.pesoliquidovalorvenda, material.peso, material.pesobruto, material.metrocubicovalorvenda, "
								+ "material.patrimonio, arquivo.cdarquivo, arquivo.tamanho, material.identificacao, material.obrigarlote, material.tributacaoipi, "
								+ "materialmestregrade.cdmaterial, producaoetapa.cdproducaoetapa, materialcoleta.cdmaterial, materialcoleta.nome")
				.join("material.unidademedida unidademedida")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.leftOuterJoin("material.arquivo arquivo")
				.leftOuterJoin("material.producaoetapa producaoetapa")
				.leftOuterJoin("material.materialcoleta materialcoleta")
				.where("material.cdmaterial = ?", cdmaterial)
				.unique();
	}
	
	public Material loadWithProducaoetapa(Material material) {
		if (material == null || material.getCdmaterial() == null) 
			throw new SinedException("Material n�o pode ser nulo.");
		
		return querySined()
				.select("material.cdmaterial, material.nome, producaoetapa.cdproducaoetapa, producaoetapa.nome, unidademedida.cdunidademedida, unidademedida.nome, " +
						"materialproduto.altura, materialproduto.largura, materialproduto.comprimento, materialproduto.fatorconversaoproducao, " +
						"listaProducao.exibirvenda, materialproducao.identificacao, materialproducao.nome, materialproducao.profundidadesulco")
				.leftOuterJoin("material.materialproduto materialproduto")
				.leftOuterJoin("material.producaoetapa producaoetapa")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("material.listaProducao listaProducao")
				.leftOuterJoin("listaProducao.material materialproducao")
				.where("material = ?", material)
				.unique();
	}
	
	/**
	 * M�todo que carrega o material pelo c�digo dele com a listamaterialnumeroserie
	 *
	 * @param cdmaterial
	 * @return
	 * @author Luiz Fernando
	 */
	public Material loadWithNumeroserie(Integer cdmaterial){
		if (cdmaterial == null) {
			throw new SinedException("Cdmaterial inv�lido.");
		}
		
		return querySined()
		.select(
				"material.cdmaterial, material.nome, "
						+ "listaMaterialnumeroserie.cdmaterialnumeroserie, listaMaterialnumeroserie.numero")
		.join("material.listaMaterialnumeroserie listaMaterialnumeroserie")
		.leftOuterJoin("listaMaterialnumeroserie.venda venda")
		.where("material.cdmaterial = ?", cdmaterial)
		.where("venda is null")
		.unique();
	}
	
	/**
	 * M�todo que busca o material para registrar n�mero de s�rie
	 *
	 * @param cdmaterial
	 * @return
	 * @author Luiz Fernando
	 */
	public Material findForRegistrarnumeroserie(Integer cdmaterial){
		if (cdmaterial == null) {
			throw new SinedException("Cdmaterial inv�lido.");
		}
		
		return querySined()
			.select(
					"material.cdmaterial, material.nome, "
							+ "listaMaterialnumeroserie.cdmaterialnumeroserie, listaMaterialnumeroserie.numero")
			.leftOuterJoin("material.listaMaterialnumeroserie listaMaterialnumeroserie")
			.leftOuterJoin("listaMaterialnumeroserie.notafiscalproduto notafiscalproduto")
			.where("material.cdmaterial = ?", cdmaterial)
//			.where("notafiscalproduto is null")			
			.where("material.ativo = true")
			.unique();
	}
	
	/**
	 * M�todo que carrega o material promocional com os materiais relacionados
	 *
	 * @param cdmaterial
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findMaterialPromocionalComMaterialrelacionado(String whereIn) {
		if (StringUtils.isBlank(whereIn)) {
			throw new SinedException("Cdmaterial inv�lido.");
		}
		return querySined()
				.removeUseWhere()
				.select(
						"material.cdmaterial, material.nome, material.identificacao, unidademedida.cdunidademedida, unidademedida.nome, unidademedida.casasdecimaisestoque, material.qtdereferencia, "
						+ "material.producao, material.vendapromocional, material.valorvendaminimo, material.valorvendamaximo, material.valorvenda,material.valorcusto, "
						+ "material.servico,material.exibiritenskitvenda, material.codigobarras, material.naoexibirminmaxvenda, listaMaterialrelacionado.cdmaterialrelacionado, "
						+ "listaMaterialrelacionado.quantidade, listaMaterialrelacionado.valorvenda, unidademedidamaterialpromocao.nome, listaMaterialrelacionado.ordemexibicao, "
						+ "materialpromocao.cdmaterial, materialpromocao.nome, materialpromocao.nomenf, unidademedidamaterialpromocao.cdunidademedida, materialpromocao.qtdereferencia, material.producao, "
						+ "materialpromocao.valorvendaminimo, materialpromocao.valorvendamaximo, materialpromocao.valorvenda, materialpromocao.valorcusto, materialpromocao.servico, materialpromocao.vendapromocional, "
						+ "materialpromocao.naoexibirminmaxvenda, materialpromocao.ativo, materialpromocao.identificacao, "
						+ "listaMaterialrelacionadoformula.ordem, listaMaterialrelacionadoformula.identificador, listaMaterialrelacionado.exibirKitObservacaoItemNota, "
						+ "listaMaterialrelacionadoformula.formula, listaMaterialrelacionadoformula.cdmaterialrelacionadoformula,"
						+ "material.frete, material.produto, material.epi, material.patrimonio, "
						+ "materialpromocao.produto, materialpromocao.epi, materialpromocao.patrimonio, materialpromocao.ncmcompleto, materialpromocao.codigobarras, " 
						+ "material.peso, material.pesobruto, materialpromocao.peso, materialpromocao.pesobruto, material.obrigarlote, "
						+ "materialmestregrade.cdmaterial, ncmcapitulo.cdncmcapitulo, ncmcapitulo.descricao, materialpromocao.ncmcompleto ")
				.join("material.unidademedida unidademedida")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.leftOuterJoin("material.listaMaterialrelacionado listaMaterialrelacionado")
				.leftOuterJoin("listaMaterialrelacionado.materialpromocao materialpromocao")
				.leftOuterJoin("materialpromocao.unidademedida unidademedidamaterialpromocao")
				.leftOuterJoin("materialpromocao.ncmcapitulo ncmcapitulo")
				.leftOuterJoin("listaMaterialrelacionado.listaMaterialrelacionadoformula listaMaterialrelacionadoformula")
				.whereIn("material.cdmaterial", whereIn)
				.list();
	}

	/**
	 * M�todo que busca o valor de custo do produto a classe do material e sua
	 * conta gerencial de venda
	 * 
	 * @param material
	 * @return
	 * @auhtor Tom�s Rabelo,Ramon Brazil
	 */
	public Material getPrecoCustoProduto(Material material) {
		if (material == null || material.getCdmaterial() == null)
			throw new SinedException("Cdmaterial inv�lido.");

		return querySined()
				.setUseWhereEmpresa(false)
				.select("material.cdmaterial, material.nome, material.valorcusto, material.valorvenda, material.produto, material.servico, material.vendapromocional, " +
						"material.epi, material.patrimonio, material.tributacaomunicipal, contagerencialvenda.cdcontagerencial, centrocustovenda.cdcentrocusto ")
				.leftOuterJoin("material.contagerencialvenda contagerencialvenda")
				.leftOuterJoin("material.centrocustovenda centrocustovenda")
				.where("material = ?", material).unique();
	}

	/**
	 * Verifica se o material e grupo de loca��o.
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Boolean isLocacaoByMaterial(Material material) {
		Material bean = querySined()
			.setUseWhereEmpresa(false)
			.setUseWhereProjeto(false)
			.select("material.cdmaterial, materialgrupo.cdmaterialgrupo, materialgrupo.locacao")
			.join("material.materialgrupo materialgrupo")
			.where("material = ?", material)
			.unique();
		
		return bean != null && bean.getMaterialgrupo() != null && 
				bean.getMaterialgrupo().getLocacao() != null && bean.getMaterialgrupo().getLocacao();
	}

	/**
	 * M�todo que retorna o material/produto de acordo com seu nome
	 * 
	 * @param codigo
	 * @author Thiago Clemente, Ramon Brazil
	 * 
	 */
	public Material loadByCodigoMaterial(Integer cdmaterial) {
		return querySined()
				.select("material.cdmaterial, material.nome, material.servico, material.identificacao, " +
						"material.valorcusto, material.valorvendaminimo, material.valorvendamaximo, material.valorvenda, " +
						"unidademedida.cdunidademedida")
				.join("material.unidademedida unidademedida")
				.where("material.cdmaterial = ?", cdmaterial).setMaxResults(1)
				.unique();
	}

	/**
	 * M�todo que busca todos os materiais das ordens de compra que est�o dentro
	 * do periodo e que possuem entrega com contas a pagar baixadas
	 * 
	 * @param empresa
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Material> findMateriaisOrdemCompraPeriodo(Empresa empresa,
			Date dtinicio, Date dtfim) {
		String inicio = dtinicio != null ? new SimpleDateFormat("dd/MM/yyyy")
				.format(dtinicio) : null;
		String fim = dtfim != null ? new SimpleDateFormat("dd/MM/yyyy")
				.format(dtfim) : null;

		StringBuilder sb = new StringBuilder();
		sb.append("material.id in (" +
						"select m.id " +
						"from Ordemcompra o " + 
						"join o.listaMaterial lm " + 
						"join o.empresa em " + 
						"join lm.material m " + 
						"join o.listaOrdemcompraentrega oce " +
						"join oce.entrega e " + 
						"where e.id in (" +
							"select e1.id from Documento d " +
							"join d.listaDocumentoOrigem lo " +
							"join d.documentoacao da " + 
							"join lo.entrega e1 " +
							"where ");

		if (inicio != null)
			sb.append("d.dtemissao >= to_date('" + inicio
					+ "', 'dd/mm/yyyy') and ");
		if (fim != null)
			sb.append("d.dtemissao <= to_date('" + fim
					+ "', 'dd/mm/yyyy') and ");

		sb
				.append("da.id = " + Documentoacao.BAIXADA.getCddocumentoacao()
						+ ")");
		if (empresa != null && empresa.getCdpessoa() != null)
			sb.append(" and em.id = " + empresa.getCdpessoa());

		sb.append(")");
		return querySined().select("material.cdmaterial, material.nome").where(
				sb.toString()).orderBy("material.nome asc").list();
	}

	/**
	 * Busca os materiais para o autocomplete do detalhe da venda promocional.
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Material> findForPromocional(String s) {
		String funcaoTiraacento = Neo.getApplicationContext().getConfig()
				.getProperties().getProperty("funcaoTiraacento");

		QueryBuilder<Material> query = querySined()
				.autocomplete()
				.select("material.cdmaterial, material.nome")
				.openParentheses()
					.where("material.produto = ?", Boolean.TRUE).or()
					.where("material.servico = ?", Boolean.TRUE)
				.closeParentheses().where("UPPER(" + funcaoTiraacento + "(material.nome)) LIKE '%' || ? || '%'", Util.strings.tiraAcento(s).toUpperCase())
				.where("COALESCE(material.vendapromocional, false) = ?", Boolean.FALSE)
				.where("material.ativo = ?", Boolean.TRUE)
				.orderBy("material.nome");

		return query.list();
	}
	
	public List<Material> findForKitFlexivel(String s, Boolean produto, Boolean servico, Boolean epi, Boolean patrimonio) {
		QueryBuilder<Material> query = querySined()
				.autocomplete()
				.select("material.cdmaterial, material.nome")
				.where("coalesce(material.produto, false) = ?", produto != null ? produto : false, true)
				.where("coalesce(material.servico, false) = ?", servico != null ? servico : false, true)
				.where("coalesce(material.epi, false) = ?", epi != null ? epi : false, true)
				.where("coalesce(material.patrimonio, false) = ?", patrimonio != null ? patrimonio : false, true)
				.whereLikeIgnoreAll("material.nome", s)
				.where("COALESCE(material.vendapromocional, false) = ?", Boolean.FALSE, true)
				.orderBy("material.nome");

		return query.list();
	}

	/**
	 * Busca todos os materiais promocionais que s�o servi�os.
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Material> findServicoPromocional() {
		return querySined()
				.select("material.cdmaterial, material.nome")
				.openParentheses()
					.where("material.produto = ?", Boolean.TRUE)
					.or()
					.where("material.servico = ?", Boolean.TRUE)
				.closeParentheses()
				.where("material.vendapromocional = ?", Boolean.TRUE)
				.orderBy("material.nome")
				.list();
	}

	/**
	 * Busca todos os materiais que s�o servi�os.
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Material> findServicosProdutos() {
		return querySined()
				.select("material.cdmaterial, material.nome, material.identificacao, material.valorvenda, " +
						"material.qtdeunidade, materialtipo.cdmaterialtipo, material.patrimonio")
				.leftOuterJoin("material.materialtipo materialtipo")
				.openParentheses()
					.where("material.produto = ?", Boolean.TRUE)
					.or()
					.where("material.servico = ?", Boolean.TRUE)
					.or()
					.where("material.patrimonio = ?", Boolean.TRUE)
				.closeParentheses()
				.where("COALESCE(material.vendapromocional, FALSE) = ?", Boolean.FALSE)
				.where("COALESCE(material.obrigarlote, FALSE) = ?", Boolean.FALSE)
//				.where("material.valorvenda > 0")
				.where("material.ativo = ?", Boolean.TRUE)
				.orderBy("material.nome")
				.list();
	}

	/**
	 * Busca todos os materiais que s�o servi�os.
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Material> findServicos() {
		return querySined()
				.select("material.cdmaterial, material.nome, material.valorvenda, material.qtdeunidade, " +
						"materialtipo.cdmaterialtipo, material.identificacao")
				.leftOuterJoin("material.materialtipo materialtipo")
				.where("material.servico = ?", Boolean.TRUE)
				.where("material.ativo = ?", Boolean.TRUE)
				.orderBy("material.nome")
				.list();
	}

	/**
	 * Busca todos os materiais que s�o servi�os para autocomplete.
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Material> findServicosAutocomplete(String parametro) {
		return querySined()
				.autocomplete()
				.select("material.cdmaterial, material.nome, material.valorvenda, material.qtdeunidade, " +
						"materialtipo.cdmaterialtipo, material.identificacao")
				.leftOuterJoin("material.materialtipo materialtipo")
				.where("material.servico = ?", Boolean.TRUE)
				.where("material.ativo = ?", Boolean.TRUE)
				.openParentheses()
				.whereLikeIgnoreAll("material.nome", parametro)
				.or()
				.whereLikeIgnoreAll("material.identificacao", parametro)
				.closeParentheses()
				.orderBy("material.nome")
				.list();
	}
	
	/**
	 * Carrega o material com a contagerencial preenchida.
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Material findWithContagerencial(Material material) {
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		return querySined().select(
				"material.cdmaterial, contagerencial.cdcontagerencial").leftOuterJoin(
				"material.contagerencial contagerencial").where("material = ?",
				material).unique();
	}

	/**
	 * Retorna lista de materiais de produ��o para serem associados ao material
	 * 
	 * @param material
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Material> findMaterialProducao(Material material) {
		return querySined()
				.select("material.cdmaterial, material.nome")
				.where("material.produto = ?", Boolean.TRUE)
				.where("material.ativo = ?", Boolean.TRUE)
				.where("material.cdmaterial <> ?", material != null ? material.getCdmaterial() : null)
				.orderBy("material.nome").list();
	}

	/**
	 * Retorna lista de materiais de produ��o para serem associados ao material
	 * 
	 * @param material
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Material> findMaterialProducao(String whereIn) {
		return querySined()
				.select(
						"material.cdmaterial, material.nome, material.identificacao, material.servico")
				// .join("material.materialclasse materialclasse")
				.openParentheses().whereLikeIgnoreAll("material.nome", whereIn)
				.or().whereLikeIgnoreAll("material.identificacao", whereIn)
				.closeParentheses().orderBy("material.nome").list();
	}
	
	/**
	 * M�todo que busca a lista de materiais ativos
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findMaterialProducaoAutoCompleteAtivos(String q) {
		return querySined()
				.autocomplete()
				.select("material.cdmaterial, material.nome, material.identificacao, material.servico")
				.openParentheses().whereLikeIgnoreAll("material.nome", q)
				.or().whereLikeIgnoreAll("material.identificacao", q)
				.closeParentheses().where("material.ativo = true")
				.orderBy("material.nome").list();
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Material loadForOrdemProducao(Material material) {
		return querySined()
				.select(
						"material.cdmaterial, material.identificacao, material.nome,  matproducao.servico, matproducao.nome, "
								+ " producao.consumo, arquivo.cdarquivo, " 
								+ "listaCaracteristica.cdmaterialcaracteristica, listaCaracteristica.nome, unidademedida.cdunidademedida, unidademedida.simbolo, " +
										"caracteristicas.cdmaterialcaracteristica, caracteristicas.nome")
				.leftOuterJoin("material.arquivo arquivo")
				.leftOuterJoin("material.listaProducao producao")
				.leftOuterJoin("material.listaCaracteristica listaCaracteristica")
				.leftOuterJoin("producao.material matproducao")
				.leftOuterJoin("matproducao.unidademedida unidademedida")
				.leftOuterJoin("matproducao.listaCaracteristica caracteristicas")
				.where("material = ?", material)
				.unique();
	}

	/**
	 * M�todo que buscas os materiais do tipo servi�o que perten�am ao
	 * materialtipo
	 * 
	 * @param materialtipo
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public List<Material> findByTipoServico(Materialtipo materialtipo) {
		if (materialtipo == null || materialtipo.getCdmaterialtipo() == null)
			throw new SinedException("Par�metros inv�lidos!");

		return querySined().select("material.cdmaterial, material.nome").join(
				"material.materialtipo materialtipo").where(
				"material.servico = ?", Boolean.TRUE).where("materialtipo = ?",
				materialtipo).orderBy("material.nome desc").list();
	}
	
	/**
	* M�todo que busca os servi�os relacionados a determinada empresa e a todas as empresas de acordo com o par�metro
	*
	* @param materialtipo
	* @param empresa
	* @return
	* @since Oct 24, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Material> findByTipoServico(Materialtipo materialtipo, Empresa empresa) {
		if (empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos!");
		
		if(materialtipo != null && materialtipo.getCdmaterialtipo() != null){
			return querySined()
			.select("material.cdmaterial, material.nome")
			.leftOuterJoin("material.materialtipo materialtipo")
			.leftOuterJoin("material.listaMaterialempresa listaMaterialempresa")
			.where("material.servico = ?", Boolean.TRUE)
			.where("materialtipo = ?", materialtipo)
			.openParentheses()
			.where("listaMaterialempresa is null").or()
			.where("listaMaterialempresa.empresa = ?", empresa)
			.closeParentheses()
			.orderBy("material.nome").list();
		}else{
			return querySined()
			.select("material.cdmaterial, material.nome")				
			.leftOuterJoin("material.listaMaterialempresa listaMaterialempresa")
			.where("material.servico = ?", Boolean.TRUE)
			.openParentheses()
			.where("listaMaterialempresa is null").or()
			.where("listaMaterialempresa.empresa = ?", empresa)
			.closeParentheses()
			.orderBy("material.nome").list();
			
		}
	}
	
	/**
	* M�todo que busca os servi�os relacionados a determinada empresa e a todas as empresas de acordo com o par�metro 
	*
	* @param empresa
	* @return
	* @since Nov 1, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Material> findByEmpresaFlex(Empresa empresa) {
		if (empresa == null || empresa.getCdpessoa() == null)
			throw new SinedException("Par�metros inv�lidos!");
				
		return querySined()
				.select("material.cdmaterial, material.nome, material.valorvenda, material.qtdeunidade, materialtipo.cdmaterialtipo")
				.leftOuterJoin("material.materialtipo materialtipo")
				.leftOuterJoin("material.listaMaterialempresa listaMaterialempresa")
				.leftOuterJoin("listaMaterialempresa.empresa empresa")
				.where("material.ativo = true")
				.where("material.servico = ?", Boolean.TRUE)
				.openParentheses()
				.where("listaMaterialempresa is null").or()
				.where("empresa = ?", empresa)
				.closeParentheses()
				.orderBy("material.nome").list();
		
	}

	/**
	 * M�todo que carrega dados relevantes do material
	 * 
	 * @param material
	 * @return
	 */
	public Material carregaMaterial(Material material) {
		return querySined()
				.setUseWhereEmpresa(false)
				.select("material.cdmaterial, material.identificacao, material.nome, material.qtdeunidade, material.valorcusto, " +
						"material.servico, unidademedida.cdunidademedida, grupotributacao.cdgrupotributacao, " +
						"tributacaoecf.cdtributacaoecf, tributacaoecf.codigoecf, tributacaoecf.codigototalizador, " +
						"tributacaoecf.numerototalizador, tributacaoecf.codigototalizadorcancelada," +
						"materialmestregrade.cdmaterial, materialmestregrade.identificacao," +
						"centrocustovenda.cdcentrocusto, contagerencialvenda.cdcontagerencial," +
						"material.tipoitemsped, contagerencial.cdcontagerencial, materialgrupo.cdmaterialgrupo," +
						"material.producao, material.vendapromocional, material.ncmcompleto, material.extipi, ncmcapitulo.cdncmcapitulo, " +
						"material.codigobarras, material.servico, material.tributacaoestadual, materialtipo.cdmaterialtipo, materialtipo.nome, " +
						"material.kitflexivel, material.tributacaoipi, material.exibiritenskitflexivelvenda, material.formaTributacao")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("material.grupotributacao grupotributacao")
				.leftOuterJoin("material.tributacaoecf tributacaoecf")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.leftOuterJoin("material.centrocustovenda centrocustovenda")
				.leftOuterJoin("material.contagerencialvenda contagerencialvenda")
				.leftOuterJoin("material.contagerencial contagerencial")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("material.materialtipo materialtipo")
				.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
				.where("material = ?", material)
				.unique();
	}
	
	/**
	 * M�todo que carrega o material para gerar a grade
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 28/01/2014
	 */
	public Material carregaMaterialForGerarGrade(Material material) {
		return query()
				.select("material.cdmaterial, material.identificacao, material.nome, material.qtdeunidade, " +
						"material.servico, material.valorcusto, material.valorvenda, material.valorvendaminimo," +
						"material.valorvendamaximo, unidademedida.cdunidademedida, grupotributacao.cdgrupotributacao, " +
						"tributacaoecf.cdtributacaoecf, tributacaoecf.codigoecf, tributacaoecf.codigototalizador, " +
						"tributacaoecf.numerototalizador, tributacaoecf.codigototalizadorcancelada," +
						"materialmestregrade.cdmaterial, materialmestregrade.identificacao," +
						"centrocustovenda.cdcentrocusto, contagerencialvenda.cdcontagerencial," +
						"material.tipoitemsped, contagerencial.cdcontagerencial, materialgrupo.cdmaterialgrupo," +
						"listaMaterialunidademedida.cdmaterialunidademedida, listaMaterialunidademedida.fracao, " +
						"listaMaterialunidademedida.qtdereferencia, listaMaterialunidademedida.valorunitario, " +
						"listaMaterialunidademedida.mostrarconversao, unidademedidaconversao.cdunidademedida, " +
						"unidademedidaconversao.nome, unidademedida.nome," +
						"listaMaterialunidademedida.prioridadevenda, listaMaterialunidademedida.prioridadecompra, materialtipo.cdmaterialtipo, " +
						"listaMaterialempresa.cdmaterialempresa, grupotributacaoEmpesa.cdgrupotributacao, " +
						"empresa.cdpessoa ")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("material.materialtipo materialtipo")
				.leftOuterJoin("material.grupotributacao grupotributacao")
				.leftOuterJoin("material.tributacaoecf tributacaoecf")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.leftOuterJoin("material.centrocustovenda centrocustovenda")
				.leftOuterJoin("material.contagerencialvenda contagerencialvenda")
				.leftOuterJoin("material.contagerencial contagerencial")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("material.listaMaterialunidademedida listaMaterialunidademedida")
				.leftOuterJoin("listaMaterialunidademedida.unidademedida unidademedidaconversao")
				.leftOuterJoin("material.listaMaterialempresa listaMaterialempresa")
				.leftOuterJoin("listaMaterialempresa.empresa empresa")
				.leftOuterJoin("listaMaterialempresa.grupotributacao grupotributacaoEmpesa")
				
				.where("material = ?", material)
				.unique();
	}

	/**
	 * M�todo que carrega material e seu centro de custo venda.
	 * 
	 * @param material
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Material getCentroCustoContaGerencialVenda(Material material) {
		return query()
				.select(
						"material.cdmaterial, material.nome, centrocustovenda.cdcentrocusto, contagerencialvenda.cdcontagerencial")
				.join("material.centrocustovenda centrocustovenda").join(
						"material.contagerencialvenda contagerencialvenda")
				.where("material = ?", material).unique();
	}

	/**
	 * Carrega informa��es para a tela de nota fiscal de produto via ajax.
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Material loadForNFProduto(Material material) {
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		return querySined()
				.setUseWhereEmpresa(false)
				.select(  "material.cdmaterial, unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, material.patrimonio, material.servico, material.tipoitemsped, "
						+ "material.codigobarras, material.ncmcompleto, material.extipi, ncmcapitulo.cdncmcapitulo, material.nve, "
						+ "material.patrimonio, material.valorvenda, ncmcapitulo.descricao, material.basecalculost, material.valorst, "
						+ "material.tributacaoestadual, material.percentualimpostoecf, material.codigonbs, material.codlistaservico, "
						+ "material.cest, material.obrigarlote, material.origemproduto, "
						+ "materialgrupo.cdmaterialgrupo, "
						+ "uficmsst.cduf, uficmsst.sigla, codigocnae.cdcodigocnae, "
						+ "grupotributacao.cdgrupotributacao, grupotributacao.tributadoicms, "
						+ "listaMaterialempresa.cdmaterialempresa, grupotributacaoEmpesa.cdgrupotributacao, empresa.cdpessoa,"
						+ "empresaTributacao.cdpessoa, codigoanp.cdcodigoanp, codigoanp.codigo, "
						+ "materialtipo.cdmaterialtipo")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
				.leftOuterJoin("material.codigoanp codigoanp")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("material.grupotributacao grupotributacao")
				.leftOuterJoin("grupotributacao.listaGrupotributacaoempresa listaGrupotributacaoempresa")
				.leftOuterJoin("listaGrupotributacaoempresa.empresa empresaTributacao")
				.leftOuterJoin("grupotributacao.uficmsst uficmsst")
				.leftOuterJoin("grupotributacao.codigocnae codigocnae")
				.leftOuterJoin("material.listaMaterialempresa listaMaterialempresa")
				.leftOuterJoin("listaMaterialempresa.empresa empresa")
				.leftOuterJoin("listaMaterialempresa.grupotributacao grupotributacaoEmpesa")
				.leftOuterJoin("material.materialtipo materialtipo")
				.where("material = ?", material)
				.unique();
	}


	public List<Material> findByTipoServico() {

		return query().select("material.cdmaterial, material.nome").join(
				"material.materialtipo materialtipo").where(
				"material.servico = ?", Boolean.TRUE).orderBy(
				"material.nome desc").list();
	}
	
	/**
	 * M�todo que busca os materiais de produ��o ativos para o autocomplete
	 *
	 * @param q
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Material> findMaterialProducaoAtivoAutocomplete(String q) {
		return query()
				.select("material.cdmaterial, material.nome, material.identificacao, material.tempoentrega")
				.openParentheses()
					.whereLikeIgnoreAll("material.nome", q)
					.or()
					.whereLikeIgnoreAll("material.identificacao", q)
				.closeParentheses()
				.where("material.ativo = ?", Boolean.TRUE)
				.where("material.producao = ?", Boolean.TRUE)
				.orderBy("material.identificacaoordenacao, material.nome")
				.autocomplete()
				.list();
	}
	
	@Override
	public List<Material> findForAutocomplete(String q, String propertyMatch, String propertyLabel, Boolean matchOption) {
		return query()
				.select("material.cdmaterial, material.nome, material.identificacao")
				.whereLikeIgnoreAll("material.nome", q).list();
	}
	
	public List<Material> findMaterialAutocomplete(String q, Boolean ativo, Boolean producao) {
		return this.findMaterialAutocomplete(q, ativo, producao, false);
	}

	public List<Material> findMaterialAutocomplete(String q, Boolean ativo, Boolean producao, boolean naoConsiderarItensQueControlamLote) {
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		boolean serie = Boolean.TRUE.equals(NeoWeb.getRequestContext().getSession().getAttribute("serieForAutocompleteMaterial"));
		String consideraRestricaoFornecedorProdutoStr = NeoWeb.getRequestContext().getParameter("consideraRestricaoFornecedorProduto");
		boolean considerarRestricaoMaterialFornecedor = consideraRestricaoFornecedorProdutoStr != null && "true".equals(consideraRestricaoFornecedorProdutoStr);
		
		QueryBuilder<Material> query= query()
				.select("material.cdmaterial, material.nome, material.identificacao, material.patrimonio, material.tempoentrega," +
						"materialmestregrade.cdmaterial, materialmestregrade.identificacao, listaMaterialnumeroserie.numero")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.leftOuterJoin("material.listaMaterialnumeroserie listaMaterialnumeroserie")
				.openParentheses()
					.whereLikeIgnoreAll("material.nome", q)
					.or()
					.whereLikeIgnoreAll("material.identificacao", q)
					.or()
					.where("UPPER(" + funcaoTiraacento + "(listaMaterialnumeroserie.numero)) LIKE '%' || ? || '%'", Util.strings.tiraAcento(q).toUpperCase(), serie)
				.closeParentheses().orderBy("material.nome")
				.where("coalesce(material.ativo, false) = ?", ativo)
				.where("coalesce(material.producao, false) = ?", producao)
				.orderBy("material.identificacaoordenacao, material.nome")
				.autocomplete();
		if(naoConsiderarItensQueControlamLote){
			query.where("coalesce(material.obrigarlote, false) = false");
		}
		if(considerarRestricaoMaterialFornecedor && SinedUtil.isRestricaoFornecedorProdutoUsuarioLogado()){
			query.leftOuterJoin("material.listaFornecedor listaFornecedor");
			query.leftOuterJoin("listaFornecedor.fornecedor fornecedor");
			query.leftOuterJoin("fornecedor.listaColaboradorFornecedor listaColaboradorFornecedor");
			query.leftOuterJoin("listaColaboradorFornecedor.colaborador colaborador");
			query.openParentheses()
				.where("listaFornecedor is null")
				.or()
				.where("listaColaboradorFornecedor is null")
				.or()
				.where("colaborador = ?", SinedUtil.getUsuarioComoColaborador())
				.closeParentheses();
		}
		return query.list();
	}
	
	/**
	 * Busca os materiais para autocomplete atrav�s do nome de nota fiscal 
	 *
	 * @param q, ativo
	 * @param ativo
	 * @return List<Material>
	 * @author Jo�o Vitor
	 * @since 14/10/2014
	 */
	public List<Material> findMaterialNotaFiscalProdutoAutocomplete(String q, Boolean ativo) {
		return query()
				.select("material.cdmaterial, material.nome, material.identificacao, material.tempoentrega, material.nomenf," +
						"materialmestregrade.cdmaterial, materialmestregrade.identificacao")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.openParentheses()
				.whereLikeIgnoreAll("material.nome", q)
				.or()
				.whereLikeIgnoreAll("material.identificacao", q)
				.or()
				.whereLikeIgnoreAll("material.nomenf", q)
				.closeParentheses()
				.where("material.ativo = ?", ativo)
				.orderBy("material.identificacaoordenacao, material.nome")
				.autocomplete()
				.list();
	}
	
	public List<Material> findMaterialEstoqueAutocomplete(String q, Localarmazenagem localarmazenagem) {
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		boolean serie = Boolean.TRUE.equals(NeoWeb.getRequestContext().getSession().getAttribute("serieForAutocompleteMaterial"));
		String consideraRestricaoFornecedorProdutoStr = NeoWeb.getRequestContext().getParameter("consideraRestricaoFornecedorProduto");
		boolean considerarRestricaoMaterialFornecedor = consideraRestricaoFornecedorProdutoStr != null && "true".equals(consideraRestricaoFornecedorProdutoStr);
		
		QueryBuilder<Material> query = query()
								.autocomplete()
								.select("material.cdmaterial, material.nome, material.identificacao, material.patrimonio, listaMaterialnumeroserie.numero")
								.leftOuterJoin("material.vgerenciarmaterial vgerenciarmaterial")
								.leftOuterJoin("material.listaMaterialnumeroserie listaMaterialnumeroserie")
								.where("material.ativo = true")
								.openParentheses()
									.whereLikeIgnoreAll("material.nome", q)
									.or()
									.whereLikeIgnoreAll("material.identificacao", q)
									.or()
									.where("UPPER(" + funcaoTiraacento + "(listaMaterialnumeroserie.numero)) LIKE '%' || ? || '%'", Util.strings.tiraAcento(q).toUpperCase(), serie)
								.closeParentheses()
								.where("vgerenciarmaterial.localarmazenagem = ?", localarmazenagem)
								.where("vgerenciarmaterial.qtdedisponivel > 0.0");
		if(considerarRestricaoMaterialFornecedor && SinedUtil.isRestricaoFornecedorProdutoUsuarioLogado()){
			query.leftOuterJoin("material.listaFornecedor listaFornecedor");
			query.leftOuterJoin("listaFornecedor.fornecedor fornecedor");
			query.leftOuterJoin("fornecedor.listaColaboradorFornecedor listaColaboradorFornecedor");
			query.leftOuterJoin("listaColaboradorFornecedor.colaborador colaborador");
			query.openParentheses()
				.where("listaFornecedor is null")
				.or()
				.where("listaColaboradorFornecedor is null")
				.or()
				.where("colaborador = ?", SinedUtil.getUsuarioComoColaborador())
				.closeParentheses();
		}
		return query.list();
	}
	
	/**
	 * Busca os materiais para autocompletecom a flag de patrimonio 
	 *
	 * @param q
	 * @param patrimonio
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/10/2013
	 */
	public List<Material> findForAutocompletePatrimonio(String q, Boolean patrimonio) {
		return query()
					.select("material.cdmaterial, material.nome, material.identificacao")
					.openParentheses()
						.whereLikeIgnoreAll("material.nome", q)
						.or()
						.whereLikeIgnoreAll("material.identificacao", q)
					.closeParentheses()
					.where("material.ativo = ?", Boolean.TRUE)
					.where("material.patrimonio = ?", patrimonio)
					.orderBy("material.identificacaoordenacao, material.nome")
					.autocomplete()
					.list();
	}
	
	/**
	 * Busca Produto/EPI/Servi�o para dar entrada no estoque.
	 *
	 * @param q
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/12/2012
	 */
	public List<Material> findProdutoEpiServicoAtivoAutocomplete(String q, String campo) {
		QueryBuilder<Material> query = query()
					.select("material.cdmaterial, material.nome, material.identificacao, material.tempoentrega, material.codigobarras, " +
							"materialmestregrade.cdmaterial, materialmestregrade.identificacao")
					.leftOuterJoin("material.materialmestregrade materialmestregrade");
		
		query.openParentheses();
		
		if(StringUtils.isEmpty(campo) || MovimentacaoestoqueFiltro.TODOS.equals(campo) || 
				MovimentacaoestoqueFiltro.NOME.equals(campo)){
			query.whereLikeIgnoreAll("material.nome", q).or();
		}
		if(StringUtils.isEmpty(campo) || MovimentacaoestoqueFiltro.TODOS.equals(campo) || 
				MovimentacaoestoqueFiltro.IDENTIFICACAO.equals(campo)){
			query.whereLikeIgnoreAll("material.identificacao", q).or();
		}
		if(StringUtils.isEmpty(campo) || MovimentacaoestoqueFiltro.TODOS.equals(campo) || 
				MovimentacaoestoqueFiltro.CODIGO_BARRAS.equals(campo)){
			query.whereLikeIgnoreAll("material.codigobarras", q);
		}
		query.closeParentheses();
					
		query
			.openParentheses()
				.openParentheses()
					.where("material.patrimonio is null")
					.or()
					.where("material.patrimonio = ?", Boolean.FALSE)
				.closeParentheses()
				.or()
				.where("material.servico = ?", Boolean.TRUE)
				.or()
				.where("material.produto = ?", Boolean.TRUE)
				.or()
				.where("material.epi = ?", Boolean.TRUE)
			.closeParentheses()
			.where("material.ativo = ?", Boolean.TRUE);
		
		if(StringUtils.isEmpty(campo) || MovimentacaoestoqueFiltro.TODOS.equals(campo)){
			query.orderBy("material.identificacaoordenacao, material.nome, material.codigobarras");
		}else  if(MovimentacaoestoqueFiltro.NOME.equals(campo)){
			query.orderBy("material.nome, material.identificacaoordenacao, material.codigobarras");
		}else if(MovimentacaoestoqueFiltro.IDENTIFICACAO.equals(campo)){
			query.orderBy("material.identificacaoordenacao, material.nome, material.codigobarras");
		}else if(MovimentacaoestoqueFiltro.CODIGO_BARRAS.equals(campo)){
			query.orderBy("material.codigobarras, material.identificacaoordenacao, material.nome");
		}
		return query.autocomplete()
					.list();
	}
	
	/**
	 * Retorna material com a sua respectiva unidade de medida
	 * 
	 * @param material
	 * @return
	 * @author Taidson
	 * @since 11/06/2010
	 */
	public Material unidadeMedidaMaterial(Material material) {
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		return querySined()
				.setUseWhereEmpresa(false)
				.select("material.cdmaterial, material.nome, material.identificacao, " +
						"material.valorvenda, material.valorvendaminimo, material.valorvendamaximo, " +
						"unidademedida.nome, unidademedida.cdunidademedida, unidademedida.simbolo, " +
						"unidademedida.casasdecimaisestoque, material.producao ")								 
				.leftOuterJoin("material.unidademedida unidademedida")
				.where("material = ?", material)
				.unique();
	}

	/**
	 * Realiza busca geral em Material conforme par�metro da busca.
	 * 
	 * @param busca
	 * @return
	 * @author Taidson
	 * @sice 04/09/2010
	 */
	public List<Material> findMaterialForBuscaGeral(String busca) {
		return query().select("material.cdmaterial, material.nome")
				.whereLikeIgnoreAll("material.nome", busca).orderBy(
						"material.nome").list();
	}

	/**
	 * Retorna todos os materiais que s�o produtos e servi�os.
	 * @param q
	 * @return
	 * @author Taidson
	 * @since 24/09/2010
	 */
	public List<Material> findProdutoServico(String q) {
		return query().select("material.cdmaterial, material.identificacao, material.nome")
				.openParentheses()
					.where("material.servico = ?", Boolean.TRUE)
					.or()
					.where("material.produto = ?", Boolean.TRUE)
				.closeParentheses()
				.openParentheses()
					.whereLikeIgnoreAll("material.nome", q)
					.or()
					.whereLikeIgnoreAll("material.identificacao", q)
				.closeParentheses()
				.orderBy("material.nome").list();
	}

	/**
	 * Busca os materiais para o registro 0200 do SPED.
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Material> findForSpedReg0200(String whereIn, String whereInMaterialproducao) {
		List<Tipoitemsped> listaTipoitemsped = new ArrayList<Tipoitemsped>();
		listaTipoitemsped.add(Tipoitemsped.MERCADORIA_REVENDA);
		listaTipoitemsped.add(Tipoitemsped.MATERIA_PRIMA);
		listaTipoitemsped.add(Tipoitemsped.EMBALAGEM);
		listaTipoitemsped.add(Tipoitemsped.PRODUTO_PROCESSO);
		listaTipoitemsped.add(Tipoitemsped.PRODUTO_ACABADO);
		listaTipoitemsped.add(Tipoitemsped.SUBPRODUTO);
		listaTipoitemsped.add(Tipoitemsped.PRODUTO_INTERMEDIARIO);
		listaTipoitemsped.add(Tipoitemsped.OUTROS_INSUMOS);
		
		return query()
				.select(
						"material.cdmaterial, material.identificacao, material.nome, material.codigobarras, material.extipi, "
								+ "material.tipoitemsped, material.ncmcompleto, material.codlistaservico, "
								+ "unidademedida.cdunidademedida, ncmcapitulo.cdncmcapitulo, codigoanp.cdcodigoanp, codigoanp.codigo")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
				.leftOuterJoin("material.codigoanp codigoanp")
				.openParentheses()
					.whereIn("material.cdmaterial", whereIn)
					.or()
					.where("material.cdmaterial in (" +
							" select m.cdmaterial " +
							" from Materialproducao mp " +
							" join mp.material m " +
							" where mp.materialmestre in (" + whereInMaterialproducao + ") " +
							" and m.tipoitemsped in (" + CollectionsUtil.listAndConcatenate(listaTipoitemsped, "id", ",") + "))", StringUtils.isNotBlank(whereInMaterialproducao))
				.closeParentheses()
				.list();
	}

	/**
	 * Atualiza o Tipo de item.
	 * 
	 * @param tipoitemsped
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void updateTipoitemsped(Tipoitemsped tipoitemsped, Material material) {
		getJdbcTemplate()
				.update(
						"UPDATE material SET tipoitemsped = ? WHERE cdmaterial = ?",
						new Object[] { tipoitemsped.ordinal(),
								material.getCdmaterial() });
	}

	/**
	 * Atualiza o G�nero do NCM.
	 * 
	 * @param ncmcapitulo
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void updateNcmcapitulo(Ncmcapitulo ncmcapitulo, Material material) {
		getJdbcTemplate().update(
				"UPDATE material SET cdncmcapitulo = ? WHERE cdmaterial = ?",
				new Object[] { ncmcapitulo.getCdncmcapitulo(),
						material.getCdmaterial() });
	}

	/**
	 * Atualiza o C�digo de barras.
	 * 
	 * @param codigobarras
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void updateCodigobarras(String codigobarras, Material material) {
		getJdbcTemplate().update(
				"UPDATE material SET codigobarras = ? WHERE cdmaterial = ?",
				new Object[] { codigobarras, material.getCdmaterial() });
	}

	/**
	 * Atualiza o NCM completo.
	 * 
	 * @param ncmcompleto
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void updateNcmcompleto(String ncmcompleto, Material material) {
		getJdbcTemplate().update(
				"UPDATE material SET ncmcompleto = ? WHERE cdmaterial = ?",
				new Object[] { ncmcompleto, material.getCdmaterial() });
	}

	/**
	 * Atualiza o Ex da TIPI.
	 * 
	 * @param extipi
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void updateExtipi(String extipi, Material material) {
		getJdbcTemplate().update(
				"UPDATE material SET extipi = ? WHERE cdmaterial = ?",
				new Object[] { extipi, material.getCdmaterial() });
	}

	/**
	 * Atualiza o C�digo do item na lista de servi�o.
	 * 
	 * @param codlistaservico
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void updateCodlistaservico(String codlistaservico, Material material) {
		getJdbcTemplate().update(
				"UPDATE material SET codlistaservico = ? WHERE cdmaterial = ?",
				new Object[] { codlistaservico, material.getCdmaterial() });
	}

	/**
	 * Carrega o material com a conta gerencial e o centro de custo de venda
	 * preenchidos.
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Material loadWithContagerencialCentrocusto(Material material) {
		return query()
				.select(
						"material.cdmaterial, material.nome, material.valorvenda, material.servico, "
								+ "material.produto, material.patrimonio, material.tributacaoestadual, contagerencial.cdcontagerencial, "
								+ "centrocusto.cdcentrocusto, contagerencial.nome, vcontagerencial.identificador, "
								+ "vcontagerencial.arvorepai")
				.leftOuterJoin("material.contagerencialvenda contagerencial")
				.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
				.leftOuterJoin("material.centrocustovenda centrocusto")
				.where("material = ?", material)
				.unique();
	}
	
	public Material loadWithContagerencial(Material material) {
		return query()
				.select(
						"material.cdmaterial, material.nome, contagerencial.cdcontagerencial, " +
						"contagerencial.nome, vcontagerencial.identificador, vcontagerencial.arvorepai")
				.leftOuterJoin("material.contagerencial contagerencial")
				.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
				.where("material = ?", material)
				.unique();
	}
	
	/**
	 * M�todo que busca os materiais com a lista de material fornecedor para
	 * listagem
	 * 
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Material> findListagem(String whereIn, String orderBy, boolean asc,Localarmazenagem local) {
		if (whereIn == null || whereIn.equals("")) {
			return new ArrayList<Material>();
		}
		
		
		QueryBuilder<Material> query = query()
				.select(
						"distinct grupo.nome,  tipo.nome, material.cdmaterial, material.produto, material.epi, material.patrimonio, "
								+ "material.servico,material.producao, material.valorcusto, material.peso, material.pesobruto, "
								+ "material.valorvenda, material.nome, material.identificacao, material.referencia, contagerencial.nome, unidademedida.cdunidademedida, unidademedida.simbolo, "
								+ "listaFornecedor.cdmaterialfornecedor, listaFornecedor.codigo, localizacaoestoque.descricao, " 
								+ "listaMaterialunidademedida.cdmaterialunidademedida, listaMaterialunidademedida.fracao, listaMaterialunidademedida.qtdereferencia, " 
								+ "vgerenciarmaterial.qtdedisponivel,listaMaterialunidademedida.mostrarconversao, unidademedidaconversao.nome, unidademedida.nome, "
								+ "materialmestregrade.cdmaterial, materialmestregrade.identificacao, materialcategoria.descricao ")
				.join("material.materialgrupo grupo")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.leftOuterJoin("material.materialtipo tipo")
				.leftOuterJoin("material.vgerenciarmaterial vgerenciarmaterial")
				.join("material.unidademedida unidademedida")
				.leftOuterJoin("material.contagerencial contagerencial")
				.leftOuterJoin("material.materialcategoria materialcategoria")
				.leftOuterJoin("material.listaFornecedor listaFornecedor")
				.leftOuterJoin("material.localizacaoestoque localizacaoestoque")
				.leftOuterJoin("material.listaMaterialunidademedida listaMaterialunidademedida")
				.leftOuterJoin("listaMaterialunidademedida.unidademedida unidademedidaconversao");
		
		if (local !=null){ 
			query
				.openParentheses()
					.where("vgerenciarmaterial.localarmazenagem =?", local)
						.or()
					.where("vgerenciarmaterial.localarmazenagem is null")
				.closeParentheses();
		}
				
		SinedUtil.quebraWhereIn("material.cdmaterial", whereIn, query);
				
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy + " " + (asc ? "ASC" : "DESC"));
		} else {
			query.orderBy("material.cdmaterial desc");
		}

		return query.list();
	}
	
	/**
	 * Retorna a lsita de material com a qtde em expedi��o (expedi��o em aberto) e qtde em compra (ordem de compra com pedido enviado)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<Material> findEmExpedicaoAndEmCompra(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			return null;
		
		StringBuilder sql = new StringBuilder();
		
		sql
			.append("SELECT m.cdmaterial AS CDMATERIAL, m.nome AS NOME, ")
			.append("(select sum(ei.qtdeexpedicao) ")
			.append("from expedicaoitem ei ")
			.append("join expedicao e on e.cdexpedicao = ei.cdexpedicao ")
			.append("where e.expedicaosituacao = " + Expedicaosituacao.EM_ABERTO.getValue())
			.append(" and ei.cdmaterial = m.cdmaterial) as EMEXPEDICAO, ")

			.append("(select sum(ocm.qtdpedida) ")
			.append("from ordemcompramaterial ocm ")
			.append("join ordemcompra oc on oc.cdordemcompra = ocm.cdordemcompra ")
			.append("join aux_ordemcompra aux_oc on aux_oc.cdordemcompra = oc.cdordemcompra ")
			.append("where aux_oc.situacao = " + Situacaosuprimentos.PEDIDO_ENVIADO.getCodigo())
			.append(" and ocm.cdmaterial = m.cdmaterial) as EMCOMPRA ")
			
			.append("from Material m ")
			.append("where m.cdmaterial in (" + whereIn + ") ");

		SinedUtil.markAsReader();
		List<Material> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						return new Material(rs.getInt("CDMATERIAL"), 
											rs.getString("NOME"), 
											rs.getDouble("EMEXPEDICAO"), 
											rs.getDouble("EMCOMPRA"));
					}
				});
		
		return lista;
	}
	
	/**
	 * M�todo que busca os materiais com reserva
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<Material> findWithReserva(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			return null;
		
		StringBuilder sql = new StringBuilder();
		String materiais = SinedUtil.quebraWhereInSubQuery("r.cdmaterial", whereIn);
		
		sql
			.append(" SELECT distinct r.cdmaterial AS CDMATERIAL ")
			.append(" from reserva r ")
			.append(" where " + materiais);

		SinedUtil.markAsReader();
		List<Material> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						return new Material(rs.getInt("CDMATERIAL"));
					}
				});
		
		return lista;
	}
	

	public void updateIdentificacao(Material material, String identificacao) {
		getJdbcTemplate().execute(
				"UPDATE MATERIAL SET IDENTIFICACAO = '" + identificacao
						+ "' WHERE CDMATERIAL = " + material.getCdmaterial());
	}

	public Material loadForOrdemcompra(Material material) {
		return query().select(
				"material.cdmaterial, material.identificacao, material.nome, listaFornecedor.cdmaterialfornecedor, listaFornecedor.codigo, fornecedor.cdpessoa, fornecedor.nome")
				.leftOuterJoin("material.listaFornecedor listaFornecedor")
				.leftOuterJoin("listaFornecedor.fornecedor fornecedor")
				.where("material = ?", material).unique();
	}

	
	/**
	* M�todo para buscar valorcusto, valorvenda, valorvendamaximo e valorvendaminimo do material
	*
	* @param whereIn
	* @return
	* @since Jun 15, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Material> findForAtualizarmaterial(String whereIn) {

		return query()
				.select(
						"material.cdmaterial, material.valorcusto, material.valorvenda, material.valorvendaminimo, " +
						"material.valorvendamaximo, material.producao, material.frete ")
				.whereIn("material.cdmaterial", whereIn)
				.openParentheses()
				.where("material.producao = ?", Boolean.FALSE)
				.or()
				.where("material.producao is null")
				.closeParentheses()
				.list();
	}
	
	public List<Material> findForAtualizarmaterialmestre(String whereIn) {

		return query()
				.select("material.cdmaterial, material.valorcusto, material.valorvenda, material.valorvendaminimo, materialproducao.consumo, " +
						"material.valorvendamaximo, material.producao, materialproducao.cdmaterialproducao, materialproducao.preco, " +
						"materialproducao.valorconsumo, producao.cdmaterial, material.frete, " +
						"listaMaterialformulacusto.cdmaterialformulacusto ")
				.join("material.listaProducao materialproducao")
				.leftOuterJoin("material.listaMaterialformulacusto listaMaterialformulacusto")
				.join("materialproducao.material producao")
				.whereIn("producao.cdmaterial", whereIn)
				.list();
	}
	
	
	/**
	* M�todo para ajustar preco da venda de acordo com os par�metros
	*  
	*
	* @param material
	* @since Jun 15, 2011
	* @author Luiz Fernando F Silva
	*/
	public void updateMaterialvalor(Material material) {

		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Par�metros inv�lidos");
		}

		getJdbcTemplate().execute("update Material set valorvenda = " + material.getValorvenda() + ",  valorvendaminimo = " + material.getValorvendaminimo() +
				", valorvendamaximo = " + material.getValorvendamaximo() + " where cdmaterial = " + material.getCdmaterial());
	}
	
	/**
	* M�todo para ajustar preco do material (materialmestre) de acordo com os par�metros
	*
	* @param material
	* @since Jun 17, 2011
	* @author Luiz Fernando F Silva
	*/
	public void updateMaterialmestrevalor(Material material){
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Par�metros inv�lidos");
		}
		
		getJdbcTemplate().execute("update Material set valorcusto = " + material.getValorcusto() + ", valorvenda = " + material.getValorvenda() + ",  valorvendaminimo = " + material.getValorvendaminimo() +
				", valorvendamaximo = " + material.getValorvendamaximo() + " where cdmaterial = " + material.getCdmaterial());
	}
	
	/**
	* Verifica se o c�digo contem apenas digito
	*
	* @param filtro
	* @return
	* @since Jun 21, 2011
	* @author Luiz Fernando F Silva
	*/
	public boolean verificaDigito(String codigo){
		
		char[] str = codigo.toCharArray();
		
		if(str != null){
			for(int i = 0; i < str.length; i++){			
				if(!Character.isDigit(str[i])){
					return false;
				}
				
			}
			return true;
		}
		return false;
		
	}
	
	public Material getUnidademedidaValorvenda(Material material){
		if(material == null || material.getCdmaterial() == null){
			throw new SinedException("Par�metros inv�lidos");
		}
		
		return query()
					.select("material.cdmaterial, material.valorvenda, unidademedida.nome")
					.join("material.unidademedida unidademedida")
					.where("material.cdmaterial = ?", material.getCdmaterial()).unique();
	}

	public Material findWithMaterialgrupo(Material material) {
		if(material == null || material.getCdmaterial() == null){
			throw new SinedException("Par�metros inv�lidos");
		}
		return query()
					.select("material.cdmaterial, materialgrupo.cdmaterialgrupo, materialgrupo.nome")
					.join("material.materialgrupo materialgrupo")
					.where("material.cdmaterial = ?", material.getCdmaterial())
					.unique();
					
	}
	
	/**
	* M�todo para buscar os materiais com pre�o de venda para o autocomplete
	*
	* @param q
	* @return
	* @since Sep 26, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Material> findMaterialAutocompleteValorvenda(String q) {
		return query()
					.select("material.cdmaterial, material.nome, material.identificacao")
					.openParentheses()
						.whereLikeIgnoreAll("material.nome", q).or()
						.whereLikeIgnoreAll("material.identificacao", q).or()
				.closeParentheses()
				.where("material.valorvenda is not null")
					.where("material.valorvenda > 0")
					.where("material.ativo = ?", Boolean.TRUE)
					.orderBy("material.nome")
					.autocomplete()
					.list();
	}
	
	/**Metodo pra carregar o Material com o MaterialGrupo para integra��o com o WMS
	 * @author Thiago Augusto
	 * @param material
	 * @return
	 */
	public Material carregaMaterialWMS(Material material) {
		return query().select(
				"material.cdmaterial, material.nome, material.qtdeunidade, material.codigofabricante, " +
				"materialgrupo.cdmaterialgrupo, materialgrupo.nome, material.pesobruto, produto.cdmaterial, produto.altura, " +
				"produto.largura, produto.comprimento, produto.pesobruto, materialcor.cdmaterialcor, materialmestregrade.cdmaterial, " +
				"unidademedida.cdunidademedida, unidademedida.simbolo")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("material.materialproduto produto")
				.leftOuterJoin("material.materialcor materialcor")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.leftOuterJoin("material.unidademedida unidademedida")
				.where("material = ?", material).unique();
	}

	/**
	 * M�todo que busca o valor de venda do material
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Material carregaValorVenda(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido");
		
		return query()
				.select("material.cdmaterial, material.valorvenda")
				.where("material = ?", material)
				.unique();
	}

/**
	 * M�todo que busca os materias para o autocomplete ao realizar a venda
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findMaterialAutocompleteVenda(String q, Empresa empresa) {
		QueryBuilder<Material> query = query();
		query
			.select("material.cdmaterial, material.nome, materialgrupo.nome, materialtipo.nome, material.valorvenda")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.materialtipo materialtipo")
			.openParentheses()
				.whereLikeIgnoreAll("material.nome", q).or()
				.whereLikeIgnoreAll("material.identificacao", q).or()
			.closeParentheses()
			.where("material.ativo = ?", Boolean.TRUE)
			.orderBy("material.nome");
		
		if (empresa != null){
			query
				.leftOuterJoin("material.listaMaterialempresa listaMaterialempresa", true)
				.leftOuterJoin("listaMaterialempresa.empresa empresa", true)
				.openParentheses()
					.where("empresa = ?", empresa)
					.or()
					.where("listaMaterialempresa is null")
				.closeParentheses();
		}
		
		if(SinedUtil.isRestricaoFornecedorProdutoUsuarioLogado()){
			query.leftOuterJoin("material.listaFornecedor listaFornecedor");
			query.leftOuterJoin("listaFornecedor.fornecedor fornecedor");
			query.leftOuterJoin("fornecedor.listaColaboradorFornecedor listaColaboradorFornecedor");
			query.leftOuterJoin("listaColaboradorFornecedor.colaborador colaborador");
			query.openParentheses()
				.where("listaFornecedor is null")
				.or()
				.where("listaColaboradorFornecedor is null")
				.or()
				.where("colaborador = ?", SinedUtil.getUsuarioComoColaborador())
				.closeParentheses();
		}
		
		return query.autocomplete().list();
	}
	

	
	/**
	 * M�todo que busca os materias para o autocomplete baseado na Empresa escolhida
	 *
	 * @param q
	 * @return
	 * @author Francisco Lourandes
	 */
	public List<Material> findMaterialByEmpresaAutocomplete(String q, Empresa empresa) {
		QueryBuilder<Material> query = query();
		query
			.select("material.cdmaterial, material.nome, material.valorvenda, material.qtdeunidade, materialtipo.cdmaterialtipo")
			.leftOuterJoin("material.materialtipo materialtipo")
			.leftOuterJoin("material.listaMaterialempresa listaMaterialempresa")
			.leftOuterJoin("listaMaterialempresa.empresa empresa")
			.where("material.ativo = true")
			.where("material.servico = ?", Boolean.TRUE)
			.openParentheses()
			.where("listaMaterialempresa is null").or()
			.where("empresa = ?", empresa)
			.closeParentheses()
			.openParentheses()
				.whereLikeIgnoreAll("material.nome", q).or()
				.whereLikeIgnoreAll("material.identificacao", q).or()
			.closeParentheses()
			.orderBy("material.nome").list();
		return query.autocomplete().list();
	}
	
	
	/**
	 * M�todo que busca o material para pesquisa de venda
	 *
	 * @param q
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 * @since 30/01/2014
	 */
	public List<Material> findMaterialAutocompleteVendaForPesquisa(String q, Empresa empresa, Boolean kitFlexivel) {
		QueryBuilder<Material> query = query();
		query
			.select("material.cdmaterial, material.nome, material.identificacao, material.valorvenda, " +
					"materialgrupo.cdmaterialgrupo, materialgrupo.nome, materialtipo.nome, materialtipo.cdmaterialtipo, " +
					"materialmestregrade.cdmaterial, materialmestregrade.identificacao")
			.leftOuterJoin("material.materialmestregrade materialmestregrade")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.materialtipo materialtipo")
			.openParentheses()
				.where("material.tipoitemsped is null")
				.or()
				.where("material.tipoitemsped <> ?", Tipoitemsped.USO_CONSUMO)
			.closeParentheses()
			.openParentheses()
				.whereLikeIgnoreAll("material.nome", q).or()
				.whereLikeIgnoreAll("material.identificacao", q).or()
			.closeParentheses()
			.openParentheses()
				.where("materialgrupo.gradeestoquetipo is null")
				.or()
				.openParentheses()
					.openParentheses()
						.where("materialgrupo.gradeestoquetipo = ?", Gradeestoquetipo.ITEMGRADE)
						.openParentheses()
							.where("materialgrupo.pesquisasomentemestre is null ").or()
							.where("materialgrupo.pesquisasomentemestre <> true ")
						.closeParentheses()
					.closeParentheses()
					.or()
					.openParentheses()
						.where("materialgrupo.gradeestoquetipo = ?", Gradeestoquetipo.ITEMGRADE)
						.where("materialgrupo.pesquisasomentemestre = true ")
						.where("materialmestregrade is null")
					.closeParentheses()
				.closeParentheses()
				.or()
				.openParentheses()
					.where("materialgrupo.gradeestoquetipo = ?", Gradeestoquetipo.MESTRE)
					.where("materialmestregrade is null")
				.closeParentheses()
			.closeParentheses()
			.where("material.ativo = ?", Boolean.TRUE)
			.orderBy("material.nome");
		
		if (empresa != null){
			query
				.leftOuterJoin("material.listaMaterialempresa listaMaterialempresa", true)
				.openParentheses()
					.where("listaMaterialempresa.empresa = ?", empresa)
					.or()
					.where("listaMaterialempresa is null")
				.closeParentheses();
		}
		
		if(kitFlexivel != null && kitFlexivel){
			query.where("coalesce(material.kitflexivel, false) = false");
		}
		
		if(SinedUtil.isRestricaoFornecedorProdutoUsuarioLogado()){
			query.leftOuterJoin("material.listaFornecedor listaFornecedor");
			query.leftOuterJoin("listaFornecedor.fornecedor fornecedor");
			query.leftOuterJoin("fornecedor.listaColaboradorFornecedor listaColaboradorFornecedor");
			query.leftOuterJoin("listaColaboradorFornecedor.colaborador colaborador");
			query.openParentheses()
				.where("listaFornecedor is null")
				.or()
				.where("listaColaboradorFornecedor is null")
				.or()
				.where("colaborador = ?", SinedUtil.getUsuarioComoColaborador())
				.closeParentheses();
		}
		
		return query.autocomplete().list();
	}
	
	public List<Material> findMaterialAutocompleteFluxocompra(String q, Empresa empresa, Boolean considerarRegraGrade) {
		return findMaterialAutocompleteFluxocompra(q, empresa, considerarRegraGrade, false);
	}
	
	/**
	 * M�todo que busca o material de acordo com empresa
	 *
	 * @param q
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findMaterialAutocompleteFluxocompra(String q, Empresa empresa, Boolean considerarRegraGrade, Boolean desconsiderarMaterialQueControlaLote) {
		QueryBuilder<Material> query = query();
		query
			.select("material.cdmaterial, material.nome, material.identificacao, material.servico," +
					"materialmestregrade.cdmaterial, materialmestregrade.identificacao")
			.leftOuterJoin("material.materialmestregrade materialmestregrade")
			.openParentheses()
				.whereLikeIgnoreAll("material.nome", q).or()
				.whereLikeIgnoreAll("material.identificacao", q)
			.closeParentheses()
			.where("material.ativo = ?", Boolean.TRUE);
		if(desconsiderarMaterialQueControlaLote){
			query.where("coalesce(material.obrigarlote, false) = false");
		}
		
		if(considerarRegraGrade != null && considerarRegraGrade){
			query
				.leftOuterJoin("material.materialgrupo materialgrupo", true)
				.openParentheses()
					.where("materialgrupo.gradeestoquetipo is null")
					.or()
					.openParentheses()
						.openParentheses()
							.where("materialgrupo.gradeestoquetipo = ?", Gradeestoquetipo.ITEMGRADE)
							.openParentheses()
								.where("materialgrupo.pesquisasomentemestre is null ").or()
								.where("materialgrupo.pesquisasomentemestre <> true ")
							.closeParentheses()
						.closeParentheses()
						.or()
						.openParentheses()
							.where("materialgrupo.gradeestoquetipo = ?", Gradeestoquetipo.ITEMGRADE)
							.where("materialgrupo.pesquisasomentemestre = true ")
							.where("materialmestregrade is null")
						.closeParentheses()
					.closeParentheses()
					.or()
					.openParentheses()
						.where("materialgrupo.gradeestoquetipo = ?", Gradeestoquetipo.MESTRE)
						.where("materialmestregrade is null")
					.closeParentheses()
				.closeParentheses();
		}
			
		if (empresa != null){
			query
				.leftOuterJoin("material.listaMaterialempresa listaMaterialempresa", true)
				.openParentheses()
					.where("listaMaterialempresa.empresa = ?", empresa)
					.or()
					.where("listaMaterialempresa is null")
				 .closeParentheses();
		}
		
		return query
				.orderBy("material.identificacaoordenacao, material.nome")
				.autocomplete()
				.list();
	}
	
	/**
	 * M�todo que busca os materias para o autocomplete da tabela de pre�o
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findMaterialAutocompleteTabelapreco(String q) {
		QueryBuilder<Material> query = query();
		query
			.select("material.cdmaterial, material.nome, materialgrupo.nome, materialtipo.nome")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.materialtipo materialtipo")
			.openParentheses()
				.whereLikeIgnoreAll("material.nome", q).or()
				.whereLikeIgnoreAll("material.identificacao", q).or()
			.closeParentheses()
			.where("material.ativo = ?", Boolean.TRUE)
			.orderBy("material.nome");
		
		return query.autocomplete().list();
	}
	
	/**
	 * 
	 * M�todo para buscar todos os materiais que tenham oportunidades associados.
	 *
	 *@author Thiago Augusto
	 *@date 02/02/2012
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Material> findListMaterialOportunidade(){
		StringBuilder sql = new StringBuilder();
		sql.append("select distinct(m.cdmaterial) as CDMATERIAL, m.nome as NOME from material m inner join oportunidadematerial  mc ");
		sql.append("	on m.cdmaterial = mc.cdmaterial");
		SinedUtil.markAsReader();
		List<Material> lista = getJdbcTemplate().query(sql.toString(),
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						Material material = new Material(rs
								.getInt("CDMATERIAL"), rs.getString("NOME"));
						return material;
					}
				});
		return lista;
	}

	/**
	 * M�todo para verificar se o material pertence a alguma tarefa
	 * 
	 * @param tarefa
	 * @param material
	 * @return
	 */
	public Boolean isMaterialTarefa(Tarefa tarefa, Material material) {
		if(tarefa == null || tarefa.getCdtarefa() == null || material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		QueryBuilder<Material> query = query()
		.select("material.cdmaterial, material.nome")
		.where("material.cdmaterial in (select m.cdmaterial from Tarefarecursogeral trg join trg.material m join trg.tarefa t where t.cdtarefa = " + tarefa.getCdtarefa() + " and m.cdmaterial = " + material.getCdmaterial() + ")");
		
		return query.list().size() > 0;
	}
		/**
	 * M�todo que busca materiais de acordo com o c�digo passado por par�metro
	 * 
	 * @param codigo
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findWithCod(String codigo) {
		if (codigo == null || "".equals(codigo)) {
			throw new SinedException("Cdmaterial inv�lido.");
		}
		
		QueryBuilder<Material> query = query();
		
		query
			.select("distinct material.cdmaterial, material.nome")
			.join("material.materialgrupo grupo")
			.join("material.unidademedida unidademedida")
			.join("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.contagerencial contagerencial")
			.leftOuterJoin("material.listaFornecedor listaFornecedor")
			.where("material.ativo = true");
			
		query.openParentheses()
			.whereLikeIgnoreAll("material.identificacao", codigo);			
				
		if(verificaDigito(codigo)){
			try {
				query.or().where("material.cdmaterial = ?", Integer.valueOf(codigo));
			} catch (Exception e) {}
		}
		
		query.or().whereLikeIgnoreAll("listaFornecedor.codigo", codigo);
		query.or();
		query.whereLikeIgnoreAll("material.referencia", codigo);
		query.or();
		query.whereLikeIgnoreAll("material.codigofabricante", codigo);
		query.or();
		query.whereLikeIgnoreAll("material.codigobarras", codigo);
		
		query.closeParentheses();
		
		return query.list();
	}
	
	/**
	 * 
	 * M�todo para carregar apenas para verificar se o material � um servi�o ou n�o para a tela de entrega
	 *
	 *@author Thiago Augusto
	 *@date 28/03/2012
	 * @param material
	 * @return
	 */
	public Material carregaMaterialServico(Material material) {
		return querySined().removeUseWhere().select(
				"material.cdmaterial, material.servico")
				.where("material = ?", material).unique();
	}
	
	
	/**
     * Busca os dados para o cache da tela de pedido de venda offline
     * @author Igor Silv�rio Costa
	 * @date 20/04/2012
	 * 
	 */
	public List<Material> findForPVOffline() {
		List<Material> lista = query()
		.select("material.cdmaterial, material.ativo, material.identificacao, material.nome, material.referencia, " +
				"materialgrupo.cdmaterialgrupo, materialgrupo.nome, material.epi, material.produto, material.patrimonio, material.servico, " +
				"unidademedida.cdunidademedida, material.valorcusto, material.valorvenda, material.valorvendaminimo, material.valorvendamaximo, " +
				"listaFornecedor.codigo, empresa.cdpessoa, material.vendapromocional, material.naoexibirminmaxvenda, " +
				"unidademedidaconversao.cdunidademedida, listaMaterialunidademedida.cdmaterialunidademedida," +
				"listaMaterialunidademedida.fracao, listaMaterialunidademedida.qtdereferencia, listaMaterialunidademedida.mostrarconversao," +
				"listaMaterialunidademedida.prioridadevenda, listaMaterialunidademedida.prioridadecompra, listaMaterialunidademedida.valorunitario, listaMaterialunidademedida.qtdereferencia ")
		.leftOuterJoin("material.materialgrupo materialgrupo")
		.leftOuterJoin("material.unidademedida unidademedida")
		.leftOuterJoin("material.listaFornecedor listaFornecedor")
		.leftOuterJoin("material.listaMaterialempresa listaMaterialempresa")
		.leftOuterJoin("listaMaterialempresa.empresa empresa")
		.leftOuterJoin("material.listaMaterialunidademedida listaMaterialunidademedida")
		.leftOuterJoin("listaMaterialunidademedida.unidademedida unidademedidaconversao")
		.list();
		
//		StringBuilder sb = new StringBuilder();
//		sb.append("m.cdmaterial, m.identificacao, m.nome, m.referencia, " +
//				"materialgrupo.nome, m.epi, material.produto, m.patrimonio, m.servico, " +
//				"unidademedida.simbolo, m.valorcusto, m.valorvenda, " +
//				"listaFornecedor.codigo, (").append(entrada).append(") as qtentrada, (").append(saida).append(") as qtsaida ")
//		.append(" from material m ")
//		.append(" left outer join materialgrupo materialgrupo on materialgrupo.cdmaterialgrupo=m.cdmaterialgrupo ")
//		.append(" left outer join unidademedida unidademedida on m.cdunidademedida=unidademedida.cdunidademedida ")
//		.append(" left outer join materialfornecedor listaFornecedor on m.listaFornecedor.cdmaterial=m.cdmaterial ");
		
		
		return lista;
	}
	
	public List<Material> findForAndroid(String whereIn) {
		
		return findForAndroid(whereIn, null);
	}
	public List<Material> findForAndroid(String whereIn, Timestamp dtUltimaSincronziacao) {
		List<Material> lista = query()
		.select("material.cdmaterial, material.ativo, material.identificacao, material.nome, material.referencia, material.exibiritenskitvenda, material.vendapromocional, material.producao, " +
				"materialgrupo.cdmaterialgrupo, materialgrupo.nome, material.epi, material.produto, material.patrimonio, material.servico, material.tipoitemsped, " +
				"unidademedida.cdunidademedida, material.valorcusto, material.valorvenda, material.valorvendaminimo, material.valorvendamaximo, " +
				"listaFornecedor.codigo, empresa.cdpessoa, material.vendapromocional, material.naoexibirminmaxvenda, " +
				"unidademedidaconversao.cdunidademedida, listaMaterialunidademedida.cdmaterialunidademedida," +
				"listaMaterialunidademedida.fracao, listaMaterialunidademedida.qtdereferencia, listaMaterialunidademedida.mostrarconversao," +
				"listaMaterialunidademedida.prioridadevenda, listaMaterialunidademedida.prioridadecompra, listaMaterialunidademedida.valorunitario, listaMaterialunidademedida.qtdereferencia," +
				"materialcoleta.cdmaterial, materialcoleta.nome, materialproduto.altura, materialproduto.largura, materialproduto.comprimento, arquivo.cdarquivo ")
		.leftOuterJoin("material.arquivo arquivo")
		.leftOuterJoin("material.materialgrupo materialgrupo")
		.leftOuterJoin("material.unidademedida unidademedida")
		.leftOuterJoin("material.materialcoleta materialcoleta")
		.leftOuterJoin("material.listaFornecedor listaFornecedor")
		.leftOuterJoin("material.listaMaterialempresa listaMaterialempresa")
		.leftOuterJoin("listaMaterialempresa.empresa empresa")
		.leftOuterJoin("material.listaMaterialunidademedida listaMaterialunidademedida")
		.leftOuterJoin("listaMaterialunidademedida.unidademedida unidademedidaconversao")
		.leftOuterJoin("material.materialproduto materialproduto")
		.whereIn("material.cdmaterial", whereIn)
		.where("material.dtaltera >= ?", dtUltimaSincronziacao, dtUltimaSincronziacao != null)
//		.where("material.ativo = ?", Boolean.TRUE, StringUtils.isBlank(whereIn))
		.list();
		return lista;
	}
	
	public Material carregaMaterialForMovimentacaoVenda(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("material.cdmaterial, material.producao, material.produto, listaProducao.cdmaterialproducao, " +
						"materialproducao.cdmaterial, materialproducao.produto, listaProducao.consumo, " +
						"listaProducao.valorconsumo, materialproducao.nome")
				.join("material.listaProducao listaProducao")
				.join("listaProducao.material materialproducao")
				.where("material = ?", material)
				.unique();
				
	}
	
	/**
     * Busca a quantidade de material disponivel por tipo de movimenta��o de todos os materiais cadastrados 
     * @author Igor Silv�rio Costa
	 * @date 20/04/2012
	 * @param tipo Tipo de Movimenta��o estoque
	 */
	@SuppressWarnings("unchecked")
	public List<GenericBean> getListaMaterialQtdedisponivel(Movimentacaoestoquetipo tipo){
		StringBuilder entrada = new StringBuilder().append("select sum(movimentacaoestoque.qtde) as qtde, movimentacaoestoque.cdmaterial")
		.append(" from movimentacaoestoque")
		.append(" where movimentacaoestoquetipo.cdmovimentacaoestoquetipo = ").append(tipo.getCdmovimentacaoestoquetipo())
		.append(" and movimentacaoestoque.dtcancelamento is null")
		.append(" group by movimentacaoestoque.cdmaterial");


		SinedUtil.markAsReader();
		List<GenericBean> listaEntrada = getJdbcTemplate().query(entrada.toString(), new RowMapper(){
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				GenericBean gb = new GenericBean();
				gb.setId(rs.getInt("cdmaterial"));
				gb.setValue(rs.getDouble("qtde"));
				return gb;
			}
		});
		
		return listaEntrada;
	}
	
	/**
     * Busca a quantidade de material disponivel por tipo de movimenta��o de todos os materiais cadastrados 
     * @author Luiz Fernando
	 * @date 04/09/2012
	 * @param tipo Tipo de Movimenta��o estoque
	 */
	@SuppressWarnings("unchecked")
	public List<Movimentacaoestoque> getListaMaterialQtdedisponivelPorLocal(Movimentacaoestoquetipo tipo){
		StringBuilder entrada = new StringBuilder().append("select sum(movimentacaoestoque.qtde) as qtde, movimentacaoestoque.cdmaterial, movimentacaoestoque.cdlocalarmazenagem")
		.append(" from movimentacaoestoque")
		.append(" where  movimentacaoestoque.dtcancelamento is null and movimentacaoestoque.cdmovimentacaoestoquetipo = ").append(tipo.getCdmovimentacaoestoquetipo())
		.append(" group by movimentacaoestoque.cdmaterial, movimentacaoestoque.cdlocalarmazenagem;");

		SinedUtil.markAsReader();
		List<Movimentacaoestoque> listaEntrada = getJdbcTemplate().query(entrada.toString(), new RowMapper(){
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Movimentacaoestoque me = new Movimentacaoestoque();
				me.setCdmaterial(rs.getInt("cdmaterial"));
				me.setQtde(rs.getDouble("qtde"));
				me.setCdlocalarmazenagem(rs.getInt("cdlocalarmazenagem"));
				return me;
			}
		});
		
		return listaEntrada;
	}
	
	/**
     * Busca a quantidade de saida de material de todos os materiais cadastrados 
     * @author Igor Silv�rio Costa
	 * @date 20/04/2012
	 * @param tipo Tipo de Movimenta��o estoque
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	private Map<Integer, Integer> getMapQtSaida(){
		StringBuilder entrada = new StringBuilder().append("select sum(movimentacaoestoque.qtde) as qtde, movimentacaoestoque.cdmaterial")
		.append(" from movimentacaoestoque")
		.append(" where movimentacaoestoquetipo = 2")
		.append(" and movimentacaoestoque.dtcancelamento is null");

		SinedUtil.markAsReader();
		List<GenericBean> listaEntrada = getJdbcTemplate().query(entrada.toString(), new RowMapper(){
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				GenericBean gb = new GenericBean();
				gb.setId(rs.getInt("cdmaterial"));
				gb.setValue(rs.getInt("qtde"));
				return gb;
			}
		});
		
		Map<Integer, Integer> mapa = new HashMap<Integer, Integer>();
		for (GenericBean genericBean : listaEntrada) 
			mapa.put((Integer) genericBean.getId(), (Integer) genericBean.getValue());
		
		return mapa;
	}

	/**
	 * M�todo que busca listaMaterialunidademedida do material
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Material findListaMaterialunidademedida(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido");
		
		return querySined()
				.setUseWhereEmpresa(false)
				.select("material.cdmaterial, material.nome, material.identificacao, material.peso, material.pesobruto, material.producao, " +
						"material.valorvenda, material.valorvendaminimo, material.valorvendamaximo, " +
						"listaMaterialunidademedida.cdmaterialunidademedida, " +
						"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.casasdecimaisestoque, unidademedida.simbolo, " +
						"unidademedida.casasdecimaisestoque, " +
						"listaMaterialunidademedida.qtdereferencia, listaMaterialunidademedida.fracao, " +
						"listaMaterialunidademedida.valorunitario, unidademedidamaterial.cdunidademedida, " +
						"listaMaterialunidademedida.mostrarconversao, unidademedidamaterial.nome, unidademedidamaterial.casasdecimaisestoque," +
						"listaMaterialunidademedida.prioridadevenda, listaMaterialunidademedida.prioridadecompra, listaMaterialunidademedida.prioridadeproducao, unidademedidamaterial.simbolo, listaMaterialunidademedida.valormaximo, " +
						"listaMaterialunidademedida.valorminimo, listaMaterialunidademedida.considerardimensoesvendaconversao," +
						"listaMaterialunidademedida.pesoliquidocalculadopelaformula ")
				.leftOuterJoin("material.listaMaterialunidademedida listaMaterialunidademedida")
				.leftOuterJoin("listaMaterialunidademedida.unidademedida unidademedida")
				.leftOuterJoin("material.unidademedida unidademedidamaterial")
				.where("material = ?", material)
				.unique();
	}
	
	public Material loadMaterialunidademedida(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido");
		
		return querySined()
				.setUseWhereEmpresa(false)
				.setUseWhereProjeto(false)
				.setUseWhereFornecedorEmpresa(false)
				.setUseWhereClienteEmpresa(false)
				.select("material.cdmaterial, listaMaterialunidademedida.cdmaterialunidademedida, material.nome, " +
						"unidademedida.cdunidademedida, unidademedida.nome, listaMaterialunidademedida.fracao,  " +
						"listaMaterialunidademedida.qtdereferencia,listaMaterialunidademedida.qtdereferencia, " +
						"listaMaterialunidademedida.valorunitario, unidademedidamaterial.cdunidademedida, " +
						"listaMaterialunidademedida.valormaximo, listaMaterialunidademedida.valorminimo, " +
						"listaMaterialunidademedida.mostrarconversao, unidademedidamaterial.nome, " +
						"listaMaterialunidademedida.prioridadevenda, listaMaterialunidademedida.prioridadecompra, unidademedida.simbolo, unidademedidamaterial.simbolo," +
						"unidademedidarelacionada.cdunidademedida, unidademedidarelacionada.nome, unidademedidarelacionada.simbolo, listaUnidademedidaconversao.fracao, " +
						"listaUnidademedidaconversao.qtdereferencia ")
				.leftOuterJoin("material.listaMaterialunidademedida listaMaterialunidademedida")
				.leftOuterJoin("listaMaterialunidademedida.unidademedida unidademedida")
				.leftOuterJoin("material.unidademedida unidademedidamaterial")
				.leftOuterJoin("unidademedidamaterial.listaUnidademedidaconversao listaUnidademedidaconversao")
				.leftOuterJoin("listaUnidademedidaconversao.unidademedidarelacionada unidademedidarelacionada")
				.where("material = ?", material)
				.unique();
	}
	
	/**
	 * M�todo que carrega o material com o tipo de material
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 17/01/2014
	 */
	public Material loadWithMaterialtipo(Material material) {
		return query()
				.select("material.cdmaterial, material.nome, materialtipo.cdmaterialtipo")
				.leftOuterJoin("material.materialtipo materialtipo")
				.where("material = ?", material)
				.unique();
	}

	/**
	 * M�todo que busca as informa��es da comiss�o do materialgrupo de acordo com os par�metros
	 *
	 * @param material
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 */
	public Material findForComissaovendaGrupomaterial(Material material, Colaborador colaborador) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");		
		
		return query()
			.select("materialgrupo.cdmaterialgrupo, material.cdmaterial, material.nome, material.valorvenda, unidademedida.cdunidademedida, " +
					"listaMaterialgrupocomissaovenda.cdmaterialgrupocomissaovenda, listaMaterialgrupocomissaovenda.comissaopara, " +
					"cargo.cdcargo, documentotipo.cddocumentotipo, " +
					"comissionamento.cdcomissionamento, comissionamento.comissionamentotipo, comissionamento.considerardiferencapreco, colaborador.cdpessoa, " +
					"comissionamento.deduzirvalor, comissionamento.quantidade, comissionamento.percentual, comissionamento.valor, " +
					"comissionamento.percentualdivisao, comissionamento.valordivisao, " +
					"listaComissionamentopessoa.cdcomissionamentopessoa, colaborador.cdpessoa, colaborador.nome, " +
					"listaComissionamentopessoa.recebecomissao, comissionamento.tipobccomissionamento, " +
					"comissionamento.considerardiferencapagamento, comissionamento.criteriocomissionamento," +
					"pedidovendatipo.cdpedidovendatipo, fornecedor.cdpessoa," +
					"listaComissionamentofaixadesconto.cdcomissionamentofaixadesconto, listaComissionamentofaixadesconto.percentualdescontoate, " +
					"listaComissionamentofaixadesconto.percentualcomissao  ")
			.join("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.unidademedida unidademedida")
			.join("materialgrupo.listaMaterialgrupocomissaovenda listaMaterialgrupocomissaovenda")
			.leftOuterJoin("listaMaterialgrupocomissaovenda.cargo cargo")
			.leftOuterJoin("listaMaterialgrupocomissaovenda.comissionamento comissionamento")
			.leftOuterJoin("listaMaterialgrupocomissaovenda.documentotipo documentotipo")
			.leftOuterJoin("comissionamento.listaComissionamentopessoa listaComissionamentopessoa")
			.leftOuterJoin("listaComissionamentopessoa.colaborador colaborador")
			.leftOuterJoin("listaMaterialgrupocomissaovenda.pedidovendatipo pedidovendatipo")
			.leftOuterJoin("listaMaterialgrupocomissaovenda.fornecedor fornecedor")
			.leftOuterJoin("comissionamento.listaComissionamentofaixadesconto listaComissionamentofaixadesconto")
			.where("material = ?", material)
			.openParentheses()
			.where("cargo.cdcargo in (select c.cdcargo " +
										"from Colaboradorcargo cc " +
										"join cc.cargo c " +
										"join cc.colaborador col " +
										"where col.cdpessoa = " + colaborador.getCdpessoa() + " ) ")
			.or()
			.where("cargo is null")
			.closeParentheses()
			.unique();
	}
	
	public Material findForComissaovenda(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");		
		
		return query()
			.select("material.cdmaterial, material.nome, materialgrupo.cdmaterialgrupo, material.valorvenda ")
			.join("material.materialgrupo materialgrupo")
			.where("material = ?", material)
			.unique();
	}

	/**
	 * M�todo que busca as informa��es da comiss�o do materialgrupo de acordo com o fornecedor e o material informado.
	 *
	 * @param material
	 * @param fornecedor
	 * @return
	 * @author Jo�o Vitor
	 */
	public Material findForComissaovendaGrupomaterialByFornecedor(Material material, Fornecedor fornecedor) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");		
		
		return query()
			.select("materialgrupo.cdmaterialgrupo, material.cdmaterial, material.valorvenda, " +
					"listaMaterialgrupocomissaovenda.cdmaterialgrupocomissaovenda, cargo.cdcargo, documentotipo.cddocumentotipo, " +
					"comissionamento.cdcomissionamento, comissionamento.comissionamentotipo, colaborador.cdpessoa, " +
					"comissionamento.deduzirvalor, comissionamento.quantidade, comissionamento.percentual, comissionamento.valor, " +
					"comissionamento.percentualdivisao, comissionamento.valordivisao, " +
					"listaComissionamentopessoa.cdcomissionamentopessoa, colaborador.cdpessoa, colaborador.nome, " +
					"listaComissionamentopessoa.recebecomissao, comissionamento.tipobccomissionamento, " +
					"comissionamento.considerardiferencapagamento, comissionamento.criteriocomissionamento, comissionamento.considerardiferencapreco, " +
					"pedidovendatipo.cdpedidovendatipo, fornecedor.cdpessoa  ")
			.join("material.materialgrupo materialgrupo")
			.join("materialgrupo.listaMaterialgrupocomissaovenda listaMaterialgrupocomissaovenda")
			.leftOuterJoin("listaMaterialgrupocomissaovenda.cargo cargo")
			.leftOuterJoin("listaMaterialgrupocomissaovenda.comissionamento comissionamento")
			.leftOuterJoin("listaMaterialgrupocomissaovenda.documentotipo documentotipo")
			.leftOuterJoin("comissionamento.listaComissionamentopessoa listaComissionamentopessoa")
			.leftOuterJoin("listaComissionamentopessoa.colaborador colaborador")
			.leftOuterJoin("listaMaterialgrupocomissaovenda.pedidovendatipo pedidovendatipo")
			.leftOuterJoin("listaMaterialgrupocomissaovenda.fornecedor fornecedor")
			.where("material = ?", material)
			.openParentheses()
			.where("fornecedor = ?", fornecedor)
			.or()
			.where("fornecedor is null")
			.closeParentheses()
			.unique();
	}
	
	/**
	 * M�todo que busca a conta gerencial e centro de custo do material
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Material loadComContagerencialCentrocusto(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");		
		
		return query()
			.select("material.cdmaterial, contagerencialvenda.cdcontagerencial, centrocustovenda.cdcentrocusto, material.producao ")
			.leftOuterJoin("material.contagerencialvenda contagerencialvenda")
			.leftOuterJoin("material.centrocustovenda centrocustovenda")
			.where("material = ?", material)
			.unique();
	}
	
	/**
	 * Busca o material pelo nome.
	 *
	 * @param nome
	 * @return
	 * @since 26/05/2012
	 * @author Rodrigo Freitas
	 */
	public Material findByNome(String nome) {
		if(nome == null || nome.equals("")){
			throw new SinedException("Nome n�o pode ser nulo.");
		}
		List<Material> lista = query()
					.select("material.cdmaterial, material.nome")
					.where("upper(material.nome) = ?", nome.toUpperCase())
					.list();
		
		if(lista == null || lista.size() == 0){
			return null;
		} else if(lista.size() > 1){
			throw new SinedException("Existem mais de um material com o nome: " + nome);
		} else return lista.get(0);
	}

	/**
	 * M�todo que verifica se existe uma refer�ncia com o nomenf diferente
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isReferenciaDiferenteNomenf(Material material) {
		if(material == null)
			throw new SinedException("Par�metro inv�lido.");
		
		if(material.getNomenf() == null){
			return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Material.class)				
			.where("material.cdmaterial <> ?", material.getCdmaterial())
			.where("lower(material.referencia) = lower(?)", material.getReferencia() )
			.openParentheses()
			.where("material.nomenf is not null")
			.closeParentheses()
			.unique() > 0;
		}else {
			return newQueryBuilder(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(Material.class)				
					.where("material.cdmaterial <> ?", material.getCdmaterial())
					.where("lower(material.referencia) = lower(?)", material.getReferencia() )
					.where("lower(COALESCE(material.nomenf,'')) <> lower(?)", material.getNomenf() )
					.unique() > 0;
		}
	}

	/**
	 * M�todo que busca os materiais com convers�o de unidade de medida
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findForRelatorioComUnidademedidaconversao(String whereIn, Boolean mostrarconversao) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Material> query = query()
				.select("distinct material.cdmaterial, listaMaterialunidademedida.cdmaterialunidademedida, listaMaterialunidademedida.fracao, listaMaterialunidademedida.qtdereferencia, " +
						"listaMaterialunidademedida.mostrarconversao, unidademedida.nome, unidademedida.cdunidademedida, unidademedidaprincipal.nome")
				.join("material.listaMaterialunidademedida listaMaterialunidademedida")
				.join("listaMaterialunidademedida.unidademedida unidademedida")
				.join("material.unidademedida unidademedidaprincipal")
//				.whereIn("material.cdmaterial", whereIn)
				.where("listaMaterialunidademedida.mostrarconversao = ?", mostrarconversao);
		
		SinedUtil.quebraWhereIn("material.cdmaterial", whereIn, query);
		
		return query.list();
	}
	
	public List<Material> findForComUnidademedidaconversao(String whereIn, Boolean mostrarconversao) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Material> query = query()
				.select("distinct material.cdmaterial, listaMaterialunidademedida.cdmaterialunidademedida, listaMaterialunidademedida.fracao, listaMaterialunidademedida.qtdereferencia, " +
						"listaMaterialunidademedida.mostrarconversao, unidademedida.nome, unidademedida.cdunidademedida, unidademedidaprincipal.cdunidademedida, unidademedidaprincipal.nome, " +
						"listaMaterialunidademedida.mostrarconversao")
				.leftOuterJoin("material.listaMaterialunidademedida listaMaterialunidademedida")
				.leftOuterJoin("listaMaterialunidademedida.unidademedida unidademedida")
				.join("material.unidademedida unidademedidaprincipal")
				.whereIn("material.cdmaterial", whereIn);
		SinedUtil.quebraWhereIn("material.cdmaterial", whereIn, query);
		return query.list();
	}

	/**
	 * M�todo que retorna true se o material tem altura ou largura, sen�o retorna false
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isMaterialComAlturaLargura(Material material) {
		if(material == null || material.getCdmaterial() == null)
			return Boolean.FALSE;
		
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Material.class)				
			.where("material.cdmaterial = ?", material.getCdmaterial())
			.openParentheses()
			.where("material.largura is not null")
			.or()
			.where("material.altura is not null")
			.closeParentheses()
			.unique() > 0; 
	}
	
	public Boolean isKit(Material material) {
		if(material == null || material.getCdmaterial() == null)
			return Boolean.FALSE;
		
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Material.class)				
			.where("material.cdmaterial = ?", material.getCdmaterial())
			.where("material.vendapromocional = true")
			.unique() > 0; 
	}

	/**
	 * M�todo utilizado para autocompelte que busca os materiais de acordo com a classe 
	 *
	 * @param q
	 * @param lista
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findAutocompleteMaterialsimilar(String q,	List<Materialclasse> lista) {
		String funcaoTiraacento = Neo.getApplicationContext().getConfig()
		.getProperties().getProperty("funcaoTiraacento");

		QueryBuilder<Material> query = query()
			.select("material.cdmaterial, material.nome")			
			.where("UPPER(" + funcaoTiraacento + "(material.nome)) LIKE '%'||?||'%'", Util.strings.tiraAcento(q).toUpperCase())
			.orderBy("material.nome");
		
		if(lista != null && lista.size() > 0){
			query.openParentheses();
			for (Materialclasse materialclasse : lista) {
				if(materialclasse.equals(Materialclasse.PRODUTO)){
					query.where("material.produto = true").or();
				} else if(materialclasse.equals(Materialclasse.EPI)){
					query.where("material.epi = true").or();
				} else if(materialclasse.equals(Materialclasse.PATRIMONIO)){
					query.where("material.patrimonio = true").or();
				} else if(materialclasse.equals(Materialclasse.SERVICO)){
					query.where("material.servico = true").or();
				}
			}
			query.closeParentheses();
		}

		return query.list();
	}

	/**
	 * Busca os materiais que tenha valor de venda cadastrado para o auto-complete.
	 *
	 * @param q
	 * @return
	 * @since 20/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Material> findMaterialValorvendaAutocomplete(String q) {
		return query()
					.select("material.cdmaterial, material.nome, material.identificacao")
					.openParentheses()
					.whereLikeIgnoreAll("material.nome", q)
					.or()
					.whereLikeIgnoreAll("material.identificacao", q)
					.closeParentheses()
					.where("material.valorvenda is not null")
					.where("material.valorvenda > 0")
					.orderBy("material.nome")
					.list();
	}
	
	
	public List<Material> findMaterialComMaterialproducao(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Material n�o pode ser nulo");
		
		return query()
				.select("material.cdmaterial, material.nome, material.producao, material.frete, materialproducao.cdmaterialproducao, " +
						"materialproducao.consumo, materialsecundario.cdmaterial, materialproducaoformula.ordem, materialproducaoformula.identificador, " +
						"materialproducaoformula.formula, materialproducaoformula.cdmaterialproducaoformula, materialproducao.altura, materialproducao.largura, " +
						"materialproducao.preco, materialproducao.valorconsumo, unidademedidaMS.cdunidademedida, unidademedidaMS.nome, unidademedida.cdunidademedida, unidademedida.nome, materialsecundario.valorcusto, " +
						"materialsecundario.nome, materialsecundario.identificacao, listaMaterialsimilar.cdmaterialsimilar, materialproducao.ordemexibicao, materialsecundario.valorvenda," +
						"materialsecundario.servico, material.valorvenda, materialproducao.opcional, materialproducao.exibirvenda, " +
						"materialproducao.discriminarnota, unidademedidasecundario.cdunidademedida, unidademedidasecundario.nome, unidademedidasecundario.simbolo, " +						
						"material.identificacao, material.produto, material.servico, " +
						"material.extipi, material.ncmcompleto, material.codigobarras, ncmcapitulo.cdncmcapitulo, ncmcapitulo.descricao, " +
						"unidademedida.simbolo, material.patrimonio, material.epi, unidademedida.cdunidademedida, unidademedida.nome, " +
						"material.producao, material.peso, material.pesobruto, material.valorcusto, " + 
						"materialgrupo.cdmaterialgrupo, materialgrupo.nome, materialgrupo.rateiocustoproducao, material.obrigarlote, " +
						"material.valorvendaminimo, material.valorvendamaximo, codigoanp.cdcodigoanp, codigoanp.codigo, " +
						"material.qtdeunidade, materialproduto.largura, materialproduto.altura, materialproduto.comprimento," +
						"materialprodutosecundario.largura, materialprodutosecundario.altura, materialprodutosecundario.comprimento," +
						"material.valorcustomateriaprima," +
						"materialsecundario.produto, materialsecundario.patrimonio, materialsecundario.servico, materialsecundario.epi, " +
						"listaMaterialformulacusto.cdmaterialformulacusto, listaMaterialformulacusto.identificador, " +
						"listaMaterialformulacusto.ordem, listaMaterialformulacusto.formula," +
						"producaoetapanome.cdproducaoetapanome, producaoetapanome.nome, pneumedida.cdpneumedida, pneumedida.nome, pneumedida.otr ")
				.leftOuterJoin("material.materialproduto materialproduto")
				.leftOuterJoin("material.pneumedida pneumedida")
				.leftOuterJoin("material.listaProducao materialproducao")
				.leftOuterJoin("materialproducao.material materialsecundario")
				.leftOuterJoin("materialsecundario.materialproduto materialprodutosecundario")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("material.codigoanp codigoanp")
				.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
				.leftOuterJoin("materialsecundario.unidademedida unidademedidasecundario")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("materialsecundario.listaMaterialsimilar listaMaterialsimilar")
				.leftOuterJoin("materialproducao.listaMaterialproducaoformula materialproducaoformula")
				.leftOuterJoin("material.listaMaterialformulacusto listaMaterialformulacusto")
				.leftOuterJoin("materialsecundario.unidademedida unidademedidaMS")
				.leftOuterJoin("materialproducao.producaoetapanome producaoetapanome")
				.whereIn("material.cdmaterial", whereIn)
				.where("material.producao = true")
				.list();
	}
	
	public Material loadForTrocaproducao(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo");
		
		return query()
				.select("material.cdmaterial, material.nome, material.producao, material.frete, " +
						"unidademedida.cdunidademedida, unidademedida.nome, material.valorcusto, " +
						"listaMaterialsimilar.cdmaterialsimilar, material.valorvenda, material.servico,  " +
						"unidademedida.simbolo, material.identificacao, material.produto, material.servico, " +
						"material.extipi, material.ncmcompleto, material.codigobarras, ncmcapitulo.cdncmcapitulo, ncmcapitulo.descricao, " +
						"unidademedida.simbolo, material.patrimonio, material.epi, material.peso, material.pesobruto, " + 
						"materialgrupo.cdmaterialgrupo, materialgrupo.nome, material.valorvendaminimo, material.valorvendamaximo, " +
						"codigoanp.cdcodigoanp, codigoanp.codigo, material.qtdeunidade ")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("material.codigoanp codigoanp")
				.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("material.listaMaterialsimilar listaMaterialsimilar")
				.where("material = ?", material)
				.unique();
	}
	
	/**
	 * Busca a lista de material para a tabela de pre�o.
	 *
	 * @param whereIn
	 * @return
	 * @since 20/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Material> findForTabelaPreco(String whereIn){
		if(whereIn == null || whereIn.equals("")) return new ArrayList<Material>();
		return query()
					.select("material.cdmaterial, material.nome, material.identificacao, material.valorcusto")
					.whereIn("material.cdmaterial", whereIn)
					.list();
	}
	
	public List<Material> findForRegistrarConsumno(String whereIn){
		if(whereIn == null || whereIn.equals("")) return new ArrayList<Material>();
		return query()
					.select("material.cdmaterial, material.nome, material.identificacao, material.valorcusto, contaGerencialConsumo.cdcontagerencial, contaGerencialConsumo.nome, " +
							"projetoRateio.cdprojeto,projetoRateio.nome")
				.leftOuterJoin("material.materialRateioEstoque materialRateioEstoque")
				.leftOuterJoin("materialRateioEstoque.contaGerencialConsumo contaGerencialConsumo")	
				.leftOuterJoin("materialRateioEstoque.projeto projetoRateio")
				.whereIn("material.cdmaterial", whereIn)
				.list();
	}

	/**
	 * M�todo que busca o material para gerar comissionamento de representante
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Material findForComissaoRepresentante(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo.");
		
		return query()
				.select("material.cdmaterial, listaFornecedor.cdmaterialfornecedor, " +
						"fornecedor.cdpessoa, materialgrupo.cdmaterialgrupo ")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("material.listaFornecedor listaFornecedor")
				.leftOuterJoin("listaFornecedor.fornecedor fornecedor")
				.where("material  = ?", material)
				.unique();
	}
	
	public Material findForValidarTiketMedio(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo.");
		
		return query()
				.select("material.cdmaterial, material.pesobruto, material.nome, material.identificacao, listaFornecedor.cdmaterialfornecedor, " +
						"fornecedor.cdpessoa, fornecedor.valorTicketMedio, fornecedor.nome, fornecedor.razaosocial")
				.leftOuterJoin("material.listaFornecedor listaFornecedor")
				.leftOuterJoin("listaFornecedor.fornecedor fornecedor")
				.where("material  = ?", material)
				.unique();
	}

	/**
	 * M�todo que busca o material com a lista de materiais similares
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @param pneumodelo 
	 * @param pneumedida 
	 */
	public Material findForTrocaMaterialsimilar(Material material, Unidademedida unidademedida, Pneumedida pneumedida, Pneumodelo pneumodelo) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo.");
		
		QueryBuilder<Material> query = query()
			.select("material.cdmaterial, material.nome, material.profundidadesulco, listaMaterialsimilar.cdmaterialsimilar, materialsimilaritem.identificacao, " +
					"materialsimilaritem.cdmaterial, materialsimilaritem.valorvenda, materialproduto.altura, materialproduto.largura, materialproduto.fatorconversao, " +
					"materialsimilaritem.nome, materialsimilaritem.profundidadesulco, unidademedida.cdunidademedida, unidademedida.nome, material.valorvenda," +
					"listaMaterialunidademedida.fracao, listaMaterialunidademedida.qtdereferencia, listaMaterialunidademedida.prioridadeproducao, " +
					"unidademedidaMUM.cdunidademedida, unidademedidaMUM.nome ")
			.leftOuterJoin("material.listaMaterialsimilar listaMaterialsimilar")
			.leftOuterJoin("listaMaterialsimilar.materialsimilaritem materialsimilaritem")
			.leftOuterJoin("materialsimilaritem.unidademedida unidademedida")
			.leftOuterJoin("materialsimilaritem.listaMaterialunidademedida listaMaterialunidademedida")
			.leftOuterJoin("listaMaterialunidademedida.unidademedida unidademedidaMUM")
			.leftOuterJoin("unidademedida.listaUnidademedidaconversao listaUnidademedidaconversao")
			.leftOuterJoin("materialsimilaritem.materialproduto materialproduto")
			.leftOuterJoin("materialsimilaritem.listaMaterialpneumodelo listaMaterialpneumodelo")
			.where("material  = ?", material)
			.openParentheses()
				.where("materialsimilaritem.unidademedida = ?", unidademedida)
				.or()
				.where("unidademedidaMUM = ?", unidademedida)
				.or()
				.where("listaUnidademedidaconversao.unidademedidarelacionada = ?", unidademedida)
			.closeParentheses();
		
		if(pneumedida != null || pneumodelo != null){
			query
				.openParentheses()
					.openParentheses()
						.openParentheses()
							.where("listaMaterialpneumodelo.pneumedida  = ?", pneumedida)
							.or()
							.where("listaMaterialpneumodelo.pneumedida  is null")
						.closeParentheses()
						.where("listaMaterialpneumodelo.pneumodelo  = ?", pneumodelo)
					.closeParentheses()
					.or()
					.where("listaMaterialpneumodelo is null")
				.closeParentheses();
		}
		
		return query.unique();
	}
	
	/**
	 * M�todo que busca os a lista de materiais para sugest�o de compra
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<Material> findForSugestaocompra(SugestaocompraFiltro filtro){
		StringBuilder sql = new StringBuilder();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		
		String campo_qtdeminima = filtro.getLocalarmazenagem() != null ? 
				" coalesce((select ml.quantidademinima " +
				"		  from Materiallocalarmazenagem ml " +
				"		  where ml.cdmaterial = m.cdmaterial " +
				"		  and ml.cdlocalarmazenagem = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem() + 
				"), p.qtdeminima) " : " p.qtdeminima ";
		
		sql.append("SELECT query.CDMATERIAL, query.NOME, query.IDENTIFICACAO, query.PESO, query.PESOBRUTO, query.UNIDADEMEDIDASIMBOLO, query.CASASDECIMAISESTOQUE, query.QTDEMINIMA, query.VENDAS ");
//		   .append("query.ESTOQUEATUAL/COALESCE(query.VENDAS, 1) AS CICLOS, ")
//		   .append("COALESCE((query.VENDAS-query.ESTOQUEATUAL)*" + (filtro.getMargemcompra() != null ? filtro.getMargemcompra() : "0") + ", 0) AS SUGESTAOCOMPRA ");
//		   .append("COALESCE((query.VENDAS + (query.VENDAS* " + (filtro.getMargemcompra() != null && filtro.getMargemcompra() > 0 ? (filtro.getMargemcompra()/100) : "0") + "))-query.ESTOQUEATUAL, 0)  AS SUGESTAOCOMPRA ");
		   
		sql  
		   .append("FROM (select m.cdmaterial AS CDMATERIAL, m.nome AS NOME, m.identificacao AS IDENTIFICACAO, m.peso AS PESO, m.pesobruto AS PESOBRUTO, um.simbolo AS UNIDADEMEDIDASIMBOLO, um.casasdecimaisestoque as CASASDECIMAISESTOQUE, " + campo_qtdeminima + " AS QTDEMINIMA, ")
		   .append("(select sum(qtde_unidadeprincipal(vm.cdmaterial, vm.cdunidademedida, COALESCE(vm.quantidade, 0))) from vendamaterial vm ")
		   .append("left outer join venda v on vm.cdvenda = v.cdvenda ")
		   .append("join material mvm on mvm.cdmaterial = vm.cdmaterial ")
		   .append("where mvm.ativo = true and v.dtvenda >= to_date('").append(format.format(filtro.getDtinicio())).append("', 'dd/MM/yyyy')")
		   .append(filtro.getDtfim() != null ? " and v.dtvenda <= to_date('" + format.format(filtro.getDtfim()) + "', 'dd/MM/yyyy') " : " ")
		   .append(" and vm.cdmaterial = m.cdmaterial ")
		   .append(" and v.cdempresa = " + filtro.getEmpresa().getCdpessoa())
		   .append(" and v.cdvendasituacao <> " + Vendasituacao.CANCELADA.getCdvendasituacao() + " ");
		
		if(filtro.getLocalarmazenagem() != null && filtro.getLocalarmazenagem().getCdlocalarmazenagem() != null)
			sql.append(" and v.cdlocalarmazenagem = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem());
		if(filtro.getMaterialgrupo() != null && filtro.getMaterialgrupo().getCdmaterialgrupo() != null)
			sql.append(" and mvm.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo());
		if(filtro.getFornecedor() != null && filtro.getFornecedor().getCdpessoa() != null)
			sql.append(" and exists (select mf.cdmaterial from materialfornecedor mf where mf.cdmaterial = vm.cdmaterial and mf.cdpessoa = " + filtro.getFornecedor().getCdpessoa() + ") ");
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioService.isRestricaoFornecedorColaborador(usuarioLogado)){
			sql.append(" AND ( "+ 
							"( "+ // Retorna somente os materiais relacionados a um fonecedor que est� relacionado ao usuario logado
								"EXISTS("+
									"SELECT 1 FROM COLABORADORFORNECEDOR CF "+
									"WHERE "+
									"CF.CDFORNECEDOR IN (SELECT MF.CDPESSOA FROM MATERIALFORNECEDOR MF WHERE MF.CDMATERIAL = VM.CDMATERIAL) AND "+
									"CF.CDCOLABORADOR = "+usuarioLogado.getCdpessoa()+
								") "+
							") "+
							"OR "+
							"("+ // Ou retorna somente os materiais relacionados a um fonecedor que n�o est� relacionado a nenhum usuario
								"(" +
									"SELECT COUNT(*) "+
									"FROM ( "+
										"SELECT DISTINCT MF.CDPESSOA FROM MATERIALFORNECEDOR MF WHERE MF.CDMATERIAL = VM.CDMATERIAL" +
									") AS FORN " +
								")"+
								" != "+
								"(" +
									"SELECT COUNT(*) "+
									"FROM ( " +
											"SELECT DISTINCT CF.CDFORNECEDOR " +
											"FROM COLABORADORFORNECEDOR CF " +
											"WHERE CF.CDFORNECEDOR IN ( "+
												" SELECT MF.CDPESSOA  "+
												" FROM MATERIALFORNECEDOR MF "+
												" WHERE MF.CDMATERIAL = VM.CDMATERIAL "+
											" )" +
									") AS FORN" +
								")"+
							") "+
							"OR "+
							"("+ // Ou retorna somente os materiais que n�o est�o relacionados a nenhum fonecedor
								"NOT EXISTS (" +
									"SELECT 1 FROM MATERIALFORNECEDOR MF WHERE MF.CDMATERIAL = VM.CDMATERIAL" +
								")"+
							")"+
							
						")");
		}
		
		sql.append(") AS VENDAS ");
		   
//		sql
//		   .append("(select sum(COALESCE(vgm.qtdedisponivel, 0)) ")
//		   .append("from vgerenciarmaterial vgm ")
//		   .append("join material mvgm on mvgm.cdmaterial = vgm.cdmaterial ")
//		   .append("where mvgm.ativo = true and  vgm.cdmaterial = m.cdmaterial");
//		   
//		if(filtro.getLocalarmazenagem() != null && filtro.getLocalarmazenagem().getCdlocalarmazenagem() != null)
//			sql.append(" and vgm.cdlocalarmazenagem = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem());
//		if(filtro.getMaterialgrupo() != null && filtro.getMaterialgrupo().getCdmaterialgrupo() != null)
//			sql.append(" and mvgm.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo());
//		if(filtro.getFornecedor() != null && filtro.getFornecedor().getCdpessoa() != null)
//			sql.append(" and exists (select mf.cdmaterial from materialfornecedor mf where mf.cdmaterial = vgm.cdmaterial and mf.cdpessoa = " + filtro.getFornecedor().getCdpessoa() + ") ");
//		
//		sql.append(") AS ESTOQUEATUAL ");
		   
		sql
		   .append("from material m ")
		   .append("left outer join unidademedida um on um.cdunidademedida = m.cdunidademedida ")
		   .append("join produto p on p.cdmaterial = m.cdmaterial and m.ativo = true ");
		
		if(filtro.getMaterialcategoria() != null && filtro.getMaterialcategoria().getIdentificador() != null){
			
			sql.append(" join materialcategoria matc on matc.cdmaterialcategoria =  m.cdmaterialcategoria ")
			   .append(" join vmaterialcategoria vmatc on vmatc.cdmaterialcategoria =  matc.cdmaterialcategoria ")
			   .append(" and (vmatc.identificador like '" + filtro.getMaterialcategoria().getIdentificador() + ".%' ")
			   .append(" or vmatc.identificador like '" + filtro.getMaterialcategoria().getIdentificador() + "')");
		}
		   
		if(filtro.getMaterialgrupo() != null && filtro.getMaterialgrupo().getCdmaterialgrupo() != null)
			   sql.append(" and m.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo());
		if(filtro.getFornecedor() != null && filtro.getFornecedor().getCdpessoa() != null)
			sql.append(" and exists (select mf.cdmaterial from materialfornecedor mf where mf.cdmaterial = m.cdmaterial and mf.cdpessoa = " + filtro.getFornecedor().getCdpessoa() + ") ");
		
		if(usuarioLogado.getRestricaoFornecedorColaborador() != null && usuarioLogado.getRestricaoFornecedorColaborador()){
			sql.append(" AND ( "+
							"( "+ // Retorna somente os materiais relacionados a um fonecedor que est� relacionado ao usuario logado
								"EXISTS("+
									"SELECT 1 FROM COLABORADORFORNECEDOR CF "+
									"WHERE "+
									"CF.CDFORNECEDOR IN (SELECT MF.CDPESSOA FROM MATERIALFORNECEDOR MF WHERE MF.CDMATERIAL = M.CDMATERIAL) AND "+
									"CF.CDCOLABORADOR = "+usuarioLogado.getCdpessoa()+
								") "+
							") "+
							"OR "+
							"("+ // Ou retorna somente os materiais relacionados a um fonecedor que n�o est� relacionado a nenhum usuario
								"(" +
									"SELECT COUNT(*) "+
									"FROM ( "+
										"SELECT DISTINCT MF.CDPESSOA FROM MATERIALFORNECEDOR MF WHERE MF.CDMATERIAL = M.CDMATERIAL" +
									") AS FORN " +
								")"+
								" != "+
								"(" +
									"SELECT COUNT(*) "+
									"FROM ( " +
											"SELECT DISTINCT CF.CDFORNECEDOR " +
											"FROM COLABORADORFORNECEDOR CF " +
											"WHERE CF.CDFORNECEDOR IN ( "+
												" SELECT MF.CDPESSOA  "+
												" FROM MATERIALFORNECEDOR MF "+
												" WHERE MF.CDMATERIAL = M.CDMATERIAL "+
											" )" +
									") AS FORN" +
								")"+
							") "+
							"OR "+
							"("+ // Ou retorna somente os materiais que n�o est�o relacionados a nenhum fonecedor
								"NOT EXISTS (" +
									"SELECT 1 FROM MATERIALFORNECEDOR MF WHERE MF.CDMATERIAL = M.CDMATERIAL" +
								")"+
							")"+
						")");
		}
		
		if(SinedUtil.isRestricaoEmpresaFornecedorUsuarioLogado()){
			String whereInEmpresa = filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null ? filtro.getEmpresa().getCdpessoa().toString() : new SinedUtil().getListaEmpresa();
			sql.append(" AND exists (select materialRestricao.cdmaterial " +
									"from Material materialRestricao " +
									"left outer join materialfornecedor mfRestricao on mfRestricao.cdmaterial = materialRestricao.cdmaterial " +
									"where m.cdmaterial = materialRestricao.cdmaterial and (mfRestricao.cdpessoa is null or true = permissao_fornecedorempresa(mfRestricao.cdpessoa, '" + whereInEmpresa + "')))");
		}
		
		sql.append(") AS query ");
		
		SinedUtil.markAsReader();
		List<Material> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Material(rs.getInt("CDMATERIAL"), 
									rs.getString("NOME"),
									rs.getString("IDENTIFICACAO"),
									rs.getDouble("QTDEMINIMA"), 
									rs.getDouble("VENDAS"), 
									rs.getString("UNIDADEMEDIDASIMBOLO"),
									rs.getInt("CASASDECIMAISESTOQUE"),
									rs.getDouble("PESO"),
									rs.getDouble("PESOBRUTO")
									);
			}
		});
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	public List<Vgerenciarmaterial> findEstoqueAtual(String whereInMaterial, SugestaocompraFiltro filtro) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT vgm.CDMATERIAL, sum(vgm.qtdedisponivel) AS ESTOQUEATUAL ")
		   .append("from vgerenciarmaterial vgm ")
		   .append("join material mvgm on mvgm.cdmaterial = vgm.cdmaterial ")
		   .append("where mvgm.ativo = true and vgm.cdmaterial in (" + whereInMaterial + ") ");
		   
		if(filtro.getLocalarmazenagem() != null && filtro.getLocalarmazenagem().getCdlocalarmazenagem() != null)
			sql.append(" and vgm.cdlocalarmazenagem = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem());
		if(filtro.getMaterialgrupo() != null && filtro.getMaterialgrupo().getCdmaterialgrupo() != null)
			sql.append(" and mvgm.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo());
		if(filtro.getFornecedor() != null && filtro.getFornecedor().getCdpessoa() != null)
			sql.append(" and exists (select mf.cdmaterial from materialfornecedor mf where mf.cdmaterial = vgm.cdmaterial and mf.cdpessoa = " + filtro.getFornecedor().getCdpessoa() + ") ");
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null){
			sql.append(" and vgm.cdempresa  = " + filtro.getEmpresa().getCdpessoa());
		}

		sql.append(" group by vgm.cdmaterial ");

		SinedUtil.markAsReader();
		List<Vgerenciarmaterial> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Vgerenciarmaterial(rs.getInt("CDMATERIAL"), 
									rs.getDouble("ESTOQUEATUAL")
									);
			}
		});
		
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	public Double findEstoqueAtual(String cdmaterial, Localarmazenagem localarmazenagem, Empresa empresa) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT sum(vgm.qtdedisponivel) AS ESTOQUEATUAL ")
		   .append("from vgerenciarmaterial vgm ")
		   .append("join material mvgm on mvgm.cdmaterial = vgm.cdmaterial ")
		   .append("where mvgm.ativo = true and vgm.cdmaterial =  " + cdmaterial);
		   
		if(localarmazenagem != null && localarmazenagem.getCdlocalarmazenagem() != null)
			sql.append(" and vgm.cdlocalarmazenagem = " + localarmazenagem.getCdlocalarmazenagem());
		if(empresa != null && empresa.getCdpessoa() != null)
			sql.append(" and vgm.cdempresa = "+empresa.getCdpessoa());

		SinedUtil.markAsReader();
		List<Double> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Double(rs.getDouble("ESTOQUEATUAL"));
			}
		});
		
		return lista.get(0);
	}
	
	/**
	 * M�todo que retorna o estoque dos produtos no ERP.
	 * Caso a data de refer�ncia n�o seja especificada retorna o estoque geral dos produtos ativos e, 
	 * caso contr�rio, retorna somente o estoque dos produtos que sofreram ajuste ap�s a �ltima atualiza��o 
	 * @param filtro
	 * @return
	 * @author Rafael Salvio
	 */
	@SuppressWarnings("unchecked")
	public List<Vgerenciarmaterial> consultarEstoqueAtualForWms(ConsultarEstoqueBean filtro) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT vgm.CDMATERIAL, sum(vgm.qtdedisponivel) AS ESTOQUEATUAL ")
		   .append("from vgerenciarmaterial vgm ")
		   .append("join material mvgm on mvgm.cdmaterial = vgm.cdmaterial ")
		   .append("left outer join localarmazenagem local on local.cdlocalarmazenagem =  vgm.cdlocalarmazenagem ")
		   .append("left outer join pessoa evgm on evgm.cdpessoa = vgm.cdempresa ")
		   .append("join materialgrupo mp on mp.cdmaterialgrupo = mvgm.cdmaterialgrupo ")
		   .append("where 1=1 ")
		   .append("and coalesce(mp.sincronizarwms, false) = true ")
		   .append("and (vgm.cdlocalarmazenagem is null or local.ativo is true) ");
		
		try{
			if(filtro.getCnpjDeposito() != null){
				sql.append("and evgm.cnpj = '").append(filtro.getCnpjDeposito()).append("' ");
			}
		} catch (Exception e) {}
		
		//	Inicialmente seriam enviados somente os c�digos dos produtos ajustados, 
		//	contudo a regra foi alterada para buscar os saldos de todos os produtos do erp. 
		if(Boolean.TRUE.equals(filtro.getSaldoForAtualizacaoERP())){
//			if(filtro.getWhereInCdmaterial() == null || filtro.getWhereInCdmaterial().trim().isEmpty()){
//				return null;
//			} else{				
//				sql.append("and mvgm.cdmaterial in ("+filtro.getWhereInCdmaterial()+") ");
//			}
		} else{
			if(filtro.getDtReferenciaInMillis() == null || filtro.getDtReferenciaInMillis().equals(0L)){
				sql.append("and mvgm.ativo is true ");
			} else{
				String dtReferencia = SinedDateUtils.toStringPostgre(new java.util.Date(filtro.getDtReferenciaInMillis()));
				sql.append("and (mvgm.dtaltera >= '" + dtReferencia + "' ")
				.append("or exists (select 1 from movimentacaoestoque mestoque where mestoque.cdmaterial = mvgm.cdmaterial ")
				.append("and mestoque.dtmovimentacao >= '" + dtReferencia + "' and mestoque.dtcancelamento is null) ");
				if(filtro.getWhereInCdmaterial() != null && !filtro.getWhereInCdmaterial().isEmpty()){
					sql.append("or mvgm.cdmaterial in ("+filtro.getWhereInCdmaterial()+")");
				}
				sql.append(")");
			}
		}
		sql.append("group by vgm.cdmaterial ");
		SinedUtil.markAsReader();
		List<Vgerenciarmaterial> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Vgerenciarmaterial(rs.getInt("CDMATERIAL"), 
									rs.getDouble("ESTOQUEATUAL")
									);
			}
		});
		
		return lista;
	}

	/**
	 * Verifica se j� existe o material de acordo com o nome e identifica��o.
	 *
	 * @param identificacao
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 31/01/2013
	 */
	public boolean haveMaterialNomeIdentificacao(String identificacao, String nome) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Material.class)
					.where("material.identificacao = ?", identificacao)
					.where("lower(retira_acento(material.nome)) like ?", nome != null ? Util.strings.tiraAcento(nome.trim().toLowerCase()) : null)
					.unique() > 0;
	}

	/**
	 * M�todo que busca os materiais de acordo com a identificacao
	 *
	 * @param identificacao
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findMaterialByIdentificacaoNome(String identificacao, String nome) {
		if(nome == null || "".equals(nome))
			throw new SinedException("Nome do material n�o pode ser nulo.");
		
		return query()
				.select("material.cdmaterial, material.identificacao, material.nome, listaProducao.cdmaterialproducao, materialitem.cdmaterial, " +
						"materialitem.identificacao ")
				.leftOuterJoin("material.listaProducao listaProducao")
				.leftOuterJoin("listaProducao.material materialitem")
				.whereLikeIgnoreAll("material.identificacao", identificacao)
				.whereLikeIgnoreAll("material.nome", nome)
				.list();
	}

	public List<Material> findByFornecedorCodigo(Fornecedor fornecedor, String cprod) {
		return query()
					.select("material.cdmaterial, material.nome, material.identificacao")
					.join("material.listaFornecedor listaFornecedor")
					.join("listaFornecedor.fornecedor fornecedor")
					.where("listaFornecedor.codigo = ?", cprod)
					.where("fornecedor = ?", fornecedor)
					.where("material.ativo = ?", Boolean.TRUE)
					.list();
	}

	public Material findByNomeIdentificacao(String identificacao, String nome) {
		return query()
				.where("material.identificacao = ?", identificacao)
				.where("upper(material.nome) = ?", nome.toUpperCase())
				.unique();
	}
	
	public Material findByIdentificacao(String identificacao, Boolean ativo, Integer cdMaterialExcecao) {
		return query()
				.select("material.cdmaterial, material.nome, material.identificacao, material.valorvenda, material.valorvendamaximo, " +
						"material.valorvendaminimo, material.valorcusto, unidademedida.cdunidademedida")
				.join("material.unidademedida unidademedida")
				.where("material.identificacao = ?", identificacao)
				.where("coalesce(material.ativo, false) = ?", ativo)
				.where("material.cdmaterial <> ?", cdMaterialExcecao)
				.unique();
	}
	
	public Material findByCdmaterial(Integer cdmaterial) {
		return query()
				.select("material.cdmaterial, material.nome, material.identificacao, material.valorvenda, material.valorvendamaximo, material.qtdeunidade," +
						"material.valorvendaminimo, material.valorcusto, unidademedida.cdunidademedida")
				.join("material.unidademedida unidademedida")
				.where("material.cdmaterial = ?", cdmaterial)
				.unique();
	}
	
	public List<Material> findCdMaterialAndIdentificacao(String cdMaterial){
		if(cdMaterial == null || cdMaterial.equals("")) return new ArrayList<Material>();
		Material material = new Material(Integer.parseInt(cdMaterial));
		return query()
					.select("material.cdmaterial, material.nome, material.identificacao, material.valorcusto")
					.where("material = ?", material)
					.or().where("material.identificacao = ?", cdMaterial)
					.list();
	}
	
	/**
	 * M�todo que busca materiais de acordo com a refer�ncia
	 *
	 * @param identificacao
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findMaterialByIdentificacao(String identificacao) {
		return query()
				.select("material.cdmaterial, material.nome, material.valorvenda, material.valorvendamaximo, " +
						"material.valorvendaminimo, material.valorcusto, material.produto, material.producao, material.epi,material.servico, " +
						"unidademedida.cdunidademedida, unidademedida.nome, contagerencial.cdcontagerencial, contagerencial.nome")
				.join("material.unidademedida unidademedida")
				.leftOuterJoin("material.contagerencial contagerencial")
				.where("material.identificacao = ?", identificacao)
				.list();
	}

	/**
	 * M�todo que busca o servico relacionado a um fornecedor
	 *
	 * @param fornecedor
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Material> findServicoByFornecedor(Fornecedor fornecedor, Empresa empresa) {
		if(fornecedor == null || fornecedor.getCdpessoa() == null)
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		
		return query()
				.select("material.cdmaterial, material.nome, material.nomenf, material.identificacao, material.referencia, "
						+ "material.produto, material.patrimonio, material.epi, material.servico, unidademedida.cdunidademedida, "
						+ "listaFornecedor.cdmaterialfornecedor, fornecedor.cdpessoa, fornecedor.nome ")
				.join("material.unidademedida unidademedida")
				.join("material.listaFornecedor listaFornecedor")
				.join("listaFornecedor.fornecedor fornecedor")
				.leftOuterJoin("material.listaMaterialempresa listaMaterialempresa")
				.leftOuterJoin("listaMaterialempresa.empresa empresa")
				.where("fornecedor = ?", fornecedor)
				.where("material.servico = true")
				.where("material.ativo = true")
				.openParentheses()
					.where("empresa is null")
					.or()
					.where("empresa = ?", empresa)
				.closeParentheses()
				.list();
	}

	/**
	 * Carrega o material para o registro no hist�rico.
	 *
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/04/2013
	 */
	public Material loadForHistoricoAlteracao(Material material) {
		return query()
					.select("material.cdmaterial, material.nome, material.ncmcompleto, material.vendapromocional, " +
							"material.valorvenda, material.codigobarras, material.ativo, material.vendaecf, material.valorvendaminimo, " +
							"unidademedida.cdunidademedida, materialgrupo.cdmaterialgrupo")
					.join("material.unidademedida unidademedida")
					.join("material.materialgrupo materialgrupo")
					.entity(material)
					.unique();
	}

	/**
	 * M�todo que faz update dos campos da conferencia do material ao receber a entrega
	 *
	 * @param material
	 * @param atualizaBean
	 * @author Luiz Fernando
	 */
	public void updateInfByConferenciaEntradafical(Material material, ImportacaoXmlNfeAtualizacaoMaterialBean atualizaBean) {
		if(material != null && material.getCdmaterial() != null && atualizaBean != null){
			StringBuilder sql_set = new StringBuilder();
			StringBuilder sql_ncm = new StringBuilder();
			
			if((atualizaBean.getCodigobarras_bd() == null || "".equals(atualizaBean.getCodigobarras_bd())) && 
					atualizaBean.getCodigobarras_xml() != null && !"".equals(atualizaBean.getCodigobarras_xml())){
				sql_set.append(" codigobarras = '").append(atualizaBean.getCodigobarras_xml()).append("' ");
			}
			if(atualizaBean.getNcmcapitulo_bd() == null && atualizaBean.getNcmcapitulo_xml() != null && 
					atualizaBean.getNcmcapitulo_xml().getCdncmcapitulo() != null){
				if(!"".equals(sql_ncm.toString())) sql_ncm.append(" , ");
				sql_ncm.append(" cdncmcapitulo = ").append(atualizaBean.getNcmcapitulo_xml().getCdncmcapitulo()).append(" ");
			}
			if((atualizaBean.getExtipi_bd() == null  || "".equals(atualizaBean.getExtipi_bd())) && 
					atualizaBean.getExtipi_xml() != null && !"".equals(atualizaBean.getExtipi_xml())){
				if(!"".equals(sql_set.toString())) sql_set.append(" , ");
				sql_set.append(" extipi = '").append(atualizaBean.getExtipi_xml()).append("' ");
			}
			if((atualizaBean.getNcmcompleto_bd() == null || "".equals(atualizaBean.getNcmcompleto_bd())) && 
					atualizaBean.getNcmcompleto_xml() != null && !"".equals(atualizaBean.getNcmcompleto_xml())){
				if(!"".equals(sql_ncm.toString())) sql_ncm.append(" , ");
				sql_ncm.append(" ncmcompleto = '").append(atualizaBean.getNcmcompleto_xml()).append("' ");
			}
			
			if(!"".equals(sql_set.toString())){
				getJdbcTemplate().execute("UPDATE MATERIAL SET " + sql_set.toString() + " where cdmaterial = " + material.getCdmaterial());
			}
			if(!"".equals(sql_ncm.toString())){
				getJdbcTemplate().execute("UPDATE MATERIAL SET " + sql_ncm.toString() + " where (ncmcompleto is null or ncmcompleto = '') and cdmaterial = " + material.getCdmaterial());
			}
		}
	}	

	/**
	 * M�todo que busca os materiais de produ��o para ordenar os itens na venda
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 28/10/2013
	 */
	public List<Material> findForOrdenarMaterialproducao(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("material.cdmaterial, material.nome, matproducao.cdmaterial, matproducao.nome, producao.ordemexibicao," +
					"material.tempoentrega,listaFornecedor.cdmaterialfornecedor,listaFornecedor.tempoentrega," +
					"fornecedor.cdpessoa, fornecedor.nome")
			.leftOuterJoin("material.listaProducao producao")
			.leftOuterJoin("producao.material matproducao")
			.leftOuterJoin("material.listaFornecedor listaFornecedor")
			.leftOuterJoin("listaFornecedor.fornecedor fornecedor")
			.whereIn("material.cdmaterial", whereIn)
			.list();
	}
	/**
	 * M�todo que busca os materiais que n�o tem grade (materialmestregrade)
	 *
	 * @param s
	 * @return
	 * @author Luiz Fernando
	 * @since 27/12/2013
	 */
	public List<Material> findForMaterialmestregrade(String s, Integer cdmaterialFilho) {
		QueryBuilder<Material> query = querySined()
							.select("material.cdmaterial, material.nome, material.identificacao ")
							.leftOuterJoin("material.materialmestregrade materialmestregrade")
							.openParentheses()
							.whereLikeIgnoreAll("material.nome", s).or()
							.whereLikeIgnoreAll("material.identificacao", s)
							.closeParentheses()
							.where("materialmestregrade is null");
		if(cdmaterialFilho != null && SinedUtil.isIntegracaoTrayCorp()){//N�o traz item que j�
			query.where("not exists(select 1 from Material m join m.materialmestregrade mmg where mmg.cdmaterial = ?)", cdmaterialFilho);
		}
		return query.list();
	}
	
	public Material loadWithMatrialgrupo(Material material){
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo.");
		
		return querySined()
				.removeUseWhere()
				.select("material.cdmaterial, material.nome, materialgrupo.cdmaterialgrupo, materialgrupo.nome, " +
						"materialgrupo.gradeestoquetipo, materialgrupo.valormvaespecifico")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.where("material = ?", material)
				.unique();
	}
	
	public Material loadForTributacao(Material material){
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo.");
		
		return querySined()
				.removeUseWhere()
				.select("material.cdmaterial, material.nome, materialgrupo.cdmaterialgrupo, materialgrupo.nome, material.ncmcompleto, material.origemproduto, " +
						"material.tributacaoestadual, materialgrupo.valormvaespecifico, material.percentualimpostoecf, unidademedida.cdunidademedida, material.servico," +
						"materialtipo.cdmaterialtipo, materialtipo.nome ")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("material.materialtipo materialtipo")
				.where("material = ?", material)
				.unique();
	}
	
	public Material loadWithGrade(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo.");
		
		return querySined()
				.removeUseWhere()
				.select("material.cdmaterial, material.nome, material.identificacao, material.valorcusto, material.valorvenda,  material.ecommerce," +
				"materialmestregrade.cdmaterial, materialmestregrade.nome, materialmestregrade.identificacao, " +
				"materialmestregrade.valorcusto, materialmestregrade.valorvenda, " +
				"materialgrupo.cdmaterialgrupo, materialgrupo.nome, materialgrupo.gradeestoquetipo, materialgrupograde.registrarpesomedio, " +
				"materialgrupograde.cdmaterialgrupo, materialgrupograde.nome, materialgrupograde.gradeestoquetipo, materialgrupograde.registrarpesomedio, " +
				"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " + 
				"unidademedidagrade.cdunidademedida, unidademedidagrade.nome, unidademedidagrade.simbolo, unidademedida.casasdecimaisestoque, " +
				"material.produto, material.servico, material.epi, material.patrimonio," +
				"materialmestregrade.produto, materialmestregrade.servico, materialmestregrade.epi, materialmestregrade.patrimonio")
		.leftOuterJoin("material.materialmestregrade materialmestregrade")
		.leftOuterJoin("material.unidademedida unidademedida")
		.leftOuterJoin("material.materialgrupo materialgrupo")
		.leftOuterJoin("materialmestregrade.unidademedida unidademedidagrade")
		.leftOuterJoin("material.materialgrupo materialgrupograde")
		.where("material = ?", material)
		.unique();
	}

	public List<Material> findForCsvGerenciamentoMaterial(String whereInMat) {
		QueryBuilder<Material> query = query();
		
		query
			.select("material.cdmaterial, material.nome, material.referencia, material.identificacao, material.peso, material.valorcusto, " +
					"material.pesobruto, materialgrupo.nome, localizacaoestoque.descricao, unidademedida.nome, unidademedida.casasdecimaisestoque, " +
					"listaMaterialunidademedida.mostrarconversao, listaMaterialunidademedida.fracao, listaMaterialunidademedida.qtdereferencia, " +
					"unidademedidaSecundaria.nome, unidademedidaSecundaria.casasdecimaisestoque")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.localizacaoestoque localizacaoestoque")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("material.listaMaterialunidademedida listaMaterialunidademedida")
			.leftOuterJoin("listaMaterialunidademedida.unidademedida unidademedidaSecundaria");
		
		SinedUtil.quebraWhereIn("material.cdmaterial", whereInMat, query);
		
		return query.list();
	}

	/**
	 * M�todo que retorna true caso o material tenha etapa de produ��o
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 08/01/2014
	 */
	public Boolean existProducaoetapa(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo.");
		
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Material.class)	
			.where("material = ?", material)
			.where("material.producaoetapa is not null")
			.unique() > 0;
	}
	
	public Boolean isControleGrade(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo.");
		
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Material.class)	
			.join("material.materialgrupo materialgrupo")
			.where("material = ?", material)
			.where("materialgrupo.gradeestoquetipo is not null")
			.unique() > 0;
	}

	/**
	 * M�todo que retorna true caso o material tenha mestre de grade
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 13/01/2014
	 */
	public Boolean existMaterialitemByMaterialgrademestre(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo.");
		
		return newQueryBuilder(Long.class)
		.select("count(*)")
		.setUseTranslator(false)
		.from(Material.class)
		.join("material.materialmestregrade materialmestregrade")
		.where("materialmestregrade = ?", material)
		.unique() > 0;
	}
	
	/**
	 * M�todo que retorna true caso o material tenha controle de estoque por mestre
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 30/01/2014
	 */
	public boolean isControleMaterialgrademestre(Material material) {
		return newQueryBuilder(Long.class)
		.select("count(*)")
		.setUseTranslator(false)
		.from(Material.class)
		.join("material.materialgrupo materialgrupo")
		.where("material = ?", material)
		.where("materialgrupo.gradeestoquetipo = ?", Gradeestoquetipo.MESTRE)
		.unique() > 0;
	}
	
	public boolean isControleMaterialitemgrade(Material material) {
		return newQueryBuilder(Long.class)
		.select("count(*)")
		.setUseTranslator(false)
		.from(Material.class)
		.join("material.materialgrupo materialgrupo")
		.where("material = ?", material)
		.where("materialgrupo.gradeestoquetipo = ?", Gradeestoquetipo.ITEMGRADE)
		.unique() > 0;
	}
	
	public Boolean isMaterialMestre(Material material) {
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.setUseTranslator(false)
				.from(Material.class)
				.join("material.materialgrupo materialgrupo")
				.where("material = ?", material)
				.where("materialgrupo.gradeestoquetipo is not null")
				.where("material.materialmestregrade is null")
				.unique() > 0;
	}
	
	public Boolean isMestreGrade(Material material) {
		if(Util.objects.isNotPersistent(material)){
			return false;
		}
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.setUseTranslator(false)
				.from(Material.class)
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.join("material.materialgrupo materialgrupo")
				.where("material = ?", material)
				.where("materialgrupo.gradeestoquetipo = ?", Gradeestoquetipo.MESTRE)
				.where("exists (select materialMestre.cdmaterial from Material m " +
				"left outer join m.materialmestregrade materialMestre " +
				"where materialMestre.cdmaterial = material.cdmaterial)")
				.unique() > 0;
	}
	
	
	/**]
	* M�todo que verifica se o material passado por par�metro est� sendo controlado por item de grade e n�o
	* existe filhos vinculados a ele. Se retornar true, o cadastro do material est� errado ( material n�o � de grade).
	*
	* @param material
	* @return
	* @since 09/09/2015
	* @author Luiz Fernando
	*/
	public boolean isControleItemgradeSemMaterialgrademestre(Material material) {
		return newQueryBuilder(Long.class)
		.select("count(*)")
		.setUseTranslator(false)
		.from(Material.class)
		.leftOuterJoin("material.materialmestregrade materialmestregrade")
		.join("material.materialgrupo materialgrupo")
		.where("material = ?", material)
		.where("materialgrupo.gradeestoquetipo = ?", Gradeestoquetipo.ITEMGRADE)
		.where("materialmestregrade is null")
		.where("not exists (select materialMestre.cdmaterial from Material m " +
				"left outer join m.materialmestregrade materialMestre " +
				"where materialMestre.cdmaterial = material.cdmaterial)")
		.unique() > 0;
	}
	
	public Boolean isControleItemGradeEPesquisasomentemestre(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo.");
		
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Material.class)	
			.join("material.materialgrupo materialgrupo")
			.where("material = ?", material)
			.where("materialgrupo.gradeestoquetipo = ?", Gradeestoquetipo.ITEMGRADE)
			.where("materialgrupo.pesquisasomentemestre = true")
			.unique() > 0;
	}

	/**
	 * M�todo que busca os itens do material mestre da grade
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @param ativo 
	 * @since 16/01/2014
	 */
	public List<Material> findMaterialitemByMaterialmestregrade(Material material, Boolean ativo, String whereInMaterialItenGrade) {
		return query()
			.select("material.cdmaterial, material.produto, material.patrimonio, material.servico, material.epi, material.obrigarlote, " +
					"material.nome, material.identificacao, material.qtdereferencia, material.producao, material.valorcusto, " +
					"material.valorvendaminimo, material.valorvendamaximo,material.valorvenda, material.producao," +
					"material.qtdeunidade, material.codigofabricante, " +
					"material.pesoliquidovalorvenda, material.peso, material.pesobruto, material.metrocubicovalorvenda, material.pesoliquidovalorvenda, " +
					"materialmestregrade.cdmaterial, materialmestregrade.produto, materialmestregrade.patrimonio, " +
					"materialmestregrade.servico, materialmestregrade.epi, materialmestregrade.nome, materialmestregrade.identificacao, " +
					"materialmestregrade.qtdereferencia, materialmestregrade.producao, materialmestregrade.valorcusto, " +
					"materialmestregrade.valorvendaminimo, materialmestregrade.valorvendamaximo," +
					"materialmestregrade.valorvenda, materialmestregrade.producao," +
					"materialmestregrade.pesoliquidovalorvenda, materialmestregrade.peso, material.metrocubicovalorvenda, " +
					"materialgrupo.cdmaterialgrupo, materialgrupo.nome, materialgrupo.gradeestoquetipo, " +
					"materialgrupomestregrade.cdmaterialgrupo, materialgrupomestregrade.nome," +
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.casasdecimaisestoque, unidademedida.simbolo, " +
					"unidademedidamestregrade.cdunidademedida, unidademedidamestregrade.nome, unidademedidamestregrade.simbolo, " +
					"materialcor.cdmaterialcor, produto.largura, produto.altura, produto.comprimento, produto.fatorconversao ") 
			.join("material.materialmestregrade materialmestregrade")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("materialmestregrade.unidademedida unidademedidamestregrade")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("materialmestregrade.materialgrupo materialgrupomestregrade")
			.leftOuterJoin("material.materialproduto produto")
			.leftOuterJoin("material.materialcor materialcor")
			.where("material.ativo = ?", ativo)
			.where("materialmestregrade = ?", material)
			.whereIn("material.cdmaterial", whereInMaterialItenGrade != null && !"".equals(whereInMaterialItenGrade) ? 
					whereInMaterialItenGrade.replaceAll(SinedUtil.REGEX_WHEREIN, "") : null)
			.orderBy("material.nome")
			.list();
	}
	
	/**
	 * M�todo que carrega os materiais de acordo com o tipo de material
	 *
	 * @param materialtipo
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 10/02/2014
	 */
	public List<Material> findForCopiarFormulapeso(Materialtipo materialtipo, Material material) {
		if(materialtipo == null)
			throw new SinedException("Tipo de material n�o pode ser nulo.");
		
		return query()
				.select("material.cdmaterial, materialtipo.cdmaterialtipo, materialtipo.nome, " +
						"listaMaterialformulapeso.cdmaterialformulapeso, listaMaterialformulapeso.ordem, " +
						"listaMaterialformulapeso.identificador, listaMaterialformulapeso.formula")
				.join("material.materialtipo materialtipo")
				.leftOuterJoin("material.listaMaterialformulapeso listaMaterialformulapeso")
				.where("materialtipo = ?", materialtipo)
				.where("material <> ?", material)
				.list();
	}

	/**
	 * Busca os dados para a atuliza��o de estoque.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/02/2014
	 */
	@SuppressWarnings("unchecked")
	public List<AtualizacaoEstoqueMaterialBean> findForAtualizacaoEstoque(final AtualizacaoEstoqueFiltro filtro, String whereInMaterial, String whereInCodigobarras) {
		final Boolean considerarLotes = filtro.getConsiderarLotes();
		
		String sql = "select cdmaterial, " +
			"nome_material,  " +
		    "codigobarras, " +
		    "produto_boolean, " +
		    "epi_boolean, " +
		    "valorcusto, " +
		    "cdunidademedida, " +
		    "nome_unidademedida, " +
		    "cdmaterialrateioestoque, " +
		    "cdcontagerencialajusteentrada, " +
		    "cdcontagerencialajustesaida, " +
		    "sum(coalesce(qtde_entrada, 0)) - sum(coalesce(qtde_saida, 0)) as qtde_sistema, " +
		    "registrarpesomedio, " +
		    (considerarLotes ? "lote_numero, lote_id, fornecedor_cnpj, " : "") +
		    "pesobruto " +
		    
		"from ( " +
	
		"select  " +
			"m.cdmaterial,  " +
			"m.nome as nome_material, " +
		    "m.codigobarras, " +
		    "m.produto as produto_boolean, " +
		    "m.epi as epi_boolean, " +
		    "m.valorcusto, " +
		    "um.cdunidademedida, " +
		    "um.nome as nome_unidademedida, " +
		    "mr.cdmaterialrateioestoque, " +
		    "mr.cdcontagerencialajusteentrada, " +
		    "mr.cdcontagerencialajustesaida, " +
		    "sum(me.qtde) as qtde_saida, " +
		    "0 as qtde_entrada, " +
		    "mg.registrarpesomedio, " +
		    (considerarLotes ? "l.numero as lote_numero, l.cdloteestoque as lote_id, pf.cnpj as fornecedor_cnpj, " : "") +
		    "m.pesobruto " +
	
		"from material m " +
		"join unidademedida um on um.cdunidademedida = m.cdunidademedida " +
		"left outer join movimentacaoestoque me on me.cdmaterial = m.cdmaterial  " +
		"left outer join materialgrupo mg on mg.cdmaterialgrupo = m.cdmaterialgrupo  " +
		"left outer join materialrateioestoque mr on mr.cdmaterialrateioestoque = m.cdmaterialrateioestoque " +
		(considerarLotes ? "left outer join loteestoque l on l.cdloteestoque = me.cdloteestoque " : "") +
		(considerarLotes ? "left outer join pessoa pf on pf.cdpessoa = l.cdfornecedor " : "") +
	
		"where m.ativo = true " +
		"and (m.patrimonio = false or m.patrimonio is null) " +
		"and (m.servico = false or m.servico is null) " +
		"and (m.vendapromocional = false or m.vendapromocional is null) " +
		"and me.cdmovimentacaoestoquetipo = 2  " +
		"and me.dtcancelamento is null  " +
		"and me.dtmovimentacao <= '" + SinedDateUtils.toStringPostgre(filtro.getDtestoque()) + "' " +
		(filtro.getMaterialgrupo() != null ? ("and m.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo() + " ") : "") +
		(filtro.getEmpresa() != null ? ("and me.cdempresa = " + filtro.getEmpresa().getCdpessoa() + " ") : "") +
		(filtro.getLocalarmazenagem() != null ? ("and me.cdlocalarmazenagem = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem() + " ") : "and me.cdlocalarmazenagem is null ") +
		(filtro.getMaterial() != null ? ("and m.cdmaterial = " + filtro.getMaterial().getCdmaterial() + " ") : "") +
		(StringUtils.isNotEmpty(whereInMaterial) ? (" and m.cdmaterial in (" + whereInMaterial + ") ") : "") +
		(StringUtils.isNotEmpty(whereInCodigobarras) ? (" and m.codigobarras in (" + whereInCodigobarras + ") ") : "") +
		(filtro.getEmpresa() != null ? (" and exists (" +
				" select m2.cdmaterial " +
				" from material m2 " +
				" left outer join materialempresa mEmpresa2 on mEmpresa2.cdmaterial = m2.cdmaterial " +
				" where m2.cdmaterial = m.cdmaterial " +
				" and (mEmpresa2.cdpessoa is null or mEmpresa2.cdpessoa = " + filtro.getEmpresa().getCdpessoa() + ")) ") : "") +
		
		"group by m.cdmaterial, m.nome, m.codigobarras, m.produto, m.epi, m.valorcusto, um.cdunidademedida, um.nome, mr.cdmaterialrateioestoque, mr.cdcontagerencialajusteentrada, mr.cdcontagerencialajustesaida, mg.registrarpesomedio, m.pesobruto " +
		(considerarLotes ? ", l.numero, l.cdloteestoque, pf.cnpj " : "") +
	
		"union all " +
	
		"select  " +
			"m.cdmaterial,  " +
			"m.nome as nome_material, " +
		    "m.codigobarras, " +
		    "m.produto as produto_boolean, " +
		    "m.epi as epi_boolean, " +
		    "m.valorcusto, " +
		    "um.cdunidademedida, " +
		    "um.nome as nome_unidademedida, " +
		    "mr.cdmaterialrateioestoque, " +
		    "mr.cdcontagerencialajusteentrada, " +
		    "mr.cdcontagerencialajustesaida, " +
		    "0 as qtde_saida, " +
		    "sum(me.qtde) as qtde_entrada, " +
		    "mg.registrarpesomedio, " +
		    (considerarLotes ? "l.numero as lote_numero, l.cdloteestoque as lote_id, pf.cnpj as fornecedor_cnpj, " : "") +
		    "m.pesobruto " +
		    
		"from material m " +
		"join unidademedida um on um.cdunidademedida = m.cdunidademedida " +
		"left outer join movimentacaoestoque me on me.cdmaterial = m.cdmaterial  " +
		"left outer join materialgrupo mg on mg.cdmaterialgrupo = m.cdmaterialgrupo  " +
		"left outer join materialrateioestoque mr on mr.cdmaterialrateioestoque = m.cdmaterialrateioestoque " +
		(considerarLotes ? "left outer join loteestoque l on l.cdloteestoque = me.cdloteestoque " : "") +
		(considerarLotes ? "left outer join pessoa pf on pf.cdpessoa = l.cdfornecedor " : "") +
	
		"where m.ativo = true " +
		"and (m.patrimonio = false or m.patrimonio is null) " +
		"and (m.servico = false or m.servico is null) " +
		"and (m.vendapromocional = false or m.vendapromocional is null) " +
		"and me.cdmovimentacaoestoquetipo = 1  " +
		"and me.dtcancelamento is null  " +
		"and me.dtmovimentacao <= '" + SinedDateUtils.toStringPostgre(filtro.getDtestoque()) + "' " +
		(filtro.getMaterialgrupo() != null ? ("and m.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo() + " ") : "") +
		(filtro.getEmpresa() != null ? ("and me.cdempresa = " + filtro.getEmpresa().getCdpessoa() + " ") : "") +
		(filtro.getLocalarmazenagem() != null ? ("and me.cdlocalarmazenagem = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem() + " ") : "and me.cdlocalarmazenagem is null ") +
		(filtro.getMaterial() != null ? ("and m.cdmaterial = " + filtro.getMaterial().getCdmaterial() + " ") : "") +
		(StringUtils.isNotEmpty(whereInMaterial) ? (" and m.cdmaterial in (" + whereInMaterial + ") ") : "") +
		(StringUtils.isNotEmpty(whereInCodigobarras) ? (" and m.codigobarras in (" + whereInCodigobarras + ") ") : "") +
		(filtro.getEmpresa() != null ? (" and exists (" +
				" select m2.cdmaterial " +
				" from material m2 " +
				" left outer join materialempresa mEmpresa2 on mEmpresa2.cdmaterial = m2.cdmaterial " +
				" where m2.cdmaterial = m.cdmaterial " +
				" and (mEmpresa2.cdpessoa is null or mEmpresa2.cdpessoa = " + filtro.getEmpresa().getCdpessoa() + ")) ") : "") +
		
		"group by m.cdmaterial, m.nome, m.codigobarras, m.produto, m.epi, m.valorcusto, um.cdunidademedida, um.nome, mr.cdmaterialrateioestoque, mr.cdcontagerencialajusteentrada, mr.cdcontagerencialajustesaida, mg.registrarpesomedio, m.pesobruto " +
		(considerarLotes ? ", l.numero, l.cdloteestoque, pf.cnpj " : "") +
		
		"union all " +
		
		"select " +
			"m.cdmaterial,  " +
			"m.nome as nome_material, " +
		    "m.codigobarras, " +
		    "m.produto as produto_boolean, " +
		    "m.epi as epi_boolean, " +
		    "m.valorcusto, " +
		    "um.cdunidademedida, " +
		    "um.nome as nome_unidademedida, " +
		    "mr.cdmaterialrateioestoque, " +
		    "mr.cdcontagerencialajusteentrada, " +
		    "mr.cdcontagerencialajustesaida, " +
		    "0 as qtde_saida, " +
		    "0 as qtde_entrada, " +
		    "mg.registrarpesomedio, " +
		    (considerarLotes ? "l.numero as lote_numero, l.cdloteestoque as lote_id, pf.cnpj as fornecedor_cnpj, " : "") +
		    "m.pesobruto " +
		    
	    "from material m " +
		"join unidademedida um on um.cdunidademedida = m.cdunidademedida " +
		"left outer join materialgrupo mg on mg.cdmaterialgrupo = m.cdmaterialgrupo  " +
		"left outer join materialrateioestoque mr on mr.cdmaterialrateioestoque = m.cdmaterialrateioestoque " +
		(considerarLotes ? "left outer join lotematerial lm on lm.cdmaterial = m.cdmaterial " : "") +
		(considerarLotes ? "left outer join loteestoque l on l.cdloteestoque = lm.cdloteestoque " : "") +
		(considerarLotes ? "left outer join pessoa pf on pf.cdpessoa = l.cdfornecedor " : "") +
		
		"where m.ativo = true " +
		"and (m.patrimonio = false or m.patrimonio is null) " +
		"and (m.servico = false or m.servico is null) " +
		"and (m.vendapromocional = false or m.vendapromocional is null) " +
		(filtro.getMaterialgrupo() != null ? ("and m.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo() + " ") : "") +
		(filtro.getMaterial() != null ? ("and m.cdmaterial = " + filtro.getMaterial().getCdmaterial() + " ") : "") +
		(StringUtils.isNotEmpty(whereInMaterial) ? (" and m.cdmaterial in (" + whereInMaterial + ") ") : "") +
		(StringUtils.isNotEmpty(whereInCodigobarras) ? (" and m.codigobarras in (" + whereInCodigobarras + ") ") : "") +
		(filtro.getEmpresa() != null ? (" and exists (" +
				" select m2.cdmaterial " +
				" from material m2 " +
				" left outer join materialempresa mEmpresa2 on mEmpresa2.cdmaterial = m2.cdmaterial " +
				" where m2.cdmaterial = m.cdmaterial " +
				" and (mEmpresa2.cdpessoa is null or mEmpresa2.cdpessoa = " + filtro.getEmpresa().getCdpessoa() + ")) ") : "") +
				
		") query " +
	
		"group by cdmaterial, nome_material, codigobarras, produto_boolean, epi_boolean, valorcusto, cdunidademedida, nome_unidademedida, cdmaterialrateioestoque, cdcontagerencialajusteentrada, cdcontagerencialajustesaida, registrarpesomedio, pesobruto " +
		(considerarLotes ? ", lote_numero, lote_id, fornecedor_cnpj " : "") +
		"order by upper(nome_material) ";
		

		SinedUtil.markAsReader();
		List<AtualizacaoEstoqueMaterialBean> lista = getJdbcTemplate().query(sql, 
			new RowMapper() {
				public AtualizacaoEstoqueMaterialBean mapRow(ResultSet rs, int rowNum) throws SQLException {
					AtualizacaoEstoqueMaterialBean bean = new AtualizacaoEstoqueMaterialBean();
					
					Material material = new Material();
					material.setCdmaterial(rs.getInt("cdmaterial"));
					material.setNome(rs.getString("nome_material"));
					material.setCodigobarras(rs.getString("codigobarras"));
					material.setProduto(rs.getBoolean("produto_boolean"));
					material.setEpi(rs.getBoolean("epi_boolean"));
					material.setValorcusto(rs.getDouble("valorcusto"));
					
					Unidademedida unidademedida = new Unidademedida();
					unidademedida.setCdunidademedida(rs.getInt("cdunidademedida"));
					unidademedida.setNome(rs.getString("nome_unidademedida"));
					
					
					MaterialRateioEstoque materialRateioEstoque = new MaterialRateioEstoque();
					if (rs.getInt("cdcontagerencialajusteentrada") != 0 || rs.getInt("cdcontagerencialajustesaida") != 0){
						materialRateioEstoque.setCdMaterialRateioEstoque(rs.getInt("cdmaterialrateioestoque"));
						if (rs.getInt("cdcontagerencialajusteentrada") != 0){
							materialRateioEstoque.setContaGerencialAjusteEntrada(new Contagerencial(rs.getInt("cdcontagerencialajusteentrada")));
						}
						if (rs.getInt("cdcontagerencialajustesaida") != 0){
							materialRateioEstoque.setContaGerencialAjusteSaida(new Contagerencial(rs.getInt("cdcontagerencialajustesaida")));
						}	
					}
					
					bean.setMaterial(material);
					bean.setUnidademedida(unidademedida);
					bean.setQtdeSistema(SinedUtil.round(rs.getDouble("qtde_sistema"), 14));
					bean.setQtdeAtual(SinedUtil.round(rs.getDouble("qtde_sistema"), 14));
					bean.setPesomedio(rs.getDouble("pesobruto"));
					bean.setRegistrarpesomedio(rs.getBoolean("registrarpesomedio"));
					bean.getMaterial().setMaterialRateioEstoque(materialRateioEstoque);
					bean.setCentroCusto(filtro.getCentroCusto());
					
					if(considerarLotes != null && considerarLotes){
						bean.setLote(rs.getString("lote_numero"));
						bean.setCnpj(rs.getString("fornecedor_cnpj"));
						if(rs.getInt("lote_id") != 0){
							bean.setLoteestoque(new Loteestoque(rs.getInt("lote_id")));							
						}
					}
					
					return bean;
				}
			}
		);
		
		return lista;
	}
	
	public void inativarMaterial(Material material) {
		if(material != null && material.getCdmaterial() != null){
			getJdbcTemplate().update("UPDATE material SET ativo = false WHERE cdmaterial = ?",
					new Object[] {material.getCdmaterial()});
		}
	}

	public void ativarMaterial(Material material) {
		if(material != null && material.getCdmaterial() != null){
			getJdbcTemplate().update("UPDATE material SET ativo = true WHERE cdmaterial = ?",
					new Object[] {material.getCdmaterial()});
		}
	}

	public List<Material> findForSolicitacaoCompraFromGerenciamentomaterial(String whereIn) {
		if(whereIn == null || whereIn.isEmpty()) {
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
				.select("material.cdmaterial, material.nome")
				.whereIn("material.cdmaterial", whereIn)
				.list();
	}

	/**
	 * M�todo que busca os materiais para copiar formula de valor de venda
	 *
	 * @param materialgrupo
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 21/05/2014
	 */
	public List<Material> findForCopiarFormulavalorvenda(Materialgrupo materialgrupo, Material material) {
		if(materialgrupo == null || materialgrupo.getCdmaterialgrupo() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("material.cdmaterial, material.valorcustomateriaprima, " +
						"listaMaterialformulavalorvenda.cdmaterialformulavalorvenda, " +
						"listaMaterialformulamargemcontribuicao.cdmaterialformulamargemcontribuicao, " +
						"listaMaterialformulacusto.cdmaterialformulacusto")
				.leftOuterJoin("material.listaMaterialformulacusto listaMaterialformulacusto")
				.leftOuterJoin("material.listaMaterialformulavalorvenda listaMaterialformulavalorvenda")
				.leftOuterJoin("material.listaMaterialformulamargemcontribuicao listaMaterialformulamargemcontribuicao")
				.where("material.materialgrupo = ?", materialgrupo)
				.where("material <> ?", material != null && material.getCdmaterial() != null ? material : null)
				.list();
	}
	
	public List<Material> findForCopiarFormulavalorvendaminimo(Materialgrupo materialgrupo, Material material) {
		if(materialgrupo == null || materialgrupo.getCdmaterialgrupo() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("material.cdmaterial, material.valorcustomateriaprima, listaMaterialformulavendaminimo.cdmaterialformulavendaminimo ")
				.leftOuterJoin("material.listaMaterialformulavendaminimo listaMaterialformulavendaminimo")
				.where("material.materialgrupo = ?", materialgrupo)
				.where("material <> ?", material != null && material.getCdmaterial() != null ? material : null)
				.list();
	}
	
	public List<Material> findForCopiarFormulavalorvendamaximo(Materialgrupo materialgrupo, Material material) {
		if(materialgrupo == null || materialgrupo.getCdmaterialgrupo() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("material.cdmaterial, material.valorcustomateriaprima, listaMaterialformulavendamaximo.cdmaterialformulavendamaximo ")
				.leftOuterJoin("material.listaMaterialformulavendamaximo listaMaterialformulavendamaximo")
				.where("material.materialgrupo = ?", materialgrupo)
				.where("material <> ?", material != null && material.getCdmaterial() != null ? material : null)
				.list();
	}

	/**
	 * M�todo que carrega o material com as formulas de valor de venda
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 21/05/2014
	 */
	public Material loadForCalcularValorvenda(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
			.setUseReadOnly(false)
			.select("material.cdmaterial, material.valorcusto, material.valorvenda, material.peso, material.pesobruto, material.origemproduto, " +
					"produto.altura, produto.largura, produto.comprimento, materialgrupo.cdmaterialgrupo, " +
					"listaMaterialformulavalorvenda.cdmaterialformulavalorvenda, listaMaterialformulavalorvenda.identificador, " +
					"listaMaterialformulavalorvenda.ordem, listaMaterialformulavalorvenda.formula, " +
					"listaProducao.cdmaterialproducao, listaProducao.exibirvenda, materialP.cdmaterial")
			.leftOuterJoin("material.listaMaterialformulavalorvenda listaMaterialformulavalorvenda")
			.leftOuterJoin("material.materialproduto produto")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.listaProducao listaProducao")
			.leftOuterJoin("listaProducao.material materialP")
			.where("material = ?", material)
			.unique();
	}
	
	public List<Material> findForCalcularValorvenda(String whereIn) {
		if (StringUtils.isEmpty(whereIn)) 
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("material.cdmaterial, material.valorcusto, material.valorvenda, material.peso, material.pesobruto, material.origemproduto, material.valorfrete, " +
					"produto.altura, produto.largura, produto.comprimento, materialgrupo.cdmaterialgrupo, " +
					"listaMaterialformulavalorvenda.cdmaterialformulavalorvenda, listaMaterialformulavalorvenda.identificador, " +
					"listaMaterialformulavalorvenda.ordem, listaMaterialformulavalorvenda.formula, " +
					"listaProducao.cdmaterialproducao, listaProducao.exibirvenda, materialP.cdmaterial")
			.leftOuterJoin("material.listaMaterialformulavalorvenda listaMaterialformulavalorvenda")
			.leftOuterJoin("material.materialproduto produto")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.listaProducao listaProducao")
			.leftOuterJoin("listaProducao.material materialP")
			.whereIn("material.cdmaterial", whereIn)
			.list();
	}
	
	public List<Material> findForCalcularValorvendaminimo(String whereIn) {
		if (StringUtils.isEmpty(whereIn)) 
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
			.setUseReadOnly(false)
			.select("material.cdmaterial, material.valorcusto, material.valorvenda, material.peso, material.pesobruto, material.origemproduto, material.valorfrete, " +
					"produto.altura, produto.largura, produto.comprimento, materialgrupo.cdmaterialgrupo, " +
					"listaMaterialformulavendaminimo.cdmaterialformulavendaminimo, listaMaterialformulavendaminimo.identificador, " +
					"listaMaterialformulavendaminimo.ordem, listaMaterialformulavendaminimo.formula, " +
					"listaProducao.cdmaterialproducao, listaProducao.exibirvenda, materialP.cdmaterial")
			.leftOuterJoin("material.listaMaterialformulavendaminimo listaMaterialformulavendaminimo")
			.leftOuterJoin("material.materialproduto produto")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.listaProducao listaProducao")
			.leftOuterJoin("listaProducao.material materialP")
			.whereIn("material.cdmaterial", whereIn)
			.list();
	}
	
	public List<Material> findForCalcularValorvendamaximo(String whereIn) {
		if (StringUtils.isEmpty(whereIn)) 
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
			.setUseReadOnly(false)
			.select("material.cdmaterial, material.valorcusto, material.valorvenda, material.peso, material.pesobruto, material.origemproduto, material.valorfrete, " +
					"produto.altura, produto.largura, produto.comprimento, materialgrupo.cdmaterialgrupo, " +
					"listaMaterialformulavendamaximo.cdmaterialformulavendamaximo, listaMaterialformulavendamaximo.identificador, " +
					"listaMaterialformulavendamaximo.ordem, listaMaterialformulavendamaximo.formula, " +
					"listaProducao.cdmaterialproducao, listaProducao.exibirvenda, materialP.cdmaterial")
			.leftOuterJoin("material.listaMaterialformulavendamaximo listaMaterialformulavendamaximo")
			.leftOuterJoin("material.materialproduto produto")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.listaProducao listaProducao")
			.leftOuterJoin("listaProducao.material materialP")
			.whereIn("material.cdmaterial", whereIn)
			.list();
	}
	
	/**
	* M�todo que carrega o material com a formula de margem de contribui��o
	* 
	* @param whereIn
	* @return
	* @since 24/06/2015
	* @author Luiz Fernando
	*/
	public List<Material> findForCalcularMargemcontribuicao(String whereIn) {
		if (StringUtils.isEmpty(whereIn)) 
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("material.cdmaterial, material.valorcusto, material.valorvenda, material.peso, material.pesobruto, material.origemproduto, " +
					"produto.altura, produto.largura, produto.comprimento, materialgrupo.cdmaterialgrupo, " +
					"listaMaterialformulamargemcontribuicao.cdmaterialformulamargemcontribuicao, listaMaterialformulamargemcontribuicao.identificador, " +
					"listaMaterialformulamargemcontribuicao.ordem, listaMaterialformulamargemcontribuicao.formula")
			.leftOuterJoin("material.listaMaterialformulamargemcontribuicao listaMaterialformulamargemcontribuicao")
			.leftOuterJoin("material.materialproduto produto")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.whereIn("material.cdmaterial", whereIn)
			.list();
	}
	
	/**
	* M�todo para ajustar NCM, Peso Liquido, Peso Bruto e Material Mestre da Grade
	*  
	*
	* @param material, materialajustarpreco
	* @since Jun 13, 2014
	* @author Lucas Costa
	*/
	public void updateMaterialAtualizarLote(Material material, Materialajustarpreco materialajustarpreco) {
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Par�metros inv�lidos");
		}
		StringBuilder query = new StringBuilder("update Material set ");

		boolean existUpdate = false; 
		if(materialajustarpreco.getMestreGrade()!= null && materialajustarpreco.getMestreGrade().getCdmaterial() != null){
			query.append(" cdmaterialmestregrade = " + materialajustarpreco.getMestreGrade().getCdmaterial());
			existUpdate = true;
		}
		if(StringUtils.isNotEmpty(materialajustarpreco.getPesoLiquido())){
			query.append(existUpdate ? ", " : "");
			query.append(" peso = " + materialajustarpreco.getPesoLiquido());
			existUpdate = true;
		}
		if(StringUtils.isNotEmpty(materialajustarpreco.getPesoBruto())){
			query.append(existUpdate ? ", " : "");
			query.append(" pesobruto = " + materialajustarpreco.getPesoBruto());
			existUpdate = true;
		}
		if(StringUtils.isNotEmpty(materialajustarpreco.getNcm())){
			query.append(existUpdate ? ", " : "");
			query.append(" ncmcompleto = '" + materialajustarpreco.getNcm() + "'");
			existUpdate = true;
		}
		
		query.append(" where cdmaterial = " + material.getCdmaterial()); 
		if(existUpdate){
			getJdbcTemplate().execute(query.toString());
		}
	}
	
	public Material loadWithMaterialMestre(Material material){
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("material.cdmaterial, materialmestregrade.cdmaterial")
				.entity(material)
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.unique();
	}
	
	public Material loadWithMaterialMestre(Material material, boolean carregarValorMinimoMaximo){
		if(carregarValorMinimoMaximo == false) {
			return loadWithMaterialMestre(material);
		}
		return query()
				.select("material.cdmaterial, materialmestregrade.cdmaterial, materialmestregrade.valorvendaminimo, materialmestregrade.valorvendamaximo ")
				.entity(material)
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.unique();
	}

	/**
	* M�todo que carrega o material para calcular o valor m�dio do custo
	*
	* @param material
	* @return
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public Material loadForCalcularCustoMedio(Material material) {
		if (material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo.");
		
		return querySined()
			.setUseWhereEmpresa(false)
			.setUseWhereProjeto(false)
			.select("material.cdmaterial, material.nome, material.dtcustoinicial, material.qtdecustoinicial, material.valorcustoinicial," +
					"unidademedida.cdunidademedida, unidademedida.casasdecimaisestoque")
			.join("material.unidademedida unidademedida")
			.where("material = ?", material)
			.unique();
	}

	/**
	* M�todo que atualiza os valores refente aos custos iniciais (dtcustoinicial, qtdecustoinicial, valorcustoinicial)
	*
	* @param whereIn
	* @param dtcustoinicial
	* @param qtdecustoinicial
	* @param valorcustoinicial
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public void updateValoresIniciais(String whereIn, Date dtcustoinicial, Double qtdecustoinicial, Double valorcustoinicial) {
		if(!StringUtils.isEmpty(whereIn)){
			getJdbcTemplate().update("update material set dtcustoinicial = ?, " +
					"qtdecustoinicial = ?, " +
					"valorcustoinicial = ? " +
					"where cdmaterial in ("+whereIn+")", new Object[]{dtcustoinicial, qtdecustoinicial, valorcustoinicial});
		}
	}
	
	public void updateCustoPorEmpresaMaterial(MaterialCustoEmpresa custoEmpresa, Date dtInicial, Double qtdecustoinicialPorEmpresa, Double custoInicial) {
		getJdbcTemplate().update("update materialcustoempresa set dtinicio = ?, " + 
				"quantidadereferencia = ?, " + 
				"custoinicial = ? " + 
				"where cdmaterialcustoempresa = ?", new Object[]{dtInicial, qtdecustoinicialPorEmpresa, custoInicial, custoEmpresa.getCdMaterialCustoEmpresa()});
	}

	/**
	* M�todo que salva o valor custo do material
	*
	* @param material
	* @param valorCustoMedio
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public void updateValorCusto(Material material, Double valorCustoMedio) {
		if(material != null && material.getCdmaterial() != null && valorCustoMedio != null){
			getJdbcTemplate().update("update material set valorcusto = ? " +
					" where cdmaterial = ? ", new Object[]{valorCustoMedio, material.getCdmaterial()});
		}
	}
	
	public void updateValorVendaminimo(Material material, Double valor) {
		if(material != null && material.getCdmaterial() != null && valor != null){
			getJdbcTemplate().update("update material set valorvendaminimo = ? " +
					" where cdmaterial = ? ", new Object[]{valor, material.getCdmaterial()});
		}
	}
	
	public void updateValorVendamaximo(Material material, Double valor) {
		if(material != null && material.getCdmaterial() != null && valor != null){
			getJdbcTemplate().update("update material set valorvendamaximo = ? " +
					" where cdmaterial = ? ", new Object[]{valor, material.getCdmaterial()});
		}
	}
	
	/**
	 * Atualiza o valor de venda no banco
	 *
	 * @param material
	 * @param valorvenda
	 * @author Rodrigo Freitas
	 * @since 09/10/2015
	 */
	public void updateValorVenda(Material material, Double valorvenda) {
		if(material != null && material.getCdmaterial() != null && valorvenda != null){
			getJdbcTemplate().update("update material set valorvenda = ? where cdmaterial = ? ", new Object[]{valorvenda, material.getCdmaterial()});
		}
	}

	/**
	* M�todo que carrega os materiais com o cdmaterial
	*
	* @param whereIn
	* @return
	* @since 03/09/2014
	* @author Luiz Fernando
	*/
	public List<Material> findForExcluirGrade(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("material.cdmaterial, material.nome")
				.whereIn("material.cdmaterial", whereIn)
				.list();
	}

	/**
	* M�todo que busca os materiais com o valor de custo inicial nulo
	*
	* @param whereIn
	* @return
	* @since 24/09/2014
	* @author Luiz Fernando
	*/
	public List<Material> buscarMaterialValidacaoCustomedio(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("material.cdmaterial, material.nome, material.valorcustoinicial, material.dtcustoinicial, " +
						"listaMaterialCustoEmpresa.cdMaterialCustoEmpresa, listaMaterialCustoEmpresa.dtInicio, listaMaterialCustoEmpresa.custoInicial")
				.leftOuterJoin("material.listaMaterialCustoEmpresa listaMaterialCustoEmpresa")
				.whereIn("material.cdmaterial", whereIn)
				.list();
	}
	
	/**
	 * M�todo de autocomplete or fornecedor
	 * 
	 * @param fornecedor
	 * @param codigo
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Material> findByFornecedorForAutocomplete(Fornecedor fornecedor, String material) {
		if(fornecedor == null || fornecedor.getCdpessoa() == null){
			fornecedor = new Fornecedor();
		}
		
		Integer cdmaterial = null;
		try{
			cdmaterial = Integer.parseInt(material);
		} catch (Exception e) {}
		
		QueryBuilder<Material> query = query()
					.select("material.cdmaterial, material.nome, material.identificacao")
					.leftOuterJoin("material.materialgrupo materialgrupo")
					.leftOuterJoin("material.listaFornecedor listaFornecedor")
					.leftOuterJoin("listaFornecedor.fornecedor fornecedor")
					.openParentheses()
						.whereLikeIgnoreAll("material.nome", material);
		
		if(cdmaterial != null){
			query
				.or()
				.where("material.cdmaterial = ?", cdmaterial);
		}
		
		query
						.or()
						.whereLikeIgnoreAll("material.identificacao", material)
					.closeParentheses()
					.where("material.ativo = ?", Boolean.TRUE)
					.openParentheses()
						.where("fornecedor.cdpessoa = ?", fornecedor.getCdpessoa())
						.or()
						.where("listaFornecedor.cdmaterialfornecedor is null")
					.closeParentheses()
					.openParentheses()
						.where("material.servico is null")
						.or()
						.where("material.servico = ?", Boolean.FALSE)
					.closeParentheses()
//					.where("not exists (select m.cdmaterial " +
//										" from Material m " +
//										" where m.materialmestregrade = material)")
					.autocomplete();
		
		query.orderBy("materialgrupo.gradeestoquetipo desc, material.materialmestregrade desc");
		return query.list();
	}

	/**
	* M�todo que carrega os materiais de produ��o de acordo com as mat�rias-primas
	*
	* @param whereInMateriaprima
	* @return
	* @since 16/10/2014
	* @author Luiz Fernando
	*/
	public List<Material> buscarMaterialProducaoAtualizarConsumo(String whereInMateriaprima) {
		if(StringUtils.isEmpty(whereInMateriaprima))
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				.setUseWhereEmpresa(false)
				.setUseWhereProjeto(false)
				.select("material.cdmaterial, material.nome")
				.where("material.producao = true")
				.where("exists (select 1 from Materialproducao mp " +
								"where mp.materialmestre.cdmaterial = material.cdmaterial " +
								"and  mp.material.cdmaterial in (" + whereInMateriaprima + ") )")
				.list();
	}
	
	public List<Material> buscarMaterialKitAtualizarConsumo(String whereInMateriaprima) {
		if(StringUtils.isEmpty(whereInMateriaprima))
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				.setUseWhereEmpresa(false)
				.setUseWhereProjeto(false)
				.select("material.cdmaterial, material.nome")
				.where("material.vendapromocional = true")
				.where("exists (select 1 from Materialrelacionado mr " +
								"where mr.material.cdmaterial = material.cdmaterial " +
								"and  mr.materialpromocao.cdmaterial in (" + whereInMateriaprima + ") )")
				.list();
	}


	/**
	* M�todo que carrega os materiais para emiss�o de etiqueta
	*
	* @param whereIn
	* @return
	* @author Jo�o Vitor
	*/
	public List<Material> buscarMaterialForEtiqueta(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
			.select("material.cdmaterial, material.nome, material.identificacao, material.codigobarras, material.valorvenda, " +
					"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
					"material.valorcusto, materialcor.cdmaterialcor, materialcor.nome, materialgrupo.cdmaterialgrupo, materialgrupo.nome, " +
					"materialproduto.cdmaterial, materialproduto.nomereduzido")
			.leftOuterJoin("material.materialcor materialcor")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("material.materialproduto materialproduto")
			.whereIn("material.cdmaterial", whereIn)
			.list();
	}
	
	/**
	* M�todo que carrega os materiais para pop up de emiss�o de etiqueta
	*
	* @param whereIn
	* @return
	* @author Jo�o Vitor
	*/
	public List<Material> buscarMaterialForPopUpEtiqueta(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
			.select("material.cdmaterial, material.nome, unidademedida.cdunidademedida,unidademedida.etiquetaunica") 
			.leftOuterJoin("material.unidademedida unidademedida")
			.whereIn("material.cdmaterial", whereIn)
			.list();
	}
	
	/**
	 * M�todo que carrega o material cujo flag 'materialcte' foi marcado.
	 * 
	 * @return
	 * @author Rafael Salvio
	 */
	public Material getMaterialForImportacaoCte(){
		return query()
				.joinFetch("material.unidademedida unidademedida")
				.where("material.materialcte is true")
				.unique();
	}
	
	/**
	 * M�todo respons�vel por alterar o status do 
	 * material para ativo caso haja devolu��o
	 * @param material
	 */
	public void ativaMaterial(Material material) {
		getJdbcTemplate().update("update material set ativo = true " +
				" where cdmaterial = ? ", new Object[]{material.getCdmaterial()});
	}

	/**
	* M�todo que busca os materiais de ajuste fiscal para o autocomplete 
	*
	* @param s
	* @param whereInMaterial
	* @return
	* @since 06/01/2015
	* @author Luiz Fernando
	*/
	public List<Material> findAjusteicms(String s, String whereInMaterial) {
		return query()
			.autocomplete()
			.select("material.cdmaterial, material.nome, material.identificacao")
			.leftOuterJoin("material.materialtipo materialtipo")
			.where("material.ativo = true")
			.openParentheses()
				.whereLikeIgnoreAll("material.nome", s)
				.or()
				.whereLikeIgnoreAll("material.identificacao", s)
			.closeParentheses()
			.whereIn("material.cdmaterial", whereInMaterial)
			.orderBy("material.nome")
			.list();
	}

	/**
	* M�todo que verifica se o material tem restri��o por empresa
	*
	* @param whereIn
	* @return
	* @since 10/03/2015
	* @author Luiz Fernando
	*/
	public boolean existeRestricaoMaterialempresa(String whereIn) {
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Material.class)
			.join("material.listaMaterialempresa listaMaterialempresa")
			.join("listaMaterialempresa.empresa empresa")
			.whereIn("material.cdmaterial", whereIn)
			.unique() > 0;
	}

	public Material loadKitFlexivel(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("material.cdmaterial, material.nome, material.identificacao, material.exibiritenskitflexivelvenda, material.kitflexivel, "
					+ "unidademedida.cdunidademedida, unidademedida.nome, "
					+ "listaMaterialkitflexivel.cdmaterialkitflexivel, listaMaterialkitflexivel.valorvenda, listaMaterialkitflexivel.quantidade, "
					+ "materialkit.cdmaterial, materialkit.nome, materialkit.identificacao, "
					+ "unidademedidakit.cdunidademedida, unidademedidakit.nome, "
					+ "materialkitflexivelformula.cdmaterialkitflexivelformula, materialkitflexivelformula.ordem, materialkitflexivelformula.identificador, materialkitflexivelformula.formula, "
					+ "materialproduto.fatorconversao ")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("material.listaMaterialkitflexivel listaMaterialkitflexivel")
				.leftOuterJoin("listaMaterialkitflexivel.materialkit materialkit")
				.leftOuterJoin("materialkit.unidademedida unidademedidakit")
				.leftOuterJoin("listaMaterialkitflexivel.listaMaterialkitflexivelformula materialkitflexivelformula")
				.leftOuterJoin("material.materialproduto materialproduto")
				.where("material = ?", material)
				.unique();
	}

	/**
	* M�todo que carrega os materiais para a formula de custo
	*
	* @param whereInMaterial
	* @return
	* @since 18/05/2015
	* @author Luiz Fernando
	*/
	public List<Material> findForCalcularValorcusto(String whereInMaterial) {
		if(whereInMaterial == null || "".equals(whereInMaterial))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("material.cdmaterial, material.valorcustomateriaprima, materialgrupo.cdmaterialgrupo, materialgrupo.rateiocustoproducao ")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.whereIn("material.cdmaterial", whereInMaterial)
			.list();
	}

	/**
	* M�todo que busca o material com o rateiocustoproducao do grupo de material 
	*
	* @param whereInMaterial
	* @return
	* @since 18/05/2015
	* @author Luiz Fernando
	*/
	public List<Material> findForCustoOperacional(String whereInMaterial) {
		if(whereInMaterial == null || "".equals(whereInMaterial))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("material.cdmaterial, material.producao, materialgrupo.cdmaterialgrupo, materialgrupo.rateiocustoproducao ")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.where("coalesce(material.producao, false) = true")
			.whereIn("material.cdmaterial", whereInMaterial)
			.list();
	}
	
	/**
	* M�todo que busca o material para a edi��o da comiss�o de ag�ncias de venda.
	*
	* @param whereIn
	* @return
	* @since 02/06/2015
	* @author Jo�o Vitor
	*/
	public List<Material> findForComissaoAgencia(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("material.cdmaterial, materialgrupo.cdmaterialgrupo, materialgrupocomissaovenda.cdmaterialgrupocomissaovenda, " +
					"materialgrupocomissaovenda.comissaopara, categoria.cdcategoria, fornecedor.cdpessoa, pedidovendatipo.cdpedidovendatipo, " +
					"cargo.cdcargo, documentotipo.cddocumentotipo, " +
					"comissionamento.cdcomissionamento, comissionamento.nome, comissionamento.percentual ")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("materialgrupo.listaMaterialgrupocomissaovenda materialgrupocomissaovenda")
			.leftOuterJoin("materialgrupocomissaovenda.cargo cargo")
			.leftOuterJoin("materialgrupocomissaovenda.comissionamento comissionamento")
			.leftOuterJoin("materialgrupocomissaovenda.documentotipo documentotipo")
			.leftOuterJoin("materialgrupocomissaovenda.pedidovendatipo pedidovendatipo")
			.leftOuterJoin("materialgrupocomissaovenda.fornecedor fornecedor")
			.leftOuterJoin("materialgrupocomissaovenda.categoria categoria")
			.whereIn("material.cdmaterial", whereIn)
			.list();
	}

	/**
	* M�todo que retorna os materiais que tenham o parametro na formula de custo
	*
	* @param param
	* @return
	* @since 23/09/2015
	* @author Luiz Fernando
	*/
	public List<Material> findRecalcularCustoAposAlteracaoTabelavalorCustoComercial(String param) {
		if(StringUtils.isBlank(param) || (!Tabelavalor.CUSTO_OPERACIONAL_ADMINISTRATIVO.equalsIgnoreCase(param) &&
				!Tabelavalor.CUSTO_COMERCIAL_ADMINISTRATIVO.equalsIgnoreCase(param)))
			return new ArrayList<Material>();
		
		return query()
				.select("material.cdmaterial, material.nome")
				.join("material.listaMaterialformulacusto listaMaterialformulacusto")
				.openParentheses()
					.whereLikeIgnoreAll("listaMaterialformulacusto.formula", 
						(Tabelavalor.CUSTO_OPERACIONAL_ADMINISTRATIVO.equals(param) ?
						"custooperacionaladministrativo" : "custocomercialadministrativo"))
					.or()
					.whereLikeIgnoreAll("listaMaterialformulacusto.formula", 
						(Tabelavalor.CUSTO_OPERACIONAL_ADMINISTRATIVO.equals(param) ?
						"custo_operacional_administrativo" : "custo_comercial_administrativo"))
				.closeParentheses()
				.list();		
	}
	
	/**
	* M�todo que carrega o material com os dados para calcular o valor de custo
	*
	* @param whereIn
	* @return
	* @since 23/09/2015
	* @author Luiz Fernando
	*/
	public List<Material> findForCalcularCusto(String whereIn) {
		if (StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("material.cdmaterial, material.valorcustomateriaprima, " +
					"materialgrupo.cdmaterialgrupo, materialgrupo.rateiocustoproducao, " +
					"listaMaterialformulacusto.cdmaterialformulacusto, listaMaterialformulacusto.identificador, " +
					"listaMaterialformulacusto.ordem, listaMaterialformulacusto.formula ")
			.join("material.materialgrupo materialgrupo")
			.join("material.listaMaterialformulacusto listaMaterialformulacusto")
			.whereIn("material.cdmaterial", whereIn)
			.list();
	}
	
	/**
	* M�todo que atualiza o valor custo materia prima de acordo com o material
	*
	* @param material
	* @param valorcustomateriaprima
	* @since 19/10/2015
	* @author Luiz Fernando
	*/
	public void updateValorcustomateriaprima(Material material, Money valorcustomateriaprima) {
		if(material != null && material.getCdmaterial() != null && valorcustomateriaprima != null){
			getJdbcTemplate().update("update material set valorcustomateriaprima = ? " +
					" where cdmaterial = ? ", new Object[]{valorcustomateriaprima.getValue().doubleValue()*100, material.getCdmaterial()});
		}
	}

	/**
	* M�todo que busca os materiais para sincroniza��o com o wms
	*
	* @return
	* @since 28/09/2015
	* @author Luiz Fernando
	*/
	public List<Material> findForSincronizacaoWms(Material material) {
		return query()
				.select("material.cdmaterial, material.identificacao, material.nome, material.codigofabricante, material.pesobruto, " +
						"materialgrupo.cdmaterialgrupo, materialgrupo.nome, " +
						"unidademedida.cdunidademedida, unidademedida.simbolo, " +
						"materialmestregrade.cdmaterial, materialmestregrade.identificacao, materialmestregrade.nome, " +
						"materialmestregrade.codigofabricante, materialmestregrade.pesobruto, " +
						"materialproduto.altura, materialproduto.largura, materialproduto.comprimento, " +
						"materialcor.cdmaterialcor," +
						"materialtipo.cdmaterialtipo ")
				.join("material.materialgrupo materialgrupo")
				.join("material.unidademedida unidademedida")
				.leftOuterJoin("material.materialtipo materialtipo")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.leftOuterJoin("material.materialproduto materialproduto")
				.leftOuterJoin("material.materialcor materialcor")
				.where("material.ativo = true")
				.where("material = ?", material)
				.list();
	}

	/**
	 * Busca os materiais que est�o previstao a atuliza��o da tributa��o
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/10/2015
	 */
	public List<Material> findForAtualizacaoTributacao() {
		return query()
				.select("material.cdmaterial, material.codigonbs, material.codlistaservico, material.ncmcompleto, material.extipi")
				.openParentheses()
				.where("material.codigonbs is not null")
				.or()
				.where("material.codlistaservico is not null")
				.or()
				.where("material.ncmcompleto is not null")
				.closeParentheses()
				.list();
	}

	/**
	* M�todo que carrega o material com os dados para buscar a tabela IBPT para calcular o imposto
	*
	* @param whereIn
	* @return
	* @since 27/10/2015
	* @author Luiz Fernando
	*/
	public List<Material> findCalcularImposto(String whereIn) {
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("material.cdmaterial, material.nome, material.identificacao, material.ncmcompleto, material.extipi, " +
						"material.codlistaservico, material.codigonbs, material.servico, material.produto, material.origemproduto," +
						"material.tributacaoipi, ncmcapitulo.cdncmcapitulo, ncmcapitulo.descricao ")
				.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
				.whereIn("material.cdmaterial", whereIn)
				.list();
	}

	/**
	* M�todo que retorna os materiais de acordo com o par�metro e que n�o tenham formula de custo
	*
	* @param whereIn
	* @return
	* @since 22/10/2015
	* @author Luiz Fernando
	*/
	public List<Material> findMaterialSemFormulaCusto(String whereIn) {
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				.setUseWhereEmpresa(false)
				.setUseWhereProjeto(false)
				.select("material.cdmaterial, material.nome")
				.leftOuterJoin("material.listaMaterialformulacusto listaMaterialformulacusto")
				.whereIn("material.cdmaterial", whereIn)
				.where("listaMaterialformulacusto is null")
				.list();
			
	}
	
	/**
	* M�todo que busca os materiais com refer�ncia do custo na formula de venda
	*
	* @param whereIn
	* @return
	* @since 07/03/2017
	* @author Luiz Fernando
	*/
	public List<Material> findMaterialFormulavendaComCusto(String whereIn) {
		return querySined()
			.setUseWhereEmpresa(false)
			.setUseWhereProjeto(false)
			.select("material.cdmaterial, material.nome")
			.join("material.listaMaterialformulavalorvenda listaMaterialformulavalorvenda")
			.whereIn("material.cdmaterial", whereIn)
			.whereLike("listaMaterialformulavalorvenda.formula", "material.valorcusto")
			.list();
	}
	
	public List<Material> findMaterialFormulavendaminimoComCusto(String whereIn) {
		return querySined()
			.setUseWhereEmpresa(false)
			.setUseWhereProjeto(false)
			.select("material.cdmaterial, material.nome")
			.join("material.listaMaterialformulavendaminimo listaMaterialformulavendaminimo")
			.whereIn("material.cdmaterial", whereIn)
			.whereLike("listaMaterialformulavendaminimo.formula", "material.valorcusto")
			.list();
	}
	
	public List<Material> findMaterialFormulavendamaximoComCusto(String whereIn) {
		return querySined()
			.setUseWhereEmpresa(false)
			.setUseWhereProjeto(false)
			.select("material.cdmaterial, material.nome")
			.join("material.listaMaterialformulavendamaximo listaMaterialformulavendamaximo")
			.whereIn("material.cdmaterial", whereIn)
			.whereLike("listaMaterialformulavendamaximo.formula", "material.valorcusto")
			.list();
	}
	

	/**
	 * Busca os material que usam na formula de custo o rateio operacional
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/10/2015
	 */
	public List<Material> findForCalculocustoRateiooperacional() {
		return query()
					.select("material.cdmaterial, material.valorcusto, material.valorcustomateriaprima, " +
							"materialformulacusto.cdmaterialformulacusto, materialformulacusto.ordem, " +
							"materialformulacusto.identificador, materialformulacusto.formula")
					.join("material.listaMaterialformulacusto materialformulacusto")
					.whereLike("materialformulacusto.formula", "rateiooperacional")
					.list();
	}
	
	/**
	* M�todo que carrega o material com a lista de caracteristicas
	*
	* @param whereIn
	* @return
	* @since 10/02/2016
	* @author Luiz Fernando
	*/
	public List<Material> findWithCaracteristica(String whereIn){
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select(
						"material.cdmaterial, listaCaracteristica.cdmaterialcaracteristica, listaCaracteristica.nome ")
				.leftOuterJoin("material.listaCaracteristica listaCaracteristica")
				.whereIn("material.cdmaterial", whereIn)
				.list();
	}
	
	public List<Material> findWithClasse(String whereIn){
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("material.cdmaterial, material.nome, material.servico, material.produto, material.epi, material.patrimonio ")
				.whereIn("material.cdmaterial", whereIn)
				.list();
	}
	
	/**
	 * Busca os materiais a serem enviados para o W3Producao.
	 * @param whereIn
	 * @return
	 */
	public List<Material> findForW3Producao(String whereIn){
		return query()
					.select("material.cdmaterial, material.nome, material.producao, material.patrimonio, material.ativo," +
							"material.producao, unidademedida.cdunidademedida, materialgrupo.cdmaterialgrupo, producaoetapa.cdproducaoetapa," +
							"materialacompanhabanda.cdmaterial, material.profundidadesulco")
					.leftOuterJoin("material.unidademedida unidademedida")
					.leftOuterJoin("material.producaoetapa producaoetapa")
					.leftOuterJoin("material.materialgrupo materialgrupo")
					.leftOuterJoin("material.materialacompanhabanda materialacompanhabanda")
					.whereIn("material.cdmaterial", whereIn)
					.list();
	}

	/**
	* M�todo que carrega o material com as materias-primas e equipamentos
	*
	* @param whereIn
	* @return
	* @since 26/02/2016
	* @author Luiz Fernando
	*/
	public List<Material> findForProducaoordemEquipamentos(String whereIn) {
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		return query()
			.select("material.cdmaterial, material.producao, listaProducao.cdmaterialproducao, " +
					"matproducao.cdmaterial, matproducao.nome, matproducao.identificacao, matproducao.patrimonio, producaoetapanome.cdproducaoetapanome ")
			.join("material.listaProducao listaProducao")
			.join("listaProducao.material matproducao")
			.leftOuterJoin("listaProducao.producaoetapanome producaoetapanome")
			.whereIn("material.cdmaterial", whereIn)
			.list();
	}
	
	/**
	* M�todo que carrega o material para calcular o peso
	*
	* @param material
	* @return
	* @since 26/04/2016
	* @author Luiz Fernando
	*/
	public Material loadForCalcularPeso(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
			.setUseWhereEmpresa(false)
			.setUseWhereProjeto(false)
			.select("material.cdmaterial, material.peso, produto.altura, produto.largura, produto.comprimento, produto.fatorconversao, " +
					"materialtipo.cdmaterialtipo, materialtipo.pesoespecifico")
			.leftOuterJoin("material.materialproduto produto")
			.leftOuterJoin("material.materialtipo materialtipo")
			.where("material = ?", material)
			.unique();
	}

	/**
	 * Busca os materiais para o WS SOAP
	 *
	 * @param produto
	 * @param servico
	 * @param dataReferencia
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/05/2016
	 */
	public List<Material> findForWSSOAP(Boolean produto, Boolean servico, Timestamp dataReferencia) {
		return query()
					.select("material.cdmaterial, material.nome, material.codigofabricante, unidademedida.simbolo, materialtipo.nome")
					.join("material.unidademedida unidademedida")
					.leftOuterJoin("material.materialtipo materialtipo")
					.where("material.produto = ?", produto)
					.where("material.servico = ?", servico)
					.where("material.dtaltera >= ?", dataReferencia)
					.orderBy("material.nome")
					.list();
	}
	
	public Material loadSined(Material material, String select){
		QueryBuilderSined<Material> query = querySined().removeUseWhere();
		
		if(StringUtils.isNotBlank(select)){
			query.select(select);
		}
		return query
				.where("material = ?", material)
				.unique();
	}
	
	public Material loadWithMaterialgrupoSined(Material material){
		return querySined()
				.setUseWhereEmpresa(false)
				.setUseWhereProjeto(false)
				.select("material.cdmaterial, material.producao, material.vendapromocional, material.materialgrupo")
				.where("material = ?", material)
				.unique();
	}

	/**
	* M�todo que verifica se o material tem formula de valor de venda que utiliza a fun��o do pre�o da banda
	*
	* @param material
	* @return
	* @since 14/07/2016
	* @author Luiz Fernando
	*/
	public boolean existeFormulaValorVendaComPrecoBanda(Material material) {
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Material.class)	
			.join("material.listaMaterialformulavalorvenda listaMaterialformulavalorvenda")
			.where("material = ?", material)
			.whereLike("listaMaterialformulavalorvenda.formula", "material.preco_banda()")
			.unique() > 0;
	}
	
	/**
	* M�todo que carrega o material para integra��o SOAP
	*
	* @param material
	* @return
	* @since 16/08/2016
	* @author Luiz Fernando
	*/
	public Material loadForIntegracaoSOAP(Material material){
		return query()
				.select("material.cdmaterial, material.nome, unidademedida.cdunidademedida, unidademedida.nome, materialcoleta.cdmaterial ")
				.join("material.unidademedida unidademedida")
				.leftOuterJoin("material.materialcoleta materialcoleta")
				.where("material = ?", material)
				.unique();
	}
	
	/**
	* M�todo que busca os materias para a listagem de gerenciamento de estoque
	*
	* @param q
	* @return
	* @since 22/08/2016
	* @author Luiz Fernando
	*/
	public List<Material> findMaterialAutocompleteEstoqueForPesquisa(String q) {
		QueryBuilder<Material> query = query();
		query
			.select("material.cdmaterial, material.nome, material.identificacao, " +
					"materialmestregrade.cdmaterial, materialmestregrade.identificacao")
			.leftOuterJoin("material.materialmestregrade materialmestregrade")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.materialtipo materialtipo")
			.openParentheses()
				.whereLikeIgnoreAll("material.nome", q).or()
				.whereLikeIgnoreAll("material.identificacao", q).or()
			.closeParentheses()
			.openParentheses()
				.where("materialgrupo.gradeestoquetipo is null")
				.or()
				.openParentheses()
					.openParentheses()
						.where("materialgrupo.gradeestoquetipo = ?", Gradeestoquetipo.ITEMGRADE)
						.openParentheses()
							.where("materialgrupo.pesquisasomentemestre is null ").or()
							.where("materialgrupo.pesquisasomentemestre <> true ")
						.closeParentheses()
					.closeParentheses()
					.or()
					.openParentheses()
						.where("materialgrupo.gradeestoquetipo = ?", Gradeestoquetipo.ITEMGRADE)
						.where("materialgrupo.pesquisasomentemestre = true ")
						.where("materialmestregrade is null")
					.closeParentheses()
				.closeParentheses()
				.or()
				.openParentheses()
					.where("materialgrupo.gradeestoquetipo = ?", Gradeestoquetipo.MESTRE)
					.where("materialmestregrade is null")
				.closeParentheses()
			.closeParentheses()
			.where("material.ativo = ?", Boolean.TRUE)
			.orderBy("material.nome");
		
		return query.autocomplete().list();
	}
	
	public List<Material> findForAutocompleteRomaneio(String q) {
		return query()
					.select("material.cdmaterial, material.nome, material.identificacao, material.patrimonio, listaMaterialnumeroserie.numero")
					.leftOuterJoin("material.listaMaterialnumeroserie listaMaterialnumeroserie")
					.openParentheses()
						.whereLikeIgnoreAll("material.identificacao", q)
						.or()
						.whereLikeIgnoreAll("material.nome", q)
						.or()
						.whereLikeIgnoreAll("listaMaterialnumeroserie.numero", q)
					.closeParentheses()
					.orderBy("material.identificacaoordenacao, material.nome")
					.autocomplete()
					.list();
	}	
	
	/**
	* M�todo que busca a quantidade min�ma do material por local ou por produto
	*
	* @param cdmaterial
	* @param localarmazenagem
	* @return
	* @since 09/09/2016
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public Double getQtdeMinimaMaterial(Integer cdmaterial, Localarmazenagem localarmazenagem) {
		if (cdmaterial == null) {
			throw new SinedException("O c�digo do material n�o pode ser nulo.");
		}
		
		String sql = " select case when m.definirqtdeminimaporlocal = true then ml.quantidademinima else p.qtdeminima end as qtdeminima " +
					 " from material m " +
					 " left outer join materiallocalarmazenagem ml on ml.cdmaterial = m.cdmaterial " +
					 " left outer join produto p on p.cdmaterial = m.cdmaterial " +
					 " where m.cdmaterial = " + cdmaterial +
					 " and ((m.definirqtdeminimaporlocal = true and ml.cdlocalarmazenagem = " + (localarmazenagem != null ? localarmazenagem.getCdlocalarmazenagem() : "null") + ") " +
					 " or  " +
					 " (m.definirqtdeminimaporlocal <> true and ml.cdlocalarmazenagem is null)) ";

		SinedUtil.markAsReader();
		List<Double> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getDouble("qtdeminima");
			}
		});
		
		return SinedUtil.isListNotEmpty(list) ? list.get(0) : null; 
	}

	/**
	 * For�a a sincroniza��o das tabelas do w3produ��o
	 *
	 * @param cdmaterial
	 * @author Rodrigo Freitas
	 * @since 28/09/2016
	 */
	public void atualizaW3producao(Integer cdmaterial) {
		getJdbcTemplate().execute("update material set ativo = ativo where cdmaterial = " + cdmaterial);
	}

	/**
	* M�todo que busca os materiais para verificar obrigatoriedade do lote
	*
	* @param whereInMaterial
	* @return
	* @since 07/12/2016
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public List<MaterialLoteBean> findForObrigatoriedadeLote(String whereInMaterial) {
		if(StringUtils.isBlank(whereInMaterial)){
			throw new SinedException("O c�digo do material n�o pode ser nulo.");
		}

		SinedUtil.markAsReader();
		String sql = "select m.cdmaterial as cdmaterial, m.obrigarlote as obrigarlote from Material m where m.cdmaterial in ("+ whereInMaterial + ")";
		List<MaterialLoteBean> list = getJdbcTemplate().query(sql, new RowMapper() {
			public MaterialLoteBean mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new MaterialLoteBean(rs.getInt("cdmaterial"), rs.getBoolean("obrigarlote"));
			}
		});
		
		return list;
	}

	/**
	* M�todo que busca os materiais para a integra��o SAGE
	*
	* @param whereIn
	* @param servico
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public List<Material> findForSagefiscal(String whereIn) {
		QueryBuilder<Material> query =  query()
			.select(
					"material.cdmaterial, material.nome, material.codigobarras, material.extipi, material.dtaltera, "
							+ "material.tipoitemsped, material.ncmcompleto, material.codlistaservico, "
							+ "unidademedida.cdunidademedida, unidademedida.simbolo, ncmcapitulo.cdncmcapitulo, codigoanp.cdcodigoanp, codigoanp.codigo, "
							+ "tributacaoecf.cstpis, tributacaoecf.cstcofins, "
							+ "materialProd.cdmaterial, materialProd.nome, materialProd.dtaltera, "
							+ "listaMaterialhistorico.dtaltera, listaMaterialhistoricoMaterialProd.dtaltera ")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
			.leftOuterJoin("material.codigoanp codigoanp")
			.leftOuterJoin("material.tributacaoecf tributacaoecf")
			.leftOuterJoin("material.listaMaterialhistorico listaMaterialhistorico")
			.leftOuterJoin("material.listaProducao listaProducao")
			.leftOuterJoin("listaProducao.material materialProd")
			.leftOuterJoin("materialProd.listaMaterialhistorico listaMaterialhistoricoMaterialProd");
			
		query.openParentheses();
		SinedUtil.quebraWhereIn("material.cdmaterial", whereIn, query);
		/*
		query.or();
		query.where("material.cdmaterial in (" +
					" select m.cdmaterial " +
					" from Materialproducao mp " +
					" join mp.material m " +
					" where " + SinedUtil.quebraWhereInSubQuery("mp.materialmestre.cdmaterial", whereIn) + ")");
		*/
		query.closeParentheses();
		return query.list();
	}

	/**
	* M�todo que busca as informa��es adicionais do contribuinte cadastradas para o grupo de tributa��o relacionado ao material
	*
	* @param material
	* @return
	* @since 31/01/2017
	* @author Mairon Cezar
	*/
	public Material findByInfoContribuinte(Material material){
		return query()
			.select("material.cdmaterial, material.nome, grupotributacao.infoadicionalcontrib ")
			.leftOuterJoin("material.grupotributacao grupotributacao")
			.where("material = ?", material)
			.unique(); 
	}

	/**
	 * Carrega informa��es do material para a integra��o com o ECF
	 *
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/03/2017
	 */
	public Material loadForIntegracaoECF(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("material.cdmaterial, material.nome, unidademedida.simbolo, " +
						"tributacaoecf.cdtributacaoecf, tributacaoecf.codigoecf")
				.leftOuterJoin("material.tributacaoecf tributacaoecf")
				.leftOuterJoin("material.unidademedida unidademedida")
				.where("material = ?", material)
				.unique();
	}

	/**
	 * Altera o peso bruto do material
	 *
	 * @param material
	 * @return
	 * @author Mairon Cezar
	 * @since 12/05/2017
	 */
	public void updatePesoBruto(Material material, Double pesobruto) {
		getJdbcTemplate().update(
				"UPDATE material SET pesobruto = ? WHERE cdmaterial = ?",
				new Object[] { pesobruto, material.getCdmaterial() });
	}
	
	public List<Material> findForRegitrarMovimentacaoAnimal(String whereIn){
		return query()
		.select(
				"material.cdmaterial, material.nome, material.identificacao, material.valorcusto, "+
				"materialgrupo.registrarpesomedio, materialgrupo.animal, contaGerencialEntrada.cdcontagerencial, contaGerencialEntrada.nome, " +
				"contaGerencialPerda.cdcontagerencial, contaGerencialPerda.nome, projetoRateio.cdprojeto,projetoRateio.nome")
		.leftOuterJoin("material.materialgrupo materialgrupo")
		.leftOuterJoin("material.materialRateioEstoque materialRateioEstoque")
		.leftOuterJoin("materialRateioEstoque.contaGerencialEntrada contaGerencialEntrada")	
		.leftOuterJoin("materialRateioEstoque.contaGerencialPerda contaGerencialPerda")
		.leftOuterJoin("materialRateioEstoque.projeto projetoRateio")
		.whereIn("material.cdmaterial", whereIn)
		.list();		
	}
	
	/**
	 * Busca os dados para o estoque atualizado dos materiais para transfer�ncia de estoque.
	 *
	 * @param filtro
	 * @return
	 * @author Mairon Cezar
	 * @since 08/06/2017
	 */
	@SuppressWarnings("unchecked")
	public List<TransferenciaCategoriaMaterialBean> findForTransferenciaCategoria(TransferenciaCategoriaFiltro filtro, String whereInMaterial, String whereInCodigobarras) {
		String regraTransferencia = parametroGeralService.getBoolean(Parametrogeral.TRANSFERENCIA_CATEGORIA)? "((substr(vmcDestino.identificador,0,3) = substr(vmc.identificador,0,3) and vmcDestino.identificador > vmc.identificador))":"((vmc.nivel = vmcDestino.nivel and vmcDestino.identificador > vmc.identificador) or (vmcDestino.nivel = vmc.nivel + 1 and vmcDestino.identificador = vmc.identificador||'.01'))";
		String sql = "select cdmaterial, " +
			"nome_material,  " +
		    "codigobarras, " +
		    "produto_boolean, " +
		    "epi_boolean, " +
		    "cdunidademedida, " +
		    "nome_unidademedida, " + 
		    "sum(coalesce(qtde_entrada, 0)) - sum(coalesce(qtde_saida, 0)) as qtde_sistema, " +
		    "registrarpesomedio, " +
		    "pesobruto, " +
			"cdmaterial_dest,  " +
			"nome_material_dest " +
		    
		"from ( " +
	
		"select  " +
			"m.cdmaterial,  " +
			"m.nome as nome_material, " +
		    "m.codigobarras, " +
		    "m.produto as produto_boolean, " +
		    "m.epi as epi_boolean, " +
		    "um.cdunidademedida, " +
		    "um.nome as nome_unidademedida, " +
		    "sum(me.qtde) as qtde_saida, " +
		    "0 as qtde_entrada, " +
		    "mg.registrarpesomedio, " +
		    "m.pesobruto, " +
			"matDestino.cdmaterial as cdmaterial_dest,  " +
			"matDestino.nome as nome_material_dest " +
	
		"from material m " +
		"join unidademedida um on um.cdunidademedida = m.cdunidademedida " +
		"join vmaterialcategoria vmc on m.cdmaterialcategoria = vmc.cdmaterialcategoria "+
		"left outer join movimentacaoestoque me on me.cdmaterial = m.cdmaterial  " +
		"left outer join materialgrupo mg on mg.cdmaterialgrupo = m.cdmaterialgrupo,  " +
		"material matDestino "+
		"join vmaterialcategoria vmcDestino on matDestino.cdmaterialcategoria = vmcDestino.cdmaterialcategoria " +
	
		"where m.ativo = true " +
		"and"+regraTransferencia+
		"and (m.patrimonio = false or m.patrimonio is null) " +
		"and (m.servico = false or m.servico is null) " +
		"and (m.vendapromocional = false or m.vendapromocional is null) " +
		"and me.cdmovimentacaoestoquetipo = 2  " +
		"and me.dtcancelamento is null  " +
		"and me.dtmovimentacao <= '" + SinedDateUtils.toStringPostgre(filtro.getDataestoque()) + "' " +
		(filtro.getMaterialgrupo() != null ? ("and m.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo() + " ") : "") +
		(filtro.getEmpresa() != null ? ("and me.cdempresa = " + filtro.getEmpresa().getCdpessoa() + " ") : "") +
		(filtro.getLocalarmazenagem() != null ? ("and me.cdlocalarmazenagem = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem() + " ") : "and me.cdlocalarmazenagem is null ") +
		(filtro.getMaterial() != null ? ("and m.cdmaterial = " + filtro.getMaterial().getCdmaterial() + " ") : "") +
		(StringUtils.isNotEmpty(whereInMaterial) ? (" and m.cdmaterial in (" + whereInMaterial + ") ") : "") +
		(StringUtils.isNotEmpty(whereInCodigobarras) ? (" and m.codigobarras in (" + whereInCodigobarras + ") ") : "") +
		(filtro.getEmpresa() != null ? (" and exists (" +
				" select m2.cdmaterial " +
				" from material m2 " +
				" left outer join materialempresa mEmpresa2 on mEmpresa2.cdmaterial = m2.cdmaterial " +
				" where m2.cdmaterial = m.cdmaterial " +
				" and (mEmpresa2.cdpessoa is null or mEmpresa2.cdpessoa = " + filtro.getEmpresa().getCdpessoa() + ")) ") : "") +
		
		"group by m.cdmaterial, m.nome, m.codigobarras, m.produto, m.epi, um.cdunidademedida, um.nome, mg.registrarpesomedio, m.pesobruto, " +
				"matDestino.cdmaterial, matDestino.nome " +
	
		"union all " +
	
		"select  " +
			"m.cdmaterial,  " +
			"m.nome as nome_material, " +
		    "m.codigobarras, " +
		    "m.produto as produto_boolean, " +
		    "m.epi as epi_boolean, " +
		    "um.cdunidademedida, " +
		    "um.nome as nome_unidademedida, " +
		    "0 as qtde_saida, " +
		    "sum(me.qtde) as qtde_entrada, " +
		    "mg.registrarpesomedio, " +
		    "m.pesobruto, " +
			"matDestino.cdmaterial as cdmaterial_dest,  " +
			"matDestino.nome as nome_material_dest " +
		    
		"from material m " +
		"join unidademedida um on um.cdunidademedida = m.cdunidademedida " +
		"join vmaterialcategoria vmc on m.cdmaterialcategoria = vmc.cdmaterialcategoria "+
		"left outer join movimentacaoestoque me on me.cdmaterial = m.cdmaterial  " +
		"left outer join materialgrupo mg on mg.cdmaterialgrupo = m.cdmaterialgrupo,  " +
		"material matDestino "+
		"join vmaterialcategoria vmcDestino on matDestino.cdmaterialcategoria = vmcDestino.cdmaterialcategoria " +
	
		"where m.ativo = true " +
		"and"+regraTransferencia+
		"and (m.patrimonio = false or m.patrimonio is null) " +
		"and (m.servico = false or m.servico is null) " +
		"and (m.vendapromocional = false or m.vendapromocional is null) " +
		"and me.cdmovimentacaoestoquetipo = 1  " +
		"and me.dtcancelamento is null  " +
		"and me.dtmovimentacao <= '" + SinedDateUtils.toStringPostgre(filtro.getDataestoque()) + "' " +
		(filtro.getMaterialgrupo() != null ? ("and m.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo() + " ") : "") +
		(filtro.getEmpresa() != null ? ("and me.cdempresa = " + filtro.getEmpresa().getCdpessoa() + " ") : "") +
		(filtro.getLocalarmazenagem() != null ? ("and me.cdlocalarmazenagem = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem() + " ") : "and me.cdlocalarmazenagem is null ") +
		(filtro.getMaterial() != null ? ("and m.cdmaterial = " + filtro.getMaterial().getCdmaterial() + " ") : "") +
		(StringUtils.isNotEmpty(whereInMaterial) ? (" and m.cdmaterial in (" + whereInMaterial + ") ") : "") +
		(StringUtils.isNotEmpty(whereInCodigobarras) ? (" and m.codigobarras in (" + whereInCodigobarras + ") ") : "") +
		(filtro.getEmpresa() != null ? (" and exists (" +
				" select m2.cdmaterial " +
				" from material m2 " +
				" left outer join materialempresa mEmpresa2 on mEmpresa2.cdmaterial = m2.cdmaterial " +
				" where m2.cdmaterial = m.cdmaterial " +
				" and (mEmpresa2.cdpessoa is null or mEmpresa2.cdpessoa = " + filtro.getEmpresa().getCdpessoa() + ")) ") : "") +
		
		"group by m.cdmaterial, m.nome, m.codigobarras, m.produto, m.epi, um.cdunidademedida, um.nome, mg.registrarpesomedio, m.pesobruto, " +
				"matDestino.cdmaterial, matDestino.nome " +
		
		"union all " +
		
		"select " +
			"m.cdmaterial,  " +
			"m.nome as nome_material, " +
		    "m.codigobarras, " +
		    "m.produto as produto_boolean, " +
		    "m.epi as epi_boolean, " +
		    "um.cdunidademedida, " +
		    "um.nome as nome_unidademedida, " +
		    "0 as qtde_saida, " +
		    "0 as qtde_entrada, " +
		    "mg.registrarpesomedio, " +
		    "m.pesobruto, " +
			"matDestino.cdmaterial as cdmaterial_dest,  " +
			"matDestino.nome as nome_material_dest " +
		    
	    "from material m " +
		"join unidademedida um on um.cdunidademedida = m.cdunidademedida " +
		"left outer join materialgrupo mg on mg.cdmaterialgrupo = m.cdmaterialgrupo  " +
		"join vmaterialcategoria vmc on m.cdmaterialcategoria = vmc.cdmaterialcategoria, "+
		"material matDestino "+
		"join vmaterialcategoria vmcDestino on matDestino.cdmaterialcategoria = vmcDestino.cdmaterialcategoria " +
		
		"where m.ativo = true " +
		"and"+regraTransferencia+
		"and (m.patrimonio = false or m.patrimonio is null) " +
		"and (m.servico = false or m.servico is null) " +
		"and (m.vendapromocional = false or m.vendapromocional is null) " +
		(filtro.getMaterialgrupo() != null ? ("and m.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo() + " ") : "") +
		(filtro.getMaterial() != null ? ("and m.cdmaterial = " + filtro.getMaterial().getCdmaterial() + " ") : "") +
		(StringUtils.isNotEmpty(whereInMaterial) ? (" and m.cdmaterial in (" + whereInMaterial + ") ") : "") +
		(StringUtils.isNotEmpty(whereInCodigobarras) ? (" and m.codigobarras in (" + whereInCodigobarras + ") ") : "") +
		(filtro.getEmpresa() != null ? (" and exists (" +
				" select m2.cdmaterial " +
				" from material m2 " +
				" left outer join materialempresa mEmpresa2 on mEmpresa2.cdmaterial = m2.cdmaterial " +
				" where m2.cdmaterial = m.cdmaterial " +
				" and (mEmpresa2.cdpessoa is null or mEmpresa2.cdpessoa = " + filtro.getEmpresa().getCdpessoa() + ")) ") : "") +
				
		") query " +
	
		"group by cdmaterial, nome_material, codigobarras, produto_boolean, epi_boolean, cdunidademedida, nome_unidademedida, registrarpesomedio, pesobruto, " +
				"cdmaterial_dest, nome_material_dest " +
		"order by upper(nome_material) ";
		
		
		List<TransferenciaCategoriaMaterialBean> lista = getJdbcTemplate().query(sql, 
			new RowMapper() {
				public TransferenciaCategoriaMaterialBean mapRow(ResultSet rs, int rowNum) throws SQLException {
					TransferenciaCategoriaMaterialBean bean = new TransferenciaCategoriaMaterialBean();
					
					Material material = new Material();
					material.setCdmaterial(rs.getInt("cdmaterial"));
					material.setNome(rs.getString("nome_material"));
					material.setCodigobarras(rs.getString("codigobarras"));
					material.setProduto(rs.getBoolean("produto_boolean"));
					material.setEpi(rs.getBoolean("epi_boolean"));
					
					Material materialDestino = new Material();
					materialDestino.setCdmaterial(rs.getInt("cdmaterial_dest"));
					materialDestino.setNome(rs.getString("nome_material_dest"));
					materialDestino.setCodigobarras(rs.getString("codigobarras"));
					materialDestino.setProduto(rs.getBoolean("produto_boolean"));
					materialDestino.setEpi(rs.getBoolean("epi_boolean"));
					
					Unidademedida unidademedida = new Unidademedida();
					unidademedida.setCdunidademedida(rs.getInt("cdunidademedida"));
					unidademedida.setNome(rs.getString("nome_unidademedida"));
					
					bean.setMaterialorigem(material);
					bean.setMaterialdestino(materialDestino);
					bean.setQuantidadeatual(rs.getDouble("qtde_sistema"));
					bean.setPesomedio(rs.getDouble("pesobruto"));
					bean.setRegistrarpesomedio(rs.getBoolean("registrarpesomedio"));
					
					return bean;
				}
			}
		);
		
		return lista;
	}
	
	public Material findForReserva(Material material) {
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
				.select(
						"material.cdmaterial, material.vendapromocional, material.servico, " +
						"listaMaterialrelacionado.cdmaterialrelacionado, listaMaterialrelacionado.quantidade, "
						+ "materialpromocao.cdmaterial, materialpromocao.servico, materialpromocao.vendapromocional, "
						+ "listaMaterialrelacionadoformula.formula, listaMaterialrelacionadoformula.cdmaterialrelacionadoformula ")
				.leftOuterJoin("material.listaMaterialrelacionado listaMaterialrelacionado")
				.leftOuterJoin("listaMaterialrelacionado.materialpromocao materialpromocao")
				.leftOuterJoin("listaMaterialrelacionado.listaMaterialrelacionadoformula listaMaterialrelacionadoformula")
				.where("material = ?", material)
				.unique();
	}
	
	public List<Material> findMateriaisWithMaterialGrupoNotAnimal(String whereIn){
		return query()
			.select("material.cdmaterial, material.nome, materialgrupo.animal ")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.whereIn("material.cdmaterial", whereIn)
			.openParentheses()
				.where("materialgrupo is null")
				.or()
				.where("coalesce(materialgrupo.animal, false) = false")
			.closeParentheses()
				
			.list();		
	}
	
	public List<Material> findMateriaisWithMaterialGrupoAnimal(){
		return query()
			.select("material.cdmaterial, material.nome, material.identificacao, materialgrupo.animal ")
			.join("material.materialgrupo materialgrupo")
			.where("materialgrupo.animal = true")
			.list();		
	}
	
	public void updateCategoriaMaterial(Materialcategoria materialcategoriaAntiga, Materialcategoria materialcategoriaNova){
		getJdbcTemplate().update("update Material set cdmaterialcategoria=? where cdmaterialcategoria=?", new Object[]{materialcategoriaNova.getCdmaterialcategoria(), materialcategoriaAntiga.getCdmaterialcategoria()});
	}
	
	public List<Material> findProdutoAtivoAutocomplete(String q,Boolean acompanhaBanda) {
		QueryBuilder<Material> query = query();
		
		if(Boolean.TRUE.equals(acompanhaBanda)){
			query.leftOuterJoin("material.materialgrupo materialgrupo")
				 .where("materialgrupo.acompanhaBanda =?",acompanhaBanda);
		}
		return query
					.select("material.cdmaterial, material.nome, material.identificacao")
					.where("material.ativo=?", Boolean.TRUE)
					.where("material.produto=?", Boolean.TRUE)
					.openParentheses()
						.whereLikeIgnoreAll("material.nome", q)
						.or()
						.whereLikeIgnoreAll("material.identificacao", q)
					.closeParentheses()
					.orderBy("material.identificacaoordenacao, material.nome")
					.autocomplete()
					.list();		
	}
	
	public List<Material> findMaterialEstoqueByCodigobarras(String codigobarras, Localarmazenagem localarmazenagem) {
		return query()
				.select("material.cdmaterial, material.nome, material.identificacao, material.patrimonio, listaMaterialnumeroserie.numero, material.codigobarras")
				.leftOuterJoin("material.vgerenciarmaterial vgerenciarmaterial")
				.leftOuterJoin("material.listaMaterialnumeroserie listaMaterialnumeroserie")
				.where("material.ativo = true")
				.where("material.codigobarras = ?", codigobarras)
				.where("vgerenciarmaterial.localarmazenagem = ?", localarmazenagem)
				.where("vgerenciarmaterial.qtdedisponivel > 0.0")
				.list();
	}
	
	public List<Material> findMaterialByCodigobarras(String codigobarras, Boolean ativo, Boolean producao) {
		return query()
				.select("material.cdmaterial, material.nome, material.identificacao, material.patrimonio, material.tempoentrega," +
						"materialmestregrade.cdmaterial, materialmestregrade.identificacao, listaMaterialnumeroserie.numero")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.leftOuterJoin("material.listaMaterialnumeroserie listaMaterialnumeroserie")
				.where("material.codigobarras = ?", codigobarras)
				.where("material.ativo = ?", ativo)
				.where("material.producao = ?", producao)
				.orderBy("material.identificacaoordenacao, material.nome")
				.autocomplete()
				.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Material> carregarEstoqueDosMateriais(String materiais, Empresa empresa) {
		if(materiais == null || materiais.trim().isEmpty())
			return new ArrayList<Material>();
		String cdempresa = null;
		if(empresa != null && empresa.getCdpessoa() != null){
			cdempresa = empresa.getCdpessoa().toString();
		}
		materiais = SinedUtil.quebraWhereInSubQuery("cdmaterial", materiais);
		String sql = "";
		if(cdempresa != null){
			sql = "SELECT cdmaterial, cdempresa, sum(qtdedisponivel) as qtdedisponivel FROM vgerenciarmaterial where " + materiais + "and cdempresa = "+cdempresa+" group by cdempresa, cdmaterial";			
		}else {
			sql = "SELECT cdmaterial, sum(qtdedisponivel) as qtdedisponivel FROM vgerenciarmaterial where " + materiais + " group by cdmaterial";	
		}
		SinedUtil.markAsReader();
		List<Material> lst = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Material m = new Material();
				m.setCdmaterial(rs.getInt("cdmaterial"));
				m.setMateriaisDisponiveisEmEstoque(rs.getDouble("qtdedisponivel"));
				return m;
			}
		});
		return lst;
	}

	public Material loadMaterialWithMaterialgrupo(Material material) {
		return query().select("material.cdmaterial, materialgrupo.cdmaterialgrupo, materialgrupo.nome, materialgrupo.coleta")
				.join("material.materialgrupo materialgrupo")
				.where("material = ?", material)
				.unique();
	}

	public Material loadMaterialWithMaterialtipoo(Material material) {
		return query().select("material.cdmaterial, materialtipo.cdmaterialtipo, materialtipo.nome")
				.join(JoinMode.LEFT_OUTER, false, "material.materialtipo materialtipo")
				.where("material = ?", material)
				.unique();
	}

	public List<Material> finMaterialAutoCompleteByMaterialtipoEmpresa(String query, Empresa empresa, Materialtipo tipo) {
		return query().select("material.cdmaterial, material.nome")
				.leftOuterJoin("material.materialtipo materialtipo")
				.leftOuterJoin("material.listaMaterialempresa materialempresa")
				.where("material.servico = ?", Boolean.TRUE)
				.where("materialtipo = ?", tipo)
				.openParentheses()
				.where("materialempresa is null").or()
				.where("materialempresa.empresa = ?", empresa)
				.closeParentheses()
				.whereLikeIgnoreAll("material.nome", query)
				.orderBy("material.nome")
				.list();
	}

	public Material getUniqueMaterial(Material material, Materialtipo materialtipo, Empresa empresa) {
		return query().select("material.cdmaterial")
				.leftOuterJoin("material.materialtipo materialtipo")
				.leftOuterJoin("material.listaMaterialempresa materialempresa")
				.where("material = ?", material)
				.where("materialtipo = ?", materialtipo)
				.openParentheses()
				.where("materialempresa is null").or()
				.where("materialempresa.empresa = ?", empresa)
				.closeParentheses()
				.unique();
	}
	
	/**
	 * M�todo que retorna true caso o material tenha mat�ria prima
	 *
	 * @param material
	 * @return
	 * @author Mairon Cezar
	 * @since 14/05/2018
	 */
	public Boolean existMateriaprima(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo.");
		
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Material.class)
			.join("material.listaProducao listaProducao")
			.where("material = ?", material)
			.unique() > 0;
	}
	
	/**
	* M�todo que carrega o material com a unidade de medida
	*
	* @param material
	* @return
	* @since 18/05/2018
	* @author Luiz Fernando
	*/
	public Material loadForUltimoValorVenda(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo.");
		
		return query()
				.select("material.cdmaterial, material.vendapromocional, material.exibiritenskitvenda, material.kitflexivel, unidademedida.cdunidademedida, unidademedida.simbolo ")
				.leftOuterJoin("material.unidademedida unidademedida")
				.where("material = ?", material)
				.unique();
	}

	public List<Material> findMaterialNotaFiscalServicoAutocomplete(String q, Boolean ativo) {
		return query()
				.select("material.cdmaterial, material.nome, material.identificacao, material.tempoentrega, material.nomenf," +
						"materialmestregrade.cdmaterial, materialmestregrade.identificacao ")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.openParentheses()
				.whereLikeIgnoreAll("material.nome", q)
				.or()
				.whereLikeIgnoreAll("material.identificacao", q)
				.or()
				.whereLikeIgnoreAll("material.nomenf", q)
				.closeParentheses()
				.where("material.ativo = ?", ativo)
				.openParentheses()
					.where("material.servico = TRUE")
					.or()
					.where("material.tributacaomunicipal = TRUE")
				.closeParentheses()
				.orderBy("material.identificacaoordenacao, material.nome")
				.autocomplete()
				.list();
	}
	
	public List<Material> findForRetornoContrato(String whereIn) {
		QueryBuilder<Material> query = query()
				.select("material.cdmaterial, material.identificacao, material.nome, material.valorvenda");
		
		SinedUtil.quebraWhereIn("material.cdmaterial", whereIn, query);
		
		return query.list();
	}
	
	public List<Material> findMaterialcoletaAutocomplete(String q, Boolean ativo) {
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		boolean serie = Boolean.TRUE.equals(NeoWeb.getRequestContext().getSession().getAttribute("serieForAutocompleteMaterial"));
		
		return query()
				.select("material.cdmaterial, material.nome, material.identificacao, material.patrimonio, material.tempoentrega," +
						"materialmestregrade.cdmaterial, materialmestregrade.identificacao, listaMaterialnumeroserie.numero")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.leftOuterJoin("material.listaMaterialnumeroserie listaMaterialnumeroserie")
				.join("material.materialgrupo materialgrupo")
				.openParentheses()
					.whereLikeIgnoreAll("material.nome", q)
					.or()
					.whereLikeIgnoreAll("material.identificacao", q)
					.or()
					.where("UPPER(" + funcaoTiraacento + "(listaMaterialnumeroserie.numero)) LIKE '%' || ? || '%'", Util.strings.tiraAcento(q).toUpperCase(), serie)
				.closeParentheses().orderBy("material.nome")
				.where("material.ativo = ?", ativo)
				.where("materialgrupo.coleta = ?", Boolean.TRUE)
				.orderBy("material.identificacaoordenacao, material.nome")
				.autocomplete()
				.list();
	}
	
	public Long getUltimoIdentificadorCdmaterial(){
		String sql ="select max(q.codigo) as codigo " +
					" from ( " +
					" select m.cdmaterial as codigo " +
					" from material m " +
					" union all " +
					" select cast(m.identificacao as NUMERIC) as codigo " +
					" from material m " +
					" where length(m.identificacao) > 0  " +
					" and m.identificacao ~ '^([0-9]+)$' " +
					" union all " +
					" select cast(nfi.identificadorespecifico as NUMERIC) as codigo " +
					" from notafiscalprodutoitem nfi " +
					" where nfi.identificadorespecifico is not null  " +
					" and length(nfi.identificadorespecifico) > 0 " +
					" and nfi.identificadorespecifico ~ '^([0-9]+)$' " +
					" ) as q ";

		SinedUtil.markAsReader();
		Long resultado = (Long) getJdbcTemplate().queryForObject(sql, 
				new RowMapper() {
					public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
						return new Long(rs.getLong("codigo"));
					}
				});
		
		return resultado;
	}

	public Material loadWithBanda(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("material.cdmaterial, materialacompanhabanda.cdmaterial, unidademedida.cdunidademedida")
				.join("material.materialacompanhabanda materialacompanhabanda")
				.join("materialacompanhabanda.unidademedida unidademedida")
				.where("material = ?", material)
				.unique();
	}

	public List<Material> findAtivosByNome(String q) {
		return query()
				.select("material.cdmaterial, material.identificacao, material.nome")
				.whereLikeIgnoreAll("material.nome", q)
				.where("material.ativo = ?", Boolean.TRUE)
				.autocomplete()
				.list();
	}

	public Material loadForMaterialRateioEstoque(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material n�o pode ser nulo");
		
		return querySined()
				.removeUseWhere()
				.select(  "material.cdmaterial, material.nome, " 
						+ "contaGerencialConsumo.cdcontagerencial,contaGerencialConsumo.nome," 
						+ "contaGerencialRequisicao.cdcontagerencial,contaGerencialRequisicao.nome,"
						+ "contaGerencialEntrada.cdcontagerencial,contaGerencialEntrada.nome,"
						+ "contaGerencialVenda.cdcontagerencial,contaGerencialVenda.nome,"
						+ "contaGerencialPerda.cdcontagerencial,contaGerencialPerda.nome,"
						+ "contaGerencialAjusteEntrada.cdcontagerencial,contaGerencialAjusteEntrada.nome,"
						+ "contaGerencialAjusteSaida.cdcontagerencial,contaGerencialAjusteSaida.nome,"
						+ "contaGerencialRomaneioEntrada.cdcontagerencial,contaGerencialRomaneioEntrada.nome,"
						+ "contaGerencialRomaneioSaida.cdcontagerencial,contaGerencialRomaneioSaida.nome,"
						+ "contaGerencialDevolucaoEntrada.cdcontagerencial,contaGerencialDevolucaoEntrada.nome,"
						+ "contaGerencialDevolucaoSaida.cdcontagerencial,contaGerencialDevolucaoSaida.nome,"
						+ "projetoRateio.cdprojeto,projetoRateio.nome")
				.leftOuterJoin("material.materialRateioEstoque materialRateioEstoque")
				.leftOuterJoin("materialRateioEstoque.contaGerencialConsumo contaGerencialConsumo")	
				.leftOuterJoin("materialRateioEstoque.contaGerencialRequisicao contaGerencialRequisicao")	
				.leftOuterJoin("materialRateioEstoque.contaGerencialEntrada contaGerencialEntrada")
				.leftOuterJoin("materialRateioEstoque.contaGerencialVenda contaGerencialVenda")
				.leftOuterJoin("materialRateioEstoque.contaGerencialPerda contaGerencialPerda")
				.leftOuterJoin("materialRateioEstoque.contaGerencialAjusteEntrada contaGerencialAjusteEntrada")
				.leftOuterJoin("materialRateioEstoque.contaGerencialAjusteSaida contaGerencialAjusteSaida")
				.leftOuterJoin("materialRateioEstoque.contaGerencialRomaneioEntrada contaGerencialRomaneioEntrada")
				.leftOuterJoin("materialRateioEstoque.contaGerencialRomaneioSaida contaGerencialRomaneioSaida")				
				.leftOuterJoin("materialRateioEstoque.contaGerencialDevolucaoEntrada contaGerencialDevolucaoEntrada")
				.leftOuterJoin("materialRateioEstoque.contaGerencialDevolucaoSaida contaGerencialDevolucaoSaida")
				.leftOuterJoin("materialRateioEstoque.projeto projetoRateio")
				.where("material = ?", material)
				.unique();
	}
	
	public Material findForInventario(Material material) {
		return query().select("material.nome,material.cdmaterial,material.identificacao," +
				"material.valorvenda,material.peso,material.pesobruto," +
				"listaFornecedor.codigo,listaFornecedor.cdmaterialfornecedor," +
				"unidademedida.cdunidademedida,unidademedida.simbolo")
		.leftOuterJoin("material.listaFornecedor listaFornecedor")
		.leftOuterJoin("material.unidademedida unidademedida")
		.where("material.cdmaterial = ?", material.getCdmaterial())
		.unique();
	}
	
	public List<Material> findMaterialWithCustoPorEmpresa(String whereIn) {
		return query()
				.select("material.cdmaterial, material.nome, material.valorcusto, " +
						"listaMaterialCustoEmpresa.cdMaterialCustoEmpresa, listaMaterialCustoEmpresa.custoMedio, " + 
						"empresa.cdpessoa, empresa.nomefantasia")
				.leftOuterJoin("material.listaMaterialCustoEmpresa listaMaterialCustoEmpresa")
				.leftOuterJoin("listaMaterialCustoEmpresa.empresa empresa")
				.whereIn("material.cdmaterial", whereIn)
				.list();
	}
	
	public List<Material> buscarMaterialComFaixaMarkup(String whereIn) {
		return query()
				.select("material.cdmaterial, material.nome, " +
						"listaMaterialFaixaMarkup.cdMaterialFaixaMarkup, listaMaterialFaixaMarkup.markup, " +
						"listaMaterialFaixaMarkup.isValorMinimo, listaMaterialFaixaMarkup.isValorIdeal, " +
						"faixa.cdFaixaMarkupNome, faixa.nome")
				.leftOuterJoin("material.listaMaterialFaixaMarkup listaMaterialFaixaMarkup")
				.leftOuterJoin("listaMaterialFaixaMarkup.faixaMarkupNome faixa")
				.whereIn("material.cdmaterial", whereIn)
				.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<MaterialHistoricoPrecoBean> findHistoricoPrecos(MaterialHistoricoPrecoFiltro filtro){
		StringBuilder query = new StringBuilder();
		query.append("select material.cdmaterial, material.nome as nomeMaterial, material.identificacao as identificacaoMaterial, venda.dtvenda, vendamaterial.preco, ");
		query.append("		cliente.cdpessoa, pessoaCliente.nome as nomeCliente, empresa.cdpessoa, venda.cdvenda, pedidovendatipo.descricao as nomeTipoPedido, ");
		query.append("		unidademedida.nome as nomeunidademedida, unidademedida.cdunidademedida, unidademedida.simbolo, ");
		query.append("      pessoaEmpresa.nome as nomeEmpresa, vendamaterial.observacao, vendamaterial.quantidade, vendamaterial.desconto, ");
		query.append("      coalesce(vendamaterial.valorseguro, 0) as valorseguro, coalesce(vendamaterial.outrasdespesas, 0) as outrasdespesas ");
		query.append("from venda ");
		query.append("  join vendamaterial on vendamaterial.cdvenda = venda.cdvenda ");
		query.append("  join material on material.cdmaterial = vendamaterial.cdmaterial ");
		query.append("  left join unidademedida on unidademedida.cdunidademedida = vendamaterial.cdunidademedida ");
		query.append("  left join empresa on empresa.cdpessoa = venda.cdempresa ");
		query.append("  left join pessoa pessoaEmpresa on pessoaEmpresa.cdpessoa = empresa.cdpessoa ");
		query.append("  left join pedidovendatipo on pedidovendatipo.cdpedidovendatipo = venda.cdpedidovendatipo ");
		query.append("  join cliente on cliente.cdpessoa = venda.cdcliente ");
		query.append("  join pessoa pessoaCliente on cliente.cdpessoa = pessoaCliente.cdpessoa ");
		query.append(" where coalesce(material.vendapromocional, false) = false");
		query.append("   and coalesce(material.kitflexivel, false) = false");
		query.append("   and coalesce(material.producao, false) = false");
		query.append("   and material.cdmaterialmestregrade is null");
		query.append("   and not exists(select 1 from material itemGrade where itemGrade.cdmaterialmestregrade = itemGrade.cdmaterial)");
		if(Util.objects.isPersistent(filtro.getMaterial())){
			query.append(" and material.cdmaterial = ").append(filtro.getMaterial().getCdmaterial());
		}
		query.append("   and cliente.cdpessoa = ").append(filtro.getCliente().getCdpessoa());
		query.append("   and empresa.cdpessoa = ").append(filtro.getEmpresa().getCdpessoa());
		query.append("   and venda.cdvendasituacao <> ").append(Vendasituacao.AGUARDANDO_APROVACAO.getCdvendasituacao());
		query.append("   and venda.cdvendasituacao <> ").append(Vendasituacao.CANCELADA.getCdvendasituacao());
		if(filtro.getDtVendaIni() != null){
			query.append("   and venda.dtvenda >= '").append(SinedDateUtils.toStringPostgre(filtro.getDtVendaIni())).append("'");
		}
		if(filtro.getDtVendaFim() != null){
			query.append("   and venda.dtvenda <= '").append(SinedDateUtils.toStringPostgre(filtro.getDtVendaFim())).append("'");
		}
		if(Util.objects.isPersistent(filtro.getPedidoVendaTipo())){
			query.append("   and venda.cdpedidovendatipo = ").append(filtro.getPedidoVendaTipo().getCdpedidovendatipo());
		}
		if(filtro.getCdVenda() != null){
			query.append("   and venda.cdvenda = ").append(filtro.getCdVenda());
		}
		if(filtro.getAtivo() != null){
			query.append("   and material.ativo = ").append(filtro.getAtivo());
		}
		query.append(" order by venda.dtvenda desc, venda.cdvenda desc, material.nome limit 100 ");

		SinedUtil.markAsReader();
		return (List<MaterialHistoricoPrecoBean>)getJdbcTemplate().query(query.toString(), new RowMapper() {
			
			@Override
			public MaterialHistoricoPrecoBean mapRow(ResultSet rs, int rowNum) throws SQLException {
				MaterialHistoricoPrecoBean bean = new MaterialHistoricoPrecoBean();
				bean.setCdVenda(rs.getInt("cdvenda"));
				bean.setDtVenda(rs.getDate("dtvenda"));
				bean.setDescricaoTipoPedidoVenda(rs.getString("nomeTipoPedido"));
				bean.setObservacao(rs.getString("observacao"));
				bean.setQuantidade(rs.getDouble("quantidade"));
				bean.setObservacao(rs.getString("observacao"));
				bean.setNomeCliente(rs.getString("nomeCliente"));
				bean.setNomeEmpresa(rs.getString("nomeEmpresa"));
				bean.setValorVenda(rs.getDouble("preco"));
				bean.setDesconto(rs.getDouble("desconto")/100);
				bean.setValorSeguro(rs.getDouble("valorseguro")/100);
				bean.setOutrasDespesas(rs.getDouble("outrasdespesas")/100);
				bean.setQuantidade(rs.getDouble("quantidade"));
				bean.setMaterial(new Material(rs.getInt("cdmaterial"), rs.getString("nomeMaterial"), rs.getString("identificacaoMaterial")));
				bean.setUnidadeMedida(new Unidademedida(rs.getInt("cdunidademedida"), rs.getString("nomeunidademedida"), rs.getString("simbolo")));
				
				Venda vendaAux = VendaService.getInstance().load(new Venda(bean.getCdVenda()));
				vendaAux.setListavendamaterial(VendamaterialService.getInstance().findTotalVendaByVenda(vendaAux));

				bean.setTotalVenda(vendaAux.getTotalvenda().getValue().doubleValue());
				
				return bean;
			}
		});
	}

	@SuppressWarnings("unchecked")
	public List<Integer> findForCriarContabilEmpresaMaterial() {
		
		String sql = " select m.cdmaterial " +
					 " from material m  " +
					 " where m.cdcontacontabil is not null and m.criarcontabilempresa = true " +
					 " and (select count(*) from empresa e where e.ativo = true and e.cdpessoa not in (select mce.cdempresa from materialcustoempresa mce where mce.cdmaterial = m.cdmaterial)) > 0 "
					 ;

		SinedUtil.markAsReader();
		List<Integer> listaCdmaterial = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Integer(rs.getInt("cdmaterial"));
			}
		});
	
		return listaCdmaterial;
	}

	public Material loadForCriarContabilEmpresa(Material material) {
		return query()
				.select("material.cdmaterial, contaContabil.cdcontacontabil")
				.join("material.contaContabil contaContabil")
				.where("material = ?", material)
				.unique();
	}

	public boolean existMaterialPadraoParaMestreGrade(Material materialmestregrade, Material material) {
		if(materialmestregrade == null || materialmestregrade.getCdmaterial() == null)
			throw new SinedException("Material mestre de grade n�o pode ser nulo.");
		
		return new QueryBuilder<Long>(getHibernateTemplate())
			.select("count(*)")
			.setUseTranslator(false)
			.from(Material.class)
			.join("material.materialmestregrade materialmestregrade")
			.where("materialmestregrade.cdmaterial = ?", materialmestregrade.getCdmaterial())
			.where("material.itemGradePadraoMaterialMestre is true")
			.where("material.cdmaterial <> ?",  material != null ? material.getCdmaterial() : null)
			.unique() > 0;
	}

	public Material findItemGradePadrao(Material material) {
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Material mestre de grade n�o pode ser nulo.");
		
		return query()
				.select("material.cdmaterial, material.identificacao, material.nome, unidademedida.cdunidademedida, unidademedida.simbolo")
				.leftOuterJoin("material.unidademedida unidademedida")
				.join("material.materialmestregrade materialmestregrade")
				.where("materialmestregrade.cdmaterial = ?", material.getCdmaterial())
				.where("material.itemGradePadraoMaterialMestre is true")
		.unique();
	}
	
	public Material loadMaterialWithUnidadeMedida(Material material){
		return querySined()
				.select("material.cdmaterial, unidademedida.cdunidademedida, material.nome")
				.join("material.unidademedida unidademedida")
				.where("material = ?", material)
				.setMaxResults(1)
				.unique();
	}

	public Boolean isLoteObrigatorio(Material material) {
		if(material == null)
			throw new SinedException("Material n�o pode ser nulo");
		
		return new QueryBuilder<Long>(getHibernateTemplate())
				.select("count(*)")
				.setUseTranslator(false)
				.from(Material.class)
				.where("material = ?", material)
				.where("material.obrigarlote is true")
				.unique() > 0;
	}
	
	public List<Material> findMateriaisFabricante(Fornecedor bean) {
		return querySined()
				.setUseReadOnly(true)
				.select("material.cdmaterial, material.nome")
				.join("material.listaFornecedor listaFornecedor")
				.join("listaFornecedor.fornecedor fornecedor")
				.where("fornecedor = ?", bean)
				.where("coalesce(fornecedor.fabricante, false) is true")
				.list();
	}
	
	public List<Material> findMateriaisFornecedor(Fornecedor bean) {
		return querySined()
				.setUseReadOnly(true)
				.select("material.cdmaterial, material.nome")
				.join("material.listaFornecedor listaFornecedor")
				.join("listaFornecedor.fornecedor fornecedor")
				.where("fornecedor = ?", bean)
				.where("coalesce(fornecedor.fabricante, false) is false")
				.list();
	}

	public boolean existsMaterialWithModeloDisponibilidade(String whereIn) {
		if(whereIn == "" || whereIn == null)
			throw new SinedException("Par�metro inv�lido.");
		
		SinedUtil.markAsReader();
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.join("material.modeloDisponibilidade modeloDisponibilidade")
				.whereIn("modeloDisponibilidade.cdmodelodisponibilidade", whereIn)
				.unique()>0;
	}

	
	public Material loadByIdEcommerce(Integer idEcommerce){
		return querySined()
				.select("material.cdmaterial, material.nome, unidademedida.cdunidademedida")
				.join("material.unidademedida unidademedida")
				.where("material.idEcommerce = ?", idEcommerce)
				.setMaxResults(1)
				.unique();
	}

	public List<Material> loadWithIdEcommerce(String whereIn) {
		if(whereIn == "" || whereIn == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				.setUseReadOnly(true)
				.select("material.cdmaterial, material.idEcommerce")
				.whereIn("material.cdmaterial", whereIn)
				.list();
	}

	public void insertMaterialTabelaSincronizacaoEcommerce(Material material) {
		getJdbcTemplate().execute("select sincronizacaotabelaecommerce_insert_update(5,false," + material.getCdmaterial() + ");");
	}
	
	private String getQueryFindDuplicadosBiCodigoBarras(String codigoBarras, String whereInMaterial){
		StringBuilder query = new StringBuilder();
		if(parametroGeralService.getBoolean(Parametrogeral.CONSIDERAR_CODIGO_NA_CONFERENCIA_EXPEDICAO)){
			query.append("select material.cdmaterial, material.nome, unidademedida.cdunidademedida, unidademedida.nome as nomeUnidade, unidademedida.simbolo, null as cdembalagem ");
			query.append("  from material ");
			query.append("  join unidademedida on unidademedida.cdunidademedida = material.cdunidademedida");
			query.append(" where coalesce(material.ativo, false) = true");
			query.append("   and codigobarras = ").append("'").append(codigoBarras).append("'");
			query.append("   and material.cdmaterial in(").append(whereInMaterial).append(")");
			query.append("   and coalesce(material.desconsiderarcodigobarras, false) <> true");
			query.append(" union all ");
		}
		query.append("select material.cdmaterial, material.nome, unidademedida.cdunidademedida, unidademedida.nome as nomeUnidade, unidademedida.simbolo, embalagem.cdembalagem ");
		query.append("  from material ");
		query.append("  join embalagem on embalagem.cdmaterial = material.cdmaterial ");
		query.append("  join unidademedida on unidademedida.cdunidademedida = embalagem.cdunidademedida");
		query.append(" where coalesce(material.ativo, false) = true");
		query.append("   and material.cdmaterial in(").append(whereInMaterial).append(")");
		query.append("   and embalagem.codigobarras = ").append("'").append(codigoBarras).append("'");;
		return query.toString();
	}
	
	public List<ConferenciaMateriaisVendaBean> findDuplicadosByCodigoBarras(final String codigoBarras, final String whereInMaterial){
		String query = getQueryFindDuplicadosBiCodigoBarras(codigoBarras, whereInMaterial);

		List<ConferenciaMateriaisVendaBean> lista = null;
		SinedUtil.markAsReader();
		try{
			lista = (List<ConferenciaMateriaisVendaBean>)getJdbcTemplate().query(query, new RowMapper() {
				@Override
				public ConferenciaMateriaisVendaBean mapRow(ResultSet rs, int rowNum) throws SQLException {
					ConferenciaMateriaisVendaBean bean = new ConferenciaMateriaisVendaBean(codigoBarras, rs.getInt("cdmaterial"),
							rs.getString("nome"),
							rs.getInt("cdunidademedida"),
							rs.getString("nomeUnidade"),
							rs.getString("simbolo"),
							rs.getInt("cdembalagem"));
					return bean;
				}
			});			
		}finally{
			SinedUtil.desmarkAsReader();
		}

		return lista;
	}
	
	public Boolean existeAtributo (AtributosMaterial atributoMaterial, Material material , String valor){
		  if(Util.objects.isNotPersistent(atributoMaterial) || Util.objects.isNotPersistent(material)){
				return false;
			}
		
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Material.class)
				.join("material.listaMaterialAtributosMaterial listaMaterialAtributosMaterial")
				.where("material = ?", material)
				.where("listaMaterialAtributosMaterial.atributosMaterial = ?",atributoMaterial)
				.where("listaMaterialAtributosMaterial.valor = ?", valor)
				.unique() > 0;
	}
	
	
	public Material buscaAtributosMaterial (Material bean){
		
		return query()
				.select("atributosMaterial.cdatributosmaterial,atributosMaterial.nome, listaMaterialAtributosMaterial.cdmaterialatributosmaterial, listaMaterialAtributosMaterial.valor")
				.join("material.listaMaterialAtributosMaterial listaMaterialAtributosMaterial")
				.join("listaMaterialAtributosMaterial.atributosMaterial atributosMaterial")
				.where("material = ?", bean)
				.unique();
	}
	
	public void updateToForceTriggers(Material materialMestre){
		getJdbcTemplate().execute("update Material set ativo = ativo where cdmaterialmestregrade = " + materialMestre.getCdmaterial());
	}
	
	public Integer generateNextSequence(String sequenceName){
		return getJdbcTemplate()
				.queryForInt("select nextval('"+sequenceName+"')");
	}
	
	public void updateIdPaiExternoItensDeGrade(Material materialMestre){
		getJdbcTemplate().execute("update Material set IdPaiExterno = "+materialMestre.getIdPaiExterno()+" where cdmaterialmestregrade = " + materialMestre.getCdmaterial());
	}
	
	public boolean verificaRestricaoFornecedorColaborador(Material material, Fornecedor fornecedor){
    	QueryBuilder<Material> query = querySined()
							    		.where("material = ?", material)
										.leftOuterJoin("material.listaFornecedor listaFornecedor")
										.leftOuterJoin("listaFornecedor.fornecedor fornecedor")
										.leftOuterJoin("fornecedor.listaColaboradorFornecedor listaColaboradorFornecedor")
										.leftOuterJoin("listaColaboradorFornecedor.colaborador colaborador")
										.openParentheses()
										.where("listaFornecedor is null")
											.or()
										.where("listaColaboradorFornecedor is null")
											.or()
											.openParentheses()	
											.where("colaborador = ?", SinedUtil.getUsuarioComoColaborador())
											.where("fornecedor = ?", fornecedor)
											.closeParentheses()
											
										.closeParentheses();
    
    	return SinedUtil.isListNotEmpty(query.list());
    }
	
	public List<Material> findAutocompleteProcessoReferenciado(String nome, Integer nota , Integer entrada){
		if(nota == null && entrada == null)
			throw new SinedException("Codigo da nota ou entrada n�o pode ser nulo");
		QueryBuilder<Material> query = query()
				.select("material.cdmaterial, material.nome, material.identificacao");
				query.openParentheses()
				.whereLikeIgnoreAll("material.nome", nome)
				.or()
				.whereLikeIgnoreAll("material.identificacao", nome)
				.closeParentheses();
				if(nota !=null){
					query
					.where("material.cdmaterial in (select m.cdmaterial from Notafiscalprodutoitem nfpi join nfpi.material m join nfpi.notafiscalproduto nfp where nfp.cdNota = ?)",nota);
				}
				if(entrada != null){
					query
					.where("material.cdmaterial in (select m.cdmaterial from Entregamaterial em join em.material m join em.entregadocumento ed where ed.cdentregadocumento = ?)",entrada);
				}
				
		return	query.list();
	}
}