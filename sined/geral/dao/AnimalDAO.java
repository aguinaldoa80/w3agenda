package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Animal;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AnimalDAO extends GenericDAO<Animal> {
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("pesos");
		save.saveOrUpdateManaged("vacinas");
		save.saveOrUpdateManaged("patologias");
	}

	/**
	 * Busca Lista de Animais pelo Cliente, com exce��o de animais com Obito registrado
	 * 
	 * @param Cliente
	 * @author Marcos Lisboa
	 */
	public List<Animal> findExcetoObitoByCliente(Cliente cliente) {
		if (cliente != null && cliente.getCdpessoa() != null) {
			return query()
			.select("animal.nome, animal.cdanimal, cliente.cdpessoa")
			.join("animal.animalSituacao animalsituacao")
			.join("animal.cliente cliente")
			.where("cliente = ?", cliente)
			.where("animalsituacao.obito <> ?", Boolean.TRUE)
			.orderBy("animal.nome")
			.list();
		}else{
			return new ArrayList<Animal>();
		}
	}
}

