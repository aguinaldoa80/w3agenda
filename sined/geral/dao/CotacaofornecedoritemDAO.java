package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedor;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritem;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CotacaofornecedoritemDAO extends GenericDAO<Cotacaofornecedoritem> {

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Cotacaofornecedoritem cotacaofornecedoritem = (Cotacaofornecedoritem)save.getEntity();
		if(cotacaofornecedoritem.getListaCotacaofornecedoritemsolicitacaocompra() != null && 
				cotacaofornecedoritem.getListaCotacaofornecedoritemsolicitacaocompra().size() > 0){
			save.saveOrUpdateManaged("listaCotacaofornecedoritemsolicitacaocompra");
		}
	}
	
	/**
	 * Carrega a lista de cotacaofornecedoritem a partir de uma cotacaofornecedor.
	 * 
	 * @param cotacaofornecedor
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cotacaofornecedoritem> findByCotacaofornecedor(Cotacaofornecedor cotacaofornecedor){
		if (cotacaofornecedor == null || cotacaofornecedor.getCdcotacaofornecedor() == null) {
			throw new SinedException("Cotacaofornecedor n�o pode ser nulo.");
		}
		return query()
					.select("cotacaofornecedoritem.cdcotacaofornecedoritem, cotacaofornecedoritem.qtdesol, cotacaofornecedoritem.qtdecot, cotacaofornecedoritem.dtentrega, " +
							"cotacaofornecedoritem.valor, cotacaofornecedoritem.frete, cotacaofornecedoritem.qtdefrequencia, cotacaofornecedoritem.icmsissincluso, " +
							"cotacaofornecedoritem.icmsiss, cotacaofornecedoritem.ipiincluso, cotacaofornecedoritem.ipi, cotacaofornecedoritem.observacao, material.cdmaterial, " +
							"material.nome, material.identificacao, material.servico, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, unidademedida.cdunidademedida, " +
							"unidademedida.simbolo, localarmazenagemsolicitacao.cdlocalarmazenagem, frequencia.cdfrequencia, materialclasse.cdmaterialclasse, "+
							"cotacaofornecedoritem.observacao, cotacaofornecedoritem.qtdesol")
					.join("cotacaofornecedoritem.material material")
					.join("material.unidademedida unidademedida")
					.join("cotacaofornecedoritem.localarmazenagem localarmazenagem")
					.leftOuterJoin("cotacaofornecedoritem.localarmazenagemsolicitacao localarmazenagemsolicitacao")
					.leftOuterJoin("cotacaofornecedoritem.frequencia frequencia")
					.leftOuterJoin("cotacaofornecedoritem.materialclasse materialclasse")
					.where("cotacaofornecedoritem.cotacaofornecedor = ?",cotacaofornecedor)
					.orderBy("localarmazenagem.nome, material.nome")
					.list();					
	}

	public List<Cotacaofornecedoritem> findByFornecedorMaterial(Material material, Fornecedor fornecedor) {
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		if (fornecedor == null || fornecedor.getCdpessoa() == null) {
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		}
		return query()
					.select("cotacaofornecedoritem.qtdecot, cotacaofornecedoritem.valor, cotacaofornecedoritem.qtdefrequencia")
					.setMaxResults(1)
					.join("cotacaofornecedoritem.cotacaofornecedor cotacaofornecedor")
					.where("cotacaofornecedoritem.material = ?", material)
					.where("cotacaofornecedor.fornecedor = ?", fornecedor)
					.where("cotacaofornecedoritem.qtdecot is not null")
					.where("cotacaofornecedoritem.valor is not null")
					.where("cotacaofornecedoritem.qtdefrequencia is not null")
					.orderBy("cotacaofornecedoritem.cdcotacaofornecedoritem desc")
					.list();
	}

	/**
	 * Deleta os registro de cotacaofornecedoritem que n�o est�o no whereIn.
	 *
	 * @param bean
	 * @param whereIn
	 * @author Rodrigo Freitas
	 * @since 22/05/2015
	 */
	public void deleteNaoExistentes(Cotacaofornecedor bean, String whereIn) {
		if (bean == null || bean.getCdcotacaofornecedor() == null) {
			throw new SinedException("Cota��ofornecedor n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("delete Cotacaofornecedoritem c where c.id not in ("+whereIn+") and c.cotacaofornecedor.id = ?", new Object[]{bean.getCdcotacaofornecedor()});
	}

}
