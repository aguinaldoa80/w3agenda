package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmhistorico;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContacrmhistoricoDAO extends GenericDAO<Contacrmhistorico>{

	private EmpresaService empresaService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Contacrmhistorico> query) {
		query()
			.select("contacrmhistorico.cdusuarioaltera, contacrmhistorico.dtaltera, contacrmhistorico.cdcontacrmhistorico, " +
					"contacrmhistorico.observacao, contacrm.cdcontacrm, contacrmcontato.cdcontacrmcontato, " +
					"meiocontato.cdmeiocontato, atividadetipo.cdatividadetipo")
			.leftOuterJoin("contacrmhistorico.oportunidadesituacao oportunidadesituacao")
			.leftOuterJoin("contacrmhistorico.contacrm contacrm")
			.leftOuterJoin("contacrmhistorico.contacrmcontato contacrmcontato")
			.leftOuterJoin("contacrmhistorico.meiocontato meiocontato")
			.leftOuterJoin("contacrmhistorico.atividadetipo atividadetipo")
			;
	}
	
	public List<Contacrmhistorico> findByContacrm(Contacrm contacrm, Atividadetipo atividadetipo, Empresa empresa) {
		if(contacrm == null || contacrm.getCdcontacrm() == null){
			throw new SinedException("A conta n�o pode ser nula.");
		}
		
		String whereInEmpresas = empresa != null? empresa.getCdpessoa().toString():
			CollectionsUtil.listAndConcatenate(empresaService.findByUsuario(), "cdpessoa", ",");
		
		return query()
					.select("contacrmhistorico.cdcontacrmhistorico, contacrmhistorico.observacao, contacrmhistorico.cdusuarioaltera, " +
							"contacrmhistorico.dtaltera, contacrmcontato.cdcontacrmcontato, contacrmcontato.nome, meiocontato.cdmeiocontato, " +
							"meiocontato.nome, atividadetipo.cdatividadetipo, atividadetipo.nome, empresa.nome")
					.leftOuterJoin("contacrmhistorico.contacrmcontato contacrmcontato")
					.leftOuterJoin("contacrmhistorico.atividadetipo atividadetipo")
					.leftOuterJoin("contacrmhistorico.meiocontato meiocontato")
					.leftOuterJoin("contacrmhistorico.empresahistorico empresa")
					.where("contacrmhistorico.contacrm = ?", contacrm)
					.where("atividadetipo=?", atividadetipo)
					.openParentheses()
						.where("empresa.cdpessoa is null")
						.or()
						.whereIn("empresa.cdpessoa", whereInEmpresas)
					.closeParentheses()
					.list();
	}
	
	public boolean haveHistorico(Contacrm contacrm, Timestamp dtenvio, String obs) {
		if(contacrm == null || contacrm.getCdcontacrm() == null){
			throw new SinedException("A conta n�o pode ser nula.");
		}
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(Contacrmhistorico.class)
					.join("contacrmhistorico.contacrm contacrm")
					.where("contacrm = ?", contacrm)
					.where("contacrmhistorico.dtaltera = ?", dtenvio)
					.where("contacrmhistorico.observacao like ?", obs)
					.unique() > 0;
	}
	
	/**
	* M�todo que carrega a lista de hist�rico da contacrm ordenada por data
	*
	* @param contacrm
	* @return
	* @since Aug 3, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Contacrmhistorico> carregaListaContacrmhistorico(Contacrm contacrm) {
		if(contacrm == null || contacrm.getCdcontacrm() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
					.leftOuterJoin("contacrmhistorico.contacrm contacrm")
					.where("contacrm = ?", contacrm)
					.orderBy("contacrmhistorico.dtaltera")
					.list();
	}

}
