package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Documentoprocesso;
import br.com.linkcom.sined.geral.bean.Documentoprocessoprojeto;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentoprocessoprojetoDAO extends GenericDAO<Documentoprocessoprojeto>{

	/**
	 * M�todo que retorna lista de documentoprocessoprojeto de um determinado documentoprocesso.
	 * @param documentoprocesso
	 * @return
	 */
	public List<Documentoprocessoprojeto> findByDocumentoprocesso(Documentoprocesso documentoprocesso){
		if(documentoprocesso == null){
			throw new SinedException("Valor inv�lido para o par�metro.");
		}
		
		return query()
					.select("documentoprocessoprojeto.cddocumentoprocessoprojeto, documentoprocesso.cddocumentoprocesso")
					.from(Documentoprocessoprojeto.class)
					.join("documentoprocessoprojeto.documentoprocesso documentoprocesso")
					.where("documentoprocesso = ?", documentoprocesso)
					.list();
	}
}
