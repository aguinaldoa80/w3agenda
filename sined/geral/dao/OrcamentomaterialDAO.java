package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Orcamentomaterial;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.OrcamentomaterialFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OrcamentomaterialDAO extends GenericDAO<Orcamentomaterial> {

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaOrcamentomaterialitem");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Orcamentomaterial> query) {
		query
			.select("orcamentomaterial.cdorcamentomaterial, orcamentomaterial.descricao, orcamentomaterial.icmsdestino, " +
					"orcamentomaterial.fatorfaturamentocompra, orcamentomaterial.fatorfaturamentocliente, orcamentomaterial.totalcompra, " +
					"orcamentomaterial.totalvenda, listaOrcamentomaterialitem.cdorcamentomaterialitem, listaOrcamentomaterialitem.cdmaterial, " +
					"listaOrcamentomaterialitem.descricao, listaOrcamentomaterialitem.qtde, listaOrcamentomaterialitem.ipi, listaOrcamentomaterialitem.ipiincluso, " +
					"listaOrcamentomaterialitem.icms, listaOrcamentomaterialitem.valorunitariocompra, listaOrcamentomaterialitem.faturamentodireto, " +
					"listaOrcamentomaterialitem.valorunitariovenda, listaOrcamentomaterialitem.totalcompra, listaOrcamentomaterialitem.totalvenda, " +
					"unidademedida.cdunidademedida, unidademedida.nome, materialclasse.cdmaterialclasse")
			.leftOuterJoin("orcamentomaterial.listaOrcamentomaterialitem listaOrcamentomaterialitem")
			.leftOuterJoin("listaOrcamentomaterialitem.unidademedida unidademedida")
			.leftOuterJoin("listaOrcamentomaterialitem.materialclasse materialclasse");	
		
	}
	
	/**
	 * Carrega a lista de material de um or�amento.
	 *
	 * @param orcamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Orcamentomaterial> findByOrcamentoFlex(OrcamentomaterialFiltro filtro, Orcamento orcamento) {
		if (orcamento == null || orcamento.getCdorcamento() == null) {
			throw new SinedException("Or�amento n�o pode ser nulo.");
		}
		return query()
					.select("orcamentomaterial.cdorcamentomaterial, orcamentomaterial.descricao, " +
							"orcamentomaterial.icmsdestino, orcamentomaterial.fatorfaturamentocompra, " +
							"orcamentomaterial.fatorfaturamentocliente, orcamentomaterialsituacao.cdorcamentomaterialsituacao, " +
							"orcamentomaterialsituacao.nome, orcamentomaterial.dtbaixa, materialclasse.cdmaterialclasse, " +
							"listaOrcamentomaterialitem.cdmaterial")
					.join("orcamentomaterial.aux_orcamentomaterial aux_orcamentomaterial")
					.join("aux_orcamentomaterial.orcamentomaterialsituacao orcamentomaterialsituacao")
					.leftOuterJoin("orcamentomaterial.listaOrcamentomaterialitem listaOrcamentomaterialitem")
					.leftOuterJoin("listaOrcamentomaterialitem.materialclasse materialclasse")
					.where("orcamentomaterial.orcamento = ?",orcamento)
					.whereLikeIgnoreAll("orcamentomaterial.descricao", filtro.getDescricao())
					.orderBy("orcamentomaterial.descricao")
					.list();
	}
	
	/**
	 * Chama a procedure de atualiza��o da tabela auxiliar de or�amento material
	 *
	 * @param requisicaomaterial
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaOrcamentomaterial(Orcamentomaterial orcamentomaterial){
		if (orcamentomaterial == null || orcamentomaterial.getCdorcamentomaterial() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		getJdbcTemplate().execute("SELECT ATUALIZA_ORCAMENTOMATERIAL(" + orcamentomaterial.getCdorcamentomaterial() + ")");
	}

	/**
	 * Faz o cancelamento de uma lista de material.
	 *
	 * @param orcamentomaterial
	 * @author Rodrigo Freitas
	 */
	public void doCancelarFlex(Orcamentomaterial orcamentomaterial) {
		if (orcamentomaterial == null || orcamentomaterial.getCdorcamentomaterial() == null) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		
		getHibernateTemplate().bulkUpdate("update Orcamentomaterial o set dtcancelamento = ? where o.id = ?", 
				new Object[]{SinedDateUtils.currentDate(), orcamentomaterial.getCdorcamentomaterial()});
	}

	/**
	 * Efetua a baixa manual da lista de material.
	 *
	 * @param orcamentomaterial
	 * @author Rodrigo Freitas
	 */
	public void doBaixarFlex(Orcamentomaterial orcamentomaterial) {
		if (orcamentomaterial == null || orcamentomaterial.getCdorcamentomaterial() == null) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		
		getHibernateTemplate().bulkUpdate("update Orcamentomaterial o set dtbaixa = ? where o.id = ?", 
				new Object[]{SinedDateUtils.currentDate(), orcamentomaterial.getCdorcamentomaterial()});
		
	}

	/**
	 * Efetua o estorno da lista de material 
	 *
	 * @param orcamentomaterial
	 * @author Rodrigo Freitas
	 */
	public void doEstornarFlex(Orcamentomaterial orcamentomaterial) {
		if (orcamentomaterial == null || orcamentomaterial.getCdorcamentomaterial() == null) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		
		getHibernateTemplate().bulkUpdate("update Orcamentomaterial o set dtbaixa = null, dtcancelamento = null where o.id = ?", 
				new Object[]{orcamentomaterial.getCdorcamentomaterial()});
	}

	/**
	 * Apaga as solicita��es de compra da lista de material.
	 *
	 * @param orcamentomaterial
	 * @author Rodrigo Freitas
	 */
	public void doEstornarProcessoCompraFlex(Orcamentomaterial orcamentomaterial) {
		if (orcamentomaterial == null || orcamentomaterial.getCdorcamentomaterial() == null) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		
		//DELETA AS SOLICITAC�ES DE COMPRA
		String sql = "DELETE FROM SOLICITACAOCOMPRA S " +
						"WHERE S.CDSOLICITACAOCOMPRA IN ( " +
						     "SELECT SS.CDSOLICITACAOCOMPRA " +
						     "FROM SOLICITACAOCOMPRA SS " +
						     "JOIN SOLICITACAOCOMPRAORIGEM SO ON SO.CDSOLICITACAOCOMPRAORIGEM = SS.CDSOLICITACAOCOMPRAORIGEM " +
						     "WHERE SO.CDORCAMENTOMATERIAL = " + orcamentomaterial.getCdorcamentomaterial() + " " +
						")";
		getJdbcTemplate().execute(sql);
		
		//DELETA AS ORIGENS DA SOLICITA��ES DE COMPRA
		sql = "DELETE FROM SOLICITACAOCOMPRAORIGEM SO WHERE SO.CDORCAMENTOMATERIAL = " + orcamentomaterial.getCdorcamentomaterial();
		getJdbcTemplate().execute(sql);
		
	}

	/**
	 * Carrega a lista de <code>Orcamentomaterial</code> para a exibi��o em um arquivo xls.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Orcamentomaterial> findForExcel(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("orcamentomaterial.descricao, listaOrcamentomaterialitem.descricao, unidademedida.simbolo, listaOrcamentomaterialitem.qtde, listaOrcamentomaterialitem.ipi, " +
							"listaOrcamentomaterialitem.ipiincluso, listaOrcamentomaterialitem.icms, listaOrcamentomaterialitem.valorunitariocompra, " +
							"listaOrcamentomaterialitem.faturamentodireto, listaOrcamentomaterialitem.valorunitariovenda, listaOrcamentomaterialitem.totalcompra, " +
							"listaOrcamentomaterialitem.totalvenda, orcamento.nome")
					.join("orcamentomaterial.orcamento orcamento")
					.join("orcamentomaterial.listaOrcamentomaterialitem listaOrcamentomaterialitem")
					.join("listaOrcamentomaterialitem.unidademedida unidademedida")
					.whereIn("orcamentomaterial.cdorcamentomaterial", whereIn)
					.orderBy("orcamentomaterial.descricao")
					.list();
	}

	/**
	 * Carrega a lista para ser feita a c�pia da(s) lista(s) de materiais.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Orcamentomaterial> findForCopiaOrcamento(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("orcamentomaterial.cdorcamentomaterial, orcamentomaterial.descricao, orcamentomaterial.icmsdestino, " +
					"orcamentomaterial.fatorfaturamentocompra, orcamentomaterial.fatorfaturamentocliente, orcamentomaterial.totalcompra, " +
					"orcamentomaterial.totalvenda, listaOrcamentomaterialitem.cdorcamentomaterialitem, listaOrcamentomaterialitem.cdmaterial, " +
					"listaOrcamentomaterialitem.descricao, listaOrcamentomaterialitem.qtde, listaOrcamentomaterialitem.ipi, listaOrcamentomaterialitem.ipiincluso, " +
					"listaOrcamentomaterialitem.icms, listaOrcamentomaterialitem.valorunitariocompra, listaOrcamentomaterialitem.faturamentodireto, " +
					"listaOrcamentomaterialitem.valorunitariovenda, listaOrcamentomaterialitem.totalcompra, listaOrcamentomaterialitem.totalvenda, " +
					"unidademedida.cdunidademedida, unidademedida.nome, materialclasse.cdmaterialclasse")
					.leftOuterJoin("orcamentomaterial.listaOrcamentomaterialitem listaOrcamentomaterialitem")
					.leftOuterJoin("listaOrcamentomaterialitem.unidademedida unidademedida")
					.leftOuterJoin("listaOrcamentomaterialitem.materialclasse materialclasse")
					.whereIn("orcamentomaterial.cdorcamentomaterial", whereIn)
					.list();
	}

}
