package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Contatotipo;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("upper(retira_acento(contato.nome))")
public class ContatoDAO extends GenericDAO<Contato>{

	/**
	 * Carrega a lista de Contatos de uma Pessoa.
	 * 
	 * @param form
	 * @return
	 * @author Rodrigo Freitas
	 * @author Fl�vio Tavares
	 * @author Tom�s Rabelo
	 */
	public List<Contato> findByPessoa(Pessoa form) {
		if (form == null || form.getCdpessoa() == null) {
			throw new SinedException("Pessoa n�o pode ser nulo.");
		}
		return querySined()
				.select("contato.cdpessoa, contato.nome, contato.emailcontato, contato.observacao, contato.dtnascimento, contato.receberboleto, contato.identidade, " +
						"munContato.cdmunicipio, munContato.nome, ufContato.cduf, ufContato.sigla," +
						"telefone.cdtelefone, telefone.telefone, telefonetipo.cdtelefonetipo," +
						"endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.bairro, endereco.caixapostal," +
						"endereco.cep,endereco.complemento, munEndereco.cdmunicipio, munEndereco.nome, ufEndereco.cduf, " +
						"ufEndereco.sigla, enderecotipo.cdenderecotipo, contatotipo.cdcontatotipo, endereco.pontoreferencia," +
						"contatotipo.nome, contato.responsavelos, contato.participacao, contato.quotas, contato.cpf, " +
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, contato.ativo, contato.recebernf ")
						
					.leftOuterJoin("contato.municipio munContato")
					.leftOuterJoin("munContato.uf ufContato")
					
					.leftOuterJoin("contato.contatotipo contatotipo")
					
					.leftOuterJoin("contato.listaTelefone telefone")
					.leftOuterJoin("telefone.telefonetipo telefonetipo")
					
					.leftOuterJoin("contato.listaEndereco endereco")
					.leftOuterJoin("endereco.municipio munEndereco")
					.leftOuterJoin("munEndereco.uf ufEndereco")
					.leftOuterJoin("endereco.enderecotipo enderecotipo")
					
					.leftOuterJoin("contato.empresa empresa")
					.leftOuterJoin("contato.listaPessoa listaPessoa")
					
					.where("listaPessoa.pessoa = ?", form)
					.orderBy("contato.nome")
					
					.list();		
	}
	
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaTelefone");
		save.saveOrUpdateManaged("listaEndereco");
		Contato contato = (Contato)save.getEntity();
		contato.setTipopessoa(Tipopessoa.PESSOA_FISICA);
		
		if (contato.getListaContato() != null && contato.getListaContato().size() > 0) {
			save.saveOrUpdateManaged("listaContato");
		}
	}


	public Contato carregaContato(Contato contato) {
		if (contato == null || contato.getCdpessoa() == null) {
			throw new SinedException("Contato n�o pode ser nulo.");
		}
		return querySined()
					.select("contato.emailcontato, contato.nome, listaTelefone.telefone, telefonetipo.cdtelefonetipo, telefonetipo.nome, municipio.nome, " +
							"uf.sigla, contato.emailcontato")
					.leftOuterJoin("contato.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("contato.listaTelefone listaTelefone")
					.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
					.where("contato = ?", contato)
					.unique();
	}


	/**
	 * Carrega a lista de contatos de uma pessoa para combo.
	 * 
	 * @param pessoa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contato> findByPessoaCombo(Pessoa pessoa, Boolean ativo) {
		if (pessoa == null || pessoa.getCdpessoa() == null) {
			throw new SinedException("Pessoa n�o pode ser nulo.");
		}
		return querySined()
					.select("contato.nome, contato.cdpessoa")
					.leftOuterJoin("contato.listaPessoa listaPessoa")
					.where("listaPessoa.pessoa = ?", pessoa)
					.where("coalesce(contato.ativo, false) = ?", ativo)
					.orderBy("contato.nome")
					.list();
	}


	/**
	 * M�todo que busca o email de um determinado contato
	 * 
	 * @param contato
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Contato getEmailContato(Contato contato) {
		return querySined()
			.select("contato.emailcontato")
			.where("contato = ?", contato)
			.unique();
	}
	
	/**
	 * M�todo para obter a lista de contatos pelo cliente informado
	 * 
	 * @param contato
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public List<Contato> getListContatoByCliente(Cliente cliente, Boolean ativo){
		if (cliente == null || cliente.getCdpessoa() == null) {
			throw new SinedException("Cliente n�o pode ser nulo.");
		}
		return querySined()
					.leftOuterJoin("contato.listaPessoa listaPessoa")
					.where("listaPessoa.pessoa = ?", cliente)
					.where("coalesce(contato.ativo,false) = ?", ativo)
					.list();
	}
	
	/**
	* Busca a lista de contato do cliente com contatotipo
	*
	* @param pessoa
	* @param contatotipo
	* @return
	* @since Jul 15, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Contato> findListIdcontatotipo(Pessoa pessoa, Contatotipo contatotipo){
		if(pessoa == null){
			throw new SinedException("Par�metro inv�lido.");			
		}
		return querySined()
					.select("pessoa.cdpessoa, contatotipo.cdcontatotipo, contato.cdpessoa, contato.nome")	
					.leftOuterJoin("contato.contatotipo contatotipo")
					.leftOuterJoin("contato.listaPessoa listaPessoa")
					.leftOuterJoin("listaPessoa.pessoa pessoa")
					.where("pessoa.cdpessoa = ?", pessoa.getCdpessoa())
					.list();
	}
	public List<Contato> findByPessoaContatotipo(String whereIn, Contatotipo contatotipo) {
		if (StringUtils.isEmpty(whereIn)) {
			throw new SinedException("Pessoa n�o pode ser nulo.");
		}
		return querySined()
				.select("contato.cdpessoa, contato.nome, contato.emailcontato, contato.observacao, contato.dtnascimento, " +
						"munContato.cdmunicipio, munContato.nome, ufContato.cduf, ufContato.sigla, contato.email, " +
						"telefone.cdtelefone, telefone.telefone, telefonetipo.cdtelefonetipo," +
						"endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.bairro, endereco.caixapostal," +
						"endereco.cep,endereco.complemento, munEndereco.cdmunicipio, munEndereco.nome, ufEndereco.cduf, " +
						"ufEndereco.sigla, enderecotipo.cdenderecotipo, contatotipo.cdcontatotipo, endereco.pontoreferencia," +
						"contatotipo.nome, contato.participacao, contato.quotas, contatotipo.socio, contato.cpf, contato.identidade, contato.responsavelos, " +
						"pessoa.cdpessoa")
					
					.leftOuterJoin("contato.listaPessoa listaPessoa")
					.leftOuterJoin("listaPessoa.pessoa pessoa")

					.leftOuterJoin("contato.municipio munContato")
					.leftOuterJoin("munContato.uf ufContato")
					
					.leftOuterJoin("contato.contatotipo contatotipo")
					
					.leftOuterJoin("contato.listaTelefone telefone")
					.leftOuterJoin("telefone.telefonetipo telefonetipo")
					
					.leftOuterJoin("contato.listaEndereco endereco")
					.leftOuterJoin("endereco.municipio munEndereco")
					.leftOuterJoin("munEndereco.uf ufEndereco")
					.leftOuterJoin("endereco.enderecotipo enderecotipo")
					
					.whereIn("pessoa.cdpessoa", whereIn)
					.where("contatotipo = ?",contatotipo)
					.orderBy("contato.nome")
					
					.list();		
	}

	/**
	 * M�todo que carrega as informa��es do contato
	 *
	 * @param contato
	 * @return
	 * @author Luiz Fernando
	 */
	public Contato loadForOrdemcompraInfo(Contato contato) {
		if(contato == null || contato.getCdpessoa() == null)
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		
		return querySined()
			.leftOuterJoinFetch("contato.listaTelefone listaTelefone")
			.leftOuterJoinFetch("listaTelefone.telefonetipo telefonetipo")
			.leftOuterJoinFetch("contato.listaPessoacategoria listaPessoacategoria")
			.leftOuterJoinFetch("listaPessoacategoria.categoria categoria")
			.where("contato = ?", contato)
			.unique();
	}

	/**
	 * 
	 * @param pessoa
	 * @return
	 */
	public List<Contato> buscaEmailsTelefones(Pessoa pessoa) {
		return querySined()
			.select("contato.cdpessoa, contato.emailcontato, telefone.telefone")
			.leftOuterJoin("contato.listaTelefone telefone")
			.where("contato = ?", pessoa)
			.list();
	}
	
	/**
	 * M�todo que busca o contato da pessoa
	 *
	 * @param pessoa
	 * @param receberBoleto
	 * @return
	 * @author Luiz Fernando
	 * @since 18/11/2013
	 */
	public List<Contato> findByPessoa(Pessoa pessoa, Boolean receberBoleto) {
		if(pessoa == null || pessoa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
			.select("contato.cdpessoa, contato.emailcontato, contato.nome")
			.leftOuterJoin("contato.listaPessoa listaPessoa")
			.where("listaPessoa.pessoa = ?", pessoa)
			.where("contato.receberboleto = ?", receberBoleto)
			.list();
	}

	public List<Contato> findForAndroid(String whereIn){
		return querySined()
				.select("contato.cdpessoa, contato.nome, contato.ativo, contatotipo.cdcontatotipo, pessoa.cdpessoa")
				.leftOuterJoin("contato.listaPessoa listaPessoa")
				.leftOuterJoin("listaPessoa.pessoa pessoa")
				.leftOuterJoin("contato.contatotipo contatotipo")
				.where("exists (select 1 from Cliente c where c.cdpessoa = pessoa.cdpessoa)")
				.whereIn("contato.cdpessoa", whereIn)
				.list();
	}
	
	/**
	 * M�todo que insere o contato  para o bot�o resgatar contato
	 * @param bean
	 * @param cdpessoaligacao
	 */
	public void insertSomenteContato(Contato bean, Integer cdpessoaligacao) {
		if(bean == null || bean.getCdpessoa() == null){
			throw new SinedException("Contato n�o pode ser nulo.");
		}
		
		getJdbcTemplate().update("insert into Contato (cdpessoa) values (?)", new Object[]{bean.getCdpessoa()});
	}

	public void updateContatoAndroid(Contato contato) {
		if(contato != null && contato.getCdpessoa() != null){
			getJdbcTemplate().update("UPDATE PESSOA SET nome = ?, " +
					 " email = ? " +
				     " WHERE CDPESSOA = ?;",
				     new Object[]{contato.getNome(), 
								  contato.getEmail(),
								  contato.getCdpessoa()});
			
		}
		
	}
	
	/**
	 * Carrega a lista de contatos de uma pessoa para combo
	 * passando o whereInPessoa.
	 * 
	 * @param pessoa
	 * @return
	 */
	public List<Contato> findByPessoaComboWhereIn(String whereInPessoa, Boolean ativo) {
		return querySined()
					.select("contato.nome, contato.cdpessoa")
					.leftOuterJoin("contato.listaPessoa listaPessoa")
					.whereIn("listaPessoa.pessoa", whereInPessoa)
					.where("coalesce(contato.ativo,false) = ?", ativo)
					.orderBy("contato.nome")
					.list();
	}
	
	/**
	 * Atualiza o email de contato.
	 * Necess�rio para o gerenciamento de email.
	 * @param contato
	 * @author Andrey Leonardo
	 * @since 04/08/2015
	 */
	public void updateContatoEmail(Contato contato){
		if(contato != null && contato.getEmailcontato() != null && !contato.getEmailcontato().isEmpty()){
			getHibernateTemplate()
			.bulkUpdate("update Contato c set c.emailcontato = ? where c.id = ?",
					new Object[]{contato.getEmailcontato(), contato.getCdpessoa()});
		}
	}


	public Contato getContatoByCdPessoaLigacao(Integer cdpessoaligacao) {
		return querySined()
				.select("contato.emailcontato, contato.recebernf, contato.cdpessoa, contato.nome, contato.receberboleto, contato.observacao, " +
						"contatotipo.nome, " +
						"listaTelefone.telefone, " +
						"telefonetipo.cdtelefonetipo ")
				.leftOuterJoin("contato.contatotipo contatotipo")
				.leftOuterJoin("contato.listaTelefone listaTelefone")
				.leftOuterJoin("listaTelefone.telefonetipo telefonetipo")
				.where("contato.cdpessoa = ?", cdpessoaligacao)
				.unique();
	}
}
