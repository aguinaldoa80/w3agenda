package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Apontamento;
import br.com.linkcom.sined.geral.bean.ApontamentoHoras;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ApontamentoHorasDAO extends GenericDAO<ApontamentoHoras>{

	/**
	 * Carrega a lista de apontamento horas a partir de um apontamento.
	 *
	 * @param apontamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<ApontamentoHoras> findByApontamento(Apontamento apontamento) {
		if(apontamento == null || apontamento.getCdapontamento() == null){
			throw new SinedException("Apontamento n�o pode ser nulo.");
		}
		return query()
					.select("apontamentoHoras.hrinicio, apontamentoHoras.hrfim")
					.where("apontamentoHoras.apontamento = ?", apontamento)
					.orderBy("apontamentoHoras.hrinicio desc")
					.list();
	}

}
