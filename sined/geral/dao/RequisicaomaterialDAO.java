package br.com.linkcom.sined.geral.dao;

import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.AUTORIZADA;
import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.CANCELADA;
import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.ESTORNAR;
import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.ROMANEIO_COMPLETO;
import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.ROMANEIO_GERADO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.OrigemSuprimentos;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.Romaneiosituacao;
import br.com.linkcom.sined.geral.bean.Situacaorequisicao;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.enumeration.Mostrarromaneio;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.RequisicaomaterialFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RequisicaomaterialDAO extends GenericDAO<Requisicaomaterial> {
	
	private ArquivoDAO arquivoDAO;
	private MaterialcategoriaService materialcategoriaService;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {this.materialcategoriaService = materialcategoriaService;}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Requisicaomaterial> query,	FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		RequisicaomaterialFiltro filtro = (RequisicaomaterialFiltro) _filtro;
		query
			.select("requisicaomaterial.cdrequisicaomaterial, requisicaomaterial.identificador, requisicaomaterial.descricao, " +
					"requisicaomaterial.qtde, requisicaomaterial.dtlimite, requisicaomaterial.dtcriacao, requisicaomaterial.observacao, requisicaomaterial.dtbaixa, " +
					"material.nome, material.cdmaterial, material.identificacao, unidademedida.cdunidademedida, unidademedida.simbolo, " +
					"materialmestregrade.nome, materialmestregrade.cdmaterial, materialmestregrade.identificacao, " +
					"localarmazenagem.nome, colaborador.nome, aux_requisicaomaterial.situacaorequisicao ")
			.join("requisicaomaterial.localarmazenagem localarmazenagem")
			.join("requisicaomaterial.colaborador colaborador")
			.join("requisicaomaterial.material material")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("material.materialmestregrade materialmestregrade")
			.join("requisicaomaterial.aux_requisicaomaterial aux_requisicaomaterial")
			.whereLikeIgnoreAll("requisicaomaterial.descricao", filtro.getDescricao())
			.whereLikeIgnoreAll("colaborador.nome", filtro.getColaborador())
			.whereIn("requisicaomaterial.identificador", filtro.getIdentificador())
			.where("material.cdmaterial = ?", filtro.getMaterial() != null ? filtro.getMaterial().getCdmaterial() : "")			
			.where("requisicaomaterial.dtlimite >= ?", filtro.getDatalimitede())
			.where("requisicaomaterial.dtlimite <= ?", filtro.getDatalimiteate())
			.where("requisicaomaterial.centrocusto = ?", filtro.getCentrocusto())
			.where("requisicaomaterial.projeto = ?", filtro.getProjeto())
			.where("requisicaomaterial.empresa = ?", filtro.getEmpresa())
			.where("localarmazenagem = ?", filtro.getLocalarmazenagem())
			.where("requisicaomaterial.departamento = ?", filtro.getDepartamento())
			.where("material.materialgrupo = ?",filtro.getMaterialgrupo())
			.where("material.materialtipo = ?",filtro.getMaterialtipo())
			.where("dtcriacao >= ?",filtro.getDtcriacaoinicio())
			.where("dtcriacao <= ?",filtro.getDtcriacaofim())
			.ignoreJoin("listaVrequisicaomaterialentrega", "vrequisicaomaterialmovimentacaoestoque", "unidademedidaVrme");
		
		if(filtro.getMaterialcategoria() != null){
			materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
			query
				.leftOuterJoin("material.materialcategoria materialcategoria")
				.leftOuterJoin("materialcategoria.vmaterialcategoria vmaterialcategoria")
				.openParentheses()
					.where("vmaterialcategoria.identificador like ? ||'.%'", filtro.getMaterialcategoria().getIdentificador())
				.or()
					.where("vmaterialcategoria.identificador like ? ", filtro.getMaterialcategoria().getIdentificador())
				.closeParentheses();
		}
				
		if(filtro.getListaclasses() != null)
			query.whereIn("requisicaomaterial.materialclasse", CollectionsUtil.listAndConcatenate(filtro.getListaclasses(), "cdmaterialclasse",","));
		
		if(filtro.getListaorigens() != null) {
			query.openParentheses();
				for (String origem : filtro.getListaorigens()) {
					if(origem.equals(OrigemSuprimentos.PROJETO.name())) {
						query.where("requisicaomaterial.projeto is not null").or();
					} else if(origem.equals(OrigemSuprimentos.USUARIO.name())) {
						query.where("requisicaomaterial.requisicaoorigem.usuario is not null").or();
					} else if(origem.equals(OrigemSuprimentos.PEDIDOVENDA.name())) {
						query.where("requisicaomaterial.requisicaoorigem.pedidovenda is not null").or();
					} 
				}
			query.closeParentheses();
		}
		
		if(filtro.getBaixa() != null){
			query.leftOuterJoin("requisicaomaterial.vrequisicaomaterialmovimentacaoestoque vrequisicaomaterialmovimentacaoestoque");
			if(filtro.getBaixa()){
				query.where("vrequisicaomaterialmovimentacaoestoque.qtde is not null")
					.where(" requisicaomaterial.qtde > vrequisicaomaterialmovimentacaoestoque.qtde ");
			}else {
				query.where("vrequisicaomaterialmovimentacaoestoque.qtde is null");
			}
		}
		
		if(filtro.getListasituacoes() != null) {
			query.whereIn("aux_requisicaomaterial.situacaorequisicao", Situacaorequisicao.listAndConcatenate(filtro.getListasituacoes()));
		}
		
		if(filtro.getMostrarromaneio() != null){
			if(Mostrarromaneio.GERADO.equals(filtro.getMostrarromaneio()) || Mostrarromaneio.SEMROMANEIO.equals(filtro.getMostrarromaneio())){
				query
					.where((Mostrarromaneio.SEMROMANEIO.equals(filtro.getMostrarromaneio()) ? " not " : "") + 
							"exists(select ro.cdromaneioorigem " +
									"from Romaneioorigem ro " +
									"join ro.requisicaomaterial rq " +
									"join ro.romaneio r " +
									"where r.romaneiosituacao <> " + Romaneiosituacao.CANCELADA.getCdromaneiosituacao() + " " +
									"and rq = requisicaomaterial)");
			}else if(Mostrarromaneio.PENDENTE.equals(filtro.getMostrarromaneio())){
				query
					.where("aux_requisicaomaterial.situacaorequisicao = ?", Situacaorequisicao.COMPRA_FINALIZADA)
					.where("requisicaomaterial.cdrequisicaomaterial in (select vrme.cdrequisicaomaterial " +
							" from Vrequisicaomaterialentrega vrme " +
							" where vrme.entregaromaneio = false) ");
			}
		}
		
		query.orderBy("requisicaomaterial.identificador desc");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Requisicaomaterial> query) {
		query
			.select("requisicaomaterial.cdrequisicaomaterial, requisicaomaterial.identificador, requisicaomaterial.descricao, requisicaomaterial.qtde, requisicaomaterial.dtlimite, " +
					"material.cdmaterial, material.nome, material.identificacao, " +
					"materialmestregrade.nome, materialmestregrade.cdmaterial, materialmestregrade.identificacao, " +
					"materialgrupo.cdmaterialgrupo, materialtipo.cdmaterialtipo, materialclasse.cdmaterialclasse, centrocusto.cdcentrocusto, pedidovenda.cdpedidovenda, producaoagenda.cdproducaoagenda, requisicaomaterial.dtcriacao, " +
					"projeto.cdprojeto, projeto.nome, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, departamento.cddepartamento, departamento.nome, departamento.codigofolha, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, requisicaoorigem.cdrequisicaoorigem, usuario.cdpessoa, " +
					"aux_requisicaomaterial.situacaorequisicao, colaborador.cdpessoa, colaborador.nome, requisicaomaterial.observacao, requisicaomaterial.cdusuarioaltera, requisicaomaterial.dtaltera, " +
					"frequencia.cdfrequencia, requisicaomaterial.qtdefrequencia, arquivo.cdarquivo, arquivo.nome, unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
					"requisicaomaterial.altura, requisicaomaterial.comprimento, requisicaomaterial.largura, requisicaomaterial.qtdvolume, requisicaomaterial.pesototal, " +
					"planejamentorecursogeral.cdplanejamentorecursogeral, planejamento.cdplanejamento, planejamento.descricao ")
			.join("requisicaomaterial.frequencia frequencia")
			.join("requisicaomaterial.material material")
			.leftOuterJoin("material.materialmestregrade materialmestregrade")
			.join("material.unidademedida unidademedida")
			.join("material.materialgrupo materialgrupo")
			.join("requisicaomaterial.materialclasse materialclasse")
			.join("requisicaomaterial.localarmazenagem localarmazenagem")
			.join("requisicaomaterial.colaborador colaborador")
			.join("requisicaomaterial.aux_requisicaomaterial aux_requisicaomaterial")
			.leftOuterJoin("material.materialtipo materialtipo")
			.leftOuterJoin("requisicaomaterial.arquivo arquivo")
			.leftOuterJoin("requisicaomaterial.centrocusto centrocusto")
			.leftOuterJoin("requisicaomaterial.projeto projeto")
			.leftOuterJoin("requisicaomaterial.empresa empresa")
			.leftOuterJoin("requisicaomaterial.departamento departamento")
			.leftOuterJoin("requisicaomaterial.requisicaoorigem requisicaoorigem")
			.leftOuterJoin("requisicaoorigem.usuario usuario")
			.leftOuterJoin("requisicaoorigem.pedidovenda pedidovenda")
			.leftOuterJoin("requisicaoorigem.producaoagenda producaoagenda")
			.leftOuterJoin("requisicaoorigem.planejamentorecursogeral planejamentorecursogeral")
			.leftOuterJoin("planejamentorecursogeral.planejamento planejamento")
			;
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Requisicaomaterial requisicaomaterial = (Requisicaomaterial) save.getEntity();
		
		if(requisicaomaterial.getArquivo() != null){
			arquivoDAO.saveFile(requisicaomaterial, "arquivo");
		}
	}

	/**
	* M�todo que carrega as requisi��es de material da listagem
	*
	* @param filtro
	* @return
	* @since 10/09/2015
	* @author Luiz Fernando
	*/
	public List<Requisicaomaterial> findListagem(RequisicaomaterialFiltro filtro) {
		QueryBuilder<Requisicaomaterial> query = query();
		updateListagemQuery(query, filtro);
		query.ignoreJoin("");
		
		return query
				.select("requisicaomaterial.cdrequisicaomaterial, requisicaomaterial.identificador, requisicaomaterial.descricao, " +
						"requisicaomaterial.qtde, requisicaomaterial.dtlimite, requisicaomaterial.dtcriacao, requisicaomaterial.observacao, requisicaomaterial.dtbaixa, " +
						"material.nome, material.cdmaterial, material.identificacao, unidademedida.cdunidademedida, " +
						"materialmestregrade.nome, materialmestregrade.cdmaterial, materialmestregrade.identificacao, " +
						"localarmazenagem.nome, colaborador.nome, aux_requisicaomaterial.situacaorequisicao, " +
						"listaVrequisicaomaterialentrega.entregaromaneio, listaVrequisicaomaterialentrega.qtde, " +
						"unidademedidaVrme.cdunidademedida, vrequisicaomaterialmovimentacaoestoque.qtde ")
				.leftOuterJoinIfNotExists("requisicaomaterial.listaVrequisicaomaterialentrega listaVrequisicaomaterialentrega")
				.leftOuterJoinIfNotExists("listaVrequisicaomaterialentrega.unidademedida unidademedidaVrme")
				.leftOuterJoinIfNotExists("requisicaomaterial.vrequisicaomaterialmovimentacaoestoque vrequisicaomaterialmovimentacaoestoque")
				.list();
				
	}
	/**
	* M�todo que carrega as requisi��es com as listas
	*
	* @param whereIn
	* @param orderBy
	* @param asc
	* @return
	* @since 10/09/2015
	* @author Luiz Fernando
	*/
	public List<Requisicaomaterial> findWithList(String whereIn, String orderBy, boolean asc) {
		if (whereIn == null || whereIn.equals("")) {
			return new ArrayList<Requisicaomaterial>();
		}
		QueryBuilder<Requisicaomaterial> query = query()
				.select("requisicaomaterial.cdrequisicaomaterial, requisicaomaterial.identificador, requisicaomaterial.descricao, " +
						"requisicaomaterial.qtde, requisicaomaterial.dtlimite, requisicaomaterial.dtcriacao, requisicaomaterial.observacao, requisicaomaterial.dtbaixa, " +
						"material.nome, material.cdmaterial, material.identificacao, unidademedida.cdunidademedida, unidademedida.simbolo, " +
						"materialmestregrade.nome, materialmestregrade.cdmaterial, materialmestregrade.identificacao, " +
						"localarmazenagem.nome, colaborador.nome, aux_requisicaomaterial.situacaorequisicao, " +
						"listaVrequisicaomaterialentrega.entregaromaneio, listaVrequisicaomaterialentrega.qtde, " +
						"unidademedidaVrme.cdunidademedida, vrequisicaomaterialmovimentacaoestoque.qtde," +
						"localpedido.cdlocalarmazenagem, localentregamaterial.cdlocalarmazenagem ")
				.leftOuterJoin("requisicaomaterial.listaVrequisicaomaterialentrega listaVrequisicaomaterialentrega WITH ( listaVrequisicaomaterialentrega.cdrequisicaomaterial in ( " + whereIn + ") )")
				.leftOuterJoin("listaVrequisicaomaterialentrega.unidademedida unidademedidaVrme")
				.leftOuterJoin("requisicaomaterial.vrequisicaomaterialmovimentacaoestoque vrequisicaomaterialmovimentacaoestoque")
				.join("requisicaomaterial.localarmazenagem localarmazenagem")
				.join("requisicaomaterial.colaborador colaborador")
				.join("requisicaomaterial.material material")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("material.materialmestregrade materialmestregrade")
				.join("requisicaomaterial.aux_requisicaomaterial aux_requisicaomaterial")	
				.leftOuterJoin("listaVrequisicaomaterialentrega.localentregamaterial localentregamaterial")
				.leftOuterJoin("listaVrequisicaomaterialentrega.localpedido localpedido")
				.whereIn("requisicaomaterial.cdrequisicaomaterial", whereIn);

		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy + " " + (asc ? "ASC" : "DESC"));
		}else {
			query.orderBy("requisicaomaterial.identificador desc");
		}

		return query.list();
	}
	
	/**
	 * Retorna todas as requisi��es de material onde for igual aos PK's 
	 * concatenadas na String whereIn
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Requisicaomaterial> findRequisicoes(String whereIn) {
		if (whereIn == null || whereIn.equals(""))
			throw new SinedException("Nenhum item selecionado.");
		
		return query()
			.select("requisicaomaterial.cdrequisicaomaterial, requisicaomaterial.dtautorizacao, requisicaomaterial.dtbaixa, aux_requisicaomaterial.situacaorequisicao, requisicaomaterial.qtde, " +
					"material.cdmaterial, material.nome, projeto.cdprojeto ")
			.join("requisicaomaterial.aux_requisicaomaterial aux_requisicaomaterial")
			.leftOuterJoin("requisicaomaterial.material material")
			.leftOuterJoin("requisicaomaterial.projeto projeto")
			.whereIn("requisicaomaterial.cdrequisicaomaterial", whereIn)
			.list();
	}

	/** 
	 * Atualiza a situa��o da requisi��o.
	 * 
	 * @param whereIn
	 * @param situa��o
	 * @author Tom�s Rabelo
	 */
	public void doUpdateRequisicoes(String whereIn, Situacaorequisicao situacao, Boolean cancelamentoRomaneio) {
		if (situacao == null) {
			throw new SinedException("Situa��o da requisi��o n�o pode ser nulo.");
		}
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		
		StringBuilder sql = new StringBuilder()
			.append("update Requisicaomaterial r set r.cdusuarioaltera = ?, r.dtaltera = ?");
		
		if(situacao.equals(AUTORIZADA)){
			sql.append(", r.dtautorizacao = ?");
		} else if(situacao.equals(CANCELADA)) {
			sql.append(", r.dtcancelamento = ?");
		} else if(situacao.equals(ESTORNAR)) {
			sql.append(", r.dtautorizacao = null, r.dtcancelamento = null, r.dtromaneiocompleto = null, dtbaixa = null");
		} else if(situacao.equals(ROMANEIO_COMPLETO)){
			sql.append(", r.dtromaneiocompleto = ?");
		} else if(!situacao.equals(ROMANEIO_GERADO)){
			sql.append(", r.dtbaixa = ?");
		}
		
		if(Boolean.TRUE.equals(cancelamentoRomaneio)){
			sql.append(", r.dtromaneiocompleto = null ");
		}
		
		sql.append(" where r.cdrequisicaomaterial in ("+whereIn+")");
		
		getHibernateTemplate().bulkUpdate(sql.toString(), situacao.equals(ESTORNAR) || situacao.equals(ROMANEIO_GERADO) ? new Object[]{cdusuarioaltera,hoje} : new Object[]{cdusuarioaltera,hoje, hoje});
	}

	/** 
	 * M�todo que busca quase todos atributos da requisi��o. Para poder gerar solicita��o de compra
	 * 
	 * @param whereIn
	 * @author Tom�s Rabelo
	 */
	public List<Requisicaomaterial> findRequisicoesParaGerarCompra(String whereIn) {
		if (whereIn == null || whereIn.equals(""))
			throw new SinedException("Nenhum item selecionado.");
		
		return query()
			.select("requisicaomaterial.cdrequisicaomaterial, requisicaomaterial.descricao, requisicaomaterial.qtde, requisicaomaterial.dtlimite, requisicaomaterial.dtbaixa, requisicaomaterial.observacao, " +
					"departamento.cddepartamento, material.cdmaterial, materialclasse.cdmaterialclasse, local.cdlocalarmazenagem, centrocusto.cdcentrocusto, " +
					"projeto.cdprojeto, empresa.cdpessoa, colaborador.cdpessoa, aux_requisicaomaterial.situacaorequisicao, " +
					"frequencia.cdfrequencia, requisicaomaterial.qtdefrequencia, requisicaomaterial.identificador, unidademedida.cdunidademedida," +
					"requisicaomaterial.identificador, requisicaomaterial.altura, requisicaomaterial.largura, requisicaomaterial.comprimento, " +
					"requisicaomaterial.pesototal, requisicaomaterial.qtdvolume, " +
					"unidademedidaConv.cdunidademedida, unidademedidaConv.nome, unidademedidaConv.simbolo, " +
					"listaMaterialunidademedida.fracao, listaMaterialunidademedida.qtdereferencia, listaMaterialunidademedida.prioridadecompra, " +
					"vrequisicaomaterialmovimentacaoestoque.qtde ")
			.join("requisicaomaterial.aux_requisicaomaterial aux_requisicaomaterial")					
			.join("requisicaomaterial.frequencia frequencia")					
			.join("requisicaomaterial.material material")
			.leftOuterJoin("requisicaomaterial.vrequisicaomaterialmovimentacaoestoque vrequisicaomaterialmovimentacaoestoque")
			.leftOuterJoin("requisicaomaterial.centrocusto centrocusto")
			.leftOuterJoin("requisicaomaterial.localarmazenagem local")
			.leftOuterJoin("requisicaomaterial.departamento departamento")
			.leftOuterJoin("requisicaomaterial.materialclasse materialclasse")
			.leftOuterJoin("requisicaomaterial.projeto projeto")
			.leftOuterJoin("requisicaomaterial.empresa empresa")
			.leftOuterJoin("requisicaomaterial.colaborador colaborador")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("material.listaMaterialunidademedida listaMaterialunidademedida")
			.leftOuterJoin("listaMaterialunidademedida.unidademedida unidademedidaConv")
			.whereIn("requisicaomaterial.cdrequisicaomaterial", whereIn)
			.list();
	}

	/**
	 * Carrega as requisi��es de material para baixar
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Requisicaomaterial> findRequisicoesParaBaixa(String whereIn) {
		if (whereIn == null || whereIn.equals(""))
			throw new SinedException("Nenhum item selecionado.");
		
		return query()
			.select("requisicaomaterial.cdrequisicaomaterial, requisicaomaterial.qtde, material.cdmaterial, material.nome, material.obrigarlote, " +
					"materialclasse.cdmaterialclasse, materialclasse.nome, colaborador.cdpessoa, " +
					"aux_requisicaomaterial.situacaorequisicao, local.cdlocalarmazenagem, local.nome, local.permitirestoquenegativo, " +
					"empresa.cdpessoa, requisicaomaterial.identificador, materialgrupo.cdmaterialgrupo, materialgrupo.gradeestoquetipo, " +
					"materialmestregrade.cdmaterial, materialmestregrade.nome, projeto.cdprojeto, projeto.nome, producaoagenda.cdproducaoagenda," +
					"centrocusto.cdcentrocusto, contaGerencialRequisicao.cdcontagerencial ")
			.join("requisicaomaterial.material material")
			.join("requisicaomaterial.materialclasse materialclasse")
			.join("requisicaomaterial.colaborador colaborador")
			.join("requisicaomaterial.localarmazenagem local")
			.join("requisicaomaterial.aux_requisicaomaterial aux_requisicaomaterial")
			.leftOuterJoin("requisicaomaterial.projeto projeto")
			.leftOuterJoin("requisicaomaterial.empresa empresa")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.materialmestregrade materialmestregrade")
			.leftOuterJoin("requisicaomaterial.requisicaoorigem requisicaoorigem")
			.leftOuterJoin("requisicaoorigem.producaoagenda producaoagenda")
			.leftOuterJoin("requisicaomaterial.centrocusto centrocusto")
			.leftOuterJoin("material.materialRateioEstoque materialRateioEstoque")
			.leftOuterJoin("materialRateioEstoque.contaGerencialRequisicao contaGerencialRequisicao")
			.whereIn("requisicaomaterial.cdrequisicaomaterial", whereIn, false)
			.list();
	}

	/**
	 * M�todo que busca o �ltimo identificador utilizado
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getUltimoIdentificador() {
		return newQueryBuilderSined(Integer.class)
			.setUseWhereProjeto(false)
			.setUseWhereEmpresa(false)
			.select("max(identificador)")
			.from(Requisicaomaterial.class)
			.unique();
	}
	
	
	/**
	 * Chama a procedure de atualiza��o da tabela auxiliar de requisi��o de material
	 *
	 * @param requisicaomaterial
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaRequisicaomaterial(Requisicaomaterial requisicaomaterial){
		if (requisicaomaterial == null || requisicaomaterial.getCdrequisicaomaterial() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		getJdbcTemplate().execute("SELECT ATUALIZA_REQUISICAOMATERIAL(" + requisicaomaterial.getCdrequisicaomaterial() + ")");
	}
	
	/**
	 * M�todo que busca as informa��es para o relat�rio PDF.
	 * @param filtro
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Requisicaomaterial> findForRequisicaomaterialReport(RequisicaomaterialFiltro filtro){
		QueryBuilder<Requisicaomaterial> query = querySined();
		updateListagemQuery(query, filtro);
		return query.list();
	}
	
	/**
	 * M�todo que retorna os dados para o relat�rio CSV;
	 * @param filtro
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Requisicaomaterial> findForCsv(RequisicaomaterialFiltro filtro){
		QueryBuilder<Requisicaomaterial> query = querySined();
		updateListagemQuery(query, filtro);
		return query
					.select("requisicaomaterial.cdrequisicaomaterial, requisicaomaterial.identificador, requisicaomaterial.qtde, " +
							"requisicaomaterial.descricao, requisicaomaterial.dtlimite, requisicaomaterial.dtcriacao, material.nome, material.cdmaterial, material.identificacao, " +
							"localarmazenagem.nome, usuario.nome, materialorigem.nome, colaborador.nome, projetoorigem.nome, " +
							"aux_requisicaomaterial.situacaorequisicao, frequencia.nome, requisicaomaterial.qtdefrequencia, " +
							"requisicaomaterial.observacao, departamento.nome, materialclasse.nome, centrocusto.nome, projeto.nome, empresa.razaosocial, empresa.nome, empresa.nomefantasia, " +
							"requisicaomaterial.dtcancelamento, requisicaomaterial.dtautorizacao, requisicaomaterial.dtbaixa, unidademedida.simbolo ")
					.leftOuterJoin("requisicaomaterial.frequencia frequencia")
					.leftOuterJoin("requisicaomaterial.empresa empresa")
					.leftOuterJoin("requisicaomaterial.departamento departamento")
					.join("requisicaomaterial.materialclasse materialclasse")
					.leftOuterJoin("requisicaomaterial.centrocusto centrocusto")
					.leftOuterJoin("requisicaomaterial.projeto projeto")
					.leftOuterJoin("requisicaomaterial.requisicaoorigem requisicaoorigem")
					.leftOuterJoin("requisicaoorigem.usuario usuario")
					.leftOuterJoin("requisicaoorigem.projeto projetoorigem")
					.leftOuterJoin("requisicaoorigem.material materialorigem")
					.leftOuterJoinIfNotExists("material.unidademedida unidademedida")
			.list();
		
	}

	/**
	 * M�todo que carrega requisicaomaterial para enviar email
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Requisicaomaterial> findForEnvioemailcompra(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro n�o podem ser nulo.");
		
		return query()
				.select("requisicaomaterial.cdrequisicaomaterial, requisicaomaterial.identificador, empresa.cdpessoa, empresa.email, " +
						"projeto.cdprojeto")
				.leftOuterJoin("requisicaomaterial.empresa empresa")
				.leftOuterJoin("requisicaomaterial.projeto projeto")
				.whereIn("requisicaomaterial.cdrequisicaomaterial", whereIn)
				.list();
	}
	
	/**
	 * M�todo busca requisicaomaterial das entregas que est�o baixadas de acordo com o par�metro
	 *
	 * @param whereInEntrega
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<Requisicaomaterial> findRequisitanteMaterialByEntregaBaixada(String whereInEntrega) {
		if(whereInEntrega == null || "".equals(whereInEntrega))
			throw new SinedException("Par�metro n�o pode ser nulo.");
		
		StringBuilder sql = new StringBuilder(
					"SELECT distinct rm.cdrequisicaomaterial as cdrequisicaomaterial, rm.identificador as identificador, " +
					"p.cdpessoa as cdpessoa, p.nome as nome, p.email as email, e.cdentrega as cdentrega " +
					"FROM requisicaomaterial rm " + 
					"JOIN pessoa p on p.cdpessoa = rm.cdcolaborador " + 
					"JOIN solicitacaocompraorigem sco on sco.cdrequisicaomaterial = rm.cdrequisicaomaterial " +
					"JOIN solicitacaocompra sc on sc.cdsolicitacaocompraorigem = sco.cdsolicitacaocompraorigem " + 
					"JOIN cotacaoorigem co on co.cdsolicitacaocompra = sc.cdsolicitacaocompra " +
					"JOIN cotacao c on c.cdcotacao = co.cdcotacao " +
					"JOIN ordemcompra oc on oc.cdcotacao = c.cdcotacao " +
					"JOIN entrega e on e.cdordemcompra = oc.cdordemcompra " +
					"JOIN aux_entrega auxe on auxe.cdentrega = e.cdentrega " +
					"JOIN entregadocumento ed on ed.cdentrega = e.cdentrega " +
					"JOIN entregamaterial em on em.cdentregadocumento = ed.cdentregadocumento " +
					"WHERE em.cdmaterial = rm.cdmaterial AND auxe.situacao = " + Situacaosuprimentos.BAIXADA.getCodigo() + " ");
		
		sql.append(" AND e.cdentrega in (").append(whereInEntrega).append(");");

		SinedUtil.markAsReader();
		List<Requisicaomaterial> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Requisicaomaterial(rs.getInt("cdrequisicaomaterial"), rs.getInt("identificador"),
						new Colaborador(rs.getInt("cdpessoa"), rs.getString("nome"), rs.getString("email")),
						rs.getInt("cdentrega")
						);
			}
		});
		
		return lista;
	}
	
	/**
	 * M�todo que busca as requisi��es de material das entregas
	 *
	 * @param whereInEntrega
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<Requisicaomaterial> findRequisitanteMaterialByEntrega(String whereInEntrega) {
		if(whereInEntrega == null || "".equals(whereInEntrega))
			throw new SinedException("Par�metro n�o pode ser nulo.");
		
		StringBuilder sql = new StringBuilder(
					"SELECT distinct rm.cdrequisicaomaterial as cdrequisicaomaterial, rm.cddepartamento as cddepartamento, " +
					"rm.cdcolaborador as cdcolaborador, oce.cdentrega as cdentrega, rm.cdmaterial as cdmaterial " +
					"FROM requisicaomaterial rm " + 
					"JOIN solicitacaocompraorigem sco on sco.cdrequisicaomaterial = rm.cdrequisicaomaterial " +
					"JOIN solicitacaocompra sc on sc.cdsolicitacaocompraorigem = sco.cdsolicitacaocompraorigem " + 
					"LEFT OUTER JOIN cotacaoorigem co on co.cdsolicitacaocompra = sc.cdsolicitacaocompra " +
					"LEFT OUTER JOIN cotacao c on c.cdcotacao = co.cdcotacao " +
					"LEFT OUTER JOIN ordemcompraorigem oco on oco.cdsolicitacaocompra = sc.cdsolicitacaocompra " +
					"LEFT OUTER JOIN ordemcompra oc on oc.cdcotacao = c.cdcotacao " +
					"JOIN ordemcompraentrega oce on oce.cdordemcompra = oc.cdordemcompra or oce.cdordemcompra = oco.cdordemcompra " +
					"WHERE oce.cdentrega in (" + whereInEntrega + ");");

		SinedUtil.markAsReader();
		List<Requisicaomaterial> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Requisicaomaterial(
						rs.getInt("cdrequisicaomaterial"), 
						rs.getInt("cddepartamento") != 0 ? new Departamento(rs.getInt("cddepartamento")) : null,
						rs.getInt("cdcolaborador") != 0 ? new Colaborador(rs.getInt("cdcolaborador")) : null,
						rs.getInt("cdentrega") != 0 ? new Entrega(rs.getInt("cdentrega")) : null,
						rs.getInt("cdmaterial") != 0 ? new Material(rs.getInt("cdmaterial")) : null
						);
			}
		});
		
		return lista;
	}

	public List<Requisicaomaterial> findProcessoCompra(String whereIn) {
		return query()
				.select("requisicaomaterial.cdrequisicaomaterial, requisicaomaterial.identificador")
				.join("requisicaomaterial.aux_requisicaomaterial aux_requisicaomaterial")
				.whereIn("requisicaomaterial.cdrequisicaomaterial", whereIn)
				.where("aux_requisicaomaterial.situacaorequisicao = ?", Situacaorequisicao.EM_PROCESSO_COMPRA)
				.list();
	}

	/**
	 * Busca as requisi��es de material pela cota��o com arquivos.
	 *
	 * @param cotacao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/11/2012
	 */
	public List<Requisicaomaterial> findByCotacaoWithArquivo(Cotacao cotacao) {
		return query()
					.select("requisicaomaterial.cdrequisicaomaterial, material.cdmaterial, arquivo.cdarquivo")
					.join("requisicaomaterial.arquivo arquivo")
					.join("requisicaomaterial.material material")
					.join("requisicaomaterial.listaSolicitacaocompraorigem listaSolicitacaocompraorigem")
					.join("listaSolicitacaocompraorigem.listaSolicitacaocompra listaSolicitacaocompra")
					.join("listaSolicitacaocompra.listaCotacaoorigem listaCotacaoorigem")
					.join("listaCotacaoorigem.cotacao cotacao")
					.where("cotacao = ?", cotacao)
					.list();
	}

	/**
	 * Busca as requisi��es com arquivo a partir de solicita��o de compra
	 *
	 * @param whereInSolicitacoes
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/11/2012
	 */
	public List<Requisicaomaterial> findBySolicitacaocompraWithArquivo(String whereInSolicitacoes) {
		return query()
				.select("requisicaomaterial.cdrequisicaomaterial, material.cdmaterial, arquivo.cdarquivo")
				.join("requisicaomaterial.arquivo arquivo")
				.join("requisicaomaterial.material material")
				.join("requisicaomaterial.listaSolicitacaocompraorigem listaSolicitacaocompraorigem")
				.join("listaSolicitacaocompraorigem.listaSolicitacaocompra listaSolicitacaocompra")
				.whereIn("listaSolicitacaocompra.cdsolicitacaocompra", whereInSolicitacoes)
				.list();
	}

	/**
	 * Busca para a gera��o de romaneio
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/04/2014
	 */
	public List<Requisicaomaterial> findForRomaneio(String whereIn) {
		return query()
				.select("requisicaomaterial.cdrequisicaomaterial, material.cdmaterial, material.nome, " +
						"material.identificacao, materialclasse.cdmaterialclasse, requisicaomaterial.qtde, " +
						"projeto.cdprojeto, localarmazenagem.cdlocalarmazenagem, empresa.cdpessoa")
				.join("requisicaomaterial.material material")
				.join("requisicaomaterial.materialclasse materialclasse")
				.leftOuterJoin("requisicaomaterial.projeto projeto")
				.leftOuterJoin("requisicaomaterial.localarmazenagem localarmazenagem")
				.leftOuterJoin("requisicaomaterial.empresa empresa")
				.whereIn("requisicaomaterial.cdrequisicaomaterial", whereIn)
				.list();
	}

	/**
	 * Carrega a situa��o da requisi��o
	 *
	 * @param requisicaomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/01/2015
	 */
	public Requisicaomaterial loadWithSituacao(Requisicaomaterial requisicaomaterial) {
		return query()
				.select("requisicaomaterial.cdrequisicaomaterial, aux_requisicaomaterial.situacaorequisicao")
				.leftOuterJoin("requisicaomaterial.aux_requisicaomaterial aux_requisicaomaterial")
				.where("requisicaomaterial = ?", requisicaomaterial)
				.unique();
	}
	
	/**
	 * M�todo que retorna a quantidade de requisicao de material do pedido de venda
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Double getQteRequisicaomaterialByPedidovenda(Pedidovenda pedidovenda) {
		
		Long qtde = newQueryBuilderSined(Long.class)
						.select("count(*)")
						.from(Requisicaomaterial.class)
						.join("requisicaomaterial.requisicaoorigem requisicaoorigem")
						.join("requisicaomaterial.aux_requisicaomaterial aux_requisicaomaterial")
						.where("aux_requisicaomaterial.situacaorequisicao <> ?", Situacaorequisicao.CANCELADA)
						.where("requisicaoorigem.pedidovenda = ?", pedidovenda)
						.unique();
		return qtde != null ? qtde.doubleValue() : 0.0;
	}
	
	/**
	 * M�todo que retorna a quantidade de requisicao de material baixada do pedido de venda
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Double getQteRequisicaomaterialBaixadaByPedidovenda(Pedidovenda pedidovenda) {
		Long qtde = newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Requisicaomaterial.class)
				.join("requisicaomaterial.requisicaoorigem requisicaoorigem")
				.join("requisicaomaterial.aux_requisicaomaterial aux_requisicaomaterial")
				.where("requisicaoorigem.pedidovenda = ?", pedidovenda)
				.where("aux_requisicaomaterial.situacaorequisicao = ?", Situacaorequisicao.COMPRA_FINALIZADA)
				.unique();
		return qtde != null ? qtde.doubleValue() : 0.0;
	}

	/**
	* M�todo que busca os materiais requisitados pelo projeto com a quantidade
	*
	* @param projeto
	* @param whereInMaterial
	* @param whereIn
	* @return
	* @since 18/03/2016
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public List<Requisicaomaterial> findForBloquearQtdeCompra(Projeto projeto, String whereInMaterial, String whereIn) {
		String sql = 	" select rm.cdmaterial as cdmaterial, sum(rm.qtde) as qtdetotal " +
						" from requisicaomaterial rm " +
						" join aux_requisicaomaterial aux_rm on aux_rm.cdrequisicaomaterial = rm.cdrequisicaomaterial and aux_rm.situacao <> " + Situacaorequisicao.CANCELADA.getCodigo() + 
						" where rm.cdprojeto = " + projeto.getCdprojeto() +
						" and rm.cdmaterial in (" + whereInMaterial + ")" +
						(StringUtils.isNotBlank(whereIn) ? " and rm.cdrequisicaomaterial not in (" + whereIn + ")"  : "") +
						" group by rm.cdmaterial";

		SinedUtil.markAsReader();
		List<Requisicaomaterial> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Requisicaomaterial bean = new Requisicaomaterial();
				bean.setMaterial(new Material(rs.getInt("cdmaterial")));
				bean.setQtde(rs.getDouble("qtdetotal"));
				return bean;
			}
		});
		
		return list;
	}
}
