package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Configuracaodre;
import br.com.linkcom.sined.geral.bean.Configuracaodretipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Movimentacaocontabil;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.modulo.contabil.controller.process.bean.GerarDREListagemLancamentosBean;
import br.com.linkcom.sined.modulo.contabil.controller.process.filter.GerarDREFiltro;
import br.com.linkcom.sined.modulo.contabil.controller.report.filtro.EmitirBalancoPatrimonialFiltro;
import br.com.linkcom.sined.modulo.contabil.controller.report.filtro.EmitirLivroRazaoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.MovimentacaocontabilFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MovimentacaocontabilDAO extends GenericDAO<Movimentacaocontabil>{
	
	private EmpresaService empresaService;
	
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Movimentacaocontabil> query, FiltroListagem _filtro){
		MovimentacaocontabilFiltro filtro = (MovimentacaocontabilFiltro) _filtro;
		
		query
			.select("movimentacaocontabil.cdmovimentacaocontabil, movimentacaocontabil.dtlancamento, movimentacaocontabil.valor," +
					"movimentacaocontabil.tipoLancamento, movimentacaocontabil.historico, " +
					"operacaocontabil.cdoperacaocontabil, operacaocontabil.nome, " +
					"loteContabil.cdLoteContabil")
			.join("movimentacaocontabil.empresa empresa")
			.leftOuterJoin("movimentacaocontabil.operacaocontabil operacaocontabil")
			.leftOuterJoin("movimentacaocontabil.loteContabil loteContabil")
			.where("movimentacaocontabil.dtlancamento>=?", filtro.getDtlancamentoinicio())
			.where("movimentacaocontabil.dtlancamento<=?", filtro.getDtlancamentofim())
			.where("operacaocontabil=?" , filtro.getOperacaocontabil())
			.where("movimentacaocontabil.tipoLancamento=?", filtro.getTipoLancamento())
			.where("movimentacaocontabil.valor>=?", filtro.getValorde())
			.where("movimentacaocontabil.valor<=?", filtro.getValorate())
			.where("movimentacaocontabil.cdmovimentacaocontabil>=?", filtro.getCdmovimentacaocontabilde())
			.where("movimentacaocontabil.cdmovimentacaocontabil<=?", filtro.getCdmovimentacaocontabilate())
			.whereLikeIgnoreAll("movimentacaocontabil.numero", filtro.getNumero())
			.where("loteContabil.cdLoteContabil = ?", filtro.getCdLoteContabil());
		
		if (filtro.getEmpresa()!=null){
			List<Empresa> listaEmpresa = new ArrayList<Empresa>();
			listaEmpresa.add(filtro.getEmpresa());
			
			if (Boolean.TRUE.equals(filtro.getBuscarDadosFiliais())){
				listaEmpresa.addAll(empresaService.findFiliais(filtro.getEmpresa()));
			}
			
			query.whereIn("empresa.cdpessoa", CollectionsUtil.listAndConcatenate(listaEmpresa, "cdpessoa", ", "));
		}
		
		if (filtro.getContaContabilDebito()!=null){
			query.where("exists (select 1" +
						"		 from Movimentacaocontabil mcdebito" +
						"		 join mcdebito.listaDebito listaDebito" +
						"		 join listaDebito.contaContabil contacontabil" +
						"		 where movimentacaocontabil.cdmovimentacaocontabil=mcdebito.cdmovimentacaocontabil" +
						"		 and contacontabil=?)", filtro.getContaContabilDebito());
		}		
		
		if (filtro.getContaContabilCredito()!=null){
			query.where("exists (select 1" +
						"		 from Movimentacaocontabil mccredito" +
						"		 join mccredito.listaCredito listaCredito" +
						"		 join listaCredito.contaContabil contacontabil" +
						"		 where movimentacaocontabil.cdmovimentacaocontabil=mccredito.cdmovimentacaocontabil" +
						"		 and contacontabil=?)", filtro.getContaContabilCredito());
		}
		
		if (filtro.getCentrocustodebito()!=null){
			query.where("exists (select 1" +
						"		 from Movimentacaocontabil mcdebito" +
						"		 join mcdebito.listaDebito listaDebito" +
						"		 join listaDebito.centrocusto centrocusto" +
						"		 where movimentacaocontabil.cdmovimentacaocontabil=mcdebito.cdmovimentacaocontabil" +
						"		 and centrocusto=?)", filtro.getCentrocustodebito());
		}
		
		if (filtro.getCentrocustocredito()!=null){
			query.where("exists (select 1" +
					"		 from Movimentacaocontabil mccredito" +
					"		 join mccredito.listaCredito listaCredito" +
					"		 join listaCredito.centrocusto centrocusto" +
					"		 where movimentacaocontabil.cdmovimentacaocontabil=mccredito.cdmovimentacaocontabil" +
					"		 and centrocusto=?)", filtro.getCentrocustocredito());
		}
		
		if (filtro.getProjetodebito()!=null){
			query.where("exists (select 1" +
						"		 from Movimentacaocontabil mcdebito" +
						"		 join mcdebito.listaDebito listaDebito" +
						"		 join listaDebito.projeto projeto" +
						"		 where movimentacaocontabil.cdmovimentacaocontabil=mcdebito.cdmovimentacaocontabil" +
						"		 and projeto=?)", filtro.getProjetodebito());
		}
		
		if (filtro.getProjetocredito()!=null){
			query.where("exists (select 1" +
						"		 from Movimentacaocontabil mccredito" +
						"		 join mccredito.listaCredito listaCredito" +
						"		 join listaCredito.projeto projeto" +
						"		 where movimentacaocontabil.cdmovimentacaocontabil=mccredito.cdmovimentacaocontabil" +
						"		 and projeto=?)", filtro.getProjetocredito());
		}
		
		query.orderBy("movimentacaocontabil.dtlancamento, operacaocontabil.nome, movimentacaocontabil.tipoLancamento");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Movimentacaocontabil> query){
		query
			.select("movimentacaocontabil.cdmovimentacaocontabil, movimentacaocontabil.dtlancamento, movimentacaocontabil.tipo," +
					"movimentacaocontabil.tipoLancamento, movimentacaocontabil.numero, movimentacaocontabil.valor, movimentacaocontabil.historico," +
					"movimentacaocontabil.dtaltera, movimentacaocontabil.cdusuarioaltera, " +
					"loteContabil.cdLoteContabil, " +
					"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, operacaocontabil.cdoperacaocontabil, operacaocontabil.nome")
			.leftOuterJoin("movimentacaocontabil.empresa empresa")
			.leftOuterJoin("movimentacaocontabil.operacaocontabil operacaocontabil")
			.leftOuterJoin("movimentacaocontabil.loteContabil loteContabil");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Movimentacaocontabil bean = (Movimentacaocontabil) save.getEntity();
		
		save.saveOrUpdateManaged("listaDebito", "movimentacaocontabildebito");
		save.saveOrUpdateManaged("listaCredito", "movimentacaocontabilcredito");
		
		if (bean.isSalvarMovimentacaocontabilorigem()){
			save.saveOrUpdateManaged("listaMovimentacaocontabilorigem");
		}
		
		super.updateSaveOrUpdate(save);
	}
	
	public List<Movimentacaocontabil> findForCsv(MovimentacaocontabilFiltro filtro) {
		QueryBuilder<Movimentacaocontabil> query = querySined();
		this.updateListagemQuery(query, filtro);
		return query.list();
	}
	
	public List<Movimentacaocontabil> findForListagem(String whereIn) {
		return query()
			.select("movimentacaocontabil.cdmovimentacaocontabil, movimentacaocontabil.dtlancamento, movimentacaocontabil.valor, " +
					"movimentacaocontabil.tipoLancamento, movimentacaocontabil.historico, operacaocontabil.cdoperacaocontabil, operacaocontabil.nome, " +
					"listaCredito.historico, listaDebito.historico ")
			.leftOuterJoin("movimentacaocontabil.operacaocontabil operacaocontabil")
			.leftOuterJoin("movimentacaocontabil.listaCredito listaCredito")
			.leftOuterJoin("movimentacaocontabil.listaDebito listaDebito")
			.whereIn("movimentacaocontabil.cdmovimentacaocontabil", whereIn)
			.list();
	}

	/**
	 * M�todo que busca os dados de movimenta��o cont�bil para gera��o do arquivo de integra��o com sistema dominio
	 *
	 * @param empresa
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Movimentacaocontabil> findForDominiointegracaoLancamentocontabil(Empresa empresa, Date dtinicio, Date dtfim) {
		return query()
				.select("movimentacaocontabil.cdmovimentacaocontabil, movimentacaocontabil.dtlancamento, movimentacaocontabil.valor, " +
						"movimentacaocontabil.historico, movimentacaocontabil.cdusuarioaltera, movimentacaocontabil.dtaltera, " +
						"movimentacaocontabil.tipo, " +
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cpf, empresa.cnpj, " +
						"operacaocontabil.cdoperacaocontabil, operacaocontabil.codigointegracao, " +
						"listaDebito.cdmovimentacaocontabildebitocredito, listaDebito.valor, listaDebito.historico, " +
						"listaCredito.cdmovimentacaocontabildebitocredito, listaCredito.valor, listaCredito.historico, " +
						"cgDebito.cdcontacontabil, cgDebito.codigoalternativo, " +
						"cgCredito.cdcontacontabil, cgCredito.codigoalternativo")
				.leftOuterJoin("movimentacaocontabil.empresa empresa")
				.leftOuterJoin("movimentacaocontabil.operacaocontabil operacaocontabil")
				.leftOuterJoin("movimentacaocontabil.listaDebito listaDebito")
				.leftOuterJoin("movimentacaocontabil.listaCredito listaCredito")
				.leftOuterJoin("listaDebito.contaContabil cgDebito")
				.leftOuterJoin("listaCredito.contaContabil cgCredito")
				.leftOuterJoin("movimentacaocontabil.debito debito")
				.leftOuterJoin("movimentacaocontabil.credito credito")
				.where("empresa = ?", empresa)
				.where("movimentacaocontabil.dtlancamento >= ?", dtinicio)
				.where("movimentacaocontabil.dtlancamento <= ?", dtfim)
				.list();
	}
	
	/**
	 * M�todo que busca os dados de movimenta��o cont�bil para gera��o do arquivo de integra��o com sistema alterdata
	 *
	 * @param empresa
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Movimentacaocontabil> findForALteradataintegracaoLancamentocontabil(Empresa empresa, Date dtinicio, Date dtfim) {
		return querySined()
				
				.select("movimentacaocontabil.cdmovimentacaocontabil, movimentacaocontabil.dtlancamento, movimentacaocontabil.valor, " +
						"movimentacaocontabil.historico, movimentacaocontabil.cdusuarioaltera, movimentacaocontabil.dtaltera, " +
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, movimentacaocontabil.numero, " +
						"operacaocontabil.cdoperacaocontabil, " +
						"debito.cdcontagerencial, debito.codigoalternativo, vcontagerencialdebito.identificador, " +
						"credito.cdcontagerencial, credito.codigoalternativo, vcontagerencialcredito.identificador, " +
						"centrocustodebito.cdcentrocusto, centrocustodebito.nome, centrocustodebito.codigoalternativo, " +
						"centrocustocredito.cdcentrocusto, centrocustocredito.nome, centrocustocredito.codigoalternativo ")
				.leftOuterJoin("movimentacaocontabil.empresa empresa")
				.leftOuterJoin("movimentacaocontabil.operacaocontabil operacaocontabil")
				.leftOuterJoin("movimentacaocontabil.debito debito")
				.leftOuterJoin("debito.vcontagerencial vcontagerencialdebito")
				.leftOuterJoin("movimentacaocontabil.credito credito")
				.leftOuterJoin("credito.vcontagerencial vcontagerencialcredito")
				.leftOuterJoin("movimentacaocontabil.centrocustodebito centrocustodebito")
				.leftOuterJoin("movimentacaocontabil.centrocustocredito centrocustocredito")
				.where("empresa = ?", empresa)
				.where("movimentacaocontabil.dtlancamento >= ?", dtinicio)
				.where("movimentacaocontabil.dtlancamento <= ?", dtfim)
				.list();
	}
	
	public void transactionGerarLancamentoContabeis(final List<Movimentacaocontabil> listaMovimentacaocontabil){
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				for (Movimentacaocontabil movimentacaocontabil: listaMovimentacaocontabil){
					saveOrUpdateNoUseTransaction(movimentacaocontabil);
				}
				return null;
			}
		});
	}
	
	public List<Movimentacaocontabil> findForSAGE(MovimentacaocontabilFiltro filtro) {
		QueryBuilder<Movimentacaocontabil> query = query();
		this.updateListagemQuery(query, filtro);
		query.select("movimentacaocontabil.cdmovimentacaocontabil, movimentacaocontabil.dtlancamento, movimentacaocontabil.historico, movimentacaocontabil.tipo, " +
					 "listaDebito.valor, vcontagerencialdebito.identificador, centrocustodebito.codigoalternativo," +
					 "listaCredito.valor, vcontagerencialcredito.identificador, centrocustocredito.codigoalternativo, " +
					 "operacaocontabil.codigointegracao, ocListaDebito.codigointegracao, ocListaCredito.codigointegracao," +
					 "operacaocontabildebito.codigointegracao, operacaocontabilcredito.codigointegracao ");
		query.leftOuterJoinIfNotExists("movimentacaocontabil.listaDebito listaDebito");
		query.leftOuterJoinIfNotExists("listaDebito.operacaocontabildebitocredito ocListaDebito");
		query.leftOuterJoinIfNotExists("listaDebito.movimentacaocontabildebito movimentacaocontabildebito");
		query.leftOuterJoinIfNotExists("movimentacaocontabildebito.operacaocontabil operacaocontabildebito");
		query.leftOuterJoinIfNotExists("listaDebito.contaContabil contagerencialdebito");
		query.leftOuterJoinIfNotExists("contagerencialdebito.vcontacontabil vcontagerencialdebito");
		query.leftOuterJoinIfNotExists("listaDebito.centrocusto centrocustodebito");
		query.leftOuterJoinIfNotExists("movimentacaocontabil.listaCredito listaCredito");
		query.leftOuterJoinIfNotExists("listaCredito.operacaocontabildebitocredito ocListaCredito");
		query.leftOuterJoinIfNotExists("listaCredito.movimentacaocontabilcredito movimentacaocontabilcredito");
		query.leftOuterJoinIfNotExists("movimentacaocontabilcredito.operacaocontabil operacaocontabilcredito");
		query.leftOuterJoinIfNotExists("listaCredito.contaContabil contagerencialcredito");
		query.leftOuterJoinIfNotExists("contagerencialcredito.vcontacontabil vcontagerencialcredito");
		query.leftOuterJoinIfNotExists("listaCredito.centrocusto centrocustocredito");
		query.leftOuterJoinIfNotExists("movimentacaocontabil.operacaocontabil operacaocontabil");
		
		return query.list();
	}
	
	public Money findForResumoDocumentotipo(MovimentacaocontabilFiltro filtro) {
		QueryBuilder<Movimentacaocontabil> query = query();
		this.updateListagemQuery(query, filtro);
		Money valorTotal = new Money();
	
		query.select("movimentacaocontabil.cdmovimentacaocontabil, movimentacaocontabil.valor");
		
		List<Movimentacaocontabil> listamovimentacaocontabil = query.list();
			for(Movimentacaocontabil movimentacaocontabil: listamovimentacaocontabil){
				valorTotal = valorTotal.add(movimentacaocontabil.getValor());
		}

		return valorTotal; 
	}

	public Double getSumValorForDRE(Empresa empresa, Date dtinicio, Date dtfim, Configuracaodretipo configuracaodretipo, Configuracaodre configuracaodre, Centrocusto centrocusto, Projeto projeto) {
//		String sql = "select sum( " +
//							"case " +
//								"when mci.cdmovimentacaocontabilcredito is not null then (mci.valor/100.0) " +
//								"when mci.cdmovimentacaocontabildebito is not null then ((mci.valor/100.0) * -1.0) " +
//							"end " +
//						") as valor " +
//						"from movimentacaocontabildebitocredito mci " +
//						"join contacontabil cc on cc.cdcontacontabil = mci.cdcontacontabil " +
//						"join vcontacontabil vcc on vcc.cdcontacontabil = cc.cdcontacontabil " +
//						"join movimentacaocontabil mc on (mc.cdmovimentacaocontabil = mci.cdmovimentacaocontabilcredito or mc.cdmovimentacaocontabil = mci.cdmovimentacaocontabildebito)  " +
//						"where 1 = 1 " +
//						"and mc.dtlancamento >= ? " +
//						"and mc.dtlancamento <= ? " +
//						"and mc.cdempresa = ? " +
//						"and exists ( " +
//							"select 1 " +
//							"from configuracaodreitem ci " +
//							"join contacontabil cc2 on cc2.cdcontacontabil = ci.cdcontacontabil " +
//							"join vcontacontabil vcc2 on vcc2.cdcontacontabil = cc2.cdcontacontabil " +
//							"where vcc.identificador like (vcc2.identificador || '%') " +
//							"and ci.cdconfiguracaodretipo = ? " +
//							"and ci.cdconfiguracaodre = ? " +
//						")";
//		
//		Double valor = (Double) getJdbcTemplate().queryForObject(sql, new Object[]{
//				dtinicio,
//				dtfim,
//				empresa.getCdpessoa(),
//				configuracaodretipo.getCdconfiguracaodretipo(),
//				configuracaodre.getCdconfiguracaodre()
//		}, new RowMapper() {
//			@Override
//			public Double mapRow(ResultSet rs, int rowNum) throws SQLException {
//				return rs.getDouble(1);
//			}
//		});
//		
//		return valor;
		
		String sql = "select sum( " +
							"calcular_saldo_conta_analitica(cc.cdcontacontabil, ?, ?, ?, ?)" +
						") / 100.0 as valor " +
						"from contacontabil cc " +
						"join vcontacontabil vcc on vcc.cdcontacontabil = cc.cdcontacontabil " +
						"where 1 = 1 " +
						"and exists ( " +
							"select 1 " +
							"from configuracaodreitem ci " +
							"join contacontabil cc2 on cc2.cdcontacontabil = ci.cdcontacontabil " +
							"join vcontacontabil vcc2 on vcc2.cdcontacontabil = cc2.cdcontacontabil " +
							"where vcc.identificador like (vcc2.identificador || '%') " +
							"and ci.cdconfiguracaodretipo = ? " +
							"and ci.cdconfiguracaodre = ? " +
						")";

		SinedUtil.markAsReader();
		Double valor = (Double) getJdbcTemplate().queryForObject(sql, new Object[]{
				dtfim,
				empresa.getCdpessoa(),
				centrocusto != null ? centrocusto.getCdcentrocusto() : null,
				projeto != null ? projeto.getCdprojeto() : null,
				configuracaodretipo.getCdconfiguracaodretipo(),
				configuracaodre.getCdconfiguracaodre()
		}, new RowMapper() {
			@Override
			public Double mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getDouble(1);
			}
		});
		
		return valor;
	}

	@SuppressWarnings("unchecked")
	public List<GerarDREListagemLancamentosBean> findForDRE(GerarDREFiltro filtro) {
		String sql = "select mc.cdmovimentacaocontabil, " +
						"case " +
							"when mci.cdmovimentacaocontabilcredito is not null then 'C' " +
							"when mci.cdmovimentacaocontabildebito is not null then 'D' " +
						"end as tipo, " +
						"mc.dtlancamento, " +
						"coalesce(mci.historico, mc.historico) as historico, " +
						"mci.valor " +
						"from movimentacaocontabildebitocredito mci " +
						"join movimentacaocontabil mc on (mc.cdmovimentacaocontabil = mci.cdmovimentacaocontabilcredito or mc.cdmovimentacaocontabil = mci.cdmovimentacaocontabildebito) " +
						"where 1 = 1 " +
						"and mc.dtlancamento >= ? " +
						"and mc.dtlancamento <= ? " +
						"and mc.cdempresa = ? " +
						"and mci.cdcontacontabil = ? ";
		
						if(Util.objects.isPersistent(filtro.getCentrocusto())){
							sql = sql + "and mci.cdcentrocusto = " + filtro.getCentrocusto().getCdcentrocusto().toString();
						}
						if(filtro.getProjeto() != null && filtro.getProjeto().getCdprojeto() != null){
							sql = sql + "and mci.cdprojeto = " + filtro.getProjeto().getCdprojeto().toString();
						}
		return (List<GerarDREListagemLancamentosBean>) getJdbcTemplate().query(sql, new Object[]{
				filtro.getDtinicio(),
				filtro.getDtfim(),
				filtro.getEmpresa().getCdpessoa(),
				filtro.getContacontabil().getCdcontacontabil()
		}, new RowMapper() {
			@Override
			public GerarDREListagemLancamentosBean mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new GerarDREListagemLancamentosBean(
							rs.getInt("cdmovimentacaocontabil"), 
							rs.getString("tipo"),
							rs.getDate("dtlancamento"), 
							rs.getString("historico"),
							new Money(rs.getLong("valor"), true));
			}
		});
	}
	
	
	
	public Money calcularSaldoContaContabil(Integer cdcontagerencial, Date dataReferencia, Empresa empresa, Centrocusto centrocusto, Projeto projeto) {
		if(dataReferencia == null){
			throw new SinedException("O par�metro dataReferencia n�o pode ser null.");
		}
		
		String sql = "SELECT calcular_saldo_conta_analitica(" + cdcontagerencial + ", '" + dataReferencia + "', " + 
															(empresa != null ? empresa.getCdpessoa() : null) + ", " + 
															(centrocusto != null ?  + centrocusto.getCdcentrocusto() : null) + ", " + 
															(projeto != null ? projeto.getCdprojeto() : null) + ");";
		return this.queryForMoney(sql);
	}
	
	
	/**
	 * Consulta SQL que resulta em um valor Money.
	 * � esperado que a consulta resulte em um valor long, o mesmo ser� transformado em Money, considerando duas casas decimais.
	 */
	private Money queryForMoney(String sql){
		
		Long result = getJdbcTemplate().queryForLong(sql);
		
		return new Money(result, true);
	}
	
	//repetir esse m�todo?? acrescentar campos Documento, historico
	public List<Movimentacaocontabil> findForRelatorioLivroRazao(EmitirLivroRazaoFiltro filtro) {
		QueryBuilder<Movimentacaocontabil> query = querySined();
		
		query
			.select("movimentacaocontabil.cdmovimentacaocontabil, movimentacaocontabil.numero, movimentacaocontabil.historico, movimentacaocontabil.dtlancamento, " +
					"contaContabilDebito.cdcontacontabil, contaContabilDebito.nome, contaContabilDebito.contaretificadora, contaContabilDebito.saldoInicial, " +
					"contaContabilDebito.dtSaldoInicial,  vcontaContabilDebito.identificador, vcontaContabilDebito.nivel, " +
					"vcontaContabilDebito.arvorepai, contaContabilDebito.naturezaContaCompensacao, centrocustodebito.cdcentrocusto, projetoDebito.cdprojeto, " +
					"contaContabilCredito.cdcontacontabil, contaContabilCredito.nome, contaContabilCredito.contaretificadora, contaContabilCredito.saldoInicial, contaContabilCredito.dtSaldoInicial, " +
					" vcontaContabilCredito.identificador, vcontaContabilCredito.nivel, " +
					"vcontaContabilCredito.arvorepai, contaContabilCredito.naturezaContaCompensacao, centrocustocredito.cdcentrocusto, projetoCredito.cdprojeto, " +
					"listaDebito.valor, listaCredito.valor")
			.leftOuterJoin("movimentacaocontabil.listaDebito listaDebito")
			.leftOuterJoin("listaDebito.contaContabil contaContabilDebito")
			.leftOuterJoin("contaContabilDebito.vcontacontabil vcontaContabilDebito")			
			.leftOuterJoin("movimentacaocontabil.listaCredito listaCredito")
			.leftOuterJoin("listaCredito.contaContabil contaContabilCredito")
			.leftOuterJoin("contaContabilCredito.vcontacontabil vcontaContabilCredito")				
			.leftOuterJoin("movimentacaocontabil.empresa empresa")	
			.leftOuterJoin("listaDebito.centrocusto centrocustodebito")
			.leftOuterJoin("listaCredito.centrocusto centrocustocredito")
			.leftOuterJoin("listaDebito.projeto projetoDebito")
			.leftOuterJoin("listaCredito.projeto projetoCredito")
			.where("empresa = ?", filtro.getEmpresa())
			.openParentheses()
				.where("contaContabilDebito = ?", filtro.getContaContabil())
				.or()
				.where("contaContabilCredito = ?", filtro.getContaContabil())
			.closeParentheses()
			.openParentheses()
				.where("centrocustodebito = ?", filtro.getCentroCusto())
				.or()
				.where("centrocustocredito = ?", filtro.getCentroCusto())
			.closeParentheses()
			.openParentheses()
				.where("projetoDebito = ?", filtro.getProjeto())
				.or()
				.where("projetoCredito = ?", filtro.getProjeto())
			.closeParentheses()
			.where("movimentacaocontabil.dtlancamento >= ?", filtro.getDtInicio())
			.where("movimentacaocontabil.dtlancamento <= ?", filtro.getDtFim())
			.orderBy("movimentacaocontabil.dtlancamento")
			.list();
		
		return query.list();
	}	
	
	public List<Movimentacaocontabil> findForBalancoPatrimonial(EmitirBalancoPatrimonialFiltro filtro) {
		QueryBuilder<Movimentacaocontabil> query = querySined();
		
		query
	        .select("contaContabilDebito.cdcontacontabil, contaContabilDebito.nome, contaContabilDebito.contaretificadora, contaContabilDebito.saldoInicial, contaContabilDebito.dtSaldoInicial, " +
	        		"vcontacontabildebito.identificador, vcontacontabildebito.nivel, vcontacontabildebito.arvorepai, contaContabilDebito.naturezaContaCompensacao, " +
	                "contaContabilCredito.cdcontacontabil, contaContabilCredito.nome, contaContabilCredito.contaretificadora, contaContabilCredito.saldoInicial, contaContabilCredito.dtSaldoInicial, " +
	                "vcontacontabilcredito.identificador, vcontacontabilcredito.nivel, vcontacontabilcredito.arvorepai, contaContabilCredito.naturezaContaCompensacao, " +
	                "listaDebito.valor, listaCredito.valor, movimentacaocontabil.dtlancamento")
	        .leftOuterJoin("movimentacaocontabil.listaDebito listaDebito")
	        .leftOuterJoin("listaDebito.contaContabil contaContabilDebito")
	        .leftOuterJoin("contaContabilDebito.vcontacontabil vcontacontabildebito")            
	        .leftOuterJoin("movimentacaocontabil.listaCredito listaCredito")
	        .leftOuterJoin("listaCredito.contaContabil contaContabilCredito")
	        .leftOuterJoin("contaContabilCredito.vcontacontabil vcontacontabilcredito")                
	        .leftOuterJoin("movimentacaocontabil.empresa empresa")    
	        .leftOuterJoin("listaDebito.centrocusto centrocustodebito")
	        .leftOuterJoin("listaCredito.centrocusto centrocustocredito")	        
			.leftOuterJoin("listaDebito.projeto projetoDebito")
			.leftOuterJoin("listaCredito.projeto projetoCredito")
			.where("empresa = ?", filtro.getEmpresa())
			.openParentheses()
				.where("contaContabilDebito = ?", filtro.getContaContabil())
				.or()
				.where("contaContabilCredito = ?", filtro.getContaContabil())
			.closeParentheses()
			.openParentheses()
				.where("centrocustodebito = ?", filtro.getCentroCusto())
				.or()
				.where("centrocustocredito = ?", filtro.getCentroCusto())
			.closeParentheses()
			.openParentheses()
				.where("projetoDebito = ?", filtro.getProjeto())
				.or()
				.where("projetoCredito = ?", filtro.getProjeto())
			.closeParentheses()
			.where("movimentacaocontabil.dtlancamento >= ?", filtro.getDtInicio())
			.where("movimentacaocontabil.dtlancamento <= ?", filtro.getDtFim())
			.list();
		
		return query.list();
	}

	public Boolean haveMovimentacaoContabil(String identificador) {
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.from(Movimentacaocontabil.class)
			.join("movimentacaocontabil.listaDebito listaDebito")
			.join("listaDebito.contaContabil contaContabilDebito")
			.join("contaContabilDebito.vcontacontabil vcontacontabilDebito")
			.join("movimentacaocontabil.listaCredito listaCredito")
			.join("listaCredito.contaContabil contaContabilCredito")
			.join("contaContabilCredito.vcontacontabil vcontacontabilCredito")
			.openParentheses()
				.where("vcontacontabilDebito.identificador like '" + identificador + "%'")
				.or()
				.where("vcontacontabilCredito.identificador like '" + identificador + "%'")
			.closeParentheses()
			.unique() > 0;
	}
	
	public List<Movimentacaocontabil> carregarMovimentacoesContabeisParaValidarExclusao(String whereIn) {
		return query()
				.select("movimentacaocontabil.cdmovimentacaocontabil, movimentacaocontabil.dtlancamento, empresa.cdpessoa")
				.leftOuterJoin("movimentacaocontabil.empresa empresa")
				.whereIn("movimentacaocontabil.cdmovimentacaocontabil", whereIn)
				.list();
	}
	
	public List<Movimentacaocontabil> carregarMovimentacoesContabeisPorLote(String whereInLoteContabil) {
		return query()
				.select("movimentacaocontabil.cdmovimentacaocontabil, movimentacaocontabil.dtlancamento, empresa.cdpessoa")
				.leftOuterJoin("movimentacaocontabil.empresa empresa")
				.whereIn("movimentacaocontabil.loteContabil", whereInLoteContabil)
				.list();
	}
	
}