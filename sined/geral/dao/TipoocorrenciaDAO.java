package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Tipoocorrencia;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("tipoocorrencia.nome")
public class TipoocorrenciaDAO extends GenericDAO<Tipoocorrencia>{

	/**
	 * Carrega a lista de periodicidade de ocorr�ncia dos itens de composi��o para ser exibida no m�dulo de or�amentos em Flex.
	 *
	 * @return List<Tipoocorrencia>
	 * @author Rodrigo Alvarenga
	 */
	public List<Tipoocorrencia> findAllForFlex(){
		return query()
			.select("tipoocorrencia.cdtipoocorrencia, tipoocorrencia.nome")
			.orderBy("tipoocorrencia.nome")
			.list();
	}
}
