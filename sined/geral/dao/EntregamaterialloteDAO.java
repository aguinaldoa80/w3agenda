package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Entregamateriallote;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EntregamaterialloteDAO extends GenericDAO<Entregamateriallote> {

	
	/**
	* M�todo que retorna os lotes do item de uma entrada fiscal
	*
	* @param entregamaterial
	* @return
	* @since 27/10/2016
	* @author Luiz Fernando
	*/
	public List<Entregamateriallote> findByEntregamaterial(Entregamaterial entregamaterial) {
		if (entregamaterial == null || entregamaterial.getCdentregamaterial() == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		return 
			query()
				.select("entregamateriallote.cdentregamateriallote, entregamateriallote.qtde, " +
						"entregamaterial.cdentregamaterial, " +
						"loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade ")
				.join("entregamateriallote.loteestoque loteestoque")
				.join("entregamateriallote.entregamaterial entregamaterial")
				.where("entregamateriallote.entregamaterial = ?", entregamaterial)
				.orderBy("entregamateriallote.cdentregamateriallote")
				.list();
	}
	
	/**
	* M�todo que retorna os lotes do item de uma entrada fiscal 
	*
	* @param entregadocumento
	* @return
	* @since 27/10/2016
	* @author Luiz Fernando
	*/
	public List<Entregamateriallote> findByEntregadocumento(Entregadocumento entregadocumento) {
		if (entregadocumento == null || entregadocumento.getCdentregadocumento() == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		return 
			query()
				.select("entregamateriallote.cdentregamateriallote, entregamateriallote.qtde, " +
						"entregamaterial.cdentregamaterial, entregamaterial.pesomedio, " +
						"loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, " +
						"materialgrupo.registrarpesomedio ")
				.join("entregamateriallote.loteestoque loteestoque")
				.join("entregamateriallote.entregamaterial entregamaterial")
				.leftOuterJoin("entregamaterial.material material")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.where("entregamaterial.entregadocumento = ?", entregadocumento)
				.orderBy("entregamateriallote.cdentregamateriallote")
				.list();
	}
	
	
}