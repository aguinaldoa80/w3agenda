package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Tipodocumentomilitar;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TipodocumentomilitarDAO extends GenericDAO<Tipodocumentomilitar> {
	
	/* singleton */
	private static TipodocumentomilitarDAO instance;
	public static TipodocumentomilitarDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(TipodocumentomilitarDAO.class);
		}
		return instance;
	}
		
}
