package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.service.TaxaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ContacorrenteFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContacorrenteDAO extends GenericDAO<Conta>{
	
	private TaxaService taxaService;
	
	public void setTaxaService(TaxaService taxaService) {
		this.taxaService = taxaService;
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Conta> query) {
		query
			 .select("conta.cdconta, conta.nome, conta.numero, conta.dvnumero, conta.agencia, conta.dvagencia, conta.dtsaldo, conta.saldo, conta.desconsiderarsaldoinicialfluxocaixa, " +
			     	 "conta.ativo, conta.limite, conta.diavencimento, conta.diafechamento, conta.cdusuarioaltera, conta.dtaltera, conta.titular, " +
			     	 "empresatitular.cdpessoa, empresatitular.nome, empresatitular.nomefantasia, empresatitular.razaosocial, empresatitular.cnpj, " +
					 "conta.sequencial, conta.sequencialpagamento, conta.sequencialcheque, conta.conciliacaoautomatica, conta.taxaboleto, conta.codigoempresasispag, " +
					 "conta.nossonumerointervalo, conta.nossonumeroinicial, conta.nossonumerofinal, conta.localpagamento, conta.considerarnumerodocumentoremessa, " +
					 "contatipo.cdcontatipo, contatipo.nome, " +
					 "banco.cdbanco, banco.nome, banco.numero, banco.gerarsispag, " +
					 "empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
					 "contapapel.cdcontapapel, " +
					 "papel.cdpapel, papel.nome, papel.descricao, " +
					 "contacarteira.cdcontacarteira, contacarteira.convenio, contacarteira.conveniolider, contacarteira.carteira, contacarteira.carteiracarne, " +
					 "contacarteira.codigocarteira, contacarteira.tipoarquivoremessa, contacarteira.fatorvencimentozerado, contacarteira.msgboleto1, " +
					 "contacarteira.msgboleto2, contacarteira.bancogeranossonumero, " +
					 "contacarteira.idemissaoboleto, contacarteira.idemissaoboletoimpresso, contacarteira.iddistribuicao, contacarteira.tipocobranca, " +
					 "contacarteira.instrucao1, contacarteira.instrucao2, contacarteira.qtdedias, " +
					 "contacarteira.especiedocumento, contacarteira.codigotransmissao, contacarteira.especiedocumentoremessa, contacarteira.aceito, " +					 
					 "contacarteira.protestoautomaticoremessa, contacarteira.enviardescontoremessa, contacarteira.enviarmultaremessa, contacarteira.enviarmoraremessa, " +
					 "contacarteira.padrao, contacarteira.variacaocarteira, contacarteira.boletocobranca, contacarteira.aprovadoproducao, contacarteira.operacao, " +
					 "contacarteira.escritural, contacarteira.limiteposicoesintervalonossonumero, contacarteira.nossonumeroindividual, " +
					 "bancoConfiguracaoRemessa.cdbancoconfiguracaoremessa, bancoConfiguracaoRemessa.nome, " +
					 "bancoConfiguracaoRetorno.cdbancoconfiguracaoretorno, bancoConfiguracaoRetorno.nome, " +
					 "bancoConfiguracaoRemessaPagamento.cdbancoconfiguracaoremessa, bancoConfiguracaoRemessaPagamento.nome, " +
					 "bancoConfiguracaoRetornoPagamento.cdbancoconfiguracaoretorno, bancoConfiguracaoRetornoPagamento.nome, " +
					 "operacaocontabil.cdoperacaocontabil, operacaocontabil.nome, contacontabil.cdcontacontabil, " +
					 "taxa.cdtaxa, listaTaxaitem.valor, listaTaxaitem.percentual, listaTaxaitem.dias, listaTaxaitem.aodia, " +
					 "listaTaxaitem.gerarmensagem, listaTaxaitem.motivo, tipotaxa.cdtipotaxa, tipotaxa.nome, listaTaxaitem.tipocalculodias," +
			 		 "listaContaprojeto.cdcontaprojeto, projeto.cdprojeto, projeto.nome, contacarteira.qtdediasdevolucao, contacarteira.qtdediasdistribuicao, " +
			 		 "contacarteira.naoassociarcontareceber, contacarteira.naogerarboleto ")
			 .leftOuterJoin("conta.banco banco")
			 .leftOuterJoin("conta.empresatitular empresatitular")
			 .leftOuterJoin("conta.listaContaempresa listaContaempresa")
			 .leftOuterJoin("listaContaempresa.empresa empresa")
			 .leftOuterJoin("conta.contatipo contatipo")
			 .leftOuterJoin("conta.listaContacarteira contacarteira")
			 .leftOuterJoin("contacarteira.bancoConfiguracaoRemessa bancoConfiguracaoRemessa")
			 .leftOuterJoin("contacarteira.bancoConfiguracaoRetorno bancoConfiguracaoRetorno")
			 .leftOuterJoin("contacarteira.bancoConfiguracaoRemessaPagamento bancoConfiguracaoRemessaPagamento")
			 .leftOuterJoin("contacarteira.bancoConfiguracaoRetornoPagamento bancoConfiguracaoRetornoPagamento")
			 .leftOuterJoin("conta.listaContapapel contapapel")
			 .leftOuterJoin("contapapel.papel papel")
			 .leftOuterJoin("conta.operacaocontabil operacaocontabil")
			 .leftOuterJoin("conta.contaContabil contacontabil")
			 .leftOuterJoin("conta.taxa taxa")
			 .leftOuterJoin("taxa.listaTaxaitem listaTaxaitem")
			 .leftOuterJoin("listaTaxaitem.tipotaxa tipotaxa")
			 .leftOuterJoin("conta.listaContaprojeto listaContaprojeto")
			 .leftOuterJoin("listaContaprojeto.projeto projeto");	
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Conta> query,FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		}
		ContacorrenteFiltro filtro = (ContacorrenteFiltro) _filtro;
		
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		query
			.select("conta.cdconta, conta.nome, conta.banco, conta.agencia, conta.numero, conta.ativo, conta.desconsiderarsaldoinicialfluxocaixa, " +
					"view.saldoatual, listaContapapel.cdcontapapel, papel.cdpapel, papel.nome")
			.leftOuterJoin("conta.vconta view")
			.leftOuterJoin("conta.listaContaempresa listaContaempresa")
			.leftOuterJoin("listaContaempresa.empresa empresa")
			.leftOuterJoin("conta.banco banco")
			.leftOuterJoin("conta.listaContapapel listaContapapel")
			.leftOuterJoin("listaContapapel.papel papel")
			.where("conta.contatipo.cdcontatipo=?",Contatipo.CONTA_BANCARIA)
			.where("banco=?",filtro.getBanco())
			.where("empresa=?",filtro.getEmpresa())
			.whereLikeIgnoreAll("conta.agencia", filtro.getAgencia())
			.whereLikeIgnoreAll("conta.numero", filtro.getNumero())
			.where("conta.ativo = ?",filtro.getAtivo())
			.where("coalesce(conta.desconsiderarsaldoinicialfluxocaixa, false) = ?",filtro.getDesconsiderarsaldoinicialfluxocaixa());
		
			if (!administrador){
				if (whereInPapel != null && !whereInPapel.equals("")){
					query.openParentheses();
					query.whereIn("papel.cdpapel", whereInPapel)
					.or()
					.where("papel.cdpapel is null");
					query.closeParentheses();
				} else {
					query.where("papel.cdpapel is null");
				}
			}
			query.orderBy("conta.nome")
			.ignoreAllJoinsPath(true)
		;
	}
	
	/**
	 * M�todo para obter o n�mero de contas correntes.
	 * Compara o banco, a ag�ncia e o n�mero da conta atual.
	 * 
	 * @param conta
	 * @return
	 * @author Flavio Tavares
	 */
	public Long findTotalContasIguais(Conta conta){
		if(conta==null){
			throw new SinedException("O par�metro contacorrente n�o pode ser null.");
		}
		
		return newQueryBuilderWithFrom(Long.class)
			.select("count(*)")
			.where("conta.contatipo.cdcontatipo = ?",Contatipo.CONTA_BANCARIA)
			.where("conta.cdconta <> ?",conta.getCdconta())
			.where("conta.banco = ?",conta.getBanco())
			.where("conta.agencia = ?",conta.getAgencia())
			.where("conta.numero = ?",conta.getNumero())
			.unique();
	}

	public Conta findByAgenciaContaBanco(String cdbanco, String cdagencia, String cdcedente) {
		if(cdbanco == null || cdagencia == null || cdcedente == null){
			throw new SinedException("Erro na obten��o da conta banc�ria.");
		}
		return query()
					.select("conta.cdconta, conta.nome")
					.join("conta.banco banco")
					.leftOuterJoin("conta.listaContacarteira listaContacarteira")
					.whereLike("listaContacarteira.convenio", cdcedente)
					.whereLike("conta.agencia", cdagencia)
					.where("banco.numero = ?", Integer.parseInt(cdbanco))
					.unique();
	}
	
	public Conta findByAgenciaNumeroBanco(String cdbanco, String cdagencia, String cdconta) {
		if(cdagencia == null || cdconta == null){
			throw new SinedException("Erro na obten��o da conta banc�ria.");
		}
		QueryBuilder<Conta> query = query()
					.select("conta.cdconta, conta.nome, conta.considerarnumerodocumentoremessa")
					.join("conta.banco banco")
					.whereLike("conta.numero", cdconta)
					.whereLike("conta.agencia", cdagencia);
		if(cdbanco != null){
			query.where("banco.numero = ?", Integer.parseInt(cdbanco));
		}
		
		return query.unique();
	}
	
	public Conta findForRetorno(String cdbanco, String cdagencia, String cdconta) {
		if(cdbanco == null || cdagencia == null || cdconta == null){
			throw new SinedException("Erro na obten��o da conta banc�ria.");
		}
		List<Conta> list = query()
		.select("conta.cdconta, conta.nome")
		.join("conta.banco banco")
		.leftOuterJoin("conta.listaContacarteira listaContacarteira")
		.openParentheses()
		.whereLike("conta.numero", cdconta)
		.or()
		.whereLike("listaContacarteira.convenio", cdconta)
		.closeParentheses()
		.whereLike("conta.agencia", cdagencia)
		.where("banco.numero = ?", Integer.parseInt(cdbanco))
		.list();
		
		return list != null && list.size() > 0 ? list.get(0) : null;
	}

	/**
	 * 
	 * M�todo que busca o valor da taxa do boleto na conta.
	 *
	 *@author Thiago Augusto
	 *@date 14/03/2012
	 * @param documento
	 * @return
	 */
	public Money getValorTaxaBoleto(Conta conta){
		
		Money valor =  newQueryBuilderSined(Money.class).removeUseWhere()
		.select("conta.taxaboleto")
		.setUseTranslator(false)
		.from(Conta.class)
		.where("conta = ?", conta)
		.unique();
		
		return valor;
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				Conta bean = (Conta) save.getEntity();
				Taxa taxas = bean.getTaxa();
				if(taxas != null){
					taxaService.saveOrUpdateNoUseTransaction(taxas);
				}
				save.saveOrUpdateManaged("listaContapapel");
				save.saveOrUpdateManaged("listaContacarteira");
				save.saveOrUpdateManaged("listaContaempresa");
				save.saveOrUpdateManaged("listaContaprojeto");
				return null;
			}
		});
	}

	/**
	 * Carregar a conta com os dados necess�rios para fazer a atualiza��o da Taxa e mensagens do
	 * boleto da conta a receber.
	 * @param contacorrente - conta a ser carregada.
	 * @return conta com taxa e itens da taxa.
	 */
	public Conta loadContaAlterContaReceberLote(Conta conta) {
		if(conta == null || conta.getCdconta() == null)
			throw new SinedException("par�metro inv�lido.");
			
		return query()
				.select("conta.cdconta, taxa.cdtaxa, taxaitem.cdtaxaitem, taxaitem.tipotaxa, " +
						"taxaitem.tipocalculodias, taxaitem.gerarmensagem, taxaitem.percentual, taxaitem.valor, taxaitem.dtlimite, taxaitem.dias")
				.leftOuterJoin("conta.taxa taxa")
				.leftOuterJoin("taxa.listaTaxaitem taxaitem")
				.where("conta = ?", conta)
				.unique();
	} 
}
