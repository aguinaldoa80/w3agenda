package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Contratomateriallocacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContratomateriallocacaoDAO extends GenericDAO<Contratomateriallocacao> {

	/**
	 * M�todo que atualiza a qtde locada do item do contrato
	 *
	 * @param contratomateriallocacao
	 * @param qtde
	 * @author Luiz Fernando
	 */
	public void updateQtdeLocacao(Contratomateriallocacao contratomateriallocacao, Double qtde) {
		if(contratomateriallocacao != null && contratomateriallocacao.getCdcontratomateriallocacao() != null){
			getHibernateTemplate()
			.bulkUpdate("update Contratomateriallocacao cml set cml.qtde = ?, cml.dtaltera = ? " +
					"where cml.cdcontratomateriallocacao = " + contratomateriallocacao.getCdcontratomateriallocacao(), 
					new Object[]{qtde, new Timestamp(System.currentTimeMillis())});
		}
	}
	
	public List<Contratomateriallocacao> findByContratomaterial(Contratomaterial contratomaterial){
		if(!Util.objects.isPersistent(contratomaterial))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("contratomateriallocacao.cdcontratomateriallocacao, contratomateriallocacao.substituicao, " +
						"contratomateriallocacao.dtmovimentacao, contratomateriallocacao.contratomateriallocacaotipo, " +
						"contratomateriallocacao.qtde")
				.join("contratomateriallocacao.contratomaterial contratomaterial")
				.where("contratomaterial = ?", contratomaterial)
				.list();
	}
}