package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Documentodigital;
import br.com.linkcom.sined.geral.bean.Documentodigitaldestinatario;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentodigitaldestinatarioDAO extends GenericDAO<Documentodigitaldestinatario>{

	public void updateToken(Documentodigitaldestinatario documentodigitaldestinatario, String token) {
		getHibernateTemplate().bulkUpdate("update Documentodigitaldestinatario d set d.token = ? where d = ?", new Object[]{token, documentodigitaldestinatario});
	}

	public boolean haveToken(String token) {
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.where("documentodigitaldestinatario.token = ?", token)
				.unique() > 0;
	}

	public void updateDtenvio(Documentodigitaldestinatario documentodigitaldestinatario, Timestamp dtenvio) {
		getHibernateTemplate().bulkUpdate("update Documentodigitaldestinatario d set d.dtenvio = ? where d = ?", new Object[]{dtenvio, documentodigitaldestinatario});
	}

	public Documentodigitaldestinatario loadByToken(String token) {
		return query()
					.select("documentodigitaldestinatario.cddocumentodigitaldestinatario, documentodigitaldestinatario.token,  documentodigitaldestinatario.dtaceite, " +
							"documentodigital.cddocumentodigital, documentodigital.hash, arquivo.cdarquivo, arquivo.tipoconteudo, arquivo.nome, " +
							"documentodigitalmodelo.cddocumentodigitalmodelo, documentodigitalmodelo.textotelaaceite, " +
							"documentodigitalusuario.cddocumentodigitalusuario, documentodigitalusuario.email, documentodigitalusuario.nome, " +
							"documentodigitalusuario.cpf, documentodigitalusuario.dtnascimento")
					.join("documentodigitaldestinatario.documentodigital documentodigital")
					.join("documentodigital.arquivo arquivo")
					.join("documentodigital.documentodigitalmodelo documentodigitalmodelo")
					.join("documentodigitaldestinatario.documentodigitalusuario documentodigitalusuario")
					.where("documentodigitaldestinatario.token = ?", token)
					.unique();
	}

	public void updateUseragentaceite(Documentodigitaldestinatario documentodigitaldestinatario, String useragent) {
		getHibernateTemplate().bulkUpdate("update Documentodigitaldestinatario d set d.useragentaceite = ? where d = ?", new Object[]{useragent, documentodigitaldestinatario});
	}

	public void updateIpaceite(Documentodigitaldestinatario documentodigitaldestinatario, String ip) {
		getHibernateTemplate().bulkUpdate("update Documentodigitaldestinatario d set d.ipaceite = ? where d = ?", new Object[]{ip, documentodigitaldestinatario});
	}

	public void updateDtaceite(Documentodigitaldestinatario documentodigitaldestinatario, Timestamp dtaceite) {
		getHibernateTemplate().bulkUpdate("update Documentodigitaldestinatario d set d.dtaceite = ? where d = ?", new Object[]{dtaceite, documentodigitaldestinatario});
	}

	public List<Documentodigitaldestinatario> findByDocumentodigital(Documentodigital documentodigital) {
		return query()
				.select("documentodigitaldestinatario.cddocumentodigitaldestinatario, documentodigitaldestinatario.dtaceite")
				.where("documentodigitaldestinatario.documentodigital = ?", documentodigital)
				.list(); 
	}

	public Documentodigitaldestinatario loadForReenvio(Documentodigitaldestinatario documentodigitaldestinatario) {
		return query()
				.select("documentodigitaldestinatario.cddocumentodigitaldestinatario, documentodigitaldestinatario.token, documentodigital.cddocumentodigital")
				.join("documentodigitaldestinatario.documentodigital documentodigital")
				.where("documentodigitaldestinatario = ?", documentodigitaldestinatario)
				.unique();
	}

	public Documentodigitaldestinatario loadForEnvioEmailObservadores(Documentodigitaldestinatario documentodigitaldestinatario) {
		return query()
				.select("documentodigitaldestinatario.cddocumentodigitaldestinatario, documentodigitaldestinatario.token,  " +
						"documentodigitaldestinatario.dtaceite, documentodigitaldestinatario.dtenvio, documentodigitaldestinatario.ipaceite, " +
						"documentodigitaldestinatario.useragentaceite, " +
						"documentodigitalusuario.cddocumentodigitalusuario, documentodigitalusuario.email, documentodigitalusuario.nome, " +
						"documentodigitalusuario.cpf, documentodigitalusuario.dtnascimento")
				.join("documentodigitaldestinatario.documentodigitalusuario documentodigitalusuario")
				.where("documentodigitaldestinatario = ?", documentodigitaldestinatario)
				.unique();
	}

}
