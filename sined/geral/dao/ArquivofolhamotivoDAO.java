package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Arquivofolhamotivo;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ArquivofolhamotivoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("arquivofolhamotivo.descricao")
public class ArquivofolhamotivoDAO extends GenericDAO<Arquivofolhamotivo>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Arquivofolhamotivo> query, FiltroListagem _filtro) {
		ArquivofolhamotivoFiltro filtro = (ArquivofolhamotivoFiltro) _filtro;
		
		query
			.select("arquivofolhamotivo.cdarquivofolhamotivo, arquivofolhamotivo.descricao")
			.whereLikeIgnoreAll("arquivofolhamotivo.descricao", filtro.getDescricao());
	}
	
}
