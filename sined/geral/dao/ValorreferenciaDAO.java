package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Valorreferencia;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ValorreferenciaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("valorreferencia.cdvalorreferencia")
public class ValorreferenciaDAO extends GenericDAO<Valorreferencia> {

	@Override
	public void updateEntradaQuery(QueryBuilder<Valorreferencia> query) {
		query
			.select("valorreferencia.cdvalorreferencia, valorreferencia.valor, planejamento.cdplanejamento, projeto.cdprojeto, " +
					"material.cdmaterial, material.nome, cargo.cdcargo, cargo.nome, valorreferencia.cdusuarioaltera, valorreferencia.dtaltera")
			.join("valorreferencia.planejamento planejamento")
			.join("planejamento.projeto projeto")
			.leftOuterJoin("valorreferencia.material material")
			.leftOuterJoin("valorreferencia.cargo cargo");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Valorreferencia> query, FiltroListagem _filtro) {
		ValorreferenciaFiltro filtro = (ValorreferenciaFiltro) _filtro;
			
		query
			.select("valorreferencia.cdvalorreferencia, valorreferencia.valor, material.nome, cargo.nome, planejamento.descricao")
			.leftOuterJoin("valorreferencia.material material")
			.leftOuterJoin("valorreferencia.cargo cargo")
			.leftOuterJoin("valorreferencia.planejamento planejamento")
			.where("planejamento = ?",filtro.getPlanejamento())
			.where("cargo = ?",filtro.getCargo())
			.where("material = ?",filtro.getMaterial())
			.where("planejamento.projeto = ?",filtro.getProjeto());
	}

	/**
	 * Carrega a listagem de valores de refer�ncia de um planejamento.
	 *
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Valorreferencia> findByPlanejamento(Planejamento planejamento) {
		if (planejamento == null || planejamento.getCdplanejamento() == null) {
			throw new SinedException("Planejamento n�o pode ser nulo.");
		}
		return query()
					.select("planejamento.cdplanejamento, material.cdmaterial, cargo.cdcargo, valorreferencia.valor")
					.join("valorreferencia.planejamento planejamento")
					.leftOuterJoin("valorreferencia.material material")
					.leftOuterJoin("valorreferencia.cargo cargo")
					.where("planejamento = ?",planejamento)
					.list();
	}
	
	/**
	 * M�todo para obter lista de valores de referencia.
	 * 
	 * @param filtro
	 * @return
	 * @author Leandro Lima
	 */
	public List<Valorreferencia> findForReport(ValorreferenciaFiltro filtro){		
		
		if(filtro == null){
			throw new SinedException("O par�metro filtro n�o pode ser null.");
		}	
		
		String orderBy = filtro.getOrderBy();
		if (orderBy == null || orderBy.isEmpty())
			orderBy = "valorreferencia.cdvalorreferencia ";
		else{
			if (orderBy.contains("material")){
				orderBy += ",cargo.cdcargo,valorreferencia.cdvalorreferencia,material.cdmaterial";
			}else if (orderBy.contains("cargo")){
				orderBy += ",planejamento.descricao desc,material.nome asc";
			}else if (orderBy.contains("planejamento")){
				orderBy += ",cargo.cdcargo,valorreferencia.cdvalorreferencia,material.cdmaterial";
			}
		}
				
		return 
		query()
		      .select("planejamento.descricao, material.nome, cargo.nome, valorreferencia.valor")		      
		      .leftOuterJoin("valorreferencia.planejamento planejamento")
		      .leftOuterJoin("planejamento.projeto projeto")
		      .leftOuterJoin("valorreferencia.material material")
		      .leftOuterJoin("valorreferencia.cargo cargo")
		      .where("projeto = ?", filtro.getProjeto())
		      .where("planejamento = ?", filtro.getPlanejamento())
		      .where("material = ?", filtro.getMaterial())
		      .where("cargo = ?", filtro.getCargo())
		      .where("planejamento.projeto = ?",filtro.getProjeto())
		      .orderBy(orderBy)
		      .list();
	}
	
}
