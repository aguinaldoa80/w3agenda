package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Menu;
import br.com.linkcom.sined.geral.bean.Menumodulo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MenuFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("retira_acento(upper(menu.nome))")
public class MenuDAO extends GenericDAO<Menu> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Menu> query, FiltroListagem _filtro) {
		MenuFiltro filtro = (MenuFiltro) _filtro;
		
		query
			.select("menu.cdmenu, menu.chave, menu.nome, menu.url, menumodulo.nome")
			.leftOuterJoin("menu.menumodulo menumodulo")
			.whereLikeIgnoreAll("menu.nome", filtro.getNome())
			.whereLikeIgnoreAll("menu.url", filtro.getUrl())
			.where("menumodulo = ?", filtro.getMenumodulo())
			;
	}

	/**
	 * Busca os menus diferentes do cdmenu passado por par�metros.
	 *
	 * @param cdmenu
	 * @return
	 * @since 27/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Menu> findForTelaEntrada(Integer cdmenu) {
		return querySined()
					
					.select("menu.cdmenu, menu.chave, menu.nome")
					.where("menu.cdmenu <> ?", cdmenu)
					.list();
	}

	/**
	 * Busca os itens de menu de um m�dulo espec�fico para montagem do menu.
	 *
	 * @param urlmodulo
	 * @return
	 * @since 30/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Menu> findByModulo(Menumodulo menumodulo) {
		return querySined()
					
					.select("menu.cdmenu, menu.nome, menu.url, menu.ordem, menu.contexto, menupai.cdmenu, menu.chave, menupai.chave")
					.join("menu.menumodulo menumodulo")
					.leftOuterJoin("menu.menupai menupai")
					.where("menumodulo = ?", menumodulo)
					.orderBy("menu.ordem, retira_acento(upper(menu.nome))")
					.list();
	}
	
}
