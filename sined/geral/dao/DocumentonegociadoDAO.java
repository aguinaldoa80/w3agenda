package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Documentonegociado;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentonegociadoDAO extends GenericDAO<Documentonegociado> {

	/* singleton */
	private static DocumentonegociadoDAO instance;
	public static DocumentonegociadoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(DocumentonegociadoDAO.class);
		}
		return instance;
	}
}