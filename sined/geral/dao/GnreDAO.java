package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Configuracaognre;
import br.com.linkcom.sined.geral.bean.Gnre;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.enumeration.GnreSituacao;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.GnreFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class GnreDAO extends GenericDAO<Gnre> {

	@Override
	public void updateEntradaQuery(QueryBuilder<Gnre> query) {
		query.leftOuterJoinFetch("gnre.listaGnrehistorico listaGnrehistorico");
		query.leftOuterJoinFetch("gnre.cliente cliente");
		query.leftOuterJoinFetch("gnre.municipioCliente municipioCliente");
		query.leftOuterJoinFetch("municipioCliente.uf uf");
		query.leftOuterJoinFetch("gnre.nota nota");
		query.orderBy("listaGnrehistorico.dtaltera desc");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Gnre> query, FiltroListagem _filtro) {
		GnreFiltro filtro = (GnreFiltro) _filtro;
		
		query
			.select("gnre.cdgnre, gnre.codigoreceita, gnre.valor, gnre.dtvencimento, gnre.dtpagamento, gnre.numerodocumento, gnre.situacao, " +
					"uf.cduf, uf.sigla, " +
					"cliente.cdpessoa, cliente.nome, " +
					"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia")
			.leftOuterJoin("gnre.empresa empresa")
			.leftOuterJoin("gnre.cliente cliente")
			.leftOuterJoin("gnre.uf uf")
			.where("gnre.uf = ?", filtro.getUf())
			.where("gnre.codigoreceita = ?", filtro.getCodigoreceita())
			.where("gnre.dtvencimento >= ?", filtro.getDtvencimento1())
			.where("gnre.dtvencimento <= ?", filtro.getDtvencimento2())
			.where("gnre.dtpagamento >= ?", filtro.getDtpagamento1())
			.where("gnre.dtpagamento <= ?", filtro.getDtpagamento2())
			.where("gnre.numerodocumento = ?", filtro.getNumerodocumento())
			.where("gnre.empresa = ?", filtro.getEmpresa())
			.where("gnre.cliente = ?", filtro.getCliente())
			.orderBy("gnre.cdgnre desc");
		
		if(filtro.getListaSituacao() != null) {
			query.whereIn("gnre.situacao", GnreSituacao.listAndConcatenate(filtro.getListaSituacao()));
		}
	}

	public List<Gnre> findForXML(String whereIn) {
		return query()
					.select("gnre.cdgnre, gnre.codigoreceita, gnre.codigodetalhamento, gnre.valor, gnre.valorfcp, gnre.dtvencimento, " +
							"gnre.dtpagamento, gnre.numerodocumento, gnre.chaveacesso, gnre.dtemissaodocumento, gnre.dtsaidadocumento, " +
							"gnre.infocomplementardocumento, gnre.situacao, " +
							"uf.cduf, uf.sigla, " +
							"cliente.cdpessoa, cliente.nome, cliente.razaosocial, cliente.cpf, cliente.cnpj, cliente.tipopessoa, " +
							"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, " +
							"enderecoEmpresa.cdendereco, enderecoEmpresa.logradouro, enderecoEmpresa.numero, enderecoEmpresa.complemento, enderecoEmpresa.bairro, enderecoEmpresa.cep, " +
							"enderecotipoEmpresa.cdenderecotipo, " +
							"telefoneEmpresa.cdtelefone, telefoneEmpresa.telefone, " +
							"telefonetipoEmpresa.cdtelefonetipo, " +
							"municipioEmpresa.cdmunicipio, municipioEmpresa.nome, municipioEmpresa.cdibge, " +
							"ufEmpresa.cduf, ufEmpresa.sigla, " +
							"municipioCliente.cdmunicipio, municipioCliente.nome, municipioCliente.cdibge, " +
							"configuracaognre.cdconfiguracaognre, configuracaognre.codigoreceita, " +
							"ufConfiguracao.cduf," +
							"listaConfiguracaognreuf.cdconfiguracaognreuf, listaConfiguracaognreuf.versao, listaConfiguracaognreuf.codigoperiodoreferencia, " +
							"listaConfiguracaognreuf.chaveacesso, listaConfiguracaognreuf.codigochaveacesso, " +
							"listaConfiguracaognreuf.dtsaida, listaConfiguracaognreuf.codigodtsaida, listaConfiguracaognreuf.dtemissao, listaConfiguracaognreuf.codigodtemissao, " +
							"listaConfiguracaognreuf.infocomplementar, listaConfiguracaognreuf.codigoinfocomplementar, listaConfiguracaognreuf.documentoorigem, " +
							"listaConfiguracaognreuf.codigodocumentoorigem, listaConfiguracaognreuf.documentoorigemtipo, listaConfiguracaognreuf.destinatario, " +
							"listaConfiguracaognreuf.iedestinatario, listaConfiguracaognreuf.ieremetente, listaConfiguracaognreuf.infocomplementar2, " +
							"listaConfiguracaognreuf.codigoinfocomplementar2, listaConfiguracaognreuf.maximoinfocomplementar, listaConfiguracaognreuf.maximoinfocomplementar2, " +
							"listaConfiguracaognreuf.referencia, listaConfiguracaognreuf.produto, listaConfiguracaognreuf.convenio")
					.leftOuterJoin("gnre.empresa empresa")
					.leftOuterJoin("empresa.listaTelefone telefoneEmpresa")
					.leftOuterJoin("telefoneEmpresa.telefonetipo telefonetipoEmpresa")
					.leftOuterJoin("empresa.listaEndereco enderecoEmpresa")
					.leftOuterJoin("enderecoEmpresa.municipio municipioEmpresa")
					.leftOuterJoin("enderecoEmpresa.enderecotipo enderecotipoEmpresa")
					.leftOuterJoin("municipioEmpresa.uf ufEmpresa")
					.leftOuterJoin("gnre.municipioCliente municipioCliente")
					.leftOuterJoin("gnre.cliente cliente")
					.leftOuterJoin("gnre.uf uf")
					.leftOuterJoin("gnre.configuracaognre configuracaognre")
					.leftOuterJoin("configuracaognre.listaConfiguracaognreuf listaConfiguracaognreuf")
					.leftOuterJoin("listaConfiguracaognreuf.uf ufConfiguracao")
					.whereIn("gnre.cdgnre", whereIn)
					.orderBy("gnre.cdgnre desc")
					.list();
	}

	public void updateSituacao(Gnre gnre, GnreSituacao situacao) {
		if(gnre == null || gnre.getCdgnre() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		getHibernateTemplate().bulkUpdate("update Gnre g set g.situacao = ? where g = ?", new Object[]{
			situacao, gnre
		});
	}

	public List<Gnre> findForGerarContaAPagar(String whereIn) {
		return query()
				.select("gnre.cdgnre, gnre.dtvencimento, gnre.valor, gnre.valorfcp, gnre.situacao, " +
						"empresa.cdpessoa, documentotipo.cddocumentotipo, fornecedor.cdpessoa, " +
						"configuracaognre.cdconfiguracaognre, " +
						"listaConfiguracaognrerateio.cdconfiguracaognrerateio, listaConfiguracaognrerateio.percentual, " +
						"contagerencial.cdcontagerencial, centrocusto.cdcentrocusto, projeto.cdprojeto")
				.leftOuterJoin("gnre.empresa empresa")
				.leftOuterJoin("gnre.configuracaognre configuracaognre")
				.leftOuterJoin("configuracaognre.fornecedor fornecedor")
				.leftOuterJoin("configuracaognre.documentotipo documentotipo")
				.leftOuterJoin("configuracaognre.listaConfiguracaognrerateio listaConfiguracaognrerateio")
				.leftOuterJoin("listaConfiguracaognrerateio.contagerencial contagerencial")
				.leftOuterJoin("listaConfiguracaognrerateio.centrocusto centrocusto")
				.leftOuterJoin("listaConfiguracaognrerateio.projeto projeto")
				.whereIn("gnre.cdgnre", whereIn)
				.orderBy("gnre.cdgnre desc")
				.list();
	}

	public List<Gnre> findForCancelar(String whereIn) {
		return query()
				.select("gnre.cdgnre, gnre.situacao")
				.whereIn("gnre.cdgnre", whereIn)
				.orderBy("gnre.cdgnre desc")
				.list();
	}

	public List<Gnre> findByNota(String whereInNota) {
		return query()
				.select("gnre.cdgnre, gnre.situacao, nota.cdNota")
				.leftOuterJoin("gnre.nota nota")
				.whereIn("nota.cdNota", whereInNota)
				.where("gnre.situacao <> ?", GnreSituacao.CANCELADA)
				.orderBy("gnre.cdgnre desc")
				.list();
	}

	public boolean hasGnreByNotaConfiguracao(Notafiscalproduto notafiscalproduto, Configuracaognre configuracaognre) {
		return newQueryBuilderSinedWithFrom(Long.class)
				.select("count(*)")
				.where("gnre.nota = ?", notafiscalproduto)
				.where("gnre.configuracaognre = ?", configuracaognre)
				.unique() > 0;
	}
	
}
