package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.Chequesituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Documentocomissaoreferenciatipoperiodo;
import br.com.linkcom.sined.geral.bean.enumeration.Documentocomissaotipoperiodo;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaovendaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentocomissaovendaDAO extends GenericDAO<Documentocomissao>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Documentocomissao> query, FiltroListagem _filtro) {
		DocumentocomissaovendaFiltro filtro = (DocumentocomissaovendaFiltro) _filtro;
		
		boolean comissionamento_canhoto_venda = ParametrogeralService.getInstance().getBoolean(Parametrogeral.COMISSIONAMENTO_CANHOTO_NOTA);
		if(comissionamento_canhoto_venda){
			filtro.setRetornocanhoto(Boolean.TRUE);
		}
				
		query
		.select("distinct documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome, " +
				"comissionamento.cdcomissionamento, comissionamento.percentual, comissionamento.valor, " +
				"documento.cddocumento, documento.valor, documentocomissao.valorcomissao, " +
				"cliente.cdpessoa, cliente.nome, clientep.cdpessoa, clientep.nome, documentoacao.cddocumentoacao, documentoacao.nome, " +
				"colaboradorcomissao.cdcolaboradorcomissao, documentocc.cddocumento, documentocc.valor, aux_documento.valoratual," +
				"venda.cdvenda, pedidovenda.cdpedidovenda, documentoacaocc.cddocumentoacao, colaborador.nome, colaboradorp.nome, documento.dtvencimento, documento.dtemissao, " +
				"clienteindicacao.cdpessoa, clienteindicacao.nome, clienteindicacaop.cdpessoa, clienteindicacaop.nome ")		
		.leftOuterJoin("documentocomissao.pessoa pessoa")
		.leftOuterJoin("documentocomissao.comissionamento comissionamento")
		.leftOuterJoin("documentocomissao.documento documento")
		.leftOuterJoin("documento.documentoacao documentoacao")
		.leftOuterJoin("documento.aux_documento aux_documento")
		.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
		.leftOuterJoin("colaboradorcomissao.documento documentocc")
		.leftOuterJoin("documentocc.documentoacao documentoacaocc")
		.leftOuterJoin("documentocomissao.venda venda")
		.leftOuterJoin("documentocomissao.pedidovenda pedidovenda")
		.leftOuterJoin("venda.listavendamaterial listavendamaterial")
		.leftOuterJoin("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
		.leftOuterJoin("listavendamaterial.material material")
		.leftOuterJoin("material.materialgrupo materialgrupo")
		.leftOuterJoin("venda.cliente cliente")
		.leftOuterJoin("pedidovenda.cliente clientep")
		.leftOuterJoin("venda.colaborador colaborador")
		.leftOuterJoin("pedidovenda.colaborador colaboradorp")
		.leftOuterJoin("venda.clienteindicacao clienteindicacao")
		.leftOuterJoin("pedidovenda.clienteindicacao clienteindicacaop")
		.leftOuterJoin("listaPedidovendamaterial.material materialp")
		.leftOuterJoin("materialp.materialgrupo materialgrupop")
		.ignoreJoin("listavendamaterial", "listaPedidovendamaterial", "material", "materialgrupo", "materialp", "materialgrupop");
		
		
		preencheWhereQueryComissionamento(query, filtro);
		
		query.orderBy("cliente.nome, clientep.nome, documento.cddocumento");		
	}
	
	/**
	 * M�todo que calcula o total de comissionamento da listagem
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Money findForTotalDocumentocomissaovenda(DocumentocomissaovendaFiltro filtro) {
		QueryBuilder<Long> query = newQueryBuilder(Long.class);
		
		query.from(Documentocomissao.class)
			.select("SUM(documentocomissao.valorcomissao)")		
			.leftOuterJoin("documentocomissao.pessoa pessoa")
			.leftOuterJoin("documentocomissao.documento documento")
			.leftOuterJoin("documento.documentoacao documentoacao")
			.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
			.leftOuterJoin("colaboradorcomissao.documento documentocc")
			.leftOuterJoin("documentocomissao.venda venda")
			.leftOuterJoin("documentocomissao.pedidovenda pedidovenda")
			.leftOuterJoin("venda.cliente cliente")
			.leftOuterJoin("pedidovenda.cliente clientep")
			.leftOuterJoin("venda.colaborador colaborador")
			.leftOuterJoin("pedidovenda.colaborador colaboradorp")
			.leftOuterJoin("venda.clienteindicacao clienteindicacao")
			.leftOuterJoin("pedidovenda.clienteindicacao clienteindicacaop")
			.leftOuterJoin("listavendamaterial.material material")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("listaPedidovendamaterial.material materialp")
			.leftOuterJoin("materialp.materialgrupo materialgrupop");
		
		preencheWhereQueryComissionamento(query, filtro);			
			
		Long total = query.setUseTranslator(false).unique();
		
		if (total != null)
			return new Money(total != 0 ? total/100 : total);
		else 
			return new Money();
	}

	/**
	 * M�todo que busca as informa��es do documentocomissao (que n�o foram gerados pelo pedido de venda)
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentocomissao> findForRecalcularComissaoNotPedidovenda(Venda venda, Documento documento) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome, " +
						"comissionamento.cdcomissionamento, comissionamento.percentual, comissionamento.valor, " +
						"documento.cddocumento, documento.valor, documentocomissao.valorcomissao, " +
						"cliente.cdpessoa, cliente.nome, documentoacao.cddocumentoacao, documentoacao.nome, " +
						"colaboradorcomissao.cdcolaboradorcomissao, documentocc.cddocumento, documentocc.valor, aux_documento.valoratual," +
						"venda.cdvenda, pedidovenda.cdpedidovenda ")		
				.leftOuterJoin("documentocomissao.pessoa pessoa")
				.leftOuterJoin("documentocomissao.comissionamento comissionamento")
				.leftOuterJoin("documentocomissao.documento documento")
				.leftOuterJoin("documento.documentoacao documentoacao")
				.leftOuterJoin("documento.aux_documento aux_documento")
				.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
				.leftOuterJoin("colaboradorcomissao.documento documentocc")
				.leftOuterJoin("documentocc.documentoacao documentoacaocc")
				.join("documentocomissao.venda venda")
				.leftOuterJoin("documentocomissao.pedidovenda pedidovenda")
				.leftOuterJoin("venda.cliente cliente")
				.leftOuterJoin("venda.colaborador colaborador")
				.openParentheses()
					.where("colaboradorcomissao.cdcolaboradorcomissao is null")
					.or()
					.where("documentoacaocc is null ")
					.or()
					.where("documentoacaocc = ?", Documentoacao.CANCELADA)
				.closeParentheses()
				.where("venda = ?", venda)
				.where("documento = ?", documento)
				.where("pedidovenda is null")
				.where("COALESCE(documentocomissao.representacao, false) = false")
				.list();			
	}
	
	/**
	 * M�todo que busca as informa��es do documentocomissao (gerados pelo pedido de venda)
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentocomissao> findForRecalcularComissaoWithPedidovenda(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome, " +
						"comissionamento.cdcomissionamento, comissionamento.percentual, comissionamento.valor, " +
						"documento.cddocumento, documento.valor, documentocomissao.valorcomissao, " +
						"cliente.cdpessoa, cliente.nome, documentoacao.cddocumentoacao, documentoacao.nome, " +
						"colaboradorcomissao.cdcolaboradorcomissao, documentocc.cddocumento, documentocc.valor, aux_documento.valoratual," +
						"venda.cdvenda, pedidovenda.cdpedidovenda ")		
				.leftOuterJoin("documentocomissao.pessoa pessoa")
				.leftOuterJoin("documentocomissao.comissionamento comissionamento")
				.leftOuterJoin("documentocomissao.documento documento")
				.leftOuterJoin("documento.documentoacao documentoacao")
				.leftOuterJoin("documento.aux_documento aux_documento")
				.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
				.leftOuterJoin("colaboradorcomissao.documento documentocc")
				.leftOuterJoin("documentocc.documentoacao documentoacaocc")
				.join("documentocomissao.venda venda")
				.join("documentocomissao.pedidovenda pedidovenda")
				.leftOuterJoin("venda.cliente cliente")
				.leftOuterJoin("venda.colaborador colaborador")
				.openParentheses()
					.where("colaboradorcomissao.cdcolaboradorcomissao is null")
					.or()
					.where("documentoacaocc is null ")
					.or()
					.where("documentoacaocc = ?", Documentoacao.CANCELADA)
				.closeParentheses()
				.where("venda = ?", venda)
				.where("COALESCE(documentocomissao.representacao, false) = false")
				.list();			
	}
	
	public List<Documentocomissao> findForRecalcularComissaoPedidovenda(Pedidovenda pedidovenda, String whereInVenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Documentocomissao> query = query()
				.select("documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome, " +
						"comissionamento.cdcomissionamento, comissionamento.percentual, comissionamento.valor, " +
						"documento.cddocumento, documento.valor, documentocomissao.valorcomissao, " +
						"cliente.cdpessoa, cliente.nome, documentoacao.cddocumentoacao, documentoacao.nome, " +
						"colaboradorcomissao.cdcolaboradorcomissao, documentocc.cddocumento, documentocc.valor, aux_documento.valoratual," +
						"pedidovenda.cdpedidovenda ")		
				.leftOuterJoin("documentocomissao.pessoa pessoa")
				.leftOuterJoin("documentocomissao.comissionamento comissionamento")
				.leftOuterJoin("documentocomissao.documento documento")
				.leftOuterJoin("documento.documentoacao documentoacao")
				.leftOuterJoin("documento.aux_documento aux_documento")
				.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
				.leftOuterJoin("colaboradorcomissao.documento documentocc")
				.leftOuterJoin("documentocc.documentoacao documentoacaocc")
				.leftOuterJoin("documentocomissao.pedidovenda pedidovenda")
				.leftOuterJoin("documentocomissao.venda venda")
				.leftOuterJoin("pedidovenda.cliente cliente")
				.leftOuterJoin("pedidovenda.colaborador colaborador")
				.openParentheses()
					.where("colaboradorcomissao.cdcolaboradorcomissao is null")
					.or()
					.where("documentoacaocc is null ")
					.or()
					.where("documentoacaocc = ?", Documentoacao.CANCELADA)
				.closeParentheses();
		
		query.openParentheses()
			.where("pedidovenda = ?", pedidovenda);
			if(StringUtils.isNotBlank(whereInVenda)){
				query.or()
					.whereIn("venda.cdvenda", whereInVenda);
			}
		query.closeParentheses();
			
		return query
				.where("COALESCE(documentocomissao.representacao, false) = false")
				.list();			
	}

	public void deleteDocumentocomissaovenda(Documentocomissao documentocomissao) {
		if(documentocomissao != null && documentocomissao.getCddocumentocomissao() != null){
			getHibernateTemplate().bulkUpdate("delete Documentocomissao dc where dc.cddocumentocomissao = ?",
					new Object[]{documentocomissao.getCddocumentocomissao()});
		}		
	}

	public List<Documentocomissao> findForPDF(
			DocumentocomissaovendaFiltro filtro) {
		QueryBuilder<Documentocomissao> query = querySined();
		updateListagemQuery(query, filtro);
		return query.list();
	}
	
	private void preencheWhereQueryComissionamento(QueryBuilder<? extends Object> query, DocumentocomissaovendaFiltro filtro){
		query
			.where("COALESCE(documentocomissao.representacao, false) = false")
			.openParentheses()
				.where("cliente = ?", filtro.getCliente())
				.or()
				.where("clientep = ?", filtro.getCliente())
			.closeParentheses()
			.openParentheses()
				.where("colaborador = ?", filtro.getColaboradorvendedor())
				.or()
				.where("colaboradorp = ?", filtro.getColaboradorvendedor())
			.closeParentheses()
			.openParentheses()
				.where("clienteindicacao = ?", filtro.getClientecomissao())
				.or()
				.where("clienteindicacaop = ?", filtro.getClientecomissao())
			.closeParentheses()
			.where("pessoa = ?", filtro.getFornecedoragencia())
			.openParentheses()
				.where("materialgrupo = ? ", filtro.getMaterialgrupo())
				.or()
				.where("materialgrupop = ? ", filtro.getMaterialgrupo())
			.closeParentheses();
		
		query.openParentheses()
				.where("venda.projeto = ?",filtro.getProjeto())
				.or()
				.where("pedidovenda.projeto = ?",filtro.getProjeto())
			.closeParentheses();
		
		if(filtro.getTipocolaborador() != null && filtro.getTipocolaborador().equals("Indica��o Cliente")){
			query.where("indicacao = true");
		}else if(filtro.getTipocolaborador() != null && filtro.getTipocolaborador().equals("Ag�ncia")){
			query.where("documentocomissao.agencia = true");
		}else {
			query.where("coalesce(indicacao, false) = false");
		}
		
		if(filtro.getRetornocanhoto() != null){
			if(filtro.getRetornocanhoto()){
				query.where("exists(select 1 from Nota n join n.listaNotavenda nv where nv.venda = venda and n.retornocanhoto = ? and n.notaStatus in (?,?,?,?,?))", 
						new Object[]{Boolean.TRUE, NotaStatus.NFE_EMITIDA, NotaStatus.NFE_LIQUIDADA, NotaStatus.NFE_DENEGADA, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA});
			} else {
				query.where("not exists(select 1 from Nota n join n.listaNotavenda nv where nv.venda = venda and n.retornocanhoto = ? and n.notaStatus in (?,?,?,?,?))", 
						new Object[]{Boolean.TRUE, NotaStatus.NFE_EMITIDA, NotaStatus.NFE_LIQUIDADA, NotaStatus.NFE_DENEGADA, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA});
			}
		}
		
		if(filtro.getColaboradorcomissao() != null && filtro.getColaboradorcomissao().getCdpessoa() != null){
			query.where("pessoa.cdpessoa = ?", filtro.getColaboradorcomissao().getCdpessoa());
		}else if(filtro.getColaboradorvendedor() != null){
			query.where("pessoa.cdpessoa = ?", filtro.getColaboradorvendedor().getCdpessoa());
		}
		
		if(filtro.getMostrar() != null && filtro.getMostrar().equals("Pagos com pend�ncia de repasse")){
			
			List<Documentoacao> lista = new ArrayList<Documentoacao>();
			lista.add(Documentoacao.BAIXADA);
			lista.add(Documentoacao.NEGOCIADA);
			
			query.where("not exists (" +
									"select d.cddocumento " +
									"from Documento d " +
									"where (retorna_array_documento_negociado(d.cddocumento, documento.cddocumento )=true) " + 
									"and d.documentoacao.cddocumentoacao not in(" + CollectionsUtil.listAndConcatenate(lista, "cddocumentoacao", ",") + "))");
			
			query.openParentheses();
			for (Documentoacao documentoacao : lista) {
				query.where("documentoacao = ?", documentoacao).or();
			}
			query.closeParentheses();
			query.where("colaboradorcomissao.cdcolaboradorcomissao is null");
		}
		
		query.openParentheses();
		query.openParentheses();
		if(filtro.getDocumentocomissaotipoperiodo() != null){
			if(Documentocomissaotipoperiodo.VENCIMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
				if(filtro.getDtinicio() != null && filtro.getDtfim() != null){
					query.openParentheses();
					if(filtro.getDocumentocomissaoreferenciatipoperiodo() != null && filtro.getDocumentocomissaoreferenciatipoperiodo().equals(Documentocomissaoreferenciatipoperiodo.CONTA_NEGOCIADA)){
						query.where("exists (select d.cddocumento " +
												"from Documento d " +
												"where (retorna_array_documento_negociado(d.cddocumento, documento.cddocumento )=true) " +
												"and d.dtvencimento >= ? " +
												"and d.dtvencimento <= ?)", new Object[]{filtro.getDtinicio(), filtro.getDtfim()});
					} else {
						query
							.where("documento.dtvencimento >= ?", filtro.getDtinicio())
							.where("documento.dtvencimento <= ?", filtro.getDtfim());
					}
					query.closeParentheses();
				} else if(filtro.getDtinicio() != null){
					query.openParentheses();
					if(filtro.getDocumentocomissaoreferenciatipoperiodo() != null && filtro.getDocumentocomissaoreferenciatipoperiodo().equals(Documentocomissaoreferenciatipoperiodo.CONTA_NEGOCIADA)){
						query.where("exists (select d.cddocumento " +
												"from Documento d " +
												"where (retorna_array_documento_negociado(d.cddocumento, documento.cddocumento )=true) " +
												"and d.dtvencimento >= ?)", filtro.getDtinicio());
					} else {
						query.where("documento.dtvencimento >= ?", filtro.getDtinicio());
					}
					query.closeParentheses();
				} else if(filtro.getDtfim() != null){
					query.openParentheses();
					if(filtro.getDocumentocomissaoreferenciatipoperiodo() != null && filtro.getDocumentocomissaoreferenciatipoperiodo().equals(Documentocomissaoreferenciatipoperiodo.CONTA_NEGOCIADA)){
						query.where("exists (select d.cddocumento " +
												"from Documento d " +
												"where (retorna_array_documento_negociado(d.cddocumento, documento.cddocumento )=true) " +
												"and d.dtvencimento <= ?)", filtro.getDtfim());
					} else {
						query.where("documento.dtvencimento <= ?", filtro.getDtfim());
					}
					query.closeParentheses();
				}
			}else if(Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
				List<Movimentacaoacao> lista = new ArrayList<Movimentacaoacao>();
				lista.add(Movimentacaoacao.NORMAL);
				lista.add(Movimentacaoacao.CONCILIADA);
				
				if(filtro.getDtinicio() != null && filtro.getDtfim() != null){
					query.openParentheses();
					query.where("exists (select d.cddocumento " +
									"from Movimentacaoorigem mo " +
									"join mo.documento d " +
									"join mo.movimentacao m " +
									"join m.movimentacaoacao ma " +
									(preencheJoinWhereCheque(filtro.getConsiderarchequedevolvido(), true, false)) +
									"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
									"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
									(preencheJoinWhereCheque(filtro.getConsiderarchequedevolvido(), false, true)) +
									"and d.cddocumento = documento.cddocumento " +
									"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(lista, "cdmovimentacaoacao", ",") + ") " +
									")");
					query.or();
					query.where("exists(select d.cddocumento " +
									"from Movimentacaoorigem mo " +
									"join mo.documento d " +
									"join mo.movimentacao m " +
									"join m.movimentacaoacao ma " +
									(preencheJoinWhereCheque(filtro.getConsiderarchequedevolvido(), true, false)) +
									"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
									(preencheJoinWhereCheque(filtro.getConsiderarchequedevolvido(), false, true)) +
									"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
									"and (retorna_array_documento_negociado(d.cddocumento, documento.cddocumento )=true) " +
									"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(lista, "cdmovimentacaoacao", ",") + ") " +				
									")");
					query.closeParentheses();
				}else if(filtro.getDtinicio() != null){
					query.openParentheses();
					query.where("exists (select d.cddocumento " +
									"from Movimentacaoorigem mo " +
									"join mo.documento d " +
									"join mo.movimentacao m " +
									"join m.movimentacaoacao ma " +
									(preencheJoinWhereCheque(filtro.getConsiderarchequedevolvido(), true, false)) +
									"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
									(preencheJoinWhereCheque(filtro.getConsiderarchequedevolvido(), false, true)) +
									"and d.cddocumento = documento.cddocumento " +
									"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(lista, "cdmovimentacaoacao", ",") + ") " +
									")");
					query.or();
					query.where("exists(select d.cddocumento " +
									"from Movimentacaoorigem mo " +
									"join mo.documento d " +
									"join mo.movimentacao m " +
									"join m.movimentacaoacao ma " +
									(preencheJoinWhereCheque(filtro.getConsiderarchequedevolvido(), true, false)) +
									"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
									(preencheJoinWhereCheque(filtro.getConsiderarchequedevolvido(), false, true)) +
									"and (retorna_array_documento_negociado(d.cddocumento, documento.cddocumento )=true) " +
									"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(lista, "cdmovimentacaoacao", ",") + ") " +
									")");
					query.closeParentheses();
				}else if(filtro.getDtfim() != null){
					query.openParentheses();
					query.where("exists (select d.cddocumento " +
									"from Movimentacaoorigem mo " +
									"join mo.documento d " +
									"join mo.movimentacao m " +
									"join m.movimentacaoacao ma " +
									(preencheJoinWhereCheque(filtro.getConsiderarchequedevolvido(), true, false)) +
									"where m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
									"and d.cddocumento = documento.cddocumento " +
									(preencheJoinWhereCheque(filtro.getConsiderarchequedevolvido(), false, true)) +
									"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(lista, "cdmovimentacaoacao", ",") + ") " +
									")");
					query.or();
					query.where("exists(select d.cddocumento " +
									"from Movimentacaoorigem mo " +
									"join mo.documento d " +
									"join mo.movimentacao m " +
									"join m.movimentacaoacao ma " +
									(preencheJoinWhereCheque(filtro.getConsiderarchequedevolvido(), true, false)) +
									"where m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
									(preencheJoinWhereCheque(filtro.getConsiderarchequedevolvido(), false, true)) +
									"and (retorna_array_documento_negociado(d.cddocumento, documento.cddocumento )=true) " +
									"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(lista, "cdmovimentacaoacao", ",") + ") " +
									")");
					query.closeParentheses();
				}else {
					if(filtro.getConsiderarchequedevolvido() == null || !filtro.getConsiderarchequedevolvido()){
						query.openParentheses();
						query.where("exists (select d.cddocumento " +
										"from Movimentacaoorigem mo " +
										"join mo.documento d " +
										"join mo.movimentacao m " +
										"join m.movimentacaoacao ma " +
										(preencheJoinWhereCheque(filtro.getConsiderarchequedevolvido(), true, false)) +
										"where d.cddocumento = documento.cddocumento " +
										(preencheJoinWhereCheque(filtro.getConsiderarchequedevolvido(), false, true)) +
										"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(lista, "cdmovimentacaoacao", ",") + ") " +
										")");
						query.or();
						query.where("exists(select d.cddocumento " +
										"from Movimentacaoorigem mo " +
										"join mo.documento d " +
										"join mo.movimentacao m " +
										"join m.movimentacaoacao ma " +
										(preencheJoinWhereCheque(filtro.getConsiderarchequedevolvido(), true, false)) +
										"where (retorna_array_documento_negociado(d.cddocumento, documento.cddocumento )=true) " +
										(preencheJoinWhereCheque(filtro.getConsiderarchequedevolvido(), false, true)) +
										"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(lista, "cdmovimentacaoacao", ",") + ") " +
										")");
						query.closeParentheses();
					}
				}
			} else if(Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo())){
				if(filtro.getDtinicio() != null && filtro.getDtfim() != null){
					query.openParentheses();
					if(filtro.getDocumentocomissaoreferenciatipoperiodo() != null && filtro.getDocumentocomissaoreferenciatipoperiodo().equals(Documentocomissaoreferenciatipoperiodo.CONTA_NEGOCIADA)){
						query.where("exists (select d.cddocumento " +
												"from Documento d " +
												"where (retorna_array_documento_negociado(d.cddocumento, documento.cddocumento )=true) " +
												"and d.dtemissao >= ? " +
												"and d.dtemissao <= ?)", new Object[]{filtro.getDtinicio(), filtro.getDtfim()});
						
					} else {
						query
							.where("documento.dtemissao >= ?", filtro.getDtinicio())
							.where("documento.dtemissao <= ?", filtro.getDtfim());
					}
					query.closeParentheses();
				}else if(filtro.getDtinicio() != null){
					query.openParentheses();
					if(filtro.getDocumentocomissaoreferenciatipoperiodo() != null && filtro.getDocumentocomissaoreferenciatipoperiodo().equals(Documentocomissaoreferenciatipoperiodo.CONTA_NEGOCIADA)){
						query.where("exists (select d.cddocumento " +
												"from Documento d " +
												"where (retorna_array_documento_negociado(d.cddocumento, documento.cddocumento )=true) " +
												"and d.dtemissao >= ?)", filtro.getDtinicio());
						
					} else {
						query.where("documento.dtemissao >= ?", filtro.getDtinicio());
					}
					query.closeParentheses();
				}else if(filtro.getDtfim() != null){
					query.openParentheses();
					if(filtro.getDocumentocomissaoreferenciatipoperiodo() != null && filtro.getDocumentocomissaoreferenciatipoperiodo().equals(Documentocomissaoreferenciatipoperiodo.CONTA_NEGOCIADA)){
						query.where("exists (select d.cddocumento " +
												"from Documento d " +
												"where (retorna_array_documento_negociado(d.cddocumento, documento.cddocumento )=true) " +
												"and d.dtemissao <= ?)", filtro.getDtfim());
						
					} else {
						query.where("documento.dtemissao <= ?", filtro.getDtfim());
					}
					query.closeParentheses();
				}
			} 
		}
		
		query.where("documento.documentoacao <> ?", Documentoacao.CANCELADA);
		query.closeParentheses();
		
		if (Boolean.FALSE.equals(filtro.getExibirComissao())) {
			query
				.where("pedidovenda is not null")
				.where("venda is null")
				.where("pedidovenda.pedidovendasituacao in (" + Pedidovendasituacao.CONFIRMADO.getValue() + ", " + Pedidovendasituacao.PREVISTA.getValue() + ")");
		}else if(Boolean.TRUE.equals(filtro.getExibirComissao())){
			query
				.where("venda is not null");
		}else {
			query.openParentheses()
				.where("pedidovenda is not null")
				.or()
				.where("venda is not null")
			.closeParentheses();
			
		}
		
		query.closeParentheses();
		query.where("documentocomissao.vendamaterial is null");
	}
	
	private String preencheJoinWhereCheque(Boolean considerarchequedevolvido, boolean join, boolean where) {
		StringBuilder joinWhere = new StringBuilder();
		if(considerarchequedevolvido == null || !considerarchequedevolvido){
			if(join){
				joinWhere.append(" left outer join m.cheque cheq ");
			}else if(where){
				joinWhere.append(" and (cheq is null or cheq.chequesituacao <> " + Chequesituacao.DEVOLVIDO.getValue() + " ) ");
			}
		}
		
		return joinWhere.toString();
	}

	/**
	 * 
	 * M�todo que faz o somat�rio geral do valor dos documentos.
	 *
	 * @name findForTotalGeralDocumentocomissaovenda
	 * @param filtro
	 * @return
	 * @return Money
	 * @author Thiago Augusto
	 * @date 24/08/2012
	 *
	 */
	public Money findForTotalGeralDocumentocomissaovenda(DocumentocomissaovendaFiltro filtro) {
		QueryBuilder<Documentocomissao> query = newQueryBuilder(Documentocomissao.class);
		
		query
		.from(Documentocomissao.class)
		.select("documentocomissao.cddocumentocomissao, documento.cddocumento, aux_documento.valoratual ")
		.leftOuterJoin("documentocomissao.pessoa pessoa")
		.leftOuterJoin("documentocomissao.documento documento")
		.leftOuterJoin("documento.aux_documento aux_documento")
		.leftOuterJoin("documento.documentoacao documentoacao")
		.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
		.leftOuterJoin("colaboradorcomissao.documento documentocc")
		.leftOuterJoin("documentocomissao.venda venda")
		.leftOuterJoin("documentocomissao.pedidovenda pedidovenda")
		.leftOuterJoin("venda.cliente cliente")
		.leftOuterJoin("pedidovenda.cliente clientep")
		.leftOuterJoin("venda.colaborador colaborador")
		.leftOuterJoin("pedidovenda.colaborador colaboradorp")
		.leftOuterJoin("venda.clienteindicacao clienteindicacao")
		.leftOuterJoin("pedidovenda.clienteindicacao clienteindicacaop")
		.leftOuterJoin("venda.listavendamaterial listavendamaterial")
		.leftOuterJoin("pedidovenda.listaPedidovendamaterial listaPedidovendamaterial")
		.leftOuterJoin("listavendamaterial.material material")
		.leftOuterJoin("material.materialgrupo materialgrupo")
		.leftOuterJoin("listaPedidovendamaterial.material materialp")
		.leftOuterJoin("materialp.materialgrupo materialgrupop");
		
		preencheWhereQueryComissionamento(query, filtro);
		
		List<Documentocomissao> lista = query.list();
		Money total = new Money();
		if(lista != null && !lista.isEmpty()){
			List<Documento> listaDocumentosUtilizados = new ArrayList<Documento>();
			for(Documentocomissao dc : lista){
				if(dc.getDocumento() != null && dc.getDocumento().getAux_documento() != null && 
						dc.getDocumento().getAux_documento().getValoratual() != null){
					if(!listaDocumentosUtilizados.contains(dc.getDocumento())){
						total = total.add(dc.getDocumento().getAux_documento().getValoratual());
						listaDocumentosUtilizados.add(dc.getDocumento());
					}
				}
			}
		}
		
		if (total != null)
			return total;
		else 
			return new Money();
	}
	
	/**
	* M�todo que verifica se existe comiss�o paga para o colaborador
	*
	* @param colaborador
	* @param venda
	* @param pedidovenda
	* @return
	* @since 16/09/2015
	* @author Luiz Fernando
	*/
	public Boolean existeComissaoRepassadaParaColaborador(Colaborador colaborador, Venda venda, Pedidovenda pedidovenda) {
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Documentocomissao.class)
			.join("documentocomissao.pessoa pessoa")
			.join("documentocomissao.colaboradorcomissao colaboradorcomissao")
			.join("colaboradorcomissao.documento documento")
			.join("documento.documentoacao documentoacao")
			.leftOuterJoin("documentocomissao.venda venda")
			.leftOuterJoin("documentocomissao.pedidovenda pedidovenda")
			.where("colaboradorcomissao.cdcolaboradorcomissao is not null")
			.where("venda = ?", venda)
			.where("pedidovenda = ?", pedidovenda)
			.where("pessoa = ?", colaborador)
			.where("documentoacao <> ?", Documentoacao.CANCELADA)
			.where("COALESCE(documentocomissao.representacao, false) = false")
			.unique() > 0;
	}

	public List<Documentocomissao> findForRecalcularComissao(Pedidovenda pedidovenda, Documento documento) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome, " +
						"comissionamento.cdcomissionamento, comissionamento.percentual, comissionamento.valor, " +
						"documento.cddocumento, documento.valor, documentocomissao.valorcomissao, " +
						"cliente.cdpessoa, cliente.nome, documentoacao.cddocumentoacao, documentoacao.nome, " +
						"colaboradorcomissao.cdcolaboradorcomissao, documentocc.cddocumento, documentocc.valor, aux_documento.valoratual," +
						"pedidovenda.cdpedidovenda ")		
				.leftOuterJoin("documentocomissao.pessoa pessoa")
				.leftOuterJoin("documentocomissao.comissionamento comissionamento")
				.leftOuterJoin("documentocomissao.documento documento")
				.leftOuterJoin("documento.documentoacao documentoacao")
				.leftOuterJoin("documento.aux_documento aux_documento")
				.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
				.leftOuterJoin("colaboradorcomissao.documento documentocc")
				.leftOuterJoin("documentocc.documentoacao documentoacaocc")
				.join("documentocomissao.pedidovenda pedidovenda")
				.leftOuterJoin("pedidovenda.cliente cliente")
				.leftOuterJoin("pedidovenda.colaborador colaborador")
				.openParentheses()
					.where("colaboradorcomissao.cdcolaboradorcomissao is null")
					.or()
					.where("documentoacaocc is null ")
					.or()
					.where("documentoacaocc = ?", Documentoacao.CANCELADA)
				.closeParentheses()
				.where("pedidovenda = ?", pedidovenda)
				.where("documento = ?", documento)
				.where("COALESCE(documentocomissao.representacao, false) = false")
				.list();
	}
}
