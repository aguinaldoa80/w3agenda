package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcomissao;
import br.com.linkcom.sined.geral.bean.Comissionamento;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.Documentocomissaoreferenciatipoperiodo;
import br.com.linkcom.sined.geral.bean.enumeration.Documentocomissaotipoperiodo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipobccomissionamento;
import br.com.linkcom.sined.geral.bean.enumeration.Vendedortipo;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentocomissaoDAO extends GenericDAO<Documentocomissao>{
	@Override
	public void updateListagemQuery(QueryBuilder<Documentocomissao> query, FiltroListagem _filtro) {
		DocumentocomissaoFiltro filtro = (DocumentocomissaoFiltro) _filtro;
				
		query
		.select("distinct documentocomissao.cddocumentocomissao, cliente.nome, contrato.descricao, documento.cddocumento, pessoa.nome")		
		.leftOuterJoin("documentocomissao.pessoa pessoa")
		.leftOuterJoin("documentocomissao.comissionamento comissionamento")
		.leftOuterJoin("documentocomissao.documento documento")
		.leftOuterJoin("documento.listaDocumentonegociacao documentonegociado")
		.leftOuterJoin("documento.documentoacao documentoacao")
		.leftOuterJoin("documento.aux_documento aux_documento")
		.join("documentocomissao.contrato contrato")
		.leftOuterJoin("documentocomissao.nota nota")
		.leftOuterJoin("contrato.cliente cliente")
		.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
		.leftOuterJoin("colaboradorcomissao.documento documentocc")
		.leftOuterJoin("contrato.contratotipo contratotipo")	
		.leftOuterJoin("contrato.comissionamento comissionamentocontrato")
		.leftOuterJoin("contrato.vendedor vendedor")
		
		.leftOuterJoin("contrato.listaContratocolaborador listaContratocolaborador")
		.leftOuterJoin("listaContratocolaborador.colaborador colaborador")
		.leftOuterJoin("listaContratocolaborador.comissionamento colaboradorcomissionamento")
		
		.ignoreJoin("listaContratocolaborador", "colaborador", "colaboradorcomissionamento");
		
		preencheWhereQueryComissionamento(query, filtro);

		query.orderBy("cliente.nome, contrato.descricao, documento.cddocumento");
	}
	
	public List<Documentocomissao> findByCliente(Cliente cliente){
		return query()
		.select("documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome," +
				"comissionamento.cdcomissionamento, comissionamento.percentual, comissionamento.valor, comissionamento.deduzirvalor, " +
				"documento.cddocumento, documento.valor, contrato.cdcontrato, contrato.descricao, " +
				"cliente.cdpessoa, cliente.nome, documentoacao.cddocumentoacao, documentoacao.nome," +
				"colaboradorcomissao.cdcolaboradorcomissao")
		
		.join("documentocomissao.pessoa pessoa")
		.join("documentocomissao.comissionamento comissionamento")
		.join("documentocomissao.documento documento")
		.join("documento.documentoacao documentoacao")
		.join("documentocomissao.contrato contrato")
		.join("contrato.cliente cliente")
		.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
		.where("cliente = ?", cliente)
		.list();
			
		
	}
	
	public List<Documentocomissao> findByPagamento(String whereIn){
		return query()
		.select("documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome," +
				"comissionamento.cdcomissionamento, comissionamento.percentual, comissionamento.valor, comissionamento.deduzirvalor, " +
				"documento.cddocumento, documento.valor, contrato.cdcontrato, contrato.descricao, contrato.valordedutivocomissionamento, " +
				"cliente.cdpessoa, cliente.nome, documentoacao.cddocumentoacao, documentoacao.nome," +
				"colaboradorcomissao.cdcolaboradorcomissao, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, aux_documento.valoratual, " +
				"nota.cdNota, documentocomissao.valorcomissao ")
		
		.join("documentocomissao.pessoa pessoa")
		.join("documentocomissao.comissionamento comissionamento")
		.leftOuterJoin("documentocomissao.documento documento")
		.leftOuterJoin("documento.aux_documento aux_documento")
		.leftOuterJoin("documento.documentoacao documentoacao")
		.leftOuterJoin("documentocomissao.contrato contrato")
		.leftOuterJoin("contrato.empresa empresa")
		.leftOuterJoin("contrato.cliente cliente")
		.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
		.leftOuterJoin("documentocomissao.nota nota")
		.whereIn("documentocomissao.cddocumentocomissao", whereIn)
		.list();
			
	}
	
	/**
	 * Seta o id de colaboradorcomiss�o no documentocomissao origem 
	 * @param documentocomissao
	 * @param colaboradorcomissao
	 * @author Taidson
	 * @since 04/11/2010
	 */	
	public void baixarComissao(Documentocomissao documentocomissao, Colaboradorcomissao colaboradorcomissao) {
		String sql = "UPDATE Documentocomissao" +
					 " SET cdcolaboradorcomissao = " + colaboradorcomissao.getCdcolaboradorcomissao()  +
					 " WHERE cddocumentocomissao = (" + documentocomissao.getCddocumentocomissao() + ")";
		getJdbcTemplate().execute(sql);
	}

	/**
	 * Busca a quantidade de comiss�es j� registradas de acordo 
	 * com os par�metros para o faturamento do contrato.
	 *
	 * @param contrato
	 * @param colaborador
	 * @param comissionamento
	 * @param vendedor
	 * @param nota 
	 * @param documento 
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Integer findForFaturamento(Contrato contrato, Pessoa colaborador, Comissionamento comissionamento, boolean vendedor) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Documentocomissao.class)
					.setUseTranslator(false)
					.leftOuterJoin("documentocomissao.contrato contrato")
					.leftOuterJoin("documentocomissao.pessoa pessoa")
					.leftOuterJoin("documentocomissao.comissionamento comissionamento")
					.where("contrato = ?", contrato)
					.where("pessoa = ?", colaborador)
					.where("comissionamento = ?", comissionamento)
					.where("documentocomissao.vendedor = ?", new Boolean(vendedor))
					.unique()
					.intValue();
	}
	
	/**
	* M�todo que busca documentocomiss�o do contrato
	*
	* @param contrato
	* @return
	* @since Sep 6, 2011
	* @author Luiz Fernando F Silva
	*/
	public Documentocomissao findByPagamento(Contrato contrato){
		if(contrato == null || contrato.getCdcontrato() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome," +
						"comissionamento.cdcomissionamento, comissionamento.percentual, comissionamento.valor, comissionamento.deduzirvalor, " +
						"documento.cddocumento, documento.valor, contrato.cdcontrato, contrato.descricao, " +
						"contratocolaborador.cdcontratocolaborador, contratocolaborador.ordemcomissao, " +
						"colaborador.cdpessoa, " +
						"cliente.cdpessoa, cliente.nome, documentoacao.cddocumentoacao, documentoacao.nome," +
						"colaboradorcomissao.cdcolaboradorcomissao, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, aux_documento.valoratual, " +
						"nota.cdNota, vendedor.cdpessoa, cvendedor.cdpessoa, contrato.vendedortipo, " +
						"ccomissionamento.cdcomissionamento, ccomissionamento.percentual, ccomissionamento.valor, ccomissionamento.deduzirvalor ")
				
				.leftOuterJoin("documentocomissao.pessoa pessoa")
//				.leftOuterJoin("documentocomissao.comissionamento comissionamento")
				.leftOuterJoin("documentocomissao.documento documento")
				.leftOuterJoin("documento.aux_documento aux_documento")
				.leftOuterJoin("documento.documentoacao documentoacao")
				.leftOuterJoin("documentocomissao.contrato contrato")
				.leftOuterJoin("contrato.empresa empresa")
				.leftOuterJoin("contrato.cliente cliente")
				.leftOuterJoin("contrato.vendedor cvendedor")
				.leftOuterJoin("contrato.listaContratocolaborador contratocolaborador")
				.leftOuterJoin("contratocolaborador.comissionamento comissionamento")
				.leftOuterJoin("contratocolaborador.contrato contratoc")
				.leftOuterJoin("contratoc.vendedor vendedor")
				.leftOuterJoin("contratoc.comissionamento ccomissionamento")
				.leftOuterJoin("contratocolaborador.colaborador colaborador")
				.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
				.leftOuterJoin("documentocomissao.nota nota")
				.where("contrato.cdcontrato = ?", contrato.getCdcontrato())
				.orderBy("contratocolaborador.ordemcomissao")
				.unique();
			
	}

	/**
	* M�todo para buscar documentocomissao gerado pelo faturamento do contrato
	*
	* @param contrato
	* @param nota
	* @return
	* @since Oct 11, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Documentocomissao> findComissaocontrato(Contrato contrato,	Nota nota) {
		if(contrato == null || contrato.getCdcontrato() == null ||
				nota == null || nota.getCdNota() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
				.select("documentocomissao.cddocumentocomissao, pessoa.cdpessoa, comissionamento.cdcomissionamento, contrato.cdcontrato, nota.cdNota")
				.join("documentocomissao.contrato contrato")
				.join("documentocomissao.nota nota")
				.leftOuterJoin("documentocomissao.comissionamento comissionamento")
				.leftOuterJoin("documentocomissao.pessoa pessoa")
				.where("contrato = ?", contrato)
				.where("nota = ?", nota)
				.list();
	}

	/**
	* M�todo que atualiza o documentocomissao com o n�mero do documento gerado pelo GERAR RECEITA AUTOM�TICA da NFS
	*
	* @param documentocomissao
	* @param documento
	* @since Oct 11, 2011
	* @author Luiz Fernando F Silva
	*/
	public void atualizaDocumentocomissao(Documentocomissao documentocomissao, Documento documento) {
		if(documentocomissao == null || documentocomissao.getCddocumentocomissao() == null ||
				documento == null || documento.getCddocumento() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		getJdbcTemplate().execute("update documentocomissao set cddocumento = " + documento.getCddocumento() + 
				" where documentocomissao.cddocumentocomissao = " + documentocomissao.getCddocumentocomissao() + ";");
		
	}
	
	/**
	 * M�todo que busca os dados para o comissionamento da venda
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentocomissao> findByPagamentoVenda(String whereIn){
		return query()
		.select("documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome, venda.cdvenda, pedidovenda.cdpedidovenda, " +
				"documento.cddocumento, documento.valor, documentocomissao.valorcomissao, colaboradorcomissao.cdcolaboradorcomissao, " +
				"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresap.cdpessoa, empresap.nome, empresap.razaosocial, empresap.nomefantasia," +
				"documentocomissao.indicacao, documentocomissao.agencia ")
		.join("documentocomissao.pessoa pessoa")
		.leftOuterJoin("documentocomissao.documento documento")
		.leftOuterJoin("documento.aux_documento aux_documento")
		.leftOuterJoin("documento.documentoacao documentoacao")
		.leftOuterJoin("documentocomissao.venda venda")
		.leftOuterJoin("venda.cliente cliente")
		.leftOuterJoin("venda.empresa empresa")
		.leftOuterJoin("documentocomissao.pedidovenda pedidovenda")
		.leftOuterJoin("pedidovenda.cliente clientep")
		.leftOuterJoin("pedidovenda.empresa empresap")
		.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")		
		.whereIn("documentocomissao.cddocumentocomissao", whereIn)
		.list();
			
	}

	/**
	 * M�todo que calcula o total de comissionamento da listagem contrato
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Money findForTotalDocumentocomissao(DocumentocomissaoFiltro filtro) {
		QueryBuilder<Documentocomissao> query = query();
		
		query.from(Documentocomissao.class)
			.select("documentocomissao.cddocumentocomissao, documentocomissao.valorcomissao ")		
			.leftOuterJoin("documentocomissao.pessoa pessoa")
			.leftOuterJoin("documentocomissao.comissionamento comissionamento")
			.leftOuterJoin("documentocomissao.documento documento")
			.leftOuterJoin("documento.listaDocumentonegociacao documentonegociado")
			.leftOuterJoin("documento.documentoacao documentoacao")
			.leftOuterJoin("documento.aux_documento aux_documento")
			.join("documentocomissao.contrato contrato")
			.leftOuterJoin("documentocomissao.nota nota")
			.leftOuterJoin("contrato.cliente cliente")
			.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
			.leftOuterJoin("colaboradorcomissao.documento documentocc")
			.leftOuterJoin("contrato.contratotipo contratotipo");

		preencheWhereQueryComissionamento(query, filtro);
		
		List<Documentocomissao> lista = query.list();
		
		Money total = new Money();
		if(SinedUtil.isListNotEmpty(lista)){
			for(Documentocomissao documentocomissao : lista){
				if(documentocomissao.getValorcomissao() != null){
					total = total.add(documentocomissao.getValorcomissao());
				}
			}
		}
		
		return total;
	}
	
	public Money getTotalComissaoColaboradorMes(Empresa empresa, Colaborador colaborador, Date dtreferencia1, Date dtreferencia2, String tipo) {

		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
					.select("sum(documentocomissao.valorcomissao)")
					.from(Documentocomissao.class)
					.setUseTranslator(false)
					.leftOuterJoin("documentocomissao.pessoa pessoa")
					.leftOuterJoin("documentocomissao.documento documento")
					.leftOuterJoin("documento.listaDocumentonegociacao listaDocumentonegociado")
					.leftOuterJoin("documento.documentoacao documentoacao")
					.join("documentocomissao.contrato contrato")
					.leftOuterJoin("contrato.empresa empresa")
					.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
					.where("pessoa.cdpessoa = ?", colaborador.getCdpessoa())
					.where("documentoacao <> ?", Documentoacao.CANCELADA)
//					.where("documentoacao <> ?", Documentoacao.NEGOCIADA)
					.where("colaboradorcomissao.cdcolaboradorcomissao is null");
					
		
//		QueryBuilder<Documentocomissao> query = query()
//				.select("documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome," +
//						"comissionamento.cdcomissionamento, comissionamento.percentual, comissionamento.valor," +
//						"documento.cddocumento, documento.valor, contrato.cdcontrato, contrato.descricao, contrato.valordedutivocomissionamento, " +
//						"cliente.cdpessoa, cliente.nome, documentoacao.cddocumentoacao, documentoacao.nome," +
//						"colaboradorcomissao.cdcolaboradorcomissao, documentocc.cddocumento, documentocc.valor," +
//						"nota.cdNota, nota.numero, aux_documento.valoratual, documentocomissao.valorcomissao, " +
//						"listaContratocolaborador.cdcontratocolaborador, colaborador.cdpessoa, listaContratocolaborador.ordemcomissao, " +
//						"comissionamentocontrato.percentual, comissionamentocontrato.valor, contrato.valordedutivocomissionamento, " +
//						"comissionamentocontrato.deduzirvalor, vendedor.cdpessoa, aux_documento.valoratual, colaboradorcomissionamento.percentual, " +
//						"colaboradorcomissionamento.valor, documentocomissao.vendedor, " +
//						"comissionamentocontrato.considerardiferencapagamento, comissionamento.considerardiferencapagamento, colaboradorcomissionamento.considerardiferencapagamento")		
//				.leftOuterJoin("documentocomissao.comissionamento comissionamento")
//				.leftOuterJoin("documento.aux_documento aux_documento")
//				.leftOuterJoin("documentocomissao.nota nota")
//				.leftOuterJoin("contrato.cliente cliente")
//				.leftOuterJoin("colaboradorcomissao.documento documentocc")
//				.leftOuterJoin("contrato.contratotipo contratotipo")	
//				.leftOuterJoin("contrato.listaContratocolaborador listaContratocolaborador")
//				.leftOuterJoin("listaContratocolaborador.colaborador colaborador")
//				.leftOuterJoin("contrato.comissionamento comissionamentocontrato")
//				.leftOuterJoin("contrato.vendedor vendedor")
//				.leftOuterJoin("listaContratocolaborador.comissionamento colaboradorcomissionamento")
		
		if(empresa != null && empresa.getCdpessoa() != null){
			query.openParentheses()
				.where("empresa is null")
				.or()
				.where("empresa = ?", empresa)
			.closeParentheses();
		}
		if(tipo != null && "Pagamento".equals(tipo)){
			query.openParentheses();
			query.where("exists (select d.cddocumento " +
							"from Movimentacaoorigem mo " +
							"join mo.documento d " +
							"join mo.movimentacao m " +
							"where m.dtmovimentacao >= '" + dtreferencia1 + "' " +
							"and m.dtmovimentacao <= '" + dtreferencia2 + "' " +
							"and d.cddocumento = documento.cddocumento " +
							")");
			query.or();
			query.where("exists(select d.cddocumento " +
							"from Movimentacaoorigem mo " +
							"join mo.documento d " +
							"join mo.movimentacao m " +
							"where m.dtmovimentacao >= '" + dtreferencia1 + "' " +
							"and m.dtmovimentacao <= '" + dtreferencia2 + "' " +
//							"and d.cddocumento in (select v.cddocumentonegociado from Vdocumentonegociado v where v.cddocumento = documento.cddocumento) " +
							"and (exists(select v.cddocumentonegociado from Vdocumentonegociado v where v.cddocumento=d.cddocumento))" + 

							
							")");
			query.closeParentheses();
		} else if(tipo != null && "Emissao".equals(tipo)){
			query
				.openParentheses()
					.where("documento is null")
					.or()
					.openParentheses()
						.where("documento.dtemissao >= ?", dtreferencia1)
						.where("documento.dtemissao <= ?", dtreferencia2)
						.where("listaDocumentonegociado is null")
					.closeParentheses()
				.closeParentheses();
			
//			query.where("exists (select v.cddocumento " +
//									"from Vdocumentonegociado v " +
//									"where v.cddocumento = documento.cddocumento " +
//									"and v.dtemissaonegociado >= '" + dataInicio + "' " +
//									"and v.dtemissaonegociado <= '" + dataFim + "' )");
		} else {
			query
				.openParentheses()
					.where("documento is null")
					.or()
					.openParentheses()
						.where("documento.dtvencimento >= ?", dtreferencia1)
						.where("documento.dtvencimento <= ?", dtreferencia2)
						.where("listaDocumentonegociado is null")
					.closeParentheses()
				.closeParentheses();
			
//			query.where("exists (select v.cddocumento " +
//									"from Vdocumentonegociado v " +
//									"where v.cddocumento = documento.cddocumento " +
//									"and v.dtvencimentonegociado >= '" + dataInicio + "' " +
//									"and v.dtvencimentonegociado <= '" + dataFim + "' )");
		}
		
//		List<Documentocomissao> lista = query.list();
//		Money total = new Money();
//		if(lista != null && !lista.isEmpty()){
//			for(Documentocomissao dc : lista){				
//				total = total.add(dc.getValorcomissaoListagem());
//			}
//		}	
				
		Long soma = query.unique();
		return soma != null ? new Money(soma, true) : new Money();
	}
	
	/**
	 * M�todo que calcula o total do comissionamento do contrato
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
//	public Money getTotalComissaoColaboradorMes(DocumentocomissaoFiltro filtro) {
//
//		Money total = new Money();
//		
//		QueryBuilder<Documentocomissao> query = query()
//		.select("documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome," +
//				"comissionamento.cdcomissionamento, comissionamento.percentual, comissionamento.valor," +
//				"documento.cddocumento, documento.valor, contrato.cdcontrato, contrato.descricao, contrato.valordedutivocomissionamento, " +
//				"cliente.cdpessoa, cliente.nome, documentoacao.cddocumentoacao, documentoacao.nome," +
//				"colaboradorcomissao.cdcolaboradorcomissao, documentocc.cddocumento, documentocc.valor," +
//				"nota.cdNota, nota.numero, aux_documento.valoratual, documentocomissao.valorcomissao, " +
//				"listaContratocolaborador.cdcontratocolaborador, colaborador.cdpessoa, listaContratocolaborador.ordemcomissao, " +
//				"comissionamentocontrato.percentual, comissionamentocontrato.valor, contrato.valordedutivocomissionamento, " +
//				"comissionamentocontrato.deduzirvalor, vendedor.cdpessoa, aux_documento.valoratual, colaboradorcomissionamento.percentual, " +
//				"colaboradorcomissionamento.valor, documentocomissao.vendedor")		
//		.leftOuterJoin("documentocomissao.pessoa pessoa")
//		.leftOuterJoin("documentocomissao.comissionamento comissionamento")
//		.leftOuterJoin("documentocomissao.documento documento")
//		.leftOuterJoin("documento.documentoacao documentoacao")
//		.leftOuterJoin("documento.aux_documento aux_documento")
//		.join("documentocomissao.contrato contrato")
//		.leftOuterJoin("documentocomissao.nota nota")
//		.leftOuterJoin("contrato.cliente cliente")
//		.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
//		.leftOuterJoin("colaboradorcomissao.documento documentocc")
//		.leftOuterJoin("contrato.contratotipo contratotipo")	
//		.leftOuterJoin("contrato.listaContratocolaborador listaContratocolaborador")
//		.leftOuterJoin("listaContratocolaborador.colaborador colaborador")
//		.leftOuterJoin("contrato.comissionamento comissionamentocontrato")
//		.leftOuterJoin("contrato.vendedor vendedor")
//		.leftOuterJoin("listaContratocolaborador.comissionamento colaboradorcomissionamento")
//		.where("contrato = ? ", filtro.getContrato())
//		.where("contratotipo = ?", filtro.getContratotipo())
//		.where("cliente = ?", filtro.getCliente());
//		
//		
//		if(filtro.getMostrar() != null && filtro.getMostrar().equals("Pagos com pend�ncia de repasse")){
//			List<Documentoacao> lista = new ArrayList<Documentoacao>();
//			lista.add(Documentoacao.BAIXADA);
//			lista.add(Documentoacao.NEGOCIADA);
//			
//			query.where("not exists (select v.cddocumento " +
//									"from Vdocumentonegociado v " +
//									"where v.cddocumento = documento.cddocumento " +
//									"and v.cddocumentoacaonegociado not in(" + CollectionsUtil.listAndConcatenate(lista, "cddocumentoacao", ",") + "))");
//			
//			query.openParentheses();
//			for (Documentoacao documentoacao : lista) {
//				query.where("documentoacao = ?", documentoacao).or();
//			}
//			query.closeParentheses();
//			
//			query.where("colaboradorcomissao.cdcolaboradorcomissao is null");
//		}
//
//		if(filtro.getDocumentocomissaotipoperiodo() != null && filtro.getDocumentocomissaotipoperiodo().equals(Documentocomissaotipoperiodo.PAGAMENTO)){
//			query.openParentheses();
//			query.where("exists (select d.cddocumento " +
//							"from Movimentacaoorigem mo " +
//							"join mo.documento d " +
//							"join mo.movimentacao m " +
//							"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
//							"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
//							"and d.cddocumento = documento.cddocumento " +
//							")");
//			query.or();
//			query.where("exists(select d.cddocumento " +
//							"from Movimentacaoorigem mo " +
//							"join mo.documento d " +
//							"join mo.movimentacao m " +
//							"where m.dtmovimentacao >= '" + filtro.getDtinicio() + "' " +
//							"and m.dtmovimentacao <= '" + filtro.getDtfim() + "' " +
//							"and d.cddocumento in (select v.cddocumentonegociado from Vdocumentonegociado v where v.cddocumento = documento.cddocumento) " +
//							")");
//			query.closeParentheses();
//		}else if(filtro.getDocumentocomissaotipoperiodo() != null && filtro.getDocumentocomissaotipoperiodo().equals(Documentocomissaotipoperiodo.VENCIMENTO)){
//			query.openParentheses();
//			query.where("documento is null");
//			query.or();
//			query
//				.openParentheses()
//				.where("documento.dtvencimento >= ?", filtro.getDtinicio())
//				.where("documento.dtvencimento <= ?", filtro.getDtfim())
//				.closeParentheses();
//			query.or();
//			query.where("exists (select v.cddocumento " +
//									"from Vdocumentonegociado v " +
//									"where v.cddocumento = documento.cddocumento " +
//									"and v.dtvencimentonegociado >= '" + filtro.getDtinicio() + "' " +
//									"and v.dtvencimentonegociado <= '" + filtro.getDtfim() + "' )");
//			query.closeParentheses();
//		} else if(filtro.getDocumentocomissaotipoperiodo() != null && filtro.getDocumentocomissaotipoperiodo().equals(Documentocomissaotipoperiodo.EMISSAO)){
//				query.openParentheses();
//				query.where("documento is null");
//				query.or();
//				query
//					.openParentheses()
//					.where("documento.dtemissao >= ?", filtro.getDtinicio())
//					.where("documento.dtemissao <= ?", filtro.getDtfim())
//					.closeParentheses();
//				query.or();
//				query.where("exists (select v.cddocumento " +
//										"from Vdocumentonegociado v " +
//										"where v.cddocumento = documento.cddocumento " +
//										"and v.dtemissaonegociado >= '" + filtro.getDtinicio() + "' " +
//										"and v.dtemissaonegociado <= '" + filtro.getDtfim() + "' )");
//				query.closeParentheses();
//		}
//		
//		List<Documentocomissao> lista = query.list();
//					
//		if(lista != null && !lista.isEmpty()){
//			for(Documentocomissao dc : lista){				
//				total = total.add(dc.getValorcomissaoListagem());
//			}
//		}		
//				
//		return total;
//	}

	/**
	 * M�todo que deleta o documentocomissao relacionado a venda
	 *
	 * @param venda
	 * @author Luiz Fernando
	 */
	public void deleteDocumentocomissaoByVendaCancelada(Venda venda) {
		if(venda == null || venda.getCdvenda() == null){
			throw new SinedException("Par�metro inv�lido");
		}
		
		getHibernateTemplate().bulkUpdate("delete from Documentocomissao d where d.venda.cdvenda = ? and d.colaboradorcomissao is null", new Object[]{venda.getCdvenda()});
	}
	
	public void deleteDocumentocomissaoByPedidovendaCancelado(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null){
			throw new SinedException("Par�metro inv�lido");
		}
		
		getHibernateTemplate().bulkUpdate("delete from Documentocomissao d where d.pedidovenda.cdpedidovenda = ? and d.colaboradorcomissao is null", new Object[]{pedidovenda.getCdpedidovenda()});
	}
	
	public List<Documentocomissao> loadWithLista(String whereIn){
		QueryBuilder<Documentocomissao> query = query();
		
		query
			.select("distinct documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome," +
					"comissionamento.cdcomissionamento, comissionamento.percentual, comissionamento.valor," +
					"documento.cddocumento, documento.valor, documento.dtvencimento, contrato.cdcontrato, contrato.descricao, contrato.valordedutivocomissionamento, " +
					"cliente.cdpessoa, cliente.nome, documentoacao.cddocumentoacao, documentoacao.nome," +
					"colaboradorcomissao.cdcolaboradorcomissao, documentocc.cddocumento, documentocc.valor," +
					"nota.cdNota, nota.numero, aux_documento.valoratual, documentocomissao.valorcomissao, " +
					"listaContratocolaborador.cdcontratocolaborador, colaborador.cdpessoa, listaContratocolaborador.ordemcomissao, " +
					"comissionamentocontrato.percentual, comissionamentocontrato.valor, contrato.valordedutivocomissionamento, " +
					"comissionamentocontrato.deduzirvalor, vendedor.cdpessoa, aux_documento.valoratual, colaboradorcomissionamento.percentual, " +
					"colaboradorcomissionamento.valor, documentocomissao.vendedor, comissionamento.tipobccomissionamento, notaTipo.cdNotaTipo, " +
					"documentoacaocc.cddocumentoacao, contratotipo.cdcontratotipo, contratotipo.nome, comissionamento.considerardiferencapagamento ")		
			.leftOuterJoin("documentocomissao.pessoa pessoa")
			.leftOuterJoin("documentocomissao.comissionamento comissionamento")
			.leftOuterJoin("documentocomissao.documento documento")
			.leftOuterJoin("documento.documentoacao documentoacao")
			.leftOuterJoin("documento.aux_documento aux_documento")
			.join("documentocomissao.contrato contrato")
			.leftOuterJoin("documentocomissao.nota nota")
			.leftOuterJoin("nota.notaTipo notaTipo")
			.leftOuterJoin("contrato.cliente cliente")
			.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
			.leftOuterJoin("colaboradorcomissao.documento documentocc")
			.leftOuterJoin("documentocc.documentoacao documentoacaocc")
			.leftOuterJoin("contrato.contratotipo contratotipo")	
			.leftOuterJoin("contrato.comissionamento comissionamentocontrato")
			.leftOuterJoin("contrato.vendedor vendedor")
			
			.leftOuterJoin("contrato.listaContratocolaborador listaContratocolaborador")
			.leftOuterJoin("listaContratocolaborador.colaborador colaborador")
			.leftOuterJoin("listaContratocolaborador.comissionamento colaboradorcomissionamento")
			.whereIn("documentocomissao.cddocumentocomissao", whereIn)
			.orderBy("cliente.nome, contrato.descricao, documento.cddocumento");
		
		return query.list();
	}
//	public List<Documentocomissao> findByPagamentoDesempenho(String whereIn) {
//	if(whereIn == null || "".equals(whereIn))
//		throw new SinedException("Par�metro inv�lido");
//	
//	return query()
//		.select("documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome," +
//				"documento.cddocumento, documento.valor, documentocomissao.valorcomissao, " +
//				"cliente.cdpessoa, cliente.nome, documentoacao.cddocumentoacao, documentoacao.nome," +
//				"colaboradorcomissao.cdcolaboradorcomissao, empresa.cdpessoa, empresa.nome ")		
//		.join("documentocomissao.pessoa pessoa")
//		.leftOuterJoin("documentocomissao.documento documento")
//		.leftOuterJoin("documento.aux_documento aux_documento")
//		.leftOuterJoin("documento.documentoacao documentoacao")
//		.join("documentocomissao.venda venda")
//		.leftOuterJoin("venda.cliente cliente")
//		.leftOuterJoin("venda.empresa empresa")
//		.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")		
//		.whereIn("documentocomissao.cddocumentocomissao", whereIn)
//		.list();
//}

	/**
	 * Busca o comissinamento para verificar existencia no rec�lculo.
	 *
	 * @param documento
	 * @param nota
	 * @param pessoa
	 * @param comissionamento
	 * @param contrato
	 * @param vendedor
	 * @return
	 * @since 19/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Documentocomissao> findDocumentocomissao(Documento documento, Nota nota, Pessoa pessoa, 
			Comissionamento comissionamento, Contrato contrato, Boolean vendedor) {
		
		QueryBuilder<Documentocomissao> query = query()
			.select("documentocomissao.cddocumentocomissao, colaboradorcomissao.cdcolaboradorcomissao, comissionamento.cdcomissionamento, documentocomissao.origemvalorhora")
			.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
			.leftOuterJoin("documentocomissao.pessoa pessoa")
			.leftOuterJoin("documentocomissao.documento documento")
			.leftOuterJoin("documentocomissao.nota nota")
			.leftOuterJoin("documentocomissao.contrato contrato")
			.leftOuterJoin("documentocomissao.comissionamento comissionamento")
			.where("pessoa = ?", pessoa)
			.where("contrato = ?", contrato);
			
		
		if(documento != null && documento.getCddocumento() != null){
			query.where("documento = ?", documento);
			if(nota != null && nota.getCdNota() != null){
				query.openParentheses()
					.where("nota = ?", nota)
					.or()
					.where("nota is null")
					.closeParentheses();
			}
		}else {
			query.where("documento is null");
			query.where("nota = ?", nota);
		}	
		if(vendedor != null && vendedor){
			query.where("documentocomissao.vendedor = true");
		}else {
			query.openParentheses()
				.where("documentocomissao.vendedor = false").or()
				.where("documentocomissao.vendedor is null")
				.closeParentheses();
		}
		return query.list();
	}
	
	/**
	 * Busca o comissinamento de documento negociado para excluir, caso o comissionamento seja valor pago;
	 *
	 * @param documento
	 * @param nota
	 * @param pessoa
	 * @param comissionamento
	 * @param contrato
	 * @param vendedor
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentocomissao> findDocumentocomissaoByDocumentonegociado(Documento documento, Nota nota, Pessoa pessoa, 
			Comissionamento comissionamento, Contrato contrato, Boolean vendedor) {
		return query()
					.select("documentocomissao.cddocumentocomissao, colaboradorcomissao.cdcolaboradorcomissao, comissionamento.cdcomissionamento, documentocomissao.origemvalorhora")
					.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
					.leftOuterJoin("documentocomissao.pessoa pessoa")
					.join("documentocomissao.documento documento")
					.leftOuterJoin("documentocomissao.nota nota")
					.leftOuterJoin("documentocomissao.contrato contrato")
					.leftOuterJoin("documentocomissao.comissionamento comissionamento")
					.join("documento.documentoacao documentoacao")
					.where("pessoa = ?", pessoa)
					.where("documento = ?", documento)
					.where("nota = ?", nota)
					.where("contrato = ?", contrato)
					.where("comissionamento = ?", comissionamento)
					.where("comissionamento.tipobccomissionamento = ?", Tipobccomissionamento.VALOR_PAGO)
					.where("documentoacao.cddocumentoacao = ?", Documentoacao.NEGOCIADA.getCddocumentoacao())
					.list();
	}

	/**
	 * Atualiza o valor da comiss�o.
	 *
	 * @param documentocomissao
	 * @param valorcomissao
	 * @since 19/06/2012
	 * @author Rodrigo Freitas
	 */
	public void updateValorComissao(Documentocomissao documentocomissao, Money valorcomissao) {
		if(documentocomissao.getComissionamento() != null && documentocomissao.getComissionamento().getCdcomissionamento() != null){
			getHibernateTemplate().bulkUpdate("update Documentocomissao d set valorcomissao = ?, cdcomissionamento = ? where d.id = ?", new Object[]{
					valorcomissao, documentocomissao.getComissionamento().getCdcomissionamento(),documentocomissao.getCddocumentocomissao()
			});
		}else {
			getHibernateTemplate().bulkUpdate("update Documentocomissao d set valorcomissao = ? where d.id = ?", new Object[]{
					valorcomissao, documentocomissao.getCddocumentocomissao()
			});
		}
	}
	
	public Money findForTotaldocumentoDocumentocomissao(DocumentocomissaoFiltro filtro) {
		QueryBuilder<Documentocomissao> query = query();
		
		query.from(Documentocomissao.class)
			.select("documento.cddocumento, documento.valor")		
			.leftOuterJoin("documentocomissao.pessoa pessoa")
			.leftOuterJoin("documentocomissao.comissionamento comissionamento")
			.leftOuterJoin("documentocomissao.documento documento")
			.leftOuterJoin("documento.listaDocumentonegociacao documentonegociado")
			.leftOuterJoin("documento.documentoacao documentoacao")
			.leftOuterJoin("documento.aux_documento aux_documento")
			.join("documentocomissao.contrato contrato")
			.leftOuterJoin("documentocomissao.nota nota")
			.leftOuterJoin("contrato.cliente cliente")
			.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
			.leftOuterJoin("colaboradorcomissao.documento documentocc")
			.leftOuterJoin("contrato.contratotipo contratotipo");
		
		preencheWhereQueryComissionamento(query, filtro);
		
		List<Documentocomissao> lista = query.list();
		
		Money total = new Money();
		if(SinedUtil.isListNotEmpty(lista)){
			for(Documentocomissao documentocomissao : lista){
				if(documentocomissao.getDocumento() != null && documentocomissao.getDocumento().getValor() != null){
					total = total.add(documentocomissao.getDocumento().getValor());
				}
			}
		}
		
		return total;
	}
	
	private void preencheWhereQueryComissionamento(QueryBuilder<? extends Object> query, DocumentocomissaoFiltro filtro){
		query
		.where("contrato = ? ", filtro.getContrato())
		.where("contratotipo = ?", filtro.getContratotipo())
		.where("cliente = ?", filtro.getCliente());
		
		
		if(filtro.getVendedortipo() != null){
			if(filtro.getVendedortipo().equals(Vendedortipo.COLABORADOR) && filtro.getColaborador() != null  && filtro.getColaborador().getCdpessoa() != null){
				query.where("pessoa.cdpessoa = ?", filtro.getColaborador().getCdpessoa());
			}else if(filtro.getVendedortipo().equals(Vendedortipo.FORNECEDOR) && filtro.getFornecedor() != null  && filtro.getFornecedor().getCdpessoa() != null){
				query.where("pessoa.cdpessoa = ?", filtro.getFornecedor().getCdpessoa());
			}
		}
		
		if(filtro.getMostrar() != null && filtro.getMostrar().equals("Pagos com pend�ncia de repasse")){
			List<Documentoacao> lista = new ArrayList<Documentoacao>();
			lista.add(Documentoacao.BAIXADA);
			lista.add(Documentoacao.NEGOCIADA);
			
			query.openParentheses();
			query.where("documento.cddocumento not in (" +
									"select d.cddocumento " +
									"from Documento d " +
									"where (retorna_array_documento_negociado(d.cddocumento, documento.cddocumento )=true) " + 
									"and d.documentoacao.cddocumentoacao not in(" + CollectionsUtil.listAndConcatenate(lista, "cddocumentoacao", ",") + "))");
			
			query.openParentheses();
			for (Documentoacao documentoacao : lista) {
				query.where("documentoacao = ?", documentoacao).or();
			}
			query.closeParentheses();
			
			
			query.openParentheses();
			query.where("colaboradorcomissao.cdcolaboradorcomissao is null").or();
			query.openParentheses();
			query.where("documentocc is null").or();
			query.where("documentocc.documentoacao = ?", Documentoacao.CANCELADA);
			query.closeParentheses();
			query.closeParentheses();
			
			query.closeParentheses();
		}
		
		if(filtro.getDocumentocomissaotipoperiodo() != null && filtro.getDocumentocomissaotipoperiodo().equals(Documentocomissaotipoperiodo.PAGAMENTO)){
			List<Movimentacaoacao> lista = new ArrayList<Movimentacaoacao>();
			lista.add(Movimentacaoacao.NORMAL);
			lista.add(Movimentacaoacao.CONCILIADA);
			
			query.openParentheses();
			query.where("exists (select d.cddocumento " +
							"from Movimentacaoorigem mo " +
							"join mo.documento d " +
							"join mo.movimentacao m " +
							"join m.movimentacaoacao ma " + 
							"where coalesce(m.dtbanco, m.dtmovimentacao) >= '" + SinedDateUtils.toStringPostgre(filtro.getDtinicio()) + "' " +
							"and coalesce(m.dtbanco, m.dtmovimentacao) <= '" + SinedDateUtils.toStringPostgre(filtro.getDtfim()) + "' " +
							"and d.cddocumento = documento.cddocumento " +
							"and ma.cdmovimentacaoacao in (" + CollectionsUtil.listAndConcatenate(lista, "cdmovimentacaoacao", ",") + ") " +
							")");
			query.where("documentoacao.cddocumentoacao <> ?", Documentoacao.NEGOCIADA.getCddocumentoacao());
			query.where("documentoacao.cddocumentoacao <> ?", Documentoacao.CANCELADA.getCddocumentoacao());
			query.closeParentheses();
		} else if(filtro.getDocumentocomissaotipoperiodo() != null && filtro.getDocumentocomissaotipoperiodo().equals(Documentocomissaotipoperiodo.VENCIMENTO)){
			query.openParentheses();
			query.openParentheses();
			query.where("documento is null");
			query.or();
			query.openParentheses();
			query.where("documentonegociado is null");
			
			if(filtro.getDocumentocomissaoreferenciatipoperiodo() != null && filtro.getDocumentocomissaoreferenciatipoperiodo().equals(Documentocomissaoreferenciatipoperiodo.CONTA_NEGOCIADA)){
				query.where("exists ( " +
									"select d.cddocumento " +
									"from Documento d " +
									"where (retorna_array_documento_negociado(d.cddocumento, documento.cddocumento )=true) " + 
									"and d.dtvencimento >= ? " +
									"and d.dtvencimento <= ? " +
									")", new Object[]{filtro.getDtinicio(), filtro.getDtfim()});
			} else { 
				query
					.where("documento.dtvencimento >= ?", filtro.getDtinicio())
					.where("documento.dtvencimento <= ?", filtro.getDtfim());
			}
				
			query.closeParentheses();
			query.closeParentheses();
			query.where("documentoacao.cddocumentoacao <> ?", Documentoacao.CANCELADA.getCddocumentoacao());
			query.closeParentheses();
		} else if(filtro.getDocumentocomissaotipoperiodo() != null && filtro.getDocumentocomissaotipoperiodo().equals(Documentocomissaotipoperiodo.EMISSAO)){
			query.openParentheses();
			query.openParentheses();
			query.where("documento is null");
			query.or();
			query.openParentheses();
			query.where("documentonegociado is null");
			
			if(filtro.getDocumentocomissaoreferenciatipoperiodo() != null && filtro.getDocumentocomissaoreferenciatipoperiodo().equals(Documentocomissaoreferenciatipoperiodo.CONTA_NEGOCIADA)){
				query.where("exists (" +
									"select d.cddocumento " +
									"from Documento d " +
									"where (retorna_array_documento_negociado(d.cddocumento, documento.cddocumento )=true) " + 
									"and d.dtemissao >= ? " +
									"and d.dtemissao <= ? " +
									")", new Object[]{filtro.getDtinicio(), filtro.getDtfim()});
			} else { 
				query
					.where("documento.dtemissao >= ?", filtro.getDtinicio())
					.where("documento.dtemissao <= ?", filtro.getDtfim());
			}
			
			query.closeParentheses();
			query.closeParentheses();
			query.where("documentoacao.cddocumentoacao <> ?", Documentoacao.CANCELADA.getCddocumentoacao());
			query.closeParentheses();
		}

	}
	
	public List<Documentocomissao> findForExcluircomissao(Contrato contrato, Nota nota, Documento documento, String whereNotInExclusao, String whereNotInExclusaoVendedor) {
		if(contrato == null || contrato.getCdcontrato() == null)
			throw new SinedException("Contato n�o pode ser nulo.");
		
		QueryBuilder<Documentocomissao> query = query();
		
		query
			.select("documentocomissao.cddocumentocomissao, colaboradorcomissao.cdcolaboradorcomissao, documento.cddocumento, documentoacao.cddocumentoacao")
			.join("documentocomissao.contrato contrato")
			.join("documentocomissao.pessoa pessoa")
			.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
			.leftOuterJoin("colaboradorcomissao.documento documento")
			.leftOuterJoin("documento.documentoacao documentoacao")
			.leftOuterJoin("documentocomissao.nota nota")
			.leftOuterJoin("documentocomissao.documento documento2")
			.where("contrato = ?", contrato)
			.where("colaboradorcomissao is null")
			.where("documento is null")
			.where("documentoacao is null")
			.where("documento2 = ?", documento)
			.where("nota = ?", nota);
		
		query.openParentheses();
		if(whereNotInExclusao != null && !"".equals(whereNotInExclusao)){
			query.openParentheses()
				.where("pessoa.cdpessoa not in (" + whereNotInExclusao + ")")
				.where("coalesce(documentocomissao.vendedor, false) = false")
			.closeParentheses();
		}else {
			query.where("1 = 0");
		}
		query.or();
		if(whereNotInExclusaoVendedor != null && !"".equals(whereNotInExclusaoVendedor)){
			query.openParentheses()
				.where("pessoa.cdpessoa not in (" + whereNotInExclusaoVendedor + ")")
				.where("coalesce(documentocomissao.vendedor, false) = true")
			.closeParentheses();
		}else {
			query.where("1 = 0");
		}
		query.closeParentheses();
		
		return query.list();
			
	}

	/**
	 * M�todo que deleta o documentocomissao. 
	 *
	 * @param documentocomissao
	 * @author Luiz Fernando
	 */
	public void deleteDocumentocomissao(Documentocomissao documentocomissao) {
		if(documentocomissao != null && documentocomissao.getCddocumentocomissao() != null){
			getHibernateTemplate().bulkUpdate("delete from Documentocomissao where cddocumentocomissao = ?",
					new Object[]{documentocomissao.getCddocumentocomissao()});
		}
	}
	
	public void deleteDocumentocomissaoByDocumento(Documento doc) {
		if(doc != null && doc.getCddocumento() != null){
			getHibernateTemplate().bulkUpdate("delete from Documentocomissao " +
					"	where cdcolaboradorcomissao is null and cddocumento= ?",
					new Object[]{doc.getCddocumento()});
		}
	}
	
	/**
	 * M�todo que atualiza o cdvenda do documentocomissao com origem do pedido de venda
	 *
	 * @param venda
	 * @param pedidovenda
	 * @author Luiz Fernando
	 */
	public void atualizaDocumentocomissao(Venda venda, Pedidovenda pedidovenda) {
		if(venda == null || venda.getCdvenda() == null ||
				pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		getJdbcTemplate().execute("update documentocomissao set cdvenda = " + venda.getCdvenda() + 
				" where documentocomissao.cdpedidovenda = " + pedidovenda.getCdpedidovenda() + ";");
		
	}
	
	/**
	 * Busca todas os comissionamentos a partir da query da listagem para a gera��o do CSV.
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentocomissao> findForRelarorioCSVListagem(DocumentocomissaoFiltro filtro) {
		QueryBuilder<Documentocomissao> query = querySined();
		this.updateListagemQuery(query, filtro);
		query.select("distinct documentocomissao.cddocumentocomissao, cliente.nome, contrato.descricao, documento.cddocumento, pessoa.cdpessoa, pessoa.nome");
		return query.list();
	}

	/**
	 * Busca os dados para o relat�rio de vis�o colaborador na listagem.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/12/2012
	 */
	public List<Documentocomissao> findForRelatorioComissionamentoColaborador(DocumentocomissaoFiltro filtro) {
		QueryBuilder<Documentocomissao> query = querySined();
		this.updateListagemQuery(query, filtro);
		return query.list();
	}
	
	public List<Documentocomissao> findForGerarArquivoSEFIP(Colaborador colaborador, Date dtinicio, Date dtfim) {

		QueryBuilder<Documentocomissao> queryBuilder = querySined()
			.select("documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome," +
					"comissionamento.cdcomissionamento, comissionamento.percentual, comissionamento.valor," +
					"documento.cddocumento, documento.valor, contrato.cdcontrato, contrato.descricao, contrato.valordedutivocomissionamento, " +
					"cliente.cdpessoa, cliente.nome, documentoacao.cddocumentoacao, documentoacao.nome," +
					"colaboradorcomissao.cdcolaboradorcomissao, documentocc.cddocumento, documentocc.valor," +
					"nota.cdNota, nota.numero, aux_documento.valoratual, documentocomissao.valorcomissao, listaContratocolaborador.valorhora, " +
					"listaContratocolaborador.cdcontratocolaborador, colaborador.cdpessoa, listaContratocolaborador.ordemcomissao, " +
					"comissionamentocontrato.percentual, comissionamentocontrato.valor, contrato.valordedutivocomissionamento, " +
					"comissionamentocontrato.deduzirvalor, vendedor.cdpessoa, aux_documento.valoratual, colaboradorcomissionamento.percentual, " +
					"colaboradorcomissionamento.valor, documentocomissao.vendedor, cliente.cnpj, listaEndereco.logradouro, " +
					"listaEndereco.bairro, listaEndereco.complemento, listaEndereco.cep, listaEndereco.numero, municipio.nome, uf.sigla")		
			.join("documentocomissao.contrato contrato")
			.join("contrato.cliente cliente")
			.join("documentocomissao.pessoa pessoa")
			.leftOuterJoin("documentocomissao.comissionamento comissionamento")
			.join("documentocomissao.documento documento")
			.join("documento.documentoacao documentoacao")
			.join("documento.aux_documento aux_documento")
			.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
			.leftOuterJoin("documentocomissao.nota nota")
			.leftOuterJoin("colaboradorcomissao.documento documentocc")
			.leftOuterJoin("contrato.contratotipo contratotipo")	
			.leftOuterJoin("contrato.listaContratocolaborador listaContratocolaborador")
			.leftOuterJoin("listaContratocolaborador.colaborador colaborador")
			.leftOuterJoin("contrato.comissionamento comissionamentocontrato")
			.leftOuterJoin("contrato.vendedor vendedor")
			.leftOuterJoin("listaContratocolaborador.comissionamento colaboradorcomissionamento")
			.join("cliente.listaEndereco listaEndereco")
			.join("listaEndereco.municipio municipio")
			.join("municipio.uf uf")
			.where("cliente.cnpj is not null")
			.where("pessoa.cdpessoa = ?", colaborador.getCdpessoa())
			.where("documentoacao <> ?", Documentoacao.CANCELADA)
//			.where("documentoacao <> ?", Documentoacao.NEGOCIADA)
			.where("documento.dtvencimento>=?", dtinicio)
			.where("documento.dtvencimento<=?", dtfim)
			.where("documentoacao.cddocumentoacao<>?", Documentoacao.CANCELADA.getCddocumentoacao());
				
		return queryBuilder.list();
	}

	public List<Documentocomissao> findForCalcularQtdeGeradaByContratoPessoa(Contrato contrato, Pessoa pessoa) {
		if(contrato == null || contrato.getCdcontrato() == null || pessoa == null || pessoa.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Documentocomissao> query = query()
			.select("documentocomissao.cddocumentocomissao, comissionamento.cdcomissionamento, " +
					"documento.cddocumento, documento.dtemissao, " +
					"nota.cdNota, nota.dtEmissao ")
			.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
			.leftOuterJoin("documentocomissao.pessoa pessoa")
			.leftOuterJoin("documentocomissao.documento documento")
			.leftOuterJoin("documentocomissao.nota nota")
			.leftOuterJoin("documentocomissao.contrato contrato")
			.leftOuterJoin("documentocomissao.comissionamento comissionamento")
			.where("pessoa = ?", pessoa)
			.where("contrato = ?", contrato)
			.orderBy("documento.dtemissao, documento.cddocumento, nota.dtEmissao, nota.cdNota");	
		return query.list();
	}

	public List<Documentocomissao> buscarComissionamentoPorFaixas(DocumentocomissaoFiltro filtro) {
		QueryBuilder<Documentocomissao> query = query();
		
		query
			.select("documentocomissao.cddocumentocomissao, documentocomissao.valorcomissao, " +
					"venda.cdvenda, venda.valorfrete, venda.taxapedidovenda, venda.desconto, venda.valorusadovalecompra, venda.dtvenda, " +
					"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.descricao, " +
					"listavendamaterial.cdvendamaterial, listavendamaterial.quantidade, listavendamaterial.multiplicador, " +
					"listavendamaterial.preco, listavendamaterial.desconto, " +
					"material.cdmaterial, material.nome, " +
					"faixaMarkupNome.cdFaixaMarkupNome, faixaMarkupNome.nome, " +
					"documento.cddocumento, documento.documentoacao, " +
					"nota.cdNota, " +
					"listaNotaDocumento.cdNotaDocumento, " +
					"notaDocumento.cddocumento, notaDocumento.documentoacao")
			.leftOuterJoin("documentocomissao.venda venda")
			.leftOuterJoin("venda.listavendamaterial listavendamaterial")
			.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
			.leftOuterJoin("venda.colaborador colaborador")
			.leftOuterJoin("documentocomissao.vendamaterial vendamaterial")
			.leftOuterJoin("vendamaterial.faixaMarkupNome faixaMarkupNome")
			.leftOuterJoin("vendamaterial.material material")
			.leftOuterJoin("documentocomissao.documento documento")
			.leftOuterJoin("documentocomissao.nota nota")
			.leftOuterJoin("nota.listaNotaDocumento listaNotaDocumento")
			.leftOuterJoin("listaNotaDocumento.documento notaDocumento")
			.where("venda.dtvenda >= ?", filtro.getDtinicio())
			.where("venda.dtvenda <= ?", filtro.getDtfim())
			.where("venda.colaborador = ?", filtro.getColaborador())
			.where("venda.pedidovendatipo = ?", filtro.getPedidovendatipo());
		
		return query.list();
		
	}
}
