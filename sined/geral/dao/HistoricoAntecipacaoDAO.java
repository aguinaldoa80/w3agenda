package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.HistoricoAntecipacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class HistoricoAntecipacaoDAO extends GenericDAO<HistoricoAntecipacao>{

	public List<HistoricoAntecipacao> findByHistoricoAntecipacao(String whereIn) {
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido.");
			
		return query()
				.select("historicoAntecipacao.cdHistoricoAntecipacao, historicoAntecipacao.valor, documento.cddocumento, documentoReferencia.cddocumento, documentoclasse.cddocumentoclasse ")
				.join("historicoAntecipacao.documento documento")
				.join("historicoAntecipacao.documentoReferencia documentoReferencia")
				.leftOuterJoin("documento.documentoclasse documentoclasse")
				.whereIn("historicoAntecipacao.cdHistoricoAntecipacao", whereIn)
				.list();
	}
	
	public List<HistoricoAntecipacao> findByDocumentoReferencia(Documento documento, Boolean isCompensacao) {
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Par�metro inv�lido.");
			
		QueryBuilder<HistoricoAntecipacao> query = query()
				.select("historicoAntecipacao.cdHistoricoAntecipacao, historicoAntecipacao.valor, documento.cddocumento, documentoReferencia.cddocumento, historicoAntecipacao.dataHora, historicoAntecipacao.dtBaixa ")
				.join("historicoAntecipacao.documento documento")
				.join("historicoAntecipacao.documentoReferencia documentoReferencia")
				.where("historicoAntecipacao.documentoReferencia = ?", documento)
				.where("documento.documentoacao.cddocumentoacao in (" + Documentoacao.BAIXADA.getCddocumentoacao() + "," + Documentoacao.BAIXADA_PARCIAL.getCddocumentoacao() + ")")
				;
		
		if(isCompensacao) {
			query.where("historicoAntecipacao.compensacao = true");
		}
		
		return query.list();
	}
	
	public List<HistoricoAntecipacao> findByDocumento(Documento documento) {
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Par�metro inv�lido.");
			
		return query()
				.select("historicoAntecipacao.cdHistoricoAntecipacao, historicoAntecipacao.valor, documento.cddocumento, historicoAntecipacao.dataHora ")
				.join("historicoAntecipacao.documento documento")
				.where("historicoAntecipacao.documento = ?", documento)
				.where("documento.documentoacao.cddocumentoacao in (" + Documentoacao.BAIXADA.getCddocumentoacao() + "," + Documentoacao.BAIXADA_PARCIAL.getCddocumentoacao() + ")")
				.list();
	}
	
	public Boolean existeAntecipacao(String whereInDocumento) {
		if(StringUtils.isBlank(whereInDocumento))
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.from(HistoricoAntecipacao.class)
					.join("historicoAntecipacao.documento documento")
					.whereIn("historicoAntecipacao.documentoReferencia.cddocumento", whereInDocumento)
					.where("documento.documentoacao.cddocumentoacao in (" + Documentoacao.BAIXADA.getCddocumentoacao() + "," + Documentoacao.BAIXADA_PARCIAL.getCddocumentoacao() + ")")
					.unique() > 0;
	}
	
	public boolean isContaGeradaAPartirDeCompensacaoDeSaldo(Documento documento) {
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.from(HistoricoAntecipacao.class)
				.where("historicoAntecipacao.documentoReferencia = ?", documento)
				.where("historicoAntecipacao.compensacao = true")
				.unique() > 0;
				
	}
}