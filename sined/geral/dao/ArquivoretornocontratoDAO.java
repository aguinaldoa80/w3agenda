package br.com.linkcom.sined.geral.dao;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Arquivoretornocontrato;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ArquivoretornocontratoDAO extends GenericDAO<Arquivoretornocontrato> {

	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}

	public boolean existeArquivo(String numeroremessa) {
		if(StringUtils.isBlank(numeroremessa))
			return false;
		
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(Arquivoretornocontrato.class)
				.where("numeroremessa = ?", numeroremessa)
				.unique() > 0;
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				
				Arquivoretornocontrato arquivoretornocontrato = (Arquivoretornocontrato) save.getEntity();
				if(arquivoretornocontrato.getArquivo() != null){
					arquivoDAO.saveFile(arquivoretornocontrato, "arquivo");
				}
				
				return null;
			}
		});
	}
}
