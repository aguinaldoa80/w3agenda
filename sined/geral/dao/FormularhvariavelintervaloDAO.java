package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Formularhvariavel;
import br.com.linkcom.sined.geral.bean.Formularhvariavelintervalo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FormularhvariavelintervaloDAO extends GenericDAO<Formularhvariavelintervalo>{

	public List<Formularhvariavelintervalo> carregaIntervalos(Formularhvariavel formularhvariavel){
		
		QueryBuilder<Formularhvariavelintervalo> query = query()			
			.where("formularhvariavelintervalo.formularhvariavel = ?",formularhvariavel);
		return query.list();
	}
	
}