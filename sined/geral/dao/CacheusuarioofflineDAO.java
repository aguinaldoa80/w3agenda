package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;

import br.com.linkcom.sined.geral.bean.Cacheusuariooffline;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CacheusuarioofflineDAO extends GenericDAO<Cacheusuariooffline> {

	/**
	 * M�todo que retorna o timestamp (dtaltera) de determinado usuario ou nulo caso este n�o esteja listado na tabela.
	 * @param cdusuario
	 * @return
	 * 
	 * @author Rafael Salvio
	 */
	public Timestamp getDtalteraByUsuario(Integer cdusuario){
		Cacheusuariooffline cacheusuariooffline = query().where("cacheusuariooffline.cdusuarioaltera = ?", cdusuario).unique();
		
		if(cacheusuariooffline != null)
			return cacheusuariooffline.getDtaltera();
		
		return null;
	}

	/**
	 * M�todo que remove todas a entradas da tabela Cacheusuariooffline.
	 * 
	 * @author Rafael Salvio
	 */
	public void resetCacheusuariooffline(){
		getJdbcTemplate().execute("DELETE FROM cacheusuariooffline");
	}
}
