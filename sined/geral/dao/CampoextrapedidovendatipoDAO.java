package br.com.linkcom.sined.geral.dao;

import java.util.Collections;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Campoextrapedidovendatipo;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CampoextrapedidovendatipoDAO  extends GenericDAO<Campoextrapedidovendatipo>{

	/**
	 * Busca todos os campos extras de um determinado {@link Pedidovendatipo}
	 * @param pedidovendatipo
	 * @return
	 */
	public List<Campoextrapedidovendatipo> findByTipo(Pedidovendatipo tipo) {
		
		if(tipo == null  || tipo.getCdpedidovendatipo() == null){
			return Collections.emptyList();
		}
		
		return querySined()
	   		 .select("campoextrapedidovendatipo.cdcampoextrapedidovendatipo, campoextrapedidovendatipo.nome, " +
	   		 		"campoextrapedidovendatipo.obrigatorio, campoextrapedidovendatipo.ordem")
	   		 		.where("campoextrapedidovendatipo.pedidovendatipo = ?", tipo)
	   		 		.orderBy("campoextrapedidovendatipo.ordem")
	   		 		.list();
	}

}
