package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorescala;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.enumeration.ColaboradorescalaSituacao;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ColaboradorescalaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColaboradorescalaDAO extends GenericDAO<Colaboradorescala> {
 
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Colaboradorescala> query) {
		
		query
			.select("colaboradorescala.cdcolaboradorescala, colaboradorescala.dtescala, colaboradorescala.horainicio, " +
					"colaboradorescala.horafim, colaboradorescala.horarealizada, colaboradorescala.situacao, colaboradorescala.cdusuarioaltera, " +
					"colaboradorescala.dtaltera, colaborador.cdpessoa, colaborador.nome, contrato.cdcontrato, cliente.cdpessoa, " +
					"listaSubstituto.cdcolaboradorescala, colab.cdpessoa, listaSubstituto.horainicio, listaSubstituto.horafim, " +
					"escalasubstituida.cdcolaboradorescala")
			.join("colaboradorescala.colaborador colaborador")
			.join("colaboradorescala.contrato contrato")
			.join("contrato.cliente cliente")
			.leftOuterJoin("colaboradorescala.listaSubstituto listaSubstituto")
			.leftOuterJoin("listaSubstituto.colaborador colab")
			.leftOuterJoin("colaboradorescala.escalasubstituida escalasubstituida")
			.orderBy("colaboradorescala.dtescala");		
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Colaboradorescala> query, FiltroListagem _filtro) {
		ColaboradorescalaFiltro filtro = (ColaboradorescalaFiltro) _filtro;
		
		query
			.select("colaboradorescala.cdcolaboradorescala, colaboradorescala.dtescala, colaboradorescala.horainicio, " +
					"colaboradorescala.horafim, colaboradorescala.horarealizada, colaboradorescala.situacao, contrato.descricao")
			.join("colaboradorescala.contrato contrato")
			.join("colaboradorescala.colaborador colaborador")
			.where("colaborador = ?", filtro.getColaborador())
			.where("contrato = ?", filtro.getContrato())
			.where("colaboradorescala.dtescala >= ?", filtro.getDtescala1())
			.where("colaboradorescala.dtescala <= ?", filtro.getDtescala2());
		
		if(filtro.getColaborador() == null || filtro.getDtescala1() == null || filtro.getDtescala2() == null){
			query.where("1=0");
		}
		
	}

	/**
	 * Atualiza a situa��o da escala. Ambas passadas por par�metro.
	 *
	 * @param load
	 * @param situacao
	 * @author Rodrigo Freitas
	 */
	public void atualizaSituacao(Colaboradorescala load, ColaboradorescalaSituacao situacao) {
		if(load == null || load.getCdcolaboradorescala() == null){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		getHibernateTemplate().bulkUpdate("update Colaboradorescala ce set ce.situacao = ? where ce = ?", 
											new Object[]{situacao, load});
	}

	/**
	 * Retorna a escala que foi substitu�da.
	 *
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Colaboradorescala getBeanFalta(Colaboradorescala bean) {
		return query()
				.select("substituto.cdcolaboradorescala")
				.leftOuterJoin("colaboradorescala.escalasubstituida substituto")
				.where("colaboradorescala = ?", bean)
				.unique()
				.getEscalasubstituida();
	}

	/**
	 * Deleta as escalas substitutas da escala passada por par�metro.
	 *
	 * @param colaboradorescala
	 * @author Rodrigo Freitas
	 */
	public void deletaSubstitutos(Colaboradorescala colaboradorescala) {
		getJdbcTemplate().execute("DELETE FROM COLABORADORESCALA WHERE CDESCALASUBSTITUIDA = " + colaboradorescala.getCdcolaboradorescala());
	}

	/**
	 * Carrega as escalas de um colaborador em um determinado espa�o de tempo.
	 *
	 * @param colaborador
	 * @param dtescala1
	 * @param dtescala2
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Colaboradorescala> findByColaboradorData(Colaborador colaborador, Date dtescala1, Date dtescala2, Contrato contrato) {
		return query()
				.select("colaboradorescala.cdcolaboradorescala, colaboradorescala.dtescala, colaboradorescala.horainicio, " +
					"colaboradorescala.horafim, colaboradorescala.horarealizada, colaboradorescala.situacao, colaboradorescala.cdusuarioaltera, " +
					"colaboradorescala.dtaltera, colaborador.cdpessoa, contrato.cdcontrato, cliente.cdpessoa, " +
					"listaSubstituto.cdcolaboradorescala, colab.cdpessoa, listaSubstituto.horainicio, listaSubstituto.horafim, " +
					"escalasubstituida.cdcolaboradorescala")
				.join("colaboradorescala.colaborador colaborador")
				.join("colaboradorescala.contrato contrato")
				.join("contrato.cliente cliente")
				.leftOuterJoin("colaboradorescala.listaSubstituto listaSubstituto")
				.leftOuterJoin("listaSubstituto.colaborador colab")
				.leftOuterJoin("colaboradorescala.escalasubstituida escalasubstituida")
				.where("colaborador = ?", colaborador)
				.where("contrato = ?", contrato)
				.where("colaboradorescala.dtescala >= ?", dtescala1)
				.where("colaboradorescala.dtescala <= ?", dtescala2)
				.list();
	}

	/**
	 * Faz a totaliza��o da listagem de escalas do colaborador.
	 *
	 * @param filtro
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public void totalizarListagem(ColaboradorescalaFiltro filtro) {
		SinedUtil.markAsReader();
		List<ColaboradorescalaFiltro> listaUpdate = getJdbcTemplate().query(
				"SELECT SUM(cast(TO_CHAR(CE.HORAFIM,'HH24')as decimal) -  cast(TO_CHAR(CE.HORAINICIO,'HH24')as decimal) " +
				"+(cast(TO_CHAR(CE.HORAFIM,'MI')as decimal) -  cast(TO_CHAR(CE.HORAINICIO,'MI')as decimal)/60)) AS PREVISTA,  " +
				"SUM(CE.HORAREALIZADA) AS REALIZADA " +
				"FROM COLABORADORESCALA CE " +
				"WHERE CE.CDCOLABORADOR = " + filtro.getColaborador().getCdpessoa() + " " +
				(filtro.getContrato() != null && filtro.getContrato().getCdcontrato() != null ? "AND CE.CDCONTRATO = " + filtro.getContrato().getCdcontrato() + " ": "") + 
				"AND CE.DTESCALA >= TO_DATE('"+ new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtescala1()) +"','DD/MM/YYYY') " +
				"AND CE.DTESCALA <= TO_DATE('"+ new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtescala2()) +"','DD/MM/YYYY')", 
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				ColaboradorescalaFiltro bean = new ColaboradorescalaFiltro();
				bean.setHoraPrevista(rs.getDouble("PREVISTA"));
				bean.setHoraRealizada(rs.getDouble("REALIZADA"));
				return bean;
			}
		});
		
		if(listaUpdate != null && listaUpdate.size() > 0){
			ColaboradorescalaFiltro bean = listaUpdate.get(0);
			filtro.setHoraPrevista(bean.getHoraPrevista());
			filtro.setHoraRealizada(bean.getHoraRealizada());
		}
	}

	/**
	 * Carrega informa��es para o preenchimento do relat�rio.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Colaboradorescala> findForReport(ColaboradorescalaFiltro filtro) {
		return querySined()
					.select("colaboradorescala.cdcolaboradorescala, colaboradorescala.dtescala, colaboradorescala.horainicio, " +
							"colaboradorescala.horafim, colaboradorescala.horarealizada, colaboradorescala.situacao, contrato.descricao")
					.join("colaboradorescala.contrato contrato")
					.join("colaboradorescala.colaborador colaborador")
					.where("colaborador = ?", filtro.getColaborador())
					.where("contrato = ?", filtro.getContrato())
					.where("colaboradorescala.dtescala >= ?", filtro.getDtescala1())
					.where("colaboradorescala.dtescala <= ?", filtro.getDtescala2())
					.orderBy("colaboradorescala.dtescala")
					.list();
	}
	
}
