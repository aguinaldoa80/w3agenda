package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Veiculoferiado;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.VeiculoferiadoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("veiculoferiado.nome")
public class VeiculoferiadoDAO extends GenericDAO<Veiculoferiado>{

	@Override
	public void updateListagemQuery(QueryBuilder<Veiculoferiado> query, FiltroListagem _filtro) {
		
		VeiculoferiadoFiltro filtro = (VeiculoferiadoFiltro) _filtro;
		
		query
			.select("veiculoferiado.cdveiculoferiado, veiculoferiado.nome, veiculoferiado.data")
			.whereLikeIgnoreAll("veiculoferiado.nome", filtro.getNome())
			.where("veiculoferiado.data >= ?", filtro.getData1())
			.where("veiculoferiado.data <= ?", filtro.getData2());
	}
	
	/**
	 * <b>M�todo respons�vel em retornar uma lista contendo os feriados dentro do intervalo passado</b>
	 * @param firtDate data inicial
	 * @param secondDate data final
	 * @return uma lista contendo os feriados dentro do intervalo passado
	 */
	public List<Veiculoferiado> listaIntervaloFeriado(Date firtDate, Date secondDate){
		if(firtDate == null || secondDate == null){
			throw new SinedException("O objeto n�o pode ser nulo.");
		}
		return query()
				.select("veiculoferiado.cdveiculoferiado, veiculoferiado.nome, veiculoferiado.data")
				.where("veiculoferiado.data >=?",firtDate)
				.where("veiculoferiado.data <=?", secondDate)
				.list()
				;
	}
	
	/**
	 * <b>M�todo respons�vel em retorna um Boolean se o data enviada � feriado ou n�o</b> 
	 * @param date data enviada 
	 * @return {@link Boolean}
	 */
	public Boolean isFeriado(Date date){
		if(date == null){
			throw new SinedException("O objeto n�o pode ser nulo.");
		}
		Veiculoferiado feriado = query()
			.select("veiculoferiado.cdveiculoferiado, veiculoferiado.nome, veiculoferiado.data")
			.where("veiculoferiado.data =?",date)
			.unique()
			;
		if(feriado == null){
			return true;
		}else {
			return false;
		}
	}
}
