package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorsituacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.MotivoReprovacaoFaturamento;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Regiao;
import br.com.linkcom.sined.geral.bean.Regiaolocal;
import br.com.linkcom.sined.geral.bean.Regiaovendedor;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentosituacao;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Comprovantestatus;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.geral.bean.enumeration.TipoVendedorEnum;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.VendaorcamentoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EmitircustovendaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

public class VendaorcamentoDAO extends GenericDAO<Vendaorcamento> {
	
	private EmpresaService empresaService;
	private MaterialcategoriaService materialcategoriaService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {
		this.materialcategoriaService = materialcategoriaService;
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Vendaorcamento> query) {
		query		
		 //.leftOuterJoinFetch("vendaorcamento.colaborador colaborador")
	     //.leftOuterJoinFetch("vendaorcamento.empresa empresa")
	     //.leftOuterJoinFetch("empresa.listaEndereco listaEndereco")
	     //.leftOuterJoinFetch("vendaorcamento.cliente cliente")
	     .leftOuterJoinFetch("vendaorcamento.clienteindicacao clienteindicacao")
	     .leftOuterJoinFetch("vendaorcamento.vendaorcamentosituacao vendaorcamentosituacao")
	     .leftOuterJoinFetch("vendaorcamento.frete frete")
	     .leftOuterJoinFetch("vendaorcamento.endereco endereco")
	     .leftOuterJoinFetch("vendaorcamento.pedidovendatipo pedidovendatipo")
	     .leftOuterJoinFetch("endereco.municipio municipio")
	     .leftOuterJoinFetch("municipio.uf uf")
	     .leftOuterJoinFetch("vendaorcamento.enderecoFaturamento enderecoFaturamento")
	     .leftOuterJoinFetch("enderecoFaturamento.municipio municipioFaturamento")
	     .leftOuterJoinFetch("municipioFaturamento.uf ufFaturamento")
	     .leftOuterJoinFetch("vendaorcamento.localarmazenagem localarmazenagem");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Vendaorcamento> query,
			FiltroListagem _filtro) {
	
		VendaorcamentoFiltro filtro = (VendaorcamentoFiltro) _filtro;		
		
		/**
		 * 	filtro dependente de Projeto (JOIN TEM QUE FICAR EM PRIMEIRO NA QUERY PARA OTIMIZA��O) 
		 */
		if(filtro.getProjeto() != null && filtro.getProjeto().getCdprojeto() != null){
			query.leftOuterJoin("vendaorcamento.projeto projeto")
				.where("projeto = ?", filtro.getProjeto())
				.ignoreJoin("projeto");
		}
		
		query
		.select("distinct vendaorcamento.cdvendaorcamento, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.exibiripivenda, " +
				"cliente.nome, cliente.identificador, contacrm.cdcontacrm, contacrm.nome, vendaorcamento.dtorcamento, vendaorcamento.valorfrete, " +
				"endereco.cdendereco, endereco.cep, endereco.complemento, endereco.numero, endereco.logradouro, vendaorcamento.valorusadovalecompra, " +
				"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.nome, uf.sigla, vendaorcamento.vendaorcamentosituacao, vendaorcamento.desconto, vendaorcamento.dtvalidade, " +
				"endereco.substituirlogradouro, endereco.descricao")
//		 .leftOuterJoin("vendaorcamento.listavendaorcamentomaterial vendaorcamentomaterial")
//		 .leftOuterJoin("vendaorcamentomaterial.material material")
//		 .leftOuterJoin("material.materialmestregrade materialmestregrade")
//		 .leftOuterJoin("material.materialgrupo materialgrupo")
//		 .leftOuterJoin("material.materialtipo materialtipo")
//		 .leftOuterJoin("material.listaFornecedor listaFornecedor")
	     .leftOuterJoin("vendaorcamento.empresa empresa")
	     .leftOuterJoin("vendaorcamento.localarmazenagem localarmazenagem")
	     .leftOuterJoin("vendaorcamento.vendaorcamentosituacao vendaorcamentosituacao")
	     .leftOuterJoin("vendaorcamento.cliente cliente")
	     .leftOuterJoin("vendaorcamento.contacrm contacrm")
	     .leftOuterJoin("cliente.listaPessoacategoria listaPessoacategoria")
	     .leftOuterJoin("vendaorcamento.clienteindicacao clienteindicacao")
	     .leftOuterJoin("vendaorcamento.colaborador colaborador")
	     .leftOuterJoin("vendaorcamento.endereco endereco")
	     .leftOuterJoin("vendaorcamento.listaOrcamentovalorcampoextra listaOrcamentovalorcampoextra")
	     .leftOuterJoin("endereco.municipio municipio")
	     .leftOuterJoin("municipio.uf uf")
	     .leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
	     .leftOuterJoin("listaClientevendedor.colaborador colaboradorcliente")
	     .where("localarmazenagem = ?", filtro.getLocalarmazenagem())
	     .where("vendaorcamento.tipopessoa = ?",filtro.getTipopessoa())
	     .where("cliente = ?",filtro.getCliente())
	     .where("clienteindicacao = ?",filtro.getClienteindicacao())
	     .where("contacrm = ?", filtro.getContacrm())
	     .where("empresa = ?",filtro.getEmpresa())
	     .where("vendaorcamento.dtorcamento >= ?", filtro.getDtinicio())
	     .where("vendaorcamento.dtorcamento <= ?", filtro.getDtfim())
	     .whereIn("vendaorcamento.cdvendaorcamento", filtro.getWhereIncdvendaorcamento())
	     .where("listaPessoacategoria.categoria=?", filtro.getCategoria())
	     .where("vendaorcamento.pedidovendatipo = ?",filtro.getPedidovendatipo())
	     .where("vendaorcamento.identificador = ?", filtro.getIdentificador())
	     .whereIn("vendaorcamento.identificadorexterno", SinedUtil.montaWhereInStringVersaoNova(filtro.getIdentificacaoexterna(),";"))
	     .orderBy("vendaorcamento.dtorcamento desc, vendaorcamento.cdvendaorcamento desc") 
	     .ignoreJoin("vendaorcamentomaterial", "material", "materialgrupo", "materialtipo", "listaClientevendedor", "colaboradorcliente",
	    		 "listaOrcamentovalorcampoextra", "materialmestregrade", "listaFornecedor", "fornecedorRepresentacao", "materialcategoria", "listaPessoacategoria", "listavendaorcamentomaterialmestre");
		
		preencheWhereQueryVendaorcamento(query, filtro);
		
		if(Boolean.TRUE.equals(filtro.getIsRestricaoVendaVendedor())){ 
			Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
			if(colaborador == null) {
				query.where("contacrm is not null");
			}else {
				if(!Boolean.TRUE.equals(filtro.getIsRestricaoClienteVendedor())){
					filtro.setColaborador(colaborador);
					query.where("colaborador = ?", colaborador);					
				}else {
					if(filtro.getColaborador() == null || !filtro.getColaborador().equals(colaborador)){
						if(filtro.getColaborador() == null){
							query.openParentheses()
								.where("colaboradorcliente = ?", colaborador)
								.or()
								.where("colaborador = ?", colaborador)
							.closeParentheses();
						}else {
							query.openParentheses()
								.where("colaboradorcliente = ?", colaborador)
								.where("colaborador = ?", filtro.getColaborador())
							.closeParentheses();
						}
					}else {
						query.openParentheses()
							.where("colaborador = ?", colaborador)
						.closeParentheses();
					}
					
				}
			}
		} else {
			if (filtro.getColaborador() != null && filtro.getTipoVendedor() != null && TipoVendedorEnum.PRINCIPAL.equals(filtro.getTipoVendedor())) {
				query
					.openParentheses()
						.where("listaClientevendedor.colaborador = ?", filtro.getColaborador())
						.where("listaClientevendedor.principal = ?", Boolean.TRUE)
					.closeParentheses();
			} else {
				query.where("colaborador = ?", filtro.getColaborador());
			}
		}
		
		if(filtro.getNaoexibirresultado() != null && filtro.getNaoexibirresultado()){
			query.where("1=0");
		}
		
		if(filtro.getRegiao() != null){
			Regiao regiao = filtro.getRegiao();
			if(regiao.getListaRegiaolocal() != null && regiao.getListaRegiaolocal().size() > 0){
				query.openParentheses();
				for (Regiaolocal regiaolocal : regiao.getListaRegiaolocal()) {
					query
						.openParentheses()
						.where("endereco.cep >= ?", regiaolocal.getCepinicio())
						.where("endereco.cep <= ?", regiaolocal.getCepfinal())
						.closeParentheses()
						.or();
				}
				query.closeParentheses();
			}
			if(regiao.getListaRegiaovendedor() != null && regiao.getListaRegiaovendedor().size() > 0){
				query.openParentheses();
				for (Regiaovendedor regiaovendedor : regiao.getListaRegiaovendedor()) {
					query.where("vendaorcamento.colaborador = ?", regiaovendedor.getColaborador()).or();
				}
				query.closeParentheses();
			}
		}
		
		List<Vendaorcamentosituacao> listaVendaorcamentosituacao = filtro.getListaVendaorcamentosituacao();
		if (listaVendaorcamentosituacao != null && !listaVendaorcamentosituacao.isEmpty()){
			query.whereIn("vendaorcamento.vendaorcamentosituacao.cdvendaorcamentosituacao", CollectionsUtil.listAndConcatenate(listaVendaorcamentosituacao,"cdvendaorcamentosituacao", ","));
		}
		
		if (filtro.getEnderecoStr()!=null && !filtro.getEnderecoStr().trim().equals("")){
			query.whereLikeIgnoreAll("(coalesce(endereco.logradouro,'') || coalesce(endereco.numero,'') || coalesce(endereco.complemento,'') || " +
									 " coalesce(endereco.bairro,'') || coalesce(endereco.cep,'') || coalesce(endereco.pontoreferencia,'') || " +
									 " coalesce(municipio.nome,'') || coalesce(uf.sigla,''))", filtro.getEnderecoStr());
		}
		
		String solicitarCompraVenda = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.SOLICITARCOMPRAVENDA);
		if(solicitarCompraVenda != null){
			if("SOLICITACAOCOMPRA".equalsIgnoreCase(solicitarCompraVenda)) {
				//TRUE = Pedidos em aberto comprados | FALSE = Pedidos em processo de compra | NULL = Todos
				if(filtro.getOrcamentoprocessocompra() != null){
					if(filtro.getOrcamentoprocessocompra()){
						query.where("(select count(*) From Solicitacaocompra sc " +
								"join sc.solicitacaocompraorigem sco " +
								"join sco.vendaorcamento scovo " +
								"where scovo.cdvendaorcamento = vendaorcamento.cdvendaorcamento) > 0 and " +
								"(select count(*) From Solicitacaocompra sc " +
								"join sc.solicitacaocompraorigem sco " +
								"join sco.vendaorcamento scovo " +
								"where scovo.cdvendaorcamento = vendaorcamento.cdvendaorcamento) = (select count(*) From Solicitacaocompra sc " +
								"join sc.solicitacaocompraorigem sco " +
								"join sco.vendaorcamento scovo " +
								"join sc.aux_solicitacaocompra aux_sc " +
								"where scovo.cdvendaorcamento = vendaorcamento.cdvendaorcamento and aux_sc.situacaosuprimentos = ?)", Situacaosuprimentos.BAIXADA);
					}else {
						query.where("(select count(*) From Solicitacaocompra sc " +
								"join sc.solicitacaocompraorigem sco " +
								"join sco.vendaorcamento scovo " +
								"join sc.aux_solicitacaocompra aux_sc " +
								"where scovo.cdvendaorcamento = vendaorcamento.cdvendaorcamento and aux_sc.situacaosuprimentos <> ?)", Situacaosuprimentos.BAIXADA);
					}
				}
			}else if("ORDEMCOMPRA".equalsIgnoreCase(solicitarCompraVenda)) {
				//TRUE = Pedidos em aberto comprados | FALSE = Pedidos em processo de compra | NULL = Todos
				if(filtro.getOrcamentoprocessocompra() != null){
					if(filtro.getOrcamentoprocessocompra()){
						query.where("(select count(*) From Ordemcompra oc " +
								" join oc.aux_ordemcompra aux_ordemcompra " + 
								" join oc.listaMaterial listaMaterial " +
								" join listaMaterial.vendaorcamento v" +
								" where v.cdvendaorcamento = vendaorcamento.cdvendaorcamento) > 0 and " +
								" (select count(*) From Ordemcompra oc " +
								" join oc.listaMaterial listaMaterial " +
								" join oc.aux_ordemcompra aux_ordemcompra " +
								" join listaMaterial.vendaorcamento v " +
								" where v.cdvendaorcamento = vendaorcamento.cdvendaorcamento ) = (select count(*) From Ordemcompra oc " +
								" join oc.aux_ordemcompra aux_ordemcompra " +
								" join oc.listaMaterial listaMaterial " +
								" join listaMaterial.vendaorcamento v" +
								" where v.cdvendaorcamento = vendaorcamento.cdvendaorcamento and aux_ordemcompra.situacaosuprimentos = ?)", Situacaosuprimentos.BAIXADA);
					}else {
						query.where("(select count(*) From Ordemcompra oc " +
								" join oc.aux_ordemcompra aux_ordemcompra " +
								" join oc.listaMaterial listaMaterial " +
								" join listaMaterial.vendaorcamento v" +
								" where v.cdvendaorcamento = vendaorcamento.cdvendaorcamento and aux_ordemcompra.situacaosuprimentos <> ?) > 0", Situacaosuprimentos.BAIXADA);
					}
				}	
			}
		}
		
	}
	
	private void joinVendaorcamentomaterialUpdatelistagem(QueryBuilder<? extends Object> query){
		query
		 .leftOuterJoin("vendaorcamento.listavendaorcamentomaterial vendaorcamentomaterial")
		 .leftOuterJoin("vendaorcamentomaterial.material material")
		 .leftOuterJoin("material.materialmestregrade materialmestregrade")
		 .leftOuterJoin("material.materialgrupo materialgrupo")
		 .leftOuterJoin("material.materialtipo materialtipo")
		 .leftOuterJoin("material.listaFornecedor listaFornecedor");
	}
	
	private void preencheWhereQueryVendaorcamento(QueryBuilder<? extends Object> query, VendaorcamentoFiltro filtro){
		if(filtro.getMaterial() != null || filtro.getMaterialgrupo() != null ||filtro.getMaterialtipo() != null || 
				filtro.getFornecedorRepresentacao() != null || filtro.getMaterialcategoria() != null){
			query.leftOuterJoinIfNotExists("vendaorcamento.listavendaorcamentomaterial vendaorcamentomaterial");
			
			if(filtro.getMaterial() != null || filtro.getMaterialgrupo() != null || filtro.getMaterialtipo() != null ||
				filtro.getMaterialcategoria() != null){
				query.leftOuterJoinIfNotExists("vendaorcamentomaterial.material material");
				query.leftOuterJoinIfNotExists("material.materialmestregrade materialmestregrade");
				
				if(filtro.getMaterialgrupo() != null){
					query.leftOuterJoinIfNotExists("material.materialgrupo materialgrupo");
					query.where("materialgrupo = ?", filtro.getMaterialgrupo());
				}
				
				if(filtro.getMaterialtipo() != null){
					query.leftOuterJoinIfNotExists("material.materialtipo materialtipo");
					query.where("materialtipo = ?", filtro.getMaterialtipo());
				}
				
				if(filtro.getMaterialcategoria() != null){
//					query.leftOuterJoinIfNotExists("material.materialcategoria materialcategoria");
//					query.where("material.materialcategoria = ?", filtro.getMaterialcategoria());
					materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
					query.whereIn("material.materialcategoria.cdmaterialcategoria", CollectionsUtil.listAndConcatenate(materialcategoriaService.findForFitlro(filtro.getMaterialcategoria().getIdentificador()), "cdmaterialcategoria", ","));
				}
				
				if(filtro.getMaterial() != null){
					query.leftOuterJoinIfNotExists("material.materialmestregrade materialmestregrade");
					query.leftOuterJoinIfNotExists("vendaorcamento.listavendaorcamentomaterialmestre listavendaorcamentomaterialmestre");
					
					query.openParentheses()
					.where("material = ?", filtro.getMaterial())
					.or()
					.where("vendaorcamentomaterial.materialmestre = ?", filtro.getMaterial())
					.or()
					.where("listavendaorcamentomaterialmestre.material = ?", filtro.getMaterial())
					.or()
					.where("materialmestregrade = ?", filtro.getMaterial());
					if(filtro.getMaterial().getVendapromocional() != null && filtro.getMaterial().getVendapromocional()){
						query.leftOuterJoinIfNotExists("vendaorcamentomaterial.materialmestre materialmestre");
						query.ignoreJoin("materialmestre");
						query.or().where("materialmestre = ?", filtro.getMaterial());
					}
					query.closeParentheses();
				}
			}
			
			if(filtro.getFornecedorRepresentacao() != null){
				query.leftOuterJoinIfNotExists("vendaorcamentomaterial.fornecedor fornecedorRepresentacao");
				query.where("fornecedorRepresentacao = ?", filtro.getFornecedorRepresentacao());
			}
		}
		
		if(filtro.getCampoextrapedidovendatipo() != null 
				&& filtro.getCampoextrapedidovendatipo().getCdcampoextrapedidovendatipo() != null 
				&& filtro.getValorCampoExtra() != null && !filtro.getValorCampoExtra().trim().isEmpty()){
			query.leftOuterJoinIfNotExists("vendaorcamento.listaOrcamentovalorcampoextra listaOrcamentovalorcampoextra");
			query
				.where("listaOrcamentovalorcampoextra.campoextrapedidovendatipo = ?", filtro.getCampoextrapedidovendatipo())
				.whereLikeIgnoreAll("listaOrcamentovalorcampoextra.valor", filtro.getValorCampoExtra());
		}
		List<String> whereInNotSstatusComprovante = new ArrayList<String>();
		if(Boolean.TRUE.equals(filtro.getComprovanteEmitido())){
			query.where("exists(select voh.acao from Vendaorcamentohistorico voh where voh.vendaorcamento.cdvendaorcamento = vendaorcamento.cdvendaorcamento and voh.acao = ?)", Comprovantestatus.EMITIDO.toString());
		}else if(Boolean.FALSE.equals(filtro.getComprovanteEmitido())){
			whereInNotSstatusComprovante.add("'"+Comprovantestatus.EMITIDO.toString()+"'");
		}
		if(Boolean.TRUE.equals(filtro.getComprovanteEnviado())){
			query.where("exists(select voh.acao from Vendaorcamentohistorico voh where voh.vendaorcamento.cdvendaorcamento = vendaorcamento.cdvendaorcamento and voh.acao = ?)", Comprovantestatus.ENVIADO.toString());
		}else if(Boolean.FALSE.equals(filtro.getComprovanteEnviado())){
			whereInNotSstatusComprovante.add("'"+Comprovantestatus.ENVIADO.toString()+"'");
		}

		if(!whereInNotSstatusComprovante.isEmpty()){
			query.where("not exists(select voh.acao from Vendaorcamentohistorico voh where voh.vendaorcamento.cdvendaorcamento = vendaorcamento.cdvendaorcamento and voh.acao in ("+
					CollectionsUtil.concatenate(whereInNotSstatusComprovante, ",")+"))");
		}
	}
	
	/**
	* M�todo para buscar todas as venda de acordo com o filtro, para ser calculado o total geral
	*
	* @param filtro
	* @return
	* @since Oct 31, 2011
	* @author Luiz Fernando F Silva
	*/
	public VendaorcamentoFiltro findForCalculoTotalGeralVendas(VendaorcamentoFiltro filtro){
		
		StringBuilder sql = new StringBuilder();
		
		sql
		.append("select sum(totalvenda) as totalvenda, sum(totalqtde) as totalqtde,  sum(totalipi) + sum(totalicmsst) + sum(totalfcpst) + sum(totalIpiKits) - sum(totalDesoneracaoIcms) as totalipi  ")
		.append(" from (")
		
			.append("select sum(totalvenda) as totalvenda, totalqtde,  sum(totalipi) as totalipi, sum(totalicmsst) as totalicmsst, sum(totaldesoneracaoicms) as totaldesoneracaoicms, sum(totalfcpst) as totalfcpst, sum((coalesce(vmm.valorIpi,0) + coalesce(vmm.valoricmsst,0) + coalesce(vmm.valorfcpst,0) /100)) as totalIpiKits  ")
			.append("from(select v.cdvendaorcamento, sum(vm.quantidade) as totalqtde, sum((vm.preco * coalesce(vm.quantidade,1) * coalesce(vm.multiplicador,1)) - (coalesce(vm.desconto,0)/100) + (coalesce( vm.valorSeguro, 0)/100)+ (coalesce( vm.outrasdespesas, 0)/100) ) + (coalesce( v.valorfrete, 0)/100) - coalesce(v.desconto,0)/100 - coalesce(v.valorusadovalecompra,0)/100 as totalvenda, " +
					" sum(coalesce(vm.valoripi,0)/100) as totalipi, sum(coalesce(vm.valoricmsst,0)/100) as totalicmsst, sum(coalesce(vm.valordesoneracaoicms,0)/100) as totaldesoneracaoicms, sum(coalesce(vm.valorfcpst,0)/100) as totalfcpst ")
			.append("from vendaorcamento v ")
			.append("left outer join vendaorcamentomaterial vm on vm.cdvendaorcamento = v.cdvendaorcamento ")
			.append("left outer join material m on m.cdmaterial = vm.cdmaterial ");
		
		sql.append(this.criaQueryTotalGeral(filtro));	
		
		sql.append(" group by v.cdvendaorcamento, v.valorfrete, v.desconto) query ");
		sql.append(" left outer join vendaorcamentomaterialmestre vmm on vmm.cdvendaorcamento = query.cdvendaorcamento ")
			.append(" group by totalqtde) queryTotal");
		

		SinedUtil.markAsReader();
		VendaorcamentoFiltro vendaorcamentoFiltro = (VendaorcamentoFiltro)getJdbcTemplate().queryForObject(sql.toString(), 
		new RowMapper() {
			public VendaorcamentoFiltro mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new VendaorcamentoFiltro(new Money(rs.getDouble("totalvenda")), 
												new Money(rs.getDouble("totalipi")), 
												rs.getDouble("totalqtde"));
			}
		});
		
		if(vendaorcamentoFiltro != null)
			return vendaorcamentoFiltro;
		else
			return new VendaorcamentoFiltro();
	}
	
	/**
	 * M�todo que calcula o saldo total das vendas
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Money findForCalculoSaldoVendas(VendaorcamentoFiltro filtro){
		StringBuilder sql = new StringBuilder();
		
		sql
			.append("select sum(saldofinal) as saldoprodutos ")
			.append("from(select distinct v.cdvendaorcamento, v.saldofinal as saldofinal ")
			.append("from vendaorcamento v ")
			.append("left outer join vendaorcamentomaterial vm on vm.cdvendaorcamento = v.cdvendaorcamento ")
			.append("left outer join material m on m.cdmaterial = vm.cdmaterial ");
		
		sql.append(this.criaQueryTotalGeral(filtro));	
		
		sql.append(" group by v.cdvendaorcamento, v.saldofinal ) query ");
		

		SinedUtil.markAsReader();
		Money saldo = (Money)getJdbcTemplate().queryForObject(sql.toString(), 
		new RowMapper() {
			public Money mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Money(new Money(rs.getDouble("saldoprodutos"), true));
			}
		});
		
		if(saldo != null)
			return saldo;
		else
			return new Money();
	}
	
	/**
	 * M�todo que cria query para os totais na venda 
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	private String criaQueryTotalGeral(VendaorcamentoFiltro filtro) {
		StringBuilder query = new StringBuilder();
		Boolean clausulaWhere = Boolean.FALSE; 
		
		//join
		query.append("left outer join cliente c on c.cdpessoa = v.cdcliente ");
		query.append("left outer join clientevendedor clientev on clientev.cdcliente= c.cdpessoa ");
		query.append("left outer join colaborador colcliente on colcliente.cdpessoa= clientev.cdcolaborador ");
		query.append("left outer join colaborador co on co.cdpessoa = v.cdcolaborador ");
		
		if(filtro.getClienteindicacao() != null && filtro.getClienteindicacao().getCdpessoa() != null)
			query.append("left outer join cliente cindicacao on cindicacao.cdpessoa = v.cdclienteindicacao ");
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null)
			query.append("left outer join empresa e on e.cdpessoa = v.cdempresa ");
		if(filtro.getRegiao() != null)
			query.append("left outer join endereco ed on ed.cdendereco = v.cdenderecoentrega ");
		if(filtro.getProjeto() != null && filtro.getProjeto().getCdprojeto() != null)
			query.append("left outer join projeto p on p.cdprojeto = v.cdprojeto ");
		if (filtro.getCampoextrapedidovendatipo() != null 
				&& filtro.getCampoextrapedidovendatipo().getCdcampoextrapedidovendatipo() != null 
				&& filtro.getValorCampoExtra() != null && !filtro.getValorCampoExtra().trim().isEmpty()){
			query.append("left outer join orcamentovalorcampoextra oce on v.cdvendaorcamento = oce.cdvendaorcamento ");
					
		}

		//where
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy"); 
		if(filtro.getDtinicio() != null){
			query.append(" where v.dtorcamento >= to_date('" + format.format(filtro.getDtinicio()) + "', 'DD/MM/YYYY') ");
			clausulaWhere = Boolean.TRUE;
		}
		
		if(filtro.getDtfim() != null){
			if(clausulaWhere)
				query.append(" and v.dtorcamento <= to_date('" + format.format(filtro.getDtfim()) + "', 'DD/MM/YYYY') ");
			else{
				query.append(" where v.dtorcamento <= to_date('" + format.format(filtro.getDtfim()) + "', 'DD/MM/YYYY') ");
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(StringUtils.isNotBlank(filtro.getWhereIncdvendaorcamento())){
			if(clausulaWhere)
				query.append(" and v.cdvendaorcamento in (" + filtro.getWhereIncdvendaorcamento() + ")");
			else{
				query.append(" where v.cdvendaorcamento in (" + filtro.getWhereIncdvendaorcamento() +")");
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null){
			query.append(clausulaWhere ? " and " : " where ");
			query.append(" ( vm.cdmaterial = " + filtro.getMaterial().getCdmaterial() + " or " + 
					     " vm.cdmaterialmestre = " + filtro.getMaterial().getCdmaterial() + ") ");
			clausulaWhere = Boolean.TRUE;
		}
		
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null){
			if(clausulaWhere)
				query.append(" and e.cdpessoa = " + filtro.getEmpresa().getCdpessoa());
			else{
				query.append(" where e.cdpessoa = " + filtro.getEmpresa().getCdpessoa());
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(filtro.getTipopessoa() != null){
			query.append(clausulaWhere ? " and " : " where ");
			query.append(" v.tipopessoa = " + filtro.getTipopessoa().ordinal());
			clausulaWhere = Boolean.TRUE;
		}
		
		if(filtro.getCliente() != null && filtro.getCliente().getCdpessoa() != null){
			if(clausulaWhere)
				query.append(" and c.cdpessoa = " + filtro.getCliente().getCdpessoa());
			else{
				query.append(" where c.cdpessoa = " + filtro.getCliente().getCdpessoa());
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(filtro.getContacrm() != null && filtro.getContacrm().getCdcontacrm() != null){
			query.append(clausulaWhere ? " and " : " where ");
			query.append(" v.cdcontacrm = " + filtro.getContacrm().getCdcontacrm());
			clausulaWhere = Boolean.TRUE;
		}
		
		if(filtro.getClienteindicacao() != null && filtro.getClienteindicacao().getCdpessoa() != null){
			if(clausulaWhere)
				query.append(" and cindicacao.cdpessoa = " + filtro.getClienteindicacao().getCdpessoa());
			else{
				query.append(" where cindicacao.cdpessoa = " + filtro.getClienteindicacao().getCdpessoa());
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(filtro.getRegiao() != null){
			Regiao regiao = filtro.getRegiao();
			if(regiao.getListaRegiaolocal() != null && regiao.getListaRegiaolocal().size() > 0){
				if(clausulaWhere)
					query.append(" and ( ");
				else{
					query.append(" where ( ");
					clausulaWhere = Boolean.TRUE;
				}
				
				for (Regiaolocal regiaolocal : regiao.getListaRegiaolocal()) {
					query
						.append(" ( ")
						.append("ed.cep >= '").append(regiaolocal.getCepinicio()).append("' ")
						.append(" AND ed.cep <= '").append(regiaolocal.getCepfinal()).append("' ")
						.append(" ) OR ");
				}
				query.delete(query.length() - 3, query.length());
				query.append(" ) "); 
			}
		}
		
		if(filtro.getIsRestricaoVendaVendedor() != null && filtro.getIsRestricaoVendaVendedor()){ 
			Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
			if(colaborador != null) {
				if(filtro.getIsRestricaoClienteVendedor() == null || !filtro.getIsRestricaoVendaVendedor()){
					filtro.setColaborador(colaborador);
					if(filtro.getColaborador() != null && filtro.getColaborador().getCdpessoa() != null){
						query.append(clausulaWhere ? " and " : " where ");
						query.append(" (v.cdcontacrm is not null or co.cdpessoa = " + filtro.getColaborador().getCdpessoa() + ") ");
						clausulaWhere = Boolean.TRUE;
					}					
				}else {
					if(filtro.getColaborador() == null || !filtro.getColaborador().equals(colaborador)){
						query.append(clausulaWhere ? " and ( " : " where ( ");
						query.append(" v.cdcontacrm is not null or ( colcliente.cdpessoa = " + colaborador.getCdpessoa() + " ");
						if(filtro.getColaborador() != null){
							query.append(" and co.cdpessoa = " + filtro.getColaborador().getCdpessoa() + " ");
						}else {
							query.append(" or co.cdpessoa = " + colaborador.getCdpessoa() + " ");
						}
						query.append(" )) "); 
						clausulaWhere = Boolean.TRUE;
					}else {
						query.append(clausulaWhere ? " and " : " where ");
						query.append(" (v.cdcontacrm is not null or co.cdpessoa = " + colaborador.getCdpessoa() + " ) ");
						clausulaWhere = Boolean.TRUE;
					}
				}
			}
		}else {
			if(filtro.getColaborador() != null && filtro.getColaborador().getCdpessoa() != null){
				if(clausulaWhere)
					query.append(" and co.cdpessoa = " + filtro.getColaborador().getCdpessoa());
				else{
					query.append(" where co.cdpessoa = " + filtro.getColaborador().getCdpessoa());
					clausulaWhere = Boolean.TRUE;
				}
			}
		}
		
		if(filtro.getMaterialgrupo() != null && filtro.getMaterialgrupo().getCdmaterialgrupo() != null){
			if(clausulaWhere)
				query.append(" and m.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo());
			else{
				query.append(" where m.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo());
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(filtro.getMaterialtipo() != null && filtro.getMaterialtipo().getCdmaterialtipo() != null){
			if(clausulaWhere)
				query.append(" and m.cdmaterialtipo = " + filtro.getMaterialtipo().getCdmaterialtipo());
			else{
				query.append(" where m.cdmaterialtipo = " + filtro.getMaterialtipo().getCdmaterialtipo());
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(filtro.getLocalarmazenagem() != null && filtro.getLocalarmazenagem().getCdlocalarmazenagem() != null){
			if(clausulaWhere)
				query.append(" and v.cdlocalarmazenagem = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem());
			else{
				query.append(" where v.cdlocalarmazenagem = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem());
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		if(filtro.getProjeto() != null && filtro.getProjeto().getCdprojeto() != null){
			if(clausulaWhere)
				query.append(" and p.cdprojeto = " + filtro.getProjeto().getCdprojeto());
			else{
				query.append(" where p.cdprojeto = " + filtro.getProjeto().getCdprojeto());
				clausulaWhere = Boolean.TRUE;
			}
		}
		
		List<Vendaorcamentosituacao> listaVendaorcamentosituacao = filtro.getListaVendaorcamentosituacao();
		if(listaVendaorcamentosituacao != null){
			if(clausulaWhere)
				query.append(" and v.cdvendaorcamentosituacao in (" + CollectionsUtil.listAndConcatenate(listaVendaorcamentosituacao,"cdvendaorcamentosituacao", ",") + ") ");
			else{
				query.append(" where v.cdvendaorcamentosituacao in (" + CollectionsUtil.listAndConcatenate(listaVendaorcamentosituacao,"cdvendaorcamentosituacao", ",") + ") ");
				clausulaWhere = Boolean.TRUE;
			}
		}		
		
		if(filtro.getCampoextrapedidovendatipo() != null && filtro.getCampoextrapedidovendatipo().getCdcampoextrapedidovendatipo() != null && filtro.getValorCampoExtra() != null && !filtro.getValorCampoExtra().trim().isEmpty()){
			if (clausulaWhere) {
				query.append(" and oce.cdcampoextrapedidovendatipo = '" + filtro.getCampoextrapedidovendatipo().getCdcampoextrapedidovendatipo() + "'");
				query.append(" and (upper(retira_acento(oce.valor)) like ('%' ||'" + filtro.getValorCampoExtra().toUpperCase() + "'|| '%')) ");
			} else {
				query.append(" where oce.cdcampoextrapedidovendatipo = " + filtro.getCampoextrapedidovendatipo());
				query.append(" and (upper(retira_acento(oce.valor)) like ('%' ||'" + filtro.getValorCampoExtra().toUpperCase() + "'|| '%')) ");
			}
		}
		
		return query.toString();
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {		
		Vendaorcamento bean = (Vendaorcamento) save.getEntity();
		
		if (bean.getListavendaorcamentomaterial() != null && !bean.getListavendaorcamentomaterial().isEmpty())
			save.saveOrUpdateManaged("listavendaorcamentomaterial");
		
		save.saveOrUpdateManaged("listavendaorcamentomaterialmestre");
	
		//if(bean.getListavendaorcamentoformapagamento() != null && !bean.getListavendaorcamentoformapagamento().isEmpty())
			save.saveOrUpdateManaged("listavendaorcamentoformapagamento");
		
		if (bean.getListaOrcamentovalorcampoextra() != null && !bean.getListaOrcamentovalorcampoextra().isEmpty())
			save.saveOrUpdateManaged("listaOrcamentovalorcampoextra");
		
		if(Hibernate.isInitialized(bean.getListaVendaOrcamentoFornecedorTicketMedio())){
			save.saveOrUpdateManaged("listaVendaOrcamentoFornecedorTicketMedio");
		}

		super.updateSaveOrUpdate(save);			
	}
	
	/**
	 * M�todo para carregar uma venda.
	 * 
	 * @param id
	 * @return
	 * @author Ramon Brazil
	 */
	public Vendaorcamento loadVendaorcamento(Integer id){
		return query()
		.select("vendaorcamento.cdvendaorcamento, vendaorcamento.dtorcamento, vendaorcamento.valorfrete, contato.cdpessoa, contato.nome, contacrm.cdcontacrm, contacrm.nome, contacrm.cpf, contacrm.cnpj, " +
				"vendaorcamentomaterial.ordem, vendaorcamentomaterial.quantidade, vendaorcamentomaterial.multiplicador, vendaorcamentomaterial.preco, vendaorcamentomaterial.desconto, vendaorcamento.desconto, " +
				"vendaorcamento.identificadorexterno, vendaorcamento.identificador, vendaorcamento.dtaltera, vendaorcamento.valorusadovalecompra, " +
				"cliente.cdpessoa, cliente.nome, cliente.email, colaborador.cdpessoa, colaborador.nome, colaborador.email, " +
				"vendaorcamento.vendaorcamentosituacao, vendaorcamento.observacao, frete.cdResponsavelFrete, " +
				"frete.nome, terceiro.nome, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.inscricaoestadual, empresa.email, " +
				"empresa.site, logomarca.cdarquivo, projeto.cdprojeto, projeto.nome, pedidovendatipo.cdpedidovendatipo, pedidovendatipo.descricao, " +
				"endereco.logradouro, endereco.numero, endereco.bairro, endereco.cep, endereco.descricao, endereco.substituirlogradouro, " +
				"endereco.complemento, municipio.nome, municipio.uf," +
				"empresa.observacaovenda, loteestoque.numero, loteestoque.validade, vendaorcamento.dtvalidade, vendaorcamento.observacaointerna, " +
				"vendaorcamentomaterial.prazoentrega, " +
				"responsavel.cdpessoa, responsavel.nome, localarmazenagem.nome")
		.leftOuterJoin("vendaorcamento.cliente cliente")
		.leftOuterJoin("vendaorcamento.contacrm contacrm")
		.leftOuterJoin("vendaorcamento.contato contato")
		.leftOuterJoin("vendaorcamento.listavendaorcamentomaterial vendaorcamentomaterial")
		.leftOuterJoin("vendaorcamentomaterial.loteestoque loteestoque")
		.leftOuterJoin("vendaorcamento.colaborador colaborador")
		.leftOuterJoin("vendaorcamento.terceiro terceiro")
		.leftOuterJoin("vendaorcamento.frete frete")
		.leftOuterJoin("vendaorcamento.vendaorcamentosituacao vendaorcamentosituacao")
		.leftOuterJoin("vendaorcamento.empresa empresa")
		.leftOuterJoin("empresa.responsavel responsavel")
		.leftOuterJoin("empresa.logomarca logomarca")
		.leftOuterJoin("vendaorcamento.endereco endereco")
		.leftOuterJoin("endereco.municipio municipio")
		.leftOuterJoin("municipio.uf uf")
		.leftOuterJoin("vendaorcamento.projeto projeto")
		.leftOuterJoin("vendaorcamento.pedidovendatipo pedidovendatipo")
		.leftOuterJoin("vendaorcamento.localarmazenagem localarmazenagem")
		.where("vendaorcamento.cdvendaorcamento=?",id)
		.orderBy("vendaorcamentomaterial.ordem")
		.unique();
	}
	/**
	 * M�todo que carrega v�rias vendas
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Vendaorcamento> findVendaorcamentoReport(String whereIn){
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return querySined()
				
				.select("vendaorcamento.cdvendaorcamento, vendaorcamento.dtorcamento, vendaorcamento.valorfrete, vendaorcamentomaterial.quantidade, vendaorcamentomaterial.multiplicador, " +
						"vendaorcamentomaterial.preco, vendaorcamentomaterial.desconto, vendaorcamento.desconto, vendaorcamento.identificadorexterno, " +
						"vendaorcamento.identificador, vendaorcamento.dtaltera, vendaorcamento.valorusadovalecompra, contato.cdpessoa, contato.nome, " +
						"cliente.cdpessoa, contacrm.cdcontacrm, contacrm.nome, contacrm.cpf, contacrm.cnpj, colaborador.cdpessoa, colaborador.nome, colaborador.email, vendaorcamento.vendaorcamentosituacao, " +
						"vendaorcamento.observacao, projeto.cdprojeto, projeto.nome, " +
						"frete.cdResponsavelFrete, pedidovendatipo.cdpedidovendatipo, pedidovendatipo.descricao, " +
						"frete.nome, terceiro.nome, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.inscricaoestadual, empresa.email, " +
						"empresa.site, logomarca.cdarquivo, endereco.logradouro, endereco.numero, endereco.bairro, endereco.cep, " +
						"endereco.complemento, municipio.nome, municipio.uf, empresa.observacaovenda, vendaorcamento.dtvalidade, " +
						"loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, vendaorcamento.observacaointerna, localarmazenagem.nome ")
				.leftOuterJoin("vendaorcamento.cliente cliente")
				.leftOuterJoin("vendaorcamento.contacrm contacrm")
				.leftOuterJoin("vendaorcamento.contato contato")
				.leftOuterJoin("vendaorcamento.listavendaorcamentomaterial vendaorcamentomaterial")
				.leftOuterJoin("vendaorcamento.colaborador colaborador")
				.leftOuterJoin("vendaorcamento.terceiro terceiro")
				.leftOuterJoin("vendaorcamento.frete frete")
				.leftOuterJoin("vendaorcamento.vendaorcamentosituacao vendaorcamentosituacao")
				.leftOuterJoin("vendaorcamento.empresa empresa")
				.leftOuterJoin("empresa.logomarca logomarca")
				.leftOuterJoin("vendaorcamento.endereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("vendaorcamentomaterial.loteestoque loteestoque")
				.leftOuterJoin("vendaorcamento.projeto projeto")
				.leftOuterJoin("vendaorcamento.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("vendaorcamento.localarmazenagem localarmazenagem")
				.whereIn("vendaorcamento.cdvendaorcamento",whereIn)
				.list();
	}
	/**
	 * Atualiza os dados da venda alterados no crud
	 * @param venda
	 * @author Thiago Gona�ves
	 */
	public void updateVendaorcamento(Vendaorcamento vendaorcamento){
		String sql = "update vendaorcamento set cdenderecoentrega = " + (vendaorcamento.getEndereco() == null ? "null" : vendaorcamento.getEndereco().getCdendereco()) +
					 " , cdusuarioaltera ="+vendaorcamento.getCdusuarioaltera() +", dtaltera = ?, "+
					 "observacao = '" + vendaorcamento.getObservacao() + "', frete = ?, terceiro = ?, valorfrete = ? where cdvendaorcamento = " + vendaorcamento.getCdvendaorcamento();
		getJdbcTemplate().update(sql, new Object[]{vendaorcamento.getDtaltera(), vendaorcamento.getFrete(), vendaorcamento.getTerceiro(), vendaorcamento.getValorfrete()});
	}
	
	/**
	 * Atualiza a situa��o da vendaorcamento
	 * @param venda
	 * @param vendasituacao
	 * @author Thiago Gon�alves
	 */
	public void atualizaSituacao(Vendaorcamento vendaorcamento, Vendaorcamentosituacao vendaorcamentosituacao){
		String sql = "update vendaorcamento set cdvendaorcamentosituacao = " + vendaorcamentosituacao.getCdvendaorcamentosituacao() + 
		 			 " where cdvendaorcamento = " + vendaorcamento.getCdvendaorcamento();
		getJdbcTemplate().execute(sql);
	}
	
	/**
	 * Retorna a listagem de venda para o relat�rio de ordem de produ��o.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Vendaorcamento> findForOrdemProducao(String whereIn) {
		return query()
					.select("vendaorcamento.cdvendaorcamento, vendaorcamento.dtorcamento, colaborador.nome, cliente.nome, " +
							"cliente.cnpj, cliente.cpf")
					.leftOuterJoin("vendaorcamento.cliente cliente")
					.leftOuterJoin("vendaorcamento.colaborador colaborador")
					.whereIn("vendaorcamento.cdvendaorcamento", whereIn)
					.list();
	}

	/**
	 * M�todo que retorna a data da �ltima venda cadastrada no sistema.
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public java.sql.Date getDtUltimoCadastro() {
		Timestamp data = newQueryBuilderSined(Timestamp.class)
			.from(Vendaorcamento.class)
			.setUseTranslator(false) 
			.select("max(vendaorcamento.dtorcamento)")
			.join("vendaorcamento.vendaorcamentosituacao vendaorcamentosituacao")
			.where("vendaorcamentosituacao <> ?", Vendaorcamentosituacao.CANCELADA)
			.unique();
		return data != null ? new Date(data.getTime()) : null;
	}

	/**
	 * M�todo que retorna o total de vendas cadastradas no sistema
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getQtdeTotalVenda() {
		return newQueryBuilderSined(Long.class)
			.from(Vendaorcamento.class)
			.setUseTranslator(false) 
			.select("count(*)")
			.join("vendaorcamento.vendaorcamentosituacao vendaorcamentosituacao")
			.where("vendaorcamentosituacao <> ?", Vendaorcamentosituacao.CANCELADA)
			.unique()
			.intValue();
	}
	
	public List<Vendaorcamento> findByCliente(Cliente cliente, Empresa empresa) {
		String whereInEmpresas = empresa != null? empresa.getCdpessoa().toString():
												CollectionsUtil.listAndConcatenate(empresaService.findByUsuario(), "cdpessoa", ",");
		return querySined()
			
			.select("vendaorcamento.cdvendaorcamento, vendaorcamento.dtorcamento, material.nome, listavendaorcamentomaterial.preco, " +
					"listavendaorcamentomaterial.desconto, listavendaorcamentomaterial.quantidade, listavendaorcamentomaterial.multiplicador," +
					"pedidovendatipo.descricao, vendaorcamento.vendaorcamentosituacao")
			.join("vendaorcamento.listavendaorcamentomaterial listavendaorcamentomaterial")
			.join("listavendaorcamentomaterial.material material")
			.join("vendaorcamento.empresa empresa")
			.leftOuterJoin("vendaorcamento.pedidovendatipo pedidovendatipo")
			.where("vendaorcamento.cliente=?", cliente)
			.whereIn("empresa.cdpessoa", whereInEmpresas)
			.list();
				
	}

	/**
	 * Verifica se tem alguma venda nas situa��es passadas por par�metro.
	 *
	 * @param situacoes
	 * @return
	 * @param whereIn 
	 * @author Tom�s Rabelo
	 */
	public boolean haveVendaorcamentoSituacao(String whereIn, Boolean validateEndereco, Vendaorcamentosituacao[] situacoes) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < situacoes.length; i++) {
			sb.append(situacoes[i].getCdvendaorcamentosituacao());
			if((i + 1) < situacoes.length){
				sb.append(",");
			}
		}
		
		QueryBuilderSined<Long> query = newQueryBuilderSined(Long.class);
		query
			
			.select("count(*)")
			.from(Vendaorcamento.class)
			.join("vendaorcamento.vendaorcamentosituacao vendaorcamentosituacao")
			.leftOuterJoin("vendaorcamento.endereco endereco")
			.whereIn("vendaorcamento.cdvendaorcamento", whereIn);
		
		if(validateEndereco) {
			query.openParentheses()
				.whereIn("vendaorcamentosituacao.cdvendaorcamentosituacao", sb.toString()).or()
				.where("endereco is null")
			.closeParentheses();
		} else {
			query.whereIn("vendaorcamentosituacao.cdvendaorcamentosituacao", sb.toString());
		}
		return query.unique() > 0;
	}
	
	/**
	 * M�todo que verifica se existe or�amento que n�o pode ser cancelado (or�amento j� cancelado ou or�amento que tenha pedido de venda/venda n�o cancelado)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean existOrcamentoNotcancelar(String whereIn) {
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
			
			.select("count(*)")
			.from(Vendaorcamento.class)
			.join("vendaorcamento.vendaorcamentosituacao vendaorcamentosituacao")
			.openParentheses()
				.openParentheses()
					.where("vendaorcamentosituacao.cdvendaorcamentosituacao = ?", Vendaorcamentosituacao.CANCELADA.getCdvendaorcamentosituacao())
					.or()
					.where("vendaorcamentosituacao.cdvendaorcamentosituacao = ?", Vendaorcamentosituacao.REPROVADO.getCdvendaorcamentosituacao())
				.closeParentheses()
				.or()
				.openParentheses()
					.where("vendaorcamentosituacao.cdvendaorcamentosituacao = ?", Vendaorcamentosituacao.AUTORIZADO.getCdvendaorcamentosituacao())
					.openParentheses()
						.where("exists (select v.vendaorcamento.cdvendaorcamento from Venda v where v.vendaorcamento.cdvendaorcamento = vendaorcamento.cdvendaorcamento and v.vendasituacao.cdvendasituacao <> ? )", Vendasituacao.CANCELADA.getCdvendasituacao())
						.or()
						.where("exists (select pv.vendaorcamento.cdvendaorcamento from Pedidovenda pv where pv.vendaorcamento.cdvendaorcamento = vendaorcamento.cdvendaorcamento and pv.pedidovendasituacao <> ? )", Pedidovendasituacao.CANCELADO)
					.closeParentheses()
				.closeParentheses()
			.closeParentheses()
			.whereIn("vendaorcamento.cdvendaorcamento", whereIn);
			
		return query.unique() > 0;
	}
	
	public boolean existOrcamentoSomenteEmAberto(String whereIn) {
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
			
			.select("count(*)")
			.from(Vendaorcamento.class)
			.join("vendaorcamento.vendaorcamentosituacao vendaorcamentosituacao")
			.openParentheses()
				.where("vendaorcamentosituacao.cdvendaorcamentosituacao = ?", Vendaorcamentosituacao.CANCELADA.getCdvendaorcamentosituacao())
				.or()
				.where("vendaorcamentosituacao.cdvendaorcamentosituacao = ?", Vendaorcamentosituacao.AUTORIZADO.getCdvendaorcamentosituacao())
				.or()
				.where("vendaorcamentosituacao.cdvendaorcamentosituacao = ?", Vendaorcamentosituacao.REPROVADO.getCdvendaorcamentosituacao())
			.closeParentheses()
			.whereIn("vendaorcamento.cdvendaorcamento", whereIn);
			
		return query.unique() > 0;
	}

	/**
	 * M�todo que carrega as vendas para gerar nota fiscal produto
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Vendaorcamento> findForCobranca(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		
		return query()
			.select("vendaorcamento.cdvendaorcamento, vendaorcamento.orcamento, cliente.cdpessoa, endereco.cdendereco, listavendaorcamentomaterial.cdvendaorcamentomaterial, " +
					"listavendaorcamentomaterial.quantidade, vendaorcamentomaterial.multiplicador, listavendaorcamentomaterial.preco, listavendaorcamentomaterial.desconto, material.cdmaterial, " +
					"material.produto, material.servico, material.patrimonio, material.epi, unidademedida.cdunidademedida, " +
					"unidademedida.nome, unidademedida.simbolo, vendaorcamento.desconto, frete.cdResponsavelFrete, frete.nome, " +
					"vendaorcamento.valorfrete, terceiro.cdpessoa, vendaorcamentosituacao.cdvendaorcamentosituacao, " +
					"empresa.cdpessoa, materialgrupo.cdmaterialgrupo, ncmcapitulo.cdncmcapitulo, material.ncmcompleto")
			.join("vendaorcamento.cliente cliente")
			.join("vendaorcamento.vendaorcamentosituacao vendaorcamentosituacao")
			.leftOuterJoin("vendaorcamento.prazopagamento prazopagamento")
			.leftOuterJoin("vendaorcamento.empresa empresa")
			.leftOuterJoin("vendaorcamento.endereco endereco")
			.leftOuterJoin("vendaorcamento.frete frete")
			.join("vendaorcamento.listavendaorcamentomaterial listavendaorcamentomaterial")
			.join("listavendaorcamentomaterial.material material")
			.join("listavendaorcamentomaterial.unidademedida unidademedida")
			.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("vendaorcamento.terceiro terceiro")
			.whereIn("vendaorcamento.cdvendaorcamento", whereIn)
			.orderBy("upper(retira_acento(cliente.nome))")
			.list();
	}

	/**
	 * M�todo que muda a situa��o de v�rias vendas
	 * 
	 * @param whereIn
	 * @param situacao
	 * @author Tom�s Rabelo
	 */
	public void updateSituacaoVendaorcamento(String whereIn, Vendaorcamentosituacao situacao) {
		if(whereIn == null || whereIn.equals(""))
			throw new SinedException("Par�metros inv�lidos.");
		
		getHibernateTemplate().bulkUpdate("update Vendaorcamento v set v.vendaorcamentosituacao = ? where v.id in ("+whereIn+")", new Object[]{situacao});
		 
	}

	
	public void updateMotivoReprovacao(String ids, MotivoReprovacaoFaturamento motivo, String justificativareprovar) {
		if(ids == null || ids.equals(""))
			throw new SinedException("Par�metros inv�lidos.");
		
		getHibernateTemplate().bulkUpdate("update Vendaorcamento v set v.justificativareprovar = ? where v.id in ("+ids+")",justificativareprovar );
		getHibernateTemplate().bulkUpdate("update Vendaorcamento v set v.motivoreprovacaofaturamento = ? where v.id in ("+ids+")",motivo);
		 
	}
	
	/**
	 * M�todo que verifica se o or�amento possui materiais em produ��o
	 * 
	 * @param venda
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean isVendaOrcamentoProducao(Vendaorcamento vendaorcamento) {
		if(vendaorcamento == null || vendaorcamento.getCdvendaorcamento() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Vendaorcamento.class)
			.join("vendaorcamento.listavendaorcamentomaterial listavendaorcamentomaterial")
			.join("listavendaorcamentomaterial.material material")
			.where("vendaorcamento = ?", vendaorcamento)
			.where("material.producao = ?", Boolean.TRUE)
			.unique() > 0;
	}
	
	/**
	 * M�todo que verifica se o or�amento possui materiais em produ��o
	 * 
	 * @param venda
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean isVendaOrcamentoProducao(String whereIn) {
		if(whereIn == null || whereIn.equals(""))
			throw new SinedException("Par�metros inv�lidos.");
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Vendaorcamento.class)
			.join("vendaorcamento.listavendaorcamentomaterial listavendaorcamentomaterial")
			.join("listavendaorcamentomaterial.material material")
			.whereIn("vendaorcamento.id", whereIn)
			.where("material.producao = ?", Boolean.TRUE)
			.unique() > 0;
	}

	/**
	 * M�todo que busca os dados para o relat�rio de Ordem de retirada
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Vendaorcamento> findForOrdemRetirada(String whereIn) {
		return query()
			.select("vendaorcamento.cdvendaorcamento, vendaorcamento.dtorcamento, colaborador.nome, cliente.cdpessoa, cliente.nome, cliente.cnpj, cliente.cpf, " +
					"listavendaorcamentomaterial.cdvendaorcamentomaterial, listavendaorcamentomaterial.quantidade, vendaorcamentomaterial.multiplicador, listavendaorcamentomaterial.preco, listavendaorcamentomaterial.desconto, " +
					"unidademedida.cdunidademedida, unidademedida.simbolo, unidademedida.nome, material.cdmaterial, material.nome, material.referencia, " +
					"localizacaoestoque.cdlocalizacaoestoque, localizacaoestoque.descricao, materialgrupo.cdmaterialgrupo, materialgrupo.nome, " +
					"listavendaorcamentomaterial.altura, listavendaorcamentomaterial.largura," +
					"listavendaorcamentomaterial.comprimento, listavendaorcamentomaterial.comprimentooriginal, listavendaorcamentomaterial.observacao")
			.join("vendaorcamento.listavendaorcamentomaterial listavendaorcamentomaterial")
			.join("listavendaorcamentomaterial.material material")
			.join("material.materialgrupo materialgrupo")
			.leftOuterJoin("vendaorcamento.cliente cliente")
			.leftOuterJoin("vendaorcamento.colaborador colaborador")
			.leftOuterJoin("material.localizacaoestoque localizacaoestoque")
			.leftOuterJoin("listavendaorcamentomaterial.unidademedida unidademedida")
			.whereIn("vendaorcamento.cdvendaorcamento", whereIn)
			.list();
	}

	
	
	/**
	 * Metodo que busca a listagem que ser� apresentada no relat�rio
	 * 
	 * @return Venda
	 * @author Thiago Augusto
	 */
	public Vendaorcamento findVendaorcamento(Vendaorcamento _vendaorcamento){
		QueryBuilder<Vendaorcamento> qry = query();
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("vendaorcamento.cdvendaorcamento, vendaorcamento.dtorcamento, vendaorcamento.valorusadovalecompra, ");
		sb.append("cliente.cdpessoa, cliente.nome, cliente.tipopessoa, cliente.cpf, cliente.cnpj, ");
		sb.append("listavendaorcamentomaterial.cdvendaorcamentomaterial, listavendaorcamentomaterial.quantidade, vendaorcamentomaterial.multiplicador, listavendaorcamentomaterial.preco, listavendaorcamentomaterial.desconto,");
		sb.append("material.cdmaterial, material.nome, material.nome, material.codigofabricante,");
		sb.append("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, ");
		sb.append("endereco.cep, endereco.pontoreferencia, municipio.nome, uf.sigla, ");
		sb.append("grupoMaterial.cdmaterialgrupo, grupoMaterial.nome, ");
		sb.append("empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.cdpessoa");
		
		
		qry.select(sb.toString())
			 .leftOuterJoin("vendaorcamento.cliente cliente")
			 .leftOuterJoin("vendaorcamento.listavendaorcamentomaterial listavendaorcamentomaterial")
			 .leftOuterJoin("listavendaorcamentomaterial.material material")
			 .leftOuterJoin("vendaorcamento.empresa empresa")
			 .leftOuterJoin("vendaorcamento.endereco endereco")
			 .leftOuterJoin("endereco.municipio municipio")
			 .leftOuterJoin("municipio.uf uf")
			 .leftOuterJoin("material.materialgrupo grupoMaterial")
			 .where("vendaorcamento.cdvendaorcamento = ?", _vendaorcamento.getCdvendaorcamento());
		return qry.unique();
		
	}
	
	/**
	 * Metodo que retorna a listagem que ser� apresentado no relat�rio. 
	 * 
	 * @return Venda
	 * @author Thiago Augusto
	 */
	public List<Vendaorcamento> findForRelatorio(VendaorcamentoFiltro filtro) {
        QueryBuilder<Vendaorcamento> query = query();
        query.select("distinct vendaorcamento.cdvendaorcamento, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
				"cliente.nome, contacrm.nome, vendaorcamento.dtorcamento, vendaorcamento.valorfrete," +
				"endereco.cdendereco, endereco.cep, endereco.complemento, endereco.numero, endereco.logradouro," +
				"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.nome, uf.sigla, vendaorcamento.vendaorcamentosituacao, vendaorcamento.desconto, " +
				"vendaorcamentomaterial.cdvendaorcamentomaterial, vendaorcamentomaterial.quantidade, vendaorcamentomaterial.multiplicador, vendaorcamentomaterial.preco, " +
				"vendaorcamentomaterial.desconto, vendaorcamentomaterial.valoripi, material.cdmaterial, material.nome, vendaorcamentomaterial.observacao, " +
				"vendaorcamentomaterial.prazoentrega, vendaorcamento.observacao, vendaorcamentomaterial.valorSeguro, vendaorcamentomaterial.outrasdespesas")
				.leftOuterJoin("vendaorcamento.listavendaorcamentomaterial vendaorcamentomaterial")
				.leftOuterJoin("vendaorcamentomaterial.material material")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("material.materialtipo materialtipo")
				.leftOuterJoin("vendaorcamento.empresa empresa")
				.leftOuterJoin("vendaorcamento.vendaorcamentosituacao vendaorcamentosituacao")
				.leftOuterJoin("vendaorcamento.cliente cliente")
				.leftOuterJoin("vendaorcamento.contacrm contacrm")
				.leftOuterJoin("vendaorcamento.colaborador colaborador")
				.leftOuterJoin("vendaorcamento.endereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("vendaorcamento.localarmazenagem localarmazenagem")	
				.leftOuterJoin("vendaorcamento.projeto projeto")
				.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
				.where("localarmazenagem = ?", filtro.getLocalarmazenagem())
		        .where("vendaorcamento.tipopessoa = ?",filtro.getTipopessoa())
				.where("cliente = ?",filtro.getCliente())
				.where("contacrm = ?", filtro.getContacrm())
				.where("empresa = ?",filtro.getEmpresa())
				.where("vendaorcamento.dtorcamento >= ?", filtro.getDtinicio())
				.where("vendaorcamento.dtorcamento <= ?", filtro.getDtfim())
				.whereIn("vendaorcamento.cdvendaorcamento ", filtro.getWhereIncdvendaorcamento())
				.where("projeto = ?", filtro.getProjeto())
				.orderBy("vendaorcamento.dtorcamento desc, vendaorcamento.cdvendaorcamento desc")
				.ignoreJoin("listaClientevendedor");
		
		if(filtro.getNaoexibirresultado() != null && filtro.getNaoexibirresultado()){
			query.where("1=0");
		}
		
		if (filtro.getColaborador() != null && filtro.getTipoVendedor() != null && TipoVendedorEnum.PRINCIPAL.equals(filtro.getTipoVendedor())) {
			query
				.openParentheses()
					.where("listaClientevendedor.colaborador = ?", filtro.getColaborador())
					.where("listaClientevendedor.principal = ?", Boolean.TRUE)
				.closeParentheses();
		} else {
			query.where("colaborador = ?", filtro.getColaborador());
		}
		
		preencheWhereQueryVendaorcamento(query, filtro);
		
		if(filtro.getRegiao() != null){
			Regiao regiao = filtro.getRegiao();
			if(regiao.getListaRegiaolocal() != null && regiao.getListaRegiaolocal().size() > 0){
				query.openParentheses();
				for (Regiaolocal regiaolocal : regiao.getListaRegiaolocal()) {
					query
						.openParentheses()
						.where("endereco.cep >= ?", regiaolocal.getCepinicio())
						.where("endereco.cep <= ?", regiaolocal.getCepfinal())
						.closeParentheses()
						.or();
				}
				query.closeParentheses();
			}
		}
		
		if(filtro.getCampoextrapedidovendatipo() != null 
				&& filtro.getCampoextrapedidovendatipo().getCdcampoextrapedidovendatipo() != null 
				&& filtro.getValorCampoExtra() != null && !filtro.getValorCampoExtra().trim().isEmpty()){
			query.leftOuterJoinIfNotExists("vendaorcamento.listaOrcamentovalorcampoextra listaOrcamentovalorcampoextra");
			query
				.where("listaOrcamentovalorcampoextra.campoextrapedidovendatipo = ?", filtro.getCampoextrapedidovendatipo())
				.whereLikeIgnoreAll("listaOrcamentovalorcampoextra.valor", filtro.getValorCampoExtra());
		}
		
		List<Vendaorcamentosituacao> listaVendaorcamentosituacao = filtro.getListaVendaorcamentosituacao();
		if (listaVendaorcamentosituacao != null)
			query.whereIn("vendaorcamento.vendaorcamentosituacao.cdvendaorcamentosituacao", CollectionsUtil.listAndConcatenate(listaVendaorcamentosituacao,"cdvendaorcamentosituacao", ","));
				
		if (!StringUtils.isEmpty(filtro.getOrderBy())) {
			query.orderBy(filtro.getOrderBy()+" "+(filtro.isAsc()?"ASC":"DESC"));
		}
		
		return query.list();
	}
	/** M�todo que retorna ordenando da �ltima para a primeira data. 
	 * @author Thiago Augusto
	 * @param cliente
	 * @return
	 */
	public List<Vendaorcamento> findByClienteOrderData(Cliente cliente) {
		return query()
			.select("vendaorcamento.cdvendaorcamento, vendaorcamento.dtorcamento, vendaorcamentosituacao.cdvendaorcamentosituacao, material.nome, " +
					"listavendamaterial.preco, listavendamaterial.desconto, listavendamaterial.quantidade,  vendaorcamentomaterial.multiplicador")
			.join("vendaorcamento.listavendaorcamentomaterial listavendaorcamentomaterial")
			.join("listavendaorcamentomaterial.material material")
			.leftOuterJoin("vendaorcamento.vendaorcamentosituacao vendaorcamentosituacao")
			.where("vendaorcamento.cliente=?", cliente)
			.where("vendaorcamentosituacao <> ?", Vendaorcamentosituacao.CANCELADA)
			.orderBy("vendaorcamento.dtorcamento DESC")
			.list();
				
	}

	/**
	 * M�todo que busca a �ltima venda com a forma de pagamento
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public Vendaorcamento buscaUltimaVendaorcamentoByCliente(Cliente cliente) {
		if(cliente == null || cliente.getCdpessoa() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("vendaorcamento.cdvendaorcamento, cliente.cdpessoa")
				.join("venda.cliente cliente")
				.where("cliente = ?", cliente)
				.orderBy("vendaorcamento.cdvendaorcamento desc")
				.setMaxResults(1)				
				.unique();
	}

	/**
	 * M�todo que busca informa��es dos colaboradores/comissao para o comissionamento da venda
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Vendaorcamento findForComissiaoVenda(Vendaorcamento vendaorcamento) {
		if(vendaorcamento == null || vendaorcamento.getCdvendaorcamento() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("vendaorcamento.cdvendaorcamento, listaVendaorcamentomaterial.cdvendaorcamentomaterial, material.cdmaterial, cliente.cdpessoa, " +
						"colaborador.cdpessoa, colaborador.nome, listaVendaorcamentomaterial.preco, listaVendaorcamentomaterial.desconto, listaVendaorcamentomaterial.quantidade, vendaorcamentomaterial.multiplicador, " +
						"vendaorcamento.desconto ")
				.leftOuterJoin("vendaorcamento.listavendaorcamentomaterial listaVendaorcamentomaterial")
				.leftOuterJoin("vendaorcamento.colaborador colaborador")
				.leftOuterJoin("vendaorcamento.cliente cliente")
				.join("listaVendaorcamentomaterial.material material")								
				.where("vendaorcamento  = ?", vendaorcamento)				
				.unique();
	}
	
	/**
	 * M�todo que busca o vendedor do pedido de venda
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Colaborador findVendedorForVendaorcamento(Vendaorcamento vendaorcamento) {
		if(vendaorcamento == null || vendaorcamento.getCdvendaorcamento() == null)
			throw new SinedException("Par�metro inv�lido.");

		SinedUtil.markAsReader();
		Integer cdcolaborador = getJdbcTemplate()
					.queryForInt("select cdcolaborador from Vendaorcamento where cdvendaorcamento = " + vendaorcamento.getCdvendaorcamento() + " ;");
				
		return cdcolaborador != null ? new Colaborador(cdcolaborador) : null;
	}
	
	/**
	 * M�todo que verifica se a venda est� cancelada
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isVendaorcamentoCancelada(Vendaorcamento vendaorcamento) {
		if(vendaorcamento == null || vendaorcamento.getCdvendaorcamento() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Vendaorcamento.class)
			.join("vendaorcamento.vendaorcamentosituacao vendaorcamentosituacao")
			.where("vendaorcamento = ?", vendaorcamento)
			.where("vendaorcamentosituacao = ?", Vendaorcamentosituacao.CANCELADA)
			.unique() > 0;
	}
	
	/**
	 * 
	 * M�todo que busca os dados necess�rios para gerar uma OS � partir do cdvenda.
	 *
	 * @name findVendaByCdVenda
	 * @param cdvenda
	 * @return
	 * @return Venda
	 * @author Thiago Augusto
	 * @date 21/05/2012
	 *
	 */
	public Vendaorcamento findVendaorcamentoByCdVendaorcamento(Integer cdvendaorcamento){
		if (cdvendaorcamento == null){
			throw new SinedException("O cdvenda n�o pode ser nulo.");
		}
		
		return query()
					.select("vendaorcamento.cdvendaorcamento, cliente.cdpessoa, cliente.nome, cliente.cnpj, cliente.cpf, " +
							"listavendaorcamentomaterial.cdvendaorcamentomaterial, listavendaorcamentomaterial.quantidade, vendaorcamentomaterial.multiplicador, " +
							"listavendaorcamentomaterial.preco, listavendaorcamentomaterial.desconto, listavendaorcamentomaterial.prazoentrega, " +
							"material.cdmaterial, material.nome, material.nome, material.codigofabricante, " +
							"unidademedida.simbolo ")
						.leftOuterJoin("vendaorcamento.cliente cliente")
						.leftOuterJoin("vendaorcamento.listavendaorcamentomaterial listavendaorcamentomaterial")
						.leftOuterJoin("listavendaorcamentomaterial.material material")
						.leftOuterJoin("material.unidademedida unidademedida")
						.where("vendaorcamento.cdvendaorcamento = ? ", cdvendaorcamento)
						.unique();
	}

	/**
	 * M�todo que carrega informa��es da venda para relat�rio de devolu��o de material
	 *
	 * @param cdvenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Vendaorcamento findForComprovantedevolucao(String cdvendaorcamento) {
		if(cdvendaorcamento == null || "".equals(cdvendaorcamento))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
		.select("vendaorcamento.cdvendaorcamento, cliente.cdpessoa, cliente.nome ")
			.leftOuterJoin("vendaorcamento.cliente cliente")
			.where("vendaorcamento.cdvendaorcamento = ? ", Integer.parseInt(cdvendaorcamento))
			.unique();
	}

	/**
	 * Busca a listagem para a gera��o do CSV.
	 *
	 * @param filtro
	 * @return
	 * @since 25/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Vendaorcamento> findForCsv(VendaorcamentoFiltro filtro) {
		QueryBuilder<Vendaorcamento> query = query();
		this.updateListagemQuery(query, filtro);
		this.joinVendaorcamentomaterialUpdatelistagem(query);
		
		List<Vendaorcamento> listaVendaorcamento = query.list();
		String whereIn = CollectionsUtil.listAndConcatenate(listaVendaorcamento, "cdvendaorcamento", ",");
		
        QueryBuilder<Vendaorcamento> queryFinal = query()
			.select("vendaorcamento.cdvendaorcamento, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
					"cliente.nome, cliente.cnpj, cliente.cpf, contacrm.nome, localarmazenagem.nome, vendaorcamento.dtorcamento, clienteindicacao.nome, " +
					"vendaorcamentomaterial.quantidade, vendaorcamentomaterial.multiplicador, vendaorcamentomaterial.preco, vendaorcamentomaterial.desconto, vendaorcamentosituacao.cdvendaorcamentosituacao, vendaorcamentosituacao.descricao, colaborador.nome, " +
					"vendaorcamento.valorfrete, vendaorcamento.desconto, vendaorcamento.valorusadovalecompra, vendaorcamentomaterial.valoripi ")
			.leftOuterJoin("vendaorcamento.listavendaorcamentomaterial vendaorcamentomaterial")
			.leftOuterJoin("vendaorcamento.empresa empresa")
			.leftOuterJoin("vendaorcamento.localarmazenagem localarmazenagem")
			.leftOuterJoin("vendaorcamento.vendaorcamentosituacao vendaorcamentosituacao")
			.leftOuterJoin("vendaorcamento.cliente cliente")
			.leftOuterJoin("vendaorcamento.contacrm contacrm")
			.leftOuterJoin("vendaorcamento.colaborador colaborador")
			.leftOuterJoin("vendaorcamento.clienteindicacao clienteindicacao")
			.whereIn("vendaorcamento.cdvendaorcamento", whereIn);
		
		if(filtro.getNaoexibirresultado() != null && filtro.getNaoexibirresultado()){
			queryFinal.where("1=0");
		}
		
		return queryFinal.list();
	}
	
	/**
	 * 
	 * M�todo que busca o identificador da venda.
	 *
	 * @name buscarIdentificadorVenda
	 * @param venda
	 * @return
	 * @return Integer
	 * @author Thiago Augusto
	 * @date 13/07/2012
	 *
	 */
	public String buscarIdentificadorVendaorcamento(Vendaorcamento vendaorcamento){
		Vendaorcamento retorno = querySined()
		.select("vendaorcamento.identificador")
		.where("vendaorcamento = ?", vendaorcamento)
		.unique();
		return retorno != null ? retorno.getIdentificador() : null;
	}
	
	/**
	 * M�todo que carrega a venda com o cliente
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Vendaorcamento loadWithCliente(Vendaorcamento vendaorcamento) {
		if(vendaorcamento == null || vendaorcamento.getCdvendaorcamento() == null)
			throw new SinedException("Venda n�o pode ser nulo.");
		
		return query()
				.select("vendaorcamento.cdvendaorcamento, cliente.cdpessoa, cliente.nome, cliente.cnpj")
				.join("vendaorcamento.cliente cliente")
				.where("vendaorcamento  = ?", vendaorcamento)
				.unique();
	}
	
	/**
	 * M�todo que carrega o materialtabelapreco da venda
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public Vendaorcamento loadWithMaterialtabelapreco(Vendaorcamento vendaorcamento) {
		if(vendaorcamento == null || vendaorcamento.getCdvendaorcamento() == null)
			throw new SinedException("Vendaorcamento n�o pode ser nula.");
		
		return query()
			.select("vendaorcamento.cdvendaorcamento, materialtabelapreco.cdmaterialtabelapreco, materialtabelapreco.nome")
			.leftOuterJoin("vendaorcamento.materialtabelapreco materialtabelapreco")
			.where("vendaorcamento = ?", vendaorcamento)
			.unique();
	}
	
	/**
	 * M�todo que busca o email do cliente e dos contatos
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Vendaorcamento> findForEnviocomprovanteemail(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("vendaorcamento.cdvendaorcamento, cliente.cdpessoa, cliente.nome, cliente.email, contato.cdpessoa, contato.emailcontato, " +
						"contacrm.cdcontacrm, contacrm.nome, listcontacrmcontato.nome, listcontacrmcontatoemail.email, "+
						"empresa.cdpessoa, empresa.razaosocial, empresa.nomefantasia, colaborador.cdpessoa, colaborador.nome, colaborador.email")
				.leftOuterJoin("vendaorcamento.cliente cliente")
				.leftOuterJoin("vendaorcamento.contacrm contacrm")
				.leftOuterJoin("contacrm.listcontacrmcontato listcontacrmcontato")
				.leftOuterJoin("listcontacrmcontato.listcontacrmcontatoemail listcontacrmcontatoemail")
				.join("vendaorcamento.empresa empresa")
				.join("vendaorcamento.colaborador colaborador")
				.leftOuterJoin("cliente.listaContato listaContato")
				.leftOuterJoin("listaContato.contato contato")
				.whereIn("vendaorcamento.cdvendaorcamento", whereIn)
				.list();
	}

	/**
	 * M�todo que volta a situa��o do or�amento para em aberto ap�s o cancelamento do pedido de venda, venda ou contrato, e que n�o exista nenhum pedido de venda, venda ou contrato Em Aberto
	 *
	 * @param whereInPedidovenda
	 * @author Luiz Fernando
	 */
	public void updateVendaorcamentoByCancelamentoOrigem(String whereInPedidovenda, String whereInVenda, String whereInContrato) {
		if(StringUtils.isBlank(whereInPedidovenda) && StringUtils.isBlank(whereInVenda) && StringUtils.isBlank(whereInContrato)){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		System.out.println("update Vendaorcamento set cdvendaorcamentosituacao = ? "
				+ "where cdvendaorcamento in (select vo.cdvendaorcamento "
				+ "from vendaorcamento vo  "
				+ (StringUtils.isNotBlank(whereInPedidovenda) ? "join pedidovenda pv on pv.cdvendaorcamento = vo.cdvendaorcamento " : "")
				+ (StringUtils.isNotBlank(whereInVenda) ? "join venda v on v.cdvendaorcamento = vo.cdvendaorcamento " : "")
				+ (StringUtils.isNotBlank(whereInContrato) ? "join contrato c on c.cdvendaorcamento = vo.cdvendaorcamento " : "")
				+ "where vo.cdvendaorcamentosituacao =  " + Vendaorcamentosituacao.AUTORIZADO.getCdvendaorcamentosituacao()
				+ (StringUtils.isNotBlank(whereInPedidovenda) ? "and pv.cdpedidovenda in ("+ whereInPedidovenda + ") " : "")
				+ (StringUtils.isNotBlank(whereInVenda) ? "and v.cdvenda in ("+ whereInVenda + ") " : "")
				+ (StringUtils.isNotBlank(whereInContrato) ? "and c.cdcontrato in ("+ whereInContrato + ") " : "")
				+ "and not exists (select pv2.cdvendaorcamento from pedidovenda pv2 where pv2.cdvendaorcamento = vo.cdvendaorcamento and pv2.pedidovendasituacao <> " + Pedidovendasituacao.CANCELADO.getValue() + ") "
				+ "and not exists (select v2.cdvendaorcamento from venda v2 where v2.cdvendaorcamento = vo.cdvendaorcamento and v2.cdvendasituacao <> " + Vendasituacao.CANCELADA.getCdvendasituacao() + ") "
				+ "and not exists (select c2.cdvendaorcamento from aux_contrato aux_c2 join contrato c2 on c2.cdcontrato = aux_c2.cdcontrato where c2.cdvendaorcamento = vo.cdvendaorcamento and aux_c2.situacao <> " + SituacaoContrato.CANCELADO.getValue() + ") "
				+ ") ");
			
		getJdbcTemplate().update("update Vendaorcamento set cdvendaorcamentosituacao = ? "
				+ "where cdvendaorcamento in (select vo.cdvendaorcamento "
				+ "from vendaorcamento vo  "
				+ (StringUtils.isNotBlank(whereInPedidovenda) ? "join pedidovenda pv on pv.cdvendaorcamento = vo.cdvendaorcamento " : "")
				+ (StringUtils.isNotBlank(whereInVenda) ? "join venda v on v.cdvendaorcamento = vo.cdvendaorcamento " : "")
				+ (StringUtils.isNotBlank(whereInContrato) ? "join contrato c on c.cdvendaorcamento = vo.cdvendaorcamento " : "")
				+ "where vo.cdvendaorcamentosituacao =  " + Vendaorcamentosituacao.AUTORIZADO.getCdvendaorcamentosituacao()
				+ (StringUtils.isNotBlank(whereInPedidovenda) ? "and pv.cdpedidovenda in ("+ whereInPedidovenda + ") " : "")
				+ (StringUtils.isNotBlank(whereInVenda) ? "and v.cdvenda in ("+ whereInVenda + ") " : "")
				+ (StringUtils.isNotBlank(whereInContrato) ? "and c.cdcontrato in ("+ whereInContrato + ") " : "")
				+ "and not exists (select pv2.cdvendaorcamento from pedidovenda pv2 where pv2.cdvendaorcamento = vo.cdvendaorcamento and pv2.pedidovendasituacao <> " + Pedidovendasituacao.CANCELADO.getValue() + ") "
				+ "and not exists (select v2.cdvendaorcamento from venda v2 where v2.cdvendaorcamento = vo.cdvendaorcamento and v2.cdvendasituacao <> " + Vendasituacao.CANCELADA.getCdvendasituacao() + ") "
				+ "and not exists (select c2.cdvendaorcamento from aux_contrato aux_c2 join contrato c2 on c2.cdcontrato = aux_c2.cdcontrato where c2.cdvendaorcamento = vo.cdvendaorcamento and aux_c2.situacao <> " + SituacaoContrato.CANCELADO.getValue() + ") "
				+ ") ",
			new Object[]{Vendaorcamentosituacao.EM_ABERTO.getCdvendaorcamentosituacao()});
	}
	
	/**
	 * M�todo que busca os dados do or�amento para criar a solicita��o de compra
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 13/09/2013
	 */
	public List<Vendaorcamento> findForSolicitacaocompra(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Vendaorcamento> qry = query();
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("vendaorcamento.cdvendaorcamento, vendaorcamento.dtorcamento, vendaorcamento.observacao, vendaorcamento.observacaointerna, vendaorcamento.vendaorcamentosituacao,  ");
		sb.append("cliente.cdpessoa, cliente.nome, cliente.tipopessoa, cliente.cpf, cliente.cnpj, vendaorcamento.saldofinal, ");
		sb.append("listavendaorcamentomaterial.cdvendaorcamentomaterial, listavendaorcamentomaterial.quantidade, listavendaorcamentomaterial.preco, listavendaorcamentomaterial.desconto, listavendaorcamentomaterial.unidademedida, listavendaorcamentomaterial.prazoentrega, ");
		sb.append("material.cdmaterial, material.nome, material.nome, material.codigofabricante, material.qtdeunidade, material.considerarvendamultiplos, ");
		sb.append("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, ");
		sb.append("endereco.cep, endereco.pontoreferencia, municipio.nome, uf.sigla, listavendaorcamentomaterial.valorcustomaterial, material.valorcusto, material.valorvenda, listavendaorcamentomaterial.valorvendamaterial, ");
		sb.append("materialgrupo.cdmaterialgrupo, materialgrupo.nome, material.produto, material.patrimonio, material.epi, material.servico, ");
		sb.append("material.valorvendaminimo, material.valorvendamaximo, listavendaorcamentomaterial.fatorconversao, listavendaorcamentomaterial.qtdereferencia, ");
		sb.append("empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.cdpessoa, vendaorcamento.valorusadovalecompra, ");
		sb.append("colaborador.cdpessoa, colaborador.nome, loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, ");
		sb.append("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, listavendaorcamentomaterial.observacao, ");
		sb.append("projeto.cdprojeto, ");
		sb.append("contagerencial.cdcontagerencial, contagerencial.nome, unidademedida.cdunidademedida, unidademedida.simbolo, vcontagerencial.identificador, ");
		sb.append("vendaorcamento.frete, terceiro.cdpessoa, terceiro.nome, vendaorcamento.valorfrete, vendaorcamento.desconto, ");
		sb.append("listavendaorcamentomaterial.altura, listavendaorcamentomaterial.largura, listavendaorcamentomaterial.comprimento, listavendaorcamentomaterial.comprimentooriginal, listavendaorcamentomaterial.multiplicador, ");
		sb.append("listavendaorcamentomaterial.observacao ");
		
		qry.select(sb.toString())
			 .leftOuterJoin("vendaorcamento.cliente cliente")
			 .leftOuterJoin("vendaorcamento.projeto projeto")
			 .leftOuterJoin("vendaorcamento.listavendaorcamentomaterial listavendaorcamentomaterial")
			 .leftOuterJoin("listavendaorcamentomaterial.material material")
			 .leftOuterJoin("listavendaorcamentomaterial.loteestoque loteestoque")
			 .leftOuterJoin("vendaorcamento.empresa empresa")
			 .leftOuterJoin("vendaorcamento.endereco endereco")
			 .leftOuterJoin("vendaorcamento.colaborador colaborador")
			 .leftOuterJoin("endereco.municipio municipio")
			 .leftOuterJoin("municipio.uf uf")
			 .leftOuterJoin("material.materialgrupo materialgrupo")
			 .leftOuterJoin("material.contagerencial contagerencial")
			 .leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			 .leftOuterJoin("listavendaorcamentomaterial.unidademedida unidademedida")
			 .leftOuterJoin("vendaorcamento.localarmazenagem localarmazenagem")
			 .leftOuterJoin("vendaorcamento.terceiro terceiro")
			 .leftOuterJoin("vendaorcamento.frete frete")
			 .whereIn("vendaorcamento.cdvendaorcamento", whereIn)
			 .orderBy("vendaorcamento.cdvendaorcamento, listavendaorcamentomaterial.ordem");
		
		return qry.list();
	}

	public boolean haveVendaorcamentoDiferenteSituacao(String whereIn, Vendaorcamentosituacao[] situacoes) {
		if(situacoes == null || situacoes.length == 0)
			throw new SinedException("Par�metro inv�lido.");
		
		List<Vendaorcamentosituacao> lista = new ArrayList<Vendaorcamentosituacao>();
		for (int i = 0; i < situacoes.length; i++) {
			lista.add(situacoes[i]);
		}
		
		QueryBuilder<Long> query = newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Vendaorcamento.class)
					.join("vendaorcamento.vendaorcamentosituacao vendaorcamentosituacao")
					.whereIn("vendaorcamento.cdvendaorcamento", whereIn);
		
		query.whereIn("vendaorcamentosituacao.cdvendaorcamentosituacao not", CollectionsUtil.listAndConcatenate(lista,"cdvendaorcamentosituacao", ","));
		
		return query.unique() > 0;
	}
	
	/**
	 * M�todo que carrega o or�amento para troca de vendedor
	 *
	 * @param vendaorcamento
	 * @return
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public Vendaorcamento loadForTrocarvendedor(Vendaorcamento vendaorcamento) {
		if(vendaorcamento == null || vendaorcamento.getCdvendaorcamento() == null)
			throw new SinedException("enda n�o pode ser nulo");
		
		return query()
				.select("vendaorcamento.cdvendaorcamento, colaborador.cdpessoa, colaborador.nome, cliente.cdpessoa, cliente.nome, " +
						"colaboradorcliente.cdpessoa, colaboradorcliente.nome ")
				.leftOuterJoin("vendaorcamento.colaborador colaborador")
				.leftOuterJoin("vendaorcamento.cliente cliente")
				.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
				.leftOuterJoin("listaClientevendedor.colaborador colaboradorcliente")
				.where("vendaorcamento = ?", vendaorcamento)
				.unique();
	}

	/**
	 * M�todo que atualiza o respons�vel do or�amento
	 *
	 * @param vendaorcamento
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public void updateColaboradorByVendaorcamento(Vendaorcamento vendaorcamento) {
		if(vendaorcamento != null && vendaorcamento.getCdvendaorcamento() != null && vendaorcamento.getColaborador() != null && 
				vendaorcamento.getColaborador().getCdpessoa() != null){
			getHibernateTemplate().bulkUpdate("update Vendaorcamento v set v.colaborador = ? where v.id in (" + vendaorcamento.getCdvendaorcamento() + ")", 
					new Object[]{vendaorcamento.getColaborador()});
		}		
	}

	public List<Vendaorcamento> findForOrdemcompra(String whereInCdvendaorcamento) {
		if(whereInCdvendaorcamento == null || "".equals(whereInCdvendaorcamento))
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Vendaorcamento> qry = query();
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("vendaorcamento.cdvendaorcamento, vendaorcamento.dtorcamento, vendaorcamento.observacao, vendaorcamento.observacaointerna, vendaorcamento.vendaorcamentosituacao,  ");
		sb.append("cliente.cdpessoa, cliente.nome, cliente.tipopessoa, cliente.cpf, cliente.cnpj, vendaorcamento.saldofinal, ");
		sb.append("listavendaorcamentomaterial.cdvendaorcamentomaterial, listavendaorcamentomaterial.quantidade, listavendaorcamentomaterial.preco, listavendaorcamentomaterial.desconto, listavendaorcamentomaterial.unidademedida, listavendaorcamentomaterial.prazoentrega, ");
		sb.append("material.cdmaterial, material.nome, material.nome, material.codigofabricante, material.qtdeunidade, material.considerarvendamultiplos, ");
		sb.append("endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.complemento, endereco.bairro, ");
		sb.append("endereco.cep, endereco.pontoreferencia, municipio.nome, uf.sigla, listavendaorcamentomaterial.valorcustomaterial, material.valorcusto, material.valorvenda, listavendaorcamentomaterial.valorvendamaterial, ");
		sb.append("materialgrupo.cdmaterialgrupo, materialgrupo.nome, material.produto, material.patrimonio, material.epi, material.servico, ");
		sb.append("material.valorvendaminimo, material.valorvendamaximo, listavendaorcamentomaterial.fatorconversao, listavendaorcamentomaterial.qtdereferencia, ");
		sb.append("empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cnpj, empresa.cdpessoa, vendaorcamento.valorusadovalecompra, ");
		sb.append("colaborador.cdpessoa, colaborador.nome, loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, ");
		sb.append("localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, listavendaorcamentomaterial.observacao, ");
		sb.append("projeto.cdprojeto, ");
		sb.append("contagerencial.cdcontagerencial, contagerencial.nome, unidademedida.cdunidademedida, unidademedida.simbolo, vcontagerencial.identificador, ");
		sb.append("vendaorcamento.frete, terceiro.cdpessoa, terceiro.nome, vendaorcamento.valorfrete, vendaorcamento.desconto, ");
		sb.append("listavendaorcamentomaterial.altura, listavendaorcamentomaterial.largura, listavendaorcamentomaterial.comprimento, listavendaorcamentomaterial.comprimentooriginal, listavendaorcamentomaterial.multiplicador ");
		
		qry.select(sb.toString())
			 .leftOuterJoin("vendaorcamento.cliente cliente")
			 .leftOuterJoin("vendaorcamento.projeto projeto")
			 .leftOuterJoin("vendaorcamento.listavendaorcamentomaterial listavendaorcamentomaterial")
			 .leftOuterJoin("listavendaorcamentomaterial.material material")
			 .leftOuterJoin("listavendaorcamentomaterial.loteestoque loteestoque")
			 .leftOuterJoin("vendaorcamento.empresa empresa")
			 .leftOuterJoin("vendaorcamento.endereco endereco")
			 .leftOuterJoin("vendaorcamento.colaborador colaborador")
			 .leftOuterJoin("endereco.municipio municipio")
			 .leftOuterJoin("municipio.uf uf")
			 .leftOuterJoin("material.materialgrupo materialgrupo")
			 .leftOuterJoin("material.contagerencial contagerencial")
			 .leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			 .leftOuterJoin("listavendaorcamentomaterial.unidademedida unidademedida")
			 .leftOuterJoin("vendaorcamento.localarmazenagem localarmazenagem")
			 .leftOuterJoin("vendaorcamento.terceiro terceiro")
			 .leftOuterJoin("vendaorcamento.frete frete")
			 .whereIn("vendaorcamento.cdvendaorcamento", whereInCdvendaorcamento)
			 .orderBy("vendaorcamento.cdvendaorcamento, listavendaorcamentomaterial.ordem");
		
		return qry.list();

	}
	

	
	/**
	* M�todo que verifica se o cliente possui restri��o sem data de libera��o
	*
	* @param cdvendaorcamento
	* @return
	* @author Lucas Costa
	*/
	public Boolean hasClienteComRestricoesEmAberto(Integer cdvendaorcamento) {
		if(cdvendaorcamento == null || cdvendaorcamento.equals("")){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		
		return (query()
			.select("vendaorcamento.cdvendaorcamento, vendaorcamento.cliente")
			.where("vendaorcamento.cdvendaorcamento in (select v.cdvendaorcamento from Vendaorcamento v " +
								  "join v.cliente c " +
								  "join c.listaRestricao lr " +
								  "where lr.dtliberacao is null and v.cdvendaorcamento = "+cdvendaorcamento+")")
			.unique() != null);
	}
	
	public boolean haveOrcamentoByIdentificadorEmpresa(String identificador, Empresa empresa, boolean verificarCancelado) {
		return newQueryBuilderSinedWithFrom(Long.class)
				.setUseReadOnly(false)
				.select("count(*)")
				.where("vendaorcamento.identificador = ?", identificador)
				.where("vendaorcamento.empresa = ?", empresa)
				.where("vendaorcamento.vendaorcamentosituacao <> ?", Vendaorcamentosituacao.CANCELADA, !verificarCancelado)
				.unique() > 0;
	}
	
	/**
	* M�todo que verifica se o identificador j� est� cadastrado
	*
	* @param identificador
	* @return true or false
	* @author Jo�o Vitor
	* @since 21/10/2014
	*/
	public boolean isIdentificadorCadastrado(Vendaorcamento vendaorcamento) {
		if(vendaorcamento == null){
			return false;
		}
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
		String dataVendaOrcamentoAtual = null;
		
		if (vendaorcamento.getDtorcamento() != null) {
			dataVendaOrcamentoAtual = simpleDateFormat.format(vendaorcamento.getDtorcamento());
		}
		
		return query()
		.select("vendaorcamento.cdvendaorcamento, vendaorcamento.identificador, vendaorcamento.dtorcamento")
		.leftOuterJoin("vendaorcamento.empresa empresa")
		.where("vendaorcamento.identificador = ?", vendaorcamento.getIdentificador())
		.where("vendaorcamento.identificador <> ''")
		.where("vendaorcamento.identificador IS NOT NULL")
		.where("vendaorcamento.cdvendaorcamento <> ?", vendaorcamento.getCdvendaorcamento())
		.where("EXTRACT(YEAR FROM date(vendaorcamento.dtorcamento)) = ?", Integer.parseInt(dataVendaOrcamentoAtual))
		.where("empresa = ?", vendaorcamento.getEmpresa())
		.unique() != null;
	}
	
	/**
	* M�todo que busca o �ltimo or�amento referente a oportunidade
	*
	* @param oportunidade
	* @return
	* @since 10/05/2017
	* @author Luiz Fernando
	*/
	public Vendaorcamento findUltimoOrcamentoByOportunidade(Oportunidade oportunidade, Vendaorcamentosituacao vendaorcamentosituacao) {
		if(oportunidade == null || oportunidade.getCdoportunidade() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("vendaorcamento.cdvendaorcamento, vendaorcamento.dtorcamento")
				.where("vendaorcamento.oportunidade = ?", oportunidade)
				.where("vendaorcamento.vendaorcamentosituacao = ?", vendaorcamentosituacao)
				.setMaxResults(1)
				.orderBy("vendaorcamento.cdvendaorcamento desc")
				.unique();
	}
	
	public Vendaorcamento findForComprovanteRTF(Vendaorcamento vendaorcamento){
		return query()
				.select("vendaorcamento.cdvendaorcamento, vendaorcamento.identificador, " +
						"material.cdmaterial, material.identificacao, material.nome, material.ncmcompleto, material.image, " +
						"unidademedida.nome, unidademedida.simbolo, " +
						"vendaorcamentomaterial.quantidade, vendaorcamentomaterial.preco, vendaorcamentomaterial.desconto," +
						"vendaorcamentomaterial.valorvendamaterial, vendaorcamentomaterial.observacao, vendaorcamentomaterial.altura, " +
						"vendaorcamentomaterial.largura, vendaorcamentomaterial.comprimento, vendaorcamentomaterial.comprimentooriginal, " +
						"vendaorcamentomaterial.prazoentrega, " +
						"ncmcapitulo.descricao, ncmcapitulo.cdncmcapitulo, " +
						"loteestoque.numero, loteestoque.validade, " +
						"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.descricao")
				.join("vendaorcamento.cliente cliente")
				.leftOuterJoin("vendaorcamento.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("vendaorcamento.empresa empresa")
				.leftOuterJoin("vendaorcamento.colaborador colaborador")
				.leftOuterJoin("vendaorcamento.endereco endereco")
				.leftOuterJoin("vendaorcamento.listavendaorcamentomaterial vendaorcamentomaterial")
				.leftOuterJoin("vendaorcamentomaterial.material material")
				.leftOuterJoin("vendaorcamentomaterial.unidademedida unidademedida")
				.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
				.leftOuterJoin("vendaorcamentomaterial.loteestoque loteestoque")
				.unique();
	}
	public List<Vendaorcamento> identificadorExternoRepetido(
			Integer cdvendaorcamento, String externo) {
		return query()
				.select("vendaorcamento.cdvendaorcamento, vendaorcamento.identificador")
				.where("vendaorcamento.cdvendaorcamento <> ?",cdvendaorcamento)
				.where("vendaorcamento.identificadorexterno  like ?", externo)
				.list();
	}
	
	public Vendaorcamento findCdMaterialById(Vendaorcamento vendaorcamento) {
		if(vendaorcamento == null) {
			throw new SinedException("Venda Or�amento n�o pode ser nula.");
		}
		
		return query()
				.select("material.cdmaterial")
				.leftOuterJoin("vendaorcamento.listavendaorcamentomaterial listavendaorcamentomaterial")
				.leftOuterJoin("listavendaorcamentomaterial.material material")
				.where("vendaorcamento = ?", vendaorcamento)
				.unique();
	}
	
	/**
	* M�todo que busca os dados para popular o relat�rio custo x venda de or�amento
	*
	* @param
	* @return
	* @since 04/02/2021
	* @author Michael Rodrigues
	*/
	public List<Vendaorcamento> findForEmitircustoorcamentovenda(
			EmitircustovendaFiltro filtro) {
		if (filtro == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		QueryBuilderSined<Vendaorcamento> query = querySined();
		query
		.select("vendaorcamento.cdvendaorcamento, vendaorcamento.dtorcamento, vendaorcamento.valorusadovalecompra, colaborador.nome, cliente.cdpessoa, cliente.nome, cliente.cnpj, cliente.cpf, cliente.identificador, " +
				"contacrm.cdcontacrm, contacrm.nome, listavendaorcamentomaterial.cdvendaorcamentomaterial, listavendaorcamentomaterial.quantidade, listavendaorcamentomaterial.preco, " +
				"unidademedida.cdunidademedida, unidademedida.simbolo, unidademedida.nome, unidademedida.casasdecimaisestoque, material.cdmaterial, material.identificacao, material.nome, material.referencia, " +
				"materialgrupo.cdmaterialgrupo, materialgrupo.nome, material.valorcusto, material.valorvenda, material.metrocubicovalorvenda, material.pesoliquidovalorvenda, " +
				"listavendaorcamentomaterial.altura, listavendaorcamentomaterial.largura, listavendaorcamentomaterial.desconto, material.produto, listavendaorcamentomaterial.saldo, " +
				"listavendaorcamentomaterial.comprimento, listavendaorcamentomaterial.comprimentooriginal, listavendaorcamentomaterial.observacao, listavendaorcamentomaterial.multiplicador, " +
				"unidademedidavenda.cdunidademedida, unidademedidavenda.nome, unidademedidavenda.casasdecimaisestoque, listavendaorcamentomaterial.fatorconversao, listavendaorcamentomaterial.qtdereferencia, " +
				"listavendaorcamentomaterial.valorcustomaterial, listavendaorcamentomaterial.valorvendamaterial, vendaorcamento.desconto, listavendaorcamentomaterial.desconto, pedidovendatipo.cdpedidovendatipo," +
				"pedidovendatipo.bonificacao, pedidovendatipo.descricao ")
		.join("vendaorcamento.empresa empresa")
		.join("vendaorcamento.listavendaorcamentomaterial listavendaorcamentomaterial")
		.join("listavendaorcamentomaterial.material material")
		.join("material.unidademedida unidademedida")
		.join("vendaorcamento.vendaorcamentosituacao vendaorcamentosituacao")
		.leftOuterJoin("material.materialgrupo materialgrupo")
		.leftOuterJoin("vendaorcamento.cliente cliente")
		.leftOuterJoin("vendaorcamento.contacrm contacrm")
		.leftOuterJoin("vendaorcamento.colaborador colaborador")
		.leftOuterJoin("listavendaorcamentomaterial.unidademedida unidademedidavenda")
		.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
		.leftOuterJoin("listaClientevendedor.colaborador colaboradorcliente")
		.leftOuterJoin("vendaorcamento.pedidovendatipo pedidovendatipo");

		if(filtro.getWhereInVendaorcamento() != null && !"".equals(filtro.getWhereInVendaorcamento())) {
			query
				.whereIn("vendaorcamento.cdvendaorcamento", filtro.getWhereInVendaorcamento())
				.where("vendaorcamentosituacao.cdvendaorcamentosituacao <> ?", Vendaorcamentosituacao.CANCELADA.getCdvendaorcamentosituacao());
		} else {					
			query
				.where("empresa = ?", filtro.getEmpresa())
				.where("vendaorcamento.dtorcamento >= ?", filtro.getDtinicio())
				.where("vendaorcamento.dtorcamento <= ?", filtro.getDtfim())
				.where("cliente = ?", filtro.getCliente())
				.where("contacrm = ?", filtro.getContacrm())
				.whereIn("vendaorcamento.cdvendaorcamento", filtro.getWhereInVendaorcamento())
				.where("vendaorcamentosituacao.cdvendaorcamentosituacao <> ?", Vendaorcamentosituacao.CANCELADA.getCdvendaorcamentosituacao());
			
			if (SinedUtil.isListNotEmpty(filtro.getListapedidovendatipo())) {
				query
					.openParentheses()
					.whereIn("pedidovendatipo.cdpedidovendatipo",CollectionsUtil.listAndConcatenate(filtro.getListapedidovendatipo(), "cdpedidovendatipo", ","))
					.or()
					.where("pedidovendatipo is null")
					.closeParentheses();
			} else {
				query
					.openParentheses()
					.where("pedidovendatipo.bonificacao <> true")
					.or()
					.where("pedidovendatipo is null")
					.closeParentheses();
			}
			
			if(filtro.getIsRestricaoVendaVendedor() != null && filtro.getIsRestricaoVendaVendedor()) {
				
				Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
				
				if(colaborador == null) {
					query.where("1 = 0");
				} else {
					
					if(filtro.getIsRestricaoClienteVendedor() == null || !filtro.getIsRestricaoVendaVendedor()) {
						filtro.setColaborador(colaborador);
						query.where("colaborador = ?", colaborador);					
					} else {
						
						if(filtro.getColaborador() == null || !filtro.getColaborador().equals(colaborador)) {
							if(filtro.getColaborador() == null) {
								query.openParentheses()
									.where("colaboradorcliente = ?", colaborador)
									.or()
									.where("colaborador = ?", colaborador)
									.closeParentheses();
							} else {
								query.openParentheses()
									.where("colaboradorcliente = ?", colaborador)
									.where("colaborador = ?", filtro.getColaborador())
									.closeParentheses();
							}
						} else {
							query.where("colaborador = ?", colaborador);
						}
						
					}
				}
			} else {
				
				if (filtro.getColaborador() != null && filtro.getIsVendedorPrincipal() != null && filtro.getIsVendedorPrincipal()) {
					query
						.openParentheses()
							.where("listaClientevendedor.colaborador = ?", filtro.getColaborador())
							.where("listaClientevendedor.principal = ?", Boolean.TRUE)
						.closeParentheses();
				} else {
					query.where("colaborador = ?", filtro.getColaborador());
				}
			}
			
			if(Boolean.TRUE.equals(filtro.getVendedoresAtuais())) {
				query.join("colaborador.colaboradorsituacao colaboradorsituacao")
				.leftOuterJoin("colaborador.listaContratocolaborador listaContratocolaborador")
				.leftOuterJoin("listaContratocolaborador.contrato contrato")
				.where("colaboradorsituacao.cdcolaboradorsituacao <> ?", Colaboradorsituacao.DEMITIDO)
				.orderBy("colaborador.nome");
			}
		}
		
		adicionaJoinWhereForMaterial(filtro, query);
		
		return query.list();
	}
	
	private void adicionaJoinWhereForMaterial(EmitircustovendaFiltro filtro,
			QueryBuilder<Vendaorcamento> query) {
		
		if(filtro.getMaterial() != null || 
				(filtro.getProduto() != null && filtro.getProduto()) ||
				(filtro.getServico() != null && filtro.getServico()) ||
				(filtro.getEpi() != null && filtro.getEpi()) ||
				(filtro.getPatrimonio() != null && filtro.getPatrimonio()) ||
				filtro.getMaterialcategoria() != null) {
			
			StringBuilder selectForMaterial = new StringBuilder();
			String whereMaterialtipo = "";
			String whereMaterial = "";
			String whereFornecedor = "";
			String whereClasseMaterial = "";
			String whereMaterialcategoria = "";
			
			if(filtro.getMaterialcategoria()!=null) {
				selectForMaterial.append(",materialcategoria.cdmaterialcategoria ");
				query.leftOuterJoinIfNotExists("material.materialcategoria materialcategoria");
				whereMaterialcategoria += " AND m.materialcategoria.cdmaterialcategoria = " + filtro.getMaterialcategoria().getCdmaterialcategoria() + " ";
			}
			
			if(filtro.getMaterial()!=null) {
				whereMaterial += " AND m.cdmaterial = " + filtro.getMaterial().getCdmaterial() + " ";
			}
			
			if (filtro.getProduto() || filtro.getServico() || filtro.getEpi() || filtro.getPatrimonio()) {
				selectForMaterial.append(",material.produto, material.servico, material.epi, material.patrimonio ");
				whereClasseMaterial = " AND ";
				whereClasseMaterial += " ( ";
				
				if (filtro.getProduto()) {
					whereClasseMaterial += " m.produto = " + filtro.getProduto() + " ";
				}
				
				if (filtro.getServico()) {
					if (filtro.getProduto()) {
						whereClasseMaterial += " OR ";
					}
					whereClasseMaterial += " m.servico = " + filtro.getServico() + " ";
				}
				
				if (filtro.getEpi()) {
					if (filtro.getProduto() || filtro.getServico()) {
						whereClasseMaterial += " OR ";
					}
					whereClasseMaterial += " m.epi = " + filtro.getEpi() + " ";
				}
				
				if (filtro.getPatrimonio()) {
					if (filtro.getProduto() || filtro.getServico() || filtro.getEpi()) {
						whereClasseMaterial += " OR ";
					}
					whereClasseMaterial += " m.patrimonio = " + filtro.getPatrimonio() + " ";
				}
				whereClasseMaterial += " ) ";
			}
			
			query.select(query.getSelect().getValue() + selectForMaterial.toString());
			query.where(" exists (	  select vom.vendaorcamento.cdvendaorcamento " +
								 	" from Vendaorcamentomaterial vom " +
								 	" join vom.material m " +
								 	" where vom.vendaorcamento.cdvendaorcamento = vendaorcamento.cdvendaorcamento " +
								 	whereMaterialtipo +
								 	whereMaterialcategoria +
								 	whereMaterial + 
								 	whereFornecedor +
								 	whereClasseMaterial + " ) ");
		}
	}
}