package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Partecontraria;
import br.com.linkcom.sined.geral.bean.Processojuridicocliente;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProcessojuridicoclienteDAO extends GenericDAO<Processojuridicocliente>{
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Processojuridicocliente> findForFichaProcessoJuridico(String whereIn) {
		return querySined()
				
			.select("processojuridico.cdprocessojuridico, cliente.nome, cliente.cpf, cliente.cnpj," +
					"enderecoCliente.logradouro, enderecoCliente.numero, enderecoCliente.complemento," +
					"enderecoCliente.bairro, enderecoCliente.cep, municipioCliente.nome, ufCliente.sigla," +
					"partecontraria.nome, partecontraria.cpf, partecontraria.cnpj, partecontraria.logradouro, " +
					"partecontraria.numero, partecontraria.cep, partecontraria.complemento, partecontraria.bairro, " +
					"municipioPartecontraria.nome, ufPartecontraria.sigla, processojuridicocliente.envolvido")
			.join("processojuridicocliente.processojuridico processojuridico")
			.leftOuterJoin("processojuridicocliente.cliente cliente")
			.leftOuterJoin("cliente.listaEndereco enderecoCliente")
			.leftOuterJoin("enderecoCliente.municipio municipioCliente")
			.leftOuterJoin("municipioCliente.uf ufCliente")
			.leftOuterJoin("processojuridicocliente.partecontraria partecontraria")
			.leftOuterJoin("partecontraria.municipio municipioPartecontraria")
			.leftOuterJoin("municipioPartecontraria.uf ufPartecontraria")
			.whereIn("processojuridico.cdprocessojuridico", whereIn)
			.list();
	}

	public Integer countByParteContraria(Partecontraria partecontraria) {
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.from(Processojuridicocliente.class)
					.leftOuterJoin("processojuridicocliente.partecontraria partecontraria")
					.where("partecontraria = ?", partecontraria)
					.unique().intValue();
	}
}