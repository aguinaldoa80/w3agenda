package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Configuracaognre;
import br.com.linkcom.sined.geral.bean.Configuracaognrerateio;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ConfiguracaognrerateioDAO extends GenericDAO<Configuracaognrerateio> {

	public List<Configuracaognrerateio> findByConfiguracaognre(Configuracaognre configuracaognre) {
		if(configuracaognre == null || configuracaognre.getCdconfiguracaognre() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
				.select("configuracaognrerateio.cdconfiguracaognrerateio, configuracaognrerateio.percentual, " +
						"centrocusto.cdcentrocusto, centrocusto.nome, " +
						"projeto.cdprojeto, projeto.nome, " +
						"contagerencial.cdcontagerencial, contagerencial.nome, vcontagerencial.identificador, vcontagerencial.arvorepai")
				.leftOuterJoin("configuracaognrerateio.contagerencial contagerencial")
				.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
				.leftOuterJoin("configuracaognrerateio.centrocusto centrocusto")
				.leftOuterJoin("configuracaognrerateio.projeto projeto")
				.where("configuracaognrerateio.configuracaognre = ?", configuracaognre)
				.list();
	}

}
