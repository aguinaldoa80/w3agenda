package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendapagamento;
import br.com.linkcom.sined.geral.bean.enumeration.GeracaocontareceberEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PedidovendapagamentoDAO extends GenericDAO<Pedidovendapagamento>{

	/**
	 * Busca a lista de pagamentos de um pedido de venda.
	 *
	 * @param pedidovenda
	 * @return
	 * @since 16/11/2011
	 * @author Rodrigo Freitas
	 */
	public List<Pedidovendapagamento> findByPedidovenda(Pedidovenda pedidovenda){
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null){
			throw new SinedException("Pedido de venda n�o pode ser nulo.");
		}
		return query()
					.select("pedidovendapagamento.cdpedidovendapagamento, pedidovendapagamento.banco, pedidovendapagamento.emitente, pedidovendapagamento.cpfcnpj, " +
							"pedidovendapagamento.agencia, pedidovendapagamento.conta, pedidovendapagamento.numero, " +
							"pedidovendapagamento.valororiginal, pedidovendapagamento.dataparcela, documentotipo.cddocumentotipo, documentotipo.cartao, " +
							"documentotipo.antecipacao, documentotipo.nome, documento.cddocumento, documento.numero, documento.valor, " +
							"documentoantecipacao.cddocumento, documentoantecipacao.numero, documentoantecipacao.valor, prazopagamento.cdprazopagamento, " +
							"prazopagamento.nome, prazopagamento.juros, pedidovenda.cdpedidovenda, pedidovendapagamento.valorjuros, cheque.cdcheque," +
							"documentoacao.cddocumentoacao, documentoacaoAntecipacao.cddocumentoacao ")
					.join("pedidovendapagamento.pedidovenda pedidovenda")
					.join("pedidovendapagamento.documentotipo documentotipo")
					.leftOuterJoin("pedidovenda.prazopagamento prazopagamento")
					.leftOuterJoin("pedidovendapagamento.documento documento")
					.leftOuterJoin("documento.documentoacao documentoacao")
					.leftOuterJoin("pedidovendapagamento.documentoantecipacao documentoantecipacao")
					.leftOuterJoin("documentoantecipacao.documentoacao documentoacaoAntecipacao")
					.leftOuterJoin("pedidovendapagamento.cheque cheque")
					.where("pedidovenda = ?", pedidovenda)
					.orderBy("pedidovendapagamento.dataparcela")
					.list();
	}

	/**
	* M�todo que verifica se existe documento vinculado ao pedido de venda
	*
	* @param pedidovenda
	* @return
	* @since 03/12/2014
	* @author Luiz Fernando
	*/
	public boolean existeDocumento(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			return false;
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Pedidovendapagamento.class)
			.join("pedidovendapagamento.pedidovenda pedidovenda")
			.leftOuterJoin("pedidovendapagamento.documento documento")
			.leftOuterJoin("pedidovendapagamento.documentoantecipacao documentoantecipacao")
			.where("pedidovenda = ?", pedidovenda)
			.openParentheses()
				.where("documentoantecipacao is not null").or()
				.where("documento is not null")
			.closeParentheses()
			.unique() > 0;
	}
	
	/**
	* M�todo que verifica se existe pedidovendapagamento sem documento vinculado
	*
	* @param pedidovenda
	* @return
	* @since 23/09/2015
	* @author Luiz Fernando
	*/
	public boolean existItemSemDocumento(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			return false;
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Pedidovendapagamento.class)
			.join("pedidovendapagamento.pedidovenda pedidovenda")
			.leftOuterJoin("pedidovendapagamento.documento documento")
			.leftOuterJoin("pedidovendapagamento.documentoantecipacao documentoantecipacao")
			.where("pedidovenda = ?", pedidovenda)
			.where("documentoantecipacao is null")
			.where("documento is null")
			.unique() > 0;
	}
	
	/**
	 * M�todo que seta o cdcheque como nulo em pedidovendapagamento
	 *
	 * @param whereIn
	 * @author Mairon Cezar
	 * @since 18/11/2016
	 */
	public void updateReferenciaCheque(String whereIn) {
		if(whereIn != null && !"".equals(whereIn)){
			getJdbcTemplate().update("update pedidovendapagamento set cdcheque = NULL where cdpedidovendapagamento in (" + whereIn + ")");
		}
	}
	
	/**
	 * M�todo que busca as informa��es do cheque do pedido de venda
	 *
	 * @param documento
	 * @return
	 * @author Mairon Cezar
	 */
	public Pedidovendapagamento findForChequeByDocumento(Documento documento) {
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("pedidovendapagamento.cdpedidovendapagamento, pedidovendapagamento.banco, pedidovendapagamento.agencia, pedidovendapagamento.agencia, " +
						"pedidovendapagamento.conta, pedidovendapagamento.numero, pedidovendapagamento.dataparcela, pedidovendapagamento.cheque")
				.leftOuterJoin("pedidovendapagamento.pedidovenda pedidovenda")
				.leftOuterJoin("pedidovendapagamento.documento documento")
				.join("pedidovendapagamento.cheque cheque")
				.where("documento = ?", documento)
				.unique();
	}

	public List<Pedidovendapagamento> buscarPorVendaDoClienteSemContaReceber(
			Cliente cliente) {
		
		return query()
				.select("pedidovendapagamento.cdpedidovendapagamento,pedidovendapagamento.valororiginal,pedidovenda.cdpedidovenda,pedidovenda.pedidovendasituacao, listaVenda.cdvenda, listavendapagamento.valororiginal")
				.join("pedidovendapagamento.pedidovenda pedidovenda")
				.join("pedidovenda.pedidovendatipo pedidovendatipo")
//				.join("pedidovendatipo.geracaocontareceberEnum geracaocontareceberEnum")
				.join("pedidovenda.cliente cliente")
				.leftOuterJoin("pedidovenda.listaVenda listaVenda")
				.leftOuterJoin("listaVenda.listavendapagamento listavendapagamento")
				.where("pedidovendapagamento.documento is null")
				.where("pedidovendapagamento.documentoantecipacao is null")
				.where("cliente.cdpessoa = ?", cliente.getCdpessoa())
				.where("pedidovendatipo.bonificacao <> true")
				.where("pedidovendatipo.geracaocontareceberEnum <> ?", GeracaocontareceberEnum.NAO_GERAR_CONTA)
				.openParentheses()
					.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.AGUARDANDO_APROVACAO)
					.or()
					.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.PREVISTA)
					.or()
					.where("pedidovenda.pedidovendasituacao = ?", Pedidovendasituacao.CONFIRMADO_PARCIALMENTE)
				.closeParentheses()
				.list();
	}

	public List<Pedidovendapagamento> findPedidovendaPagamentoByPedidovenda(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null){
			throw new SinedException("Par�metros inv�lidos!");
		}
		
		return query()
			.select("pedidovendapagamento.cdpedidovendapagamento, pedidovendapagamento.numero, pedidovendapagamento.valororiginal, pedidovendapagamento.banco, pedidovendapagamento.agencia, " +
					"pedidovendapagamento.conta, pedidovendapagamento.dataparcela, pedidovendapagamento.valorjuros, documento.cddocumento, documento.dtvencimento, documentotipo.cddocumentotipo, documentotipo.nome, " +
					"prazopagamento.cdprazopagamento, prazopagamento.nome, pedidovenda.cdpedidovenda, documentotipo.antecipacao, documentotipo.exibirnavenda, " + 
					"documentoantecipacao.cddocumento, documentoantecipacao.numero, documentoantecipacao.valor, " +
					"aux_documento.cddocumento, aux_documento.valoratual, documento.valor, documentoacao.cddocumentoacao, cheque.cdcheque, pedidovendapagamento.emitente, pedidovendapagamento.cpfcnpj ")
			.join("pedidovendapagamento.pedidovenda pedidovenda")
			.leftOuterJoin("pedidovendapagamento.documento documento")
			.leftOuterJoin("documento.documentoacao documentoacao")
			.leftOuterJoin("documento.aux_documento aux_documento")
			.leftOuterJoin("pedidovendapagamento.documentoantecipacao documentoantecipacao")
			.leftOuterJoin("pedidovendapagamento.documentotipo documentotipo")
			.leftOuterJoin("pedidovenda.prazopagamento prazopagamento")
			.leftOuterJoin("pedidovendapagamento.cheque cheque")
			.where("pedidovenda = ?", pedidovenda)
			.orderBy("pedidovendapagamento.dataparcela, documento.cddocumento")
			.list();
	}
	
}
