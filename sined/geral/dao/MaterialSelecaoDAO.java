package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.MaterialSelecaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;


@DefaultOrderBy("material.nome")
public class MaterialSelecaoDAO extends GenericDAO<Material> {

	@Override
	public void updateListagemQuery(QueryBuilder<Material> query, FiltroListagem _filtro) {
		MaterialSelecaoFiltro filtro = (MaterialSelecaoFiltro) _filtro;
		
		query
				.select(
						"distinct " +
						"material.cdmaterial, material.produto, material.epi, material.patrimonio, material.servico, " +
						"material.nome, material.identificacao, unidademedida.cdunidademedida, unidademedida.simbolo, " +
						"materialtipo.nome, materialgrupo.cdmaterialgrupo, materialgrupo.nome, materialcategoria.descricao")
				.join("material.unidademedida unidademedida")
				.join("material.materialgrupo materialgrupo")
				.leftOuterJoin("material.materialtipo materialtipo")
				.leftOuterJoin("material.materialcategoria materialcategoria")
				.whereLikeIgnoreAll("material.nome", filtro.getNome())
				.whereLikeIgnoreAll("material.cdmaterial", filtro.getCodigo())
				.whereLikeIgnoreAll("material.identificacao", filtro.getIdentificacao())
				.orderBy("material.cdmaterial desc");
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Material> query) {
		throw new SinedException("A��o n�o permitida");
	}

	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		throw new SinedException("A��o n�o permitida");
	}
	
	@Override
	public void saveOrUpdateNoUseTransactionWithoutLog(Material bean) {
		throw new SinedException("A��o n�o permitida");
	}
	
	@Override
	protected SaveOrUpdateStrategy save(Object entity) {
		throw new SinedException("A��o n�o permitida");
	}
	
	@Override
	public void saveOrUpdateNoUseTransaction(Material bean) {
		throw new SinedException("A��o n�o permitida");
	}
	
	@Override
	public void saveOrUpdateWithoutLog(Material bean) {
		throw new SinedException("A��o n�o permitida");
	}

	@Override
	public void saveOrUpdate(Material bean) {
		throw new SinedException("A��o n�o permitida");
	}
}