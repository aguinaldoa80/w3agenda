package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialimposto;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialimpostoDAO extends GenericDAO<Materialimposto>{

	/**
	 * Deleta registro da tabela relacionada ao material
	 *
	 * @param material
	 * @author Rodrigo Freitas
	 * @since 19/10/2015
	 */
	public void deleteByMaterial(Material material) {
		if(material == null || material.getCdmaterial() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		getHibernateTemplate().bulkUpdate("delete from Materialimposto mi where mi.material = ?", material);
	}

	/**
	 * Deleta registro da tabela relacionada ao material
	 *
	 * @param material
	 * @param whereNotIn
	 * @author Rodrigo Freitas
	 * @since 19/10/2015
	 */
	public void deleteByMaterialWhereNotIn(Material material, String whereNotIn) {
		if(material == null || material.getCdmaterial() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		if(whereNotIn != null && !whereNotIn.equals("")){
			getHibernateTemplate().bulkUpdate("delete from Materialimposto mi where mi.material = ? and mi.cdmaterialimposto not in (" + whereNotIn + ")", material);
		} else {
			getHibernateTemplate().bulkUpdate("delete from Materialimposto mi where mi.material = ?", material);
		}
	}

}