package br.com.linkcom.sined.geral.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("taxaitem.cdtipotaxa")
public class TaxaitemDAO extends GenericDAO<Taxaitem>{

	
	/**
	 * Retorna dados uma Taxaitem caso a Taxa for do tipo Multa.
	 * 
	 * @author Taidson
	 * @since 23/05/2010
	 * */
	public Taxaitem findByTaxaMulta(Taxa taxa){
		if(taxa == null || taxa.getCdtaxa() == null){
			throw new SinedException("Os par�metros documento ou cddocumento n�o podem ser null.");
		}
		
		return 
			query()
			.select("taxa.cdtaxa, taxaitem.cdtaxaitem, taxaitem.percentual,taxaitem.valor, taxaitem.dtlimite, tipotaxa.cdtipotaxa")
			.leftOuterJoin("taxaitem.taxa taxa")
			.leftOuterJoin("taxaitem.tipotaxa tipotaxa")
			.where("taxa = ?",taxa)
			.where("tipotaxa = ?",Tipotaxa.MULTA)
			.unique();
	}

	/**
	 * Retorna dados uma Taxaitem caso a Taxa for do tipo Juros.
	 * 
	 * @author Taidson
	 * @since 23/05/2010
	 * */
	public Taxaitem findByTaxaJuros(Taxa taxa){
		if(taxa == null || taxa.getCdtaxa() == null){
			throw new SinedException("Os par�metros documento ou cddocumento n�o podem ser null.");
		}
		
		return 
		query()
			.select("taxa.cdtaxa, taxaitem.cdtaxaitem, taxaitem.percentual,taxaitem.valor, taxaitem.dtlimite, tipotaxa.cdtipotaxa")
			.leftOuterJoin("taxaitem.taxa taxa")
			.leftOuterJoin("taxaitem.tipotaxa tipotaxa")
			.where("taxa = ?",taxa)
			.where("tipotaxa = ?",Tipotaxa.JUROS)
			.unique();
	}
	
	/**
	 * Retorna todas as taxas itens vinculadas � uma determinada Taxa.
	 * 
	 * @author Taidson
	 * @since 01/06/2010
	 * */
	public List<Taxaitem> findTaxasItens (Taxa taxa){
		return 
		query()
		.select("taxaitem.cdtaxaitem, taxaitem.percentual, taxaitem.valor, taxaitem.dtlimite, tipotaxa.cdtipotaxa, tipotaxa.nome, taxaitem.gerarmensagem")
		.leftOuterJoin("taxaitem.taxa taxa")
		.leftOuterJoin("taxaitem.tipotaxa tipotaxa")
		.where("taxa = ?",taxa)
		.list();
		
	}
	
	/**
	 * Retorna dados uma Taxaitem caso a Taxa for do tipo Desconto.
	 * 
	 * @author Marden
	 * @since 16/08/2011
	 * */
	public Taxaitem findByTaxaDesconto(Taxa taxa){
		if(taxa == null || taxa.getCdtaxa() == null){
			throw new SinedException("Os par�metros documento ou cddocumento n�o podem ser null.");
		}
		
		return 
			query()
			.select("taxa.cdtaxa, taxaitem.cdtaxaitem, taxaitem.percentual,taxaitem.valor, taxaitem.dtlimite, tipotaxa.cdtipotaxa")
			.leftOuterJoin("taxaitem.taxa taxa")
			.leftOuterJoin("taxaitem.tipotaxa tipotaxa")
			.where("taxa = ?",taxa)
			.where("tipotaxa = ?",Tipotaxa.DESCONTO)
			.unique();
	}
	
	/**
	 * 
	 * M�todo que carrega a taxaitem com tipo taxa para a gera��o do csv de conta a pagar e conta a receber
	 *
	 *@author Thiago Augusto
	 *@date 15/03/2012
	 * @param taxa
	 * @return
	 */
	public List<Taxaitem> findForTaxa(Taxa taxa){
		return query()
				.select("taxaitem.cdtaxaitem, taxaitem.motivo, taxaitem.valor, taxaitem.gerarmensagem, taxaitem.dtlimite, tipotaxa.cdtipotaxa, tipotaxa.nome")
				.leftOuterJoin("taxaitem.tipotaxa tipotaxa")
				.where("taxaitem.taxa = ?", taxa)
				.list();
	}

	public void atualizaDataVencimento(String selectedItens, Date dtlimite) {
		String set = "";
		set += "dtlimite='" + new SimpleDateFormat("yyyy-MM-dd").format(dtlimite) + "',";
		
		set = set.substring(0, set.length()-1);
		
		String sql = "update taxaitem set <SET> where cdtaxa in (select documento.cdtaxa from documento where cddocumento in (" + selectedItens + "))";
		sql = sql.replace("<SET>", set);
		
		getJdbcTemplate().update(sql);
		
	}

	/**
	* M�todo que atualiza a data limite da taxa
	*
	* @param taxaitem
	* @param dtlimite
	* @since 07/12/2015
	* @author Luiz Fernando
	*/
	public void updateDtlimite(Taxaitem taxaitem, java.sql.Date dtlimite) {
		String sql = "update taxaitem set dtlimite='" + new SimpleDateFormat("yyyy-MM-dd").format(dtlimite) + "' " +
				" where cdtaxaitem = " + taxaitem.getCdtaxaitem();
		getJdbcTemplate().update(sql);
		
	}

	public Taxaitem loadTaxaitemByDocumento(Documento documento, Tipotaxa tipotaxa) {
		if(documento == null || documento.getCddocumento() == null || tipotaxa == null || tipotaxa.getCdtipotaxa() == null)
			throw new SinedException("Par�metro inv�lido");
		
		return query()
				.select("taxaitem.cdtaxaitem, taxaitem.motivo, taxaitem.valor, taxaitem.dtlimite, taxaitem.aodia, taxaitem.gerarmensagem, taxa.cdtaxa, tipotaxa.cdtipotaxa ")
				.join("taxaitem.taxa taxa")
				.join("taxaitem.tipotaxa tipotaxa")
				.join("taxa.listaDocumento listaDocumento")
				.where("listaDocumento.cddocumento = ?", documento.getCddocumento())
				.where("taxaitem.tipotaxa = ?", tipotaxa)
				.unique();
	}

	public void updateValorMotivo(Taxaitem taxaitem, Money valor, Date dtlimite, String motivo) {
		if(taxaitem != null && taxaitem.getCdtaxaitem() != null && (valor != null || motivo != null)){
			String sql = 
					" update taxaitem set " +
					" valor=" + (valor != null ? (valor.getValue().doubleValue() * 100) : "valor") +
					" , " +
					" dtlimite = " + (dtlimite != null ? "'" + new SimpleDateFormat("yyyy-MM-dd").format(dtlimite) + "'" : "dtlimite") +
					" , " +
					" motivo=" + (motivo != null ? "'" + motivo + "'" : "motivo") +
					" where cdtaxaitem = " + taxaitem.getCdtaxaitem();
			getJdbcTemplate().update(sql);
		}
		
	}
	
	public void deleteFromArquivoretornoAtualizarJurosMulta(Taxa taxa) {
		getHibernateTemplate().bulkUpdate("delete from Taxaitem ti where ti.taxa=? and ti.tipotaxa in (?, ?)", new Object[]{taxa, Tipotaxa.JUROS, Tipotaxa.MULTA});
	}
	
	public List<Taxaitem> findTaxaitemByDocumento(Documento documento, Tipotaxa... tipotaxa) {
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Par�metro inv�lido");
		
		String whereInTipotaxa = null;
		StringBuilder values = new StringBuilder();
		for (int i=0; i < tipotaxa.length; i++) {
			values.append(tipotaxa[i].getCdtipotaxa()).append(",");
		}
		whereInTipotaxa = values.toString().substring(0, values.toString().length()-1);
		
		return query()
				.select("taxaitem.cdtaxaitem, taxaitem.motivo, taxaitem.valor, taxaitem.dtlimite, taxaitem.aodia, taxaitem.gerarmensagem, taxa.cdtaxa, tipotaxa.cdtipotaxa ")
				.join("taxaitem.taxa taxa")
				.join("taxaitem.tipotaxa tipotaxa")
				.join("taxa.listaDocumento listaDocumento")
				.where("listaDocumento.cddocumento = ?", documento.getCddocumento())
				.whereIn("taxaitem.tipotaxa", whereInTipotaxa)
				.list();
	}
}