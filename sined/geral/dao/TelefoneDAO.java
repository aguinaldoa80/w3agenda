package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Telefonetipo;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("telefone.telefone")
public class TelefoneDAO extends GenericDAO<Telefone>{

	/**
	 * M�todo para salvar um telefone sem usar transaction.
	 * 
	 * @param bean
	 * @author Fl�vio Tavares
	 */
	public void saveOrUpdateNoTransaction(Telefone bean) {
		SaveOrUpdateStrategy save = save(bean);
		save.useTransaction(false);
		save.execute();
		getHibernateTemplate().flush();
	}

	/**
	 * Carrega a lista de telefones de uma pessoa.
	 * 
	 * @param pessoa
	 * @return
	 * @author Hugo Ferreira
	 */
	@SuppressWarnings("unchecked")
	public List<Telefone> carregarListaTelefone(Pessoa pessoa, Integer cdNota) {
		if (pessoa == null || pessoa.getCdpessoa() == null) {
			throw new SinedException("O par�metro pessoa n�o pode ser nulo.");
		}
		
//		return query()
//					.select("telefone.cdtelefone, telefone.telefone")
//					.join("telefone.pessoa pessoa")
//					.where("pessoa = ?",pessoa)
//					.list();
		
		
		StringBuilder query = new StringBuilder();
		List<Object> parametros = new ArrayList<Object>();
		
		query.append("SELECT cdtelefone, telefone, telefonetipo.cdtelefonetipo, telefonetipo.nome ");
		query.append("FROM telefone ");
		query.append("LEFT OUTER JOIN telefonetipo telefonetipo on telefonetipo.cdtelefonetipo = telefone.cdtelefonetipo ");
		query.append("WHERE cdpessoa = ? "); parametros.add(pessoa.getCdpessoa());
		if (cdNota != null) {
			query.append("UNION ");
			query.append("SELECT -1 AS cdtelefone, telefone ");
			query.append("FROM nota ");
			query.append("WHERE cdnota = ? "); parametros.add(cdNota);
			query.append("AND telefone IS NOT NULL ");
			query.append("AND NOT EXISTS (");
				query.append("SELECT telefone ");
				query.append("FROM telefone ");
				query.append("WHERE telefone.cdpessoa = ? "); parametros.add(pessoa.getCdpessoa());
				query.append("AND telefone = nota.telefone");
			query.append(")");
		}
		

		SinedUtil.markAsReader();
		List lista = getJdbcTemplate().query(query.toString(), parametros.toArray(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Telefone tel = new Telefone();
				
				tel.setCdtelefone(rs.getInt("cdtelefone"));
				tel.setTelefone(rs.getString("telefone"));
				tel.setTelefonetipo(new Telefonetipo(rs.getInt("cdtelefonetipo"), rs.getString("nome")));
				
				return tel;
			}
		});
		
		return (List<Telefone>)lista;
	}

	/**
	 * Busca os telefones daquela pessoa.
	 *
	 * @param c
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Telefone> findByPessoa(Pessoa pessoa) {
		if (pessoa == null || pessoa.getCdpessoa() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return 
			query()
				.select("telefone.cdtelefone, telefone.telefone, telefonetipo.nome, telefonetipo.cdtelefonetipo")
				.join("telefone.telefonetipo telefonetipo")
				.where("telefone.pessoa = ?",pessoa)
				.orderBy("telefonetipo.nome")
				.list();
	}
	
	/**
	 * Busca um telefone �nico, respeitando a prioridade por tipo
	 *
	 * @param c
	 * @return
	 * @author Mairon Cezar
	 */
	public Telefone loadTelefoneUnicoByPessoa(Pessoa pessoa) {
		if (pessoa == null || pessoa.getCdpessoa() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return 
			query()
				.select("telefone.cdtelefone, telefone.telefone, telefonetipo.nome, telefonetipo.cdtelefonetipo")
				.join("telefone.telefonetipo telefonetipo")
				.where("telefone.pessoa = ?",pessoa)
				.orderBy("telefonetipo.cdtelefonetipo")
				.setMaxResults(1)
				.unique();
	}
}
