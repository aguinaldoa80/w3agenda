package br.com.linkcom.sined.geral.dao;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.enumeration.EmailStatusEnum;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.EmailCobranca;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.EmailCobrancaFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

/**
 * @author Andrey Leonardo
 *
 */
public class EmailCobrancaDAO extends GenericDAO<EmailCobranca>{
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaEmailCobrancaDocumento");
		super.updateSaveOrUpdate(save);
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<EmailCobranca> query,
			FiltroListagem _filtro) {
		EmailCobrancaFiltro filtro = (EmailCobrancaFiltro)_filtro;
		
		query.select("emailCobranca.cdemailcobranca, emailCobranca.email, emailCobranca.dataenvio, emailCobranca.envioAutomatico, " +
				"emailCobranca.ultimaverificacao, emailCobranca.situacaoemail, documento.cddocumento, " +
				"aux_documento.valoratual, documentoacao.cddocumentoacao, documentoacao.nome, pessoa.nome")
				.join("emailCobranca.listaEmailCobrancaDocumento listaEmailCobrancaDocumento")
				.join("listaEmailCobrancaDocumento.documento documento")
				.leftOuterJoin("documento.pessoa pessoa")
				.join("documento.aux_documento aux_documento")				
				.join("documento.documentoacao documentoacao")				
				.whereLikeIgnoreAll("pessoa.nome", filtro.getCliente())
				.whereLikeIgnoreAll("emailCobranca.email", filtro.getEmail())
				.where("emailCobranca.dataenvio >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getEnvioDe()))
				.where("emailCobranca.dataenvio <= ?", SinedDateUtils.dateToTimestampFinalDia(filtro.getEnvioAte()))
				.orderBy("emailCobranca.dataenvio desc");
		
		if(filtro.getListaEmailStatus() != null) 
			query.whereIn("emailCobranca.situacaoemail", EmailStatusEnum.listAndConcatenate(filtro.getListaEmailStatus()));
		
		List<Documentoacao> listaAcao = filtro.getListaDocumentoacao();
		if (listaAcao != null) {
			query.whereIn("documentoacao.cddocumentoacao", SinedUtil.listAndConcatenate(listaAcao, "cddocumentoacao", ","));
		}
	}
	
	/**
	 * Verifica se o email est� com status lido
	 * @param whereIn
	 * @return
	 * @author Andrey Leonardo
	 */
	public Boolean isEmailLido(String whereIn){
		QueryBuilder<Long> queryBuilder = newQueryBuilderWithFrom(Long.class)
		.select("count(*)")
		.whereIn("emailCobranca.cdemailcobranca", whereIn)
		.where("emailCobranca.situacaoemail = ?", EmailStatusEnum.LIDO);
		
		return queryBuilder.unique() > 0;
	}
	
	/**
	 * Verifica se o email est� com status inv�lido
	 * @param whereIn
	 * @return
	 * @author Andrey Leonardo
	 */
	public Boolean isEmailInvalido(String whereIn){
		QueryBuilder<Long> queryBuilder = newQueryBuilderWithFrom(Long.class)
		.select("count(*)")
		.whereIn("emailCobranca.cdemailcobranca", whereIn)
		.where("emailCobranca.situacaoemail = ?", EmailStatusEnum.EMAIL_INVALIDO);
		
		return queryBuilder.unique() > 0;
	}
	
	/**
	 * @param whereIn
	 * @return
	 * @author Andrey Leonardo
	 */
	public List<EmailCobranca> loadEmailCobrancaToReenvio(String whereIn){
		return querySined()
			
			.select("emailCobranca.cdemailcobranca, emailCobranca.email," +
				"documento.cddocumento, pessoa.nome")
		.leftOuterJoin("emailCobranca.pessoa pessoa")
		.leftOuterJoin("emailCobranca.listaEmailCobrancaDocumento listaEmailCobrancaDocumento")
		.leftOuterJoin("listaEmailCobrancaDocumento.documento documento")	
		.whereIn("emailCobranca.cdemailcobranca", whereIn)
		.list();
	}
	
	/**
	 * @param emailCobranca
	 * @author Andrey Leonardo
	 */
	public void updateEmailCobranca(EmailCobranca emailCobranca){		
		if(emailCobranca != null && emailCobranca.getCdemailcobranca() != null){
			getHibernateTemplate()
			.bulkUpdate("update EmailCobranca e set e.dataenvio = ?, e.ultimaverificacao = null, e.situacaoemail = ? where e.id = ?", 
					new Object[]{new Timestamp(new Date().getTime()), EmailStatusEnum.ENVIADO, emailCobranca.getCdemailcobranca()});
		}
	}	
	
	/**
	 * @param emailCobranca
	 * @param status
	 * @author Andrey Leonardo
	 */
	public void updateEmailCobrancaStatus(EmailCobranca emailCobranca, String status){
		if(emailCobranca != null && status != null && !status.isEmpty()){
			getHibernateTemplate()
			.bulkUpdate("update EmailCobranca e set e.situacaoemail = ? where e.id = ?",
					new Object[]{EmailStatusEnum.valueOf(status), emailCobranca.getCdemailcobranca()});
		}
	}
	
	/**
	 * @param emailCobranca
	 * @author Andrey Leonardo
	 */
	public void updateEmailCobrancaEmail(EmailCobranca emailCobranca){
		if(emailCobranca != null && emailCobranca.getEmail() != null && !emailCobranca.getEmail().isEmpty()){
			getHibernateTemplate()
			.bulkUpdate("update EmailCobranca e set e.email = ? where e.id = ?",
					new Object[]{emailCobranca.getEmail(), emailCobranca.getCdemailcobranca()});
		}
	}
	
	/**
	 * Verifica se o cdpessoa � referencia da tabela contato ou n�o
	 * e retorna um email cobranca preenchido com os valores retornados pela query
	 * @param emailCobranca
	 * @return
	 * @author Andrey Leonardo
	 */
	@SuppressWarnings("unchecked")
	public EmailCobranca loadEmailPessoaContato(EmailCobranca emailCobranca){
		if(emailCobranca == null || emailCobranca.getCdemailcobranca() == null){
			throw new SinedException("Email Cobran�a n�o pode ser nula.");
		}
		StringBuilder sql = new StringBuilder();		
		sql
			.append("SELECT pessoa.cdpessoa, pessoa.email, contato.cdpessoa, contato.emailcontato")
			.append(" FROM pessoa ")			
			.append(" LEFT OUTER JOIN contato ON contato.cdpessoa = pessoa.cdpessoa ")			
			.append(" WHERE pessoa.cdpessoa = ?");

		SinedUtil.markAsReader();
		List<EmailCobranca> lista = getJdbcTemplate().query(sql.toString(),new Object[]{emailCobranca.getPessoa().getCdpessoa()}, new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				EmailCobranca bean = new EmailCobranca();
				
				bean.setPessoa(new Pessoa(rs.getInt(1)));
				bean.getPessoa().setEmail(rs.getString(2));
				if(rs.getInt(3) > 0){
					bean.setContato(new Contato(rs.getInt(3)));
					bean.getContato().setEmailcontato(rs.getString(4));
				}
				return bean;
			}
		});
		emailCobranca.setContato(lista.get(0).getContato());
		emailCobranca.setPessoa(lista.get(0).getPessoa());
		return emailCobranca;
	}
	
	/**
	 * @param emailCobranca
	 * @return
	 * @author Andrey Leonardo
	 */
	public EmailCobranca loadEmailCobranca(EmailCobranca emailCobranca){
		return query().select("emailCobranca.cdemailcobranca, emailCobranca.email, emailCobranca.envioAutomatico, " +
				"pessoa.cdpessoa, documento.cddocumento, emailCobranca.dataenvio, " +
				"emailCobranca.ultimaverificacao, emailCobranca.situacaoemail," +
				"historicoEmailCobranca.cdhistorico, historicoEmailCobranca.event, historicoEmailCobranca.response," +
				"historicoEmailCobranca.attempt,historicoEmailCobranca.urlclicked, historicoEmailCobranca.status," +
				"historicoEmailCobranca.reason, historicoEmailCobranca.type, historicoEmailCobranca.ip, " +
				"historicoEmailCobranca.useragent, historicoEmailCobranca.ultimaverificacao, " +
				"listaEmailCobrancaDocumento.cdEmailCobrancaDocumento")
				.join("emailCobranca.listaEmailCobrancaDocumento listaEmailCobrancaDocumento")
				.join("listaEmailCobrancaDocumento.documento documento")
				.leftOuterJoin("emailCobranca.pessoa pessoa")
				.leftOuterJoin("emailCobranca.historicoEmailCobranca historicoEmailCobranca")
				.where("emailCobranca.cdemailcobranca = ?", emailCobranca.getCdemailcobranca())
				.unique();
	}

	@Override
	public ListagemResult<EmailCobranca> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<EmailCobranca> query = query();
		updateListagemQuery(query, filtro);
		return new ListagemResult<EmailCobranca>(query, filtro);
	}
}
