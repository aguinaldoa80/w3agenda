package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Vcategoria;
import br.com.linkcom.sined.geral.bean.view.Vwclientehistorico;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.VwclientehistoricoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VwclientehistoricoDAO extends GenericDAO<Vwclientehistorico>{
	
	private CategoriaService categoriaService;
	private EmpresaService empresaService;
	
	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Vwclientehistorico> query,FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		}
		VwclientehistoricoFiltro filtro = (VwclientehistoricoFiltro)_filtro;
		String whereInEmpresas = (filtro.getEmpresahistorico() != null)? filtro.getEmpresahistorico().getCdpessoa().toString():
																		CollectionsUtil.listAndConcatenate(empresaService.findByUsuario(), "cdpessoa", ",");
	
		Vcategoria vcategoria = new Vcategoria();
		if (filtro.getCategoria() != null){
			vcategoria = categoriaService.carregarIdentificador(filtro.getCategoria());
		}
		
		if(SinedUtil.isRestricaoEmpresaClienteUsuarioLogado()){
			query.where("true = permissao_clienteempresa(pessoa_cdpessoa, '" + new SinedUtil().getListaEmpresa() + "')");
		}
		 
		query
			.orderBy("pessoa_nome");
		if (filtro.getCategoria() != null){
			query.where("identificador like ?", vcategoria.getIdentificador() + "%");
		}
		if(filtro.getCliente() != null){
			query.where("pessoa_nome = ?", filtro.getCliente().getNome());
		}
		if(filtro.getAtivo() != null){
			query.where("pessoa_cdpessoa in (select c.cdpessoa from Cliente c where c.ativo = " + (filtro.getAtivo() ? "true" : "false") + ")");			
		} else {
			query.where("pessoa_cdpessoa in (select c.cdpessoa from Cliente c)");
		}
		if(filtro.getAtividadetipo() != null && filtro.getAtividadetipo().getCdatividadetipo() != null){
			query.where("pessoa_cdpessoa in ( select distinct c.cdpessoa " +
												"from Clientehistorico ch " +
												"join ch.cliente c " +
												"join ch.atividadetipo atividadetipo " +
												(whereInEmpresas != null? "join ch.empresahistorico em ": "")+
												"where atividadetipo.cdatividadetipo = "+filtro.getAtividadetipo().getCdatividadetipo()+
												(whereInEmpresas != null? "and em.cdpessoa in ("+whereInEmpresas+") ": "")+					
												") ");
		}
		if(filtro.getSituacao() != null){
			query.where("pessoa_cdpessoa in ( select distinct c.cdpessoa " +
						"					  from Clientehistorico ch " +
						"				      join ch.cliente c " +
						(whereInEmpresas != null? "join ch.empresahistorico em ": "")+
						"					  where ch.situacao = ? "+
						(whereInEmpresas != null? "and em.cdpessoa in ("+whereInEmpresas+") ": "")+
						")", filtro.getSituacao());
		}

		Boolean existeFlagMarcada = Boolean.FALSE;
		query.openParentheses();
			if(filtro.getExibirInteracao() != null && filtro.getExibirInteracao()){
				query
					.where("exists ( select c.cdpessoa " +
							"from Clientehistorico ch " +
							"join ch.cliente c " +
							(whereInEmpresas != null? "join ch.empresahistorico em ": "")+
							"where c.cdpessoa = pessoa_cdpessoa " +
							(whereInEmpresas != null? "and em.cdpessoa in ("+whereInEmpresas+")": "")+
							(filtro.getDtinteracaoinicio() != null ? " and ch.dtaltera >= '"+ SinedDateUtils.dateToTimestampInicioDia(filtro.getDtinteracaoinicio())+ "' " : "") +
							(filtro.getDtinteracaofim() != null ? " and ch.dtaltera <= '"+ SinedDateUtils.dateToTimestampFinalDia(filtro.getDtinteracaofim())+ "' " : "") +
							") ");
				existeFlagMarcada = Boolean.TRUE;
			}
			if(filtro.getExibirInteracaoVenda() != null && filtro.getExibirInteracaoVenda()){
				if(existeFlagMarcada){
					query.or();
				}
				query
					.where("exists ( select c.cdpessoa " +
							"from Vendahistorico vh " +
							"join vh.venda v " +
							"join v.cliente c " +
							(whereInEmpresas != null? "join v.empresa em ": "")+
							"where c.cdpessoa = pessoa_cdpessoa " +
							(whereInEmpresas != null? "and em.cdpessoa in ("+whereInEmpresas+") ": "")+
							(filtro.getDtinteracaoinicio() != null ? " and vh.dtaltera >= '"+ SinedDateUtils.dateToTimestampInicioDia(filtro.getDtinteracaoinicio())+ "' " : "") +
							(filtro.getDtinteracaofim() != null ? " and vh.dtaltera <= '"+ SinedDateUtils.dateToTimestampFinalDia(filtro.getDtinteracaofim())+ "' ": "") +
							") ");
				existeFlagMarcada = Boolean.TRUE;
			}
			if(filtro.getExibirRequisicao() != null && filtro.getExibirRequisicao()){
				if(existeFlagMarcada){
					query.or();
				}
				query
					.where("exists ( select c.cdpessoa " +
							"from Requisicaohistorico rh " +
							"join rh.requisicao r " +
							"join r.cliente c " +
							(whereInEmpresas != null? "join r.empresa em ": "")+
							"where c.cdpessoa = pessoa_cdpessoa " +
							(whereInEmpresas != null? "and em.cdpessoa in ("+whereInEmpresas+") ": "")+
							(filtro.getDtinteracaoinicio() != null ? " and rh.dtaltera >= '"+ SinedDateUtils.dateToTimestampInicioDia(filtro.getDtinteracaoinicio())+ "' " : "") +
							(filtro.getDtinteracaofim() != null ? " and rh.dtaltera <= '"+ SinedDateUtils.dateToTimestampFinalDia(filtro.getDtinteracaofim())+ "' ": "") +
							") ");
				existeFlagMarcada = Boolean.TRUE;
			}
		query.closeParentheses();
	}


}
