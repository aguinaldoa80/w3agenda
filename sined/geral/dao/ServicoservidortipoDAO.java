package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ServicoservidortipoDAO extends GenericDAO<Servicoservidortipo>{
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Servicoservidortipo> query) {
		query.leftOuterJoinFetch("servicoservidortipo.materialtipo materialtipo");
	}
	
	/**
	 * Retorna o tipo de servi�o de acordo com o tipo de material
	 * @param materialtipo
	 * @return
	 * @author Thiago Gon�alves
	 */
	public Servicoservidortipo findByMaterialtipo(Materialtipo materialtipo){
		if (materialtipo == null)
			return null;
		
		return query().where("servicoservidortipo.materialtipo = ?", materialtipo)
					  .setMaxResults(1)
					  .unique();
	}
	
	/**
	 * Retorna os tipos de servi�o que est�o relacionados � rede e associados a algum servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servicoservidortipo> findByServicorede(){
		return query().where("servicoservidortipo.servicorede = ?", Boolean.TRUE)
					  .where("exists (select servicoservidor from Servicoservidor servicoservidor " +
					  		 "         where servicoservidor.servicoservidortipo = servicoservidortipo)")
					  .list();
	}
}
