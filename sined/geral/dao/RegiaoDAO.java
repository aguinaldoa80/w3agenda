package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.sined.geral.bean.Regiao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.RegiaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class RegiaoDAO extends GenericDAO<Regiao> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Regiao> query, FiltroListagem _filtro) {
		RegiaoFiltro filtro = (RegiaoFiltro) _filtro;
		
		query
			.select("distinct regiao.cdregiao, regiao.nome, regiao.ativo")
			.leftOuterJoin("regiao.listaRegiaolocal listaRegiaolocal")
			.leftOuterJoin("regiao.listaRegiaovendedor listaRegiaovendedor")
			.leftOuterJoin("listaRegiaovendedor.colaborador colaborador")
			.where("regiao.ativo = ?", filtro.getAtivo())
			.where("colaborador = ?", filtro.getColaborador())
			.where("listaRegiaolocal.cepinicio <= ?", filtro.getCepfinal())
			.where("listaRegiaolocal.cepfinal >= ?", filtro.getCepinicio())
			.orderBy("regiao.nome")
			.ignoreJoin("listaRegiaolocal", "listaRegiaovendedor", "colaborador")
			;
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaRegiaolocal");
		save.saveOrUpdateManaged("listaRegiaovendedor");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Regiao> query) {
		query
			.leftOuterJoinFetch("regiao.listaRegiaolocal listaRegiaolocal")
			.leftOuterJoinFetch("regiao.listaRegiaovendedor listaRegiaovendedor")
			.leftOuterJoinFetch("listaRegiaovendedor.colaborador colaborador");
	}

	/**
	 * Carrega a regi�o para o filtro no faturamento
	 *
	 * @param regiao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/06/2016
	 */
	public Regiao loadForFiltro(Regiao regiao) {
		if(regiao == null || regiao.getCdregiao() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
        return query()
					.select("regiao.cdregiao, regiao.nome, regiao.ativo, " +
							"regiaolocal.cdregiaolocal, regiaolocal.cepinicio, regiaolocal.cepfinal, " +
							"regiaovendedor.cdregiaovendedor, colaborador.cdpessoa, colaborador.nome")
					.leftOuterJoin("regiao.listaRegiaolocal regiaolocal")
					.leftOuterJoin("regiao.listaRegiaovendedor regiaovendedor")
					.leftOuterJoin("regiaovendedor.colaborador colaborador")
					.where("regiao = ?", regiao)
					.unique();
	}

	public Regiao findByCep(Cep cep) {
		if(cep == null) return null;
			
		return query()
			.select("regiao.cdregiao, regiao.nome")
			.join("regiao.listaRegiaolocal listaRegiaolocal")
			.where("listaRegiaolocal.cepinicio >= ?", cep)
			.where("listaRegiaolocal.cepfinal <= ?", cep)
			.setMaxResults(1)
			.unique();
	}
	
}
