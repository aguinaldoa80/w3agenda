package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.PropriedadeRuralHistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PropriedadeRuralHistoricoDAO extends GenericDAO<PropriedadeRuralHistorico> {
	@Override
	public void updateEntradaQuery(QueryBuilder<PropriedadeRuralHistorico> query) {
		query()
			.select("propriedadeRuralHistorico.cdPropriedadeRuralHistorico, propriedadeRuralHistorico.nome, propriedadeRuralHistorico.cafir, " + 
					"propriedadeRuralHistorico.caepf, propriedadeRuralHistorico.tipoExploracao, propriedadeRuralHistorico.tipoTerceiros, empresa.cdpessoa, empresa.nomefantasia, " +
					"propriedadeRuralHistorico.participantesPercentuais")
			.leftOuterJoin("propriedadeRuralHistorico.empresa empresa")
			.unique();
	}
}
