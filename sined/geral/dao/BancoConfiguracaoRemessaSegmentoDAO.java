package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessa;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaSegmento;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class BancoConfiguracaoRemessaSegmentoDAO extends GenericDAO<BancoConfiguracaoRemessaSegmento> {

	public List<BancoConfiguracaoRemessaSegmento> findAllByBancoConfiguracaoRemessa(BancoConfiguracaoRemessa bancoConfiguracaoRemessa){
		return query()
				.joinFetch("bancoConfiguracaoRemessaSegmento.bancoConfiguracaoRemessa bancoConfiguracaoRemessa")
				.joinFetch("bancoConfiguracaoRemessaSegmento.bancoConfiguracaoSegmento bancoConfiguracaoSegmento")
				.joinFetch("bancoConfiguracaoSegmento.listaBancoConfiguracaoSegmentoCampo bancoConfiguracaoSegmentoCampo")
				.where("bancoConfiguracaoRemessa = ?", bancoConfiguracaoRemessa)
				.orderBy("bancoConfiguracaoRemessaSegmento.ordem, bancoConfiguracaoSegmentoCampo.posInicial")
				.list();
	}
	
}
