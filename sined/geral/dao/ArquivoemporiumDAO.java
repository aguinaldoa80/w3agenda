package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Arquivoemporium;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivoemporiumtipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ArquivoemporiumFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumUtil;
import br.com.linkcom.sined.util.emporium.registros.RegistroPLU;
import br.com.linkcom.sined.util.emporium.registros.RegistroPLU_KIT;
import br.com.linkcom.sined.util.emporium.registros.RegistroSKU;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ArquivoemporiumDAO extends GenericDAO<Arquivoemporium> {

	@Override
	public void updateListagemQuery(QueryBuilder<Arquivoemporium> query, FiltroListagem _filtro) {
		ArquivoemporiumFiltro filtro = (ArquivoemporiumFiltro) _filtro;
		
		query
			.select("arquivoemporium.cdarquivoemporium, arquivoemporium.dtaltera, arquivo.cdarquivo, arquivo.nome, " +
					"arquivoemporium.arquivoemporiumtipo, arquivoresultado.cdarquivo, arquivoresultado.nome, arquivoemporium.dtmovimento")
			.leftOuterJoin("arquivoemporium.arquivo arquivo")
			.leftOuterJoin("arquivoemporium.arquivoresultado arquivoresultado")
			.where("arquivoemporium.dtaltera >= ?", SinedDateUtils.dateToBeginOfDay(filtro.getDtgeracao1()))
			.where("arquivoemporium.dtaltera <= ?", SinedDateUtils.dataToEndOfDay(filtro.getDtgeracao2()))
			.where("arquivoemporium.dtmovimento >= ?", filtro.getDtmovimento1())
			.where("arquivoemporium.dtmovimento <= ?", filtro.getDtmovimento2())
			.where("arquivoemporium.arquivoemporiumtipo = ?", filtro.getArquivoemporiumtipo())
			.where("arquivoresultado is not null", filtro.getComresultados() != null && filtro.getComresultados().booleanValue())
			.orderBy("arquivoemporium.dtaltera desc");
	}

	@SuppressWarnings("unchecked")
	public List<RegistroSKU> getRegistrosExclusaoSKU(String banco_cliente) {
		final Properties properties = IntegracaoEmporiumUtil.util.getPropertiesDefaultValues(banco_cliente, "SKU");

		SinedUtil.markAsReader();
		List<RegistroSKU> listaUpdate = getJdbcTemplate().query("SELECT * FROM EMPORIUMCODIGOBARRASEXCLUSAO", 
			new RowMapper() {
				public RegistroSKU mapRow(ResultSet rs, int rowNum) throws SQLException {
					RegistroSKU registroSKU = new RegistroSKU();
					IntegracaoEmporiumUtil.util.populateDefaultValues(registroSKU, properties);
					
					registroSKU.setStatus("1");
					registroSKU.setPlu_id(rs.getInt("cdmaterial") + "");
					registroSKU.setSku_id(rs.getString("codigobarras"));
					
					return registroSKU;
				}
			}
		);
		
		return listaUpdate;
	}

	@SuppressWarnings("unchecked")
	public List<RegistroPLU> getRegistrosExclusaoPLU(String banco_cliente) {
		final Properties properties = IntegracaoEmporiumUtil.util.getPropertiesDefaultValues(banco_cliente, "PLU");

		SinedUtil.markAsReader();
		List<RegistroPLU> listaUpdate = getJdbcTemplate().query("SELECT * FROM EMPORIUMMATERIALEXCLUSAO", 
			new RowMapper() {
				public RegistroPLU mapRow(ResultSet rs, int rowNum) throws SQLException {
					RegistroPLU registroPLU = new RegistroPLU();
					IntegracaoEmporiumUtil.util.populateDefaultValues(registroPLU, properties);
					
					registroPLU.setStatus("1");
					registroPLU.setId(rs.getInt("cdmaterial") + "");
					registroPLU.setShort_description(IntegracaoEmporiumUtil.util.maxPrintString(rs.getString("nome"), 20));
					registroPLU.setLong_description(IntegracaoEmporiumUtil.util.maxPrintString(rs.getString("nome"), 50)); 
					registroPLU.setCommercial_description(IntegracaoEmporiumUtil.util.maxPrintString(rs.getString("nome"), 50)); 
					
					return registroPLU;
				}
			}
		);
		
		return listaUpdate;
	}

	@SuppressWarnings("unchecked")
	public List<RegistroPLU> getRegistrosExclusaoPLUFlag(String banco_cliente) {
		final Properties properties = IntegracaoEmporiumUtil.util.getPropertiesDefaultValues(banco_cliente, "PLU");

		SinedUtil.markAsReader();
		List<RegistroPLU> listaUpdate = getJdbcTemplate().query("SELECT M.CDMATERIAL, M.NOME " +
																"FROM EMPORIUMMATERIALEXCLUSAOFLAG E " +
																"JOIN MATERIAL M ON M.CDMATERIAL = E.CDMATERIAL", 
			new RowMapper() {
				public RegistroPLU mapRow(ResultSet rs, int rowNum) throws SQLException {
					RegistroPLU registroPLU = new RegistroPLU();
					IntegracaoEmporiumUtil.util.populateDefaultValues(registroPLU, properties);
					
					registroPLU.setStatus("1");
					registroPLU.setId(rs.getInt("cdmaterial") + "");
					registroPLU.setShort_description(IntegracaoEmporiumUtil.util.maxPrintString(rs.getString("nome"), 20));
					registroPLU.setLong_description(IntegracaoEmporiumUtil.util.maxPrintString(rs.getString("nome"), 50)); 
					registroPLU.setCommercial_description(IntegracaoEmporiumUtil.util.maxPrintString(rs.getString("nome"), 50)); 
					
					return registroPLU;
				}
			}
		);
		
		return listaUpdate;
	}

	@SuppressWarnings("unchecked")
	public List<RegistroPLU_KIT> getRegistrosExclusaoPLU_KIT(String banco_cliente) {
		final Properties properties = IntegracaoEmporiumUtil.util.getPropertiesDefaultValues(banco_cliente, "PLU_KIT");

		SinedUtil.markAsReader();
		List<RegistroPLU_KIT> listaUpdate = getJdbcTemplate().query("SELECT * FROM EMPORIUMMATERIALRELACIONADOEXCLUSAO", 
			new RowMapper() {
				public RegistroPLU_KIT mapRow(ResultSet rs, int rowNum) throws SQLException {
					RegistroPLU_KIT registroPLUKIT = new RegistroPLU_KIT();
					IntegracaoEmporiumUtil.util.populateDefaultValues(registroPLUKIT, properties);
					
					registroPLUKIT.setId_main(rs.getInt("cdmaterialprincipal") + "");
					registroPLUKIT.setId(rs.getInt("cdmaterialrelacionado") + "");
					registroPLUKIT.setQuantity(IntegracaoEmporiumUtil.util.printDouble(rs.getDouble("quantidade"), 9, 3));
					registroPLUKIT.setType_price(rs.getInt("cdmaterialprincipal") + "");
					
					return registroPLUKIT;
				}
			}
		);
		
		return listaUpdate;
	}

	public void deleteTabelasAuxiliares() {
		getJdbcTemplate().execute("DELETE FROM EMPORIUMMATERIALRELACIONADOEXCLUSAO");
		getJdbcTemplate().execute("DELETE FROM EMPORIUMMATERIALEXCLUSAOFLAG");
		getJdbcTemplate().execute("DELETE FROM EMPORIUMMATERIALEXCLUSAO");
		getJdbcTemplate().execute("DELETE FROM EMPORIUMCODIGOBARRASEXCLUSAO");
	}

	public void updateArquivoresultado(Arquivoemporium arquivoemporium) {
		getHibernateTemplate().bulkUpdate("update Arquivoemporium a set a.arquivoresultado = ? where a.cdarquivoemporium = ?", new Object[]{
			arquivoemporium.getArquivoresultado(), arquivoemporium.getCdarquivoemporium()
		});
	}

	public List<Arquivoemporium> getResultadosFaltantes() {
		return query()
					.select("arquivoemporium.cdarquivoemporium, arquivo.nome, arquivoemporium.arquivoemporiumtipo")
					.join("arquivoemporium.arquivo arquivo")
					.leftOuterJoin("arquivoemporium.arquivoresultado arquivoresultado")
					.where("arquivoresultado is null")
					.list();
	}
	
	public List<Arquivoemporium> findForWebserviceGET(Arquivoemporiumtipo arquivoemporiumtipo) {
		return query()
					.select("arquivoemporium.cdarquivoemporium, arquivo.cdarquivo, arquivo.nome, arquivoemporium.arquivoemporiumtipo")
					.join("arquivoemporium.arquivo arquivo")
					.leftOuterJoin("arquivoemporium.arquivoresultado arquivoresultado")
					.where("arquivoresultado is null")
					.where("arquivoemporium.enviadoemporium = ?", Boolean.FALSE)
					.where("arquivoemporium.arquivoemporiumtipo = ?", arquivoemporiumtipo)
					.list();
	}
	
	public List<Arquivoemporium> findForWebserviceSEND() {
		return query()
		.select("arquivoemporium.cdarquivoemporium, arquivo.nome, arquivoemporium.arquivoemporiumtipo")
		.join("arquivoemporium.arquivo arquivo")
		.leftOuterJoin("arquivoemporium.arquivoresultado arquivoresultado")
		.where("arquivoresultado is null")
		.where("arquivoemporium.enviadoemporium = ?", Boolean.TRUE)
		.list();
	}

	public List<Arquivoemporium> getExportacoesPendentes() {
		return querySined()
				.setUseReadOnly(false)
				.select("arquivoemporium.cdarquivoemporium, arquivoresultado.cdarquivo, arquivoemporium.arquivoemporiumtipo, arquivoemporium.dtmovimento")
				.join("arquivoemporium.arquivoresultado arquivoresultado")
				.openParentheses()
				.where("arquivoemporium.processado = ?", Boolean.FALSE)
				.or()
				.where("arquivoemporium.processado is null")
				.closeParentheses()
				.openParentheses()
				.where("arquivoemporium.arquivoemporiumtipo = ?", Arquivoemporiumtipo.EXPORTACAO_CLIENTES).or()
				.where("arquivoemporium.arquivoemporiumtipo = ?", Arquivoemporiumtipo.EXPORTACAO_DADOSFISCAISGERAIS).or()
				.where("arquivoemporium.arquivoemporiumtipo = ?", Arquivoemporiumtipo.EXPORTACAO_DADOSFISCAISTRIBUTACAO).or()
				.where("arquivoemporium.arquivoemporiumtipo = ?", Arquivoemporiumtipo.EXPORTACAO_MOVIMENTODETALHADO)
				.closeParentheses()
				.list();
	}

	public void updateProcessado(Arquivoemporium arquivoemporium) {
		getHibernateTemplate().bulkUpdate("update Arquivoemporium a set a.processado = ? where a.cdarquivoemporium = ?", new Object[]{
			Boolean.TRUE, arquivoemporium.getCdarquivoemporium()
		});
	}

	public void updateEnviadoemporium(Integer id) {
		getHibernateTemplate().bulkUpdate("update Arquivoemporium e set e.enviadoemporium = ? where e.id = ?", new Object[]{
				Boolean.TRUE, id
		});
	}

	public Arquivoemporium loadForEmporium(Arquivoemporium arquivoemporium) {
		return querySined()
					.setUseReadOnly(false)
					.leftOuterJoinFetch("arquivoemporium.arquivo arquivo")
					.leftOuterJoinFetch("arquivoemporium.arquivoresultado arquivoresultado")
					.entity(arquivoemporium)
					.unique();
	}
		
}
