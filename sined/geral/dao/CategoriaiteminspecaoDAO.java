package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Categoriaiteminspecao;
import br.com.linkcom.sined.geral.bean.Veiculomodelo;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CategoriaiteminspecaoDAO extends GenericDAO<Categoriaiteminspecao> {

	/**
	 * <b>M�todo respons�vel em carregar uma lista de categoria ItemInspe��o a partir de um<br>
	 * modelo</b>
	 * @author Biharck
	 * @param modelo
	 * @return uma lista de categoria item inspe��o
	 */
	public List<Categoriaiteminspecao> loadCategoriaItemInspecao(Veiculomodelo modelo, Boolean visual){
		if(modelo == null){
			throw new SinedException("A propriedade nao pode ser nula");
		}
		
		if(visual != null && !visual)
			visual = null; //Para remover o item da query
		
		return query()
			.select("inspecaoitem.nome, inspecaoitem.cdinspecaoitem, inspecaoitem.visual, inspecaoitemtipo.nome")
			.join("categoriaiteminspecao.inspecaoitem inspecaoitem")
			.join("inspecaoitem.inspecaoitemtipo inspecaoitemtipo")
			.where("categoriaiteminspecao.categoriaveiculo=?", modelo.getCategoriaveiculo())
			.where("inspecaoitem.visual=?",visual)
			.where("inspecaoitem.ativo=?",Boolean.TRUE)
			.orderBy("inspecaoitemtipo.nome, inspecaoitem.nome")
			.list()
			;
	}
	
	/**
	 * <b>M�todo respons�vel em carregar uma lista de categoria ItemInspe��o a partir de um<br>
	 * modelo</b>
	 * @author Biharck
	 * @param modelo
	 * @return uma lista de categoria item inspe��o
	 */
	public List<Categoriaiteminspecao> loadCategoriaItemInspecao(Veiculomodelo modelo, String orderBy, Boolean asc){
		if(modelo == null){
			throw new SinedException("A propriedade nao pode ser nula");
		}
		String ordenacao = "";
		if(orderBy!=null && !orderBy.equals("")){
			ordenacao = orderBy + (asc?" asc":" desc");
		}
		return query()
			.select("categoriaiteminspecao.venckm, categoriaiteminspecao.vencperiodo, " +
					"inspecaoitem.nome, inspecaoitem.cdinspecaoitem, inspecaoitem.visual, inspecaoitemtipo.nome")
			.join("categoriaiteminspecao.inspecaoitem inspecaoitem")
			.join("inspecaoitem.inspecaoitemtipo inspecaoitemtipo")
			.where("categoriaiteminspecao.categoriaveiculo=?", modelo.getCategoriaveiculo())
			.orderBy(ordenacao)
			.list()
			;
	}
	
}
