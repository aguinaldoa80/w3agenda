package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.PropriedadeRural;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.LcdprArquivoFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.PropriedadeRuralFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PropriedadeRuralDAO extends GenericDAO<PropriedadeRural> {
	@Override
	public void updateEntradaQuery(QueryBuilder<PropriedadeRural> query) {
		query
			.select("propriedadeRural.cdPropriedadeRural, propriedadeRural.nome, propriedadeRural.usarSequencial, " +
					"propriedadeRural.cafir, propriedadeRural.caepf, propriedadeRural.tipoExploracao, propriedadeRural.tipoTerceiros, " +
					"propriedadeRural.ativo, propriedadeRural.codigoImovel, propriedadeRural.principal, " +
					"empresa.cdpessoa, empresa.razaosocial, empresa.nomefantasia, " +
					"listaPropriedadeRuralTelefone.cdPropriedadeRuralTelefone, listaPropriedadeRuralTelefone.telefone, " +
					"telefoneTipo.cdtelefonetipo, telefoneTipo.nome, " +
					"listaParticipante.cdParticipantePropriedadeRural, listaParticipante.percentual, " +
					"participanteColaborador.cdpessoa, participanteColaborador.nome, " +
					"pais.cdpais, pais.nome, listaEndereco.cdendereco, listaEndereco.logradouro, listaEndereco.bairro, listaEndereco.cep, " +
					"listaEndereco.caixapostal, listaEndereco.numero, listaEndereco.complemento, " +
					"enderecotipo.cdenderecotipo, enderecotipo.nome, " +
					"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.nome, uf.sigla, " +
					"listaHistorico.cdPropriedadeRuralHistorico, listaHistorico.observacao, listaHistorico.acao, listaHistorico.dtaltera, " +
					"usuario.cdpessoa, usuario.nome")
			.leftOuterJoin("propriedadeRural.empresa empresa")
			.leftOuterJoin("propriedadeRural.listaPropriedadeRuralTelefone listaPropriedadeRuralTelefone")
			.leftOuterJoin("listaPropriedadeRuralTelefone.telefoneTipo telefoneTipo")
			.leftOuterJoin("propriedadeRural.listaParticipantePropriedadeRural listaParticipante")
			.leftOuterJoin("listaParticipante.colaborador participanteColaborador")
			.leftOuterJoin("propriedadeRural.listaPropriedadeRuralHistorico listaHistorico")
			.leftOuterJoin("listaHistorico.usuarioaltera usuario")
			.leftOuterJoin("propriedadeRural.listaEndereco listaEndereco")
			.leftOuterJoin("listaEndereco.municipio municipio")
			.leftOuterJoin("listaEndereco.enderecotipo enderecotipo")
			.leftOuterJoin("listaEndereco.pais pais")
			.leftOuterJoin("municipio.uf uf")
			.orderBy("listaHistorico.dtaltera desc");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<PropriedadeRural> query, FiltroListagem _filtro) {
		PropriedadeRuralFiltro filtro = (PropriedadeRuralFiltro) _filtro;
		
		query
			.select("propriedadeRural.cdPropriedadeRural, propriedadeRural.nome, propriedadeRural.codigoImovel, " +
					"propriedadeRural.cafir, propriedadeRural.caepf, " +
					"empresa.cdpessoa, empresa.nomefantasia, empresa.razaosocial")
			.leftOuterJoin("propriedadeRural.empresa empresa")
			.whereLikeIgnoreAll("propriedadeRural.nome", filtro.getNome())
			.where("empresa = ?", filtro.getEmpresa())
			.where("propriedadeRural.cafir = ?", filtro.getCafir())
			.where("propriedadeRural.caepf = ?", filtro.getCaepf())
			.where("propriedadeRural.codigoImovel = ?", filtro.getCodigoImovel())
			.where("propriedadeRural.tipoExploracao = ?", filtro.getTipoExploracao())
			.where("propriedadeRural.tipoTerceiros = ?", filtro.getTipoTerceiros())
			.where("propriedadeRural.ativo = ?", filtro.getAtivo());
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaEndereco");
		save.saveOrUpdateManaged("listaPropriedadeRuralTelefone");
		save.saveOrUpdateManaged("listaParticipantePropriedadeRural");
	}
	
	public Integer getCodigoImovelUltimaPropriedadeRural(Integer cdpessoa) {
		return newQueryBuilderSined(Integer.class)
			.from(PropriedadeRural.class)
			.setUseTranslator(false)
			.select("max(propriedadeRural.codigoImovel)")
			.join("propriedadeRural.empresa empresa")
			.where("empresa.cdpessoa = ?", cdpessoa)
			.where("propriedadeRural.ativo = true")
			.unique();
	}
	
	public boolean existeCodigoImovelCadastrado(Integer codigoImovel, Integer cdPropriedadeRural, Integer cdpessoa) {
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(PropriedadeRural.class)
			.join("propriedadeRural.empresa empresa")
			.where("propriedadeRural.codigoImovel = ?", codigoImovel)
			.where("empresa.cdpessoa = ?", cdpessoa)
			.where("propriedadeRural.ativo = true")
			.where("propriedadeRural.cdPropriedadeRural not in (?)", cdPropriedadeRural, cdPropriedadeRural != null)
			.unique() > 0;
	}

	public List<PropriedadeRural> loadForLcdpr(LcdprArquivoFiltro filtro) {
		return query()
				.select("propriedadeRural.cdPropriedadeRural, propriedadeRural.codigoImovel, propriedadeRural.nome, propriedadeRural.cafir, " +
						"propriedadeRural.caepf, propriedadeRural.principal, listaEndereco.cdendereco, listaEndereco.logradouro, listaEndereco.numero, " +
						"listaEndereco.bairro, listaEndereco.cep, listaEndereco.complemento, listaEndereco.bairro, enderecotipo.cdenderecotipo, " +
						"municipio.cdmunicipio, municipio.cdibge, uf.cduf, uf.sigla, colaborador.cdpessoa, empresa.inscricaoestadual, empresa.cdpessoa, " +
						"propriedadeRural.tipoExploracao, propriedadeRural.tipoTerceiros, listaParticipantePropriedadeRural.cdParticipantePropriedadeRural, " +
						"listaParticipantePropriedadeRural.percentual, colaboradorParticipante.cdpessoa, colaboradorParticipante.nome, " +
						"colaboradorParticipante.cpf")
				.join("propriedadeRural.listaEndereco listaEndereco")
				.join("listaEndereco.enderecotipo enderecotipo")
				.leftOuterJoin("listaEndereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.join("propriedadeRural.empresa empresa")
				.join("empresa.listaEmpresaproprietario listaEmpresaproprietario")
				.join("listaEmpresaproprietario.colaborador colaborador")
				.join("propriedadeRural.listaParticipantePropriedadeRural listaParticipantePropriedadeRural")
				.join("listaParticipantePropriedadeRural.colaborador colaboradorParticipante")
				.where("empresa.cdpessoa in (select e.cdpessoa FROM Empresaproprietario ep " +
												"JOIN ep.empresa e " +
												"WHERE ep.colaborador.cdpessoa = " + filtro.getColaborador().getCdpessoa() + ")")
				.orderBy("empresa.cdpessoa, propriedadeRural.codigoImovel")
				.list();
	}
	
	//verifica se a empresa j� tem uma propriedade rural marcada como principal
	public Boolean havePrincipal(Integer cdpessoa) {
		return newQueryBuilderSined(Long.class)
				.select("count(*)")
				.from(PropriedadeRural.class)
				.join("propriedadeRural.empresa empresa")
				.where("propriedadeRural.principal = true")
				.where("empresa.cdpessoa = ?", cdpessoa)
				.unique() > 0;
	}
	
	public PropriedadeRural loadPrincipal(Integer cdpessoa){
		return query()
				.select("propriedadeRural.cdPropriedadeRural, propriedadeRural.codigoImovel, propriedadeRural.nome, propriedadeRural.principal")
				.join("propriedadeRural.empresa empresa")
				.where("propriedadeRural.principal = true")
				.where("empresa.cdpessoa = ?", cdpessoa)
				.unique();		
	}

	public void updatePrincipal(Integer cdPropriedadeRural) {
		getHibernateTemplate().bulkUpdate("update PropriedadeRural p set p.principal = false where cdPropriedadeRural = " + cdPropriedadeRural); 
	}

}
