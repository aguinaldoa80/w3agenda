package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Sexo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("sexo.nome")
public class SexoDAO extends GenericDAO<Sexo> {
	
	/* singleton */
	private static SexoDAO instance;
	public static SexoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(SexoDAO.class);
		}
		return instance;
	}
		
}
