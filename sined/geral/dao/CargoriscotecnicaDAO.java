package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Cargoriscotecnica;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CargoriscotecnicaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CargoriscotecnicaDAO extends GenericDAO<Cargoriscotecnica> {

	@Override
	public void updateListagemQuery(QueryBuilder<Cargoriscotecnica> query,	FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inválidos");
		}
		CargoriscotecnicaFiltro filtro = (CargoriscotecnicaFiltro) _filtro;
		query
		.select("cargoriscotecnica.cdcargoriscotecnica, cargoriscotecnica.nome")
		.whereLikeIgnoreAll("cargoriscotecnica.nome", filtro.getNome());
	}
	
	
	/* singleton */
	private static CargoriscotecnicaDAO instance;
	public static CargoriscotecnicaDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(CargoriscotecnicaDAO.class);
		}
		return instance;
	}
}