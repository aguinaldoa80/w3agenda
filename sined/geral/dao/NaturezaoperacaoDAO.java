package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.NaturezaoperacaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("naturezaoperacao.descricao")
public class NaturezaoperacaoDAO extends GenericDAO<Naturezaoperacao>{

	/**
	 * M�todo que busca a natureza da opera��o pelo c�digo
	 *
	 * @param codigonaturezaoperacao
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Naturezaoperacao> findByNaturezaoperacao(String codigonaturezaoperacao) {
		if(codigonaturezaoperacao == null || "".equals(codigonaturezaoperacao))
			throw new SinedException("C�digo da natureza da opera��o n�o pode ser nulo. ");
		
		return query()
				.select("naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.codigonfse")
				.where("naturezaoperacao.codigonfse = ?", codigonaturezaoperacao)
				.where("naturezaoperacao.ativo = true")
				.list();
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Naturezaoperacao> query, FiltroListagem _filtro) {
		
		NaturezaoperacaoFiltro filtro = (NaturezaoperacaoFiltro) _filtro;
		
		query
			.select("distinct naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome, naturezaoperacao.descricao, naturezaoperacao.codigonfse," +
					"naturezaoperacao.padrao, naturezaoperacao.ativo, operacaocontabilavista.nome, operacaocontabilaprazo.nome," +
					"notaTipo.nome, operacaocontabilpagamento.nome")
			.leftOuterJoin("naturezaoperacao.operacaocontabilavista operacaocontabilavista")
			.leftOuterJoin("naturezaoperacao.operacaocontabilaprazo operacaocontabilaprazo")
			.leftOuterJoin("naturezaoperacao.operacaocontabilpagamento operacaocontabilpagamento")
			.leftOuterJoin("naturezaoperacao.notaTipo notaTipo")
			.leftOuterJoin("naturezaoperacao.listaNaturezaoperacaocfop listaNaturezaoperacaocfop")
			.leftOuterJoin("listaNaturezaoperacaocfop.cfop cfop")
			.whereLikeIgnoreAll("naturezaoperacao.nome", filtro.getNome())
			.whereLikeIgnoreAll("naturezaoperacao.descricao", filtro.getDescricao())
			.whereLikeIgnoreAll("naturezaoperacao.codigonfse", filtro.getCodigonfse())
			.where("operacaocontabilavista=?", filtro.getOperacaocontabilavista())
			.where("operacaocontabilaprazo=?", filtro.getOperacaocontabilaprazo())
			.where("operacaocontabilpagamento=?", filtro.getOperacaocontabilpagamento())
			.where("notaTipo=?", filtro.getNotaTipo())
			.where("coalesce(naturezaoperacao.ativo,false)=?", filtro.getAtivo())
			.where("cfop=?", filtro.getCfop())
			.orderBy("naturezaoperacao.nome")
			.ignoreJoin("listaNaturezaoperacaocfop", "cfop");
		
		super.updateListagemQuery(query, filtro);
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Naturezaoperacao> query) {
		query.leftOuterJoinFetch("naturezaoperacao.notaTipo notaTipo");
		super.updateEntradaQuery(query);
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaNaturezaoperacaocfop");
		save.saveOrUpdateManaged("listaNaturezaoperacaocst");
		super.updateSaveOrUpdate(save);
	}
	
	/**
	 * 
	 * @param cdnaturezaoperacaoExcecao
	 * @author Thiago Clemente
	 * @param notaTipo 
	 * 
	 */
	public Long getTotalNaturezaoperacaoPadrao(Integer cdnaturezaoperacaoExcecao, NotaTipo notaTipo){
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.where("naturezaoperacao.cdnaturezaoperacao<>?", cdnaturezaoperacaoExcecao)
				.where("naturezaoperacao.padrao=?", Boolean.TRUE)
				.where("naturezaoperacao.notaTipo = ?", notaTipo)
				.unique();
	}
	
	/**
	 * 
	 * @author Thiago Clemente
	 * 
	 */
	public Naturezaoperacao loadPadrao(NotaTipo notaTipo){
		return query() 
				.where("naturezaoperacao.padrao=?", Boolean.TRUE)
				.where("naturezaoperacao.notaTipo = ?", notaTipo)
				.setMaxResults(1)
				.unique();
	}
	
	/**
	 * 
	 * @param naturezaoperacao
	 * @param notaTipo
	 * @param ativo
	 * @author Thiago Clemente
	 * 
	 */
	public List<Naturezaoperacao> find(Naturezaoperacao naturezaoperacao, NotaTipo notaTipo, Boolean ativo){
		return query()
				.leftOuterJoin("naturezaoperacao.notaTipo notaTipo")
				.where("notaTipo=?", notaTipo)
				.openParentheses()
					.where("naturezaoperacao=?", naturezaoperacao)
					.or()
					.where("coalesce(naturezaoperacao.ativo,false)=?", ativo)
				.closeParentheses()
				.orderBy("naturezaoperacao.nome")
				.list();
	}
	
	public List<Naturezaoperacao> findForAutocompleteNota(String q, NotaTipo notaTipo){
		return query()
				.select("naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome")
				.where("naturezaoperacao.ativo=?", Boolean.TRUE)
				.where("naturezaoperacao.notaTipo=?", notaTipo)
				.whereLikeIgnoreAll("naturezaoperacao.nome", q)
				.orderBy("naturezaoperacao.nome")
				.list();
	}
	
	public List<Naturezaoperacao> findNomes(){
		return query()
				.select("naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome")
				.orderBy("naturezaoperacao.nome")
				.list();
	}

	/**
	* M�todo que carrega a natureza de opera��o com o CST para valida��o das informa��es na nota
	*
	* @param naturezaoperacao
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public Naturezaoperacao loadForValidaLancamentoImpostoNota(Naturezaoperacao naturezaoperacao) {
		if(naturezaoperacao == null || naturezaoperacao.getCdnaturezaoperacao() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("naturezaoperacao.cdnaturezaoperacao, listaNaturezaoperacaocst.cdnaturezaoperacaocst, " +
						"listaNaturezaoperacaocst.tipocobrancapis, listaNaturezaoperacaocst.tipocobrancaicms, " +
						"listaNaturezaoperacaocst.tipocobrancacofins, listaNaturezaoperacaocst.tipocobrancaipi ")
				.join("naturezaoperacao.listaNaturezaoperacaocst listaNaturezaoperacaocst")
				.where("naturezaoperacao = ?", naturezaoperacao)
				.unique();
	}

	/**
	* M�todo que busca natureza de opera��o para a integra��o SAGE
	*
	* @param whereIn
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public List<Naturezaoperacao> findForSagefiscal(String whereIn) {
		return query()
			.select("naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome, naturezaoperacao.descricao, naturezaoperacao.dtaltera")
			.whereIn("naturezaoperacao.cdnaturezaoperacao", whereIn)
			.list();
	}
	
	public List<Naturezaoperacao> findByCfops(String whereinCfops) {
		return query()
				.select("naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome, naturezaoperacao.codigonfse")
				.leftOuterJoin("naturezaoperacao.listaNaturezaoperacaocfop naturezaoperacaocfop")
				.leftOuterJoin("naturezaoperacaocfop.cfop cfop")
				.where("naturezaoperacao.notaTipo = ?", NotaTipo.NOTA_FISCAL_PRODUTO)
				.whereIn("cfop.codigo", whereinCfops)
				.where("naturezaoperacao.ativo = ?", Boolean.TRUE)
				.list();
	}

	public Naturezaoperacao loadForSped(Naturezaoperacao naturezaoperacao) {
		return query()
			.select("naturezaoperacao.cdnaturezaoperacao, listaCredito.sped, listaDebito.sped, " +
					"contacontabildebito.cdcontacontabil, contacontabilcredito.cdcontacontabil, " +
					"vcontacontabilDebito.identificador, vcontacontabilCredito.identificador ")
			.leftOuterJoin("naturezaoperacao.operacaocontabilpagamento operacaocontabilpagamento")
			.leftOuterJoin("operacaocontabilpagamento.listaDebito listaDebito")
			.leftOuterJoin("listaDebito.contaContabil contacontabildebito")
			.leftOuterJoin("operacaocontabilpagamento.listaCredito listaCredito")
			.leftOuterJoin("listaCredito.contaContabil contacontabilcredito")
			.leftOuterJoin("contacontabildebito.vcontacontabil vcontacontabilDebito")
			.leftOuterJoin("contacontabilcredito.vcontacontabil vcontacontabilCredito")
			.where("naturezaoperacao = ?", naturezaoperacao)
			.unique();
	}

	public List<Naturezaoperacao> findByAtivoEntradaFiscalLikeNome(String whereIn) {
		return query()
				.select("naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome")
				.where("naturezaoperacao.ativo = ?", Boolean.TRUE)
				.where("naturezaoperacao.notaTipo = ?", NotaTipo.ENTRADA_FISCAL)
				.whereLikeIgnoreAll("naturezaoperacao.nome", whereIn)
				.orderBy("naturezaoperacao.nome")
				.list();
	}

	public Naturezaoperacao findNaturezaoperacaoConferenciaDeDados(String descricao) {
		return query()
				.select("naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome, naturezaoperacao.descricao")
				.whereLikeIgnoreAll("nome", descricao).or()
				.whereLikeIgnoreAll("descricao", descricao)
				.setMaxResults(1)
				.unique();
	}
	
	public List<Naturezaoperacao> findByNotaTipoNome (String nome){
		return query()
				.select("naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome")
				.leftOuterJoin("naturezaoperacao.notaTipo notatipo")
				.whereLikeIgnoreAll("notatipo.nome", nome)
				.orderBy("notatipo.nome")
				.list();
		
	}
	
	public Naturezaoperacao findByNome (String nome){
		return query()
				.select("naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome")
				.leftOuterJoin("naturezaoperacao.notaTipo notatipo")
				.whereEqualIgnoreAll("naturezaoperacao.nome", nome)
				.or()
				.whereEqualIgnoreAll("naturezaoperacao.descricao", nome)
				.setMaxResults(1)
				.unique();
		
	}
}