package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Configuracaoecom;
import br.com.linkcom.sined.geral.bean.Configuracaoecomerro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ConfiguracaoecomerroDAO extends GenericDAO<Configuracaoecomerro>{

	public Configuracaoecomerro getErroByConfiguracaoecom(Configuracaoecom configuracaoecom, String mensagem) {
		QueryBuilder<Configuracaoecomerro> query = query().where("configuracaoecomerro.mensagem = ?", mensagem);
		
		if(configuracaoecom != null) query.where("configuracaoecomerro.configuracaoecom = ?", configuracaoecom);
		else query.where("configuracaoecomerro.configuracaoecom is null");
		
		List<Configuracaoecomerro> lista = query.list();
		if(lista != null && lista.size() > 0) return lista.get(0);
		else return null;
	}

	public void addContadorErro(Configuracaoecomerro configuracaoecomerro) {
		getHibernateTemplate().bulkUpdate("update Configuracaoecomerro c set c.contador = coalesce(c.contador, 0) + 1 where c.id = ?", configuracaoecomerro.getCdconfiguracaoecomerro());
	}

}
