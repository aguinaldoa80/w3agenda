package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Localarmazenagemempresa;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class LocalarmazenagemempresaDAO extends GenericDAO<Localarmazenagemempresa> {

	/**
	 * Carrega a lista de localarmazenagemempresa a partir de um localarmzenagem.
	 * 
	 * @param form
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Localarmazenagemempresa> findByLocalarmazenagem(Localarmazenagem form) {
		if (form == null || form.getCdlocalarmazenagem() == null) {
			throw new SinedException("Localarmazenagem n�o pode ser nulo.");
		}
		return query()
					.select("localarmazenagemempresa.cdlocalarmazenagemempresa, empresa.cdpessoa, empresa.nomefantasia, empresa.ativo")
					.join("localarmazenagemempresa.empresa empresa")
					.where("localarmazenagemempresa.localarmazenagem = ?", form)
					.list();
	}
	
	/**
	* M�todo que busca as empresas vinculadas aos locais de armazenagem
	*
	* @param whereIn
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<Localarmazenagemempresa> findForAndroid(String whereIn) {
		return query()
			.select("localarmazenagemempresa.cdlocalarmazenagemempresa, localarmazenagem.cdlocalarmazenagem, empresa.cdpessoa ")
			.join("localarmazenagemempresa.empresa empresa")
			.join("localarmazenagemempresa.localarmazenagem localarmazenagem")
			.whereIn("localarmazenagemempresa.cdlocalarmazenagemempresa", whereIn)
			.list();
	}
	
	/**
	 * M�todo que carrega o localarmazenagem e empresa a partir de um cnpj para ajuste de estoque realizado pelo WMS
	 * 
	 * @param cnpj
	 * @return
	 * @author Rafael Salvio
	 */
	public Localarmazenagemempresa findForAjusteEstoqueWMS(Cnpj cnpj){
		if(cnpj == null || cnpj.getValue() == null || cnpj.getValue().trim().isEmpty()){
			throw new SinedException("Cnpj n�o � v�lido ou � nulo.");
		}
		
		return query()
				.select("localarmazenagemempresa.cdlocalarmazenagemempresa, localarmazenagem.cdlocalarmazenagem, empresa.cdpessoa")
				.join("localarmazenagemempresa.empresa empresa")
				.join("localarmazenagemempresa.localarmazenagem localarmazenagem")
				.where("empresa.cnpj = ?", cnpj)
				.where("localarmazenagem.ativo is true")
				.where("localarmazenagem.principal is true")
				.unique();
	}
}
