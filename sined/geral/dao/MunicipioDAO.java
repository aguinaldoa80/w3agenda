package br.com.linkcom.sined.geral.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.MunicipioFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("municipio.nome")
public class MunicipioDAO extends GenericDAO<Municipio> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Municipio> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		MunicipioFiltro filtro = (MunicipioFiltro) _filtro;
		query
			.select("distinct municipio.cdmunicipio, municipio.nome, uf.sigla")
			.leftOuterJoin("municipio.uf uf")
			.whereLikeIgnoreAll("municipio.nome", filtro.getNome())
			.where("uf = ?", filtro.getUf())
			.orderBy("municipio.nome,uf.sigla");
	}
	
	/**
	 * M�todo para obter todos os municipios de um uf.
	 * 
	 * @param uf
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Municipio> findByUf(Uf uf){
		if(uf == null || uf.getCduf() == null){
			throw new SinedException("O par�metro uf e cduf n�o podem ser null.");
		}
		return 
			query()
			.select("municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla")
			.join("municipio.uf uf")
			.where("uf = ?", uf)
			.orderBy("municipio.nome")
			.list();
	}
	
	public List<Municipio> findByUfProcJuridico(Uf uf){
		if(uf == null || uf.getCduf() == null){
			return 
			query()
			.select("municipio.cdmunicipio,municipio.nome")
			.where("municipio.uf is null")
			.orderBy("municipio.nome")
			.list();
		}else{
			return
			query()
			.select("municipio.cdmunicipio,municipio.nome")
			.join("municipio.uf uf")
			.where("uf = ?",uf)
			.orderBy("municipio.nome")
			.list();
		}
	}
	
	/* singleton */
	private static MunicipioDAO instance;
	public static MunicipioDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(MunicipioDAO.class);
		}
		return instance;
	}

	/**
	 * Carrega o municipio.
	 * 
	 * @param municipio
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Municipio carregaMunicipio(Municipio municipio) {
		if (municipio == null || municipio.getCdmunicipio() == null) {
			throw new SinedException("Municipio n�o pode ser nulo.");
		}
		return query()
					.select("municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla")
					.join("municipio.uf uf")
					.where("municipio = ?",municipio)
					.unique();
	}

	/**
	 * Faz busca para o autocomplete da tela de nota fiscal de produto.
	 *
	 * @param s
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Municipio> loadListaWithUfAutocomplete(String s, Integer cduf) {
		return query()
					.select("municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla")
					.join("municipio.uf uf")
					.whereLikeIgnoreAll("municipio.nome", s)
					.where("uf.cduf = ?", cduf)
					.orderBy("municipio.nome, uf.sigla")
					.list();
	}
	
	public List<Municipio> loadListaWithUfAutocomplete(String s) {
		return this.loadListaWithUfAutocomplete(s, null);
	}
	
	/**
	 * 
	 *<p>M�todo respons�vel em carregar inform��es do municipo para o webService de cep
	 *
	 * @param nomeMunicipio {@link Municipio}.getNome()
	 * @param siglaUf {@link Uf}.getSigla()
	 * @return {@link Municipio}
	 * 
	 * @author Taidson
	 * @since 25/04/2010
	 */
	public Municipio getByNomeAndUf(String nomeMunicipio,String siglaUf) {
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		
		return 
			query()
				.select("municipio.cdmunicipio,uf.cduf")
				.leftOuterJoin("municipio.uf uf")
				.where("upper(" + funcaoTiraacento + "(municipio.nome)) = ?", Util.strings.tiraAcento(nomeMunicipio).toUpperCase())
				.whereLikeIgnoreAll("upper(uf.sigla)", siglaUf.toUpperCase())
				.unique();
	}
	
	/**
	 * 
	 *<p>M�todo respons�vel em carregar dados do Uf
	 *
	 * @param uf {@link Uf}
	 * @return {@link List} Municip�o
	 * 
	 * @author Taidson
	 * @since 25/04/2010
	 */
	public List<Municipio> find(Uf uf) {
		if (uf == null) 
			return new ArrayList<Municipio>();
		else
			return queryWithOrderBy()
				.join("municipio.uf uf")
				.where("uf = ?", uf)				
				.list();
	}

	public Municipio findByNomeSigla(String nome, String siglauf) {
		if(nome == null || nome.equals("")){
			throw new SinedException("Munic�pio n�o pode ser nulo.");
		}
		if(siglauf == null || siglauf.equals("")){
			throw new SinedException("Sigla da UF n�o pode ser nulo.");
		}
		
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		
		List<Municipio> list = query()
									.select("municipio.cdmunicipio, municipio.nome")
									.join("municipio.uf uf")
									.where("upper(" + funcaoTiraacento + "(municipio.nome)) like ?", Util.strings.tiraAcento(nome).toUpperCase())
									.where("uf.sigla like ?", siglauf)
									.list();
		
		if(list != null && list.size() > 0) return list.get(0);
		else return null;
	}

	/**
	 * Busca o munic�pio de acordo com seu c�digo IBGE.
	 * 
	 *
	 * @param cdibge
	 * @return
	 * @since Jul 5, 2011
	 * @author Rodrigo Freitas
	 */
	public Municipio findByCdibge(String cdibge) {
		if(cdibge == null || cdibge.equals("")){
			throw new SinedException("C�digo n�o pode ser nulo.");
		}
		List<Municipio> lista = query()
									.select("municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla")
									.join("municipio.uf uf")
									.where("municipio.cdibge like ?", cdibge)
									.list();
		
		// FEITO ASSIM PORQUE N�O EXISTE UNIQUE NO CAMPO "cdibge"
		if(lista != null && lista.size() > 0){
			return lista.get(0);
		} else {
			return null;
		}
	}

	public List<Municipio> findForPVOffline() {
		return query()
		.orderBy("UPPER(TIRAACENTO(municipio.nome)) asc")
		.list();
	}
	
	public List<Municipio> findForAndroid(String whereIn) {
		return query()
		.leftOuterJoin("municipio.uf uf")
		.whereIn("municipio.cdmunicipio", whereIn)
		.orderBy("UPPER(TIRAACENTO(municipio.nome)) asc")
		.list();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Municipio> query) {
		query
			.select("municipio.cdmunicipio, municipio.nome, municipio.cdusuarioaltera, municipio.codigotom, municipio.codigogoiania, " +
					"municipio.dtaltera, municipio.cdibge, municipio.cdsiafi, uf.cduf, uf.sigla")
			.leftOuterJoin("municipio.uf uf");	
	}
	
	public Municipio getByMunicipioNome(String nome) {
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		return 
			querySined()
				.setUseReadOnly(true)
				.select("municipio.cdmunicipio,municipio.nome")
				.where("upper(" + funcaoTiraacento + "(municipio.nome)) = ?", Util.strings.tiraAcento(nome).toUpperCase())
				.unique();
	}
}
