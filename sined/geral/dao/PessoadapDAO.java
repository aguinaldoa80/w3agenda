package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Pessoadap;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PessoadapDAO extends GenericDAO<Pessoadap> {
	
	
	/**
	 * Busca o dap de uma pessoa
	 *
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/11/2016
	 */
	public List<Pessoadap> findByPessoa(Pessoa pessoa){
		return query()
					.select("pessoadap.cdpessoadap, pessoadap.identificador, pessoadap.dtvencimento")
					.where("pessoadap.pessoa = ?", pessoa)
					.orderBy("pessoadap.dtvencimento desc")
					.list();
	}

	
}
