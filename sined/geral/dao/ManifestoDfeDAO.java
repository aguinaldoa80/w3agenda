package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.LoteConsultaDfeItemStatusEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.ManifestoDfeSituacaoEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.ManifestoDfeFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

public class ManifestoDfeDAO extends GenericDAO<ManifestoDfe> {

	@Override
	public void updateListagemQuery(QueryBuilder<ManifestoDfe> query, FiltroListagem _filtro) {
		ManifestoDfeFiltro manifestoDfeFiltro = (ManifestoDfeFiltro) _filtro;
		
		query
			.select("manifestoDfe.chaveAcesso, manifestoDfe.cpf, manifestoDfe.cnpj, manifestoDfe.dhEmissao, manifestoDfe.dhRecbto, " +
					"manifestoDfe.statusEnum, manifestoDfe.manifestoDfeSituacaoEnum, manifestoDfe.razaoSocial, manifestoDfe.valorNf, " +
					"loteConsultaDfeItem.cdLoteConsultaDfeItem, loteConsultaDfe.cdLoteConsultaDfe, empresa.cdpessoa, empresa.nome, empresa.nomefantasia")
			.leftOuterJoin("manifestoDfe.loteConsultaDfeItem loteConsultaDfeItem")
		    .leftOuterJoin("loteConsultaDfeItem.loteConsultaDfe loteConsultaDfe")
		    .leftOuterJoin("loteConsultaDfe.empresa empresa")
			.where("empresa = ?", manifestoDfeFiltro.getEmpresa())
			.where("manifestoDfe.chaveAcesso = ?", manifestoDfeFiltro.getChaveAcesso())
			.where("manifestoDfe.cpf = ?", manifestoDfeFiltro.getCpf())
			.where("manifestoDfe.cnpj = ?", manifestoDfeFiltro.getCnpj())
			.where("manifestoDfe.dhEmissao >= ?", manifestoDfeFiltro.getDhEmissaoInicio())
			.where("manifestoDfe.dhEmissao <= ?", SinedDateUtils.dataToEndOfDay(manifestoDfeFiltro.getDhEmissaoFim()))
			.where("manifestoDfe.dhRecbto >= ?", manifestoDfeFiltro.getDhRecbtoInicio())
			.where("manifestoDfe.dhRecbto <= ?", SinedDateUtils.dataToEndOfDay(manifestoDfeFiltro.getDhRecbtoFim()))
			.where("manifestoDfe.chaveAcesso <= ?", manifestoDfeFiltro.getChaveAcesso())
			.whereIn("manifestoDfe.statusEnum", manifestoDfeFiltro.getListaStatusEnum() != null ? 
					LoteConsultaDfeItemStatusEnum.listAndConcatenate(manifestoDfeFiltro.getListaStatusEnum()) : null)
			.whereIn("manifestoDfe.manifestoDfeSituacaoEnum", manifestoDfeFiltro.getListaManifestoDfeSituacaoEnum() != null ? 
					ManifestoDfeSituacaoEnum.listAndConcatenate(manifestoDfeFiltro.getListaManifestoDfeSituacaoEnum()) : null)
			.whereLikeIgnoreAll("manifestoDfe.razaoSocial", manifestoDfeFiltro.getNome());
		
		if (StringUtils.isNotEmpty(manifestoDfeFiltro.getNumeroNf())) {
			query.where("substring(manifestoDfe.chaveAcesso, 26, 9) = ?", manifestoDfeFiltro.getNumeroNf());
		}
		
		if(manifestoDfeFiltro.getTipopessoa() != null && manifestoDfeFiltro.getCpf() == null && manifestoDfeFiltro.getCnpj() == null){
			if(Tipopessoa.PESSOA_FISICA.equals(manifestoDfeFiltro.getTipopessoa())){
				query.where("manifestoDfe.cpf is not null");
			}else if(Tipopessoa.PESSOA_JURIDICA.equals(manifestoDfeFiltro.getTipopessoa())){
				query.where("manifestoDfe.cnpj is not null");
			} 
		}
		
		if (BooleanUtils.isTrue(manifestoDfeFiltro.getOutros())) {
			query.where("manifestoDfe.statusEnum = ?", LoteConsultaDfeItemStatusEnum.DFE_AUTORIZADA)
				.where("exists(select entregadocumento.cdentregadocumento " +
						"from Entregadocumento entregadocumento " +
						"where entregadocumento.chaveacesso = manifestoDfe.chaveAcesso and " +
						"entregadocumento.entregadocumentosituacao != 1)");
		}
		
		if (BooleanUtils.isFalse(manifestoDfeFiltro.getOutros())) {
			query.where("manifestoDfe.statusEnum = ?", LoteConsultaDfeItemStatusEnum.DFE_AUTORIZADA)
//				.openParentheses()
//					.where("manifestoDfe.manifestoDfeSituacaoEnum = ?", ManifestoDfeSituacaoEnum.CIENCIA)
//					.or()
//					.where("manifestoDfe.manifestoDfeSituacaoEnum = ?", ManifestoDfeSituacaoEnum.CONFIRMACAO)
//				.closeParentheses()
				.openParentheses()
					.where("not exists(select entregadocumento.cdentregadocumento " +
							"from Entregadocumento entregadocumento " +
							"where entregadocumento.chaveacesso = manifestoDfe.chaveAcesso)")
					.or()
					.where("exists(select entregadocumento.cdentregadocumento " +
							"from Entregadocumento entregadocumento " +
							"where entregadocumento.chaveacesso = manifestoDfe.chaveAcesso and entregadocumento.entregadocumentosituacao = 1)")
				.closeParentheses();
		}
			
		query.orderBy("manifestoDfe.cdManifestoDfe desc")
			.list();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<ManifestoDfe> query) {
		query
			.leftOuterJoinFetch("manifestoDfe.arquivoXml arquivoXml")
			.leftOuterJoinFetch("manifestoDfe.arquivoNfeCompletoXml arquivoNfeCompletoXml")
			.list();
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		ManifestoDfe bean = (ManifestoDfe) save.getEntity();
		
		if (bean.getListaManifestoDfeHistorico() != null && bean.getListaManifestoDfeHistorico().size() > 0) {
			save.saveOrUpdateManaged("listaManifestoDfeHistorico");
		}
	}
	
	public ManifestoDfe procurarPorChaveAcesso(String chaveAcesso) {
		if (StringUtils.isEmpty(chaveAcesso)) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("manifestoDfe.cdManifestoDfe, manifestoDfe.chaveAcesso, manifestoDfe.cpf, manifestoDfe.cnpj, " +
						"manifestoDfe.razaoSocial, manifestoDfe.ie, manifestoDfe.tipoOperacaoNota, manifestoDfe.valorNf, " +
						"manifestoDfe.dhEmissao, manifestoDfe.dhRecbto, manifestoDfe.nuProt, manifestoDfe.statusEnum, " +
						"manifestoDfe.coStat, manifestoDfe.motivo, manifestoDfe.manifestoDfeSituacaoEnum," +
						"arquivoNfeCompletoXml.cdarquivo ")
				.leftOuterJoin("manifestoDfe.arquivoNfeCompletoXml arquivoNfeCompletoXml")
				.where("manifestoDfe.chaveAcesso = ?", chaveAcesso)
				.unique();
	}

	public void atualizarStatusEnum(ManifestoDfe manifestoDfe) {
		if (manifestoDfe == null || manifestoDfe.getStatusEnum() == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		getHibernateTemplate().bulkUpdate("update ManifestoDfe m set m.statusEnum = ? where m.id = ?", 
				new Object[] {manifestoDfe.getStatusEnum(), manifestoDfe.getCdManifestoDfe()});
	}

	public void atualizarArquivoXml(ManifestoDfe manifestoDfe) {
		if (manifestoDfe == null || manifestoDfe.getArquivoXml() == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		getHibernateTemplate().bulkUpdate("update ManifestoDfe m set m.arquivoXml = ? where m.id = ?", 
				new Object[] {manifestoDfe.getArquivoXml(), manifestoDfe.getCdManifestoDfe()});
	}

	public void atualizarArquivoNfeCompletoXml(ManifestoDfe manifestoDfe) {
		if (manifestoDfe == null || manifestoDfe.getArquivoNfeCompletoXml() == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		getHibernateTemplate().bulkUpdate("update ManifestoDfe m set m.arquivoNfeCompletoXml = ? where m.id = ?", 
				new Object[] {manifestoDfe.getArquivoNfeCompletoXml(), manifestoDfe.getCdManifestoDfe()});
	}

	public List<Empresa> procurarEmpresasDosManifestoDfe(String selectedItens) {
		if (StringUtils.isEmpty(selectedItens)) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("select distinct e.cdpessoa ");
		sql.append("from manifestodfe m ");
		sql.append("join loteconsultadfeitem li on li.cdloteconsultadfeitem = m.cdloteconsultadfeitem ");
		sql.append("join loteconsultadfe l on l.cdloteconsultadfe = li.cdloteconsultadfe ");
		sql.append("join empresa e on e.cdpessoa = l.cdempresa ");
		sql.append("where m.cdmanifestodfe in (" + selectedItens + ")");

		SinedUtil.markAsReader();
		@SuppressWarnings("unchecked")
		List<Empresa> listaEmpresa = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			
			@Override
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Empresa(rs.getInt("cdpessoa"));
			}
		});
		
		return listaEmpresa;
	}

	public ManifestoDfe procurarEmpresaPorId(Integer cdManifestoDfe) {
		if (cdManifestoDfe == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("empresa.cdpessoa, empresa.nome")
				.leftOuterJoin("manifestoDfe.loteConsultaDfeItem loteConsultaDfeItem")
				.leftOuterJoin("loteConsultaDfeItem.loteConsultaDfe loteConsultaDfe")
				.leftOuterJoin("loteConsultaDfe.empresa empresa")
				.where("manifestoDfe.cdManifestoDfe = ?", cdManifestoDfe)
				.unique();
	}

	public ManifestoDfe procurarPorId(Integer cdManifestoDfe) {
		if (cdManifestoDfe == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("manifestoDfe.cdManifestoDfe, manifestoDfe.chaveAcesso, manifestoDfe.cnpj, manifestoDfe.cpf ")
				.where("manifestoDfe.cdManifestoDfe = ?", cdManifestoDfe)
				.unique();
	}

	public void atualizarManifestoDfeSituacaoEnum(ManifestoDfe manifestoDfe) {
		if (manifestoDfe == null || manifestoDfe.getManifestoDfeSituacaoEnum() == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		getHibernateTemplate().bulkUpdate("update ManifestoDfe m set m.manifestoDfeSituacaoEnum = ? where m.id = ?", 
				new Object[] {manifestoDfe.getManifestoDfeSituacaoEnum(), manifestoDfe.getCdManifestoDfe()});
	}

	public List<ManifestoDfe> procurarStatusOuManifestoDfeSituacaoDiferenteDe(String selectedItens, LoteConsultaDfeItemStatusEnum statusEnum,
			List<ManifestoDfeSituacaoEnum> listaManifestoDfeSituacaoEnum) {
		if (StringUtils.isEmpty(selectedItens) || statusEnum == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		QueryBuilder<ManifestoDfe> query = query();
		
		query
			.select("manifestoDfe.cdManifestoDfe, manifestoDfe.chaveAcesso, manifestoDfe.statusEnum, manifestoDfe.manifestoDfeSituacaoEnum ")
			.whereIn("manifestoDfe.cdManifestoDfe", selectedItens);
		
		query
			.openParentheses()
				.openParentheses()
					.where("manifestoDfe.statusEnum != ?", statusEnum)
				.closeParentheses()
				.or()
				.openParentheses();
		for (ManifestoDfeSituacaoEnum m : listaManifestoDfeSituacaoEnum) {
			query
				.where("manifestoDfe.manifestoDfeSituacaoEnum != ?", m);
		}
		
		query
				.closeParentheses()
			.closeParentheses();
		
		return query.list();
	}

	public List<ManifestoDfe> procurarManifestoDfeSituacaoEnumDiferenteDe(String selectedItens, List<ManifestoDfeSituacaoEnum> listaManifestoDfeSituacaoEnum) {
		if (StringUtils.isEmpty(selectedItens) || listaManifestoDfeSituacaoEnum == null || listaManifestoDfeSituacaoEnum.size() == 0) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		QueryBuilder<ManifestoDfe> query = query();
		
		query
			.select("manifestoDfe.cdManifestoDfe, manifestoDfe.chaveAcesso ")
			.whereIn("manifestoDfe.cdManifestoDfe", selectedItens);
		
		for (ManifestoDfeSituacaoEnum m : listaManifestoDfeSituacaoEnum) {
			query
				.where("manifestoDfe.manifestoDfeSituacaoEnum != ?", m);
		}
		
		return query
				.list();
	}

	public List<ManifestoDfe> findForDownloadXmlCompleto(String whereIn) {
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("manifestoDfe.cdManifestoDfe, manifestoDfe.chaveAcesso, arquivoNfeCompletoXml.cdarquivo")
				.join("manifestoDfe.arquivoNfeCompletoXml arquivoNfeCompletoXml")
				.whereIn("manifestoDfe.cdManifestoDfe", whereIn)
				.list();
	}
}