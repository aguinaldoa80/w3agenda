package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratohistorico;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContratohistoricoDAO extends GenericDAO<Contratohistorico> {

	/**
	 * Carrega a lista de hist�rico de um contrato.
	 *
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contratohistorico> findByContrato(Contrato bean) {
		if(bean == null || bean.getCdcontrato() == null){
			throw new SinedException("Contrato n�o pode ser nulo.");
		}
		
		return query()
					.select("contratohistorico.cdcontratohistorico, contratohistorico.dthistorico, contratohistorico.visualizarContratoHistorico, " +
							"contratohistorico.valor, contratohistorico.dtproximovencimento, contratohistorico.formafaturamento, " +
							"contratohistorico.historico, contratohistorico.anotacao, contratohistorico.rateio, contratohistorico.parcelas, " +
							"contratohistorico.acao, contratohistorico.observacao, motivoCancelamentoFaturamento.cdmotivocancelamentofaturamento," +
							"motivoCancelamentoFaturamento.descricao, usuario.cdpessoa, usuario.nome, contagerencial.cdcontagerencial, " +
							"centrocusto.cdcentrocusto, projeto.cdprojeto, listaRateioitem.valor, listaRateioitem.percentual ")
					.leftOuterJoin("contratohistorico.contrato contrato")
					.leftOuterJoin("contrato.rateio rateio")
					.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
					.leftOuterJoin("listaRateioitem.contagerencial contagerencial")
					.leftOuterJoin("listaRateioitem.centrocusto centrocusto")
					.leftOuterJoin("listaRateioitem.projeto projeto")
					.leftOuterJoin("contratohistorico.usuario usuario")
					.leftOuterJoin("contratohistorico.motivoCancelamentoFaturamento motivoCancelamentoFaturamento")
					.where("contrato = ?", bean)
					.orderBy("contratohistorico.dthistorico desc")
					.list();
	}

	public Contratohistorico findForVisualizacao(Contratohistorico contratohistorico) {
		if(contratohistorico == null || contratohistorico.getCdcontratohistorico() == null){
			throw new SinedException("A propriedade cdcontratohistorico em contratohistorico n�o pode ser null.");
		}
		
		return query()
					.select("contratohistorico.cdcontratohistorico, contratohistorico.dthistorico, contratohistorico.acao, contratohistorico.observacao, " +
							"contratohistorico.valor, contratohistorico.dtproximovencimento, contratohistorico.formafaturamento, contratohistorico.historico, " +
							"contratohistorico.anotacao, contratohistorico.rateio, contratohistorico.parcelas, contratohistorico.quantidade, " +
							"contratohistorico.responsavel, contratohistorico.dtrenovacao, " +
							"fatorAjuste.cdfatorajuste, fatorAjuste.descricao, indiceCorrecao.cdindicecorrecao, indiceCorrecao.descricao")
					.leftOuterJoin("contratohistorico.fatorAjuste fatorAjuste")
					.leftOuterJoin("contratohistorico.indiceCorrecao indiceCorrecao")
					.where("contratohistorico = ?", contratohistorico)		
					.unique();
	}
}