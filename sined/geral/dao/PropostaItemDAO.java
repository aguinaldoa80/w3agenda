package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.geral.bean.PropostaItem;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PropostaItemDAO extends GenericDAO<PropostaItem> {

	/**
	 * Busca os iten a partir de uma proposta.
	 *
	 * @param proposta
	 * @return
	 * @since 24/01/2012
	 * @author Rodrigo Freitas
	 */
	public List<PropostaItem> findByProposta(Proposta proposta) {
		return query()
					.select("propostaItem.cdpropostaitem, propostaItem.valor, propostacaixaitem.nome, " +
							"propostacaixaitem.obrigatorio, propostacaixaitem.cdpropostacaixaitem")
					.join("propostaItem.propostacaixaitem propostacaixaitem")
					.join("propostaItem.proposta proposta")
					.where("proposta = ?", proposta)
					.list();
	}
	
	
}
