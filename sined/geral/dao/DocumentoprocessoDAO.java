package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Documentoprocesso;
import br.com.linkcom.sined.geral.bean.Documentoprocessosituacao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.DocumentoprocessoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;


public class DocumentoprocessoDAO extends GenericDAO<Documentoprocesso>{
	
	private ArquivoDAO arquivoDao;	
	
	public void setArquivoDao(ArquivoDAO arquivoDao) {
		this.arquivoDao = arquivoDao;
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Documentoprocesso> query) {
		query
		.select("documentoprocesso.cddocumentoprocesso,documentoprocesso.titulo," +
				"documentoprocesso.documentoprocessosituacao, documentoprocesso.documentoprocessotipo," +
				"responsavel.cdpessoa, responsavel.nome, documentoprocesso.arquivo," +
				"documentoprocesso.aprovadopor, documentoprocesso.descricao, documentoprocesso.dtdocumento,"+
				"listahistorico.cddocumentoprocessohistorico,documentoprocesso.dtaltera,documentoprocesso.cdusuarioaltera, " +
				"situacaohist.cddocumentoprocessosituacao,situacaohist.nome," +
				"arquivohist.cdarquivo,arquivohist.nome,arquivohist.tipoconteudo," +
				"arquivohist.dtmodificacao,arquivohist.tamanho, listahistorico.observacao, listahistorico.dtaltera, " +
				"documentoprocessoprojeto.cddocumentoprocessoprojeto, projeto.cdprojeto, projeto.nome, documentoprocesso.vencimento, " +
				"listadocumentoprocessomaterial.cddocumentoprocessomaterial, material.cdmaterial, material.nome")
		.leftOuterJoin("documentoprocesso.arquivo arquivo")
		.leftOuterJoin("documentoprocesso.responsavel responsavel")		
		.leftOuterJoin("documentoprocesso.listahistorico listahistorico")
		.leftOuterJoin("listahistorico.arquivo arquivohist")
		.leftOuterJoin("listahistorico.documentoprocessosituacao situacaohist")
		.leftOuterJoin("documentoprocesso.listadocumentoprocessoprojeto documentoprocessoprojeto")
		.leftOuterJoin("documentoprocesso.listadocumentoprocessomaterial listadocumentoprocessomaterial")
		.leftOuterJoin("documentoprocessoprojeto.projeto projeto")
		.leftOuterJoin("listadocumentoprocessomaterial.material material")
		.orderBy("listahistorico.dtaltera desc");
		
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		arquivoDao.saveFile(save.getEntity(), "arquivo");		
	}
		
	@Override
	public void updateListagemQuery(QueryBuilder<Documentoprocesso> query,FiltroListagem _filtro) {
		Usuario pessoa;
		try{
			 pessoa = (Usuario)Neo.getUser();
		}catch(Exception e){
			throw new SinedException("Erro. Usu�rio logado inv�lido em DocumentoprocessoDAO");
		}				
		DocumentoprocessoFiltro filtro = (DocumentoprocessoFiltro)_filtro;
		List<Documentoprocessosituacao> listasituacao = filtro.getListasituacao();		
		if(listasituacao ==null){
			listasituacao = new ArrayList<Documentoprocessosituacao>();
			listasituacao.add(Documentoprocessosituacao.DISPONIVEL);
			listasituacao.add(Documentoprocessosituacao.EM_APROVACAO);
			listasituacao.add(Documentoprocessosituacao.ARQUIVO_MORTO);
		}	
		
		Timestamp fim = null;
		if(filtro.getFim()!=null){
			fim = new Timestamp(SinedDateUtils.incrementDate(filtro.getFim(), 1, Calendar.DATE).getTime());
			fim.setTime(fim.getTime()-(86400000));	
		}
		query
		.select("documentoprocesso.cddocumentoprocesso, documentoprocesso.titulo, documentoprocesso.vencimento," +
				"documentoprocesso.dtdocumento, responsavel.cdpessoa, responsavel.nome," +
				"documentoprocessotipo.cddocumentoprocessotipo,documentoprocessotipo.nome," +
				"documentoprocessosituacao.cddocumentoprocessosituacao, documentoprocessosituacao.nome," +
				"documentoprocesso.arquivo")
		.leftOuterJoin("documentoprocesso.responsavel responsavel")
		.leftOuterJoin("documentoprocesso.documentoprocessotipo documentoprocessotipo")
		.leftOuterJoin("documentoprocesso.documentoprocessosituacao documentoprocessosituacao")
		.leftOuterJoin("documentoprocesso.listadocumentoprocessomaterial listadocumentoprocessomaterial")
		.leftOuterJoin("listadocumentoprocessomaterial.material material")
		.where("documentoprocesso.cddocumentoprocesso=?", filtro.getNumDocumento())
		.whereLikeIgnoreAll("documentoprocesso.titulo", filtro.getTitulo())
		.whereLikeIgnoreAll("documentoprocesso.aprovadopor", filtro.getAprovadopor())
		.where("documentoprocesso.responsavel=?", filtro.getResponsavel())
		.where("documentoprocesso.documentoprocessotipo=?", filtro.getTipo())		
		.where("documentoprocesso.dtdocumento >= ?", filtro.getInicio())
		.where("documentoprocesso.dtdocumento <= ?", fim)
		.where("documentoprocesso.vencimento >= ?", filtro.getInicioVencimento())
		.where("documentoprocesso.vencimento <= ?", filtro.getFimVencimento())
		.where("material = ?", filtro.getMaterial())
		.orderBy("documentoprocesso.cddocumentoprocesso desc");
		//If necess�rios para quando trazer os documentos em aprova��o
		//trazer somente os que o usu�rio logado � respons�vel
		if(!listasituacao.contains(Documentoprocessosituacao.EM_APROVACAO)){			
			query			
			.whereIn("documentoprocessosituacao.cddocumentoprocessosituacao", 
					(filtro.getListasituacao()!=null && 
					filtro.getListasituacao().size()>0) ?  
							CollectionsUtil.listAndConcatenate(filtro.getListasituacao(),
							"cddocumentoprocessosituacao" , ","): null);			
		}else{			
			if(listasituacao==null || listasituacao.isEmpty() || listasituacao.size()==3){
				query.openParentheses();
				query.where(" documentoprocesso.documentoprocessosituacao=? or " +
						"documentoprocesso.documentoprocessosituacao=? or " +
						"(documentoprocesso.documentoprocessosituacao=? " +
						"and documentoprocesso.responsavel=?)",
						new Object[]{Documentoprocessosituacao.DISPONIVEL,
						Documentoprocessosituacao.ARQUIVO_MORTO,
						Documentoprocessosituacao.EM_APROVACAO, pessoa});
				query.closeParentheses();
			}else{
				if(listasituacao.size()==2){
					listasituacao.remove(Documentoprocessosituacao.EM_APROVACAO);
					query.openParentheses();
					query.
					where("documentoprocesso.documentoprocessosituacao=? or " +
							"(documentoprocesso.documentoprocessosituacao=? " +
							"and documentoprocesso.responsavel=?)",
							new Object[]{listasituacao.get(0),
							Documentoprocessosituacao.EM_APROVACAO, pessoa});
					listasituacao.add(Documentoprocessosituacao.EM_APROVACAO);
					query.closeParentheses();
					
				}else{
					query.where("(documentoprocesso.documentoprocessosituacao=? " +
							"and documentoprocesso.responsavel=?)",
							new Object[]{
							Documentoprocessosituacao.EM_APROVACAO, pessoa});
				}
			}
		}
		
		if(filtro.getProjeto() != null){
			query
				.leftOuterJoin("documentoprocesso.listadocumentoprocessoprojeto documentoprocessoprojeto")
				.leftOuterJoin("documentoprocessoprojeto.projeto projeto")
				.where("projeto.cdprojeto = ?", filtro.getProjeto().getCdprojeto());
		}
	}

	/**
	 * Retorna o documento com os dados do arquivo e da lista de
	 * hist�rico carregados
	 * @param documento
	 * @return	 
	 * @author C�ntia Nogueira
	 */
	public Documentoprocesso loadArquivo(Documentoprocesso documento){
		if(documento== null || documento.getCddocumentoprocesso()==null){
			throw new SinedException("Objeto n�o pode ser null em DocumentoprocessoDAO.");
		}
		
		return query()
		       .select("documentoprocesso.cddocumentoprocesso, documentoprocesso.arquivo, "+
		       "listahistorico.cddocumentoprocessohistorico, documentoprocesso.dtaltera,documentoprocesso.cdusuarioaltera," +				
				"arquivohist.cdarquivo,arquivohist.nome,arquivohist.tipoconteudo," +
				"arquivohist.dtmodificacao,arquivohist.tamanho, listahistorico.observacao, responsavel.cdpessoa," +
				"responsavel.nome,listahistorico.dtaltera, situacaohist.cddocumentoprocessosituacao,situacaohist.nome")
		       .leftOuterJoin("documentoprocesso.listahistorico listahistorico")
		       .leftOuterJoin("listahistorico.arquivo arquivohist")
		       .leftOuterJoin("documentoprocesso.responsavel responsavel")
		       .leftOuterJoin("listahistorico.documentoprocessosituacao situacaohist")
		       .where("documentoprocesso.cddocumentoprocesso=?", documento.getCddocumentoprocesso())
		       .unique();
	}

	/**
	 * Retorna todos os documentos do usu�rio logado com arquivos
	 * carregados
	 * @return
	 * @author C�ntia Nogueira
	 */
	public List<Documentoprocesso> findAllwithArquivos(){
		Usuario usuario;
		try{
			 usuario = (Usuario)Neo.getUser();
		}catch(Exception e){
			throw new SinedException("Erro. Usu�rio logado inv�lido em DocumentoprocessoDAO");
		}
		
		return query()
		 		.select("documentoprocesso.cddocumentoprocesso, documentoprocesso.titulo," +
		 				"documentoprocesso.arquivo")
		 		.where("documentoprocesso.responsavel=?" , usuario) 
		 		.list();
		 				
	}

	/**
	 * Lista de documento/processo de acordo com o respons�vel
	 *
	 * @param usuario
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Documentoprocesso> findByResponsavel(Usuario usuario) {
		if (usuario == null || usuario.getCdpessoa() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("documentoprocesso.cddocumentoprocesso, documentoprocesso.titulo")
					.where("documentoprocesso.responsavel = ?",usuario)
					.orderBy("documentoprocesso.titulo")
					.list();
	}
	
	/**
	 * Retorna com a situacao do documento
	 * @param bean
	 * @return
	 * @author C�ntia Nogueira
	 */
	public Documentoprocesso loadSituacao(Documentoprocesso bean) {
		if (bean == null || bean.getCddocumentoprocesso() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("documentoprocesso.cddocumentoprocesso, documentoprocesso.documentoprocessosituacao")
					.where("documentoprocesso.cddocumentoprocesso = ?",bean.getCddocumentoprocesso())
					.orderBy("documentoprocesso.titulo")
					.unique();
	}
	
	/**
	 * Retorna os dados para o relat�rio de documento
	 * @param filtro
	 * @return
	 * @author C�ntia Nogueira
	 */
	public List<Documentoprocesso> findForRelatorio(DocumentoprocessoFiltro filtro){
		if(filtro==null){		
			throw new SinedException("Erro. Filtro n�o pode ser nulo em  DocumentoprocessoDAO");
		}				
		
		List<Documentoprocessosituacao> listasituacao = filtro.getListasituacao();		
		if(listasituacao ==null){
			listasituacao = new ArrayList<Documentoprocessosituacao>();
			listasituacao.add(Documentoprocessosituacao.DISPONIVEL);
			listasituacao.add(Documentoprocessosituacao.EM_APROVACAO);
			listasituacao.add(Documentoprocessosituacao.ARQUIVO_MORTO);
		}	
		Timestamp fim = null;
		if(filtro.getFim()!=null){
			fim = new Timestamp(SinedDateUtils.incrementDate(filtro.getFim(), 1, Calendar.DATE).getTime());
			fim.setTime(fim.getTime()-(86400000));	
		}
		
		 return querySined()
				 
		.select("documentoprocesso.cddocumentoprocesso, documentoprocesso.titulo," +
				"documentoprocesso.dtdocumento, responsavel.cdpessoa, responsavel.nome," +
				"documentoprocessotipo.cddocumentoprocessotipo,documentoprocessotipo.nome," +
				"documentoprocessosituacao.cddocumentoprocessosituacao, documentoprocessosituacao.nome," +
				"documentoprocesso.arquivo")
		.leftOuterJoin("documentoprocesso.responsavel responsavel")
		.leftOuterJoin("documentoprocesso.documentoprocessotipo documentoprocessotipo")
		.leftOuterJoin("documentoprocesso.documentoprocessosituacao documentoprocessosituacao")
		.where("documentoprocesso.cddocumentoprocesso=?", filtro.getNumDocumento())
		.whereLikeIgnoreAll("documentoprocesso.titulo", filtro.getTitulo())
		.whereLikeIgnoreAll("documentoprocesso.aprovadopor", filtro.getAprovadopor())
		.where("documentoprocesso.responsavel=?", filtro.getResponsavel())
		.where("documentoprocesso.documentoprocessotipo=?", filtro.getTipo())		
		.where("documentoprocesso.dtdocumento >= ?", filtro.getInicio())
		.where("documentoprocesso.dtdocumento <= ?", fim)
		.whereIn( "documentoprocessosituacao.cddocumentoprocessosituacao", CollectionsUtil.listAndConcatenate(listasituacao,
											"cddocumentoprocessosituacao", ","))
		.orderBy("documentoprocesso.dtdocumento asc")
		.list();
		
	}
		
	/**
	 * M�todo que verifica se o documento/processo bean possui vencimento atrasado.
	 * @param bean
	 * @return
	 * @author Rafael Salvio
	 */
	public Boolean isDtVencimentoAtrasada(String whereIn){
		if(whereIn==null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.from(Documentoprocesso.class)
			.whereIn("documentoprocesso.cddocumentoprocesso", whereIn)
			.openParentheses()
			.where("documentoprocesso.vencimento is null")
			.or()
			.where("documentoprocesso.vencimento < ?", new Date(System.currentTimeMillis()))
			.closeParentheses()
			.unique() > 0;
	}
	
	/**
	 * M�todo que retorna uma lista de documentoprocesso de acordo com o whereIn fornecido
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Documentoprocesso> loadForRenovar(String whereIn){
		if(whereIn==null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
					.select("documentoprocesso.cddocumentoprocesso, documentoprocesso.vencimento")
					.whereIn("documentoprocesso.cddocumentoprocesso", whereIn)
					.list();
	}
	
	/**
	 * M�todo que atualiza a data de vencimento de um documentoprocesso.
	 * @param documentoprocesso
	 * @author Rafael Salvio
	 */
	public void saveDocumentoprocessoRenovado(Documentoprocesso documentoprocesso){
		if(documentoprocesso == null || documentoprocesso.getCddocumentoprocesso() == null){
			throw new SinedException("Par�metros inv�lidos!");
		}
		
		String query = "update Documentoprocesso set vencimento = ? where cddocumentoprocesso = ?";
		getHibernateTemplate().bulkUpdate(query, new Object[]{documentoprocesso.getNovovencimento(), documentoprocesso.getCddocumentoprocesso()});
	}
}
