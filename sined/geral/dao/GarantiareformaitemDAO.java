package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Garantiareforma;
import br.com.linkcom.sined.geral.bean.Garantiareformaitem;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.enumeration.Garantiasituacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class GarantiareformaitemDAO extends GenericDAO<Garantiareformaitem>{

	
	public void updatePedidovendamaterial(Garantiareformaitem bean, Pedidovendamaterial pedidovendamaterial){
		String sql = "update garantiareformaitem set cdpedidovendamaterialdestino = " + pedidovendamaterial.getCdpedidovendamaterial() + 
					 " where cdgarantiareformaitem = " + bean.getCdgarantiareformaitem();
		getJdbcTemplate().execute(sql);
	}
	
	public void desassociaPedidovendamaterial(Garantiareformaitem bean){
		String sql = "update garantiareformaitem set cdpedidovendamaterialdestino = null " + 
					 " where cdgarantiareformaitem = " + bean.getCdgarantiareformaitem();
		getJdbcTemplate().execute(sql);
	}
	
	public void updateVendamaterial(Garantiareformaitem bean, Vendamaterial vendamaterial){
		String sql = "update garantiareformaitem set cdvendamaterialdestino = " + vendamaterial.getCdvendamaterial() + 
					 " where cdgarantiareformaitem = " + bean.getCdgarantiareformaitem();
		getJdbcTemplate().execute(sql);
	}
	
	public void desassociaVendamaterial(Garantiareformaitem bean){
		String sql = "update garantiareformaitem set cdvendamaterialdestino = null " + 
					 " where cdgarantiareformaitem = " + bean.getCdgarantiareformaitem();
		getJdbcTemplate().execute(sql);
	}
	
	public Garantiareformaitem loadByPedidovendamaterial(Pedidovendamaterial pedidovendamaterialdestino){
		if(pedidovendamaterialdestino == null){
			return null;
		}
		return query()
				.select("garantiareformaitem.cdgarantiareformaitem, garantiareforma.cdgarantiareforma")
				.join("garantiareformaitem.garantiareforma garantiareforma")
				.join("garantiareformaitem.pedidovendamaterialdestino pedidovendamaterialdestino")
				.where("pedidovendamaterialdestino = ?", pedidovendamaterialdestino)
				.where("garantiareforma.garantiasituacao = ?", Garantiasituacao.UTILIZADA)
				.unique();
	}
	
	public Garantiareformaitem loadByVendamaterial(Vendamaterial vendamaterialdestino){
		if(vendamaterialdestino == null){
			return null;
		}
		return query()
				.select("garantiareformaitem.cdgarantiareformaitem, garantiareforma.cdgarantiareforma")
				.join("garantiareformaitem.garantiareforma garantiareforma")
				.join("garantiareformaitem.vendamaterialdestino vendamaterialdestino")
				.where("vendamaterialdestino = ?", vendamaterialdestino)
				.where("garantiareforma.garantiasituacao = ?", Garantiasituacao.UTILIZADA)
				.unique();
	}
	
	public Garantiareformaitem loadByGarantiareformaitem(Garantiareformaitem garantiareformaitem){
		if(garantiareformaitem == null){
			return null;
		}
		return query()
				.select("garantiareformaitem.cdgarantiareformaitem, garantiareforma.cdgarantiareforma")
				.join("garantiareformaitem.garantiareforma garantiareforma")
				.where("garantiareformaitem = ?", garantiareformaitem)
				.unique();
	}
	
	
	public Garantiareformaitem loadByGarantiareforma(Garantiareforma garantiareforma, Garantiasituacao... garantiasituacao){
		if(garantiareforma == null){
			return null;
		}
		String whereInSituacao = this.montaWhereInSituacoes(garantiasituacao);
		return query()
				.select("garantiareformaitem.cdgarantiareformaitem, garantiareforma.cdgarantiareforma")
				.join("garantiareformaitem.garantiareforma garantiareforma")
				.where("garantiareforma = ?", garantiareforma)
				.whereIn("garantiareforma.garantiasituacao", whereInSituacao)
				.unique();
	}
	
	public Garantiareformaitem loadByGarantiareformaForCancelarValecompra(Garantiareforma garantiareforma, Garantiareformaitem garantiareformaitem){
		return query()
		.select("garantiareformaitem.cdgarantiareformaitem, garantiareforma.cdgarantiareforma, cliente.cdpessoa, cliente.nome")
		.join("garantiareformaitem.garantiareforma garantiareforma")
		.join("garantiareforma.cliente cliente")
		.where("garantiareforma = ?", garantiareforma)
		.where("garantiareformaitem = ?", garantiareformaitem)
		.where("garantiareforma.garantiasituacao = ?", Garantiasituacao.UTILIZADA)
		.unique();		
	}
	
	private String montaWhereInSituacoes(Garantiasituacao... situacoes){
		String whereInSituacao = null;
		StringBuilder values = new StringBuilder();
		for (int i=0; i < situacoes.length; i++) {
			values.append(situacoes[i].ordinal()).append(",");
		}
		whereInSituacao = values.length() == 0? null: values.toString().substring(0, values.toString().length()-1);
		return whereInSituacao;
	}
	
	public Garantiareformaitem loadByVerificacaoUsoVenda(Garantiareformaitem garantiareformaitem, Garantiareforma garantiareforma){
		if(garantiareformaitem == null && garantiareforma == null){
			return null;
		}
		return query()
				.select("garantiareformaitem.cdgarantiareformaitem, garantiareforma.cdgarantiareforma, vendamaterialdestino.cdvendamaterial, " +
						"pedidovendamaterialdestino.cdpedidovendamaterial, pedidovenda.cdpedidovenda, venda.cdvenda")
				.join("garantiareformaitem.garantiareforma garantiareforma")
				.leftOuterJoin("garantiareformaitem.pedidovendamaterialdestino pedidovendamaterialdestino")
				.leftOuterJoin("pedidovendamaterialdestino.pedidovenda pedidovenda")
				.leftOuterJoin("garantiareformaitem.vendamaterialdestino vendamaterialdestino")
				.leftOuterJoin("vendamaterialdestino.venda venda")
				.where("garantiareformaitem = ?", garantiareformaitem)
				.where("garantiareforma = ?", garantiareforma)
				.where("garantiareforma.garantiasituacao = ?", Garantiasituacao.UTILIZADA)
				.unique();
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Garantiareformaitem bean = (Garantiareformaitem) save.getEntity();
		
		if (bean.isSaveListaGarantiareformaitemconstatacao()){
			save.saveOrUpdateManaged("listaGarantiareformaitemconstatacao");
		}
		
		super.updateSaveOrUpdate(save);
	}
}