package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Producaoetapanome;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ProducaoetapanomeFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("producaoetapanome.nome")
public class ProducaoetapanomeDAO extends GenericDAO<Producaoetapanome> {

	@Override
	public void updateListagemQuery(QueryBuilder<Producaoetapanome> query,	FiltroListagem _filtro) {
		ProducaoetapanomeFiltro filtro = (ProducaoetapanomeFiltro) _filtro;
		
		query
			.select("producaoetapanome.cdproducaoetapanome, producaoetapanome.nome")
			.whereLikeIgnoreAll("producaoetapanome.nome", filtro.getNome())
			.orderBy("producaoetapanome.nome");
			
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Producaoetapanome> query) {
		query
			.select("producaoetapanome.cdproducaoetapanome, producaoetapanome.nome, producaoetapanome.permitirtrocarprodutofinal, " +
					"producaoetapanome.permitirgerargarantia, " +
					"listaProducaoetapanomecampo.cdproducaoetapanomecampo, listaProducaoetapanomecampo.nome, " +
					"listaProducaoetapanomecampo.tipo, listaProducaoetapanomecampo.obrigatorio, producaoetapanome.enviaritemadicionalaposconclusao," +
					"producaoetapanome.enviarservicoaposconclusao, producaoetapanome.enviarbandaaposconclusao")
			.leftOuterJoin("producaoetapanome.listaProducaoetapanomecampo listaProducaoetapanomecampo");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaProducaoetapanomecampo");
	}
	
	/**
	* M�todo que carrega o nome de etapa de produ��o com os campos adicionais
	*
	* @param producaoetapanome
	* @return
	* @since 23/02/2016
	* @author Luiz Fernando
	*/
	public Producaoetapanome loadWithCamposAdicionais(Producaoetapanome producaoetapanome) {
		if(producaoetapanome == null || producaoetapanome.getCdproducaoetapanome() == null)
			throw new SinedException("Par�metro inv�lido");
		
		return query()
			.select("producaoetapanome.cdproducaoetapanome, producaoetapanome.nome, " +
					"listaProducaoetapanomecampo.cdproducaoetapanomecampo, listaProducaoetapanomecampo.nome, " +
					"listaProducaoetapanomecampo.tipo, listaProducaoetapanomecampo.obrigatorio")
			.leftOuterJoin("producaoetapanome.listaProducaoetapanomecampo listaProducaoetapanomecampo")
			.where("producaoetapanome = ?", producaoetapanome)
			.unique();
	}
	
	/**
	* M�todo que busca os nomes das etapas de produ��o
	*
	* @return
	* @since 23/02/2016
	* @author Luiz Fernando
	*/
	public List<Producaoetapanome> findForW3Producao(String whereIn){
		return query()
			.select("producaoetapanome.cdproducaoetapanome, producaoetapanome.nome, producaoetapanome.permitirgerargarantia, producaoetapanome.enviaritemadicionalaposconclusao," +
					"producaoetapanome.enviarservicoaposconclusao, producaoetapanome.enviarbandaaposconclusao")
			.whereIn("producaoetapanome.cdproducaoetapanome", whereIn)
			.list();
	}
	
	public boolean isPermitirtrocarprodutofinal(Producaoetapanome producaoetapanome){
		return newQueryBuilderWithFrom(Long.class)
					.select("count(*)")
					.where("producaoetapanome=?", producaoetapanome)
					.where("producaoetapanome.permitirtrocarprodutofinal=?", Boolean.TRUE)
					.unique()>0;
	}
}
