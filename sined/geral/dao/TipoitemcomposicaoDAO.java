package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Tipoitemcomposicao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("tipoitemcomposicao.nome")
public class TipoitemcomposicaoDAO extends GenericDAO<Tipoitemcomposicao>{

}
