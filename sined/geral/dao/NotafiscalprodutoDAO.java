package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.GnreSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoLancamentoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.bean.enumeration.SIntegraNaturezaoperacao;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVinculoExpedicao;
import br.com.linkcom.sined.geral.bean.enumeration.TipoVendedorEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotafiscalprodutoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.RelatorioMargemContribuicaoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SintegraFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedpiscofinsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaoicmsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaoipiFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaopiscofinsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.SagefiscalFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotafiscalprodutoDAO extends GenericDAO<Notafiscalproduto> {

	private EnderecoService enderecoService;
	private UsuarioService usuarioService;
	private ParametrogeralService parametrogeralService;
	
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Notafiscalproduto> query, FiltroListagem _filtro) {
		NotafiscalprodutoFiltro filtro = (NotafiscalprodutoFiltro) _filtro;
		Integer idArquivonf = null;
		String cdarquivonf = NeoWeb.getRequestContext().getParameter("cdarquivonf");
		if(cdarquivonf != null && !cdarquivonf.equals("")){
			idArquivonf = Integer.parseInt(cdarquivonf);
		}

		query
			.select("distinct notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.dtEmissao, notafiscalproduto.valorfrete, notafiscalproduto.numeronota, " +
					"notafiscalproduto.diferencaduplicata,notafiscalproduto.situacaoExpedicao, notafiscalproduto.modeloDocumentoFiscalEnum, notafiscalproduto.origemCupom, " +
					"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.simplesremessa, " +
					"cliente.nome, notaStatus.cdNotaStatus, notaStatus.nome, notafiscalproduto.valor, empresa.nomefantasia, empresa.razaosocial, empresa.nome, notafiscalproduto.tipooperacaonota, notafiscalproduto.cadastrarcobranca")
	
			.leftOuterJoin("notafiscalproduto.notaTipo notaTipo")
			.leftOuterJoin("notafiscalproduto.projeto projeto")
			.leftOuterJoin("notafiscalproduto.empresa empresa")
			.leftOuterJoin("notafiscalproduto.cliente cliente")
			.leftOuterJoin("notafiscalproduto.enderecoCliente enderecoCliente")
			.leftOuterJoin("enderecoCliente.municipio municipioCliente")
			.leftOuterJoin("municipioCliente.uf ufCliente")
			.leftOuterJoin("notafiscalproduto.notaStatus notaStatus")
			.leftOuterJoin("notafiscalproduto.naturezaoperacao naturezaoperacao")
			
			.leftOuterJoin("notafiscalproduto.listaArquivonfnota arquivonfnota")
			.leftOuterJoin("arquivonfnota.arquivonf arquivonf");
		
		if (filtro.getColaborador()!= null){
		    if (filtro.getTipoVendedor() != null && TipoVendedorEnum.PRINCIPAL.equals(filtro.getTipoVendedor())) {
				query
				    .leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
				    .openParentheses()
					.where("listaClientevendedor.colaborador = ?", filtro.getColaborador())
					.where("listaClientevendedor.principal = ?", Boolean.TRUE)
				.closeParentheses();
		    } else {
		    	query
			         .leftOuterJoin("notafiscalproduto.listaNotavenda listaNotavenda")
			         .leftOuterJoin("listaNotavenda.venda venda")
			         .where("venda.colaborador = ?", filtro.getColaborador());
		    }
		    query.ignoreJoin("listaClienteVendedor","colaborador", "fornecedor","principal","listaNotavenda","venda","expedicao");
		}
		
		
			if(filtro.getMaterial() != null || filtro.getCfop() != null || filtro.getMaterialgrupo() != null){
				query
				.leftOuterJoin("notafiscalproduto.listaItens item")
				.leftOuterJoin("item.material material")
				.where("material = ?", filtro.getMaterial())
				.where("item.cfop = ?", filtro.getCfop())
				.where("material.materialgrupo = ?", filtro.getMaterialgrupo());
			}
			
			query
			.where("empresa = ?", filtro.getEmpresa())
			//.whereIn("notafiscalproduto.numero", filtro.getNumNota() != null && !filtro.getNumNota().equals("") ? "'" + filtro.getNumNota().replaceAll(",", "','") + "'" : null)
			.where("cast(notafiscalproduto.numero AS long) >= ?", filtro.getNumNotaDe())
			.where("cast(notafiscalproduto.numero AS long) <= ?", filtro.getNumNotaAte())
			.where("notafiscalproduto.dtEmissao >= ?", filtro.getDtEmissaoInicio())
			.where("notafiscalproduto.dtEmissao <= ?", filtro.getDtEmissaoFim())
			.where("cliente = ?", filtro.getCliente())
			.where("projeto = ?", filtro.getProjeto())
			.where("arquivonf.cdarquivonf = ?", idArquivonf)
			.whereIn("notaStatus.cdNotaStatus", filtro.getListaNotaStatus() != null ? CollectionsUtil.listAndConcatenate(filtro.getListaNotaStatus(), "cdNotaStatus", ",") : null)
			.where("notaTipo = ?", NotaTipo.NOTA_FISCAL_PRODUTO)
			.where("notafiscalproduto.situacaoExpedicao = ?", filtro.getSituacaoExpedicao())
			.where("notafiscalproduto.valor = ?", filtro.getValor())
//			.whereLikeIgnoreAll("notafiscalproduto.naturezaoperacao", naturezaoperacao)
			.where("notafiscalproduto.naturezaoperacao=?", filtro.getNaturezaoperacao())
			.where("notafiscalproduto.tipooperacaonota=?", filtro.getTipooperacaonota())
			.where("notafiscalproduto.operacaonfe=?", filtro.getOperacaonfe())
			.where("notafiscalproduto.localdestinonfe=?", filtro.getLocaldestinonfe())
			.where("municipioCliente.uf=?", filtro.getUf())
			.where("notafiscalproduto.modeloDocumentoFiscalEnum = ?", filtro.getModeloDocumentoFiscalEnum())
			.ignoreJoin("arquivonfnota", "arquivonf", "item", "material");
			
		if(filtro.getGnregerada() != null && filtro.getGnregerada()){
			query.where("exists(select g.cdgnre from Gnre g where g.nota = notafiscalproduto and g.situacao <> ?)", GnreSituacao.CANCELADA);
		} else if(filtro.getGnregerada() != null && !filtro.getGnregerada()){
			query.where("not exists(select g.cdgnre from Gnre g where g.nota = notafiscalproduto and g.situacao <> ?)", GnreSituacao.CANCELADA);
		}
			
		if (filtro.getDtPagamentoInicio() != null && filtro.getDtPagamentoFim() != null)
			query.where("notafiscalproduto.cdNota in (select n.cdNota from Nota n join n.listaNotaDocumento lnd join lnd.documento d join d.listaMovimentacaoOrigem lmo join lmo.movimentacao m join m.movimentacaoacao macao where macao.cdmovimentacaoacao <> " + Movimentacaoacao.CANCELADA.getCdmovimentacaoacao() + " and m.dtmovimentacao >= ? and m.dtmovimentacao <= ?)", new Object[]{filtro.getDtPagamentoInicio(), filtro.getDtPagamentoFim()});
		else if (filtro.getDtPagamentoInicio() != null)
			query.where("notafiscalproduto.cdNota in (select n.cdNota from Nota n join n.listaNotaDocumento lnd join lnd.documento d join d.listaMovimentacaoOrigem lmo join lmo.movimentacao m join m.movimentacaoacao macao where macao.cdmovimentacaoacao <> " + Movimentacaoacao.CANCELADA.getCdmovimentacaoacao() + " and m.dtmovimentacao >= ?)", filtro.getDtPagamentoInicio());
		else if (filtro.getDtPagamentoFim() != null)
			query.where("notafiscalproduto.cdNota in (select n.cdNota from Nota n join n.listaNotaDocumento lnd join lnd.documento d join d.listaMovimentacaoOrigem lmo join lmo.movimentacao m join m.movimentacaoacao macao where macao.cdmovimentacaoacao <> " + Movimentacaoacao.CANCELADA.getCdmovimentacaoacao() + " and m.dtmovimentacao <= ?)", filtro.getDtPagamentoFim());			
		
		if(filtro.getOrderBy() == null || filtro.getOrderBy().equals("")){
			query.orderBy("notafiscalproduto.dtEmissao desc, notafiscalproduto.numero desc");
		}
		
		if(filtro.getMostraComNumero() != null){
			if(filtro.getMostraComNumero()){
				query
					.where("notafiscalproduto.numero is not null")
					.where("notafiscalproduto.numero <> ?", "");
			} else {
				query
					.openParentheses()
					.where("notafiscalproduto.numero is null")
					.or()
					.where("notafiscalproduto.numero = ?", "")
					.closeParentheses();
			}
		}
		
		if(filtro.getEnderecostr() != null && !"".equals(filtro.getEnderecostr())){
			query.whereLikeIgnoreAll("(coalesce(enderecoCliente.logradouro,'') || coalesce(enderecoCliente.numero,'') || coalesce(enderecoCliente.complemento,'') || " +
									 " coalesce(enderecoCliente.bairro,'') || coalesce(enderecoCliente.cep,'') || coalesce(enderecoCliente.pontoreferencia,'') || " +
									 " coalesce(municipioCliente.nome,'') || coalesce(ufCliente.sigla,''))", filtro.getEnderecostr());
		}
		
		boolean usuarioComRestricao = SinedUtil.getUsuarioLogado().getRestricaoFornecedorColaborador();
		Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
		if(usuarioComRestricao){
			query.leftOuterJoin("notafiscalproduto.transportador transportador")
				.leftOuterJoin("transportador.listaColaboradorFornecedor colaboradorFornecedor")
				.openParentheses()
					.where("colaboradorFornecedor.cdcolaboradorfornecedor is null")
					.or()
					.where("colaboradorFornecedor.colaborador = ?", colaborador)
				.closeParentheses();
		}
		
		if(!SinedUtil.isUsuarioLogadoAdministrador()){
			query.leftOuterJoin("notafiscalproduto.listaRestricao restricao")
					.openParentheses()
						.where("restricao.cdnotafiscalprodutorestricao is null")
						.or()
						.where("restricao.usuario = ?", SinedUtil.getUsuarioLogado())
					.closeParentheses();
		}
		
		if(filtro.getRegistroreceita() != null){
			if(filtro.getRegistroreceita()){
				query.where("exists(select an.cdarquivonfnota " +
						"from Arquivonfnota an " +
						"where an.nota = notafiscalproduto " +
						"and an.chaveacesso is not null " +
						"and an.protocolonfe is not null " +
						"and an.dtprocessamento is not null " +
						" )");
			} else {
				query.where("not exists(select an.cdarquivonfnota " +
						"from Arquivonfnota an " +
						"where an.nota = notafiscalproduto " +
						"and an.chaveacesso is not null " +
						"and an.protocolonfe is not null " +
						"and an.dtprocessamento is not null " +
						" )");
			}
		} 
		
		addWhereRestricaoClienteVendedor(query);
	}
	
	/**
	* M�todo que adiciona o where de restri��o cliente/vendedor
	*
	* @param query
	* @since 22/10/2015
	* @author Luiz Fernando
	*/
	public void addWhereRestricaoClienteVendedor(QueryBuilder<Notafiscalproduto> query){
		boolean restricaoClienteVendedor = usuarioService.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado());
		Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
		if(restricaoClienteVendedor && colaborador != null){
			query.leftOuterJoinIfNotExists("notafiscalproduto.cliente cliente");
			query.where("cliente.cdpessoa in (select c.cdpessoa " +
											" from Cliente c " +
											" left outer join c.listaClientevendedor clientevendedor " +
											" left outer join clientevendedor.colaborador colaborador " +
											" where colaborador.cdpessoa = " + colaborador.getCdpessoa() + " " +
											" or colaborador is null ) ");
		}
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		Notafiscalproduto bean = (Notafiscalproduto)save.getEntity();
		bean.setNotaTipo(NotaTipo.NOTA_FISCAL_PRODUTO);
		
		if (bean.getEndereconota() != null){
			Endereco endereco = bean.getEndereconota();
			enderecoService.saveOrUpdateNoUseTransaction(endereco);
		}
		
		save.saveOrUpdateManaged("listaReferenciada");
		save.saveOrUpdateManaged("listaAutorizacao");
		save.saveOrUpdateManaged("listaDuplicata");
		save.saveOrUpdateManaged("listaAjustefiscal");
		save.saveOrUpdateManaged("listaRestricao");
//		save.saveOrUpdateManaged("listaItens"); FOI RETIRADO ISSO POIS AGORA SALVA NO SAVE OR UPDATE DO SERVICE ESTA LISTA 
		
		//Somente salvar a lista de hist�rico se houver itens nela.
		if (bean.getListaNotaHistorico() != null && !bean.getListaNotaHistorico().isEmpty()) {
			save.saveOrUpdateManaged("listaNotaHistorico");
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Notafiscalproduto> query) {
		
		query	
			.select("notafiscalproduto.cdNota, notafiscalproduto.retornocanhoto, notafiscalproduto.impostocumulativoinss, notafiscalproduto.impostocumulativoiss, notafiscalproduto.impostocumulativoir, notafiscalproduto.hremissao, " +
					"notafiscalproduto.impostocumulativoicms, notafiscalproduto.impostocumulativopis, notafiscalproduto.impostocumulativocofins, notafiscalproduto.impostocumulativocsll, " +
					"notaStatus.cdNotaStatus, notaStatus.nome, notafiscalproduto.numero, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.inscricaoestadual, notafiscalproduto.modeloDocumentoFiscalEnum, " +
					"empresa.cnpj, logomarca.cdarquivo, empresa.inscricaomunicipal, notafiscalproduto.dtEmissao, notafiscalproduto.datahorasaidaentrada, notafiscalproduto.valordesconto, " +
					"notafiscalproduto.formapagamentonfe, projeto.cdprojeto, cliente.cdpessoa, notafiscalproduto.localdestinonfe, notafiscalproduto.operacaonfe, notafiscalproduto.presencacompradornfe, " +
					"cliente.razaosocial, cliente.nome, cliente.email, cliente.cpf, cliente.cnpj, cliente.inscricaoestadual, cliente.inscricaosuframa, notafiscalproduto.telefoneCliente, enderecoCliente.cdendereco, " +
					"notafiscalproduto.situacaoExpedicao,notafiscalproduto.valorprodutos, notafiscalproduto.valorfrete, notafiscalproduto.valornaoincluido, notafiscalproduto.valordesconto, notafiscalproduto.valorseguro, notafiscalproduto.outrasdespesas, " +
					"notafiscalproduto.valorbcicms, notafiscalproduto.valoricms, notafiscalproduto.valorbcicmsst, notafiscalproduto.valoricmsst, " +
					"notafiscalproduto.valoripi, notafiscalproduto.valorii, notafiscalproduto.valorpis, notafiscalproduto.valorcofins, notafiscalproduto.retencaoICMSSTUFdestino, " +
					"notafiscalproduto.valorcsll, notafiscalproduto.valorbcirrf, notafiscalproduto.valorirrf, notafiscalproduto.diferencaduplicata, " +
					"notafiscalproduto.valorbcprevsocial, notafiscalproduto.valorprevsocial, notafiscalproduto.valorservicos, notafiscalproduto.recopi, " +
					"notafiscalproduto.valorbciss, notafiscalproduto.valoriss, notafiscalproduto.valorpisservicos, notafiscalproduto.valorcofinsservicos, " +
					"notafiscalproduto.valorfcp, notafiscalproduto.valorfcpretidost, notafiscalproduto.valorfcpretidostanteriormente, " +
					"notafiscalproduto.valorbcfcpdestinatario, notafiscalproduto.valoripidevolvido, notafiscalproduto.origemCupom, " +
					"notafiscalproduto.valor, transportador.cdpessoa, enderecotransportador.cdendereco, notafiscalproduto.telefonetransportador, transportador.razaosocial, " +
					"transportador.cpf, transportador.cnpj, transportador.inscricaoestadual, notafiscalproduto.responsavelfrete, notafiscalproduto.transporteveiculo, transportador.nome, transportador.email, " +
					"notafiscalproduto.placaveiculo, ufveiculo.cduf, ufveiculo.cdibge, notafiscalproduto.rntc, notafiscalproduto.infoadicionalfisco, notafiscalproduto.infoadicionalcontrib, " +
					"documentotipo.cddocumentotipo, documentotipo.nome, " +
					"material.cdmaterial, material.nome, material.nomenf, material.identificacao, unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, item.gtin, item.qtde, item.valorunitario, item.valorbruto," +
					"item.cdnotafiscalprodutoitem, item.valorfrete, item.valordesconto,item.valorseguro, item.outrasdespesas, item.tipocobrancaicms, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, " +
					"item.icmsst, item.valoricmsst, item.qtdeseloipi, item.codigoseloipi, item.tipocobrancaipi, item.valorbcipi, item.ipi, item.valoripi, " +
					"item.tipocobrancapis, item.valorbcpis, item.pis, item.valorpis, item.tipocobrancacofins, item.valorbccofins, item.cofins, item.valorcofins, " +
					"item.tipotributacaoiss, item.valorbciss, item.iss, item.valoriss, itemlistaservico.cditemlistaservico, itemlistaservico.codigo, itemlistaservico.descricao, item.recopi, item.cbenef, " +
					"item.valorIcmsPropSub, item.redBaseCalcEfetiva, item.valorBaseCalcEfetiva, item.icmsEfetiva, item.valorIcmsEfetiva, item.numeroFci, " +
					"municipioiss.cdmunicipio, municipioiss.nome, ufiss.sigla, item.origemproduto, item.numeropedidocompra, item.itempedidocompra, " +
					"item.infoadicional, municipiogerador.cdmunicipio, " +
					"item.dadosexportacao, item.drawbackexportacao, item.numregistroexportacao, item.valorbcfcpst, item.fcpst, item.valorfcpst, item.chaveacessoexportacao, item.quantidadeexportacao, " +
					"municipiogerador.nome, ufgerador.sigla, notafiscalproduto.qtdevolume, notafiscalproduto.espvolume, notafiscalproduto.marcavolume, " +
					"notafiscalproduto.numvolume, notafiscalproduto.pesoliquido, notafiscalproduto.pesobruto, cfop.cdcfop, cfop.codigo, cfop.descricaoresumida, " +
					"ncmcapitulo.cdncmcapitulo, prazopagamentofatura.cdprazopagamento, notafiscalproduto.cadastrarcobranca, " +
					"notafiscalproduto.numerofatura, notafiscalproduto.valororiginalfatura, notafiscalproduto.valordescontofatura, notafiscalproduto.valorliquidofatura, " +
					"item.ncmcompleto, item.valordesconto, notafiscalproduto.tipooperacaonota, notafiscalproduto.finalidadenfe, notafiscalproduto.cadastrarreferencia, " +
					"notafiscalproduto.tipopessoaentrega, notafiscalproduto.cnpjentrega, notafiscalproduto.cpfentrega, notafiscalproduto.logradouroentrega, " +
					"notafiscalproduto.numeroentrega, notafiscalproduto.complementoentrega, notafiscalproduto.bairroentrega, " +
					"notafiscalproduto.telefoneEntrega, notafiscalproduto.emailEntrega, notafiscalproduto.inscricaoEstadualEntrega, paisRetirada.cdpais, paisRetirada.cdbacen, paisRetirada.nome, " +
					"clienteEntrega.cdpessoa, clienteEntrega.nome, clienteEntrega.razaosocial, clienteEntrega.cnpj, clienteEntrega.cpf, paisEntrega.cdpais, paisEntrega.cdbacen, paisEntrega.nome, " +
					"notafiscalproduto.tipopessoaretirada, notafiscalproduto.cnpjretirada, notafiscalproduto.cpfretirada, notafiscalproduto.logradouroretirada, notafiscalproduto.emailRetirada, " +
					"fornecedorRetirada.cdpessoa, fornecedorRetirada.nome, fornecedorRetirada.razaosocial, " +
					"notafiscalproduto.numeroretirada, notafiscalproduto.complementoretirada, notafiscalproduto.bairroretirada, notafiscalproduto.telefoneRetirada, notafiscalproduto.inscricaoEstadualRetirada, " +
					"paisEntrega.cdpais, paisEntrega.cdbacen, paisEntrega.nome, " +
					"municipioentrega.cdmunicipio, municipioentrega.nome, ufentrega.cduf, ufentrega.sigla, " +
					"municipioretirada.cdmunicipio, municipioretirada.nome, ufretirada.cduf, ufretirada.sigla, notafiscalproduto.enderecodiferenteentrega, " +
					"notafiscalproduto.enderecodiferenteretirada, item.extipi, item.incluirvalorprodutos, item.tributadoicms, item.tipocalculopis, item.aliquotareaispis, " +
					"item.qtdevendidapis, item.tipocalculocofins, item.aliquotareaiscofins, item.qtdevendidacofins, item.modalidadebcicms, item.cest, item.modalidadebcicmsst, item.reducaobcicmsst, " +
					"item.margemvaloradicionalicmsst, item.reducaobcicms, item.bcoperacaopropriaicms, uficmsst.sigla, uficmsst.cduf, item.motivodesoneracaoicms, item.valordesoneracaoicms, item.abaterdesoneracaoicms, item.percentualdesoneracaoicms, item.tipotributacaoicms, " +
					"item.aliquotacreditoicms, item.valorcreditoicms, item.cnpjprodutoripi, item.tipocalculoipi, item.aliquotareaisipi, item.qtdevendidaipi, item.codigoenquadramentoipi, item.incluirissvalor, " +
					"item.incluiricmsvalor, item.valorbcfcp, item.incluiripivalor, item.incluirpisvalor, item.incluircofinsvalor, notafiscalproduto.valorpisretido, notafiscalproduto.valorcofinsretido, " +
					"notafiscalproduto.valorbccsll, notafiscalproduto.cadastrartransportador, notafiscalproduto.totalcalculado, " +
					"codigocnae.cdcodigocnae, codigocnae.cnae, codigocnae.descricaocnae, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
					"item.valorbcii, item.despesasaduaneiras, item.valoriof, item.valorii, item.dadosimportacao, materialclasse.cdmaterialclasse, materialclasse.nome, item.nve, " +
					"item.valorbccsll, item.csll, item.valorcsll, item.valorbcir, item.ir, item.valorir, item.valorbcinss, item.inss, item.valorinss, item.fcp, item.valorfcp, " +
					"materialgrupo.cdmaterialgrupo, " +
					"declaracaoimportacao.cddeclaracaoimportacao, declaracaoimportacao.numerodidsida, declaracaoimportacao.dtregistro, " +
					"loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, " +
					"codigoanp.cdcodigoanp, codigoanp.codigo, ufconsumo.cduf, ufconsumo.sigla, " +
					"item.dadoscombustivel, item.codif, item.qtdetemperatura, item.percentualglp, item.percentualgnn, item.percentualgni, item.valorpartida, item.qtdebccide, item.valoraliqcide, item.valorcide, item.percentualimpostoecf, " +
					"item.pesobruto, item.pesoliquido, item.valorFiscalIcms, item.valorFiscalIpi, item.valortotaltributos, notafiscalproduto.valortotaltributos, " +
					"notafiscalproduto.valorTotalImpostoFederal, notafiscalproduto.valorTotalImpostoEstadual, notafiscalproduto.valorTotalImpostoMunicipal, " +
					"item.percentualImpostoFederal, item.percentualImpostoEstadual, item.percentualImpostoMunicipal, " +
					"item.valorImpostoFederal, item.valorImpostoEstadual, item.valorImpostoMunicipal, " +
					"materialgrupo.cdmaterialgrupo, notafiscalproduto.numeronotaempenho, notafiscalproduto.numerocontrato, " +
					"notafiscalproduto.numeropedido, naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome, naturezaoperacao.descricao, naturezaoperacao.simplesremessa, notafiscalproduto.naturezaoperacaoantigo," +
					"empresa.email, patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, material.patrimonio," +
					"notafiscalproduto.enderecoavulso, endereconota.cdendereco, endereconota.cep, endereconota.logradouro, endereconota.bairro, endereconota.numero, endereconota.complemento, endereconota.caixapostal, " +
					"ufnota.cduf, ufnota.sigla, ufnota.nome, municipionota.cdmunicipio, municipionota.nome," +
					"grupotributacao.cdgrupotributacao, grupotributacao.nome, materialtipo.cdmaterialtipo, material.percentualimpostoecf, item.identificadorespecifico, " +
					"ufembarque.cduf, ufembarque.sigla, notafiscalproduto.localembarque, notafiscalproduto.cadastrarautorizacaoadicional, notafiscalproduto.valordesoneracao, " +
					"item.percentualdevolvido, item.valoripidevolvido, item.valordeducaoservico, item.valoroutrasretencoesservico, item.valordescontocondicionadoservico, " +
					"item.valordescontoincondicionadoservico, item.valorissretido, item.exigibilidadeiss, item.codigoservico, item.processoservico, item.incentivofiscal, " +
					"municipioservico.cdmunicipio, municipioservico.nome, ufservico.sigla, paisservico.cdpais, " +
					"notafiscalproduto.dtcompetenciaservico, notafiscalproduto.valordeducaoservico, notafiscalproduto.valoroutrasretencoesservico, notafiscalproduto.valordescontocondicionadoservico, " +
					"notafiscalproduto.valordescontoincondicionadoservico, notafiscalproduto.valorissretido, regimetributacao.cdregimetributacao, " +
					"listaAjustefiscal.cdajustefiscal, listaAjustefiscal.codigoajuste, listaAjustefiscal.descricaocomplementar, listaAjustefiscal.aliquotaicms, " +
					"listaAjustefiscal.basecalculoicms, listaAjustefiscal.valoricms, listaAjustefiscal.outrosvalores, obslancamentofiscal.cdobslancamentofiscal, obslancamentofiscal.descricao, " +
					"materialajuste.cdmaterial, materialajuste.nome, materialajuste.identificacao, " +
					"contaboleto.cdconta, contacarteira.cdcontacarteira, contacarteira.carteira, contacarteira.variacaocarteira, vendamaterial.cdvendamaterial, vendamaterial.ordem, venda.cdvenda, venda.dtvenda, " +
					"vendamaterialmestre.cdvendamaterialmestre, vendamaterialmestre.nomealternativo, pedidovendamaterial.cdpedidovendamaterial, pedidovendamaterial.ordem, " +
					"listaRestricao.cdnotafiscalprodutorestricao, usuarioRestricao.cdpessoa, usuarioRestricao.nome, " +
					"item.dadosicmspartilha, item.valorbcdestinatario, item.valorbcfcpdestinatario, item.fcpdestinatario, item.icmsdestinatario, item.icmsinterestadual, item.icmsinterestadualpartilha, " +
					"item.valorfcpdestinatario, item.valoricmsdestinatario, item.valoricmsremetente, " +
					"notafiscalproduto.valorfcpdestinatario, notafiscalproduto.valoricmsdestinatario, notafiscalproduto.valoricmsremetente," +
					"listaMaterialnumeroserie.numero, materialnumeroserie.numero, notafiscalproduto.serienfe, notafiscalproduto.razaosocialalternativa, " +
					"pneu.cdpneu, empresa.considerarIdPneu, item.valorbcicmsstremetente, item.valoricmsstremetente, item.valorbcicmsstdestinatario, item.valoricmsstdestinatario, " +
					"emporiumVenda.cdemporiumvenda")
					
			.leftOuterJoin("notafiscalproduto.codigocnae codigocnae")
			.leftOuterJoin("notafiscalproduto.municipioentrega municipioentrega")
			.leftOuterJoin("municipioentrega.uf ufentrega")
			.leftOuterJoin("notafiscalproduto.municipioretirada municipioretirada")
			.leftOuterJoin("municipioretirada.uf ufretirada")
			.leftOuterJoin("notafiscalproduto.fornecedorRetirada fornecedorRetirada")
			.leftOuterJoin("notafiscalproduto.paisRetirada paisRetirada")
			.leftOuterJoin("notafiscalproduto.clienteEntrega clienteEntrega")
			.leftOuterJoin("notafiscalproduto.paisEntrega paisEntrega")
			.leftOuterJoin("notafiscalproduto.notaTipo notaTipo")
			.leftOuterJoin("notafiscalproduto.regimetributacao regimetributacao")
			.leftOuterJoin("notafiscalproduto.notaStatus notaStatus")
			.leftOuterJoin("notafiscalproduto.prazopagamentofatura prazopagamentofatura")
			.leftOuterJoin("notafiscalproduto.ufembarque ufembarque")
			.leftOuterJoin("notafiscalproduto.cliente cliente")
			.leftOuterJoin("notafiscalproduto.documentotipo documentotipo")
			.leftOuterJoin("notafiscalproduto.listaItens item")
			.leftOuterJoin("item.patrimonioitem patrimonioitem")
			.leftOuterJoin("patrimonioitem.materialnumeroserie materialnumeroserie")
			.leftOuterJoin("item.unidademedida unidademedida")
			.leftOuterJoin("item.codigoanp codigoanp")
			.leftOuterJoin("item.ufconsumo ufconsumo")
			.leftOuterJoin("item.cfop cfop")
			.leftOuterJoin("item.material material")
			.leftOuterJoin("material.listaMaterialnumeroserie listaMaterialnumeroserie")
			.leftOuterJoin("item.materialclasse materialclasse")
			.leftOuterJoin("item.itemlistaservico itemlistaservico")
			.leftOuterJoin("item.ncmcapitulo ncmcapitulo")
			.leftOuterJoin("item.uficmsst uficmsst")
			.leftOuterJoin("item.loteestoque loteestoque")
			.leftOuterJoin("item.vendamaterial vendamaterial")			
			.leftOuterJoin("vendamaterial.venda venda")
			.leftOuterJoin("item.vendamaterialmestre vendamaterialmestre")
			.leftOuterJoin("item.pedidovendamaterial pedidovendamaterial")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			.leftOuterJoin("material.materialtipo materialtipo")

			.leftOuterJoin("item.listaNotafiscalprodutoitemdi notafiscalprodutoitemdi")
			.leftOuterJoin("notafiscalprodutoitemdi.declaracaoimportacao declaracaoimportacao")
			
			.leftOuterJoin("cfop.cfoptipo cfoptipo")
			.leftOuterJoin("cfop.cfopescopo cfopescopo")
			.leftOuterJoin("notafiscalproduto.municipiogerador municipiogerador")
			.leftOuterJoin("municipiogerador.uf ufgerador")
			.leftOuterJoin("item.municipioservico municipioservico")
			.leftOuterJoin("municipioservico.uf ufservico")
			.leftOuterJoin("item.paisservico paisservico")
			.leftOuterJoin("item.municipioiss municipioiss")
			.leftOuterJoin("municipioiss.uf ufiss")
			.leftOuterJoin("notafiscalproduto.projeto projeto")
			.leftOuterJoin("notafiscalproduto.empresa empresa")
			.leftOuterJoin("empresa.logomarca logomarca")
			.leftOuterJoin("notafiscalproduto.enderecoCliente enderecoCliente")
			.leftOuterJoin("notafiscalproduto.transportador transportador")
			.leftOuterJoin("notafiscalproduto.enderecotransportador enderecotransportador")
			.leftOuterJoin("notafiscalproduto.responsavelfrete responsavelfrete")
			.leftOuterJoin("notafiscalproduto.ufveiculo ufveiculo")
			.leftOuterJoin("notafiscalproduto.emporiumVenda emporiumVenda")
//			.leftOuterJoin("notafiscalproduto.listaNotaHistorico historico")
//			.leftOuterJoin("historico.notaStatus notaStatusHistorico")
			.leftOuterJoin("item.localarmazenagem localarmazenagem")
			.leftOuterJoin("notafiscalproduto.naturezaoperacao naturezaoperacao")
			.leftOuterJoin("item.grupotributacao grupotributacao")
			
			.leftOuterJoin("notafiscalproduto.endereconota endereconota")
			.leftOuterJoin("endereconota.municipio municipionota")
			.leftOuterJoin("municipionota.uf ufnota")
			.leftOuterJoin("notafiscalproduto.listaAjustefiscal listaAjustefiscal")
			.leftOuterJoin("listaAjustefiscal.obslancamentofiscal obslancamentofiscal")
			.leftOuterJoin("listaAjustefiscal.material materialajuste")
			.leftOuterJoin("notafiscalproduto.contaboleto contaboleto")
			.leftOuterJoin("notafiscalproduto.contacarteira contacarteira")
			.leftOuterJoin("notafiscalproduto.listaRestricao listaRestricao")
			.leftOuterJoin("listaRestricao.usuario usuarioRestricao")
			
			.leftOuterJoin("item.pneu pneu")
			
			.orderBy("material.nome, cfop.codigo");
	}

	/**
	 * M�todo de refer�ncia ao DAO. Carrega as informa��es necess�rias para
	 * calcular o total de cada nota fiscal de Produto.
	 * 
	 * @param listaNfProduto
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Nota> carregarInfoParaCalcularTotal(List<Nota> listaNfProduto) {
		if (listaNfProduto == null || listaNfProduto.isEmpty()) {
			return new ArrayList<Nota>();
		}
		
		return new ArrayList<Nota>(carregarInfoParaCalcularTotalProduto(CollectionsUtil.listAndConcatenate(listaNfProduto, "cdNota", ","), null, null));
	}
	
	public List<Notafiscalproduto> carregarInfoParaCalcularTotalProduto(String whereIn, String orderBy, Boolean asc) {
		QueryBuilder<Notafiscalproduto> query = query();
		
		query
			.select("notafiscalproduto.cdNota, notafiscalproduto.notaTipo, notafiscalproduto.dtEmissao, notafiscalproduto.datahorasaidaentrada, " +
					"notafiscalproduto.numero, notafiscalproduto.valor, notafiscalproduto.valorprodutos, notaStatus.cdNotaStatus, notaStatus.nome, " +
					"cliente.nome, projeto.nome, cliente.cpf, cliente.cnpj, " + 
					"notadocumento.cdNotaDocumento, notadocumento.ordem, documento.cddocumento, documento.numero, documentoacao.cddocumentoacao," +
					"contaboleto.cdconta, documentotipo.cddocumentotipo")
			.leftOuterJoin("notafiscalproduto.listaItens produtoItem")
			.leftOuterJoin("notafiscalproduto.projeto projeto")
			.leftOuterJoin("notafiscalproduto.cliente cliente")
			.leftOuterJoin("notafiscalproduto.notaStatus notaStatus")			
			.leftOuterJoin("notafiscalproduto.listaNotaDocumento notadocumento")
			.leftOuterJoin("notadocumento.documento documento")
			.leftOuterJoin("documento.documentoacao documentoacao")
			.leftOuterJoin("notafiscalproduto.contaboleto contaboleto")
			.leftOuterJoin("notafiscalproduto.documentotipo documentotipo")
			.orderBy("notafiscalproduto.dtEmissao desc");
		
		SinedUtil.quebraWhereIn("notafiscalproduto.cdNota", whereIn, query);
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("notafiscalproduto.dtEmissao desc");
		}
		
		return query.list();
	}

//	/**
//	 * Verifica se o cd passado por par�metro � de uma nota fiscal de produto.
//	 *
//	 * @param cdNota
//	 * @return
//	 * @author Rodrigo Freitas
//	 */
//	public boolean isNotafiscalproduto(int cdNota) {
//		return newQueryBuilderSined(Long.class)
//					.select("count(*)")
//					.setUseTranslator(false)
//					.from(Notafiscalproduto.class, "nf")
//					.where("nf.cdNota = ?", cdNota)
//					.unique() > 0;
//	}

	/**
	 * Carrega informa��es necess�rias para a NFe.
	 *
	 * @param cdNota
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Notafiscalproduto loadForNfe(Integer cdNota) {
		return query()
					.select("notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.dtEmissao, notafiscalproduto.formapagamentonfe, notafiscalproduto.datahorasaidaentrada, notafiscalproduto.modeloDocumentoFiscalEnum, " +
							"notafiscalproduto.valorbcicms, notafiscalproduto.valoricms, notafiscalproduto.valorbcicmsst, notafiscalproduto.valoricmsst, notafiscalproduto.valorprodutos, notafiscalproduto.valorfrete, " +
							"notafiscalproduto.valornaoincluido, notafiscalproduto.valordesconto, notafiscalproduto.valorseguro, notafiscalproduto.recopi, notafiscalproduto.numerofatura, " +
							"notafiscalproduto.valoripi, notafiscalproduto.valorcofins, notafiscalproduto.outrasdespesas, notafiscalproduto.valor, notafiscalproduto.valorpis, notafiscalproduto.valorcofins, " +
							"notafiscalproduto.valorcsll, notafiscalproduto.valorirrf, notafiscalproduto.valorbcirrf, notafiscalproduto.valorbcprevsocial, " +
							"notafiscalproduto.valorprevsocial, notafiscalproduto.transporteveiculo, notafiscalproduto.placaveiculo, notafiscalproduto.rntc, notafiscalproduto.telefoneCliente, " +
							"notafiscalproduto.infoadicionalfisco, notafiscalproduto.infoadicionalcontrib, notafiscalproduto.valorii, notafiscalproduto.retencaoICMSSTUFdestino, " +
							"notafiscalproduto.localdestinonfe, notafiscalproduto.operacaonfe, notafiscalproduto.presencacompradornfe, notafiscalproduto.hremissao, " +
							"codigocnae.cnae, ufveiculo.cduf, ufveiculo.sigla, " +
							"enderecocliente.cdendereco, enderecotransportador.cdendereco, " +
							"empresa.cdpessoa, empresa.cnpj, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
							"cliente.cdpessoa, cliente.tipopessoa, cliente.cpf, cliente.cnpj, cliente.razaosocial, cliente.nome, cliente.inscricaoestadual, cliente.email, cliente.passaporte, cliente.inscricaosuframa, " +
							"transportador.cdpessoa, transportador.tipopessoa, transportador.cpf, transportador.cnpj, transportador.razaosocial, transportador.nome, " +
							"transportador.inscricaoestadual, responsavelfrete.cdResponsavelFrete, responsavelfrete.cdnfe, " +
							"municipioiss.cdibge, municipiogerador.cdibge, " +
							"material.cdmaterial, material.nome,  material.nomenf, material.servico, material.kitflexivel, material.codigoAnvisa, material.valorvendamaximo, " +
							"material.codigoIsencao, material.isento, material.codigoIsencao, " +
							"item.gtin, unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, item.qtde, item.valorunitario, item.valorfrete, item.valordesconto,item.valorseguro, item.valorbruto," +
							"item.outrasdespesas, item.tipocobrancaicms, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, item.icmsst, item.valoricmsst, item.numeroFci, " +
							"item.codigoseloipi, item.qtdeseloipi, item.tipocobrancaipi, item.valorbcipi, item.ipi, item.valoripi, item.tipocobrancapis, item.valorbcpis, item.fcp, item.valorfcp," +
							"item.valorbcfcpst, item.fcpst, item.valorfcpst, " +
							"item.pis, item.valorpis, item.tipocobrancacofins, item.valorbccofins, item.cofins, item.valorcofins, item.numeropedidocompra, item.itempedidocompra, " +
							"item.valorbciss, item.iss, item.valoriss, item.tipotributacaoiss, item.infoadicional, notafiscalproduto.qtdevolume, " +
							"notafiscalproduto.espvolume, notafiscalproduto.marcavolume, notafiscalproduto.numvolume, notafiscalproduto.pesoliquido, notafiscalproduto.pesobruto, cfop.cdcfop, cfop.codigo, cfoptipo.cdcfoptipo, " +
							"item.ncmcompleto, ncmcapitulo.cdncmcapitulo, notafiscalproduto.valororiginalfatura, notafiscalproduto.valordescontofatura, notafiscalproduto.valorliquidofatura, notafiscalproduto.cadastrarcobranca, " +
							"notafiscalproduto.tipooperacaonota, notafiscalproduto.finalidadenfe, notafiscalproduto.tipopessoaentrega, notafiscalproduto.cnpjentrega, notafiscalproduto.cpfentrega, notafiscalproduto.logradouroentrega, " +
							"notafiscalproduto.numeroentrega, notafiscalproduto.complementoentrega, notafiscalproduto.bairroentrega, notafiscalproduto.tipopessoaretirada, notafiscalproduto.cnpjretirada, notafiscalproduto.cpfretirada, " +
							"notafiscalproduto.logradouroretirada, notafiscalproduto.numeroretirada, notafiscalproduto.complementoretirada, notafiscalproduto.bairroretirada, municipioentrega.cdmunicipio, " +
							"ufentrega.cduf, municipioretirada.cdmunicipio, ufretirada.cduf, notafiscalproduto.enderecodiferenteentrega, notafiscalproduto.enderecodiferenteretirada, " +
							"municipioretirada.cdibge, municipioentrega.cdibge, municipioretirada.nome, municipioentrega.nome, ufretirada.sigla, ufentrega.sigla, " +
							"item.extipi, item.incluirvalorprodutos, item.tributadoicms, item.tipocalculopis, item.aliquotareaispis, " +
							"item.qtdevendidapis, item.tipocalculocofins, item.aliquotareaiscofins, item.qtdevendidacofins, item.modalidadebcicms, item.cest, item.modalidadebcicmsst, " +
							"item.reducaobcicmsst, item.margemvaloradicionalicmsst, item.reducaobcicms, item.bcoperacaopropriaicms, uficmsst.sigla, uficmsst.cdibge, " +
							"item.motivodesoneracaoicms, item.valordesoneracaoicms, item.abaterdesoneracaoicms, item.percentualdesoneracaoicms, item.tipotributacaoicms, item.aliquotacreditoicms, item.valorcreditoicms, item.cnpjprodutoripi, item.tipocalculoipi, " +
							"item.aliquotareaisipi, item.qtdevendidaipi, item.codigoenquadramentoipi, item.incluirissvalor, item.incluiricmsvalor, item.incluiripivalor, item.incluirpisvalor, " +
							"item.incluircofinsvalor, notafiscalproduto.valorpisretido, notafiscalproduto.valorcofinsretido, notafiscalproduto.valorbccsll, notafiscalproduto.cadastrartransportador, " +
							"itemlistaservico.codigo, item.valorbcii, item.despesasaduaneiras, item.valoriof, item.valorii, item.dadosimportacao, item.origemproduto, " +
							"declaracaoimportacao.cddeclaracaoimportacao, declaracaoimportacao.numerodidsida, declaracaoimportacao.dtregistro, " +
							"codigoanp.cdcodigoanp, codigoanp.codigo, codigoanp.produto, ufconsumo.cduf, ufconsumo.sigla, endereconota.cdendereco, notafiscalproduto.enderecoavulso, " +
							"item.dadoscombustivel, item.codif, item.qtdetemperatura, item.percentualglp, item.percentualgnn, item.percentualgni, item.valorpartida, item.qtdebccide, item.valoraliqcide, item.valorcide, " +
							"item.valortotaltributos, notafiscalproduto.valortotaltributos, notafiscalproduto.valorTotalImpostoFederal, notafiscalproduto.valorTotalImpostoEstadual, notafiscalproduto.valorTotalImpostoMunicipal, " +
							"notafiscalproduto.numeronotaempenho, notafiscalproduto.numerocontrato, notafiscalproduto.numeropedido," +
							"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome, naturezaoperacao.descricao, notafiscalproduto.valorbciss, notafiscalproduto.valoriss, notafiscalproduto.valorcofinsservicos, " +
							"notafiscalproduto.valorpisservicos, notafiscalproduto.valorservicos, materialgrupo.cdmaterialgrupo, materialgrupo.medicamento, materialgrupo.incluirLoteNaNfe, " +
							"notafiscalproduto.valorfcp, notafiscalproduto.valorfcpretidost, notafiscalproduto.valorfcpretidostanteriormente, " +
							"notafiscalproduto.valorbcfcpdestinatario, notafiscalproduto.valoripidevolvido, " +
							"grupotributacao.cdgrupotributacao, grupotributacao.nome, material.identificacao, item.identificadorespecifico, " +
							"notafiscalproduto.localembarque, ufembarque.cduf, ufembarque.sigla, " +
							"item.nve, item.recopi, item.cbenef, notafiscalproduto.valordesoneracao, cliente.inscricaomunicipal, cliente.contribuinteicmstipo, " +
							"item.percentualdevolvido, item.valoripidevolvido, item.valordeducaoservico, item.valoroutrasretencoesservico, item.valordescontocondicionadoservico, " +
							"item.valordescontoincondicionadoservico, item.valorissretido, item.exigibilidadeiss, item.codigoservico, item.processoservico, item.incentivofiscal, " +
							"municipioservico.cdmunicipio, municipioservico.nome, municipioservico.cdibge, ufservico.sigla, paisservico.cdpais, paisservico.cdbacen, " +
							"notafiscalproduto.dtcompetenciaservico, notafiscalproduto.valordeducaoservico, notafiscalproduto.valoroutrasretencoesservico, notafiscalproduto.valordescontocondicionadoservico, " +
							"notafiscalproduto.valordescontoincondicionadoservico, notafiscalproduto.valorissretido, regimetributacao.cdregimetributacao, regimetributacao.codigonfe, " +
							"cfopescopo.cdcfopescopo, item.dadosexportacao, item.drawbackexportacao, item.numregistroexportacao, item.chaveacessoexportacao, item.quantidadeexportacao, " +
							"materialgrupo.papelimune, vendamaterialmestre.nomealternativo, " +
							"item.dadosicmspartilha, item.valorbcdestinatario, item.valorbcfcpdestinatario, item.fcpdestinatario, item.icmsdestinatario, item.icmsinterestadual, item.icmsinterestadualpartilha, " +
							"item.valorfcpdestinatario, item.valoricmsdestinatario, item.valoricmsremetente, item.valorbcicmsstremetente, item.valorbcicmsstdestinatario, item.valoricmsstdestinatario, " +
							"notafiscalproduto.valorfcpdestinatario, notafiscalproduto.valoricmsdestinatario, notafiscalproduto.valoricmsremetente, notafiscalproduto.serienfe, notafiscalproduto.razaosocialalternativa, " +
							"paisEntrega.cdpais, paisEntrega.nome, paisEntrega.cdbacen, " +
							"clienteEntrega.cdpessoa, clienteEntrega.nome, clienteEntrega.razaosocial," +
							"notafiscalproduto.telefoneEntrega, notafiscalproduto.emailEntrega, notafiscalproduto.inscricaoEstadualEntrega, " +
							"paisRetirada.cdpais, paisRetirada.nome, paisRetirada.cdbacen, " +
							"fornecedorRetirada.cdpessoa, fornecedorRetirada.nome, fornecedorRetirada.razaosocial, " +
							"notafiscalproduto.telefoneRetirada, notafiscalproduto.emailRetirada, notafiscalproduto.inscricaoEstadualRetirada," +
							"item.valorIcmsPropSub, item.redBaseCalcEfetiva, item.valorBaseCalcEfetiva, item.icmsEfetiva, item.valorIcmsEfetiva, " +
							"loteestoque.numero, listaLotematerial.validade, listaLotematerial.fabricacao, listaLotematerial.codigoAgregacao, materiallote.cdmaterial ")
					.from(Notafiscalproduto.class)
					.leftOuterJoin("notafiscalproduto.codigocnae codigocnae")
					.leftOuterJoin("notafiscalproduto.ufembarque ufembarque")					
					.leftOuterJoin("notafiscalproduto.municipioentrega municipioentrega")
					.leftOuterJoin("municipioentrega.uf ufentrega")
					.leftOuterJoin("notafiscalproduto.paisEntrega paisEntrega")
					.leftOuterJoin("notafiscalproduto.clienteEntrega clienteEntrega")
					.leftOuterJoin("notafiscalproduto.municipioretirada municipioretirada")
					.leftOuterJoin("municipioretirada.uf ufretirada")
					.leftOuterJoin("notafiscalproduto.paisRetirada paisRetirada")
					.leftOuterJoin("notafiscalproduto.fornecedorRetirada fornecedorRetirada")
					.leftOuterJoin("notafiscalproduto.responsavelfrete responsavelfrete")
					.leftOuterJoin("notafiscalproduto.cliente cliente")
					.leftOuterJoin("notafiscalproduto.regimetributacao regimetributacao")
					.leftOuterJoin("notafiscalproduto.transportador transportador")
					.leftOuterJoin("notafiscalproduto.enderecoCliente enderecocliente")
					.leftOuterJoin("notafiscalproduto.endereconota endereconota")
					.leftOuterJoin("notafiscalproduto.enderecotransportador enderecotransportador")
					.leftOuterJoin("notafiscalproduto.ufveiculo ufveiculo")
					.leftOuterJoin("notafiscalproduto.municipiogerador municipiogerador")
					.leftOuterJoin("notafiscalproduto.empresa empresa")
					.leftOuterJoin("notafiscalproduto.listaItens item")
					.leftOuterJoin("item.codigoanp codigoanp")
					.leftOuterJoin("item.ufconsumo ufconsumo")
					.leftOuterJoin("item.cfop cfop")
					.leftOuterJoin("cfop.cfoptipo cfoptipo")
					.leftOuterJoin("cfop.cfopescopo cfopescopo")
					.leftOuterJoin("item.material material")
					.leftOuterJoin("material.materialgrupo materialgrupo")
					.leftOuterJoin("item.municipioservico municipioservico")
					.leftOuterJoin("municipioservico.uf ufservico")
					.leftOuterJoin("item.paisservico paisservico")
					.leftOuterJoin("item.ncmcapitulo ncmcapitulo")
					.leftOuterJoin("item.municipioiss municipioiss")
					.leftOuterJoin("item.unidademedida unidademedida")
					.leftOuterJoin("item.uficmsst uficmsst")
					.leftOuterJoin("item.itemlistaservico itemlistaservico")
					.leftOuterJoin("item.listaNotafiscalprodutoitemdi notafiscalprodutoitemdi")
					.leftOuterJoin("notafiscalprodutoitemdi.declaracaoimportacao declaracaoimportacao")
					.leftOuterJoin("notafiscalproduto.naturezaoperacao naturezaoperacao")
					.leftOuterJoin("item.grupotributacao grupotributacao")
					.leftOuterJoin("item.vendamaterialmestre vendamaterialmestre")
					.leftOuterJoin("item.loteestoque loteestoque")
					.leftOuterJoin("loteestoque.listaLotematerial listaLotematerial")
					.leftOuterJoin("listaLotematerial.material materiallote")
					.where("notafiscalproduto.cdNota = ?", cdNota)
					.orderBy("item.cdnotafiscalprodutoitem")
					.unique();
	}

	/**
	 * Busca os dados da nota para a emiss�o do DANFE
	 *
	 * @param itensSelecionados
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/11/2013
	 */
	public List<Notafiscalproduto> findForDanfe(String itensSelecionados) {
		QueryBuilder<Notafiscalproduto> query = query();
		updateEntradaQuery(query);
		
		query
			.select(query.getSelect().getValue() + ", listaReferenciada.chaveacesso, listaNotafiscalprodutoColeta.cdnotafiscalprodutocoleta ")
			.leftOuterJoinIfNotExists("notafiscalproduto.listaReferenciada listaReferenciada")
			.leftOuterJoin("notafiscalproduto.listaNotafiscalprodutoColeta listaNotafiscalprodutoColeta");
		
		return query
					.whereIn("notafiscalproduto.cdNota", itensSelecionados)
					.orderBy("notafiscalproduto.numero, notafiscalproduto.cdNota, material.nome, cfop.codigo")
					.list();
	}

	public String getNextNumero() {
		return query()
					.select("notafiscalproduto.cdNota, notafiscalproduto.numero")
					.where("notafiscalproduto.numero is not null")
					.where("notafiscalproduto.numero <> ''")
					.setMaxResults(1)
					.orderBy("notafiscalproduto.cdNota desc")
					.unique().getNumero();
	}

	public List<Notafiscalproduto> loadForReceita(String whereIn) {
		return query()
					.select("notafiscalproduto.cdNota, notafiscalproduto.valor, notafiscalproduto.valordescontofatura, notafiscalproduto.datahorasaidaentrada, " +
							"cliente.cdpessoa, cliente.nome, cliente.razaosocial, cliente.naoconsiderartaxaboleto, notafiscalproduto.numero, notafiscalproduto.dtEmissao, " +
							"documentotipo.cddocumentotipo, documentotipo.nome, documentotipoDup.cddocumentotipo, documentotipo.nome, " + 
							"prazopagamentofatura.cdprazopagamento, notafiscalproduto.cadastrarcobranca, " +
							"listaDuplicata.numero, listaDuplicata.dtvencimento, listaDuplicata.valor, empresa.cdpessoa, " +
							"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.simplesremessa," +
							"contaboleto.cdconta, contacarteira.cdcontacarteira, contacarteira.msgboleto2," +
							"projeto.cdprojeto, notaStatus.cdNotaStatus, enderecoNF.cdendereco, " +
							"listaNotavenda.cdnotavenda, listaNotaContrato.cdNotaContrato, contrato.cdcontrato, listaRateioitem.percentual, listaRateioitem.valor, " +
							"contagerencialRateio.cdcontagerencial, centrocustoRateio.cdcentrocusto, projetoRateio.cdprojeto, " +
							"enderecocobranca.cdendereco, endereco.cdendereco, enderecotipo.cdenderecotipo, contrato.dataparceladiautil, contrato.dataparcelaultimodiautilmes ")
					.leftOuterJoin("notafiscalproduto.prazopagamentofatura prazopagamentofatura")
					.leftOuterJoin("notafiscalproduto.documentotipo documentotipo")
					.leftOuterJoin("notafiscalproduto.empresa empresa")
					.leftOuterJoin("notafiscalproduto.naturezaoperacao naturezaoperacao")
					.leftOuterJoin("notafiscalproduto.listaDuplicata listaDuplicata")
					.leftOuterJoin("listaDuplicata.documentotipo documentotipoDup")
					.leftOuterJoin("notafiscalproduto.contaboleto contaboleto")
					.leftOuterJoin("notafiscalproduto.contacarteira contacarteira")
					.leftOuterJoin("notafiscalproduto.projeto projeto")
					.leftOuterJoin("notafiscalproduto.notaStatus notaStatus")
					.leftOuterJoin("notafiscalproduto.cliente cliente")
					.leftOuterJoin("cliente.listaEndereco endereco")
					.leftOuterJoin("endereco.enderecotipo enderecotipo")
					.leftOuterJoin("notafiscalproduto.enderecoCliente enderecoNF")
					.leftOuterJoin("notafiscalproduto.listaNotaContrato listaNotaContrato")
					.leftOuterJoin("listaNotaContrato.contrato contrato")
					.leftOuterJoin("contrato.rateio rateio")
					.leftOuterJoin("contrato.enderecocobranca enderecocobranca")
					.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
					.leftOuterJoin("listaRateioitem.contagerencial contagerencialRateio")
					.leftOuterJoin("listaRateioitem.centrocusto centrocustoRateio")
					.leftOuterJoin("listaRateioitem.projeto projetoRateio")
					.leftOuterJoin("notafiscalproduto.listaNotavenda listaNotavenda")
					.whereIn("notafiscalproduto.cdNota", whereIn)
					.orderBy("notafiscalproduto.cdNota, listaDuplicata.dtvencimento")
					.list();
	}

	public Notafiscalproduto findByArquivonfnota(Arquivonfnota arquivonfnota) {
		return query()
					.select("notafiscalproduto.cdNota, cliente.cdpessoa, cliente.email, cliente.nome, empresa.cdpessoa, empresa.email")
					.join("notafiscalproduto.listaArquivonfnota listaArquivonfnota")
					.join("notafiscalproduto.cliente cliente")
					.leftOuterJoin("notafiscalproduto.empresa empresa")
					.where("listaArquivonfnota = ?", arquivonfnota)
					.unique();
	}

	/**
	 * Busca as notas para o registro C100 do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Notafiscalproduto> findForSpedRegC100(SpedarquivoFiltro filtro) {
		if(filtro != null) {
			QueryBuilder<Notafiscalproduto> query = querySined()
					
					.select("notafiscalproduto.tipooperacaonota, cliente.cdpessoa, notaStatus.cdNotaStatus, empresa.cdpessoa, " +
							"notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.finalidadenfe, notafiscalproduto.dtEmissao, " +
							"notafiscalproduto.datahorasaidaentrada, notafiscalproduto.valor, notafiscalproduto.formapagamentonfe, notafiscalproduto.modeloDocumentoFiscalEnum, " +
							"notafiscalproduto.valordesconto, notafiscalproduto.valorprodutos, responsavelfrete.cdnfe, " +
							"notafiscalproduto.valorfrete, notafiscalproduto.valorseguro, notafiscalproduto.outrasdespesas, " +
							"notafiscalproduto.valorbcicms, notafiscalproduto.valoricms, notafiscalproduto.valorbcicmsst, notafiscalproduto.origemCupom, " +
							"notafiscalproduto.valoricmsst, notafiscalproduto.valoripi, notafiscalproduto.valorpis, notafiscalproduto.valorcofins, " +
							"notafiscalproduto.infoadicionalfisco, material.cdmaterial, material.identificacao, material.origemproduto, " +
							"unidademedida.cdunidademedida, item.qtde, item.valorbruto, item.valordesconto, item.tipocobrancaicms, " +
							"cfop.codigo, cfop.cdcfop, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, item.identificadorespecifico, " +
							"item.icmsst, item.valoricmsst, item.tipocobrancaipi, item.valorbcipi, item.ipi, item.valoripi, " +
							"item.tipocobrancapis, item.valorbcpis, item.pis, item.qtdevendidapis, item.aliquotareaispis, item.valorpis, " +
							"item.tipocobrancacofins, item.valorbccofins, item.cofins, item.qtdevendidacofins, item.aliquotareaiscofins, " +
							"item.valorcofins, item.valorfrete, item.valordesconto, item.valorseguro, item.outrasdespesas, " +
							"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome, uf.cduf, uf.sigla, " +
							"listaAjustefiscal.cdajustefiscal, " +
							"listaAjustefiscal.codigoajuste, " +
							"listaAjustefiscal.descricaocomplementar, listaAjustefiscal.aliquotaicms, " +
							"listaAjustefiscal.basecalculoicms, listaAjustefiscal.valoricms, " +
							"listaAjustefiscal.outrosvalores, " +
							"obslancamentofiscal.cdobslancamentofiscal, obslancamentofiscal.descricao, " +
							"materialajuste.cdmaterial, materialajuste.nome, materialajuste.identificacao, " +
							"enderecoCliente.cdendereco, enderecoCliente.inscricaoestadual, " +
							"notafiscalproduto.valorfcpdestinatario, notafiscalproduto.valoricmsdestinatario, notafiscalproduto.valoricmsremetente, ufGerador.sigla ")
					.join("notafiscalproduto.notaStatus notaStatus")
					.join("notafiscalproduto.empresa empresa")
					.join("notafiscalproduto.cliente cliente")
					.leftOuterJoin("notafiscalproduto.responsavelfrete responsavelfrete")
					.join("notafiscalproduto.listaItens item")
					.leftOuterJoin("item.cfop cfop")
					.join("item.material material")
					.join("material.unidademedida unidademedida")
					.leftOuterJoin("notafiscalproduto.enderecoCliente enderecoCliente")
					.leftOuterJoin("enderecoCliente.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("notafiscalproduto.naturezaoperacao naturezaoperacao")
					.leftOuterJoin("notafiscalproduto.listaAjustefiscal listaAjustefiscal")
					.leftOuterJoin("listaAjustefiscal.obslancamentofiscal obslancamentofiscal")
					.leftOuterJoin("listaAjustefiscal.material materialajuste")
					.leftOuterJoin("notafiscalproduto.listaArquivonfnota arquivonfnota")
					.leftOuterJoin("arquivonfnota.arquivonf arquivonf")
					.leftOuterJoin("notafiscalproduto.municipiogerador municipiogerador")
					.leftOuterJoin("municipiogerador.uf ufGerador")
					.where("empresa = ?", filtro.getEmpresa())
					.openParentheses()
						.where("notaStatus = ?", NotaStatus.NFE_EMITIDA)
						.or()
						.where("notaStatus = ?", NotaStatus.NFE_LIQUIDADA)
						.or()
						.where("notaStatus = ?", NotaStatus.NFE_DENEGADA)
						.or()
						.openParentheses()
							.where("notaStatus = ?", NotaStatus.CANCELADA)
							.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_SUCESSO)
							.where("arquivonfnota.chaveacesso is not null")
							.where("arquivonfnota.protocolonfe is not null")
							.where("arquivonfnota.dtprocessamento is not null")
						.closeParentheses()
					.closeParentheses();
			
			addWhereDtemissaoDtentrada(query, filtro.getDtinicio(), filtro.getDtfim());
			
			boolean entradas = filtro.getBuscarentradas() != null && filtro.getBuscarentradas();
			boolean saidas = filtro.getBuscarsaidas() != null && filtro.getBuscarsaidas();
			if(entradas && !saidas){
				query.where("notafiscalproduto.tipooperacaonota = ?", Tipooperacaonota.ENTRADA);
			} else if(!entradas && saidas){
				query.where("notafiscalproduto.tipooperacaonota = ?", Tipooperacaonota.SAIDA);
			} else if(!entradas && !saidas){
				query.where("1=0");
			}
			
			return query.list();
		} else return null;
	}
	
	private void addWhereDtemissaoDtentrada(QueryBuilder<Notafiscalproduto> query, Date dtinicio, Date dtfim) {
		query.openParentheses();
		if(parametrogeralService.getBoolean(Parametrogeral.SPED_DATA_EMISSAO)){
			query
				.where("notafiscalproduto.dtEmissao >= ?", dtinicio)
				.where("notafiscalproduto.dtEmissao <= ?", dtfim);
		} else {
			query
				.openParentheses()
					.where("notafiscalproduto.datahorasaidaentrada is not null")
					.where("notafiscalproduto.datahorasaidaentrada >= ?", SinedDateUtils.dateToTimestampInicioDia(dtinicio))
					.where("notafiscalproduto.datahorasaidaentrada <= ?", SinedDateUtils.dateToTimestampFinalDia(dtfim))
				.closeParentheses()
				.or()
				.openParentheses()
					.where("notafiscalproduto.datahorasaidaentrada is null")
					.where("notafiscalproduto.dtEmissao >= ?", dtinicio)
					.where("notafiscalproduto.dtEmissao <= ?", dtfim)
				.closeParentheses();
		}
		query.closeParentheses();
	}
	
	/**
	* M�todo que busca as notas para a integra��o SAGE
	*
	* @param filtro
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public List<Notafiscalproduto> findForSagefiscal(SagefiscalFiltro filtro) {
		boolean entradas = filtro.getBuscarentradas() != null && filtro.getBuscarentradas();
		boolean saidas = filtro.getBuscarsaidas() != null && filtro.getBuscarsaidas();
		
		if(!entradas && !saidas){
			return new ArrayList<Notafiscalproduto>();
		}
		
		QueryBuilder<Notafiscalproduto> query = querySageFiscal()
				.where("empresa = ?", filtro.getEmpresa())
				.openParentheses()
					.where("notaStatus = ?", NotaStatus.NFE_EMITIDA)
					.or()
					.where("notaStatus = ?", NotaStatus.NFE_LIQUIDADA)
					.or()
					.where("notaStatus = ?", NotaStatus.NFE_DENEGADA)
					.or()
					.openParentheses()
						.where("notaStatus = ?", NotaStatus.CANCELADA)
						.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_SUCESSO)
						.where("arquivonfnota.chaveacesso is not null")
						.where("arquivonfnota.protocolonfe is not null")
						.where("arquivonfnota.dtprocessamento is not null")
					.closeParentheses()
				.closeParentheses()
				.openParentheses()
					.openParentheses()
						.where("notafiscalproduto.datahorasaidaentrada is not null")
						.where("notafiscalproduto.datahorasaidaentrada >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getDtinicio()))
						.where("notafiscalproduto.datahorasaidaentrada <= ?", SinedDateUtils.dateToTimestampFinalDia(filtro.getDtfim()))
					.closeParentheses()
				.or()
					.openParentheses()
						.where("notafiscalproduto.datahorasaidaentrada is null")
						.where("notafiscalproduto.dtEmissao >= ?", filtro.getDtinicio())
						.where("notafiscalproduto.dtEmissao <= ?", filtro.getDtfim())
					.closeParentheses()
				.closeParentheses();
		
		
		if(entradas && !saidas){
			query.where("notafiscalproduto.tipooperacaonota = ?", Tipooperacaonota.ENTRADA);
		} else if(!entradas && saidas){
			query.where("notafiscalproduto.tipooperacaonota = ?", Tipooperacaonota.SAIDA);
		}
		
		return query.list();
	}
	
	public Notafiscalproduto loadForSagefiscal(Integer cdNota) {
		return querySageFiscal()
					.where("notafiscalproduto.cdNota = ?", cdNota)
					.unique();
	}
	
	private QueryBuilder<Notafiscalproduto> querySageFiscal() {
		return querySined()
				
				.select("notafiscalproduto.tipooperacaonota, cliente.cdpessoa, cliente.cpf, cliente.cnpj, notaStatus.cdNotaStatus, " +
						"notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.finalidadenfe, notafiscalproduto.dtEmissao, " +
						"notafiscalproduto.datahorasaidaentrada, notafiscalproduto.valor, notafiscalproduto.formapagamentonfe, " +
						"notafiscalproduto.valordesconto, notafiscalproduto.valorprodutos, responsavelfrete.cdResponsavelFrete, responsavelfrete.cdnfe, " +
						"notafiscalproduto.valorfrete, notafiscalproduto.valorseguro, notafiscalproduto.outrasdespesas, " +
						"notafiscalproduto.valorbcicms, notafiscalproduto.valoricms, notafiscalproduto.valorbcicmsst, " +
						"notafiscalproduto.valoricmsst, notafiscalproduto.valoripi, notafiscalproduto.valorpis, notafiscalproduto.valorcofins, " +
						"notafiscalproduto.valorirrf, notafiscalproduto.valorcsll, notafiscalproduto.valorbccsll, " +
						"notafiscalproduto.infoadicionalfisco, notafiscalproduto.infoadicionalcontrib, material.cdmaterial, material.origemproduto, " +
						"notafiscalproduto.valorfcpdestinatario, notafiscalproduto.valoricmsdestinatario, notafiscalproduto.valoricmsremetente, " +
						"unidademedidaIt.cdunidademedida, unidademedidaIt.simbolo, " +
						"unidademedida.cdunidademedida, unidademedida.simbolo, item.qtde, item.valorbruto, item.valordesconto, item.tipocobrancaicms, " +
						"cfop.codigo, cfop.cdcfop, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, item.valorunitario, " +
						"item.icmsst, item.valoricmsst, item.tipocobrancaipi, item.valorbcipi, item.ipi, item.valoripi, " +
						"item.tipocobrancapis, item.valorbcpis, item.pis, item.qtdevendidapis, item.aliquotareaispis, item.valorpis, item.origemproduto, " +
						"item.tipocobrancacofins, item.valorbccofins, item.cofins, item.qtdevendidacofins, item.aliquotareaiscofins, item.tipocobrancapis, item.tipocobrancacofins, " +
						"item.valorcofins, item.valorfrete, item.valordesconto, item.valorseguro, item.outrasdespesas, item.valorFiscalIcms, item.valorFiscalIpi, " +
						"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome, " +
						"listaAjustefiscal.cdajustefiscal, " +
						"listaAjustefiscal.codigoajuste, " +
						"listaAjustefiscal.descricaocomplementar, listaAjustefiscal.aliquotaicms, " +
						"listaAjustefiscal.basecalculoicms, listaAjustefiscal.valoricms, " +
						"listaAjustefiscal.outrosvalores, " +
						"obslancamentofiscal.cdobslancamentofiscal, obslancamentofiscal.descricao, " +
						"materialajuste.cdmaterial, materialajuste.nome, materialajuste.identificacao, " +
						
						"enderecoClienteCadastro.cdendereco, enderecoClienteCadastro.inscricaoestadual, paisClienteCadastro.cdpais, ufClienteCadastro.cduf, ufClienteCadastro.sigla, enderecotipoClienteCadastro.cdenderecotipo, " +
						"enderecoNota.cdendereco, enderecoNota.inscricaoestadual, paisNota.cdpais, ufNota.cduf, ufNota.sigla, " +
						"enderecoCliente.cdendereco, enderecoCliente.inscricaoestadual, paisCliente.cdpais, ufCliente.cduf, ufCliente.sigla, " +
						
						"notafiscalproduto.valorfcpdestinatario, notafiscalproduto.valoricmsdestinatario, notafiscalproduto.valoricmsremetente," +
						"municipiogerador.cdmunicipio, ufGerador.cduf, ufGerador.sigla, " +
						"notareferenciada.cdnotafiscalprodutoreferenciada, notareferenciada.tiponotareferencia, notareferenciada.chaveacesso, " +
						"grupotributacao.cdgrupotributacao, grupotributacao.tipocobrancaipi, grupotributacao.valorFiscalIcms, grupotributacao.valorFiscalIpi")
				.join("notafiscalproduto.notaStatus notaStatus")
				.join("notafiscalproduto.empresa empresa")
				.join("notafiscalproduto.cliente cliente")
				.leftOuterJoin("notafiscalproduto.responsavelfrete responsavelfrete")
				.leftOuterJoin("notafiscalproduto.listaItens item")
				.leftOuterJoin("item.cfop cfop")
				.leftOuterJoin("item.material material")
				.leftOuterJoin("item.unidademedida unidademedidaIt")
				.leftOuterJoin("item.grupotributacao grupotributacao")
				.leftOuterJoin("material.unidademedida unidademedida")
				
				.leftOuterJoin("notafiscalproduto.endereconota enderecoNota")
				.leftOuterJoin("enderecoNota.municipio municipioNota")
				.leftOuterJoin("enderecoNota.pais paisNota")
				.leftOuterJoin("municipioNota.uf ufNota")
				
				.leftOuterJoin("notafiscalproduto.enderecoCliente enderecoCliente")
				.leftOuterJoin("enderecoCliente.municipio municipioCliente")
				.leftOuterJoin("enderecoCliente.pais paisCliente")
				.leftOuterJoin("municipioCliente.uf ufCliente")
				
				.leftOuterJoin("cliente.listaEndereco enderecoClienteCadastro")
				.leftOuterJoin("enderecoClienteCadastro.enderecotipo enderecotipoClienteCadastro")
				.leftOuterJoin("enderecoClienteCadastro.municipio municipioClienteCadastro")
				.leftOuterJoin("enderecoClienteCadastro.pais paisClienteCadastro")
				.leftOuterJoin("municipioClienteCadastro.uf ufClienteCadastro")
				
				.leftOuterJoin("notafiscalproduto.naturezaoperacao naturezaoperacao")
				.leftOuterJoin("notafiscalproduto.listaReferenciada notareferenciada")
				.leftOuterJoin("notafiscalproduto.listaAjustefiscal listaAjustefiscal")
				.leftOuterJoin("listaAjustefiscal.obslancamentofiscal obslancamentofiscal")
				.leftOuterJoin("listaAjustefiscal.material materialajuste")
				.leftOuterJoin("notafiscalproduto.listaArquivonfnota arquivonfnota")
				.leftOuterJoin("arquivonfnota.arquivonf arquivonf")
				.leftOuterJoin("notafiscalproduto.municipiogerador municipiogerador")
				.leftOuterJoin("municipiogerador.uf ufGerador");
	}

	public List<Notafiscalproduto> findForSelect(String projetos) {
		return 
			query()
			.select("notaFiscalProduto.cdNota, notaFiscalProduto.dtEmissao, notaFiscalProduto.numero")
			.join("notaFiscalProduto.projeto projeto")
			.join("notaFiscalProduto.notaTipo notaTipo")
			.join("notaFiscalProduto.notaStatus notaStatus")
			.where("notaTipo = ?", NotaTipo.NOTA_FISCAL_PRODUTO)
			.where("notaStatus = ?", NotaStatus.EMITIDA)
			.whereIn("projeto.cdprojeto", projetos)
			.orderBy("notaFiscalProduto.dtEmissao desc")
			.list();
	}

	public List<Notafiscalproduto> findForReport(NotaFiltro filtro) {
		String whereIn = filtro.getWhereIn();
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Par�metro whereIn inv�lido.");
		}
		
		return 
		query()
			.select("notafiscalproduto.cdNota, notafiscalproduto.dtEmissao, notafiscalproduto.numero, notafiscalproduto.tipooperacaonota, cliente.cdpessoa, cliente.nome, " +
					"cliente.razaosocial, enderecoCliente.logradouro, enderecoCliente.numero, enderecoCliente.complemento, enderecoCliente.bairro, " +
					"municipio.nome, uf.sigla, enderecoCliente.cep, cliente.tipopessoa, cliente.cpf, cliente.cnpj, cliente.inscricaomunicipal, cliente.inscricaoestadual, " +
					"notafiscalproduto.datahorasaidaentrada, notafiscalproduto.valor, notafiscalproduto.valordesconto, notafiscalproduto.valorprodutos, notafiscalproduto.outrasdespesas," +
					"notafiscalproduto.valorbcicms, notafiscalproduto.valoricms, notafiscalproduto.valorbcicmsst, notafiscalproduto.valoricmsst," +
					"notafiscalproduto.valoripi, material.cdmaterial, material.nome, material.nomenf, unidademedida.cdunidademedida,notafiscalproduto.valorcsll,notafiscalproduto.valorbccsll, " +
					"item.qtde, item.valorbruto, item.valordesconto, item.tipocobrancaicms, item.valorbcicms, item.icms,  item.valoricms, notafiscalproduto.valorcofins, notafiscalproduto.valorcofinsretido, " +
					"item.valorbcicmsst, item.valoricmsst, item.icmsst, item.tipocobrancaipi, item.valorbcipi, item.ipi, item.valoripi, " +
					"item.tipocobrancapis, item.valorbcpis, item.pis, item.qtdevendidapis, item.aliquotareaispis, item.valorpis, item.tipocobrancacofins," +
					"item.valorbccofins, item.cofins, item.qtdevendidacofins, item.aliquotareaiscofins, item.valorcofins, item.valorunitario, notafiscalproduto.valorbciss, notafiscalproduto.valoriss," +
					"contrato.cdcontrato, item.iss, item.cofins, notafiscalproduto.valorbcprevsocial, notafiscalproduto.valorprevsocial, documento.cddocumento, " +
					"loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade," +
					"notaTipo.cdNotaTipo, notaTipo.nome, cfop.cdcfop, cfop.codigo, responsavelfrete.cdResponsavelFrete, responsavelfrete.nome, " +
					"unidademedidaIt.cdunidademedida, unidademedidaIt.nome, unidademedidaIt.simbolo, item.tipocobrancaicms, item.icms, notafiscalproduto.valorfrete, " +
					"notafiscalproduto.valorseguro, notafiscalproduto.outrasdespesas, notafiscalproduto.placaveiculo, " +
					"notafiscalproduto.formapagamentonfe, empresa.inscricaoestadualst," +
					"listaNotavenda.cdnotavenda, venda.cdvenda, colaborador.cdpessoa, colaborador.nome, item.icms, item.tipocobrancaicms," +
					"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome")
			.leftOuterJoin("notafiscalproduto.notaStatus notaStatus")
			.leftOuterJoin("notafiscalproduto.empresa empresa")
			.leftOuterJoin("notafiscalproduto.cliente cliente")
			.leftOuterJoin("notafiscalproduto.enderecoCliente enderecoCliente")
			.leftOuterJoin("enderecoCliente.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("notafiscalproduto.listaItens item")
			.leftOuterJoin("notafiscalproduto.listaNotaDocumento listaNotaDocumento")
			.leftOuterJoin("listaNotaDocumento.documento documento")
			.leftOuterJoin("notafiscalproduto.listaNotaContrato listaNotaContrato")
			.leftOuterJoin("listaNotaContrato.contrato contrato")
			.leftOuterJoin("item.unidademedida unidademedidaIt")
			.leftOuterJoin("item.material material")
			.leftOuterJoin("material.unidademedida unidademedida")
			.leftOuterJoin("item.loteestoque loteestoque")
			
			.leftOuterJoin("notafiscalproduto.notaTipo notaTipo")
			.leftOuterJoin("item.cfop cfop")
			.leftOuterJoin("notafiscalproduto.responsavelfrete responsavelfrete")
			.leftOuterJoin("notafiscalproduto.listaNotavenda listaNotavenda")
			.leftOuterJoin("listaNotavenda.venda venda")
			.leftOuterJoin("venda.colaborador colaborador")
			
			.leftOuterJoin("notafiscalproduto.naturezaoperacao naturezaoperacao")
			
			.whereIn("notafiscalproduto.cdNota", whereIn)
			.orderBy("item.cdnotafiscalprodutoitem")
			.list();
	}
	
	/**
	 * M�todo que busca notas pelo whereIn
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Notafiscalproduto> findByWhereIn(String whereIn) {
		return query()
			.select("notafiscalproduto.numero,notafiscalproduto.cdNota,notafiscalproduto.serienfe, notafiscalproduto.numero, notafiscalproduto.valor, notafiscalproduto.dtEmissao, notafiscalproduto.numero, listaNotaDocumento.cdNotaDocumento, " +
					"documento.cddocumento, documentoacao.cddocumentoacao, notaStatus.cdNotaStatus, notafiscalproduto.cadastrarcobranca, prazopagamentofatura.cdprazopagamento, notafiscalproduto.valorliquidofatura," +
					"notafiscalproduto.tipooperacaonota,coleta.cdcoleta," +
					"empresa.cdpessoa,empresa.nome," +
					"listaNotafiscalprodutoColeta.cdnotafiscalprodutocoleta," +
					"coleta.cdcoleta")
			.leftOuterJoin("notafiscalproduto.notaStatus notaStatus")
			.leftOuterJoin("notafiscalproduto.listaNotafiscalprodutoColeta listaNotafiscalprodutoColeta")
			.leftOuterJoin("listaNotafiscalprodutoColeta.coleta coleta")
			.leftOuterJoin("coleta.listaColetaMaterial listaColetaMaterial")
			.leftOuterJoin("notafiscalproduto.empresa empresa")
			.leftOuterJoin("notafiscalproduto.prazopagamentofatura prazopagamentofatura")
			.leftOuterJoin("notafiscalproduto.listaNotaDocumento listaNotaDocumento")
			.leftOuterJoin("listaNotaDocumento.documento documento")
			.leftOuterJoin("documento.documentoacao documentoacao")
			.whereIn("notafiscalproduto.id", whereIn)
			.orderBy("notafiscalproduto.cdNota,coleta.cdcoleta")
			.list();
	}
	
	/**
	 * Criado c�digo novo pelo fato do whereIn ficar muito grande e travar a query
	 * O c�digo anterior foi mantido com o mesmo nome do m�todo sem o "NEW"
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Notafiscalproduto> carregarInfoParaCalcularTotalNew(NotaFiltro filtro) {
		QueryBuilder<Notafiscalproduto> query = query();
		
		query
			.select("notafiscalproduto.cdNota, notafiscalproduto.notaTipo, notafiscalproduto.dtEmissao, " +
					"notafiscalproduto.numero, notafiscalproduto.valor, notaStatus.cdNotaStatus, notaStatus.nome, " +
					"cliente.nome, projeto.nome, cliente.cpf, cliente.cnpj, " + 
					"notadocumento.cdNotaDocumento, notadocumento.ordem, documento.cddocumento, documento.numero, documentoacao.cddocumentoacao, naturezaoperacao.nome")
//			.leftOuterJoin("notafiscalproduto.listaItens produtoItem")
			.leftOuterJoin("notafiscalproduto.projeto projeto")
			.leftOuterJoin("notafiscalproduto.empresa empresa")
			.leftOuterJoin("notafiscalproduto.cliente cliente")
			.leftOuterJoin("notafiscalproduto.notaTipo notaTipo")
			.leftOuterJoin("notafiscalproduto.notaStatus notaStatus")			
			.leftOuterJoin("notafiscalproduto.listaNotaDocumento notadocumento")
			.leftOuterJoin("notadocumento.documento documento")
			.leftOuterJoin("documento.documentoacao documentoacao")
			.leftOuterJoin("notafiscalproduto.naturezaoperacao naturezaoperacao")
//			.leftOuterJoin("notafiscalproduto.listaItens item")
//			.leftOuterJoin("item.material material")
			;
		
		if(filtro.getMaterial() != null || filtro.getCfop() != null){
			query.leftOuterJoin("notafiscalproduto.listaItens item");
			query.where("item.material = ?", filtro.getMaterial());
			query.where("item.cfop = ?", filtro.getCfop());
		}
//			.whereIn("notafiscalproduto.cdNota", filtro)
		query.orderBy("notafiscalproduto.dtEmissao desc");
		
		query
		.where("empresa = ?", filtro.getEmpresa())
		.whereIn("notafiscalproduto.numero", filtro.getNumNota() != null && !filtro.getNumNota().equals("") ? "'" + filtro.getNumNota().replaceAll(",", "','") + "'" : null)
		.where("notafiscalproduto.dtEmissao >= ?", filtro.getDtEmissaoInicio())
		.where("notafiscalproduto.dtEmissao <= ?", filtro.getDtEmissaoFim())
		//.whereLikeIgnoreAll("notafiscalproduto.naturezaoperacao", filtro.getNaturezaoperacao())
		.where("cliente = ?", filtro.getCliente())
		.where("projeto = ?", filtro.getProjeto())
		.where("naturezaoperacao = ?", filtro.getNaturezaoperacao())
		.where("cast(notafiscalproduto.numero AS long) >= ?", filtro.getNumNotaDe())
		.where("cast(notafiscalproduto.numero AS long) <= ?", filtro.getNumNotaAte())
		.where("notafiscalproduto.valor = ?", filtro.getValor())
		.where("notafiscalproduto.modeloDocumentoFiscalEnum = ?", filtro.getModeloDocumentoFiscalEnum())
		.where("notafiscalproduto.tipooperacaonota = ?", filtro.getTipooperacaonota())
		.whereIn("notaStatus.cdNotaStatus", filtro.getListaNotaStatus() != null ? CollectionsUtil.listAndConcatenate(filtro.getListaNotaStatus(), "cdNotaStatus", ",") : null)
		.whereIn("notaTipo.cdNotaTipo", filtro.getListaNotaTipo() != null ? CollectionsUtil.listAndConcatenate(filtro.getListaNotaTipo(), "cdNotaTipo", ",") : null);
	
		if (filtro.getDtPagamentoInicio() != null && filtro.getDtPagamentoFim() != null)
			query.where("notafiscalproduto.cdNota in (select n.cdNota from Nota n join n.listaNotaDocumento lnd join lnd.documento d join d.listaMovimentacaoOrigem lmo join lmo.movimentacao m where m.dtmovimentacao >= ? and m.dtmovimentacao <= ?)", new Object[]{filtro.getDtPagamentoInicio(), filtro.getDtPagamentoFim()});
		else if (filtro.getDtPagamentoInicio() != null)
			query.where("notafiscalproduto.cdNota in (select n.cdNota from Nota n join n.listaNotaDocumento lnd join lnd.documento d join d.listaMovimentacaoOrigem lmo join lmo.movimentacao m where m.dtmovimentacao >= ?)", filtro.getDtPagamentoInicio());
		else if (filtro.getDtPagamentoFim() != null)
			query.where("notafiscalproduto.cdNota in (select n.cdNota from Nota n join n.listaNotaDocumento lnd join lnd.documento d join d.listaMovimentacaoOrigem lmo join lmo.movimentacao m where m.dtmovimentacao <= ?)", filtro.getDtPagamentoFim());
		
		
		query.orderBy("notafiscalproduto.dtEmissao desc");

		if(!SinedUtil.isUsuarioLogadoAdministrador()){
			query.leftOuterJoin("notafiscalproduto.listaRestricao restricao")
					.openParentheses()
						.where("restricao.cdnotafiscalprodutorestricao is null")
						.or()
						.where("restricao.usuario = ?", SinedUtil.getUsuarioLogado())
					.closeParentheses();
		}
		
		addWhereRestricaoClienteVendedor(query);
		
		if(filtro.getRegistroreceita() != null){
			if(filtro.getRegistroreceita()){
				query.where("exists(select an.cdarquivonfnota " +
						"from Arquivonfnota an " +
						"where an.nota = notafiscalproduto " +
						"and an.chaveacesso is not null " +
						"and an.protocolonfe is not null " +
						"and an.dtprocessamento is not null " +
						" )");
			} else {
				query.where("not exists(select an.cdarquivonfnota " +
						"from Arquivonfnota an " +
						"where an.nota = notafiscalproduto " +
						"and an.chaveacesso is not null " +
						"and an.protocolonfe is not null " +
						"and an.dtprocessamento is not null " +
						" )");
			}
		} 
		
		return query.list();
	}

	/**
	 * Busca a lista de Nota Fiscal de Produto para a gera��o do CSV.
	 *
	 * @param filtro
	 * @return
	 * @since Jul 4, 2011
	 * @author Rodrigo Freitas
	 */
	public List<Notafiscalproduto> findForCsv(NotafiscalprodutoFiltro filtro) {
		QueryBuilder<Notafiscalproduto> query = query();
		this.updateListagemQuery(query, filtro);
		List<Notafiscalproduto> listaTotal = query.list();
		
		if(listaTotal != null && listaTotal.size() > 0){
			String whereIn = CollectionsUtil.listAndConcatenate(listaTotal, "cdNota", ",");
			return query()
						.select("notafiscalproduto.cdNota, notafiscalproduto.dtEmissao, notafiscalproduto.numero, " +
								"notafiscalproduto.valor, notafiscalproduto.tipooperacaonota, notafiscalproduto.datahorasaidaentrada, " +
								"notafiscalproduto.formapagamentonfe, notafiscalproduto.valorprodutos, " +
								"notafiscalproduto.valorfrete, notafiscalproduto.valordesconto, notafiscalproduto.outrasdespesas, " +
								"cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj," +
								"responsavelfrete.cdResponsavelFrete, responsavelfrete.nome, item.tipocobrancaicms, item.valordesconto," +
								"item.valorbcicms, item.valoricms, item.valorbcicmsst, item.valoricmsst, item.tipocobrancaipi," +
								"item.valoripi, item.tipocobrancapis, item.valorpis, item.tipocobrancacofins, item.valorcofins, naturezaoperacao.nome," +
								"cfop.cdcfop, cfop.codigo, documentotipo.cddocumentotipo, documentotipo.nome")
						.leftOuterJoin("notafiscalproduto.responsavelfrete responsavelfrete")
						.leftOuterJoin("notafiscalproduto.cliente cliente")
						.leftOuterJoin("notafiscalproduto.naturezaoperacao naturezaoperacao")
						.join("notafiscalproduto.listaItens item")
						.leftOuterJoin("item.cfop cfop")
						.leftOuterJoin("notafiscalproduto.listaNotaDocumento notadocumento")
                        .leftOuterJoin("notadocumento.documento documento")
                        .leftOuterJoin("documento.documentotipo documentotipo")
						.whereIn("notafiscalproduto.cdNota", whereIn)
						.orderBy("notafiscalproduto.dtEmissao desc")
						.list();
		} else return new ArrayList<Notafiscalproduto>();
	}
	
	/**
	 * M�todo que busca os dados na Nota Fiscal de Produto para Integra��o com Dom�nio
	 *
	 * @param empresa
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Notafiscalproduto> findForDominiointegracaoSaida(Empresa empresa, Date dtinicio, Date dtfim) {
		
		return query()
				.select("notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.valor, notafiscalproduto.datahorasaidaentrada, " +
						"notafiscalproduto.valorfrete, notafiscalproduto.valorseguro, notafiscalproduto.outrasdespesas, " +
						"notafiscalproduto.valorprodutos, notafiscalproduto.tipooperacaonota, listareferenciada.chaveacesso, " +
						"cliente.cpf, cliente.cnpj, cliente.inscricaomunicipal, cliente.inscricaoestadual, " +
						"uf.sigla, notafiscalproduto.formapagamentonfe, " +
						"material.cdmaterial, " +
						"listaitens.qtde, listaitens.valorbcipi, listaitens.valoripi, listaitens.valorbcipi, listaitens.valorunitario," +
						"listaitens.valoricms, listaitens.valoricmsst, listaitens.icms, listaitens.valorfrete, listaitens.valorseguro, " +
						"listaitens.ipi, listaitens.pis, listaitens.valorpis, listaitens.cofins, listaitens.valorcofins, listaitens.valorbcpis," +
						"listaitens.valorbccofins, " +
						"cfop.codigo, endereco.cdendereco, municipio.cdmunicipio, municipio.cdibge, uf.cduf, uf.sigla, uf.cdibge," +
						"arquivonf.cdarquivonf, arquivonfnota.numeronfse, arquivonfnota.cdarquivonfnota, " +
						"naturezaoperacao.cdnaturezaoperacao, " +
						"operacaocontabilavista.cdoperacaocontabil, operacaocontabilavista.codigointegracao," +
						"operacaocontabilaprazo.cdoperacaocontabil, operacaocontabilaprazo.codigointegracao, " +
						"enderecoNF.cdendereco, municipioNF.cdmunicipio, municipioNF.cdibge, ufNF.cduf, ufNF.sigla, ufNF.cdibge ")
				.leftOuterJoin("notafiscalproduto.empresa empresa")
				.leftOuterJoin("notafiscalproduto.cliente cliente")
				.leftOuterJoin("cliente.listaEndereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("notafiscalproduto.enderecoCliente enderecoNF")
				.leftOuterJoin("enderecoNF.municipio municipioNF")
				.leftOuterJoin("municipioNF.uf ufNF")
				.leftOuterJoin("notafiscalproduto.listaItens listaitens")
				.leftOuterJoin("listaitens.material material")
				.leftOuterJoin("listaitens.cfop cfop")
				.leftOuterJoin("notafiscalproduto.listaReferenciada listareferenciada")
				.leftOuterJoin("notafiscalproduto.listaArquivonfnota arquivonfnota")
				.leftOuterJoin("arquivonfnota.arquivonf arquivonf")
				.leftOuterJoin("arquivonf.configuracaonfe configuracaonfe")
				.leftOuterJoin("notafiscalproduto.naturezaoperacao naturezaoperacao")
				.leftOuterJoin("naturezaoperacao.operacaocontabilavista operacaocontabilavista")
				.leftOuterJoin("naturezaoperacao.operacaocontabilaprazo operacaocontabilaprazo")
				.where("empresa = ?", empresa)
				.where("notafiscalproduto.datahorasaidaentrada >= ?", SinedDateUtils.dateToTimestampInicioDia(dtinicio))
				.where("notafiscalproduto.datahorasaidaentrada <= ?", SinedDateUtils.dateToTimestampFinalDia(dtfim))
				.list();
				
	}

	/**
	 * M�todo que busca as informa��es para o RegistroC100 do SPED PIS/COFINS
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Notafiscalproduto> findForSpedPiscofinsRegC100(SpedpiscofinsFiltro filtro, Empresa empresa) {
		if(filtro != null) {
			QueryBuilder<Notafiscalproduto> query = querySined()
					
					.select("notafiscalproduto.tipooperacaonota, cliente.cdpessoa, cliente.cnpj, cliente.cpf, empresa.cnpj, empresa.cdpessoa, notafiscalproduto.localdestinonfe, notafiscalproduto.tipooperacaonota, " +
							"notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.finalidadenfe, notafiscalproduto.dtEmissao, notafiscalproduto.modeloDocumentoFiscalEnum, " +
							"notafiscalproduto.datahorasaidaentrada, notafiscalproduto.valor, notafiscalproduto.formapagamentonfe, " +
							"notafiscalproduto.valordesconto, notafiscalproduto.valorprodutos, responsavelfrete.cdnfe, notafiscalproduto.origemCupom, " +
							"notafiscalproduto.valorfrete, notafiscalproduto.valorseguro, notafiscalproduto.outrasdespesas, " +
							"notafiscalproduto.valorbcicms, notafiscalproduto.valoricms, notafiscalproduto.valorbcicmsst, notaStatus.cdNotaStatus, " + 
							"notafiscalproduto.valoricmsst, notafiscalproduto.valoripi, notafiscalproduto.valorpis, notafiscalproduto.valorcofins, " +
							"notafiscalproduto.infoadicionalfisco, material.cdmaterial, material.identificacao, material.origemproduto, " +
							"unidademedida.cdunidademedida, item.qtde, item.valorbruto, item.valordesconto, item.tipocobrancaicms, " +
							"cfop.codigo, cfop.cdcfop, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, item.identificadorespecifico, " +
							"item.valorunitario, item.qtde, " +
							"item.icmsst, item.valoricmsst, item.tipocobrancaipi, item.valorbcipi, item.ipi, item.valoripi, " +
							"item.tipocobrancapis, item.valorbcpis, item.pis, item.qtdevendidapis, item.aliquotareaispis, item.valorpis, " +
							"item.tipocobrancacofins, item.valorbccofins, item.cofins, item.qtdevendidacofins, item.aliquotareaiscofins, " +
							"item.valorcofins, grupotributacao.cdgrupotributacao, " +
							"listaGrupotributacaoimposto.controle, listaGrupotributacaoimposto.codigonaturezareceita," +
							"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome, " +
							"declaracaoimportacao.cddeclaracaoimportacao, declaracaoimportacao.numerodidsida, declaracaoimportacaoadicao.numerodrawback, vcontacontabil.identificador, " +
							"tributacaoecf.cdtributacaoecf, tributacaoecf.codigoecf, tributacaoecf.cstpis, tributacaoecf.aliqpis, tributacaoecf.cstcofins,tributacaoecf.aliqcofins, " +
							"tributacaoecf.csticms, tributacaoecf.origemproduto")
					.join("notafiscalproduto.notaStatus notaStatus")
					.join("notafiscalproduto.empresa empresa")
					.join("notafiscalproduto.cliente cliente")
					.leftOuterJoin("notafiscalproduto.responsavelfrete responsavelfrete")
					.join("notafiscalproduto.listaItens item")
					.leftOuterJoin("item.listaNotafiscalprodutoitemdi notafiscalprodutoitemdi")
					.leftOuterJoin("notafiscalprodutoitemdi.declaracaoimportacao declaracaoimportacao")
					.leftOuterJoin("declaracaoimportacao.listaDeclaracaoimportacaoadicao declaracaoimportacaoadicao")
					.leftOuterJoin("item.cfop cfop")
					.join("item.material material")
					.join("material.unidademedida unidademedida")
					.leftOuterJoin("material.tributacaoecf tributacaoecf")
					.leftOuterJoin("material.grupotributacao grupotributacao")
					.leftOuterJoin("grupotributacao.listaGrupotributacaoimposto listaGrupotributacaoimposto")
					.leftOuterJoin("notafiscalproduto.naturezaoperacao naturezaoperacao")
					.leftOuterJoin("material.contaContabil contacontabil")
					.leftOuterJoin("contacontabil.vcontacontabil vcontacontabil")
					.where("empresa = ?", empresa)
					.openParentheses()
						.where("naturezaoperacao is null")
						.or()
						.where("naturezaoperacao.simplesremessa is null")
						.or()
						.where("naturezaoperacao.simplesremessa = false")
					.closeParentheses()
					.openParentheses()
						.where("notaStatus = ?", NotaStatus.NFE_EMITIDA)
						.or()
						.where("notaStatus = ?", NotaStatus.NFE_LIQUIDADA)
					.closeParentheses();
			
			addWhereDtemissaoDtentrada(query, filtro.getDtinicio(), filtro.getDtfim());
			
			boolean entradas = filtro.getBuscarentradas() != null && filtro.getBuscarentradas();
			boolean saidas = filtro.getBuscarsaidas() != null && filtro.getBuscarsaidas();
			if(entradas && !saidas){
				query.where("notafiscalproduto.tipooperacaonota = ?", Tipooperacaonota.ENTRADA);
			} else if(!entradas && saidas){
				query.where("notafiscalproduto.tipooperacaonota = ?", Tipooperacaonota.SAIDA);
			} else if(!entradas && !saidas){
				query.where("1=0");
			}
			
			query.where("cfop.codigo <> '5908' and cfop.codigo <> '1909' ");
			
			return query.list();
		} else
			return null;
	}

	/**
	 * M�todo para gerar registro50 do SIntegra
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Notafiscalproduto> findForSIntegraRegistro50(SintegraFiltro filtro) {
		if(filtro == null)
			throw new SinedException("Par�metro inv�lido");
		
		QueryBuilder<Notafiscalproduto> query = query()
				.select(
						"ufinscricaoestadual.sigla, cfop.cdcfop, cfop.codigo,   " +
						"notaStatus.cdNotaStatus, enderecoCliente.cdendereco, enderecoCliente.inscricaoestadual, " +
						"municipio.cdmunicipio, uf.sigla, ufsped.sigla, pais.cdpais, " +
						
						"cliente.cnpj, cliente.cpf, cliente.inscricaoestadual, " +
						"material.cdmaterial, material.nome, material.nomenf, material.ncmcompleto, material.origemproduto, " +
						"unidademedida.cdunidademedida, unidademedida.simbolo, " +
						
						"notafiscalproduto.dtEmissao, notafiscalproduto.numero, notafiscalproduto.valorbcicms, notafiscalproduto.valoricms, notafiscalproduto.valorbcicmsst, " +
						"notafiscalproduto.cdNota, notafiscalproduto.tipooperacaonota, notafiscalproduto.datahorasaidaentrada, notafiscalproduto.valor, " +
						"notafiscalproduto.valordesconto, notafiscalproduto.valorprodutos, notafiscalproduto.valorfrete, notafiscalproduto.valorseguro, " +
						"notafiscalproduto.valoripi, notafiscalproduto.valorpis, notafiscalproduto.valorcofins, " +
						
						"item.outrasdespesas, item.icms, item.tipocobrancaicms, item.tipotributacaoicms, item.cdnotafiscalprodutoitem, item.valoricms, " +
						"item.qtde, item.valorbruto, item.valorunitario, item.valordesconto, item.valorbcicms, item.valorbcicmsst, item.icmsst, item.valoricmsst, item.tipocobrancaipi, " +
						"item.valorbcipi, item.ipi, item.valoripi, unidademedidaIt.cdunidademedida, unidademedidaIt.nome, unidademedidaIt.simbolo, item.valorfrete, item.valorseguro, item.valorii," +
						"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome")
				.leftOuterJoin("notafiscalproduto.cliente cliente")
				.leftOuterJoin("notafiscalproduto.enderecoCliente enderecoCliente")
				.leftOuterJoin("cliente.ufinscricaoestadual ufinscricaoestadual")
				.leftOuterJoin("enderecoCliente.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("enderecoCliente.pais pais")
//				.leftOuterJoin("endereco.municipio municipio")
//				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("notafiscalproduto.notaStatus notaStatus")
				.join("notafiscalproduto.empresa empresa")
				.join("empresa.ufsped ufsped")
				.leftOuterJoin("notafiscalproduto.listaItens item")
				.join("item.material material")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("item.cfop cfop")
				.leftOuterJoin("item.unidademedida unidademedidaIt")
				.leftOuterJoin("notafiscalproduto.listaNotaHistorico listaNotaHistorico")
				.leftOuterJoin("notafiscalproduto.naturezaoperacao naturezaoperacao")
				.where("empresa = ?", filtro.getEmpresa())
				.where("notafiscalproduto.dtEmissao >= ?", filtro.getDtinicio())
				.where("notafiscalproduto.dtEmissao <= ?", filtro.getDtfim())
				.where("notafiscalproduto.modeloDocumentoFiscalEnum <> ?", ModeloDocumentoFiscalEnum.NFCE);
		
		query
			.openParentheses()
				.where("notaStatus = ?", NotaStatus.NFE_EMITIDA)
				.or()
				.where("notaStatus = ?", NotaStatus.NFE_LIQUIDADA)
				.or()
				.openParentheses()
					.where("notaStatus = ?", NotaStatus.CANCELADA)
					.openParentheses()
						.where("listaNotaHistorico.notaStatus = ?", NotaStatus.NFE_EMITIDA).or()
						.where("listaNotaHistorico.notaStatus = ?", NotaStatus.NFE_LIQUIDADA)
					.closeParentheses()
				.closeParentheses();
			if(filtro.getConsiderarnotasnaoeletronicas() != null && filtro.getConsiderarnotasnaoeletronicas()){
				query
					.or()
					.where("notaStatus = ?", NotaStatus.EMITIDA)
					.or()
					.where("notaStatus = ?", NotaStatus.LIQUIDADA)
					.or()
					.where("notaStatus = ?", NotaStatus.CANCELADA);
			}
		query		
			.closeParentheses();
		
		if(filtro.getNaturezaoperacao() != null && 
				!SIntegraNaturezaoperacao.TOTALIDADE_OPERACOES_INFORMANTE.equals(filtro.getNaturezaoperacao())){
			if(SIntegraNaturezaoperacao.INTERESTADUAIS_COM_OU_SEM_SUBSTITUICAO_TRIBUTARIA.equals(filtro.getNaturezaoperacao())){
				query.where("exists " + 
						"(select 1 " +
						"from empresa e " +
						"where e.cdpessoa = " + filtro.getEmpresa().getCdpessoa() +
						" and e.ufsped <> uf.cduf )");
			}else if(SIntegraNaturezaoperacao.INTERESTADUAIS_SOMENTE_OPERACOES_REGIME_SUBSTITUICAO_TRIBUTARIA.equals(filtro.getNaturezaoperacao())){
				query.where("exists " + 
						"(select 1 " +
						"from empresa e " +
						"where e.cdpessoa = " + filtro.getEmpresa().getCdpessoa() +
						" and e.ufsped <> uf.cduf )");
				query
					.where("item.valoricmsst is not null ")
					.where("item.valoricmsst > 0 ");
			}
		}
		
		boolean entradas = filtro.getBuscarentradas() != null && filtro.getBuscarentradas();
		boolean saidas = filtro.getBuscarsaidasProduto() != null && filtro.getBuscarsaidasProduto();
		if(entradas && !saidas){
			query.where("notafiscalproduto.tipooperacaonota = ?", Tipooperacaonota.ENTRADA);
		} else if(!entradas && saidas){
			query.where("notafiscalproduto.tipooperacaonota = ?", Tipooperacaonota.SAIDA);
		} else if(!entradas && !saidas){
			query.where("1=0");
		}
		
		return query.list();
				
	}
	

	/**
	 * Verifica se existe a nota cadastrada.
	 *
	 * @param empresa
	 * @param numeroNota
	 * @param serienfe
	 * @return
	 * @since 26/05/2012
	 * @author Rodrigo Freitas
	 */
	public boolean isExisteNota(Empresa empresa, String numeroNota, Integer serienfe, ModeloDocumentoFiscalEnum modeloDocumentoFiscalEnum, Notafiscalproduto nfp) {
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(beanClass)
					.join("notafiscalproduto.notaStatus notaStatus")
					.join("notafiscalproduto.empresa empresa")
					.where("empresa = ?", empresa)			
					.where("notaStatus <> ?", NotaStatus.CANCELADA)			
					.where("notafiscalproduto.numero = ?", numeroNota)
					.where("notafiscalproduto <> ?", nfp, nfp != null && nfp.getCdNota() != null)
					.where("notafiscalproduto.serienfe = ?", serienfe)
					.where("notafiscalproduto.modeloDocumentoFiscalEnum = ?", modeloDocumentoFiscalEnum)
					.where("notafiscalproduto.cdNota not in ( select n.cdNota " +
															" from Arquivonfnota anfnota " +
															" join anfnota.arquivonf anf " +
															" join anf.configuracaonfe cnfe " +
															" join anfnota.nota n where cnfe.prefixowebservice in (" + Prefixowebservice.getValuesPrefixosSefazHomolgoacao() + "))")
					.unique() > 0;
	}

	/**
	 * Busca as notas para o cancelamento na receita.
	 *
	 * @param whereIn
	 * @return
	 * @since 04/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Notafiscalproduto> findForCancelamentoNfe(String whereIn) {
		return query()
					.select("notafiscalproduto.cdNota, notafiscalproduto.numero, arquivonfnota.cdarquivonfnota, " +
							"arquivonfnota.chaveacesso, arquivonfnota.protocolonfe, configuracaonfe.prefixowebservice")
					.leftOuterJoin("notafiscalproduto.listaArquivonfnota arquivonfnota")
					.leftOuterJoin("arquivonfnota.nota nota")
					.leftOuterJoin("arquivonfnota.arquivonf arquivonf")
					.leftOuterJoin("arquivonf.configuracaonfe configuracaonfe")
					.where("arquivonfnota.dtcancelamento is null")
					.where("arquivonfnota.dtprocessamento is not null")
					.whereIn("nota.cdNota", whereIn)
					.list();
	}

	/**
	 * Busca a lista de NF de produto para rec�lculo da comiss�o.
	 *
	 * @param whereIn
	 * @param dtrecalculoDe
	 * @param dtrecalculoAte
	 * @return
	 * @since 19/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Notafiscalproduto> findForRecalculoComissao(String whereIn, Date dtrecalculoDe, Date dtrecalculoAte) {
		return query()
				.select("notafiscalproduto.cdNota, contrato.cdcontrato, comissionamento.cdcomissionamento, comissionamento.tipobccomissionamento")
				.join("notafiscalproduto.listaNotaContrato notaContrato")
				.join("notaContrato.contrato contrato")
				.leftOuterJoin("contrato.comissionamento comissionamento")
				.leftOuterJoin("notafiscalproduto.listaNotaDocumento notadocumento")
				.whereIn("contrato.cdcontrato", whereIn)
				.where("notafiscalproduto.dtEmissao >= ?", dtrecalculoDe)
				.where("notafiscalproduto.dtEmissao <= ?", dtrecalculoAte)
				.where("notadocumento is null")
				.orderBy("notafiscalproduto.dtEmissao, notafiscalproduto.cdNota")
				.list();
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutoDAO#carregaNotaVenda(Notafiscalproduto notafiscalproduto)
	 *
	 * @param notafiscalproduto
	 * @return
	 * @author Luiz Fernando
	 */
	public Notafiscalproduto carregaNotaVenda(Notafiscalproduto notafiscalproduto) {
		if(notafiscalproduto == null || notafiscalproduto.getCdNota() == null)
			throw new SinedException("Nota n�o pode ser nulo.");
		
		return query()
			.select("notafiscalproduto.cdNota, listaNotavenda.cdnotavenda, venda.cdvenda, vendamaterial.cdvendamaterial, vendamaterial.quantidade, " +
					"vendamaterial.comprimentooriginal, vendamaterial.altura, vendamaterial.largura, material.cdmaterial")
			.join("notafiscalproduto.listaNotavenda listaNotavenda")
			.join("listaNotavenda.venda venda")
			.join("venda.listavendamaterial vendamaterial")
			.join("vendamaterial.material material")
			.where("notafiscalproduto = ?", notafiscalproduto)
			.unique();
	}

	/**
	 * M�todo que busca os dados das notas para apura��o de pis/cofins
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Notafiscalproduto> findforApuracaopiscofins(ApuracaopiscofinsFiltro filtro) {
		if(filtro == null) 
			throw new SinedException("Filtro n�o pode ser nulo");
			
		QueryBuilder<Notafiscalproduto> query = querySined()
					.select("notafiscalproduto.tipooperacaonota, cliente.cdpessoa, cliente.nome, cliente.razaosocial, cliente.cpf, cliente.cnpj, " +
							"notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.finalidadenfe, notafiscalproduto.dtEmissao, " +
							"notafiscalproduto.datahorasaidaentrada, notafiscalproduto.valor, notafiscalproduto.formapagamentonfe, " +
							"notafiscalproduto.valordesconto, notafiscalproduto.valorprodutos, responsavelfrete.cdnfe, " +
							"notafiscalproduto.valorfrete, notafiscalproduto.valorseguro, notafiscalproduto.outrasdespesas, " +
							"notafiscalproduto.valorbcicms, notafiscalproduto.valoricms, notafiscalproduto.valorbcicmsst, " +
							"notafiscalproduto.valoricmsst, notafiscalproduto.valoripi, notafiscalproduto.valorpis, notafiscalproduto.valorcofins, " +
							"notafiscalproduto.infoadicionalfisco, material.cdmaterial, " +
							"unidademedida.cdunidademedida, item.qtde, item.valorbruto, item.valordesconto, item.tipocobrancaicms, " +
							"cfop.codigo, cfop.cdcfop, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, " +
							"item.icmsst, item.valoricmsst, item.tipocobrancaipi, item.valorbcipi, item.ipi, item.valoripi, " +
							"item.tipocobrancapis, item.valorbcpis, item.pis, item.qtdevendidapis, item.aliquotareaispis, item.valorpis, " +
							"item.tipocobrancacofins, item.valorbccofins, item.cofins, item.qtdevendidacofins, item.aliquotareaiscofins, " +
							"item.valorcofins, cfopescopo.cdcfopescopo, cfopescopo.descricao, naturezaoperacao.cdnaturezaoperacao," +
							"naturezaoperacao.nome")
					.join("notafiscalproduto.notaStatus notaStatus")
					.join("notafiscalproduto.empresa empresa")
					.join("notafiscalproduto.cliente cliente")
					.leftOuterJoin("notafiscalproduto.responsavelfrete responsavelfrete")
					.join("notafiscalproduto.listaItens item")
					.leftOuterJoin("item.cfop cfop")
					.leftOuterJoin("cfop.cfopescopo cfopescopo")
					.join("item.material material")
					.join("material.unidademedida unidademedida")
					.leftOuterJoin("notafiscalproduto.naturezaoperacao naturezaoperacao")
					.where("empresa = ?", filtro.getEmpresa())
					.openParentheses()
						.where("naturezaoperacao is null")
						.or()
						.where("naturezaoperacao.simplesremessa is null")
						.or()
						.where("naturezaoperacao.simplesremessa = false")
					.closeParentheses()
					.openParentheses()
						.where("notaStatus = ?", NotaStatus.NFE_EMITIDA)
						.or()
						.where("notaStatus = ?", NotaStatus.NFE_LIQUIDADA)
						.or()
						.where("notaStatus = ?", NotaStatus.EMITIDA)
						.or()
						.where("notaStatus = ?", NotaStatus.LIQUIDADA)
					.closeParentheses();
		
		boolean entradas = filtro.getEntrada() != null && filtro.getEntrada();
		boolean saidas = filtro.getSaida() != null && filtro.getSaida();
		if(entradas && !saidas){
			query.where("notafiscalproduto.tipooperacaonota = ?", Tipooperacaonota.ENTRADA);
		} else if(!entradas && saidas){
			query.where("notafiscalproduto.tipooperacaonota = ?", Tipooperacaonota.SAIDA);
		} else if(!entradas && !saidas){
			query.where("1=0");
		}
		
		addWhereDtemissaoDtentrada(query, filtro.getDtinicio(), filtro.getDtfim());
		
		return query
					.orderBy("notafiscalproduto.dtEmissao")
					.list();
	}

	/**
	 * M�todo que busca os dados da nota para apuracao de ipi
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Notafiscalproduto> findForApuracaoipi(ApuracaoipiFiltro filtro) {
		if(filtro == null) 
			throw new SinedException("Filtro n�o pode ser nulo");
		
		QueryBuilder<Notafiscalproduto> query = query()
			.select("notafiscalproduto.tipooperacaonota, cliente.cdpessoa, " +
					"notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.finalidadenfe, notafiscalproduto.dtEmissao, " +
					"notafiscalproduto.datahorasaidaentrada, notafiscalproduto.valor, notafiscalproduto.formapagamentonfe, " +
					"notafiscalproduto.valordesconto, notafiscalproduto.valorprodutos, responsavelfrete.cdnfe, " +
					"notafiscalproduto.valorfrete, notafiscalproduto.valorseguro, notafiscalproduto.outrasdespesas, " +
					"notafiscalproduto.valorbcicms, notafiscalproduto.valoricms, notafiscalproduto.valorbcicmsst, " +
					"notafiscalproduto.valoricmsst, notafiscalproduto.valoripi, notafiscalproduto.valorpis, notafiscalproduto.valorcofins, " +
					"notafiscalproduto.infoadicionalfisco, material.cdmaterial, " +
					"unidademedida.cdunidademedida, item.qtde, item.valorbruto, item.valordesconto, item.tipocobrancaicms, " +
					"cfop.codigo, cfop.cdcfop, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, " +
					"item.icmsst, item.valoricmsst, item.tipocobrancaipi, item.valorbcipi, item.ipi, item.valoripi, " +
					"item.tipocobrancapis, item.valorbcpis, item.pis, item.qtdevendidapis, item.aliquotareaispis, item.valorpis, " +
					"item.tipocobrancacofins, item.valorbccofins, item.cofins, item.qtdevendidacofins, item.aliquotareaiscofins, " +
					"item.valorcofins, cfopescopo.cdcfopescopo, cfopescopo.descricao, item.valorunitario, item.qtde," +
					"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome, item.valorFiscalIpi, item.valorFiscalIcms")
			.join("notafiscalproduto.notaStatus notaStatus")
			.join("notafiscalproduto.empresa empresa")
			.join("notafiscalproduto.cliente cliente")
			.leftOuterJoin("notafiscalproduto.responsavelfrete responsavelfrete")
			.join("notafiscalproduto.listaItens item")
			.join("item.cfop cfop")
			.join("cfop.cfopescopo cfopescopo")
			.join("item.material material")
			.join("material.unidademedida unidademedida")
			.leftOuterJoin("notafiscalproduto.naturezaoperacao naturezaoperacao")
			.where("empresa = ?", filtro.getEmpresa())
			.openParentheses()
				.where("notaStatus = ?", NotaStatus.NFE_EMITIDA)
				.or()
				.where("notaStatus = ?", NotaStatus.NFE_LIQUIDADA)
			.closeParentheses();
		
		boolean entradas = filtro.getEntrada() != null && filtro.getEntrada();
		boolean saidas = filtro.getSaida() != null && filtro.getSaida();
		if(entradas && !saidas){
			query.where("notafiscalproduto.tipooperacaonota = ?", Tipooperacaonota.ENTRADA);
		} else if(!entradas && saidas){
			query.where("notafiscalproduto.tipooperacaonota = ?", Tipooperacaonota.SAIDA);
		} else if(!entradas && !saidas){
			query.where("1=0");
		}
		
		addWhereDtemissaoDtentrada(query, filtro.getDtinicio(), filtro.getDtfim());
		
		return query.list();
	}

	/**
	 * M�todo que busca os dados da nota para apuracao de icms
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Notafiscalproduto> findForApuracaoicms(ApuracaoicmsFiltro filtro, Tipooperacaonota tipooperacaonota) {
		if(filtro == null) 
			throw new SinedException("Filtro n�o pode ser nulo");
		
		QueryBuilder<Notafiscalproduto> query = query()
			.select("notafiscalproduto.tipooperacaonota, cliente.cdpessoa, " +
					"notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.finalidadenfe, notafiscalproduto.dtEmissao, " +
					"notafiscalproduto.datahorasaidaentrada, notafiscalproduto.valor, notafiscalproduto.formapagamentonfe, " +
					"notafiscalproduto.valordesconto, notafiscalproduto.valorprodutos, responsavelfrete.cdnfe, " +
					"notafiscalproduto.valorfrete, notafiscalproduto.valorseguro, notafiscalproduto.outrasdespesas, " +
					"notafiscalproduto.valorbcicms, notafiscalproduto.valoricms, notafiscalproduto.valorbcicmsst, " +
					"notafiscalproduto.valoricmsst, notafiscalproduto.valoripi, notafiscalproduto.valorpis, notafiscalproduto.valorcofins, " +
					"notafiscalproduto.infoadicionalfisco, material.cdmaterial, " +
					"unidademedida.cdunidademedida, item.qtde, item.valorbruto, item.valordesconto, item.tipocobrancaicms, " +
					"cfop.codigo, cfop.cdcfop, item.valorbcicms, item.icms, item.valoricms, item.valorbcicmsst, " +
					"item.icmsst, item.valoricmsst, item.tipocobrancaipi, item.valorbcipi, item.ipi, item.valoripi, " +
					"item.tipocobrancapis, item.valorbcpis, item.pis, item.qtdevendidapis, item.aliquotareaispis, item.valorpis, " +
					"item.tipocobrancacofins, item.valorbccofins, item.cofins, item.qtdevendidacofins, item.aliquotareaiscofins, " +
					"item.valorcofins, item.valorfrete, cfopescopo.cdcfopescopo, cfopescopo.descricao, item.valorunitario, item.qtde," +
					"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome, item.valorFiscalIpi, item.valorFiscalIcms")
			.join("notafiscalproduto.notaStatus notaStatus")
			.join("notafiscalproduto.empresa empresa")
			.join("notafiscalproduto.cliente cliente")
			.leftOuterJoin("notafiscalproduto.responsavelfrete responsavelfrete")
			.join("notafiscalproduto.listaItens item")
			.join("item.cfop cfop")
			.join("cfop.cfopescopo cfopescopo")
			.join("item.material material")
			.join("material.unidademedida unidademedida")
			.leftOuterJoin("notafiscalproduto.naturezaoperacao naturezaoperacao")
			.where("empresa = ?", filtro.getEmpresa())
			.where("notafiscalproduto.tipooperacaonota = ?", tipooperacaonota)
			.openParentheses()
				.where("notaStatus = ?", NotaStatus.NFE_EMITIDA)
				.or()
				.where("notaStatus = ?", NotaStatus.NFE_LIQUIDADA)
			.closeParentheses();
		
		addWhereDtemissaoDtentrada(query, filtro.getDtinicio(), filtro.getDtfim());
		
		return query.list();
	}

	/**
	 * M�todo que busca os dados da nota para gerar movimenta��o de estoque
	 *
	 * @param nota
	 * @return
	 * @author Luiz Fernando
	 */
	public Notafiscalproduto findForMovimentacaoestoque(Nota nota) {
		if(nota == null || nota.getCdNota() == null)
			throw new SinedException("Nota n�o pode ser nula.");
		
		return query()
				.select("notafiscalproduto.cdNota, notaStatus.cdNotaStatus, notaStatus.nome, notafiscalproduto.numero, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
						"notafiscalproduto.dtEmissao, notafiscalproduto.datahorasaidaentrada, notafiscalproduto.valordesconto, " +
						"projeto.cdprojeto, notafiscalproduto.valorprodutos, notafiscalproduto.valorfrete, notafiscalproduto.valorservicos, " +
						"notafiscalproduto.valor, material.producao, materialclasse.cdmaterialclasse, " +
						"material.cdmaterial, material.nome, unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, item.gtin, item.qtde, item.valorunitario, item.valorbruto," +
						"item.cdnotafiscalprodutoitem, item.valorfrete, item.valordesconto,item.valorseguro, item.outrasdespesas,  " +
						"notafiscalproduto.valorliquidofatura, material.produto, material.servico, material.epi, material.patrimonio, " +
						"item.valordesconto, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome ")
				.join("notafiscalproduto.notaStatus notaStatus")
				.leftOuterJoin("notafiscalproduto.cliente cliente")
				.join("notafiscalproduto.listaItens item")
				.join("item.material material")
				.leftOuterJoin("item.unidademedida unidademedida")
				.leftOuterJoin("item.materialclasse materialclasse")
				.leftOuterJoin("material.materialtipo materialtipo")
				.leftOuterJoin("notafiscalproduto.projeto projeto")
				.leftOuterJoin("notafiscalproduto.empresa empresa")
				.leftOuterJoin("item.localarmazenagem localarmazenagem")
				.where("notafiscalproduto.cdNota = ?", nota.getCdNota())
				.openParentheses()
					.where("notaStatus = ?", NotaStatus.LIQUIDADA)
					.or()
					.where("notaStatus = ?", NotaStatus.NFE_LIQUIDADA)
				.closeParentheses()
				.unique();
				
	}
	
	/**
	 * M�todo que busca os dados da nota para gerar movimenta��o de estoque
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Notafiscalproduto> findForMovimentacaoestoque(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Nota n�o pode ser nula.");
		
		return query()
				.select("notafiscalproduto.cdNota, notaStatus.cdNotaStatus, notaStatus.nome, notafiscalproduto.numero, empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
						"notafiscalproduto.dtEmissao, notafiscalproduto.datahorasaidaentrada, notafiscalproduto.valordesconto, notafiscalproduto.tipooperacaonota, " +
						"projeto.cdprojeto, notafiscalproduto.valorprodutos, notafiscalproduto.valorfrete, notafiscalproduto.valorservicos, " +
						"notafiscalproduto.valor, material.producao, materialclasse.cdmaterialclasse, " +
						"material.cdmaterial, material.nome, material.valorcusto, unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, item.gtin, item.qtde, item.valorunitario, item.valorbruto," +
						"item.cdnotafiscalprodutoitem, item.valorfrete, item.valordesconto,item.valorseguro, item.outrasdespesas,  " +
						"notafiscalproduto.valorliquidofatura, material.produto, material.servico, material.epi, material.patrimonio, " +
						"item.valordesconto, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
						"loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade," +
						"patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta, cfop.codigo, contaGerencial.cdcontagerencial, projetoMaterialRateioEstoque.cdprojeto ")
				.join("notafiscalproduto.notaStatus notaStatus")
				.leftOuterJoin("notafiscalproduto.cliente cliente")
				.join("notafiscalproduto.listaItens item")
				.join("item.material material")
				.leftOuterJoin("item.patrimonioitem patrimonioitem")
				.leftOuterJoin("item.unidademedida unidademedida")
				.leftOuterJoin("item.materialclasse materialclasse")
				.leftOuterJoin("material.materialtipo materialtipo")
				.leftOuterJoin("notafiscalproduto.projeto projeto")
				.leftOuterJoin("notafiscalproduto.empresa empresa")
				.leftOuterJoin("item.localarmazenagem localarmazenagem")
				.leftOuterJoin("item.loteestoque loteestoque")
				.leftOuterJoin("item.cfop cfop")
				.leftOuterJoin("cfop.contaGerencial contaGerencial")
				.leftOuterJoin("material.materialRateioEstoque materialRateioEstoque")
				.leftOuterJoin("materialRateioEstoque.projeto projetoMaterialRateioEstoque")
				.whereIn("notafiscalproduto.cdNota", whereIn)
//				.where("notaStatus <> ?", NotaStatus.LIQUIDADA)
//				.where("notaStatus <> ?", NotaStatus.NFE_LIQUIDADA)
				.list();
				
	}

	/**
	 * M�todo que calcula o valor total das notas da listagem
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Money findForCalculoTotalGeral(NotafiscalprodutoFiltro filtro) {
		Money totalnotas = new Money();
		
		if(filtro != null){
			QueryBuilder<Notafiscalproduto> query = query();
			this.updateListagemQuery(query, filtro);
			List<Notafiscalproduto> listaTotal = query.list();
			
			if(listaTotal != null && listaTotal.size() > 0){
				QueryBuilder<Long> queryLong = newQueryBuilder(Long.class)
					.from(Notafiscalproduto.class)
					.select("sum(notafiscalproduto.valor)")
					.setUseTranslator(false);
				
				SinedUtil.quebraWhereIn("notafiscalproduto.cdNota", CollectionsUtil.listAndConcatenate(listaTotal, "cdNota", ","), queryLong);
				
				Long total = queryLong.unique();
				
				if(total != null)
					totalnotas = new Money(total, true);
			}
		}	
		
		return totalnotas;
	}

	/**
	 * M�todo que verifica se existe uma nota emitida para a venda
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 * @since 24/10/2013
	 */
	public Boolean isExisteNotaEmitida(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Venda n�o pode ser nula.");
		
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(beanClass)
			.join("notafiscalproduto.notaStatus notaStatus")
			.join("notafiscalproduto.listaNotavenda listaNotavenda")
			.join("listaNotavenda.venda venda")
			.where("venda = ?", venda)			
			.where("notaStatus != ?", NotaStatus.CANCELADA)	
			.where("notaStatus != ?", NotaStatus.NFE_DENEGADA)	
			.unique() > 0;
	}

	/**
	 * Atualiza a hora e data de saida da nota para a atual
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 * @since 21/11/2013
	 */
	public void updateDataentradasaidaDataatual(String whereIn) {
		Timestamp timestamp = SinedDateUtils.currentTimestamp();
		getJdbcTemplate().update("UPDATE NOTAFISCALPRODUTO SET datahorasaidaentrada = ? WHERE CDNOTA IN (" + whereIn + ")", new Object[]{timestamp});
	}

	/**
	* M�todo que carrega a nota fiscal para registrar ajuste fiscal
	*
	* @param notafiscalproduto
	* @return
	* @since 06/01/2015
	* @author Luiz Fernando
	*/
	public Notafiscalproduto carregarDocumentoParaAjusteFiscal(Notafiscalproduto notafiscalproduto) {
		if(notafiscalproduto == null || notafiscalproduto.getCdNota() == null)
			throw new SinedException("Par�metro inv�lido");
		
		return query()
				.select("notafiscalproduto.cdNota, material.cdmaterial, material.nome, material.identificacao ")
				.join("notafiscalproduto.listaItens listaItens")
				.join("listaItens.material material")
				.where("notafiscalproduto = ?", notafiscalproduto)
				.unique();
	}
	
	
	/**
	* M�todo que carrega a entrada fiscal para para o registro K200
	*
	* @param material
	* @param filtro	
	* @return 
	* @since 26/01/2015
	* @author Jo�o Vitor
	*/
	public List<Notafiscalproduto> findForSpedRegK200(Material material, SpedarquivoFiltro filtro) {
        if(material == null || filtro == null)
            throw new SinedException("Par�metro inv�lido");

        return querySined()

                .select("notafiscalproduto.cdNota, material.cdmaterial, material.identificacao, cliente.cdpessoa")
                .join("notafiscalproduto.listaItens listaItens")
                .join("listaItens.material material")
                .join("notafiscalproduto.cliente cliente")
                .join("notafiscalproduto.naturezaoperacao naturezaoperacao")
                .where("naturezaoperacao.simplesremessa = ?", Boolean.TRUE)
                .where("notafiscalproduto.tipooperacaonota = ?", Tipooperacaonota.SAIDA)
                .where("notafiscalproduto.dtEmissao >= ?", filtro.getDtinicio())
                .where("notafiscalproduto.dtEmissao <= ?", filtro.getDtfim())
                .where("notafiscalproduto.notaStatus = ?", NotaStatus.LIQUIDADA)
                .where("material = ?", material)
                .list();
    }
	
	/**
	 * @param whereIn
	 * @return
	 * @since 25/08/2015
	 * @author Andrey Leonardo
	 */
	public List<Notafiscalproduto> findForMemorandoExportacao(String whereIn){
		if(whereIn == null ||  "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		return query()
				.select("notafiscalproduto.cdNota, naturezaoperacao.cdnaturezaoperacao, " +
						"listaNaturezaoperacaocfop.cdnaturezaoperacaocfop, cfop.cdcfop, " +
						"cfoptipo.cdcfoptipo, cfoptipo.descricao, cfopescopo.cdcfopescopo, " +
						"cfopescopo.descricao, notafiscalprodutoitementregamaterial.cdnotafiscalprodutoitementregamaterial")
				.leftOuterJoin("notafiscalproduto.naturezaoperacao naturezaoperacao")
				.leftOuterJoin("naturezaoperacao.listaNaturezaoperacaocfop listaNaturezaoperacaocfop")
				.leftOuterJoin("listaNaturezaoperacaocfop.cfop cfop")
				.leftOuterJoin("cfop.cfoptipo cfoptipo")
				.leftOuterJoin("cfop.cfopescopo cfopescopo")
				.leftOuterJoin("notafiscalproduto.listaItens notafiscalprodutoitem")
				.leftOuterJoin("notafiscalprodutoitem.listaNotaprodutoitementregamaterial notafiscalprodutoitementregamaterial")
				
				.whereIn("notafiscalproduto.cdNota", whereIn)
				.list();
	}
	
	public List<Notafiscalproduto> loadForMemorandoExportacao(String whereIn){
		QueryBuilder<Notafiscalproduto> query = query();
		
		return query.select("notafiscalproduto.cdNota, notafiscalproduto.numero," +
				"notafiscalprodutoitem.cdnotafiscalprodutoitem, fornecedor.cdpessoa, fornecedor.razaosocial")				
				.leftOuterJoin("notafiscalproduto.listaItens notafiscalprodutoitem")
				.leftOuterJoin("notafiscalprodutoitem.listaNotaprodutoitementregamaterial notaprodutoitementregamaterial")
				.leftOuterJoin("notaprodutoitementregamaterial.entregamaterial entregamaterial")
				.leftOuterJoin("entregamaterial.entregadocumento entregadocumento")
				.leftOuterJoin("entregadocumento.fornecedor fornecedor")				
				.whereIn("notafiscalproduto.cdNota", whereIn)
				.list();	
	}
	
	/**
	 * Busca as notas para verifica��o da necessidade de c�lculo para Partilha de ICMS Interestadual
	 *
	 * @param whereIn
	 * @param dadosAdicionais
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/06/2016
	 */
	public List<Notafiscalproduto> findForDIFALVerificacaoConferencia(String whereIn, boolean dadosAdicionais) {
		if(whereIn == null ||  "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		StringBuilder selectSb = new StringBuilder();
		selectSb.append("notafiscalproduto.cdNota, notafiscalproduto.operacaonfe, notafiscalproduto.localdestinonfe, notafiscalproduto.modeloDocumentoFiscalEnum, ");
		selectSb.append("cliente.cdpessoa, cliente.inscricaoestadual, cliente.contribuinteicmstipo, ");
		selectSb.append("enderecoCliente.cdendereco, enderecoCliente.inscricaoestadual");
		
		QueryBuilder<Notafiscalproduto> query = query()
				.join("notafiscalproduto.cliente cliente")
				.leftOuterJoin("notafiscalproduto.enderecoCliente enderecoCliente")
				.whereIn("notafiscalproduto.cdNota", whereIn);
		
		if(dadosAdicionais){
			selectSb.append(", ");
			selectSb.append("notafiscalproduto.dtEmissao, notafiscalproduto.numero, notafiscalproduto.valor,");
			selectSb.append("cliente.nome, ");
			selectSb.append("empresa.cdpessoa, ");
			selectSb.append("enderecoEmpresa.cdendereco, ");
			selectSb.append("enderecotipoEmpresa.cdenderecotipo, ");
			selectSb.append("naturezaoperacao.cdnaturezaoperacao, ");
			selectSb.append("notafiscalprodutoitem.cdnotafiscalprodutoitem, notafiscalprodutoitem.valorbruto, notafiscalprodutoitem.valorfrete, ");
			selectSb.append("notafiscalprodutoitem.outrasdespesas, notafiscalprodutoitem.valordesconto, notafiscalprodutoitem.valoripi, ");
			selectSb.append("notafiscalprodutoitem.ncmcompleto, ");
			selectSb.append("grupotributacao.cdgrupotributacao, grupotributacao.formulabcicmsdestinatario, ");
			selectSb.append("material.cdmaterial, material.nome, ");
			selectSb.append("materialgrupo.cdmaterialgrupo");
			
			query
				.leftOuterJoin("notafiscalproduto.listaItens notafiscalprodutoitem")
				.leftOuterJoin("notafiscalprodutoitem.grupotributacao grupotributacao")
				.leftOuterJoin("notafiscalprodutoitem.material material")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("notafiscalproduto.empresa empresa")
				.leftOuterJoin("empresa.listaEndereco enderecoEmpresa")
				.leftOuterJoin("enderecoEmpresa.enderecotipo enderecotipoEmpresa")
				.leftOuterJoin("notafiscalproduto.naturezaoperacao naturezaoperacao")
				;
		}
		
		return query.select(selectSb.toString()).list();
	}
	
	/**
	 * Atualiza campo referente ao ICMS Interestadual
	 *
	 * @param notafiscalproduto
	 * @author Rodrigo Freitas
	 * @since 13/06/2016
	 */
	public void updateICMSInterestadual(Notafiscalproduto notafiscalproduto) {
		getJdbcTemplate().update("update Notafiscalproduto n set valorfcpdestinatario = ?, valoricmsdestinatario = ?, valoricmsremetente = ? where cdnota = ?", new Object[]{
				notafiscalproduto.getValorfcpdestinatario().toLong(), notafiscalproduto.getValoricmsdestinatario().toLong(), notafiscalproduto.getValoricmsremetente().toLong(), notafiscalproduto.getCdNota()
		});
	}
	
	public List<Notafiscalproduto> findForGerarLancamentoContabil(GerarLancamentoContabilFiltro filtro, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento, String whereNotIn, String whereInNaturezaOperacao){
		if(StringUtils.isBlank(whereNotIn)){
			whereNotIn = "-1";
		}
		return query()
				.select("notafiscalproduto.cdNota, notafiscalproduto.dtEmissao, notafiscalproduto.numero, notafiscalproduto.valor," +
						"notafiscalproduto.valorprodutos, notafiscalproduto.outrasdespesas, notafiscalproduto.valorfrete, notafiscalproduto.valorseguro," +
						"notafiscalproduto.valorirrf, notafiscalproduto.valorprevsocial, notafiscalproduto.valorcsll, notafiscalproduto.valorii," +
						"notafiscalproduto.valoriss, notafiscalproduto.valoricms, notafiscalproduto.valoricmsst, notafiscalproduto.valoripi, notafiscalproduto.valorpis," +
						"notafiscalproduto.valorcofinsretido, notafiscalproduto.valorcofins, notafiscalproduto.valordesconto, notafiscalproduto.valorpisretido, notafiscalproduto.valorissretido," +
						"notaTipo.cdNotaTipo," +
						"cliente.nome, cliente.razaosocial," +
						"contaContabil.cdcontacontabil," +
						"listaItens.qtde, listaItens.valorunitario, listaItens.valorbruto, listaItens.valorpis, listaItens.valorcofins, listaItens.valoriss, listaItens.valoripi, " +
						"listaItens.valorir, listaItens.inss, listaItens.valorcsll, listaItens.valoricms, listaItens.valoricmsst, " +
						"operacaocontabilpagamento.cdoperacaocontabil, operacaocontabilpagamento.nome, operacaocontabilpagamento.tipo," +
						"operacaocontabilpagamento.historico, operacaocontabilpagamento.codigointegracao, operacaocontabilpagamento.agruparvalormesmaconta, " +
						"tipoLancamento.cdOperacaoContabilTipoLancamento, tipoLancamento.nome, tipoLancamento.ordem, tipoLancamento.movimentacaocontabilTipoLancamento, " +
						"contaContabil.cdcontacontabil, contaContabil.nome," +
						"listaDebito.cdoperacaocontabildebitocredito, listaDebito.tipo, listaDebito.controle, listaDebito.centrocusto, listaDebito.projeto, listaDebito.historico, contacontabildebito.cdcontacontabil, contacontabildebito.nome, listaDebito.formula, listaDebito.codigointegracao, " +
						"listaCredito.cdoperacaocontabildebitocredito, listaCredito.tipo, listaCredito.controle, listaCredito.centrocusto, listaCredito.projeto, listaCredito.historico, contacontabilcredito.cdcontacontabil, contacontabilcredito.nome, listaCredito.formula, listaCredito.codigointegracao, " +
						"centrocustovenda.cdcentrocusto, centrocustovenda.nome, contaContabilMaterial.cdcontacontabil, contaContabilMaterial.nome ")
				.join("notafiscalproduto.notaTipo notaTipo")
				.join("notafiscalproduto.cliente cliente")
				.leftOuterJoin("cliente.contaContabil contaContabil")
				.leftOuterJoin("notafiscalproduto.naturezaoperacao naturezaoperacao")
				.leftOuterJoin("naturezaoperacao.operacaocontabilpagamento operacaocontabilpagamento")
				.leftOuterJoin("operacaocontabilpagamento.listaDebito listaDebito")
				.leftOuterJoin("listaDebito.contaContabil contacontabildebito")
				.leftOuterJoin("operacaocontabilpagamento.listaCredito listaCredito")
				.leftOuterJoin("listaCredito.contaContabil contacontabilcredito")
				.leftOuterJoin("notafiscalproduto.listaItens listaItens")
				.leftOuterJoin("listaItens.material material")
				.leftOuterJoin("material.centrocustovenda centrocustovenda")
				.leftOuterJoin("material.contaContabil contaContabilMaterial")
				.leftOuterJoin("operacaocontabilpagamento.tipoLancamento tipoLancamento")
				.where("notafiscalproduto.empresa=?", filtro.getEmpresa())
				.where("notafiscalproduto.dtEmissao>=?", filtro.getDtPeriodoInicio())
				.where("notafiscalproduto.dtEmissao<=?", filtro.getDtPeriodoFim())
				.where("notafiscalproduto.projeto=?", filtro.getProjeto())
				.whereIn("naturezaoperacao.cdnaturezaoperacao", whereInNaturezaOperacao)
				.where("notafiscalproduto.notaStatus not in (?, ?, ?, ?, ?)", new Object[]{NotaStatus.CANCELADA, NotaStatus.NFSE_CANCELANDO, NotaStatus.NFE_CANCELANDO, NotaStatus.NFE_DENEGADA, NotaStatus.EM_ESPERA})
				.where("coalesce(cliente.incidiriss, false)=?", Boolean.TRUE, MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamento))
				.where("not exists (select 1 from Movimentacaocontabilorigem mco join mco.nota n where mco.nota=notafiscalproduto and mco.movimentacaocontabil.tipoLancamento=?)", new Object[]{movimentacaocontabilTipoLancamento})
				.whereIn("notafiscalproduto.cdNota not ", whereNotIn)
				.list();
	}

	/**
	 * Busca as notas relacionadas a venda, para pesquisa em campos autocomplete
	 *
	 * @param param
	 * @return
	 * @author Mairon Cezar
	 * @since 17/05/2017
	 */
	public  List<Notafiscalproduto> findNotaWithVendaForAutocomplete(String param, String cdempresa) {
		return query()
			.select("notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.dtEmissao, cliente.nome")
			.join("notafiscalproduto.cliente cliente")
			.join("notafiscalproduto.empresa empresa")
			.join("notafiscalproduto.listaItens item")
			.join("item.vendamaterial vendamaterial")			
			.join("vendamaterial.venda venda")
			.openParentheses()
				.whereLikeIgnoreAll("notafiscalproduto.numero",param)
				.or()
				.whereLikeIgnoreAll("cliente.nome",param)
			.closeParentheses()
			.whereIn("empresa.cdpessoa", cdempresa)
			.list();
	}
	
	
	public List<Notafiscalproduto> findNotaWithNotaForAutocomplete(String param, String cdempresa) {

		return query()
				.select("notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.dtEmissao, cliente.nome," +
						"notafiscalproduto.situacaoExpedicao")
				.join("notafiscalproduto.cliente cliente")
				.join("notafiscalproduto.empresa empresa")
				.leftOuterJoin("notafiscalproduto.listaNotafiscalprodutoColeta listaNotafiscalprodutoColeta")
				.openParentheses()
					.whereLikeIgnoreAll("notafiscalproduto.numero",param)
					.or()
					.whereLikeIgnoreAll("cliente.nome",param)
				.closeParentheses()
				.where("notafiscalproduto.tipooperacaonota = ?", Tipooperacaonota.SAIDA)
				.openParentheses()
					.where("notafiscalproduto.notaStatus =?", NotaStatus.NFE_EMITIDA)
						.or()
					.where("notafiscalproduto.notaStatus =?", NotaStatus.NFE_LIQUIDADA)
				.closeParentheses()
				.openParentheses()
					.where("notafiscalproduto.situacaoExpedicao is null")
						.or()
					.where("notafiscalproduto.situacaoExpedicao = ?",SituacaoVinculoExpedicao.PENDENTE)
				.closeParentheses()
				.where("listaNotafiscalprodutoColeta is not null")
				.whereIn("empresa.cdpessoa", cdempresa)
				.orderBy("notafiscalproduto.cdNota desc")
				.list();
	}
	
	/**
	 * Busca a nota fiscal de produto pelo n�mero e empresa.
	 * 	 
	 * @param empresa
	 * @param numeronota
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/10/2017
	 */
	public Notafiscalproduto findNotaByNumeroEmpresa(Empresa empresa, String numeronota, boolean includeCancelada) {
		QueryBuilder<Notafiscalproduto> query = query();
		if(includeCancelada){
			query.where("notafiscalproduto.notaStatus in (?, ?, ?, ?)", new Object[]{NotaStatus.NFE_EMITIDA, NotaStatus.NFE_LIQUIDADA, NotaStatus.NFE_DENEGADA, NotaStatus.CANCELADA});
		} else {
			query.where("notafiscalproduto.notaStatus in (?, ?, ?)", new Object[]{NotaStatus.NFE_EMITIDA, NotaStatus.NFE_LIQUIDADA, NotaStatus.NFE_DENEGADA});
		}
		
		List<Notafiscalproduto> list = query
				.select("notafiscalproduto.cdNota, notafiscalproduto.numero, cliente.cdpessoa, cliente.cpf, cliente.cnpj")
				.join("notafiscalproduto.cliente cliente")
				.where("notafiscalproduto.numero = ?", numeronota)
				.where("notafiscalproduto.empresa = ?", empresa)
				.list();
		
		if(list == null || list.size() == 0) return null;
		if(list.size() > 1) throw new SinedException("Mais de uma nota encontrada na busca solicitada.");
		return list.get(0);
	}
	
	public List<Notafiscalproduto> findNotasSemSerie(String whereIn){
		return query()
			.select("notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.serienfe, " +
					"empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cdpessoa, " +
					"empresa.serienfe, empresa.serieNfce ")
			.join("notafiscalproduto.empresa empresa")
			.whereIn("notafiscalproduto.cdNota", whereIn)
			.list();
	}
	
	/**
	 * Busca as notas com a chave de acesso informada.
	 * 	 
	 * @param chaveacesso
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/12/2017
	 */
	public List<Notafiscalproduto> findByChaveacesso(String chaveacesso) {
		return query()
					.select("notafiscalproduto.cdNota, notafiscalproduto.dtEmissao, notafiscalproduto.numero, notafiscalproduto.serienfe, " +
							"cliente.cdpessoa, cliente.cpf, cliente.cnpj")
					.join("notafiscalproduto.cliente cliente")
					.join("notafiscalproduto.listaArquivonfnota arquivonfnota")
					.join("arquivonfnota.arquivonf arquivonf")
					.where("arquivonfnota.chaveacesso = ?", chaveacesso)
					.openParentheses()
					.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_ERRO)
					.or()
					.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_SUCESSO)
					.closeParentheses()
					.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Notafiscalproduto> findByNotaSemContaReceber(Date dateToSearch) {
		List<NotaStatus> lista = new ArrayList<NotaStatus>();
		lista.add(NotaStatus.LIQUIDADA);
		lista.add(NotaStatus.NFSE_LIQUIDADA);
		lista.add(NotaStatus.NFE_LIQUIDADA);
		lista.add(NotaStatus.CANCELADA);
		lista.add(NotaStatus.NFSE_CANCELANDO);
		lista.add(NotaStatus.NFE_CANCELANDO);
		lista.add(NotaStatus.NFE_DENEGADA);
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("select p.cdnota, n.cdempresa, n.numero ");
		sql.append("from notafiscalproduto p ");
		sql.append("join nota n on n.cdnota = p.cdnota ");
		sql.append("join naturezaoperacao no on no.cdnaturezaoperacao = p.cdnaturezaoperacao ");
		sql.append("join notadocumento nd on nd.cdnota = p.cdnota ");
		sql.append("join documento d on d.cddocumento = nd.cddocumento ");
		sql.append("where n.cdnotastatus not in (" + CollectionsUtil.listAndConcatenate(lista, "cdNotaStatus", ",") + ") ");
		sql.append("	and no.simplesremessa <> true and d.cddocumentoclasse = 2 ");
		sql.append("    and n.dtemissao between '" + dateToSearch + "' and current_date ");

		SinedUtil.markAsReader();
		List<Notafiscalproduto> notaFiscalList = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			
			@Override
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Notafiscalproduto n = new Notafiscalproduto(rs.getInt("cdnota"));
				n.setNumero(rs.getString("numero"));
				if(rs.getInt("cdempresa") > 0) n.setEmpresa(new Empresa(rs.getInt("cdempresa")));
				return n;
			}
		});
		
		return notaFiscalList;
	}
	
	public Notafiscalproduto findNotaByNumeroSerie(String numero, Integer serie) {
		return query()
				.select("arquivonfnota.chaveacesso, cfop.codigo, item.valoricms, arquivonf.arquivonfsituacao, arquivonfnota.dtcancelamento, " +
						"notafiscalproduto.notaStatus,notafiscalproduto.serienfe,notafiscalproduto.dtEmissao,notafiscalproduto.cdNota," +
						"material.cdmaterial,material.identificacao,material.nome")
				.leftOuterJoin("notafiscalproduto.listaItens item")
				.leftOuterJoin("item.cfop cfop")
				.leftOuterJoin("item.material material")
				.leftOuterJoin("notafiscalproduto.listaArquivonfnota arquivonfnota")
				.leftOuterJoin("arquivonfnota.arquivonf arquivonf")
				.where("notafiscalproduto.numero = ?", numero)
				.where("notafiscalproduto.serienfe = ?", serie)
				.unique();
	}
	
	public List<Notafiscalproduto> findForAutocompleteCompleteNota(String numero,String idEmpresaParametroRequisicaoAutocomplete) {
      	Integer cdpessoa = 	null;
      	if(idEmpresaParametroRequisicaoAutocomplete != null){
      		try{
      			cdpessoa = Integer.parseInt(idEmpresaParametroRequisicaoAutocomplete);
      		}catch (Exception e) {
			}
      	}
		return querySined().autocomplete()
				.select("notafiscalproduto.cdNota,notafiscalproduto.numero," +
						"empresa.cdpessoa")
				.leftOuterJoin("notafiscalproduto.empresa empresa")
				.where("empresa.cdpessoa = ?", cdpessoa)
				.whereLikeIgnoreAll("notafiscalproduto.numero",numero)
				.openParentheses()
					.where("notafiscalproduto.notaStatus =?", NotaStatus.NFE_EMITIDA)
						.or()
					.where("notafiscalproduto.notaStatus =?", NotaStatus.NFE_LIQUIDADA)
				.closeParentheses()
				.list();
	}
	
	public void updateHoraEmissao(Nota nota, Hora hora) {
		if(nota == null || nota.getCdNota() == null){
			throw new SinedException("Nota n�o pode ser nula.");
		}
		if(hora == null){
			throw new SinedException("A hora n�o pode ser nula.");
		}
		getJdbcTemplate().execute("UPDATE NOTAFISCALPRODUTO SET hremissao = '" + SinedDateUtils.toStringHoraPostgre(hora) + "' WHERE CDNOTA = " + nota.getCdNota());
	}
	public void updateSituacaoExpedicao(Notafiscalproduto nota,SituacaoVinculoExpedicao situacao) {
		if(nota == null || nota.getCdNota() == null){
			throw new SinedException("Nota n�o pode ser nula.");
		}
		Integer atalizar  = null;
		if(situacao !=null){
			 atalizar =situacao.getValue();
		}
		getJdbcTemplate().execute("update notafiscalproduto  set situacaoExpedicao = "+atalizar+" where cdNota = "+nota.getCdNota()+"");
			
	}
	
	public void updateInfoAdicionalContrib(Notafiscalproduto notafiscalproduto) {
		if (notafiscalproduto == null || notafiscalproduto.getCdNota() == null) {
			throw new SinedException("Nota Fiscal de Produto n�o pode ser nula.");
		}
        
        getJdbcTemplate().execute("update notafiscalproduto set infoadicionalcontrib = '"+notafiscalproduto.getInfoadicionalcontrib()+"' where cdNota = "+notafiscalproduto.getCdNota()+"");
    }
    
    public Notafiscalproduto carregaInfoAdicionalContrib(Notafiscalproduto notafiscalproduto) {
        if (notafiscalproduto == null || notafiscalproduto.getCdNota() == null) {
            throw new SinedException("Nota Fiscal de Produto n�o pode ser nula.");
        }
        
        return query()
                .select("notafiscalproduto.cdNota, notafiscalproduto.infoadicionalcontrib ")
                .where("notafiscalproduto.cdNota = ?", notafiscalproduto.getCdNota())
                .unique();
    }
    

	public List<Notafiscalproduto> carregarDadosMargemContribuicao(RelatorioMargemContribuicaoFiltro filtro) {
		QueryBuilder<Notafiscalproduto> query = query();
		
		List<NotaStatus> listaNotaStatus = new ArrayList<NotaStatus>();
		listaNotaStatus.add(NotaStatus.NFE_EMITIDA);
		listaNotaStatus.add(NotaStatus.EMITIDA);
		listaNotaStatus.add(NotaStatus.NFE_LIQUIDADA);
		
		query
			.select("notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.valorprodutos, notafiscalproduto.taxaVenda, " +
					"listaItens.cdnotafiscalprodutoitem, listaItens.valorbruto, listaItens.valoricms, listaItens.valoricmsst, listaItens.valoricmsstremetente, " +
					"listaItens.valoricmsstdestinatario, listaItens.valorfcp, listaItens.valorfcpst, listaItens.valorIcmsPropSub, listaItens.valorIcmsEfetiva, listaItens.valorcreditoicms, " +
					"listaItens.valoripi, listaItens.valorpis, listaItens.valorcofins, listaItens.valoriss, listaItens.valorcsll, listaItens.valorir, listaItens.valorinss, " +
					"listaItens.valoriof, listaItens.valorii, listaItens.valorfrete, listaItens.valorseguro, listaItens.valordesconto, listaItens.outrasdespesas, listaItens.valorComissao, listaItens.valorcustomaterial, " +
					"material.cdmaterial, material.nome, material.valorcusto, " +
					"materialgrupo.cdmaterialgrupo, materialgrupo.nome, " +
					"materialcategoria.cdmaterialcategoria, materialcategoria.descricao, " +
					"materialtipo.cdmaterialtipo, materialtipo.nome, " +
					"cliente.cdpessoa, cliente.nome, cliente.razaosocial, " +
					"fornecedor.cdpessoa, fornecedor.tipopessoa, fornecedor.nome, fornecedor.razaosocial, " +
					"colaborador.cdpessoa, colaborador.nome, " +
					"municipio.cdmunicipio, municipio.nome ")
				.leftOuterJoin("notafiscalproduto.empresa empresa")
				.leftOuterJoin("notafiscalproduto.cliente cliente")
				.leftOuterJoin("notafiscalproduto.colaborador colaborador")
				.leftOuterJoin("cliente.listaPessoacategoria listaPessoacategoria")
				.leftOuterJoin("listaPessoacategoria.categoria categoria")
				.leftOuterJoin("notafiscalproduto.enderecoCliente enderecoCliente")
				.leftOuterJoin("enderecoCliente.municipio municipio")
				.leftOuterJoin("notafiscalproduto.listaItens listaItens")
				.leftOuterJoin("listaItens.material material")
				.leftOuterJoin("material.listaFornecedor listaFornecedor")
				.leftOuterJoin("listaFornecedor.fornecedor fornecedor")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("material.materialtipo materialtipo")
				.leftOuterJoin("material.materialcategoria materialcategoria")
				.leftOuterJoin("notafiscalproduto.listaNotavenda listaNotavenda")
				.leftOuterJoin("notafiscalproduto.notaStatus notaStatus")
				.where("empresa = ?", filtro.getEmpresa())
				.where("notafiscalproduto.cadastrarcobranca = TRUE")
				.where("cliente = ?", filtro.getCliente())
				.where("categoria = ?", filtro.getCategoria())
				.where("material = ?", filtro.getMaterial())
				.where("materialgrupo = ?", filtro.getMaterialGrupo())
				.where("materialtipo = ?", filtro.getMaterialTipo())
				.where("materialcategoria = ?", filtro.getMaterialCategoria())
				.where("fornecedor = ?", filtro.getFornecedor())
				.where("notafiscalproduto.dtEmissao >= ?", filtro.getDtInicio())
				.where("notafiscalproduto.dtEmissao <= ?", filtro.getDtFim())
				.whereIn("notaStatus.cdNotaStatus", SinedUtil.listAndConcatenate(listaNotaStatus, "cdNotaStatus", ","));
		
		if (filtro.getCdNota() != null) {
			query
				.where("notafiscalproduto.cdNota = ?", Integer.parseInt(filtro.getCdNota()));
		}
		
		return query.list();
	}
	
	public List<Notafiscalproduto> carregarDadosMargemContribuicaoNota(RelatorioMargemContribuicaoFiltro filtro) {
		QueryBuilder<Notafiscalproduto> query = query();
		
		query
			.select("notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.valorprodutos, " +
					"cliente.cdpessoa, cliente.nome, cliente.razaosocial, cliente.tipopessoa, " +
					"listaItens.cdnotafiscalprodutoitem ")
			.leftOuterJoin("notafiscalproduto.cliente cliente")
			.leftOuterJoin("notafiscalproduto.listaItens listaItens")
			.whereIn("notafiscalproduto.cdNota", filtro.getIdsNota());
		
		return query
					.list();
	}

	public List<Notafiscalproduto> findForSIntegraRegistro61(SintegraFiltro filtro) {
		if(filtro == null)
			throw new SinedException("Par�metro inv�lido");
		
		QueryBuilder<Notafiscalproduto> query = query()
				.select("notafiscalproduto.cdNota, notafiscalproduto.dtEmissao, notafiscalproduto.serienfe, notafiscalproduto.valor, " +
						"notafiscalproduto.valorbcicms, empresa.cdpessoa, " +
						"notafiscalproduto.valoricms, notafiscalproduto.valorprodutos, notafiscalproduto.numero," +
						"listaItens.tipocobrancaicms, listaItens.valorFiscalIcms, listaItens.valorbcicms, listaItens.valorunitario, " +
						"listaItens.qtde, listaItens.valorbruto, listaItens.icms, material.cdmaterial, material.nome, material.nomenf, listaItens.reducaobcicms, " +
						"material.ncmcompleto, unidademedida.cdunidademedida, unidademedida.simbolo")
				.leftOuterJoin("notafiscalproduto.listaItens listaItens")
				.leftOuterJoin("listaItens.material material")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("notafiscalproduto.empresa empresa")
				.leftOuterJoin("notafiscalproduto.notaStatus notaStatus")
				.leftOuterJoin("notafiscalproduto.listaNotaHistorico listaNotaHistorico")
				.where("notafiscalproduto.modeloDocumentoFiscalEnum = 1")
				.where("notafiscalproduto.dtEmissao >= ?", filtro.getDtinicio())
				.where("notafiscalproduto.dtEmissao <= ?", filtro.getDtfim())
				.where("empresa = ?", filtro.getEmpresa())
				.openParentheses()
					.where("notaStatus = ?", NotaStatus.NFE_EMITIDA)
					.or()
					.where("notaStatus = ?", NotaStatus.NFE_LIQUIDADA)
					.or()
					.openParentheses()
						.where("notaStatus = ?", NotaStatus.CANCELADA)
						.openParentheses()
							.where("listaNotaHistorico.notaStatus = ?", NotaStatus.NFE_EMITIDA).or()
							.where("listaNotaHistorico.notaStatus = ?", NotaStatus.NFE_LIQUIDADA)
						.closeParentheses()
					.closeParentheses()
				.closeParentheses()
				.orderBy("notafiscalproduto.dtEmissao, notafiscalproduto.serienfe, notafiscalproduto.numero");
		
		return query.list();
	}
    
    public Notafiscalproduto loadForCalcularEntradaMaterial (Notafiscalproduto notaFiscalProduto){
    	SinedUtil.validaObjectWithExecption(notaFiscalProduto);
    	
    	return query()
    			.select("notafiscalproduto.cdNota, notafiscalproduto.valorprodutos, notafiscalproduto.outrasdespesas, notafiscalproduto.valordesconto, notafiscalproduto.valorfrete, " +
    					"notafiscalproduto.valorseguro, notafiscalproduto.valorbcicms, notafiscalproduto.valoricms, notafiscalproduto.valoricmsst, notafiscalproduto.valorpis, " +
    					"notafiscalproduto.valorcofins, notafiscalproduto.valorii, notafiscalproduto.valorfcpretidost, listaitens.cdnotafiscalprodutoitem, listaitens.qtde, listaitens.valorunitario, " +
    					"listaitens.valorfrete, listaitens.valorseguro, listaitens.valordesconto, listaitens.cfop, listaitens.valorir, listaitens.valorinss, listaitens.valorcsll, listaitens.valorbcicms, " +
    					"listaitens.valoricmsst, listaitens.valorbcipi, listaitens.valoripi, listaitens.valorbcpis, listaitens.valorpis, listaitens.valorbccofins, listaitens.valorcofins, listaitens.valorbcii, " +
    					"localarmazenagem.cdlocalarmazenagem, material.cdmaterial ,listaitens.valorfcp, listaitens.valorfcpst,listaitens.valoriof, grupotributacao.cdgrupotributacao, grupotributacao.naoconsideraricmsst")
    			.leftOuterJoin("notafiscalproduto.listaItens listaitens")
    			.leftOuterJoin("listaitens.material material")
    			.leftOuterJoin("listaitens.localarmazenagem localarmazenagem")
    			.leftOuterJoin("listaitens.grupotributacao grupotributacao")
    			.where("notafiscalproduto = ?",notaFiscalProduto)
    			.unique();
    }
    
	public List<Notafiscalproduto> findForComprovanteTEF(String whereIn) {
		return query()
				.select("notafiscalproduto.cdNota, notafiscalproduto.dtEmissao, notafiscalproduto.numero, notafiscalproduto.serienfe, notafiscalproduto.infoadicionalcontrib, " +
						"notafiscalproduto.valorprodutos, notafiscalproduto.valorfrete, notafiscalproduto.valorTotalImpostoFederal, notafiscalproduto.valorTotalImpostoEstadual, notafiscalproduto.valorTotalImpostoMunicipal, " +
						"notafiscalproduto.valordesconto, notafiscalproduto.valorseguro, notafiscalproduto.outrasdespesas, notafiscalproduto.pesoliquido, notafiscalproduto.pesobruto, " +
						"prazopagamentofatura.cdprazopagamento, prazopagamentofatura.nome, " +
						"enderecoCliente.cdendereco, enderecoCliente.substituirlogradouro, enderecoCliente.descricao, enderecoCliente.logradouro, enderecoCliente.numero, enderecoCliente.complemento, enderecoCliente.bairro, " +
						"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla, " +
						"notafiscalprodutoitem.cdnotafiscalprodutoitem, notafiscalprodutoitem.qtde, notafiscalprodutoitem.valordesconto, notafiscalprodutoitem.valorunitario, notafiscalprodutoitem.valorbruto, " +
						"material.cdmaterial, material.cdmaterial, material.nome, material.pesobruto, material.peso, " +
						"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
						"notafiscalprodutoduplicata.cdnotafiscalprodutoduplicata, notafiscalprodutoduplicata.valor, notafiscalprodutoduplicata.dtvencimento, " +
						"cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj, cliente.tipopessoa, " +
						"documentotipo.cddocumentotipo, documentotipo.nome, " +
						"empresa.cdpessoa, empresa.nome, empresa.cnpj, " +
						"enderecoEmpresa.cdendereco, enderecoEmpresa.substituirlogradouro, enderecoEmpresa.descricao, enderecoEmpresa.logradouro, enderecoEmpresa.numero, enderecoEmpresa.complemento, enderecoEmpresa.bairro, " +
						"municipioEmpresa.cdmunicipio, municipioEmpresa.nome, ufEmpresa.cduf, ufEmpresa.sigla, " +
						"telefoneEmpresa.cdtelefone, telefoneEmpresa.telefone")
				.leftOuterJoin("notafiscalproduto.empresa empresa")
				.leftOuterJoin("empresa.listaTelefone telefoneEmpresa")
				.leftOuterJoin("empresa.listaEndereco enderecoEmpresa")
				.leftOuterJoin("enderecoEmpresa.municipio municipioEmpresa")
				.leftOuterJoin("municipioEmpresa.uf ufEmpresa")
				.leftOuterJoin("notafiscalproduto.prazopagamentofatura prazopagamentofatura")
				.leftOuterJoin("notafiscalproduto.cliente cliente")
				.leftOuterJoin("notafiscalproduto.enderecoCliente enderecoCliente")
				.leftOuterJoin("enderecoCliente.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("notafiscalproduto.listaItens notafiscalprodutoitem")
				.leftOuterJoin("notafiscalprodutoitem.material material")
				.leftOuterJoin("notafiscalprodutoitem.unidademedida unidademedida")
				.leftOuterJoin("notafiscalproduto.listaDuplicata notafiscalprodutoduplicata")
				.leftOuterJoin("notafiscalprodutoduplicata.documentotipo documentotipo")
				.whereIn("notafiscalproduto.cdNota", whereIn)
				.list();
	}
    
	public List<Notafiscalproduto> findNotaCriadasViaIntegracaoECF(String whereIn) {
		return query()
					.select("notafiscalproduto.cdNota, notafiscalproduto.hremissao")
					.whereIn("notafiscalproduto.cdNota", whereIn)
					.where("notafiscalproduto.origemCupom = true")
					.list();
	}
	
	public List<Notafiscalproduto> findForGnre(String whereIn) {
		return query()
				.select("notafiscalproduto.cdNota, notafiscalproduto.localdestinonfe, notafiscalproduto.operacaonfe, notafiscalproduto.datahorasaidaentrada, " +
						"notafiscalproduto.valoricmsdestinatario, notafiscalproduto.valorfcp, notafiscalproduto.valoricmsst, notafiscalproduto.numero, " +
						"notafiscalproduto.dtEmissao, notafiscalproduto.infoadicionalcontrib, " +
						"municipio.cdmunicipio, uf.cduf, uf.sigla, empresa.cdpessoa, cliente.cdpessoa, cliente.contribuinteICMS, notaStatus.cdNotaStatus")
				.leftOuterJoin("notafiscalproduto.empresa empresa")
				.leftOuterJoin("notafiscalproduto.notaStatus notaStatus")
				.leftOuterJoin("notafiscalproduto.cliente cliente")
				.leftOuterJoin("notafiscalproduto.enderecoCliente enderecoCliente")
				.leftOuterJoin("enderecoCliente.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.whereIn("notafiscalproduto.cdNota", whereIn)
				.list();
	}
	
    public List<Notafiscalproduto> findForAutocompleteByProcessoReferenciado(String numero, Tipooperacaonota tipoOperacao,Integer empresa){
    	return query()
    				.autocomplete()
    				.select("notafiscalproduto.cdNota,notafiscalproduto.numero")
    				.leftOuterJoin("notafiscalproduto.empresa empresa")
    				.whereLikeIgnoreAll("notafiscalproduto.numero", numero)
    				.where("notafiscalproduto.tipooperacaonota=?", tipoOperacao)
    				.where("empresa.cdpessoa=?", empresa)
    				.list();
    }

	public Notafiscalproduto findEmitidaNaReceitaByVenda(Venda venda){
		return query()
				.select("notafiscalproduto.dtEmissao, notafiscalproduto.numero, notafiscalproduto.valorbcicms, notafiscalproduto.valoricms, notafiscalproduto.valorbcicmsst, " +
						"notafiscalproduto.cdNota")
				.join("notafiscalproduto.notaStatus notaStatus")
				.join("notafiscalproduto.listaNotavenda listaNotavenda")
				.join("listaNotavenda.venda venda")
				.where("venda = ?", venda)
				.where("notaStatus <> ?", NotaStatus.CANCELADA)
				/*.openParentheses()
					.where("notaStatus = ?", NotaStatus.NFE_EMITIDA)
					.or()
					.where("notaStatus = ?", NotaStatus.NFE_LIQUIDADA)
				.closeParentheses()*/
				.setMaxResults(1)
				.unique();
	}
}