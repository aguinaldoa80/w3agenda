package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendasincronizacao;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PedidovendasincronizacaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PedidovendasincronizacaoDAO extends GenericDAO<Pedidovendasincronizacao> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Pedidovendasincronizacao> query, FiltroListagem _filtro) {
		PedidovendasincronizacaoFiltro filtro = (PedidovendasincronizacaoFiltro) _filtro;
		
		query
			.select("pedidovendasincronizacao.cdpedidovendasincronizacao, pedidovendasincronizacao.sincronizado, " +
					"empresa.nome, empresa.razaosocial, empresa.nomefantasia, cliente.nome, pedidovendasincronizacao.pedidovendaIdExterno, pedidovenda.cdpedidovenda, " +
					"pedidovendasincronizacao.dtsincronizacao, pedidovendasincronizacao.dttentativasincronizacao ")
			.join("pedidovendasincronizacao.cliente cliente")
			.join("pedidovendasincronizacao.empresa empresa")
			.join("pedidovendasincronizacao.pedidovenda pedidovenda")
			.where("empresa = ?", filtro.getEmpresa())
			.where("cliente = ?", filtro.getCliente())
			.whereIn("pedidovenda.cdpedidovenda", filtro.getWhereInCdPedidoVenda());
		
		if(filtro.getSincronizado() != null && filtro.getSincronizado()){
			query.where("pedidovendasincronizacao.sincronizado = true");
		}else if(filtro.getSincronizado() != null && !filtro.getSincronizado()){
			query.where("pedidovendasincronizacao.sincronizado = false");
		}
		query.orderBy("pedidovenda.cdpedidovenda desc");
			
		
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Pedidovendasincronizacao> query) {
		query
			.select("pedidovendasincronizacao.cdpedidovendasincronizacao, pedidovendasincronizacao.pedidovendaDataEmissao, " +
					"pedidovendasincronizacao.pedidovendaDataLancamento, pedidovendasincronizacao.pedidovendaFlagTroca, " +
					"pedidovendasincronizacao.pedidovendaIdExterno, pedidovendasincronizacao.sincronizado, " +
					"pedidovendasincronizacao.clienteNome, pedidovendasincronizacao.clienteNatureza, pedidovendasincronizacao.pedidovendaObservacaointerna, " +
					"pedidovendasincronizacao.clienteDocumento, pedidovendasincronizacao.clienteFilial, " +
					"pedidovendasincronizacao.clienteEndereco, pedidovendasincronizacao.filialwmsNome, " +
					"pedidovendasincronizacao.filialwmsNatureza, pedidovendasincronizacao.filialwmsDocumento, " +
					"pedidovendasincronizacao.filialwmsFilial, pedidovendasincronizacao.enderecoLogradouro, " +
					"pedidovendasincronizacao.enderecoNumero, pedidovendasincronizacao.enderecoComplemento, " +
					"pedidovendasincronizacao.enderecoBairro, pedidovendasincronizacao.enderecoCep, " +
					"pedidovendasincronizacao.enderecoPontoreferencia, pedidovendasincronizacao.enderecoMunicipio, " +
					"pedidovendasincronizacao.enderecoUf, cliente.cdpessoa, empresa.cdpessoa, endereco.cdendereco, " +
					"pedidovenda.cdpedidovenda, pedidovendamaterialsincronizacao.cdpedidovendamaterialsincronizacao, " +
					"pedidovendamaterialsincronizacao.materialDescricao, pedidovendamaterialsincronizacao.materialReferencia, " +
					"pedidovendamaterialsincronizacao.materialDescricaoEmbalagem, pedidovendamaterialsincronizacao.materialPeso, " +
					"pedidovendamaterialsincronizacao.materialAltura, pedidovendamaterialsincronizacao.materialLargura, " +
					"pedidovendamaterialsincronizacao.materialComprimento, " +
					"pedidovendamaterialsincronizacao.materialQuantidadeVolume, pedidovendamaterialsincronizacao.materialgrupoNome, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoDeposito, pedidovendamaterialsincronizacao.pedidovendaprodutoObservacao, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoQuantidade, pedidovendamaterialsincronizacao.materialcorId, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoPrevisaoEntrega, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoTipo, pedidovendasincronizacao.dtsincronizacao, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoValorUnitario, pedidovendasincronizacao.dttentativasincronizacao, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoFlagReenvio, material.cdmaterial, material.identificacao, " +
					"materialgrupo.cdmaterialgrupo, pedidovendasincronizacao.erro, pedidovendamaterial.cdpedidovendamaterial, " +
					"pedidovendasincronizacao.errolog, pedidovendasincronizacao.contadorenvio, pedidovendasincronizacao.reenvio," +
					"listaHistorico.cdpedidovendasincronizacaohistorico, listaHistorico.observacao, listaHistorico.cdusuarioaltera, " +
					"listaHistorico.dtaltera, listaHistorico.msglog, materialmestre.cdmaterial, " +
					"pedidovendamaterialunidademedidasincronizacao.cdpedidovendamaterialunidademedidasincronizacao, pedidovendamaterialunidademedidasincronizacao.fracao, " +
					"pedidovendamaterialunidademedidasincronizacao.valorunitario, pedidovendamaterialunidademedidasincronizacao.qtdereferencia, " +
					"material2.cdmaterial, material2.nome, unidademedida.cdunidademedida, unidademedida.simbolo, materialunidademedida.cdmaterialunidademedida, " +
					"transportador.cdpessoa, transportador.nome, pedidovendasincronizacao.transportadorNome, pedidovendasincronizacao.transportadorNatureza, " +
					"pedidovendasincronizacao.transportadorCpfCnpj")			
			.leftOuterJoin("pedidovendasincronizacao.cliente cliente")
			.leftOuterJoin("pedidovendasincronizacao.empresa empresa")
			.leftOuterJoin("pedidovendasincronizacao.endereco endereco")
			.leftOuterJoin("pedidovendasincronizacao.pedidovenda pedidovenda")
			.leftOuterJoin("pedidovendasincronizacao.listaPedidovendamaterialsincronizacao pedidovendamaterialsincronizacao")
			.leftOuterJoin("pedidovendasincronizacao.listaPedidovendamaterialunidademedidasincronizacao pedidovendamaterialunidademedidasincronizacao")
			.leftOuterJoin("pedidovendamaterialunidademedidasincronizacao.material material2")
			.leftOuterJoin("pedidovendamaterialunidademedidasincronizacao.unidademedida unidademedida")
			.leftOuterJoin("pedidovendamaterialunidademedidasincronizacao.materialunidademedida materialunidademedida")
			.leftOuterJoin("pedidovendamaterialsincronizacao.pedidovendamaterial pedidovendamaterial")
			.leftOuterJoin("pedidovendamaterialsincronizacao.material material")
			.leftOuterJoin("pedidovendamaterialsincronizacao.materialmestre materialmestre")
			.leftOuterJoin("pedidovendamaterialsincronizacao.materialgrupo materialgrupo")
			.leftOuterJoin("pedidovendasincronizacao.listaHistorico listaHistorico")
			.leftOuterJoin("pedidovendasincronizacao.transportador transportador")
			.orderBy("listaHistorico.dtaltera desc");
	
		super.updateEntradaQuery(query);
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaPedidovendamaterialsincronizacao");
		save.saveOrUpdateManaged("listaPedidovendamaterialunidademedidasincronizacao");

		super.updateSaveOrUpdate(save);
	}

	/**
	 * M�todo que verifica se existe pedido de venda n�o sincronizado
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existPedidovendaNaoSincronizado(Integer contadorenvio) {
		Integer numero = newQueryBuilder(Integer.class)
				.select("pedidovendasincronizacao.cdpedidovendasincronizacao")
				.setUseTranslator(false)
				.from(Pedidovendasincronizacao.class)	
				.join("pedidovendasincronizacao.pedidovenda pedidovenda")
				.where("pedidovendasincronizacao.sincronizado = false ")
				.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.AGUARDANDO_APROVACAO)
				.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.CANCELADO)
				.where("COALESCE(pedidovendasincronizacao.contadorenvio, 0) < ?", contadorenvio)
				.setMaxResults(1)
				.unique();
		
		return numero != null;
	}
	
	/**
	 * M�todo que veririca se existe pedido de venda para ser desmarcado no wms
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean existPedidovendaDesmarcaritens() {
		Integer numero =  newQueryBuilder(Integer.class)
					.select("pedidovendasincronizacao.cdpedidovendasincronizacao")
					.setUseTranslator(false)
					.from(Pedidovendasincronizacao.class)			
					.where("pedidovendasincronizacao.desmarcaritens = true")
					.setMaxResults(1)
					.unique();
		
		
		return numero != null;
	}

	/**
	 * M�todo que busca as informa��es do pedidovendasincroniza��o para fazer a sincronica��o com o wms
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovendasincronizacao> findForSincronizacaoPedidoVendaPendente(Integer contadorenvio) {
		return query()
			.select("pedidovendasincronizacao.cdpedidovendasincronizacao, pedidovendasincronizacao.pedidovendaDataEmissao, " +
					"pedidovendasincronizacao.pedidovendaDataLancamento, pedidovendasincronizacao.pedidovendaFlagTroca, " +
					"pedidovendasincronizacao.pedidovendaIdExterno, pedidovendasincronizacao.sincronizado, " +
					"pedidovendasincronizacao.clienteNome, pedidovendasincronizacao.clienteNatureza, pedidovendasincronizacao.reenvio, " +
					"pedidovendasincronizacao.clienteDocumento, pedidovendasincronizacao.clienteFilial, pedidovendasincronizacao.pedidovendaObservacaointerna, " +
					"pedidovendasincronizacao.clienteEndereco, pedidovendasincronizacao.filialwmsNome, " +
					"pedidovendasincronizacao.filialwmsNatureza, pedidovendasincronizacao.filialwmsDocumento, " +
					"pedidovendasincronizacao.filialwmsFilial, pedidovendasincronizacao.enderecoLogradouro, " +
					"pedidovendasincronizacao.enderecoNumero, pedidovendasincronizacao.enderecoComplemento, " +
					"pedidovendasincronizacao.enderecoBairro, pedidovendasincronizacao.enderecoCep, " +
					"pedidovendasincronizacao.enderecoPontoreferencia, pedidovendasincronizacao.enderecoMunicipio, " +
					"pedidovendasincronizacao.enderecoUf, cliente.cdpessoa, empresa.cdpessoa, endereco.cdendereco, " +
					"pedidovenda.cdpedidovenda, pedidovendamaterialsincronizacao.cdpedidovendamaterialsincronizacao, " +
					"pedidovendamaterialsincronizacao.materialDescricao, pedidovendamaterialsincronizacao.materialReferencia, " +
					"pedidovendamaterialsincronizacao.materialDescricaoEmbalagem, pedidovendamaterial.cdpedidovendamaterial, " +
					"pedidovendamaterialsincronizacao.materialQuantidadeVolume, pedidovendamaterialsincronizacao.materialgrupoNome, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoDeposito,  pedidovendamaterialsincronizacao.materialPeso, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoQuantidade, pedidovendamaterialsincronizacao.materialAltura, " +
					"pedidovendamaterialsincronizacao.materialLargura, pedidovendamaterialsincronizacao.materialComprimento, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoPrevisaoEntrega, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoTipo, pedidovendamaterialsincronizacao.materialcorId, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoValorUnitario, pedidovendamaterialsincronizacao.pedidovendaprodutoFlagReenvio, " +
					"material.cdmaterial, material.identificacao, materialgrupo.cdmaterialgrupo, " +
					"localarmazenagem.cdlocalarmazenagem, projeto.cdprojeto, empresapedidovenda.cdpessoa," +
					"pedidovendamaterial.observacao, pedidovenda.observacao, pedidovendamaterialsincronizacao.pedidovendaprodutoObservacao, " +
					"materialtipo.cdmaterialtipo, arquivoLegenda.cdarquivo, materialmestre.cdmaterial, " +
					"pedidovendamaterialunidademedidasincronizacao.cdpedidovendamaterialunidademedidasincronizacao, pedidovendamaterialunidademedidasincronizacao.fracao, " +
					"pedidovendamaterialunidademedidasincronizacao.valorunitario, pedidovendamaterialunidademedidasincronizacao.qtdereferencia, " +
					"material2.cdmaterial, material2.nome, unidademedida.cdunidademedida, unidademedida.simbolo, materialunidademedida.cdmaterialunidademedida, " +
					"transportador.cdpessoa, transportador.nome, pedidovendasincronizacao.transportadorNome, pedidovendasincronizacao.transportadorNatureza, " +
					"pedidovendasincronizacao.transportadorCpfCnpj")
			.join("pedidovendasincronizacao.cliente cliente")
			.join("pedidovendasincronizacao.empresa empresa")
			.join("pedidovendasincronizacao.endereco endereco")
			.join("pedidovendasincronizacao.pedidovenda pedidovenda")
			.join("pedidovenda.empresa empresapedidovenda")
			.leftOuterJoin("pedidovendasincronizacao.listaPedidovendamaterialsincronizacao pedidovendamaterialsincronizacao")
			.join("pedidovendamaterialsincronizacao.material material")
			.leftOuterJoin("pedidovendamaterialsincronizacao.materialmestre materialmestre")
			.leftOuterJoin("material.materialtipo materialtipo")
			.leftOuterJoin("materialtipo.arquivoLegenda arquivoLegenda")
			.leftOuterJoin("pedidovendamaterialsincronizacao.materialgrupo materialgrupo")
			.join("pedidovendamaterialsincronizacao.pedidovendamaterial pedidovendamaterial")
			.leftOuterJoin("pedidovenda.localarmazenagem localarmazenagem")
			.leftOuterJoin("pedidovenda.projeto projeto")
			.leftOuterJoin("pedidovendasincronizacao.listaPedidovendamaterialunidademedidasincronizacao pedidovendamaterialunidademedidasincronizacao")
			.leftOuterJoin("pedidovendamaterialunidademedidasincronizacao.material material2")
			.leftOuterJoin("pedidovendamaterialunidademedidasincronizacao.unidademedida unidademedida")
			.leftOuterJoin("pedidovendamaterialunidademedidasincronizacao.materialunidademedida materialunidademedida")
			.leftOuterJoin("pedidovendasincronizacao.transportador transportador")
			.where("pedidovendasincronizacao.sincronizado = false")
			.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.AGUARDANDO_APROVACAO)
			.where("pedidovenda.pedidovendasituacao <> ?", Pedidovendasituacao.CANCELADO)
			.where("pedidovenda.integrar = ?", Boolean.TRUE)
			.where("COALESCE(pedidovendasincronizacao.contadorenvio, 0) < ?", contadorenvio)
			.list();

	}

	/**
	 * M�todo que seta o pedidovendasincronizacao como sincronizado
	 *
	 * @param pedidovendasincronizacao
	 * @author Luiz Fernando
	 */
	public void updatePedidovendasincronizacaoSincronizado(Pedidovendasincronizacao pedidovendasincronizacao) {
		if(pedidovendasincronizacao == null || pedidovendasincronizacao.getCdpedidovendasincronizacao() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		Timestamp dataSincronizacao = new Timestamp(System.currentTimeMillis());
		
		getHibernateTemplate().bulkUpdate("update Pedidovendasincronizacao set sincronizado = ?, reenvio = ?, dtsincronizacao = ?, erro = null where cdpedidovendasincronizacao = ?",
				new Object[]{Boolean.TRUE, Boolean.TRUE, dataSincronizacao, pedidovendasincronizacao.getCdpedidovendasincronizacao()});
	}

	/**
	 * M�todo que seta a data da tentativa de sincroniza��o
	 *
	 * @param pedidovendasincronizacao
	 * @author Luiz Fernando
	 */
	public void updatePedidovendasincronizacaoTentativaDeSincronizacao(Pedidovendasincronizacao pedidovendasincronizacao) {
		if(pedidovendasincronizacao == null || pedidovendasincronizacao.getCdpedidovendasincronizacao() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		Timestamp dataSincronizacao = new Timestamp(System.currentTimeMillis());
		
		getHibernateTemplate().bulkUpdate("update Pedidovendasincronizacao set dttentativasincronizacao = ? where cdpedidovendasincronizacao = ?",
				new Object[]{dataSincronizacao, pedidovendasincronizacao.getCdpedidovendasincronizacao()});
	}

	/**
	 * M�todo que carrega o pedidovendasincronizacao
	 *
	 * @param pedidoVenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Pedidovendasincronizacao findByPedidovenda(Pedidovenda pedidoVenda, Pedidovendasituacao pedidovendasituacao) {
		if(pedidoVenda == null || pedidoVenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("pedidovendasincronizacao.cdpedidovendasincronizacao, pedidovendasincronizacao.pedidovendaDataEmissao, " +
					"pedidovendasincronizacao.pedidovendaDataLancamento, pedidovendasincronizacao.pedidovendaFlagTroca, " +
					"pedidovendasincronizacao.pedidovendaIdExterno, pedidovendasincronizacao.sincronizado, " +
					"pedidovendasincronizacao.clienteNome, pedidovendasincronizacao.clienteNatureza, pedidovendasincronizacao.reenvio, " +
					"pedidovendasincronizacao.clienteDocumento, pedidovendasincronizacao.clienteFilial, " +
					"pedidovendasincronizacao.clienteEndereco, pedidovendasincronizacao.filialwmsNome, " +
					"pedidovendasincronizacao.filialwmsNatureza, pedidovendasincronizacao.filialwmsDocumento, " +
					"pedidovendasincronizacao.filialwmsFilial, pedidovendasincronizacao.enderecoLogradouro, " +
					"pedidovendasincronizacao.enderecoNumero, pedidovendasincronizacao.enderecoComplemento, " +
					"pedidovendasincronizacao.enderecoBairro, pedidovendasincronizacao.enderecoCep, " +
					"pedidovendasincronizacao.enderecoPontoreferencia, pedidovendasincronizacao.enderecoMunicipio, " +
					"pedidovendasincronizacao.enderecoUf, cliente.cdpessoa, empresa.cdpessoa, endereco.cdendereco, " +
					"pedidovenda.cdpedidovenda, pedidovendamaterialsincronizacao.cdpedidovendamaterialsincronizacao, " +
					"pedidovendamaterialsincronizacao.materialDescricao, pedidovendamaterialsincronizacao.materialReferencia, " +
					"pedidovendamaterialsincronizacao.materialDescricaoEmbalagem, pedidovendamaterial.cdpedidovendamaterial, " +
					"pedidovendamaterialsincronizacao.materialQuantidadeVolume, pedidovendamaterialsincronizacao.materialgrupoNome, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoDeposito,  pedidovendamaterialsincronizacao.materialPeso, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoQuantidade, pedidovendamaterialsincronizacao.materialAltura, " +
					"pedidovendamaterialsincronizacao.materialLargura, pedidovendamaterialsincronizacao.materialComprimento, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoPrevisaoEntrega, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoTipo, pedidovendamaterialsincronizacao.pedidovendaprodutoObservacao, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoValorUnitario, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoFlagReenvio, material.cdmaterial, materialgrupo.cdmaterialgrupo, " +
					"materialmestre.cdmaterial")			
			.join("pedidovendasincronizacao.cliente cliente")
			.join("pedidovendasincronizacao.empresa empresa")
			.join("pedidovendasincronizacao.endereco endereco")
			.join("pedidovendasincronizacao.pedidovenda pedidovenda")
			.leftOuterJoin("pedidovendasincronizacao.listaPedidovendamaterialsincronizacao pedidovendamaterialsincronizacao")
			.leftOuterJoin("pedidovendamaterialsincronizacao.material material")
			.leftOuterJoin("pedidovendamaterialsincronizacao.materialmestre materialmestre")
			.leftOuterJoin("pedidovendamaterialsincronizacao.materialgrupo materialgrupo")
			.leftOuterJoin("pedidovendamaterialsincronizacao.pedidovendamaterial pedidovendamaterial")
			.leftOuterJoin("pedidovendasincronizacao.listaPedidovendamaterialunidademedidasincronizacao pedidovendamaterialunidademedidasincronizacao")
			
			.where("pedidovenda = ?", pedidoVenda)
			.where("pedidovenda.pedidovendasituacao <> ?", pedidovendasituacao)
			.unique();
	}
	
	public Pedidovendasincronizacao findUpdateByPedidovenda(Pedidovenda pedidoVenda) {
		if(pedidoVenda == null || pedidoVenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("pedidovendasincronizacao.cdpedidovendasincronizacao, pedidovendasincronizacao.pedidovendaDataEmissao, " +
					"pedidovendasincronizacao.pedidovendaDataLancamento, pedidovendasincronizacao.pedidovendaFlagTroca, " +
					"pedidovendasincronizacao.pedidovendaIdExterno, pedidovendasincronizacao.sincronizado, " +
					"pedidovendasincronizacao.clienteNome, pedidovendasincronizacao.clienteNatureza, pedidovendasincronizacao.reenvio, " +
					"pedidovendasincronizacao.clienteDocumento, pedidovendasincronizacao.clienteFilial, " +
					"pedidovendasincronizacao.clienteEndereco, pedidovendasincronizacao.filialwmsNome, " +
					"pedidovendasincronizacao.filialwmsNatureza, pedidovendasincronizacao.filialwmsDocumento, " +
					"pedidovendasincronizacao.filialwmsFilial, pedidovendasincronizacao.enderecoLogradouro, " +
					"pedidovendasincronizacao.enderecoNumero, pedidovendasincronizacao.enderecoComplemento, " +
					"pedidovendasincronizacao.enderecoBairro, pedidovendasincronizacao.enderecoCep, " +
					"pedidovendasincronizacao.enderecoPontoreferencia, pedidovendasincronizacao.enderecoMunicipio, " +
					"pedidovendasincronizacao.enderecoUf, cliente.cdpessoa, empresa.cdpessoa, endereco.cdendereco, " +
					"pedidovenda.cdpedidovenda, pedidovendamaterialsincronizacao.cdpedidovendamaterialsincronizacao, " +
					"pedidovendamaterialsincronizacao.materialDescricao, pedidovendamaterialsincronizacao.materialReferencia, " +
					"pedidovendamaterialsincronizacao.materialDescricaoEmbalagem, pedidovendamaterial.cdpedidovendamaterial, " +
					"pedidovendamaterialsincronizacao.materialQuantidadeVolume, pedidovendamaterialsincronizacao.materialgrupoNome, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoDeposito,  pedidovendamaterialsincronizacao.materialPeso, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoQuantidade, pedidovendamaterialsincronizacao.pedidovendaprodutoObservacao, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoPrevisaoEntrega, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoTipo, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoValorUnitario, " +
					"pedidovendamaterialsincronizacao.pedidovendaprodutoFlagReenvio, material.cdmaterial, materialgrupo.cdmaterialgrupo ")			
			.join("pedidovendasincronizacao.cliente cliente")
			.join("pedidovendasincronizacao.empresa empresa")
			.join("pedidovendasincronizacao.endereco endereco")
			.join("pedidovendasincronizacao.pedidovenda pedidovenda")
			.leftOuterJoin("pedidovendasincronizacao.listaPedidovendamaterialsincronizacao pedidovendamaterialsincronizacao")
			.leftOuterJoin("pedidovendamaterialsincronizacao.material material")
			.leftOuterJoin("pedidovendamaterialsincronizacao.materialgrupo materialgrupo")
			.leftOuterJoin("pedidovendamaterialsincronizacao.pedidovendamaterial pedidovendamaterial")
			.where("pedidovenda = ?", pedidoVenda)
			.unique();
	}

	/**
	 * M�todo que busca o Id do pedidovendasincronizacao a partir do pedido de venda
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Integer getCdpedidovendasincronizacaoByPedidovenda(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilder(Integer.class)
			.select("pedidovendasincronizacao.cdpedidovendasincronizacao")
			.setUseTranslator(false)
			.from(Pedidovendasincronizacao.class)
			.join("pedidovendasincronizacao.pedidovenda pedidovenda")
			.where("pedidovenda = ?", pedidovenda)
			.unique();
		
	}

	/**
	 * M�todo que grava o erro no pedidovendasincronizacao
	 *
	 * @param pedidovendasincronizacao
	 * @author Luiz Fernando
	 */
	public void updatePedidovendasincronizacaoComErro(Pedidovendasincronizacao pedidovendasincronizacao) {
		if(pedidovendasincronizacao == null || pedidovendasincronizacao.getCdpedidovendasincronizacao() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		if(pedidovendasincronizacao.getErro() != null && !"".equals(pedidovendasincronizacao.getErro())){
			String erro = "";
			String errolog = "";
			if(pedidovendasincronizacao.getErro().length() > 1000)
				erro = pedidovendasincronizacao.getErro().substring(0, 1000);
			else
				erro = pedidovendasincronizacao.getErro();
			
			if(pedidovendasincronizacao.getErrolog() != null){
				errolog = pedidovendasincronizacao.getErrolog();
			}
			
			getHibernateTemplate().bulkUpdate("update Pedidovendasincronizacao set erro = ?, errolog = ?, contadorenvio = (COALESCE(contadorenvio, 0)+1) where cdpedidovendasincronizacao = ?",
					new Object[]{erro, errolog, pedidovendasincronizacao.getCdpedidovendasincronizacao()});
		}
		
	}

	/**
	 * M�todo que busca o pedidovendasincronizacao para desmarcar itens no wms
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovendasincronizacao> findForDesmarcaritensPedidoVenda() {
		return query()
			.select("pedidovendasincronizacao.cdpedidovendasincronizacao, pedidovenda.cdpedidovenda, empresa.cdpessoa, empresaPedidovenda.cdpessoa ")			
			.join("pedidovendasincronizacao.empresa empresa")
			.join("pedidovendasincronizacao.pedidovenda pedidovenda")
			.join("pedidovenda.empresa empresaPedidovenda")
			.where("pedidovendasincronizacao.desmarcaritens = true")
			.list();
	}

	/**
	 * M�todo que atualiza o pedido de venda que foi desmarcado no wms
	 *
	 * @param pedidovendasincronizacao
	 * @param desmarcaritens
	 * @author Luiz Fernando
	 */
	public void updatePedidovendaItensdesmarcados(Pedidovendasincronizacao pedidovendasincronizacao, boolean desmarcaritens) {
		if(pedidovendasincronizacao != null && pedidovendasincronizacao.getCdpedidovendasincronizacao() != null){
			getHibernateTemplate().bulkUpdate("update Pedidovendasincronizacao set desmarcaritens = ? where cdpedidovendasincronizacao = ?",
					new Object[]{desmarcaritens, pedidovendasincronizacao.getCdpedidovendasincronizacao()});
		}		
	}

	/**
	 * M�todo que atualiza o contador do sincroniza��o do pedido
	 *
	 * @param novovalorcontadorenvio
	 * @param whereIn
	 * @author Luiz Fernando
	 */
	public void updateValorcontadorenvio(Integer novovalorcontadorenvio, String whereIn) {
		if(novovalorcontadorenvio != null && whereIn != null && !"".equals(whereIn)){
			getHibernateTemplate().bulkUpdate("update Pedidovendasincronizacao set contadorenvio = ? where cdpedidovendasincronizacao in (" + whereIn + ")",
					new Object[]{novovalorcontadorenvio});
		}
	}

	/**
	 * M�todo que faz update no campo sincronizado
	 *
	 * @param pedidovendasincronizacao
	 * @param sincronizado
	 * @author Luiz Fernando
	 */
	public void updatePedidovendasincronizacaoNaoSincronizado(Pedidovendasincronizacao pedidovendasincronizacao, Boolean sincronizado) {
		if(pedidovendasincronizacao != null && pedidovendasincronizacao.getCdpedidovendasincronizacao() != null && sincronizado != null){
			getHibernateTemplate().bulkUpdate("update Pedidovendasincronizacao set sincronizado = ? where cdpedidovendasincronizacao = ?",
					new Object[]{sincronizado, pedidovendasincronizacao.getCdpedidovendasincronizacao()});
		}
		
	}
}
