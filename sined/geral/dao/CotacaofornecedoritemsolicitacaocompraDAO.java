package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritem;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritemsolicitacaocompra;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CotacaofornecedoritemsolicitacaocompraDAO extends GenericDAO<Cotacaofornecedoritemsolicitacaocompra> {

	/**
	 * Busca os itens de Cotacaofornecedoritemsolicitacaocompra a partir do Cotacaofornecedoritem
	 *
	 * @param cotacaofornecedoritem
	 * @return
	 * @author Rodrigo Freitas
	 * @since 22/05/2015
	 */
	public List<Cotacaofornecedoritemsolicitacaocompra> findByCotacaofornecedoritem(Cotacaofornecedoritem cotacaofornecedoritem) {
		if(cotacaofornecedoritem == null || cotacaofornecedoritem.getCdcotacaofornecedoritem() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
					.select("cotacaofornecedoritemsolicitacaocompra.cdcotacaofornecedoritemsolicitacaocompra, solicitacaocompra.cdsolicitacaocompra")
					.join("cotacaofornecedoritemsolicitacaocompra.solicitacaocompra solicitacaocompra")
					.where("cotacaofornecedoritemsolicitacaocompra.cotacaofornecedoritem = ?", cotacaofornecedoritem)
					.list();
	}
	
	
}
