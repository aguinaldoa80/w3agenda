package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Tabelaimposto;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.TabelaimpostoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

public class TabelaimpostoDAO extends GenericDAO<Tabelaimposto> {
	
	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Tabelaimposto> query, FiltroListagem _filtro) {
		TabelaimpostoFiltro filtro = (TabelaimpostoFiltro) _filtro;
		
		query
			.select("tabelaimposto.cdtabelaimposto, tabelaimposto.versao, tabelaimposto.dtimportacao, tabelaimposto.chave, " +
					"tabelaimposto.dtvigenciainicio, tabelaimposto.dtvigenciafim, arquivo.cdarquivo, arquivo.nome, " +
					"empresa.cdpessoa, empresa.nome, empresa.nomefantasia")
			.join("tabelaimposto.empresa empresa")
			.join("tabelaimposto.arquivo arquivo")
			.where("tabelaimposto.empresa = ?", filtro.getEmpresa())
			.orderBy("tabelaimposto.dtimportacao desc");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Tabelaimposto> query) {
		query.leftOuterJoinFetch("tabelaimposto.arquivo arquivo");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Tabelaimposto tabelaimposto = (Tabelaimposto) save.getEntity();
		
		if(tabelaimposto.getListaTabelaimpostoitem() != null && tabelaimposto.getListaTabelaimpostoitem().size() > 0) {
			save.saveOrUpdateManaged("listaTabelaimpostoitem");
		}
		if (tabelaimposto.getArquivo() != null) {
			arquivoDAO.saveFile(tabelaimposto, "arquivo");
		}
	}
	
	/**
	 * Busca as tabelas de uma empresa
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/08/2015
	 */
	public List<Tabelaimposto> findByEmpresa(Empresa empresa){
		return querySined()
				
				.select("tabelaimposto.cdtabelaimposto, arquivo.nome")
				.join("tabelaimposto.arquivo arquivo")
				.where("tabelaimposto.empresa = ?", empresa)
				.orderBy("arquivo.nome")
				.list();
	}

	/**
	 * Busca a maior data fim da vig�ncia de uma tabela.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/08/2015
	 */
	public Date getMaxDtvigenciafim() {
		return newQueryBuilderSined(Date.class)
					
					.select("max(tabelaimposto.dtvigenciafim)")
					.setUseTranslator(false)
					.from(Tabelaimposto.class)
					.unique();
	}

	/**
	 * Busca as tabelas vigentes da empresa
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @param codigoncm 
	 * @param codigoitem 
	 * @param codigonbs 
	 * @since 18/08/2015
	 */
	public List<Tabelaimposto> findVigentesByEmpresa(Empresa empresa, String[] codigos, String extipi, boolean includeItens) {
		Date currentDate = SinedDateUtils.currentDate();
		QueryBuilderSined<Tabelaimposto> query = querySined();
		
		query
					.where("tabelaimposto.empresa = ?", empresa)
					.where("tabelaimposto.dtvigenciainicio <= ?", currentDate)
					.where("tabelaimposto.dtvigenciafim >= ?", currentDate);
		
		if(includeItens){
			query
				.select("tabelaimposto.versao, tabelaimposto.cdtabelaimposto, tabelaimposto.chave, listaTabelaimpostoitem.codigo, listaTabelaimpostoitem.extipi, " +
						"listaTabelaimpostoitem.tipo, listaTabelaimpostoitem.descricao, listaTabelaimpostoitem.nacionalfederal, " +
						"listaTabelaimpostoitem.importadofederal, listaTabelaimpostoitem.estadual, listaTabelaimpostoitem.municipal")
				.join("tabelaimposto.listaTabelaimpostoitem listaTabelaimpostoitem");
		} else {
			query
				.select("tabelaimposto.cdtabelaimposto, tabelaimposto.chave, tabelaimposto.versao ");
		}
		
		if(includeItens && codigos != null && codigos.length > 0){
			query.openParentheses();
			for (int i = 0; i < codigos.length; i++) {
				if(extipi != null){
					query
						.openParentheses()
						.where("listaTabelaimpostoitem.codigo = ?", codigos[i])
						.where("listaTabelaimpostoitem.extipi = ?", extipi)
						.closeParentheses()
						.or();
				} else {
					query
						.openParentheses()
						.where("listaTabelaimpostoitem.codigo = ?", codigos[i])
						.where("listaTabelaimpostoitem.extipi is null")
						.closeParentheses()
						.or();
				}
			}
			query.closeParentheses();
		}
		
		return query.orderBy("tabelaimposto.dtimportacao desc").list();
	}

	/**
	 * Verifica se existe tabela de imposto cadastrada
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/10/2015
	 */
	public Boolean haveTabelaimposto() {
		SinedUtil.markAsReader();
		return newQueryBuilderWithFrom(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.unique() > 0;
	}
	
}

