package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Spedarquivo;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class SpedarquivoDAO extends GenericDAO<Spedarquivo> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Spedarquivo> query, FiltroListagem _filtro) {
		SpedarquivoFiltro filtro = (SpedarquivoFiltro) _filtro;
		
		if(filtro.getEmpresa() == null) query.where("1=0");
		
		query
			.select("spedarquivo.cdspedarquivo, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
					"spedarquivo.mesano, arquivo.cdarquivo, arquivo.nome, spedarquivo.dtaltera")
			.leftOuterJoin("spedarquivo.arquivo arquivo")
			.leftOuterJoin("spedarquivo.empresa empresa")
			.where("empresa = ?", filtro.getEmpresa())
			.where("spedarquivo.mesano = ?", filtro.getMesano())
			.orderBy("spedarquivo.mesano desc");
	}
	
	/**
	 * Deleta registro de Spedarquivo de acordo com a empresa e mes/ano.
	 *
	 * @param empresa
	 * @param mesano
	 * @author Rodrigo Freitas
	 */
	public void deleteByEmpresaMesano(Empresa empresa, Date mesano) {
		getHibernateTemplate().bulkUpdate("delete from Spedarquivo s where s.empresa.cdpessoa = ? and s.mesano = ?", new Object[]{empresa.getCdpessoa(), mesano});
	}

	public List<Spedarquivo> getSpedMesAnterior(Empresa e) {
		return query()
				.leftOuterJoin("spedarquivo.empresa empresa")
				.where("empresa.cdempresa", e.getCdpessoa())
				.where("date_part('month', spedarquivo.mesano) = ?", SinedDateUtils.getMes(SinedDateUtils.currentDate()) - 1)
				.where("date_part('year', spedarquivo.mesano) = ?", SinedDateUtils.getAno(SinedDateUtils.currentDate()))
				.list();
	}
	
}
