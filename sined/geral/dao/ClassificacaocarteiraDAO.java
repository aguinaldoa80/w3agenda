package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Tipocarteiratrabalho;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ClassificacaocarteiraDAO extends GenericDAO<Tipocarteiratrabalho> {
	
	/* singleton */
	private static ClassificacaocarteiraDAO instance;
	public static ClassificacaocarteiraDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ClassificacaocarteiraDAO.class);
		}
		return instance;
	}
		
}
