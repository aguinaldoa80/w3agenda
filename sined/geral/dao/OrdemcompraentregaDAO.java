package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompraentrega;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OrdemcompraentregaDAO extends GenericDAO<Ordemcompraentrega>{

	public List<Ordemcompraentrega> findByEntrega(Entrega entrega) {
		if(entrega == null || entrega.getCdentrega() == null){
			throw new SinedException("Entregan�o pode ser nula.");
		}
		return query()
				.select("ordemcompraentrega.cdordemcompraentrega, ordemcompra.cdordemcompra, entrega.cdentrega")
				.join("ordemcompraentrega.ordemcompra ordemcompra")
				.join("ordemcompraentrega.entrega entrega")
				.where("ordemcompraentrega.entrega = ?",entrega)
				.list();
	}

	public List<Ordemcompraentrega> findByOrdemcompra(Ordemcompra ordemcompra) {
		if (ordemcompra == null || ordemcompra.getCdordemcompra() == null) {
			throw new SinedException("Ordem de compra n�o pode ser nulo.");
		}
		return query()
					.select("entrega.cdentrega, listaEntregadocumento.cdentregadocumento, entrega.dtentrega, entrega.dtcancelamento, entregamaterial.qtde, " +
							"entregamaterial.valorunitario, entregamaterial.valorfrete, entregamaterial.valordesconto, " +
							"listaEntregadocumento.valorfrete, listaEntregadocumento.valordesconto, material.cdmaterial, " +
							"documento.cddocumento")
					.leftOuterJoin("ordemcompraentrega.ordemcompra ordemcompra")
					.leftOuterJoin("ordemcompraentrega.entrega entrega")	
					.leftOuterJoin("entrega.listaEntregadocumento listaEntregadocumento")
					.leftOuterJoin("listaEntregadocumento.listaEntregamaterial entregamaterial")
					.leftOuterJoin("entregamaterial.material material")
					.leftOuterJoin("entrega.listaDocumentoorigem listaDocumentoorigem")
					.leftOuterJoin("listaDocumentoorigem.documento documento")
					.where("ordemcompra = ?", ordemcompra)
					.where("entrega.dtcancelamento is null")
					.orderBy("entrega.dtentrega desc")
					.list();
	}

}
