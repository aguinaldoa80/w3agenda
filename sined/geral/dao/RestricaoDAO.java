package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Restricao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("restricao.dtrestricao")
public class RestricaoDAO extends GenericDAO<Restricao>{
	
	public Restricao restricaoDesbloquear(Contrato contrato){
		return querySined()
		.joinFetch("restricao.contrato contrato")
		.where("contrato = ?", contrato)
		.where("dtliberacao is null")
		.unique();
	}
	
	public List<Restricao> findByCliente(Cliente cliente){
		return querySined()
				.join("restricao.pessoa pessoa")
				.where("pessoa.cdpessoa = ?", cliente.getCdpessoa() )
				.list();
	}
	
	/**
	 * M�todo que verifica se o cliente tem restri��o
	 *
	 * @param whereInOportunidade
	 * @return
	 * @author Luiz Fernando
	 * @since 15/10/2013
	 */
	public Boolean isClienteWithRestricaoByOportunidade(String whereInOportunidade) {
		if(whereInOportunidade == null || "".equals(whereInOportunidade))
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilderSined(Long.class)
			
			.from(Restricao.class)
			.setUseTranslator(false) 
			.select("count(*)")
			.join("restricao.pessoa pessoa")
			.where("restricao.dtliberacao is null")
			.where("exists (select c.cdpessoa from Oportunidade o join o.cliente c where c.cdpessoa = pessoa.cdpessoa and o.cdoportunidade in (" + whereInOportunidade + ") )")
			.unique()
			.intValue() > 0;
	}

	/**
	 * M�todo que carrega restri��es de um cliente
	 * 
	 * @param cdpessoa
	 * @return
	 * @author Cleiton Amorim
	 */
	public List<Restricao> carregaRestricoesByCliente(Integer cdpessoa) {
		return querySined()
			.select("restricao.cdrestricao, restricao.dtliberacao")
			.join("restricao.pessoa pessoa")
			.where("pessoa.cdpessoa = ?", cdpessoa)						
			.list();
	}	
}
