package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Colaboradorsituacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Sindicato;
import br.com.linkcom.sined.geral.bean.enumeration.CAGED;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ColaboradorcargoFiltro;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.AlterarcargocolaboradorFilter;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.AlterarsituacaocolaboradorFilter;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.ExportarcolaboradorFiltro;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.GerarArquivoSEFIPFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.ColaboradorReportFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.EmitirautorizacaoReportFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.EntregaEPIReportFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColaboradorcargoDAO extends GenericDAO<Colaboradorcargo> {

	@Override
	public void updateListagemQuery(QueryBuilder<Colaboradorcargo> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		ColaboradorcargoFiltro filtro = (ColaboradorcargoFiltro) _filtro;
		query
			.select("colaboradorcargo.cdcolaboradorcargo, colaboradorcargo.dtinicio, colaboradorcargo.dtfim, colaborador.nome, cargo.nome, departamento.nome, colaboradorcargo.matricula")
			.leftOuterJoin("colaboradorcargo.colaborador colaborador")
			.leftOuterJoin("colaboradorcargo.cargo cargo")
			.leftOuterJoin("colaboradorcargo.empresa empresa")
			.leftOuterJoin("colaboradorcargo.projeto projeto")
			.leftOuterJoin("colaboradorcargo.departamento departamento")
			.leftOuterJoin("colaboradorcargo.regimecontratacao regimecontratacao")
			.leftOuterJoin("colaboradorcargo.colaboradorsituacao colaboradorsituacao")
			.where("colaboradorcargo.matricula = ?", filtro.getMatricula())
			.where("colaborador = ?", filtro.getColaborador())
			.where("departamento = ?", filtro.getDepartamento())
			.where("cargo = ?", filtro.getCargo())
			.where("regimecontratacao = ?", filtro.getRegimecontratacao())
			.where("empresa = ?", filtro.getEmpresa())
			.where("projeto = ?", filtro.getProjeto())
			.where("colaboradorcargo.vencimentosituacao >= ?", filtro.getVencimentosituacaoInicio())
			.where("colaboradorcargo.vencimentosituacao <= ?", filtro.getVencimentosituacaoFim())
			.openParentheses()
				.where("colaboradorcargo.dtinicio >= ?", filtro.getDtinicio()).or()
				.where("colaboradorcargo.dtfim >= ?", filtro.getDtinicio())
			.closeParentheses();
		
		if(filtro.getColaboradorSituacao() != null && !filtro.getColaboradorSituacao().isEmpty()){
			query.whereIn("colaboradorsituacao.cdcolaboradorsituacao", CollectionsUtil.listAndConcatenate(filtro.getColaboradorSituacao(), "cdcolaboradorsituacao", ","));
		}
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Colaboradorcargo> query) {
		query
			.select("colaboradorcargo.cdcolaboradorcargo, colaboradorcargo.matricula, colaboradorcargo.dtinicio, colaboradorcargo.dtfim, colaboradorcargo.observacao, colaboradorcargo.caged, " +
					"colaboradorcargo.salario, colaboradorcargo.cdusuarioaltera, colaboradorcargo.dtaltera, colaboradorcargo.codigocfip, colaboradorcargo.tiposalario, " +
					"colaboradorcargo.implementacaomedidas, colaboradorcargo.usoepiiniterrupto, colaboradorcargo.prazovalidade, projeto.cdprojeto, " +
					"colaboradorcargo.trocaprograma, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cdpessoa, colaboradorcargo.higienizacao, regimecontratacao.cdregimecontratacao, " +
					"colaborador.cdpessoa, cargo.cdcargo, departamento.cddepartamento, sindicato.cdsindicato, listaColaboradorcargoprofissiografia.dtinicio, " +
					"listaColaboradorcargoprofissiografia.dtfim, listaColaboradorcargoprofissiografia.cdcolaboradorcargoprofissiografia, cargoatividade.cdcargoatividade, " +
					"listaColaboradorcargorisco.cdcolaboradorcargorisco, listaColaboradorcargorisco.dtinicio, listaColaboradorcargorisco.dtfim, listaColaboradorcargorisco.epi, " +
					"listaColaboradorcargorisco.epc, riscotrabalho.cdriscotrabalho, exameresponsavel.cdexameresponsavel, cargoriscointensidade.cdcargoriscointensidade, " +
					"cargoriscotecnica.cdcargoriscotecnica, materialepi.cdmaterial, " +
					"listcolaboradorcargohistorico.cdcolaboradorcargohistorico, listcolaboradorcargohistorico.cdusuarioaltera, " +
					"listcolaboradorcargohistorico.dtaltera, listcolaboradorcargohistorico.acao, colaboradorcargo.vencimentosituacao, " +
					"colaboradorsituacao.cdcolaboradorsituacao, colaboradorsituacao.nome, fornecedor.cdpessoa, fornecedor.nome, " +
					"centroCusto.cdcentrocusto, centroCusto.nome")
			.leftOuterJoin("colaboradorcargo.colaborador colaborador")
			.leftOuterJoin("colaboradorcargo.cargo cargo")
			.leftOuterJoin("colaboradorcargo.departamento departamento")
			.leftOuterJoin("colaboradorcargo.sindicato sindicato")
			.leftOuterJoin("colaboradorcargo.projeto projeto")
			.leftOuterJoin("colaboradorcargo.regimecontratacao regimecontratacao")
			.leftOuterJoin("colaboradorcargo.listaColaboradorcargoprofissiografia listaColaboradorcargoprofissiografia")
			.leftOuterJoin("colaboradorcargo.empresa empresa")
			.leftOuterJoin("listaColaboradorcargoprofissiografia.cargoatividade cargoatividade")
			.leftOuterJoin("colaboradorcargo.listaColaboradorcargorisco listaColaboradorcargorisco")
			.leftOuterJoin("listaColaboradorcargorisco.materialepi materialepi")
			.leftOuterJoin("listaColaboradorcargorisco.riscotrabalho riscotrabalho")
			.leftOuterJoin("listaColaboradorcargorisco.exameresponsavel exameresponsavel")
			.leftOuterJoin("listaColaboradorcargorisco.cargoriscointensidade cargoriscointensidade")
			.leftOuterJoin("listaColaboradorcargorisco.cargoriscotecnica cargoriscotecnica")
			.leftOuterJoin("colaboradorcargo.listcolaboradorcargohistorico listcolaboradorcargohistorico")
			.leftOuterJoin("colaboradorcargo.colaboradorsituacao colaboradorsituacao")
			.leftOuterJoin("colaboradorcargo.fornecedor fornecedor")
			.leftOuterJoin("colaboradorcargo.centroCusto centroCusto");
		
	}

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaColaboradorcargorisco");
		save.saveOrUpdateManaged("listaColaboradorcargoprofissiografia");
	}

	/* singleton */
	private static ColaboradorcargoDAO instance;
	public static ColaboradorcargoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradorcargoDAO.class);
		}
		return instance;
	}

	/**
	 * Gera uma lista de colaboradorcargo para o relat�rio de colaboradores
	 *
	 * @throws SinedException - Caso o par�metro filtro seja null.
	 * @param filtro
	 * @return Lista de Colaboradorcargo
	 * @author Flavio Tavares
	 */
	public List<Colaboradorcargo> findForRelatorioColaboradores(ColaboradorReportFiltro filtro){
		if(filtro == null)
			throw new SinedException("O par�metro filtro n�o deve ser null.");

		QueryBuilder<Colaboradorcargo> query = querySined()
			.select("colaborador.nome, colaborador.cpf, departamento.nome, cargo.nome, colaboradorcargo.dtinicio, colaboradorcargo.dtfim")
			.from(Colaboradorcargo.class, "colaboradorcargo")
			.leftOuterJoin("colaboradorcargo.colaborador colaborador")
			.leftOuterJoin("colaboradorcargo.departamento departamento")
			.leftOuterJoin("colaboradorcargo.cargo cargo")
//			.leftOuterJoin("cargo.cbo cbo")
			.whereLikeIgnoreAll("colaborador.nome", filtro.getNome())
			.where("colaboradorcargo.departamento = ?", filtro.getDepartamento())
			.where("colaboradorcargo.matricula = ?", filtro.getMatricula())
			.where("colaboradorcargo.cargo = ?", filtro.getCargo())
			.where("colaborador.dtadmissao >= ?", filtro.getDtinicio())
			.where("colaborador.dtadmissao <= ?", filtro.getDtfim())
			.where("colaborador.recontratar = ?",filtro.getRecontratar())
			.where("colaborador.colaboradorsituacao = ?",filtro.getColaboradorsituacao());
			if(filtro.getEmpresa() != null) {
				query.where("colaboradorcargo.cargo = (select max(cc.cargo) from colaboradorcargo cc join cc.colaborador c where c.cdpessoa = colaborador.cdpessoa)");
				query.where("colaboradorcargo.empresa = ?", filtro.getEmpresa());
			}
			query.orderBy("colaborador.nome");
		
		
		if(filtro.getColaboradorsituacao() != null && filtro.getColaboradorsituacao().getCdcolaboradorsituacao().equals(Colaboradorsituacao.DEMITIDO)){
			query.where("colaboradorcargo.cdcolaboradorcargo = (select max(cc.cdcolaboradorcargo) from Colaboradorcargo cc join cc.colaborador c where c.id = colaborador.id)");
		} 
		
		
		return query.list();
	}

	/**
	 * Gera uma lista de colaboradores para a emiss�o de
	 *
	 * @throws SinedException - Caso o par�metro filtro seja null.
	 * @param filtro
	 * @return Lista de Colaboradorcargo
	 * @author Jo�o Paulo Zica
	 */
	public List<Colaboradorcargo> findForAutorizacaoExame(EmitirautorizacaoReportFiltro filtro){
		if(filtro == null)
			throw new SinedException("O par�metro filtro n�o deve ser null.");

		return query()
		.leftOuterJoinFetch("colaboradorcargo.colaborador colaborador")
		.leftOuterJoinFetch("colaboradorcargo.cargo cargo")
		.leftOuterJoinFetch("colaborador.ufctps uf")
		.leftOuterJoinFetch("colaborador.endereco endereco")
		.leftOuterJoinFetch("colaborador.sexo sexo")
		.leftOuterJoinFetch("endereco.municipio municipio")
		.leftOuterJoinFetch("municipio.uf uf")
		.leftOuterJoinFetch("colaborador.colaboradorsituacao colaboradorsituacao")
		.whereLikeIgnoreAll("colaborador.nome", filtro.getNome())
		.where("colaborador.cpf = ?", filtro.getCpf())
		.where("departamento = ?", filtro.getDepartamento())
		.where("cargo = ?", filtro.getCargo())
		.where("dtinicio >=", filtro.getDtinicio())
		.where("dtinicio <=", filtro.getDtfim())
		.list();
	}

	/**
	 * M�todo para alterar o cargo de um colaborador.
	 * Se o par�metro inserirHistorico for true, o cargo antigo sera "inserido no hist�rico",
	 * ou seja, ser� inserido um novo registro na tabela "COLABORADORCARGO" com os dados do novo cargo.
	 * Caso contr�rio, se inserirHistorico for false, o registro ser� apenas atualizado para
	 * o novo cargo.
	 *
	 * @param colaborador
	 * @param inserirHistorico
	 * @author Flavio
	 */
	public void alterarCargoColaborador(Colaborador colaborador) {
		if(colaborador==null || colaborador.getCdpessoa()==null){
			throw new SinedException("Par�metros inv�lidos em colaborador .");
		}
		String sql = null;
		Object[] campos = null;

		if(colaborador.getInserirHistorico()){
			// SETA O CAMPO DTFIM PARA A DATA ATUAL
			sql = "update colaboradorcargo "+
						 "set dtfim = ?, " +
						 "observacao = ?," +
						 "dtaltera = ?," +
						 "cdusuarioaltera = ? "+
						 "where cdcolaboradorcargo = " +
						 	"(select max(cdcolaboradorcargo) from colaboradorcargo " +
						 	"where cdpessoa = ? and dtfim is null)";
			campos = new Object[]{colaborador.getDttroca(),colaborador.getObservacao(),new Date(System.currentTimeMillis()),SinedUtil.getUsuarioLogado().getCdpessoa(),colaborador.getCdpessoa()};
			getJdbcTemplate().update(sql,campos);

			// SALVA UM NOVO REGISTRO COM OS DADOS DO NOVO CARGO
			Colaboradorcargo colaboradorcargo = makeColaboradorcargo(colaborador);
			this.saveOrUpdate(colaboradorcargo);
		}else{
			// ATUALIZA OS DADOS DO NOVO CARGO
			sql = "update colaboradorcargo " +
					"set cdcargo = ?, " +
					"cddepartamento = ? " +
					"where cdcolaboradorcargo = " +
						"(select cdcolaboradorcargo from colaboradorcargo " +
						"where cdpessoa = ? and dtfim is null)";
			campos = new Object[]{colaborador.getCargotransient().getCdcargo(),colaborador.getDepartamentotransient().getCddepartamento(),
					colaborador.getCdpessoa()};
			getJdbcTemplate().update(sql,campos);
		}

	}

	/**
	 * M�todo para criar um bean Colaboradorcargo com os dados de um bean Colaborador.
	 *
	 * @param colaborador
	 * @return Colaboradorcargo
	 * @author Flavio
	 */
	private Colaboradorcargo makeColaboradorcargo(Colaborador colaborador){
		if(colaborador==null){
			throw new SinedException("O par�metro colaborador n�o pode ser null.");
		}
		Colaboradorcargo colaboradorcargo = new Colaboradorcargo();
		colaboradorcargo.setColaborador(colaborador);
		colaboradorcargo.setRegimecontratacao(colaborador.getRegimecontratacao());
		colaboradorcargo.setCargo(colaborador.getCargotransient());
		colaboradorcargo.setDepartamento(colaborador.getDepartamentotransient());
		colaboradorcargo.setDtinicio(colaborador.getDtinicio());
		colaboradorcargo.setEmpresa(colaborador.getEmpresa());
//		colaboradorcargo.setObservacao(colaborador.getObservacao());
		colaboradorcargo.setSalario(colaborador.getSalario());
		colaboradorcargo.setDtinicio(colaborador.getDttroca());
		colaboradorcargo.setSindicato(colaborador.getSindicato());
		colaboradorcargo.setFornecedor(colaborador.getFornecedor());
		return colaboradorcargo;
	}

	/**
	 * Metodo para obter lista de colaboradorcargo.
	 * Usado para listagem de colaboradores para altera��o de cargo.
	 *
	 * @param filtro
	 * @return List<Colaboradorcargo>
	 * @author Flavio Tavares
	 */
	public List<Colaboradorcargo> findColaboradorcargoAltera(AlterarcargocolaboradorFilter filtro){
		if(filtro==null){
			throw new SinedException("O par�metro filtro n�o pode ser null.");
		}
		
		return query()
					.select("distinct colaboradorcargo.colaborador, colaboradorcargo.vencimentosituacao")
					.from(Colaboradorcargo.class, "colaboradorcargo")
					.join("colaboradorcargo.colaborador colaborador")
					.join("colaboradorcargo.departamento departamento")
					.join("colaboradorcargo.cargo cargo")
					.leftOuterJoin("colaboradorcargo.sindicato sindicato")
					.leftOuterJoin("colaborador.colaboradorsituacao colaboradorsituacao")
					.whereLikeIgnoreAll("colaborador.nome", filtro.getNome())
					.where("colaboradorcargo.matricula = ?",filtro.getMatricula())
					.where("colaborador.cpf = ?",filtro.getCpf())
					.where("departamento = ?",filtro.getDepartamentoAtual())
					.where("cargo = ?",filtro.getCargoAtual())
					.whereIn("colaboradorsituacao.cdcolaboradorsituacao", CollectionsUtil.listAndConcatenate(filtro.getColaboradorSituacao(), "cdcolaboradorsituacao", ","))
					.where("dtfim is null")
					.where("colaboradorcargo.vencimentosituacao >= ?", filtro.getVencimentosituacaoInicio())
					.where("colaboradorcargo.vencimentosituacao <= ?", filtro.getVencimentosituacaoFim())
					.orderBy(filtro.getOrderBy()+(filtro.isAsc()?" asc":" desc"))
					.list();
	}


	/**
	 * M�todo para localiza��o de colaborador para altera��o de situa��o
	 *
	 * @param filtro
	 * @author Jo�o Paulo Zica
	 */
	public List<Colaboradorcargo> findColaboradorAltera(AlterarsituacaocolaboradorFilter filtro){
		if (filtro ==  null){
			throw new SinedException("O par�metro filtro n�o pode ser null");
		}
		return
		query()
			.select("distinct colaborador.cdpessoa, colaborador.nome, colaborador.cpf, colaboradorsituacao.nome, colaborador.dtadmissao, regimecontratacao.nome, colaboradorcargo.vencimentosituacao")
			.from(Colaboradorcargo.class, "colaboradorcargo")
			.leftOuterJoin("colaboradorcargo.colaborador colaborador")
			.leftOuterJoin("colaboradorcargo.cargo cargo")
			.leftOuterJoin("colaboradorcargo.departamento departamento")
			.leftOuterJoin("colaborador.colaboradorsituacao colaboradorsituacao")
			.leftOuterJoin("colaboradorcargo.regimecontratacao regimecontratacao")
			.whereLikeIgnoreAll("colaborador.nome", filtro.getNome())
			.where("colaboradorcargo.matricula = ?",filtro.getMatricula())
			.where("colaborador.cpf = ?",filtro.getCpf())
			.where("departamento = ?", filtro.getDepartamento())
			.where("cargo = ?", filtro.getCargo())
			.where("colaborador.dtadmissao >= ?", filtro.getDtadminicio())
			.where("colaborador.dtadmissao <= ?", filtro.getDtadmfim())
			.where("colaboradorsituacao = ?", filtro.getColaboradorsituacao())
			.where("regimecontratacao = ?",filtro.getRegimecontratacao())
			.where("colaboradorcargo.dtinicio = (select max(cc.dtinicio) FROM Colaboradorcargo cc JOIN cc.colaborador c WHERE c.cdpessoa = colaborador.cdpessoa)")
			.where("colaboradorcargo.cdcolaboradorcargo = (select max(cc.cdcolaboradorcargo) FROM Colaboradorcargo cc JOIN cc.colaborador c WHERE c.cdpessoa = colaborador.cdpessoa)")
			/* Os WHEREs abaixo impedem que o usu�rio filtre colaboradores com situacao 'Demitido'
			.openParentheses()
			.where("colaboradorcargo.dtfim is null")
			.or()
			.where("colaboradorsituacao = ?", new Colaboradorsituacao(Colaboradorsituacao.DEMITIDO))
			.closeParentheses()
			*/
			.where("colaboradorcargo.vencimentosituacao >= ?", filtro.getVencimentosituacaoInicio())
			.where("colaboradorcargo.vencimentosituacao <= ?", filtro.getVencimentosituacaoFim())
			.orderBy(filtro.getOrderBy()+(filtro.isAsc()?" asc":" desc"))
			.list();
	}

	/**
	 * M�todo para localiza��o de colaborador para exporta��o
	 *
	 * @param filtro
	 * @author Jo�o Paulo Zica
	 */
	public List<Colaboradorcargo> findColaboradorExport(ExportarcolaboradorFiltro filtro){
		if (filtro ==  null){
			throw new SinedException("O par�metro filtro n�o pode ser null");
		}
		return
		query()
			.leftOuterJoinFetch("colaboradorcargo.colaborador colaborador")
			.leftOuterJoinFetch("colaboradorcargo.cargo cargo")
			.leftOuterJoinFetch("colaborador.ufctps uf")
			.leftOuterJoinFetch("colaborador.endereco endereco")
			.leftOuterJoinFetch("endereco.municipio municipio")
			.leftOuterJoinFetch("municipio.uf uf")
			.leftOuterJoinFetch("colaborador.colaboradorsituacao colaboradorsituacao")
			.whereLikeIgnoreAll("colaborador.nome", filtro.getNome())
			.where("departamento = ?", filtro.getDepartamento())
			.where("cargo = ?", filtro.getCargo())
			.where("dtinicio >=", filtro.getDtinicio())
			.where("dtinicio <=", filtro.getDtfim())
			.list();
	}

	/**
	 * M�todo para montar a lista de hist�rico profissional
	 * 
	 * @param colaborador
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public List<Colaboradorcargo> findHistoricoProfissional(Colaborador colaborador){
		if(colaborador==null || colaborador.getCdpessoa()==null){
			throw new SinedException("Par�metros inv�lidos em colaborador.");
		}
		return 
			query()
			.select("colaboradorcargo.cdcolaboradorcargo,colaboradorcargo.colaborador,colaboradorcargo.regimecontratacao,colaboradorcargo.cargo,colaboradorcargo.departamento," +
					"colaboradorcargo.dtinicio,colaboradorcargo.dtfim,colaboradorcargo.observacao,colaboradorcargo.salario,colaboradorcargo.caged," +
					"sindicato.cdsindicato,colaboradorcargo.tiposalario,colaboradorcargo.codigocfip,colaboradorcargo.implementacaomedidas,colaboradorcargo.usoepiiniterrupto," +
					"colaboradorcargo.prazovalidade,colaboradorcargo.trocaprograma,colaboradorcargo.higienizacao, " +
					"projeto.cdprojeto, projeto.nome, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cdpessoa")
			.leftOuterJoin("colaboradorcargo.regimecontratacao regimecontratacao")
			.leftOuterJoin("colaboradorcargo.cargo cargo")
			.leftOuterJoin("colaboradorcargo.empresa empresa")
			.leftOuterJoin("colaboradorcargo.departamento departamento")
			.leftOuterJoin("colaboradorcargo.colaborador colaborador")
			.leftOuterJoin("colaboradorcargo.sindicato sindicato")
			.leftOuterJoin("colaboradorcargo.projeto projeto")
			.where("colaboradorcargo.dtfim is not null")
			.where("colaboradorcargo.colaborador.cdpessoa = ?",colaborador.getCdpessoa())
			.orderBy("colaboradorcargo.dtinicio,colaboradorcargo.dtfim")
			.list();
	}
	
	/**
	 * M�todo para localizar o cargo atual
	 * 
	 * @param colaborador
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public Colaboradorcargo findCargoAtual(Colaborador colaborador){
		if(colaborador == null || colaborador.getCdpessoa()==null){
			throw new SinedException("O par�metro colaborador e cdpessoa n�o podem ser null.");
		}
		
		StringBuilder campos = new StringBuilder();
		campos.append("colaboradorcargo.cdcolaboradorcargo, colaboradorcargo.dtaltera, colaboradorcargo.cdusuarioaltera,");
		campos.append("colaboradorcargo.codigocfip, colaboradorcargo.implementacaomedidas, colaboradorcargo.usoepiiniterrupto, colaboradorcargo.prazovalidade,");
		campos.append("colaboradorcargo.trocaprograma, colaboradorcargo.higienizacao, colaboradorcargo.tiposalario, colaboradorcargo.caged, colaboradorcargo.matricula,");
		campos.append("colaboradorcargo.regimecontratacao, cargo.cdcargo, cargo.nome, cargo.codigoFolha,");
		campos.append("colaboradorcargo.dtinicio, colaboradorcargo.dtfim, colaboradorcargo.salario,");
		campos.append("colaboradorcargo.observacao, sindicato.cdsindicato, sindicato.nome, sindicato.codigofolha, projeto.cdprojeto, projeto.nome,");
		campos.append("colaborador.cdpessoa, colaborador.nome, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cdpessoa, ");
		campos.append("departamento.cddepartamento, departamento.nome, departamento.descricao, departamento.codigofolha, ");
		campos.append("colaboradorsituacao.cdcolaboradorsituacao, colaboradorsituacao.nome ");
		
		return query()
			.select(campos.toString())
			.leftOuterJoin("colaboradorcargo.regimecontratacao regimecontratacao")
			.leftOuterJoin("colaboradorcargo.empresa empresa")
			.join("colaboradorcargo.cargo cargo")
			.leftOuterJoin("colaboradorcargo.departamento departamento")
			.join("colaboradorcargo.colaborador colaborador")
			.leftOuterJoin("cargo.cbo cbo")
			.leftOuterJoin("colaboradorcargo.sindicato sindicato")
			.leftOuterJoin("colaboradorcargo.projeto projeto")
			.leftOuterJoin("colaboradorcargo.colaboradorsituacao colaboradorsituacao")
			.where("colaboradorcargo.dtfim is null")
			.where("colaborador.cdpessoa = ?",colaborador.getCdpessoa())
			.setMaxResults(1)
			.unique();
	}
	
	/**
	 * M�todo para obter o departamento, cargo, data de admiss�o e regime contrata��o atuais do colaborador
	 * 
	 * @param colaborador
	 * @return
	 * @author Flavio
	 */
	public Colaboradorcargo loadDadosCargo(Colaborador colaborador){
		return 
			query()
			.select("colaboradorcargo.cdcolaboradorcargo,colaboradorcargo.colaborador,colaboradorcargo.departamento, colaboradorcargo.cargo, colaboradorcargo.regimecontratacao," +
					"colaboradorcargo.dtinicio,colaboradorcargo.salario,colaboradorcargo.observacao, colaboradorcargo.cdusuarioaltera,colaboradorcargo.dtaltera, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cdpessoa")
			.leftOuterJoin("colaboradorcargo.colaborador colaborador")
			.leftOuterJoin("colaboradorcargo.departamento departamento")
			.leftOuterJoin("colaboradorcargo.empresa empresa")
			.leftOuterJoin("colaboradorcargo.cargo cargo")
			.leftOuterJoin("colaboradorcargo.regimecontratacao regimecontratacao")
			.where("colaboradorcargo.colaborador.cdpessoa = ?",colaborador.getCdpessoa())
			.unique();
	}
	
	
	/**
	 * M�todo para carregar determinados campos de um colaboradorcargo informados pelo pr�metros.
	 * 
	 * @param bean
	 * @param campos
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Colaboradorcargo load(Colaboradorcargo bean, String campos) {
		if(bean==null || bean.getColaborador()==null){
			throw new SinedException("Os par�metros bean e cdcolaboradorcargo n�o podem ser null.");
		}
		QueryBuilder<Colaboradorcargo> query = query();
		
		if(campos!=null && !campos.equals("")){
			query.select(campos);
		}
		
		return query
				.where("colaboradorcargo.cdcolaboradorcargo=?",bean.getCdcolaboradorcargo())
				.unique();
	}
	
	
	/**
	 * M�todo para obter o sindicato de um colaboradorcargo
	 * 
	 * @param bean
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Colaboradorcargo findSindicatoCargoAtual(Colaboradorcargo bean){
		if(bean == null || bean.getColaborador() == null){
			throw new SinedException("O par�metro bean e colaborador n�o podem ser null.");
		}
		return query()
			.select("sindicato.cdsindicato,sindicato.nome")
			.from(Colaboradorcargo.class,"colaboradorcargo")
			.leftOuterJoin("colaboradorcargo.sindicato sindicato")
			.where("colaboradorcargo.colaborador.cdpessoa = ?",bean.getColaborador().getCdpessoa())
			.where("colaboradorcargo.dtfim is null")
			.unique();
	}

	/**
	 * M�todo que verifica se h� algum colaborador cargo cadastrado com a data de in�cio
	 * 
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeColaboradorCargoData(Colaboradorcargo bean) {
		if(bean == null){
			throw new SinedException("O par�metros inv�lidos.");
		}
		QueryBuilder<Long> query = newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(beanClass)
			.join("colaboradorcargo.colaborador colaborador")
			.where("colaboradorcargo.dtinicio >= ?", bean.getDtinicio())
			.where("colaboradorcargo.dtfim <= ?", bean.getDtfim())
			.where("colaborador = ?", bean.getColaborador());
		
		if(bean.getCdcolaboradorcargo() != null)
			query.where("colaboradorcargo <> ?", bean);
		
		return query.unique() > 0;
	}

	/**
	 * M�todo que carrega dados dos cargos dos colaboradores para Formul�rio PPP
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Colaboradorcargo> carregaColaboradoresCargos(String whereIn) {
		if(whereIn == null || whereIn.equals(""))
			throw new SinedException("Par�metros inv�lidos.");
		return query()
			.select("colaboradorcargo.cdcolaboradorcargo, colaboradorcargo.dtinicio, colaboradorcargo.dtfim, colaboradorcargo.codigocfip, " +
					"colaboradorcargo.implementacaomedidas, colaboradorcargo.usoepiiniterrupto, colaboradorcargo.prazovalidade, colaboradorcargo.trocaprograma, " +
					"colaboradorcargo.higienizacao, cargo.cdcargo, cargo.nome, cargo.descricao, cbo.cdcbo, cbo.nome, departamento.cddepartamento, departamento.nome, " +
					"listaColaboradorcargoprofissiografia.cdcolaboradorcargoprofissiografia, listaColaboradorcargoprofissiografia.dtinicio, listaColaboradorcargoprofissiografia.dtfim, " +
					"cargoatividade.cdcargoatividade, cargoatividade.descricao, listaColaboradorcargorisco.cdcolaboradorcargorisco, listaColaboradorcargorisco.dtinicio, " +
					"listaColaboradorcargorisco.dtfim, listaColaboradorcargorisco.epi, listaColaboradorcargorisco.epc, riscotrabalho.cdriscotrabalho, " +
					"riscotrabalho.nome, riscotrabalhotipo.cdriscotrabalhotipo, riscotrabalhotipo.nome, cargoriscointensidade.cdcargoriscointensidade, cargoriscointensidade.nome, " +
					"cargoriscotecnica.cdcargoriscotecnica, cargoriscotecnica.nome, exameresponsavel.cdexameresponsavel, exameresponsavel.nit, exameresponsavel.nome, exameresponsavel.registroconselho, " +
					"materialepi.cdmaterial, materialepi.nome, materialepi.caepi, projeto.cdprojeto, projeto.cnpj, projeto.cei, colaboradorcargo.matricula, "+
					"colaborador.cdpessoa, colaborador.nome")
			.join("colaboradorcargo.cargo cargo")
			.join("colaboradorcargo.departamento departamento")
			.leftOuterJoin("cargo.cbo cbo")
			.leftOuterJoin("colaboradorcargo.listaColaboradorcargoprofissiografia listaColaboradorcargoprofissiografia")
			.leftOuterJoin("colaboradorcargo.listaColaboradorcargorisco listaColaboradorcargorisco")
			.leftOuterJoin("colaboradorcargo.projeto projeto")
			.leftOuterJoin("colaboradorcargo.colaborador colaborador")
			.leftOuterJoin("listaColaboradorcargorisco.riscotrabalho riscotrabalho")
			.leftOuterJoin("listaColaboradorcargorisco.cargoriscointensidade cargoriscointensidade")
			.leftOuterJoin("listaColaboradorcargorisco.exameresponsavel exameresponsavel")
			.leftOuterJoin("listaColaboradorcargorisco.cargoriscotecnica cargoriscotecnica")
			.leftOuterJoin("listaColaboradorcargorisco.materialepi materialepi")
			.leftOuterJoin("riscotrabalho.riscotrabalhotipo riscotrabalhotipo")
			.leftOuterJoin("listaColaboradorcargoprofissiografia.cargoatividade cargoatividade")
			.whereIn("colaboradorcargo.cdcolaboradorcargo", whereIn)
			.orderBy("colaboradorcargo.dtinicio, listaColaboradorcargorisco.dtinicio, listaColaboradorcargoprofissiografia.dtinicio")
			.list();
	}
	
	
	/**
	 * Carrega os cargos anteriores, que est�o em aberto, de um colaborador .
	 * @param colaborador
	 * @return
	 * @author Taidson
	 * @since 01/06/2010
	 */
	public List<Colaboradorcargo> loadCargosAnteriores(Colaborador colaborador){
		return 
			query()
			.select("colaboradorcargo.cdcolaboradorcargo")
			.leftOuterJoin("colaboradorcargo.colaborador colaborador")
			.where("colaboradorcargo.colaborador.cdpessoa = ?",colaborador.getCdpessoa())
			.where("colaboradorcargo.dtfim is null")
			.list();
	}
	
	/**
	 * Seta a data atual na dtfim do �ltimo cargo anterior do colaborador.
	 * @param colaboradorcargo
	 * @return
	 * @author Taidson
	 * @since 27/05/2010
	 */
	public void setDtfimCargoAnterior(Colaboradorcargo colaboradorcargo){
		
		String query ="update Colaboradorcargo set dtfim = ? where cdcolaboradorcargo = ?" ;
		
		getHibernateTemplate().bulkUpdate(
				query, new Object[]{new Date(System.currentTimeMillis()), colaboradorcargo.getCdcolaboradorcargo()}
		);	
	}
	
	/**
	 * Retorno uma lista de cargos de um determinado projeto
	 * @param projeto
	 * @return
	 * @author Taidson
	 * @since 18/06/2010
	 */
	public List<Colaboradorcargo> cargosProjeto(Projeto projeto){
		return 
			query()
		.select("colaboradorcargo.cdcolaboradorcargo, projeto.cdprojeto, projeto.nome, cargo.cdcargo, cargo.nome")
		.join("colaboradorcargo.cargo cargo")
		.leftOuterJoin("colaboradorcargo.projeto projeto")
		.where("projeto = ?", projeto)
		.orderBy("cargo.nome")
		.list();
	}
	
	/**
	 * Retorno uma lista de colaboradores de um determinado cargo
	 * @param cargo
	 * @return
	 * @author Taidson
	 * @since 18/06/2010
	 */
	public List<Colaboradorcargo> colaboradoresCargo(Cargo cargo){
		return 
		query()
		.select("colaboradorcargo.cdcolaboradorcargo, cargo.cdcargo, colaborador.cdpessoa, colaborador.nome")
		.join("colaboradorcargo.cargo cargo")
		.join("colaboradorcargo.colaborador colaborador")
		.leftOuterJoin("colaboradorcargo.projeto projeto")
		.where("cargo = ?", cargo)
//		.where("projeto is not null")
		.orderBy("colaborador.nome")
		.list();
	}
	
	/**
	 * M�todo que busca a lista de colaboradores de acordo com cargo e empresa
	 *
	 * @param cargo
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Colaboradorcargo> colaboradoresCargo(Cargo cargo, Empresa empresa){
		return query()
			.select("colaboradorcargo.cdcolaboradorcargo, cargo.cdcargo, colaborador.cdpessoa, colaborador.nome")
			.join("colaboradorcargo.cargo cargo")
			.join("colaboradorcargo.colaborador colaborador")
			.leftOuterJoin("colaboradorcargo.projeto projeto")
			.leftOuterJoin("colaboradorcargo.empresa empresa")
			.where("cargo = ?", cargo)
			.where("empresa = ?", empresa)
			.orderBy("colaborador.nome")
			.list();
	}
	
	/**
	 * Retorno dados para relat�rio de Controle de Entrega de EPI
	 * @param filtro
	 * @return
	 * @author Taidson
	 * @since 18/06/2010
	 */
	public List<Colaboradorcargo> findForRelatorioEntregaEPI(EntregaEPIReportFiltro filtro, String whereIn) {
		if (filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		
		QueryBuilder<Colaboradorcargo> query = querySined()
					.select("colaborador.cdpessoa, colaborador.nome, colaborador.dtadmissao, colaborador.ctps," +
							"colaboradortipo.nome, projeto.nome, listaColaboradorequipamento.cdcolaboradorequipamento, " +
							"cargo.nome, listaColaboradorequipamento.dtentrega, listaColaboradorequipamento.qtde, " +
							"epi.nomereduzido, epi.nome, colaborador.cpf, epi.duracao, epiCargo.nome, epiCargo.nomereduzido, " +
							"epiCargo.duracao, listaCargomaterialseguranca.quantidade, epi.cdmaterial, epiCargo.cdmaterial, " +
							"listaColaboradorequipamento.dtvencimento, listaColaboradorequipamento.certificadoaprovacao ")
					.join("colaboradorcargo.cargo cargo")
					.join("colaboradorcargo.colaborador colaborador")
					.leftOuterJoin("colaborador.colaboradortipo colaboradortipo")
					.leftOuterJoin("colaboradorcargo.projeto projeto")
					.leftOuterJoin("colaboradorcargo.empresa empresa")
					
					.leftOuterJoin("colaborador.listaColaboradorequipamento listaColaboradorequipamento")
					.leftOuterJoin("listaColaboradorequipamento.epi epi")
					
					.leftOuterJoin("cargo.listaCargomaterialseguranca listaCargomaterialseguranca")
					.leftOuterJoin("listaCargomaterialseguranca.epi epiCargo")
			
					.where("colaborador = ?", filtro.getColaborador())
					.where("projeto = ?", filtro.getProjeto())
					.where("cargo = ?", filtro.getCargo())
					.where("empresa = ?", filtro.getEmpresa())
					
					.whereIn("colaborador.cdpessoa", whereIn)
					
					.where("colaboradorcargo.dtfim is null")
					.where("listaColaboradorequipamento.dtdevolucao is null")
					
					.where("listaColaboradorequipamento.dtentrega >= ?", filtro.getDtinicio())
					.where("listaColaboradorequipamento.dtentrega <= ?", filtro.getDtfim());
		
		if(filtro.getOrderBy() != null && !filtro.getOrderBy().equals("")){
			query.orderBy(filtro.getOrderBy() + (filtro.isAsc() ? " asc":" desc") + ", listaColaboradorequipamento.dtentrega");
		} else {
			query.orderBy("colaborador.nome, listaColaboradorequipamento.dtentrega");
		}
		
		return query.list();
	}
	
	/**
	 * Seta a data da demiss�o na data fim dos cargos, somente para os cargos
	 * em que a data fim esteja nula.
	 * @param colaborador
	 * @param dtdemissao
	 * @author Taidson
	 * @since 20/08/2010
	 */
	public void demissaoColaborador(Colaborador colaborador) {
		if(colaborador==null || colaborador.getCdpessoa()==null){
			throw new SinedException("Par�metros inv�lidos em colaborador .");
		}
		String sql = null;
		Object[] campos = null;

		sql = "update colaboradorcargo "+
					 "set dtfim = ? " +
					 "where cdpessoa = ? and dtfim is null";
		campos = new Object[]{colaborador.getDtdemissao(), colaborador.getCdpessoa()};
		getJdbcTemplate().update(sql,campos);
	}

	public List<Colaboradorcargo> findForAutocompleteColaborador(String s) {
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		
		return query()
					.select("colaboradorcargo.cdcolaboradorcargo, colaboradorcargo.matricula, colaborador.nome")
					.join("colaboradorcargo.colaborador colaborador")
					.openParentheses()
					.where("UPPER(" + funcaoTiraacento + "(colaboradorcargo.matricula)) LIKE ?||'%'", Util.strings.tiraAcento(s).toUpperCase())
					.or()
					.where("UPPER(" + funcaoTiraacento + "(colaborador.nome)) LIKE ?||'%'", Util.strings.tiraAcento(s).toUpperCase())
					.closeParentheses()
					.list();
	}

	public void updateDtfim(Colaboradorcargo colaboradorcargo, Date dtfim) {
		getHibernateTemplate().bulkUpdate("update Colaboradorcargo c set c.dtfim = ? where c.id = ?", 
				new Object[]{dtfim, colaboradorcargo.getCdcolaboradorcargo()});
	}

	public void updateCaged(Colaboradorcargo colaboradorcargo, CAGED caged) {
		getHibernateTemplate().bulkUpdate("update Colaboradorcargo c set c.caged = ? where c.id = ?", 
				new Object[]{caged, colaboradorcargo.getCdcolaboradorcargo()});
	}

	public void updateSindicato(Colaboradorcargo colaboradorcargo, Sindicato sindicato) {
		getHibernateTemplate().bulkUpdate("update Colaboradorcargo c set c.sindicato = ? where c.id = ?", 
				new Object[]{sindicato, colaboradorcargo.getCdcolaboradorcargo()});
	}

	public void updateDtinicio(Colaboradorcargo colaboradorcargo, Date dtinicio) {
		getHibernateTemplate().bulkUpdate("update Colaboradorcargo c set c.dtinicio = ? where c.id = ?", 
				new Object[]{dtinicio, colaboradorcargo.getCdcolaboradorcargo()});
	}

	public boolean haveColaboradorcargo(Cargo cargo, Date dtinicio, Colaborador colaborador) {
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.from(Colaboradorcargo.class)
				.join("colaboradorcargo.cargo cargo")
				.join("colaboradorcargo.colaborador colaborador")
				.where("cargo = ?", cargo)
				.where("colaborador = ?", colaborador)
				.where("colaboradorcargo.dtinicio = ?", dtinicio)
				.setUseTranslator(Boolean.FALSE)
				.unique() > 0;
	}
	
	/**
	 * 
	 * M�todo para buscar uma lista de cargos de colaborador a partir de um colaborador.
	 *
	 *@author Thiago Augusto
	 *@date 12/01/2012
	 * @param colaborador
	 * @return
	 */
	public List<Colaboradorcargo> findListColaboradorCargoForColaborador(Colaborador colaborador){
		return query()
			.leftOuterJoin("colaboradorcargo.colaborador colaborador")
			.where("colaborador = ? ", colaborador)
			.list();
	}
	
	/**
	 * 
	 * M�todo para buscar uma lista de cargos de colaborador a partir de uma Lista de cdcolaborador.
	 *
	 * @author Filipe Santos
	 * @date 24/01/2012
	 * @param List<colaborador>
	 * @return
	 */
	public List<Colaboradorcargo> findByCdpessoa(String whereIn){
		return query()
			.leftOuterJoinFetch("colaboradorcargo.colaborador colaborador")		
			.leftOuterJoinFetch("colaboradorcargo.cargo cargo")
			.whereIn("colaborador.cdpessoa", whereIn)
			.where("colaboradorcargo.dtfim is null")
			.where("colaboradorcargo.salario > 0")
			.list();
	}
	
	/**
	 * Atualiza as informa��es do cargo do colaborador
	 *
	 * @param colaboradorcargo
	 * @param salario
	 * @param sindicato
	 * @param empresa
	 * @since 25/04/2012
	 * @author Rodrigo Freitas
	 */
	public void updateSindicatoEmpresaSalario(Colaboradorcargo colaboradorcargo, Money salario, Sindicato sindicato, Empresa empresa) {
		
		if(colaboradorcargo != null && (salario != null | sindicato != null || empresa != null)){
			List<Object> listaParam = new ArrayList<Object>();
			
			StringBuilder sb = new StringBuilder();
			sb.append("update Colaboradorcargo c set ");
			
			if(salario != null){
				sb.append(" c.salario = ? ");
				listaParam.add(salario);
			} else sb.append(" c.salario = null ");
			
			sb.append(" , ");
			
			if(sindicato != null){
				sb.append(" c.sindicato = ? ");
				listaParam.add(sindicato);
			} else sb.append(" c.sindicato = null ");
			
			sb.append(" , ");
			
			if(empresa != null){
				sb.append(" c.empresa = ? ");
				listaParam.add(empresa);
			} else sb.append(" c.empresa = null ");
			
			sb.append(" where c.id = ? ");
			listaParam.add(colaboradorcargo.getCdcolaboradorcargo());
			
			getHibernateTemplate().bulkUpdate(sb.toString(), listaParam.toArray());
		}
		
	}
	
	public List<Colaboradorcargo> findForGerarArquivoSEFIP(GerarArquivoSEFIPFiltro filtro){
		return query()
				.select("colaborador.cdpessoa, colaborador.nome, colaborador.rg, colaborador.numeroinscricao, cargo.cdcargo, cargo.nome," +
						"cbo.cdcbo")
				.join("colaboradorcargo.colaborador colaborador")
				.join("colaboradorcargo.cargo cargo")
				.leftOuterJoin("cargo.cbo cbo")
				.leftOuterJoin("colaboradorcargo.departamento departamento")
				.where("coalesce(colaborador.numeroinscricao,'')<>''")
				.where("cargo=?", filtro.getCargo())
				.where("departamento=?", filtro.getDepartamento())
				.where("exists (select 1" +
					   "		from Documentocomissao dc" +
					   "		join dc.pessoa p" +
					   "		where p.cdpessoa=colaborador.cdpessoa)")	
				.list();
	}
	
	/**
	 * M�todo que carrega o cargo atual com o respons�vel do departamento
	 *
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 */
	public Colaboradorcargo loadCargoatualWithResponsavelDepartamento(Colaborador colaborador){
		if(colaborador == null || colaborador.getCdpessoa() == null)
			throw new SinedException("O colaborador n�o pode ser nulo.");
		
		StringBuilder campos = new StringBuilder();
		campos.append("colaboradorcargo.cdcolaboradorcargo, colaborador.cdpessoa, colaborador.nome, ");
		campos.append("departamento.cddepartamento, responsavel.cdpessoa, responsavel.nome, responsavel.email ");
		
		return query()
			.select(campos.toString())
			.join("colaboradorcargo.departamento departamento")
			.join("colaboradorcargo.colaborador colaborador")
			.join("departamento.responsavel responsavel")
			.where("colaboradorcargo.dtfim is null")
			.where("colaborador.cdpessoa = ?",colaborador.getCdpessoa())
			.setMaxResults(1)
			.unique();
	}
	
	/**
	 * M�todo que busca o cargo atual para calculo do valor das atividades
	 *
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 */
	public Colaboradorcargo findCargoAtualForTotalatividade(Colaborador colaborador){
		if(colaborador == null || colaborador.getCdpessoa()==null){
			throw new SinedException("O par�metro colaborador e cdpessoa n�o podem ser null.");
		}
		
		StringBuilder campos = new StringBuilder();
		campos.append("colaboradorcargo.cdcolaboradorcargo, cargo.custohora, ");
		campos.append("cargo.cdcargo, cargo.nome, colaboradorcargo.dtinicio, colaboradorcargo.dtfim, colaboradorcargo.salario,");
		campos.append("colaborador.cdpessoa, colaborador.nome, empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.cdpessoa, ");
		campos.append("listaCargoatividade.cdcargoatividade, atividadetipo.cdatividadetipo, listaCargoatividade.valorhoradiferenciada ");
		
		return query()
			.select(campos.toString())
			.leftOuterJoin("colaboradorcargo.empresa empresa")
			.join("colaboradorcargo.cargo cargo")
			.join("colaboradorcargo.colaborador colaborador")
			.leftOuterJoin("cargo.listaCargoatividade listaCargoatividade")
			.leftOuterJoin("listaCargoatividade.atividadetipo atividadetipo")
			.where("colaboradorcargo.dtfim is null")
			.where("colaborador.cdpessoa = ?",colaborador.getCdpessoa())
			.unique();
	}

	@Override
	public ListagemResult<Colaboradorcargo> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Colaboradorcargo> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("colaboradorcargo.cdcolaboradorcargo");
		
		return new ListagemResult<Colaboradorcargo>(query, filtro);
	}
}