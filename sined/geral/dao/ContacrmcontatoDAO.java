package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContacrmcontatoDAO extends GenericDAO<Contacrmcontato>{
	
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				
				save.saveOrUpdateManaged("listcontacrmcontatoemail");
				save.saveOrUpdateManaged("listcontacrmcontatofone");
				return null;
			}
		});
	}
	
	public List<Contacrmcontato> findByContacrm(Contacrm contacrm){
		
		if(contacrm == null || contacrm.getCdcontacrm() == null){
			throw new SinedException("O par�metro contacrm e cdcontacrm n�o podem ser null.");
		}
		return 
			query()
					.select("contacrmcontato.cdcontacrmcontato, contacrmcontato.nome , contacrm.cdcontacrm, contacrm.nome, " +
							"sexo.cdsexo, sexo.nome, listcontacrmcontatofone.cdcontacrmcontatofone, listcontacrmcontatofone.telefone, " +
							"telefonetipo.cdtelefonetipo, listcontacrmcontatoemail.cdcontacrmcontatoemail, listcontacrmcontatoemail.email, " +
							"contacrmcontato.logradouro, contacrmcontato.numero, contacrmcontato.bairro, contacrmcontato.cep, contacrmcontato.complemento, " +
							"lead.cdlead, lead.nome, municipio.cdmunicipio, uf.cduf")
					.leftOuterJoin("contacrmcontato.contacrm contacrm")
					.leftOuterJoin("contacrmcontato.sexo sexo")
					.leftOuterJoin("contacrmcontato.listcontacrmcontatofone listcontacrmcontatofone")
					.leftOuterJoin("listcontacrmcontatofone.telefonetipo telefonetipo")
					.leftOuterJoin("contacrmcontato.listcontacrmcontatoemail listcontacrmcontatoemail")
					.leftOuterJoin("contacrmcontato.lead lead")
					.leftOuterJoin("contacrmcontato.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.where("contacrm = ?",contacrm)
					.orderBy("contacrmcontato.nome")
					.list();
		
	}
	
	public List<Contacrmcontato> findByContacrmForCombo(Contacrm contacrm){
		if(contacrm == null || contacrm.getCdcontacrm() == null){
			throw new SinedException("O par�metro contacrm e cdcontacrm n�o podem ser null.");
		}
		return query()
					.select("contacrmcontato.cdcontacrmcontato, contacrmcontato.nome")
					.where("contacrmcontato.contacrm = ?",contacrm)
					.orderBy("contacrmcontato.nome")
					.list();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Contacrmcontato> query) {
			query.leftOuterJoinFetch("contacrmcontato.sexo sexo");
		super.updateEntradaQuery(query);
	}

	/**
	 * M�todo que busca os emails e telefones do contato
	 *
	 * @param contacrmcontato
	 * @return
	 * @author Luiz Fernando
	 */
	public Contacrmcontato buscaEmailsTelefones(Contacrmcontato contacrmcontato) {
		if(contacrmcontato == null || contacrmcontato.getCdcontacrmcontato() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("contacrmcontato.nome, contacrmcontato.cdcontacrmcontato,listcontacrmcontatoemail.email,listcontacrmcontatofone.telefone,telefonetipo.nome")
				.leftOuterJoin("contacrmcontato.listcontacrmcontatoemail listcontacrmcontatoemail")
				.leftOuterJoin("contacrmcontato.listcontacrmcontatofone listcontacrmcontatofone")
				.leftOuterJoin("listcontacrmcontatofone.telefonetipo telefonetipo")
				.where("contacrmcontato = ?", contacrmcontato)
				.unique();
	}

	/**
	 * Buscar os contatos das contas
	 *
	 * @param whereIn
	 * @return
	 * @since 05/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Contacrmcontato> findByContacrm(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
					.select("contacrmcontato.cdcontacrmcontato, contacrmcontato.nome")
					.join("contacrmcontato.contacrm contacrm")
					.whereIn("contacrm.cdcontacrm", whereIn)
					.list();
	}

	
	/**
	* M�todo que busca as contas crm com o endere�o
	*
	* @param whereIn
	* @return
	* @since 02/07/2015
	* @author Luiz Fernando
	*/
	public List<Contacrmcontato> findContatoEnderecoByContacrm(String whereIn){
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("O par�metro whereIn n�o pode ser null.");
		}
		return 
			query()
				.select("contacrmcontato.cdcontacrmcontato, contacrmcontato.logradouro, contacrmcontato.numero, " +
						"contacrmcontato.bairro, contacrmcontato.cep, contacrmcontato.complemento, " +
						"municipio.cdmunicipio, municipio.nome, " +
						"uf.cduf, uf.nome, uf.sigla")
				.leftOuterJoin("contacrmcontato.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.whereIn("contacrmcontato.contacrm.cdcontacrm ", whereIn)
				.orderBy("contacrmcontato.nome")
				.list();
	}
}