package br.com.linkcom.sined.geral.dao;

import java.util.HashSet;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.geral.bean.PropostaArquivo;
import br.com.linkcom.sined.geral.bean.Propostacaixa;
import br.com.linkcom.sined.geral.bean.Propostarevisao;
import br.com.linkcom.sined.geral.bean.Propostasituacao;
import br.com.linkcom.sined.geral.bean.Prospeccao;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.PropostaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PropostaDAO extends GenericDAO<Proposta>{
 		
	private ArquivoDAO arquivoDAO;
	private EmpresaService empresaService;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Proposta> query) {

		query
			.leftOuterJoinFetch("proposta.municipio municipio")
			.leftOuterJoinFetch("municipio.uf uf")
			.leftOuterJoinFetch("proposta.interacao interacao")
			.leftOuterJoin("proposta.cliente cliente")
			.leftOuterJoinFetch("proposta.listaPropostarevisao listaPropostarevisao")
			.leftOuterJoin("proposta.empresa empresa")
			.leftOuterJoinFetch("listaPropostarevisao.arquivoPCM arquivoPCM")
			.leftOuterJoinFetch("listaPropostarevisao.arquivoPTM arquivoPTM")
			.orderBy("listaPropostarevisao.cdpropostarevisao");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Proposta> query, FiltroListagem _filtro) {
		PropostaFiltro filtro = (PropostaFiltro) _filtro;
		
		query
			.select("distinct cliente.nome,projeto.nome ,municipio.nome, uf.sigla, proposta.numero, proposta.sufixo, proposta.cdproposta, " +
					"proposta.descricao, propostasituacao.cdpropostasituacao, propostasituacao.nome, proposta.numeroconvite, " +
					"aux_proposta.dtentregapcm, aux_proposta.dtvalidade, " +
					"aux_proposta.revisaoptm, aux_proposta.revisaopcm, " +
					"aux_proposta.dtentregaptm, aux_proposta.valor")
			.join("proposta.cliente cliente")
			.leftOuterJoin("proposta.projeto projeto")
			.leftOuterJoin("proposta.propostacaixa propostacaixa")
			.leftOuterJoin("proposta.listaItem item")
			.leftOuterJoin("item.propostacaixaitem propostacaixaitem")
			.join("proposta.propostasituacao propostasituacao")
			.leftOuterJoin("proposta.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("proposta.aux_proposta aux_proposta")
			.where("proposta.numero = ?",filtro.getNumero())
			.where("proposta.sufixo = ?",filtro.getSufixo())
			.where("cliente = ?",filtro.getCliente())
			.where("projeto = ?", filtro.getProjeto())
			.where("aux_proposta.valor >= ?",filtro.getValorde())
			.where("aux_proposta.valor <= ?",filtro.getValorate())
			.openParentheses()
			.where("aux_proposta.dtentregapcm >= ?",filtro.getDtentregade())
			.where("aux_proposta.dtentregapcm <= ?",filtro.getDtentregaate())
			.or()
			.where("aux_proposta.dtentregaptm >= ?",filtro.getDtentregade())
			.where("aux_proposta.dtentregaptm <= ?",filtro.getDtentregaate())
			.closeParentheses()
			.where("aux_proposta.dtvalidade >= ?",filtro.getDtvalidadede())
			.where("aux_proposta.dtvalidade <= ?",filtro.getDtvalidadeate())
			.where("municipio = ?",filtro.getMunicipio())
			.where("uf = ?",filtro.getUf())
			.where("propostacaixa = ?", filtro.getPropostaCaixa())
			.where("propostacaixaitem = ?", filtro.getPropostaCaixaItem())
			.whereLikeIgnoreAll("item.valor", filtro.getNomeItem())
			.orderBy("proposta.sufixo DESC, proposta.numero DESC");
		
		if (filtro != null && filtro.getListaSituacao() != null && filtro.getListaSituacao().size() > 0) {
			query
			.whereIn("propostasituacao.cdpropostasituacao", CollectionsUtil.listAndConcatenate(filtro.getListaSituacao(), "cdpropostasituacao", ","));
		}
		
		query.ignoreJoin("item","propostacaixaitem");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		Proposta proposta = (Proposta) save.getEntity();
		if (proposta.getListaPropostarevisao() != null) {
			for (Propostarevisao propostarevisao : proposta
					.getListaPropostarevisao()) {
				if (propostarevisao.getArquivoPCM() != null) {
					arquivoDAO.saveFile(propostarevisao, "arquivoPCM");
				}
				if (propostarevisao.getArquivoPTM() != null) {
					arquivoDAO.saveFile(propostarevisao, "arquivoPTM");
				}
			}
		}
		save.saveOrUpdateManaged("listaPropostarevisao");
		save.saveOrUpdateManaged("listaItem");
		for (PropostaArquivo pa : proposta.getListaArquivos()) {
			if (pa.getArquivo() != null){
				arquivoDAO.saveFile(pa, "arquivo");
			}
		}
		save.saveOrUpdateManaged("listaArquivos");
	}
	
	/**
	 * Carrega a lista de propostas com as informa��es necess�rias para a cria��o do relat�rio.
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Proposta> findForReport(PropostaFiltro filtro) {
		if (filtro == null) {
			throw new SinedException("Filtro n�o pode ser nulo.");
		}
		
		QueryBuilder<Proposta> query = querySined();
		updateListagemQuery(query, filtro);
		query
			.select("proposta.cdproposta, proposta.numero, proposta.cliente, proposta.sufixo, cliente.nome, municipio.nome, uf.sigla, proposta.descricao," +
					"proposta.propostasituacao, aux_proposta.materiais, aux_proposta.mdo, aux_proposta.mobilizacao, contato.cdpessoa," +
					"aux_proposta.valor, aux_proposta.dtentregaptm, aux_proposta.dtentregapcm, " +
					"aux_proposta.revisaoptm, aux_proposta.revisaopcm, proposta.dtcarta, proposta.dtvisita, proposta.numeroconvite")
			.leftOuterJoinIfNotExists("proposta.contato contato")
			.orderBy("proposta.sufixo DESC, proposta.numero");
		
		query.setIgnoreJoinPaths(new HashSet<String>());
		
		return query.list();
	}
	
	/**
	 * Carrega lista de propostas de uma prospec��o.
	 * 
	 * @param prospeccao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Proposta> findByProspeccao(Prospeccao prospeccao){
		if (prospeccao == null || prospeccao.getCdprospeccao() == null) {
			throw new SinedException("Prospec��o n�o pode ser nulo.");
		}
		return query()
					.select("proposta.numero, proposta.sufixo")
					.join("proposta.prospeccao prospeccao")
					.where("prospeccao = ?",prospeccao)
					.list();
		
	}

	/**
	 * Carrega a lista de propostas de uma propostacaixa.
	 * 
	 * @param propostacaixa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Proposta> findForVerificacao(Proposta proposta) {
		if (proposta == null || proposta.getPropostacaixa() == null || proposta.getPropostacaixa().getCdpropostacaixa() == null) {
			throw new SinedException("Propostacaixa n�o pode ser nulo.");
		}
		return query()
					.select("proposta.cdproposta, proposta.sufixo")
					.leftOuterJoin("proposta.propostacaixa propostacaixa")
					.where("propostacaixa = ?",proposta.getPropostacaixa())
					.where("proposta.cdproposta <> ?",proposta.getCdproposta())
					.list();
	}

	/**
	 * Atualiza a intera��o da proposta no banco.
	 * 
	 * @param proposta
	 * @author Rodrigo Freitas
	 */
	public void updateInteracao(Proposta proposta) {
		if (proposta == null || proposta.getCdproposta() == null) {
			throw new SinedException("Prospecc��o n�o pode ser nula.");
		}
		getHibernateTemplate().bulkUpdate("update Proposta p set interacao = ? where p.id = ?", 
				new Object[]{proposta.getInteracao(),proposta.getCdproposta()});
	}
	
	/**
	 * Carrega a proposta com o cdinteracao preenchido. 
	 * 
	 * @param proposta
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Proposta loadWithInteracao(Proposta proposta){
		if (proposta == null || proposta.getCdproposta() == null) {
			throw new SinedException("Proposta n�o pode ser nula.");
		}
		return query()
					.select("proposta.cdproposta, interacao.cdinteracao")
					.leftOuterJoin("proposta.interacao interacao")
					.where("proposta = ?",proposta)
					.unique();
	}

	public Proposta bucarProposta(Integer cdpropostacaixa) {
		return query()
			   .select("caixa.nome, cliente.nome, proposta.descricao, proposta.numero, proposta.sufixo")
			   .leftOuterJoin("proposta.propostacaixa caixa")
			   .leftOuterJoin("proposta.cliente cliente")
			   .where("caixa.cdpropostacaixa = ?", cdpropostacaixa)
			   .unique();
	}
	
	
	/**
	 * Carrega a lista de Proposta com as informa��es do "Cliente" da "Proposta Caixa" e "Proposta".
	 * 
	 * @param whereIn
	 * @return
	 * @author Simon Gontijo
	 */
	public List<Proposta> findListaProposta(String whereIn) {
		if (whereIn == null)  throw new SinedException("O par�metro n�o pode ser nulo.");
		else if (whereIn.equals("")) throw new SinedException("Nenhum item selecionado.");
		return query()
			   .select("caixa.nome, cliente.nome, proposta.descricao, proposta.numero, proposta.sufixo")
			   .leftOuterJoin("proposta.propostacaixa caixa")
			   .leftOuterJoin("proposta.cliente cliente")
			   .whereIn("caixa.cdpropostacaixa", whereIn)
			   .orderBy("caixa.nome, proposta.numero, cliente.nome")
			   .list();
	}
	
	/**
	 * Carrega o n�mero da �ltima proposta cadastrada.
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 * @param sufixo 
	 */
	public Integer findUltimoNumero(String sufixo){
		return newQueryBuilder(Integer.class)
				.select("max(proposta.numero)")
				.from(Proposta.class)
				.setUseTranslator(false)
				.where("proposta.sufixo = ?", sufixo)
				.unique();
	}

	/**
	 * Carrega a proposta com as informa��es da View <code>aux_proposta</code>
	 * 
	 * @param proposta
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Proposta loadWithView(Proposta proposta) {
		if (proposta == null || proposta.getCdproposta() == null) {
			throw new SinedException("Proposta n�o pode ser nula.");
		}
		return query()
					.select("proposta.cdproposta, aux_proposta.valor, aux_proposta.mobilizacao, aux_proposta.mdo, aux_proposta.materiais")
					.join("proposta.aux_proposta aux_proposta")
					.where("proposta = ?",proposta)
					.unique();
	}

	/**
	 * Busca as propostas que est�o naquela caixa.
	 *
	 * @param propostacaixa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Proposta> findByPropostacaixa(Propostacaixa propostacaixa) {
		if (propostacaixa == null || propostacaixa.getCdpropostacaixa() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return 
			query()
				.select("proposta.cdproposta, proposta.descricao, proposta.numero, proposta.sufixo, cliente.nome")
				.join("proposta.cliente cliente")
				.where("proposta.propostacaixa = ?",propostacaixa)
				.orderBy("proposta.sufixo, proposta.numero")
				.list();
	}
	

	/**
	 * Chama a procedure de atualiza��o da tabela auxiliar de proposta
	 *
	 * @param proposta
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaProposta(Proposta proposta){
		if (proposta == null || proposta.getCdproposta() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		getJdbcTemplate().execute("SELECT ATUALIZA_PROPOSTA(" + proposta.getCdproposta() + ")");
	}

	/**
	 * Carrega informa��es para a gera��o do contrato a partir de uma proposta.
	 *
	 * @param proposta
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Proposta loadForContrato(Proposta proposta) {
		return query()
					.select("proposta.cdproposta, cliente.cdpessoa, cliente.nome, proposta.descricao, aux_proposta.valor, " +
							"propostasituacao.cdpropostasituacao ")
					.join("proposta.aux_proposta aux_proposta")
					.join("proposta.propostasituacao propostasituacao")
					.join("proposta.cliente cliente")
					.where("proposta = ?", proposta)
					.unique();
	}
	
	/**
	 * Somat�rio do valor total de proposta
	 * @param proposta
	 * @return
	 * @author Taidson
	 * @since 10/06/2010
	 */
	public Long somaValorTotal(Proposta proposta){
		String sql = "select sum(ap.valor) from proposta p " +
				"join aux_proposta ap on (p.cdproposta = ap.cdproposta) " +
				"where p.cdproposta = " + proposta.getCdproposta();
		return this.queryForLong(sql);
	}
	
	private Long queryForLong(String sql){
		SinedUtil.markAsReader();
		Long result = getJdbcTemplate().queryForLong(sql);
		return new Long(result);
	}

	public boolean havePropostaCliente(Cliente cliente) {
		return newQueryBuilderSined(Long.class)
						.select("count(*)")
						.from(Proposta.class)
						.setUseTranslator(false)
						.where("proposta.cliente = ?", cliente)
						.unique() > 0;
	}
	
	/**
	 * Busca propostas do cliente informado como par�metro.
	 * Nota.: S� s�o retornadas propostas que n�o est�o associadas a empresa ou que est�o associadas e o usu�rio possui acesso � empresa
	 * @param cliente
	 * @param empresa
	 * @return List<Proposta>
	 * @author Taidson
	 * @since 16/09/2010
	 */
	public List<Proposta> findByCliente(Cliente cliente, Empresa empresa) {
		if (cliente == null || cliente.getCdpessoa() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		String whereInEmpresas = empresa != null? empresa.getCdpessoa().toString():
												CollectionsUtil.listAndConcatenate(empresaService.findByUsuario(), "cdpessoa", ",");
		return 
			query()
				.select("proposta.cdproposta, proposta.descricao, proposta.numero, proposta.sufixo, " +
						"cliente.cdpessoa, cliente.nome, propostasituacao.cdpropostasituacao, propostasituacao.nome")
				.join("proposta.cliente cliente")
				.join("proposta.propostasituacao propostasituacao")
				.leftOuterJoin("proposta.empresa empresa")
				.where("cliente = ?", cliente)
				.openParentheses()
					.whereIn("empresa.cdpessoa", whereInEmpresas)
					.or()
					.where("empresa.cdpessoa is null")
				.closeParentheses()
				.orderBy("proposta.sufixo, proposta.numero")
				.list();

	}

	/**
	* M�todo utilizado para o combo proposta, exibe o n�mero e o ano da proposta
	*
	* @return
	* @since Aug 25, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Proposta> findNumeroAnoProposta() {
		return query()
				.select("proposta.cdproposta, proposta.numero, proposta.sufixo")
				.orderBy("proposta.sufixo DESC, proposta.numero")
				.list();
	}

	/**
	 * Busca a lista de proposta para o combo em flex.
	 * 
	 * @return
	 * @since 05/10/2011
	 * @author Rodrigo Freitas
	 */
	public List<Proposta> findForComboFlex() {
		return query()
				.select("proposta.cdproposta, proposta.numero, proposta.sufixo, " +
						"propostasituacao.cdpropostasituacao, propostasituacao.nome")
				.join("proposta.propostasituacao propostasituacao")
				.orderBy("proposta.sufixo DESC, proposta.numero")
				.list();
	}
	
	/**
	 * 
	 * M�todo para carregar Proposta para emitir RTF
	 *
	 *@author Thiago Augusto
	 *@date 06/02/2012
	 * @param proposta
	 * @return
	 */
	public Proposta carregarProposta(Proposta proposta){
		return query()
			.select("proposta.cdproposta, proposta.descricao, cliente.cdpessoa, cliente.cpf, cliente.cnpj, cliente.nome, " +
					"listaEndereco.cdendereco, listaEndereco.logradouro, listaEndereco.numero, listaEndereco.complemento, " +
					"listaEndereco.bairro, municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla, proposta.observacao, " +
					"listaPropostarevisao.formapagamento, listaPropostarevisao.mobilizacao, listaPropostarevisao.mdo, listaPropostarevisao.materiais, " +
					"listaPropostarevisao.prazo, contato.emailcontato, listaTelefone.telefone, listaTelefone.telefone, " +
					"propostacaixa.cdpropostacaixa, arquivo.cdarquivo, arquivo.nome, arquivo.tamanho, arquivo.tipoconteudo, contato.cdpessoa, contato.nome, " +
					"empresa.cdpessoa, logomarca.cdarquivo, empresa.nome, empresa.razaosocial, empresa.nomefantasia, listaPropostarevisao.dtvalidade, listaPropostarevisao.cdpropostarevisao," +
					"projeto.cdprojeto, projeto.nome ")
			.leftOuterJoin("proposta.empresa empresa")
			.leftOuterJoin("empresa.logomarca logomarca")
			.leftOuterJoin("proposta.cliente cliente")
			.leftOuterJoin("proposta.contato contato")
			.leftOuterJoin("contato.listaTelefone listaTelefone")
			.leftOuterJoin("proposta.propostacaixa propostacaixa")
			.leftOuterJoin("propostacaixa.arquivo arquivo")
			.leftOuterJoin("cliente.listaEndereco listaEndereco")
			.leftOuterJoin("listaEndereco.municipio municipio")
			.leftOuterJoin("municipio.uf uf")
			.leftOuterJoin("proposta.listaPropostarevisao listaPropostarevisao")
			.leftOuterJoin("proposta.projeto projeto")
			.where("proposta = ?", proposta)
			.orderBy("listaPropostarevisao.cdpropostarevisao ")
			.unique();
	}
	
	@Override
	public Proposta load(Proposta bean) {
		return query()
			.select("proposta.cdproposta, propostacaixa.cdpropostacaixa, arquivo.cdarquivo, arquivo.nome")
			.leftOuterJoin("proposta.propostacaixa propostacaixa")
			.leftOuterJoin("propostacaixa.arquivo arquivo")
			.where("proposta = ?", bean)
			.unique();
	}

	/**
	 * M�todo que atualiza a situa��o da proposta
	 *
	 * @param proposta
	 * @param propostasituacao
	 * @author Luiz Fernando
	 */
	public void atualizaSituacao(Proposta proposta,	Propostasituacao propostasituacao) {
		if(proposta == null || proposta.getCdproposta() == null || propostasituacao == null ){
			throw new SinedException("Par�metros inv�lidos");
		}		
		
		getHibernateTemplate().bulkUpdate("update Proposta p set p.propostasituacao = ? where p.cdproposta = ?",
										new Object[]{propostasituacao, proposta.getCdproposta()});
	}

	/**
	 * M�todo que carrega a proposta para gerar nota fiscal
	 *
	 * @param proposta
	 * @return
	 * @author Luiz Fernando
	 */
	public Proposta loadForNotafiscalservico(Proposta proposta) {
		return query()
			.select("proposta.cdproposta, cliente.cdpessoa, cliente.nome, proposta.descricao, aux_proposta.valor, " +
					"propostasituacao.cdpropostasituacao ")
			.join("proposta.aux_proposta aux_proposta")
			.join("proposta.propostasituacao propostasituacao")
			.join("proposta.cliente cliente")
			.where("proposta = ?", proposta)
			.unique();
	}
	
	
	public Boolean verificaPropostaPorContato(Contato contato){
		return newQueryBuilder(Long.class)
				.select("count(*)")
				.from(Proposta.class)
				.where("proposta.contato = ?", contato)
				.unique() > 0;
		
	}
}

