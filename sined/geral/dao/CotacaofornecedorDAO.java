package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedor;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CotacaofornecedorDAO extends GenericDAO<Cotacaofornecedor> {
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Cotacaofornecedor> query) {
		query
		.leftOuterJoinFetch("cotacaofornecedor.listaCotacaofornecedoritem listaCotacaofornecedoritem")
		.leftOuterJoinFetch("listaCotacaofornecedoritem.material material")
		.leftOuterJoinFetch("listaCotacaofornecedoritem.localarmazenagem localarmazenagem")
		.leftOuterJoinFetch("localarmazenagem.endereco endereco")
		.leftOuterJoinFetch("endereco.municipio municipio")
		.leftOuterJoinFetch("municipio.uf uf")
		.orderBy("material.nome, localarmazenagem.nome");		
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Cotacaofornecedor> query, FiltroListagem _filtro) {
		Fornecedor fornecedor = null;
		User user = Neo.getUser();
		if (user instanceof Fornecedor) {
			fornecedor = (Fornecedor)user;
		}
		query
			.select("cotacaofornecedor.cdcotacaofornecedor")
			.join("cotacaofornecedor.fornecedor fornecedor")
			.join("cotacaofornecedor.cotacao cotacao")
			.join("cotacao.aux_cotacao aux_cotacao")
			.where("fornecedor = ?",fornecedor)
			.where("aux_cotacao.situacaosuprimentos = ?",Situacaosuprimentos.EM_COTACAO);
	}
	
	/**
	 * Carrega a listagem com as listas necess�rias preenchidas.
	 *
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cotacaofornecedor> loadWithLista(String whereIn, String orderBy, boolean asc){
		QueryBuilder<Cotacaofornecedor> query = query()
				.select("cotacaofornecedor.cdcotacaofornecedor, cotacaofornecedor.dtvalidade, fornecedor.nome, material.nome, material.identificacao, localarmazenagem.cdlocalarmazenagem, " +
						"localarmazenagem.nome, prazopagamento.nome, cotacao.cdcotacao")
				.join("cotacaofornecedor.fornecedor fornecedor")
				.leftOuterJoin("cotacaofornecedor.prazopagamento prazopagamento")
				.join("cotacaofornecedor.listaCotacaofornecedoritem listaCotacaofornecedoritem")
				.join("listaCotacaofornecedoritem.material material")
				.join("listaCotacaofornecedoritem.localarmazenagem localarmazenagem")
				.join("cotacaofornecedor.cotacao cotacao")
				.join("cotacao.aux_cotacao aux_cotacao")
				.whereIn("cotacaofornecedor.cdcotacaofornecedor", whereIn);
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("cotacaofornecedor.cdcotacaofornecedor DESC");
		}
		
		return query.list();
	}
	
	/**
	 * Deleta os registro de cotacaofornecedor que n�o est�o no whereIn.
	 *
	 * @param bean
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void deleteNaoExistentes(Cotacao bean, String whereIn) {
		if (bean == null || bean.getCdcotacao() == null) {
			throw new SinedException("COta��o n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("delete Cotacaofornecedor c where c.id not in ("+whereIn+") and c.cotacao.id = ?", new Object[]{bean.getCdcotacao()});
	}

	/**
	 * M�todo busca cota��es escolhidas na tela de listagem para gerar relat�rio emiss�o cota��o.
	 * 
	 * Est� buscando informa��es em tr�s partes pois estava duplicando os itens.
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Cotacaofornecedor> findFornCotacoes(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		List<Cotacaofornecedor> listaFinal = querySined()
											.joinFetch("cotacaofornecedor.cotacao cotacao")
											.joinFetch("cotacaofornecedor.listaCotacaofornecedoritem listaCotacaofornecedoritem")
											.joinFetch("listaCotacaofornecedoritem.localarmazenagem")
											.joinFetch("listaCotacaofornecedoritem.material material")
											.joinFetch("listaCotacaofornecedoritem.unidademedida unidademedida")
//											.leftOuterJoinFetch("material.listaCaracteristica")
											.whereIn("cotacao.cdcotacao", whereIn)
											.list();
		
		List<Cotacaofornecedor> listaFornecedor = query()	
											.joinFetch("cotacaofornecedor.cotacao cotacao")
											.joinFetch("cotacaofornecedor.fornecedor fornecedor")
											.leftOuterJoinFetch("fornecedor.listaEndereco listaEndereco")
											.leftOuterJoinFetch("listaEndereco.municipio municipio")
											.leftOuterJoinFetch("municipio.uf")
											.whereIn("cotacao.cdcotacao", whereIn)
											.list();
		
		for (Cotacaofornecedor cf1 : listaFornecedor) {
			for (Cotacaofornecedor cf : listaFinal) {
				if(cf.getCdcotacaofornecedor().equals(cf1.getCdcotacaofornecedor())){
					cf.setFornecedor(cf1.getFornecedor());
					break;
				}
			}
		}
											
		List<Cotacaofornecedor> listaContato = query()	
											.joinFetch("cotacaofornecedor.cotacao cotacao")									
											.leftOuterJoinFetch("cotacaofornecedor.contato contato")
											.leftOuterJoinFetch("contato.listaTelefone")
											.whereIn("cotacao.cdcotacao", whereIn)
											.list();
		
		for (Cotacaofornecedor cf1 : listaContato) {
			for (Cotacaofornecedor cf : listaFinal) {
				if(cf.getCdcotacaofornecedor().equals(cf1.getCdcotacaofornecedor())){
					cf.setContato(cf1.getContato());
					break;
				}
			}
		}
		
		return listaFinal;
	}

	/**
	 * Carrega a cotacaofornecedor que vem como string por par�metro.
	 *
	 * @param where
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Cotacaofornecedor findFornecedorCotacao(String where) {
		if (where == null || where.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		return query()
			.joinFetch("cotacaofornecedor.cotacao cotacao")
			.joinFetch("cotacaofornecedor.fornecedor fornecedor")
			.joinFetch("cotacaofornecedor.listaCotacaofornecedoritem listaCotacaofornecedoritem")
			.joinFetch("listaCotacaofornecedoritem.localarmazenagem")
			.leftOuterJoinFetch("listaCotacaofornecedoritem.material material")
			.leftOuterJoinFetch("listaCotacaofornecedoritem.unidademedida unidademedidaIt")
			.leftOuterJoinFetch("material.unidademedida unidademedida")
//			.leftOuterJoinFetch("material.listaCaracteristica")
			.leftOuterJoinFetch("cotacaofornecedor.contato contato")
			.leftOuterJoinFetch("contato.listaTelefone")
//			.leftOuterJoinFetch("fornecedor.listaEndereco listaEndereco")
//			.leftOuterJoinFetch("listaEndereco.municipio municipio")
//			.leftOuterJoinFetch("municipio.uf uf")
			.where("cotacaofornecedor.cdcotacaofornecedor = ?", new Integer(where))
			.unique();
	}

	/**
	 * M�todo que atualiza conviteenviado das cotacaofornecedor
	 * 
	 * @param whereIn
	 * @author Tom�s Rabelo
	 */
	public void doUpdateConviteEnviado(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item para update.");
		}
		
		Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
		Timestamp hoje = new Timestamp(System.currentTimeMillis());
		
		getHibernateTemplate().bulkUpdate("update Cotacaofornecedor c set c.conviteenviado = ?, c.cdusuarioaltera = ?, c.dtaltera = ? where c.cdcotacaofornecedor in ("+whereIn+")", new Object[]{Boolean.TRUE,cdusuarioaltera,hoje});
	}
	
	/**
	 * M�todo que atualiza o prazo de pagamento do fornecedor
	 * 
	 * @param cotacaofornecedor
	 * @author Tom�s Rabelo
	 */
	public void updatePrazoPagamento(Cotacaofornecedor cotacaofornecedor) {
		getHibernateTemplate().bulkUpdate("update Cotacaofornecedor c set c.prazopagamento.cdprazopagamento = "+(cotacaofornecedor.getPrazopagamento() != null && cotacaofornecedor.getPrazopagamento().getCdprazopagamento() != -1 ? cotacaofornecedor.getPrazopagamento().getCdprazopagamento() : null)+" where c.cdcotacaofornecedor = "+cotacaofornecedor.getCdcotacaofornecedor());
	}

	/**
	* M�todo para buscar os 3 �ltimos fornecedores utilizados na cota��o
	*
	* @param whereIn
	* @return
	* @since Nov 4, 2011
	* @author Luiz Fernando F Silva
	*/
	@SuppressWarnings("unchecked")
	public List<Fornecedor> find3FornecedoresMaisUsadosByMaterial(String whereIn) {
		if(whereIn == null || "".equals(whereIn)){
			throw new SinedException("Nenhum material passado como par�metro.");
		}
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT cdfornecedor, COUNT(cdfornecedor), p.nome ")
			.append("FROM ( SELECT DISTINCT cf.cdfornecedor as cdfornecedor ")
			.append("FROM cotacaofornecedor cf ")
			.append("join cotacaofornecedoritem cfi on cfi.cdcotacaofornecedor = cf.cdcotacaofornecedor ")
			.append("join fornecedor f on f.cdpessoa = cf.cdfornecedor ");
		
		boolean usuarioComRestricao = SinedUtil.getUsuarioLogado().getRestricaoFornecedorColaborador();
		Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
		if(usuarioComRestricao){
			sql.append("left join colaboradorfornecedor colfor on colfor.cdfornecedor = f.cdpessoa ")
				.append("where (");
			if(colaborador != null && colaborador.getCdpessoa() != null){
				sql.append("colfor.cdcolaborador = " + colaborador.getCdpessoa() + " or ");
			}
				sql.append("colfor.cdcolaboradorfornecedor is null) ");
		}else{
			sql.append("where 1=1 ");
		}
		
		sql.append(new SinedUtil().getWhereFornecedorempresa("f", true, null));
		
		sql.append("and cfi.cdmaterial in (").append(whereIn).append(") and f.ativo = true) as query ")
			.append("join pessoa p on p.cdpessoa = query.cdfornecedor ")
			.append("group by cdfornecedor, p.nome ")
			.append("order by count(cdfornecedor) desc ")
			.append("limit 3 ");
		
		List<Fornecedor> listaFornecedor = new ArrayList<Fornecedor>();

		SinedUtil.markAsReader();
		listaFornecedor = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Fornecedor(rs.getInt("cdfornecedor"), rs.getString("nome"));
			}
		});
		
		return listaFornecedor;
		
	}
	
}
