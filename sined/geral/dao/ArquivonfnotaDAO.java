package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Arquivonf;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NumeroNfeNaoUtilizadoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ArquivonfnotaDAO extends GenericDAO<Arquivonfnota> {

	/**
	 * Busca a lista de Arquivonfnota de um Arquivonf passado por par�metro.
	 *
	 * @param arquivonf
	 * @return
	 * @since 13/02/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonfnota> findByArquivonf(Arquivonf arquivonf) {
		if(arquivonf == null || arquivonf.getCdarquivonf() == null){
			throw new SinedException("O id do arquivo de nota fiscal n�o pode ser nulo.");
		}
		return query()
					.select("arquivonfnota.cdarquivonfnota, nota.cdNota, nota.numero, nota.numeronota, notaStatus.cdNotaStatus, arquivonf.arquivonfsituacao, arquivonf.cdarquivonf ")
					.join("arquivonfnota.arquivonf arquivonf")
					.join("arquivonfnota.nota nota")
					.leftOuterJoin("nota.notaStatus notaStatus")
					.where("arquivonfnota.arquivonf = ?", arquivonf)
					.list();
	}

	/**
	 * Atualiza alguns dados do bean Arquivonfnota com informa��es de Nota Fiscal Eletr�nica
	 *
	 * @param arquivonfnota
	 * @param numeroNfse
	 * @param codigoVerificacao
	 * @param dataEmissao
	 * @param dataCompetencia
	 * @since 23/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateNumeroCodigo(Arquivonfnota arquivonfnota, String numeroNfse, String codigoVerificacao, Timestamp dataEmissao, Date dataCompetencia) {
		if(arquivonfnota == null || arquivonfnota.getCdarquivonfnota() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		if(dataEmissao != null && dataCompetencia != null){
			getHibernateTemplate().bulkUpdate("update Arquivonfnota an set an.numeronfse = ?, an.codigoverificacao = ?, an.dtemissao = ?, an.dtcompetencia = ? where an.id = ?", 
											new Object[]{numeroNfse, codigoVerificacao, dataEmissao, dataCompetencia, arquivonfnota.getCdarquivonfnota()});
		} else if(codigoVerificacao != null){
			getHibernateTemplate().bulkUpdate("update Arquivonfnota an set an.numeronfse = ?, an.codigoverificacao = ? where an.id = ?", 
					new Object[]{numeroNfse, codigoVerificacao, arquivonfnota.getCdarquivonfnota()});
		}else {
			getHibernateTemplate().bulkUpdate("update Arquivonfnota an set an.numeronfse = ? where an.id = ?", 
					new Object[]{numeroNfse, arquivonfnota.getCdarquivonfnota()});
		}
	}
	

	public void updateNumeroParacatu(Arquivonfnota arquivonfnota, String numeroNfse, Timestamp dataEmissao, String urlimpressaoparacatu) {
		if(arquivonfnota == null || arquivonfnota.getCdarquivonfnota() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Arquivonfnota an set an.numeronfse = ?, an.dtemissao = ?, an.urlimpressaoparacatu = ? where an.id = ?", 
				new Object[]{numeroNfse, dataEmissao, urlimpressaoparacatu, arquivonfnota.getCdarquivonfnota()});
	}

	/**
	 * Atualiza o arquivo de XML para o cancelamento de um Arquivonfnota
	 *
	 * @param arquivonfnota
	 * @since 28/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoxmlcancelamento(Arquivonfnota arquivonfnota) {
		if(arquivonfnota == null || arquivonfnota.getCdarquivonfnota() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Arquivonfnota an set an.arquivoxmlcancelamento = ?, an.cancelando = ? where an.id = ?", 
										new Object[]{arquivonfnota.getArquivoxmlcancelamento(), Boolean.FALSE, arquivonfnota.getCdarquivonfnota()});
		
	}
	
	public void updateTipocontigenciacancelamento(Arquivonfnota arquivonfnota) {
		if(arquivonfnota == null || arquivonfnota.getCdarquivonfnota() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		
		if(arquivonfnota.getTipocontigenciacancelamento() != null){
			getHibernateTemplate().bulkUpdate("update Arquivonfnota an set an.tipocontigenciacancelamento = ? where an.id = ?", 
					new Object[]{arquivonfnota.getTipocontigenciacancelamento(), arquivonfnota.getCdarquivonfnota()});
		} else {
			getHibernateTemplate().bulkUpdate("update Arquivonfnota an set an.tipocontigenciacancelamento = null where an.id = ?", 
					new Object[]{arquivonfnota.getCdarquivonfnota()});
		}
	}
	
	/**
	 * Atualiza o arquivo de retorno do cancelamento de um Arquivonfnota
	 *
	 * @param arquivonfnota
	 * @since 29/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoretornocancelamento(Arquivonfnota arquivonfnota) {
		if(arquivonfnota == null || arquivonfnota.getCdarquivonfnota() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Arquivonfnota an set an.arquivoretornocancelamento = ? where an.id = ?", 
										new Object[]{arquivonfnota.getArquivoretornocancelamento(), arquivonfnota.getCdarquivonfnota()});
	}

	public void updateArquivoxmlretornoconsulta(Arquivonfnota arquivonfnota) {
		if(arquivonfnota == null || arquivonfnota.getCdarquivonfnota() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Arquivonfnota an set an.arquivoxmlretornoconsulta = ? where an.id = ?", 
										new Object[]{arquivonfnota.getArquivoxmlretornoconsulta(), arquivonfnota.getCdarquivonfnota()});
	}
	
	/**
	 * Busca registros de Arquivonfnota para o cancelamento de NF-e.
	 *
	 * @return
	 * @since 28/02/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonfnota> findForCancelamento() {
		return query()
					.select("arquivonfnota.cdarquivonfnota, arquivoxmlcancelamento.cdarquivo, configuracaonfe.cdconfiguracaonfe, arquivonf.cdarquivonf")
					.join("arquivonfnota.arquivoxmlcancelamento arquivoxmlcancelamento")
					.join("arquivonfnota.arquivonf arquivonf")
					.join("arquivonf.configuracaonfe configuracaonfe")
					.where("arquivoxmlcancelamento is not null")
					.where("arquivonfnota.dtcancelamento is null")
					.openParentheses()
					.where("arquivonfnota.cancelando = ?", Boolean.FALSE).or()
					.where("arquivonfnota.cancelando is null")
					.closeParentheses()
					.list();
	}
	

	/**
	 * Busca os novos cancelamentos para o emissor desktop.
	 *
	 * @param empresa
	 * @return
	 * @since 14/08/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonfnota> findNovosCancelamentos(Empresa empresa) {
		return querySined()
					
					.select("arquivonfnota.cdarquivonfnota, arquivoxmlcancelamento.cdarquivo, configuracaonfe.cdconfiguracaonfe, arquivonf.cdarquivonf")
					.join("arquivonfnota.arquivoxmlcancelamento arquivoxmlcancelamento")
					.join("arquivonfnota.arquivonf arquivonf")
					.join("arquivonfnota.nota nota")
					.join("nota.notaStatus notaStatus")
					.join("arquivonf.empresa empresa")
					.join("arquivonf.configuracaonfe configuracaonfe")
					.where("arquivoxmlcancelamento is not null")
					.where("arquivonfnota.dtcancelamento is null")
					.where("empresa = ?", empresa)
					.openParentheses()
					.where("notaStatus = ?", NotaStatus.NFE_CANCELANDO)
					.or()
					.where("notaStatus = ?", NotaStatus.NFSE_CANCELANDO)
					.closeParentheses()
					.openParentheses()
					.where("arquivonfnota.cancelando = ?", Boolean.FALSE).or()
					.where("arquivonfnota.cancelando is null")
					.closeParentheses()
					.openParentheses()
					.where("arquivonfnota.cancelamentoporevento is null")
					.or()
					.where("arquivonfnota.cancelamentoporevento = false")
					.closeParentheses()
					.list();
	}
	
	/**
	 * Busca os novos cancelamentos para o emissor desktop.
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Arquivonfnota> findNovosCancelamentosPorEvento(Empresa empresa) {
		return querySined()
					
					.select("arquivonfnota.cdarquivonfnota, arquivoxmlcancelamento.cdarquivo, configuracaonfe.cdconfiguracaonfe, arquivonf.cdarquivonf, " +
							"arquivonfnota.tipocontigenciacancelamento")
					.join("arquivonfnota.arquivoxmlcancelamento arquivoxmlcancelamento")
					.join("arquivonfnota.arquivonf arquivonf")
					.join("arquivonfnota.nota nota")
					.join("nota.notaStatus notaStatus")
					.join("arquivonf.empresa empresa")
					.join("arquivonf.configuracaonfe configuracaonfe")
					.where("arquivoxmlcancelamento is not null")
					.where("arquivonfnota.dtcancelamento is null")
					.where("empresa = ?", empresa)
					.openParentheses()
					.where("notaStatus = ?", NotaStatus.NFE_CANCELANDO)
					.or()
					.where("notaStatus = ?", NotaStatus.NFSE_CANCELANDO)
					.closeParentheses()
					.openParentheses()
					.where("arquivonfnota.cancelando = ?", Boolean.FALSE).or()
					.where("arquivonfnota.cancelando is null")
					.closeParentheses()
					.where("arquivonfnota.cancelamentoporevento = true")
					.list();
	}

	/**
	 * Marca a flag cancelando no Arquivonfnota.
	 *
	 * @param bean
	 * @return
	 * @since 29/02/2012
	 * @author Rodrigo Freitas
	 */
	public int marcarArquivonfnota(String whereIn, String flag) {
		return getHibernateTemplate()
				.bulkUpdate("update Arquivonfnota a set a." + flag + " = ? where a.cdarquivonfnota in (" + whereIn + ") and (a." + flag + " = ? or a." + flag + " is null)", 
							new Object[]{Boolean.TRUE, Boolean.FALSE});
	}
	
	/**
	 * Desmarca a flag cancelando no Arquivonfnota.
	 *
	 * @param bean
	 * @return
	 * @since 29/02/2012
	 * @author Rodrigo Freitas
	 */
	public void desmarcarArquivonfnota(Integer cdarquivonfnota, String flag) {
		getHibernateTemplate().bulkUpdate("update Arquivonfnota a set a." + flag + " = ? where a.cdarquivonfnota in (" + cdarquivonfnota + ")", 
				new Object[]{Boolean.FALSE});
	}

	/**
	 * Carrega informa��es para o cancelamento de NF-e
	 *
	 * @param arquivonfnota
	 * @return
	 * @since 29/02/2012
	 * @author Rodrigo Freitas
	 */
	public Arquivonfnota loadForCancelamento(Arquivonfnota arquivonfnota) {
		if(arquivonfnota == null || arquivonfnota.getCdarquivonfnota() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		return query()
					.select("arquivonfnota.cdarquivonfnota, nota.cdNota, nota.cancelarvenda, notaStatus.cdNotaStatus")
					.join("arquivonfnota.nota nota")
					.join("nota.notaStatus notaStatus")
					.where("arquivonfnota = ?", arquivonfnota)
					.unique();
	}
	
	public Arquivonfnota loadForConsulta(Arquivonfnota arquivonfnota) {
		if(arquivonfnota == null || arquivonfnota.getCdarquivonfnota() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		return query()
					.select("arquivonfnota.cdarquivonfnota, arquivonfnota.chaveacesso, nota.cdNota, nota.numero, arquivoxmlconsulta.cdarquivo, " +
							"configuracaonfe.cdconfiguracaonfe, configuracaonfe.prefixowebservice, configuracaonfe.tipoconfiguracaonfe, " +
							"configuracaonfe.inscricaomunicipal, configuracaonfe.serienfse, " +
							"arquivonf.cdarquivonf, arquivonfnota.numeronfse, notaStatus.cdNotaStatus, empresa.cdpessoa, empresa.cnpj ")
					.join("arquivonfnota.nota nota")
					.join("nota.notaStatus notaStatus")
					.join("nota.empresa empresa")
					.leftOuterJoin("arquivonfnota.arquivoxmlconsulta arquivoxmlconsulta")
					.leftOuterJoin("arquivonfnota.arquivonf arquivonf")
					.leftOuterJoin("arquivonf.configuracaonfe configuracaonfe")
					.where("arquivonfnota = ?", arquivonfnota)
					.unique();
	}

	/**
	 * Atualiza a data de cancelamento da NF-e.
	 *
	 * @param arquivonfnota
	 * @since 29/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateCancelado(Arquivonfnota arquivonfnota) {
		if(arquivonfnota == null || arquivonfnota.getCdarquivonfnota() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Arquivonfnota an set an.dtcancelamento = ? where an.id = ?", 
										new Object[]{new Timestamp(System.currentTimeMillis()), arquivonfnota.getCdarquivonfnota()});
	}

	/**
	 * Busca o resgitro de Arquivonfnota que n�o foi cancelado.
	 *
	 * @param nota
	 * @return
	 * @since 05/03/2012
	 * @author Rodrigo Freitas
	 */
	public Arquivonfnota findByNotaNotCancelada(Nota nota, boolean dtcancelamentoNulo) {
		if(nota == null || nota.getCdNota() == null){
			throw new SinedException("Nota n�o pode ser nulo.");
		}
		return querySined()
					
					.select("arquivonfnota.cdarquivonfnota, arquivonfnota.numeronfse, arquivonfnota.codigoverificacao, empresa.cdpessoa, " +
							"nota.cdNota, nota.numeronota, nota.serienfe, arquivonfnota.numero_filtro, arquivonfnota.dtemissao, arquivonfnota.dtcompetencia, " +
							"arquivonf.cdarquivonf, arquivoxml.cdarquivo, arquivoxmlassinado.cdarquivo, arquivoretornoconsulta.cdarquivo, " +
							"arquivonfnota.protocolonfe, arquivonfnota.chaveacesso, arquivonf.assinando, arquivonf.enviando, arquivonf.consultando, " +
							"configuracaonfe.cdconfiguracaonfe, configuracaonfe.prefixowebservice, arquivoretornoenvio.cdarquivo")
					.join("arquivonfnota.nota nota")
					.join("nota.empresa empresa")
					.join("arquivonfnota.arquivonf arquivonf")
					.join("arquivonf.configuracaonfe configuracaonfe")
					.leftOuterJoin("arquivonf.arquivoxmlassinado arquivoxmlassinado")
					.leftOuterJoin("arquivonf.arquivoretornoconsulta arquivoretornoconsulta")
					.leftOuterJoin("arquivonfnota.arquivoxml arquivoxml")
					.leftOuterJoin("arquivonf.arquivoretornoenvio arquivoretornoenvio")
					.where("arquivonfnota.dtcancelamento " + (dtcancelamentoNulo ? " is null" : " is not null"))
					.where("arquivonfnota.numeronfse is not null")
					.where("nota = ?", nota)
					.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_SUCESSO)
					.unique();
	}
	
	/**
	 * Busca uma lista de Arquivonfnota a partir do whereIn.
	 *
	 * @param whereIn
	 * @return
	 * @since 05/03/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonfnota> findByNotaNotCancelada(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			return new ArrayList<Arquivonfnota>();
		}
		return query()
					.select("arquivonfnota.cdarquivonfnota, arquivonfnota.numeronfse, arquivonfnota.codigoverificacao, arquivonfnota.dtcancelamento, " +
							"arquivonfnota.dtemissao, arquivonfnota.dtcompetencia, arquivonf.cdarquivonf, arquivonf.arquivonfsituacao, nota.cdNota, " +
							"arquivoxml.cdarquivo, arquivoxmlassinado.cdarquivo, arquivoretornoconsulta.cdarquivo, " +
							"arquivonfnota.protocolonfe, arquivonfnota.chaveacesso")
					.join("arquivonfnota.nota nota")
					.join("arquivonfnota.arquivonf arquivonf")
					.leftOuterJoin("arquivonf.arquivoxmlassinado arquivoxmlassinado")
					.leftOuterJoin("arquivonf.arquivoretornoconsulta arquivoretornoconsulta")
					.leftOuterJoin("arquivonfnota.arquivoxml arquivoxml")
					.where("arquivonfnota.dtcancelamento is null")
					.whereIn("nota.cdNota", whereIn)
					.openParentheses()
					.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_ERRO)
					.or()
					.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_SUCESSO)
					.closeParentheses()
					.where("arquivonfnota.chaveacesso is not null")
					.where("arquivonfnota.protocolonfe is not null")
					.where("arquivonfnota.dtprocessamento is not null")
					.list();
	}
	
	/**
	 * Busca uma lista de Arquivonfnota a partir do whereIn.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/07/2014
	 */
	public List<Arquivonfnota> findByNotaAndCancelada(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			return new ArrayList<Arquivonfnota>();
		}
		return query()
					.select("arquivonfnota.cdarquivonfnota, arquivonfnota.numeronfse, arquivonfnota.codigoverificacao, arquivonfnota.dtcancelamento, " +
							"arquivonfnota.dtemissao, arquivonfnota.dtcompetencia, arquivonf.cdarquivonf, arquivonf.arquivonfsituacao, nota.cdNota, nota.numero, nota.serienfe, " +
							"arquivoxml.cdarquivo,arquivoxmlcancelamento.cdarquivo,arquivoretornocancelamento.cdarquivo, arquivoxmlassinado.cdarquivo, arquivoretornoconsulta.cdarquivo, " +
							"arquivonf.arquivonfsituacao, arquivonfnota.protocolonfe, arquivonfnota.chaveacesso, arquivonfnota.dtprocessamento, configuracaonfe.cdconfiguracaonfe, " +
							"configuracaonfe.modelonfe, notaStatus.cdNotaStatus")
					.join("arquivonfnota.nota nota")
					.join("nota.notaStatus notaStatus")
					.join("arquivonfnota.arquivonf arquivonf")
					.leftOuterJoin("arquivonfnota.arquivoxmlcancelamento arquivoxmlcancelamento")
					.leftOuterJoin("arquivonfnota.arquivoretornocancelamento arquivoretornocancelamento")
					.join("arquivonf.configuracaonfe configuracaonfe")
					.leftOuterJoin("arquivonf.arquivoxmlassinado arquivoxmlassinado")
					.leftOuterJoin("arquivonf.arquivoretornoconsulta arquivoretornoconsulta")
					.leftOuterJoin("arquivonfnota.arquivoxml arquivoxml")
					.whereIn("nota.cdNota", whereIn)
					.openParentheses()
					.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_ERRO)
					.or()
					.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_SUCESSO)
					.closeParentheses()
					.where("arquivonfnota.chaveacesso is not null")
					.where("arquivonfnota.protocolonfe is not null")
					.where("arquivonfnota.dtprocessamento is not null")
					.list();
	}

	/**
	 * Busca uma lista de Arquivonfnota a partir do whereIn.
	 *
	 * @param whereIn
	 * @return
	 * @since 05/03/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonfnota> findByNota(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			return new ArrayList<Arquivonfnota>();
		}
		return query()
					.select("arquivonfnota.cdarquivonfnota, arquivonfnota.numeronfse, arquivonfnota.codigoverificacao, arquivonfnota.dtcancelamento, " +
							"arquivonfnota.dtemissao, arquivonfnota.dtcompetencia, arquivonf.cdarquivonf, arquivonf.arquivonfsituacao, nota.cdNota, " +
							"arquivoxml.cdarquivo, arquivoxmlassinado.cdarquivo, arquivoretornoconsulta.cdarquivo, " +
							"arquivonfnota.protocolonfe, arquivonfnota.chaveacesso, arquivonf.emitindo, arquivonf.arquivonfsituacao, " +
							"configuracaonfe.prefixowebservice, arquivonfnota.numero_filtro ")
					.join("arquivonfnota.nota nota")
					.join("arquivonfnota.arquivonf arquivonf")
					.leftOuterJoin("arquivonf.arquivoxmlassinado arquivoxmlassinado")
					.leftOuterJoin("arquivonf.arquivoretornoconsulta arquivoretornoconsulta")
					.leftOuterJoin("arquivonfnota.arquivoxml arquivoxml")
					.leftOuterJoin("arquivonf.configuracaonfe configuracaonfe")
					.whereIn("nota.cdNota", whereIn)
					.list();
	}
	
	/**
	 * Busca o registro de Arquivonfnota a partir de uma chave de acesso e de um Arquivonf
	 *
	 * @param arquivonf
	 * @param chNFe
	 * @return
	 * @since 22/03/2012
	 * @author Rodrigo Freitas
	 */
	public Arquivonfnota findByChaveAcesso(Arquivonf arquivonf, String chNFe) {
		if(arquivonf == null || arquivonf.getCdarquivonf() == null){
			throw new SinedException("O id do arquivo de nota fiscal n�o pode ser nulo.");
		}
		if(chNFe == null){
			throw new SinedException("A chave de acesso n�o pode ser nula.");
		}
		return query()
					.select("arquivonfnota.cdarquivonfnota, nota.cdNota, nota.numero, notaStatus.cdNotaStatus")
					.join("arquivonfnota.nota nota")
					.join("nota.notaStatus notaStatus")
					.where("arquivonfnota.arquivonf = ?", arquivonf)
					.where("arquivonfnota.chaveacesso = ?", chNFe)
					.unique();
	}

	/**
	 * Atualiza o campos de um Arquivonfnota para o retorno da consulta de NF-e
	 *
	 * @param arquivonfnota
	 * @param dataRec
	 * @param nProtStr
	 * @since 22/03/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivonfnota(Arquivonfnota arquivonfnota, Timestamp dataRec, String nProtStr) {
		if(dataRec != null){
			getHibernateTemplate().bulkUpdate("update Arquivonfnota a set a.dtprocessamento = ?, a.protocolonfe = ? where a.id = ?",
					new Object[]{dataRec, nProtStr, arquivonfnota.getCdarquivonfnota()});
		}else {
			getHibernateTemplate().bulkUpdate("update Arquivonfnota a set a.protocolonfe = ? where a.id = ?",
					new Object[]{nProtStr, arquivonfnota.getCdarquivonfnota()});
		}
	}

	/**
	 * Atualiza a chave de acesso do item do lote.
	 *
	 * @param arquivonfnota
	 * @param chNFeStr
	 * @author Rodrigo Freitas
	 * @since 02/01/2013
	 */
	public void updateChaveacesso(Arquivonfnota arquivonfnota, String chNFeStr) {
		getHibernateTemplate().bulkUpdate("update Arquivonfnota a set a.chaveacesso = ? where a.id = ?",
				new Object[]{chNFeStr, arquivonfnota.getCdarquivonfnota()});
	}

	/**
	 * Atualiza o arquivo XML da nota.
	 *
	 * @param arquivonfnota
	 * @since 26/03/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoxml(Arquivonfnota arquivonfnota) {
		if(arquivonfnota == null || arquivonfnota.getCdarquivonfnota() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Arquivonfnota an set an.arquivoxml = ? where an.id = ?", 
										new Object[]{arquivonfnota.getArquivoxml(), arquivonfnota.getCdarquivonfnota()});
	}

	/**
	 * Busca o Arquivonfnota a partir de uma Notafiscalproduto, n�o pega as notas canceladas.
	 *
	 * @param nota
	 * @return
	 * @since 09/04/2012
	 * @author Rodrigo Freitas
	 */
	public Arquivonfnota findByNotaProduto(Notafiscalproduto nota) {
		if(nota == null || nota.getCdNota() == null){
			throw new SinedException("Nota n�o pode ser nula.");
		}
		
		return query()
					.select("arquivonfnota.cdarquivonfnota,arquivonfnota.numeronfse,arquivonfnota.codigoverificacao,arquivonfnota.dtcancelamento," +
							"empresa.cdpessoa, empresa.cnpj, " +
							"arquivonfnota.dtemissao, arquivonfnota.dtcompetencia, arquivonf.cdarquivonf, arquivoxml.cdarquivo, arquivoxmlretornoconsulta.cdarquivo, " +
							"arquivoxmlassinado.cdarquivo,arquivoretornocancelamento.cdarquivo, arquivoretornoconsulta.cdarquivo, arquivonfnota.protocolonfe, arquivonfnota.chaveacesso, " +
							"configuracaonfe.cdconfiguracaonfe, arquivonf.arquivonfsituacao, arquivonfnota.dtprocessamento, configuracaonfe.modelonfe, " +
							"nota.serienfe, notaStatus.cdNotaStatus")
					.join("arquivonfnota.nota nota")
					.join("nota.empresa empresa")
					.join("nota.notaStatus notaStatus")
					.join("arquivonfnota.arquivonf arquivonf")
					.leftOuterJoin("arquivonfnota.arquivoretornocancelamento arquivoretornocancelamento")
					.leftOuterJoin("arquivonf.configuracaonfe configuracaonfe")
					.leftOuterJoin("arquivonf.arquivoxmlassinado arquivoxmlassinado")
					.leftOuterJoin("arquivonf.arquivoretornoconsulta arquivoretornoconsulta")
					.leftOuterJoin("arquivonfnota.arquivoxml arquivoxml")
					.leftOuterJoin("arquivonfnota.arquivoxmlretornoconsulta arquivoxmlretornoconsulta")
					.where("arquivonfnota.dtcancelamento is null")
					.where("nota = ?", nota)
					.openParentheses()
					.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_ERRO)
					.or()
					.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_SUCESSO)
					.closeParentheses()
					.where("arquivonfnota.chaveacesso is not null")
					.where("arquivonfnota.protocolonfe is not null")
					.where("arquivonfnota.dtprocessamento is not null")
					.openParentheses()
						.where("nota.origemCupom = true")//Notas vindas de cupom podem n�o ter a configura��o de nfe
						.or()
						.where("configuracaonfe is not null")
					.closeParentheses()
					.orderBy("arquivonfnota.cdarquivonfnota desc")
					.unique();
	}
	

	/**
	 * Busca o registro de Arquivonfnota mesmo cancelada.
	 *
	 * @param nota
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/10/2012
	 */
	public Arquivonfnota findByNotaProdutoAndCancelada(Notafiscalproduto nota) {
		if(nota == null || nota.getCdNota() == null){
			throw new SinedException("Nota n�o pode ser nula.");
		}
		
		return query()
					.select("arquivonfnota.cdarquivonfnota, arquivonfnota.numeronfse, arquivonfnota.codigoverificacao, empresa.cdpessoa, empresa.cnpj, arquivonfnota.dtcancelamento, " +
							"arquivonfnota.dtemissao, arquivonfnota.dtcompetencia, arquivonf.cdarquivonf, arquivoxml.cdarquivo, arquivonfnota.qrCode, arquivoxmlretornoconsulta.cdarquivo, " +
							"arquivonf.arquivonfsituacao,arquivoxmlassinado.cdarquivo, arquivoretornoconsulta.cdarquivo, arquivonfnota.protocolonfe, arquivonfnota.chaveacesso, " +
							"configuracaonfe.cdconfiguracaonfe,arquivoretornocancelamento.cdarquivo,arquivonfnota.dtprocessamento, configuracaonfe.modelonfe, " +
							"arquivonf.tipocontigencia, arquivonf.dtentradacontigencia, arquivonf.justificativacontigencia, " +
							"nota.serienfe, notaStatus.cdNotaStatus")
					.join("arquivonfnota.nota nota")
					.join("nota.empresa empresa")
					.join("nota.notaStatus notaStatus")
					.join("arquivonfnota.arquivonf arquivonf")
					.leftOuterJoin("arquivonfnota.arquivoretornocancelamento arquivoretornocancelamento")
					.join("arquivonf.configuracaonfe configuracaonfe")
					.leftOuterJoin("arquivonf.arquivoxmlassinado arquivoxmlassinado")
					.leftOuterJoin("arquivonf.arquivoretornoconsulta arquivoretornoconsulta")
					.leftOuterJoin("arquivonfnota.arquivoxml arquivoxml")
					.leftOuterJoin("arquivonfnota.arquivoxmlretornoconsulta arquivoxmlretornoconsulta")
					.where("nota = ?", nota)
					.openParentheses()
					.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_ERRO)
					.or()
					.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_SUCESSO)
					.closeParentheses()
					.where("arquivonfnota.chaveacesso is not null")
					.where("arquivonfnota.protocolonfe is not null")
					.where("arquivonfnota.dtprocessamento is not null")
					.unique();
	}
	
	public Arquivonfnota findByNotaServicoAndCancelada(NotaFiscalServico nota) {
		if(nota == null || nota.getCdNota() == null){
			throw new SinedException("Nota n�o pode ser nula.");
		}
		
		return query()
					.select("arquivonf.cdarquivonf, arquivonf.arquivonfsituacao, arquivonf.tipocontigencia, arquivonf.dtentradacontigencia, arquivonf.justificativacontigencia, " +
							"arquivonfnota.cdarquivonfnota, arquivonfnota.numeronfse, arquivonfnota.codigoverificacao, arquivonfnota.dtemissao, " +
							"arquivonfnota.dtcompetencia, arquivonfnota.dtprocessamento, arquivonfnota.dtcancelamento," +
							"empresa.cdpessoa, empresa.cnpj, " +
							"arquivoxml.cdarquivo, arquivoxmlconsulta.cdarquivo, arquivoxmlretornoconsulta.cdarquivo, arquivoxmlcancelamento.cdarquivo, " +
							"configuracaonfe.cdconfiguracaonfe, configuracaonfe.modelonfe ")
					.join("arquivonfnota.nota nota")
					.join("nota.empresa empresa")
					.join("arquivonfnota.arquivonf arquivonf")
					.join("arquivonf.configuracaonfe configuracaonfe")
					.leftOuterJoin("arquivonfnota.arquivoxml arquivoxml")
					.leftOuterJoin("arquivonfnota.arquivoxmlconsulta arquivoxmlconsulta")
					.leftOuterJoin("arquivonfnota.arquivoxmlretornoconsulta arquivoxmlretornoconsulta")
					.leftOuterJoin("arquivonfnota.arquivoxmlcancelamento arquivoxmlcancelamento")
					.where("nota = ?", nota)
					.where("arquivonfnota.dtcompetencia is not null")
					.openParentheses()
						.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_ERRO)
						.or()
						.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_SUCESSO)
					.closeParentheses()
					.unique();
	}
	
	public List<Arquivonfnota> findByNotaProduto(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Par�metros inv�lidos.");
		}
		
		return query()
					.select("arquivonfnota.cdarquivonfnota, arquivonfnota.numeronfse, arquivonfnota.codigoverificacao, empresa.cdpessoa, " +
							"arquivonfnota.dtemissao, arquivonfnota.dtcompetencia, arquivonf.cdarquivonf, arquivoxml.cdarquivo, nota.cdNota, nota.serienfe, " +
							"arquivoxmlassinado.cdarquivo, arquivoretornoconsulta.cdarquivo, arquivonfnota.protocolonfe, arquivonfnota.chaveacesso, " +
							"configuracaonfe.cdconfiguracaonfe, configuracaonfe.serienfse, arquivonf.arquivonfsituacao, configuracaonfe.modelonfe")
					.join("arquivonfnota.nota nota")
					.join("nota.empresa empresa")
					.join("arquivonfnota.arquivonf arquivonf")
					.join("arquivonf.configuracaonfe configuracaonfe")
					.leftOuterJoin("arquivonf.arquivoxmlassinado arquivoxmlassinado")
					.leftOuterJoin("arquivonf.arquivoretornoconsulta arquivoretornoconsulta")
					.leftOuterJoin("arquivonfnota.arquivoxml arquivoxml")
					.where("arquivonfnota.dtcancelamento is null")
					.whereIn("nota.cdNota", whereIn)
					.openParentheses()
					.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_ERRO)
					.or()
					.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_SUCESSO)
					.closeParentheses()
					.where("arquivonfnota.chaveacesso is not null")
					.where("arquivonfnota.protocolonfe is not null")
					.where("arquivonfnota.dtprocessamento is not null")
					.list();
	}

	/**
	 * Busca informa��es necess�rias para o download do arquivo XML da NFS-e.
	 *
	 * @param whereIn
	 * @return
	 * @since 26/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonfnota> findForDownloadXml(String whereIn) {
		return query()
					.select("arquivonfnota.cdarquivonfnota, arquivonfnota.numeronfse, arquivoxml.cdarquivo, " +
							"arquivoretornoconsulta.cdarquivo, arquivonf.cdarquivonf, nota.cdNota, nota.numero, " +
							"cliente.cdpessoa, cliente.email, cliente.nome, configuracaonfe.prefixowebservice, " +
							"arquivoxml2.cdarquivo, arquivoretornoenvio.cdarquivo, arquivoxmlretornoconsulta.cdarquivo ")
					.leftOuterJoin("arquivonfnota.nota nota")
					.leftOuterJoin("nota.cliente cliente")
					.leftOuterJoin("arquivonfnota.arquivoxmlretornoconsulta arquivoxmlretornoconsulta")
					.leftOuterJoin("arquivonfnota.arquivonf arquivonf")
					.leftOuterJoin("arquivonf.configuracaonfe configuracaonfe")
					.leftOuterJoin("arquivonf.arquivoretornoconsulta arquivoretornoconsulta")
					.leftOuterJoin("arquivonf.arquivoretornoenvio arquivoretornoenvio")
					.leftOuterJoin("arquivonf.arquivoxml arquivoxml2")
					.leftOuterJoin("arquivonfnota.arquivoxml arquivoxml")
					.whereIn("nota.cdNota", whereIn)
					.where("arquivonfnota.cdarquivonfnota in (" +
								" select max(an.cdarquivonfnota) " +
								" from Arquivonfnota an " +
								" join an.arquivonf a " +
								" join an.nota n " +
								" where a.arquivonfsituacao = " + Arquivonfsituacao.PROCESSADO_COM_SUCESSO.ordinal() + " " + 
								" and n.cdNota = nota.cdNota " +
							")")
					.list();
	}

	/**
	 * Remover o cancelamento para o estorno de uma nota.
	 *
	 * @param nota
	 * @author Rodrigo Freitas
	 * @since 02/10/2012
	 */
	public void removerCancelamento(Nota nota) {
		getHibernateTemplate()
			.bulkUpdate("update Arquivonfnota an set an.arquivoxmlcancelamento = null, an.motivocancelamento = null where an.nota = ?", nota);
	}

	/**
	 * M�todo que atualiza o campo cancelamentoporevento
	 *
	 * @param arquivonfnota
	 * @param cancelamentoPorEvento
	 * @author Luiz Fernando
	 */
	public void updateCancelamentoPorEvento(Arquivonfnota arquivonfnota, Boolean cancelamentoPorEvento) {
		if(arquivonfnota == null || arquivonfnota.getCdarquivonfnota() == null){
			throw new SinedException("O id n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Arquivonfnota an set an.cancelamentoporevento = ? where an.id = ?", 
										new Object[]{cancelamentoPorEvento, arquivonfnota.getCdarquivonfnota()});
		
	}

	public Arquivonfnota findGeradaByNota(Nota nota) {
		List<Arquivonfnota> lista = query()
			.select("arquivonfnota.cdarquivonfnota, arquivonf.cdarquivonf")
			.join("arquivonfnota.nota nota")
			.join("arquivonfnota.arquivonf arquivonf")
			.where("nota = ?", nota)
			.openParentheses()
				.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.GERADO)
				.or()
				.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.PROCESSADO_COM_SUCESSO)
			.closeParentheses()
			.list();
		
		if(lista != null && lista.size() == 1){
			return lista.get(0);
		} else return null;
	}

	public boolean haveNotaEmitida(String numeronfse, Configuracaonfe configuracaonfe, Empresa empresa) {
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.from(Arquivonfnota.class)
					.leftOuterJoin("arquivonfnota.arquivonf arquivonf")
					.where("arquivonf.empresa = ?", empresa)
					.where("arquivonf.configuracaonfe = ?", configuracaonfe)
					.where("arquivonfnota.numeronfse = ?", numeronfse)
					.unique() > 0;
	}
	
	public boolean haveNotaEmitida(String numeronfse, String codigoverificacao) {
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.from(Arquivonfnota.class)
					.where("arquivonfnota.numeronfse = ?", numeronfse)
					.where("arquivonfnota.codigoverificacao = ?", codigoverificacao)
					.unique() > 0;
	}
	
	public boolean haveNotaEmitida(String numeronfse) {
		return newQueryBuilder(Long.class)
					.select("count(*)")
					.from(Arquivonfnota.class)
					.where("arquivonfnota.numeronfse = ?", numeronfse)
					.unique() > 0;
	}
	
	/**
	 * Busca uma lista de Arquivonfnota processado partir do whereIn de notas.
	 *
	 * @param whereIn
	 * @return
	 * @since 05/03/2012
	 * @author Jo�o Vitor
	 */
	public List<Arquivonfnota> findArquivoNotaProcessadoByNota(String whereIn, NumeroNfeNaoUtilizadoFiltro filtro) {
		if(whereIn == null || whereIn.equals("")){
			return new ArrayList<Arquivonfnota>();
		}
		return query()
					.select("arquivonfnota.cdarquivonfnota, arquivonfnota.dtemissao, arquivonfnota.dtprocessamento, " +
							"nota.cdNota, nota.notaStatus, nota.numeronota, nota.dtEmissao")
					.join("arquivonfnota.nota nota")
					.join("arquivonfnota.arquivonf arquivonf")
					.whereIn("nota.cdNota", whereIn)
					.where("nota.numeronota IS NOT NULL")
					.where("nota.empresa = ?", filtro.getEmpresa())
					.where("nota.notaTipo = ?", NotaTipo.NOTA_FISCAL_PRODUTO)
					.where("nota.notaStatus = ?", NotaStatus.CANCELADA)
					.where("nota.dtEmissao >= ?", filtro.getDtinicioemissao())
					.where("nota.dtEmissao <= ?", filtro.getDtfimemissao())
					.where("arquivonf.configuracaonfe = ?", filtro.getConfiguracaonfe())
					.list();
	}
	
	public List<Arquivonfnota> findForNotaNfeNaoUtilizadoNaoCanceladaByNumero(String whereIn, NumeroNfeNaoUtilizadoFiltro filtro){
		if (StringUtils.isBlank(whereIn)){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
		.select("arquivonfnota.cdarquivonfnota, arquivonfnota.dtemissao, arquivonfnota.dtprocessamento, " +
		"nota.cdNota, nota.notaStatus, nota.numeronota, nota.dtEmissao")
		.join("arquivonfnota.nota nota")
		.join("arquivonfnota.arquivonf arquivonf")
		.where("nota.empresa = ?", filtro.getEmpresa())
		.where("nota.notaTipo = ?", NotaTipo.NOTA_FISCAL_PRODUTO)
		.where("nota.notaStatus = ?", NotaStatus.CANCELADA)
		.where("arquivonf.configuracaonfe = ?", filtro.getConfiguracaonfe())
		.where("arquivonfnota.dtprocessamento IS NULL")
		.whereIn("nota.numeronota", whereIn)
		.list();
	}

	public void updateArquivoxmlconsulta(Arquivonfnota arquivonfnota) {
		getHibernateTemplate().bulkUpdate("update Arquivonfnota a set a.arquivoxmlconsulta = ? where a.cdarquivonfnota = ?", 
				new Object[]{arquivonfnota.getArquivoxmlconsulta(), arquivonfnota.getCdarquivonfnota()});
	}

	public List<Arquivonfnota> findArquivonfnotaConsulta(Empresa empresa) {
		return querySined()
				
				.select("arquivonfnota.cdarquivonfnota, arquivoxmlconsulta.cdarquivo, configuracaonfe.cdconfiguracaonfe, arquivonf.cdarquivonf, arquivonf.tipocontigencia ")
				.leftOuterJoin("arquivonfnota.arquivoxmlconsulta arquivoxmlconsulta")
				.leftOuterJoin("arquivonfnota.arquivonf arquivonf")
				.leftOuterJoin("arquivonf.empresa empresa")
				.leftOuterJoin("arquivonf.configuracaonfe configuracaonfe")
				.where("empresa = ?", empresa)
				.where("arquivoxmlconsulta is not null")
				.openParentheses()
					.where("arquivonfnota.consultandonota = ?", Boolean.FALSE)
					.or()
					.where("arquivonfnota.consultandonota is null")
				.closeParentheses()
				.list();
	}

	public Arquivonfnota loadByCdarquivonfnota(Integer cdarquivonfnota) {
		if(cdarquivonfnota == null){
			return null;
		}
		
		return query()
				.select("arquivonf.cdarquivonf, arquivonf.consultando")
				.leftOuterJoin("arquivonfnota.arquivonf arquivonf")
				.where("arquivonfnota.cdarquivonfnota = ?", cdarquivonfnota)
				.unique();
	}
	
	public Arquivonfnota loadUltimoWithArquivoNf(Notafiscalproduto nota) {
		if(nota == null){
			return null;
		}
		
		return query()
				.select("arquivonfnota.cdarquivonfnota, arquivonf.cdarquivonf, arquivonf.consultando")
				.leftOuterJoin("arquivonfnota.arquivonf arquivonf")
				.leftOuterJoin("arquivonfnota.nota nota")
				.where("nota = ?", nota)
				.orderBy("arquivonfnota.cdarquivonfnota desc")
				.setMaxResults(1)
				.unique();
	}
}
