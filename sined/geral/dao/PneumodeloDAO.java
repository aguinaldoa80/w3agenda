package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Pneumarca;
import br.com.linkcom.sined.geral.bean.Pneumodelo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PneumodeloFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

@DefaultOrderBy("pneumodelo.nome")
public class PneumodeloDAO extends GenericDAO<Pneumodelo> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Pneumodelo> query, FiltroListagem _filtro) {
		PneumodeloFiltro filtro = (PneumodeloFiltro) _filtro;
		
		query
			.select("pneumodelo.cdpneumodelo, pneumodelo.nome, pneumodelo.codigointegracao, pneumodelo.otr, pneumarca.nome")
			.join("pneumodelo.pneumarca pneumarca")
			.where("pneumodelo.pneumarca = ?", filtro.getPneumarca())
			.where("pneumodelo.otr = ?", filtro.getOtr());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Pneumodelo> query) {
		query.joinFetch("pneumodelo.pneumarca pneumarca");
	}
	
	/**
	* M�todo autocomplete para buscar modelo de pneu
	*
	* @param q
	* @param whereInPneumarca
	* @return
	* @since 23/10/2015
	* @author Luiz Fernando
	*/
	public List<Pneumodelo> findAutocomplete(String q, String whereInPneumarca, boolean otr) {
		QueryBuilder<Pneumodelo> query = query()
						.select("pneumodelo.cdpneumodelo, pneumodelo.nome")
						.whereLikeIgnoreAll("pneumodelo.nome", q)
						.whereIn("pneumodelo.pneumarca.cdpneumarca", whereInPneumarca)
						.orderBy("pneumodelo.nome")
						.autocomplete();
		if(otr){
			query.where("coalesce(pneumodelo.otr, false) = true");
		}else{
			query.where("coalesce(pneumodelo.otr, false) = false");
		}
		return query.list();
	}
	
	/**
	 * Busca a lista de pneumodelo para enviar para o W3Producao
	 * @param whereIn
	 * @return
	 */
	public List<Pneumodelo> findForW3Producao(String whereIn){
		return query()
					.select("pneumodelo.cdpneumodelo, pneumodelo.nome, pneumarca.cdpneumarca")
					.leftOuterJoin("pneumodelo.pneumarca pneumarca")
					.whereIn("pneumodelo.cdpneumodelo", whereIn)
					.list();
	}
	
	/**
	 * Busca a modelo de pneu pelo nome.
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/04/2016
	 */
	public Pneumodelo loadByNomeMarca(String nome, Pneumarca pneumarca) {
		if(nome == null || nome.trim().equals("")){
			throw new SinedException("Par�metro inv�lido.");
		}
		if(pneumarca == null || pneumarca.getCdpneumarca() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
				.select("pneumodelo.cdpneumodelo, pneumodelo.nome")
				.where("pneumodelo.pneumarca = ?", pneumarca)
				.where("upper(pneumodelo.nome) like ?", nome.toUpperCase())
				.unique();
	}
	
	/**
	* M�todo que busca os modelos dos pneus
	*
	* @param whereIn
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<Pneumodelo> findForAndroid(String whereIn) {
		return query()
		.select("pneumodelo.cdpneumodelo, pneumodelo.nome, pneumarca.cdpneumarca")
		.leftOuterJoin("pneumodelo.pneumarca pneumarca")
		.whereIn("pneumodelo.cdpneumodelo", whereIn)
		.list();
	}
	
	@Override
	public ListagemResult<Pneumodelo> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Pneumodelo> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("pneumodelo.cdpneumodelo");
		
		return new ListagemResult<Pneumodelo>(query, filtro);
	}

	public List<Pneumodelo> findByPneuSegmento(Boolean otr, Pneumarca bean) {
		QueryBuilderSined<Pneumodelo> query = querySined();
		query
			.select("pneumodelo.cdpneumodelo, pneumodelo.nome, pneumarca.cdpneumarca")
			.leftOuterJoin("pneumodelo.pneumarca pneumarca")
			.where("pneumarca = ?", bean);
			if(Boolean.TRUE.equals(otr)){
				query.where("pneumodelo.otr = true");
			}else {
				query.where("pneumodelo.otr = false");
			}
			return query
					.orderBy("pneumodelo.nome")
					.list();
	}
}
