package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Agendainteracao;
import br.com.linkcom.sined.geral.bean.Agendainteracaohistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AgendainteracaohistoricoDAO extends GenericDAO<Agendainteracaohistorico>{

	public List<Agendainteracaohistorico> findByAgendainteracao(Agendainteracao agendainteracao){
		return query()
				.select("agendainteracaohistorico.cdagendainteracaohistorico, agendainteracaohistorico.dtaltera, agendainteracaohistorico.cdusuarioaltera, " +
						"agendainteracaohistorico.observacao, agendainteracao.dtcancelamento, agendainteracao.dtconclusao")
				.leftOuterJoin("agendainteracaohistorico.agendainteracao agendainteracao")
				.where("agendainteracao.cdagendainteracao = ?", agendainteracao.getCdagendainteracao())
				.orderBy("agendainteracaohistorico.dtaltera desc")
				.list();
	}
	
	public List<Agendainteracaohistorico> findForAndroidHistorico(String whereIn, String orderBy, boolean asc) {
		if (whereIn == null || whereIn.equals("")) {
			return new ArrayList<Agendainteracaohistorico>();
		}
		int qtdeDiasRetroativo = 90;
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Timestamp(System.currentTimeMillis()));
		cal.add(Calendar.DAY_OF_WEEK, (qtdeDiasRetroativo * -1));
		 
		return query()
				.select("agendainteracaohistorico.cdagendainteracaohistorico, agendainteracaohistorico.observacao, " +
						"agendainteracaohistorico.dtaltera, agendainteracaohistorico.interacaoapp, " +
						"agendainteracao.cdagendainteracao, agendainteracao.dtproximainteracao, " +
						"cliente.cdpessoa, cliente.nome, cliente.ativo, endereco.logradouro, " +
						"endereco.numero, endereco.bairro, endereco.complemento, " +
						"municipio.cdmunicipio, municipio.nome, uf.cduf, uf.nome, uf.sigla, " +
						"telefone.telefone, " +
						"empresa.nome, contatocontacrm.nome, telefone.telefone, contato.nome, " +
						"telefonecliente.telefone, responsavel.cdpessoa, responsavel.nome")
				.leftOuterJoin("agendainteracaohistorico.agendainteracao agendainteracao")
				.leftOuterJoin("agendainteracao.cliente cliente")
				.leftOuterJoin("agendainteracao.contacrm contacrm")
				.leftOuterJoin("agendainteracao.empresahistorico empresa")
				.leftOuterJoin("contacrm.listcontacrmcontato contatocontacrm")
				.leftOuterJoin("contatocontacrm.listcontacrmcontatofone telefone")
				.leftOuterJoin("cliente.listaContato listaContato")
				.leftOuterJoin("listaContato.contato contato")
				.leftOuterJoin("contato.listaTelefone telefonecliente")
				.leftOuterJoin("agendainteracao.responsavel responsavel")
				.leftOuterJoin("cliente.listaEndereco endereco")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")				
				.whereIn("empresa.cdpessoa", whereIn)
				.where("agendainteracaohistorico.interacaoapp = ?", Boolean.TRUE)
				.where("agendainteracaohistorico.dtaltera >= ?", new Timestamp(cal.getTime().getTime()))
				//.orderBy("agendainteracao.cdagendainteracao")
				.orderBy("agendainteracaohistorico.dtaltera")
				.list();

	}
	
}
