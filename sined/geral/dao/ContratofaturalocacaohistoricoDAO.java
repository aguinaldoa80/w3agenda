package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacaohistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContratofaturalocacaohistoricoDAO extends GenericDAO<Contratofaturalocacaohistorico>{

	/**
	 * Busca o histórico da fatura
	 *
	 * @param contratofaturalocacao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public List<Contratofaturalocacaohistorico> findByContratofaturalocacao(Contratofaturalocacao contratofaturalocacao) {
		return query()
					.where("contratofaturalocacaohistorico.contratofaturalocacao = ?", contratofaturalocacao)
					.orderBy("contratofaturalocacaohistorico.dtaltera DESC")
					.list();
	}

}
