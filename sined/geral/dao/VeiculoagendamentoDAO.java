package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Ordemservicotipo;
import br.com.linkcom.sined.geral.bean.Veiculoordemservico;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculoagendamentoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VeiculoagendamentoDAO extends GenericDAO<Veiculoordemservico> {
	
	
	@Override
	public void updateListagemQuery(QueryBuilder<Veiculoordemservico> query, FiltroListagem _filtro) {
		VeiculoagendamentoFiltro filtro = (VeiculoagendamentoFiltro) _filtro;
		query	
			.select("veiculoordemservico.cdveiculoordemservico, colaborador.nome, veiculo.placa, veiculo.prefixo," +
					"veiculoordemservico.enviaemail, veiculoordemservico.dtprevista")
			.join("veiculoordemservico.veiculo veiculo")
			.join("veiculo.colaborador colaborador")
			.whereLikeIgnoreAll("veiculo.placa", filtro.getPlaca())
			.where("veiculoordemservico.dtprevista >= ?",filtro.getDataInicio())
			.where("veiculoordemservico.dtprevista <= ?",filtro.getDataFim())
			.where("veiculoordemservico.ordemservicotipo = ?",Ordemservicotipo.AGENDAMENTO)
			.orderBy("veiculoordemservico.dtprevista");
		super.updateListagemQuery(query, _filtro);
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Veiculoordemservico> query) {
		query			
			.leftOuterJoinFetch("veiculoordemservico.veiculo veiculo")
			.leftOuterJoinFetch("veiculo.veiculomodelo veiculomodelo")
			.leftOuterJoinFetch("veiculo.colaborador colaborador")
			.leftOuterJoinFetch("veiculo.vwveiculo vwveiculo")
			;
		super.updateEntradaQuery(query);
	}
	
	public Veiculoordemservico findInspecaoDoAgendamento(Veiculoordemservico ordemservico){
		if(ordemservico.getDtprevista() == null){
			throw new SinedException("Data prevista n�o pode ser nula.");
		}
		return query()
			.select("veiculoordemservico.cdveiculoordemservico")
			.join("veiculoordemservico.veiculo veiculo")
			.join("veiculoordemservico.ordemservicotipo ordemservicotipo")
			.where("veiculo = ?", ordemservico.getVeiculo())
			.where("ordemservicotipo = ?",Ordemservicotipo.INSPECAO)
			.where("veiculoordemservico.dtrealizada = ?",ordemservico.getDtprevista())			
			.unique();
	}
	
	public Veiculoordemservico loadForDelete(Veiculoordemservico bean){
		if(bean == null || bean.getCdveiculoordemservico() == null){
			throw new SinedException("Agendamento n�o pode ser nulo.");
		}
		return query()
			.select("veiculoordemservico.cdveiculoordemservico, veiculo.cdveiculo, veiculoordemservico.dtprevista")
			.join("veiculoordemservico.veiculo veiculo")
			.where("veiculoordemservico = ?",bean)
			.unique();
	}

	/**
	 * M�todo que verifica se h� agendamento para o veiculo na data prevista
	 * 
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeAgendamentoData(Veiculoordemservico bean) {
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(beanClass)
			.join("veiculoordemservico.veiculo veiculo")
			.join("veiculoordemservico.ordemservicotipo ordemservicotipo")
			.where("veiculo = ?", bean.getVeiculo())
			.where("ordemservicotipo = ?", Ordemservicotipo.AGENDAMENTO)
			.where("veiculoordemservico.dtprevista = ?", bean.getDtprevista())
			.where("veiculoordemservico <> ?", bean.getCdveiculoordemservico() != null ? bean : null)
			.unique() > 0;
	}
}




