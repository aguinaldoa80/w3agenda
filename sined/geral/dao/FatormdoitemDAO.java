package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Fatormdoitem;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("fatormdoitem.sigla")
public class FatormdoitemDAO extends GenericDAO<Fatormdoitem>{
}
