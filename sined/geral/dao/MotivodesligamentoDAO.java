package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Motivodesligamento;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MotivodesligamentoDAO extends GenericDAO<Motivodesligamento> {
	
	/* singleton */
	private static MotivodesligamentoDAO instance;
	public static MotivodesligamentoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(MotivodesligamentoDAO.class);
		}
		return instance;
	}
		
}
