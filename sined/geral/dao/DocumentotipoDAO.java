package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.DocumentotipoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("documentotipo.nome")
public class DocumentotipoDAO extends GenericDAO<Documentotipo>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Documentotipo> query,FiltroListagem _filtro) {
		if(_filtro==null){
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		}
		DocumentotipoFiltro filtro = (DocumentotipoFiltro) _filtro;
		
		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		query
			.select("documentotipo.cddocumentotipo, documentotipo.nome, documentotipo.notafiscal, listaDocumentotipopapel.cddocumentotipopapel, papel.cdpapel, papel.nome, " +
					"tipotaxacontareceber.nome, documentotipo.taxanoprocessamentoextratocartaocredito")
			.leftOuterJoin("documentotipo.listaDocumentotipopapel listaDocumentotipopapel")
			.leftOuterJoin("documentotipo.tipotaxacontareceber tipotaxacontareceber")
			.leftOuterJoin("listaDocumentotipopapel.papel papel")
			.whereLikeIgnoreAll("documentotipo.nome",filtro.getNome())
			.where("documentotipo.notafiscal = ?", filtro.getNotafiscal());
			
			if (!administrador){
				if (whereInPapel != null && !whereInPapel.equals("")){
					query.openParentheses();
					query.whereIn("papel.cdpapel", whereInPapel)
					.or()
					.where("papel.cdpapel is null");
					query.closeParentheses();
				} else {
					query.where("papel.cdpapel is null");
				}
			}
			
			query.orderBy("documentotipo.nome");
	}

	/**
	 * Renorna uma lista de tipo de documento contendo apenas o campo Nome
	 * 
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Documentotipo> findDescricao(String whereIn) {
		return query()
			.select("documentotipo.nome")
			.whereIn("documentotipo.cddocumentotipo", whereIn)
			.list();
	}
	
	/**
	 * Carrega o tipo que tem a nota fiscal marcado.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Documentotipo findNotafiscal(){
		return query()
					.select("documentotipo.cddocumentotipo, documentotipo.notafiscal, documentotipo.taxanoprocessamentoextratocartaocredito, contadestino.cdconta, " +
							"tipotaxacontareceber.cdtipotaxa, tipotaxacontareceber.nome")
					.leftOuterJoin("documentotipo.contadestino contadestino")
					.leftOuterJoin("documentotipo.tipotaxacontareceber tipotaxacontareceber")
					.where("documentotipo.notafiscal = ?", Boolean.TRUE)
					.unique();
	}
	
	public Documentotipo findBoleto(){
		return query()
				.select("documentotipo.cddocumentotipo, documentotipo.boleto, contadestino.cdconta")
				.leftOuterJoin("documentotipo.contadestino contadestino")
				.where("documentotipo.boleto = ?", Boolean.TRUE)
				.unique();
	}

	/**
	 * Verifica se tem outro tipo com nota fiscal marcado.
	 *
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Boolean haveAnotherNota(Documentotipo bean) {
		QueryBuilder<Long> query = newQueryBuilder(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(Documentotipo.class)
					.where("documentotipo.notafiscal = ?", Boolean.TRUE);
		
		if(bean != null && bean.getCddocumentotipo() != null){
			query.where("documentotipo <> ?", bean);
		}
		
		return query.unique() > 0;
	}

	/**
	 * M�todo que busca os documentos tipos que podem ser exibidos na ordem de compra
	 * 
	 * @param documentotipo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Documentotipo> findDocumentoTipoOrdemCompra(Documentotipo documentotipo) {
		return query()
			.select("documentotipo.cddocumentotipo, documentotipo.nome")
			.openParentheses()
				.where("documentotipo.ordemcompra = ?", Boolean.TRUE)
				.where("documentotipo.exibirnacompra = ?", Boolean.TRUE)
			.closeParentheses()
			.or()
			.where("documentotipo = ?", documentotipo)
			.orderBy("documentotipo.nome")
			.list();
	}

	/**
	 * M�todo que retorna os tipos de documento
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Documentotipo> findAllForFlex() {
		return query()
			.select("documentotipo.cddocumentotipo, documentotipo.nome")
			.orderBy("documentotipo.nome")
			.list();
	}
		
	@Override
	public void updateEntradaQuery(QueryBuilder<Documentotipo> query) {
		query
			.leftOuterJoinFetch("documentotipo.formapagamentocredito formapagamentocredito")
			.leftOuterJoinFetch("documentotipo.formapagamentodebito formapagamentodebito")
			.leftOuterJoinFetch("documentotipo.listaDocumentotipopapel listaDocumentotipopapel")
			.leftOuterJoinFetch("listaDocumentotipopapel.papel papel")
			.leftOuterJoinFetch("documentotipo.contagerencialtaxa contagerencialtaxa")
			.leftOuterJoinFetch("documentotipo.prazoApropriacao prazoApropriacao")
			.leftOuterJoinFetch("contagerencialtaxa.vcontagerencial vcontagerencialtaxa");
	}
	
	
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				save.saveOrUpdateManaged("listaDocumentotipopapel");
				save.saveOrUpdateManaged("listaPrazoPagamentoECF");
				return null;
			}
		});
	}
	
	/**
	 * 
	 * M�todo que busca o DocumentoTipo com a listaDocumentoTipo e Papel para verificar n�veis de acesso no financeiro
	 *
	 *@author Thiago Augusto
	 *@date 20/03/2012
	 * @param documento
	 * @return
	 */
	public Documentotipo findDocumentoTipo(Documentotipo documentotipo){
		return querySined()
					
					.select("documentotipo.cddocumentotipo, documentotipo.nome, listaDocumentotipopapel.cddocumentotipopapel, papel.cdpapel, papel.nome, documentotipo.boleto ")
					.leftOuterJoin("documentotipo.listaDocumentotipopapel listaDocumentotipopapel")
					.leftOuterJoin("listaDocumentotipopapel.papel papel")
					.where("documentotipo = ? ", documentotipo)
					.unique();
	}
	
	/**
	 * 
	 * M�todo que busca a lista de DocumentoTipo a partir das permiss�es do usu�rio logado
	 *
	 *@author Thiago Augusto
	 *@param forVenda
	 *@param forCompra
	 *@date 23/03/2012
	 * @return
	 */
	public List<Documentotipo> getListaDocumentoTipoUsuario(Boolean forVenda, Boolean forCompra){

		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		QueryBuilder<Documentotipo> query = querySined()
					
					.select("documentotipo.cddocumentotipo, documentotipo.nome ")
					.leftOuterJoin("documentotipo.listaDocumentotipopapel listaDocumentotipopapel");
		
		
		if (!administrador){
			if (whereInPapel != null && !whereInPapel.equals("")){
				query.openParentheses();
				query.whereIn("listaDocumentotipopapel.papel.cdpapel", whereInPapel)
				.or()
				.where("listaDocumentotipopapel.papel is null");
				query.closeParentheses();
			} else {
				query.where("listaDocumentotipopapel.papel is null");
			}
		}
		if(forVenda != null){
			query.where("documentotipo.exibirnavenda = ?", forVenda);
		}
		
		if(forCompra != null){
			query.where("documentotipo.exibirnacompra = ?", forCompra);
		}
		
		return query
				.orderBy("documentotipo.nome")
				.list();
	}
	
	/**
     * Busca os dados para o cache da tela de pedido de venda offline
     */
	public List<Documentotipo> findForPVOffline() {
		return query()
		.select("documentotipo.cddocumentotipo, documentotipo.nome, documentotipo.boleto")
		.list();
	}
	
	public List<Documentotipo> findForAndroid(String whereIn) {
		return query()
		.select("documentotipo.cddocumentotipo, documentotipo.nome, documentotipo.boleto")
		.whereIn("documentotipo.cddocumentotipo", whereIn)
		.list();
	}

	/**
	 * M�todo que busca documentotipo boleto 
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentotipo> findDocumentoTipoBoleto(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("documentotipo.cddocumentotipo, documentotipo.nome, documentotipo.boleto ")
				.whereIn("documentotipo.cddocumentotipo", whereIn)
				.where("documentotipo.boleto = true")
				.list();
	}

	/**
	 * Buscar o tipo de documento para o processamento da integra��o do ECF
	 *
	 * @param finalizadoraId
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/04/2014
	 */
	public Documentotipo findByFinalizadoraEmporium(Integer finalizadoraId) {
		return query()
				.select("documentotipo.cddocumentotipo, documentotipo.nome, contadestino.cdconta, " +
						"documentotipo.diasreceber, documentotipo.taxavenda, documentotipo.percentual")
				.leftOuterJoin("documentotipo.contadestino contadestino")
				.where("documentotipo.finalizadoraemporium = ?", finalizadoraId)
				.unique();
	}

	/**
	 * M�todo que busca os documentos tipo de antecipa��o
	 *
	 * @return
	 * @author Luiz Fernando
	 * @since 25/10/2013
	 */
	public List<Documentotipo> findAntecipacao() {
		return query()
			.select("documentotipo.cddocumentotipo, documentotipo.nome")
			.where("documentotipo.antecipacao = true")
			.list();
	}

	/**
	 * 
	 * M�todo que busca a conta do tipo de documento para ser usada na tela de Conta Receber.
	 *
	 *@author Lucas Costa
	 *@date 21/07/2014
	 *@param documentotipo
	 *@return
	 */
	public Documentotipo findForContaReceber (Documentotipo documentotipo){
		if(documentotipo == null || documentotipo.getCddocumentotipo() == null){
			throw new SinedException("Erro na obten��o da conta banc�ria.");
		}
		
		return query()
			.select("documentotipo.boleto, contadestino.cdconta, contadestino.nome, listaContacarteira.cdcontacarteira,"+
					"listaContacarteira.carteira, listaContacarteira.msgboleto1, listaContacarteira.msgboleto2, listaContacarteira.padrao")
			.leftOuterJoin("documentotipo.contadestino contadestino")
			.leftOuterJoin("contadestino.listaContacarteira listaContacarteira")
			.where("documentotipo.cddocumentotipo = ?", documentotipo.getCddocumentotipo())
			.unique();
		
	}
	
	/**
	* M�todo que carrega o tipo de documento com a conta de destino (baixa)
	*
	* @param documentotipo
	* @return
	* @since 28/04/2015
	* @author Luiz Fernando
	*/
	public Documentotipo loadWithContadestino (Documentotipo documentotipo){
		if(documentotipo == null || documentotipo.getCddocumentotipo() == null){
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
			.select("documentotipo.cddocumentotipo, contadestino.cdconta, contadestino.nome, ")
			.leftOuterJoin("documentotipo.contadestino contadestino")
			.where("documentotipo = ?", documentotipo)
			.unique();
		
	}

	/**
	 * Busca formas de pagamento para o WS SOAP
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/04/2016
	 */
	public List<Documentotipo> findForWSSOAP() {
		return query()
					.select("documentotipo.cddocumentotipo, documentotipo.nome")
					.orderBy("documentotipo.nome")
					.list();
	}
	
	/**
	* M�todo que verifica se o tipo de documento � antecipa��o
	*
	* @param documentotipo
	* @return
	* @since 06/05/2016
	* @author Luiz Fernando
	*/
	public Boolean isAntecipacao(Documentotipo documentotipo) {
		if(documentotipo == null || documentotipo.getCddocumentotipo() == null){
			return false;
		}
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Documentotipo.class)
			.where("documentotipo = ?", documentotipo)
			.where("COALESCE(documentotipo.antecipacao, false) = true")
			.unique() > 0;
	}
	
	/**
	 * Busca as formas de pagamento do cliente
	 *
	 * @return
	 * @author Thiago Clemente
	 * @since 27/05/2016
	 * 
	 */
	public List<Documentotipo> findByCliente(Cliente cliente, boolean verificaPermissaoUsuarioLogado) {
		QueryBuilder<Documentotipo> queryBuilder = query();
		queryBuilder.select("documentotipo.cddocumentotipo, documentotipo.nome");
		queryBuilder.join("documentotipo.listaClienteDocumentoTipo listaClienteDocumentoTipo");
		queryBuilder.join("listaClienteDocumentoTipo.cliente cliente");
		queryBuilder.where("cliente=?", cliente);
		queryBuilder.orderBy("documentotipo.nome");
		
		if (verificaPermissaoUsuarioLogado && !Boolean.TRUE.equals(SinedUtil.isUsuarioLogadoAdministrador())){
			Usuario usuario = (Usuario)Neo.getUser();
			if (usuario.getListaUsuariopapel()!=null && !usuario.getListaUsuariopapel().isEmpty()){
				queryBuilder.join("documentotipo.listaDocumentotipopapel listaDocumentotipopapel");			
				queryBuilder.join("listaDocumentotipopapel.papel papel");			
				queryBuilder.whereIn("papel.cdpapel", CollectionsUtil.listAndConcatenate(usuario.getListaUsuariopapel(), "papel.cdpapel", ","));		
			}
		}
		
		return queryBuilder.list();
	}
	
	/**
	 * Busca a forma de pagamento utilizada pelo cliente na �ltima venda. 
	 *
	 * @return
	 * @author Thiago Clemente
	 * @since 03/06/2016
	 * 
	 */
	public Documentotipo getUltimaVenda(Cliente cliente) {
		return query()
				.select("documentotipo.cddocumentotipo, documentotipo.nome")
				.join("documentotipo.listaVenda listaVenda")
				.where("listaVenda.cliente=?", cliente)
				.orderBy("listaVenda.dtvenda desc")
				.setMaxResults(1)
				.unique();
	}

	/**
	 * Busca a lista de tipo de documento com a flag de permite venda sem an�lise de cr�dito
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/06/2016
	 */
	public List<Documentotipo> findWithPermiteVendaSemAnalise(String whereIn) {
		if(whereIn == null || whereIn.trim().equals("")){
			throw new SinedException("Par�metro inv�lido");
		}
		return query()
					.select("documentotipo.cddocumentotipo, documentotipo.nome, documentotipo.permitirvendasemanalisecredito")
					.whereIn("documentotipo.cddocumentotipo", whereIn)
					.list();
	}
	
	public boolean isMovimentacaoTaxa(Documentotipo documentotipo){
		if (documentotipo==null || documentotipo.getCddocumentotipo()==null){
			return false;
		}
		
		return newQueryBuilderWithFrom(Long.class)
				.select("count(*)")
				.join("documentotipo.centrocusto centrocusto")
				.join("documentotipo.contagerencialtaxa contagerencialtaxa")
				.where("documentotipo=?", documentotipo)
				.where("documentotipo.taxavenda>0")
				.where("documentotipo.gerarmovimentacaoseparadataxa=?", Boolean.TRUE)
				.unique()>0;
	}

	/**
	 * 
	 * M�todo que busca a lista de DocumentoTipo que possua a flag exibirnacompra marcada.
	 * Nota: Os ids passados pelo par�metro whereInIgnoreFlagCompra se referem aos tipos que devem ser exibidos mesmo que n�o possuam a flag marcada.
	 * Isso serve para os casos de cadastros antigos, onde n�o existia a flag(ou a flag mudou) para o tipo de documento no momento do cadastro.
	 *
	 *@author Mairon Cezar
	 *@param whereInIgnoreFlagCompra
	 *@date 05/01/2018
	 * @return
	 */
	public List<Documentotipo> findForCompra(String whereInIgnoreFlagCompra) {
		QueryBuilder<Documentotipo> query = querySined()
					.select("documentotipo.cddocumentotipo, documentotipo.nome, documentotipo.permitirvendasemanalisecredito");
		if(Util.strings.isNotEmpty(whereInIgnoreFlagCompra)){
			query
				.openParentheses()
					.where("documentotipo.exibirnacompra = ?", Boolean.TRUE)
					.or()
					.whereIn("documentotipo.cddocumentotipo", whereInIgnoreFlagCompra)
				.closeParentheses();
		}else{
			query.where("documentotipo.exibirnacompra = ?", Boolean.TRUE);
		}
		query.orderBy("documentotipo.nome");
		return query.list();
	}
	
	/**
	 * 
	 * M�todo que busca a lista de DocumentoTipo a partir das permiss�es do usu�rio logado e que possuem a flag exibirnavenda marcada.
	 * Nota: Os ids passados pelo par�metro whereInIgnoreFlagVenda se referem aos tipos que devem ser exibidos mesmo que n�o possuam a flag marcada.
	 * Isso serve para os casos de cadastros antigos, onde n�o existia a flag(ou a flag mudou) para o tipo de documento no momento do cadastro.
	 *
	 *@author Mairon Cezar
	 *@param whereInIgnoreFlagVenda
	 *@date 05/01/2018
	 * @return
	 */
	public List<Documentotipo> getListaDocumentoTipoUsuarioForVenda(String whereInIgnoreFlagVenda){

		String whereInPapel = SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		QueryBuilder<Documentotipo> query = querySined()
					
					.select("documentotipo.cddocumentotipo, documentotipo.nome ")
					.leftOuterJoin("documentotipo.listaDocumentotipopapel listaDocumentotipopapel");
		
		
		if (!administrador){
			if (whereInPapel != null && !whereInPapel.equals("")){
				query.openParentheses();
				query.whereIn("listaDocumentotipopapel.papel.cdpapel", whereInPapel)
				.or()
				.where("listaDocumentotipopapel.papel is null");
				query.closeParentheses();
			} else {
				query.where("listaDocumentotipopapel.papel is null");
			}
		}
		if(Util.strings.isNotEmpty(whereInIgnoreFlagVenda)){
			query.openParentheses();
			query.where("documentotipo.exibirnavenda = ?", Boolean.TRUE);
			
			
			query.or();
			query.whereIn("documentotipo.cddocumentotipo", whereInIgnoreFlagVenda);
			query.closeParentheses();
		}else{
			query.where("documentotipo.exibirnavenda = ?", Boolean.TRUE);
		}
		
		return query
				.orderBy("documentotipo.nome")
				.list();
	}
	
	/**
	 * 
	 * M�todo que busca a lista de DocumentoTipo a partir das permiss�es do usu�rio logado e que possuem a flag exibirnacompra marcada.
	 * Nota: Os ids passados pelo par�metro whereInIgnoreFlagCompra se referem aos tipos que devem ser exibidos mesmo que n�o possuam a flag marcada.
	 * Isso serve para os casos de cadastros antigos, onde n�o existia a flag(ou a flag mudou) para o tipo de documento no momento do cadastro.
	 *
	 *@author Mairon Cezar
	 *@param whereInIgnoreFlagCompra
	 *@date 05/01/2018
	 * @return
	 */
	public List<Documentotipo> getListaDocumentoTipoUsuarioForCompra(String whereInIgnoreFlagCompra){

		String whereInPapel =  SinedUtil.getWhereInCdPapel();
		boolean administrador = SinedUtil.isUsuarioLogadoAdministrador();
		
		QueryBuilder<Documentotipo> query = querySined()
					
					.select("documentotipo.cddocumentotipo, documentotipo.nome ")
					.leftOuterJoin("documentotipo.listaDocumentotipopapel listaDocumentotipopapel");
		
		
		if (!administrador){
			if (whereInPapel != null && !whereInPapel.equals("")){
				query.openParentheses();
				query.whereIn("listaDocumentotipopapel.papel.cdpapel", whereInPapel)
				.or()
				.where("listaDocumentotipopapel.papel is null");
				query.closeParentheses();
			} else {
				query.where("listaDocumentotipopapel.papel is null");
			}
		}
		if(Util.strings.isNotEmpty(whereInIgnoreFlagCompra)){
			query.openParentheses();
			query.where("documentotipo.exibirnacompra = ?", Boolean.TRUE);
			
			
			query.or();
			query.whereIn("documentotipo.cddocumentotipo", whereInIgnoreFlagCompra);
			query.closeParentheses();
		}else{
			query.where("documentotipo.exibirnacompra = ?", Boolean.TRUE);
		}
		
		return query
				.orderBy("documentotipo.nome")
				.list();
	}

	public List<Documentotipo> findByCodECF(Integer codECF, Documentotipo bean) {
		Integer cdDocTipo = null;
		if(bean != null ){
			cdDocTipo = bean.getCddocumentotipo();
		}
		if(codECF == null){
			codECF = -99999; // valor n�o valido para o campo
		}
		return query()
				.select("documentotipo.cddocumentotipo,documentotipo.nome," +
						"documentotipo.diasreceber, documentotipo.taxavenda, documentotipo.percentual," +
						"contadestino.cdconta," +
						"documentotipo.finalizadoraemporium," +
						"listaPrazoPagamentoECF.cdPrazoPagamentoECF,listaPrazoPagamentoECF.codECF," +
						"prazoPagamento.cdprazopagamento,prazoPagamento.nome" +
						"")
				.leftOuterJoin("documentotipo.listaPrazoPagamentoECF listaPrazoPagamentoECF")
				.leftOuterJoin("listaPrazoPagamentoECF.prazoPagamento prazoPagamento")
				.leftOuterJoin("documentotipo.contadestino contadestino")
				.where("documentotipo.cddocumentotipo <> ?", cdDocTipo)
				.openParentheses()
					.where("documentotipo.finalizadoraemporium = ?", codECF)
						.or()
					.where("listaPrazoPagamentoECF.codECF = ?", codECF)
				.closeParentheses()
				.list();
	}

	public Documentotipo loadListaForEntrada(Documentotipo bean) {
		return query()
				.select("documentotipo.cddocumentotipo," +
						"listaPrazoPagamentoECF.cdPrazoPagamentoECF,listaPrazoPagamentoECF.codECF," +
						"prazoPagamento.cdprazopagamento,prazoPagamento.nome")
				.leftOuterJoin("documentotipo.listaPrazoPagamentoECF listaPrazoPagamentoECF")
				.leftOuterJoin("listaPrazoPagamentoECF.prazoPagamento prazoPagamento")
				.where("documentotipo.cddocumentotipo = ?", bean.getCddocumentotipo())
				.unique();
	}
	
	public Documentotipo loadWithPrazoApropriacao(Documentotipo bean) {
		if(bean == null || bean.getCddocumentotipo() == null){
			return null;
		}
		return query()
				.select("documentotipo.cddocumentotipo," +
						"prazoApropriacao.cdprazopagamento,prazoApropriacao.nome")
				.leftOuterJoin("documentotipo.prazoApropriacao prazoApropriacao")
				.where("documentotipo.cddocumentotipo = ?", bean.getCddocumentotipo())
				.unique();
	}
}