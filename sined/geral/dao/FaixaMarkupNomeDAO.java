package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.FaixaMarkupNome;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.FaixaMarkupNomeFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FaixaMarkupNomeDAO extends GenericDAO<FaixaMarkupNome>{

	
	@Override
	public void updateListagemQuery(QueryBuilder<FaixaMarkupNome> query,
			FiltroListagem _filtro) {
		FaixaMarkupNomeFiltro filtro = (FaixaMarkupNomeFiltro)_filtro;
		query.whereLikeIgnoreAll("faixaMarkupNome.nome", filtro.getNome());
	}
	
	public FaixaMarkupNome loadByNome(String nome, Integer cdFaixaMarkupNomeExcecao){
		return query()
				.where("upper(faixaMarkupNome.nome) = upper(?)", nome.trim())
				.where("faixaMarkupNome.cdFaixaMarkupNome <> ?", cdFaixaMarkupNomeExcecao)
				.setMaxResults(1)
				.unique();
	}
}
