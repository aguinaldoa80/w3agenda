package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("produto.nome")
public class ProdutoDAO extends GenericDAO<Produto> {

	/**
	 * Carrega um produto com todas as informa��es do produto.
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Produto carregaProduto(Material material) {
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		return query()
		.select("produto.cdmaterial, produto.nomereduzido, produto.qtdeminima, " +
		"produto.observacao, produto.largura, produto.comprimento, produto.altura, produto.fatorconversaoproducao, " +
		"produto.materialcorte, produto.fatorconversao, produto.arredondamentocomprimento, produto.margemarredondamento")
		.where("produto.cdmaterial = ?",material.getCdmaterial())
		.unique();
	}
	/**
	 * Carrega o cdmaterial e a quantidade m�nima de produto.
	 * 
	 * @author Taidson
	 * @since 22/04/2010
	 */
	public Produto qtdeMinimaProduto(Integer cdmaterial) {
		if (cdmaterial == null) {
			throw new SinedException("O c�digo do material n�o pode ser nulo.");
		}
		return query()
		.select("produto.cdmaterial, produto.qtdeminima")
		.where("produto.cdmaterial = ?", cdmaterial)
		.unique();
	}
	
	/**
	 * Deleta somente o registro da tabela Produto.
	 * 
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void deleteProduto(Material material) {
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		Produto produto = new Produto();
		produto.setCdmaterial(material.getCdmaterial());
		produto = loadForEntrada(produto);
		
		String delete = "DELETE FROM PRODUTO WHERE CDMATERIAL = ?";
		getJdbcTemplate().update(delete, new Object[]{material.getCdmaterial()});
	}

	/**
	 * Insere um registro na tabela de Produto.
	 * 
	 * @param produto
	 * @author Rodrigo Freitas
	 */
	public void insertProduto(Produto produto) {
		if (produto == null || produto.getCdmaterial() == null) {
			throw new SinedException("Produto n�o pode ser nulo.");
		}
				
		String insert = "INSERT INTO PRODUTO (CDMATERIAL, NOMEREDUZIDO, QTDEMINIMA, OBSERVACAO, LARGURA, COMPRIMENTO, " +
						"CDUSUARIOALTERA, DTALTERA, ALTURA, MATERIALCORTE, FATORCONVERSAO, ARREDONDAMENTOCOMPRIMENTO, " +
						"MARGEMARREDONDAMENTO, FATORCONVERSAOPRODUCAO) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		getJdbcTemplate().update(insert, new Object[]{produto.getCdmaterial(), produto.getNomereduzido(), produto.getQtdeminima(),
													  produto.getObservacao(), produto.getLargura(), produto.getComprimento(), 
													  SinedUtil.getUsuarioLogado().getCdpessoa(), 
													  new Timestamp(System.currentTimeMillis()),
													  produto.getAltura(), produto.getMaterialcorte(), produto.getFatorconversao(),
													  produto.getArredondamentocomprimento(), produto.getMargemarredondamento(),
													  produto.getFatorconversaoproducao()});
	}

	
	/**
	 * Faz update no produto.
	 * 
	 * @param produto
	 * @author Rodrigo Freitas
	 */
	public void updateProduto(Produto produto){
		if (produto == null || produto.getCdmaterial() == null) {
			throw new SinedException("Produto n�o pode ser nulo.");
		}
		
		String update = "UPDATE PRODUTO SET NOMEREDUZIDO = ?, QTDEMINIMA = ?, OBSERVACAO = ?, " +
						"  LARGURA = ?, COMPRIMENTO = ?, CDUSUARIOALTERA = ?, DTALTERA = ?, " +
						"  ALTURA = ?, MATERIALCORTE = ?, FATORCONVERSAO = ?, ARREDONDAMENTOCOMPRIMENTO = ?, " +
						"MARGEMARREDONDAMENTO = ?, FATORCONVERSAOPRODUCAO = ?  WHERE PRODUTO.CDMATERIAL = ?";
		getJdbcTemplate().update(update, new Object[]{produto.getNomereduzido(), produto.getQtdeminima(),
				produto.getObservacao(), produto.getLargura(), produto.getComprimento(), 
				SinedUtil.getUsuarioLogado().getCdpessoa(), new Timestamp(System.currentTimeMillis()),
				produto.getAltura(), produto.getMaterialcorte(), produto.getFatorconversao(),
				produto.getArredondamentocomprimento(), produto.getMargemarredondamento(), 
				produto.getFatorconversaoproducao(), produto.getCdmaterial()});
	}
	
	public void updateDimensoesProduto(Produto produto){
		if (produto == null || produto.getCdmaterial() == null) {
			throw new SinedException("Produto n�o pode ser nulo.");
		}
		
		String update = "UPDATE PRODUTO SET LARGURA = ?, COMPRIMENTO = ?, ALTURA = ? WHERE PRODUTO.CDMATERIAL = ? ";
		getJdbcTemplate().update(update, new Object[]{produto.getLargura(), produto.getComprimento(), 
				produto.getAltura(), produto.getCdmaterial()});
	}
	
	/**
	 * Retorna o n�mero de registros da tabela Produto do cdmaterial passado.
	 * 
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Long countProduto(Material material){
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Produto.class)
					.where("produto.cdmaterial = ?",material.getCdmaterial())
					.unique();
	}
	
	public Produto carregaAlturaComprimentoLargura(Produto produto) {
		if(produto == null || produto.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("produto.cdmaterial, produto.largura, produto.comprimento, produto.altura, produto.fatorconversao, produto.fatorconversaoproducao, " +
						"produto.arredondamentocomprimento, produto.margemarredondamento ")
				.where("produto = ?", produto)
				.unique();
	}
	
	public List<Produto> findWithAlturaComprimentoLargura(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Produto> query = query()
				.select("produto.cdmaterial, produto.largura, produto.comprimento, produto.altura, produto.fatorconversao, produto.fatorconversaoproducao, " +
						"produto.arredondamentocomprimento, produto.margemarredondamento ");
		
		SinedUtil.quebraWhereIn("produto.cdmaterial", whereIn, query);
		
		return query
				.list();
	}

	/**
	 * Retorna o n�mero de registros que tem na tabela produto com o 
	 * mesmo nome do produto cadastrado.
	 * 
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Long verificaProduto(Material bean) {
		if (bean == null || bean.getProduto_nomereduzido() == null) {
			throw new SinedException("Nome do produto n�o pode ser nulo.");
		}
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.setUseTranslator(false)
					.from(Produto.class)
					.where("upper(produto.nomereduzido) = ?",bean.getProduto_nomereduzido().toUpperCase())
					.where("produto.cdmaterial <> ?",bean.getCdmaterial())
					.unique();
	}
	
	/**
	 * Busca os dados para a integra��o com a ECF.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @param idMaterial 
	 * @since 02/04/2013
	 */
	public List<Produto> findForIntegracaoEmporium(Integer idMaterial) {
		return query()
					.select("produto.cdmaterial, produto.nome, produto.nomereduzido, produto.ncmcompleto, produto.vendapromocional, produto.percentualimpostoecf, " +
							"produto.valorvenda, unidademedida.simbolo, unidademedida.nome, materialgrupo.cdmaterialgrupo, materialgrupo.nome, " +
							"materialrelacionado.quantidade, materialrelacionado.valorvenda, materialpromocao.cdmaterial, produto.codigobarras, " +
							"produto.ativo, tributacaoecf.cdtributacaoecf, tributacaoecf.codigoecf, " +
							"materialimposto.cdmaterialimposto, materialimposto.nacionalfederal, materialimposto.importadofederal, materialimposto.estadual, materialimposto.municipal, " +
							"empresa.cdpessoa, empresa.integracaopdv")
					.join("produto.unidademedida unidademedida")
					.join("produto.materialgrupo materialgrupo")
					.leftOuterJoin("produto.tributacaoecf tributacaoecf")
					.leftOuterJoin("produto.listaMaterialimposto materialimposto")
					.leftOuterJoin("materialimposto.empresa empresa")
					.leftOuterJoin("produto.listaMaterialrelacionado materialrelacionado")
					.leftOuterJoin("materialrelacionado.materialpromocao materialpromocao")
					.where("produto.vendaecf = ?", Boolean.TRUE)
					.where("produto.cdmaterial = ?", idMaterial)
					.list();
	}
	
	/**
	 * M�todo que busca os produtos para calculo do valor de custo
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Produto> findForCustovenda(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		QueryBuilder<Produto> query = query()
				.select("produto.cdmaterial, produto.largura, produto.comprimento, produto.altura, produto.fatorconversao, " +
						"produto.arredondamentocomprimento, produto.margemarredondamento ");
		SinedUtil.quebraWhereIn("produto.cdmaterial", whereIn, query);
		return query
				.openParentheses()
					.where("produto.largura is not null").or()
					.where("produto.comprimento is not null").or()
					.where("produto.altura is not null")
				.closeParentheses()
				.list();
	}
	
	public List<Produto> findForCalculoFormula(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("produto.cdmaterial, produto.largura, produto.altura, produto.peso")
				.whereIn("produto.cdmaterial", whereIn)
				.list();
	}
	
	/**
	 * M�todo que busca as quantidades e informa��es dos Produtos Mestres em um determinado processo de venda. 
	 * 
	 * @param whereIn
	 * @return
	 */
	public List<Produto> findProdutosMestre(String whereIn) {		
		return query()
			.select("produto.cdmaterial, produto.largura, produto.altura, produto.comprimento, listaProducao.cdmaterial, listaProducao.consumo," +
					"listaProducao.consumo, material.cdmaterial")
			.join("prodtuo.listaProducao listaProducao")
			.join("listaProducao.material material")
			.whereIn("produto.cdmaterial", whereIn)
			.list();
	}	
	
}
