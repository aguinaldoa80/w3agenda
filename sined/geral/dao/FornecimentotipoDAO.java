package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Fornecimentotipo;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.FornecimentotipoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("fornecimentotipo.nome")
public class FornecimentotipoDAO extends GenericDAO<Fornecimentotipo> {

	@Override
	public void updateListagemQuery(QueryBuilder<Fornecimentotipo> query, FiltroListagem _filtro) {
		FornecimentotipoFiltro filtro = (FornecimentotipoFiltro) _filtro;
		query
			.whereLikeIgnoreAll("fornecimentotipo.nome", filtro.getNome())
			.where("fornecimentotipo.ativo = ?",filtro.getAtivo());
	}
	
}
