package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Autorizacaohistorico;
import br.com.linkcom.sined.geral.bean.Autorizacaotrabalho;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class AutorizacaohistoricoDAO extends GenericDAO<Autorizacaohistorico>{
	ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	public List<Autorizacaohistorico> findBy(Autorizacaotrabalho autorizacaotrabalho){
		return
			query()
				.joinFetch("autorizacaohistorico.autorizacaotrabalho autorizacaotrabalho")
				.joinFetch("autorizacaohistorico.autorizacaoestado autorizacaoestado")
				.joinFetch("autorizacaohistorico.autorizacaoestadodestino autorizacaoestadodestino")
				.joinFetch("autorizacaohistorico.colaborador colaborador")
				.leftOuterJoinFetch("autorizacaohistorico.arquivo arquivo")
				.where("autorizacaotrabalho=?",autorizacaotrabalho)
				.orderBy("autorizacaohistorico.dtautorizacaoalterada desc,autorizacaohistorico.cdautorizacaohistorico desc")
				.list();
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				arquivoDAO.saveFile(save.getEntity(), "arquivo");			
				return null;
			}		
		});
	}
	
	public static AutorizacaohistoricoDAO instance;
	
	/*singleton*/
	public static AutorizacaohistoricoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(AutorizacaohistoricoDAO.class);
		}
		return instance;
	}
}
