package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaovendarepresentacaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class DocumentocomissaovendarepresentacaoDAO extends GenericDAO<Documentocomissao>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Documentocomissao> query, FiltroListagem _filtro) {
		DocumentocomissaovendarepresentacaoFiltro filtro = (DocumentocomissaovendarepresentacaoFiltro) _filtro;
				
		query
		.select("distinct documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome, " +
				"comissionamento.cdcomissionamento, comissionamento.percentual, comissionamento.valor, " +
				"documentocomissao.valorcomissao, " +
				"cliente.cdpessoa, cliente.nome, " +
				"colaboradorcomissao.cdcolaboradorcomissao, documentocc.cddocumento, documentocc.valor, " +
				"venda.cdvenda, documentoacaocc.cddocumentoacao, colaborador.nome, " +
				"fornecedorAgencia.cdpessoa, fornecedorAgencia.nome ")		
		.leftOuterJoin("documentocomissao.pessoa pessoa")
		.leftOuterJoin("documentocomissao.comissionamento comissionamento")
		.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
		.leftOuterJoin("colaboradorcomissao.documento documentocc")
		.leftOuterJoin("documentocc.documentoacao documentoacaocc")
		.join("documentocomissao.venda venda")
		.leftOuterJoin("venda.fornecedorAgencia fornecedorAgencia")
		.leftOuterJoin("documentocomissao.pedidovenda pedidovenda")
		.leftOuterJoin("venda.listavendamaterial listavendamaterial")
		.leftOuterJoin("listavendamaterial.material material")
		.leftOuterJoin("material.materialgrupo materialgrupo")
		.leftOuterJoin("venda.cliente cliente")
		.leftOuterJoin("venda.colaborador colaborador")
		.ignoreJoin("listavendamaterial", "material", "materialgrupo");
		
		preencheWhereQueryComissionamento(query, filtro);
		
		query.orderBy("cliente.nome, pessoa.nome");		
	}
	
	/**
	 * M�todo que calcula o total de comissionamento da listagem
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Money findForTotalDocumentocomissaovenda(DocumentocomissaovendarepresentacaoFiltro filtro) {
		QueryBuilder<Long> query = newQueryBuilder(Long.class);
		
		query.from(Documentocomissao.class)
			.select("SUM(documentocomissao.valorcomissao)")		
			.leftOuterJoin("documentocomissao.pessoa pessoa")
			.leftOuterJoin("documentocomissao.documento documento")
			.leftOuterJoin("documento.documentoacao documentoacao")
			.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
			.leftOuterJoin("colaboradorcomissao.documento documentocc")
			.join("documentocomissao.venda venda")
			.leftOuterJoin("venda.fornecedorAgencia fornecedorAgencia")
			.leftOuterJoin("documentocomissao.pedidovenda pedidovenda")
			.leftOuterJoin("venda.cliente cliente")
			.leftOuterJoin("venda.colaborador colaborador")
			.leftOuterJoin("venda.listavendamaterial listavendamaterial")
			.leftOuterJoin("listavendamaterial.material material")
			.leftOuterJoin("material.materialgrupo materialgrupo")
			;
		
		preencheWhereQueryComissionamento(query, filtro);			
			
		Long total = query.setUseTranslator(false).unique();
		
		if (total != null)
			return new Money(total != 0 ? total/100 : total);
		else 
			return new Money();
	}

	/**
	 * M�todo que busca as informa��es do documentocomissao (que n�o foram gerados pelo pedido de venda)
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentocomissao> findForRecalcularComissaoNotPedidovenda(Venda venda, Documento documento) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome, " +
						"comissionamento.cdcomissionamento, comissionamento.percentual, comissionamento.valor, " +
						"documento.cddocumento, documento.valor, documentocomissao.valorcomissao, " +
						"cliente.cdpessoa, cliente.nome, documentoacao.cddocumentoacao, documentoacao.nome, " +
						"colaboradorcomissao.cdcolaboradorcomissao, documentocc.cddocumento, documentocc.valor, aux_documento.valoratual," +
						"venda.cdvenda, pedidovenda.cdpedidovenda ")		
				.leftOuterJoin("documentocomissao.pessoa pessoa")
				.leftOuterJoin("documentocomissao.comissionamento comissionamento")
				.leftOuterJoin("documentocomissao.documento documento")
				.leftOuterJoin("documento.documentoacao documentoacao")
				.leftOuterJoin("documento.aux_documento aux_documento")
				.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
				.leftOuterJoin("colaboradorcomissao.documento documentocc")
				.join("documentocomissao.venda venda")
				.leftOuterJoin("documentocomissao.pedidovenda pedidovenda")
				.leftOuterJoin("venda.cliente cliente")
				.leftOuterJoin("venda.colaborador colaborador")
				.leftOuterJoin("venda.pedidovendatipo pedidovendatipo")
				.where("colaboradorcomissao.cdcolaboradorcomissao is null")
				.where("venda = ?", venda)
				.where("documento = ?", documento)
				.where("pedidovenda is null")
				.where("COALESCE(documentocomissao.representacao, false) = true")
				.list();			
	}
	
	/**
	 * M�todo que busca as informa��es do documentocomissao (gerados pelo pedido de venda)
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentocomissao> findForRecalcularComissaoWithPedidovenda(Venda venda) {
		if(venda == null || venda.getCdvenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome, " +
						"comissionamento.cdcomissionamento, comissionamento.percentual, comissionamento.valor, " +
						"documento.cddocumento, documento.valor, documentocomissao.valorcomissao, " +
						"cliente.cdpessoa, cliente.nome, documentoacao.cddocumentoacao, documentoacao.nome, " +
						"colaboradorcomissao.cdcolaboradorcomissao, documentocc.cddocumento, documentocc.valor, aux_documento.valoratual," +
						"venda.cdvenda, pedidovenda.cdpedidovenda ")		
				.leftOuterJoin("documentocomissao.pessoa pessoa")
				.leftOuterJoin("documentocomissao.comissionamento comissionamento")
				.leftOuterJoin("documentocomissao.documento documento")
				.leftOuterJoin("documento.documentoacao documentoacao")
				.leftOuterJoin("documento.aux_documento aux_documento")
				.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
				.leftOuterJoin("colaboradorcomissao.documento documentocc")
				.join("documentocomissao.venda venda")
				.join("documentocomissao.pedidovenda pedidovenda")
				.leftOuterJoin("venda.cliente cliente")
				.leftOuterJoin("venda.colaborador colaborador")
				.where("colaboradorcomissao.cdcolaboradorcomissao is null")
				.where("venda = ?", venda)
				.where("COALESCE(documentocomissao.representacao, false) = true")
				.list();			
	}

	public void deleteDocumentocomissaovenda(Documentocomissao documentocomissao) {
		if(documentocomissao != null && documentocomissao.getCddocumentocomissao() != null){
			getHibernateTemplate().bulkUpdate("delete Documentocomissao dc where dc.cddocumentocomissao = ?",
					new Object[]{documentocomissao.getCddocumentocomissao()});
		}		
	}

	public List<Documentocomissao> findForPDF(
			DocumentocomissaovendarepresentacaoFiltro filtro) {
		QueryBuilder<Documentocomissao> query = querySined();
		updateListagemQuery(query, filtro);
		return query.list();
	}
	
	private void preencheWhereQueryComissionamento(QueryBuilder<? extends Object> query, DocumentocomissaovendarepresentacaoFiltro filtro){
		query
			.where("documentocomissao.representacao = true")
			.where("colaborador = ?", filtro.getColaboradorvendedor())
			.where("venda.dtvenda >= ?", filtro.getDtinicio())
			.where("venda.dtvenda <= ?", filtro.getDtfim())
			.where("venda.cliente = ?", filtro.getCliente());
		
		if(filtro.getTipocolaborador() != null && filtro.getTipocolaborador().equals("Ag�ncia")){
			query
				.where("pessoa = ?", filtro.getFornecedoragencia())
				.where("COALESCE(documentocomissao.agencia, false) = true");
		}else {
			query.where("COALESCE(documentocomissao.agencia, false) = false");
		}
		
		if(filtro.getColaboradorcomissao() != null && filtro.getColaboradorcomissao().getCdpessoa() != null){
			query.where("pessoa.cdpessoa = ?", filtro.getColaboradorcomissao().getCdpessoa());
		}
		
	}

	public List<Documentocomissao> findForRecalcularComissao(Pedidovenda pedidovenda, Documento documento) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome, " +
						"comissionamento.cdcomissionamento, comissionamento.percentual, comissionamento.valor, " +
						"documento.cddocumento, documento.valor, documentocomissao.valorcomissao, " +
						"cliente.cdpessoa, cliente.nome, documentoacao.cddocumentoacao, documentoacao.nome, " +
						"colaboradorcomissao.cdcolaboradorcomissao, documentocc.cddocumento, documentocc.valor, aux_documento.valoratual," +
						"pedidovenda.cdpedidovenda ")		
				.leftOuterJoin("documentocomissao.pessoa pessoa")
				.leftOuterJoin("documentocomissao.comissionamento comissionamento")
				.leftOuterJoin("documentocomissao.documento documento")
				.leftOuterJoin("documento.documentoacao documentoacao")
				.leftOuterJoin("documento.aux_documento aux_documento")
				.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
				.leftOuterJoin("colaboradorcomissao.documento documentocc")
				.join("documentocomissao.pedidovenda pedidovenda")
				.leftOuterJoin("pedidovenda.cliente cliente")
				.leftOuterJoin("pedidovenda.colaborador colaborador")
				.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
				.where("colaboradorcomissao.cdcolaboradorcomissao is null")
				.where("pedidovenda = ?", pedidovenda)
				.where("documento = ?", documento)
				.where("COALESCE(documentocomissao.representacao, false) = true")
				.list();
	}

	public List<Documentocomissao> findForRecalcularComissaoWithPedidovenda(Pedidovenda pedidovenda) {
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("documentocomissao.cddocumentocomissao, pessoa.cdpessoa, pessoa.nome, " +
						"comissionamento.cdcomissionamento, comissionamento.percentual, comissionamento.valor, " +
						"documento.cddocumento, documento.valor, documentocomissao.valorcomissao, " +
						"cliente.cdpessoa, cliente.nome, documentoacao.cddocumentoacao, documentoacao.nome, " +
						"colaboradorcomissao.cdcolaboradorcomissao, documentocc.cddocumento, documentocc.valor, aux_documento.valoratual," +
						"pedidovenda.cdpedidovenda ")		
				.leftOuterJoin("documentocomissao.pessoa pessoa")
				.leftOuterJoin("documentocomissao.comissionamento comissionamento")
				.leftOuterJoin("documentocomissao.documento documento")
				.leftOuterJoin("documento.documentoacao documentoacao")
				.leftOuterJoin("documento.aux_documento aux_documento")
				.leftOuterJoin("documentocomissao.colaboradorcomissao colaboradorcomissao")
				.leftOuterJoin("colaboradorcomissao.documento documentocc")
				.join("documentocomissao.pedidovenda pedidovenda")
				.leftOuterJoin("pedidovenda.cliente cliente")
				.leftOuterJoin("pedidovenda.colaborador colaborador")
				.where("colaboradorcomissao.cdcolaboradorcomissao is null")
				.where("pedidovenda = ?", pedidovenda)
				.where("COALESCE(documentocomissao.representacao, false) = true")
				.list();
	}
}
