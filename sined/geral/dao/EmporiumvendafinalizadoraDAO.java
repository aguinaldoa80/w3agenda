package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Emporiumvenda;
import br.com.linkcom.sined.geral.bean.Emporiumvendafinalizadora;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EmporiumvendafinalizadoraDAO extends GenericDAO<Emporiumvendafinalizadora>{

	public List<Emporiumvendafinalizadora> findByEmporiumvenda(Emporiumvenda emporiumvenda) {
		return query()
				.where("emporiumvendafinalizadora.emporiumvenda = ?", emporiumvenda)
				.list();
	}
	

}
