package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Veiculodespesa;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculodespesaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VeiculodespesaDAO extends GenericDAO<Veiculodespesa> {

	@Override
	public void updateListagemQuery(QueryBuilder<Veiculodespesa> query,	FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		VeiculodespesaFiltro filtro = (VeiculodespesaFiltro) _filtro;
		query
			.select("veiculodespesa.cdveiculodespesa, veiculodespesa.motivo, veiculodespesa.dtentrada, veiculodespesa.valortotal, " +
					"veiculo.placa, bempatrimonio.nome, veiculodespesatipo.descricao, empresa.nome, empresa.razaosocial, empresa.nomefantasia, veiculodespesa.baixado, veiculodespesa.faturado, " +
					"colaborador.nome, veiculo.placa")
			.leftOuterJoin("veiculodespesa.veiculo veiculo")
			.leftOuterJoin("veiculodespesa.colaborador colaborador")
			.leftOuterJoin("veiculo.patrimonioitem patrimonioitem")
			.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
			.leftOuterJoin("veiculodespesa.empresa empresa")
			.leftOuterJoin("veiculodespesa.veiculodespesatipo veiculodespesatipo")
			.where("empresa = ?", filtro.getEmpresa())
			.where("veiculo = ?", filtro.getVeiculo())
			.where("colaborador = ?", filtro.getColaborador())
			.where("veiculodespesa.dtentrada >= ?", filtro.getDtentradade())
			.where("veiculodespesa.dtentrada <= ?", filtro.getDtentradaate())
			.where("veiculodespesa.baixado = ?", filtro.getBaixado())
			.where("veiculodespesa.faturado = ?", filtro.getFaturado())
			.whereLikeIgnoreAll("veiculo.placa", filtro.getPlaca())
			.orderBy("veiculodespesa.cdveiculodespesa desc");
		
		if(SinedUtil.isListNotEmpty(filtro.getListadespesatipo())){
			query.whereIn("veiculodespesatipo.cdveiculodespesatipo", CollectionsUtil.listAndConcatenate(filtro.getListadespesatipo(), "cdveiculodespesatipo", ","));
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Veiculodespesa> query) {
		query
			.select("veiculodespesa.cdveiculodespesa, veiculodespesa.motivo, veiculodespesa.faturado, veiculodespesa.baixado, veiculodespesa.km, veiculodespesa.dtentrada, veiculodespesa.veiculodespesadefinirprojeto, " +
					"veiculodespesa.valortotal, veiculodespesatipo.cdveiculodespesatipo, veiculo.cdveiculo, veiculo.placa, bempatrimonio.nome, empresa.cdpessoa, " +
					"fornecedor.cdpessoa, fornecedor.nome, projeto.cdprojeto, centrocusto.cdcentrocusto, listaveiculodespesaitem.cdveiculodespesaitem, " +
					"listaveiculodespesaitem.nome, listaveiculodespesaitem.quantidade, listaveiculodespesaitem.valorunitario, listaveiculodespesaitemprojeto.cdprojeto, " +
					"material.cdmaterial, material.nome, material.identificacao, material.valorcusto, " +
					"materialRateioEstoque.cdMaterialRateioEstoque, contaGerencialConsumo.cdcontagerencial, centrocustoItem.cdcentrocusto, " +
					"colaborador.cdpessoa, veiculodespesa.valorrealgasolina, listaveiculodespesaitem.consumoproprio, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome")
			.leftOuterJoin("veiculodespesa.veiculodespesatipo veiculodespesatipo")
			.leftOuterJoin("veiculodespesa.listaveiculodespesaitem listaveiculodespesaitem")
			.leftOuterJoin("veiculodespesa.veiculo veiculo")
			.leftOuterJoin("veiculo.patrimonioitem patrimonioitem")
			.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
			.leftOuterJoin("veiculodespesa.fornecedor fornecedor")
			.leftOuterJoin("veiculodespesa.empresa empresa")
			.leftOuterJoin("veiculodespesa.centrocusto centrocusto")
			.leftOuterJoin("veiculodespesa.projeto projeto")
			.leftOuterJoin("veiculodespesa.colaborador colaborador")
			.leftOuterJoin("listaveiculodespesaitem.material material")
			.leftOuterJoin("material.materialRateioEstoque materialRateioEstoque")
			.leftOuterJoin("materialRateioEstoque.contaGerencialConsumo contaGerencialConsumo")
			.leftOuterJoin("listaveiculodespesaitem.projeto listaveiculodespesaitemprojeto")
			.leftOuterJoin("listaveiculodespesaitem.localarmazenagem localarmazenagem")
			.leftOuterJoin("listaveiculodespesaitem.centrocusto centrocustoItem");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaveiculodespesaitem");
	}

	/**
	 * M�todo que busca as despesas para faturamento
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Veiculodespesa> findForFaturar(String whereIn) {
		if (whereIn == null || whereIn.equals("")) 
			throw new SinedException("Where In dos ids de despesas n�o pode ser nulo.");
		return query()
			.select("veiculodespesa.cdveiculodespesa, veiculodespesa.baixado, veiculodespesa.faturado, centrocusto.cdcentrocusto, centrocusto.nome, veiculodespesa.veiculodespesadefinirprojeto, " +
				"projeto.cdprojeto, projeto.nome, veiculodespesa.valortotal, fornecedor.cdpessoa, fornecedor.nome, listaveiculodespesaitem.cdveiculodespesaitem, " +
				"listaveiculodespesaitem.quantidade, listaveiculodespesaitem.valorunitario, listaveiculodespesaitemprojeto.cdprojeto")
			.join("veiculodespesa.centrocusto centrocusto")
			.join("veiculodespesa.fornecedor fornecedor")
			.leftOuterJoin("veiculodespesa.projeto projeto")
			.leftOuterJoin("veiculodespesa.listaveiculodespesaitem listaveiculodespesaitem")
			.leftOuterJoin("listaveiculodespesaitem.projeto listaveiculodespesaitemprojeto")
			.whereIn("veiculodespesa.cdveiculodespesa", whereIn)
			.list();
	}

	/**
	 * M�todo que atualiza os veiculos despesa para baixado. Quando as contas a pagar s�o criadas.
	 * 
	 * @param whereIn
	 * @author Tom�s Rabelo
	 */
	public void updateFaturado(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Veiculo despesa n�o pode ser nulo.");
		}
		getJdbcTemplate().update("update Veiculodespesa set faturado = true where cdveiculodespesa in ("+whereIn+")");
	}
	
	public void updateBaixado(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Veiculo despesa n�o pode ser nulo.");
		}
		getJdbcTemplate().update("update Veiculodespesa set baixado = true where cdveiculodespesa in ("+whereIn+")");
	}
	
	/**
	 * 
	 * M�todo que busca a lista de VeiculoDespesa para o relat�rio.
	 *
	 * @name findForReport
	 * @param filtro
	 * @return
	 * @return List<Veiculodespesa>
	 * @author Thiago Augusto
	 * @date 11/07/2012
	 *
	 */
	public List<Veiculodespesa> findForReport(VeiculodespesaFiltro filtro){
		return querySined()
				
			.select("veiculodespesa.cdveiculodespesa, veiculodespesa.motivo, veiculodespesa.dtentrada, veiculodespesa.valortotal, " +
					"veiculo.placa, bempatrimonio.nome, veiculodespesatipo.cdveiculodespesatipo ,veiculodespesatipo.descricao, empresa.nome, empresa.razaosocial, empresa.nomefantasia, " +
					"veiculodespesa.baixado, veiculodespesa.km, listaveiculodespesaitem.cdveiculodespesaitem, listaveiculodespesaitem.quantidade")
			.leftOuterJoin("veiculodespesa.veiculo veiculo")
			.leftOuterJoin("veiculo.patrimonioitem patrimonioitem")
			.leftOuterJoin("patrimonioitem.bempatrimonio bempatrimonio")
			.leftOuterJoin("veiculodespesa.empresa empresa")
			.leftOuterJoin("veiculodespesa.veiculodespesatipo veiculodespesatipo")
			.leftOuterJoin("veiculodespesa.listaveiculodespesaitem listaveiculodespesaitem")
			.where("empresa = ?", filtro.getEmpresa())
			.where("veiculo = ?", filtro.getVeiculo())
			.where("veiculodespesa.dtentrada >= ?", filtro.getDtentradade())
			.where("veiculodespesa.dtentrada <= ?", filtro.getDtentradaate())
			.whereIn("veiculodespesatipo.cdveiculodespesatipo", CollectionsUtil.listAndConcatenate(filtro.getListadespesatipo(), "cdveiculodespesatipo", ","))
			.list();
	}

	@SuppressWarnings("unchecked")
	public List<Veiculodespesa> findForCalculoKmPorLitro(String whereIn) {
		if(StringUtils.isBlank(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		String sql = 
					"select query.cdveiculodespesa, query.km_anterior " +
					"from (" +
					" select vd.cdveiculodespesa, " + 
					" 	(select vd2.km  " +
					" 	from veiculodespesa vd2 " + 
					" 	where vd2.cdveiculo = vd.cdveiculo " + 
					" 	and vd2.dtentrada <= vd.dtentrada " +
					" 	and vd2.cdveiculodespesa < vd.cdveiculodespesa " +
					" 	and vd2.cdveiculodespesatipo = 4 " +
					" 	order by vd2.dtentrada desc, vd2.cdveiculodespesa desc " +
					" 	limit 1) as km_anterior " +
					
					" from veiculodespesa vd " +
					" where vd.cdveiculodespesa in (" + whereIn + ")" +
					") as query " +
					"where query.km_anterior is not null ";

		SinedUtil.markAsReader();
		List<Veiculodespesa> list = getJdbcTemplate().query(sql ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Veiculodespesa(rs.getInt("cdveiculodespesa"), rs.getInt("km_anterior"));
			}
		});
		
		return list;
	}
	
}
