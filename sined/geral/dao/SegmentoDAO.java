package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.SegmentoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("nome")
public class SegmentoDAO extends GenericDAO<Segmento>{

	@Override
	public void updateListagemQuery(QueryBuilder<Segmento> query,
			FiltroListagem _filtro) {
		
		SegmentoFiltro filtro = (SegmentoFiltro)_filtro;
		
		query
			.select("segmento.cdsegmento, segmento.nome")
			.whereLikeIgnoreAll("segmento.nome", filtro.getNome());
		
		super.updateListagemQuery(query, _filtro);
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("campoExtraSegmentoList");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Segmento> query) {
		query.leftOuterJoinFetch("segmento.campoExtraSegmentoList campoExtraSegmentoList");
	}

	public Segmento findByNome(String nome) {
		if(nome == null || nome.equals("")){
			throw new SinedException("Nome da qualifica��o n�o pode ser nulo.");
		}
		
		String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
		List<Segmento> lista = query()
			.select("segmento.cdsegmento, segmento.nome")
			.where("UPPER(" + funcaoTiraacento + "(segmento.nome)) LIKE ?", Util.strings.tiraAcento(nome).toUpperCase())
			.list();
		
		if(lista != null && lista.size() > 1){
			throw new SinedException("Mais de um segmento encontrado.");
		} else if(lista != null && lista.size() == 1){
			return lista.get(0);
		} else return null;
	}

}
