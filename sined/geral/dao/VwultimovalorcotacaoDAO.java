package br.com.linkcom.sined.geral.dao;


import java.util.List;

import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.view.Vwultimovalorcotacao;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VwultimovalorcotacaoDAO extends GenericDAO<Vwultimovalorcotacao> {

	public List<Vwultimovalorcotacao> findUltimosValores(Fornecedor fornecedor, String whereInMaterial) {
		return query()
			.select("vwultimovalorcotacao.id, vwultimovalorcotacao.tipo, vwultimovalorcotacao.valor, material.cdmaterial, fornecedor.cdpessoa")
			.join("vwultimovalorcotacao.material material")
			.join("vwultimovalorcotacao.fornecedor fornecedor")
			.where("fornecedor.cdpessoa = ?", fornecedor.getCdpessoa())
			.whereIn("material.cdmaterial", whereInMaterial)
			.list();
	}

	
}
