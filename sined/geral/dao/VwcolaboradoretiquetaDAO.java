package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.view.Vwcolaboradoretiqueta;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.VwcolaboradoretiquetaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VwcolaboradoretiquetaDAO extends GenericDAO<Vwcolaboradoretiqueta>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Vwcolaboradoretiqueta> query,FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		}
		VwcolaboradoretiquetaFiltro filtro = (VwcolaboradoretiquetaFiltro)_filtro;
		
		query
			.orderBy("pessoa_nome");
			if(filtro.getDepartamento() != null){
				query.where("cddepartamento = ?", filtro.getDepartamento().getCddepartamento());
			}
			if(filtro.getColaboradorsituacao() != null){
				query.where("cdsituacao = ?", filtro.getColaboradorsituacao().getCdcolaboradorsituacao());
			}else{
				query.where("1=0");
			}
	}
	
	public List<Vwcolaboradoretiqueta> findForColaboradorEtiqueta(String whereIn) {
		return querySined()
		.whereIn("id", whereIn)
		.list();
	}
	

}
