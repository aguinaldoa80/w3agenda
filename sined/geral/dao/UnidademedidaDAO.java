package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.UnidademedidaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("unidademedida.nome")
public class UnidademedidaDAO extends GenericDAO<Unidademedida> {

	@Override
	public void updateListagemQuery(QueryBuilder<Unidademedida> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		UnidademedidaFiltro filtro = (UnidademedidaFiltro) _filtro;
		query
			.select("unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, unidademedida.ativo")
			.whereLikeIgnoreAll("unidademedida.nome", filtro.getNome())
			.whereLikeIgnoreAll("unidademedida.simbolo", filtro.getSimbolo())
			.where("unidademedida.ativo = ?", filtro.getAtivo());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Unidademedida> query) {
	//	query.leftOuterJoinFetch("unidademedida.listaUnidademedidaconversao listaUnidademedidaconversao");
		query
		.select("unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, unidademedida.ativo, unidademedida.casasdecimaisestoque, unidademedida.etiquetaunica," +
				"listaUnidademedidaconversao.fracao, listaUnidademedidaconversao.qtdereferencia, listaUnidademedidaconversao.cdunidademedidaconversao, " +
				"unidademedidarelacionada.cdunidademedida, unidademedidarelacionada.nome, unidademedidarelacionada.simbolo, unidademedidarelacionada.ativo")
		.leftOuterJoin("unidademedida.listaUnidademedidaconversao listaUnidademedidaconversao")
		.leftOuterJoin("listaUnidademedidaconversao.unidademedidarelacionada unidademedidarelacionada");
	
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaUnidademedidaconversao", "unidademedida");
	}
	
	/**
	 * Carrega a lista de unidades de medida para ser exibida na parte em flex.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Unidademedida> findAllForFlex(){
		return query()
			.select("unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo")
			.where("unidademedida.ativo = ?", Boolean.TRUE)
			.orderBy("unidademedida.nome")
			.list();
	}

	/**
	 * Busca a unidade de medida que tem o s�mbolo passado por par�metro.
	 *
	 * @param unidade
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Unidademedida findBySimbolo(String unidade) {
		if(unidade == null || unidade.equals("")){
			throw new SinedException("Unidade de medida inv�lida.");
		}
		return query()
				.select("unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo")
				.where("upper(unidademedida.simbolo) like ?",unidade.toUpperCase())
				.unique();
	}

	/**
	 * M�todo que carrega a unidade de medida pelo material
	 * 
	 * @param material
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Unidademedida findByMaterial(Material material) {
		if(material == null || material.equals("")){
			throw new SinedException("Par�metro inv�lidos.");
		}
		return query()
			.select("unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo")
			.where("unidademedida.id = (select material.unidademedida.id from Material material where material = ?)", material)
			.unique();
	}

	/**
	 * Retorna as unidades e unidades relacionadas 
	 * que coincidem com a unidade passada como par�metro.
	 * @param unidademedida
	 * @return
	 * @author Taidson
	 * @since 06/01/2011
	 */
	public List<Unidademedida> unidadeAndUnidadeRelacionada(Unidademedida unidademedida){
		return query()
		.select("unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, " +
				"unidademedidarelacionada.cdunidademedida, unidademedidarelacionada.nome, unidademedidarelacionada.simbolo, listaUnidademedidaconversao.fracao, " +
				"relacaounidademedida.cdunidademedida, relacaounidademedida.nome, relacaounidademedida.simbolo, " +
				"listaUnidademedidaconversao.qtdereferencia")
		.leftOuterJoin("unidademedida.listaUnidademedidaconversao listaUnidademedidaconversao")
		.leftOuterJoin("listaUnidademedidaconversao.unidademedidarelacionada unidademedidarelacionada")
		.leftOuterJoin("listaUnidademedidaconversao.unidademedida relacaounidademedida")
		.where("unidademedida.ativo = ?", Boolean.TRUE)
		.openParentheses()
		.where("unidademedida = ?", unidademedida)
		.or()
		.where("unidademedidarelacionada = ?", unidademedida)
		.closeParentheses()
		.list();
	}

	/**
	 * Busca as unidades de medida para o registro 0190 do SPED.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Unidademedida> findForSpedReg0190(String whereIn) {
		return query()
					.select("unidademedida.cdunidademedida, unidademedida.simbolo, unidademedida.nome")
					.whereIn("unidademedida.cdunidademedida", whereIn)
					.list();
	}	
	
	/**
	 * M�todo que busca as unidades de medida para o Registro0190 do SPED PIS/COFINS
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Unidademedida> findForSpedPiscofinsReg0190(String whereIn) {
		return query()
					.select("unidademedida.cdunidademedida, unidademedida.simbolo, unidademedida.nome")
					.whereIn("unidademedida.cdunidademedida", whereIn)
					.list();
	}
	
	public Unidademedida findForConverter(Unidademedida unidademedida) {
		if(unidademedida == null || unidademedida.getCdunidademedida() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
			.select("unidademedida.cdunidademedida, listaUnidademedidaconversao.cdunidademedidaconversao, unidademedida.casasdecimaisestoque, " +
					"unidademedidarelacionada.cdunidademedida, listaUnidademedidaconversao.fracao, listaUnidademedidaconversao.qtdereferencia," +
					"unidademedidarelacionada.casasdecimaisestoque ")
			.leftOuterJoin("unidademedida.listaUnidademedidaconversao listaUnidademedidaconversao")
			.leftOuterJoin("listaUnidademedidaconversao.unidademedidarelacionada unidademedidarelacionada")
			.where("unidademedida = ?", unidademedida)
			.unique();
	}

	public List<Unidademedida> findForPVOffline() {
		return query()
		.select("unidademedida.cdunidademedida, unidademedida.nome, unidademedida.ativo, unidademedida.simbolo, " +
				"listaUnidademedidaconversao.cdunidademedidaconversao," +
				"listaUnidademedidaconversao.fracao, listaUnidademedidaconversao.qtdereferencia, " +
				"unidademedidarelacionada.cdunidademedida, unidademedidarelacionada.nome, unidademedidarelacionada.ativo")
		.leftOuterJoin("unidademedida.listaUnidademedidaconversao listaUnidademedidaconversao")
		.leftOuterJoin("listaUnidademedidaconversao.unidademedidarelacionada unidademedidarelacionada")
		.list();
	}
	
	public List<Unidademedida> findForAndroid(String whereIn) {
		return query()
		.select("unidademedida.cdunidademedida, unidademedida.nome, unidademedida.ativo, unidademedida.simbolo, " +
				"listaUnidademedidaconversao.cdunidademedidaconversao," +
				"listaUnidademedidaconversao.fracao, listaUnidademedidaconversao.qtdereferencia, " +
				"unidademedidarelacionada.cdunidademedida, unidademedidarelacionada.nome, unidademedidarelacionada.ativo")
		.leftOuterJoin("unidademedida.listaUnidademedidaconversao listaUnidademedidaconversao")
		.leftOuterJoin("listaUnidademedidaconversao.unidademedidarelacionada unidademedidarelacionada")
		.whereIn("unidademedida.cdunidademedida", whereIn)
		.list();
	}
	
	/**
	 * M�todo que busca as unidades de medida
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Unidademedida> findForImportarproducao() {
		return query()
				.select("unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo")
				.where("unidademedida.simbolo is not null")
				.list();
	}
	
	/**
	 * M�todo que carrega a unidade de medida pela tarefa
	 * 
	 * @param tarefa
	 * @return
	 * @author Lucas Costa
	 */
	public Unidademedida findByTarefa(Tarefa tarefa) {
		if(tarefa == null || tarefa.getCdtarefa() == null){
			throw new SinedException("Par�metro inv�lidos.");
		}
		return query()
			.select("unidademedida.cdunidademedida, unidademedida.nome")
			.where("unidademedida.id = (select tarefa.unidademedida.id from Tarefa tarefa where tarefa = ?)", tarefa)
			.unique();
	}
	
	/**
	 * M�todo que busca as unidades de medidas a serem enviadas para o W3Producao
	 * @param whereIn
	 * @return
	 */
	public List<Unidademedida> findForW3Producao(String whereIn){
		return query()
				.select("unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, unidademedida.ativo")
				.whereIn("unidademedida.cdunidademedida", whereIn)
				.list();
	}
	
	/**
	* M�todo que retorna a quantidade na unidade principal do material
	*
	* @param material
	* @param unidademedida
	* @param qtde
	* @return
	* @since 24/10/2016
	* @author Luiz Fernando
	*/
	public Double getQtdeUnidadePrincipal(Material material, Unidademedida unidademedida, Double qtde){
		String sql = "SELECT qtde_unidadeprincipal(" + material.getCdmaterial() + "," + unidademedida.getCdunidademedida() + "," + qtde + ") as qtde ";

		SinedUtil.markAsReader();
		Double qtdeUnidadePrincipal = (Double) getJdbcTemplate().queryForObject(sql.toString(), 
		new RowMapper() {
			public Double mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getDouble("qtde");
			}
		});
		
		return qtdeUnidadePrincipal;
	}

	/**
	* M�todo que busca as unidades de medida para a integra��o SAGE
	*
	* @param listaSimboloUnidademedida
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public List<Unidademedida> findForSagefiscal(List<String> listaSimboloUnidademedida) {
		QueryBuilder<Unidademedida> query = query().select("unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo");
		query.openParentheses();
		for (String simbolo : listaSimboloUnidademedida) {
			query.where("unidademedida.simbolo = ?", simbolo).or();
		}
		query.closeParentheses();
		
		return query.list();
	}

	@Override
	public ListagemResult<Unidademedida> findForExportacao(FiltroListagem filtro) {
		QueryBuilder<Unidademedida> query = query();
		updateListagemQuery(query, filtro);
		query.orderBy("unidademedida.cdunidademedida");
		
		return new ListagemResult<Unidademedida>(query, filtro);
	}
}

