package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.view.Vwdocumentonotacontrato;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class VwdocumentonotacontratoDAO extends GenericDAO<Vwdocumentonotacontrato>{

	public List<Vwdocumentonotacontrato> listaDocumentonotacontrato (Contrato contrato){
		return query()
		.select("vwdocumentonotacontrato.cddocumento, vwdocumentonotacontrato.cdcontrato, vwdocumentonotacontrato.dtvencimento")
		.where("vwdocumentonotacontrato.cdcontrato = ?", contrato.getCdcontrato())
		.orderBy("vwdocumentonotacontrato.dtvencimento")
		.list();
	}
}
