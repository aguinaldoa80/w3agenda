package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Beneficiovalor;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class BeneficiovalorDAO extends GenericDAO<Beneficiovalor>{
	
	public List<Beneficiovalor> findByCdsbeneficio(String whereIn){
		return query()
				.join("beneficiovalor.beneficio beneficio")
				.whereIn("beneficio.cdbeneficio", whereIn)
				.list();
	}
}
