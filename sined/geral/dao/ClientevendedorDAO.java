package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clientevendedor;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ClientevendedorDAO extends GenericDAO<Clientevendedor>{

	/**
	 * M�todo para buscar os Colaboadores que est�o como clientevendedor - Autocomplete 
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<Colaborador> findClientevendedorAutocomplete(String q) {
		List<Colaborador> lista = new ArrayList<Colaborador>();
		String where;
		if (q != null && !q.equals("")) {
			if (q.indexOf('?') > 0) {
				throw new IllegalArgumentException("A cl�usula where n�o pode ter o caracter '?'. Deve ser passada apenas a express�o que se deseja fazer o like.");
			}
			String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
			if (funcaoTiraacento != null) {
				where = "UPPER(" + funcaoTiraacento + "(p.nome)) LIKE '%'||'" + Util.strings.tiraAcento(q).toUpperCase() + "'||'%'";
			}
			else {
				where = "UPPER(p.nome) LIKE '%'||'" + Util.strings.tiraAcento(q).toUpperCase() + "'||'%'";
			}
			
			if(where != null && !"".equals(where)){
				StringBuilder sql = new StringBuilder();
				sql
					.append("select p.cdpessoa as cdpessoa, p.nome as nome ")
					.append("from Clientevendedor cv ")
					.append("join pessoa p on p.cdpessoa = cv.cdcolaborador ")
					.append("where " + where)
					.append(" group by p.cdpessoa, p.nome ")
					.append(" order by p.nome ");
//				System.out.println("query: " + sql.toString());
				SinedUtil.markAsReader();
				lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						return new Colaborador(rs.getInt("cdpessoa"), rs.getString("nome"));
					}
				});				
			}			
		}
		
		
		return lista;
//		return query()
//				.select("clientevendedor.cdclientevendedor, colaborador.cdpessoa, colaborador.nome")
//				.join("clientevendedor.colaborador colaborador")
//				.whereLikeIgnoreAll("colaborador.nome", q)
//				.groupBy("")
//				.orderBy("colaborador.nome")				
//				.list();
	}
	
	/**
	 * Busca pelo cliente.
	 *
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/10/2012
	 */
	public List<Clientevendedor> findByCliente(Cliente cliente) {
		return querySined()
					.select("clientevendedor.cdclientevendedor, clientevendedor.principal, colaborador.cdpessoa, colaborador.nome, cliente.cdpessoa")
					.join("clientevendedor.colaborador colaborador")
					.join("clientevendedor.cliente cliente")
					.where("cliente = ?", cliente)
					.orderBy("colaborador.nome")				
					.list();
	}
	
	/**
	* M�todo que retorna o vendedor principal do cliente
	*
	* @param cliente
	* @return
	* @since 10/11/2016
	* @author Luiz Fernando
	*/
	public List<Clientevendedor> findVendedorPrincipalByCliente(Cliente cliente) {
		return querySined()
				.select("clientevendedor.cdclientevendedor, colaborador.cdpessoa, colaborador.nome, cliente.cdpessoa")
				.join("clientevendedor.colaborador colaborador")
				.join("clientevendedor.cliente cliente")
				.where("cliente = ?", cliente)
				.where("coalesce(clientevendedor.principal, false) = true ")
				.orderBy("colaborador.nome")	
				.list();
	}
	
	/**
	 * Busca pelo cliente Agencias e Colaboradores para o cadastro de Cliente.
	 *
	 * @param cliente
	 * @return
	 * @author Jo�o Vitor
	 * @since 05/02/2015
	 */
	public List<Clientevendedor> findByClienteForAgenciaColaborador(Cliente cliente) {
		return querySined()
					.select("clientevendedor.cdclientevendedor, clientevendedor.principal, clientevendedor.tiporesponsavel, colaborador.cdpessoa, colaborador.nome, cliente.cdpessoa, agencia.cdpessoa, agencia.nome")
					.leftOuterJoin("clientevendedor.colaborador colaborador")
					.leftOuterJoin("clientevendedor.cliente cliente")
					.leftOuterJoin("clientevendedor.agencia agencia")
					.where("cliente = ?", cliente)
					.list();
	}
	
	public List<Clientevendedor> findForAndroid(String whereIn) {
		return querySined()
		.select("cliente.cdpessoa, colaborador.cdpessoa, clientevendedor.cdclientevendedor")
			.leftOuterJoin("clientevendedor.cliente cliente")
			.leftOuterJoin("clientevendedor.colaborador colaborador")
		.whereIn("clientevendedor.cdclientevendedor", whereIn)
		.list();
	}

	public List<Clientevendedor> findForW3Producoa(String whereIn, Boolean principal) {
		QueryBuilder<Clientevendedor> query = querySined()
				.select("cliente.cdpessoa, colaborador.cdpessoa,colaborador.nome, clientevendedor.cdclientevendedor")
					.leftOuterJoin("clientevendedor.cliente cliente")
					.leftOuterJoin("clientevendedor.colaborador colaborador")
				.where("clientevendedor.principal = ?", principal);
		
		if(StringUtils.isNotBlank(whereIn)){
			SinedUtil.quebraWhereIn("clientevendedor.cdclientevendedor", whereIn, query);
		}
		
		return query.list();
	}
	
	/**
	 * Busca pelo cliente e colaborador.
	 *
	 * @param cliente
	 * @param vendedor
	 * @return
	 * @author Mairon Cezar
	 * @since 14/09/2020
	 */
	public Clientevendedor findByClienteAndColaborador(Cliente cliente, Colaborador vendedor) {
		return querySined()
					.select("clientevendedor.cdclientevendedor, clientevendedor.principal, clientevendedor.tiporesponsavel, colaborador.cdpessoa, colaborador.nome, cliente.cdpessoa")
					.leftOuterJoin("clientevendedor.colaborador colaborador")
					.leftOuterJoin("clientevendedor.cliente cliente")
					.leftOuterJoin("clientevendedor.agencia agencia")
					.where("cliente = ?", cliente)
					.where("colaborador = ?", vendedor)
					.setMaxResults(1)
					.unique();
	}
}
