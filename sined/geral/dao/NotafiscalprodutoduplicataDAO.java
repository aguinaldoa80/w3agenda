package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoduplicata;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class NotafiscalprodutoduplicataDAO extends GenericDAO<Notafiscalprodutoduplicata> {

	public List<Notafiscalprodutoduplicata> findByNotafiscalproduto(Notafiscalproduto form) {
		if(form == null || form.getCdNota() == null){
			throw new SinedException("Nota fiscal de produto n�o pode ser nula.");
		}
		return query()
					.select("notafiscalprodutoduplicata.cdnotafiscalprodutoduplicata, notafiscalprodutoduplicata.numero, " +
							"notafiscalprodutoduplicata.dtvencimento, notafiscalprodutoduplicata.valor, notafiscalprodutoduplicata.autorizacao, " +
							"notafiscalprodutoduplicata.bandeira, notafiscalprodutoduplicata.adquirente, " +
							"documentotipo.cddocumentotipo, documentotipo.nome, documentotipo.meiopagamentonfe, documentotipo.tipointegracaonfe," +
							"documentotipo.cnpjcredenciadoranfe, documentotipo.bandeiracartaonfe ")
					.leftOuterJoin("notafiscalprodutoduplicata.documentotipo documentotipo")
					.where("notafiscalprodutoduplicata.notafiscalproduto = ?", form)
					.orderBy("notafiscalprodutoduplicata.dtvencimento")
					.list();
	}

	public void updateNumeroDuplicata(Notafiscalprodutoduplicata dup, String numero) {
		if(dup != null && dup.getCdnotafiscalprodutoduplicata() != null && numero != null && !"".equals(numero)){
			getJdbcTemplate().execute("UPDATE NOTAFISCALPRODUTODUPLICATA SET NUMERO = '" + numero + "' WHERE CDNOTAFISCALPRODUTODUPLICATA = " + dup.getCdnotafiscalprodutoduplicata());
		}
	}
	
	public Boolean existeDuplicata(Notafiscalproduto notafiscalproduto) {
		if(notafiscalproduto == null || notafiscalproduto.getCdNota() == null){
			throw new SinedException("Nota fiscal de produto n�o pode ser nula.");
		}
		
		return newQueryBuilderSined(Long.class)
			.select("count(*)")
			.from(Notafiscalprodutoduplicata.class)
			.where("notafiscalprodutoduplicata.notafiscalproduto = ?", notafiscalproduto)
			.unique() > 0;
	}

	public List<Notafiscalprodutoduplicata> findByNotaForIntegracaoTEF(String whereInNota) {
		if(StringUtils.isBlank(whereInNota)){
			throw new SinedException("N�o foi encontrada nenhuma nota.");
		}
		return querySined()
					
					.select("notafiscalprodutoduplicata.cdnotafiscalprodutoduplicata, notafiscalprodutoduplicata.numero, " +
							"notafiscalprodutoduplicata.dtvencimento, notafiscalprodutoduplicata.valor, notafiscalprodutoduplicata.autorizacao, " +
							"notafiscalprodutoduplicata.bandeira, notafiscalprodutoduplicata.adquirente, " +
							"documentotipo.cddocumentotipo, documentotipo.nome, documentotipo.meiopagamentonfe, documentotipo.tipointegracaonfe," +
							"documentotipo.cnpjcredenciadoranfe, documentotipo.bandeiracartaonfe, " +
							"notafiscalproduto.cdNota, notafiscalproduto.numero, notafiscalproduto.dtEmissao, " +
							"cliente.cdpessoa, cliente.nome")
					.leftOuterJoin("notafiscalprodutoduplicata.documentotipo documentotipo")
					.leftOuterJoin("notafiscalprodutoduplicata.notafiscalproduto notafiscalproduto")
					.leftOuterJoin("notafiscalproduto.cliente cliente")
					.whereIn("notafiscalproduto.cdNota", whereInNota)
					.orderBy("notafiscalproduto.cdNota, notafiscalprodutoduplicata.numero")
					.list();
	}

	public void updateInfoCartao(Notafiscalprodutoduplicata notafiscalprodutoduplicata,	String autorizacao, String bandeira, String adquirente) {
		if(notafiscalprodutoduplicata == null || notafiscalprodutoduplicata.getCdnotafiscalprodutoduplicata() == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		getHibernateTemplate().bulkUpdate("update Notafiscalprodutoduplicata n set n.autorizacao = ?, n.bandeira = ?, n.adquirente = ? where n = ?", new Object[]{
			autorizacao, bandeira, adquirente, notafiscalprodutoduplicata
		});
	}
	
}

