package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Veiculotipo;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculotipomarcacorcombustivelFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("veiculotipo.descricao")
public class VeiculotipoDAO extends GenericDAO<Veiculotipo> {

	@Override
	public void updateListagemQuery(QueryBuilder<Veiculotipo> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inválidos");
		}
		VeiculotipomarcacorcombustivelFiltro filtro = (VeiculotipomarcacorcombustivelFiltro) _filtro;
		query
			.select("veiculotipo.cdveiculotipo, veiculotipo.descricao, veiculotipo.implemento")
			.whereLikeIgnoreAll("veiculotipo.descricao", filtro.getDescricao());
	}
	
	
}
