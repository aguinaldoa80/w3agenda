package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel.TipoComprovante;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ComprovanteConfiguravelFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ComprovanteConfiguravelDAO extends GenericDAO<ComprovanteConfiguravel> {

	@Override
	public void updateListagemQuery(QueryBuilder<ComprovanteConfiguravel> query, FiltroListagem _filtro) {
		ComprovanteConfiguravelFiltro filtro = (ComprovanteConfiguravelFiltro) _filtro;
		query
			.whereLikeIgnoreAll("descricao", filtro.getDescricao())
			.where("pedidoVenda = ?", filtro.getPedidoVenda())
			.where("venda = ?", filtro.getVenda())
			.where("orcamento = ?", filtro.getOrcamento())
			.where("ativo = ?", filtro.getAtivo())
			.where("txt = ?", filtro.getTxt());
		super.updateListagemQuery(query, filtro);
	}
	
	public List<ComprovanteConfiguravel> findAtivosByPedidovenda() {
		return query()
		.where("comprovanteConfiguravel.pedidoVenda = ?", Boolean.TRUE)
		.where("comprovanteConfiguravel.ativo = ?", Boolean.TRUE)
		.orderBy("comprovanteConfiguravel.descricao asc")
		.list();
	}

	public List<ComprovanteConfiguravel> findAtivosByVenda() {
		return query()
		.where("comprovanteConfiguravel.venda = ?", Boolean.TRUE)
		.where("comprovanteConfiguravel.ativo = ?", Boolean.TRUE)
		.orderBy("comprovanteConfiguravel.descricao asc")
		.list();
	}
	
	public List<ComprovanteConfiguravel> findAtivosByOrcamento() {
		return query()
		.where("comprovanteConfiguravel.orcamento = ?", Boolean.TRUE)
		.where("comprovanteConfiguravel.ativo = ?", Boolean.TRUE)
		.orderBy("comprovanteConfiguravel.descricao asc")
		.list();
	}
	
	public List<ComprovanteConfiguravel> findAtivosByColeta() {
		return query()
		.where("comprovanteConfiguravel.coleta = ?", Boolean.TRUE)
		.where("comprovanteConfiguravel.ativo = ?", Boolean.TRUE)
		.orderBy("comprovanteConfiguravel.descricao asc")
		.list();
	}

	/**
	* M�todo que retorna os comprovantes de venda que n�o est�o vinculados a empresa da venda
	*
	* @param whereIn
	* @param tipoComprovante
	* @return
	* @since 31/01/2017
	* @author Luiz Fernando
	*/
	public List<ComprovanteConfiguravel> findComprovanteAlternativo(String whereIn, TipoComprovante tipoComprovante) {
		QueryBuilder<ComprovanteConfiguravel> query = query();
		
		query.where("comprovanteConfiguravel.ativo = true");
		
		if(TipoComprovante.PEDIDO_VENDA.equals(tipoComprovante)){
			query.
				where("comprovanteConfiguravel.pedidoVenda = true")
				.where("comprovanteConfiguravel.cdcomprovanteconfiguravel not in ( select c.cdcomprovanteconfiguravel " +
																				 " from Pedidovenda p " +
																				 " join p.empresa e " +
																				 " join e.comprovanteConfiguravelPedidoVenda c " +
																				 " where p.cdpedidovenda in (" + whereIn + "))");
		}else if(TipoComprovante.ORCAMENTO.equals(tipoComprovante)){
			query.
			where("comprovanteConfiguravel.orcamento = true")
			.where("comprovanteConfiguravel.cdcomprovanteconfiguravel not in ( select c.cdcomprovanteconfiguravel " +
																			 " from Vendaorcamento v " +
																			 " join v.empresa e " +
																			 " join e.comprovanteConfiguravelOrcamento c " +
																			 " where v.cdvendaorcamento in (" + whereIn + "))");
		}else if(TipoComprovante.VENDA.equals(tipoComprovante)){
			query.
			where("comprovanteConfiguravel.venda = true")
			.where("comprovanteConfiguravel.cdcomprovanteconfiguravel not in ( select c.cdcomprovanteconfiguravel " +
																			 " from Venda v " +
																			 " join v.empresa e " +
																			 " join e.comprovanteConfiguravelVenda c " +
																			 " where v.cdvenda in (" + whereIn + "))");
		}
		
		return query
			.orderBy("comprovanteConfiguravel.descricao asc")
			.list();
	}
	
	@Override
	public ListagemResult<ComprovanteConfiguravel> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<ComprovanteConfiguravel> query = query();
		updateListagemQuery(query, filtro);
		
		return new ListagemResult<ComprovanteConfiguravel>(query, filtro);
	}
}
