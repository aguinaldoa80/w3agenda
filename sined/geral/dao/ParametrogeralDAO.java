package br.com.linkcom.sined.geral.dao;

import java.util.Arrays;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ParametrogeralFiltro;
import br.com.linkcom.sined.util.ParametroCacheUtil;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;


public class ParametrogeralDAO extends GenericDAO<Parametrogeral> {

	@Override
	public void updateListagemQuery(QueryBuilder<Parametrogeral> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		ParametrogeralFiltro filtro = (ParametrogeralFiltro) _filtro;
		query
			.whereLikeIgnoreAll("parametrogeral.nome", filtro.getNome())
			.whereLikeIgnoreAll("parametrogeral.valor", filtro.getValor())								
			.orderBy("parametrogeral.nome");
		
//		if(!ParametrogeralService.getInstance().isUsuarioAdmin())
//			query		
//				.where("coalesce (parametrogeral.parametrointerno, false) = false");
	}
	
	@Override
	public void saveOrUpdate(Parametrogeral bean) {
		if(bean.getNome() != null){
			ParametroCacheUtil.remove(bean.getNome());
		}
		super.saveOrUpdate(bean);
	}
	
	@Override
	public void saveOrUpdateNoUseTransaction(Parametrogeral bean) {
		if(bean.getNome() != null){
			ParametroCacheUtil.remove(bean.getNome());
		}
		super.saveOrUpdateNoUseTransaction(bean);
	}
	
	@Override
	public void saveOrUpdateNoUseTransactionWithoutLog(Parametrogeral bean) {
		if(bean.getNome() != null){
			ParametroCacheUtil.remove(bean.getNome());
		}
		super.saveOrUpdateNoUseTransactionWithoutLog(bean);
	}
	
	@Override
	public void saveOrUpdateWithoutLog(Parametrogeral bean) {
		if(bean.getNome() != null){
			ParametroCacheUtil.remove(bean.getNome());
		}
		super.saveOrUpdateWithoutLog(bean);
	}
	
	/**
	 * <p>Fornece o valor de um Par�metro Geral do Sistema a partir de seu nome</p>
	 * 
	 * @param nome
	 * @return String valor do Par�metro Geral
	 * @author Hugo Ferreira
	 */
	public String getValorPorNome(String nome) {
		if (nome == null) {
			throw new SinedException("Par�metro nome n�o pode ser null.");
		}
		
		String valor = ParametroCacheUtil.get(nome);
		if(valor==null){
			SinedUtil.markAsReader();
			valor = newQueryBuilderWithFrom(String.class)
						.select("parametrogeral.valor")
						.where("parametrogeral.nome = ?", nome)
						.setUseTranslator(false)
						.unique();
			ParametroCacheUtil.put(nome, valor);
		}
		if(valor==null && parametroGeralStringVazia(nome) ){
			return "";
		}else if (valor == null) {
			throw new SinedException("Par�metro Geral \"" + nome + "\" n�o encontrado.");
		}
		
		return valor;
	}
	private boolean parametroGeralStringVazia(String nome){
		if(Parametrogeral.EXPEDICAO_RECAPAGEM.equals(nome)){
			return true;
		}
		return false;
	}
	
	/* singleton */
	private static ParametrogeralDAO instance;
	public static ParametrogeralDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(ParametrogeralDAO.class);
		}
		return instance;
	}

	public Parametrogeral findByNome(String nome) {
		if (nome == null || nome.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		List<Parametrogeral> lista = querySined()
					
					.select("parametrogeral.cdparametrogeral, parametrogeral.nome, parametrogeral.valor")
					.where("parametrogeral.nome = ?",nome)
					.list();
		return lista != null && lista.size() > 0 ? lista.get(0) : null;
	}

	public void updateValorPorNome(String nome, String valor) {
		ParametroCacheUtil.remove(nome);
		getHibernateTemplate().bulkUpdate("update Parametrogeral p set p.valor = ? where p.nome = ?", new Object[]{valor, nome});
	}

	/**
	 * Recupera um par�metro pelo nome (M�todo criado para fazer a consulta sem exigir o contexto de request)
	 * 
	 * @author Giovane Freitas <giovane.freitas@linkcom.com.br>
	 * @param string
	 * @return
	 */
	public Parametrogeral findForArquivoGerencial(String nome) {
		if (nome == null || nome.equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		SinedUtil.markAsReader();
		List<Parametrogeral> lista = query()
					.select("parametrogeral.cdparametrogeral, parametrogeral.nome, parametrogeral.valor")
					.where("parametrogeral.nome = ?",nome)
					.list();
		return lista != null && lista.size() > 0 ? lista.get(0) : null;
	}
	
	/**
	 * M�todo que busca o valor do par�metro de acordo com o nome, 
	 * mas se o valor for null o m�todo retorno o null
	 * 
	 *
	 * @param nome
	 * @return
	 * @author Luiz Fernando
	 */
	public String buscaValorPorNome(String nome) {
		if (nome == null) {
			throw new SinedException("Par�metro nome n�o pode ser null.");
		}
		
		String valor = ParametroCacheUtil.get(nome);
		if(valor==null){
			SinedUtil.markAsReader();
			valor = newQueryBuilderWithFrom(String.class)
						.select("parametrogeral.valor")
						.where("parametrogeral.nome = ?", nome)
						.setUseTranslator(false)
						.unique();
			ParametroCacheUtil.put(nome, valor);
		}
		return valor;
	}

	public List<Parametrogeral> findForAndroid(String... nomes) {
		return querySined()
					
					.select("parametrogeral.cdparametrogeral, parametrogeral.nome, parametrogeral.valor")
					.whereIn("parametrogeral.nome", SinedUtil.montaWhereInString(CollectionsUtil.concatenate(Arrays.asList(nomes), ",")))
					.list();
	}
	@Override
	public void updateEntradaQuery(QueryBuilder<Parametrogeral> query) {
	
		query.select("parametrogeral.cdparametrogeral,parametrogeral.nome,parametrogeral.valor, parametrogeral.objetivo, " +
				"parametrogeral.cdusuarioaltera,parametrogeral.dtaltera,parametrogeral.parametrointerno," +
				"listaparametrogeralhistorico.cdparametrogeralhistorico,listaparametrogeralhistorico.dataaltera," +
				"listaparametrogeralhistorico.acaoexecutada,listaparametrogeralhistorico.responsavel,listaparametrogeralhistorico.observacao")
				.leftOuterJoin("parametrogeral.listaparametrogeralhistorico listaparametrogeralhistorico")
				.orderBy("listaparametrogeralhistorico.dataaltera desc");
	}
	/**
	 * M�todo retorna parametro interno para validacao
	 * @param parametrogeral
	 * @author C�sar
	 */
	public Parametrogeral findForParametroInterno(Parametrogeral parametrogeral){
		return querySined()
				
				.select("parametrogeral.cdparametrogeral,parametrogeral.nome,parametrogeral.parametrointerno")
				.where("parametrogeral = ?",parametrogeral)
				.unique();
	}

	/**
	 * Retorna o nome do banco de dados.
	 *
	 * @return
	 * @since 02/04/2019
	 * @author Rodrigo Freitas
	 */
	public String getNomeBancoDeDados() {
		return (String) getJdbcTemplate().queryForObject("SELECT current_database()", String.class);
	}

	@Override
	public ListagemResult<Parametrogeral> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Parametrogeral> query = query();
		updateListagemQuery(query, filtro);
		
		return new ListagemResult<Parametrogeral>(query, filtro);
	}
}
