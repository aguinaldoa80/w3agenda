package br.com.linkcom.sined.geral.dao;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Pais;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PaisFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PaisDAO extends GenericDAO<Pais>{

	@Override
	public void updateListagemQuery(QueryBuilder<Pais> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inválidos");
		}
		
		PaisFiltro filtro = (PaisFiltro) _filtro;
		query
			.select("pais.nome, pais.cdpais")
			.where("pais.nome = ?", filtro.getCdpais())
			.whereLikeIgnoreAll("pais.nome", filtro.getNome());
	}

	public Pais findByNome(String nome) {
		if(StringUtils.isBlank(nome)) return null;
		
		return query()
				.select("pais.cdpais, pais.nome")
				.whereLikeIgnoreAll("pais.nome", nome)
				.setMaxResults(1)
				.unique();
	}

}
