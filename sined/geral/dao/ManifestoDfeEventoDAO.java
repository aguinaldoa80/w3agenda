package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfeEvento;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.SituacaoEmissorEnum;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ManifestoDfeEventoDAO extends GenericDAO<ManifestoDfeEvento> {

	public List<ManifestoDfeEvento> procurarPorManifestoDfe(ManifestoDfe manifestoDfe) {
		if (manifestoDfe == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
			.select("manifestoDfeEvento.chaveAcesso, manifestoDfeEvento.nuProt, manifestoDfeEvento.tpEvento, manifestoDfeEvento.evento, manifestoDfeEvento.motivo, " +
				"manifestoDfeEvento.dhEvento, manifestoDfeEvento.dhRegEvento, manifestoDfeEvento.situacaoEnum, manifestoDfeEvento.coStat, " +
				"arquivoXml.cdarquivo, arquivoXml.nome, arquivoXml.tipoconteudo, " +
				"arquivoRetornoXml.cdarquivo, arquivoRetornoXml.nome, arquivoRetornoXml.tipoconteudo ")
			.leftOuterJoin("manifestoDfeEvento.arquivoXml arquivoXml")
			.leftOuterJoin("manifestoDfeEvento.arquivoRetornoXml arquivoRetornoXml")
			.where("manifestoDfeEvento.manifestoDfe = ?", manifestoDfe)
			.orderBy("manifestoDfeEvento.cdManifestoDfeEvento desc")
			.list();
	}

	public List<ManifestoDfeEvento> procurarManifestoDfeEventoSituacaoDiferenteDe(String selectedItens, SituacaoEmissorEnum situacaoEnum) {
		if (StringUtils.isEmpty(selectedItens) || situacaoEnum == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
			.select("manifestoDfeEvento.cdManifestoDfeEvento, manifestoDfeEvento.situacaoEnum ")
			.where("manifestoDfeEvento.situacaoEnum <> ?", situacaoEnum)
			.whereIn("manifestoDfeEvento.cdManifestoDfeEvento", selectedItens)
			.list();
	}
	
	public List<Empresa> procurarEmpresasDosManifestoDfeEvento(String selectedItens) {
		if (StringUtils.isEmpty(selectedItens)) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("select distinct e.cdpessoa ");
		sql.append("from manifestodfeevento me ");
		sql.append("join manifestodfe m on m.cdmanifestodfe = me.cdmanifestodfe ");
		sql.append("join loteconsultadfeitem li on li.cdloteconsultadfeitem = m.cdloteconsultadfeitem ");
		sql.append("join loteconsultadfe l on l.cdloteconsultadfe = li.cdloteconsultadfe ");
		sql.append("join empresa e on e.cdpessoa = l.cdempresa ");
		sql.append("where me.cdmanifestodfeevento in (" + selectedItens + ")");

		SinedUtil.markAsReader();
		@SuppressWarnings("unchecked")
		List<Empresa> listaEmpresa = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			
			@Override
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Empresa(rs.getInt("cdpessoa"));
			}
		});
		
		return listaEmpresa;
	}

	public ManifestoDfeEvento procurarPorId(Integer cdManifestoDfeEvento) {
		if (cdManifestoDfeEvento == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("manifestoDfeEvento.cdManifestoDfeEvento, manifestoDfeEvento.chaveAcesso, manifestoDfeEvento.cnpj, manifestoDfeEvento.cpf, manifestoDfeEvento.situacaoEnum, manifestoDfeEvento.tpEvento, " +
						"manifestoDfe.cdManifestoDfe, " +
						"loteConsultaDfeItem.cdLoteConsultaDfeItem, " +
						"empresa.cdpessoa, configuracaoNfe.cdconfiguracaonfe, " +
						"arquivoXml.cdarquivo ")
				.leftOuterJoin("manifestoDfeEvento.manifestoDfe manifestoDfe")
				.leftOuterJoin("manifestoDfeEvento.arquivoXml arquivoXml")
				.leftOuterJoin("manifestoDfe.loteConsultaDfeItem loteConsultaDfeItem")
				.leftOuterJoin("loteConsultaDfeItem.loteConsultaDfe loteConsultaDfe")
				.leftOuterJoin("loteConsultaDfe.empresa empresa")
				.leftOuterJoin("loteConsultaDfe.configuracaoNfe configuracaoNfe")
				.where("manifestoDfeEvento.cdManifestoDfeEvento = ?", cdManifestoDfeEvento)
				.unique();
	}

	public ManifestoDfeEvento procurarEmpresaPorId(Integer cdManifestoDfeEvento) {
		if (cdManifestoDfeEvento == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("empresa.cdpessoa, empresa.nome ")
				.leftOuterJoin("manifestoDfeEvento.manifestoDfe manifestoDfe")
				.leftOuterJoin("manifestoDfeEvento.arquivoManifestoXml arquivoManifestoXml")
				.leftOuterJoin("manifestoDfe.loteConsultaDfeItem loteConsultaDfeItem")
				.leftOuterJoin("loteConsultaDfeItem.loteConsultaDfe loteConsultaDfe")
				.leftOuterJoin("loteConsultaDfe.empresa empresa")
				.where("manifestoDfeEvento.cdManifestoDfeEvento = ?", cdManifestoDfeEvento)
				.unique();
	}

	public List<ManifestoDfeEvento> procurarManifestoDfeEventoParaConsulta(Empresa empresa) {
		if (empresa == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return querySined()
				
				.select("manifestoDfeEvento.cdManifestoDfeEvento, configuracaoNfe.cdconfiguracaonfe, arquivoXml.cdarquivo")
				.leftOuterJoin("manifestoDfeEvento.configuracaoNfe configuracaoNfe")
				.leftOuterJoin("manifestoDfeEvento.arquivoXml arquivoXml")
				.leftOuterJoin("manifestoDfeEvento.manifestoDfe manifestoDfe")
				.leftOuterJoin("manifestoDfe.loteConsultaDfeItem loteConsultaDfeItem")
				.leftOuterJoin("loteConsultaDfeItem.loteConsultaDfe loteConsultaDfe")
				.leftOuterJoin("loteConsultaDfe.empresa empresa")
				.where("empresa = ?", empresa)
				.where("manifestoDfeEvento.situacaoEnum = ?", SituacaoEmissorEnum.GERADO)
				.where("arquivoXml is not null")
				.openParentheses()
					.where("manifestoDfeEvento.enviado is null")
					.or()
					.where("manifestoDfeEvento.enviado = ?", Boolean.FALSE)
				.closeParentheses()
				.list();
	}

	public Integer marcarManifestoDfeEventoService(String whereInManifestoDfeEvento, String flag) {
		return getHibernateTemplate()
				.bulkUpdate("update ManifestoDfeEvento m set m." + flag + " = ? where m.cdManifestoDfeEvento in (" + whereInManifestoDfeEvento + ") and (m." + flag + " = ? or m." + flag + " is null)", 
							new Object[]{Boolean.TRUE, Boolean.FALSE});
	}

	public List<ManifestoDfeEvento> procurarPorIds(String cdManifestoDfeEvento) {
		if (StringUtils.isEmpty(cdManifestoDfeEvento)) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("manifestoDfeEvento.cdManifestoDfeEvento, manifestoDfeEvento.chaveAcesso, manifestoDfeEvento.cnpj, manifestoDfeEvento.cpf, " +
						"manifestoDfe.cdManifestoDfe, " +
						"loteConsultaDfeItem.cdLoteConsultaDfeItem, " +
						"empresa.cdpessoa, " +
						"arquivoXml.cdarquivo ")
				.leftOuterJoin("manifestoDfeEvento.manifestoDfe manifestoDfe")
				.leftOuterJoin("manifestoDfeEvento.arquivoXml arquivoXml")
				.leftOuterJoin("manifestoDfe.loteConsultaDfeItem loteConsultaDfeItem")
				.leftOuterJoin("loteConsultaDfeItem.loteConsultaDfe loteConsultaDfe")
				.leftOuterJoin("loteConsultaDfe.empresa empresa")
				.whereIn("manifestoDfeEvento.cdManifestoDfeEvento", cdManifestoDfeEvento)
				.list();
	}
}