package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Cemobra;
import br.com.linkcom.sined.geral.bean.Cemtipologia;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CemtipologiaDAO extends GenericDAO<Cemtipologia>{

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaCemcomponente");
		save.saveOrUpdateManaged("listaCemperfil");
		save.saveOrUpdateManaged("listaCemvidro");
		save.saveOrUpdateManaged("listaCempainel");
	}

	public List<Cemtipologia> findByCemobra(Cemobra cemobra) {
		return query()
					.join("cemtipologia.cemobra cemobra")
					.leftOuterJoinFetch("cemtipologia.listaCemcomponente listaCemcomponente")
					.leftOuterJoinFetch("cemtipologia.listaCemperfil listaCemperfil")
					.leftOuterJoinFetch("cemtipologia.listaCemvidro listaCemvidro")
					.leftOuterJoinFetch("cemtipologia.listaCempainel listaCempainel")
					.where("cemobra = ?", cemobra)
					.list();
	}
	
}
