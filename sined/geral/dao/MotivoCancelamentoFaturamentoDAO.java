package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.MotivoCancelamentoFaturamento;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MotivoCancelamentoFaturamentoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
public class MotivoCancelamentoFaturamentoDAO extends GenericDAO<MotivoCancelamentoFaturamento>{
	
	@Override
	public void updateListagemQuery(
			QueryBuilder<MotivoCancelamentoFaturamento> query,
			FiltroListagem _filtro) {
		MotivoCancelamentoFaturamentoFiltro filtro = (MotivoCancelamentoFaturamentoFiltro)_filtro;
		
		query.select("motivoCancelamentoFaturamento.cdmotivocancelamentofaturamento, " +
				"motivoCancelamentoFaturamento.descricao")
		.whereLikeIgnoreAll("motivoCancelamentoFaturamento.descricao", filtro.getDescricao());
	}

}
