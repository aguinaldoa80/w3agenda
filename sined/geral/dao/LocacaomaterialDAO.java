package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Locacaomaterial;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.LocacaomaterialFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class LocacaomaterialDAO extends GenericDAO<Locacaomaterial> {
	
	private MaterialcategoriaService materialcategoriaService;
	
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {this.materialcategoriaService = materialcategoriaService;}	
	
	@Override
	public void updateListagemQuery(QueryBuilder<Locacaomaterial> query, FiltroListagem _filtro) {
		LocacaomaterialFiltro filtro = (LocacaomaterialFiltro) _filtro;
		
		query
			.select("fornecedor.nome, bempatrimonio.nome, locacaomaterial.cdlocacaomaterial, locacaomaterial.valorlocacao, " +
					"locacaomaterial.dtinicio, locacaomaterial.dtfim, locacaomaterialtipo.nome")
			.join("locacaomaterial.fornecedor fornecedor")
			.join("locacaomaterial.bempatrimonio bempatrimonio")
			.join("locacaomaterial.locacaomaterialtipo locacaomaterialtipo")
			.orderBy("fornecedor.nome");	
		
//		CENTRALIZA��O DO C�DIGO REUTILIZ�VEL
		this.putWhere(query, filtro);
	}

	@Override
	public void updateEntradaQuery(QueryBuilder<Locacaomaterial> query) {
		query
			.select("locacaomaterial.cdlocacaomaterial, fornecedor.cdpessoa,fornecedor.nome, bempatrimonio.cdmaterial,bempatrimonio.nome, locacaomaterial.qtde, locacaomaterial.numerooc, " +
					"locacaomaterial.numerocontrato, locacaomaterial.numeropatrimonio, locacaomaterial.marca, locacaomaterial.valorlocacao, " +
					"locacaomaterialtipo.cdlocacaomaterialtipo, locacaomaterial.dtinicio, locacaomaterial.dtfim, locacaomaterial.valorequipamento, " +
					"locacaomaterial.indenizacao, locacaomaterial.descricaoindenizado, locacaomaterial.valorindenizacao, materialgrupo.cdmaterialgrupo,materialgrupo.nome, " +
					"materialtipo.cdmaterialtipo, locacaomaterial.cdusuarioaltera, locacaomaterial.dtaltera")
			.join("locacaomaterial.fornecedor fornecedor")
			.join("locacaomaterial.bempatrimonio bempatrimonio")
			.join("bempatrimonio.materialgrupo materialgrupo")
			.leftOuterJoin("bempatrimonio.materialtipo materialtipo")
			.join("locacaomaterial.locacaomaterialtipo locacaomaterialtipo");
	}
	
	/**
	 * M�todo que centraliza as condi��es da listagem e do relat�rio de loca��o de material
	 *
	 * @param query
	 * @param filtro
	 * @author Rodrigo Freitas
	 * @author Tom�s Rabelo
	 */
	private void putWhere(QueryBuilder<Locacaomaterial> query, LocacaomaterialFiltro filtro) {
		query
			.where("fornecedor = ?", filtro.getFornecedor())
			.where("bempatrimonio = ?", filtro.getBempatrimonio())
			.where("bempatrimonio.materialgrupo = ?", filtro.getMaterialgrupo())
			.where("bempatrimonio.materialtipo = ?", filtro.getMaterialtipo())
			.where("locacaomaterial.valorlocacao >= ?", filtro.getValorlocacao1())
			.where("locacaomaterial.valorlocacao <= ?", filtro.getValorlocacao2())
			.where("locacaomaterialtipo = ?", filtro.getLocacaomaterialtipo());
			
			if(filtro.getDtinicio() != null && filtro.getDtfim() != null){
				query
					.where("locacaomaterial.dtinicio <= ?",filtro.getDtfim())
					.where("locacaomaterial.dtfim >= ?",filtro.getDtinicio()).or()
					.where("locacaomaterial.dtfim >= ?",filtro.getDtinicio())
					.where("locacaomaterial.dtinicio is null").or()
					.where("locacaomaterial.dtinicio <= ?",filtro.getDtfim())
					.where("locacaomaterial.dtfim is null");
			} else if(filtro.getDtinicio() != null){
				query
					.where("locacaomaterial.dtfim >= ?",filtro.getDtinicio()).or()
					.where("locacaomaterial.dtfim is null");
			} else if(filtro.getDtfim() != null){
				query
					.where("locacaomaterial.dtinicio <= ?",filtro.getDtfim()).or()
					.where("locacaomaterial.dtinicio is null");
			}
			
			if(filtro.getMaterialcategoria() != null){
				materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
				query
					.leftOuterJoin("bempatrimonio.materialcategoria materialcategoria")
					.leftOuterJoin("materialcategoria.vmaterialcategoria vmaterialcategoria")
					.openParentheses()
						.where("vmaterialcategoria.identificador like ? ||'.%'", filtro.getMaterialcategoria().getIdentificador())
					.or()
						.where("vmaterialcategoria.identificador like ? ", filtro.getMaterialcategoria().getIdentificador())
					.closeParentheses();
			}
			
			boolean usuarioComRestricao = SinedUtil.getUsuarioLogado().getRestricaoFornecedorColaborador();
			Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
			if(usuarioComRestricao){
				query.leftOuterJoin("fornecedor.listaColaboradorFornecedor colaboradorFornecedor")
					.openParentheses()
						.where("colaboradorFornecedor.cdcolaboradorfornecedor is null")
						.or()
						.where("colaboradorFornecedor.colaborador = ?", colaborador)
					.closeParentheses();
			}
	}
	
	/**
	 * M�todo que carrega a lista de loca��es para o relat�rio de controle.
	 *
	 * @see br.com.linkcom.sined.geral.dao.LocacaomaterialDAO#putWhere
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Locacaomaterial> findForReport(LocacaomaterialFiltro filtro){
		if (filtro == null) {
			throw new SinedException("Filtro n�o pode ser nulo.");
		}
		QueryBuilder<Locacaomaterial> query = querySined()
					.select("locacaomaterial.cdlocacaomaterial, fornecedor.nome,  bempatrimonio.nome, locacaomaterial.qtde, locacaomaterial.numerooc, " +
					"locacaomaterial.numerocontrato, locacaomaterial.numeropatrimonio, locacaomaterial.marca, locacaomaterial.valorlocacao, " +
					"locacaomaterialtipo.nome, locacaomaterial.dtinicio, locacaomaterial.dtfim, locacaomaterial.valorequipamento, " +
					"locacaomaterial.indenizacao, locacaomaterial.descricaoindenizado, locacaomaterial.valorindenizacao")
					.join("locacaomaterial.fornecedor fornecedor")
					.join("locacaomaterial.bempatrimonio bempatrimonio")
					.join("locacaomaterial.locacaomaterialtipo locacaomaterialtipo");
		
//		CENTRALIZA��O DO C�DIGO REUTILIZ�VEL
		this.putWhere(query, filtro);
		
		return query.orderBy("fornecedor.nome").list();
	}

	public List<Locacaomaterial> findByVencida(Date data, Date dateToSearch) {
		return query()
				.where("locacaomaterial.dtfim < ?", data)
				.wherePeriodo("locacaomaterial.dtfim", dateToSearch, SinedDateUtils.currentDate())
				.list();
	}
	
	public List<Locacaomaterial> findForCsvReport(LocacaomaterialFiltro filtro) {
		QueryBuilder<Locacaomaterial> query = querySined();
		updateListagemQuery(query, filtro);
		return query.list();
	}
	
}
