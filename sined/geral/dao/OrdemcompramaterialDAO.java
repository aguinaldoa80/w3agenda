package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialfornecedor;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class OrdemcompramaterialDAO extends GenericDAO<Ordemcompramaterial> {

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaSolicitacaocompraordemcompramaterial");
	}
	
	/**
	 * Busca a lista de material de uma ordem de compra.
	 *
	 * @param o
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Ordemcompramaterial> findByOrdemcompra(Ordemcompra o) {
		if (o == null || o.getCdordemcompra() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return queryFindByOrdemcompra()
					.where("ordemcompra = ?",o)
					.list();
	}
	
	/**
	 * Busca a lista de material de v�rias ordem de compra.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 22/11/2012
	 */
	public List<Ordemcompramaterial> findByOrdemcompraWherein(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return queryFindByOrdemcompra()
					.whereIn("ordemcompra.cdordemcompra", whereIn)
					.list();
	}

	/**
	 * Query centralizada para os m�todos de findByOrdemcompra.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 22/11/2012
	 */
	private QueryBuilder<Ordemcompramaterial> queryFindByOrdemcompra() {
		return query()
					.select("ordemcompramaterial.cdordemcompramaterial, ordemcompramaterial.valor, ordemcompramaterial.icmsissincluso, ordemcompramaterial.qtdpedida, " +
							"ordemcompramaterial.ipiincluso, ordemcompramaterial.ipi, ordemcompramaterial.icmsiss, material.nome, material.cdmaterial, material.identificacao, " +
							"materialgrupo.cdmaterialgrupo, materialgrupo.nome, arquivo.cdarquivo, unidademedida.cdunidademedida, unidademedida.nome, " +
							"unidademedida.simbolo, projeto.cdprojeto, projeto.nome, ordemcompra.cdordemcompra, ordemcompramaterial.qtdefrequencia, " +
							"localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, ordemcompramaterial.observacao, ordemcompramaterial.valoroutrasdespesas, " +
							"projetoIt.cdprojeto, centrocustoIt.cdcentrocusto, materialmestregrade.cdmaterial, materialmestregrade.nome, materialmestregrade.identificacao," +
							"unidademedidaMaterial.cdunidademedida ")
					.leftOuterJoin("ordemcompramaterial.material material")
					.leftOuterJoin("material.materialmestregrade materialmestregrade")
					.leftOuterJoin("ordemcompramaterial.localarmazenagem localarmazenagem")
					.leftOuterJoin("ordemcompramaterial.unidademedida unidademedida")
					.leftOuterJoin("material.unidademedida unidademedidaMaterial")
					.leftOuterJoin("ordemcompramaterial.projeto projetoIt")
					.leftOuterJoin("ordemcompramaterial.centrocusto centrocustoIt")
					.leftOuterJoin("material.materialgrupo materialgrupo")
					.leftOuterJoin("materialgrupo.arquivo arquivo")
					.leftOuterJoin("ordemcompramaterial.ordemcompra ordemcompra")
					.leftOuterJoin("ordemcompra.rateio rateio")
					.leftOuterJoin("rateio.listaRateioitem listaRateioitem")
					.leftOuterJoin("listaRateioitem.projeto projeto");
	}

	/**
	 * M�todo que busca a unidade de medida da ordem de compra
	 *
	 * @param ordemcompra
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Ordemcompramaterial findUnidademedidaMaterial(Ordemcompra ordemcompra, Material material) {
		if(ordemcompra == null || ordemcompra.getCdordemcompra() == null || material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
				.select("ordemcompramaterial.cdordemcompramaterial, unidademedida.cdunidademedida, unidademedida.nome")
				.join("ordemcompramaterial.ordemcompra ordemcompra")
				.join("ordemcompramaterial.unidademedida unidademedida")
				.join("ordemcompramaterial.material material")
				.where("ordemcompra = ?", ordemcompra)
				.where("material = ?", material)
				.unique();
	}
	
	/**
	 * M�todo que busca o �ltimo valor de compra do material
	 *
	 * @param material
	 * @param fornecedor
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Ordemcompramaterial> findByFornecedorMaterial(Material material, Fornecedor fornecedor) {
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		if (fornecedor == null || fornecedor.getCdpessoa() == null) {
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		}
		return query()
					.select("ordemcompramaterial.qtdpedida, ordemcompramaterial.valor, ordemcompramaterial.qtdefrequencia, " +
							"ordemcompra.dtcriacao")
					.setMaxResults(1)
					.join("ordemcompramaterial.ordemcompra ordemcompra")
					.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
					.join("ordemcompramaterial.material material")
					.join("ordemcompra.fornecedor fornecedor")
					.where("material = ?", material)
					.where("fornecedor = ?", fornecedor)
					.where("ordemcompramaterial.qtdpedida is not null")
					.where("ordemcompramaterial.valor is not null")
					.where("ordemcompramaterial.qtdefrequencia is not null")
					.where("aux_ordemcompra.situacaosuprimentos not in ("+ Situacaosuprimentos.CANCELADA.getCodigo() + "," + Situacaosuprimentos.EM_ABERTO.getCodigo() + ")")
					.orderBy("ordemcompra.dtcriacao desc, ordemcompramaterial.cdordemcompramaterial desc")
					.list();
	}
	
	/**
	 * M�todo que busca os dados de projeto, centro de custo e conta gerencial do item da ordem de compra
	 *
	 * @param ordemcompramaterial
	 * @return
	 * @author Luiz Fernando
	 */
	public Ordemcompramaterial findForCalcularrateio(Ordemcompramaterial ordemcompramaterial) {
		if (ordemcompramaterial == null || ordemcompramaterial.getCdordemcompramaterial() == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
					.select("ordemcompramaterial.cdordemcompramaterial, contagerencial.cdcontagerencial, centrocusto.cdcentrocusto," +
							"projeto.cdprojeto, material.cdmaterial ")
					.leftOuterJoin("ordemcompramaterial.material material")
					.leftOuterJoin("ordemcompramaterial.contagerencial contagerencial")
					.leftOuterJoin("ordemcompramaterial.centrocusto centrocusto")
					.leftOuterJoin("ordemcompramaterial.projeto projeto")
					.where("ordemcompramaterial = ?", ordemcompramaterial)
					.unique();
	}	
	
	/**
	 * M�todo que busca o �ltimo valor de compra do material
	 *
	 * @param material
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 * @param empresa 
	 */
	public List<Ordemcompramaterial> findForUltimovalorcompraByMaterial(Material material, Empresa empresa) {
		if (material == null || material.getCdmaterial() == null) {
			throw new SinedException("Material n�o pode ser nulo.");
		}
		return query()
					.select("ordemcompramaterial.qtdpedida, ordemcompramaterial.valor, ordemcompramaterial.qtdefrequencia ")
					.setMaxResults(1)
					.join("ordemcompramaterial.ordemcompra ordemcompra")
					.join("ordemcompramaterial.material material")
					.leftOuterJoin("ordemcompra.empresa empresa")					
					.where("material = ?", material)
					.where("empresa = ?", empresa)
					.where("ordemcompramaterial.qtdpedida is not null")
					.where("ordemcompramaterial.valor is not null")
					.where("ordemcompramaterial.qtdefrequencia is not null")
					.orderBy("ordemcompramaterial.cdordemcompramaterial desc")
					.list();
	}

	/**
	 * Carrega as infos da ordem de compra para verificar se precisa de romaneio.
	 *
	 * @param ordemcompramaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/12/2012
	 */
	public Ordemcompramaterial loadByOrdemcompramaterialForRomaneio(Ordemcompramaterial ordemcompramaterial) {
		if (ordemcompramaterial == null || ordemcompramaterial.getCdordemcompramaterial() == null) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		return query()
					.select("ordemcompramaterial.cdordemcompramaterial, ordemcompramaterial.qtdpedida, material.cdmaterial, material.nome, material.identificacao, " +
							"localit.cdlocalarmazenagem, localit.nome, local.cdlocalarmazenagem, local.nome, ordemcompra.cdordemcompra")
					.leftOuterJoin("ordemcompramaterial.material material")
					.leftOuterJoin("ordemcompramaterial.localarmazenagem localit")
					.leftOuterJoin("ordemcompramaterial.ordemcompra ordemcompra")
					.leftOuterJoin("ordemcompra.localarmazenagem local")
					.entity(ordemcompramaterial)
					.unique();
	}

	/**
	 * Busca os itens da ordem de compra relacionados com a entrega passada por par�metro.
	 *
	 * @param entrega
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/12/2012
	 */
	public List<Ordemcompramaterial> findByEntrega(Entrega entrega) {
		return query()
					.select("ordemcompramaterial.cdordemcompramaterial, ordemcompramaterial.qtdpedida, material.cdmaterial")
					.join("ordemcompramaterial.material material")
					.join("ordemcompramaterial.listaOrdemcompraentregamaterial ordemcompraentregamaterial")
					.join("ordemcompraentregamaterial.entregamaterial entregamaterial")
					.join("entregamaterial.entregadocumento entregadocumento")
					.join("entregadocumento.entrega entrega")
					.where("entrega = ?", entrega)
					.list();
	}

	public List<Ordemcompramaterial> loadForConferenciaEntrega(String whereIn) {
		return query()
					.select("ordemcompramaterial.cdordemcompramaterial, ordemcompramaterial.qtdpedida, material.cdmaterial")
					.join("ordemcompramaterial.material material")
					.whereIn("ordemcompramaterial.cdordemcompramaterial", whereIn)
					.list();
	}
	
	public Ordemcompramaterial loadWithUnidademedida(Ordemcompramaterial ordemcompramaterial) {
		return query()
			.select("ordemcompramaterial.cdordemcompramaterial, unidademedida.cdunidademedida, unidademedida.nome")
			.join("ordemcompramaterial.ordemcompra ordemcompra")
			.join("ordemcompramaterial.unidademedida unidademedida")
			.where("ordemcompramaterial = ?", ordemcompramaterial)
			.unique();
	}
	
	/**
	* M�todo que carrega a lista de materiais da ordem de compra com o bean de ordem de compra
	*
	* @param whereIn
	* @return
	* @since 21/09/2015
	* @author Luiz Fernando
	*/
	public List<Ordemcompramaterial> findWithOrdemcompra(String whereIn) {
		return query()
			.select("ordemcompramaterial.cdordemcompramaterial, ordemcompra.cdordemcompra, unidademedida.cdunidademedida")
			.join("ordemcompramaterial.ordemcompra ordemcompra")
			.join("ordemcompramaterial.unidademedida unidademedida")
			.whereIn("ordemcompramaterial.cdordemcompramaterial", whereIn)
			.list();
	}
	
	/**
	* M�todo que busca �ltimas compras  
	*
	* @param cdmaterial
	* @param cdempresa
	* @param limite
	* @return
	* @since 05/04/2016
	* @author Luiz Fernando
	*/
	public List<Ordemcompramaterial> findUltimascomprasByMaterial(Integer cdmaterial, Integer cdempresa, Integer limite) {
		if(cdmaterial == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("ordemcompramaterial.qtdpedida, ordemcompramaterial.valor, ordemcompramaterial.qtdefrequencia, " +
						"ordemcompra.dtcriacao, fornecedor.cdpessoa, fornecedor.nome, " +
						"ordemcompra.cdordemcompra ")
				.join("ordemcompramaterial.material material")
				.join("ordemcompramaterial.ordemcompra ordemcompra")
				.join("ordemcompra.fornecedor fornecedor")
				.join("ordemcompra.aux_ordemcompra aux_ordemcompra")
				.where("material.cdmaterial = ?", cdmaterial)
				.where("ordemcompra.dtcriacao is not null")
				.where("ordemcompra.empresa.cdpessoa = ?", cdempresa)
				.setMaxResults(limite)
				.orderBy("ordemcompra.dtcriacao desc, ordemcompra.cdordemcompra")
				.where("aux_ordemcompra.situacaosuprimentos <> ?", Situacaosuprimentos.CANCELADA)
				.list();
	}
	
	/**
	* M�todo que retorna o pre�o m�ximo de compra. 
	* 'Pre�o m�ximo de compra': m�dia aritm�tica das compras dos �ltimos N meses ou o valor da �ltima compra que n�o estiver neste per�odo
	*
	* @param cdmaterial
	* @param cdempresa
	* @param qtdeMeses
	* @return
	* @since 05/04/2016
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public Double getPrecoMaximoCompra(String cdmaterial, String cdempresa,	Integer qtdeMeses) {
		Date dtinicio = SinedDateUtils.addMesData(new Date(System.currentTimeMillis()), -qtdeMeses); 
		
		String sql =	" select (COALESCE(ocm.valor, 0) / COALESCE(ocm.qtdpedida, 1) / COALESCE(ocm.qtdefrequencia, 1)) as valorunitario " +
						" from ordemcompramaterial ocm " +
						" join ordemcompra oc on oc.cdordemcompra = ocm.cdordemcompra " +
						" join aux_ordemcompra aux_oc on aux_oc.cdordemcompra = oc.cdordemcompra " +
						" where ocm.cdmaterial = " + cdmaterial +
						" and aux_oc.situacao <> " + Situacaosuprimentos.CANCELADA.getCodigo() + " " +
						" and oc.dtcriacao is not null " +
						(StringUtils.isNotBlank(cdempresa) ? " and oc.cdempresa = " + cdempresa + " " : "");

		String whereData = " and oc.dtcriacao >= '" + SinedDateUtils.toStringPostgre(dtinicio) + "'";

		SinedUtil.markAsReader();
		List<Double> lista = getJdbcTemplate().query(sql + whereData, 
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						return rs.getDouble("valorunitario");
					}
				});
		
		if(SinedUtil.isListEmpty(lista)){
			sql += " order by oc.dtcriacao desc, oc.cdordemcompra " +
				   " limit 1";
			SinedUtil.markAsReader();
			lista = getJdbcTemplate().query(sql, 
					new RowMapper() {
						public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
							return rs.getDouble("valorunitario");
						}
					});
		}
		
		Double precoMaximoCompra = null;
		if(SinedUtil.isListNotEmpty(lista)){
			Double total = 0d;
			for(Double valorunitario : lista) total += valorunitario;
			
			precoMaximoCompra = total / lista.size();
		}
		
		return precoMaximoCompra;
	}
	
	/**
	* M�todo que retorna a quantidade restante do item da ordem de compra
	*
	* @param ordemcompramaterial
	* @return
	* @since 25/10/2016
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public Double getQtdeByOrdemcompramaterial(Ordemcompramaterial ordemcompramaterial){
		
		if(ordemcompramaterial == null || ordemcompramaterial.getSolicitacaocompra() == null || ordemcompramaterial.getSolicitacaocompra().getCdsolicitacaocompra() == null){
			return 0.0;
		}
		
		String sql = 	"SELECT (qtde_unidadeprincipal(sc.cdmaterial, sc.cdunidademedida, sc.qtde)-SUM(qtde_unidadeprincipal(OM.cdmaterial, OM.cdunidademedida, OM.QTDPEDIDA)*OM.QTDEFREQUENCIA)) AS RESTO " +
						"FROM SOLICITACAOCOMPRAORDEMCOMPRAMATERIAL SCOCM " +
						"JOIN SOLICITACAOCOMPRA SC ON SC.CDSOLICITACAOCOMPRA = SCOCM.CDSOLICITACAOCOMPRA " +
						"JOIN ORDEMCOMPRAMATERIAL OM ON OM.CDORDEMCOMPRAMATERIAL = SCOCM.CDORDEMCOMPRAMATERIAL " +
						"JOIN ORDEMCOMPRA OC ON OC.CDORDEMCOMPRA = OM.CDORDEMCOMPRA " +
						"JOIN AUX_ORDEMCOMPRA AUX_OC ON AUX_OC.CDORDEMCOMPRA = OC.CDORDEMCOMPRA " +
						"WHERE AUX_OC.SITUACAO <> 9 " +
						"AND SC.CDSOLICITACAOCOMPRA = " + ordemcompramaterial.getSolicitacaocompra().getCdsolicitacaocompra() + " " +
						(ordemcompramaterial.getCdordemcompramaterial() != null ? "AND OM.CDORDEMCOMPRAMATERIAL <>" + ordemcompramaterial.getCdordemcompramaterial() + " " : "") +
						"GROUP BY  SC.CDSOLICITACAOCOMPRA, OM.QTDPEDIDA, OM.QTDEFREQUENCIA";

		SinedUtil.markAsReader();
		List<Double> listaUpdate = getJdbcTemplate().query(sql, 
			new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return rs.getDouble("RESTO");
				}
	
			});
		
		if(listaUpdate != null && listaUpdate.size() > 0){
			return listaUpdate.get(0);
		} else {
			String sql2 = 	"SELECT qtde_unidadeprincipal(SC.cdmaterial, SC.cdunidademedida, SC.QTDE) AS RESTO " +
							"FROM SOLICITACAOCOMPRA SC " +
							"WHERE SC.CDSOLICITACAOCOMPRA = " + ordemcompramaterial.getSolicitacaocompra().getCdsolicitacaocompra();

			SinedUtil.markAsReader();
			List<Double> listaUpdate2 = getJdbcTemplate().query(sql2, 
				new RowMapper() {
					public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
						return rs.getDouble("RESTO");
					}
				
				});
			
			if(listaUpdate2 != null && listaUpdate2.size() > 0){
				return listaUpdate2.get(0);
			} else {
				return 0.0;
			}
			
		}
	}
	
	/*public List<Ordemcompramaterial> findCodigoalternativo(Integer cdmaterial, Integer cdpessoa) {
		if(cdmaterial == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("materialfornecedor.codigo")
				.where("materialfornecedor.cdmaterial = ?", cdmaterial)
				.where("materialfornecedor.cdpessoa = ?", cdpessoa)
				.list();
	}*/
}
