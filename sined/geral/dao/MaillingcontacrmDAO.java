package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.MaillingcontacrmFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaillingcontacrmDAO extends GenericDAO<Contacrm> {
 
	@Override
	public void updateListagemQuery(QueryBuilder<Contacrm> query, FiltroListagem _filtro) {
		
		MaillingcontacrmFiltro filtro = (MaillingcontacrmFiltro) _filtro;
		query
			.select("contacrm.cdcontacrm ")
			.leftOuterJoin("contacrm.responsavel responsavel")
			.leftOuterJoin("contacrm.listcontacrmsegmento listcontacrmsegmento")
			.leftOuterJoin("listcontacrmsegmento.segmento segmento")
			.join("contacrm.listcontacrmcontato listcontacrmcontato")
			.join("listcontacrmcontato.listcontacrmcontatoemail listcontacrmcontatoemail")
			.where("listcontacrmcontato.id in (" +
					"select contacrmcontato.id from Campanhacontato campanhacontato " +
					"join campanhacontato.contacrmcontato contacrmcontato " +
					"join campanhacontato.campanha campanha " +
					"where campanha = ? )", filtro.getCampanha())
			.where("responsavel = ?", filtro.getResponsavel());
		
		if(filtro.getListasegmento() != null && !filtro.getListasegmento().isEmpty())
			query.whereIn("segmento", CollectionsUtil.listAndConcatenate(filtro.getListasegmento(), "cdsegmento",","));
		
		query.ignoreJoin("listcontacrmsegmento, listcontacrmcontato, listcontacrmcontatoemail");
	}
	
	public List<Contacrm> loadWithLista(String whereIn, String orderBy, boolean asc) {
		
		QueryBuilder<Contacrm> query = query()
					.select("contacrm.cdcontacrm, contacrm.nome, responsavel.nome, " +
							"listcontacrmcontatoemail.cdcontacrmcontatoemail, listcontacrmcontatoemail.email")
					.join("contacrm.listcontacrmcontato listcontacrmcontato")
					.join("listcontacrmcontato.listcontacrmcontatoemail listcontacrmcontatoemail")
					.leftOuterJoin("contacrm.responsavel responsavel")
					.whereIn("contacrm.cdcontacrm", whereIn);
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("contacrm.nome");
		}
		
		return query.list();
	}
	
}
