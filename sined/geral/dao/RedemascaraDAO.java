package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Redemascara;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("redemascara.nome")
public class RedemascaraDAO extends GenericDAO<Redemascara>{
			
}
