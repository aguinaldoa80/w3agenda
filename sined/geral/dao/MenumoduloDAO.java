package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Menumodulo;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("retira_acento(upper(menumodulo.nome))")
public class MenumoduloDAO extends GenericDAO<Menumodulo> {

	public List<Menumodulo> findForTag() {
		return querySined()
				
				.select("menumodulo.cdmenumodulo, menumodulo.nome, menumodulo.urlprefixo, menumodulo.ordem, menumodulo.eventoclick, menumodulo.dashboard")
				.orderBy("menumodulo.ordem, menumodulo.nome")
				.list();
	}

	public List<Menumodulo> findForTag(String contexto) {
		return querySined()
				
				.select("menumodulo.cdmenumodulo, menumodulo.nome, menumodulo.urlprefixo, menumodulo.ordem, menumodulo.eventoclick, menumodulo.dashboard")
				.where("menumodulo.contexto = ?", contexto)
					.or()
				.where("menumodulo.contexto is null")
				.orderBy("menumodulo.ordem, menumodulo.nome")
				.list();
	}

	public Menumodulo findByPrefixo(String urlprefixo, String contexto) {
		return querySined()
				
				.select("menumodulo.cdmenumodulo, menumodulo.nome, menumodulo.dashboard")
				.where("menumodulo.urlprefixo = ?", urlprefixo)
				.openParentheses()
					.where("menumodulo.contexto = ?", contexto)
						.or()
					.where("menumodulo.contexto is null")
				.closeParentheses()
				.unique();
	}
	
}
