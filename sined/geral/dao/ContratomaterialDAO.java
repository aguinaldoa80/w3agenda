package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Romaneioitem;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ContratomaterialFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContratomaterialDAO extends GenericDAO<Contratomaterial> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Contratomaterial> query, FiltroListagem _filtro) {
		ContratomaterialFiltro filtro = (ContratomaterialFiltro)_filtro;
		
		query
			.select("contratomaterial.cdcontratomaterial, contratomaterial.dtinicio, contratomaterial.dtfim, " +
					"contratomaterial.qtde, contrato.descricao, cliente.nome, servico.nome")
			.leftOuterJoin("contratomaterial.contrato contrato")
			.leftOuterJoin("contrato.cliente cliente")
			.leftOuterJoin("contratomaterial.servico servico")
			.where("contratomaterial.detalhamento = ?", Boolean.TRUE)
			.where("cliente = ?", filtro.getCliente())
			.where("contrato = ?", filtro.getContrato())
			;
		
		if(filtro.getSemProducaoagenda() != null && filtro.getSemProducaoagenda()){
			query
				.where("not exists (select pa.cdproducaoagenda " +
									"from Producaoagenda pa " +
									"join pa.contrato cpa " +
									"where cpa.cdcontrato = contrato.cdcontrato)");
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Contratomaterial> query) {
		query
			.select("contratomaterial.cdcontratomaterial, contratomaterial.hrinicio, contratomaterial.hrfim, " +
					"contratomaterial.valorunitario, contratomaterial.periodocobranca, contratomaterial.valordesconto, " +
					"contratomaterial.detalhamento, contratomaterial.dtinicio, contratomaterial.dtfim, contratomaterial.qtde, " +
					"contrato.cdcontrato, contrato.descricao, cliente.cdpessoa, cliente.identificador, cliente.nome, cliente.cnpj, cliente.cpf, " +
					"indice.cdindice, servico.cdmaterial, servico.identificacao, servico.nome, materialpromocional.cdmaterial")
			.leftOuterJoin("contratomaterial.contrato contrato")
			.leftOuterJoin("contrato.cliente cliente")
			.leftOuterJoin("contratomaterial.servico servico")
			.leftOuterJoin("contratomaterial.materialpromocional materialpromocional")
			.leftOuterJoin("contratomaterial.indice indice")
			;
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaContratomaterialitem");
		save.saveOrUpdateManaged("listaContratomaterialpessoa");
		save.saveOrUpdateManaged("listaContratomateriallocacao");
	}
	
	/**
	 * Retorna uma lista de contratos de material de acordo com o cliente
	 * @param cliente
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Contratomaterial> findByCliente(Cliente cliente){
		List<Contratomaterial> list = query().select("contratomaterial.cdcontratomaterial, contrato.descricao, servico.nome")
										   	 .join("contratomaterial.contrato contrato")
										   	 .join("contratomaterial.servico servico")
										   	 .where("contrato.cliente = ?", cliente)
										   	 .list();
		for (Contratomaterial contratomaterial : list) {
			contratomaterial.setDescricao(contratomaterial.getContrato().getDescricao() + " (" + contratomaterial.getServico().getNome() + ")");
		}
		return list;
	}
	
	/**
	 * Retorna o cliente de determinado contrato de material
	 * @param contratomaterial
	 * @return
	 * @author Thiago Gon�alves
	 */
	public Cliente findCliente(Contratomaterial contratomaterial){
		Contratomaterial bean = query().leftOuterJoinFetch("contratomaterial.contrato contrato")
									   .leftOuterJoinFetch("contrato.cliente cliente")
									   .where("contratomaterial=?", contratomaterial)
									   .unique();
		return bean.getContrato().getCliente();
	}

	/**
	 * Busca lista de Contratomaterial a partir de um contrato.
	 *
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contratomaterial> findByContrato(Contrato contrato) {
		List<Contratomaterial> list = query().select("contratomaterial.cdcontratomaterial, servico.nome, contratomaterial.qtde, contratomaterial.periodocobranca, " +
													 "contratomaterial.valorunitario, contratomaterial.dtfim, contratomaterial.dtinicio, " +
													 "servico.cdmaterial, servico.servico, contratomaterial.valordesconto, contrato.cdcontrato")
										   	 .join("contratomaterial.contrato contrato")
										   	 .join("contratomaterial.servico servico")
										   	 .where("contrato = ?", contrato)
										   	 .list();
		for (Contratomaterial contratomaterial : list) {
			contratomaterial.setDescricao(contratomaterial.getServico().getNome());
		}
		return list;
	}
	
	/**
	 * Busca os itens dos contratos passados por par�metros.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/12/2012
	 */
	public List<Contratomaterial> findByContrato(String whereIn) {
		return query()
				.select("contratomaterial.cdcontratomaterial, servico.nome, contratomaterial.qtde, contratomaterial.periodocobranca, " +
						"contratomaterial.valorunitario, contratomaterial.dtfim, contratomaterial.dtinicio, " +
						"servico.cdmaterial, servico.servico, contratomaterial.valordesconto, servico.patrimonio, " +
						"servico.produto, servico.epi, contratomaterialitem.cdcontratomaterialitem, contratomaterialitem.qtde, " +
						"materialDetalhe.cdmaterial, materialDetalhe.servico, materialDetalhe.patrimonio, materialDetalhe.produto, materialDetalhe.epi, " +
						"materialDetalhe.nome, contrato.cdcontrato, cliente.cdpessoa, patrimonioitem.cdpatrimonioitem, patrimonioitem.plaqueta")
						.join("contratomaterial.contrato contrato")
						.join("contrato.cliente cliente")
						.join("contratomaterial.servico servico")
						.leftOuterJoin("contratomaterial.patrimonioitem patrimonioitem")
						.leftOuterJoin("contratomaterial.listaContratomaterialitem contratomaterialitem")
						.leftOuterJoin("contratomaterialitem.material materialDetalhe")
						.whereIn("contrato.cdcontrato", whereIn)
						.list();
	}
	
	/**
	 * Busca os itens dos contratos passados por par�metros.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/12/2012
	 */
	public List<Contratomaterial> findByContratoProducao(String whereIn) {
		return query()
					.select("contratomaterial.cdcontratomaterial, servico.nome, contratomaterial.qtde, contratomaterial.periodocobranca, " +
							"contratomaterial.valorunitario, contratomaterial.dtfim, contratomaterial.dtinicio, contratomaterial.hrinicio, " +
							"servico.cdmaterial, servico.servico, contratomaterial.valordesconto, servico.patrimonio, " +
							"servico.produto, servico.epi, contratomaterialitem.cdcontratomaterialitem, contratomaterialitem.qtde, " +
							"materialDetalhe.cdmaterial, materialDetalhe.servico, materialDetalhe.patrimonio, materialDetalhe.produto, materialDetalhe.epi, " +
							"materialDetalhe.nome, contrato.cdcontrato, cliente.cdpessoa, empresa.cdpessoa")
					.join("contratomaterial.contrato contrato")
					.join("contrato.cliente cliente")
					.join("contratomaterial.servico servico")
					.leftOuterJoin("contrato.empresa empresa")
					.leftOuterJoin("contratomaterial.listaContratomaterialitem contratomaterialitem")
					.leftOuterJoin("contratomaterialitem.material materialDetalhe")
					.openParentheses()
					.where("servico.producao = ?", Boolean.TRUE)
					.or()
					.where("materialDetalhe.producao = ?", Boolean.TRUE)
					.closeParentheses()
					.whereIn("contrato.cdcontrato", whereIn)
					.list();
	}
	
	/**
	 * Busca a lista de material do contrato ordenado pelo grupo de material 
	 *
	 * @param contrato
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contratomaterial> findForRTFByContrato(Contrato contrato) {
		List<Contratomaterial> list = query().select("contratomaterial.cdcontratomaterial, servico.nome, contratomaterial.qtde, contratomaterial.periodocobranca, " +
													 "contratomaterial.valorunitario, contratomaterial.dtfim, contratomaterial.dtinicio, " +
													 "servico.cdmaterial, servico.servico, contratomaterial.valordesconto, materialgrupo.cdmaterialgrupo," +
													 "servico.valorcusto, contratomaterial.hrinicio, contratomaterial.hrfim, patrimonioitem.plaqueta, servico.valorindenizacao," +
													 "contratomaterial.observacao, listaCaracteristica.nome, listaCaracteristica.valor ")
										   	 .join("contratomaterial.contrato contrato")
										   	 .join("contratomaterial.servico servico")
										   	 .leftOuterJoin("contratomaterial.patrimonioitem patrimonioitem")
										   	 .leftOuterJoin("servico.materialgrupo materialgrupo")
										   	 .leftOuterJoin("servico.listaCaracteristica listaCaracteristica")
										   	 .where("contrato = ?", contrato)
										   	 .orderBy("contratomaterial.cdcontratomaterial")
										   	 .list();
		
		for (Contratomaterial contratomaterial : list) {
			contratomaterial.setDescricao(contratomaterial.getServico().getNome());
		}
		return list;
	}

	public List<Contratomaterial> findByClienteTipoServico(Cliente cliente, List<Materialtipo> materialtipos) {
		QueryBuilder<Contratomaterial> query = query()
													.select("contratomaterial.cdcontratomaterial, contratomaterial.qtde, contrato.descricao, servico.nome")
											   	 	.join("contratomaterial.contrato contrato")
											   	 	.join("contratomaterial.servico servico")
											   	 	.join("servico.materialtipo materialtipo")
											   	 	.where("contrato.cliente = ?", cliente);
		
		if(materialtipos != null && materialtipos.size() > 0){
			query.openParentheses();
			for (Materialtipo materialtipo : materialtipos) {
				query.where("materialtipo = ?", materialtipo).or();
			}
			query.closeParentheses();
		} else {
			query.where("1=0");
		}
		
		List<Contratomaterial> list = query.list();
		
		for (Contratomaterial contratomaterial : list) {
			contratomaterial.setDescricao(contratomaterial.getContrato().getDescricao() + " (" + contratomaterial.getServico().getNome() + ")");
		}
		return list;
	}
	
	/**
	 * M�todo que retorna a lista de contratos materiais que ser� usada para a montagem do combo, 
	 * na tela de entrada de contrato materiais rede.
	 * 
	 * Al�m do filtro dos tipos de materiais, somente ser�o exibidos os contratos materiais que n�o est�o vinculados. 
	 * @param cliente
	 * @param materialtipos
	 * @param cdContratoMaterialRedeCod
	 * @return
	**/
	public List<Contratomaterial> findForCombo(Cliente cliente, List<Materialtipo> materialtipos, Integer cdContratoMaterialRedeCod) {
		
		QueryBuilder<Contratomaterial> query = query()
													.select("contratomaterial.cdcontratomaterial, contrato.descricao, servico.nome")
											   	 	.join("contratomaterial.contrato contrato")
											   	 	.join("contratomaterial.servico servico")
											   	 	.join("servico.materialtipo materialtipo")
											   	 	.leftOuterJoin("contratomaterial.listaContratomaterialrede listaContratomaterialrede")
											   	 	.where("contrato.cliente = ?", cliente);
		
		if(cdContratoMaterialRedeCod != null){
			query.where("(listaContratomaterialrede.cdcontratomaterialrede =? OR listaContratomaterialrede.cdcontratomaterialrede is null)", cdContratoMaterialRedeCod);
		}else{
			query.where("listaContratomaterialrede.cdcontratomaterialrede is null", cdContratoMaterialRedeCod);
		}
		
		query.openParentheses();
		for (Materialtipo materialtipo : materialtipos) {
			query.where("materialtipo = ?", materialtipo).or();
		}
		query.closeParentheses();
		
		List<Contratomaterial> list = query.list();
		
		for (Contratomaterial contratomaterial : list) {
			contratomaterial.setDescricao(contratomaterial.getContrato().getDescricao() + " (" + contratomaterial.getServico().getNome() + ")");
		}
		return list;
	}
	
	public Contratomaterial loadContratomaterial(Contratomaterial contratomaterial){
		return query()
		.select("contratomaterial.cdcontratomaterial, contrato.cdcontrato")
		.join("contratomaterial.contrato contrato")
		.where("contratomaterial = ?", contratomaterial)
		.unique();
	}
	
	public Contratomaterial loadForEmail(Contratomaterial contratomaterial){
		return query()
		.select("contratomaterial.qtde, unidademedida.nome")
		.join("contratomaterial.servico servico")
		.join("servico.unidademedida unidademedida")
		.where("contratomaterial = ?", contratomaterial)
		.unique();
	}

	/**
	 * Atualiza o item de patrim�nio do item do contrato.
	 *
	 * @param contratomaterial
	 * @param patrimonioitem
	 * @author Rodrigo Freitas
	 * @since 27/09/2013
	 */
	public void updatePatrimonioitem(Contratomaterial contratomaterial, Patrimonioitem patrimonioitem) {
		getHibernateTemplate().bulkUpdate("update Contratomaterial c set c.patrimonioitem = ? where c = ?", new Object[]{
			patrimonioitem, contratomaterial
		});
	}

	/**
	 * Atualiza o campo servi�o do item do contrato
	 *
	 * @param contratomaterial
	 * @param material
	 * @author Rodrigo Freitas
	 * @since 11/10/2013
	 */
	public void updateMaterial(Contratomaterial contratomaterial, Material material) {
		getHibernateTemplate().bulkUpdate("update Contratomaterial c set c.servico = ? where c = ?", new Object[]{
			material, contratomaterial
		});
	}

	/**
	 * Atualiza o campo valor unit�rio do item do contrato
	 *
	 * @param contratomaterial
	 * @param valorunitario
	 * @author Rodrigo Freitas
	 * @since 27/09/2016
	 */
	public void updateValorunitario(Contratomaterial contratomaterial, Double valorunitario) {
		getHibernateTemplate().bulkUpdate("update Contratomaterial c set c.valorunitario = ? where c = ?", new Object[]{
			valorunitario, contratomaterial
		});
	}
	

	/**
	 * Atualiza o campo valor fechado do item do contrato
	 *
	 * @param contratomaterial
	 * @param valorunitario
	 * @author Rodrigo Freitas
	 * @since 27/09/2016
	 */
	public void updateValorfechado(Contratomaterial contratomaterial, Double valorfechado) {
		getHibernateTemplate().bulkUpdate("update Contratomaterial c set c.valorfechado = ? where c = ?", new Object[]{
				valorfechado, contratomaterial
		});
	}
	
	public Contratomaterial loadByRomaneioitem(Romaneioitem romaneioitem, boolean byContratofechamento){
		QueryBuilder<Contratomaterial> qry = query();
		if(byContratofechamento){
			qry
				.select("contratomaterial.valorunitario, contratomaterial.valordesconto, contratomaterial.valorfechado")
				.join("contratomaterial.contrato contrato")
				.join("contrato.listaRomaneioorigemFechamento romaneioorigemFechamento")
				.join("romaneioorigemFechamento.romaneio romaneio")
				.where("exists(select 1 from Romaneioitem ri where ri.material = contratomaterial.servico and ri = ?)", romaneioitem)
				.where("romaneio = ?", romaneioitem.getRomaneio())
				.where("contratomaterial.servico = ?", romaneioitem.getMaterial())
				.where("contratomaterial.patrimonioitem = ?", romaneioitem.getPatrimonioitem());			
		}else{
			qry
				.select("contratomaterial.valorunitario, contratomaterial.valordesconto, contratomaterial.valorfechado")
				.join("contratomaterial.contrato contrato")
				.join("contrato.listaRomaneioorigem romaneioorigem")
				.join("romaneioorigem.romaneio romaneio")
				.where("exists(select 1 from Romaneioitem ri where ri.material = contratomaterial.servico and ri = ?)", romaneioitem)
				.where("romaneio = ?", romaneioitem.getRomaneio())
				.where("contratomaterial.servico = ?", romaneioitem.getMaterial())
				.where("contratomaterial.patrimonioitem = ?", romaneioitem.getPatrimonioitem());
		}
		
		return qry.unique();
	}
	
	public void updateCamposRetornoContrato(Contratomaterial contratomaterial) {
		Double qtde = contratomaterial.getQtde();
		Double valorunitario = contratomaterial.getValorunitario();
		Date dtfim = contratomaterial.getDtfim();
		
		StringBuilder set = new StringBuilder();
		
		set.append(qtde != null ? " qtde = " + qtde : "");
		set.append(set.length() > 0 && valorunitario != null ? "," : "").append(valorunitario != null ? " valorunitario = " + valorunitario : "");
		set.append(set.length() > 0 && dtfim != null  ? "," : "").append(dtfim != null ? " dtfim = '" + dtfim + "'" : "");
		
		getHibernateTemplate().bulkUpdate("update Contratomaterial c set " + set.toString() + " where c.cdcontratomaterial = " + contratomaterial.getCdcontratomaterial());
	}
}