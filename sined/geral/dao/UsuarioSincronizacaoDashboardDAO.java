package br.com.linkcom.sined.geral.dao;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.UsuarioSincronizacaoDashboard;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class UsuarioSincronizacaoDashboardDAO extends GenericDAO<UsuarioSincronizacaoDashboard>{

	public List<UsuarioSincronizacaoDashboard> findForSincronizacao(){
		return query()
				.select("usuarioSincronizacaoDashboard.idusuario, usuarioSincronizacaoDashboard.dtSincronizacao, usuarioSincronizacaoDashboard.token")
				.where("cast(usuarioSincronizacaoDashboard.dtSincronizacao as date) is null")
				.where("usuarioSincronizacaoDashboard.criar=?",Boolean.TRUE)
				.list();
	}
	
	public List<UsuarioSincronizacaoDashboard> findForSincronizacaoDelete(){
		return query()
				.select("usuarioSincronizacaoDashboard.idusuario, usuarioSincronizacaoDashboard.dtSincronizacao, usuarioSincronizacaoDashboard.token")
				.where("cast(usuarioSincronizacaoDashboard.dtSincronizacao as date) is null")
				.where("usuarioSincronizacaoDashboard.deletar=?",Boolean.TRUE)
				.list();
	}
	
	public void updateError(Integer idusuario, Integer codigo, String mensagem){
		mensagem = mensagem==null ? null : mensagem.replaceAll("'", "");
		if(idusuario==null)
			throw new RuntimeException("O par�metro idusuario n�o pode  ser nulo.");
		getJdbcTemplate().update("UPDATE usuariosincronizacaodashboard SET codigoerro = ?, mensagemerro = ? WHERE idusuario = ?",new Object[]{codigo, mensagem, idusuario});
	}

	public void updateSincronizacao(Integer idusuario,String token, String cdpessoa, Integer codigo, String mensagem) {
		if(idusuario==null || StringUtils.isBlank(cdpessoa) || StringUtils.isBlank(token))
			throw new RuntimeException("Os par�metros idusuario, cdpessoa e e o token n�o podem ser nulos.");		
		getJdbcTemplate().update("UPDATE usuariosincronizacaodashboard SET dtsincronizacao = ?, codigoerro = ?, mensagemerro = ?, token = ?, cdpessoa = ? WHERE idusuario = ?",new Object[]{new Date(),codigo , mensagem, token,cdpessoa, idusuario} );

	}
	public void updateSincronizacaodelete(Integer idusuario,Integer codigo, String mensagem) {
		if(idusuario==null)
			throw new RuntimeException("O par�metro idusuario n�o pode ser nulo.");		
		getJdbcTemplate().update("UPDATE usuariosincronizacaodashboard SET dtsincronizacao = ?, codigoerro = ?, mensagemerro = ?, token = ?, cdpessoa = ? WHERE idusuario = ?",new Object[]{new Date(),codigo , mensagem, null,null, idusuario} );

	}

	public UsuarioSincronizacaoDashboard findByUsuario(Usuario usuario) {
		if(usuario==null || usuario.getCdpessoa()==null)
			throw new RuntimeException("Os par�metros usuario e o id n�o podem ser nulos.");	
		return query()
					.select("usuarioSincronizacaoDashboard.idusuario, usuarioSincronizacaoDashboard.token, usuarioSincronizacaoDashboard.cdpessoa")
					.where("cast(usuarioSincronizacaoDashboard.dtSincronizacao as date) is not null")
					.where("usuarioSincronizacaoDashboard.idusuario=?",usuario.getCdpessoa())
					.unique();
	}
	
}
