package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Faturamentohistorico;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class FaturamentohistoricoDAO extends GenericDAO<Faturamentohistorico> {

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaFaturamentohistoricomaterial");
	}

	/**
	 * Verifica se o documento pertence a algum faturamento.
	 *
	 * @param documento
	 * @return
	 * @since 21/09/2012
	 * @author Rodrigo Freitas
	 */
	public boolean haveHistoricoDocumento(Documento documento) {
		return newQueryBuilderSined(Long.class)
					.select("count(*)")
					.from(Faturamentohistorico.class)
					.join("faturamentohistorico.documento documento")
					.where("documento = ?", documento)
					.unique() > 0;
	}

	/**
	 * Busca os dados do faturamento para gerar NF a partir da conta a receber.
	 *
	 * @param whereIn
	 * @return
	 * @since 21/09/2012
	 * @author Rodrigo Freitas
	 */
	public List<Faturamentohistorico> findForFaturamentoByDocumento(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
					.select("faturamentohistorico.numeroparcela, " +
							"faturamentohistorico.totalparcelas, " +
							"faturamentohistorico.deducao, " +
							"faturamentohistorico.descontocondicionado, " +
							"faturamentohistorico.descontoincondicionado, " +
							"faturamentohistorico.outrasretencoes, " +
							"faturamentohistorico.descontoproduto, " +
							"faturamentohistorico.mes, " +
							"faturamentohistorico.ano, " +
							"faturamentohistorico.valor, " +
							"listaFaturamentohistoricomaterial.qtde, " +
							"listaFaturamentohistoricomaterial.valorunitario, " +
							"listaFaturamentohistoricomaterial.valordesconto, " +
							"material.cdmaterial, " +
							"material.nome, " +
							"material.servico, " +
							"material.tributacaoestadual, " +
							"material.ncmcompleto, " +
							"material.nve, " +
							"material.extipi, " +
							"materialgrupo.cdmaterialgrupo, " +
							"ncmcapitulo.cdncmcapitulo, " +
							"material.codigobarras, " +
							"unidademedida.cdunidademedida, " +
							"unidademedida.simbolo, " +
							"contrato.cdcontrato, " +
							"contrato.anotacaocorpo, " +
							"contrato.descricao, " +
							"contrato.faturamentolote, " +
							"codigotributacao.cdcodigotributacao, " +
							"itemlistaservicoBean.cditemlistaservico, " +
							"codigocnaeBean.cdcodigocnae, " +
							"grupotributacao.cdgrupotributacao, grupotributacao.infoadicionalfisco, grupotributacao.infoadicionalcontrib, " +
							"conta.cdconta, " +
							"empresa.cdpessoa, " +
							"empresa.serienfe, " +
							"empresa.nome, " +
							"endereco.cdendereco, " +
							"cliente.cdpessoa, " +
							"cliente.nome, " +
							"listaEndereco.cdendereco, " +
							"frequencia.cdfrequencia, " +
							"documento.cddocumento, " +
							"documento.dtvencimento," +
							"documento.valor ")
					.leftOuterJoin("faturamentohistorico.documento documento")
					.leftOuterJoin("faturamentohistorico.listaFaturamentohistoricomaterial listaFaturamentohistoricomaterial")
					.leftOuterJoin("listaFaturamentohistoricomaterial.material material")
					.leftOuterJoin("material.unidademedida unidademedida")
					.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
					.leftOuterJoin("material.materialgrupo materialgrupo")
					.leftOuterJoin("faturamentohistorico.contrato contrato")
					.leftOuterJoin("contrato.codigotributacao codigotributacao")
					.leftOuterJoin("contrato.itemlistaservicoBean itemlistaservicoBean")
					.leftOuterJoin("contrato.codigocnaeBean codigocnaeBean")
					.leftOuterJoin("contrato.grupotributacao grupotributacao")
					.leftOuterJoin("contrato.conta conta")
					.leftOuterJoin("contrato.empresa empresa")
					.leftOuterJoin("contrato.endereco endereco")
					.leftOuterJoin("contrato.frequencia frequencia")
					.leftOuterJoin("contrato.cliente cliente")
					.leftOuterJoin("cliente.listaEndereco listaEndereco")
					.whereIn("documento.cddocumento", whereIn)
					.list();
	}
	
}
