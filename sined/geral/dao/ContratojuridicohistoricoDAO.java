package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contratojuridico;
import br.com.linkcom.sined.geral.bean.Contratojuridicohistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ContratojuridicohistoricoDAO extends GenericDAO<Contratojuridicohistorico>{

	public List<Contratojuridicohistorico> findByContratojuridico(Contratojuridico contratojuridico) {
		return query()
					.select("contratojuridicohistorico.cdcontratojuridicohistorico, contratojuridicohistorico.acao, " +
							"contratojuridicohistorico.observacao, contratojuridicohistorico.cdusuarioaltera, contratojuridicohistorico.dtaltera")
					.where("contratojuridicohistorico.contratojuridico = ?", contratojuridico)
					.orderBy("contratojuridicohistorico.dtaltera desc")
					.list();
	}

}
