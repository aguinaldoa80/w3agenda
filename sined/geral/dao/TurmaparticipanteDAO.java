package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Turma;
import br.com.linkcom.sined.geral.bean.Turmaparticipante;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("turmaparticipante.colaborador.nome")
public class TurmaparticipanteDAO extends GenericDAO<Turmaparticipante> {


	/* singleton */
	private static TurmaparticipanteDAO instance;
	public static TurmaparticipanteDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(TurmaparticipanteDAO.class);
		}
		return instance;
	}

	/**
	 * M�todo para obter lista de <code>Turmaparticipante</code> pela turma.
	 * 
	 * @param turma
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Turmaparticipante> findByTurma(Turma turma){
		if(turma == null || turma.getCdturma() == null){
			throw new SinedException("Os par�metros turma ou cdturma n�o podem ser null.");
		}
		return 
			query()
			.select("turmaparticipante.cdturmaparticipante,turma.cdturma,colaborador.cdpessoa")
			.join("turmaparticipante.turma turma")
			.join("turmaparticipante.colaborador colaborador")
			.where("turma = ?",turma)
			.list();
	}
	
}