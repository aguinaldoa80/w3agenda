package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Tipocargo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CargoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("cargo.nome")
public class CargoDAO extends GenericDAO<Cargo> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Cargo> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		CargoFiltro filtro = (CargoFiltro) _filtro;
		query
		.select("distinct cargo.cdcargo, cargo.nome, cargo.descricao, cargo.codigoFolha, cbo.cdcbo, cbo.nome, cargo.ativo")
			.leftOuterJoin("cargo.cbo cbo")
			.where("cargo.codigoFolha = ?",filtro.getCodigoFolha())
			.whereLikeIgnoreAll("cargo.nome", filtro.getNome())
			.whereLikeIgnoreAll("cargo.descricao", filtro.getDescricao())
			.where("cbo = ?", filtro.getCbo())
			.where("cargo.ativo = ?", filtro.getMostrar())
			.orderBy("cargo.nome")
			;
		
		if(filtro.getDepartamento() != null){
			query
				.leftOuterJoin("cargo.listaCargodepartamento cargodepartamento")
				.leftOuterJoin("cargodepartamento.departamento departamento")
				.where("departamento = ?", filtro.getDepartamento())
				.ignoreJoin("cargodepartamento","departamento");
		}
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaCargodepartamento");
		save.saveOrUpdateManaged("listaCargomaterialseguranca");
		save.saveOrUpdateManaged("listaCargoatividade");
		save.saveOrUpdateManaged("listaCargoexameobrigatorio");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Cargo> query) {
		StringBuilder campos  = new StringBuilder();
		campos.append("cargo.cdcargo, cargo.nome, cargo.descricao, cargo.codigoFolha, cargo.ativo, ");
		campos.append("cargo.cdusuarioaltera, cargo.dtaltera, cargo.totalhorasemana, riscotrabalho.cdriscotrabalho, ");
		campos.append("departamento.cddepartamento, departamento.nome, departamento.descricao, departamento.codigofolha, ");
		campos.append("listaCargoatividade.descricao, listaCargoatividade.cdcargoatividade, listaCargoatividade.valorhoradiferenciada, ");
		campos.append("atividadetipo.cdatividadetipo, atividadetipo.nome, ");
		campos.append("cbo.cdcbo, cbo.nome, cargo.custohora, contagerencial.cdcontagerencial, vcontagerencial.arvorepai, vcontagerencial.identificador, contagerencial.nome, tipocargo.cdtipocargo, ");
		campos.append("listaCargomaterialseguranca.cdcargomaterialseguranca, listaCargomaterialseguranca.quantidade, epi.cdmaterial, ");
		campos.append("listaCargoexameobrigatorio.cdcargoexameobrigatorio, exametipo.nome, exametipo.cdexametipo, examenatureza.nome, examenatureza.cdexamenatureza, frequencia.nome, frequencia.cdfrequencia");
		query
		.select(campos.toString())
		.leftOuterJoin("cargo.listaCargodepartamento cargodepartamento")
		.leftOuterJoin("cargodepartamento.departamento departamento")
		.leftOuterJoin("cargo.cbo cbo")
		.leftOuterJoin("cargo.tipocargo tipocargo")
		.leftOuterJoin("cargo.contagerencial contagerencial")
		.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
		.leftOuterJoin("cargo.listaCargomaterialseguranca listaCargomaterialseguranca")
		.leftOuterJoin("cargo.listaCargoatividade listaCargoatividade")
		.leftOuterJoin("listaCargoatividade.atividadetipo atividadetipo")
		.leftOuterJoin("listaCargomaterialseguranca.riscotrabalho riscotrabalho")
		.leftOuterJoin("listaCargomaterialseguranca.epi epi")
		.leftOuterJoin("cargo.listaCargoexameobrigatorio listaCargoexameobrigatorio")
		.leftOuterJoin("listaCargoexameobrigatorio.exametipo exametipo")
		.leftOuterJoin("listaCargoexameobrigatorio.examenatureza examenatureza")
		.leftOuterJoin("listaCargoexameobrigatorio.frequencia frequencia")
		;
	}
	

	public List<Cargo> loadWithLista(String whereIn, String orderBy, boolean asc) {
		QueryBuilder<Cargo> query = query()
					.select("cargo.cdcargo, cargo.nome, cargo.descricao, cargo.codigoFolha, cbo.cdcbo, cbo.nome, cargo.ativo, departamento.nome")
					.leftOuterJoin("cargo.listaCargodepartamento cargodepartamento")
					.leftOuterJoin("cargodepartamento.departamento departamento")
					.leftOuterJoin("cargo.cbo cbo")
					.whereIn("cargo.cdcargo", whereIn);
		
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("cargo.nome");
		}
		
		return query.list();
	}
	
	/**
	 * Localiza os cargos ativos relacionados a um determinado departamento
	 * 
	 * @param departamento
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public List<Cargo> findCargo(Departamento departamento, String campos) {
		if (departamento == null || departamento.getCddepartamento() == null) {
			throw new SinedException("O par�metro departamento n�o pode ser null.");
		}
		QueryBuilder<Cargo> lista = query()
			.join("cargo.listaCargodepartamento cargodepartmento")
			.join("cargodepartmento.departamento departamento")
			.where("departamento = ?", departamento)
			.where("(ativo is null or ativo = true)")
			.orderBy("cargo.nome");
		if (!"".equals(campos)) {
			lista.select(campos);
		}
		return lista.list();
	}
	
	/**
	 * M�todo para obter todos os cargos relacionados a um determinado departamento.
	 * 
	 * @param departamento
	 * @return
	 * @author Fl�vio Tavares 
	 */
	public List<Cargo> findAllCargos(Departamento departamento){
		if(departamento==null || departamento.getCddepartamento()==null){
			throw new SinedException("Par�metros inv�lidos em departamento.");
		}
		return 
			query()
			.join("cargo.listaCargodepartamento cargodepartmento")
			.join("cargodepartmento.departamento departamento")
			.where("departamento = ?", departamento)
			.orderBy("cargo.nome")
			.list();
	}
	
	/**
	 * M�todo para obter cargos por tarefa. Utiliza os registros da tabela
	 * TAREFARECURSOHUMANO para buscar os cargos associados a determinada tarefa.
	 * 
	 * @param tarefa
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Cargo> findByTarefa(Tarefa tarefa){
		if(tarefa == null || tarefa.getCdtarefa() == null){
			throw new SinedException("Os par�metros tarefa ou cdtarefa n�o podem ser null.");
		}
		
		return 
			query()
			.select("distinct cargo.cdcargo,cargo.nome")
			.join("cargo.listaTarefarecursohumano trh")
			.join("trh.tarefa tarefa")
			.where("tarefa = ?",tarefa)
			.list();
	}
	
	/**
	 * Carrega a lista de cargo para ser exibida na parte em flex.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cargo> findAllForFlex(){
		return query()
			.select("cargo.cdcargo, cargo.nome, " +
					"tipocargo.cdtipocargo, cargo.custohora")
			.join("cargo.tipocargo tipocargo")
			.orderBy("cargo.nome")
			.list();
	}
	
	/**
	 * Carrega a lista de cargo do tipo M�o-de-Obra Direta para ser exibida na parte em flex.
	 *
	 * @return List<Cargo>
	 * @author Rodrigo Alvarenga
	 */
	public List<Cargo> findAllMODForFlex(){
		return query()
			.select("cargo.cdcargo, cargo.nome, " +
					"tipocargo.cdtipocargo")
			.join("cargo.tipocargo tipocargo")
			.where("tipocargo = ?", Tipocargo.MOD)
			.orderBy("cargo.nome")
			.list();
	}
	
	/* singleton */
	private static CargoDAO instance;
	public static CargoDAO getInstance() {
		if(instance == null){
			instance = Neo.getObject(CargoDAO.class);
		}
		return instance;
	}

	
	/**
	 * M�todo para obter cargos por planejamento. Utiliza os registros da tabela
	 * TAREFARECURSOHUMANO para buscar os cargos associados a determinado planejamento.
	 * 
	 * @param  planejamento
	 * @return
	 * @author Ramon Brazil
	 */
	public List<Cargo> findByPlanejamento(Planejamento planejamento){
		if( planejamento == null ||  planejamento.getCdplanejamento() == null){
			throw new SinedException("Os par�metros planejamento ou cdplanejamento n�o podem ser null.");
		}
		
		return 
			query()
			.select("distinct cargo.cdcargo,cargo.nome")
			.join("cargo.listaTarefarecursohumano trh")
			.join("trh.tarefa tarefa")
			.join("tarefa.planejamento  planejamento")
			.where("planejamento = ?", planejamento)
			.list();
	}

	/**
	 * M�todo para buscar Cargos por planejamento e tarefa.
	 * 
	 * @param planejamento
	 * @param tarefa
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Cargo> findByPlanejamentoTarefa(Planejamento planejamento, Tarefa tarefa){
		QueryBuilder<Cargo> query = query()
			.select("distinct cargo.cdcargo,cargo.nome")
			.leftOuterJoin("cargo.listaTarefarecursohumano trh")
			.leftOuterJoin("trh.tarefa tarefa")
			.leftOuterJoin("cargo.listaPlanejamentorecursohumano prh")
			.leftOuterJoin("prh.planejamento planejamento")
			.orderBy("cargo.nome");
		
		if(tarefa != null){
			query.where("tarefa = ?", tarefa);
		} else {
			query.where("planejamento = ?", planejamento);
		}
		
		return query.list();
	};
	
	
	/**
	 * Retorna uma lista com todos os cargos
	 * @return
	 * @author Taidson
	 * @since 22/06/2010
	 */
	public List<Cargo> carregaCargos(){
		return 
			query()
			.select("distinct cargo.cdcargo,cargo.nome")
			.orderBy("cargo.nome")
			.list();
	}

	/**
	 * Busca o cargo de acordo com a matr�cula.
	 *
	 * @param matricula
	 * @return
	 * @since Jun 13, 2011
	 * @author Rodrigo Freitas
	 */
	public Cargo findByMatricula(Integer matricula) {
		List<Cargo> lista = query()
								.select("cargo.cdcargo, cargo.custohora, cargo.totalhorasemana, tipocargo.cdtipocargo")
								.leftOuterJoin("cargo.listaColaboradorcargo colaboradorcargo")
								.leftOuterJoin("cargo.tipocargo tipocargo")
								.where("colaboradorcargo.matricula = ?", matricula)
								.orderBy("colaboradorcargo.dtinicio desc")
								.list();
		if(lista == null || lista.size() == 0){
			return null;
		} else {
			return lista.get(0);
		}
	}

	/**
	 * Busca o cargo pelo seu c�digo folha.
	 *
	 * @param codigofolha
	 * @return
	 * @since Jul 5, 2011
	 * @author Rodrigo Freitas
	 */
	public Cargo findByCodigofolha(Integer codigofolha) {
		if(codigofolha == null || codigofolha.equals("")){
			throw new SinedException("C�digo folha n�o pode ser nulo.");
		}
				return query()
					.select("cargo.cdcargo, cargo.nome")
					.where("cargo.codigoFolha = ?", codigofolha)
					.or()
					.where("cargo.cdcargo = ?", codigofolha)
					.unique();
	}
	
	/**
	 * 
	 * M�todo que busca a lista de Cargos para a gera��o do relat�rio.
	 *
	 * @name findForReport
	 * @param filtro
	 * @return
	 * @return List<Cargo>
	 * @author Thiago Augusto
	 * @date 07/05/2012
	 *
	 */
	public List<Cargo> findForReport(CargoFiltro filtro){
		return query()
					.select("cargo.cdcargo, cargo.nome, cargo.codigoFolha, cargo.ativo, cbo.nome, departamento.nome, departamento.descricao")
					.leftOuterJoin("cargo.cbo cbo")
					.leftOuterJoin("cargo.listaCargodepartamento listaCargodepartamento")
					.leftOuterJoin("listaCargodepartamento.departamento departamento")
					.whereLikeIgnoreAll("cargo.nome", filtro.getNome())
					.whereLikeIgnoreAll("cargo.descricao", filtro.getDescricao())
					.where("cbo = ?", filtro.getCbo())
					.where("cargo.ativo = ?", filtro.getMostrar())
					.orderBy("cargo.nome")
					.list();
	}

}
