package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Leadhistorico;
import br.com.linkcom.sined.geral.bean.Leadsituacao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.LeadFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class LeadDAO extends GenericDAO<Lead>{
	
	protected UsuarioService usuarioService;
	
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Lead> query, FiltroListagem _filtro) {

		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		
		LeadFiltro filtro = (LeadFiltro)_filtro;
		query
			.select("distinct new br.com.linkcom.sined.geral.bean.Lead(" +
					
					"lead.cdlead, lead.nome, lead.empresa, lead.dtretorno, lead.proximopasso, lead.dtultimohistorico, lead.qtdehistorico, lead.email, " +
					"leadsituacao.cdleadsituacao, leadsituacao.nome, " +
					"responsavel.nome, agencia.nome, lead.tiporesponsavel, " +
					"leadqualificacao.nome, coalesce(responsavel.nome, agencia.nome))")
			.setUseTranslator(false)	
			.leftOuterJoin("lead.leadsituacao leadsituacao")
			.leftOuterJoin("lead.leadqualificacao leadqualificacao")
			.leftOuterJoin("lead.responsavel responsavel")
			.leftOuterJoin("lead.agencia agencia")
			.whereLikeIgnoreAll("lead.nome", filtro.getNome())
			.whereLikeIgnoreAll("lead.empresa", filtro.getEmpresa())
			.where("lead.dtretorno >= ?", filtro.getDtinicioretorno())
			.where("lead.dtretorno <= ?", filtro.getDtfimretorno())
			.where("lead.dtultimohistorico >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getDtinicioultimocontato()))
			.where("lead.dtultimohistorico <= ?", SinedDateUtils.dateToTimestampFinalDia(filtro.getDtfimultimocontato()))
			.where("lead.qtdehistorico >= ?", filtro.getIniciointeracao())
			.where("lead.qtdehistorico <= ?", filtro.getFiminteracao())
			.where("leadqualificacao = ?", filtro.getLeadqualificacao())
			.where("lead.concorrente = ?", filtro.getConcorrente());
		
		if(filtro.getDtiniciohistorico() != null || filtro.getDtfimhistorico() != null || filtro.getAtividadetipo() != null){
			query
				.leftOuterJoin("lead.listLeadhistorico listLeadhistorico", true)
				.leftOuterJoin("listLeadhistorico.atividadetipo atividadetipo", true)
				.where("listLeadhistorico.dtaltera >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getDtiniciohistorico()))
				.where("listLeadhistorico.dtaltera <= ?", SinedDateUtils.dateToTimestampFinalDia(filtro.getDtfimhistorico()))
				.where("atividadetipo = ?", filtro.getAtividadetipo());
		}
		if(filtro.getLeadSegmento() != null && filtro.getLeadSegmento().getSegmento() != null){
			query
				.leftOuterJoin("lead.listLeadsegmento listLeadsegmento", true)
				.where("listLeadsegmento.segmento = ?", filtro.getLeadSegmento() != null ? filtro.getLeadSegmento().getSegmento() : "");
			
			if (filtro.getLeadSegmento().getSegmento().getCampoExtraSegmento() != null) {
				query
					.leftOuterJoin("listLeadsegmento.lead lead2", true)
					.leftOuterJoin("lead2.campoExtraLeadList campoExtraLeadList", true)
					.leftOuterJoin("campoExtraLeadList.campoExtraSegmento campoExtraSegmento", true)
					.where("campoExtraSegmento.cdcampoextrasegmento = ?", filtro.getLeadSegmento().getSegmento().getCampoExtraSegmento().getCdcampoextrasegmento());
				
				if (filtro.getValorCampoAdicional() != null && !filtro.getValorCampoAdicional().equals("")) {
					query
						.whereLikeIgnoreAll("campoExtraLeadList.valor", filtro.getValorCampoAdicional());
				}
			}
		}
		if(filtro.getMunicipio() != null || filtro.getUf() != null){
			query
				.leftOuterJoin("lead.municipio municipio", true)
				.where("municipio.uf = ?", filtro.getUf())
				.where("municipio = ?", filtro.getMunicipio());
		}
		if(filtro.getCampanha() != null || filtro.getCampanhatipo() != null){
			query
				.leftOuterJoin("lead.listLeadcampanha listLeadcampanha", true)
				.leftOuterJoin("listLeadcampanha.campanha campanha", true)
				.where("campanha = ?", filtro.getCampanha())
				.where("campanha.campanhatipo = ?", filtro.getCampanhatipo());
		}
		
		if (filtro.getConvertidos() != null && !filtro.getConvertidos()){
			query
				.where("lead.cliente is null")
				.where("not exists (select 1 from Contacrm c where c.lead = lead)");
		} else if (filtro.getConvertidos() != null && filtro.getConvertidos()){
			query
				.openParentheses()
					.where("lead.cliente is not null").or()
					.where("exists (select 1 from Contacrm c where c.lead = lead)")
				.closeParentheses();
		}
		
		if(SinedUtil.isRestricaoClienteVendedor(usuarioLogado)){
			if(!usuarioService.isVisualizaOutrasOportunidades(usuarioLogado)){
				if(ColaboradorService.getInstance().isColaborador(usuarioLogado.getCdpessoa())){
					query.openParentheses();
					query.where("responsavel.cdpessoa = ?", usuarioLogado.getCdpessoa());
					query.closeParentheses();
				}else if(FornecedorService.getInstance().isFornecedor(usuarioLogado.getCdpessoa())){
					query.openParentheses();
					query.where("agencia.cdpessoa = ?", usuarioLogado.getCdpessoa());
					query.closeParentheses();
				}
			}else{
				query.where("responsavel = ?", filtro.getResponsavel());
				query.where("agencia = ?", filtro.getAgencia());
			}
		}else{
			query.where("responsavel = ?", filtro.getResponsavel());
			query.where("agencia = ?", filtro.getAgencia());
		}
		
		
		
		List<Leadsituacao> listaLeadsituacao = filtro.getListaLeadsituacao();
		if (listaLeadsituacao != null){
			query.whereIn("leadsituacao.cdleadsituacao", CollectionsUtil.listAndConcatenate(listaLeadsituacao,"cdleadsituacao", ","));
		}
	}
	
	@Override
	public void updateExportacaoQuery(QueryBuilder<Lead> query, FiltroListagem _filtro) {		
		super.updateExportacaoQuery(query, _filtro);
		query
			.select("lead.cdlead, lead.nome, lead.empresa, lead.dtretorno, leadsituacao.cdleadsituacao, listLeadsegmento.segmento, " +
					"leadsituacao.nome, listleademail.email, listleademail.cdleademail, lead.website, concorrente.nome, lead.logradouro, lead.numero, " +
					"listLeadtelefone.telefone, listLeadtelefone.cdleadtelefone, lead.complemento, lead.bairro, lead.cep, lead.municipio, municipio.uf, lead.sexo, " +
					"responsavel.cdpessoa, responsavel.nome, leadqualificacao.cdleadqualificacao, leadqualificacao.nome, lead.dtretorno, lead.proximopasso, cliente.cdpessoa ")
			.setIgnoreJoinPaths(new HashSet<String>());
	}
	
	public List<Lead> loadWithLista(String whereIn, String orderBy, boolean asc) {
		
		QueryBuilder<Lead> query = query()
					.select("lead.cdlead, lead.nome, lead.email, lead.empresa, lead.dtretorno, leadsituacao.cdleadsituacao, lead.tiporesponsavel, " +
							"leadsituacao.nome, listleademail.email, listleademail.cdleademail, " +
							"listLeadtelefone.telefone, listLeadtelefone.cdleadtelefone, telefonetipo.cdtelefonetipo, " +
							"responsavel.cdpessoa, responsavel.nome, agencia.cdpessoa, agencia.nome, leadqualificacao.cdleadqualificacao, leadqualificacao.nome, " +
							"lead.dtretorno, lead.proximopasso, cliente.cdpessoa, lead.dtultimohistorico, lead.qtdehistorico, campanha.nome, " +
							"atividadetipo.cdatividadetipo, atividadetipo.nome, listLeadhistorico.dtaltera ")
					.leftOuterJoin("lead.leadsituacao leadsituacao")
					.leftOuterJoin("lead.leadqualificacao leadqualificacao")
					.leftOuterJoin("lead.listLeadsegmento listLeadsegmento")
					.leftOuterJoin("lead.listLeadtelefone listLeadtelefone")
					.leftOuterJoin("listLeadtelefone.telefonetipo telefonetipo")
					.leftOuterJoin("lead.listLeadcampanha listLeadcampanha")
					.leftOuterJoin("listLeadcampanha.campanha campanha")
					.leftOuterJoin("lead.listleademail listleademail")
					.leftOuterJoin("lead.listLeadhistorico listLeadhistorico")
					.leftOuterJoin("listLeadhistorico.atividadetipo atividadetipo")
					.leftOuterJoin("lead.responsavel responsavel")
					.leftOuterJoin("lead.agencia agencia")
					.leftOuterJoin("lead.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("lead.cliente cliente")
					.whereIn("lead.cdlead", whereIn);
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("coalesce(to_char(lead.dtultimohistorico, 'YYYY:MM:DD'), '') desc");
		}
		return query.list();
	}
	

	@Override
	public void updateEntradaQuery(QueryBuilder<Lead> query) {
		query
			.leftOuterJoinFetch("lead.responsavel responsavel2")
			.leftOuterJoinFetch("lead.listLeadhistorico leadhistorico")
			.leftOuterJoinFetch("lead.listLeadcampanha listLeadcampanha")
			.leftOuterJoinFetch("lead.concorrente concorrente")
			.leftOuterJoinFetch("listLeadcampanha.campanha campanha")
			.leftOuterJoinFetch("campanha.responsavel responsavel")
			.orderBy("leadhistorico.dtaltera DESC");
	}
	
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				
				save.saveOrUpdateManaged("listLeadsegmento");
				save.saveOrUpdateManaged("listLeadtelefone");
				save.saveOrUpdateManaged("listleademail");
				save.saveOrUpdateManaged("listLeadcampanha");
				
				Lead lead = (Lead) save.getEntity();
				
				if (lead.getCampoExtraLeadList() != null && !lead.getCampoExtraLeadList().isEmpty())
					save.saveOrUpdateManaged("campoExtraLeadList");
				
				return null;
			}
		});
	}
	
	public List<Lead> findForReport(LeadFiltro filtro){
		if(filtro==null){
			throw new SinedException("A propriedade n�o pode ser nula");
		}
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		
		QueryBuilder<Lead> query = querySined();
			query
				.select("lead.cdlead, lead.nome, lead.empresa, leadsituacao.nome, leadsituacao.cdleadsituacao, " +
					"listleademail.email, listleademail.cdleademail, municipio.cdmunicipio, municipio.uf, " +
					"listLeadtelefone.telefone, listLeadtelefone.cdleadtelefone, telefonetipo.cdtelefonetipo, " +
					"telefonetipo.nome, responsavel.cdpessoa, responsavel.nome ")
					.leftOuterJoin("lead.leadsituacao leadsituacao")
					.leftOuterJoin("lead.leadqualificacao leadqualificacao")
					.leftOuterJoin("lead.listLeadsegmento listLeadsegmento")
					.leftOuterJoin("lead.listLeadtelefone listLeadtelefone")
					.leftOuterJoin("listLeadtelefone.telefonetipo telefonetipo")
					.leftOuterJoin("lead.listleademail listleademail")
					.leftOuterJoin("lead.listLeadhistorico listLeadhistorico")
					.leftOuterJoin("lead.listLeadcampanha listLeadcampanha")
					.leftOuterJoin("listLeadcampanha.campanha campanha")
					.leftOuterJoin("lead.responsavel responsavel")
					.leftOuterJoin("lead.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.leftOuterJoin("lead.concorrente concorrente")
					.leftOuterJoin("lead.cliente cliente")
					.leftOuterJoin("lead.listaContacrm listaContacrm")
				.whereLikeIgnoreAll("lead.nome", filtro.getNome())
				.whereLikeIgnoreAll("lead.empresa", filtro.getEmpresa())
				.where("responsavel = ?", filtro.getResponsavel())
				.where("leadqualificacao = ?", filtro.getLeadqualificacao())
				.where("leadsituacao = ?", filtro.getLeadsituacao())
				.where("listLeadsegmento.segmento = ?", filtro.getLeadSegmento() != null ? filtro.getLeadSegmento().getSegmento() : "")
				.where("municipio.uf = ?", filtro.getUf())
				.where("municipio = ?", filtro.getMunicipio())
				.where("campanha = ?", filtro.getCampanha())
				.where("concorrente = ?", filtro.getConcorrente())
				.where("lead.dtretorno >= ?", filtro.getDtinicioretorno())
				.where("lead.dtretorno <= ?", filtro.getDtfimretorno());
				
				if(filtro.getDtiniciohistorico() != null || filtro.getDtfimhistorico() != null || filtro.getAtividadetipo() != null){
					query
						.leftOuterJoin("listLeadhistorico.atividadetipo atividadetipo", true)
						.where("listLeadhistorico.dtaltera >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getDtiniciohistorico()))
						.where("listLeadhistorico.dtaltera <= ?", SinedDateUtils.dateToTimestampFinalDia(filtro.getDtfimhistorico()))
						.where("atividadetipo = ?", filtro.getAtividadetipo());
				}
			
				if (filtro.getConvertidos() != null && !filtro.getConvertidos()){
					query
					.where("cliente is null")
					.where("listaContacrm is null");
				} else if (filtro.getConvertidos() != null && filtro.getConvertidos()){
					query
					.openParentheses()
					.where("cliente is not null")
					.or()
					.where("listaContacrm is not null")
					.closeParentheses();
				}
				
				List<Leadsituacao> listaLeadsituacao = filtro.getListaLeadsituacao();
				if (listaLeadsituacao != null)
					query.whereIn("leadsituacao.cdleadsituacao", CollectionsUtil.listAndConcatenate(listaLeadsituacao,"cdleadsituacao", ","));
				
				if(SinedUtil.isRestricaoClienteVendedor(usuarioLogado)){
					query.where("responsavel.cdpessoa = ?", usuarioLogado.getCdpessoa());
				}
				
			return query.orderBy("lead.nome").list();
	}
	
	public Lead findForContacrm(Integer whereIn){
		if (whereIn == null) {
			throw new SinedException("Where in n�o pode estar vazio.");
		}
		return query()
					.select("distinct lead.cdlead, lead.nome, lead.empresa, lead.website, lead.logradouro, lead.tiporesponsavel, " +
							"lead.numero, lead.complemento, lead.bairro, lead.cep, leadsituacao.cdleadsituacao, leadsituacao.nome, " +
							"leadqualificacao.cdleadqualificacao, leadqualificacao.nome, lead.email, lead.site, lead.forcas, lead.fraquezas, lead.estrategiaMarketing, " +
							"lead.sexo, responsavel.cdpessoa, responsavel.nome, responsavel.cpf, responsavel.cnpj, agencia.nome, agencia.cdpessoa, agencia.cpf, agencia.cnpj, municipio.cdmunicipio, municipio.nome, " +
							"municipio.uf, concorrente.cdconcorrente, concorrente.nome, campanha.cdcampanha ")
						.leftOuterJoin("lead.leadsituacao leadsituacao")
						.leftOuterJoin("lead.leadqualificacao leadqualificacao")
						.leftOuterJoin("lead.responsavel responsavel")
						.leftOuterJoin("lead.agencia agencia")
						.leftOuterJoin("lead.municipio municipio")
						.leftOuterJoin("municipio.uf uf")
						.leftOuterJoin("lead.sexo sexo")
						.leftOuterJoin("lead.concorrente concorrente")
						.leftOuterJoin("lead.listLeadcampanha listLeadcampanha")
						.leftOuterJoin("listLeadcampanha.campanha campanha")
						.where("lead.cdlead = ?", whereIn)
					.unique();
	}
	
	public List<Lead> findLead(String whereIn) {
		return query()
					.select("lead.cdlead, lead.nome, lead.email, listleademail.email, listleademail.cdleademail, " +
							"responsavel.nome, responsavel.cdpessoa")
					.join("lead.responsavel responsavel")
					.leftOuterJoin("lead.listleademail listleademail")
					.whereIn("lead.cdlead", whereIn)
					.list();
	}
	
	public Lead carregaLead(String whereIn) {
		return query()
				.select("lead.cdlead, lead.nome, lead.email, listleademail.email, listleademail.cdleademail, " +
						"listleademail.contato, listleademail.email, " +
						"responsavel.nome, responsavel.cdpessoa, lead.empresa, lead.leadsituacao, " +
						"listLeadhistorico.cdleadhistorico, listLeadhistorico.dtaltera, listLeadhistorico.observacao, " +
						"listLeadhistorico.cdusuarioaltera, listLeadtelefone.telefone, telefonetipo.cdtelefonetipo, telefonetipo.nome, " +
						"lead.logradouro, lead.numero, lead.complemento, lead.bairro, lead.cep, municipio.cdmunicipio, municipio.nome, uf.cduf, uf.sigla")
					.join("lead.responsavel responsavel")
					.leftOuterJoin("lead.listleademail listleademail")
					.leftOuterJoin("lead.listLeadtelefone listLeadtelefone")
					.leftOuterJoin("listLeadtelefone.telefonetipo telefonetipo")
					.leftOuterJoin("lead.listLeadhistorico listLeadhistorico")
					.leftOuterJoin("lead.municipio municipio")
					.leftOuterJoin("municipio.uf uf")
					.whereIn("lead.cdlead", whereIn)
				.unique();
	}
	
	public List<Lead> findLeadsAutocomplete(String q) {
		return query()
					.select("lead.nome, lead.cdlead")
					.whereLikeIgnoreAll("lead.nome", q)
					.orderBy("lead.nome")
					.list();
	}
	
	/**
	 * Realiza busca geral em Lead conforme par�metro da busca.
	 * @param busca
	 * @return
	 * @author Taidson
	 * @since 04/09/2010
	 */
	@SuppressWarnings("unchecked")
	public List<Lead> findLeadForBuscaGeral(String busca) {
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		List<Lead> lista = new ListSet<Lead>(Lead.class);
		if(busca != null && !busca.trim().isEmpty()){
			busca = busca.toUpperCase();
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT l.cdlead, l.nome \n")
		    .append("FROM Lead as l \n")		    
			.append("LEFT OUTER JOIN Contacrm cc ON l.cdlead = cc.cdlead \n")
			.append("LEFT OUTER JOIN Contacrmcontato ccc ON cc.cdcontacrm = ccc.cdcontacrm \n")
			.append("LEFT OUTER JOIN Contacrmcontatoemail cce ON ccc.cdcontacrmcontato = cce.cdcontacrmcontato \n")
			.append("LEFT OUTER JOIN Leademail le ON le.cdlead = l.cdlead \n")
			.append("WHERE 1 = 1");
		    if(SinedUtil.isRestricaoClienteVendedor(usuarioLogado)){
				if(ColaboradorService.getInstance().isColaborador(usuarioLogado.getCdpessoa())){
					sql.append("AND l.cdresponsavel = " + usuarioLogado.getCdpessoa());										
				}else if(FornecedorService.getInstance().isFornecedor(usuarioLogado.getCdpessoa())){	
					sql.append("AND l.cdagencia = " + usuarioLogado.getCdpessoa());										
				}
			}
			sql.append("AND (upper(retira_acento(l.nome)) LIKE ('%'||'")
		    .append(Util.strings.tiraAcento(busca).toUpperCase())
		    .append("'||'%') OR ")
		    .append(" upper(retira_acento(l.email)) LIKE ('%'||'")
		    .append(Util.strings.tiraAcento(busca).toUpperCase())
		    .append("'||'%')) \n")
		
			.append("union \n")
		
			.append("SELECT l.cdlead, l.nome \n")
			.append("FROM Lead as l \n")
			.append("LEFT OUTER JOIN Contacrm cc ON l.cdlead = cc.cdlead \n")
			.append("LEFT OUTER JOIN Contacrmcontato ccc ON cc.cdcontacrm = ccc.cdcontacrm \n")
			.append("LEFT OUTER JOIN Contacrmcontatoemail cce ON ccc.cdcontacrmcontato = cce.cdcontacrmcontato \n")
			.append("LEFT OUTER JOIN Leademail le ON le.cdlead = l.cdlead \n")
			.append("WHERE 1 = 1");
		    if(SinedUtil.isRestricaoClienteVendedor(usuarioLogado)){
				if(ColaboradorService.getInstance().isColaborador(usuarioLogado.getCdpessoa())){
					sql.append("AND l.cdresponsavel = " + usuarioLogado.getCdpessoa());										
				}else if(FornecedorService.getInstance().isFornecedor(usuarioLogado.getCdpessoa())){	
					sql.append("AND l.cdagencia = " + usuarioLogado.getCdpessoa());										
				}
			}
			sql.append("AND upper(retira_acento(ccc.nome)) LIKE ('%'||'")
		    .append(Util.strings.tiraAcento(busca).toUpperCase())
		    .append("'||'%') \n")
			
			.append("union  \n")
			
			.append("SELECT l.cdlead, l.nome \n")
			.append("FROM Lead as l \n")
			.append("LEFT OUTER JOIN Contacrm cc ON l.cdlead = cc.cdlead \n")
			.append("LEFT OUTER JOIN Contacrmcontato ccc ON cc.cdcontacrm = ccc.cdcontacrm \n")
			.append("LEFT OUTER JOIN Contacrmcontatoemail cce ON ccc.cdcontacrmcontato = cce.cdcontacrmcontato \n")
			.append("LEFT OUTER JOIN Leademail le ON le.cdlead = l.cdlead \n")
			.append("WHERE 1 = 1");
		    if(SinedUtil.isRestricaoClienteVendedor(usuarioLogado)){
				if(ColaboradorService.getInstance().isColaborador(usuarioLogado.getCdpessoa())){
					sql.append("AND l.cdresponsavel = " + usuarioLogado.getCdpessoa());										
				}else if(FornecedorService.getInstance().isFornecedor(usuarioLogado.getCdpessoa())){	
					sql.append("AND l.cdagencia = " + usuarioLogado.getCdpessoa());										
				}
			}
			sql.append("AND upper(retira_acento(le.email)) LIKE ('%'||'")
		    .append(Util.strings.tiraAcento(busca).toUpperCase())
		    .append("'||'%') \n")
		
		    .append("union \n")
		
		    .append("SELECT l.cdlead, l.nome \n")
		    .append("FROM Lead as l \n")
		    .append("LEFT OUTER JOIN Contacrm cc ON l.cdlead = cc.cdlead \n")
		    .append("LEFT OUTER JOIN Contacrmcontato ccc ON cc.cdcontacrm = ccc.cdcontacrm \n")
		    .append("LEFT OUTER JOIN Contacrmcontatoemail cce ON ccc.cdcontacrmcontato = cce.cdcontacrmcontato \n")
		    .append("LEFT OUTER JOIN Leademail le ON le.cdlead = l.cdlead \n")
			.append("WHERE 1 = 1");
		    if(SinedUtil.isRestricaoClienteVendedor(usuarioLogado)){
				if(ColaboradorService.getInstance().isColaborador(usuarioLogado.getCdpessoa())){
					sql.append("AND l.cdresponsavel = " + usuarioLogado.getCdpessoa());										
				}else if(FornecedorService.getInstance().isFornecedor(usuarioLogado.getCdpessoa())){	
					sql.append("AND l.cdagencia = " + usuarioLogado.getCdpessoa());										
				}
			}
		    sql.append("AND upper(retira_acento(cce.email)) LIKE ('%'||'")
		    .append(Util.strings.tiraAcento(busca).toUpperCase())
		    .append("'||'%') \n")
	
		    .append("ORDER BY cdlead; \n");

			SinedUtil.markAsReader();
			lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Lead(rs.getInt(1),rs.getString(2));
				}
			});
		}
		
		return lista;
	}

	public List<Lead> findByEmail(String email) {
		return query()
					.select("lead.cdlead, lead.nome")
					.join("lead.listleademail listleademail")
					.where("listleademail.email like ?", email)
					.list();
	}
	
	/**
	* M�todo que busca Lead ordenado pela data crescente de leadhistorico
	*
	* @param whereIn
	* @return
	* @since Aug 22, 2011
	* @author Luiz Fernando F Silva
	*/
	public Lead carregaLeadOrderByDataLeadhistorico(String whereIn) {
		return query()
				.select("lead.cdlead, lead.nome, listleademail.email, listleademail.cdleademail, " +
						"responsavel.nome, responsavel.cdpessoa, lead.empresa, lead.leadsituacao, " +
						"listLeadhistorico.cdleadhistorico, listLeadhistorico.dtaltera, listLeadhistorico.observacao, " +
						"listLeadhistorico.cdusuarioaltera")
					.join("lead.responsavel responsavel")
					.leftOuterJoin("lead.listleademail listleademail")
					.leftOuterJoin("lead.listLeadhistorico listLeadhistorico")
					.whereIn("lead.cdlead", whereIn)
					.orderBy("listLeadhistorico.dtaltera")
				.unique();
	}
	
	/**
	 * 
	 * M�todo para fazer o update da situa��o do lead.
	 *
	 *@author Thiago Augusto
	 *@date 24/01/2012
	 * @param lead
	 */
	public void updateLeadSituacao(Lead lead){
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE Lead SET cdleadsituacao = ? WHERE cdlead = ?");
		getJdbcTemplate().update(sql.toString(), new Object[]{lead.getLeadsituacao().getCdleadsituacao(), lead.getCdlead()});
	}
	
	/**
	 * 
	 * M�todo para fazer o update da qualificacao do lead.
	 *
	 *@author Thiago Augusto
	 *@date 24/01/2012
	 * @param lead
	 */
	public void updateLeadQualificacao(Lead lead){
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE Lead SET cdleadqualificacao = ? WHERE cdlead = ?");
		getJdbcTemplate().update(sql.toString(), new Object[]{lead.getLeadqualificacao().getCdleadqualificacao(), lead.getCdlead()});
	}
	
	/**
	 * 
	 * M�todo para fazer o update do proximopasso do lead.
	 *
	 *@author Thiago Augusto
	 *@date 24/01/2012
	 * @param lead
	 */
	public void updateLeadProximoPasso(Lead lead){
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE Lead SET proximopasso = ? WHERE cdlead = ?");
		getJdbcTemplate().update(sql.toString(), new Object[]{lead.getProximopasso(), lead.getCdlead()});
	}
	/**
	 * 
	 * M�todo para fazer o update da dtretorno do lead.
	 *
	 *@author Thiago Augusto
	 *@date 24/01/2012
	 * @param lead
	 */
	public void updateLeadDtRetorno(Lead lead){
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE Lead SET dtretorno = ? WHERE cdlead = ?");
		getJdbcTemplate().update(sql.toString(), new Object[]{lead.getDtretorno(), lead.getCdlead()});
	}
	
	/**
	 * 
	 * M�todo que faz o update do Cliente no bean Lead.
	 *
	 * @name updateClienteLead
	 * @param lead
	 * @return void
	 * @author Thiago Augusto
	 * @date 12/06/2012
	 *
	 */
	public void updateClienteLead(Lead lead){
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE Lead SET cdpessoa = ?, dtconversao = ? WHERE cdlead = ?");
		getJdbcTemplate().update(sql.toString(), new Object[]{lead.getCliente().getCdpessoa(), lead.getDtconversao(), lead.getCdlead()});
	}
	
	/**
	 * M�todo que busca os totais por tipo de atividade e por lead
	 *
	 * @param lead
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<Leadhistorico> getTotaiAtividadetipoBylead(String whereIn){
		
		StringBuilder sql = new StringBuilder();
		
		sql
			.append("select count(*) AS QTDE, ativ.cdatividadetipo AS CDATIVIDADETIPO, ativ.nome AS NOME ")
			.append("from leadhistorico lh ")
			.append("join lead l on l.cdlead = lh.cdlead ")
			.append("join atividadetipo ativ on ativ.cdatividadetipo = lh.cdatividadetipo ");
			
		
		if(whereIn != null && !"".equals(whereIn)){
			sql.append("where l.cdlead in (" + whereIn + ") ");
		}
		
		sql.append("group by ativ.cdatividadetipo, ativ.nome");

		SinedUtil.markAsReader();
		List<Leadhistorico> list = getJdbcTemplate().query(sql.toString() ,new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				Leadhistorico leadhistorico = new Leadhistorico(
						rs.getDouble("QTDE"),
						rs.getString("NOME"));
			
				return leadhistorico;
			}
		});
		
		return list;
		
	}
	
	/**
	 * Atualiza o responsavel de um conjunto de leads
	 * @param whereInCdlead
	 * @param responsavel
	 */
	public void updateResponsavelLeads(String whereInCdlead, Colaborador responsavel) {
		String sql = "UPDATE Lead SET cdresponsavel = ? WHERE cdlead in (" + whereInCdlead + ");";
		getJdbcTemplate().update(sql.toString(), new Object[]{responsavel.getCdpessoa()});
	}
	
	/**
	 * M�todo que carrega os Leads selecionados na tela de listagem para desassocia��o de uma campanha.
	 * 
	 * @param whereIn
	 * @return
	 * @throws Exception
	 * @author Rafael Salvio
	 */
	public List<Lead> findAllForDesassociarCampanha(String whereIn) throws Exception{
		if(whereIn == null || whereIn.trim().isEmpty())
			throw new Exception("Par�metro inv�lido");
		
		return query()
					.select("lead.cdlead, listLeadcampanha.cdcampanhalead, campanha.cdcampanha, campanha.nome")
					.leftOuterJoin("lead.listLeadcampanha listLeadcampanha")
					.leftOuterJoin("listLeadcampanha.campanha campanha")
					.whereIn("lead.cdlead", whereIn)
					.list();
	}
	
	/**
	 * 
	 * @param empresa
	 * @author Thiago Clemente
	 */
	public List<Lead> findForBuscarEmpresa(String empresa) {
		return query()
				.select("lead.cdlead, lead.nome, lead.empresa")
				.where("coalesce(lead.empresa,'') <> ''")
				.whereLikeIgnoreAll("lead.empresa", empresa)
				.orderBy("lead.empresa, lead.nome")
				.list();
	}

	/**
	 * M�todo que busca os leads da listagem
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Lead> findForCSV(LeadFiltro filtro) {
		QueryBuilder<Lead> query = querySined();
		this.updateListagemQuery(query, filtro);
		query.setUseTranslator(true);
		
		query
			.select(
				"lead.cdlead, lead.nome, lead.empresa, lead.dtretorno, lead.email, " +
				"lead.proximopasso, lead.dtultimohistorico, lead.qtdehistorico, " +
				"leadsituacao.nome, leadsituacao.cdleadsituacao," +
				"listleademail.email, listleademail.cdleademail, " +
				"responsavel.cdpessoa, responsavel.nome, " +
				"leadqualificacao.cdleadqualificacao, leadqualificacao.nome, " +
				"cliente.cdpessoa, " +
				"campanha.nome, " +
				"atividadetipo.cdatividadetipo, atividadetipo.nome,  " +
				"listLeadtelefone.telefone, listLeadtelefone.cdleadtelefone, " +
				"telefonetipo.cdtelefonetipo, telefonetipo.nome, " + 
				"listLeadhistorico.dtaltera, listLeadhistorico.cdusuarioaltera, listLeadhistorico.observacao, " +
				"atividadetipo.cdatividadetipo, atividadetipo.nome, " +
				"segmento.nome, segmento.cdsegmento ")
				
			.leftOuterJoinIfNotExists(new String[]{
				"lead.listleademail listleademail", 
				"lead.listLeadhistorico listLeadhistorico", 
				"listLeadhistorico.atividadetipo atividadetipo", 
				"lead.listLeadsegmento listLeadsegmento", 
				"listLeadsegmento.segmento segmento", 
				"lead.municipio municipio", 
				"lead.listLeadtelefone listLeadtelefone", 
				"listLeadtelefone.telefonetipo telefonetipo", 
				"lead.listLeadcampanha listLeadcampanha", 
				"listLeadcampanha.campanha campanha", 
				"lead.cliente cliente",
			})
			.setIgnoreJoinPaths(new HashSet<String>());
		
		return query.list();
	}
	
	public Lead findLeadForOportunidade(String whereIn){
		return query()
		.select("lead.cdlead, lead.nome, lead.tiporesponsavel, lead.empresa, agencia.nome, agencia.cdpessoa, " +
				"responsavel.nome, responsavel.cdpessoa, campanha.cdcampanha, campanha.nome,  lead.site, " +
				"lead.forcas, lead.fraquezas, lead.estrategiaMarketing, concorrente.cdconcorrente, concorrente.nome ")
		.leftOuterJoin("lead.responsavel responsavel")
		.leftOuterJoin("lead.agencia agencia")
		.leftOuterJoin("lead.concorrente concorrente")
		.leftOuterJoin("lead.listLeadcampanha listLeadcampanha")
		.leftOuterJoin("listLeadcampanha.campanha campanha")
		.whereIn("lead.cdlead", whereIn)
		.unique();
	}
	
	public List<Lead> findWithResponsavel(String whereIn){
		return query()
					.select("lead.cdlead, lead.nome, responsavel.cdpessoa, responsavel.nome")
					.leftOuterJoin("lead.responsavel responsavel")
					.whereIn("lead.cdlead", whereIn)
					.list();
	}

	public void updateDataConversaoLead(Lead lead){
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE Lead SET dtconversao = ? WHERE cdlead = ?");
		getJdbcTemplate().update(sql.toString(), new Object[]{new Date(), lead.getCdlead()});
	}
}
