package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.PropostaCaixaItem;
import br.com.linkcom.sined.geral.bean.Propostacaixa;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PropostaCaixaItemDAO extends GenericDAO<PropostaCaixaItem>{

	/**M�todo que busca a lista de itens referente a Caixa de Proposta.
	 * @author Thiago Augusto
	 * @param item
	 * @return
	 */
	public List<PropostaCaixaItem> findByItem(Propostacaixa item){
		if(item == null  || item.getCdpropostacaixa() == null){
			  throw new SinedException("Objeto n�o pode ser nulo em SolicitacaoservicotipoitemDAO.");
		  }
		return query()
			.select("propostaCaixaItem.nome, propostaCaixaItem.cdpropostacaixaitem, " +
					"propostaCaixaItem.obrigatorio, propostaCaixaItem.conteudopadrao")
			.where("propostaCaixaItem.caixa = ?", item)
			.orderBy("propostaCaixaItem.nome")
			.list();
	}
	
	/**
	 * 
	 * M�todo que busca todos os Itens Caixa de Proposta passada.
	 *
	 *@author Thiago Augusto
	 *@date 02/02/2012
	 * @param bean
	 * @return
	 */
	public List<PropostaCaixaItem> findListByPropostaCaixa(Propostacaixa bean){
		return query()
			.select("propostaCaixaItem.cdpropostacaixaitem, propostaCaixaItem.nome, propostaCaixaItem.conteudopadrao")
			.leftOuterJoin("propostaCaixaItem.caixa  caixa")
			.where("caixa = ?", bean)
			.list();
	}
}
