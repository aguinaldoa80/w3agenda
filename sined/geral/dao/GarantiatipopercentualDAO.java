package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.sined.geral.bean.Garantiatipo;
import br.com.linkcom.sined.geral.bean.Garantiatipopercentual;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("garantiatipopercentual.percentual")
public class GarantiatipopercentualDAO extends GenericDAO<Garantiatipopercentual>{
	
	public List<Garantiatipopercentual> findForW3Producao(String whereIn){
		return query()
				.select("garantiatipopercentual.cdgarantiatipopercentual, garantiatipopercentual.percentual, garantiatipo.cdgarantiatipo")
				.join("garantiatipopercentual.garantiatipo garantiatipo")
				.whereIn("garantiatipopercentual.cdgarantiatipopercentual", whereIn)
				.list();
	}
	
	public List<Garantiatipopercentual> findByGarantiatipo(Garantiatipo garantiatipo){
		return query()
				.select("garantiatipopercentual.cdgarantiatipopercentual, garantiatipopercentual.percentual, garantiatipo.cdgarantiatipo")
				.join("garantiatipopercentual.garantiatipo garantiatipo")
				.where("garantiatipopercentual.garantiatipo=?", garantiatipo)
				.orderBy("garantiatipopercentual.percentual")
				.list();
	}
	
}
