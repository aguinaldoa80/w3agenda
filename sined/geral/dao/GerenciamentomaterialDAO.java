package br.com.linkcom.sined.geral.dao;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.view.Vgerenciarmaterial;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.TotalizadorGerenciamentoMaterial;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.GerenciamentomaterialFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.rest.w3producao.estoque.EstoqueW3producaoRESTWSBean;

public class GerenciamentomaterialDAO extends GenericDAO<Vgerenciarmaterial> {
	
	private MaterialService materialService;
	private MaterialcategoriaService materialcategoriaService;
	private ParametrogeralService parametrogeralService;
	
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {
		this.materialcategoriaService = materialcategoriaService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Vgerenciarmaterial> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		GerenciamentomaterialFiltro filtro = (GerenciamentomaterialFiltro) _filtro;
		filtro.setForceCountAllFields(true);
		
		StringBuilder whereQtde = new StringBuilder();
		boolean exibirQtdereservada = filtro.getEmpresa() != null;
		if(Boolean.TRUE.equals(filtro.getFromSelecionarLoteestoque())){
			filtro.setAgruparporlote(true);
		}
		String selectLote = Boolean.TRUE.equals(filtro.getAgruparporlote()) ? ",loteestoque.cdloteestoque, loteestoque.numero, aux_loteestoque.validade" : "";
		
		if(exibirQtdereservada){
			query
			.select("new Vgerenciarmaterial(vgerenciarmaterial.cdmaterial, vgerenciarmaterial.cdmaterialgrupo, vgerenciarmaterial.nome, vgerenciarmaterial.identificacao, " +
					"vgerenciarmaterial.cdmaterialclasse, sum(vgerenciarmaterial.qtdedisponivel), " +
					"vgerenciarmaterial.valorcustomaterial, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome " +
					selectLote +
					", sum(qtdereservada(vgerenciarmaterial.cdmaterial, localarmazenagem.cdlocalarmazenagem, empresa.cdpessoa, loteestoque.cdloteestoque, false)), vgerenciarmaterial.pesobruto,unidadeMedida.cdunidademedida,unidadeMedida.simbolo," +
							"vgerenciarmaterial.largura,vgerenciarmaterial.altura,vgerenciarmaterial.comprimento )");
		}else {
			query
			.select("new Vgerenciarmaterial(vgerenciarmaterial.cdmaterial, vgerenciarmaterial.cdmaterialgrupo, vgerenciarmaterial.nome, vgerenciarmaterial.identificacao, " +
					"vgerenciarmaterial.cdmaterialclasse, sum(vgerenciarmaterial.qtdedisponivel), " +
					"vgerenciarmaterial.valorcustomaterial, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome " +
					selectLote + ", vgerenciarmaterial.pesobruto,unidadeMedida.cdunidademedida,unidadeMedida.simbolo," +
							"vgerenciarmaterial.largura,vgerenciarmaterial.altura,vgerenciarmaterial.comprimento) ");
		}
		
		query
			.leftOuterJoin("vgerenciarmaterial.localarmazenagem localarmazenagem")
			.leftOuterJoin("vgerenciarmaterial.loteestoque loteestoque")
			.leftOuterJoin("loteestoque.aux_loteestoque aux_loteestoque")
			.leftOuterJoin("vgerenciarmaterial.unidadeMedida unidadeMedida")
			.leftOuterJoin("vgerenciarmaterial.projeto projeto")
			.leftOuterJoin("projeto.projetotipo projetotipo")
			.leftOuterJoin("vgerenciarmaterial.empresa empresa")
			.setUseTranslator(false);
		
			if (filtro.getProjeto() != null){
				query.where("projeto = ?", filtro.getProjeto());
			}
			query.where("vgerenciarmaterial.largura >= ?",filtro.getLarguraDe());
			query.where("vgerenciarmaterial.altura >= ?",filtro.getAlturaDe());
			query.where("vgerenciarmaterial.comprimento >= ?",filtro.getComprimentoDe());
			query.where("vgerenciarmaterial.largura <= ?",filtro.getLarguraAte());
			query.where("vgerenciarmaterial.altura <= ?",filtro.getAlturaAte());
			query.where("vgerenciarmaterial.comprimento <= ?",filtro.getComprimentoAte());
			
			
			if(filtro.getProjetotipo() != null){
				query.where("projetotipo = ?", filtro.getProjetotipo());
			}
			
			String projetos = SinedUtil.getListaProjeto();
			if(projetos != null && !projetos.trim().equals("")){
				query.whereIn("projeto.cdprojeto", projetos);
			}
		
			if(filtro.getMinimo()!=null){
				//query.where("vgerenciarmaterial.qtdedisponivel >= ?", filtro.getMinimo());
				whereQtde.append(" SUM(vgerenciarmaterial.qtdedisponivel) >= " + filtro.getMinimo() + "");
			}
			if(filtro.getMaximo()!=null){
//				query.where("vgerenciarmaterial.qtdedisponivel <= ?", filtro.getMaximo());
				whereQtde.append(!"".equals(whereQtde.toString()) ? " and " : "");
				whereQtde.append(" SUM(vgerenciarmaterial.qtdedisponivel) <= " + filtro.getMaximo() + "");
			}
			if(filtro.getAbaixominimo()){
//				query.where("vgerenciarmaterial.qtdedisponivel < (select produto.qtdeminima " +
//																	" from Produto produto " +
//																	" where produto.cdmaterial = vgerenciarmaterial.cdmaterial) ");
				whereQtde.append(!"".equals(whereQtde.toString()) ? " and " : "");
				whereQtde.append("( SUM(vgerenciarmaterial.qtdedisponivel) < (select produto.qtdeminima " +
																	" from Produto produto " +
																	" where produto.cdmaterial = vgerenciarmaterial.cdmaterial) " +
								 " or " +
								 " SUM(vgerenciarmaterial.qtdedisponivel) < ( " +
					 					" select min(mla.quantidademinima) " +
										" from Materiallocalarmazenagem mla " +
										" where mla.material.cdmaterial = vgerenciarmaterial.cdmaterial " +
										(filtro.getLocalarmazenagem() != null ? 
											" and mla.localarmazenagem.cdlocalarmazenagem = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem() : "") +
										" group by mla.material.cdmaterial ) ) ");
			}
			if(filtro.getSomentematerialinativo() != null && filtro.getSomentematerialinativo()){
				query.where("vgerenciarmaterial.cdmaterial in " +
					  "(select m.cdmaterial from Material m " +
					  "where m.ativo = false)");
			}
			if(filtro.getSomentematerialativo() != null && filtro.getSomentematerialativo()){
				query.where("vgerenciarmaterial.cdmaterial in " +
					  "(select m.cdmaterial from Material m " +
					  "where m.ativo = true)");
			}
			if(filtro.getMaterialreserva() != null && filtro.getMaterialreserva()){
				query.where("qtdereservada(vgerenciarmaterial.cdmaterial, localarmazenagem.cdlocalarmazenagem, empresa.cdpessoa, loteestoque.cdloteestoque) > 0");
			}
			query.where("exists (SELECT m.cdmaterial FROM Material m join m.localizacaoestoque le " +
					"WHERE m.cdmaterial=vgerenciarmaterial.cdmaterial AND le=?)", filtro.getLocalizacaoestoque());
			
			if(filtro.getMaterialcategoria() != null){
				materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
				query.where("exists (SELECT m.cdmaterial FROM Material m join m.materialcategoria mc " +
						"join mc.vmaterialcategoria vm where vgerenciarmaterial.cdmaterial= m.cdmaterial and (vm.identificador like ? || '.%' or vm.identificador like ?))",
						new Object[]{filtro.getMaterialcategoria().getIdentificador(), filtro.getMaterialcategoria().getIdentificador()});
			}
			if(filtro.getMaterial() != null){
				if(filtro.getFiltroItemGrade() != null && filtro.getFiltroItemGrade() && 
						filtro.getWhereInMaterialItemGrade() != null &&
						!"".equals(filtro.getWhereInMaterialItemGrade())){
					
					SinedUtil.quebraWhereIn("vgerenciarmaterial.cdmaterial", filtro.getWhereInMaterialItemGrade(), query);
				}else {
					query.where("vgerenciarmaterial.cdmaterial = ?", filtro.getMaterial().getCdmaterial());
				}
			} 
			
			if(filtro.getMaterialgrupo() != null){
				query.where("vgerenciarmaterial.cdmaterialgrupo = ?", filtro.getMaterialgrupo().getCdmaterialgrupo());
			}
		
			if(filtro.getLocalarmazenagem() != null){
				query.where("localarmazenagem = ?", filtro.getLocalarmazenagem());
			}else if(StringUtils.isNotEmpty(filtro.getWhereInLocalRestricaoUsuario())){
				query.openParentheses()
					.whereIn("localarmazenagem.cdlocalarmazenagem", filtro.getWhereInLocalRestricaoUsuario())
					.or()
					.where("localarmazenagem is null")
					.closeParentheses();
				
				if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null){
					query.where("exists (select 1 from Localarmazenagem la " +
							"left outer join la.listaempresa le " +
							"left outer join le.empresa e " +
							"where la.cdlocalarmazenagem = localarmazenagem.cdlocalarmazenagem " +
							"and (e.cdpessoa in (" + filtro.getEmpresa().getCdpessoa().toString() + ") or le is null) )");
				}
			}
			
			if(filtro.getLoteestoque() != null)
				query.where("loteestoque = ?", filtro.getLoteestoque());
			
//			if(filtro.getEmpresa() != null){
//				query
//					.where("vgerenciarmaterial.cdmaterial in " +
//						   "(select m.cdmaterial from Materialempresa me " +
//						   "join me.material m " +
//						   "join me.empresa e " +
//						   "where e.cdpessoa = "+filtro.getEmpresa().getCdpessoa()+")");
//			}
			
//			if(filtro.getEmpresa() != null){
//				query
//					.leftOuterJoin("localarmazenagem.listaempresa localarmazenagemempresa")
//					.openParentheses()
//					.where("localarmazenagemempresa.empresa = ?", filtro.getEmpresa())
//					.or()
//					.where("localarmazenagemempresa.localarmazenagem is null")
//					.closeParentheses();
//			}
			if (filtro.getEmpresa() != null){
				query.openParentheses()
						.where("empresa = ?", filtro.getEmpresa())
						.or()
						.where("vgerenciarmaterial.empresa is null")
					.closeParentheses();
			}else if(StringUtils.isNotEmpty(filtro.getWhereInEmpresaRestricaoUsuario())){
				query.openParentheses()
					.whereIn("empresa.cdpessoa", filtro.getWhereInEmpresaRestricaoUsuario())
					.or()
					.openParentheses()
						.where("empresa is null")
						.openParentheses()
							.where("localarmazenagem is null")
							.or()
							.whereIn("localarmazenagem.cdlocalarmazenagem", StringUtils.isNotEmpty(filtro.getWhereInLocalRestricaoUsuario()) ? 
									filtro.getWhereInLocalRestricaoUsuario() : 
									(filtro.getLocalarmazenagem() != null ? filtro.getLocalarmazenagem().getCdlocalarmazenagem().toString() : null))
						.closeParentheses()
					.closeParentheses()
				.closeParentheses();
			}
			
			if(filtro.getInventario() != null && filtro.getInventario().getCdinventario() != null){
				query.
					where("vgerenciarmaterial.cdmaterial in " +
						  "(select m.cdmaterial from Inventario i " +
						  "join i.listaInventariomaterial im " +
						  "join im.material m " +
						  "where i.cdinventario = " + filtro.getInventario().getCdinventario() + ")");
			}
			
			if(filtro.getIndicadorpropriedade() != null){
				query.
				where("vgerenciarmaterial.cdmaterial in " +
					  "(select m.cdmaterial from Inventario i " +
					  "join i.listaInventariomaterial im " +
					  "join im.material m " +
					  "where im.indicadorpropriedade = " + filtro.getIndicadorpropriedade().getValue() + ")");
			}
			
			if(filtro.getFornecedor() != null && filtro.getFornecedor().getCdpessoa() != null){
				query.
				where("vgerenciarmaterial.cdmaterial in " +
					  "(select m.cdmaterial from Materialfornecedor mf " +
					  "join mf.material m " +
					  "join mf.fornecedor f " +
					  "where f.cdpessoa = " + filtro.getFornecedor().getCdpessoa() + ")");
			}
		
			if (filtro.getContagerencial() != null){
				query.
				where("vgerenciarmaterial.cdmaterial in " +
					  "(select m.cdmaterial from Material m " +
					  "join m.contagerencial cg  " +
					  "where cg.cdcontagerencial = " + filtro.getContagerencial().getCdcontagerencial() + ")");
			}
			
			if (filtro.getCaracteristica() != null && !"".equals(filtro.getCaracteristica())){
				String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
				query.
				where("vgerenciarmaterial.cdmaterial in " +
					  "(select m.cdmaterial from Material m " +
					  "join m.listaCaracteristica c " +
					  "where UPPER(" + (funcaoTiraacento != null ? funcaoTiraacento + "(c.nome))" : "c.nome )" ) + " like  '%' || '" + Util.strings.tiraAcento(filtro.getCaracteristica()).toUpperCase() + "' || '%') ");
			}
			
			if (filtro.getMaterialtipo() != null && filtro.getMaterialtipo().getCdmaterialtipo() != null){
				query.
				where("vgerenciarmaterial.cdmaterial in " +
					  "(select m.cdmaterial from Material m " +
					  "join m.materialtipo mt " +
					  "where mt.cdmaterialtipo = " + filtro.getMaterialtipo().getCdmaterialtipo() + ") ");
			}
			
			if(filtro.getSemplaquetas() != null && filtro.getSemplaquetas()){
				query
					.where("vgerenciarmaterial.cdmaterialclasse = ?", Materialclasse.PATRIMONIO.getCdmaterialclasse())
					.where("vgerenciarmaterial.cdmaterial in " +
						   "(select b.cdmaterial from Patrimonioitem p " +
						   "join p.bempatrimonio b " +
						   "where p.plaqueta is null or " +
						   "p.plaqueta = '' and " +
						   "p.ativo = "+Boolean.TRUE+") and " +
						   "localarmazenagem.cdlocalarmazenagem in " +
						   "(select l.cdlocalarmazenagem from Movpatrimonio m " +
						   "join m.localarmazenagem l " +
						   "join m.patrimonioitem p " +
						   "where p.plaqueta is null or " +
						   "p.plaqueta = '' and " +
						   "p.ativo = "+Boolean.TRUE+")");
			} else if(filtro.getListaclasses() != null){
				query.whereIn("vgerenciarmaterial.cdmaterialclasse", CollectionsUtil.listAndConcatenate(filtro.getListaclasses(), "cdmaterialclasse",","));
			} 
			
			if(filtro.getPlaqueta() != null && !filtro.getPlaqueta().equals("")){
				query
					.where("vgerenciarmaterial.cdmaterialclasse = ?", Materialclasse.PATRIMONIO.getCdmaterialclasse())
					.where("vgerenciarmaterial.cdmaterial in " +
						   "(select b.cdmaterial from Patrimonioitem p " +
						   "join p.bempatrimonio b " +
						   "where upper(p.plaqueta) like upper('%"+filtro.getPlaqueta()+"%') and " +
						   "p.ativo = "+Boolean.TRUE+")");
			}
			if(filtro.getFromSelecionarLoteestoque() != null && filtro.getFromSelecionarLoteestoque()){
				query
					.where("vgerenciarmaterial.loteestoque is not null");
			}
			String having = Boolean.TRUE.equals(filtro.getAgruparporlote())? " having sum(vgerenciarmaterial.qtdedisponivel) > 0": "";
			
			if(!"".equals(whereQtde.toString())){
				query
				.groupBy("vgerenciarmaterial.cdmaterial, vgerenciarmaterial.cdmaterialgrupo, vgerenciarmaterial.nome, vgerenciarmaterial.identificacao,unidadeMedida.cdunidademedida, " +
						"vgerenciarmaterial.cdmaterialclasse, vgerenciarmaterial.valorcustomaterial, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome,vgerenciarmaterial.largura,vgerenciarmaterial.altura,vgerenciarmaterial.comprimento " +
						(Boolean.TRUE.equals(filtro.getAgruparporlote()) ? ", loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, aux_loteestoque.validade" : "") + 
						(exibirQtdereservada ? ", empresa.cdpessoa" : "")+ ", vgerenciarmaterial.pesobruto", whereQtde.toString()+having)
				;
			}else {
				query
				.groupBy("vgerenciarmaterial.cdmaterial, vgerenciarmaterial.cdmaterialgrupo, vgerenciarmaterial.nome, vgerenciarmaterial.identificacao,unidadeMedida.cdunidademedida, " +
						"vgerenciarmaterial.cdmaterialclasse, vgerenciarmaterial.valorcustomaterial,unidadeMedida.cdunidademedida, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome " +
						(Boolean.TRUE.equals(filtro.getAgruparporlote()) ? ",loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, aux_loteestoque.validade" : "") +
						(exibirQtdereservada ? ", empresa.cdpessoa" : "") + ", vgerenciarmaterial.pesobruto,vgerenciarmaterial.largura,vgerenciarmaterial.altura,vgerenciarmaterial.comprimento"+having)
						;
			}
			
			if(Boolean.TRUE.equals(filtro.getAgruparporlote())){
				query.orderBy("aux_loteestoque.validade");
			}
	}

	public List<Vgerenciarmaterial> findForReport(GerenciamentomaterialFiltro filtro) {
		filtro.setFiltroItemGrade(false);
		if(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null && 
				materialService.isControleMaterialitemgrade(filtro.getMaterial())){
			List<Material> listaItemGrade = materialService.findMaterialitemByMaterialmestregrade(filtro.getMaterial());
			if(listaItemGrade != null && !listaItemGrade.isEmpty()){
				filtro.setWhereInMaterialItemGrade(CollectionsUtil.listAndConcatenate(listaItemGrade, "cdmaterial", ","));
				filtro.setFiltroItemGrade(true);
			}
		}
		
		QueryBuilder<Vgerenciarmaterial> query = querySined();
		this.updateListagemQuery(query, filtro);
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Vgerenciarmaterial> findForReportPeriodo(GerenciamentomaterialFiltro filtro, boolean pdf) {
		filtro.setFiltroItemGrade(false);
		if(filtro.getMaterial() != null && 
				filtro.getMaterial().getCdmaterial() != null && 
				materialService.isControleMaterialitemgrade(filtro.getMaterial())){
			List<Material> listaItemGrade = materialService.findMaterialitemByMaterialmestregrade(filtro.getMaterial());
			if(listaItemGrade != null && !listaItemGrade.isEmpty()){
				filtro.setWhereInMaterialItemGrade(CollectionsUtil.listAndConcatenate(listaItemGrade, "cdmaterial", ","));
				filtro.setFiltroItemGrade(true);
			}
		}
		
		StringBuilder sql = new StringBuilder();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy"); 
		
		Boolean produto = null;
		Boolean epi = null;
		Boolean patrimonio = null;
		
		if(filtro.getListaclasses() != null && !filtro.getListaclasses().isEmpty()){
			for(Materialclasse mc : filtro.getListaclasses()){
				if(Materialclasse.PRODUTO.equals(mc)) produto = true;
				else if(Materialclasse.PATRIMONIO.equals(mc)) patrimonio = true;
				else if(Materialclasse.EPI.equals(mc)) epi = true;
			}
		}
		
		sql
		.append("	SELECT query.cdmaterial, query.identificacao, query.cdmaterialgrupo, ")
		.append("	query.cdlocalarmazenagem, query.cdloteestoque, query.nome, ")
		.append("	(sum(query.qtdeentrada) - sum(query.qtdesaida)) AS qtdedisponivel, ")
		.append("	query.tempoentrega, query.valorcusto AS valorcustomaterial, query.cdmaterialclasse, " + (pdf ? "query.dtmovimentacao, " : ""))
		.append("	query.localnome AS localnome, query.pesobruto ")
		.append("FROM ( ")
		
		.append("	SELECT ")
		.append("	CASE WHEN ma.cdmaterialmestregrade IS NOT NULL AND COALESCE(mg.gradeestoquetipo, 0) = 0 THEN magrade.cdmaterial ELSE ma.cdmaterial END AS cdmaterial, ")
		.append("	CASE WHEN ma.cdmaterialmestregrade IS NOT NULL AND COALESCE(mg.gradeestoquetipo, 0) = 0 THEN magrade.identificacao ELSE ma.identificacao END AS identificacao, ")
		.append("	CASE WHEN ma.cdmaterialmestregrade IS NOT NULL AND COALESCE(mg.gradeestoquetipo, 0) = 0 THEN magrade.cdmaterialgrupo ELSE ma.cdmaterialgrupo END AS cdmaterialgrupo, ")
		.append("	me.cdlocalarmazenagem, ")
		.append("	me.cdloteestoque, ")
		.append("	CASE WHEN ma.cdmaterialmestregrade IS NOT NULL AND COALESCE(mg.gradeestoquetipo, 0) = 0 THEN magrade.nome ELSE ma.nome END AS nome, ")
		.append("	COALESCE(sum(me.qtde), (0)::numeric) AS qtdeentrada, ")
		.append("	0 AS qtdesaida, ")
		.append("	CASE WHEN ma.cdmaterialmestregrade IS NOT NULL AND COALESCE(mg.gradeestoquetipo, 0) = 0 THEN magrade.tempoentrega ELSE ma.tempoentrega END AS tempoentrega, ")
		.append("	CASE WHEN ma.cdmaterialmestregrade IS NOT NULL AND COALESCE(mg.gradeestoquetipo, 0) = 0 THEN COALESCE(magrade.valorcusto, 0::numeric) * 100::numeric ELSE COALESCE(ma.valorcusto, 0::numeric) * 100::numeric END AS valorcusto, ")
		.append("	me.cdmaterialclasse, ") 
		.append(pdf ? "me.dtmovimentacao, " : "")
		.append("	local1.nome AS localnome, ma.pesobruto ")
				
		.append("	FROM movimentacaoestoque me ")
		.append("	JOIN material ma ON ma.cdmaterial = me.cdmaterial ")
		.append("	LEFT OUTER JOIN materialgrupo mg ON mg.cdmaterialgrupo = ma.cdmaterialgrupo ")
		.append("	LEFT OUTER JOIN localarmazenagem local1 ON local1.cdlocalarmazenagem = me.cdlocalarmazenagem ")
		.append("	LEFT OUTER JOIN material magrade ON magrade.cdmaterial = ma.cdmaterialmestregrade ")
		.append("	WHERE ((me.cdmovimentacaoestoquetipo = 1) AND (me.dtcancelamento IS NULL)) ");
		
		if(filtro.getDtmovimentacaoInicio() != null || filtro.getDtmovimentacaoFim() != null){
			if(filtro.getDtmovimentacaoInicio() != null){
				sql.append(" and me.dtmovimentacao >= to_date('"+ format.format(filtro.getDtmovimentacaoInicio())+ "', 'DD/MM/YYYY')");
			}
			if(filtro.getDtmovimentacaoFim() != null){
				sql.append(" and me.dtmovimentacao <= to_date('"+ format.format(filtro.getDtmovimentacaoFim())+ "', 'DD/MM/YYYY')");
			}
		}
		
		addWhereResumo(filtro, sql);
		
		if(filtro.getWhereInMaterialItemGrade() != null && !filtro.getWhereInMaterialItemGrade().trim().equals("")){
			sql.append(" and ma.cdmaterial in (" +  filtro.getWhereInMaterialItemGrade() + ") ");
		}else if(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null){
			sql.append(" and me.cdmaterial = " +  filtro.getMaterial().getCdmaterial() + " ");
		}else if(filtro.getWhereInMaterialInventario() != null && !"".equals(filtro.getWhereInMaterialInventario())){
			sql.append(" and me.cdmaterial in (" +  filtro.getWhereInMaterialInventario() + ") ");
		}
		if(filtro.getMaterialtipo() != null){
			sql.append(" and ma.cdmaterialtipo = " +  filtro.getMaterialtipo().getCdmaterialtipo() + " ");
		}
		if(Util.objects.isPersistent(filtro.getLocalarmazenagem()))
			sql.append(" and me.cdlocalarmazenagem = " +  filtro.getLocalarmazenagem().getCdlocalarmazenagem() + " ");
		
		if(Util.objects.isPersistent(filtro.getProjeto())){
			sql.append(" and me.cdprojeto = ").append(filtro.getProjeto().getCdprojeto()).append(" ");
		}
		if(Util.objects.isPersistent(filtro.getEmpresa())){
			sql.append(" and me.cdempresa = ").append(filtro.getEmpresa().getCdpessoa()).append(" ");
		}
		if(Util.objects.isPersistent(filtro.getFornecedor())){
			sql.append(" and exists(select 1 from materialfornecedor mafornecedor on mafornecedor.cdmaterial = ma.cdmatrial and mafornecedor.cdpessoa = ").append(filtro.getFornecedor().getCdpessoa()).append(") ");
		}
		
		String projetos = SinedUtil.getListaProjeto();
		if(projetos != null && !projetos.trim().equals("")){
			sql.append(" and me.cdprojeto in (").append(projetos).append(") ");
		}
		
		sql
		.append("	GROUP BY ma.cdmaterial, me.cdmaterialclasse, me.cdlocalarmazenagem, ")
		.append("		me.cdloteestoque, ma.cdmaterialgrupo, ma.nome, ma.tempoentrega, ")
		.append("		ma.valorcusto, ma.identificacao, " + (pdf ? " me.dtmovimentacao, " : "") + " local1.nome, mg.gradeestoquetipo, magrade.cdmaterial, ma.pesobruto ")
		
		.append(" UNION ALL  ")
		
		.append("	SELECT ")
		.append("	CASE WHEN ma.cdmaterialmestregrade IS NOT NULL AND COALESCE(mg.gradeestoquetipo, 0) = 0 THEN magrade.cdmaterial ELSE ma.cdmaterial END AS cdmaterial, ")
		.append("	CASE WHEN ma.cdmaterialmestregrade IS NOT NULL AND COALESCE(mg.gradeestoquetipo, 0) = 0 THEN magrade.identificacao ELSE ma.identificacao END AS identificacao, ")
		.append("	CASE WHEN ma.cdmaterialmestregrade IS NOT NULL AND COALESCE(mg.gradeestoquetipo, 0) = 0 THEN magrade.cdmaterialgrupo ELSE ma.cdmaterialgrupo END AS cdmaterialgrupo, ")
		.append("	me.cdlocalarmazenagem, ")
		.append("	me.cdloteestoque, ")
		.append("	CASE WHEN ma.cdmaterialmestregrade IS NOT NULL AND COALESCE(mg.gradeestoquetipo, 0) = 0 THEN magrade.nome ELSE ma.nome END AS nome, ")
		.append("	0 AS qtdeentrada, ")
		.append("	COALESCE(sum(me.qtde), (0)::numeric) AS qtdesaida, ")
		.append("	CASE WHEN ma.cdmaterialmestregrade IS NOT NULL AND COALESCE(mg.gradeestoquetipo, 0) = 0 THEN magrade.tempoentrega ELSE ma.tempoentrega END AS tempoentrega, ")
		.append("	CASE WHEN ma.cdmaterialmestregrade IS NOT NULL AND COALESCE(mg.gradeestoquetipo, 0) = 0 THEN COALESCE(magrade.valorcusto, 0::numeric) * 100::numeric ELSE COALESCE(ma.valorcusto, 0::numeric) * 100::numeric END AS valorcusto, ")
		.append("	me.cdmaterialclasse, ") 
		.append(pdf ? "me.dtmovimentacao, " : "")
		.append("	local1.nome AS localnome, ma.pesobruto ")
		
		.append("	FROM movimentacaoestoque me ")
		.append("	JOIN material ma ON ma.cdmaterial = me.cdmaterial ")
		.append("	LEFT OUTER JOIN materialgrupo mg ON mg.cdmaterialgrupo = ma.cdmaterialgrupo ")
		.append("	LEFT OUTER JOIN localarmazenagem local1 ON local1.cdlocalarmazenagem = me.cdlocalarmazenagem ")
		.append("	LEFT OUTER JOIN material magrade ON magrade.cdmaterial = ma.cdmaterialmestregrade ")
		.append("	WHERE ((me.cdmovimentacaoestoquetipo = 2) AND (me.dtcancelamento IS NULL)) ");
		
		if(filtro.getDtmovimentacaoInicio() != null || filtro.getDtmovimentacaoFim() != null){
			if(filtro.getDtmovimentacaoInicio() != null){
				sql.append(" and me.dtmovimentacao >= to_date('"+ format.format(filtro.getDtmovimentacaoInicio())+ "', 'DD/MM/YYYY') ");
			}
			if(filtro.getDtmovimentacaoFim() != null){
				sql.append(" and me.dtmovimentacao <= to_date('"+ format.format(filtro.getDtmovimentacaoFim())+ "', 'DD/MM/YYYY') ");
			}
		}
		addWhereResumo(filtro, sql);
		
		if(filtro.getWhereInMaterialItemGrade() != null && !filtro.getWhereInMaterialItemGrade().trim().equals("")){
			sql.append(" and ma.cdmaterial in (" +  filtro.getWhereInMaterialItemGrade() + ") ");
		}else if(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null){
			sql.append(" and me.cdmaterial = " +  filtro.getMaterial().getCdmaterial() + " ");
		}else if(filtro.getWhereInMaterialInventario() != null && !"".equals(filtro.getWhereInMaterialInventario())){
			sql.append(" and me.cdmaterial in (" +  filtro.getWhereInMaterialInventario() + ") ");
		}	
		if(filtro.getMaterialtipo() != null){
			sql.append(" and ma.cdmaterialtipo = " +  filtro.getMaterialtipo().getCdmaterialtipo() + " ");
		}
		if(filtro.getLocalarmazenagem() != null && filtro.getLocalarmazenagem().getCdlocalarmazenagem() != null)
			sql.append(" and me.cdlocalarmazenagem = " +  filtro.getLocalarmazenagem().getCdlocalarmazenagem() + " ");
		
		if(filtro.getProjeto() != null && filtro.getProjeto().getCdprojeto() != null){
			sql.append(" and me.cdprojeto = ").append(filtro.getProjeto().getCdprojeto()).append(" ");
		}
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null){
			sql.append(" and me.cdempresa = ").append(filtro.getEmpresa().getCdpessoa()).append(" ");
		}
		
		if(projetos != null && !projetos.trim().equals("")){
			sql.append(" and me.cdprojeto in (").append(projetos).append(") ");
		}
		
		sql
		.append("	GROUP BY ma.cdmaterial, me.cdmaterialclasse, me.cdlocalarmazenagem, ")
		.append("		me.cdloteestoque, ma.cdmaterialgrupo, ma.nome, ma.tempoentrega, ")
		.append("		ma.valorcusto, ma.identificacao, " + (pdf ? "me.dtmovimentacao, " : "") + " local1.nome, mg.gradeestoquetipo, magrade.cdmaterial, ma.pesobruto ")
		.append(") query ")
		.append("GROUP BY query.cdmaterial, query.cdmaterialgrupo, query.cdlocalarmazenagem, ")
		.append("	query.cdloteestoque, query.nome, query.tempoentrega, query.valorcusto, ")
		.append("	query.cdmaterialclasse, query.identificacao, " + (pdf ? "query.dtmovimentacao, " : "") + " query.localnome, query.pesobruto ");
		
		if(patrimonio != null && patrimonio || (((produto == null || !produto) && (epi == null || !epi)) && (patrimonio == null || !patrimonio))){
			sql.append(" UNION ALL  ")
			
			
			.append("SELECT ma.cdmaterial, ma.identificacao, ma.cdmaterialgrupo, ")
			.append("	m1.cdlocalarmazenagem, NULL::\"unknown\" AS cdloteestoque, ma.nome, ")
			.append("	count(m1.cdlocalarmazenagem) AS qtdedisponivel, ma.tempoentrega, ")
			.append("	(COALESCE(ma.valorcusto, (0)::numeric) * (100)::numeric) AS ")
			.append("	valorcustomaterial, 2 AS cdmaterialclasse, " + (pdf ? "m1.dtmovimentacao, " : "") + " local1.nome AS localnome, ma.pesobruto ")
			.append("FROM ((movpatrimonio m1 JOIN patrimonioitem pi ON ((pi.cdpatrimonioitem = ")
			.append("	m1.cdpatrimonioitem))) JOIN material ma ON ((ma.cdmaterial = pi.cdbempatrimonio)) " )
			.append("	LEFT OUTER JOIN localarmazenagem local1 ON ((local1.cdlocalarmazenagem = m1.cdlocalarmazenagem))) ")
			.append("	WHERE ((m1.cdmovpatrimonio = ( ")
			.append("	SELECT max(m2.cdmovpatrimonio) AS max ")
			.append("	FROM movpatrimonio m2 ")
			.append("	WHERE (m2.cdpatrimonioitem = m1.cdpatrimonioitem) ")
			.append("	GROUP BY m2.cdpatrimonioitem ")
			.append("	)) AND (m1.dtcancelamento IS NULL)) ");
			
			if(filtro.getDtmovimentacaoInicio() != null || filtro.getDtmovimentacaoFim() != null){
				if(filtro.getDtmovimentacaoInicio() != null){
					sql.append(" and m1.dtmovimentacao >= to_date('"+ format.format(filtro.getDtmovimentacaoInicio())+ "', 'DD/MM/YYYY') ");
				}
				if(filtro.getDtmovimentacaoFim() != null){
					sql.append(" and m1.dtmovimentacao <= to_date('"+ format.format(filtro.getDtmovimentacaoFim())+ "', 'DD/MM/YYYY') ");
				}
			}
			
			if(filtro.getWhereInMaterialItemGrade() != null && !filtro.getWhereInMaterialItemGrade().trim().equals("")){
				sql.append(" and ma.cdmaterial in (" +  filtro.getWhereInMaterialItemGrade() + ") ");
			}else if(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null){
				sql.append(" and ma.cdmaterial = " +  filtro.getMaterial().getCdmaterial() + " ");
			}else if(filtro.getWhereInMaterialInventario() != null && !"".equals(filtro.getWhereInMaterialInventario())){
				sql.append(" and ma.cdmaterial in (" +  filtro.getWhereInMaterialInventario() + ") ");
			}
		
			if(filtro.getLocalarmazenagem() != null && filtro.getLocalarmazenagem().getCdlocalarmazenagem() != null)
				sql.append(" and m1.cdlocalarmazenagem = " +  filtro.getLocalarmazenagem().getCdlocalarmazenagem() + " ");
			
			sql
			.append("GROUP BY ma.cdmaterial, ma.cdmaterialgrupo, m1.cdlocalarmazenagem, ma.nome, ")
			.append("	ma.tempoentrega, ma.valorcusto, ma.identificacao, " + (pdf ? "m1.dtmovimentacao, " : "") + " local1.nome, ma.pesobruto ");
		}
		
		sql.append(pdf ? "ORDER BY dtmovimentacao " : "");
		
		if(filtro.getMinimo() != null || filtro.getMaximo() != null){
			StringBuilder query2 = new StringBuilder(" SELECT * FROM ( " + sql.toString() + " ) query2 ");
			StringBuilder whereQtde = new StringBuilder();
			if(filtro.getMinimo()!=null){
				whereQtde.append("where qtdedisponivel >= " + filtro.getMinimo());
			}
			if(filtro.getMaximo()!=null){
				if(whereQtde.toString().equals("")) whereQtde.append(" where ");
				else whereQtde.append(" and  ");
				whereQtde.append(" qtdedisponivel <= " + filtro.getMaximo());
			}
			query2.append(whereQtde); 
			
			sql = query2;
		}
		
		List<Vgerenciarmaterial> lista = new ArrayList<Vgerenciarmaterial>();
		if(pdf){
			SinedUtil.markAsReader();
			lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
				public Vgerenciarmaterial mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Vgerenciarmaterial(rs.getInt("cdmaterial"), 
							rs.getInt("cdmaterialgrupo"),
							rs.getString("nome"),
							rs.getInt("cdmaterialclasse"),
							rs.getDouble("qtdedisponivel"),
							rs.getDouble("valorcustomaterial"),
							rs.getInt("cdlocalarmazenagem"),
							rs.getString("localnome"),
							rs.getDate("dtmovimentacao"),
							rs.getDouble("pesobruto"));
					}
			});
		}else {
			SinedUtil.markAsReader();
			lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
				public Vgerenciarmaterial mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Vgerenciarmaterial(rs.getInt("cdmaterial"), 
							rs.getInt("cdmaterialgrupo"),
							rs.getString("nome"),
							rs.getInt("cdmaterialclasse"),
							rs.getDouble("qtdedisponivel"),
							rs.getDouble("valorcustomaterial"),
							rs.getInt("cdlocalarmazenagem"),
							rs.getString("localnome"),
							rs.getDouble("pesobruto"));
					}
			});
		}
		
		return lista;
		
	}
	
	private void addWhereResumo(GerenciamentomaterialFiltro filtro, StringBuilder sql) {
		if(filtro.getListaclasses() != null && sql != null){
			Boolean produto = null;
			Boolean epi = null;
			Boolean patrimonio = null;
			Boolean servico = null;
			for(Materialclasse mc : filtro.getListaclasses()){
				if(Materialclasse.PRODUTO.equals(mc)) produto = true;
				else if(Materialclasse.PATRIMONIO.equals(mc)) patrimonio = true;
				else if(Materialclasse.EPI.equals(mc)) epi = true;
				else if(Materialclasse.SERVICO.equals(mc)) servico = true;
			}
			StringBuilder whereClasse = new StringBuilder(" and ( ");
			boolean addOrClasse = false;
			if(produto != null && produto){
				whereClasse.append(" me.cdmaterialclasse = " + Materialclasse.PRODUTO.getCdmaterialclasse() + " ");
				addOrClasse = true;
			}
			if(epi != null && epi){
				whereClasse.append((addOrClasse ? " or " : " " ) + " me.cdmaterialclasse = " + Materialclasse.EPI.getCdmaterialclasse() + " ");
				addOrClasse = true;
			}
			if(patrimonio != null && patrimonio){
				whereClasse.append((addOrClasse ? " or " : " " ) + " me.cdmaterialclasse = " + Materialclasse.PATRIMONIO.getCdmaterialclasse() + " ");
				addOrClasse = true;
			}
			if(servico != null && servico){
				whereClasse.append((addOrClasse ? " or " : " " ) + " me.cdmaterialclasse = " + Materialclasse.SERVICO.getCdmaterialclasse() + " ");
				addOrClasse = true;
			}
			whereClasse.append(" ) ");
			sql.append(whereClasse);
		} 
		
		if(filtro.getMaterialgrupo() != null){
			sql.append(" and mg.cdmaterialgrupo = ").append(filtro.getMaterialgrupo().getCdmaterialgrupo()).append(" ");
		}
		if(Boolean.TRUE.equals(filtro.getSomentematerialativo())){
			sql.append(" and coalesce(ma.ativo, false) = true ");
		}else if(Boolean.TRUE.equals(filtro.getSomentematerialinativo())){
			sql.append(" and coalesce(ma.ativo, false) = false ");
		}
	}

	/**
	 * M�todo que calcula o total geral
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public TotalizadorGerenciamentoMaterial findForTotalGeral(GerenciamentomaterialFiltro filtro){
		
		QueryBuilder<Vgerenciarmaterial> query = query();
		this.updateListagemQuery(query, filtro);
		
		List<Vgerenciarmaterial> lista = query.list();
		
		BigDecimal qtdetotal = new BigDecimal(0);
		Money valottotal = new Money();
		BigDecimal pesototal = new BigDecimal(0);
		BigDecimal pesobrutototal = new BigDecimal(0);
		
		if(lista != null && lista.size() > 0){
			String whereInMat = CollectionsUtil.listAndConcatenate(lista, "cdmaterial", ",");
			List<Material> listaMaterial = materialService.findForCsvGerenciamentoMaterial(whereInMat);
			
			for (Vgerenciarmaterial v : lista) {
				Material mat = new Material(v.getCdmaterial());
				
				if(!listaMaterial.contains(mat))
					continue;
				
				mat = listaMaterial.get(listaMaterial.indexOf(mat));
				
				BigDecimal qtde = new BigDecimal(v.getQtdedisponivel());
				Double peso = mat.getPeso() != null ? mat.getPeso() : 0d;
				Double pesobruto = mat.getPesobruto() != null ? mat.getPesobruto() : 0d;
				Double valorcusto = mat.getValorcusto() != null ? mat.getValorcusto() : 0d;
				
				qtdetotal = qtdetotal.add(qtde);
				pesototal = pesototal.add(qtde.multiply(new BigDecimal(peso)));
				pesobrutototal = pesobrutototal.add(qtde.multiply(new BigDecimal(pesobruto)));
				valottotal = valottotal.add(new Money(qtde).multiply(new Money(valorcusto)));
			}
		}
		
		TotalizadorGerenciamentoMaterial totalizadorGerenciamentoMaterial = new TotalizadorGerenciamentoMaterial();
		
		totalizadorGerenciamentoMaterial.setPesobrutototal(pesobrutototal);
		totalizadorGerenciamentoMaterial.setPesototal(pesototal);
		totalizadorGerenciamentoMaterial.setQtdetotal(qtdetotal);
		totalizadorGerenciamentoMaterial.setValottotal(valottotal);
		
//		if(filtro == null) throw new SinedException("Par�metro inv�lido.");
//		
//		String select = "select SUM(query.qtde) as qtdetotal, SUM(query.valor) as valortotal, SUM(query.pesoMaterial) as pesototal, SUM(query.pesobrutoMaterial) as pesobrutototal " +
//						"from ( " +
//						"select v.cdmaterial, m.peso, m.pesobruto, v.cdlocalarmazenagem, v.cdloteestoque, v.cdempresa, " +
//						"v.qtdedisponivel as qtde, " +
//						"v.qtdedisponivel * COALESCE(v.valorcustomaterial, 0) as valor, " +
//						"v.qtdedisponivel * COALESCE(m.peso, 0) as pesoMaterial, " +
//						"v.qtdedisponivel * COALESCE(m.pesobruto, 0) as pesobrutoMaterial ";
//		
//		StringBuilder sql = new StringBuilder();
//		StringBuilder where = new StringBuilder();
//		sql
//			.append(select)
//			.append("from Vgerenciarmaterial v ")
//			.append("left outer join material m on m.cdmaterial = v.cdmaterial ");
//
//		if(filtro.getMaterial() != null){
//			where.append("where v.cdmaterial = " + filtro.getMaterial().getCdmaterial());
//		} 
//		
//		if(filtro.getMaterialgrupo() != null){
//			where.append(!"".equals(where.toString()) ? " and " : " where ");
//			where.append(" v.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo());
//		}
//	
//		if(filtro.getLocalarmazenagem() != null){
//			where.append(!"".equals(where.toString()) ? " and " : " where ");
//			where.append(" v.cdlocalarmazenagem = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem());
//		}
//
//		if (filtro.getProjeto() != null){
//			where.append(!"".equals(where.toString()) ? " and " : " where ");
//			where.append(" v.cdmaterial in " +
//					  "(select m.cdmaterial from Movimentacaoestoque me " +
//					  "join material m on m.cdmaterial = m.cdmaterial " +
//					  "join projeto p on p.cdprojeto = me.cdprojeto " +
//					  "where p.cdprojeto = " + filtro.getProjeto().getCdprojeto() + ")");
//		}
//		
//		if (filtro.getCaracteristica() != null && !"".equals(filtro.getCaracteristica())){
//			where.append(!"".equals(where.toString()) ? " and " : " where ");
//			where.append(" v.cdmaterial in " +
//				  "(select m.cdmaterial from Material m " +
//				  "join materialcaracteristica mc on mc.cdmaterial = m.cdmaterial " +
//				  "where mc.nome like  '%' || '" + filtro.getCaracteristica() + "' || '%') ");
//		}
//		
//		if (filtro.getMaterialtipo() != null && filtro.getMaterialtipo().getCdmaterialtipo() != null){
//			where.append(!"".equals(where.toString()) ? " and " : " where ");
//			where.append(" v.cdmaterial in " +
//				  "(select m.cdmaterial from Material m " +
//				  "join materialtipo mt on mt.cdmaterialtipo = m.cdmaterialtipo " +
//				  "where mt.cdmaterialtipo = " + filtro.getMaterialtipo().getCdmaterialtipo() + ") ");
//		}
//	
//		if(filtro.getEmpresa() != null){
//			where.append(!"".equals(where.toString()) ? " and " : " where ");
//			where.append(" v.cdempresa = " + filtro.getEmpresa().getCdpessoa() + " ");
//		}
//		
//		if(filtro.getInventario() != null && filtro.getInventario().getCdinventario() != null){
//			where.append(!"".equals(where.toString()) ? " and " : " where ");
//			where.append(" v.cdmaterial in " +
//						"(select m.cdmaterial from Inventario i " +
//						"join inventariomaterial im on im.cdinventario = i.cdinventario " +
//						"join material m on m.cdmaterial = im.cdmaterial " +
//						"where i.cdinventario = " + filtro.getInventario().getCdinventario() + ") ");
//		}
//		
//		if(filtro.getIndicadorpropriedade() != null){
//			where.append(!"".equals(where.toString()) ? " and " : " where ");
//			where.append(" v.cdmaterial in " +
//						  "(select m.cdmaterial from Inventario i " +
//						  "join inventariomaterial im on im.cdinventario = i.cdinventario " +
//						  "join material m on m.cdmaterial = im.cdmaterial " +
//						  "where i.indicadorpropriedade = " + filtro.getIndicadorpropriedade().getValue() + ")");
//		}
//		
//		if(filtro.getSemplaquetas() != null && filtro.getSemplaquetas()){
//			where.append(!"".equals(where.toString()) ? " and " : " where ");
//			where.append(" v.cdmaterialclasse = " + Materialclasse.PATRIMONIO.getCdmaterialclasse());
//			
//			where.append(" and v.cdmaterial in " +
//				   "(select b.cdmaterial from Patrimonioitem p " +
//				   "join bempatrimonio b on b.cdmaterial = p.cdbempatrimonio " +
//				   "where p.plaqueta is null or " +
//				   "p.plaqueta = '' and " +
//				   "p.ativo = "+Boolean.TRUE+") and " +
//				   "la.cdlocalarmazenagem in " +
//				   "(select l.cdlocalarmazenagem from Movpatrimonio m " +
//				   "join localarmazenagem l on l.cdlocalarmazenagem = m.cdlocalarmazenagem " +
//				   "join patrimonioitem p on p.cdpatrimonioitem = m.cdpatrimonioitem " +
//				   "where p.plaqueta is null or " +
//				   "p.plaqueta = '' and " +
//				   "p.ativo = "+Boolean.TRUE+") ");
//		} else if(filtro.getListaclasses() != null){
//			where.append(!"".equals(where.toString()) ? " and " : " where ");
//			where.append(" v.cdmaterialclasse in (" + CollectionsUtil.listAndConcatenate(filtro.getListaclasses(), "cdmaterialclasse",",") + ") ");
//		} 
//		
//		if(filtro.getFornecedor() != null && filtro.getFornecedor().getCdpessoa() != null){
//			where.append(!"".equals(where.toString()) ? " and " : " where ");
//			where.append(" v.cdmaterial in " +
//				  "(select m.cdmaterial from Materialfornecedor mf " +
//				  "join material m on m.cdmaterial = mf.cdmaterial " +
//				  "where mf.cdpessoa = " + filtro.getFornecedor().getCdpessoa() + ") ");
//		}
//		
//		if(filtro.getMaterialreserva() != null && filtro.getMaterialreserva()){
//			where.append(!"".equals(where.toString()) ? " and " : " where ");
//			where.append(" v.cdmaterial in " +
//					  "(select pvm.cdmaterial from Pedidovenda pv " +
//					  "join pedidovendatipo pvtipo on pvtipo.cdpedidovendatipo = pv.cdpedidovendatipo " +
//					  "join pedidovendamaterial pvm on pvm.cdpedidovenda = pv.cdpedidovenda " +
//					  "where pv.pedidovendasituacao <> " + Pedidovendasituacao.CANCELADO.getValue() +
//					  " and pv.pedidovendasituacao <> " + Pedidovendasituacao.CONFIRMADO.getValue() +
//					  " and pvtipo.reserva = true) ");
//		}
//		
//		if(filtro.getMinimo()!=null){
//			where.append(!"".equals(where.toString()) ? " and " : " where ");
//			where.append(" v.qtdedisponivel >= " + filtro.getMinimo() + "");
//		}
//		if(filtro.getMaximo()!=null){
//			where.append(!"".equals(where.toString()) ? " and " : " where ");
//			where.append(" v.qtdedisponivel <= " + filtro.getMaximo() + "");
//		}
//		if(filtro.getAbaixominimo()){
//			where.append(!"".equals(where.toString()) ? " and " : " where ");
//			where.append(" v.qtdedisponivel < (select produto.qtdeminima " +
//												" from Produto produto " +
//												" where produto.cdmaterial = v.cdmaterial)");
//		}
//		
//		if(!"".equals(where.toString()))
//			sql.append(where.toString());
//		
//		sql.append(" ) as query ");
//		
//		System.out.println("QUERY: " + sql.toString());
//		List<TotalizadorGerenciamentoMaterial> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
//			public TotalizadorGerenciamentoMaterial mapRow(ResultSet rs, int rowNum)throws SQLException {
//				TotalizadorGerenciamentoMaterial total = new TotalizadorGerenciamentoMaterial();
//				BigDecimal qtdetotal = rs.getBigDecimal("qtdetotal");
//				BigDecimal valortotal = rs.getBigDecimal("valortotal");
//				BigDecimal pesototal = rs.getBigDecimal("pesototal");
//				BigDecimal pesobrutototal = rs.getBigDecimal("pesobrutototal");
//				
//				total.setQtdetotal(qtdetotal != null ? qtdetotal : new BigDecimal(0));
//				total.setValottotal(valortotal != null ? new Money(valortotal.longValue(), true) : new Money());
//				total.setPesototal(pesototal != null ? new BigDecimal(SinedUtil.roundByParametro(pesototal.doubleValue())) : new BigDecimal(0));
//				total.setPesobrutototal(pesobrutototal != null ? new BigDecimal(SinedUtil.roundByParametro(pesobrutototal.doubleValue())) : new BigDecimal(0));
//				return total;
//			}
//		});
	
//		return lista != null && lista.size() > 0 ? lista.get(0) : new TotalizadorGerenciamentoMaterial();
	
		return totalizadorGerenciamentoMaterial;
	}	
	
	/**
	 * M�todo que retorna a qtde reservada
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public Double getQtdeTotalReservada(GerenciamentomaterialFiltro filtro){
		if(filtro == null)
			throw new SinedException("Filtro n�o pode ser nulo");
		
		String whereInCdmaterial = whereInCdMaterial(filtro, filtro.getMaterial());
		if(whereInCdmaterial != null && "".equals(whereInCdmaterial)){ //se o whereIn for vazio, significa que n�o deve ser exibido nenhum resultado.
			return 0d;
		}
		
		
		StringBuilder sql = new StringBuilder();  
		
		sql.append("SELECT SUM(RESERVADO) AS RESERVADO FROM ( ");
		sql.append(" SELECT SUM(DADOSRESERVA.RESERVADO) AS RESERVADO, DADOSRESERVA.CDEMPRESA, DADOSRESERVA.CDMATERIAL, ");
		sql.append(" 		DADOSRESERVA.CDLOCALARMAZENAGEM, DADOSRESERVA.CDLOTEESTOQUE, DADOSRESERVA.CDPROJETO, VGERENCIARMATERIAL.qtdedisponivel ");
		sql.append(" FROM( ");
		
		sql.append(" SELECT SUM(R.quantidade) AS RESERVADO, R.CDMATERIAL, R.CDEMPRESA, R.CDLOCALARMAZENAGEM, R.CDLOTEESTOQUE, R.CDPROJETO " +
					" FROM RESERVA R " +
					" JOIN MATERIAL M ON M.CDMATERIAL = R.CDMATERIAL " +
					" LEFT OUTER JOIN LOCALARMAZENAGEM L ON L.CDLOCALARMAZENAGEM = R.CDLOCALARMAZENAGEM ");
		
		if(Util.strings.isNotEmpty(filtro.getPlaqueta()) || Boolean.TRUE.equals(filtro.getSemplaquetas())){
			sql.append(" join bempatrimonio on bempatrimonio.cdmaterial = r.cdmaterial ");
			sql.append(" join patrimonioitem on bempatrimonio.cdmaterial = patrimonioitem.cdbempatrimonio ");
		}
		
		sql.append(" WHERE (L.CDLOCALARMAZENAGEM IS NULL OR L.ATIVO = TRUE) ");
		
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null){
			sql.append(" and R.CDEMPRESA = " + filtro.getEmpresa().getCdpessoa() + " ");
		}else if(StringUtils.isNotEmpty(filtro.getWhereInEmpresaRestricaoUsuario())){
			sql.append(" and (R.CDEMPRESA IN(" + filtro.getWhereInEmpresaRestricaoUsuario() + ") OR ( ");
			sql.append(" R.CDEMPRESA IS NULL AND (R.CDLOCALARMAZENAGEM IS NULL ");
			if(StringUtils.isNotEmpty(filtro.getWhereInLocalRestricaoUsuario())){
				sql.append(" OR R.CDLOCALARMAZENAGEM IN ("+filtro.getWhereInLocalRestricaoUsuario()+")");
			}
			sql.append(" )))");
		}
		
		if(filtro.getLocalarmazenagem() != null && filtro.getLocalarmazenagem().getCdlocalarmazenagem() != null){
			sql.append(" and R.CDLOCALARMAZENAGEM = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem() + " ");
		}else if(StringUtils.isNotEmpty(filtro.getWhereInLocalRestricaoUsuario())){
			
			sql.append(" and (R.CDLOCALARMAZENAGEM IS NULL OR R.CDLOCALARMAZENAGEM IN("+filtro.getWhereInLocalRestricaoUsuario()+"))");
			
			if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null){
				sql.append(" AND EXISTS (select 1 from Localarmazenagem la " +
						"left outer join LOCALARMAZENAGEMEMPRESA LE ON R.CDLOCALARMAZENAGEM = LA.CDLOCALARMAZENAGEM " +
						"left outer join EMPRESA E ON E.CDPESSOA = LE.CDEMPRESA " +
						"where la.cdlocalarmazenagem = R.cdlocalarmazenagem " +
						"and (e.cdpessoa in (" + filtro.getEmpresa().getCdpessoa().toString() + ") or e.cdpessoa is null) )");
			}
		}
		if(filtro.getMaterialgrupo() != null && filtro.getMaterialgrupo().getCdmaterialgrupo() != null){
			sql.append(" and M.CDMATERIALGRUPO = " + filtro.getMaterialgrupo().getCdmaterialgrupo() + " ");
		}
		
		
		if(Util.strings.isNotEmpty(whereInCdmaterial)){
			sql.append(" and M.cdmaterial in ("+whereInCdmaterial+")");
		}else if(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null){
			sql.append(" and M.CDMATERIAL = " + filtro.getMaterial().getCdmaterial() + " ");
		}
		
		if(filtro.getMaterialtipo() != null && filtro.getMaterialtipo().getCdmaterialtipo() != null){
			sql.append(" and M.CDMATERIALTIPO = " + filtro.getMaterialtipo().getCdmaterialtipo() + " ");
		}
		if(filtro.getLoteestoque() != null && filtro.getLoteestoque().getCdloteestoque() != null){
			sql.append(" and R.CDLOTEESTOQUE = " + filtro.getLoteestoque().getCdloteestoque() + " ");
		}
		
		if (SinedUtil.isListNotEmpty(filtro.getListaclasses())){
			sql.append(" and ( 1 = 0 or ");
			for(Materialclasse materialclasse : filtro.getListaclasses()){
				if(Materialclasse.PRODUTO.equals(materialclasse)) sql.append(" COALESCE(M.PRODUTO, FALSE) = TRUE ");
				if(Materialclasse.EPI.equals(materialclasse)) sql.append(" COALESCE(M.EPI, FALSE) = TRUE ");
				if(Materialclasse.SERVICO.equals(materialclasse)) sql.append(" COALESCE(M.SERVICO, FALSE) = TRUE ");
				if(Materialclasse.PATRIMONIO.equals(materialclasse)) sql.append(" COALESCE(M.PATRIMONIO, FALSE) = TRUE ");
			}
			sql.append(" ) ");
		}
		
		if(filtro.getSemplaquetas() != null && filtro.getSemplaquetas()){
			sql.append(" and  coalesce(patrimonioitem.plaqueta, '') = '' and patrimonioitem.ativo = true");
			sql.append(" and exists(select 1 from movpatrimonio ");
			sql.append(" 			  join localarmazenagem on movpatrimonio.cdlocalarmazenagem = localarmazenagem.cdlocalarmazenagem ");
			sql.append(" 			 where patrimonioitem.cdpatrimonioitem = movpatrimonio.cdpatrimonioitem ");
			sql.append(" 			   and localarmazenagem.cdlocalarmazenagem = r.cdlocalarmazenagem) ");
		}
		
		if(filtro.getPlaqueta() != null && !filtro.getPlaqueta().equals("")){
			sql.append(" and upper(coalesce(patrimonioitem.plaqueta, '')) = like upper('%"+filtro.getPlaqueta()+"%') and patrimonioitem.ativo = true");
		}
		sql.append(" GROUP BY R.CDMATERIAL, R.CDEMPRESA, R.CDLOCALARMAZENAGEM, R.CDLOTEESTOQUE, R.CDPROJETO) DADOSRESERVA ");
		
		sql.append("  LEFT OUTER JOIN VGERENCIARMATERIAL ON VGERENCIARMATERIAL.CDMATERIAL = DADOSRESERVA.CDMATERIAL ");
		sql.append(" 									AND VGERENCIARMATERIAL.CDLOCALARMAZENAGEM = DADOSRESERVA.CDLOCALARMAZENAGEM ");
		sql.append(" 									AND VGERENCIARMATERIAL.CDEMPRESA = DADOSRESERVA.CDEMPRESA ");
		sql.append("                                    AND VGERENCIARMATERIAL.CDPROJETO = DADOSRESERVA.CDPROJETO ");
		sql.append(" 									AND COALESCE(DADOSRESERVA.CDLOTEESTOQUE, -1) = COALESCE(VGERENCIARMATERIAL.cdloteestoque, -1) ");
		sql.append(" WHERE 1 = 1 ");
		String having = " HAVING 1 = 1 ";
		if(filtro.getMinimo()!=null){
			having += " AND SUM(vgerenciarmaterial.qtdedisponivel) >= " + filtro.getMinimo() + "";
		}
		if(filtro.getMaximo()!=null){
			having += " AND SUM(vgerenciarmaterial.qtdedisponivel) <= " + filtro.getMinimo() + "";
		}
		
		if(filtro.getAbaixominimo()){
			having += " and ( SUM(vgerenciarmaterial.qtdedisponivel) < (select produto.qtdeminima " +
																" from produto " +
																" where produto.cdmaterial = DADOSRESERVA.cdmaterial) " +
							 " or " +
							 " SUM(vgerenciarmaterial.qtdedisponivel) < ( " +
				 					" select min(mla.quantidademinima) " +
									" from Materiallocalarmazenagem mla " +
									" where mla.cdmaterial = DADOSRESERVA.cdmaterial " +
									(filtro.getLocalarmazenagem() != null ? 
										" and mla.cdlocalarmazenagem = " + filtro.getLocalarmazenagem().getCdlocalarmazenagem() : "") +
									" ) ) ";
		}
		sql.append(" GROUP BY DADOSRESERVA.CDEMPRESA, DADOSRESERVA.CDMATERIAL, DADOSRESERVA.CDLOCALARMAZENAGEM, DADOSRESERVA.CDLOTEESTOQUE, DADOSRESERVA.CDPROJETO, VGERENCIARMATERIAL.qtdedisponivel ");
		sql.append(having);
		
		
		sql.append(" ) QUERY ");
		
		SinedUtil.markAsReader();
		List<Double> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getDouble("RESERVADO");
			}
		});
		
		if(lista != null && lista.size() > 0){
			return lista.get(0);
		} else {
			return null;
		}
	}
	
	/**
	 * Busca as informa��es para a listagem do CSV.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Vgerenciarmaterial> findForCsv(GerenciamentomaterialFiltro filtro){
		filtro.setFiltroItemGrade(false);
		if(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null && 
				materialService.isControleMaterialitemgrade(filtro.getMaterial())){
			List<Material> listaItemGrade = materialService.findMaterialitemByMaterialmestregrade(filtro.getMaterial());
			if(listaItemGrade != null && !listaItemGrade.isEmpty()){
				filtro.setWhereInMaterialItemGrade(CollectionsUtil.listAndConcatenate(listaItemGrade, "cdmaterial", ","));
				filtro.setFiltroItemGrade(true);
			}
		}
		
		QueryBuilder<Vgerenciarmaterial> query = querySined();
		updateListagemQuery(query, filtro);
		return query.list();
	}

	public List<Vgerenciarmaterial> findForSolicitacaocompra(String whereIngerenciamentomaterial) {
		if(whereIngerenciamentomaterial == null || whereIngerenciamentomaterial.isEmpty()) {
			throw new SinedException("Par�metro inv�lido.");
		}
		return query()
				.whereIn("vgerenciarmaterial.cdmaterial", whereIngerenciamentomaterial)
				.list();
	}
	
	/**
	 * 
	 * Busca os dados do material para alimentar o pop-up de sele��o de local de origem de equipamento no cadastro de colaborador
	 * 
	 * @param material
	 * @return
	 * @since 25/07/2014
	 * @author Rafael Patr�cio
	 */
	public List<Vgerenciarmaterial> findForCadastroColaborador(Material material){
		if(material == null || material.getCdmaterial() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
					.select("new Vgerenciarmaterial(vgerenciarmaterial.cdmaterial, vgerenciarmaterial.nome, " +
							"SUM(vgerenciarmaterial.qtdedisponivel), localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome)")
					.setUseTranslator(false)
					.join("vgerenciarmaterial.localarmazenagem localarmazenagem")
					.where("vgerenciarmaterial.cdmaterial = ?", material.getCdmaterial())
					.groupBy("vgerenciarmaterial.cdmaterial, vgerenciarmaterial.nome, " +
							"vgerenciarmaterial.nome, localarmazenagem.cdlocalarmazenagem, " +
							"localarmazenagem.nome ")
					.list();
	}
	
	/**
	* M�todo que retorna o material com a qtde disponivel at� uma determinada data
	*
	* @param whereIn
	* @param dtinicio
	* @return
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public List<Material> findForDefinirCustoInicial(String whereIn, Date dtinicio, Date dtfim, Movimentacaoestoque movimentacaoestoque, Empresa empresa) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido.");

		StringBuilder sql = new StringBuilder();
		
		sql
		.append("SELECT query.cdmaterial, ")
		.append("	(sum(query.qtdeentrada) - sum(query.qtdesaida)) AS qtdedisponivel ")
		.append("FROM ( ")
		.append("	SELECT me.cdmaterial, COALESCE(sum(me.qtde), (0)::numeric) AS qtdeentrada, 0 AS qtdesaida ")
		.append("	FROM movimentacaoestoque me ")
		.append("	WHERE ((me.cdmovimentacaoestoquetipo = 1) AND (me.dtcancelamento IS NULL)) ")
		.append("	and me.cdmaterial in (" +  whereIn + ") ");
		
		if(empresa != null) {
			sql.append(" and me.cdempresa = " + empresa.getCdpessoa() + " ");
		}
		
		if(dtinicio != null){
			sql.append(" and me.dtmovimentacao >= to_date('"+ new SimpleDateFormat("dd/MM/yyyy").format(dtinicio)+ "', 'DD/MM/YYYY') ");
		}
		if(dtfim != null){
			sql.append(" and me.dtmovimentacao <= to_date('"+ new SimpleDateFormat("dd/MM/yyyy").format(dtfim)+ "', 'DD/MM/YYYY') ");
		}
		if(movimentacaoestoque != null && movimentacaoestoque.getCdmovimentacaoestoque() != null){
			sql.append(" and me.cdmovimentacaoestoque <= " + movimentacaoestoque.getCdmovimentacaoestoque() + " ");
		}
		
		sql
		.append("	GROUP BY me.cdmaterial ")
		
		.append(" UNION ALL  ")
		
		.append("	SELECT me.cdmaterial, 0 AS qtdeentrada, COALESCE(sum(me.qtde), (0)::numeric) AS qtdesaida ")
		.append("	FROM movimentacaoestoque me ")
		.append("	WHERE ((me.cdmovimentacaoestoquetipo = 2) AND (me.dtcancelamento IS NULL)) ")
		.append("	and me.cdmaterial in (" +  whereIn + ") ");
		
		if(empresa != null) {
			sql.append(" and me.cdempresa = " + empresa.getCdpessoa() + " ");
		}
		if(dtinicio != null){
			sql.append(" and me.dtmovimentacao >= to_date('"+ new SimpleDateFormat("dd/MM/yyyy").format(dtinicio)+ "', 'DD/MM/YYYY') ");
		}
		if(dtfim != null){
			sql.append(" and me.dtmovimentacao <= to_date('"+ new SimpleDateFormat("dd/MM/yyyy").format(dtfim)+ "', 'DD/MM/YYYY') ");
		}
		if(movimentacaoestoque != null && movimentacaoestoque.getCdmovimentacaoestoque() != null){
			sql.append(" and me.cdmovimentacaoestoque <= " + movimentacaoestoque.getCdmovimentacaoestoque() + " ");
		}
		
		sql
		.append("	GROUP BY me.cdmaterial")
		.append(") query ")
		.append("GROUP BY query.cdmaterial ");

//		SinedUtil.markAsReader();
		List<Material> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Material mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Material(rs.getInt("cdmaterial"), rs.getDouble("qtdedisponivel"));
				}
		});
		return lista;
	}
	
	/**
	* M�todo que verifica se existe quantidade diferente de zero de algum material para um determinado local de armazenagem 
	*
	* @param localarmazenagem
	* @return
	* @since 18/11/2016
	* @author Luiz Fernando
	*/
	public boolean existeQtdeEmEstoque(Localarmazenagem localarmazenagem) {
		if(localarmazenagem == null || localarmazenagem.getCdlocalarmazenagem() == null)
			throw new SinedException("Local n�o pode ser nulo.");

		StringBuilder sql = new StringBuilder();
		sql
			.append(" select count(*) ")
			.append(" from ( ")
			.append(" select vgerenciar.cdmaterial as cdmaterial, sum(vgerenciar.qtdedisponivel) as qtdedisponivel ")
			.append(" from Vgerenciarmaterial vgerenciar ")
			.append(" where vgerenciar.cdlocalarmazenagem = " + localarmazenagem.getCdlocalarmazenagem())
			.append(" group by vgerenciar.cdmaterial ")
			.append(" having sum(vgerenciar.qtdedisponivel) <> 0 ")
			.append(" ) query ") ;

		SinedUtil.markAsReader();
		Long contador = getJdbcTemplate().queryForLong(sql.toString());
		
		return contador != null && contador > 0;
	}
	
	@SuppressWarnings("unchecked")
	public List<Vgerenciarmaterial> findForW3Producao(EstoqueW3producaoRESTWSBean estoqueW3producaoRESTWSBean) {
		if (estoqueW3producaoRESTWSBean.getWhereInEmpresa()==null || estoqueW3producaoRESTWSBean.getWhereInEmpresa().trim().equals("")) {
			return new ArrayList<Vgerenciarmaterial>();
		}
		
		Integer cdlocalarmazenagem = -1;
		
		try {
			cdlocalarmazenagem = Integer.valueOf(parametrogeralService.buscaValorPorNome(Parametrogeral.LOCALARMAZENAGEM_CHAO_DE_FABRICA));
		} catch (Exception e) {
		}				
		
		StringBuilder sql = new StringBuilder();
		sql.append("select v.cdmaterial, sum(v.qtdedisponivel) as qtdedisponivel ");
		sql.append("from vgerenciarmaterial v ");
		sql.append("where v.cdempresa in (" + estoqueW3producaoRESTWSBean.getWhereInEmpresa() + ") ");
		sql.append("and (v.cdlocalarmazenagem=" + cdlocalarmazenagem + " or v.cdlocalarmazenagem is null) ");
		sql.append("group by v.cdmaterial");

		SinedUtil.markAsReader();
		List<Vgerenciarmaterial> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Vgerenciarmaterial mapRow(ResultSet rs, int rowNum) throws SQLException {
				Vgerenciarmaterial vgerenciarmaterial = new Vgerenciarmaterial();
				vgerenciarmaterial.setCdmaterial(rs.getInt("cdmaterial"));
				vgerenciarmaterial.setQtdedisponivel(rs.getDouble("qtdedisponivel"));
				return vgerenciarmaterial;
			}
		});		
		
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	private String whereInCdMaterial(GerenciamentomaterialFiltro filtro, Material material){
		StringBuilder sql = new StringBuilder();
		StringBuilder sqlWhere = new StringBuilder();
		sql.append("select material.cdmaterial from material");
		sqlWhere.append(" where 1 = 1 ");
		if((filtro.getInventario() != null && filtro.getInventario().getCdinventario() != null) || filtro.getIndicadorpropriedade() != null){
			sql.append(" join inventariomaterial on inventariomaterial.cdmaterial = material.cdmaterial");
			if(filtro.getInventario() != null && filtro.getInventario().getCdinventario() != null){
				sqlWhere.append(" and inventariomaterial.cdinventario = " + filtro.getInventario().getCdinventario());;
			}
			if(filtro.getIndicadorpropriedade() != null){
				sqlWhere.append(" and inventariomaterial.indicadorpropriedade = " + filtro.getIndicadorpropriedade().getValue());
			}
		}
		
		if(filtro.getFornecedor() != null && filtro.getFornecedor().getCdpessoa() != null){
			sql.append(" join materialfornecedor on materialfornecedor.cdmaterial = material.cdmaterial");
			sqlWhere.append(" and materialfornecedor.cdpessoa = " + filtro.getFornecedor().getCdpessoa());
		}
	
		if (filtro.getContagerencial() != null){
			sql.append(" join contagerencial on contagerencial.cdcontagerencial = material.cdcontagerencial");
			sqlWhere.append(" and contagerencial.cdcontagerencial = " + filtro.getContagerencial().getCdcontagerencial());
		}
		
		if (filtro.getCaracteristica() != null && !"".equals(filtro.getCaracteristica())){
			String funcaoTiraacento = Neo.getApplicationContext().getConfig().getProperties().getProperty("funcaoTiraacento");
			sql.append(" join materialcaracteristica on materialcaracteristica.cdmaterial = material.cdmaterial");
			sqlWhere.append(" and UPPER(" + (funcaoTiraacento != null ? funcaoTiraacento + "(materialcaracteristica.nome))" : "materialcaracteristica.nome )" ) + " like  '%' || '" + Util.strings.tiraAcento(filtro.getCaracteristica()).toUpperCase() + "' || '%'");
		}
		
		if (filtro.getMaterialtipo() != null && filtro.getMaterialtipo().getCdmaterialtipo() != null){
			sqlWhere.append(" and material.cdmaterialtipo = " + filtro.getMaterialtipo().getCdmaterialtipo());
		}
		
		if(filtro.getLocalizacaoestoque() != null && filtro.getLocalizacaoestoque().getCdlocalizacaoestoque() != null){
			sqlWhere.append(" and material.cdlocalizacaoestoque = " + filtro.getLocalizacaoestoque().getCdlocalizacaoestoque());
		}
		
		if(filtro.getMaterialcategoria() != null){
			materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
			sql.append(" join materialcategoria on materialcategoria.cdmaterialcategoria = material.cdmaterialcategoria ");
			sql.append(" join vmaterialcategoria on vmaterialcategoria.cdmaterialcategoria = materialcategoria.cdmaterialcategoria ");
			sqlWhere.append(" and (vmaterialcategoria.identificador like '"+filtro.getMaterialcategoria().getIdentificador()+"' || '.%' or vmaterialcategoria.identificador like '"+filtro.getMaterialcategoria().getIdentificador()+"')");
		}
		if(filtro.getMaterial() != null){
			if(filtro.getFiltroItemGrade() != null && filtro.getFiltroItemGrade() && 
					filtro.getWhereInMaterialItemGrade() != null &&
					!"".equals(filtro.getWhereInMaterialItemGrade())){
				sqlWhere.append(" and material.cdmaterial in (" + filtro.getWhereInMaterialItemGrade() + (material != null ? "," + material.getCdmaterial() : "") + ") ");	
			}else {
				sqlWhere.append(" and material.cdmaterial = " + filtro.getMaterial().getCdmaterial());
			}
		} 
		
		if(filtro.getMaterialgrupo() != null){
			sqlWhere.append(" and material.cdmaterialgrupo = " + filtro.getMaterialgrupo().getCdmaterialgrupo());
		}
		
		if(Boolean.TRUE.equals(filtro.getSomentematerialativo())){
			sqlWhere.append(" and material.ativo = true");
		}
		if(Boolean.TRUE.equals(filtro.getSomentematerialinativo())){
			sqlWhere.append(" and coalesce(material.ativo, false) = false");
		}
		
		if(sqlWhere.toString().equalsIgnoreCase(" where 1 = 1 ")){
			return null;
		}
		SinedUtil.markAsReader();
		List<String> lista = getJdbcTemplate().query(sql.toString()+sqlWhere.toString(), new RowMapper() {

			@Override
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString("cdmaterial");
			}
		});
		
		return lista == null || lista.size() == 0 ? "" : CollectionsUtil.concatenate(lista, ","); 
	}
}