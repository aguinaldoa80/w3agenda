package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Bandaredecomercial;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.BandaredecomercialFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("bandaredecomercial.nome")
public class BandaredecomercialDAO extends GenericDAO<Bandaredecomercial>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Bandaredecomercial> query, FiltroListagem _filtro) {
		BandaredecomercialFiltro filtro = (BandaredecomercialFiltro)_filtro;
		query
			 .whereLikeIgnoreAll("bandaredecomercial.nome", filtro.getNome());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Bandaredecomercial> query) {
		query.leftOuterJoinFetch("bandaredecomercial.listaMaterial listaMaterial")
			 .leftOuterJoinFetch("bandaredecomercial.listaRegra listaRegra")
			 .leftOuterJoinFetch("listaRegra.bandaredeupload bandaredeupload")
			 .leftOuterJoinFetch("listaRegra.bandarededownload bandarededownload");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaMaterial");
		save.saveOrUpdateManaged("listaRegra");
	}
}
