package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Producaoagendamaterialitem;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoagendamaterialitemDAO extends GenericDAO<Producaoagendamaterialitem>{

	public List<Producaoagendamaterialitem> findByProducaoagenda(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		return query()
					.select("producaoagendamaterialitem.cdproducaoagendamaterialitem, producaoagendamaterialitem.qtde, producaoagendamaterialitem.volume, " +
							"material.cdmaterial, material.nome, material.identificacao, material.qtdereferencia, " +
							"producaoagenda.cdproducaoagenda")
					.join("producaoagendamaterialitem.producaoagenda producaoagenda")
					.join("producaoagendamaterialitem.material material")
					.whereIn("producaoagenda.cdproducaoagenda", whereIn)
					.list();
	}

}
