package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Fornecimento;
import br.com.linkcom.sined.geral.bean.Fornecimentocontrato;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.FornecimentoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

public class FornecimentoDAO extends GenericDAO<Fornecimento> {

	@Override
	public void updateListagemQuery(QueryBuilder<Fornecimento> query, FiltroListagem _filtro) {
		if (_filtro ==  null){
			throw new SinedException("Dados inv�lidos");
		}
		FornecimentoFiltro filtro = (FornecimentoFiltro) _filtro;
		
		query
			.select("fornecimento.cdfornecimento, fornecimento.historico, fornecimento.dtcontrato, fornecimento.valor, fornecimento.baixado, fornecimentocontrato.descricao")
			.join("fornecimento.fornecimentocontrato fornecimentocontrato")
			.whereLikeIgnoreAll("fornecimento.historico", filtro.getHistorico())
			.where("fornecimentocontrato = ?", filtro.getFornecimentocontrato())
			.where("fornecimento.baixado = ?", filtro.getBaixado())
			.where("fornecimento.dtcontrato >= ?", filtro.getDtinicio())
			.where("fornecimento.dtcontrato <= ?", filtro.getDtfim())
			.where("fornecimento.empresa = ?", filtro.getEmpresa());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Fornecimento> query) {
		((QueryBuilderSined<Fornecimento>) query).setUseWhereFornecedorEmpresa(true);
		
		query
			.leftOuterJoinFetch("fornecimento.listaFornecimentoitem")
			.leftOuterJoin("fornecimento.empresa empresa");
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				save.saveOrUpdateManaged("listaFornecimentoitem");
				return null;
			}
		});
	}

	/**
	 * Carrega a lista a partir de uma whereIn para que o fronecimento seja faturado.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Fornecimento> findForFaturar(String whereIn) {
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Where In dos ids de fornecimento n�o pode ser nulo.");
		}
		return query()
					.select("fornecimento.cdfornecimento, fornecimento.baixado, centrocusto.cdcentrocusto, centrocusto.nome, " +
							"projeto.cdprojeto, projeto.nome, contagerencial.cdcontagerencial, contagerencial.nome, " +
							"fornecimento.valor, fornecimentocontrato.cdfornecimentocontrato, fornecedor.cdpessoa, fornecedor.nome, " +
							"fornecimentocontrato.dtproximovencimento, prazopagamento.cdprazopagamento")
					.join("fornecimento.fornecimentocontrato fornecimentocontrato")
					.leftOuterJoin("fornecimentocontrato.fornecedor fornecedor")
					.join("fornecimentocontrato.centrocusto centrocusto")
					.join("fornecimentocontrato.prazopagamento prazopagamento")
					.leftOuterJoin("fornecimentocontrato.projeto projeto")
					.join("fornecimentocontrato.contagerencial contagerencial")
					.whereIn("fornecimento.cdfornecimento", whereIn)
					.list();
	}

	/**
	 * Atualiza o fronecimento para baixado.
	 *
	 * @param fornecimento
	 * @author Rodrigo Freitas
	 */
	public void updateBaixado(String whereIn, Boolean baixado) {
		if (whereIn == null || "".equals(whereIn)) {
			throw new SinedException("Fornecimento n�o pode ser nulo.");
		}
		getHibernateTemplate().bulkUpdate("update Fornecimento f set f.baixado = ? where f.cdfornecimento in (" + whereIn + ")", 
				new Object[]{baixado});
	}
	
	/**
	 * Retorna a quantidade de fornecimento que ser� o n�mero da parcela em prazo
	 * de contrato de fornecimento.
	 * 
	 * @param fornecimentocontrato
	 * @return
	 * @author Taidson
	 * @since 28/06/2010
	 */
	public Integer getQtdeFornecimento(Fornecimentocontrato fornecimentocontrato) {
		return newQueryBuilderSined(Long.class)
			.from(Fornecimento.class)
			.setUseTranslator(false) 
			.select("count(*)")
			.join("fornecimento.fornecimentocontrato fornecimentocontrato")
			.where("fornecimentocontrato = ?", fornecimentocontrato)
			.unique()
			.intValue();
	}

	/**
	 * M�todo que busca o Fornecimento para registrar entrada fiscal
	 *
	 * @param fornecimento
	 * @return
	 * @author Luiz Fernando
	 */
	public Fornecimento findForRegistrarEntradafiscal(Fornecimento fornecimento) {
		if (fornecimento == null || fornecimento.getCdfornecimento() == null) {
			throw new SinedException("Fornecimento n�o pode ser nulo.");
		}
		return query()
					.select("fornecimento.cdfornecimento, fornecimentocontrato.cdfornecimentocontrato, fornecedor.cdpessoa, fornecedor.nome, " +
							"fornecimento.dtcontrato, fornecimento.cdentregadocumento ")
					.leftOuterJoin("fornecimento.fornecimentocontrato fornecimentocontrato")
					.leftOuterJoin("fornecimentocontrato.fornecedor fornecedor")
					.where("fornecimento = ?", fornecimento)
					.unique();
	}

	/**
	 * M�todo para fazer update da entrada fiscal gerada
	 *
	 * @param idFornecimento
	 * @param cdentregadocumento
	 * @author Luiz Fernando
	 */
	public void updateCdentregadocumento(String idFornecimento,	Integer cdentregadocumento) {
		if(idFornecimento != null && !"".equals(idFornecimento) && SinedUtil.validaNumeros(idFornecimento)){
			Integer cdfornecimento = Integer.parseInt(idFornecimento);
			getHibernateTemplate().bulkUpdate("update Fornecimento f set f.cdentregadocumento = ? where f.cdfornecimento = ?",
					new Object[]{cdentregadocumento, cdfornecimento});	
		}
	}

	public Boolean existDocumentoNotCancelado(Fornecimento fornecimento) {
		if(fornecimento == null || fornecimento.getCdfornecimento() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return newQueryBuilder(Long.class)
			.select("count(*)")
			.setUseTranslator(false)
			.from(Fornecimento.class)
			.where("fornecimento = ?", fornecimento)
			.where("exists (select d.cddocumento from Documento d " +
					"join d.listaDocumentoOrigem do " +
					"join do.fornecimento f " +
					"join d.documentoacao da " +
					"where da.cddocumentoacao <> " + Documentoacao.CANCELADA.getCddocumentoacao() + " and fornecimento.cdfornecimento = f.cdfornecimento )")
			.unique() > 0;
	}

	public List<Fornecimento> findByVencido(Date data, Date dateToSearch) {
		return query()
				.where("fornecimento.dtcontrato < ?", data)
				.wherePeriodo("fornecimento.dtcontrato", dateToSearch, SinedDateUtils.currentDate())
				.list();
	}
	
	public List<Fornecimento> findForReport(FornecimentoFiltro filtro) {
		QueryBuilder<Fornecimento> query = querySined();
		updateListagemQuery(query, filtro);
		return query.list();
	}
}
