package br.com.linkcom.sined.geral.dao;


import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Controleorcamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Projetotipo;
import br.com.linkcom.sined.geral.bean.auxiliar.TotalOrcamentoMes;
import br.com.linkcom.sined.geral.bean.enumeration.Controleorcamentosituacao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ControleorcamentoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

import com.ibm.icu.text.SimpleDateFormat;

public class ControleorcamentoDAO extends GenericDAO<Controleorcamento>{
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Controleorcamento> query) {
		query
			.select("controleorcamento.cdcontroleorcamento, controleorcamento.situacao, " +
					"controleorcamento.descricao,controleorcamento.dtExercicioInicio,controleorcamento.dtExercicioFim," +
					"controleorcamento.descricao,controleorcamento.descricao," +
					"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia," +
					"projeto.cdprojeto, projeto.nome, " +
					"projetoTipo.cdprojetotipo,projetoTipo.nome," +
					"orcamentoProjeto.cdprojeto, orcamentoProjeto.nome, " +
					"orcamentoProjetoTipo.cdprojetotipo,orcamentoProjetoTipo.nome," +
					"centrocusto.cdcentrocusto, centrocusto.nome, contagerencial.cdcontagerencial, " +
					"contagerencial.nome,listacontroleorcamentohistorico.cdcontroleorcamentohistorico ,listacontroleorcamentohistorico.dataaltera," +
					"listacontroleorcamentohistorico.acaoexecutada,listacontroleorcamentohistorico.responsavel,listacontroleorcamentohistorico.observacao," +
					"listaControleorcamentoitem.cdcontroleorcamentoitem, listaControleorcamentoitem.valor, " +
					"listaControleorcamentoitem.mesano, controleorcamento.tipolancamento ")
			.leftOuterJoin("controleorcamento.empresa empresa")
			.leftOuterJoin("controleorcamento.projeto orcamentoProjeto")
			.leftOuterJoin("controleorcamento.projetoTipo orcamentoProjetoTipo")
			.leftOuterJoin("controleorcamento.listacontroleorcamentohistorico listacontroleorcamentohistorico")
			.leftOuterJoin("controleorcamento.listaControleorcamentoitem listaControleorcamentoitem")
			.leftOuterJoin("listaControleorcamentoitem.projeto projeto")
			.leftOuterJoin("listaControleorcamentoitem.projetoTipo projetoTipo")
			.leftOuterJoin("listaControleorcamentoitem.centrocusto centrocusto")
			.leftOuterJoin("listaControleorcamentoitem.contagerencial contagerencial")			
			.orderBy("controleorcamento.dtExercicioInicio")
			.orderBy("listacontroleorcamentohistorico.dataaltera desc");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Controleorcamento> query, FiltroListagem _filtro) {
		ControleorcamentoFiltro filtro = (ControleorcamentoFiltro) _filtro;
		Date dtFim = null;
		Date dtInicio = null;
		if(filtro.getDtExercicioFim() != null){
			dtFim = SinedDateUtils.lastDateOfMonth(filtro.getDtExercicioFim());
		}
		if(filtro.getDtExercicioInicio() != null){
			dtInicio = SinedDateUtils.firstDateOfMonth(filtro.getDtExercicioInicio());
		}
		query
			.select("distinct controleorcamento.cdcontroleorcamento, controleorcamento.situacao, controleorcamento.descricao," +
					"controleorcamento.dtExercicioInicio,controleorcamento.dtExercicioFim," +
					"empresa.razaosocial, empresa.nomefantasia ")
			.leftOuterJoin("controleorcamento.empresa empresa")
			.leftOuterJoin("controleorcamento.listaControleorcamentoitem listaControleorcamentoitem ")
			.leftOuterJoin("listaControleorcamentoitem.contagerencial contagerencial")
			.leftOuterJoin("listaControleorcamentoitem.centrocusto centrocusto")
			.leftOuterJoin("listaControleorcamentoitem.projeto projeto")
			.where("empresa = ?", filtro.getEmpresa())
			.where("centrocusto = ?", filtro.getCentrocusto())
			.where("projeto = ?", filtro.getProjeto())
			.where("contagerencial = ?", filtro.getContagerencial())
			.where("listaControleorcamentoitem.valor = ?", filtro.getValor())
			.where("controleorcamento.dtExercicioInicio >= ?", dtInicio)
			.where("controleorcamento.dtExercicioFim <= ?", dtFim)
			.whereLikeIgnoreAll("controleorcamento.descricao", filtro.getDescricao())
			.ignoreJoin("listaControleorcamentoitem", "contagerencial", "centrocusto", "projeto");
		
		
		if(filtro.getListaControleoportunidadesituacao() != null && !filtro.getListaControleoportunidadesituacao().isEmpty()){
			query.whereIn("controleorcamento.situacao", Controleorcamentosituacao.listAndConcatenate(filtro.getListaControleoportunidadesituacao()));
		}
		query.orderBy("controleorcamento.dtExercicioInicio");
	}
	
	public List<Controleorcamento> loadWithLista(String whereIn, String orderBy, boolean asc) {
		QueryBuilder<Controleorcamento> query = query()
					.select("controleorcamento.cdcontroleorcamento, controleorcamento.descricao, controleorcamento.dtExercicioInicio,controleorcamento.dtExercicioFim, controleorcamento.situacao, " +
							"listaControleorcamentoitem.cdcontroleorcamentoitem, listaControleorcamentoitem.valor, " +
							"contagerencial.cdcontagerencial, contagerencial.nome, centrocusto.cdcentrocusto, centrocusto.nome, " +
							"projeto.cdprojeto, projeto.nome, contagerencialpai.cdcontagerencial, contagerencialpai.nome," +
							"vcontagerencial.cdcontagerencial, vcontagerencial.nome, vcontagerencial.arvorepai, empresa.razaosocial, empresa.nomefantasia ")					
					.leftOuterJoin("controleorcamento.empresa empresa ")
					.leftOuterJoin("controleorcamento.listaControleorcamentoitem listaControleorcamentoitem ")
					.leftOuterJoin("listaControleorcamentoitem.centrocusto centrocusto")
					.leftOuterJoin("listaControleorcamentoitem.projeto projeto")					
					.leftOuterJoin("listaControleorcamentoitem.contagerencial contagerencial")
					.leftOuterJoin("contagerencial.contagerencialpai contagerencialpai")
					.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
					.whereIn("controleorcamento.cdcontroleorcamento", whereIn);
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("controleorcamento.cdcontroleorcamento DESC");
		}
		
		return query.list();
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaControleorcamentoitem");
	}
	
	/**
	 * M�todo para buscar os Controles de Or�amento
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Controleorcamento> findSituacaoByWhereIn(String whereIn){
		return query()
				.select("controleorcamento.cdcontroleorcamento, controleorcamento.situacao")
				.whereIn("controleorcamento.cdcontroleorcamento", whereIn)
				.list();
	}
	
	/**
	 * M�todo para buscar os Controle de Or�amento para serem atualizados
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Controleorcamento> findForAprovar(String whereIn) {		
		return query()
				.select("controleorcamento.cdcontroleorcamento, controleorcamento.situacao, " +
						"listaControleorcamentoitem.cdcontroleorcamentoitem ,centrocusto.cdcentrocusto, contagerencial.cdcontagerencial ")
				.leftOuterJoin("controleorcamento.listaControleorcamentoitem listaControleorcamentoitem")
				.leftOuterJoin("listaControleorcamentoitem.centrocusto centrocusto")
				.leftOuterJoin("listaControleorcamentoitem.contagerencial contagerencial")
				.where("controleorcamento.situacao <> ?", Controleorcamentosituacao.APROVADO)
				.whereIn("controleorcamento.cdcontroleorcamento", whereIn)
				.list();
	}
	/**
	 * M�todo para buscar os Controle de Or�amento para serem atualizados
	 * Alterado com intuito de atender ao estornar
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando - Cria��o
	 * @author C�sar - Altera��o
	 */
	public List<Controleorcamento> findForEstornar(String whereIn) {		
		return query()
			.select("controleorcamento.cdcontroleorcamento, controleorcamento.situacao, " +
			"listaControleorcamentoitem.cdcontroleorcamentoitem ,centrocusto.cdcentrocusto, contagerencial.cdcontagerencial ")
			.leftOuterJoin("controleorcamento.listaControleorcamentoitem listaControleorcamentoitem")
			.leftOuterJoin("listaControleorcamentoitem.centrocusto centrocusto")
			.leftOuterJoin("listaControleorcamentoitem.contagerencial contagerencial")
			.where("controleorcamento.situacao = ?", Controleorcamentosituacao.APROVADO)
			.whereIn("controleorcamento.cdcontroleorcamento", whereIn)
			.list();
	}

	/**
	 * M�todo para atualizar situa��o do Controle de Or�amento
	 *
	 * @param controleorcamento
	 * @author Luiz Fernando
	 */
	public void atualizarSituacao(Controleorcamento controleorcamento) {
		if(controleorcamento == null || controleorcamento.getCdcontroleorcamento() == null ||
				controleorcamento.getSituacao() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		getHibernateTemplate().bulkUpdate("update Controleorcamento c set c.situacao = ? where c.cdcontroleorcamento = ?", 
								new Object[]{controleorcamento.getSituacao(), controleorcamento.getCdcontroleorcamento()});
		
	}
	/**
	 * M�todo para atualizar situa��o dos Controles de Or�amentos
	 *
	 * @param situca��o, whereIn
	 * @author yago
	 */
	public void updateSituacao(String whereIn, Controleorcamentosituacao situacao) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		getHibernateTemplate().bulkUpdate("update Controleorcamento c set c.situacao = ? where c.cdcontroleorcamento in (" + whereIn + ")", situacao);
	}
	/**
	 * M�todo que busca as Contas gerenciais que tenham seu registro cadastrado no rateio de conta a pagar (de acordo com os par�metros)
	 *
	 * @param whereInContagerencial
	 * @param whereInCentrocusto 
	 * @param vencimento
	 * @param cdcontapagar
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public List<TotalOrcamentoMes> findForControleorcamentoMes(String whereInContagerencial, String whereInCentrocusto, Date vencimento, Integer cdcontapagar, Integer cdempresa){
		if(vencimento == null || whereInContagerencial == null || whereInContagerencial.equals("") || whereInCentrocusto == null || whereInCentrocusto.equals(""))
			throw new SinedException("Par�metros inv�lidos.");
		
		String mes = new SimpleDateFormat("MM").format(vencimento);
		String ano = new SimpleDateFormat("yyyy").format(vencimento);
		StringBuilder sql = new StringBuilder();
		
		sql
			.append("select sum(ri.valor) as valor, cg.cdcontagerencial, cc.cdcentrocusto ")
			.append("from contagerencial cg ")
			.append("join rateioitem ri on ri.cdcontagerencial = cg.cdcontagerencial ")
			.append("join centrocusto cc on cc.cdcentrocusto = ri.cdcentrocusto ")
			.append("join rateio r on r.cdrateio = ri.cdrateio ")
			.append("join documento d on d.cdrateio = r.cdrateio ")
			.append("where d.cddocumentoacao <> 0 ")
			.append("and extract(month from d.dtvencimento) = '" + mes +"' ") 
			.append("and extract(year from d.dtvencimento) = '" + ano +"' ")
			.append("and d.cddocumentoclasse = 1 ")
			.append("and (cg.cdcontagerencial in (" + whereInContagerencial + ") or cc.cdcentrocusto in (" + whereInCentrocusto + ")) ");
		
		if(cdcontapagar != null)
			sql.append("and d.cddocumento <> " + cdcontapagar + " ");
		
		if(cdempresa != null)
			sql.append("and ( d.cdempresa is null or d.cdempresa = " + cdempresa + " ) ");
		else
			sql.append("and d.cdempresa is null ");
		
		sql.append("group by cg.cdcontagerencial, cc.cdcentrocusto; ");
		
		SinedUtil.markAsReader();
		List<TotalOrcamentoMes> lista = getJdbcTemplate().query(sql.toString(), new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new TotalOrcamentoMes(rs.getInt("cdcontagerencial"), rs.getInt("cdcentrocusto"), rs.getDouble("valor"));
			}
		});
		
		return lista;
	}

	/**
	 * Busca os or�amentos dos anos para An�lise de Receitas e Despesas
	 *
	 * @param anoDe
	 * @param anoAte
	 * @param whereInEmpresa
	 * @param whereInCentrocusto
	 * @param whereInProjeto 
	 * @return
	 * @since 10/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Controleorcamento> findForAnaliseRecDesp(Date anoDe, Date anoAte, String whereInEmpresa, String whereInCentrocusto, String whereInProjeto, String whereInProjetotipo) {
		java.util.Date dtFim = null;
		java.util.Date dtInicio = null;
		if(anoAte != null){
			dtFim = SinedDateUtils.lastDateOfMonth(anoAte);
		}
		if(anoDe != null){
			dtInicio = SinedDateUtils.firstDateOfMonth(anoDe);
		}
		QueryBuilder<Controleorcamento> query = query()
					.select("controleorcamento.cdcontroleorcamento, controleorcamento.tipolancamento, " +
							"controleorcamentoitem.valor, contagerencial.cdcontagerencial, contagerencial.nome, vcontagerencial.identificador, tipooperacao.cdtipooperacao, tipooperacao.nome, " +
							"controleorcamentoitem.mesano,controleorcamento.dtExercicioInicio,controleorcamento.dtExercicioFim ")
					.leftOuterJoin("controleorcamento.listaControleorcamentoitem controleorcamentoitem")
					.leftOuterJoin("controleorcamentoitem.contagerencial contagerencial")
					.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
					.leftOuterJoin("contagerencial.tipooperacao tipooperacao")
					.leftOuterJoin("controleorcamento.empresa empresa")
					.where("controleorcamento.situacao = ?", Controleorcamentosituacao.APROVADO)
					.whereIn("controleorcamentoitem.centrocusto", whereInCentrocusto)
					.whereIn("controleorcamentoitem.projeto", whereInProjeto)
					.whereIn("controleorcamentoitem.projeto.projetotipo", whereInProjetotipo)
					.where("controleorcamento.dtExercicioInicio <= ?", dtInicio)
					.where("controleorcamento.dtExercicioFim >= ?", dtFim);
		
		
		if(whereInEmpresa != null && !whereInEmpresa.equals("")){
			query
				.openParentheses()
				.where("empresa is null")
				.or()
				.whereIn("empresa.cdpessoa", whereInEmpresa)
				.closeParentheses();
		}
		
		return query.list();
	}
	
	/**
	 * M�todo que busca os dados que alimentar�o o relat�rio pdf de controle de or�amento
	 * 
	 * @param filtro
	 * @return
	 * 
	 * @author Rafael Salvio
	 */
	public List<Controleorcamento> findForReport(ControleorcamentoFiltro filtro){
		QueryBuilder<Controleorcamento> query = query();
		Date dtFim = null;
		Date dtInicio = null;
		if(filtro.getDtExercicioFim() != null){
			dtFim = SinedDateUtils.lastDateOfMonth(filtro.getDtExercicioFim());
		}
		if(filtro.getDtExercicioInicio() != null){
			dtInicio = SinedDateUtils.firstDateOfMonth(filtro.getDtExercicioInicio());
		}
		query
			.select("controleorcamento.cdcontroleorcamento, controleorcamentoitem.cdcontroleorcamentoitem,controleorcamento.dtExercicioInicio,controleorcamento.dtExercicioFim," +
					"controleorcamento.descricao, controleorcamentoitem.valor, controleorcamentoitem.mesano, centrocusto.cdcentrocusto, centrocusto.nome, " +
					"projeto.cdprojeto, projeto.nome, contagerencial.cdcontagerencial, contagerencial.nome, contagerencialpai.cdcontagerencial, " +
					"vcontagerencial.identificador, vcontagerencial.cdcontagerencial")
			.leftOuterJoin("controleorcamento.empresa empresa")
			.leftOuterJoin("controleorcamento.listaControleorcamentoitem controleorcamentoitem")
			.leftOuterJoin("controleorcamentoitem.centrocusto centrocusto")
			.leftOuterJoin("controleorcamentoitem.projeto projeto")
			.leftOuterJoin("controleorcamentoitem.contagerencial contagerencial")
			.leftOuterJoin("contagerencial.contagerencialpai contagerencialpai")
			.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			.where("empresa = ?", filtro.getEmpresa())
			.where("centrocusto = ?", filtro.getCentrocusto())
			.where("projeto = ?", filtro.getProjeto())
			.where("contagerencial = ?", filtro.getContagerencial())
			.where("listaControleorcamentoitem.valor = ?", filtro.getValor())
			.where("controleorcamento.dtExercicioInicio >= ?", dtInicio)
			.where("controleorcamento.dtExercicioFim <= ?", dtFim);
	
		if(filtro.getListaControleoportunidadesituacao() != null && !filtro.getListaControleoportunidadesituacao().isEmpty()){
			query.whereIn("controleorcamento.situacao", Controleorcamentosituacao.listAndConcatenate(filtro.getListaControleoportunidadesituacao()));
		}
		
		return query.list();
	}

	@Override
	public ListagemResult<Controleorcamento> findForExportacao(
			FiltroListagem filtro) {
		QueryBuilder<Controleorcamento> query = query();
		updateListagemQuery(query, filtro);
		
		return new ListagemResult<Controleorcamento>(query, filtro);
	}
	

	public List<Controleorcamento> findByEmpresaAndExercicio(Empresa empresa,Date exercicioInicio,Date exercicioFim,Integer cdControleOrcamento, Projeto projeto, Projetotipo projetoTipo) {

		Date dtFim = null;
		Date dtInicio = null;
		if(exercicioInicio != null){
			dtFim = SinedDateUtils.lastDateOfMonth(exercicioFim);
		}
		if(exercicioInicio != null){
			dtInicio = SinedDateUtils.firstDateOfMonth(exercicioInicio);
		}
		return query()
				.select("controleorcamento.cdcontroleorcamento")
				.leftOuterJoin("controleorcamento.empresa empresa")
				.where("controleorcamento.empresa = ?", empresa)
				.where("controleorcamento.situacao <> ?", Controleorcamentosituacao.INATIVO)
				.where("controleorcamento.dtExercicioFim >= ?", dtInicio)
				.where("controleorcamento.dtExercicioInicio <= ?", dtFim)
				.where("controleorcamento.projeto = ?", projeto)
				.where("controleorcamento.projetoTipo = ?", projetoTipo)
				.where("controleorcamento.cdcontroleorcamento <> ?", cdControleOrcamento)
				.list();	
	}
	
	public Controleorcamento buscarValorFluxoCaixa(Controleorcamento controleOrcamento) {
		if(controleOrcamento == null || controleOrcamento.getCdcontroleorcamento() == null){
			throw new SinedException("Controle de or�amento n�o pode ser nulo.");
		}
		
		return query()
				.select("controleorcamento.cdcontroleorcamento,controleorcamento.tipolancamento," +
						"controleorcamentoitem.valor, controleorcamentoitem.mesano," +
						"centrocusto.cdcentrocusto, centrocusto.nome,controleorcamento.dtExercicioInicio,controleorcamento.dtExercicioFim," +
						"contagerencial.cdcontagerencial, contagerencial.nome, contagerencial.tipooperacao, " +
						"vcontagerencial.identificador")
				.leftOuterJoin("controleorcamento.listaControleorcamentoitem controleorcamentoitem")
				.leftOuterJoin("controleorcamentoitem.centrocusto centrocusto")
				.leftOuterJoin("controleorcamentoitem.contagerencial contagerencial")
				.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
				.where("controleorcamento = ?", controleOrcamento)
				.unique();
	}

	public List<Controleorcamento> findOrcamentosForCsv(ControleorcamentoFiltro filtro) {
		QueryBuilder<Controleorcamento> query = query();
		this.updateListagemQuery(query, filtro);
		
		query
			.select("controleorcamento.cdcontroleorcamento, controleorcamento.descricao, controleorcamento.tipolancamento, " +
					"controleorcamento.situacao, controleorcamento.dtExercicioInicio, controleorcamento.dtExercicioFim, " +
					"listaControleorcamentoitem.cdcontroleorcamentoitem, listaControleorcamentoitem.valor, " +
					"listaControleorcamentoitem.mesano, centrocusto.nome, projeto.nome, projetotipo.nome, " +
					"vcontagerencial.identificador, vcontagerencial.nome")
			.leftOuterJoin("contagerencial.vcontagerencial vcontagerencial")
			.leftOuterJoin("controleorcamento.projetoTipo projetotipo");
		query.orderBy("vcontagerencial.identificador, listaControleorcamentoitem.mesano");
		
		query.setIgnoreJoinPaths(new HashSet<String>());
		
		return query.list();
	}

	public Controleorcamento findOrcamentoById(Controleorcamento controleorcamento) {
		QueryBuilder<Controleorcamento> query = query()
				.select("controleorcamento.cdcontroleorcamento, controleorcamento.tipolancamento, controleorcamento.descricao, " +
						"projeto.cdprojeto, projetoTipo.cdprojetotipo")
				.leftOuterJoin("controleorcamento.projeto projeto")
				.leftOuterJoin("controleorcamento.projetoTipo projetoTipo")
				.where("controleorcamento = ?", controleorcamento);
				
				return query.unique();
	}
	
	public Controleorcamento loadOrcamentoForDateValidation(Controleorcamento controleorcamento) {
		return query()
				.select("controleorcamento.cdcontroleorcamento,controleorcamento.dtExercicioInicio, controleorcamento.dtExercicioFim ")
						.where("controleorcamento = ?", controleorcamento).unique();
		 
	}
	
	public boolean existeItemDataSuperiorOrcamento (Controleorcamento controleorcamento) {
		return newQueryBuilder(Long.class).from(Controleorcamento.class)
				.select("count(*)")
				.leftOuterJoin("controleorcamento.listaControleorcamentoitem listaControleorcamentoitem")
				.where("controleorcamento = ?", controleorcamento)
				.where("listaControleorcamentoitem.mesano > ?", controleorcamento.getDtExercicioFim())
				.unique() > 0;
	}
	
}
