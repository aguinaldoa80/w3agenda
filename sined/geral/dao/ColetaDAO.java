package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.ColetaMaterialPedidovendamaterial;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.LabelColoboradorFiltro;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVinculoExpedicao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.service.ColetaMaterialService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ColetamaterialNotaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ColetaFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ColetaDAO extends GenericDAO<Coleta> {
	private ColetaMaterialService coletaMaterialService;
	
	public void setColetaMaterialService(ColetaMaterialService coletaMaterialService) {
		this.coletaMaterialService = coletaMaterialService;
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Coleta> query) {
		query
			.select("coleta.cdcoleta, coleta.tipo, coleta.observacao, coleta.cdusuarioaltera, coleta.dtaltera, coleta.dtconfirmacaodevolucao, " +
					"coleta.situacaoColeta,coleta.motivodevolucao, empresa.cdpessoa, empresa.nome, " +
					"colaborador.cdpessoa, colaborador.nome, cliente.cdpessoa, cliente.nome, fornecedor.cdpessoa, fornecedor.nome, " +
					"pedidovenda.cdpedidovenda, pedidovenda.identificador, entregadocumentoColeta.cdentregadocumentocoleta, entregadocumento.cdentregadocumento, " +
					"notafiscalprodutoColeta.cdnotafiscalprodutocoleta, notafiscalprodutoColeta.devolucao, notafiscalproduto.cdNota, notafiscalproduto.tipooperacaonota, " +
					"coletaMaterial.cdcoletamaterial, coletaMaterial.quantidade, coletaMaterial.quantidadedevolvida, coletaMaterial.quantidadedevolvidanota, coletaMaterial.observacao, " +
					"material.cdmaterial, material.nome, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
					"pedidovendamaterialServicos.cdpedidovendamaterial, coletaMaterial.valorunitario, coletaMaterial.quantidadecomprada," +					
					"motivodevolucao.cdmotivodevolucao, motivodevolucao.descricao, listaColetaMaterialPedidovendamaterial.cdColetaMaterialPedidovendamaterial,"+
					"listaColetaMaterialPedidovendamaterial.producaoRecusada")
			.join("coleta.colaborador colaborador")
			.leftOuterJoin("coleta.empresa empresa")
			.leftOuterJoin("coleta.cliente cliente")
			.leftOuterJoin("coleta.fornecedor fornecedor")
			.leftOuterJoin("coleta.pedidovenda pedidovenda")
			.leftOuterJoin("coleta.listaEntregadocumentoColeta entregadocumentoColeta")
			.leftOuterJoin("entregadocumentoColeta.entregadocumento entregadocumento")
			.leftOuterJoin("coleta.listaNotafiscalprodutoColeta notafiscalprodutoColeta")
			.leftOuterJoin("notafiscalprodutoColeta.notafiscalproduto notafiscalproduto")
			.leftOuterJoin("coleta.listaColetaMaterial coletaMaterial")			
			.leftOuterJoin("coletaMaterial.material material")
			.leftOuterJoin("coletaMaterial.pneu pneu")
			.leftOuterJoin("coletaMaterial.localarmazenagem localarmazenagem")
			.leftOuterJoin("coletaMaterial.pedidovendamaterial pedidovendamaterial")
			.leftOuterJoin("coletaMaterial.listaColetaMaterialPedidovendamaterial listaColetaMaterialPedidovendamaterial")
			.leftOuterJoin("listaColetaMaterialPedidovendamaterial.pedidovendamaterial pedidovendamaterialServicos")
//			.leftOuterJoin("coletaMaterial.motivodevolucao motivodevolucao")
			.leftOuterJoin("coletaMaterial.listaColetamaterialmotivodevolucao listaColetamaterialmotivodevolucao")
			.leftOuterJoin("listaColetamaterialmotivodevolucao.motivodevolucao motivodevolucao")
			.orderBy("coletaMaterial.cdcoletamaterial");
		query = SinedUtil.setJoinsByComponentePneu(query);
	}

	@Override
	public void updateListagemQuery(QueryBuilder<Coleta> query,	FiltroListagem _filtro) {
		ColetaFiltro filtro = (ColetaFiltro) _filtro;
		Integer cdpedidovenda;
		try {
			cdpedidovenda = Integer.parseInt(filtro.getIdentificadorpedidovenda());
		} catch (Exception e) {
			cdpedidovenda = null;
		}
		query
			.select("distinct new br.com.linkcom.sined.geral.bean.Coleta(" +
						"coleta.cdcoleta,coleta.situacaoColeta, colaborador.cdpessoa, colaborador.nome, coalesce(cliente.nome, fornecedor.nome), coleta.tipo, " +
						"empresaColeta.cdpessoa, empresaColeta.nome, pedidovenda.identificacaoexterna " +
					")")
			.setUseTranslator(false)
			.join("coleta.colaborador colaborador")
			.leftOuterJoin("coleta.empresa empresaColeta")
			.leftOuterJoin("coleta.cliente cliente")
			.leftOuterJoin("cliente.listaClientevendedor listaClientevendedor")
			.leftOuterJoin("coleta.fornecedor fornecedor")
			.leftOuterJoin("coleta.pedidovenda pedidovenda")
			.leftOuterJoin("coleta.listaColetaMaterial coletaMaterial")			
			.where("(select min(ch.dtaltera) from Coletahistorico ch where ch.coleta = coleta) >= ?", SinedDateUtils.dateToTimestampInicioDia(filtro.getDataColetaDe()))
			.where("(select min(ch.dtaltera) from Coletahistorico ch where ch.coleta = coleta) <= ?",  SinedDateUtils.dateToTimestampFinalDia(filtro.getDataColetaAte()))
			.where("coleta.cdcoleta = ?", filtro.getCdcoleta())
			.where("coletaMaterial.material = ?", filtro.getMaterial())
			.where("coletaMaterial.localarmazenagem = ?", filtro.getLocalarmazenagem())
			.whereLikeIgnoreAll("coletaMaterial.observacao", filtro.getObservacao())
//			.where("coletaMaterial.motivodevolucao = ?", filtro.getMotivodevolucao())
			.openParentheses()
			.where("pedidovenda.cdpedidovenda = ?", cdpedidovenda)
			.or()
			.where("pedidovenda.identificador = ?", filtro.getIdentificadorpedidovenda())
			.closeParentheses()
			.where("pedidovenda.identificacaoexterna = ?", filtro.getIdentificacaoexterna())
			.where("pedidovenda.pedidovendasituacao = ?", filtro.getPedidovendasituacao())
			.where("coleta.situacaoColeta = ?", filtro.getSituacaoColeta())
			.where("empresaColeta = ?", filtro.getEmpresa())
			.orderBy("coleta.cdcoleta DESC")
			.ignoreJoin("coletaMaterial","listaClientevendedor");
		
		if(LabelColoboradorFiltro.COLETOR.equals(filtro.getTipoColaborador())){
			query.where("colaborador = ?", filtro.getColaborador());
		}
		else if(LabelColoboradorFiltro.VENDENDOR_PRINCIPAL.equals(filtro.getTipoColaborador()) &&  filtro.getColaborador()!=null){
			query.where("listaClientevendedor.colaborador = ?", filtro.getColaborador())
			.where("listaClientevendedor.principal = ?", Boolean.TRUE);
		}
		
		if(filtro.getTipo() != null){
			query.where("coleta.tipo = ?", filtro.getTipo());
			if(filtro.getCliente() != null){
				query.where("coleta.cliente = ?", filtro.getCliente());
			} else if(filtro.getFornecedor() != null){
				query.where("coleta.fornecedor = ?", filtro.getFornecedor());
			}
		}
		
		String tipoNota = filtro.getTipoNota();
		
		if (tipoNota!=null && !tipoNota.equals("")){
			query.leftOuterJoin("coleta.listaEntregadocumentoColeta listaEntregadocumentoColeta");
			query.leftOuterJoin("coleta.listaNotafiscalprodutoColeta listaNotafiscalprodutoColeta");
			query.leftOuterJoin("listaNotafiscalprodutoColeta.notafiscalproduto notafiscalproduto");
			if (ColetaFiltro.TIPO_NOTA_TODOS.equals(tipoNota)){
				query.openParentheses();
				query.where("listaEntregadocumentoColeta is not null");
				query.or();
				query.where("listaNotafiscalprodutoColeta is not null");
				query.closeParentheses();
			}else if (ColetaFiltro.TIPO_NOTA_ENTRADA.equals(tipoNota)){
				query.openParentheses();
				query.where("listaEntregadocumentoColeta is not null");
				query.or();
				query.where("notafiscalproduto.tipooperacaonota=?", Tipooperacaonota.ENTRADA);
				query.closeParentheses();				
			}else if (ColetaFiltro.TIPO_NOTA_SAIDA.equals(tipoNota)){
				query.where("notafiscalproduto.tipooperacaonota=?", Tipooperacaonota.SAIDA);				
			}else if (ColetaFiltro.TIPO_NOTA_NENHUM.equals(tipoNota)){
				query.openParentheses();
				query.where("listaEntregadocumentoColeta is null");
				query.where("listaNotafiscalprodutoColeta is null");
				query.closeParentheses();
			}
		}
		
		if (filtro.getMotivodevolucao()!=null){
			query.where("exists (select 1 from Coletamaterialmotivodevolucao cmmd join cmmd.coletaMaterial cm where cm=coletaMaterial and cm.motivodevolucao=?)", filtro.getMotivodevolucao());
		}
	}
	
	/**
	 * Carrega as coletas para a listagem
	 *
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/07/2014
	 */
	public List<Coleta> loadWithLista(String whereIn, String orderBy, boolean asc) {
		QueryBuilder<Coleta> query = query()
					.select("coleta.cdcoleta,coleta.situacaoColeta, coletaMaterial.cdcoletamaterial, coletaMaterial.quantidade, " +
							"coletaMaterial.quantidadedevolvida, coletaMaterial.observacao, coleta.dtconfirmacaodevolucao, " +
							"empresaColeta.cdpessoa, empresaColeta.nome, empresaColeta.nomefantasia,  colaborador.cdpessoa, colaborador.nome, material.cdmaterial, material.nome, " +
							"pedidovendamaterial.cdpedidovendamaterial, fornecedor.cdpessoa, fornecedor.nome, cliente.cdpessoa, cliente.nome, coleta.tipo," +
							"motivodevolucao.cdmotivodevolucao, motivodevolucao.descricao, " +
							"listaEntregadocumentoColeta.cdentregadocumentocoleta, entregadocumento.cdentregadocumento, entregadocumento.entregadocumentosituacao, " +
							"listaNotafiscalprodutoColeta.cdnotafiscalprodutocoleta, notafiscalproduto.cdNota, notafiscalproduto.tipooperacaonota, notaStatus.cdNotaStatus, "+
							"pedidovenda.cdpedidovenda, pedidovenda.identificador, pedidovenda.identificacaoexterna ")
			.join("coleta.colaborador colaborador")
			.leftOuterJoin("coleta.empresa empresaColeta")
			.leftOuterJoin("coleta.fornecedor fornecedor")
			.leftOuterJoin("coleta.cliente cliente")
			.leftOuterJoin("coleta.listaEntregadocumentoColeta listaEntregadocumentoColeta")
			.leftOuterJoin("listaEntregadocumentoColeta.entregadocumento entregadocumento")
			.leftOuterJoin("coleta.listaNotafiscalprodutoColeta listaNotafiscalprodutoColeta")
			.leftOuterJoin("listaNotafiscalprodutoColeta.notafiscalproduto notafiscalproduto")
			.leftOuterJoin("notafiscalproduto.notaStatus notaStatus")
			.leftOuterJoin("coleta.listaColetaMaterial coletaMaterial")
			.leftOuterJoin("coleta.pedidovenda pedidovenda")
			.leftOuterJoin("coletaMaterial.material material")
			.leftOuterJoin("coletaMaterial.pedidovendamaterial pedidovendamaterial")
//			.leftOuterJoin("coletaMaterial.motivodevolucao motivodevolucao")
			.leftOuterJoin("coletaMaterial.listaColetamaterialmotivodevolucao listaColetamaterialmotivodevolucao")
			.leftOuterJoin("listaColetamaterialmotivodevolucao.motivodevolucao motivodevolucao");
		
		SinedUtil.quebraWhereIn("coleta.cdcoleta", whereIn, query);
		
		if (!StringUtils.isEmpty(orderBy)) {
			query.orderBy(orderBy+" "+(asc?"ASC":"DESC"));
		} else {
			query.orderBy("coleta.cdcoleta DESC");
		}
		
		return query.list();
	}

	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaColetaMaterial");
	}

	public List<Coleta> findForEmitirFichaColeta(String whereIn, String whereInPedidoVendaMaterial) throws Exception{
		if(whereIn == null || whereIn.trim().isEmpty()){
			throw new Exception("Wherein n�o pode ser nulo.");
		}
		
		return query()
				.select("coleta.cdcoleta, coleta.tipo, coleta.observacao, coleta.motivodevolucao, colaborador.cdpessoa, colaborador.nome, " +
						"colaboradorPedidovenda.cdpessoa, colaboradorPedidovenda.nome, " +
						"cliente.cdpessoa, cliente.nome, fornecedor.cdpessoa, fornecedor.nome, " +
						"pedidovenda.cdpedidovenda, pedidovenda.dtpedidovenda, pedidovenda.identificador, " +
						"pedidovenda.identificacaoexterna, " +
						"pedidovendaCliente.cdpessoa, pedidovendaCliente.nome, " +
						"entregadocumentoColeta.cdentregadocumentocoleta, entregadocumento.cdentregadocumento, " +
						"colaborador.cdpessoa, colaborador.nome, " +
						"endereco.cdendereco, endereco.logradouro, endereco.bairro, endereco.cep, endereco.caixapostal, endereco.numero, " +
						"endereco.complemento, endereco.pontoreferencia, municipio.cdmunicipio, municipio.nome, uf.cduf, uf.nome, uf.sigla, " +
						"notafiscalprodutoColeta.cdnotafiscalprodutocoleta, notafiscalproduto.cdNota, " +
						"coletaMaterial.cdcoletamaterial, coletaMaterial.quantidade, coletaMaterial.quantidadedevolvida, coletaMaterial.observacao, " +
						"material.cdmaterial, material.nome, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, coletaMaterial.valorunitario, " +
						"materialPedidovenda.cdmaterial, materialPedidovenda.nome, " +
						"producaoetapa.cdproducaoetapa, producaoetapa.nome, unidademedida.cdunidademedida, unidademedida.nome, " +
						"materialcoleta.cdmaterial, materialcoleta.nome, " +
						"motivodevolucao.cdmotivodevolucao, motivodevolucao.descricao, " +
						"pneu.cdpneu, pneu.serie, pneu.dot, pneu.numeroreforma,pneu.lonas,pneu.acompanhaRoda, " +
						"pneumarca.cdpneumarca, pneumarca.nome, " +
						"pneumedida.cdpneumedida, pneumedida.nome, " +
						"pneumodelo.cdpneumodelo, pneumodelo.nome, " +
				 		"pneumaterialbanda.cdmaterial, pneumaterialbanda.identificacao, pneumaterialbanda.nome, pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome, " +
				 		"pedidovendamaterialServicos.cdpedidovendamaterial, pedidovendamaterialServicos.quantidade, pedidovendamaterialServicos.observacao")
				.join("coleta.colaborador colaborador")
				.leftOuterJoin("coleta.cliente cliente")
				.leftOuterJoin("coleta.fornecedor fornecedor")
				.leftOuterJoin("coleta.pedidovenda pedidovenda")
				.leftOuterJoin("pedidovenda.colaborador colaboradorPedidovenda")
				.leftOuterJoin("pedidovenda.endereco endereco")
				.leftOuterJoin("pedidovenda.cliente pedidovendaCliente")
				.leftOuterJoin("endereco.municipio municipio")
				.leftOuterJoin("municipio.uf uf")
				.leftOuterJoin("coleta.listaEntregadocumentoColeta entregadocumentoColeta")
				.leftOuterJoin("entregadocumentoColeta.entregadocumento entregadocumento")
				.leftOuterJoin("coleta.listaNotafiscalprodutoColeta notafiscalprodutoColeta")
				.leftOuterJoin("notafiscalprodutoColeta.notafiscalproduto notafiscalproduto")
				.leftOuterJoin("coleta.listaColetaMaterial coletaMaterial")
				.leftOuterJoin("coletaMaterial.material material")
				.leftOuterJoin("coletaMaterial.localarmazenagem localarmazenagem")
				.leftOuterJoin("coletaMaterial.listaColetaMaterialPedidovendamaterial listaColetaMaterialPedidovendamaterial")
				.leftOuterJoin("listaColetaMaterialPedidovendamaterial.pedidovendamaterial pedidovendamaterialServicos")
//				.leftOuterJoin("coletaMaterial.motivodevolucao motivodevolucao")
//				.leftOuterJoin("coletaMaterial.pedidovendamaterial pedidovendamaterial")
				.leftOuterJoin("pedidovendamaterialServicos.material materialPedidovenda")
				.leftOuterJoin("pedidovendamaterialServicos.unidademedida unidademedida")
				.leftOuterJoin("materialPedidovenda.producaoetapa producaoetapa")
				.leftOuterJoin("pedidovendamaterialServicos.materialcoleta materialcoleta")
				.leftOuterJoin("pedidovendamaterialServicos.pneu pneu")
				.leftOuterJoin("pneu.pneumarca pneumarca")
				.leftOuterJoin("pneu.pneumodelo pneumodelo")
			    .leftOuterJoin("pneu.pneumedida pneumedida")
			    .leftOuterJoin("pneu.materialbanda pneumaterialbanda")
			    .leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
			    .leftOuterJoin("coletaMaterial.listaColetamaterialmotivodevolucao listaColetamaterialmotivodevolucao")
			    .leftOuterJoin("listaColetamaterialmotivodevolucao.motivodevolucao motivodevolucao")
				.whereIn("coleta.cdcoleta", whereIn)
				.whereIn("pedidovendamaterialServicos.cdpedidovendamaterial", whereInPedidoVendaMaterial)
				.list();
	}
	
	/**
	 * Carrega a lista de coleta para a devolu��o
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 24/07/2014
	 */
	public List<Coleta> loadForGerarNotafiscalproduto(String whereIn, String whereInPedidovendamaterial) {
		if(whereIn == null || whereIn.trim().isEmpty()){
			throw new SinedException("Wherein n�o pode ser nulo.");
		}
		
		QueryBuilder<Coleta> query = query()
				.select("coleta.cdcoleta, coleta.motivodevolucao, coletaMaterial.cdcoletamaterial, coletaMaterial.quantidade, coletaMaterial.quantidadedevolvida, coletaMaterial.quantidadedevolvidanota, coletaMaterial.quantidadecomprada, " +
						"empresaColeta.cdpessoa, empresaColeta.nome, colaborador.cdpessoa, colaborador.nome, coleta.observacao, coleta.tipo, " +
						"coletaMaterial.observacao, material.cdmaterial, material.nome, material.identificacao, material.nomenf, material.valorcusto, " +
						"unidademedida.cdunidademedida, unidademedida.nome, localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome, " +
						"pedidovenda.cdpedidovenda, pedidovenda.identificador, cliente.cdpessoa, cliente.nome, cliente.razaosocial, cliente.cpf, cliente.cnpj, cliente.inscricaoestadual, clientePV.cdpessoa, clientePV.nome, " +
						"empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia, coletaMaterial.valorunitario, material.servico, " +
						"material.produto, material.patrimonio, material.epi, " +
						"vendamaterial.cdvendamaterial, vendamaterial.quantidade, venda.cdvenda, vendasituacao.cdvendasituacao, ncmcapitulo.cdncmcapitulo, material.ncmcompleto, " +
						"material.nve, materialgrupo.cdmaterialgrupo, materialgrupo.nome, pedidovendatipo.cdpedidovendatipo, pedidovendatipo.presencacompradornfecoleta, naturezaoperacaonotafiscalentrada.cdnaturezaoperacao, " +
						"enderecoPV.cdendereco, municipioPV.cdmunicipio, motivodevolucao.cdmotivodevolucao, motivodevolucao.descricao, terceiro.cdpessoa, terceiro.nome, " +
						"naturezaoperacaosaidafiscal.cdnaturezaoperacao, naturezaoperacaosaidafiscal.nome, pedidovendatipo.presencacompradornfecoleta, " +
						"vendamaterial.observacao, " +
						"naturezaoperacaosaidafiscal.cdnaturezaoperacao, naturezaoperacaosaidafiscal.nome, pedidovendatipo.presencacompradornfecoleta, pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome, " +
						"vendamaterial.observacao, " +
						"loteestoquePV.cdloteestoque, loteestoquePV.numero, loteestoque.cdloteestoque, loteestoque.numero," +
						"pedidovendaItem.cdpedidovenda, pedidovendamaterialServicos.cdpedidovendamaterial, " +
						"pedidovendamaterialServicos.observacao, pedidovendamaterialServicos.cdpedidovendamaterial, pedidovendamaterialServicos.quantidade")
				.leftOuterJoin("coleta.empresa empresaColeta")
				.leftOuterJoin("coleta.cliente cliente")
				.leftOuterJoin("coleta.pedidovenda pedidovenda")
				.leftOuterJoin("pedidovenda.terceiro terceiro")
				.leftOuterJoin("pedidovenda.endereco enderecoPV")
				.leftOuterJoin("enderecoPV.municipio municipioPV")
				.leftOuterJoin("pedidovenda.cliente clientePV")
				.leftOuterJoin("pedidovenda.empresa empresa")
				.leftOuterJoin("coleta.listaColetaMaterial coletaMaterial")
//				.leftOuterJoin("coletaMaterial.pedidovendamaterial pedidovendamaterial")
//				.leftOuterJoin("pedidovendamaterial.pedidovenda pedidovendaItem")
//				.leftOuterJoin("pedidovendamaterial.loteestoque loteestoquePV")
//				.leftOuterJoin("pedidovendamaterial.listaVendamaterial vendamaterial")
				.leftOuterJoin("coletaMaterial.material material")
				.leftOuterJoin("material.unidademedida unidademedida")
				.leftOuterJoin("material.ncmcapitulo ncmcapitulo")
				.leftOuterJoin("material.materialgrupo materialgrupo")
				.leftOuterJoin("coletaMaterial.localarmazenagem localarmazenagem")
				.leftOuterJoin("pedidovenda.pedidovendatipo pedidovendatipo")
				.leftOuterJoin("pedidovendatipo.naturezaoperacaonotafiscalentrada naturezaoperacaonotafiscalentrada")
				.leftOuterJoin("pedidovendatipo.naturezaoperacaosaidafiscal naturezaoperacaosaidafiscal")
//				.leftOuterJoin("coletaMaterial.motivodevolucao motivodevolucao")
				.leftOuterJoin("coletaMaterial.pneu pneu")
			    .leftOuterJoin("coleta.colaborador colaborador")
			    .leftOuterJoin("coletaMaterial.listaColetamaterialmotivodevolucao listaColetamaterialmotivodevolucao")
			    .leftOuterJoin("listaColetamaterialmotivodevolucao.motivodevolucao motivodevolucao")
			    .leftOuterJoin("coletaMaterial.listaColetaMaterialPedidovendamaterial listaColetaMaterialPedidovendamaterial")
				.leftOuterJoin("listaColetaMaterialPedidovendamaterial.pedidovendamaterial pedidovendamaterialServicos")
				.leftOuterJoin("pedidovendamaterialServicos.pedidovenda pedidovendaItem")
				.leftOuterJoin("pedidovendamaterialServicos.loteestoque loteestoquePV")
				.leftOuterJoin("pedidovendamaterialServicos.listaVendamaterial vendamaterial")
				.leftOuterJoin("vendamaterial.venda venda")
				.leftOuterJoin("venda.vendasituacao vendasituacao")
				.leftOuterJoin("vendamaterial.loteestoque loteestoque")
			    .whereIn("coleta.cdcoleta", whereIn)
			    .whereIn("pedidovendamaterialServicos.cdpedidovendamaterial", StringUtils.isNotBlank(whereInPedidovendamaterial) ? whereInPedidovendamaterial : null)
//				.whereIn("pedidovendamaterial.cdpedidovendamaterial", StringUtils.isNotBlank(whereInPedidovendamaterial) ? whereInPedidovendamaterial : null)
				.orderBy("coleta.cdcoleta, pedidovendamaterialServicos.cdpedidovendamaterial");
			query = SinedUtil.setJoinsByComponentePneu(query);
			return query.list();
	}
	
	/**
	 * Busca as coletas de determinados pedidos de venda.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/07/2014
	 */
	public List<Coleta> findByWhereInPedidovenda(String whereIn, String whereInPedidovendamaterial) {
		if(whereIn == null || whereIn.trim().isEmpty()){
			throw new SinedException("Wherein n�o pode ser nulo.");
		}
		
		return query()
			.select("coleta.cdcoleta, pedidovenda.cdpedidovenda, coletaMaterial.cdcoletamaterial, " +
					"coletaMaterial.quantidadedevolvida, pedidovenda.pedidovendasituacao, pedidovendamaterial.cdpedidovendamaterial, " +
					"pedidovendamaterialServico.cdpedidovendamaterial")
			.leftOuterJoin("coleta.listaColetaMaterial coletaMaterial")
			.leftOuterJoin("coletaMaterial.pedidovendamaterial pedidovendamaterial")
			.leftOuterJoin("coletaMaterial.listaColetaMaterialPedidovendamaterial listaColetaMaterialPedidovendamaterial")
			.leftOuterJoin("listaColetaMaterialPedidovendamaterial.pedidovendamaterial pedidovendamaterialServico")
			.leftOuterJoin("coleta.pedidovenda pedidovenda")
			.whereIn("pedidovenda.cdpedidovenda", whereIn)
			.whereIn("pedidovendamaterialServico.cdpedidovendamaterial", whereInPedidovendamaterial, StringUtils.isBlank(whereInPedidovendamaterial))
			.list();
	}
	/*
	public List<Coleta> findByWhereInVenda(String whereIn) {
		if(whereIn == null || whereIn.trim().isEmpty()){
			throw new SinedException("Wherein n�o pode ser nulo.");
		}
		
		return query()
			.select("coleta.cdcoleta, pedidovenda.cdpedidovenda, coletaMaterial.cdcoletamaterial, " +
					"coletaMaterial.quantidadedevolvida, pedidovenda.pedidovendasituacao, pedidovendamaterial.cdpedidovendamaterial")
			.leftOuterJoin("coleta.listaColetaMaterial coletaMaterial")
			.leftOuterJoin("coletaMaterial.pedidovendamaterial pedidovendamaterial")
			.leftOuterJoin("coleta.pedidovenda pedidovenda")
			.where("pedidovenda.cdpedidovenda in (select pv.cdpedidovenda from Venda v join v.pedidovenda pv where v.cdvenda in (" + whereIn + "))")
			.where("pedidovendamaterial.cdpedidovendamaterial in (select pvm.cdpedidovendamaterial from Vendamaterial vm join vm.pedidovendamaterial pvm join vm.venda v where v.cdvenda in (" + whereIn + "))")
			.list();
	}*/
	
	public List<Coleta> findByWhereInVenda(String whereIn, String whereInVendamaterial) {
		if(whereIn == null || whereIn.trim().isEmpty()){
			throw new SinedException("Wherein n�o pode ser nulo.");
		}
		return query()
			.select("coleta.cdcoleta, pedidovenda.cdpedidovenda, coletaMaterial.cdcoletamaterial, " +
					"coletaMaterial.quantidadedevolvida, pedidovenda.pedidovendasituacao, pedidovendamaterial.cdpedidovendamaterial, " +
					"pedidovendamaterialServico.cdpedidovendamaterial")
			.leftOuterJoin("coleta.listaColetaMaterial coletaMaterial")
			.leftOuterJoin("coletaMaterial.pedidovendamaterial pedidovendamaterial")
			.leftOuterJoin("coleta.pedidovenda pedidovenda")
			.leftOuterJoin("coletaMaterial.listaColetaMaterialPedidovendamaterial listaColetaMaterialPedidovendamaterial")
			.leftOuterJoin("listaColetaMaterialPedidovendamaterial.pedidovendamaterial pedidovendamaterialServico")
			.where("pedidovenda.cdpedidovenda in (select pv.cdpedidovenda from Venda v join v.pedidovenda pv where v.cdvenda in (" + whereIn + "))")
			.where("pedidovendamaterialServico.cdpedidovendamaterial in (select pvm.cdpedidovendamaterial from Vendamaterial vm join vm.pedidovendamaterial pvm join vm.venda v " +
					"where v.cdvenda in (" + whereIn + ")" + (whereInVendamaterial== null ?"": " and vm.cdvendamaterial in (" + whereInVendamaterial + ")")+")")
			.list();
	}

	/**
	 * Carrega coleta para a gera��o do pagamento
	 *
	 * @param coleta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/07/2014
	 */
	public Coleta loadForPagamento(Coleta coleta) {
		return query()
				.select("coleta.cdcoleta, empresa.cdpessoa, empresa.nome, fornecedor.cdpessoa, fornecedor.nome, " +
						"cliente.cdpessoa, cliente.nome, " +
						"contagerencial.cdcontagerencial, coletaMaterial.quantidade, coletaMaterial.quantidadedevolvida, " +
						"coletaMaterial.valorunitario")
				.leftOuterJoin("coleta.empresa empresa")
				.leftOuterJoin("coleta.listaColetaMaterial coletaMaterial")
				.leftOuterJoin("coleta.fornecedor fornecedor")
				.leftOuterJoin("coleta.cliente cliente")
				.leftOuterJoin("fornecedor.contagerencial contagerencial")
				.entity(coleta)
				.unique();
	}

	/**
	 * M�todo que atualiza a data de devolu��o das coletas
	 *
	 * @param whereIn
	 * @param data
	 * @author Rodrigo Freitas
	 * @since 30/07/2014
	 */
	public void updateDtConfirmacaoDevolucao(String whereIn, Date data) {
		getHibernateTemplate().bulkUpdate("update Coleta c set c.dtconfirmacaodevolucao = ? where c.cdcoleta in (" + whereIn + ")", new Object[]{
			data
		});
	}

	/**
	 * Atualiza o campo de motivo de devolu��o.
	 *
	 * @param whereIn
	 * @param motivodevolucao
	 * @author Rodrigo Freitas
	 * @since 27/04/2015
	 */
	public void updateMotivoDevolucao(String whereIn, String motivodevolucao) {
		getHibernateTemplate().bulkUpdate("update Coleta c set c.motivodevolucao = ? where c.cdcoleta in (" + whereIn + ")", new Object[]{
			motivodevolucao
		});
	}
	public void updateSituacaoColeta(Coleta coleta, SituacaoVinculoExpedicao situaColeta) {
		getHibernateTemplate().bulkUpdate("update Coleta c set c.situacaoColeta = ? where c.cdcoleta = "+coleta.getCdcoleta()+"", new Object[]{
			situaColeta
		});
	}
	
	public boolean isColetaPossuiNotaTodosItensSemSaldo(String whereIn, Tipooperacaonota tipooperacaonota){
		List<Coleta> listaColeta = query()
									.select("coleta.cdcoleta, coletaMaterial.quantidade, material.cdmaterial, pedidovendamaterialServicos.cdpedidovendamaterial ")
									.join("coleta.listaColetaMaterial coletaMaterial")
									.join("coletaMaterial.material material")
									.leftOuterJoin("coletaMaterial.listaColetaMaterialPedidovendamaterial listaColetaMaterialPedidovendamaterial")
									.leftOuterJoin("listaColetaMaterialPedidovendamaterial.pedidovendamaterial pedidovendamaterialServicos")
									.whereIn("coleta.cdcoleta", whereIn)
									.list();
		
		boolean todos = true;
		
		for (Coleta coleta: listaColeta){
			Map<ColetamaterialNotaBean, Double> mapaQtdeMaterial = new HashMap<ColetamaterialNotaBean, Double>();
			ColetamaterialNotaBean coletamaterialNotaBean;
			for(ColetaMaterial coletaMaterial: coleta.getListaColetaMaterial()){
				List<Integer> listaCdpedidovendamaterial = new ArrayList<Integer>();
				Integer cdpedidovendamaterial = null;
				
				if(SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())) {
					for(ColetaMaterialPedidovendamaterial itemServico : coletaMaterial.getListaColetaMaterialPedidovendamaterial()) {
						if(itemServico.getPedidovendamaterial() != null) {
							listaCdpedidovendamaterial.add(itemServico.getPedidovendamaterial().getCdpedidovendamaterial());
						}
						if(cdpedidovendamaterial == null) {
							cdpedidovendamaterial = itemServico.getPedidovendamaterial().getCdpedidovendamaterial();
						}
					}
				}
				
				coletamaterialNotaBean = new ColetamaterialNotaBean(coletaMaterial.getMaterial().getCdmaterial(), cdpedidovendamaterial, listaCdpedidovendamaterial);
				
				Double qtde = mapaQtdeMaterial.get(coletamaterialNotaBean);
				if (qtde==null){
					qtde = 0D;
				}
				qtde += coletaMaterial.getQuantidade();
				mapaQtdeMaterial.put(coletamaterialNotaBean, qtde);
			}
			
			for (ColetamaterialNotaBean bean: mapaQtdeMaterial.keySet()){
				Double qtdeColeta = mapaQtdeMaterial.get(bean);
//				String sql = "select coalesce(sum(nfpi.qtde), 0) from nota n join notafiscalproduto nfp on (nfp.cdnota=n.cdnota and n.cdnotastatus not in (3)) join notafiscalprodutoitem nfpi on (nfpi.cdnotafiscalproduto=nfp.cdnota and nfpi.cdmaterial=" + cdmaterial + ") where " +  (tipooperacaonota != null ? "nfp.tipooperacaonota = " + tipooperacaonota.ordinal() : "1=1") + " and exists (select 1 from notafiscalprodutocoleta nfpc where nfpc.cdnotafiscalproduto=nfp.cdnota and nfpc.cdcoleta=" + coleta.getCdcoleta() + ")";
//				Double qtdeNF = (Double) getJdbcTemplate().queryForObject(sql, Double.class);
				Double qtdeNF = getQuantidadeItemNota(coleta.getCdcoleta(), bean.getCdmaterial(), bean.getCdpedidovendamaterial(), tipooperacaonota);
				//Se algum material da coleta possui saldo na NF, ent�o a coleta ainda possui quantidade para gerar nota
				//Se a quantidade do material na coleta for maior que a quantidade de itens da NF, ent�o a coleta ainda possui quantidade suficiente para gerar nota 
				if (qtdeColeta>qtdeNF){
					todos = false;
					break;
				}
			}			
		}
		
		return todos;
	}
	
	public Double getQuantidadeItemNota(Integer cdcoleta, Integer cdmaterial, Integer cdpedidovendamaterial, Tipooperacaonota tipooperacaonota){
		String sqlNfp = " select coalesce(sum(nfpi.qtde), 0) " +
					 " from nota n " +
					 " join notafiscalproduto nfp on nfp.cdnota=n.cdnota " +
					 " join notafiscalprodutoitem nfpi on nfpi.cdnotafiscalproduto=nfp.cdnota " +
					 " where " +  (tipooperacaonota != null ? "nfp.tipooperacaonota = " + tipooperacaonota.ordinal() : "1=1") + 
					 " and n.cdnotastatus not in (" + NotaStatus.CANCELADA.getCdNotaStatus() + ") " +
					 " and nfpi.cdmaterial = " + cdmaterial +
					 (cdpedidovendamaterial != null ? " and nfpi.cdpedidovendamaterial = " + cdpedidovendamaterial : " ") +
					 "and (" +
					 "  exists ( select 1 " +
					 "				from notafiscalprodutocoleta nfpc " +
					 "				left outer join coletamaterial cm on cm.cdcoleta = nfpc.cdcoleta " +
					 "				left outer join coletamaterialpedidovendamaterial cmpvm on cmpvm.cdcoletamaterial = cm.cdcoletamaterial " +
					 "				left outer join nota nColeta on nColeta.cdnota = nfpc.cdnotafiscalproduto " +
					 " 				where nfpc.cdnotafiscalproduto = nfp.cdnota " +
					 "				and nfpc.cdcoleta = " + cdcoleta + 
					 "				and cmpvm.cdpedidovendamaterial = nfpi.cdpedidovendamaterial " +
					 " 				and nColeta.cdnotastatus not in (" + NotaStatus.CANCELADA.getCdNotaStatus() + ") " +
					 "				and nfpi.cdmaterial =" + cdmaterial + ") " +
					 " 		or " +
					 "  exists ( select 1 " +
					 "				from entregadocumentohistorico edh " +
					 " 				join entregadocumentocoleta edColeta on edColeta.cdentregadocumento = edh.cdentregadocumento " +
					 "				join coletamaterial cm on cm.cdcoleta = edColeta.cdcoleta " +
					 "				left outer join coletamaterialpedidovendamaterial cmpvm on cmpvm.cdcoletamaterial = cm.cdcoletamaterial " +
					 "				join nota nColeta on nColeta.cdnota = edh.cdnotafiscalproduto " +
					 " 				where edh.cdnotafiscalproduto = nfp.cdnota " +
					 "				and edColeta.cdcoleta = " + cdcoleta + 
					 "				and cmpvm.cdpedidovendamaterial = nfpi.cdpedidovendamaterial " +
					 " 				and nColeta.cdnotastatus not in (" + NotaStatus.CANCELADA.getCdNotaStatus() + ") " +
					 "				and nfpi.cdmaterial =" + cdmaterial + ") " +
					 ") ";

		SinedUtil.markAsReader();
		Double qtdeNfp = (Double) getJdbcTemplate().queryForObject(sqlNfp, Double.class);
		
		Double qtdeEf = 0d;
		if(Tipooperacaonota.ENTRADA.equals(tipooperacaonota)){
			String sqlEf = " select coalesce(sum(em.qtde), 0) " +
			 " from entregadocumento ed " +
			 " join entregamaterial em on em.cdentregadocumento = ed.cdentregadocumento " +
			 " where ed.entregadocumentosituacao not in (" + Entregadocumentosituacao.CANCELADA.ordinal() + ") " +
			 " and em.cdmaterial = " + cdmaterial +
			 (cdpedidovendamaterial != null ? " and em.cdpedidovendamaterial = " + cdpedidovendamaterial : " ") +
			 " and exists ( select 1 " +
			 "				from entregadocumentocoleta edc " +
			 "				left outer join coletamaterial cm on cm.cdcoleta = edc.cdcoleta " +
			 "				left outer join coletamaterialpedidovendamaterial cmpvm on cmpvm.cdcoletamaterial = cm.cdcoletamaterial " +
			 " 				where ed.cdentregadocumento = edc.cdentregadocumento " +
			 "				and edc.cdcoleta = " + cdcoleta + 
			 "				and cmpvm.cdpedidovendamaterial = em.cdpedidovendamaterial " +
			 "				and em.cdmaterial =" + cdmaterial + ")";

			SinedUtil.markAsReader();
			qtdeEf = (Double) getJdbcTemplate().queryForObject(sqlEf, Double.class);
		}
		
		return qtdeNfp + qtdeEf;
	}
	
	public List<Coleta> findAllWithColetaMaterial(Date dateToSearch) {
		return query()
				.select("coleta.cdcoleta, empresa.cdpessoa ")
				.leftOuterJoin("coleta.listaColetaMaterial listaColetaMaterial")
				.leftOuterJoin("coleta.empresa empresa")
				.wherePeriodo("coleta.dtaltera", dateToSearch, SinedDateUtils.currentDate())
				.where("coleta.dtconfirmacaodevolucao is null")
				.where("coalesce(listaColetaMaterial.quantidadedevolvida, 0) > 0")
				.list();
	}

	public List<Coleta> findByWhereIn(String whereIn) throws Exception {
		if(whereIn == null || whereIn.trim().isEmpty()){
			throw new Exception("Wherein n�o pode ser nulo.");
		}
		return query()
				.select("coleta.cdcoleta,coleta.situacaoColeta," +
						"listaColetaMaterial.cdcoletamaterial,listaColetaMaterial.quantidade," +
						"pedidovenda.cdpedidovenda,pedidovenda.identificacaoexterna," +
						"cliente.cdpessoa,cliente.nome," +
						"pneu.cdpneu,pneu.serie,pneu.dot,pneu.numeroreforma," +
						"pneumarca.cdpneumarca,pneumarca.nome," +
						"pneumodelo.cdpneumodelo,pneumodelo.nome," +
						"pneumedida.cdpneumedida,pneumedida.nome," +
						"materialbanda.cdmaterial,materialbanda.nome," +
						"pneuqualificacao.cdpneuqualificacao,pneuqualificacao.nome," +
						"localarmazenagem.cdlocalarmazenagem,localarmazenagem.nome," +
						"material.cdmaterial,material.nome," +
						"unidademedida.cdunidademedida,unidademedida.nome,unidademedida.simbolo")
				.leftOuterJoin("coleta.listaColetaMaterial listaColetaMaterial")
				.leftOuterJoin("coleta.pedidovenda pedidovenda")
				.leftOuterJoin("coleta.cliente cliente")
				.leftOuterJoin("listaColetaMaterial.pneu pneu")
				.leftOuterJoin("pneu.pneumarca pneumarca")
				.leftOuterJoin("pneu.materialbanda materialbanda")
				.leftOuterJoin("pneu.pneumodelo pneumodelo")
				.leftOuterJoin("pneu.pneumedida pneumedida")
				.leftOuterJoin("pneu.pneuqualificacao pneuqualificacao")
				.leftOuterJoin("listaColetaMaterial.localarmazenagem localarmazenagem")
				.leftOuterJoin("listaColetaMaterial.material material")
				.leftOuterJoin("material.unidademedida unidademedida")
				.whereIn("coleta.cdcoleta", whereIn)
				.list();
	}

	public Coleta findById(Integer id) {
		return query()
				.select("coleta.cdcoleta," +
						"listaColetaMaterial.cdcoletamaterial")
				.leftOuterJoin("coleta.listaColetaMaterial listaColetaMaterial")	
				.where("coleta.cdcoleta = ?", id)
				.unique();
	}

	
}
