package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Contatotipo;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class PessoaContatoDAO extends GenericDAO<PessoaContato> {

	public List<PessoaContato> findByPessoa(Pessoa pessoa) {
		return query()
				.select("pessoaContato.cdpessoacontato, contato.cdpessoa, contato.nome, contato.emailcontato, contato.observacao, contato.dtnascimento, " +
						"munContato.cdmunicipio, munContato.nome, ufContato.cduf, ufContato.sigla, contato.email, contato.receberboleto, contato.recebernf, " +
						"telefone.cdtelefone, telefone.telefone, telefonetipo.cdtelefonetipo, contato.ativo, empresa.cdpessoa, " +
						"endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.bairro, endereco.caixapostal," +
						"endereco.cep,endereco.complemento, munEndereco.cdmunicipio, munEndereco.nome, ufEndereco.cduf, " +
						"ufEndereco.sigla, enderecotipo.cdenderecotipo, contatotipo.cdcontatotipo, endereco.pontoreferencia," +
						"contatotipo.nome, contato.participacao, contato.quotas, contatotipo.socio, contato.cpf, contato.identidade, contato.responsavelos, " +
						"pessoa.cdpessoa")
						
				.leftOuterJoin("pessoaContato.pessoa pessoa")
				.leftOuterJoin("pessoaContato.contato contato")
				
				.leftOuterJoin("contato.municipio munContato")
				.leftOuterJoin("munContato.uf ufContato")
				
				.leftOuterJoin("contato.contatotipo contatotipo")
				
				.leftOuterJoin("contato.listaTelefone telefone")
				.leftOuterJoin("telefone.telefonetipo telefonetipo")
				
				.leftOuterJoin("contato.listaEndereco endereco")
				.leftOuterJoin("endereco.municipio munEndereco")
				.leftOuterJoin("munEndereco.uf ufEndereco")
				.leftOuterJoin("endereco.enderecotipo enderecotipo")
				.leftOuterJoin("contato.empresa empresa")
				
				.where("pessoaContato.pessoa = ?", pessoa)
				.list();
	}

	public List<PessoaContato> findByPessoaContatotipo(String cdpessoa, Contatotipo contatoTipo) {
		if (StringUtils.isEmpty(cdpessoa)) {
			throw new SinedException("Pessoa n�o pode ser nulo.");
		}
		return query()
				.select("pessoaContato.cdpessoacontato, contato.cdpessoa, contato.nome, contato.emailcontato, contato.observacao, contato.dtnascimento, " +
						"munContato.cdmunicipio, munContato.nome, ufContato.cduf, ufContato.sigla, contato.email, " +
						"telefone.cdtelefone, telefone.telefone, telefonetipo.cdtelefonetipo," +
						"endereco.cdendereco, endereco.logradouro, endereco.numero, endereco.bairro, endereco.caixapostal," +
						"endereco.cep,endereco.complemento, munEndereco.cdmunicipio, munEndereco.nome, ufEndereco.cduf, " +
						"ufEndereco.sigla, enderecotipo.cdenderecotipo, contatotipo.cdcontatotipo, endereco.pontoreferencia," +
						"contatotipo.nome, contato.participacao, contato.quotas, contatotipo.socio, contato.cpf, contato.identidade, contato.responsavelos, " +
						"pessoa.cdpessoa")
						
					.leftOuterJoin("pessoaContato.pessoa pessoa")
					.leftOuterJoin("pessoaContato.contato contato")
					
					.leftOuterJoin("contato.municipio munContato")
					.leftOuterJoin("munContato.uf ufContato")
					
					.leftOuterJoin("contato.contatotipo contatotipo")
					
					.leftOuterJoin("contato.listaTelefone telefone")
					.leftOuterJoin("telefone.telefonetipo telefonetipo")
					
					.leftOuterJoin("contato.listaEndereco endereco")
					.leftOuterJoin("endereco.municipio munEndereco")
					.leftOuterJoin("munEndereco.uf ufEndereco")
					.leftOuterJoin("endereco.enderecotipo enderecotipo")
					
					.whereIn("pessoa.cdpessoa", cdpessoa)
					.where("contatotipo = ?",contatoTipo)
					.orderBy("contato.nome")
					
					.list();
	}

	public List<PessoaContato> findContatoByPessoaAndContato(Integer cdpessoa, Integer cdcontato) {
		return query()
				.leftOuterJoin("pessoaContato.pessoa pessoa")
				.leftOuterJoin("pessoaContato.contato contato")
				.where("pessoa.cdpessoa = ?", cdpessoa)
				.where("pessoa.cdpessoa = ?", cdcontato)
				.list();
	}
}
