package br.com.linkcom.sined.geral.dao;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Tabelaimpostoitem;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.TabelaimpostoitemFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TabelaimpostoitemDAO extends GenericDAO<Tabelaimpostoitem> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Tabelaimpostoitem> query, FiltroListagem _filtro) {
		TabelaimpostoitemFiltro filtro = (TabelaimpostoitemFiltro) _filtro;
		
		query
			.where("tabelaimpostoitem.tabelaimposto = ?", filtro.getTabelaimposto())
			.where("tabelaimpostoitem.tabelaimposto.empresa = ?", filtro.getEmpresa())
			.where("tabelaimpostoitem.tipo = ?", filtro.getTipo())
			.whereLike("tabelaimpostoitem.codigo", filtro.getCodigo());
	}
	
}
