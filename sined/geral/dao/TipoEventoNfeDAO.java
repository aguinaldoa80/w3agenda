package br.com.linkcom.sined.geral.dao;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.TipoEventoNfe;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class TipoEventoNfeDAO extends GenericDAO<TipoEventoNfe> {

	public TipoEventoNfe procurarPorCodigo(String codigo) {
		if (StringUtils.isEmpty(codigo)) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("tipoEventoNfe.cdTipoEventoNfe, tipoEventoNfe.codigo, tipoEventoNfe.descricao ")
				.where("tipoEventoNfe.codigo = ?", codigo)
				.unique();
	}
}