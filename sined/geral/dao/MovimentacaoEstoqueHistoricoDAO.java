package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.MovimentacaoEstoqueHistorico;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MovimentacaoEstoqueHistoricoDAO extends
		GenericDAO<MovimentacaoEstoqueHistorico> {

	public List<MovimentacaoEstoqueHistorico> findByMovimentacaoEstoque(Movimentacaoestoque movimentacaoEstoque) {
		if (movimentacaoEstoque == null || movimentacaoEstoque.getCdmovimentacaoestoque() == null) {
			throw new SinedException("A movimenta��o de estoque n�o pode ser nula.");
		}
		return query()
				.select("movimentacaoEstoqueHistorico.cdMovimentacaoEstoqueHistorico, movimentacaoEstoqueHistorico.observacao, "
						+ "movimentacaoEstoqueHistorico.cdusuarioaltera, movimentacaoEstoqueHistorico.movimentacaoEstoqueAcao, "
						+ "movimentacaoEstoqueHistorico.dtaltera, movimentacaoestoque.cdmovimentacaoestoque")
				.leftOuterJoin("movimentacaoEstoqueHistorico.movimentacaoEstoque movimentacaoestoque")
				.where("movimentacaoestoque = ?", movimentacaoEstoque)
				.orderBy("movimentacaoEstoqueHistorico.dtaltera desc").list()
				;
	}
}
