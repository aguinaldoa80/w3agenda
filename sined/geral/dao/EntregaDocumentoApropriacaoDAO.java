package br.com.linkcom.sined.geral.dao;

import java.util.List;

import br.com.linkcom.sined.geral.bean.EntregaDocumentoApropriacao;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class EntregaDocumentoApropriacaoDAO extends GenericDAO<EntregaDocumentoApropriacao>{

	public List<EntregaDocumentoApropriacao> findByEntregaDocumento(Entregadocumento entregaDocumento){
		return query()
				.where("entregaDocumentoApropriacao.entregaDocumento = ?", entregaDocumento)
				.list();
	}
}
