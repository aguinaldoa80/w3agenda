package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clientehistorico;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.AtividadetipoSituacaoEnum;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ClientehistoricoDAO extends GenericDAO<Clientehistorico>{

	private EmpresaService empresaService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	  
	public List<Clientehistorico> findByCliente(Cliente cliente, Atividadetipo atividadetipo, Empresa empresa) {
		if(cliente == null || cliente.getCdpessoa() == null)
			throw new SinedException("Cliente n�o pode ser nulo.");
		
		return querySined()
				
				.select("clientehistorico.dtaltera, clientehistorico.observacao, meiocontato.nome, " +
						"atividadetipo.nome, atividadetipo.reclamacao, contato.nome, usuarioaltera.cdpessoa, usuarioaltera.nome, clientehistorico.cdusuarioaltera," +
						"clientehistorico.situacao, situacaohistorico.cdsituacaohistorico, situacaohistorico.descricao, clientehistorico.cdclientehistorico, " +
						"empresa.nome, contrato.descricao ")
				.leftOuterJoin("clientehistorico.meiocontato meiocontato")
				.leftOuterJoin("clientehistorico.atividadetipo atividadetipo")
				.leftOuterJoin("clientehistorico.contato contato")
				.leftOuterJoin("clientehistorico.usuarioaltera usuarioaltera")
				.leftOuterJoin("clientehistorico.situacaohistorico situacaohistorico")
				.leftOuterJoin("clientehistorico.empresahistorico empresa")
				.leftOuterJoin("clientehistorico.contrato contrato")
				.where("clientehistorico.cliente=?",cliente)
				.where("atividadetipo = ?", atividadetipo)
				.where("empresa = ?", empresa)
				.orderBy("clientehistorico.dtaltera desc")
				.list();
	}

	/**
	 * M�todo que busca o hist�rico do cliente de acordo com o tipo de atividade
	 *
	 * @param cliente
	 * @param atividadetipo
	 * @return
	 * @author Luiz Fernando
	 * @param dtfim 
	 * @param dtinicio 
	 */
	public List<Clientehistorico> findForRelatorioHistoricoCliente(Cliente cliente, Atividadetipo atividadetipo, Empresa empresa, Date dtinicio, Date dtfim, AtividadetipoSituacaoEnum situacao) {
		if(cliente == null || cliente.getCdpessoa() == null)
			throw new SinedException("Cliente n�o pode ser nulo");
		
		String whereInEmpresas = (empresa != null)? empresa.getCdpessoa().toString():
								CollectionsUtil.listAndConcatenate(empresaService.findByUsuario(), "cdpessoa", ",");
			
		return querySined()
			.select("cliente.cdpessoa, cliente.nome, clientehistorico.dtaltera, clientehistorico.observacao, usuarioaltera.cdpessoa, usuarioaltera.nome," +
					"atividadetipo.nome, clientehistorico.situacao, empresa.nome")
			.join("clientehistorico.cliente cliente")
			.leftOuterJoin("clientehistorico.atividadetipo atividadetipo")
			.leftOuterJoin("clientehistorico.usuarioaltera usuarioaltera")
			.leftOuterJoin("clientehistorico.empresahistorico empresa")
			.where("cliente = ?", cliente)
			.where("atividadetipo = ?", atividadetipo)
			.where("clientehistorico.situacao = ?", situacao)
			.where("clientehistorico.dtaltera >= ?", SinedDateUtils.dateToTimestampInicioDia(dtinicio))
			.where("clientehistorico.dtaltera <= ?", SinedDateUtils.dateToTimestampFinalDia(dtfim))
			.openParentheses()
				.where("empresa.cdpessoa is null")
				.or()
				.whereIn("empresa.cdpessoa", whereInEmpresas)
			.closeParentheses()
			.orderBy("clientehistorico.dtaltera desc")
			.list();
	}

	/**
	* M�todo que carrega os hist�ricos dos clientes
	*
	* @param whereIn
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<Clientehistorico> findForAndroid(String whereIn) {
		return query()
			.select("clientehistorico.cdclientehistorico, clientehistorico.dtaltera, clientehistorico.observacao, clientehistorico.situacao, " +
					"meiocontato.cdmeiocontato, contato.cdpessoa, cliente.cdpessoa, usuarioaltera.cdpessoa, " +
					"usuarioaltera.nome, atividadetipo.cdatividadetipo, situacaohistorico.cdsituacaohistorico ")
			.join("clientehistorico.cliente cliente")
			.leftOuterJoin("clientehistorico.atividadetipo atividadetipo")
			.leftOuterJoin("clientehistorico.usuarioaltera usuarioaltera")
			.leftOuterJoin("clientehistorico.meiocontato meiocontato")
			.leftOuterJoin("clientehistorico.contato contato")
			.leftOuterJoin("clientehistorico.situacaohistorico situacaohistorico")
			.leftOuterJoin("clientehistorico.empresahistorico empresa")
			.whereIn("clientehistorico.cdclientehistorico", whereIn)
			.list();
	}
}
