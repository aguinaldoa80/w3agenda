package br.com.linkcom.sined.geral.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.sined.geral.bean.Arquivonf;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ArquivonfFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

public class ArquivonfDAO extends GenericDAO<Arquivonf> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Arquivonf> query, FiltroListagem _filtro) {
		ArquivonfFiltro filtro = (ArquivonfFiltro) _filtro;
		
		Integer idNota = null;
		String cdNota = NeoWeb.getRequestContext().getParameter("cdNota");
		if(cdNota != null && !cdNota.equals("")){
			idNota = Integer.parseInt(cdNota);
		}
		
		if(filtro.getIds() != null && !filtro.getIds().equals("")){
			while(filtro.getIds().indexOf(",,") != -1){
				filtro.setIds(filtro.getIds().replaceAll(",,", ","));
			}
			
			if(filtro.getIds().substring(0, 1).equals(",")){
				filtro.setIds(filtro.getIds().substring(1, filtro.getIds().length()));
			}
			
			if(filtro.getIds().substring(filtro.getIds().length() - 1, filtro.getIds().length()).equals(",")){
				filtro.setIds(filtro.getIds().substring(0, filtro.getIds().length()-1));
			}
		}
		
		query
			.select("distinct arquivonf.cdarquivonf, arquivonf.dtenvio, arquivonf.numerorecibo, arquivonf.numeroprotocolo, arquivonf.arquivonfsituacao, " +
					"arquivoxml.cdarquivo, arquivoxml.nome, " +
					"arquivoxmlassinado.cdarquivo, arquivoxmlassinado.nome, " +
					"arquivoretornoenvio.cdarquivo, arquivoretornoenvio.nome, " +
					"arquivoretornoconsulta.cdarquivo, arquivoretornoconsulta.nome, " +
					"configuracaonfe.tipoconfiguracaonfe, configuracaonfe.descricao, configuracaonfe.prefixowebservice")
			.leftOuterJoin("arquivonf.arquivoxml arquivoxml")
			.leftOuterJoin("arquivonf.arquivoxmlassinado arquivoxmlassinado")
			.leftOuterJoin("arquivonf.arquivoretornoenvio arquivoretornoenvio")
			.leftOuterJoin("arquivonf.arquivoretornoconsulta arquivoretornoconsulta")
			.leftOuterJoin("arquivonf.configuracaonfe configuracaonfe")
			.leftOuterJoin("arquivonf.listaArquivonfnota arquivonfnota")
			.leftOuterJoin("arquivonfnota.nota nota")
			.leftOuterJoin("nota.empresa empresa")
			.where("configuracaonfe.tipoconfiguracaonfe = ?", filtro.getTipoconfiguracaonfe())
			.where("configuracaonfe = ?", filtro.getConfiguracaonfe())
			.wherePeriodo("arquivonf.dtenvio", filtro.getDtenvio1(), filtro.getDtenvio2())
			.where("nota.numero = ?", filtro.getNumeroNota())
			.where("nota.cdNota = ?", idNota)
			.where("empresa = ?", filtro.getEmpresa())
			.whereIn("arquivonf.cdarquivonf", SinedUtil.montaWhereInInteger(filtro.getIds()))
			.orderBy("arquivonf.cdarquivonf desc")
			.ignoreJoin("arquivonfnota", "nota", "empresa");
		
		if(filtro.getListaSituacao() != null) {
			query.whereIn("arquivonf.arquivonfsituacao", Arquivonfsituacao.listAndConcatenate(filtro.getListaSituacao()));
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Arquivonf> query) {
		// TODO SinedUtil.desmarkAsReader() - #erroReader verificar motivo porque executou insert no processo usando o reader (https://budini.w3erp.com.br/w3erp/faturamento/crud/NotaFiscalServico?ACAO=consultar&cdNota=72177) 12/08/2020 16:59  
		SinedUtil.desmarkAsReader();
		query
			.leftOuterJoinFetch("arquivonf.listaArquivonfnota listaArquivonfnota")
			.leftOuterJoinFetch("listaArquivonfnota.nota nota")
			.leftOuterJoinFetch("listaArquivonfnota.arquivoxmlcancelamento arquivoxmlcancelamento")
			.leftOuterJoinFetch("arquivonf.arquivoxml arquivoxml")
			.leftOuterJoinFetch("arquivonf.arquivoxmlconsultasituacao arquivoxmlconsultasituacao")
			.leftOuterJoinFetch("arquivonf.arquivoxmlconsultalote arquivoxmlconsultalote")
			.leftOuterJoinFetch("arquivonf.arquivoxmlassinado arquivoxmlassinado")
			.leftOuterJoinFetch("arquivonf.arquivoretornoenvio arquivoretornoenvio")
			.leftOuterJoinFetch("arquivonf.arquivoretornoconsulta arquivoretornoconsulta")
			.leftOuterJoinFetch("arquivonf.arquivoretornoconsultasituacao arquivoretornoconsultasituacao")
			.leftOuterJoinFetch("arquivonf.configuracaonfe configuracaonfe")
			.leftOuterJoinFetch("configuracaonfe.uf uf");
	}
	
	/**
	 * Busca os arquivos novos para emiss�o de NF-e por empresa.
	 *
	 * @param empresa
	 * @return
	 * @since 09/08/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonf> findArquivonfNovosForEmissao(Empresa empresa){
		QueryBuilder<Arquivonf> query = querySined();
		
		query
				.select("arquivonf.cdarquivonf, arquivoxml.cdarquivo, configuracaonfe.cdconfiguracaonfe, arquivonf.tipocontigencia")
				.leftOuterJoin("arquivonf.empresa empresa")
				.leftOuterJoin("arquivonf.arquivoxml arquivoxml")
				.leftOuterJoin("arquivonf.arquivoxmlassinado arquivoxmlassinado")
				.leftOuterJoin("arquivonf.configuracaonfe configuracaonfe")
				.where("empresa = ?", empresa)
				.where("arquivoxml is not null")
				.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.GERADO)
				.openParentheses()
					.where("arquivonf.emitindo = ?", Boolean.FALSE)
					.or()
					.where("arquivonf.emitindo is null")
				.closeParentheses();
		
		Prefixowebservice[] prefixosNaoTemWebservice = new Prefixowebservice[]{
				Prefixowebservice.VINHEDOISS_PROD,
				Prefixowebservice.BRUMADINHOISS_PROD,
				Prefixowebservice.IGARAPEISS_PROD,
				Prefixowebservice.LAVRASISS_HOM,
				Prefixowebservice.LAVRASISS_PROD,
				Prefixowebservice.PARACATUISS_PROD,
				Prefixowebservice.ARANDU_HOM,
				Prefixowebservice.ARANDU_PROD,
				Prefixowebservice.CABEDELOISS_HOM,
				Prefixowebservice.CABEDELOISS_PROD,
				Prefixowebservice.SIMOESFILHOISS_HOM,
				Prefixowebservice.SIMOESFILHOISS_PROD,
				Prefixowebservice.LIMEIRA_HOM,
				Prefixowebservice.LIMEIRA_PROD,
				Prefixowebservice.VOTORANTIMISS_PROD, 
				Prefixowebservice.MOGIMIRIMISS_PROD,
				Prefixowebservice.SETELAGOAS_PROD,
				Prefixowebservice.SANTANADEPARNAIBA_PROD,
				Prefixowebservice.SERRATALHADA_HOM,
				Prefixowebservice.SERRATALHADA_PROD,
				Prefixowebservice.MARABA_HOM,
				Prefixowebservice.MARABA_PROD
		}; 
		for (int i = 0; i < prefixosNaoTemWebservice.length; i++) {
			query.where("configuracaonfe.prefixowebservice <> ?", prefixosNaoTemWebservice[i]);
		}
		
		return query.list();
	}

	/**
	 * Busca a lista de arquivos de NF que foram gerados para serem assinados na aplica��o desktop de NF-e.
	 *
	 * @return
	 * @since 01/02/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonf> findGerados() {
		return query()
					.select("arquivonf.cdarquivonf, arquivoxml.cdarquivo, configuracaonfe.cdconfiguracaonfe")
					.leftOuterJoin("arquivonf.arquivoxml arquivoxml")
					.leftOuterJoin("arquivonf.arquivoxmlassinado arquivoxmlassinado")
					.leftOuterJoin("arquivonf.configuracaonfe configuracaonfe")
					.where("arquivoxmlassinado is null")
					.openParentheses()
					.where("arquivonf.assinando = ?", Boolean.FALSE)
					.or()
					.where("arquivonf.assinando is null")
					.closeParentheses()
					.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.GERADO)
					.list();
	}
	
	/**
	 * Busca a lista de arquivos de NF que foram assinados para serem enviados na aplica��o desktop de NF-e.
	 *
	 * @return
	 * @since 01/02/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonf> findAssinados() {
		return query()
					.select("arquivonf.cdarquivonf, arquivoxmlassinado.cdarquivo, configuracaonfe.cdconfiguracaonfe")
					.leftOuterJoin("arquivonf.arquivoxmlassinado arquivoxmlassinado")
					.leftOuterJoin("arquivonf.configuracaonfe configuracaonfe")
					.where("arquivoxmlassinado is not null")
					.openParentheses()
					.where("arquivonf.enviando = ?", Boolean.FALSE)
					.or()
					.where("arquivonf.enviando is null")
					.closeParentheses()
					.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.GERADO)
					.list();
	}
	
	/**
	 * Busca a lista de arquivos de NF que foram enviados para serem consultados na aplica��o desktop de NF-e.
	 *
	 * @return
	 * @since 15/02/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonf> findEnviados() {
		return query()
					.select("arquivonf.cdarquivonf, arquivoxmlconsultalote.cdarquivo, arquivoxmlconsultasituacao.cdarquivo, configuracaonfe.cdconfiguracaonfe")
					.leftOuterJoin("arquivonf.arquivoxmlconsultasituacao arquivoxmlconsultasituacao")
					.leftOuterJoin("arquivonf.arquivoxmlconsultalote arquivoxmlconsultalote")
					.leftOuterJoin("arquivonf.configuracaonfe configuracaonfe")
					.where("arquivoxmlconsultalote is not null")
					.openParentheses()
					.where("arquivonf.consultando = ?", Boolean.FALSE)
					.or()
					.where("arquivonf.consultando is null")
					.closeParentheses()
					.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.ENVIADO)
					.list();
	}

	/**
	 * Marca a flag no Arquivonf dependendo do nome da propriedade que vem no bean.
	 *
	 * @param bean
	 * @return
	 * @since 06/02/2012
	 * @author Rodrigo Freitas
	 */
	public int marcarArquivonf(String whereIn, String flag) {
		return getHibernateTemplate()
				.bulkUpdate("update Arquivonf a set a." + flag + " = ? where a.cdarquivonf in (" + whereIn + ") and (a." + flag + " = ? or a." + flag + " is null)", 
							new Object[]{Boolean.TRUE, Boolean.FALSE});
	}
	
	/**
	 * Desmarca a flag no Arquivonf dependendo do nome da propriedade que vem no bean.
	 *
	 * @param bean
	 * @return
	 * @since 06/02/2012
	 * @author Rodrigo Freitas
	 */
	public void desmarcarArquivonf(String whereIn, String flag) {
		getHibernateTemplate().bulkUpdate("update Arquivonf a set a." + flag + " = ? where a.cdarquivonf in (" + whereIn + ")", 
				new Object[]{Boolean.FALSE});
	}

	/**
	 * Atualiza o XML assinado do Arquivonf.
	 *
	 * @param arquivonf
	 * @since 08/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoxmlassinado(Arquivonf arquivonf) {
		getHibernateTemplate().bulkUpdate("update Arquivonf a set a.arquivoxmlassinado = ? where a.id = ?", 
				new Object[]{arquivonf.getArquivoxmlassinado(), arquivonf.getCdarquivonf()});
	}
	
	/**
	 * Atualiza o XML do retorno do envio do Arquivonf.
	 *
	 * @param arquivonf
	 * @since 08/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoretornoenvio(Arquivonf arquivonf) {
		getHibernateTemplate().bulkUpdate("update Arquivonf a set a.arquivoretornoenvio = ?, dtenvio = ? where a.id = ?", 
				new Object[]{arquivonf.getArquivoretornoenvio(), SinedDateUtils.currentDate(), arquivonf.getCdarquivonf()});
	}
	
	/**
	 * Atualiza o XML da consulta da situa��o do Arquivonf.
	 *
	 * @param arquivonf
	 * @since 08/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoxmlconsultasituacao(Arquivonf arquivonf) {
		getHibernateTemplate().bulkUpdate("update Arquivonf a set a.arquivoxmlconsultasituacao = ? where a.id = ?", 
				new Object[]{arquivonf.getArquivoxmlconsultasituacao(), arquivonf.getCdarquivonf()});
	}
	
	/**
	 * Atualiza o XML da consulta do lote do Arquivonf.
	 *
	 * @param arquivonf
	 * @since 08/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoxmlconsultalote(Arquivonf arquivonf) {
		getHibernateTemplate().bulkUpdate("update Arquivonf a set a.arquivoxmlconsultalote = ? where a.id = ?", 
				new Object[]{arquivonf.getArquivoxmlconsultalote(), arquivonf.getCdarquivonf()});
	}
	
	/**
	 * Atualiza o XML do retorno da consulta do lote do Arquivonf.
	 *
	 * @param arquivonf
	 * @since 15/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoretornoconsulta(Arquivonf arquivonf) {
		getHibernateTemplate().bulkUpdate("update Arquivonf a set a.arquivoretornoconsulta = ? where a.id = ?", 
				new Object[]{arquivonf.getArquivoretornoconsulta(), arquivonf.getCdarquivonf()});
	}
	
	/**
	 * Atualiza o XML do retorno da consulta da situa��o do lote do Arquivonf.
	 *
	 * @param arquivonf
	 * @since 15/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoretornoconsultasituacao(Arquivonf arquivonf) {
		getHibernateTemplate().bulkUpdate("update Arquivonf a set a.arquivoretornoconsultasituacao = ? where a.id = ?", 
				new Object[]{arquivonf.getArquivoretornoconsultasituacao(), arquivonf.getCdarquivonf()});
	}

	/**
	 * Atualiza o n�mero de protocolo do Arquivonf.
	 *
	 * @param arquivonf
	 * @param protocoloStr
	 * @since 13/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateProtocolo(Arquivonf arquivonf, String protocoloStr) {
		getHibernateTemplate().bulkUpdate("update Arquivonf a set a.numeroprotocolo = ? where a.id = ?", 
				new Object[]{protocoloStr, arquivonf.getCdarquivonf()});
	}

	/**
	 * Atualiza a situa��o do Arquivonf.
	 *
	 * @param arquivonf
	 * @param situacao
	 * @since 13/02/2012
	 * @author Rodrigo Freitas
	 */
	public void updateSituacao(Arquivonf arquivonf, Arquivonfsituacao situacao) {
		getHibernateTemplate().bulkUpdate("update Arquivonf a set a.arquivonfsituacao = ? where a.id = ?", 
				new Object[]{situacao, arquivonf.getCdarquivonf()});
	}
	
	public void updateEnviando(String whereIn, Boolean enviando) {
		if(StringUtils.isNotBlank(whereIn)){
			getHibernateTemplate().bulkUpdate("update Arquivonf a set a.enviando = ? where a.id in (" + whereIn + ")", 
				new Object[]{enviando});
		}
	}

	/**
	 * Atualiza o n�mero do recibo e a data de recebimento do Arquivonf.
	 *
	 * @param arquivonf
	 * @param numerorecibo
	 * @param dtrecebimento
	 * @since 21/03/2012
	 * @author Rodrigo Freitas
	 */
	public void updateReciboDatarecebimento(Arquivonf arquivonf, String numerorecibo, Timestamp dtrecebimento) {
		getHibernateTemplate().bulkUpdate("update Arquivonf a set a.numerorecibo = ?, a.dtrecebimento = ? where a.id = ?", 
				new Object[]{numerorecibo, dtrecebimento, arquivonf.getCdarquivonf()});
	}
	
	public void updateDataRecebimento(Arquivonf arquivonf, Timestamp dtrecebimento) {
		getHibernateTemplate().bulkUpdate("update Arquivonf a set a.dtrecebimento = ? where a.id = ?", 
				new Object[]{dtrecebimento, arquivonf.getCdarquivonf()});
	}

	/**
	 * Busca os Arquivonf a partir de uma nota.
	 *
	 * @param nota
	 * @return
	 * @since 04/04/2012
	 * @author Rodrigo Freitas
	 */
	public Arquivonf findGeradosByNota(Nota nota) {
		if(nota == null || nota.getCdNota() == null){
			throw new SinedException("Nota n�o pode ter o id nulo.");
		}
		List<Arquivonf> lista = query()
									.select("arquivonf.cdarquivonf, nota.cdNota")
									.leftOuterJoin("arquivonf.listaArquivonfnota listaArquivonfnota")
									.leftOuterJoin("listaArquivonfnota.nota nota")
									.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.GERADO)
									.where("nota = ?", nota)
									.list();
		
		return lista.size() > 0 ? lista.get(0) : null;
	}

	public Arquivonf loadForParacatu(Arquivonf arquivonf) {
		return query()
					.select("arquivonf.cdarquivonf, configuracaonfe.loginparacatu, configuracaonfe.senhaparacatu, configuracaonfe.senha, " +
							"municipio.cdsiafi, arquivoxml.cdarquivo, configuracaonfe.token, configuracaonfe.codigochaveacesso ")
					.join("arquivonf.arquivoxml arquivoxml")
					.join("arquivonf.configuracaonfe configuracaonfe")
					.leftOuterJoin("configuracaonfe.endereco endereco")
					.leftOuterJoin("endereco.municipio municipio")
					.entity(arquivonf)
					.unique();
	}
	
	public void updateConsultando(Arquivonf arquivonf, Boolean consultando) {
		getHibernateTemplate().bulkUpdate("update Arquivonf a set a.consultando = ? where a.id = ?", 
				new Object[]{consultando, arquivonf.getCdarquivonf()});
	}

	public List<Arquivonf> findArquivonfNovosForConsulta(Empresa empresa) {
		QueryBuilder<Arquivonf> query = querySined();
		
		query
				.select("arquivonf.cdarquivonf, arquivoxml.cdarquivo, configuracaonfe.cdconfiguracaonfe, " +
						"arquivonf.tipocontigencia, arquivoxmlconsultalote.cdarquivo, arquivoxml.cdarquivo ")
				.leftOuterJoin("arquivonf.empresa empresa")
				.leftOuterJoin("arquivonf.arquivoxml arquivoxml")
				.leftOuterJoin("arquivonf.arquivoxmlconsultalote arquivoxmlconsultalote")
				.leftOuterJoin("arquivonf.configuracaonfe configuracaonfe")
				.where("empresa = ?", empresa)
				.where("arquivoxmlconsultalote is not null")
				.where("arquivonf.arquivonfsituacao = ?", Arquivonfsituacao.ENVIADO)
				.where("arquivonf.emitindo = ?", Boolean.TRUE)
				.openParentheses()
					.where("arquivonf.consultandolote = ?", Boolean.FALSE)
					.or()
					.where("arquivonf.consultandolote is null")
				.closeParentheses();
		
		List<Prefixowebservice> prefixosValidos = new ArrayList<Prefixowebservice>();
		prefixosValidos.add(Prefixowebservice.SEFAZBA_HOM);
		prefixosValidos.add(Prefixowebservice.SEFAZBA_PROD);
		prefixosValidos.add(Prefixowebservice.SEFAZGO_HOM);
		prefixosValidos.add(Prefixowebservice.SEFAZGO_PROD);
		prefixosValidos.add(Prefixowebservice.SEFAZMG_HOM);
		prefixosValidos.add(Prefixowebservice.SEFAZMG_PROD);
		prefixosValidos.add(Prefixowebservice.SEFAZAM_HOM);
		prefixosValidos.add(Prefixowebservice.SEFAZAM_PROD);
		prefixosValidos.add(Prefixowebservice.SEFAZPE_HOM);
		prefixosValidos.add(Prefixowebservice.SEFAZPE_PROD);
		prefixosValidos.add(Prefixowebservice.SEFAZPR_HOM);
		prefixosValidos.add(Prefixowebservice.SEFAZPR_PROD);
		prefixosValidos.add(Prefixowebservice.SEFAZRS_HOM);
		prefixosValidos.add(Prefixowebservice.SEFAZRS_PROD);
		prefixosValidos.add(Prefixowebservice.SEFAZSP_HOM);
		prefixosValidos.add(Prefixowebservice.SEFAZSP_PROD);
		prefixosValidos.add(Prefixowebservice.SVAN_HOM);
		prefixosValidos.add(Prefixowebservice.SVAN_PROD);
		prefixosValidos.add(Prefixowebservice.SVRS_HOM);
		prefixosValidos.add(Prefixowebservice.SVRS_PROD);
		prefixosValidos.add(Prefixowebservice.JUAZEIRO_HOM);
		prefixosValidos.add(Prefixowebservice.JUAZEIRO_PROD);
		prefixosValidos.add(Prefixowebservice.ITUMBIARA_HOM);
		prefixosValidos.add(Prefixowebservice.ITUMBIARA_PROD);
		
		query.whereIn("configuracaonfe.prefixowebservice", Prefixowebservice.listAndConcatenate(prefixosValidos));
		
		return query.list();
	}
	
	public Arquivonf findPrefixoWebService(Integer cdarquivonf) {
		if(cdarquivonf == null) return null;
		
		return query()
					.select("arquivonf.cdarquivonf, configuracaonfe.prefixowebservice")
					.leftOuterJoin("arquivonf.configuracaonfe configuracaonfe")
					.where("arquivonf.cdarquivonf = ?", cdarquivonf)
					.unique();
	}

	public Arquivonf carregarArquivoNfNota(Arquivonf arquivoNf) {
		if (arquivoNf == null) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		return query()
				.select("nota.cdNota")
				.leftOuterJoin("arquivonf.listaArquivonfnota arquivonfnota")
				.leftOuterJoin("arquivonfnota.nota nota")
				.where("arquivonf = ?", arquivoNf)
				.unique();
	}

	public Arquivonf loadForEntradaWriter(Arquivonf arquivonf) {
		QueryBuilderSined<Arquivonf> query = querySined().setUseReadOnly(false);
		this.updateEntradaQuery(query);
		return query.entity(arquivonf).unique();
	}
}
