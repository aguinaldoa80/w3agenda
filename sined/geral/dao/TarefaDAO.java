package br.com.linkcom.sined.geral.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.enumeration.Tarefasituacao;
import br.com.linkcom.sined.geral.service.TarefaarquivoService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.TarefaFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.RelComparativoIndice;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.RelComparativoTarefas;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.ComparativoTarefaReportFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.Apontamentotarefaatraso;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

@DefaultOrderBy("tarefa.descricao")
public class TarefaDAO extends GenericDAO<Tarefa>{
	
	private TarefaarquivoService tarefaarquivoService;
	
	public void setTarefaarquivoService(
			TarefaarquivoService tarefaarquivoService) {
		this.tarefaarquivoService = tarefaarquivoService;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Tarefa> query,FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		}
		TarefaFiltro filtro = (TarefaFiltro) _filtro;
		
		query
			.select("tarefa.cdtarefa, tarefa.descricao, tarefa.dtinicio, tarefa.dtfim, tarefa.duracao, projeto.cdprojeto, tarefa.tarefasituacao")
			.join("tarefa.planejamento planejamento")
			.join("planejamento.projeto projeto")
			.leftOuterJoin("tarefa.listaTarefacolaborador listaTarefacolaborador")
			.leftOuterJoin("listaTarefacolaborador.colaborador colaborador")
			.where("projeto = ?",filtro.getProjeto())
			.where("planejamento = ?",filtro.getPlanejamento())
			.where("colaborador = ?", filtro.getColaborador())			
			.whereLikeIgnoreAll("tarefa.descricao", filtro.getDescricao())
			.where("tarefa.dtinicio >= ?", filtro.getDtinicioDe())
			.where("tarefa.dtinicio <= ?", filtro.getDtinicioAte())
		;
		
		if(filtro.getListaSituacao() != null){
			query.whereIn("tarefa.tarefasituacao", Tarefasituacao.listAndConcatenate(filtro.getListaSituacao()));
		}
		
		if(filtro.getProjeto() == null || filtro.getPlanejamento() == null){
			query.where("1=0");
		}
		
	}

	public List<Tarefa> listagemTarefas(TarefaFiltro filtro){
		QueryBuilder<Tarefa> query = query()
		
		.select("tarefa.cdtarefa, projeto.cdprojeto, planejamento.cdplanejamento, tarefa.descricao, " +
				"tarefa.qtde, indice.cdindice, unidademedida.cdunidademedida, area.cdarea, tarefa.dtinicio, tarefa.dtfim, " +
				"tarefa.duracao, tarefa.cdusuarioaltera, tarefa.dtaltera, material.cdmaterial, material.nome, " +
				"listaTarefarecursogeral.qtde, listaTarefarecursogeral.cdtarefarecursogeral, cargo.cdcargo, cargo.nome, " +
				"listaTarefarecursohumano.qtde, listaTarefarecursohumano.cdtarefarecursohumano, tarefapai.cdtarefa, " +
				"listaTarefarecursogeral.outro, listaTarefarecursogeral.tiporecursogeral, unidademedidarg.cdunidademedida, unidademedidarg.nome, " +
				"listaTarefacolaborador.cdtarefacolaborador, colaborador.cdpessoa, listaSolicitacaocompraorigem.cdsolicitacaocompraorigem ")
		.leftOuterJoin("tarefa.indice indice")
		.leftOuterJoin("tarefa.unidademedida unidademedida")
		.leftOuterJoin("tarefa.area area")
		.join("tarefa.planejamento planejamento")
		.join("planejamento.projeto projeto")
		.leftOuterJoin("tarefa.listaTarefarecursogeral listaTarefarecursogeral")
		.leftOuterJoin("listaTarefarecursogeral.material material")
		.leftOuterJoin("listaTarefarecursogeral.unidademedida unidademedidarg")
		.leftOuterJoin("listaTarefarecursogeral.listaSolicitacaocompraorigem listaSolicitacaocompraorigem")
		.leftOuterJoin("tarefa.listaTarefarecursohumano listaTarefarecursohumano")
		.leftOuterJoin("listaTarefarecursohumano.cargo cargo")
		.leftOuterJoin("tarefa.listaTarefacolaborador listaTarefacolaborador")
		.leftOuterJoin("listaTarefacolaborador.colaborador colaborador")
		.leftOuterJoin("tarefa.tarefapai tarefapai")
		.where("projeto = ?",filtro.getProjeto())
		.where("planejamento = ?",filtro.getPlanejamento())
		.where("colaborador = ?", filtro.getColaborador())
		.whereLikeIgnoreAll("tarefa.descricao", filtro.getDescricao())
		.where("tarefa.dtinicio >= ?", filtro.getDtinicioDe())
		.where("tarefa.dtinicio <= ?", filtro.getDtinicioAte());
		
		return query.list();
	}
	
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaTarefarecursogeral");
		save.saveOrUpdateManaged("listaTarefarecursohumano");
		
		Tarefa tarefa = (Tarefa) save.getEntity();
		tarefaarquivoService.saveArquivos(tarefa);
		if(SinedUtil.isListNotEmpty(tarefa.getListaTarefacolaborador())){
			save.saveOrUpdateManaged("listaTarefacolaborador");
		}
		if(SinedUtil.isListNotEmpty(tarefa.getListaTarefaarquivo())){
			save.saveOrUpdateManaged("listaTarefaarquivo");
		}
		if(SinedUtil.isListNotEmpty(tarefa.getListaAtividade())){
			save.saveOrUpdateManaged("listaAtividade");
		}
		if(SinedUtil.isListNotEmpty(tarefa.getListaProducaodiaria())){
			save.saveOrUpdateManaged("listaProducaodiaria");
		}
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Tarefa> query) {
		query
			.select("tarefa.cdtarefa, projeto.cdprojeto, planejamento.cdplanejamento, tarefa.descricao, tarefa.observacao, " +
					"tarefa.qtde, indice.cdindice, unidademedida.cdunidademedida, area.cdarea, tarefa.dtinicio, tarefa.dtfim, " +
					"tarefa.duracao, tarefa.cdusuarioaltera, tarefa.dtaltera, material.cdmaterial, material.nome, " +
					"listaTarefarecursogeral.qtde, listaTarefarecursogeral.cdtarefarecursogeral, cargo.cdcargo, cargo.nome, " +
					"listaTarefarecursohumano.qtde, listaTarefarecursohumano.cdtarefarecursohumano, tarefapai.cdtarefa, " +
					"unidademedidarg.cdunidademedida, unidademedidarg.nome, listaTarefarecursogeral.outro, listaTarefarecursogeral.tiporecursogeral, " +
					"listaTarefacolaborador.cdtarefacolaborador, listaTarefacolaborador.exclusivo, colaborador.cdpessoa, colaborador.nome, tarefa.tarefasituacao, " +
					"listaSolicitacaocompraorigem.cdsolicitacaocompraorigem, listaTarefaarquivo.cdtarefaarquivo, listaTarefaarquivo.data, " +
					"listaTarefaarquivo.observacao, arquivo.cdarquivo, arquivo.nome")
			.leftOuterJoin("tarefa.indice indice")
			.leftOuterJoin("tarefa.unidademedida unidademedida")
			.leftOuterJoin("tarefa.area area")
			.join("tarefa.planejamento planejamento")
			.join("planejamento.projeto projeto")
			.leftOuterJoin("tarefa.listaTarefaarquivo listaTarefaarquivo")
			.leftOuterJoin("listaTarefaarquivo.arquivo arquivo")
			.leftOuterJoin("tarefa.listaTarefarecursogeral listaTarefarecursogeral")
			.leftOuterJoin("listaTarefarecursogeral.material material")
			.leftOuterJoin("listaTarefarecursogeral.unidademedida unidademedidarg")
			.leftOuterJoin("listaTarefarecursogeral.listaSolicitacaocompraorigem listaSolicitacaocompraorigem")
			.leftOuterJoin("tarefa.listaTarefarecursohumano listaTarefarecursohumano")
			.leftOuterJoin("tarefa.listaTarefacolaborador listaTarefacolaborador")
			.leftOuterJoin("listaTarefacolaborador.colaborador colaborador")
			.leftOuterJoin("listaTarefarecursohumano.cargo cargo")
			.leftOuterJoin("tarefa.tarefapai tarefapai")
			.orderBy("material.nome, cargo.nome");
	}

	/**
	 * Busca todas as tarefas menos a tarefa passada por par�metro.
	 * 
	 * OBS: N�o verificado o par�metro pois a tarefa pode vir nula.
	 *
	 * @param form
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefa> findAllMenosTarefa(Tarefa form) {
		return query()
					.select("tarefa.cdtarefa, tarefa.descricao")
					.where("tarefa.cdtarefa <> ?",form != null && form.getCdtarefa() != null ? form.getCdtarefa() : null)
					.list();
	}
	
	/**
	 * Carrega a listagem de composi��o.
	 *
	 * @param ValorreferenciaFiltro
	 * @return
	 * @author Leandro Lima
	 * @author Rodrigo Freitas - Mudan�a na query
	 */
	@SuppressWarnings("unchecked")
	public List<RelComparativoIndice> findForReport(Planejamento planejamento) {
		if(planejamento == null){
			throw new SinedException("Par�metros incorretos.");			
		}
		SinedUtil.markAsReader();
		List<RelComparativoIndice> listaUpdate = getJdbcTemplate().query(
				"SELECT * " +
				"FROM( " +
				"SELECT I.CDINDICE AS CDINDICE, I.DESCRICAO AS INDICE, I.QTDE AS QTDEINDICE, U.SIMBOLO AS UNIDADEINDICE, M.NOME AS ITEM, SUM(IG.QTDE) AS QTDEPREVISTA, UN.SIMBOLO AS UNIDADEPREVISTA, " +
				"SUM(TRG.QTDE) AS QTDETAREFA, (SELECT SUM(A.QTDEHORAS) FROM APONTAMENTO A JOIN TAREFA T2 ON T2.CDTAREFA = A.CDTAREFA WHERE T2.CDINDICE = I.CDINDICE) AS QTDEREALIZADA " +
				"FROM INDICE I " +
				"JOIN UNIDADEMEDIDA U ON U.CDUNIDADEMEDIDA = I.CDUNIDADEMEDIDA " +
				"JOIN INDICERECURSOGERAL IG ON IG.CDINDICE = I.CDINDICE " +
				"JOIN MATERIAL M ON M.CDMATERIAL = IG.CDMATERIAL " +
				"JOIN UNIDADEMEDIDA UN ON UN.CDUNIDADEMEDIDA = M.CDUNIDADEMEDIDA " +
				"JOIN TAREFA T ON T.CDINDICE = I.CDINDICE " +
				"JOIN PLANEJAMENTO P ON P.CDPLANEJAMENTO = T.CDPLANEJAMENTO " +
				"JOIN TAREFARECURSOGERAL TRG ON TRG.CDTAREFA = T.CDTAREFA AND TRG.CDMATERIAL = M.CDMATERIAL " +
				"WHERE 1 = 1 " +
				"AND P.CDPLANEJAMENTO = " + planejamento.getCdplanejamento() + " " +
//				(filtro.getIndice() != null && !filtro.getIndice() && filtro.getListaIndice() != null && filtro.getListaIndice().size() > 0 ? "AND I.CDINDICE IN ("+CollectionsUtil.listAndConcatenate(filtro.getListaIndice(), "indice.cdindice", ",")+") " : "") +
//				(filtro.getProjeto() != null && !filtro.getProjeto() && filtro.getListaProjeto() != null && filtro.getListaProjeto().size() > 0 ? "AND P.CDPROJETO IN ("+CollectionsUtil.listAndConcatenate(filtro.getListaProjeto(), "cdprojeto", ",")+")" : "") +
				"GROUP BY M.CDMATERIAL, M.NOME, I.CDINDICE, I.DESCRICAO, UN.SIMBOLO, I.QTDE, U.SIMBOLO " +
				        
				"UNION ALL " +
				        
				"SELECT I.CDINDICE AS CDINDICE, I.DESCRICAO AS INDICE, I.QTDE AS QTDEINDICE, U.SIMBOLO AS UNIDADEINDICE, C.NOME AS ITEM, SUM(IH.QTDE) AS QTDE, 'HH' AS UNIDADEPREVISTA, " +
				"SUM(TRH.QTDE) AS QTDETAREFA, (SELECT SUM(A.QTDEHORAS) FROM APONTAMENTO A JOIN TAREFA T2 ON T2.CDTAREFA = A.CDTAREFA WHERE T2.CDINDICE = I.CDINDICE) AS QTDEREALIZADA  " +
				"FROM INDICE I " +
				"JOIN UNIDADEMEDIDA U ON U.CDUNIDADEMEDIDA = I.CDUNIDADEMEDIDA  " +
				"JOIN INDICERECURSOHUMANO IH ON IH.CDINDICE = I.CDINDICE  " +
				"JOIN CARGO C ON C.CDCARGO = IH.CDCARGO  " +
				"JOIN TAREFA T ON T.CDINDICE = I.CDINDICE  " +
				"JOIN PLANEJAMENTO P ON P.CDPLANEJAMENTO = T.CDPLANEJAMENTO  " +
				"JOIN TAREFARECURSOHUMANO TRH ON TRH.CDTAREFA = T.CDTAREFA AND TRH.CDCARGO = C.CDCARGO " +
				"WHERE 1 = 1  " +
				"AND P.CDPLANEJAMENTO = " + planejamento.getCdplanejamento() + " " +
//				(filtro.getIndice() != null && !filtro.getIndice() && filtro.getListaIndice() != null && filtro.getListaIndice().size() > 0 ? "AND I.CDINDICE IN ("+CollectionsUtil.listAndConcatenate(filtro.getListaIndice(), "indice.cdindice", ",")+") " : "") +
//				(filtro.getProjeto() != null && !filtro.getProjeto() && filtro.getListaProjeto() != null && filtro.getListaProjeto().size() > 0 ? "AND P.CDPROJETO IN ("+CollectionsUtil.listAndConcatenate(filtro.getListaProjeto(), "cdprojeto", ",")+")" : "") +
				"GROUP BY C.CDCARGO, C.NOME, I.CDINDICE, I.DESCRICAO, I.QTDE, U.SIMBOLO  " +
				")  QUERY " +
				"ORDER BY INDICE, ITEM",
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				RelComparativoIndice c = new RelComparativoIndice();
				c.setCdindice(rs.getInt("CDINDICE"));
				c.setIndice(rs.getString("INDICE"));
				c.setQtdeIndice(rs.getInt("QTDEINDICE"));
				c.setUnidadeIndice(rs.getString("UNIDADEINDICE"));
				c.setItem(rs.getString("ITEM"));
				c.setQtdeItemPrevista(rs.getInt("QTDEPREVISTA"));
				c.setUnidadeItemPrevista(rs.getString("UNIDADEPREVISTA"));
				c.setQtdeTarefa(rs.getInt("QTDETAREFA"));
				
				Double d = rs.getDouble("QTDEREALIZADA");
				c.setQtdeRealizada(d.intValue());
				return c;
			}
		});
		
		return listaUpdate;
	}

//	/**
//	 * M�todo que carrega a lista de tarefas qeu tem como pai a tarefa passada por par�mentro.
//	 *
//	 * @param tarefa
//	 * @param planejamento
//	 * @return
//	 * @author Rodrigo Freitas
//	 */
//	public List<Tarefa> listaWithPai(Tarefa tarefa, Planejamento planejamento) {
//		if(planejamento == null || planejamento.getCdplanejamento() == null){
//			throw new SinedException("Planejamento n�o pode ser nulo.");
//		}
//		QueryBuilder<Tarefa> query = query()
//					.select("tarefa.cdtarefa, tarefa.predecessoras, tarefa.id, projeto.cdprojeto, planejamento.cdplanejamento, tarefa.descricao, " +
//					"tarefa.qtde, indice.cdindice, indice.descricao, unidademedida.cdunidademedida, unidademedida.nome, area.cdarea, area.descricao, tarefa.dtinicio, tarefa.dtfim, " +
//					"tarefa.duracao, tarefa.cdusuarioaltera, tarefa.dtaltera, material.cdmaterial, material.nome, " +
//					"listaTarefarecursogeral.qtde, listaTarefarecursogeral.acumulativo, listaTarefarecursogeral.cdtarefarecursogeral, cargo.cdcargo, cargo.nome, " +
//					"listaTarefarecursohumano.qtde, listaTarefarecursohumano.cdtarefarecursohumano, tarefapai.cdtarefa, " +
//					"listaTarefarecursogeral.outro, listaTarefarecursogeral.tiporecursogeral, unidademedidarg.cdunidademedida, unidademedidarg.nome")
//					.leftOuterJoin("tarefa.indice indice")
//					.leftOuterJoin("tarefa.unidademedida unidademedida")
//					.leftOuterJoin("tarefa.area area")
//					.join("tarefa.planejamento planejamento")
//					.join("planejamento.projeto projeto")
//					.leftOuterJoin("tarefa.listaTarefarecursogeral listaTarefarecursogeral")
//					.leftOuterJoin("listaTarefarecursogeral.material material")
//					.leftOuterJoin("listaTarefarecursogeral.unidademedida unidademedidarg")
//					.leftOuterJoin("tarefa.listaTarefarecursohumano listaTarefarecursohumano")
//					.leftOuterJoin("listaTarefarecursohumano.cargo cargo")
//					.leftOuterJoin("tarefa.tarefapai tarefapai")
//					.where("planejamento = ?", planejamento)
//					
//					.orderBy("tarefa.id, material.nome, cargo.nome");
//		
//		if(tarefa == null){
//			query.where("tarefapai is null");
//		} else {
//			query.where("tarefapai = ?", tarefa);
//		}
//		return query
//					.list();
//	}
	
	public List<Tarefa> findAllTarefas(Planejamento planejamento){
		if(planejamento == null || planejamento.getCdplanejamento() == null){
			throw new SinedException("Planejamento n�o pode ser nulo.");
		}
		
		QueryBuilder<Tarefa> query = query()
					.select("tarefa.cdtarefa, tarefa.predecessoras, tarefa.id, projeto.cdprojeto, planejamento.cdplanejamento, tarefa.descricao, tarefa.observacao, " +
					"tarefa.qtde, indice.cdindice, indice.descricao, unidademedida.cdunidademedida, unidademedida.nome, area.cdarea, area.descricao, tarefa.dtinicio, tarefa.dtfim, " +
					"tarefa.duracao, tarefa.cdusuarioaltera, tarefa.dtaltera, material.cdmaterial, material.nome, " +
					"listaTarefarecursogeral.qtde, listaTarefarecursogeral.acumulativo, listaTarefarecursogeral.cdtarefarecursogeral, cargo.cdcargo, cargo.nome, " +
					"listaTarefarecursohumano.qtde, listaTarefarecursohumano.cdtarefarecursohumano, tarefapai.cdtarefa, " +
					"listaTarefarecursogeral.outro, listaTarefarecursogeral.tiporecursogeral, unidademedidarg.cdunidademedida, unidademedidarg.nome, " +
					"listaSolicitacaocompraorigem.cdsolicitacaocompraorigem ")
					.leftOuterJoin("tarefa.indice indice")
					.leftOuterJoin("tarefa.unidademedida unidademedida")
					.leftOuterJoin("tarefa.area area")
					.join("tarefa.planejamento planejamento")
					.join("planejamento.projeto projeto")
					.leftOuterJoin("tarefa.listaTarefarecursogeral listaTarefarecursogeral")
					.leftOuterJoin("listaTarefarecursogeral.material material")
					.leftOuterJoin("listaTarefarecursogeral.unidademedida unidademedidarg")
					.leftOuterJoin("listaTarefarecursogeral.listaSolicitacaocompraorigem listaSolicitacaocompraorigem")
					.leftOuterJoin("tarefa.listaTarefarecursohumano listaTarefarecursohumano")
					.leftOuterJoin("listaTarefarecursohumano.cargo cargo")
					.leftOuterJoin("tarefa.tarefapai tarefapai")
					.where("planejamento = ?", planejamento)
					.orderBy("tarefa.id, material.nome, cargo.nome");
		
		return query.list();
	}

	/**
	 * Deleta todas as tarefas que n�o est�o no whereIn passado por par�metro.
	 *
	 * @param string
	 * @param planejamento
	 * @author Rodrigo Freitas
	 */
	public void deleteWhereNotIn(String string, Planejamento planejamento) {
		getJdbcTemplate().update("DELETE FROM TAREFA WHERE TAREFA.CDPLANEJAMENTO = "+planejamento.getCdplanejamento()+" AND TAREFA.CDTAREFA NOT IN ("+string+")");	
	}

	
	/**
	 * 
	 * M�todo para buscar a lista de tarefas de um determinado projeto/planejamento
	 *
	 * @param filtro
	 * @return
	 * @author Andr� Brunelli
	 * @author Rodrigo Freitas - Mudan�a na query
	 */
	@SuppressWarnings("unchecked")
	public List<RelComparativoTarefas> findForReportTarefas(ComparativoTarefaReportFiltro filtro) {
		if (filtro == null ) {
			throw new SinedException("Par�metros incorretos.");
		}
		SinedUtil.markAsReader();
		List<RelComparativoTarefas> listaUpdate = getJdbcTemplate().query(
				"SELECT * " + 
				"FROM(   " +
				"SELECT T.CDTAREFA, T.DESCRICAO AS TAREFA, T.QTDE AS QTDETAREFA, U.SIMBOLO AS UNIDADETAREFA, M.CDMATERIAL AS COD, M.NOME AS ITEM, SUM(TG.QTDE) AS QTDEPREVISTA, UN.SIMBOLO AS UNIDADEPREVISTA, " +
				"(SELECT SUM(A.QTDEHORAS) FROM APONTAMENTO A WHERE A.CDTAREFA = T.CDTAREFA AND A.CDMATERIAL = M.CDMATERIAL) AS QTDEREALIZADA " +
				"FROM TAREFA T   " +
				"LEFT JOIN UNIDADEMEDIDA U ON U.CDUNIDADEMEDIDA = T.CDUNIDADEMEDIDA   " +
				"JOIN TAREFARECURSOGERAL TG ON TG.CDTAREFA = T.CDTAREFA   " +
				"JOIN MATERIAL M ON M.CDMATERIAL = TG.CDMATERIAL   " +
				"JOIN UNIDADEMEDIDA UN ON UN.CDUNIDADEMEDIDA = M.CDUNIDADEMEDIDA   " +
				"WHERE T.CDPLANEJAMENTO = " + filtro.getPlanejamento().getCdplanejamento() + " "+
				"AND TG.ACUMULATIVO = TRUE "+
				"GROUP BY M.CDMATERIAL, M.NOME, T.CDTAREFA, T.DESCRICAO, UN.SIMBOLO, T.QTDE, U.SIMBOLO   " +
				
				"UNION ALL   " +
				
				"SELECT T.CDTAREFA, T.DESCRICAO AS TAREFA, T.QTDE AS QTDETAREFA, U.SIMBOLO AS UNIDADETAREFA, M.CDMATERIAL AS COD, M.NOME AS ITEM, SUM(TG.QTDE) AS QTDEPREVISTA, UN.SIMBOLO AS UNIDADEPREVISTA, " +
				"(SELECT AVG(A.QTDEHORAS) FROM APONTAMENTO A WHERE A.CDTAREFA = T.CDTAREFA AND A.CDMATERIAL = M.CDMATERIAL) AS QTDEREALIZADA " +
				"FROM TAREFA T   " +
				"LEFT JOIN UNIDADEMEDIDA U ON U.CDUNIDADEMEDIDA = T.CDUNIDADEMEDIDA   " +
				"JOIN TAREFARECURSOGERAL TG ON TG.CDTAREFA = T.CDTAREFA   " +
				"JOIN MATERIAL M ON M.CDMATERIAL = TG.CDMATERIAL   " +
				"JOIN UNIDADEMEDIDA UN ON UN.CDUNIDADEMEDIDA = M.CDUNIDADEMEDIDA   " +
				"WHERE T.CDPLANEJAMENTO = " + filtro.getPlanejamento().getCdplanejamento() + " "+
				"AND TG.ACUMULATIVO = FALSE "+
				"GROUP BY M.CDMATERIAL, M.NOME, T.CDTAREFA, T.DESCRICAO, UN.SIMBOLO, T.QTDE, U.SIMBOLO   " +
				
				"UNION ALL   " +
				
				"SELECT T.CDTAREFA, T.DESCRICAO AS TAREFA, T.QTDE AS QTDETAREFA, U.SIMBOLO AS UNIDADETAREFA, C.CDCARGO AS COD, C.NOME AS ITEM, SUM(TH.QTDE) AS QTDEPREVISTA, 'H/H' AS UNIDADEPREVISTA, " +
				"(SELECT SUM(A.QTDEHORAS) FROM APONTAMENTO A WHERE A.CDTAREFA = T.CDTAREFA AND A.CDCARGO = C.CDCARGO) AS QTDEREALIZADA " +
				"FROM TAREFA T   " +
				"JOIN UNIDADEMEDIDA U ON U.CDUNIDADEMEDIDA = T.CDUNIDADEMEDIDA   " +
				"JOIN TAREFARECURSOHUMANO TH ON TH.CDTAREFA = T.CDTAREFA   " +
				"JOIN CARGO C ON C.CDCARGO = TH.CDCARGO   " +
				"WHERE T.CDPLANEJAMENTO = " + filtro.getPlanejamento().getCdplanejamento() + " " +
				"GROUP BY C.CDCARGO, C.NOME, T.CDTAREFA, T.DESCRICAO, T.QTDE, U.SIMBOLO   " +
				")   QUERY " +
				"ORDER BY TAREFA, CDTAREFA, ITEM",
		new RowMapper() {
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				RelComparativoTarefas c = new RelComparativoTarefas();
				c.setCdtarefa(rs.getInt("CDTAREFA"));
				c.setTarefa(rs.getString("TAREFA"));
				c.setQtdeTarefa(rs.getInt("QTDETAREFA"));
				c.setUnidadeTarefa(rs.getString("UNIDADETAREFA"));
				c.setItem(rs.getString("ITEM"));
				c.setQtdeItemPrevista(rs.getInt("QTDEPREVISTA"));
				c.setUnidadeItemPrevista(rs.getString("UNIDADEPREVISTA"));
				c.setUnidadeItemRealizada(rs.getString("UNIDADEPREVISTA"));
				Double reali = rs.getDouble("QTDEREALIZADA");
				c.setQtdeItemRealizada(reali != null ? reali.intValue() : 0);
				
				return c;
			}
		});
		
		
		
		return listaUpdate;
	}

	/**
	 * M�todo que busca dados da Tarefa necess�rios para criar o arquivo CSV para importar no project
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Tarefa> carregaTarefas(String whereIn) {
		if(whereIn == null || whereIn.equals(""))
			throw new SinedException("Parametros inv�lidos.");
		
		return query()
			.select("tarefa.cdtarefa, tarefa.descricao, tarefa.qtde, tarefa.dtinicio, tarefa.dtfim, tarefa.duracao, tarefa.predecessoras, " +
					"planejamento.cdplanejamento, planejamento.descricao, projeto.cdprojeto, projeto.nome, tarefapai.cdtarefa")
			.join("tarefa.planejamento planejamento")
			.join("planejamento.projeto projeto")
			.leftOuterJoin("tarefa.tarefapai tarefapai")
			.whereIn("tarefa.cdtarefa", whereIn)
			.orderBy("tarefa.cdtarefa")
			.list();
	}

	/**
	 * M�todo que busca qtde da tarefa
	 * 
	 * @param tarefa
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Double getQtdePrevistaTarefa(Tarefa tarefa) {
		if(tarefa == null || tarefa.getCdtarefa() == null)
			throw new SinedException("Par�metro inv�lido");
		
		return newQueryBuilderSined(Double.class)
			.from(Tarefa.class)
			.setUseTranslator(false)
			.select("tarefa.qtde")
			.where("tarefa = ?", tarefa)
			.unique();
	}

	/**
	 * M�todo que carrega todas as tarefas pertencentes a um projeto e um planejamento
	 * 
	 * @param projeto
	 * @param planejamento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Tarefa> findAllTarefas(Projeto projeto, Planejamento planejamento) {
		if(projeto == null || projeto.getCdprojeto() == null || planejamento == null || planejamento.getCdplanejamento() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("tarefa.cdtarefa, tarefa.id, tarefa.qtde, tarefa.predecessoras, tarefa.descricao, tarefa.dtinicio, tarefa.dtfim, tarefa.duracao, unidademedida.cdunidademedida, " +
					"unidademedida.simbolo, listaProducaodiaria.cdproducaodiaria, listaProducaodiaria.qtde, listaProducaodiaria.dtproducao, listaProducaodiaria.percentual, tarefapai.cdtarefa")
			.leftOuterJoin("tarefa.tarefapai tarefapai")
			.leftOuterJoin("tarefa.unidademedida unidademedida")
			.leftOuterJoin("tarefa.listaProducaodiaria listaProducaodiaria")
			.join("tarefa.planejamento planejamento")
			.join("planejamento.projeto projeto")
			.where("projeto = ?", projeto)
			.where("planejamento = ?", planejamento)
			.orderBy("tarefa.id, listaProducaodiaria.dtproducao")
			.list();
	}
	
	/**
	 * M�todo que carrega a menor data pai de uma tarefa dento de um projeto e planejamento
	 * 
	 * @param projeto
	 * @param planejamento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Date getMenorDataTarefaPaiProjetoPlanejamento(Projeto projeto, Planejamento planejamento) {
		return newQueryBuilderSined(Date.class)
			.select("min(tarefa.dtinicio)")
			.from(Tarefa.class)
			.join("tarefa.planejamento planejamento")
			.join("planejamento.projeto projeto")
			.where("projeto = ?", projeto)
			.where("planejamento = ?", planejamento)
			.where("tarefa.tarefapai is null")
			.setUseTranslator(false)
			.unique();
	}
	
	/**
	 * M�todo que carrega a maior data pai de uma tarefa dento de um projeto e planejamento
	 * 
	 * @param projeto
	 * @param planejamento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Date getMaiorDataTarefaPaiProjetoPlanejamento(Projeto projeto, Planejamento planejamento) {
		return newQueryBuilderSined(Date.class)
			.select("max(tarefa.dtinicio+tarefa.duracao)")
			.from(Tarefa.class)
			.join("tarefa.planejamento planejamento")
			.join("planejamento.projeto projeto")
			.where("projeto = ?", projeto)
			.where("planejamento = ?", planejamento)
			.where("tarefa.tarefapai is null")
			.setUseTranslator(false)
			.unique();
	}

	/**
	 * M�todo que carrega todas as tarefas folhas da �rvore
	 * 
	 * @param projeto
	 * @param planejamento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Tarefa> loadTarefasFilhas(Projeto projeto, Planejamento planejamento) {
		if(projeto == null || projeto.getCdprojeto() == null || planejamento == null || planejamento.getCdplanejamento() == null)
			throw new SinedException("Par�metros inv�lidos.");
		
		return query()
			.select("tarefa.cdtarefa, tarefa.descricao")
			.join("tarefa.tarefapai tarefapai")
			.join("tarefa.planejamento planejamento")
			.join("planejamento.projeto projeto")
			.where("projeto = ?", projeto)
			.where("planejamento = ?", planejamento)
			.orderBy("tarefa.descricao desc")
			.list();
	}

	/**
	* M�todo que carrega a lista de colaborador (tarefacolaborador) da tarefa
	*
	* @param whereIn
	* @return
	* @since Aug 8, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Tarefa> findListatarefacolaborador(String whereIn) {
		if(whereIn == null ||whereIn.equals("")){
			throw new SinedException("Par�metros Inv�lidos");
		}
		
		return query()
					.select("tarefa.cdtarefa, listaTarefacolaborador.cdtarefacolaborador")
					.leftOuterJoin("tarefa.listaTarefacolaborador listaTarefacolaborador")
					.list();
	}

	/**
	* M�todo que carrega a lista de tarefa com a situa��o
	*
	* @param whereIn
	* @return
	* @since Aug 8, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Tarefa> findSituacaoForTarefa(String whereIn) {
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Par�metros inv�lidos.");
		}
		return query()
					.select("tarefa.cdtarefa, tarefa.tarefasituacao, tarefa.dtinicio, tarefa.dtfim")
					.whereIn("tarefa.cdtarefa", whereIn)
					.list();
	}
	
	/**
	* M�todo que atualiza a situa��o da Tarefa
	*
	* @param tarefasituacao
	* @param tarefa
	* @since Aug 8, 2011
	* @author Luiz Fernando F Silva
	*/
	public void updateSituacao(Tarefasituacao tarefasituacao, Tarefa tarefa){
		if(tarefa == null || tarefa.getCdtarefa() == null){
			throw new SinedException("Par�metros inv�lidos");
		}		
		if(tarefasituacao != null)
			getHibernateTemplate().bulkUpdate("update Tarefa t set t.tarefasituacao = ? where t.cdtarefa = ?",
										new Object[]{tarefasituacao, tarefa.getCdtarefa()});
		else
			getHibernateTemplate().bulkUpdate("update Tarefa t set t.tarefasituacao = null where t.cdtarefa = ?",
					new Object[]{tarefa.getCdtarefa()});
		
	}
	
	/**
	* M�todo que carrega a lista de tarefa com a situa��o
	*
	* @return
	* @since Aug 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Tarefa> findSituacaoTodasTarefas(){	
		return query()
				.select("tarefa.cdtarefa, tarefa.tarefasituacao, tarefa.dtinicio, tarefa.dtfim, tarefa.duracao")
				.openParentheses()
				.where("tarefa.tarefasituacao <> ?", Tarefasituacao.CONCLUIDA)
				.or()
				.where("tarefa.tarefasituacao is null")
				.closeParentheses()
				.list();
	}
	
	public List<Tarefa> findSituacaoTarefas(String whereIn){	
		return query()
					.select("tarefa.cdtarefa, tarefa.tarefasituacao, tarefa.dtinicio, tarefa.dtfim, tarefa.duracao")
					.whereIn("tarefa.cdtarefa", whereIn)
					.list();
	}

	/**
	* Retorna a lista de Tarefas que tem o mesmo colaborador alocado na mesma data
	*
	* @param colaborador
	* @return
	* @since Aug 10, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Tarefa> findTarefasConflitantesDataColaborador(Colaborador colaborador, Date dtinicio) {		
		if(colaborador == null || colaborador.getCdpessoa() == null || dtinicio == null){
			throw new SinedException("Par�metros inv�lidos");
		}
		
		return query()
					.select("tarefa.cdtarefa, tarefa.dtinicio, tarefa.dtfim, tarefa.duracao, listatarefacolaborador.cdtarefacolaborador, " +
							"colaborador.cdpessoa, colaborador.nome, projeto.cdprojeto, projeto.nome")
					.leftOuterJoin("tarefa.listaTarefacolaborador listatarefacolaborador")
					.leftOuterJoin("listatarefacolaborador.colaborador colaborador")
					.leftOuterJoin("tarefa.planejamento planejamento")
					.leftOuterJoin("planejamento.projeto projeto")
					.where("colaborador = ?", colaborador)
					.where("tarefa.tarefasituacao <> ?", Tarefasituacao.CONCLUIDA)
					.list();
	}
	
	
	
	/**
	* M�todo que busca as tarefas para verificar apontamentos n�o lan�ados por colaboradores
	* retorna uma lista de Apontamentotarefaatraso (bean auxiliar) 
	*
	* @return
	* @since Aug 12, 2011
	* @author Luiz Fernando F Silva
	*/
	@SuppressWarnings("unchecked")
	public List<Apontamentotarefaatraso> buscaTarefaVerificarAtrasoApontamento(){
		
		String sql = "SELECT p.cdpessoa, p.nome, p.email, t.cdtarefa,  p2.nome as responsavel, p2.email as respEmail,  proj.cdprojeto " +
		 "from Tarefa t " +
	     "join Tarefacolaborador tc on tc.cdtarefa = t.cdtarefa " +
	     "join Colaborador c on c.cdpessoa = tc.cdcolaborador " +
	     "join Pessoa p on p.cdpessoa = c.cdpessoa " +
	     "join Planejamento plan on plan.cdplanejamento = t.cdplanejamento " +
	     "join Projeto proj on proj.cdprojeto = plan.cdprojeto " +	     
	     "join Colaborador c2 on c2.cdpessoa = proj.cdcolaborador " +
	     "join Pessoa p2 on p2.cdpessoa = c2.cdpessoa " +
	     "where current_date >= t.dtinicio	and current_date <= t.dtinicio + (t.duracao + 1)";

		SinedUtil.markAsReader();
		List<Apontamentotarefaatraso> listaVerificacao = getJdbcTemplate().query(sql, 
			new RowMapper(){
				public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Apontamentotarefaatraso(rs.getInt("cdtarefa"), rs.getInt("cdpessoa"), rs.getString("nome"), 
						rs.getString("email"), rs.getString("responsavel"), rs.getString("respEmail"), rs.getInt("cdprojeto"));
			}
		
		});
		
		return listaVerificacao;
	}
	
	/**
	 * 
	 * M�todo que busca a lista de Tarefas para o report de Checklist.
	 *
	 * @name findListTarefaForChecklistReport
	 * @param filtro
	 * @return
	 * @return List<Tarefa>
	 * @author Thiago Augusto
	 * @date 25/06/2012
	 *
	 */
	public List<Tarefa> findListTarefaForChecklistReport(String whereIn){
		QueryBuilder<Tarefa> query = querySined();
			query.select("tarefa.cdtarefa, tarefa.descricao, tarefa.tarefasituacao, " +
					"projeto.cdprojeto, projeto.nome, projeto.dtprojeto, projeto.dtfimprojeto, colaborador.cdpessoa, colaborador.nome, cliente.cdpessoa, cliente.nome ")
			.leftOuterJoin("tarefa.planejamento planejamento")
			.leftOuterJoin("planejamento.projeto projeto")
			.leftOuterJoin("projeto.cliente cliente")
			.leftOuterJoin("tarefa.listaTarefacolaborador listaTarefacolaborador")
			.leftOuterJoin("listaTarefacolaborador.colaborador colaborador")
//			.leftOuterJoin("tarefa.listaTarefarequisicao listaTarefarequisicao")
//			.leftOuterJoin("listaTarefarequisicao.requisicao requisicao")
//			.leftOuterJoin("requisicao.colaboradorresponsavel colaboradorresponsavel")
			.whereIn("tarefa.cdtarefa", whereIn);
				
		return query.list();
	}

	public void updateDatainicio(Date data, Tarefa tarefa) {
		getHibernateTemplate().bulkUpdate("update Tarefa t set t.dtinicio = ? where t.cdtarefa = ?", new Object[]{
				data, tarefa.getCdtarefa()
		});
	}
	
	/**
	* M�todo que faz o update da data fim da tarefa
	*
	* @param data
	* @param tarefa
	* @since 26/06/2014
	* @author Luiz Fernando
	*/
	public void updateDatafim(Date data, Tarefa tarefa) {
		if(data != null && tarefa != null && tarefa.getCdtarefa() != null){
			getHibernateTemplate().bulkUpdate("update Tarefa t set t.dtfim = ? where t.cdtarefa = ?", new Object[]{
					data, tarefa.getCdtarefa()
			});
		}
	}

	/**
	 * Busca a lista de tarefa para gerar romaneio
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/12/2013
	 */
	public List<Tarefa> findForRomaneio(String whereIn) {
		return query()
					.select("tarefa.cdtarefa, tarefa.descricao, projeto.cdprojeto, projeto.nome, " +
							"cliente.cdpessoa, cliente.nome, listaTarefarecursogeral.cdtarefarecursogeral, " +
							"listaTarefarecursogeral.qtde, material.cdmaterial, material.nome, material.identificacao, " +
							"material.patrimonio")
					.leftOuterJoin("tarefa.planejamento planejamento")
					.leftOuterJoin("planejamento.projeto projeto")
					.leftOuterJoin("projeto.cliente cliente")
					.leftOuterJoin("tarefa.listaTarefarecursogeral listaTarefarecursogeral")
					.leftOuterJoin("listaTarefarecursogeral.material material")
					.whereIn("tarefa.cdtarefa", whereIn)
					.list();
	}
	
	/**
	* M�todo que busca as tarefas sem a data fim
	*
	* @return
	* @since 30/06/2014
	* @author Luiz Fernando
	*/
	public List<Tarefa> findForAtualizarDtfimTarefa() {
		return query()
					.select("tarefa.cdtarefa, tarefa.tarefasituacao, tarefa.dtinicio, tarefa.dtfim, tarefa.duracao")
					.where("tarefa.dtfim is null")
					.list();
	}
	
	/**
	* M�todo que carrega as terafas para verificar aloca��o
	*
	* @param whereIn
	* @return
	* @since 20/03/2015
	* @author Luiz Fernando
	*/
	public List<Tarefa> findForVerificarAlocacaoTarefa(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		return query()
					.select("tarefa.cdtarefa, tarefa.dtinicio, tarefa.dtfim, tarefa.duracao")
					.whereIn("tarefa.cdtarefa", whereIn)
					.list();
	}
	
	/**
	* M�todo que carrega as terafas para gerar Ordem de servi�o
	*
	* @param whereIn
	* @return
	* @since 24/03/2015
	* @author Jo�o Vitor
	*/
	public List<Tarefa> findForGerarOrdemServico(String whereIn) {
		if(whereIn == null || "".equals(whereIn))
			throw new SinedException("Par�metro inv�lido");
		
		return query()
					.select("tarefa.cdtarefa, tarefa.descricao, listaTarefacolaborador.cdtarefacolaborador, colaborador.cdpessoa, colaborador.nome")
					.leftOuterJoin("tarefa.listaTarefacolaborador listaTarefacolaborador")
					.leftOuterJoin("listaTarefacolaborador.colaborador colaborador")
					.whereIn("tarefa.cdtarefa", whereIn)
					.orderBy("colaborador.nome")
					.list();
	}
}
