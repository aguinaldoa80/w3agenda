package br.com.linkcom.sined.geral.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import br.com.linkcom.sined.geral.bean.MaterialTabelaPrecoItemHistorico;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecoitem;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class MaterialTabelaPrecoItemHistoricoDAO extends GenericDAO<MaterialTabelaPrecoItemHistorico>{

	public MaterialTabelaPrecoItemHistorico loadUltimoHistoricoComVendaNaoCancelada(Materialtabelaprecoitem bean){
		StringBuilder query = new StringBuilder();
		query.append("select max(a.cdmaterialtabelaprecoitemhistorico) as cdmaterialtabelaprecoitemhistorico");
		query.append("  from (");
		query.append("select max(MaterialTabelaPrecoItemHistorico.cdmaterialtabelaprecoitemhistorico) as cdmaterialtabelaprecoitemhistorico");
		query.append("  from MaterialTabelaPrecoItemHistorico");
		query.append(" join vendamaterial on MaterialTabelaPrecoItemHistorico.cdvendamaterial = MaterialTabelaPrecoItemHistorico.cdvendamaterial");
		query.append(" join venda on venda.cdvenda = vendamaterial.cdvenda");
		query.append(" where MaterialTabelaPrecoItemHistorico.cdMaterialTabelaPrecoItem = ").append(bean.getCdmaterialtabelaprecoitem());
		if(bean.getVendaMaterial() != null){
			query.append("   and MaterialTabelaPrecoItemHistorico.cdvendamaterial <> ").append(bean.getVendaMaterial().getCdvendamaterial());
		}
		query.append("   and venda.cdvendasituacao <> 4");
		query.append("   and venda.cdvendasituacao <> 6");
		query.append(" union");
		query.append(" select max(MaterialTabelaPrecoItemHistorico.cdmaterialtabelaprecoitemhistorico) as cdmaterialtabelaprecoitemhistorico");
		query.append(" from MaterialTabelaPrecoItemHistorico");
		query.append(" join pedidovendamaterial on MaterialTabelaPrecoItemHistorico.cdpedidovendamaterial = MaterialTabelaPrecoItemHistorico.cdpedidovendamaterial");
		query.append(" join pedidovenda on pedidovenda.cdpedidovenda = pedidovendamaterial.cdpedidovenda");
		query.append(" where MaterialTabelaPrecoItemHistorico.cdMaterialTabelaPrecoItem = ").append(bean.getCdmaterialtabelaprecoitem());
		if(bean.getPedidoVendaMaterial() != null){
			query.append("   and MaterialTabelaPrecoItemHistorico.cdpedidovendamaterial <> ").append(bean.getPedidoVendaMaterial().getCdpedidovendamaterial());
		}
		query.append("   and pedidovenda.pedidovendasituacao <> 1");
		query.append("   and pedidovenda.pedidovendasituacao <> 4");
		query.append(" union");
		query.append(" select max(MaterialTabelaPrecoItemHistorico.cdmaterialtabelaprecoitemhistorico) as cdmaterialtabelaprecoitemhistorico");
		query.append(" from MaterialTabelaPrecoItemHistorico");
		query.append(" join vendaorcamentomaterial on MaterialTabelaPrecoItemHistorico.cdvendaorcamentomaterial = MaterialTabelaPrecoItemHistorico.cdvendaorcamentomaterial");
		query.append(" join vendaorcamento on vendaorcamento.cdvendaorcamento = vendaorcamentomaterial.cdvendaorcamento");
		query.append(" where MaterialTabelaPrecoItemHistorico.cdMaterialTabelaPrecoItem = ").append(bean.getCdmaterialtabelaprecoitem());
		if(bean.getVendaOrcamentoMaterial() != null){
			query.append("   and MaterialTabelaPrecoItemHistorico.cdvendaorcamentomaterial <> ").append(bean.getVendaOrcamentoMaterial().getCdvendaorcamentomaterial());
		}
		query.append("   and vendaorcamento.cdvendaorcamentosituacao <> 3) a");

		SinedUtil.markAsReader();
		@SuppressWarnings("unchecked")
		List<MaterialTabelaPrecoItemHistorico> lista = getJdbcTemplate().query(query.toString(), new RowMapper() {
			
			@Override
			public MaterialTabelaPrecoItemHistorico mapRow(ResultSet rs, int rowNum) throws SQLException {
				MaterialTabelaPrecoItemHistorico bean = new MaterialTabelaPrecoItemHistorico();
				bean.setCdMaterialTabelaPrecoItemHistorico(rs.getInt("cdmaterialtabelaprecoitemhistorico"));
				return bean;
			}
		});
		MaterialTabelaPrecoItemHistorico historico = SinedUtil.isListNotEmpty(lista)? this.load(lista.get(0)): null;
		return historico;
	}
}
