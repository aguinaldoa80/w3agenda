package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.sined.geral.bean.DeclaracaoExportacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.DeclaracaoExportacaoNotaService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.DeclaracaoExportacaoFiltro;
import br.com.linkcom.sined.util.SinedException;


public class DeclaracaoExportacaoDAO extends GenericDAO<DeclaracaoExportacao>{

	DeclaracaoExportacaoNotaService declaracaoExportacaoNotaService;
	
	public void setDeclaracaoExportacaoNotaService(DeclaracaoExportacaoNotaService declaracaoExportacaoNotaService) {this.declaracaoExportacaoNotaService = declaracaoExportacaoNotaService;}
	
	
	@Override
	public void updateEntradaQuery(QueryBuilder<DeclaracaoExportacao> query) {
		query.select("declaracaoExportacao.cdDeclaracaoExportacao,declaracaoExportacao.tipoDocumentoDeclaracao,declaracaoExportacao.numeroDeclaracao," +
				"declaracaoExportacao.dtDeclaracao,declaracaoExportacao.dtConhecimentoEmbarque,declaracaoExportacao.tipoNaturezaDeclaracao,declaracaoExportacao.dtRE," +
				"declaracaoExportacao.numeroRE,declaracaoExportacao.dtAverbacaoDE,declaracaoExportacao.numeroEmbarque,declaracaoExportacao.tipoConhecimentoEmbarque," +
				"declaracaoExportacao.cdusuarioaltera,declaracaoExportacao.dtaltera,declaracaoExportacao.listaDeclaracaoExportacaoHistorico");
		super.updateEntradaQuery(query);
	}
	
	@Override
	public DeclaracaoExportacao loadForEntrada(DeclaracaoExportacao bean) {
		
		if(bean.getCdDeclaracaoExportacao() == null){
			bean = null;
		}

		return query().select("declaracaoExportacao.cdDeclaracaoExportacao,declaracaoExportacao.tipoDocumentoDeclaracao,declaracaoExportacao.numeroDeclaracao," +
				"declaracaoExportacao.dtDeclaracao,declaracaoExportacao.tipoNaturezaDeclaracao,declaracaoExportacao.dtRE,declaracaoExportacao.cdusuarioaltera,declaracaoExportacao.dtaltera," +
				"declaracaoExportacao.numeroRE,declaracaoExportacao.dtConhecimentoEmbarque,declaracaoExportacao.dtAverbacaoDE,declaracaoExportacao.numeroEmbarque,declaracaoExportacao.tipoConhecimentoEmbarque," +
				"listaDeclaracaoExportacaoNota.cdDeclaracaoExportacaoNota," +
				"pais.cdpais,pais.cdbacen," +
				"empresa.cdpessoa,empresa.nome," +
				"notaFiscalProduto.cdNota,notaFiscalProduto.serienfe, notaFiscalProduto.numero, " +
				"notaFiscalProduto.valor, notaFiscalProduto.dtEmissao, notaFiscalProduto.numero," +
				"listaDeclaracaoExportacaoHistorico.cddeclaracaoexportacaohistorico,listaDeclaracaoExportacaoHistorico.dtaltera,listaDeclaracaoExportacaoHistorico.observacao,listaDeclaracaoExportacaoHistorico.acao,listaDeclaracaoExportacaoHistorico.cdusuarioaltera")
		.leftOuterJoin("declaracaoExportacao.listaDeclaracaoExportacaoNota listaDeclaracaoExportacaoNota")
		.leftOuterJoin("declaracaoExportacao.empresa empresa")
		.leftOuterJoin("listaDeclaracaoExportacaoNota.notaFiscalProduto notaFiscalProduto")
		.leftOuterJoin("declaracaoExportacao.listaDeclaracaoExportacaoHistorico listaDeclaracaoExportacaoHistorico")
		.leftOuterJoin("declaracaoExportacao.pais pais")
		.where("declaracaoExportacao = ? ", bean).unique();
		 
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<DeclaracaoExportacao> query,FiltroListagem _filtro) {
		if(_filtro == null){
			throw new SinedException("O par�metro _filtro n�o pode ser null.");
		}
		DeclaracaoExportacaoFiltro filtro = (DeclaracaoExportacaoFiltro) _filtro;
		query.select("distinct declaracaoExportacao.cdDeclaracaoExportacao,declaracaoExportacao.tipoDocumentoDeclaracao,declaracaoExportacao.numeroDeclaracao," +
				"declaracaoExportacao.dtDeclaracao,declaracaoExportacao.tipoNaturezaDeclaracao,declaracaoExportacao.tipoDocumentoDeclaracao," +
				"declaracaoExportacao.tipoConhecimentoEmbarque," +
				"empresa.nomefantasia" )
				.leftOuterJoin("declaracaoExportacao.empresa empresa")
				.leftOuterJoin("declaracaoExportacao.listaDeclaracaoExportacaoNota listaDeclaracaoExportacaoNota")
				.leftOuterJoin("listaDeclaracaoExportacaoNota.notaFiscalProduto notaFiscalProduto")
				.leftOuterJoin("notaFiscalProduto.listaItens listaItens")
			.where("declaracaoExportacao.empresa = ?",filtro.getEmpresa())
			.where("declaracaoExportacao.dtDeclaracao >=?",filtro.getDtInicioDeclaracao())
			.where("declaracaoExportacao.dtDeclaracao <=?",filtro.getDtFimDeclaracao())
			.where("declaracaoExportacao.dtRE >=?",filtro.getDtInicioDE())
			.where("declaracaoExportacao.dtRE <=?",filtro.getDtFimDE())
			.where("declaracaoExportacao.numeroDeclaracao=?",filtro.getNumeroDeclaracao())
			.where("declaracaoExportacao.tipoDocumentoDeclaracao=?",filtro.getTipoDocumentoDeclaracao())
			.where("declaracaoExportacao.tipoConhecimentoEmbarque =?", filtro.getTipoDeEmbarque())
			.where("declaracaoExportacao.tipoNaturezaDeclaracao=?",filtro.getTipoNatureza())
			.where("notaFiscalProduto.dtEmissao <=?",filtro.getDtInicioEmissaoNF())
			.where("notaFiscalProduto.dtEmissao >=?",filtro.getDtFimEmissaoNF())
			.where("listaItens.material=? ", filtro.getMaterial())
			.whereLikeIgnoreAll("notaFiscalProduto.numero", filtro.getNumeroNFe())
			.ignoreJoin("listaDeclaracaoExportacaoNota","notaFiscalProduto","listaItens");
	}
	
	
	
	
	public List<DeclaracaoExportacao> findForSped(String whereIn) {
		return query().select("declaracaoExportacao.cdDeclaracaoExportacao,declaracaoExportacao.tipoDocumentoDeclaracao,declaracaoExportacao.numeroDeclaracao," +
				"declaracaoExportacao.dtDeclaracao,declaracaoExportacao.tipoNaturezaDeclaracao,declaracaoExportacao.dtRE," +
				"declaracaoExportacao.numeroRE,declaracaoExportacao.dtAverbacaoDE,declaracaoExportacao.numeroEmbarque,declaracaoExportacao.tipoConhecimentoEmbarque," +
				"pais.cdpais,pais.cdbacen,listaItens.qtde," +
				"notaFiscalProduto.cdNota,notaFiscalProduto.dtEmissao,notaFiscalProduto.numero,notaFiscalProduto.serienfe," +
				"material.cdmaterial, material.identificacao," +
				"unidadeMedida.cdunidademedida, unidadeMedida.nome," +
				"empresa.cdpessoa,empresa.cnpj," +
				"fornecedor.cdpessoa,listaEndereco.cdendereco," +
				"codigocnae.cdcodigocnae,codigocnae.cnae")
				.leftOuterJoin("declaracaoExportacao.listaDeclaracaoExportacaoNota listaDeclaracaoExportacaoNota")
				.leftOuterJoin("declaracaoExportacao.empresa empresa")
				.leftOuterJoin("listaDeclaracaoExportacaoNota.notaFiscalProduto notaFiscalProduto")
				.leftOuterJoin("notaFiscalProduto.codigocnae codigocnae")
				.leftOuterJoin("notaFiscalProduto.transportador fornecedor")
				.leftOuterJoin("notaFiscalProduto.listaItens listaItens")
				.leftOuterJoin("listaItens.unidademedida unidadeMedida")
				.leftOuterJoin("fornecedor.listaEndereco listaEndereco")
				.leftOuterJoin("declaracaoExportacao.pais pais")
				.leftOuterJoin("listaItens.material material")
				.whereIn("declaracaoExportacao.cdDeclaracaoExportacao",whereIn)
				.list();
	}	
	
	public List<DeclaracaoExportacao> findByEmpresa(Empresa empresa) {
		return query().select("declaracaoExportacao.cdDeclaracaoExportacao," +
				"declaracaoExportacao.dtDeclaracao,declaracaoExportacao.numeroDeclaracao")
				.where("declaracaoExportacao.empresa = ? ",empresa)
				.list();
	}
	
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				DeclaracaoExportacao declaracao = (DeclaracaoExportacao)save.getEntity();
				save.saveOrUpdateManaged("listaDeclaracaoExportacaoNota");
				return null;
			}
		});
	}
	
	
	
}
