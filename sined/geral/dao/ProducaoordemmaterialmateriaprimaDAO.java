package br.com.linkcom.sined.geral.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Producaoordemmaterialmateriaprima;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class ProducaoordemmaterialmateriaprimaDAO extends GenericDAO<Producaoordemmaterialmateriaprima>{
	
	/**
	* M�todo que busca a lista de Materia prima alterada ao recalcular a produ��o
	*
	* @param whereIn
	* @return
	* @since 20/02/2015
	* @author Luiz Fernando
	*/
	public List<Producaoordemmaterialmateriaprima> findByProducaoordemmaterial(String whereIn){
		if(StringUtils.isEmpty(whereIn))
			throw new SinedException("Par�metro inv�lido.");
		
		return query()
				.select("producaoordemmaterialmateriaprima.cdproducaoordemmaterialmateriaprima, " +
						"producaoordemmaterialmateriaprima.qtdeprevista, producaoordemmaterialmateriaprima.naocalcularqtdeprevista, " +
						"producaoordemmaterial.cdproducaoordemmaterial, producaoordemmaterialmateriaprima.agitacao, producaoordemmaterialmateriaprima.quantidadepercentual, " +
						"producaoordemmaterialmateriaprima.fracaounidademedida, producaoordemmaterialmateriaprima.qtdereferenciaunidademedida, "+
						"material.cdmaterial, material.identificacao, material.nome, " +
						"materialproducao.cdmaterialproducao, " +
						"loteestoque.cdloteestoque, loteestoque.numero, loteestoque.validade, " +
						"unidademedida.cdunidademedida, unidademedida.nome," +
						"producaoetapanome.cdproducaoetapanome, producaoetapanome.nome ")
				.join("producaoordemmaterialmateriaprima.producaoordemmaterial producaoordemmaterial")
				.join("producaoordemmaterialmateriaprima.material material")
				.leftOuterJoin("producaoordemmaterialmateriaprima.materialproducao materialproducao")
				.leftOuterJoin("producaoordemmaterialmateriaprima.loteestoque loteestoque")
				.leftOuterJoin("producaoordemmaterialmateriaprima.unidademedida unidademedida")
				.leftOuterJoin("producaoordemmaterialmateriaprima.producaoetapanome producaoetapanome")
				.whereIn("producaoordemmaterial.cdproducaoordemmaterial", whereIn)
				.list();
	}
}
