package br.com.linkcom.sined.geral.utils;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.auxiliar.TotalCamposRateadosVendaBean;
import br.com.linkcom.sined.geral.bean.auxiliar.ValoresPassiveisDeRateioInterface;
import br.com.linkcom.sined.util.MoneyUtils;
import br.com.linkcom.sined.util.SinedUtil;

public class VendaUtils {

	public static TotalCamposRateadosVendaBean getValoresRateados(ValoresPassiveisDeRateioInterface valoresPassiveisDeRateio, Money totalvendaItemSemArredondamento, Money descontoItem){
		TotalCamposRateadosVendaBean bean = new TotalCamposRateadosVendaBean();
		
		Double valorvalecompra = MoneyUtils.moneyToDouble(valoresPassiveisDeRateio.getValorusadovalecompra());
		Double desconto = MoneyUtils.moneyToDouble(valoresPassiveisDeRateio.getDesconto());
		Double valortotal = MoneyUtils.moneyToDouble(valoresPassiveisDeRateio.getTotalvendaSemArredondamento());
		Double valorfrete = MoneyUtils.moneyToDouble(valoresPassiveisDeRateio.getValorfrete());

		Double valor_it = totalvendaItemSemArredondamento.doubleValue();
		if (descontoItem != null) {
			valor_it -= descontoItem.doubleValue();
		}

		Double percent = (valor_it * 100d) / valortotal;
		Double desconto_it = SinedUtil.round((percent * desconto) / 100d, 2);
		Double valorvalecompra_it = SinedUtil.round((percent * valorvalecompra) / 100d, 2);
		SinedUtil.round((percent * valorfrete) / 100d, 2);
		Double frete_it = SinedUtil.round((percent * valorfrete) / 100d, 2);
		valor_it += frete_it;

		valor_it -= desconto_it;
		valor_it -= valorvalecompra_it;
		
		bean.setDesconto(desconto_it);
		bean.setFrete(frete_it);
		bean.setValecompra(valorvalecompra_it);
		
		return bean;
	}
}
