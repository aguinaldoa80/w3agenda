
/**
 * WmsWebServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

    package br.com.linkcom.sined.wms;

    /**
     *  WmsWebServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class WmsWebServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public WmsWebServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public WmsWebServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for cadastrarEnderecoEntrega method
            * override this method for handling normal response from cadastrarEnderecoEntrega operation
            */
           public void receiveResultcadastrarEnderecoEntrega(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarEnderecoEntregaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cadastrarEnderecoEntrega operation
           */
            public void receiveErrorcadastrarEnderecoEntrega(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for listarPedidosNaoCarregados method
            * override this method for handling normal response from listarPedidosNaoCarregados operation
            */
           public void receiveResultlistarPedidosNaoCarregados(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.ListarPedidosNaoCarregadosResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from listarPedidosNaoCarregados operation
           */
            public void receiveErrorlistarPedidosNaoCarregados(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for cadastrarFaturamentoProduto method
            * override this method for handling normal response from cadastrarFaturamentoProduto operation
            */
           public void receiveResultcadastrarFaturamentoProduto(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarFaturamentoProdutoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cadastrarFaturamentoProduto operation
           */
            public void receiveErrorcadastrarFaturamentoProduto(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for excluirItemPedido method
            * override this method for handling normal response from excluirItemPedido operation
            */
           public void receiveResultexcluirItemPedido(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.ExcluirItemPedidoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from excluirItemPedido operation
           */
            public void receiveErrorexcluirItemPedido(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for cancelarNotaSaida method
            * override this method for handling normal response from cancelarNotaSaida operation
            */
           public void receiveResultcancelarNotaSaida(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.CancelarNotaSaidaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cancelarNotaSaida operation
           */
            public void receiveErrorcancelarNotaSaida(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for cadastrarTransportador method
            * override this method for handling normal response from cadastrarTransportador operation
            */
           public void receiveResultcadastrarTransportador(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarTransportadorResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cadastrarTransportador operation
           */
            public void receiveErrorcadastrarTransportador(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for consultarNotasEntradaConfirmadas method
            * override this method for handling normal response from consultarNotasEntradaConfirmadas operation
            */
           public void receiveResultconsultarNotasEntradaConfirmadas(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.ConsultarNotasEntradaConfirmadasResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from consultarNotasEntradaConfirmadas operation
           */
            public void receiveErrorconsultarNotasEntradaConfirmadas(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for cadastrarPedidoVenda method
            * override this method for handling normal response from cadastrarPedidoVenda operation
            */
           public void receiveResultcadastrarPedidoVenda(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarPedidoVendaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cadastrarPedidoVenda operation
           */
            public void receiveErrorcadastrarPedidoVenda(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for cadastrarPedidoVendaProduto method
            * override this method for handling normal response from cadastrarPedidoVendaProduto operation
            */
           public void receiveResultcadastrarPedidoVendaProduto(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarPedidoVendaProdutoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cadastrarPedidoVendaProduto operation
           */
            public void receiveErrorcadastrarPedidoVendaProduto(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for cadastrarPedidoVendaCompleto method
            * override this method for handling normal response from cadastrarPedidoVendaCompleto operation
            */
           public void receiveResultcadastrarPedidoVendaCompleto(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarPedidoVendaCompletoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cadastrarPedidoVendaCompleto operation
           */
            public void receiveErrorcadastrarPedidoVendaCompleto(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for consultarCarregamentos method
            * override this method for handling normal response from consultarCarregamentos operation
            */
           public void receiveResultconsultarCarregamentos(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.ConsultarCarregamentosResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from consultarCarregamentos operation
           */
            public void receiveErrorconsultarCarregamentos(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for cadastrarPedidoCompra method
            * override this method for handling normal response from cadastrarPedidoCompra operation
            */
           public void receiveResultcadastrarPedidoCompra(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarPedidoCompraResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cadastrarPedidoCompra operation
           */
            public void receiveErrorcadastrarPedidoCompra(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for cadastrarProduto method
            * override this method for handling normal response from cadastrarProduto operation
            */
           public void receiveResultcadastrarProduto(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarProdutoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cadastrarProduto operation
           */
            public void receiveErrorcadastrarProduto(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for cadastrarProdutoembalagem method
            * override this method for handling normal response from cadastrarProdutoembalagem operation
            */
           public void receiveResultcadastrarProdutoembalagem(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarProdutoembalagemResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cadastrarProdutoembalagem operation
           */
            public void receiveErrorcadastrarProdutoembalagem(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for marcarItemPedido method
            * override this method for handling normal response from marcarItemPedido operation
            */
           public void receiveResultmarcarItemPedido(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.MarcarItemPedidoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from marcarItemPedido operation
           */
            public void receiveErrormarcarItemPedido(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for consultarCarregamentoProdutos method
            * override this method for handling normal response from consultarCarregamentoProdutos operation
            */
           public void receiveResultconsultarCarregamentoProdutos(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.ConsultarCarregamentoProdutosResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from consultarCarregamentoProdutos operation
           */
            public void receiveErrorconsultarCarregamentoProdutos(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for isAtivo method
            * override this method for handling normal response from isAtivo operation
            */
           public void receiveResultisAtivo(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.IsAtivoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from isAtivo operation
           */
            public void receiveErrorisAtivo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for cadastrarNotaEntrada method
            * override this method for handling normal response from cadastrarNotaEntrada operation
            */
           public void receiveResultcadastrarNotaEntrada(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarNotaEntradaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cadastrarNotaEntrada operation
           */
            public void receiveErrorcadastrarNotaEntrada(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for cadastrarClasseProduto method
            * override this method for handling normal response from cadastrarClasseProduto operation
            */
           public void receiveResultcadastrarClasseProduto(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarClasseProdutoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cadastrarClasseProduto operation
           */
            public void receiveErrorcadastrarClasseProduto(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for cadastrarPedidoVendaProdutoCompleto method
            * override this method for handling normal response from cadastrarPedidoVendaProdutoCompleto operation
            */
           public void receiveResultcadastrarPedidoVendaProdutoCompleto(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarPedidoVendaProdutoCompletoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cadastrarPedidoVendaProdutoCompleto operation
           */
            public void receiveErrorcadastrarPedidoVendaProdutoCompleto(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for cadastrarFornecedor method
            * override this method for handling normal response from cadastrarFornecedor operation
            */
           public void receiveResultcadastrarFornecedor(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarFornecedorResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cadastrarFornecedor operation
           */
            public void receiveErrorcadastrarFornecedor(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for desmarcarItemPedido method
            * override this method for handling normal response from desmarcarItemPedido operation
            */
           public void receiveResultdesmarcarItemPedido(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.DesmarcarItemPedidoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from desmarcarItemPedido operation
           */
            public void receiveErrordesmarcarItemPedido(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for confirmarExclusaoItemPedido method
            * override this method for handling normal response from confirmarExclusaoItemPedido operation
            */
           public void receiveResultconfirmarExclusaoItemPedido(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.ConfirmarExclusaoItemPedidoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from confirmarExclusaoItemPedido operation
           */
            public void receiveErrorconfirmarExclusaoItemPedido(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for cadastrarTransportadora method
            * override this method for handling normal response from cadastrarTransportadora operation
            */
           public void receiveResultcadastrarTransportadora(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarTransportadoraResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cadastrarTransportadora operation
           */
            public void receiveErrorcadastrarTransportadora(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for cadastrarCliente method
            * override this method for handling normal response from cadastrarCliente operation
            */
           public void receiveResultcadastrarCliente(
                    br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarClienteResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cadastrarCliente operation
           */
            public void receiveErrorcadastrarCliente(java.lang.Exception e) {
            }
                


    }
    