package br.com.linkcom.sined.controller.simple;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.controller.simple.bean.AvisoEquipeBean;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Portlet;
import br.com.linkcom.sined.geral.service.AvisoService;
import br.com.linkcom.sined.geral.service.DashboardespecificoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.W3ControleUtil;
import br.com.linkcom.sined.util.menu.DashboardBean;
import br.com.linkcom.util.rest.CriptografiaUtil;

@Bean
@Controller(path = {"/adm/index","/financeiro/index","/veiculo/index","/rh/index","/sistema/index","/projeto/index","/juridico/index","/crm/index",
					"/faturamento/index","/servicointerno/index","/suprimento/index","/briefcase/index","/help/index","/fiscal/index","/patrimonio/index"})
public class Index extends MultiActionController {
	
	private AvisoService avisoService;
	private DashboardespecificoService dashboardespecificoService;
	
	public void setDashboardespecificoService(
			DashboardespecificoService dashboardespecificoService) {
		this.dashboardespecificoService = dashboardespecificoService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}


	@DefaultAction
	public ModelAndView index(WebRequestContext request){
		
//		NAO APAGAR!!
//		try{
//			ClassManager classManager = WebClassRegister.getClassManager(getServletContext(), "br.com.linkcom.neo");
//			Class<?>[] classesWithAnnotation = classManager.getClassesWithAnnotation(Controller.class);
//			
//			ReflectionCache reflectionCache = ReflectionCacheFactory.getReflectionCache();
//			
//			for (int i = 0; i < classesWithAnnotation.length; i++) {
//				Controller annotation = reflectionCache.getAnnotation(classesWithAnnotation[i], Controller.class);
//				if(annotation != null && annotation.authorizationModule().equals(HasAccessAuthorizationModule.class)){
//					System.out.println(classesWithAnnotation[i].getName());
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
		String nomeBanco = request.getParameter("nomeBanco");
		String uidDashboard = request.getParameter("uidDashboard");
		
		if (StringUtils.isNotEmpty(nomeBanco) && StringUtils.isNotEmpty(uidDashboard)) {
			request.setAttribute("nomeBanco", nomeBanco);
			request.setAttribute("uidDashboard", uidDashboard);
		}
		
		Boolean exibirDashboardsAntigos = ParametrogeralService.getInstance().getBoolean("EXIBIR_DASHBOARDS_ANTIGOS");
		
		if (exibirDashboardsAntigos) {
			String isDashboardAntigoParam = request.getParameter("isDashboardAntigo");
			boolean dashboardAntigo = StringUtils.isNotBlank(isDashboardAntigoParam) && StringUtils.equalsIgnoreCase(isDashboardAntigoParam, "true");
			Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
			
			if(!dashboardAntigo){
				try {
					String cdDashboard = request.getParameter("cdDashboard");
					String geralDashboard = request.getParameter("geralDashboard");
					
					if(StringUtils.isBlank(cdDashboard)){
						try {
							String dashboardDefault = InitialContext.doLookup("W3ERP_DASHBOARD_DEFAULT");				
							if(StringUtils.isNotBlank(dashboardDefault)){
								geralDashboard = dashboardDefault.startsWith("g") ? "true" : "false";
								cdDashboard = dashboardDefault.replaceAll("g", "");
							}
						} catch (Exception e) {
	//						e.printStackTrace();
						}
					}
					
					if(StringUtils.isNotBlank(cdDashboard)) {
						Boolean geralDashboardAtual = StringUtils.isNotBlank(geralDashboard) && StringUtils.equalsIgnoreCase(geralDashboard, "true");
						
						DashboardBean dashboardBean = null;
						List<DashboardBean> listaDashboard = dashboardespecificoService.findAllDashboard(true);
						for (DashboardBean dashboardBeanIt : listaDashboard) {
							Integer cddashboard = dashboardBeanIt.getCddashboard();
							Boolean geral = dashboardBeanIt.getGeral();
							
							if(geralDashboardAtual.equals(geral) && cdDashboard.equals(cddashboard.toString())){
								dashboardBean = dashboardBeanIt;
								break;
							}
						}
						
						if(dashboardBean != null){
							if(geralDashboardAtual){
								String servidorBanco = InitialContext.doLookup("W3ERP_DASHBOARD_SERVER_BANCO");
								String usuarioBanco = InitialContext.doLookup("W3ERP_DASHBOARD_USUARIO_BANCO");
								String senhaBanco = InitialContext.doLookup("W3ERP_DASHBOARD_SENHA_BANCO");
								nomeBanco = ParametrogeralService.getInstance().getNomeBancoDeDados();
								
								request.setAttribute("servidorBanco", URLEncoder.encode(servidorBanco, "UTF-8"));
								request.setAttribute("usuarioBanco", URLEncoder.encode(CriptografiaUtil.criptografar(usuarioBanco), "UTF-8"));
								request.setAttribute("senhaBanco", URLEncoder.encode(CriptografiaUtil.criptografar(senhaBanco), "UTF-8"));
								request.setAttribute("nomeBanco", URLEncoder.encode(nomeBanco, "UTF-8"));
								request.setAttribute("idUsuario", URLEncoder.encode(nomeBanco + "_" + usuarioLogado.getCdpessoa(), "UTF-8"));
							}
							
							String urlDashboard = dashboardBean.getUrl();
							String idDashboard = dashboardBean.getId().toString();
							String usuarioSistema = dashboardBean.getUsuariosistema();
							String senhaSistema = dashboardBean.getSenhasistema();
							
							request.setAttribute("urlDashboard", urlDashboard);
							request.setAttribute("idDashboard", idDashboard);
							request.setAttribute("usuarioSistema", URLEncoder.encode(CriptografiaUtil.criptografar(usuarioSistema), "UTF-8"));
							request.setAttribute("senhaSistema", URLEncoder.encode(CriptografiaUtil.criptografar(senhaSistema), "UTF-8"));
							request.setAttribute("showDashboard", Boolean.TRUE);
						} else {
							dashboardAntigo = true;
						}
					} else {
						dashboardAntigo = true;
					}
				} catch (Exception e) {
					e.printStackTrace();
					dashboardAntigo = true;
				}
			}
			
			request.setAttribute("isDashboardAntigo", dashboardAntigo);
			if(dashboardAntigo){
				//endere�o rss para carregamento do painel "Not�cias"
				String urlRSS = ParametrogeralService.getInstance().buscaValorPorNome(Parametrogeral.BLOGPAINEL);
				request.setAttribute("urlRSS", urlRSS);
				
				request.setAttribute("listaPortlets", Portlet.values());
			}
		}
		
		SinedUtil.carregaAtalhosUsuario(request);
		return new ModelAndView("index"); 
	}
	
	public ModelAndView mostraPendenciaFinanceira(WebRequestContext request){
		return new ModelAndView("direct:/popup/popupPendenciaFinanceira");
	}
	
	public ModelAndView ajaxAvisoUsuario(WebRequestContext request){
		return avisoService.ajaxAvisoUsuario(request);
	}
	
	public void ajaxAvisoUsuarioMarcarNotificado(WebRequestContext request){
		avisoService.ajaxAvisoUsuarioMarcarNotificado(request);
	}
	
	public void ajaxAvisoUsuarioMarcarLido(WebRequestContext request){
		avisoService.ajaxAvisoUsuarioMarcarLido(request);
	}
	
	public void ajaxAvisoUsuarioMarcarLidoTodos(WebRequestContext request){
		avisoService.ajaxAvisoUsuarioMarcarLidoTodos(request);
	}
}
