package br.com.linkcom.sined.controller.simple;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.NovidadeVersao;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_novidadeVersao;
import br.com.linkcom.sined.util.ControleAcessoUtil;
import br.com.linkcom.sined.util.bean.NovidadeVersaoRetorno;

@Controller(path="/adm/NovidadeVersao")
public class NovidadeVersaoProcess extends MultiActionController {

	@DefaultAction
	public ModelAndView index(WebRequestContext request, NovidadeVersao filtro) {	
		NovidadeVersaoRetorno novidadeVersaoRetorno = ControleAcessoUtil.util.doNovidadeVersao(request.getServletRequest(), false, false, filtro);
		return new ModelAndView("novidadeVersao", "novidadeVersaoRetorno", novidadeVersaoRetorno);
	}
	
	@Action("popUpComboVersao")
	public ModelAndView popUpComboVersao(WebRequestContext request){			
		List<NovidadeVersao> listaNovidadeVersao = ControleAcessoUtil.util.doVersoesComNovidade(request.getServletRequest());
		Aux_novidadeVersao aux_novidadeVersao = new Aux_novidadeVersao();
		request.setAttribute("listaNovidadeVersao", listaNovidadeVersao);		
		
		return new ModelAndView("direct:/popup/popupVersoesNovidade", "aux_novidadeVersao", aux_novidadeVersao);
	}
	
	@Action("selecionarVersao")
	public ModelAndView selecionarVersao(WebRequestContext request, Aux_novidadeVersao aux_novidadeVersao){	
		NovidadeVersao novidadeVersao = aux_novidadeVersao.getNovidadeVersao();
		return continueOnAction("index", novidadeVersao);
	}
}