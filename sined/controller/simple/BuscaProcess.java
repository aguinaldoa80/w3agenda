package br.com.linkcom.sined.controller.simple;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.controller.simple.bean.BuscaBean;
import br.com.linkcom.sined.geral.service.AgendamentoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ContacrmService;
import br.com.linkcom.sined.geral.service.ContapagarService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.LeadService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.geral.service.RomaneioService;
import br.com.linkcom.sined.geral.service.UsuarioService;

@Bean
@Controller(path = {"/adm/Busca","/financeiro/Busca","/veiculo/Busca",
					"/rh/Busca","/sistema/Busca","/projeto/Busca","/juridico/Busca","/crm/Busca",
					"/faturamento/Busca","/servicointerno/Busca","/suprimento/Busca","/briefcase/Busca",
					"/fiscal/Busca", "/producao/Busca"})
public class BuscaProcess extends MultiActionController {

	private ClienteService clienteService;
	private ColaboradorService colaboradorService;
	private ContapagarService contapagarService;
	private ContareceberService contareceberService;
	private FornecedorService fornecedorService;
	private UsuarioService usuarioService;
	private MovimentacaoService movimentacaoService;
	private ContratoService contratoService;
	private AgendamentoService agendamentoService;
	private MaterialService materialService;
	private LeadService leadService;
	private ContacrmService contacrmService;
	private OportunidadeService oportunidadeService;
	private RomaneioService romaneioService;
	
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setContapagarService(ContapagarService contapagarService) {
		this.contapagarService = contapagarService;
	}
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setContareceberService(ContareceberService contareceberService) {
		this.contareceberService = contareceberService;
	}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setAgendamentoService(AgendamentoService agendamentoService) {
		this.agendamentoService = agendamentoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setLeadService(LeadService leadService) {
		this.leadService = leadService;
	}
	public void setContacrmService(ContacrmService contacrmService) {
		this.contacrmService = contacrmService;
	}
	public void setOportunidadeService(OportunidadeService oportunidadeService) {
		this.oportunidadeService = oportunidadeService;
	}
	public void setRomaneioService(RomaneioService romaneioService) {
		this.romaneioService = romaneioService;
	}
	/**
	 * M�todo padr�o chamado quando realizar busca. Par�metro deve ser diferente de vazio e 
	 * possuir pelo menos 3 caracteres
	 * 
	 * @param request
	 * @return
	 * @author Tom�s Rabelo
	 */
	@DefaultAction
	public ModelAndView busca(WebRequestContext request){
		String busca = request.getParameter("busca");
		
		if(busca != null && !busca.equals("") && busca.length() > 2){
			request.setAttribute("paramBusca", busca);
			return new ModelAndView("busca", "buscaBean", carregaListasBusca(request, busca)); 
		}
		return new ModelAndView("index"); 
	}
	
	/**
	 * M�todo que valida se o usu�rio tem permiss�o de ver a tela e realiza a busca na tabela
	 * 
	 * @param busca
	 * @return
	 * @author Tom�s Rabelo
	 */
	private BuscaBean carregaListasBusca(WebRequestContext request, String busca) {
		BuscaBean buscaBean = new BuscaBean();
		
		// Cliente
		if (Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/crm/crud/Cliente", "consultar", Neo.getUser())) {
			request.setAttribute("moduloCliente", "crm");
			buscaBean.setListaClientes(clienteService.findClientesForBuscaGeral(busca));
		}
		else if (Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/financeiro/crud/Cliente", "consultar", Neo.getUser())) {
			request.setAttribute("moduloCliente", "financeiro");
			buscaBean.setListaClientes(clienteService.findClientesForBuscaGeral(busca));
		}
		else if (Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/faturamento/crud/Cliente", "consultar", Neo.getUser())) {
			request.setAttribute("moduloCliente", "faturamento");
			buscaBean.setListaClientes(clienteService.findClientesForBuscaGeral(busca));
		}
		else if (Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/juridico/crud/Cliente", "consultar", Neo.getUser())) {
			request.setAttribute("moduloCliente", "juridico");
			buscaBean.setListaClientes(clienteService.findClientesForBuscaGeral(busca));
		}
		
		// Colaborador
		if(Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/rh/crud/Colaborador", "consultar", Neo.getUser())){
			buscaBean.setListaColaboradores(colaboradorService.findColaboradoresForBuscaGeral(busca));
		}
		
		// Conta a pagar
		if(Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/financeiro/crud/Contapagar", "consultar", Neo.getUser())){
			buscaBean.setListaContasPagar(contapagarService.findContasPagarForBuscaGeral(busca));
		}
		
		// Conta a receber
		if(Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/financeiro/crud/Contareceber", "consultar", Neo.getUser())){
			buscaBean.setListaContasReceber(contareceberService.findContasReceberForBuscaGeral(busca));
		}
		
		// Fornecedor
		if (Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/suprimento/crud/Fornecedor", "consultar", Neo.getUser())) {
			request.setAttribute("moduloFornecedor", "suprimento");
			buscaBean.setListaFornecedores(fornecedorService.findFornecedoresForBuscaGeral(busca));
		}
		else if (Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/financeiro/crud/Fornecedor", "consultar", Neo.getUser())) {
			request.setAttribute("moduloFornecedor", "financeiro");
			buscaBean.setListaFornecedores(fornecedorService.findFornecedoresForBuscaGeral(busca));
		}
		
		// Movimenta��o Financeira
		if(Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/financeiro/crud/Movimentacao", "consultar", Neo.getUser())){
			buscaBean.setListaMovimentacoes(movimentacaoService.findMovimentacoesForBuscaGeral(busca));
		}
		
		// Usu�rio
		if(Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/sistema/crud/Usuario", "consultar", Neo.getUser())){
			buscaBean.setListaUsuario(usuarioService.findUsuariosForBuscaGeral(busca));
		}
		
		// Contrato
		if(Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/faturamento/crud/Contrato", "consultar", Neo.getUser())){
			buscaBean.setListaContratos(contratoService.findContratosForBuscaGeral(busca));
		}
		
		// Agendamento
		if(Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/financeiro/crud/Agendamento", "consultar", Neo.getUser())){
			buscaBean.setListaAgendamentos(agendamentoService.findAgendamentosForBuscaGeral(busca));
		}
		
		// Material
		if(Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/suprimento/crud/Material", "consultar", Neo.getUser())){
			buscaBean.setListaMateriais(materialService.findMaterialForBuscaGeral(busca));
		}
		
		// Lead
		if(Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/crm/crud/Lead", "consultar", Neo.getUser())){
			buscaBean.setListaLeads(leadService.findLeadForBuscaGeral(busca));
		}
		
		// Conta CRM
		if(Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/crm/crud/Contacrm", "consultar", Neo.getUser())){
			buscaBean.setListaContascrm(contacrmService.findContacrmForBuscaGeral(busca));
		}
		
		// Oporturnidade
		if(Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/crm/crud/Oportunidade", "consultar", Neo.getUser())){
			buscaBean.setListaOportunidades(oportunidadeService.findOportunidadeForBuscaGeral(busca));
		}
		
		// Romaneio
		if(Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/suprimento/crud/Romaneio", "consultar", Neo.getUser())){
			buscaBean.setListaRomaneios(romaneioService.findRomaneioForBuscaGeral(busca));
		}
		
		return buscaBean;
	}
}
