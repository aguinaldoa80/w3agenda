package br.com.linkcom.sined.controller.simple.bean;

import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.bean.Usuario;


public class BuscaBean {
	
	protected List<Cliente> listaClientes = new ListSet<Cliente>(Cliente.class);
	protected List<Colaborador> listaColaboradores = new ListSet<Colaborador>(Colaborador.class);
//	protected List<Contato> listaContatos = new ListSet<Contato>(Contato.class);
	protected List<Documento> listaContasPagar = new ListSet<Documento>(Documento.class);
	protected List<Documento> listaContasReceber = new ListSet<Documento>(Documento.class);
	protected List<Fornecedor> listaFornecedores = new ListSet<Fornecedor>(Fornecedor.class);
	protected List<Usuario> listaUsuario = new ListSet<Usuario>(Usuario.class);
	protected List<Movimentacao> listaMovimentacoes = new ListSet<Movimentacao>(Movimentacao.class);
	protected List<Contrato> listaContratos = new ListSet<Contrato>(Contrato.class);
	protected List<Agendamento> listaAgendamentos = new ListSet<Agendamento>(Agendamento.class);
	protected List<Material> listaMateriais = new ListSet<Material>(Material.class);
	protected List<Lead> listaLeads = new ListSet<Lead>(Lead.class);
	protected List<Contacrm> listaContascrm = new ListSet<Contacrm>(Contacrm.class);
	protected List<Oportunidade> listaOportunidades = new ListSet<Oportunidade>(Oportunidade.class);
	protected List<Romaneio> listaRomaneios = new ListSet<Romaneio>(Romaneio.class);
	
	public List<Cliente> getListaClientes() {
		return listaClientes;
	}
	public List<Colaborador> getListaColaboradores() {
		return listaColaboradores;
	}
	
//	public List<Contato> getListaContatos() {
//		return listaContatos;
//	}
	public List<Documento> getListaContasPagar() {
		return listaContasPagar;
	}
	public List<Documento> getListaContasReceber() {
		return listaContasReceber;
	}
	public List<Fornecedor> getListaFornecedores() {
		return listaFornecedores;
	}
	public List<Usuario> getListaUsuario() {
		return listaUsuario;
	}
	public List<Movimentacao> getListaMovimentacoes() {
		return listaMovimentacoes;
	}
	public List<Contrato> getListaContratos() {
		return listaContratos;
	}
	public List<Agendamento> getListaAgendamentos() {
		return listaAgendamentos;
	}
	public List<Material> getListaMateriais() {
		return listaMateriais;
	}
	public List<Lead> getListaLeads() {
		return listaLeads;
	}
	public List<Contacrm> getListaContascrm() {
		return listaContascrm;
	}
	public List<Oportunidade> getListaOportunidades() {
		return listaOportunidades;
	}
	public List<Romaneio> getListaRomaneios() {
		return listaRomaneios;
	}
	
	
	public void setListaMovimentacoes(List<Movimentacao> listaMovimentacoes) {
		this.listaMovimentacoes = listaMovimentacoes;
	}
//	public void setListaContatos(List<Contato> listaContatos) {
//		this.listaContatos = listaContatos;
//	}
	public void setListaContasPagar(List<Documento> listaContasPagar) {
		this.listaContasPagar = listaContasPagar;
	}
	public void setListaContasReceber(List<Documento> listaContasReceber) {
		this.listaContasReceber = listaContasReceber;
	}
	public void setListaFornecedores(List<Fornecedor> listaFornecedores) {
		this.listaFornecedores = listaFornecedores;
	}
	public void setListaUsuario(List<Usuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}
	public void setListaClientes(List<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}
	public void setListaColaboradores(List<Colaborador> listaColaboradores) {
		this.listaColaboradores = listaColaboradores;
	}
	public void setListaContratos(List<Contrato> listaContratos) {
		this.listaContratos = listaContratos;
	}
	public void setListaAgendamentos(List<Agendamento> listaAgendamentos) {
		this.listaAgendamentos = listaAgendamentos;
	}
	public void setListaMateriais(List<Material> listaMateriais) {
		this.listaMateriais = listaMateriais;
	}
	public void setListaLeads(List<Lead> listaLeads) {
		this.listaLeads = listaLeads;
	}
	public void setListaContascrm(List<Contacrm> listaContascrm) {
		this.listaContascrm = listaContascrm;
	}
	public void setListaOportunidades(List<Oportunidade> listaOportunidades) {
		this.listaOportunidades = listaOportunidades;
	}
	public void setListaRomaneios(List<Romaneio> listaRomaneios) {
		this.listaRomaneios = listaRomaneios;
	}
}
