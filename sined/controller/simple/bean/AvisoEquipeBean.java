package br.com.linkcom.sined.controller.simple.bean;

public class AvisoEquipeBean {
	
	private String titulo;
	private String conteudo;
	private String dtInicio;
	
	public String getTitulo() {
		return titulo;
	}
	public String getConteudo() {
		return conteudo;
	}
	public String getDtInicio() {
		return dtInicio;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}
	public void setDtInicio(String dtInicio) {
		this.dtInicio = dtInicio;
	}
	
}
