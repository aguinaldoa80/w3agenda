/*
 * Created on 29/08/2006
 */
package br.com.linkcom.sined.controller;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.service.ArquivoService;

public class ArquivoFotoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArquivoService arquivoService = (ArquivoService) Neo.getApplicationContext().getBean("arquivoService");
		
		Long cdfile;
		try {
			String requestURI = request.getRequestURI();
			
			if(requestURI.endsWith(".jpg")){
				requestURI = requestURI.substring(0, requestURI.length()-4);
			}
			
			cdfile = extractCdfile(requestURI);
		}
		catch (Exception e) {
        	cdfile = Long.valueOf(0);// = new Long(0);
		}

		Arquivo arquivo = new Arquivo();
		arquivo.setCdfile(cdfile);
		arquivo = (Arquivo) arquivoService.loadWithContents(arquivo);			
		
		response.setContentType(arquivo.getTipoconteudo());
		response.setHeader("Content-Disposition", "attachment; filename=\"" + arquivo.getNome() + "\";");
		
//		response.setContentLength(arquivo.getSize().intValue());
		
		// Se o conte�do � uma imagem suportada para redimensionamento.
		if (arquivo.getTipoconteudo().matches(".*png.*") || arquivo.getTipoconteudo().matches(".*jpe?g.*") || arquivo.getTipoconteudo().matches(".*gif.*") || arquivo.getTipoconteudo().matches(".*bmp.*")) {
			// L� o arquivo e carrega a imagem.
			ByteArrayInputStream entrada = new ByteArrayInputStream(arquivo.getContent());
			BufferedImage imagem = ImageIO.read(entrada);
			entrada.close();

			// Tamanho a ser gerado.				
			int largura;
			int altura;				
			
			try {
				largura = 108;
				altura  = 160;				
				//largura = new Integer(parametros[parametros.length - 2]).intValue();					
				//altura  = new Integer(parametros[parametros.length - 3]).intValue();
			} catch (Exception e) {
				// Default
				largura = 108;
				altura  = 160;
			}

			int larguraImagem = imagem.getWidth(null);
			int alturaImagem = imagem.getHeight(null);
			BufferedImage imagemNova;
			if (larguraImagem <= largura && alturaImagem <= altura) {
				// N�o redimensiona a imagem.
				imagemNova = imagem;
			}
			else {
				// Define largura e altura.
				if (larguraImagem >= alturaImagem) {
					altura = -1;
				}
				else {
					largura = -1;
				}

				// Redimensiona a foto.
				Image imagemTemp = imagem.getScaledInstance(largura, altura, Image.SCALE_DEFAULT);
				imagemNova = new BufferedImage(imagemTemp.getWidth(null), imagemTemp.getHeight(null), BufferedImage.TYPE_INT_RGB);
				Graphics2D graphics2D = imagemNova.createGraphics();
				graphics2D.drawImage(imagemTemp, 0, 0, null);
				graphics2D.dispose();
			}

			// Envia o arquivo.
//			OutputStream saida = new GZIPOutputStream(response.getOutputStream());
			BufferedOutputStream saida = new BufferedOutputStream(response.getOutputStream());

			if (arquivo.getTipoconteudo().matches(".*png.*")) {
				ImageIO.write(imagemNova, "png", saida);
			}
			else if (arquivo.getTipoconteudo().matches(".*jpe?g.*")) {
				ImageIO.write(imagemNova, "jpg", saida);
			}
			else if (arquivo.getTipoconteudo().matches(".*gif.*")) {
				ImageIO.write(imagemNova, "gif", saida);
			}
			else if (arquivo.getTipoconteudo().matches(".*bmp.*")) {
				ImageIO.write(imagemNova, "bmp", saida);
			}
			else {
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				return;
			}
			
//			if(saida instanceof GZIPOutputStream){
//				((GZIPOutputStream)saida).finish();
//			}
			
			saida.flush();
		}
		else { // N�o � uma imagem, ou n�o � uma imagem suportada para redimensionamento.
			response.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
		}
		
		return;		
	}
	
	private Long extractCdfile(String requestURI) throws Exception {
		Pattern pattern = Pattern.compile(".+?/([0-9]+)");
		Matcher matcher = pattern.matcher(requestURI);
		if (matcher.find()) {
			return new Long(matcher.group(1));
		}
		else {
			throw new Exception("URL inv�lida");
		}
	}	

}
