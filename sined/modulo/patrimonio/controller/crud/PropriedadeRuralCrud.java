package br.com.linkcom.sined.modulo.patrimonio.controller.crud;


import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.PropriedadeRural;
import br.com.linkcom.sined.geral.bean.PropriedadeRuralHistorico;
import br.com.linkcom.sined.geral.bean.enumeration.TipoTerceiros;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.PropriedadeRuralHistoricoService;
import br.com.linkcom.sined.geral.service.PropriedadeRuralService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.PropriedadeRuralFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/patrimonio/crud/PropriedadeRural", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields={"cdPropriedadeRural", "nome", "codigoImovel", "cafir", "caepf", "empresa"})
public class PropriedadeRuralCrud extends CrudControllerSined<PropriedadeRuralFiltro, PropriedadeRural, PropriedadeRural> {
	private PropriedadeRuralService propriedadeRuralService;
	private EnderecoService enderecoService;
	private EmpresaService empresaService;
	private PropriedadeRuralHistoricoService propriedadeRuralHistoricoService;
	
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setPropriedadeRuralService(PropriedadeRuralService propriedadeRuralService) {this.propriedadeRuralService = propriedadeRuralService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setPropriedadeRuralHistoricoService(PropriedadeRuralHistoricoService propriedadeRuralHistoricoService) {this.propriedadeRuralHistoricoService = propriedadeRuralHistoricoService;}
	
	@Override
	protected void validateBean(PropriedadeRural bean, BindException errors){
		enderecoService.validateListaEndereco(errors, bean.getListaEndereco());
		propriedadeRuralService.validateListaParticipantes(errors, bean.getListaParticipantePropriedadeRural(), bean.getTipoTerceiros(), bean.getEmpresa());
		
		if(bean.getCodigoImovel() != null && propriedadeRuralService.existeCodigoImovelCadastrado(bean.getCodigoImovel(), bean.getCdPropriedadeRural(), bean.getEmpresa().getCdpessoa())) {
			errors.reject("001", "J� existe uma propriedade rural cadastrada com o c�digo do im�vel informado nessa empresa.");
		}
		
		PropriedadeRural propriedadePrincipal = propriedadeRuralService.loadPrincipal(bean.getEmpresa().getCdpessoa());
		if((propriedadePrincipal == null && !bean.getPrincipal()) || (propriedadePrincipal != null && propriedadePrincipal.getCdPropriedadeRural().equals(bean.getCdPropriedadeRural()) && !bean.getPrincipal())){
			errors.reject("001", "Esta propriedade rural deve ser marcada como principal, pois n�o existe nenhuma marcada no momento.");
			bean.setPrincipal(Boolean.TRUE);
		}
	}
	
	@Override
	protected PropriedadeRural criar(WebRequestContext request, PropriedadeRural form) throws Exception {
		/*Integer ultimoCodigoImovel = 0;
		if(form.getEmpresa() != null && form.getEmpresa().getCdpessoa() != null){
			ultimoCodigoImovel =  propriedadeRuralService.getCodigoImovelUltimaPropriedadeRural(form.getEmpresa().getCdpessoa());
		}*/
		
		PropriedadeRural bean = super.criar(request, form);
		bean.setUsarSequencial(false);
		bean.setAtivo(true);
		bean.setTipoTerceiros(TipoTerceiros.NAO_SE_APLICA);
		
		return bean;
	}
	
	public ModelAndView ajaxBuscarUltimoCodigoImovelCadastrado(WebRequestContext request) {
		String cdPessoaString = request.getParameter("cdpessoa");
		Integer proximoSequencial = 0;
		if(!org.apache.commons.lang.StringUtils.isBlank(cdPessoaString)){
			Integer cdpessoa = Integer.parseInt(cdPessoaString);
			proximoSequencial = propriedadeRuralService.getCodigoImovelUltimaPropriedadeRural(cdpessoa);
		}
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("proximoSequencial", proximoSequencial != null ? proximoSequencial + 1 : "");
		
		return json;
	}
	
	@Override
	protected void salvar(WebRequestContext request, PropriedadeRural bean) throws Exception {
		boolean criar = bean.getCdPropriedadeRural() == null;
		PropriedadeRural propriedadeRuralAntiga = null;
		PropriedadeRural propriedadePrincipal = propriedadeRuralService.loadPrincipal(bean.getEmpresa().getCdpessoa());
		
		if(!criar){
			propriedadeRuralAntiga = propriedadeRuralService.loadForEntrada(bean);
		}
		
		if(criar && !propriedadeRuralService.havePrincipal(bean.getEmpresa().getCdpessoa())){
			bean.setPrincipal(true);
		}
		
		super.salvar(request, bean);
		
		if(propriedadePrincipal != null && !propriedadePrincipal.getCdPropriedadeRural().equals(bean.getCdPropriedadeRural())){
			propriedadeRuralService.updatePrincipal(propriedadePrincipal.getCdPropriedadeRural());			
		}
		
		propriedadeRuralHistoricoService.preencherHistorico(bean, propriedadeRuralAntiga, criar);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, PropriedadeRuralFiltro filtro) throws CrudException {
		List<Empresa> listaFazendas = empresaService.findFazendas();
		request.setAttribute("listaFazendas", listaFazendas);
		return super.doListagem(request, filtro);
	}
	
	@Override
	public ModelAndView doEntrada(WebRequestContext request, PropriedadeRural form) throws CrudException {
		List<Empresa> listaFazendas = empresaService.findFazendas();
		request.setAttribute("listaFazendas", listaFazendas);
		return super.doEntrada(request, form);
	}
	
	public ModelAndView visualizarPropriedadeRuralHistorico(WebRequestContext request, PropriedadeRuralHistorico propriedadeRuralHistorico){
		propriedadeRuralHistorico = propriedadeRuralHistoricoService.loadForEntrada(propriedadeRuralHistorico);
		
		request.setAttribute("TEMPLATE_beanName", propriedadeRuralHistorico);
		request.setAttribute("TEMPLATE_beanClass", PropriedadeRuralHistorico.class);
		request.setAttribute("observacao", "Propriedade Rural");
		request.setAttribute("consultar", true);

		return new ModelAndView("direct:crud/popup/visualizacaoPropriedadeRural").addObject("propriedadeRuralHistorico", propriedadeRuralHistorico);
	}
	
	public void ajaxConsultaPropriedadeRuralPrincipal(WebRequestContext request){
		String cdPessoaString = request.getParameter("cdpessoa");
		String hasPrincipal = "var hasPrincipal = ";
		
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		
		if(!org.apache.commons.lang.StringUtils.isBlank(cdPessoaString)){
			Integer cdpessoa = Integer.parseInt(cdPessoaString);
			PropriedadeRural propriedadePrincipal = propriedadeRuralService.loadPrincipal(cdpessoa);
			
			if(propriedadePrincipal == null){
				hasPrincipal += "false;";
			} else {
				hasPrincipal += "true;";
				hasPrincipal += "var nome = '" + propriedadePrincipal.getNome() + "';";
			}
		}else {
			hasPrincipal += "false;";
		}
		
		view.println(hasPrincipal);
	}
	
}
