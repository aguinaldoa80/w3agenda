package br.com.linkcom.sined.modulo.producao.controller.report;

import java.text.SimpleDateFormat;
import java.util.List;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialorigem;
import br.com.linkcom.sined.geral.filter.SinedFilter;
import br.com.linkcom.sined.geral.service.ProducaoordemmaterialService;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.EtiquetaExpedicaoproducaoBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Bean
@DisplayName("EMITIR ETIQUETA DE EXPEDI��O DE PRODU��O")
@Controller(path = "/producao/relatorio/EmitirEtiquetaExpedicaoproducao",authorizationModule=ReportAuthorizationModule.class)
public class EtiquetaExpedicaoproducaoReport extends SinedReport<SinedFilter>{
	private ProducaoordemmaterialService producaoordemmaterialService;
		
	public void setProducaoordemmaterialService(ProducaoordemmaterialService producaoordemmaterialService) {
		this.producaoordemmaterialService = producaoordemmaterialService;
	}

	@Override
	public String getNomeArquivo() {	
		return "EMITIR ETIQUETA DE EXPEDI��O DE PRODU��O";
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	SinedFilter filtro) throws Exception {
		String whereIn = SinedUtil.getItensSelecionados(request);
		String etiquetaTipo = request.getParameter("etiquetaTipo");
		List<EtiquetaExpedicaoproducaoBean> listaBean = new ListSet<EtiquetaExpedicaoproducaoBean>(EtiquetaExpedicaoproducaoBean.class);
		
		try{
			if(whereIn == null || "".equals(whereIn)){
				throw new SinedException("Nenhum item selecionado");
			}
			else if(etiquetaTipo == null || etiquetaTipo.trim().isEmpty()){
				throw new SinedException("N�o foi poss�vel verificar o tipo de etiqueta!");
			}
			
			List<Producaoordemmaterial> lista = producaoordemmaterialService.findAllForEtiquetaExpedicaoproducao(whereIn);
			if(lista != null && ! lista.isEmpty()){
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				for(Producaoordemmaterial pom : lista){
					EtiquetaExpedicaoproducaoBean bean = new EtiquetaExpedicaoproducaoBean();
					
					StringBuilder cliente = new StringBuilder();
					if(pom.getListaProducaoordemmaterialorigem() != null && !pom.getListaProducaoordemmaterialorigem().isEmpty()){
						Producaoordemmaterialorigem pomo = pom.getListaProducaoordemmaterialorigem().iterator().next();
						if(pomo.getProducaoagenda() != null){
							
							if(pomo.getProducaoagenda().getCliente() != null){
								cliente.append(pomo.getProducaoagenda().getCliente().getNome());
							}
							
							if(pomo.getProducaoagenda().getContrato() != null){
								if(cliente.length() > 0)
									cliente.append(" - ");
								
								if(pomo.getProducaoagenda().getContrato().getIdentificador() != null 
										&& !pomo.getProducaoagenda().getContrato().getIdentificador().trim().isEmpty()){
									cliente.append(pomo.getProducaoagenda().getContrato().getIdentificador());
								}
								else{
									cliente.append(pomo.getProducaoagenda().getContrato().getCdcontrato());
								}
								
								if(pomo.getProducaoagenda().getContrato().getContratotipo() != null)
									cliente.append("/" + pomo.getProducaoagenda().getContrato().getContratotipo().getNome());
							}
							
							bean.setCliente(cliente.toString());
						}
					}
					if(pom.getMaterial().getIdentificacao() != null)
						bean.setProduto(pom.getMaterial().getIdentificacao() + " - ");
					bean.setProduto(bean.getProduto() + pom.getMaterial().getNome());
					bean.setDataentrega(sdf.format(pom.getProducaoordem().getDtprevisaoentrega()));
					bean.setTotal(pom.getQtde().toString());
					bean.setDatavalidade("");
					bean.setIdentificador_produto(pom.getCdproducaoordemmaterial().toString());
					
					listaBean.add(bean);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			return null;
		}
		
		Report report = new Report("producao/EtiquetaExpedicaoproducao"+etiquetaTipo);
		report.setDataSource(listaBean);
		return report;
	}

	@Override
	public String getTitulo(SinedFilter filtro) {
		return null;
	}
}