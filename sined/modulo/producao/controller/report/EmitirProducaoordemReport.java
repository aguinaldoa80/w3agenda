package br.com.linkcom.sined.modulo.producao.controller.report;

import java.util.LinkedHashMap;
import java.util.LinkedList;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ProducaoordemService;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.EmitirProducaoordemReportBean;

@Controller(path="/producao/relatorio/EmitirProducaoordem")
public class EmitirProducaoordemReport extends ReportTemplateController<ReportTemplateFiltro> {
	
	private ProducaoordemService producaoordemService;
	
	public void setProducaoordemService(ProducaoordemService producaoordemService) {
		this.producaoordemService = producaoordemService;
	}

	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.EMITIR_PRODUCAO_ORDEM;
	}

	@Override
	protected LinkedHashMap<String, Object> carregaDados(WebRequestContext request, ReportTemplateFiltro filtro) throws Exception {	
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		String whereIn = request.getParameter("selectedItens");
		LinkedList<EmitirProducaoordemReportBean> lista = producaoordemService.makeProducaoordemReportTemplate(whereIn);
		
		if(lista != null && lista.size() > 0){
			map.put("EmitirProducaoordemReportBean", lista.get(0));
		}
		map.put("lista", lista);
		return map;
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	ReportTemplateFiltro filtro) {
		return getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.EMITIR_PRODUCAO_ORDEM);
	}

	@Override
	protected String getFileName() {
		return "ordemproducao";
	}
}