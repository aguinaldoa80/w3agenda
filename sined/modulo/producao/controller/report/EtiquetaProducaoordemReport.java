package br.com.linkcom.sined.modulo.producao.controller.report;

import java.util.List;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.filter.SinedFilter;
import br.com.linkcom.sined.geral.service.ProducaoordemmaterialService;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.EtiquetaProducaoordemBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/producao/relatorio/EmitirEtiquetaProducaoordem", authorizationModule=ReportAuthorizationModule.class)
public class EtiquetaProducaoordemReport extends SinedReport<SinedFilter>{
	private ProducaoordemmaterialService producaoordemmaterialService;
		
	public void setProducaoordemmaterialService(ProducaoordemmaterialService producaoordemmaterialService) {
		this.producaoordemmaterialService = producaoordemmaterialService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,SinedFilter filtro) throws Exception {	
		String whereIn = SinedUtil.getItensSelecionados(request);	
		String fromPage = request.getParameter("fromPage");	
		String etiquetaTipo = request.getParameter("etiquetaTipo");
		List<EtiquetaProducaoordemBean> listaBean = new ListSet<EtiquetaProducaoordemBean>(EtiquetaProducaoordemBean.class);
		
		try{
			if(whereIn == null || "".equals(whereIn)){
				throw new SinedException("Nenhum item selecionado");
			}
			else if(fromPage == null || fromPage.trim().equals("")){
				throw new SinedException("N�o foi poss�vel verificar a p�gina de origem!");
			}
			else if(etiquetaTipo == null || etiquetaTipo.trim().isEmpty()){
				throw new SinedException("N�o foi poss�vel verificar o tipo de etiqueta!");
			}
			
			List<Producaoordemmaterial> lista = producaoordemmaterialService.findAllForEtiquetaProducaoordem(whereIn, fromPage);
			if(lista != null && !lista.isEmpty()){
				for(Producaoordemmaterial pom : lista){
					EtiquetaProducaoordemBean bean = new EtiquetaProducaoordemBean();
					
					if(pom.getProducaoordem().getDepartamento() != null)
						bean.setDepartamento(pom.getProducaoordem().getDepartamento().getNome());
					if(pom.getMaterial().getIdentificacao() != null)
						bean.setProduto(pom.getMaterial().getIdentificacao() + " - ");
					bean.setProduto(bean.getProduto() + pom.getMaterial().getNome());
					bean.setTotal(pom.getQtde().toString());
					
					listaBean.add(bean);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			return null;
		}
		
		Report report = new Report("producao/EtiquetaProducaoordem"+etiquetaTipo);
		report.setDataSource(listaBean);
		return report;
	}
	
	@Override
	public String getNomeArquivo() {	
		return null;
	}
	
	@Override
	public String getTitulo(SinedFilter filtro) {
		return null;
	}
}