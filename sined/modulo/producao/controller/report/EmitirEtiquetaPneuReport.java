package br.com.linkcom.sined.modulo.producao.controller.report;

import java.util.LinkedList;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ProducaoagendamaterialService;

@Controller(path = "/producao/relatorio/EmitirEtiquetaPneuReport", authorizationModule=ReportAuthorizationModule.class)
public class EmitirEtiquetaPneuReport extends ReportTemplateController<ReportTemplateFiltro>{
	
	private ProducaoagendamaterialService producaoAgendaMaterialService;
	
	public void setProducaoAgendaMaterialService(ProducaoagendamaterialService producaoAgendaMaterialService) {this.producaoAgendaMaterialService = producaoAgendaMaterialService;}
	
	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.ETIQUETA_DO_PNEU;
	}

	@Override
	protected String getFileName() {
		return "etiqueta_pneu";
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	ReportTemplateFiltro filtro) {
		return producaoAgendaMaterialService.getTemplateByRaw(request); 
	}
	
	@Override
	protected LinkedList<ReportTemplateBean> carregaListaDados(WebRequestContext request, ReportTemplateFiltro filtro)throws Exception {
		return producaoAgendaMaterialService.carregaDadosEtiquetaPneuReport(request);
	}

}
