package br.com.linkcom.sined.modulo.producao.controller.report.bean;

import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;

public class PedidovendamaterialReportBean {
	
	protected Integer cdpedidovendamaterial;
	protected MaterialReportBean materialReportBean;
	
	protected String altura;
	protected String largura;
	protected String comprimento;
	protected String quantidade;
	protected String preco;
	
	public PedidovendamaterialReportBean(){}
	
	public PedidovendamaterialReportBean(Pedidovendamaterial pedidovendamaterial){
		cdpedidovendamaterial = pedidovendamaterial.getCdpedidovendamaterial();
		materialReportBean = new MaterialReportBean(pedidovendamaterial.getMaterial());
		altura = pedidovendamaterial.getAltura() != null ? pedidovendamaterial.getAltura().toString(): "";
		largura = pedidovendamaterial.getLargura() != null ? pedidovendamaterial.getLargura().toString() : "";
		comprimento = pedidovendamaterial.getComprimento() != null ? pedidovendamaterial.getComprimento().toString() : "";
		quantidade = pedidovendamaterial.getQuantidade() != null ? pedidovendamaterial.getQuantidade().toString() : "";
		preco = pedidovendamaterial.getPreco() != null ? pedidovendamaterial.getPreco().toString() : "";
	}

	public Integer getCdpedidovendamaterial() {
		return cdpedidovendamaterial;
	}
	public MaterialReportBean getMaterialReportBean() {
		return materialReportBean;
	}
	public String getAltura() {
		return altura;
	}
	public String getLargura() {
		return largura;
	}
	public String getComprimento() {
		return comprimento;
	}
	public String getQuantidade() {
		return quantidade;
	}
	public String getPreco() {
		return preco;
	}

	public void setCdpedidovendamaterial(Integer cdpedidovendamaterial) {
		this.cdpedidovendamaterial = cdpedidovendamaterial;
	}
	public void setMaterialReportBean(MaterialReportBean materialReportBean) {
		this.materialReportBean = materialReportBean;
	}
	public void setAltura(String altura) {
		this.altura = altura;
	}
	public void setLargura(String largura) {
		this.largura = largura;
	}
	public void setComprimento(String comprimento) {
		this.comprimento = comprimento;
	}
	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}
	public void setPreco(String preco) {
		this.preco = preco;
	}
}
