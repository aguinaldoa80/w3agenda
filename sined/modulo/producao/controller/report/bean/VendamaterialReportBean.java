package br.com.linkcom.sined.modulo.producao.controller.report.bean;

import br.com.linkcom.sined.geral.bean.Vendamaterial;

public class VendamaterialReportBean {
	
	protected Integer cdvendamaterial;
	protected MaterialReportBean materialReportBean;
	
	protected String altura;
	protected String largura;
	protected String comprimento;
	protected String quantidade;
	protected String preco;
	
	public VendamaterialReportBean(){}
	
	public VendamaterialReportBean(Vendamaterial vendamaterial){
		cdvendamaterial = vendamaterial.getCdvendamaterial();
		materialReportBean = new MaterialReportBean(vendamaterial.getMaterial());
		altura = vendamaterial.getAltura() != null ? vendamaterial.getAltura().toString(): "";
		largura = vendamaterial.getLargura() != null ? vendamaterial.getLargura().toString() : "";
		comprimento = vendamaterial.getComprimento() != null ? vendamaterial.getComprimento().toString() : "";
		quantidade = vendamaterial.getQuantidade() != null ? vendamaterial.getQuantidade().toString() : "";
		preco = vendamaterial.getPreco() != null ? vendamaterial.getPreco().toString() : "";
	}
	
	public Integer getCdvendamaterial() {
		return cdvendamaterial;
	}
	public MaterialReportBean getMaterialReportBean() {
		return materialReportBean;
	}
	public String getAltura() {
		return altura;
	}
	public String getLargura() {
		return largura;
	}
	public String getComprimento() {
		return comprimento;
	}
	public String getQuantidade() {
		return quantidade;
	}
	public String getPreco() {
		return preco;
	}
	
	public void setCdvendamaterial(Integer cdvendamaterial) {
		this.cdvendamaterial = cdvendamaterial;
	}
	public void setMaterialReportBean(MaterialReportBean materialReportBean) {
		this.materialReportBean = materialReportBean;
	}
	public void setAltura(String altura) {
		this.altura = altura;
	}
	public void setLargura(String largura) {
		this.largura = largura;
	}
	public void setComprimento(String comprimento) {
		this.comprimento = comprimento;
	}
	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}
	public void setPreco(String preco) {
		this.preco = preco;
	}
}
