package br.com.linkcom.sined.modulo.producao.controller.report.bean;

import br.com.linkcom.sined.geral.bean.Contratomaterial;

public class ContratomaterialReportBean {
	
	protected Integer cdcontratomaterial; 
	protected MaterialReportBean materialReportBean;
	protected Double qtde;
	
	public ContratomaterialReportBean(){}
	
	public ContratomaterialReportBean(Contratomaterial contratomaterial) {
		if(contratomaterial != null){
			cdcontratomaterial = contratomaterial.getCdcontratomaterial();
			materialReportBean = new MaterialReportBean(contratomaterial.getServico());
			qtde = contratomaterial.getQtde();
		}
	}

	public Integer getCdcontratomaterial() {
		return cdcontratomaterial;
	}
	public MaterialReportBean getMaterialReportBean() {
		return materialReportBean;
	}
	public Double getQtde() {
		return qtde;
	}

	public void setCdcontratomaterial(Integer cdcontratomaterial) {
		this.cdcontratomaterial = cdcontratomaterial;
	}
	public void setMaterialReportBean(MaterialReportBean materialReportBean) {
		this.materialReportBean = materialReportBean;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
}
