package br.com.linkcom.sined.modulo.producao.controller.report.bean;

import java.util.LinkedList;

import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratomaterial;

public class ContratoReportBean {
	
	protected Integer cdcontrato; 
	
	protected LinkedList<ContratomaterialReportBean> listaContrartomaterialReportBean;

	public ContratoReportBean(){}
	
	public ContratoReportBean(Contrato contrato) {
		if(contrato != null){
			cdcontrato = contrato.getCdcontrato();
			if(contrato.getListaContratomaterial() !=  null && !contrato.getListaContratomaterial().isEmpty()){
				listaContrartomaterialReportBean = new LinkedList<ContratomaterialReportBean>();
				for(Contratomaterial contratomaterial : contrato.getListaContratomaterial()){
					listaContrartomaterialReportBean.add(new ContratomaterialReportBean(contratomaterial));
				}
			}
		}
	}

	public Integer getCdcontrato() {
		return cdcontrato;
	}
	public LinkedList<ContratomaterialReportBean> getListaContrartomaterialReportBean() {
		return listaContrartomaterialReportBean;
	}

	public void setCdcontrato(Integer cdcontrato) {
		this.cdcontrato = cdcontrato;
	}
	public void setListaContrartomaterialReportBean(
			LinkedList<ContratomaterialReportBean> listaContrartomaterialReportBean) {
		this.listaContrartomaterialReportBean = listaContrartomaterialReportBean;
	}
}
