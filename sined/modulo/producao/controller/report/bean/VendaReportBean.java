package br.com.linkcom.sined.modulo.producao.controller.report.bean;

import java.util.LinkedList;

import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;

public class VendaReportBean {
	
	protected Integer cdvenda;
	protected String dtvenda;
	
	protected LinkedList<VendamaterialReportBean> listavendamaterial;
	
	public VendaReportBean(){}
	
	public VendaReportBean(Venda venda) {
		if(venda != null){
			cdvenda = venda.getCdvenda();
			dtvenda =	venda.getDtvendastr();
			
			if(venda.getListavendamaterial() != null && !venda.getListavendamaterial().isEmpty()){
				listavendamaterial = new LinkedList<VendamaterialReportBean>();
				for(Vendamaterial vendamaterial : venda.getListavendamaterial()){
					listavendamaterial.add(new VendamaterialReportBean(vendamaterial));
				}
			}
		}
	}

	public Integer getCdvenda() {
		return cdvenda;
	}
	public String getDtvenda() {
		return dtvenda;
	}
	public LinkedList<VendamaterialReportBean> getListavendamaterial() {
		return listavendamaterial;
	}

	public void setCdvenda(Integer cdvenda) {
		this.cdvenda = cdvenda;
	}
	public void setDtvenda(String dtvenda) {
		this.dtvenda = dtvenda;
	}
	public void setListavendamaterial(
			LinkedList<VendamaterialReportBean> listavendamaterial) {
		this.listavendamaterial = listavendamaterial;
	}
}
