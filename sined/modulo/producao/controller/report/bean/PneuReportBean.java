package br.com.linkcom.sined.modulo.producao.controller.report.bean;


import br.com.linkcom.sined.geral.bean.Pneu;

public class PneuReportBean {
	
	private String id;
	private String marca;
	private String modelo;
	private String medida;
	private String seriefogo;
	private String dot;
	private String numeroreforma;
	private String banda;
	private String qualificacao;
	
	private String segmento;
	private String otrpneutipo;
	private String lonas;
	private String descricao;
	private String roda;
	private String otrconstrucao;
	private String otrclassificacaocodigo;
	private String otrservicotipo;
	private String otrdesenho;
	
	public PneuReportBean(){}
	
	public PneuReportBean(Pneu pneu){
		id = pneu != null && pneu.getCdpneu() != null ? pneu.getCdpneu().toString() : "";
		marca = pneu != null && pneu.getPneumarca() != null && pneu.getPneumarca().getNome() != null ? pneu.getPneumarca().getNome() : "";
		modelo = pneu != null && pneu.getPneumodelo() != null && pneu.getPneumodelo().getNome() != null ? pneu.getPneumodelo().getNome() : "";
		medida = pneu != null && pneu.getPneumedida() != null && pneu.getPneumedida().getNome() != null ? pneu.getPneumedida().getNome() : "";
		seriefogo = pneu != null && pneu.getSerie() != null ? pneu.getSerie() : "";
		dot = pneu != null && pneu.getDot() != null ? pneu.getDot() : "";
		numeroreforma = pneu != null && pneu.getNumeroreforma() != null && pneu.getNumeroreforma().getNome() != null ? pneu.getNumeroreforma().getNome() : "";
		banda = pneu != null && pneu.getMaterialbanda() != null && pneu.getMaterialbanda().getNome() != null ? pneu.getMaterialbanda().getNome() : "";
		qualificacao = pneu != null && pneu.getPneuqualificacao() != null && pneu.getPneuqualificacao().getNome() != null ? pneu.getPneuqualificacao().getNome() : ""; 
		lonas = pneu != null && pneu.getLonas() != null ? pneu.getLonas().toString() : "";
		roda = pneu != null && pneu.getAcompanhaRoda() != null ? Boolean.TRUE ==pneu.getAcompanhaRoda() ? "Sim" :"N�o"  : "";
	/*	...
		segmento = pneu != null && pneu.getCdpneu() != null ? pneu.getCdpneu().toString() : "";
		otrpneutipo = pneu != null && pneu.getCdpneu() != null ? pneu.getCdpneu().toString() : "";
		lonas = pneu != null && pneu.getCdpneu() != null ? pneu.getCdpneu().toString() : "";
		descricao = pneu != null && pneu.getCdpneu() != null ? pneu.getCdpneu().toString() : "";
		roda = pneu != null && pneu.getCdpneu() != null ? pneu.getCdpneu().toString() : "";
		otrconstrucao = pneu != null && pneu.getCdpneu() != null ? pneu.getCdpneu().toString() : "";
		otrclassificacaocodigo = pneu != null && pneu.getCdpneu() != null ? pneu.getCdpneu().toString() : "";
		otrservicotipo = pneu != null && pneu.getCdpneu() != null ? pneu.getCdpneu().toString() : "";
		otrdesenho = pneu != null && pneu.getCdpneu() != null ? pneu.getCdpneu().toString() : "";*/
	
	}

	public String getMarca() {
		return marca;
	}

	public String getModelo() {
		return modelo;
	}

	public String getMedida() {
		return medida;
	}

	public String getSeriefogo() {
		return seriefogo;
	}

	public String getDot() {
		return dot;
	}

	public String getNumeroreforma() {
		return numeroreforma;
	}

	public String getBanda() {
		return banda;
	}

	public String getQualificacao() {
		return qualificacao;
	}
	
	public String getId() {
		return id;
	}
	
	public String getSegmento() {
		return segmento;
	}
	
	public String getOtrpneutipo() {
		return otrpneutipo;
	}
	
	public String getLonas() {
		return lonas;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public String getRoda() {
		return roda;
	}
	
	public String getOtrconstrucao() {
		return otrconstrucao;
	}
	
	public String getOtrclassificacaocodigo() {
		return otrclassificacaocodigo;
	}
	
	public String getOtrservicotipo() {
		return otrservicotipo;
	}
	
	public String getOtrdesenho() {
		return otrdesenho;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public void setMedida(String medida) {
		this.medida = medida;
	}

	public void setSeriefogo(String seriefogo) {
		this.seriefogo = seriefogo;
	}

	public void setDot(String dot) {
		this.dot = dot;
	}

	public void setNumeroreforma(String numeroreforma) {
		this.numeroreforma = numeroreforma;
	}

	public void setBanda(String banda) {
		this.banda = banda;
	}

	public void setQualificacao(String qualificacao) {
		this.qualificacao = qualificacao;
	}
	
	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}
	
	public void setOtrpneutipo(String otrpneutipo) {
		this.otrpneutipo = otrpneutipo;
	}
	
	public void setLonas(String lonas) {
		this.lonas = lonas;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setRoda(String roda) {
		this.roda = roda;
	}
	
	public void setOtrconstrucao(String otrconstrucao) {
		this.otrconstrucao = otrconstrucao;
	}
	
	public void setOtrclassificacaocodigo(String otrclassificacaocodigo) {
		this.otrclassificacaocodigo = otrclassificacaocodigo;
	}
	
	public void setOtrservicotipo(String otrservicotipo) {
		this.otrservicotipo = otrservicotipo;
	}
	
	public void setOtrdesenho(String otrdesenho) {
		this.otrdesenho = otrdesenho;
	}
	
}