package br.com.linkcom.sined.modulo.producao.controller.report.bean;

public class EtiquetaExpedicaoproducaoBean {
	protected String cliente;
	protected String produto;
	protected String dataentrega;
	protected String total;
	protected String datavalidade;
	protected String identificador_produto;
	
	
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getProduto() {
		return produto;
	}
	public void setProduto(String produto) {
		this.produto = produto;
	}
	public String getDataentrega() {
		return dataentrega;
	}
	public void setDataentrega(String dataentrega) {
		this.dataentrega = dataentrega;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getDatavalidade() {
		return datavalidade;
	}
	public void setDatavalidade(String datavalidade) {
		this.datavalidade = datavalidade;
	}
	public String getIdentificador_produto() {
		return identificador_produto;
	}
	public void setIdentificador_produto(String identificadorProduto) {
		identificador_produto = identificadorProduto;
	}
}
