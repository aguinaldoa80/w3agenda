package br.com.linkcom.sined.modulo.producao.controller.report.bean;


import java.awt.Image;

import br.com.linkcom.sined.geral.bean.Material;

public class MaterialReportBean {
	
	protected String cdmaterial;
	protected String nome;
	protected String identificacao;
	protected String materialgrupo_nome;
	protected String codigobarras; 
	protected String valorvenda; 
	protected String valorcusto; 
	protected String cor; 
	protected Image codigobarras_imagem;
	protected String unidade_medida_nome;
	protected String unidade_medida_simbolo;
	protected String numero_lote;
	protected String validade_lote;
	protected String empresa_recebimento;
	protected Integer cdentrega;
	protected Integer cdordemcompra;
	protected Integer cdentregamaterial;
	protected Integer cdentregadocumento;
	protected Integer cdmovimentacaoestoque;
	
	public MaterialReportBean(){}
	
	public MaterialReportBean(Material material){
		if(material != null){
			nome = material.getNome();
			identificacao = material.getIdentificacao();
			if(material.getMaterialgrupo() != null){
				materialgrupo_nome = material.getMaterialgrupo().getNome();
			}
		}
	}
	
	public String getCdmaterial() {
		return cdmaterial;
	}
	public String getIdentificacao() {
		return identificacao;
	}
	public String getMaterialgrupo_nome() {
		return materialgrupo_nome;
	}
	

	public String getCodigobarras() {
		return codigobarras;
	}
	
	public String getNome() {
		return nome;
	}
	
	public String getValorvenda() {
		return valorvenda;
	}
	
	public String getValorcusto() {
		return valorcusto;
	}
	
	public String getCor() {
		return cor;
	}
	public Image getCodigobarras_imagem() {
		return codigobarras_imagem;
	}
	public String getUnidade_medida_nome() {
		return unidade_medida_nome;
	}
	public String getUnidade_medida_simbolo() {
		return unidade_medida_simbolo;
	}
	public String getNumero_lote() {
		return numero_lote;
	}
	public String getValidade_lote() {
		return validade_lote;
	}
	
	public void setCdmaterial(String cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}
	
	public void setMaterialgrupo_nome(String materialgrupoNome) {
		materialgrupo_nome = materialgrupoNome;
	}
	
	public void setCodigobarras(String codigobarras) {
		this.codigobarras = codigobarras;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setValorvenda(String valorvenda) {
		this.valorvenda = valorvenda;
	}
	
	public void setValorcusto(String valorcusto) {
		this.valorcusto = valorcusto;
	}
	
	public void setCor(String cor) {
		this.cor = cor;
	}
	public void setCodigobarras_imagem(Image codigobarrasImagem) {
		codigobarras_imagem = codigobarrasImagem;
	}
	
	public void setUnidade_medida_nome(String unidadeMedidaNome) {
		unidade_medida_nome = unidadeMedidaNome;
	}
	public void setUnidade_medida_simbolo(String unidadeMedidaSimbolo) {
		unidade_medida_simbolo = unidadeMedidaSimbolo;
	}
	
	public void setNumero_lote(String numeroLote) {
		numero_lote = numeroLote;
	}
	public void setValidade_lote(String validadeLote) {
		validade_lote = validadeLote;
	}
	
	public String getEmpresa_recebimento() {
		return empresa_recebimento;
	}
	public void setEmpresa_recebimento(String empresaRecebimento) {
		empresa_recebimento = empresaRecebimento;
	}
	
	public Integer getCdentrega() {
		return cdentrega;
	}

	public void setCdentrega(Integer cdentrega) {
		this.cdentrega = cdentrega;
	}

	public Integer getCdordemcompra() {
		return cdordemcompra;
	}

	public void setCdordemcompra(Integer cdordemcompra) {
		this.cdordemcompra = cdordemcompra;
	}

	public Integer getCdentregamaterial() {
		return cdentregamaterial;
	}

	public void setCdentregamaterial(Integer cdentregamaterial) {
		this.cdentregamaterial = cdentregamaterial;
	}

	public Integer getCdentregadocumento() {
		return cdentregadocumento;
	}

	public void setCdentregadocumento(Integer cdentregadocumento) {
		this.cdentregadocumento = cdentregadocumento;
	}

	public Integer getCdmovimentacaoestoque() {
		return cdmovimentacaoestoque;
	}

	public void setCdmovimentacaoestoque(Integer cdmovimentacaoestoque) {
		this.cdmovimentacaoestoque = cdmovimentacaoestoque;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final MaterialReportBean other = (MaterialReportBean) obj;
		if (cdmaterial == null) {
			if (other.cdmaterial != null)
				return false;
		} else if (!cdmaterial.equals(other.cdmaterial))
			return false;
		return true;
	}
	
}
