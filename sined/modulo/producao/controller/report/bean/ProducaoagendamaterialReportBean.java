package br.com.linkcom.sined.modulo.producao.controller.report.bean;

import java.util.LinkedList;

import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterialmateriaprima;

public class ProducaoagendamaterialReportBean {
	
	protected Integer cdproducaoagendamaterial; 
	protected MaterialReportBean materialReportBean; 
	protected Double largura;
	protected Double altura;
	protected Double comprimento; 
	protected Double qtde;
	protected String producaoetapa_nome;
	protected String observacao;
	protected String unidade_medida;
	
	protected LinkedList<ProducaoagendamaterialmateriaprimaReportBean> listaProducaoagendamaterialmateriaprimaReportBean;
	
	public ProducaoagendamaterialReportBean (){}
	
	public ProducaoagendamaterialReportBean (Producaoagendamaterial producaoagendamaterial){
		if(producaoagendamaterial != null){
			cdproducaoagendamaterial = producaoagendamaterial.getCdproducaoagendamaterial();
			materialReportBean = new MaterialReportBean(producaoagendamaterial.getMaterial());
			altura = producaoagendamaterial.getAltura();
			largura = producaoagendamaterial.getLargura();
			comprimento = producaoagendamaterial.getComprimento();
			qtde =  producaoagendamaterial.getQtde();
			producaoetapa_nome = producaoagendamaterial.getProducaoetapa() != null ? producaoagendamaterial.getProducaoetapa().getNome() : "";
			observacao = producaoagendamaterial.getObservacao();
			if (producaoagendamaterial.getUnidademedida() != null && producaoagendamaterial.getUnidademedida().getNome() != null) {
				unidade_medida = producaoagendamaterial.getUnidademedida().getNome();
			}
			
			if (producaoagendamaterial.getListaProducaoagendamaterialmateriaprima() != null
					&& !producaoagendamaterial.getListaProducaoagendamaterialmateriaprima().isEmpty()) {
				listaProducaoagendamaterialmateriaprimaReportBean = new LinkedList<ProducaoagendamaterialmateriaprimaReportBean>();
				for (Producaoagendamaterialmateriaprima bean : producaoagendamaterial.getListaProducaoagendamaterialmateriaprima()) {
					listaProducaoagendamaterialmateriaprimaReportBean.add(new ProducaoagendamaterialmateriaprimaReportBean(bean));
				}
			}
		}
	}
	
	public Integer getCdproducaoagendamaterial() {
		return cdproducaoagendamaterial;
	}
	public MaterialReportBean getMaterialReportBean() {
		return materialReportBean;
	}
	public Double getLargura() {
		return largura;
	}
	public Double getAltura() {
		return altura;
	}
	public Double getComprimento() {
		return comprimento;
	}
	public Double getQtde() {
		return qtde;
	}
	public String getProducaoetapa_nome() {
		return producaoetapa_nome;
	}
	public String getUnidade_medida() {
		return unidade_medida;
	}
	public LinkedList<ProducaoagendamaterialmateriaprimaReportBean> getListaProducaoagendamaterialmateriaprimaReportBean() {
		return listaProducaoagendamaterialmateriaprimaReportBean;
	}
	
	public void setListaProducaoagendamaterialmateriaprimaReportBean(
			LinkedList<ProducaoagendamaterialmateriaprimaReportBean> listaProducaoagendamaterialmateriaprimaReportBean) {
		this.listaProducaoagendamaterialmateriaprimaReportBean = listaProducaoagendamaterialmateriaprimaReportBean;
	}
	public void setCdproducaoagendamaterial(Integer cdproducaoagendamaterial) {
		this.cdproducaoagendamaterial = cdproducaoagendamaterial;
	}
	public void setMaterialReportBean(MaterialReportBean materialReportBean) {
		this.materialReportBean = materialReportBean;
	}
	public void setLargura(Double largura) {
		this.largura = largura;
	}
	public void setAltura(Double altura) {
		this.altura = altura;
	}
	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setProducaoetapa_nome(String producaoetapaNome) {
		producaoetapa_nome = producaoetapaNome;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setUnidade_medida(String unidadeMedida) {
		unidade_medida = unidadeMedida;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ProducaoagendamaterialReportBean other = (ProducaoagendamaterialReportBean) obj;
		if (cdproducaoagendamaterial == null) {
			if (other.cdproducaoagendamaterial != null)
				return false;
		} else if (!cdproducaoagendamaterial.equals(other.cdproducaoagendamaterial))
			return false;
		return true;
	}
}
