package br.com.linkcom.sined.modulo.producao.controller.report.bean;

import br.com.linkcom.sined.geral.bean.Producaoagendamaterialitem;

public class ProducaoagendamaterialitemReportBean {
	
	protected Integer cdproducaoagendamaterialitem; 
	protected MaterialReportBean materialReportBean;
	protected Double qtde;
	
	public ProducaoagendamaterialitemReportBean(){}
	
	public ProducaoagendamaterialitemReportBean(Producaoagendamaterialitem producaoagendamaterialitem) {
		if(producaoagendamaterialitem != null){
			cdproducaoagendamaterialitem = producaoagendamaterialitem.getCdproducaoagendamaterialitem();
			materialReportBean = new MaterialReportBean(producaoagendamaterialitem.getMaterial());
			qtde = producaoagendamaterialitem.getQtde();
		}
	}
	
	public Integer getCdproducaoagendamaterialitem() {
		return cdproducaoagendamaterialitem;
	}
	public MaterialReportBean getMaterialReportBean() {
		return materialReportBean;
	}
	public Double getQtde() {
		return qtde;
	}

	public void setCdproducaoagendamaterialitem(Integer cdproducaoagendamaterialitem) {
		this.cdproducaoagendamaterialitem = cdproducaoagendamaterialitem;
	}
	public void setMaterialReportBean(MaterialReportBean materialReportBean) {
		this.materialReportBean = materialReportBean;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ProducaoagendamaterialitemReportBean other = (ProducaoagendamaterialitemReportBean) obj;
		if (cdproducaoagendamaterialitem == null) {
			if (other.cdproducaoagendamaterialitem != null)
				return false;
		} else if (!cdproducaoagendamaterialitem.equals(other.cdproducaoagendamaterialitem))
			return false;
		return true;
	}
}
