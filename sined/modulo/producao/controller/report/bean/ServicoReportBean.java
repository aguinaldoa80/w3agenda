package br.com.linkcom.sined.modulo.producao.controller.report.bean;


import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;

public class ServicoReportBean {
	
	private String codigoMaterial;
	private String material;
	private String cicloProducaoMaterial;
	private String observacao;
	private String unidadeMedida;
	private String materialColeta;
	
	
	public ServicoReportBean(){}
	
	public ServicoReportBean(Pedidovendamaterial pedidovendamaterial){
		if(pedidovendamaterial != null){
			codigoMaterial = pedidovendamaterial.getMaterial() != null && pedidovendamaterial.getMaterial().getCdmaterial() != null ? pedidovendamaterial.getMaterial().getCdmaterial().toString() : "";
			material = pedidovendamaterial.getMaterial() != null ? pedidovendamaterial.getMaterial().getNome() : "";
			cicloProducaoMaterial = pedidovendamaterial.getMaterial() != null && pedidovendamaterial.getMaterial().getProducaoetapa() != null ? pedidovendamaterial.getMaterial().getProducaoetapa().getNome() : "";
			observacao = pedidovendamaterial.getObservacao() != null ? pedidovendamaterial.getObservacao() : "";
			unidadeMedida = pedidovendamaterial.getUnidademedida() != null ? pedidovendamaterial.getUnidademedida().getNome() : "";
			materialColeta = pedidovendamaterial.getMaterialcoleta() != null ? pedidovendamaterial.getMaterialcoleta().getNome() : ""; 
		}
	}

	public String getCodigoMaterial() {
		return codigoMaterial;
	}

	public String getMaterial() {
		return material;
	}

	public String getCicloProducaoMaterial() {
		return cicloProducaoMaterial;
	}

	public String getObservacao() {
		return observacao;
	}

	public String getUnidadeMedida() {
		return unidadeMedida;
	}

	public String getMaterialColeta() {
		return materialColeta;
	}

	public void setCodigoMaterial(String codigoMaterial) {
		this.codigoMaterial = codigoMaterial;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public void setCicloProducaoMaterial(String cicloProducaoMaterial) {
		this.cicloProducaoMaterial = cicloProducaoMaterial;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public void setMaterialColeta(String materialColeta) {
		this.materialColeta = materialColeta;
	}
}