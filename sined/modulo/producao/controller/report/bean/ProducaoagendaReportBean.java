package br.com.linkcom.sined.modulo.producao.controller.report.bean;

import java.text.SimpleDateFormat;
import java.util.LinkedList;

import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterialitem;

public class ProducaoagendaReportBean {
	
	protected Integer cdproducaoagenda; 
	protected String producaoagendatipo_nome;
	protected String cliente_nome;
	protected ContratoReportBean contratoReportBean;
	protected PedidovendaReportBean pedidovendaReportBean;
	protected VendaReportBean vendaReportBean;
	protected String dtentrega;
	protected String producaoagendasituacao;
	
	protected LinkedList<ProducaoagendamaterialitemReportBean> listaProducaoagendamaterialitemReportBean;
	protected LinkedList<ProducaoagendamaterialReportBean> listaProducaoagendamaterialReportBean;
	
	public ProducaoagendaReportBean(){}
	
	public ProducaoagendaReportBean(Producaoagenda producaoagenda){
		if(producaoagenda != null){
			cdproducaoagenda = producaoagenda.getCdproducaoagenda();
			producaoagendatipo_nome = producaoagenda.getProducaoagendatipo() != null ? producaoagenda.getProducaoagendatipo().getNome() : "";
			cliente_nome = (producaoagenda.getCliente() != null && producaoagenda.getCliente().getNome() != null ?
					producaoagenda.getCliente().getNome() : "");
			dtentrega = producaoagenda.getDtentrega() != null ? new SimpleDateFormat("dd/MM/yyyy hh:mm").format(producaoagenda.getDtentrega()) : "";
			producaoagendasituacao = producaoagenda.getProducaoagendasituacao() != null ? producaoagenda.getProducaoagendasituacao().getNome() : "";
			
			contratoReportBean = new ContratoReportBean(producaoagenda.getContrato());
			pedidovendaReportBean = new PedidovendaReportBean(producaoagenda.getPedidovenda());
			vendaReportBean = new VendaReportBean(producaoagenda.getVenda());
			
			if(producaoagenda.getListaProducaoagendamaterial() != null && !producaoagenda.getListaProducaoagendamaterial().isEmpty()){
				listaProducaoagendamaterialReportBean = new LinkedList<ProducaoagendamaterialReportBean>();
				for(Producaoagendamaterial producaoagendamaterial : producaoagenda.getListaProducaoagendamaterial()){
					listaProducaoagendamaterialReportBean.add(new ProducaoagendamaterialReportBean(producaoagendamaterial));
				}
			}
			if(producaoagenda.getListaProducaoagendamaterialitem() != null && !producaoagenda.getListaProducaoagendamaterialitem().isEmpty()){
				listaProducaoagendamaterialitemReportBean = new LinkedList<ProducaoagendamaterialitemReportBean>();
				for(Producaoagendamaterialitem producaoagendamaterialitem : producaoagenda.getListaProducaoagendamaterialitem()){
					listaProducaoagendamaterialitemReportBean.add(new ProducaoagendamaterialitemReportBean(producaoagendamaterialitem));
				}
			}
		}
	}
	
	public Integer getCdproducaoagenda() {
		return cdproducaoagenda;
	}
	public String getProducaoagendatipo_nome() {
		return producaoagendatipo_nome;
	}
	public String getCliente_nome() {
		return cliente_nome;
	}
	public ContratoReportBean getContratoReportBean() {
		return contratoReportBean;
	}
	public PedidovendaReportBean getPedidovendaReportBean() {
		return pedidovendaReportBean;
	}
	public VendaReportBean getVendaReportBean() {
		return vendaReportBean;
	}
	public String getDtentrega() {
		return dtentrega;
	}
	public String getProducaoagendasituacao() {
		return producaoagendasituacao;
	}
	public LinkedList<ProducaoagendamaterialitemReportBean> getListaProducaoagendamaterialitemReportBean() {
		return listaProducaoagendamaterialitemReportBean;
	}
	public LinkedList<ProducaoagendamaterialReportBean> getListaProducaoagendamaterialReportBean() {
		return listaProducaoagendamaterialReportBean;
	}

	public void setCdproducaoagenda(Integer cdproducaoagenda) {
		this.cdproducaoagenda = cdproducaoagenda;
	}
	public void setProducaoagendatipo_nome(String producaoagendatipoNome) {
		producaoagendatipo_nome = producaoagendatipoNome;
	}
	public void setCliente_nome(String clienteNome) {
		cliente_nome = clienteNome;
	}
	public void setContratoReportBean(ContratoReportBean contratoReportBean) {
		this.contratoReportBean = contratoReportBean;
	}
	public void setPedidovendaReportBean(PedidovendaReportBean pedidovendaReportBean) {
		this.pedidovendaReportBean = pedidovendaReportBean;
	}
	public void setVendaReportBean(VendaReportBean vendaReportBean) {
		this.vendaReportBean = vendaReportBean;
	}
	public void setDtentrega(String dtentrega) {
		this.dtentrega = dtentrega;
	}
	public void setProducaoagendasituacao(String producaoagendasituacao) {
		this.producaoagendasituacao = producaoagendasituacao;
	}
	public void setListaProducaoagendamaterialitemReportBean(
			LinkedList<ProducaoagendamaterialitemReportBean> listaProducaoagendamaterialitemReportBean) {
		this.listaProducaoagendamaterialitemReportBean = listaProducaoagendamaterialitemReportBean;
	}
	public void setListaProducaoagendamaterialReportBean(
			LinkedList<ProducaoagendamaterialReportBean> listaProducaoagendamaterialReportBean) {
		this.listaProducaoagendamaterialReportBean = listaProducaoagendamaterialReportBean;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ProducaoagendaReportBean other = (ProducaoagendaReportBean) obj;
		if (cdproducaoagenda == null) {
			if (other.cdproducaoagenda != null)
				return false;
		} else if (!cdproducaoagenda.equals(other.cdproducaoagenda))
			return false;
		return true;
	}	
}
