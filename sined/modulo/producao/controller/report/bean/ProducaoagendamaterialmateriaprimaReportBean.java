package br.com.linkcom.sined.modulo.producao.controller.report.bean;

import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterialmateriaprima;

public class ProducaoagendamaterialmateriaprimaReportBean {
	
	protected Integer cdproducaoagendamaterialmateriaprima;
	protected Double qtdeprevista;
	protected Integer agitacao;
	protected Integer quantidadepercentual;
	protected String unidade_medida;
	protected MaterialReportBean material;
	protected LoteestoqueReportBean loteestoque;
	
	public ProducaoagendamaterialmateriaprimaReportBean(){}
	
	public ProducaoagendamaterialmateriaprimaReportBean(Producaoagendamaterialmateriaprima producaoagendamaterialmateriaprima){
		if(producaoagendamaterialmateriaprima != null){
			cdproducaoagendamaterialmateriaprima = producaoagendamaterialmateriaprima.getCdproducaoagendamaterialmateriaprima();
			qtdeprevista = producaoagendamaterialmateriaprima.getQtdeprevista();
			agitacao = producaoagendamaterialmateriaprima.getAgitacao();
			quantidadepercentual = producaoagendamaterialmateriaprima.getQuantidadepercentual();
			material = new MaterialReportBean(producaoagendamaterialmateriaprima.getMaterial());
			loteestoque = new LoteestoqueReportBean(producaoagendamaterialmateriaprima.getLoteestoque());
			
			if (producaoagendamaterialmateriaprima.getUnidademedida() != null && producaoagendamaterialmateriaprima.getUnidademedida().getNome() != null) {
				unidade_medida = producaoagendamaterialmateriaprima.getUnidademedida().getNome();
			}
		}
	}
	
	public Integer getCdproducaoagendamaterialmateriaprima() {
		return cdproducaoagendamaterialmateriaprima;
	}
	public void setCdproducaoagendamaterialmateriaprima(
			Integer cdproducaoagendamaterialmateriaprima) {
		this.cdproducaoagendamaterialmateriaprima = cdproducaoagendamaterialmateriaprima;
	}
	public Double getQtdeprevista() {
		return qtdeprevista;
	}
	public void setQtdeprevista(Double qtdeprevista) {
		this.qtdeprevista = qtdeprevista;
	}
	public Integer getAgitacao() {
		return agitacao;
	}
	public void setAgitacao(Integer agitacao) {
		this.agitacao = agitacao;
	}
	public Integer getQuantidadepercentual() {
		return quantidadepercentual;
	}
	public void setQuantidadepercentual(Integer quantidadepercentual) {
		this.quantidadepercentual = quantidadepercentual;
	}
	public String getUnidade_medida() {
		return unidade_medida;
	}
	public void setUnidade_medida(String unidadeMedida) {
		unidade_medida = unidadeMedida;
	}
		
	public MaterialReportBean getMaterial() {
		return material;
	}

	public void setMaterial(MaterialReportBean material) {
		this.material = material;
	}

	public LoteestoqueReportBean getLoteestoque() {
		return loteestoque;
	}

	public void setLoteestoque(LoteestoqueReportBean loteestoque) {
		this.loteestoque = loteestoque;
	}



	public class MaterialReportBean {
		
		private String nome;
		
		public MaterialReportBean(Material material) {
			this.nome = material.getNome();
		}

		public String getNome() {
			return nome;
		}
		
		public void setNome(String nome) {
			this.nome = nome;
		}
	}
	
	public class LoteestoqueReportBean{
		private String numero;
		
		public LoteestoqueReportBean(Loteestoque loteestoque) {
			if(loteestoque != null){
				this.numero = loteestoque.getNumero();
			}
		}

		public String getNumero() {
			return numero;
		}
		
		public void setNumero(String numero) {
			this.numero = numero;
		}
	}
}
