package br.com.linkcom.sined.modulo.producao.controller.report.bean;

public class EtiquetaProducaoordemBean {
	protected String departamento;
	protected String produto;
	protected String total;
	
	
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getProduto() {
		return produto;
	}
	public void setProduto(String produto) {
		this.produto = produto;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	
}
