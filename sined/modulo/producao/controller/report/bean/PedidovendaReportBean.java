package br.com.linkcom.sined.modulo.producao.controller.report.bean;

import java.util.LinkedList;

import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;

public class PedidovendaReportBean {
	
	protected Integer cdpedidovenda;
	protected String identificacaoexterna;
	protected String dtpedidovenda;
	
	protected LinkedList<PedidovendamaterialReportBean> listaPedidovendamaterial;

	public PedidovendaReportBean(){}
	
	public PedidovendaReportBean(Pedidovenda pedidovenda) {
		if(pedidovenda != null){
			cdpedidovenda = pedidovenda.getCdpedidovenda();
			identificacaoexterna = pedidovenda.getIdentificacaoexterna();
			dtpedidovenda =	pedidovenda.getDtpedidovendastr();
			
			if(pedidovenda.getListaPedidovendamaterial() != null && !pedidovenda.getListaPedidovendamaterial().isEmpty()){
				listaPedidovendamaterial = new LinkedList<PedidovendamaterialReportBean>();
				for(Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()){
					listaPedidovendamaterial.add(new PedidovendamaterialReportBean(pedidovendamaterial));
				}
			}
		}
	}
	
	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}
	public String getDtpedidovenda() {
		return dtpedidovenda;
	}
	public LinkedList<PedidovendamaterialReportBean> getListaPedidovendamaterial() {
		return listaPedidovendamaterial;
	}
	public String getIdentificacaoexterna() {
		return identificacaoexterna;
	}
	public void setIdentificacaoexterna(String identificacaoexterna) {
		this.identificacaoexterna = identificacaoexterna;
	}
	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
	public void setDtpedidovenda(String dtpedidovenda) {
		this.dtpedidovenda = dtpedidovenda;
	}
	public void setListaPedidovendamaterial(
			LinkedList<PedidovendamaterialReportBean> listaPedidovendamaterial) {
		this.listaPedidovendamaterial = listaPedidovendamaterial;
	}
}
