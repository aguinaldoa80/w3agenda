package br.com.linkcom.sined.modulo.producao.controller.report.bean;

import java.util.LinkedList;

import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialorigem;


public class ProducaoordemmaterialReportBean {
	
	protected Integer cdproducaoordemmaterial; 
	protected MaterialReportBean materialReportBean;
	protected PneuReportBean pneuReportBean;
	protected Double qtde;
	protected Double qtdeproduzido;
	protected String producaoetapaitem_nome = "Etapa �nica";
	protected Integer producaoetapaitem_ordem;
	protected Integer producaoetapaitem_qtdediasprevisto;
	protected String observacao;
	
	protected LinkedList<ProducaoagendaReportBean> listaProducaoagendaReportBean;
	
	public ProducaoordemmaterialReportBean(){}
	
	public ProducaoordemmaterialReportBean(Producaoordemmaterial producaoordemmaterial){
		if(producaoordemmaterial != null){
			cdproducaoordemmaterial = producaoordemmaterial.getCdproducaoordemmaterial();
			materialReportBean = new MaterialReportBean(producaoordemmaterial.getMaterial());
			qtde = producaoordemmaterial.getQtde();
			qtdeproduzido = producaoordemmaterial.getQtdeproduzido();
			observacao = producaoordemmaterial.getObservacao();
			if(producaoordemmaterial.getProducaoetapaitem() != null){
				producaoetapaitem_nome = producaoordemmaterial.getProducaoetapaitem().getProducaoetapanome() != null ? producaoordemmaterial.getProducaoetapaitem().getProducaoetapanome().getNome() : "";
				producaoetapaitem_ordem = producaoordemmaterial.getProducaoetapaitem().getOrdem();
				producaoetapaitem_qtdediasprevisto = producaoordemmaterial.getProducaoetapaitem().getQtdediasprevisto();
			}
			if(producaoordemmaterial.getListaProducaoordemmaterialorigem() != null && !producaoordemmaterial.getListaProducaoordemmaterialorigem().isEmpty()){
				listaProducaoagendaReportBean = new LinkedList<ProducaoagendaReportBean>();
				for(Producaoordemmaterialorigem item : producaoordemmaterial.getListaProducaoordemmaterialorigem()){
					listaProducaoagendaReportBean.add(new ProducaoagendaReportBean(item.getProducaoagenda()));
				}
			}
			pneuReportBean = new PneuReportBean(producaoordemmaterial.getPneu());
		}
	}
	
	public Integer getCdproducaoordemmaterial() {
		return cdproducaoordemmaterial;
	}
	public MaterialReportBean getMaterialReportBean() {
		return materialReportBean;
	}
	public Double getQtde() {
		return qtde;
	}
	public Double getQtdeproduzido() {
		return qtdeproduzido;
	}
	public String getProducaoetapaitem_nome() {
		return producaoetapaitem_nome;
	}
	public Integer getProducaoetapaitem_ordem() {
		return producaoetapaitem_ordem;
	}
	public Integer getProducaoetapaitem_qtdediasprevisto() {
		return producaoetapaitem_qtdediasprevisto;
	}
	public LinkedList<ProducaoagendaReportBean> getListaProducaoagendaReportBean() {
		return listaProducaoagendaReportBean;
	}
	public PneuReportBean getPneuReportBean() {
		return pneuReportBean;
	}

	public void setPneuReportBean(PneuReportBean pneuReportBean) {
		this.pneuReportBean = pneuReportBean;
	}
	public void setCdproducaoordemmaterial(Integer cdproducaoordemmaterial) {
		this.cdproducaoordemmaterial = cdproducaoordemmaterial;
	}
	public void setMaterialReportBean(MaterialReportBean materialReportBean) {
		this.materialReportBean = materialReportBean;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setQtdeproduzido(Double qtdeproduzido) {
		this.qtdeproduzido = qtdeproduzido;
	}
	public void setProducaoetapaitem_nome(String producaoetapaitemNome) {
		producaoetapaitem_nome = producaoetapaitemNome;
	}
	public void setProducaoetapaitem_ordem(Integer producaoetapaitemOrdem) {
		producaoetapaitem_ordem = producaoetapaitemOrdem;
	}
	public void setProducaoetapaitem_qtdediasprevisto(
			Integer producaoetapaitemQtdediasprevisto) {
		producaoetapaitem_qtdediasprevisto = producaoetapaitemQtdediasprevisto;
	}
	public void setListaProducaoagendaReportBean(
			LinkedList<ProducaoagendaReportBean> listaProducaoagendaReportBean) {
		this.listaProducaoagendaReportBean = listaProducaoagendaReportBean;
	}

	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
}
