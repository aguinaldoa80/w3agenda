package br.com.linkcom.sined.modulo.producao.controller.report.bean;

import java.util.LinkedList;



public class EmitirProducaoordemReportBean {
	
	protected Integer cdproducaoordem; 
	protected String departamento_nome;
	protected String dtproducaoordem;
	protected String dtprevisaoentrega;
	protected String producaoordemsituacao;
	protected String historico;
	
	protected LinkedList<ProducaoordemmaterialReportBean> listaProducaoordemmaterialReportBean;

	public Integer getCdproducaoordem() {
		return cdproducaoordem;
	}
	public String getDepartamento_nome() {
		return departamento_nome;
	}
	public String getDtproducaoordem() {
		return dtproducaoordem;
	}
	public String getDtprevisaoentrega() {
		return dtprevisaoentrega;
	}
	public String getProducaoordemsituacao() {
		return producaoordemsituacao;
	}
	public String getHistorico() {
		return historico;
	}
	public LinkedList<ProducaoordemmaterialReportBean> getListaProducaoordemmaterialReportBean() {
		return listaProducaoordemmaterialReportBean;
	}
	
	public void setCdproducaoordem(Integer cdproducaoordem) {
		this.cdproducaoordem = cdproducaoordem;
	}
	public void setDepartamento_nome(String departamentoNome) {
		departamento_nome = departamentoNome;
	}
	public void setDtproducaoordem(String dtproducaoordem) {
		this.dtproducaoordem = dtproducaoordem;
	}
	public void setDtprevisaoentrega(String dtprevisaoentrega) {
		this.dtprevisaoentrega = dtprevisaoentrega;
	}
	public void setProducaoordemsituacao(String producaoordemsituacao) {
		this.producaoordemsituacao = producaoordemsituacao;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	public void setListaProducaoordemmaterialReportBean(
			LinkedList<ProducaoordemmaterialReportBean> listaProducaoordemmaterialReportBean) {
		this.listaProducaoordemmaterialReportBean = listaProducaoordemmaterialReportBean;
	}
}
