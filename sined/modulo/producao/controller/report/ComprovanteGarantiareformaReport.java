package br.com.linkcom.sined.modulo.producao.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.GarantiareformaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.GarantiareformaFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
		path = "/producao/relatorio/ComprovanteGarantiareforma",
		authorizationModule=ReportAuthorizationModule.class
)
public class ComprovanteGarantiareformaReport extends SinedReport<GarantiareformaFiltro>{

	private GarantiareformaService garantiareformaService;
	
	public void setGarantiareformaService(
			GarantiareformaService garantiareformaService) {
		this.garantiareformaService = garantiareformaService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,
			GarantiareformaFiltro filtro) throws Exception {
		String whereIn = request.getParameter("selectedItens");
		return garantiareformaService.createComprovanteGarantiareforma(whereIn);
	}

	@Override
	public String getNomeArquivo() {
		return "comprovanteGarantiareforma";
	}

	@Override
	public String getTitulo(GarantiareformaFiltro filtro) {
		return "GARANTIA DE REFORMA";
	}

}
