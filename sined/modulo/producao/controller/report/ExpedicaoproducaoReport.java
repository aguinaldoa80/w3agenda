package br.com.linkcom.sined.modulo.producao.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ExpedicaoproducaoService;
import br.com.linkcom.sined.modulo.producao.controller.crud.filter.ExpedicaoproducaoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/producao/relatorio/Expedicaoproducao",	authorizationModule=ReportAuthorizationModule.class)
public class ExpedicaoproducaoReport extends SinedReport<ExpedicaoproducaoFiltro> {
	
	private ExpedicaoproducaoService expedicaoproducaoService;
	
	public void setExpedicaoService(ExpedicaoproducaoService expedicaoproducaoService) {
		this.expedicaoproducaoService = expedicaoproducaoService;
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, ExpedicaoproducaoFiltro filtro) throws ResourceGenerationException {
		return new ModelAndView("redirect:/producao/crud/Expedicaoproducao");
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, ExpedicaoproducaoFiltro filtro) throws Exception {
		return expedicaoproducaoService.gerarRelatorioListagem(filtro);
	}
	
	@Override
	public String getTitulo(ExpedicaoproducaoFiltro filtro) {
		return "EXPEDIÇÕES";
	}
	
	@Override
	public String getNomeArquivo() {
		return "expedicao";
	}
}
