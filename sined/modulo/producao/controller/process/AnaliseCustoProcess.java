package br.com.linkcom.sined.modulo.producao.controller.process;

import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.DownloadFileServlet;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Tabelavalor;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialproducaoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.ProducaoagendaService;
import br.com.linkcom.sined.geral.service.TabelavalorService;
import br.com.linkcom.sined.geral.service.VendamaterialService;
import br.com.linkcom.sined.modulo.producao.controller.process.bean.AnaliseCustoBean;
import br.com.linkcom.sined.modulo.producao.controller.process.bean.CustoTotalBean;
import br.com.linkcom.sined.modulo.producao.controller.process.filter.AnaliseCustoFiltro;
import br.com.linkcom.sined.util.Grafico;
import br.com.linkcom.sined.util.annotation.GraficoLabel;
import br.com.linkcom.sined.util.bean.GenericBean;

@Controller(path="/producao/process/AnaliseCusto", authorizationModule=ProcessAuthorizationModule.class)
public class AnaliseCustoProcess extends MultiActionController{

	DecimalFormat df = new DecimalFormat("#,##0.00");
	protected ProducaoagendaService producaoagendaService;
	protected MaterialService materialService;
	protected ParametrogeralService parametrogeralService;
	protected MaterialproducaoService materialproducaoService;
	protected VendamaterialService vendamaterialService;
	private ArquivoService arquivoService;
	protected TabelavalorService tabelavalorService;
	
	public void setProducaoagendaService(ProducaoagendaService producaoagendaService) {this.producaoagendaService = producaoagendaService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setMaterialproducaoService(MaterialproducaoService materialproducaoService) {this.materialproducaoService = materialproducaoService;}
	public void setVendamaterialService(VendamaterialService vendamaterialService) {this.vendamaterialService = vendamaterialService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setTabelavalorService(TabelavalorService tabelavalorService) {this.tabelavalorService = tabelavalorService;}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, AnaliseCustoFiltro filtro){
		return new ModelAndView("process/analiseCusto", "filtro", filtro);
	}
	
	public ModelAndView listar(WebRequestContext request, AnaliseCustoFiltro filtro) throws Exception{
		List<AnaliseCustoBean> listaAnaliseCustoAux = this.getLista(request, filtro);
		
		if(filtro.getTipoAnalise().name().equals("SINTETICO")){
			if(!listaAnaliseCustoAux.isEmpty()){
				CustoTotalBean custoTotal = this.analiseSintetica(listaAnaliseCustoAux);			
				List<GenericBean> listaPercentual = new ArrayList<GenericBean>();			
				List<Method> listaMetodos = Arrays.asList(custoTotal.getClass().getDeclaredMethods());
				for(Method metodo : listaMetodos){
					GraficoLabel annotation = metodo.getAnnotation(GraficoLabel.class);
					if(annotation != null){
						listaPercentual.add(new GenericBean(annotation.value(), metodo.invoke(custoTotal)));
					}
				}
				Arquivo arquivo = Grafico.gerarPieChartAnaliseCustoSintetica(listaPercentual);
				if (arquivo != null){
					arquivoService.save(arquivo, null);
					arquivoService.loadAsImage(arquivo);
					try {
						if(arquivo.getCdarquivo() != null){
							DownloadFileServlet.addCdfile(request.getSession(), arquivo.getCdarquivo());
							request.setAttribute("exibefoto", true);
							request.setAttribute("cdArquivo", arquivo.getCdarquivo());
						}
						else{
							request.setAttribute("cdArquivo", null);
						}
					} catch (Exception e) {
						request.setAttribute("exibefoto", false);
					}
				}
				return new ModelAndView("direct:ajax/analiseCustoSinteticoListagem", "analiseCustoSintetica", custoTotal);
			}
			else{
				return new ModelAndView("direct:ajax/analiseCustoSinteticoListagem", "analiseCustoSintetica", listaAnaliseCustoAux);
			}			
		}		
		else{
			return new ModelAndView("direct:ajax/analiseCustoListagem", "listaAnaliseCustoBean", listaAnaliseCustoAux);
		}
	}
	
	public List<AnaliseCustoBean> getLista(WebRequestContext request, AnaliseCustoFiltro filtro){
		List<Tabelavalor> listaTabelavalor = tabelavalorService.findByIdentificador(Tabelavalor.CUSTO_OPERACIONAL_ADMINISTRATIVO + "," +
																					Tabelavalor.CUSTO_COMERCIAL_PRODUCAO); 
		
		Double custoOperacional = tabelavalorService.getValor(Tabelavalor.CUSTO_OPERACIONAL_ADMINISTRATIVO, listaTabelavalor);
		Double custoComercial = tabelavalorService.getValor(Tabelavalor.CUSTO_COMERCIAL_PRODUCAO, listaTabelavalor);;
		
		List<Producaoagenda> listaProducaoagenda = producaoagendaService.findForAnaliseCusto(filtro.getEmpresa(), filtro.getDtinicio(), filtro.getDtfim());
		List<AnaliseCustoBean> listaAnaliseCustoBean = new ArrayList<AnaliseCustoBean>();
		
		for(Producaoagenda pa : listaProducaoagenda){
			for(Producaoagendamaterial pam : pa.getListaProducaoagendamaterial()){
				AnaliseCustoBean analiseCustoBean = new AnaliseCustoBean();
				analiseCustoBean.setMaterial(pam.getMaterial());
				analiseCustoBean.setQtdeVendido(pam.getQtde());
				if(pam.getMaterial() != null && pam.getMaterial().getMaterialgrupo() != null && pam.getMaterial().getMaterialgrupo().getRateiocustoproducao() != null){
					Double rateio = pam.getMaterial().getMaterialgrupo().getRateiocustoproducao();
					Double custo = custoOperacional * (rateio != null ? rateio : 1.0);
					analiseCustoBean.setCustoOperacional(custo);
				} else {
					analiseCustoBean.setCustoOperacional(custoOperacional);
				}
				analiseCustoBean.setCustoComercial(custoComercial);
				
				String nomeMaterialBanda = "";
				if(pa.getVenda() != null){
					for(Vendamaterial vendamaterial : pa.getVenda().getListavendamaterial()){
					if(vendamaterial.getPneu() != null && vendamaterial.getPneu().getMaterialbanda() != null && vendamaterial.getMaterial().equals(pam.getMaterial())){
							nomeMaterialBanda = vendamaterial.getPneu().getMaterialbanda().getNome();
						}
					}
				}
				
				if(pa.getPedidovenda() != null){
					for(Pedidovendamaterial pedidovendamaterial : pa.getPedidovenda().getListaPedidovendamaterial()){
						if(pedidovendamaterial.getPneu() != null && pedidovendamaterial.getPneu().getMaterialbanda() != null && pedidovendamaterial.getMaterial().equals(pam.getMaterial())){
							nomeMaterialBanda = pedidovendamaterial.getPneu().getMaterialbanda().getNome();
						}
					}
				}
				
				if(StringUtils.isNotBlank(nomeMaterialBanda)){
					analiseCustoBean.setNomeMaterial(pam.getMaterial().getNome() + " - " + nomeMaterialBanda);
				} else {
					analiseCustoBean.setNomeMaterial(pam.getMaterial().getNome());
				}
				
				listaAnaliseCustoBean.add(analiseCustoBean);
			}
		}
		
		//agrupando
		Map<Integer, AnaliseCustoBean> mapa = new HashMap<Integer, AnaliseCustoBean>();
		for (AnaliseCustoBean analiseCustoBean : listaAnaliseCustoBean) {
			Integer cdmaterial = analiseCustoBean.getMaterial().getCdmaterial();
			if(mapa.containsKey(cdmaterial)){
				AnaliseCustoBean beanExistente = mapa.get(cdmaterial);
				beanExistente.setQtdeVendido(beanExistente.getQtdeVendido() + analiseCustoBean.getQtdeVendido());
				if(beanExistente.getNomeMaterial().length() < analiseCustoBean.getNomeMaterial().length()){
					beanExistente.setNomeMaterial(analiseCustoBean.getNomeMaterial());
				}
			}else{
				mapa.put(cdmaterial, analiseCustoBean);
			}
		}
		
		List<AnaliseCustoBean> listaAnaliseCustoAux = new ArrayList<AnaliseCustoBean>();
		
		if(filtro.getDtinicio() != null){
			for(AnaliseCustoBean item : mapa.values()){
				Double preco = 0.0;
				Double qtde = 0.0;
				Double valorUnitario = 0.0;
				List<Vendamaterial> listaVendamaterial = vendamaterialService.findByMaterialAndDataForAnaliseCusto(item.getMaterial(), filtro.getDtinicio(), filtro.getDtfim());
				for(Vendamaterial vendamaterial : listaVendamaterial){
					if(vendamaterial.getPreco() != null && vendamaterial.getQuantidade() != null){
						preco += vendamaterial.getPreco();
						qtde += vendamaterial.getQuantidade();
					}
				}
				if(qtde != null && qtde != 0.0){
					valorUnitario = preco / qtde;
				}
				item.setValorUnitarioVenda(df.format(valorUnitario));
				listaAnaliseCustoAux.add(item);
			}
		} else {
			listaAnaliseCustoAux.addAll(mapa.values());
		}
		return listaAnaliseCustoAux;		
	}
	
	public ModelAndView gerarCsv(WebRequestContext request, AnaliseCustoFiltro filtro){
		return new ResourceModelAndView(producaoagendaService.makeCSVListagemAnaliseCusto(this.getLista(request, filtro)));
	}
	
	public ModelAndView exibirCusto(WebRequestContext request, Material material){
		List<Materialproducao> listaProducao = materialproducaoService.findListaProducao(material.getCdmaterial());
		material.getListaProducao().addAll(listaProducao);
		return new ModelAndView("direct:/process/popup/popupAnaliseCusto").addObject("material", material);
	}
	
	
	/**
	 * @param listaAnaliseCusto
	 * @return
	 * @author Andrey Leonardo
	 * @since 23/06/2015
	 */
	public CustoTotalBean analiseSintetica(List<AnaliseCustoBean> listaAnaliseCusto){
		CustoTotalBean analiseSintetica;
		Double totalVendido = 0.0;
		Double custoMP = 0.0;
		Double custoOP = 0.0;
		Double custoComercial = 0.0;
		Double custoTotal = 0.0;
		Double markup = 0.0;
		for(AnaliseCustoBean analise : listaAnaliseCusto){
			totalVendido += analise.getTotalVendidoDouble();
			custoMP += analise.getCustoMateriaPrimaDouble();
			custoOP += analise.getCustoOperacional();
			custoComercial += analise.getCustoComercial();
			custoTotal += analise.getCustoTotalDouble();			
		}
		markup = custoTotal/totalVendido;
		analiseSintetica = new CustoTotalBean(totalVendido, custoMP, custoOP, custoComercial, custoTotal, markup);
		return analiseSintetica;
	}
	
	
}
