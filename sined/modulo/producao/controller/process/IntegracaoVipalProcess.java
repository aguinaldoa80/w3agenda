package br.com.linkcom.sined.modulo.producao.controller.process;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.service.ProducaoagendamaterialService;
import br.com.linkcom.sined.modulo.producao.controller.process.filter.IntegracaoVipalFiltro;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/producao/process/IntegracaoVipal", authorizationModule=ProcessAuthorizationModule.class)
public class IntegracaoVipalProcess extends MultiActionController {
	
	private ProducaoagendamaterialService producaoagendamaterialService;
	
	public void setProducaoagendamaterialService(
			ProducaoagendamaterialService producaoagendamaterialService) {
		this.producaoagendamaterialService = producaoagendamaterialService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, IntegracaoVipalFiltro filtro){
		return new ModelAndView("process/integracaoVipal", "filtro", filtro);
	}
	
	public ModelAndView gerar(WebRequestContext request, IntegracaoVipalFiltro filtro){
		try{
			List<Producaoagendamaterial> listaProducaoagendamaterial = producaoagendamaterialService.findForIntegracaoVipal(filtro);
			
			
			List<Arquivo> listaXML = new ArrayList<Arquivo>();
			if(SinedUtil.isListNotEmpty(listaProducaoagendamaterial)){
				StringBuilder sbCliente = new StringBuilder();
				StringBuilder sbPlaninha = new StringBuilder();
				for (Producaoagendamaterial producaoagendamaterial : listaProducaoagendamaterial) {
	//				listaXML.add(producaoagendamaterialService.gerarArquivoVipalCliente(producaoagendamaterial));
	//				listaXML.add(producaoagendamaterialService.gerarArquivoVipalPlanilha(producaoagendamaterial));
					sbCliente.append(producaoagendamaterialService.gerarStringVipalCliente(producaoagendamaterial)).append("\n");
					sbPlaninha.append(producaoagendamaterialService.gerarStringVipalPlanilha(producaoagendamaterial)).append("\n");
				}
				
				listaXML.add(producaoagendamaterialService.gerarArquivoVipalCliente(sbCliente.substring(0, sbCliente.length()-1)));
				listaXML.add(producaoagendamaterialService.gerarArquivoVipalPlanilha(sbPlaninha.substring(0, sbPlaninha.length()-1)));
			}
			
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			if(listaXML != null && !listaXML.isEmpty()){				
				ZipOutputStream zip = new ZipOutputStream(byteArrayOutputStream);
				
				for (Arquivo arquivo : listaXML) {
					if(arquivo != null){
						ZipEntry zipEntry = new ZipEntry(arquivo.getName());
						zipEntry.setSize(arquivo.getSize());
						zip.putNextEntry(zipEntry);
						zip.write(arquivo.getContent());
					}
				}
				
				zip.closeEntry();
				zip.close();
			} else {
				request.addError("Nenhum arquivo gerado a partir do filtro selecionado.");
				return sendRedirectToAction("index");
			}
			
			return new ResourceModelAndView(new Resource("application/zip", "vipal_" + SinedUtil.datePatternForReport() + ".zip", byteArrayOutputStream.toByteArray()));
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na gera��o de arquivo da Vipal: " + e.getMessage());
			return sendRedirectToAction("index");
		}
	}

}
