package br.com.linkcom.sined.modulo.producao.controller.process.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class IntegracaoVipalFiltro extends FiltroListagemSined {

	protected Empresa empresa;
	protected Date dtinicio;
	protected Date dtfim;

	@Required
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	@Required
	@DisplayName("Data in�cio")
	public Date getDtinicio() {
		return dtinicio;
	}

	@Required
	@DisplayName("Data fim")
	public Date getDtfim() {
		return dtfim;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

}
