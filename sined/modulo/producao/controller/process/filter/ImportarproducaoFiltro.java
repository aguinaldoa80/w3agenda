package br.com.linkcom.sined.modulo.producao.controller.process.filter;

import java.sql.Timestamp;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Producaoetapa;
import br.com.linkcom.sined.geral.bean.Projeto;

public class ImportarproducaoFiltro {
	
	protected Cliente cliente;
	protected Contrato contrato;
	protected Contagerencial contagerencial;
	protected Projeto projeto;
	protected Producaoetapa producaoetapa;
	protected Materialgrupo materialgrupo;
	protected Timestamp dtentrega;
	protected Arquivo arquivo;
	
	@Required
	public Cliente getCliente() {
		return cliente;
	}
	@Required
	public Contrato getContrato() {
		return contrato;
	}
	@Required
	@DisplayName("Conta Gerencial de Compra")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	@DisplayName("Etapas de Produ��o")
	@Required
	public Producaoetapa getProducaoetapa() {
		return producaoetapa;
	}
	@DisplayName("Data de Entrega")
	@Required
	public Timestamp getDtentrega() {
		return dtentrega;
	}
	@DisplayName("Arquivo")
	@Required
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public void setProducaoetapa(Producaoetapa producaoetapa) {
		this.producaoetapa = producaoetapa;
	}
	public void setDtentrega(Timestamp dtentrega) {
		this.dtentrega = dtentrega;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	
}
