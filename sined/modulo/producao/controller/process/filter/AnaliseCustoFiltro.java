package br.com.linkcom.sined.modulo.producao.controller.process.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.modulo.producao.controller.process.bean.enumeration.AnaliseCustoTipoEnum;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class AnaliseCustoFiltro extends FiltroListagemSined{

	protected Empresa empresa;
	protected Date dtinicio;
	protected Date dtfim;
	protected AnaliseCustoTipoEnum tipoAnalise;
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Data in�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data fim")
	public Date getDtfim() {
		return dtfim;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	@DisplayName("Tipo")
	public AnaliseCustoTipoEnum getTipoAnalise() {
		return tipoAnalise;
	}
	public void setTipoAnalise(AnaliseCustoTipoEnum tipoAnalise) {
		this.tipoAnalise = tipoAnalise;
	}
	
}
