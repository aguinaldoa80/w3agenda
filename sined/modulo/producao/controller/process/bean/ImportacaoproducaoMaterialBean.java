package br.com.linkcom.sined.modulo.producao.controller.process.bean;

import br.com.linkcom.sined.geral.bean.Cemcomponente;
import br.com.linkcom.sined.geral.bean.Cemcomponenteacumulado;
import br.com.linkcom.sined.geral.bean.Cempainel;
import br.com.linkcom.sined.geral.bean.Cemperfil;
import br.com.linkcom.sined.geral.bean.Cemperfilacumulado;
import br.com.linkcom.sined.geral.bean.Cemvidro;
import br.com.linkcom.sined.geral.bean.Material;

public class ImportacaoproducaoMaterialBean {
	
	private Material material;
	private Cemcomponenteacumulado cemcomponenteacumulado;
	private Cemperfilacumulado cemperfilacumulado;
	private Cemcomponente cemcomponente;
	private Cemperfil cemperfil;
	private Cemvidro cemvidro;
	private Cempainel cempainel;
	
	public ImportacaoproducaoMaterialBean(){}
	
	public ImportacaoproducaoMaterialBean(Cemcomponenteacumulado cemcomponenteacumulado) {
		this.cemcomponenteacumulado = cemcomponenteacumulado;
	}
	public ImportacaoproducaoMaterialBean(Cemperfilacumulado cemperfilacumulado) {
		this.cemperfilacumulado = cemperfilacumulado;
	}
	public ImportacaoproducaoMaterialBean(Cemcomponente cemcomponente) {
		this.cemcomponente = cemcomponente;
	}
	public ImportacaoproducaoMaterialBean(Cemperfil cemperfil) {
		this.cemperfil = cemperfil;
	}
	public ImportacaoproducaoMaterialBean(Cemvidro cemvidro) {
		this.cemvidro = cemvidro;
	}
	public ImportacaoproducaoMaterialBean(Cempainel cempainel) {
		this.cempainel = cempainel;
	}
	
	public ImportacaoproducaoMaterial getImportacaoproducaoMaterial(){
		if(this.cemcomponenteacumulado != null) return this.cemcomponenteacumulado;
		if(this.cemperfilacumulado != null) return this.cemperfilacumulado;
		if(this.cemcomponente != null) return this.cemcomponente;
		if(this.cemperfil != null) return this.cemperfil;
		if(this.cemvidro != null) return this.cemvidro;
		if(this.cempainel != null) return this.cempainel;
		return null;
	}
	
	public Material getMaterial() {
		return material;
	}
	public Cemcomponenteacumulado getCemcomponenteacumulado() {
		return cemcomponenteacumulado;
	}
	public Cemperfilacumulado getCemperfilacumulado() {
		return cemperfilacumulado;
	}
	public Cemcomponente getCemcomponente() {
		return cemcomponente;
	}
	public Cemperfil getCemperfil() {
		return cemperfil;
	}
	public Cemvidro getCemvidro() {
		return cemvidro;
	}
	public Cempainel getCempainel() {
		return cempainel;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setCemcomponenteacumulado(
			Cemcomponenteacumulado cemcomponenteacumulado) {
		this.cemcomponenteacumulado = cemcomponenteacumulado;
	}
	public void setCemperfilacumulado(Cemperfilacumulado cemperfilacumulado) {
		this.cemperfilacumulado = cemperfilacumulado;
	}
	public void setCemcomponente(Cemcomponente cemcomponente) {
		this.cemcomponente = cemcomponente;
	}
	public void setCemperfil(Cemperfil cemperfil) {
		this.cemperfil = cemperfil;
	}
	public void setCemvidro(Cemvidro cemvidro) {
		this.cemvidro = cemvidro;
	}
	public void setCempainel(Cempainel cempainel) {
		this.cempainel = cempainel;
	}
	
}
