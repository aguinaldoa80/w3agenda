package br.com.linkcom.sined.modulo.producao.controller.process.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum AnaliseCustoTipoEnum {
	
	ANALITICO (0, "Analítico"),
	SINTETICO (1, "Sintético");
	
	private Integer value;
	private String nome;
	
	private AnaliseCustoTipoEnum(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
