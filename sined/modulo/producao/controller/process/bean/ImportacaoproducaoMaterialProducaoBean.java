package br.com.linkcom.sined.modulo.producao.controller.process.bean;

import br.com.linkcom.sined.geral.bean.Cemtipologia;
import br.com.linkcom.sined.geral.bean.Material;

public class ImportacaoproducaoMaterialProducaoBean {
	
	private Material material;
	private Cemtipologia cemtipologia;
	
	public ImportacaoproducaoMaterialProducaoBean(Cemtipologia cemtipologia) {
		this.cemtipologia = cemtipologia;
	}
	
	public ImportacaoproducaoMaterial getImportacaoproducaoMaterial(){
		if(this.cemtipologia != null) return this.cemtipologia;
		return null;
	}
	
	public Material getMaterial() {
		return material;
	}
	public Cemtipologia getCemtipologia() {
		return cemtipologia;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setCemtipologia(Cemtipologia cemtipologia) {
		this.cemtipologia = cemtipologia;
	}
	
}
