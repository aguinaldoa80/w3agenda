package br.com.linkcom.sined.modulo.producao.controller.process.bean;

import java.text.DecimalFormat;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.GraficoLabel;

/**
 * @author Andrey Leonardo
 * @since 23/06/2015
 * Para adicionar novos campos no pie chart
 * utilize a annotation GraficoLabel.
 */
public class CustoTotalBean {
	String totalVendido;
	String custoMP;
	String custoOP;
	String custoCO;
	String custoTotal;
	String markup;
	Double percentualMPVendido = 0.0;
	Double percentualOPVendido = 0.0;
	Double percentualCOVendido = 0.0;
	Double percentualCustoTotalVendido = 0.0;
	DecimalFormat df = new DecimalFormat("#,##0.00");
	
	public CustoTotalBean(){}
	
	public CustoTotalBean(Double totalVendido, Double custoMP, Double custoOP, Double custoCO, Double custoTotal, Double markup){
		this.totalVendido = df.format(totalVendido);
		this.custoMP = df.format(custoMP);
		this.custoOP = df.format(custoOP);
		this.custoCO = df.format(custoCO);
		this.custoTotal = df.format(custoTotal);
		this.markup = df.format(markup);
		if(totalVendido != 0.0){
			this.percentualMPVendido = SinedUtil.round(((custoMP/totalVendido)*100), 2);
			this.percentualCOVendido = SinedUtil.round(((custoCO/totalVendido)*100), 2);
			this.percentualOPVendido = SinedUtil.round(((custoOP/totalVendido)*100), 2);
			this.percentualCustoTotalVendido = SinedUtil.round(((custoTotal/totalVendido)*100), 2);
		}
	}
	@DisplayName("Total Vendido")
	public String getTotalVendido() {
		return totalVendido;
	}

	public void setTotalVendido(String totalVendido) {
		this.totalVendido = totalVendido;
	}
	@DisplayName("Custo Mat�ria Prima")
	public String getCustoMP() {
		return custoMP;
	}

	public void setCustoMP(String custoMP) {
		this.custoMP = custoMP;
	}
	@DisplayName("Custo Operacional")
	public String getCustoOP() {
		return custoOP;
	}

	public void setCustoOP(String custoOP) {
		this.custoOP = custoOP;
	}
	@DisplayName("Custo Comercial")
	public String getCustoCO() {
		return custoCO;
	}

	public void setCustoCO(String custoCO) {
		this.custoCO = custoCO;
	}
	@DisplayName("Custo Total")
	public String getCustoTotal() {
		return custoTotal;
	}

	public void setCustoTotal(String custoTotal) {
		this.custoTotal = custoTotal;
	}
	@DisplayName("Markup")
	public String getMarkup() {
		return markup;
	}

	public void setMarkup(String markup) {
		this.markup = markup;
	}
	@GraficoLabel("Custo Mat�ria Prima")
	@DisplayName("Percentual sobre vendido")
	public Double getPercentualMPVendido() {
		return percentualMPVendido;
	}

	public void setPercentualMPVendido(Double percentualMPVendido) {
		this.percentualMPVendido = percentualMPVendido;
	}
	@GraficoLabel("Custo Operacional")
	@DisplayName("Percentual sobre vendido")
	public Double getPercentualOPVendido() {
		return percentualOPVendido;
	}

	public void setPercentualOPVendido(Double percentualOPVendido) {
		this.percentualOPVendido = percentualOPVendido;
	}
	@GraficoLabel("Custo Comercial")
	@DisplayName("Percentual sobre vendido")
	public Double getPercentualCOVendido() {
		return percentualCOVendido;
	}
	public void setPercentualCOVendido(Double percentualCOVendido) {
		this.percentualCOVendido = percentualCOVendido;
	}
	@DisplayName("Percentual sobre vendido")
	public Double getPercentualCustoTotalVendido() {
		return percentualCustoTotalVendido;
	}

	public void setPercentualCustoTotalVendido(Double percentualCustoTotalVendido) {
		this.percentualCustoTotalVendido = percentualCustoTotalVendido;
	}
}
