package br.com.linkcom.sined.modulo.producao.controller.process.bean;

import br.com.linkcom.sined.geral.bean.enumeration.TipoImportacaoProducao;

public interface ImportacaoproducaoMaterial {

	public TipoImportacaoProducao getTipoImportacao();
	public String getCodigo();
	public String getNomeMaterial();
	public String getUnidademedidaMaterial();
	public String getGrupoMaterial();
	public String getCorMaterial();
	public String getTipoMaterial();
	public Double getCustoMaterial();
	public Double getAlturaMaterial();
	public Double getLarguraMaterial();
	public Double getComprimentoMaterial();
	public Double getPesoMaterial();
	public Double getQuantidade();
	public Double getVolume();
	
}
