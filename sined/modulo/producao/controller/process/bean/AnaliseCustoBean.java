package br.com.linkcom.sined.modulo.producao.controller.process.bean;

import java.text.DecimalFormat;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Producaoagenda;

public class AnaliseCustoBean {

	protected Producaoagenda producaoagenda;
	protected Material material;
	protected String nomeMaterial;
	protected Double qtdeVendido;
	protected Double custoOperacional = 0.00;
	protected Double custoComercial = 0.00;
	protected String valorUnitarioVenda;
	DecimalFormat df = new DecimalFormat("#,##0.00");
	
	public Producaoagenda getProducaoagenda() {
		return producaoagenda;
	}
	public Material getMaterial() {
		return material;
	}
	public String getNomeMaterial() {
		return nomeMaterial;
	}
	public Double getQtdeVendido() {
		return qtdeVendido;
	}
	public String getCustoMateriaPrima() {		 
		return  df.format(this.getCustoMateriaPrimaDouble());
	}
	public Double getCustoMateriaPrimaDouble(){
		return this.getMaterial().getValorcusto() != null ? this.getMaterial().getValorcusto() : 0.0;
	}
	public Double getCustoOperacional() {
		return custoOperacional;
	}
	public Double getCustoComercial() {
		return custoComercial;
	}
	public String getValorUnitarioCusto() {		
		return df.format(this.getValorUnitarioCustoDouble()); 
	}
	public Double getValorUnitarioCustoDouble(){
		//Soma das tr�s colunas anteriores
		return (this.getCustoMateriaPrimaDouble() != null ? this.getCustoMateriaPrimaDouble() : 0.0) 
		+ (this.custoOperacional != null ? this.custoOperacional : 0.0) 
		+ (this.custoComercial != null ? this.custoComercial : 0.0);		
	}
	public String getValorUnitarioVenda() {
		return valorUnitarioVenda != null ? valorUnitarioVenda : "0.0";
	}
	public String getCustoTotal() {
		return df.format(this.getCustoTotalDouble());
	}
	public Double getCustoTotalDouble(){
		//Valor unit�rio de custo * qtde
		return this.getValorUnitarioCustoDouble() * this.getQtdeVendido();
	}
	public String getTotalVendido() {		 
		return df.format(this.getTotalVendidoDouble());
	}
	public Double getTotalVendidoDouble(){
		return this.qtdeVendido * Double.parseDouble(this.getValorUnitarioVenda().replaceAll("\\.", "").replace(",", "."));
	}
	
	public String getMarkup() {				
		return df.format(this.getMarkupDouble());
	}	
	public Double getMarkupDouble(){
		//Custo Unit�rio / Venda Unit�rio
		Double markup = 0.0;
		Double valorUnitarioVenda = Double.parseDouble(this.getValorUnitarioVenda().replaceAll("\\.", "").replace(",", "."));
		if(valorUnitarioVenda != null && valorUnitarioVenda != 0.0){
			markup =  this.getValorUnitarioCustoDouble() / (valorUnitarioVenda);
		}
		return markup;
	}
	
	public String getCustoOperacionalStr(){
		return df.format(this.getCustoOperacional());
	}
	
	public String getCustoComercialStr(){
		return df.format(this.getCustoComercial());
	}
	
	public void setProducaoagenda(Producaoagenda producaoagenda) {
		this.producaoagenda = producaoagenda;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setNomeMaterial(String nomeMaterial) {
		this.nomeMaterial = nomeMaterial;
	}
	public void setQtdeVendido(Double qtdeVendido) {
		this.qtdeVendido = qtdeVendido;
	}
	public void setCustoOperacional(Double custoOperacional) {
		this.custoOperacional = custoOperacional;
	}
	public void setCustoComercial(Double custoComercial) {
		this.custoComercial = custoComercial;
	}
	public void setValorUnitarioVenda(String valorUnitarioVenda) {
		this.valorUnitarioVenda = valorUnitarioVenda;
	}

}
