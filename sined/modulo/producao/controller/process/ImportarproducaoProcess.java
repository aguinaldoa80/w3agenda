package br.com.linkcom.sined.modulo.producao.controller.process;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Cemcomponente;
import br.com.linkcom.sined.geral.bean.Cemcomponenteacumulado;
import br.com.linkcom.sined.geral.bean.Cemobra;
import br.com.linkcom.sined.geral.bean.Cempainel;
import br.com.linkcom.sined.geral.bean.Cemperfil;
import br.com.linkcom.sined.geral.bean.Cemperfilacumulado;
import br.com.linkcom.sined.geral.bean.Cemtipologia;
import br.com.linkcom.sined.geral.bean.Cemvidro;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.service.CemobraService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ContratoJson;
import br.com.linkcom.sined.modulo.producao.controller.process.filter.ImportarproducaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(
		path="/producao/process/Importarproducao",
		authorizationModule=ProcessAuthorizationModule.class
)
public class ImportarproducaoProcess extends MultiActionController {
	
	private CemobraService cemobraService;
	private ContratoService contratoService;
	private ContagerencialService contagerencialService;
	
	public void setContagerencialService(
			ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setCemobraService(CemobraService cemobraService) {
		this.cemobraService = cemobraService;
	}
	
	@DefaultAction
	public ModelAndView carregar(WebRequestContext request, ImportarproducaoFiltro filtro){
		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaResultadoCompensacaoOutras());
		return new ModelAndView("process/importarproducao", "filtro", filtro);
	}
	
	/**
	 * M�todo que importa os dados de produ��o
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @throws Exception
	 * @author Luiz Fernando
	 */
	public ModelAndView importar(WebRequestContext request, ImportarproducaoFiltro filtro) throws Exception {
		try{
			this.importarArquivoObra(request, filtro);
		} catch (Exception e) {
			request.addError("Problemas na importa��o de produ��o.");
			request.addError(e.getMessage());
			e.printStackTrace();
			return new ModelAndView("/process/importarproducao", "filtro", filtro);
		}
		
		return new ModelAndView("/process/importarproducao", "filtro", new ImportarproducaoFiltro());
	}
	
	/**
	 * Action em ajax para buscar as informa��es do cliente
	 *
	 * @param request
	 * @param producaoagenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public ModelAndView carregaInfoClienteAjax(WebRequestContext request, Producaoagenda producaoagenda){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(producaoagenda.getCliente() != null){
			// BUSCA A LISTA DE CONTRATO DO CLIENTE
			List<Contrato> listaContrato = contratoService.findByClienteForProducaoAgenda(producaoagenda.getCliente());
			List<ContratoJson> listaContratoJson = new ArrayList<ContratoJson>();
			for (Contrato contrato : listaContrato) {
				listaContratoJson.add(new ContratoJson(contrato));
			}
			jsonModelAndView.addObject("listaContrato", listaContratoJson);
		}
		return jsonModelAndView;
	}

	@SuppressWarnings("unchecked")
	private void importarArquivoObra(WebRequestContext request, ImportarproducaoFiltro filtro) throws UnsupportedEncodingException, JDOMException, IOException {
		if(filtro.getArquivo() == null){
			throw new SinedException("Arquivo n�o encontrado.");
		}
		byte[] content = filtro.getArquivo().getContent();
		Element obraElement = SinedUtil.getRootElementXML(content);
		
		if(obraElement == null || obraElement.getName() == null || !obraElement.getName().toUpperCase().equals("OBRA")){
			throw new SinedException("Arquivo inv�lido: Elemento raiz 'OBRA' n�o encontrado.");
		}
		
		// DADOS DA OBRA
		Element dadosObraElement = SinedUtil.getChildElement("DADOS_OBRA", obraElement.getContent());
		if(dadosObraElement == null) throw new SinedException("Arquivo inv�lido: Elemento 'DADOS_OBRA' n�o encontrado.");
		
		Cemobra cemobra = new Cemobra();
		
		Element codigoDadosObraElement = SinedUtil.getChildElement("CODIGO", dadosObraElement.getContent());
		Element nomeDadosObraElement = SinedUtil.getChildElement("NOME", dadosObraElement.getContent());
		
		cemobra.setCodigo(SinedUtil.getElementString(codigoDadosObraElement));
		cemobra.setNome(SinedUtil.getElementString(nomeDadosObraElement));
		cemobra.setArquivo(filtro.getArquivo());
		
		// DADOS DO CLIENTE
		Element dadosClienteElement = SinedUtil.getChildElement("DADOS_CLIENTE", obraElement.getContent());
		if(dadosClienteElement == null) throw new SinedException("Arquivo inv�lido: Elemento 'DADOS_CLIENTE' n�o encontrado.");
		
		Element nomeDadosClienteElement = SinedUtil.getChildElement("NOME", dadosClienteElement.getContent());
		Element cnpjcpfDadosClienteElement = SinedUtil.getChildElement("CNPJ_CPF", dadosClienteElement.getContent());
		Element logradouroDadosClienteElement = SinedUtil.getChildElement("END_LOGR", dadosClienteElement.getContent());
		Element numeroDadosClienteElement = SinedUtil.getChildElement("END_NUMERO", dadosClienteElement.getContent());
		Element complementoDadosClienteElement = SinedUtil.getChildElement("END_COMPL", dadosClienteElement.getContent());
		Element bairroDadosClienteElement = SinedUtil.getChildElement("END_BAIRRO", dadosClienteElement.getContent());
		Element cidadeDadosClienteElement = SinedUtil.getChildElement("END_CIDADE", dadosClienteElement.getContent());
		Element cepDadosClienteElement = SinedUtil.getChildElement("END_CEP", dadosClienteElement.getContent());
		Element ufDadosClienteElement = SinedUtil.getChildElement("END_UF", dadosClienteElement.getContent());
		
		cemobra.setCliente_nome(SinedUtil.getElementString(nomeDadosClienteElement));
		cemobra.setCliente_cnpjcpf(SinedUtil.getElementString(cnpjcpfDadosClienteElement));
		cemobra.setCliente_logradouro(SinedUtil.getElementString(logradouroDadosClienteElement));
		cemobra.setCliente_numero(SinedUtil.getElementString(numeroDadosClienteElement));
		cemobra.setCliente_complemento(SinedUtil.getElementString(complementoDadosClienteElement));
		cemobra.setCliente_bairro(SinedUtil.getElementString(bairroDadosClienteElement));
		cemobra.setCliente_cidade(SinedUtil.getElementString(cidadeDadosClienteElement));
		cemobra.setCliente_cep(SinedUtil.getElementString(cepDadosClienteElement));
		cemobra.setCliente_uf(SinedUtil.getElementString(ufDadosClienteElement));
		
		// PERFIS ACUMULADOS
		Element perfils_acumuladoElement = SinedUtil.getChildElement("PERFIS_ACUMULADO", obraElement.getContent());
		if(perfils_acumuladoElement == null) throw new SinedException("Arquivo inv�lido: Elemento 'PERFIS_ACUMULADO' n�o encontrado.");
		
		Set<Cemperfilacumulado> listaCemperfilacumulado = new ListSet<Cemperfilacumulado>(Cemperfilacumulado.class);
		List<Element> listaPerfilacumulado = SinedUtil.getListChildElement("PERFIL", perfils_acumuladoElement.getContent());
		
		if(listaPerfilacumulado != null && listaPerfilacumulado.size() > 0){
			for (Element perfilacumuladoElement : listaPerfilacumulado) {
				
				Element refPerfilElement = SinedUtil.getChildElement("REF", perfilacumuladoElement.getContent());
				Element codigoPerfilElement = SinedUtil.getChildElement("CODIGO", perfilacumuladoElement.getContent());
				Element linhaPerfilElement = SinedUtil.getChildElement("LINHA", perfilacumuladoElement.getContent());
				Element tratPerfilElement = SinedUtil.getChildElement("TRAT", perfilacumuladoElement.getContent());
				Element classe_idPerfilElement = SinedUtil.getChildElement("CLASSE_ID", perfilacumuladoElement.getContent());
				Element descricaoPerfilElement = SinedUtil.getChildElement("DESCRICAO", perfilacumuladoElement.getContent());
				Element qtdePerfilElement = SinedUtil.getChildElement("QTDE", perfilacumuladoElement.getContent());
				Element tamPerfilElement = SinedUtil.getChildElement("TAM", perfilacumuladoElement.getContent());
				Element pesoPerfilElement = SinedUtil.getChildElement("PESO", perfilacumuladoElement.getContent());
				Element peso_sobraPerfilElement = SinedUtil.getChildElement("PESO_SOBRA", perfilacumuladoElement.getContent());
				Element custoPerfilElement = SinedUtil.getChildElement("CUSTO", perfilacumuladoElement.getContent());
				Element custo_tratPerfilElement = SinedUtil.getChildElement("CUSTO_TRAT", perfilacumuladoElement.getContent());
				
				Cemperfilacumulado cemperfilacumulado = new Cemperfilacumulado();
				
				cemperfilacumulado.setRef(SinedUtil.getElementString(refPerfilElement));
				cemperfilacumulado.setCodigo(SinedUtil.getElementString(codigoPerfilElement));
				cemperfilacumulado.setLinha(SinedUtil.getElementString(linhaPerfilElement));
				cemperfilacumulado.setTrat(SinedUtil.getElementString(tratPerfilElement));
				cemperfilacumulado.setClasse_id(SinedUtil.getElementString(classe_idPerfilElement));
				cemperfilacumulado.setDescricao(SinedUtil.getElementString(descricaoPerfilElement));
				cemperfilacumulado.setQtde(SinedUtil.getElementDouble(qtdePerfilElement));
				cemperfilacumulado.setTam(SinedUtil.getElementDouble(tamPerfilElement));
				cemperfilacumulado.setPeso(SinedUtil.getElementDouble(pesoPerfilElement));
				cemperfilacumulado.setPeso_sobra(SinedUtil.getElementDouble(peso_sobraPerfilElement));
				cemperfilacumulado.setCusto(SinedUtil.getElementDouble(custoPerfilElement));
				cemperfilacumulado.setCusto_trat(SinedUtil.getElementDouble(custo_tratPerfilElement));
				
				listaCemperfilacumulado.add(cemperfilacumulado);
			}
		}
		cemobra.setListaCemperfilacumulado(listaCemperfilacumulado);
		
		// COMPONENTES ACUMULADOS
		Element componentes_acumuladoElement = SinedUtil.getChildElement("COMPONENTES_ACUMULADO", obraElement.getContent());
		if(componentes_acumuladoElement == null) throw new SinedException("Arquivo inv�lido: Elemento 'COMPONENTES_ACUMULADO' n�o encontrado.");
		
		Set<Cemcomponenteacumulado> listaCemcomponenteacumulado = new ListSet<Cemcomponenteacumulado>(Cemcomponenteacumulado.class);
		List<Element> listaComponenteacumulado = SinedUtil.getListChildElement("COMPONENTE", componentes_acumuladoElement.getContent());
		
		if(listaComponenteacumulado != null && listaComponenteacumulado.size() > 0){
			for (Element componenteacumuladoElement : listaComponenteacumulado) {
				Element refComponenteElement = SinedUtil.getChildElement("REF", componenteacumuladoElement.getContent());
				Element codigoComponenteElement = SinedUtil.getChildElement("CODIGO", componenteacumuladoElement.getContent());
				Element codigocorComponenteElement = SinedUtil.getChildElement("CODIGOCOR", componenteacumuladoElement.getContent());
				Element grupoComponenteElement = SinedUtil.getChildElement("GRUPO", componenteacumuladoElement.getContent());
				Element descricaoComponenteElement = SinedUtil.getChildElement("DESCRICAO", componenteacumuladoElement.getContent());
				Element unidComponenteElement = SinedUtil.getChildElement("UNID", componenteacumuladoElement.getContent());
				Element qtdeComponenteElement = SinedUtil.getChildElement("QTDE", componenteacumuladoElement.getContent());
				Element tamComponenteElement = SinedUtil.getChildElement("TAM", componenteacumuladoElement.getContent());
				Element embalagemComponenteElement = SinedUtil.getChildElement("EMBALAGEM", componenteacumuladoElement.getContent());
				Element num_barrasComponenteElement = SinedUtil.getChildElement("NUM_BARRAS", componenteacumuladoElement.getContent());
				Element custoComponenteElement = SinedUtil.getChildElement("CUSTO", componenteacumuladoElement.getContent());
				
				Cemcomponenteacumulado cemcomponenteacumulado = new Cemcomponenteacumulado();
				
				cemcomponenteacumulado.setRef(SinedUtil.getElementString(refComponenteElement));
				cemcomponenteacumulado.setCodigo(SinedUtil.getElementString(codigoComponenteElement));
				cemcomponenteacumulado.setCodigocor(SinedUtil.getElementString(codigocorComponenteElement));
				cemcomponenteacumulado.setGrupo(SinedUtil.getElementString(grupoComponenteElement));
				cemcomponenteacumulado.setDescricao(SinedUtil.getElementString(descricaoComponenteElement));
				cemcomponenteacumulado.setUnid(SinedUtil.getElementString(unidComponenteElement));
				cemcomponenteacumulado.setQtde(SinedUtil.getElementDouble(qtdeComponenteElement));
				cemcomponenteacumulado.setTam(SinedUtil.getElementDouble(tamComponenteElement));
				cemcomponenteacumulado.setEmbalagem(SinedUtil.getElementDouble(embalagemComponenteElement));
				cemcomponenteacumulado.setNum_barras(SinedUtil.getElementDouble(num_barrasComponenteElement));
				cemcomponenteacumulado.setCusto(SinedUtil.getElementDouble(custoComponenteElement));
				
				listaCemcomponenteacumulado.add(cemcomponenteacumulado);
			}
		}
		cemobra.setListaCemcomponenteacumulado(listaCemcomponenteacumulado);
		
		// TIPOLOGIAS
		Element tipologiasElement = SinedUtil.getChildElement("TIPOLOGIAS", obraElement.getContent());
		if(tipologiasElement == null) throw new SinedException("Arquivo inv�lido: Elemento 'TIPOLOGIAS' n�o encontrado.");
		
		List<Element> listaTipologia = SinedUtil.getListChildElement("TIPOLOGIA", tipologiasElement.getContent());
		Set<Cemtipologia> listaCemtipologia = new ListSet<Cemtipologia>(Cemtipologia.class);
		
		if(listaTipologia != null && listaTipologia.size() > 0){
			for (Element tipologiaElement : listaTipologia) {
				Element projetistaTipologiaElement = SinedUtil.getChildElement("PROJETISTA", tipologiaElement.getContent());
				Element codesqdTipologiaElement = SinedUtil.getChildElement("CODESQD", tipologiaElement.getContent());
				Element tipoTipologiaElement = SinedUtil.getChildElement("TIPO", tipologiaElement.getContent());
				Element qtdeTipologiaElement = SinedUtil.getChildElement("QTDE", tipologiaElement.getContent());
				Element larguraTipologiaElement = SinedUtil.getChildElement("LARGURA", tipologiaElement.getContent());
				Element alturaTipologiaElement = SinedUtil.getChildElement("ALTURA", tipologiaElement.getContent());
				Element trat_perfTipologiaElement = SinedUtil.getChildElement("TRAT_PERF", tipologiaElement.getContent());
				Element cor_compTipologiaElement = SinedUtil.getChildElement("COR_COMP", tipologiaElement.getContent());
				Element descrTipologiaElement = SinedUtil.getChildElement("DESCR", tipologiaElement.getContent());
				Element descr_vidrosTipologiaElement = SinedUtil.getChildElement("DESCR_VIDROS", tipologiaElement.getContent());
				Element peso_unitTipologiaElement = SinedUtil.getChildElement("PESO_UNIT", tipologiaElement.getContent());
				Element custo_unitTipologiaElement = SinedUtil.getChildElement("CUSTO_UNIT", tipologiaElement.getContent());
				Element preco_unitTipologiaElement = SinedUtil.getChildElement("PRECO_UNIT", tipologiaElement.getContent());
				
				Cemtipologia cemtipologia = new Cemtipologia();
				
				cemtipologia.setProjetista(SinedUtil.getElementString(projetistaTipologiaElement));
				cemtipologia.setCodesqd(SinedUtil.getElementString(codesqdTipologiaElement));
				cemtipologia.setTipo(SinedUtil.getElementString(tipoTipologiaElement));
				cemtipologia.setQtde(SinedUtil.getElementDouble(qtdeTipologiaElement));
				cemtipologia.setLargura(SinedUtil.getElementDouble(larguraTipologiaElement));
				cemtipologia.setAltura(SinedUtil.getElementDouble(alturaTipologiaElement));
				cemtipologia.setTrat_perf(SinedUtil.getElementString(trat_perfTipologiaElement));
				cemtipologia.setCor_comp(SinedUtil.getElementString(cor_compTipologiaElement));
				cemtipologia.setDescr(SinedUtil.getElementString(descrTipologiaElement));
				cemtipologia.setDescr_vidros(SinedUtil.getElementString(descr_vidrosTipologiaElement));
				cemtipologia.setPeso_unit(SinedUtil.getElementDouble(peso_unitTipologiaElement));
				cemtipologia.setCusto_unit(SinedUtil.getElementDouble(custo_unitTipologiaElement));
				cemtipologia.setPreco_unit(SinedUtil.getElementDouble(preco_unitTipologiaElement));
				
				Element componentesElement = SinedUtil.getChildElement("COMPONENTES", tipologiaElement.getContent());
				if(componentesElement != null){
					List<Element> listaComponente = SinedUtil.getListChildElement("COMPONENTE", componentesElement.getContent());
					if(listaComponente != null && listaComponente.size() > 0){
						Set<Cemcomponente> listaCemcomponente = new ListSet<Cemcomponente>(Cemcomponente.class);
						for (Element componenteElement : listaComponente) {
							Element refComponenteElement = SinedUtil.getChildElement("REF", componenteElement.getContent());
							Element codigoComponenteElement = SinedUtil.getChildElement("CODIGO", componenteElement.getContent());
							Element codigocorComponenteElement = SinedUtil.getChildElement("CODIGOCOR", componenteElement.getContent());
							Element descricaoComponenteElement = SinedUtil.getChildElement("DESCRICAO", componenteElement.getContent());
							Element unidComponenteElement = SinedUtil.getChildElement("UNID", componenteElement.getContent());
							Element qtdeComponenteElement = SinedUtil.getChildElement("QTDE", componenteElement.getContent());
							Element tamComponenteElement = SinedUtil.getChildElement("TAM", componenteElement.getContent());
							Element custoComponenteElement = SinedUtil.getChildElement("CUSTO", componenteElement.getContent());
							
							Cemcomponente cemcomponente = new Cemcomponente();
							
							cemcomponente.setRef(SinedUtil.getElementString(refComponenteElement));
							cemcomponente.setCodigo(SinedUtil.getElementString(codigoComponenteElement));
							cemcomponente.setCodigocor(SinedUtil.getElementString(codigocorComponenteElement));
							cemcomponente.setDescricao(SinedUtil.getElementString(descricaoComponenteElement));
							cemcomponente.setUnid(SinedUtil.getElementString(unidComponenteElement));
							cemcomponente.setQtde(SinedUtil.getElementDouble(qtdeComponenteElement));
							cemcomponente.setTam(SinedUtil.getElementDouble(tamComponenteElement));
							cemcomponente.setCusto(SinedUtil.getElementDouble(custoComponenteElement));
							
							listaCemcomponente.add(cemcomponente);
						}
						cemtipologia.setListaCemcomponente(listaCemcomponente);
					}
				}
				
				Element perfisElement = SinedUtil.getChildElement("PERFIS", tipologiaElement.getContent());
				if(perfisElement != null){
					List<Element> listaPerfil = SinedUtil.getListChildElement("PERFIL", perfisElement.getContent());
					if(listaPerfil != null && listaPerfil.size() > 0){
						Set<Cemperfil> listaCemperfil = new ListSet<Cemperfil>(Cemperfil.class);
						for (Element perfilElement : listaPerfil) {
							Element refPerfilElement = SinedUtil.getChildElement("REF", perfilElement.getContent());
							Element codigoPerfilElement = SinedUtil.getChildElement("CODIGO", perfilElement.getContent());
							Element linhaPerfilElement = SinedUtil.getChildElement("LINHA", perfilElement.getContent());
							Element tratPerfilElement = SinedUtil.getChildElement("TRAT", perfilElement.getContent());
							Element descricaoPerfilElement = SinedUtil.getChildElement("DESCRICAO", perfilElement.getContent());
							Element pesoPerfilElement = SinedUtil.getChildElement("PESO", perfilElement.getContent());
							Element qtdePerfilElement = SinedUtil.getChildElement("QTDE", perfilElement.getContent());
							Element tamPerfilElement = SinedUtil.getChildElement("TAM", perfilElement.getContent());
							Element ang_esqPerfilElement = SinedUtil.getChildElement("ANG_ESQ", perfilElement.getContent());
							Element ang_dirPerfilElement = SinedUtil.getChildElement("ANG_DIR", perfilElement.getContent());
							Element custoPerfilElement = SinedUtil.getChildElement("CUSTO", perfilElement.getContent());
							Element tamanhos_barrasPerfilElement = SinedUtil.getChildElement("TAMANHOS_BARRAS", perfilElement.getContent());
							
							Cemperfil cemperfil = new Cemperfil();
							
							cemperfil.setRef(SinedUtil.getElementString(refPerfilElement));
							cemperfil.setCodigo(SinedUtil.getElementString(codigoPerfilElement));
							cemperfil.setLinha(SinedUtil.getElementString(linhaPerfilElement));
							cemperfil.setTrat(SinedUtil.getElementString(tratPerfilElement));
							cemperfil.setDescricao(SinedUtil.getElementString(descricaoPerfilElement));
							cemperfil.setPeso(SinedUtil.getElementDouble(pesoPerfilElement));
							cemperfil.setQtde(SinedUtil.getElementDouble(qtdePerfilElement));
							cemperfil.setTam(SinedUtil.getElementDouble(tamPerfilElement));
							cemperfil.setAng_esq(SinedUtil.getElementString(ang_esqPerfilElement));
							cemperfil.setAng_dir(SinedUtil.getElementString(ang_dirPerfilElement));
							cemperfil.setCusto(SinedUtil.getElementDouble(custoPerfilElement));
							cemperfil.setTamanhos_barras(SinedUtil.getElementDouble(tamanhos_barrasPerfilElement));
							
							listaCemperfil.add(cemperfil);
						}
						cemtipologia.setListaCemperfil(listaCemperfil);
					}
				}
				
				Element vidrosElement = SinedUtil.getChildElement("VIDROS", tipologiaElement.getContent());
				if(vidrosElement != null){
					List<Element> listaVidro = SinedUtil.getListChildElement("VIDRO", vidrosElement.getContent());
					if(listaVidro != null && listaVidro.size() > 0){
						Set<Cemvidro> listaCemvidro = new ListSet<Cemvidro>(Cemvidro.class);
						for (Element vidroElement : listaVidro) {
							Element codigoVidroElement = SinedUtil.getChildElement("CODIGO", vidroElement.getContent());
							Element refVidroElement = SinedUtil.getChildElement("REF", vidroElement.getContent());
							Element descricaoVidroElement = SinedUtil.getChildElement("DESCRICAO", vidroElement.getContent());
							Element codigocorVidroElement = SinedUtil.getChildElement("CODIGOCOR", vidroElement.getContent());
							Element qtdeVidroElement = SinedUtil.getChildElement("QTDE", vidroElement.getContent());
							Element alturaVidroElement = SinedUtil.getChildElement("ALTURA", vidroElement.getContent());
							Element larguraVidroElement = SinedUtil.getChildElement("LARGURA", vidroElement.getContent());
							Element superficieVidroElement = SinedUtil.getChildElement("SUPERFICIE", vidroElement.getContent());
							Element custoVidroElement = SinedUtil.getChildElement("CUSTO", vidroElement.getContent());
							
							Cemvidro cemvidro = new Cemvidro();
							
							cemvidro.setCodigo(SinedUtil.getElementString(codigoVidroElement));
							cemvidro.setRef(SinedUtil.getElementString(refVidroElement));
							cemvidro.setDescricao(SinedUtil.getElementString(descricaoVidroElement));
							cemvidro.setCodigocor(SinedUtil.getElementString(codigocorVidroElement));
							cemvidro.setQtde(SinedUtil.getElementDouble(qtdeVidroElement));
							cemvidro.setAltura(SinedUtil.getElementDouble(alturaVidroElement));
							cemvidro.setLargura(SinedUtil.getElementDouble(larguraVidroElement));
							cemvidro.setSuperficie(SinedUtil.getElementDouble(superficieVidroElement));
							cemvidro.setCusto(SinedUtil.getElementDouble(custoVidroElement));
							
							listaCemvidro.add(cemvidro);
						}
						cemtipologia.setListaCemvidro(listaCemvidro);
					}
				}
				
				Element paineisElement = SinedUtil.getChildElement("PAINEIS", tipologiaElement.getContent());
				if(paineisElement != null){
					List<Element> listaPainel = SinedUtil.getListChildElement("PAINEL", paineisElement.getContent());
					if(listaPainel != null && listaPainel.size() > 0){
						Set<Cempainel> listaCempainel = new ListSet<Cempainel>(Cempainel.class);
						for (Element painelElement : listaPainel) {
							Element codigoPainelElement = SinedUtil.getChildElement("CODIGO", painelElement.getContent());
							Element refPainelElement = SinedUtil.getChildElement("REF", painelElement.getContent());
							Element descricaoPainelElement = SinedUtil.getChildElement("DESCRICAO", painelElement.getContent());
							Element qtdePainelElement = SinedUtil.getChildElement("QTDE", painelElement.getContent());
							Element alturaPainelElement = SinedUtil.getChildElement("ALTURA", painelElement.getContent());
							Element larguraPainelElement = SinedUtil.getChildElement("LARGURA", painelElement.getContent());
							Element custoPainelElement = SinedUtil.getChildElement("CUSTO", painelElement.getContent());
							
							Cempainel cempainel = new Cempainel();
							
							cempainel.setCodigo(SinedUtil.getElementString(codigoPainelElement));
							cempainel.setRef(SinedUtil.getElementString(refPainelElement));
							cempainel.setDescricao(SinedUtil.getElementString(descricaoPainelElement));
							cempainel.setQtde(SinedUtil.getElementDouble(qtdePainelElement));
							cempainel.setAltura(SinedUtil.getElementDouble(alturaPainelElement));
							cempainel.setLargura(SinedUtil.getElementDouble(larguraPainelElement));
							cempainel.setCusto(SinedUtil.getElementDouble(custoPainelElement));
							
							listaCempainel.add(cempainel);
						}
						cemtipologia.setListaCempainel(listaCempainel);
					}
				}
				
				listaCemtipologia.add(cemtipologia);
			}
		}
		cemobra.setListaCemtipologia(listaCemtipologia);
		
		cemobraService.saveFromImportacao(cemobra);
		cemobraService.saveProducaoFromImportacao(cemobra, filtro);
		
		request.addMessage("Importa��o realizada com sucesso.");
	}

}
