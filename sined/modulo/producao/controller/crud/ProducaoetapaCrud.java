package br.com.linkcom.sined.modulo.producao.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Producaoetapa;
import br.com.linkcom.sined.geral.bean.Producaoetapaitem;
import br.com.linkcom.sined.geral.bean.enumeration.BaixarEstoqueMateriasPrimasEnum;
import br.com.linkcom.sined.geral.service.ProducaoetapaService;
import br.com.linkcom.sined.geral.service.ProducaoetapanomeService;
import br.com.linkcom.sined.modulo.producao.controller.crud.filter.ProducaoetapaFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Producaoetapa", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class ProducaoetapaCrud extends CrudControllerSined<ProducaoetapaFiltro, Producaoetapa, Producaoetapa>{
	
	private ProducaoetapanomeService producaoetapanomeService;
	private ProducaoetapaService producaoetapaService;
	
	public void setProducaoetapanomeService(ProducaoetapanomeService producaoetapanomeService) {this.producaoetapanomeService = producaoetapanomeService;}
	public void setProducaoetapaService(ProducaoetapaService producaoetapaService) {this.producaoetapaService = producaoetapaService;}


	@Override
	protected void entrada(WebRequestContext request, Producaoetapa form) throws Exception {
		request.setAttribute("listaBaixarEstoqueMateriaprima", getListaComboBaixarEstoqueMateriaprima());
	}
	
	/**
	* M�todo que retorna o combo ordenado de baixa de estoque materia prima 
	*
	* @return
	* @since 02/03/2016
	* @author Luiz Fernando
	*/
	private List<BaixarEstoqueMateriasPrimasEnum> getListaComboBaixarEstoqueMateriaprima(){
		List<BaixarEstoqueMateriasPrimasEnum> lista = new ArrayList<BaixarEstoqueMateriasPrimasEnum>();
		lista.add(BaixarEstoqueMateriasPrimasEnum.AO_PRODUZIR_AGENDA);
		lista.add(BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_CADA_ETAPA);
		lista.add(BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA);
		return lista;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Producaoetapa bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "fk_producaoordemmaterial_3")) {
				throw new SinedException("N�o � poss�vel remover um item da etapa que esteja vinculado a uma Ordem de produ��o.");
			}else {
				e.printStackTrace();
				throw new SinedException(e.getMessage());
			}
		}
	}
	
	@Override
	protected void validateBean(Producaoetapa bean, BindException errors) {
		if (bean.getListaProducaoetapaitem()!=null){
			for (Producaoetapaitem producaoetapaitem: bean.getListaProducaoetapaitem()){
				if (producaoetapanomeService.isPermitirtrocarprodutofinal(producaoetapaitem.getProducaoetapanome())
						&& (Boolean.TRUE.equals(bean.getNaoagruparmaterial()) || !BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA.equals(bean.getBaixarEstoqueMateriaprima()))){
					errors.reject("001", "Para incluir etapa marcada para 'Permitir trocar produto final', o ciclo deve estar configurado para 'n�o separar ordens de produ��o por material' e baixar estoque de mat�rias-primas 'Ao concluir �ltima etapa'.");
					break;
				}
			}
		}
		if(bean.getOrdemprioridade()!=null){
			Boolean ordemPrioridade = producaoetapaService.validaOrdemPrioridade(bean);
			if(ordemPrioridade){
				errors.reject("002", "Ordem de prioridade ja existente");
			}
		}
		super.validateBean(bean, errors);
		
	}
}