package br.com.linkcom.sined.modulo.producao.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoetapaitem;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.util.SinedUtil;

public class ProducaoSobraItemBean {

	private Material material;
	private Unidademedida unidademedida;
	private Long qtdeutilizada;
	private Double qtdeproduzida;
	private Double percentualsobra;
	private Double qtdesobra;
	
	private Producaoagenda producaoagenda;
	private Producaoagendamaterial producaoagendamaterial;
	private Producaoetapaitem producaoetapaitem;
	
	public ProducaoSobraItemBean(){}
	
	public ProducaoSobraItemBean(Material material, Unidademedida unidademedida, Long qtdeutilizada, Double qtdeproduzida, Double qtdesobra, 
			Producaoagenda producaoagenda, Producaoagendamaterial producaoagendamaterial, Producaoetapaitem producaoetapaitem) {
		this.material = material;
		this.unidademedida = unidademedida;
		this.qtdeutilizada = qtdeutilizada;
		this.qtdeproduzida = qtdeproduzida != null ? SinedUtil.round(qtdeproduzida, 4) : null;
		this.qtdesobra = qtdesobra != null ? SinedUtil.round(qtdesobra, 4) : null;
		if(qtdesobra != null && qtdeutilizada != null){
			this.percentualsobra = SinedUtil.round(qtdesobra / qtdeutilizada * 100, 4);
		}
		this.producaoagenda = producaoagenda;
		this.producaoagendamaterial = producaoagendamaterial;
		this.producaoetapaitem = producaoetapaitem;
	}
	
	public Material getMaterial() {
		return material;
	}
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	public Long getQtdeutilizada() {
		return qtdeutilizada;
	}
	public Double getQtdeproduzida() {
		return qtdeproduzida;
	}
	public Double getPercentualsobra() {
		return percentualsobra;
	}
	public Double getQtdesobra() {
		return qtdesobra;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setQtdeutilizada(Long qtdeutilizada) {
		this.qtdeutilizada = qtdeutilizada;
	}
	public void setQtdeproduzida(Double qtdeproduzida) {
		this.qtdeproduzida = qtdeproduzida;
	}
	public void setPercentualsobra(Double percentualsobra) {
		this.percentualsobra = percentualsobra;
	}
	public void setQtdesobra(Double qtdesobra) {
		this.qtdesobra = qtdesobra;
	}
	public Producaoagenda getProducaoagenda() {
		return producaoagenda;
	}
	public Producaoagendamaterial getProducaoagendamaterial() {
		return producaoagendamaterial;
	}
	public void setProducaoagenda(Producaoagenda producaoagenda) {
		this.producaoagenda = producaoagenda;
	}
	public void setProducaoagendamaterial(Producaoagendamaterial producaoagendamaterial) {
		this.producaoagendamaterial = producaoagendamaterial;
	}
	public Producaoetapaitem getProducaoetapaitem() {
		return producaoetapaitem;
	}
	public void setProducaoetapaitem(Producaoetapaitem producaoetapaitem) {
		this.producaoetapaitem = producaoetapaitem;
	}
}
