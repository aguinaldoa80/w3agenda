package br.com.linkcom.sined.modulo.producao.controller.crud.bean;

import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;

public class ConferenciaProducaoBean {

	private List<Producaoordemmaterial> listaProducaoordemmaterial = new ListSet<Producaoordemmaterial>(Producaoordemmaterial.class);
	private List<MateriaprimaConferenciaProducaoBean> listaMateriaprima = new ListSet<MateriaprimaConferenciaProducaoBean>(MateriaprimaConferenciaProducaoBean.class);
	private Boolean producaoEmLote;
	private String identificadorsessao;
	private String whereInProducaoordemForConclusao;
	
	public List<Producaoordemmaterial> getListaProducaoordemmaterial() {
		return listaProducaoordemmaterial;
	}
	public List<MateriaprimaConferenciaProducaoBean> getListaMateriaprima() {
		return listaMateriaprima;
	}
	public void setListaProducaoordemmaterial(
			List<Producaoordemmaterial> listaProducaoordemmaterial) {
		this.listaProducaoordemmaterial = listaProducaoordemmaterial;
	}
	public void setListaMateriaprima(
			List<MateriaprimaConferenciaProducaoBean> listaMateriaprima) {
		this.listaMateriaprima = listaMateriaprima;
	}

	public Boolean getProducaoEmLote() {
		return producaoEmLote;
	}
	public void setProducaoEmLote(Boolean producaoEmLote) {
		this.producaoEmLote = producaoEmLote;
	}
	public String getIdentificadorsessao() {
		return identificadorsessao;
	}
	public void setIdentificadorsessao(String identificadorsessao) {
		this.identificadorsessao = identificadorsessao;
	}
	public String getWhereInProducaoordemForConclusao() {
		return whereInProducaoordemForConclusao;
	}
	public void setWhereInProducaoordemForConclusao(
			String whereInProducaoordemForConclusao) {
		this.whereInProducaoordemForConclusao = whereInProducaoordemForConclusao;
	}
}
