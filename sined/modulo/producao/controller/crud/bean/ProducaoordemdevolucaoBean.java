package br.com.linkcom.sined.modulo.producao.controller.crud.bean;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;

public class ProducaoordemdevolucaoBean {

	private String whereInProducaoordem;
	private String whereInColeta;
	private Empresa empresacoleta;
	private Empresa empresaproducaoordem;
	private Cliente clientecoleta;
	private Cliente clienteproducaoordem;
	private Naturezaoperacao naturezaoperacaosaidafiscal;
	private Boolean gerarNota = Boolean.TRUE;
	private String motivodevolucao;
	private String urlretorno;
	private List<ProducaoordemdevolucaoitemBean> listaItens;
	
	private Boolean exibirPopupConsumoMateriaprima;
	
	public String getWhereInProducaoordem() {
		return whereInProducaoordem;
	}
	public String getWhereInColeta() {
		return whereInColeta;
	}
	public Empresa getEmpresacoleta() {
		return empresacoleta;
	}
	public Cliente getClientecoleta() {
		return clientecoleta;
	}
	public Boolean getGerarNota() {
		return gerarNota;
	}
	@DisplayName("Motivo da Devolu��o")
	public String getMotivodevolucao() {
		return motivodevolucao;
	}
	public String getUrlretorno() {
		return urlretorno;
	}
	public List<ProducaoordemdevolucaoitemBean> getListaItens() {
		return listaItens;
	}
	public Empresa getEmpresaproducaoordem() {
		return empresaproducaoordem;
	}
	public Cliente getClienteproducaoordem() {
		return clienteproducaoordem;
	}
	public Naturezaoperacao getNaturezaoperacaosaidafiscal() {
		return naturezaoperacaosaidafiscal;
	}
	
	public void setWhereInProducaoordem(String whereInProducaoordem) {
		this.whereInProducaoordem = whereInProducaoordem;
	}
	public void setWhereInColeta(String whereInColeta) {
		this.whereInColeta = whereInColeta;
	}
	public void setEmpresacoleta(Empresa empresacoleta) {
		this.empresacoleta = empresacoleta;
	}
	public void setClientecoleta(Cliente clientecoleta) {
		this.clientecoleta = clientecoleta;
	}
	public void setGerarNota(Boolean gerarNota) {
		this.gerarNota = gerarNota;
	}
	public void setMotivodevolucao(String motivodevolucao) {
		this.motivodevolucao = motivodevolucao;
	}
	public void setUrlretorno(String urlretorno) {
		this.urlretorno = urlretorno;
	}
	public void setListaItens(List<ProducaoordemdevolucaoitemBean> listaItens) {
		this.listaItens = listaItens;
	}
	public void setEmpresaproducaoordem(Empresa empresaproducaoordem) {
		this.empresaproducaoordem = empresaproducaoordem;
	}
	public void setClienteproducaoordem(Cliente clienteproducaoordem) {
		this.clienteproducaoordem = clienteproducaoordem;
	}
	public void setNaturezaoperacaosaidafiscal(Naturezaoperacao naturezaoperacaosaidafiscal) {
		this.naturezaoperacaosaidafiscal = naturezaoperacaosaidafiscal;
	}
	
	public Boolean getExibirPopupConsumoMateriaprima() {
		return exibirPopupConsumoMateriaprima;
	}
	public void setExibirPopupConsumoMateriaprima(Boolean exibirPopupConsumoMateriaprima) {
		this.exibirPopupConsumoMateriaprima = exibirPopupConsumoMateriaprima;
	}
}
