package br.com.linkcom.sined.modulo.producao.controller.crud.bean;

import java.awt.Image;

import br.com.linkcom.neo.util.Util;


public class ComprovanteGarantiareformaBean {

	private String cdgarantia = "";
	private String cliente = "";
	private String enderecocliente = "";
	private String servicogarantido = "";
	private String pneu = "";
	private String medidapneu = "";
	private String desenhopneu = "";
	private String dot = "";
	private String fogo = "";
	private String marcapneu = "";
	private String profundidadesulco = "";
	private String residuobanda = "";
	private String servico = "";
	private String codigoservicogarantido = "";
	private String osreclamacao = "";
	private String osrelacionada = "";
	private String constatacao = "";
	private String resultado = "";
	private String vendedor = "";
	private String garantiasituacao;
	private Boolean reimpressao;
	private String dtgarantia;
	private Image logo;
	private String mensagemDestino = "";
	
	public String getCdgarantia() {
		return cdgarantia;
	}
	public void setCdgarantia(String cdgarantia) {
		this.cdgarantia = cdgarantia;
	}
	public String getCliente() {
		return Util.strings.emptyIfNull(cliente);
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getEnderecocliente() {
		return Util.strings.emptyIfNull(enderecocliente);
	}
	public void setEnderecocliente(String enderecocliente) {
		this.enderecocliente = enderecocliente;
	}
	public String getServicogarantido() {
		return Util.strings.emptyIfNull(servicogarantido);
	}
	public void setServicogarantido(String servicogarantido) {
		this.servicogarantido = servicogarantido;
	}
	public String getPneu() {
		return Util.strings.emptyIfNull(pneu);
	}
	public void setPneu(String pneu) {
		this.pneu = pneu;
	}
	public String getMedidapneu() {
		return Util.strings.emptyIfNull(medidapneu);
	}
	public void setMedidapneu(String medidapneu) {
		this.medidapneu = medidapneu;
	}
	public String getDesenhopneu() {
		return Util.strings.emptyIfNull(desenhopneu);
	}
	public void setDesenhopneu(String desenhopneu) {
		this.desenhopneu = desenhopneu;
	}
	public String getDot() {
		return Util.strings.emptyIfNull(dot);
	}
	public void setDot(String dot) {
		this.dot = dot;
	}
	public String getFogo() {
		return Util.strings.emptyIfNull(fogo);
	}
	public void setFogo(String fogo) {
		this.fogo = fogo;
	}
	public String getMarcapneu() {
		return Util.strings.emptyIfNull(marcapneu);
	}
	public void setMarcapneu(String marcapneu) {
		this.marcapneu = marcapneu;
	}
	public String getProfundidadesulco() {
		return Util.strings.emptyIfNull(profundidadesulco);
	}
	public void setProfundidadesulco(String profundidadesulco) {
		this.profundidadesulco = profundidadesulco;
	}
	public String getResiduobanda() {
		return Util.strings.emptyIfNull(residuobanda);
	}
	public void setResiduobanda(String residuobanda) {
		this.residuobanda = residuobanda;
	}
	public String getServico() {
		return Util.strings.emptyIfNull(servico);
	}
	public void setServico(String servico) {
		this.servico = servico;
	}
	public String getCodigoservicogarantido() {
		return Util.strings.emptyIfNull(codigoservicogarantido);
	}
	public void setCodigoservicogarantido(String codigoservicogarantido) {
		this.codigoservicogarantido = codigoservicogarantido;
	}
	public String getOsreclamacao() {
		return Util.strings.emptyIfNull(osreclamacao);
	}
	public void setOsreclamacao(String osreclamacao) {
		this.osreclamacao = osreclamacao;
	}
	public String getOsrelacionada() {
		return Util.strings.emptyIfNull(osrelacionada);
	}
	public void setOsrelacionada(String osrelacionada) {
		this.osrelacionada = osrelacionada;
	}
	public String getConstatacao() {
		return Util.strings.emptyIfNull(constatacao);
	}
	public void setConstatacao(String constatacao) {
		this.constatacao = constatacao;
	}
	public String getResultado() {
		return Util.strings.emptyIfNull(resultado);
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	public String getVendedor() {
		return Util.strings.emptyIfNull(vendedor);
	}
	public void setVendedor(String vendedor) {
		this.vendedor = vendedor;
	}
	public String getGarantiasituacao() {
		return Util.strings.emptyIfNull(garantiasituacao);
	}
	public void setGarantiasituacao(String garantiasituacao) {
		this.garantiasituacao = garantiasituacao;
	}
	public Boolean getReimpressao() {
		return reimpressao;
	}
	public void setReimpressao(Boolean reimpressao) {
		this.reimpressao = reimpressao;
	}
	public String getDtgarantia() {
		return dtgarantia;
	}
	public void setDtgarantia(String dtgarantia) {
		this.dtgarantia = dtgarantia;
	}
	public Image getLogo() {
		return logo;
	}
	public void setLogo(Image logo) {
		this.logo = logo;
	}
	public String getMensagemDestino() {
		return Util.strings.emptyIfNull(mensagemDestino);
	}
	public void setMensagemDestino(String mensagemDestino) {
		this.mensagemDestino = mensagemDestino;
	}
	
}
