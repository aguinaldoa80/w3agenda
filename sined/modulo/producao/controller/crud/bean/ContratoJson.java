package br.com.linkcom.sined.modulo.producao.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Contrato;

public class ContratoJson {

	private Integer cdcontrato;
	private String identificador;
	private String descricao;
	private String contratotiponome;
	
	public ContratoJson() {
	}
	
	public ContratoJson(Contrato contrato){
		this.cdcontrato = contrato.getCdcontrato();
		this.identificador = contrato.getIdentificador();
		this.descricao = contrato.getDescricao();
		if(contrato.getContratotipo() != null){
			this.contratotiponome = contrato.getContratotipo().getNome();
		}
	}
	
	public Integer getCdcontrato() {
		return cdcontrato;
	}
	public String getIdentificador() {
		return identificador;
	}
	public String getContratotiponome() {
		return contratotiponome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setCdcontrato(Integer cdcontrato) {
		this.cdcontrato = cdcontrato;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setContratotiponome(String contratotiponome) {
		this.contratotiponome = contratotiponome;
	}
	
}
