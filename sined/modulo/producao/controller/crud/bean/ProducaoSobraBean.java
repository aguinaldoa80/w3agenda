package br.com.linkcom.sined.modulo.producao.controller.crud.bean;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Localarmazenagem;

public class ProducaoSobraBean {

	private Boolean considerarsobraperda;
	private Localarmazenagem localsobra;
	private List<ProducaoSobraItemBean> listaProducaoSobraItemBean;
	
	public Boolean getConsiderarsobraperda() {
		return considerarsobraperda;
	}
	public Localarmazenagem getLocalsobra() {
		return localsobra;
	}
	public List<ProducaoSobraItemBean> getListaProducaoSobraItemBean() {
		return listaProducaoSobraItemBean;
	}
	public void setConsiderarsobraperda(Boolean considerarsobraperda) {
		this.considerarsobraperda = considerarsobraperda;
	}
	public void setLocalsobra(Localarmazenagem localsobra) {
		this.localsobra = localsobra;
	}
	public void setListaProducaoSobraItemBean(List<ProducaoSobraItemBean> listaProducaoSobraItemBean) {
		this.listaProducaoSobraItemBean = listaProducaoSobraItemBean;
	}
}
