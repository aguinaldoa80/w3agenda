package br.com.linkcom.sined.modulo.producao.controller.crud.bean;

import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.enumeration.SitucaoPneuEtiqueteEnum;

public class DadosEtiquetaProducaoBean {
	
	private Integer cdProgucaoAgenda;
	private Cliente cliente;
	private Pneu pneu;
	private SitucaoPneuEtiqueteEnum situacao;
	private Integer cdPedidoVenda;
	private String indentificacaoExterno;
	@DisplayName("Agenda")
	public Integer getCdProgucaoAgenda() {
		return cdProgucaoAgenda;
	}
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Pneu")
	public Pneu getPneu() {
		return pneu;
	}
	@DisplayName("Situa��o")
	public SitucaoPneuEtiqueteEnum getSituacao() {
		return situacao;
	}
	@DisplayName("Pedido/Ident. Externo")
	public Integer getCdPedidoVenda() {
		return cdPedidoVenda;
	}
	@DisplayName("Pedido/Ident. Externo")
	public String getIndentificacaoExterno() {
		return indentificacaoExterno;
	}
	
	
	public void setCdProgucaoAgenda(Integer cdProgucaoAgenda) {
		this.cdProgucaoAgenda = cdProgucaoAgenda;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}
	public void setSituacao(SitucaoPneuEtiqueteEnum situacao) {
		this.situacao = situacao;
	}
	public void setCdPedidoVenda(Integer cdPedidoVenda) {
		this.cdPedidoVenda = cdPedidoVenda;
	}
	public void setIndentificacaoExterno(String indentificacaoExterno) {
		this.indentificacaoExterno = indentificacaoExterno;
	}
	
	///transit
	@DisplayName("Pedido/Ident. Externo")
	public String getDadosVenda(){
		if(getCdPedidoVenda() ==null){
			return getIndentificacaoExterno();
		}else if(getIndentificacaoExterno() == null){
			return getCdPedidoVenda() + "";
		}
		return getCdPedidoVenda() +" / "+ getIndentificacaoExterno();
	}
	//nota��o Id adicionado para o valor injetar o valor no selectIndex da listagem do neo
	@Id
	public String getSelecteditens(){
		return getCdProgucaoAgenda()+"|"+getPneu().getCdpneu();
	}
	
}
