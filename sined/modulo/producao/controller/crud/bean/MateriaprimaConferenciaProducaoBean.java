package br.com.linkcom.sined.modulo.producao.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Unidademedida;

public class MateriaprimaConferenciaProducaoBean {

	private Empresa empresa;
	private Material material;
	private String whereInAgendaProducao;
	private String whereInOrdemProducaoAgendaProducao;
	private String whereInOrdemProducao;
	private Localarmazenagem localarmazenagem;
	private Double qtdedisponivel;
	private Double qtde;
	private Unidademedida unidademedida;
	private Loteestoque loteestoque;
	private Double fracaounidademedida;
	private Double qtdereferenciaunidademedida;
	private Integer casasdecimaisestoqueunidademedida;
	private Boolean baixarmateriaprimacascata;
	private Producaoordemmaterial producaoOrdemMaterial;
	private Producaoagendamaterial producaoAgendaMaterial;

	 
	public Material getMaterial() {
		return material;
	}
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public Double getQtdedisponivel() {
		return qtdedisponivel;
	}
	public Double getQtde() {
		return qtde;
	}
	public String getWhereInAgendaProducao() {
		return whereInAgendaProducao;
	}
	public String getWhereInOrdemProducaoAgendaProducao() {
		return whereInOrdemProducaoAgendaProducao;
	}
	public String getWhereInOrdemProducao() {
		return whereInOrdemProducao;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	public Double getFracaounidademedida() {
		return fracaounidademedida;
	}
	public Double getQtdereferenciaunidademedida() {
		return qtdereferenciaunidademedida;
	}
	public Integer getCasasdecimaisestoqueunidademedida() {
		return casasdecimaisestoqueunidademedida;
	}
	public Producaoagendamaterial getProducaoAgendaMaterial() {
		return producaoAgendaMaterial;
	}
	public Producaoordemmaterial getProducaoOrdemMaterial() {
		return producaoOrdemMaterial;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setWhereInAgendaProducao(String whereInAgendaProducao) {
		this.whereInAgendaProducao = whereInAgendaProducao;
	}
	public void setWhereInOrdemProducaoAgendaProducao(String whereInOrdemProducaoAgendaProducao) {
		this.whereInOrdemProducaoAgendaProducao = whereInOrdemProducaoAgendaProducao;
	}
	public void setWhereInOrdemProducao(String whereInOrdemProducao) {
		this.whereInOrdemProducao = whereInOrdemProducao;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setQtdedisponivel(Double qtdedisponivel) {
		this.qtdedisponivel = qtdedisponivel;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	public void setFracaounidademedida(Double fracaounidademedida) {
		this.fracaounidademedida = fracaounidademedida;
	}
	public void setQtdereferenciaunidademedida(Double qtdereferenciaunidademedida) {
		this.qtdereferenciaunidademedida = qtdereferenciaunidademedida;
	}
	public void setCasasdecimaisestoqueunidademedida(Integer casasdecimaisestoqueunidademedida) {
		this.casasdecimaisestoqueunidademedida = casasdecimaisestoqueunidademedida;
	}
	public Boolean getBaixarmateriaprimacascata() {
		return baixarmateriaprimacascata;
	}
	public void setBaixarmateriaprimacascata(Boolean baixarmateriaprimacascata) {
		this.baixarmateriaprimacascata = baixarmateriaprimacascata;
	}
	public void setProducaoAgendaMaterial(
			Producaoagendamaterial producaoAgendaMaterial) {
		this.producaoAgendaMaterial = producaoAgendaMaterial;
	}
	public void setProducaoOrdemMaterial(
			Producaoordemmaterial producaoOrdemMaterial) {
		this.producaoOrdemMaterial = producaoOrdemMaterial;
	}
}
