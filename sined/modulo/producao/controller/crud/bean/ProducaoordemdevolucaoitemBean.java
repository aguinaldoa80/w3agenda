package br.com.linkcom.sined.modulo.producao.controller.crud.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Motivodevolucao;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;

public class ProducaoordemdevolucaoitemBean {

	private Producaoordemmaterial producaoordemmaterial;
	private Pedidovendamaterial pedidovendamaterial;
	private ColetaMaterial coletaMaterial;
	private Material material;
	private Pneu pneu;
	private Double quantidade;
	private Double quantidadedevolvida;
	private Double quantidadecomprada;
	private Double valorunitario;
	private Localarmazenagem localarmazenagem;
	private Empresa empresa;
	private Motivodevolucao motivodevolucao;
	private String observacao;
	private Motivodevolucao motivodevolucao1;
	private Motivodevolucao motivodevolucao2;
	private Motivodevolucao motivodevolucao3;
	private Motivodevolucao motivodevolucao4;
	private Motivodevolucao motivodevolucao5;
	
	private List<Pedidovendamaterial> listaPedidovendamaterial;
	
	public Producaoordemmaterial getProducaoordemmaterial() {
		return producaoordemmaterial;
	}
	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}
	public ColetaMaterial getColetaMaterial() {
		return coletaMaterial;
	}
	public Material getMaterial() {
		return material;
	}
	public Pneu getPneu() {
		return pneu;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Double getQuantidadedevolvida() {
		return quantidadedevolvida;
	}
	public Double getQuantidadecomprada() {
		return quantidadecomprada;
	}
	public Double getValorunitario() {
		return valorunitario;
	}
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Motivo da Devolu��o")
	public Motivodevolucao getMotivodevolucao() {
		return motivodevolucao;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	public void setProducaoordemmaterial(Producaoordemmaterial producaoordemmaterial) {
		this.producaoordemmaterial = producaoordemmaterial;
	}
	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}
	public void setColetaMaterial(ColetaMaterial coletaMaterial) {
		this.coletaMaterial = coletaMaterial;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setQuantidadedevolvida(Double quantidadedevolvida) {
		this.quantidadedevolvida = quantidadedevolvida;
	}
	public void setQuantidadecomprada(Double quantidadecomprada) {
		this.quantidadecomprada = quantidadecomprada;
	}
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setMotivodevolucao(Motivodevolucao motivodevolucao) {
		this.motivodevolucao = motivodevolucao;
	}public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public Motivodevolucao getMotivodevolucao1() {
		return motivodevolucao1;
	}
	public Motivodevolucao getMotivodevolucao2() {
		return motivodevolucao2;
	}
	public Motivodevolucao getMotivodevolucao3() {
		return motivodevolucao3;
	}
	public Motivodevolucao getMotivodevolucao4() {
		return motivodevolucao4;
	}
	public Motivodevolucao getMotivodevolucao5() {
		return motivodevolucao5;
	}
	public void setMotivodevolucao1(Motivodevolucao motivodevolucao1) {
		this.motivodevolucao1 = motivodevolucao1;
	}
	public void setMotivodevolucao2(Motivodevolucao motivodevolucao2) {
		this.motivodevolucao2 = motivodevolucao2;
	}
	public void setMotivodevolucao3(Motivodevolucao motivodevolucao3) {
		this.motivodevolucao3 = motivodevolucao3;
	}
	public void setMotivodevolucao4(Motivodevolucao motivodevolucao4) {
		this.motivodevolucao4 = motivodevolucao4;
	}
	public void setMotivodevolucao5(Motivodevolucao motivodevolucao5) {
		this.motivodevolucao5 = motivodevolucao5;
	}
	
	public List<Motivodevolucao> getListaMotivodevolucao(){
		List<Motivodevolucao> lista = new ArrayList<Motivodevolucao>();
		
		if (motivodevolucao1!=null){
			lista.add(motivodevolucao1);
		}
		
		if (motivodevolucao2!=null){
			lista.add(motivodevolucao2);
		}
		
		if (motivodevolucao3!=null){
			lista.add(motivodevolucao3);
		}
		
		if (motivodevolucao4!=null){
			lista.add(motivodevolucao4);
		}
		
		if (motivodevolucao5!=null){
			lista.add(motivodevolucao5);
		}
		
		return lista;
	}
	
	public List<Pedidovendamaterial> getListaPedidovendamaterial() {
		return listaPedidovendamaterial;
	}
	
	public void setListaPedidovendamaterial(List<Pedidovendamaterial> listaPedidovendamaterial) {
		this.listaPedidovendamaterial = listaPedidovendamaterial;
	}	
}