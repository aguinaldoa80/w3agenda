package br.com.linkcom.sined.modulo.producao.controller.crud.bean;

import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;

public class ConsumoMateriaprimaProducaoBean {

	private String urlretorno;
	private List<Producaoordemmaterial> listaProducaoordemmaterial = new ListSet<Producaoordemmaterial>(Producaoordemmaterial.class);
	
	public String getUrlretorno() {
		return urlretorno;
	}
	public List<Producaoordemmaterial> getListaProducaoordemmaterial() {
		return listaProducaoordemmaterial;
	}
	
	public void setUrlretorno(String urlretorno) {
		this.urlretorno = urlretorno;
	}
	public void setListaProducaoordemmaterial(List<Producaoordemmaterial> listaProducaoordemmaterial) {
		this.listaProducaoordemmaterial = listaProducaoordemmaterial;
	}
}
