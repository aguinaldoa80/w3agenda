package br.com.linkcom.sined.modulo.producao.controller.crud.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Material;

public class TrocarProdutoFinalBean {
	
	private Material material;
	private String unidadeMedidaStr;
	private Double qtde;
	
	@Required
	@DisplayName("Material")
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Unidade de medida")
	public String getUnidadeMedidaStr() {
		return unidadeMedidaStr;
	}
	@Required
	@DisplayName("Quantidade")
	@MaxLength(9)
	public Double getQtde() {
		return qtde;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setUnidadeMedidaStr(String unidadeMedidaStr) {
		this.unidadeMedidaStr = unidadeMedidaStr;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}	
}
