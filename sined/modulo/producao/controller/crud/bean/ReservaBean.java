package br.com.linkcom.sined.modulo.producao.controller.crud.bean;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Reserva;

public class ReservaBean {

	private List<Reserva> listareserva;
	
	public List<Reserva> getListareserva() {
		return listareserva;
	}
	
	public void setListareserva(List<Reserva> listareserva) {
		this.listareserva = listareserva;
	}
	
}
