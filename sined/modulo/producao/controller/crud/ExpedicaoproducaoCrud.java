package br.com.linkcom.sined.modulo.producao.controller.crud;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Expedicaoproducao;
import br.com.linkcom.sined.geral.bean.Expedicaoproducaohistorico;
import br.com.linkcom.sined.geral.bean.Expedicaoproducaoitem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendahistorico;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialorigem;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoetiqueta;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.ExpedicaoproducaoService;
import br.com.linkcom.sined.geral.service.ExpedicaoproducaohistoricoService;
import br.com.linkcom.sined.geral.service.MovimentacaoEstoqueHistoricoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.ProducaoagendaService;
import br.com.linkcom.sined.geral.service.ProducaoagendahistoricoService;
import br.com.linkcom.sined.geral.service.ProducaoordemmaterialService;
import br.com.linkcom.sined.geral.service.RegiaoService;
import br.com.linkcom.sined.modulo.producao.controller.crud.filter.ExpedicaoproducaoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/producao/crud/Expedicaoproducao", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"cdexpedicaoproducao", "identificadorcarregamento", "dtexpedicao", "transportadora", "motorista", "veiculo", "expedicaosituacao"})
public class ExpedicaoproducaoCrud extends CrudControllerSined<ExpedicaoproducaoFiltro, Expedicaoproducao, Expedicaoproducao> {

	private ExpedicaoproducaoService expedicaoproducaoService;
	private EnderecoService enderecoService;
	private ContatoService contatoService;
	private ExpedicaoproducaohistoricoService expedicaoproducaohistoricoService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private ProducaoordemmaterialService producaoordemmaterialService;
	private ProducaoagendaService producaoagendaService;
	private ProducaoagendahistoricoService producaoagendahistoricoService;
	private ContratoService contratoService;
	private RegiaoService regiaoService;
	private ParametrogeralService parametrogeralService;
	private ClienteService clienteService;
	private MovimentacaoEstoqueHistoricoService movimentacaoEstoqueHistoricoService;
	
	public void setRegiaoService(RegiaoService regiaoService) {
		this.regiaoService = regiaoService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setProducaoagendahistoricoService(
			ProducaoagendahistoricoService producaoagendahistoricoService) {
		this.producaoagendahistoricoService = producaoagendahistoricoService;
	}
	public void setProducaoagendaService(
			ProducaoagendaService producaoagendaService) {
		this.producaoagendaService = producaoagendaService;
	}
	public void setExpedicaoproducaohistoricoService(
			ExpedicaoproducaohistoricoService expedicaoproducaohistoricoService) {
		this.expedicaoproducaohistoricoService = expedicaoproducaohistoricoService;
	}
	public void setExpedicaoproducaoService(ExpedicaoproducaoService expedicaoproducaoService) {
		this.expedicaoproducaoService = expedicaoproducaoService;
	}
	public void setContatoService(ContatoService contatoService) {
		this.contatoService = contatoService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setMovimentacaoestoqueService(
			MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setProducaoordemmaterialService(
			ProducaoordemmaterialService producaoordemmaterialService) {
		this.producaoordemmaterialService = producaoordemmaterialService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setMovimentacaoEstoqueHistoricoService(
			MovimentacaoEstoqueHistoricoService movimentacaoEstoqueHistoricoService) {
		this.movimentacaoEstoqueHistoricoService = movimentacaoEstoqueHistoricoService;
	}
	
	@Override
	protected Expedicaoproducao criar(WebRequestContext request, Expedicaoproducao form) throws Exception {
		Expedicaoproducao bean = super.criar(request, form);
		
		String cdproducaoagenda = request.getParameter("cdproducaoagenda");
		if(cdproducaoagenda != null && !cdproducaoagenda.equals("")){
			Producaoagenda producaoagenda = new Producaoagenda(Integer.parseInt(cdproducaoagenda));
			producaoagenda = producaoagendaService.loadForEntrada(producaoagenda);
			if(producaoagenda.getCliente() != null){
				producaoagenda.setCliente(clienteService.load(producaoagenda.getCliente(), "cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj"));
			}
			expedicaoproducaoService.createByProducaoagenda(bean, producaoagenda);
		}
		
		return bean;
	}
	
	@Override
	protected void listagem(WebRequestContext request,
			ExpedicaoproducaoFiltro filtro) throws Exception {
		request.setAttribute("listaEtiqueta", Tipoetiqueta.getListaTipoetiquetaExpedicao());
		
		List<Expedicaosituacao> listaExpedicaosituacao = new ArrayList<Expedicaosituacao>();
		for (Expedicaosituacao expedicaosituacao : Expedicaosituacao.values()) {
			listaExpedicaosituacao.add(expedicaosituacao);
		}
		request.setAttribute("listaExpedicaosituacao", listaExpedicaosituacao);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, ExpedicaoproducaoFiltro filtro) throws CrudException {
		if(filtro.getRegiao() != null){
			filtro.setRegiao(regiaoService.loadFetch(filtro.getRegiao(), "listaRegiaolocal"));
		}
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Expedicaoproducao bean) throws Exception {
		Producaoagenda producaoagenda = bean.getProducaoagenda();
		boolean isCriar = bean.getCdexpedicaoproducao() == null;
		bean.setIsCriar(isCriar);
		
		super.salvar(request, bean);
		
		if(producaoagenda != null){
			Producaoagendahistorico producaoagendahistorico = new Producaoagendahistorico();
			producaoagendahistorico.setObservacao("Criada a Expedi��o <a href=\"javascript:visualizarExpedicaoproducao(" + bean.getCdexpedicaoproducao() + ");\">" + bean.getCdexpedicaoproducao() + "</a>");
			producaoagendahistorico.setProducaoagenda(producaoagenda);
			
			producaoagendahistoricoService.saveOrUpdate(producaoagendahistorico);
		}
		
		Expedicaoproducaohistorico expedicaoproducaohistorico = new Expedicaoproducaohistorico();
		expedicaoproducaohistorico.setExpedicaoproducao(bean);
		if(producaoagenda != null){
			expedicaoproducaohistorico.setObservacao("Criada a partir da Agenda de Produ��o <a href=\"javascript:visualizarProducaoagenda(" + producaoagenda.getCdproducaoagenda() + ");\">" + producaoagenda.getCdproducaoagenda() + "</a>");
		}
		expedicaoproducaohistorico.setExpedicaoacao(isCriar ? Expedicaoacao.CRIADA : Expedicaoacao.ALTERADA);
		expedicaoproducaohistoricoService.saveOrUpdate(expedicaoproducaohistorico);
	}
	/**
	 * Action em ajax que busca as informa��es do cliente;
	 *
	 * @param request
	 * @param expedicaoproducaoitem
	 * @return
	 * @since 16/07/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView ajaxCarregaInfoCliente(WebRequestContext request, Expedicaoproducaoitem expedicaoproducaoitem){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(expedicaoproducaoitem.getCliente() != null){
			GenericBean genericBean;
			
			List<Contrato> listaContratoBD = contratoService.findBycliente(expedicaoproducaoitem.getCliente());
			List<GenericBean> listaContrato = new ArrayList<GenericBean>();
			for (Contrato contrato : listaContratoBD) {
				genericBean = new GenericBean("br.com.linkcom.sined.geral.bean.Contrato[cdcontrato=" + contrato.getCdcontrato() + "]", contrato.getCdcontrato() + " - " + contrato.getDescricao());
				listaContrato.add(genericBean);
			}
			
			List<Endereco> listaEndereco = enderecoService.findByCliente(expedicaoproducaoitem.getCliente());
			List<GenericBean> listaEnderecocliente = new ArrayList<GenericBean>();
			for (Endereco endereco : listaEndereco) {
				genericBean = new GenericBean("br.com.linkcom.sined.geral.bean.Endereco[cdendereco=" + endereco.getCdendereco() + "]", endereco.getDescricaoCombo());
				listaEnderecocliente.add(genericBean);
			}
			
			List<Contato> listaContato = contatoService.findByPessoa(expedicaoproducaoitem.getCliente());
			List<GenericBean> listaContatocliente = new ArrayList<GenericBean>();
			for (Contato contato : listaContato) {
				genericBean = new GenericBean("br.com.linkcom.sined.geral.bean.Contato[cdpessoa=" + contato.getCdpessoa() + "]", contato.getNome());
				listaContatocliente.add(genericBean);
			}
			
			jsonModelAndView.addObject("listaContrato", listaContrato);
			jsonModelAndView.addObject("listaEnderecocliente", listaEnderecocliente);
			jsonModelAndView.addObject("listaContatocliente", listaContatocliente);
		}
		
		return jsonModelAndView;
	}
	
	/**
	 * Action para confirma��o da expedi��o.
	 *
	 * @see br.com.linkcom.sined.geral.service.ExpedicaoService.findForConfirmacao(String whereIn)
	 * @see br.com.linkcom.sined.geral.service.ExpedicaoService.updateSituacao(String whereIn, Expedicaosituacao situacao)
	 * @see  br.com.linkcom.sined.geral.service.ExpedicaoService.makeMovimentacaoestoqueByExpedicao(List<Movimentacaoestoque> listaMovimentacaoestoque, Expedicao expedicao, Expedicaoitem expedicaoitem, Material material, Double qtde, Movimentacaoestoquetipo movimentacaoestoquetipo)
	 * 
	 * @param request
	 * @return
	 * @since 16/07/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView confirmar(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		try{
			
			boolean notValido = expedicaoproducaoService.haveExpedicaoNotInSituacao(whereIn, Expedicaosituacao.EM_ABERTO);
			
			if(notValido){
				request.addError("Para confirmar a(s) expedi��o(�es) tem que ter situa��o(�es) 'Em ABERTO'.");
				return sendRedirectToAction("listagem");
			}
			
			String paramVendacomexpedicao = parametrogeralService.getValorPorNome(Parametrogeral.VENDA_COM_EXPEDICAO);
			
			List<Expedicaoproducao> listaExpedicaoproducao = expedicaoproducaoService.findForConfirmacao(whereIn);
			List<Expedicaoproducaoitem> listaExpedicaoproducaoitem;
			List<Movimentacaoestoque> listaMovimentacaoestoque = new ArrayList<Movimentacaoestoque>();
			
			if("TRUE".equalsIgnoreCase(paramVendacomexpedicao)){
				for (Expedicaoproducao expedicaoproducao : listaExpedicaoproducao) {
					listaExpedicaoproducaoitem = expedicaoproducao.getListaExpedicaoproducaoitem();
					if(listaExpedicaoproducaoitem != null && listaExpedicaoproducaoitem.size() > 0){
						for (Expedicaoproducaoitem expedicaoproducaoitem : listaExpedicaoproducaoitem) {
//							if(expedicaoproducaoitem.getMaterial().getProducao() != null && expedicaoproducaoitem.getMaterial().getProducao()){
//								Set<Materialproducao> listaProducao = expedicaoproducaoitem.getMaterial().getListaProducao();
//								if(listaProducao != null && listaProducao.size() > 0){
//									for (Materialproducao materialproducao : listaProducao) {
//										expedicaoproducaoService.makeMovimentacaoestoqueByExpedicao(listaMovimentacaoestoque, 
//												expedicaoproducao, expedicaoproducaoitem, materialproducao.getMaterial(), 
//												materialproducao.getConsumo() * expedicaoproducaoitem.getQtdeexpedicaoproducao(), Movimentacaoestoquetipo.SAIDA);
//									}
//									
//									expedicaoproducaoService.makeMovimentacaoestoqueByExpedicao(listaMovimentacaoestoque, 
//											expedicaoproducao, expedicaoproducaoitem, expedicaoproducaoitem.getMaterial(), 
//											expedicaoproducaoitem.getQtdeexpedicaoproducao(), Movimentacaoestoquetipo.ENTRADA);
//									
//									expedicaoproducaoService.makeMovimentacaoestoqueByExpedicao(listaMovimentacaoestoque, 
//											expedicaoproducao, expedicaoproducaoitem, expedicaoproducaoitem.getMaterial(), 
//											expedicaoproducaoitem.getQtdeexpedicaoproducao(), Movimentacaoestoquetipo.SAIDA);
//								}
//							} else {
								expedicaoproducaoService.makeMovimentacaoestoqueByExpedicao(listaMovimentacaoestoque, 
										expedicaoproducao, expedicaoproducaoitem, expedicaoproducaoitem.getMaterial(), 
											expedicaoproducaoitem.getQtdeexpedicaoproducao(), Movimentacaoestoquetipo.SAIDA);
//							}
						}
					}
				}
			}
			
			HashMap<Integer, StringBuilder> mapExpedicaoMovimentacaoestoque = new HashMap<Integer, StringBuilder>();
			if(listaMovimentacaoestoque != null && !listaMovimentacaoestoque.isEmpty()){
				for (Movimentacaoestoque me : listaMovimentacaoestoque) {
					movimentacaoestoqueService.saveOrUpdate(me);
					movimentacaoEstoqueHistoricoService.preencherHistorico(me, MovimentacaoEstoqueAcao.CRIAR, "CRIADO VIA EXPEDI��O DE PRODU��O " + SinedUtil.makeLinkHistorico(me.getMovimentacaoestoqueorigem().getExpedicaoproducao().getCdexpedicaoproducao().toString(), "visualizarExpedicaoProducao"), true);	
					if(me.getMovimentacaoestoqueorigem() != null && me.getMovimentacaoestoqueorigem().getExpedicaoproducao() != null &&
							me.getMovimentacaoestoqueorigem().getExpedicaoproducao().getCdexpedicaoproducao() != null){
						if(mapExpedicaoMovimentacaoestoque.get(me.getMovimentacaoestoqueorigem().getExpedicaoproducao().getCdexpedicaoproducao()) == null){
							mapExpedicaoMovimentacaoestoque.put(me.getMovimentacaoestoqueorigem().getExpedicaoproducao().getCdexpedicaoproducao(), 
									new StringBuilder("<a href=\"javascript:visualizaMovimentacaoestoque("+me.getCdmovimentacaoestoque()+");\">"+me.getCdmovimentacaoestoque()+"</a>"));
						}else {
							mapExpedicaoMovimentacaoestoque.put(me.getMovimentacaoestoqueorigem().getExpedicaoproducao().getCdexpedicaoproducao(),
								mapExpedicaoMovimentacaoestoque.get(me.getMovimentacaoestoqueorigem().getExpedicaoproducao().getCdexpedicaoproducao()).append(
								", <a href=\"javascript:visualizaMovimentacaoestoque("+me.getCdmovimentacaoestoque()+");\">"+me.getCdmovimentacaoestoque()+"</a>")); 
						}
					}
				}
			}
			
			expedicaoproducaoService.updateSituacao(whereIn, Expedicaosituacao.CONFIRMADA);
			expedicaoproducaohistoricoService.saveHistorico(whereIn, "", Expedicaoacao.CONFIRMADA, mapExpedicaoMovimentacaoestoque);
			
			request.addMessage("Expedi��o(��es) confirmada(s) com sucesso."); 
		} catch (Exception e) {
			request.addError(e.getMessage());
			request.addError("Erro na confirma��o da(s) expedi��o(��es).");
		}
		if("true".equals(request.getParameter("entrada"))){
			return new ModelAndView("redirect:/producao/crud/Expedicaoproducao?ACAO=consultar&cdexpedicaoproducao=" + whereIn);
		} else return sendRedirectToAction("listagem");
	}
	
	/**
	 * Ajax que carrega um producaoordemmaterial a partir de uma etiqueta com c�digo de barras.
	 * 
	 * @param request
	 * @param etiqueta
	 * @return
	 * @author Rafael Salvio
	 */
	public ModelAndView ajaxLoadEtiqueta(WebRequestContext request){
		String etiqueta = request.getParameter("etiqueta");
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(etiqueta == null || etiqueta.trim().isEmpty()){
			jsonModelAndView.addObject("msgErro", "Etiqueta n�o preenchida.");
		}
		else{
			Producaoordemmaterial pom = producaoordemmaterialService.loadByEtiqueta(Integer.parseInt(etiqueta));
			
			if(pom == null){
				jsonModelAndView.addObject("msgErro", "Etiqueta inv�lida.");
			}
			else{
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				GenericBean bean;
				
				if(pom.getListaProducaoordemmaterialorigem() != null && !pom.getListaProducaoordemmaterialorigem().isEmpty()){
					Producaoordemmaterialorigem pomo = pom.getListaProducaoordemmaterialorigem().iterator().next();
					if(pomo.getProducaoagenda() != null){
						
						if(pomo.getProducaoagenda().getContrato() != null){
							String contratoStr = "[cdcontrato=" + pomo.getProducaoagenda().getContrato().getCdcontrato() + "]";
							bean = new GenericBean(Contrato.class.getName() + contratoStr, pomo.getProducaoagenda().getContrato().getDescricao());
							jsonModelAndView.addObject("contrato", bean);
						}
						
						if(pomo.getProducaoagenda().getCliente() != null){
							String clienteStr = "[cdpessoa=" + pomo.getProducaoagenda().getCliente().getCdpessoa() + 
												",nome=" + pomo.getProducaoagenda().getCliente().getNome() + "]";
							bean = new GenericBean(Cliente.class.getName() + clienteStr, pomo.getProducaoagenda().getCliente().getNomeCpfOuCnpj());
							jsonModelAndView.addObject("cliente", bean);
						}
					}
				}
				
				String materialStr = "[cdmaterial=" + pom.getMaterial().getCdmaterial();
				if(pom.getMaterial().getIdentificacao() != null)
					materialStr +=	",identificacao=" + pom.getMaterial().getIdentificacao();
				materialStr += ",nome=" + pom.getMaterial().getNome() + "]";
				bean = new GenericBean(Material.class.getName() + materialStr, pom.getMaterial().getNome());
				jsonModelAndView.addObject("produto", bean);
				jsonModelAndView.addObject("qtde", pom.getQtde());
				jsonModelAndView.addObject("qtdeproduzida", pom.getQtdeproduzido() != null ? pom.getQtdeproduzido() : 0);
				jsonModelAndView.addObject("dtexpedicao", sdf.format(pom.getProducaoordem().getDtprevisaoentrega()));
			}
		}
		
		return jsonModelAndView;
	}
}
