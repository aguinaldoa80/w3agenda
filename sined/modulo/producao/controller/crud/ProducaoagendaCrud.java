package br.com.linkcom.sined.modulo.producao.controller.crud;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratohistorico;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendahistorico;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterialmateriaprima;
import br.com.linkcom.sined.geral.bean.Producaoagendatipo;
import br.com.linkcom.sined.geral.bean.Producaoetapa;
import br.com.linkcom.sined.geral.bean.Reserva;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.BaixarEstoqueMateriasPrimasEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoagendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoordemsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.geral.bean.enumeration.SitucaoPneuEtiqueteEnum;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.ContratohistoricoService;
import br.com.linkcom.sined.geral.service.InventarioService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialproducaoService;
import br.com.linkcom.sined.geral.service.MaterialunidademedidaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PneuService;
import br.com.linkcom.sined.geral.service.ProducaoagendaService;
import br.com.linkcom.sined.geral.service.ProducaoagendahistoricoService;
import br.com.linkcom.sined.geral.service.ProducaoagendaitemadicionalService;
import br.com.linkcom.sined.geral.service.ProducaoagendamaterialService;
import br.com.linkcom.sined.geral.service.ProducaoagendatipoService;
import br.com.linkcom.sined.geral.service.ProducaochaofabricaService;
import br.com.linkcom.sined.geral.service.ProducaoetapaService;
import br.com.linkcom.sined.geral.service.ProducaoordemService;
import br.com.linkcom.sined.geral.service.ReservaService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ContratoJson;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.DadosEtiquetaProducaoBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.Materiaprima;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ProducaoSobraBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ReservaBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.filter.ProducaoagendaFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@Controller(path="/producao/crud/Producaoagenda", authorizationModule=CrudAuthorizationModule.class)
public class ProducaoagendaCrud extends CrudControllerSined<ProducaoagendaFiltro, Producaoagenda, Producaoagenda>{
	
	private ProducaoagendahistoricoService producaoagendahistoricoService;
	private ContratoService contratoService;
	private PedidovendaService pedidovendaService;
	private ContareceberService contareceberService;
	private ProducaoagendaService producaoagendaService;
	private ProducaoagendamaterialService producaoagendamaterialService;
	private ContratohistoricoService contratohistoricoService;
	private MaterialService materialService;
	private MaterialproducaoService materialproducaoService;
	private VendaService vendaService;
	private ProducaoagendatipoService producaoagendatipoService;
	private ProducaoordemService producaoordemService;
	private ParametrogeralService parametrogeralService;
	private UnidademedidaService unidademedidaService;
	private ReservaService reservaService;
	private PneuService pneuService;
	private ProducaoetapaService producaoetapaService;
	private ProducaochaofabricaService producaochaofabricaService;
	private ClienteService clienteService;
	private MaterialunidademedidaService materialunidademedidaService;
	private ProducaoagendaitemadicionalService producaoagendaitemadicionalService;
	private ReportTemplateService reportTemplateService;
	private InventarioService inventarioService;
	
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {this.reportTemplateService = reportTemplateService;}
	public void setProducaochaofabricaService(
			ProducaochaofabricaService producaochaofabricaService) {
		this.producaochaofabricaService = producaochaofabricaService;
	}
	public void setPneuService(PneuService pneuService) {
		this.pneuService = pneuService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setProducaoordemService(
			ProducaoordemService producaoordemService) {
		this.producaoordemService = producaoordemService;
	}
	public void setProducaoagendaService(
			ProducaoagendaService producaoagendaService) {
		this.producaoagendaService = producaoagendaService;
	}
	public void setContareceberService(ContareceberService contareceberService) {
		this.contareceberService = contareceberService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setProducaoagendahistoricoService(
			ProducaoagendahistoricoService producaoagendahistoricoService) {
		this.producaoagendahistoricoService = producaoagendahistoricoService;
	}
	public void setProducaoagendamaterialService(ProducaoagendamaterialService producaoagendamaterialService) {
		this.producaoagendamaterialService = producaoagendamaterialService;
	}
	public void setContratohistoricoService(ContratohistoricoService contratohistoricoService) {
		this.contratohistoricoService = contratohistoricoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setMaterialproducaoService(MaterialproducaoService materialproducaoService) {
		this.materialproducaoService = materialproducaoService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setProducaoagendatipoService(ProducaoagendatipoService producaoagendatipoService) {
		this.producaoagendatipoService = producaoagendatipoService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setReservaService(ReservaService reservaService) {
		this.reservaService = reservaService;
	}
	public void setProducaoetapaService(ProducaoetapaService producaoetapaService) {
		this.producaoetapaService = producaoetapaService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setMaterialunidademedidaService(MaterialunidademedidaService materialunidademedidaService) {
		this.materialunidademedidaService = materialunidademedidaService;
	}
	public void setProducaoagendaitemadicionalService(ProducaoagendaitemadicionalService producaoagendaitemadicionalService) {
		this.producaoagendaitemadicionalService = producaoagendaitemadicionalService;
	}
	public void setInventarioService(InventarioService inventarioService) {
		this.inventarioService = inventarioService;
	}
	
	@Override
	protected Producaoagenda criar(WebRequestContext request, Producaoagenda form) throws Exception {
		if(SinedUtil.isAmbienteDesenvolvimento() && "true".equals(request.getParameter("copiar")) && form.getCdproducaoagenda() != null){
			form = producaoagendaService.criarCopia(form);
			return form;
		}
		return super.criar(request, form);
	}
	
	public ModelAndView emitirEtiqueta (WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		List<DadosEtiquetaProducaoBean> listaBean = new ArrayList<DadosEtiquetaProducaoBean>();
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		List<Producaoagenda> listaProducao = producaoagendaService.findForProducao(whereIn);
		for (Producaoagenda agenda : listaProducao) {
			if(SinedUtil.isListNotEmpty(agenda.getListaProducaoagendamaterial())){		
				for (Producaoagendamaterial material: agenda.getListaProducaoagendamaterial()) {
					if(material.getPneu() != null){
						SitucaoPneuEtiqueteEnum situacao = producaoordemService.pneuProduzidoOuCancelado(material.getPneu(),producaoetapaService.getUltimaEtapa(material.getProducaoetapa()));
						if(!SitucaoPneuEtiqueteEnum.INCOMPLETO.equals(situacao)){
							DadosEtiquetaProducaoBean bean = new DadosEtiquetaProducaoBean();
							bean.setCdProgucaoAgenda(agenda.getCdproducaoagenda());
							bean.setPneu(pneuService.loadForEntrada(material.getPneu()));
							bean.setSituacao(situacao);
							bean.setCliente(agenda.getCliente());
							if(agenda.getPedidovenda()!=null){
								bean.setCdPedidoVenda(agenda.getPedidovenda().getCdpedidovenda());
								bean.setIndentificacaoExterno(agenda.getPedidovenda().getIdentificacaoexterna());
							}
							listaBean.add(bean);
						}
					}
				}
			}
		}
		if(SinedUtil.isListEmpty(listaBean)){
			SinedUtil.fechaPopUp(request);
			request.addError("N�o � poss�vel emitir etiqueta de pneu conclu�do da(s) agenda(s) . As agendas selecionadas n�o possuem registro de produ��o conclu�da.");
			return null;
		}
		ReportTemplateBean template = reportTemplateService.loadTemplateByCategoria(EnumCategoriaReportTemplate.ETIQUETA_DO_PNEU);
		if(template == null){
			SinedUtil.fechaPopUp(request);
			request.addError("N�o existe nenhum templete de relat�rio da categoria Etiqueta do Pneu Produzido/Recusado cadastrado.");
			return null;
		}
		
		
		request.setAttribute("consultar", true);
		request.setAttribute("ocultarDescricao", true);
		request.setAttribute("ignorarPagina��o", true);
		request.setAttribute("listaItens", listaBean);
		return new ModelAndView("direct:/crud/popup/popupEmisaoEtiqueta", "bean", new DadosEtiquetaProducaoBean());
	}
	
	
	@Override
	protected void entrada(WebRequestContext request, Producaoagenda form) throws Exception {
		
		String obrigarLoteProducao = parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_LOTE_PRODUCAO);
		String exibirCamposAgendaProducao = parametrogeralService.getValorPorNome(Parametrogeral.EXIBIR_CAMPOS_AGENDAPRODUCAO);
		
		request.setAttribute("isRecapagem", SinedUtil.isRecapagem());
		
		request.setAttribute("OBRIGAR_LOTE_PRODUCAO", obrigarLoteProducao);
		request.setAttribute("EXIBIR_CAMPOS_AGENDAPRODUCAO", exibirCamposAgendaProducao);
		request.setAttribute("AGENDAPRODUCAO_DATAENTREGA", Boolean.TRUE.equals(parametrogeralService.getBoolean(Parametrogeral.AGENDAPRODUCAO_DATAENTREGA)));
		
		if(form.getCdproducaoagenda() != null){
			if(form.getCliente() != null){
				form.setCliente(clienteService.load(form.getCliente(), "cliente.cdpessoa, cliente.nome, cliente.cpf, cliente.cnpj"));
			}
			
			List<Reserva> listaReservas = reservaService.findByProducaoagenda(form.getCdproducaoagenda().toString());
			form.setExistereserva(SinedUtil.isListNotEmpty(listaReservas));
			
			form.setListaProducaoagendahistorico(producaoagendahistoricoService.findByProducaoagenda(form));
			producaoagendaService.carregarProducaoagendamaterialmateriaprima(form);
			
			boolean podeEditar = true;
			if(form.getProducaoagendasituacao() != null && !form.getProducaoagendasituacao().equals(Producaoagendasituacao.EM_ESPERA)){
				if(form.getProducaoagendasituacao().equals(Producaoagendasituacao.EM_ANDAMENTO)){
					boolean haveBaixaAposProducao = false;
					Set<Producaoagendamaterial> listaProducaoagendamaterial = form.getListaProducaoagendamaterial();
					if(listaProducaoagendamaterial != null && listaProducaoagendamaterial.size() > 0){
						for (Producaoagendamaterial producaoagendamaterial : listaProducaoagendamaterial) {
							if(producaoagendamaterial.getProducaoetapa() != null){
								if(producaoagendamaterial.getProducaoetapa().getBaixarEstoqueMateriaprima() == null){
									producaoagendamaterial.setProducaoetapa(producaoetapaService.load(producaoagendamaterial.getProducaoetapa()));
								}
								if(producaoagendamaterial.getProducaoetapa() != null && producaoagendamaterial.getProducaoetapa().getBaixarEstoqueMateriaprima() != null &&
									producaoagendamaterial.getProducaoetapa().getBaixarEstoqueMateriaprima().equals(BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA)){
									haveBaixaAposProducao = true;
									break;
								}
							}
						}
					}
					podeEditar = haveBaixaAposProducao;
					request.setAttribute("haveBaixaAposProducao", haveBaixaAposProducao);
				} else {
					podeEditar = false;
				}
			}
			request.setAttribute("podeEditar", podeEditar);
			
			boolean consultar = request.getAttribute(CONSULTAR) != null && (Boolean)request.getAttribute(CONSULTAR);
			if(!consultar){
				if(!podeEditar && form.getProducaoagendasituacao() != null && !form.getProducaoagendasituacao().equals(Producaoagendasituacao.EM_ESPERA)){
					throw new SinedException("N�o � poss�vel editar um registro de agenda de produ��o com situa��o diferente de 'EM ESPERA'.");
				}
				
				if (form.getExistereserva()) {
					throw new SinedException("N�o � poss�vel alterar uma agenda com reserva. � necess�rio cancelar a reserva para alterar a agenda de produ��o.");
				}
			}
			
			if(form.getContrato() != null && form.getContrato().getCdcontrato() != null){
				form.setContrato(contratoService.loadWithContratotipo(form.getContrato()));
			}
			if(form.getPedidovenda() != null && form.getPedidovenda().getCdpedidovenda() != null){
				form.setPedidovenda(pedidovendaService.load(form.getPedidovenda(), "pedidovenda.cdpedidovenda, pedidovenda.dtpedidovenda, pedidovenda.origemOtr"));
			}
			if(form.getVenda() != null && form.getVenda().getCdvenda() != null){
				form.setVenda(vendaService.load(form.getVenda(), "venda.cdvenda, venda.dtvenda"));
			}
			
			Set<Producaoagendamaterial> listaProducaoagendamaterial = form.getListaProducaoagendamaterial();
			if(listaProducaoagendamaterial != null && listaProducaoagendamaterial.size() > 0){
				for (Producaoagendamaterial producaoagendamaterial : listaProducaoagendamaterial) {
					if(producaoagendamaterial.getPneu() != null && producaoagendamaterial.getPneu().getCdpneu() != null){
						producaoagendamaterial.setPneu(pneuService.loadForEntrada(producaoagendamaterial.getPneu()));
					}
				}
			}
			
			form.setListaProducaoagendaitemadicional(producaoagendaitemadicionalService.findByProducaoagenda(form));
		}
		producaoagendaService.setAtributeDimensaoproducao(request);
	}
	
	@Override
	protected void listagem(WebRequestContext request, ProducaoagendaFiltro filtro) throws Exception {
		// MANDA PARA A TELA DE LISTAGEM AS SITUA��ES
		List<Producaoagendasituacao> listaProducaoagendasituacaoCompleta = new ArrayList<Producaoagendasituacao>();
		Producaoagendasituacao[] producaoagendasituacaoArray = Producaoagendasituacao.values();
		for (int i = 0; i < producaoagendasituacaoArray.length; i++) {
			listaProducaoagendasituacaoCompleta.add(producaoagendasituacaoArray[i]);
		}
		request.setAttribute("isRecapagem", SinedUtil.isRecapagem());
		request.setAttribute("listaProducaoagendasituacaoCompleta", listaProducaoagendasituacaoCompleta);
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Producaoagenda form) throws CrudException {
		String whereIn = request.getParameter("itenstodelete");
		
		if(whereIn != null && !"".equals(whereIn) || form.getCdproducaoagenda() != null){
			List<Reserva> listaReservas = null;
			
			if(form.getCdproducaoagenda() != null){
				listaReservas = reservaService.findByProducaoagenda(form.getCdproducaoagenda().toString());
			} else {
				listaReservas = reservaService.findByProducaoagenda(whereIn);
			}
			
			form.setExistereserva(SinedUtil.isListNotEmpty(listaReservas));
			
			if (form.getExistereserva()) {
				throw new SinedException("N�o � poss�vel Alterar ou Excluir uma agenda com reserva.");
			}
		}
		
		if(whereIn != null && !"".equals(whereIn) || form.getCdproducaoagenda() != null){
			List<Venda> listaVenda = null;
			if(form.getCdproducaoagenda() != null){
				listaVenda = vendaService.findByProducaoagenda(form.getCdproducaoagenda().toString());
			} else {
				listaVenda = vendaService.findByProducaoagenda(whereIn);
			}
			if(listaVenda != null && !listaVenda.isEmpty()){
				request.setAttribute("listaVendaForUpdateSituacaoEmProducao", listaVenda);
			}
		}
		return super.doExcluir(request, form);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected ModelAndView getExcluirModelAndView(WebRequestContext request, Producaoagenda bean) {
		if(request.getAttribute("listaVendaForUpdateSituacaoEmProducao") != null){
			try {
				List<Venda> listaVenda = (List<Venda>) request.getAttribute("listaVendaForUpdateSituacaoEmProducao");
				if(listaVenda != null && !listaVenda.isEmpty()){
					StringBuilder whereInVenda = new StringBuilder();
					for(Venda venda : listaVenda){
						if(venda.getVendasituacao() != null && Vendasituacao.REALIZADA.equals(venda.getVendasituacao())){
							if(!whereInVenda.toString().equals("")) whereInVenda.append(",");
							whereInVenda.append(venda.getCdvenda());
						}
					}
					if(!whereInVenda.toString().equals("")){
						vendaService.updateSituacaoVenda(whereInVenda.toString(), Vendasituacao.EMPRODUCAO);
					}
				}
			} catch (Exception e) {
				request.addMessage("N�o foi poss�vel alterar a situa��o da venda.", MessageType.INFO);
			}
		}
		return super.getExcluirModelAndView(request, bean);
	}
	
	@Override
	protected void validateBean(Producaoagenda bean, BindException errors) {
		producaoagendaService.validaObrigatoriedadePneu(bean, null, errors);
		super.validateBean(bean, errors);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Producaoagenda bean) throws Exception {
		boolean isCriar = bean.getCdproducaoagenda() == null;
		
		List<Material> listaExcluidos = new ArrayList<Material>();
		if(!isCriar && bean.getContrato() != null){
			List<Producaoagendamaterial> listaItens = producaoagendamaterialService.findByProducaoagenda(bean);
			boolean itemexcluido = false;
			
			if(listaItens != null && !listaItens.isEmpty()){
				for(Producaoagendamaterial producaoagendamaterial : listaItens){
					itemexcluido = true;
					if(bean.getListaProducaoagendamaterial() != null && !bean.getListaProducaoagendamaterial().isEmpty()){
						for(Producaoagendamaterial pgm_novo : bean.getListaProducaoagendamaterial()){
							if(producaoagendamaterial.getMaterial().equals(pgm_novo.getMaterial())){
								itemexcluido = false;
								break;
							}
						}
					}
					if(itemexcluido){
						if(!listaExcluidos.contains(producaoagendamaterial.getMaterial())){
							listaExcluidos.add(producaoagendamaterial.getMaterial());
						}
					}
				}
			}
		}
		
		List<Pedidovendamaterial> listaPedidovendamaterialBandaAlterada = new ArrayList<Pedidovendamaterial>();
		if(bean.getListaProducaoagendamaterial() != null && bean.getListaProducaoagendamaterial().size() > 0){
			for (Producaoagendamaterial item : bean.getListaProducaoagendamaterial()) {
				if(item.getPneu() != null){
					if(item.getPneu().existeDados()){
						if(item.getPedidovendamaterial() != null && item.getPneu().getCdpneu() != null && pneuService.bandaAlterada(item.getPneu(), item.getPneu().getMaterialbanda())){
							listaPedidovendamaterialBandaAlterada.add(item.getPedidovendamaterial());
						}
						pneuService.saveOrUpdate(item.getPneu());
					} else {
						item.setPneu(null);
					}
				}
			}
		}
		
		super.salvar(request, bean);
		
		// SALVAR HIST�RICO
		Producaoagendahistorico producaoagendahistorico = new Producaoagendahistorico();
		producaoagendahistorico.setProducaoagenda(bean);
		if(isCriar){
			producaoagendahistorico.setObservacao("Criado");
		} else {
			producaoagendahistorico.setObservacao("Alterado");
		}
		producaoagendahistoricoService.saveOrUpdate(producaoagendahistorico);
		
		if(listaExcluidos != null && !listaExcluidos.isEmpty()){
			StringBuilder itensExcluidos = new StringBuilder();
			for(Material material : listaExcluidos){
				if(!"".equals(itensExcluidos.toString())) itensExcluidos.append(",");
				itensExcluidos.append(material.getNome());
			}
			Contratohistorico contratohistorico = new Contratohistorico();
			contratohistorico.setContrato(bean.getContrato());
			contratohistorico.setObservacao("Iten(s) exclu�do(s) da Agenda de produ��o (<a href=\"javascript:visualizaProducaoagenda(" + bean.getCdproducaoagenda() + ");\">" + bean.getCdproducaoagenda() + "</a>) : " + itensExcluidos.toString());
			contratohistorico.setDthistorico(new Timestamp(System.currentTimeMillis()));
			contratohistorico.setUsuario(SinedUtil.getUsuarioLogado());
			
			contratohistoricoService.saveOrUpdate(contratohistorico);
		}
		
		pedidovendaService.updateBandaAlterada(listaPedidovendamaterialBandaAlterada);
	}	
	
	@Override
	protected ListagemResult<Producaoagenda> getLista(WebRequestContext request, ProducaoagendaFiltro filtro) {
		ListagemResult<Producaoagenda> listagemResult = super.getLista(request, filtro);
		List<Producaoagenda> lista = listagemResult.list();
		
		String whereInCliente = SinedUtil.listAndConcatenate(lista, "cliente.cdpessoa", ",");
		List<Object[]> listaClientePendencia = null;
		if(whereInCliente != null && !whereInCliente.equals("")){
			listaClientePendencia = contareceberService.verificaPendenciaFinanceiraByClientes(whereInCliente);
		}
		
		String whereInProducaoagenda = SinedUtil.listAndConcatenate(lista, "cdproducaoagenda", ",");
		List<Producaoagenda> listaProducaoagenda = null;
		List<Reserva> listaReservas = null;
		if(whereInProducaoagenda != null && !whereInProducaoagenda.equals("")){
			listaProducaoagenda = producaoagendaService.findForVerificacaoSolicitacaocompra(whereInProducaoagenda);
			listaReservas = reservaService.findByProducaoagenda(whereInProducaoagenda);
		}
		
		for (Producaoagenda producaoagenda : lista) {
			if (SinedUtil.isListNotEmpty(listaReservas)) {
				for (Reserva reserva : listaReservas) {
					if (reserva != null && reserva.getProducaoagenda() != null) {
						if (reserva.getProducaoagenda().equals(producaoagenda)) {
							producaoagenda.setExistereserva(Boolean.TRUE);
							break;
						}
					}
				}
			}
		}
		
		for (Producaoagenda producaoagenda : lista) {
			if(producaoagenda.getProducaoagendasituacao() != null &&
					producaoagenda.getContrato() != null &&
					producaoagenda.getContrato().getAux_contrato() != null &&
					producaoagenda.getContrato().getAux_contrato().getSituacao() != null &&
					!producaoagenda.getProducaoagendasituacao().equals(Producaoagendasituacao.CANCELADA) &&
					producaoagenda.getContrato().getAux_contrato().getSituacao().equals(SituacaoContrato.CANCELADO)){
				producaoagenda.setContratocancelado(Boolean.TRUE);
			}
			
			if(listaProducaoagenda.contains(producaoagenda)){
				producaoagenda.setComprasolicitada(Boolean.TRUE);
			}
			
			if(listaClientePendencia != null && producaoagenda.getCliente() != null){
				for (Object[] objects : listaClientePendencia) {
					if(objects.length >= 2 && 
							objects[0] != null && 
							objects[1] != null && 
							objects[0].equals(producaoagenda.getCliente().getCdpessoa())){
						producaoagenda.setPendenciafinanceira((Boolean)objects[1]);
					}
				}				
			}
		}
		
		return listagemResult;
	}
	
	/**
	 * Action em ajax para buscar as informa��es do cliente
	 *
	 * @param request
	 * @param producaoagenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public ModelAndView carregaInfoClienteAjax(WebRequestContext request, Producaoagenda producaoagenda){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		String paramFiltro = request.getParameter("filtro");
		boolean fromFiltro = paramFiltro != null && paramFiltro.trim().toUpperCase().equals("TRUE");
		
		if(producaoagenda.getCliente() != null){
			// BUSCA A LISTA DE CONTRATO DO CLIENTE
			List<Contrato> listaContrato = contratoService.findByClienteForProducaoAgenda(producaoagenda.getCliente());
			List<ContratoJson> listaContratoJson = new ArrayList<ContratoJson>();
			for (Contrato contrato : listaContrato) {
				listaContratoJson.add(new ContratoJson(contrato));
			}
			jsonModelAndView.addObject("listaContrato", listaContratoJson);
			
			if(!fromFiltro){
				// BUSCA A LISTA DE PEDIDO DE VENDA DO CLIENTE
				List<Pedidovenda> listaPedidovenda = pedidovendaService.findByCliente(producaoagenda.getCliente());
				List<GenericBean> listaPedidovendaJson = new ArrayList<GenericBean>();
				for (Pedidovenda pedidovenda : listaPedidovenda) {
					listaPedidovendaJson.add(new GenericBean(pedidovenda.getCdpedidovenda(), 
							pedidovenda.getCdpedidovenda() + " - " + SinedDateUtils.toString(pedidovenda.getDtpedidovenda())));
				}
				jsonModelAndView.addObject("listaPedidovenda", listaPedidovendaJson);
			}
		}
		return jsonModelAndView;
	}
	
	/**
	 * Action que faz a valida��o e redireciona para a tela de solicita��o de compra
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public ModelAndView solicitarCompra(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		boolean erro = false;
		
		List<Producaoagenda> listaProducaoagenda = producaoagendaService.findWithSituacao(whereIn);
		for (Producaoagenda producaoagenda : listaProducaoagenda) {
			if(producaoagenda.getProducaoagendasituacao() == null || 
					!producaoagenda.getProducaoagendasituacao().equals(Producaoagendasituacao.EM_ESPERA)){
				request.addError("Somente para situa��o Em espera.");
				erro = true;
			}
		}
		
		// SE DER ERRO REDIRECIONA PARA A TELA DE LISTAGEM
		if(erro) return sendRedirectToAction("listagem");
		
		String valor = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.SOLICITARCOMPRAPRODUCAO);
		
		if(valor != null && !valor.isEmpty()){
			if (valor.equals("REQUISICAOMATERIAL")){
				return new ModelAndView("redirect:/suprimento/crud/Requisicaomaterial?ACAO=criar&idsproducaoagenda=" + whereIn);
			}
			else if (valor.equals("ORDEMCOMPRA")){
				return new ModelAndView("redirect:/suprimento/crud/Ordemcompra?ACAO=criar&idsproducaoagenda=" + whereIn);
			}
		}
		return new ModelAndView("redirect:/suprimento/crud/Solicitacaocompra?ACAO=criar&idsproducaoagenda=" + whereIn);
	}	
	
	/**
	 * Action que faz a valida��o
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public ModelAndView produzir(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		boolean erro = false;
		boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		
		List<Producaoagenda> listaProducaoagenda = producaoagendaService.findWithProducaoetapaAndSituacao(whereIn);
		for (Producaoagenda producaoagenda : listaProducaoagenda) {
			if(producaoagenda.getProducaoagendasituacao() == null || 
					!producaoagenda.getProducaoagendasituacao().equals(Producaoagendasituacao.EM_ESPERA)){
				request.addError("Somente para situa��o Em espera.");
				erro = true;
			}
			
			if (producaoagenda.getProducaoagendatipo() == null || (producaoagenda.getProducaoagendatipo() != null && producaoagenda.getProducaoagendatipo().getProducaoetapa() == null)) {
				if (SinedUtil.isListNotEmpty(producaoagenda.getListaProducaoagendamaterial())) {
					for (Producaoagendamaterial producaoagendamaterial : producaoagenda.getListaProducaoagendamaterial()) {
						if (producaoagendamaterial.getProducaoetapa() == null && producaoagendamaterial.getMaterial() != null && producaoagendamaterial.getMaterial().getProducaoetapa() == null) {
							request.addError("Agenda de produ��o sem etapas de produ��o.");
							erro = true;
						}
						
						if (producaoagendamaterial.getProducaoetapa() != null && BaixarEstoqueMateriasPrimasEnum.AO_PRODUZIR_AGENDA.equals(producaoagendamaterial.getProducaoetapa().getBaixarEstoqueMateriaprima())) {
							if (inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(SinedDateUtils.currentDate(), producaoagenda.getEmpresa(), 
									producaoagenda.getLocalbaixamateriaprima(), producaoagenda.getProjeto())) {
								request.addError("N�o � poss�vel realizar criar ou alterar a movimenta��o de estoque,  pois o per�odo est� vinculado � um registro de invent�rio. " +
										"Para que consiga prosseguir com a a��o � necess�rio cancelar o invent�rio.");
								erro = true;
							}
						}
					}
				} else {
					request.addError("Agenda de produ��o sem materiais para produ��o.");
					erro = true;
				}
			}
		}
		
		// SE DER ERRO REDIRECIONA PARA A TELA DE LISTAGEM
		if(erro) return sendRedirectToAction(entrada ? "consultar&cdproducaoagenda="+whereIn : "listagem");
		
		if(SinedUtil.isRecapagem()){
			return new ModelAndView("redirect:/producao/crud/Producaoordem?ACAO=produzirAutomatico&selectedItens=" + whereIn + "&entrada="+entrada);
		} else {
			return new ModelAndView("redirect:/producao/crud/Producaoordem?ACAO=conferenciaProducao&selectedItens=" + whereIn + "&entrada="+entrada);
		}
	}
	
	/**
	 * M�todo que exibe a popup para simular qtde a produzir
	 * @param request
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 * @since 08/01/2014
	 */
	public ModelAndView abrirMateriaprima(WebRequestContext request, Material bean){
		Material material = materialService.loadMaterialComMaterialproducao(bean);
		if(material != null){
			material.setProduto_largura(bean.getProduto_largura());
			material.setProduto_altura(bean.getProduto_altura());
			material.setProduto_comprimento(bean.getProduto_comprimento());
			material.setProduto_qtde(bean.getProduto_qtde());
			material.setProducaoagendatrans(bean.getProducaoagendatrans());
			material.setPneu(bean.getPneu());
			if(bean.getPneu() != null && bean.getPneu().getMaterialbanda() != null){
				Material materialbanda = materialService.load(bean.getPneu().getMaterialbanda(), "material.cdmaterial, material.nome, material.identificacao");
				if(materialbanda != null){
					bean.getPneu().getMaterialbanda().setNome(materialbanda.getNome());
					bean.getPneu().getMaterialbanda().setIdentificacao(materialbanda.getIdentificacao());
				}
			}
			
			List<Materiaprima> listaMateriaprima = bean.getListaMateriaprima();
			
			if(listaMateriaprima == null || listaMateriaprima.isEmpty()){
				material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), false));
				
				try{
					material.setProduto_altura(bean.getProduto_altura());
					material.setProduto_largura(bean.getProduto_largura());
					material.setQuantidade(bean.getProduto_qtde());
					material.setProducaoagendatrans(bean.getProducaoagendatrans());
					materialproducaoService.getValorvendaproducao(material);			
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				for (Materialproducao materialproducao : material.getListaProducao()) {
					if(materialproducao.getMaterial() != null){
						materialproducao.setUnidademedida(materialproducao.getMaterial().getUnidademedida());
					}
					if(materialproducao.getConsumo() != null && materialproducao.getConsumo() > 0 && bean.getProduto_qtde() != null){
						materialproducao.setConsumo(materialproducao.getConsumo() * bean.getProduto_qtde());
					}
				}
				listaMateriaprima = producaoagendaService.createListaMateriaprima(material.getListaProducao(), material.getPneu(), null, true);
			}
			material.setListaMateriaprima(listaMateriaprima);
			producaoagendaService.verificaTrocaMaterialsimilar(material.getListaMateriaprima());
		}
		request.setAttribute("index", request.getParameter("index"));
		request.setAttribute("consultar", Boolean.valueOf(request.getParameter("consultar") != null ? request.getParameter("consultar") : "false"));
		
		String obrigarLoteProducao = parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_LOTE_PRODUCAO);
		String exibirCamposAgendaProducao = parametrogeralService.getValorPorNome(Parametrogeral.EXIBIR_CAMPOS_AGENDAPRODUCAO);
		
		if (StringUtils.isEmpty(obrigarLoteProducao)) {
			obrigarLoteProducao = "FALSE";
		}
		
		if (StringUtils.isEmpty(exibirCamposAgendaProducao)) {
			exibirCamposAgendaProducao = "FALSE";
		}
		
		request.setAttribute("OBRIGAR_LOTE_PRODUCAO", new Boolean(Boolean.valueOf(obrigarLoteProducao)));
		request.setAttribute("EXIBIR_CAMPOS_AGENDAPRODUCAO", new Boolean(Boolean.valueOf(exibirCamposAgendaProducao)));
		request.setAttribute("URL_TELA", "/" + SinedUtil.getContexto() + "/producao/crud/Producaoagenda");
		request.setAttribute("METODO_AUTOCOMPLETE_LOTE", "loteestoqueService.findQtdeLoteestoqueForAutocompleteProducaoagenda");
		request.setAttribute("RECALCULAR_QUANTIDADES_ORDEMPRODUCAO", parametrogeralService.getBoolean(Parametrogeral.RECALCULAR_QUANTIDADES_ORDEMPRODUCAO));
		return new ModelAndView("direct:/crud/popup/popUpVisualizarmateriaprima", "bean", material);
	}
	
	/**
	 * Action que gera o CSV de acordo com o filtro selecionado.
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public ModelAndView gerarCsv(WebRequestContext request, ProducaoagendaFiltro filtro){
		return new ResourceModelAndView(producaoagendaService.makeCSVListagem(filtro));
	}
	
	public ModelAndView buscarInfMaterialAjax(WebRequestContext request, Material material){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		material = materialService.loadWithProducaoetapa(material);
		jsonModelAndView.addObject("producaoetapa", material != null && material.getProducaoetapa() != null ?
				"br.com.linkcom.sined.geral.bean.Producaoetapa[cdproducaoetapa=" + material.getProducaoetapa().getCdproducaoetapa() + "]" : null );
		
		jsonModelAndView.addObject("unidademedida", material != null && material.getUnidademedida() != null ?
				"br.com.linkcom.sined.geral.bean.Unidademedida[cdunidademedida=" +  material.getUnidademedida().getCdunidademedida() + "]" : null );
		
		jsonModelAndView.addObject("unidademedidaNome", material != null && material.getUnidademedida() != null ? material.getUnidademedida().getNome() : null );
		jsonModelAndView.addObject("altura", material != null && material.getMaterialproduto() != null ? material.getMaterialproduto().getAltura() : null );
		jsonModelAndView.addObject("largura", material != null && material.getMaterialproduto() != null ? material.getMaterialproduto().getLargura() : null );
		jsonModelAndView.addObject("comprimento", material != null && material.getMaterialproduto() != null ? material.getMaterialproduto().getComprimento() : null );
		jsonModelAndView.addObject("nomeBanda", material != null && material.getNomeBanda() != null ? material.getNomeBanda() : null);
		jsonModelAndView.addObject("profundidadesulcoBanda", material != null && material.getProfundidadesulcoBanda() != null ? material.getProfundidadesulcoBanda() : null);
		
		return jsonModelAndView;
	}
	
	public ModelAndView buscarInfProducaoagendatipo(WebRequestContext request, Producaoagendatipo producaoagendatipo){
		producaoagendatipo = producaoagendatipoService.loadWithProducaoetapa(producaoagendatipo);
		return new JsonModelAndView().addObject("producaoetapa", producaoagendatipo != null && producaoagendatipo.getProducaoetapa() != null ?
							"br.com.linkcom.sined.geral.bean.Producaoetapa[cdproducaoetapa=" + producaoagendatipo.getProducaoetapa().getCdproducaoetapa() + "]" : null );
	}
	
	/**
	 * M�todo ajax para recalcular a produ��o
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 * @since 08/01/2014
	 */
	public ModelAndView recalcularMaterialProducaoAJAX(WebRequestContext request, Material bean){
		return producaoagendaService.recalcularMaterialProducaoAJAX(request, bean);
	}
	
	/**
	* M�todo para abrir uma popup para troca de material similar
	*
	* @param request
	* @return
	* @since 20/02/2015
	* @author Luiz Fernando
	*/
	public ModelAndView abrirTrocaMaterial(WebRequestContext request){
		return producaoagendaService.abrirTrocaMaterial(request);
	}
	
	/**
	 * Abre popup para inser��o da justificativa do cancelamento.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/07/2014
	 */
	public ModelAndView abrirPopupCancelar(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || "".equals(whereIn)){
			throw new SinedException("Nenhum item selecionado");
		}
		
		if(producaoagendaService.haveProducaoagendaSituacao(whereIn, Producaoagendasituacao.CONCLUIDA, Producaoagendasituacao.CANCELADA, Producaoagendasituacao.CHAO_DE_FABRICA)){
			if(producaoagendaService.haveProducaoagendaSituacao(whereIn, Producaoagendasituacao.CONCLUIDA) && producaoordemService.haveProducaoordemSituacaoByProducaoagenda(whereIn, Producaoordemsituacao.CONCLUIDA)){
				request.addError("Para cancelamento da agenda de produ��o � necess�rio antes que seja estornada a ordem de produ��o vinculada.");
			}else {
				request.addError("Para cancelar uma agenda de produ��o o registro tem que estar com a situa��o 'EM ABERTO' ou 'EM ANDAMENTO'.");
			}
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		boolean haveOrdemproducaoEmandamentoConcluida = producaoordemService.haveProducaoordemSituacaoByProducaoagenda(whereIn, Producaoordemsituacao.EM_ANDAMENTO, Producaoordemsituacao.CONCLUIDA);
		request.setAttribute("haveOrdemproducaoEmandamentoConcluida", haveOrdemproducaoEmandamentoConcluida);
		
		Producaoagendahistorico producaoagendahistorico = new Producaoagendahistorico();
		producaoagendahistorico.setWhereIn(whereIn);
		return new ModelAndView("direct:/crud/popup/popUpCancelarProducaoagenda", "bean", producaoagendahistorico);
	}
	
	/**
	 * M�todo que executa a a��o de cancelamento de uma agenda de produ��o.
	 * Este processo cancela as sa�das do estoque das mat�rias-primas, agendas de produ��o e ordens de produ��o vinculadas.
	 *
	 * @param request
	 * @param producaoagendahistorico
	 * @author Rodrigo Freitas
	 * @since 29/07/2014
	 */
	public void saveCancelar(WebRequestContext request, Producaoagendahistorico producaoagendahistorico){
		String whereIn = producaoagendahistorico.getWhereIn();
		if(whereIn == null || "".equals(whereIn)){
			throw new SinedException("Nenhum item selecionado");
		}
		
		for(String id : whereIn.split(",")){
			Producaoagenda producaoAgenda = producaoagendaService.loadProducaoagenda(new Producaoagenda(Integer.parseInt(id)));
			if (producaoAgenda != null && inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(SinedDateUtils.currentDate(), producaoAgenda.getEmpresa(), 
					producaoAgenda.getLocalbaixamateriaprima(), producaoAgenda.getProjeto())) {
				request.addError("N�o � poss�vel realizar criar ou alterar a movimenta��o de estoque,  pois o per�odo est� vinculado � um registro de invent�rio. " +
						"Para que consiga prosseguir com a a��o � necess�rio cancelar o invent�rio.");
				SinedUtil.fechaPopUp(request);
				return;
			}
		}
		
		reservaService.deleteReservaByProducaoagenda(whereIn);
		producaoagendaService.cancelar(request, whereIn, producaoagendahistorico.getObservacao(), true);
	}
	
	/**
	* M�todo que faz reserva dos materiais da agenda de produ��o
	*
	* @param request
	* @return
	* @since 18/05/2015
	* @author Jo�o Vitor
	*/
	public ModelAndView realizarReserva(WebRequestContext request){
		
		boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		ReservaBean bean = new ReservaBean();
		List<Reserva> listaReservas = new ArrayList<Reserva>();
		
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado");
			SinedUtil.redirecionamento(request, "/producao/crud/Producaoagenda");
			return null;
		}
		
		List<Producaoagenda> listaProducaoagenda = producaoagendaService.findWithSituacao(whereIn);
		List<Reserva> listaReserva = reservaService.findByProducaoagenda(whereIn);
		
		for (Producaoagenda producaoagenda : listaProducaoagenda) {
			if(producaoagenda.getProducaoagendasituacao() == null || !producaoagenda.getProducaoagendasituacao().equals(Producaoagendasituacao.EM_ESPERA)){
				request.addError("Somente para situa��o Em espera.");
				SinedUtil.redirecionamento(request, "/producao/crud/Producaoagenda");
				return null;
			}
			
			if (producaoagenda.getCemobra() != null) {
				request.addError("Somente para situa��o Em espera e agendas de origem diferente de 'Importa��o CEM'.");
				SinedUtil.redirecionamento(request, "/producao/crud/Producaoagenda");
				return null;
			}
			
			if (SinedUtil.isListNotEmpty(listaReserva)) {
				request.addError("S� � poss�vel realizar reserva para agenda de produ��o que ainda n�o possuem reserva.");
				SinedUtil.redirecionamento(request, "/producao/crud/Producaoagenda");
				return null;
			}
		}
		
		
		
		List<Producaoagenda> listaProducaoagendamateriais = producaoagendaService.findProducaoagendamateriaprimaByProducaoagenda(whereIn);
		
			if (SinedUtil.isListNotEmpty(listaProducaoagendamateriais)) {
				for (Producaoagenda producaoagenda : listaProducaoagendamateriais) {
					if (SinedUtil.isListNotEmpty(producaoagenda.getListaProducaoagendamaterial())) {
						for (Producaoagendamaterial producaoagendamaterial : producaoagenda.getListaProducaoagendamaterial()) {
							if (SinedUtil.isListNotEmpty(producaoagendamaterial.getListaProducaoagendamaterialmateriaprima())) {
								for (Producaoagendamaterialmateriaprima producaoagendamaterialmateriaprima : producaoagendamaterial.getListaProducaoagendamaterialmateriaprima()) {
									Reserva reserva = new Reserva();
									reserva.setFracaounidademedida(producaoagendamaterialmateriaprima.getFracaounidademedida());
									reserva.setLoteestoque(producaoagendamaterialmateriaprima.getLoteestoque());
									reserva.setMaterial(producaoagendamaterialmateriaprima.getMaterial());
									reserva.setProducaoagenda(producaoagenda);
									reserva.setEmpresa(producaoagenda.getEmpresa());
									reserva.setQtdereferenciaunidademedida(producaoagendamaterialmateriaprima.getQtdereferenciaunidademedida());
									
									if (producaoagendamaterialmateriaprima.getFracaounidademedida() != null && producaoagendamaterialmateriaprima.getFracaounidademedida() != 0 && producaoagendamaterialmateriaprima.getUnidademedida() != null && producaoagendamaterialmateriaprima.getUnidademedida().getCasasdecimaisestoque() != null) {
										producaoagendamaterialmateriaprima.getUnidademedida().setCasasdecimaisestoque(producaoagendamaterialmateriaprima.getUnidademedida().getCasasdecimaisestoque());
										reserva.setQuantidade(SinedUtil.roundByUnidademedida(producaoagendamaterialmateriaprima.getQtdeprevista() / (producaoagendamaterialmateriaprima.getFracaounidademedida()  / (producaoagendamaterialmateriaprima.getQtdereferenciaunidademedida() != null ? producaoagendamaterialmateriaprima.getQtdereferenciaunidademedida() : 1.0)), producaoagendamaterialmateriaprima.getUnidademedida()));
									} else {
										reserva.setQuantidade(producaoagendamaterialmateriaprima.getQtdeprevista());
									}
									
									reserva.setUnidademedida(producaoagendamaterialmateriaprima.getUnidademedida());
									listaReservas.add(reserva);
								}
							} else {
								Material material = materialService.loadMaterialComMaterialproducao(producaoagendamaterial.getMaterial());
								if(material != null){
									for (Materialproducao materialproducao : material.getListaProducao()) {
										if(materialproducao.getConsumo() != null && materialproducao.getConsumo() > 0 && producaoagendamaterial.getQtde() != null){
											materialproducao.setConsumo(materialproducao.getConsumo() * producaoagendamaterial.getQtde());
										}
										Reserva reserva = new Reserva();
										reserva.setMaterial(materialproducao.getMaterial());
										reserva.setProducaoagenda(producaoagenda);
										reserva.setEmpresa(producaoagenda.getEmpresa());
										reserva.setQuantidade(materialproducao.getConsumo());
										reserva.setUnidademedida(materialproducao.getUnidademedida());
										listaReservas.add(reserva);
									}
								}
						}
					}
				}
			}
		}
		if (SinedUtil.isListEmpty(listaReservas)) {
			request.addError("Agenda(s) de produ��o sem mat�rias-primas.");
			SinedUtil.redirecionamento(request, "/producao/crud/Producaoagenda");
			return null;
		}
		
		bean.setListareserva(listaReservas);
		
		request.setAttribute("entrada", entrada);
		request.setAttribute("selectedItens", whereIn);
		
		return new ModelAndView("direct:/crud/popup/popUpReservarEstoque", "bean", bean);
	}
	
	/**
	* M�todo que cancela reserva dos materiais da agenda de produ��o
	*
	* @param request
	* @return
	* @since 19/05/2015
	* @author Jo�o Vitor
	*/
	public void cancelarReserva(WebRequestContext request){
		
		boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if (StringUtils.isNotEmpty(whereIn)) {
			reservaService.deleteReservaByProducaoagenda(whereIn);
			request.addMessage("Reserva(s) cancelada(s).");
		} else {
			request.addMessage("N�o existem reservas para a(s) agenda(s) de produ��o selecionada(s).");
		}
		SinedUtil.redirecionamento(request, "/producao/crud/Producaoagenda" + (entrada ? "?ACAO=consultar&cdproducaoagenda=" + whereIn : ""));
	}
	
	/**
	* M�todo que salva os dados ap�s o c�lculo de sobra
	*
	* @param request
	* @param producaoSobraBean
	* @since 15/05/2015
	* @author Jo�o Vitor
	*/
	public void saveReservarMaterial(WebRequestContext request, ReservaBean reservaBean){
		boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if (reservaBean != null && SinedUtil.isListNotEmpty(reservaBean.getListareserva())) {
			for (Reserva reserva : reservaBean.getListareserva()) {
				reservaService.saveOrUpdate(reserva);
			}
			request.addMessage("Reserva de material realizada com sucesso.");
		} else {
			request.addError("Nenhuma reserva de material foi feita.");
		}
		SinedUtil.redirecionamento(request, "/producao/crud/Producaoagenda" + (entrada ? "?ACAO=consultar&cdproducaoagenda=" + whereIn : ""));
	}
	
	/**
	* M�todo que abre uma popup com os c�lculos de sobra
	*
	* @param request
	* @return
	* @since 03/12/2014
	* @author Luiz Fernando
	*/
	public ModelAndView calcularSobra(WebRequestContext request){
		boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado");
			SinedUtil.redirecionamento(request, "/producao/crud/Producaoagenda");
			return null;
		}
		
		if(producaoagendaService.notProducaoagendaSituacao(whereIn, Producaoagendasituacao.EM_ANDAMENTO)){
			request.addError("� permitido calcular sobra somente para agenda de produ��o com situa��o Em Andamento");
			SinedUtil.redirecionamento(request, "/producao/crud/Producaoagenda" + (entrada ? "?ACAO=consultar&cdproducaoagenda=" + whereIn : ""));
			return null;
		}
		
		ProducaoSobraBean producaoSobraBean = producaoagendaService.getProducaoSobra(whereIn);
		request.setAttribute("entrada", entrada);
		request.setAttribute("selectedItens", whereIn);
		return new ModelAndView("direct:/crud/popup/popUpCalcularSobra", "bean", producaoSobraBean);
	}
	
	/**
	* M�todo que salva os dados ap�s o c�lculo de sobra
	*
	* @param request
	* @param producaoSobraBean
	* @since 03/12/2014
	* @author Luiz Fernando
	*/
	public void saveCalcularSobra(WebRequestContext request, ProducaoSobraBean producaoSobraBean){
		boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(producaoSobraBean.getConsiderarsobraperda() != null && producaoSobraBean.getConsiderarsobraperda()){
			producaoagendaService.registrarQuantidadePerdaSobraUltimaOrdemproducao(producaoSobraBean);
			request.addMessage("Registro de perda da sobra realizada com sucesso.");
		}else if(producaoSobraBean.getLocalsobra() != null){
			producaoagendaService.registrarEntradaPerdaSobraUltimaOrdemproducao(producaoSobraBean);
			request.addMessage("Registro de entrada de perda da sobra realizada com sucesso.");
		}
		SinedUtil.redirecionamento(request, "/producao/crud/Producaoagenda" + (entrada ? "?ACAO=consultar&cdproducaoagenda=" + whereIn : ""));
	}
	
	/**
	 * M�todo que armazena os dados necess�rios para executar o autocomplete de lote.
	 * 
	 * @param request
	 * @author Jo�o Vitor
	 */
	public void ajaxSetDadosSessaoForLoteAutocomplete(WebRequestContext request){
		pedidovendaService.ajaxSetDadosSessaoForLoteAutocomplete(request, "PRODUCAOAGENDA");
	}
	
	/**
	 * Ajax para buscar o JSON com a lista de unidade de medida.
	 *
	 * @param request
	 * @return
	 * @author Jo�o Vitor
	 * @since 05/05/2015
	 */
	public ModelAndView ajaxPreencheComboUnidadeMedida(WebRequestContext request){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		List<Unidademedida> lista = null;
		String cdmaterial = request.getParameter("cdmaterial");
		Boolean verificarPrioridadeProducao = Boolean.valueOf(request.getParameter("verificarPrioridadeProducao") != null ? request.getParameter("verificarPrioridadeProducao") : "false");
		
		Unidademedida unidademedida = new Unidademedida();
		
		if (StringUtils.isNotBlank(cdmaterial)) {
			unidademedida = materialService.unidadeMedidaMaterial(new Material(Integer.parseInt(cdmaterial))).getUnidademedida();
			lista = unidademedidaService.getUnidademedidaByMaterial(new Material(Integer.parseInt(cdmaterial)));
			if(verificarPrioridadeProducao){
				Material material = materialService.findListaMaterialunidademedida(new Material(Integer.parseInt(cdmaterial)));
				Materialunidademedida materialunidademedida = materialunidademedidaService.getMaterialunidademedidaPrioritariaProducao(material);
				if(materialunidademedida != null && materialunidademedida.getUnidademedida() != null){
					unidademedida = materialunidademedida.getUnidademedida();
				}
				
			}
		}
		
		jsonModelAndView.addObject("unidademedida", unidademedida);
		jsonModelAndView.addObject("lista", lista);
		
		return jsonModelAndView; 
	}
	
	public ModelAndView buscarInfProducaoetapa(WebRequestContext request, Producaoetapa producaoetapa){
		producaoetapa = producaoetapaService.load(producaoetapa);
		return new JsonModelAndView().addObject("baixarEstoqueMateriaprima", producaoetapa != null ? producaoetapa.getBaixarEstoqueMateriaprima().getValue().toString() : "");
	}
	
	/**
	* M�todo que converte a qtde prevista da materia prima
	*
	* @param request
	* @param producaoagendamaterialmateriaprima
	* @return
	* @since 19/02/2016
	* @author Luiz Fernando
	*/
	public ModelAndView converteUnidademedida(WebRequestContext request, Materiaprima materiaprima){
		return producaoagendaService.converteUnidademedida(request, materiaprima);
	}
	
	
	/**
	 * M�todo que faz a cria��o dos registros referente ao envio para o ch�o de f�brica
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/03/2016
	 */
	public ModelAndView enviarChaofabrica(WebRequestContext request){
		String entrada = request.getParameter("entrada");
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		try{
			producaochaofabricaService.createChaofabricaByProducaoagenda(whereIn);
			request.addMessage("Envio para o ch�o de f�brica realizado com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro ao enviar para o ch�o de f�brica: " + e.getMessage());
		}
		
		if(entrada != null && entrada.equals("true")){
			return sendRedirectToAction("consultar", "cdproducaoagenda=" + whereIn);
		} else {
			return sendRedirectToAction("listagem");
		}
	}
}