package br.com.linkcom.sined.modulo.producao.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Cemobra;
import br.com.linkcom.sined.geral.bean.Cemtipologia;
import br.com.linkcom.sined.geral.service.CemtipologiaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(path="/producao/crud/Cemobra", authorizationModule=CrudAuthorizationModule.class)
public class CemobraCrud extends CrudControllerSined<FiltroListagemSined, Cemobra, Cemobra> {

	private CemtipologiaService cemtipologiaService;
	
	public void setCemtipologiaService(CemtipologiaService cemtipologiaService) {
		this.cemtipologiaService = cemtipologiaService;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void entrada(WebRequestContext request, Cemobra form) throws Exception {
		if(form.getCdcemobra() != null){
			form.setListaCemtipologia(SinedUtil.listToSet(cemtipologiaService.findByCemobra(form), Cemtipologia.class));
		}
	}
	
}