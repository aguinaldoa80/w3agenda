package br.com.linkcom.sined.modulo.producao.controller.crud;

import java.util.Arrays;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Producaochaofabrica;
import br.com.linkcom.sined.geral.bean.enumeration.Producaochaofabricasituacao;
import br.com.linkcom.sined.geral.service.ProducaochaofabricaService;
import br.com.linkcom.sined.modulo.producao.controller.crud.filter.ProducaochaofabricaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/producao/crud/Producaochaofabrica", authorizationModule=CrudAuthorizationModule.class)
public class ProducaochaofabricaCrud extends CrudControllerSined<ProducaochaofabricaFiltro, Producaochaofabrica, Producaochaofabrica> {
	
	private ProducaochaofabricaService producaochaofabricaService;
	
	public void setProducaochaofabricaService(
			ProducaochaofabricaService producaochaofabricaService) {
		this.producaochaofabricaService = producaochaofabricaService;
	}

	@Override
	protected void entrada(WebRequestContext request, Producaochaofabrica form) throws Exception {
		throw new SinedException("A��o n�o permitida.");
	}
	
	@Override
	protected void listagem(WebRequestContext request, ProducaochaofabricaFiltro filtro) throws Exception {
		request.setAttribute("listaProducaochaofabricasituacaoCompleta", Arrays.asList(Producaochaofabricasituacao.values()));
	}
	
	public ModelAndView imprimir(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		producaochaofabricaService.updateEmitirEtiquetaAutomatico(whereIn, Boolean.TRUE);
		return sendRedirectToAction("listagem");
	}
	
}