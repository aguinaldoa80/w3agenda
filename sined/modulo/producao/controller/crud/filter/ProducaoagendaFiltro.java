package br.com.linkcom.sined.modulo.producao.controller.crud.filter;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.ProducaoagendaFiltroMostrar;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoagendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoetiqueta;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ProducaoagendaFiltro extends FiltroListagemSined {

	private Empresa empresa;
	private Cliente cliente;
	private Contrato contrato;
	private Projeto projeto;
	private Integer pedidovenda;
	private Timestamp dtentrega1;
	private Timestamp dtentrega2;
	private Material material;
	private ProducaoagendaFiltroMostrar producaoagendaFiltroMostrar;
	private List<Producaoagendasituacao> listaProducaoagendasituacao;
	private Tipoetiqueta etiqueta;
	private String identificacaoexterna;
	
	public ProducaoagendaFiltro() {
		listaProducaoagendasituacao = new ArrayList<Producaoagendasituacao>();
		listaProducaoagendasituacao.add(Producaoagendasituacao.EM_ESPERA);
		listaProducaoagendasituacao.add(Producaoagendasituacao.EM_ANDAMENTO);
		listaProducaoagendasituacao.add(Producaoagendasituacao.CHAO_DE_FABRICA);
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	public Contrato getContrato() {
		return contrato;
	}
	@DisplayName("C�digo do pedido de venda")
	public Integer getPedidovenda() {
		return pedidovenda;
	}
	public Timestamp getDtentrega1() {
		return dtentrega1;
	}
	public Timestamp getDtentrega2() {
		return dtentrega2;
	}
	public List<Producaoagendasituacao> getListaProducaoagendasituacao() {
		return listaProducaoagendasituacao;
	}
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Mostrar")
	public ProducaoagendaFiltroMostrar getProducaoagendaFiltroMostrar() {
		return producaoagendaFiltroMostrar;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setProducaoagendaFiltroMostrar(
			ProducaoagendaFiltroMostrar producaoagendaFiltroMostrar) {
		this.producaoagendaFiltroMostrar = producaoagendaFiltroMostrar;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public void setPedidovenda(Integer pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public void setDtentrega1(Timestamp dtentrega1) {
		this.dtentrega1 = dtentrega1;
	}
	public void setDtentrega2(Timestamp dtentrega2) {
		this.dtentrega2 = dtentrega2;
	}
	public void setListaProducaoagendasituacao(
			List<Producaoagendasituacao> listaProducaoagendasituacao) {
		this.listaProducaoagendasituacao = listaProducaoagendasituacao;
	}
	public Tipoetiqueta getEtiqueta() {
		return etiqueta;
	}
	public void setEtiqueta(Tipoetiqueta etiqueta) {
		this.etiqueta = etiqueta;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	@DisplayName("Ident. Externa")
	public String getIdentificacaoexterna() {
		return identificacaoexterna;
	}
	public void setIdentificacaoexterna(String identificacaoexterna) {
		this.identificacaoexterna = identificacaoexterna;
	}
}
