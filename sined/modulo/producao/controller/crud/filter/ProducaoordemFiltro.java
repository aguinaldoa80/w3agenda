package br.com.linkcom.sined.modulo.producao.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoordemsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoetiqueta;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ProducaoordemFiltro extends FiltroListagemSined {

	private Empresa empresa;
	private Departamento departamento;
	private Date dtprevisaoentrega1;
	private Date dtprevisaoentrega2;
	private Date dtordemproducao1;
	private Date dtordemproducao2;
	private Material material;
	private Cliente cliente;
	private Integer cdpedidovenda;
	private List<Producaoordemsituacao> listaProducaoordemsituacao;
	private Tipoetiqueta etiqueta;
	private String identificacaoexterna;
	private Boolean exibirRegistrosComPerda;
	private Integer cdpneu;
	private Integer cdpedidovendamaterial;
	
	public ProducaoordemFiltro() {
		listaProducaoordemsituacao = new ArrayList<Producaoordemsituacao>();
		listaProducaoordemsituacao.add(Producaoordemsituacao.EM_ESPERA);
		listaProducaoordemsituacao.add(Producaoordemsituacao.EM_ANDAMENTO);
		listaProducaoordemsituacao.add(Producaoordemsituacao.MATERIA_PRIMA_PENDENTE);
	}
	
	public Date getDtprevisaoentrega1() {
		return dtprevisaoentrega1;
	}
	public Date getDtprevisaoentrega2() {
		return dtprevisaoentrega2;
	}
	public List<Producaoordemsituacao> getListaProducaoordemsituacao() {
		return listaProducaoordemsituacao;
	}
	public Material getMaterial() {
		return material;
	}
	public Departamento getDepartamento() {
		return departamento;
	}
	public Date getDtordemproducao1() {
		return dtordemproducao1;
	}
	public Date getDtordemproducao2() {
		return dtordemproducao2;
	}
	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
	public void setDtordemproducao1(Date dtordemproducao1) {
		this.dtordemproducao1 = dtordemproducao1;
	}
	public void setDtordemproducao2(Date dtordemproducao2) {
		this.dtordemproducao2 = dtordemproducao2;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setDtprevisaoentrega1(Date dtprevisaoentrega1) {
		this.dtprevisaoentrega1 = dtprevisaoentrega1;
	}
	public void setDtprevisaoentrega2(Date dtprevisaoentrega2) {
		this.dtprevisaoentrega2 = dtprevisaoentrega2;
	}
	public void setListaProducaoordemsituacao(
			List<Producaoordemsituacao> listaProducaoordemsituacao) {
		this.listaProducaoordemsituacao = listaProducaoordemsituacao;
	}
	public Tipoetiqueta getEtiqueta() {
		return etiqueta;
	}
	public void setEtiqueta(Tipoetiqueta etiqueta) {
		this.etiqueta = etiqueta;
	}
	
	public String getIdentificacaoexterna() {
		return identificacaoexterna;
	}
	public void setIdentificacaoexterna(String identificacaoexterna) {
		this.identificacaoexterna = identificacaoexterna;
	}
	
	@DisplayName("Exibir ordens de produ��o com registro de perda")
	public Boolean getExibirRegistrosComPerda() {
		return exibirRegistrosComPerda;
	}
	public void setExibirRegistrosComPerda(Boolean exibirRegistrosComPerda) {
		this.exibirRegistrosComPerda = exibirRegistrosComPerda;
	}

	@DisplayName("Pneu")
	@MaxLength(8)
	public Integer getCdpneu() {
		return cdpneu;
	}
	@DisplayName("Item")
	@MaxLength(8)
	public Integer getCdpedidovendamaterial() {
		return cdpedidovendamaterial;
	}

	public void setCdpneu(Integer cdpneu) {
		this.cdpneu = cdpneu;
	}
	public void setCdpedidovendamaterial(Integer cdpedidovendamaterial) {
		this.cdpedidovendamaterial = cdpedidovendamaterial;
	}
}
