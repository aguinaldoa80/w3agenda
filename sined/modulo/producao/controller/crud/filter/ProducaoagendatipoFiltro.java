package br.com.linkcom.sined.modulo.producao.controller.crud.filter;

import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ProducaoagendatipoFiltro extends FiltroListagemSined {
	
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
