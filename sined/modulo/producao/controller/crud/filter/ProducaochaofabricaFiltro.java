package br.com.linkcom.sined.modulo.producao.controller.crud.filter;

import java.util.List;

import br.com.linkcom.sined.geral.bean.enumeration.Producaochaofabricasituacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ProducaochaofabricaFiltro extends FiltroListagemSined {

	private String whereInProducaoagenda;
	private String whereInPedidovenda;
	private String whereInPneu;
	private List<Producaochaofabricasituacao> listaProducaochaofabricasituacao;
	
	public String getWhereInProducaoagenda() {
		return whereInProducaoagenda;
	}

	public void setWhereInProducaoagenda(String whereInProducaoagenda) {
		this.whereInProducaoagenda = whereInProducaoagenda;
	}

	public String getWhereInPedidovenda() {
		return whereInPedidovenda;
	}

	public void setWhereInPedidovenda(String whereInPedidovenda) {
		this.whereInPedidovenda = whereInPedidovenda;
	}

	public String getWhereInPneu() {
		return whereInPneu;
	}

	public void setWhereInPneu(String whereInPneu) {
		this.whereInPneu = whereInPneu;
	}

	public List<Producaochaofabricasituacao> getListaProducaochaofabricasituacao() {
		return listaProducaochaofabricasituacao;
	}
	
	public void setListaProducaochaofabricasituacao(
			List<Producaochaofabricasituacao> listaProducaochaofabricasituacao) {
		this.listaProducaochaofabricasituacao = listaProducaochaofabricasituacao;
	}
	
}
