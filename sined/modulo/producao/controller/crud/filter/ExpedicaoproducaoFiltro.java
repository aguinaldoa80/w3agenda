package br.com.linkcom.sined.modulo.producao.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Regiao;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoetiqueta;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ExpedicaoproducaoFiltro extends FiltroListagemSined {
	
	private Integer cdexpedicaoproducao;
	private Date dtexpedicao1;
	private Date dtexpedicao2;
	private Integer identificadorcarregamento;
	private Fornecedor transportadora;
	private Colaborador motorista;
	private Veiculo veiculo;
	private Cliente cliente;
	private Uf uf;
	private Municipio municipio;
	private Regiao regiao;
	private Tipoetiqueta tipoetiqueta;
	private List<Expedicaosituacao> listaExpedicaosituacao;
	private Contrato contrato;
	
	private String selectedItensEtiqueta;
	
	public ExpedicaoproducaoFiltro() {
		listaExpedicaosituacao = new ArrayList<Expedicaosituacao>();
		listaExpedicaosituacao.add(Expedicaosituacao.EM_ABERTO);
		listaExpedicaosituacao.add(Expedicaosituacao.CONFIRMADA);
	}
	
	@MaxLength(9)
	@DisplayName("Carregamento")
	public Integer getIdentificadorcarregamento() {
		return identificadorcarregamento;
	}

	public Fornecedor getTransportadora() {
		return transportadora;
	}

	public Cliente getCliente() {
		return cliente;
	}
	
	public Date getDtexpedicao1() {
		return dtexpedicao1;
	}

	public Date getDtexpedicao2() {
		return dtexpedicao2;
	}
	
	public Uf getUf() {
		return uf;
	}
	
	@DisplayName("Munic�pio")
	public Municipio getMunicipio() {
		return municipio;
	}

	@DisplayName("Regi�o")
	public Regiao getRegiao() {
		return regiao;
	}
	
	@DisplayName("ID")
	@MaxLength(9)
	public Integer getCdexpedicaoproducao() {
		return cdexpedicaoproducao;
	}
	
	@DisplayName("Tipo de etiqueta")
	public Tipoetiqueta getTipoetiqueta() {
		return tipoetiqueta;
	}

	@DisplayName("Situa��es")
	public List<Expedicaosituacao> getListaExpedicaosituacao() {
		return listaExpedicaosituacao;
	}
	
	public String getSelectedItensEtiqueta() {
		return selectedItensEtiqueta;
	}
	
	public Colaborador getMotorista() {
		return motorista;
	}
	
	@DisplayName("Ve�culo")
	public Veiculo getVeiculo() {
		return veiculo;
	}
	
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
	public void setMotorista(Colaborador motorista) {
		this.motorista = motorista;
	}
	
	public void setSelectedItensEtiqueta(String selectedItensEtiqueta) {
		this.selectedItensEtiqueta = selectedItensEtiqueta;
	}
	
	public void setTipoetiqueta(Tipoetiqueta tipoetiqueta) {
		this.tipoetiqueta = tipoetiqueta;
	}
	
	public void setCdexpedicaoproducao(Integer cdexpedicaoproducao) {
		this.cdexpedicaoproducao = cdexpedicaoproducao;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}

	public void setDtexpedicao1(Date dtexpedicao1) {
		this.dtexpedicao1 = dtexpedicao1;
	}

	public void setDtexpedicao2(Date dtexpedicao2) {
		this.dtexpedicao2 = dtexpedicao2;
	}

	public void setIdentificadorcarregamento(Integer identificadorcarregamento) {
		this.identificadorcarregamento = identificadorcarregamento;
	}

	public void setTransportadora(Fornecedor transportadora) {
		this.transportadora = transportadora;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setListaExpedicaosituacao(
			List<Expedicaosituacao> listaExpedicaosituacao) {
		this.listaExpedicaosituacao = listaExpedicaosituacao;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

}
