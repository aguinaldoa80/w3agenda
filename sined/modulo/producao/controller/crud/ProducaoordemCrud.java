package br.com.linkcom.sined.modulo.producao.controller.crud;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.AbstractCrudController;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.ColetaMaterialPedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Garantiatipo;
import br.com.linkcom.sined.geral.bean.Garantiatipopercentual;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Motivodevolucao;
import br.com.linkcom.sined.geral.bean.NotafiscalprodutoColeta;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoetapa;
import br.com.linkcom.sined.geral.bean.Producaoetapaitem;
import br.com.linkcom.sined.geral.bean.Producaoetapanome;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemconstatacao;
import br.com.linkcom.sined.geral.bean.Producaoordemequipamento;
import br.com.linkcom.sined.geral.bean.Producaoordemhistorico;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialmateriaprima;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialoperador;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialorigem;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialperdadescarte;
import br.com.linkcom.sined.geral.bean.auxiliar.ProducaoordemmaterialMaterialprimaPerdaBean;
import br.com.linkcom.sined.geral.bean.enumeration.BaixarEstoqueMateriasPrimasEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoordemsituacao;
import br.com.linkcom.sined.geral.service.ColetaService;
import br.com.linkcom.sined.geral.service.GarantiareformaService;
import br.com.linkcom.sined.geral.service.GarantiatipoService;
import br.com.linkcom.sined.geral.service.InventarioService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialproducaoService;
import br.com.linkcom.sined.geral.service.MotivodevolucaoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoColetaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PneuService;
import br.com.linkcom.sined.geral.service.ProducaoagendaService;
import br.com.linkcom.sined.geral.service.ProducaoetapaService;
import br.com.linkcom.sined.geral.service.ProducaoetapaitemService;
import br.com.linkcom.sined.geral.service.ProducaoordemService;
import br.com.linkcom.sined.geral.service.ProducaoordemequipamentoService;
import br.com.linkcom.sined.geral.service.ProducaoordemhistoricoService;
import br.com.linkcom.sined.geral.service.ProducaoordemmaterialService;
import br.com.linkcom.sined.geral.service.ProducaoordemmaterialcampoService;
import br.com.linkcom.sined.geral.service.ProducaoordemmaterialoperadorService;
import br.com.linkcom.sined.geral.service.ProducaoordemmaterialperdadescarteService;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ConferenciaProducaoBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ConsumoMateriaprimaProducaoBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.Materiaprima;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.MateriaprimaConferenciaProducaoBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ProducaoordemdevolucaoBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.filter.ProducaoordemFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@Controller(path="/producao/crud/Producaoordem", authorizationModule=CrudAuthorizationModule.class)
public class ProducaoordemCrud extends CrudControllerSined<ProducaoordemFiltro, Producaoordem, Producaoordem>{
	
	private ProducaoordemhistoricoService producaoordemhistoricoService;
	private ProducaoordemService producaoordemService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private ParametrogeralService parametrogeralService;
	private MaterialService materialService;
	private PedidovendaService pedidovendaService;
	private ProducaoetapaService producaoetapaService;
	private PneuService pneuService;
	private ColetaService coletaService;
	private ProducaoordemmaterialcampoService producaoordemmaterialcampoService;
	private ProducaoordemequipamentoService producaoordemequipamentoService;
	private MaterialproducaoService materialproducaoService;
	private ProducaoagendaService producaoagendaService;
	private ProducaoetapaitemService producaoetapaitemService;
	private ProducaoordemmaterialService producaoordemmaterialService;
	private MotivodevolucaoService motivodevolucaoService;
	private GarantiatipoService garantiatipoService;
	private GarantiareformaService garantiareformaService;
	private ProducaoordemmaterialperdadescarteService producaoordemmaterialperdadescarteService;
	private NotafiscalprodutoColetaService notafiscalprodutoColetaService;
	private ProducaoordemmaterialoperadorService producaoordemmaterialoperadorService;
	private InventarioService inventarioService;
	private LocalarmazenagemService localarmazenagemService;
	
	public void setPneuService(PneuService pneuService) {
		this.pneuService = pneuService;
	}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setProducaoordemService(ProducaoordemService producaoordemService) {
		this.producaoordemService = producaoordemService;
	}
	public void setProducaoordemhistoricoService(ProducaoordemhistoricoService producaoordemhistoricoService) {
		this.producaoordemhistoricoService = producaoordemhistoricoService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setProducaoetapaService(ProducaoetapaService producaoetapaService) {
		this.producaoetapaService = producaoetapaService;
	}
	public void setColetaService(ColetaService coletaService) {
		this.coletaService = coletaService;
	}
	public void setProducaoordemmaterialcampoService(ProducaoordemmaterialcampoService producaoordemmaterialcampoService) {
		this.producaoordemmaterialcampoService = producaoordemmaterialcampoService;
	}
	public void setProducaoordemequipamentoService(ProducaoordemequipamentoService producaoordemequipamentoService) {
		this.producaoordemequipamentoService = producaoordemequipamentoService;
	}
	public void setMaterialproducaoService(MaterialproducaoService materialproducaoService) {
		this.materialproducaoService = materialproducaoService;
	}
	public void setProducaoagendaService(ProducaoagendaService producaoagendaService) {
		this.producaoagendaService = producaoagendaService;
	}
	public void setProducaoetapaitemService(ProducaoetapaitemService producaoetapaitemService) {
		this.producaoetapaitemService = producaoetapaitemService;
	}
	public void setProducaoordemmaterialService(ProducaoordemmaterialService producaoordemmaterialService) {
		this.producaoordemmaterialService = producaoordemmaterialService;
	}
	public void setMotivodevolucaoService(MotivodevolucaoService motivodevolucaoService) {
		this.motivodevolucaoService = motivodevolucaoService;
	}
	public void setGarantiatipoService(GarantiatipoService garantiatipoService) {
		this.garantiatipoService = garantiatipoService;
	}
	public void setGarantiareformaService(GarantiareformaService garantiareformaService) {
		this.garantiareformaService = garantiareformaService;
	}
	public void setProducaoordemmaterialperdadescarteService(
			ProducaoordemmaterialperdadescarteService producaoordemmaterialperdadescarteService) {
		this.producaoordemmaterialperdadescarteService = producaoordemmaterialperdadescarteService;
	}
	public void setNotafiscalprodutoColetaService(NotafiscalprodutoColetaService notafiscalprodutoColetaService) {
		this.notafiscalprodutoColetaService = notafiscalprodutoColetaService;
	}
	public void setProducaoordemmaterialoperadorService(
			ProducaoordemmaterialoperadorService producaoordemmaterialoperadorService) {
		this.producaoordemmaterialoperadorService = producaoordemmaterialoperadorService;
	}
	public void setInventarioService(InventarioService inventarioService) {
		this.inventarioService = inventarioService;
	}
	public void setLocalarmazenagemService(
			LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Producaoordem form) throws CrudException {
		if(producaoordemService.existeProducaoagenda(form)){
			throw new SinedException("N�o � permitido excluir uma Ordem de produ��o que tenha v�nculo com agenda de produ��o.");
		}
		return super.doExcluir(request, form);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void entrada(WebRequestContext request, Producaoordem form) throws Exception {
		if(form.getCdproducaoordem() != null){
			form.setProducaoordemanterior(producaoordemService.getProducaoordemanterior(form));
			form.setListaProducaoordemhistorico(producaoordemhistoricoService.findByProducaoordem(form));
			form.setListaProducaoordemequipamento(SinedUtil.listToSet(producaoordemequipamentoService.findByProducaoordem(form), Producaoordemequipamento.class));
			producaoordemmaterialcampoService.preencherListaCamposAdicionais(form.getListaProducaoordemmaterial());
			producaoordemService.carregarProducaoordemmaterialmateriaprima(form);
			
			boolean consultar = request.getAttribute(CONSULTAR) != null && (Boolean)request.getAttribute(CONSULTAR);
			if(!consultar){
				if(form.getProducaoordemsituacao() != null && !form.getProducaoordemsituacao().equals(Producaoordemsituacao.EM_ESPERA)){
					throw new SinedException("N�o � poss�vel editar um registro de ordem de produ��o com situa��o diferente de 'EM ESPERA'.");
				}
			}
		}
		
		boolean pedidovendaVinculadoPrevisto = true;
		if(form.getListaProducaoordemmaterial() != null && !form.getListaProducaoordemmaterial().isEmpty()){
			for (Producaoordemmaterial producaoordemmaterial : form.getListaProducaoordemmaterial()) {
				if(producaoordemmaterial.getPedidovendamaterial() != null && 
						producaoordemmaterial.getPedidovendamaterial().getCdpedidovendamaterial() != null){
					Pedidovenda pedidovenda = pedidovendaService.getPedidovendaByPedidovendamaterial(producaoordemmaterial.getPedidovendamaterial());
					if(pedidovenda != null && pedidovenda.getPedidovendasituacao() != null){
						if(!pedidovenda.getPedidovendasituacao().equals(Pedidovendasituacao.AGUARDANDO_APROVACAO) && 
								!pedidovenda.getPedidovendasituacao().equals(Pedidovendasituacao.PREVISTA) &&
								!pedidovenda.getPedidovendasituacao().equals(Pedidovendasituacao.CONFIRMADO_PARCIALMENTE)){
							pedidovendaVinculadoPrevisto = false;
						}
					}
				}
				if(producaoordemmaterial.getCdproducaoordemmaterial() != null){
					producaoordemmaterial.setListaProducaoordemmaterialperdadescarte(producaoordemmaterialperdadescarteService.findBy(producaoordemmaterial));
				}
			}
		}
		request.setAttribute("pedidovendaVinculadoPrevisto", pedidovendaVinculadoPrevisto);
		if(!AbstractCrudController.CONSULTAR.equals(request.getParameter(MultiActionController.ACTION_PARAMETER))){
			producaoordemequipamentoService.preencherListaEquipamentos(form);
		}
		
		Set<Producaoordemconstatacao> listaProducaoordemconstatacao = form.getListaProducaoordemconstatacao();
		if (listaProducaoordemconstatacao!=null){
			List<Producaoordemconstatacao> listaOrdenada = new ListSet<Producaoordemconstatacao>(Producaoordemconstatacao.class, listaProducaoordemconstatacao);
			Collections.sort(listaOrdenada, 
								new Comparator<Producaoordemconstatacao>() {
									@Override
									public int compare(Producaoordemconstatacao o1, Producaoordemconstatacao o2) {
										if (o1.getMotivodevolucao()!=null 
												&& o1.getMotivodevolucao().getDescricao()!=null 
												&& o2.getMotivodevolucao()!=null
												&& o2.getMotivodevolucao().getDescricao()!=null){
											return o1.getMotivodevolucao().getDescricao().trim().compareToIgnoreCase(o2.getMotivodevolucao().getDescricao().trim());
										}else {
											return 0;											
										}
									}
								});
			form.setListaProducaoordemconstatacao(new ListSet<Producaoordemconstatacao>(Producaoordemconstatacao.class, listaOrdenada));
		}
		
		request.setAttribute("listaGarantiatipo", garantiatipoService.findAtivos());
		request.setAttribute("listaMotivodevolucaoConstatacao", motivodevolucaoService.findForConstacacao());
	}
	
	@Override
	protected void listagem(WebRequestContext request, ProducaoordemFiltro filtro) throws Exception {
		// MANDA PARA A TELA DE LISTAGEM AS SITUA��ES
		List<Producaoordemsituacao> listaProducaoordemsituacaoCompleta = new ArrayList<Producaoordemsituacao>();
		Producaoordemsituacao[] producaoordemsituacaoArray = Producaoordemsituacao.values();
		for (int i = 0; i < producaoordemsituacaoArray.length; i++) {
			listaProducaoordemsituacaoCompleta.add(producaoordemsituacaoArray[i]);
		}
		request.setAttribute("listaProducaoordemsituacaoCompleta", listaProducaoordemsituacaoCompleta);
	}
	
	@Override
	protected ListagemResult<Producaoordem> getLista(WebRequestContext request, ProducaoordemFiltro filtro) {
		ListagemResult<Producaoordem> listagemResult = super.getLista(request, filtro);
		List<Producaoordem> list = listagemResult.list();
		
		if(list != null && list.size() > 0){
			String whereIn = CollectionsUtil.listAndConcatenate(list, "cdproducaoordem", ",");
			
			list.removeAll(list);
			list.addAll(producaoordemService.loadWithLista(whereIn, filtro.getOrderBy(), filtro.isAsc()));
		}
		
		List<Producaoordem> listAux = new ArrayList<Producaoordem>();
		for(Producaoordem producaoordem : list){
			if(producaoordem.getListaProducaoordemmaterial() != null && !producaoordem.getListaProducaoordemmaterial().isEmpty()){
				for(Producaoordemmaterial pom : producaoordem.getListaProducaoordemmaterial()){
					if(pom.getQtdeperdadescarte() != null && pom.getQtdeperdadescarte() > 0){
						producaoordem.setHasPerda(Boolean.TRUE);
						if(filtro.getExibirRegistrosComPerda() != null && !filtro.getExibirRegistrosComPerda()){
							listAux.add(producaoordem);
						}
						break;
					}
				}
			}
		}
		if(listAux != null && !listAux.isEmpty() && filtro.getExibirRegistrosComPerda() != null && !filtro.getExibirRegistrosComPerda()){
			list.removeAll(listAux);
		}
		return listagemResult;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Producaoordem bean) throws Exception {
		boolean isCriar = bean.getCdproducaoordem() == null;
		
		List<Pedidovendamaterial> listaPedidovendamaterialBandaAlterada = new ArrayList<Pedidovendamaterial>();
		if(bean.getListaProducaoordemmaterial() != null && bean.getListaProducaoordemmaterial().size() > 0){
			for (Producaoordemmaterial item : bean.getListaProducaoordemmaterial()) {
				if(item.getPneu() != null){
					if(item.getPneu().existeDados()){
						if(item.getPedidovendamaterial() != null && item.getPneu().getCdpneu() != null && pneuService.bandaAlterada(item.getPneu(), item.getPneu().getMaterialbanda())){
							listaPedidovendamaterialBandaAlterada.add(item.getPedidovendamaterial());
						}
						pneuService.saveOrUpdate(item.getPneu());
					} else {
						item.setPneu(null);
					}
				}
			}
		}
		
		super.salvar(request, bean);
		producaoordemequipamentoService.updateVinculoProducaoordemmaterialEquipamento(bean);
		producaoordemService.salvarCamposAdicionais(bean.getListaProducaoordemmaterial());
		
		// SALVAR HIST�RICO
		Producaoordemhistorico producaoordemhistorico = new Producaoordemhistorico();
		producaoordemhistorico.setProducaoordem(bean);
		if(isCriar){
			producaoordemhistorico.setObservacao("Criado");
		} else {
			producaoordemhistorico.setObservacao("Alterado");
		}
		if(bean.getHistorico() != null && !"".equals(bean.getHistorico())){
			producaoordemhistorico.setObservacao((producaoordemhistorico.getObservacao() != null && !"".equals(producaoordemhistorico.getObservacao()) ? 
					producaoordemhistorico.getObservacao() + ". ": "") + bean.getHistorico()); 
		}
		producaoordemhistoricoService.saveOrUpdate(producaoordemhistorico);
		
		pedidovendaService.updateBandaAlterada(listaPedidovendamaterialBandaAlterada);
	}	
	
	/**
	 * Action que gera o CSV de acordo com o filtro selecionado.
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public ModelAndView gerarCsv(WebRequestContext request, ProducaoordemFiltro filtro){
		return new ResourceModelAndView(producaoordemService.makeCSVListagem(filtro));
	}
	
	/**
	 * Action para abrir a tela de confer�ncia da produ��o.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/01/2013
	 */
	public ModelAndView conferenciaProducao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		ConferenciaProducaoBean bean = producaoordemService.prepareForConferenciaProducao(whereIn, null, true, null, null, false);
		request.setAttribute("isAgendaproducao", true);
		
		String obrigarLoteProducao = parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_LOTE_PRODUCAO);
		if (StringUtils.isEmpty(obrigarLoteProducao)) {
			obrigarLoteProducao = "FALSE";
		}
		request.setAttribute("OBRIGAR_LOTE_PRODUCAO", new Boolean(Boolean.valueOf(obrigarLoteProducao)));
		request.setAttribute("URL_TELA", "/" + SinedUtil.getContexto() + "/producao/crud/Producaoordem");
		request.setAttribute("METODO_AUTOCOMPLETE_LOTE", "loteestoqueService.findQtdeLoteestoqueForAutocompleteProducaoordem");
		
		return new ModelAndView("process/conferenciaProducao", "bean", bean);
	}
	
	/**
	 * M�todo que faz a cria��o da produ��o autom�tico sem a tela de confirma��o
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/08/2014
	 */
	public ModelAndView produzirAutomatico(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		try{
			ConferenciaProducaoBean bean = producaoordemService.prepareForConferenciaProducao(whereIn, null, true, null, null, false);
			
//			String valor = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.SOLICITARCOMPRAPRODUCAO);
//			if(valor != null && !valor.trim().toUpperCase().equals("REQUISICAOMATERIAL")){
				List<MateriaprimaConferenciaProducaoBean> listaMateriaprima = producaoordemService.makeListaMateriaprimaProducao(bean, true);
				for (MateriaprimaConferenciaProducaoBean it : bean.getListaMateriaprima()) {
					for (MateriaprimaConferenciaProducaoBean it_aux : listaMateriaprima) {
						if(it_aux.getMaterial() != null && 
								it.getMaterial() != null && 
								it.getMaterial().equals(it_aux.getMaterial())){
							it.setQtde(it_aux.getQtde());
							break;
						}
					}
				}
//			}
			
			producaoordemService.conferenciaCriarProducao(bean);
			request.addMessage("Ordem(ns) de produ��o criada(s) com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na cria��o das ordem(ns) de produ��o.");
			request.addError(e.getMessage());
		}
		return sendRedirectToAction("listagem");
	}
	
	/**
	 * Action que salva as ordens de produ��o da tela de confer�ncia.
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/01/2013
	 */
	public ModelAndView salvarConferenciaProducao(WebRequestContext request, ConferenciaProducaoBean bean){
		try{
			producaoordemService.conferenciaCriarProducao(bean);
			request.addMessage("Ordem(ns) de produ��o criada(s) com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na cria��o das ordem(ns) de produ��o.");
			request.addError(e.getMessage());
		}
		return sendRedirectToAction("listagem");
	}
	
	/**
	 * Ajax para buscar a lista de mat�rias-primas que ser�o usadas a 
	 * partir da quantidade dos materiais principais
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/01/2013
	 */
	public ModelAndView ajaxBuscarMateriaprimaConferencia(WebRequestContext request, ConferenciaProducaoBean bean){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(bean.getListaProducaoordemmaterial() != null){
			Boolean isAgendaproducao = Boolean.valueOf(request.getParameter("isAgendaproducao") != null ? request.getParameter("isAgendaproducao") : "false");
			List<MateriaprimaConferenciaProducaoBean> listaMateriaprima = producaoordemService.makeListaMateriaprimaProducao(bean, isAgendaproducao);
			
			HashMap<Integer, Boolean> mapaLocalPermiteEstoqueNegativo = new HashMap<Integer, Boolean>();
			if(SinedUtil.isListNotEmpty(listaMateriaprima)){
				for (MateriaprimaConferenciaProducaoBean materiaprima : bean.getListaMateriaprima()) {
					if(materiaprima.getLocalarmazenagem() != null && materiaprima.getLocalarmazenagem().getCdlocalarmazenagem() != null &&
							!mapaLocalPermiteEstoqueNegativo.containsKey(materiaprima.getLocalarmazenagem().getCdlocalarmazenagem())){
						mapaLocalPermiteEstoqueNegativo.put(materiaprima.getLocalarmazenagem().getCdlocalarmazenagem(), localarmazenagemService.getPermitirestoquenegativo(materiaprima.getLocalarmazenagem()));
					}
				}
			}
			
			jsonModelAndView.addObject("existeMateriaPrima", SinedUtil.isListNotEmpty(listaMateriaprima));
			jsonModelAndView.addObject("listaMateriaprima", listaMateriaprima);
			jsonModelAndView.addObject("mapaLocalPermiteEstoqueNegativo", mapaLocalPermiteEstoqueNegativo);
		}
		
		return jsonModelAndView;
	}
	
	/**
	 * Action em ajax para buscar a quantidade dispon�vel da mat�ria-prima.
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/01/2013
	 */
	public ModelAndView ajaxBuscarQuantidadeDiponivelMateriaprima(WebRequestContext request, MateriaprimaConferenciaProducaoBean bean){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(bean.getMaterial() != null){
			Double qtdedisponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(bean.getMaterial(), Materialclasse.PRODUTO, bean.getLocalarmazenagem(), bean.getEmpresa(), null, bean.getLoteestoque());
			if(qtdedisponivel != null){
				qtdedisponivel = SinedUtil.round(qtdedisponivel, 10);
			}
			
			Boolean localPermiteEstoqueNegativo = bean.getLocalarmazenagem() != null ? localarmazenagemService.getPermitirestoquenegativo(bean.getLocalarmazenagem()) : Boolean.TRUE;
			
			jsonModelAndView.addObject("qtdedisponivel", qtdedisponivel);
			jsonModelAndView.addObject("localPermiteEstoqueNegativo", localPermiteEstoqueNegativo);
		}
		
		return jsonModelAndView;
	}
	
	/**
	 * Action que prepara os dados para abrir a tela de confirma��o do registro de produ��o.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/01/2013
	 */
	public ModelAndView registrarProducao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		List<Producaoordem> listaProducaoordemValidacao = producaoordemService.findWithSituacaoDepartamento(whereIn);
		Departamento departamento = null;
		boolean haveRegistro = false;
		boolean erro = false;
		
		for (Producaoordem producaoordem : listaProducaoordemValidacao) {
			if(producaoordem.getProducaoordemsituacao() == null || 
					(!producaoordem.getProducaoordemsituacao().equals(Producaoordemsituacao.EM_ESPERA) &&
						!producaoordem.getProducaoordemsituacao().equals(Producaoordemsituacao.EM_ANDAMENTO)) ){
				request.addError("Somente para situa��o Em espera ou Em andamento.");
				erro = true;
			}
			
			if(!haveRegistro){
				departamento = producaoordem.getDepartamento();
			} else {
				boolean departamentoIgual = (departamento == null && producaoordem.getDepartamento() == null) ||
												(departamento != null && departamento.equals(producaoordem.getDepartamento()));
				if(!departamentoIgual){
					request.addError("Somente para ordens de produ��o do mesmo departamento.");
					erro = true;
				}
			}
			haveRegistro = true;
		}
		
		List<Producaoordem> listaProducaoordem = producaoordemService.findForRegistrarProducao(whereIn);
		for (Producaoordem producaoOrdem : listaProducaoordem) {
			for (Producaoordemmaterial producaoordemmaterial : producaoOrdem.getListaProducaoordemmaterial()) {
				boolean ultimaetapa = true;
				if(producaoordemmaterial.getProducaoetapaitem() != null){
					ultimaetapa = producaoetapaService.isUltimaEtapa(producaoordemmaterial.getProducaoetapaitem());
				}
				
				if (ultimaetapa && producaoordemmaterial.getProducaoetapa() != null && BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA.equals(producaoordemmaterial.getProducaoetapa().getBaixarEstoqueMateriaprima())) {
					if (inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(SinedDateUtils.currentDate(), producaoordemmaterial.getEmpresa(), 
							producaoordemmaterial.getLocalarmazenagem(), null)) {
						request.addError("N�o � poss�vel realizar criar ou alterar a movimenta��o de estoque,  pois o per�odo est� vinculado � um registro de invent�rio. " +
								"Para que consiga prosseguir com a a��o � necess�rio cancelar o invent�rio.");
						erro = true;
					}
				}
				
				if (!ultimaetapa && producaoordemmaterial.getProducaoetapa() != null && BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_CADA_ETAPA.equals(producaoordemmaterial.getProducaoetapa().getBaixarEstoqueMateriaprima())) {
					if (inventarioService.hasEstoqueEscrituradoNotCanceladoPosteriorDataByEmpresaLocalProjeto(SinedDateUtils.currentDate(), producaoordemmaterial.getEmpresa(), 
							producaoordemmaterial.getLocalarmazenagem(), null)) {
						request.addError("N�o � poss�vel realizar criar ou alterar a movimenta��o de estoque,  pois o per�odo est� vinculado � um registro de invent�rio. " +
								"Para que consiga prosseguir com a a��o � necess�rio cancelar o invent�rio.");
						erro = true;
					}
				}
			}
		}
		
		// SE DER ERRO REDIRECIONA PARA A TELA DE LISTAGEM
		if(erro) return sendRedirectToAction("listagem");
		
		String obrigarLoteProducao = parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_LOTE_PRODUCAO);
		
		if (StringUtils.isEmpty(obrigarLoteProducao)) {
			obrigarLoteProducao = "FALSE";
		}
		
		request.setAttribute("OBRIGAR_LOTE_PRODUCAO", new Boolean(Boolean.valueOf(obrigarLoteProducao)));
		
		request.setAttribute("listaProducaoordemmaterialTela", producaoordemService.prepareForRegistroProducao(whereIn));
		request.setAttribute("listaProducaoordemequipamentoTela", producaoordemequipamentoService.findByProducaoordem(whereIn));
		request.setAttribute("whereInProducaoordem", whereIn);
		request.setAttribute("PERMITIRPRODUZIR_QTDE_SUP_PREVISTO", parametrogeralService.getBoolean(Parametrogeral.PERMITIRPRODUZIR_QTDE_SUP_PREVISTO));
		return new ModelAndView("process/registrarProducao");
	}
	
	/**
	 * Action para salvar o registro de produ��o
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/01/2013
	 */
	public ModelAndView salvarRegistrarProducao(WebRequestContext request, Producaoordem bean){
		String whereInProducaoordem = request.getParameter("whereInProducaoordem");
		if(StringUtils.isNotBlank(whereInProducaoordem)){
			List<Producaoordem> listaProducaoordemValidacao = producaoordemService.findWithSituacaoDepartamento(whereInProducaoordem);
			boolean erro = false;
			if(SinedUtil.isListNotEmpty(listaProducaoordemValidacao)){
				for (Producaoordem producaoordem : listaProducaoordemValidacao) {
					if(producaoordem.getProducaoordemsituacao() == null || 
							(!producaoordem.getProducaoordemsituacao().equals(Producaoordemsituacao.EM_ESPERA) &&
									!producaoordem.getProducaoordemsituacao().equals(Producaoordemsituacao.EM_ANDAMENTO)) ){
						request.addError("Somente para situa��o Em espera ou Em andamento.");
						erro = true;
					}
				}
			}
			if (bean.isSalvarRegistrarProducaoLote()){
				for (Producaoordemmaterial pom : bean.getListaProducaoordemmaterial()) {
					Double qtdeRegistro = 0d;
					Double qtdePerdadescarte = 0d;
					Producaoordemmaterial beanDaSessao = getProducaoordemmaterialBySession(request, pom.getCdproducaoordemmaterial());
					
					if(beanDaSessao != null){
						if(SinedUtil.isListNotEmpty(beanDaSessao.getListaProducaoordemmaterialoperador())){
							pom.setListaProducaoordemmaterialoperador(beanDaSessao.getListaProducaoordemmaterialoperador());
							
							for (Producaoordemmaterialoperador pomo : beanDaSessao.getListaProducaoordemmaterialoperador()) {
								qtdeRegistro += pomo.getQtdeproduzido();
							}
						}
						
						if(SinedUtil.isListNotEmpty(beanDaSessao.getListaProducaoordemmaterialperdadescarte())){
							pom.setListaProducaoordemmaterialperdadescarte(beanDaSessao.getListaProducaoordemmaterialperdadescarte());
							for(Producaoordemmaterialperdadescarte pomp: beanDaSessao.getListaProducaoordemmaterialperdadescarte()){
								if(pomp.getCdproducaoordemmaterialperdadescarte() == null && pomp.getQtdeperdadescarte() != null){
									qtdePerdadescarte += pomp.getQtdeperdadescarte();
								}
							}
						}
						
						if(SinedUtil.isListNotEmpty(beanDaSessao.getListaMateriaprimaperda())){
							pom.setListaMateriaprimaperda(beanDaSessao.getListaMateriaprimaperda());
						}
						
						if((parametrogeralService.getBoolean("BLOQUEAR_CAMPO_PERDA_DESCARTE") && pom.getQtderegistroperda() != null && !pom.getQtderegistroperda().equals(qtdePerdadescarte)) || 
								(!parametrogeralService.getBoolean("BLOQUEAR_CAMPO_PERDA_DESCARTE") && pom.getQtderegistroperda() != null && qtdePerdadescarte > 0d && !pom.getQtderegistroperda().equals(qtdePerdadescarte))) {
							request.addError("N�o foi poss�vel salvar o Registro de Produ��o em Lote! A soma do(s) campo(s) 'Quantidade por Motivo' da " +
									"Tela 'Registrar Motivo Perda/Descarte' deve ser igual � quantidade informada no campo 'Registro Perda/Descarte' da " +
									"Tela 'Registrar Produ��o em Lote'. Verifique as informa��es do material " + materialService.load(pom.getMaterial()).getNome() +
									" para salvar o Registro de Produ��o em Lote.");
							erro = true;
						}
					}
					
				}
			}
			
			//Caso seja a �ltima etapa, o sistema deve verificar se a etapa corrente est� com o campo 'Permitir gerar garantia' no cadastro de etapa Marcado, caso contr�rio desconsiderar os itens abaixo:
			//Se o Tipo de Garantia cadastrado na etapa corrente n�o permite gerar garantia (quando o campo 'Gerar garantia?' estiver marcado como N�O):
			//O sistema deve impedir que o ator conclua o sub-fluxo 'Registrar Produ��o', neste caso o sistema deve exibir a mensagem 15.
			if (!erro){
				boolean bloquearRegistrarProducaoGarantia = false;
				
				if (bean.getListaProducaoordemmaterial()!=null){
					for (Producaoordemmaterial producaoordemmaterial: bean.getListaProducaoordemmaterial()){
						if (producaoordemmaterial.getProducaoetapaitem()!=null
								&& producaoordemmaterial.getProducaoetapaitem().getProducaoetapanome()!=null
								&& producaoordemmaterial.getGarantiatipo()!=null
								&& Boolean.TRUE.equals(producaoordemmaterial.getUltimaetapa())
								&& Boolean.TRUE.equals(producaoordemmaterial.getProducaoetapaitem().getProducaoetapanome().getPermitirgerargarantia())
								&& !Boolean.TRUE.equals(producaoordemmaterial.getGarantiatipo().getGeragarantia())){
							garantiareformaService.saveProducao(producaoordemmaterial.getProducaoordem(), false, false);
							bloquearRegistrarProducaoGarantia = true;
						}
					}
				}
				
				if (bloquearRegistrarProducaoGarantia){
					request.addError("Tipo de garantia definido na ordem de produ��o n�o concede garantia para o cliente, favor realizar a devolu��o do pneu.");
					erro = true;
				}
			}
			
			if(erro) return sendRedirectToAction("listagem");
		}
		
		List<Producaoordemmaterial> listaProducaoordemmaterialAux = new ArrayList<Producaoordemmaterial>();
		HashMap<Integer, List<Integer>> mapCdproducaoagendaProducaoordem = new HashMap<Integer, List<Integer>>();
		HashMap<Integer, Double> mapQtdeproduzidaProducaoemlote = new HashMap<Integer, Double>();
		StringBuilder whereInProducaoordemAux = new StringBuilder();
		StringBuilder whereInProducaoagendaAux = new StringBuilder();
		
		if (bean.isSalvarRegistrarProducaoLote() && SinedUtil.isListNotEmpty(bean.getListaProducaoordemmaterial())){
			for(Producaoordemmaterial pom: bean.getListaProducaoordemmaterial()){
				producaoordemmaterialoperadorService.preencherQtdeRegistroPerda(pom);
				mapQtdeproduzidaProducaoemlote.put(pom.getCdproducaoordemmaterial(), pom.getQtderegistro());
			}
		}
		
		try{
			producaoordemService.registroProducao(bean);
			
			if(SinedUtil.isListNotEmpty(bean.getListaProducaoordemequipamento())){
				producaoordemequipamentoService.salvarPatrimonioitemEquipamento(bean.getListaProducaoordemequipamento());
			}
			
			if(SinedUtil.isListNotEmpty(bean.getListaProducaoordemmaterial())){
				producaoordemService.salvarCamposAdicionais(bean.getListaProducaoordemmaterial());
				for(Producaoordemmaterial item : bean.getListaProducaoordemmaterial()){
					Producaoetapa producaoetapa = item.getProducaoetapa();
					if(producaoetapa != null && producaoetapa.getCdproducaoetapa() != null){ 
						producaoetapa = producaoetapaService.loadBaixarEstoqueMateriaprima(producaoetapa);
						if((BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA.equals(item.getProducaoetapa().getBaixarEstoqueMateriaprima()) &&
									Boolean.TRUE.equals(item.getUltimaetapa())) ||
							(BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_CADA_ETAPA.equals(item.getProducaoetapa().getBaixarEstoqueMateriaprima()))){	
							listaProducaoordemmaterialAux.add(item);
							if((BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA.equals(item.getProducaoetapa().getBaixarEstoqueMateriaprima()) &&
									item.getUltimaetapa() != null && item.getUltimaetapa())){
								
								Set<Producaoordemmaterialorigem> listaProducaoordemmaterialorigem = item.getListaProducaoordemmaterialorigem();
								if(listaProducaoordemmaterialorigem != null && listaProducaoordemmaterialorigem.size() > 0){
									for (Producaoordemmaterialorigem producaoordemmaterialorigem : listaProducaoordemmaterialorigem) {
										if(producaoordemmaterialorigem.getProducaoagenda() != null && 
												producaoordemmaterialorigem.getProducaoagenda().getCdproducaoagenda() != null){
											if(item.getProducaoordem() != null && item.getProducaoordem().getCdproducaoordem() != null){
												if(mapCdproducaoagendaProducaoordem.get(producaoordemmaterialorigem.getProducaoagenda().getCdproducaoagenda()) == null){
													mapCdproducaoagendaProducaoordem.put(producaoordemmaterialorigem.getProducaoagenda().getCdproducaoagenda(), new ArrayList<Integer>());
												}
												if(!mapCdproducaoagendaProducaoordem.get(producaoordemmaterialorigem.getProducaoagenda().getCdproducaoagenda()).contains(item.getProducaoordem().getCdproducaoordem())){
													mapCdproducaoagendaProducaoordem.get(producaoordemmaterialorigem.getProducaoagenda().getCdproducaoagenda()).add(item.getProducaoordem().getCdproducaoordem());
												}
											}
											whereInProducaoagendaAux.append(producaoordemmaterialorigem.getProducaoagenda().getCdproducaoagenda().toString()).append(",");
										}
									}
								}
							}else if(item.getProducaoordem() != null && item.getProducaoordem().getCdproducaoordem() != null){
								whereInProducaoordemAux.append(item.getProducaoordem().getCdproducaoordem()).append(",");
							}
						}
					}else {
						listaProducaoordemmaterialAux.add(item);
						if(item.getProducaoordem() != null && item.getProducaoordem().getCdproducaoordem() != null){
							whereInProducaoordemAux.append(item.getProducaoordem().getCdproducaoordem()).append(",");
						}
					}
				}
			} else {
				request.addMessage("Registro de produ��o realizado com sucesso.");
			}
		} catch (Exception e) {
			request.addError("Erro no registro de produ��o.");
			request.addError(e.getMessage());
			e.printStackTrace();
		}
		if(SinedUtil.isListNotEmpty(listaProducaoordemmaterialAux) && (StringUtils.isNotBlank(whereInProducaoordemAux.toString()) || StringUtils.isNotBlank(whereInProducaoagendaAux.toString()))){
			String whereInAgenda = StringUtils.isNotBlank(whereInProducaoagendaAux.toString()) ? whereInProducaoagendaAux.substring(0, whereInProducaoagendaAux.length()-1) : null;
			String whereInProducao = StringUtils.isNotBlank(whereInProducaoordemAux.toString()) ? whereInProducaoordemAux.substring(0, whereInProducaoordemAux.length()-1) : null;
			if (bean.isSalvarRegistrarProducaoLote()){
				for(Producaoordemmaterial pom: listaProducaoordemmaterialAux){
					if(mapQtdeproduzidaProducaoemlote.containsKey(pom.getCdproducaoordemmaterial())){
						Double qtdeProduzida = mapQtdeproduzidaProducaoemlote.get(pom.getCdproducaoordemmaterial());
						pom.setQtderegistro(qtdeProduzida);
					}
				}
			}
			ConferenciaProducaoBean conferenciaProducaoBean = producaoordemService.prepareForConferenciaProducao(whereInAgenda, whereInProducao, false, listaProducaoordemmaterialAux, mapCdproducaoagendaProducaoordem, bean.isSalvarRegistrarProducaoLote());

//			for(Producaoordemmaterial pom : listaProducaoordemmaterialAux){
//				for(Producaoordemmaterial pom2 : conferenciaProducaoBean.getListaProducaoordemmaterial()){
//					pom2.setQtde(pom.getQtderegistro());
//					pom2.setEncontrado(Boolean.TRUE);
//					pom2.setWhereInProducaoordem(whereInProducaoordemAux.substring(0, whereInProducaoordemAux.length()-1));
//				}
//			}
			
			if(conferenciaProducaoBean != null && SinedUtil.isListNotEmpty(conferenciaProducaoBean.getListaMateriaprima())){
				for(MateriaprimaConferenciaProducaoBean item : conferenciaProducaoBean.getListaMateriaprima()){
					if(item.getQtdedisponivel() != null){
						item.setQtdedisponivel(SinedUtil.round(item.getQtdedisponivel(), 10));
					}
				}
			}
			
			if(SinedUtil.isRecapagem()){
				return baixarMateriasPrimasOrdemproducao(request, conferenciaProducaoBean);
			}
			
			String obrigarLoteProducao = parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_LOTE_PRODUCAO);
			if (StringUtils.isEmpty(obrigarLoteProducao)) {
				obrigarLoteProducao = "FALSE";
			}
			request.setAttribute("OBRIGAR_LOTE_PRODUCAO", new Boolean(Boolean.valueOf(obrigarLoteProducao)));
			request.setAttribute("URL_TELA", "/" + SinedUtil.getContexto() + "/producao/crud/Producaoordem");
			request.setAttribute("METODO_AUTOCOMPLETE_LOTE", "loteestoqueService.findQtdeLoteestoqueForAutocompleteProducaoordem");
			
			if(conferenciaProducaoBean != null && SinedUtil.isListNotEmpty(conferenciaProducaoBean.getListaMateriaprima()) && StringUtils.isNotBlank(whereInProducaoordem)){
				conferenciaProducaoBean.setWhereInProducaoordemForConclusao(whereInProducaoordem);
				producaoordemService.updateSituacao(whereInProducaoordem, Producaoordemsituacao.MATERIA_PRIMA_PENDENTE);
			}
			request.setAttribute("fromProducaoordem", true);
			conferenciaProducaoBean.setIdentificadorsessao(new SimpleDateFormat("yyyyMMddHHmmssSSSS").format(System.currentTimeMillis()));
			request.getSession().setAttribute("baixarMateriasPrimasOrdemproducaoSessao" + conferenciaProducaoBean.getIdentificadorsessao(), true);
			return new ModelAndView("process/conferenciaProducao", "bean", conferenciaProducaoBean);
		} else {
			request.addMessage("Registro de produ��o realizado com sucesso.");
		}
		
		return sendRedirectToAction("listagem");
	}
	
	public ModelAndView baixarMateriaprimaPendente(WebRequestContext request, ConferenciaProducaoBean bean){
		String whereInProducaoordem = SinedUtil.getItensSelecionados(request);
		if(whereInProducaoordem == null || whereInProducaoordem.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		List<Producaoordem> listaProducaoordemValidacao = producaoordemService.findWithSituacaoDepartamento(whereInProducaoordem);
		boolean erro = false;
		
		for (Producaoordem producaoordem : listaProducaoordemValidacao) {
			if(producaoordem.getProducaoordemsituacao() == null || 
					(!producaoordem.getProducaoordemsituacao().equals(Producaoordemsituacao.MATERIA_PRIMA_PENDENTE)) ){
				request.addError("Somente para situa��o Baixa de mat�ria-prima pendente.");
				erro = true;
			}
		}
		if(erro) return sendRedirectToAction("listagem");
		
		List<Producaoordemmaterial> listaProducaoordemmaterialAux = producaoordemmaterialService.findByProducaoordem(whereInProducaoordem, null);
		
		String obrigarLoteProducao = parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_LOTE_PRODUCAO);
		if (StringUtils.isEmpty(obrigarLoteProducao)) {
			obrigarLoteProducao = "FALSE";
		}
		request.setAttribute("OBRIGAR_LOTE_PRODUCAO", new Boolean(Boolean.valueOf(obrigarLoteProducao)));
		request.setAttribute("URL_TELA", "/" + SinedUtil.getContexto() + "/producao/crud/Producaoordem");
		request.setAttribute("METODO_AUTOCOMPLETE_LOTE", "loteestoqueService.findQtdeLoteestoqueForAutocompleteProducaoordem");
		
		ConferenciaProducaoBean conferenciaProducaoBean = producaoordemService.prepareForConferenciaProducao(null, whereInProducaoordem, false, listaProducaoordemmaterialAux, null, false);
		conferenciaProducaoBean.setWhereInProducaoordemForConclusao(whereInProducaoordem);
		request.setAttribute("fromProducaoordem", true);
		conferenciaProducaoBean.setIdentificadorsessao(new SimpleDateFormat("yyyyMMddHHmmssSSSS").format(System.currentTimeMillis()));
		request.getSession().setAttribute("baixarMateriasPrimasOrdemproducaoSessao" + conferenciaProducaoBean.getIdentificadorsessao(), true);
		return new ModelAndView("process/conferenciaProducao", "bean", conferenciaProducaoBean);
	}
	
	public ModelAndView baixarMateriasPrimasOrdemproducao(WebRequestContext request, ConferenciaProducaoBean bean){
		try{
			if(bean.getIdentificadorsessao() != null && request.getSession().getAttribute("baixarMateriasPrimasOrdemproducaoSessao" + bean.getIdentificadorsessao()) == null){
				request.addError("O processo de produ��o j� foi realizado.");
			}else {
				if(bean.getIdentificadorsessao() != null){
					request.getSession().removeAttribute("baixarMateriasPrimasOrdemproducaoSessao" + bean.getIdentificadorsessao());
				}
				for(Producaoordemmaterial producaoordemmaterial : bean.getListaProducaoordemmaterial()){
					String whereInProducaoagenda = CollectionsUtil.listAndConcatenate(producaoordemmaterial.getListaProducaoordemmaterialorigem(), "producaoagenda.cdproducaoagenda", ",");
					String whereInProducaoagendamaterial = CollectionsUtil.listAndConcatenate(producaoordemmaterial.getListaProducaoordemmaterialorigem(), "producaoagendamaterial.cdproducaoagendamaterial", ",");
					producaoordemService.baixarEstoqueMateriaPrimaProducaoOrdem(producaoordemmaterial, whereInProducaoagenda, whereInProducaoagendamaterial, bean.getListaMateriaprima(), bean.getProducaoEmLote());
				}
				request.addMessage("Registro de produ��o realizado com sucesso.");
			}
			
			if(StringUtils.isNotBlank(bean.getWhereInProducaoordemForConclusao())){
				producaoordemService.updateSituacao(bean.getWhereInProducaoordemForConclusao(), Producaoordemsituacao.CONCLUIDA);
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro no registro de produ��o.");
			request.addError(e.getMessage());
		}
		return sendRedirectToAction("listagem");
	}
	
	public ModelAndView buscarInfoMaterialAjax(WebRequestContext request, Material material){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if (material!=null && material.getCdmaterial()!=null){
			material = materialService.loadWithProducaoetapa(material);
			
			jsonModelAndView.addObject("unidademedida", material != null && material.getUnidademedida() != null ?
					"br.com.linkcom.sined.geral.bean.Unidademedida[cdunidademedida=" +  material.getUnidademedida().getCdunidademedida() + "]" : null );
			
			jsonModelAndView.addObject("unidademedidaNome", material != null && material.getUnidademedida() != null ? material.getUnidademedida().getNome() : null );
			jsonModelAndView.addObject("nomeBanda", material != null && material.getNomeBanda() != null ? material.getNomeBanda() : null);
			jsonModelAndView.addObject("profundidadesulcoBanda", material != null && material.getProfundidadesulcoBanda() != null ? material.getProfundidadesulcoBanda() : null);
		}
		
		return jsonModelAndView;
	}
	
	/**
	 * Ajax que busca a quantidade dispon�vel em estoque.
	 *
	 * @param request
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/11/2015
	 */
	public ModelAndView buscarEstoqueAjax(WebRequestContext request, Material material){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		Localarmazenagem localarmazenagem = new Localarmazenagem(material.getCdlocalarmazenagem());
		
		Double qtdedisponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(material, Materialclasse.PRODUTO, localarmazenagem);
		jsonModelAndView.addObject("qtdedisponivel", qtdedisponivel);
		
		return jsonModelAndView;
	}
	
	/**
	 * M�todo que armazena os dados necess�rios para executar o autocomplete de lote.
	 * 
	 * @param request
	 * @author Jo�o Vitor
	 */
	public void ajaxSetDadosSessaoForLoteAutocomplete(WebRequestContext request){
		pedidovendaService.ajaxSetDadosSessaoForLoteAutocomplete(request, "PRODUCAOORDEM");
	}
	
	/**
	* M�todo que abre uma popup para registrar devolu��o
	*
	* @param request
	* @return
	* @throws Exception
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public ModelAndView registrarDevolucao(WebRequestContext request) throws Exception{
		boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false"); 
		String whereInProducaoordem = SinedUtil.getItensSelecionados(request);
		String whereInColeta = null;
		String urlRetorno = "/producao/crud/Producaoordem" + (entrada ? "?ACAO=consultar&cdproducaoordem="+whereInProducaoordem : "");
		
		if(whereInProducaoordem == null || whereInProducaoordem.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		List<Integer> listaCdPedidovendamaterial = new ArrayList<Integer>();
		StringBuilder msgDevolucao = new StringBuilder();
		List<Producaoordem> listaProducaoordemValidacao = producaoordemService.findForDevolverColeta(whereInProducaoordem);
		boolean erro = false;
		String whereInPV = "";
		String whereInPVM = "";
		StringBuilder whereInPVSB = new StringBuilder();
		StringBuilder whereInPVMSB = new StringBuilder();
		for (Producaoordem producaoordem : listaProducaoordemValidacao) {
			if(producaoordem.getProducaoordemsituacao() == null || 
					(!producaoordem.getProducaoordemsituacao().equals(Producaoordemsituacao.EM_ESPERA) 
						&& !producaoordem.getProducaoordemsituacao().equals(Producaoordemsituacao.EM_ANDAMENTO)) ){
				if(producaoordem.getProducaoordemsituacao().equals(Producaoordemsituacao.CONCLUIDA)){
					msgDevolucao.append(producaoordem.getCdproducaoordem()).append(",");
				}else {
					request.addError("Somente para situa��o Em espera ou Em andamento.");
				}
				erro = true;
			}
			
			if(!erro && producaoordem.getListaProducaoordemmaterial() != null){
				for(Producaoordemmaterial pom : producaoordem.getListaProducaoordemmaterial()){
					if(pom.getPedidovendamaterial() != null && pom.getPedidovendamaterial().getCdpedidovendamaterial() != null){
						listaCdPedidovendamaterial.add(pom.getPedidovendamaterial().getCdpedidovendamaterial());
						whereInPVMSB.append(pom.getPedidovendamaterial().getCdpedidovendamaterial()).append(",");
						if(pom.getListaProducaoordemmaterialorigem() != null){
							for(Producaoordemmaterialorigem pomo : pom.getListaProducaoordemmaterialorigem()){
								if(pomo.getProducaoagenda() != null && pomo.getProducaoagenda().getPedidovenda() != null 
										&& pomo.getProducaoagenda().getPedidovenda().getCdpedidovenda() != null){
									Pedidovenda pedidovenda = pomo.getProducaoagenda().getPedidovenda();
									if(Pedidovendasituacao.CONFIRMADO.equals(pomo.getProducaoagenda().getPedidovenda().getPedidovendasituacao())){
										request.addError("N�o � poss�vel interromper ordem de produ��o vinculada ao pedido " + pedidovenda.getCdpedidovenda() + 
												", pedido est� confirmado.");
										erro = true;
										break;
									}else {
										whereInPVSB.append(pedidovenda.getCdpedidovenda()).append(",");
									}
								}
							}
						}
						if(erro){
							break;
						}
					}
					pom.setWhereInProducaoagendamaterial(producaoordemmaterialService.getWhereInProducaoagendamaterialOrigem(pom));
				}					
			}
		}
		
		if(erro){
			if(msgDevolucao.length() > 0){
				request.addError("N�o h� material dispon�vel para devolu��o, a(s) ordem(ns) de produ��o " + msgDevolucao.substring(0, msgDevolucao.length()-1) + " j� est�(�o) conclu�da(s).");
			}
			return new ModelAndView("redirect:" + urlRetorno);
		} 
		
		if(!whereInPVSB.toString().isEmpty()){
			whereInPV = whereInPVSB.substring(0, whereInPVSB.length()-1);
		}
		if(!whereInPVMSB.toString().isEmpty()){
			whereInPVM = whereInPVMSB.substring(0, whereInPVMSB.length()-1);
		}
		
		if(!whereInPV.isEmpty()){
			List<Coleta> listaColeta = coletaService.findByWhereInPedidovenda(whereInPV, whereInPVM);
			if(SinedUtil.isListNotEmpty(listaColeta)){
				whereInColeta = SinedUtil.listAndConcatenate(listaColeta, "cdcoleta", ",");					
			} 
		}
		
		List<Coleta> listaColeta = null;
		if(StringUtils.isNotBlank(whereInColeta)){
			try {
				listaColeta = coletaService.findForRegistrarDevolucao(whereInColeta, listaCdPedidovendamaterial);
				producaoordemService.setInformacoesProducaoordemmaterial(listaProducaoordemValidacao, listaColeta);
			} catch (Exception e) {
				request.addError(e.getMessage());
				return new ModelAndView("redirect:" + urlRetorno);
			} 
		}	
		
		ProducaoordemdevolucaoBean bean = producaoordemService.criaProducaoordemdevolucaoBean(listaProducaoordemValidacao, listaColeta, whereInProducaoordem, whereInColeta);
		bean.setUrlretorno(urlRetorno);
		request.setAttribute("existeColeta", SinedUtil.isListNotEmpty(listaColeta));
		request.setAttribute("interromperProducao", Boolean.valueOf(request.getParameter("interromperProducao") != null ? request.getParameter("interromperProducao") : "false"));
		return new ModelAndView("process/registrarDevolucao", "bean", bean);
	}
	
	/**
	* M�todo que salva os registros de devolu��o
	*
	* @param request
	* @param bean
	* @return
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public ModelAndView salvarRegistrarDevolucao(WebRequestContext request, ProducaoordemdevolucaoBean bean){
		return producaoordemService.salvarRegistrarDevolucao(request, bean,null);
	}
	
	/**
	* M�todo que estorna a ordem de produ��o
	*
	* @see br.com.linkcom.sined.geral.service.ProducaoordemService#estornarOrdemproducao(List<Producaoordem> listaProducaoordem)
	*
	* @param request
	* @return
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public ModelAndView estornar(WebRequestContext request){
		boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false"); 
		String whereIn = SinedUtil.getItensSelecionados(request);
		String urlretorno = "/producao/crud/Producaoordem" + (entrada ? "?ACAO=consultar&cdproducaoordem="+whereIn : "");
		
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		List<Producaoordem> listaProducaoordem = producaoordemService.findForEstornar(whereIn);
		
		if(SinedUtil.isListEmpty(listaProducaoordem)){
			request.addError("Nenhuma ordem de produ��o encontrada para realizar o estorno.");
			return new ModelAndView("redirect:" + urlretorno);
		}
		
		HashMap<Producaoordemmaterial, List<ColetaMaterial>> mapOrdemColeta = new HashMap<Producaoordemmaterial, List<ColetaMaterial>>();
		for(Producaoordem producaoordem : listaProducaoordem){
			if(!Producaoordemsituacao.CONCLUIDA.equals(producaoordem.getProducaoordemsituacao()) && 
					!Producaoordemsituacao.EM_ANDAMENTO.equals(producaoordem.getProducaoordemsituacao())){
				request.addError("S� � permitido o estorno para as ordens de produ��o na situa��o de 'Conclu�da' ou 'Em Andamento'.");
				return new ModelAndView("redirect:" + urlretorno);
			}else if(SinedUtil.isListNotEmpty(producaoordem.getListaProducaoordemmaterial())){
				List<Integer> listaCdPedidovendamaterial =  new ArrayList<Integer>();
				StringBuilder whereInPVSB = new StringBuilder();
				StringBuilder whereInPVMSB = new StringBuilder();
				for(Producaoordemmaterial pom : producaoordem.getListaProducaoordemmaterial()){
					if(pom.getPedidovendamaterial() != null && pom.getPedidovendamaterial().getCdpedidovendamaterial() != null && pom.getQtdeperdadescarte() != null && pom.getQtdeperdadescarte() > 0){
						listaCdPedidovendamaterial.add(pom.getPedidovendamaterial().getCdpedidovendamaterial());
						whereInPVMSB.append(pom.getPedidovendamaterial().getCdpedidovendamaterial()).append(",");
						if(pom.getListaProducaoordemmaterialorigem() != null){
							for(Producaoordemmaterialorigem pomo : pom.getListaProducaoordemmaterialorigem()){
								if(pomo.getProducaoagenda() != null && pomo.getProducaoagenda().getPedidovenda() != null 
										&& pomo.getProducaoagenda().getPedidovenda().getCdpedidovenda() != null){
									whereInPVSB.append(pomo.getProducaoagenda().getPedidovenda().getCdpedidovenda()).append(",");
								}
							}
						}
					}
				}					
				if(whereInPVSB.length() > 0 && whereInPVMSB.length() > 0){
					List<Coleta> listaColeta = coletaService.findByWhereInPedidovenda(whereInPVSB.substring(0, whereInPVSB.length()-1), whereInPVMSB.substring(0, whereInPVMSB.length()-1));
					if(SinedUtil.isListNotEmpty(listaColeta)){
						String whereInColeta = SinedUtil.listAndConcatenate(listaColeta, "cdcoleta", ",");
						if(notafiscalprodutoColetaService.haveNotaDevolucaoNaoCanceladaByColeta(whereInColeta, whereInPVMSB.substring(0, whereInPVMSB.length()-1))){
							List<NotafiscalprodutoColeta> lista = notafiscalprodutoColetaService.findNotaDevolucao(whereInColeta, whereInPVMSB.substring(0, whereInPVMSB.length()-1), Boolean.TRUE);
							String linkNota = getLinkNotaDevolucao(lista);
							request.addError("A quantidade de descarte do material na ordem de produ��o est� vinculada a nota " + linkNota + ". Favor cancelar primeiro a nota antes de efetuar o estorno.");
							return new ModelAndView("redirect:" + urlretorno);
						}
						StringBuilder sbWhereInColetamaterial = new StringBuilder();
						for(Coleta coleta : listaColeta){
							if(SinedUtil.isListNotEmpty(coleta.getListaColetaMaterial())){
								for(ColetaMaterial coletaMaterial : coleta.getListaColetaMaterial()){
									if(coletaMaterial.getQuantidadedevolvida() != null && coletaMaterial.getQuantidadedevolvida() > 0){
										for(Producaoordemmaterial pom : producaoordem.getListaProducaoordemmaterial()){
											if(SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())) {
												for(ColetaMaterialPedidovendamaterial itemServico : coletaMaterial.getListaColetaMaterialPedidovendamaterial()) {
													if(pom.getPedidovendamaterial() != null && pom.getPedidovendamaterial().getCdpedidovendamaterial() != null 
															&& pom.getQtdeperdadescarte() != null && pom.getQtdeperdadescarte() > 0 
															&& pom.getPedidovendamaterial().equals(itemServico.getPedidovendamaterial())
															&& coletaMaterial.getQuantidadedevolvida() >= pom.getQtdeperdadescarte()){
														List<ColetaMaterial> lista = mapOrdemColeta.get(coletaMaterial);
														if(lista == null) lista = new ArrayList<ColetaMaterial>();
														List<ColetaMaterial> listaColetaMaterial = mapOrdemColeta.get(pom);
														ColetaMaterial coletaMaterialMap = null;
														Integer posicao = null;
														if(SinedUtil.isListNotEmpty(listaColetaMaterial)){
															posicao = listaColetaMaterial.indexOf(coletaMaterial);
															if(posicao >= 0) coletaMaterialMap = listaColetaMaterial.get(posicao);
														}
														if(coletaMaterialMap == null) coletaMaterialMap = new ColetaMaterial(coletaMaterial.getCdcoletamaterial());
														
														if(coletaMaterialMap.getQtdeDevolvidaEstonar() == null){
															coletaMaterialMap.setQtdeDevolvidaEstonar(pom.getQtdeperdadescarte());
														}else if(coletaMaterialMap.getQtdeDevolvidaEstonar() + pom.getQtdeperdadescarte() <= coletaMaterial.getQuantidadedevolvida()){
															coletaMaterialMap.setQtdeDevolvidaEstonar(coletaMaterialMap.getQtdeDevolvidaEstonar() + pom.getQtdeperdadescarte());
														}
														
														if(posicao == null || posicao < 0){
															lista.add(coletaMaterialMap);
														}
														mapOrdemColeta.put(pom, lista);
														break;
													}
												}
											}
											
//											if(pom.getPedidovendamaterial() != null && pom.getPedidovendamaterial().getCdpedidovendamaterial() != null && pom.getQtdeperdadescarte() != null && pom.getQtdeperdadescarte() > 0 &&
//													pom.getPedidovendamaterial().equals(coletaMaterial.getPedidovendamaterial())){
//												if(coletaMaterial.getQuantidadedevolvida() >= pom.getQtdeperdadescarte()){
//													List<ColetaMaterial> lista = mapOrdemColeta.get(coletaMaterial);
//													if(lista == null) lista = new ArrayList<ColetaMaterial>();
//													List<ColetaMaterial> listaColetaMaterial = mapOrdemColeta.get(pom);
//													ColetaMaterial coletaMaterialMap = null;
//													Integer posicao = null;
//													if(SinedUtil.isListNotEmpty(listaColetaMaterial)){
//														posicao = listaColetaMaterial.indexOf(coletaMaterial);
//														if(posicao >= 0) coletaMaterialMap = listaColetaMaterial.get(posicao);
//													}
//													if(coletaMaterialMap == null) coletaMaterialMap = new ColetaMaterial(coletaMaterial.getCdcoletamaterial());
//													
//													if(coletaMaterialMap.getQtdeDevolvidaEstonar() == null){
//														coletaMaterialMap.setQtdeDevolvidaEstonar(pom.getQtdeperdadescarte());
//													}else if(coletaMaterialMap.getQtdeDevolvidaEstonar() + pom.getQtdeperdadescarte() <= coletaMaterial.getQuantidadedevolvida()){
//														coletaMaterialMap.setQtdeDevolvidaEstonar(coletaMaterialMap.getQtdeDevolvidaEstonar() + pom.getQtdeperdadescarte());
//													}
//													
//													if(posicao == null || posicao < 0){
//														lista.add(coletaMaterialMap);
//													}
//													mapOrdemColeta.put(pom, lista);
//												}
//											}
										}
									}
								}
								sbWhereInColetamaterial.append(SinedUtil.listAndConcatenate(coleta.getListaColetaMaterial(), "cdcoletamaterial", ",")).append(",");
							}
						}
						if(sbWhereInColetamaterial.length() > 0){
							producaoordem.setWhereInColetamaterial(sbWhereInColetamaterial.substring(0, sbWhereInColetamaterial.length()-1));
						}
					} 
				}
			}
		}
		
		try {
			producaoordemService.estornarOrdemproducao(listaProducaoordem, mapOrdemColeta);
			request.addMessage("Estorno realizado com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro ao estorno ordem de produ��o: " + e.getMessage());
		}
		
		return new ModelAndView("redirect:" + urlretorno); 
	}
	
	private String getLinkNotaDevolucao(List<NotafiscalprodutoColeta> lista) {
		StringBuilder sb = new StringBuilder();
		if(SinedUtil.isListNotEmpty(lista)){
			List<Integer> listaCdnota = new ArrayList<Integer>();
			for(NotafiscalprodutoColeta nc : lista){
				if(nc.getNotafiscalproduto() != null && nc.getNotafiscalproduto().getCdNota() != null && !listaCdnota.contains(nc.getNotafiscalproduto().getCdNota())){
					listaCdnota.add(nc.getNotafiscalproduto().getCdNota());
					if(sb.length() > 0) sb.append(",");
					sb.append(SinedUtil.makeLinkHistorico(nc.getNotafiscalproduto().getCdNota().toString(), 
							nc.getNotafiscalproduto().getNumero() != null ? nc.getNotafiscalproduto().getNumero() : nc.getNotafiscalproduto().getCdNota().toString(), 
							"visualizarNota"));
				}
			}
		}
		
		return sb.toString();
	}
	/**
	* M�todo que abre a popup de campos adiconais da etapa de produ��o
	*
	* @param request
	* @param producaoordemmaterial
	* @return
	* @since 26/02/2016
	* @author Luiz Fernando
	*/
	public ModelAndView popUpCamposAdicionais(WebRequestContext request, Producaoordemmaterial producaoordemmaterial){
		request.setAttribute("consultar", Boolean.valueOf(request.getParameter("consultar") != null ? request.getParameter("consultar") : "false"));
		request.setAttribute("index", request.getParameter("index"));
		request.setAttribute("obrigarCamposAdicionais", Boolean.valueOf(request.getParameter("obrigarCamposAdicionais") != null ? request.getParameter("obrigarCamposAdicionais") : "false"));
		return new ModelAndView("direct:/crud/popup/popUpCamposAdicionais", "bean", producaoordemmaterial);
	}
	
	/**
	* M�todo que busca os equipamentos do material
	*
	* @param request
	* @param material
	* @return
	* @since 26/02/2016
	* @author Luiz Fernando
	*/
	public ModelAndView buscarEquipamentosAjax(WebRequestContext request, Material material){
		List<Material> listaMaterialEquipamento = new ArrayList<Material>();
		List<Material> lista = materialService.findForProducaoordemEquipamentos(material.getCdmaterial().toString());
		if(SinedUtil.isListNotEmpty(lista)){
			for(Material mat : lista){
				if(SinedUtil.isListNotEmpty(mat.getListaProducao())){
					for(Materialproducao materialproducao : mat.getListaProducao()){
						if(materialproducao.getMaterial() != null && materialproducao.getMaterial().getPatrimonio() != null && 
								materialproducao.getMaterial().getPatrimonio()){
							listaMaterialEquipamento.add(materialproducao.getMaterial());
						}
					}
				}
			}
		}
		return new JsonModelAndView().addObject("listaMaterialEquipamento", listaMaterialEquipamento);
	}
	
	
	
	/**
	 * M�todo que exibe a popup para simular qtde a produzir
	 * @param request
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 * @since 08/01/2014
	 */
	public ModelAndView abrirMateriaprima(WebRequestContext request, Material bean){
		Material material = materialService.loadMaterialComMaterialproducao(bean);
		if(material != null){
			material.setProduto_largura(material.getProduto_largura());
			material.setProduto_altura(material.getProduto_altura());
			material.setProduto_comprimento(material.getProduto_comprimento());
			material.setProduto_qtde(bean.getProduto_qtde());
			material.setProducaoordemtrans(bean.getProducaoordemtrans());
			material.setPneu(bean.getPneu());
			if(bean.getPneu() != null && bean.getPneu().getMaterialbanda() != null){
				Material materialbanda = materialService.load(bean.getPneu().getMaterialbanda(), "material.cdmaterial, material.nome, material.identificacao, material.unidademedida");
				if(materialbanda != null){
					bean.getPneu().getMaterialbanda().setNome(materialbanda.getNome());
					bean.getPneu().getMaterialbanda().setIdentificacao(materialbanda.getIdentificacao());
					bean.getPneu().getMaterialbanda().setUnidademedida(materialbanda.getUnidademedida());
				}
			}
			
			List<Materiaprima> listaMateriaprima = bean.getListaMateriaprima();
			
			if(listaMateriaprima == null || listaMateriaprima.isEmpty()){
				material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), false));
				
				try{
					material.setProduto_altura(bean.getProduto_altura());
					material.setProduto_largura(bean.getProduto_largura());
					material.setQuantidade(bean.getProduto_qtde());
					material.setProducaoordemtrans(bean.getProducaoordemtrans());
					materialproducaoService.getValorvendaproducao(material);			
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				for (Materialproducao materialproducao : material.getListaProducao()) {
					if(materialproducao.getMaterial() != null){
						materialproducao.setUnidademedida(materialproducao.getMaterial().getUnidademedida());
					}
					if(materialproducao.getConsumo() != null && materialproducao.getConsumo() > 0 && bean.getProduto_qtde() != null){
						materialproducao.setConsumo(materialproducao.getConsumo() * bean.getProduto_qtde());
					}
				}
				
				Producaoetapanome producaoetapanome = null;
				if(bean.getProducaoetapaitemtrans() != null && bean.getProducaoetapaitemtrans().getCdproducaoetapaitem() != null){
					Producaoetapaitem producaoetapaitem = producaoetapaitemService.load(bean.getProducaoetapaitemtrans());
					if(producaoetapaitem != null){
						producaoetapanome = producaoetapaitem.getProducaoetapanome();
					}
				}
				listaMateriaprima = producaoagendaService.createListaMateriaprima(material.getListaProducao(), material.getPneu(), producaoetapanome, true);
			}
			material.setListaMateriaprima(listaMateriaprima);
			producaoagendaService.verificaTrocaMaterialsimilar(material.getListaMateriaprima());
		}
		request.setAttribute("index", request.getParameter("index"));
		request.setAttribute("consultar", Boolean.valueOf(request.getParameter("consultar") != null ? request.getParameter("consultar") : "false"));
		
		String obrigarLoteProducao = parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_LOTE_PRODUCAO);
		
		if (StringUtils.isEmpty(obrigarLoteProducao)) {
			obrigarLoteProducao = "FALSE";
		}
		
		material.setProducaoetapaitemtrans(bean.getProducaoetapaitemtrans());
		request.setAttribute("OBRIGAR_LOTE_PRODUCAO", new Boolean(Boolean.valueOf(obrigarLoteProducao)));
		request.setAttribute("URL_TELA", "/" + SinedUtil.getContexto() + "/producao/crud/Producaoordem");
		request.setAttribute("METODO_AUTOCOMPLETE_LOTE", "loteestoqueService.findQtdeLoteestoqueForAutocompleteProducaoordem");
		if(bean.getProducaoetapaitemtrans() != null){
			Producaoetapaitem producaoetapaitem = producaoetapaitemService.load(bean.getProducaoetapaitemtrans(), "producaoetapaitem.cdproducaoetapaitem, producaoetapaitem.producaoetapanome");
			if(producaoetapaitem != null && producaoetapaitem.getProducaoetapanome() != null){
				request.setAttribute("PRODUCAOETAPANOME_PRODUCAOORDEM", producaoetapaitem.getProducaoetapanome());
			}
		}
		request.setAttribute("RECALCULAR_QUANTIDADES_ORDEMPRODUCAO", parametrogeralService.getBoolean(Parametrogeral.RECALCULAR_QUANTIDADES_ORDEMPRODUCAO));
		return new ModelAndView("direct:/crud/popup/popUpVisualizarmateriaprima", "bean", material);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see 
	*
	* @param request
	* @param materiaprima
	* @return
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public ModelAndView converteUnidademedida(WebRequestContext request, Materiaprima materiaprima){
		return producaoagendaService.converteUnidademedida(request, materiaprima);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.ProducaoagendaService#ajaxPreencheComboUnidadeMedida(WebRequestContext request)
	*
	* @param request
	* @return
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxPreencheComboUnidadeMedida(WebRequestContext request){
		return producaoagendaService.ajaxPreencheComboUnidadeMedida(request); 
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.ProducaoagendaService#abrirTrocaMaterial(WebRequestContext request)
	*
	* @param request
	* @return
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public ModelAndView abrirTrocaMaterial(WebRequestContext request){
		return producaoagendaService.abrirTrocaMaterial(request);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.ProducaoagendaService#recalcularMaterialProducaoAJAX(WebRequestContext request, Material bean)
	*
	* @param request
	* @param bean
	* @return
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public ModelAndView recalcularMaterialProducaoAJAX(WebRequestContext request, Material bean){
		return producaoagendaService.recalcularMaterialProducaoAJAX(request, bean);
	}
	
	/**
	* M�todo que abre a popup para registrar equipamento
	*
	* @param request
	* @return
	* @since 26/02/2016
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public ModelAndView abrirRegistrarEquipamento(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		List<Producaoordem> lista = producaoordemService.findWithSituacao(whereIn);
		if(SinedUtil.isListNotEmpty(lista)){
			for(Producaoordem producaoordem : lista){
				if(!Producaoordemsituacao.EM_ESPERA.equals(producaoordem.getProducaoordemsituacao()) && 
						!Producaoordemsituacao.EM_ANDAMENTO.equals(producaoordem.getProducaoordemsituacao())){
					request.addError("N�o � poss�vel registrar equipamento para ordem de produ��o diferente de 'Em espera' e 'Em andamento'.");
					SinedUtil.redirecionamento(request, "/producao/crud/Producaoordem");
					return null;
				}
			}
		}
		
		Producaoordem producaoordem = new Producaoordem();
		producaoordem.setListaProducaoordemequipamento(SinedUtil.listToSet(producaoordemequipamentoService.findByProducaoordem(whereIn, Boolean.TRUE), Producaoordemequipamento.class));
		
		request.setAttribute("entrada", Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false"));
		request.setAttribute("whereIn", whereIn);
		request.setAttribute("exibirNovaLInha", whereIn.split(",").length == 1);
		return new ModelAndView("direct:/crud/popup/popUpRegistrarEquipamento", "bean", producaoordem);
	}
	
	public ModelAndView abrirPopUpConsumoMateriaprimaAposCriarNotaDevolucao(WebRequestContext request) {
		return producaoordemService.abrirPopUpConsumoMateriaprimaAposCriarNotaDevolucao(request);
	}
	
	/**
	* M�todo que salva os dados da popup de registro de equipamento
	*
	* @param request
	* @param producaoordem
	* @since 26/02/2016
	* @author Luiz Fernando
	*/
	public void salvarRegistrarEquipamento(WebRequestContext request, Producaoordem producaoordem){
		String whereIn = request.getParameter("whereIn");
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		try{
			if(SinedUtil.isListNotEmpty(producaoordem.getListaProducaoordemequipamento())){
				producaoordemequipamentoService.registrarEquipamento(producaoordem.getListaProducaoordemequipamento());
			}
			request.addMessage("Registro de equipamento realizado com sucesso.");
		} catch (Exception e) {
			request.addError("Erro ao registrar equipamento.");
			request.addError(e.getMessage());
			e.printStackTrace();
		}
		
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false"); 
		SinedUtil.redirecionamento(request, "/producao/crud/Producaoordem" + (entrada ? "?ACAO=consultar&cdproducaoordem="+whereIn : ""));
	}
	
	public ModelAndView saveConsumoMateriaPrima(WebRequestContext request, ConsumoMateriaprimaProducaoBean bean){
		return producaoordemService.saveConsumoMateriaPrima(request, bean);
	}
	
	public ModelAndView registrarProducaoLote(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if (whereIn==null || whereIn.trim().equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		List<Producaoordem> listaProducaoordemValidacao = producaoordemService.findWithSituacaoDepartamento(whereIn);
		Departamento departamento = null;
		boolean haveRegistro = false;
		boolean erro = false;
		
		for (Producaoordem producaoordem: listaProducaoordemValidacao) {
			if (!Producaoordemsituacao.EM_ESPERA.equals(producaoordem.getProducaoordemsituacao()) &&
				!Producaoordemsituacao.EM_ANDAMENTO.equals(producaoordem.getProducaoordemsituacao())){
				request.addError("Somente para situa��o Em espera.");
				erro = true;
			}
			
			if (!haveRegistro){
				departamento = producaoordem.getDepartamento();
			}else {
				boolean departamentoIgual = (departamento == null && producaoordem.getDepartamento() == null) ||
												(departamento != null && departamento.equals(producaoordem.getDepartamento()));
				if (!departamentoIgual){
					request.addError("Somente para ordens de produ��o do mesmo departamento.");
					erro = true;
				}
			}
			
			haveRegistro = true;
		}
		
		List<Producaoordemmaterial> listaProducaoordemmaterialTela = producaoordemService.prepareForRegistroProducaoemlote(whereIn);
		
		if (!erro){
			for (Producaoordemmaterial producaoordemmaterial: listaProducaoordemmaterialTela){
				if (producaoordemmaterial.getProducaoetapaitem()!=null 
						&& producaoordemmaterial.getProducaoetapaitem().getProducaoetapa()!=null
						&& (Boolean.TRUE.equals(producaoordemmaterial.getProducaoetapaitem().getProducaoetapa().getNaoagruparmaterial()) || !BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA.equals(producaoordemmaterial.getProducaoetapaitem().getProducaoetapa().getBaixarEstoqueMateriaprima()))){
					request.addError("Para produ��o em lote, o ciclo deve estar configurado para 'n�o separar ordens de produ��o por material' e baixar estoque de mat�rias-primas 'Ao concluir �ltima etapa'.");
					erro = true;
					break;
				}
			}
		}
		//SE DER ERRO REDIRECIONA PARA A TELA DE LISTAGEM
		if (erro){
			return sendRedirectToAction("listagem");
		}

		String obrigarLoteProducao = parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_LOTE_PRODUCAO);
		
		if (StringUtils.isEmpty(obrigarLoteProducao)) {
			obrigarLoteProducao = "FALSE";
		}
		String identificadorTela = new SimpleDateFormat("yyyyMMddHHmmssSSSS").format(System.currentTimeMillis());
		request.setAttribute("identificadorTela", identificadorTela);
		request.setAttribute("OBRIGAR_LOTE_PRODUCAO", new Boolean(Boolean.valueOf(obrigarLoteProducao)));
		request.setAttribute("listaProducaoordemmaterialTela", listaProducaoordemmaterialTela);
		request.getSession().setAttribute("listaProducaoordemmaterialTela"+identificadorTela, listaProducaoordemmaterialTela);
		request.setAttribute("listaProducaoordemequipamentoTela", producaoordemequipamentoService.findByProducaoordem(whereIn));
		request.setAttribute("whereInProducaoordem", whereIn);
		request.setAttribute("BLOQUEAR_CAMPO_PERDA_DESCARTE", parametrogeralService.getBoolean("BLOQUEAR_CAMPO_PERDA_DESCARTE"));
		
		return new ModelAndView("process/registrarProducaoLote");
	}
	
	public ModelAndView popupTrocarProdutoFinal(WebRequestContext request, Producaoordemmaterial producaoordemmaterial) throws Exception {
		Producaoordem producaoordem = new Producaoordem();
		producaoordem.setProducaoordemmaterial(producaoordemmaterialService.load(producaoordemmaterial, "producaoordemmaterial.cdproducaoordemmaterial, producaoordemmaterial.qtde, producaoordemmaterial.material"));
		
		request.setAttribute("whereInProducaoordem", request.getParameter("whereInProducaoordem"));
		
		return new ModelAndView("direct:/crud/popup/popUpTrocarProdutoFinal", "bean", producaoordem);
	}
	
	public void salvarPopupTrocarProdutoFinal(WebRequestContext request, Producaoordem producaoordem) throws Exception {
		producaoordemmaterialService.transactionProducaoTrocarProdutoFinal(producaoordem);
		View.getCurrent().println("<script>parent.location = '" + request.getServletRequest().getContextPath() + "/producao/crud/Producaoordem?ACAO=registrarProducaoLote&selectedItens=" + request.getParameter("whereInProducaoordem") + "';</script>");
	}
	
	public ModelAndView popupOperador(WebRequestContext request, Producaoordemmaterial producaoordemmaterial) throws Exception {
		Producaoordemmaterial bean = getProducaoordemmaterialBySession(request, producaoordemmaterial.getCdproducaoordemmaterial());
		bean.setQtderegistro(producaoordemmaterial.getQtderegistro());
		bean.setQtderegistroperda(producaoordemmaterial.getQtderegistroperda());
		request.setAttribute("index", request.getParameter("index"));
		request.setAttribute("identificadorTela", request.getParameter("identificadorTela"));
		request.setAttribute("whereInProducaoordem", request.getParameter("whereInProducaoordem"));
		request.setAttribute("listaMotivodevolucao", motivodevolucaoService.findForMotivodevolucao());
		request.setAttribute("BLOQUEAR_CAMPO_PERDA_DESCARTE", parametrogeralService.getBoolean("BLOQUEAR_CAMPO_PERDA_DESCARTE"));
		return new ModelAndView("direct:/crud/popup/popUpOperador", "bean", bean);
	}
	
	public void salvarPopupOperador(WebRequestContext request, Producaoordemmaterial producaoordemmaterial) throws Exception {
		producaoordemmaterialService.transactionProducaoOperador(producaoordemmaterial);
		View.getCurrent().println("<script>parent.location = '" + request.getServletRequest().getContextPath() + "/producao/crud/Producaoordem?ACAO=registrarProducaoLote&selectedItens=" + request.getParameter("whereInProducaoordem") + "';</script>");
	}
	
	public ModelAndView buscarTipoGarantia(WebRequestContext request, Producaoordemmaterial producaoordemmaterial){
		Garantiatipo garantiatipo = producaoordemmaterial.getGarantiatipo();
		List<Garantiatipopercentual> listaGarantiatipopercentual = new ArrayList<Garantiatipopercentual>();
		boolean desabilitaPorcentagem = false;
		
		if (garantiatipo!=null){
			garantiatipo = garantiatipoService.loadForEntrada(garantiatipo);
			if (Boolean.TRUE.equals(garantiatipo.getGeragarantia())){
				listaGarantiatipopercentual.addAll(garantiatipo.getListaGarantiatipopercentual());
				Collections.sort(listaGarantiatipopercentual, new Comparator<Garantiatipopercentual>() {
																@Override
																public int compare(Garantiatipopercentual o1, Garantiatipopercentual o2) {
																	return o2.getPercentual().compareTo(o1.getPercentual());
																}
				});
			}else {
				listaGarantiatipopercentual.add(new Garantiatipopercentual(0D));
				desabilitaPorcentagem = true;
			}
		}
		
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		jsonModelAndView.addObject("lista", listaGarantiatipopercentual);
		jsonModelAndView.addObject("desabilitaPorcentagem", desabilitaPorcentagem);
		
		return jsonModelAndView;
	}
	
	@Override
	protected void validateBean(Producaoordem bean, BindException errors) {
		if (bean.getListaProducaoordemconstatacao()!=null){
			Set<String> listaErro = new HashSet<String>();
			List<Producaoordemconstatacao> listaProducaoordemconstatacaoAux = new ListSet<Producaoordemconstatacao>(Producaoordemconstatacao.class, bean.getListaProducaoordemconstatacao());
					
			if (listaProducaoordemconstatacaoAux.size()>5){
				listaErro.add("� permitido no m�ximo 5 constata��es.");
			}
			
			for (int i=0; i<listaProducaoordemconstatacaoAux.size(); i++){
				Motivodevolucao motivodevolucao1 = listaProducaoordemconstatacaoAux.get(i).getMotivodevolucao();
				for (int j=0; j<listaProducaoordemconstatacaoAux.size(); j++){
					Motivodevolucao motivodevolucao2 = listaProducaoordemconstatacaoAux.get(j).getMotivodevolucao();
					if (i!=j){
						if (motivodevolucao1!=null
								&& motivodevolucao2!=null
								&& motivodevolucao1.getCdmotivodevolucao().equals(motivodevolucao2.getCdmotivodevolucao())){
							listaErro.add("A constata��o " + motivodevolucaoService.load(motivodevolucao1).getDescricao() + " est� duplicado. Verifique as constata��es e remova o item em duplicidade.");
						}
					}
				}
			}
			
			int cont = 1;
			for (String erro: listaErro){
				errors.reject(String.valueOf(cont++), erro);
			}
		}
		
		producaoordemService.validaObrigatoriedadePneu(bean, null, errors);
		
		super.validateBean(bean, errors);
	}
	
	public ModelAndView abrirPerdadescarte(WebRequestContext request, Producaoordemmaterial bean){
		Producaoordemmaterial producaoordemmaterial = getProducaoordemmaterialBySession(request, bean.getCdproducaoordemmaterial());
		producaoordemmaterial.setQtderegistroperda(bean.getQtderegistroperda());
		request.setAttribute("index", request.getParameter("index"));
		request.setAttribute("identificadorTela", request.getParameter("identificadorTela"));
		request.setAttribute("listaMotivodevolucao", motivodevolucaoService.findForMotivodevolucao());
		return new ModelAndView("direct:/crud/popup/popupRegistrarPerdadescarte", "bean", producaoordemmaterial);
	}
	
	@SuppressWarnings("unchecked")
	public Producaoordemmaterial getProducaoordemmaterialBySession(WebRequestContext request, Integer cdproducaoordemmaterial){
		String identificadorTela = request.getParameter("identificadorTela");
		Object obj = request.getSession().getAttribute("listaProducaoordemmaterialTela"+identificadorTela);
		List<Producaoordemmaterial> lista = (List<Producaoordemmaterial>)obj;
		for(Producaoordemmaterial pom: lista){
			if(pom.getCdproducaoordemmaterial().equals(cdproducaoordemmaterial)){
				return pom;
			}
		}
		return null;
	}
	
	public ModelAndView savePerdadescarte(WebRequestContext request, Producaoordemmaterial bean) throws Exception {
		Producaoordemmaterial producaoordemmaterial = getProducaoordemmaterialBySession(request, bean.getCdproducaoordemmaterial());
		List<Producaoordemmaterialperdadescarte> lista = new ArrayList<Producaoordemmaterialperdadescarte>();
		if(SinedUtil.isListNotEmpty(bean.getListaProducaoordemmaterialperdadescarte())){
			for(Producaoordemmaterialperdadescarte pomp: bean.getListaProducaoordemmaterialperdadescarte()){
				if(!pomp.getIsPorOperador() && pomp.getCdproducaoordemmaterialperdadescarte() == null){
					lista.add(pomp);
				}
			}
		}
		if(producaoordemmaterial.getListaProducaoordemmaterialperdadescarte() == null){
			producaoordemmaterial.setListaProducaoordemmaterialperdadescarte(new ArrayList<Producaoordemmaterialperdadescarte>());
		}
		List<Producaoordemmaterialperdadescarte> listaRemover = new ArrayList<Producaoordemmaterialperdadescarte>();
		for(Producaoordemmaterialperdadescarte pomp: producaoordemmaterial.getListaProducaoordemmaterialperdadescarte()){
			if(!pomp.getIsPorOperador() && pomp.getCdproducaoordemmaterialperdadescarte() == null){
				listaRemover.add(pomp);
			}
		}
		producaoordemmaterial.getListaProducaoordemmaterialperdadescarte().removeAll(listaRemover);
		producaoordemmaterial.getListaProducaoordemmaterialperdadescarte().addAll(lista);
		return new ModelAndView("direct:/crud/popup/listagemPerdadescarte", "producaoordemmaterial", producaoordemmaterial);
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView saveOperadores(WebRequestContext request, Producaoordemmaterial bean) throws Exception {
		Producaoordemmaterial producaoordemmaterial = getProducaoordemmaterialBySession(request, bean.getCdproducaoordemmaterial());
		
		if(producaoordemmaterial.getListaProducaoordemmaterialperdadescarte() == null){
			producaoordemmaterial.setListaProducaoordemmaterialperdadescarte(new ArrayList<Producaoordemmaterialperdadescarte>());
		}
		if(producaoordemmaterial.getListaProducaoordemmaterialoperador() == null){
			producaoordemmaterial.setListaProducaoordemmaterialoperador(SinedUtil.listToSet(new ArrayList<Producaoordemmaterialoperador>(), Producaoordemmaterialoperador.class));
		}
		if(bean.getListaProducaoordemmaterialperdadescarte() == null){
			bean.setListaProducaoordemmaterialperdadescarte(new ArrayList<Producaoordemmaterialperdadescarte>());
		}
		if(bean.getListaProducaoordemmaterialoperador() == null){
			bean.setListaProducaoordemmaterialoperador(SinedUtil.listToSet(new ArrayList<Producaoordemmaterialoperador>(), Producaoordemmaterialoperador.class));
		}
		
		List<Producaoordemmaterialperdadescarte> listaExcluir = new ArrayList<Producaoordemmaterialperdadescarte>();
		List<Producaoordemmaterialoperador> listaExcluirOperador = new ArrayList<Producaoordemmaterialoperador>();
		boolean novoRegistro;
		for(Producaoordemmaterialoperador pomo: producaoordemmaterial.getListaProducaoordemmaterialoperador()){
			novoRegistro = pomo.getCdproducaoordemmaterialoperador() == null;
			if(novoRegistro){
				listaExcluirOperador.add(pomo);
			}
		}
		for(Producaoordemmaterialperdadescarte pomp: producaoordemmaterial.getListaProducaoordemmaterialperdadescarte()){
			novoRegistro = pomp.getCdproducaoordemmaterialperdadescarte() == null;
			if(novoRegistro){
				listaExcluir.add(pomp);
			}
		}
		boolean existeNovo = false;
		producaoordemmaterial.getListaProducaoordemmaterialperdadescarte().removeAll(listaExcluir);
		producaoordemmaterial.getListaProducaoordemmaterialoperador().removeAll(listaExcluirOperador);
		for(Producaoordemmaterialoperador pomo: bean.getListaProducaoordemmaterialoperador()){
			novoRegistro = pomo.getCdproducaoordemmaterialoperador() == null;
			if(novoRegistro){
				existeNovo = true;
				producaoordemmaterial.getListaProducaoordemmaterialoperador().add(pomo);
				if(SinedUtil.isListNotEmpty(pomo.getListaProducaoordemmaterialperdadescarte())){
					for(Producaoordemmaterialperdadescarte pomp: pomo.getListaProducaoordemmaterialperdadescarte()){
						pomp.setProducaoordemmaterial(producaoordemmaterial);
						pomp.setProducaoordemmaterialoperador(pomo);
						producaoordemmaterial.getListaProducaoordemmaterialperdadescarte().add(pomp);
					}
				}
			}
		}

		return new JsonModelAndView().addObject("existeNovo", existeNovo);
	}

	public ModelAndView ajaxLoadlistaoperadores(WebRequestContext request, Producaoordemmaterial bean) throws Exception {
		Producaoordemmaterial producaoordemmaterial = getProducaoordemmaterialBySession(request, bean.getCdproducaoordemmaterial());
		return new ModelAndView("direct:/crud/popup/listagemOperadores", "producaoordemmaterial", producaoordemmaterial);
	}
	
	public ModelAndView ajaxLoadlistaperdadescarte(WebRequestContext request, Producaoordemmaterial bean) throws Exception {
		Producaoordemmaterial producaoordemmaterial = getProducaoordemmaterialBySession(request, bean.getCdproducaoordemmaterial());
		return new ModelAndView("direct:/crud/popup/listagemPerdadescarte", "producaoordemmaterial", producaoordemmaterial);
	}
	
	public ModelAndView ajaxValidaQtdes(WebRequestContext request){
		String whereIn = request.getParameter("whereInProducaoordemmaterial");
		String whereInQtderegistroperda = request.getParameter("whereInQtderegistroperda");
		String msgErro = "";
		String[] arrayQtde = whereInQtderegistroperda.split("_");
		int index = 0;
		for(String cdproducaoordemmaterial: whereIn.split(",")){
			Double qtdeRegistroperda = arrayQtde[index] != ""? Double.parseDouble(arrayQtde[index].replace(",", ".")): 0d;
			Producaoordemmaterial producaoordemmaterial = getProducaoordemmaterialBySession(request, Integer.parseInt(cdproducaoordemmaterial));
			Double qtdePerda = 0d;
			for(Producaoordemmaterialperdadescarte pomp: producaoordemmaterial.getListaProducaoordemmaterialperdadescarte()){
				if(pomp.getCdproducaoordemmaterialperdadescarte() == null && pomp.getQtdeperdadescarte() != null){
					qtdePerda += pomp.getQtdeperdadescarte();
				}
			}
			if(parametrogeralService.getBoolean("BLOQUEAR_CAMPO_PERDA_DESCARTE") && !qtdePerda.equals(qtdeRegistroperda)){
				msgErro += "A soma de Quantidade por Motivo do material "+producaoordemmaterial.getMaterial().getNome()+" deve ser igual � quantidade de 'Registro de perda/descarte'.";
			}
			index++;
		}

		return new JsonModelAndView().addObject("msgErro", msgErro);
	}
	
	public ModelAndView abrePopupMateriaprimaPerda(WebRequestContext request, Producaoordem bean){
		Boolean consumoMateriaPrimaRegistradaComoPerdadescarte = "true".equalsIgnoreCase(request.getParameter("consumoMateriaPrimaRegistradaComoPerdadescarte"));
		for(Producaoordemmaterial producaoordemmaterial: bean.getListaProducaoordemmaterial()){
			if(producaoordemmaterial.getQtderegistroperda() != null && producaoordemmaterial.getQtderegistroperda() > 0){
				producaoordemmaterial.setListaMateriaprimaperda(new ArrayList<ProducaoordemmaterialMaterialprimaPerdaBean>());
				Producaoordemmaterial beanAux = producaoordemmaterialService.loadWithProducaoetapaitem(producaoordemmaterial);
				producaoordemService.carregarProducaoordemmaterialmateriaprima(bean);
				boolean isUltimaEtapa = beanAux.getProducaoetapaitem()!=null && producaoetapaService.isUltimaEtapa(beanAux.getProducaoetapaitem()); 
				
				if(SinedUtil.isListNotEmpty(producaoordemmaterial.getListaProducaoordemmaterialmateriaprima())){
					for (Producaoordemmaterialmateriaprima mp : producaoordemmaterial.getListaProducaoordemmaterialmateriaprima()) {
						Material material = mp.getMaterial();
						boolean baixarEstoqueMateriaprima = (beanAux.getProducaoetapa() == null) ||
										(isUltimaEtapa && BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA.equals(beanAux.getProducaoetapa().getBaixarEstoqueMateriaprima())) ||
										(BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_CADA_ETAPA.equals(beanAux.getProducaoetapa().getBaixarEstoqueMateriaprima()) &&
										(mp.getProducaoetapanome() == null || mp.getProducaoetapanome().equals(beanAux.getProducaoetapaitem().getProducaoetapanome())));

						if(baixarEstoqueMateriaprima){//Baixa estoque de mat�ria-prima de acordo com o ciclo de produ��o.
							ProducaoordemmaterialMaterialprimaPerdaBean mat = new ProducaoordemmaterialMaterialprimaPerdaBean();
							mat.setSelected(true);
							mat.setMateriaprima(material);
							mat.setConsumo((mp.getQtdeprevista() / producaoordemmaterial.getQtde() * producaoordemmaterial.getQtderegistroperda()));
							mat.setCdproducaoordem(producaoordemmaterial.getProducaoordem().getCdproducaoordem());
							producaoordemmaterial.getListaMateriaprimaperda().add(mat);
						}
					}
				}else {
					List<Materialproducao> listaProducao = materialproducaoService.findListaProducao(producaoordemmaterial.getMaterial().getCdmaterial(), !consumoMateriaPrimaRegistradaComoPerdadescarte);
					producaoordemmaterial.setMaterial(materialService.load(producaoordemmaterial.getMaterial(), "material.cdmaterial, material.nome"));
					if(SinedUtil.isListNotEmpty(listaProducao)){
						for(Materialproducao materiaprima: listaProducao){
							boolean baixarEstoqueMateriaprima = (beanAux.getProducaoetapa() == null) ||
																(isUltimaEtapa && BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA.equals(beanAux.getProducaoetapa().getBaixarEstoqueMateriaprima())) ||
																(BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_CADA_ETAPA.equals(beanAux.getProducaoetapa().getBaixarEstoqueMateriaprima()) &&
																(materiaprima.getProducaoetapanome() == null || materiaprima.getProducaoetapanome().equals(beanAux.getProducaoetapaitem().getProducaoetapanome())));
							
							if(baixarEstoqueMateriaprima){//Baixa estoque de mat�ria-prima de acordo com o ciclo de produ��o.
								ProducaoordemmaterialMaterialprimaPerdaBean mat = new ProducaoordemmaterialMaterialprimaPerdaBean();
								mat.setSelected(true);
								mat.setMateriaprima(materiaprima.getMaterial());
								mat.setConsumo(materiaprima.getConsumo() * producaoordemmaterial.getQtderegistroperda());
								mat.setCdproducaoordem(producaoordemmaterial.getProducaoordem().getCdproducaoordem());
								producaoordemmaterial.getListaMateriaprimaperda().add(mat);
							}
						}
					}
				}
			}
		}
		request.setAttribute("index", request.getParameter("index"));
		request.setAttribute("identificadorTela", request.getParameter("identificadorTela"));
		request.setAttribute("whereInProducaoordem", request.getParameter("whereInProducaoordem"));
		return new ModelAndView("direct:/process/popup/popupRegistrarperdaSelecaoMateriaprima", "bean", bean);
	}
	
	public void savePopupmateriaprimaPerda(WebRequestContext request, Producaoordem bean){
		for(Producaoordemmaterial producaoordemmaterial: bean.getListaProducaoordemmaterial()){
			if(SinedUtil.isListNotEmpty(producaoordemmaterial.getListaMateriaprimaperda())){
				Producaoordemmaterial producaoordemmaterialDaSessao = getProducaoordemmaterialBySession(request, producaoordemmaterial.getCdproducaoordemmaterial());
				producaoordemmaterialDaSessao.setListaMateriaprimaperda(producaoordemmaterial.getListaMateriaprimaperda());
			}
		}	
	}
}