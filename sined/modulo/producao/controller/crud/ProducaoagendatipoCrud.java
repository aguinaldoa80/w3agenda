package br.com.linkcom.sined.modulo.producao.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Producaoagendatipo;
import br.com.linkcom.sined.modulo.producao.controller.crud.filter.ProducaoagendatipoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@Controller(path="/producao/crud/Producaoagendatipo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class ProducaoagendatipoCrud extends CrudControllerSined<ProducaoagendatipoFiltro, Producaoagendatipo, Producaoagendatipo>{
	
	@Override
	protected void salvar(WebRequestContext request, Producaoagendatipo bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_PRODUCAOAGENDATIPO_NOME")) {
				throw new SinedException("Tipo de Produ��o j� cadastrado no sistema.");
			} else
				e.printStackTrace();
			
		}
	}
}