package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumTipoEmissao;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.geral.service.ExpedicaoService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirExpedicaoEtiquetaBean;

@Controller(path = "/faturamento/relatorio/EmitirEtiquetaExpedicaoReport", authorizationModule=ReportAuthorizationModule.class)
public class EmitirEtiquetaExpedicaoReport extends ReportTemplateController<ReportTemplateFiltro>{
	
	private ExpedicaoService expedicaoService;
	
	public void setExpedicaoService(ExpedicaoService expedicaoService) {
		this.expedicaoService = expedicaoService;
	}

	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.ETIQUETA_EPEDICAO;
	}
	
	@Override
	public ModelAndView doGerar(WebRequestContext request, ReportTemplateFiltro filtro) throws Exception {
		request.setAttribute("titulo", "Etiqueta de Volumes da Expedi��o");
		try{
			ReportTemplateBean bean = this.setTemplate(request, filtro);
			if(EnumTipoEmissao.W3ERP.equals(bean.getTipoEmissao()) || EnumTipoEmissao.TXT.equals(bean.getTipoEmissao())){
				try{
					String type = EnumTipoEmissao.W3ERP.equals(bean.getTipoEmissao()) ? "remote_printing" : "txt";
					String extensao = EnumTipoEmissao.W3ERP.equals(bean.getTipoEmissao()) ? "w3erp" : "txt";
					
					Resource recurso = getResource(request, filtro, "application/" + type, extensao);
					if (recurso == null) {
						return goToAction(FILTRO);
					}
					
					HttpServletResponse response = request.getServletResponse();
					response.setContentType(recurso.getContentType());
					response.addHeader("Content-Disposition", "attachment; filename=\"" + recurso.getFileName() +"\";");
					response.getOutputStream().write(recurso.getContents());
					
				} catch (Exception e) {
					throw new Exception(e.getMessage(),e.getCause());
				}			
				return null;
			}else {
				return super.doGerar(request, filtro);
			}
		
		}catch (NullPointerException e) {
			request.addError("Template n�o encontrado");
			return doFiltro(request, filtro);
		}catch (Exception e) {
			request.addError(e.getMessage()+" " +e.getCause());
			return doFiltro(request, filtro);
		}
	}
	
	@Override
	protected String getFileName() {
		return "etiqueta_expedicao";
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request, ReportTemplateFiltro filtro) {	
		ReportTemplateBean bean = getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.ETIQUETA_EPEDICAO);
		if(filtro.getTemplate() != null){
			if(filtro.getTemplate().getCdreporttemplate() != null){
				List<ReportTemplateBean> beans =  getReportTemplateService().loadListaTemplateByCategoria(EnumCategoriaReportTemplate.ETIQUETA_EPEDICAO);
				for(ReportTemplateBean n: beans){
					if(n.getCdreporttemplate().equals(filtro.getTemplate().getCdreporttemplate())){
						bean = n;
					}
				}
			}
		}	
		if(request.getParameter("templateSelecionado") != null && !request.getParameter("templateSelecionado").equals("")){
			bean  = getReportTemplateService().loadTemplateByCategoriaAndNome(EnumCategoriaReportTemplate.ETIQUETA_EPEDICAO, request.getParameter("templateSelecionado"));
		}
		return bean;
	}	
	
	
	@Override
	protected LinkedHashMap<String, Object> carregaDados(WebRequestContext request, ReportTemplateFiltro filtro) throws Exception {
		LinkedHashMap<String,Object> datasource = new LinkedHashMap<String, Object>();		
		String whereIn = request.getParameter("selectedItens");
		List<Expedicao> lista = expedicaoService.findForEtiquetaExpedicao(whereIn);		
		LinkedList<EmitirExpedicaoEtiquetaBean> listaBean = new LinkedList<EmitirExpedicaoEtiquetaBean>();
		EmitirExpedicaoEtiquetaBean bean;		
		for(Expedicao expedicao: lista){
			List<Expedicaoitem> listaExpedicaoitem = expedicao.getListaExpedicaoitem();
			if(listaExpedicaoitem != null && listaExpedicaoitem.size() > 0){
				for (Expedicaoitem expedicaoitem : listaExpedicaoitem) {
					bean = new EmitirExpedicaoEtiquetaBean();
					if(expedicao.getEmpresa() != null) bean.setNome_empresa(expedicao.getEmpresa().getRazaosocialOuNome());
					if(expedicao.getEmpresa() != null){
						if(expedicao.getEmpresa().getEndereco() != null){
							Endereco enderecoEmpresa = expedicao.getEmpresa().getEndereco();
							if(enderecoEmpresa.getNumero() != null) bean.setEnd_numero_empresa(enderecoEmpresa.getNumero());
							if(enderecoEmpresa.getComplemento() != null) bean.setEnd_complemento_empresa(enderecoEmpresa.getComplemento());
							if(enderecoEmpresa.getLogradouro() != null) bean.setEnd_logradouro_empresa(enderecoEmpresa.getLogradouro());
							if(enderecoEmpresa.getBairro() != null) bean.setEnd_bairro_empresa(enderecoEmpresa.getBairro());
							if(enderecoEmpresa.getCep() != null) bean.setEnd_cep_empresa(enderecoEmpresa.getCep().toString());
							if(enderecoEmpresa.getMunicipio() != null){
								bean.setEnd_cidade_empresa(enderecoEmpresa.getMunicipio().getNome());
								if(enderecoEmpresa.getMunicipio().getUf() != null) 
									bean.setEnd_estado_empresa(enderecoEmpresa.getMunicipio().getUf().getSigla());							
							}
						}							
					}
					if(expedicaoitem.getCliente() != null){
						bean.setNome_cliente(expedicaoitem.getCliente().getRazaoOrNomeByTipopessoa());
						if(expedicaoitem.getCliente().getEndereco() != null){
							Endereco enderecoCliente = expedicaoitem.getCliente().getEndereco();
							if(enderecoCliente.getNumero() != null) bean.setEnd_numero_cliente(enderecoCliente.getNumero());
							if(enderecoCliente.getComplemento() != null) bean.setEnd_complemento_cliente(enderecoCliente.getComplemento());
							if(enderecoCliente.getLogradouro() != null) bean.setEnd_logradouro_cliente(enderecoCliente.getLogradouro());
							if(enderecoCliente.getBairro() != null) bean.setEnd_bairro_cliente(enderecoCliente.getBairro());
							if(enderecoCliente.getCep() != null) bean.setEnd_cep_cliente(enderecoCliente.getCep().toString());
							if(enderecoCliente.getMunicipio() != null){
								bean.setEnd_cidade_cliente(enderecoCliente.getMunicipio().getNome());
								if(enderecoCliente.getMunicipio().getUf() != null) 
									bean.setEnd_estado_cliente(enderecoCliente.getMunicipio().getUf().getSigla());							
							}
						}	
					}
					if(expedicaoitem.getVendamaterial() != null){
						if(expedicaoitem.getVendamaterial().getVenda() != null) {
							bean.setNumero_pedido(expedicaoitem.getVendamaterial() .getVenda().getIdentificadorOrCdvenda());														
						}
					}
					bean.setQuantidade_volumes(expedicao.getQuantidadevolumes());
					listaBean.add(bean);
				}						
			}
		}
		datasource.put("lista", listaBean);
		return datasource;
	}
}
