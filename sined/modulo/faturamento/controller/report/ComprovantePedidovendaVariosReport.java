package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.service.ComprovanteConfiguravelService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendahistoricoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.PedidovendaFiltro;
import br.com.linkcom.sined.util.SinedException;

@Controller(
			path = "/faturamento/relatorio/Pedidovendas", 
			authorizationModule=ReportAuthorizationModule.class
)
public class ComprovantePedidovendaVariosReport extends ResourceSenderController<PedidovendaFiltro>{

	private PedidovendaService pedidovendaService;
	private EmpresaService empresaService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private ComprovanteConfiguravelService comprovanteConfiguravelService;
	private ParametrogeralService parametrogeralService;
	
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}	
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}	
	public void setPedidovendahistoricoService(PedidovendahistoricoService pedidovendahistoricoService) {this.pedidovendahistoricoService = pedidovendahistoricoService;}
	public void setComprovanteConfiguravelService(ComprovanteConfiguravelService comprovanteConfiguravelService) {this.comprovanteConfiguravelService = comprovanteConfiguravelService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	
	@Override
	public ModelAndView doGerar(WebRequestContext request, PedidovendaFiltro filtro) throws Exception {
		ComprovanteConfiguravel comprovante;
		String whereIn = filtro.getWhereIn();
		
		Pedidovendasituacao [] situacoes = { Pedidovendasituacao.AGUARDANDO_APROVACAO };
		
		if(!parametrogeralService.getBoolean("EMITIR_COMPROVANTE_AGUARDANDOAPROVACAO") && pedidovendaService.havePedidovendaSituacao(whereIn, situacoes)) {
			request.addError("O envio e emiss�o do comprovante n�o s�o permitidos na situa��o de Aguardando Aprova��o.");
			if(pedidovendaService.isOtr(Integer.parseInt(whereIn.split(",")[0]))){
				return new ModelAndView("redirect:/faturamento/crud/PedidovendaOtr");
			}
			return new ModelAndView("redirect:/faturamento/crud/Pedidovenda");
		}
		
		boolean variosRelatorios = StringUtils.isNotEmpty(whereIn);
		String cdpedidovendaStr = whereIn.contains(",") ? whereIn.substring(0, whereIn.indexOf(",")) : whereIn;
		Integer cdpedidovenda = variosRelatorios ? Integer.parseInt( cdpedidovendaStr) : filtro.getCdpedidovenda();
		Pedidovenda pedidovenda = pedidovendaService.load(new Pedidovenda(cdpedidovenda));
		
		if(StringUtils.isNotBlank(request.getParameter("cdcomprovanteconfiguravel"))){
			comprovante = comprovanteConfiguravelService.load(new ComprovanteConfiguravel(Integer.parseInt(request.getParameter("cdcomprovanteconfiguravel"))));
		}else {
			comprovante = empresaService.findComprovaConfiguravelByTipoAndEmpresa(
					ComprovanteConfiguravel.TipoComprovante.PEDIDO_VENDA, pedidovenda.getEmpresa() );
		}
			
		ModelAndView retorno;
		if( comprovante != null && comprovante.getLayout()!=null){
			if(variosRelatorios) 
				retorno = pedidovendaService.gerarComprovantesConfiguraveis(request, whereIn, comprovante);
			else 
				retorno = pedidovendaService.gerarComprovanteConfiguravel(request, cdpedidovenda, comprovante);
		}else{
			retorno = super.doGerar(request, filtro);
		}
		List<Pedidovenda> listaPedidoVenda = new ArrayList<Pedidovenda>();
		if(variosRelatorios){
			listaPedidoVenda.addAll(pedidovendaService.findForComprovante(whereIn));
		}else{
			listaPedidoVenda.add(pedidovenda);
		}

		pedidovendahistoricoService.makeHistoricoComprovanteEmitido(listaPedidoVenda);
		return retorno;
	}


	@Override
	public Resource generateResource(WebRequestContext request, PedidovendaFiltro filtro) throws Exception {
		return pedidovendaService.gerarVariosComprovantes(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, PedidovendaFiltro filtro) throws Exception {
		return new ModelAndView("redirect:/faturamento/crud/Venda");
	}
	
}
