package br.com.linkcom.sined.modulo.faturamento.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.service.VendaorcamentoService;
import br.com.linkcom.sined.geral.service.VendaorcamentohistoricoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.VendaorcamentoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/faturamento/relatorio/VendaOrcamento", 
			authorizationModule=ReportAuthorizationModule.class
)
public class ComprovantevendaorcamentoReport extends SinedReport<VendaorcamentoFiltro>{

	private VendaorcamentoService vendaorcamentoService;
	private VendaorcamentohistoricoService vendaorcamentohistoricoService;
	
	public void setVendaorcamentoService(
			VendaorcamentoService vendaorcamentoService) {
		this.vendaorcamentoService = vendaorcamentoService;
	}
	
	public void setVendaorcamentohistoricoService(
			VendaorcamentohistoricoService vendaorcamentohistoricoService) {
		this.vendaorcamentohistoricoService = vendaorcamentohistoricoService;
	}
	
	@Override
	public ModelAndView doGerar(WebRequestContext request, VendaorcamentoFiltro filtro) throws Exception {
		Vendaorcamento vendaorcamento = vendaorcamentoService.load(new Vendaorcamento(filtro.getCdvendaorcamento()));
		ComprovanteConfiguravel comprovante = empresaService.findComprovaConfiguravelByTipoAndEmpresa(
												ComprovanteConfiguravel.TipoComprovante.ORCAMENTO, 
												vendaorcamento.getEmpresa());
		ModelAndView retorno;
		if( comprovante != null && comprovante.getLayout()!=null){
			retorno = vendaorcamentoService.gerarComprovanteConfiguravel(request, filtro.getCdvendaorcamento(), comprovante);
		}else{
			retorno = super.doGerar(request, filtro);
		}
		
		vendaorcamentohistoricoService.makeHistoricoComprovanteEmitido(vendaorcamento);
		return retorno;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	VendaorcamentoFiltro filtro) throws Exception {
		return vendaorcamentoService.gerarComprovante(filtro);
	}
	
	@Override
	public String getNomeArquivo() {
		return "comprovantevendaorcamento";
	}

	@Override
	public String getTitulo(VendaorcamentoFiltro filtro) {
		return "OR�AMENTO";
	}
	
}
