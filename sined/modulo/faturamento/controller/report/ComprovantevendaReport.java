package br.com.linkcom.sined.modulo.faturamento.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.geral.service.VendahistoricoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.VendaFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/faturamento/relatorio/Venda", 
			authorizationModule=ReportAuthorizationModule.class
)
public class ComprovantevendaReport extends SinedReport<VendaFiltro>{

	private VendaService vendaService;
	private VendahistoricoService vendahistoricoService;
	
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	
	public void setVendahistoricoService(VendahistoricoService vendahistoricoService) {
		this.vendahistoricoService = vendahistoricoService;
	}
	
	@Override
	public ModelAndView doGerar(WebRequestContext request, VendaFiltro filtro) throws Exception {
		String whereIn = filtro.getWhereIn();
		
		if(whereIn == null) {
			whereIn = filtro.getCdvenda().toString();
		}
		
		Vendasituacao [] situacoes = { Vendasituacao.AGUARDANDO_APROVACAO };
		
		if(!parametrogeralService.getBoolean("EMITIR_COMPROVANTE_AGUARDANDOAPROVACAO") && vendaService.haveVendaSituacao(whereIn, false, situacoes)) {
			request.addError("O envio e emiss�o do comprovante n�o s�o permitidos na situa��o de Aguardando Aprova��o.");
			String urlRedirecionamento = Boolean.TRUE.equals(filtro.getOrigemOtr())? "redirect:/faturamento/crud/VendaOtr": "redirect:/faturamento/crud/Venda";
			return new ModelAndView(urlRedirecionamento);
		}
		
		Venda venda = vendaService.loadWithPedidovendaAndEmpresa(new Venda(Integer.parseInt(whereIn.split(",")[0])));
		ComprovanteConfiguravel comprovante = empresaService.findComprovaConfiguravelByTipoAndEmpresa(ComprovanteConfiguravel.TipoComprovante.VENDA, venda.getEmpresa());
		
		ModelAndView retorno;
		
		if( comprovante != null && comprovante.getLayout()!=null){
			retorno = vendaService.gerarComprovanteConfiguravel(request, filtro.getCdvenda(), comprovante);
		}else{
			retorno = super.doGerar(request, filtro);
		}
		
		vendahistoricoService.makeHistoricoComprovanteEmitido(venda);
		return retorno;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	VendaFiltro filtro) throws Exception {
		return vendaService.gerarComprovante(filtro);
	}
	
	@Override
	public String getNomeArquivo() {
		return "comprovantevenda";
	}

	@Override
	public String getTitulo(VendaFiltro filtro) {
		return "VENDA";
	}
	
}
