package br.com.linkcom.sined.modulo.faturamento.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ExpedicaoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ExpedicaoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/faturamento/relatorio/Expedicao",	authorizationModule=ReportAuthorizationModule.class)
public class ExpedicaoReport extends SinedReport<ExpedicaoFiltro> {
	
	private ExpedicaoService expedicaoService;
	
	public void setExpedicaoService(ExpedicaoService expedicaoService) {
		this.expedicaoService = expedicaoService;
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, ExpedicaoFiltro filtro) throws ResourceGenerationException {
		return new ModelAndView("redirect:/faturamento/crud/Expedicao");
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, ExpedicaoFiltro filtro) throws Exception {
		return expedicaoService.gerarRelatorioListagem(filtro);
	}
	
	@Override
	public String getTitulo(ExpedicaoFiltro filtro) {
		return "EXPEDIÇÕES";
	}
	
	@Override
	public String getNomeArquivo() {
		return "expedicao";
	}
}
