package br.com.linkcom.sined.modulo.faturamento.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(path = "/faturamento/relatorio/OrdemRetirada", authorizationModule=ReportAuthorizationModule.class)
public class OrdemRetiradaReport extends ResourceSenderController<FiltroListagemSined>{

	private VendaService vendaService;
	
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		return vendaService.gerarOrdemRetirada(SinedUtil.getItensSelecionados(request));
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		return new ModelAndView("redirect:/faturamento/crud/Venda");
	}
	
}
