package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.sql.Date;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendamaterialService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.PedidovendaFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.report.ArquivoW3erpUtil;

@Controller(path="/faturamento/relatorio/PedidovendaMatricial", authorizationModule=ReportAuthorizationModule.class)
public class PedidovendaMatricialReport extends ResourceSenderController<PedidovendaFiltro>{

	private int LARGURA_MAXIMA = 80;
	private int TOTAL_LINHAS = 31;

	private PedidovendaService pedidovendaService;
	private PedidovendamaterialService pedidovendamaterialService;
	private ClienteService clienteService;
	private EmpresaService empresaService;
	private ParametrogeralService parametrogeralService;
	private VendaService vendaService;
	
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	
	public void setPedidovendamaterialService(PedidovendamaterialService pedidovendamaterialService) {
		this.pedidovendamaterialService = pedidovendamaterialService;
	}
	
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request, PedidovendaFiltro filtro) throws Exception {
		
		if (filtro.getWhereIn() == null || filtro.getWhereIn().trim().isEmpty()){
			return null;
		}
		
		StringBuilder report = new StringBuilder();

		String[] listaCdpedidovenda = filtro.getWhereIn().split(",");
		
		for (String cdpedidovenda : listaCdpedidovenda){
			Pedidovenda pedidoVenda = pedidovendaService.loadForComprovante(Integer.valueOf(cdpedidovenda));
			montaComprovanteMatricial(report, pedidoVenda);
		}		
		
		Resource resource = new Resource();
		resource.setContentType("application/w3erp");
		resource.setFileName("pedido_venda_" + SinedUtil.datePatternForReport() + ".w3erp");
	    resource.setContents(report.toString().getBytes());
	    
		return resource;
	}
	
	private void montaComprovanteMatricial(StringBuilder output, Pedidovenda pedidoVenda) {

		List<String> baseReport = new ArrayList<String>();
		
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		NumberFormat decimalFormat = new DecimalFormat("#,##0.###");
		NumberFormat moneyFormat = new DecimalFormat("#,##0.00");

		int posicaoListaItem = montaBaseRelatorio(pedidoVenda, baseReport, df,
				decimalFormat, moneyFormat);
		
		List<String> report = new ArrayList<String>();
		report.addAll(baseReport);
		int posicaoItem = posicaoListaItem;
		
		for (Pedidovendamaterial item : pedidoVenda.getListaPedidovendamaterial()){
			
			item.setTotal(vendaService.getValortotal(item.getPreco(), item.getQuantidade(), item.getDesconto(), item.getMultiplicador(), item.getOutrasdespesas(), item.getValorSeguro()));
			
			StringBuilder linha = new StringBuilder();
			linha.append(StringUtils.rightPad(decimalFormat.format(item.getQuantidade()), 10));
			linha.append(StringUtils.rightPad(StringUtils.substring(item.getUnidademedida().getNome(), 0, 9), 10));
			linha.append(StringUtils.rightPad(StringUtils.substring(item.getMaterial().getNome(), 0, 29), 30));
			linha.append(StringUtils.leftPad(moneyFormat.format(item.getPreco()), 10));
			linha.append(StringUtils.leftPad(moneyFormat.format(item.getTotal().getValue()), 10));
			
			if(item.getPesoVendaOuMaterial() != null){
				double pesoLiquido = item.getQuantidade() * item.getPesoVendaOuMaterial();
				linha.append(StringUtils.leftPad(decimalFormat.format(pesoLiquido), 10));
			}
			
			report.add(posicaoItem++, linha.toString());
			
			if (report.size() >= TOTAL_LINHAS){
				gravaLinhas(output, report, posicaoItem);
				
				report = new ArrayList<String>();
				report.addAll(baseReport);
				posicaoItem = posicaoListaItem;
			}
		}
		
		gravaLinhas(output, report, posicaoItem);
	}

	private void gravaLinhas(StringBuilder output, List<String> report,
			int posicaoItem) {
		
		while (report.size() < TOTAL_LINHAS)
			report.add(posicaoItem, "");
		
		for (int i = 0; i < report.size(); i++){
			//output.append(StringUtils.leftPad(String.valueOf(i+1), 2, "0") + " " + report.get(i));
			output.append(report.get(i));
			output.append(ArquivoW3erpUtil.QUEBRA_LINHA);
		}
		
		output
			.append(ArquivoW3erpUtil.QUEBRA_LINHA)
			.append(ArquivoW3erpUtil.QUEBRA_LINHA);
	}

	private int montaBaseRelatorio(Pedidovenda pedidoVenda,
			List<String> baseReport, DateFormat df, NumberFormat decimalFormat,
			NumberFormat moneyFormat) {
		
		Date dtentrega = pedidoVenda.getMaiorDataentregaTrans();
		if(dtentrega != null){
			baseReport.add("Previs�o de Entrega " + df.format(dtentrega) + StringUtils.leftPad("Pedido Venda " + pedidoVenda.getCdpedidovenda(), LARGURA_MAXIMA - 21));
		}else {
			baseReport.add("Emitido Em " + df.format(pedidoVenda.getDtpedidovenda()) + StringUtils.leftPad("Pedido Venda " + pedidoVenda.getCdpedidovenda(), LARGURA_MAXIMA - 21));
		}
		
		Empresa empresa = empresaService.loadWithEnderecoTelefone(pedidoVenda.getEmpresa());
		
		baseReport.add(StringUtils.substring(empresa.getRazaosocialOuNome(), 0, LARGURA_MAXIMA));
		baseReport.add(StringUtils.substring(empresa.getEndereco().getLogradouroCompletoComBairro(), 0, LARGURA_MAXIMA));
		baseReport.add(empresa.getTelefonesSemQuebraLinha());
		baseReport.add("");

		Cliente cliente = clienteService.carregarDadosCliente(pedidoVenda.getCliente());
		
		baseReport.add(StringUtils.substring("Cliente: " + cliente.getNome(), 0, LARGURA_MAXIMA));
		if (cliente.getEndereco() != null)
			baseReport.add(StringUtils.substring(cliente.getEndereco().getLogradouroCompletoComBairro(), 0, LARGURA_MAXIMA));
		else
			baseReport.add("");
		baseReport.add("Fone " + cliente.getTelefonesSemQuebraLinhaPipe());
		
		StringBuilder sb = new StringBuilder();
		sb.append("I.E. ");
		if (cliente.getInscricaoestadual() != null)
			sb.append(cliente.getInscricaoestadual());
		if (cliente.getCnpj() != null)
			sb.append(" ").append(cliente.getCpfOuCnpj());
		baseReport.add(sb.toString());
		
		baseReport.add("");
		baseReport.add("Quantd.   UN        Descri��o dos Produtos        Valor Unt.     Total      Peso");

		double pesoLiquidoTotal = 0;
		Money valorTotal = new Money(0);

		List<Pedidovendamaterial> listaProdutos = pedidovendamaterialService.findByPedidoVenda(pedidoVenda, "pedidovendamaterial.cdpedidovendamaterial, listaCaracteristica.cdmaterialcaracteristica");
		pedidoVenda.setListaPedidovendamaterial(listaProdutos);
		
		for (Pedidovendamaterial item : listaProdutos){
			
			item.setTotal(vendaService.getValortotal(item.getPreco(), item.getQuantidade(), item.getDesconto(), item.getMultiplicador(), item.getOutrasdespesas(), item.getValorSeguro()));
			
			if(item.getPesoVendaOuMaterial() != null){
				double pesoLiquido = item.getQuantidade() * item.getPesoVendaOuMaterial();
				pesoLiquidoTotal += pesoLiquido;
			}
			
			valorTotal = valorTotal.add(item.getTotal());
		}
		
		//Armazenando a posi��o onde os itens ser�o inseridos
		int posicaoListaItem = baseReport.size();
		
		baseReport.add("");
		baseReport.add("Vencimento:  "); 
		baseReport.add("Peso Total:  " + StringUtils.leftPad(decimalFormat.format(pesoLiquidoTotal), 10));
		baseReport.add("Mercadorias: " + StringUtils.leftPad(moneyFormat.format(valorTotal.getValue()), 10));
		if (pedidoVenda.getDesconto() != null)
			baseReport.add("Desconto:    " + StringUtils.leftPad(moneyFormat.format(pedidoVenda.getDesconto().getValue()), 10));
		else
			baseReport.add("Desconto:    " + StringUtils.leftPad(moneyFormat.format(0.0), 10));
		baseReport.add("Valor Total: " + StringUtils.leftPad(moneyFormat.format(pedidoVenda.getTotalvenda().getValue()), 10));
		baseReport.add("");
		
		baseReport.add("Observa��es:            ASS.:________________________________________");
		
		String texto = pedidoVenda.getObservacao();
		
		List<String> observacoes = quebrarLinhas(LARGURA_MAXIMA, texto);
		
		if(observacoes != null){
			for (int i = 0; i < observacoes.size(); i++){
				if (baseReport.size() < TOTAL_LINHAS)
					baseReport.add(observacoes.get(i));
			}
		}
		return posicaoListaItem;
	}

	private List<String> quebrarLinhas(int colunas, String texto) {
		List<String> linhas = new ArrayList<String>();
		while (texto != null && texto.length() > 0){
			int posicao = colunas;
			if (texto.length() > colunas){
				char charAt = texto.charAt(posicao);
				while (charAt != '\n' && charAt != '\r' && charAt != '.' && charAt != ',' && charAt != ';' && charAt != '\t' && charAt != ' '){
					posicao--;
					charAt = texto.charAt(posicao);
				}
				linhas.add(texto.substring(0, posicao));
				if (charAt == ' ')
					posicao++;
				texto = texto.substring(posicao);
			} else {
				linhas.add(texto);
				texto = "";
			}
		}
		return linhas;
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, PedidovendaFiltro filtro) throws Exception {
		String urlRedirecionamento = "redirect:/faturamento/crud/Pedidovenda";
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || "".equals(whereIn)){
			if(pedidovendaService.isOtr(Integer.parseInt(whereIn.split(",")[0]))){
				urlRedirecionamento = "redirect:/faturamento/crud/PedidovendaOtr";
			}
		}
		return new ModelAndView(urlRedirecionamento);
	}
	
	@Override
	public ModelAndView doGerar(WebRequestContext request, PedidovendaFiltro filtro) throws Exception {
		String whereIn = filtro.getWhereIn();
		Pedidovendasituacao [] situacoes = { Pedidovendasituacao.AGUARDANDO_APROVACAO };

		if(!parametrogeralService.getBoolean("EMITIR_COMPROVANTE_AGUARDANDOAPROVACAO") && pedidovendaService.havePedidovendaSituacao(whereIn, situacoes)) {
			request.addError("O envio e emiss�o do comprovante n�o s�o permitidos na situa��o de Aguardando Aprova��o.");
			if(pedidovendaService.isOtr(Integer.parseInt(whereIn.split(",")[0]))){
				return new ModelAndView("redirect:/faturamento/crud/PedidovendaOtr");
			}
			return new ModelAndView("redirect:/faturamento/crud/Pedidovenda");
		}
		
		return super.doGerar(request, filtro);
	}
}
