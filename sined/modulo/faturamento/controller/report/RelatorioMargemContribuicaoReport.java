package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.service.MargemContribuicaoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.RelatorioMargemContribuicaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.RelatorioMargemContribuicaoNotaVendaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.RelatorioMargemContribuicaoNotaVendaItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.enumeration.FormaAgrupamentoEnum;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.RelatorioMargemContribuicaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/faturamento/relatorio/RelatorioMargemContribuicao", authorizationModule=ReportAuthorizationModule.class)
public class RelatorioMargemContribuicaoReport extends SinedReport<RelatorioMargemContribuicaoFiltro> {
	
	private VendaService vendaService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private MargemContribuicaoService margemContribuicaoService;
	
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	
	public void setMargemContribuicaoService(MargemContribuicaoService margemContribuicaoService) {
		this.margemContribuicaoService = margemContribuicaoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	RelatorioMargemContribuicaoFiltro filtro) throws Exception {
		filtro.setFormaAgrupamentoEnum(filtro.getFormaAgrupamentoEnum() != null ? filtro.getFormaAgrupamentoEnum() : FormaAgrupamentoEnum.PRODUTO);
		
		List<Venda> listaVenda = null;
		List<Notafiscalproduto> listaNotaFiscalProduto = null;
		
		if ("Venda".equals(filtro.getOrigemString())) {
			listaVenda = vendaService.carregarDadosMargemContribuicao(filtro);
		} else if ("Nota".equals(filtro.getOrigemString())) {
			listaNotaFiscalProduto = notafiscalprodutoService.carregarDadosMargemContribuicao(filtro);
		} else {
			listaVenda = vendaService.carregarDadosMargemContribuicao(filtro);
			listaNotaFiscalProduto = notafiscalprodutoService.carregarDadosMargemContribuicao(filtro);
		}
		
		filtro.setFormaAgrupamentoEnum(filtro.getFormaAgrupamentoEnum() != null ? filtro.getFormaAgrupamentoEnum() : FormaAgrupamentoEnum.PRODUTO);
		
		List<RelatorioMargemContribuicaoBean> listaRelatorioMargemContribuicaoBean = margemContribuicaoService.createListaRelatorioMargemContribuicaoBean(filtro, listaVenda, listaNotaFiscalProduto);
		if(SinedUtil.isListEmpty(listaRelatorioMargemContribuicaoBean) ) {
			throw new SinedException("Nenhum resultado encontrado.");
		}
		
		IReport report = margemContribuicaoService.gerarRelatorioPdf(filtro, listaRelatorioMargemContribuicaoBean);
		desbloquearTela(request);
		return report;
	}
	
	public ModelAndView gerarCsv(WebRequestContext request, RelatorioMargemContribuicaoFiltro filtro) throws Exception {
		filtro.setFormaAgrupamentoEnum(filtro.getFormaAgrupamentoEnum() != null ? filtro.getFormaAgrupamentoEnum() : FormaAgrupamentoEnum.PRODUTO);
		
		List<Venda> listaVenda = null;
		List<Notafiscalproduto> listaNotaFiscalProduto = null;
		
		if ("Venda".equals(filtro.getOrigemString())) {
			listaVenda = vendaService.carregarDadosMargemContribuicao(filtro);
		} else if ("Nota".equals(filtro.getOrigemString())) {
			listaNotaFiscalProduto = notafiscalprodutoService.carregarDadosMargemContribuicao(filtro);
		} else {
			listaVenda = vendaService.carregarDadosMargemContribuicao(filtro);
			listaNotaFiscalProduto = notafiscalprodutoService.carregarDadosMargemContribuicao(filtro);
		}
		
		List<RelatorioMargemContribuicaoBean> listaRelatorioMargemContribuicaoBean = margemContribuicaoService.createListaRelatorioMargemContribuicaoBean(filtro, listaVenda, listaNotaFiscalProduto);
		if(SinedUtil.isListEmpty(listaRelatorioMargemContribuicaoBean)) {
			request.addError("Nenhum resultado encontrado.");
			return new ModelAndView("redirect:/faturamento/relatorio/RelatorioMargemContribuicao");
		}
		
		Resource resource = margemContribuicaoService.gerarRelatorioCsv(listaRelatorioMargemContribuicaoBean, filtro);
		desbloquearTela(request);
		
		return new ResourceModelAndView(resource);
	}
	
	public ModelAndView mostrarTela(WebRequestContext request, RelatorioMargemContribuicaoFiltro filtro) throws Exception {
		filtro.setFormaAgrupamentoEnum(filtro.getFormaAgrupamentoEnum() != null ? filtro.getFormaAgrupamentoEnum() : FormaAgrupamentoEnum.GRUPO_MATERIAL);
		filtro.setMostrarTela(Boolean.TRUE);
		
		List<Venda> listaVenda = null;
		List<Notafiscalproduto> listaNotaFiscalProduto = null;
		
		if ("Venda".equals(filtro.getOrigemString())) {
			listaVenda = vendaService.carregarDadosMargemContribuicao(filtro);
		} else if ("Nota".equals(filtro.getOrigemString())) {
			listaNotaFiscalProduto = notafiscalprodutoService.carregarDadosMargemContribuicao(filtro);
		} else {
			listaVenda = vendaService.carregarDadosMargemContribuicao(filtro);
			listaNotaFiscalProduto = notafiscalprodutoService.carregarDadosMargemContribuicao(filtro);
		}
		
		List<RelatorioMargemContribuicaoBean> listaRelatorioMargemContribuicaoBean = margemContribuicaoService.createListaRelatorioMargemContribuicaoBean(filtro, listaVenda, listaNotaFiscalProduto);
		
		if (listaRelatorioMargemContribuicaoBean != null && listaRelatorioMargemContribuicaoBean.size() > 0) {
			request.setAttribute("listaRelatorioMargemContribuicaoBean", listaRelatorioMargemContribuicaoBean);
			request.setAttribute("formaAgrupamento", filtro.getFormaAgrupamentoEnum().getDescricao().toUpperCase());
			
			return new ModelAndView("/process/relatorioMargemContribuicaoVisualizar", "filtro", filtro);
		} else {
			request.addError("Nenhum resultado encontrado.");
			
			return new ModelAndView("redirect:/faturamento/relatorio/RelatorioMargemContribuicao");
		}
	}

	@Override
	public String getNomeArquivo() {
		return "margemContribuicao";
	}

	@Override
	public String getTitulo(RelatorioMargemContribuicaoFiltro filtro) {
		return "Margem de Contribuição de Produto";
	}
	
	public ModelAndView listar(WebRequestContext request, RelatorioMargemContribuicaoFiltro filtro) {
		List<Notafiscalproduto> listaNota = null;
		List<Venda> listaVenda = null;
		
		if (filtro.getIdsNota() != null) {
			listaNota = notafiscalprodutoService.carregarDadosMargemContribuicaoNota(filtro);
		}
		
		if (filtro.getIdsVenda() != null) {
			listaVenda = vendaService.carregarDadosMargemContribuicaoVenda(filtro);
		}
		
		List<RelatorioMargemContribuicaoNotaVendaBean> listaRelatorioMargemContribuicaoNotaVendaBean = margemContribuicaoService.createLista(listaVenda, listaNota);
		
		request.setAttribute("listaRelatorioMargemContribuicaoNotaVendaBean", listaRelatorioMargemContribuicaoNotaVendaBean);
		
		return new ModelAndView("direct:process/relatorioMargemContribuicaoListagem");
	}
	
	public ModelAndView listarItens(WebRequestContext request, RelatorioMargemContribuicaoFiltro filtro) {
		List<Notafiscalproduto> listaNotaFiscalProduto = null;
		List<Venda> listaVenda = null;
		
		if (StringUtils.isNotEmpty(filtro.getCdNota()) && !"undefined".equals(filtro.getCdNota())) {
			listaNotaFiscalProduto = notafiscalprodutoService.carregarDadosMargemContribuicao(filtro);
		}
		
		if (StringUtils.isNotEmpty(filtro.getCdVenda()) && !"undefined".equals(filtro.getCdVenda())) {
			listaVenda = vendaService.carregarDadosMargemContribuicao(filtro);
		}
		
		List<RelatorioMargemContribuicaoNotaVendaItemBean> listaRelatorioMargemContribuicaoNotaVendaItemBean = margemContribuicaoService.createListaItens(listaNotaFiscalProduto, listaVenda);
		
		request.setAttribute("listaRelatorioMargemContribuicaoNotaVendaItemBean", listaRelatorioMargemContribuicaoNotaVendaItemBean);
		
		return new ModelAndView("direct:process/relatorioMargemContribuicaoItemListagem");
	}
	
	public ModelAndView limparFiltro(WebRequestContext request, RelatorioMargemContribuicaoFiltro filtro) throws Exception {
		filtro = new RelatorioMargemContribuicaoFiltro();	
		return new ModelAndView("redirect:/faturamento/relatorio/RelatorioMargemContribuicao");
	}

}