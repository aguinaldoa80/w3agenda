package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.sql.Date;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutocorrecao;
import br.com.linkcom.sined.geral.service.NotafiscalprodutocorrecaoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/faturamento/relatorio/Cartacorrecao", authorizationModule=ReportAuthorizationModule.class)
public class CartacorrecaoReport extends SinedReport<Notafiscalprodutocorrecao>{

	private NotafiscalprodutocorrecaoService notafiscalprodutocorrecaoService;
	
	public void setNotafiscalprodutocorrecaoService(
			NotafiscalprodutocorrecaoService notafiscalprodutocorrecaoService) {
		this.notafiscalprodutocorrecaoService = notafiscalprodutocorrecaoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, Notafiscalprodutocorrecao notafiscalprodutocorrecao) throws Exception {		
		return notafiscalprodutocorrecaoService.criarRelatorioCartacorrecao(notafiscalprodutocorrecao);
	}
	
	@Override
	protected void configurarParametros(WebRequestContext request, Notafiscalprodutocorrecao command, Report report) {
		Empresa empresa = this.getEmpresa(command);

		if (report != null){
			report.addParameter("LOGO", SinedUtil.getLogo(empresa));
			
			if (ParametrogeralService.getInstance().getBoolean("EXIBIR_LOGO_LINKCOM")) {
				report.addParameter("LOGO_LINKCOM", SinedUtil.getLogoLinkCom());
			}
			
			report.addParameter("EMPRESA", empresa == null ? null : (empresa.getNomefantasia() != null ? empresa.getNomefantasia() : empresa.getRazaosocialOuNome()));
			report.addParameter("TITULO", this.getTitulo(command));
			report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
			report.addParameter("DATA", new Date(System.currentTimeMillis()));
			
			this.adicionaParametrosFiltro(report, command);
		}
	}
	
	@Override
	public String getNomeArquivo() {
		return "cartacorrecao";
	}

	@Override
	public String getTitulo(Notafiscalprodutocorrecao filtro) {
		return "Carta de Corre��o";
	}
	
	@Override
	protected Empresa getEmpresa(Notafiscalprodutocorrecao bean) {
		if(bean != null && bean.getCdnotafiscalprodutocorrecao() != null){
			bean = notafiscalprodutocorrecaoService.loadWithEmpresa(bean);
			if(bean != null && bean.getNotafiscalproduto() != null && bean.getNotafiscalproduto().getEmpresa() != null){
				return empresaService.loadComArquivo(bean.getNotafiscalproduto().getEmpresa());
			} else return null;
		}
		return null;
	}
	
}
