package br.com.linkcom.sined.modulo.faturamento.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.AbstractCrudController;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(path="/faturamento/relatorio/NotaFiscalServicoRPS", authorizationModule=ReportAuthorizationModule.class)
public class NotaFiscalServicoRPSReport extends ResourceSenderController<FiltroListagemSined> {

	private NotaService notaService;
	private NotaFiscalServicoService notaFiscalServicoService;
	
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, FiltroListagemSined filtro)throws ResourceGenerationException {
		return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico?ACAO="+AbstractCrudController.LISTAGEM);
	}
	
	@Override
	public Resource generateResource(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(notaService.haveNFProduto(whereIn)){
			throw new SinedException("A��o permitida somente para nota fiscal de servi�o.");
		}
		
		return notaFiscalServicoService.gerarRelatorioNfse(whereIn, true);
	}

}
