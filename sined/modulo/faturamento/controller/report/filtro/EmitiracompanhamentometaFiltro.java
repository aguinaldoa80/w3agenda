package br.com.linkcom.sined.modulo.faturamento.controller.report.filtro;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.SinedDateUtils;

public class EmitiracompanhamentometaFiltro {

	protected Empresa empresa;
	protected Date dtreferenciaInicio;
	protected Date dtreferenciaFim;
	protected Colaborador colaborador;
	protected String tiporelatorio;
	protected Boolean isVendedorPrincipal;
		
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Data de ReferÍncia")
	public Date getDtreferenciaInicio() {
		return dtreferenciaInicio;
	}
	@Required
	public Date getDtreferenciaFim() {
		return dtreferenciaFim;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}	
	public void setDtreferenciaInicio(Date dtreferenciaInicio) {
		this.dtreferenciaInicio = dtreferenciaInicio;
	}
	public void setDtreferenciaFim(Date dtreferenciaFim) {
		this.dtreferenciaFim = dtreferenciaFim;
	}
	public Date getFirstDateMonth(){
		Date firstdatemonth = null;
		if(this.dtreferenciaInicio != null)
			firstdatemonth = SinedDateUtils.firstDateOfMonth(this.dtreferenciaInicio);
		else
			firstdatemonth = SinedDateUtils.firstDateOfMonth(SinedDateUtils.currentDate());
			
		return firstdatemonth;
	}
	public Date getLastDateMonth(){
		Date lastdatemonth = null;
		if(this.dtreferenciaFim != null)
			lastdatemonth = SinedDateUtils.lastDateOfMonth(this.dtreferenciaInicio);		
		return lastdatemonth;		
	}
	public String getTiporelatorio() {
		return tiporelatorio;
	}
	public void setTiporelatorio(String tiporelatorio) {
		this.tiporelatorio = tiporelatorio;
	}
	
	@DisplayName("Vendedor principal")
	public Boolean getIsVendedorPrincipal() {
		return isVendedorPrincipal;
	}
	
	public void setIsVendedorPrincipal(Boolean isVendedorPrincipal) {
		this.isVendedorPrincipal = isVendedorPrincipal;
	}
}
