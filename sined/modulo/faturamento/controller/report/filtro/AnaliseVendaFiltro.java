package br.com.linkcom.sined.modulo.faturamento.controller.report.filtro;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.enumeration.OrdenacaoAnaliseVendaReport;
import br.com.linkcom.sined.geral.bean.enumeration.TipoOrdenacaoAnaliseVendaReport;
import br.com.linkcom.sined.util.SinedDateUtils;

public class AnaliseVendaFiltro {

	protected Empresa empresa;
	protected Categoria categoria;
	protected Cliente cliente;
	protected Date dtinicio;
	protected Date dtfim;
	protected Boolean clienteforaperiodo;
	protected Materialtipo materialtipo;
	protected Materialgrupo materialgrupo;
	protected Colaborador[] arrayColaborador;
	protected Materialcategoria materialcategoria;
	
	protected OrdenacaoAnaliseVendaReport tipoOrdenacao;
	protected TipoOrdenacaoAnaliseVendaReport formaOrdenacao;
	protected List<Frequencia> listaFrequencia;
	
	protected Boolean isRestricaoClienteVendedor;
	protected Boolean isRestricaoVendaVendedor;
	protected Boolean isVendedorPrincipal;
	
	public AnaliseVendaFiltro() {}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Date getDtinicio() {
		if (dtinicio == null) {
			Date dataInicioRelatorio = new Date(System.currentTimeMillis());
			dataInicioRelatorio = SinedDateUtils.addMesData(dataInicioRelatorio, -3);
			return SinedDateUtils.firstDateOfMonth(dataInicioRelatorio);
		} else
			return dtinicio; 
	}
	public Date getDtfim() {
		return dtfim;
	}
	public Boolean getClienteforaperiodo() {
		if (clienteforaperiodo == null)
			return true;
		else 
			return clienteforaperiodo;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setClienteforaperiodo(Boolean clienteforaperiodo) {
		this.clienteforaperiodo = clienteforaperiodo;
	}

	@DisplayName("Tipo de Material")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	@DisplayName("Grupo de Material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Vendedor")
	public Colaborador[] getArrayColaborador() {
		return arrayColaborador;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	@DisplayName("Campo a Ordenar:")
	public OrdenacaoAnaliseVendaReport getTipoOrdenacao() {
		return tipoOrdenacao;
	}
	@DisplayName("Tipo de Ordena��o")
	public TipoOrdenacaoAnaliseVendaReport getFormaOrdenacao() {
		return formaOrdenacao;
	}
	@DisplayName("Periodicidade")
	public List<Frequencia> getListaFrequencia() {
		return listaFrequencia;
	}

	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setArrayColaborador(Colaborador[] arrayColaborador) {
		this.arrayColaborador = arrayColaborador;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	public Boolean getIsRestricaoClienteVendedor() {
		return isRestricaoClienteVendedor;
	}
	public Boolean getIsRestricaoVendaVendedor() {
		return isRestricaoVendaVendedor;
	}

	public void setIsRestricaoClienteVendedor(Boolean isRestricaoClienteVendedor) {
		this.isRestricaoClienteVendedor = isRestricaoClienteVendedor;
	}
	public void setIsRestricaoVendaVendedor(Boolean isRestricaoVendaVendedor) {
		this.isRestricaoVendaVendedor = isRestricaoVendaVendedor;
	}
	public void setTipoOrdenacao(OrdenacaoAnaliseVendaReport tipoOrdenacao) {
		this.tipoOrdenacao = tipoOrdenacao;
	}
	
	public void setListaFrequencia(List<Frequencia> listaFrequencia) {
		this.listaFrequencia = listaFrequencia;
	}

	public void setFormaOrdenacao(TipoOrdenacaoAnaliseVendaReport formaOrdenacao) {
		this.formaOrdenacao = formaOrdenacao;
	}
	
	@DisplayName("Vendedor principal")
	public Boolean getIsVendedorPrincipal() {
		return isVendedorPrincipal;
	}
	
	public void setIsVendedorPrincipal(Boolean isVendedorPrincipal) {
		this.isVendedorPrincipal = isVendedorPrincipal;
	}
}
