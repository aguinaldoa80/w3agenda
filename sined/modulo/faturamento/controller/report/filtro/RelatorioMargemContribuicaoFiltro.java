package br.com.linkcom.sined.modulo.faturamento.controller.report.filtro;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.MargemContribuicao;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.enumeration.FormaAgrupamentoEnum;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.enumeration.OrdenacaoEnum;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.enumeration.SemaforoEnum;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class RelatorioMargemContribuicaoFiltro extends FiltroListagemSined {

	private Empresa empresa;
	private MargemContribuicao margemContribuicao;
	private Date dtInicio = SinedDateUtils.firstDateOfMonth();
	private Date dtFim;
	private Cliente cliente;
	private Categoria categoria;
	private Fornecedor fornecedor;
	private Material material;
	private Materialgrupo materialGrupo;
	private Materialtipo materialTipo;
	private Materialcategoria materialCategoria;
	private SemaforoEnum semaforoEnum;
	private Boolean margem;
	private Money valorMargemInicio;
	private Money valorMargemFim;
	private Double percentualMargemInicio;
	private Double percentualMargemFim;
	private OrdenacaoEnum ordenacaoEnum;
	private FormaAgrupamentoEnum formaAgrupamentoEnum;
	private Boolean vendaFaturada = Boolean.TRUE;
	private Boolean vendaRealizada = Boolean.TRUE;
	private Boolean origem;
	
	private Boolean mostrarTela = Boolean.FALSE;
	
	private String idsNota;
	private String idsVenda;
	
	private String cdNota;
	private String cdVenda;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@DisplayName("Margem de Contribuição")
	public MargemContribuicao getMargemContribuicao() {
		return margemContribuicao;
	}

	public void setMargemContribuicao(MargemContribuicao margemContribuicao) {
		this.margemContribuicao = margemContribuicao;
	}

	public Date getDtInicio() {
		return dtInicio;
	}

	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}

	public Date getDtFim() {
		return dtFim;
	}

	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Categoria getCategoria() {
		return categoria;
	}
	
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public Materialgrupo getMaterialGrupo() {
		return materialGrupo;
	}

	public void setMaterialGrupo(Materialgrupo materialGrupo) {
		this.materialGrupo = materialGrupo;
	}

	public Materialtipo getMaterialTipo() {
		return materialTipo;
	}

	public void setMaterialTipo(Materialtipo materialTipo) {
		this.materialTipo = materialTipo;
	}

	public Materialcategoria getMaterialCategoria() {
		return materialCategoria;
	}

	public void setMaterialCategoria(Materialcategoria materialCategoria) {
		this.materialCategoria = materialCategoria;
	}

	@DisplayName("Semáforo")
	public SemaforoEnum getSemaforoEnum() {
		return semaforoEnum;
	}

	public void setSemaforoEnum(SemaforoEnum semaforoEnum) {
		this.semaforoEnum = semaforoEnum;
	}

	public Boolean getMargem() {
		return margem;
	}

	public void setMargem(Boolean margem) {
		this.margem = margem;
	}

	public Money getValorMargemInicio() {
		return valorMargemInicio;
	}

	public void setValorMargemInicio(Money valorMargemInicio) {
		this.valorMargemInicio = valorMargemInicio;
	}

	public Money getValorMargemFim() {
		return valorMargemFim;
	}

	public void setValorMargemFim(Money valorMargemFim) {
		this.valorMargemFim = valorMargemFim;
	}

	public Double getPercentualMargemInicio() {
		return percentualMargemInicio;
	}

	public void setPercentualMargemInicio(Double percentualMargemInicio) {
		this.percentualMargemInicio = percentualMargemInicio;
	}

	public Double getPercentualMargemFim() {
		return percentualMargemFim;
	}

	public void setPercentualMargemFim(Double percentualMargemFim) {
		this.percentualMargemFim = percentualMargemFim;
	}

	@DisplayName("Ordenação")
	public OrdenacaoEnum getOrdenacaoEnum() {
		return ordenacaoEnum;
	}

	public void setOrdenacaoEnum(OrdenacaoEnum ordenacaoEnum) {
		this.ordenacaoEnum = ordenacaoEnum;
	}

	@DisplayName("Forma de Agrupamento")
	public FormaAgrupamentoEnum getFormaAgrupamentoEnum() {
		return formaAgrupamentoEnum;
	}

	public void setFormaAgrupamentoEnum(FormaAgrupamentoEnum formaAgrupamentoEnum) {
		this.formaAgrupamentoEnum = formaAgrupamentoEnum;
	}

	public Boolean getVendaFaturada() {
		return vendaFaturada;
	}

	public void setVendaFaturada(Boolean vendaFaturada) {
		this.vendaFaturada = vendaFaturada;
	}

	public Boolean getVendaRealizada() {
		return vendaRealizada;
	}

	public void setVendaRealizada(Boolean vendaRealizada) {
		this.vendaRealizada = vendaRealizada;
	}
	
	public Boolean getOrigem() {
		return origem;
	}
	
	public void setOrigem(Boolean origem) {
		this.origem = origem;
	}
	
	public String getOrigemString() {
		if (origem != null && origem) {
			return "Venda";
		} else if (origem != null && !origem) {
			return "Nota";
		} else {
			return "Todos";
		}
	}
	
	public String getMargemString() {
		if (margem != null && margem) {
			return "Valor";
		} else if (margem != null && !margem) {
			return "Percentual";
		} else {
			return "";
		}
	}
	
	public Boolean getMostrarTela() {
		return mostrarTela;
	}
	
	public void setMostrarTela(Boolean mostrarTela) {
		this.mostrarTela = mostrarTela;
	}
	
	public String getIdsNota() {
		return idsNota;
	}
	
	public void setIdsNota(String idsNota) {
		this.idsNota = idsNota;
	}
	
	public String getIdsVenda() {
		return idsVenda;
	}
	
	public void setIdsVenda(String idsVenda) {
		this.idsVenda = idsVenda;
	}

	public String getCdNota() {
		return cdNota;
	}

	public void setCdNota(String cdNota) {
		this.cdNota = cdNota;
	}

	public String getCdVenda() {
		return cdVenda;
	}

	public void setCdVenda(String cdVenda) {
		this.cdVenda = cdVenda;
	}
}
