package br.com.linkcom.sined.modulo.faturamento.controller.report.filtro;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.TipoPessoaVendaEnum;
import br.com.linkcom.sined.util.SinedDateUtils;

public class EmitircustovendaFiltro {

	protected Empresa empresa;
	protected Date dtinicio;
	protected Date dtfim;
	protected Cliente cliente;
	protected Contacrm contacrm;
	protected Colaborador colaborador;
	protected Material material;
	protected Materialcategoria materialcategoria;
	protected String calcularmargempor;
	protected TipoPessoaVendaEnum tipopessoa;
	
	private Boolean produto;
	private Boolean patrimonio;
	private Boolean epi;
	private Boolean servico;
	
	private String whereInPedidovenda;
	private String whereInVenda;
	private String whereInVendaorcamento;
	protected Boolean isRestricaoClienteVendedor;
	protected Boolean isRestricaoVendaVendedor;
	protected Boolean vendedoresAtuais;
	protected Boolean considerarDescontos = Boolean.TRUE;
	protected List<Pedidovendatipo> listapedidovendatipo;
	protected String origem;
	protected List<Pedidovendasituacao> listapedidovendasituacao;
	protected Boolean exibirformulacalculo;
	protected Boolean considerarValeCompra = Boolean.TRUE;
	
	protected Boolean isVendedorPrincipal;

	public Empresa getEmpresa() {
		return empresa;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Produto")
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Categoria material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	
	public Boolean getProduto() {
		return produto;
	}
	public Boolean getPatrimonio() {
		return patrimonio;
	}
	public Boolean getEpi() {
		return epi;
	}
	public Boolean getServico() {
		return servico;
	}
	public Boolean getVendedoresAtuais() {
		return vendedoresAtuais;
	}
	public void setProduto(Boolean produto) {
		this.produto = produto;
	}
	public void setPatrimonio(Boolean patrimonio) {
		this.patrimonio = patrimonio;
	}
	public void setEpi(Boolean epi) {
		this.epi = epi;
	}
	public void setServico(Boolean servico) {
		this.servico = servico;
	}
	public void setVendedoresAtuais(Boolean vendedoresAtuais) {
		this.vendedoresAtuais = vendedoresAtuais;
	}
	public Date getDtinicio() {
		if (dtinicio == null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(System.currentTimeMillis());
			return SinedDateUtils.firstDateOfMonth(new Date(calendar.getTimeInMillis()));
		} else
			return dtinicio; 
	}
	public Date getDtfim() {
		if (dtfim == null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(System.currentTimeMillis());
			return SinedDateUtils.lastDateOfMonth(new Date(calendar.getTimeInMillis()));
		} else
			return dtfim;
	}
	@DisplayName("Tipo pedido de venda")
	public List<Pedidovendatipo> getListapedidovendatipo() {
		return listapedidovendatipo;
	}
	@DisplayName("Origem")
	public String getOrigem() {
		return origem;
	}
	@DisplayName("Situa��o pedido de venda")
	public List<Pedidovendasituacao> getListapedidovendasituacao() {
		return listapedidovendasituacao;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public String getWhereInPedidovenda() {
		return whereInPedidovenda;
	}
	public String getWhereInVenda() {
		return whereInVenda;
	}
	public String getWhereInVendaorcamento() {
		return whereInVendaorcamento;
	}
	public void setWhereInPedidovenda(String whereInPedidovenda) {
		this.whereInPedidovenda = whereInPedidovenda;
	}
	public void setWhereInVenda(String whereInVenda) {
		this.whereInVenda = whereInVenda;
	}
	public void setWhereInVendaorcamento(String whereInVendaorcamento) {
		this.whereInVendaorcamento = whereInVendaorcamento;
	}

	@DisplayName("Calcular margem por")
	public String getCalcularmargempor() {
		return calcularmargempor != null ? calcularmargempor : "Custo";
	}
	public void setCalcularmargempor(String calcularmargempor) {
		this.calcularmargempor = calcularmargempor;
	}

	@DisplayName("Tipo de pessoa")
	public TipoPessoaVendaEnum getTipopessoa() {
		return tipopessoa;
	}

	public void setTipopessoa(TipoPessoaVendaEnum tipopessoa) {
		this.tipopessoa = tipopessoa;
	}
	
	public Boolean getIsRestricaoClienteVendedor() {
		return isRestricaoClienteVendedor;
	}
	public Boolean getIsRestricaoVendaVendedor() {
		return isRestricaoVendaVendedor;
	}
	public void setIsRestricaoClienteVendedor(Boolean isRestricaoClienteVendedor) {
		this.isRestricaoClienteVendedor = isRestricaoClienteVendedor;
	}
	public void setIsRestricaoVendaVendedor(Boolean isRestricaoVendaVendedor) {
		this.isRestricaoVendaVendedor = isRestricaoVendaVendedor;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	@DisplayName("Considerar desconto no lucro")
	public Boolean getConsiderarDescontos() {
		return considerarDescontos;
	}
	public void setConsiderarDescontos(Boolean considerarDescontos) {
		this.considerarDescontos = considerarDescontos;
	}
	public void setListapedidovendasituacao(List<Pedidovendasituacao> listapedidovendasituacao) {
		this.listapedidovendasituacao = listapedidovendasituacao;
	}
	public void setListapedidovendatipo(List<Pedidovendatipo> listapedidovendatipo) {
		this.listapedidovendatipo = listapedidovendatipo;
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	
	@DisplayName("Exibir f�rmula de c�lculo")
	public Boolean getExibirformulacalculo() {
		return exibirformulacalculo;
	}
	public void setExibirformulacalculo(Boolean exibirformulacalculo) {
		this.exibirformulacalculo = exibirformulacalculo;
	}
	public void setConsiderarValeCompra(Boolean considerarValeCompra) {
		this.considerarValeCompra = considerarValeCompra;
	}
	@DisplayName("Considerar vale compra no lucro")
	public Boolean getConsiderarValeCompra() {
		return considerarValeCompra;
	}
	
	@DisplayName("Vendedor principal")
	public Boolean getIsVendedorPrincipal() {
		return isVendedorPrincipal;
	}
	
	public void setIsVendedorPrincipal(Boolean isVendedorPrincipal) {
		this.isVendedorPrincipal = isVendedorPrincipal;
	}

	@DisplayName("Conta")
	public Contacrm getContacrm() {
		return contacrm;
	}

	public void setContacrm(Contacrm contacrm) {
		this.contacrm = contacrm;
	}
}
