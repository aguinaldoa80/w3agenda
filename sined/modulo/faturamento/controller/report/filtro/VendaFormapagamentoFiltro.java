package br.com.linkcom.sined.modulo.faturamento.controller.report.filtro;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;

public class VendaFormapagamentoFiltro {

	protected Empresa empresa;
	protected Date dtvenda1;
	protected Date dtvenda2;
	protected List<Documentotipo> listaDocumentotipo = new ListSet<Documentotipo>(Documentotipo.class);
	
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	public Date getDtvenda1() {
		return dtvenda1;
	}
	@Required
	public Date getDtvenda2() {
		return dtvenda2;
	}
	public List<Documentotipo> getListaDocumentotipo() {
		return listaDocumentotipo;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtvenda1(Date dtvenda1) {
		this.dtvenda1 = dtvenda1;
	}
	public void setDtvenda2(Date dtvenda2) {
		this.dtvenda2 = dtvenda2;
	}
	public void setListaDocumentotipo(List<Documentotipo> listaDocumentotipo) {
		this.listaDocumentotipo = listaDocumentotipo;
	}
	
}