package br.com.linkcom.sined.modulo.faturamento.controller.report.filtro;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Vendasituacao;

public class VendasPorVendedorFiltro {

	protected Empresa empresa;
	protected Cliente cliente;
	protected Date dtinicio;
	protected Date dtfim;
	protected Colaborador[] colaboradorList;
	protected List<Vendasituacao> situacaoList;
	protected Boolean recapagem;

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Date getDtinicio() {
		return dtinicio;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public Date getDtfim() {
		return dtfim;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

	@DisplayName("Vendedor")
	public Colaborador[] getColaboradorList() {
		return colaboradorList;
	}
	
	public void setColaboradorList(Colaborador[] colaboradorList) {
		this.colaboradorList = colaboradorList;
	}

	@DisplayName("Situa��o")
	public List<Vendasituacao> getSituacaoList() {
		return situacaoList;
	}

	public void setSituacaoList(List<Vendasituacao> situacaoList) {
		this.situacaoList = situacaoList;
	}
	
	public Boolean getRecapagem() {
		return recapagem;
	}
	
	public void setRecapagem(Boolean recapagem) {
		this.recapagem = recapagem;
	}
	
	public String getSituacaoListString() {
		StringBuilder s = new StringBuilder();
		for (Vendasituacao v : situacaoList) {
			if (v.equals(Vendasituacao.EMPRODUCAO)) {
				s.append(Vendasituacao.EMPRODUCAO.getDescricao());
			} else if (v.equals(Vendasituacao.FATURADA)) {
				s.append(Vendasituacao.FATURADA.getDescricao());
			} else if (v.equals(Vendasituacao.REALIZADA)) {
				s.append(Vendasituacao.REALIZADA.getDescricao());
			}			
			s.append(", ");
		}
		
		return s.toString().substring(0, s.length() - 2);
	}
}
