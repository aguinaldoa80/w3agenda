package br.com.linkcom.sined.modulo.faturamento.controller.report.filtro;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.EmpresaService;

public class EvolucaoFaturamentoFiltro {

	protected Date dtinicio;
	protected Date dtfim;
	protected Empresa empresa;
	
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public boolean isPrincipal() {
		if(getEmpresa() != null){
			return EmpresaService.getInstance().loadPrincipal().getCdpessoa().equals(getEmpresa().getCdpessoa());
		} else return false;
	}
	
}
