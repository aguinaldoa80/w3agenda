package br.com.linkcom.sined.modulo.faturamento.controller.report.filtro;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;

public class EmitirIndenizacaoContratoFiltro {

	private Empresa empresa;
	private Date dtinicio;
	private Date dtfim;
	private Cliente cliente;
	private Contrato contrato;
	private Material material;
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	@Required
	public Date getDtinicio() {
		return dtinicio;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	@Required
	public Date getDtfim() {
		return dtfim;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@DisplayName("Contrato")
	public Contrato getContrato() {
		return contrato;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	
	@DisplayName("Material indenizado")
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
}
