package br.com.linkcom.sined.modulo.faturamento.controller.report.filtro;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;

public class QuantitativaDeVendasFiltro extends FiltroListagem{

	private Empresa empresa;
	private Date dtInicio;
	private Date dtFim;
	private Materialgrupo materialgrupo;
	private Materialtipo materialtipo;
	private String tipoDeOrdenacao;
	private String[] listaTipoDeOrdenacao = {"Crescente", "Decrescente"};
	
	
	public QuantitativaDeVendasFiltro() {
		this.tipoDeOrdenacao = "Decrescente";
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@DisplayName("Data In�cio")
	@Required
	public Date getDtInicio() {
		return dtInicio;
	}
	
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	
	@DisplayName("Data Fim")
	@Required
	public Date getDtFim() {
		return dtFim;
	}

	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}

	@DisplayName("Grupo de Material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}

	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}

	@DisplayName("Tipo de Material")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}

	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	
	@DisplayName("Ordena��o")
	public String getTipoDeOrdenacao() {
		return tipoDeOrdenacao;
	}

	public void setTipoDeOrdenacao(String tipoDeOrdenacao) {
		this.tipoDeOrdenacao = tipoDeOrdenacao;
	}

	public String[] getListaTipoDeOrdenacao() {
		return listaTipoDeOrdenacao;
	}
}
