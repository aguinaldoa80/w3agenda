package br.com.linkcom.sined.modulo.faturamento.controller.report.filtro;

import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;

public class EmitirFaturaLocacaoContratoFiltro extends ReportTemplateFiltro {
	
	private String whereIn;
	
	public EmitirFaturaLocacaoContratoFiltro(){
	}
	
	public EmitirFaturaLocacaoContratoFiltro(String whereIn){
		this.whereIn = whereIn;
	}
	
	public String getWhereIn() {
		return whereIn;
	}
	
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
}
