package br.com.linkcom.sined.modulo.faturamento.controller.report.filtro;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ApuracaovendaFiltro extends FiltroListagemSined {
	
	protected Empresa empresa;
	protected Date dtinicio;
	protected Date dtfim;
	protected Materialgrupo materialgrupo;
	protected Cliente cliente;
	protected Documentotipo documentotipo;
	protected Boolean vendedoresatuais;
	protected Colaborador colaborador;
	protected String tiporelatorio;
	protected Materialcategoria materialcategoria;
	protected Boolean considerarfrete;
	protected Boolean isVendedorPrincipal;
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	public Date getDtinicio() {
		return dtinicio;
	}
	@Required
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Grupo de Material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Forma de Pagamento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	@DisplayName("Vendedores Atuais")
	public Boolean getVendedoresatuais() {
		return vendedoresatuais;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	@DisplayName("Considerar Frete")
	public Boolean getConsiderarfrete() {
		return considerarfrete;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public String getTiporelatorio() {
		return tiporelatorio;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setVendedoresatuais(Boolean vendedoresatuais) {
		this.vendedoresatuais = vendedoresatuais;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setTiporelatorio(String tiporelatorio) {
		this.tiporelatorio = tiporelatorio;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	public void setConsiderarfrete(Boolean considerarfrete) {
		this.considerarfrete = considerarfrete;
	}
	
	@DisplayName("Vendedor principal")
	public Boolean getIsVendedorPrincipal() {
		return isVendedorPrincipal;
	}
	
	public void setIsVendedorPrincipal(Boolean isVendedorPrincipal) {
		this.isVendedorPrincipal = isVendedorPrincipal;
	}
}
