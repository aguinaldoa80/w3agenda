package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.FrequenciaService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.AnaliseVendaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/faturamento/relatorio/AnaliseVenda",	authorizationModule=ReportAuthorizationModule.class)
public class AnaliseVendaReport extends SinedReport<AnaliseVendaFiltro> {

	private VendaService vendaService;
	private CategoriaService categoriaservice;
	private UsuarioService usuarioService;
	private ColaboradorService colaboradorService;
	private FrequenciaService frequenciaService;
	
	public void setCategoriaservice(CategoriaService categoriaservice) {
		this.categoriaservice = categoriaservice;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setFrequenciaService(FrequenciaService frequenciaService) {
		this.frequenciaService = frequenciaService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, AnaliseVendaFiltro filtro) throws Exception {
		filtro.setIsRestricaoClienteVendedor(usuarioService.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado()));
		filtro.setIsRestricaoVendaVendedor(usuarioService.isRestricaoVendaVendedor(SinedUtil.getUsuarioLogado()));
		
		String parameterMensal = request.getParameter("mensal");
		if(parameterMensal != null && parameterMensal.trim().toUpperCase().equals("TRUE")){
			return vendaService.gerarRelatorioMensalAnaliseVenda(filtro);
		}
		return vendaService.gerarRelatorioSinteticoAnaliseVenda(filtro);
	}

	@Override
	protected void adicionaParametrosFiltro(Report report, AnaliseVendaFiltro filtro) {
		report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
		report.addParameter("CLIENTE", filtro.getCliente() != null ? clienteService.load(filtro.getCliente(), "cliente.nome").getNome() : null);
		report.addParameter("CATEGORIA", filtro.getCategoria() != null ? categoriaservice.load(filtro.getCategoria(), "categoria.nome").getNome() : null);				
		super.adicionaParametrosFiltro(report, filtro);
	}	
	
	@Override
	public String getNomeArquivo() {
		return "analisevenda";
	}

	@Override
	public String getTitulo(AnaliseVendaFiltro filtro) {
		String parameterMensal = NeoWeb.getRequestContext().getParameter("mensal");
		if(parameterMensal != null && parameterMensal.trim().toUpperCase().equals("TRUE")){
			return "AN�LISE DE VENDA MENSAL";
		}
		
		if (filtro.getClienteforaperiodo())
			return "CLIENTES FORA DA SUA PERIODICIDADE DE COMPRA";
		else	
			return "AN�LISE DE VENDA";
	}

	public ModelAndView ajaxLoadComboVendedor(WebRequestContext request){
		if(request.getParameter("cdempresa") == null)
			throw new SinedException("Empresa n�o pode ser nula");
		
		Empresa empresa = null;
		try {
			empresa = new Empresa(Integer.parseInt(request.getParameter("cdempresa")));
		} catch (Exception e) {}
		
		List<Colaborador> lista = colaboradorService.findWithVenda(empresa);
		
		StringBuilder strOpt = new StringBuilder();
		if(lista != null && !lista.isEmpty()){
			for(Colaborador c : lista){
				strOpt.append("<option value='br.com.linkcom.sined.geral.bean.Colaborador[cdpessoa=")
					.append(c.getCdpessoa())
					.append("]'>")
					.append(c.getNome())
					.append("</option>");
			}
		}
		
		return new JsonModelAndView().addObject("strHtml", strOpt.toString());
	}
	
	@Override
	protected void filtro(WebRequestContext request, AnaliseVendaFiltro filtro) throws Exception {
		super.filtro(request, filtro);
		request.setAttribute("listaFrequencia", frequenciaService.findAll());
		request.setAttribute("listaColaborador", colaboradorService.findWithVenda(filtro.getEmpresa()));
	}
}
