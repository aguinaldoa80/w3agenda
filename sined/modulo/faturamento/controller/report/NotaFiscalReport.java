package br.com.linkcom.sined.modulo.faturamento.controller.report;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.AbstractCrudController;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path="/faturamento/relatorio/NotaFiscal", authorizationModule=ReportAuthorizationModule.class)
public class NotaFiscalReport extends SinedReport<NotaFiltro> {

	private NotaService notaService;
	
	public void setNotaService(NotaService notaService) {
		this.notaService = notaService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, NotaFiltro filtro)throws Exception {
		if(notaService.haveNFProduto(filtro.getWhereIn())){
			throw new SinedException("A��o permitida somente para nota fiscal de servi�o.");
		}
		
		return notaService.gerarRelatorioNotaFiscal(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, NotaFiltro filtro)throws ResourceGenerationException {
		return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico?ACAO="+AbstractCrudController.LISTAGEM);
	}
	
	@Override
	protected void validate(Object obj, BindException errors, String acao) {
		if (obj instanceof NotaFiltro) {
			NotaFiltro filtro = (NotaFiltro) obj;
			
			/*
			 * Valida a a�ao GERAR
			 */
			if(GERAR.equals(acao)){
				if(StringUtils.isBlank(filtro.getWhereIn())){
					errors.reject("001","Nenhuma nota especificada.");
				}
			}
		}
	}
	
	@Override
	public String getNomeArquivo() {
		return "notafiscal";
	}

	@Override
	public String getTitulo(NotaFiltro filtro) {
		return "Nota Fiscal";
	}

}
