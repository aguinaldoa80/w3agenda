package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentosituacao;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.RegiaoService;
import br.com.linkcom.sined.geral.service.VendaorcamentoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.VendaorcamentoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Bean
@Controller(path="/faturamento/relatorio/GerenciarVendaOrcamento", authorizationModule=ReportAuthorizationModule.class)
public class GerenciarVendaOrcamentoReport extends SinedReport<VendaorcamentoFiltro>{
	
	private VendaorcamentoService vendaorcamentoService;
	private ColaboradorService colaboradorService;
	private MaterialService materialService;
	private RegiaoService regiaoService;
	
	public void setRegiaoService(RegiaoService regiaoService) {this.regiaoService = regiaoService;}
	public void setVendaorcamentoService(VendaorcamentoService vendaorcamentoService) {this.vendaorcamentoService = vendaorcamentoService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}

	@Override
	public IReport createReportSined(WebRequestContext request, VendaorcamentoFiltro filtro) throws Exception {
		if(filtro.getRegiao() != null){
			filtro.setRegiao(regiaoService.loadForFiltro(filtro.getRegiao()));
		}
		return vendaorcamentoService.gerarRelatorioListagem(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "gerenciarvendaorcamento";
	}

	@Override
	public String getTitulo(VendaorcamentoFiltro filtro) {
		return "GERENCIAR OR�AMENTO";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, VendaorcamentoFiltro filtro) {
		
		report.addParameter("VENDEDOR", filtro.getColaborador() != null ? colaboradorService.load(filtro.getColaborador(), "colaborador.nome").getNome() : null);
		report.addParameter("EMPRESAFILTRO", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		report.addParameter("CODIGOVENDAORCAMENTO", filtro.getWhereIncdvendaorcamento());
		String dataInicio = null;
		String dataFim = null;
		if (filtro.getDtinicio() != null){
			dataInicio = SinedDateUtils.toString(filtro.getDtinicio());
		}
		if (filtro.getDtfim() != null){
			dataFim = SinedDateUtils.toString(filtro.getDtfim());
		}
		report.addParameter("DTINICIO", dataInicio);
		report.addParameter("DTFIM", dataFim);
		report.addParameter("CLIENTE", filtro.getCliente() != null ? clienteService.load(filtro.getCliente(), "cliente.nome").getNome() : null);
		report.addParameter("PRODUTO", filtro.getMaterial() != null ? materialService.load(filtro.getMaterial(), "material.nome").getNome() : null);
		
		
		List<Vendaorcamentosituacao> listaVendaorcamentosituacao = filtro.getListaVendaorcamentosituacao();
		if(listaVendaorcamentosituacao != null && listaVendaorcamentosituacao.size() > 0){
			
			for (Vendaorcamentosituacao vendaorcamentosituacao : listaVendaorcamentosituacao) {
				switch (vendaorcamentosituacao.getCdvendaorcamentosituacao()) {
					case 1:
						vendaorcamentosituacao.setDescricao(Vendaorcamentosituacao.EM_ABERTO.getDescricao());
						break;
					case 2:
						vendaorcamentosituacao.setDescricao(Vendaorcamentosituacao.AUTORIZADO.getDescricao());
						break;
					case 3:
						vendaorcamentosituacao.setDescricao(Vendaorcamentosituacao.CANCELADA.getDescricao());
						break;
					case 4:
						vendaorcamentosituacao.setDescricao(Vendaorcamentosituacao.REPROVADO.getDescricao());
						break;
					default:
						break;
				}
			}
			
			report.addParameter("SITUACOES", CollectionsUtil.listAndConcatenate(listaVendaorcamentosituacao, "descricao", ", "));
		}
	}
	
	/**
	 * Verifica se a empresa da venda possui um comprovante configur�vel definido 
	 * @param request
	 * @param venda
	 * @return
	 * @since 25/06/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView gerarCsv(WebRequestContext request, VendaorcamentoFiltro filtro){
		if(filtro.getRegiao() != null){
			filtro.setRegiao(regiaoService.loadForFiltro(filtro.getRegiao()));
		}
		List<Vendaorcamento> listaVendaorcamento = vendaorcamentoService.findForCsv(filtro);
		
		StringBuilder rel = new StringBuilder();
		
		rel.append("\"C�digo\";");
		rel.append("\"Empresa\";");
		rel.append("\"Cliente/Conta\";");
		rel.append("\"CPF/CNPJ\";");
		rel.append("\"Local de entrega\";");
		rel.append("\"Data\";");
		rel.append("\"Quantidade total\";");
		rel.append("\"Valor total\";");
		rel.append("\"Situa��o\";");
		rel.append("\"Indica��o\";");
		rel.append("\"Criado por\";");
	

		rel.append("\n");
				
		for (Vendaorcamento vendaorcamento : listaVendaorcamento) {
			rel.append("\"" + vendaorcamento.getCdvendaorcamento());
			rel.append("\";");
			
			if(vendaorcamento.getEmpresa() != null) rel.append("\"" + vendaorcamento.getEmpresa().getRazaosocialOuNome());
			rel.append("\";");
			
			if(vendaorcamento.getCliente() != null) rel.append("\"" + vendaorcamento.getCliente().getNome());
			if(vendaorcamento.getContacrm() != null) rel.append("\"" + vendaorcamento.getContacrm().getNome());
			rel.append("\";");
			
			if(vendaorcamento.getCliente() != null) {
				if(vendaorcamento.getCliente().getCnpj() != null){
					rel.append("\"" + vendaorcamento.getCliente().getCnpj());
				} else if(vendaorcamento.getCliente().getCpf() != null){
					rel.append("\"" + vendaorcamento.getCliente().getCpf());
				} else {
					rel.append("\"");
				}
			} else {
				rel.append("\"");
			}
			rel.append("\";");
			
			if(vendaorcamento.getLocalarmazenagem() != null) 
				rel.append("\"" + vendaorcamento.getLocalarmazenagem().getNome());
			else
				rel.append("\"");
			rel.append("\";");
			
			if(vendaorcamento.getDtorcamento() != null) 
				rel.append("\"" + SinedDateUtils.toString(vendaorcamento.getDtorcamento()));
			else
				rel.append("\"");
			rel.append("\";");
			
			Double totalqtde = vendaorcamento.getTotalquantidades();
			Money totalvenda = vendaorcamento.getTotalvendaMaisImpostos();
			
			rel.append("\"" + (totalqtde != null ? new Money(totalqtde) : ""));
			rel.append("\";");
			
			rel.append("\"" + (totalvenda != null ? totalvenda : ""));
			rel.append("\";");
			
			if(vendaorcamento.getVendaorcamentosituacao() != null) 
				rel.append("\"" + SinedUtil.escapeCsv(vendaorcamento.getVendaorcamentosituacao().getDescricao()));
			else
				rel.append("\"");
			rel.append("\";");
			
			if(vendaorcamento.getClienteindicacao() != null) 
				rel.append("\"" + SinedUtil.escapeCsv(vendaorcamento.getClienteindicacao().getNome()));
			else
				rel.append("\"");
			rel.append("\";");
			
			if(vendaorcamento.getColaborador() != null) 
				rel.append("\"" + SinedUtil.escapeCsv(vendaorcamento.getColaborador().getNome()));
			else
				rel.append("\"");
			rel.append("\";");
			rel.append("\n");
		}
		
		Resource resource = new Resource("text/csv","vendaorcamento_" + SinedUtil.datePatternForReport() + ".csv", rel.toString().replaceAll(";null;", ";;").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
}
