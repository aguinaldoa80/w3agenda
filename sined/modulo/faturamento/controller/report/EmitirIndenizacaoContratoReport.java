package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EmitirIndenizacaoContratoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path="/faturamento/relatorio/EmitirIndenizacaoContrato", authorizationModule=ReportAuthorizationModule.class)
public class EmitirIndenizacaoContratoReport extends SinedReport<EmitirIndenizacaoContratoFiltro>{

	private ContratoService contratoService;
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,
			EmitirIndenizacaoContratoFiltro filtro) throws Exception {
		return contratoService.makeReportIndenizacaoContrato(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "indenizacaocontrato";
	}

	@Override
	public String getTitulo(EmitirIndenizacaoContratoFiltro filtro) {
		return "RELATÓRIO DE INDENIZAÇÃO";
	}
	
	public void ajaxPreencheContratosByCliente(WebRequestContext request, EmitirIndenizacaoContratoFiltro filtro){
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		List<Contrato> listaContratos = filtro.getCliente() != null? contratoService.findForIndenizacao(filtro.getCliente()): new ArrayList<Contrato>();
		view.println(SinedUtil.convertToJavaScript(listaContratos, "listaContratos", ""));
	}
}
