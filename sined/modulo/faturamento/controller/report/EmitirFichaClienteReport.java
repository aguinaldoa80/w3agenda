package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.LinkedHashMap;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path = "/crm/relatorio/EmitirFichaCliente", authorizationModule=ReportAuthorizationModule.class)
public class EmitirFichaClienteReport extends ReportTemplateController<ReportTemplateFiltro>{

	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.EMITIR_FICHA_CLIENTE;
	}

	@Override
	protected String getFileName() {
		return "ficha_cliente";
	}

	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	ReportTemplateFiltro filtro) {
		return getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.EMITIR_FICHA_CLIENTE);
	}
	
	@Override
	protected LinkedHashMap<String, Object> carregaDados(WebRequestContext request, ReportTemplateFiltro filtro) throws Exception {
		LinkedHashMap<String, Object> mapa =  new LinkedHashMap<String, Object>();
		mapa.put("lista", clienteService.gerarListaDadosForEmitirFichaCliente(SinedUtil.getItensSelecionados(request)));
		return mapa;
	}
}
