package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.PedidovendaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirEtiquetaDestinatarioBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirEtiquetaDestinatarioRemetenteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirEtiquetaRemetenteBean;

@Controller(path = "/faturamento/relatorio/EmitirEtiquetaDestinatarioRemetente", authorizationModule=ReportAuthorizationModule.class)
public class EmitirEtiquetaDestinatarioRemetenteReport extends ReportTemplateController<ReportTemplateFiltro>{
	
	private VendaService vendaService;
	
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}


	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.ETIQUETA_DESTINATARIO_REMETENTE;
	}

	@Override
	protected String getFileName() {
		return "etiqueta_destinatario_remetente";
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request, ReportTemplateFiltro filtro) {
		if(filtro != null && filtro.getTemplate() != null && filtro.getTemplate().getCdreporttemplate() != null){
			return getReportTemplateService().load(filtro.getTemplate());
		}
		
		return getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.ETIQUETA_DESTINATARIO_REMETENTE);
	}	
	
	@Override
	protected LinkedHashMap<String, Object> carregaDados(
			WebRequestContext request, ReportTemplateFiltro filtro)
			throws Exception {
		LinkedHashMap<String,Object> datasource = new LinkedHashMap<String, Object>();
		String whereIn = request.getParameter("selectedItens");

		List<Venda> vendas = vendaService.findForEtiquetaDestinatarioRemetente(whereIn);
		
		EmitirEtiquetaDestinatarioRemetenteBean bean = new EmitirEtiquetaDestinatarioRemetenteBean();
		for(Venda venda: vendas){
			EmitirEtiquetaRemetenteBean remetente = new EmitirEtiquetaRemetenteBean(venda.getEmpresa());
			EmitirEtiquetaDestinatarioBean destinatario = new EmitirEtiquetaDestinatarioBean(venda.getCliente());
			
			bean.getListaDestinatarios().add(destinatario);
			bean.getListaRemetentes().add(remetente);
		}
		datasource.put("bean", bean);
		return datasource;
	}
}
