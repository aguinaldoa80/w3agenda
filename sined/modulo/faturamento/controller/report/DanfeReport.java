package br.com.linkcom.sined.modulo.faturamento.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(
			path = "/faturamento/relatorio/Danfe", 
			authorizationModule=ReportAuthorizationModule.class
)
public class DanfeReport extends ResourceSenderController<FiltroListagemSined>{

	private NotafiscalprodutoService notafiscalprodutoService;
	private NotaService notaService;
	
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	
	@Override
	public Resource generateResource(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		String itensSelecionados = SinedUtil.getItensSelecionados(request);
		if(notaService.haveNFDiferenteStatus(itensSelecionados, NotaStatus.EM_ESPERA, NotaStatus.EMITIDA, NotaStatus.NFE_EMITIDA, NotaStatus.NFE_LIQUIDADA, NotaStatus.CANCELADA, NotaStatus.NFE_DENEGADA)){
			request.addError("Para impress�o do danfe as notas tem que estar com a situa��o 'Em Espera', 'Emitida', 'NF-e EMITIDA', 'NF-e LIQUIDADA', 'NF-e DENEGADA' ou 'CANCELADA'.");
			return null;
		}
		if(notafiscalprodutoService.haveNotaCriadasViaIntegracaoECF(itensSelecionados)){
			request.addError("N�o foi poss�vel imprimir a DANFE. Nota importada via integra��o com ECF.");
			return null;
		}
			
		return notafiscalprodutoService.gerarDanfe(itensSelecionados);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		if(request.getParameter("isNfe") != null && !"true".equalsIgnoreCase(request.getParameter("isNfe"))){
			return new ModelAndView("redirect:/faturamento/crud/Notafiscalconsumidor");
		}else {
			return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto");
		}
	}
	
}
