package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumTipoEmissao;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.util.GeradorRelatorioException;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.ColetaMaterialPedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.service.ColetaService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendahistoricoService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.geral.service.VendahistoricoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EtiquetaPneuFiltro;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path = "/faturamento/relatorio/EmitirFichaColeta", authorizationModule=ReportAuthorizationModule.class)
public class EmitirFichaColetaReport extends ReportTemplateController<EtiquetaPneuFiltro>{

	private ColetaService coletaService;
	private PedidovendaService pedidovendaService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private VendaService vendaService;
	private VendahistoricoService vendahistoricoService;
	
	public void setVendahistoricoService(
			VendahistoricoService vendahistoricoService) {
		this.vendahistoricoService = vendahistoricoService;
	}
	public void setPedidovendahistoricoService(
			PedidovendahistoricoService pedidovendahistoricoService) {
		this.pedidovendahistoricoService = pedidovendahistoricoService;
	}
	public void setColetaService(ColetaService coletaService) {
		this.coletaService = coletaService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	
	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.EMITIR_FICHA_COLETA;
	}

	@Override
	protected String getFileName() {
		return "ficha_coleta";
	}

	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	EtiquetaPneuFiltro filtro) {
		ReportTemplateBean bean = getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.EMITIR_FICHA_COLETA);
		if(request.getParameter("templateSelecionado") != null && !request.getParameter("templateSelecionado").equals("")){
			bean  = getReportTemplateService().loadTemplateByCategoriaAndNome(EnumCategoriaReportTemplate.EMITIR_FICHA_COLETA,request.getParameter("templateSelecionado"));
		}
		return bean;
	}
	
	public List<ReportTemplateBean> buscarlisaTemplates() throws Exception {
		return getReportTemplateService().loadTemplatesByCategoriaOrderByNewest(EnumCategoriaReportTemplate.EMITIR_FICHA_COLETA);
	}
	
	@Override
	public ModelAndView doGerar(WebRequestContext request, EtiquetaPneuFiltro filtro) throws Exception {
		try{
			ReportTemplateBean bean = this.setTemplate(request, filtro);
			if(EnumTipoEmissao.W3ERP.equals(bean.getTipoEmissao())){
				try{
					Resource recurso = getResource(request, filtro, "application/remote_printing", "w3erp");
					if (recurso == null) {
						return goToAction(FILTRO);
					}
					
					HttpServletResponse response = request.getServletResponse();
					response.setContentType(recurso.getContentType());
					response.addHeader("Content-Disposition", "attachment; filename=\"" + recurso.getFileName() +"\";");
					response.getOutputStream().write(recurso.getContents());
					
				} catch (Exception e) {
					throw new Exception(e.getMessage(),e.getCause());
				}			
				return null;
			}else {
				return super.doGerar(request, filtro);
			}
		
		}catch (NullPointerException e) {
			request.addError("Template n�o encontrado");
			return doFiltro(request, filtro);
		}catch (Exception e) {
			request.addError(e.getMessage()+" " +e.getCause());
			return doFiltro(request, filtro);
		}
		
	} 
	@Override
	public ModelAndView doFiltro(WebRequestContext request, EtiquetaPneuFiltro filtro) throws Exception {
		if(StringUtils.isNotBlank(request.getParameter("whereInPedidoVenda"))){
			String whereIn = request.getParameter("whereInPedidoVenda");
			if(pedidovendaService.isOtr(Integer.parseInt(whereIn.split(",")[0]))){
				return new ModelAndView("redirect:/faturamento/crud/PedidovendaOtr");
			}
			return new ModelAndView("redirect:/faturamento/crud/Pedidovenda");
		}else if(StringUtils.isNotBlank(request.getParameter("whereInVenda"))){
			String whereIn = request.getParameter("whereInVenda");
			if(vendaService.isOtr(Integer.parseInt(whereIn.split(",")[0]))){
				return new ModelAndView("redirect:/faturamento/crud/VendaOtr");
			}
			return new ModelAndView("redirect:/faturamento/crud/Venda");
		}else { 
			return new ModelAndView("redirect:/faturamento/crud/Coleta");
		}
	}
	
	@Override
	protected LinkedHashMap<String, Object> carregaDados(WebRequestContext request, EtiquetaPneuFiltro filtro) throws Exception {
		LinkedHashMap<String, Object> mapa =  new LinkedHashMap<String, Object>();
		String whereIn = "";
		StringBuilder whereInPneuSelecionado = new StringBuilder();
		if(StringUtils.isNotBlank(request.getParameter("whereInPedidoVenda"))){
			String whereInPedidoVenda = request.getParameter("whereInPedidoVenda");
			String whereInPedidoVendaMaterial = request.getParameter("whereInPedidoVendaMaterial");
			if(pedidovendaService.havePedidovendaSituacao(whereInPedidoVenda, Pedidovendasituacao.CANCELADO, Pedidovendasituacao.AGUARDANDO_APROVACAO)){
				throw new GeradorRelatorioException("N�o � permitido emitir Ficha/Etiqueta de pedido de venda cancelado ou aguardando aprova��o.");
			}
			
			List<Coleta> listaColeta = coletaService.findByWhereInPedidovenda(whereInPedidoVenda,whereInPedidoVendaMaterial);
			if(SinedUtil.isListEmpty(listaColeta)){
				throw new GeradorRelatorioException("N�o � permitido emitir Ficha/Etiqueta de Pneu de venda que n�o possui coleta vinculada.");
			}
			whereIn = CollectionsUtil.listAndConcatenate(listaColeta, "cdcoleta", ",");
			for(Coleta coleta : listaColeta){
				for(ColetaMaterial cm: coleta.getListaColetaMaterial()){
					if(SinedUtil.isListNotEmpty(cm.getListaColetaMaterialPedidovendamaterial())){
						for(ColetaMaterialPedidovendamaterial cmpvm : cm.getListaColetaMaterialPedidovendamaterial()){
							if(Util.objects.isPersistent(cmpvm.getPedidovendamaterial())){
								whereInPneuSelecionado.append(Integer.toString(cmpvm.getPedidovendamaterial().getCdpedidovendamaterial())).append(",");
							}
						}
					}
					if(Util.objects.isPersistent(cm.getPedidovendamaterial())){
						whereInPneuSelecionado.append(Integer.toString(cm.getPedidovendamaterial().getCdpedidovendamaterial())).append(",");
					}
				}
			}
			pedidovendahistoricoService.makeHistoricoFichaColetaEmitido(whereInPedidoVenda);
		}else if(StringUtils.isNotBlank(request.getParameter("whereInVenda"))){
			String whereInVenda = request.getParameter("whereInVenda");
			String whereInVendaMaterial = request.getParameter("whereInVendaMaterial");
			if(vendaService.haveVendaSituacao(whereInVenda, Boolean.FALSE, Vendasituacao.CANCELADA, Vendasituacao.AGUARDANDO_APROVACAO)){
				throw new GeradorRelatorioException("N�o � permitido emitir Ficha/Etiqueta de venda cancelada ou aguardando aprova��o.");
			}
			
			List<Coleta> listaColeta = coletaService.findByWhereInVenda(whereInVenda,whereInVendaMaterial);
			if(SinedUtil.isListEmpty(listaColeta)){
				throw new GeradorRelatorioException("N�o � permitido emitir Ficha/Etiqueta de Pneu de venda que n�o possui coleta vinculada.");
			}
			whereIn = CollectionsUtil.listAndConcatenate(listaColeta, "cdcoleta", ",");
			for(Coleta coleta : listaColeta){
				for(ColetaMaterial cm: coleta.getListaColetaMaterial()){
					if(Util.objects.isPersistent(cm.getPedidovendamaterial())){
						whereInPneuSelecionado.append(Integer.toString(cm.getPedidovendamaterial().getCdpedidovendamaterial())).append(",");
					}
				}
			}
			vendahistoricoService.makeHistoricoFichaColetaEmitido(whereInVenda);
		}else {
			whereIn = SinedUtil.getItensSelecionados(request);
		}
		mapa.put("lista", coletaService.gerarListaDadosForEmitirFichaColeta(whereIn, whereInPneuSelecionado.length() > 0 ? whereInPneuSelecionado.substring(0, whereInPneuSelecionado.length()-1) : null));
		return mapa;
	}
}
