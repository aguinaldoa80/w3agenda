package br.com.linkcom.sined.modulo.faturamento.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ContratoFiltro;

@Controller(path = "/faturamento/relatorio/ContratoCSV",	authorizationModule=ReportAuthorizationModule.class)
public class ContratoCSVReport extends ResourceSenderController<ContratoFiltro> {
	
	private ContratoService contratoService;
	
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request, ContratoFiltro filtro) throws Exception {
		return contratoService.gerarRelarorioCSVListagem(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request,	ContratoFiltro filtro) throws Exception {
		return null;
	}
	
	
}
