package br.com.linkcom.sined.modulo.faturamento.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EvolucaoFaturamentoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/faturamento/relatorio/EvolucaoFaturamento",	authorizationModule=ReportAuthorizationModule.class)
public class EvolucaoFaturamentoReport extends SinedReport<EvolucaoFaturamentoFiltro> {
	
	private NotaService notaService;
	
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	
	@Override
	public IReport createReportSined(WebRequestContext request, EvolucaoFaturamentoFiltro filtro) throws Exception {
		return notaService.gerarRelarorioEvolucaoFaturamento(filtro);
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, EvolucaoFaturamentoFiltro filtro) {
		String empresa = getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa());
		report.addParameter("EMPRESAFILTRO", empresa == null || empresa.equals("") ? "Todas" : empresa);
		report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
		
		super.adicionaParametrosFiltro(report, filtro);
	}
	
	@Override
	public String getTitulo(EvolucaoFaturamentoFiltro filtro) {
		return "EVOLU��O DO FATURAMENTO";
	}
	
	@Override
	public String getNomeArquivo() {
		return "evolucaofat";
	}
}
