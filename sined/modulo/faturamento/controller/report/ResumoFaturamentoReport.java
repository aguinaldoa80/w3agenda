package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/faturamento/relatorio/resumoFaturamento",	authorizationModule=ReportAuthorizationModule.class)
public class ResumoFaturamentoReport extends SinedReport<NotaFiltro> {
	
    private NotaService notaService;
	
	public void setNotaService(NotaService notaService) {
		this.notaService = notaService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, NotaFiltro filtro) throws Exception {
		
		boolean isNfe = "true".equals(request.getParameter("isNfe"));
		List<NotaTipo> listaTipo = new ArrayList<NotaTipo>();
		if("SERVICO".equals(request.getParameter("tipo"))){
			listaTipo.add(NotaTipo.NOTA_FISCAL_SERVICO);
			filtro.setListaNotaTipo(listaTipo);
		} else if("PRODUTO".equals(request.getParameter("tipo"))){ 
			if(isNfe){
				listaTipo.add(NotaTipo.NOTA_FISCAL_PRODUTO);
			} else {
				listaTipo.add(NotaTipo.NOTA_FISCAL_CONSUMIDOR);
			}
			filtro.setListaNotaTipo(listaTipo);
		} 
		if(isNfe){
			filtro.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFE);
		} else {
			filtro.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFCE);
		}
		
		
		Report report = notaService.gerarRelarorioResumoFaturamento(filtro);
		report.addParameter("notaTipo", CollectionsUtil.listAndConcatenate(listaTipo, "nome", ", "));
		return report;
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, NotaFiltro filtro) {
		report.addParameter("numeroNota", StringUtils.trimToNull(filtro.getNumNota()));
		report.addParameter("emissao", SinedUtil.getDescricaoPeriodo(filtro.getDtEmissaoInicio(), filtro.getDtEmissaoFim()));
		report.addParameter("projeto", filtro.getProjeto() != null ? getProjetoService().load(filtro.getProjeto(), "projeto.nome").getNome() : null);
		report.addParameter("destinatario", filtro.getCliente() != null ? getClienteService().load(filtro.getCliente(), "cliente.nome").getNome() : null);
		report.addParameter("situacao", NotaStatus.getDescriptionProperties(filtro.getListaNotaStatus()));
		
		super.adicionaParametrosFiltro(report, filtro);
	}
	
	@Override
	public String getTitulo(NotaFiltro filtro) {
		return "RELATÓRIO DE RESUMO DE FATURAMENTO";
	}
	
	@Override
	public String getNomeArquivo() {
		return "resumofaturamento";
	}
}
