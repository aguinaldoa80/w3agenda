package br.com.linkcom.sined.modulo.faturamento.controller.report;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.service.ComprovanteConfiguravelService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendahistoricoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.PedidovendaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path="/faturamento/relatorio/Pedidovenda", authorizationModule=ReportAuthorizationModule.class)
public class ComprovantepedidovendaReport extends SinedReport<PedidovendaFiltro> {

	private PedidovendaService pedidovendaService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private ComprovanteConfiguravelService comprovanteConfiguravelService;

	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}	
	public void setPedidovendahistoricoService(PedidovendahistoricoService pedidovendahistoricoService) {this.pedidovendahistoricoService = pedidovendahistoricoService;}	
	public void setComprovanteConfiguravelService(ComprovanteConfiguravelService comprovanteConfiguravelService) {this.comprovanteConfiguravelService = comprovanteConfiguravelService;}

	@Override
	public IReport createReportSined(WebRequestContext request, PedidovendaFiltro filtro) throws Exception {
		return pedidovendaService.gerarComprovante(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "comprovantepedidovenda";
	}

	@Override
	public String getTitulo(PedidovendaFiltro filtro) {
		return "Pedido de Venda";
	}
	
	@Override
	public ModelAndView doGerar(WebRequestContext request, PedidovendaFiltro filtro) throws Exception {
		String whereIn = filtro.getWhereIn();
		
		if(whereIn == null) {
			whereIn = filtro.getCdpedidovenda().toString();
		}
		
		Pedidovendasituacao [] situacoes = { Pedidovendasituacao.AGUARDANDO_APROVACAO };
		
		if(!parametrogeralService.getBoolean("EMITIR_COMPROVANTE_AGUARDANDOAPROVACAO") && pedidovendaService.havePedidovendaSituacao(whereIn, situacoes)) {
			request.addError("O envio e emiss�o do comprovante n�o s�o permitidos na situa��o de Aguardando Aprova��o.");
			String urlRedirecionamento = Boolean.TRUE.equals(filtro.getOrigemOtr())? "redirect:/faturamento/crud/PedidovendaOtr": "redirect:/faturamento/crud/Pedidovenda";
			return new ModelAndView(urlRedirecionamento);
		}
		
		Pedidovenda pedidovenda = pedidovendaService.loadForEntrada(new Pedidovenda(Integer.parseInt(whereIn.split(",")[0])));
		ComprovanteConfiguravel comprovante = null;
		
		if(StringUtils.isNotBlank(request.getParameter("cdcomprovanteconfiguravel"))){
			comprovante = comprovanteConfiguravelService.load(new ComprovanteConfiguravel(Integer.parseInt(request.getParameter("cdcomprovanteconfiguravel"))));
		}else {
			comprovante = empresaService.findComprovaConfiguravelByTipoAndEmpresa(ComprovanteConfiguravel.TipoComprovante.PEDIDO_VENDA, pedidovenda.getEmpresa());
		}
		
		ModelAndView retorno;
		if( comprovante != null && comprovante.getLayout()!=null){
			retorno = pedidovendaService.gerarComprovanteConfiguravel(request, filtro.getCdpedidovenda(), comprovante);
		}else{
			retorno = super.doGerar(request, filtro);
		}
		pedidovendahistoricoService.makeHistoricoComprovanteEmitido(pedidovenda);
		return retorno;
	}

}
