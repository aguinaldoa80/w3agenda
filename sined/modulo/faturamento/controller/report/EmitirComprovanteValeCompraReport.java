package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.LinkedHashMap;
import java.util.List;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.ClienteEmpresa;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.ValecompraService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirComprovanteValeCompraBean;

@Controller(path="/crm/relatorio/EmitirComprovanteValeCompra", authorizationModule=ReportAuthorizationModule.class)
public class EmitirComprovanteValeCompraReport extends ReportTemplateController<ReportTemplateFiltro>{

	private ValecompraService valecompraService;
	
	public void setValecompraService(ValecompraService valecompraService) {
		this.valecompraService = valecompraService;
	}
	
	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.EMITIR_COMPROVANTE_VALE_COMPRA;
	}

	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	ReportTemplateFiltro filtro) {
		if(filtro != null && filtro.getTemplate() != null && filtro.getTemplate().getCdreporttemplate() != null){
			return getReportTemplateService().load(filtro.getTemplate());
		}
		
		return getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.EMITIR_COMPROVANTE_VALE_COMPRA);
	}

	@Override
	protected LinkedHashMap<String, Object> carregaDados(
			WebRequestContext request, ReportTemplateFiltro filtro)
			throws Exception {
			LinkedHashMap<String,Object> datasource = new LinkedHashMap<String, Object>();
			Cliente cliente = new Cliente();
			cliente.setCdpessoa(Integer.parseInt(request.getParameter("selectedItens")));
			cliente = clienteService.findForValeCompra(cliente);
			cliente.setSaldoValecompra(valecompraService.getSaldoByCliente(cliente));
			
			List<ClienteEmpresa> listaEmpresa = cliente.getListaClienteEmpresa();
			Empresa empresa = new Empresa();
			if (listaEmpresa == null || listaEmpresa.size() > 1) {
				empresa = empresaService.loadPrincipalEmitirValeCompra();
			}else {				
				empresa = listaEmpresa.get(0).getEmpresa();																
			}
						
			EmitirComprovanteValeCompraBean emitirValeCompraBean = new EmitirComprovanteValeCompraBean(cliente, empresa);
			datasource.put("bean", emitirValeCompraBean);
			return datasource;
	}
	
	@Override
	protected String getFileName() {
		return "comprovante_vale_compra";
	}
}
