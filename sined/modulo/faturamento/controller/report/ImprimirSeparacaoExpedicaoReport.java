package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.Expedicaohistorico;
import br.com.linkcom.sined.geral.bean.Regiao;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaoacao;
import br.com.linkcom.sined.geral.service.ExpedicaoService;
import br.com.linkcom.sined.geral.service.ExpedicaohistoricoService;
import br.com.linkcom.sined.geral.service.RegiaoService;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;
import br.com.linkcom.sined.util.report.MergeReport;

@Controller(path = "/faturamento/relatorio/ImprimirSeparacaoExpedicao", authorizationModule=ReportAuthorizationModule.class)
public class ImprimirSeparacaoExpedicaoReport extends ResourceSenderController<FiltroListagemSined>{

	private ExpedicaoService expedicaoService;
	private ExpedicaohistoricoService expedicaohistoricoService;
	private RegiaoService regiaoService;

	public void setExpedicaoService(ExpedicaoService expedicaoService) {this.expedicaoService = expedicaoService;}
	public void setExpedicaohistoricoService(ExpedicaohistoricoService expedicaohistoricoService) {this.expedicaohistoricoService = expedicaohistoricoService;}
	public void setRegiaoService(RegiaoService regiaoService) {this.regiaoService = regiaoService;}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		return new ModelAndView("redirect:/faturamento/crud/Expedicao" + (request.getParameter("ENTRADA") != null &&  request.getParameter("ENTRADA").equals("true")? "?ACAO=consultar&cdexpedicao=" + request.getParameter("selectedItens") : ""));
	}

	@Override
	public Resource generateResource(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		String whereIn = request.getParameter("whereIn");
		List<Expedicao> listaExpedicao = expedicaoService.findForImprimirSeparacao(whereIn);
		
		String fileName = "expedicao_separacao_" + SinedUtil.datePatternForReport() + ".pdf";
		MergeReport mergeReport = new MergeReport(fileName);
		if(SinedUtil.isListNotEmpty(listaExpedicao)){
			List<Expedicao> listaExpedicaoForHistorico = new ArrayList<Expedicao>();
			for(Expedicao expedicao : listaExpedicao){
				if(!listaExpedicaoForHistorico.contains(expedicao)){
					listaExpedicaoForHistorico.add(expedicao);
				}
				mergeReport.addReport(criaReport(expedicao));
			}
			
			for(Expedicao expedicao : listaExpedicaoForHistorico){
				Expedicaohistorico expedicaohistorico = new Expedicaohistorico();
				expedicaohistorico.setExpedicaoacao(Expedicaoacao.RELATORIO_IMPRESSO);
				expedicaohistorico.setExpedicao(expedicao);
				expedicaohistoricoService.saveOrUpdate(expedicaohistorico);
			}
		}

		return mergeReport.generateResource();
	}

	private Report criaReport(Expedicao expedicao){
		Venda venda = expedicaoService.getVendaForReport(expedicao);
//		Report report = new Report("/faturamento/imprimirExpedicaoSeparacao");
		Report report = new Report("/faturamento/imprimirExpedicaoSeparacaoPaisagem");
		
		report.addParameter("TITULO", "Relat�rio de Separa��o");
		report.addParameter("EMPRESA", (expedicao.getEmpresa()!=null ? expedicao.getEmpresa().getNome() : ""));
		report.addParameter("CDEXPEDICAO", expedicao.getCdexpedicao());
		report.addParameter("OBSERVACAO", expedicao.getObservacao() !=null ? expedicao.getObservacao() : "");
		report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		report.addParameter("DATA", new Date(System.currentTimeMillis()));
		
		if(venda != null){
			report.addParameter("CDVENDA", venda.getIdentificadorOrCdvenda());
			report.addParameter("DTVENDA", venda.getDtvenda() != null ? SinedDateUtils.toString(venda.getDtvenda()) : "");
			report.addParameter("VENDEDOR", venda.getColaborador() != null ? venda.getColaborador().getNome() : "");
			report.addParameter("RAZAOSOCIAL", venda.getCliente() != null && venda.getCliente().getRazaosocial() != null ? venda.getCliente().getRazaosocial() : "");
			report.addParameter("NOMEFANTASIA", venda.getCliente() !=null && venda.getCliente().getNome() != null ? venda.getCliente().getNome() : "");
			
			Regiao regiacao = venda.getEndereco() != null ? regiaoService.findByCep(venda.getEndereco().getCep()) : null;
			
			report.addParameter("ENDERECO", venda.getEndereco() != null ? venda.getEndereco().getLogradouroCompleto() : "");
			report.addParameter("MUNICIPIO", venda.getEndereco() != null && venda.getEndereco().getMunicipio() != null ? venda.getEndereco().getMunicipio().getNome() : "");
			report.addParameter("ESTADO", venda.getEndereco() != null && venda.getEndereco().getMunicipio() != null && venda.getEndereco().getMunicipio().getUf() != null ? venda.getEndereco().getMunicipio().getUf().getSigla() : "");
			report.addParameter("CEP", venda.getEndereco() != null && venda.getEndereco().getCep() != null ? venda.getEndereco().getCep().toString() : "");
			report.addParameter("REGIAO", regiacao != null ? regiacao.getNome() : "");
			report.setDataSource(expedicao.getListaExpedicaoitem());
		}
		
		return report;
	}
}
