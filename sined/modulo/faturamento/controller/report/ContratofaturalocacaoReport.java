package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.geradorrelatorio.util.pdf.WKHTMLConfigEnum;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.service.ContratofaturalocacaoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ContratofaturalocacaoFiltro;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path = "/faturamento/relatorio/Contratofaturalocacao")
public class ContratofaturalocacaoReport extends ReportTemplateController<ReportTemplateFiltro> {

	private ContratofaturalocacaoService contratofaturalocacaoService;
	
	public void setContratofaturalocacaoService(
			ContratofaturalocacaoService contratofaturalocacaoService) {
		this.contratofaturalocacaoService = contratofaturalocacaoService;
	}


	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.CONTRATO_FATURA_LOCACAO_LISTAGEM;
	}

	@Override
	protected LinkedHashMap<String, Object> carregaDados(WebRequestContext request, ReportTemplateFiltro filtro) throws Exception {	
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		map.put("lista", request.getAttribute("lista"));
		
		Empresa empresa = null;
		Object empresaFiltro = request.getAttribute("empresaFiltro");
		if(empresaFiltro != null){
			empresa = empresaService.loadComArquivo((Empresa)empresaFiltro);
		} else {
			empresa = empresaService.loadArquivoPrincipal();
		}
		map.put("logo", SinedUtil.getLogoURLForReport(empresa));
		map.put("empresa", empresa);
		
		return map;
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	ReportTemplateFiltro filtro) {
		return getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.CONTRATO_FATURA_LOCACAO_LISTAGEM);
	}

	@Override
	protected String getFileName() {
		return "contratofaturalocacao";
	}
	
	@Override
	protected WKHTMLConfigEnum getOrietation(ReportTemplateBean template) {
		return WKHTMLConfigEnum.ORIENTATION_LANDSCAPE;
	}
	
	public ModelAndView gerarRelatorioListagem(WebRequestContext request, ContratofaturalocacaoFiltro filtro){
		List<Contratofaturalocacao> lista = contratofaturalocacaoService.findForListagemCsvPdf(filtro);
		List<Rateioitem> listaRateioitem = contratofaturalocacaoService.getListaRateioitemByContratofaturalocacao(lista);
		contratofaturalocacaoService.makeCentrocustoValorVO(lista, listaRateioitem);
		
		request.setAttribute("lista", new LinkedList<Contratofaturalocacao>(lista));
		request.setAttribute("empresaFiltro", filtro.getEmpresa());
		return continueOnAction("gerar", new ReportTemplateFiltro());
	}

}
