package br.com.linkcom.sined.modulo.faturamento.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.enumeration.GerenciarVendaSituacao;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.RegiaoService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.VendaFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.SinedReport;

@Bean
@Controller(path="/faturamento/relatorio/GerenciarVenda", authorizationModule=ReportAuthorizationModule.class)
public class GerenciarVendaReport extends SinedReport<VendaFiltro>{
	
	private VendaService vendaService;
	private ColaboradorService colaboradorService;
	private MaterialService materialService;
	private RegiaoService regiaoService;
	
	public void setRegiaoService(RegiaoService regiaoService) {this.regiaoService = regiaoService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}

	@Override
	public IReport createReportSined(WebRequestContext request, VendaFiltro filtro) throws Exception {
		if(filtro.getRegiao() != null){
			filtro.setRegiao(regiaoService.loadFetch(filtro.getRegiao(), "listaRegiaolocal"));
		}
		//request.getServletResponse().addCookie(new Cookie("blockbutton", "false"));
		return vendaService.gerarRelatorioListagem(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "gerenciarvenda";
	}

	@Override
	public String getTitulo(VendaFiltro filtro) {
		return "GERENCIAR VENDA";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, VendaFiltro filtro) {
		
		report.addParameter("VENDEDOR", filtro.getColaborador() != null ? colaboradorService.load(filtro.getColaborador(), "colaborador.nome").getNome() : null);
		report.addParameter("EMPRESAFILTRO", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		report.addParameter("CODIGOVENDA", filtro.getWhereIncdvenda());
		String dataInicio = null;
		String dataFim = null;
		if (filtro.getDtinicio() != null){
			dataInicio = SinedDateUtils.toString(filtro.getDtinicio());
		}
		if (filtro.getDtfim() != null){
			dataFim = SinedDateUtils.toString(filtro.getDtfim());
		}
		report.addParameter("DTINICIO", dataInicio);
		report.addParameter("DTFIM", dataFim);
		report.addParameter("CLIENTE", filtro.getCliente() != null ? clienteService.load(filtro.getCliente(), "cliente.nome").getNome() : null);
		report.addParameter("PRODUTO", filtro.getMaterial() != null ? materialService.load(filtro.getMaterial(), "material.nome").getNome() : null);
		report.addParameter("SITUACOES", filtro.getListaVendasituacao() != null && filtro.getListaVendasituacao().size() > 0 ? GerenciarVendaSituacao.listAndConcatenateDescription(filtro.getListaVendasituacao()) : null);
	}
	
	public ModelAndView gerarExcel(WebRequestContext request, VendaFiltro filtro) throws Exception {
		if(filtro.getRegiao() != null){
			filtro.setRegiao(regiaoService.loadForFiltro(filtro.getRegiao()));
		}
		return new ResourceModelAndView(vendaService.gerarRelatorioListagemExcel(filtro));
	}
}