package br.com.linkcom.sined.modulo.faturamento.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.PedidovendaFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.SinedReport;

@Bean
@Controller(path="/faturamento/relatorio/GerenciarPedidovenda", authorizationModule=ReportAuthorizationModule.class)
public class GerenciarPedidovendaReport extends SinedReport<PedidovendaFiltro>{
	
	private PedidovendaService pedidovendaService;
	private ColaboradorService colaboradorService;
	private MaterialService materialService;
	
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, PedidovendaFiltro filtro) throws Exception {
		return pedidovendaService.gerarRelatorioListagem(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "gerenciarpedidovenda";
	}

	@Override
	public String getTitulo(PedidovendaFiltro filtro) {
		return "VISUALIZAR PEDIDO DE VENDA";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, PedidovendaFiltro filtro) {
		
		report.addParameter("VENDEDOR", filtro.getColaborador() != null ? colaboradorService.load(filtro.getColaborador(), "colaborador.nome").getNome() : null);
		report.addParameter("EMPRESAFILTRO", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		report.addParameter("CODIGO", filtro.getWhereIncdpedidovenda());
		String dataInicio = null;
		String dataFim = null;
		if (filtro.getDtinicio() != null){
			dataInicio = SinedDateUtils.toString(filtro.getDtinicio());
		}
		if (filtro.getDtfim() != null){
			dataFim = SinedDateUtils.toString(filtro.getDtfim());
		}
		report.addParameter("DTINICIO", dataInicio);
		report.addParameter("DTFIM", dataFim);
		report.addParameter("CLIENTE", filtro.getCliente() != null ? clienteService.load(filtro.getCliente(), "cliente.nome").getNome() : null);
		report.addParameter("PRODUTO", filtro.getMaterial() != null ? materialService.load(filtro.getMaterial(), "material.nome").getNome() : null);
		report.addParameter("SITUACOES", filtro.getListaPedidoVendasituacao() != null && filtro.getListaPedidoVendasituacao().size() > 0 ? Pedidovendasituacao.listAndConcatenateDescription(filtro.getListaPedidoVendasituacao()) : null);
	}
	
	public ModelAndView gerarCsv(WebRequestContext request, PedidovendaFiltro filtro) throws Exception {
		return new ResourceModelAndView(pedidovendaService.gerarRelatorioListagemCsv(filtro));
	}
	@Override
	public ModelAndView doGerar(WebRequestContext request,
			PedidovendaFiltro filtro) throws Exception {
		request.setAttribute("titulo", getTitulo(filtro));
		return super.doGerar(request, filtro);
	}
}