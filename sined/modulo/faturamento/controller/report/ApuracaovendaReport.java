package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.ApuracaovendaFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/faturamento/relatorio/Apuracaovenda", authorizationModule=ReportAuthorizationModule.class)
public class ApuracaovendaReport extends SinedReport<ApuracaovendaFiltro> {
	private VendaService vendaService;
	private ColaboradorService colaboradorService;
	private UsuarioService usuarioService;
	
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	
	@Override
	public IReport createReportSined(WebRequestContext request, ApuracaovendaFiltro filtro) throws Exception {
		return vendaService.gerarRelatorioApuracaovenda(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "apuracaovenda";
	}

	@Override
	public String getTitulo(ApuracaovendaFiltro filtro) {
		if(filtro.getTiporelatorio() != null && filtro.getTiporelatorio().equals("sintetico")){
			return "Relatório Sintético de Venda por Grupo";
		}
		return "Relatório Analítico de Venda por Grupo";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, ApuracaovendaFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);
		
		if(filtro.getEmpresa() != null){
			report.addParameter("NOMEEMPRESA", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}
		report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
	}
	
	public ModelAndView ajaxLoadComboVendedor(WebRequestContext request, Empresa empresa){
		boolean ativos = Boolean.parseBoolean(request.getParameter("ativos"));
		Boolean isRestricaoVendaVendedor = usuarioService.isRestricaoVendaVendedor(SinedUtil.getUsuarioLogado());		
		List<Colaborador> lista = colaboradorService.findColaboradoresComVenda(empresa, isRestricaoVendaVendedor ? SinedUtil.getUsuarioComoColaborador() : null, ativos);
				
		StringBuilder strOpt = new StringBuilder("<option value='<null>'></option>");
		for(Colaborador c : lista){
			strOpt.append("<option value='br.com.linkcom.sined.geral.bean.Colaborador[cdpessoa=")
				.append(c.getCdpessoa())
				.append("]'>")
				.append(c.getNome())
				.append("</option>");
		}
		
		return new JsonModelAndView().addObject("strHtml", strOpt.toString());
	}
}
