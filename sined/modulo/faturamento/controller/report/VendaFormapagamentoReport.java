package br.com.linkcom.sined.modulo.faturamento.controller.report;


import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.VendaFormapagamentoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/faturamento/relatorio/VendaFormapagamento", authorizationModule=ReportAuthorizationModule.class)
public class VendaFormapagamentoReport extends SinedReport<VendaFormapagamentoFiltro>{

	private VendaService vendaService;
	
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request, VendaFormapagamentoFiltro filtro) throws Exception {
		return vendaService.gerarRelatorioVendaFormapagamento(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "vendaformapagamento";
	}

	@Override
	public String getTitulo(VendaFormapagamentoFiltro filtro) {
		return "Vendas por Forma de Pagamento";
	}
		
}