package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoitemService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.DanfeBean;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path = "/faturamento/relatorio/DanfeConfiguravelCliente", authorizationModule=ReportAuthorizationModule.class)
public class DanfeConfiguravelReport extends ReportTemplateController<ReportTemplateFiltro>{
	
	private NotafiscalprodutoService notafiscalprodutoService;
	private NotafiscalprodutoitemService notafiscalprodutoitemService;
	
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setNotafiscalprodutoitemService(NotafiscalprodutoitemService notafiscalprodutoitemService) {this.notafiscalprodutoitemService = notafiscalprodutoitemService;}
	
	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.DANFE;
	}

	@Override
	protected String getFileName() {
		return "danfe_" + SinedUtil.datePatternForReport();
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	ReportTemplateFiltro filtro) {
		return getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.DANFE);
	}
	
	@Override
	protected LinkedHashMap<String, Object> carregaDados(WebRequestContext request, ReportTemplateFiltro filtro) throws Exception {
		
		List<Notafiscalproduto> listaNota = notafiscalprodutoService.findForDanfe(SinedUtil.getItensSelecionados(request));
		LinkedList<DanfeBean> listaDanfe = new LinkedList<DanfeBean>();
		
		if(listaNota!=null && !listaNota.isEmpty()){
			for(Notafiscalproduto nota : listaNota){
				notafiscalprodutoitemService.ordenaListaItens(nota);
				listaDanfe.add(notafiscalprodutoService.converterNotafiscalprodutoEmDanfeBean(nota));
			}
		}
		LinkedHashMap<String, Object> mapa =  new LinkedHashMap<String, Object>();
		mapa.put("lista", listaDanfe);
		return mapa;
	}

}
