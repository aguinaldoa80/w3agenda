package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

public class DanfeLegendaBean {
	
	private String codigo = "";
	private List<Image> image = new ArrayList<Image>();
	
	public String getCodigo() {
		return codigo;
	}
	
	public List<Image> getImage() {
		return image;
	}
	
	public void setImage(List<Image> image) {
		this.image = image;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

}
