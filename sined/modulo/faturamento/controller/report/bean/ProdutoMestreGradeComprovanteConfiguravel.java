package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

public class ProdutoMestreGradeComprovanteConfiguravel {
	
	private String CODIGO;
	private String DESCRICAO;
	private String IDENTIFICADORMATERIALMESTRE;
	
	
	public String getCODIGO() {
		return CODIGO;
	}
	public String getDESCRICAO() {
		return DESCRICAO;
	}
	public String getIDENTIFICADORMATERIALMESTRE() {
		return IDENTIFICADORMATERIALMESTRE;
	}
	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}
	public void setDESCRICAO(String dESCRICAO) {
		DESCRICAO = dESCRICAO;
	}
	public void setIDENTIFICADORMATERIALMESTRE(String iDENTIFICADORMATERIALMESTRE) {
		IDENTIFICADORMATERIALMESTRE = iDENTIFICADORMATERIALMESTRE;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((CODIGO == null) ? 0 : CODIGO.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProdutoMestreGradeComprovanteConfiguravel other = (ProdutoMestreGradeComprovanteConfiguravel) obj;
		if (CODIGO == null) {
			if (other.CODIGO != null)
				return false;
		} else if (!CODIGO.equals(other.CODIGO))
			return false;
		return true;
	}
	
	
}
