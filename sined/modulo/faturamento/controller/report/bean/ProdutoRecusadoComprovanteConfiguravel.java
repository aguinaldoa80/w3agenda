package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

public class ProdutoRecusadoComprovanteConfiguravel {
	
	private String CDMATERIAL;
	private String CODIGO;
	private String DESCRICAO;
	private String BANDACODIGO;
	private String BANDAIDENTIFICADOR;
	private String BANDADESCRICAO;
	private String UNIDADEMEDIDA;
	private String UNIDADEMEDIDASIMBOLO;
	private String PRECO;
	private Double QUANTIDADE;
	private String ENTREGA;
	private String TOTAL;
	private String OBSERVACAO;
	private String DESCONTO;
	private String MARCA;
	private String SERIE;
	private String DOT;
	private String MODELO;
	private String MEDIDA;
	private String NUMEROREFORMA;
	private String PNEUID;
	
	public String getCDMATERIAL() {
		return CDMATERIAL;
	}
	public void setCDMATERIAL(String cDMATERIAL) {
		CDMATERIAL = cDMATERIAL;
	}
	public String getCODIGO() {
		return CODIGO;
	}
	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}
	public String getDESCRICAO() {
		return DESCRICAO;
	}
	public void setDESCRICAO(String dESCRICAO) {
		DESCRICAO = dESCRICAO;
	}
	public String getBANDACODIGO() {
		return BANDACODIGO;
	}
	public void setBANDACODIGO(String bANDACODIGO) {
		BANDACODIGO = bANDACODIGO;
	}
	public String getBANDAIDENTIFICADOR() {
		return BANDAIDENTIFICADOR;
	}
	public void setBANDAIDENTIFICADOR(String bANDAIDENTIFICADOR) {
		BANDAIDENTIFICADOR = bANDAIDENTIFICADOR;
	}
	public String getBANDADESCRICAO() {
		return BANDADESCRICAO;
	}
	public void setBANDADESCRICAO(String bANDADESCRICAO) {
		BANDADESCRICAO = bANDADESCRICAO;
	}
	public String getUNIDADEMEDIDA() {
		return UNIDADEMEDIDA;
	}
	public void setUNIDADEMEDIDA(String uNIDADEMEDIDA) {
		UNIDADEMEDIDA = uNIDADEMEDIDA;
	}
	public String getUNIDADEMEDIDASIMBOLO() {
		return UNIDADEMEDIDASIMBOLO;
	}
	public void setUNIDADEMEDIDASIMBOLO(String uNIDADEMEDIDASIMBOLO) {
		UNIDADEMEDIDASIMBOLO = uNIDADEMEDIDASIMBOLO;
	}
	public String getPRECO() {
		return PRECO;
	}
	public void setPRECO(String pRECO) {
		PRECO = pRECO;
	}
	public Double getQUANTIDADE() {
		return QUANTIDADE;
	}
	public void setQUANTIDADE(Double qUANTIDADE) {
		QUANTIDADE = qUANTIDADE;
	}
	public String getENTREGA() {
		return ENTREGA;
	}
	public void setENTREGA(String eNTREGA) {
		ENTREGA = eNTREGA;
	}
	public String getTOTAL() {
		return TOTAL;
	}
	public void setTOTAL(String tOTAL) {
		TOTAL = tOTAL;
	}
	public String getOBSERVACAO() {
		return OBSERVACAO;
	}
	public void setOBSERVACAO(String oBSERVACAO) {
		OBSERVACAO = oBSERVACAO;
	}
	public String getDESCONTO() {
		return DESCONTO;
	}
	public void setDESCONTO(String dESCONTO) {
		DESCONTO = dESCONTO;
	}
	public String getMARCA() {
		return MARCA;
	}
	public void setMARCA(String mARCA) {
		MARCA = mARCA;
	}
	public String getSERIE() {
		return SERIE;
	}
	public void setSERIE(String sERIE) {
		SERIE = sERIE;
	}
	public String getDOT() {
		return DOT;
	}
	public void setDOT(String dOT) {
		DOT = dOT;
	}
	public String getMODELO() {
		return MODELO;
	}
	public void setMODELO(String mODELO) {
		MODELO = mODELO;
	}
	public String getMEDIDA() {
		return MEDIDA;
	}
	public void setMEDIDA(String mEDIDA) {
		MEDIDA = mEDIDA;
	}
	public String getNUMEROREFORMA() {
		return NUMEROREFORMA;
	}
	public void setNUMEROREFORMA(String nUMEROREFORMA) {
		NUMEROREFORMA = nUMEROREFORMA;
	}
	public String getPNEUID() {
		return PNEUID;
	}
	public void setPNEUID(String pNEUID) {
		PNEUID = pNEUID;
	}

}