package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.LinkedList;

public class DescricaoDeServicoBean {
	
	private String ordemServico;
	private String projetoSigla;
	private String projetoNome;
	private LinkedList<OrdemServicoCamposAdicionaisBean> camposAdicionais;
	private String projetoMunicipio;
	private String projetoUf;
	private String projetoEndereco;
	private String projetoUfSigla;
	private Double notaValorServicos;
	private Double notaBaseCalculoIR;
	private Double notaIndiceIR;
	private Double notaBaseCalculoISS;
	private Double notaIndiceISS;
	private Double notaBaseCalculoPIS;
	private Double notaIndicePIS;
	private Double notaBaseCalculoCofins;
	private Double notaIndiceCofins;
	private Double notaBaseCalculoCSLL;
	private Double notaIndiceCSLL;
	private Double notaBaseCalculoINSS;
	private Double notaIndiceINSS;
	private Double notaBaseCalculoICMS;
	private Double notaIndiceICMS;
	private String descricaoOS;
	private String projetoCEI;
	private LinkedList<OrdemServicoItemBean> listaOrdemServicoBean;
	private String vencimentoInformadoFaturar;
	private String itemListaServico;
	private String responsavelProjeto;
	private VendaBean vendaBean;
	
	public String getOrdemServico() {
		return ordemServico;
	}
	public String getProjetoSigla() {
		return projetoSigla;
	}
	public String getProjetoNome() {
		return projetoNome;
	}
	public LinkedList<OrdemServicoCamposAdicionaisBean> getCamposAdicionais() {
		return camposAdicionais;
	}
	public String getProjetoMunicipio() {
		return projetoMunicipio;
	}
	public String getProjetoUf() {
		return projetoUf;
	}
	public String getProjetoEndereco() {
		return projetoEndereco;
	}
	public String getProjetoUfSigla() {
		return projetoUfSigla;
	}
	public Double getNotaValorServicos() {
		return notaValorServicos;
	}
	public Double getNotaBaseCalculoIR() {
		return notaBaseCalculoIR;
	}
	public Double getNotaIndiceIR() {
		return notaIndiceIR;
	}
	public Double getNotaBaseCalculoISS() {
		return notaBaseCalculoISS;
	}
	public Double getNotaIndiceISS() {
		return notaIndiceISS;
	}
	public Double getNotaBaseCalculoPIS() {
		return notaBaseCalculoPIS;
	}
	public Double getNotaIndicePIS() {
		return notaIndicePIS;
	}
	public Double getNotaBaseCalculoCofins() {
		return notaBaseCalculoCofins;
	}
	public Double getNotaIndiceCofins() {
		return notaIndiceCofins;
	}
	public Double getNotaBaseCalculoCSLL() {
		return notaBaseCalculoCSLL;
	}
	public Double getNotaIndiceCSLL() {
		return notaIndiceCSLL;
	}
	public Double getNotaBaseCalculoINSS() {
		return notaBaseCalculoINSS;
	}
	public Double getNotaIndiceINSS() {
		return notaIndiceINSS;
	}
	public Double getNotaBaseCalculoICMS() {
		return notaBaseCalculoICMS;
	}
	public Double getNotaIndiceICMS() {
		return notaIndiceICMS;
	}
	public String getDescricaoOS() {
		return descricaoOS;
	}
	public String getProjetoCEI() {
		return projetoCEI;
	}
	public LinkedList<OrdemServicoItemBean> getListaOrdemServicoBean() {
		return listaOrdemServicoBean;
	}
	public String getVencimentoInformadoFaturar() {
		return vencimentoInformadoFaturar;
	}
	public String getItemListaServico() {
		return itemListaServico;
	}
	public String getResponsavelProjeto() {
		return responsavelProjeto;
	}
	public void setOrdemServico(String ordemServico) {
		this.ordemServico = ordemServico;
	}
	public void setProjetoSigla(String projetoSigla) {
		this.projetoSigla = projetoSigla;
	}
	public void setProjetoNome(String projetoNome) {
		this.projetoNome = projetoNome;
	}
	public void setCamposAdicionais(LinkedList<OrdemServicoCamposAdicionaisBean> camposAdicionais) {
		this.camposAdicionais = camposAdicionais;
	}
	public void setProjetoMunicipio(String projetoMunicipio) {
		this.projetoMunicipio = projetoMunicipio;
	}
	public void setProjetoUf(String projetoUf) {
		this.projetoUf = projetoUf;
	}
	public void setProjetoEndereco(String projetoEndereco) {
		this.projetoEndereco = projetoEndereco;
	}
	public void setProjetoUfSigla(String projetoUfSigla) {
		this.projetoUfSigla = projetoUfSigla;
	}
	public void setNotaValorServicos(Double notaValorServicos) {
		this.notaValorServicos = notaValorServicos;
	}
	public void setNotaBaseCalculoIR(Double notaBaseCalculoIR) {
		this.notaBaseCalculoIR = notaBaseCalculoIR;
	}
	public void setNotaIndiceIR(Double notaIndiceIR) {
		this.notaIndiceIR = notaIndiceIR;
	}
	public void setNotaBaseCalculoISS(Double notaBaseCalculoISS) {
		this.notaBaseCalculoISS = notaBaseCalculoISS;
	}
	public void setNotaIndiceISS(Double notaIndiceISS) {
		this.notaIndiceISS = notaIndiceISS;
	}
	public void setNotaBaseCalculoPIS(Double notaBaseCalculoPIS) {
		this.notaBaseCalculoPIS = notaBaseCalculoPIS;
	}
	public void setNotaIndicePIS(Double notaIndicePIS) {
		this.notaIndicePIS = notaIndicePIS;
	}
	public void setNotaBaseCalculoCofins(Double notaBaseCalculoCofins) {
		this.notaBaseCalculoCofins = notaBaseCalculoCofins;
	}
	public void setNotaIndiceCofins(Double notaIndiceCofins) {
		this.notaIndiceCofins = notaIndiceCofins;
	}
	public void setNotaBaseCalculoCSLL(Double notaBaseCalculoCSLL) {
		this.notaBaseCalculoCSLL = notaBaseCalculoCSLL;
	}
	public void setNotaIndiceCSLL(Double notaIndiceCSLL) {
		this.notaIndiceCSLL = notaIndiceCSLL;
	}
	public void setNotaBaseCalculoINSS(Double notaBaseCalculoINSS) {
		this.notaBaseCalculoINSS = notaBaseCalculoINSS;
	}
	public void setNotaIndiceINSS(Double notaIndiceINSS) {
		this.notaIndiceINSS = notaIndiceINSS;
	}
	public void setNotaBaseCalculoICMS(Double notaBaseCalculoICMS) {
		this.notaBaseCalculoICMS = notaBaseCalculoICMS;
	}
	public void setNotaIndiceICMS(Double notaIndiceICMS) {
		this.notaIndiceICMS = notaIndiceICMS;
	}
	public void setDescricaoOS(String descricaoOS) {
		this.descricaoOS = descricaoOS;
	}
	public void setProjetoCEI(String projetoCEI) {
		this.projetoCEI = projetoCEI;
	}
	public void setListaOrdemServicoBean(LinkedList<OrdemServicoItemBean> listaOrdemServicoBean) {
		this.listaOrdemServicoBean = listaOrdemServicoBean;
	}
	public void setVencimentoInformadoFaturar(String vencimentoInformadoFaturar) {
		this.vencimentoInformadoFaturar = vencimentoInformadoFaturar;
	}	
	public void setItemListaServico(String itemListaServico) {
		this.itemListaServico = itemListaServico;
	}
	public void setResponsavelProjeto(String responsavelProjeto) {
		this.responsavelProjeto = responsavelProjeto;
	}
	public VendaBean getVendaBean() {
		return vendaBean;
	}
	public void setVendaBean(VendaBean vendaBean) {
		this.vendaBean = vendaBean;
	}
}
