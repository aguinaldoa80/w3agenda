package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import br.com.linkcom.sined.geral.bean.Material;

public class ProdutokititemComprovanteConfiguravel {
	
	private String DESCRICAO;
	private Double PRECO;
	private Double QUANTIDADE;
	private Double TOTAL;
	
	private Material material;

	public String getDESCRICAO() {
		return DESCRICAO;
	}
	public Double getPRECO() {
		return PRECO;
	}
	public Double getQUANTIDADE() {
		return QUANTIDADE;
	}
	public Double getTOTAL() {
		return TOTAL;
	}
	public Material getMaterial() {
		return material;
	}

	public void setDESCRICAO(String dESCRICAO) {
		DESCRICAO = dESCRICAO;
	}
	public void setPRECO(Double pRECO) {
		PRECO = pRECO;
	}
	public void setQUANTIDADE(Double qUANTIDADE) {
		QUANTIDADE = qUANTIDADE;
	}
	public void setTOTAL(Double tOTAL) {
		TOTAL = tOTAL;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
}