package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

public class EmitirComprovanteTEFPagamentoBean {

	private String formapagamento;
	private Double valor;
	
	public String getFormapagamento() {
		return formapagamento;
	}
	public Double getValor() {
		return valor;
	}
	public void setFormapagamento(String formapagamento) {
		this.formapagamento = formapagamento;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	
}
