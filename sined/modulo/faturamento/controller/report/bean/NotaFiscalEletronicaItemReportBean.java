package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

public class NotaFiscalEletronicaItemReportBean {
	
	private Double quantidade;
	private String descricao;
	private Double aliquota;
	private Double valorunitario;
	private Double valortotal;
	private Double desconto;
	
	public NotaFiscalEletronicaItemReportBean(Double quantidade,
			String descricao, Double aliquota, Double valorunitario,
			Double valortotal, Double desconto) {
		super();
		this.quantidade = quantidade;
		this.descricao = descricao;
		this.aliquota = aliquota != null? aliquota: 0d;
		this.valorunitario = valorunitario;
		this.valortotal = valortotal;
		this.desconto = desconto;
	}
	
	public Double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Double getAliquota() {
		return aliquota;
	}
	public void setAliquota(Double aliquota) {
		this.aliquota = aliquota;
	}
	public Double getValorunitario() {
		return valorunitario;
	}
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
	public Double getValortotal() {
		return valortotal;
	}
	public void setValortotal(Double valortotal) {
		this.valortotal = valortotal;
	}
	public Double getDesconto() {
		return desconto;
	}
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	
}
