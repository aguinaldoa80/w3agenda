package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import br.com.linkcom.neo.types.Money;

public class RelatorioMargemContribuicaoNotaVendaItemBean {

	private String nome;
	private Money valorBruto;
	private Money desconto;
	private Money imposto;
	private Money taxaFinanceira;
	private Money cmv;
	private Money comissao;
	private Money frete;
	private Money margem;
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public Money getValorBruto() {
		return valorBruto;
	}

	public void setValorBruto(Money valorBruto) {
		this.valorBruto = valorBruto;
	}

	public Money getDesconto() {
		return desconto;
	}

	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}

	public Money getImposto() {
		return imposto;
	}

	public void setImposto(Money imposto) {
		this.imposto = imposto;
	}

	public Money getTaxaFinanceira() {
		return taxaFinanceira;
	}

	public void setTaxaFinanceira(Money taxaFinanceira) {
		this.taxaFinanceira = taxaFinanceira;
	}

	public Money getCmv() {
		return cmv;
	}

	public void setCmv(Money cmv) {
		this.cmv = cmv;
	}

	public Money getComissao() {
		return comissao;
	}

	public void setComissao(Money comissao) {
		this.comissao = comissao;
	}

	public Money getFrete() {
		return frete;
	}

	public void setFrete(Money frete) {
		this.frete = frete;
	}

	public Money getMargem() {
		return margem;
	}

	public void setMargem(Money margem) {
		this.margem = margem;
	}
	
	public void addValorBruto(Money valorBruto) {
		if (this.valorBruto == null) {
			this.valorBruto = new Money();
		}
		
		if (valorBruto == null) {
			valorBruto = new Money();
		}
		
		this.valorBruto = this.valorBruto.add(valorBruto);
	}
	
	public void addDesconto(Money desconto) {
		if (this.desconto == null) {
			this.desconto = new Money();
		}
		
		if (desconto == null) {
			desconto = new Money();
		}
		
		this.desconto = this.desconto.add(desconto);
	}
	
	public void addImposto(Money imposto) {
		if (this.imposto == null) {
			this.imposto = new Money();
		}
		
		if (imposto == null) {
			imposto = new Money();
		}
		
		this.imposto = this.imposto.add(imposto);
	}
	
	public void addTaxaFinanceira(Money taxaFinanceira) {
		if (this.taxaFinanceira == null) {
			this.taxaFinanceira = new Money();
		}
		
		if (taxaFinanceira == null) {
			taxaFinanceira = new Money();
		}
		
		this.taxaFinanceira = this.taxaFinanceira.add(taxaFinanceira);
	}
	
	public void addCmv(Money cmv) {
		if (this.cmv == null) {
			this.cmv = new Money();
		}
		
		if (cmv == null) {
			cmv = new Money();
		}
		
		this.cmv = this.cmv.add(cmv);
	}
	
	public void addComissao(Money comissao) {
		if (this.comissao == null) {
			this.comissao = new Money();
		}
		
		if (comissao == null) {
			comissao = new Money();
		}
		
		this.comissao = this.comissao.add(comissao);
	}
	
	public void addFrete(Money frete) {
		if (this.frete == null) {
			this.frete = new Money();
		}
		
		if (frete == null) {
			frete = new Money();
		}
		
		this.frete = this.frete.add(frete);
	}
	
	public void addMargem(Money margem) {
		if (this.margem == null) {
			this.margem = new Money();
		}
		
		if (margem == null) {
			margem = new Money();
		}
		
		this.margem = this.margem.add(margem);
	}
}
