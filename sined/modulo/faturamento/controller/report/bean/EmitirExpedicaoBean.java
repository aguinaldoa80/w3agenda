package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.sql.Date;
import java.util.LinkedList;

public class EmitirExpedicaoBean {
	
	private Integer id;
	private Date data;
	private String transportadora;
	private String observacao;
	private LinkedList<EmitirExpedicaoClienteBean> listaCliente;
	
	private Double qtde_total_vendida;
	private Double qtde_total_expedicao;
	private Double peso_total_bruto;
	
	public Integer getId() {
		return id;
	}
	public Date getData() {
		return data;
	}
	public String getTransportadora() {
		return transportadora;
	}
	public String getObservacao() {
		return observacao;
	}
	public LinkedList<EmitirExpedicaoClienteBean> getListaCliente() {
		return listaCliente;
	}
	public void setListaCliente(
			LinkedList<EmitirExpedicaoClienteBean> listaCliente) {
		this.listaCliente = listaCliente;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setTransportadora(String transportadora) {
		this.transportadora = transportadora;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public Double getQtde_total_vendida() {
		return qtde_total_vendida;
	}
	public Double getQtde_total_expedicao() {
		return qtde_total_expedicao;
	}
	public Double getPeso_total_bruto() {
		return peso_total_bruto;
	}
	public void setQtde_total_vendida(Double qtdeTotalVendida) {
		qtde_total_vendida = qtdeTotalVendida;
	}
	public void setQtde_total_expedicao(Double qtdeTotalExpedicao) {
		qtde_total_expedicao = qtdeTotalExpedicao;
	}
	public void setPeso_total_bruto(Double pesoTotalBruto) {
		peso_total_bruto = pesoTotalBruto;
	}
}
