package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.LinkedList;

public class EmitirEtiquetaDestinatarioRemetenteBean {

	private LinkedList<EmitirEtiquetaDestinatarioBean> listaDestinatarios;
	private LinkedList<EmitirEtiquetaRemetenteBean> listaRemetentes;
	
	public EmitirEtiquetaDestinatarioRemetenteBean(){
		listaDestinatarios = new LinkedList<EmitirEtiquetaDestinatarioBean>();
		listaRemetentes = new LinkedList<EmitirEtiquetaRemetenteBean>();
	}
	
	public LinkedList<EmitirEtiquetaDestinatarioBean> getListaDestinatarios() {
		return listaDestinatarios;
	}
	public void setListaDestinatarios(
			LinkedList<EmitirEtiquetaDestinatarioBean> listaDestinatarios) {
		this.listaDestinatarios = listaDestinatarios;
	}
	
	public LinkedList<EmitirEtiquetaRemetenteBean> getListaRemetentes() {
		return listaRemetentes;
	}
	public void setListaRemetentes(
			LinkedList<EmitirEtiquetaRemetenteBean> listaRemetentes) {
		this.listaRemetentes = listaRemetentes;
	}
}
