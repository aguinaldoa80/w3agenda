package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.ArrayList;
import java.util.List;

public class ProdutoagrupadoComprovanteConfiguravel {
	
	
	private String CODIGO;
	private String DESCRICAO;
	private String CDMATERIALGRUPO;
	private String NCMCOMPLETO;
	private String IMAGEM;
	private String OBSERVACAO;
	private String CARACTERISTICA;
	
	private List<ProdutoagrupadoitemComprovanteConfiguravel> LISTAPRODUTOAGRUPADOITEM = new ArrayList<ProdutoagrupadoitemComprovanteConfiguravel>();

	
	public String getCODIGO() {
		return CODIGO;
	}
	public String getDESCRICAO() {
		return DESCRICAO;
	}
	public String getCDMATERIALGRUPO() {
		return CDMATERIALGRUPO;
	}
	public String getNCMCOMPLETO() {
		return NCMCOMPLETO;
	}
	public String getIMAGEM() {
		return IMAGEM;
	}
	public String getOBSERVACAO() {
		return OBSERVACAO;
	}
	public List<ProdutoagrupadoitemComprovanteConfiguravel> getLISTAPRODUTOAGRUPADOITEM() {
		return LISTAPRODUTOAGRUPADOITEM;
	}
	
	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}
	public void setDESCRICAO(String dESCRICAO) {
		DESCRICAO = dESCRICAO;
	}
	public void setCDMATERIALGRUPO(String cDMATERIALGRUPO) {
		CDMATERIALGRUPO = cDMATERIALGRUPO;
	}
	public void setNCMCOMPLETO(String nCMCOMPLETO) {
		NCMCOMPLETO = nCMCOMPLETO;
	}
	public void setIMAGEM(String iMAGEM) {
		IMAGEM = iMAGEM;
	}
	public void setOBSERVACAO(String oBSERVACAO) {
		OBSERVACAO = oBSERVACAO;
	}
	public void setLISTAPRODUTOAGRUPADOITEM(List<ProdutoagrupadoitemComprovanteConfiguravel> lISTAPRODUTOAGRUPADOITEM) {
		LISTAPRODUTOAGRUPADOITEM = lISTAPRODUTOAGRUPADOITEM;
	}
	public String getCARACTERISTICA() {
		return CARACTERISTICA;
	}
	public void setCARACTERISTICA(String cARACTERISTICA) {
		CARACTERISTICA = cARACTERISTICA;
	}
}