package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.LinkedList;

public class AvaliacaoBean {

	private Integer pontuacao;
	
	public Integer getPontuacao() {
		return pontuacao;
	}
	public LinkedList<AvaliacaoItemBean> getAvaliacaoItemBeans() {
		return avaliacaoItemBeans;
	}
	public void setPontuacao(Integer pontuacao) {
		this.pontuacao = pontuacao;
	}
	public void setAvaliacaoItemBeans(
			LinkedList<AvaliacaoItemBean> avaliacaoItemBeans) {
		this.avaliacaoItemBeans = avaliacaoItemBeans;
	}
	private LinkedList<AvaliacaoItemBean> avaliacaoItemBeans;
	
}
