package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.awt.Image;
import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.types.Money;

public class AcompanhamentometaAnaliticoBean {
	
	private String nome_vendedor;
	private String nome_meta;
	private Money meta;
	private Money metadiaria;
	private Money metapendente;
	@SuppressWarnings("unused")
	private Money valortotalvenda;
	
	private Date aux_dtreferencia;
	private Date aux_firstDateMonth;
	private Date aux_lastDateMonth;
	private Integer aux_diasuteis;
	private Integer aux_diascorridos;
	private Integer aux_diasrestantes;
	
	private Image grafico;
	
	private List<AcompanhamentometaAnaliticoItensBean> listaItens;
	
	public String getNome_vendedor() {
		return nome_vendedor;
	}
	public String getNome_meta() {
		return nome_meta;
	}
	public Money getMeta() {
		return meta;
	}
	public Money getMetadiaria() {
		if(this.getMeta() != null && this.getAux_diasuteis() != null && this.getAux_diasuteis() > 0){
			return new Money(this.getMeta().getValue().doubleValue()/this.getAux_diascorridos());
		}
		return metadiaria;
	}
	
	public Money getMetapendente() {
		metapendente = new Money();
		if(meta != null){
			metapendente = meta.subtract(getValortotalvenda());
		}
		return metapendente != null && metapendente.getValue().doubleValue() > 0 ? metapendente : new Money();
	}
	
	
	public void setNome_vendedor(String nomeVendedor) {
		nome_vendedor = nomeVendedor;
	}
	public void setNome_meta(String nomeMeta) {
		nome_meta = nomeMeta;
	}
	public void setMeta(Money meta) {
		this.meta = meta;
	}
	public void setMetadiaria(Money metadiaria) {
		this.metadiaria = metadiaria;
	}
	public void setMetapendente(Money metapendente) {
		this.metapendente = metapendente;
	}
	
	public Date getAux_dtreferencia() {
		return aux_dtreferencia;
	}
	public Date getAux_firstDateMonth() {
		return aux_firstDateMonth;
	}
	public Date getAux_lastDateMonth() {
		return aux_lastDateMonth;
	}
	public Integer getAux_diasuteis() {
		return aux_diasuteis;
	}
	public Integer getAux_diascorridos() {
		return aux_diascorridos;
	}
	public Integer getAux_diasrestantes() {
		return aux_diasrestantes;
	}
	public void setAux_dtreferencia(Date auxDtreferencia) {
		aux_dtreferencia = auxDtreferencia;
	}
	public void setAux_firstDateMonth(Date auxFirstDateMonth) {
		aux_firstDateMonth = auxFirstDateMonth;
	}
	public void setAux_lastDateMonth(Date auxLastDateMonth) {
		aux_lastDateMonth = auxLastDateMonth;
	}
	public void setAux_diasuteis(Integer auxDiasuteis) {
		aux_diasuteis = auxDiasuteis;
	}
	public void setAux_diascorridos(Integer auxDiascorridos) {
		aux_diascorridos = auxDiascorridos;
	}
	public void setAux_diasrestantes(Integer auxDiasrestantes) {
		aux_diasrestantes = auxDiasrestantes;
	}
	
	public Money getValortotalvenda() {
		Money valor = new Money();
		if(getListaItens() != null && !getListaItens().isEmpty()){
			for(AcompanhamentometaAnaliticoItensBean item : getListaItens()){
				if(item.getMetadiaria() != null){
					valor = valor.add(item.getVendadiaria());
				}
			}
		}
		return valor;
	}
	public void setValortotalvenda(Money valortotalvenda) {
		this.valortotalvenda = valortotalvenda;
	}
	
	public List<AcompanhamentometaAnaliticoItensBean> getListaItens() {
		return listaItens;
	}
	public void setListaItens(List<AcompanhamentometaAnaliticoItensBean> listaItens) {
		this.listaItens = listaItens;
	}
	
	public Image getGrafico() {
		return grafico;
	}
	public void setGrafico(Image grafico) {
		this.grafico = grafico;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AcompanhamentometaAnaliticoBean other = (AcompanhamentometaAnaliticoBean) obj;
		if (nome_vendedor == null) {
			if (other.nome_vendedor != null)
				return false;
		} else if (!nome_vendedor.equals(other.nome_vendedor))
			return false;
		return true;
	}
}
