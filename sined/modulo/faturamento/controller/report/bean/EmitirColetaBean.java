package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.LinkedList;

public class EmitirColetaBean {
	
	protected Integer codigoColeta;
	protected String tipo;
	protected String pessoa;
	protected String coletor;
	protected String observacao;
	protected String motivodevolucao;
	protected Integer pedidovenda_id;
	protected String responsavelPedido;
	protected String clienteEndereco;
	protected String dataPedido;
	protected String identificadorPedido;
	protected String vendedorPrincipalPedido;
	protected String identificadorExternoPedido;
	protected String notafiscal_ids;
	protected String entradafiscal_ids;
	protected Integer notaRemessaEntrega;
	protected Integer notaRemessaPropria;
	protected Integer notaRetorno;
	protected LinkedList<EmitirColetaMaterialBean> listaColetaMaterial = new LinkedList<EmitirColetaMaterialBean>();
	
	public Integer getCodigoColeta() {
		return codigoColeta;
	}
	public String getTipo() {
		return tipo;
	}
	public String getPessoa() {
		return pessoa;
	}
	public String getColetor() {
		return coletor;
	}
	public String getObservacao() {
		return observacao;
	}
	public String getMotivodevolucao() {
		return motivodevolucao;
	}
	public Integer getPedidovenda_id() {
		return pedidovenda_id;
	}
	public String getResponsavelPedido() {
		return responsavelPedido;
	}
	public String getClienteEndereco() {
		return clienteEndereco;
	}
	public String getDataPedido() {
		return dataPedido;
	}
	public String getIdentificadorPedido() {
		return identificadorPedido;
	}
	public String getVendedorPrincipalPedido() {
		return vendedorPrincipalPedido;
	}
	public String getIdentificadorExternoPedido() {
		return identificadorExternoPedido;
	}
	public String getNotafiscal_ids() {
		return notafiscal_ids;
	}
	public String getEntradafiscal_ids() {
		return entradafiscal_ids;
	}
	public Integer getNotaRemessaEntrega() {
		return notaRemessaEntrega;
	}
	public Integer getNotaRemessaPropria() {
		return notaRemessaPropria;
	}
	public Integer getNotaRetorno() {
		return notaRetorno;
	}
	public LinkedList<EmitirColetaMaterialBean> getListaColetaMaterial() {
		return listaColetaMaterial;
	}
	
	public void setCodigoColeta(Integer codigoColeta) {
		this.codigoColeta = codigoColeta;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setPessoa(String pessoa) {
		this.pessoa = pessoa;
	}
	public void setColetor(String coletor) {
		this.coletor = coletor;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setMotivodevolucao(String motivodevolucao) {
		this.motivodevolucao = motivodevolucao;
	}
	public void setPedidovenda_id(Integer pedidovenda_id) {
		this.pedidovenda_id = pedidovenda_id;
	}
	public void setResponsavelPedido(String responsavelPedido) {
		this.responsavelPedido = responsavelPedido;
	}
	public void setClienteEndereco(String clienteEndereco) {
		this.clienteEndereco = clienteEndereco;
	}
	public void setDataPedido(String dataPedido) {
		this.dataPedido = dataPedido;
	}
	public void setIdentificadorPedido(String identificadorPedido) {
		this.identificadorPedido = identificadorPedido;
	}
	public void setVendedorPrincipalPedido(String vendedorPrincipalPedido) {
		this.vendedorPrincipalPedido = vendedorPrincipalPedido;
	}
	public void setIdentificadorExternoPedido(String identificadorExternoPedido) {
		this.identificadorExternoPedido = identificadorExternoPedido;
	}
	public void setNotafiscal_ids(String notafiscal_ids) {
		this.notafiscal_ids = notafiscal_ids;
	}
	public void setEntradafiscal_ids(String entradafiscal_ids) {
		this.entradafiscal_ids = entradafiscal_ids;
	}
	public void setNotaRemessaEntrega(Integer notaRemessaEntrega) {
		this.notaRemessaEntrega = notaRemessaEntrega;
	}
	public void setNotaRemessaPropria(Integer notaRemessaPropria) {
		this.notaRemessaPropria = notaRemessaPropria;
	}
	public void setNotaRetorno(Integer notaRetorno) {
		this.notaRetorno = notaRetorno;
	}
	public void setListaColetaMaterial(
			LinkedList<EmitirColetaMaterialBean> listaColetaMaterial) {
		this.listaColetaMaterial = listaColetaMaterial;
	}
}
