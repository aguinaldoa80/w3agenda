package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import br.com.linkcom.neo.types.Money;

public class TotaisComprovanteConfiguravel {
	
	private String TITULO;
	private String DESCRICAO;
	private String VALOR;
	
	private String CDMATERIALGRUPO;
	private String CDMATERIALGRUPOMESTRE;
	
	private Money valor;
	
	public String getTITULO() {
		return TITULO;
	}
	public String getDESCRICAO() {
		return DESCRICAO;
	}
	public String getVALOR() {
		if(valor != null) return valor.toString();
		return VALOR;
	}
	
	public void setTITULO(String tITULO) {
		TITULO = tITULO;
	}
	public void setDESCRICAO(String dESCRICAO) {
		DESCRICAO = dESCRICAO;
	}
	public void setVALOR(String vALOR) {
		VALOR = vALOR;
	}
	
	
	public Money getValor() {
		return valor;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public String getCDMATERIALGRUPO() {
		return CDMATERIALGRUPO;
	}
	public void setCDMATERIALGRUPO(String cDMATERIALGRUPO) {
		CDMATERIALGRUPO = cDMATERIALGRUPO;
	}
	
	public String getCDMATERIALGRUPOMESTRE() {
		return CDMATERIALGRUPOMESTRE;
	}
	public void setCDMATERIALGRUPOMESTRE(String cDMATERIALGRUPOMESTRE) {
		CDMATERIALGRUPOMESTRE = cDMATERIALGRUPOMESTRE;
	}
}
