package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.ArrayList;
import java.util.List;

public class AnaliseVendaMensalBean {
	
	private Integer cliente_id;
	private String cliente_identificador;
	private String cliente_nome;
	private String cliente_endereco;
	
	private List<AnaliseVendaMensalItemBean> listaItem = new ArrayList<AnaliseVendaMensalItemBean>();
	
	public AnaliseVendaMensalBean(Integer clienteId,
			String clienteIdentificador, String clienteNome,
			String clienteEndereco) {
		cliente_id = clienteId;
		cliente_identificador = clienteIdentificador;
		cliente_nome = clienteNome;
		cliente_endereco = clienteEndereco;
	}

	public String getCliente_identificador() {
		return cliente_identificador;
	}

	public String getCliente_nome() {
		return cliente_nome;
	}

	public String getCliente_endereco() {
		return cliente_endereco;
	}

	public List<AnaliseVendaMensalItemBean> getListaItem() {
		return listaItem;
	}
	
	public Integer getCliente_id() {
		return cliente_id;
	}
	
	public void setCliente_id(Integer clienteId) {
		cliente_id = clienteId;
	}

	public void setCliente_identificador(String clienteIdentificador) {
		cliente_identificador = clienteIdentificador;
	}

	public void setCliente_nome(String clienteNome) {
		cliente_nome = clienteNome;
	}

	public void setCliente_endereco(String clienteEndereco) {
		cliente_endereco = clienteEndereco;
	}

	public void setListaItem(List<AnaliseVendaMensalItemBean> listaItem) {
		this.listaItem = listaItem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cliente_id == null) ? 0 : cliente_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnaliseVendaMensalBean other = (AnaliseVendaMensalBean) obj;
		if (cliente_id == null) {
			if (other.cliente_id != null)
				return false;
		} else if (!cliente_id.equals(other.cliente_id))
			return false;
		return true;
	}
	
}