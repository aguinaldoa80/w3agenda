package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;

public class AcompanhamentometaBean {
	
	private String nome_empresa;
	private String nome_vendedor;
	private String nome_meta;
	private Money meta;
	private Money metadiaria;
	private Money valorvenda;
	private Money valorrestante;
	private Money objetivodiario;
	private Double percentualvendido;
	private Double percentualmes;
	private Double diferenca;
	
	private Date aux_dtreferencia;
	private Date aux_firstDateMonth;
	private Date aux_lastDateMonth;
	private Integer aux_diasuteis;
	private Integer aux_diascorridos;
	private Integer aux_diasrestantes;
	
	public String getNome_empresa() {
		return nome_empresa;
	}
	public String getNome_vendedor() {
		return nome_vendedor;
	}
	public String getNome_meta() {
		return nome_meta;
	}
	public Money getMeta() {
		return meta;
	}
	public Money getMetadiaria() {
		if(this.getMeta() != null && this.getAux_diasuteis() != null && this.getAux_diasuteis() > 0){
			return new Money(this.getMeta().getValue().doubleValue()/this.getAux_diascorridos());
		}
		return metadiaria;
	}
	public Money getValorvenda() {
		
		return valorvenda;
	}
	public Money getValorrestante() {
		if(this.getMeta() != null && this.getValorvenda() != null && this.getValorvenda().getValue().doubleValue() > 0){
			return this.getMeta().subtract(this.getValorvenda());
		}
		return valorrestante;
	}
	public Money getObjetivodiario() {
		if(this.getValorrestante() != null){
			if(this.getAux_diasrestantes() != null && this.getAux_diasrestantes() > 0)
				return new Money(this.getValorrestante().getValue().doubleValue()/this.getAux_diasrestantes());
			else return new Money();
		}
		return objetivodiario;
	}
	public Double getPercentualvendido() {
		if(this.getValorvenda() != null && this.getMeta() != null && this.getMeta().getValue().doubleValue() > 0){
			return (this.getValorvenda().getValue().doubleValue() / this.getMeta().getValue().doubleValue())*100;
		}
		return percentualvendido;
	}
	public Double getPercentualmes() {
		if(this.getAux_diascorridos() != null && this.getAux_diasuteis() != null && this.getAux_diasuteis() > 0){
//			return new Double((this.getAux_diascorridos() * 100d)  / this.getAux_diasuteis()) ;
			return  100d / this.getAux_diascorridos() * this.getAux_diasuteis() ;
		}
		return percentualmes;
	}
	public Double getDiferenca() {
		if(this.getPercentualvendido() != null && this.getPercentualmes() != null){
			return this.getPercentualvendido() - this.getPercentualmes();
		}
		return diferenca;
	}
	
	
	public void setNome_empresa(String nomeEmpresa) {
		nome_empresa = nomeEmpresa;
	}
	public void setNome_vendedor(String nomeVendedor) {
		nome_vendedor = nomeVendedor;
	}
	public void setNome_meta(String nomeMeta) {
		nome_meta = nomeMeta;
	}
	public void setMeta(Money meta) {
		this.meta = meta;
	}
	public void setMetadiaria(Money metadiaria) {
		this.metadiaria = metadiaria;
	}
	public void setValorvenda(Money valorvenda) {
		this.valorvenda = valorvenda;
	}
	public void setValorrestante(Money valorrestante) {
		this.valorrestante = valorrestante;
	}
	public void setObjetivodiario(Money objetivodiario) {
		this.objetivodiario = objetivodiario;
	}
	public void setPercentualvendido(Double percentualvendido) {
		this.percentualvendido = percentualvendido;
	}
	public void setPercentualmes(Double percentualmes) {
		this.percentualmes = percentualmes;
	}
	public void setDiferenca(Double diferenca) {
		this.diferenca = diferenca;
	}
	
	
	public Date getAux_dtreferencia() {
		return aux_dtreferencia;
	}
	public Date getAux_firstDateMonth() {
		return aux_firstDateMonth;
	}
	public Date getAux_lastDateMonth() {
		return aux_lastDateMonth;
	}
	public Integer getAux_diasuteis() {
		return aux_diasuteis;
	}
	public Integer getAux_diascorridos() {
		return aux_diascorridos;
	}
	public Integer getAux_diasrestantes() {
		return aux_diasrestantes;
	}
	public void setAux_dtreferencia(Date auxDtreferencia) {
		aux_dtreferencia = auxDtreferencia;
	}
	public void setAux_firstDateMonth(Date auxFirstDateMonth) {
		aux_firstDateMonth = auxFirstDateMonth;
	}
	public void setAux_lastDateMonth(Date auxLastDateMonth) {
		aux_lastDateMonth = auxLastDateMonth;
	}
	public void setAux_diasuteis(Integer auxDiasuteis) {
		aux_diasuteis = auxDiasuteis;
	}
	public void setAux_diascorridos(Integer auxDiascorridos) {
		aux_diascorridos = auxDiascorridos;
	}
	public void setAux_diasrestantes(Integer auxDiasrestantes) {
		aux_diasrestantes = auxDiasrestantes;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AcompanhamentometaBean other = (AcompanhamentometaBean) obj;
		if (nome_vendedor == null) {
			if (other.nome_vendedor != null)
				return false;
		} else if (!nome_vendedor.equals(other.nome_vendedor))
			return false;
		return true;
	}
}
