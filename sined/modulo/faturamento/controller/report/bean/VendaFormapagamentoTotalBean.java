package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import br.com.linkcom.sined.geral.bean.Conta;

public class VendaFormapagamentoTotalBean {
	
	private Conta conta;
	private Double valorbruto;
	private Double taxa;
	private Double valorrecebido;
	
	public Conta getConta() {
		return conta;
	}
	public Double getValorbruto() {
		return valorbruto;
	}
	public Double getTaxa() {
		return taxa;
	}
	public Double getValorrecebido() {
		return valorrecebido;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setValorbruto(Double valorbruto) {
		this.valorbruto = valorbruto;
	}
	public void setTaxa(Double taxa) {
		this.taxa = taxa;
	}
	public void setValorrecebido(Double valorrecebido) {
		this.valorrecebido = valorrecebido;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((conta == null) ? 0 : conta.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendaFormapagamentoTotalBean other = (VendaFormapagamentoTotalBean) obj;
		if (conta == null) {
			if (other.conta != null)
				return false;
		} else if (!conta.equals(other.conta))
			return false;
		return true;
	}
	
}