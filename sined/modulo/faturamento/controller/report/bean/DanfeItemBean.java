package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

public class DanfeItemBean {
	
	private Integer qtdeReg = 0;
	private String codigo = "";
	private String automacao = "";
	private String descricao = "";
	private String ncm = "";
	private String excecao = "";
	private String genero = "";
	private String embalagem = "";
	private Double quantidade = 0d;
	private Double valorUnitario = 0d;
	private String desconto = "";
	private String situacaoTributaria = "";
	private Double aliquotaIcms = 0d;
	private Double aliquotaIpi = 0d;
	private String reduzido = "";
	private Double valorIpi = 0d;
	private Double valorTotal = 0d;
	private String tipoPisCofins = "";
	private Double baseIcms = 0d;
	private Double baseIcmsSt = 0d;
	private Double valorIcms = 0d;
	private Double valorIcmsSt = 0d;
	private String cfop = "";
	private String infoadicional = "";
	private String plaqueta = "";
	private String tributos = "";
	
	public Integer getQtdeReg() {
		return qtdeReg;
	}
	public String getCodigo() {
		return codigo;
	}
	public String getAutomacao() {
		return automacao;
	}
	public String getDescricao() {
		return descricao;
	}
	public String getNcm() {
		return ncm;
	}
	public String getExcecao() {
		return excecao;
	}
	public String getGenero() {
		return genero;
	}
	public String getEmbalagem() {
		return embalagem;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Double getValorUnitario() {
		return valorUnitario;
	}
	public String getDesconto() {
		return desconto;
	}
	public String getSituacaoTributaria() {
		return situacaoTributaria;
	}
	public Double getAliquotaIcms() {
		return aliquotaIcms;
	}
	public Double getAliquotaIpi() {
		return aliquotaIpi;
	}
	public String getReduzido() {
		return reduzido;
	}
	public Double getValorIpi() {
		return valorIpi;
	}
	public Double getValorTotal() {
		return valorTotal;
	}
	public String getTipoPisCofins() {
		return tipoPisCofins;
	}
	public Double getBaseIcms() {
		return baseIcms;
	}
	public Double getBaseIcmsSt() {
		return baseIcmsSt;
	}
	public Double getValorIcms() {
		return valorIcms;
	}
	public Double getValorIcmsSt() {
		return valorIcmsSt;
	}
	public String getCfop() {
		return cfop;
	}
	public String getInfoadicional() {
		return infoadicional;
	}
	public String getPlaqueta() {
		return plaqueta;
	}
	public String getTributos() {
		return tributos;
	}
	public void setTributos(String tributos) {
		this.tributos = tributos;
	}
	public void setPlaqueta(String plaqueta) {
		this.plaqueta = plaqueta;
	}
	public void setInfoadicional(String infoadicional) {
		this.infoadicional = infoadicional;
	}
	public void setCfop(String cfop) {
		this.cfop = cfop;
	}
	public void setQtdeReg(Integer qtdeReg) {
		this.qtdeReg = qtdeReg;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setAutomacao(String automacao) {
		this.automacao = automacao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setNcm(String ncm) {
		this.ncm = ncm;
	}
	public void setExcecao(String excecao) {
		this.excecao = excecao;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public void setEmbalagem(String embalagem) {
		this.embalagem = embalagem;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setValorUnitario(Double valorUnitario) {
		this.valorUnitario = valorUnitario;
	}
	public void setDesconto(String desconto) {
		this.desconto = desconto;
	}
	public void setSituacaoTributaria(String situacaoTributaria) {
		this.situacaoTributaria = situacaoTributaria;
	}
	public void setAliquotaIcms(Double aliquotaIcms) {
		this.aliquotaIcms = aliquotaIcms;
	}
	public void setAliquotaIpi(Double aliquotaIpi) {
		this.aliquotaIpi = aliquotaIpi;
	}
	public void setReduzido(String reduzido) {
		this.reduzido = reduzido;
	}
	public void setValorIpi(Double valorIpi) {
		this.valorIpi = valorIpi;
	}
	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}
	public void setTipoPisCofins(String tipoPisCofins) {
		this.tipoPisCofins = tipoPisCofins;
	}
	public void setBaseIcms(Double baseIcms) {
		this.baseIcms = baseIcms;
	}
	public void setBaseIcmsSt(Double baseIcmsSt) {
		this.baseIcmsSt = baseIcmsSt;
	}
	public void setValorIcms(Double valorIcms) {
		this.valorIcms = valorIcms;
	}
	public void setValorIcmsSt(Double valorIcmsSt) {
		this.valorIcmsSt = valorIcmsSt;
	}

}
