package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Cliente;

public class EmitirEtiquetaDestinatarioBean extends ReportTemplateBean{

	private String nome;
	private String razaosocial;
	private String cnpjCpf;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String cep;
	private String municipio;
	private String uf;
	
	public EmitirEtiquetaDestinatarioBean(Cliente cliente){
		if(cliente!=null){
			this.setNome(cliente.getNome());
			this.setRazaosocial(cliente.getRazaosocial());
			this.setCnpjCpf(cliente.getCpfcnpj());
			this.setLogradouro(cliente.getEnderecoLogradouro());
			this.setNumero(cliente.getEnderecoNumero());
			this.setComplemento(cliente.getEnderecoComplemento());
			this.setBairro(cliente.getEnderecoBairro());
			this.setCep(cliente.getEnderecoCep());
			this.setMunicipio(cliente.getEnderecoCidade());
			this.setUf(cliente.getEnderecoSiglaEstado());
		}
	}
	
	public String getNome() {
		return Util.strings.emptyIfNull(nome);
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getRazaosocial() {
		return Util.strings.emptyIfNull(razaosocial);
	}
	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}
	
	public String getCnpjCpf() {
		return Util.strings.emptyIfNull(cnpjCpf);
	}
	public void setCnpjCpf(String cnpjCpf) {
		this.cnpjCpf = cnpjCpf;
	}
	
	public String getLogradouro() {
		return Util.strings.emptyIfNull(logradouro);
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	
	public String getNumero() {
		return Util.strings.emptyIfNull(numero);
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public String getComplemento() {
		return Util.strings.emptyIfNull(complemento);
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	
	public String getBairro() {
		return Util.strings.emptyIfNull(bairro);
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	
	public String getCep() {
		return Util.strings.emptyIfNull(cep);
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	
	public String getMunicipio() {
		return Util.strings.emptyIfNull(municipio);
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	
	public String getUf() {
		return Util.strings.emptyIfNull(uf);
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
}
