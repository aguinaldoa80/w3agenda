package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

public class VendaPagamentoBean {
	
	private String formapagamento;
	private String numerobanco;
	private String conta;
	private String agencia;
	private String numero;
	private String vencimento;
	private String valor;
	
	public String getFormapagamento() {
		return formapagamento;
	}
	public String getNumerobanco() {
		return numerobanco;
	}
	public String getConta() {
		return conta;
	}
	public String getAgencia() {
		return agencia;
	}
	public String getNumero() {
		return numero;
	}
	public String getVencimento() {
		return vencimento;
	}
	public String getValor() {
		return valor;
	}
	public void setFormapagamento(String formapagamento) {
		this.formapagamento = formapagamento;
	}
	public void setNumerobanco(String numerobanco) {
		this.numerobanco = numerobanco;
	}
	public void setConta(String conta) {
		this.conta = conta;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setVencimento(String vencimento) {
		this.vencimento = vencimento;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public void preencherCamposNulos(){
		if(formapagamento == null) formapagamento = "";
		if(numerobanco == null) numerobanco = "";
		if(conta == null) conta = "";
		if(agencia == null) agencia = "";
		if(numero == null) numero = "";
		if(vencimento == null) vencimento = "";
		if(valor == null) valor = "";
	}
}
	
