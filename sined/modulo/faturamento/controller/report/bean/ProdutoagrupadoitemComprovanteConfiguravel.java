package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

public class ProdutoagrupadoitemComprovanteConfiguravel {
	
	
	private String UNIDADEMEDIDA;
	private String PRECO;
	private Double QUANTIDADE;
	private Double QUANTIDADEUNIDADEPRINCIPAL;
	private Double QUANTIDADEMETROCUBICO;
	private String VALORMETROCUBICO;
	private String ENTREGA;
	private String TOTAL;
	private String OBSERVACAO;
	private String LOTEESTOQUE;
	private String DESCONTO;
	private String COMPRIMENTO;
	private String LARGURA;
	private String ALTURA;
	private String PESOLIQUIDO;
	private String PESOBRUTO;
	private String FORNECEDOR;
	
	public String getUNIDADEMEDIDA() {
		return UNIDADEMEDIDA;
	}
	public String getPRECO() {
		return PRECO;
	}
	public Double getQUANTIDADE() {
		return QUANTIDADE;
	}
	public Double getQUANTIDADEUNIDADEPRINCIPAL() {
		return QUANTIDADEUNIDADEPRINCIPAL;
	}
	public Double getQUANTIDADEMETROCUBICO() {
		return QUANTIDADEMETROCUBICO;
	}
	public String getVALORMETROCUBICO() {
		return VALORMETROCUBICO;
	}
	public String getENTREGA() {
		return ENTREGA;
	}
	public String getTOTAL() {
		return TOTAL;
	}
	public String getOBSERVACAO() {
		return OBSERVACAO;
	}
	public String getLOTEESTOQUE() {
		return LOTEESTOQUE;
	}
	public String getDESCONTO() {
		return DESCONTO;
	}
	public String getCOMPRIMENTO() {
		return COMPRIMENTO;
	}
	public String getLARGURA() {
		return LARGURA;
	}
	public String getALTURA() {
		return ALTURA;
	}
	public String getPESOLIQUIDO() {
		return PESOLIQUIDO;
	}
	public String getPESOBRUTO() {
		return PESOBRUTO;
	}
	public String getFORNECEDOR() {
		return FORNECEDOR;
	}
	
	public void setUNIDADEMEDIDA(String uNIDADEMEDIDA) {
		UNIDADEMEDIDA = uNIDADEMEDIDA;
	}
	public void setPRECO(String pRECO) {
		PRECO = pRECO;
	}
	public void setQUANTIDADE(Double qUANTIDADE) {
		QUANTIDADE = qUANTIDADE;
	}
	public void setQUANTIDADEUNIDADEPRINCIPAL(Double qUANTIDADEUNIDADEPRINCIPAL) {
		QUANTIDADEUNIDADEPRINCIPAL = qUANTIDADEUNIDADEPRINCIPAL;
	}
	public void setQUANTIDADEMETROCUBICO(Double qUANTIDADEMETROCUBICO) {
		QUANTIDADEMETROCUBICO = qUANTIDADEMETROCUBICO;
	}
	public void setVALORMETROCUBICO(String vALORMETROCUBICO) {
		VALORMETROCUBICO = vALORMETROCUBICO;
	}
	public void setENTREGA(String eNTREGA) {
		ENTREGA = eNTREGA;
	}
	public void setTOTAL(String tOTAL) {
		TOTAL = tOTAL;
	}
	public void setOBSERVACAO(String oBSERVACAO) {
		OBSERVACAO = oBSERVACAO;
	}
	public void setLOTEESTOQUE(String lOTEESTOQUE) {
		LOTEESTOQUE = lOTEESTOQUE;
	}
	public void setDESCONTO(String dESCONTO) {
		DESCONTO = dESCONTO;
	}
	public void setCOMPRIMENTO(String cOMPRIMENTO) {
		COMPRIMENTO = cOMPRIMENTO;
	}
	public void setLARGURA(String lARGURA) {
		LARGURA = lARGURA;
	}
	public void setALTURA(String aLTURA) {
		ALTURA = aLTURA;
	}
	public void setPESOLIQUIDO(String pESOLIQUIDO) {
		PESOLIQUIDO = pESOLIQUIDO;
	}
	public void setPESOBRUTO(String pESOBRUTO) {
		PESOBRUTO = pESOBRUTO;
	}
	public void setFORNECEDOR(String fORNECEDOR) {
		FORNECEDOR = fORNECEDOR;
	}
}