package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Material;

public class ProdutokitflexivelitemComprovanteConfiguravel {
	
	private String CODIGO;
	private String DESCRICAO;
	private Double PRECO;
	private Double QUANTIDADE;
	private String TOTAL;
	private Double TOTALITEM;
	private String CDMATERIALGRUPO;
	private String NCMCOMPLETO;
	private String IMAGEM;
	private String OBSERVACAO;
	private String CARACTERISTICA;
	private Double PESOLIQUIDO;
	private Double PESOBRUTO;
	private String ENTREGA;
	private String LOTEESTOQUE;
	private String COMPRIMENTO;
	private String LARGURA;
	private String ALTURA;
	private String UNIDADEMEDIDA;
	
	private Material material;
	
	public String getCODIGO() {
		return CODIGO;
	}
	public String getDESCRICAO() {
		return DESCRICAO;
	}
	public Double getPRECO() {
		return PRECO;
	}
	public Double getQUANTIDADE() {
		return QUANTIDADE;
	}
	public String getTOTAL() {
		return TOTAL != null ? TOTAL : (getTOTALITEM() != null ? new Money(getTOTALITEM()).toString() : "");
	}
	public Material getMaterial() {
		return material;
	}

	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}
	public void setDESCRICAO(String dESCRICAO) {
		DESCRICAO = dESCRICAO;
	}
	public void setPRECO(Double pRECO) {
		PRECO = pRECO;
	}
	public void setQUANTIDADE(Double qUANTIDADE) {
		QUANTIDADE = qUANTIDADE;
	}
	public void setTOTAL(String tOTAL) {
		TOTAL = tOTAL;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public String getCDMATERIALGRUPO() {
		return CDMATERIALGRUPO;
	}
	public String getNCMCOMPLETO() {
		return NCMCOMPLETO;
	}
	public String getIMAGEM() {
		return IMAGEM;
	}
	public String getOBSERVACAO() {
		return OBSERVACAO;
	}
	public String getCARACTERISTICA() {
		return CARACTERISTICA;
	}
	public Double getPESOLIQUIDO() {
		return PESOLIQUIDO;
	}
	public Double getPESOBRUTO() {
		return PESOBRUTO;
	}
	public Double getTOTALITEM() {
		return TOTALITEM;
	}
	
	public void setCDMATERIALGRUPO(String cDMATERIALGRUPO) {
		CDMATERIALGRUPO = cDMATERIALGRUPO;
	}
	public void setNCMCOMPLETO(String nCMCOMPLETO) {
		NCMCOMPLETO = nCMCOMPLETO;
	}
	public void setIMAGEM(String iMAGEM) {
		IMAGEM = iMAGEM;
	}
	public void setOBSERVACAO(String oBSERVACAO) {
		OBSERVACAO = oBSERVACAO;
	}
	public void setCARACTERISTICA(String cARACTERISTICA) {
		CARACTERISTICA = cARACTERISTICA;
	}
	public void setPESOLIQUIDO(Double pESOLIQUIDO) {
		PESOLIQUIDO = pESOLIQUIDO;
	}
	public void setPESOBRUTO(Double pESOBRUTO) {
		PESOBRUTO = pESOBRUTO;
	}
	public String getENTREGA() {
		return ENTREGA;
	}
	public String getLOTEESTOQUE() {
		return LOTEESTOQUE;
	}
	public String getCOMPRIMENTO() {
		return COMPRIMENTO;
	}
	public String getLARGURA() {
		return LARGURA;
	}
	public String getALTURA() {
		return ALTURA;
	}
	public String getUNIDADEMEDIDA() {
		return UNIDADEMEDIDA;
	}
	public void setENTREGA(String eNTREGA) {
		ENTREGA = eNTREGA;
	}
	public void setLOTEESTOQUE(String lOTEESTOQUE) {
		LOTEESTOQUE = lOTEESTOQUE;
	}
	public void setCOMPRIMENTO(String cOMPRIMENTO) {
		COMPRIMENTO = cOMPRIMENTO;
	}
	public void setLARGURA(String lARGURA) {
		LARGURA = lARGURA;
	}
	public void setALTURA(String aLTURA) {
		ALTURA = aLTURA;
	}
	public void setUNIDADEMEDIDA(String uNIDADEMEDIDA) {
		UNIDADEMEDIDA = uNIDADEMEDIDA;
	}
	public void setTOTALITEM(Double tOTALITEM) {
		TOTALITEM = tOTALITEM;
	}
}