package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

public class CondicaoPagamentoComprovanteConfiguravel {
	
	private String DESCRICAO;
	private Integer QTDEPARCELA;
	private String DATA1PARCELA;
	private String VALORPARCELA;
	private String VALORTOTAL;
	
	public String getDESCRICAO() {
		return DESCRICAO;
	}
	public void setDESCRICAO(String dESCRICAO) {
		DESCRICAO = dESCRICAO;
	}
	public Integer getQTDEPARCELA() {
		return QTDEPARCELA;
	}
	public void setQTDEPARCELA(Integer qTDEPARCELA) {
		QTDEPARCELA = qTDEPARCELA;
	}
	public String getDATA1PARCELA() {
		return DATA1PARCELA;
	}
	public void setDATA1PARCELA(String dATA1PARCELA) {
		DATA1PARCELA = dATA1PARCELA;
	}
	public String getVALORPARCELA() {
		return VALORPARCELA;
	}
	public void setVALORPARCELA(String vALORPARCELA) {
		VALORPARCELA = vALORPARCELA;
	}
	public String getVALORTOTAL() {
		return VALORTOTAL;
	}
	public void setVALORTOTAL(String vALORTOTAL) {
		VALORTOTAL = vALORTOTAL;
	}

}
