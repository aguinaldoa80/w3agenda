package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

public class PagamentoComprovanteConfiguravel {
	private String FORMAPAGAMENTO;
	private Integer NUMEROBANCO;
	private Integer CONTA;
	private Integer AGENCIA;
	private String NUMERO;
	private String VENCIMENTO;
	private String VALOR;
	public String getFORMAPAGAMENTO() {
		return FORMAPAGAMENTO;
	}
	public Integer getNUMEROBANCO() {
		return NUMEROBANCO;
	}
	public Integer getCONTA() {
		return CONTA;
	}
	public Integer getAGENCIA() {
		return AGENCIA;
	}
	public String getNUMERO() {
		return NUMERO;
	}
	public String getVENCIMENTO() {
		return VENCIMENTO;
	}
	public String getVALOR() {
		return VALOR;
	}
	public void setFORMAPAGAMENTO(String formapagamento) {
		FORMAPAGAMENTO = formapagamento;
	}
	public void setNUMEROBANCO(Integer numerobanco) {
		NUMEROBANCO = numerobanco;
	}
	public void setCONTA(Integer conta) {
		CONTA = conta;
	}
	public void setAGENCIA(Integer agencia) {
		AGENCIA = agencia;
	}
	public void setNUMERO(String numero) {
		NUMERO = numero;
	}
	public void setVENCIMENTO(String vencimento) {
		VENCIMENTO = vencimento;
	}
	public void setVALOR(String valor) {
		VALOR = valor;
	}
			
}
