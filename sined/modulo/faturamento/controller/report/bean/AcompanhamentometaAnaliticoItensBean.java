package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.math.BigDecimal;
import java.util.Date;

import br.com.linkcom.neo.types.Money;

public class AcompanhamentometaAnaliticoItensBean {
	
	private Date data;
	private Money vendatotal;
	private Money vendadiaria;
	private Money metadiaria;
	private Money metapendente;
	
	private Boolean primeiroitem;
	private Double percentual;

	public Date getData() {
		return data;
	}
	public Money getVendatotal() {
		return vendatotal;
	}
	public Money getVendadiaria() {
		return vendadiaria;
	}
	public Money getMetadiaria() {
		return metadiaria;
	}

	public void setData(Date data) {
		this.data = data;
	}
	public void setVendatotal(Money vendatotal) {
		this.vendatotal = vendatotal;
	}
	public void setVendadiaria(Money vendadiaria) {
		this.vendadiaria = vendadiaria;
	}
	public void setMetadiaria(Money metadiaria) {
		this.metadiaria = metadiaria;
	}

	public Boolean getPrimeiroitem() {
		return primeiroitem;
	}
	public Double getPercentual() {
		return percentual;
	}
	public void setPrimeiroitem(Boolean primeiroitem) {
		this.primeiroitem = primeiroitem;
	}
	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}
	public Money getMetapendente() {
		if(this.metadiaria == null||
			this.vendadiaria == null ||
			metadiaria.subtract(vendadiaria).getValue().compareTo(BigDecimal.ZERO) < 0){
			metapendente = new Money();
		}else{
			metapendente = metadiaria.subtract(vendadiaria);
		}
		
		return metapendente;
	}
	public void setMetapendente(Money metapendente) {
		this.metapendente = metapendente;
	}
}
