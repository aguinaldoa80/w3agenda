package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

public class VendasPorVendendorBean {

	protected String vendedor;
	protected Double quantidade;
	protected Double precoMedio;
	protected Double quantidadeConvertida;
	protected Double precoMedioConvertida;
	protected Double faturamentoTotal;
	protected Double faturamentoTotalPercentagem;
	protected Double descontoMedio;
	protected Double descontoMedioPercentagem;
	protected Double comissionamentoMedio;
	protected Double comissionamentoMedioPercentagem;

	public String getVendedor() {
		return vendedor;
	}

	public void setVendedor(String vendedor) {
		this.vendedor = vendedor;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public Double getPrecoMedio() {
		return precoMedio;
	}

	public void setPrecoMedio(Double precoMedio) {
		this.precoMedio = precoMedio;
	}

	public Double getQuantidadeConvertida() {
		return quantidadeConvertida;
	}

	public void setQuantidadeConvertida(Double quantidadeConvertida) {
		this.quantidadeConvertida = quantidadeConvertida;
	}

	public Double getPrecoMedioConvertida() {
		return precoMedioConvertida;
	}

	public void setPrecoMedioConvertida(Double precoMedioConvertida) {
		this.precoMedioConvertida = precoMedioConvertida;
	}

	public Double getFaturamentoTotal() {
		return faturamentoTotal;
	}

	public void setFaturamentoTotal(Double faturamentoTotal) {
		this.faturamentoTotal = faturamentoTotal;
	}

	public Double getFaturamentoTotalPercentagem() {
		return faturamentoTotalPercentagem;
	}
	
	public void setFaturamentoTotalPercentagem(Double faturamentoTotalPercentagem) {
		this.faturamentoTotalPercentagem = faturamentoTotalPercentagem;
	}

	public Double getDescontoMedio() {
		return descontoMedio;
	}

	public void setDescontoMedio(Double descontoMedio) {
		this.descontoMedio = descontoMedio;
	}

	public Double getDescontoMedioPercentagem() {
		return descontoMedioPercentagem;
	}

	public void setDescontoMedioPercentagem(Double descontoMedioPercentagem) {
		this.descontoMedioPercentagem = descontoMedioPercentagem;
	}

	public Double getComissionamentoMedio() {
		return comissionamentoMedio;
	}
	
	public void setComissionamentoMedio(Double comissionamentoMedio) {
		this.comissionamentoMedio = comissionamentoMedio;
	}
	
	public Double getComissionamentoMedioPercentagem() {
		return comissionamentoMedioPercentagem;
	}
	
	public void setComissionamentoMedioPercentagem(Double comissionamentoMedioPercentagem) {
		this.comissionamentoMedioPercentagem = comissionamentoMedioPercentagem;
	}
}
