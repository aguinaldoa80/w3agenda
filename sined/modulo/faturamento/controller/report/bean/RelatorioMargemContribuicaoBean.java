package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Venda;

public class RelatorioMargemContribuicaoBean {

	private String descricao;
	private Money faturamentoBruto;
	private Money deducao;
	private Money receitaLiquida;
	private Money cmv;
	private Money despesas;
	private Money valorMargem;
	private Money percentualMargem;
	private Money resultadoBruto;
	private String semaforo;
	
	private List<Venda> listaVenda;
	private List<Notafiscalproduto> listaNota;
	
	private Money valorGastosEmpresa;
	private Money margemContribuicaoTotal;
	private Money pontoEquilibrioOperacional;
	private Money pontoEquilibrio;
	
	private String idsVenda;
	private String idsNota;
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public Money getFaturamentoBruto() {
		return faturamentoBruto;
	}

	public void setFaturamentoBruto(Money faturamentoBruto) {
		this.faturamentoBruto = faturamentoBruto;
	}

	public Money getDeducao() {
		return deducao;
	}

	public void setDeducao(Money deducao) {
		this.deducao = deducao;
	}

	public Money getReceitaLiquida() {
		return receitaLiquida;
	}

	public void setReceitaLiquida(Money receitaLiquida) {
		this.receitaLiquida = receitaLiquida;
	}

	public Money getCmv() {
		return cmv;
	}
	
	public void setCmv(Money cmv) {
		this.cmv = cmv;
	}

	public Money getDespesas() {
		return despesas;
	}

	public void setDespesas(Money despesas) {
		this.despesas = despesas;
	}

	public Money getValorMargem() {
		return valorMargem;
	}

	public void setValorMargem(Money valorMargem) {
		this.valorMargem = valorMargem;
	}

	public Money getPercentualMargem() {
		return percentualMargem;
	}

	public void setPercentualMargem(Money percentualMargem) {
		this.percentualMargem = percentualMargem;
	}

	public Money getResultadoBruto() {
		return resultadoBruto;
	}

	public void setResultadoBruto(Money resultadoBruto) {
		this.resultadoBruto = resultadoBruto;
	}

	public String getSemaforo() {
		return semaforo;
	}

	public void setSemaforo(String semaforo) {
		this.semaforo = semaforo;
	}
	
	public List<Venda> getListaVenda() {
		return listaVenda;
	}
	
	public void setListaVenda(List<Venda> listaVenda) {
		this.listaVenda = listaVenda;
	}
	
	public List<Notafiscalproduto> getListaNota() {
		return listaNota;
	}
	
	public void setListaNota(List<Notafiscalproduto> listaNota) {
		this.listaNota = listaNota;
	}
	
	public void addFaturamentoBruto(Money faturamentoBruto) {
		if (this.faturamentoBruto == null) {
			this.faturamentoBruto = new Money();
		}
		
		if (faturamentoBruto == null) {
			faturamentoBruto = new Money();
		}
		
		this.faturamentoBruto = this.faturamentoBruto.add(faturamentoBruto);
	}
	
	public void addDeducao(Money deducao) {
		if (this.deducao == null) {
			this.deducao = new Money();
		}
		
		if (deducao == null) {
			deducao = new Money();
		}
		
		this.deducao = this.deducao.add(deducao);
	}
	
	public void addReceitaLiquida(Money receitaLiquida) {
		if (this.receitaLiquida == null) {
			this.receitaLiquida = new Money();
		}
		
		if (receitaLiquida == null) {
			receitaLiquida = new Money();
		}
		
		this.receitaLiquida = this.receitaLiquida.add(receitaLiquida);
	}
	
	public void addCmv(Money cmv) {
		if (this.cmv == null) {
			this.cmv = new Money();
		}
		
		if (cmv == null) {
			cmv = new Money();
		}
		
		this.cmv = this.cmv.add(cmv);
	}
	
	public void addDespesas(Money despesas) {
		if (this.despesas == null) {
			this.despesas = new Money();
		}
		
		if (despesas == null) {
			despesas = new Money();
		}
		
		this.despesas = this.despesas.add(despesas);
	}
	
	public void addValorMargem(Money valorMargem) {
		if (this.valorMargem == null) {
			this.valorMargem = new Money();
		}
		
		if (valorMargem == null) {
			valorMargem = new Money();
		}
		
		this.valorMargem = this.valorMargem.add(valorMargem);
	}
	
	public void addPercentualMargem(Money percentualMargem) {
		if (this.percentualMargem == null) {
			this.percentualMargem = new Money();
		}
		
		if (percentualMargem == null) {
			percentualMargem = new Money();
		}
		
		this.percentualMargem = this.percentualMargem.add(percentualMargem);
	}
	
	public void addResultadoBruto(Money resultadoBruto) {
		if (this.resultadoBruto == null) {
			this.resultadoBruto = new Money();
		}
		
		if (resultadoBruto == null) {
			resultadoBruto = new Money();
		}
		
		this.resultadoBruto = this.resultadoBruto.add(resultadoBruto);
	}
	
	public void addListaVenda(Venda venda) {
		if (listaVenda == null) {
			listaVenda = new ArrayList<Venda>();
		}
		
		listaVenda.add(venda);
	}
	
	public void addListaNota(Notafiscalproduto notaFiscalProduto) {
		if (listaNota == null) {
			listaNota = new ArrayList<Notafiscalproduto>();
		}
		
		listaNota.add(notaFiscalProduto);
	}
	
	public Money getMargemContribuicaoTotal() {
		return margemContribuicaoTotal;
	}
	
	public void setMargemContribuicaoTotal(Money margemContribuicaoTotal) {
		this.margemContribuicaoTotal = margemContribuicaoTotal;
	}
	
	public Money getPontoEquilibrioOperacional() {
		return pontoEquilibrioOperacional;
	}
	
	public void setPontoEquilibrioOperacional(Money pontoEquilibrioOperacional) {
		this.pontoEquilibrioOperacional = pontoEquilibrioOperacional;
	}
	
	public Money getValorGastosEmpresa() {
		return valorGastosEmpresa;
	}
	
	public void setValorGastosEmpresa(Money valorGastosEmpresa) {
		this.valorGastosEmpresa = valorGastosEmpresa;
	}
	
	public String getIdsVenda() {
		return idsVenda;
	}
	
	public void setIdsVenda(String idsVenda) {
		this.idsVenda = idsVenda;
	}
	
	public String getIdsNota() {
		return idsNota;
	}
	
	public void setIdsNota(String idsNota) {
		this.idsNota = idsNota;
	}
	
	public Money getPontoEquilibrio() {
		return pontoEquilibrio;
	}
	
	public void setPontoEquilibrio(Money pontoEquilibrio) {
		this.pontoEquilibrio = pontoEquilibrio;
	}
}
