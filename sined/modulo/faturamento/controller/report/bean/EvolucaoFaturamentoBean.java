package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;

public class EvolucaoFaturamentoBean {

	Date data;
	Double valor;
	
	public Date getData() {
		return data;
	}
	public Double getValor() {
		return valor;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	public void addValor(Money valor){
		if(this.valor == null) this.valor = 0d;
		if(valor == null) valor = new Money();
		this.valor = this.valor + valor.getValue().doubleValue();		
	}
	
}
