package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

public class VendasPorVendedorOutrosFaturamentosBean {

	private String nomeGrupo;
	private Double quantidade;
	private Double precoMedio;
	private Double faturaTotal;
	private Double descontoMedio;
	private Double descontoMedioPercentagem;

	public String getNomeGrupo() {
		return nomeGrupo;
	}

	public void setNomeGrupo(String nomeGrupo) {
		this.nomeGrupo = nomeGrupo;
	}

	public Double getPrecoMedio() {
		return precoMedio;
	}

	public void setPrecoMedio(Double precoMedio) {
		this.precoMedio = precoMedio;
	}

	public Double getFaturaTotal() {
		return faturaTotal;
	}

	public void setFaturaTotal(Double faturaTotal) {
		this.faturaTotal = faturaTotal;
	}

	public Double getDescontoMedio() {
		return descontoMedio;
	}

	public void setDescontoMedio(Double descontoMedio) {
		this.descontoMedio = descontoMedio;
	}

	public Double getDescontoMedioPercentagem() {
		return descontoMedioPercentagem;
	}

	public void setDescontoMedioPercentagem(Double descontoMedioPercentagem) {
		this.descontoMedioPercentagem = descontoMedioPercentagem;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
}
