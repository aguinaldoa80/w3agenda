package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.Date;
import java.util.LinkedList;

import com.ibm.icu.text.SimpleDateFormat;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Valecompra;

public class EmitirComprovanteValeCompraBean {
	
	private Money saldo;
	private String data_emissao;
	private String nome_cliente;
	private String razaosocial_cliente;
	private String cpf_cnpj_cliente;
	private String cdcliente_cliente;
	private String identificador_cliente;
	private String contato_cliente;
	private String email_contato_cliente;
	private String logradouro_cliente;
	private String numero_cliente;
	private String bairro_cliente;
	private String municipio_cliente;
	private String uf_cliente;
	private String telefone_cliente;
	private String email_contato;
	private EmpresaBean empresaBean;
	private LinkedList<ValeCompraBean> listaValeCompraBean;
	
	public EmitirComprovanteValeCompraBean(Cliente cliente, Empresa empresa){
		if(cliente!=null){
			this.setCdcliente_cliente(cliente.getCdpessoa().toString());
			Contato contatoResponsavel = cliente.getContatoResponsavel();
			this.setContato_cliente(contatoResponsavel!=null && contatoResponsavel.getNome()!=null && Boolean.TRUE.equals(contatoResponsavel.getResponsavelos())?
									contatoResponsavel.getNome():
									cliente.getContatos());
			this.setCpf_cnpj_cliente(cliente.getCpfcnpj());
			this.setData_emissao(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
			this.setEmail_contato(cliente.getEmail());
			this.setIdentificador_cliente(cliente.getIdentificador());
			this.setNome_cliente(cliente.getNome());
			this.setRazaosocial_cliente(cliente.getRazaosocial());
			this.setSaldo(cliente.getSaldoValecompra());
			this.setEmail_contato_cliente(cliente.getEmailContatoEspecifico());
					
			Endereco endereco = cliente.getEndereco();
			if (endereco != null) {
				this.setLogradouro_cliente(endereco.getLogradouro());
				this.setNumero_cliente(cliente.getEndereco().getNumero());
				this.setBairro_cliente(cliente.getEndereco().getBairro());
				this.setMunicipio_cliente(cliente.getEndereco().getMunicipio() != null ? cliente.getEndereco().getMunicipio().getNome() : "");
				this.setUf_cliente(cliente.getEndereco().getMunicipio() != null ? cliente.getEndereco().getMunicipio().getUf().getSigla() : "");
			}			
			
			if (cliente.getTelefone() != null) {
				this.setTelefone_cliente(cliente.getTelefone().getTelefone());	
			}
								
						
			this.setListaValeCompraBean(new LinkedList<ValeCompraBean>());
			if(cliente.getListaValecompra()!=null){
				for(Valecompra valeCompra: cliente.getListaValecompra()){
					ValeCompraBean valeCompraBean = new ValeCompraBean(valeCompra);
					this.getListaValeCompraBean().add(valeCompraBean);
				}	
			}
			
			this.setEmpresaBean(new EmpresaBean(empresa));						
		}
	}
	
	public Money getSaldo() {
		return saldo==null? new Money(): saldo;
	}
	public void setSaldo(Money saldo) {
		this.saldo = saldo;
	}
	public String getData_emissao() {
		return data_emissao;
	}

	public void setData_emissao(String data_emissao) {
		this.data_emissao = data_emissao;
	}

	public String getNome_cliente() {
		return Util.strings.emptyIfNull(nome_cliente);
	}
	public void setNome_cliente(String nomeCliente) {
		nome_cliente = nomeCliente;
	}
	public String getRazaosocial_cliente() {
		return razaosocial_cliente;
	}
	public void setRazaosocial_cliente(String razaosocialCliente) {
		razaosocial_cliente = razaosocialCliente;
	}
	public String getCpf_cnpj_cliente() {
		return Util.strings.emptyIfNull(cpf_cnpj_cliente);
	}
	public void setCpf_cnpj_cliente(String cpfCnpjCliente) {
		cpf_cnpj_cliente = cpfCnpjCliente;
	}
	public String getCdcliente_cliente() {
		return Util.strings.emptyIfNull(cdcliente_cliente);
	}
	public void setCdcliente_cliente(String cdclienteCliente) {
		cdcliente_cliente = cdclienteCliente;
	}
	public String getIdentificador_cliente() {
		return Util.strings.emptyIfNull(identificador_cliente);
	}
	public void setIdentificador_cliente(String identificadorCliente) {
		identificador_cliente = identificadorCliente;
	}
	public String getContato_cliente() {
		return Util.strings.emptyIfNull(contato_cliente);
	}
	public void setContato_cliente(String contatoCliente) {
		contato_cliente = contatoCliente;
	}
	public String getEmail_contato() {
		return Util.strings.emptyIfNull(email_contato);
	}
	public void setEmail_contato(String emailContato) {
		email_contato = emailContato;
	}
	
	public String getEmail_contato_cliente() {
		return email_contato_cliente;
	}

	public void setEmail_contato_cliente(String email_contato_cliente) {
		this.email_contato_cliente = email_contato_cliente;
	}

	public String getMunicipio_cliente() {
		return municipio_cliente;
	}

	public void setMunicipio_cliente(String municipio_cliente) {
		this.municipio_cliente = municipio_cliente;
	}

	public String getTelefone_cliente() {
		return telefone_cliente;
	}

	public void setTelefone_cliente(String telefone_cliente) {
		this.telefone_cliente = telefone_cliente;
	}
	public LinkedList<ValeCompraBean> getListaValeCompraBean() {
		return listaValeCompraBean;
	}
	public void setListaValeCompraBean(
			LinkedList<ValeCompraBean> listaValeCompraBean) {
		this.listaValeCompraBean = listaValeCompraBean;
	}

	public EmpresaBean getEmpresaBean() {
		return empresaBean;
	}

	public void setEmpresaBean(EmpresaBean empresaBean) {
		this.empresaBean = empresaBean;
	}

	public String getLogradouro_cliente() {
		return logradouro_cliente;
	}

	public void setLogradouro_cliente(String logradouro_cliente) {
		this.logradouro_cliente = logradouro_cliente;
	}

	public String getNumero_cliente() {
		return numero_cliente;
	}

	public void setNumero_cliente(String numero_cliente) {
		this.numero_cliente = numero_cliente;
	}

	public String getBairro_cliente() {
		return bairro_cliente;
	}

	public void setBairro_cliente(String bairro_cliente) {
		this.bairro_cliente = bairro_cliente;
	}

	public String getUf_cliente() {
		return uf_cliente;
	}

	public void setUf_cliente(String uf_cliente) {
		this.uf_cliente = uf_cliente;
	}
	

	

}
