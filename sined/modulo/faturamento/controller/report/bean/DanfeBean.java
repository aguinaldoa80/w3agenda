package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.util.SinedUtil;

public class DanfeBean {
	
	private Integer numeroPaginas = 0;
	private Integer numeroReg = 0;
	private String emitenteRazao = "";
	private String emitenteEndereco = "";
	private String emitenteBairro = "";
	private String emitenteMunicipio = "";
	private String emitenteUf = "";
	private String emitenteTelefone = "";
	private String emitenteCep = "";
	private String emitenteCnpj = "";
	private String emitenteInscricao = "";
	private String emitenteInscricaoST = "";
	private String chaveAcesso = "";
	private Integer serie = 0;
	private Integer tipoNota = 0;
	private Integer numeroNota = 0;
	private String natOper = "";
	private String inscricaoEstadual = "";
	private String cnpj = "";
	private String razaoSocial = "";
	private String endereco = "";
	private String enderecoCompleto = "";
	private String bairro = "";
	private String cep = "";
	private String municipio = "";
	private String telefone = "";
	private String uf = "";
	private String dataEmissao = "";
	private String dataSaida = "";
	private String horaSaida = "";
	private Double baseCalculoIcms = 0d;
	private Double valorIcms = 0d;
	private Double baseCalculoIcmsSt = 0d;
	private Double valorTotalProdutos = 0d;
	private Double valorFrete = 0d;
	private Double valorDesoneracao = 0d;
	private Double valorSeguro = 0d;
	private Double valorTotalTributos = 0d;
	private Double valorTotalImpostoFederal = 0d;
	private Double valorTotalImpostoEstadual = 0d;
	private Double valorTotalImpostoMunicipal = 0d;
	private Double desconto = 0d;
	private Double despesasAcessorias = 0d;
	private Double valorIpi = 0d;
	private Double valorTotalNota = 0d;
	private String transportadorRazao = "";
	private Integer transportadorTipoFrete = 0;
	private String transportadorCodigoAntt = "";
	private String transportadorPlaca = "";
	private String transportadorUf = "";
	private String transportadorCnpj = "";
	private String transportadorEndereco = "";
	private String transportadorMunicipio = "";
	private String transportadorInscricao = "";
	private Integer transportadorQtde = 0;
	private String transportadorEspecie = "";
	private Double transportadorPesoLiquido = 0d;
	private String infocomplementar = "";
	private String infocomplementarfisco = "";
	private String protocolo = "";
	private String senha = "";
	private Image logo;
	private String transportadorVeiculoUf = "";
	private String inscricaoMunicipal = "";
	private String inscricaoEstadualEmpresa = "";
	private Double valorTotalServicos = 0d;
	private Double baseCalculoIss = 0d;
	private Double valorIss = 0d;
	private Double valorIcmsSt = 0d;
	private String transportadorMarca = "";
	private Double transportadorPesoBruto = 0d;
	private String transportadorNumeracao = "";
	private Integer entradaSaida = 0;
	private String formapagamento = "";
	private String site = "";
	private String email = "";
	
	private String numeroFatura;
	private Double valorOriginalFatura;
	private Double valorDescontoFatura;
	private Double valorLiquidoFatura;
	
	private String numeroDuplicata1;
	private String numeroDuplicata2;
	private String numeroDuplicata3;
	private String numeroDuplicata4;
	private String numeroDuplicata5;
	private String numeroDuplicata6;
	private String numeroDuplicata7;
	private String numeroDuplicata8;
	private String numeroDuplicata9;
	private String numeroDuplicata10;
	
	private Date vencimentoDuplicata1;
	private Date vencimentoDuplicata2;
	private Date vencimentoDuplicata3;
	private Date vencimentoDuplicata4;
	private Date vencimentoDuplicata5;
	private Date vencimentoDuplicata6;
	private Date vencimentoDuplicata7;
	private Date vencimentoDuplicata8;
	private Date vencimentoDuplicata9;
	private Date vencimentoDuplicata10;
	
	private Double valorDuplicata1;
	private Double valorDuplicata2;
	private Double valorDuplicata3;
	private Double valorDuplicata4;
	private Double valorDuplicata5;
	private Double valorDuplicata6;
	private Double valorDuplicata7;
	private Double valorDuplicata8;
	private Double valorDuplicata9;
	private Double valorDuplicata10;
	
	private String textoDuplicatas;
	
	private String notaStatus;
	private String inscricaoMunicipalEstadualCupom;
	
	private String cabecalhoTipoTributacaoIcms = "CST / CSON";
	
	private String dadostef;
	private String comprovantetef;
	
	private LinkedList<DanfeItemBean> listaItens = new LinkedList<DanfeItemBean>();
	private List<DanfeLegendaBean> listaLegenda = new ArrayList<DanfeLegendaBean>();
	
	private Boolean tributadoIssqn;
	
	private Image qrCode;
	private String url = "";
	
	public Integer getNumeroPaginas() {
		return numeroPaginas;
	}
	public Integer getNumeroReg() {
		return numeroReg;
	}
	public String getEmitenteRazao() {
		return emitenteRazao;
	}
	public String getEmitenteEndereco() {
		return emitenteEndereco;
	}
	public String getEmitenteBairro() {
		return emitenteBairro;
	}
	public String getEmitenteMunicipio() {
		return emitenteMunicipio;
	}
	public String getEmitenteUf() {
		return emitenteUf;
	}
	public String getEmitenteTelefone() {
		return emitenteTelefone;
	}
	public String getEmitenteCep() {
		return emitenteCep;
	}
	public String getEmitenteCnpj() {
		return emitenteCnpj;
	}
	public String getEmitenteInscricao() {
		return emitenteInscricao;
	}
	public String getEmitenteInscricaoST() {
		return emitenteInscricaoST;
	}
	public String getChaveAcesso() {
		return chaveAcesso;
	}
	public Integer getSerie() {
		return serie;
	}
	public Integer getTipoNota() {
		return tipoNota;
	}
	public Integer getNumeroNota() {
		return numeroNota;
	}
	public String getNatOper() {
		return natOper;
	}
	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}
	public String getCnpj() {
		return cnpj;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public String getEndereco() {
		return endereco;
	}
	public String getEnderecoCompleto() {
		return enderecoCompleto;
	}
	public String getBairro() {
		return bairro;
	}
	public String getCep() {
		return cep;
	}
	public String getMunicipio() {
		return municipio;
	}
	public String getTelefone() {
		return telefone;
	}
	public String getUf() {
		return uf;
	}
	public String getDataEmissao() {
		return dataEmissao;
	}
	public String getDataSaida() {
		return dataSaida;
	}
	public String getHoraSaida() {
		return horaSaida;
	}
	public Double getBaseCalculoIcms() {
		return baseCalculoIcms;
	}
	public Double getValorIcms() {
		return valorIcms;
	}
	public Double getBaseCalculoIcmsSt() {
		return baseCalculoIcmsSt;
	}
	public Double getValorTotalProdutos() {
		return valorTotalProdutos;
	}
	public Double getValorFrete() {
		return valorFrete;
	}
	public Double getValorSeguro() {
		return valorSeguro;
	}
	public Double getDesconto() {
		return desconto;
	}
	public Double getDespesasAcessorias() {
		return despesasAcessorias;
	}
	public Double getValorIpi() {
		return valorIpi;
	}
	public Double getValorTotalNota() {
		return valorTotalNota;
	}
	public String getTransportadorRazao() {
		return transportadorRazao;
	}
	public Integer getTransportadorTipoFrete() {
		return transportadorTipoFrete;
	}
	public String getTransportadorCodigoAntt() {
		return transportadorCodigoAntt;
	}
	public String getTransportadorPlaca() {
		return transportadorPlaca;
	}
	public String getTransportadorUf() {
		return transportadorUf;
	}
	public String getTransportadorCnpj() {
		return transportadorCnpj;
	}
	public String getTransportadorEndereco() {
		return transportadorEndereco;
	}
	public String getTransportadorMunicipio() {
		return transportadorMunicipio;
	}
	public String getTransportadorInscricao() {
		return transportadorInscricao;
	}
	public Integer getTransportadorQtde() {
		return transportadorQtde;
	}
	public String getTransportadorEspecie() {
		return transportadorEspecie;
	}
	public Double getTransportadorPesoLiquido() {
		return transportadorPesoLiquido;
	}
	public String getInfocomplementar() {
		return infocomplementar;
	}
	public String getProtocolo() {
		return protocolo;
	}
	public String getSenha() {
		return senha;
	}
	public Image getLogo() {
		return logo;
	}
	public List<DanfeItemBean> getListaItens() {
		return listaItens;
	}
	public String getTransportadorVeiculoUf() {
		return transportadorVeiculoUf;
	}
	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}
	public String getInscricaoEstadualEmpresa() {
		return inscricaoEstadualEmpresa;
	}
	public Double getValorTotalServicos() {
		return valorTotalServicos;
	}
	public Double getBaseCalculoIss() {
		return baseCalculoIss;
	}
	public Double getValorIss() {
		return valorIss;
	}
	public Double getValorIcmsSt() {
		return valorIcmsSt;
	}
	public String getTransportadorMarca() {
		return transportadorMarca;
	}
	public Double getTransportadorPesoBruto() {
		return transportadorPesoBruto;
	}
	public String getTransportadorNumeracao() {
		return transportadorNumeracao;
	}
	public Integer getEntradaSaida() {
		return entradaSaida;
	}
	public String getFormapagamento() {
		return formapagamento;
	}
	public String getNumeroFatura() {
		return numeroFatura;
	}
	public Double getValorOriginalFatura() {
		return valorOriginalFatura;
	}
	public Double getValorDescontoFatura() {
		return valorDescontoFatura;
	}
	public Double getValorLiquidoFatura() {
		return valorLiquidoFatura;
	}
	public String getNumeroDuplicata1() {
		return numeroDuplicata1;
	}
	public Date getVencimentoDuplicata1() {
		return vencimentoDuplicata1;
	}
	public Double getValorDuplicata1() {
		return valorDuplicata1;
	}
	public String getNumeroDuplicata2() {
		return numeroDuplicata2;
	}
	public Date getVencimentoDuplicata2() {
		return vencimentoDuplicata2;
	}
	public Double getValorDuplicata2() {
		return valorDuplicata2;
	}
	public String getNumeroDuplicata3() {
		return numeroDuplicata3;
	}
	public Date getVencimentoDuplicata3() {
		return vencimentoDuplicata3;
	}
	public Double getValorDuplicata3() {
		return valorDuplicata3;
	}
	public String getNumeroDuplicata4() {
		return numeroDuplicata4;
	}
	public Date getVencimentoDuplicata4() {
		return vencimentoDuplicata4;
	}
	public Double getValorDuplicata4() {
		return valorDuplicata4;
	}
	public String getCabecalhoTipoTributacaoIcms() {
		return cabecalhoTipoTributacaoIcms;
	}
	public String getNumeroDuplicata5() {
		return numeroDuplicata5;
	}
	public String getNumeroDuplicata6() {
		return numeroDuplicata6;
	}
	public String getNumeroDuplicata7() {
		return numeroDuplicata7;
	}
	public String getNumeroDuplicata8() {
		return numeroDuplicata8;
	}
	public String getNumeroDuplicata9() {
		return numeroDuplicata9;
	}
	public String getNumeroDuplicata10() {
		return numeroDuplicata10;
	}
	public Date getVencimentoDuplicata5() {
		return vencimentoDuplicata5;
	}
	public Date getVencimentoDuplicata6() {
		return vencimentoDuplicata6;
	}
	public Date getVencimentoDuplicata7() {
		return vencimentoDuplicata7;
	}
	public Date getVencimentoDuplicata8() {
		return vencimentoDuplicata8;
	}
	public Date getVencimentoDuplicata9() {
		return vencimentoDuplicata9;
	}
	public Date getVencimentoDuplicata10() {
		return vencimentoDuplicata10;
	}
	public Double getValorDuplicata5() {
		return valorDuplicata5;
	}
	public Double getValorDuplicata6() {
		return valorDuplicata6;
	}
	public Double getValorDuplicata7() {
		return valorDuplicata7;
	}
	public Double getValorDuplicata8() {
		return valorDuplicata8;
	}
	public Double getValorDuplicata9() {
		return valorDuplicata9;
	}
	public Double getValorDuplicata10() {
		return valorDuplicata10;
	}
	public String getTextoDuplicatas() {
		return textoDuplicatas;
	}
	public Double getValorTotalTributos() {
		return valorTotalTributos;
	}
	public Double getValorTotalImpostoFederal() {
		return valorTotalImpostoFederal;
	}
	public String getDadostef() {
		return dadostef;
	}
	public String getComprovantetef() {
		return comprovantetef;
	}
	public Double getValorDesoneracao() {
		return valorDesoneracao;
	}
	public void setValorDesoneracao(Double valorDesoneracao) {
		this.valorDesoneracao = valorDesoneracao;
	}
	public void setComprovantetef(String comprovantetef) {
		this.comprovantetef = comprovantetef;
	}
	public void setDadostef(String dadostef) {
		this.dadostef = dadostef;
	}
	public void setValorTotalImpostoFederal(Double valorTotalImpostoFederal) {
		this.valorTotalImpostoFederal = valorTotalImpostoFederal;
	}
	public Double getValorTotalImpostoEstadual() {
		return valorTotalImpostoEstadual;
	}
	public void setValorTotalImpostoEstadual(Double valorTotalImpostoEstadual) {
		this.valorTotalImpostoEstadual = valorTotalImpostoEstadual;
	}
	public Double getValorTotalImpostoMunicipal() {
		return valorTotalImpostoMunicipal;
	}
	public void setValorTotalImpostoMunicipal(Double valorTotalImpostoMunicipal) {
		this.valorTotalImpostoMunicipal = valorTotalImpostoMunicipal;
	}
	public void setValorTotalTributos(Double valorTotalTributos) {
		this.valorTotalTributos = valorTotalTributos;
	}
	public void setNumeroDuplicata5(String numeroDuplicata5) {
		this.numeroDuplicata5 = numeroDuplicata5;
	}
	public void setNumeroDuplicata6(String numeroDuplicata6) {
		this.numeroDuplicata6 = numeroDuplicata6;
	}
	public void setNumeroDuplicata7(String numeroDuplicata7) {
		this.numeroDuplicata7 = numeroDuplicata7;
	}
	public void setNumeroDuplicata8(String numeroDuplicata8) {
		this.numeroDuplicata8 = numeroDuplicata8;
	}
	public void setNumeroDuplicata9(String numeroDuplicata9) {
		this.numeroDuplicata9 = numeroDuplicata9;
	}
	public void setNumeroDuplicata10(String numeroDuplicata10) {
		this.numeroDuplicata10 = numeroDuplicata10;
	}
	public void setVencimentoDuplicata5(Date vencimentoDuplicata5) {
		this.vencimentoDuplicata5 = vencimentoDuplicata5;
	}
	public void setVencimentoDuplicata6(Date vencimentoDuplicata6) {
		this.vencimentoDuplicata6 = vencimentoDuplicata6;
	}
	public void setVencimentoDuplicata7(Date vencimentoDuplicata7) {
		this.vencimentoDuplicata7 = vencimentoDuplicata7;
	}
	public void setVencimentoDuplicata8(Date vencimentoDuplicata8) {
		this.vencimentoDuplicata8 = vencimentoDuplicata8;
	}
	public void setVencimentoDuplicata9(Date vencimentoDuplicata9) {
		this.vencimentoDuplicata9 = vencimentoDuplicata9;
	}
	public void setVencimentoDuplicata10(Date vencimentoDuplicata10) {
		this.vencimentoDuplicata10 = vencimentoDuplicata10;
	}
	public void setValorDuplicata5(Double valorDuplicata5) {
		this.valorDuplicata5 = valorDuplicata5;
	}
	public void setValorDuplicata6(Double valorDuplicata6) {
		this.valorDuplicata6 = valorDuplicata6;
	}
	public void setValorDuplicata7(Double valorDuplicata7) {
		this.valorDuplicata7 = valorDuplicata7;
	}
	public void setValorDuplicata8(Double valorDuplicata8) {
		this.valorDuplicata8 = valorDuplicata8;
	}
	public void setValorDuplicata9(Double valorDuplicata9) {
		this.valorDuplicata9 = valorDuplicata9;
	}
	public void setValorDuplicata10(Double valorDuplicata10) {
		this.valorDuplicata10 = valorDuplicata10;
	}
	public void setTextoDuplicatas(String textoDuplicatas) {
		this.textoDuplicatas = textoDuplicatas;
	}
	public void setCabecalhoTipoTributacaoIcms(
			String cabecalhoTipoTributacaoIcms) {
		this.cabecalhoTipoTributacaoIcms = cabecalhoTipoTributacaoIcms;
	}
	public void setNumeroFatura(String numeroFatura) {
		this.numeroFatura = numeroFatura;
	}
	public void setValorOriginalFatura(Double valorOriginalFatura) {
		this.valorOriginalFatura = valorOriginalFatura;
	}
	public void setValorDescontoFatura(Double valorDescontoFatura) {
		this.valorDescontoFatura = valorDescontoFatura;
	}
	public void setValorLiquidoFatura(Double valorLiquidoFatura) {
		this.valorLiquidoFatura = valorLiquidoFatura;
	}
	public void setNumeroDuplicata1(String numeroDuplicata1) {
		this.numeroDuplicata1 = numeroDuplicata1;
	}
	public void setVencimentoDuplicata1(Date vencimentoDuplicata1) {
		this.vencimentoDuplicata1 = vencimentoDuplicata1;
	}
	public void setValorDuplicata1(Double valorDuplicata1) {
		this.valorDuplicata1 = valorDuplicata1;
	}
	public void setNumeroDuplicata2(String numeroDuplicata2) {
		this.numeroDuplicata2 = numeroDuplicata2;
	}
	public void setVencimentoDuplicata2(Date vencimentoDuplicata2) {
		this.vencimentoDuplicata2 = vencimentoDuplicata2;
	}
	public void setValorDuplicata2(Double valorDuplicata2) {
		this.valorDuplicata2 = valorDuplicata2;
	}
	public void setNumeroDuplicata3(String numeroDuplicata3) {
		this.numeroDuplicata3 = numeroDuplicata3;
	}
	public void setVencimentoDuplicata3(Date vencimentoDuplicata3) {
		this.vencimentoDuplicata3 = vencimentoDuplicata3;
	}
	public void setValorDuplicata3(Double valorDuplicata3) {
		this.valorDuplicata3 = valorDuplicata3;
	}
	public void setNumeroDuplicata4(String numeroDuplicata4) {
		this.numeroDuplicata4 = numeroDuplicata4;
	}
	public void setVencimentoDuplicata4(Date vencimentoDuplicata4) {
		this.vencimentoDuplicata4 = vencimentoDuplicata4;
	}
	public void setValorDuplicata4(Double valorDuplicata4) {
		this.valorDuplicata4 = valorDuplicata4;
	}
	public void setFormapagamento(String formapagamento) {
		this.formapagamento = formapagamento;
	}
	public void setEntradaSaida(Integer entradaSaida) {
		this.entradaSaida = entradaSaida;
	}
	public void setTransportadorMarca(String transportadorMarca) {
		this.transportadorMarca = transportadorMarca;
	}
	public void setTransportadorPesoBruto(Double transportadorPesoBruto) {
		this.transportadorPesoBruto = transportadorPesoBruto;
	}
	public void setTransportadorNumeracao(String transportadorNumeracao) {
		this.transportadorNumeracao = transportadorNumeracao;
	}
	public void setValorIcmsSt(Double valorIcmsSt) {
		this.valorIcmsSt = valorIcmsSt;
	}
	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}
	public void setInscricaoEstadualEmpresa(String inscricaoEstadualEmpresa) {
		this.inscricaoEstadualEmpresa = inscricaoEstadualEmpresa;
	}
	public void setValorTotalServicos(Double valorTotalServicos) {
		this.valorTotalServicos = valorTotalServicos;
	}
	public void setBaseCalculoIss(Double baseCalculoIss) {
		this.baseCalculoIss = baseCalculoIss;
	}
	public void setValorIss(Double valorIss) {
		this.valorIss = valorIss;
	}
	public void setTransportadorVeiculoUf(String transportadorVeiculoUf) {
		this.transportadorVeiculoUf = transportadorVeiculoUf;
	}
	public void setListaItens(LinkedList<DanfeItemBean> listaItens) {
		this.listaItens = listaItens;
	}
	public void setNumeroPaginas(Integer numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}
	public void setNumeroReg(Integer numeroReg) {
		this.numeroReg = numeroReg;
	}
	public void setEmitenteRazao(String emitenteRazao) {
		this.emitenteRazao = emitenteRazao;
	}
	public void setEmitenteEndereco(String emitenteEndereco) {
		this.emitenteEndereco = emitenteEndereco;
	}
	public void setEmitenteBairro(String emitenteBairro) {
		this.emitenteBairro = emitenteBairro;
	}
	public void setEmitenteMunicipio(String emitenteMunicipio) {
		this.emitenteMunicipio = emitenteMunicipio;
	}
	public void setEmitenteUf(String emitenteUf) {
		this.emitenteUf = emitenteUf;
	}
	public void setEmitenteTelefone(String emitenteTelefone) {
		this.emitenteTelefone = emitenteTelefone;
	}
	public void setEmitenteCep(String emitenteCep) {
		this.emitenteCep = emitenteCep;
	}
	public void setEmitenteCnpj(String emitenteCnpj) {
		this.emitenteCnpj = emitenteCnpj;
	}
	public void setEmitenteInscricao(String emitenteInscricao) {
		this.emitenteInscricao = emitenteInscricao;
	}
	public void setEmitenteInscricaoST(String emitenteInscricaoST) {
		this.emitenteInscricaoST = emitenteInscricaoST;
	}
	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}
	public void setSerie(Integer serie) {
		this.serie = serie;
	}
	public void setTipoNota(Integer tipoNota) {
		this.tipoNota = tipoNota;
	}
	public void setNumeroNota(Integer numeroNota) {
		this.numeroNota = numeroNota;
	}
	public void setNatOper(String natOper) {
		this.natOper = natOper;
	}
	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public void setEnderecoCompleto(String enderecoCompleto) {
		this.enderecoCompleto = enderecoCompleto;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	public void setDataSaida(String dataSaida) {
		this.dataSaida = dataSaida;
	}
	public void setHoraSaida(String horaSaida) {
		this.horaSaida = horaSaida;
	}
	public void setBaseCalculoIcms(Double baseCalculoIcms) {
		this.baseCalculoIcms = baseCalculoIcms;
	}
	public void setValorIcms(Double valorIcms) {
		this.valorIcms = valorIcms;
	}
	public void setBaseCalculoIcmsSt(Double baseCalculoIcmsSt) {
		this.baseCalculoIcmsSt = baseCalculoIcmsSt;
	}
	public void setValorTotalProdutos(Double valorTotalProdutos) {
		this.valorTotalProdutos = valorTotalProdutos;
	}
	public void setValorFrete(Double valorFrete) {
		this.valorFrete = valorFrete;
	}
	public void setValorSeguro(Double valorSeguro) {
		this.valorSeguro = valorSeguro;
	}
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	public void setDespesasAcessorias(Double despesasAcessorias) {
		this.despesasAcessorias = despesasAcessorias;
	}
	public void setValorIpi(Double valorIpi) {
		this.valorIpi = valorIpi;
	}
	public void setValorTotalNota(Double valorTotalNota) {
		this.valorTotalNota = valorTotalNota;
	}
	public void setTransportadorRazao(String transportadorRazao) {
		this.transportadorRazao = transportadorRazao;
	}
	public void setTransportadorTipoFrete(Integer transportadorTipoFrete) {
		this.transportadorTipoFrete = transportadorTipoFrete;
	}
	public void setTransportadorCodigoAntt(String transportadorCodigoAntt) {
		this.transportadorCodigoAntt = transportadorCodigoAntt;
	}
	public void setTransportadorPlaca(String transportadorPlaca) {
		this.transportadorPlaca = transportadorPlaca;
	}
	public void setTransportadorUf(String transportadorUf) {
		this.transportadorUf = transportadorUf;
	}
	public void setTransportadorCnpj(String transportadorCnpj) {
		this.transportadorCnpj = transportadorCnpj;
	}
	public void setTransportadorEndereco(String transportadorEndereco) {
		this.transportadorEndereco = transportadorEndereco;
	}
	public void setTransportadorMunicipio(String transportadorMunicipio) {
		this.transportadorMunicipio = transportadorMunicipio;
	}
	public void setTransportadorInscricao(String transportadorInscricao) {
		this.transportadorInscricao = transportadorInscricao;
	}
	public void setTransportadorQtde(Integer transportadorQtde) {
		this.transportadorQtde = transportadorQtde;
	}
	public void setTransportadorEspecie(String transportadorEspecie) {
		this.transportadorEspecie = transportadorEspecie;
	}
	public void setTransportadorPesoLiquido(Double transportadorPesoLiquido) {
		this.transportadorPesoLiquido = transportadorPesoLiquido;
	}
	public void setInfocomplementar(String infocomplementar) {
		this.infocomplementar = infocomplementar;
	}
	public void setProtocolo(String protocolo) {
		this.protocolo = protocolo;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public void setLogo(Image logo) {
		this.logo = logo;
	}
	
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getInfocomplementarfisco() {
		return infocomplementarfisco;
	}
	public void setInfocomplementarfisco(String infocomplementarfisco) {
		this.infocomplementarfisco = infocomplementarfisco;
	}
	
	public String getNotaStatus() {
		return notaStatus;
	}
	public void setNotaStatus(String notaStatus) {
		this.notaStatus = notaStatus;
	}
	
	public String getInscricaoMunicipalEstadualCupom() {
		String s = null;
		
		if(StringUtils.isNotBlank(getInscricaoMunicipal())){
			s = "I.M.: " + getInscricaoMunicipal();
		}
		if(StringUtils.isNotBlank(getInscricaoEstadualEmpresa())){
			if(s != null) s += " ";
			s = "I.E.: " + getInscricaoEstadualEmpresa();
		}
		
		return s;
	}
	public void setInscricaoMunicipalEstadualCupom(String inscricaoMunicipalEstadualCupom) {
		this.inscricaoMunicipalEstadualCupom = inscricaoMunicipalEstadualCupom;
	}
	
	public List<DanfeLegendaBean> getListaLegenda() {
		return listaLegenda;
	}
	
	public void setListaLegenda(List<DanfeLegendaBean> listaLegenda) {
		this.listaLegenda = listaLegenda;
	}
	
	public Boolean getTributadoIssqn() {
		return tributadoIssqn;
	}
	
	public void setTributadoIssqn(Boolean tributadoIssqn) {
		this.tributadoIssqn = tributadoIssqn;
	}
	
	public Image getQrCode() {
		return qrCode;
	}
	
	public void setQrCode(Image qrCode) {
		this.qrCode = qrCode;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public Report makeReport(String modelo_da_danfe){
		Report report = new Report("/faturamento/danfe" + (StringUtils.isNotBlank(modelo_da_danfe) ? "_"+modelo_da_danfe : ""));
		Report sub_legenda = new Report("/faturamento/danfe_sub_legenda");
		
		report.addSubReport("SUB_DANFE_LEGENDA", sub_legenda);
		
		report.addParameter("formapagamento", formapagamento);
		report.addParameter("numeroPaginas", numeroPaginas);
		report.addParameter("numeroReg", numeroReg);
		report.addParameter("emitenteRazao", emitenteRazao);
		report.addParameter("emitenteEndereco", emitenteEndereco);
		report.addParameter("emitenteBairro", emitenteBairro);
		report.addParameter("emitenteMunicipio", emitenteMunicipio);
		report.addParameter("emitenteUf", emitenteUf);
		report.addParameter("emitenteTelefone", emitenteTelefone);
		report.addParameter("emitenteCep", emitenteCep);
		report.addParameter("emitenteCnpj", emitenteCnpj);
		report.addParameter("emitenteInscricao", emitenteInscricao);
		report.addParameter("emitenteInscricaoST", (emitenteInscricaoST != null ? emitenteInscricaoST : ""));
		report.addParameter("chaveAcesso", chaveAcesso);
		report.addParameter("serie", serie);
		report.addParameter("tipoNota", tipoNota);
		report.addParameter("numeroNota", numeroNota);
		report.addParameter("natOper", natOper);
		report.addParameter("inscricaoEstadual", inscricaoEstadual);
		report.addParameter("cnpj", cnpj);
		report.addParameter("razaoSocial", razaoSocial);
		report.addParameter("endereco", endereco);
		report.addParameter("enderecoCompleto", enderecoCompleto);
		report.addParameter("bairro", bairro);
		report.addParameter("cep", cep);
		report.addParameter("municipio", municipio);
		report.addParameter("telefone", telefone);
		report.addParameter("uf", uf);
		report.addParameter("dataEmissao", dataEmissao);
		report.addParameter("dataSaida", dataSaida);
		report.addParameter("horaSaida", horaSaida);
		report.addParameter("baseCalculoIcms", baseCalculoIcms);
		report.addParameter("valorIcms", valorIcms);
		report.addParameter("baseCalculoIcmsSt", baseCalculoIcmsSt);
		report.addParameter("valorTotalProdutos", valorTotalProdutos);
		report.addParameter("valorFrete", valorFrete);
		report.addParameter("valorDesoneracao", valorDesoneracao);
		report.addParameter("valorSeguro", valorSeguro);
		report.addParameter("desconto", desconto);
		report.addParameter("despesasAcessorias", despesasAcessorias);
		report.addParameter("valorIpi", valorIpi);
		report.addParameter("valorTotalNota", valorTotalNota);
		report.addParameter("transportadorRazao", transportadorRazao);
		report.addParameter("transportadorTipoFrete", transportadorTipoFrete);
		report.addParameter("transportadorCodigoAntt", transportadorCodigoAntt);
		report.addParameter("transportadorPlaca", transportadorPlaca);
		report.addParameter("transportadorUf", transportadorUf);
		report.addParameter("transportadorCnpj", transportadorCnpj);
		report.addParameter("transportadorEndereco", transportadorEndereco);
		report.addParameter("transportadorMunicipio", transportadorMunicipio);
		report.addParameter("transportadorInscricao", transportadorInscricao);
		report.addParameter("transportadorQtde", transportadorQtde);
		report.addParameter("transportadorEspecie", transportadorEspecie);
		report.addParameter("transportadorPesoLiquido", transportadorPesoLiquido);
		report.addParameter("infocomplementar", infocomplementar);
		report.addParameter("infocomplementarfisco", infocomplementarfisco);
		report.addParameter("protocolo", protocolo);
		report.addParameter("senha", senha);
		report.addParameter("logo", logo);
		report.addParameter("transportadorVeiculoUf", transportadorVeiculoUf);
		report.addParameter("inscricaoMunicipal", inscricaoMunicipal);
		report.addParameter("valorTotalServicos", valorTotalServicos);
		report.addParameter("baseCalculoIss", baseCalculoIss);
		report.addParameter("valorIss", valorIss);
		report.addParameter("valorIcmsSt", valorIcmsSt);
		report.addParameter("valorTotalTributos", valorTotalTributos);
		report.addParameter("valorTotalImpostoFederal", valorTotalImpostoFederal);
		report.addParameter("valorTotalImpostoEstadual", valorTotalImpostoEstadual);
		report.addParameter("valorTotalImpostoMunicipal", valorTotalImpostoMunicipal);
		report.addParameter("transportadorMarca", transportadorMarca);
		report.addParameter("transportadorPesoBruto", transportadorPesoBruto);
		report.addParameter("transportadorNumeracao", transportadorNumeracao);
		report.addParameter("entradaSaida", entradaSaida);
		report.addParameter("numeroFatura", numeroFatura);
		report.addParameter("valorOriginalFatura", valorOriginalFatura);
		report.addParameter("valorDescontoFatura", valorDescontoFatura);
		report.addParameter("valorLiquidoFatura", valorLiquidoFatura);
		report.addParameter("numeroDuplicata1", numeroDuplicata1);
		report.addParameter("numeroDuplicata2", numeroDuplicata2);
		report.addParameter("numeroDuplicata3", numeroDuplicata3);
		report.addParameter("numeroDuplicata4", numeroDuplicata4);
		report.addParameter("numeroDuplicata5", numeroDuplicata5);
		report.addParameter("numeroDuplicata6", numeroDuplicata6);
		report.addParameter("numeroDuplicata7", numeroDuplicata7);
		report.addParameter("numeroDuplicata8", numeroDuplicata8);
		report.addParameter("numeroDuplicata9", numeroDuplicata9);
		report.addParameter("numeroDuplicata10", numeroDuplicata10);
		report.addParameter("vencimentoDuplicata1", vencimentoDuplicata1);
		report.addParameter("vencimentoDuplicata2", vencimentoDuplicata2);
		report.addParameter("vencimentoDuplicata3", vencimentoDuplicata3);
		report.addParameter("vencimentoDuplicata4", vencimentoDuplicata4);
		report.addParameter("vencimentoDuplicata5", vencimentoDuplicata5);
		report.addParameter("vencimentoDuplicata6", vencimentoDuplicata6);
		report.addParameter("vencimentoDuplicata7", vencimentoDuplicata7);
		report.addParameter("vencimentoDuplicata8", vencimentoDuplicata8);
		report.addParameter("vencimentoDuplicata9", vencimentoDuplicata9);
		report.addParameter("vencimentoDuplicata10", vencimentoDuplicata10);
		report.addParameter("valorDuplicata1", valorDuplicata1);
		report.addParameter("valorDuplicata2", valorDuplicata2);
		report.addParameter("valorDuplicata3", valorDuplicata3);
		report.addParameter("valorDuplicata4", valorDuplicata4);
		report.addParameter("valorDuplicata5", valorDuplicata5);
		report.addParameter("valorDuplicata6", valorDuplicata6);
		report.addParameter("valorDuplicata7", valorDuplicata7);
		report.addParameter("valorDuplicata8", valorDuplicata8);
		report.addParameter("valorDuplicata9", valorDuplicata9);
		report.addParameter("valorDuplicata10", valorDuplicata10);
		report.addParameter("textoDuplicatas", textoDuplicatas);
		report.addParameter("comprovantetef", comprovantetef);
		report.addParameter("site", !"".equals(site) ? site : null);
		report.addParameter("email", !"".equals(email) ? email : null);
		report.addParameter("cabecalhoTipoTributacaoIcms", cabecalhoTipoTributacaoIcms);
		report.addParameter("notaStatus", notaStatus);
		report.addParameter("listaLegenda", listaLegenda);
		report.addParameter("tributadoIssqn", tributadoIssqn);
		report.addParameter("qrCode", qrCode);
		report.addParameter("url", url);
		report.addParameter("inscricaoMunicipalEstadualCupom", getInscricaoMunicipalEstadualCupom());
		if (ParametrogeralService.getInstance().getBoolean("EXIBIR_LOGO_LINKCOM")) {
			report.addParameter("LOGO_LINKCOM", SinedUtil.getLogoLinkCom());
		}
		
		//TODO infoadicional 
		
		report.setDataSource(listaItens);
		
		return report;
	}

}
