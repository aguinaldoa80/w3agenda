package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.ArrayList;
import java.util.List;

public class EmitircurvaabcvendaReportBean {
	
	private Double quantidade = 0D;
	private Double peso = 0D;
	private Double total = 0D;
	
	public Double getQuantidade() {
		return quantidade;
	}
	public Double getPeso() {
		return peso;
	}
	public Double getTotal() {
		return total;
	}
	public static List<EmitircurvaabcvendaReportBean> getLista(int total){
		 List<EmitircurvaabcvendaReportBean> lista = new ArrayList<EmitircurvaabcvendaReportBean>();
		 for (int i=1; i<=total; i++){
			 lista.add(new EmitircurvaabcvendaReportBean());
		 }
		 return lista;
	}
	
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	public void setTotal(Double total) {
		this.total = total;
	}	
}
