package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.util.SinedUtil;

public class ApuracaovendaBean {
	
	protected Materialgrupo materialgrupo;
	protected Material material;
	protected Unidademedida unidademedida;
	
	protected Double quantidadeOriginal;
	protected Double quantidade;
	protected Double valorcusto;
	protected Double valorlucro;
	protected Double valorvenda;
	protected Money desconto;
	protected Money frete;
	
	protected Double totalcusto;
	protected Double totalvenda;
	
	protected Double percentualquantidade;
	protected Double percentualvalorcusto;
	protected Double percentualvalorlucro;
	protected Double percentualvalorvenda;
	
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	public Material getMaterial() {
		return material;
	}
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	public Double getValorcusto() {
		return valorcusto;
	}
	public Double getValorlucro() {
		return valorlucro;
	}
	public Double getValorvenda() {
		return valorvenda;
	}
	public Double getTotalcusto() {
		return totalcusto;
	}
	public Double getTotalvenda() {
		return totalvenda;
	}
	public Double getPercentualquantidade() {
		return percentualquantidade;
	}
	public Double getPercentualvalorcusto() {
		return percentualvalorcusto;
	}
	public Double getPercentualvalorlucro() {
		return percentualvalorlucro;
	}
	public Double getPercentualvalorvenda() {
		return percentualvalorvenda;
	}
	public Money getDesconto() {
		return desconto;
	}
	public Money getFrete() {
		return frete;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Double getQuantidadeOriginal() {
		return quantidadeOriginal;
	}
	public void setQuantidadeOriginal(Double quantidadeOriginal) {
		this.quantidadeOriginal = quantidadeOriginal;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
	public void setFrete(Money frete) {
		this.frete = frete;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setValorcusto(Double valorcusto) {
		this.valorcusto = valorcusto;
	}
	public void setValorlucro(Double valorlucro) {
		this.valorlucro = valorlucro;
	}
	public void setValorvenda(Double valorvenda) {
		this.valorvenda = valorvenda;
	}
	public void setTotalcusto(Double totalcusto) {
		this.totalcusto = totalcusto;
	}
	public void setTotalvenda(Double totalvenda) {
		this.totalvenda = totalvenda;
	}
	public void setPercentualquantidade(Double percentualquantidade) {
		this.percentualquantidade = percentualquantidade;
	}
	public void setPercentualvalorcusto(Double percentualvalorcusto) {
		this.percentualvalorcusto = percentualvalorcusto;
	}
	public void setPercentualvalorlucro(Double percentualvalorlucro) {
		this.percentualvalorlucro = percentualvalorlucro;
	}
	public void setPercentualvalorvenda(Double percentualvalorvenda) {
		this.percentualvalorvenda = percentualvalorvenda;
	}
	
	public void addDesconto(Money desconto) {
		if(this.desconto == null) this.desconto = new Money(); 
		if(desconto != null){
			this.desconto = this.desconto.add(desconto);
		}
	}
	
	public void addFrete(Money frete) {
		if(this.frete == null) this.frete = new Money(); 
		if(frete != null){
			this.frete = this.frete.add(frete);
		}
	}
	
	public void addQtdeOriginal(Double quantidadeOriginal) {
		if(this.quantidadeOriginal == null) this.quantidadeOriginal = 0d;
		if(quantidadeOriginal != null){
			this.quantidadeOriginal += quantidadeOriginal;
		}
	}
	
	public void addQtde(Double quantidade) {
		if(this.quantidade == null) this.quantidade = 0d;
		if(quantidade != null){
			this.quantidade += quantidade;
		}
	}
	
	public void addTotalvenda(Double preco, Double qtde, Money desconto, Money frete) {
		Double valoritem = 0d;
		if(preco != null){
			valoritem = SinedUtil.round((preco * (qtde != null ? qtde : 1d)), 2);
		}		
		if(desconto != null){
			valoritem -= desconto.getValue().doubleValue();
		}
		if(frete != null){
			valoritem += frete.getValue().doubleValue();
		}
		
		if(this.totalvenda == null) this.totalvenda = 0d;
		this.totalvenda += valoritem;
	}
	
	public void addTotalcusto(Double custo, Double qtde) {
		Double valoritem = 0d;
		if(custo != null){
			valoritem = SinedUtil.round((custo * (qtde != null ? qtde : 1d)), 2);
		}	
		
		if(this.totalcusto == null) this.totalcusto = 0d;
		this.totalcusto += valoritem;
	}
	
}
