package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.sql.Date;
import java.util.LinkedList;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.SinedUtil;

public class EmitirFaturaLocacaoBean {
	
	private String empresa_nome;
	private String empresa_endereco;
	private String empresa_logradouro;
	private String empresa_numero;
	private String empresa_bairro;
	private String empresa_cep;
	private String empresa_municipio;
	private String empresa_uf;
	private String empresa_telefone;
	private String empresa_telefone_principal;
	private String empresa_email;
	private String empresa_cnpj;
	private String empresa_inscricaoestadual;
	private String empresa_inscricaomunicipal;
	private String empresa_url_logomarca;
	
	private Money valor_total;
	private String valor_total_extenso;
	private String numero;
	private String mes_referencia;
	private String periodo_servico;
	private String contrato_numero;
	private Date contrato_data_inicio;
	private Date contrato_data_renovacao;
	private Date data_emissao;
	private Date data_vencimento;
	private Date data_contrato;
	private String data_contrato_extenso;
	
	private Double contrato_quantidade;
	private Money contrato_valorunitario;
	private Money contrato_valortotal;
	
	private String cliente_nome;
	private String cliente_razaosocial;
	private String cliente_endereco;
	private String cliente_logradouro;
	private String cliente_numero;
	private String cliente_bairro;
	private String cliente_cep;
	private String cliente_cpf_cnpj;
	private String cliente_municipio;
	private String cliente_uf;
	private String cliente_inscricaoestadual;
	private String cliente_telefone;
	private String cliente_telefone_principal;
	
	private String endereco_fatura_logradouro;
	private String endereco_fatura_numero;
	private String endereco_fatura_bairro;
	private String endereco_fatura_municipio;
	private String endereco_fatura_cep;
	private String endereco_fatura_complemento;
	private String endereco_fatura_uf;
	
	
	private String endereco_entrega_logradouro;
	private String endereco_entrega_numero;
	private String endereco_entrega_bairro;
	private String endereco_entrega_municipio;
	private String endereco_entrega_complemento;
	private String endereco_entrega_cep;
	private String endereco_entrega_uf;
	
	private String cfop;
	private String dados_adicionais_fatura;
	private String dados_adicionais_empresa;
	
	private Money valor_ir;
	private Money valor_csll;
	private Money valor_pis;
	private Money valor_cofins;
	private Money valor_iss;
	private Money valor_liquido;
	
	private String situacao_nome;
	private String url_marca_dagua_cancelada;
	
	private LinkedList<EmitirFaturaLocacaoMaterialBean> listaMateriais = new LinkedList<EmitirFaturaLocacaoMaterialBean>();	
	
	public String getEmpresa_nome() {
		return empresa_nome;
	}
	public String getEmpresa_endereco() {
		return empresa_endereco;
	}
	public String getEmpresa_telefone() {
		return empresa_telefone;
	}
	public String getEmpresa_cnpj() {
		return empresa_cnpj;
	}
	public String getEmpresa_inscricaomunicipal() {
		return empresa_inscricaomunicipal;
	}
	public String getNumero() {
		return numero;
	}
	public String getMes_referencia() {
		return mes_referencia;
	}
	public Date getData_emissao() {
		return data_emissao;
	}
	public Date getData_vencimento() {
		return data_vencimento;
	}
	public Date getData_contrato() {
		return data_contrato;
	}
	public String getData_contrato_extenso() {
		return data_contrato_extenso;
	}
	public String getCliente_nome() {
		return cliente_nome;
	}
	public String getCliente_endereco() {
		return cliente_endereco;
	}
	public String getCliente_bairro() {
		return cliente_bairro;
	}
	public String getCliente_cep() {
		return cliente_cep;
	}
	public String getCliente_cpf_cnpj() {
		return cliente_cpf_cnpj;
	}
	public String getCliente_municipio() {
		return cliente_municipio;
	}
	public String getCliente_uf() {
		return cliente_uf;
	}
	public String getCliente_inscricaoestadual() {
		return cliente_inscricaoestadual;
	}
	public String getDados_adicionais_fatura() {
		return dados_adicionais_fatura;
	}
	public String getDados_adicionais_empresa() {
		return dados_adicionais_empresa;
	}
	public LinkedList<EmitirFaturaLocacaoMaterialBean> getListaMateriais() {
		return listaMateriais;
	}
	public String getCfop() {
		return cfop;
	}
	public String getEmpresa_url_logomarca() {
		return empresa_url_logomarca;
	}
	public String getEmpresa_email() {
		return empresa_email;
	}
	public String getEmpresa_inscricaoestadual() {
		return empresa_inscricaoestadual;
	}
	public Money getValor_total() {
		return valor_total;
	}
	public String getValor_total_extenso() {
		return valor_total_extenso;
	}
	public String getPeriodo_servico() {
		return periodo_servico;
	}
	public String getContrato_numero() {
		return contrato_numero;
	}
	public Date getContrato_data_inicio() {
		return contrato_data_inicio;
	}
	public Date getContrato_data_renovacao() {
		return contrato_data_renovacao;
	}
	public String getCliente_telefone() {
		return cliente_telefone;
	}
	public String getCliente_razaosocial() {
		return cliente_razaosocial;
	}
	public Double getContrato_quantidade() {
		return contrato_quantidade;
	}
	public void setContrato_quantidade(Double contrato_quantidade) {
		this.contrato_quantidade = contrato_quantidade;
	}
	public Money getContrato_valorunitario() {
		return contrato_valorunitario;
	}
	public void setContrato_valorunitario(Money contrato_valorunitario) {
		this.contrato_valorunitario = contrato_valorunitario;
	}
	public Money getContrato_valortotal() {
		return contrato_valortotal;
	}
	public void setContrato_valortotal(Money contrato_valortotal) {
		this.contrato_valortotal = contrato_valortotal;
	}
	public void setCliente_razaosocial(String clienteRazaosocial) {
		cliente_razaosocial = clienteRazaosocial;
	}
	public void setEmpresa_email(String empresaEmail) {
		empresa_email = empresaEmail;
	}
	public void setEmpresa_inscricaoestadual(String empresaInscricaoestadual) {
		empresa_inscricaoestadual = empresaInscricaoestadual;
	}
	public void setValor_total(Money valorTotal) {
		valor_total = valorTotal;
	}
	public void setValor_total_extenso(String valorTotalExtenso) {
		valor_total_extenso = valorTotalExtenso;
	}
	public void setPeriodo_servico(String periodoServico) {
		periodo_servico = periodoServico;
	}
	public void setContrato_numero(String contratoNumero) {
		contrato_numero = contratoNumero;
	}
	public void setContrato_data_inicio(Date contratoDataInicio) {
		contrato_data_inicio = contratoDataInicio;
	}
	public void setContrato_data_renovacao(Date contratoDataRenovacao) {
		contrato_data_renovacao = contratoDataRenovacao;
	}
	public void setCliente_telefone(String clienteTelefone) {
		cliente_telefone = clienteTelefone;
	}
	public void setEmpresa_url_logomarca(String empresaUrlLogomarca) {
		empresa_url_logomarca = empresaUrlLogomarca;
	}
	public void setCfop(String cfop) {
		this.cfop = cfop;
	}
	public void setListaMateriais(
			LinkedList<EmitirFaturaLocacaoMaterialBean> listaMateriais) {
		this.listaMateriais = listaMateriais;
	}
	public void setEmpresa_nome(String empresaNome) {
		empresa_nome = empresaNome;
	}
	public void setEmpresa_endereco(String empresaEndereco) {
		empresa_endereco = empresaEndereco;
	}
	public void setEmpresa_telefone(String empresaTelefone) {
		empresa_telefone = empresaTelefone;
	}
	public void setEmpresa_cnpj(String empresaCnpj) {
		empresa_cnpj = empresaCnpj;
	}
	public void setEmpresa_inscricaomunicipal(String empresaInscricaomunicipal) {
		empresa_inscricaomunicipal = empresaInscricaomunicipal;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setMes_referencia(String mesReferencia) {
		mes_referencia = mesReferencia;
	}
	public void setData_emissao(Date dataEmissao) {
		data_emissao = dataEmissao;
	}
	public void setData_vencimento(Date dataVencimento) {
		data_vencimento = dataVencimento;
	}
	public void setData_contrato(Date dataContrato) {
		data_contrato = dataContrato;
	}
	public void setData_contrato_extenso(String dataContratoExtenso) {
		data_contrato_extenso = dataContratoExtenso;
	}
	public void setCliente_nome(String clienteNome) {
		cliente_nome = clienteNome;
	}
	public void setCliente_endereco(String clienteEndereco) {
		cliente_endereco = clienteEndereco;
	}
	public void setCliente_bairro(String clienteBairro) {
		cliente_bairro = clienteBairro;
	}
	public void setCliente_cep(String clienteCep) {
		cliente_cep = clienteCep;
	}
	public void setCliente_cpf_cnpj(String clienteCpfCnpj) {
		cliente_cpf_cnpj = clienteCpfCnpj;
	}
	public void setCliente_municipio(String clienteMunicipio) {
		cliente_municipio = clienteMunicipio;
	}
	public void setCliente_uf(String clienteUf) {
		cliente_uf = clienteUf;
	}
	public void setCliente_inscricaoestadual(String clienteInscricaoestadual) {
		cliente_inscricaoestadual = clienteInscricaoestadual;
	}
	public void setDados_adicionais_fatura(String dadosAdicionaisFatura) {
		dados_adicionais_fatura = dadosAdicionaisFatura;
	}
	public void setDados_adicionais_empresa(String dadosAdicionaisEmpresa) {
		dados_adicionais_empresa = dadosAdicionaisEmpresa;
	}
	public String getEmpresa_logradouro() {
		return empresa_logradouro;
	}
	public String getEmpresa_numero() {
		return empresa_numero;
	}
	public String getEmpresa_bairro() {
		return empresa_bairro;
	}
	public String getEmpresa_cep() {
		return empresa_cep;
	}
	public String getEmpresa_municipio() {
		return empresa_municipio;
	}
	public String getEmpresa_uf() {
		return empresa_uf;
	}
	public String getEmpresa_telefone_principal() {
		return empresa_telefone_principal;
	}	
	public String getEndereco_entrega_cep() {
		return endereco_entrega_cep;
	}
	public String getEndereco_entrega_uf() {
		return endereco_entrega_uf;
	}
	public String getCliente_logradouro() {
		return cliente_logradouro;
	}
	public String getCliente_numero() {
		return cliente_numero;
	}
	public String getCliente_telefone_principal() {
		return cliente_telefone_principal;
	}
	public void setEmpresa_logradouro(String empresaLogradouro) {
		empresa_logradouro = empresaLogradouro;
	}
	public void setEmpresa_numero(String empresaNumero) {
		empresa_numero = empresaNumero;
	}
	public void setEmpresa_bairro(String empresaBairro) {
		empresa_bairro = empresaBairro;
	}
	public void setEmpresa_cep(String empresaCep) {
		empresa_cep = empresaCep;
	}
	public void setEmpresa_municipio(String empresaMunicipio) {
		empresa_municipio = empresaMunicipio;
	}
	public void setEmpresa_uf(String empresaUf) {
		empresa_uf = empresaUf;
	}
	public void setEmpresa_telefone_principal(String empresaTelefonePrincipal) {
		empresa_telefone_principal = empresaTelefonePrincipal;
	}
	public void setEndereco_entrega_cep(String enderecoEntregaCep) {
		endereco_entrega_cep = enderecoEntregaCep;
	}
	public void setEndereco_entrega_uf(String enderecoEntregaUf) {
		endereco_entrega_uf = enderecoEntregaUf;
	}
	public void setCliente_logradouro(String clienteLogradouro) {
		cliente_logradouro = clienteLogradouro;
	}
	public void setCliente_numero(String clienteNumero) {
		cliente_numero = clienteNumero;
	}
	public void setCliente_telefone_principal(String clienteTelefonePrincipal) {
		cliente_telefone_principal = clienteTelefonePrincipal;
	}
	public String getEndereco_fatura_logradouro() {
		return endereco_fatura_logradouro;
	}
	public String getEndereco_fatura_numero() {
		return endereco_fatura_numero;
	}
	public String getEndereco_fatura_complemento() {
		return endereco_fatura_complemento;
	}
	public String getEndereco_fatura_uf() {
		return endereco_fatura_uf;
	}
	public String getEndereco_fatura_bairro() {
		return endereco_fatura_bairro;
	}
	public String getEndereco_fatura_municipio() {
		return endereco_fatura_municipio;
	}
	public String getEndereco_fatura_cep() {
		return endereco_fatura_cep;
	}
	public String getEndereco_entrega_logradouro() {
		return endereco_entrega_logradouro;
	}
	public String getEndereco_entrega_numero() {
		return endereco_entrega_numero;
	}
	public String getEndereco_entrega_bairro() {
		return endereco_entrega_bairro;
	}
	public String getEndereco_entrega_municipio() {
		return endereco_entrega_municipio;
	}	
	public String getEndereco_entrega_complemento() {
		return endereco_entrega_complemento;
	}
	public Money getValor_ir() {
		return valor_ir;
	}
	public Money getValor_csll() {
		return valor_csll;
	}
	public Money getValor_pis() {
		return valor_pis;
	}
	public Money getValor_cofins() {
		return valor_cofins;
	}
	public Money getValor_liquido() {
		return valor_liquido;
	}
	public Money getValor_iss() {
		return valor_iss;
	}
	public String getSituacao_nome() {
		return situacao_nome;
	}
	public String getUrl_marca_dagua_cancelada() {
		System.out.println(SinedUtil.getUrlWithContext());
		return url_marca_dagua_cancelada;
	}
	
	public void setEndereco_fatura_uf(String enderecoFaturaUf) {
		endereco_fatura_uf = enderecoFaturaUf;
	}
	public void setEndereco_fatura_logradouro(String enderecoFaturaLogradouro) {
		endereco_fatura_logradouro = enderecoFaturaLogradouro;
	}
	public void setEndereco_fatura_numero(String enderecoFaturaNumero) {
		endereco_fatura_numero = enderecoFaturaNumero;
	}
	public void setEndereco_fatura_bairro(String enderecoFaturaBairro) {
		endereco_fatura_bairro = enderecoFaturaBairro;
	}
	public void setEndereco_fatura_municipio(String enderecoFaturaMunicipio) {
		endereco_fatura_municipio = enderecoFaturaMunicipio;
	}
	public void setEndereco_entrega_logradouro(String enderecoEntregaLogradouro) {
		endereco_entrega_logradouro = enderecoEntregaLogradouro;
	}
	public void setEndereco_entrega_numero(String enderecoEntregaNumero) {
		endereco_entrega_numero = enderecoEntregaNumero;
	}
	public void setEndereco_entrega_bairro(String enderecoEntregaBairro) {
		endereco_entrega_bairro = enderecoEntregaBairro;
	}
	public void setEndereco_entrega_municipio(String enderecoEntregaMunicipio) {
		endereco_entrega_municipio = enderecoEntregaMunicipio;
	}
	public void setEndereco_fatura_cep(String enderecoFaturaCep) {
		endereco_fatura_cep = enderecoFaturaCep;
	}
	public void setEndereco_fatura_complemento(String enderecoFaturaComplemento) {
		endereco_fatura_complemento = enderecoFaturaComplemento;
	}
	public void setEndereco_entrega_complemento(String enderecoEntregaComplemento) {
		endereco_entrega_complemento = enderecoEntregaComplemento;
	}
	public void setValor_ir(Money valorIr) {
		valor_ir = valorIr;
	}
	public void setValor_csll(Money valorCsll) {
		valor_csll = valorCsll;
	}
	public void setValor_pis(Money valorPis) {
		valor_pis = valorPis;
	}
	public void setValor_cofins(Money valorCofins) {
		valor_cofins = valorCofins;
	}
	public void setValor_liquido(Money valorLiquido) {
		valor_liquido = valorLiquido;
	}
	public void setValor_iss(Money valorIss) {
		valor_iss = valorIss;
	}
	public void setSituacao_nome(String situacao_nome) {
		this.situacao_nome = situacao_nome;
	}
	public void setUrl_marca_dagua_cancelada(String url_marca_dagua_cancelada) {
		this.url_marca_dagua_cancelada = url_marca_dagua_cancelada;
	}	
}