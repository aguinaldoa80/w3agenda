package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import br.com.linkcom.neo.util.DoubleUtils;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendavalorcampoextra;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.VendamaterialReportBean;

public class VendaInfoContribuinteBean {

	protected Integer cdvenda;
	protected String dtvenda;
	protected LinkedList<VendamaterialReportBean> listavendamaterial;
	protected String situacao;
	protected String identificador;
	protected String identificadorExterno;
	protected Double frete;
	protected String descricaoFrete;
	protected Double valorDesconto;
	protected String formaPagamento;
	protected String prazoPagamento;
	protected String endereco;
	protected String projetoNome;
	protected Double pesoBrutoTotal;
	protected Double pesoLiquidoTotal;
	protected String dataEntrega;
	protected Double valorTotal;
	/*protected Double valorSeguro;
	protected Double valorOutrasDespesas;
	protected Double valorIpi;
	protected Double valorIcms;
	protected Double valorIcmsSt;
	protected Double valorFcp;
	protected Double valorFcpSt;
	protected Double valorDifal;*/
	protected Double valeCompra;
	protected String observacao;
	protected String observacaoInterna;
	protected LinkedList<CampoAdicionalBean> listavendaCamposAdicionais;
	protected Integer vendedorCodigo;
	protected String vendedorNome;
	protected String vendedorMatricula;
	protected String vendedorCpf;
	
	public VendaInfoContribuinteBean(Venda venda, Colaboradorcargo colaboradorcargo){
		if(venda != null){
			this.setCdvenda(venda.getCdvenda());
			this.setDtvenda(venda.getDtvendastr());
			this.setListavendamaterial(new LinkedList<VendamaterialReportBean>());			
			if(venda.getListavendamaterial() != null && !venda.getListavendamaterial().isEmpty()){
				for(Vendamaterial vendamaterial : venda.getListavendamaterial()){
					listavendamaterial.add(new VendamaterialReportBean(vendamaterial));
				}
			}
			
			this.setSituacao(venda.getVendasituacao() != null? venda.getVendasituacao().getDescricao(): "");
			this.setIdentificador(venda.getIdentificador() != null ? venda.getIdentificador(): "");
			this.setIdentificadorExterno(venda.getIdentificadorexterno() != null ? venda.getIdentificadorexterno() : "");
			
			this.setFrete(venda.getValorfrete() != null? venda.getValorfrete().getValue().doubleValue(): 0);
			this.setDescricaoFrete(venda.getFrete() != null? venda.getFrete().getNome(): "");
			this.setValorDesconto(venda.getDesconto() != null? venda.getDesconto().getValue().doubleValue(): 0);
			this.setFormaPagamento(venda.getDocumentotipo() != null? venda.getDocumentotipo().getNome(): "");
			this.setPrazoPagamento(venda.getPrazopagamento() != null? venda.getPrazopagamento().getNome(): "");
			this.setEndereco(venda.getEndereco() != null? venda.getEndereco().getDescricaoCombo(): "");
			this.setProjetoNome(venda.getProjeto() != null? venda.getProjeto().getNome(): "");
			this.setPesoBrutoTotal(venda.getTotalpesobruto() != null? venda.getTotalpesobruto(): 0);
			this.setPesoLiquidoTotal(venda.getTotalpesoliquido() != null? venda.getTotalpesoliquido(): 0);
			this.setDataEntrega(venda.getDtprazoentregamax());
			this.setValorTotal(venda.getTotalvenda() != null? venda.getTotalvenda().getValue().doubleValue(): 0);
/*			this.setValorIpi(venda.getTotalipi() != null? venda.getTotalipi().getValue().doubleValue(): 0);
			this.setValorIcms(venda.getTotalIcms() != null? venda.getTotalIcms().getValue().doubleValue(): 0);
			this.setValorIcmsSt(venda.getTotalIcmsSt() != null? venda.getTotalIcmsSt().getValue().doubleValue(): 0);
			this.setValorFcp(venda.getTotalFcp() != null? venda.getTotalFcp().getValue().doubleValue(): 0);
			this.setValorFcpSt(venda.getTotalFcpSt() != null? venda.getTotalFcpSt().getValue().doubleValue(): 0);
			this.setValorDifal(venda.getTotalDifal() != null? venda.getTotalDifal().getValue().doubleValue(): 0);
			/this.setValorSeguro(venda.get() != null? venda.getTotalDifal().getValue().doubleValue(): 0);*/
			this.setValeCompra(venda.getValorusadovalecompra() != null? venda.getValorusadovalecompra().getValue().doubleValue(): 0);
			this.setListavendaCamposAdicionais(new LinkedList<CampoAdicionalBean>());
			this.setObservacao(venda.getObservacao() != null ? venda.getObservacao() : "");
			this.setObservacaoInterna(venda.getObservacaointerna() != null ? venda.getObservacaointerna() : "");
			this.setVendedorCodigo(venda.getColaborador()!=null ? venda.getColaborador().getCdpessoa() : null);
			this.setVendedorCpf(venda.getColaborador()!=null ? venda.getColaborador().getCpfCnpj() : "");
			this.setVendedorMatricula(colaboradorcargo != null && colaboradorcargo.getMatricula() != null ? colaboradorcargo.getMatricula().toString(): "");
			this.setVendedorNome(venda.getColaborador()!=null ? venda.getColaborador().getNome() : "");
			
			if(venda.getListaVendavalorcampoextra() != null){
				for(Vendavalorcampoextra campoextra: venda.getListaVendavalorcampoextra()){
					listavendaCamposAdicionais.add(new CampoAdicionalBean(campoextra));
				}
			}
		}
	}

	
	public String getDtvenda() {
		return Util.strings.emptyIfNull(dtvenda);
	}
	public void setDtvenda(String dtvenda) {
		this.dtvenda = dtvenda;
	}
	public String getSituacao() {
		return Util.strings.emptyIfNull(situacao);
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String getIdentificador() {
		return Util.strings.emptyIfNull(identificador);
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getIdentificadorExterno() {
		return Util.strings.emptyIfNull(identificadorExterno);
	}
	public void setIdentificadorExterno(String identificadorExterno) {
		this.identificadorExterno = identificadorExterno != null ? identificadorExterno
				: "";
	}
	public String getDescricaoFrete() {
		return Util.strings.emptyIfNull(descricaoFrete);
	}
	public void setDescricaoFrete(String descricaoFrete) {
		this.descricaoFrete = descricaoFrete;
	}
	public String getFormaPagamento() {
		return Util.strings.emptyIfNull(formaPagamento);
	}
	public void setFormaPagamento(String formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	public String getPrazoPagamento() {
		return Util.strings.emptyIfNull(prazoPagamento);
	}
	public void setPrazoPagamento(String prazoPagamento) {
		this.prazoPagamento = prazoPagamento;
	}
	public String getEndereco() {
		return Util.strings.emptyIfNull(endereco);
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getProjetoNome() {
		return Util.strings.emptyIfNull(projetoNome);
	}
	public void setProjetoNome(String projetoNome) {
		this.projetoNome = projetoNome;
	}
	public String getDataEntrega() {
		return dataEntrega;
	}
	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega != null ? new SimpleDateFormat("dd/mm/yyyy").format(dataEntrega): "";
	}
	public String getObservacao() {
		return Util.strings.emptyIfNull(observacao);
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getObservacaoInterna() {
		return Util.strings.emptyIfNull(observacaoInterna);
	}
	public void setObservacaoInterna(String observacaoInterna) {
		this.observacaoInterna = observacaoInterna;
	}
	public Integer getCdvenda() {
		return cdvenda;
	}
	public void setCdvenda(Integer cdvenda) {
		this.cdvenda = cdvenda;
	}
	public LinkedList<VendamaterialReportBean> getListavendamaterial() {
		return listavendamaterial;
	}
	public void setListavendamaterial(
			LinkedList<VendamaterialReportBean> listavendamaterial) {
		this.listavendamaterial = listavendamaterial;
	}
	public Double getFrete() {
		return frete != null? frete: 0;
	}
	public void setFrete(Double frete) {
		this.frete = frete;
	}
	public Double getValorDesconto() {
		return valorDesconto != null? valorDesconto: 0;
	}
	public void setValorDesconto(Double valorDesconto) {
		this.valorDesconto = valorDesconto;
	}
	public Double getPesoBrutoTotal() {
		return pesoBrutoTotal != null? pesoBrutoTotal: 0;
	}
	public void setPesoBrutoTotal(Double pesoBrutoTotal) {
		this.pesoBrutoTotal = pesoBrutoTotal;
	}
	public Double getPesoLiquidoTotal() {
		return pesoLiquidoTotal != null? pesoLiquidoTotal: 0;
	}
	public void setPesoLiquidoTotal(Double pesoLiquidoTotal) {
		this.pesoLiquidoTotal = pesoLiquidoTotal;
	}
	public Double getValorTotal() {
		return valorTotal != null? valorTotal: 0;
	}
	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}
	/*public Double getValorIpi() {
		return valorIpi != null? valorIpi: 0;
	}
	public void setValorIpi(Double valorIpi) {
		this.valorIpi = valorIpi;
	}
	public Double getValorIcms() {
		return DoubleUtils.zeroWhenNull(valorIcms);
	}
	public void setValorIcms(Double valorIcms) {
		this.valorIcms = valorIcms;
	}
	public Double getValorIcmsSt() {
		return DoubleUtils.zeroWhenNull(valorIcmsSt);
	}
	public void setValorIcmsSt(Double valorIcmsSt) {
		this.valorIcmsSt = valorIcmsSt;
	}
	public Double getValorFcp() {
		return DoubleUtils.zeroWhenNull(valorFcp);
	}
	public void setValorFcp(Double valorFcp) {
		this.valorFcp = valorFcp;
	}
	public Double getValorFcpSt() {
		return DoubleUtils.zeroWhenNull(valorFcpSt);
	}
	public void setValorFcpSt(Double valorFcpST) {
		this.valorFcpSt = valorFcpST;
	}
	public Double getValorDifal() {
		return DoubleUtils.zeroWhenNull(valorDifal);
	}
	public void setValorDifal(Double valorDifal) {
		this.valorDifal = valorDifal;
	}
	public Double getValorSeguro() {
		return DoubleUtils.zeroWhenNull(valorSeguro);
	}
	public void setValorOutrasDespesas(Double valorOutrasDespesas) {
		this.valorOutrasDespesas = valorOutrasDespesas;
	}
	public Double getValorOutrasDespesas() {
		return DoubleUtils.zeroWhenNull(valorOutrasDespesas);
	}
	public void setValorSeguro(Double valorSeguro) {
		this.valorSeguro = valorSeguro;
	}*/
	public Double getValeCompra() {
		return valeCompra != null? valeCompra: 0;
	}
	public void setValeCompra(Double valeCompra) {
		this.valeCompra = valeCompra;
	}
	public LinkedList<CampoAdicionalBean> getListavendaCamposAdicionais() {
		return listavendaCamposAdicionais;
	}
	public void setListavendaCamposAdicionais(
			LinkedList<CampoAdicionalBean> listavendaCamposAdicionais) {
		this.listavendaCamposAdicionais = listavendaCamposAdicionais;
	}

	public Integer getVendedorCodigo() {
		return vendedorCodigo;
	}
	public void setVendedorCodigo(Integer vendedorCodigo) {
		this.vendedorCodigo = vendedorCodigo;
	}

	public String getVendedorNome() {
		return Util.strings.emptyIfNull(vendedorNome);
	}
	public void setVendedorNome(String vendedorNome) {
		this.vendedorNome = vendedorNome;
	}

	public String getVendedorMatricula() {
		return Util.strings.emptyIfNull(vendedorMatricula);
	}
	public void setVendedorMatricula(String vendedorMatricula) {
		this.vendedorMatricula = vendedorMatricula;
	}

	public String getVendedorCpf() {
		return Util.strings.emptyIfNull(vendedorCpf);
	}
	public void setVendedorCpf(String vendedorCpf) {
		this.vendedorCpf = vendedorCpf;
	}
}
