package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.LinkedList;


public class EmitirExpedicaoClienteBean {
	
	private String cliente;
	private String contato;
	private String endereco;
	private String observacaoVendaPedido;
	private LinkedList<EmitirExpedicaoClienteItemBean> listaItem = new LinkedList<EmitirExpedicaoClienteItemBean>();
	private String telefone;
	private Double qtde_total_vendida;
	private Double qtde_total_expedicao;
	private Double peso_total_bruto;
	
	public String getCliente() {
		return cliente;
	}
	public String getContato() {
		return contato;
	}
	public String getEndereco() {
		return endereco;
	}
	public LinkedList<EmitirExpedicaoClienteItemBean> getListaItem() {
		return listaItem;
	}
	public String getObservacaoVendaPedido() {
		return observacaoVendaPedido;
	}
	public void setObservacaoVendaPedido(String observacaoVendaPedido) {
		this.observacaoVendaPedido = observacaoVendaPedido;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public void setContato(String contato) {
		this.contato = contato;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public void setListaItem(LinkedList<EmitirExpedicaoClienteItemBean> listaItem) {
		this.listaItem = listaItem;
	}
	public Double getQtde_total_vendida() {
		return qtde_total_vendida;
	}
	public Double getQtde_total_expedicao() {
		return qtde_total_expedicao;
	}
	public Double getPeso_total_bruto() {
		return peso_total_bruto;
	}
	public void setQtde_total_vendida(Double qtdeTotalVendida) {
		qtde_total_vendida = qtdeTotalVendida;
	}
	public void setQtde_total_expedicao(Double qtdeTotalExpedicao) {
		qtde_total_expedicao = qtdeTotalExpedicao;
	}
	public void setPeso_total_bruto(Double pesoTotalBruto) {
		peso_total_bruto = pesoTotalBruto;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		result = prime * result + ((contato == null) ? 0 : contato.hashCode());
		result = prime * result
				+ ((endereco == null) ? 0 : endereco.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmitirExpedicaoClienteBean other = (EmitirExpedicaoClienteBean) obj;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		if (contato == null) {
			if (other.contato != null)
				return false;
		} else if (!contato.equals(other.contato))
			return false;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		return true;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
}
