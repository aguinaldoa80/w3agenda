package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel;


public class ComprovanteConfiguravelAlternativo {
	
	private String whereIn;
	private ComprovanteConfiguravel comprovanteConfiguravel;

	public ComprovanteConfiguravelAlternativo(String whereIn){
		this.whereIn = whereIn;
	}
	
	public String getWhereIn() {
		return whereIn;
	}
	public ComprovanteConfiguravel getComprovanteConfiguravel() {
		return comprovanteConfiguravel;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	public void setComprovanteConfiguravel(
			ComprovanteConfiguravel comprovanteConfiguravel) {
		this.comprovanteConfiguravel = comprovanteConfiguravel;
	}
}