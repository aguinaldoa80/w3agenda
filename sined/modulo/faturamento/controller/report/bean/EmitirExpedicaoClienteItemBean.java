package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.Date;


public class EmitirExpedicaoClienteItemBean {
	
	private String numero_nf;
	private Integer id_pedidovenda;
	private Integer id_venda;
	private Date data_venda;
	private String material;
	private Double qtde_vendida;
	private Double qtde_expedicao;
	private Double peso_bruto;
	
	private Integer coleta;
	private Integer pedido_venda_coleta;
	private String identificador_pedido_venda_coleta;
//	private Integer nota_fiscal_retorno;
	private Integer id_pneu;
	private String pneu_marca;
	private String pneu_modelo;
	private String pneu_medida;
	private String pneu_seriefogo;
	private Integer pneu_dot;
	private String pneu_numeroreforma;
	private String pneu_banda;
	private String pneu_qualificacao;
	private Integer id_pneu_recusado;
	private String pneu_marca_recusado;
	private String pneu_modelo_recusado;
	private String pneu_medida_recusado;
	private String pneu_seriefogo_recusado;
	private Integer pneu_dot_recusado;
	private String pneu_numeroreforma_recusado;
	private String pneu_banda_recusado;
	private String pneu_qualificacao_recusado;
	private String lote;
	private Integer material_codigo_mestre_da_grade;
	private String material_nome_mestre_da_grade;
	private Double material_altura;
	private Double material_comprimento;
	private Double material_largura;
	private Integer ordementrega;
	private String pneu_roda;
	private String pneu_lonas;
	private String pneu_descricao;
	private String otrPneuDesenho;
	private String otrPneuTipoServico;
	private String pneu_roda_recusado;
	private String pneu_lonas_recusado;
	private String pneu_descricao_recusado;
	private String otrPneuDesenho_recusado;
	private String otrPneuTipoServico_recusado;
	


	/////////////////////////////////////////|
	//  INICIO DOS GET 				/////////|
	/////////////////////////////////////////|
	
	public String getNumero_nf() {
		return numero_nf;
	}
	public Integer getId_pedidovenda() {
		return id_pedidovenda;
	}
	public Integer getId_venda() {
		return id_venda;
	}
	public Date getData_venda() {
		return data_venda;
	}
	public String getMaterial() {
		return material;
	}
	public Double getQtde_vendida() {
		return qtde_vendida;
	}
	public Double getQtde_expedicao() {
		return qtde_expedicao;
	}
	public Double getPeso_bruto() {
		return peso_bruto;
	}
	
	
	/////////////////////////////////////////|
	//  INICIO DOS SET 				/////////|
	/////////////////////////////////////////|
	
	public Integer getColeta() {
		return coleta;
	}
	public void setColeta(Integer coleta) {
		this.coleta = coleta;
	}
	public void setPedido_venda_coleta(Integer pedido_venda_coleta) {
		this.pedido_venda_coleta = pedido_venda_coleta;
	}
	public void setIdentificador_pedido_venda_coleta(
			String identificador_pedido_venda_coleta) {
		this.identificador_pedido_venda_coleta = identificador_pedido_venda_coleta;
	}
//	public void setNota_fiscal_retorno(Integer nota_fiscal_retorno) {
//		this.nota_fiscal_retorno = nota_fiscal_retorno;
//	}
	public void setId_pneu(Integer id_pneu) {
		this.id_pneu = id_pneu;
	}
	public void setPneu_marca(String pneu_marca) {
		this.pneu_marca = pneu_marca;
	}
	public void setPneu_modelo(String pneu_modelo) {
		this.pneu_modelo = pneu_modelo;
	}
	public void setPneu_medida(String pneu_medida) {
		this.pneu_medida = pneu_medida;
	}
	public void setPneu_seriefogo(String pneu_seriefogo) {
		this.pneu_seriefogo = pneu_seriefogo;
	}
	public void setPneu_dot(Integer pneu_dot) {
		this.pneu_dot = pneu_dot;
	}
	public void setPneu_numeroreforma(String pneu_numeroreforma) {
		this.pneu_numeroreforma = pneu_numeroreforma;
	}
	public void setPneu_banda(String pneu_banda) {
		this.pneu_banda = pneu_banda;
	}
	public void setPneu_qualificacao(String pneu_qualificacao) {
		this.pneu_qualificacao = pneu_qualificacao;
	}
	public void setId_pneu_recusado(Integer id_pneu_recusado) {
		this.id_pneu_recusado = id_pneu_recusado;
	}
	public void setPneu_marca_recusado(String pneu_marca_recusado) {
		this.pneu_marca_recusado = pneu_marca_recusado;
	}
	public void setPneu_modelo_recusado(String pneu_modelo_recusado) {
		this.pneu_modelo_recusado = pneu_modelo_recusado;
	}
	public void setPneu_medida_recusado(String pneu_medida_recusado) {
		this.pneu_medida_recusado = pneu_medida_recusado;
	}
	public void setPneu_seriefogo_recusado(String pneu_seriefogo_recusado) {
		this.pneu_seriefogo_recusado = pneu_seriefogo_recusado;
	}
	public void setPneu_dot_recusado(Integer pneu_dot_recusado) {
		this.pneu_dot_recusado = pneu_dot_recusado;
	}
	public void setPneu_numeroreforma_recusado(String pneu_numeroreforma_recusado) {
		this.pneu_numeroreforma_recusado = pneu_numeroreforma_recusado;
	}
	public void setPneu_banda_recusado(String pneu_banda_recusado) {
		this.pneu_banda_recusado = pneu_banda_recusado;
	}
	public void setPneu_qualificacao_recusado(String pneu_qualificacao_recusado) {
		this.pneu_qualificacao_recusado = pneu_qualificacao_recusado;
	}
	public Integer getPedido_venda_coleta() {
		return pedido_venda_coleta;
	}
	public String getIdentificador_pedido_venda_coleta() {
		return identificador_pedido_venda_coleta;
	}
//	public Integer getNota_fiscal_retorno() {
//		return nota_fiscal_retorno;
//	}
	public Integer getId_pneu() {
		return id_pneu;
	}
	public String getPneu_marca() {
		return pneu_marca;
	}
	public String getPneu_modelo() {
		return pneu_modelo;
	}
	public String getPneu_medida() {
		return pneu_medida;
	}
	public String getPneu_seriefogo() {
		return pneu_seriefogo;
	}
	public Integer getPneu_dot() {
		return pneu_dot;
	}
	public String getPneu_numeroreforma() {
		return pneu_numeroreforma;
	}
	public String getPneu_banda() {
		return pneu_banda;
	}
	public String getPneu_qualificacao() {
		return pneu_qualificacao;
	}
	public Integer getId_pneu_recusado() {
		return id_pneu_recusado;
	}
	public String getPneu_marca_recusado() {
		return pneu_marca_recusado;
	}
	public String getPneu_modelo_recusado() {
		return pneu_modelo_recusado;
	}
	public String getPneu_medida_recusado() {
		return pneu_medida_recusado;
	}
	public String getPneu_seriefogo_recusado() {
		return pneu_seriefogo_recusado;
	}
	public Integer getPneu_dot_recusado() {
		return pneu_dot_recusado;
	}
	public String getPneu_numeroreforma_recusado() {
		return pneu_numeroreforma_recusado;
	}
	public String getPneu_banda_recusado() {
		return pneu_banda_recusado;
	}
	public String getPneu_qualificacao_recusado() {
		return pneu_qualificacao_recusado;
	}
	public void setNumero_nf(String numeroNf) {
		numero_nf = numeroNf;
	}
	public void setId_pedidovenda(Integer idPedidovenda) {
		id_pedidovenda = idPedidovenda;
	}
	public void setId_venda(Integer id_venda) {
		this.id_venda = id_venda;
	}
	public void setData_venda(Date dataVenda) {
		data_venda = dataVenda;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public void setQtde_vendida(Double qtdeVendida) {
		qtde_vendida = qtdeVendida;
	}
	public void setQtde_expedicao(Double qtdeExpedicao) {
		qtde_expedicao = qtdeExpedicao;
	}
	public void setPeso_bruto(Double pesoBruto) {
		peso_bruto = pesoBruto;
	}
	
	public String getLote() {
		return lote;
	}
	
	public void setLote(String lote) {
		this.lote = lote;
	}
	
	public Integer getMaterial_codigo_mestre_da_grade() {
		return material_codigo_mestre_da_grade;
	}
	
	public void setMaterial_codigo_mestre_da_grade(Integer material_codigo_mestre_da_grade) {
		this.material_codigo_mestre_da_grade = material_codigo_mestre_da_grade;
	}
	
	public String getMaterial_nome_mestre_da_grade() {
		return material_nome_mestre_da_grade;
	}
	
	public void setMaterial_nome_mestre_da_grade(String material_nome_mestre_da_grade) {
		this.material_nome_mestre_da_grade = material_nome_mestre_da_grade;
	}
	
	public Double getMaterial_altura() {
		return material_altura;
	}
	
	public void setMaterial_altura(Double material_altura) {
		this.material_altura = material_altura;
	}
	
	public Double getMaterial_comprimento() {
		return material_comprimento;
	}
	
	public void setMaterial_comprimento(Double material_comprimento) {
		this.material_comprimento = material_comprimento;
	}
	
	public Double getMaterial_largura() {
		return material_largura;
	}
	
	public void setMaterial_largura(Double material_largura) {
		this.material_largura = material_largura;
	}
	public Integer getOrdementrega() {
		return ordementrega;
	}
	public void setOrdementrega(Integer ordementrega) {
		this.ordementrega = ordementrega;
	}
	public String getPneu_roda() {
		return pneu_roda;
	}
	public String getPneu_lonas() {
		return pneu_lonas;
	}
	public String getPneu_descricao() {
		return pneu_descricao;
	}
	public String getOtrPneuDesenho() {
		return otrPneuDesenho;
	}
	public String getOtrPneuTipoServico() {
		return otrPneuTipoServico;
	}
	public void setPneu_roda(String pneu_roda) {
		this.pneu_roda = pneu_roda;
	}
	public void setPneu_lonas(String pneu_lonas) {
		this.pneu_lonas = pneu_lonas;
	}
	public void setPneu_descricao(String pneu_descricao) {
		this.pneu_descricao = pneu_descricao;
	}
	public void setOtrPneuDesenho(String otrPneuDesenho) {
		this.otrPneuDesenho = otrPneuDesenho;
	}
	public void setOtrPneuTipoServico(String otrPneuTipoServico) {
		this.otrPneuTipoServico = otrPneuTipoServico;
	}
	public String getPneu_roda_recusado() {
		return pneu_roda_recusado;
	}
	public String getPneu_lonas_recusado() {
		return pneu_lonas_recusado;
	}
	public String getPneu_descricao_recusado() {
		return pneu_descricao_recusado;
	}
	public String getOtrPneuDesenho_recusado() {
		return otrPneuDesenho_recusado;
	}
	public String getOtrPneuTipoServico_recusado() {
		return otrPneuTipoServico_recusado;
	}
	public void setPneu_roda_recusado(String pneu_roda_recusado) {
		this.pneu_roda_recusado = pneu_roda_recusado;
	}
	public void setPneu_lonas_recusado(String pneu_lonas_recusado) {
		this.pneu_lonas_recusado = pneu_lonas_recusado;
	}
	public void setPneu_descricao_recusado(String pneu_descricao_recusado) {
		this.pneu_descricao_recusado = pneu_descricao_recusado;
	}
	public void setOtrPneuDesenho_recusado(String otrPneuDesenho_recusado) {
		this.otrPneuDesenho_recusado = otrPneuDesenho_recusado;
	}
	public void setOtrPneuTipoServico_recusado(String otrPneuTipoServico_recusado) {
		this.otrPneuTipoServico_recusado = otrPneuTipoServico_recusado;
	}
	
}
