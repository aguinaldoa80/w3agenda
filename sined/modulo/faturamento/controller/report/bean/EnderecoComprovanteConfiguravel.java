package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

public class EnderecoComprovanteConfiguravel {

	private String ENDERECOCOMPLETO;
	private String ENDERECOTIPO;
	private String ENDERECOUFSIGLA;
	
	
	public String getENDERECOCOMPLETO() {
		return ENDERECOCOMPLETO;
	}
	public String getENDERECOTIPO() {
		return ENDERECOTIPO;
	}
	public String getENDERECOUFSIGLA() {
		return ENDERECOUFSIGLA;
	}
	
	public void setENDERECOCOMPLETO(String eNDERECOCOMPLETO) {
		ENDERECOCOMPLETO = eNDERECOCOMPLETO;
	}
	public void setENDERECOTIPO(String eNDERECOTIPO) {
		ENDERECOTIPO = eNDERECOTIPO;
	}
	public void setENDERECOUFSIGLA(String eNDERECOUFSIGLA) {
		ENDERECOUFSIGLA = eNDERECOUFSIGLA;
	}
}
