package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

public class OrdemcompraMaterialBean {

	private String nome;
	private String descricao;
	private String unidade_medida;
	private Double largura;
	private Double comprimento;
	private Double altura;
	private String observacao;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getUnidade_medida() {
		return unidade_medida;
	}

	public void setUnidade_medida(String unidade_medida) {
		this.unidade_medida = unidade_medida;
	}

	public Double getLargura() {
		return largura;
	}

	public void setLargura(Double largura) {
		this.largura = largura;
	}

	public Double getComprimento() {
		return comprimento;
	}
	
	public String getObservacao() {
		return observacao;
	}

	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}

	public Double getAltura() {
		return altura;
	}

	public void setAltura(Double altura) {
		this.altura = altura;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

}
