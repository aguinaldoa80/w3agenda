package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.Date;

import br.com.linkcom.sined.geral.bean.Documentotipo;

public class VendaFormapagamentoBean {

	private Date dtprevisto;
	private Date dtlancamento;
	private Documentotipo documentotipo;
	private Double valorbruto;
	private Double taxaadm;
	private Double valorliquido;
	
	public Date getDtprevisto() {
		return dtprevisto;
	}
	public Date getDtlancamento() {
		return dtlancamento;
	}
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	public Double getValorbruto() {
		return valorbruto;
	}
	public Double getTaxaadm() {
		return taxaadm;
	}
	public Double getValorliquido() {
		return valorliquido;
	}
	public void setDtprevisto(Date dtprevisto) {
		this.dtprevisto = dtprevisto;
	}
	public void setDtlancamento(Date dtlancamento) {
		this.dtlancamento = dtlancamento;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setValorbruto(Double valorbruto) {
		this.valorbruto = valorbruto;
	}
	public void setTaxaadm(Double taxaadm) {
		this.taxaadm = taxaadm;
	}
	public void setValorliquido(Double valorliquido) {
		this.valorliquido = valorliquido;
	}

}
