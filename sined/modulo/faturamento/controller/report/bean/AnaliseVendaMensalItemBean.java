package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.sql.Date;

import br.com.linkcom.sined.util.SinedDateUtils;

public class AnaliseVendaMensalItemBean {
	
	private Integer material_id;
	private String material_identificador;
	private String material_nome;
	private Date data;
	private Double qtde = 0d;
	
	public AnaliseVendaMensalItemBean(Integer materialId,
			String materialIdentificador, String materialNome, Date data) {
		material_id = materialId;
		material_identificador = materialIdentificador;
		material_nome = materialNome;
		this.data = data;
	}
	
	public String getMaterial_identificador() {
		return material_identificador;
	}
	public String getMaterial_nome() {
		return material_nome;
	}
	public Date getData() {
		return data;
	}
	public Double getQtde() {
		return qtde;
	}
	public Integer getMaterial_id() {
		return material_id;
	}
	public void setMaterial_id(Integer materialId) {
		material_id = materialId;
	}
	public void setMaterial_identificador(String materialIdentificador) {
		material_identificador = materialIdentificador;
	}
	public void setMaterial_nome(String materialNome) {
		material_nome = materialNome;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnaliseVendaMensalItemBean other = (AnaliseVendaMensalItemBean) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!SinedDateUtils.equalsIgnoreHour(data, other.data))
			return false;
		if (material_id == null) {
			if (other.material_id != null)
				return false;
		} else if (!material_id.equals(other.material_id))
			return false;
		return true;
	}
	
}