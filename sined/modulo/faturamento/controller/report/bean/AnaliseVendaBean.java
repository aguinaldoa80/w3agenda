package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.Date;

public class AnaliseVendaBean {
	
	private String cliente;
	private Date dtultcompra;
	private String periodocompra;	
	private String materiaissolicitados;
	private String identificador;
	private String nomeContato;
	
	public String getCliente() {
		return cliente;
	}
	public Date getDtultcompra() {
		return dtultcompra;
	}
	public String getPeriodocompra() {
		return periodocompra;
	}
	public String getMateriaissolicitados() {
		return materiaissolicitados;
	}
	public String getIdentificador() {
		return identificador;
	}
	public String getNomeContato() {
		return nomeContato;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public void setDtultcompra(Date date) {
		this.dtultcompra = date;
	}
	public void setPeriodocompra(String periodocompra) {
		this.periodocompra = periodocompra;
	}
	public void setMateriaissolicitados(String materiaissolicitados) {
		this.materiaissolicitados = materiaissolicitados;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setNomeContato(String nomeContato) {
		this.nomeContato = nomeContato;
	}
}
