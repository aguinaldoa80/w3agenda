package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;


public class EmitirExpedicaoEtiquetaBean {
	
	private String nome_empresa;
	private String end_numero_empresa;
	private String end_complemento_empresa;
	private String end_logradouro_empresa;
	private String end_bairro_empresa;
	private String end_cidade_empresa;
	private String end_estado_empresa;
	private String end_cep_empresa;
	
	private String nome_cliente;
	private String end_numero_cliente;
	private String end_complemento_cliente;
	private String end_logradouro_cliente;
	private String end_bairro_cliente;
	private String end_cidade_cliente;
	private String end_estado_cliente;
	private String end_cep_cliente;

	private String numero_pedido;
	private Integer quantidade_volumes;
	public String getNome_empresa() {
		return nome_empresa;
	}
	public void setNome_empresa(String nome_empresa) {
		this.nome_empresa = nome_empresa;
	}
	public String getEnd_numero_empresa() {
		return end_numero_empresa;
	}
	public void setEnd_numero_empresa(String end_numero_empresa) {
		this.end_numero_empresa = end_numero_empresa;
	}
	public String getEnd_complemento_empresa() {
		return end_complemento_empresa;
	}
	public void setEnd_complemento_empresa(String end_complemento_empresa) {
		this.end_complemento_empresa = end_complemento_empresa;
	}
	public String getEnd_logradouro_empresa() {
		return end_logradouro_empresa;
	}
	public void setEnd_logradouro_empresa(String end_logradouro_empresa) {
		this.end_logradouro_empresa = end_logradouro_empresa;
	}
	public String getEnd_bairro_empresa() {
		return end_bairro_empresa;
	}
	public void setEnd_bairro_empresa(String end_bairro_empresa) {
		this.end_bairro_empresa = end_bairro_empresa;
	}
	public String getEnd_cidade_empresa() {
		return end_cidade_empresa;
	}
	public void setEnd_cidade_empresa(String end_cidade_empresa) {
		this.end_cidade_empresa = end_cidade_empresa;
	}
	public String getEnd_estado_empresa() {
		return end_estado_empresa;
	}
	public void setEnd_estado_empresa(String end_estado_empresa) {
		this.end_estado_empresa = end_estado_empresa;
	}
	public String getNome_cliente() {
		return nome_cliente;
	}
	public void setNome_cliente(String nome_cliente) {
		this.nome_cliente = nome_cliente;
	}
	public String getEnd_numero_cliente() {
		return end_numero_cliente;
	}
	public void setEnd_numero_cliente(String end_numero_cliente) {
		this.end_numero_cliente = end_numero_cliente;
	}
	public String getEnd_complemento_cliente() {
		return end_complemento_cliente;
	}
	public void setEnd_complemento_cliente(String end_complemento_cliente) {
		this.end_complemento_cliente = end_complemento_cliente;
	}
	public String getEnd_logradouro_cliente() {
		return end_logradouro_cliente;
	}
	public void setEnd_logradouro_cliente(String end_logradouro_cliente) {
		this.end_logradouro_cliente = end_logradouro_cliente;
	}
	public String getEnd_bairro_cliente() {
		return end_bairro_cliente;
	}
	public void setEnd_bairro_cliente(String end_bairro_cliente) {
		this.end_bairro_cliente = end_bairro_cliente;
	}
	public String getEnd_cidade_cliente() {
		return end_cidade_cliente;
	}
	public void setEnd_cidade_cliente(String end_cidade_cliente) {
		this.end_cidade_cliente = end_cidade_cliente;
	}
	public String getEnd_estado_cliente() {
		return end_estado_cliente;
	}
	public void setEnd_estado_cliente(String end_estado_cliente) {
		this.end_estado_cliente = end_estado_cliente;
	}
	public String getNumero_pedido() {
		return numero_pedido;
	}
	public void setNumero_pedido(String numero_pedido) {
		this.numero_pedido = numero_pedido;
	}
	public Integer getQuantidade_volumes() {
		return quantidade_volumes;
	}
	public void setQuantidade_volumes(Integer quantidade_volumes) {
		this.quantidade_volumes = quantidade_volumes;
	}
    public String getEnd_cep_empresa() {
        return end_cep_empresa;
    }
    public void setEnd_cep_empresa(String end_cep_empresa) {
        this.end_cep_empresa = end_cep_empresa;
    }
    public String getEnd_cep_cliente() {
        return end_cep_cliente;
    }
    public void setEnd_cep_cliente(String end_cep_cliente) {
        this.end_cep_cliente = end_cep_cliente;
    }
}
