package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.LinkedList;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.TelefoneBean;
import br.com.linkcom.sined.util.rest.arvoreacesso.ContatoBean;
import br.com.linkcom.sined.util.rest.arvoreacesso.EnderecoBean;

public class ClienteBean {

	protected String nome = "";
	protected String nomefantasia;
	protected String razaosocial = "";
	protected String cpf = "";
	protected String cnpj = "";
	protected String email = "";
	protected String inscricaoestadual = "";
	protected String inscricaomunicipal = "";
	protected LinkedList<EnderecoBean> endereco;
	protected LinkedList<TelefoneBean> telefone;
	protected LinkedList<ContatoBean> contato;
	protected String site = "";
	protected String responsavel = "";
	protected String observacao = "";
	protected String informacoesadicionaiscontribuinte = "";
	
	public ClienteBean(){

	}
	
	public ClienteBean(Cliente cliente){
		this.setContato(new LinkedList<ContatoBean>());
		this.setTelefone(new LinkedList<TelefoneBean>());
		this.setEndereco(new LinkedList<EnderecoBean>());
		if(cliente != null){
			this.setNome(cliente.getNome());
			this.setRazaosocial(cliente.getRazaosocial());
			this.setCpf(cliente.getCpf() != null? cliente.getCpf().getValue(): "");
			this.setCnpf(cliente.getCnpj() != null? cliente.getCnpj().getValue(): "");
			this.setEmail(cliente.getEmail());
			this.setInformacoesadicionaiscontribuinte(cliente.getInfoadicionalcontrib());
			this.setInscricaoestadual(cliente.getInscricaoestadual());
			this.setInscricaomunicipal(cliente.getInscricaomunicipal());
			this.setSite(cliente.getSite());
			this.setResponsavel(cliente.getContatoResponsavel() != null? cliente.getContatoResponsavel().getNome(): "");
			this.setObservacao(cliente.getObservacao());

			if(cliente.getListaEndereco() != null){
				for(Endereco end: cliente.getListaEndereco()){
					this.getEndereco().add(new EnderecoBean(end));
				}				
			}
			
			if(cliente.getListaTelefone() != null){
				for(Telefone tel: cliente.getListaTelefone()){
					this.getTelefone().add(new TelefoneBean(tel));
				}				
			}
			
			if(cliente.getListaContato() != null){
				for(PessoaContato pessoaContato: cliente.getListaContato()) {
					Contato contato = pessoaContato.getContato();
					
					this.getContato().add(new ContatoBean(contato));
				}				
			}
		}
	}
	
	public String getNome() {
		return Util.strings.emptyIfNull(nome);
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getNomefantasia() {
		return Util.strings.emptyIfNull(nomefantasia);
	}
	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}
	
	public String getRazaosocial() {
		return Util.strings.emptyIfNull(razaosocial);
	}
	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}
	
	public String getCpf() {
		return Util.strings.emptyIfNull(cpf);
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getCnpf() {
		return Util.strings.emptyIfNull(cnpj);
	}
	public void setCnpf(String cnpf) {
		this.cnpj = cnpf;
	}
	
	public String getEmail() {
		return Util.strings.emptyIfNull(email);
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getInscricaoestadual() {
		return Util.strings.emptyIfNull(inscricaoestadual);
	}
	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}
	
	public String getInscricaomunicipal() {
		return Util.strings.emptyIfNull(inscricaomunicipal);
	}
	public void setInscricaomunicipal(String inscricaomunicipal) {
		this.inscricaomunicipal = inscricaomunicipal;
	}
	
	public LinkedList<EnderecoBean> getEndereco() {
		return endereco;
	}
	public void setEndereco(LinkedList<EnderecoBean> endereco) {
		this.endereco = endereco;
	}
	
	public LinkedList<TelefoneBean> getTelefone() {
		return telefone;
	}
	public void setTelefone(LinkedList<TelefoneBean> telefone) {
		this.telefone = telefone;
	}
	
	public LinkedList<ContatoBean> getContato() {
		return contato;
	}
	public void setContato(LinkedList<ContatoBean> contato) {
		this.contato = contato;
	}
	
	public String getSite() {
		return Util.strings.emptyIfNull(site);
	}
	public void setSite(String site) {
		this.site = site;
	}
	
	public String getResponsavel() {
		return Util.strings.emptyIfNull(responsavel);
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	
	public String getObservacao() {
		return Util.strings.emptyIfNull(observacao);
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public String getInformacoesadicionaiscontribuinte() {
		return Util.strings.emptyIfNull(informacoesadicionaiscontribuinte);
	}
	public void setInformacoesadicionaiscontribuinte(
			String informacoesadicionaiscontribuinte) {
		this.informacoesadicionaiscontribuinte = informacoesadicionaiscontribuinte;
	}
	
	
}
