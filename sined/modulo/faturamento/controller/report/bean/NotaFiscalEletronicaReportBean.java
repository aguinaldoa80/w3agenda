package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.awt.Image;
import java.util.Date;
import java.util.LinkedList;


public class NotaFiscalEletronicaReportBean {
	
	private Date dtEmissao;
	private Date dtVencimento;
	private Date primeiroVencimentoParcela;
	private String numNfse = "";
	private String numeroNfse = "";
	private Date dtCompetencia;
	private String codVerificacao = "";
	private Image logoPrestador;
	private Image logoNFse;
	private String nomePrefeitura = "";
	private String enderecoPrefeitura = "";
	private String prestadorNome = "";
	private String prestadorCpfCnpj = "";
	private String prestadorInscMun = "";
	private String prestadorInscEst = "";
	private String prestadorEndereco = "";
	private String prestadorCidade = "";
	private String prestadorEstado = "";
	private String prestadorTelefone = "";
	private String prestadorEmail = "";
	private String tomadorNome = "";
	private String tomadorCpfCnpj = "";
	private String tomadorInscMun = "";
	private String tomadorInscEst = "";
	private String tomadorEndereco = "";
	private String tomadorEnderecoLogradouro = "";
	private String tomadorEnderecoNumero = "";
	private String tomadorEnderecoComplemento = "";
	private String tomadorEnderecoBairro = "";
	private String tomadorEnderecoCep = "";
	private String tomadorCidade = "";
	private String tomadorEstado = "";
	private String tomadorTelefone = "";
	private String tomadorEmail = "";
	private String discriminacaoServico = "";
	private String discriminacaoDuplicatas = "";
	private String cnaeDescricao = "";
	private String itemListaDescricao = "";
	private String codMunicipioPrestacao = "";
	private String naturezaOperacao = "";
	private String valorServicos = "";
	private String descontoServicos = "";
	private String retencoesServicos = "";
	private String issRetidoServicos = "";
	private String deducoesServicos = "";
	private String descontoIncServicos = "";
	private String baseCalculoServicos = "";
	private String aliquotaServicos = "";
	private String valorLiquido = "";
	private String valorIss = "";
	private String valorIR = "";
	private String valorPis = "";
	private String valorConfis = "";
	private String valorCsll = "";
	private String valorInss = "";
	private String valorIssRetido = "";
	private String valorIRRetido = "";
	private String valorPisRetido = "";
	private String valorConfisRetido = "";
	private String valorCsllRetido = "";
	private String valorInssRetido = "";
	private String valorOutrasRetencoes = "";
	private String percentualIss = "";
	private Boolean incideIss = Boolean.FALSE;
	private String outrasinformacoes = "";
	private Boolean mostrarCtiss = Boolean.TRUE;
	private Boolean mostrarAtividade = Boolean.FALSE;
	private String patternDataemissao = "dd/MM/yyyy '�s' HH:mm:ss";
	private String patternData = "dd/MM/yyyy";
	private String regimeEspecial = "";
	private String serie ="";
	private String entradaSaida = "";
	private Boolean rps = Boolean.FALSE;
	private String quantidadeServico;
	private String totalprodutos;
	private String valorTotal;
	private String totalDescontos;
	private Integer status;
	private String valorUnitario;
	private String valorPorExtenso = "";
	private String valorLiquidoPorExtenso = "";
	private String numRPS;
	private String prestadorNomeFantasia = "";
	private String prestadorEnderecoSemMunicipioSemComplemento = "";
	private String prestadorEnderecoComplemento = "";
	private String tomadorEnderecoSemMunicipioSemComplemento = "";
	private String descontoCondicionadoServicos = "";
	private Boolean prestadorSimplesNacional;
	private String discriminacaoComValorServico;
	private String codigoNfeNaturezaOperacao = "";
	private String dataHoraEmissao = "";
	private String codigocontrole = "";
	private String vencimentos = "";
	private String observacaovenda = "";
	private String observacaonota = "";
	private String observacaoitabirito = "";
	private Boolean variasParcelas = Boolean.FALSE;
	private Double valorDescontoNotaMunicipal;
	private String cdnota;
	private Integer cdconfiguracaonfe;
	private String logoNfseBase64;
	private String logoPrestadorBase64;
	private LinkedList<NotaFiscalEletronicaItemReportBean> listaItens = new LinkedList<NotaFiscalEletronicaItemReportBean>();
		
	public Date getDtEmissao() {
		return dtEmissao;
	}
	
	public Date getDtVencimento() {
		return dtVencimento;
	}

	public String getNumNfse() {
		return numNfse;
	}
	public String getNumeroNfse() {
		return numeroNfse;
	}
	public Date getDtCompetencia() {
		return dtCompetencia;
	}
	public String getCodVerificacao() {
		return codVerificacao;
	}
	public Image getLogoPrestador() {
		return logoPrestador;
	}
	public String getPrestadorNome() {
		return prestadorNome;
	}
	public String getPrestadorCpfCnpj() {
		return prestadorCpfCnpj;
	}
	public String getPrestadorInscMun() {
		return prestadorInscMun;
	}
	public String getPrestadorEndereco() {
		return prestadorEndereco;
	}
	public String getPrestadorCidade() {
		return prestadorCidade;
	}
	public String getPrestadorEstado() {
		return prestadorEstado;
	}
	public String getPrestadorTelefone() {
		return prestadorTelefone;
	}
	public String getPrestadorEmail() {
		return prestadorEmail;
	}
	public String getTomadorNome() {
		return tomadorNome;
	}
	public String getTomadorCpfCnpj() {
		return tomadorCpfCnpj;
	}
	public String getTomadorInscMun() {
		return tomadorInscMun;
	}
	public String getTomadorEndereco() {
		return tomadorEndereco;
	}
	public String getTomadorCidade() {
		return tomadorCidade;
	}
	public String getTomadorEstado() {
		return tomadorEstado;
	}
	public String getTomadorTelefone() {
		return tomadorTelefone;
	}
	public String getTomadorEmail() {
		return tomadorEmail;
	}
	public String getDiscriminacaoServico() {
		return discriminacaoServico;
	}
	public String getDiscriminacaoDuplicatas() {
		return discriminacaoDuplicatas;
	}
	public String getCnaeDescricao() {
		return cnaeDescricao;
	}
	public String getItemListaDescricao() {
		return itemListaDescricao;
	}
	public String getCodMunicipioPrestacao() {
		return codMunicipioPrestacao;
	}
	public String getValorServicos() {
		return valorServicos;
	}
	public String getDescontoServicos() {
		return descontoServicos;
	}
	public String getRetencoesServicos() {
		return retencoesServicos;
	}
	public String getIssRetidoServicos() {
		return issRetidoServicos;
	}
	public String getDeducoesServicos() {
		return deducoesServicos;
	}
	public String getDescontoIncServicos() {
		return descontoIncServicos;
	}
	public String getBaseCalculoServicos() {
		return baseCalculoServicos;
	}
	public String getAliquotaServicos() {
		return aliquotaServicos;
	}
	public String getValorLiquido() {
		return valorLiquido;
	}
	public String getValorIss() {
		return valorIss;
	}
	public String getNaturezaOperacao() {
		return naturezaOperacao;
	}
	public String getOutrasinformacoes() {
		return outrasinformacoes;
	}
	public Boolean getMostrarCtiss() {
		return mostrarCtiss;
	}
	public Boolean getMostrarAtividade() {
		return mostrarAtividade;
	}
	public String getRegimeEspecial() {
		return regimeEspecial;
	}
	public String getTomadorEnderecoLogradouro() {
		return tomadorEnderecoLogradouro;
	}
	public String getTomadorEnderecoNumero() {
		return tomadorEnderecoNumero;
	}
	public String getTomadorEnderecoComplemento() {
		return tomadorEnderecoComplemento;
	}
	public String getTomadorEnderecoBairro() {
		return tomadorEnderecoBairro;
	}
	public String getTomadorEnderecoCep() {
		return tomadorEnderecoCep;
	}
	public Boolean getRps() {
		return rps;
	}
	public String getPrestadorInscEst() {
		return prestadorInscEst;
	}
	public String getSerie() {
		return serie;
	}
	public String getEntradaSaida() {
		return entradaSaida;
	}
	public String getTomadorInscEst() {
		return tomadorInscEst;
	}
	public String getQuantidadeServico() {
		return quantidadeServico;
	}
	public String getPatternData() {
		return patternData;
	}
	public String getValorIR() {
		return valorIR;
	}
	public String getValorPis() {
		return valorPis;
	}
	public String getValorConfis() {
		return valorConfis;
	}
	public String getValorCsll() {
		return valorCsll;
	}
	public String getValorInss() {
		return valorInss;
	}
	public String getTotalprodutos() {
		return totalprodutos;
	}
	public String getValorTotal() {
		return valorTotal;
	}
	public String getTotalDescontos() {
		return totalDescontos;
	}
	public Integer getStatus() {
		return status;
	}
	public String getPrestadorNomeFantasia() {
		return prestadorNomeFantasia;
	}
	public String getPrestadorEnderecoSemMunicipioSemComplemento() {
		return prestadorEnderecoSemMunicipioSemComplemento;
	}
	public String getPrestadorEnderecoComplemento() {
		return prestadorEnderecoComplemento;
	}
	public String getTomadorEnderecoSemMunicipioSemComplemento() {
		return tomadorEnderecoSemMunicipioSemComplemento;
	}
	public String getDescontoCondicionadoServicos() {
		return descontoCondicionadoServicos;
	}
	public Boolean getPrestadorSimplesNacional() {
		return prestadorSimplesNacional;
	}
	public String getDiscriminacaoComValorServico() {
		return discriminacaoComValorServico;
	}
	public String getCodigoNfeNaturezaOperacao() {
		return codigoNfeNaturezaOperacao;
	}
	public String getDataHoraEmissao() {
		return dataHoraEmissao;
	}
	public String getCodigocontrole() {
		return codigocontrole;
	}
	public String getVencimentos() {
		return vencimentos;
	}
	public void setVencimentos(String vencimentos) {
		this.vencimentos = vencimentos;
	}
	public String getObservacaovenda() {
		return observacaovenda;
	}
	public String getObservacaoitabirito() {
		return observacaoitabirito;
	}
	public String getObservacaonota() {
		return observacaonota;
	}
	public String getNomePrefeitura() {
		return nomePrefeitura;
	}
	public void setNomePrefeitura(String nomePrefeitura) {
		this.nomePrefeitura = nomePrefeitura;
	}
	public String getEnderecoPrefeitura() {
		return enderecoPrefeitura;
	}
	public void setEnderecoPrefeitura(String enderecoPrefeitura) {
		this.enderecoPrefeitura = enderecoPrefeitura;
	}
	public void setObservacaonota(String observacaonota) {
		this.observacaonota = observacaonota;
	}
	public void setObservacaoitabirito(String observacaoitabirito) {
		this.observacaoitabirito = observacaoitabirito;
	}
	public void setObservacaovenda(String observacaovenda) {
		this.observacaovenda = observacaovenda;
	}
	public void setCodigocontrole(String codigocontrole) {
		this.codigocontrole = codigocontrole;
	}
	public void setRps(Boolean rps) {
		this.rps = rps;
	}
	public String getPatternDataemissao() {
		return patternDataemissao;
	}
	public Image getLogoNFse() {
		return logoNFse;
	}
	public LinkedList<NotaFiscalEletronicaItemReportBean> getListaItens() {
		return listaItens;
	}
	public void setListaItens(
			LinkedList<NotaFiscalEletronicaItemReportBean> listaItens) {
		this.listaItens = listaItens;
	}
	public void setPatternDataemissao(String patternDataemissao) {
		this.patternDataemissao = patternDataemissao;
	}
	public void setTomadorEnderecoLogradouro(String tomadorEnderecoLogradouro) {
		this.tomadorEnderecoLogradouro = tomadorEnderecoLogradouro;
	}
	public void setTomadorEnderecoNumero(String tomadorEnderecoNumero) {
		this.tomadorEnderecoNumero = tomadorEnderecoNumero;
	}
	public void setTomadorEnderecoComplemento(String tomadorEnderecoComplemento) {
		this.tomadorEnderecoComplemento = tomadorEnderecoComplemento;
	}
	public void setTomadorEnderecoBairro(String tomadorEnderecoBairro) {
		this.tomadorEnderecoBairro = tomadorEnderecoBairro;
	}
	public void setTomadorEnderecoCep(String tomadorEnderecoCep) {
		this.tomadorEnderecoCep = tomadorEnderecoCep;
	}
	public void setRegimeEspecial(String regimeEspecial) {
		this.regimeEspecial = regimeEspecial;
	}
	public void setMostrarAtividade(Boolean mostrarAtividade) {
		this.mostrarAtividade = mostrarAtividade;
	}
	public void setMostrarCtiss(Boolean mostrarCtiss) {
		this.mostrarCtiss = mostrarCtiss;
	}
	public void setOutrasinformacoes(String outrasinformacoes) {
		this.outrasinformacoes = outrasinformacoes;
	}
	public void setNaturezaOperacao(String naturezaOperacao) {
		this.naturezaOperacao = naturezaOperacao;
	}
	public void setDtEmissao(Date dtEmissao) {
		this.dtEmissao = dtEmissao;
	}
	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}
	public void setNumNfse(String numNfse) {
		this.numNfse = numNfse;
	}
	public void setNumeroNfse(String numeroNfse) {
		this.numeroNfse = numeroNfse;
	}
	public void setDtCompetencia(Date dtCompetencia) {
		this.dtCompetencia = dtCompetencia;
	}
	public void setCodVerificacao(String codVerificacao) {
		this.codVerificacao = codVerificacao;
	}
	public void setLogoPrestador(Image logoPrestador) {
		this.logoPrestador = logoPrestador;
	}
	public void setPrestadorNome(String prestadorNome) {
		this.prestadorNome = prestadorNome;
	}
	public void setPrestadorCpfCnpj(String prestadorCpfCnpj) {
		this.prestadorCpfCnpj = prestadorCpfCnpj;
	}
	public void setPrestadorInscMun(String prestadorInscMun) {
		this.prestadorInscMun = prestadorInscMun;
	}
	public void setPrestadorEndereco(String prestadorEndereco) {
		this.prestadorEndereco = prestadorEndereco;
	}
	public void setPrestadorCidade(String prestadorCidade) {
		this.prestadorCidade = prestadorCidade;
	}
	public void setPrestadorEstado(String prestadorEstado) {
		this.prestadorEstado = prestadorEstado;
	}
	public void setPrestadorTelefone(String prestadorTelefone) {
		this.prestadorTelefone = prestadorTelefone;
	}
	public void setPrestadorEmail(String prestadorEmail) {
		this.prestadorEmail = prestadorEmail;
	}
	public void setTomadorNome(String tomadorNome) {
		this.tomadorNome = tomadorNome;
	}
	public void setTomadorCpfCnpj(String tomadorCpfCnpj) {
		this.tomadorCpfCnpj = tomadorCpfCnpj;
	}
	public void setTomadorInscMun(String tomadorInscMun) {
		this.tomadorInscMun = tomadorInscMun;
	}
	public void setTomadorEndereco(String tomadorEndereco) {
		this.tomadorEndereco = tomadorEndereco;
	}
	public void setTomadorCidade(String tomadorCidade) {
		this.tomadorCidade = tomadorCidade;
	}
	public void setTomadorEstado(String tomadorEstado) {
		this.tomadorEstado = tomadorEstado;
	}
	public void setTomadorTelefone(String tomadorTelefone) {
		this.tomadorTelefone = tomadorTelefone;
	}
	public void setTomadorEmail(String tomadorEmail) {
		this.tomadorEmail = tomadorEmail;
	}
	public void setDiscriminacaoServico(String discriminacaoServico) {
		this.discriminacaoServico = discriminacaoServico;
	}
	public void setDiscriminacaoDuplicatas(String discriminacaoDuplicatas) {
		this.discriminacaoDuplicatas = discriminacaoDuplicatas;
	}
	public void setCnaeDescricao(String cnaeDescricao) {
		this.cnaeDescricao = cnaeDescricao;
	}
	public void setItemListaDescricao(String itemListaDescricao) {
		this.itemListaDescricao = itemListaDescricao;
	}
	public void setCodMunicipioPrestacao(String codMunicipioPrestacao) {
		this.codMunicipioPrestacao = codMunicipioPrestacao;
	}
	public void setValorServicos(String valorServicos) {
		this.valorServicos = valorServicos;
	}
	public void setDescontoServicos(String descontoServicos) {
		this.descontoServicos = descontoServicos;
	}
	public void setRetencoesServicos(String retencoesServicos) {
		this.retencoesServicos = retencoesServicos;
	}
	public void setIssRetidoServicos(String issRetidoServicos) {
		this.issRetidoServicos = issRetidoServicos;
	}
	public void setDeducoesServicos(String deducoesServicos) {
		this.deducoesServicos = deducoesServicos;
	}
	public void setDescontoIncServicos(String descontoIncServicos) {
		this.descontoIncServicos = descontoIncServicos;
	}
	public void setBaseCalculoServicos(String baseCalculoServicos) {
		this.baseCalculoServicos = baseCalculoServicos;
	}
	public void setAliquotaServicos(String aliquotaServicos) {
		this.aliquotaServicos = aliquotaServicos;
	}
	public void setValorLiquido(String valorLiquido) {
		this.valorLiquido = valorLiquido;
	}
	public void setValorIss(String valorIss) {
		this.valorIss = valorIss;
	}
	public void setLogoNFse(Image logoNFse) {
		this.logoNFse = logoNFse;
	}
	public void setPrestadorInscEst(String prestadorInscEst) {
		this.prestadorInscEst = prestadorInscEst;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public void setEntradaSaida(String entradaSaida) {
		this.entradaSaida = entradaSaida;
	}
	public void setTomadorInscEst(String tomadorInscEst) {
		this.tomadorInscEst = tomadorInscEst;
	}
	public void setQuantidadeServico(String quantidadeServico) {
		this.quantidadeServico = quantidadeServico;
	}
	public void setPatternData(String patternData) {
		this.patternData = patternData;
	}
	public void setValorIR(String valorIR) {
		this.valorIR = valorIR;
	}
	public void setValorPis(String valorPis) {
		this.valorPis = valorPis;
	}
	public void setValorConfis(String valorConfis) {
		this.valorConfis = valorConfis;
	}
	public void setValorCsll(String valorCsll) {
		this.valorCsll = valorCsll;
	}
	public void setValorInss(String valorInss) {
		this.valorInss = valorInss;
	}
	public void setTotalprodutos(String totalprodutos) {
		this.totalprodutos = totalprodutos;
	}
	public void setValorTotal(String valorTotal) {
		this.valorTotal = valorTotal;
	}
	public void setTotalDescontos(String totalDescontos) {
		this.totalDescontos = totalDescontos;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getValorUnitario() {
		return valorUnitario;
	}
	public void setValorUnitario(String valorUnitario) {
		this.valorUnitario = valorUnitario;
	}
	public String getValorPorExtenso() {
		return valorPorExtenso;
	}
	public void setValorPorExtenso(String valorPorExtenso) {
		this.valorPorExtenso = valorPorExtenso;
	}
	public String getValorLiquidoPorExtenso() {
		return valorLiquidoPorExtenso;
	}
	public void setValorLiquidoPorExtenso(String valorLiquidoPorExtenso) {
		this.valorLiquidoPorExtenso = valorLiquidoPorExtenso;
	}
	public String getValorIssRetido() {
		return valorIssRetido;
	}
	public void setValorIssRetido(String valorIssRetido) {
		this.valorIssRetido = valorIssRetido;
	}
	public String getValorIRRetido() {
		return valorIRRetido;
	}
	public void setValorIRRetido(String valorIRRetido) {
		this.valorIRRetido = valorIRRetido;
	}
	public String getValorPisRetido() {
		return valorPisRetido;
	}
	public void setValorPisRetido(String valorPisRetido) {
		this.valorPisRetido = valorPisRetido;
	}
	public String getValorConfisRetido() {
		return valorConfisRetido;
	}
	public void setValorConfisRetido(String valorConfisRetido) {
		this.valorConfisRetido = valorConfisRetido;
	}
	public String getValorCsllRetido() {
		return valorCsllRetido;
	}
	public void setValorCsllRetido(String valorCsllRetido) {
		this.valorCsllRetido = valorCsllRetido;
	}
	public String getValorInssRetido() {
		return valorInssRetido;
	}
	public void setValorInssRetido(String valorInssRetido) {
		this.valorInssRetido = valorInssRetido;
	}
	public String getValorOutrasRetencoes() {
		return valorOutrasRetencoes;
	}
	public void setValorOutrasRetencoes(String valorOutrasRetencoes) {
		this.valorOutrasRetencoes = valorOutrasRetencoes;
	}
	public String getNumRPS() {
		return numRPS;
	}
	public void setNumRPS(String numRPS) {
		this.numRPS = numRPS;
	}
	public String getPercentualIss() {
		return percentualIss;
	}
	public Date getPrimeiroVencimentoParcela() {
		return primeiroVencimentoParcela;
	}
	public void setPrimeiroVencimentoParcela(Date primeiroVencimentoParcela) {
		this.primeiroVencimentoParcela = primeiroVencimentoParcela;
	}
	public void setPercentualIss(String percentualIss) {
		this.percentualIss = percentualIss;
	}
	public Boolean getIncideIss() {
		return incideIss;
	}
	public Boolean getVariasParcelas() {
		return variasParcelas;
	}
	public void setVariasParcelas(Boolean variasParcelas) {
		this.variasParcelas = variasParcelas;
	}
	public void setIncideIss(Boolean incideIss) {
		this.incideIss = incideIss;
	}
	public void setPrestadorNomeFantasia(String prestadorNomeFantasia) {
		this.prestadorNomeFantasia = prestadorNomeFantasia;
	}
	public void setPrestadorEnderecoSemMunicipioSemComplemento(String prestadorEnderecoSemMunicipioSemComplemento) {
		this.prestadorEnderecoSemMunicipioSemComplemento = prestadorEnderecoSemMunicipioSemComplemento;
	}
	public void setPrestadorEnderecoComplemento(String prestadorEnderecoComplemento) {
		this.prestadorEnderecoComplemento = prestadorEnderecoComplemento;
	}
	public void setTomadorEnderecoSemMunicipioSemComplemento(String tomadorEnderecoSemMunicipioSemComplemento) {
		this.tomadorEnderecoSemMunicipioSemComplemento = tomadorEnderecoSemMunicipioSemComplemento;
	}
	public void setDescontoCondicionadoServicos(String descontoCondicionadoServicos) {
		this.descontoCondicionadoServicos = descontoCondicionadoServicos;
	}
	public void setPrestadorSimplesNacional(Boolean prestadorSimplesNacional) {
		this.prestadorSimplesNacional = prestadorSimplesNacional;
	}
	public void setDiscriminacaoComValorServico(String discriminacaoComValorServico) {
		this.discriminacaoComValorServico = discriminacaoComValorServico;
	}
	public void setCodigoNfeNaturezaOperacao(String codigoNfeNaturezaOperacao) {
		this.codigoNfeNaturezaOperacao = codigoNfeNaturezaOperacao;
	}
	public void setDataHoraEmissao(String dataHoraEmissao) {
		this.dataHoraEmissao = dataHoraEmissao;
	}
	public Double getValorDescontoNotaMunicipal() {
		return valorDescontoNotaMunicipal;
	}
	public void setValorDescontoNotaMunicipal(Double valorDescontoNotaMunicipal) {
		this.valorDescontoNotaMunicipal = valorDescontoNotaMunicipal;
	}
	public String getCdnota() {
		return cdnota;
	}
	public void setCdnota(String cdnota) {
		this.cdnota = cdnota;
	}
	public String getLogoNfseBase64() {
		return logoNfseBase64;
	}
	public void setLogoNfseBase64(String logoNfseBase64) {
		this.logoNfseBase64 = logoNfseBase64;
	}
	public String getLogoPrestadorBase64() {
		return logoPrestadorBase64;
	}
	public void setLogoPrestadorBase64(String logoPrestadorBase64) {
		this.logoPrestadorBase64 = logoPrestadorBase64;
	}
	public Integer getCdconfiguracaonfe() {
		return cdconfiguracaonfe;
	}
	public void setCdconfiguracaonfe(Integer cdconfiguracaonfe) {
		this.cdconfiguracaonfe = cdconfiguracaonfe;
	}
	
}