package br.com.linkcom.sined.modulo.faturamento.controller.report.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SemaforoEnum {

	VERDE (0, "Verde"),
	AMARELO (1, "Amarelo"),
	VERMELHO (2, "Vermelho");
	
	private Integer value;
	private String descricao;
	
	private SemaforoEnum(Integer value, String descricao) {
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
