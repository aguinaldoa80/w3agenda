package br.com.linkcom.sined.modulo.faturamento.controller.report.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum FormaAgrupamentoEnum {

	PRODUTO (0, "Produto"),
	GRUPO_MATERIAL (1, "Grupo de Material"),
	CATEGORIA_MATERIAL (2, "Categoria do Material"),
	TIPO_MATERIAL (3, "Tipo do Material"),
	MUNICIPIO (4, "Município"),
	VENDEDOR (5, "Vendedor"),
	CLIENTE (6, "Cliente"),
	FABRICANTE (7, "Fabricante");
	
	private Integer value;
	private String descricao;
	
	private FormaAgrupamentoEnum(Integer value, String descricao) {
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
