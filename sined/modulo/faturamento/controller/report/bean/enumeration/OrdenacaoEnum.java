package br.com.linkcom.sined.modulo.faturamento.controller.report.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum OrdenacaoEnum {

	MARGEM (0, "Margem"),
	FATURAMENTO_BRUTO (1, "Faturamento Bruto"),
	SEMAFORO (2, "Sem�foro");
	
	private Integer value;
	private String descricao;
	
	private OrdenacaoEnum(Integer value, String descricao) {
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
