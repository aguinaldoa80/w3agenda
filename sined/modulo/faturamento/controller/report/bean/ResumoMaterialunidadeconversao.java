package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Unidademedida;

public class ResumoMaterialunidadeconversao {
	
	private Material material;
	private Unidademedida unidademedida;
	private Double qtde;
	
	
	public Material getMaterial() {
		return material;
	}
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	public Double getQtde() {
		return qtde;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResumoMaterialunidadeconversao other = (ResumoMaterialunidadeconversao) obj;
		if (material == null) {
			if (other.getMaterial() != null )
				return false;
		} else {
			if (material.equals(other.getMaterial())){
				if (unidademedida == null) {
					if (other.getUnidademedida() != null )
						return false;
				} else if (!unidademedida.equals(other.getUnidademedida())){
					return false;
				}
			}else  
				return false;
		}
		return true;
	}
}
