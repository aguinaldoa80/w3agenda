package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import br.com.linkcom.sined.geral.bean.Documentotipo;

public class VendaFormapagamentoTotalTaxaBean {
	
	private Documentotipo documentotipo;
	private Double valor;
	
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	public Double getValor() {
		return valor;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((documentotipo == null) ? 0 : documentotipo.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendaFormapagamentoTotalTaxaBean other = (VendaFormapagamentoTotalTaxaBean) obj;
		if (documentotipo == null) {
			if (other.documentotipo != null)
				return false;
		} else if (!documentotipo.equals(other.documentotipo))
			return false;
		return true;
	}
	
}
