package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.Date;

public class EmitirIndenizacaoContratoBean {

	private String cliente;
	private String contrato;
	private Date dtindenizacao;
	private String materialindenizado;
	private Double quantidade;
	private Double valorindenizado;
	private Double valorcusto;
	private Double totalindenizado;
	
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getContrato() {
		return contrato;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public Date getDtindenizacao() {
		return dtindenizacao;
	}
	public void setDtindenizacao(Date dtindenizacao) {
		this.dtindenizacao = dtindenizacao;
	}
	public String getMaterialindenizado() {
		return materialindenizado;
	}
	public void setMaterialindenizado(String materialindenizado) {
		this.materialindenizado = materialindenizado;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public Double getValorindenizado() {
		return valorindenizado;
	}
	public void setValorindenizado(Double valorindenizado) {
		this.valorindenizado = valorindenizado;
	}
	public Double getValorcusto() {
		return valorcusto;
	}
	public void setValorcusto(Double valorcusto) {
		this.valorcusto = valorcusto;
	}
	public Double getTotalindenizado() {
		return totalindenizado;
	}
	public void setTotalindenizado(Double totalindenizado) {
		this.totalindenizado = totalindenizado;
	}
}
