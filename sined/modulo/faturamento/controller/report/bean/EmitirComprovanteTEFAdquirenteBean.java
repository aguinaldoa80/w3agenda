package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

public class EmitirComprovanteTEFAdquirenteBean {

	private String comprovante = "";
	private String nsutid = "";
	private String autorizacao = "";
	private String adquirente = "";
	private String bandeira = "";
	private String data = "";

	public String getComprovante() {
		return comprovante;
	}

	public String getNsutid() {
		return nsutid;
	}

	public String getAutorizacao() {
		return autorizacao;
	}

	public String getAdquirente() {
		return adquirente;
	}

	public String getBandeira() {
		return bandeira;
	}

	public String getData() {
		return data;
	}

	public void setNsutid(String nsutid) {
		this.nsutid = nsutid;
	}

	public void setAutorizacao(String autorizacao) {
		this.autorizacao = autorizacao;
	}

	public void setAdquirente(String adquirente) {
		this.adquirente = adquirente;
	}

	public void setBandeira(String bandeira) {
		this.bandeira = bandeira;
	}

	public void setData(String data) {
		this.data = data;
	}

	public void setComprovante(String comprovante) {
		this.comprovante = comprovante;
	}
	
}
