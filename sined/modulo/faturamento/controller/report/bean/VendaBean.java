package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.LinkedList;



public class VendaBean {
	
	private Integer venda_situacao;
	private String venda_situacaonome;
	private String vendedor_nome;
	private String vendedor_email;
	private Integer vendedor_codigo;
	private String vendedor_nomeexibicao;
	private String vendedor_telefones;
	private String pedidovenda_tipo;
	
	private String cliente_identificador;
	private String cliente_nome;
	private String cliente_razaosocial;
	private String cliente_cpf;
	private String cliente_cnpj;
	private String cliente_email;
	private String cliente_endereco;
	private String cliente_inscricaoestadual;
	private String cliente_observacao;
	private String cliente_rg;
	private String cliente_responsavel;
	private String cliente_telefones;
	
	private String cliente_contatonome;
	private String cliente_contatoemail;
	private String cliente_contatotelefones;

	private String projeto_nome;
	
	private Integer codigo;
	private Integer codigoorcamento;
	private String data;
	private String data_hora;
	private String endereco;
	private String identificador;
	private String identificadorexterno;
	private String datahoraatualizacao;
	
	private String observacao;
	private String observacaointerna;
	private String dataentrega;
	private String valorfrete;
	private String valordesconto;
	private String terceiro;
	private String valortotal;
	private String valorfinalmaisipi;
	private String descricaofrete;
	private String prazopagamento;
	private String formapagamento;
	
	private String empresa_nome;
	private String empresa_cnpj;
	private String empresa_inscricaoestadual;
	private String empresa_endereco;
	private String empresa_telefone;
	private String empresa_site;
	private String empresa_email;
	private String empresa_municipio;
	
	private Double pesoliquidototal;
	private Double pesobrutototal;
	private String totalmetrocubico;
	private String produto_valortotal;
	private String produto_qtdetotalproduto;
	private String produto_qtdetotalservico;
	private String pagamento_valortotal;
	
	private LinkedList<PedidoVendaItemBean> listaPedidoVendaItens;
	private LinkedList<VendaItemBean> listaVendaItens;
	private LinkedList<VendaPagamentoBean> listaVendaPagamento;

	public Integer getVenda_situacao() {
		return venda_situacao;
	}

	public String getVenda_situacaonome() {
		return venda_situacaonome;
	}

	public String getVendedor_nome() {
		return vendedor_nome;
	}

	public String getVendedor_email() {
		return vendedor_email;
	}

	public Integer getVendedor_codigo() {
		return vendedor_codigo;
	}

	public String getVendedor_nomeexibicao() {
		return vendedor_nomeexibicao;
	}

	public String getVendedor_telefones() {
		return vendedor_telefones;
	}

	public String getPedidovenda_tipo() {
		return pedidovenda_tipo;
	}

	public String getCliente_identificador() {
		return cliente_identificador;
	}
	
	public String getCliente_nome() {
		return cliente_nome;
	}

	public String getCliente_razaosocial() {
		return cliente_razaosocial;
	}

	public String getCliente_cpf() {
		return cliente_cpf;
	}

	public String getCliente_cnpj() {
		return cliente_cnpj;
	}

	public String getCliente_email() {
		return cliente_email;
	}

	public String getCliente_endereco() {
		return cliente_endereco;
	}

	public String getCliente_inscricaoestadual() {
		return cliente_inscricaoestadual;
	}

	public String getCliente_observacao() {
		return cliente_observacao;
	}

	public String getCliente_rg() {
		return cliente_rg;
	}

	public String getCliente_responsavel() {
		return cliente_responsavel;
	}

	public String getCliente_telefones() {
		return cliente_telefones;
	}

	public String getCliente_contatonome() {
		return cliente_contatonome;
	}

	public String getCliente_contatoemail() {
		return cliente_contatoemail;
	}

	public String getCliente_contatotelefones() {
		return cliente_contatotelefones;
	}

	public String getProjeto_nome() {
		return projeto_nome;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public Integer getCodigoorcamento() {
		return codigoorcamento;
	}

	public String getData() {
		return data;
	}

	public String getData_hora() {
		return data_hora;
	}

	public String getEndereco() {
		return endereco;
	}

	public String getIdentificador() {
		return identificador;
	}

	public String getIdentificadorexterno() {
		return identificadorexterno;
	}

	public String getDatahoraatualizacao() {
		return datahoraatualizacao;
	}

	public String getObservacao() {
		return observacao;
	}

	public String getObservacaointerna() {
		return observacaointerna;
	}

	public String getDataentrega() {
		return dataentrega;
	}

	public String getValorfrete() {
		return valorfrete;
	}

	public String getValordesconto() {
		return valordesconto;
	}

	public String getTerceiro() {
		return terceiro;
	}

	public String getValortotal() {
		return valortotal;
	}

	public String getValorfinalmaisipi() {
		return valorfinalmaisipi;
	}

	public String getDescricaofrete() {
		return descricaofrete;
	}

	public String getPrazopagamento() {
		return prazopagamento;
	}

	public String getFormapagamento() {
		return formapagamento;
	}

	public String getEmpresa_nome() {
		return empresa_nome;
	}

	public String getEmpresa_cnpj() {
		return empresa_cnpj;
	}

	public String getEmpresa_inscricaoestadual() {
		return empresa_inscricaoestadual;
	}

	public String getEmpresa_endereco() {
		return empresa_endereco;
	}

	public String getEmpresa_telefone() {
		return empresa_telefone;
	}

	public String getEmpresa_site() {
		return empresa_site;
	}

	public String getEmpresa_email() {
		return empresa_email;
	}

	public String getEmpresa_municipio() {
		return empresa_municipio;
	}

	public Double getPesoliquidototal() {
		return pesoliquidototal;
	}

	public Double getPesobrutototal() {
		return pesobrutototal;
	}

	public String getTotalmetrocubico() {
		return totalmetrocubico;
	}

	public String getProduto_valortotal() {
		return produto_valortotal;
	}

	public String getProduto_qtdetotalproduto() {
		return produto_qtdetotalproduto;
	}

	public String getProduto_qtdetotalservico() {
		return produto_qtdetotalservico;
	}

	public LinkedList<VendaItemBean> getListaVendaItens() {
		return listaVendaItens;
	}
	
	public LinkedList<PedidoVendaItemBean> getListaPedidoVendaItens() {
		return listaPedidoVendaItens;
	}
	
	public void setListaPedidoVendaItens(
			LinkedList<PedidoVendaItemBean> listaPedidoVendaItens) {
		this.listaPedidoVendaItens = listaPedidoVendaItens;
	}

	public void setVenda_situacao(Integer vendaSituacao) {
		venda_situacao = vendaSituacao;
	}

	public void setVenda_situacaonome(String vendaSituacaonome) {
		venda_situacaonome = vendaSituacaonome;
	}

	public void setVendedor_nome(String vendedorNome) {
		vendedor_nome = vendedorNome;
	}

	public void setVendedor_email(String vendedorEmail) {
		vendedor_email = vendedorEmail;
	}

	public void setVendedor_codigo(Integer vendedorCodigo) {
		vendedor_codigo = vendedorCodigo;
	}

	public void setVendedor_nomeexibicao(String vendedorNomeexibicao) {
		vendedor_nomeexibicao = vendedorNomeexibicao;
	}

	public void setVendedor_telefones(String vendedorTelefones) {
		vendedor_telefones = vendedorTelefones;
	}

	public void setPedidovenda_tipo(String pedidovendaTipo) {
		pedidovenda_tipo = pedidovendaTipo;
	}

	public void setCliente_identificador(String clienteIdentificador) {
		cliente_identificador = clienteIdentificador;
	}
	
	public void setCliente_nome(String clienteNome) {
		cliente_nome = clienteNome;
	}

	public void setCliente_razaosocial(String clienteRazaosocial) {
		cliente_razaosocial = clienteRazaosocial;
	}

	public void setCliente_cpf(String clienteCpf) {
		cliente_cpf = clienteCpf;
	}

	public void setCliente_cnpj(String clienteCnpj) {
		cliente_cnpj = clienteCnpj;
	}

	public void setCliente_email(String clienteEmail) {
		cliente_email = clienteEmail;
	}

	public void setCliente_endereco(String clienteEndereco) {
		cliente_endereco = clienteEndereco;
	}

	public void setCliente_inscricaoestadual(String clienteInscricaoestadual) {
		cliente_inscricaoestadual = clienteInscricaoestadual;
	}

	public void setCliente_observacao(String clienteObservacao) {
		cliente_observacao = clienteObservacao;
	}

	public void setCliente_rg(String clienteRg) {
		cliente_rg = clienteRg;
	}

	public void setCliente_responsavel(String clienteResponsavel) {
		cliente_responsavel = clienteResponsavel;
	}

	public void setCliente_telefones(String clienteTelefones) {
		cliente_telefones = clienteTelefones;
	}

	public void setCliente_contatonome(String clienteContatonome) {
		cliente_contatonome = clienteContatonome;
	}

	public void setCliente_contatoemail(String clienteContatoemail) {
		cliente_contatoemail = clienteContatoemail;
	}

	public void setCliente_contatotelefones(String clienteContatotelefones) {
		cliente_contatotelefones = clienteContatotelefones;
	}

	public void setProjeto_nome(String projetoNome) {
		projeto_nome = projetoNome;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public void setCodigoorcamento(Integer codigoorcamento) {
		this.codigoorcamento = codigoorcamento;
	}

	public void setData(String data) {
		this.data = data;
	}

	public void setData_hora(String dataHora) {
		data_hora = dataHora;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public void setIdentificadorexterno(String identificadorexterno) {
		this.identificadorexterno = identificadorexterno;
	}

	public void setDatahoraatualizacao(String datahoraatualizacao) {
		this.datahoraatualizacao = datahoraatualizacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setObservacaointerna(String observacaointerna) {
		this.observacaointerna = observacaointerna;
	}

	public void setDataentrega(String dataentrega) {
		this.dataentrega = dataentrega;
	}

	public void setValorfrete(String valorfrete) {
		this.valorfrete = valorfrete;
	}

	public void setValordesconto(String valordesconto) {
		this.valordesconto = valordesconto;
	}

	public void setTerceiro(String terceiro) {
		this.terceiro = terceiro;
	}

	public void setValortotal(String valortotal) {
		this.valortotal = valortotal;
	}

	public void setValorfinalmaisipi(String valorfinalmaisipi) {
		this.valorfinalmaisipi = valorfinalmaisipi;
	}

	public void setDescricaofrete(String descricaofrete) {
		this.descricaofrete = descricaofrete;
	}

	public void setPrazopagamento(String prazopagamento) {
		this.prazopagamento = prazopagamento;
	}

	public void setFormapagamento(String formapagamento) {
		this.formapagamento = formapagamento;
	}

	public void setEmpresa_nome(String empresaNome) {
		empresa_nome = empresaNome;
	}

	public void setEmpresa_cnpj(String empresaCnpj) {
		empresa_cnpj = empresaCnpj;
	}

	public void setEmpresa_inscricaoestadual(String empresaInscricaoestadual) {
		empresa_inscricaoestadual = empresaInscricaoestadual;
	}

	public void setEmpresa_endereco(String empresaEndereco) {
		empresa_endereco = empresaEndereco;
	}

	public void setEmpresa_telefone(String empresaTelefone) {
		empresa_telefone = empresaTelefone;
	}

	public void setEmpresa_site(String empresaSite) {
		empresa_site = empresaSite;
	}

	public void setEmpresa_email(String empresaEmail) {
		empresa_email = empresaEmail;
	}

	public void setEmpresa_municipio(String empresaMunicipio) {
		empresa_municipio = empresaMunicipio;
	}

	public void setPesoliquidototal(Double pesoliquidototal) {
		this.pesoliquidototal = pesoliquidototal;
	}

	public void setPesobrutototal(Double pesobrutototal) {
		this.pesobrutototal = pesobrutototal;
	}

	public void setTotalmetrocubico(String totalmetrocubico) {
		this.totalmetrocubico = totalmetrocubico;
	}

	public void setProduto_valortotal(String produtoValortotal) {
		produto_valortotal = produtoValortotal;
	}

	public void setProduto_qtdetotalproduto(String produtoQtdetotalproduto) {
		produto_qtdetotalproduto = produtoQtdetotalproduto;
	}

	public void setProduto_qtdetotalservico(String produtoQtdetotalservico) {
		produto_qtdetotalservico = produtoQtdetotalservico;
	}

	public void setListaVendaItens(LinkedList<VendaItemBean> listaVendaItens) {
		this.listaVendaItens = listaVendaItens;
	}

	public String getPagamento_valortotal() {
		return pagamento_valortotal;
	}

	public void setPagamento_valortotal(String pagamentoValortotal) {
		pagamento_valortotal = pagamentoValortotal;
	}

	public LinkedList<VendaPagamentoBean> getListaVendaPagamento() {
		return listaVendaPagamento;
	}

	public void setListaVendaPagamento(LinkedList<VendaPagamentoBean> listaVendaPagamento) {
		this.listaVendaPagamento = listaVendaPagamento;
	}
	
	public void preencherCamposNulos(){
		if(venda_situacaonome == null) venda_situacaonome = "";
		if(vendedor_nome == null) vendedor_nome = "";
		if(vendedor_email == null) vendedor_email = "";
		if(vendedor_nomeexibicao == null) vendedor_nomeexibicao = "";
		if(vendedor_telefones == null) vendedor_telefones = "";
		if(pedidovenda_tipo == null) pedidovenda_tipo = "";
		
		if(cliente_nome == null) cliente_nome = "";
		if(cliente_razaosocial == null) cliente_razaosocial = "";
		if(cliente_cpf == null) cliente_cpf = "";
		if(cliente_cnpj == null) cliente_cnpj = "";
		if(cliente_email == null) cliente_email = "";
		if(cliente_endereco == null) cliente_endereco = "";
		if(cliente_inscricaoestadual == null) cliente_inscricaoestadual = "";
		if(cliente_observacao == null) cliente_observacao = "";
		if(cliente_rg == null) cliente_rg = "";
		if(cliente_responsavel == null) cliente_responsavel = "";
		if(cliente_telefones == null) cliente_telefones = "";
		
		if(cliente_contatonome == null) cliente_contatonome = "";
		if(cliente_contatoemail == null) cliente_contatoemail = "";
		if(cliente_contatotelefones == null) cliente_contatotelefones = "";

		if(projeto_nome == null) projeto_nome = "";
		
		if(data == null) data = "";
		if(data_hora == null) data_hora = "";
		if(endereco == null) endereco = "";
		if(identificador == null) identificador = "";
		if(identificadorexterno == null) identificadorexterno = "";
		if(datahoraatualizacao == null) datahoraatualizacao = "";
		
		if(observacao == null) observacao = "";
		if(observacaointerna == null) observacaointerna = "";
		if(dataentrega == null) dataentrega = "";
		if(valorfrete == null) valorfrete = "";
		if(valordesconto == null) valordesconto = "";
		if(terceiro == null) terceiro = "";
		if(valortotal == null) valortotal = "";
		if(valorfinalmaisipi == null) valorfinalmaisipi = "";
		if(descricaofrete == null) descricaofrete = "";
		if(prazopagamento == null) prazopagamento = "";
		if(formapagamento == null) formapagamento = "";
		
		if(empresa_nome == null) empresa_nome = "";
		if(empresa_cnpj == null) empresa_cnpj = "";
		if(empresa_inscricaoestadual == null) empresa_inscricaoestadual = "";
		if(empresa_endereco == null) empresa_endereco = "";
		if(empresa_telefone == null) empresa_telefone = "";
		if(empresa_site == null) empresa_site = "";
		if(empresa_email == null) empresa_email = "";
		if(empresa_municipio == null) empresa_municipio = "";
		
		if(pesoliquidototal == null) pesoliquidototal = 0d;
		if(pesobrutototal == null) pesobrutototal = 0d;
		if(totalmetrocubico == null) totalmetrocubico = "";
		if(produto_valortotal == null) produto_valortotal = "";
		if(produto_qtdetotalproduto == null) produto_qtdetotalproduto = "";
		if(produto_qtdetotalservico == null) produto_qtdetotalservico = "";
		if(pagamento_valortotal == null) pagamento_valortotal = "";
	}
}
	
