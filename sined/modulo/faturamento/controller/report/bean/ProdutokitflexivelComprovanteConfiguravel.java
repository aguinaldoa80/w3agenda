package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Material;

public class ProdutokitflexivelComprovanteConfiguravel {
	
	
	private String CODIGO;
	private String DESCRICAO;
	private Double QUANTIDADE;
	private Double PRECO;
	private String TOTAL;
	private Double TOTALKIT;
	private String CDMATERIALGRUPO;
	private String NCMCOMPLETO;
	private String IMAGEM;
	private String OBSERVACAO;
	private String CARACTERISTICA;
	private Double PESOLIQUIDO;
	private Double PESOBRUTO;
	private String IDENTIFICADORMATERIALMESTRE;
	private Double DESCONTO;
	private String ENTREGA;
	private String UNIDADEMEDIDA;
	
	private List<ProdutokitflexivelitemComprovanteConfiguravel> LISTAPRODUTOKIFLEXIVELTITEM = new ArrayList<ProdutokitflexivelitemComprovanteConfiguravel>();

	private Material material;
	
	public String getCODIGO() {
		return CODIGO;
	}
	public String getDESCRICAO() {
		return DESCRICAO;
	}
	public Double getQUANTIDADE() {
		return QUANTIDADE;
	}
	public Double getPRECO() {
		return PRECO;
	}
	public String getTOTAL() {
		return TOTAL != null ? TOTAL : (getTOTALKIT() != null ? new Money(getTOTALKIT()).toString() : "");
	}
	public List<ProdutokitflexivelitemComprovanteConfiguravel> getLISTAPRODUTOKIFLEXIVELTITEM() {
		return LISTAPRODUTOKIFLEXIVELTITEM;
	}
	
	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}
	public void setDESCRICAO(String dESCRICAO) {
		DESCRICAO = dESCRICAO;
	}
	public void setQUANTIDADE(Double qUANTIDADE) {
		QUANTIDADE = qUANTIDADE;
	}
	public void setPRECO(Double pRECO) {
		PRECO = pRECO;
	}
	public void setTOTAL(String tOTAL) {
		TOTAL = tOTAL;
	}
	public void setLISTAPRODUTOKIFLEXIVELTITEM(List<ProdutokitflexivelitemComprovanteConfiguravel> lISTAPRODUTOKIFLEXIVELTITEM) {
		LISTAPRODUTOKIFLEXIVELTITEM = lISTAPRODUTOKIFLEXIVELTITEM;
	}

	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public String getCDMATERIALGRUPO() {
		return CDMATERIALGRUPO;
	}
	public String getNCMCOMPLETO() {
		return NCMCOMPLETO;
	}
	public String getIMAGEM() {
		return IMAGEM;
	}
	public String getOBSERVACAO() {
		return OBSERVACAO;
	}
	public String getCARACTERISTICA() {
		return CARACTERISTICA;
	}
	public Double getPESOLIQUIDO() {
		return PESOLIQUIDO;
	}
	public Double getPESOBRUTO() {
		return PESOBRUTO;
	}
	public Double getTOTALKIT() {
		return TOTALKIT;
	}
	
	public void setCDMATERIALGRUPO(String cDMATERIALGRUPO) {
		CDMATERIALGRUPO = cDMATERIALGRUPO;
	}
	public void setNCMCOMPLETO(String nCMCOMPLETO) {
		NCMCOMPLETO = nCMCOMPLETO;
	}
	public void setIMAGEM(String iMAGEM) {
		IMAGEM = iMAGEM;
	}
	public void setOBSERVACAO(String oBSERVACAO) {
		OBSERVACAO = oBSERVACAO;
	}
	public void setCARACTERISTICA(String cARACTERISTICA) {
		CARACTERISTICA = cARACTERISTICA;
	}
	public void setPESOLIQUIDO(Double pESOLIQUIDO) {
		PESOLIQUIDO = pESOLIQUIDO;
	}
	public void setPESOBRUTO(Double pESOBRUTO) {
		PESOBRUTO = pESOBRUTO;
	}
	public String getIDENTIFICADORMATERIALMESTRE() {
		return IDENTIFICADORMATERIALMESTRE;
	}
	public void setIDENTIFICADORMATERIALMESTRE(String iDENTIFICADORMATERIALMESTRE) {
		IDENTIFICADORMATERIALMESTRE = iDENTIFICADORMATERIALMESTRE;
	}
	public Double getDESCONTO() {
		return DESCONTO;
	}
	public void setDESCONTO(Double dESCONTO) {
		DESCONTO = dESCONTO;
	}
	public String getENTREGA() {
		return ENTREGA;
	}
	public void setENTREGA(String eNTREGA) {
		ENTREGA = eNTREGA;
	}
	public void setTOTALKIT(Double tOTALKIT) {
		TOTALKIT = tOTALKIT;
	}
	public String getUNIDADEMEDIDA() {
		return UNIDADEMEDIDA;
	}
	public void setUNIDADEMEDIDA(String uNIDADEMEDIDA) {
		UNIDADEMEDIDA = uNIDADEMEDIDA;
	}	
}