package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;


public class PedidoVendaItemBean {
	
	private Integer cdpedidovendamaterial;
	
	private Integer material_cdmaterial;
	private String material_codigo;
	private String material_identificador;
	private String material_nome;
	private String material_nomenf;
	
	private String banda_codigo;
	private String banda_identificador;
	private String banda_descricao;
	
	private String pneu_marca;
	private String pneu_serie;
	private String pneu_dot;
	private String pneu_modelo;
	private String pneu_medida;
	private String pneu_numeroreforma;
	private Integer pneu_pneuid;
	private String pneu_roda;
	private String pneu_lonas;
	private String pneu_descricao;
	private String otrPneuDesenho;
	private String otrPneuTipoServico;
	
	public Integer getCdpedidovendamaterial() {
		return cdpedidovendamaterial;
	}
	public Integer getMaterial_cdmaterial() {
		return material_cdmaterial;
	}
	public String getMaterial_codigo() {
		return material_codigo;
	}
	public String getMaterial_identificador() {
		return material_identificador;
	}
	public String getMaterial_nome() {
		return material_nome;
	}
	public String getMaterial_nomenf() {
		return material_nomenf;
	}
	public String getBanda_codigo() {
		return banda_codigo;
	}
	public String getBanda_identificador() {
		return banda_identificador;
	}
	public String getBanda_descricao() {
		return banda_descricao;
	}
	public String getPneu_marca() {
		return pneu_marca;
	}
	public String getPneu_serie() {
		return pneu_serie;
	}
	public String getPneu_dot() {
		return pneu_dot;
	}
	public String getPneu_modelo() {
		return pneu_modelo;
	}
	public String getPneu_medida() {
		return pneu_medida;
	}
	public String getPneu_numeroreforma() {
		return pneu_numeroreforma;
	}
	public Integer getPneu_pneuid() {
		return pneu_pneuid;
	}
	public void setCdpedidovendamaterial(Integer cdpedidovendamaterial) {
		this.cdpedidovendamaterial = cdpedidovendamaterial;
	}
	public void setMaterial_cdmaterial(Integer materialCdmaterial) {
		material_cdmaterial = materialCdmaterial;
	}
	public void setMaterial_codigo(String materialCodigo) {
		material_codigo = materialCodigo;
	}
	public void setMaterial_identificador(String materialIdentificador) {
		material_identificador = materialIdentificador;
	}
	public void setMaterial_nome(String materialNome) {
		material_nome = materialNome;
	}
	public void setMaterial_nomenf(String material_nomenf) {
		this.material_nomenf = material_nomenf;
	}
	public void setBanda_codigo(String bandaCodigo) {
		banda_codigo = bandaCodigo;
	}
	public void setBanda_identificador(String bandaIdentificador) {
		banda_identificador = bandaIdentificador;
	}
	public void setBanda_descricao(String bandaDescricao) {
		banda_descricao = bandaDescricao;
	}
	public void setPneu_marca(String pneuMarca) {
		pneu_marca = pneuMarca;
	}
	public void setPneu_serie(String pneuSerie) {
		pneu_serie = pneuSerie;
	}
	public void setPneu_dot(String pneuDot) {
		pneu_dot = pneuDot;
	}
	public void setPneu_modelo(String pneuModelo) {
		pneu_modelo = pneuModelo;
	}
	public void setPneu_medida(String pneuMedida) {
		pneu_medida = pneuMedida;
	}
	public void setPneu_numeroreforma(String pneuNumeroreforma) {
		pneu_numeroreforma = pneuNumeroreforma;
	}
	public void setPneu_pneuid(Integer pneuPneuid) {
		pneu_pneuid = pneuPneuid;
	}	
	public String getPneu_roda() {
		return pneu_roda;
	}
	public String getPneu_lonas() {
		return pneu_lonas;
	}
	public String getPneu_descricao() {
		return pneu_descricao;
	}
	public String getOtrPneuDesenho() {
		return otrPneuDesenho;
	}
	public String getOtrPneuTipoServico() {
		return otrPneuTipoServico;
	}
	public void setPneu_roda(String pneu_roda) {
		this.pneu_roda = pneu_roda;
	}
	public void setPneu_lonas(String pneu_lonas) {
		this.pneu_lonas = pneu_lonas;
	}
	public void setPneu_descricao(String pneu_descricao) {
		this.pneu_descricao = pneu_descricao;
	}
	public void setOtrPneuDesenho(String otrPneuDesenho) {
		this.otrPneuDesenho = otrPneuDesenho;
	}
	public void setOtrPneuTipoServico(String otrPneuTipoServico) {
		this.otrPneuTipoServico = otrPneuTipoServico;
	}
	public void preencherCamposNulos(){
		if(material_codigo == null) material_codigo = "";
		if(material_identificador == null) material_identificador = "";
		if(material_nome == null) material_nome = "";
		
		if(banda_codigo == null) banda_codigo = "";
		if(banda_identificador == null) banda_identificador = "";
		if(banda_descricao == null) banda_descricao = "";
		
		if(pneu_marca == null) pneu_marca = "";
		if(pneu_serie == null) pneu_serie = "";
		if(pneu_dot == null) pneu_dot = "";
		if(pneu_modelo == null) pneu_modelo = "";
		if(pneu_medida == null) pneu_medida = "";
		if(pneu_numeroreforma == null) pneu_numeroreforma = "";
		if(pneu_roda == null) pneu_roda = "";
		if(pneu_lonas == null) pneu_lonas = "";
		if(pneu_descricao == null) pneu_descricao = "";
		if(otrPneuDesenho == null) otrPneuDesenho = "";
		if(otrPneuTipoServico == null) otrPneuTipoServico = "";
	}
}
	
