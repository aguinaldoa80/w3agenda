package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.util.SinedUtil;


public class EmpresaBean {
	
	protected String empresa_nome;
	protected String empresa_razaosocial;
	protected String empresa_cnpj;
	protected String empresa_logradouro;
	protected String empresa_numero;
	protected String empresa_bairro;
	protected String empresa_municipio;
	protected String empresa_uf;
	protected String empresa_telefone;
	protected String empresa_email;
	protected String empresa_logomarca;
	
	
	public EmpresaBean (Empresa empresa){
		if (empresa != null) {
			this.setEmpresa_nome(empresa.getNome());
			this.setEmpresa_razaosocial(empresa.getRazaosocial());
			this.setEmpresa_cnpj(empresa.getCpfOuCnpjValueProprietarioPrincipalOuEmpresa() != null? empresa.getCpfOuCnpjValueProprietarioPrincipalOuEmpresa(): "");

			Endereco endereco = empresa.getEndereco();
			if (endereco != null) {
				this.setEmpresa_logradouro(endereco.getLogradouro());
				this.setEmpresa_numero(empresa.getEndereco().getNumero());
				this.setEmpresa_bairro(empresa.getEndereco().getBairro());
				this.setEmpresa_municipio( empresa.getEndereco().getMunicipio().getNome());
				this.setEmpresa_uf( empresa.getEndereco().getMunicipio().getUf().getSigla());
			}	
			
			if (empresa.getTelefone() != null) {
				this.setEmpresa_telefone(empresa.getTelefone().getTelefone());	
			}
			this.setEmpresa_email(empresa.getEmail());			
			this.setEmpresa_logomarca(SinedUtil.getLogoURLForReport(empresa));
			
			
		}
		
	}

	public String getEmpresa_nome() {
		return empresa_nome;
	}

	public void setEmpresa_nome(String empresa_nome) {
		this.empresa_nome = empresa_nome;
	}

	public String getEmpresa_razaosocial() {
		return empresa_razaosocial;
	}

	public void setEmpresa_razaosocial(String empresa_razaosocial) {
		this.empresa_razaosocial = empresa_razaosocial;
	}

	public String getEmpresa_cnpj() {
		return empresa_cnpj;
	}

	public void setEmpresa_cnpj(String empresa_cnpj) {
		this.empresa_cnpj = empresa_cnpj;
	}

	public String getEmpresa_telefone() {
		return empresa_telefone;
	}

	public void setEmpresa_telefone(String empresa_telefone) {
		this.empresa_telefone = empresa_telefone;
	}

	public String getEmpresa_email() {
		return empresa_email;
	}

	public void setEmpresa_email(String empresa_email) {
		this.empresa_email = empresa_email;
	}

	public String getEmpresa_logradouro() {
		return empresa_logradouro;
	}

	public void setEmpresa_logradouro(String empresa_logradouro) {
		this.empresa_logradouro = empresa_logradouro;
	}

	public String getEmpresa_numero() {
		return empresa_numero;
	}

	public void setEmpresa_numero(String empresa_numero) {
		this.empresa_numero = empresa_numero;
	}

	public String getEmpresa_bairro() {
		return empresa_bairro;
	}

	public void setEmpresa_bairro(String empresa_bairro) {
		this.empresa_bairro = empresa_bairro;
	}

	public String getEmpresa_municipio() {
		return empresa_municipio;
	}

	public void setEmpresa_municipio(String empresa_municipio) {
		this.empresa_municipio = empresa_municipio;
	}

	public String getEmpresa_uf() {
		return empresa_uf;
	}

	public void setEmpresa_uf(String empresa_uf) {
		this.empresa_uf = empresa_uf;
	}

	public String getEmpresa_logomarca() {
		return empresa_logomarca;
	}

	public void setEmpresa_logomarca(String empresa_logo) {
		this.empresa_logomarca = empresa_logo;
	}
	
}
