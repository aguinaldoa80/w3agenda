package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.LinkedList;

import br.com.linkcom.neo.types.Money;

public class OrdemCompraBean {

	private String ano;
	private Integer numero;
	private String projetos;
	private String observacao;
	private String observacao_grupo_material;
	private Money frete;
	private Money desconto;
	private String proxima_entrega;
	private String tipo_frete;
	private Double icms_st;
	private String transportador;
	private String transportador_telefone;
	private String inspecao;
	private Boolean exibir_inspecao;
	private Boolean exige_aprovacao;
	private String local_armazenagem_nome;
	private String local_armazenagem_endereco;
	private String prazo_pagamento;
	private String garantia;
	private String contas_gerenciais;
	private String tipo_documento;
	private String criada_por;
	private String criada_por_rubrica;
	private String data_criacao;
	private String autorizada_por;
	private String autorizada_por_rubrica;
	private String aprovada_por;
	private String aprovada_por_rubrica;
	private String data_emissao;
	private Money total_item;
	private Money total;
	private OrdemcompraEmpresaBean empresa;
	private OrdemcompraClienteBean cliente;
	private OrdemcompraContatoBean contato;
	private OrdemcompraFornecedorBean fornecedor;
	private OrdemcompraFornecedorBean fornecedoroptriangular;
	private AvaliacaoBean avaliacaoBean;
	
	private LinkedList<OrdemCompraItemBean> itens = new LinkedList<OrdemCompraItemBean>();

	public String getAno() {
		return ano;
	}
	
	public String getAprovada_por() {
		return aprovada_por;
	}

	public String getAutorizada_por() {
		return autorizada_por;
	}

	public String getContas_gerenciais() {
		return contas_gerenciais;
	}

	public OrdemcompraContatoBean getContato() {
		return contato;
	}

	public String getCriada_por() {
		return criada_por;
	}

	public String getData_emissao() {
		return data_emissao;
	}

	public String getData_criacao() {
		return data_criacao;
	}

	public Money getDesconto() {
		return desconto;
	}

	public OrdemcompraEmpresaBean getEmpresa() {
		return empresa;
	}
	
	public OrdemcompraClienteBean getCliente() {
		return cliente;
	}

	public Boolean getExibir_inspecao() {
		return exibir_inspecao;
	}

	public OrdemcompraFornecedorBean getFornecedor() {
		return fornecedor;
	}

	public OrdemcompraFornecedorBean getFornecedoroptriangular() {
		return fornecedoroptriangular;
	}

	public Money getFrete() {
		return frete;
	}

	public String getGarantia() {
		return garantia;
	}

	public Double getIcms_st() {
		return icms_st;
	}

	public String getInspecao() {
		return inspecao;
	}

	public LinkedList<OrdemCompraItemBean> getItens() {
		return itens;
	}

	public String getLocal_armazenagem_endereco() {
		return local_armazenagem_endereco;
	}

	public String getLocal_armazenagem_nome() {
		return local_armazenagem_nome;
	}

	public Integer getNumero() {
		return numero;
	}

	public String getObservacao() {
		return observacao;
	}

	public String getObservacao_grupo_material() {
		return observacao_grupo_material;
	}

	public String getPrazo_pagamento() {
		return prazo_pagamento;
	}

	public String getProjetos() {
		return projetos;
	}

	public String getProxima_entrega() {
		return proxima_entrega;
	}

	public String getTipo_documento() {
		return tipo_documento;
	}

	public String getTipo_frete() {
		return tipo_frete;
	}

	public String getTransportador() {
		return transportador;
	}

	public String getTransportador_telefone() {
		return transportador_telefone;
	}
	
	public AvaliacaoBean getAvaliacaoBean(){
		return avaliacaoBean;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}
	
	public void setAprovada_por(String aprovada_por) {
		this.aprovada_por = aprovada_por;
	}

	public void setAutorizada_por(String autorizada_por) {
		this.autorizada_por = autorizada_por;
	}

	public void setContas_gerenciais(String contas_gerenciais) {
		this.contas_gerenciais = contas_gerenciais;
	}

	public void setContato(OrdemcompraContatoBean contato) {
		this.contato = contato;
	}

	public void setCriada_por(String criada_por) {
		this.criada_por = criada_por;
	}

	public void setData_emissao(String data) {
		this.data_emissao = data;
	}

	public void setData_criacao(String data_criacao) {
		this.data_criacao = data_criacao;
	}

	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}

	public void setEmpresa(OrdemcompraEmpresaBean empresa) {
		this.empresa = empresa;
	}
	
	public void setCliente(OrdemcompraClienteBean cliente) {
		this.cliente = cliente;
	}

	public void setExibir_inspecao(Boolean exibir_inspecao) {
		this.exibir_inspecao = exibir_inspecao;
	}

	public void setFornecedor(OrdemcompraFornecedorBean fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	public void setFornecedoroptriangular(OrdemcompraFornecedorBean fornecedoroptriangular) {
		this.fornecedoroptriangular = fornecedoroptriangular;
	}
	
	public void setFrete(Money frete) {
		this.frete = frete;
	}

	public void setGarantia(String garantia) {
		this.garantia = garantia;
	}

	public void setIcms_st(Double icmsst) {
		this.icms_st = icmsst;
	}

	public void setInspecao(String inspecao) {
		this.inspecao = inspecao;
	}

	public void setItens(LinkedList<OrdemCompraItemBean> itens) {
		this.itens = itens;
	}

	public void setLocal_armazenagem_endereco(String local_armazenagem_endereco) {
		this.local_armazenagem_endereco = local_armazenagem_endereco;
	}

	public void setLocal_armazenagem_nome(String local_armazenagem_nome) {
		this.local_armazenagem_nome = local_armazenagem_nome;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setObservacao_grupo_material(String observacao_grupo_material) {
		this.observacao_grupo_material = observacao_grupo_material;
	}

	public void setPrazo_pagamento(String prazo_pagamento) {
		this.prazo_pagamento = prazo_pagamento;
	}

	public void setProjetos(String projetos) {
		this.projetos = projetos;
	}

	public void setProxima_entrega(String proxima_entrega) {
		this.proxima_entrega = proxima_entrega;
	}

	public void setTipo_documento(String tipo_documento) {
		this.tipo_documento = tipo_documento;
	}

	public void setTipo_frete(String tipo_frete) {
		this.tipo_frete = tipo_frete;
	}

	public void setTransportador(String transportador) {
		this.transportador = transportador;
	}

	public void setTransportador_telefone(String transportador_telefone) {
		this.transportador_telefone = transportador_telefone;
	}

	public Money getTotal_item() {
		return total_item;
	}

	public void setTotal_item(Money total_item) {
		this.total_item = total_item;
	}

	public Money getTotal() {
		return total;
	}

	public void setTotal(Money total) {
		this.total = total;
	}

	public String getCriada_por_rubrica() {
		return criada_por_rubrica;
	}

	public void setCriada_por_rubrica(String criada_por_rubrica) {
		this.criada_por_rubrica = criada_por_rubrica;
	}

	public String getAutorizada_por_rubrica() {
		return autorizada_por_rubrica;
	}

	public void setAutorizada_por_rubrica(String autorizada_por_rubrica) {
		this.autorizada_por_rubrica = autorizada_por_rubrica;
	}

	public String getAprovada_por_rubrica() {
		return aprovada_por_rubrica;
	}

	public void setAprovada_por_rubrica(String aprovada_por_rubrica) {
		this.aprovada_por_rubrica = aprovada_por_rubrica;
	}

	public Boolean getExige_aprovacao() {
		return exige_aprovacao;
	}
	
	public void setExige_aprovacao(Boolean exige_aprovacao) {
		this.exige_aprovacao = exige_aprovacao;
	}
	
	public void setAvaliacaoBean(AvaliacaoBean avaliacaoBean) {
		this.avaliacaoBean = avaliacaoBean;
	}
}
