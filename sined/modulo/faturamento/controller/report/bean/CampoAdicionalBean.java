package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Vendavalorcampoextra;

public class CampoAdicionalBean {

	protected String nome;
	protected String valor;
	
	public CampoAdicionalBean(Vendavalorcampoextra vendavalorcampoextra){
		if(vendavalorcampoextra != null){
			if(vendavalorcampoextra.getCampoextrapedidovendatipo() != null){
				this.setNome(vendavalorcampoextra.getCampoextrapedidovendatipo().getNome());
			}
			this.setValor(vendavalorcampoextra.getValor());
		}
	}
	
	public String getNome() {
		return Util.strings.emptyIfNull(nome);
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getValor() {
		return Util.strings.emptyIfNull(valor);
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
}
