package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

public class ExpedicaoEtiquetaBean {
	
	private String nome_empresa;
	private Integer codigo_venda;
	private String nome_cliente;
	private String contato_cliente;
	private String endereco_cliente;
	private String identificador_produto;
	private String identificador_nome_produto;
	private String qtde;
	private String dimensoes;
	private List<Image> listaImagem = new ArrayList<Image>();
	
	public String getNome_empresa() {
		return nome_empresa;
	}
	public Integer getCodigo_venda() {
		return codigo_venda;
	}
	public String getNome_cliente() {
		return nome_cliente;
	}
	public String getContato_cliente() {
		return contato_cliente;
	}
	public String getEndereco_cliente() {
		return endereco_cliente;
	}
	public String getIdentificador_produto() {
		return identificador_produto;
	}
	public String getIdentificador_nome_produto() {
		return identificador_nome_produto;
	}
	public String getQtde() {
		return qtde;
	}
	public String getDimensoes() {
		return dimensoes;
	}
	public List<Image> getListaImagem() {
		return listaImagem;
	}
	public void setListaImagem(List<Image> listaImagem) {
		this.listaImagem = listaImagem;
	}
	public void setQtde(String qtde) {
		this.qtde = qtde;
	}
	public void setDimensoes(String dimensoes) {
		this.dimensoes = dimensoes;
	}
	public void setNome_empresa(String nomeEmpresa) {
		nome_empresa = nomeEmpresa;
	}
	public void setCodigo_venda(Integer codigoVenda) {
		codigo_venda = codigoVenda;
	}
	public void setNome_cliente(String nomeCliente) {
		nome_cliente = nomeCliente;
	}
	public void setContato_cliente(String contatoCliente) {
		contato_cliente = contatoCliente;
	}
	public void setEndereco_cliente(String enderecoCliente) {
		endereco_cliente = enderecoCliente;
	}
	public void setIdentificador_produto(String identificadorProduto) {
		identificador_produto = identificadorProduto;
	}
	public void setIdentificador_nome_produto(String identificadorNomeProduto) {
		identificador_nome_produto = identificadorNomeProduto;
	}

}
