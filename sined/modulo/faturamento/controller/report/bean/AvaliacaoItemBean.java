package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

public class AvaliacaoItemBean {

	private String pergunta;	
	private String resposta;
	private Integer pontuacao;
	
	public String getPergunta() {
		return pergunta;
	}
	public String getResposta() {
		return resposta;
	}
	public Integer getPontuacao() {
		return pontuacao;
	}
	public void setPergunta(String pergunta) {
		this.pergunta = pergunta;
	}
	public void setResposta(String resposta) {
		this.resposta = resposta;
	}
	public void setPontuacao(Integer pontuacao) {
		this.pontuacao = pontuacao;
	}

}
