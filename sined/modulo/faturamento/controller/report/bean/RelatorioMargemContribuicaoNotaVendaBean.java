package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Venda;

public class RelatorioMargemContribuicaoNotaVendaBean {
	
	private Venda venda;
	private Notafiscalproduto notafiscalproduto;
	private String descricao;
	private String numero;
	private String clienteNome;
	private Money valorBruto;
	
	private String idsVendaMaterial;
	private String idsNotaItens;
	
	public Venda getVenda() {
		return venda;
	}
	
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}
	
	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}
	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getClienteNome() {
		return clienteNome;
	}

	public void setClienteNome(String clienteNome) {
		this.clienteNome = clienteNome;
	}

	public Money getValorBruto() {
		return valorBruto;
	}

	public void setValorBruto(Money valorBruto) {
		this.valorBruto = valorBruto;
	}
	
	public void addValorBruto(Money valorBruto) {
		if (this.valorBruto == null) {
			this.valorBruto = new Money();
		}
		
		if (valorBruto == null) {
			valorBruto = new Money();
		}
		
		this.valorBruto = this.valorBruto.add(valorBruto);
	}
	
	public String getIdsNotaItens() {
		return idsNotaItens;
	}
	
	public void setIdsNotaItens(String idsNotaItens) {
		this.idsNotaItens = idsNotaItens;
	}
	
	public String getIdsVendaMaterial() {
		return idsVendaMaterial;
	}
	
	public void setIdsVendaMaterial(String idsVendaMaterial) {
		this.idsVendaMaterial = idsVendaMaterial;
	}
}