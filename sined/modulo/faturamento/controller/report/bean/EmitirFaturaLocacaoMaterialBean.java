package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import br.com.linkcom.neo.types.Money;

public class EmitirFaturaLocacaoMaterialBean {
	
	private String material_nome;
	private String patrimonio_plaqueta;
	private Money valor;
	
	public String getMaterial_nome() {
		return material_nome;
	}
	public String getPatrimonio_plaqueta() {
		return patrimonio_plaqueta;
	}
	public Money getValor() {
		return valor;
	}
	public void setMaterial_nome(String materialNome) {
		material_nome = materialNome;
	}
	public void setPatrimonio_plaqueta(String patrimonioPlaqueta) {
		patrimonio_plaqueta = patrimonioPlaqueta;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
}
