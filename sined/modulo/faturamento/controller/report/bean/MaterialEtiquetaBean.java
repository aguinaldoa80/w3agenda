package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Ordemcompra;

public class MaterialEtiquetaBean{

	private Material material;
	private String lote;
	private String validadeLote;
	private Integer numerocopiasimpressaoetiqueta;
	private String empresaRecebimento;
	private Integer cdEntrega;
	private Integer cdEntregaMaterial;
	private Integer cdordemcompra;
	private Integer cdentregadocumento;
	private Integer cdmovimentacaoestoque;
	private String tipoetiqueta;
	
	public MaterialEtiquetaBean() {}
	
	public MaterialEtiquetaBean(MaterialEtiquetaBean materialEtiqueta, Ordemcompra ordemcompra, Movimentacaoestoque movimentacaoestoque) {
		this(materialEtiqueta);
		if(ordemcompra != null)
			setCdordemcompra(ordemcompra.getCdordemcompra());
		if(movimentacaoestoque != null)
			setCdmovimentacaoestoque(movimentacaoestoque.getCdmovimentacaoestoque());
	}
	
	public MaterialEtiquetaBean(MaterialEtiquetaBean materialEtq, Ordemcompra ordemcompra) {
		this(materialEtq, ordemcompra, null);
	}
	
	public MaterialEtiquetaBean(MaterialEtiquetaBean materialEtq, Movimentacaoestoque movimentacaoestoque) {
		this(materialEtq, null, movimentacaoestoque);
	}
	
	private MaterialEtiquetaBean(MaterialEtiquetaBean materialEtiqueta){
		setMaterial(materialEtiqueta.getMaterial());
		setLote(materialEtiqueta.getLote());
		setValidadeLote(materialEtiqueta.getValidadeLote());
		setNumerocopiasimpressaoetiqueta(materialEtiqueta.getNumerocopiasimpressaoetiqueta());
		setEmpresaRecebimento(materialEtiqueta.getEmpresaRecebimento());
		setCdEntrega(materialEtiqueta.getCdEntrega());
		setCdentregadocumento(materialEtiqueta.getCdentregadocumento());
		setCdEntregaMaterial(materialEtiqueta.getCdEntregaMaterial());
		setCdordemcompra(materialEtiqueta.getCdordemcompra());
		setCdmovimentacaoestoque(materialEtiqueta.getCdmovimentacaoestoque());
		setTipoetiqueta(materialEtiqueta.getTipoetiqueta());
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(!(obj instanceof MaterialEtiquetaBean))
			return false;
		MaterialEtiquetaBean inst = (MaterialEtiquetaBean) obj;
		if(tipoetiqueta == null || tipoetiqueta.isEmpty() || !"configuravel".equals(tipoetiqueta))
			return equalsDefault(inst);
		return equalsDefault(inst) && equalsComplement(inst);
	}
	
	private boolean equalsComplement(MaterialEtiquetaBean inst) {
		if(inst.getCdmovimentacaoestoque() == null || cdmovimentacaoestoque == null  || !inst.getCdmovimentacaoestoque().equals(cdmovimentacaoestoque))
			return false;
		if(inst.getCdordemcompra() == null || cdordemcompra == null || !inst.getCdordemcompra().equals(cdordemcompra))
			return false;
		return true;
	}

	private boolean equalsDefault(MaterialEtiquetaBean inst){
		if(inst.getMaterial() == null || material == null || !material.equals(inst.getMaterial()))
			return false;
		if(inst.getLote() == null || lote == null || !lote.equals(inst.getLote()))
			return false;
		if(inst.getEmpresaRecebimento() == null || empresaRecebimento == null || !empresaRecebimento.equals(inst.getEmpresaRecebimento()))
			return false;
		if(inst.getMaterial().getUnidademedida() == null || material.getUnidademedida() == null || !material.getUnidademedida().equals(inst.getMaterial().getUnidademedida()))
			return false;
		return true;
	}

	public void addNumerocopiasimpressaoetiqueta(int numerocopiasimpressaoetiqueta) {
		if(getNumerocopiasimpressaoetiqueta() == null)
			this.setNumerocopiasimpressaoetiqueta(numerocopiasimpressaoetiqueta);
		else
			this.setNumerocopiasimpressaoetiqueta(getNumerocopiasimpressaoetiqueta() + numerocopiasimpressaoetiqueta);
	}
	
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}

	public String getLote() {
		return Util.strings.emptyIfNull(lote);
	}
	public void setLote(String lote) {
		this.lote = lote;
	}
	
	public String getValidadeLote() {
		return Util.strings.emptyIfNull(validadeLote);
	}
	public void setValidadeLote(String validadeLote) {
		this.validadeLote = validadeLote;
	}

	@Required
	@DisplayName("N�mero de c�pias")
	public Integer getNumerocopiasimpressaoetiqueta() {
		return numerocopiasimpressaoetiqueta;
	}
	public void setNumerocopiasimpressaoetiqueta(
			Integer numerocopiasimpressaoetiqueta) {
		this.numerocopiasimpressaoetiqueta = numerocopiasimpressaoetiqueta;
	}

	public String getEmpresaRecebimento() {
		return Util.strings.emptyIfNull(empresaRecebimento);
	}
	public void setEmpresaRecebimento(String empresaRecebimento) {
		this.empresaRecebimento = empresaRecebimento;
	}
	public Integer getCdEntrega() {
		return cdEntrega;
	}
	public void setCdEntrega(Integer cdEntrega) {
		this.cdEntrega = cdEntrega;
	}
	public Integer getCdEntregaMaterial() {
		return cdEntregaMaterial;
	}
	public void setCdEntregaMaterial(Integer cdEntregaMaterial) {
		this.cdEntregaMaterial = cdEntregaMaterial;
	}
	public Integer getCdordemcompra() {
		return cdordemcompra;
	}
	public void setCdordemcompra(Integer cdordemcompra) {
		this.cdordemcompra = cdordemcompra;
	}
	public Integer getCdentregadocumento() {
		return cdentregadocumento;
	}
	public void setCdentregadocumento(Integer cdentregadocumento) {
		this.cdentregadocumento = cdentregadocumento;
	}
	public Integer getCdmovimentacaoestoque() {
		return cdmovimentacaoestoque;
	}
	public void setCdmovimentacaoestoque(Integer cdmovimentacaoestoque) {
		this.cdmovimentacaoestoque = cdmovimentacaoestoque;
	}

	public String getTipoetiqueta() {
		return tipoetiqueta;
	}

	public void setTipoetiqueta(String tipoetiqueta) {
		this.tipoetiqueta = tipoetiqueta;
	}

}