package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

public class EmitirComprovanteTEFItemBean {

	private Integer produto_codigo;
	private String produto_identificador;
	private String produto_nome;
	private Double quantidade;
	private String unidademedida_nome;
	private String unidademedida_simbolo;
	private Double valor_unitario;
	private Double valor_desconto;
	private Double valor_total;
	
	public Integer getProduto_codigo() {
		return produto_codigo;
	}
	public String getProduto_identificador() {
		return produto_identificador;
	}
	public String getProduto_nome() {
		return produto_nome;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public String getUnidademedida_nome() {
		return unidademedida_nome;
	}
	public String getUnidademedida_simbolo() {
		return unidademedida_simbolo;
	}
	public Double getValor_unitario() {
		return valor_unitario;
	}
	public Double getValor_desconto() {
		return valor_desconto;
	}
	public Double getValor_total() {
		return valor_total;
	}
	public void setProduto_codigo(Integer produto_codigo) {
		this.produto_codigo = produto_codigo;
	}
	public void setProduto_identificador(String produto_identificador) {
		this.produto_identificador = produto_identificador;
	}
	public void setProduto_nome(String produto_nome) {
		this.produto_nome = produto_nome;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setUnidademedida_nome(String unidademedida_nome) {
		this.unidademedida_nome = unidademedida_nome;
	}
	public void setUnidademedida_simbolo(String unidademedida_simbolo) {
		this.unidademedida_simbolo = unidademedida_simbolo;
	}
	public void setValor_unitario(Double valor_unitario) {
		this.valor_unitario = valor_unitario;
	}
	public void setValor_desconto(Double valor_desconto) {
		this.valor_desconto = valor_desconto;
	}
	public void setValor_total(Double valor_total) {
		this.valor_total = valor_total;
	}
	
}
