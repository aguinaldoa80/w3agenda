package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

//import br.com.linkcom.sined.geral.bean.enumeration.CampoComprovanteConfiguravel.TipoCampo;

public class ProdutoComprovanteConfiguravel {
	
	private String CDMATERIAL;
	private String CODIGO;
	private String IDENTIFICADOR;
	private String IDENTIFICADORESPECIFICO;
	private String IDENTIFICADORINTEGRACAO;
	private String IDENTIFICADORMATERIALMESTRE;
	private String DESCRICAO;
	private Integer CDMATERIALTIPO;
	private Integer CDMATERIALCATEGORIA;
	private String COLETACODIGO;
	private String COLETAIDENTIFICADOR;
	private String COLETADESCRICAO;
	private String BANDACODIGO;
	private String BANDAIDENTIFICADOR;
	private String BANDADESCRICAO;
	
	private String PNEUDESCRICAO;
	private String RODA;
	private String LONAS;
	
	private String OTRPNEUTIPODESENHO;
	private String OTRPNEUTIPOSERVICO;
	
	private String UNIDADEMEDIDA;
	private String UNIDADEMEDIDASIMBOLO;
	private String PRECO;
	private Double QUANTIDADE;
	private Double QUANTIDADEUNIDADEPRINCIPAL;
	private Double QUANTIDADEMETROCUBICO;
	private String VALORMETROCUBICO;
	private String ENTREGA;
	private String TOTAL;
	private String OBSERVACAO;
	private String LOTEESTOQUE;
	private String DESCONTO;
	private String CDMATERIALGRUPO;
	private String COMPRIMENTO;
	private String PESOLIQUIDO;
	private String PESOBRUTO;
	private String LARGURA;
	private String ALTURA;
	private String NCM;
	private String NCMCOMPLETO;
	private String NCMDESCRICAO;
	private String IMAGEM;
	private String CDMATERIALMESTRE;
	private String FORNECEDOR;
	private String FORNECEDORINSCRICAOESTADUAL;
	private String FORNECEDORRAZAOSOCIAL;
	private String FORNECEDORCPFCNPJ;
	private String FORNECEDORTELEFONE;
	private String FORNECEDORENDERECO;
	private String CARACTERISTICA;
	private String COLETACARACTERISTICA;
	private String LOCALIZACAOESTOQUE;
	private String CDPEDIDOVENDAMATERIAL;
	private String CDVENDAMATERIAL;
	private String MARCA;
	private String SERIE;
	private String DOT;
	private String MODELO;
	private String MEDIDA;
	private String NUMEROREFORMA;
	private String PNEUID;
	private Double VALORIPI;
	private Double IPI;
    private Double ICMS;
    private Double ICMSST;
    private Double FCP;
    private Double FCPST;
    private Double DIFAL;
    private Double VALORICMS;
    private Double VALORICMSST;
    private Double VALORFCP;
    private Double VALORFCPST;
    private Double VALORDIFAL;
	private String VENCIMENTOPRODUTOMESES;
	private String CDMATERIALMESTREGRADE;
	private Double OUTRASDESPESAS;
	private Double SEGURO;
	
	public String getCDMATERIALMESTREGRADE() {
		return CDMATERIALMESTREGRADE;
	}
	public void setCDMATERIALMESTREGRADE(String cDMATERIALMESTREGRADE) {
		CDMATERIALMESTREGRADE = cDMATERIALMESTREGRADE;
	}
	public String getCODIGO() {
		return CODIGO;
	}
	public String getDESCRICAO() {
		return DESCRICAO;
	}
	public Integer getCDMATERIALTIPO() {
		return CDMATERIALTIPO;
	}
	public Integer getCDMATERIALCATEGORIA() {
		return CDMATERIALCATEGORIA;
	}
	public String getUNIDADEMEDIDA() {
		return UNIDADEMEDIDA;
	}
	public String getUNIDADEMEDIDASIMBOLO() {
		return UNIDADEMEDIDASIMBOLO;
	}
	public String getPRECO() {
		return PRECO;
	}
	public Double getQUANTIDADE() {
		return QUANTIDADE;
	}
	public Double getQUANTIDADEUNIDADEPRINCIPAL() {
		return QUANTIDADEUNIDADEPRINCIPAL;
	}
	public String getENTREGA() {
		return ENTREGA;
	}
	public String getTOTAL() {
		return TOTAL;
	}
	public String getOBSERVACAO() {
		return OBSERVACAO;
	}
	public String getCDPEDIDOVENDAMATERIAL() {
		return CDPEDIDOVENDAMATERIAL;
	}
	public String getMARCA() {
		return MARCA;
	}
	public String getSERIE() {
		return SERIE;
	}
	public String getDOT() {
		return DOT;
	}
	public String getMODELO() {
		return MODELO;
	}
	public String getMEDIDA() {
		return MEDIDA;
	}
	public String getNUMEROREFORMA() {
		return NUMEROREFORMA;
	}
	public String getCDVENDAMATERIAL() {
		return CDVENDAMATERIAL;
	}
	public String getPNEUID() {
		return PNEUID;
	}
	public String getVENCIMENTOPRODUTOMESES() {
		return VENCIMENTOPRODUTOMESES;
	}
	public String getIDENTIFICADORINTEGRACAO() {
		return IDENTIFICADORINTEGRACAO;
	}
	public void setIDENTIFICADORINTEGRACAO(String iDENTIFICADORINTEGRACAO) {
		IDENTIFICADORINTEGRACAO = iDENTIFICADORINTEGRACAO;
	}
	public void setPNEUID(String pNEUID) {
		PNEUID = pNEUID;
	}
	public void setCDVENDAMATERIAL(String cDVENDAMATERIAL) {
		CDVENDAMATERIAL = cDVENDAMATERIAL;
	}
	public void setMODELO(String mODELO) {
		MODELO = mODELO;
	}
	public void setMEDIDA(String mEDIDA) {
		MEDIDA = mEDIDA;
	}
	public void setNUMEROREFORMA(String nUMEROREFORMA) {
		NUMEROREFORMA = nUMEROREFORMA;
	}
	public void setMARCA(String mARCA) {
		MARCA = mARCA;
	}
	public void setSERIE(String sERIE) {
		SERIE = sERIE;
	}
	public void setDOT(String dOT) {
		DOT = dOT;
	}
	public void setCDPEDIDOVENDAMATERIAL(String cDPEDIDOVENDAMATERIAL) {
		CDPEDIDOVENDAMATERIAL = cDPEDIDOVENDAMATERIAL;
	}
	public void setCODIGO(String codigo) {
		CODIGO = codigo;
	}
	public void setDESCRICAO(String descricao) {
		DESCRICAO = descricao;
	}
	public void setCDMATERIALTIPO(Integer cDMATERIALTIPO) {
		CDMATERIALTIPO = cDMATERIALTIPO;
	}
	public void setCDMATERIALCATEGORIA(Integer cDMATERIALCATEGORIA) {
		CDMATERIALCATEGORIA = cDMATERIALCATEGORIA;
	}
	public void setUNIDADEMEDIDA(String unidademedida) {
		UNIDADEMEDIDA = unidademedida;
	}
	public void setUNIDADEMEDIDASIMBOLO(String unidademedidasimbolo) {
		UNIDADEMEDIDASIMBOLO = unidademedidasimbolo;
	}
	public void setPRECO(String preco) {
		PRECO = preco;
	}
	public void setQUANTIDADE(Double quantidade) {
		QUANTIDADE = quantidade;
	}
	public void setQUANTIDADEUNIDADEPRINCIPAL(Double qUANTIDADEUNIDADEPRINCIPAL) {
		QUANTIDADEUNIDADEPRINCIPAL = qUANTIDADEUNIDADEPRINCIPAL;
	}
	public void setENTREGA(String entrega) {
		ENTREGA = entrega;
	}
	public void setTOTAL(String total) {
		TOTAL = total;
	}
	public void setOBSERVACAO(String oBSERVACAO) {
		OBSERVACAO = oBSERVACAO;
	}
	public String getLOTEESTOQUE() {
		return LOTEESTOQUE;
	}
	public void setLOTEESTOQUE(String lOTEESTOQUE) {
		LOTEESTOQUE = lOTEESTOQUE;
	}
	public String getDESCONTO() {
		return DESCONTO;
	}
	public void setDESCONTO(String dESCONTO) {
		DESCONTO = dESCONTO;
	}
	public String getCDMATERIALGRUPO() {
		return CDMATERIALGRUPO;
	}
	public void setCDMATERIALGRUPO(String cDMATERIALGRUPO) {
		CDMATERIALGRUPO = cDMATERIALGRUPO;
	}
	public String getCOMPRIMENTO() {
		return COMPRIMENTO;
	}
	public void setCOMPRIMENTO(String cOMPRIMENTO) {
		COMPRIMENTO = cOMPRIMENTO;
	}
	public String getPESOLIQUIDO() {
		return PESOLIQUIDO;
	}
	public String getPESOBRUTO() {
		return PESOBRUTO;
	}
	public void setPESOLIQUIDO(String pESOLIQUIDO) {
		PESOLIQUIDO = pESOLIQUIDO;
	}
	public void setPESOBRUTO(String pESOBRUTO) {
		PESOBRUTO = pESOBRUTO;
	}
	public String getLARGURA() {
		return LARGURA;
	}
	public String getALTURA() {
		return ALTURA;
	}
	public void setLARGURA(String lARGURA) {
		LARGURA = lARGURA;
	}
	public void setALTURA(String aLTURA) {
		ALTURA = aLTURA;
	}
	public Double getQUANTIDADEMETROCUBICO() {
		return QUANTIDADEMETROCUBICO;
	}
	public void setQUANTIDADEMETROCUBICO(Double qUANTIDADEMETROCUBICO) {
		QUANTIDADEMETROCUBICO = qUANTIDADEMETROCUBICO;
	}
	public String getVALORMETROCUBICO() {
		return VALORMETROCUBICO;
	}
	public void setVALORMETROCUBICO(String vALORMETROCUBICO) {
		VALORMETROCUBICO = vALORMETROCUBICO;
	}
	public String getNCM() {
		return NCM;
	}
	public String getNCMCOMPLETO() {
		return NCMCOMPLETO;
	}
	public String getNCMDESCRICAO() {
		return NCMDESCRICAO;
	}
	public void setNCM(String nCM) {
		NCM = nCM;
	}
	public void setNCMCOMPLETO(String nCMCOMPLETO) {
		NCMCOMPLETO = nCMCOMPLETO;
	}
	public void setNCMDESCRICAO(String nCMDESCRICAO) {
		NCMDESCRICAO = nCMDESCRICAO;
	}
	public String getIMAGEM() {
		return IMAGEM;
	}
	public void setIMAGEM(String iMAGEM) {
		IMAGEM = iMAGEM;
	}
	public String getCDMATERIALMESTRE() {
		return CDMATERIALMESTRE;
	}
	public void setCDMATERIALMESTRE(String cDMATERIALMESTRE) {
		CDMATERIALMESTRE = cDMATERIALMESTRE;
	}
	public String getFORNECEDOR() {
		return FORNECEDOR;
	}
	public void setFORNECEDOR(String fORNECEDOR) {
		FORNECEDOR = fORNECEDOR;
	}
	public String getCARACTERISTICA() {
		return CARACTERISTICA;
	}
	public void setCARACTERISTICA(String cARACTERISTICA) {
		CARACTERISTICA = cARACTERISTICA;
	}
	public String getFORNECEDORTELEFONE() {
		return FORNECEDORTELEFONE;
	}
	public void setFORNECEDORTELEFONE(String fORNECEDORTELEFONE) {
		FORNECEDORTELEFONE = fORNECEDORTELEFONE;
	}
	public String getFORNECEDORCPFCNPJ() {
		return FORNECEDORCPFCNPJ;
	}
	public void setFORNECEDORCPFCNPJ(String fORNECEDORCPFCNPJ) {
		FORNECEDORCPFCNPJ = fORNECEDORCPFCNPJ;
	}
	public String getFORNECEDORENDERECO() {
		return FORNECEDORENDERECO;
	}
	public void setFORNECEDORENDERECO(String fORNECEDORENDERECO) {
		FORNECEDORENDERECO = fORNECEDORENDERECO;
	}
	public String getFORNECEDORINSCRICAOESTADUAL() {
		return FORNECEDORINSCRICAOESTADUAL;
	}
	public String getFORNECEDORRAZAOSOCIAL() {
		return FORNECEDORRAZAOSOCIAL;
	}
	public void setFORNECEDORINSCRICAOESTADUAL(
			String fORNECEDORINSCRICAOESTADUAL) {
		FORNECEDORINSCRICAOESTADUAL = fORNECEDORINSCRICAOESTADUAL;
	}
	public void setFORNECEDORRAZAOSOCIAL(String fORNECEDORRAZAOSOCIAL) {
		FORNECEDORRAZAOSOCIAL = fORNECEDORRAZAOSOCIAL;
	}
	public String getIDENTIFICADOR() {
		return IDENTIFICADOR;
	}
	public void setIDENTIFICADOR(String iDENTIFICADOR) {
		IDENTIFICADOR = iDENTIFICADOR;
	}
	public String getIDENTIFICADORESPECIFICO() {
		return IDENTIFICADORESPECIFICO;
	}
	public void setIDENTIFICADORESPECIFICO(String iDENTIFICADORESPECIFICO) {
		IDENTIFICADORESPECIFICO = iDENTIFICADORESPECIFICO;
	}
	
	public String getIDENTIFICADORMATERIALMESTRE() {
		return IDENTIFICADORMATERIALMESTRE;
	}
	public void setIDENTIFICADORMATERIALMESTRE(String iDENTIFICADORMATERIALMESTRE) {
		IDENTIFICADORMATERIALMESTRE = iDENTIFICADORMATERIALMESTRE;
	}
	public String getLOCALIZACAOESTOQUE() {
		return LOCALIZACAOESTOQUE;
	}
	public void setLOCALIZACAOESTOQUE(String lOCALIZACAOESTOQUE) {
		LOCALIZACAOESTOQUE = lOCALIZACAOESTOQUE;
	}
	public String getCOLETACODIGO() {
		return COLETACODIGO;
	}
	public String getCOLETAIDENTIFICADOR() {
		return COLETAIDENTIFICADOR;
	}
	public String getCOLETADESCRICAO() {
		return COLETADESCRICAO;
	}
	public String getCOLETACARACTERISTICA() {
		return COLETACARACTERISTICA;
	}
	public void setCOLETACODIGO(String cOLETACODIGO) {
		COLETACODIGO = cOLETACODIGO;
	}
	public void setCOLETAIDENTIFICADOR(String cOLETAIDENTIFICADOR) {
		COLETAIDENTIFICADOR = cOLETAIDENTIFICADOR;
	}
	public void setCOLETADESCRICAO(String cOLETADESCRICAO) {
		COLETADESCRICAO = cOLETADESCRICAO;
	}
	public void setCOLETACARACTERISTICA(String cOLETACARACTERISTICA) {
		COLETACARACTERISTICA = cOLETACARACTERISTICA;
	}
	public String getBANDACODIGO() {
		return BANDACODIGO;
	}
	public String getBANDAIDENTIFICADOR() {
		return BANDAIDENTIFICADOR;
	}
	public String getBANDADESCRICAO() {
		return BANDADESCRICAO;
	}
	public String getPNEUDESCRICAO() {
		return PNEUDESCRICAO;
	}
	public String getRODA() {
		return RODA;
	}
	public String getLONAS() {
		return LONAS;
	}
	public String getOTRPNEUTIPODESENHO() {
		return OTRPNEUTIPODESENHO;
	}
	public String getOTRPNEUTIPOSERVICO() {
		return OTRPNEUTIPOSERVICO;
	}
	public void setBANDACODIGO(String bANDACODIGO) {
		BANDACODIGO = bANDACODIGO;
	}
	public void setBANDAIDENTIFICADOR(String bANDAIDENTIFICADOR) {
		BANDAIDENTIFICADOR = bANDAIDENTIFICADOR;
	}
	public void setBANDADESCRICAO(String bANDADESCRICAO) {
		BANDADESCRICAO = bANDADESCRICAO;
	}
	public void setPNEUDESCRICAO(String pNEUDESCRICAO) {
		PNEUDESCRICAO = pNEUDESCRICAO;
	}
	public void setRODA(String rODA) {
		RODA = rODA;
	}
	public void setLONAS(String lONAS) {
		LONAS = lONAS;
	}
	public void setOTRPNEUTIPODESENHO(String oTRPNEUTIPODESENHO) {
		OTRPNEUTIPODESENHO = oTRPNEUTIPODESENHO;
	}
	public void setOTRPNEUTIPOSERVICO(String oTRPNEUTIPOSERVICO) {
		OTRPNEUTIPOSERVICO = oTRPNEUTIPOSERVICO;
	}
	public String getCDMATERIAL() {
		return CDMATERIAL;
	}
	public void setCDMATERIAL(String cDMATERIAL) {
		CDMATERIAL = cDMATERIAL;
	}
	public Double getVALORIPI() {
		return VALORIPI;
	}
	public Double getIPI() {
		return IPI;
	}
    public Double getICMS() {
        return ICMS;
    }
    public Double getICMSST() {
        return ICMSST;
    }
    public Double getFCP() {
        return FCP;
    }
    public Double getFCPST() {
        return FCPST;
    }
    public Double getDIFAL() {
        return DIFAL;
    }
    public Double getVALORICMS() {
        return VALORICMS;
    }
    public Double getVALORICMSST() {
        return VALORICMSST;
    }
    public Double getVALORFCP() {
        return VALORFCP;
    }
    public Double getVALORFCPST() {
        return VALORFCPST;
    }
    public Double getVALORDIFAL() {
        return VALORDIFAL;
    }
    public void setICMS(Double iCMS) {
        ICMS = iCMS;
    }
    public void setICMSST(Double iCMSST) {
        ICMSST = iCMSST;
    }
    public void setFCP(Double fCP) {
        FCP = fCP;
    }
    public void setFCPST(Double fCPST) {
        FCPST = fCPST;
    }
    public void setDIFAL(Double dIFAL) {
        DIFAL = dIFAL;
    }
    public void setVALORDIFAL(Double vALORDIFAL) {
        VALORDIFAL = vALORDIFAL;
    }
    public void setVALORFCP(Double vALORFCP) {
        VALORFCP = vALORFCP;
    }
    public void setVALORFCPST(Double vALORFCPST) {
        VALORFCPST = vALORFCPST;
    }
    public void setVALORICMS(Double vALORICMS) {
        VALORICMS = vALORICMS;
    }
    public void setVALORICMSST(Double vALORICMSST) {
        VALORICMSST = vALORICMSST;
    }
	public void setVALORIPI(Double vALORIPI) {
		VALORIPI = vALORIPI;
	}
	public void setIPI(Double iPI) {
		IPI = iPI;
	}
	public void setVENCIMENTOPRODUTOMESES(String vENCIMENTOPRODUTOMESES) {
		VENCIMENTOPRODUTOMESES = vENCIMENTOPRODUTOMESES;
	}
	public Double getOUTRASDESPESAS() {
		return OUTRASDESPESAS;
	}
	public void setOUTRASDESPESAS(Double oUTRASDESPESAS) {
		OUTRASDESPESAS = oUTRASDESPESAS;
	}
	public Double getSEGURO() {
		return SEGURO;
	}
	public void setSEGURO(Double sEGURO) {
		SEGURO = sEGURO;
	}
}