package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.LinkedList;
import java.util.List;

import br.com.linkcom.sined.modulo.producao.controller.report.bean.PneuReportBean;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.ServicoReportBean;


public class EmitirColetaMaterialBean {
	
	protected String material_codigo;
	protected String material_nome;
	protected Double quantidade;
	protected Double quantidadedevolvida;
	protected Double valorunitario;
	protected String observacao;
	protected String localentrada_nome;
	protected String motivodevolucao_descricao;	
	protected Integer cdpedidovendamaterial;
	protected PneuReportBean pneuReportBean;
	protected ServicoReportBean servicoReportBean;
	
	protected LinkedList<ServicoReportBean> listaServicoReportBean;
	protected LinkedList<Integer> listaCdpedidovendamaterial;
	
	public String getMaterial_codigo() {
		return material_codigo;
	}
	public String getMaterial_nome() {
		return material_nome;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Double getQuantidadedevolvida() {
		return quantidadedevolvida;
	}
	public Double getValorunitario() {
		return valorunitario;
	}
	public String getObservacao() {
		return observacao;
	}
	public String getLocalentrada_nome() {
		return localentrada_nome;
	}
	public String getMotivodevolucao_descricao() {
		return motivodevolucao_descricao;
	}
	public Integer getCdpedidovendamaterial() {
		return cdpedidovendamaterial;
	}
	public PneuReportBean getPneuReportBean() {
		return pneuReportBean;
	}
	public ServicoReportBean getServicoReportBean() {
		return servicoReportBean;
	}
	
	public void setMaterial_codigo(String material_codigo) {
		this.material_codigo = material_codigo;
	}
	public void setMaterial_nome(String material_nome) {
		this.material_nome = material_nome;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setQuantidadedevolvida(Double quantidadedevolvida) {
		this.quantidadedevolvida = quantidadedevolvida;
	}
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setLocalentrada_nome(String localentrada_nome) {
		this.localentrada_nome = localentrada_nome;
	}
	public void setMotivodevolucao_descricao(String motivodevolucao_descricao) {
		this.motivodevolucao_descricao = motivodevolucao_descricao;
	}
	public void setCdpedidovendamaterial(Integer cdpedidovendamaterial) {
		this.cdpedidovendamaterial = cdpedidovendamaterial;
	}
	public void setPneuReportBean(PneuReportBean pneuReportBean) {
		this.pneuReportBean = pneuReportBean;
	}
	public void setServicoReportBean(ServicoReportBean servicoReportBean) {
		this.servicoReportBean = servicoReportBean;
	}
	public LinkedList<ServicoReportBean> getListaServicoReportBean() {
		return listaServicoReportBean;
	}
	public void setListaServicoReportBean(LinkedList<ServicoReportBean> listaServicoReportBean) {
		this.listaServicoReportBean = listaServicoReportBean;
	}
	public LinkedList<Integer> getListaCdpedidovendamaterial() {
		return listaCdpedidovendamaterial;
	}
	public void setListaCdpedidovendamaterial(LinkedList<Integer> listaCdpedidovendamaterial) {
		this.listaCdpedidovendamaterial = listaCdpedidovendamaterial;
	}
}
