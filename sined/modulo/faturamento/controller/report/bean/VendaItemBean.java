package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;


public class VendaItemBean {
	
	private Double qtdetotalproduto;	
	private Double qtdetotalservico;	
	private String localizacaoestoque;
	private Integer cdpedidovendamaterial;
	private Integer cdvendamaterial;
	private Double quantidadeunidadeprincipal;
	private Double quantidade;
	private Double quantidademetrocubico;
	private String valormetrocubico;
	private Double preco;
	private String entrega;
	private Double total;
	private String observacao;
	private String loteestoque;
	private Double desconto;
	private Double comprimento;
	private Double pesoliquido;
	private Double pesobruto;
	private Double altura;
	private Double largura;
	private Double valoripi;
	private Double ipi;
	private String vencimentoprodutomeses;
	private Double valortotal;
	private Double valorseguro;
	private Double valoroutrasdespesas;
	private Integer cdmaterialgrupo;
	private Integer identificadorintegracao;
	private String identificadorespecifico;
	
	private Integer material_cdmaterial;
	private String material_codigo;
	private String material_identificador;
	private String material_nome;
	private String material_nomenf;
	private String material_caracteristica;
	private String material_ncm;
	private String material_ncmcompleto;
	private String material_ncmdescricao;
	
	private String banda_codigo;
	private String banda_identificador;
	private String banda_descricao;
	
	private Integer coleta_codigo;
	private String coleta_identificador;
	private String coleta_descricao;
	private String coleta_caracteristica;
	
	private String unidademedida_nome;
	private String unidademedida_simbolo;
	
	private String fornecedor_nome;
	private String fornecedor_razaosocial;
	private String fornecedor_cpfcnpj;
	private String fornecedor_inscricaoestadual;
	private String fornecedor_telefone;
	private String fornecedor_endereco;
	
	private String pneu_marca;
	private String pneu_serie;
	private String pneu_dot;
	private String pneu_modelo;
	private String pneu_medida;
	private String pneu_numeroreforma;
	private Integer pneu_pneuid;
	private String pneu_roda;
	private String pneu_lonas;
	private String pneu_descricao;
	private String otrPneuDesenho;
	private String otrPneuTipoServico;
	
	public Double getQtdetotalproduto() {
		return qtdetotalproduto;
	}
	public Double getQtdetotalservico() {
		return qtdetotalservico;
	}
	public String getLocalizacaoestoque() {
		return localizacaoestoque;
	}
	public Integer getCdpedidovendamaterial() {
		return cdpedidovendamaterial;
	}
	public Integer getCdvendamaterial() {
		return cdvendamaterial;
	}
	public Double getQuantidadeunidadeprincipal() {
		return quantidadeunidadeprincipal;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Double getQuantidademetrocubico() {
		return quantidademetrocubico;
	}
	public String getValormetrocubico() {
		return valormetrocubico;
	}
	public Double getPreco() {
		return preco;
	}
	public String getEntrega() {
		return entrega;
	}
	public Double getTotal() {
		return total;
	}
	public String getObservacao() {
		return observacao;
	}
	public String getLoteestoque() {
		return loteestoque;
	}
	public Double getDesconto() {
		return desconto;
	}
	public Double getComprimento() {
		return comprimento;
	}
	public Double getPesoliquido() {
		return pesoliquido;
	}
	public Double getPesobruto() {
		return pesobruto;
	}
	public Double getAltura() {
		return altura;
	}
	public Double getLargura() {
		return largura;
	}
	public Double getValoripi() {
		return valoripi;
	}
	public Double getIpi() {
		return ipi;
	}
	public String getVencimentoprodutomeses() {
		return vencimentoprodutomeses;
	}
	public Double getValortotal() {
		return valortotal;
	}
	public Double getValorseguro() {
		return valorseguro;
	}
	public Double getValoroutrasdespesas() {
		return valoroutrasdespesas;
	}
	public Integer getCdmaterialgrupo() {
		return cdmaterialgrupo;
	}
	public Integer getIdentificadorintegracao() {
		return identificadorintegracao;
	}
	public String getIdentificadorespecifico() {
		return identificadorespecifico;
	}
	public Integer getMaterial_cdmaterial() {
		return material_cdmaterial;
	}
	public String getMaterial_codigo() {
		return material_codigo;
	}
	public String getMaterial_identificador() {
		return material_identificador;
	}
	public String getMaterial_nome() {
		return material_nome;
	}
	public String getMaterial_nomenf() {
		return material_nomenf;
	}
	public String getMaterial_caracteristica() {
		return material_caracteristica;
	}
	public String getMaterial_ncm() {
		return material_ncm;
	}
	public String getMaterial_ncmcompleto() {
		return material_ncmcompleto;
	}
	public String getMaterial_ncmdescricao() {
		return material_ncmdescricao;
	}
	public String getBanda_codigo() {
		return banda_codigo;
	}
	public String getBanda_identificador() {
		return banda_identificador;
	}
	public String getBanda_descricao() {
		return banda_descricao;
	}
	public Integer getColeta_codigo() {
		return coleta_codigo;
	}
	public String getColeta_identificador() {
		return coleta_identificador;
	}
	public String getColeta_descricao() {
		return coleta_descricao;
	}
	public String getColeta_caracteristica() {
		return coleta_caracteristica;
	}
	public String getUnidademedida_nome() {
		return unidademedida_nome;
	}
	public String getUnidademedida_simbolo() {
		return unidademedida_simbolo;
	}
	public String getFornecedor_nome() {
		return fornecedor_nome;
	}
	public String getFornecedor_razaosocial() {
		return fornecedor_razaosocial;
	}
	public String getFornecedor_cpfcnpj() {
		return fornecedor_cpfcnpj;
	}
	public String getFornecedor_inscricaoestadual() {
		return fornecedor_inscricaoestadual;
	}
	public String getFornecedor_telefone() {
		return fornecedor_telefone;
	}
	public String getFornecedor_endereco() {
		return fornecedor_endereco;
	}
	public String getPneu_marca() {
		return pneu_marca;
	}
	public String getPneu_serie() {
		return pneu_serie;
	}
	public String getPneu_dot() {
		return pneu_dot;
	}
	public String getPneu_modelo() {
		return pneu_modelo;
	}
	public String getPneu_medida() {
		return pneu_medida;
	}
	public String getPneu_numeroreforma() {
		return pneu_numeroreforma;
	}
	public Integer getPneu_pneuid() {
		return pneu_pneuid;
	}
	public String getPneu_roda() {
		return pneu_roda;
	}
	public String getPneu_lonas() {
		return pneu_lonas;
	}
	public String getPneu_descricao() {
		return pneu_descricao;
	}
	public String getOtrPneuDesenho() {
		return otrPneuDesenho;
	}
	public String getOtrPneuTipoServico() {
		return otrPneuTipoServico;
	}
	public void setQtdetotalproduto(Double qtdetotalproduto) {
		this.qtdetotalproduto = qtdetotalproduto;
	}
	public void setQtdetotalservico(Double qtdetotalservico) {
		this.qtdetotalservico = qtdetotalservico;
	}
	public void setLocalizacaoestoque(String localizacaoestoque) {
		this.localizacaoestoque = localizacaoestoque;
	}
	public void setCdpedidovendamaterial(Integer cdpedidovendamaterial) {
		this.cdpedidovendamaterial = cdpedidovendamaterial;
	}
	public void setCdvendamaterial(Integer cdvendamaterial) {
		this.cdvendamaterial = cdvendamaterial;
	}
	public void setQuantidadeunidadeprincipal(Double quantidadeunidadeprincipal) {
		this.quantidadeunidadeprincipal = quantidadeunidadeprincipal;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setQuantidademetrocubico(Double quantidademetrocubico) {
		this.quantidademetrocubico = quantidademetrocubico;
	}
	public void setValormetrocubico(String valormetrocubico) {
		this.valormetrocubico = valormetrocubico;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public void setEntrega(String entrega) {
		this.entrega = entrega;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setLoteestoque(String loteestoque) {
		this.loteestoque = loteestoque;
	}
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}
	public void setPesoliquido(Double pesoliquido) {
		this.pesoliquido = pesoliquido;
	}
	public void setPesobruto(Double pesobruto) {
		this.pesobruto = pesobruto;
	}
	public void setAltura(Double altura) {
		this.altura = altura;
	}
	public void setLargura(Double largura) {
		this.largura = largura;
	}
	public void setValoripi(Double valoripi) {
		this.valoripi = valoripi;
	}
	public void setIpi(Double ipi) {
		this.ipi = ipi;
	}
	public void setVencimentoprodutomeses(String vencimentoprodutomeses) {
		this.vencimentoprodutomeses = vencimentoprodutomeses;
	}
	public void setValortotal(Double valortotal) {
		this.valortotal = valortotal;
	}
	public void setValorseguro(Double valorseguro) {
		this.valorseguro = valorseguro;
	}
	public void setValoroutrasdespesas(Double valoroutrasdespesas) {
		this.valoroutrasdespesas = valoroutrasdespesas;
	}
	public void setCdmaterialgrupo(Integer cdmaterialgrupo) {
		this.cdmaterialgrupo = cdmaterialgrupo;
	}
	public void setIdentificadorintegracao(Integer identificadorintegracao) {
		this.identificadorintegracao = identificadorintegracao;
	}
	public void setIdentificadorespecifico(String identificadorespecifico) {
		this.identificadorespecifico = identificadorespecifico;
	}
	public void setMaterial_cdmaterial(Integer materialCdmaterial) {
		material_cdmaterial = materialCdmaterial;
	}
	public void setMaterial_codigo(String materialCodigo) {
		material_codigo = materialCodigo;
	}
	public void setMaterial_identificador(String materialIdentificador) {
		material_identificador = materialIdentificador;
	}
	public void setMaterial_nome(String materialNome) {
		material_nome = materialNome;
	}
	public void setMaterial_nomenf(String material_nomenf) {
		this.material_nomenf = material_nomenf;
	}
	public void setMaterial_caracteristica(String materialCaracteristica) {
		material_caracteristica = materialCaracteristica;
	}
	public void setMaterial_ncm(String materialNcm) {
		material_ncm = materialNcm;
	}
	public void setMaterial_ncmcompleto(String materialNcmcompleto) {
		material_ncmcompleto = materialNcmcompleto;
	}
	public void setMaterial_ncmdescricao(String materialNcmdescricao) {
		material_ncmdescricao = materialNcmdescricao;
	}
	public void setBanda_codigo(String bandaCodigo) {
		banda_codigo = bandaCodigo;
	}
	public void setBanda_identificador(String bandaIdentificador) {
		banda_identificador = bandaIdentificador;
	}
	public void setBanda_descricao(String bandaDescricao) {
		banda_descricao = bandaDescricao;
	}
	public void setColeta_codigo(Integer coletaCodigo) {
		coleta_codigo = coletaCodigo;
	}
	public void setColeta_identificador(String coletaIdentificador) {
		coleta_identificador = coletaIdentificador;
	}
	public void setColeta_descricao(String coletaDescricao) {
		coleta_descricao = coletaDescricao;
	}
	public void setColeta_caracteristica(String coletaCaracteristica) {
		coleta_caracteristica = coletaCaracteristica;
	}
	public void setUnidademedida_nome(String unidademedidaNome) {
		unidademedida_nome = unidademedidaNome;
	}
	public void setUnidademedida_simbolo(String unidademedidaSimbolo) {
		unidademedida_simbolo = unidademedidaSimbolo;
	}
	public void setFornecedor_nome(String fornecedorNome) {
		fornecedor_nome = fornecedorNome;
	}
	public void setFornecedor_razaosocial(String fornecedorRazaosocial) {
		fornecedor_razaosocial = fornecedorRazaosocial;
	}
	public void setFornecedor_cpfcnpj(String fornecedorCpfcnpj) {
		fornecedor_cpfcnpj = fornecedorCpfcnpj;
	}
	public void setFornecedor_inscricaoestadual(String fornecedorInscricaoestadual) {
		fornecedor_inscricaoestadual = fornecedorInscricaoestadual;
	}
	public void setFornecedor_telefone(String fornecedorTelefone) {
		fornecedor_telefone = fornecedorTelefone;
	}
	public void setFornecedor_endereco(String fornecedorEndereco) {
		fornecedor_endereco = fornecedorEndereco;
	}
	public void setPneu_marca(String pneuMarca) {
		pneu_marca = pneuMarca;
	}
	public void setPneu_serie(String pneuSerie) {
		pneu_serie = pneuSerie;
	}
	public void setPneu_dot(String pneuDot) {
		pneu_dot = pneuDot;
	}
	public void setPneu_modelo(String pneuModelo) {
		pneu_modelo = pneuModelo;
	}
	public void setPneu_medida(String pneuMedida) {
		pneu_medida = pneuMedida;
	}
	public void setPneu_numeroreforma(String pneuNumeroreforma) {
		pneu_numeroreforma = pneuNumeroreforma;
	}
	public void setPneu_pneuid(Integer pneuPneuid) {
		pneu_pneuid = pneuPneuid;
	}
	public void setPneu_roda(String pneu_roda) {
		this.pneu_roda = pneu_roda;
	}
	public void setPneu_lonas(String pneu_lonas) {
		this.pneu_lonas = pneu_lonas;
	}
	public void setPneu_descricao(String pneu_descricao) {
		this.pneu_descricao = pneu_descricao;
	}
	public void setOtrPneuDesenho(String otrPneuDesenho) {
		this.otrPneuDesenho = otrPneuDesenho;
	}
	public void setOtrPneuTipoServico(String otrPneuTipoServico) {
		this.otrPneuTipoServico = otrPneuTipoServico;
	}	
	
	public void preencherCamposNulos(){
		if(qtdetotalproduto == null) qtdetotalproduto = 0d;	
		if(qtdetotalservico == null) qtdetotalservico = 0d;	
		if(localizacaoestoque == null) localizacaoestoque = "";
		if(quantidadeunidadeprincipal == null) quantidadeunidadeprincipal = 0d;
		if(quantidade == null) quantidade = 0d;
		if(quantidademetrocubico == null) quantidademetrocubico = 0d;
		if(valormetrocubico == null) valormetrocubico = "";
		if(preco == null) preco = 0d;
		if(entrega == null) entrega = "";
		if(total == null) total = 0d;
		if(observacao == null) observacao = "";
		if(loteestoque == null) loteestoque = "";
		if(desconto == null) desconto = 0d;
		if(comprimento == null) comprimento = 0d;
		if(pesoliquido == null) pesoliquido = 0d;
		if(pesobruto == null) pesobruto = 0d;
		if(altura == null) altura = 0d;
		if(largura == null) largura = 0d;
		if(valoripi == null) valoripi = 0d;
		if(ipi == null) ipi = 0d;
		if(vencimentoprodutomeses == null) vencimentoprodutomeses = "";
		if(valortotal == null) valortotal = 0d;	
		if(identificadorespecifico == null) identificadorespecifico = "";
		
		if(material_codigo == null) material_codigo = "";
		if(material_identificador == null) material_identificador = "";
		if(material_nome == null) material_nome = "";
		if(material_caracteristica == null) material_caracteristica = "";
		if(material_ncm == null) material_ncm = "";
		if(material_ncmcompleto == null) material_ncmcompleto = "";
		if(material_ncmdescricao == null) material_ncmdescricao = "";
		
		if(banda_codigo == null) banda_codigo = "";
		if(banda_identificador == null) banda_identificador = "";
		if(banda_descricao == null) banda_descricao = "";
		
		if(coleta_identificador == null) coleta_identificador = "";
		if(coleta_descricao == null) coleta_descricao = "";
		if(coleta_caracteristica == null) coleta_caracteristica = "";
		
		if(unidademedida_nome == null) unidademedida_nome = "";
		if(unidademedida_simbolo == null) unidademedida_simbolo = "";
		
		if(fornecedor_nome == null) fornecedor_nome = "";
		if(fornecedor_razaosocial == null) fornecedor_razaosocial = "";
		if(fornecedor_cpfcnpj == null) fornecedor_cpfcnpj = "";
		if(fornecedor_inscricaoestadual == null) fornecedor_inscricaoestadual = "";
		if(fornecedor_telefone == null) fornecedor_telefone = "";
		if(fornecedor_endereco == null) fornecedor_endereco = "";
		
		if(pneu_marca == null) pneu_marca = "";
		if(pneu_serie == null) pneu_serie = "";
		if(pneu_dot == null) pneu_dot = "";
		if(pneu_modelo == null) pneu_modelo = "";
		if(pneu_medida == null) pneu_medida = "";
		if(pneu_numeroreforma == null) pneu_numeroreforma = "";
		if(otrPneuTipoServico == null) otrPneuTipoServico = "";
		if(otrPneuDesenho == null) otrPneuDesenho = "";
		if(pneu_roda == null) pneu_roda = "";
		if(pneu_lonas == null) pneu_lonas = "";
		if(pneu_descricao == null) pneu_descricao = "";
	}
}
	
