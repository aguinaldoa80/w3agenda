package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Valecompra;
import br.com.linkcom.sined.geral.bean.Valecompraorigem;

import com.ibm.icu.text.SimpleDateFormat;

public class ValeCompraBean {

	private Integer cdvalecompra;
	private String data;
	private String identificacao;
	private String tipo;
	private Money valor;
	private String origem;
	
	public ValeCompraBean(Valecompra valeCompra){
		if(valeCompra!=null){
			this.setCdvalecompra(valeCompra.getCdvalecompra());
			this.setData(new SimpleDateFormat("dd/MM/yyyy").format(valeCompra.getData()));
			this.setIdentificacao(valeCompra.getIdentificacao());
			this.setTipo(valeCompra.getTipooperacao()!=null?valeCompra.getTipooperacao().getNome(): "");
			this.setValor(valeCompra.getValor());
			StringBuilder dadosOrigem = new StringBuilder();
			for(Valecompraorigem orig: valeCompra.getListaValecompraorigem()){
				if(orig.getDocumento()!=null && orig.getDocumento().getCddocumento()!=null){
					dadosOrigem.append("Documento: "+orig.getDocumento().getCddocumento().toString()+"; ");
				}else if(orig.getPedidovenda()!=null && orig.getPedidovenda().getCdpedidovenda()!=null){
					dadosOrigem.append("Pedido de venda: "+orig.getPedidovenda().getCdpedidovenda().toString()+"; ");
				}else if(orig.getVenda()!=null && orig.getVenda().getCdvenda()!=null){
					dadosOrigem.append("Venda: "+orig.getVenda().getCdvenda().toString()+"; ");
				}else if(orig.getColeta()!=null && orig.getColeta().getCdcoleta()!=null){
					dadosOrigem.append("Coleta: "+orig.getColeta().getCdcoleta().toString()+"; ");
				}
			}
			this.setOrigem(dadosOrigem.toString());
		}
	}
	
	public Integer getCdvalecompra() {
		return cdvalecompra==null? 0: cdvalecompra;
	}
	public void setCdvalecompra(Integer cdvalecompra) {
		this.cdvalecompra = cdvalecompra;
	}
	public String getData() {
		return Util.strings.emptyIfNull(data);
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getIdentificacao() {
		return Util.strings.emptyIfNull(identificacao);
	}
	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}
	public String getTipo() {
		return Util.strings.emptyIfNull(tipo);
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Money getValor() {
		return valor==null? new Money(): valor;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public String getOrigem() {
		return Util.strings.emptyIfNull(origem);
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}

}
