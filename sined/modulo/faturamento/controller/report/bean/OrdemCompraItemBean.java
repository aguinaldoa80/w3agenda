package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

public class OrdemCompraItemBean {

	private Double qtde_pedida;
	private OrdemcompraMaterialBean material;
	private Double valor;
	private Double icms_iss;
	private Double ipi;
	private Boolean icms_iss_incluso;
	private Boolean ipi_incluso;
	private String frequencia;
	private Double qtde_frequencia;
	private String conta_gerencial;
	private String observacao;
	private String unidade_medida;

	public Double getQtde_pedida() {
		return qtde_pedida;
	}

	public void setQtde_pedida(Double qtdpedida) {
		this.qtde_pedida = qtdpedida;
	}

	public OrdemcompraMaterialBean getMaterial() {
		return material;
	}

	public void setMaterial(OrdemcompraMaterialBean material) {
		this.material = material;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Double getIcms_iss() {
		return icms_iss;
	}

	public void setIcms_iss(Double icmsiss) {
		this.icms_iss = icmsiss;
	}

	public Double getIpi() {
		return ipi;
	}

	public void setIpi(Double ipi) {
		this.ipi = ipi;
	}

	public Boolean getIcms_iss_incluso() {
		return icms_iss_incluso;
	}

	public void setIcms_iss_incluso(Boolean icmsissincluso) {
		this.icms_iss_incluso = icmsissincluso;
	}

	public Boolean getIpi_incluso() {
		return ipi_incluso;
	}

	public void setIpi_incluso(Boolean ipiincluso) {
		this.ipi_incluso = ipiincluso;
	}

	public String getFrequencia() {
		return frequencia;
	}

	public void setFrequencia(String frequenciaStr) {
		this.frequencia = frequenciaStr;
	}

	public Double getQtde_frequencia() {
		return qtde_frequencia;
	}

	public void setQtde_frequencia(Double qtdefrequencia) {
		this.qtde_frequencia = qtdefrequencia;
	}

	public String getConta_gerencial() {
		return conta_gerencial;
	}

	public void setConta_gerencial(String conta_gerencial) {
		this.conta_gerencial = conta_gerencial;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getUnidade_medida() {
		return unidade_medida;
	}

	public void setUnidade_medida(String unidade_medida) {
		this.unidade_medida = unidade_medida;
	}

}
