package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Material;

public class ProdutokitComprovanteConfiguravel {
	
	
	private String CODIGO;
	private String DESCRICAO;
	private Double QUANTIDADE;
	private Double PRECO;
	private Double TOTAL;
	
	private List<ProdutokititemComprovanteConfiguravel> LISTAPRODUTOKITITEM = new ArrayList<ProdutokititemComprovanteConfiguravel>();

	private Material material;
	
	public String getCODIGO() {
		return CODIGO;
	}
	public String getDESCRICAO() {
		return DESCRICAO;
	}
	public Double getQUANTIDADE() {
		return QUANTIDADE;
	}
	public Double getPRECO() {
		return PRECO;
	}
	public Double getTOTAL() {
		return TOTAL;
	}
	public List<ProdutokititemComprovanteConfiguravel> getLISTAPRODUTOKITITEM() {
		return LISTAPRODUTOKITITEM;
	}
	
	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}
	public void setDESCRICAO(String dESCRICAO) {
		DESCRICAO = dESCRICAO;
	}
	public void setQUANTIDADE(Double qUANTIDADE) {
		QUANTIDADE = qUANTIDADE;
	}
	public void setPRECO(Double pRECO) {
		PRECO = pRECO;
	}
	public void setTOTAL(Double tOTAL) {
		TOTAL = tOTAL;
	}
	public void setLISTAPRODUTOKITITEM(List<ProdutokititemComprovanteConfiguravel> lISTAPRODUTOKITITEM) {
		LISTAPRODUTOKITITEM = lISTAPRODUTOKITITEM;
	}

	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
}