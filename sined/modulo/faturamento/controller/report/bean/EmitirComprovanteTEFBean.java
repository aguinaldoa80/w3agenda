package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class EmitirComprovanteTEFBean {

	private String empresa_nome = "";
	private String empresa_cnpj = "";
	private String empresa_endereco = "";
	private String empresa_telefone = "";
	
	private String cliente_nome = "";
	private String cliente_cpfcnpj = "";
	private String cliente_endereco = "";
	private String cliente_telefone = "";
	private String cliente_ie = "";
	
	private String data = "";
	private Integer codigo;
	private String condicao_pagamento = "";
	private Double pesobruto_total;
	private Double pesoliquido_total;
	private Integer quantidade_itens;
	private Double valor_produtos;
	private Double valor_desconto;
	private Double valor_total;
	private Double valor_pagamento;
	private String observacao = "";
	
	private String nota_chaveacesso = "";
	private String nota_numero = "";
	private Integer nota_serie;
	private String nota_protocoloautorizacao = "";
	private String nota_dataautorizacao = "";
	private String nota_dataemissao = "";
	private String nota_horaemissao = "";
	private String nota_qrcode = "";
	private Double nota_valor_total_imposto_federal = 0d;
	private Double nota_valor_total_imposto_estadual = 0d;
	private Double nota_valor_total_imposto_municipal = 0d;
	
	private List<EmitirComprovanteTEFItemBean> listaItens = new LinkedList<EmitirComprovanteTEFItemBean>();
	private List<EmitirComprovanteTEFPagamentoBean> listaPagamentos = new LinkedList<EmitirComprovanteTEFPagamentoBean>();
	private List<EmitirComprovanteTEFAdquirenteBean> listaAdquirente = new LinkedList<EmitirComprovanteTEFAdquirenteBean>();
	
	public void addPagamento(EmitirComprovanteTEFPagamentoBean it){
		if(listaPagamentos == null) listaPagamentos = new ArrayList<EmitirComprovanteTEFPagamentoBean>();
		listaPagamentos.add(it);
	}
	
	public void addItem(EmitirComprovanteTEFItemBean it){
		if(listaItens == null) listaItens = new ArrayList<EmitirComprovanteTEFItemBean>();
		listaItens.add(it);
	}
	
	public void addAdquirente(EmitirComprovanteTEFAdquirenteBean ad){
		if(listaAdquirente == null) listaAdquirente = new ArrayList<EmitirComprovanteTEFAdquirenteBean>();
		listaAdquirente.add(ad);
	}
	
	public String getEmpresa_nome() {
		return empresa_nome;
	}
	public String getEmpresa_cnpj() {
		return empresa_cnpj;
	}
	public String getEmpresa_endereco() {
		return empresa_endereco;
	}
	public String getCliente_nome() {
		return cliente_nome;
	}
	public String getCliente_cpfcnpj() {
		return cliente_cpfcnpj;
	}
	public String getCliente_endereco() {
		return cliente_endereco;
	}
	public String getCliente_telefone() {
		return cliente_telefone;
	}
	public String getCliente_ie() {
		return cliente_ie;
	}
	public String getData() {
		return data;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public String getCondicao_pagamento() {
		return condicao_pagamento;
	}
	public Double getPesobruto_total() {
		return pesobruto_total;
	}
	public Double getPesoliquido_total() {
		return pesoliquido_total;
	}
	public Integer getQuantidade_itens() {
		return quantidade_itens;
	}
	public Double getValor_produtos() {
		return valor_produtos;
	}
	public Double getValor_desconto() {
		return valor_desconto;
	}
	public Double getValor_total() {
		return valor_total;
	}
	public Double getValor_pagamento() {
		return valor_pagamento;
	}
	public String getObservacao() {
		return observacao;
	}
	public String getNota_chaveacesso() {
		return nota_chaveacesso;
	}
	public String getNota_numero() {
		return nota_numero;
	}
	public Integer getNota_serie() {
		return nota_serie;
	}
	public String getNota_protocoloautorizacao() {
		return nota_protocoloautorizacao;
	}
	public String getNota_dataautorizacao() {
		return nota_dataautorizacao;
	}
	public String getNota_qrcode() {
		return nota_qrcode;
	}
	public List<EmitirComprovanteTEFItemBean> getListaItens() {
		return listaItens;
	}
	public List<EmitirComprovanteTEFPagamentoBean> getListaPagamentos() {
		return listaPagamentos;
	}
	public List<EmitirComprovanteTEFAdquirenteBean> getListaAdquirente() {
		return listaAdquirente;
	}
	public String getEmpresa_telefone() {
		return empresa_telefone;
	}
	public String getNota_dataemissao() {
		return nota_dataemissao;
	}
	public String getNota_horaemissao() {
		return nota_horaemissao;
	}
	public Double getNota_valor_total_imposto_federal() {
		return nota_valor_total_imposto_federal;
	}
	public Double getNota_valor_total_imposto_estadual() {
		return nota_valor_total_imposto_estadual;
	}
	public Double getNota_valor_total_imposto_municipal() {
		return nota_valor_total_imposto_municipal;
	}
	public void setNota_valor_total_imposto_federal(
			Double nota_valor_total_imposto_federal) {
		this.nota_valor_total_imposto_federal = nota_valor_total_imposto_federal;
	}
	public void setNota_valor_total_imposto_estadual(
			Double nota_valor_total_imposto_estadual) {
		this.nota_valor_total_imposto_estadual = nota_valor_total_imposto_estadual;
	}
	public void setNota_valor_total_imposto_municipal(
			Double nota_valor_total_imposto_municipal) {
		this.nota_valor_total_imposto_municipal = nota_valor_total_imposto_municipal;
	}
	public void setNota_dataemissao(String nota_dataemissao) {
		this.nota_dataemissao = nota_dataemissao;
	}
	public void setNota_horaemissao(String nota_horaemissao) {
		this.nota_horaemissao = nota_horaemissao;
	}
	public void setEmpresa_telefone(String empresa_telefone) {
		this.empresa_telefone = empresa_telefone;
	}
	public void setListaAdquirente(
			List<EmitirComprovanteTEFAdquirenteBean> listaAdquirente) {
		this.listaAdquirente = listaAdquirente;
	}
	public void setData(String data) {
		this.data = data;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setCondicao_pagamento(String condicao_pagamento) {
		this.condicao_pagamento = condicao_pagamento;
	}
	public void setPesobruto_total(Double pesobruto_total) {
		this.pesobruto_total = pesobruto_total;
	}
	public void setPesoliquido_total(Double pesoliquido_total) {
		this.pesoliquido_total = pesoliquido_total;
	}
	public void setQuantidade_itens(Integer quantidade_itens) {
		this.quantidade_itens = quantidade_itens;
	}
	public void setValor_produtos(Double valor_produtos) {
		this.valor_produtos = valor_produtos;
	}
	public void setValor_desconto(Double valor_desconto) {
		this.valor_desconto = valor_desconto;
	}
	public void setValor_total(Double valor_total) {
		this.valor_total = valor_total;
	}
	public void setValor_pagamento(Double valor_pagamento) {
		this.valor_pagamento = valor_pagamento;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setNota_chaveacesso(String nota_chaveacesso) {
		this.nota_chaveacesso = nota_chaveacesso;
	}
	public void setNota_numero(String nota_numero) {
		this.nota_numero = nota_numero;
	}
	public void setNota_serie(Integer nota_serie) {
		this.nota_serie = nota_serie;
	}
	public void setNota_protocoloautorizacao(String nota_protocoloautorizacao) {
		this.nota_protocoloautorizacao = nota_protocoloautorizacao;
	}
	public void setNota_dataautorizacao(String nota_dataautorizacao) {
		this.nota_dataautorizacao = nota_dataautorizacao;
	}
	public void setNota_qrcode(String nota_qrcode) {
		this.nota_qrcode = nota_qrcode;
	}
	public void setListaItens(List<EmitirComprovanteTEFItemBean> listaItens) {
		this.listaItens = listaItens;
	}
	public void setListaPagamentos(
			List<EmitirComprovanteTEFPagamentoBean> listaPagamentos) {
		this.listaPagamentos = listaPagamentos;
	}
	public void setEmpresa_endereco(String empresa_endereco) {
		this.empresa_endereco = empresa_endereco;
	}
	public void setCliente_nome(String cliente_nome) {
		this.cliente_nome = cliente_nome;
	}
	public void setCliente_cpfcnpj(String cliente_cpfcnpj) {
		this.cliente_cpfcnpj = cliente_cpfcnpj;
	}
	public void setCliente_endereco(String cliente_endereco) {
		this.cliente_endereco = cliente_endereco;
	}
	public void setCliente_telefone(String cliente_telefone) {
		this.cliente_telefone = cliente_telefone;
	}
	public void setCliente_ie(String cliente_ie) {
		this.cliente_ie = cliente_ie;
	}
	public void setEmpresa_nome(String empresa_nome) {
		this.empresa_nome = empresa_nome;
	}
	public void setEmpresa_cnpj(String empresa_cnpj) {
		this.empresa_cnpj = empresa_cnpj;
	}
	
}
