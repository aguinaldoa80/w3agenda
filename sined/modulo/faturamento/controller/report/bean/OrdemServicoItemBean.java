package br.com.linkcom.sined.modulo.faturamento.controller.report.bean;

public class OrdemServicoItemBean {
	
	private String item;
	private String numeroAutorizacaoFaturamentoItemOS;
	private String nome;
	private String nomenf;
	private Double quantidade;
	private Double valorunitario;
	
	public String getItem() {
		return item;
	}
	public String getNumeroAutorizacaoFaturamentoItemOS() {
		return numeroAutorizacaoFaturamentoItemOS;
	}
	public String getNome() {
		return nome;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public void setNumeroAutorizacaoFaturamentoItemOS(String numeroAutorizacaoFaturamentoItemOS) {
		this.numeroAutorizacaoFaturamentoItemOS = numeroAutorizacaoFaturamentoItemOS;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNomenf() {
		return nomenf;
	}
	public void setNomenf(String nomenf) {
		this.nomenf = nomenf;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Double getValorunitario() {
		return valorunitario;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
}
	
