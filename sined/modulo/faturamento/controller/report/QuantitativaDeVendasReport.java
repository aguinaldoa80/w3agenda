package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.io.IOException;

import javax.servlet.http.Cookie;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MaterialtipoService;
import br.com.linkcom.sined.geral.service.VendamaterialService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.QuantitativaDeVendasFiltro;

@Controller(path = "/faturamento/relatorio/Quantitativadevendas", authorizationModule=ReportAuthorizationModule.class)
public class QuantitativaDeVendasReport extends ResourceSenderController<QuantitativaDeVendasFiltro> {

	private VendamaterialService vendamaterialService;
	private EmpresaService empresaService;
	private MaterialgrupoService materialgrupoService;
	private MaterialtipoService materialtipoService;
	
	private static final String[] TIPO_DE_ORDENACAO = { "Crescente", "Decrescente" };

	public void setVendamaterialService(VendamaterialService vendamaterialService) {
		this.vendamaterialService = vendamaterialService;
	}
	
	public void setMaterialgrupoService(MaterialgrupoService materialgrupoService) {
		this.materialgrupoService = materialgrupoService;
	}
	
	public void setMaterialtipoService(MaterialtipoService materialtipoService) {
		this.materialtipoService = materialtipoService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, QuantitativaDeVendasFiltro filtro) throws Exception {
		request.setAttribute("listaTipoDeOrdenacao", TIPO_DE_ORDENACAO);
		filtro.setPageSize(Integer.MAX_VALUE);
		filtro.setTipoDeOrdenacao(TIPO_DE_ORDENACAO[1]);
		return new ModelAndView("relatorio/quantitativadevendas", "filtro", filtro);
	}

	@Override
	public Resource generateResource(WebRequestContext request, QuantitativaDeVendasFiltro filtro) {
		try {
			if(filtro.getMaterialgrupo() != null)
				filtro.setMaterialgrupo(materialgrupoService.load(filtro.getMaterialgrupo(), "materialgrupo.cdmaterialgrupo, materialgrupo.nome"));
			if(filtro.getMaterialtipo() != null)
				filtro.setMaterialtipo(materialtipoService.load(filtro.getMaterialtipo(), "materialtipo.cdmaterialtipo, materialtipo.nome"));
			if(filtro.getEmpresa() != null){
				filtro.setEmpresa(empresaService.load(filtro.getEmpresa(), "empresa.cdpessoa, empresa.razaosocial"));				
			}
			Resource r  = vendamaterialService.gerarRelatorioExcelQuantitaticaDeVendas(filtro);
			request.getServletResponse().addCookie(new Cookie("blockbutton", "false"));
			return r;
		}catch (IOException e){
			throw new RuntimeException("Relatório indisponível temporariamente, por favor entre em contato com o suporte.");
		}
	}
}
