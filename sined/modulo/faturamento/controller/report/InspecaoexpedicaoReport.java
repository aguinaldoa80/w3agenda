package br.com.linkcom.sined.modulo.faturamento.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.ExpedicaoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ExpedicaoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/faturamento/relatorio/Inspecaoexpedicao", authorizationModule=ReportAuthorizationModule.class)
public class InspecaoexpedicaoReport extends SinedReport<ExpedicaoFiltro>{

	private ExpedicaoService expedicaoService;

	public void setExpedicaoService(ExpedicaoService expedicaoService) {
		this.expedicaoService = expedicaoService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request,	ExpedicaoFiltro filtro) throws Exception {
		return expedicaoService.gerarRelatorioInspecaoExpedicao(filtro, request.getParameter("selectedItens"));
	}

	@Override
	public String getTitulo(ExpedicaoFiltro filtro) {
		return "INSPE��O DE EXPEDI��O";
	}
	
	@Override
	public String getNomeArquivo() {
		return "inspecaoentrega";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, ExpedicaoFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);
		if(filtro.getEmpresa() != null){
			report.addParameter("EMPRESA", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}
		
	}

}
