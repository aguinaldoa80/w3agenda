package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.VendaorcamentoService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EmitircustovendaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/faturamento/relatorio/Emitircustovendaorcamento", authorizationModule=ReportAuthorizationModule.class)
public class EmitircustovendaOrcamentovendaReport extends SinedReport<EmitircustovendaFiltro>{

	private VendaorcamentoService vendaorcamentoService;
	private ColaboradorService colaboradorService;
	
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setPedidovendaService(VendaorcamentoService vendaorcamentoService) {this.vendaorcamentoService = vendaorcamentoService;}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request,	EmitircustovendaFiltro filtro) throws ResourceGenerationException {
		String urlRedirecionamento = "/faturamento/crud/Vendaorcamento";
		SinedUtil.redirecionamento(request, urlRedirecionamento);
		
		return null;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, EmitircustovendaFiltro filtro) throws Exception {
		EmitircustovendaFiltro filtro2 = new EmitircustovendaFiltro();
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(whereIn == null || "".equals(whereIn)) {
			throw new SinedException("Par�metro inv�lido.");
		}
		
		filtro2.setWhereInVendaorcamento(whereIn);
		
		return vendaorcamentoService.gerarRelatorioCustoorcamentovenda(filtro2);
	}

	@Override
	public String getNomeArquivo() {
		return "emitircustovenda_vendaorcamento";
	}

	@Override
	public String getTitulo(EmitircustovendaFiltro filtro) {
		return "Relat�rio de Custos x Venda (Or�amento)";
	}
		
	@Override
	protected void adicionaParametrosFiltro(Report report, EmitircustovendaFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);
		
		if(filtro.getEmpresa() != null){
			report.addParameter("NOMEEMPRESA", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}
		
		report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
	}
	
	public ModelAndView ajaxLoadComboVendedor(WebRequestContext request){
		if(request.getParameter("cdempresa") == null) {
			throw new SinedException("Empresa n�o pode ser nula");
		}
		
		Empresa empresa = new Empresa(Integer.parseInt(request.getParameter("cdempresa")));
		List<Colaborador> lista = colaboradorService.findColaboradoresComVenda(empresa, null, false);
		
		StringBuilder strOpt = new StringBuilder();
		strOpt.append("<option value='<null>'></option>");
		
		for(Colaborador c : lista){
			strOpt.append("<option value='br.com.linkcom.sined.geral.bean.Colaborador[cdpessoa=")
				.append(c.getCdpessoa())
				.append("]'>")
				.append(c.getNome())
				.append("</option>");
		}
		
		return new JsonModelAndView().addObject("strHtml", strOpt.toString());
	}
}