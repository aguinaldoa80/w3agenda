package br.com.linkcom.sined.modulo.faturamento.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.MetacomercialService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MetacomercialFiltro;

@Controller(path = "/faturamento/relatorio/MetacomercialCSV", authorizationModule=ReportAuthorizationModule.class)
public class MetacomercialCSVReport extends ResourceSenderController<MetacomercialFiltro>{

	private MetacomercialService metacomercialService;
	
	public void setMetacomercialService(MetacomercialService metacomercialService) {
		this.metacomercialService = metacomercialService;
	}

	@Override
	public Resource generateResource(WebRequestContext request, MetacomercialFiltro filtro) throws Exception {
		return metacomercialService.createMetacomercialCSVReport(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, MetacomercialFiltro filtro) throws Exception {
		return null;
	}

}