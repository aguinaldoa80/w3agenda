package br.com.linkcom.sined.modulo.faturamento.controller.report;


import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.EmitircurvaabcFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/faturamento/relatorio/Emitircurvaabcvenda", authorizationModule=ReportAuthorizationModule.class)
public class EmitircurvaabcvendaReport extends SinedReport<EmitircurvaabcFiltro>{

	private VendaService vendaService;
	private UsuarioService usuarioService;
	private ColaboradorService colaboradorService;
	private static final String ALERTA_MAIOR_QUE_CINCO_ANOS = "Per�odo m�ximo permitido de 05 anos (60 meses).";
	private static final String ALERTA_MAIOR_QUE_DOIS_ANOS = "Per�odo m�ximo permitido de 02 anos (24 meses).\n"+
											"Para realizar o filtro num per�odo superior a 02 anos, favor preencher o(s) campo(s) Cliente ou Material.";
	
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	
	// FEITO ESSA ACTION PORQUE NO TABLET N�O ACEITA O PARAMETERS DO SUBMIT
	public ModelAndView gerarCSV(WebRequestContext request, EmitircurvaabcFiltro filtro) throws Exception {
		SinedUtil.setCookieBlockButton(request);
		if(filtro.getCliente() != null || filtro.getMaterial() != null){
			if(SinedDateUtils.mesesEntre(filtro.getDtInicio(), filtro.getDtFim() != null ? filtro.getDtFim() : SinedDateUtils.currentDate()) > 60){
				request.addError(ALERTA_MAIOR_QUE_CINCO_ANOS);
				return doFiltro(request, filtro);
			}
		}else{
			if(SinedDateUtils.mesesEntre(filtro.getDtInicio(), filtro.getDtFim() != null ? filtro.getDtFim() : SinedDateUtils.currentDate()) > 24){
				request.addError(ALERTA_MAIOR_QUE_DOIS_ANOS);
				return doFiltro(request, filtro);
			}
		}
		
		filtro.setIsRestricaoClienteVendedor(usuarioService.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado()));
		filtro.setIsRestricaoVendaVendedor(usuarioService.isRestricaoVendaVendedor(SinedUtil.getUsuarioLogado()));
		
		Resource resource = vendaService.gerarRelatorioCSVCurvaABC(filtro);
		return new ResourceModelAndView(resource);
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	EmitircurvaabcFiltro filtro) throws Exception {
		filtro.setIsRestricaoClienteVendedor(usuarioService.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado()));
		filtro.setIsRestricaoVendaVendedor(usuarioService.isRestricaoVendaVendedor(SinedUtil.getUsuarioLogado()));
		return super.generateResource(request, filtro);
	}

	@Override
	public IReport createReportSined(WebRequestContext request, EmitircurvaabcFiltro filtro) throws Exception {
		if(filtro.getCliente() != null || filtro.getMaterial() != null){
			if(SinedDateUtils.mesesEntre(filtro.getDtInicio(), filtro.getDtFim() != null ? filtro.getDtFim() : SinedDateUtils.currentDate()) > 60){
				throw new SinedException(ALERTA_MAIOR_QUE_CINCO_ANOS);
			}
		}else{
			if(SinedDateUtils.mesesEntre(filtro.getDtInicio(), filtro.getDtFim() != null ? filtro.getDtFim() : SinedDateUtils.currentDate()) > 24){
				throw new SinedException(ALERTA_MAIOR_QUE_DOIS_ANOS);
			}
		}
		return vendaService.gerarRelatorioCurvaABC(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "emitircurvaabcvenda";
	}

	@Override
	public String getTitulo(EmitircurvaabcFiltro filtro) {
		return "CURVA ABC";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, EmitircurvaabcFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);
		
		if(filtro.getEmpresa() != null){
			report.addParameter("NOMEEMPRESA", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		} else {
			report.addParameter("NOMEEMPRESA", "");
		}
		
		if(filtro.getProjeto() != null){
			Projeto projeto = getProjetoService().load(filtro.getProjeto(), "projeto.nome");
			report.addParameter("NOMEPROJETO", projeto.getNome());
		} else {
			report.addParameter("NOMEPROJETO", "");
		}
		
		report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtInicio(), filtro.getDtFim()));
	}
	
	public ModelAndView ajaxLoadComboVendedor(WebRequestContext request, Empresa empresa){
		Boolean isRestricaoVendaVendedor = usuarioService.isRestricaoVendaVendedor(SinedUtil.getUsuarioLogado());
		List<Colaborador> lista = colaboradorService.findColaboradoresComVenda(empresa, isRestricaoVendaVendedor ? SinedUtil.getUsuarioComoColaborador() : null, false);
		
		StringBuilder strOpt = new StringBuilder("<option value='<null>'></option>");
		for(Colaborador c : lista){
			strOpt.append("<option value='br.com.linkcom.sined.geral.bean.Colaborador[cdpessoa=")
				.append(c.getCdpessoa())
				.append("]'>")
				.append(c.getNome())
				.append("</option>");
		}
		
		return new JsonModelAndView().addObject("strHtml", strOpt.toString());
	}

}

