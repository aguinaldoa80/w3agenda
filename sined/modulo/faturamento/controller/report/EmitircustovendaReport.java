package br.com.linkcom.sined.modulo.faturamento.controller.report;


import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendatipoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.geral.service.VendaorcamentoService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EmitircustovendaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/faturamento/relatorio/Emitircustovenda", authorizationModule=ReportAuthorizationModule.class)
public class EmitircustovendaReport extends SinedReport<EmitircustovendaFiltro>{

	private VendaService vendaService;
	private ColaboradorService colaboradorService;
	private UsuarioService usuarioService;
	private PedidovendatipoService pedidovendatipoService;
	private PedidovendaService pedidovendaService;
	private VendaorcamentoService vendaorcamentoService;
	
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setPedidovendatipoService(PedidovendatipoService pedidovendatipoService) {this.pedidovendatipoService = pedidovendatipoService;}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	public void setVendaorcamentoService(VendaorcamentoService vendaorcamentoService) {this.vendaorcamentoService = vendaorcamentoService;}
	
	@Override
	public IReport createReportSined(WebRequestContext request, EmitircustovendaFiltro filtro) throws Exception {
		filtro.setIsRestricaoClienteVendedor(usuarioService.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado()));
		filtro.setIsRestricaoVendaVendedor(usuarioService.isRestricaoVendaVendedor(SinedUtil.getUsuarioLogado()));
		
		String whereIn = request.getParameter("selectedItens");
		if(whereIn != null && !"".equals(whereIn)){
			EmitircustovendaFiltro filtro2 = new EmitircustovendaFiltro();
			filtro2.setConsiderarDescontos(true);
			filtro2.setWhereInVenda(whereIn);
			return vendaService.gerarRelatorioCustovenda(filtro2);
		}else if(filtro.getDtinicio() == null || filtro.getDtfim() == null){
			throw new SinedException("Par�metro inv�lido");
		}
		if (filtro.getOrigem() != null && filtro.getOrigem().equalsIgnoreCase("Pedido de Venda")) {
			return pedidovendaService.gerarRelatorioCustopedidovenda(filtro);
		} else if (filtro.getOrigem() != null && filtro.getOrigem().equalsIgnoreCase("Venda")){
			return vendaService.gerarRelatorioCustovenda(filtro);
		} else if (filtro.getOrigem() != null && filtro.getOrigem().equalsIgnoreCase("Or�amento")) {
			return vendaorcamentoService.gerarRelatorioCustoorcamentovenda(filtro);
		} else {
			throw new SinedException("Par�metro inv�lido");
		}
	}

	@Override
	public String getNomeArquivo() {
		return "emitircustovenda";
	}

	@Override
	public String getTitulo(EmitircustovendaFiltro filtro) {
		return "Relat�rio de Custos x Venda";
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request,	EmitircustovendaFiltro filtro) throws ResourceGenerationException {
		
		List<String> listaCalcularmargempor = new ArrayList<String>();
		
		listaCalcularmargempor.add("Custo");
		listaCalcularmargempor.add("Venda");
		
		List<String> listaOrigem = new ArrayList<String>();
		
		listaOrigem.add("Venda");
		listaOrigem.add("Pedido de Venda");
		listaOrigem.add("Or�amento");
		
		List<Pedidovendasituacao>listaPedidoVendasituacao = new ArrayList<Pedidovendasituacao>();
		listaPedidoVendasituacao.add(Pedidovendasituacao.PREVISTA);
		listaPedidoVendasituacao.add(Pedidovendasituacao.CONFIRMADO);
		listaPedidoVendasituacao.add(Pedidovendasituacao.CONFIRMADO_PARCIALMENTE);
		listaPedidoVendasituacao.add(Pedidovendasituacao.AGUARDANDO_APROVACAO);
		
		request.setAttribute("listaPedidoVendasituacao", listaPedidoVendasituacao);
		request.setAttribute("listaCalcularmargempor", listaCalcularmargempor);
		request.setAttribute("listaPedidovendatipo", pedidovendatipoService.findAll());
		request.setAttribute("listaOrigem", listaOrigem);
		
		return super.doFiltro(request, filtro);
	}
		
	@Override
	protected void adicionaParametrosFiltro(Report report, EmitircustovendaFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);
		
		if(filtro.getEmpresa() != null){
			report.addParameter("NOMEEMPRESA", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}
		report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
	}
	
	public ModelAndView ajaxLoadComboVendedor(WebRequestContext request, Empresa empresa){
		boolean ativos = Boolean.parseBoolean(request.getParameter("ativos"));
		Boolean isRestricaoVendaVendedor = usuarioService.isRestricaoVendaVendedor(SinedUtil.getUsuarioLogado());		
		List<Colaborador> lista = colaboradorService.findColaboradoresComVenda(empresa, isRestricaoVendaVendedor ? SinedUtil.getUsuarioComoColaborador() : null, ativos);
				
		StringBuilder strOpt = new StringBuilder("<option value='<null>'></option>");
		for(Colaborador c : lista){
			strOpt.append("<option value='br.com.linkcom.sined.geral.bean.Colaborador[cdpessoa=")
				.append(c.getCdpessoa())
				.append("]'>")
				.append(c.getNome())
				.append("</option>");
		}
		
		return new JsonModelAndView().addObject("strHtml", strOpt.toString());
	}

}

