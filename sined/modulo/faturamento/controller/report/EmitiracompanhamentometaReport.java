package br.com.linkcom.sined.modulo.faturamento.controller.report;


import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Metacomercial;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.MetacomercialService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EmitiracompanhamentometaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/faturamento/relatorio/Emitiracompanhamentometa", authorizationModule=ReportAuthorizationModule.class)
public class EmitiracompanhamentometaReport extends SinedReport<EmitiracompanhamentometaFiltro>{

	private MetacomercialService metacomercialService;
	
	public void setMetacomercialService(MetacomercialService metacomercialService) {
		this.metacomercialService = metacomercialService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request, EmitiracompanhamentometaFiltro filtro) throws Exception {
		if(filtro.getDtreferenciaInicio() == null)
			throw new SinedException("O campo Data de Refer�ncia n�o pode ser nulo");
		
		DateFormat formatadorMesAno = new SimpleDateFormat("MMyyyy");
	
		if (!formatadorMesAno.format(filtro.getDtreferenciaInicio()).equals(formatadorMesAno.format(filtro.getDtreferenciaFim()))){
			throw new SinedException("O relat�rio deve ser analisado dentro de um mesmo m�s.");
		}
		if(filtro.getTiporelatorio() != null && filtro.getTiporelatorio().equals("analitico")){
			return metacomercialService.gerarRelatorioAcompanhamentometaAnalitico(filtro);
		}
		return metacomercialService.gerarRelatorioAcompanhamentometaSintetico(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "emitiracompanhamentometa";
	}

	@Override
	public String getTitulo(EmitiracompanhamentometaFiltro filtro) {
		return "Acompanhamento de Meta " + ("analitico".equalsIgnoreCase(filtro.getTiporelatorio()) ? "(Anal�tico)" : "");
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, EmitiracompanhamentometaFiltro filtro) throws ResourceGenerationException {
		if(filtro.getDtreferenciaFim() == null)	filtro.setDtreferenciaFim(new Date(System.currentTimeMillis()));
		if(filtro.getDtreferenciaInicio() == null) filtro.setDtreferenciaInicio(filtro.getFirstDateMonth());
		return super.doFiltro(request, filtro);
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, EmitiracompanhamentometaFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);
		
		if(filtro.getEmpresa() != null){
			report.addParameter("NOMEEMPRESA", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}
	}
	
	public ModelAndView ajaxLoadComboVendedor(WebRequestContext request, Empresa empresa){
		List<Colaborador> lista = ColaboradorService.getInstance().findColaboradoresComVenda(empresa, null, false);
		
		StringBuilder strOpt = new StringBuilder("<option value='<null>'></option>");
		for(Colaborador c : lista){
			strOpt.append("<option value='br.com.linkcom.sined.geral.bean.Colaborador[cdpessoa=")
				.append(c.getCdpessoa())
				.append("]'>")
				.append(c.getNome())
				.append("</option>");
		}
		
		return new JsonModelAndView().addObject("strHtml", strOpt.toString());
	}
		
		

}

