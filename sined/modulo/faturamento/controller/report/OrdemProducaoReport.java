package br.com.linkcom.sined.modulo.faturamento.controller.report;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(
	path = "/faturamento/relatorio/OrdemProducao", 
	authorizationModule=ReportAuthorizationModule.class
)
public class OrdemProducaoReport extends ResourceSenderController<FiltroListagemSined>{

	private VendaService vendaService;
	
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}

	@Override
	public Resource generateResource(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		if(vendaService.haveVendaSituacao(SinedUtil.getItensSelecionados(NeoWeb.getRequestContext()), false, Vendasituacao.CANCELADA, Vendasituacao.FATURADA, Vendasituacao.REALIZADA))
			throw new SinedException("S� � poss�vel emitir ordem de produ��o para a(s) venda(s) com situa��o igual a 'Em produ��o'.");

		return vendaService.gerarOrdemProducao(SinedUtil.getItensSelecionados(request));
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		return new ModelAndView("redirect:/faturamento/crud/Venda");
	}
	
}
