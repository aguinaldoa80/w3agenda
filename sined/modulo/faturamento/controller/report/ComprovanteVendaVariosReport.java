package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.geral.service.VendahistoricoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.VendaFiltro;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(
			path = "/faturamento/relatorio/Vendas", 
			authorizationModule=ReportAuthorizationModule.class
)
public class ComprovanteVendaVariosReport extends ResourceSenderController<VendaFiltro>{

	private VendaService vendaService;
	private EmpresaService empresaService;
	private VendahistoricoService vendahistoricoService;
	private ParametrogeralService parametrogeralService;
	
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setVendahistoricoService(VendahistoricoService vendahistoricoService) {
		this.vendahistoricoService = vendahistoricoService;
	}
	
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	@Override
	public ModelAndView doGerar(WebRequestContext request, VendaFiltro filtro) throws Exception {
		ComprovanteConfiguravel comprovante;
		String whereIn = filtro.getWhereIn();
		
		Vendasituacao [] situacoes = { Vendasituacao.AGUARDANDO_APROVACAO };
		
		if(!parametrogeralService.getBoolean("EMITIR_COMPROVANTE_AGUARDANDOAPROVACAO") && vendaService.haveVendaSituacao(whereIn, false, situacoes)) {
			request.addError("O envio e emiss�o do comprovante n�o s�o permitidos na situa��o de Aguardando Aprova��o.");
			if(vendaService.isOtr(Integer.parseInt(whereIn.split(",")[0]))){
				return new ModelAndView("redirect:/faturamento/crud/VendaOtr");
			}
			return new ModelAndView("redirect:/faturamento/crud/Venda");
		}
		
		boolean variosRelatorios = StringUtils.isNotEmpty(whereIn);
		String cdvendaStr = whereIn.contains(",") ? whereIn.substring(0, whereIn.indexOf(",")) : whereIn;
		Integer cdvenda = variosRelatorios ? Integer.parseInt( cdvendaStr) : filtro.getCdvenda();
		Venda venda = vendaService.load(new Venda(cdvenda));
		
		comprovante = empresaService.findComprovaConfiguravelByTipoAndEmpresa(
					ComprovanteConfiguravel.TipoComprovante.VENDA, 
					venda.getEmpresa());
		ModelAndView retorno;
		if( comprovante != null && comprovante.getLayout()!=null){
			if(variosRelatorios) 
				retorno = vendaService.gerarComprovantesConfiguraveis(request, whereIn, comprovante);
			else 
				retorno = vendaService.gerarComprovanteConfiguravel(request, cdvenda, comprovante);
		}else{
			retorno = super.doGerar(request, filtro);
		}
		List<Venda> listaVenda = new ArrayList<Venda>();
		if(variosRelatorios){
			listaVenda.addAll(vendaService.findVendaReport(whereIn));
		}else{
			listaVenda.add(venda);
		}
		vendahistoricoService.makeHistoricoComprovanteEmitido(listaVenda);
		return retorno;
	}


	@Override
	public Resource generateResource(WebRequestContext request, VendaFiltro filtro) throws Exception {
		return vendaService.gerarVariosComprovantes(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, VendaFiltro filtro) throws Exception {
		if(StringUtils.isNotBlank(filtro.getWhereIn()) && vendaService.isOtr(Integer.parseInt(filtro.getWhereIn().split(",")[0]))){
			return new ModelAndView("redirect:/faturamento/crud/VendaOtr");
		}
		return new ModelAndView("redirect:/faturamento/crud/Venda");
	}
	
}
