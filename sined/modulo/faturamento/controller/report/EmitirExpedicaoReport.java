package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.LinkedHashMap;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ExpedicaoService;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path = "/faturamento/relatorio/EmitirExpedicao", authorizationModule=ReportAuthorizationModule.class)
public class EmitirExpedicaoReport extends ReportTemplateController<ReportTemplateFiltro>{
	
	private ExpedicaoService expedicaoService;

	public void setExpedicaoService(ExpedicaoService expedicaoService) {this.expedicaoService = expedicaoService;}

	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.EMITIR_EXPEDICAO;
	}

	@Override
	protected String getFileName() {
		return "expedicao";
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	ReportTemplateFiltro filtro) {
		return getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.EMITIR_EXPEDICAO);
	}

	@Override
	protected LinkedHashMap<String, Object> carregaDados(WebRequestContext  request, ReportTemplateFiltro filtro) throws Exception {
		LinkedHashMap<String, Object> mapa =  new LinkedHashMap<String, Object>();
		mapa.put("lista", expedicaoService.gerarEmissaoExpedicaoBean(SinedUtil.getItensSelecionados(request)));
		return mapa;
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, ReportTemplateFiltro filtro) throws Exception {
		return new ModelAndView("redirect:/faturamento/crud/Expedicao");
	}
	
}
