package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.LinkedHashMap;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ContratofaturalocacaoService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EmitirFaturaLocacaoContratoFiltro;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path = "/faturamento/relatorio/Emitirfaturalocacao", authorizationModule=ReportAuthorizationModule.class)
public class EmitirFaturaLocacaoContratoReport extends ReportTemplateController<EmitirFaturaLocacaoContratoFiltro> {
	
	private ContratofaturalocacaoService contratofaturalocacaoService;
	
	public void setContratofaturalocacaoService(ContratofaturalocacaoService contratofaturalocacaoService) {
		this.contratofaturalocacaoService = contratofaturalocacaoService;
	}
	
	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.CONTRATO_FATURA_LOCACAO_EMISSAO;
	}
	
	@Override
	protected LinkedHashMap<String, Object> carregaDados(WebRequestContext request, EmitirFaturaLocacaoContratoFiltro filtro) throws Exception {	
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		if (filtro.getWhereIn()!=null){
			map.put("lista", contratofaturalocacaoService.emitirFaturaLocacao(filtro.getWhereIn()));			
		}else {
			map.put("lista", contratofaturalocacaoService.emitirFaturaLocacao(SinedUtil.getItensSelecionados(request)));
		}
		return map;
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	EmitirFaturaLocacaoContratoFiltro filtro) {
		return getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.CONTRATO_FATURA_LOCACAO_EMISSAO);
	}
	
	@Override
	protected String getFileName() {
		return "faturalocacao";
	}
	
}
