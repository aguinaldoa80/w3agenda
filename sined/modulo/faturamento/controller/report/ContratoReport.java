package br.com.linkcom.sined.modulo.faturamento.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ContratoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/faturamento/relatorio/Contrato",	authorizationModule=ReportAuthorizationModule.class)
public class ContratoReport extends SinedReport<ContratoFiltro> {
	
	private ContratoService contratoService;
	
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, ContratoFiltro filtro) throws Exception {
		return contratoService.gerarRelatorioListagem(filtro);
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, ContratoFiltro filtro) {
		if(filtro.getClientenome() != null){
			report.addParameter("CLIENTE", clienteService.load(filtro.getClientenome() , "cliente.nome").getNome());
		} else if(filtro.getClienterazaosocial() != null){
			report.addParameter("CLIENTE", clienteService.load(filtro.getClienterazaosocial() , "cliente.nome").getNome());
		}
		report.addParameter("DESCRICAO", filtro.getDescricao() == null || filtro.getDescricao().equals("") ? null : filtro.getDescricao());
		report.addParameter("EMPRESAFILTRO", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		report.addParameter("INICIO", SinedUtil.getDescricaoPeriodo(filtro.getDtiniciode(), filtro.getDtinicioate()));
		report.addParameter("TERMINO", SinedUtil.getDescricaoPeriodo(filtro.getDtfimde(), filtro.getDtfimate()));
		report.addParameter("SITUACOES", filtro.getListaSituacao() != null ? SituacaoContrato.listAndConcatenate(filtro.getListaSituacao()) : "");
		
		super.adicionaParametrosFiltro(report, filtro);
	}
	
	@Override
	public String getTitulo(ContratoFiltro filtro) {
		return "CONTRATOS";
	}
	
	@Override
	public String getNomeArquivo() {
		return "contrato";
	}
}
