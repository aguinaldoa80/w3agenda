package br.com.linkcom.sined.modulo.faturamento.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(path = "/faturamento/relatorio/GPS",	authorizationModule=ReportAuthorizationModule.class)
public class GPSReport extends ResourceSenderController<FiltroListagemSined> {
	
	private NotaFiscalServicoService notaFiscalServicoService;
	
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	
	@Override
	public Resource generateResource(WebRequestContext request,FiltroListagemSined filtro) throws Exception {
		return notaFiscalServicoService.gerarGPS(SinedUtil.getItensSelecionados(request));
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico");
	}
	
	
}
