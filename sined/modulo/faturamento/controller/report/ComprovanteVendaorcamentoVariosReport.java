package br.com.linkcom.sined.modulo.faturamento.controller.report;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.VendaorcamentoService;
import br.com.linkcom.sined.geral.service.VendaorcamentohistoricoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.VendaorcamentoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(
			path = "/faturamento/relatorio/VendaOrcamentos", 
			authorizationModule=ReportAuthorizationModule.class
)
public class ComprovanteVendaorcamentoVariosReport extends ResourceSenderController<VendaorcamentoFiltro>{

	private EmpresaService empresaService;
	private VendaorcamentoService vendaorcamentoService;
	private VendaorcamentohistoricoService vendaorcamentohistoricoService;
	private ParametrogeralService parametrogeralService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setVendaorcamentoService(VendaorcamentoService vendaorcamentoService) {
		this.vendaorcamentoService = vendaorcamentoService;
	}
	
	public void setVendaorcamentohistoricoService(VendaorcamentohistoricoService vendaorcamentohistoricoService) {
		this.vendaorcamentohistoricoService = vendaorcamentohistoricoService;
	}
	
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	@Override
	public ModelAndView doGerar(WebRequestContext request, VendaorcamentoFiltro filtro) throws Exception {
		String whereIn = filtro.getWhereIn();
		boolean variosRelatorios = StringUtils.isNotEmpty(whereIn);
		String cdvendaStr = whereIn.contains(",") ? whereIn.substring(0, whereIn.indexOf(",")) : whereIn;
		Integer cdvendaorcamento = variosRelatorios ? Integer.parseInt( cdvendaStr) : filtro.getCdvendaorcamento();
		
		Vendaorcamento vendaorcamento = vendaorcamentoService.load(new Vendaorcamento(cdvendaorcamento));
		ComprovanteConfiguravel comprovante = empresaService.findComprovaConfiguravelByTipoAndEmpresa(
					ComprovanteConfiguravel.TipoComprovante.ORCAMENTO, 
					vendaorcamento.getEmpresa() );
		ModelAndView retorno;
		if(comprovante != null && comprovante.getLayout()!=null){
			if(variosRelatorios) 
				retorno = vendaorcamentoService.gerarComprovantesConfiguraveis(request, whereIn, comprovante);
			else 
				retorno = vendaorcamentoService.gerarComprovanteConfiguravel(request, cdvendaorcamento, comprovante);
		} else {
			retorno = super.doGerar(request, filtro);
		}
		
		List<Vendaorcamento> listaVendaorcamento = new ArrayList<Vendaorcamento>();
		if(variosRelatorios){
			listaVendaorcamento.addAll(vendaorcamentoService.findVendaorcamentoReport(whereIn));
		}else{
			listaVendaorcamento.add(vendaorcamento);
		}
		vendaorcamentohistoricoService.makeHistoricoComprovanteEmitido(listaVendaorcamento);
		
		return retorno;
	}

	@Override
	public Resource generateResource(WebRequestContext request, VendaorcamentoFiltro filtro) throws Exception {
		return vendaorcamentoService.gerarVariosComprovantes(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, VendaorcamentoFiltro filtro) throws Exception {
		return new ModelAndView("redirect:/faturamento/crud/VendaOrcamento");
	}
	
}
