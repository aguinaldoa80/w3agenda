package br.com.linkcom.sined.modulo.faturamento.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.geral.service.VendasituacaoService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.VendasPorVendedorFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/faturamento/relatorio/VendasPorVendedor", authorizationModule = ReportAuthorizationModule.class)
public class VendasPorVendedorReport extends SinedReport<VendasPorVendedorFiltro> {

	private ColaboradorService colaboradorService;
	private VendasituacaoService vendaSituacaoService;
	private VendaService vendaService;

	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}

	public void setVendaSituacaoService(VendasituacaoService vendaSituacaoService) {
		this.vendaSituacaoService = vendaSituacaoService;
	}
	
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request,	VendasPorVendedorFiltro filtro) throws Exception {
		return vendaService.gerarRelatorioVendasPorVendedor(filtro);
	}

	@Override
	protected void adicionaParametrosFiltro(Report report, VendasPorVendedorFiltro filtro) {
		report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
		report.addParameter("EMPRESAFILTRO", filtro.getEmpresa() != null ? empresaService.load(filtro.getEmpresa()).getNome() : "");
		report.addParameter("CLIENTEFILTRO", filtro.getCliente() != null ? clienteService.load(filtro.getCliente()).getNome() : "");
		report.addParameter("SITUACAOFILTRO", filtro.getSituacaoList() != null ? filtro.getSituacaoListString() : "");
		report.addParameter("RECAPAGEM", filtro.getRecapagem());
		
		super.adicionaParametrosFiltro(report, filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "vendasPorVendedor";
	}

	@Override
	public String getTitulo(VendasPorVendedorFiltro filtro) {
		return "VENDAS POR VENDEDOR";
	}

	@Override
	protected void filtro(WebRequestContext request, VendasPorVendedorFiltro filtro) throws Exception {
		super.filtro(request, filtro);

		filtro.setDtinicio(SinedDateUtils.firstDateOfMonth());
		filtro.setDtfim(SinedDateUtils.lastDateOfMonth());
		
		if (filtro.getSituacaoList() == null) {
			filtro.setSituacaoList(vendaSituacaoService.getSituacaoVendaPorVendedorPadrao());
		}
		
		request.setAttribute("listaSituacao", vendaSituacaoService.getSituacaoVendaPorVendedor());
		request.setAttribute("listaColaborador", colaboradorService.carregaColaboradorAtivo());
	}
}
