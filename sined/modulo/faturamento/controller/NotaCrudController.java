package br.com.linkcom.sined.modulo.faturamento.controller;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.crud.AbstractCrudController;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.NotaHistoricoService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.geral.service.TelefoneService;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


public class NotaCrudController<FILTRO extends FiltroListagem, FORMBEAN extends Nota, BEAN extends Nota> extends CrudControllerSined<FILTRO, FORMBEAN, BEAN> {

	protected NotaHistoricoService notaHistoricoService;
	protected ClienteService clienteService;
	protected TelefoneService telefoneService;
	protected NotaService notaService;
	private FornecedorService fornecedorService;
	
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setNotaHistoricoService(NotaHistoricoService notaHistoricoService) {this.notaHistoricoService = notaHistoricoService;}
	public void setTelefoneService(TelefoneService telefoneService) {this.telefoneService = telefoneService;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, FILTRO filtro) throws CrudException {
		return new ModelAndView("redirect:/faturamento/crud/Nota?ACAO=" + LISTAGEM);
	}
	
	@Override
	protected void entrada(WebRequestContext request, FORMBEAN form) throws Exception {
		boolean podeEditar = !NotaStatus.LIQUIDADA.equals(form.getNotaStatus()) && !NotaStatus.NFSE_LIQUIDADA.equals(form.getNotaStatus());
		request.setAttribute("podeEditar", podeEditar);
		
		//Action Editar
		if(AbstractCrudController.EDITAR.equals(request.getParameter(MultiActionController.ACTION_PARAMETER))){
			if(!podeEditar){
				throw new SinedException("Uma nota liquidada n�o deve ser editada.");
			}
		}
		if (form.getCdNota() != null) {
			this.obterHistorico(form);
			this.telefonePersistenteParaTransiente(form);
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, BEAN bean) throws Exception {
		if (bean.getCdNota() == null) {
			notaHistoricoService.adicionaHistoricoEmitida(bean);
		}
		this.telefoneTransienteParaPersistente(bean);
		super.salvar(request, bean);
	}

	
	/**
	 * Carrega e seta o hist�rico da Nota.
	 * 
	 * @see br.com.linkcom.sined.geral.service.NotaHistoricoService#carregarListaHistorico(Nota)
	 * @param bean
	 * @author Hugo Ferreira
	 */
	private void obterHistorico(FORMBEAN bean) {
		List<NotaHistorico> listaHistorico = notaHistoricoService.carregarListaHistorico(bean);
		bean.setListaNotaHistorico(listaHistorico);
	}
	
	/**
	 * Recebe um cliente;
	 * Carrega o cliente com os seguintes dados:
	 * 	- Raz�o social
	 * 	- CNPJ/CPF
	 *  - Inscri��o Estadual
	 * 	- Todos os endere�os associados
	 * 	- Todos os telefones associados
	 * 
	 * Envia os dados para a View
	 * 
	 * @see
	 * @param request
	 * @author Hugo Ferreira
	 */
	public void ajaxOnSelectProjeto(WebRequestContext request, Notafiscalproduto notaProduto) {
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");

		if (notaProduto.getCliente() == null || notaProduto.getCliente().getCdpessoa() == null) {
			view.println("var sucesso = false;");
			return;
		}

		try {
			Cliente cliente = notaProduto.getCliente();
			cliente = clienteService.carregarDadosCliente(cliente);
			
			if (cliente == null) {
				view.println("var msg = 'O Projeto selecionado n�o possui um cliente associado.';");
				view.println("var sucesso = false;");
				return;
			}
			

			String razaoSocial = cliente.getRazaosocial() != null ? cliente.getRazaosocial() : "";
			String cnpj = cliente.getCpfOuCnpj() != null ? cliente.getCpfOuCnpj() : "";
			String inscricaoEstadual = cliente.getInscricaoestadual() != null ? cliente.getInscricaoestadual() : "";

			view.println("var sucesso = true;");
			view.println("var razaoSocial = '" + razaoSocial + "';");
			view.println("var inscricaoEstadual = '" + inscricaoEstadual + "';");
			view.println("var cnpjCpf = '" + cnpj + "';");
		} catch (Exception e) {
			view.println("var sucesso = false;");
			view.println("var msg = 'Ocorreu um erro ao recuperar os dados do cliente referente ao projeto.';");
		}
	}

	/**
	 * Recebe um transportador;
	 * Carrega o transportador com os seguintes dados:
	 * 	- Raz�o social
	 * 	- CNPJ/CPF
	 *  - Inscri��o Estadual
	 * 	- Todos os endere�os associados
	 * 	- Todos os telefones associados
	 * 
	 * Envia os dados para a View
	 * 
	 * @see
	 * @param request
	 * @author Fl�vio Tavares
	 */
	public void ajaxOnSelectTransportador(WebRequestContext request, Fornecedor transportador){
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		if (transportador == null || transportador.getCdpessoa() == null) {
			view.println("var sucesso = false;");
			view.println("var msg = false;");
			return;
		}

		try {
			transportador = fornecedorService.load(transportador, "fornecedor.cdpessoa,fornecedor.razaosocial, fornecedor.inscricaoestadual," +
					"fornecedor.cnpj, fornecedor.cpf");
			
			String razaoSocial = transportador.getRazaosocial() != null ? transportador.getRazaosocial() : "";
			String cnpj = transportador.getCpfOuCnpj() != null ? transportador.getCpfOuCnpj() : "";
			String inscricaoEstadual = transportador.getInscricaoestadual() != null ? transportador.getInscricaoestadual() : "";

			view.println("var sucesso = true;");
			view.println("var razaoSocial = '" + razaoSocial + "';");
			view.println("var inscricaoEstadual = '" + inscricaoEstadual + "';");
			view.println("var cnpjCpf = '" + cnpj + "';");
			view.println("var msg = false;");
		} catch (Exception e) {
			view.println("var sucesso = false;");
			view.println("var msg = 'Ocorreu um erro ao recuperar os dados do transportador.';");
		}
	}
	
	/**
	 * Copia o telefone do cliente do campo transiente (telefoneClienteTransiente) para
	 * o campo persistente (telefoneCliente).
	 * 
	 * @param bean
	 * @author Hugo Ferreira
	 */
	protected void telefoneTransienteParaPersistente(BEAN bean) {
		Telefone telefone = bean.getTelefoneClienteTransiente();
		if (telefone != null && telefone.getCdtelefone() != null) {
			if (telefone.getCdtelefone() != -1) {
				telefone = telefoneService.load(telefone, "telefone.telefone");
				bean.setTelefoneCliente(telefone.getTelefone());
			} else {
				bean.setTelefoneCliente(notaService.load(bean, "nota.telefoneCliente").getTelefoneCliente());
			}
		} 
	}
	
	/**
	 * Verifica se o telefone persistido se encontra na lista do combo.
	 * Se estiver, apenas copia o telefone da propriedade persistente (telefoneCliente)
	 * para a propriedade transiente (telefoneClienteTransiente);
	 * Caso n�o esteja, cria um novo item de br.com.linkcom.sined.geral.bean.Telefone
	 * com o cdtelefone=-1 e com o valor igual ao valor do telefone persistente.
	 * 
	 * @see br.com.linkcom.sined.geral.service.TelefoneService#findForCombo(String...)
	 * @param form
	 * @author Hugo Ferreira
	 */
	protected void telefonePersistenteParaTransiente(FORMBEAN form) {
		List<Telefone> listaTelefone = telefoneService.carregarListaTelefone(form.getCliente());
		Telefone telefone = new Telefone();
		telefone.setCdtelefone(-1);
		telefone.setTelefone(form.getTelefoneCliente());
		
		if (listaTelefone.contains(telefone)) {
			int index = listaTelefone.indexOf(telefone);
			form.setTelefoneClienteTransiente(listaTelefone.get(index));
		} else {
			listaTelefone.add(telefone);
			form.setTelefoneClienteTransiente(telefone);
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, BEAN bean)throws Exception {
		throw new SinedException("N�o � poss�vel excluir uma Nota Fiscal.");
	}
}
