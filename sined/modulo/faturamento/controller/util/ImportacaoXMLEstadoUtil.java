package br.com.linkcom.sined.modulo.faturamento.controller.util;

import br.com.linkcom.lknfe.xml.nfe.importacao.estadual.NotaXML;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.PessoaService;

public class ImportacaoXMLEstadoUtil {

	public static Cliente buscarCliente(NotaXML notaxml) {
		Cliente cliente = null;
		Pessoa pessoa = null;
		//pesquisar pessoa 
	    if(notaxml.getCPF_dest() != null && !notaxml.getCPF_dest().equals("") ){
	    	Cpf cpf = new Cpf(notaxml.getCPF_dest());
	    	cliente = ClienteService.getInstance().findByCpf(cpf);
	    	if(cliente == null){
	    		pessoa = PessoaService.getInstance().findPessoaByCpf(cpf);
	    		
	    		cliente = new Cliente();
    			cliente.setCpf(cpf);
    			cliente.setNome(notaxml.getxNome());
    			cliente.setInscricaoestadual(notaxml.getIE_dest());
    			cliente.setEmail(notaxml.getEmail_dest());
    			cliente.setAtivo(true);
    			cliente.setTipopessoa(Tipopessoa.PESSOA_FISICA);
    			
	    		if(pessoa == null){
	    			ClienteService.getInstance().saveOrUpdate(cliente);
	    		} else if(pessoa != null){
	    			cliente.setCdpessoa(pessoa.getCdpessoa());
	    			ClienteService.getInstance().insertSomenteCliente(cliente);
	    		}
	    	}
	    } else if(notaxml.getCNPJ_dest() != null && !notaxml.getCNPJ_dest().equals("")){
	    	Cnpj cnpj = new Cnpj(notaxml.getCNPJ_dest());
	    	cliente = ClienteService.getInstance().findByCnpj(cnpj);
	    	if(cliente == null){
	    		pessoa = PessoaService.getInstance().findPessoaByCnpj(cnpj);
	    		
	    		cliente = new Cliente();
    			cliente.setCnpj(cnpj);
    			cliente.setNome(notaxml.getxNome());
    			cliente.setInscricaoestadual(notaxml.getIE_dest());
    			cliente.setEmail(notaxml.getEmail_dest());
    			cliente.setAtivo(true);
    			cliente.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
	    		
	    		if(pessoa == null){
	    			ClienteService.getInstance().saveOrUpdate(cliente);
	    		} else if(pessoa != null){
	    			cliente.setCdpessoa(pessoa.getCdpessoa());
	    			ClienteService.getInstance().insertSomenteCliente(cliente);
	    		}
	    	}
	    } else if(notaxml.getxNome() != null && !notaxml.getxNome().equals("")){ 
	    	cliente = ClienteService.getInstance().findByNome(notaxml.getxNome());
	    	if(cliente == null){
	    		pessoa = PessoaService.getInstance().findPessoaByNome(notaxml.getxNome());
	    		
	    		cliente = new Cliente();
    			cliente.setNome(notaxml.getxNome());
    			cliente.setInscricaoestadual(notaxml.getIE_dest());
    			cliente.setEmail(notaxml.getEmail_dest());
    			cliente.setAtivo(true);
    			cliente.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
	    		
	    		if(pessoa == null){
	    			ClienteService.getInstance().saveOrUpdate(cliente);
	    		} else if(pessoa != null){
	    			cliente.setCdpessoa(pessoa.getCdpessoa());
	    			ClienteService.getInstance().insertSomenteCliente(cliente);
	    		}
	    	}
	    }
	    
	    return cliente;
	}
}
