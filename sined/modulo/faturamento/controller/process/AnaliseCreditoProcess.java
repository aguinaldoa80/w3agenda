package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.jdom.Element;
import org.jdom.JDOMException;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AnaliseCreditoBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/faturamento/process/AnaliseCreditoProcess", authorizationModule=ProcessAuthorizationModule.class)
public class AnaliseCreditoProcess extends MultiActionController {
	
	private ClienteService clienteService;
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	
	@DefaultAction
	public void getAnaliseAtual(WebRequestContext request) {	
		try{
			String cdcliente = request.getParameter("cdcliente");
			String estrelas = null; 
			View view = View.getCurrent();
			if(cdcliente != null && cdcliente != ""){
				
				Cliente cliente = new Cliente(Integer.parseInt(cdcliente));
				cliente = clienteService.load(cliente, "cliente.cdpessoa, cliente.cnpj, cliente.cpf");
				
				AnaliseCreditoBean bean = new AnaliseCreditoBean();
				if(cliente.getCnpj() != null && cliente.getCnpj().getValue() != null){
					bean.setCnpj(cliente.getCnpj().getValue());
				}else if (cliente.getCpf() !=null && cliente.getCpf().getValue() !=null){
					bean.setCpf(cliente.getCpf().getValue());
				}
				
				AnaliseCreditoBean analiseCreditoBean = this.getCreditoInW3Controle(bean);
				estrelas = ""+analiseCreditoBean.getNotaCliente();
			}else{
				estrelas = "'<null>'";
			}
			view.println(estrelas);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private AnaliseCreditoBean getCreditoInW3Controle(AnaliseCreditoBean bean) throws NamingException, IOException, JDOMException {
		URL url = montaURLRequisicao();
		String consulta = null;
		if(bean.getCnpj() != null){
			consulta = "TIPO=CNPJ&VALOR="+bean.getCnpj();
		}else{
			consulta = "TIPO=CPF&VALOR="+bean.getCpf();
		}
		
		URLConnection conn = url.openConnection(); 
		conn.setDoOutput(true); 
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
		wr.write(consulta); 
		wr.flush(); 
		
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
		String line; 
		String xml = ""; 
		
		while ((line = rd.readLine()) != null) { xml += line; } 
		
		wr.close(); 
		rd.close(); 
		
		return criarBean(xml);
	}
	
	@SuppressWarnings("unchecked")
	private AnaliseCreditoBean criarBean(String xml) throws JDOMException, IOException {
		Element rootElement = SinedUtil.getRootElementXML(Util.strings.tiraAcento(xml).getBytes());
		if(rootElement != null && rootElement.getContent().size() > 0){
			AnaliseCreditoBean bean = new AnaliseCreditoBean();
			
			Element notaElement = SinedUtil.getChildElement("NotaCliente", rootElement.getContent());
			Element clienteCPFElement = SinedUtil.getChildElement("ClienteCPF", rootElement.getContent());
			Element clienteCNPJElement = SinedUtil.getChildElement("ClienteCNPJ", rootElement.getContent());
			
			bean.setNotaCliente(SinedUtil.getElementInteger(notaElement));
			bean.setCpf(SinedUtil.getElementString(clienteCPFElement));
			bean.setCnpj(SinedUtil.getElementString(clienteCNPJElement));
			
			return bean;
		}	
		return null;
	}
		
	/**
	 * M�todo que monta a URL para consulta ao W3Controle para Analise de Credito do cliente
	 *
	 * @return
	 * @throws MalformedURLException
	 * @throws NamingException
	 * @author yago.silvestre
	 * @since 14/03/2019
	 */
	private URL montaURLRequisicao() throws MalformedURLException, NamingException{
		String urlString = "linkcom.w3erp.com.br";
	
		if(SinedUtil.isAmbienteDesenvolvimento()){
			urlString = InitialContext.doLookup("URL_CONSULTA_ANALISE_CREDITO_W3");
			if(urlString == null || urlString.trim().equals("")){
				throw new SinedException("A URL do desenvolvimento n�o foi encontrada para verifica��o de contig�ncia.");
			}
		}	
		return new URL("http://" + urlString + "/w3controle/pub/process/ClienteAnaliseCredito");
	}
	
}
