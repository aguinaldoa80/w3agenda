package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Codigotributacao;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratohistorico;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Itemlistaservico;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaContrato;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Contratoacao;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.CodigotributacaoService;
import br.com.linkcom.sined.geral.service.ContratohistoricoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.ItemlistaservicoService;
import br.com.linkcom.sined.geral.service.NotaContratoService;
import br.com.linkcom.sined.geral.service.NotaDocumentoService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.filter.NotaFiscalEletronicaSubstituicaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/faturamento/process/NotaFiscalEletronicaSubstituicao", authorizationModule=ProcessAuthorizationModule.class)
public class NotaFiscalEletronicaSubstituicaoProcess extends MultiActionController {
	
	private static final DateFormat FORMAT_DATETIME = new SimpleDateFormat("yyyy-MM-dd//hh:mm:ss");
	public static final String FILTRO_SUBTITUICAO_NFSE = "FILTRO_SUBTITUICAO_NFSE";
	
	private NotaFiscalServicoService notaFiscalServicoService;
	private DocumentoService documentoService;
	private NotaService notaService;
	private NotaContratoService notaContratoService;
	private ContratohistoricoService contratohistoricoService;
	private ArquivonfnotaService arquivonfnotaService;
	private CodigotributacaoService codigotributacaoService;
	private ItemlistaservicoService itemlistaservicoService;
	private NotaDocumentoService notaDocumentoService;
	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setNotaDocumentoService(
			NotaDocumentoService notaDocumentoService) {
		this.notaDocumentoService = notaDocumentoService;
	}
	public void setItemlistaservicoService(
			ItemlistaservicoService itemlistaservicoService) {
		this.itemlistaservicoService = itemlistaservicoService;
	}
	public void setCodigotributacaoService(
			CodigotributacaoService codigotributacaoService) {
		this.codigotributacaoService = codigotributacaoService;
	}
	public void setArquivonfnotaService(
			ArquivonfnotaService arquivonfnotaService) {
		this.arquivonfnotaService = arquivonfnotaService;
	}
	public void setContratohistoricoService(
			ContratohistoricoService contratohistoricoService) {
		this.contratohistoricoService = contratohistoricoService;
	}
	public void setNotaContratoService(NotaContratoService notaContratoService) {
		this.notaContratoService = notaContratoService;
	}
	public void setNotaService(NotaService notaService) {
		this.notaService = notaService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setNotaFiscalServicoService(
			NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	
	@SuppressWarnings("unchecked")
	@DefaultAction
	public ModelAndView index(WebRequestContext request, NotaFiscalEletronicaSubstituicaoFiltro filtro){
		Object obj = request.getSession().getAttribute("IDS_CONTAS_NAO_PERMITIDAS");
		if(obj != null){
			List<Integer> listaCds = (List<Integer>)obj;
			request.getSession().removeAttribute("IDS_CONTAS_NAO_PERMITIDAS");
			if(listaCds.size() > 0){
				StringBuilder sb = new StringBuilder();
				for (Integer integer : listaCds) {
					sb.append(integer).append(",");
				}
				sb.delete(sb.length()-1, sb.length());
				List<Documento> listaDocumento = documentoService.carregaDocumentos(sb.toString());
				for (Documento doc : listaDocumento) {
					documentoService.ajustaNumeroParcelaDocumento(doc);
				}
				filtro.setListaDocumento(listaDocumento);
			}
		}
		
		
		return new ModelAndView("/process/substituicaoNfse", "filtro", filtro);
	}
	
	@SuppressWarnings("unchecked")
	@Input("index")
	public ModelAndView processarArquivo(WebRequestContext request, NotaFiscalEletronicaSubstituicaoFiltro filtro) throws JDOMException, IOException{
		if(filtro.getArquivo() == null || filtro.getArquivo().getContent().length == 0){
			throw new SinedException("Arquivo n�o encontrado.");
		}
		
		SAXBuilder sb = new SAXBuilder();  
		Document d = sb.build(new ByteArrayInputStream(filtro.getArquivo().getContent()));
		Element compNfse = d.getRootElement();
		
		if(compNfse.getName().equals("ConsultarLoteRpsResposta") || compNfse.getName().equals("ConsultarNfseResposta")){
			Element listaNfse = SinedUtil.getChildElement("ListaNfse", compNfse.getContent());
			if(listaNfse != null){
				Element compNfse2 = SinedUtil.getChildElement("CompNfse", listaNfse.getContent());
				if(compNfse2 != null){
					compNfse = compNfse2;
				}
			}
		}
		
		Element nfse = SinedUtil.getChildElement("Nfse", compNfse.getContent());
		Element infNfse = SinedUtil.getChildElement("InfNfse", nfse.getContent());
		if(infNfse == null)
			infNfse = nfse;
		
		Element prestadorServico = SinedUtil.getChildElement("PrestadorServico", infNfse.getContent());
		Element indentificacaoPrestador = SinedUtil.getChildElement("IdentificacaoPrestador", prestadorServico.getContent());
		Element cnpj = SinedUtil.getChildElement("Cnpj", indentificacaoPrestador.getContent());
		if(cnpj == null){
			Element cpfcnpj = SinedUtil.getChildElement("CpfCnpj", indentificacaoPrestador.getContent());
			if(cpfcnpj != null){
				cnpj = SinedUtil.getChildElement("Cnpj", cpfcnpj.getContent());
			}
		}
		
		Cnpj cnpjType = new Cnpj(cnpj.getText());
		filtro.setCnpj_substituido_busca(cnpjType);
		
		Element nfseSubstituida = SinedUtil.getChildElement("NfseSubstituida", infNfse.getContent());
		
		if(nfseSubstituida != null){
			String numNfse = nfseSubstituida.getText();
			if(numNfse == null || "".equals(numNfse)){
				nfseSubstituida = SinedUtil.getChildElement("NfseSubstituida", infNfse.getContent());
				if(nfseSubstituida != null){
					Element numeroNfseElem = SinedUtil.getChildElement("Numero", nfseSubstituida.getContent()); 
					numNfse = numeroNfseElem.getText();
				}
			}
			
			NotaFiscalServico nfs = notaFiscalServicoService.findByNumNfseCNPJProcessoNovo(numNfse, cnpjType);
			if(nfs == null){
				throw new SinedException("Nota com n�mero de NFS-e " + numNfse + " n�o encontrada.");
			}
			
			List<NotaDocumento> listaNotaDocumento = notaDocumentoService.findByNotasForValidacao(nfs.getCdNota() + "");
			if(listaNotaDocumento != null && listaNotaDocumento.size() > 0){
				List<Integer> listaCds = new ArrayList<Integer>();
				
				for (NotaDocumento notaDocumento : listaNotaDocumento) {
					if(notaDocumento.getFromwebservice() == null || !notaDocumento.getFromwebservice()){
						Documento doc = notaDocumento.getDocumento();
						if(!doc.getDocumentoacao().equals(Documentoacao.CANCELADA)){
							listaCds.add(doc.getCddocumento());
						}
					}
				}
				
				if(listaCds.size() > 0){
					request.getSession().setAttribute("IDS_CONTAS_NAO_PERMITIDAS", listaCds);
					return sendRedirectToAction("index");
				}
			}
			
			nfs = notaFiscalServicoService.loadForEntrada(nfs);
			
			filtro.setCdNota_substituido(nfs.getCdNota());
			filtro.setCliente_substituido(nfs.getCliente());
			String numeroNota = nfs.getNumero();
			if(StringUtils.isBlank(numeroNota)){
				List<Arquivonfnota> listaArquivonfnota = arquivonfnotaService.findByNota(nfs.getCdNota().toString());
				if(listaArquivonfnota != null){
					for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
						if(arquivonfnota.getDtcancelamento() == null && 
								arquivonfnota.getArquivonf() != null && 
								arquivonfnota.getArquivonf().getArquivonfsituacao() != null &&
								arquivonfnota.getArquivonf().getArquivonfsituacao().equals(Arquivonfsituacao.PROCESSADO_COM_SUCESSO) &&
								arquivonfnota.getNumeronfse() != null &&
								arquivonfnota.getCodigoverificacao() != null){
							numeroNota = arquivonfnota.getNumeronfse();
							break;
						}
					}
				}
			}
			filtro.setNumero_substituido(numeroNota);
			filtro.setValor_substituido(nfs.getValorNota());
			filtro.setEmpresa_substituido(nfs.getEmpresa());
		}
		
		
		request.setAttribute("msgAlert", "A nota fiscal que ser� substitu�da ir� ser cancelada no sistema.");
		
		
		this.preencheInformacoesNotaNova(filtro, infNfse);
		request.getSession().setAttribute(FILTRO_SUBTITUICAO_NFSE, filtro);
		
		return new ModelAndView("/process/substituicaoNfse_dadosSubstituicao", "filtro", filtro);
	}
	
	@SuppressWarnings("unchecked")
	private void preencheInformacoesNotaNova(NotaFiscalEletronicaSubstituicaoFiltro filtro, Element infNfse) {
		Element servico = SinedUtil.getChildElement("Servico", infNfse.getContent());
		if(servico == null){
			servico = SinedUtil.getChildElement("DeclaracaoPrestacaoServico", infNfse.getContent());
		}
		
		Element valores = SinedUtil.getChildElement("Valores", servico.getContent());
		Element tomadorServico = SinedUtil.getChildElement("TomadorServico", infNfse.getContent());
		if(tomadorServico == null){
			tomadorServico = SinedUtil.getChildElement("Tomador", servico.getContent());
		}
		
		Element identificacaoTomador = SinedUtil.getChildElement("IdentificacaoTomador", tomadorServico.getContent());
		Element cpfCnpj = SinedUtil.getChildElement("CpfCnpj", identificacaoTomador.getContent());
		
		Element identificacaoNfse = SinedUtil.getChildElement("IdentificacaoNfse", infNfse.getContent());
		
		Element numero = SinedUtil.getChildElement("Numero", infNfse.getContent());
		if(numero == null && identificacaoNfse != null){
			numero = SinedUtil.getChildElement("Numero", identificacaoNfse.getContent());
		}
		if(numero != null){
			filtro.setNumeronfse(numero.getText());
		}
		
		Element codigoverificacao = SinedUtil.getChildElement("CodigoVerificacao", infNfse.getContent());
		if(codigoverificacao == null && identificacaoNfse != null){
			codigoverificacao = SinedUtil.getChildElement("CodigoVerificacao", identificacaoNfse.getContent());
		}
		if(codigoverificacao != null){
			filtro.setCodigoverificacao(codigoverificacao.getText());
		}
		
		try{
			Element dataemissao = SinedUtil.getChildElement("DataEmissao", infNfse.getContent());
			String dtEmissaoStr = dataemissao.getText().replaceAll("T", "//");
			filtro.setDtemissao(new Timestamp(FORMAT_DATETIME.parse(dtEmissaoStr).getTime()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try{
			Element competencia = SinedUtil.getChildElement("Competencia", infNfse.getContent());
			String competenciaStr = competencia.getText().replaceAll("T", "//");
			filtro.setCompetencia(new Date(FORMAT_DATETIME.parse(competenciaStr).getTime()));
		} catch (Exception e) {
			try {
				Element competencia = SinedUtil.getChildElement("Competencia", infNfse.getContent());
				if(competencia == null){
					competencia = SinedUtil.getChildElement("Competencia", servico.getContent());
				}
				String competenciaStr = competencia.getText().replaceAll("T", "//");
				filtro.setCompetencia(new Date(new SimpleDateFormat("yyyy-MM-dd").parse(competenciaStr).getTime()));
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		
		Element valorServicos = SinedUtil.getChildElement("ValorServicos", valores.getContent());
		if(valorServicos != null){
			try{
				filtro.setValorservicos(new Money(valorServicos.getText()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		Element valorDeducoes = SinedUtil.getChildElement("ValorDeducoes", valores.getContent());
		if(valorDeducoes != null){
			try{
				filtro.setValordeducoes(new Money(valorDeducoes.getText()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		Element valorPis = SinedUtil.getChildElement("ValorPis", valores.getContent());
		if(valorPis != null){
			try{
				filtro.setValorpis(new Money(valorPis.getText()));
				if(filtro.getValorpis() != null && filtro.getValorpis().getValue().doubleValue() > 0){
					filtro.setPisretido(true);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		Element valorCofins = SinedUtil.getChildElement("ValorCofins", valores.getContent());
		if(valorCofins != null){
			try{
				filtro.setValorcofins(new Money(valorCofins.getText()));
				if(filtro.getValorcofins() != null && filtro.getValorcofins().getValue().doubleValue() > 0){
					filtro.setCofinsretido(true);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		Element valorInss = SinedUtil.getChildElement("ValorInss", valores.getContent());
		if(valorInss != null){
			try{
				filtro.setValorinss(new Money(valorInss.getText()));
				if(filtro.getValorinss() != null && filtro.getValorinss().getValue().doubleValue() > 0){
					filtro.setInssretido(true);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		Element valorIr = SinedUtil.getChildElement("ValorIr", valores.getContent());
		if(valorIr != null){
			try{
				filtro.setValorir(new Money(valorIr.getText()));
				if(filtro.getValorir() != null && filtro.getValorir().getValue().doubleValue() > 0){
					filtro.setIrretido(true);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		Element valorCsll = SinedUtil.getChildElement("ValorCsll", valores.getContent());
		if(valorCsll != null){
			try{
				filtro.setValorcsll(new Money(valorCsll.getText()));
				if(filtro.getValorcsll() != null && filtro.getValorcsll().getValue().doubleValue() > 0){
					filtro.setCsllretido(true);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		Element issRetido = SinedUtil.getChildElement("IssRetido", valores.getContent());
		if(issRetido != null){
			String issRetidoStr = issRetido.getText();
			if(issRetidoStr != null && issRetidoStr.equals("1")){
				filtro.setIssretido(Boolean.TRUE);
			} else {
				filtro.setIssretido(Boolean.FALSE);
			}
		}
		
		Element valorIss = SinedUtil.getChildElement("ValorIss", valores.getContent());
		if(valorIss != null){
			try{
				filtro.setValoriss(new Money(valorIss.getText()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		Element outrasRetencoes = SinedUtil.getChildElement("OutrasRetencoes", valores.getContent());
		if(outrasRetencoes != null){
			try{
				filtro.setOutrasretencoes(new Money(outrasRetencoes.getText()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		Element baseCalculo = SinedUtil.getChildElement("BaseCalculo", valores.getContent());
		if(baseCalculo != null){
			try{
				filtro.setBasecalculo(new Money(baseCalculo.getText()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		Element aliquota = SinedUtil.getChildElement("Aliquota", valores.getContent());
		if(aliquota != null){
			try{
				filtro.setAliquota(new Money(aliquota.getText()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		Element descontoIncondicionado = SinedUtil.getChildElement("DescontoIncondicionado", valores.getContent());
		if(descontoIncondicionado != null){
			try{
				filtro.setDescontoincondicionado(new Money(descontoIncondicionado.getText()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		Element descontoCondicionado = SinedUtil.getChildElement("DescontoCondicionado", valores.getContent());
		if(descontoCondicionado != null){
			try{
				filtro.setDescontocondicionado(new Money(descontoCondicionado.getText()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		Element itemListaServico = SinedUtil.getChildElement("ItemListaServico", servico.getContent());
		if(itemListaServico != null){
			Itemlistaservico itemlistaservico = itemlistaservicoService.loadByCodigo(itemListaServico.getText());
			filtro.setItemlistaservico(itemlistaservico);
		}
		
		Element codigoTributacaoMunicipio = SinedUtil.getChildElement("CodigoTributacaoMunicipio", servico.getContent());
		if(codigoTributacaoMunicipio != null){
			Codigotributacao codigotributacao = codigotributacaoService.loadByCodigo(codigoTributacaoMunicipio.getText());
			filtro.setCodigotributacao(codigotributacao);
		}
		
		Element discriminacao = SinedUtil.getChildElement("Discriminacao", servico.getContent());
		if(discriminacao != null){
			filtro.setDiscriminacao(discriminacao.getText());
		}
		
		Element cpf = SinedUtil.getChildElement("Cpf", cpfCnpj.getContent());
		if(cpf != null){
			try{
				filtro.setCpf(new Cpf(cpf.getText()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		Element cnpj = SinedUtil.getChildElement("Cnpj", cpfCnpj.getContent());
		if(cnpj != null){
			try{
				filtro.setCnpj(new Cnpj(cnpj.getText()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public ModelAndView confimarSubstituicao(WebRequestContext request, NotaFiscalEletronicaSubstituicaoFiltro filtro){
		Nota substituidora = notaService.carregarItensParaAcao(filtro.getNotaSubstituidora());
		String numeroSubstituidora = substituidora.getNumero();
		if(numeroSubstituidora == null || numeroSubstituidora.equals("")){
			numeroSubstituidora = "sem n�mero";
		}
		substituidora.setNotaStatus(NotaStatus.NFSE_EMITIDA);
		
		Nota substituida = notaService.carregarItensParaAcao(new Nota(filtro.getCdNota_substituido()));
		String numeroSubstituida = substituida.getNumero();
		if(numeroSubstituida == null || numeroSubstituida.equals("")){
			numeroSubstituida = "sem n�mero";
		}
		substituida.setNotaStatus(NotaStatus.CANCELADA);
		
		StringBuilder sb = new StringBuilder();
		
		List<NotaContrato> listaNotaContrato = notaContratoService.findByNota(substituida);//pegar faturamento
		if(filtro.getCriarNova() && listaNotaContrato != null && listaNotaContrato.size() > 0){
			NotaContrato notaContrato;
			Set<Contrato> contratos = new LinkedHashSet<Contrato>();
			for (NotaContrato nc : listaNotaContrato) {
				nc.getContrato().setFaturamentocontrato(nc.getFaturamentocontrato());
				contratos.add(nc.getContrato());
			}
			
			Contratohistorico ch = null;
			for (Contrato contrato : contratos) {
				notaContrato = new NotaContrato();
				notaContrato.setContrato(contrato);
				notaContrato.setNota(substituidora);
				notaContrato.setFaturamentocontrato(contrato.getFaturamentocontrato());
				notaContratoService.saveOrUpdate(notaContrato);
				
				try{
					sb
					.append("<a href=\"javascript:visualizarContrato(")
					.append(contrato.getCdcontrato())
					.append(")\">")
					.append(contrato.getCdcontrato())
					.append("</a>,");
				}catch (Exception e) {
					e.printStackTrace();
				}
				
				
				ch = new Contratohistorico();
				ch.setAcao(Contratoacao.ALTERADO);
				ch.setContrato(contrato);
				ch.setDthistorico(new Timestamp(System.currentTimeMillis()));
				ch.setObservacao("Nota <a href=\"javascript:visualizarNota("+substituida.getCdNota()+")\">"+numeroSubstituida+"</a> substitu�da pela nota <a href=\"javascript:visualizarNota("+substituidora.getCdNota()+")\">"+numeroSubstituidora+"</a>.");
				ch.setUsuario(SinedUtil.getUsuarioLogado());
				contratohistoricoService.saveOrUpdate(ch);
			}
			
			if(sb != null && sb.length() > 0) 
				sb.delete(sb.length()-1, sb.length());
		}
		
		try{
			notaService.alterarStatusAcao(substituida, "Nota substitu�da pela nota <a href=\"javascript:visualizarNota("+substituidora.getCdNota()+")\">"+numeroSubstituidora+"</a>.", null);
			
			notaDocumentoService.adicionaMensagensCancelamentoNotaWebserviceTrue(request, substituida.getCdNota() + "", false);
			
			String hist = "Nota criada pela substui��o da nota <a href=\"javascript:visualizarNota("+substituida.getCdNota()+")\">"+numeroSubstituida+"</a>.";
			if(sb != null && sb.length() > 0) hist += " Origem do(s) contrato(s) " + sb.toString() + ".";
			notaService.alterarStatusAcao(substituidora, hist, null);
			
			Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaNotCancelada(new NotaFiscalServico(substituida));
			
			Arquivonfnota arquivonfnota_nova = new Arquivonfnota();
			arquivonfnota_nova.setArquivonf(arquivonfnota.getArquivonf());
			arquivonfnota_nova.setCodigoverificacao(filtro.getCodigoverificacao());
			arquivonfnota_nova.setDtcompetencia(new Timestamp(filtro.getCompetencia().getTime()));
			arquivonfnota_nova.setDtemissao(filtro.getDtemissao());
			arquivonfnota_nova.setNota(substituidora);
			arquivonfnota_nova.setNumeronfse(filtro.getNumeronfse());
			arquivonfnotaService.saveOrUpdate(arquivonfnota_nova);
			
			NotaFiscalEletronicaSubstituicaoFiltro filtroArquivo = (NotaFiscalEletronicaSubstituicaoFiltro) request.getSession().getAttribute(FILTRO_SUBTITUICAO_NFSE);
			if(filtroArquivo != null && filtroArquivo.getArquivo() != null){
				arquivonfnota_nova.setArquivoxml(filtroArquivo.getArquivo());
				arquivoDAO.saveFile(arquivonfnota_nova, "arquivoxml");
				arquivonfnotaService.updateArquivoxml(arquivonfnota_nova);
			}
			
			arquivonfnotaService.updateCancelado(arquivonfnota);
			
			request.addMessage("NFS-e substiu�da com sucesso.");
		} catch (Exception e) {
			request.addError(e.getMessage());
			e.printStackTrace();
		}
		
		request.getSession().removeAttribute(FILTRO_SUBTITUICAO_NFSE);
		return sendRedirectToAction("index");
	}
	
	public ModelAndView ajaxBuscaNotaSubstituida(WebRequestContext request, NotaFiscalEletronicaSubstituicaoFiltro filtro){
		JsonModelAndView json = new JsonModelAndView();
		
		try {
			if(filtro.getNumero_substituido_busca() == null || filtro.getCnpj_substituido_busca() == null){
				throw new SinedException("N�mero da nota e CNPJ da empresa prestadora obrigat�rio.");
			}
			
			List<NotaFiscalServico> listaNota = notaFiscalServicoService.findByNumCNPJ(filtro.getNumero_substituido_busca(), filtro.getCnpj_substituido_busca());
			if(listaNota == null || listaNota.size() == 0){
				throw new SinedException("Nota n�o encontrada");
			}
			if(listaNota.size() > 1){
				throw new SinedException("Mais de uma nota encontrada");
			}
			
			NotaFiscalServico nfs = listaNota.get(0);
			nfs = notaFiscalServicoService.loadForEntrada(nfs);
			
			json.addObject("empresa_substituido_value", "br.com.linkcom.sined.geral.bean.Empresa[cdpessoa=" + nfs.getEmpresa().getCdpessoa() + "]");
			json.addObject("empresa_substituido", nfs.getEmpresa().getNome());
			json.addObject("numero_substituido", nfs.getNumero());
			json.addObject("cliente_substituido", nfs.getCliente().getNome());
			json.addObject("valor_substituido", nfs.getValorNota());
			json.addObject("cdNota_substituido", nfs.getCdNota());
			json.addObject("sucesso", true);
		} catch (Exception e) {
			json.addObject("sucesso", false);
			json.addObject("msgErro", e.getMessage());
		}
		
		return json;
	}
	
}