package br.com.linkcom.sined.modulo.faturamento.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.service.InutilizacaonfeService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.InutilizacaonfeFiltro;

@Controller(path="/faturamento/process/InutilizacaonfeCSV", authorizationModule=ProcessAuthorizationModule.class)
public class InutilizacaonfeCSVProcess extends ResourceSenderController<InutilizacaonfeFiltro> {

	private InutilizacaonfeService inutilizacaonfeService;
	
	public void setInutilizacaonfeService(InutilizacaonfeService inutilizacaonfeService) {
		this.inutilizacaonfeService = inutilizacaonfeService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	InutilizacaonfeFiltro filtro) throws Exception {
		boolean isNfe = "true".equals(request.getParameter("isNfe"));
		
		if(isNfe){
			filtro.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFE);
		} else {
			filtro.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFCE);
		}
		
		return inutilizacaonfeService.geraArquivoCSVListagem(filtro, isNfe);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, InutilizacaonfeFiltro filtro) throws Exception {
		boolean isNfe = "true".equals(request.getParameter("isNfe"));
		
		if (isNfe) {
			return new ModelAndView("redirect:/faturamento/crud/Inutilizacaonfe").addObject("filtro", filtro);
		} else {
			return new ModelAndView("redirect:/faturamento/crud/Inutilizacaonfce").addObject("filtro", filtro);
		}
	}
}
