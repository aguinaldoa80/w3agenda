package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.lknfe.xml.nfe.importacao.estadual.ImportarXML;
import br.com.linkcom.lknfe.xml.nfe.importacao.estadual.NotaXML;
import br.com.linkcom.lknfe.xml.nfe.importacao.estadual.NotaXMLDuplicata;
import br.com.linkcom.lknfe.xml.nfe.importacao.estadual.NotaXMLItem;
import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivonf;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Ncmcapitulo;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoduplicata;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.ResponsavelFrete;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Finalidadenfe;
import br.com.linkcom.sined.geral.bean.enumeration.Formapagamentonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicms;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicmsst;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Motivodesoneracaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Presencacompradornfe;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoiss;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ArquivonfService;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.CfopService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ConfiguracaonfeService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.ItemlistaservicoService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaoService;
import br.com.linkcom.sined.geral.service.NcmcapituloService;
import br.com.linkcom.sined.geral.service.NotaDocumentoService;
import br.com.linkcom.sined.geral.service.NotaHistoricoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.UfService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportarXMLEstadoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportarXMLEstadoConferenciaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportarXMLEstadoItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.filter.ImportarXMLEstadoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.ZipManipulation;

@Controller(path="/faturamento/process/ImportarXMLEstado", authorizationModule=ProcessAuthorizationModule.class)
public class ImportarXMLEstadoProcess extends MultiActionController{
	
	private MunicipioService municipioService;
	private PessoaService pessoaService;
	private ClienteService clienteService;
	private EnderecoService enderecoService;
	private EmpresaService empresaService;
	private NotaHistoricoService notaHistoricoService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private ArquivoService arquivoService;
	private ItemlistaservicoService itemlistaservicoService;
	private FornecedorService fornecedorService;
	private UfService ufService;
	private CfopService cfopService;
	private ArquivoDAO arquivoDAO;
	private ArquivonfService arquivonfService;
	private ArquivonfnotaService arquivonfnotaService;
	private NcmcapituloService ncmcapituloService;
	private ContareceberService contareceberService;
	private NotaDocumentoService notaDocumentoService;
	private DocumentotipoService documentotipoService;
	private RateioService rateioService;
	private ConfiguracaonfeService configuracaonfeService;
	private UnidademedidaService unidademedidaService;
	private NaturezaoperacaoService naturezaoperacaoService;
	private MaterialService materialService;
	private ParametrogeralService parametrogeralService;
	
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {this.unidademedidaService = unidademedidaService;}
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {this.configuracaonfeService = configuracaonfeService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}
	public void setContareceberService(ContareceberService contareceberService) {this.contareceberService = contareceberService;}
	public void setNcmcapituloService(NcmcapituloService ncmcapituloService) {this.ncmcapituloService = ncmcapituloService;}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {this.arquivonfnotaService = arquivonfnotaService;}
	public void setArquivonfService(ArquivonfService arquivonfService) {this.arquivonfService = arquivonfService;}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	public void setCfopService(CfopService cfopService) {this.cfopService = cfopService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setUfService(UfService ufService) {this.ufService = ufService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setItemlistaservicoService(ItemlistaservicoService itemlistaservicoService) {this.itemlistaservicoService = itemlistaservicoService;}
	public void setNotaHistoricoService(NotaHistoricoService notaHistoricoService) {this.notaHistoricoService = notaHistoricoService;}
	public void setMunicipioService(MunicipioService municipioService) {this.municipioService = municipioService;}
	public void setPessoaService(PessoaService pessoaService) {this.pessoaService = pessoaService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setNaturezaoperacaoService(NaturezaoperacaoService naturezaoperacaoService) {this.naturezaoperacaoService = naturezaoperacaoService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, ImportarXMLEstadoFiltro filtro){
		List<Configuracaonfe> listaConfiguracao = configuracaonfeService.findAtivosByTipoEmpresa(Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO, null);
		request.setAttribute("listaConfiguracao", listaConfiguracao);
		
		return new ModelAndView("/process/importarXMLEstado", "filtro", filtro);
	}
	
	public ModelAndView verificarImportacaoXml(WebRequestContext request, ImportarXMLEstadoFiltro filtro){
		try{
			Empresa empresaFiltro = filtro.getConfiguracaonfe() != null && filtro.getConfiguracaonfe().getEmpresa() != null ? filtro.getConfiguracaonfe().getEmpresa() : null; 
			request.getSession().setAttribute("IMPORTARXML_ESTADO_CONFIGURACAONFE", filtro.getConfiguracaonfe());
			request.getSession().setAttribute("IMPORTARXML_ESTADO_ARQUIVO", filtro.getArquivo());
			
			if(filtro.getArquivo().getName().toLowerCase().endsWith(".zip")){
				Arquivo arquivoZip = filtro.getArquivo();
				ZipManipulation zip = new ZipManipulation(arquivoZip);
				
				try {
					zip.readEntries();
				} catch (IllegalArgumentException e) {
					request.addError("N�o foi poss�vel processar o arquivo. Favor verificar se existe caracteres especiais ou acentos no nome dos arquivos dentro do zip.");
					request.addError(e.getMessage());
					e.printStackTrace();
					return sendRedirectToAction("index");
				}
				
				if(zip.getEntriesNames() == null){
					request.addError("Nehum xml encontrado.");
					return sendRedirectToAction("index");
				}else if(zip.getEntriesNames().size() == 0){
					request.addError("N�o foi poss�vel localizar nenhum documento dentro arquivo.");
					return sendRedirectToAction("index");
				}else {
					if(zip.getEntriesNames().size() == 1){
						return associarMaterial(request, filtro);
					}else {
						ImportarXMLEstadoConferenciaBean bean = new ImportarXMLEstadoConferenciaBean();
						bean.setIdentificadortela(new SimpleDateFormat("yyyyMMddHHmmssSSSS").format(System.currentTimeMillis()));
						bean.setListaNotas(new ArrayList<ImportarXMLEstadoBean>());
						
						for(String filename : zip.getEntriesNames()){
							Arquivo arquivoXML = new Arquivo(zip.getBytesFromEntry(filename), filename, "zip");
							try {
								ImportarXMLEstadoBean beanNota = processarXml(new String(arquivoXML.getContent(), "UTF-8"), filtro);
								beanNota.setArquivoxml(arquivoXML);
								validaInformacaoNota(beanNota);
								
								
								if(beanNota.getEmpresa() != null && empresaFiltro != null && !empresaFiltro.equals(beanNota.getEmpresa())){
									throw new SinedException("A empresa do xml � diferente da empresa do filtro. (Arquivo: " + filename + ")");
								}
								bean.getListaNotas().add(beanNota);
							}catch (SinedException e) {
								request.addError(e.getMessage());
								e.printStackTrace();
							}catch (Exception e) {
								e.printStackTrace();
								request.addError("N�o foi poss�vel processar o xml da nota: " + filename);
							}
						}
						
						if(SinedUtil.isListEmpty(bean.getListaNotas())){
							return sendRedirectToAction("index");
						}
						request.getSession().setAttribute("LISTA_IMPORTARXML_ESTADO_NOTA_" + bean.getIdentificadortela(), bean.getListaNotas());
						return new ModelAndView("/process/importarXMLEstadoConferenciaNota", "bean", bean);
					}
				}
			}else if(filtro.getArquivo().getName().toLowerCase().endsWith(".xml")){
				return associarMaterial(request, filtro);
			}else{
				throw new SinedException("Somente arquivos com a extens�o .zip ou .xml s�o aceitos.");
			}
		} catch (Exception e) {
			request.addError("Problemas na importa��o de notas.");
			request.addError(e.getMessage());
			e.printStackTrace();
			return sendRedirectToAction("index");
		}
	}
	
	private ImportarXMLEstadoBean processarXml(String stringXml, ImportarXMLEstadoFiltro filtro) throws Exception {
		NotaXML notaxml = ImportarXML.preencheBean(stringXml);
		
		if(notaxml.getIndPag() == null){
			notaxml.setIndPag(filtro.getFormapagamentonfe().getValue());	
		}
		
		Documentotipo documentotipo = documentotipoService.findNotafiscal();
		if(documentotipo == null){
			throw new SinedException("Tipo de documento 'Nota Fiscal' n�o encontrado.");
		}
		
		Cnpj cnpj_emit = new Cnpj(notaxml.getCNPJ_emit());
		Empresa empresa = empresaService.findByCnpj(cnpj_emit);
		if(empresa == null){
			throw new SinedException("Empresa n�o encontrada. - " + notaxml.getCNPJ_emit());
		}
		
		if(notaxml.getnNF() != null && notafiscalprodutoService.isExisteNota(empresa, notaxml.getnNF(), notaxml.getSerie(), ModeloDocumentoFiscalEnum.NFE, null)){
			throw new SinedException("Nota j� cadastrada. - " + notaxml.getnNF());
		}
		
		List<ImportarXMLEstadoItemBean> listaItens = new ArrayList<ImportarXMLEstadoItemBean>();
		if(notaxml.getListadet() != null){
			ImportarXMLEstadoItemBean item;
			for (NotaXMLItem it: notaxml.getListadet()) {
				item = new ImportarXMLEstadoItemBean();
				item.setItem(it);
				
				String codigo = it.getcProd();
				try{
					Material material = materialService.findByIdentificacao(codigo);
					item.setMaterial(material);
					item.setNomeMaterial(material.getAutocompleteDescription());
				} catch (Exception e) {
				}
				
				listaItens.add(item);
			}
		}
		
		ImportarXMLEstadoBean bean = new ImportarXMLEstadoBean();
		bean.setNotaXML(notaxml);
		bean.setListaItens(listaItens);
		bean.setEmpresa(empresa);
		bean.setCliente(buscarCliente(notaxml));
		if(notaxml.getTpNF() != null) bean.setTipooperacaonota(Tipooperacaonota.values()[notaxml.getTpNF()]);
		if(notaxml.getvNF() != null) bean.setValornota(new Money(notaxml.getvNF()));
		
		// ADICIONA UMA LINHA NO RATEIO
		Rateio rateio = new Rateio();
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		listaRateioitem.add(new Rateioitem());
		rateio.setListaRateioitem(listaRateioitem);
		bean.setRateio(rateio);
		
		
		
		String whereinCfops = this.montaWhereinCfopByNotaxml(notaxml);
		List<Naturezaoperacao> listaNaturezaoperacao = naturezaoperacaoService.findByCfops(whereinCfops);
		if(listaNaturezaoperacao != null && listaNaturezaoperacao.size() == 1){
			bean.setNaturezaoperacao(listaNaturezaoperacao.get(0));
		}
		
		return bean;
	}
	
	/**
	 * Action que abre na tela para o usu�rio a lista dos materiais da NF para serem associados.
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @since 14/06/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView associarMaterial(WebRequestContext request, ImportarXMLEstadoFiltro filtro){
		try{
			String stringXml = null;
			if(filtro.getArquivo().getName().toLowerCase().endsWith(".zip")){
				Arquivo arquivoZip = filtro.getArquivo();
				ZipManipulation zip = new ZipManipulation(arquivoZip);
				zip.readEntries();
				
				if(zip.getEntriesNames() == null){
					request.addError("Nehum xml encontrado.");
				}else {
					if(zip.getEntriesNames().size() == 1){
						String filename = zip.getEntriesNames().get(0);
						Arquivo arquivoXML = new Arquivo(zip.getBytesFromEntry(filename), filename, "zip");
						stringXml = new String(arquivoXML.getContent(), "UTF-8");
					}
				}
			}else {
				stringXml = new String(filtro.getArquivo().getContent(), "UTF-8");
			}
			
			ImportarXMLEstadoBean bean = processarXml(stringXml,filtro);
			NotaXML notaxml = bean.getNotaXML();
			
			if(filtro.getConfiguracaonfe() != null && filtro.getConfiguracaonfe().getEmpresa() != null && bean != null && bean.getEmpresa() != null && 
					!bean.getEmpresa().equals(filtro.getConfiguracaonfe().getEmpresa())){
				throw new SinedException("A empresa do xml � diferente da empresa do filtro. (Arquivo: " + filtro.getArquivo().getName() + ")");
			}
			
			bean.setIdentificadortela(new SimpleDateFormat("yyyyMMddHHmmssSSSS").format(System.currentTimeMillis()));
			request.getSession().setAttribute("IMPORTARXML_ESTADO_ASSOCIACAO_MATERIAL_" + bean.getIdentificadortela(), notaxml);
			request.setAttribute("haveDuplicata", notaxml.getListadup() != null && notaxml.getListadup().size() > 0);
			
			return new ModelAndView("/process/importarXMLEstadoAssociacao", "bean", bean);
		} catch (Exception e) {
			request.addError("Problemas na importa��o de notas.");
			request.addError(e.getMessage());
			e.printStackTrace();
			return sendRedirectToAction("index");
		}
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView associarMaterialNotas(WebRequestContext request, ImportarXMLEstadoBean beanTela){
		ImportarXMLEstadoBean bean = null;
		String identificadortela = request.getParameter("identificadortela");
		Object object = request.getParameter("posicaoLista");
		if (object==null){
			object = beanTela.getIndexlista()!=null ? beanTela.getIndexlista().toString() : null;
		}
		
		Integer index = Integer.parseInt((String)object);
		Object attr = request.getSession().getAttribute("LISTA_IMPORTARXML_ESTADO_NOTA_" + identificadortela);
		List<ImportarXMLEstadoBean> listaNotas = null;
		if (attr != null) {
			listaNotas = (List<ImportarXMLEstadoBean>) attr;
			bean = listaNotas.get(index);
			bean.setIndexlista(index);
			bean.setIdentificadortela(identificadortela);
			
			request.getSession().setAttribute("IMPORTARXML_ESTADO_ASSOCIACAO_MATERIAL_" + identificadortela, bean.getNotaXML());
			request.setAttribute("haveDuplicata", bean.getNotaXML().getListadup() != null && bean.getNotaXML().getListadup().size() > 0);
		}
		request.setAttribute("popUp", true);
		return new ModelAndView("direct:process/popup/popUpImportarXMLEstadoAssociacao","bean",bean);
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView saveAssociarMaterial(WebRequestContext request, ImportarXMLEstadoBean beanTela){
		String identificadortela = beanTela.getIdentificadortela();
		Object object = request.getParameter("posicaoLista");
		if (object==null){
			object = beanTela.getIndexlista()!=null ? beanTela.getIndexlista().toString() : null;
		}
		
		Integer index = Integer.parseInt((String)object);
		Object attr = request.getSession().getAttribute("LISTA_IMPORTARXML_ESTADO_NOTA_" + identificadortela);
		List<ImportarXMLEstadoBean> listaNotas = null;
		ImportarXMLEstadoBean beanSessao = null;
		String replicarRateio = request.getParameter("replicarRateio");
		
		List<ImportarXMLEstadoBean> listaNotasMensagem = new ArrayList<ImportarXMLEstadoBean>();
		if (attr != null) {
			listaNotas = (List<ImportarXMLEstadoBean>) attr;
			beanSessao = listaNotas.get(index);
			beanSessao.setListaItens(beanTela.getListaItens());
			beanSessao.setCriarContareceber(beanTela.getCriarContareceber());
			beanSessao.setRateio(beanTela.getRateio());
			listaNotas.set(index, beanSessao);
			
			if("true".equalsIgnoreCase(replicarRateio)){
				for(ImportarXMLEstadoBean notas : listaNotas){
					notas.setRateio(rateioService.getNovoRateio(beanTela.getRateio()));
					notas.setCriarContareceber(beanTela.getCriarContareceber());
				}
			}
			
			verificaListaItens(listaNotas, beanTela);
			
			for(ImportarXMLEstadoBean notas : listaNotas){
				validaInformacaoNota(notas);
				
				ImportarXMLEstadoBean notasJson = new ImportarXMLEstadoBean();
				notasJson.setInformacaocompleta(notas.getInformacaocompleta());
				notasJson.setMensagemerro(notas.getMensagemerro());
				listaNotasMensagem.add(notasJson);
			}
			
			
			request.getSession().setAttribute("LISTA_IMPORTARXML_ESTADO_NOTA_" + identificadortela, listaNotas);
			request.getSession().removeAttribute("IMPORTARXML_ESTADO_ASSOCIACAO_MATERIAL_" + identificadortela);
		}
		return new JsonModelAndView()
				.addObject("sucesso", true)
				.addObject("listaNotasMensagem", listaNotasMensagem);
	}
	
	private void verificaListaItens(List<ImportarXMLEstadoBean> listaNotas, ImportarXMLEstadoBean beanTela) {
		if(beanTela != null && SinedUtil.isListNotEmpty(beanTela.getListaItens())){
			HashMap<String, Material> mapCodMaterial = new HashMap<String, Material>();
			for (ImportarXMLEstadoItemBean item: beanTela.getListaItens()) {
				if(item.getItem() != null && item.getItem().getcProd() != null && item.getMaterial() != null){
					mapCodMaterial.put(item.getItem().getcProd(), item.getMaterial());
				}
			}
			
			if(mapCodMaterial.size() > 0 && SinedUtil.isListNotEmpty(listaNotas)){
				for(ImportarXMLEstadoBean bean : listaNotas){
					if(SinedUtil.isListNotEmpty(bean.getListaItens())){
						for (ImportarXMLEstadoItemBean item: bean.getListaItens()) {
							if(item.getItem() != null && item.getItem().getcProd() != null && item.getMaterial() == null){
								item.setMaterial(mapCodMaterial.get(item.getItem().getcProd()));
							}
						}
					}
				}
			}
		}
	}
	/**
	 * Action que realiza a importa��o da NF
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @throws Exception
	 * @since 14/06/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView importar(WebRequestContext request, ImportarXMLEstadoBean bean) throws Exception {
		Object obj = request.getSession().getAttribute("IMPORTARXML_ESTADO_ASSOCIACAO_MATERIAL_" + bean.getIdentificadortela());
		if(obj == null){
			request.addError("Problemas na importa��o de notas. N�o foi poss�vel pegar o bean da sess�o. Favor fazer o processo novamente.");
			return sendRedirectToAction("index");
		}
		NotaXML notaxml = (NotaXML)obj;
		
		Object obj2 = request.getSession().getAttribute("IMPORTARXML_ESTADO_ARQUIVO");
		if(obj2 == null){
			request.addError("Problemas na importa��o de notas. N�o foi poss�vel pegar o arquivo da sess�o. Favor fazer o processo novamente.");
			return sendRedirectToAction("index");
		}
		Arquivo arquivo = (Arquivo)obj2;
		
		Object obj3 = request.getSession().getAttribute("IMPORTARXML_ESTADO_CONFIGURACAONFE");
		if(obj3 == null){
			request.addError("Problemas na importa��o de notas. N�o foi poss�vel pegar a configura��o da sess�o. Favor fazer o processo novamente.");
			return sendRedirectToAction("index");
		}
		Configuracaonfe configuracaonfe = (Configuracaonfe)obj3;
		
		importar(request, bean, notaxml, arquivo, configuracaonfe);
		
		request.getSession().removeAttribute("IMPORTARXML_ESTADO_ASSOCIACAO_MATERIAL_" + bean.getIdentificadortela());
		return new ModelAndView("/process/importarXMLEstado", "filtro", new ImportarXMLEstadoFiltro());
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView importarNotas(WebRequestContext request, ImportarXMLEstadoConferenciaBean bean) throws Exception {
		Object attr = request.getSession().getAttribute("LISTA_IMPORTARXML_ESTADO_NOTA_" + bean.getIdentificadortela());
		if(attr == null){
			request.addError("Problemas na importa��o de notas. N�o foi poss�vel pegar o bean da sess�o. Favor fazer o processo novamente.");
			return sendRedirectToAction("index");
		}
		
		Object obj3 = request.getSession().getAttribute("IMPORTARXML_ESTADO_CONFIGURACAONFE");
		if(obj3 == null){
			request.addError("Problemas na importa��o de notas. N�o foi poss�vel pegar a configura��o da sess�o. Favor fazer o processo novamente.");
			return sendRedirectToAction("index");
		}
		Configuracaonfe configuracaonfe = (Configuracaonfe)obj3;
		
		List<ImportarXMLEstadoBean> listaNotas = (List<ImportarXMLEstadoBean>) attr;
		
		int sucesso = 0;
		int erro = 0;
		if(SinedUtil.isListNotEmpty(listaNotas)){
			if(SinedUtil.isListNotEmpty(bean.getListaNotas())){
				int i = 0;
				ImportarXMLEstadoBean notaSessao;
				for(ImportarXMLEstadoBean notaTela : bean.getListaNotas()){
					notaSessao = listaNotas.get(i); 
					notaSessao.setMarcado(notaTela.getMarcado());
					notaSessao.setNaturezaoperacao(notaTela.getNaturezaoperacao());
					i++;
				}
			}
			
			for(ImportarXMLEstadoBean nota : listaNotas){
				if(Boolean.TRUE.equals(nota.getMarcado())){
					if(importar(request, nota, nota.getNotaXML(), nota.getArquivoxml(), configuracaonfe)){
						sucesso++;
					}else {
						erro++;
					}
				}
			}
		}
		
		if(sucesso == 0 && erro == 0){
			request.addError("Nenhum xml processado.");
		}
		
		request.getSession().removeAttribute("LISTA_IMPORTARXML_ESTADO_NOTA_" + bean.getIdentificadortela());
		return new ModelAndView("/process/importarXMLEstado", "filtro", new ImportarXMLEstadoFiltro());
	}
	
	private boolean importar(WebRequestContext request, ImportarXMLEstadoBean bean, NotaXML notaxml, Arquivo arquivo, Configuracaonfe configuracaonfe){
		try{
			return this.importarArquivo(request, notaxml, arquivo, bean, configuracaonfe);
		} catch (Exception e) {
			try {
				if(!SinedUtil.existMessageError(request.getMessages(), "Problemas na importa��o de notas.")){
					request.addError("Problemas na importa��o de notas.");
				}
			} catch (Exception e2) {
				request.addError(e.getMessage());
			}
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * M�todo que faz a importa��o do arquivo.
	 *
	 * @param request
	 * @param notaxml
	 * @param arquivo
	 * @param listaItensAssociados
	 * @throws Exception
	 * @since 14/06/2012
	 * @author Rodrigo Freitas
	 * @param rateio 
	 * @param criarConteceber 
	 */
	private boolean importarArquivo(WebRequestContext request, NotaXML notaxml, Arquivo arquivo, ImportarXMLEstadoBean bean, Configuracaonfe configuracaonfe) throws Exception{
		Notafiscalproduto nota = new Notafiscalproduto();

		List<ImportarXMLEstadoItemBean> listaItensAssociados = bean.getListaItens();
		Rateio rateio = bean.getRateio();
		Naturezaoperacao naturezaPadrao = bean.getNaturezaoperacao() != null && bean.getNaturezaoperacao().getCdnaturezaoperacao() != null? naturezaoperacaoService.load(bean.getNaturezaoperacao()): null;
		
		nota.setNotaTipo(NotaTipo.NOTA_FISCAL_PRODUTO);
		
		if(naturezaPadrao != null){
			nota.setNaturezaoperacao(naturezaPadrao);
			nota.setOperacaonfe(naturezaPadrao.getOperacaonfe());
		}
		if(notaxml.getIndPag() != null)	nota.setFormapagamentonfe(Formapagamentonfe.values()[notaxml.getIndPag()]);
		nota.setNumero(notaxml.getnNF());
		nota.setDtEmissao(notaxml.getdEmi() != null ? notaxml.getdEmi() : notaxml.getDhEmi());
		
		if(notaxml.getTpNF() != null) nota.setTipooperacaonota(Tipooperacaonota.values()[notaxml.getTpNF()]);
		if(notaxml.getcMunFG() != null) nota.setMunicipiogerador(municipioService.findByCdibge(notaxml.getcMunFG()));
		if(notaxml.getFinNFe() != null) nota.setFinalidadenfe(Finalidadenfe.values()[notaxml.getFinNFe() - 1]);
		if(notaxml.getIdDest() != null) nota.setLocaldestinonfe(Localdestinonfe.values()[notaxml.getIdDest() - 1]);
		if(notaxml.getIndFinal() != null) nota.setOperacaonfe(Operacaonfe.values()[notaxml.getIndFinal()]);
		if(notaxml.getIndPres() != null) nota.setPresencacompradornfe(Presencacompradornfe.getByCdnfe(notaxml.getIndPres()));

		Documentotipo documentotipo = documentotipoService.findNotafiscal();
		if(documentotipo == null){
			request.addError("Tipo de documento 'Nota Fiscal' n�o encontrado.");
			return false;
		}
		
		Cnpj cnpj_emit = new Cnpj(notaxml.getCNPJ_emit());
		Empresa empresa = empresaService.findByCnpj(cnpj_emit);
		if(empresa == null){
			request.addError("Empresa n�o encontrada. - " + notaxml.getCNPJ_emit());
			return false;
		}
		
		if(notaxml.getSerie() == null){
			request.addError("S�rie da nf-e n�o pode ficar em branco.");
			return false;
		}
		
		if(notaxml.getdSaiEnt() != null && !empresaService.getNaopreencherdtsaidaNota(empresa)){
			String hora = "00:00";
			if(notaxml.gethSaiEnt() != null){
				hora = notaxml.gethSaiEnt().substring(0, 5);
			}
			String data = SinedDateUtils.toString(notaxml.getdSaiEnt()) + " " + hora;
			nota.setDatahorasaidaentrada(SinedDateUtils.stringToTimestamp(data, "dd/MM/yyyy HH:mm"));
		}
		
		if(notaxml.getnNF() != null && notafiscalprodutoService.isExisteNota(empresa, notaxml.getnNF(), notaxml.getSerie(), ModeloDocumentoFiscalEnum.NFE, null)){
			request.addError("Nota j� cadastrada. - " + notaxml.getnNF());
			return false;
		}
		nota.setEmpresa(empresa);
		nota.setSerienfe(notaxml.getSerie());
		nota.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFE);
		
		Cliente cliente = buscarCliente(notaxml);
	    nota.setCliente(cliente);
	    
	    Endereco endereco = null;
	    
	    if(notaxml.getCEP_dest() != null)
			//pesquisar endere�o
			endereco = enderecoService.loadEndereco(notaxml.getCEP_dest(), 
															notaxml.getNro_dest(),
															cliente.getCdpessoa());
		if(endereco == null){
			Municipio municipio = municipioService.findByCdibge(String.valueOf(notaxml.getcMun_dest()));
			endereco = new Endereco();
			endereco.setLogradouro(notaxml.getxLgr_dest());
			endereco.setNumero(notaxml.getNro_dest());
			endereco.setBairro(notaxml.getxBairro_dest());
			endereco.setComplemento(notaxml.getxCpl_dest() != null ? (notaxml.getxCpl_dest().length() > 50 ? notaxml.getxCpl_dest().substring(0, 50) : null) : null);
			if(notaxml.getCEP_dest() != null) endereco.setCep(new Cep(notaxml.getCEP_dest()));
			endereco.setMunicipio(municipio);
			endereco.setPessoa(cliente);
			endereco.setEnderecotipo(Enderecotipo.UNICO);
			
			enderecoService.saveOrUpdate(endereco);
		}
		nota.setEnderecoCliente(endereco);
		
		if(notaxml.getCNPJ_retirada() != null) {
			nota.setTipopessoaretirada(Tipopessoa.PESSOA_JURIDICA);
			nota.setCnpjretirada(new Cnpj(notaxml.getCNPJ_retirada()));
		}
		if(notaxml.getCPF_retirada() != null) {
			nota.setTipopessoaretirada(Tipopessoa.PESSOA_FISICA);
			nota.setCpfretirada(new Cpf(notaxml.getCPF_retirada()));
		}
		nota.setLogradouroretirada(notaxml.getxLgr_retirada());
		nota.setNumeroretirada(notaxml.getNro_retirada());
		nota.setComplementoretirada(notaxml.getxCpl_retirada());
		nota.setBairroretirada(notaxml.getxBairro_retirada());
		if(notaxml.getcMun_retirada() != null) nota.setMunicipioretirada(municipioService.findByCdibge(notaxml.getcMun_retirada()));
		
		if(notaxml.getCNPJ_entrega() != null) {
			nota.setTipopessoaentrega(Tipopessoa.PESSOA_JURIDICA);
			nota.setCnpjentrega(new Cnpj(notaxml.getCNPJ_entrega()));
		}
		if(notaxml.getCPF_entrega() != null) {
			nota.setTipopessoaentrega(Tipopessoa.PESSOA_FISICA);
			nota.setCpfentrega(new Cpf(notaxml.getCPF_entrega()));
		}
		nota.setLogradouroentrega(notaxml.getxLgr_entrega());
		nota.setNumeroentrega(notaxml.getNro_entrega());
		nota.setComplementoentrega(notaxml.getxCpl_entrega());
		nota.setBairroentrega(notaxml.getxBairro_entrega());
		if(notaxml.getcMun_entrega() != null) nota.setMunicipioentrega(municipioService.findByCdibge(notaxml.getcMun_entrega()));
		
		if(notaxml.getvBC_ICMSTot() != null) nota.setValorbcicms(new Money(notaxml.getvBC_ICMSTot()));
		if(notaxml.getvICMS() != null) nota.setValoricms(new Money(notaxml.getvICMS()));
		if(notaxml.getvBCST() != null) nota.setValorbcicmsst(new Money(notaxml.getvBCST()));
		if(notaxml.getvST() != null) nota.setValoricmsst(new Money(notaxml.getvST()));
		if(notaxml.getvProd() != null) nota.setValorprodutos(new Money(notaxml.getvProd()));
		if(notaxml.getvFrete() != null) nota.setValorfrete(new Money(notaxml.getvFrete()));
		if(notaxml.getvSeg() != null) nota.setValorseguro(new Money(notaxml.getvSeg()));
		if(notaxml.getvDesc() != null) nota.setValordesconto(new Money(notaxml.getvDesc()));
		if(notaxml.getvIPI() != null) nota.setValoripi(new Money(notaxml.getvIPI()));
		if(notaxml.getvPIS_ICMSTot() != null) nota.setValorpis(new Money(notaxml.getvPIS_ICMSTot()));
		if(notaxml.getvCOFINS_ICMSTot() != null) nota.setValorcofins(new Money(notaxml.getvCOFINS_ICMSTot()));
		if(notaxml.getvOutro() != null) nota.setOutrasdespesas(new Money(notaxml.getvOutro()));
		if(notaxml.getvNF() != null) nota.setValor(new Money(notaxml.getvNF()));
		
		if(notaxml.getvServ() != null) nota.setValorservicos(new Money(notaxml.getvServ()));
		if(notaxml.getvBC_ISSQNtot() != null) nota.setValorbciss(new Money(notaxml.getvBC_ISSQNtot()));
		if(notaxml.getvISS() != null) nota.setValoriss(new Money(notaxml.getvISS()));
		if(notaxml.getvPIS_ISSQNtot() != null) nota.setValorpisservicos(new Money(notaxml.getvPIS_ISSQNtot()));
		if(notaxml.getvCOFINS_ISSQNtot() != null) nota.setValorcofinsservicos(new Money(notaxml.getvCOFINS_ISSQNtot()));
		
		if(notaxml.getvRetPIS() != null) nota.setValorpisretido(new Money(notaxml.getvRetPIS()));
		if(notaxml.getvRetCOFINS() != null) nota.setValorcofinsretido(new Money(notaxml.getvRetCOFINS()));
		if(notaxml.getvRetCSLL() != null) nota.setValorcsll(new Money(notaxml.getvRetCSLL()));
		if(notaxml.getvBCIRRF() != null) nota.setValorbcirrf(new Money(notaxml.getvBCIRRF()));
		if(notaxml.getvIRRF() != null) nota.setValorirrf(new Money(notaxml.getvIRRF()));
		if(notaxml.getvBCRetPrev() != null) nota.setValorbcprevsocial(new Money(notaxml.getvBCRetPrev()));
		if(notaxml.getvRetPrev() != null) nota.setValorprevsocial(new Money(notaxml.getvRetPrev()));
		
		
		if(notaxml.getModFrete() != null){
			switch (notaxml.getModFrete()) {
			case 0:
				nota.setResponsavelfrete(ResponsavelFrete.EMITENTE);
				break;
			case 1:
				nota.setResponsavelfrete(ResponsavelFrete.DESTINATARIO);
				break;
			case 2:
				nota.setResponsavelfrete(ResponsavelFrete.TERCEIROS);
				break;
			case 3:
				nota.setResponsavelfrete(ResponsavelFrete.PROPRIO_REMETENTE);
				break;
			case 4:
				nota.setResponsavelfrete(ResponsavelFrete.PROPRIO_DESTINATARIO);
				break;
			case 9:
				nota.setResponsavelfrete(ResponsavelFrete.SEM_FRETE);
				break;

			default:
				nota.setResponsavelfrete(ResponsavelFrete.SEM_FRETE);
				break;
			}
		}
		
		Fornecedor fornecedor = null;
		Pessoa pessoa = null;
		//pesquisar pessoa 
	    if(notaxml.getCPF_transporta() != null && !notaxml.getCPF_transporta().equals("") ){
	    	Cpf cpf = new Cpf(notaxml.getCPF_transporta());
	    	fornecedor = fornecedorService.findByCpf(cpf);
	    	if(fornecedor == null){
	    		pessoa = pessoaService.findPessoaByCpf(cpf);
	    		
	    		fornecedor = new Fornecedor();
    			fornecedor.setCpf(cpf);
    			fornecedor.setNome(notaxml.getxNome_transporta());
    			fornecedor.setAtivo(true);
    			fornecedor.setAcesso(true);
    			fornecedor.setTipopessoa(Tipopessoa.PESSOA_FISICA);
    			
	    		if(pessoa == null){
	    			fornecedorService.saveOrUpdate(fornecedor);
	    		} else if(pessoa != null){
	    			fornecedor.setCdpessoa(pessoa.getCdpessoa());
	    			fornecedorService.insertSomenteFornecedor(fornecedor);
	    		}
	    	}
	    } else if(notaxml.getCNPJ_transporta() != null && !notaxml.getCNPJ_transporta().equals("")){
	    	Cnpj cnpj = new Cnpj(notaxml.getCNPJ_transporta());
	    	fornecedor = fornecedorService.findByCnpj(cnpj);
	    	if(fornecedor == null){
	    		pessoa = pessoaService.findPessoaByCnpj(cnpj);
	    		
	    		fornecedor = new Fornecedor();
    			fornecedor.setCnpj(cnpj);
    			fornecedor.setNome(notaxml.getxNome_transporta());
    			fornecedor.setAtivo(true);
    			fornecedor.setAcesso(true);
    			fornecedor.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
	    		
	    		if(pessoa == null){
	    			fornecedorService.saveOrUpdate(fornecedor);
	    		} else if(pessoa != null){
	    			fornecedor.setCdpessoa(pessoa.getCdpessoa());
	    			fornecedorService.insertSomenteFornecedor(fornecedor);
	    		}
	    	}
	    } else if(notaxml.getxNome_transporta() != null && !notaxml.getxNome_transporta().equals("")){ 
	    	try {
	    		fornecedor = fornecedorService.findByNome(notaxml.getxNome_transporta());
	    		if(fornecedor == null){
	    			pessoa = pessoaService.findPessoaByNome(notaxml.getxNome_transporta());
	    			
	    			fornecedor = new Fornecedor();
	    			fornecedor.setNome(notaxml.getxNome_transporta());
	    			fornecedor.setAtivo(true);
	    			fornecedor.setAcesso(true);
	    			fornecedor.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
	    			
	    			if(pessoa == null){
	    				fornecedorService.saveOrUpdate(fornecedor);
	    			} else if(pessoa != null){
	    				fornecedor.setCdpessoa(pessoa.getCdpessoa());
	    				fornecedorService.insertSomenteFornecedor(fornecedor);
	    			}
	    		}
			} catch (Exception e) {}
	    }
	    
	    nota.setTransportador(fornecedor);
	    
	    if(notaxml.getPlaca() != null || notaxml.getUF_veicTransp() != null && notaxml.getRNTC() != null){
	    	nota.setTransporteveiculo(Boolean.TRUE);
	    	nota.setPlacaveiculo(notaxml.getPlaca());
	    	if(notaxml.getUF_veicTransp() != null) nota.setUfveiculo(ufService.findBySigla(notaxml.getUF_veicTransp()));
	    	nota.setRntc(notaxml.getRNTC());
	    } else {
	    	nota.setTransporteveiculo(Boolean.FALSE);
	    }
	    
	    nota.setQtdevolume(notaxml.getqVol());
	    nota.setEspvolume(notaxml.getEsp());
	    nota.setMarcavolume(notaxml.getMarca());
	    nota.setNumvolume(notaxml.getnVol());
	    nota.setPesoliquido(notaxml.getPesoL());
	    nota.setPesobruto(notaxml.getPesoB());
	    
		nota.setInfoadicionalcontrib(notaxml.getInfCpl());
		nota.setInfoadicionalfisco(notaxml.getInfAdFisco());
		
		nota.setNumerofatura(notaxml.getnFat());
		if(notaxml.getvOrig_fat() != null) nota.setValororiginalfatura(new Money(notaxml.getvOrig_fat()));
		if(notaxml.getvDesc_fat() != null) nota.setValordescontofatura(new Money(notaxml.getvDesc_fat()));
		if(notaxml.getvLiq_fat() != null) nota.setValorliquidofatura(new Money(notaxml.getvLiq_fat()));
		
		List<Notafiscalprodutoduplicata> listaDuplicata = new ArrayList<Notafiscalprodutoduplicata>();
		if(notaxml.getListadup() != null){
			Notafiscalprodutoduplicata notafiscalprodutoduplicata;
			for (NotaXMLDuplicata notaXMLDuplicata : notaxml.getListadup()) {
				notafiscalprodutoduplicata = new Notafiscalprodutoduplicata();
				notafiscalprodutoduplicata.setNotafiscalproduto(nota);
				notafiscalprodutoduplicata.setNumero(notaXMLDuplicata.getnDup());
				notafiscalprodutoduplicata.setDtvencimento(notaXMLDuplicata.getdVenc());
				if(notaXMLDuplicata.getvDup() != null) notafiscalprodutoduplicata.setValor(new Money(notaXMLDuplicata.getvDup()));
				
				listaDuplicata.add(notafiscalprodutoduplicata);
			}
			nota.setListaDuplicata(listaDuplicata);
		}
		Boolean criarContaReceber = Boolean.TRUE.equals(bean.getCriarContareceber()) && SinedUtil.isListNotEmpty(nota.getListaDuplicata());
		if(criarContaReceber){
			nota.setNotaStatus(NotaStatus.NFE_LIQUIDADA);
		} else {
			nota.setNotaStatus(NotaStatus.NFE_EMITIDA);
		}
		
		if(notaxml.getListadet() != null){
			List<Notafiscalprodutoitem> listaItens = new ArrayList<Notafiscalprodutoitem>();
			Notafiscalprodutoitem notafiscalprodutoitem;
			for (NotaXMLItem notaxmlitem : notaxml.getListadet()) {
				notafiscalprodutoitem = new Notafiscalprodutoitem();
				notafiscalprodutoitem.setNotafiscalproduto(nota);
				
				Material material = null;
				for (ImportarXMLEstadoItemBean itemAssociado : listaItensAssociados) {
					if(itemAssociado.getItem() != null && 
							itemAssociado.getItem().getSequencial() != null &&
							notaxmlitem.getSequencial() != null &&
							itemAssociado.getItem().getSequencial().equals(notaxmlitem.getSequencial())){
						material = itemAssociado.getMaterial();
						break;
					}
				}
				
				if(material == null){
					request.addError("Material n�o encontrado.");
					return false;
				}				
				
				notafiscalprodutoitem.setMaterial(material);
				
				List<Materialclasse> listaMaterialclasse = materialService.findClasses(material);
				
				for (Materialclasse classe : listaMaterialclasse) {
					if(Materialclasse.PRODUTO.equals(classe)){
						notafiscalprodutoitem.setMaterialclasse(classe);
						break;
					}else if(Materialclasse.EPI.equals(classe)){
						notafiscalprodutoitem.setMaterialclasse(classe);
						break;
					}else if(Materialclasse.PATRIMONIO.equals(classe)){
						notafiscalprodutoitem.setMaterialclasse(classe);
					}else if(Materialclasse.SERVICO.equals(classe)){
						notafiscalprodutoitem.setMaterialclasse(classe);
					}
				}				
				
				if(notaxmlitem.getcEAN() != null && !"".equals(notaxmlitem.getcEAN()) && SinedUtil.validaCEAN(notaxmlitem.getcEAN()))
					notafiscalprodutoitem.setGtin(notaxmlitem.getcEAN());
				
				if(notaxmlitem.getNCM() != null && notaxmlitem.getNCM().length() >= 2){
					String ncmcapituloStr = notaxmlitem.getNCM().substring(0, 2);
					Ncmcapitulo ncmcapitulo = ncmcapituloService.load(new Ncmcapitulo(Integer.parseInt(ncmcapituloStr)));
					
					notafiscalprodutoitem.setNcmcapitulo(ncmcapitulo);
					notafiscalprodutoitem.setNcmcompleto(notaxmlitem.getNCM());
				}
				notafiscalprodutoitem.setExtipi(notaxmlitem.getEXTIPI());
				if(notaxmlitem.getCFOP() != null) notafiscalprodutoitem.setCfop(cfopService.findByCodigo(notaxmlitem.getCFOP()));
				
				if(notaxmlitem.getuCom() != null && !notaxmlitem.getuCom().equals("")){
					Unidademedida unidademedida = unidademedidaService.findBySimbolo(notaxmlitem.getuCom());
					notafiscalprodutoitem.setUnidademedida(unidademedida);
				}
				
				notafiscalprodutoitem.setQtde(notaxmlitem.getqCom());
				notafiscalprodutoitem.setValorunitario(notaxmlitem.getvUnCom());
				if(notaxmlitem.getvProd() != null) notafiscalprodutoitem.setValorbruto(new Money(notaxmlitem.getvProd()));
				if(notaxmlitem.getvFrete() != null) notafiscalprodutoitem.setValorfrete(new Money(notaxmlitem.getvFrete()));
				if(notaxmlitem.getvDesc() != null) notafiscalprodutoitem.setValordesconto(new Money(notaxmlitem.getvDesc()));
				if(notaxmlitem.getvSeg() != null) notafiscalprodutoitem.setValorseguro(new Money(notaxmlitem.getvSeg()));
				if(notaxmlitem.getvOutro() != null) notafiscalprodutoitem.setOutrasdespesas(new Money(notaxmlitem.getvOutro()));
				
				if(notaxmlitem.getIndTot() != null){
					switch (notaxmlitem.getIndTot()) {
					case 0:
						notafiscalprodutoitem.setIncluirvalorprodutos(Boolean.FALSE);
						break;
					case 1:
						notafiscalprodutoitem.setIncluirvalorprodutos(Boolean.TRUE);
						break;
					default:
						notafiscalprodutoitem.setIncluirvalorprodutos(Boolean.TRUE);
						break;
					}
				}
				
				notafiscalprodutoitem.setTributadoicms(notaxmlitem.getcSitTrib_ISSQN() == null);
				
				if(notaxmlitem.getOrig() != null) notafiscalprodutoitem.setOrigemproduto(Origemproduto.getEnum(notaxmlitem.getOrig()));
				if(notaxmlitem.getCST_ICMS() != null) notafiscalprodutoitem.setTipocobrancaicms(Tipocobrancaicms.getTipocobrancaicms(notaxmlitem.getCST_ICMS()));
				if(notaxmlitem.getCSOSN_ICMS() != null) notafiscalprodutoitem.setTipocobrancaicms(Tipocobrancaicms.getTipocobrancaicms(notaxmlitem.getCSOSN_ICMS()));
				if(notafiscalprodutoitem.getTipocobrancaicms() != null){
					if(notafiscalprodutoitem.getTipocobrancaicms().isSimplesnacional()){
						notafiscalprodutoitem.setTipotributacaoicms(Tipotributacaoicms.SIMPLES_NACIONAL);
					} else {
						notafiscalprodutoitem.setTipotributacaoicms(Tipotributacaoicms.NORMAL);
					}
				}
				if(notaxmlitem.getModBC() != null) notafiscalprodutoitem.setModalidadebcicms(Modalidadebcicms.values()[notaxmlitem.getModBC()]);
				if(notaxmlitem.getpRedBC() != null) notafiscalprodutoitem.setReducaobcicms(notaxmlitem.getpRedBC());
				if(notaxmlitem.getvBC() != null) notafiscalprodutoitem.setValorbcicms(new Money(notaxmlitem.getvBC()));
				notafiscalprodutoitem.setIcms(notaxmlitem.getpICMS());
				if(notaxmlitem.getvICMS() != null) notafiscalprodutoitem.setValoricms(new Money(notaxmlitem.getvICMS()));
				if(notaxmlitem.getMotDesICMS() != null) notafiscalprodutoitem.setMotivodesoneracaoicms(Motivodesoneracaoicms.getMotivodesoneracaoicms(notaxmlitem.getMotDesICMS()));
				if(notaxmlitem.getModBCST() != null) notafiscalprodutoitem.setModalidadebcicmsst(Modalidadebcicmsst.values()[notaxmlitem.getModBCST()]);
				if(notaxmlitem.getpMVAST() != null) notafiscalprodutoitem.setMargemvaloradicionalicmsst(notaxmlitem.getpMVAST());
				if(notaxmlitem.getpRedBCST() != null) notafiscalprodutoitem.setReducaobcicmsst(notaxmlitem.getpRedBCST());
				if(notaxmlitem.getvBCST() != null) notafiscalprodutoitem.setValorbcicmsst(new Money(notaxmlitem.getvBCST()));
				notafiscalprodutoitem.setIcmsst(notaxmlitem.getpICMSST());
				if(notaxmlitem.getvICMSST() != null) notafiscalprodutoitem.setValoricmsst(new Money(notaxmlitem.getvICMSST()));
				if(notaxmlitem.getpBCOp() != null) notafiscalprodutoitem.setBcoperacaopropriaicms(new Money(notaxmlitem.getpBCOp()));
				if(notaxmlitem.getUFST() != null) notafiscalprodutoitem.setUficmsst(ufService.findBySigla(notaxmlitem.getUFST()));
				if(notaxmlitem.getvBCSTRet() != null) notafiscalprodutoitem.setValorbcicms(new Money(notaxmlitem.getvBCSTRet()));
				if(notaxmlitem.getvICMSSTRet() != null) notafiscalprodutoitem.setValoricms(new Money(notaxmlitem.getvICMSSTRet()));
				if(notaxmlitem.getvBCSTDest() != null) notafiscalprodutoitem.setValorbcicmsst(new Money(notaxmlitem.getvBCSTDest()));
				if(notaxmlitem.getvICMSSTDest() != null) notafiscalprodutoitem.setValoricmsst(new Money(notaxmlitem.getvICMSSTDest()));
				notafiscalprodutoitem.setAliquotacreditoicms(notaxmlitem.getpCredSN());
				if(notaxmlitem.getvCredICMSSN() != null) notafiscalprodutoitem.setValorcreditoicms(new Money(notaxmlitem.getvCredICMSSN()));
		
				if(notaxmlitem.getCNPJProd() != null) notafiscalprodutoitem.setCnpjprodutoripi(new Cnpj(notaxmlitem.getCNPJProd()));
				notafiscalprodutoitem.setCodigoseloipi(notaxmlitem.getcSelo());
				notafiscalprodutoitem.setQtdeseloipi(notaxmlitem.getqSelo());
				if(notaxmlitem.getCST_IPI() != null) notafiscalprodutoitem.setTipocobrancaipi(Tipocobrancaipi.getTipocobrancaipi(notaxmlitem.getCST_IPI()));
				if(notaxmlitem.getvBC_IPI() != null){
					notafiscalprodutoitem.setTipocalculoipi(Tipocalculo.PERCENTUAL);
				} else if(notaxmlitem.getqUnid_IPI() != null){
					notafiscalprodutoitem.setTipocalculoipi(Tipocalculo.EM_VALOR);
				}
				if(notaxmlitem.getvBC_IPI() != null) notafiscalprodutoitem.setValorbcipi(new Money(notaxmlitem.getvBC_IPI()));
				notafiscalprodutoitem.setIpi(notaxmlitem.getpIPI());
				notafiscalprodutoitem.setQtdevendidaipi(notaxmlitem.getqUnid_IPI());
				if(notaxmlitem.getvUnid_IPI() != null) notafiscalprodutoitem.setAliquotareaisipi(new Money(notaxmlitem.getvUnid_IPI()));
				if(notaxmlitem.getvIPI() != null) notafiscalprodutoitem.setValoripi(new Money(notaxmlitem.getvIPI()));
				
				if(notaxmlitem.getCST_PIS() != null) notafiscalprodutoitem.setTipocobrancapis(Tipocobrancapis.getTipocobrancapis(notaxmlitem.getCST_PIS()));
				if(notaxmlitem.getvBC_PIS() != null) notafiscalprodutoitem.setValorbcpis(new Money(notaxmlitem.getvBC_PIS()));
				notafiscalprodutoitem.setPis(notaxmlitem.getpPIS());
				notafiscalprodutoitem.setQtdevendidapis(notaxmlitem.getqBCProd_PIS());
				if(notaxmlitem.getvAliqProd_PIS() != null) notafiscalprodutoitem.setAliquotareaispis(new Money(notaxmlitem.getvAliqProd_PIS()));
				if(notaxmlitem.getvPIS() != null) notafiscalprodutoitem.setValorpis(new Money(notaxmlitem.getvPIS()));
				
				if(notaxmlitem.getCST_COFINS() != null) notafiscalprodutoitem.setTipocobrancacofins(Tipocobrancacofins.getTipocobrancacofins(notaxmlitem.getCST_COFINS()));
				if(notaxmlitem.getvBC_COFINS() != null) notafiscalprodutoitem.setValorbccofins(new Money(notaxmlitem.getvBC_COFINS()));
				notafiscalprodutoitem.setCofins(notaxmlitem.getpCOFINS());
				notafiscalprodutoitem.setQtdevendidacofins(notaxmlitem.getqBCProd_COFINS());
				if(notaxmlitem.getvAliqProd_COFINS() != null) notafiscalprodutoitem.setAliquotareaiscofins(new Money(notaxmlitem.getvAliqProd_COFINS()));
				if(notaxmlitem.getvCOFINS() != null) notafiscalprodutoitem.setValorcofins(new Money(notaxmlitem.getvCOFINS()));
				
				if(notaxmlitem.getvBC_ISSQN() != null) notafiscalprodutoitem.setValorbciss(new Money(notaxmlitem.getvBC_ISSQN()));
				notafiscalprodutoitem.setIss(notaxmlitem.getvAliq_ISSQN());
				if(notaxmlitem.getvISSQN() != null) notafiscalprodutoitem.setValoriss(new Money(notaxmlitem.getvISSQN()));
				if(notaxmlitem.getcMunFG_ISSQN() != null) notafiscalprodutoitem.setMunicipioiss(municipioService.findByCdibge(notaxmlitem.getcMunFG_ISSQN()));
				if(notaxmlitem.getcListServ_ISSQN() != null) notafiscalprodutoitem.setItemlistaservico(itemlistaservicoService.loadByCodigo(notaxmlitem.getcListServ_ISSQN()));
				if(notaxmlitem.getcSitTrib_ISSQN() != null) notafiscalprodutoitem.setTipotributacaoiss(Tipotributacaoiss.getTipotributacaoiss(notaxmlitem.getcSitTrib_ISSQN()));
				
				notafiscalprodutoitem.setInfoadicional(notaxmlitem.getInfAdProd());
				
				listaItens.add(notafiscalprodutoitem);
			}
			nota.setListaItens(listaItens);
		}
		
		notafiscalprodutoService.saveOrUpdate(nota);
		
		NotaHistorico notaHistorico = new NotaHistorico();
		notaHistorico.setNota(nota);
		if(criarContaReceber){
			notaHistorico.setNotaStatus(NotaStatus.NFE_LIQUIDADA);
		} else {
			notaHistorico.setNotaStatus(NotaStatus.NFE_EMITIDA);
		}
		notaHistorico.setObservacao("Nota importada via XML.");
		
		notaHistoricoService.saveOrUpdate(notaHistorico);
		
		Arquivonfnota arquivonfnota = new Arquivonfnota();
		
		arquivoService.saveOrUpdate(arquivo);
		arquivonfnota.setArquivoxml(arquivo);	
	    arquivoDAO.saveFile(arquivonfnota, "arquivoxml");
		
		Arquivonf arquivonf = new Arquivonf();
		arquivonf.setDtenvio(SinedDateUtils.currentDate());
		arquivonf.setEmpresa(empresa);
		arquivonf.setConfiguracaonfe(configuracaonfe);
		arquivonf.setArquivonfsituacao(Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
		arquivonf.setDtrecebimento(notaxml.getDhRecbto());
		
		arquivonf.setAssinando(Boolean.TRUE);
		arquivonf.setEnviando(Boolean.TRUE);
		arquivonf.setConsultando(Boolean.TRUE);
		
		arquivonf.setArquivoxml(arquivo);
		arquivonf.setArquivoxmlassinado(arquivo);
		
		arquivonfService.saveOrUpdate(arquivonf);
		
		arquivonfnota.setArquivonf(arquivonf);
		arquivonfnota.setNota(nota);
		arquivonfnota.setChaveacesso(notaxml.getChNFe());
		arquivonfnota.setDtprocessamento(notaxml.getDhRecbto());
		arquivonfnota.setProtocolonfe(notaxml.getnProt());
		
		arquivonfnotaService.saveOrUpdate(arquivonfnota);
		
		if(criarContaReceber){
			for (Notafiscalprodutoduplicata nfpDup : listaDuplicata) {
				Documento documento = new Documento();
				documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
				documento.setDocumentotipo(documentotipo);
				documento.setDescricao(notaxml.getNatOp());
				documento.setNumero(notaxml.getnNF());
				documento.setTipopagamento(Tipopagamento.CLIENTE);
				documento.setPessoa(nota.getCliente());
				documento.setCliente(nota.getCliente());
				documento.setDtemissao(nota.getDtEmissao());
				documento.setDtvencimento(nfpDup.getDtvencimento() != null ? nfpDup.getDtvencimento() : nota.getDtEmissao());
				documento.setValor(nfpDup.getValor());
				documento.setEmpresa(nota.getEmpresa());
				
				Rateio rat = new Rateio();
				List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>(rateio.getListaRateioitem());
				rat.setListaRateioitem(listaRateioitem);
				rateioService.atualizaValorRateio(rat, documento.getValor());
				
				documento.setRateio(rat);
				
				StringBuilder historicoDocumento = new StringBuilder();
				historicoDocumento.append("Origem da nota <a href=\"javascript:visualizarNotaFiscalProduto(")
									 .append(nota.getCdNota()).append(")\">")
									 .append(nota.getNumero() != null ? nota.getNumero() : "sem n�mero")
									 .append("</a>");
				
				documento.setObservacaoHistorico(historicoDocumento.toString());
				if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_MENSAGEM_NUMERO_NF)){
					documento.setMensagem1("Referente a NF: " + (nota.getNumero() != null ? nota.getNumero() : "sem n�mero"));
				}
				
				Documentohistorico documentohistorico = new Documentohistorico(documento);
				
				List<Documentohistorico> listaHistorico = new ArrayList<Documentohistorico>();
				listaHistorico.add(documentohistorico);
				
				documento.setListaDocumentohistorico(new ListSet<Documentohistorico>(Documentohistorico.class, listaHistorico));
				
				contareceberService.saveOrUpdate(documento);
				
				NotaDocumento notaDocumento = new NotaDocumento(nota, documento);
				notaDocumentoService.saveOrUpdate(notaDocumento);
				
				notaHistorico = new NotaHistorico(null, 
						nota, 
						nota.getNotaStatus(), 
						"Gerada a conta a receber <a href=\"javascript:visualizarContaReceber("+documento.getCddocumento()+")\">"+documento.getCddocumento()+"</a>", 
						SinedUtil.getUsuarioLogado().getCdpessoa(), 
						new Timestamp(System.currentTimeMillis()));

				notaHistoricoService.saveOrUpdate(notaHistorico);
			}
		}
		
		request.addMessage("Arquivo importado com sucesso: " + arquivo.getName());
		return true;
	}
	
	private Cliente buscarCliente(NotaXML notaxml) {
		Cliente cliente = null;
		Pessoa pessoa = null;
		//pesquisar pessoa 
	    if(notaxml.getCPF_dest() != null && !notaxml.getCPF_dest().equals("") ){
	    	Cpf cpf = new Cpf(notaxml.getCPF_dest());
	    	cliente = clienteService.findByCpf(cpf);
	    	if(cliente == null){
	    		pessoa = pessoaService.findPessoaByCpf(cpf);
	    		
	    		cliente = new Cliente();
    			cliente.setCpf(cpf);
    			cliente.setNome(notaxml.getxNome());
    			cliente.setInscricaoestadual(notaxml.getIE_dest());
    			cliente.setEmail(notaxml.getEmail_dest());
    			cliente.setAtivo(true);
    			cliente.setTipopessoa(Tipopessoa.PESSOA_FISICA);
    			
	    		if(pessoa == null){
	    			clienteService.saveOrUpdate(cliente);
	    		} else if(pessoa != null){
	    			cliente.setCdpessoa(pessoa.getCdpessoa());
	    			clienteService.insertSomenteCliente(cliente);
	    		}
	    	}
	    } else if(notaxml.getCNPJ_dest() != null && !notaxml.getCNPJ_dest().equals("")){
	    	Cnpj cnpj = new Cnpj(notaxml.getCNPJ_dest());
	    	cliente = clienteService.findByCnpj(cnpj);
	    	if(cliente == null){
	    		pessoa = pessoaService.findPessoaByCnpj(cnpj);
	    		
	    		cliente = new Cliente();
    			cliente.setCnpj(cnpj);
    			cliente.setNome(notaxml.getxNome());
    			cliente.setInscricaoestadual(notaxml.getIE_dest());
    			cliente.setEmail(notaxml.getEmail_dest());
    			cliente.setAtivo(true);
    			cliente.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
	    		
	    		if(pessoa == null){
	    			clienteService.saveOrUpdate(cliente);
	    		} else if(pessoa != null){
	    			cliente.setCdpessoa(pessoa.getCdpessoa());
	    			clienteService.insertSomenteCliente(cliente);
	    		}
	    	}
	    } else if(notaxml.getxNome() != null && !notaxml.getxNome().equals("")){ 
	    	cliente = clienteService.findByNome(notaxml.getxNome());
	    	if(cliente == null){
	    		pessoa = pessoaService.findPessoaByNome(notaxml.getxNome());
	    		
	    		cliente = new Cliente();
    			cliente.setNome(notaxml.getxNome());
    			cliente.setInscricaoestadual(notaxml.getIE_dest());
    			cliente.setEmail(notaxml.getEmail_dest());
    			cliente.setAtivo(true);
    			cliente.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
	    		
	    		if(pessoa == null){
	    			clienteService.saveOrUpdate(cliente);
	    		} else if(pessoa != null){
	    			cliente.setCdpessoa(pessoa.getCdpessoa());
	    			clienteService.insertSomenteCliente(cliente);
	    		}
	    	}
	    }
	    
	    return cliente;
	}
	
	public ModelAndView abrirImportaArquivo(WebRequestContext request, ImportarXMLEstadoBean bean) throws Exception{
		Object obj = request.getSession().getAttribute("IMPORTARXML_ESTADO_ASSOCIACAO_MATERIAL_" + bean.getIdentificadortela());
		if(obj == null){
			request.addError("Problemas na importa��o de notas. N�o foi poss�vel pegar o bean da sess�o. Favor fazer o processo novamente.");
			return sendRedirectToAction("index");
		}
		NotaXML notaxml = (NotaXML)obj;
		
		String whereinCfops = this.montaWhereinCfopByNotaxml(notaxml);
		List<Naturezaoperacao> listaNaturezaoperacao = naturezaoperacaoService.findByCfops(whereinCfops);
		if(listaNaturezaoperacao.isEmpty()){
			listaNaturezaoperacao = naturezaoperacaoService.findByCfops(null);
		}
		return new ModelAndView("direct:/process/popup/selecionarNaturezaImportacaoxml", "listaNaturezaoperacao", listaNaturezaoperacao);
	}
	
	public ModelAndView ajaxVerificaNaturezaForImportacaoXML(WebRequestContext request, ImportarXMLEstadoBean bean){
		Object obj = request.getSession().getAttribute("IMPORTARXML_ESTADO_ASSOCIACAO_MATERIAL_" + bean.getIdentificadortela());
		String msgErro = "";
		if(obj == null){
			msgErro = "Problemas na importa��o de notas. N�o foi poss�vel pegar o bean da sess�o. Favor fazer o processo novamente.\n";
		}
		NotaXML notaxml = (NotaXML)obj;

		String whereinCfops = this.montaWhereinCfopByNotaxml(notaxml);
		List<Naturezaoperacao> listaNaturezaoperacao = naturezaoperacaoService.findByCfops(whereinCfops);
		String cdnaturezaoperacaounica = listaNaturezaoperacao.size() == 1? listaNaturezaoperacao.get(0).getCdnaturezaoperacao().toString(): "";
		return new JsonModelAndView()
					.addObject("msgErro", msgErro)
					.addObject("cdnaturezaoperacaounica", cdnaturezaoperacaounica);
	}
	
	private String montaWhereinCfopByNotaxml(NotaXML notaxml){
		List<String> listaCfops = new ArrayList<String>();
		for(NotaXMLItem notaxmlitem: notaxml.getListadet()){
			if(Util.strings.isNotEmpty(notaxmlitem.getCFOP())){
				if(!listaCfops.contains(notaxmlitem.getCFOP())){
					listaCfops.add(notaxmlitem.getCFOP());
				}
			}
		}
		return SinedUtil.montaWhereInString(CollectionsUtil.concatenate(listaCfops, ","));
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView ajaxValidaNotasImportacaoXML(WebRequestContext request, ImportarXMLEstadoConferenciaBean beanTela){
		Object attr = request.getSession().getAttribute("LISTA_IMPORTARXML_ESTADO_NOTA_" + beanTela.getIdentificadortela());
		StringBuilder msgErro = new StringBuilder();
		String whereInIndex = request.getParameter("whereInIndex");
		
		if(attr == null || StringUtils.isBlank(whereInIndex)){
			msgErro.append("Problemas na importa��o de notas. N�o foi poss�vel pegar o bean da sess�o. Favor fazer o processo novamente.\n");
		}else {
			List<ImportarXMLEstadoBean> listaNotas = (List<ImportarXMLEstadoBean>) attr;
			if(SinedUtil.isListNotEmpty(listaNotas)){
				String[] arrayIndex = whereInIndex.split(",");
				ImportarXMLEstadoBean nota;
				String msgErroNota; 
				for(String index : arrayIndex){
					nota = listaNotas.get(Integer.parseInt(index));
					msgErroNota = validaInformacaoNota(nota);
					
					if(msgErroNota != null && msgErroNota.length() > 0 && msgErro.indexOf(msgErroNota) == -1){
						msgErro.append(msgErroNota + "\n");
					}
				}
			}
		}
		
		return new JsonModelAndView().addObject("msgErro", msgErro.toString());
	}
	
	public String validaInformacaoNota(ImportarXMLEstadoBean nota){
		nota.setMensagemerro(null);
		nota.setInformacaocompleta(true);
		
		StringBuilder msgErroNota = new StringBuilder(); 
		if(SinedUtil.isListNotEmpty(nota.getListaItens())){
			for(ImportarXMLEstadoItemBean item : nota.getListaItens()){
				if(item.getMaterial() == null && msgErroNota.indexOf("Existe(m) material(is) que n�o foi(ram) associado(s), associe todos para continuar!") == -1){
					msgErroNota.append("   Existe(m) material(is) que n�o foi(ram) associado(s), associe todos para continuar!\n");
				}
			}
		}
		if(nota.getRateio() != null && SinedUtil.isListNotEmpty(nota.getRateio().getListaRateioitem())){
			Double percentualTotal = 0d;
			for(Rateioitem rateioitem : nota.getRateio().getListaRateioitem()){
				if(rateioitem.getContagerencial() == null && msgErroNota.indexOf("O campo Conta gerencial � obrigat�rio") == -1){
					msgErroNota.append("   O campo Conta gerencial � obrigat�rio.\n");
				}
				if(rateioitem.getCentrocusto() == null && msgErroNota.indexOf("O campo Centro de custo � obrigat�rio") == -1){
					msgErroNota.append("   O campo Centro de custo � obrigat�rio.\n");
				}
				if(rateioitem.getPercentual() == null){
					if(msgErroNota.indexOf("O campo Percentual � obrigat�rio") == -1){
						msgErroNota.append("   O campo Percentual � obrigat�rio.\n");
					}
				}else {
					percentualTotal += rateioitem.getPercentual();
				}
			}
			if(percentualTotal != 100d && msgErroNota.indexOf("O somat�rio dos percentuais tem que ser 100") == -1){
				msgErroNota.append("   O somat�rio dos percentuais tem que ser 100%.\n");
			}
		}
		if(msgErroNota.length() > 0){
			nota.setMensagemerro("Nota: " + nota.getNotaXML().getnNF() + "\n" + msgErroNota + "\n");
			nota.setInformacaocompleta(false);
		}
		return nota.getMensagemerro();
	}
}
