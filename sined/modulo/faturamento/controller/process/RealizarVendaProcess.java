package br.com.linkcom.sined.modulo.faturamento.controller.process;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/faturamento/process/RealizarVenda", authorizationModule=ProcessAuthorizationModule.class)
public class RealizarVendaProcess extends MultiActionController {

	
	private ParametrogeralService parametrogeralService;
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	/**
	 * Carrega a pagina com algumas informa��es padr�o.
	 *
	 * @see br.com.linkcom.sined.geral.service.UsuarioempresaService#carregaEmpresa
	 * @param request
	 * @param venda 
	 * @return
	 * @author Ramon Brazil
	 */
	@DefaultAction
	public ModelAndView index(WebRequestContext request, Venda venda){
		
		String parametros = "";
		
		String cdpedidovenda = request.getParameter("cdpedidovenda");
		if(cdpedidovenda != null && !cdpedidovenda.equals("")){
			parametros += "&cdpedidovenda=" + cdpedidovenda + "&frompedidovenda=true";
			parametros +="&boquearIdentificador="+parametrogeralService.getValorPorNome(Parametrogeral.PEDIDOVENDA_IDENTIFICADOR_EXTERNO);
			String aprovarPedidovenda = request.getParameter("aprovarPedidovenda");
			if(aprovarPedidovenda != null && aprovarPedidovenda.equals("true")){
				parametros += "&aprovarPedidovenda=true";
			}
			
			String percentualPedidovenda = request.getParameter("percentualPedidovenda");
			if(percentualPedidovenda != null && !percentualPedidovenda.equals("")){
				parametros += "&percentualPedidovenda=" + percentualPedidovenda;
			}
			
			String somenteservico = request.getParameter("somenteservico");
			if(somenteservico != null && !somenteservico.equals("")){
				parametros += "&somenteservico=" + somenteservico;
			}
			
			String somenteproduto = request.getParameter("somenteproduto");
			if(somenteproduto != null && !somenteproduto.equals("")){
				parametros += "&somenteproduto=" + somenteproduto;
			}
		}
		
		String cdvendaorcamento = request.getParameter("cdvendaorcamento");
		if(cdvendaorcamento != null && !cdvendaorcamento.equals("")){
			parametros += "&cdvendaorcamento=" + cdvendaorcamento;
		}
		String cliente = request.getParameter("cliente");
		if(cliente != null && !cliente.isEmpty() && !"<null>".equalsIgnoreCase(cliente)) {
			String cdcliente = cliente.substring(cliente.indexOf("=")+1, cliente.indexOf(","));
			parametros+= "&cdcliente=" + cdcliente;
		}
		request.setAttribute("ignoreHackAndroidDownload", true);
		
		Boolean closeOnSave = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("closeOnSave")) ? request.getParameter("closeOnSave") : "FALSE");
		if (closeOnSave != null) {
			parametros+= "&closeOnSave="+closeOnSave;
		}
		
		Boolean showListagemLink = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("showListagemLink")) ? request.getParameter("showListagemLink") : "TRUE");
		if (showListagemLink != null) {
			parametros+= "&showListagemLink="+showListagemLink;
		}
		
		Boolean closeOnCancel = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("closeOnCancel")) ? request.getParameter("closeOnCancel") : "FALSE");
		if (closeOnCancel != null) {
			parametros+= "&closeOnCancel="+closeOnCancel;
		}
		
		
		Boolean emitirComprovante = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("emitirComprovante")) ? request.getParameter("emitirComprovante") : "TRUE");
		if (emitirComprovante != null) {
			parametros += "&emitirComprovante=" + emitirComprovante;
		}
		
		String complementoOtr = SinedUtil.complementoOtr();
		SinedUtil.redirecionamento(request, "/faturamento/crud/Venda"+complementoOtr+"?ACAO=criar" + parametros);
		return null;
	}
}