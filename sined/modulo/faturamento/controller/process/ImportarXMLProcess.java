package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.lknfe.xml.nfe.importacao.ImportarXML;
import br.com.linkcom.lknfe.xml.nfe.importacao.NotaItemXML;
import br.com.linkcom.lknfe.xml.nfe.importacao.NotaXML;
import br.com.linkcom.lknfe.xml.nfe.importacao.XMLUtil;
import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivonf;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Codigocnae;
import br.com.linkcom.sined.geral.bean.Codigotributacao;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Itemlistaservico;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoItem;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoRPS;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Regimetributacao;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ArquivonfService;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.CodigocnaeService;
import br.com.linkcom.sined.geral.service.CodigotributacaoService;
import br.com.linkcom.sined.geral.service.ConfiguracaonfeService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.ItemlistaservicoService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaoService;
import br.com.linkcom.sined.geral.service.NotaDocumentoService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoItemService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoRPSService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotaHistoricoService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.geral.service.RegimetributacaoService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.filter.ImportarXMLFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.ZipManipulation;

@Controller(path="/faturamento/process/ImportarXML", authorizationModule=ProcessAuthorizationModule.class)
public class ImportarXMLProcess extends MultiActionController{

	private MunicipioService municipioService;
	private PessoaService pessoaService;
	private ClienteService clienteService;
	private EnderecoService enderecoService;
	private EmpresaService empresaService;
	private NotaHistoricoService notaHistoricoService;
	private ArquivonfService arquivonfService;
	private NotaFiscalServicoItemService notafiscalServicoItemService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private ArquivoService arquivoService;
	private ItemlistaservicoService itemlistaservicoService;
	private CodigotributacaoService codigotributacaoService;
	private ArquivonfnotaService arquivonfnotaService;
	private ArquivoDAO arquivoDAO;
	private ConfiguracaonfeService configuracaonfeService;
	private ParametrogeralService parametrogeralService;
	private DocumentoService documentoService;
	private NotaDocumentoService notaDocumentoService;
	private NotaService notaService;
	private CodigocnaeService codigocnaeService;
	private RegimetributacaoService regimetributacaoService;
	private NaturezaoperacaoService naturezaoperacaoService;
	private NotaFiscalServicoRPSService notaFiscalServicoRPSService;
	
	public void setNotaFiscalServicoRPSService(NotaFiscalServicoRPSService notaFiscalServicoRPSService) {this.notaFiscalServicoRPSService = notaFiscalServicoRPSService;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {this.configuracaonfeService = configuracaonfeService;}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {this.arquivonfnotaService = arquivonfnotaService;}
	public void setArquivonfService(ArquivonfService arquivonfService) {this.arquivonfService = arquivonfService;}
	public void setCodigotributacaoService(CodigotributacaoService codigotributacaoService) {this.codigotributacaoService = codigotributacaoService;}
	public void setItemlistaservicoService(ItemlistaservicoService itemlistaservicoService) {this.itemlistaservicoService = itemlistaservicoService;}
	public void setNotaHistoricoService(NotaHistoricoService notaHistoricoService) {this.notaHistoricoService = notaHistoricoService;}
	public void setMunicipioService(MunicipioService municipioService) {this.municipioService = municipioService;}
	public void setPessoaService(PessoaService pessoaService) {this.pessoaService = pessoaService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setNotafiscalServicoItemService(NotaFiscalServicoItemService notafiscalServicoItemService) {this.notafiscalServicoItemService = notafiscalServicoItemService;}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setCodigocnaeService(CodigocnaeService codigocnaeService) {this.codigocnaeService = codigocnaeService;}
	public void setRegimetributacaoService(RegimetributacaoService regimetributacaoService) {this.regimetributacaoService = regimetributacaoService;}
	public void setNaturezaoperacaoService(NaturezaoperacaoService naturezaoperacaoService) {this.naturezaoperacaoService = naturezaoperacaoService;}
	
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, ImportarXMLFiltro filtro){
		return new ModelAndView("/process/importarXML", "filtro", filtro);
	}
	
	/**
	 * Verifica se o arquivo � .zip ou .xml e executa a importa�ao da nota fiscal de servi�o eletr�nica.
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public ModelAndView importar(WebRequestContext request, ImportarXMLFiltro filtro) throws Exception{
		Configuracaonfe configuracaonfe = configuracaonfeService.load(filtro.getConfiguracaonfe(), "configuracaonfe.cdconfiguracaonfe, configuracaonfe.prefixowebservice");
		filtro.setConfiguracaonfe(configuracaonfe);
		Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
		
		if(prefixowebservice.equals(Prefixowebservice.BRUMADINHOISS_PROD) || 
				prefixowebservice.equals(Prefixowebservice.IGARAPEISS_PROD)){
			return this.processaInfoBrumadinho(request, filtro);
		}
		
		if(prefixowebservice.equals(Prefixowebservice.VINHEDOISS_PROD)){
			return this.processaInfoVinhedo(request, filtro);
		}
		
		if(prefixowebservice.equals(Prefixowebservice.SANTANADEPARNAIBA_PROD)){
			return this.processaInfoSantanadeparnaiba(request, filtro);
		}
		
		if(prefixowebservice.equals(Prefixowebservice.VOTORANTIMISS_PROD) || 
				prefixowebservice.equals(Prefixowebservice.MOGIMIRIMISS_PROD)){
			return this.processaInfoGeisweb(request, filtro);
		}
		
		if(prefixowebservice.equals(Prefixowebservice.VOTORANTIMISS_NOVO_PROD) ||
				prefixowebservice.equals(Prefixowebservice.VOTORANTIMISS_NOVO_HOM)) {
			return this.processaInfoAssessorPublico(request, filtro);
		}
		
		if (prefixowebservice.equals(Prefixowebservice.AVARE_NOVO_HOM) ||
				prefixowebservice.equals(Prefixowebservice.AVARE_NOVO_PROD)) {
			return this.processaInfoSigBancos(request, filtro);
		}
		
		try{
			if(filtro.getArquivo().getName().toLowerCase().endsWith(".zip")){
				Arquivo arquivoZip = filtro.getArquivo();
				ZipManipulation zip = new ZipManipulation(arquivoZip);
				zip.readEntries();
				
				for(String filename : zip.getEntriesNames()){
					Arquivo arquivoXML = new Arquivo(zip.getBytesFromEntry(filename), filename, "zip");
					try {
						importarArquivo(request, arquivoXML, filtro, prefixowebservice);
					} catch (Exception e) {
						e.printStackTrace();
						request.addError("N�o foi poss�vel importar a nota: " + filename);
					}
				}
			}else if(filtro.getArquivo().getName().toLowerCase().endsWith(".xml")){
				Arquivo arquivoXML = filtro.getArquivo();
				importarArquivo(request, arquivoXML, filtro, prefixowebservice);
			}else{
				throw new SinedException("Somente arquivos com a extens�o .zip ou .xml s�o aceitos.");
			}
		} catch (Exception e) {
			request.addError("Problemas na importa��o de notas.");
			request.addError(e.getMessage());
			e.printStackTrace();
		}
		
		filtro.setArquivo(null);
		return new ModelAndView("/process/importarXML", "filtro", filtro);
	}
	
	private ModelAndView processaInfoSigBancos(WebRequestContext request,	ImportarXMLFiltro filtro) {
		try{
			List<NotaXML> listaNotaXML = new ArrayList<NotaXML>();
			
			Arquivo arquivoXML = filtro.getArquivo();
			if (arquivoXML.getName().endsWith(".xml")) {
				List<NotaXML> listaNotaXMLAntiga = this.preencheBeanSigBancosArquivo(arquivoXML);
				
				for (NotaXML notaxml : listaNotaXMLAntiga) {
					if(!arquivonfnotaService.haveNotaEmitida(notaxml.getNumeronfse(), filtro.getConfiguracaonfe(), notaxml.getCnpjempresa())){
						listaNotaXML.add(notaxml);
					}
				}
			} else {
				throw new SinedException("Somente arquivos com a extens�o .xml s�o aceitos.");
			}
			
			Collections.sort(listaNotaXML,new Comparator<NotaXML>(){
				public int compare(NotaXML o1, NotaXML o2) {
					String str1 = StringUtils.stringCheia(o1.getNumeronfse(), "0", 10, false);
					String str2 = StringUtils.stringCheia(o2.getNumeronfse(), "0", 10, false);
					return str1.compareTo(str2);
				}
			});
			
			Configuracaonfe configuracaonfe = configuracaonfeService.loadForEntrada(filtro.getConfiguracaonfe());
			List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForConferenciaNfseSigBancos(configuracaonfe.getEmpresa());
			for (NotaFiscalServico nf : listaNota) {
				String numero = nf.getNumero();
				for (NotaXML xml : listaNotaXML) {
					if(!xml.isConflito()){
						String numeronfse = xml.getNumeronfse();
						if(numeronfse.trim().equals(numero)){
							if(xml.getCdnota() != null){
								xml.setConflito(true);
								xml.setCdnota(null);
							} else {
								xml.setCdnota(nf.getCdNota());
							}
						}
					}
				}
			}
			
			List<NotaXML> listaEncontrada = new ArrayList<NotaXML>();
			List<NotaXML> listaNaoEncontrada = new ArrayList<NotaXML>();
			
			for (NotaXML xml : listaNotaXML) {
				if(xml.getCdnota() == null){
					listaNaoEncontrada.add(xml);
				} else {
					listaEncontrada.add(xml);
				}
			}
			
			filtro.setListaEncontrada(listaEncontrada);
			filtro.setListaNaoEncontrada(listaNaoEncontrada);
			
			arquivoService.save(filtro.getArquivo(), null);
			
			if(listaNaoEncontrada != null && listaNaoEncontrada.size() > 0){
				request.setAttribute("cdempresa", configuracaonfe.getEmpresa().getCdpessoa());
				return new ModelAndView("/process/conferenciaNotaXMLSigBancos", "filtro", filtro);
			}
			
			return continueOnAction("saveConferenciaNotaXMLAssessorPublico", filtro);
		} catch (Exception e) {
			request.addError("Problemas na importa��o de notas.");
			request.addError(e.getMessage());
			e.printStackTrace();
			
			filtro.setArquivo(null);
			return new ModelAndView("/process/importarXML", "filtro", filtro);
		}
	}
	
	private List<NotaXML> preencheBeanSigBancosArquivo(Arquivo arquivo) throws Exception {
		String stringXml = new String(arquivo.getContent(), "UTF-8");
		return new ImportarXML().preencheSigBancos(stringXml);
	}
	
	private ModelAndView processaInfoAssessorPublico(WebRequestContext request,	ImportarXMLFiltro filtro) throws Exception {
		try{
			List<NotaXML> listaNotaXML = new ArrayList<NotaXML>();
			
			Arquivo arquivoXML = filtro.getArquivo();
			if (arquivoXML.getName().endsWith(".xml")) {
				List<NotaXML> listaNotaXMLAntiga = this.preencheBeanAssessorPublicoArquivo(arquivoXML);
				
				for (NotaXML notaxml : listaNotaXMLAntiga) {
					if(!arquivonfnotaService.haveNotaEmitida(notaxml.getNumeronfse(), filtro.getConfiguracaonfe(), notaxml.getCnpjempresa())){
						listaNotaXML.add(notaxml);
					}
				}
			} else {
				throw new SinedException("Somente arquivos com a extens�o .xml s�o aceitos.");
			}
			
			Collections.sort(listaNotaXML,new Comparator<NotaXML>(){
				public int compare(NotaXML o1, NotaXML o2) {
					String str1 = StringUtils.stringCheia(o1.getNumeronfse(), "0", 10, false);
					String str2 = StringUtils.stringCheia(o2.getNumeronfse(), "0", 10, false);
					return str1.compareTo(str2);
				}
			});
			
			Configuracaonfe configuracaonfe = configuracaonfeService.loadForEntrada(filtro.getConfiguracaonfe());
			List<NotaFiscalServicoRPS> listaNota = notaFiscalServicoRPSService.findForConferenciaNfseGeisweb(configuracaonfe.getEmpresa());
			for (NotaFiscalServicoRPS nf : listaNota) {
				Integer numero = nf.getNumero();
				for (NotaXML xml : listaNotaXML) {
					if(!xml.isConflito()){
						String numeronfse = xml.getNumeronfse();
						if(numeronfse.trim().equals(numero.toString())){
							if(xml.getCdnota() != null){
								xml.setConflito(true);
								xml.setCdnota(null);
							} else {
								xml.setCdnota(nf.getNotaFiscalServico().getCdNota());
							}
						}
					}
				}
			}
			
			List<NotaXML> listaEncontrada = new ArrayList<NotaXML>();
			List<NotaXML> listaNaoEncontrada = new ArrayList<NotaXML>();
			
			for (NotaXML xml : listaNotaXML) {
				if(xml.getCdnota() == null){
					listaNaoEncontrada.add(xml);
				} else {
					listaEncontrada.add(xml);
				}
			}
			
			filtro.setListaEncontrada(listaEncontrada);
			filtro.setListaNaoEncontrada(listaNaoEncontrada);
			
			arquivoService.save(filtro.getArquivo(), null);
			
			if(listaNaoEncontrada != null && listaNaoEncontrada.size() > 0){
				request.setAttribute("cdempresa", configuracaonfe.getEmpresa().getCdpessoa());
				return new ModelAndView("/process/conferenciaNotaXMLAssessorPublico", "filtro", filtro);
			}
			
			return continueOnAction("saveConferenciaNotaXMLAssessorPublico", filtro);
		} catch (Exception e) {
			request.addError("Problemas na importa��o de notas.");
			request.addError(e.getMessage());
			e.printStackTrace();
			
			filtro.setArquivo(null);
			return new ModelAndView("/process/importarXML", "filtro", filtro);
		}
	}
	
	private List<NotaXML> preencheBeanAssessorPublicoArquivo(Arquivo arquivo) throws Exception {
		String stringXml = new String(arquivo.getContent(), "UTF-8");
		return new ImportarXML().preencheAssessorPublico(stringXml);
	}
	
	private ModelAndView processaInfoGeisweb(WebRequestContext request, ImportarXMLFiltro filtro) {
		try{
			List<NotaXML> listaNotaXML = new ArrayList<NotaXML>();
			
			Arquivo arquivoXML = filtro.getArquivo();
			if(arquivoXML.getName().endsWith(".xml")){
				List<NotaXML> listaNotaXMLAntiga = this.preencheBeanGeiswebArquivo(arquivoXML);
				
				for (NotaXML notaxml : listaNotaXMLAntiga) {
					if(!arquivonfnotaService.haveNotaEmitida(notaxml.getNumeronfse(), notaxml.getCodigoverificacao())){
						listaNotaXML.add(notaxml);
					}
				}
			} else{
				throw new SinedException("Somente arquivos com a extens�o .xml s�o aceitos.");
			}
			
			Collections.sort(listaNotaXML,new Comparator<NotaXML>(){
				public int compare(NotaXML o1, NotaXML o2) {
					String str1 = StringUtils.stringCheia(o1.getNumeronfse(), "0", 10, false);
					String str2 = StringUtils.stringCheia(o2.getNumeronfse(), "0", 10, false);
					return str1.compareTo(str2);
				}
			});
			
			Configuracaonfe configuracaonfe = configuracaonfeService.loadForEntrada(filtro.getConfiguracaonfe());
			List<NotaFiscalServicoRPS> listaNota = notaFiscalServicoRPSService.findForConferenciaNfseGeisweb(configuracaonfe.getEmpresa());
			for (NotaFiscalServicoRPS nf : listaNota) {
				Integer numero = nf.getNumero();
				for (NotaXML xml : listaNotaXML) {
					if(!xml.isConflito()){
						String numeronfse = xml.getNumeronfse();
						if(numeronfse.trim().equals(numero.toString())){
							if(xml.getCdnota() != null){
								xml.setConflito(true);
								xml.setCdnota(null);
							} else {
								xml.setCdnota(nf.getNotaFiscalServico().getCdNota());
							}
						}
					}
				}
			}
			
			List<NotaXML> listaEncontrada = new ArrayList<NotaXML>();
			List<NotaXML> listaNaoEncontrada = new ArrayList<NotaXML>();
			
			for (NotaXML xml : listaNotaXML) {
				if(xml.getCdnota() == null){
					listaNaoEncontrada.add(xml);
				} else {
					listaEncontrada.add(xml);
				}
			}
			
			filtro.setListaEncontrada(listaEncontrada);
			filtro.setListaNaoEncontrada(listaNaoEncontrada);
			
			arquivoService.save(filtro.getArquivo(), null);
			
			if(listaNaoEncontrada != null && listaNaoEncontrada.size() > 0){
				request.setAttribute("cdempresa", configuracaonfe.getEmpresa().getCdpessoa());
				return new ModelAndView("/process/conferenciaNotaXMLGeisweb", "filtro", filtro);
			}
			
			return continueOnAction("saveConferenciaNotaXMLGeisweb", filtro);
		} catch (Exception e) {
			request.addError("Problemas na importa��o de notas.");
			request.addError(e.getMessage());
			e.printStackTrace();
			
			filtro.setArquivo(null);
			return new ModelAndView("/process/importarXML", "filtro", filtro);
		}
	}
	
	private ModelAndView processaInfoSantanadeparnaiba(WebRequestContext request, ImportarXMLFiltro filtro) {
		try{
			List<NotaXML> listaNotaXML = new ArrayList<NotaXML>();
			
			if(filtro.getArquivo().getName().endsWith(".txt")){
				Arquivo arquivoXML = filtro.getArquivo();
				List<NotaXML> listaNotaXMLAntiga = this.preencheBeanSantanadeparnaibaArquivo(arquivoXML);
				
				for (NotaXML notaxml : listaNotaXMLAntiga) {
					if(!arquivonfnotaService.haveNotaEmitida(notaxml.getNumeronfse(), null)){
						Codigotributacao cod = codigotributacaoService.findByCodigo(notaxml.getCodigotributacaomunicipio().trim());
						if(cod != null){
							notaxml.setDescricao(cod.getDescricao());
						}
						
						listaNotaXML.add(notaxml);
					}
				}
			} else{
				throw new SinedException("Somente arquivos com a extens�o .txt s�o aceitos.");
			}
			
			List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForConferenciaNfse();
			for (NotaFiscalServico nf : listaNota) {
				for (NotaXML xml : listaNotaXML) {
					if(nf.getCdNota().toString().equals(xml.getNumeroNota().trim())){
						xml.setCdnota(nf.getCdNota());
					}
				}
			}
			
			List<NotaXML> listaEncontrada = new ArrayList<NotaXML>();
			List<NotaXML> listaNaoEncontrada = new ArrayList<NotaXML>();
			
			for (NotaXML xml : listaNotaXML) {
				if(xml.getCdnota() == null){
					listaNaoEncontrada.add(xml);
				} else {
					listaEncontrada.add(xml);
				}
			}
			
			filtro.setListaEncontrada(listaEncontrada);
			filtro.setListaNaoEncontrada(listaNaoEncontrada);
			
			if(listaNaoEncontrada != null && listaNaoEncontrada.size() > 0){
				return new ModelAndView("/process/conferenciaNotaXMLBrumadinho", "filtro", filtro);
			}
			
			return continueOnAction("saveConferenciaNotaXMLBrumadinho", filtro);
		} catch (Exception e) {
			request.addError("Problemas na importa��o de notas.");
			request.addError(e.getMessage());
			e.printStackTrace();
			
			filtro.setArquivo(null);
			return new ModelAndView("/process/importarXML", "filtro", filtro);
		}
	}
	
	private ModelAndView processaInfoVinhedo(WebRequestContext request, ImportarXMLFiltro filtro){
		try{
			List<NotaXML> listaNotaXML = new ArrayList<NotaXML>();
			
			if(filtro.getArquivo().getName().endsWith(".xml")){
				Arquivo arquivoXML = filtro.getArquivo();
				List<NotaXML> listaNotaXMLAntiga = this.preencheBeanVinhedoArquivo(arquivoXML);
				
				for (NotaXML notaxml : listaNotaXMLAntiga) {
					if(!arquivonfnotaService.haveNotaEmitida(notaxml.getNumeronfse(), notaxml.getCodigoverificacao())){
						listaNotaXML.add(notaxml);
					}
				}
			} else{
				throw new SinedException("Somente arquivos com a extens�o .xml s�o aceitos.");
			}
			
			Collections.sort(listaNotaXML,new Comparator<NotaXML>(){
				public int compare(NotaXML o1, NotaXML o2) {
					String str1 = StringUtils.stringCheia(o1.getNumeronfse(), "0", 10, false);
					String str2 = StringUtils.stringCheia(o2.getNumeronfse(), "0", 10, false);
					return str1.compareTo(str2);
				}
			});
			
			List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForConferenciaNfse();
			for (NotaFiscalServico nf : listaNota) {
				Money valorTotalServicos = nf.getValorTotalServicos();
				Cliente cliente = nf.getCliente();
				String cpfcnpj = cliente.getCpfOuCnpj();
				if(cpfcnpj != null){
					cpfcnpj = StringUtils.soNumero(cpfcnpj);
				}
				
				for (NotaXML xml : listaNotaXML) {
					if(!xml.isConflito()){
						String cnpj = xml.getCnpj();
						String cpf = xml.getCpf();
						BigDecimal valorservicos = xml.getValorservicos();
						
						
						boolean cnpjIgual = cnpj != null && cnpj.equals(cpfcnpj);
						boolean cpfIgual = cpf != null && cpf.equals(cpfcnpj);
						boolean valorIgual = valorservicos != null && valorservicos.doubleValue() == valorTotalServicos.getValue().doubleValue();
						
						if((cpfIgual || cnpjIgual) && valorIgual){
							if(xml.getCdnota() != null){
								xml.setConflito(true);
								xml.setCdnota(null);
							} else {
								xml.setCdnota(nf.getCdNota());
							}
						}
					}
				}
			}
			
			List<NotaXML> listaEncontrada = new ArrayList<NotaXML>();
			List<NotaXML> listaNaoEncontrada = new ArrayList<NotaXML>();
			
			for (NotaXML xml : listaNotaXML) {
				if(xml.getCdnota() == null){
					listaNaoEncontrada.add(xml);
				} else {
					listaEncontrada.add(xml);
				}
			}
			
			filtro.setListaEncontrada(listaEncontrada);
			filtro.setListaNaoEncontrada(listaNaoEncontrada);
			
			if(listaNaoEncontrada != null && listaNaoEncontrada.size() > 0){
				return new ModelAndView("/process/conferenciaNotaXMLBrumadinho", "filtro", filtro);
			}
			
			return continueOnAction("saveConferenciaNotaXMLBrumadinho", filtro);
		} catch (Exception e) {
			request.addError("Problemas na importa��o de notas.");
			request.addError(e.getMessage());
			e.printStackTrace();
			
			filtro.setArquivo(null);
			return new ModelAndView("/process/importarXML", "filtro", filtro);
		}
	}
	
	private ModelAndView processaInfoBrumadinho(WebRequestContext request, ImportarXMLFiltro filtro) {
		try{
			List<NotaXML> listaNotaXML = new ArrayList<NotaXML>();
			
			if(filtro.getArquivo().getName().endsWith(".zip")){
				Arquivo arquivoZip = filtro.getArquivo();
				ZipManipulation zip = new ZipManipulation(arquivoZip);
				zip.readEntries();
				
				for(String filename : zip.getEntriesNames()){
					Arquivo arquivoXML = new Arquivo(zip.getBytesFromEntry(filename), filename, "zip");
					try {
						NotaXML notaxml = this.preencheBeanBrumadinhoArquivo(arquivoXML);
						
						if(!arquivonfnotaService.haveNotaEmitida(notaxml.getNumeronfse(), notaxml.getCodigoverificacao())){
							listaNotaXML.add(notaxml);
						}
					} catch (Exception e) {
						e.printStackTrace();
						request.addError("N�o foi poss�vel importar a nota: " + filename);
					}
				}
			} else if(filtro.getArquivo().getName().endsWith(".xml")){
				Arquivo arquivoXML = filtro.getArquivo();
				NotaXML notaxml = this.preencheBeanBrumadinhoArquivo(arquivoXML);
				
				if(!arquivonfnotaService.haveNotaEmitida(notaxml.getNumeronfse(), notaxml.getCodigoverificacao())){
					listaNotaXML.add(notaxml);
				}
			} else{
				throw new SinedException("Somente arquivos com a extens�o .zip ou .xml s�o aceitos.");
			}
			
			Collections.sort(listaNotaXML,new Comparator<NotaXML>(){
				public int compare(NotaXML o1, NotaXML o2) {
					String str1 = StringUtils.stringCheia(o1.getNumeronfse(), "0", 10, false);
					String str2 = StringUtils.stringCheia(o2.getNumeronfse(), "0", 10, false);
					return str1.compareTo(str2);
				}
			});
			
			List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForConferenciaNfse();
			for (NotaFiscalServico nf : listaNota) {
				Money valorTotalServicos = nf.getValorTotalServicos();
				Cliente cliente = nf.getCliente();
				String cpfcnpj = cliente.getCpfOuCnpj();
				if(cpfcnpj != null){
					cpfcnpj = StringUtils.soNumero(cpfcnpj);
				}
				
				for (NotaXML xml : listaNotaXML) {
					if(!xml.isConflito()){
						String cnpj = xml.getCnpj();
						String cpf = xml.getCpf();
						BigDecimal valorservicos = xml.getValorservicos();
						
						
						boolean cnpjIgual = cnpj != null && cnpj.equals(cpfcnpj);
						boolean cpfIgual = cpf != null && cpf.equals(cpfcnpj);
						boolean valorIgual = valorservicos != null && valorservicos.doubleValue() == valorTotalServicos.getValue().doubleValue();
						
						if((cpfIgual || cnpjIgual) && valorIgual){
							if(xml.getCdnota() != null){
								xml.setConflito(true);
								xml.setCdnota(null);
							} else {
								xml.setCdnota(nf.getCdNota());
							}
						}
					}
				}
			}
			
			List<NotaXML> listaEncontrada = new ArrayList<NotaXML>();
			List<NotaXML> listaNaoEncontrada = new ArrayList<NotaXML>();
			
			for (NotaXML xml : listaNotaXML) {
				if(xml.getCdnota() == null){
					listaNaoEncontrada.add(xml);
				} else {
					listaEncontrada.add(xml);
				}
			}
			
			filtro.setListaEncontrada(listaEncontrada);
			filtro.setListaNaoEncontrada(listaNaoEncontrada);
			
			if(listaNaoEncontrada != null && listaNaoEncontrada.size() > 0){
				return new ModelAndView("/process/conferenciaNotaXMLBrumadinho", "filtro", filtro);
			}
			
			return continueOnAction("saveConferenciaNotaXMLBrumadinho", filtro);
		} catch (Exception e) {
			request.addError("Problemas na importa��o de notas.");
			request.addError(e.getMessage());
			e.printStackTrace();
			
			filtro.setArquivo(null);
			return new ModelAndView("/process/importarXML", "filtro", filtro);
		}
	}
	
	public ModelAndView selecionarNotaConferenciaBrumadinho(WebRequestContext request, NotaXML notaXML){
		request.setAttribute("listaNota", notaFiscalServicoService.findForConferenciaNfse());
		request.setAttribute("indexNF", request.getParameter("index"));
		request.setAttribute("cliente", notaXML.getNome());
		request.setAttribute("descricaoservicos", notaXML.getDescricao());
		request.setAttribute("valorservicos", "R$ " + SinedUtil.descriptionDecimal(notaXML.getValorservicos().doubleValue(), true));
		return new ModelAndView("direct:/process/popup/selecionarNotaConferenciaBrumadinho");
	}
	
	public ModelAndView selecionarNotaConferenciaGeisweb(WebRequestContext request, NotaXML notaXML){
		request.setAttribute("listaNota", notaFiscalServicoRPSService.findForConferenciaNfseGeisweb(new Empresa(Integer.parseInt(request.getParameter("cdempresa")))));
		request.setAttribute("indexNF", request.getParameter("index"));
		request.setAttribute("cliente", notaXML.getNome());
		request.setAttribute("descricaoservicos", notaXML.getDescricao());
		request.setAttribute("valorservicos", "R$ " + SinedUtil.descriptionDecimal(notaXML.getValorservicos().doubleValue(), true));
		return new ModelAndView("direct:/process/popup/selecionarNotaConferenciaGeisweb");
	}
	
	public ModelAndView selecionarNotaConferenciaAssessorPublico(WebRequestContext request, NotaXML notaXML){
		request.setAttribute("listaNota", notaFiscalServicoRPSService.findForConferenciaNfseGeisweb(new Empresa(Integer.parseInt(request.getParameter("cdempresa")))));
		request.setAttribute("indexNF", request.getParameter("index"));
		request.setAttribute("cliente", notaXML.getNome());
		request.setAttribute("descricaoservicos", notaXML.getDescricao());
		request.setAttribute("valorservicos", "R$ " + SinedUtil.descriptionDecimal(notaXML.getValorservicos().doubleValue(), true));
		return new ModelAndView("direct:/process/popup/selecionarNotaConferenciaAssessorPublico");
	}
	
	public ModelAndView selecionarNotaConferenciaSigBancos(WebRequestContext request, NotaXML notaXML){
		request.setAttribute("listaNota", notaFiscalServicoService.findForConferenciaNfseSigBancos(new Empresa(Integer.parseInt(request.getParameter("cdempresa")))));
		request.setAttribute("indexNF", request.getParameter("index"));
		request.setAttribute("cliente", notaXML.getNome());
		request.setAttribute("descricaoservicos", notaXML.getDescricao());
		request.setAttribute("valorservicos", "R$ " + SinedUtil.descriptionDecimal(notaXML.getValorservicos().doubleValue(), true));
		return new ModelAndView("direct:/process/popup/selecionarNotaConferenciaSigBancos");
	}
	
	public ModelAndView saveConferenciaNotaXMLAssessorPublico(WebRequestContext request, ImportarXMLFiltro filtro) throws Exception {
		boolean erro = false;
		
		Configuracaonfe configuracaonfe = configuracaonfeService.loadForEntrada(filtro.getConfiguracaonfe());
		
		Arquivonf arquivonf = new Arquivonf();
		arquivonf.setArquivoretornoconsulta(filtro.getArquivo());
		arquivonf.setArquivonfsituacao(Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
		arquivonf.setConfiguracaonfe(configuracaonfe);
		arquivonf.setEmpresa(configuracaonfe.getEmpresa());
		arquivonfService.saveOrUpdate(arquivonf);
		
		List<NotaXML> listaNaoEncontrada = filtro.getListaNaoEncontrada();
		List<NotaXML> listaEncontrada = filtro.getListaEncontrada();
		
		erro = this.processaConferenciaNotaXMLAssessorPublico(request, arquivonf, listaNaoEncontrada, erro, filtro);
		erro = this.processaConferenciaNotaXMLAssessorPublico(request, arquivonf, listaEncontrada, erro, filtro);
		
		if(erro){
			request.addMessage("Processo de atualiza��o da(s) nota(s) terminado com algumas pend�ncias.", MessageType.WARN);
		} else {
			request.addMessage("Processo de atualiza��o da(s) nota(s) terminado com sucesso.");
		}
		filtro.setArquivo(null);
		return new ModelAndView("/process/importarXML", "filtro", filtro);
	}
	
	public ModelAndView saveConferenciaNotaXMLGeisweb(WebRequestContext request, ImportarXMLFiltro filtro){
		boolean erro = false;
		
		Configuracaonfe configuracaonfe = configuracaonfeService.loadForEntrada(filtro.getConfiguracaonfe());
		
		Arquivonf arquivonf = new Arquivonf();
		arquivonf.setArquivoretornoconsulta(filtro.getArquivo());
		arquivonf.setArquivonfsituacao(Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
		arquivonf.setConfiguracaonfe(configuracaonfe);
		arquivonf.setEmpresa(configuracaonfe.getEmpresa());
		arquivonfService.saveOrUpdate(arquivonf);
		
		List<NotaXML> listaNaoEncontrada = filtro.getListaNaoEncontrada();
		List<NotaXML> listaEncontrada = filtro.getListaEncontrada();
		
		erro = this.processaConferenciaNotaXMLGeisweb(request, arquivonf, listaNaoEncontrada, erro);
		erro = this.processaConferenciaNotaXMLGeisweb(request, arquivonf, listaEncontrada, erro);
		
		if(erro){
			request.addMessage("Processo de atualiza��o da(s) nota(s) terminado com algumas pend�ncias.", MessageType.WARN);
		} else {
			request.addMessage("Processo de atualiza��o da(s) nota(s) terminado com sucesso.");
		}
		filtro.setArquivo(null);
		return new ModelAndView("/process/importarXML", "filtro", filtro);
	}
	
	private boolean processaConferenciaNotaXMLGeisweb(WebRequestContext request, Arquivonf arquivonf, List<NotaXML> lista, boolean erro) {
		if(lista != null){
			for (NotaXML notaXML : lista) {
				if(notaXML.getCdnota() != null){
					try {
						Nota nota = new Nota(notaXML.getCdnota());
						String numeroNfse = notaXML.getNumeronfse();
						String codigoVerificacao = notaXML.getCodigoverificacao();
						Timestamp dtemissao = notaXML.getDtemissao();
						
						Arquivonfnota arquivonfnota = new Arquivonfnota();
						arquivonfnota.setArquivonf(arquivonf);
						arquivonfnota.setCodigoverificacao(codigoVerificacao);
						arquivonfnota.setDtcompetencia(dtemissao);
						arquivonfnota.setDtemissao(dtemissao);
						arquivonfnota.setNota(nota);
						arquivonfnota.setNumeronfse(numeroNfse);
						arquivonfnotaService.saveOrUpdate(arquivonfnota);
						
						if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
							documentoService.updateDocumentoNumeroByNota(numeroNfse, nota);
						}
						
						if(notaDocumentoService.haveDocumentoNaoCanceladoByNota(nota) || notaService.isNotaPossuiReceita(nota)){
							nota.setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
						} else {
							nota.setNotaStatus(NotaStatus.NFSE_EMITIDA);
						}
						
						notaService.alterarStatusAcao(nota, "Arquivo NFS-e processado com sucesso.", null);
					} catch (Exception e) {
						e.printStackTrace();
						erro = true;
						request.addError("N�o foi poss�vel processar a nota com id " + notaXML.getCdnota() + ": " + e.getMessage());
					}
				}
			}
		}
		
		return erro;
	}
	
	private boolean processaConferenciaNotaXMLAssessorPublico(WebRequestContext request, Arquivonf arquivonf, List<NotaXML> lista, boolean erro, ImportarXMLFiltro filtro) throws Exception {
		if(lista != null){
			for (NotaXML notaXML : lista) {
				if(notaXML.getCdnota() != null){
					try {
						Nota nota = new Nota(notaXML.getCdnota());
						String numeroNfse = notaXML.getNumeronfse();
						String codigoVerificacao = notaXML.getCodigoverificacao();
						Timestamp dtemissao = notaXML.getDtemissao();
						
						Arquivonfnota arquivonfnota = new Arquivonfnota();
						arquivonfnota.setArquivonf(arquivonf);
						arquivonfnota.setCodigoverificacao(codigoVerificacao);
						arquivonfnota.setDtcompetencia(dtemissao);
						arquivonfnota.setDtemissao(dtemissao);
						arquivonfnota.setNota(nota);
						arquivonfnota.setNumeronfse(numeroNfse);
						arquivonfnotaService.saveOrUpdate(arquivonfnota);
						
						if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
							documentoService.updateDocumentoNumeroByNota(numeroNfse, nota);
						}
						
						if(notaDocumentoService.haveDocumentoNaoCanceladoByNota(nota) || notaService.isNotaPossuiReceita(nota)){
							nota.setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
						} else {
							nota.setNotaStatus(NotaStatus.NFSE_EMITIDA);
						}
						
						notaService.alterarStatusAcao(nota, "Arquivo NFS-e processado com sucesso.", null);
					} catch (Exception e) {
						e.printStackTrace();
						erro = true;
						request.addError("N�o foi poss�vel processar a nota com id " + notaXML.getCdnota() + ": " + e.getMessage());
					}
				} else {
					Arquivo arquivo = arquivoService.loadWithContents(arquivonf.getArquivoretornoconsulta());
					String stringXml = new String(arquivo.getContent(), "UTF-8");
					
					try {
						while(!stringXml.startsWith("<?xml") && stringXml.length() > 0){
							stringXml = stringXml.substring(1);
						}
						
						if(stringXml == null || stringXml.length() == 0){
							throw new Exception("XML inv�lido. N�o possui cabe�alho.");
						}
						
						SAXBuilder sb = new SAXBuilder();
						Document d = sb.build(new ByteArrayInputStream(stringXml.getBytes("ISO-8859-1")));
						Element nfse = d.getRootElement();
						
						if (nfse != null) {
							List<Element> listaNota = XMLUtil.getListChildElement("NOTA", nfse.getContent());
							
							if (listaNota != null && listaNota.size() > 0) {
								for (Element notaElement : listaNota) {
									Element codElement = XMLUtil.getChildElement("COD", notaElement.getContent());
									
									if (codElement != null && notaXML.getNumeronfse().equals(codElement.getText())) {
										NotaXML notaxml = new ImportarXML().preencheBeanAssessorPublico(notaElement);
										String arquivoStr = "<NFSE>" + new XMLOutputter().outputString(notaElement) + "</NFSE>";
										
										Resource resource = new Resource("application/xml", "nota" + codElement.getText() + ".xml", arquivoStr.getBytes());
										
										Arquivo arquivoNota = new Arquivo();
										arquivoNota.setNome(resource.getFileName());
										arquivoNota.setContent(resource.getContents());
										arquivoNota.setDtmodificacao(new Timestamp(System.currentTimeMillis()));
										arquivoNota.setTamanho(Long.parseLong("" + resource.getContents().length));
										arquivoNota.setTipoconteudo(resource.getContentType());
										
										erro = !this.saveNotaByXML(request, arquivoNota, filtro, notaxml);
									}
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
						erro = true;
						request.addError("Erros ao tentar salvar as notas.");
						request.addError(e.getMessage());
					}
				}
			}
		}
		
		return erro;
	}
	
	public ModelAndView saveConferenciaNotaXMLBrumadinho(WebRequestContext request, ImportarXMLFiltro filtro){
		
		List<Arquivonf> listaArquivonf = new ArrayList<Arquivonf>();
		List<NotaXML> listaNaoEncontrada = filtro.getListaNaoEncontrada();
		List<NotaXML> listaEncontrada = filtro.getListaEncontrada();
		
		boolean erro = false;
		
		erro = this.processaConferenciaNotaXMLBrumadinho(request, listaArquivonf, listaNaoEncontrada, erro);
		erro = this.processaConferenciaNotaXMLBrumadinho(request, listaArquivonf, listaEncontrada, erro);
		
		for (Arquivonf arquivonf : listaArquivonf) {
			arquivonfService.updateSituacao(arquivonf, Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
		}
		
		if(erro){
			request.addMessage("Processo de atualiza��o da(s) nota(s) terminado com algumas pend�ncias.", MessageType.WARN);
		} else {
			request.addMessage("Processo de atualiza��o da(s) nota(s) terminado com sucesso.");
		}
		filtro.setArquivo(null);
		return new ModelAndView("/process/importarXML", "filtro", filtro);
	}
	
	private boolean processaConferenciaNotaXMLBrumadinho(WebRequestContext request, List<Arquivonf> listaArquivonf, 
			List<NotaXML> lista, boolean erro) {
		
		if(lista != null){
			for (NotaXML notaXML : lista) {
				if(notaXML.getCdnota() != null){
					try{
						Nota nota = new Nota(notaXML.getCdnota());
						String numeroNfse = notaXML.getNumeronfse();
						String codigoVerificacao = notaXML.getCodigoverificacao();
						Timestamp dtemissao = notaXML.getDtemissao();
						Arquivonfnota arquivonfnota = arquivonfnotaService.findGeradaByNota(nota);
						
						if(arquivonfnota != null){
							if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
								documentoService.updateDocumentoNumeroByNota(numeroNfse, nota);
							}
							
							arquivonfnotaService.updateNumeroCodigo(arquivonfnota, numeroNfse, codigoVerificacao, dtemissao, new Date(dtemissao.getTime()));
							
							if(notaDocumentoService.haveDocumentoNaoCanceladoByNota(nota) || notaService.isNotaPossuiReceita(nota)){
								nota.setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
							} else {
								nota.setNotaStatus(NotaStatus.NFSE_EMITIDA);
							}
							notaService.alterarStatusAcao(nota, "Arquivo NFS-e processado com sucesso.", null);
							listaArquivonf.add(arquivonfnota.getArquivonf());
						} else {
							erro = true;
							request.addError("N�o foi poss�vel processar a nota com id " + notaXML.getCdnota());
						}
					} catch (Exception e) {
						e.printStackTrace();
						erro = true;
						request.addError("N�o foi poss�vel processar a nota com id " + notaXML.getCdnota() + ": " + e.getMessage());
					}
				}
			}
		}
		
		return erro;
	}
	
	private List<NotaXML> preencheBeanGeiswebArquivo(Arquivo arquivo) throws Exception {
		String stringXml = new String(arquivo.getContent(), "UTF-8");
		return new ImportarXML().preencheBeanGeisweb(stringXml);
	}
	
	private List<NotaXML> preencheBeanVinhedoArquivo(Arquivo arquivo) throws Exception {
		String stringXml = new String(arquivo.getContent(), "UTF-8");
		return new ImportarXML().preencheBeanVinhedo(stringXml);
	}
	
	private NotaXML preencheBeanBrumadinhoArquivo(Arquivo arquivo) throws Exception {
		String stringXml = new String(arquivo.getContent(), "UTF-8");
		return new ImportarXML().preencheBeanBrumadinho(stringXml);
	}
	
	private List<NotaXML> preencheBeanSantanadeparnaibaArquivo(Arquivo arquivo) throws Exception {
		String stringXml = new String(arquivo.getContent(), "UTF-8");
		return new ImportarXML().preencheBeanSantanadeparnaiba(stringXml);
	}
	
	/**
	 * Importa os dados de um arquivo de nota fiscal de servi�o eletr�nica em xml
	 * @param request
	 * @param arquivo
	 * @throws Exception
	 * @param filtro 
	 * @author Thiers Euller, Rodrigo Freitas
	 * @param prefixowebservice 
	 */
	public void importarArquivo(WebRequestContext request, Arquivo arquivo, ImportarXMLFiltro filtro, Prefixowebservice prefixowebservice) throws Exception {
		String stringXml = new String(arquivo.getContent(), "UTF-8");
		
		boolean sucesso = false;
		if(prefixowebservice.equals(Prefixowebservice.BETIMISS_HOM) || prefixowebservice.equals(Prefixowebservice.BETIMISS_PROD)){
			List<NotaXML> listaNotaxml = new ImportarXML().preencheBeanBetim(stringXml);
			for (NotaXML notaXML : listaNotaxml) {
				if(!sucesso){
					sucesso = this.saveNotaByXML(request, arquivo, filtro, notaXML);
				}
			}
		} else {
			NotaXML notaxml = new ImportarXML().preencheBean(stringXml);
			sucesso = this.saveNotaByXML(request, arquivo, filtro, notaxml);
		}
		if(sucesso){
			request.addMessage("Arquivo importados com sucesso: " + arquivo.getName());
		}
		
	}
	
	private boolean saveNotaByXML(WebRequestContext request, Arquivo arquivo, ImportarXMLFiltro filtro, NotaXML notaxml) throws UnsupportedEncodingException {
		Cnpj cnpjPrestador = new Cnpj(notaxml.getCnpjempresa());
        Empresa empresa = empresaService.findByCnpj(cnpjPrestador);	
		if(empresa == null){
			request.addError("Empresa n�o encontrada. - " + arquivo.getName() + " - " + cnpjPrestador);
			return false;
		}
		
		if(notaxml.getNumeroNota() != null && notaFiscalServicoService.isExisteNota(empresa, notaxml.getNumeroNota())){
			request.addError("Nota j� cadastrada. - " + arquivo.getName() + " - " + notaxml.getNumeroNota());
			return false;
		}
		if(notaxml.getNumeronfse() != null && notaFiscalServicoService.isExisteNotaByNumeronfse(empresa, notaxml.getNumeronfse(), filtro.getConfiguracaonfe())){
			request.addError("NFS-e j� cadastrada. - " + arquivo.getName() + " - " + notaxml.getNumeronfse());
			return false;
		}
			
		Cliente cliente = null;
		Pessoa pessoa = null;
		//pesquisar pessoa 
	    if(notaxml.getCpf() != null && !notaxml.getCpf().equals("") ){
	    	Cpf cpf = new Cpf(notaxml.getCpf());
	    	cliente = clienteService.findByCpf(cpf);
	    	if(cliente == null){
	    		pessoa = pessoaService.findPessoaByCpf(cpf);
	    		if(pessoa == null){
	    			cliente = new Cliente();
	    			cliente.setCpf(cpf);
	    			cliente.setNome(notaxml.getNome());
	    			cliente.setRazaosocial(notaxml.getNome());
	    			cliente.setAtivo(true);
	    			cliente.setTipopessoa(Tipopessoa.PESSOA_FISICA);
	    			clienteService.saveOrUpdate(cliente);
	    		}else if(pessoa != null){
	    			cliente = new Cliente();
	    			cliente.setCdpessoa(pessoa.getCdpessoa());
	    			cliente.setAtivo(true);
	    			clienteService.insertSomenteCliente(cliente);
	    		}
	    	}
	    }else if(notaxml.getCnpj() != null && !notaxml.getCnpj().equals("")){
	    	Cnpj cnpj = new Cnpj(notaxml.getCnpj());
	    	cliente = clienteService.findByCnpj(cnpj);
	    	if(cliente == null){
	    		pessoa = pessoaService.findPessoaByCnpj(cnpj);
	    		if(pessoa == null){
	    			cliente = new Cliente();
	    			cliente.setCnpj(cnpj);
	    			cliente.setNome(notaxml.getNome());
	    			cliente.setRazaosocial(notaxml.getNome());
	    			cliente.setAtivo(true);
	    			cliente.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
	    			clienteService.saveOrUpdate(cliente);
	    		}else if(pessoa != null){
	    			cliente = new Cliente();
	    			cliente.setCdpessoa(pessoa.getCdpessoa());
	    			cliente.setAtivo(true);
	    			clienteService.insertSomenteCliente(cliente);
	    		}
	    	}
	    }else if(notaxml.getNome() != null && !notaxml.getNome().equals("")){ 
	    	cliente = clienteService.findByNome(notaxml.getNome());
	    	if(cliente == null){
	    		pessoa = pessoaService.findPessoaByNome(notaxml.getNome());
	    		if(pessoa == null){
	    			cliente = new Cliente();
	    			cliente.setNome(notaxml.getNome());
	    			cliente.setAtivo(true);
	    			cliente.setTipopessoa(Tipopessoa.PESSOA_FISICA);
	    			clienteService.saveOrUpdate(cliente);
	    		} else if(pessoa != null){
	    			cliente = new Cliente();
	    			cliente.setCdpessoa(pessoa.getCdpessoa());
	    			cliente.setAtivo(true);
	    			clienteService.insertSomenteCliente(cliente);
	    		}
	    	}
	    }
	    
	    if(notaxml.getCep() != null && notaxml.getCep().length() == 7){
			notaxml.setCep("0" + notaxml.getCep());
	    }
	    
	    //pesquisar endere�o
	    Endereco endereco = notaxml.getCep() != null ? enderecoService.loadEndereco(notaxml.getCep(), notaxml.getNumero(), cliente.getCdpessoa()) : null;
		if(endereco == null && notaxml.getLogradouro() != null){
			endereco = enderecoService.loadEnderecoLogradouro(new String(notaxml.getLogradouro().getBytes("LATIN1")), notaxml.getNumero(), cliente.getCdpessoa());
			if(endereco == null){
				Municipio municipio = municipioService.findByCdibge(String.valueOf(notaxml.getCdmunicipio()));
				endereco = new Endereco();
				if(notaxml.getLogradouro() != null)
					endereco.setLogradouro(new String(notaxml.getLogradouro().getBytes("LATIN1")));
				endereco.setNumero(notaxml.getNumero());
				endereco.setBairro(notaxml.getBairro());
				endereco.setCep(notaxml.getCep() != null ? new Cep(notaxml.getCep()) : null);
				endereco.setMunicipio(municipio);
				endereco.setPessoa(cliente);
				endereco.setEnderecotipo(Enderecotipo.UNICO);
				
				enderecoService.saveOrUpdate(endereco);
			}
		}
		//pesquisar municipioissqn
		Municipio municipioissqn = municipioService.findByCdibge(notaxml.getCdmunicipioissqn());
		Codigocnae codigocnae = null;
		Regimetributacao regimetributacao = null;
		Naturezaoperacao naturezaoperacao = null;
		if(notaxml.getCodigocnae() != null && !"".equals(notaxml.getCodigocnae()) && notaxml.getCodigocnae().length() == 7){
			codigocnae = codigocnaeService.getCodigocnaeForImportacaoxml(notaxml.getCodigocnae());
		}
		if(notaxml.getRegimeEspecialTributacao() != null && !"".equals(notaxml.getRegimeEspecialTributacao())){
			regimetributacao = regimetributacaoService.getRegimetributacaoForImportacaoxml(notaxml.getRegimeEspecialTributacao());
		}
		if(notaxml.getNaturezaOperacao() != null && !"".equals(notaxml.getNaturezaOperacao())){
			naturezaoperacao = naturezaoperacaoService.getNaturezaoperacaoForImportacaoxml(notaxml.getNaturezaOperacao());
		}
		
		//Preencher objetos para salvar no banco.

		NotaFiscalServico notaFiscalServico = new NotaFiscalServico();
		notaFiscalServico.setEmpresa(empresa);
		notaFiscalServico.setNotaStatus(NotaStatus.NFSE_EMITIDA);
		notaFiscalServico.setNotaTipo(NotaTipo.NOTA_FISCAL_SERVICO);
		notaFiscalServico.setNotaimportadaxml(Boolean.TRUE);
		notaFiscalServico.setProjeto(filtro.getProjeto());
		notaFiscalServico.setNumero(notaxml.getNumeroNota());
		
		if(filtro.getGerarsequencial() != null && filtro.getGerarsequencial()){
			Integer proximoNumero = empresaService.carregaProxNumNF(empresa);
			if(proximoNumero == null){
				proximoNumero = 1;
			}
			notaFiscalServico.setNumero(proximoNumero.toString());
			proximoNumero++;

			empresaService.updateProximoNumNF(empresa, proximoNumero);
		}
		
		Itemlistaservico itemlistaservico = org.apache.commons.lang.StringUtils.isNotBlank(notaxml.getItemListaServico()) ? itemlistaservicoService.loadByCodigo(notaxml.getItemListaServico()) : null;
		Codigotributacao codigotributacao = org.apache.commons.lang.StringUtils.isNotBlank(notaxml.getCodigotributacaomunicipio()) ? codigotributacaoService.loadByCodigo(notaxml.getCodigotributacaomunicipio()) : null;
		if(codigotributacao == null && org.apache.commons.lang.StringUtils.isNotBlank(notaxml.getCodigotributacaomunicipio())){
			codigotributacao = codigotributacaoService.findByCodigo(notaxml.getCodigotributacaomunicipio());
		}
		if(itemlistaservico == null && org.apache.commons.lang.StringUtils.isNotBlank(notaxml.getItemListaServico()) &&
				"0".equals(notaxml.getItemListaServico().substring(0,1))){
			itemlistaservico = itemlistaservicoService.loadByCodigo(notaxml.getItemListaServico().substring(1, notaxml.getItemListaServico().length()));
		}
		
		notaFiscalServico.setCodigotributacao(codigotributacao);
		notaFiscalServico.setItemlistaservicoBean(itemlistaservico);
		
		notaFiscalServico.setDtEmissao(notaxml.getDtemissaorps());
		notaFiscalServico.setBasecalculo(new Money(notaxml.getBasecalculo()));
		notaFiscalServico.setBasecalculoiss(new Money(notaxml.getBasecalculo()));
		notaFiscalServico.setIss(notaxml.getIss() != null ? notaxml.getIss() / 100d : null);
		if(notaxml.getIssRetido() != null && notaxml.getIssRetido().intValue() == 1){
			notaFiscalServico.setIncideiss(true);
		}
		
		notaFiscalServico.setBasecalculoicms(new Money(notaxml.getBasecalculo()));
		notaFiscalServico.setBasecalculocofins(new Money(notaxml.getBasecalculo()));
		notaFiscalServico.setBasecalculocsll(new Money(notaxml.getBasecalculo()));
		notaFiscalServico.setBasecalculoinss(new Money(notaxml.getBasecalculo()));
		notaFiscalServico.setBasecalculoir(new Money(notaxml.getBasecalculo()));
		notaFiscalServico.setBasecalculopis(new Money(notaxml.getBasecalculo()));
		
		boolean arredondar = parametrogeralService.getBoolean(Parametrogeral.ARREDONDAR_ALIQUOTA_XML_NFS);
		if(notaxml.getValoriss() != null && notaxml.getBasecalculo() != null && notaxml.getBasecalculo().doubleValue() > 0){
			notaFiscalServico.setIss(SinedUtil.roundAliquotaNfse((notaxml.getValoriss().doubleValue() * 100d) / notaxml.getBasecalculo().doubleValue(), 3, arredondar));
		}
		if(notaxml.getValorissretido() != null && notaxml.getBasecalculo() != null && notaxml.getBasecalculo().doubleValue() > 0){
			notaFiscalServico.setIss(SinedUtil.roundAliquotaNfse((notaxml.getValorissretido().doubleValue() * 100d) / notaxml.getBasecalculo().doubleValue(), 3, arredondar));
		}
		if(notaxml.getValorcofins() != null && notaxml.getBasecalculo() != null && notaxml.getBasecalculo().doubleValue() > 0){
			notaFiscalServico.setIncidecofins(Boolean.TRUE);
			notaFiscalServico.setCofins(SinedUtil.roundAliquotaNfse((notaxml.getValorcofins().doubleValue() * 100d) / notaxml.getBasecalculo().doubleValue(), 3, arredondar));
		}
		if(notaxml.getValorcsll() != null && notaxml.getBasecalculo() != null && notaxml.getBasecalculo().doubleValue() > 0){
			notaFiscalServico.setIncidecsll(Boolean.TRUE);
			notaFiscalServico.setCsll(SinedUtil.roundAliquotaNfse((notaxml.getValorcsll().doubleValue() * 100d) / notaxml.getBasecalculo().doubleValue(), 3, arredondar));
		}
		if(notaxml.getValorinss() != null && notaxml.getBasecalculo() != null && notaxml.getBasecalculo().doubleValue() > 0){
			notaFiscalServico.setIncideinss(Boolean.TRUE);
			notaFiscalServico.setInss(SinedUtil.roundAliquotaNfse((notaxml.getValorinss().doubleValue() * 100d) / notaxml.getBasecalculo().doubleValue(), 3, arredondar));
		}
		if(notaxml.getValorir() != null && notaxml.getBasecalculo() != null && notaxml.getBasecalculo().doubleValue() > 0){
			notaFiscalServico.setIncideir(Boolean.TRUE);
			notaFiscalServico.setIr(SinedUtil.roundAliquotaNfse((notaxml.getValorir().doubleValue() * 100d) / notaxml.getBasecalculo().doubleValue(), 3, arredondar));
		}
		if(notaxml.getValorpis() != null && notaxml.getBasecalculo() != null && notaxml.getBasecalculo().doubleValue() > 0){
			notaFiscalServico.setIncidepis(Boolean.TRUE);
			notaFiscalServico.setPis(SinedUtil.roundAliquotaNfse((notaxml.getValorpis().doubleValue() * 100d) / notaxml.getBasecalculo().doubleValue(), 3, arredondar));
		}
		
		notaFiscalServico.setMunicipioissqn(municipioissqn);
		notaFiscalServico.setCodigocnaeBean(codigocnae);
		notaFiscalServico.setRegimetributacao(regimetributacao);
		notaFiscalServico.setNaturezaoperacao(naturezaoperacao);
		notaFiscalServico.setCliente(cliente);
		notaFiscalServico.setEnderecoCliente(endereco);
		
		notaFiscalServicoService.saveOrUpdate(notaFiscalServico);
		
		NotaHistorico notaHistorico = new NotaHistorico();
		notaHistorico.setNota(notaFiscalServico);
		notaHistorico.setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
		notaHistorico.setObservacao("Nota importada via XML.");
		
		notaHistoricoService.saveOrUpdate(notaHistorico);
		
		List<NotaFiscalServicoItem> listNotaFiscalServicoItem = new ArrayList<NotaFiscalServicoItem>();
		
		if (notaxml.getListaNotaItemXml() != null && notaxml.getListaNotaItemXml().size() > 0) {
			for (NotaItemXML notaItemXML : notaxml.getListaNotaItemXml()) {
				NotaFiscalServicoItem notaFiscalServicoItem = new NotaFiscalServicoItem();
				
				if (notaItemXML.getDesconto() != null) {
					notaFiscalServicoItem.setDesconto(new Money(notaItemXML.getDesconto()));
				}
				notaFiscalServicoItem.setDescricao(notaItemXML.getDescricao());
				notaFiscalServicoItem.setPrecoUnitario(notaItemXML.getPrecoUnitario());
				notaFiscalServicoItem.setQtde(notaItemXML.getQtde());
				notaFiscalServicoItem.setNotaFiscalServico(notaFiscalServico);
				
				listNotaFiscalServicoItem.add(notaFiscalServicoItem);
				
				notafiscalServicoItemService.saveOrUpdate(notaFiscalServicoItem);
			}
		} else {
			NotaFiscalServicoItem notaFiscalServicoItem = new NotaFiscalServicoItem();
			notaFiscalServicoItem.setQtde(notaxml.getQtde());
			notaFiscalServicoItem.setPrecoUnitario(notaxml.getPrecounitario().doubleValue());
			notaFiscalServicoItem.setDescricao(new String(notaxml.getDescricao().getBytes(), "LATIN1"));
			notaFiscalServicoItem.setNotaFiscalServico(notaFiscalServico);
			
			listNotaFiscalServicoItem.add(notaFiscalServicoItem);
			
			notafiscalServicoItemService.saveOrUpdate(notaFiscalServicoItem);
		}
		
		notaFiscalServico.setListaItens(listNotaFiscalServicoItem);
		
		if(filtro != null){
			if(filtro.getGrupotributacao() != null){
				if(notaFiscalServico.getCliente() != null) notaFiscalServico.setCliente(clienteService.loadForEntrada(notaFiscalServico.getCliente()));
				if(notaFiscalServico.getEmpresa() != null) notaFiscalServico.setEmpresa(empresaService.loadForEntrada(notaFiscalServico.getEmpresa()));
				if(notaFiscalServico.getEnderecoCliente() != null) notaFiscalServico.setEnderecoCliente(enderecoService.loadEndereco(notaFiscalServico.getEnderecoCliente()));
				notaFiscalServicoService.calculaTributacaoNota(notaFiscalServico, filtro.getGrupotributacao(), notaFiscalServico.getValorTotalServicos());
			}
			if(notaxml.getIssRetido() != null && notaxml.getIssRetido().intValue() == 1){
				notaFiscalServico.setIncideiss(true);
			}
			notaFiscalServico.setCstcofins(filtro.getTipocobrancacofins());
			notaFiscalServico.setCstpis(filtro.getTipocobrancapis());
		}
		
		notaFiscalServicoService.saveOrUpdate(notaFiscalServico);
		
		Arquivonf arquivonf = new Arquivonf();
		
		arquivonf.setDtenvio(SinedDateUtils.currentDate());
		arquivonf.setEmpresa(empresa);
		arquivonf.setConfiguracaonfe(filtro.getConfiguracaonfe());
		arquivonf.setArquivonfsituacao(Arquivonfsituacao.PROCESSADO_COM_SUCESSO);
		
		arquivonf.setAssinando(Boolean.TRUE);
		arquivonf.setEnviando(Boolean.TRUE);
		arquivonf.setConsultando(Boolean.TRUE);
		
		arquivonf.setArquivoxml(arquivo);
		arquivonf.setArquivoxmlassinado(arquivo);
		
		arquivoDAO.saveFile(arquivonf, "arquivoxml");
		arquivoService.saveOrUpdate(arquivo);
		
		arquivonfService.saveOrUpdate(arquivonf);
		
		Arquivonfnota arquivonfnota = new Arquivonfnota();
		
		arquivonfnota.setArquivonf(arquivonf);
		arquivonfnota.setNota(notaFiscalServico);
		arquivonfnota.setNumeronfse(notaxml.getNumeronfse());
		arquivonfnota.setCodigoverificacao(notaxml.getCodigoverificacao());
		arquivonfnota.setDtemissao(notaxml.getDtemissao());
		arquivonfnota.setArquivoxml(arquivo);
		if(notaxml.getDtcompetencia() != null) arquivonfnota.setDtcompetencia(new Timestamp(notaxml.getDtcompetencia().getTime()));
		
		arquivonfnotaService.saveOrUpdate(arquivonfnota);
		return true;
	}
}
