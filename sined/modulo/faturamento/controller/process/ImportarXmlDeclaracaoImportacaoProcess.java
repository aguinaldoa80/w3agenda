package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.sf.json.JSON;

import org.apache.commons.lang.StringUtils;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Declaracaoimportacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitemdi;
import br.com.linkcom.sined.geral.bean.enumeration.FormaintermediacaDI;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Operacao;
import br.com.linkcom.sined.geral.bean.enumeration.Presencacompradornfe;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.bean.enumeration.ViatransporteDI;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.CfopService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.GrupotributacaoService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoitemService;
import br.com.linkcom.sined.geral.service.PaisService;
import br.com.linkcom.sined.geral.service.UfService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxCalculaTotalValorNotaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.DeclaracaoImportacaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.DeclaracaoImportacaoBeanXml;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.DeclaracaoImportacaoitemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.DeclaracaoImportacaoitemBeanXml;
import br.com.linkcom.sined.modulo.faturamento.controller.process.filter.ImportarXMLDeclaracaoImportacaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/faturamento/process/ImportarXMLDeclaracaoImportacao", authorizationModule=ProcessAuthorizationModule.class)
public class ImportarXmlDeclaracaoImportacaoProcess extends MultiActionController{
	
	public static final String ATTR_BEAN_DI_SESSAO = "ATTR_BEAN_DI_SESSAO";
	public static final String ATTR_NOTAFISCALPRODUTO_DI_SESSAO = "ATTR_NOTAFISCALPRODUTO_DI_SESSAO";
	
	private UfService ufService;
	private PaisService paisService;
	private ClienteService clienteService;
	private FornecedorService fornecedorService;
	private NaturezaoperacaoService  naturezaoperacaoService;
	private EmpresaService empresaService;
	private MunicipioService municipioService;
	private EnderecoService enderecoService;
	private MaterialService materialService;
	private GrupotributacaoService grupotributacaoService;
	private CategoriaService categoriaService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private NotafiscalprodutoitemService notafiscalprodutoitemService;
	private CfopService cfopService;
	private LocalarmazenagemService localarmazenagemService;
	
	public void setUfService(UfService ufService) {this.ufService = ufService;}
	public void setPaisService(PaisService paisService) {this.paisService = paisService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setNaturezaoperacaoService(NaturezaoperacaoService naturezaoperacaoService) {this.naturezaoperacaoService = naturezaoperacaoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setMunicipioService(MunicipioService municipioService) {this.municipioService = municipioService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setGrupotributacaoService(GrupotributacaoService grupotributacaoService) {this.grupotributacaoService = grupotributacaoService;}
	public void setCategoriaService(CategoriaService categoriaService) {this.categoriaService = categoriaService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setNotafiscalprodutoitemService(NotafiscalprodutoitemService notafiscalprodutoitemService) {this.notafiscalprodutoitemService = notafiscalprodutoitemService;}
	public void setCfopService(CfopService cfopService) {this.cfopService = cfopService;}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {this.localarmazenagemService = localarmazenagemService;}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, ImportarXMLDeclaracaoImportacaoFiltro filtro){
		request.getSession().removeAttribute(ATTR_BEAN_DI_SESSAO);
		return new ModelAndView("/process/importacaoXMLDI", "filtro", filtro);
	}
	
	public ModelAndView importar(WebRequestContext request, ImportarXMLDeclaracaoImportacaoFiltro filtro) throws Exception {
		request.getSession().removeAttribute(ATTR_BEAN_DI_SESSAO);
		if(filtro.getArquivo() == null || !"text/xml".equalsIgnoreCase(filtro.getArquivo().getContenttype())){
			request.addError("� preciso informar um arquivo xml.");
			return continueOnAction("index", filtro); 
		}
		
		if(filtro.getEmpresa() == null){
			request.addError("� preciso informar a empresa.");
			return continueOnAction("index", filtro); 
		}
		
		try{
			DeclaracaoImportacaoBeanXml beanXml = this.processarArquivoXml(request, filtro.getArquivo());
			DeclaracaoImportacaoBean bean = this.criaDeclaracaoImportacaoBean(request, beanXml, filtro.getEmpresa());
			filtro.setBean(bean);
			request.getSession().setAttribute(ATTR_BEAN_DI_SESSAO, bean);
			SinedUtil.redirecionamento(request, "/sistema/crud/Declaracaoimportacao?ACAO=criar&importacaoXML=true");
			return null;
		} catch (Exception e) {
			request.addError("Problemas na importa��o de declara��o de importa��o.");
			request.addError(e.getMessage());
			e.printStackTrace();
			
			return continueOnAction("index", filtro); 
		}
	}
	
	public ModelAndView conferenciaDIcliente(WebRequestContext request, DeclaracaoImportacaoBean bean) throws Exception {
		bean = (DeclaracaoImportacaoBean) request.getSession().getAttribute(ImportarXmlDeclaracaoImportacaoProcess.ATTR_BEAN_DI_SESSAO);
		return new ModelAndView("/process/importacaoXMLDIcliente", "bean", bean);
	}
	
	private DeclaracaoImportacaoBean criaDeclaracaoImportacaoBean(WebRequestContext request, DeclaracaoImportacaoBeanXml beanXml, Empresa empresa) throws Exception{
		DeclaracaoImportacaoBean bean = new DeclaracaoImportacaoBean();
		
		bean.setBeanXml(beanXml);
		bean.setEmpresa(empresa);
		bean.setDeclaracaoimportacao(new Declaracaoimportacao());
		bean.getDeclaracaoimportacao().setImportacaoXML(Boolean.TRUE);
		bean.getDeclaracaoimportacao().setFormaintermediacao(FormaintermediacaDI.CONTA_PROPRIA);
		bean.getDeclaracaoimportacao().setNumerodidsida(beanXml.getNumerodidsida());
		bean.getDeclaracaoimportacao().setDtregistro(beanXml.getDtregistro());
		bean.getDeclaracaoimportacao().setViatransporte(beanXml.getViatransporte());
		bean.getDeclaracaoimportacao().setValorafrmm(beanXml.getValorafrmm());
		bean.getDeclaracaoimportacao().setCnpjadquirente(beanXml.getCnpjadquirente());
		bean.getDeclaracaoimportacao().setUf(beanXml.getUf());
		bean.getDeclaracaoimportacao().setCodigoexportador(beanXml.getCodigoexportador());
		bean.getDeclaracaoimportacao().setData(beanXml.getData());
		
		bean.getDeclaracaoimportacao().setLocaldesembaraco(beanXml.getLocaldesembaraco());
		bean.getDeclaracaoimportacao().setData(beanXml.getData());
		bean.getDeclaracaoimportacao().setData(beanXml.getData());
			
		bean.setPis(beanXml.getPisPasepAliquotaAdValorem());
		bean.setCofins(beanXml.getCofinsAliquotaAdValorem());
		bean.setTaxaCambial(calcularTaxaCambial(beanXml.getCondicaoVendaValorReais(), beanXml.getCondicaoVendaValorMoeda()));
		bean.setValorReceita(beanXml.getValorReceita() != null ? new Money(beanXml.getValorReceita()) : null);
		bean.setValorTaxaCapatazias(beanXml.getValorReais() != null ? new Money(beanXml.getValorReais()) : null);
		bean.setValorDespesasAduaneiras(bean.getValorTaxaCapatazias());
		bean.setMargemContribuicao(calcularMargemContribuicao(beanXml.getValorReais(), beanXml.getQtdeTotal()));
		
		if (StringUtils.isNotBlank(beanXml.getFornecedorNome())){
			Cliente cliente = clienteService.findByNome(beanXml.getFornecedorNome());
			if(cliente != null){
				bean.setCliente(clienteService.carregarDadosCliente(cliente));
				bean.setEnderecoCliente(cliente.getEnderecoForDI());
			}
		}
		
		bean.setListaItens(new ArrayList<DeclaracaoImportacaoitemBean>());
		for (DeclaracaoImportacaoitemBeanXml itemBeanXml: beanXml.getListaItens()) {
			DeclaracaoImportacaoitemBean itemBean = new DeclaracaoImportacaoitemBean();
			itemBean.setItemBeanXml(itemBeanXml);
			itemBean.setQuantidade(itemBeanXml.getQuantidade() != null ? itemBeanXml.getQuantidade() : 1d);
			itemBean.setNcmcompleto(beanXml.getDadosMercadoriaCodigoNcm());
			itemBean.setValorUnitario(calcularValorUnitario(itemBeanXml.getValorUnitario(), bean.getTaxaCambial(), bean.getMargemContribuicao()));
			itemBean.setValorBruto(new Money(itemBean.getValorUnitario() * itemBeanXml.getQuantidade()));
			itemBean.setValorIss(new Money());
			itemBean.setValorIpi(new Money());
			itemBean.setValorFrete(new Money());
			itemBean.setValorDesconto(new Money());
			itemBean.setValorSeguro(new Money());
			itemBean.setValorPis(calcularValorPisCofins(itemBean.getValorBruto(), bean.getPis()));
			itemBean.setValorCofins(calcularValorPisCofins(itemBean.getValorBruto(), bean.getCofins()));
			itemBean.setPesoBruto(itemBean.getQuantidade());
			itemBean.setPesoLiquido(itemBean.getQuantidade());
			bean.getListaItens().add(itemBean);
		}
		
		bean.setValorTotalProdutos(calcularValorTotalProdutos(bean));
		bean.setValorPis(new Money(calcularValorPisCofins(bean.getValorTotalProdutos(), bean.getPis())));
		bean.setValorCofins(new Money(calcularValorPisCofins(bean.getValorTotalProdutos(), bean.getCofins())));
		bean.setValorOutrasDespesas(calcularValorOutrasDespesas(bean.getValorReceita(), bean.getValorPis(), bean.getValorCofins()));
		bean.setValorTotalNota(calcularValorTotalNota(bean));
		
		return bean;
	}
	
	private Money calcularValorTotalNota(DeclaracaoImportacaoBean bean) {
		Money valorTotalNota = new Money();
		
		if(bean.getValorTotalProdutos() != null){
			valorTotalNota = valorTotalNota.add(bean.getValorTotalProdutos());
		}
		if(bean.getValorCofins() != null){
			valorTotalNota = valorTotalNota.add(bean.getValorCofins());
		}
		if(bean.getValorPis() != null){
			valorTotalNota = valorTotalNota.add(bean.getValorPis());
		}
		if(bean.getValorOutrasDespesas() != null){
			valorTotalNota = valorTotalNota.add(bean.getValorOutrasDespesas());
		}
		
		return valorTotalNota;
	}
	
	private Money calcularValorOutrasDespesas(Money valorReceita, Money valorPis, Money valorCofins) {
		Money valorOutrasDespesas = new Money();
		if(valorReceita != null && valorReceita.getValue().doubleValue() > 0){
			valorOutrasDespesas = valorOutrasDespesas.add(valorReceita);
			
			if(valorPis != null){
				valorOutrasDespesas = valorOutrasDespesas.subtract(valorPis);
			}
			
			if(valorCofins != null){
				valorOutrasDespesas = valorOutrasDespesas.subtract(valorCofins);
			}
		}
		return valorOutrasDespesas;
	}
	
	private Money calcularValorPisCofins(Money valor, Double aliq) {
		if(valor != null && aliq != null){
			return new Money(valor.getValue().doubleValue() * aliq / 100d);
		}
		return new Money();
	}
	
	private Money calcularValorTotalProdutos(DeclaracaoImportacaoBean bean) {
		Double valorTotal = 0d;
		if(SinedUtil.isListNotEmpty(bean.getListaItens())){
			for(DeclaracaoImportacaoitemBean itemBean : bean.getListaItens()){
				if(itemBean.getValorBruto() != null){
					valorTotal += itemBean.getValorBruto().getValue().doubleValue();
				}
			}
		}
		return new Money(valorTotal);
	}
	
	private Double calcularValorUnitario(Double valorUnitario, Double valorTaxaCambial, Double margemContribuicao) {
		Double vl = 0d;
		
		if(valorTaxaCambial != null && valorUnitario != null){
			vl += (valorTaxaCambial * valorUnitario);
			if(margemContribuicao != null) vl += margemContribuicao;
		}
		
		return SinedUtil.round(vl, 6);
	}
	
	private Double calcularMargemContribuicao(Double valorReais, Double qtdeTotal) {
		if(valorReais != null && qtdeTotal != null){
			return valorReais / qtdeTotal;
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private DeclaracaoImportacaoBeanXml processarArquivoXml(WebRequestContext request, Arquivo arquivo) throws UnsupportedEncodingException, JDOMException, IOException, ParseException, Exception{
		Element root = SinedUtil.getRootElementXML(arquivo.getContent());
		if(root == null) throw new SinedException("O layout do arquivo XML n�o corresponde ao padr�o reconhecido pelo sistema. Favor verificar se o XML foi gerado corretamente!");
		
		if(!root.getName().equalsIgnoreCase("ListaDeclaracoes")){
			throw new SinedException("O layout do arquivo XML n�o corresponde ao padr�o reconhecido pelo sistema. Favor verificar se o XML foi gerado corretamente! <br>Tag ListaDeclaracoes n�o encontrada.");
		}
		
		DeclaracaoImportacaoBeanXml bean = new DeclaracaoImportacaoBeanXml();
		
		Element elNumeroDI, elDataRegistro, elViaTransporteCodigo, elValorAFRMM, elImportadorNumero, elImportadorEnderecoUf, elCodExportador, elDataDesembaraco, elArmazem, elNomeArmazem, 
				elFornecedorNome, elFornecedorComplemento, elFornecedorLogradouro, elFornecedorNumero, elFornecedorEstado, elFornecedorCidade, 
				elCondicaoVendaValorReais, elCondicaoVendaValorMoeda,
				elDescricaoMercadoria, elDadosMercadoriaCodigoNcm, elQuantidade, elValorUnitario, elAcrescimo, elValorReais,
				elCofinsAliquotaAdValorem, elPisPasepAliquotaAdValorem, elValorReceita;
		
		List<Element> listaDI, listaAdicao, listaMercadoria, listaPagamento;
		listaDI = SinedUtil.getListChildElement("declaracaoImportacao", root.getContent());
		
		if(SinedUtil.isListEmpty(listaDI)) throw new SinedException("O layout do arquivo XML n�o corresponde ao padr�o reconhecido pelo sistema. Favor verificar se o XML foi gerado corretamente! <br>Tag declaracaoImportacao n�o encontrada.");
		
		for (Element elDeclaracaoImportacao: listaDI) {
			
			elNumeroDI = SinedUtil.getChildElement("numeroDI", elDeclaracaoImportacao.getContent());
			if (elNumeroDI == null || org.apache.commons.lang.StringUtils.isBlank(elNumeroDI.getText())) throw new SinedException("O layout do arquivo XML n�o corresponde ao padr�o reconhecido pelo sistema. Favor verificar se o XML foi gerado corretamente! <br>Tag numeroDI n�o encontrada.");
			
			bean.setNumerodidsida(elNumeroDI.getText());
			
			elDataRegistro = SinedUtil.getChildElement("dataRegistro", elDeclaracaoImportacao.getContent());
			if (elDataRegistro != null && org.apache.commons.lang.StringUtils.isNotBlank(elDataRegistro.getText())) bean.setDtregistro(SinedDateUtils.stringToDate(elDataRegistro.getText(), "yyyyMMdd"));
			
			elViaTransporteCodigo = SinedUtil.getChildElement("viaTransporteCodigo", elDeclaracaoImportacao.getContent());
			if (elViaTransporteCodigo != null && org.apache.commons.lang.StringUtils.isNotBlank(elViaTransporteCodigo.getText())) bean.setViatransporte(ViatransporteDI.getByCdnfe(Integer.parseInt(elViaTransporteCodigo.getText())));
			
			elValorAFRMM = SinedUtil.getChildElement("valorAFRMM", elDeclaracaoImportacao.getContent());
			if (elValorAFRMM != null && org.apache.commons.lang.StringUtils.isNotBlank(elValorAFRMM.getText())) bean.setValorafrmm(new Money(new Double(elValorAFRMM.getText().replaceAll("\\.", "").replace(",", "."))));
			
			elImportadorNumero = SinedUtil.getChildElement("importadorNumero", elDeclaracaoImportacao.getContent());
			if (elImportadorNumero != null && org.apache.commons.lang.StringUtils.isNotBlank(elImportadorNumero.getText())) bean.setCnpjadquirente(new Cnpj(elImportadorNumero.getText()));
			
			elImportadorEnderecoUf = SinedUtil.getChildElement("importadorEnderecoUf", elDeclaracaoImportacao.getContent());
			if (elImportadorEnderecoUf != null && org.apache.commons.lang.StringUtils.isNotBlank(elImportadorEnderecoUf.getText())) bean.setUf(ufService.findBySigla(elImportadorEnderecoUf.getText()));
			
			elCodExportador = SinedUtil.getChildElement("codExportador", elDeclaracaoImportacao.getContent());
			if (elCodExportador != null && org.apache.commons.lang.StringUtils.isNotBlank(elCodExportador.getText())) bean.setCodigoexportador(elCodExportador.getText());
			
			elDataDesembaraco = SinedUtil.getChildElement("dataDesembaraco", elDeclaracaoImportacao.getContent());
			if (elDataDesembaraco != null && org.apache.commons.lang.StringUtils.isNotBlank(elDataDesembaraco.getText())) bean.setData(SinedDateUtils.stringToDate(elDataDesembaraco.getText(), "yyyyMMdd"));
			
			elArmazem = SinedUtil.getChildElement("armazem", elDeclaracaoImportacao.getContent());
			if (elArmazem != null){
				elNomeArmazem = SinedUtil.getChildElement("nomeArmazem", elArmazem.getContent());
				if (elNomeArmazem != null && org.apache.commons.lang.StringUtils.isNotBlank(elNomeArmazem.getText())) bean.setLocaldesembaraco(elNomeArmazem.getText());
			}
			
			listaAdicao = SinedUtil.getListChildElement("adicao", elDeclaracaoImportacao.getContent());
			if(SinedUtil.isListNotEmpty(listaAdicao)){
				bean.setListaItens(new ArrayList<DeclaracaoImportacaoitemBeanXml>());
				for (Element elAdicao: listaAdicao) {
					elCondicaoVendaValorReais = SinedUtil.getChildElement("condicaoVendaValorReais", elAdicao.getContent());
					if (elCondicaoVendaValorReais != null && org.apache.commons.lang.StringUtils.isNotBlank(elCondicaoVendaValorReais.getText())) bean.setCondicaoVendaValorReais(new Double(elCondicaoVendaValorReais.getText().replaceAll("\\.", "").replace(",", "."))/100d);
					
					elCondicaoVendaValorMoeda = SinedUtil.getChildElement("condicaoVendaValorMoeda", elAdicao.getContent());
					if (elCondicaoVendaValorMoeda != null && org.apache.commons.lang.StringUtils.isNotBlank(elCondicaoVendaValorMoeda.getText())) bean.setCondicaoVendaValorMoeda(new Double(elCondicaoVendaValorMoeda.getText().replaceAll("\\.", "").replace(",", "."))/100d);
					
					elFornecedorNome = SinedUtil.getChildElement("fornecedorNome", elAdicao.getContent());
					if (elFornecedorNome != null && org.apache.commons.lang.StringUtils.isNotBlank(elFornecedorNome.getText())){
						bean.setFornecedorNome(elFornecedorNome.getText());
					}
					
					elFornecedorComplemento = SinedUtil.getChildElement("fornecedorComplemento", elAdicao.getContent());
					if (elFornecedorComplemento != null && org.apache.commons.lang.StringUtils.isNotBlank(elFornecedorComplemento.getText())) bean.setFornecedorComplemento(elFornecedorComplemento.getText());
					
					elFornecedorLogradouro = SinedUtil.getChildElement("fornecedorLogradouro", elAdicao.getContent());
					if (elFornecedorLogradouro != null && org.apache.commons.lang.StringUtils.isNotBlank(elFornecedorLogradouro.getText())) bean.setFornecedorLogradouro(elFornecedorLogradouro.getText());
					
					elFornecedorNumero = SinedUtil.getChildElement("fornecedorNumero", elAdicao.getContent());
					if (elFornecedorNumero != null && org.apache.commons.lang.StringUtils.isNotBlank(elFornecedorNumero.getText())) bean.setFornecedorNumero(elFornecedorNumero.getText());
					
					elFornecedorEstado = SinedUtil.getChildElement("fornecedorEstado", elAdicao.getContent());
					if (elFornecedorEstado != null && org.apache.commons.lang.StringUtils.isNotBlank(elFornecedorEstado.getText())) bean.setFornecedorEstado(paisService.findByNome(elFornecedorEstado.getText()));
					
					elFornecedorCidade = SinedUtil.getChildElement("fornecedorCidade", elAdicao.getContent());
					if (elFornecedorCidade != null && org.apache.commons.lang.StringUtils.isNotBlank(elFornecedorCidade.getText())) bean.setFornecedorCidade(elFornecedorCidade.getText());

					
					elDadosMercadoriaCodigoNcm = SinedUtil.getChildElement("dadosMercadoriaCodigoNcm", elAdicao.getContent());
					if (elDadosMercadoriaCodigoNcm != null && org.apache.commons.lang.StringUtils.isNotBlank(elDadosMercadoriaCodigoNcm.getText())) bean.setDadosMercadoriaCodigoNcm(elDadosMercadoriaCodigoNcm.getText());
					
					elPisPasepAliquotaAdValorem = SinedUtil.getChildElement("pisPasepAliquotaAdValorem", elAdicao.getContent());
					if (elPisPasepAliquotaAdValorem != null && org.apache.commons.lang.StringUtils.isNotBlank(elPisPasepAliquotaAdValorem.getText())) bean.setPisPasepAliquotaAdValorem(new Double(elPisPasepAliquotaAdValorem.getText().replaceAll("\\.", "").replace(",", "."))/100d);
					
					elCofinsAliquotaAdValorem = SinedUtil.getChildElement("cofinsAliquotaAdValorem", elAdicao.getContent());
					if (elCofinsAliquotaAdValorem != null && org.apache.commons.lang.StringUtils.isNotBlank(elCofinsAliquotaAdValorem.getText())) bean.setCofinsAliquotaAdValorem(new Double(elCofinsAliquotaAdValorem.getText().replaceAll("\\.", "").replace(",", "."))/100d);
					
					elAcrescimo = SinedUtil.getChildElement("acrescimo", elAdicao.getContent());
					if (elAcrescimo != null){
						elValorReais = SinedUtil.getChildElement("valorReais", elAcrescimo.getContent());
						if (elValorReais != null && org.apache.commons.lang.StringUtils.isNotBlank(elValorReais.getText())) bean.setValorReais(new Double(elValorReais.getText().replaceAll("\\.", "").replace(",", "."))/100d);
					}
					
					listaMercadoria = SinedUtil.getListChildElement("mercadoria", elAdicao.getContent());
					if(SinedUtil.isListNotEmpty(listaMercadoria)){
						Double qtdeTotal = 0d;
						for (Element elMercadoria: listaMercadoria) {
							DeclaracaoImportacaoitemBeanXml itemBean = new DeclaracaoImportacaoitemBeanXml();
							if (elMercadoria != null){
								elDescricaoMercadoria = SinedUtil.getChildElement("descricaoMercadoria", elMercadoria.getContent());
								if (elDescricaoMercadoria != null && org.apache.commons.lang.StringUtils.isNotBlank(elDescricaoMercadoria.getText())) itemBean.setDescricaoMercadoria(elDescricaoMercadoria.getText());
								
								elQuantidade = SinedUtil.getChildElement("quantidade", elMercadoria.getContent());
								if (elQuantidade != null && org.apache.commons.lang.StringUtils.isNotBlank(elQuantidade.getText())) itemBean.setQuantidade(new Double(elQuantidade.getText().replaceAll("\\.", "").replace(",", "."))/100d);
								
								elValorUnitario = SinedUtil.getChildElement("valorUnitario", elMercadoria.getContent());
								if (elValorUnitario != null && org.apache.commons.lang.StringUtils.isNotBlank(elValorUnitario.getText())) itemBean.setValorUnitario(new Double(elValorUnitario.getText().replaceAll("\\.", "").replace(",", "."))/10000000000d);
								
								if(itemBean.getQuantidade() != null){
									qtdeTotal += itemBean.getQuantidade();
								}
							}
							bean.getListaItens().add(itemBean);
						}
						
						bean.setQtdeTotal(qtdeTotal);
					}
				}
			}
			
			listaPagamento = SinedUtil.getListChildElement("pagamento", elDeclaracaoImportacao.getContent());
			if(SinedUtil.isListNotEmpty(listaPagamento)){
				Double valorReceita = 0d;
				for(Element elPagamento: listaPagamento){
					elValorReceita = SinedUtil.getChildElement("valorReceita", elPagamento.getContent());
					if (elValorReceita != null && org.apache.commons.lang.StringUtils.isNotBlank(elValorReceita.getText())) valorReceita += (new Double(elValorReceita.getText().replaceAll("\\.", "").replace(",", "."))/100d);
				}
				bean.setValorReceita(valorReceita);
			}
		}	
		
		return bean;
	}
	
	private Double calcularTaxaCambial(Double condicaoVendaValorReais, Double condicaoVendaValorMoeda) {
		if(condicaoVendaValorReais == null || condicaoVendaValorMoeda == null || condicaoVendaValorMoeda == 0d) return null;
		return SinedUtil.round(condicaoVendaValorReais/condicaoVendaValorMoeda, 4);
		
	}
	
	public ModelAndView conferenciaDI(WebRequestContext request, DeclaracaoImportacaoBean declaracaoImportacaoBean){
		DeclaracaoImportacaoBean beanSessao = (DeclaracaoImportacaoBean) request.getSession().getAttribute(ImportarXmlDeclaracaoImportacaoProcess.ATTR_BEAN_DI_SESSAO);
		if(beanSessao == null){
			request.addError("Problemas na importa��o de declara��o de importa��o. N�o foi poss�vel obter as informa��es do xml.");
			return continueOnAction("index", new ImportarXMLDeclaracaoImportacaoFiltro()); 
		}
		
		if(declaracaoImportacaoBean.getCliente() != null){
			Cliente cliente = clienteService.carregarDadosCliente(declaracaoImportacaoBean.getCliente());
			beanSessao.setCliente(cliente);
			declaracaoImportacaoBean.setEnderecoCliente(cliente.getEnderecoForDI());
		}
		if(declaracaoImportacaoBean.getTransportador() != null){
			Fornecedor fornecedor = fornecedorService.carregaFornecedor(declaracaoImportacaoBean.getTransportador());
			beanSessao.setTransportador(fornecedor);
		}
		
		request.getSession().setAttribute(ATTR_BEAN_DI_SESSAO, beanSessao);
		
		request.setAttribute("listaNaturezaoperacao", naturezaoperacaoService.find(null, NotaTipo.NOTA_FISCAL_PRODUTO, Boolean.TRUE));
		return new ModelAndView("process/importacaoXMLDIconferencia", "bean", beanSessao);
	}
	
	public ModelAndView ajaxBuscaDadosCliente(WebRequestContext request, Cliente cliente){
		Endereco endereco = null;
		if(cliente.getCdpessoa() != null){
			cliente = clienteService.carregarDadosCliente(cliente);
			if(cliente != null){
				endereco = cliente.getEnderecoForDI();
			}
		}
		return new JsonModelAndView()
				.addObject("razaosocial", cliente != null ? cliente.getRazaosocial() : "")
				.addObject("logradouroCompletoComBairro", endereco != null ? endereco.getLogradouroCompleto() : "");
	}
	
	public ModelAndView ajaxBuscaDadosTransportador(WebRequestContext request, Fornecedor fornecedor){
		Endereco endereco = null;
		if(fornecedor.getCdpessoa() != null){
			fornecedor = fornecedorService.carregaFornecedor(fornecedor);
			if(fornecedor != null){
				endereco = fornecedor.getEnderecoForDI();
			}
		}
		return new JsonModelAndView()
				.addObject("razaosocial", fornecedor != null ? fornecedor.getRazaosocial() : "")
				.addObject("cnpj", fornecedor != null && fornecedor.getCnpj() != null ? fornecedor.getCnpj().toString() : "")
				.addObject("logradouroCompletoComBairro", endereco != null ? endereco.getLogradouroCompleto() : "");
	}
	
	public void ajaxCarregaGrupotributacao(WebRequestContext request, Material material){
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		
		Cliente cliente = material.getCliente() != null && material.getCliente().getCdpessoa() != null ? clienteService.loadForTributacao(material.getCliente()) : null;
		Naturezaoperacao naturezaoperacao = material.getNaturezaoperacao();
		Empresa empresanota = null; 
		if (material.getEmpresaTributacaoOrigem() != null && material.getEmpresaTributacaoOrigem().getCdpessoa() != null){
			empresanota = material.getEmpresaTributacaoOrigem();
			Empresa empresa = empresaService.loadWithEndereco(material.getEmpresaTributacaoOrigem());
			material.setEnderecoTributacaoOrigem(empresa.getEndereco());
		} else {
			Empresa empresa = empresaService.loadPrincipalWithEndereco();
			material.setEnderecoTributacaoOrigem(empresa.getEndereco());
		}		
		
		Endereco enderecoDestinoCliente = null;
		if(material.getEnderecoavulso() != null && material.getEnderecoavulso() && material.getEndereconotaDestino() != null && 
				material.getEndereconotaDestino().getMunicipio() != null && 
				material.getEndereconotaDestino().getMunicipio().getCdmunicipio() != null){
			enderecoDestinoCliente =  material.getEndereconotaDestino();
			enderecoDestinoCliente.setMunicipio(municipioService.carregaMunicipio(material.getEndereconotaDestino().getMunicipio()));
		}else if(material.getEnderecoTributacaoDestino() != null && material.getEnderecoTributacaoDestino().getCdendereco() != null){
			enderecoDestinoCliente = enderecoService.carregaEnderecoComUfPais(material.getEnderecoTributacaoDestino());
		}
		
		List<Categoria> listaCategoriaCliente = cliente != null ? categoriaService.findByPessoa(cliente) : null;
		Material beanMaterial = materialService.loadForTributacao(material);
		
		List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
				grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.ENTRADA, 
																		empresanota, 
																		naturezaoperacao, 
																		beanMaterial.getMaterialgrupo(),
																		cliente,
																		true, 
																		beanMaterial, 
																		null,
																		enderecoDestinoCliente, 
																		null,
																		listaCategoriaCliente,
																		null,
																		material.getOperacaonfe(),
																		null,
																		null,
																		new Date(System.currentTimeMillis())));
		
		JSON listaGrupotributacaoJSON = View.convertToJson(listaGrupotributacao);
		String jsonGrupotributacao = listaGrupotributacaoJSON.toString(0);
		view.println("var listaGrupotributacao = " + jsonGrupotributacao);
		String cdgrupoTribuacaoJaInformado = request.getParameter("cdGrupoTributacaoDoItem");
		Grupotributacao grupotributacao = null;
		if(StringUtils.isNotEmpty(cdgrupoTribuacaoJaInformado)){
			grupotributacao = new Grupotributacao(Integer.parseInt(cdgrupoTribuacaoJaInformado));
		}else{
			grupotributacao = grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao);
		}
		
		if(grupotributacao != null){
			view.println("var grupotributacaoSelecionado = 'br.com.linkcom.sined.geral.bean.Grupotributacao[cdgrupotributacao=" + grupotributacao.getCdgrupotributacao() + "]';");
		}
	}
	
	public void ajaxOnChangeGrupotributacao(WebRequestContext request, Grupotributacao grupotributacao){
		notafiscalprodutoService.ajaxOnChangeGrupotributacao(request, grupotributacao);
	}
	
	public ModelAndView criarNotaFiscal(WebRequestContext request, DeclaracaoImportacaoBean bean){
		Notafiscalproduto notafiscalproduto = criaNotafiscalProduto(bean);
		request.getSession().setAttribute(ATTR_NOTAFISCALPRODUTO_DI_SESSAO, notafiscalproduto);
		return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto?ACAO=criar&importacaoXmlDi=true");
	}
	
	private Notafiscalproduto criaNotafiscalProduto(DeclaracaoImportacaoBean declaracaoImportacaoBean) {
		Notafiscalproduto nfp = new Notafiscalproduto();

		nfp.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFE);
		nfp.setEmpresa(declaracaoImportacaoBean.getEmpresa());
		nfp.setDtEmissao(new Date(System.currentTimeMillis()));
		nfp.setTipooperacaonota(Tipooperacaonota.ENTRADA);
		nfp.setNaturezaoperacao(declaracaoImportacaoBean.getNaturezaoperacao());
		nfp.setCadastrarcobranca(Boolean.FALSE);
		nfp.setCadastrartransportador(Boolean.TRUE);
		nfp.setPresencacompradornfe(Presencacompradornfe.OUTROS);
		nfp.setLocaldestinonfe(Localdestinonfe.EXTERIOR);
		
		if(declaracaoImportacaoBean.getCliente() != null){
			Cliente cliente = clienteService.carregarDadosCliente(declaracaoImportacaoBean.getCliente());
			nfp.setCliente(cliente);
			nfp.setEnderecoCliente(cliente.getEnderecoForDI());
		}
		if(declaracaoImportacaoBean.getTransportador() != null){
			Fornecedor fornecedor = fornecedorService.carregaFornecedor(declaracaoImportacaoBean.getTransportador());
			nfp.setTransportador(fornecedor);
			nfp.setEnderecotransportador(fornecedor.getEnderecoForDI());
		}
		
		nfp.setTotalcalculado(declaracaoImportacaoBean.getTotalcalculado());
		nfp.setValorprodutos(declaracaoImportacaoBean.getValorTotalProdutos());
		nfp.setValor(declaracaoImportacaoBean.getValorTotalNota());
		
		nfp.setValorpis(declaracaoImportacaoBean.getValorPis());
		nfp.setValorcofins(declaracaoImportacaoBean.getValorCofins());
		
		Localarmazenagem localprincipal = localarmazenagemService.loadPrincipalByEmpresa(nfp.getEmpresa());
		
		nfp.setListaItens(new ArrayList<Notafiscalprodutoitem>());
		if(SinedUtil.isListNotEmpty(declaracaoImportacaoBean.getListaItens())){
			HashMap<Integer, Grupotributacao> mapGrupotributacao = new HashMap<Integer, Grupotributacao>();
			HashMap<Integer, Cfop> mapCFOP = new HashMap<Integer, Cfop>();
			Material material;
			for(DeclaracaoImportacaoitemBean declaracaoImportacaoItemBean : declaracaoImportacaoBean.getListaItens()){
				Notafiscalprodutoitem item = new Notafiscalprodutoitem();
				item.setDadosimportacao(Boolean.TRUE);
				item.setIncluirvalorprodutos(Boolean.TRUE);
				
				if(declaracaoImportacaoItemBean.getMaterial() != null){
					material = materialService.unidadeMedidaMaterial(declaracaoImportacaoItemBean.getMaterial());
					item.setMaterial(declaracaoImportacaoItemBean.getMaterial());
					item.setUnidademedida(material.getUnidademedida());
				}
				item.setMaterialclasse(Materialclasse.PRODUTO);
				item.setGrupotributacao(declaracaoImportacaoItemBean.getGrupotributacao());
				item.setNcmcompleto(declaracaoImportacaoItemBean.getNcmcompleto());
				item.setLocalarmazenagem(localprincipal);
				
				if(declaracaoImportacaoItemBean.getCfop() != null){
					if(mapCFOP.get(declaracaoImportacaoItemBean.getCfop()) == null){
						mapCFOP.put(declaracaoImportacaoItemBean.getCfop().getCdcfop(), cfopService.load(declaracaoImportacaoItemBean.getCfop(), "cfop.cdcfop, cfop.codigo, cfop.descricaoresumida"));
					}
					item.setCfop(mapCFOP.get(declaracaoImportacaoItemBean.getCfop().getCdcfop()));
				}
				
				item.setQtde(declaracaoImportacaoItemBean.getQuantidade());
				item.setValorunitario(declaracaoImportacaoItemBean.getValorUnitario());
				item.setValorbruto(declaracaoImportacaoItemBean.getValorBruto() != null ? new Money(declaracaoImportacaoItemBean.getValorBruto()) : new Money());
				item.setValorfrete(declaracaoImportacaoItemBean.getValorFrete());
				item.setValordesconto(declaracaoImportacaoItemBean.getValorDesconto());
				item.setValorseguro(declaracaoImportacaoItemBean.getValorSeguro());
				item.setPesobruto(declaracaoImportacaoItemBean.getPesoBruto());
				item.setPesoliquido(declaracaoImportacaoItemBean.getPesoLiquido());
				item.setValoricms(new Money());
				item.setValoripi(declaracaoImportacaoItemBean.getValorIpi());
				item.setValoriss(declaracaoImportacaoItemBean.getValorIss());
				
				item.setValorbcii(new Money());
				item.setDespesasaduaneiras(new Money());
				item.setValorii(new Money());
				item.setValoriof(new Money());
				
				if(nfp.getListaItens().size() == 0){
					item.setOutrasdespesas(declaracaoImportacaoBean.getValorOutrasDespesas());
					item.setDespesasaduaneiras(declaracaoImportacaoBean.getValorDespesasAduaneiras());
				}
				
				if(declaracaoImportacaoBean.getDeclaracaoimportacao() != null && declaracaoImportacaoBean.getDeclaracaoimportacao().getCddeclaracaoimportacao() != null){
					item.setListaNotafiscalprodutoitemdi(new ListSet<Notafiscalprodutoitemdi>(Notafiscalprodutoitemdi.class));
					Notafiscalprodutoitemdi notafiscalprodutoitemdi = new Notafiscalprodutoitemdi();
					notafiscalprodutoitemdi.setDeclaracaoimportacao(declaracaoImportacaoBean.getDeclaracaoimportacao());
					item.getListaNotafiscalprodutoitemdi().add(notafiscalprodutoitemdi);
				}
				
				if(item.getGrupotributacao() != null && item.getGrupotributacao().getCdgrupotributacao() != null){
					if(mapGrupotributacao.get(item.getGrupotributacao().getCdgrupotributacao()) == null){
						mapGrupotributacao.put(item.getGrupotributacao().getCdgrupotributacao(), grupotributacaoService.loadForEntrada(item.getGrupotributacao()));
					}
					notafiscalprodutoitemService.preencherDadosTributacaoForDI(item, mapGrupotributacao.get(item.getGrupotributacao().getCdgrupotributacao()));
				}
				
				item.setValorbcpis(item.getValorbruto());
				item.setPis(declaracaoImportacaoBean.getPis());
				if(declaracaoImportacaoItemBean.getValorPis() != null){
					item.setValorpis(new Money(declaracaoImportacaoItemBean.getValorPis()));
				}
				
				item.setValorbccofins(item.getValorbruto());
				item.setCofins(declaracaoImportacaoBean.getCofins());
				if(declaracaoImportacaoItemBean.getValorCofins() != null){
					item.setValorcofins(new Money(declaracaoImportacaoItemBean.getValorCofins()));
				}
				
				nfp.getListaItens().add(item);
			}
		}
		
		setTotalNota(nfp);
		
		return nfp;
	}
	
	private void setTotalNota(Notafiscalproduto nf) {
		AjaxCalculaTotalValorNotaBean beanTot = notafiscalprodutoService.calculaTotalNota(nf.getFinalidadenfe(), nf.getListaItens());
		
		nf.setValornaoincluido(beanTot.getTotalNaoincluido());
		nf.setValorfrete(beanTot.getTotalFrete());
		nf.setValordesconto(beanTot.getTotalDesconto());
		nf.setValorseguro(beanTot.getTotalSeguro());
		nf.setOutrasdespesas(beanTot.getTotalOutrasdespesas());
		nf.setValortotaltributos(beanTot.getTotalTributos());
		nf.setValorTotalImpostoFederal(beanTot.getTotalImpostoFederal());
		nf.setValorTotalImpostoEstadual(beanTot.getTotalImpostoEstadual());
		nf.setValorTotalImpostoMunicipal(beanTot.getTotalImpostoMunicipal());
		nf.setValorbcicms(beanTot.getTotalBcicms());
		nf.setValoricms(beanTot.getTotalIcms());
		nf.setValorbcicmsst(beanTot.getTotalBcicmsst());
		nf.setValoricmsst(beanTot.getTotalIcmsst());
		nf.setValorii(beanTot.getTotalIi());
		nf.setValoripi(beanTot.getTotalIpi());
		nf.setValorservicos(beanTot.getTotalServicos());
		nf.setValorbciss(beanTot.getTotalBciss());
		nf.setValoriss(beanTot.getTotalIss());
		nf.setValorpisservicos(beanTot.getTotalPisservicos());
		nf.setValorcofinsservicos(beanTot.getTotalCofinsservicos());
		nf.setValordesoneracao(beanTot.getTotalIcmsdesonerado());
		nf.setValordeducaoservico(beanTot.getTotaldeducaoservico());
		nf.setValoroutrasretencoesservico(beanTot.getTotaloutrasretencoesservico());
		nf.setValordescontocondicionadoservico(beanTot.getTotaldescontocondicionadoservico());
		nf.setValordescontoincondicionadoservico(beanTot.getTotaldescontoincondicionadoservico());
		nf.setValorissretido(beanTot.getTotalissretido());
		nf.setPesobruto(beanTot.getPesobruto());
		nf.setPesoliquido(beanTot.getPesoliquido());
	}
}
