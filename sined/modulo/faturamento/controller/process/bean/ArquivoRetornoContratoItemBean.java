package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Material;

public class ArquivoRetornoContratoItemBean {
	
	private Date datageracao;
	private Date datavencimento;
	private String codigo;
	private String descricao;
	private Double qtde;
	private Double valortotal;
	private Material material;
	private String nomeMaterial;
	private Contratomaterial contratomaterial;
	
	public Date getDatageracao() {
		return datageracao;
	}
	public Date getDatavencimento() {
		return datavencimento;
	}
	public String getCodigo() {
		return codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public Double getQtde() {
		return qtde;
	}
	public Double getValortotal() {
		return valortotal;
	}
	public Material getMaterial() {
		return material;
	}
	public String getNomeMaterial() {
		return nomeMaterial;
	}
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	
	public void setDatageracao(Date datageracao) {
		this.datageracao = datageracao;
	}
	public void setDatavencimento(Date datavencimento) {
		this.datavencimento = datavencimento;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setValortotal(Double valortotal) {
		this.valortotal = valortotal;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setNomeMaterial(String nomeMaterial) {
		this.nomeMaterial = nomeMaterial;
	}
	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}
}