package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Modelodocumentofiscal;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.ResponsavelFrete;
import br.com.linkcom.sined.geral.bean.enumeration.Formapagamentonfe;

public class ImportacaoXmlNfeBean {
	
	private Integer tipoconsulta;
	private Empresa empresa;
	private Boolean agrupamentoMaterial = Boolean.FALSE;
	private String controller;
	private String whereInOrdemcompra;
	private List<Entregamaterial> listaMaterialEntrega;
	private ConferenciaDadosFornecedorBean conferenciaDadosFornecedorBean;
	protected Boolean fromPedidoColeta;
	protected String whereInPedido;
	protected Boolean fromColeta;
	protected String whereInColeta;
	protected Boolean receberEntregaByMaterialmestre;
	protected Boolean producaoautomatica;
	protected Integer cdordemcompra;
	
	private Cnpj cnpj;
	private Cpf cpf;
	private String razaosocial;
	private String nomefantasia;
	private String logradouro;
	private String numeroendereco;
	private String complemento;
	private String bairro;
	private String cmunicipio;
	private Integer cuf;
	private String siglauf;
	private Cep cep;
	private String fone;
	private String ie;
	private String iest;
	private String im;
	private String chaveacesso;
	
	private String natop;
	private Formapagamentonfe indpag;
	private Modelodocumentofiscal modelodocumentofiscal;
	private Integer serie;
	private String numero;
	private Date dtemissao;
	private Date dsaient;
	private String infadfisco;
	private String infcpl;
	private ResponsavelFrete responsavelFrete;
	private String transportadora;
	private String placaveiculo;
	
	private Money totalmercadoria;
	private Money totalbcicms;
	private Money totalicms;
	private Money totalbcicmsst;
	private Money totalicmsst;
	private Money totalseguro;
	private Money totalfrete;
	private Money totaldesconto;
	private Money totalipi;
	private Money totalpis;
	private Money totalcofins;
	private Money totalFcp;
	private Money totalFcpSt;
	private Money totalFcpStRet;
	private Money totaloutrasdespesas;
	
	private List<ImportacaoXmlNfeItemBean> listaItens;
	private List<ImportacaoXmlNfeAssociadaBean> listaNfeAssociadas;
	private List<ImportacaoXmlNfeDuplicataBean> listaDuplicatas;
	private List<Pedidovendamaterial> listaPedidovendamaterial;
	private Fornecedor fornecedor;
	
	private String errosImportacao;
	
	public ImportacaoXmlNfeBean() {
		super();
		this.listaItens = new ListSet<ImportacaoXmlNfeItemBean>(ImportacaoXmlNfeItemBean.class);
		this.listaNfeAssociadas = new ListSet<ImportacaoXmlNfeAssociadaBean>(ImportacaoXmlNfeAssociadaBean.class);
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public Integer getTipoconsulta() {
		return tipoconsulta;
	}
	
	public Money getTotalmercadoria() {
		return totalmercadoria;
	}

	public Money getTotalbcicms() {
		return totalbcicms;
	}
	public Money getTotalicms() {
		return totalicms;
	}
	public Money getTotalbcicmsst() {
		return totalbcicmsst;
	}
	public Money getTotalicmsst() {
		return totalicmsst;
	}
	public Money getTotalseguro() {
		return totalseguro;
	}
	public Money getTotalipi() {
		return totalipi;
	}
	public Money getTotalpis() {
		return totalpis;
	}
	public Money getTotalcofins() {
		return totalcofins;
	}
	public Money getTotaloutrasdespesas() {
		return totaloutrasdespesas;
	}
	public String getNatop() {
		return natop;
	}
	public Formapagamentonfe getIndpag() {
		return indpag;
	}
	public Modelodocumentofiscal getModelodocumentofiscal() {
		return modelodocumentofiscal;
	}
	public List<ImportacaoXmlNfeItemBean> getListaItens() {
		return listaItens;
	}
	public List<ImportacaoXmlNfeDuplicataBean> getListaDuplicatas() {
		return listaDuplicatas;
	}
	public List<ImportacaoXmlNfeAssociadaBean> getListaNfeAssociadas() {
		return listaNfeAssociadas;
	}
	public Integer getCuf() {
		return cuf;
	}
	public Integer getSerie() {
		return serie;
	}
	public String getNumero() {
		return numero;
	}
	public Date getDtemissao() {
		return dtemissao;
	}
	public Cnpj getCnpj() {
		return cnpj;
	}
	public Cpf getCpf() {
		return cpf;
	}
	public String getRazaosocial() {
		return razaosocial;
	}
	public String getNomefantasia() {
		return nomefantasia;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public String getNumeroendereco() {
		return numeroendereco;
	}
	public String getComplemento() {
		return complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public String getCmunicipio() {
		return cmunicipio;
	}
	public Cep getCep() {
		return cep;
	}
	public String getFone() {
		return fone;
	}
	public String getIe() {
		return ie;
	}
	public String getIest() {
		return iest;
	}
	public String getIm() {
		return im;
	}
	public Money getTotalfrete() {
		return totalfrete;
	}
	public Money getTotaldesconto() {
		return totaldesconto;
	}
	public String getWhereInOrdemcompra() {
		return whereInOrdemcompra;
	}
	public List<Entregamaterial> getListaMaterialEntrega() {
		return listaMaterialEntrega;
	}
	public ConferenciaDadosFornecedorBean getConferenciaDadosFornecedorBean() {
		return conferenciaDadosFornecedorBean;
	}
	public String getController() {
		return controller;
	}
	public Date getDsaient() {
		return dsaient;
	}
	public String getInfadfisco() {
		return infadfisco;
	}
	public String getInfcpl() {
		return infcpl;
	}
	public ResponsavelFrete getResponsavelFrete() {
		return responsavelFrete;
	}
	public String getTransportadora() {
		return transportadora;
	}
	public String getPlacaveiculo() {
		return placaveiculo;
	}
	public Boolean getAgrupamentoMaterial() {
		return agrupamentoMaterial;
	}
	public Boolean getProducaoautomatica() {
		return producaoautomatica;
	}
	public void setProducaoautomatica(Boolean producaoautomatica) {
		this.producaoautomatica = producaoautomatica;
	}
	public Integer getCdordemcompra() {
		return cdordemcompra;
	}
	public void setCdordemcompra(Integer cdordemcompra) {
		this.cdordemcompra = cdordemcompra;
	}
	public void setAgrupamentoMaterial(Boolean agrupamentoMaterial) {
		this.agrupamentoMaterial = agrupamentoMaterial;
	}
	public void setResponsavelFrete(ResponsavelFrete responsavelFrete) {
		this.responsavelFrete = responsavelFrete;
	}
	public void setTransportadora(String transportadora) {
		this.transportadora = transportadora;
	}
	public void setPlacaveiculo(String placaveiculo) {
		this.placaveiculo = placaveiculo;
	}
	public void setInfadfisco(String infadfisco) {
		this.infadfisco = infadfisco;
	}
	public void setInfcpl(String infcpl) {
		this.infcpl = infcpl;
	}
	public void setDsaient(Date dsaient) {
		this.dsaient = dsaient;
	}
	public void setController(String controller) {
		this.controller = controller;
	}
	public void setConferenciaDadosFornecedorBean(
			ConferenciaDadosFornecedorBean conferenciaDadosFornecedorBean) {
		this.conferenciaDadosFornecedorBean = conferenciaDadosFornecedorBean;
	}
	public void setListaMaterialEntrega(List<Entregamaterial> listaMaterialEntrega) {
		this.listaMaterialEntrega = listaMaterialEntrega;
	}
	public void setWhereInOrdemcompra(String whereInOrdemcompra) {
		this.whereInOrdemcompra = whereInOrdemcompra;
	}
	public void setTotalfrete(Money totalfrete) {
		this.totalfrete = totalfrete;
	}
	public void setTotaldesconto(Money totaldesconto) {
		this.totaldesconto = totaldesconto;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setNumeroendereco(String numeroendereco) {
		this.numeroendereco = numeroendereco;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setCmunicipio(String cmunicipio) {
		this.cmunicipio = cmunicipio;
	}
	public void setCep(Cep cep) {
		this.cep = cep;
	}
	public void setFone(String fone) {
		this.fone = fone;
	}
	public void setIe(String ie) {
		this.ie = ie;
	}
	public void setIest(String iest) {
		this.iest = iest;
	}
	public void setIm(String im) {
		this.im = im;
	}
	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}
	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	public void setDtemissao(Date dtemissao) {
		this.dtemissao = dtemissao;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setSerie(Integer serie) {
		this.serie = serie;
	}
	public void setCuf(Integer cuf) {
		this.cuf = cuf;
	}
	public void setListaItens(List<ImportacaoXmlNfeItemBean> listaItens) {
		this.listaItens = listaItens;
	}
	public void setListaDuplicatas(List<ImportacaoXmlNfeDuplicataBean> listaDuplicatas) {
		this.listaDuplicatas = listaDuplicatas;
	}
	public void setListaNfeAssociadas(List<ImportacaoXmlNfeAssociadaBean> listaNfeAssociadas) {
		this.listaNfeAssociadas = listaNfeAssociadas;
	}
	public void setTotalmercadoria(Money totalmercadoria) {
		this.totalmercadoria = totalmercadoria;
	}
	public void setTotalbcicms(Money totalbcicms) {
		this.totalbcicms = totalbcicms;
	}
	public void setTotalicms(Money totalicms) {
		this.totalicms = totalicms;
	}
	public void setTotalbcicmsst(Money totalbcicmsst) {
		this.totalbcicmsst = totalbcicmsst;
	}
	public void setTotalicmsst(Money totalicmsst) {
		this.totalicmsst = totalicmsst;
	}
	public void setTotalseguro(Money totalseguro) {
		this.totalseguro = totalseguro;
	}
	public void setTotalipi(Money totalipi) {
		this.totalipi = totalipi;
	}
	public void setTotalpis(Money totalpis) {
		this.totalpis = totalpis;
	}
	public void setTotalcofins(Money totalcofins) {
		this.totalcofins = totalcofins;
	}
	public void setTotaloutrasdespesas(Money totaloutrasdespesas) {
		this.totaloutrasdespesas = totaloutrasdespesas;
	}
	public void setNatop(String natop) {
		this.natop = natop;
	}
	public void setIndpag(Formapagamentonfe indpag) {
		this.indpag = indpag;
	}
	public void setModelodocumentofiscal(
			Modelodocumentofiscal modelodocumentofiscal) {
		this.modelodocumentofiscal = modelodocumentofiscal;
	}
	public String getChaveacesso() {
		return chaveacesso;
	}
	public void setChaveacesso(String chaveacesso) {
		this.chaveacesso = chaveacesso;
	}
	
	public void setTipoconsulta(Integer tipoconsulta) {
		this.tipoconsulta = tipoconsulta;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Boolean getFromPedidoColeta() {
		return fromPedidoColeta;
	}

	public String getWhereInPedido() {
		return whereInPedido;
	}

	public void setFromPedidoColeta(Boolean fromPedidoColeta) {
		this.fromPedidoColeta = fromPedidoColeta;
	}
	public void setWhereInPedido(String whereInPedido) {
		this.whereInPedido = whereInPedido;
	}
	
	public Boolean getFromColeta() {
		return fromColeta;
	}
	public void setFromColeta(Boolean fromColeta) {
		this.fromColeta = fromColeta;
	}

	public String getWhereInColeta() {
		return whereInColeta;
	}
	public void setWhereInColeta(String whereInColeta) {
		this.whereInColeta = whereInColeta;
	}

	public Boolean getReceberEntregaByMaterialmestre() {
		return receberEntregaByMaterialmestre;
	}

	public void setReceberEntregaByMaterialmestre(Boolean receberEntregaByMaterialmestre) {
		this.receberEntregaByMaterialmestre = receberEntregaByMaterialmestre;
	}
	
	public String getSiglauf() {
		return siglauf;
	}
	
	public void setSiglauf(String siglauf) {
		this.siglauf = siglauf;
	}

	public List<Pedidovendamaterial> getListaPedidovendamaterial() {
		return listaPedidovendamaterial;
	}

	public void setListaPedidovendamaterial(List<Pedidovendamaterial> listaPedidovendamaterial) {
		this.listaPedidovendamaterial = listaPedidovendamaterial;
	}

	public Money getTotalFcp() {
		return totalFcp;
	}

	public void setTotalFcp(Money totalFcp) {
		this.totalFcp = totalFcp;
	}

	public Money getTotalFcpSt() {
		return totalFcpSt;
	}

	public void setTotalFcpSt(Money totalFcpSt) {
		this.totalFcpSt = totalFcpSt;
	}
	
	public Money getTotalFcpStRet() {
		return totalFcpStRet;
	}
	
	public void setTotalFcpStRet(Money totalFcpStRet) {
		this.totalFcpStRet = totalFcpStRet;
	}
	
	public String getErrosImportacao() {
		return errosImportacao;
	}
	
	public void setErrosImportacao(String errosImportacao) {
		this.errosImportacao = errosImportacao;
	}
	
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
}
