package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import java.util.List;

import br.com.linkcom.neo.types.Money;

public class AjaxRealizarVendaPagamentoBean {
	
	private Integer parcela;
	private List<AjaxRealizarVendaPagamentoItemBean> lista;
	
	private Boolean isValorminimo = Boolean.TRUE;
	private Double juros;
	private Money valorMinimoPagamento;
	
	public Integer getParcela() {
		return parcela;
	}
	public List<AjaxRealizarVendaPagamentoItemBean> getLista() {
		return lista;
	}
	public void setParcela(Integer parcela) {
		this.parcela = parcela;
	}
	public void setLista(List<AjaxRealizarVendaPagamentoItemBean> lista) {
		this.lista = lista;
	}
	
	public Boolean getIsValorminimo() {
		return isValorminimo;
	}
	public Double getJuros() {
		return juros;
	}
	public void setIsValorminimo(Boolean isValorminimo) {
		this.isValorminimo = isValorminimo;
	}
	public void setJuros(Double juros) {
		this.juros = juros;
	}
	public Money getValorMinimoPagamento() {
		return valorMinimoPagamento;
	}
	public void setValorMinimoPagamento(Money valorMinimoPagamento) {
		this.valorMinimoPagamento = valorMinimoPagamento;
	}
}
