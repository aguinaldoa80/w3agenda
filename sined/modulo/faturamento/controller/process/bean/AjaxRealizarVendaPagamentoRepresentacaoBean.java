package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import java.util.List;

public class AjaxRealizarVendaPagamentoRepresentacaoBean {
	
	private Integer parcela;
	private List<AjaxRealizarVendaPagamentoRepresentacaoItemBean> lista;
	
	public Integer getParcela() {
		return parcela;
	}
	public List<AjaxRealizarVendaPagamentoRepresentacaoItemBean> getLista() {
		return lista;
	}
	public void setParcela(Integer parcela) {
		this.parcela = parcela;
	}
	public void setLista(List<AjaxRealizarVendaPagamentoRepresentacaoItemBean> lista) {
		this.lista = lista;
	}
}
