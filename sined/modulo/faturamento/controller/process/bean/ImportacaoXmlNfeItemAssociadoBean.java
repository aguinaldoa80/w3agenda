package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.util.SinedUtil;

public class ImportacaoXmlNfeItemAssociadoBean {
	
	private String whereInOrdemcompramaterial;
	private Material material;
	private Double qtdeassociada;
	
	private Double xmlqtdeinformada;
	
	public ImportacaoXmlNfeItemAssociadoBean(){}
			
	public ImportacaoXmlNfeItemAssociadoBean(Material material, String whereInOrdemcompramaterial, Double qtde) {
		this.material = material;
		this.whereInOrdemcompramaterial = whereInOrdemcompramaterial;
		this.qtdeassociada = qtde;
	}
	
	public String getWhereInOrdemcompramaterial() {
		return whereInOrdemcompramaterial;
	}
	public Material getMaterial() {
		return material;
	}
	public Double getQtdeassociada() {
		return qtdeassociada;
	}
	
	public void setWhereInOrdemcompramaterial(String whereInOrdemcompramaterial) {
		this.whereInOrdemcompramaterial = whereInOrdemcompramaterial;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setQtdeassociada(Double qtdeassociada) {
		this.qtdeassociada = qtdeassociada;
	}
	
	public Double getXmlqtdeinformada() {
		return xmlqtdeinformada;
	}

	public void setXmlqtdeinformada(Double xmlqtdeinformada) {
		this.xmlqtdeinformada = xmlqtdeinformada;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((material == null) ? 0 : material.hashCode());
		result = prime * result
				+ ((qtdeassociada == null) ? 0 : qtdeassociada.hashCode());
		result = prime
				* result
				+ ((whereInOrdemcompramaterial == null) ? 0
						: whereInOrdemcompramaterial.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImportacaoXmlNfeItemAssociadoBean other = (ImportacaoXmlNfeItemAssociadoBean) obj;
		if (material == null) {
			if (other.material != null)
				return false;
		} else if (!material.equals(other.material))
			return false;
		if (qtdeassociada == null) {
			if (other.qtdeassociada != null)
				return false;
		} else if (!qtdeassociada.equals(other.qtdeassociada) && !SinedUtil.round(qtdeassociada, 10).equals(SinedUtil.round(other.qtdeassociada, 10)))
			return false;
		if (whereInOrdemcompramaterial == null) {
			if (other.whereInOrdemcompramaterial != null)
				return false;
		} else if (!whereInOrdemcompramaterial
				.equals(other.whereInOrdemcompramaterial))
			return false;
		return true;
	}
}
