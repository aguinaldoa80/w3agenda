package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;

public class ImportacaoXmlNfeDuplicataBean {
	
	protected Integer sequencial;
	protected String numero;
	protected Date dtvencimento;
	protected Money valor;
	
	public Integer getSequencial() {
		return sequencial;
	}
	public String getNumero() {
		return numero;
	}
	public Date getDtvencimento() {
		return dtvencimento;
	}
	public Money getValor() {
		return valor;
	}
	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
}
