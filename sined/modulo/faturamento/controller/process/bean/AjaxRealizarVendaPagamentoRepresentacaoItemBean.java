package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import br.com.linkcom.sined.geral.bean.Fornecedor;



public class AjaxRealizarVendaPagamentoRepresentacaoItemBean {
	
	private String dtparcela;
	private String valor;
	private String valorjuros;
	private Fornecedor fornecedor;
	
	public AjaxRealizarVendaPagamentoRepresentacaoItemBean() {}
	
	public AjaxRealizarVendaPagamentoRepresentacaoItemBean(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	public String getDtparcela() {
		return dtparcela;
	}
	public String getValor() {
		return valor;
	}
	public void setDtparcela(String dtparcela) {
		this.dtparcela = dtparcela;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public String getValorjuros() {
		return valorjuros;
	}
	public void setValorjuros(String valorjuros) {
		this.valorjuros = valorjuros;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
}
