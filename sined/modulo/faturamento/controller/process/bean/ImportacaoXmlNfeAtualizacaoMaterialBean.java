package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Ncmcapitulo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoitemsped;

public class ImportacaoXmlNfeAtualizacaoMaterialBean {
	
	private Integer index;
	private Material material;
	
	protected Tipoitemsped tipoitemsped;
	
	protected Ncmcapitulo ncmcapitulo_bd;
	protected String codigobarras_bd;
	protected String ncmcompleto_bd;
	protected String extipi_bd;
	protected String codlistaservico_bd;
	
	protected Ncmcapitulo ncmcapitulo_xml;
	protected String codigobarras_xml;
	protected String ncmcompleto_xml;
	protected String extipi_xml;
	protected String codlistaservico_xml;
	
	public Integer getIndex() {
		return index;
	}
	public Material getMaterial() {
		return material;
	}
	public Tipoitemsped getTipoitemsped() {
		return tipoitemsped;
	}
	public Ncmcapitulo getNcmcapitulo_bd() {
		return ncmcapitulo_bd;
	}
	public String getCodigobarras_bd() {
		return codigobarras_bd;
	}
	public String getNcmcompleto_bd() {
		return ncmcompleto_bd;
	}
	public String getExtipi_bd() {
		return extipi_bd;
	}
	public String getCodlistaservico_bd() {
		return codlistaservico_bd;
	}
	public Ncmcapitulo getNcmcapitulo_xml() {
		return ncmcapitulo_xml;
	}
	public String getCodigobarras_xml() {
		return codigobarras_xml;
	}
	public String getNcmcompleto_xml() {
		return ncmcompleto_xml;
	}
	public String getExtipi_xml() {
		return extipi_xml;
	}
	public String getCodlistaservico_xml() {
		return codlistaservico_xml;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setTipoitemsped(Tipoitemsped tipoitemsped) {
		this.tipoitemsped = tipoitemsped;
	}
	public void setNcmcapitulo_bd(Ncmcapitulo ncmcapitulo_bd) {
		this.ncmcapitulo_bd = ncmcapitulo_bd;
	}
	public void setCodigobarras_bd(String codigobarras_bd) {
		this.codigobarras_bd = codigobarras_bd;
	}
	public void setNcmcompleto_bd(String ncmcompleto_bd) {
		this.ncmcompleto_bd = ncmcompleto_bd;
	}
	public void setExtipi_bd(String extipi_bd) {
		this.extipi_bd = extipi_bd;
	}
	public void setCodlistaservico_bd(String codlistaservico_bd) {
		this.codlistaservico_bd = codlistaservico_bd;
	}
	public void setNcmcapitulo_xml(Ncmcapitulo ncmcapitulo_xml) {
		this.ncmcapitulo_xml = ncmcapitulo_xml;
	}
	public void setCodigobarras_xml(String codigobarras_xml) {
		this.codigobarras_xml = codigobarras_xml;
	}
	public void setNcmcompleto_xml(String ncmcompleto_xml) {
		this.ncmcompleto_xml = ncmcompleto_xml;
	}
	public void setExtipi_xml(String extipi_xml) {
		this.extipi_xml = extipi_xml;
	}
	public void setCodlistaservico_xml(String codlistaservico_xml) {
		this.codlistaservico_xml = codlistaservico_xml;
	}
	
}
