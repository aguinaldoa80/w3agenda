package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;


public class ImpostoBean {
	
	private String nome;
	private String imposto;
	private Double valor;
	
	public String getNome() {
		return nome;
	}
	public Double getValor() {
		return valor;
	}
	public String getImposto() {
		return imposto;
	}
	public void setImposto(String imposto) {
		this.imposto = imposto;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}

}
