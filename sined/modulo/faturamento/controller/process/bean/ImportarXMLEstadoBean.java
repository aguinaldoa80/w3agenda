package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import java.util.List;

import br.com.linkcom.lknfe.xml.nfe.importacao.estadual.NotaXML;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;

public class ImportarXMLEstadoBean {
	
	private Boolean marcado;
	private List<ImportarXMLEstadoItemBean> listaItens;
	private Rateio rateio;
	private Boolean criarContareceber = Boolean.TRUE;
	private Naturezaoperacao naturezaoperacao;
	private NotaXML notaXML;
	private Integer indexlista;
	private String identificadortela;
	
	private Empresa empresa;
	private Cliente cliente;
	private Tipooperacaonota tipooperacaonota;
	private Money valornota;
	private Arquivo arquivoxml;
	
	private String mensagemerro;
	private Boolean informacaocompleta;
	
	public Rateio getRateio() {
		return rateio;
	}
	public List<ImportarXMLEstadoItemBean> getListaItens() {
		return listaItens;
	}
	public Boolean getCriarContareceber() {
		return criarContareceber;
	}
	public void setCriarContareceber(Boolean criarContareceber) {
		this.criarContareceber = criarContareceber;
	}
	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}
	public void setListaItens(List<ImportarXMLEstadoItemBean> listaItens) {
		this.listaItens = listaItens;
	}
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}
	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}
	public NotaXML getNotaXML() {
		return notaXML;
	}
	public void setNotaXML(NotaXML notaXML) {
		this.notaXML = notaXML;
	}
	public Integer getIndexlista() {
		return indexlista;
	}
	public void setIndexlista(Integer indexlista) {
		this.indexlista = indexlista;
	}
	public String getIdentificadortela() {
		return identificadortela;
	}
	public void setIdentificadortela(String identificadortela) {
		this.identificadortela = identificadortela;
	}
	public Boolean getMarcado() {
		return marcado;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Tipooperacaonota getTipooperacaonota() {
		return tipooperacaonota;
	}
	public Money getValornota() {
		return valornota;
	}
	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setTipooperacaonota(Tipooperacaonota tipooperacaonota) {
		this.tipooperacaonota = tipooperacaonota;
	}
	public void setValornota(Money valornota) {
		this.valornota = valornota;
	}
	public Arquivo getArquivoxml() {
		return arquivoxml;
	}
	public void setArquivoxml(Arquivo arquivoxml) {
		this.arquivoxml = arquivoxml;
	}
	public Boolean getInformacaocompleta() {
		return informacaocompleta;
	}
	public void setInformacaocompleta(Boolean informacaocompleta) {
		this.informacaocompleta = informacaocompleta;
	}
	public String getMensagemerro() {
		return mensagemerro;
	}
	public void setMensagemerro(String mensagemerro) {
		this.mensagemerro = mensagemerro;
	}
}
