package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Entregadocumento;

public class ImportacaoXmlNfeAssociadaBean {
	
	private String chaveacesso;
	private List<Entregadocumento> listaNfe;

	public String getChaveacesso() {
		return chaveacesso;
	}
	public void setChaveacesso(String chaveacesso) {
		this.chaveacesso = chaveacesso;
	}
	
	public List<Entregadocumento> getListaNfe() {
		return listaNfe;
	}
	public void setListaNfe(List<Entregadocumento> listaNfe) {
		this.listaNfe = listaNfe;
	}
}
