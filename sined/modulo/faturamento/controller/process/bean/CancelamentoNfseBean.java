package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.MotivoCancelamentoFaturamento;

public class CancelamentoNfseBean {
	
	private String whereIn;
	private String motivo;
	private Boolean cancelamentoVenda = Boolean.FALSE;
	private MotivoCancelamentoFaturamento motivoCancelamentoFaturamento;
	
	public String getWhereIn() {
		return whereIn;
	}
	public String getMotivo() {
		return motivo;
	}
	public Boolean getCancelamentoVenda() {
		return cancelamentoVenda;
	}
	public void setCancelamentoVenda(Boolean cancelamentoVenda) {
		this.cancelamentoVenda = cancelamentoVenda;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	@DisplayName("Motivo de cancelamento")
	public MotivoCancelamentoFaturamento getMotivoCancelamentoFaturamento() {
		return motivoCancelamentoFaturamento;
	}
	public void setMotivoCancelamentoFaturamento(
			MotivoCancelamentoFaturamento motivoCancelamentoFaturamento) {
		this.motivoCancelamentoFaturamento = motivoCancelamentoFaturamento;
	}

}
