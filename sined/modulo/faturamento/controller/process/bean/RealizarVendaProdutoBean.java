package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Material;

public class RealizarVendaProdutoBean {
	
	private Integer id;
	private Material material;
	private Money valor;
	private Double quantidade;
		
	public Integer getId() {
		return id;
	}
	public Material getMaterial() {
		return material;
	}
	public Money getValor() {
		return valor;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Money getValortotal() {
		
		Money totalRetorno = new Money(0);
		if (valor!=null && quantidade!=null){
			totalRetorno = new Money(valor.multiply(new Money(quantidade)));
		}
		
		return totalRetorno;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}		
}