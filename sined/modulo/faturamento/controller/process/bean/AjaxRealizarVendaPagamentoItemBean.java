package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;



public class AjaxRealizarVendaPagamentoItemBean {
	
	private String dtparcela;
	private String valor;
	private String valorjuros;
	
	private Date dtparceladuplicata;
	private Money valorduplicata;
	
	public String getDtparcela() {
		return dtparcela;
	}
	public String getValor() {
		return valor;
	}
	public void setDtparcela(String dtparcela) {
		this.dtparcela = dtparcela;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public String getValorjuros() {
		return valorjuros;
	}
	public void setValorjuros(String valorjuros) {
		this.valorjuros = valorjuros;
	}
	
	public Date getDtparceladuplicata() {
		return dtparceladuplicata;
	}
	public Money getValorduplicata() {
		return valorduplicata;
	}
	public void setDtparceladuplicata(Date dtparceladuplicata) {
		this.dtparceladuplicata = dtparceladuplicata;
	}
	public void setValorduplicata(Money valorduplicata) {
		this.valorduplicata = valorduplicata;
	}
}
