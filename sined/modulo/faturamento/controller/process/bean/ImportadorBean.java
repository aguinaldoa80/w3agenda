package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;



public class ImportadorBean {

	private String msgErro;
	private Boolean erro = Boolean.FALSE;
	private Integer numeroNota;
	
	public ImportadorBean() {
	}
	
	public ImportadorBean(String msgErro, Boolean erro, Integer numeroNota) {
		this.msgErro = msgErro;
		this.erro = erro;
		this.numeroNota = numeroNota;
	}

	public String getMsgErro() {
		return msgErro;
	}
	public Boolean getErro() {
		return erro;
	}
	public Integer getNumeroNota() {
		return numeroNota;
	}
	public void setMsgErro(String msgErro) {
		this.msgErro = msgErro;
	}
	public void setErro(Boolean erro) {
		this.erro = erro;
	}
	public void setNumeroNota(Integer numeroNota) {
		this.numeroNota = numeroNota;
	}
	
}
