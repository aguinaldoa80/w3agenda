package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;

public class SelecionarConfiguracaoBean {

	private String whereIn;
	private Empresa empresa;
	private Configuracaonfe configuracaonfe;
	private String operacao;
	private Boolean nfe;

	public String getWhereIn() {
		return whereIn;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	@Required
	public Configuracaonfe getConfiguracaonfe() {
		return configuracaonfe;
	}

	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setConfiguracaonfe(Configuracaonfe configuracaonfe) {
		this.configuracaonfe = configuracaonfe;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
	
	public Boolean getNfe() {
		return nfe;
	}
	
	public void setNfe(Boolean nfe) {
		this.nfe = nfe;
	}
}