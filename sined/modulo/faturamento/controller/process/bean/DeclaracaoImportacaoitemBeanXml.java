package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;


public class DeclaracaoImportacaoitemBeanXml {

	private String descricaoMercadoria;
	private String dadosMercadoriaCodigoNcm;
	private Double quantidade;
	private String dadosMercadoriaMedidaEstatisticaUnidade;
	private String ncm;
	private Double valorUnitario;
	
	public String getDescricaoMercadoria() {
		return descricaoMercadoria;
	}
	public String getDadosMercadoriaCodigoNcm() {
		return dadosMercadoriaCodigoNcm;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public String getDadosMercadoriaMedidaEstatisticaUnidade() {
		return dadosMercadoriaMedidaEstatisticaUnidade;
	}
	public String getNcm() {
		return ncm;
	}
	public Double getValorUnitario() {
		return valorUnitario;
	}
	public void setDescricaoMercadoria(String descricaoMercadoria) {
		this.descricaoMercadoria = descricaoMercadoria;
	}
	public void setDadosMercadoriaCodigoNcm(String dadosMercadoriaCodigoNcm) {
		this.dadosMercadoriaCodigoNcm = dadosMercadoriaCodigoNcm;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setDadosMercadoriaMedidaEstatisticaUnidade(
			String dadosMercadoriaMedidaEstatisticaUnidade) {
		this.dadosMercadoriaMedidaEstatisticaUnidade = dadosMercadoriaMedidaEstatisticaUnidade;
	}
	public void setNcm(String ncm) {
		this.ncm = ncm;
	}
	public void setValorUnitario(Double valorUnitario) {
		this.valorUnitario = valorUnitario;
	}
}
