package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import br.com.linkcom.neo.types.Money;

public class Aux_imposto {

	protected Money valorbcicmsst;
	protected Money valoricmsst;
	
	public Aux_imposto() {}

	public Aux_imposto(Money valorbcicmsst, Money valoricmsst){
		this.valorbcicmsst = valorbcicmsst != null ? valorbcicmsst : new Money();
		this.valoricmsst = valoricmsst != null ? valoricmsst : new Money();
	}
	
	public Money getValorbcicmsst() {
		return valorbcicmsst;
	}
	public Money getValoricmsst() {
		return valoricmsst;
	}
	public void setValorbcicmsst(Money valorbcicmsst) {
		this.valorbcicmsst = valorbcicmsst;
	}
	public void setValoricmsst(Money valoricmsst) {
		this.valoricmsst = valoricmsst;
	}
}
