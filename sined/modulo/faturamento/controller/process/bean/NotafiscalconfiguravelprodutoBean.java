package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresaconfiguracaonfproduto;

public class NotafiscalconfiguravelprodutoBean {
	
	private String selectedItens;
	private Empresaconfiguracaonfproduto empresaconfiguracaonfproduto;	
	
	public String getSelectedItens() {
		return selectedItens;
	}
	@Required
	@DisplayName("Configuração")
	public Empresaconfiguracaonfproduto getEmpresaconfiguracaonfproduto() {
		return empresaconfiguracaonfproduto;
	}
	public void setSelectedItens(String selectedItens) {
		this.selectedItens = selectedItens;
	}
	public void setEmpresaconfiguracaonfproduto(Empresaconfiguracaonfproduto empresaconfiguracaonfproduto) {
		this.empresaconfiguracaonfproduto = empresaconfiguracaonfproduto;
	}
	
}