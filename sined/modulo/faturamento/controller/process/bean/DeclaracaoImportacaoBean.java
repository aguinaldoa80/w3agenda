package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Declaracaoimportacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;


public class DeclaracaoImportacaoBean {

	private DeclaracaoImportacaoBeanXml beanXml;
	private Empresa empresa;
	private Declaracaoimportacao declaracaoimportacao;
	private Cliente cliente;
	private Endereco enderecoCliente;
	private Fornecedor transportador;
	private Naturezaoperacao naturezaoperacao;
	
	private Double taxaCambial;
	private Money valorTaxaCapatazias;
	private Double margemContribuicao;
	private Money valorReceita;
	private Double pis;
	private Money valorPis;
	private Double cofins;
	private Money valorCofins;
	private Money valorOutrasDespesas;
	private Money valorDespesasAduaneiras;
	private Money valorTotalProdutos;
	private Money valorTotalNota;
	private Boolean totalcalculado = false;
	
	private List<DeclaracaoImportacaoitemBean> listaItens;

	public Empresa getEmpresa() {
		return empresa;
	}

	public Declaracaoimportacao getDeclaracaoimportacao() {
		return declaracaoimportacao;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public Endereco getEnderecoCliente() {
		return enderecoCliente;
	}

	public Fornecedor getTransportador() {
		return transportador;
	}

	@DisplayName("Natureza de Opera��o")
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}

	public Double getTaxaCambial() {
		return taxaCambial;
	}

	@DisplayName("Al�quota PIS (%)")
	public Double getPis() {
		return pis;
	}

	public Money getValorPis() {
		return valorPis;
	}
	
	@DisplayName("Al�quota COFINS (%)")
	public Double getCofins() {
		return cofins;
	}

	public Money getValorCofins() {
		return valorCofins;
	}

	public Money getValorOutrasDespesas() {
		return valorOutrasDespesas;
	}

	public Money getValorDespesasAduaneiras() {
		return valorDespesasAduaneiras;
	}

	public Money getValorTotalProdutos() {
		return valorTotalProdutos;
	}

	public Money getValorTotalNota() {
		return valorTotalNota;
	}

	public List<DeclaracaoImportacaoitemBean> getListaItens() {
		return listaItens;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setDeclaracaoimportacao(Declaracaoimportacao declaracaoimportacao) {
		this.declaracaoimportacao = declaracaoimportacao;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setEnderecoCliente(Endereco enderecoCliente) {
		this.enderecoCliente = enderecoCliente;
	}

	public void setTransportador(Fornecedor transportador) {
		this.transportador = transportador;
	}

	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}

	public void setTaxaCambial(Double taxaCambial) {
		this.taxaCambial = taxaCambial;
	}

	public void setPis(Double pis) {
		this.pis = pis;
	}

	public void setValorPis(Money valorPis) {
		this.valorPis = valorPis;
	}

	public void setCofins(Double cofins) {
		this.cofins = cofins;
	}

	public void setValorCofins(Money valorCofins) {
		this.valorCofins = valorCofins;
	}

	public void setValorOutrasDespesas(Money valorOutrasDespesas) {
		this.valorOutrasDespesas = valorOutrasDespesas;
	}

	public void setValorDespesasAduaneiras(Money valorDespesasAduaneiras) {
		this.valorDespesasAduaneiras = valorDespesasAduaneiras;
	}

	public void setValorTotalProdutos(Money valorTotalProdutos) {
		this.valorTotalProdutos = valorTotalProdutos;
	}

	public void setValorTotalNota(Money valorTotalNota) {
		this.valorTotalNota = valorTotalNota;
	}

	public void setListaItens(List<DeclaracaoImportacaoitemBean> listaItens) {
		this.listaItens = listaItens;
	}

	public DeclaracaoImportacaoBeanXml getBeanXml() {
		return beanXml;
	}

	public void setBeanXml(DeclaracaoImportacaoBeanXml beanXml) {
		this.beanXml = beanXml;
	}

	public Money getValorTaxaCapatazias() {
		return valorTaxaCapatazias;
	}

	public void setValorTaxaCapatazias(Money valorTaxaCapatazias) {
		this.valorTaxaCapatazias = valorTaxaCapatazias;
	}

	public Double getMargemContribuicao() {
		return margemContribuicao;
	}

	public void setMargemContribuicao(Double margemContribuicao) {
		this.margemContribuicao = margemContribuicao;
	}

	public Money getValorReceita() {
		return valorReceita;
	}

	public void setValorReceita(Money valorReceita) {
		this.valorReceita = valorReceita;
	}

	public Boolean getTotalcalculado() {
		return totalcalculado;
	}

	public void setTotalcalculado(Boolean totalcalculado) {
		this.totalcalculado = totalcalculado;
	}
	
}
