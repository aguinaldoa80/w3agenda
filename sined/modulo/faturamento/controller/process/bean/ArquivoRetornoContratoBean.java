package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Rateio;

public class ArquivoRetornoContratoBean {
	
	private Boolean selecao;
	private String numeroremessa;
	private Contrato contrato;
	private Money valorArquivo;
	private Rateio rateio;
	private Money valorContrato;
	private List<ArquivoRetornoContratoItemBean> listaItens;
	
	public Boolean getSelecao() {
		return selecao;
	}
	public String getNumeroremessa() {
		return numeroremessa;
	}
	public Contrato getContrato() {
		return contrato;
	}
	public Money getValorArquivo() {
		return valorArquivo;
	}
	public Rateio getRateio() {
		return rateio;
	}
	public Money getValorContrato() {
		return valorContrato;
	}
	public List<ArquivoRetornoContratoItemBean> getListaItens() {
		return listaItens;
	}
	
	public void setSelecao(Boolean selecao) {
		this.selecao = selecao;
	}
	public void setNumeroremessa(String numeroremessa) {
		this.numeroremessa = numeroremessa;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public void setValorArquivo(Money valorArquivo) {
		this.valorArquivo = valorArquivo;
	}
	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}
	public void setValorContrato(Money valorContrato) {
		this.valorContrato = valorContrato;
	}
	public void setListaItens(List<ArquivoRetornoContratoItemBean> listaItens) {
		this.listaItens = listaItens;
	}
}