package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

public class RegistrarCCeBean {
	
	private String whereIn;
	private String correcao;
	
	public String getWhereIn() {
		return whereIn;
	}
	public String getCorrecao() {
		return correcao;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	public void setCorrecao(String correcao) {
		this.correcao = correcao;
	}

}
