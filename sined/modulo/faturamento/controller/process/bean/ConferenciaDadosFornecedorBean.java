package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Uf;

public class ConferenciaDadosFornecedorBean {
	
	private Integer cdpessoa;
	
	private Tipopessoa tipopessoa_xml;
	private Tipopessoa tipopessoa_bd;
	private Cnpj cnpj_xml;
	private Cnpj cnpj_bd;
	private Cpf cpf_xml;
	private Cpf cpf_bd;
	private String razaosocial_xml;
	private String razaosocial_bd;
	private String nomefantasia_xml;
	private String nomefantasia_bd;
	private String ie_xml;
	private String ie_bd;
	private String im_xml;
	private String im_bd;
	private String logradouro_xml;
	private String logradouro_bd;
	private String numero_xml;
	private String numero_bd;
	private String complemento_xml;
	private String complemento_bd;
	private String bairro_xml;
	private String bairro_bd;
	private Municipio municipio_xml;
	private Municipio municipio_bd;
	private Uf uf_xml;
	private Uf uf_bd;
	private Cep cep_xml;
	private Cep cep_bd;
	
	public Tipopessoa getTipopessoa_xml() {
		return tipopessoa_xml;
	}
	public Tipopessoa getTipopessoa_bd() {
		return tipopessoa_bd;
	}
	public Cnpj getCnpj_xml() {
		return cnpj_xml;
	}
	public Cnpj getCnpj_bd() {
		return cnpj_bd;
	}
	public Cpf getCpf_xml() {
		return cpf_xml;
	}
	public Cpf getCpf_bd() {
		return cpf_bd;
	}
	public String getRazaosocial_xml() {
		return razaosocial_xml;
	}
	public String getRazaosocial_bd() {
		return razaosocial_bd;
	}
	public String getNomefantasia_xml() {
		return nomefantasia_xml;
	}
	public String getNomefantasia_bd() {
		return nomefantasia_bd;
	}
	public String getIe_xml() {
		return ie_xml;
	}
	public String getIe_bd() {
		return ie_bd;
	}
	public String getIm_xml() {
		return im_xml;
	}
	public String getIm_bd() {
		return im_bd;
	}
	public String getLogradouro_xml() {
		return logradouro_xml;
	}
	public String getLogradouro_bd() {
		return logradouro_bd;
	}
	public String getNumero_xml() {
		return numero_xml;
	}
	public String getNumero_bd() {
		return numero_bd;
	}
	public String getComplemento_xml() {
		return complemento_xml;
	}
	public String getComplemento_bd() {
		return complemento_bd;
	}
	public String getBairro_xml() {
		return bairro_xml;
	}
	public String getBairro_bd() {
		return bairro_bd;
	}
	public Municipio getMunicipio_xml() {
		return municipio_xml;
	}
	public Municipio getMunicipio_bd() {
		return municipio_bd;
	}
	public Uf getUf_xml() {
		return uf_xml;
	}
	public Uf getUf_bd() {
		return uf_bd;
	}
	public Cep getCep_xml() {
		return cep_xml;
	}
	public Cep getCep_bd() {
		return cep_bd;
	}
	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setTipopessoa_xml(Tipopessoa tipopessoa_xml) {
		this.tipopessoa_xml = tipopessoa_xml;
	}
	public void setTipopessoa_bd(Tipopessoa tipopessoa_bd) {
		this.tipopessoa_bd = tipopessoa_bd;
	}
	public void setCnpj_xml(Cnpj cnpj_xml) {
		this.cnpj_xml = cnpj_xml;
	}
	public void setCnpj_bd(Cnpj cnpj_bd) {
		this.cnpj_bd = cnpj_bd;
	}
	public void setCpf_xml(Cpf cpf_xml) {
		this.cpf_xml = cpf_xml;
	}
	public void setCpf_bd(Cpf cpf_bd) {
		this.cpf_bd = cpf_bd;
	}
	public void setRazaosocial_xml(String razaosocial_xml) {
		this.razaosocial_xml = razaosocial_xml;
	}
	public void setRazaosocial_bd(String razaosocial_bd) {
		this.razaosocial_bd = razaosocial_bd;
	}
	public void setNomefantasia_xml(String nomefantasia_xml) {
		this.nomefantasia_xml = nomefantasia_xml;
	}
	public void setNomefantasia_bd(String nomefantasia_bd) {
		this.nomefantasia_bd = nomefantasia_bd;
	}
	public void setIe_xml(String ie_xml) {
		this.ie_xml = ie_xml;
	}
	public void setIe_bd(String ie_bd) {
		this.ie_bd = ie_bd;
	}
	public void setIm_xml(String im_xml) {
		this.im_xml = im_xml;
	}
	public void setIm_bd(String im_bd) {
		this.im_bd = im_bd;
	}
	public void setLogradouro_xml(String logradouro_xml) {
		this.logradouro_xml = logradouro_xml;
	}
	public void setLogradouro_bd(String logradouro_bd) {
		this.logradouro_bd = logradouro_bd;
	}
	public void setNumero_xml(String numero_xml) {
		this.numero_xml = numero_xml;
	}
	public void setNumero_bd(String numero_bd) {
		this.numero_bd = numero_bd;
	}
	public void setComplemento_xml(String complemento_xml) {
		this.complemento_xml = complemento_xml;
	}
	public void setComplemento_bd(String complemento_bd) {
		this.complemento_bd = complemento_bd;
	}
	public void setBairro_xml(String bairro_xml) {
		this.bairro_xml = bairro_xml;
	}
	public void setBairro_bd(String bairro_bd) {
		this.bairro_bd = bairro_bd;
	}
	public void setMunicipio_xml(Municipio municipio_xml) {
		this.municipio_xml = municipio_xml;
	}
	public void setMunicipio_bd(Municipio municipio_bd) {
		this.municipio_bd = municipio_bd;
	}
	public void setUf_xml(Uf uf_xml) {
		this.uf_xml = uf_xml;
	}
	public void setUf_bd(Uf uf_bd) {
		this.uf_bd = uf_bd;
	}
	public void setCep_xml(Cep cep_xml) {
		this.cep_xml = cep_xml;
	}
	public void setCep_bd(Cep cep_bd) {
		this.cep_bd = cep_bd;
	}
	
	public String getName_tipopessoa() {
		return getTipopessoa_xml().name();
	}

}
