package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;

public class AjaxCalculaTotalValorNotaBean {

	private Money totalNota = new Money();
	private Money totalNaoincluido = new Money();
	private Money totalProdutos = new Money();
	private Money totalFrete = new Money();
	private Money totalDesconto = new Money();
	private Money totalSeguro = new Money();
	private Money totalOutrasdespesas = new Money();
	private Money totalBcicms = new Money();
	private Money totalIcms = new Money();
	private Money totalBcicmsst = new Money();
	private Money totalIcmsst = new Money();
	private Money totalIi = new Money();
	private Money totalIpi = new Money();
	private Money totalPis = new Money();
	private Money totalCofins = new Money();
	private Money totalServicos = new Money();
	private Money totalBciss = new Money();
	private Money totalIss = new Money();
	private Money totalBccsll = new Money();
	private Money totalCsll = new Money();
	private Money totalBcir = new Money();
	private Money totalIr = new Money();
	private Money totalBcinss = new Money();
	private Money totalInss = new Money();
	private Money totalPisservicos = new Money();
	private Money totalCofinsservicos = new Money();
	private Money totalTributos = new Money();
	private Money totalImpostoFederal = new Money();
	private Money totalImpostoEstadual = new Money();
	private Money totalImpostoMunicipal = new Money();
	private Money totalIcmsdesonerado = new Money();
	private Money totaldeducaoservico = new Money();
	private Money totaloutrasretencoesservico = new Money();
	private Money totaldescontocondicionadoservico = new Money();
	private Money totaldescontoincondicionadoservico = new Money();
	private Money totalissretido = new Money();
	private Money totalFcp = new Money();
	private Money totalFcpst = new Money();
	private Money totalFcpstretidoanteriormente = new Money();
	private Money totalIpidevolvido = new Money();
	
	private Double pesoliquido = 0d;
	private Double pesobruto = 0d;
	private Double qtdeTotal = 0d;
	private Money totalNotaReceita = new Money();
	private Localdestinonfe localdestinonfe = Localdestinonfe.INTERNA;
	
	private boolean haveServico = false;

	public Money getTotalNota() {
		return totalNota;
	}

	public Money getTotalNaoincluido() {
		return totalNaoincluido;
	}

	public Money getTotalProdutos() {
		return totalProdutos;
	}

	public Money getTotalFrete() {
		return totalFrete;
	}

	public Money getTotalDesconto() {
		return totalDesconto;
	}

	public Money getTotalSeguro() {
		return totalSeguro;
	}

	public Money getTotalOutrasdespesas() {
		return totalOutrasdespesas;
	}

	public Money getTotalBcicms() {
		return totalBcicms;
	}

	public Money getTotalIcms() {
		return totalIcms;
	}

	public Money getTotalBcicmsst() {
		return totalBcicmsst;
	}

	public Money getTotalIcmsst() {
		return totalIcmsst;
	}

	public Money getTotalIi() {
		return totalIi;
	}

	public Money getTotalIpi() {
		return totalIpi;
	}

	public Money getTotalPis() {
		return totalPis;
	}

	public Money getTotalCofins() {
		return totalCofins;
	}

	public Money getTotalServicos() {
		return totalServicos;
	}

	public Money getTotalBciss() {
		return totalBciss;
	}

	public Money getTotalIss() {
		return totalIss;
	}
	
	public Money getTotalBccsll() {
		return totalBccsll;
	}

	public Money getTotalCsll() {
		return totalCsll;
	}
	
	public Money getTotalBcir() {
		return totalBcir;
	}

	public Money getTotalIr() {
		return totalIr;
	}
	
	public Money getTotalBcinss() {
		return totalBcinss;
	}

	public Money getTotalInss() {
		return totalInss;
	}

	public Money getTotalPisservicos() {
		return totalPisservicos;
	}

	public Money getTotalCofinsservicos() {
		return totalCofinsservicos;
	}

	public Money getTotalTributos() {
		return totalTributos;
	}
	
	public Money getTotalImpostoFederal() {
		return totalImpostoFederal;
	}
	
	public Money getTotalImpostoEstadual() {
		return totalImpostoEstadual;
	}
	
	public Money getTotalImpostoMunicipal() {
		return totalImpostoMunicipal;
	}

	public Double getPesoliquido() {
		return pesoliquido;
	}

	public Double getPesobruto() {
		return pesobruto;
	}

	public Double getQtdeTotal() {
		return qtdeTotal;
	}

	public boolean isHaveServico() {
		return haveServico;
	}
	
	public Money getTotalIcmsdesonerado() {
		return totalIcmsdesonerado;
	}
	
	public Money getTotaldeducaoservico() {
		return totaldeducaoservico;
	}

	public Money getTotaloutrasretencoesservico() {
		return totaloutrasretencoesservico;
	}

	public Money getTotaldescontocondicionadoservico() {
		return totaldescontocondicionadoservico;
	}

	public Money getTotaldescontoincondicionadoservico() {
		return totaldescontoincondicionadoservico;
	}

	public Money getTotalissretido() {
		return totalissretido;
	}
	
	public Localdestinonfe getLocaldestinonfe() {
		return localdestinonfe;
	}
	
	public Money getTotalFcp() {
		return totalFcp;
	}

	public Money getTotalFcpst() {
		return totalFcpst;
	}

	public Money getTotalFcpstretidoanteriormente() {
		return totalFcpstretidoanteriormente;
	}

	public Money getTotalIpidevolvido() {
		return totalIpidevolvido;
	}

	public void setTotalFcp(Money totalFcp) {
		this.totalFcp = totalFcp;
	}

	public void setTotalFcpst(Money totalFcpst) {
		this.totalFcpst = totalFcpst;
	}

	public void setTotalFcpstretidoanteriormente(Money totalFcpstretidoanteriormente) {
		this.totalFcpstretidoanteriormente = totalFcpstretidoanteriormente;
	}

	public void setTotalIpidevolvido(Money totalIpidevolvido) {
		this.totalIpidevolvido = totalIpidevolvido;
	}

	public void setLocaldestinonfe(Localdestinonfe localdestinonfe) {
		this.localdestinonfe = localdestinonfe;
	}

	public void setTotaldeducaoservico(Money totaldeducaoservico) {
		this.totaldeducaoservico = totaldeducaoservico;
	}

	public void setTotaloutrasretencoesservico(Money totaloutrasretencoesservico) {
		this.totaloutrasretencoesservico = totaloutrasretencoesservico;
	}

	public void setTotaldescontocondicionadoservico(
			Money totaldescontocondicionadoservico) {
		this.totaldescontocondicionadoservico = totaldescontocondicionadoservico;
	}

	public void setTotaldescontoincondicionadoservico(
			Money totaldescontoincondicionadoservico) {
		this.totaldescontoincondicionadoservico = totaldescontoincondicionadoservico;
	}

	public void setTotalissretido(Money totalissretido) {
		this.totalissretido = totalissretido;
	}

	public void setTotalIcmsdesonerado(Money totalIcmsdesonerado) {
		this.totalIcmsdesonerado = totalIcmsdesonerado;
	}

	public void setTotalNota(Money totalNota) {
		this.totalNota = totalNota;
	}

	public void setTotalNaoincluido(Money totalNaoincluido) {
		this.totalNaoincluido = totalNaoincluido;
	}

	public void setTotalProdutos(Money totalProdutos) {
		this.totalProdutos = totalProdutos;
	}

	public void setTotalFrete(Money totalFrete) {
		this.totalFrete = totalFrete;
	}

	public void setTotalDesconto(Money totalDesconto) {
		this.totalDesconto = totalDesconto;
	}

	public void setTotalSeguro(Money totalSeguro) {
		this.totalSeguro = totalSeguro;
	}

	public void setTotalOutrasdespesas(Money totalOutrasdespesas) {
		this.totalOutrasdespesas = totalOutrasdespesas;
	}

	public void setTotalBcicms(Money totalBcicms) {
		this.totalBcicms = totalBcicms;
	}

	public void setTotalIcms(Money totalIcms) {
		this.totalIcms = totalIcms;
	}

	public void setTotalBcicmsst(Money totalBcicmsst) {
		this.totalBcicmsst = totalBcicmsst;
	}

	public void setTotalIcmsst(Money totalIcmsst) {
		this.totalIcmsst = totalIcmsst;
	}

	public void setTotalIi(Money totalIi) {
		this.totalIi = totalIi;
	}

	public void setTotalIpi(Money totalIpi) {
		this.totalIpi = totalIpi;
	}

	public void setTotalPis(Money totalPis) {
		this.totalPis = totalPis;
	}

	public void setTotalCofins(Money totalCofins) {
		this.totalCofins = totalCofins;
	}

	public void setTotalServicos(Money totalServicos) {
		this.totalServicos = totalServicos;
	}

	public void setTotalBciss(Money totalBciss) {
		this.totalBciss = totalBciss;
	}

	public void setTotalIss(Money totalIss) {
		this.totalIss = totalIss;
	}
	
	public void setTotalBccsll(Money totalBccsll) {
		this.totalBccsll = totalBccsll;
	}

	public void setTotalCsll(Money totalCsll) {
		this.totalCsll = totalCsll;
	}
	
	public void setTotalBcir(Money totalBcir) {
		this.totalBcir = totalBcir;
	}

	public void setTotalIr(Money totalIr) {
		this.totalIr = totalIr;
	}

	public void setTotalBcinss(Money totalBcinss) {
		this.totalBcinss = totalBcinss;
	}

	public void setTotalInss(Money totalInss) {
		this.totalInss = totalInss;
	}

	public void setTotalPisservicos(Money totalPisservicos) {
		this.totalPisservicos = totalPisservicos;
	}

	public void setTotalCofinsservicos(Money totalCofinsservicos) {
		this.totalCofinsservicos = totalCofinsservicos;
	}

	public void setTotalTributos(Money totalTributos) {
		this.totalTributos = totalTributos;
	}
	
	public void setTotalImpostoFederal(Money totalImpostoFederal) {
		this.totalImpostoFederal = totalImpostoFederal;
	}
	
	public void setTotalImpostoEstadual(Money totalImpostoEstadual) {
		this.totalImpostoEstadual = totalImpostoEstadual;
	}
	
	public void setTotalImpostoMunicipal(Money totalImpostoMunicipal) {
		this.totalImpostoMunicipal = totalImpostoMunicipal;
	}

	public void setPesoliquido(Double pesoliquido) {
		this.pesoliquido = pesoliquido;
	}

	public void setPesobruto(Double pesobruto) {
		this.pesobruto = pesobruto;
	}

	public void setQtdeTotal(Double qtdeTotal) {
		this.qtdeTotal = qtdeTotal;
	}

	public void setHaveServico(boolean haveServico) {
		this.haveServico = haveServico;
	}

	public Money getTotalNotaReceita() {
		return totalNotaReceita;
	}

	public void setTotalNotaReceita(Money totalNotaReceita) {
		this.totalNotaReceita = totalNotaReceita;
	}
}
