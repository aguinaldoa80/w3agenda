package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Pais;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.FormaintermediacaDI;
import br.com.linkcom.sined.geral.bean.enumeration.ViatransporteDI;


public class DeclaracaoImportacaoBeanXml {

	protected String numerodidsida;
	protected Date dtregistro;
	protected String localdesembaraco;
	protected Date data;
	protected Uf uf;
	protected String codigoexportador;
	protected ViatransporteDI viatransporte;
	protected Money valorafrmm;
	protected FormaintermediacaDI formaintermediacao;
	protected Cnpj cnpjadquirente;
	protected Uf ufadquirente;
	
	private Double pisPasepAliquotaAdValorem;
	private Double cofinsAliquotaAdValorem;
	private String dadosMercadoriaCodigoNcm;
	
	private Double condicaoVendaValorReais;
	private Double condicaoVendaValorMoeda;
	private Double valorReais;
	private Double valorReceita;
	
	private String fornecedorNome;
	private String fornecedorComplemento;
	private String fornecedorLogradouro;
	private String fornecedorNumero;
	private Pais fornecedorEstado;
	private String fornecedorCidade;
	
	private Double qtdeTotal;
	
	private List<DeclaracaoImportacaoitemBeanXml> listaItens;

	public String getNumerodidsida() {
		return numerodidsida;
	}

	public Date getDtregistro() {
		return dtregistro;
	}

	public String getLocaldesembaraco() {
		return localdesembaraco;
	}

	public Date getData() {
		return data;
	}

	public Uf getUf() {
		return uf;
	}

	public String getCodigoexportador() {
		return codigoexportador;
	}

	public ViatransporteDI getViatransporte() {
		return viatransporte;
	}

	public Money getValorafrmm() {
		return valorafrmm;
	}

	public FormaintermediacaDI getFormaintermediacao() {
		return formaintermediacao;
	}

	public Cnpj getCnpjadquirente() {
		return cnpjadquirente;
	}

	public Uf getUfadquirente() {
		return ufadquirente;
	}

	public Double getPisPasepAliquotaAdValorem() {
		return pisPasepAliquotaAdValorem;
	}

	public Double getCofinsAliquotaAdValorem() {
		return cofinsAliquotaAdValorem;
	}

	public String getDadosMercadoriaCodigoNcm() {
		return dadosMercadoriaCodigoNcm;
	}

	public Double getCondicaoVendaValorReais() {
		return condicaoVendaValorReais;
	}

	public Double getCondicaoVendaValorMoeda() {
		return condicaoVendaValorMoeda;
	}

	public Double getValorReais() {
		return valorReais;
	}

	public String getFornecedorNome() {
		return fornecedorNome;
	}

	public String getFornecedorComplemento() {
		return fornecedorComplemento;
	}

	public String getFornecedorLogradouro() {
		return fornecedorLogradouro;
	}

	public String getFornecedorNumero() {
		return fornecedorNumero;
	}

	public Pais getFornecedorEstado() {
		return fornecedorEstado;
	}

	public String getFornecedorCidade() {
		return fornecedorCidade;
	}

	public List<DeclaracaoImportacaoitemBeanXml> getListaItens() {
		return listaItens;
	}

	public void setNumerodidsida(String numerodidsida) {
		this.numerodidsida = numerodidsida;
	}

	public void setDtregistro(Date dtregistro) {
		this.dtregistro = dtregistro;
	}

	public void setLocaldesembaraco(String localdesembaraco) {
		this.localdesembaraco = localdesembaraco;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public void setCodigoexportador(String codigoexportador) {
		this.codigoexportador = codigoexportador;
	}

	public void setViatransporte(ViatransporteDI viatransporte) {
		this.viatransporte = viatransporte;
	}

	public void setValorafrmm(Money valorafrmm) {
		this.valorafrmm = valorafrmm;
	}

	public void setFormaintermediacao(FormaintermediacaDI formaintermediacao) {
		this.formaintermediacao = formaintermediacao;
	}

	public void setCnpjadquirente(Cnpj cnpjadquirente) {
		this.cnpjadquirente = cnpjadquirente;
	}

	public void setUfadquirente(Uf ufadquirente) {
		this.ufadquirente = ufadquirente;
	}

	public void setPisPasepAliquotaAdValorem(Double pisPasepAliquotaAdValorem) {
		this.pisPasepAliquotaAdValorem = pisPasepAliquotaAdValorem;
	}

	public void setCofinsAliquotaAdValorem(Double cofinsAliquotaAdValorem) {
		this.cofinsAliquotaAdValorem = cofinsAliquotaAdValorem;
	}

	public void setDadosMercadoriaCodigoNcm(String dadosMercadoriaCodigoNcm) {
		this.dadosMercadoriaCodigoNcm = dadosMercadoriaCodigoNcm;
	}

	public void setCondicaoVendaValorReais(Double condicaoVendaValorReais) {
		this.condicaoVendaValorReais = condicaoVendaValorReais;
	}

	public void setCondicaoVendaValorMoeda(Double condicaoVendaValorMoeda) {
		this.condicaoVendaValorMoeda = condicaoVendaValorMoeda;
	}

	public void setValorReais(Double valorReais) {
		this.valorReais = valorReais;
	}

	public void setFornecedorNome(String fornecedorNome) {
		this.fornecedorNome = fornecedorNome;
	}

	public void setFornecedorComplemento(String fornecedorComplemento) {
		this.fornecedorComplemento = fornecedorComplemento;
	}

	public void setFornecedorLogradouro(String fornecedorLogradouro) {
		this.fornecedorLogradouro = fornecedorLogradouro;
	}

	public void setFornecedorNumero(String fornecedorNumero) {
		this.fornecedorNumero = fornecedorNumero;
	}

	public void setFornecedorEstado(Pais fornecedorEstado) {
		this.fornecedorEstado = fornecedorEstado;
	}

	public void setFornecedorCidade(String fornecedorCidade) {
		this.fornecedorCidade = fornecedorCidade;
	}

	public void setListaItens(List<DeclaracaoImportacaoitemBeanXml> listaItens) {
		this.listaItens = listaItens;
	}

	public Double getQtdeTotal() {
		return qtdeTotal;
	}

	public void setQtdeTotal(Double qtdeTotal) {
		this.qtdeTotal = qtdeTotal;
	}

	public Double getValorReceita() {
		return valorReceita;
	}

	public void setValorReceita(Double valorReceita) {
		this.valorReceita = valorReceita;
	}
	
}
