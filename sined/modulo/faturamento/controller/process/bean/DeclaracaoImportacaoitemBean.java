package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Material;

public class DeclaracaoImportacaoitemBean {

	private DeclaracaoImportacaoitemBeanXml itemBeanXml;
	private Material material;
	private Grupotributacao grupotributacao;
	private String ncmcompleto;
	private Cfop cfop;
	private Double quantidade;
	private Money valorIss;
	private Money valorIpi;
	private Money valorPis;
	private Money valorCofins;
	private Money valorFrete;
	private Money valorDesconto;
	private Money valorSeguro;
	private Double valorUnitario;
	private Money valorBruto;
	private Double pesoLiquido;
	private Double pesoBruto;
	
	public DeclaracaoImportacaoitemBeanXml getItemBeanXml() {
		return itemBeanXml;
	}
	public void setItemBeanXml(DeclaracaoImportacaoitemBeanXml itemBeanXml) {
		this.itemBeanXml = itemBeanXml;
	}
	public Material getMaterial() {
		return material;
	}
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}
	public String getNcmcompleto() {
		return ncmcompleto;
	}
	public Cfop getCfop() {
		return cfop;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Money getValorIss() {
		return valorIss;
	}
	public Money getValorIpi() {
		return valorIpi;
	}
	public Money getValorPis() {
		return valorPis;
	}
	public Money getValorCofins() {
		return valorCofins;
	}
	public Money getValorFrete() {
		return valorFrete;
	}
	public Money getValorDesconto() {
		return valorDesconto;
	}
	public Money getValorSeguro() {
		return valorSeguro;
	}
	public Double getValorUnitario() {
		return valorUnitario;
	}
	public Money getValorBruto() {
		return valorBruto;
	}
	public Double getPesoLiquido() {
		return pesoLiquido;
	}
	public Double getPesoBruto() {
		return pesoBruto;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}
	public void setNcmcompleto(String ncmcompleto) {
		this.ncmcompleto = ncmcompleto;
	}
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setValorIss(Money valorIss) {
		this.valorIss = valorIss;
	}
	public void setValorIpi(Money valorIpi) {
		this.valorIpi = valorIpi;
	}
	public void setValorPis(Money valorPis) {
		this.valorPis = valorPis;
	}
	public void setValorCofins(Money valorCofins) {
		this.valorCofins = valorCofins;
	}
	public void setValorFrete(Money valorFrete) {
		this.valorFrete = valorFrete;
	}
	public void setValorDesconto(Money valorDesconto) {
		this.valorDesconto = valorDesconto;
	}
	public void setValorSeguro(Money valorSeguro) {
		this.valorSeguro = valorSeguro;
	}
	public void setValorUnitario(Double valorUnitario) {
		this.valorUnitario = valorUnitario;
	}
	public void setValorBruto(Money valorBruto) {
		this.valorBruto = valorBruto;
	}
	public void setPesoLiquido(Double pesoLiquido) {
		this.pesoLiquido = pesoLiquido;
	}
	public void setPesoBruto(Double pesoBruto) {
		this.pesoBruto = pesoBruto;
	}
}
