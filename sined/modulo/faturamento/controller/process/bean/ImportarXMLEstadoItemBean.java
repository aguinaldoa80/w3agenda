package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import br.com.linkcom.lknfe.xml.nfe.importacao.estadual.NotaXMLItem;
import br.com.linkcom.sined.geral.bean.Material;

public class ImportarXMLEstadoItemBean {
	
	private String nomeMaterial;
	private Material material;
	private NotaXMLItem item;
	
	public Material getMaterial() {
		return material;
	}
	public NotaXMLItem getItem() {
		return item;
	}
	public String getNomeMaterial() {
		return nomeMaterial;
	}
	public void setNomeMaterial(String nomeMaterial) {
		this.nomeMaterial = nomeMaterial;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setItem(NotaXMLItem item) {
		this.item = item;
	}

}
