package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresaconfiguracaonf;

public class NotafiscalconfiguravelBean {
	
	private String selectedItens;
	private Empresaconfiguracaonf empresaconfiguracaonf;	
	
	public String getSelectedItens() {
		return selectedItens;
	}
	@Required
	@DisplayName("Configuração")
	public Empresaconfiguracaonf getEmpresaconfiguracaonf() {
		return empresaconfiguracaonf;
	}
	public void setSelectedItens(String selectedItens) {
		this.selectedItens = selectedItens;
	}
	public void setEmpresaconfiguracaonf(Empresaconfiguracaonf empresaconfiguracaonf) {
		this.empresaconfiguracaonf = empresaconfiguracaonf;
	}
	
}