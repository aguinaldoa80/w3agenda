package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;


public class AnaliseCreditoBean {

	private String cpf; 
   	private String cnpj;
   	private Integer notaCliente;
   	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public Integer getNotaCliente() {
		return notaCliente;
	}
	public void setNotaCliente(Integer notaCliente) {
		this.notaCliente = notaCliente;
	}
   	
   	
	
}
