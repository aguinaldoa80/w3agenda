package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;


public class EmporiumvendaBean {
	
	private Integer cdvenda;
	private String numero_ticket;
	
	public EmporiumvendaBean(Integer cdvenda, String numero_ticket){
		this.cdvenda = cdvenda;
		this.numero_ticket = numero_ticket;
	}

	public Integer getCdvenda() {
		return cdvenda;
	}
	public String getNumero_ticket() {
		return numero_ticket;
	}
	public void setCdvenda(Integer cdvenda) {
		this.cdvenda = cdvenda;
	}
	public void setNumero_ticket(String numeroTicket) {
		numero_ticket = numeroTicket;
	}
}
