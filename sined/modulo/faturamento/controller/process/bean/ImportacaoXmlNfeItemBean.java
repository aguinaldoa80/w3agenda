package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.enumeration.Motivodesoneracaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Rastro;

public class ImportacaoXmlNfeItemBean {
	
	private Boolean checado;
	private String whereInOrdemcompramaterial;
	private Integer index;
	private Material material;
	private Integer sequencial;
	
	private Double qtde;
	private Double valorunitario;
	
	private String cprod;
	private String xprod;
	private String unidademedidacomercial;
	private String cean;
	private String ncm;
	private String extipi;
	private Cfop cfop;
	private String cListServ;
	
	private Double valorproduto;
	private Money valorfrete;
	private Money valorseguro;
	private Money valoroutrasdespesas;
	private Money valordesconto;
	
	private Tipocobrancaicms csticms;
	private Money valorbcicms;
	private Double icms;
	private Money valoricms;
	private Money valorbcicmsst;
	private Double icmsst;
	private Money valoricmsst;
	private Money valormva;
	private Origemproduto origemProduto;
	
	private Money valorIcmsDeson;
	private Motivodesoneracaoicms motDesICMS;
	
	private Tipocobrancaipi cstipi;
	private Money valorbcipi;
	private Double ipi;
	private Money valoripi;
	
	private Tipocobrancapis cstpis;
	private Money valorbcpis;
	private Double pis;
	private Double qtdebcpis;
	private Double valorunidbcpis;
	private Money valorpis;
	
	private Tipocobrancacofins cstcofins;
	private Money valorbccofins;
	private Double cofins;
	private Double qtdebccofins;
	private Double valorunidbccofins;
	private Money valorcofins;
	
//	CAMPOS DE Imposto de Importação (II)
	private Money valorbcii;
	private Money valorii;
	private Money valorDespesasAduaneiras;
	private Money iof;
	
	private Money valorBcFcp;
	private Double fcp;
	private Money valorFcp;
	
	private Money valorBcFcpSt;
	private Double fcpSt;
	private Money valorFcpSt;
	
	private Money valorBcStRet;
	private Double st;
	private Money valorIcmsStRet;
	
	private Money valorBcFcpStRet;
	private Double fcpStRet;
	private Money valorFcpStRet;
	
	private String nomeMaterial;
	private Pedidovendamaterial pedidovendamaterial;
	private Double qtderestante;
	private Double qtdeassociada;
	private List<ImportacaoXmlNfeItemAssociadoBean> listaItemAssociado;
	private Boolean itemUtilizado;
	private Unidademedida unidademedidaAssociada;
	private String unidademedidaAssociada_simbolo;
	
	private List<Rastro> listaRastro;
	private Boolean obrigarLote = Boolean.FALSE;

	
	public Double getQtde() {
		return qtde;
	}
	public Double getValorunitario() {
		return valorunitario;
	}
	public String getCprod() {
		return cprod;
	}
	public String getXprod() {
		return xprod;
	}
	public Cfop getCfop() {
		return cfop;
	}
	public String getUnidademedidacomercial() {
		return unidademedidacomercial;
	}
	public Double getValorproduto() {
		return valorproduto;
	}
	public Money getValorfrete() {
		return valorfrete;
	}
	public Money getValorseguro() {
		return valorseguro;
	}
	public Money getValoroutrasdespesas() {
		return valoroutrasdespesas;
	}
	public Tipocobrancaicms getCsticms() {
		return csticms;
	}
	public Money getValorbcicms() {
		return valorbcicms;
	}
	public Double getIcms() {
		return icms;
	}
	public Money getValoricms() {
		return valoricms;
	}
	public Money getValorbcicmsst() {
		return valorbcicmsst;
	}
	public Double getIcmsst() {
		return icmsst;
	}
	public Money getValoricmsst() {
		return valoricmsst;
	}
	public Money getValormva() {
		return valormva;
	}
	public Origemproduto getOrigemProduto() {
		return origemProduto;
	}
	public Tipocobrancaipi getCstipi() {
		return cstipi;
	}
	public Money getValorbcipi() {
		return valorbcipi;
	}
	public Double getIpi() {
		return ipi;
	}
	public Money getValoripi() {
		return valoripi;
	}
	public Tipocobrancapis getCstpis() {
		return cstpis;
	}
	public Money getValorbcpis() {
		return valorbcpis;
	}
	public Double getPis() {
		return pis;
	}
	public Double getQtdebcpis() {
		return qtdebcpis;
	}
	public Double getValorunidbcpis() {
		return valorunidbcpis;
	}
	public Money getValorpis() {
		return valorpis;
	}
	public Tipocobrancacofins getCstcofins() {
		return cstcofins;
	}
	public Money getValorbccofins() {
		return valorbccofins;
	}
	public Double getCofins() {
		return cofins;
	}
	public Double getQtdebccofins() {
		return qtdebccofins;
	}
	public Double getValorunidbccofins() {
		return valorunidbccofins;
	}
	public Money getValorcofins() {
		return valorcofins;
	}
	public Money getValordesconto() {
		return valordesconto;
	}
	public String getWhereInOrdemcompramaterial() {
		return whereInOrdemcompramaterial;
	}
	public Integer getIndex() {
		return index;
	}
	public Material getMaterial() {
		return material;
	}
	public String getCean() {
		return cean;
	}
	public String getNcm() {
		return ncm;
	}
	public String getExtipi() {
		return extipi;
	}
	public String getCListServ() {
		return cListServ;
	}
	public Integer getSequencial() {
		return sequencial;
	}
	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}
	public void setCListServ(String listServ) {
		cListServ = listServ;
	}
	public void setCean(String cean) {
		this.cean = cean;
	}
	public void setNcm(String ncm) {
		this.ncm = ncm;
	}
	public void setExtipi(String extipi) {
		this.extipi = extipi;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public void setWhereInOrdemcompramaterial(String whereInOrdemcompramaterial) {
		this.whereInOrdemcompramaterial = whereInOrdemcompramaterial;
	}
	public void setValordesconto(Money valordesconto) {
		this.valordesconto = valordesconto;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
	public void setCprod(String cprod) {
		this.cprod = cprod;
	}
	public void setXprod(String xprod) {
		this.xprod = xprod;
	}
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	public void setUnidademedidacomercial(String unidademedidacomercial) {
		this.unidademedidacomercial = unidademedidacomercial;
	}
	public void setValorproduto(Double valorproduto) {
		this.valorproduto = valorproduto;
	}
	public void setValorfrete(Money valorfrete) {
		this.valorfrete = valorfrete;
	}
	public void setValorseguro(Money valorseguro) {
		this.valorseguro = valorseguro;
	}
	public void setValoroutrasdespesas(Money valoroutrasdespesas) {
		this.valoroutrasdespesas = valoroutrasdespesas;
	}
	public void setCsticms(Tipocobrancaicms csticms) {
		this.csticms = csticms;
	}
	public void setValorbcicms(Money valorbcicms) {
		this.valorbcicms = valorbcicms;
	}
	public void setIcms(Double icms) {
		this.icms = icms;
	}
	public void setValoricms(Money valoricms) {
		this.valoricms = valoricms;
	}
	public void setValorbcicmsst(Money valorbcicmsst) {
		this.valorbcicmsst = valorbcicmsst;
	}
	public void setIcmsst(Double icmsst) {
		this.icmsst = icmsst;
	}
	public void setValoricmsst(Money valoricmsst) {
		this.valoricmsst = valoricmsst;
	}
	public void setValormva(Money valormva) {
		this.valormva = valormva;
	}
	public void setOrigemProduto(Origemproduto origemProduto) {
		this.origemProduto = origemProduto;
	}
	public void setCstipi(Tipocobrancaipi cstipi) {
		this.cstipi = cstipi;
	}
	public void setValorbcipi(Money valorbcipi) {
		this.valorbcipi = valorbcipi;
	}
	public void setIpi(Double ipi) {
		this.ipi = ipi;
	}
	public void setValoripi(Money valoripi) {
		this.valoripi = valoripi;
	}
	public void setCstpis(Tipocobrancapis cstpis) {
		this.cstpis = cstpis;
	}
	public void setValorbcpis(Money valorbcpis) {
		this.valorbcpis = valorbcpis;
	}
	public void setPis(Double pis) {
		this.pis = pis;
	}
	public void setQtdebcpis(Double qtdebcpis) {
		this.qtdebcpis = qtdebcpis;
	}
	public void setValorunidbcpis(Double valorunidbcpis) {
		this.valorunidbcpis = valorunidbcpis;
	}
	public void setValorpis(Money valorpis) {
		this.valorpis = valorpis;
	}
	public void setCstcofins(Tipocobrancacofins cstcofins) {
		this.cstcofins = cstcofins;
	}
	public void setValorbccofins(Money valorbccofins) {
		this.valorbccofins = valorbccofins;
	}
	public void setCofins(Double cofins) {
		this.cofins = cofins;
	}
	public void setQtdebccofins(Double qtdebccofins) {
		this.qtdebccofins = qtdebccofins;
	}
	public void setValorunidbccofins(Double valorunidbccofins) {
		this.valorunidbccofins = valorunidbccofins;
	}
	public void setValorcofins(Money valorcofins) {
		this.valorcofins = valorcofins;
	}
	public String getNomeMaterial() {
		return nomeMaterial;
	}
	public void setNomeMaterial(String nomeMaterial) {
		this.nomeMaterial = nomeMaterial;
	}
	public Money getValorbcii() {
		return valorbcii;
	}
	public void setValorbcii(Money valorbcii) {
		this.valorbcii = valorbcii;
	}
	public Money getValorii() {
		return valorii;
	}
	public void setValorii(Money valorii) {
		this.valorii = valorii;
	}
	public Money getValorDespesasAduaneiras() {
		return valorDespesasAduaneiras;
	}
	public void setValorDespesasAduaneiras(Money valorDespesasAduaneiras) {
		this.valorDespesasAduaneiras = valorDespesasAduaneiras;
	}
	public Money getIof() {
		return iof;
	}
	public void setIof(Money iof) {
		this.iof = iof;
	}

	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}
	public Boolean getChecado() {
		return checado;
	}
	public Double getQtderestante() {
		return qtderestante;
	}
	public Double getQtdeassociada() {
		return qtdeassociada;
	}
	public List<ImportacaoXmlNfeItemAssociadoBean> getListaItemAssociado() {
		return listaItemAssociado;
	}
	public Boolean getItemUtilizado() {
		return itemUtilizado;
	}
	public Unidademedida getUnidademedidaAssociada() {
		return unidademedidaAssociada;
	}
	public String getUnidademedidaAssociada_simbolo() {
		return unidademedidaAssociada_simbolo;
	}
	
	public void setChecado(Boolean checado) {
		this.checado = checado;
	}
	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}
	public void setQtderestante(Double qtderestante) {
		this.qtderestante = qtderestante;
	}
	public void setQtdeassociada(Double qtdeassociada) {
		this.qtdeassociada = qtdeassociada;
	}
	public void setListaItemAssociado(List<ImportacaoXmlNfeItemAssociadoBean> listaItemAssociado) {
		this.listaItemAssociado = listaItemAssociado;
	}
	public void setItemUtilizado(Boolean itemUtilizado) {
		this.itemUtilizado = itemUtilizado;
	}
	public void setUnidademedidaAssociada(Unidademedida unidademedidaAssociada) {
		this.unidademedidaAssociada = unidademedidaAssociada;
	}
	public void setUnidademedidaAssociada_simbolo(
			String unidademedidaAssociadaSimbolo) {
		unidademedidaAssociada_simbolo = unidademedidaAssociadaSimbolo;
	}
	
	public Money getValorBcFcp() {
		return valorBcFcp;
	}
	
	public void setValorBcFcp(Money valorBcFcp) {
		this.valorBcFcp = valorBcFcp;
	}
	
	public Double getFcp() {
		return fcp;
	}
	
	public void setFcp(Double fcp) {
		this.fcp = fcp;
	}
	
	public Money getValorFcp() {
		return valorFcp;
	}
	
	public void setValorFcp(Money valorFcp) {
		this.valorFcp = valorFcp;
	}
	
	public Money getValorBcFcpSt() {
		return valorBcFcpSt;
	}
	
	public void setValorBcFcpSt(Money valorBcFcpSt) {
		this.valorBcFcpSt = valorBcFcpSt;
	}
	
	public Double getFcpSt() {
		return fcpSt;
	}
	
	public void setFcpSt(Double fcpSt) {
		this.fcpSt = fcpSt;
	}
	
	public Money getValorFcpSt() {
		return valorFcpSt;
	}
	
	public void setValorFcpSt(Money valorFcpSt) {
		this.valorFcpSt = valorFcpSt;
	}
	
	public Money getValorBcStRet() {
		return valorBcStRet;
	}
	
	public void setValorBcStRet(Money valorBcStRet) {
		this.valorBcStRet = valorBcStRet;
	}
	
	public Double getSt() {
		return st;
	}
	
	public void setSt(Double st) {
		this.st = st;
	}
	
	public Money getValorIcmsStRet() {
		return valorIcmsStRet;
	}
	
	public void setValorIcmsStRet(Money valorIcmsStRet) {
		this.valorIcmsStRet = valorIcmsStRet;
	}
	
	public Money getValorBcFcpStRet() {
		return valorBcFcpStRet;
	}
	
	public void setValorBcFcpStRet(Money valorBcFcpStRet) {
		this.valorBcFcpStRet = valorBcFcpStRet;
	}
	
	public Double getFcpStRet() {
		return fcpStRet;
	}
	
	public void setFcpStRet(Double fcpStRet) {
		this.fcpStRet = fcpStRet;
	}
	
	public Money getValorFcpStRet() {
		return valorFcpStRet;
	}
	
	public void setValorFcpStRet(Money valorFcpStRet) {
		this.valorFcpStRet = valorFcpStRet;
	}
	
	public Money getValorIcmsDeson() {
		return valorIcmsDeson;
	}
	
	public void setValorIcmsDeson(Money valorIcmsDeson) {
		this.valorIcmsDeson = valorIcmsDeson;
	}
	
	public Motivodesoneracaoicms getMotDesICMS() {
		return motDesICMS;
	}
	
	public void setMotDesICMS(Motivodesoneracaoicms motDesICMS) {
		this.motDesICMS = motDesICMS;
	}
	
	public List<Rastro> getListaRastro() {
		return listaRastro;
	}
	
	public void setListaRastro(List<Rastro> listaRastro) {
		this.listaRastro = listaRastro;
	}

	public Boolean getObrigarLote() {
		return obrigarLote;
	}
	public void setObrigarLote(Boolean obrigarLote) {
		this.obrigarLote = obrigarLote;
	}
}
