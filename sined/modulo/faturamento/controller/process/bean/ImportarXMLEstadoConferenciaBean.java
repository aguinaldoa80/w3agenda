package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import java.util.List;

public class ImportarXMLEstadoConferenciaBean {
	
	private String identificadortela;
	private List<ImportarXMLEstadoBean> listaNotas;

	public String getIdentificadortela() {
		return identificadortela;
	}
	public List<ImportarXMLEstadoBean> getListaNotas() {
		return listaNotas;
	}

	public void setIdentificadortela(String identificadortela) {
		this.identificadortela = identificadortela;
	}
	public void setListaNotas(List<ImportarXMLEstadoBean> listaNotas) {
		this.listaNotas = listaNotas;
	}
}
