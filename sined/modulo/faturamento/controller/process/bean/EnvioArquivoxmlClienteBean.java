package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;

public class EnvioArquivoxmlClienteBean {
	
	private Arquivo arquivoxml;
	private Notafiscalproduto nota;
	private String chaveacesso;
	private String protocolonfe;
	
	public Arquivo getArquivoxml() {
		return arquivoxml;
	}
	public Notafiscalproduto getNota() {
		return nota;
	}
	public String getChaveacesso() {
		return chaveacesso;
	}
	public String getProtocolonfe() {
		return protocolonfe;
	}
	public void setArquivoxml(Arquivo arquivoxml) {
		this.arquivoxml = arquivoxml;
	}
	public void setNota(Notafiscalproduto nota) {
		this.nota = nota;
	}
	public void setChaveacesso(String chaveacesso) {
		this.chaveacesso = chaveacesso;
	}
	public void setProtocolonfe(String protocolonfe) {
		this.protocolonfe = protocolonfe;
	}

}
