package br.com.linkcom.sined.modulo.faturamento.controller.process.bean;

import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;

public class ConferenciaDIFALBean {
	
	private List<Notafiscalproduto> lista = new ListSet<Notafiscalproduto>(Notafiscalproduto.class);
	private Configuracaonfe configuracaonfe;
	private String whereIn;
	
	public List<Notafiscalproduto> getLista() {
		return lista;
	}
	public String getWhereIn() {
		return whereIn;
	}
	public Configuracaonfe getConfiguracaonfe() {
		return configuracaonfe;
	}
	
	public void setConfiguracaonfe(Configuracaonfe configuracaonfe) {
		this.configuracaonfe = configuracaonfe;
	}
	public void setLista(List<Notafiscalproduto> lista) {
		this.lista = lista;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

}
