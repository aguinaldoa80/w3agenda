package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Memorandoexportacao;
import br.com.linkcom.sined.geral.bean.Memorandoexportacaonotafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitementregamaterial;
import br.com.linkcom.sined.geral.service.MemorandoexportacaoService;
import br.com.linkcom.sined.geral.service.MemorandoexportacaonotafiscalprodutoitemService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.util.SinedUtil;
@Controller(path="/faturamento/process/MemorandoExportacao", authorizationModule=ProcessAuthorizationModule.class)
public class MemorandoexportacaoProcess extends MultiActionController{
	private NotafiscalprodutoService notafiscalprodutoService;
	private MemorandoexportacaoService memorandoexportacaoService;
	private MemorandoexportacaonotafiscalprodutoitemService memorandoexportacaonotafiscalprodutoitemService;
	
	public void setNotafiscalprodutoService(
			NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}	
	public void setMemorandoexportacaoService(
			MemorandoexportacaoService memorandoexportacaoService) {
		this.memorandoexportacaoService = memorandoexportacaoService;
	}
	/**
	 * @param request
	 * @return
	 * @since 09/2015
	 * @author Andrey Leonardo
	 */
	public ModelAndView abrirPopupGerarMemorando(WebRequestContext request){
		String itensSelecionados = request.getParameter("itensSelecionados");
		Boolean isNovoMemorando = true;
		List<Memorandoexportacao> listaMemorando = memorandoexportacaoService.loadByNotafiscalproduto(itensSelecionados);
		
		if(listaMemorando.isEmpty()){
			listaMemorando = new ArrayList<Memorandoexportacao>();
		}
			
		List<Notafiscalproduto> listaNotaFiscalProduto = notafiscalprodutoService.loadForMemorandoExportacao(itensSelecionados);
		for(Notafiscalproduto notaproduto : listaNotaFiscalProduto){			
			for(Notafiscalprodutoitem produtoitem : notaproduto.getListaItens()){
				for(Notafiscalprodutoitementregamaterial produtoitementregamaterial : produtoitem.getListaNotaprodutoitementregamaterial()){
					Entregadocumento entregadocumento = produtoitementregamaterial.getEntregamaterial().getEntregadocumento();
					Fornecedor fornecedor = entregadocumento.getFornecedor();
					
					boolean achou = false;
					
					for(Memorandoexportacao memorando2 : listaMemorando){
						if(fornecedor.equals(memorando2.getRemetente()) && notaproduto.equals(memorando2.getNotafiscalproduto())){
							if(memorando2.getListaitens() == null){
								memorando2.setListaitens(new ArrayList<Memorandoexportacaonotafiscalprodutoitem>());
							}else{
								for(Memorandoexportacaonotafiscalprodutoitem memorandonotaitem : memorando2.getListaitens())
								{
									if(memorandonotaitem.getNotafiscalprodutoitem().getCdnotafiscalprodutoitem().equals(produtoitem.getCdnotafiscalprodutoitem())){
										achou=true;
										break;
									}
								}
							}
							if(!achou){
								memorando2.getListaitens().add(new Memorandoexportacaonotafiscalprodutoitem(produtoitem));
								achou = true;
							}
						}
						if(memorando2.getCdmemorandoexportacao() != null){
							isNovoMemorando = false;
						}
					}
						
					if(!achou){
						List<Memorandoexportacaonotafiscalprodutoitem> listaItens = new ArrayList<Memorandoexportacaonotafiscalprodutoitem>();
						listaItens.add(new Memorandoexportacaonotafiscalprodutoitem(produtoitem));
						
						Memorandoexportacao memorando = new Memorandoexportacao();
						memorando.setNotafiscalproduto(notaproduto);
						memorando.setRemetente(fornecedor);
						memorando.setListaitens(listaItens);
						listaMemorando.add(memorando);
					}
				}
			}
		}
		String whereIn = CollectionsUtil.listAndConcatenate(listaMemorando, "cdmemorandoexportacao", ",");
		return new ModelAndView("direct:process/popup/memorandoExportacao", "listaMemorando", listaMemorando).addObject("whereIn", whereIn).addObject("isNovoMemorando", isNovoMemorando);
	}
	
	/**
	 * @param request
	 * @param bean
	 * @since 09/2015
	 * @author Andrey Leonardo
	 */
	public void gerarMemorandoExportacao(WebRequestContext request, Memorandoexportacao bean){		
		for(Memorandoexportacao memorandoExportacao : bean.getListaMemorando()){
			memorandoexportacaoService.saveOrUpdate(memorandoExportacao);
		}
		String whereIn = CollectionsUtil.listAndConcatenate(bean.getListaMemorando(), "cdmemorandoexportacao", ",");
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
				"<script>" +
					"parent.setTimeout(function() {parent.location = '" + request.getServletRequest().getContextPath() + "/faturamento/relatorio/EmitirMemorandoexportacaomatricial?ACAO=gerar&whereIn="+whereIn+"&representante="+bean.getRepresentante().getCdpessoa() + "';}, 500);" +
					"parent.setTimeout(function() {parent.$.akModalRemove(true);}, 550);" +
				"</script>");
	}
	
	/**
	 * @param request
	 * @param bean
	 * @since 09/2015
	 * @author Andrey Leonardo
	 */
	public void excluirMemorandoExportacao(WebRequestContext request, Memorandoexportacao bean){
		List<Memorandoexportacao> listaMemorando = memorandoexportacaoService.loadForDeleteMemorandoexportacao(bean.getWhereIn());
		for(Memorandoexportacao memorando : listaMemorando){
			for(Memorandoexportacaonotafiscalprodutoitem memorandoitem : memorando.getListaitens()){
				memorandoexportacaonotafiscalprodutoitemService.delete(memorandoitem);
			}
			memorandoexportacaoService.delete(memorando);
		}
		request.addMessage("Memorando(s) exclu�do(s) com sucesso!");
		SinedUtil.fechaPopUp(request);
	}
	/**
	 * @param memorandoexportacaonotafiscalprodutoitemService
	 * @since 09/2015
	 * @author Andrey Leonardo
	 */
	public void setMemorandoexportacaonotafiscalprodutoitemService(
			MemorandoexportacaonotafiscalprodutoitemService memorandoexportacaonotafiscalprodutoitemService) {
		this.memorandoexportacaonotafiscalprodutoitemService = memorandoexportacaonotafiscalprodutoitemService;
	}
}
