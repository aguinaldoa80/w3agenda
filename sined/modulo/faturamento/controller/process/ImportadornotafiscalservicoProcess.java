package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportadorBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.filter.ImportadornotafiscalservicoFiltro;

@Controller(path="/faturamento/process/Importadornotafiscalservico", authorizationModule=ProcessAuthorizationModule.class)
public class ImportadornotafiscalservicoProcess extends MultiActionController {
	
	private NotaFiscalServicoService notaFiscalServicoService;
	
	public void setNotaFiscalServicoService(
			NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	
	/**
	 * M�todo de entrada do processo que somente mostra o filtro para preenchimento do usu�rio.
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	@DefaultAction
	public ModelAndView index(WebRequestContext request, ImportadornotafiscalservicoFiltro filtro) throws Exception {
		if (filtro.getEmpresa() == null) filtro.setEmpresa((Empresa)request.getSession().getAttribute("empresaSelecionada"));
		return new ModelAndView("process/importadornotafiscalservico", "filtro", filtro);
	}
	
	/**
	 * Action que faz o processamento do arquivo de importa��o das notas ficais de servi�o.
	 * 
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	public ModelAndView processar(WebRequestContext request, ImportadornotafiscalservicoFiltro filtro) throws Exception {
		if(filtro.getArquivo() == null){
			request.addError("Arquivo � obrigat�rio.");
			return index(request, filtro) ;
		}
		
		List<ImportadorBean> msgs = notaFiscalServicoService.processarArquivo(request, new String(filtro.getArquivo().getContent()), filtro.getEmpresa());
		
		if(msgs != null && msgs.size() > 0){
			for (ImportadorBean bean : msgs) {
				if(bean.getErro()){
					request.addError(bean.getMsgErro());
				} else {
					request.addMessage(bean.getMsgErro());
				}
			}
		} else request.addError("Arquivo n�o importado.");
		
		return index(request, filtro);
	}

}
