
package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoRPS;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoRPSService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotaHistoricoService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.filter.RegistrarCanhotoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;

@Controller(path="/faturamento/process/RegistrarCanhoto", authorizationModule=ProcessAuthorizationModule.class)
public class RegistrarCanhotoProcess extends MultiActionController {
	
	private NotaFiscalServicoService notaFiscalServicoService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private NotaService notaService;
	private NotaHistoricoService notaHistoricoService;
	private ArquivonfnotaService arquivonfnotaService;
	private EmpresaService empresaService;
	private NotaFiscalServicoRPSService notaFiscalServicoRPSService;
	
	public void setNotaFiscalServicoRPSService(
			NotaFiscalServicoRPSService notaFiscalServicoRPSService) {
		this.notaFiscalServicoRPSService = notaFiscalServicoRPSService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setNotaFiscalServicoService(
			NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	public void setArquivonfnotaService(
			ArquivonfnotaService arquivonfnotaService) {
		this.arquivonfnotaService = arquivonfnotaService;
	}
	public void setNotafiscalprodutoService(
			NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	public void setNotaHistoricoService(
			NotaHistoricoService notaHistoricoService) {
		this.notaHistoricoService = notaHistoricoService;
	}
	public void setNotaService(NotaService notaService) {
		this.notaService = notaService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, RegistrarCanhotoFiltro filtro) {
		List<NotaTipo> listaNotaTipo = new ArrayList<NotaTipo>();
		listaNotaTipo.add(NotaTipo.NOTA_FISCAL_PRODUTO);
		listaNotaTipo.add(NotaTipo.NOTA_FISCAL_SERVICO);
		request.setAttribute("listaNotaTipo", listaNotaTipo);
		
		if(filtro.getEmpresa() == null){
			filtro.setEmpresa(empresaService.loadPrincipal());
		}
		
		Integer cdNota = filtro.getCdNota();
		if(cdNota != null){
			
			
			if(notaService.haveNFDiferenteStatus(cdNota + "", NotaStatus.NFE_EMITIDA, NotaStatus.NFE_LIQUIDADA, NotaStatus.NFE_DENEGADA, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA) &&
					!notaFiscalServicoRPSService.haveRPS(new NotaFiscalServico(cdNota))){
				request.addError("N�o � permitido registrar um canhoto de uma nota que ainda n�o foi enviada para a receita ou prefeitura.");
				return this.retornoTela(request, filtro);
			} else {
				Nota nota = notaService.loadWithCliente(new Nota(cdNota));
				
				filtro.setCdNota(cdNota);
				filtro.setNotaTipo(nota.getNotaTipo());
				if(filtro.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_PRODUTO)){
					filtro.setNumeronota(nota.getNumero());
				} else if(filtro.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_SERVICO)){
					List<Arquivonfnota> listaArquivonfnota = arquivonfnotaService.findByNota(cdNota + "");
					if(listaArquivonfnota != null && listaArquivonfnota.size() > 0){
						for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
							if(arquivonfnota.getDtcancelamento() == null &&
									arquivonfnota.getArquivonf() != null && 
									arquivonfnota.getArquivonf().getArquivonfsituacao() != null &&
									arquivonfnota.getArquivonf().getArquivonfsituacao().equals(Arquivonfsituacao.PROCESSADO_COM_SUCESSO) &&
									arquivonfnota.getNumeronfse() != null &&
									arquivonfnota.getCodigoverificacao() != null){
								filtro.setNumeronota(arquivonfnota.getNumero_filtro() + "");
							}
						}
					} else {
						NotaFiscalServicoRPS notaFiscalServicoRPS = notaFiscalServicoRPSService.findByNota(new NotaFiscalServico(cdNota));
						if(notaFiscalServicoRPS != null){
							filtro.setNumeronota(notaFiscalServicoRPS.getNumero() + "");
						}
					}
				}
				
				if(notaService.haveRetornoCanhoto(cdNota)){
					request.addError("Esta nota " + filtro.getNumeronota() + " j� teve canhoto registrado.");
					return this.retornoTela(request, filtro);
				} else {
					filtro.setDtemissao(nota.getDtEmissao());
					filtro.setNomecliente(nota.getCliente().getNome());
					filtro.setEmpresa(nota.getEmpresa());
				}
			}
		}
		
		return new ModelAndView("process/registrarCanhoto", "filtro", filtro);
	}
	
	private ModelAndView retornoTela(WebRequestContext request, RegistrarCanhotoFiltro filtro) {
		Integer cdNota = filtro.getCdNota();
		String origem = request.getParameter("origem");
		String tipo = request.getParameter("tipo");
		
		if(tipo != null && tipo.equalsIgnoreCase("SERVICO")){
			return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico" + (cdNota != null && origem != null && origem.equalsIgnoreCase("ENTRADA") ? "?ACAO=consultar&cdNota=" + cdNota : ""));
		} else if(tipo != null && tipo.equalsIgnoreCase("PRODUTO")){
			return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto" + (cdNota != null && origem != null && origem.equalsIgnoreCase("ENTRADA") ? "?ACAO=consultar&cdNota=" + cdNota : ""));
		} else {
			this.limparFiltro(filtro);
			return new ModelAndView("process/registrarCanhoto", "filtro", filtro);
		}
	}
	
	private void limparFiltro(RegistrarCanhotoFiltro filtro) {
		filtro.setObservacao(null);
		filtro.setNumeronota(null);
		filtro.setNomecliente(null);
		filtro.setDtemissao(null);
		filtro.setCdNota(null);
	}
	
	public ModelAndView salvar(WebRequestContext request, RegistrarCanhotoFiltro filtro) {
		Integer cdNota = filtro.getCdNota();
		if(notaService.haveRetornoCanhoto(cdNota)){
			request.addError("Esta nota j� teve canhoto registrado.");
		} else {
			notaService.updateRetornoCanhoto(cdNota);
			
			NotaHistorico notaHistorico = new NotaHistorico();
			notaHistorico.setNota(new Nota(cdNota));
			notaHistorico.setNotaStatus(NotaStatus.CANHOTO_REGISTRADO);
			notaHistorico.setObservacao(filtro.getObservacao());
			notaHistorico.setDtrecebimento(filtro.getDtrecebimento());
			notaHistoricoService.saveOrUpdate(notaHistorico);
			
			request.addMessage("Registro de canhoto realizado com sucesso.");
		}
		
		this.limparFiltro(filtro);
		return continueOnAction("index", filtro);
	}
	
	public ModelAndView ajaxCarregaDadosValidacao(WebRequestContext request, RegistrarCanhotoFiltro filtro) {
		try{
			String numeronota = filtro.getNumeronota();
			Empresa empresa = filtro.getEmpresa();
			Integer cdNota = null;
			if(filtro.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_PRODUTO)){
				Notafiscalproduto notafiscalproduto = notafiscalprodutoService.findNotaByNumeroEmpresa(empresa, numeronota, false);
				if(notafiscalproduto == null){
					return new JsonModelAndView().addObject("erro", "Nota n�o encontrada na base.");
				}
				cdNota = notafiscalproduto.getCdNota();
			} else if(filtro.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_SERVICO)){
				NotaFiscalServico notaFiscalServico = notaFiscalServicoService.findNotaByNumeroEmpresa(empresa, numeronota);
				if(notaFiscalServico == null){
					try {
						NotaFiscalServicoRPS notaFiscalServicoRPS = notaFiscalServicoRPSService.findByNumeroEmpresa(empresa, Integer.parseInt(numeronota));
						if(notaFiscalServicoRPS != null){
							notaFiscalServico = notaFiscalServicoRPS.getNotaFiscalServico();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(notaFiscalServico == null){
					return new JsonModelAndView().addObject("erro", "Nota n�o encontrada na base.");
				}
				cdNota = notaFiscalServico.getCdNota();
			}
			
			if(notaService.haveRetornoCanhoto(cdNota)){
				return new JsonModelAndView().addObject("erro", "Esta nota " + numeronota + " j� teve canhoto registrado.");
			}
			
			Nota nota = notaService.loadWithCliente(new Nota(cdNota));
			return new JsonModelAndView()
						.addObject("dtemissao", SinedDateUtils.toString(nota.getDtEmissao()))
						.addObject("nomecliente", nota.getCliente().getNome())
						.addObject("cdNota", nota.getCdNota());
		} catch (Exception e) {
			e.printStackTrace();
			return new JsonModelAndView().addObject("erro", e.getMessage());
		}
	}
	
}