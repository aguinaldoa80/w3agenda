package br.com.linkcom.sined.modulo.faturamento.controller.process.filter;

import java.util.List;

import br.com.linkcom.lknfe.xml.nfe.importacao.NotaXML;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;

public class ImportarXMLFiltro {

	private Arquivo arquivo;
	private Configuracaonfe configuracaonfe;
	private Grupotributacao grupotributacao;
	private Tipocobrancacofins tipocobrancacofins;
	private Tipocobrancapis tipocobrancapis;
	private Projeto projeto;
	private Boolean gerarsequencial;
	
	private List<NotaXML> listaEncontrada;
	private List<NotaXML> listaNaoEncontrada;
	
	@DisplayName("XML")
	@Required
	public Arquivo getArquivo() {
		return arquivo;
	}
	@DisplayName("Grupo de tributação")
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}
	@DisplayName("CST COFINS")
	public Tipocobrancacofins getTipocobrancacofins() {
		return tipocobrancacofins;
	}
	@DisplayName("CST PIS")
	public Tipocobrancapis getTipocobrancapis() {
		return tipocobrancapis;
	}
	@Required
	@DisplayName("Configuração de NFS-e")
	public Configuracaonfe getConfiguracaonfe() {
		return configuracaonfe;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Gerar sequencial de nota")
	public Boolean getGerarsequencial() {
		return gerarsequencial;
	}
	public void setGerarsequencial(Boolean gerarsequencial) {
		this.gerarsequencial = gerarsequencial;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public List<NotaXML> getListaEncontrada() {
		return listaEncontrada;
	}
	public List<NotaXML> getListaNaoEncontrada() {
		return listaNaoEncontrada;
	}
	public void setListaEncontrada(List<NotaXML> listaEncontrada) {
		this.listaEncontrada = listaEncontrada;
	}
	public void setListaNaoEncontrada(List<NotaXML> listaNaoEncontrada) {
		this.listaNaoEncontrada = listaNaoEncontrada;
	}
	public void setConfiguracaonfe(Configuracaonfe configuracaonfe) {
		this.configuracaonfe = configuracaonfe;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}
	public void setTipocobrancacofins(Tipocobrancacofins tipocobrancacofins) {
		this.tipocobrancacofins = tipocobrancacofins;
	}
	public void setTipocobrancapis(Tipocobrancapis tipocobrancapis) {
		this.tipocobrancapis = tipocobrancapis;
	}
	
}
