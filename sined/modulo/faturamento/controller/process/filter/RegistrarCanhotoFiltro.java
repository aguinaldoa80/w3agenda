package br.com.linkcom.sined.modulo.faturamento.controller.process.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.util.SinedDateUtils;

public class RegistrarCanhotoFiltro {
	
	private Empresa empresa;
	private Date dtrecebimento = SinedDateUtils.currentDate();
	private NotaTipo notaTipo;
	private String numeronota;
	private Date dtemissao;
	private String nomecliente;
	private String observacao;
	private Integer cdNota;
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Data de recebimento")
	public Date getDtrecebimento() {
		return dtrecebimento;
	}
	@Required
	@DisplayName("Tipo de nota")
	public NotaTipo getNotaTipo() {
		return notaTipo;
	}
	@Required
	@DisplayName("N�mero da Nota")
	public String getNumeronota() {
		return numeronota;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	@DisplayName("Data de emiss�o")
	public Date getDtemissao() {
		return dtemissao;
	}
	@DisplayName("Cliente")
	public String getNomecliente() {
		return nomecliente;
	}
	public Integer getCdNota() {
		return cdNota;
	}
	public void setCdNota(Integer cdNota) {
		this.cdNota = cdNota;
	}
	public void setDtemissao(Date dtemissao) {
		this.dtemissao = dtemissao;
	}
	public void setNumeronota(String numeronota) {
		this.numeronota = numeronota;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtrecebimento(Date dtrecebimento) {
		this.dtrecebimento = dtrecebimento;
	}
	public void setNotaTipo(NotaTipo notaTipo) {
		this.notaTipo = notaTipo;
	}
	public void setNomecliente(String nomecliente) {
		this.nomecliente = nomecliente;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
}