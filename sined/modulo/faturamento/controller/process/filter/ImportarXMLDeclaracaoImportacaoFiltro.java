package br.com.linkcom.sined.modulo.faturamento.controller.process.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.DeclaracaoImportacaoBean;

public class ImportarXMLDeclaracaoImportacaoFiltro {
	
	private Empresa empresa;
	private Arquivo arquivo;
	private DeclaracaoImportacaoBean bean;
	
	@Required
	@DisplayName("Arquivo XML")
	public Arquivo getArquivo() {
		return arquivo;
	}
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	public DeclaracaoImportacaoBean getBean() {
		return bean;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setBean(DeclaracaoImportacaoBean bean) {
		this.bean = bean;
	}
}