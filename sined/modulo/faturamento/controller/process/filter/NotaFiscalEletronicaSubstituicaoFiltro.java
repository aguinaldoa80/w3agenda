package br.com.linkcom.sined.modulo.faturamento.controller.process.filter;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Codigotributacao;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Itemlistaservico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;

public class NotaFiscalEletronicaSubstituicaoFiltro {

	private List<Documento> listaDocumento;
	private Arquivo arquivo;
	private Boolean criarNova;
	
	private NotaFiscalServico notaSubstituidora;
	
	private String numero_substituido_busca;
	private Cnpj cnpj_substituido_busca;
	
	private Cliente cliente_substituido;
	private Money valor_substituido;
	private String numero_substituido;
	private Integer cdNota_substituido;
	private Empresa empresa_substituido;
	
	private Timestamp dtemissao;
	private Money valorservicos;
	private Money valordeducoes;
	private Boolean pisretido;
	private Money valorpis;
	private Boolean cofinsretido;
	private Money valorcofins;
	private Boolean inssretido;
	private Money valorinss;
	private Boolean irretido;
	private Money valorir;
	private Boolean csllretido;
	private Money valorcsll;
	private Boolean issretido;
	private Money valoriss;
	private Money outrasretencoes;
	private Money basecalculo;
	private Money aliquota;
	private Money descontoincondicionado;
	private Money descontocondicionado;
	private Itemlistaservico itemlistaservico;
	private Codigotributacao codigotributacao;
	private String discriminacao;
	private Cnpj cnpj;
	private Cpf cpf;
	
	private Date competencia;
	private String numeronfse;
	private String codigoverificacao;
	
	@Required
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@DisplayName("N�mero")
	public String getNumero_substituido() {
		return numero_substituido;
	}

	@DisplayName("Id")
	public Integer getCdNota_substituido() {
		return cdNota_substituido;
	}
	
	public Cliente getCliente_substituido() {
		return cliente_substituido;
	}

	public Money getValor_substituido() {
		return valor_substituido;
	}
	
	public Boolean getCriarNova() {
		return criarNova;
	}
	
	public NotaFiscalServico getNotaSubstituidora() {
		return notaSubstituidora;
	}
	
	public Empresa getEmpresa_substituido() {
		return empresa_substituido;
	}
	
	public Timestamp getDtemissao() {
		return dtemissao;
	}

	public Money getValorservicos() {
		return valorservicos;
	}

	public Money getValordeducoes() {
		return valordeducoes;
	}

	public Money getValorpis() {
		return valorpis;
	}

	public Money getValorcofins() {
		return valorcofins;
	}

	public Money getValorinss() {
		return valorinss;
	}

	public Money getValorir() {
		return valorir;
	}

	public Money getValorcsll() {
		return valorcsll;
	}

	public Boolean getIssretido() {
		return issretido;
	}

	public Money getValoriss() {
		return valoriss;
	}

	public Money getOutrasretencoes() {
		return outrasretencoes;
	}

	public Money getBasecalculo() {
		return basecalculo;
	}

	public Money getAliquota() {
		return aliquota;
	}

	public Money getDescontoincondicionado() {
		return descontoincondicionado;
	}

	public Money getDescontocondicionado() {
		return descontocondicionado;
	}

	public Itemlistaservico getItemlistaservico() {
		return itemlistaservico;
	}
	
	public Codigotributacao getCodigotributacao() {
		return codigotributacao;
	}

	public String getDiscriminacao() {
		return discriminacao;
	}

	public Cnpj getCnpj() {
		return cnpj;
	}
	
	public Cpf getCpf() {
		return cpf;
	}
	
	public Date getCompetencia() {
		return competencia;
	}

	public String getNumeronfse() {
		return numeronfse;
	}

	public String getCodigoverificacao() {
		return codigoverificacao;
	}
	
	public List<Documento> getListaDocumento() {
		return listaDocumento;
	}
	
	public String getNumero_substituido_busca() {
		return numero_substituido_busca;
	}
	
	public Cnpj getCnpj_substituido_busca() {
		return cnpj_substituido_busca;
	}
	
	public void setCnpj_substituido_busca(Cnpj cnpjSubstituidoBusca) {
		cnpj_substituido_busca = cnpjSubstituidoBusca;
	}
	
	public void setNumero_substituido_busca(String numeroSubstituidoBusca) {
		numero_substituido_busca = numeroSubstituidoBusca;
	}

	public void setListaDocumento(List<Documento> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}

	public void setCompetencia(Date competencia) {
		this.competencia = competencia;
	}

	public void setNumeronfse(String numeronfse) {
		this.numeronfse = numeronfse;
	}

	public void setCodigoverificacao(String codigoverificacao) {
		this.codigoverificacao = codigoverificacao;
	}

	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}

	public void setDtemissao(Timestamp dtemissao) {
		this.dtemissao = dtemissao;
	}

	public void setValorservicos(Money valorservicos) {
		this.valorservicos = valorservicos;
	}

	public void setValordeducoes(Money valordeducoes) {
		this.valordeducoes = valordeducoes;
	}

	public void setValorpis(Money valorpis) {
		this.valorpis = valorpis;
	}

	public void setValorcofins(Money valorcofins) {
		this.valorcofins = valorcofins;
	}

	public void setValorinss(Money valorinss) {
		this.valorinss = valorinss;
	}

	public void setValorir(Money valorir) {
		this.valorir = valorir;
	}

	public void setValorcsll(Money valorcsll) {
		this.valorcsll = valorcsll;
	}

	public void setIssretido(Boolean issretido) {
		this.issretido = issretido;
	}

	public void setValoriss(Money valoriss) {
		this.valoriss = valoriss;
	}

	public void setOutrasretencoes(Money outrasretencoes) {
		this.outrasretencoes = outrasretencoes;
	}

	public void setBasecalculo(Money basecalculo) {
		this.basecalculo = basecalculo;
	}

	public void setAliquota(Money aliquota) {
		this.aliquota = aliquota;
	}

	public void setDescontoincondicionado(Money descontoincondicionado) {
		this.descontoincondicionado = descontoincondicionado;
	}

	public void setDescontocondicionado(Money descontocondicionado) {
		this.descontocondicionado = descontocondicionado;
	}

	public void setItemlistaservico(Itemlistaservico itemlistaservico) {
		this.itemlistaservico = itemlistaservico;
	}
	
	public void setCodigotributacao(Codigotributacao codigotributacao) {
		this.codigotributacao = codigotributacao;
	}

	public void setDiscriminacao(String discriminacao) {
		this.discriminacao = discriminacao;
	}

	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public void setCriarNova(Boolean criarNova) {
		this.criarNova = criarNova;
	}

	public void setNotaSubstituidora(NotaFiscalServico notaSubstituidora) {
		this.notaSubstituidora = notaSubstituidora;
	}

	public void setCliente_substituido(Cliente cliente_substituido) {
		this.cliente_substituido = cliente_substituido;
	}

	public void setValor_substituido(Money valor_substituido) {
		this.valor_substituido = valor_substituido;
	}

	public void setNumero_substituido(String numero_substituido) {
		this.numero_substituido = numero_substituido;
	}

	public void setCdNota_substituido(Integer cdNota_substituido) {
		this.cdNota_substituido = cdNota_substituido;
	}

	public void setEmpresa_substituido(Empresa empresa_substituido) {
		this.empresa_substituido = empresa_substituido;
	}
	
	public Boolean getPisretido() {
		return pisretido;
	}

	public Boolean getCofinsretido() {
		return cofinsretido;
	}

	public Boolean getInssretido() {
		return inssretido;
	}

	public Boolean getIrretido() {
		return irretido;
	}

	public Boolean getCsllretido() {
		return csllretido;
	}

	public void setPisretido(Boolean pisretido) {
		this.pisretido = pisretido;
	}

	public void setCofinsretido(Boolean cofinsretido) {
		this.cofinsretido = cofinsretido;
	}

	public void setInssretido(Boolean inssretido) {
		this.inssretido = inssretido;
	}

	public void setIrretido(Boolean irretido) {
		this.irretido = irretido;
	}

	public void setCsllretido(Boolean csllretido) {
		this.csllretido = csllretido;
	}
}