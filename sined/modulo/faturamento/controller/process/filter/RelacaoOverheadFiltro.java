package br.com.linkcom.sined.modulo.faturamento.controller.process.filter;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;

public class RelacaoOverheadFiltro {
	
	private String mesano1;
	private String mesano2;
	private Empresa empresa;
	
	public RelacaoOverheadFiltro() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		
		int mes = (calendar.get(Calendar.MONTH) + 1);
		
		String mesanoatual = (mes < 10 ? "0" + mes : mes + "") + "/" + calendar.get(Calendar.YEAR);
		mesano1 = mesanoatual;
		mesano2 = mesanoatual;
	}
	
	@Required
	@DisplayName("M�s/Ano In�cio")
	public String getMesano1() {
		return mesano1;
	}
	@Required
	@DisplayName("M�s/Ano Fim")
	public String getMesano2() {
		return mesano2;
	}
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public Date getDtref1(){
		if(mesano1 != null){
			try {
				return new Date(new SimpleDateFormat("MM/yyyy").parse(mesano1).getTime());
			} catch (ParseException e) {
				return null;
			}
		} else return null;
	}
	
	public Date getDtref2(){
		if(mesano2 != null){
			try {
				return new Date(new SimpleDateFormat("MM/yyyy").parse(mesano2).getTime());
			} catch (ParseException e) {
				return null;
			}
		} else return null;
	}
	
	public void setMesano1(String mesano1) {
		this.mesano1 = mesano1;
	}
	public void setMesano2(String mesano2) {
		this.mesano2 = mesano2;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}