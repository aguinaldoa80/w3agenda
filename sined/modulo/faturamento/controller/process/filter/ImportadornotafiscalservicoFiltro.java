package br.com.linkcom.sined.modulo.faturamento.controller.process.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Empresa;


public class ImportadornotafiscalservicoFiltro  {
	
	private Empresa empresa;
	private Arquivo arquivo;

	@Required
	@DisplayName("Arquivo de importação")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
}
