package br.com.linkcom.sined.modulo.faturamento.controller.process.filter;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Contratotipo;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ArquivoRetornoContratoBean;

public class ArquivoRetornoContratoFiltro {
	
	public static final String TIPO_RMPC = "RMPC";   
	public static final String TIPO_SUA_CONTA = "Sua Conta";
	public static final String TIPO_SCPC = "SCPC";
	
	private String tipo;
	private Contratotipo contratotipo;
	private Arquivo arquivo;
	private List<ArquivoRetornoContratoBean> listaBean;
	
	private String numeroremessa;
	private List<String> listaClienteContratoNaoEncontrado;
	
	@Required
	@DisplayName("Tipo de Retorno")
	public String getTipo() {
		return tipo;
	}
	@Required
	@DisplayName("Tipo de Contrato")
	public Contratotipo getContratotipo() {
		return contratotipo;
	}
	@Required
	@DisplayName("Arquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	public List<ArquivoRetornoContratoBean> getListaBean() {
		return listaBean;
	}
	public String getNumeroremessa() {
		return numeroremessa;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setContratotipo(Contratotipo contratotipo) {
		this.contratotipo = contratotipo;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setListaBean(List<ArquivoRetornoContratoBean> listaBean) {
		this.listaBean = listaBean;
	}
	public void setNumeroremessa(String numeroremessa) {
		this.numeroremessa = numeroremessa;
	}
	public List<String> getListaClienteContratoNaoEncontrado() {
		return listaClienteContratoNaoEncontrado;
	}
	public void setListaClienteContratoNaoEncontrado(List<String> listaClienteContratoNaoEncontrado) {
		this.listaClienteContratoNaoEncontrado = listaClienteContratoNaoEncontrado;
	}
}