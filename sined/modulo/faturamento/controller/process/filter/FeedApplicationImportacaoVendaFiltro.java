package br.com.linkcom.sined.modulo.faturamento.controller.process.filter;

import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Empresa;


public class FeedApplicationImportacaoVendaFiltro {
	
	protected Empresa empresa;
	protected Arquivo arquivo;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public Arquivo getArquivo() {
		return arquivo;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
}
