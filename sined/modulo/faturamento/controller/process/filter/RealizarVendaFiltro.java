package br.com.linkcom.sined.modulo.faturamento.controller.process.filter;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.RealizarVendaProdutoBean;

public class RealizarVendaFiltro {
	
	private Empresa empresa;
	private Colaborador colaborador;
	private Cliente cliente;
	private Endereco endereco;
	private String codigo;
	private Material material;
	private Money valor;
	private Double quantidade = new Double(1);
	private List<RealizarVendaProdutoBean> listaRealizarVendaProdutoBean;
	
	@Required
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Vendedor")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@Required
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("Local de entrega")
	public Endereco getEndereco() {
		return endereco;
	}
	
	@DisplayName("C�digo")
	@MaxLength(10)
	public String getCodigo() {
		return codigo;
	}
	@Required
	@DisplayName("Descri��o")
	public Material getMaterial() {
		return material;
	}
	@Required
	@DisplayName("Valor")
	public Money getValor() {
		return valor;
	}
	@Required
	@DisplayName("Qtde")
	@MaxLength(5)
	public Double getQuantidade() {
		return quantidade;
	}
	public List<RealizarVendaProdutoBean> getListaRealizarVendaProdutoBean() {
		return listaRealizarVendaProdutoBean;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setListaRealizarVendaProdutoBean(List<RealizarVendaProdutoBean> listaRealizarVendaProdutoBean) {
		this.listaRealizarVendaProdutoBean = listaRealizarVendaProdutoBean;
	}
}