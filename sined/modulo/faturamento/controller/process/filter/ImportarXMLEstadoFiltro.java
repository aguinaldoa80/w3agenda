package br.com.linkcom.sined.modulo.faturamento.controller.process.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Formapagamentonfe;

public class ImportarXMLEstadoFiltro {
	
	private Arquivo arquivo;
	private Configuracaonfe configuracaonfe;
	private Formapagamentonfe formapagamentonfe; 
	
	@Required
	@DisplayName("XML")
	public Arquivo getArquivo() {
		return arquivo;
	}
	@Required
	@DisplayName("Configuração de NF-e")
	public Configuracaonfe getConfiguracaonfe() {
		return configuracaonfe;
	}
	@Required
	@DisplayName("Forma de pagamento")
	public Formapagamentonfe getFormapagamentonfe() {
		return formapagamentonfe;
	}
	public void setConfiguracaonfe(Configuracaonfe configuracaonfe) {
		this.configuracaonfe = configuracaonfe;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setFormapagamentonfe(Formapagamentonfe formapagamentonfe) {
		this.formapagamentonfe = formapagamentonfe;
	}
}