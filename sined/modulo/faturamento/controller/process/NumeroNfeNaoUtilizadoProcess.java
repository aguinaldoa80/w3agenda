package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Inutilizacaonfe;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.enumeration.Inutilizacaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.ConfiguracaonfeService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.InutilizacaonfeService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.NumeroNfeNaoUtilizadoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.enumeration.Avisonumeronfenaoutilizado;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NumeroNfeNaoUtilizadoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/faturamento/process/NumeroNfeNaoUtilizado", authorizationModule=ProcessAuthorizationModule.class)
public class NumeroNfeNaoUtilizadoProcess extends MultiActionController {
	
	private EmpresaService empresaService;
	private NotaService notaService;
	private ArquivonfnotaService arquivonfnotaService;
	private ConfiguracaonfeService configuracaonfeService;
	private InutilizacaonfeService inutilizacaonfeService; 
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setNotaService(NotaService notaService) {
		this.notaService = notaService;
	}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {
		this.arquivonfnotaService = arquivonfnotaService;
	}
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {
		this.configuracaonfeService = configuracaonfeService;
	}
	public void setInutilizacaonfeService(InutilizacaonfeService inutilizacaonfeService) {
		this.inutilizacaonfeService = inutilizacaonfeService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, NumeroNfeNaoUtilizadoFiltro filtro) throws Exception{
		filtro.setDtinicioemissao(SinedDateUtils.firstDateOfMonth(new Date(System.currentTimeMillis())));

		Empresa empresa = filtro.getEmpresa();
		if(filtro.getEmpresa() == null){
			filtro.setEmpresa(empresaService.loadPrincipal());
		}
		
		List<Empresa> listaEmpresa = empresaService.findAtivos(empresa);
		request.setAttribute("listaEmpresa", listaEmpresa);
		request.setAttribute("listaConfiguracaoNfe", configuracaonfeService.findAtivosByTipoEmpresa(Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO, filtro.getEmpresa()));
		return new ModelAndView("/process/numeronfenaoutilizado").addObject("filtro", filtro);
	}

	/**
	 * M�todo para carregar listagem do filtro no jsp via ajax.
	 * @see br.com.linkcom.sined.geral.service.ColaboradorcargoService#findColaboradorAltera
	 * @param request
	 * @param filtro
	 * @author Jo�o Vitor
	 */
	@Action("listagem")
	public ModelAndView listagem(WebRequestContext request, NumeroNfeNaoUtilizadoFiltro filtro){
		
		//setando um dia antes e um depois utilizado no filtro para busca
		filtro.setDtinicioemissao(new Date(filtro.getDtinicioemissao().getTime() - 86400000));
		filtro.setDtfimemissao(new Date (filtro.getDtfimemissao().getTime() + 86400000));
				
		List<Nota> listaNotas = notaService.findForNotaNfeNaoUtilizado(filtro);
		
		//Nota processada - todo o seu processo foi conclu�do
		List<Nota> listaNotasProcessadas = new ArrayList<Nota>();
		
		List<Nota> listaNotasCanceladas = null;
		Map<Double, Nota> mapNotasCanceladasNaoProcessadas = null;
		List<NumeroNfeNaoUtilizadoBean> listaNfeNaoUtilizado = new ArrayList<NumeroNfeNaoUtilizadoBean>();

		if (SinedUtil.isListNotEmpty(listaNotas)) {
			listaNotasCanceladas = new ArrayList<Nota>();
			//Separa��o das notas processadas e canceladas
			this.filtraNotasCanceladasProcessadas(listaNotas, listaNotasCanceladas,listaNotasProcessadas);
			//Preenchimento do map com as notas canceladas que n�o foram processadas
			mapNotasCanceladasNaoProcessadas = this.filtraNotasCanceladasNaoProcessadas(filtro, listaNotasCanceladas,mapNotasCanceladasNaoProcessadas, listaNotasProcessadas);

			if (SinedUtil.isListNotEmpty(listaNotasProcessadas)) {
				//Organizando lista de notas processadas para buscar a primeira nota do periodo
				listaNotasProcessadas = this.organizaListasNotasProcessadas(listaNotasProcessadas);
				
				if (listaNotasProcessadas.get(0) != null && listaNotasProcessadas.get(0).getCdNota() != null && listaNotasProcessadas.get(0).getNumeronota() != null) {
					listaNfeNaoUtilizado = this.findForNfeNaoUtilizado(listaNotasProcessadas, listaNfeNaoUtilizado);
					
					if(SinedUtil.isListNotEmpty(listaNfeNaoUtilizado))
						listaNfeNaoUtilizado = this.findNotasNaoUtilizadasAndProcessadas(listaNfeNaoUtilizado, filtro.getInutilizacaoProcessadas());
					
					//Preenchendo List de NF-e n�o utilizadas
					if (SinedUtil.isListNotEmpty(listaNfeNaoUtilizado) 
							&& mapNotasCanceladasNaoProcessadas != null 
							&& mapNotasCanceladasNaoProcessadas.size() > 0) {						
						listaNfeNaoUtilizado = this.verificaNotaCanceladaNaoProcessadaForListaNfeNaoUtilizado(mapNotasCanceladasNaoProcessadas,listaNfeNaoUtilizado);
						listaNfeNaoUtilizado = this.verificaNotaCanceladaNaoProcessadaForMap(mapNotasCanceladasNaoProcessadas,listaNfeNaoUtilizado);
						
					} else if (mapNotasCanceladasNaoProcessadas != null && mapNotasCanceladasNaoProcessadas.size() > 0){
						listaNfeNaoUtilizado = this.verificaNotaCanceladaNaoProcessadaForMap(mapNotasCanceladasNaoProcessadas,listaNfeNaoUtilizado);
					}
					
					if(SinedUtil.isListNotEmpty(listaNfeNaoUtilizado)){
						String whereIn = SinedUtil.listAndConcatenate(listaNfeNaoUtilizado, "numeroNFE", ",").replace(".0", "");
						verificaNotaOutrasSituacoes(filtro,listaNfeNaoUtilizado, whereIn);
						listaNfeNaoUtilizado = this.verificaNotaListaInutilizacao(filtro,listaNfeNaoUtilizado);
					}
				}
			}
			//Organizando notas e add no filtro
			listaNfeNaoUtilizado = this.organizaListaNfeNaoUtilizado(listaNfeNaoUtilizado);
			filtro.setListaNfeNaoUtilizado(listaNfeNaoUtilizado);
			
			if (filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null) {
				request.setAttribute("listaConfiguracaoNfe", configuracaonfeService.findAtivosByTipoEmpresa(Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO, filtro.getEmpresa()));
				request.setAttribute("INUTILIZACAO_NAO_PROCESSADA", Avisonumeronfenaoutilizado.INUTILIZACAO_NAO_PROCESSADA);
				request.setAttribute("FORA_DAS_SITUACOES_INDICADAS", Avisonumeronfenaoutilizado.FORA_DAS_SITUACOES_INDICADAS);
				request.setAttribute("INUTILIZACAONFE_CRIADA_NAO_PROCESSADA", Avisonumeronfenaoutilizado.INUTILIZACAONFE_CRIADA_NAO_PROCESSADA);
				request.setAttribute("NOTA_PROCESSADA_SUCESSO", Avisonumeronfenaoutilizado.NOTA_PROCESSADA_SUCESSO);
				request.setAttribute("NOTA_PROCESSADA_ERRO", Avisonumeronfenaoutilizado.NOTA_PROCESSADA_ERRO);
				request.setAttribute("NOTA_GERADA_ENVIANDO", Avisonumeronfenaoutilizado.NOTA_GERADA_ENVIANDO);
				request.setAttribute("NOTA_NAO_INUTILIZADA", Avisonumeronfenaoutilizado.NOTA_NAO_INUTILIZADA);
				request.getSession().setAttribute("empresa", filtro.getEmpresa());
				request.getSession().setAttribute("configuracaonfe", filtro.getConfiguracaonfe());
			}
		}
		return new ModelAndView("direct:ajax/numeronfenaoutilizadolistagem", "filtro", filtro);
	}
	
	/**
	 * M�todo respons�vel por organizar as notas
	 * @param listaNotasProcessadas
	 * @return 
	 */
	public List<Nota> organizaListasNotasProcessadas(List<Nota> listaNotasProcessadas) {
		Collections.sort(listaNotasProcessadas, new Comparator<Nota>(){
			public int compare(Nota nota1, Nota nota2) {
				return nota1.getNumeronota().compareTo(nota2.getNumeronota());
			}
		});		
		return listaNotasProcessadas;
	}
	
	/**
	 * M�todo respons�vel por organizar as notas n�o utilizadas
	 * @param listaNotasProcessadas
	 * @return 
	 */
	public List<NumeroNfeNaoUtilizadoBean> organizaListaNfeNaoUtilizado(List<NumeroNfeNaoUtilizadoBean> listaNfeNaoUtilizado) {
		Collections.sort(listaNfeNaoUtilizado, new Comparator<NumeroNfeNaoUtilizadoBean>(){
			public int compare(NumeroNfeNaoUtilizadoBean nota1, NumeroNfeNaoUtilizadoBean nota2) {
				return nota1.getNumeroNFE().compareTo(nota2.getNumeroNFE());
			}
		});		
		
		List<NumeroNfeNaoUtilizadoBean> listaNfeNaoUtilizadoFinal = new ArrayList<NumeroNfeNaoUtilizadoBean>();
		for (NumeroNfeNaoUtilizadoBean numeroNfeNaoUtilizadoBean : listaNfeNaoUtilizado) {
			boolean achou = false;
			for (NumeroNfeNaoUtilizadoBean numeroNfeNaoUtilizadoBean2 : listaNfeNaoUtilizadoFinal) {
				if(numeroNfeNaoUtilizadoBean.getNumeroNFE().equals(numeroNfeNaoUtilizadoBean2.getNumeroNFE())){
					achou = true;
					break;
				}
			}
			
			if(!achou){
				listaNfeNaoUtilizadoFinal.add(numeroNfeNaoUtilizadoBean);
			}
		}
		
		return listaNfeNaoUtilizadoFinal;
	}
	
	/**
	 * M�todo respons�vel por localizar as notas que j� foram processadas e set sua situa��o
	 * @param listaNfeNaoUtilizado
	 * @param inutilizacaoProcessadas
	 * @return listaNfeNaoUtilizado
	 * @since 01/03/2016
	 * @author C�sar
	 */
	public List<NumeroNfeNaoUtilizadoBean> findNotasNaoUtilizadasAndProcessadas(List<NumeroNfeNaoUtilizadoBean> listaNfeNaoUtilizado, Boolean inutilizacaoProcessadas){
		
		String whereIn = SinedUtil.listAndConcatenate(listaNfeNaoUtilizado, "numeroNFE", ",");
		List<Inutilizacaonfe> listaInutilizacaonfe = inutilizacaonfeService.findForInutilizacao(whereIn);
		List<NumeroNfeNaoUtilizadoBean> listanotasRemover = new ArrayList<NumeroNfeNaoUtilizadoBean>();
		
		for (Inutilizacaonfe inutilizacaonfe : listaInutilizacaonfe) {
			for (NumeroNfeNaoUtilizadoBean numeronaoutilizado : listaNfeNaoUtilizado) {
				if(inutilizacaonfe.getNumeronfe().equals(numeronaoutilizado.getNumeroNFE()) && inutilizacaoProcessadas){
					
					numeronaoutilizado.setCdinutilizacaonfe(inutilizacaonfe.getCdinutilizacaonfe());
					if(inutilizacaonfe.getSituacao() != null){
						if(inutilizacaonfe.getSituacao().equals(Inutilizacaosituacao.PROCESSADO_COM_SUCESSO))					
							numeronaoutilizado.setAvisonfenaoutilizado(Avisonumeronfenaoutilizado.NOTA_PROCESSADA_SUCESSO);
						else if(inutilizacaonfe.getSituacao().equals(Inutilizacaosituacao.PROCESSADO_COM_ERRO))
							numeronaoutilizado.setAvisonfenaoutilizado(Avisonumeronfenaoutilizado.NOTA_PROCESSADA_ERRO);
						else if(inutilizacaonfe.getSituacao().equals(Inutilizacaosituacao.ENVIANDO))
							numeronaoutilizado.setAvisonfenaoutilizado(Avisonumeronfenaoutilizado.NOTA_GERADA_ENVIANDO);
					}
				}else if (inutilizacaonfe.getNumeronfe().equals(numeronaoutilizado.getNumeroNFE()) && inutilizacaoProcessadas.equals(Boolean.FALSE)){ 
					listanotasRemover.add(numeronaoutilizado);
				}
			}
		}
		if(listanotasRemover != null && !SinedUtil.isListEmpty(listanotasRemover)){
			for (NumeroNfeNaoUtilizadoBean numeroNfeNaoUtilizadoBean : listanotasRemover) {
				listaNfeNaoUtilizado.remove(numeroNfeNaoUtilizadoBean);
			}
		}
		return listaNfeNaoUtilizado;
	}
	
	/**
	 * M�todo respons�vel por processar as notas n�o utilizadas
	 * @param listaNotasProcessadas
	 * @param listaNfeNaoUtilizado
	 * @return 
	 */
	public List<NumeroNfeNaoUtilizadoBean> findForNfeNaoUtilizado(List<Nota> listaNotasProcessadas, List<NumeroNfeNaoUtilizadoBean> listaNfeNaoUtilizado) {
		
		Double numeroNfePrimeiraNota = listaNotasProcessadas.get(0).getNumeronota();

		int indexLista = 0;
		for(int indexNumero = 0; indexLista < listaNotasProcessadas.size(); indexNumero++) {
			if (listaNotasProcessadas.get(indexLista) != null && listaNotasProcessadas.get(indexLista).getCdNota() != null  && listaNotasProcessadas.get(indexLista).getNumeronota() != null) {
				if (!listaNotasProcessadas.get(indexLista).getNumeronota().equals(indexNumero + numeroNfePrimeiraNota)) {				
					NumeroNfeNaoUtilizadoBean bean = new NumeroNfeNaoUtilizadoBean();
					bean.setNumeroNFE(indexNumero + numeroNfePrimeiraNota);		
					bean.setAvisonfenaoutilizado(Avisonumeronfenaoutilizado.NOTA_NAO_INUTILIZADA);
					listaNfeNaoUtilizado.add(bean);
				} else {
					indexLista++;
				}
			}
		}
		return listaNfeNaoUtilizado;
	}
	
	/**
	 * M�todo respons�vel por receber as notas fiscais canceladas e filtrar as notas n�o que foram processadas
	 * @param filtro
	 * @param listaNotasCanceladas
	 * @param mapNotasCanceladasNaoProcessadas
	 * @param listaNotasProcessadas
	 * @return
	 */
	private Map<Double, Nota> filtraNotasCanceladasNaoProcessadas(NumeroNfeNaoUtilizadoFiltro filtro,List<Nota> listaNotasCanceladas,Map<Double, Nota> mapNotasCanceladasNaoProcessadas,List<Nota> listaNotasProcessadas) {
		List<Arquivonfnota> listaNotasCanceladasProcessadas;
		if (SinedUtil.isListNotEmpty(listaNotasCanceladas)) {
			
			//Busca uma lista de Arquivonfnota processado partir do whereIn de notas com status de cancelada
			listaNotasCanceladasProcessadas = arquivonfnotaService.findArquivoNotaProcessadoByNota(SinedUtil.listAndConcatenate(listaNotasCanceladas, "cdNota", ","), filtro);
			if (SinedUtil.isListNotEmpty(listaNotasCanceladasProcessadas)) {
				mapNotasCanceladasNaoProcessadas = new HashMap<Double, Nota>();

				for (Arquivonfnota arquivonfnota : listaNotasCanceladasProcessadas) {
					if (arquivonfnota != null && arquivonfnota.getNota() != null && arquivonfnota.getNota().getCdNota() != null) {
						if (arquivonfnota.getDtprocessamento() != null) {
							listaNotasProcessadas.add(arquivonfnota.getNota());
						} else {
							mapNotasCanceladasNaoProcessadas.put(arquivonfnota.getNota().getNumeronota(), arquivonfnota.getNota());
						}
					}
				}
			}
		}
		
		//confer�ncia para que map n�o contenha nota processada
		if (mapNotasCanceladasNaoProcessadas != null && mapNotasCanceladasNaoProcessadas.size() > 0) {
			for (Nota notaProcessada : listaNotasProcessadas) {
				if (mapNotasCanceladasNaoProcessadas.containsKey(notaProcessada.getNumeronota())) {
					mapNotasCanceladasNaoProcessadas.remove(notaProcessada.getNumeronota());
				}
			}
		}
		return mapNotasCanceladasNaoProcessadas;
	}
	
	/**
	 * M�todo respons�vel por separar as notas canceladas das notas processadas
	 * @param listaNotas
	 * @param listaNotasCanceladas
	 * @param listaNotasProcessadas
	 */
	private void filtraNotasCanceladasProcessadas(List<Nota> listaNotas,List<Nota> listaNotasCanceladas, List<Nota> listaNotasProcessadas) {
		for (Nota nota : listaNotas) {
			if (nota != null && nota.getNotaStatus() != null && nota.getNotaStatus().equals(NotaStatus.CANCELADA)) {
				listaNotasCanceladas.add(nota);
			} else {
				listaNotasProcessadas.add(nota);
			}
		}
	}
	
	/**
	 * M�todo respons�vel por preencher a listaNfeNaoUtilizado atrav�s do mapNotasCanceladasNaoProcessadas
	 * @param mapNotasCanceladasNaoProcessadas
	 * @param listaNfeNaoUtilizado
	 * @return listaNfeNaoUtilizado
	 */
	private List<NumeroNfeNaoUtilizadoBean> verificaNotaCanceladaNaoProcessadaForMap(Map<Double, Nota> mapNotasCanceladasNaoProcessadas,List<NumeroNfeNaoUtilizadoBean> listaNfeNaoUtilizado) {
		for (Entry<Double, Nota> notaCanceladaNaoProcessada: mapNotasCanceladasNaoProcessadas.entrySet()) {
			 NumeroNfeNaoUtilizadoBean bean = new NumeroNfeNaoUtilizadoBean();
			 bean.setCdNota(notaCanceladaNaoProcessada.getValue().getCdNota());
			 bean.setAvisonfenaoutilizado(Avisonumeronfenaoutilizado.INUTILIZACAO_NAO_PROCESSADA);
			 bean.setNotaStatus(notaCanceladaNaoProcessada.getValue().getNotaStatus());
			 bean.setNumeroNFE(notaCanceladaNaoProcessada.getValue().getNumeronota());
			 listaNfeNaoUtilizado.add(bean);
		}
		return listaNfeNaoUtilizado;
	}
	
	/**
	 * Verificando notas do mapNotasCanceladasNaoProcessadas est�o contida na listaNfeNaoUtilizado e atualizando seu status
	 * @param mapNotasCanceladasNaoProcessadas
	 * @param listaNfeNaoUtilizado
	 * @return listaNfeNaoUtilizado
	 */
	private List<NumeroNfeNaoUtilizadoBean> verificaNotaCanceladaNaoProcessadaForListaNfeNaoUtilizado(Map<Double, Nota> mapNotasCanceladasNaoProcessadas,List<NumeroNfeNaoUtilizadoBean> listaNfeNaoUtilizado) {
		if (mapNotasCanceladasNaoProcessadas != null && mapNotasCanceladasNaoProcessadas.size() > 0) {
			for (NumeroNfeNaoUtilizadoBean bean : listaNfeNaoUtilizado) {
				if (mapNotasCanceladasNaoProcessadas.containsKey(bean.getNumeroNFE())) {
					bean.setCdNota(mapNotasCanceladasNaoProcessadas.get(bean.getNumeroNFE()).getCdNota());
					bean.setAvisonfenaoutilizado(Avisonumeronfenaoutilizado.INUTILIZACAO_NAO_PROCESSADA);
					bean.setNotaStatus(mapNotasCanceladasNaoProcessadas.get(bean.getNumeroNFE()).getNotaStatus());
				}
			}
		}
		return listaNfeNaoUtilizado;
	}
	
	/**
	 * M�todo respons�vel por verificar notas inutilizadas e n�o processadas
	 * @param filtro
	 * @param listaNfeNaoUtilizado
	 * @return listaNfeNaoUtilizado
	 */
	private List<NumeroNfeNaoUtilizadoBean> verificaNotaListaInutilizacao(NumeroNfeNaoUtilizadoFiltro filtro,List<NumeroNfeNaoUtilizadoBean> listaNfeNaoUtilizado) {
		List<Inutilizacaonfe> listaInutilizacao = inutilizacaonfeService.findForNumeroNfeNaoUtilizado(filtro.getEmpresa());

		if (SinedUtil.isListNotEmpty(listaInutilizacao)) {
			for (NumeroNfeNaoUtilizadoBean bean : listaNfeNaoUtilizado) {
				for (Inutilizacaonfe inutilizacaonfe : listaInutilizacao) {
					if (bean.getNumeroNFE().equals(inutilizacaonfe.getNuminicio().doubleValue()) && bean.getNumeroNFE().equals(inutilizacaonfe.getNumfim().doubleValue())) {
						bean.setCdinutilizacaonfe(inutilizacaonfe.getCdinutilizacaonfe());
						bean.setAvisonfenaoutilizado(Avisonumeronfenaoutilizado.INUTILIZACAONFE_CRIADA_NAO_PROCESSADA);
						break;
					}
				}
			}
		}
		return listaNfeNaoUtilizado;
	}
	
	/**
	 * M�todo respons�vel por verificar se existe notas que fora as seguintes situa��es :
	 * - INUTILIZACAO_NAO_PROCESSADA
	 * - INUTILIZACAONFE_CRIADA_NAO_PROCESSADA
	 * - NOTA_NAO_INUTILIZADA
	 * @param filtro
	 * @param listaNfeNaoUtilizado
	 * @param whereIn
	 */
	private void verificaNotaOutrasSituacoes(NumeroNfeNaoUtilizadoFiltro filtro, List<NumeroNfeNaoUtilizadoBean> listaNfeNaoUtilizado, String whereIn) {
		Map<Double, Nota> mapaNotaOutrasSituacoes;
		List<Nota> listaNotaOutrasSituacoes = notaService.findForNotaNfeNaoUtilizadoByNumero(whereIn, filtro);
		if (SinedUtil.isListNotEmpty(listaNotaOutrasSituacoes)) {
			mapaNotaOutrasSituacoes = new HashMap<Double, Nota>();
			for (Nota nota : listaNotaOutrasSituacoes) {
				if (nota != null && nota.getCdNota() != null) {
					mapaNotaOutrasSituacoes.put(nota.getNumeronota(), nota);
				}
			}
			if (mapaNotaOutrasSituacoes != null && mapaNotaOutrasSituacoes.size() > 0) {
				for (NumeroNfeNaoUtilizadoBean bean : listaNfeNaoUtilizado) {
					if (mapaNotaOutrasSituacoes.containsKey(bean.getNumeroNFE())) {
						bean.setCdNota(mapaNotaOutrasSituacoes.get(bean.getNumeroNFE()).getCdNota());
						bean.setNotaStatus(mapaNotaOutrasSituacoes.get(bean.getNumeroNFE()).getNotaStatus());
						bean.setAvisonfenaoutilizado(Avisonumeronfenaoutilizado.FORA_DAS_SITUACOES_INDICADAS);
					}
				}
			}
		}
	}
	
	@Action("criarRegistroInutilizacao")
	public ModelAndView criarRegistroInutilizacao(WebRequestContext request, NumeroNfeNaoUtilizadoFiltro filtro) throws Exception {
		if (request.getSession().getAttribute("empresa") != null) {
			filtro.setEmpresa((Empresa)request.getSession().getAttribute("empresa"));
		}
		if ((Configuracaonfe)request.getSession().getAttribute("configuracaonfe") != null) {
			filtro.setConfiguracaonfe((Configuracaonfe)request.getSession().getAttribute("configuracaonfe"));
		}
		
		String mensagem = "As notas j� est�o inutilizadas : ";
		Boolean validaInutilizacao = false;
		for (NumeroNfeNaoUtilizadoBean notaForInulizar : filtro.getListaNfeNaoUtilizado()) {
			if(notaForInulizar.getIsChecked()){
				List<Inutilizacaonfe> listainutilizacaonfe = inutilizacaonfeService.validaInutilizacao(notaForInulizar.getNumeroNFE().intValue());
				if(!listainutilizacaonfe.isEmpty() && !validaInutilizacao){
					mensagem +=  notaForInulizar.getNumeroNFE().intValue();
					validaInutilizacao = true;
				}else if(!listainutilizacaonfe.isEmpty() && validaInutilizacao){
					mensagem += "," + notaForInulizar.getNumeroNFE().intValue();
				}
			}
		}
		if(validaInutilizacao){
			request.addError(mensagem);
		}
		//Se false para valida��o, as notas s�o adicionadas para inutiliza��o
		if(!validaInutilizacao){
			try {
				List<Inutilizacaonfe> listaInutilizacaoNfe = null;
				listaInutilizacaoNfe = new ArrayList<Inutilizacaonfe>();
				for (NumeroNfeNaoUtilizadoBean bean : filtro.getListaNfeNaoUtilizado()) {
					if (bean.getIsChecked()) {
						Inutilizacaonfe inutilizacaonfe = new Inutilizacaonfe();
						inutilizacaonfe.setEmpresa(filtro.getEmpresa());
						inutilizacaonfe.setConfiguracaonfe(filtro.getConfiguracaonfe());
						inutilizacaonfe.setDtinutilizacao(SinedDateUtils.currentDate());
						inutilizacaonfe.setNuminicio(bean.getNumeroNFE().intValue());
						inutilizacaonfe.setNumfim(bean.getNumeroNFE().intValue());
						inutilizacaonfe.setJustificativa(filtro.getJustificativa());
						inutilizacaonfe.setSituacao(Inutilizacaosituacao.GERADO);
						inutilizacaonfe.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFE);
						listaInutilizacaoNfe.add(inutilizacaonfe);
					}
				}
	
				for (Inutilizacaonfe inutilizacaonfe : listaInutilizacaoNfe) {
					inutilizacaonfeService.saveOrUpdate(inutilizacaonfe);
				}
				request.addMessage("Inutiliza��o de n�mero da NF-e gerada com sucesso.");
			} catch (Exception e) {
				request.addError(e.getMessage());
				request.addError("Erro ao gerada Inutiliza��o de n�mero da NF-e.");
			}
		}
		return sendRedirectToAction("index");
	}
}
