package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.service.ArquivoretornocontratoService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.ContratotipoService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ArquivoRetornoContratoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ArquivoRetornoContratoItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.filter.ArquivoRetornoContratoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/faturamento/process/ArquivoRetornoContrato", authorizationModule=ProcessAuthorizationModule.class)
public class ArquivoRetornoContratoProcess extends MultiActionController{
	
	private static final int QTDE_COLUNAS_LINHA_TIPO_RMPC = 128;
	private static final int QTDE_COLUNAS_LINHA_TIPO_SCPC = 128;
	private static final int QTDE_COLUNAS_LINHA_TIPO_SUA_CONTA = 3;
	private static final String ARQUIVORETORNOCONTRATO_FILTRO_SESSAO = "ARQUIVORETORNOCONTRATO_FILTRO_SESSAO";
	
	private ContratoService contratoService;
	private ContratotipoService contratotipoService;
	private MaterialService materialService;
	private ArquivoretornocontratoService arquivoretornocontratoService;
	
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setContratotipoService(ContratotipoService contratotipoService) {this.contratotipoService = contratotipoService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setArquivoretornocontratoService(ArquivoretornocontratoService arquivoretornocontratoService) {this.arquivoretornocontratoService = arquivoretornocontratoService;}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, ArquivoRetornoContratoFiltro filtro) throws Exception {
		request.setAttribute("listaTipo", Arrays.asList(ArquivoRetornoContratoFiltro.TIPO_RMPC, ArquivoRetornoContratoFiltro.TIPO_SCPC, ArquivoRetornoContratoFiltro.TIPO_SUA_CONTA));
		request.setAttribute("listaContratotipo", contratotipoService.findAll());
		
		if(!Boolean.TRUE.equals((Boolean)request.getAttribute("filtrou"))){
			request.getSession().setAttribute(ARQUIVORETORNOCONTRATO_FILTRO_SESSAO, null);
			filtro.setNumeroremessa(null);
		}
		request.setAttribute("permissaoListagemMaterial", Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/suprimento/crud/Material", "listagem", SinedUtil.getUsuarioLogado()));
		return new ModelAndView("process/arquivoRetornoContrato", "filtro", filtro);
	}
	
	public ModelAndView filtrar(WebRequestContext request, ArquivoRetornoContratoFiltro filtro) throws Exception {
		if(validarArquivo(filtro)){
			filtro.setListaBean(getListaBean(filtro));
			if(org.apache.commons.lang.StringUtils.isNotBlank(filtro.getNumeroremessa()) && arquivoretornocontratoService.existeArquivo(filtro.getNumeroremessa())){
				request.addMessage("Arquivo j� processado.", MessageType.ERROR);
			}else if(SinedUtil.isListNotEmpty(filtro.getListaClienteContratoNaoEncontrado())){
				StringBuilder msgErro = new StringBuilder("Existe(m) cliente(s) sem contrato ativo, favor cadastr�-los antes de prosseguir.");
				for(String clienteContratoNaoEncontrado : filtro.getListaClienteContratoNaoEncontrado()){
					msgErro.append("<br>").append(clienteContratoNaoEncontrado);
				}
				request.addMessage(msgErro, MessageType.ERROR);
			}else {
				request.setAttribute("filtrou", true);
			}
		}else {
			request.addMessage("Arquivo inv�lido.", MessageType.ERROR);			
		}
		request.getSession().setAttribute(ARQUIVORETORNOCONTRATO_FILTRO_SESSAO, filtro.getArquivo());
		return index(request, filtro);
	}	
	
	public ModelAndView processarArquivo(WebRequestContext request, ArquivoRetornoContratoFiltro filtro) throws Exception {
		if(validaContratos(request, filtro)){
			request.setAttribute("filtrou", true);
			return index(request, filtro);
		}
		
		filtro.setArquivo((Arquivo) request.getSession().getAttribute(ARQUIVORETORNOCONTRATO_FILTRO_SESSAO));
		if(ArquivoRetornoContratoFiltro.TIPO_SCPC.equals(filtro.getTipo())){
			contratoService.preencheMaterialRetornoContrato(filtro.getListaBean());
		}
		contratoService.transactionArquivoRetornoContrato(filtro);
		
		request.addMessage("Os registros de contrato foram atualizados com sucesso.", MessageType.INFO);
		
		filtro.setTipo(null);
		filtro.setContratotipo(null);
		filtro.setArquivo(null);
		filtro.setListaBean(null);
		
		return index(request, filtro);
	}
	
	private boolean validaContratos(WebRequestContext request, ArquivoRetornoContratoFiltro filtro) {
		boolean erro = false;
		if(ArquivoRetornoContratoFiltro.TIPO_SCPC.equals(filtro.getTipo())){
			if(SinedUtil.isListNotEmpty(filtro.getListaBean())){
				StringBuilder whereInMaterial = new StringBuilder();
				List<Cliente> listaCliente = new ArrayList<Cliente>();
				StringBuilder erroClientes = new StringBuilder();
				for(ArquivoRetornoContratoBean bean : filtro.getListaBean()){
					if(Boolean.TRUE.equals(bean.getSelecao()) && bean.getContrato() != null){
						if(SinedUtil.isListNotEmpty(bean.getListaItens())){
							for(ArquivoRetornoContratoItemBean item : bean.getListaItens()){
								if(item.getMaterial() != null && item.getMaterial().getCdmaterial() != null){
									whereInMaterial.append(item.getMaterial().getCdmaterial()).append(",");
								}
							}
						}
						if(bean.getContrato().getCliente() != null){
							if(!listaCliente.contains(bean.getContrato().getCliente())){
								listaCliente.add(bean.getContrato().getCliente());
							}else {
								if(!erroClientes.toString().contains(bean.getContrato().getCliente().getNome())){
									erroClientes.append("<br>").append(bean.getContrato().getCliente().getNome());
								}
								erro = true;
							}
						}
					}
				}
				
				if(erro && erroClientes.length() > 0){
					request.addError("� permitido selecionar apenas um contrato por cliente. Favor verificar os clientes abaixo: " + erroClientes);
				}
				
				if(whereInMaterial.length() > 0){
					List<Material> listaMaterial = materialService.findForRetornoContrato(whereInMaterial.substring(0, whereInMaterial.length()-1));
					if(SinedUtil.isListNotEmpty(listaMaterial)){
						for(Material material : listaMaterial){
							if(material.getValorvenda() == null){
								request.addError("Produto/Servi�o '" + material.getIdentificacaoNomeSerie() + "' sem valor de venda. ");
								erro = true;
							}else {
								for(ArquivoRetornoContratoBean bean : filtro.getListaBean()){
									if(Boolean.TRUE.equals(bean.getSelecao()) && bean.getContrato() != null && SinedUtil.isListNotEmpty(bean.getListaItens())){
										for(ArquivoRetornoContratoItemBean item : bean.getListaItens()){
											if(material.equals(item.getMaterial())){
												item.getMaterial().setValorvenda(material.getValorvenda());
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	
		return erro;
	}
	
	private List<ArquivoRetornoContratoBean> getListaBean(ArquivoRetornoContratoFiltro filtro) throws Exception {
		List<ArquivoRetornoContratoBean> listaBean = new ArrayList<ArquivoRetornoContratoBean>();
		List<Contrato> listaContrato = contratoService.findForArquivoRetornoContrato(filtro);
		String[] linhas = getLinhas(filtro);
		
		List<String> listaClienteContratoNaoEncontrado = new ArrayList<String>();
		
		for (int i=0; i<linhas.length; i++){
			String linha1 = linhas[i];
			String linha3;
			String linha4;
			Cpf cpf = null;
			Cnpj cnpj = null;
			String razaosocial = null;
			String clienteContratoNaoEncontrado = null;
			
			if (ArquivoRetornoContratoFiltro.TIPO_RMPC.equals(filtro.getTipo())){
				if (linha1.startsWith("1")){
					linha3 = linhas[i + 2];
					
					try {
						if (linha1.substring(89, 90).equalsIgnoreCase("F")){
							cpf = new Cpf(SinedUtil.formataTamanho(new Long(linha1.substring(90, 104)).toString(), 11, '0', false));
						}else if (linha1.substring(89, 90).equalsIgnoreCase("J")){
							cnpj = new Cnpj(linha1.substring(90, 104));
						}
					} catch (Exception e) {
					}
					
					for (Contrato contrato: getListaContrato(listaContrato, cpf, cnpj)){
						ArquivoRetornoContratoBean bean = new ArquivoRetornoContratoBean();
						bean.setContrato(contrato);
						bean.setValorArquivo(new Money(new Long(linha3.substring(45, 58)), true));
						listaBean.add(bean);
					}
					
				}
			}else if (ArquivoRetornoContratoFiltro.TIPO_SUA_CONTA.equals(filtro.getTipo())){
				String[] colunas = linha1.split("\\;");
				
				try {
					cpf = new Cpf(colunas[0]);
				} catch (Exception e) {
				}
				
				try {
					cnpj = new Cnpj(colunas[0]);
				} catch (Exception e) {
				}
				
				for (Contrato contrato: getListaContrato(listaContrato, cpf, cnpj)){
					ArquivoRetornoContratoBean bean = new ArquivoRetornoContratoBean();
					bean.setContrato(contrato);
					bean.setValorArquivo(new Money(colunas[1]));
					listaBean.add(bean);
				}
			}else if (ArquivoRetornoContratoFiltro.TIPO_SCPC.equals(filtro.getTipo())){
				if(i == 0){
					filtro.setNumeroremessa(linha1.substring(73,83));
				}
				if (linha1.startsWith("1")){
					linha3 = linhas[i + 2];
					
					try {
						if (linha1.substring(89, 90).equalsIgnoreCase("F")){
							cpf = new Cpf(SinedUtil.formataTamanho(new Long(linha1.substring(90, 104)).toString(), 11, '0', false));
						}else if (linha1.substring(89, 90).equalsIgnoreCase("J")){
							cnpj = new Cnpj(linha1.substring(90, 104));
						}
						razaosocial = linha1.substring(44, 89);
						clienteContratoNaoEncontrado = razaosocial + (cpf != null ? " - " + cpf : cnpj != null ? " - " + cnpj : "");
					} catch (Exception e) {
					}
					
					List<ArquivoRetornoContratoItemBean> listaItens = new ArrayList<ArquivoRetornoContratoItemBean>();
					ArquivoRetornoContratoItemBean itemBean; 
					int qtdeItem = 1;
					while(true){
						int indexItem = i + 2 + qtdeItem;
						if(linhas.length < indexItem){
							break;
						}
						
						linha4 = linhas[indexItem];
						if(!linha4.startsWith("4")){
							break;
						}
						
						itemBean = new ArquivoRetornoContratoItemBean();
						itemBean.setDatageracao(SinedDateUtils.stringToDate(linha4.substring(19, 27), "ddMMyyyy"));
						itemBean.setDatavencimento(SinedDateUtils.stringToDate(linha4.substring(27, 35), "ddMMyyyy"));
						itemBean.setCodigo(linha4.substring(45, 50));
						itemBean.setDescricao(linha4.substring(50, 75));
						itemBean.setQtde(Double.parseDouble(linha4.substring(75, 80)));
						itemBean.setValortotal(Double.parseDouble(linha4.substring(93, 106))/100d);
						if(org.apache.commons.lang.StringUtils.isNotBlank(itemBean.getCodigo())){
							itemBean.setMaterial(materialService.findByIdentificacao(itemBean.getCodigo()));
						}
						listaItens.add(itemBean);
						
						qtdeItem++;
					};
					
					boolean existeContratoCliente = false;
					for (Contrato contrato: getListaContrato(listaContrato, cpf, cnpj)){
						ArquivoRetornoContratoBean bean = new ArquivoRetornoContratoBean();
						bean.setContrato(contrato);
						bean.setValorArquivo(new Money(new Long(linha3.substring(45, 58)), true));
						bean.setListaItens(listaItens);
						listaBean.add(bean);
						existeContratoCliente = true;
					}
					
					if(!existeContratoCliente && !listaClienteContratoNaoEncontrado.contains(clienteContratoNaoEncontrado)){
						listaClienteContratoNaoEncontrado.add(clienteContratoNaoEncontrado);
					}
				}
			}			
		}
		
		Collections.sort(listaBean, new Comparator<ArquivoRetornoContratoBean>() {
			@Override
			public int compare(ArquivoRetornoContratoBean o1, ArquivoRetornoContratoBean o2) {
				int compare1 = o1.getContrato().getCliente().getNome().compareToIgnoreCase(o2.getContrato().getCliente().getNome());
				int compare2 = o1.getContrato().getDescricaoIdentificador().compareToIgnoreCase(o2.getContrato().getDescricaoIdentificador());
				
				if (compare1==0){
					return compare2;
				}else {
					return compare1;
				}
			}
		});
		
		filtro.setListaClienteContratoNaoEncontrado(listaClienteContratoNaoEncontrado);
		return listaBean;
	}
	
	private String[] getLinhas(ArquivoRetornoContratoFiltro filtro) throws Exception {
		return new String(filtro.getArquivo().getContent()).split("\\r\\n");
	}
	
	private boolean validarArquivo(ArquivoRetornoContratoFiltro filtro) throws Exception {
		try {
			String[] linhas = getLinhas(filtro);
			
			if (linhas.length>0){
				int qtdeLinhas = 0;
				for (String linha: linhas){
					if (ArquivoRetornoContratoFiltro.TIPO_RMPC.equals(filtro.getTipo())){
						if (linha.length()!=QTDE_COLUNAS_LINHA_TIPO_RMPC){
							return false;
						}
					}else if (ArquivoRetornoContratoFiltro.TIPO_SUA_CONTA.equals(filtro.getTipo())){
						if (StringUtils.countOccurrencesOf(linha, ";")!=QTDE_COLUNAS_LINHA_TIPO_SUA_CONTA - 1){
							return false;
						}
					}else if (ArquivoRetornoContratoFiltro.TIPO_SCPC.equals(filtro.getTipo())){
						if (linha.length()!=QTDE_COLUNAS_LINHA_TIPO_SCPC || (qtdeLinhas == 0 && !linha.substring(0, 1).equals("0"))){
							return false;
						}
					}
					qtdeLinhas++;
				}
				
				return true;
			}			
		} catch (Exception e) {
		}
		
		return false;
	}
	
	private List<Contrato> getListaContrato(List<Contrato> listaContrato, Cpf cpf, Cnpj cnpj){
		List<Contrato> listaContratoRetorno = new ArrayList<Contrato>();
		
		for (Contrato contrato: listaContrato){
			if (cpf!=null && cpf.equals(contrato.getCliente().getCpf())){
				listaContratoRetorno.add(contrato);
			}else if (cnpj!=null && cnpj.equals(contrato.getCliente().getCnpj())){
				listaContratoRetorno.add(contrato);
			}
		}
		
		return listaContratoRetorno;
	}
}