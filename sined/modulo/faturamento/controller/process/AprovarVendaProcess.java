package br.com.linkcom.sined.modulo.faturamento.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/faturamento/process/AprovarVenda", authorizationModule=ProcessAuthorizationModule.class)
public class AprovarVendaProcess extends MultiActionController {

	/**
	 * M�todo que prepara a venda para Aprova��o
	 * 
	 * @param request
	 * @param venda
	 * @return
	 * @author Tom�s Rabelo
	 */
	@DefaultAction
	public ModelAndView index(WebRequestContext request,Venda venda){
		String cdvenda = request.getParameter("cdvenda");
		if(cdvenda == null || cdvenda.equals(""))
			throw new SinedException("Par�metro inv�lido.");

		String complementoOtr = SinedUtil.complementoOtr();
		SinedUtil.redirecionamento(request, "/faturamento/crud/Venda"+complementoOtr+"?ACAO=editar&aprovarOrcamento=true&cdvenda=" + cdvenda);
		
		return null;
	}
}