package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Faturamentocontrato;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaContrato;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Referencia;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.enumeration.Mes;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaodespesaviagem;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculodias;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.DespesaviagemService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentoorigemService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.FaturamentocontratoService;
import br.com.linkcom.sined.geral.service.NotaDocumentoService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotaHistoricoService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.RateioitemService;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/faturamento/process/Gerareceitaemlote", authorizationModule=CrudAuthorizationModule.class)
public class GerareceitaemloteProcess extends MultiActionController {
	
	private FaturamentocontratoService faturamentocontratoService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private DocumentotipoService documentotipoService;
	private RateioitemService rateioitemService;
	private RateioService rateioService;
	private NotaService notaService;
	private DocumentoService documentoService;
	private NotaDocumentoService notaDocumentoService;
	private NotaHistoricoService notaHistoricoService;
	private ClienteService clienteService;
	private ContaService contaService;
	private DocumentoorigemService documentoorigemService;
	private DespesaviagemService despesaviagemService;
	private ArquivonfnotaService arquivonfnotaService;
	private ContratoService contratoService;
	private ParametrogeralService parametrogeralService;
	
	public void setArquivonfnotaService(
			ArquivonfnotaService arquivonfnotaService) {
		this.arquivonfnotaService = arquivonfnotaService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setFaturamentocontratoService(
			FaturamentocontratoService faturamentocontratoService) {
		this.faturamentocontratoService = faturamentocontratoService;
	}
	public void setNotaFiscalServicoService(
			NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	public void setDocumentotipoService(
			DocumentotipoService documentotipoService) {
		this.documentotipoService = documentotipoService;
	}
	public void setRateioitemService(RateioitemService rateioitemService) {
		this.rateioitemService = rateioitemService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setNotaService(NotaService notaService) {
		this.notaService = notaService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setNotaDocumentoService(
			NotaDocumentoService notaDocumentoService) {
		this.notaDocumentoService = notaDocumentoService;
	}
	public void setNotaHistoricoService(
			NotaHistoricoService notaHistoricoService) {
		this.notaHistoricoService = notaHistoricoService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setDocumentoorigemService(
			DocumentoorigemService documentoorigemService) {
		this.documentoorigemService = documentoorigemService;
	}
	public void setDespesaviagemService(DespesaviagemService despesaviagemService) {
		this.despesaviagemService = despesaviagemService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	/**
	 * M�todo que gera receita em lote
	 * 
	 * @param request
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView gerarReceita(WebRequestContext request){
		String whereIn = request.getParameter("cds");
		
		List<Faturamentocontrato> listaFaturamentos = faturamentocontratoService.findFaturamentos(whereIn);
		faturamentocontratoService.preencheListaFaturamentos(listaFaturamentos);
		 
		Documentotipo documentotipo = documentotipoService.findNotafiscal();
		if(documentotipo == null || documentotipo.getCddocumentotipo() == null){
			request.addError("Favor cadastrar um tipo de documento que seja nota fiscal.");
			return new ModelAndView("redirect:/faturamento/crud/Faturamentocontrato");
		}
		
		for (Faturamentocontrato faturamentocontrato : listaFaturamentos) 
			this.geraReceita(faturamentocontrato, documentotipo);
		
		request.addMessage("Receita(s) gerada(s) com sucesso.", MessageType.INFO);
		return new ModelAndView("redirect:/faturamento/crud/Faturamentocontrato");
	}
	
	/**
	 * M�todo que seta os valores na conta a receber
	 * 
	 * @param faturamentocontrato
	 * @param documentotipo
	 * 
	 * @author Tom�s Rabelo
	 */
	private void geraReceita(Faturamentocontrato faturamentocontrato, Documentotipo documentotipo) {
		Nota notaAux = faturamentocontrato.getListaNotascontrato().iterator().next().getNota();
		Contrato contratoAux = faturamentocontrato.getListaNotascontrato().iterator().next().getContrato();
		
		Documento documento = new Documento();
		
		documento.setDocumentotipo(documentotipo);
		
		documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
		documento.setDocumentoacao(Documentoacao.PREVISTA);
		documento.setTipopagamento(Tipopagamento.CLIENTE);
		documento.setReferencia(Referencia.MES_ANO_CORRENTE);

		documento.setPessoa(contratoAux.getCliente());
		documento.setDtemissao(notaAux.getDtEmissao());
		documento.setDtvencimento(contratoService.getDataVencimento(contratoAux, contratoService.getDtVencimento(faturamentocontrato.getListaNotascontrato())));
		documento.setValor(faturamentocontrato.getValortotalaux());
		
		documento.setEmpresa(contratoAux.getEmpresa());
		documento.setConta(contratoAux.getConta());
		documento.setEndereco(contratoAux.getEnderecocobranca());
		
		if(documento.getEndereco() == null && notaAux.getCliente() != null){
			Cliente cliente = clienteService.loadForBoleto(notaAux.getCliente().getCdpessoa());
			if(cliente.getListaEndereco() != null)
				documento.setEndereco(cliente.getEnderecoWithPrioridade(Enderecotipo.UNICO, Enderecotipo.FATURAMENTO));
		}
		
		if(contratoAux.getConta() != null){
			Conta conta = contaService.carregaContaComMensagem(contratoAux.getConta(), null);
			
			//Preenche a carteira do documento com a carteira padr�o
			documento.setContacarteira(conta.getContacarteira());
			
			if(conta.getContacarteira().getMsgboleto1() != null)
				documento.setMensagem4(conta.getContacarteira().getMsgboleto1());
			
			if(conta.getContacarteira().getMsgboleto2() != null)
				documento.setMensagem5(conta.getContacarteira().getMsgboleto2());	
		}
				
		documento.setAcaohistorico(Documentoacao.PREVISTA);
		
		StringBuilder observacaoServico = new StringBuilder("");
		StringBuilder observacaoProduto = new StringBuilder("");
		StringBuilder observacaoDespesaviagem = new StringBuilder("");
//		StringBuilder mensagem = new StringBuilder("");
		StringBuilder numeros = new StringBuilder("");
		StringBuilder mesAnoReferente = new StringBuilder("");
		for (NotaContrato notaContrato : faturamentocontrato.getListaNotascontrato()) {
			if(notaContrato.getNota() instanceof NotaFiscalServico){
				observacaoServico.append("<a href=\"javascript:visualizarNotaFiscalServico(")
									.append(notaContrato.getNota().getCdNota()).append(")\">")
									.append((notaContrato.getNota().getNumero() != null ? notaContrato.getNota().getNumero() : "sem n�mero"))
									.append("</a>, ");
				Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaNotCancelada((NotaFiscalServico)notaContrato.getNota());
				if(arquivonfnota != null){
					numeros.append(notaFiscalServicoService.formataNumNfse(arquivonfnota.getNumeronfse())).append("  ").append((arquivonfnota.getCodigoverificacao() != null && !arquivonfnota.getCodigoverificacao().equals("") ? " C�digo de Verifica��o da NFS-e: "+arquivonfnota.getCodigoverificacao() : "")).append(", ");
				} else {
					numeros.append((notaContrato.getNota().getNumero() != null ? notaContrato.getNota().getNumero() : "<sem n�mero>")).append(", ");
				}
			}else{
				observacaoProduto.append("<a href=\"javascript:visualizarNotaFiscalProduto(")
								 .append(notaContrato.getNota().getCdNota()).append(")\">")
								 .append((notaContrato.getNota().getNumero() != null ? notaContrato.getNota().getNumero() : "sem n�mero"))
								 .append("</a>, ");
				numeros.append(notaContrato.getNota().getNumero() != null ? notaContrato.getNota().getNumero() : "<sem n�mero>").append(", ");
			}
			if(mesAnoReferente.toString().equals("") && notaContrato.getContrato().getFrequencia() != null && notaContrato.getContrato().getFrequencia().equals(new Frequencia(Frequencia.MENSAL))){
				
				Mes mes = notaContrato.getContrato().getMes();
				Integer ano = notaContrato.getContrato().getAno();
				
				if(mes != null && ano != null){
					if(mes.ordinal() > 0){
						mes = Mes.values()[mes.ordinal() - 1];
					} else {
						mes = Mes.DEZEMBRO;
						ano = ano - 1;
					}
					
					mesAnoReferente
						.append(" Referente a: ")
						.append(mes)
						.append(" / ")
						.append(ano);
				}
			}
		}
		
		if(faturamentocontrato.getListaDespesaviagem() != null){
			for (Despesaviagem despesaviagem : faturamentocontrato.getListaDespesaviagem()) {
				if(despesaviagem.getCddespesaviagem() != null){
					observacaoDespesaviagem.append("<a href=\"javascript:visualizarDespesaviagem(")
										.append(despesaviagem.getCddespesaviagem()).append(")\">")
										.append(despesaviagem.getCddespesaviagem())
										.append("</a>, ");
				}
			}
		}
		
		if(observacaoServico != null && !observacaoServico.toString().equals("")){
			observacaoServico.insert(0, "Origem da nota de servi�o ");
			observacaoServico.delete(observacaoServico.length()-2, observacaoServico.length());
			observacaoServico.insert(observacaoServico.length(), "<BR>");
		}
		if(observacaoProduto != null && !observacaoProduto.toString().equals("")){
			observacaoProduto.insert(0, "Origem da nota de produto ");
			observacaoProduto.delete(observacaoProduto.length()-2, observacaoProduto.length());
		}
		if(observacaoDespesaviagem != null && !observacaoDespesaviagem.toString().equals("")){
			observacaoDespesaviagem.insert(0, "Origem da despesa de viagem");
			observacaoDespesaviagem.delete(observacaoDespesaviagem.length()-2, observacaoDespesaviagem.length());
		}
		/*
		if(mensagem != null && !mensagem.toString().equals("")){
			mensagem.insert(0, "Referente a NF: ");
			mensagem.delete(mensagem.length()-2, mensagem.length());
		}*/
		if(numeros != null && !numeros.toString().equals("")){
			numeros.delete(numeros.length()-2, numeros.length());
		}
		
		documento.setObservacaoHistorico(observacaoServico.toString()+observacaoProduto.toString()+observacaoDespesaviagem.toString());
		if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_MENSAGEM_NUMERO_NF)){
			documento.setMensagem1("Referente a NF: "+numeros.toString());
		}
		
		documento.setDescricao("Documento gerado a partir da(s) NF: "+numeros.toString() + mesAnoReferente);
		documento.setNumero(numeros.toString());
		
		Documentohistorico documentohistorico = new Documentohistorico(documento);
		
		List<Documentohistorico> listaHistorico = new ArrayList<Documentohistorico>();
		listaHistorico.add(documentohistorico);
		
		documento.setListaDocumentohistorico(new ListSet<Documentohistorico>(Documentohistorico.class, listaHistorico));
		
		Rateio rateio = new Rateio();
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		
		listaRateioitem = faturamentocontrato.getListarateioitemWithProjeto();
		
		Taxa taxa = new Taxa();
		List<Taxaitem> listaTaxaitem = new ArrayList<Taxaitem>();
		
		if(contratoAux.getRateio() != null && contratoAux.getRateio().getCdrateio() != null){
			listaRateioitem.addAll(rateioitemService.findByRateio(contratoAux.getRateio()));
		}
		
		contratoService.preencherTaxasContrato(listaTaxaitem, contratoAux);
		
		rateioitemService.agruparItensRateio(listaRateioitem);
		
		String msg;
		String msg1 = "";
		String msg2 = "";
		for (Taxaitem taxaitem : listaTaxaitem) {
			taxaitem.setDtlimite(documento.getDtvencimento());
			
			if(taxaitem.getDias() != null && documento.getDtvencimento() != null){
				if(taxaitem.getTipocalculodias() != null && taxaitem.getTipocalculodias().equals(Tipocalculodias.ANTES_VENCIMENTO)){
					taxaitem.setDtlimite(SinedDateUtils.addDiasData(documento.getDtvencimento(), -taxaitem.getDias()));
				}else {
					taxaitem.setDtlimite(SinedDateUtils.addDiasData(documento.getDtvencimento(), taxaitem.getDias()));
				}
			}
			
			msg = taxaitem.getTipotaxa().getNome() + " de " + (taxaitem.isPercentual() ? "" : "R$") + taxaitem.getValor().toString() + (taxaitem.isPercentual() ? "%" : "") + ateApos(taxaitem.getTipotaxa().getCdtipotaxa()) + SinedDateUtils.toString(taxaitem.getDtlimite()) + ". ";
			if((msg1+msg).length() <= 80){
				msg1 += msg;
			} else if((msg2+msg).length() <= 80){
				msg2 += msg;
			}
		}
		
		documento.setMensagem2(msg1);
		documento.setMensagem3(msg2);
		rateio.getListaRateioitem().addAll(listaRateioitem);
		rateioService.atualizaValorRateio(rateio, documento.getValor());
		
		taxa.setListaTaxaitem(new ListSet<Taxaitem>(Taxaitem.class, listaTaxaitem));
		
		documento.setRateio(rateio);
		documento.setTaxa(taxa);
		
		documentoService.saveOrUpdate(documento);
		
		for (NotaContrato notaContrato : faturamentocontrato.getListaNotascontrato()) {
			notaService.liquidarNota(notaContrato.getNota());
			
			NotaDocumento notaDocumento = new NotaDocumento();
			notaDocumento.setDocumento(documento);
			notaDocumento.setNota(notaContrato.getNota());
			notaDocumentoService.saveOrUpdate(notaDocumento);

			NotaHistorico notaHistorico = new NotaHistorico(null, 
					notaContrato.getNota(), 
					notaContrato.getNota().getNotaStatus(), 
					"Gerada a conta a receber <a href=\"javascript:visualizarContaReceber("+documento.getCddocumento()+")\">"+documento.getCddocumento()+"</a>", 
					SinedUtil.getUsuarioLogado().getCdpessoa(), 
					new Timestamp(System.currentTimeMillis()));

			notaHistoricoService.saveOrUpdate(notaHistorico);
		}
		
		if(faturamentocontrato.getListaDespesaviagem() != null && !faturamentocontrato.getListaDespesaviagem().isEmpty()){
			Documentoorigem documentoorigem;
			for(Despesaviagem despesaviagem : faturamentocontrato.getListaDespesaviagem()){
				if(despesaviagem.getCddespesaviagem() != null){	
					documentoorigem = new Documentoorigem();
					documentoorigem.setDocumento(documento);
					documentoorigem.setDespesaviagemacerto(despesaviagem);
					despesaviagemService.updateSituacao(despesaviagem.getCddespesaviagem().toString(), Situacaodespesaviagem.BAIXADA);
					documentoorigemService.saveOrUpdate(documentoorigem);
				}
			}
		}
		
	}
	
	private String ateApos(Integer cdtipotaxa){
		if(cdtipotaxa == 3 || cdtipotaxa == 4){
			return " at� ";
		}
		return " ap�s ";
	}
	
}
