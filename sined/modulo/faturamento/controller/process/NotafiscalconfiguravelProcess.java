package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.money.Extenso;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresaconfiguracaonf;
import br.com.linkcom.sined.geral.bean.Empresaconfiguracaonfcampo;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoItem;
import br.com.linkcom.sined.geral.service.EmpresaconfiguracaonfService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.NotafiscalconfiguravelBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.utils.StringUtils;

@Controller(path="/faturamento/process/Notafiscalconfiguravel", authorizationModule=ProcessAuthorizationModule.class)
public class NotafiscalconfiguravelProcess extends MultiActionController {
	
	private EmpresaconfiguracaonfService empresaconfiguracaonfService;
	private NotaFiscalServicoService notaFiscalServicoService;
	
	public void setNotaFiscalServicoService(
			NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	public void setEmpresaconfiguracaonfService(
			EmpresaconfiguracaonfService empresaconfiguracaonfService) {
		this.empresaconfiguracaonfService = empresaconfiguracaonfService;
	}
	
	public ModelAndView escolhaConf(WebRequestContext request, NotafiscalconfiguravelBean bean){
		return new ModelAndView("direct:/process/popup/escolhaConf", "bean", bean);
	}
	
	public void confirmaEscolha(WebRequestContext request, NotafiscalconfiguravelBean bean){
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
				"<script>parent.location = '../../faturamento/process/Notafiscalconfiguravel?ACAO=gerarArquivo&selectedItens=" + bean.getSelectedItens() + "&empresaconfiguracaonf.cdempresaconfiguracaonf=" + bean.getEmpresaconfiguracaonf().getCdempresaconfiguracaonf() + "';parent.$.akModalRemove(true);</script>");
	}
	
	public ModelAndView gerarArquivo(WebRequestContext request, NotafiscalconfiguravelBean bean){
		Empresaconfiguracaonf conf = empresaconfiguracaonfService.loadForEntrada(bean.getEmpresaconfiguracaonf());
		NotaFiltro filtro = new NotaFiltro();
		filtro.setWhereIn(bean.getSelectedItens());
		List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForReport(filtro);
		
		StringBuilder sb = new StringBuilder();
		
		for (NotaFiscalServico nf : listaNota) {
			if(nf.getEnderecoCliente() == null) {
				request.addError("Favor cadastrar o endere�o do cliente.");
				continue;
			}
			
			sb.append(this.makeNF(nf, conf));
			sb.append("\n\n"); // QUEBRA DE P�GINA
		}
		
		if(request.getBindException().hasErrors()){
			return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto");
		}
		
		Resource resource = new Resource("application/remote_printing", "impressaonf_" + SinedUtil.datePatternForReport() + ".w3erp", sb.toString().getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}

	private String makeNF(NotaFiscalServico nf, Empresaconfiguracaonf conf) {
		StringBuilder sb = new StringBuilder();
		List<Empresaconfiguracaonfcampo> listaCampo;
		for (int i = 1; i <= conf.getLinhasnotafiscal(); i++) {
			if(i == conf.getLinhacorpoinicio()){
				listaCampo = this.getCamposCorpo(conf.getListaCampo());
				sb.append(this.makeCorpo(listaCampo, conf.getLinhacorpoinicio(), conf.getLinhacorpofim(), nf));
				i = conf.getLinhacorpofim();
			} else {
				listaCampo = this.getCamposLinha(conf.getListaCampo(), i);
				sb.append(this.makeLinha(listaCampo, nf));
				sb.append("\n");
			}
		}
		
		return sb.toString();
	}

	
	private String makeCorpo(List<Empresaconfiguracaonfcampo> listaCampo, Integer linhacorpoinicio, Integer linhacorpofim, NotaFiscalServico nf) {
		int linhasescritas = 1;
		int totalcorpo = (linhacorpofim - linhacorpoinicio) + 1;
		
		Collections.sort(listaCampo, new Comparator<Empresaconfiguracaonfcampo>(){
			public int compare(Empresaconfiguracaonfcampo o1, Empresaconfiguracaonfcampo o2) {
				return o1.getColuna().compareTo(o2.getColuna());
			}
		});
		
		StringBuilder sb = new StringBuilder();
		
		List<NotaFiscalServicoItem> listaItens = nf.getListaItens();
		int coluna, col;
		String str;
		char[] charArray;
		for (NotaFiscalServicoItem it : listaItens) {
			coluna = 1;
			
			for (Empresaconfiguracaonfcampo cp : listaCampo) {
				sb.append(StringUtils.geraStringVazia(cp.getColuna() - coluna));
				str = this.getCampoCorpoStr(it, cp);
				charArray = str.toCharArray();
				col = cp.getTamanhomax();
				for (int i = 0; i < charArray.length; i++) {
					if(col == 0) {
						col = cp.getTamanhomax();
						sb.append("\n");
						linhasescritas++;
						if(linhasescritas > totalcorpo){
							return sb.toString();
						}
						
						sb.append(StringUtils.geraStringVazia(cp.getColuna() - 1));
					}
					sb.append(charArray[i]);
					col--;
				}
				
				coluna = cp.getColuna() + cp.getTamanhomax();
				sb.append(StringUtils.geraStringVazia(col));
			}
			
			if((linhasescritas + 2) > totalcorpo){
				return sb.toString();
			}
			sb.append("\n\n");
			linhasescritas+=2;
		}
		
		for (int i = linhasescritas; i <= totalcorpo; i++) {
			sb.append("\n");
		}
		
		return sb.toString();
	}
	
	private String getCampoCorpoStr(NotaFiscalServicoItem it, Empresaconfiguracaonfcampo cp) {
		StringBuilder sb = new StringBuilder();
		
		switch (cp.getCamponota()) {
			case CAMPO_LIVRE:
				sb.append(cp.getCampolivre());
				break;
			
			case QTDE_NOTAITEM:
				sb.append(it.getQtde());
				break;
				
			case DISCRIMINACAOSERVICO_NOTAITEM:
				if (it.getDescricao() != null && !it.getDescricao().equals("")) {
					sb.append(it.getDescricao());
				} else {
					if (it.getMaterial() != null && it.getMaterial().getNome() != null && !it.getMaterial().getNome().equals("")) {
						sb.append(it.getMaterial().getNome());
					}
				}
				break;
				
			case VALORUNITARIO_NOTAITEM:
				sb.append(it.getPrecoUnitario().toString());
				break;
				
			case VALORTOTALITEM_NOTAITEM:
				sb.append(it.getTotalParcial().toString());
				break;
			
			default:
				break;
		}
		
		return sb.toString();
	}

	
	private List<Empresaconfiguracaonfcampo> getCamposCorpo(List<Empresaconfiguracaonfcampo> listaCampo) {
		List<Empresaconfiguracaonfcampo> lista = new ArrayList<Empresaconfiguracaonfcampo>();
		for (Empresaconfiguracaonfcampo campo : listaCampo) {
			if(campo.getCorpo() != null && campo.getCorpo()){
				lista.add(campo);
			}
		}
		
		return lista;
	}
	
	private String makeLinha(List<Empresaconfiguracaonfcampo> listaCampo, NotaFiscalServico nf) {
		StringBuilder sb = new StringBuilder();
		String str;
		Empresaconfiguracaonfcampo campo;
		Iterator<Empresaconfiguracaonfcampo> iterator = listaCampo.iterator();
		boolean preencheu;
		for (int i = 1; iterator.hasNext(); i++) {
			preencheu = false;
			while(iterator.hasNext()){
				campo = iterator.next();
				
				str = "";
				if(campo.getColuna() != null && campo.getColuna().equals(i)){
					str = this.getCampoStr(campo, nf);
					
					if(campo.getTamanhomax() != null){
						if(str.length() > campo.getTamanhomax()){ 
							str = str.substring(0, campo.getTamanhomax());
						} else {
							str = br.com.linkcom.neo.util.StringUtils.stringCheia(str, " ", campo.getTamanhomax(), true);
						}
					}
					i = i + str.length() - 1;
					iterator.remove();
					sb.append(str);
					preencheu = true;
					break;
				}
			}
			if(!preencheu) sb.append(" ");
			iterator = listaCampo.iterator();
		}
		return sb.toString();
	}
	
	private String getCampoStr(Empresaconfiguracaonfcampo campo, NotaFiscalServico nf) {
		StringBuilder sb = new StringBuilder();
		Endereco endereco;
		Municipio municipio;
		Cliente cliente;
		Date dtemissao;
		switch (campo.getCamponota()) {
			case CAMPO_LIVRE:
				sb.append(campo.getCampolivre());
				break;
	
			case NUMERO_NOTA :
				if(nf.getNumeroFormatado() != null && !nf.getNumeroFormatado().equals("")){
					sb.append(nf.getNumeroFormatado());
				}
				break;
	
			case DTEMISSAO_NOTA :
				sb.append(SinedDateUtils.toString(nf.getDtEmissao()));
				break;
	
			case END_CLIENTE :
				endereco = nf.getEnderecoCliente();
				sb.append(endereco.getLogradouro());
				break;
	
			case ENDNUM_CLIENTE :
				endereco = nf.getEnderecoCliente();
				sb.append(endereco.getLogradouro());
				if(endereco.getNumero() != null && !endereco.getNumero().equals("")){
					sb.append(", ");
					sb.append(endereco.getNumero());
				}
				break;
	
			case ENDNUMCOMP_CLIENTE :
				endereco = nf.getEnderecoCliente();
				sb.append(endereco.getLogradouro());
				if(endereco.getNumero() != null && !endereco.getNumero().equals("")){
					sb.append(", ");
					sb.append(endereco.getNumero());
				}
				if(endereco.getComplemento() != null && !endereco.getComplemento().equals("")){
					sb.append(", ");
					sb.append(endereco.getComplemento());
				}
				break;
	
			case ENDNUMCOMPBAI_CLIENTE :
				endereco = nf.getEnderecoCliente();
				sb.append(endereco.getLogradouro());
				if(endereco.getNumero() != null && !endereco.getNumero().equals("")){
					sb.append(", ");
					sb.append(endereco.getNumero());
				}
				if(endereco.getComplemento() != null && !endereco.getComplemento().equals("")){
					sb.append(", ");
					sb.append(endereco.getComplemento());
				}
				if(endereco.getBairro() != null && !endereco.getBairro().equals("")){
					sb.append(", ");
					sb.append(endereco.getBairro());
				}
				break;
	
			case NUMERO_CLIENTE :
				endereco = nf.getEnderecoCliente();
				if(endereco.getNumero() != null && !endereco.getNumero().equals("")){
					sb.append(endereco.getNumero());
				}
				break;
	
			case COMPLEMENTO_CLIENTE :
				endereco = nf.getEnderecoCliente();
				if(endereco.getComplemento() != null && !endereco.getComplemento().equals("")){
					sb.append(endereco.getComplemento());
				}
				break;
	
			case BAIRRO_CLIENTE :
				endereco = nf.getEnderecoCliente();
				if(endereco.getBairro() != null && !endereco.getBairro().equals("")){
					sb.append(endereco.getBairro());
				}
				break;
	
			case CEP_CLIENTE :
				endereco = nf.getEnderecoCliente();
				if(endereco.getCep() != null && !endereco.getCep().toString().equals("")){
					sb.append(endereco.getCep().toString());
				}
				break;
	
			case MUNICIPIO_CLIENTE :
				endereco = nf.getEnderecoCliente();
				if(endereco.getMunicipio() != null){
					municipio = endereco.getMunicipio();
					sb.append(municipio.getNome());
				}
				break;
	
			case MUNICIPIOUF_CLIENTE :
				endereco = nf.getEnderecoCliente();
				if(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null){
					municipio = endereco.getMunicipio();
					sb.append(municipio.getNome());
					sb.append(" - ");
					sb.append(municipio.getUf().getSigla());
				}
				break;
	
			case UFDESCRICAO_CLIENTE :
				endereco = nf.getEnderecoCliente();
				if(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null){
					municipio = endereco.getMunicipio();
					sb.append(municipio.getUf().getNome());
				}
				break;
	
			case UFSIGLA_CLIENTE :
				endereco = nf.getEnderecoCliente();
				if(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null){
					municipio = endereco.getMunicipio();
					sb.append(municipio.getUf().getSigla());
				}
				break;
	
			case CPFCNPJ_CLIENTE :
				cliente = nf.getCliente();
				sb.append(cliente.getCpfOuCnpj());
				break;
	
			case INSCESTADUAL_CLIENTE:
				cliente = nf.getCliente();
				if(cliente.getInscricaoestadual() != null && !cliente.getInscricaoestadual().equals("")){
					sb.append(cliente.getInscricaoestadual());
				}
				break;
	
			case INSCMUNICIPAL_CLIENTE:
				if(nf.getInscricaomunicipal() != null)
					sb.append(!nf.getInscricaomunicipal().trim().equals("") ? nf.getInscricaomunicipal() : null);
				else{
					cliente = nf.getCliente();
					if(cliente.getInscricaomunicipal() != null && !cliente.getInscricaomunicipal().equals("")){
						sb.append(cliente.getInscricaomunicipal());
					}
				}
				break;
	
			case RAZAOSOCIALNOME_CLIENTE:
				cliente = nf.getCliente();
				if(cliente.getRazaosocial() != null && cliente.getRazaosocial().equals("")){
					sb.append(cliente.getRazaosocial());
				} else {
					sb.append(cliente.getNome());
				}
				break;
	
			case RAZAOSOCIAL_CLIENTE:
				cliente = nf.getCliente();
				if(cliente.getRazaosocial() != null && cliente.getRazaosocial().equals("")){
					sb.append(cliente.getRazaosocial());
				}
				break;
	
			case NOME_CLIENTE:
				cliente = nf.getCliente();
				sb.append(cliente.getNome());
				break;
	
			case DIAEMISSAO_NOTA:
				dtemissao = nf.getDtEmissao();
				sb.append(SinedDateUtils.getDateProperty(dtemissao, Calendar.DAY_OF_MONTH));
				break;
	
			case MESNUMEMISSAO_NOTA:
				dtemissao = nf.getDtEmissao();
				sb.append((SinedDateUtils.getDateProperty(dtemissao, Calendar.MONTH) + 1));
				break;
	
			case MESDESCRICAOEMISSAO_NOTA:
				dtemissao = nf.getDtEmissao();
				
				int month = SinedDateUtils.getDateProperty(dtemissao, Calendar.MONTH);
				String mesEmissao = "";
				if(month == 0) 	mesEmissao = "Janeiro";
				if(month == 1) 	mesEmissao = "Fevereiro";
				if(month == 2) 	mesEmissao = "Mar�o";
				if(month == 3) 	mesEmissao = "Abril";
				if(month == 4) 	mesEmissao = "Maio";
				if(month == 5) 	mesEmissao = "Junho";
				if(month == 6) 	mesEmissao = "Julho";
				if(month == 7) 	mesEmissao = "Agosto";
				if(month == 8) 	mesEmissao = "Setembro";
				if(month == 9) 	mesEmissao = "Outubro";
				if(month == 10) mesEmissao = "Novembro";
				if(month == 11) mesEmissao = "Dezembro";
				
				sb.append(mesEmissao);
				break;
	
			case ANOEMISSAO_NOTA:
				dtemissao = nf.getDtEmissao();
				sb.append(SinedDateUtils.getDateProperty(dtemissao, Calendar.YEAR));
				break;
	
			case VALORSERVICOS_NOTA:
				sb.append(nf.getValorTotalServicos());
				break;
	
			case VALORRETENCAO_NOTA:
				sb.append(nf.getValorImposto());
				break;
	
			case VALORIMPOSTOS_NOTA:
				sb.append(nf.getValorImpostoTotal());
				break;
	
			case VALORISS_NOTA:
				if(nf.getValorIss() != null) 
					sb.append(nf.getValorIss());
				break;
	
			case PERCENTUALISS_NOTA:
				if(nf.getIss() != null) 
					sb.append(nf.getIss());
				break;
	
			case VALORIR_NOTA:
				if(nf.getValorIr() != null) 
					sb.append(nf.getValorIr());
				break;
	
			case PERCENTUALIR_NOTA:
				if(nf.getIr() != null) 
					sb.append(nf.getIr());
				break;
	
			case VALORINSS_NOTA:
				if(nf.getValorInss() != null) 
					sb.append(nf.getValorInss());
				break;
	
			case PERCENTUALINSS_NOTA:
				if(nf.getInss() != null) 
					sb.append(nf.getInss());
				break;
	
			case VALORPIS_NOTA:
				if(nf.getValorPis() != null) 
					sb.append(nf.getValorPis());
				break;
	
			case PERCENTUALPIS_NOTA:
				if(nf.getPis() != null) 
					sb.append(nf.getPis());
				break;
	
			case VALORCOFINS_NOTA:
				if(nf.getValorCofins() != null) 
					sb.append(nf.getValorCofins());
				break;
	
			case PERCENTUALCOFINS_NOTA:
				if(nf.getCofins() != null) 
					sb.append(nf.getCofins());
				break;
	
			case VALORCSLL_NOTA:
				if(nf.getValorCsll() != null) 
					sb.append(nf.getValorCsll());
				break;
	
			case PERCENTUALCSLL_NOTA:
				if(nf.getCsll() != null) 
					sb.append(nf.getCsll());
				break;
	
			case VALORICMS_NOTA:
				if(nf.getValorIcms() != null) 
					sb.append(nf.getValorIcms());
				break;
	
			case PERCENTUALICMS_NOTA:
				if(nf.getIcms() != null) 
					sb.append(nf.getIcms());
				break;
	
			case DUPLICATA_NOTA:
				sb.append(nf.getDuplicata());
				break;
	
			case FATURA_NOTA:
				sb.append(nf.getFatura());
				break;
	
			case PRACAPAGAMENTO_NOTA:
				sb.append(nf.getPracaPagamento());
				break;
	
			case VALORTOTALEXTENSO_NOTA:
				sb.append(new Extenso(nf.getValorNota()).toString());
				break;
	
			case VOLORTOTAL_NOTA:
				sb.append(nf.getValorNota().subtract(nf.getDescontocondicionado()).toString());
				break;

			case DTVENCIMENTO_NOTA:
				sb.append(SinedDateUtils.toString(nf.getDtVencimento()));
				break;
	
			case DTVENCIMENTO_NOTA_TIPO_VENCIMENTO:
				if(nf != null && nf.getDtVencimento() != null)
					sb.append(SinedDateUtils.toString(nf.getDtVencimento()));
				else if(nf != null && nf.getTipovencimento() != null)
					sb.append(nf.getTipovencimento().getNome() != null ? nf.getTipovencimento().getNome() : "");
				break;
				
			case BASE_CALCULO:
				sb.append(nf.getBasecalculo());
				break;
				
			case DESCONTO_CONDICIONADO:
				sb.append(nf.getDescontocondicionado());
				break;
				
			case DESCONTO_INCONDICIONADO:
				sb.append(nf.getDescontoincondicionado());
				break;
				
			case DEDUCOES:
				sb.append(nf.getDeducao());
				break;
				
			case OUTRAS_RETENCOES:
				sb.append(nf.getOutrasretencoes());
				break;
				
			case CDCONTRATO:
				String cdcontrato = "";
				if(nf.getListaNotaContrato() != null){
					for(int i = 0; i < nf.getListaNotaContrato().size(); i++){
						if(nf.getListaNotaContrato().get(i).getContrato().getCdcontrato() != null){
							cdcontrato += nf.getListaNotaContrato().get(i).getContrato().getCdcontrato().toString() + ", ";
						}
					}
				}
				if(!cdcontrato.equals("")){
					cdcontrato = cdcontrato.substring(0, cdcontrato.length() - 2);
				}
				sb.append(cdcontrato);
				break;
			
			case CDDOCUMENTO:
				if(nf.getListaNotaDocumento() != null){
					String coddocumento = CollectionsUtil.listAndConcatenate(nf.getListaNotaDocumento(), "documento.cddocumento", ",");
					sb.append(coddocumento);
				}
				break;
			
			case TIPO_VENCIMENTO:
				if(nf.getTipovencimento() != null)
					sb.append(nf.getTipovencimento().getNome() != null ? nf.getTipovencimento().getNome() : "");
				break;
				
			case NATUREZA_OPERACAO:				
				sb.append(nf.getNaturezaoperacao() != null && nf.getNaturezaoperacao().getDescricao() != null ? nf.getNaturezaoperacao().getDescricao():" ");
				break;
				
			case TELEFONE:				
				sb.append(nf.getCliente() != null && nf.getCliente().getTelefone() != null ? nf.getCliente().getTelefone():" ");
				break;
				
			case DADOS_ADICIONAIS:				
				sb.append(nf.getDadosAdicionais() != null ? nf.getDadosAdicionais():" ");
				break;
				
			default:
				break;
		}
		return sb.toString();
	}
	
	private List<Empresaconfiguracaonfcampo> getCamposLinha(List<Empresaconfiguracaonfcampo> listaCampo, int i) {
		List<Empresaconfiguracaonfcampo> lista = new ArrayList<Empresaconfiguracaonfcampo>();
		for (Empresaconfiguracaonfcampo campo : listaCampo) {
			if(campo.getLinha() != null && campo.getLinha().equals(i)){
				lista.add(campo);
			}
		}
		
		return lista;
	}
	
}
