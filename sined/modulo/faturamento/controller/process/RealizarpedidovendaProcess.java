package br.com.linkcom.sined.modulo.faturamento.controller.process;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/faturamento/process/Realizarpedidovenda", authorizationModule=ProcessAuthorizationModule.class)
public class RealizarpedidovendaProcess extends MultiActionController{
	
	private UsuarioService usuarioService;
	
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}

	/**
	 * M�todo principal para a exibi��o da tela Realizar Pedido de Venda
	 * 
	 * @author Thiago Augusto
	 * @param request
	 * @param pedidoVenda
	 * @return
	 */
	@DefaultAction
	public ModelAndView index(WebRequestContext request, Pedidovenda pedidoVenda) {
		String parametros = "";
		
		Boolean closeOnSave = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("closeOnSave")) ? request.getParameter("closeOnSave") : "FALSE");
		if (closeOnSave != null) {
			parametros+= "&closeOnSave="+closeOnSave;
		}
		
		Boolean showListagemLink = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("showListagemLink")) ? request.getParameter("showListagemLink") : "TRUE");
		if (showListagemLink != null) {
			parametros+= "&showListagemLink="+showListagemLink;
		}
		
		Boolean closeOnCancel = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("closeOnCancel")) ? request.getParameter("closeOnCancel") : "FALSE");
		if (closeOnCancel != null) {
			parametros+= "&closeOnCancel="+closeOnCancel;
		}
		
		String cdvendaorcamento = request.getParameter("cdvendaorcamento");
		if(cdvendaorcamento != null && !cdvendaorcamento.equals("")){
			parametros += "&cdvendaorcamento=" + cdvendaorcamento;
		}
		
		String cliente = request.getParameter("cliente");
		if(cliente != null && !cliente.isEmpty() && !"<null>".equalsIgnoreCase(cliente)) {
			String cdcliente = cliente.substring(cliente.indexOf("=")+1, cliente.indexOf(","));
			parametros+= "&cdcliente=" + cdcliente;
		}
		
		Boolean emitirComprovante = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("emitirComprovante")) ? request.getParameter("emitirComprovante") : "TRUE");
		if (emitirComprovante != null) {
			parametros += "&emitirComprovante=" + emitirComprovante;
		}
		
		request.setAttribute("ignoreHackAndroidDownload", true);
		if((parametros == null || "".equals(parametros)) && usuarioService.isSemprelogaroffline(SinedUtil.getUsuarioLogado())){
			SinedUtil.redirecionamento(request, "/offline/crud/Pedidovenda?ACAO=criar");
			return null;
		}
		SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda?ACAO=criar" + parametros);
		return null;
	}
}
