package br.com.linkcom.sined.modulo.faturamento.controller.process;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/faturamento/process/RealizarVendaOrcamento", authorizationModule=ProcessAuthorizationModule.class)
public class RealizarVendaorcamentoProcess extends MultiActionController {

	@DefaultAction
	public void index(WebRequestContext request, Vendaorcamento vendaorcamento){
		String cliente = request.getParameter("cliente");
		String parametros = "";
		
		Boolean closeOnSave = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("closeOnSave")) ? request.getParameter("closeOnSave") : "FALSE");
		if (closeOnSave != null) {
			parametros+= "&closeOnSave="+closeOnSave;
		}
		
		Boolean showListagemLink = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("showListagemLink")) ? request.getParameter("showListagemLink") : "TRUE");
		if (showListagemLink != null) {
			parametros+= "&showListagemLink="+showListagemLink;
		}
		
		Boolean closeOnCancel = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("closeOnCancel")) ? request.getParameter("closeOnCancel") : "FALSE");
		if (closeOnCancel != null) {
			parametros+= "&closeOnCancel="+closeOnCancel;
		}
		
		Boolean emitirComprovante = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("emitirComprovante")) ? request.getParameter("emitirComprovante") : "TRUE");
		if (emitirComprovante != null) {
			parametros += "&emitirComprovante=" + emitirComprovante;
		}

		if(cliente != null && !cliente.isEmpty() && !"<null>".equalsIgnoreCase(cliente)) {
			String cdcliente = cliente.substring(cliente.indexOf("=")+1, cliente.indexOf(","));
			SinedUtil.redirecionamento(request, "/faturamento/crud/VendaOrcamento?ACAO=criar&cdcliente="+cdcliente + parametros);
		} else {
			SinedUtil.redirecionamento(request, "/faturamento/crud/VendaOrcamento?ACAO=criar" + parametros);
		}
	}
	
}