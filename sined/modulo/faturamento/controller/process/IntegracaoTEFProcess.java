package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Arquivotef;
import br.com.linkcom.sined.geral.bean.Arquivotefitem;
import br.com.linkcom.sined.geral.bean.Configuracaotef;
import br.com.linkcom.sined.geral.bean.Configuracaotefadquirente;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoduplicata;
import br.com.linkcom.sined.geral.bean.Vendapagamento;
import br.com.linkcom.sined.geral.bean.enumeration.MeioPagamentoNFe;
import br.com.linkcom.sined.geral.service.ArquivotefService;
import br.com.linkcom.sined.geral.service.ConfiguracaotefService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoduplicataService;
import br.com.linkcom.sined.geral.service.VendapagamentoService;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/faturamento/process/IntegracaoTEF", authorizationModule=ProcessAuthorizationModule.class)
public class IntegracaoTEFProcess extends MultiActionController {
	
	private NotaService notaService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private EmpresaService empresaService;
	private NotafiscalprodutoduplicataService notafiscalprodutoduplicataService;
	private ConfiguracaotefService configuracaotefService;
	private ArquivotefService arquivotefService;
	private VendapagamentoService vendapagamentoService;
	
	public void setVendapagamentoService(
			VendapagamentoService vendapagamentoService) {
		this.vendapagamentoService = vendapagamentoService;
	}
	public void setArquivotefService(ArquivotefService arquivotefService) {
		this.arquivotefService = arquivotefService;
	}
	public void setConfiguracaotefService(
			ConfiguracaotefService configuracaotefService) {
		this.configuracaotefService = configuracaotefService;
	}
	public void setNotafiscalprodutoduplicataService(
			NotafiscalprodutoduplicataService notafiscalprodutoduplicataService) {
		this.notafiscalprodutoduplicataService = notafiscalprodutoduplicataService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setNotafiscalprodutoService(
			NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	public void setNotaService(NotaService notaService) {
		this.notaService = notaService;
	}
	
	public ModelAndView confirmacao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		String tipo = request.getParameter("tipo");
		
		Arquivotef arquivotef = new Arquivotef();
		
		try {
			Empresa empresa = null;
			Configuracaotef configuracaotef = null;
			List<Arquivotefitem> listaArquivotefitem = new ListSet<Arquivotefitem>(Arquivotefitem.class);
			
			if(tipo.equals("notafiscalprodutoduplicata")){
				if(notaService.haveNFDiferenteStatus(whereIn, NotaStatus.EMITIDA)){
					throw new SinedException("A(s) nota(s) fiscal(is) tem que estar na situa��o 'EMITIDA'.");
				}
				
				String msgErro = notafiscalprodutoService.validaDivergenciaValoresParcela(request);
				if(org.apache.commons.lang.StringUtils.isNotEmpty(msgErro)){
					throw new SinedException(msgErro);
				}
				
				empresa = empresaService.getEmpresaForTEFNotas(whereIn);
				configuracaotef = configuracaotefService.loadByEmpresa(empresa);
				arquivotef.setConfiguracaotef(configuracaotef);
				
				List<Notafiscalprodutoduplicata> listaNotafiscalprodutoduplicata = notafiscalprodutoduplicataService.findByNotaForIntegracaoTEF(whereIn);
				for (Notafiscalprodutoduplicata notafiscalprodutoduplicata : listaNotafiscalprodutoduplicata) {
					Documentotipo documentotipo = notafiscalprodutoduplicata.getDocumentotipo();
					if(StringUtils.isBlank(notafiscalprodutoduplicata.getAutorizacao()) && 
							documentotipo != null && 
							documentotipo.getMeiopagamentonfe() != null && 
							(documentotipo.getMeiopagamentonfe().equals(MeioPagamentoNFe.CARTAO_CREDITO) || 
									documentotipo.getMeiopagamentonfe().equals(MeioPagamentoNFe.CARTAO_DEBITO))){
						Arquivotefitem arquivotefitem = new Arquivotefitem();
						arquivotefitem.setNotafiscalprodutoduplicata(notafiscalprodutoduplicata);
						listaArquivotefitem.add(arquivotefitem);
						
						arquivotef.setValortotal(arquivotef.getValortotal().add(notafiscalprodutoduplicata.getValor()));
					}
				}
			} else if(tipo.equals("vendapagamento")){
				empresa = empresaService.getEmpresaByVendas(whereIn);
				configuracaotef = configuracaotefService.loadByEmpresa(empresa);
				arquivotef.setConfiguracaotef(configuracaotef);
				
				List<Vendapagamento> listaVendapagamento = vendapagamentoService.findByVendaForIntegracaoTEF(whereIn);
				for (Vendapagamento vendapagamento : listaVendapagamento) {
					Documentotipo documentotipo = vendapagamento.getDocumentotipo();
					if(StringUtils.isBlank(vendapagamento.getAutorizacao()) && 
							documentotipo != null && 
							documentotipo.getMeiopagamentonfe() != null && 
							(documentotipo.getMeiopagamentonfe().equals(MeioPagamentoNFe.CARTAO_CREDITO) || 
									documentotipo.getMeiopagamentonfe().equals(MeioPagamentoNFe.CARTAO_DEBITO))){
						Arquivotefitem arquivotefitem = new Arquivotefitem();
						arquivotefitem.setVendapagamento(vendapagamento);
						listaArquivotefitem.add(arquivotefitem);
						
						arquivotef.setValortotal(arquivotef.getValortotal().add(vendapagamento.getValororiginal()));
					}
				}
			}
			
			if(listaArquivotefitem.isEmpty()){
				throw new SinedException("N�o foi identificado nenhum pagamento com meio de pagamento de cart�o de cr�dito ou d�bito.");
			}
			arquivotef.setListaArquivotefitem(listaArquivotefitem);
			
			List<Configuracaotefadquirente> listaConfiguracaotefadquirente = new ArrayList<Configuracaotefadquirente>(configuracaotef.getListaConfiguracaotefadquirente());
			Collections.sort(listaConfiguracaotefadquirente, new Comparator<Configuracaotefadquirente>() {
				@Override
				public int compare(Configuracaotefadquirente o1, Configuracaotefadquirente o2) {
					return o1.getAdquirente().compareTo(o2.getAdquirente());
				}
			});
			
			request.setAttribute("listaConfiguracaotefterminal", arquivotef.getConfiguracaotef() != null ? arquivotef.getConfiguracaotef().getListaConfiguracaotefterminal() : null);
			request.setAttribute("listaConfiguracaotefadquirente", listaConfiguracaotefadquirente);
			request.setAttribute("tipo", tipo);
			
			return new ModelAndView("direct:/process/popup/confirmacaoIntegracaoTEF", "bean", arquivotef);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			SinedUtil.fechaPopUp(request);
			return null;
		}
	}	
	
	public ModelAndView saveConfirmacao(WebRequestContext request, Arquivotef arquivotef){
		try {
			arquivotefService.saveAndSendControlPay(arquivotef);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		request.addMessage("Pagamento enviado ao TEF com sucesso.");
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	public ModelAndView imprimirComprovanteTEF(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		String tipo = request.getParameter("tipo");
		String urlRetorno = null;
		if(StringUtils.equalsIgnoreCase(tipo, "venda")){
			urlRetorno = "/faturamento/crud/Venda?ACAO=listagem";
		} else if(StringUtils.equalsIgnoreCase(tipo, "nfe")){
			urlRetorno = "/faturamento/crud/Notafiscalproduto?ACAO=listagem";
		} else if(StringUtils.equalsIgnoreCase(tipo, "nfce")){
			urlRetorno = "/faturamento/crud/Notafiscalconsumidor?ACAO=listagem";
		}
		
		try {
			arquivotefService.imprimirComprovante(request, whereIn, tipo);
			return new ModelAndView("redirect:" + urlRetorno);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na impress�o: " + e.getMessage());
			return new ModelAndView("redirect:" + urlRetorno);
		}
	}
	
}